// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### System: GTA V Online Mission Controller - Initialization ----------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Script: FM_Mission_Controller_2020.sc -----------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: This header contains the initializes mission controller content.
// ##### Process pregame is declared and processed here. Starting Positions, Starting outfits, asset loading, interior loading, and processing the initial entity creation.
// ##### Init calls wrappers from various other places like Spawning where the Spawning systems are held.
// ##### Quick Restarts are also processed in _INIT.
// ##### Mission Variation systems are called from INIT to override globals and make a mission behave differently based on parameters that the creator has set up.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### This script was forked from FM_Mission_Controller.sc at the beginning of 2020		 																				  
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

USING "FM_Mission_Controller_Variations_2020.sch"
USING "FM_Mission_Controller_Spawning_2020.sch"

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Initialization Utility/Helper functions  																								
// ##### Description: Functions that are used in various places of INIT or are used as wrappers / helper functions.												
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL HAVE_ALL_PLAYERS_PROCESSED_PRE_GAME()
	//Failsafe Timeout
	IF NOT HAS_NET_TIMER_STARTED(MC_serverBD.tdProcessedPreGameTimeout)
		REINIT_NET_TIMER(MC_serverBD.tdProcessedPreGameTimeout)
		PRINTLN("[HAVE_ALL_PLAYERS_PROCESSED_PRE_GAME] Starting MC_serverBD.tdProcessedPreGameTimeout timer")
	ELIF (GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdProcessedPreGameTimeout) >= PROCESSED_PRE_GAME_FAILSAFE_TIMEOUT)
		RESET_NET_TIMER(MC_serverBD.tdProcessedPreGameTimeout)
		PRINTLN("[HAVE_ALL_PLAYERS_PROCESSED_PRE_GAME] MC_serverBD.tdProcessedPreGameTimeout has passed 20 seconds!")
		
		RETURN TRUE
	ENDIF
	
	//Players joining the mission
	INT iPlayerJoiningBitset = Get_Bitfield_Of_Players_On_Or_Joining_Mission(Get_UniqueID_For_This_Players_Mission(LocalPlayer))
	
	INT iJoiningTotal = 0
	
	INT i
	
	PRINTLN("[PLAYER_LOOP] - HAVE_ALL_PLAYERS_PROCESSED_PRE_GAME 2")
	REPEAT NUM_NETWORK_PLAYERS i
		IF IS_BIT_SET(iPlayerJoiningBitset, i)
			
			iJoiningTotal += 1
			
			PRINTLN("[HAVE_ALL_PLAYERS_PROCESSED_PRE_GAME] - Player ", i, " is set to join")
			
		ENDIF
	ENDREPEAT
	
	PRINTLN("[HAVE_ALL_PLAYERS_PROCESSED_PRE_GAME] iJoiningTotal = ", iJoiningTotal)
	
	//Total participants who have processed pre game
	INT iJoinedPlayers = 0
	
	INT iParticipant
	
	PRINTLN("[PLAYER_LOOP] - HAVE_ALL_PLAYERS_PROCESSED_PRE_GAME")
	REPEAT NUM_NETWORK_PLAYERS iParticipant
		PARTICIPANT_INDEX participantIndex = INT_TO_NATIVE(PARTICIPANT_INDEX, iParticipant)
		
		IF NETWORK_IS_PARTICIPANT_ACTIVE(participantIndex)
			PRINTLN("[HAVE_ALL_PLAYERS_PROCESSED_PRE_GAME] - Participant ", iParticipant, " is active")
			
			PLAYER_INDEX playerIndex = NETWORK_GET_PLAYER_INDEX(participantIndex)
			
			IF NETWORK_IS_PLAYER_ACTIVE(playerIndex)
				PRINTLN("[HAVE_ALL_PLAYERS_PROCESSED_PRE_GAME] - Player ", iParticipant, " is active")
				
				IF IS_BIT_SET(iPlayerJoiningBitset, iParticipant)
					PRINTLN("[HAVE_ALL_PLAYERS_PROCESSED_PRE_GAME] - Player ", iParticipant, " is joining")
					
					IF IS_BIT_SET(MC_PlayerBD[iParticipant].iClientBitSet3, PBBOOL3_PROCESSED_PRE_GAME)
						PRINTLN("[HAVE_ALL_PLAYERS_PROCESSED_PRE_GAME] - Player ", iParticipant, " has processed pre game")
						
						iJoinedPlayers += 1
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	PRINTLN("[HAVE_ALL_PLAYERS_PROCESSED_PRE_GAME] iJoinedPlayers = ", iJoinedPlayers)
	
	//Proceed if everyone is ready
	IF iJoinedPlayers >= iJoiningTotal
		PRINTLN("[HAVE_ALL_PLAYERS_PROCESSED_PRE_GAME] TRUE")
		
		RETURN TRUE
	ENDIF
	
	PRINTLN("[HAVE_ALL_PLAYERS_PROCESSED_PRE_GAME] FALSE")
	
	RETURN FALSE
ENDFUNC

//Returns TRUE when the mission controller should progress.
FUNC BOOL SHOULD_MISSION_CONTROLLER_PROGRESS()

	IF HAS_FIRST_STRAND_MISSION_BEEN_PASSED()		
	AND NOT SHOULD_MISSION_CONTROLLER_CONTINUE_AFTER_CUTSCENE_AND_RESTART()
	AND g_sTransitionSessionData.sStrandMissionData.iSwitchState != 0
		#IF IS_DEBUG_BUILD
			PRINTLN("[TS] [MSRAND] - SHOULD_MISSION_CONTROLLER_PROGRESS iSwitchState = ", g_sTransitionSessionData.sStrandMissionData.iSwitchState)
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_CORONA_READY_TO_START_WITH_JOB()
		PRINTLN("[MMacK][Intro] IS_CORONA_READY_TO_START_WITH_JOB")
		RETURN TRUE
	ELSE	
		PRINTLN("[MMacK][Intro] IS_CORONA_READY_TO_START_WITH_JOB false")
	ENDIF	

	IF IS_FAKE_MULTIPLAYER_MODE_SET()
		PRINTLN("[MMacK][Intro] IS_FAKE_MULTIPLAYER_MODE_SET")
		RETURN TRUE
	ELSE	
		PRINTLN("[MMacK][Intro] IS_FAKE_MULTIPLAYER_MODE_SET false")
	ENDIF
		
	IF bIsSCTV OR DID_I_JOIN_MISSION_AS_SPECTATOR()
		PRINTLN("[MMacK][Intro] SCTV")
		RETURN TRUE
	ELSE	
		PRINTLN("[MMacK][Intro] IS_PLAYER_SCTV or DID_I_JOIN_MISSION_AS_SPECTATOR false")
	ENDIF
	
	PRINTLN("[MMacK][Intro] SHOULD_MISSION_CONTROLLER_PROGRESS returning false.")
	
	RETURN FALSE
ENDFUNC

FUNC BOOL ARE_ALL_PLAYERS_READY_AFTER_START_POSITION_SET_INIT()
	INT i
	PLAYER_INDEX PlayerID
	PRINTLN("[PLAYER_LOOP] - ARE_ALL_PLAYERS_READY_AFTER_START_POSITION_SET_INIT")
	REPEAT NUM_NETWORK_PLAYERS i
		PARTICIPANT_INDEX piIndex = INT_TO_PARTICIPANTINDEX(i)		
		IF NETWORK_IS_PARTICIPANT_ACTIVE(piIndex)
			PlayerID = NETWORK_GET_PLAYER_INDEX(piIndex)
			IF IS_NET_PLAYER_OK(PlayerID, FALSE, TRUE)
				IF NOT DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(PlayerID)
					IF NOT IS_BIT_SET(MC_PlayerBD[i].iClientbitSet4, PBBOOL4_START_POSITION_SET)
						PRINTLN("[LM] ARE_ALL_PLAYERS_READY_AFTER_START_POSITION_SET_INIT - waiting on player ", i)
						RETURN FALSE
					ENDIF
				ENDIF 
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

FUNC BOOL ARE_JIP_SPECTATORS_BLOCKED_ON_CURRENT_MISSION()
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySix, ciOptionsBS26_DisableJIPSpectators) // all new heist content should have an option to do this.
		RETURN TRUE	
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_JOINING_PLAYER_A_VALID_MISSION_PLAYER(INT iplayer)
	
	PLAYER_INDEX playerID = INT_TO_PLAYERINDEX(iplayer)		
	
	// Is player active and in my vote?
	IF NETWORK_IS_PLAYER_ACTIVE(playerID)
	AND NOT IS_THIS_PLAYER_NOT_GOING_TO_TAKE_PART_IN_THIS_CORONA(playerID)
	AND NOT IS_PLAYER_SCTV(playerID)
	AND NOT DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(playerID)
	AND NOT IS_PLAYER_IN_PROCESS_OF_JOINING_MISSION_AS_SPECTATOR(playerID)
		
		RETURN TRUE
		
	#IF IS_DEBUG_BUILD
	ELSE
		IF NOT NETWORK_IS_PLAYER_ACTIVE(playerID)
			PRINTLN("[RCC MISSION] IS_JOINING_PLAYER_A_VALID_MISSION_PLAYER - Player ",iplayer," rejected, player not active")
		ELIF IS_THIS_PLAYER_NOT_GOING_TO_TAKE_PART_IN_THIS_CORONA(playerID)
			PRINTLN("[RCC MISSION] IS_JOINING_PLAYER_A_VALID_MISSION_PLAYER - Player ",iplayer," rejected, player not going to take part in this corona")
		ELIF IS_PLAYER_SCTV(playerID)
			PRINTLN("[RCC MISSION] IS_JOINING_PLAYER_A_VALID_MISSION_PLAYER - Player ",iplayer," rejected, player is sctv")
		ELIF DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(playerID)
			PRINTLN("[RCC MISSION] IS_JOINING_PLAYER_A_VALID_MISSION_PLAYER - Player ",iplayer," rejected, player joined as spectator")
		ELIF IS_PLAYER_IN_PROCESS_OF_JOINING_MISSION_AS_SPECTATOR(playerID)
			PRINTLN("[RCC MISSION] IS_JOINING_PLAYER_A_VALID_MISSION_PLAYER - Player ",iplayer," rejected, player in process of joining as spectator")
		ELSE
			PRINTLN("[RCC MISSION] IS_JOINING_PLAYER_A_VALID_MISSION_PLAYER - Player ",iplayer," rejected, unknown reason")
		ENDIF
	#ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC INT GET_NUMBER_PLAYERS_JOINING_MISSION(INT iteam)
	
	INT iTempplayerjoinbitset,icount,iplayer
	
	iTempplayerjoinbitset = Get_Bitfield_Of_Players_On_Or_Joining_Mission(Get_UniqueID_For_This_Players_Mission(LocalPlayer))
	PRINTLN("[RCC MISSION] GET_NUMBER_PLAYERS_JOINING_MISSION called for team ",iteam,", iTempplayerjoinbitset = ",iTempplayerjoinbitset)
	
	PRINTLN("[PLAYER_LOOP] - GET_NUMBER_PLAYERS_JOINING_MISSION")
	REPEAT NUM_NETWORK_PLAYERS iplayer
		IF IS_BIT_SET(iTempplayerjoinbitset,iplayer)
			IF GlobalplayerBD_FM[iplayer].sClientCoronaData.iTeamChosen = iteam
				IF IS_JOINING_PLAYER_A_VALID_MISSION_PLAYER(iplayer)
					icount++
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	PRINTLN("[RCC MISSION] GET_NUMBER_PLAYERS_JOINING_MISSION for team = ",iteam," is = ",icount)
	
	RETURN icount

ENDFUNC

FUNC BOOL IS_PLAYER_USING_SEQUENTIAL_SPAWNING_PROCEDURES()

	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitsetTwentyFour, ciOptionsBS24_SequentialStartPlayerSpawning)
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_SET_INTO_NEAREST_VEHICLE)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC PED_INDEX GET_SCARY_PED_ON_TEAM_ZERO()
	INT tempScaryPedInt = GET_NEAREST_PLAYER_TO_ENTITY_ON_TEAM(LocalPlayerPed, 0)
	PLAYER_INDEX tempScaryPedIndex = INT_TO_PLAYERINDEX( tempScaryPedInt )
	PED_INDEX tempScaryPed = GET_PLAYER_PED( tempScaryPedIndex )
	
	RETURN tempScaryPed
ENDFUNC

FUNC BOOL IS_EVERYONE_RUNNING(BOOL bPrints)

INT iparticipant
PARTICIPANT_INDEX tempPart
PLAYER_INDEX tempPlayer

	PRINTLN("[PLAYER_LOOP] - IS_EVERYONE_RUNNING")
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iparticipant
		tempPart = INT_TO_PARTICIPANTINDEX(iparticipant)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
			
			tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
			
			IF IS_NET_PLAYER_OK(tempPlayer, FALSE, FALSE)
			AND (NOT IS_PLAYER_SPECTATOR_ONLY(tempPlayer))
				IF GET_MC_CLIENT_GAME_STATE(iparticipant) < GAME_STATE_RUNNING
					IF bPrints
						PRINTLN("[RCC MISSION] IS_EVERYONE_RUNNING Waiting for iparticipant: ", iparticipant)
					ENDIF
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN TRUE

ENDFUNC

FUNC BOOL IS_EVERYONE_READY_FOR_NEXT_ROUND()
	
	IF IS_THIS_A_ROUNDS_MISSION()
	OR IS_THIS_A_QUICK_RESTART_JOB()
		
		INT iparticipant
		PARTICIPANT_INDEX tempPart
		BOOL bIsPlayerSpectator 
		
		PRINTLN("[PLAYER_LOOP] - IS_EVERYONE_READY_FOR_NEXT_ROUND")
		REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iparticipant
			
			tempPart = INT_TO_PARTICIPANTINDEX(iparticipant)
			
			IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
				
				bIsPlayerSpectator = IS_BIT_SET( MC_playerBD[iparticipant].iClientBitSet, PBBOOL_ANY_SPECTATOR )
				
				IF SHOULD_THIS_SPECTATOR_BE_ROAMING_AT_START(NETWORK_GET_PLAYER_INDEX(tempPart))
				OR IS_PLAYER_SCTV(NETWORK_GET_PLAYER_INDEX(tempPart))
					bIsPlayerSpectator = TRUE
				ENDIF
				
				IF MC_playerBD[iparticipant].iSyncRoundCutsceneStage != ANIM_STAGE_FOUR
				AND MC_playerBD[iparticipant].iSyncRoundIntroCutsceneStage != JOB_CUT_PLAY 
				AND MC_playerBD[iparticipant].iSyncRoundIntroCutsceneStage != JOB_CUT_TIME_TRAIL
				AND MC_playerBD[iparticipant].iSyncRoundTeamCutsceneStage != JOB_TEAMCUT_PLAY
				AND bIsPlayerSpectator = FALSE
					CPRINTLN(DEBUG_MISSION,"[RCC MISSION][ROUNDCAM] - IS_EVERYONE_READY_FOR_NEXT_ROUND: FALSE for iparticipant = ",iparticipant, " MC_playerBD[iparticipant].iSyncRoundCutsceneStage = ", ENUM_TO_INT(MC_playerBD[iparticipant].iSyncRoundCutsceneStage) )
					CPRINTLN(DEBUG_MISSION,"[RCC MISSION][ROUNDCAM] - IS_EVERYONE_READY_FOR_NEXT_ROUND: FALSE for iparticipant = ",iparticipant, " MC_playerBD[iparticipant].iSyncRoundTeamCutsceneStage = ", ENUM_TO_INT(MC_playerBD[iparticipant].iSyncRoundTeamCutsceneStage) )
					CPRINTLN(DEBUG_MISSION,"[RCC MISSION][ROUNDCAM] - IS_EVERYONE_READY_FOR_NEXT_ROUND: FALSE for iparticipant = ",iparticipant, " MC_playerBD[iparticipant].iSyncRoundIntroCutsceneStage = ", ENUM_TO_INT(MC_playerBD[iparticipant].iSyncRoundIntroCutsceneStage) )
					CPRINTLN(DEBUG_MISSION,"[RCC MISSION][ROUNDCAM] - IS_EVERYONE_READY_FOR_NEXT_ROUND: FALSE for iparticipant = ",iparticipant, " bIsPlayerSpectator = ", bIsPlayerSpectator )
					
					iparticipant = NETWORK_GET_MAX_NUM_PARTICIPANTS()
					
					RETURN FALSE
				ELSE
					CPRINTLN(DEBUG_MISSION,"[RCC MISSION][ROUNDCAM] - IS_EVERYONE_READY_FOR_NEXT_ROUND: TRUE for iparticipant = ",iparticipant, " MC_playerBD[iparticipant].iSyncRoundCutsceneStage = ", ENUM_TO_INT(MC_playerBD[iparticipant].iSyncRoundCutsceneStage) )
					CPRINTLN(DEBUG_MISSION,"[RCC MISSION][ROUNDCAM] - IS_EVERYONE_READY_FOR_NEXT_ROUND: TRUE for iparticipant = ",iparticipant, " MC_playerBD[iparticipant].iSyncRoundTeamCutsceneStage = ", ENUM_TO_INT(MC_playerBD[iparticipant].iSyncRoundTeamCutsceneStage) )
					CPRINTLN(DEBUG_MISSION,"[RCC MISSION][ROUNDCAM] - IS_EVERYONE_READY_FOR_NEXT_ROUND: TRUE for iparticipant = ",iparticipant, " MC_playerBD[iparticipant].iSyncRoundIntroCutsceneStage = ", ENUM_TO_INT(MC_playerBD[iparticipant].iSyncRoundIntroCutsceneStage) )
					CPRINTLN(DEBUG_MISSION,"[RCC MISSION][ROUNDCAM] - IS_EVERYONE_READY_FOR_NEXT_ROUND: TRUE for iparticipant = ",iparticipant, " bIsPlayerSpectator = ", bIsPlayerSpectator )
				ENDIF
				
			ENDIF
			
		ENDREPEAT
		
		CPRINTLN(DEBUG_MISSION,"[RCC MISSION][ROUNDCAM] - IS_EVERYONE_READY_FOR_NEXT_ROUND = TRUE, everyone is ready ")
		RETURN TRUE
	ELSE
		CPRINTLN(DEBUG_MISSION,"[RCC MISSION][ROUNDCAM] - IS_EVERYONE_READY_FOR_NEXT_ROUND = TRUE, not a round match or quick restartt ")
		RETURN TRUE
	ENDIF
	
	CPRINTLN(DEBUG_MISSION,"[RCC MISSION][ROUNDCAM] - IS_EVERYONE_READY_FOR_NEXT_ROUND = FALSE, wait ")
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    This function waits for all non-spectating players to have synchronised for all intro
///    cutscenes to happen at the same time
/// RETURNS:
///    TRUE when every participant has MC_playerBD[iparticipant].bSyncCutsceneDone set to true
FUNC BOOL IS_EVERYONE_READY_FOR_GAMEPLAY()
	
	IF IS_THIS_A_ROUNDS_MISSION()
	OR IS_THIS_A_QUICK_RESTART_JOB()
		INT iParticipant
		PARTICIPANT_INDEX tempPart
		
		PRINTLN("[PLAYER_LOOP] - IS_EVERYONE_READY_FOR_GAMEPLAY")
		REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
			tempPart = INT_TO_PARTICIPANTINDEX(iParticipant)
			
			IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
				IF MC_playerBD[iParticipant].bSyncCutsceneDone = FALSE
				AND NOT IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet, PBBOOL_ANY_SPECTATOR)
				AND NOT IS_PARTICIPANT_A_SPECTATOR(iParticipant)
					CPRINTLN(DEBUG_MISSION,"[RCC MISSION][ROUNDCAM] - IS_EVERYONE_READY_FOR_GAMEPLAY: FALSE iParticipant = ", iParticipant, " MC_playerBD[iParticipant].bSyncCutsceneDone = ", (MC_playerBD[iParticipant].bSyncCutsceneDone))
					
					RETURN FALSE
				ELSE
					CPRINTLN(DEBUG_MISSION,"[RCC MISSION][ROUNDCAM] - IS_EVERYONE_READY_FOR_GAMEPLAY: iParticipant = ", iParticipant, " MC_playerBD[iParticipant].bSyncCutsceneDone = ", (MC_playerBD[iParticipant].bSyncCutsceneDone))
				ENDIF
			ENDIF
		ENDREPEAT
		
		CPRINTLN(DEBUG_MISSION,"[RCC MISSION][ROUNDCAM] - IS_EVERYONE_READY_FOR_GAMEPLAY = TRUE, everyone is ready")
		
		RETURN TRUE
	ELSE
		CPRINTLN(DEBUG_MISSION,"[RCC MISSION][ROUNDCAM] - IS_EVERYONE_READY_FOR_GAMEPLAY = TRUE, not a round match or a quick restart")
		
		RETURN TRUE
	ENDIF
	
	CPRINTLN(DEBUG_MISSION,"[RCC MISSION][ROUNDCAM] - IS_EVERYONE_READY_FOR_GAMEPLAY = FALSE, wait")
	
	RETURN FALSE
	
ENDFUNC

PROC PRE_PROCESS_WORLD_PROPS()
	INT i
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfWorldProps - 1
		PROCESS_WORLD_PROP(i)
	ENDFOR
ENDPROC 

PROC CLIENT_ASSIGN_LATE_CORONA_DATA()
	
	IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitset4, PBBOOL4_CLIENT_ASSIGNED_LATE_CORONA_DATA)
		EXIT
	ENDIF

	PRINTLN("[LM][PROCESS_PRE_GAME][CLIENT_ASSIGN_LATE_CORONA_DATA] Assigning g_iMyRaceModelChoice: ", g_iMyRaceModelChoice, " g_mnMyRaceModel: ", FMMC_GET_MODEL_NAME_FOR_DEBUG(g_mnMyRaceModel))	
	
	MC_playerBD[iLocalPart].iMyRaceModelChoice = g_iMyRaceModelChoice 
	MC_playerBD[iLocalPart].mnMyRaceModel = g_mnMyRaceModel 
	
	SET_BIT(MC_playerBD[iLocalPart].iClientBitset4, PBBOOL4_CLIENT_ASSIGNED_LATE_CORONA_DATA)
	
ENDPROC
	
/// PURPOSE:
///    This function doesn't allow PROCESS_SCRIPT_INIT to progress until everyone has synchronised
/// RETURNS:
///    TRUE when all players have joined and we are beyond the INITIALISATIOn stage
FUNC BOOL SHOULD_MOVE_TO_MISSION_RUNNING_STAGE()
	BOOL bCanProgress = FALSE

	IF SHOULD_MISSION_CONTROLLER_PROGRESS()
		IF IS_NET_PLAYER_OK(LocalPlayer,TRUE,FALSE)
			
			IF IS_MISSION_USING_ANY_ALT_VAR()
				IF NOT HAS_ALT_VAR_SERVER_DATA_BEEN_SET_YET()
					PRINTLN("[LM][AltVars][SHOULD_MOVE_TO_MISSION_RUNNING_STAGE] - Controller and Corona is ready to move on but we are waiting for Mission Variation Data to sync.")
					RETURN FALSE
				ELSE
					PRINTLN("[LM][AltVars][SHOULD_MOVE_TO_MISSION_RUNNING_STAGE] - Controller and Corona is ready to move on and we are reading Server Data for Mission Variations.")
				ENDIF
			ENDIF
						
			IF NOT IS_BIT_SET(MC_serverBD_4.rsgSpawnSeed.iMiscBS, ciRandomSpawnGroupBS_HasBeenSet)
				IF DID_I_JOIN_MISSION_AS_SPECTATOR()
					IF NOT HAS_NET_TIMER_STARTED(tdInvalidServerBDBailTimer)
						START_NET_TIMER(tdInvalidServerBDBailTimer)
					ELIF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdInvalidServerBDBailTimer, ciJIP_INVALID_SERVER_BD_BAIL_TIMER)					
						ASSERTLN("SHOULD_MOVE_TO_MISSION_RUNNING_STAGE - Bailing as stuck joining a mission where our server broadcast data is invalid as jipper")
						DEBUG_PRINTCALLSTACK()
						NETWORK_BAIL(ENUM_TO_INT(NETWORK_BAIL_TS_SAFETY_BAIL))
					ENDIF
				ENDIF
				PRINTLN("SHOULD_MOVE_TO_MISSION_RUNNING_STAGE - Waiting for ciRandomSpawnGroupBS_HasBeenSet. Require spawn group data before moving forward.")
				RETURN FALSE
			ENDIF
			
			IF MC_serverBD.iNumberOfTeams > 0
			AND IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_ALL_JOINED_START)			
				IF GET_MC_SERVER_GAME_STATE() > GAME_STATE_INI	
				//And we've got a valid hashed mac and match time
				AND g_MissionControllerserverBD_LB.iHashMAC != 0
				AND g_MissionControllerserverBD_LB.iMatchTimeID != 0
				//or the server is at the end stage
				OR GET_MC_SERVER_GAME_STATE() = GAME_STATE_END
					bCanProgress = TRUE
				#IF IS_DEBUG_BUILD
				ELSE
					PRINTLN("[RCC MISSION] MISSION - g_MissionControllerserverBD_LB.iHashMAC = ", g_MissionControllerserverBD_LB.iHashMAC)
					PRINTLN("[RCC MISSION] MISSION - g_MissionControllerserverBD_LB.iMatchTimeID = ", g_MissionControllerserverBD_LB.iMatchTimeID)
					PRINTLN("[RCC MISSION] MISSION - TRANSITION TIME - GAME_STATE_INI  - FRAME COUNT = (", GET_FRAME_COUNT(), ") POSIX TIME = ", GET_CLOUD_TIME_AS_INT(), ")")
					PRINTLN("[RCC MISSION] server still in GAME_STATE_INI !!!!!!")
				#ENDIF
				ENDIF
			
			ELSE
				#IF IS_DEBUG_BUILD
				PRINTLN("[RCC MISSION]  - TRANSITION TIME - MC_serverBD.iNumberOfTeams = 0  - FRAME COUNT = (", GET_FRAME_COUNT(), ") POSIX TIME = ", GET_CLOUD_TIME_AS_INT(), ")")
				IF MC_serverBD.iNumberOfTeams <= 0
					PRINTLN("[RCC MISSION] MC_serverBD.iNumberOfTeams = 0 !!!!!!")
				ENDIF
				IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_ALL_JOINED_START)
					PRINTLN("[RCC MISSION] SBBOOL_ALL_JOINED_START not set")
				ENDIF
				#ENDIF
				IF DID_I_JOIN_MISSION_AS_SPECTATOR()
					IF NETWORK_GET_NUM_PARTICIPANTS() <= 1
						IF NOT HAS_NET_TIMER_STARTED(tdJipNoTeamTimer)
							REINIT_NET_TIMER(tdJipNoTeamTimer)
						ELIF HAS_NET_TIMER_EXPIRED(tdJipNoTeamTimer, ciJIP_NO_TEAM_TIMEOUT)
							ASSERTLN("[RCC MISSION] SHOULD_MOVE_TO_MISSION_RUNNING_STAGE - Bailing as stuck joining empty mission as jipper")
							DEBUG_PRINTCALLSTACK()
							NETWORK_BAIL(ENUM_TO_INT(NETWORK_BAIL_TS_SAFETY_BAIL))
						ENDIF
					ELSE
						RESET_NET_TIMER(tdJipNoTeamTimer)
					ENDIF
				ENDIF
			ENDIF
		#IF IS_DEBUG_BUILD
		ELSE
			PRINTLN("[RCC MISSION] player not ok !!!!!!")
		#ENDIF
		ENDIF
	#IF IS_DEBUG_BUILD
	ELSE
		PRINTLN("[RCC MISSION] SHOULD_MISSION_CONTROLLER_PROGRESS = FALSE")
	#ENDIF
	ENDIF
	
	IF bCanProgress
	AND DID_I_JOIN_MISSION_AS_SPECTATOR()
		RESET_NET_TIMER(tdJipNoTeamTimer)
	ENDIF
	
	RETURN bCanProgress
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Special Case - Cargobob
// ##### Description: Functions and Processes that are geared toward the startup sequence for players using a Cargobob.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC INT private_GET_CARGOBOB_INDEX_TO_SPAWN_IN(INT iTeam)
	INT toReturn
	
	//The spawned vehicles are in a 1D array
	//T1: 0 & 4 ---- T2: 1 & 5 ---- T3: 2 & 6 ---- T4: 3 & 7
	//Setup variables depending on if the ped is in the first or second team vehicle
	IF GET_PARTICIPANT_NUMBER_IN_TEAM() > ciNUM_PLAYERS_PER_AIRCRAFT_SPAWN-1
		toReturn =  iTeam + ciNUM_PLAYERS_PER_AIRCRAFT_SPAWN
	ELSE
		toReturn =  iTeam
	ENDIF
	
	IF toReturn < 0 
	OR toReturn >= 8
		PRINTLN("[JJT] private_GET_CARGOBOB_INDEX_TO_SPAWN_IN - toReturn is invalid! toReturn = ", toReturn)
	ENDIF
	
	RETURN toReturn
ENDFUNC


PROC private_GET_CARGOBOB_POINT_TO_SPAWN(VECTOR& positionOut, FLOAT& headingOut, INT iPlayerTeam, INT slotInTeam, BOOL failsafeOverride = FALSE)
	
	INT cbIndex = private_GET_CARGOBOB_INDEX_TO_SPAWN_IN(iPlayerTeam)
	
	IF failsafeOverride
		PRINTLN("[JJT] private_GET_CARGOBOB_POINT_TO_SPAWN - failsafeOverride is set!")
		cbIndex = iPlayerTeam
	ENDIF
	
	IF cbIndex < 0 OR cbIndex >=8
		PRINTLN("[JJT] private_GET_CARGOBOB_POINT_TO_SPAWN - cbIndex is invalid! cbIndex = ", cbIndex)
		EXIT
	ENDIF
	
	IF NETWORK_DOES_NETWORK_ID_EXIST( MC_serverBD_1.niDZSpawnVehicle[cbIndex] )
	AND IS_VEHICLE_DRIVEABLE( NET_TO_VEH( MC_serverBD_1.niDZSpawnVehicle[cbIndex] ))
		INT i, iIndex, iTeamSlot
		VECTOR vehDimMin, vehDimMax, vTempPosition
		FLOAT vTempHeading, fXOffsetAmount
		
		BOOL bOverCapacity = (slotInTeam >= 8)
		
		//Correct team slot for chosen CB
		iTeamSlot 	= PICK_INT( (slotInTeam > ciNUM_PLAYERS_PER_AIRCRAFT_SPAWN-1), 	slotInTeam - ciNUM_PLAYERS_PER_AIRCRAFT_SPAWN, 	slotInTeam )
		g_f_CargoX	= PICK_FLOAT( (iTeamSlot % 2 = 0), 0, 1 )
		iIndex 		= cbIndex
		
		g_SpawnData.bCargobobUseBehaviour = TRUE
		fXOffsetAmount = 0.45
				
		GET_MODEL_DIMENSIONS( CARGOBOB4, vehDimMin, vehDimMax )
		
		
		IF bOverCapacity
		OR failsafeOverride			
			//Assign 8 to 11 to the first aircraft, 12+ in the second
			iIndex = PICK_INT( (slotInTeam < 12), iPlayerTeam, iPlayerTeam + ciNUM_PLAYERS_PER_AIRCRAFT_SPAWN )
			
			// If we're over 8 (bOverCapacity) then we need to do this to get it in range of vCargobobSpawnCoord
			// We need to make this in range.
			FOR i = 0 TO 3
				IF iTeamSlot >= 4
					iTeamSlot -= ciNUM_PLAYERS_PER_AIRCRAFT_SPAWN
				ENDIF
			ENDFOR
			
			//Move them slightly more to the center than the original players spawning in the same CB
			fXOffsetAmount = -0.3
		ENDIF
		
		
		IF IS_ENTITY_ALIVE( NET_TO_VEH(MC_serverBD_1.niDZSpawnVehicle[iIndex]) )
			
			//Get all spawn points for the cargobob
			FOR i = 0 TO 3
				vTempHeading 	= GET_ENTITY_HEADING( NET_TO_VEH(MC_serverBD_1.niDZSpawnVehicle[iIndex]) ) + 180
				vTempPosition 	= GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS( 
							 	  	NET_TO_VEH(MC_serverBD_1.niDZSpawnVehicle[iIndex]),
							 		<< PICK_FLOAT( 	(i % 2 = 0), fXOffsetAmount, -fXOffsetAmount ),
							 		   (vehDimMin.y * 0.30) + (cfDISTANCE_BETWEEN_PLAYERS * i),
									   vehDimMin.z + 0.6909 >> ) //Exact positioning inside of the cargobob hold
					
				g_SpawnData.vCargobobSpawnCoord[i] = vTempPosition
				g_SpawnData.fCargobobSpawnHeading[i] = vTempHeading
			ENDFOR
			
			//Backup coord right at the back of the CB
			g_SpawnData.vCargobobBackupCoord = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS( 
										 	  	NET_TO_VEH(MC_serverBD_1.niDZSpawnVehicle[iIndex]),
										 		<< 0,
										 		   (vehDimMin.y * 0.30) + (cfDISTANCE_BETWEEN_PLAYERS * 5),
												   vehDimMin.z + 0.6909 >> ) //Exact positioning inside of the cargobob hold
			
			positionOut = g_SpawnData.vCargobobSpawnCoord[iTeamSlot]
			headingOut = g_SpawnData.fCargobobSpawnHeading[iTeamSlot]

			vDZPlayerSpawnLoc = g_SpawnData.vCargobobSpawnCoord[iTeamSlot]
			fDZPlayerVehHeading = g_SpawnData.fCargobobSpawnHeading[iTeamSlot]
			iDZVehicleIndex = iIndex
			
			IF fDZPlayerVehHeading > 360
				fDZPlayerVehHeading = fDZPlayerVehHeading - 360
			ELIF fDZPlayerVehHeading < -360
				fDZPlayerVehHeading = fDZPlayerVehHeading + 360
			ENDIF
			
		ENDIF
		
		#IF IS_DEBUG_BUILD
		
			PRINTLN("=*=*=*=*=*=*===========[JJT] DROP ZONE CARGOBOB CONFIG PRINTOUT ==============*=*=*=*=*=*=")
			PRINTLN("=*=*=*=*=*=*==================================================================*=*=*=*=*=*=")
			PRINTLN("=*=*=*=*=*=*==================================================================*=*=*=*=*=*=")
			PRINTLN("******")
			PRINTLN("=*=*=*=*=*=*================= Team cargobob spawn points: ")
			FOR i = 0 TO 3
				PRINTLN("=*=*=*=*=*=*=========== Slot ", i+1,": ", g_SpawnData.vCargobobSpawnCoord[i], " ", g_SpawnData.fCargobobSpawnHeading[i])
			ENDFOR
			PRINTLN("=*=*=*=*=*=*=========== vCargobobBackupCoord: ", g_SpawnData.vCargobobBackupCoord)
			PRINTLN("=*=*=*=*=*=*======================= Variables: ")
			PRINTLN("=*=*=*=*=*=*=========== vehDimMin        = ", vehDimMin )
			PRINTLN("=*=*=*=*=*=*=========== vehDimMax        = ", vehDimMax )
			PRINTLN("=*=*=*=*=*=*=========== iIndex           = ", iIndex)
			PRINTLN("=*=*=*=*=*=*=========== iTeamSlot        = ", iTeamSlot)
			PRINTLN("=*=*=*=*=*=*=========== fXOffsetAmount   = ", fXOffsetAmount)
			PRINTLN("=*=*=*=*=*=*=========== bOverCapacity    = ", bOverCapacity)
			PRINTLN("=*=*=*=*=*=*=========== failsafeOverride = ", failsafeOverride)
			PRINTLN("=*=*=*=*=*=*=========== g_f_CargoX       = ", g_f_CargoX)
			PRINTLN("******")
			PRINTLN("=*=*=*=*=*=*==================================================================*=*=*=*=*=*=")
			PRINTLN("=*=*=*=*=*=*==================================================================*=*=*=*=*=*=")
		#ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
			IF NOT NETWORK_DOES_NETWORK_ID_EXIST( MC_serverBD_1.niDZSpawnVehicle[cbIndex] )
				PRINTLN("[JJT] private_GET_CARGOBOB_POINT_TO_SPAWN - Cargobob doesn't exist. Index = ", cbIndex)
			ENDIF
			IF NOT IS_VEHICLE_DRIVEABLE( NET_TO_VEH( MC_serverBD_1.niDZSpawnVehicle[cbIndex] ))
				PRINTLN("[JJT] private_GET_CARGOBOB_POINT_TO_SPAWN - Cargobob isn't driveable. Index = ", cbIndex)
			ENDIF
			SCRIPT_ASSERT("Calling to spawn in a cargobob but the vehicle doesn't exist or is undrivable!")
		#ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    For use with Team Air Spawn Points, this will spawn players in the back
///    of a flying cargobob that gets spawned with the air spawn points
FUNC BOOL HAS_SET_CARGOBOB_SPAWN_POINT(INT iPlayerTeam)
	
	PRINTLN("[JJT] Calling HAS_SET_CARGOBOB_SPAWN_POINT with iPlayerTeam = ", iPlayerTeam)
	
	#IF IS_DEBUG_BUILD
	DEBUG_PRINTCALLSTACK()
	#ENDIF
	
	//Double check that the spawn point is an aerial point
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iPlayerTeam][0].iSpawnBitSet, ci_SpawnBS_AirSpawn)
		PRINTLN("[JJT] Team spawn point for team ", iPlayerTeam, " IS NOT an aerial spawn point.")
		RETURN TRUE
	ELSE
		PRINTLN("[JJT] Team spawn point for team ", iPlayerTeam, " IS an aerial spawn point.")
	ENDIF

	CLEAR_SPECIFIC_SPAWN_LOCATION()
	
	IF iPlayerTeam < FMMC_MAX_TEAMS
		IF NETWORK_DOES_NETWORK_ID_EXIST( MC_serverBD_1.niDZSpawnVehicle[private_GET_CARGOBOB_INDEX_TO_SPAWN_IN(iPlayerTeam)] ) 
	        PRINTLN("[JJT] MC_serverBD_1.niDZSpawnVehicle[", private_GET_CARGOBOB_INDEX_TO_SPAWN_IN(iPlayerTeam), "] does exist.")
			
			INT iSlotInTeam = GET_PARTICIPANT_NUMBER_IN_TEAM() 
			PRINTLN("[JJT] HAS_SET_CARGOBOB_SPAWN_POINT - Player slot = ", iSlotInTeam)
			
			VECTOR vCustomSpawnPoint
			FLOAT fCustomHeading
			
			PRINTLN("[JJT] HAS_SET_CARGOBOB_SPAWN_POINT - Calling private_GET_CARGOBOB_POINT_TO_SPAWN")
			private_GET_CARGOBOB_POINT_TO_SPAWN(vCustomSpawnPoint, fCustomHeading, iPlayerTeam, iSlotInTeam)
		 	
	       	IF NOT IS_VECTOR_ZERO(vCustomSpawnPoint)
				PRINTLN("[JJT] HAS_SET_CARGOBOB_SPAWN_POINT - Success! Found point to spawn (",
						VECTOR_TO_STRING(vCustomSpawnPoint), ", ", fCustomHeading, ").")

				PRINTLN("[JJT] HAS_SET_CARGOBOB_SPAWN_POINT - Completed in ", iTestCargobobExistsCount, " call(s).")
				
				
				#IF IS_DEBUG_BUILD
				IF IS_FAKE_MULTIPLAYER_MODE_SET(TRUE)
					SETUP_SPECIFIC_SPAWN_LOCATION(vCustomSpawnPoint, fCustomHeading, 5,FALSE,0,TRUE,FALSE,0)
					SET_PLAYER_NEXT_RESPAWN_LOCATION(SPAWN_LOCATION_AT_SPECIFIC_COORDS,TRUE)
				ENDIF
				#ENDIF

				SET_ENTITY_COORDS(LocalPlayerPed, vCustomSpawnPoint)
				SET_ENTITY_HEADING(LocalPlayerPed, fCustomHeading)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0) 
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)

				//Set this as a hack to skip conventional intro spawning logic
				//lets us place the exact position of the ped rather than go through Neil's spawning stuff
				SET_BIT(iLocalBoolCheck8,LBOOL8_HAS_START_POSITION_WARPED)

				RETURN TRUE
	   		ELSE
				PRINTLN("[JJT] HAS_SET_CARGOBOB_SPAWN_POINT - Using backup spawn because team spawn point is vector zero!")
			ENDIF
		ELSE
			PRINTLN("[JJT] Trying to spawn into an aerial spawn point but the vehicle has not been loaded! :( -- ", private_GET_CARGOBOB_INDEX_TO_SPAWN_IN(iPlayerTeam))

//==========FIRST CARGOBOB DOESN'T EXIST, SPAWN INTO FIRST TEAM CB =======================================================================================================
			
			IF iPlayerTeam > -1 AND iPlayerTeam < 4
				IF NETWORK_DOES_NETWORK_ID_EXIST( MC_serverBD_1.niDZSpawnVehicle[iPlayerTeam] ) 
					PRINTLN("[JJT] -Backup- MC_serverBD_1.niDZSpawnVehicle[",iPlayerTeam, "] does exist!")
					
					INT iSlotInTeam = GET_PARTICIPANT_NUMBER_IN_TEAM() 
					PRINTLN("[JJT] -Backup- HAS_SET_CARGOBOB_SPAWN_POINT - Player slot = ", iSlotInTeam)
					
					VECTOR vCustomSpawnPoint
					FLOAT fCustomHeading
					
					PRINTLN("[JJT] -Backup- HAS_SET_CARGOBOB_SPAWN_POINT - Calling private_GET_CARGOBOB_POINT_TO_SPAWN with overcapacity set to true")
					private_GET_CARGOBOB_POINT_TO_SPAWN(vCustomSpawnPoint, fCustomHeading, iPlayerTeam, iSlotInTeam, TRUE)
				 	
			       	IF NOT IS_VECTOR_ZERO(vCustomSpawnPoint)
						PRINTLN("[JJT] -Backup- HAS_SET_CARGOBOB_SPAWN_POINT - Success! Found point to spawn (",
								VECTOR_TO_STRING(vCustomSpawnPoint), ", ", fCustomHeading, ").")

						PRINTLN("[JJT] -Backup- HAS_SET_CARGOBOB_SPAWN_POINT - Completed in ", iTestCargobobExistsCount, " call(s).")

						SET_ENTITY_COORDS(LocalPlayerPed, vCustomSpawnPoint)
						SET_ENTITY_HEADING(LocalPlayerPed, fCustomHeading)
						SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0) 
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)

						//Set this as a hack to skip conventional intro spawning logic
						//lets us place the exact position of the ped rather than go through Neil's spawning stuff
						SET_BIT(iLocalBoolCheck8,LBOOL8_HAS_START_POSITION_WARPED)

						RETURN TRUE
			   		ELSE
						PRINTLN("[JJT] -Backup- HAS_SET_CARGOBOB_SPAWN_POINT - Using backup spawn because team spawn point is vector zero!")
					ENDIF
							
					
				ELSE
					PRINTLN("[JJT] -Backup- First vehicle didn't exist, nor does the first team vehicle! >:( -- ", iPlayerTeam)
				ENDIF
			ELSE
				PRINTLN("[JJT] -Backup- HAS_SET_CARGOBOB_SPAWN_POINT - iPlayerTeam = ", iPlayerTeam, "!!!")
			ENDIF
			
//========================================================================================================================================================================
			
		ENDIF
	ELSE
		PRINTLN("[JJT] HAS_SET_CARGOBOB_SPAWN_POINT - Called with iPlayerTeam more than FMMC_MAX_TEAMS")
	ENDIF
	
	IF iTestCargobobExistsCount > ciCARGOBOB_RETRY_TEST_LIMIT	
	
		PRINTLN("[JJT] HAS_SET_CARGOBOB_SPAWN_POINT - Using fallback spawn location. This should not be getting hit.")
		SETUP_SPECIFIC_SPAWN_LOCATION(	g_FMMC_STRUCT.sFMMCEndConditions[iPlayerTeam].vStartPos, 
										g_FMMC_STRUCT.sFMMCEndConditions[iPlayerTeam].fStartHeading	 )
		RETURN TRUE
	ENDIF
	
	iTestCargobobExistsCount++
	RETURN FALSE
	
ENDFUNC

FUNC VECTOR private_GET_CARGOBOB_RAMP_POSITION(ENTITY_INDEX veh)
	VECTOR vehDimMin, vehDimMax
	GET_MODEL_DIMENSIONS( CARGOBOB4, vehDimMin, vehDimMax )
	RETURN GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(veh,<< 0,vehDimMin.y * 0.45,vehDimMin.z + 1.5 >>)
ENDFUNC

PROC LOWER_CARGOBOB_RAMP_FOR_INTRO()
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[0][0].iSpawnBitSet, ci_SpawnBS_AirSpawn)
		EXIT
	ENDIF
	
	INT i
	FOR i = 0 TO 7
		IF NOT IS_BIT_SET(iLocalBoolCheck15, LBOOL15_HAS_LOWERED_CB_RAMP_1 + i)
			IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.niDZSpawnVehicle[i])
				IF NETWORK_HAS_CONTROL_OF_ENTITY(NET_TO_VEH(MC_serverBD_1.niDZSpawnVehicle[i]))
					IF IS_VEHICLE_DRIVEABLE(NET_TO_VEH(MC_serverBD_1.niDZSpawnVehicle[i]))
						SET_VEHICLE_DOOR_CONTROL(NET_TO_VEH(MC_serverBD_1.niDZSpawnVehicle[i]), SC_DOOR_REAR_LEFT, DT_DOOR_SWINGING_FREE, 1)
						SET_BIT(iLocalBoolCheck15, LBOOL15_HAS_LOWERED_CB_RAMP_1 + i)
						PRINTLN("[JJT] LOWER_CARGOBOB_RAMP_FOR_INTRO - Setting the cargobob ", i, " ramp to lower.")
					ELSE
						PRINTLN("[JJT] LOWER_CARGOBOB_RAMP_FOR_INTRO_BACKUP - Cargobob ", i, " not drivable!!!")
					ENDIF
				#IF IS_DEBUG_BUILD
				ELSE
					PRINTLN("We don't have control of cargobob ", i)
				#ENDIF
				ENDIF
			#IF IS_DEBUG_BUILD
			ELSE
				PRINTLN("cargobob ", i, " does not exist")
			#ENDIF
			ENDIF
		#IF IS_DEBUG_BUILD
		ELSE
			PRINTLN("LBOOL15_HAS_LOWERED_CB_RAMP_1 + ",i," is set")
		#ENDIF
		ENDIF
	ENDFOR
	
	IF NOT IS_BIT_SET(iLocalBoolCheck14, LBOOL14_DZ_STARTED_CARGOBOB_SOUNDS)
		SET_BIT(iLocalBoolCheck14, LBOOL14_DZ_STARTED_CARGOBOB_SOUNDS)
		
		STRING iSoundSet = "DLC_Apartments_Drop_Zone_Sounds"
		
		IF iDZVehicleIndex > -1 AND iDZVehicleIndex < 8
		
			IF iDZCargobobRampSoundID < 0
				iDZCargobobRampSoundID = GET_SOUND_ID()
			ENDIF
			
			PRINTLN("[JJT] LOWER_CARGOBOB_RAMP_FOR_INTRO - Starting Cargobob_Door_Open")
			PLAY_SOUND_FROM_COORD(iDZCargobobRampSoundID, 
								  "Cargobob_Door_Open", 
								  private_GET_CARGOBOB_RAMP_POSITION(NET_TO_VEH(MC_serverBD_1.niDZSpawnVehicle[iDZVehicleIndex])), 
								  iSoundSet)
								  
			SET_VARIABLE_ON_SOUND(iDZCargobobRampSoundID, "Time", 2.8)
		ELSE
			iDZCargobobRampSoundID = GET_SOUND_ID()
			PRINTLN("[JJT] LOWER_CARGOBOB_RAMP_FOR_INTRO - Error: iDZVehicleIndex = ", iDZVehicleIndex, ". Starting Cargobob_Door_Open on player.")
			PLAY_SOUND_FROM_COORD(iDZCargobobRampSoundID, "Cargobob_Door_Open", GET_ENTITY_COORDS(LocalPlayerPed), iSoundSet)		  
			SET_VARIABLE_ON_SOUND(iDZCargobobRampSoundID, "Time", 2.8)
		ENDIF
		
		//Scene to lower the noise of the CB rotors
		sCargoBobAudioScene = "DLC_Apartments_Drop_Zone_Helicopter_Scene"
				
		IF NOT IS_AUDIO_SCENE_ACTIVE(sCargoBobAudioScene)
			PRINTLN("[JJT] LOWER_CARGOBOB_RAMP_FOR_INTRO - Starting scene (\"", sCargoBobAudioScene, "\")")
			START_AUDIO_SCENE(sCargoBobAudioScene) 
		ENDIF
	ENDIF
	
	//Stop the lower ramp sound and play the heli beep loop when the door is open
	IF iDZVehicleIndex > -1 AND iDZVehicleIndex < 8
		IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.niDZSpawnVehicle[iDZVehicleIndex])
			IF IS_VEHICLE_DRIVEABLE(NET_TO_VEH(MC_serverBD_1.niDZSpawnVehicle[iDZVehicleIndex]))
				
				#IF IS_DEBUG_BUILD
					PRINTLN("Cargobob ", iDZVehicleIndex, " ramp door angle is ", GET_VEHICLE_DOOR_ANGLE_RATIO(NET_TO_VEH(MC_serverBD_1.niDZSpawnVehicle[iDZVehicleIndex]), SC_DOOR_REAR_LEFT ))
				#ENDIF
				
				IF GET_VEHICLE_DOOR_ANGLE_RATIO(NET_TO_VEH(MC_serverBD_1.niDZSpawnVehicle[iDZVehicleIndex]), SC_DOOR_REAR_LEFT ) >= 0.96
					IF iDZCargobobRampSoundID != -1
						PRINTLN("[JJT] LOWER_CARGOBOB_RAMP_FOR_INTRO - Stopping ramp sound.")
						STOP_SOUND(iDZCargobobRampSoundID)
						iDZCargobobRampSoundID = -1
					ENDIF					
				
					IF SoundIDCargoBob = -1
						SoundIDCargoBob = GET_SOUND_ID()
					ENDIF
					IF iStartCargoBobSound = -1
						STRING sSoundSet 
						sSoundSet = "DLC_Apartments_Drop_Zone_Sounds"
						PLAY_SOUND_FROM_COORD(SoundIDCargoBob, 
											  "Helicopter_Loop", 
											  private_GET_CARGOBOB_RAMP_POSITION(NET_TO_VEH(MC_serverBD_1.niDZSpawnVehicle[iDZVehicleIndex])) + <<0,0,0.5>>, 
											  sSoundSet)	
						
						PRINTLN("[JJT] LOWER_CARGOBOB_RAMP_FOR_INTRO - Starting ramp down (loop) sound.")
						iStartCargoBobSound = 1
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//Runs as the game starts
PROC LOWER_CARGOBOB_RAMP_FOR_INTRO_BACKUP()
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[0][0].iSpawnBitSet, ci_SpawnBS_AirSpawn)
		PRINTLN("[JJT] LOWER_CARGOBOB_RAMP_FOR_INTRO_BACKUP - Mode doesn't have Cargobob4 spawns.")
		EXIT
	ENDIF
	
	PRINTLN("LOWER_CARGOBOB_RAMP_FOR_INTRO_BACKUP - running")

	INT i
	FOR i = 0 TO 7
		IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.niDZSpawnVehicle[i])
			IF NETWORK_HAS_CONTROL_OF_ENTITY(NET_TO_VEH(MC_serverBD_1.niDZSpawnVehicle[i]))
				IF IS_VEHICLE_DRIVEABLE(NET_TO_VEH(MC_serverBD_1.niDZSpawnVehicle[i]))
					#IF IS_DEBUG_BUILD
						PRINTLN("Cargobob ", i, " angle = ", GET_VEHICLE_DOOR_ANGLE_RATIO(NET_TO_VEH(MC_serverBD_1.niDZSpawnVehicle[i]), SC_DOOR_REAR_LEFT ))
					#ENDIF
					
					IF GET_VEHICLE_DOOR_ANGLE_RATIO(NET_TO_VEH(MC_serverBD_1.niDZSpawnVehicle[i]), SC_DOOR_REAR_LEFT ) < 0.85
						SET_VEHICLE_DOOR_CONTROL(NET_TO_VEH(MC_serverBD_1.niDZSpawnVehicle[i]), SC_DOOR_REAR_LEFT, DT_DOOR_SWINGING_FREE, 1)
						
						#IF IS_DEBUG_BUILD
							PRINTLN("[JJT] LOWER_CARGOBOB_RAMP_FOR_INTRO_BACKUP - Performed emergency ramp lowering on veh ", i)
							PRINTLN("[JJT] LOWER_CARGOBOB_RAMP_FOR_INTRO_BACKUP - Ramp angle was ",
									GET_VEHICLE_DOOR_ANGLE_RATIO(NET_TO_VEH(MC_serverBD_1.niDZSpawnVehicle[i]), SC_DOOR_REAR_LEFT ))
						#ENDIF
					ENDIF
				ELSE
					PRINTLN("[JJT] LOWER_CARGOBOB_RAMP_FOR_INTRO_BACKUP - Cargobob ", i, " not drivable!!!")
				ENDIF
			#IF IS_DEBUG_BUILD
			ELSE
				PRINTLN("Don't have control of cargobob ", i)
			#ENDIF
			ENDIF
		#IF IS_DEBUG_BUILD
		ELSE
			PRINTLN("cargobob ", i , " does not exist.")
		#ENDIF
		ENDIF
	ENDFOR
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Special Case - Yacht
// ##### Description: Functions and Processes that are geared toward the startup processes for the Yacht.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC DIFFERENTIATE_YACHT_FROM_PLAYER_YACHT(INT iYachtIndex)

	PRINTLN("[MYACHT] DIFFERENTIATE_YACHT_FROM_PLAYER_YACHT being called on yacht ", iYachtIndex)
	
	IF sMissionYachtData[iYachtIndex].Appearance.iTint = sMissionYachtData[ciYACHT_LOBBY_HOST_YACHT_INDEX].Appearance.iTint
		IF sMissionYachtData[iYachtIndex].Appearance.iTint = ciYACHT_COLOUR__Command
			sMissionYachtData[iYachtIndex].Appearance.iTint = ciYACHT_COLOUR__Intrepid
			sMissionYachtData[iYachtIndex].Appearance.iLighting = ciYACHT_LIGHTING__PresidentialGold
		ELSE
			sMissionYachtData[iYachtIndex].Appearance.iTint = ciYACHT_COLOUR__Command
			sMissionYachtData[iYachtIndex].Appearance.iLighting = ciYACHT_LIGHTING__PresidentialRose
		ENDIF
		
		PRINTLN("[MYACHT] DIFFERENTIATE_YACHT_FROM_PLAYER_YACHT | Changing tint & lighting")
	ENDIF
ENDPROC

PROC FILL_FMMC_YACHT_DATA_STRUCT_FROM_FMMC_GLOBALS(INT iYachtIndex)
	sMissionYachtData[iYachtIndex].Appearance.iOption = g_FMMC_STRUCT.sMissionYachtInfo[iYachtIndex].iMYacht_ModelIndex
	sMissionYachtData[iYachtIndex].Appearance.iTint = g_FMMC_STRUCT.sMissionYachtInfo[iYachtIndex].iMYacht_ColourIndex
	sMissionYachtData[iYachtIndex].Appearance.iLighting = g_FMMC_STRUCT.sMissionYachtInfo[iYachtIndex].iMYacht_LightingIndex
	sMissionYachtData[iYachtIndex].Appearance.iRailing = g_FMMC_STRUCT.sMissionYachtInfo[iYachtIndex].iMYacht_FittingsIndex
	sMissionYachtData[iYachtIndex].Appearance.iFlag = g_FMMC_STRUCT.sMissionYachtInfo[iYachtIndex].iMYacht_FlagIndex
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sMissionYachtInfo[iYachtIndex].iMYacht_Bitset, ciYACHT_BITSET__DIFFERENTIATE_FROM_OWNED_YACHT)
		DIFFERENTIATE_YACHT_FROM_PLAYER_YACHT(iYachtIndex)
	ENDIF
ENDPROC

PROC FILL_FMMC_YACHT_DATA_STRUCT_FROM_OTHER_PLAYER(INT iYachtIndex, INT iOtherPlayerYachtIndex)
	sMissionYachtData[iYachtIndex].Appearance.iOption = GET_OPTION_OF_PRIVATE_YACHT(iOtherPlayerYachtIndex)
	sMissionYachtData[iYachtIndex].Appearance.iTint = GET_TINT_OF_PRIVATE_YACHT(iOtherPlayerYachtIndex)
	sMissionYachtData[iYachtIndex].Appearance.iLighting = GET_LIGHTING_OF_PRIVATE_YACHT_X(iOtherPlayerYachtIndex)
	sMissionYachtData[iYachtIndex].Appearance.iRailing = GET_RAILING_OF_PRIVATE_YACHT(iOtherPlayerYachtIndex)
	sMissionYachtData[iYachtIndex].Appearance.iFlag = GET_FLAG_OF_PRIVATE_YACHT(iOtherPlayerYachtIndex)
ENDPROC

FUNC BOOL SHOULD_HIDE_MY_PERSONAL_YACHT(INT iYachtIndex)

	IF IS_BIT_SET(g_FMMC_STRUCT.sMissionYachtInfo[iYachtIndex].iMYacht_Bitset, ciYACHT_BITSET__MATCH_LOBBY_HOST_YACHT)
		IF AM_I_FMMC_LOBBY_LEADER()
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL GET_FMMC_YACHT_DATA_FROM_LOBBY_HOST()

	IF GET_FMMC_LOBBY_LEADER() = INVALID_PLAYER_INDEX()
		PRINTLN("[MYACHT][HostOwnedYacht] GET_FMMC_YACHT_DATA_FROM_LOBBY_HOST | Waiting for lobby host!")
		RETURN FALSE
	ENDIF

	IF NOT IS_FMMC_LOBBY_HOST_YACHT_IN_CORRECT_LOCATION(GET_FMMC_LOBBY_LEADER())
		PRINTLN("[MYACHT][HostOwnedYacht] GET_FMMC_YACHT_DATA_FROM_LOBBY_HOST | Waiting for Freemode to move the lobby host's yacht where we want it for the mission || Currently at ", GET_PRIVATE_YACHT_ASSIGNED_TO_PLAYER(GET_FMMC_LOBBY_LEADER()), ", need it to move to ", GET_FMMC_YACHT_ID(ciYACHT_LOBBY_HOST_YACHT_INDEX))
		RETURN FALSE
	ENDIF

	//Get the lobby host's yacht and use that
	INT iFreemodeYachtToUse = GET_PRIVATE_YACHT_ASSIGNED_TO_PLAYER(GET_FMMC_LOBBY_LEADER())
	PRINTLN("[MYACHT][HostOwnedYacht] iFreemodeYachtToUse is ", iFreemodeYachtToUse)
	
	IF iFreemodeYachtToUse > -1
		FILL_FMMC_YACHT_DATA_STRUCT_FROM_OTHER_PLAYER(ciYACHT_LOBBY_HOST_YACHT_INDEX, iFreemodeYachtToUse)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_FREEMODE_YACHT_INIT_STATE(INT iYachtIndex)
	IF GET_FMMC_YACHT_DATA_FROM_LOBBY_HOST()
		SET_FMMC_YACHT_STATE(iYachtIndex, FMMC_YACHT_SPAWN_STATE__SPAWNING_YACHT)
	ENDIF
ENDPROC

PROC PROCESS_FMMC_YACHT__INIT_STATE(INT iYachtIndex)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sMissionYachtInfo[ciYACHT_LOBBY_HOST_YACHT_INDEX].iMYacht_Bitset, ciYACHT_BITSET__MATCH_LOBBY_HOST_YACHT)
		// Call this if the yacht data needs to be initialised. If the lobby host is bringing their yacht in, the data will have already been initialised
		INIT_PRIVATE_YACHT_DATA()
	ENDIF
	
	PRINTLN("[MYACHT] Initialising Yacht | Creator Settings: ", iYachtIndex," ===================================================================================================")
	PRINTLN("iMYacht_ModelIndex: ", g_FMMC_STRUCT.sMissionYachtInfo[iYachtIndex].iMYacht_ModelIndex)
	PRINTLN("iMYacht_ColourIndex: ", g_FMMC_STRUCT.sMissionYachtInfo[iYachtIndex].iMYacht_ColourIndex)
	PRINTLN("iMYacht_LightingIndex: ", g_FMMC_STRUCT.sMissionYachtInfo[iYachtIndex].iMYacht_LightingIndex)
	PRINTLN("iMYacht_FittingsIndex: ", g_FMMC_STRUCT.sMissionYachtInfo[iYachtIndex].iMYacht_FittingsIndex)
	PRINTLN("iMYacht_FlagIndex: ", g_FMMC_STRUCT.sMissionYachtInfo[iYachtIndex].iMYacht_FlagIndex)
	PRINTLN("[MYACHT] ============================================================================================================================================")
	
	FILL_FMMC_YACHT_DATA_STRUCT_FROM_FMMC_GLOBALS(iYachtIndex)
	SET_FMMC_YACHT_STATE(iYachtIndex, FMMC_YACHT_SPAWN_STATE__SPAWNING_YACHT)
	
	IF sMissionYachtVars[iYachtIndex].eMissionYachtSpawnState = FMMC_YACHT_SPAWN_STATE__SPAWNING_YACHT
		PRINTLN("[MYACHT] Initialising Yacht | Initialised Data: ", iYachtIndex," ===================================================================================================")
		PRINTLN("Appearance.iOption: ", sMissionYachtData[iYachtIndex].Appearance.iOption)
		PRINTLN("Appearance.iTint: ", sMissionYachtData[iYachtIndex].Appearance.iTint)
		PRINTLN("Appearance.iLighting: ", sMissionYachtData[iYachtIndex].Appearance.iLighting)
		PRINTLN("Appearance.iRailing: ", sMissionYachtData[iYachtIndex].Appearance.iRailing)
		PRINTLN("iMYacht_FlagAppearance.iFlagIndex: ", sMissionYachtData[iYachtIndex].Appearance.iFlag)
		PRINTLN("[MYACHT] ============================================================================================================================================")
	ENDIF
ENDPROC

PROC PROCESS_FREEMODE_YACHT__SPAWNING_YACHT_STATE(INT iYachtIndex)

	REQUEST_MODEL(GET_YACHT_COLLISION_MODEL())
	
	IF IS_PRIVATE_YACHT_FULLY_LOADED(GET_PRIVATE_YACHT_ASSIGNED_TO_PLAYER(GET_FMMC_LOBBY_LEADER()))
		SET_BIT(g_TransitionSessionNonResetVars.iFMMC_IPL_Bitset, ciFMMC_IPL_FMMCYachts)
		SET_FMMC_YACHT_STATE(iYachtIndex, FMMC_YACHT_SPAWN_STATE__SPAWNING_EXTRA_VEHICLES)
	ELSE
		PRINTLN("[MYACHT][MissionYacht ", iYachtIndex, "] PROCESS_FMMC_YACHT - Waiting for IS_PRIVATE_YACHT_FULLY_LOADED to return TRUE for Yacht ", GET_PRIVATE_YACHT_ASSIGNED_TO_PLAYER(GET_FMMC_LOBBY_LEADER()))
	ENDIF
ENDPROC

PROC PROCESS_FMMC_YACHT__SPAWNING_YACHT_STATE(INT iYachtIndex)

	REQUEST_ASSETS_FOR_MISSION_CREATOR_YACHT(sMissionYachtData[iYachtIndex])
	PRINTLN("[MYACHT][MissionYacht ", iYachtIndex, "] PROCESS_FMMC_YACHT - Requesting Yacht assets")
	
	IF HAVE_ASSETS_LOADED_FOR_MISSION_CREATOR_YACHT(sMissionYachtData[iYachtIndex])
		PRINTLN("[MYACHT][MissionYacht ", iYachtIndex, "] PROCESS_FMMC_YACHT - Creating Yacht ", iYachtIndex)
    	CREATE_YACHT_FOR_MISSION_CREATOR(sMissionYachtData[iYachtIndex])
		SET_BIT(g_TransitionSessionNonResetVars.iFMMC_IPL_Bitset, ciFMMC_IPL_FMMCYachts)
		
		SET_FMMC_YACHT_STATE(iYachtIndex, FMMC_YACHT_SPAWN_STATE__SPAWNING_EXTRA_VEHICLES)
	ENDIF
ENDPROC

PROC PROCESS_FREEMODE_YACHT__SPAWNING_EXTRA_VEHICLES_STATE(INT iYachtIndex)
	
	BOOL bWaitingForExtraVeh
	
	IF AM_I_FMMC_LOBBY_LEADER()
		IF SHOULD_FMMC_YACHT_EXTRA_VEHICLE_BE_SPAWNED(iYachtIndex, GET_PRIVATE_YACHT_SPAWN_VEHICLE_MODEL(0))
		AND NOT IS_PRIVATE_YACHT_VEHICLE_FLAG_SET(0, PYVF_DONT_SPAWN)
			IF NOT HAS_MY_PRIVATE_YACHT_VEHCILE_BEEN_CREATED(0)
				bWaitingForExtraVeh = TRUE
				PRINTLN("[MYACHT][HostOwnedYacht] PROCESS_FMMC_YACHT | Waiting for HAS_MY_PRIVATE_YACHT_VEHCILE_BEEN_CREATED for ", FMMC_GET_MODEL_NAME_FOR_DEBUG(GET_PRIVATE_YACHT_SPAWN_VEHICLE_MODEL(0)))
				PRINTLN("[MYACHT][HostOwnedYacht] PROCESS_FMMC_YACHT | gPrivateYachtSpawnVehicleDetails[0].iFlags: ", gPrivateYachtSpawnVehicleDetails[0].iFlags)
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT bWaitingForExtraVeh
		SET_FMMC_YACHT_STATE(iYachtIndex, FMMC_YACHT_SPAWN_STATE__COMPLETE)
	ENDIF
ENDPROC


PROC SET_UP_YACHT_EXTRA_VEHICLE(VEHICLE_INDEX viNewVeh, INT iYachtIndex, INT iExtraVehicle)
	
	PRINTLN("[MYACHT] SET_UP_YACHT_EXTRA_VEHICLE | Applying colours from GET_PRIVATE_YACHT_VEHICLE_CAR_COLOURS:")
	APPLY_YACHT_COLOURS_TO_VEHICLE(viNewVeh, iYachtIndex, iExtraVehicle)
	
	// Anchor
	IF IS_PRIVATE_YACHT_VEHICLE_FLAG_SET(iExtraVehicle, PYVF_SET_ANCHOR)
		IF CAN_ANCHOR_BOAT_HERE(viNewVeh)
			SET_BOAT_ANCHOR(viNewVeh, TRUE)	
			PRINTLN("[MYACHT] SET_UP_YACHT_EXTRA_VEHICLE | Applying anchor")
		ELSE
			PRINTLN("[MYACHT] SET_UP_YACHT_EXTRA_VEHICLE | Can't apply the anchor")
		ENDIF
	ENDIF		
ENDPROC

PROC PROCESS_FMMC_YACHT__SPAWNING_EXTRA_VEHICLES_STATE(INT iYachtIndex)
	
	BOOL bWaitingForExtraVehicles
	
	IF bIsLocalPlayerHost
	
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.sMissionYachtInfo[ciYACHT_LOBBY_HOST_YACHT_INDEX].iMYacht_Bitset, ciYACHT_BITSET__MATCH_LOBBY_HOST_YACHT)
			// Call this if the yacht data needs to be initialised. If the lobby host is bringing their yacht in, the data will have already been initialised
			INIT_YACHT_VEHICLE_FLAGS(sMissionYachtData[iYachtIndex].Appearance.iOption)
		ENDIF
		
		INT iExtraVeh = 0
		FOR iExtraVeh = 0 TO NUMBER_OF_PRIVATE_YACHT_VEHICLES - 1
			IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.niYachtVehicles[iYachtIndex][iExtraVeh])
				RELOOP	
			ELSE
				IF IS_MODEL_VALID(GET_PRIVATE_YACHT_SPAWN_VEHICLE_MODEL(iExtraVeh))
				AND SHOULD_FMMC_YACHT_EXTRA_VEHICLE_BE_SPAWNED(iYachtIndex, GET_PRIVATE_YACHT_SPAWN_VEHICLE_MODEL(iExtraVeh))
					bWaitingForExtraVehicles = TRUE
					
					VECTOR vVehicleCoords = <<0,0,0>>
					FLOAT fVehicleHeading = 0.0
					GET_PRIVATE_YACHT_SPAWN_VEHICLE_LOCATION(iExtraVeh, sMissionYachtData[iYachtIndex].iLocation, vVehicleCoords, fVehicleHeading, IS_PRIVATE_YACHT_VEHICLE_FLAG_SET(iExtraVeh, PYVF_SPAWN_ON_LAND))
					
					REQUEST_MODEL(GET_PRIVATE_YACHT_SPAWN_VEHICLE_MODEL(iExtraVeh))
					IF HAS_MODEL_LOADED(GET_PRIVATE_YACHT_SPAWN_VEHICLE_MODEL(iExtraVeh))						
						IF FMMC_CREATE_NET_VEHICLE(MC_serverBD_1.niYachtVehicles[iYachtIndex][iExtraVeh], GET_PRIVATE_YACHT_SPAWN_VEHICLE_MODEL(iExtraVeh), vVehicleCoords, fVehicleHeading, DEFAULT,
									DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE) // Force Creation During Init
							VEHICLE_INDEX viNewVeh = NET_TO_VEH(MC_serverBD_1.niYachtVehicles[iYachtIndex][iExtraVeh])
							SET_UP_YACHT_EXTRA_VEHICLE(viNewVeh, iYachtIndex, iExtraVeh)
						
							PRINTLN("[MYACHT] PROCESS_FMMC_YACHT__SPAWNING_EXTRA_VEHICLES_STATE | Spawned in extra vehicle ", iExtraVeh, " / ", FMMC_GET_MODEL_NAME_FOR_DEBUG(GET_PRIVATE_YACHT_SPAWN_VEHICLE_MODEL(iExtraVeh)))
							SET_MODEL_AS_NO_LONGER_NEEDED(GET_PRIVATE_YACHT_SPAWN_VEHICLE_MODEL(iExtraVeh))
						ELSE
							PRINTLN("[MYACHT] PROCESS_FMMC_YACHT__SPAWNING_EXTRA_VEHICLES_STATE | Failed to create extra vehicle ", iExtraVeh, " / ", FMMC_GET_MODEL_NAME_FOR_DEBUG(GET_PRIVATE_YACHT_SPAWN_VEHICLE_MODEL(iExtraVeh)))
						ENDIF
					ELSE
						PRINTLN("[MYACHT] PROCESS_FMMC_YACHT__SPAWNING_EXTRA_VEHICLES_STATE | Loading model for extra veh ", iExtraVeh, " / ", FMMC_GET_MODEL_NAME_FOR_DEBUG(GET_PRIVATE_YACHT_SPAWN_VEHICLE_MODEL(iExtraVeh)))
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	
	IF NOT bWaitingForExtraVehicles
		SET_FMMC_YACHT_STATE(iYachtIndex, FMMC_YACHT_SPAWN_STATE__COMPLETE)
	ENDIF
ENDPROC

/// PURPOSE: Returns the render target for yacht models 
FUNC STRING GET_YACHT_RENDER_TARGET(MODEL_NAMES mModel)
	SWITCH mModel
		CASE APA_PROP_AP_PORT_TEXT   RETURN "port_text"   BREAK
		CASE APA_PROP_AP_STARB_TEXT  RETURN "starb_text"  BREAK
		CASE APA_PROP_AP_STERN_TEXT  RETURN "stern_text"  BREAK
	ENDSWITCH
	RETURN ""
ENDFUNC

/// PURPOSE: Registers and links a render target to a specified model
PROC REGISTER_AND_LINK_RENDER_TARGET(INT &iRenderTargetID, STRING sRenderTarget, MODEL_NAMES mModel)
	IF NOT IS_NAMED_RENDERTARGET_REGISTERED(sRenderTarget)
		REGISTER_NAMED_RENDERTARGET(sRenderTarget)
		IF NOT IS_NAMED_RENDERTARGET_LINKED(mModel)
			LINK_NAMED_RENDERTARGET(mModel)
			IF iRenderTargetID = -1
				iRenderTargetID = GET_NAMED_RENDERTARGET_RENDER_ID(sRenderTarget)
			ELSE
				// .. Already have render target ID
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Display chosen name on yacht (COPIED FROM MAINTAIN_YACHT_NAME in am_mp_yacht.sc)
PROC MAINTAIN_FMMC_YACHT_NAME(INT iYachtIndex, FMMC_YACHT_VARS& sYachtVars)
	
	IF IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sMissionYachtInfo[iYachtIndex].tlYachtName)
		EXIT
	ENDIF
	
	INT iCurrentYacht = sMissionYachtData[iYachtIndex].iLocation
	TEXT_LABEL_63 sNameToUse = g_FMMC_STRUCT.sMissionYachtInfo[iYachtIndex].tlYachtName
	
	MP_PROP_OFFSET_STRUCT sPropOffset
	
	IF NOT g_bMissionEnding
	AND GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(LocalPlayer), mpYachts[iCurrentYacht].vBaseLocation) <= 150
		SWITCH sYachtVars.iYachtNameControl
			CASE 0
				// Prop creation & render targets
				IF CAN_REGISTER_MISSION_OBJECTS(3)
					IF NOT DOES_ENTITY_EXIST(sYachtVars.oYachtNamePort)
						REQUEST_MODEL(APA_PROP_AP_PORT_TEXT)
						IF HAS_MODEL_LOADED(APA_PROP_AP_PORT_TEXT)
							GET_POSITION_AS_OFFSET_FOR_YACHT(iCurrentYacht, MP_PROP_ELEMENT_YACHT_NAME_PORT, sPropOffset)
							sYachtVars.oYachtNamePort = CREATE_OBJECT_NO_OFFSET(APA_PROP_AP_PORT_TEXT, sPropOffset.vLoc, FALSE, FALSE, TRUE)
							SET_ENTITY_ROTATION(sYachtVars.oYachtNamePort, sPropOffset.vRot)
							REGISTER_AND_LINK_RENDER_TARGET(sYachtVars.iPortRenderID, GET_YACHT_RENDER_TARGET(APA_PROP_AP_PORT_TEXT), APA_PROP_AP_PORT_TEXT)
							SET_MODEL_AS_NO_LONGER_NEEDED(APA_PROP_AP_PORT_TEXT)
						ENDIF
					ENDIF
					IF NOT DOES_ENTITY_EXIST(sYachtVars.oYachtNameStarb)
						REQUEST_MODEL(APA_PROP_AP_STARB_TEXT)
						IF HAS_MODEL_LOADED(APA_PROP_AP_STARB_TEXT)
							GET_POSITION_AS_OFFSET_FOR_YACHT(iCurrentYacht, MP_PROP_ELEMENT_YACHT_NAME_STARB, sPropOffset)
							sYachtVars.oYachtNameStarb = CREATE_OBJECT_NO_OFFSET(APA_PROP_AP_STARB_TEXT, sPropOffset.vLoc, FALSE, FALSE, TRUE)
							SET_ENTITY_ROTATION(sYachtVars.oYachtNameStarb, sPropOffset.vRot)
							REGISTER_AND_LINK_RENDER_TARGET(sYachtVars.iStarbRenderID, GET_YACHT_RENDER_TARGET(APA_PROP_AP_STARB_TEXT), APA_PROP_AP_STARB_TEXT)
							SET_MODEL_AS_NO_LONGER_NEEDED(APA_PROP_AP_STARB_TEXT)
						ENDIF
					ENDIF
					IF NOT DOES_ENTITY_EXIST(sYachtVars.oYachtNameStern)
						REQUEST_MODEL(APA_PROP_AP_STERN_TEXT)
						IF HAS_MODEL_LOADED(APA_PROP_AP_STERN_TEXT)
							GET_POSITION_AS_OFFSET_FOR_YACHT(iCurrentYacht, MP_PROP_ELEMENT_YACHT_NAME_STERN, sPropOffset)
							sYachtVars.oYachtNameStern = CREATE_OBJECT_NO_OFFSET(APA_PROP_AP_STERN_TEXT, sPropOffset.vLoc, FALSE, FALSE, TRUE)
							SET_ENTITY_ROTATION(sYachtVars.oYachtNameStern, sPropOffset.vRot)
							REGISTER_AND_LINK_RENDER_TARGET(sYachtVars.iSternRenderID, GET_YACHT_RENDER_TARGET(APA_PROP_AP_STERN_TEXT), APA_PROP_AP_STERN_TEXT)
							SET_MODEL_AS_NO_LONGER_NEEDED(APA_PROP_AP_STERN_TEXT)
						ENDIF
					ENDIF
					IF DOES_ENTITY_EXIST(sYachtVars.oYachtNamePort)
					AND DOES_ENTITY_EXIST(sYachtVars.oYachtNameStarb)
					AND DOES_ENTITY_EXIST(sYachtVars.oYachtNameStern)
						SET_YACHT_NAME_CONTROL(sYachtVars, 1)
					ENDIF
				ENDIF
			BREAK
			CASE 1
				// White Text?
				sYachtVars.iTintID = sMissionYachtData[iYachtIndex].Appearance.iTint
				sYachtVars.bYachtWhiteText = FALSE
				
				IF GET_TINT_OF_PRIVATE_YACHT(iCurrentYacht) = 1
				OR GET_TINT_OF_PRIVATE_YACHT(iCurrentYacht) = 4
				OR GET_TINT_OF_PRIVATE_YACHT(iCurrentYacht) = 5
				OR GET_TINT_OF_PRIVATE_YACHT(iCurrentYacht) = 6
				OR GET_TINT_OF_PRIVATE_YACHT(iCurrentYacht) = 9
				OR GET_TINT_OF_PRIVATE_YACHT(iCurrentYacht) = 11
				OR GET_TINT_OF_PRIVATE_YACHT(iCurrentYacht) = 15
					sYachtVars.bYachtWhiteText = TRUE
				ENDIF
				
				// Port & Starb Scaleform
				IF HAS_SCALEFORM_MOVIE_LOADED(sYachtVars.sYachtNameOverlay)
					BEGIN_SCALEFORM_MOVIE_METHOD(sYachtVars.sYachtNameOverlay, "SET_YACHT_NAME")

						SCALEFORM_MOVIE_METHOD_ADD_PARAM_LITERAL_STRING(sNameToUse)
						sYachtVars.iHashYachtName = GET_HASH_KEY(sNameToUse)

						// isWhiteText param
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(sYachtVars.bYachtWhiteText)
						// Country
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")
					END_SCALEFORM_MOVIE_METHOD()
					SET_SCALEFORM_MOVIE_TO_USE_LARGE_RT(sYachtVars.sYachtNameOverlay, TRUE)
					SET_YACHT_NAME_CONTROL(sYachtVars, 2)
				ELSE
					sYachtVars.sYachtNameOverlay = REQUEST_SCALEFORM_MOVIE("YACHT_NAME")
				ENDIF
			BREAK
			CASE 2
				// Stern Scaleform
				IF HAS_SCALEFORM_MOVIE_LOADED(sYachtVars.sYachtNameSternOverlay)
					BEGIN_SCALEFORM_MOVIE_METHOD(sYachtVars.sYachtNameSternOverlay, "SET_YACHT_NAME")

						SCALEFORM_MOVIE_METHOD_ADD_PARAM_LITERAL_STRING(sNameToUse)
						sYachtVars.iHashYachtName = GET_HASH_KEY(sNameToUse)

						// isWhiteText param
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(sYachtVars.bYachtWhiteText)
						
						// Country
						sYachtVars.iFlagID = sMissionYachtData[iYachtIndex].Appearance.iFlag
						sYachtVars.tlFlagCountry = "FLAG_CNTRY_"
						sYachtVars.tlFlagCountry += sYachtVars.iFlagID
						
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sYachtVars.tlFlagCountry)
					END_SCALEFORM_MOVIE_METHOD()
					SET_SCALEFORM_MOVIE_TO_USE_LARGE_RT(sYachtVars.sYachtNameSternOverlay, TRUE)
					SET_YACHT_NAME_CONTROL(sYachtVars, 3)
				ELSE
					sYachtVars.sYachtNameSternOverlay = REQUEST_SCALEFORM_MOVIE("YACHT_NAME_STERN")
				ENDIF
			BREAK
			CASE 3
				// Display name
				SET_TEXT_RENDER_ID(sYachtVars.iPortRenderID)
				SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
				SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
				DRAW_SCALEFORM_MOVIE(sYachtVars.sYachtNameOverlay, cfPortCentreX, cfPortCentreY, cfPortWidth, cfPortHeight, 255, 255, 255, 255)
				
				SET_TEXT_RENDER_ID(sYachtVars.iStarbRenderID)
				SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
				SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
				DRAW_SCALEFORM_MOVIE(sYachtVars.sYachtNameOverlay, cfStarbCentreX, cfStarbCentreY, cfStarbWidth, cfStarbHeight, 255, 255, 255, 255)
				
				SET_TEXT_RENDER_ID(sYachtVars.iSternRenderID)
				SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
				SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
				DRAW_SCALEFORM_MOVIE(sYachtVars.sYachtNameSternOverlay, cfSternCentreX, cfSternCentreY, cfSternWidth, cfSternHeight, 255, 255, 255, 255)
				
				SET_TEXT_RENDER_ID(GET_DEFAULT_SCRIPT_RENDERTARGET_RENDER_ID())

				IF sYachtVars.iHashYachtName != GET_HASH_KEY(sNameToUse)
					PRINTLN("[MYACHT] MAINTAIN_FMMC_YACHT_NAME | Moving name control back to 1 due to sYachtVars.iHashYachtName ", sYachtVars.iHashYachtName, " not matching GET_HASH_KEY(sNameToUse) ", GET_HASH_KEY(sNameToUse))
					SET_YACHT_NAME_CONTROL(sYachtVars, 1)
				ENDIF

				IF sYachtVars.iFlagID != sMissionYachtData[iYachtIndex].Appearance.iFlag
					PRINTLN("[MYACHT] MAINTAIN_FMMC_YACHT_NAME | Moving name control back to 1 due to sYachtVars.iFlagID ", sYachtVars.iFlagID, " not matching sMissionYachtData[iYachtIndex].Appearance.iFlag ", sMissionYachtData[iYachtIndex].Appearance.iFlag)
					SET_YACHT_NAME_CONTROL(sYachtVars, 1)
				ENDIF
				
				IF sYachtVars.iTintID != sMissionYachtData[iYachtIndex].Appearance.iTint
					PRINTLN("[MYACHT] MAINTAIN_FMMC_YACHT_NAME | Moving name control back to 1 due to sYachtVars.iTintID ", sYachtVars.iTintID, " not matching sMissionYachtData[iYachtIndex].Appearance.iTint ", sMissionYachtData[iYachtIndex].Appearance.iTint)
					SET_YACHT_NAME_CONTROL(sYachtVars, 1)
				ENDIF
			BREAK
		ENDSWITCH
	ELSE
		CLEANUP_FMMC_YACHT_NAME(sYachtVars)
	ENDIF
ENDPROC

PROC PROCESS_FMMC_YACHT__COMPLETE(INT iYachtIndex)
	IF IS_BIT_SET(MC_serverBD.iServerBitSet7, SBBOOL7_YACHT_MOVED_TO_DESTINATION)
		sMissionYachtData[iYachtIndex].iLocation = GET_FMMC_YACHT_DESTINATION_ID()
		g_SpawnData.iYachtToWarpTo = sMissionYachtData[iYachtIndex].iLocation
		PRINTLN("[MYACHT][MissionYacht ", iYachtIndex, "] PROCESS_FMMC_YACHT - Using post cutscene location")
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sMissionYachtInfo[iYachtIndex].iMYacht_Bitset, ciYACHT_BITSET__MATCH_LOBBY_HOST_YACHT)
		MAINTAIN_FMMC_YACHT_NAME(iYachtIndex, sMissionYachtVars[iYachtIndex])
	ENDIF
	
	UPDATE_YACHT_FOR_MISSION_CREATOR(sMissionYachtData[iYachtIndex])
ENDPROC

PROC PROCESS_FMMC_YACHT(INT iYachtIndex)
	
	sMissionYachtData[iYachtIndex].iLocation = GET_FMMC_YACHT_ID(iYachtIndex)
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sMissionYachtInfo[iYachtIndex].iMYacht_Bitset, ciYACHT_BITSET__MATCH_LOBBY_HOST_YACHT)
	AND NOT IS_FAKE_MULTIPLAYER_MODE_SET()
		
		SWITCH sMissionYachtVars[iYachtIndex].eMissionYachtSpawnState
			CASE FMMC_YACHT_SPAWN_STATE__INIT
				PROCESS_FREEMODE_YACHT_INIT_STATE(iYachtIndex)
			BREAK
			
			CASE FMMC_YACHT_SPAWN_STATE__SPAWNING_YACHT
				PROCESS_FREEMODE_YACHT__SPAWNING_YACHT_STATE(iYachtIndex)
			BREAK
			
			CASE FMMC_YACHT_SPAWN_STATE__SPAWNING_EXTRA_VEHICLES
				PROCESS_FREEMODE_YACHT__SPAWNING_EXTRA_VEHICLES_STATE(iYachtIndex)
			BREAK
		ENDSWITCH
		
		IF NOT IS_PRIVATE_YACHT_ACTIVE(GET_FMMC_YACHT_ID(iYachtIndex))
			PRINTLN("[HostOwnedYacht] Making yacht position ", GET_FMMC_YACHT_ID(iYachtIndex), " active ready for the host's yacht to be created there")
			SET_PRIVATE_YACHT_ACTIVE(GET_FMMC_YACHT_ID(iYachtIndex), TRUE)
		ENDIF

	ELSE
		SWITCH sMissionYachtVars[iYachtIndex].eMissionYachtSpawnState
			CASE FMMC_YACHT_SPAWN_STATE__INIT
				PROCESS_FMMC_YACHT__INIT_STATE(iYachtIndex)
			BREAK
			
			CASE FMMC_YACHT_SPAWN_STATE__SPAWNING_YACHT
				PROCESS_FMMC_YACHT__SPAWNING_YACHT_STATE(iYachtIndex)
			BREAK
			
			CASE FMMC_YACHT_SPAWN_STATE__SPAWNING_EXTRA_VEHICLES
				PROCESS_FMMC_YACHT__SPAWNING_EXTRA_VEHICLES_STATE(iYachtIndex)
			BREAK
			
			CASE FMMC_YACHT_SPAWN_STATE__COMPLETE
				PROCESS_FMMC_YACHT__COMPLETE(iYachtIndex)
			BREAK
		ENDSWITCH
	ENDIF
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Special Case - Arena
// ##### Description: Functions and Processes that are geared toward the startup sequence for the Arena Interior.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL DOES_ARENA_SET_UP_NEED_EXTRA_TIME()
	IF IS_LONG_BIT_SET(g_FMMC_STRUCT.sArenaInfo.iArena_MiscBS, ARENA_FLRM_BRIDGE_C) //Used Externally IS_LONG_BIT_SET
	OR IS_LONG_BIT_SET(g_FMMC_STRUCT.sArenaInfo.iArena_MiscBS, ARENA_FLRM_BRIDGE_D) //Used Externally IS_LONG_BIT_SET
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SET_ARENA_SPAWN_FIX_COORDS(VECTOR vStartPos, FLOAT fStartHeading)
	IF g_sTransitionSessionData.sMissionRoundData.iNumberOfRoundsPlayed > 0
	AND CONTENT_IS_USING_ARENA()
		vArenaSpawnVec = vStartPos
		fArenaSpawnHead = fStartHeading
		PRINTLN("[ARENA_SPAWN_FIX] SET_ARENA_SPAWN_FIX_COORDS - Storing start position: ", vArenaSpawnVec, " Heading: ", fArenaSpawnHead)
	ENDIF
ENDPROC

FUNC BOOL ARENA_SPAWN_FIX()
	IF g_sTransitionSessionData.sMissionRoundData.iNumberOfRoundsPlayed > 0
	AND CONTENT_IS_USING_ARENA()
	AND NOT IS_VECTOR_ZERO(vArenaSpawnVec)
		IF DOES_ENTITY_EXIST(LocalPlayerPed)
			VECTOR vPlayerPos = GET_ENTITY_COORDS(LocalPlayerPed)
			IF NOT ARE_VECTORS_ALMOST_EQUAL(vArenaSpawnVec, vPlayerPos, 3) AND (GET_FRAME_COUNT() % 20 = 0)
			OR NOT IS_BIT_SET(iLocalBoolCheck29, LBOOL29_ARENA_SPAWN_FIX_HAS_BEEN_DONE)
				VECTOR vNewCoord = vArenaSpawnVec
				PRINTLN("ARENA_SPAWN_FIX - Warping from: ", vPlayerPos, " to ", vArenaSpawnVec)
				IF NET_WARP_TO_COORD(vNewCoord , fArenaSpawnHead, TRUE, FALSE, FALSE, FALSE, TRUE, TRUE, TRUE, TRUE)
					VEHICLE_INDEX viPlayerVeh
					IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
						viPlayerVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
						IF DOES_ENTITY_EXIST(viPlayerVeh)
							IF SET_VEHICLE_ON_GROUND_PROPERLY(viPlayerVeh, 5)
								vPlayerPos = GET_ENTITY_COORDS(viPlayerVeh)
								PRINTLN("ARENA_SPAWN_FIX - Set play on ground 1. Player vehicle position: ", vPlayerPos)
							ENDIF
						ENDIF
					ENDIF
					vPlayerPos = GET_ENTITY_COORDS(LocalPlayerPed)
					PRINTLN("ARENA_SPAWN_FIX - Warping to position. vPlayerPos: ", vPlayerPos)
					SET_BIT(iLocalBoolCheck29, LBOOL29_ARENA_SPAWN_FIX_HAS_BEEN_DONE)
					RETURN TRUE
				ENDIF
			ELSE
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE	
		VECTOR vdebug = GET_ENTITY_COORDS(LocalPlayerPed)
		PRINTLN("ARENA_SPAWN_FIX - ", vdebug, " vs ", vArenaSpawnVec, " Rounds: ", g_sTransitionSessionData.sMissionRoundData.iNumberOfRoundsPlayed, " using Arena: ", CONTENT_IS_USING_ARENA())
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SET_ARENA_SPAWN_FIX_STAGE(eArenaSpawnFixStage eNewStage)
	PRINTLN("SET_ARENA_SPAWN_FIX_STAGE - New stage = ", ENUM_TO_INT(eNewStage))
	MC_playerBD_1[iLocalPart].eArenaSpawnStage = eNewStage
ENDPROC

FUNC BOOL PROCESS_ARENA_SPAWN_FIX()
	BOOL bReturn
	VECTOR vTemp
	VEHICLE_INDEX viPlayerVeh
	IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		viPlayerVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
	ENDIF
	
	BOOL bDontWarp = IS_LOCAL_PLAYER_SPAWNING_IN_ARENA_BOX_SEAT() OR IS_PLAYER_IN_ARENA_BOX_SEAT()
	
	IF g_sTransitionSessionData.sMissionRoundData.iNumberOfRoundsPlayed > 0
	AND CONTENT_IS_USING_ARENA()
	AND NOT IS_VECTOR_ZERO(vArenaSpawnVec)
		SWITCH MC_playerBD_1[iLocalPart].eArenaSpawnStage
			CASE eArenaSpawnFix_Idle	
				IF (((GET_PLAYER_SWITCH_TYPE() != SWITCH_TYPE_SHORT
				AND GET_PLAYER_SWITCH_STATE() = SWITCH_STATE_JUMPCUT_DESCENT
				AND GET_SKYSWOOP_STAGE() != SKYSWOOP_NONE)))
				OR IS_SKYSWOOP_AT_GROUND()
					SET_ARENA_SPAWN_FIX_STAGE(eArenaSpawnFix_FadeOut)
				ENDIF
			BREAK
			CASE eArenaSpawnFix_FadeOut
				PRINTLN("PROCESS_ARENA_SPAWN_FIX - In eArenaSpawnFix_FadeOut")
				IF NOT IS_SCREEN_FADING_OUT()
					DO_SCREEN_FADE_OUT(2500)
				ENDIF
				IF IS_SCREEN_FADED_OUT()
					SET_ARENA_SPAWN_FIX_STAGE(eArenaSpawnFix_Warp)
				ENDIF
			BREAK
			CASE eArenaSpawnFix_Warp
				PRINTLN("PROCESS_ARENA_SPAWN_FIX - In eArenaSpawnFix_Warp")
				IF bDontWarp
					PRINTLN("PROCESS_ARENA_SPAWN_FIX - Not doing the warp.")
					SET_ARENA_SPAWN_FIX_STAGE(eArenaSpawnFix_Wait)
				ELSE
					IF ARENA_SPAWN_FIX()
						IF DOES_ENTITY_EXIST(viPlayerVeh)
							FREEZE_ENTITY_POSITION(viPlayerVeh, TRUE)
						ENDIF
						SET_ARENA_SPAWN_FIX_STAGE(eArenaSpawnFix_Wait)
					ENDIF
				ENDIF
			BREAK
			CASE eArenaSpawnFix_Wait
				PRINTLN("PROCESS_ARENA_SPAWN_FIX - In eArenaSpawnFix_Wait")
				IF IS_SKYSWOOP_AT_GROUND()
				AND IS_INTERIOR_READY(g_ArenaInterior)
					IF NOT HAS_NET_TIMER_STARTED(tdArenaSpawnFixTimer)
						REINIT_NET_TIMER(tdArenaSpawnFixTimer)
						REINIT_NET_TIMER(tdArenaSpawnFixSafety)
						PRINTLN("PROCESS_ARENA_SPAWN_FIX - Starting wait timer(s)")
					ELSE
						IF DOES_ENTITY_EXIST(viPlayerVeh)
						AND NOT bDontWarp
							IF HAS_NET_TIMER_EXPIRED(tdArenaSpawnFixTimer, 5500)
								IF (SET_VEHICLE_ON_GROUND_PROPERLY(viPlayerVeh, 5)
								AND HAS_NET_TIMER_EXPIRED(tdArenaSpawnFixTimer, 6500))
									PRINTLN("PROCESS_ARENA_SPAWN_FIX - placed on ground properly first time.")
									SET_ARENA_SPAWN_FIX_STAGE(eArenaSpawnFix_FadeIn)
								ELIF HAS_NET_TIMER_EXPIRED(tdArenaSpawnFixSafety, 7000)
									PRINTLN("PROCESS_ARENA_SPAWN_FIX - Safety timer stage 1 expired.")
									vtemp = GET_ENTITY_COORDS(viPlayerVeh)
									IF GET_GROUND_Z_FOR_3D_COORD(vTemp, vTemp.z)
										vTemp.z += 1
										IF vTemp.z > 100.0
											PRINTLN("PROCESS_ARENA_SPAWN_FIX - Ground Z pos: ", vTemp)
											SET_ENTITY_COORDS(viPlayerVeh, vTemp)
										ELSE
											vtemp = vArenaSpawnVec - <<0, 0, 0.5>>
											SET_ENTITY_COORDS(viPlayerVeh, vtemp)
											PRINTLN("PROCESS_ARENA_SPAWN_FIX - Moving player to stored position -1z: ", vtemp)
										ENDIF
									ENDIF
									IF SET_VEHICLE_ON_GROUND_PROPERLY(viPlayerVeh, 7)
										PRINTLN("PROCESS_ARENA_SPAWN_FIX - Safety timer on ground properly.")
										SET_ARENA_SPAWN_FIX_STAGE(eArenaSpawnFix_FadeIn)
									ELSE
										IF HAS_NET_TIMER_EXPIRED(tdArenaSpawnFixSafety, 7500)
											PRINTLN("PROCESS_ARENA_SPAWN_FIX - Safety timer stage 2 expired.")
											SET_ARENA_SPAWN_FIX_STAGE(eArenaSpawnFix_FadeIn)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ELSE
							IF HAS_NET_TIMER_EXPIRED(tdArenaSpawnFixTimer, 2000)
								SET_ARENA_SPAWN_FIX_STAGE(eArenaSpawnFix_Sync)
							ENDIF
						ENDIF
					ENDIF
				ELSE
					bReturn = FALSE
					PRINTLN("PROCESS_ARENA_SPAWN_FIX - Camera not at ground or interior not ready yet.")
				ENDIF
			BREAK
			CASE eArenaSpawnFix_Sync
				INT iParticipantsReady, i
				PRINTLN("[PLAYER_LOOP] - PROCESS_ARENA_SPAWN_FIX")
				FOR i = 0 TO (MAX_NUM_MC_PLAYERS - 1)
					IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
					AND NOT IS_PARTICIPANT_A_SPECTATOR(i)
						IF MC_playerBD_1[i].eArenaSpawnStage >= eArenaSpawnFix_Sync
							iParticipantsReady++
						ELSE
							PRINTLN("PROCESS_ARENA_SPAWN_FIX - participant ", i," is not in sync state. They are in state: ", MC_playerBD_1[i].eArenaSpawnStage)
						ENDIF
					ENDIF
				ENDFOR
				IF GET_NUMBER_OF_ACTIVE_PARTICIPANTS() = iParticipantsReady
					SET_ARENA_SPAWN_FIX_STAGE(eArenaSpawnFix_FadeIn)
					REINIT_NET_TIMER(tdArenaSpawnFadeInTimer)
				ENDIF
			BREAK
			CASE eArenaSpawnFix_FadeIn
				bReturn = TRUE
				IF HAS_NET_TIMER_EXPIRED(tdArenaSpawnFadeInTimer, 1500) //to stop a camera pop on intro cam.
				AND (jobIntroData.jobIntroStage = JOB_CUT_TIME_TRAIL OR bDontWarp)
					PRINTLN("PROCESS_ARENA_SPAWN_FIX - In eArenaSpawnFix_FadeIn")
					IF NOT IS_SCREEN_FADING_IN()
						DO_SCREEN_FADE_IN(750)
						IF DOES_ENTITY_EXIST(viPlayerVeh)
						AND NOT IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()
							SET_VEHICLE_FORWARD_SPEED(viPlayerVeh, 0.1)
						ENDIF
						PRINTLN("PROCESS_ARENA_SPAWN_FIX - SET_VEHICLE_FORWARD_SPEED(viPlayerVeh, 0.1) called to fix wheels")
					ENDIF
					IF IS_SCREEN_FADED_IN()
						SET_ARENA_SPAWN_FIX_STAGE(eArenaSpawnFix_Progress)
					ENDIF
				ENDIF
			BREAK
			CASE eArenaSpawnFix_Progress
				bReturn = TRUE
			BREAK
		ENDSWITCH
	ELSE
		IF g_sTransitionSessionData.sMissionRoundData.iNumberOfRoundsPlayed = 0
			PRINTLN("PROCESS_ARENA_SPAWN_FIX - Not doing arena spawn fix. Is the first round.")
		ENDIF
		IF NOT CONTENT_IS_USING_ARENA()
			PRINTLN("PROCESS_ARENA_SPAWN_FIX - Not doing arena spawn fix. Arena not in use.")
		ENDIF
		IF IS_VECTOR_ZERO(vArenaSpawnVec)
			PRINTLN("PROCESS_ARENA_SPAWN_FIX - Not doing arena spawn fix. vArenaSpawnVec is zero.")
		ENDIF
		bReturn = TRUE
	ENDIF
		
	RETURN bReturn
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Entity Creation
// ##### Description: Contains Wrappers and the main process functions for loading and creating the entities
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC INITIALIZE_SPAWN_GROUP_DEPENDENT_SERVER_DATA()
	INT iTeam
	
	MC_PROCESS_REPOSITION_ENTITY_BASED_ON_SUB_SPAWN_GROUP_SETTINGS(g_FMMC_STRUCT.sWarpLocationSettings_EndCutscene, CREATION_TYPE_END_CUTSCENE, 0, g_FMMC_STRUCT.iEndCutsceneSpawnGroup, g_FMMC_STRUCT.iEndCutsceneSpawnSubGroupBS, NULL)
		
	FOR iTeam = 0 TO FMMC_MAX_TEAMS-1 // server num not init yet.
		MC_ASSIGN_PMC_BASED_ON_FIRST_ACTIVE_SUB_SPAWN_GROUP(g_FMMC_STRUCT.iPostMissionSceneId_OverrideWithSpawnGroup[iTeam], iTeam)
	ENDFOR
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Restart Logic
// ##### Description: Main Functions involved in starting up the mission and used in the main state machine. Where the player spawns is called here, as well as skyswoop etc.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC SET_START_POSITION_WARPING_VEHICLE_ON_GROUND_PROPERLY(BOOL bSkycamDown = FALSE)
	
	IF IS_THIS_A_QUICK_RESTART_JOB()
	AND NOT bSkycamDown
		EXIT
	ENDIF
	
	IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		
		PRINTLN("[InitPosition] - SET_START_POSITION_WARPING_VEHICLE_ON_GROUND_PROPERLY - In a vehicle.")
				
		VEHICLE_INDEX veh = GET_VEHICLE_PED_IS_IN(localPlayerPed)
		
		IF DOES_ENTITY_EXIST(veh)
		AND NETWORK_HAS_CONTROL_OF_ENTITY(veh)
			IF IS_VEHICLE_DRIVEABLE(veh)
			AND NOT IS_PED_IN_ANY_HELI(localPlayerPed)
			AND NOT IS_PED_IN_ANY_PLANE(localPlayerPed)
				SET_VEHICLE_ON_GROUND_PROPERLY(veh)
				FREEZE_ENTITY_POSITION(veh, FALSE)
				ACTIVATE_PHYSICS(veh)
				PRINTLN("[InitPosition] - SET_START_POSITION_WARPING_VEHICLE_ON_GROUND_PROPERLY - Calling SET_VEHICLE_ON_GROUND_PROPERLY")
			ELSE
				PRINTLN("[InitPosition] - SET_START_POSITION_WARPING_VEHICLE_ON_GROUND_PROPERLY - Plane/Heli Do not call.")
			ENDIF
		ELSE
			PRINTLN("[InitPosition] - SET_START_POSITION_WARPING_VEHICLE_ON_GROUND_PROPERLY - We do not have net control.")
		ENDIF
	ENDIF		
ENDPROC

PROC PROCESS_HOLD_UP_LOGIC_BETWEEN_ROUNDS()
	IF IS_EVERYONE_READY_FOR_GAMEPLAY() = FALSE
		PRINTLN("[RCC MISSION][PROCESS_CLIENT][ROUNDCAM][PROCESS_HOLD_UP_LOGIC_BETWEEN_ROUNDS] g_b_HoldupCutsceneBetweenRounds = TRUE GAME_STATE_TEAM_PLAY IS_EVERYONE_READY_FOR_GAMEPLAY() = FALSE ")	
		g_b_HoldupCutsceneBetweenRounds = TRUE
	ELSE
		PRINTLN("[RCC MISSION][PROCESS_CLIENT][ROUNDCAM][PROCESS_HOLD_UP_LOGIC_BETWEEN_ROUNDS] g_b_HoldupCutsceneBetweenRounds = FALSE GAME_STATE_TEAM_PLAY IS_EVERYONE_READY_FOR_GAMEPLAY() = TRUE ")
		g_b_HoldupCutsceneBetweenRounds = FALSE
	ENDIF
	
	IF GET_SKYSWOOP_STAGE() != SKYSWOOP_NONE
		PRINTLN("[RCC MISSION][PROCESS_CLIENT][ROUNDCAM][PROCESS_HOLD_UP_LOGIC_BETWEEN_ROUNDS] g_b_HoldupSkycamBetweenRounds = TRUE GET_SKYSWOOP_STAGE() = ", GET_SKYSWOOP_STRING(GET_SKYSWOOP_STAGE()))
		g_b_HoldupSkycamBetweenRounds = TRUE
	ELSE
		PRINTLN("[RCC MISSION][PROCESS_CLIENT][ROUNDCAM][PROCESS_HOLD_UP_LOGIC_BETWEEN_ROUNDS] g_b_HoldupSkycamBetweenRounds = FALSE GET_SKYSWOOP_STAGE() = ", GET_SKYSWOOP_STRING(GET_SKYSWOOP_STAGE()))
		g_b_HoldupSkycamBetweenRounds = FALSE
	ENDIF
ENDPROC

//Not spectator specific, also used by intro cutscenes
PROC SET_SERVER_SYNC_BETWEEN_ROUNDS()
	IF IS_THIS_A_ROUNDS_MISSION() 
	OR IS_THIS_A_QUICK_RESTART_JOB()
		IF bIsLocalPlayerHost
			RESET_NET_TIMER(MC_serverBD.iBetweenRoundsTimer) 
			DEBUG_PRINTCALLSTACK()
			PRINTLN("[RCC MISSION]  SET_SERVER_SYNC_BETWEEN_ROUNDS set to ",GET_ABS_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.iBetweenRoundsTimer),  " " ) 					
		ENDIF
	ENDIF
ENDPROC

//Not spectator specific, also used by intro cutscenes
PROC SET_SERVER_SYNC_BETWEEN_ROUNDS_START()
	IF IS_THIS_A_ROUNDS_MISSION() 
	OR IS_THIS_A_QUICK_RESTART_JOB()
		IF bIsLocalPlayerHost
			IF NOT HAS_NET_TIMER_STARTED(MC_serverBD.iBetweenRoundsTimer)
				START_NET_TIMER(MC_serverBD.iBetweenRoundsTimer) 
				DEBUG_PRINTCALLSTACK()
				PRINTLN("[RCC MISSION]  SET_SERVER_SYNC_BETWEEN_ROUNDS_START set to ",GET_ABS_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.iBetweenRoundsTimer),  " " ) 					
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//Not spectator specific, also used by intro cutscenes
FUNC BOOL HAS_ROUNDS_AND_QUICK_RESTART_FINISHED()
	DEBUG_PRINTCALLSTACK()
	PRINTLN("[RCC MISSION][ROUNDCAM] HAS_ROUNDS_AND_QUICK_RESTART_FINISHED: IS_THIS_A_ROUNDS_MISSION = ", IS_THIS_A_ROUNDS_MISSION(), " " )
	PRINTLN("[RCC MISSION][ROUNDCAM] HAS_ROUNDS_AND_QUICK_RESTART_FINISHED: IS_THIS_A_QUICK_RESTART_JOB = ", IS_THIS_A_QUICK_RESTART_JOB(), " " )

	IF IS_THIS_A_ROUNDS_MISSION()
	OR IS_THIS_A_QUICK_RESTART_JOB()	
	 
		IF GET_MC_SERVER_GAME_STATE() = GAME_STATE_END
			PRINTLN("[RCC MISSION][ROUNDCAM] HAS_ROUNDS_AND_QUICK_RESTART_FINISHED:  GET_MC_SERVER_GAME_STATE() = GAME_STATE_END RETURN TRUE ", " " )
			RETURN TRUE
		ENDIF
		
		BOOL bShowPlayer = TRUE
		IF bIsSCTV OR DID_I_JOIN_MISSION_AS_SPECTATOR() OR bIsAnySpectator
			bShowPlayer = FALSE
		ENDIF
		
		IF NOT (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciENABLE_START_IN_CUTSCENE)
		AND GET_MC_TEAM_STARTING_RULE(0) = 0)
			SET_SKYSWOOP_DOWN(DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, HAS_SERVER_CHECKED_ALL_CLIENTS_READY_FOR_INTRO(), bShowPlayer)			
		ELSE
			PRINTLN("[JS] HAS_ROUNDS_AND_QUICK_RESTART_FINISHED - intro cutscene. not forcing skycam down yet")
			RETURN TRUE
		ENDIF
		
		PRINTLN("[RCC MISSION][ROUNDCAM] HAS_ROUNDS_AND_QUICK_RESTART_FINISHED: HAS_SERVER_CHECKED_ALL_CLIENTS_READY_FOR_INTRO = ", HAS_SERVER_CHECKED_ALL_CLIENTS_READY_FOR_INTRO(), " " )
		IF IS_SKYSWOOP_MOVING()
			
			BEGIN_TEXT_COMMAND_BUSYSPINNER_ON("MP_SPINLOADING")
			END_TEXT_COMMAND_BUSYSPINNER_ON(ENUM_TO_INT(LOADING_ICON_LOADING))
			
			PRINTLN("[RCC MISSION][ROUNDCAM] HAS_ROUNDS_AND_QUICK_RESTART_FINISHED: Skycam func Started and moved on!" )
			IF bIsLocalPlayerHost
				SET_SERVER_SYNC_BETWEEN_ROUNDS()
				SET_SERVER_SYNC_BETWEEN_ROUNDS_START()
			ENDIF
			RETURN TRUE
		ELIF IS_SKYSWOOP_AT_GROUND()
			PRINTLN("[RCC MISSION][ROUNDCAM] HAS_ROUNDS_AND_QUICK_RESTART_FINISHED: Skycam func is at ground so moved on!" )
			RETURN TRUE
		ENDIF
		PRINTLN("[RCC MISSION][ROUNDCAM] HAS_ROUNDS_AND_QUICK_RESTART_FINISHED: waiting on skyswoop down to start. " )
		RETURN FALSE
	ELSE
		PRINTLN("[RCC MISSION][ROUNDCAM] HAS_ROUNDS_AND_QUICK_RESTART_FINISHED: not a rounds or a quick restart.  " )

	ENDIF
	
	RETURN TRUE
ENDFUNC

///PURPOSE: This function pulls the skycam down if we're in a rounds mission (else it'll just chill out in the sky)
///    otherwise it'll just progress
FUNC BOOL HAS_SKYCAM_BEEN_PULLED_DOWN_FOR_ROUNDS_AND_HEIST_QUICK_RESTART_SPECTATORS()
	BOOL bReturn = FALSE
	
	IF GET_MC_SERVER_GAME_STATE() = GAME_STATE_END
		PRINTLN("[RCC MISSION][ROUNDCAM] HAS_SKYCAM_BEEN_PULLED_DOWN_FOR_ROUNDS_AND_HEIST_QUICK_RESTART_SPECTATORS:  GET_MC_SERVER_GAME_STATE() = GAME_STATE_END RETURN TRUE ", " " )
		RETURN TRUE
	ENDIF
	
	IF IS_THIS_A_ROUNDS_MISSION()
	AND (iPartToUse != -1 AND NOT MC_playerBD[iPartToUse].bCelebrationScreenIsActive)
				
		SET_SKYSWOOP_DOWN(DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, FALSE, FALSE)
		
		IF IS_SKYSWOOP_MOVING()
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] [ROUNDCAM] HAS_SKYCAM_BEEN_PULLED_DOWN_FOR_ROUNDS_AND_HEIST_QUICK_RESTART_SPECTATORS() IS_THIS_A_ROUNDS_MISSION = TRUE start skycam but hold up " )
			
			BEGIN_TEXT_COMMAND_BUSYSPINNER_ON("MP_SPINLOADING")
			END_TEXT_COMMAND_BUSYSPINNER_ON(ENUM_TO_INT(LOADING_ICON_LOADING))
			
			SET_SERVER_SYNC_BETWEEN_ROUNDS()
			SET_SERVER_SYNC_BETWEEN_ROUNDS_START()
			bReturn = true
		ELIF IS_SKYSWOOP_AT_GROUND()
		
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] [ROUNDCAM] HAS_SKYCAM_BEEN_PULLED_DOWN_FOR_ROUNDS_AND_HEIST_QUICK_RESTART_SPECTATORS() IS_THIS_A_ROUNDS_MISSION = TRUE Skycam func is at ground so moved on! " )
			SET_SERVER_SYNC_BETWEEN_ROUNDS()
			SET_SERVER_SYNC_BETWEEN_ROUNDS_START()
			bReturn = true
		ELSE
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] [ROUNDCAM] HAS_SKYCAM_BEEN_PULLED_DOWN_FOR_ROUNDS_AND_HEIST_QUICK_RESTART_SPECTATORS() IS_THIS_A_ROUNDS_MISSION = TRUE waiting on skycam moving. " )
		ENDIF
		
	ELIF IS_THIS_A_ROUNDS_MISSION() AND (iPartToUse = -1 OR MC_playerBD[iPartToUse].bCelebrationScreenIsActive)
		CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] [ROUNDCAM] HAS_SKYCAM_BEEN_PULLED_DOWN_FOR_ROUNDS_AND_HEIST_QUICK_RESTART_SPECTATORS() IS_THIS_A_ROUNDS_MISSION = TRUE iPartToUse = -1, don't have a target yet, move on " )
		bReturn = true
	
	ELIF  ( IS_THIS_A_JOB_THAT_HAS_QUICK_RESTARTS() AND IS_THIS_A_QUICK_RESTART_JOB() )
	AND (iPartToUse != -1 AND NOT MC_playerBD[iPartToUse].bCelebrationScreenIsActive)
	
		IF SET_SKYSWOOP_DOWN()
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] [ROUNDCAM] HAS_SKYCAM_BEEN_PULLED_DOWN_FOR_ROUNDS_AND_HEIST_QUICK_RESTART_SPECTATORS() Is a heist quick restart or " )
			bReturn = true
		ENDIF
	ELIF NOT IS_THIS_A_ROUNDS_MISSION()
		CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] [ROUNDCAM] HAS_SKYCAM_BEEN_PULLED_DOWN_FOR_ROUNDS_AND_HEIST_QUICK_RESTART_SPECTATORS: Skycam func passed!" )
		bReturn = true
	ELSE
		CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] [ROUNDCAM] HAS_SKYCAM_BEEN_PULLED_DOWN_FOR_ROUNDS_AND_HEIST_QUICK_RESTART_SPECTATORS: Waiting for skycam!" )
	ENDIF
	
	
	RETURN bReturn
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Main "Setup" Functions
// ##### Description: Wrappers that set up a lot of the mission controller data or calls important native functions.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC SET_SCRIPT_CLOUD_CONTENT_FIXES()
	PRINTLN("[SET_SCRIPT_CLOUD_CONTENT_FIXES] Applying cloud content fixes / overrides.")
ENDPROC

PROC CHECK_EMERGENCY_CLEANUP()

	IF g_bEmergencyCleanupNeeded
		IF NOT IS_SCREEN_FADED_IN()
		AND NOT IS_SCREEN_FADING_IN()
			DO_SCREEN_FADE_IN(500)
		ENDIF
		SET_GAMEPLAY_CAM_RELATIVE_HEADING()
		ANIMPOSTFX_STOP("MP_job_load")
		CLEANUP_ALL_CORONA_FX(TRUE)
		SET_SKYFREEZE_CLEAR(TRUE)
		RENDER_SCRIPT_CAMS(FALSE,FALSE)
		DESTROY_ALL_CAMS(TRUE)
		
		NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
		
		g_bEmergencyCleanupNeeded = FALSE
		SCRIPT_ASSERT("[RCC MISSION] - BAD SCRIPT HIT EMERGENCY CLEANUP - g_bEmergencyCleanupNeeded TRUE!!! ")
	ENDIF
ENDPROC

FUNC BOOL SETUP_PLAYSTATS_CREATE_MATCH_HISTORY_ID_2()
	IF (IS_THIS_A_ROUNDS_MISSION_FOR_CORONA() OR IS_A_STRAND_MISSION_BEING_INITIALISED())
	AND g_MissionControllerserverBD_LB.iHashMAC != 0
	AND g_MissionControllerserverBD_LB.iMatchTimeID != 0
		RETURN TRUE
	ENDIF
	IF PLAYSTATS_CREATE_MATCH_HISTORY_ID_2(g_MissionControllerserverBD_LB.iHashMAC,g_MissionControllerserverBD_LB.iMatchTimeID)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC 

// Wipe the globals at the start of each brand new round
PROC WIPE_ROUNDS_TEAM_TOTALS()
	INT i
	IF IS_THIS_A_ROUNDS_MISSION()
		PRINTLN("[2167725], WIPE_ROUNDS_TEAM_TOTALS, iNumberOfRoundsPlayed = ", g_sTransitionSessionData.sMissionRoundData.iNumberOfRoundsPlayed)
		IF g_sTransitionSessionData.sMissionRoundData.iNumberOfRoundsPlayed = 0
			PRINTLN("[PLAYER_LOOP] - WIPE_ROUNDS_TEAM_TOTALS")
			REPEAT NUM_NETWORK_PLAYERS i	
				IF g_i_TeamScoreRounds[i] <> 0
					g_i_TeamScoreRounds[i] = 0					
					PRINTLN("[2167725], WIPE_ROUNDS_TEAM_TOTALS, i = ", i, " g_i_TeamScoreRounds = ", g_i_TeamScoreRounds[i])
				ENDIF
				IF g_i_PlayerTilesScoreRounds[i] <> 0
					g_i_PlayerTilesScoreRounds[i] = 0
				ENDIF
				IF g_i_PlayerTilesScoreRoundsColumn2[i] <> 0
					g_i_PlayerTilesScoreRoundsColumn2[i] = 0
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF
ENDPROC

///PURPOSE: Stores the checkpoint state for the start of the mission. 
///    		
///    	STORE_MISSION_START_CHECKPOINT() will cache the mission start checkpoint into a bit set so it will 
///    		remain the same throughout the mission.
///    
///    	During the play of the mission the checkpoint state will advance when the player hits specific 
///    		checkpoints. This causes an issue when the checkpoint is updated when the script is activating 
///    		content based on a checkpoint state.
//		e.g. when we start the mission in checkpoint 1 we want certain things to happen on checkpoint 1 and 
///    		suddenly the player hits checkpoint 2 and the checkpoint advances and no longer calls anything 
///    		related to checkpoint 1. 
///    
///    	********** Note: **********
///    	Always check the checkpoints from the top e.g. 2 before 1
///    	If bRetrySecondStartPoint is TRUE then bRetryStartPoint will always be TRUE. BAD MP LOGIC NOTE MINE.
FUNC BOOL STORE_MISSION_START_CHECKPOINT()
	
	IF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(3)
		SET_BIT(iLocalBoolCheck11, LBOOL11_RETRY_CHECKPOINT_4)
		PRINTLN("[RCC MISSION] STORE_MISSION_START_CHECKPOINT - LBOOL11_RETRY_CHECKPOINT_4")
		RETURN TRUE
	ENDIF
	
	IF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(2)
		SET_BIT(iLocalBoolCheck11, LBOOL11_RETRY_CHECKPOINT_3)
		PRINTLN("[RCC MISSION] STORE_MISSION_START_CHECKPOINT - LBOOL11_RETRY_CHECKPOINT_3")
		RETURN TRUE
	ENDIF
	
	IF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(1)
		SET_BIT(iLocalBoolCheck12, LBOOL12_RETRY_CHECKPOINT_2)
		PRINTLN("[RCC MISSION] STORE_MISSION_START_CHECKPOINT - LBOOL12_RETRY_CHECKPOINT_2")
		RETURN TRUE
	ENDIF
	
	IF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(0)
		SET_BIT(iLocalBoolCheck12, LBOOL12_RETRY_CHECKPOINT_1)
		PRINTLN("[RCC MISSION] STORE_MISSION_START_CHECKPOINT - LBOOL12_RETRY_CHECKPOINT_1")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

// PERSONAL VEHICLE SPAWN AT START OF MISSION / QUICK RESTART
FUNC BOOL IS_PERSONAL_VEHICLE_ALLOWED_TO_SPAWN_AT_MISSION_START()

	IF IS_BIT_SET(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_SPAWN_PERSONAL_VEHICLE_AT_START)
		RETURN TRUE
	ENDIF
	
	IF g_TransitionSessionNonResetVars.bHasQuickRestarted
		IF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(3)
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_DisablePersonalVehOnCheckpoint_3)
				RETURN FALSE
			ENDIF
		ELIF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(2)
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciOptionsBS17_DisablePersonalVehOnCheckpoint_2)
				RETURN FALSE
			ENDIF
		ELIF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(1)
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciDISABLE_PERSONAL_VEH_CHECKPOINT_1)
				RETURN FALSE
			ENDIF
		ELIF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(0)
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciDISABLE_PERSONAL_VEH_CHECKPOINT_0)
				RETURN FALSE
			ENDIF
		ELSE
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciDISABLE_PERSONAL_VEH_INITIAL_SPAWN)
				RETURN FALSE
			ENDIF
		ENDIF
	ELSE
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciDISABLE_PERSONAL_VEH_INITIAL_SPAWN)
			RETURN FALSE
		ENDIF
	ENDIF
			
	RETURN TRUE
ENDFUNC

PROC SETUP_TEAM_CHAT_AND_TARGETTING_RESTRICTIONS(INT iRuleOverride = -1)
	PRINTLN("[SETUP_TEAM_CHAT_AND_TARGETTING_RESTRICTIONS]")
	DEBUG_PRINTCALLSTACK()
	
	INT iTeam, iPart
	
	//Reset Settings (in case end of mission cleanup hasn't worked)
	PRINTLN("[SETUP_TEAM_CHAT_AND_TARGETTING_RESTRICTIONS] Reset Settings")
	
	FOR iTeam = 0 TO (FMMC_MAX_TEAMS - 1)
		NETWORK_OVERRIDE_TEAM_RESTRICTIONS(iTeam, FALSE)
		PRINTLN("[SETUP_TEAM_CHAT_AND_TARGETTING_RESTRICTIONS] Setting All Teams NETWORK_OVERRIDE_TEAM_RESTRICTIONS to FALSE ... Team = ", iTeam)
	ENDFOR
	
	PRINTLN("[PLAYER_LOOP] - SETUP_TEAM_CHAT_AND_TARGETTING_RESTRICTIONS")
	FOR iPart = 0 TO (MAX_NUM_MC_PLAYERS - 1)
		PARTICIPANT_INDEX tempPart = INT_TO_PARTICIPANTINDEX(iPart)
		
		IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
			PLAYER_INDEX playerIndex = NETWORK_GET_PLAYER_INDEX(tempPart)
			
			NETWORK_OVERRIDE_SEND_RESTRICTIONS(playerIndex, FALSE)
			PRINTLN("[SETUP_TEAM_CHAT_AND_TARGETTING_RESTRICTIONS] Setting All Players NETWORK_OVERRIDE_SEND_RESTRICTIONS to FALSE... Name = ", GET_PLAYER_NAME(playerIndex))
			NETWORK_OVERRIDE_RECEIVE_RESTRICTIONS(playerIndex, FALSE)
			PRINTLN("[SETUP_TEAM_CHAT_AND_TARGETTING_RESTRICTIONS] Setting All Players NETWORK_OVERRIDE_RECEIVE_RESTRICTIONS to FALSE... Name = ", GET_PLAYER_NAME(playerIndex))
		ENDIF
	ENDFOR
	
	NETWORK_OVERRIDE_SEND_RESTRICTIONS_ALL(FALSE)
	NETWORK_OVERRIDE_RECEIVE_RESTRICTIONS_ALL(FALSE)
	
	//Change Settings
	PRINTLN("[SETUP_TEAM_CHAT_AND_TARGETTING_RESTRICTIONS] Change Settings")
	
	FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
		IF DOES_TEAM_LIKE_TEAM(MC_playerBD[iLocalPart].iTeam, iTeam, DEFAULT, iRuleOverride)
			IF NETWORK_IS_GAME_IN_PROGRESS()
				SET_PED_CAN_BE_TARGETTED_BY_TEAM(LocalPlayerPed, iTeam, FALSE)
				PRINTLN("[SETUP_TEAM_CHAT_AND_TARGETTING_RESTRICTIONS] local player ped can be targeted by item: ", iTeam)
			ENDIF
		ELSE
			IF NETWORK_IS_GAME_IN_PROGRESS()
				PRINTLN("[SETUP_TEAM_CHAT_AND_TARGETTING_RESTRICTIONS] local player ped cannot be targeted by item: ", iTeam)
				SET_PED_CAN_BE_TARGETTED_BY_TEAM(LocalPlayerPed, iTeam, TRUE)
			ENDIF
		ENDIF
		
		IF DOES_TEAM_LIKE_TEAM(MC_playerBD[iLocalPart].iTeam, iTeam, DEFAULT, iRuleOverride)
		OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSet, ciUseAllTeamChat)
			#IF IS_DEBUG_BUILD
			IF DOES_TEAM_LIKE_TEAM(MC_playerBD[iLocalPart].iTeam, iTeam)
				PRINTLN("[SETUP_TEAM_CHAT_AND_TARGETTING_RESTRICTIONS] DOES_TEAM_LIKE_TEAM(", MC_playerBD[iLocalPart].iTeam, ", ", iTeam, ")")
			ENDIF
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSet, ciUseAllTeamChat)
				PRINTLN("[SETUP_TEAM_CHAT_AND_TARGETTING_RESTRICTIONS] IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSet, ciUseAllTeamChat)")
			ENDIF
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iTeamBitset, ciBS_TEAM_CROSS_TEAM_CHAT_T0 + iTeam)
				PRINTLN("[SETUP_TEAM_CHAT_AND_TARGETTING_RESTRICTIONS] IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[", MC_playerBD[iLocalPart].iTeam, "].iTeamBitset, ciBS_TEAM_CROSS_TEAM_CHAT_T0 + ", iTeam, ")")
			ENDIF
			#ENDIF
			
			NETWORK_OVERRIDE_TEAM_RESTRICTIONS(iTeam, TRUE)
			PRINTLN("[SETUP_TEAM_CHAT_AND_TARGETTING_RESTRICTIONS] NETWORK_OVERRIDE_TEAM_RESTRICTIONS - TRUE - TEAM ", iTeam)
	    ELSE
			NETWORK_OVERRIDE_TEAM_RESTRICTIONS(iTeam, FALSE)
			PRINTLN("[SETUP_TEAM_CHAT_AND_TARGETTING_RESTRICTIONS] NETWORK_OVERRIDE_TEAM_RESTRICTIONS - FALSE - TEAM ", iTeam)
	    ENDIF
	ENDFOR
	
	PRINTLN("[SETUP_TEAM_CHAT_AND_TARGETTING_RESTRICTIONS] Cross Team Chat")
	FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
		//Recieve
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitset, ciBS_TEAM_CROSS_TEAM_CHAT_T0 + MC_playerBD[iLocalPart].iTeam)
			PRINTLN("[SETUP_TEAM_CHAT_AND_TARGETTING_RESTRICTIONS] IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[", iTeam, "].iTeamBitset, ciBS_TEAM_CROSS_TEAM_CHAT_T0 + ", MC_playerBD[iLocalPart].iTeam)
			
			//Text chat
			IF DOES_TEAM_LIKE_TEAM(MC_playerBD[iLocalPart].iTeam, iTeam, DEFAULT, iRuleOverride)
			OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSet, ciUseAllTeamChat)
				NETWORK_SET_SCRIPT_CONTROLLING_TEAMS(TRUE)
				PRINTLN("[SETUP_TEAM_CHAT_AND_TARGETTING_RESTRICTIONS] Setting NETWORK_SET_SCRIPT_CONTROLLING_TEAMS as TRUE Receive")
			ENDIF
			
			PRINTLN("[PLAYER_LOOP] - NETWORK_OVERRIDE_SEND_RESTRICTIONS 2")
			FOR iPart = 0 TO (MAX_NUM_MC_PLAYERS - 1)
				PARTICIPANT_INDEX tempPart = INT_TO_PARTICIPANTINDEX(iPart)
				
				IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
					PLAYER_INDEX playerIndex = NETWORK_GET_PLAYER_INDEX(tempPart)
					
					IF GET_PLAYER_TEAM(playerIndex) = iTeam
						NETWORK_OVERRIDE_RECEIVE_RESTRICTIONS(playerIndex, TRUE)
						NETWORK_SET_SAME_TEAM_AS_LOCAL_PLAYER(iPart, TRUE)
						#IF IS_DEBUG_BUILD
						PRINTLN("[SETUP_TEAM_CHAT_AND_TARGETTING_RESTRICTIONS] NETWORK_OVERRIDE_RECEIVE_RESTRICTIONS - GET_PLAYER_TEAM(", iPart, "-", GET_PLAYER_NAME(playerIndex), ") = ", iTeam)
					ELSE
						PRINTLN("[SETUP_TEAM_CHAT_AND_TARGETTING_RESTRICTIONS] NETWORK_OVERRIDE_RECEIVE_RESTRICTIONS - ELSE ... playerIndex = ", iPart, "-", GET_PLAYER_NAME(playerIndex), " ... GET_PLAYER_TEAM(playerIndex) = ", GET_PLAYER_TEAM(playerIndex)," ... iTeam = ", iTeam)
						#ENDIF
					ENDIF
				ENDIF
			ENDFOR
		ENDIF
		
		//Send
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iTeamBitset, ciBS_TEAM_CROSS_TEAM_CHAT_T0 + iTeam)
			PRINTLN("[SETUP_TEAM_CHAT_AND_TARGETTING_RESTRICTIONS] IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[", MC_playerBD[iLocalPart].iTeam, "].iTeamBitset, ciBS_TEAM_CROSS_TEAM_CHAT_T0 + ", iTeam, ")")
			
			//Text chat
			IF DOES_TEAM_LIKE_TEAM(MC_playerBD[iLocalPart].iTeam, iTeam, DEFAULT, iRuleOverride)
			OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSet, ciUseAllTeamChat)
				NETWORK_SET_SCRIPT_CONTROLLING_TEAMS(TRUE)
				PRINTLN("[SETUP_TEAM_CHAT_AND_TARGETTING_RESTRICTIONS] Setting NETWORK_SET_SCRIPT_CONTROLLING_TEAMS as TRUE Send")
			ENDIF
			
			PRINTLN("[PLAYER_LOOP] - NETWORK_OVERRIDE_SEND_RESTRICTIONS")
			FOR iPart = 0 TO (MAX_NUM_MC_PLAYERS - 1)
				PARTICIPANT_INDEX tempPart = INT_TO_PARTICIPANTINDEX(iPart)
				
				IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
					PLAYER_INDEX playerIndex = NETWORK_GET_PLAYER_INDEX(tempPart)
					
					IF GET_PLAYER_TEAM(playerIndex) = iTeam
						NETWORK_OVERRIDE_SEND_RESTRICTIONS(playerIndex, TRUE)
						NETWORK_SET_SAME_TEAM_AS_LOCAL_PLAYER(iPart, TRUE)
						#IF IS_DEBUG_BUILD
						PRINTLN("[SETUP_TEAM_CHAT_AND_TARGETTING_RESTRICTIONS] NETWORK_OVERRIDE_SEND_RESTRICTIONS - GET_PLAYER_TEAM(", iPart, "-", GET_PLAYER_NAME(playerIndex), ") = ", iTeam)
					ELSE
						PRINTLN("[SETUP_TEAM_CHAT_AND_TARGETTING_RESTRICTIONS] NETWORK_OVERRIDE_SEND_RESTRICTIONS - ELSE ... playerIndex = ", iPart, "-", GET_PLAYER_NAME(playerIndex), " ... GET_PLAYER_TEAM(playerIndex) = ", GET_PLAYER_TEAM(playerIndex)," ... iTeam = ", iTeam)
						#ENDIF
					ENDIF
				ENDIF
			ENDFOR
		ENDIF
	ENDFOR
ENDPROC

PROC START_PLACED_PEDS_IN_COVER()
	
	IF NOT IS_BIT_SET(MC_ServerBD.iServerBitSet7, SBBOOL7_SET_ALL_PLACED_PEDS_INTO_COVER)
		INT i = 0
		FOR i = 0 TO FMMC_MAX_PEDS-1
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niPed[i])
				PED_INDEX tempPed = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[i])			
				IF NOT IS_PED_INJURED(tempPed)
				AND NOT IS_PED_IN_ANY_VEHICLE(tempPed)	
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedBitsetTwelve, ciPed_BSTwelve_SpawnPedInCover)
						VECTOR vCoverCoords = GET_ENTITY_COORDS(tempPed)
						PRINTLN("[LM][RCC MISSION] START_PLACED_PEDS_IN_COVER - iPed: ", i, " Entering Cover.")
						SET_PED_TO_LOAD_COVER(tempPed, TRUE)
						COVERPOINT_INDEX cpIndex = GET_CLOSEST_COVER_POINT_TO_LOCATION(GET_ENTITY_COORDS(tempPed), vCoverCoords)
						TASK_PUT_PED_DIRECTLY_INTO_COVER(tempPed, vCoverCoords, -1, TRUE, DEFAULT, TRUE, FALSE, cpIndex)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(tempPed, TRUE)
						FMMC_SET_LONG_BIT(iPedStartedInCoverBitset, i)
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
		SET_BIT(MC_ServerBD.iServerBitSet7, SBBOOL7_SET_ALL_PLACED_PEDS_INTO_COVER)
	ENDIF
	
ENDPROC

PROC PROCESS_ENSURE_PEDS_ALWAYS_DROP_A_PICKUP_FROM_INDEX_IF_ASSIGNED()
	
	INT iPedsWithWeaponBS[FMMC_MAX_WEAPONS][FMMC_MAX_PEDS_BITSET]	
	INT i = 0
	FOR i = 0 TO FMMC_MAX_PEDS-1
		IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedBitsetFifteen, ciPED_BSFifteen_AlwaysEnsurePickupIndexIsDropped)
			RELOOP
		ENDIF
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iDropPickupIndex = -1
			RELOOP
		ENDIF
		
		FMMC_SET_LONG_BIT(iPedsWithWeaponBS[g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iDropPickupIndex], i)
		
		PRINTLN("[PostEntityCreation][PedDropsAndPickups] - iPed: ", i, " has iWeapon: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iDropPickupIndex, " as a drop with ciPED_BSFifteen_AlwaysEnsurePickupIndexIsDropped")		
	ENDFOR
	
	INT iPedsWithPickupIndex
	INT iPed = 0
	FOR i = 0 TO FMMC_MAX_WEAPONS-1
		
		iPedsWithPickupIndex = 0
		FOR iPed = 0 TO FMMC_MAX_PEDS_BITSET-1
			iPedsWithPickupIndex += COUNT_SET_BITS(iPedsWithWeaponBS[i][iPed])
		ENDFOR
		
		IF iPedsWithPickupIndex = 0
			RELOOP
		ENDIF
						
		INT iRandomPed = GET_RANDOM_INT_IN_RANGE(0, iPedsWithPickupIndex)
		
		PRINTLN("[PostEntityCreation][PedDropsAndPickups] - There are ", iPedsWithPickupIndex, " peds who have assigned Weapon: ", i, " as a drop and the ", iRandomPed+1, " (th) ped in the list has been given the 100% chance to drop")
		
		INT iCount = 0
		
		FOR iPed = 0 TO FMMC_MAX_PEDS-1
			IF FMMC_IS_LONG_BIT_SET(iPedsWithWeaponBS[i], iPed)				
				IF iCount = iRandomPed	
					BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_ForcePedDropPickupIndexChance, DEFAULT, iPed, DEFAULT, DEFAULT, DEFAULT, DEFAULT, FALSE)
					PRINTLN("[PostEntityCreation][PedDropsAndPickups] - Assigning the 100% chance to drop for iPed: ", iPed, " iWeapon: ", i)
					BREAKLOOP
				ENDIF
				iCount++
			ENDIF			
		ENDFOR
	ENDFOR
	
ENDPROC

PROC PROCESS_POST_ENTITY_CREATION()
	
	START_PLACED_PEDS_IN_COVER()
	
	PROCESS_ENSURE_PEDS_ALWAYS_DROP_A_PICKUP_FROM_INDEX_IF_ASSIGNED()
	
	SET_ALL_MISSION_VEHICLES_ABLE_TO_MIGRATE(MC_serverBD_1.sFMMC_SBD, TRUE)
	
ENDPROC

PROC RESET_MICROPHONE_POSITION_FROM_START_TRANSITION()
	IF NOT IS_VECTOR_ZERO(vStartMicrophonePositionOverride)
		PRINTLN("[LM][RCC MISSION][RESET_MICROPHONE_POSITION_FROM_START_TRANSITION] - Resetting microphone from Position: ", vStartMicrophonePositionOverride)
		SET_MICROPHONE_POSITION(FALSE, vStartMicrophonePositionOverride, vStartMicrophonePositionOverride, vStartMicrophonePositionOverride)
		vStartMicrophonePositionOverride = <<0.0, 0.0, 0.0>>
	ENDIF
ENDPROC

PROC PROCESS_FREEZE_MICROPHONE_AT_PLAYER_FOR_MISSION()
	
	IF eMissionIntroStage != MISSION_INTRO_STAGE_INIT
		RESET_MICROPHONE_POSITION_FROM_START_TRANSITION()
		EXIT
	ENDIF
	
	// Stops positional sounds changing while we're warping the player via a faded screen/renderphase pause or skycam...
	IF IS_VECTOR_ZERO(vStartMicrophonePositionOverride)			
		vStartMicrophonePositionOverride = GET_ENTITY_COORDS(localPlayerPed)
		PRINTLN("[LM][RCC MISSION] [SCRIPT INIT] PROCESS_SCRIPT_INIT PROCESS_FREEZE_MICROPHONE_AT_PLAYER_FOR_MISSION - Calling FREEZE_MICROPHONE - Position: ", vStartMicrophonePositionOverride)
	ENDIF
	SET_MICROPHONE_POSITION(TRUE, vStartMicrophonePositionOverride, vStartMicrophonePositionOverride, vStartMicrophonePositionOverride)
	
	PRINTLN("[LM][RCC MISSION] [SCRIPT INIT] PROCESS_SCRIPT_INIT PROCESS_FREEZE_MICROPHONE_AT_PLAYER_FOR_MISSION - Calling FREEZE_MICROPHONE - So that sound position does not change during the transition from corona to gameplay.")
	FREEZE_MICROPHONE()
ENDPROC

PROC CHECK_IF_OBJECT_IS_CONTAINER_AND_SET_SERVER_DATA( INT iTeam, INT iObject )
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[ iObject ].iRule[ iteam ] = FMMC_OBJECTIVE_LOGIC_MINIGAME
	AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[ iObject ].mn = PROP_CONTAINER_LD
		SET_BIT( MC_serverBD.iIsHackContainer, iObject )
	ENDIF
ENDPROC

PROC SET_UP_DESPAWN_PROPS()
	INT iProp = 0
	FOR iProp = 0 TO FMMC_MAX_NUM_FADEOUT_PROPS_EVERY_FRAME-1
		iPropFadeoutEveryFrameIndex[iProp] = -1
		iFlashToggle[iProp] = 1
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iForceCleanupPropAfterTime > 0
			IF NOT IS_BIT_SET(iLocalBoolCheck28, LBOOL28_USING_PROP_CLEANUP_FUNCTIONALITY)
				SET_BIT(iLocalBoolCheck28, LBOOL28_USING_PROP_CLEANUP_FUNCTIONALITY)
				CPRINTLN( DEBUG_CONTROLLER, "[MANAGE_SPECTATOR_INIT] SETTING LBOOL28_USING_PROP_CLEANUP_FUNCTIONALITY") 
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

PROC SET_SERVER_TARGETTYPES_FOR_ENTITY(INT iNumber, INT iTeam, INT iTargetType = ci_TARGET_NONE)
	
	BOOL bCheckExtraObjectives = FALSE
	INT iRuleType = ciRULE_TYPE_NONE
	INT iPriority = FMMC_PRIORITY_IGNORE
	
	SWITCH iTargetType
		CASE ci_TARGET_PLAYER
			IF MC_serverBD_4.iPlayerRulePriority[iNumber][iteam] < FMMC_MAX_RULES
			AND MC_serverBD_4.iPlayerRule[iNumber][iteam] != FMMC_OBJECTIVE_LOGIC_NONE
				iPriority = MC_serverBD_4.iPlayerRulePriority[iNumber][iteam]
				PRINTLN("[RCC MISSION] SET_SERVER_TARGETTYPES_FOR_ENTITY - PLAYER RULE ",iNumber," is an objective for team ",iteam,", priority ",iPriority)
				
				MC_serverBD.iTargetType[iPriority][iteam] = iTargetType
			ENDIF
		BREAK
		
		CASE ci_TARGET_LOCATION
			IF MC_serverBD_4.iGotoLocationDataPriority[iNumber][iteam] < FMMC_MAX_RULES
				iPriority = MC_serverBD_4.iGotoLocationDataPriority[iNumber][iteam]
				PRINTLN("[RCC MISSION] SET_SERVER_TARGETTYPES_FOR_ENTITY - LOCATION ", iNumber, " is an objective for team ", iteam, ", priority ",iPriority)
				
				MC_serverBD.iGotoLocationDataTakeoverTime[iteam][iPriority] = GET_CAPTURE_TIME_FOR_CURRENT_RULE(iTeam, iPriority)
				PRINTLN("SET_SERVER_TARGETTYPES_FOR_ENTITY - MC_serverBD.iGotoLocationDataTakeoverTime[",iteam,"][",iPriority,"]: ", MC_serverBD.iGotoLocationDataTakeoverTime[iteam][iPriority])
				MC_serverBD.iRuleReCapture[iteam][iPriority] = GET_FMMC_OBJECTIVE_CAPTURES_OR_KILLS_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iteam].iReCapture[iPriority])
				
				IF MC_serverBD_4.iGotoLocationDataRule[iNumber][iteam] = FMMC_OBJECTIVE_LOGIC_CAPTURE
					SET_BIT(MC_serverBD.iLocteamFailBitset[iNumber],iteam)
				ENDIF
				
				IF MC_serverBD_4.iGotoLocationDataRule[iNumber][iteam]!= FMMC_OBJECTIVE_LOGIC_NONE
					MC_serverBD.iTargetType[iPriority][iteam] = iTargetType
					
					bCheckExtraObjectives = TRUE
					iRuleType = ciRULE_TYPE_GOTO
				ENDIF
			ENDIF
		BREAK
		
		CASE ci_TARGET_PED
			IF MC_serverBD_4.iPedPriority[iNumber][iteam] < FMMC_MAX_RULES
				iPriority = MC_serverBD_4.iPedPriority[iNumber][iteam]
				PRINTLN("[RCC MISSION] SET_SERVER_TARGETTYPES_FOR_ENTITY - PED ",iNumber," is an objective for team ",iteam,", priority ",iPriority)
				
				MC_serverBD.iPedTakeoverTime[iteam][iPriority] = GET_CAPTURE_TIME_FOR_CURRENT_RULE(iTeam, iPriority)
				MC_serverBD.iRuleReCapture[iteam][iPriority] = GET_FMMC_OBJECTIVE_CAPTURES_OR_KILLS_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iteam].iReCapture[iPriority])
				
				IF MC_serverBD_4.iPedRule[iNumber][iteam] != FMMC_OBJECTIVE_LOGIC_NONE
					SET_BIT(MC_serverBD.iPedteamFailBitset[iNumber],iteam)
					MC_serverBD.iTargetType[iPriority][iteam] = iTargetType
					
					bCheckExtraObjectives = TRUE
					iRuleType = ciRULE_TYPE_PED
					
					SET_PED_CAN_BE_KILLED_ON_THIS_RULE( iNumber, iTeam )
					
					IF MC_serverBD_4.iPedRule[iNumber][iteam] = FMMC_OBJECTIVE_LOGIC_PHOTO
						IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iPhotoCanBeDead, iPriority)
							FMMC_SET_LONG_BIT(MC_serverBD.iDeadPedPhotoBitset[iteam], iNumber)
						ENDIF
					ENDIF
					
				ENDIF
			ENDIF
		BREAK
		
		CASE ci_TARGET_VEHICLE
			IF MC_serverBD_4.iVehPriority[iNumber][iteam] < FMMC_MAX_RULES
				iPriority = MC_serverBD_4.iVehPriority[iNumber][iteam]
				PRINTLN("[RCC MISSION] SET_SERVER_TARGETTYPES_FOR_ENTITY - VEHICLE ",iNumber," is an objective for team ",iteam,", priority ",iPriority)
				
				MC_serverBD.iVehTakeoverTime[iteam][iPriority] = GET_CAPTURE_TIME_FOR_CURRENT_RULE(iTeam, iPriority)
				MC_serverBD.iRuleReCapture[iteam][iPriority] = GET_FMMC_OBJECTIVE_CAPTURES_OR_KILLS_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iteam].iReCapture[iPriority])
				
				IF MC_serverBD_4.ivehRule[iNumber][iteam] != FMMC_OBJECTIVE_LOGIC_NONE
					SET_BIT(MC_serverBD.iVehteamFailBitset[iNumber],iteam)
					MC_serverBD.iTargetType[iPriority][iteam] = iTargetType
					
					bCheckExtraObjectives = TRUE
					iRuleType = ciRULE_TYPE_VEHICLE

					SET_VEHICLE_CAN_BE_KILLED_ON_THIS_RULE( iNumber, iTeam )
				ENDIF
			ENDIF
		BREAK
		
		CASE ci_TARGET_OBJECT
			IF MC_serverBD_4.iObjPriority[iNumber][iteam] < FMMC_MAX_RULES
				iPriority = MC_serverBD_4.iObjPriority[iNumber][iteam]
				PRINTLN("[RCC MISSION] SET_SERVER_TARGETTYPES_FOR_ENTITY - OBJECT ",iNumber," is an objective for team ",iteam,", priority ",iPriority)
				
				MC_serverBD.iObjTakeoverTime[iteam][iPriority] = GET_CAPTURE_TIME_FOR_CURRENT_RULE(iTeam, iPriority)
				MC_serverBD.iRuleReCapture[iteam][iPriority] = GET_FMMC_OBJECTIVE_CAPTURES_OR_KILLS_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iteam].iReCapture[iPriority])
				
				IF MC_serverBD_4.iObjRule[iNumber][iteam] != FMMC_OBJECTIVE_LOGIC_NONE
					SET_BIT(MC_serverBD.iObjteamFailBitset[iNumber], iteam)
					MC_serverBD.iTargetType[iPriority][iteam] = ci_TARGET_OBJECT
					
					bCheckExtraObjectives = TRUE
					iRuleType = ciRULE_TYPE_OBJECT
					
					IF MC_serverBD_4.iObjRule[iNumber][iteam] = FMMC_OBJECTIVE_LOGIC_KILL
					OR MC_serverBD_4.iObjRule[iNumber][iteam] = FMMC_OBJECTIVE_LOGIC_DAMAGE
						SET_OBJECT_CAN_BE_KILLED_ON_THIS_RULE( iNumber, iTeam )
					ELIF MC_serverBD_4.iObjRule[iNumber][iteam] = FMMC_OBJECTIVE_LOGIC_MINIGAME
						SET_BIT(MC_serverBD.iWasHackObj,iNumber)
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	IF bCheckExtraObjectives
		
		INT iextraObjectiveNum
		
		// Loop through all potential entites that have multiple rules
		FOR iextraObjectiveNum = 0 TO (MAX_NUM_EXTRA_OBJECTIVE_ENTITIES - 1)
			
			// If the type matches the one passed
			IF g_FMMC_STRUCT.iExtraObjectiveEntityType[iextraObjectiveNum] = iRuleType
			AND g_FMMC_STRUCT.iExtraObjectiveEntityId[iextraObjectiveNum] = iNumber
				
				INT iextraObjectiveLoop
				
				FOR iextraObjectiveLoop = 0 TO (MAX_NUM_EXTRA_OBJECTIVE_PER_ENTITY - 1)
					IF g_FMMC_STRUCT.iExtraObjectivePriority[iextraObjectiveNum][iextraObjectiveLoop][iTeam] < FMMC_MAX_RULES
					AND g_FMMC_STRUCT.iExtraObjectivePriority[iextraObjectiveNum][iextraObjectiveLoop][iTeam] >= 0
					AND (GET_FMMC_RULE_FROM_CREATOR_RULE(g_FMMC_STRUCT.iExtraObjectiveRule[iextraObjectiveNum][iextraObjectiveLoop][iTeam]) != FMMC_OBJECTIVE_LOGIC_NONE)
						MC_serverBD.iTargetType[g_FMMC_STRUCT.iExtraObjectivePriority[iextraObjectiveNum][iextraObjectiveLoop][iTeam]][iteam] = iTargetType
						
						iPriority = g_FMMC_STRUCT.iExtraObjectivePriority[iextraObjectiveNum][iextraObjectiveLoop][iTeam]
						INT iRule = GET_FMMC_RULE_FROM_CREATOR_RULE(g_FMMC_STRUCT.iExtraObjectiveRule[iextraObjectiveNum][iextraObjectiveLoop][iTeam])
						
						PRINTLN("[RCC MISSION] SET_SERVER_TARGETTYPES_FOR_ENTITY - (extra objective entity) target type ",iTargetType," for team ",iteam,", priority ",iPriority)
						IF iPriority < FMMC_MAX_RULES
							
							SWITCH iTargetType
								CASE ci_TARGET_LOCATION
									MC_serverBD.iGotoLocationDataTakeoverTime[iteam][iPriority] = GET_CAPTURE_TIME_FOR_CURRENT_RULE(iTeam, iPriority)
									PRINTLN("SET_SERVER_TARGETTYPES_FOR_ENTITY - MC_serverBD.iGotoLocationDataTakeoverTime[",iteam,"][",iPriority,"]: ", MC_serverBD.iGotoLocationDataTakeoverTime[iteam][iPriority])
									MC_serverBD.iRuleReCapture[iteam][iPriority] = GET_FMMC_OBJECTIVE_CAPTURES_OR_KILLS_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iteam].iReCapture[iPriority])
									
									IF iRule = FMMC_OBJECTIVE_LOGIC_CAPTURE
										SET_BIT(MC_serverBD.iLocteamFailBitset[iNumber],iteam)
									ENDIF
								BREAK
								
								CASE ci_TARGET_PED
									MC_serverBD.iPedTakeoverTime[iteam][iPriority] = GET_CAPTURE_TIME_FOR_CURRENT_RULE(iTeam, iPriority)
									MC_serverBD.iRuleReCapture[iteam][iPriority] = GET_FMMC_OBJECTIVE_CAPTURES_OR_KILLS_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iteam].iReCapture[iPriority])
									
									IF iRule = FMMC_OBJECTIVE_LOGIC_PHOTO
										IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iPhotoCanBeDead,iPriority)
											FMMC_SET_LONG_BIT(MC_serverBD.iDeadPedPhotoBitset[iteam], iNumber)
										ENDIF									
									ENDIF
								BREAK
								
								CASE ci_TARGET_VEHICLE
									
									MC_serverBD.iVehTakeoverTime[iteam][iPriority] = GET_CAPTURE_TIME_FOR_CURRENT_RULE(iTeam, iPriority)
									MC_serverBD.iRuleReCapture[iteam][iPriority] = GET_FMMC_OBJECTIVE_CAPTURES_OR_KILLS_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iteam].iReCapture[iPriority])
								
								BREAK
								
								CASE ci_TARGET_OBJECT
									
									MC_serverBD.iObjTakeoverTime[iteam][iPriority] = GET_CAPTURE_TIME_FOR_CURRENT_RULE(iTeam, iPriority)
									MC_serverBD.iRuleReCapture[iteam][iPriority] = GET_FMMC_OBJECTIVE_CAPTURES_OR_KILLS_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iteam].iReCapture[iPriority])
									
									
									IF iRule = FMMC_OBJECTIVE_LOGIC_MINIGAME
										SET_BIT(MC_serverBD.iWasHackObj,iNumber)
									ENDIF
									
								BREAK
							ENDSWITCH
							
						ENDIF
						
					ENDIF
				ENDFOR
				
				//We found the multi-rule number for this entity, there won't be any more so break out of the objective entity loop:
				iextraObjectiveNum = MAX_NUM_EXTRA_OBJECTIVE_ENTITIES
			ENDIF
			
		ENDFOR
	ENDIF
	
	IF MC_serverBD.iMaxObjectives[iteam] < iPriority
		MC_serverBD.iMaxObjectives[iteam] = iPriority
		PRINTLN("[RCC MISSION] MC_serverBD.iMaxObjectives[iteam] updated to ",MC_serverBD.iMaxObjectives[iteam]," for team ",iteam)
	ENDIF
	
ENDPROC

PROC RESET_UNTRACKED_CONTINUITY_VARS()
	
	INT i
	BOOL bQuickRestartReset = IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_ClearOnQuickRestart) AND (IS_THIS_A_QUICK_RESTART_JOB() OR IS_CORONA_INITIALISING_A_QUICK_RESTART())
	
	//Server Continuity
	IF bQuickRestartReset
	OR NOT IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_PedDeathTracking)
		PRINTLN("[JS][CONTINUITY] - RESET_UNTRACKED_CONTINUITY_VARS - CLEARING iPedDeathBitset")
		FOR i = 0 TO FMMC_MAX_PEDS_BITSET - 1
			g_TransitionSessionNonResetVars.sMissionContinuityVars.iPedDeathBitset[i] = 0
		ENDFOR
	ENDIF
	
	IF bQuickRestartReset
	OR NOT IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_PedSpecialTracking)
		PRINTLN("[JS][CONTINUITY] - RESET_UNTRACKED_CONTINUITY_VARS - CLEARING iPedSpecialBitset")
		FOR i = 0 TO FMMC_MAX_PEDS_BITSET - 1
			g_TransitionSessionNonResetVars.sMissionContinuityVars.iPedSpecialBitset[i] = 0
		ENDFOR
	ENDIF
	
	IF bQuickRestartReset
	OR NOT IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_ObjectDestroyedTracking)
		PRINTLN("[JS][CONTINUITY] - RESET_UNTRACKED_CONTINUITY_VARS - CLEARING iObjectDestroyedBitset")
		FOR i = 0 TO FMMC_MAX_OBJ_CONTINUITY_BITSET - 1
			g_TransitionSessionNonResetVars.sMissionContinuityVars.iObjectDestroyedBitset[i] = 0
		ENDFOR
	ENDIF
	
	IF bQuickRestartReset
	OR NOT IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_EMPUsedTracking)
		PRINTLN("[JS][CONTINUITY] - RESET_UNTRACKED_CONTINUITY_VARS - CLEARING iObjectDestroyedBitset")
		CLEAR_BIT(g_TransitionSessionNonResetVars.sMissionContinuityVars.iGenericTrackingBitset, ciContinuityGenericTrackingBS_EMPUsed)
	ENDIF
	
	IF bQuickRestartReset
	OR NOT IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_DoorsUsedAltConfigTracking)
		PRINTLN("[JS][CONTINUITY] - RESET_UNTRACKED_CONTINUITY_VARS - CLEARING iDoorsUseAltConfigBitset")
		FOR i = 0 TO FMMC_MAX_DOOR_CONTINUITY_BITSET - 1
			g_TransitionSessionNonResetVars.sMissionContinuityVars.iDoorsUseAltConfigBitset[i] = 0
		ENDFOR
	ENDIF
	
	//Not affected by bQuickRestartReset
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_LocationTracking)
		PRINTLN("[JS][CONTINUITY] - RESET_UNTRACKED_CONTINUITY_VARS - CLEARING iLocationTrackingBitset")
		g_TransitionSessionNonResetVars.sMissionContinuityVars.iLocationTrackingBitset = 0
	ENDIF
	
	//Not affected by bQuickRestartReset
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_ObjectTracking)
		PRINTLN("[JS][CONTINUITY] - RESET_UNTRACKED_CONTINUITY_VARS - CLEARING iObjectTrackingBitset")
		FOR i = 0 TO FMMC_MAX_OBJ_CONTINUITY_BITSET - 1
			g_TransitionSessionNonResetVars.sMissionContinuityVars.iObjectTrackingBitset[i] = 0
		ENDFOR
	ENDIF
	
	//Not affected by bQuickRestartReset
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_InteractableTracking)
		PRINTLN("[JS][CONTINUITY] - RESET_UNTRACKED_CONTINUITY_VARS - CLEARING iInteractablesCompleteBitset")
		FOR i = 0 TO FMMC_MAX_INTERACTABLE_CONTINUITY_BITSET - 1
			g_TransitionSessionNonResetVars.sMissionContinuityVars.iInteractablesCompleteBitset[i] = 0
		ENDFOR
	ENDIF
	
	IF bQuickRestartReset
	OR NOT IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_VehicleDestroyedTracking)
		PRINTLN("[JS][CONTINUITY] - RESET_UNTRACKED_CONTINUITY_VARS - CLEARING iVehicleDestroyedTrackingBitset")
		g_TransitionSessionNonResetVars.sMissionContinuityVars.iVehicleDestroyedTrackingBitset = 0
	ENDIF
	
	IF bQuickRestartReset
	OR NOT IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_DynoPropDestroyedTracking)
		PRINTLN("[JS][CONTINUITY] - RESET_UNTRACKED_CONTINUITY_VARS - CLEARING iDynoPropDestroyedTrackingBitset")
		g_TransitionSessionNonResetVars.sMissionContinuityVars.iDynoPropDestroyedTrackingBitset = 0
	ENDIF
	
	//Not affected by bQuickRestartReset
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_WeatherTracking)
		PRINTLN("[JS][CONTINUITY] - RESET_UNTRACKED_CONTINUITY_VARS - CLEARING iWeather")
		g_TransitionSessionNonResetVars.sMissionContinuityVars.iWeather = -1
	ENDIF
	
	//Not affected by bQuickRestartReset
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_TimeofDayTracking)
		PRINTLN("[JS][CONTINUITY] - RESET_UNTRACKED_CONTINUITY_VARS - CLEARING iTimeHour and iTimeMinute")
		g_TransitionSessionNonResetVars.sMissionContinuityVars.iTimeHour = -1
		g_TransitionSessionNonResetVars.sMissionContinuityVars.iTimeMinute = -1
	ENDIF
	
	//Not affected by bQuickRestartReset
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_TrackLeaderHeistCompletionStat)
		PRINTLN("[JS][CONTINUITY] - RESET_UNTRACKED_CONTINUITY_VARS - CLEARING iCurrentHeistCompletionStat")
		g_TransitionSessionNonResetVars.sMissionContinuityVars.iCurrentHeistCompletionStat = -2
	ENDIF
	
	//Not affected by bQuickRestartReset
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_ContentSpecificTracking)
		MC_serverBD_1.sMissionContinuityVars.iContentSpecificContinuityBitset = 0
		PRINTLN("[JS][CONTINUITY] - RESET_UNTRACKED_CONTINUITY_VARS - CLEARING iContentSpecificContinuityBitset")
	ENDIF

	//Local Continuity
	//Not affected by bQuickRestartReset
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_VehicleSeatTracking)
		PRINTLN("[JS][CONTINUITY] - RESET_UNTRACKED_CONTINUITY_VARS - CLEARING iEndVehicleContinuityId")
		PRINTLN("[JS][CONTINUITY] - RESET_UNTRACKED_CONTINUITY_VARS - CLEARING eEndVehicleSeat")
		g_TransitionSessionNonResetVars.sMissionLocalContinuityVars.iEndVehicleContinuityId = -1
		g_TransitionSessionNonResetVars.sMissionLocalContinuityVars.eEndVehicleSeat = VS_DRIVER 
	ENDIF
	
	//Not affected by bQuickRestartReset
	//Resets on every quick restart
	IF (IS_THIS_A_QUICK_RESTART_JOB() OR IS_CORONA_INITIALISING_A_QUICK_RESTART())
	OR NOT IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_EarlyEndSpectatorTracking)
		PRINTLN("[JS][CONTINUITY] - RESET_UNTRACKED_CONTINUITY_VARS - CLEARING ciContinuityGenericTrackingBS_LivesFailSpectator")
		CLEAR_BIT(g_TransitionSessionNonResetVars.sMissionLocalContinuityVars.iGeneralLocalTrackingBitset, ciContinuityGenericTrackingBS_LivesFailSpectator)
	ENDIF
	
	IF bQuickRestartReset
	OR NOT IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_DroneUsageTracking)
		PRINTLN("[JS][CONTINUITY] - RESET_UNTRACKED_CONTINUITY_VARS - CLEARING ciContinuityGenericTrackingBS_DroneUsed")
		CLEAR_BIT(g_TransitionSessionNonResetVars.sMissionLocalContinuityVars.iGeneralLocalTrackingBitset, ciContinuityGenericTrackingBS_DroneUsed)
	ENDIF
	
	IF bQuickRestartReset
	OR NOT IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_WeaponPickupTracking)
		PRINTLN("[JS][CONTINUITY] - RESET_UNTRACKED_CONTINUITY_VARS - CLEARING iPickupCollectedTrackingBitset")
		FOR i = 0 TO FMMC_MAX_PICKUP_CONTINUITY_BITSET - 1
			g_TransitionSessionNonResetVars.sMissionLocalContinuityVars.iPickupCollectedTrackingBitset[i] = 0
		ENDFOR
		CLEAR_BIT(g_TransitionSessionNonResetVars.sMissionLocalContinuityVars.iGeneralLocalTrackingBitset, ciContinuityGenericTrackingBS_MidMissionInventoryCollected)
		PRINTLN("[JS][CONTINUITY] - RESET_UNTRACKED_CONTINUITY_VARS - CLEARING ciContinuityGenericTrackingBS_MidMissionInventoryCollected")
	ENDIF
	
	IF bQuickRestartReset
	OR NOT IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_DialogueTracking)
		PRINTLN("[JS][CONTINUITY] - RESET_UNTRACKED_CONTINUITY_VARS - CLEARING iDialogueTrackingBitset")
		FOR i = 0 TO ciMAX_DIALOGUE_BIT_SETS - 1
			g_TransitionSessionNonResetVars.sMissionLocalContinuityVars.iDialogueTrackingBitset[i] = 0
		ENDFOR
	ENDIF
	
	//Not affected by bQuickRestartReset
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_HeistBagTracking)
		PRINTLN("[JS][CONTINUITY] - RESET_UNTRACKED_CONTINUITY_VARS - CLEARING iHeistBag")
		g_TransitionSessionNonResetVars.sMissionLocalContinuityVars.iHeistBag = -1
	ENDIF
	
	//Not affected by bQuickRestartReset
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_ObjectCarrierTracking)
		PRINTLN("[JS][CONTINUITY] - RESET_UNTRACKED_CONTINUITY_VARS - CLEARING iObjectHeld")
		g_TransitionSessionNonResetVars.sMissionLocalContinuityVars.iObjectHeld = -1
	ENDIF
	
	//Not affected by bQuickRestartReset
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_PrerequisiteTracking)
		FOR i = 0 TO ciPREREQ_Bitsets - 1
			PRINTLN("[JS][CONTINUITY] - RESET_UNTRACKED_CONTINUITY_VARS - CLEARING iPrerequisiteBS[", i, "]")
			g_TransitionSessionNonResetVars.sMissionLocalContinuityVars.iPrerequisiteBS[i] = 0
		ENDFOR
	ENDIF
	
	//Not affected by bQuickRestartReset
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_OutfitTracking)
		g_TransitionSessionNonResetVars.sMissionLocalContinuityVars.iOutfit = -1
		CLEAR_BIT(g_TransitionSessionNonResetVars.sMissionLocalContinuityVars.iGeneralLocalTrackingBitset, ciContinuityGenericTrackingBS_DisguiseUsed)
		PRINTLN("[JS][CONTINUITY] - RESET_UNTRACKED_CONTINUITY_VARS - CLEARING iOutfit and disguise bit")
	ENDIF
	
	//Not affected by bQuickRestartReset
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_BagContentsTracking)
		PRINTLN("[JS][CONTINUITY] - RESET_UNTRACKED_CONTINUITY_VARS - CLEARING iCashGrabbed & fCurrentBagCapacity")
		g_TransitionSessionNonResetVars.sMissionLocalContinuityVars.sLootBag.iCashGrabbed = 0
		g_TransitionSessionNonResetVars.sMissionLocalContinuityVars.sLootBag.iTotalCashGrabbed = 0
		g_TransitionSessionNonResetVars.sMissionLocalContinuityVars.sLootBag.iStolenGoods = 0
		g_TransitionSessionNonResetVars.sMissionLocalContinuityVars.sLootBag.iLootBitSet = 0
		g_TransitionSessionNonResetVars.sMissionLocalContinuityVars.sLootBag.fCurrentBagCapacity = 0.0
		FOR i = 0 TO ciLOOT_TYPE_MAX - 1
			g_TransitionSessionNonResetVars.sMissionLocalContinuityVars.sLootBag.fCurrentCapacity[i] = 0.0
			PRINTLN("[JS][CONTINUITY] - RESET_UNTRACKED_CONTINUITY_VARS - CLEARING sLootBag.fCurrentCapacity[",i,"]")
		ENDFOR
	ENDIF
	
	//Not affected by bQuickRestartReset
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_PlayerAbilityTracking)
		PRINTLN("[RM][CONTINUITY] - RESET_UNTRACKED_CONTINUITY_VARS - CLEARING Player Ability Tracking")
		FOR i = 0 TO ciNUM_PLAYER_ABILITIES - 1
			g_TransitionSessionNonResetVars.sMissionLocalContinuityVars.sPlayerAbilities.sPAContinuity[i].bIsActive = FALSE
			g_TransitionSessionNonResetVars.sMissionLocalContinuityVars.sPlayerAbilities.sPAContinuity[i].iUseCount = 0			
		ENDFOR		
	ENDIF
	
	IF bQuickRestartReset
		g_TransitionSessionNonResetVars.sMissionLocalContinuityVars.iPlayerThermalCharges = 0
		PRINTLN("[JS][CONTINUITY] - RESET_UNTRACKED_CONTINUITY_VARS - CLEARING iPlayerThermalCharges")
	ENDIF
	
	IF (IS_THIS_A_QUICK_RESTART_JOB() OR IS_CORONA_INITIALISING_A_QUICK_RESTART())
		PRINTLN("[JS][CONTINUITY] - RESET_UNTRACKED_CONTINUITY_VARS - CLEARING iTeamDeaths and iNumPlayerDeaths")
		FOR i = 0 TO FMMC_MAX_TEAMS - 1
			g_TransitionSessionNonResetVars.sMissionContinuityVars.iTeamDeaths[i] = 0
		ENDFOR
		g_TransitionSessionNonResetVars.sMissionLocalContinuityVars.iNumPlayerDeaths = 0
	ENDIF
	
	//Not affected by bQuickRestartReset
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_VehiclePositionTracking)
		PRINTLN("[JS][CONTINUITY] - RESET_UNTRACKED_CONTINUITY_VARS - CLEARING Vehicle Position Tracking")
		FOR i = 0 TO FMMC_MAX_VEHICLES - 1
			g_TransitionSessionNonResetVars.sMissionLocalContinuityVars.vVehicleLocations[i] = <<0,0,0>>
		ENDFOR		
	ENDIF
	
ENDPROC

PROC RESTORE_MISSION_CONTINUITY_VARS()
	
	IF (HAS_FIRST_STRAND_MISSION_BEEN_PASSED_NO_RESET() OR HAS_FIRST_STRAND_MISSION_BEEN_PASSED())
	OR (IS_THIS_A_QUICK_RESTART_JOB() OR IS_CORONA_INITIALISING_A_QUICK_RESTART())
#IF IS_DEBUG_BUILD
	OR bXMLOverride_Continuity
#ENDIF
		PRINTLN("[JS][CONTINUITY] - RESTORE_MISSION_CONTINUITY_VARS - On a strand mission (or restarting one), restoring continuity vars")
		
		RESET_UNTRACKED_CONTINUITY_VARS()

		MC_serverBD_1.sMissionContinuityVars = g_TransitionSessionNonResetVars.sMissionContinuityVars
		sMissionLocalContinuityVars = g_TransitionSessionNonResetVars.sMissionLocalContinuityVars
#IF IS_DEBUG_BUILD
		//Server Continuity
		PRINTLN("[JS][CONTINUITY] - RESTORE_MISSION_CONTINUITY_VARS - iGenericTrackingBitset = ", MC_serverBD_1.sMissionContinuityVars.iGenericTrackingBitset)
		INT i
		FOR i = 0 TO FMMC_MAX_OBJ_CONTINUITY_BITSET - 1
			PRINTLN("[JS][CONTINUITY] - RESTORE_MISSION_CONTINUITY_VARS - iObjectTrackingBitset[",i,"] = ", MC_serverBD_1.sMissionContinuityVars.iObjectTrackingBitset[i])
			PRINTLN("[JS][CONTINUITY] - RESTORE_MISSION_CONTINUITY_VARS - iObjectDestroyedBitset[",i,"] = ", MC_serverBD_1.sMissionContinuityVars.iObjectDestroyedBitset[i])
		ENDFOR
		FOR i = 0 TO FMMC_MAX_DOOR_CONTINUITY_BITSET - 1
			PRINTLN("[JS][CONTINUITY] - RESTORE_MISSION_CONTINUITY_VARS - iDoorsUseAltConfigBitset[",i,"] = ", MC_serverBD_1.sMissionContinuityVars.iDoorsUseAltConfigBitset[i])
		ENDFOR
		FOR i = 0 TO FMMC_MAX_INTERACTABLE_CONTINUITY_BITSET - 1
			PRINTLN("[JS][CONTINUITY] - RESTORE_MISSION_CONTINUITY_VARS - iInteractablesCompleteBitset[",i,"] = ", MC_serverBD_1.sMissionContinuityVars.iInteractablesCompleteBitset[i])
		ENDFOR
		PRINTLN("[JS][CONTINUITY] - RESTORE_MISSION_CONTINUITY_VARS - iLocationTrackingBitset = ", MC_serverBD_1.sMissionContinuityVars.iLocationTrackingBitset)
		PRINTLN("[JS][CONTINUITY] - RESTORE_MISSION_CONTINUITY_VARS - iVehicleDestroyedTrackingBitset = ", MC_serverBD_1.sMissionContinuityVars.iVehicleDestroyedTrackingBitset)
		PRINTLN("[JS][CONTINUITY] - RESTORE_MISSION_CONTINUITY_VARS - iDynoPropDestroyedTrackingBitset = ", MC_serverBD_1.sMissionContinuityVars.iDynoPropDestroyedTrackingBitset)
		FOR i = 0 TO FMMC_MAX_PEDS_BITSET - 1
			PRINTLN("[JS][CONTINUITY] - RESTORE_MISSION_CONTINUITY_VARS - iPedDeathBitset[",i,"] = ", MC_serverBD_1.sMissionContinuityVars.iPedDeathBitset[i])
			
			PRINTLN("[JS][CONTINUITY] - RESTORE_MISSION_CONTINUITY_VARS - iPedSpecialBitset[",i,"] = ", MC_serverBD_1.sMissionContinuityVars.iPedSpecialBitset[i])
		ENDFOR
		PRINTLN("[JS][CONTINUITY] - RESTORE_MISSION_CONTINUITY_VARS - iWeather = ", MC_serverBD_1.sMissionContinuityVars.iWeather)
		PRINTLN("[JS][CONTINUITY] - RESTORE_MISSION_CONTINUITY_VARS - iTimeHour = ", MC_serverBD_1.sMissionContinuityVars.iTimeHour)
		PRINTLN("[JS][CONTINUITY] - RESTORE_MISSION_CONTINUITY_VARS - iTimeMinute = ", MC_serverBD_1.sMissionContinuityVars.iTimeMinute)
		PRINTLN("[JS][CONTINUITY] - RESTORE_MISSION_CONTINUITY_VARS - iTotalMissionTime = ", MC_serverBD_1.sMissionContinuityVars.iTotalMissionTime)		
		PRINTLN("[JS][CONTINUITY] - RESTORE_MISSION_CONTINUITY_VARS - iCurrentHeistCompletionStat = ", MC_serverBD_1.sMissionContinuityVars.iCurrentHeistCompletionStat)			
		PRINTLN("[JS][CONTINUITY] - RESTORE_MISSION_CONTINUITY_VARS - iStrandMissionsComplete = ", MC_serverBD_1.sMissionContinuityVars.iStrandMissionsComplete)
		PRINTLN("[JS][CONTINUITY] - RESTORE_MISSION_CONTINUITY_VARS - iHackFails = ", MC_serverBD_1.sMissionContinuityVars.iHackFails)
		PRINTLN("[JS][CONTINUITY] - RESTORE_MISSION_CONTINUITY_VARS - iGrabbedCashTotalDropped = ", MC_serverBD_1.sMissionContinuityVars.iGrabbedCashTotalDropped)
		PRINTLN("[JS][CONTINUITY] - RESTORE_MISSION_CONTINUITY_VARS - iGrabbedCashTotalTake = ", MC_serverBD_1.sMissionContinuityVars.iGrabbedCashTotalTake)
		FOR i = 0 TO FMMC_MAX_TEAMS - 1
			PRINTLN("[JS][CONTINUITY] - RESTORE_MISSION_CONTINUITY_VARS - iTeamKills[",i,"] = ", MC_serverBD_1.sMissionContinuityVars.iTeamKills[i])
			PRINTLN("[JS][CONTINUITY] - RESTORE_MISSION_CONTINUITY_VARS - iTeamDeaths[",i,"] = ", MC_serverBD_1.sMissionContinuityVars.iTeamDeaths[i])
			PRINTLN("[JS][CONTINUITY] - RESTORE_MISSION_CONTINUITY_VARS - iVariableTeamLives[",i,"] = ", MC_serverBD_1.sMissionContinuityVars.iVariableTeamLives[i])
			PRINTLN("[JS][CONTINUITY] - RESTORE_MISSION_CONTINUITY_VARS - iTeamHeadshots[",i,"] = ", MC_serverBD_1.sMissionContinuityVars.iTeamHeadshots[i])
		ENDFOR
		PRINTLN("[JS][CONTINUITY] - RESTORE_MISSION_CONTINUITY_VARS - iContinuitySeed = ", MC_serverBD_1.sMissionContinuityVars.iContinuitySeed)
		PRINTLN("[JS][CONTINUITY] - RESTORE_MISSION_CONTINUITY_VARS - iContentSpecificContinuityBitset = ", MC_serverBD_1.sMissionContinuityVars.iContentSpecificContinuityBitset)
		FOR i = 0 TO ciContinuityTelemetryInts_Max - 1
			PRINTLN("[JS][CONTINUITY] - RESTORE_MISSION_CONTINUITY_VARS - iTelemetryInts[", i, "] = ", MC_serverBD_1.sMissionContinuityVars.iTelemetryInts[i])
		ENDFOR
		
		//Local Continuity
		PRINTLN("[JS][CONTINUITY] - RESTORE_MISSION_CONTINUITY_VARS - iEndVehicleContinuityId = ", sMissionLocalContinuityVars.iEndVehicleContinuityId)
		PRINTLN("[JS][CONTINUITY] - RESTORE_MISSION_CONTINUITY_VARS - eEndVehicleSeat = ", ENUM_TO_INT(sMissionLocalContinuityVars.eEndVehicleSeat))
		PRINTLN("[JS][CONTINUITY] - RESTORE_MISSION_CONTINUITY_VARS - iGeneralLocalTrackingBitset = ", sMissionLocalContinuityVars.iGeneralLocalTrackingBitset)
		FOR i = 0 TO FMMC_MAX_PICKUP_CONTINUITY_BITSET - 1
			PRINTLN("[JS][CONTINUITY] - RESTORE_MISSION_CONTINUITY_VARS - iPickupCollectedTrackingBitset[",i,"] = ", sMissionLocalContinuityVars.iPickupCollectedTrackingBitset[i])
		ENDFOR
		FOR i = 0 TO ciMAX_DIALOGUE_BIT_SETS - 1
			PRINTLN("[JS][CONTINUITY] - RESTORE_MISSION_CONTINUITY_VARS - iDialogueTrackingBitset[",i,"] = ", sMissionLocalContinuityVars.iDialogueTrackingBitset[i])
		ENDFOR
		PRINTLN("[JS][CONTINUITY] - RESTORE_MISSION_CONTINUITY_VARS - iHeistBag = ", sMissionLocalContinuityVars.iHeistBag)
		PRINTLN("[JS][CONTINUITY] - RESTORE_MISSION_CONTINUITY_VARS - iOutfit = ", sMissionLocalContinuityVars.iOutfit)
		PRINTLN("[JS][CONTINUITY] - RESTORE_MISSION_CONTINUITY_VARS - sLootBag.iCashGrabbed = ", sMissionLocalContinuityVars.sLootBag.iCashGrabbed)
		PRINTLN("[JS][CONTINUITY] - RESTORE_MISSION_CONTINUITY_VARS - sLootBag.iTotalCashGrabbed = ", sMissionLocalContinuityVars.sLootBag.iTotalCashGrabbed)
		PRINTLN("[JS][CONTINUITY] - RESTORE_MISSION_CONTINUITY_VARS - sLootBag.iTotalCashGrabbed = ", sMissionLocalContinuityVars.sLootBag.iStolenGoods)
		PRINTLN("[JS][CONTINUITY] - RESTORE_MISSION_CONTINUITY_VARS - sLootBag.iLootBitSet = ", sMissionLocalContinuityVars.sLootBag.iLootBitSet)
		PRINTLN("[JS][CONTINUITY] - RESTORE_MISSION_CONTINUITY_VARS - sLootBag.fCurrentBagCapacity = ", sMissionLocalContinuityVars.sLootBag.fCurrentBagCapacity)
		FOR i = 0 TO ciLOOT_TYPE_MAX - 1
			PRINTLN("[JS][CONTINUITY] - RESTORE_MISSION_CONTINUITY_VARS - sLootBag.fCurrentCapacity[",i,"] = ", sMissionLocalContinuityVars.sLootBag.fCurrentCapacity[i])
		ENDFOR
		FOR i = 0 TO ciPREREQ_Bitsets - 1
			PRINTLN("[JS][CONTINUITY] - RESTORE_MISSION_CONTINUITY_VARS - iPrerequisiteBS[", i, "] = ", sMissionLocalContinuityVars.iPrerequisiteBS[i])
		ENDFOR
		FOR i = 0 TO ciNUM_PLAYER_ABILITIES - 1
			PRINTLN("[RM][CONTINUITY] - RESTORE_MISSION_CONTINUITY_VARS - sPAContinuity[", i, "] Is Active = ", sMissionLocalContinuityVars.sPlayerAbilities.sPAContinuity[i].bIsActive)
			PRINTLN("[RM][CONTINUITY] - RESTORE_MISSION_CONTINUITY_VARS - sPAContinuity[", i, "] USe Count = ", sMissionLocalContinuityVars.sPlayerAbilities.sPAContinuity[i].iUseCount)
		ENDFOR	
		PRINTLN("[JS][CONTINUITY] - RESTORE_MISSION_CONTINUITY_VARS - iObjectHeld = ", sMissionLocalContinuityVars.iObjectHeld)
		PRINTLN("[JS][CONTINUITY] - RESTORE_MISSION_CONTINUITY_VARS - iTripSkipCost = ", sMissionLocalContinuityVars.iTripSkipCost)
		PRINTLN("[JS][CONTINUITY] - RESTORE_MISSION_CONTINUITY_VARS - iNumPedKills = ", sMissionLocalContinuityVars.iNumPedKills)
		PRINTLN("[JS][CONTINUITY] - RESTORE_MISSION_CONTINUITY_VARS - iAmbientCopsKilled = ", sMissionLocalContinuityVars.iAmbientCopsKilled)
		PRINTLN("[JS][CONTINUITY] - RESTORE_MISSION_CONTINUITY_VARS - iNumHeadshots = ", sMissionLocalContinuityVars.iNumHeadshots)
		PRINTLN("[JS][CONTINUITY] - RESTORE_MISSION_CONTINUITY_VARS - iStartHits = ", sMissionLocalContinuityVars.iStartHits)
		PRINTLN("[JS][CONTINUITY] - RESTORE_MISSION_CONTINUITY_VARS - iStartShots = ", sMissionLocalContinuityVars.iStartShots)
		PRINTLN("[JS][CONTINUITY] - RESTORE_MISSION_CONTINUITY_VARS - iCivilianKills = ", sMissionLocalContinuityVars.iCivilianKills)
		PRINTLN("[JS][CONTINUITY] - RESTORE_MISSION_CONTINUITY_VARS - iNumPlayerDeaths = ", sMissionLocalContinuityVars.iNumPlayerDeaths)
		PRINTLN("[JS][CONTINUITY] - RESTORE_MISSION_CONTINUITY_VARS - iHackTime = ", sMissionLocalContinuityVars.iHackTime)
		PRINTLN("[JS][CONTINUITY] - RESTORE_MISSION_CONTINUITY_VARS - iNumHacks = ", sMissionLocalContinuityVars.iNumHacks)
		PRINTLN("[JS][CONTINUITY] - RESTORE_MISSION_CONTINUITY_VARS - iWantedTime = ", sMissionLocalContinuityVars.iWantedTime)
		PRINTLN("[JS][CONTINUITY] - RESTORE_MISSION_CONTINUITY_VARS - iNumWantedLose = ", sMissionLocalContinuityVars.iNumWantedLose)
		PRINTLN("[JS][CONTINUITY] - RESTORE_MISSION_CONTINUITY_VARS - iMedalObjectives = ", sMissionLocalContinuityVars.iMedalObjectives)
		PRINTLN("[JS][CONTINUITY] - RESTORE_MISSION_CONTINUITY_VARS - iMedalInteractables = ", sMissionLocalContinuityVars.iMedalInteractables)
		PRINTLN("[JS][CONTINUITY] - RESTORE_MISSION_CONTINUITY_VARS - iMedalEquipment = ", sMissionLocalContinuityVars.iMedalEquipment)
		PRINTLN("[JS][CONTINUITY] - RESTORE_MISSION_CONTINUITY_VARS - iStartStealthKills = ", sMissionLocalContinuityVars.iStartStealthKills)
		PRINTLN("[JS][CONTINUITY] - RESTORE_MISSION_CONTINUITY_VARS - iPlayerThermalCharges = ", sMissionLocalContinuityVars.iPlayerThermalCharges)
		PRINTLN("[JS][CONTINUITY] - RESTORE_MISSION_CONTINUITY_VARS - iPlayerThermalCharges = ", sMissionLocalContinuityVars.iNumRebreathersFromFreemode)
		FOR i = 0 TO ciLocalContinuityTelemetryInts_Max - 1
			PRINTLN("[JS][CONTINUITY] - RESTORE_MISSION_CONTINUITY_VARS - iLocalTelemetryInts[", i, "] = ", sMissionLocalContinuityVars.iLocalTelemetryInts[i])
		ENDFOR
		FOR i = 0 TO FMMC_MAX_VEHICLES - 1
			PRINTLN("[JS][CONTINUITY] - RESTORE_MISSION_CONTINUITY_VARS - vVehicleLocations[", i, "] = ", sMissionLocalContinuityVars.vVehicleLocations[i])
		ENDFOR
#ENDIF
	ELSE
		PRINTLN("[JS][CONTINUITY] - RESTORE_MISSION_CONTINUITY_VARS - Not on a strand mission, clearing continuity vars")
		FMMC_MISSION_CONTINUITY_VARS sEmptyStruct
		g_TransitionSessionNonResetVars.sMissionContinuityVars = sEmptyStruct
		FMMC_MISSION_LOCAL_CONTINUITY_VARS sEmptyLocalStruct
		g_TransitionSessionNonResetVars.sMissionLocalContinuityVars = sEmptyLocalStruct
		
		CLEAR_ALL_ENTITY_CHECKPOINT_CONTINUITY_VARS()
		
		//Debug Override
#IF IS_DEBUG_BUILD
		IF (g_sCasinoHeistMissionConfigData.iContinuityOverrideObjects[0] > 0
		OR g_sCasinoHeistMissionConfigData.iContinuityOverrideObjects[1] > 0)
		AND IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_ObjectTracking)
			MC_serverBD_1.sMissionContinuityVars.iObjectTrackingBitset[0] = g_sCasinoHeistMissionConfigData.iContinuityOverrideObjects[0]
			MC_serverBD_1.sMissionContinuityVars.iObjectTrackingBitset[1] = g_sCasinoHeistMissionConfigData.iContinuityOverrideObjects[1]
			PRINTLN("[JS][CONTINUITY] - RESTORE_MISSION_CONTINUITY_VARS - DEBUG - iObjectTrackingBitset[0] = ", MC_serverBD_1.sMissionContinuityVars.iObjectTrackingBitset[0])
			PRINTLN("[JS][CONTINUITY] - RESTORE_MISSION_CONTINUITY_VARS - DEBUG - iObjectTrackingBitset[1] = ", MC_serverBD_1.sMissionContinuityVars.iObjectTrackingBitset[1])
		ENDIF
		
		IF g_sCasinoHeistMissionConfigData.iContinuityOverrideLocations > 0
		AND IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_LocationTracking)
			MC_serverBD_1.sMissionContinuityVars.iLocationTrackingBitset = g_sCasinoHeistMissionConfigData.iContinuityOverrideLocations
			PRINTLN("[JS][CONTINUITY] - RESTORE_MISSION_CONTINUITY_VARS - DEBUG - iLocationTrackingBitset = ", MC_serverBD_1.sMissionContinuityVars.iLocationTrackingBitset)
		ENDIF
		
#ENDIF
	ENDIF

ENDPROC

PROC SET_MISSION_INIT_COMPLETE_GLOBAL()
	IF MC_playerBD[iLocalPart].iGameState >= GAME_STATE_INI
	AND MC_playerBD[iLocalPart].iGameState <= GAME_STATE_RUNNING
		IF NOT (g_bMissionInitComplete)
			g_bMissionInitComplete = TRUE 
			PRINTLN("g_bMissionInitComplete = TRUE")
		ENDIF
	ELSE
		IF (g_bMissionInitComplete)
			g_bMissionInitComplete = FALSE 
			PRINTLN("g_bMissionInitComplete = FALSE")
		ENDIF
	ENDIF
ENDPROC

PROC MANAGE_SPECTATOR_INIT()

	INT iteam,iRule,i
	
	g_iMissionPlayerLeftBS = 0
	
	IF NOT IS_BIT_SET(iLocalBoolCheck9, LBOOL9_SCTV_LOADDED_FMMC_BLOCK)
	AND (bIsSCTV OR DID_I_JOIN_MISSION_AS_SPECTATOR())
		REQUEST_ADDITIONAL_TEXT("FMMC", ODDJOB_TEXT_SLOT)
		IF HAS_THIS_ADDITIONAL_TEXT_LOADED("FMMC", ODDJOB_TEXT_SLOT)
			SET_BIT(iLocalBoolCheck9, LBOOL9_SCTV_LOADDED_FMMC_BLOCK)
		ENDIF
	ENDIF
	
	IF NOT (bIsSCTV OR DID_I_JOIN_MISSION_AS_SPECTATOR()) 
		SET_BIT( iLocalBoolCheck, CHECK_SPEC_STATUS )
		PRINTLN("NEITHER: IS_PLAYER_SCTV OR DID_I_JOIN_MISSION_AS_SPECTATOR is set")
	ELSE // If we're a spectator
		
		IF bIsSCTV
			IF NOT IS_BIT_SET(MPSpecGlobals.iBailBitset, GLOBAL_SPEC_BAIL_BS_RESET_STATE)
				CLEAR_BIT(MPSpecGlobals.iBitset, GLOBAL_SPEC_BS_READY_FOR_LEADERBOARD)
				SET_BIT(MPSpecGlobals.iBailBitset, GLOBAL_SPEC_BAIL_BS_RESET_STATE)
				PRINTLN("=== HUD === [2339732] MANAGE_SPECTATOR_INIT, SET_BIT(MPSpecGlobals.iBailBitset, GLOBAL_SPEC_BAIL_BS_RESET_STATE) ")
			ENDIF
		ENDIF
		
		INT iMyFutureTeam = GET_PRE_ASSIGNMENT_PLAYER_TEAM()
		
		IF IS_TRANSITION_ACTIVE()
		AND MC_ServerBD_4.iCurrentHighestPriority[ iMyFutureTeam ] < FMMC_MAX_RULES
		AND GET_CURRENT_TRANSITION_STATE() != TRANSITION_STATE_WAITING_FOR_EXTERNAL_TERMINATION_CALL
			g_bFM_ON_TEAM_MISSION = TRUE
	    	PRINTLN("[RCC MISSION] maintransition is still running. wait a bit. set g_bFM_ON_TEAM_MISSION = TRUE for the maintransition to know. " )
	    	EXIT
	    ENDIF	
		
		FIND_PREFERRED_SPECTATOR_TARGET()
		
		IF HAS_SKYCAM_BEEN_PULLED_DOWN_FOR_ROUNDS_AND_HEIST_QUICK_RESTART_SPECTATORS()  // and if it's a rounds or a QR, pull down skycam
			SET_BIT( iLocalBoolCheck, CHECK_SPEC_STATUS )
			PRINTLN("[RCC MISSION][ROUNDCAM] MANAGE_SPECTATOR_INIT, IS_THIS_A_ROUNDS_MISSION ", IS_THIS_A_ROUNDS_MISSION(), " IS_THIS_A_ROUNDS_MISSION_FOR_CORONA = ", IS_THIS_A_ROUNDS_MISSION_FOR_CORONA(), " GET_SKYSWOOP_STAGE ", GET_SKYSWOOP_STRING(GET_SKYSWOOP_STAGE()))

			PRINTLN("[RCC MISSION][ROUNDCAM] MANAGE_SPECTATOR_INIT: g_sTransitionSessionData.sMissionRoundData.iNumberOfRounds = ", g_sTransitionSessionData.sMissionRoundData.iNumberOfRounds, " ")
			
			IF NOT bIsSCTV
				SET_BIT(iLocalBoolCheck3,LBOOL3_LATE_SPECTATE)
				PRINTLN("[RCC MISSION] participant entering as LATE spectator: ",iLocalPart)
			ELSE
				SET_PLAYER_TEAM(LocalPlayer,TEAM_SCTV)
				PRINTLN("[RCC MISSION] participant entering as SCTV spectator: ",iLocalPart)
			ENDIF
			
			g_bFM_ON_TEAM_MISSION = TRUE
			g_iOverheadNamesState = TAGS_WHEN_INTEAM
			
			SET_BIT(MC_playerBD[iLocalPart].iClientBitSet,PBBOOL_ANY_SPECTATOR)
			
			CLEAR_PED_PARACHUTE_PACK_VARIATION(LocalPlayerPed)
			
			IF MC_serverBD.iNumberOfTeams = 1
			OR DO_ALL_TEAMS_LIKE_EACH_OTHER()
				g_bOnCoopMission = TRUE 
				g_bVSMission = FALSE
				PRINTLN("[RCC MISSION] g_bOnCoopMission: ",g_bOnCoopMission)
			ELSE
				g_bVSMission = TRUE
				g_bOnCoopMission = FALSE
				PRINTLN("[RCC MISSION] g_bVSMission: ",g_bVSMission)
			ENDIF
			
			CLEAR_PLAYER_WANTED_LEVEL(LocalPlayer)
			IF MC_serverBD.iPolice < POLICE_AMB_ON
				CLIENT_SET_POLICE(MC_serverBD.iPolice)
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciBLOCK_ON_FOOT_DISPATCH)
				SET_PED_MODEL_IS_SUPPRESSED(S_M_M_PrisGuard_01, TRUE)
			ENDIF
			
			IF IS_BIT_SET(MC_serverBD.iOptionsMenuBitSet, ciEMERGENCY_SERVICES_DISABLED) 
				ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, FALSE)
				ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, FALSE) 
			ENDIF 
				
			SET_BIT(MC_playerBD[iLocalPart].iClientBitSet,PBBOOL_START_SPECTATOR)
			SET_LOCAL_PLAYER_CAN_COLLECT_PORTABLE_PICKUPS(FALSE)
			SET_BIG_MESSAGE_ENABLED_WHEN_SPECTATING(TRUE) 
			SET_FMMC_PROP_MODELS_AS_NOT_NEEDED()
			RESET_CREATE_AND_ENTER_EOM_AMBIENT_VEHICLE_DATA()
			
			IF DID_I_JOIN_MISSION_AS_SPECTATOR()	
				NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE, NSPC_SET_INVISIBLE | NSPC_CLEAR_TASKS | NSPC_NO_COLLISION | NSPC_FREEZE_POSITION | NSPC_LEAVE_CAMERA_CONTROL_ON)
			ELSE
				NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
			ENDIF
			
			IF SHOULD_TRANSITION_SESSIONS_HIDE_RADAR_UNTILL_LAUNCHED()
				CLEAR_TRANSITION_SESSIONS_HIDE_RADAR_UNTILL_LAUNCHED()
			ENDIF
			
			TOGGLE_STRIPPER_AUDIO(FALSE)
			
			START_AUDIO_SCENE("MP_POSITIONED_RADIO_MUTE_SCENE")
			
			IF NOT IS_MISSION_MUSIC_BLOCKED_FOR_INTRO_CUTSCENE()
				SET_MISSION_MUSIC(g_FMMC_STRUCT.iAudioScore, DEFAULT, IS_BIT_SET(iLocalBoolCheck3,LBOOL3_LATE_SPECTATE))
			ENDIF
		
			SET_BIT(iLocalBoolCheck3, LBOOL3_MUSIC_PLAYING)
			PRINTLN("triggering music for spectator")

			FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
			
				LocalMainCutscenePlayer[iteam] = INVALID_PLAYER_INDEX()
				iCurrentHighPriority[iteam]= FMMC_PRIORITY_IGNORE
				iOldHighPriority[iteam]= FMMC_PRIORITY_IGNORE
				iNumHighPriorityLoc[iteam] = 0
				iNumHighPriorityPed[iteam] = 0
				iNumHighPriorityDeadPed[iteam] = 0
				iNumHighPriorityVeh[iteam] = 0
				iNumHighPriorityObj[iteam] = 0

				FOR iRule = 0 TO (FMMC_MAX_RULES-1)
					IF g_FMMC_STRUCT.sFMMCEndConditions[iteam].fDropOffRadius[iRule] = 0.0
						g_FMMC_STRUCT.sFMMCEndConditions[iteam].fDropOffRadius[iRule] = 10.0
					ENDIF
				ENDFOR
				
			ENDFOR
						
			FOR i = 0 TO (FMMC_MAX_VEHICLES-1)
				IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iPriority[MC_playerBD[iLocalPart].iteam] < FMMC_MAX_RULES
					IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn != DUMMY_MODEL_FOR_SCRIPT
					AND IS_MODEL_VALID(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn)
						PRINTLN("[RCC MISSION] suppressing vehicle model: ",ENUM_TO_INT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn))
						
						IF CAN_MODEL_BE_SUPPRESSED(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn)
							SET_VEHICLE_MODEL_IS_SUPPRESSED(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn,TRUE)
							IF g_iNumSupressed < MAX_NUM_SUPRESSED_MODELS
								mnSuppressedVehicles[g_iNumSupressed] = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn
							ENDIF
							VALIDATE_NUMBER_MODELS_SUPRESSED(1)
						ENDIF
						
						IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn = Technical
						AND CAN_MODEL_BE_SUPPRESSED(Rebel)
							PRINTLN("[RCC MISSION] suppressing vehicle model: ",ENUM_TO_INT(Rebel))
							SET_VEHICLE_MODEL_IS_SUPPRESSED(Rebel,TRUE)
							IF g_iNumSupressed < MAX_NUM_SUPRESSED_MODELS
								mnSuppressedVehicles[g_iNumSupressed] = Rebel
							ENDIF
							VALIDATE_NUMBER_MODELS_SUPRESSED(1)
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
			
			FOR i = 0 TO (FMMC_MAX_NUM_OBJECTS-1)
				IF IS_BIT_SET(MC_serverBD.iFragableCrate,i)
				OR IS_THIS_MODEL_A_CCTV_CAMERA(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].mn)
					IF NETWORK_DOES_NETWORK_ID_EXIST(GET_OBJECT_NET_ID(i))
						IF NOT IS_DAMAGE_TRACKER_ACTIVE_ON_NETWORK_ID(GET_OBJECT_NET_ID(i))
							ACTIVATE_DAMAGE_TRACKER_ON_NETWORK_ID(GET_OBJECT_NET_ID(i),TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
			
			//If this is a strand mission then set that we're ready for the reset
			IF HAS_FIRST_STRAND_MISSION_BEEN_PASSED()
				CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Calling SET_MISSION_CONTROLLER_READY_FOR_RESET for spectators!" )
				SET_MISSION_CONTROLLER_READY_FOR_RESET()
			ENDIF
						
			FIND_PREFERRED_SPECTATOR_TARGET()
			
			ACTIVATE_KILL_TICKERS(TRUE)
			
			SET_BIT(MC_playerBD[iLocalPart].iClientBitSet,PBBOOL_ALL_HEADSHOTS)
		
			SET_BIT(iLocalBoolCheck2,LBOOL2_WHOLE_UPDATE_DONE)

			SET_IGNORE_NO_GPS_FLAG_UNTIL_FIRST_NORMAL_NODE(TRUE)
			CLEANUP_ALL_CORONA_FX(TRUE)
			SET_SKYFREEZE_CLEAR()
			DO_CORONA_POLICE_SETTINGS()
			MC_SET_ALL_MISSION_ENTITIES_VISIBLITY(TRUE)
			SET_HIDOF_OVERRIDE(FALSE,FALSE,0,0,0,0)
			IF ANIMPOSTFX_IS_RUNNING("MP_job_load")
				ANIMPOSTFX_STOP("MP_job_load")
			ENDIF
			CHECK_EMERGENCY_CLEANUP()
			
			APPLY_OUTFIT_TO_LOCAL_PLAYER(INT_TO_ENUM(MP_OUTFIT_ENUM, GET_LOCAL_DEFAULT_OUTFIT(MC_playerBD[iLocalPart].iTeam)))
			BROADCAST_GENERAL_EVENT(GENERAL_EVENT_TYPE_PLAYER_CHANGED_APPEARANCE, ALL_PLAYERS())
			PRINTLN("[RCC MISSION] MANAGE_SPECTATOR_INIT - Applying default outfit for spectator.")
			
			SET_UP_DESPAWN_PROPS()
			
			g_TransitionSessionNonResetVars.mnArenaVehicleChosen = DUMMY_MODEL_FOR_SCRIPT
			PRINTLN("[RCC MISSION] MANAGE_SPECTATOR_INIT - Clearing g_TransitionSessionNonResetVars.mnArenaVehicleChosen")
			SET_PLAYER_SELECTING_CUSTOM_VEHICLE(FALSE, TRUE)			
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_CLIENT_MOVE_TO_INTRO_CUTSCENE()
	IF IS_FAKE_MULTIPLAYER_MODE_SET() 
	AND NOT ALLOW_INTRO_IN_FAKE_MULTIPLAYER_MODE()
		RETURN FALSE
	ENDIF
	
	IF (HAS_FIRST_STRAND_MISSION_BEEN_PASSED() AND HAS_FIRST_STRAND_MISSION_BEEN_PASSED_NO_RESET())
		RETURN FALSE
	ENDIF
	
	IF NOT IS_THIS_THE_FIRST_ROUND_OR_NOT_A_ROUNDS_MISSION()
		RETURN FALSE
	ENDIF
	
	IF IS_THIS_A_QUICK_RESTART_JOB()
		IF NOT(IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciENABLE_START_IN_CUTSCENE) AND GET_MC_TEAM_STARTING_RULE(0) = 0)
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF HAS_SERVER_TIME_PASSED_ROUND(10000)
		IF DID_I_JOIN_MISSION_AS_SPECTATOR()
		OR bIsSCTV
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    This is the function that moves the mission stage to RUNNING, INTRO_CUTSCENE etc.
PROC PROGRESS_CONTROLLER_TO_NEXT_STAGE()	
	IF NOT IS_BIT_SET(MC_PlayerBD[iLocalPart].iClientBitset, PBBOOL_CLIENT_STATE_INITIALISED)
		IF SHOULD_CLIENT_MOVE_TO_INTRO_CUTSCENE()
			
			IF IS_THIS_A_VERSUS_MISSION()
				CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] [SCRIPT INIT] Trying team intro cutscene")
				SET_BIT( iLocalBoolCheck3,  LBOOL3_TEAM_INTRO ) 
			ENDIF
			
			CLEAR_BIT(GlobalplayerBD_FM_2[NATIVE_TO_INT(LocalPlayer)].iMissionDataBitSetTwo, ciMISSION_DATA_MISSION_SCRIPT_CLEANED_UP)
			
			// Included here to prevent the 1 frame gap where we don't see the entities.
			// url:bugstar:4151361 - Riot Van - The Riot Van visibly popped into view when the mission began.
			MC_SET_ALL_MISSION_ENTITIES_VISIBLITY(TRUE)

			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] [SCRIPT INIT] - Moving to Intro Cutscene")
			
			IF g_bStartedInAirlockTransition
				PRINTLN("[MSRAND] [SCRIPT INIT] - Clearing g_bStartedInAirlockTransition (1)")
				g_bStartedInAirlockTransition = FALSE
				ENABLE_INTERACTION_MENU()
			ENDIF
			
			IF g_bStartedInFadeTransition
				PRINTLN("[MSRAND] [SCRIPT INIT] - Clearing g_bStartedInFadeTransition (1)")
				g_bStartedInFadeTransition = FALSE
			ENDIF
			
			SET_BIT(MC_PlayerBD[iLocalPart].iClientBitset, PBBOOL_CLIENT_STATE_INITIALISED)
		ELSE
		
			SET_PLAYER_INTO_COVER_AFTER_INITIAL_SPAWNING()
			
			IF (IS_THIS_A_QUICK_RESTART_JOB() OR IS_CORONA_INITIALISING_A_QUICK_RESTART())
				PROCESS_LOCAL_PLAYER_INTRO_GAIT()
			ENDIF
			
			IF SET_SKYSWOOP_DOWN(DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, g_bStartedInAirlockTransition)
				CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] [SCRIPT INIT] - Moving straight into running state")
				DO_CORONA_POLICE_SETTINGS()
				MC_SET_ALL_MISSION_ENTITIES_VISIBLITY(TRUE)
				DO_SCREEN_FADE_IN(500)
				
				IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_PLAYER_OUT_OF_LIVES)
					//Player is rejoining early end spectate from a previous strand
					NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
				ENDIF
				
				IF SHOULD_TRANSITION_SESSIONS_HIDE_RADAR_UNTILL_LAUNCHED()
					CLEAR_TRANSITION_SESSIONS_HIDE_RADAR_UNTILL_LAUNCHED()
				ENDIF
				ANIMPOSTFX_STOP("MP_job_load")
				CLEANUP_ALL_CORONA_FX(TRUE)
				SET_SKYFREEZE_CLEAR()
				SET_HIDOF_OVERRIDE(FALSE,FALSE,0,0,0,0)
				SETUP_TEAM_CHAT_AND_TARGETTING_RESTRICTIONS()
				CHECK_EMERGENCY_CLEANUP()
				
				IF IS_BIT_SET(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_SPAWN_IN_PLACED_VEHICLE)
				AND IS_FAKE_MULTIPLAYER_MODE_SET()		
					IF NOT IS_BIT_SET( iLocalBoolCheck5, LBOOL5_GIVEN_SPAWN_VEHICLE_STARTING_VELOCITY )
						UNFREEZE_AND_PUNT_PLAYER_SPAWN_VEHICLE_FORWARDS( 60.0 )
						SET_BIT( iLocalBoolCheck5, LBOOL5_GIVEN_SPAWN_VEHICLE_STARTING_VELOCITY )
					ENDIF
				ENDIF
				
				IF g_bStartedInAirlockTransition
					PRINTLN("[MSRAND] [SCRIPT INIT] - Clearing g_bStartedInAirlockTransition (2)")
					g_bStartedInAirlockTransition = FALSE
					ENABLE_INTERACTION_MENU()
				ENDIF
				
				IF g_bStartedInFadeTransition
					PRINTLN("[MSRAND] [SCRIPT INIT] - Clearing g_bStartedInFadeTransition (2)")
					g_bStartedInFadeTransition = FALSE
				ENDIF			
				SET_START_POSITION_WARPING_VEHICLE_ON_GROUND_PROPERLY(TRUE)
				SET_BIT(MC_PlayerBD[iLocalPart].iClientBitset, PBBOOL_CLIENT_STATE_INITIALISED)
			ELSE
				CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] [SCRIPT INIT] - waiting on skyswoop down before moving into running state. ")
			ENDIF
		ENDIF
		IF IS_BIT_SET(MC_PlayerBD[iLocalPart].iClientBitset, PBBOOL_CLIENT_STATE_INITIALISED)
			PROCESS_START_OF_MISSION_TELEMETRY()
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_RESTORE_GRABBED_CASH()
	
	IF NOT (IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_BagContentsTracking) AND sMissionLocalContinuityVars.sLootBag.iCashGrabbed != 0)
		EXIT
	ENDIF
	
	IF sMissionLocalContinuityVars.sLootBag.iCashGrabbed != 0
		MC_playerBD[iLocalPart].sLootBag.iCashGrabbed = sMissionLocalContinuityVars.sLootBag.iCashGrabbed
		g_TransitionSessionNonResetVars.bInvolvedInCashGrab = TRUE
		PRINTLN("PROCESS_RESTORE_GRABBED_CASH - Cash restored from sMissionLocalContinuityVars.sLootBag.iCashGrabbed: ", MC_playerBD[iLocalPart].sLootBag.iCashGrabbed)
	ENDIF
	IF sMissionLocalContinuityVars.sLootBag.iTotalCashGrabbed != 0
		MC_playerBD[iLocalPart].sLootBag.iTotalCashGrabbed = sMissionLocalContinuityVars.sLootBag.iTotalCashGrabbed
		PRINTLN("PROCESS_RESTORE_GRABBED_CASH - Cash restored from sMissionLocalContinuityVars.sLootBag.iTotalCashGrabbed: ", MC_playerBD[iLocalPart].sLootBag.iTotalCashGrabbed)
	ENDIF
	IF sMissionLocalContinuityVars.sLootBag.iStolenGoods != 0
		MC_playerBD[iLocalPart].sLootBag.iStolenGoods = sMissionLocalContinuityVars.sLootBag.iStolenGoods
		PRINTLN("PROCESS_RESTORE_GRABBED_CASH - Stolen goods restored from sMissionLocalContinuityVars.sLootBag.iStolenGoods: ", MC_playerBD[iLocalPart].sLootBag.iStolenGoods)
		MC_playerBD[iLocalPart].sLootBag.iLootBitSet = sMissionLocalContinuityVars.sLootBag.iLootBitSet
		PRINTLN("PROCESS_RESTORE_GRABBED_CASH - Cash restored from sMissionLocalContinuityVars.sLootBag.iLootBitSet: ", MC_playerBD[iLocalPart].sLootBag.iLootBitSet)
	ENDIF
	IF sMissionLocalContinuityVars.sLootBag.fCurrentBagCapacity != 0
		MC_playerBD[iLocalPart].sLootBag.fCurrentBagCapacity = sMissionLocalContinuityVars.sLootBag.fCurrentBagCapacity
		PRINTLN("PROCESS_RESTORE_GRABBED_CASH - Capacity restored from sMissionLocalContinuityVars.sLootBag.fCurrentBagCapacity: ", MC_playerBD[iLocalPart].sLootBag.fCurrentBagCapacity)
		INT i
		FOR i = 0 TO ciLOOT_TYPE_MAX - 1
			MC_playerBD[iLocalPart].sLootBag.fCurrentCapacity[i] = sMissionLocalContinuityVars.sLootBag.fCurrentCapacity[i]
			PRINTLN("PROCESS_RESTORE_GRABBED_CASH - Capacity restored from sMissionLocalContinuityVars.sLootBag.fCurrentCapacity[",i,"]: ", MC_playerBD[iLocalPart].sLootBag.fCurrentCapacity[i])
		ENDFOR
	ENDIF
	
	IF IS_THIS_A_QUICK_RESTART_JOB()
	AND MC_playerBD[iLocalPart].sLootBag.iCashGrabbed != 0
		REMOVE_BAG_CONTENTS_ON_RESPAWN()
	ENDIF
ENDPROC

// FMMC2020 Refactor - Pull all the playerbd being set to 0 or -1 into this function so it's all handled in one place.
PROC INITIALIZE_PLAYER_BROADCAST_DATA()
	INT iCompanionIndex = 0
	FOR iCompanionIndex = 0 TO FMMC_MAX_COMPANIONS-1
		MC_playerBD_1[iLocalPart].sPedCompanionTaskData[iCompanionIndex].targetEntityFriend = INVALID_PLAYER_INDEX()
	ENDFOR
ENDPROC

/// PURPOSE:
///    Called once, before we get into RUNNING or one of the INTRO CUTSCENEs
///    For one-off initialisation flags that need to be set on the player
///    among a variety of other things
PROC MANAGE_LOCAL_PLAYER_INIT()
	
	INITIALIZE_PLAYER_BROADCAST_DATA()
	
	#IF IS_DEBUG_BUILD 
	IF bIdleKickPrints
		g_B_IdleKickPrints = TRUE
	ENDIF
	#ENDIF
	
	INT iteam, iRule, i

	INT iTeamSlot
	FOR iTeamSlot = 0 TO FMMC_MAX_TEAMS-1
		FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[iTeamSlot]-1
			IF g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeamSlot][i].iActivationTeam > -2
				SET_BIT(iLocalBoolCheck28, LBOOL28_CAN_TEAMS_ACTIVATE_OTHER_TEAMS_RESPAWN_POINTS)
			ENDIF
		ENDFOR
	ENDFOR
		
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciOPTION_VEHICLE_WEAPON_ENABLED)
		PRINTLN("[RCC MISSION] MANAGE_LOCAL_PLAYER_INIT - ciOPTION_VEHICLE_WEAPON_ENABLED - SET_INVERT_GHOSTING")
		SET_INVERT_GHOSTING(TRUE)
	ENDIF
	
	IF NOT IS_FAKE_MULTIPLAYER_MODE_SET(TRUE)
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[GET_PRE_ASSIGNMENT_PLAYER_TEAM()].iTeamBitset2, ciBS2_KILL_PLAYER_IN_WATER)
			PRINTLN("[RCC MISSION] Player dies instantly in water bitset is set.")
			IF bLocalPlayerPedOK
				PRINTLN("[RCC MISSION] Setting player dies instantly in water.")
				SET_PED_DIES_INSTANTLY_IN_WATER(LocalPlayerPed, TRUE)
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iTeamBitSet3, ciBS3_ENABLE_MULTIPLE_PLAYER_SAME_VEHICLE_RESPAWN)
		PRINTLN("[RCC MISSION] MANAGE_LOCAL_PLAYER_INIT - Setting SET_ON_TEAM_RACE_RACE_GLOBAL")
		SET_ON_TEAM_RACE_RACE_GLOBAL(TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTen, ciDISABLE_RADIO)
		SET_RADIO_TO_STATION_INDEX(255)
		IF IS_PED_IN_ANY_VEHICLE(localPlayerPed)
		AND NETWORK_HAS_CONTROL_OF_ENTITY(GET_VEHICLE_PED_IS_IN(localPlayerPed))
			PRINTLN("[RCC MISSION][INIT] Disabling Radio ")
			SET_VEHICLE_RADIO_ENABLED(GET_VEHICLE_PED_IS_IN(localPlayerPed), FALSE)
		ENDIF		
		g_s_PlayersRadioStation = ""
	ENDIF

	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iLocalPart].iTeam].iTeamBitSet2, ciBS2_ALLOW_INSTANT_RESPAWN_ON_DEATH)			
		g_bMissionHideRespawnBar = TRUE
		g_bMissionInstantRespawn = TRUE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySix, ciOptionsBS26_HideRespawnBar)
		g_bMissionHideRespawnBar = TRUE
	ENDIF
	
	IF sMissionLocalContinuityVars.eEndVehicleSeat != VS_DRIVER
	AND IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_VehicleSeatTracking)
		SET_PED_CONFIG_FLAG(localPlayerPed, PCF_PreventAutoShuffleToDriversSeat, TRUE)
	ENDIF
		
	IF IS_THIS_A_QUICK_RESTART_JOB() OR IS_PLAYER_USING_SEQUENTIAL_SPAWNING_PROCEDURES()
		CLEAR_ROOM_AFTER_START_POSITION_WARP()
	ENDIF
	
	IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_PLAYER_OUT_OF_LIVES) //Player is rejoining early end spectate from a previous strand
		IF NOT IS_ENTITY_VISIBLE(LocalPlayerPed)
			SET_ENTITY_VISIBLE(LocalPlayerPed, TRUE)
		ENDIF
	ENDIF
	
	IF IS_PLAYER_USING_SEQUENTIAL_SPAWNING_PROCEDURES()
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed, TRUE)
			VEHICLE_INDEX vehTemp = GET_VEHICLE_PED_IS_IN(LocalPlayerPed, TRUE)
			IF IS_VEHICLE_DRIVEABLE(vehTemp)
				IF NETWORK_HAS_CONTROL_OF_ENTITY(vehTemp)
					SET_ENTITY_VISIBLE(vehTemp, TRUE)
					PRINTLN("[RCC MISSION] MANAGE_LOCAL_PLAYER_INIT - Setting Vehicle Visible - Avoiding mid-migration visibility issues.")
				ELSE
					PRINTLN("[RCC MISSION] MANAGE_LOCAL_PLAYER_INIT - Can't Set Vehicle Visible - Do not have control.")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(LocalPlayer)].iMissionModeBit, ciMISSION_USING_ORBITAL_CANNON)

	INIT_HACKING_DATA(sHackingData)
		
	INVALIDATE_CINEMATIC_VEHICLE_IDLE_MODE()
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciALLOW_UNDERWATER_DIALOGUE)
		ALLOW_DIALOGUE_IN_WATER(TRUE)
	ENDIF
	
	INVALIDATE_IDLE_CAM() //url:bugstar:4151498
	
	IF STRAND_MISSION_SHOULD_SKIP_INTRO_IN_MC()
		SET_GAMEPLAY_CAM_RELATIVE_HEADING()
		SET_GAMEPLAY_CAM_RELATIVE_PITCH()
	ENDIF

	IF g_sFMMCEOM.bQuickRestartPVWarp	
		REQUEST_PERSONAL_VEHICLE_TO_WARP_NEAR()
		g_sFMMCEOM.bQuickRestartPVWarp = FALSE
	ENDIF
		
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyThree, ciOptionsBS23_DisablePlayerPropertiesInMission)
		INT iMaxProperties = ENUM_TO_INT(PPAG_ARENA_GARAGE)
		INT iProperty
		FOR iProperty = 0 TO iMaxProperties
			IF INT_TO_ENUM(PREVENT_PROPERTY_ACCESS_FLAGS, iProperty) != PPAF_FREEMODE_EVENT_BEAST
			AND INT_TO_ENUM(PREVENT_PROPERTY_ACCESS_FLAGS, iProperty) != PPAF_GB_HUNT_THE_BOSS
			AND INT_TO_ENUM(PREVENT_PROPERTY_ACCESS_FLAGS, iProperty) != PPAF_GB_BOSS_V_BOSS_DM
				PRINTLN("[RCC MISSION] MANAGE_LOCAL_PLAYER_INIT - SET_PLAYER_BLOCK_FROM_PROPERTY_FLAG with iProperty: ", iProperty)
				SET_PLAYER_BLOCK_FROM_PROPERTY_FLAG(INT_TO_ENUM(PREVENT_PROPERTY_ACCESS_FLAGS, iProperty))
			ENDIF
		ENDFOR
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iLocalPart].iteam].iTeamBitset2, ciBS2_BLOCK_WEAPON_SWAP_ON_PICKUP)
		SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_BlockAutoSwapOnWeaponPickups, TRUE)
		PRINTLN( "[RCC MISSION] MANAGE_LOCAL_PLAYER_INIT - Block auto Weapon Swap when collecting pickups. setting PCF_BlockAutoSwapOnWeaponPickups" )
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iLocalPart].iteam].iTeamBitset3, ciBS3_PREFER_FRONT_SEATS)
		SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_PlayerPreferFrontSeatMP, TRUE)
		PRINTLN("[RCC MISSION] MANAGE_LOCAL_PLAYER_INIT - Setting PCF_PlayerPreferFrontSeatMP due to ciBS3_PREFER_FRONT_SEATS")
	ENDIF
	
	#IF IS_DEBUG_BUILD 
	IF bAllowTripSkipNoPlay
		PRINTLN( "[RCC MISSION] MANAGE_LOCAL_PLAYER_INIT - sc_AllowTripSkipNoPlay is being used.")
	ENDIF
	#ENDIF
	
	FOR i = 0 TO FMMC_MAX_GO_TO_LOCATIONS-1
		iColourTeamCacheGotoLocateOld[i] = -1
	ENDFOR
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_EXPEDITE_AND_DONT_REVALIDATE_DIAG_TRIGGERS)
		INT iDiag
		REPEAT g_FMMC_STRUCT.iNumberOfDialogueTriggers iDiag
			IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDiag].iDialogueBitset3, iBS_Dialogue3_ExpediteAndDontRevalidate)
				PRINTLN("[RCC MISSION] MANAGE_LOCAL_PLAYER_INIT Setting LBOOL28_EXPEDITE_DIALOGUE_TRIGGERS_ENABLED ON! for iDialogueProgress: ", iDiag)
				SET_BIT(iLocalBoolCheck28, LBOOL28_EXPEDITE_DIALOGUE_TRIGGERS_ENABLED)
				BREAKLOOP
			ENDIF
		ENDREPEAT
	ELSE
		PRINTLN("[RCC MISSION] MANAGE_LOCAL_PLAYER_INIT Setting LBOOL28_EXPEDITE_DIALOGUE_TRIGGERS_ENABLED ON!")
		SET_BIT(iLocalBoolCheck28, LBOOL28_EXPEDITE_DIALOGUE_TRIGGERS_ENABLED)
	ENDIF
	
	IF IS_LOADED_MISSION_TYPE_FLOW_MISSION()
		PRINTLN("[RCC MISSION] MANAGE_LOCAL_PLAYER_INIT - SETTING CAR MOD SHOP IMMUNE TO AGGRO PEDS")
		SET_SHOP_IGNORES_KICKING_OFF_CHECKS(CARMOD_SHOP_01_AP, TRUE)
		SET_SHOP_IGNORES_KICKING_OFF_CHECKS(CARMOD_SHOP_05_ID2, TRUE)
		SET_SHOP_IGNORES_KICKING_OFF_CHECKS(CARMOD_SHOP_06_BT1, TRUE)
		SET_SHOP_IGNORES_KICKING_OFF_CHECKS(CARMOD_SHOP_07_CS1, TRUE)
		SET_SHOP_IGNORES_KICKING_OFF_CHECKS(CARMOD_SHOP_08_CS6, TRUE)
		SET_SHOP_IGNORES_KICKING_OFF_CHECKS(CARMOD_SHOP_SUPERMOD, TRUE)
	ENDIF
	
	SET_LOCAL_PLAYERS_FM_TEAM_DECORATOR(MC_playerBD[iLocalPart].iteam)
										
	CLEAR_PLAYER_WANTED_LEVEL(LocalPlayer)
	
	CLEAR_PED_PARACHUTE_PACK_VARIATION(LocalPlayerPed)

	BOOL bOverrideCountermeasureAmmo = (g_FMMC_STRUCT.iTeamVehicleModCountermeasure_AmmoPerLife[g_i_Mission_team] > 0)
	SET_OVERRIDE_COUNTERMEASURE_AMMO(bOverrideCountermeasureAmmo)
	
	IF (g_FMMC_STRUCT.iTeamVehicleModCountermeasure_Cooldown[g_i_Mission_team] > 0)
		SET_OVERRIDE_FLARE_COOLDOWN(TRUE, g_FMMC_STRUCT.iTeamVehicleModCountermeasure_Cooldown[g_i_Mission_team] * 1000)
		SET_OVERRIDE_CHAFF_COOLDOWN(TRUE, g_FMMC_STRUCT.iTeamVehicleModCountermeasure_Cooldown[g_i_Mission_team] * 1000)
		PRINTLN("MANAGE_LOCAL_PLAYER_INIT - cooldown: ", g_FMMC_STRUCT.iTeamVehicleModCountermeasure_Cooldown[g_i_Mission_team], " ammo: ", g_FMMC_STRUCT.iTeamVehicleModCountermeasure_AmmoPerLife[g_i_mission_team])
	ELSE
		SET_OVERRIDE_FLARE_COOLDOWN(FALSE)
		SET_OVERRIDE_CHAFF_COOLDOWN(FALSE)
	ENDIF
	
	IF NOT IS_PLAYER_WEARING_PARACHUTE()
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciRESPAWN_WITH_PARACHUTE_EQUIPPED)
			GET_PLAYER_PARACHUTE_SMOKE_TRAIL_COLOR(LocalPlayer, iCachedParachuteSmokeRValue, iCachedParachuteSmokeGValue, iCachedParachuteSmokeBValue)
			MPGlobalsAmbience.bDisableTakeOffChute = TRUE
			EQUIP_STORED_MP_PARACHUTE(LocalPlayer, g_FMMC_STRUCT.iTeamParachuteTint[g_i_Mission_team], g_FMMC_STRUCT.iTeamParachutePackTint[g_i_Mission_team])
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciENABLE_VEHICLE_TARGETTING_RETICULE)
		USE_VEHICLE_TARGETING_RETICULE(TRUE)
		PRINTLN("[RCC MISSION] MANAGE_LOCAL_PLAYER_INIT - USE_VEHICLE_TARGETING_RETICULE(TRUE)")
	ENDIF
	
	SET_PED_ABLE_TO_DROWN( LocalPlayerPed )
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciCLOBBER_PLAYER_ON_TEAM_SWAP)
		g_ForceRedWastedKillerShard = TRUE
	ENDIF
	
	// 1910990
	IF NOT IS_BIT_SET(iLocalBoolCheck8, LBOOL8_CLEARED_HEADSHOTS)
		Clear_All_Generated_MP_Headshots(FALSE)
		PRINTLN("[RCC MISSION] MANAGE_LOCAL_PLAYER_INIT - LBOOL8_CLEARED_HEADSHOTS ")
		SET_BIT(iLocalBoolCheck8, LBOOL8_CLEARED_HEADSHOTS)
	ENDIF

	BYPASS_CELLPHONE_CAMERA_DEFAULT_SAVE_ROUTINE(TRUE)
	
	SET_CHECKPOINT_ANIM_GLOBAL(jobIntroData)  // Intro/Restarts
	
	SET_PLAYER_WILL_SPAWN_USING_SAME_NODE_TYPE_AS_TARGET(IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciRESPAWN_USING_SAME_NODE_TYPE))
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciDISABLE_NAVMESH_FALLBACK)
		SET_PLAYER_VEHICLE_SPAWNING_CAN_USE_NAVMESH_FALLBACK(FALSE)
	ELSE
		SET_PLAYER_VEHICLE_SPAWNING_CAN_USE_NAVMESH_FALLBACK(TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciMUTE_AMBIENT_CITY_SOUNDS)
		IF NOT IS_AUDIO_SCENE_ACTIVE("GTAO_Mute_Traffic_Ambience_Scene")
			START_AUDIO_SCENE("GTAO_Mute_Traffic_Ambience_Scene")
			PRINTLN("[LM][RCC MISSION] MANAGE_LOCAL_PLAYER_INIT - - START_AUDIO_SCENE: GTAO_Mute_Traffic_Ambience_Scene")
		ENDIF
	ENDIF
	
	IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
	AND DOES_CONTENT_REQUIRE_HEIST_POSIX_AND_HASHED_MAC()
		IF NETWORK_IS_HOST() //Odd check but has worked for years...
			PRINTLN("[RCC MISSION] MANAGE_LOCAL_PLAYER_INIT - SEND_EVENT_HEIST_POSIX_AND_HASHED_MAC")
			SEND_EVENT_HEIST_POSIX_AND_HASHED_MAC()
		ENDIF
	ENDIF		
	
	SET_ALL_CLIENTS_RECEIVE_INVALID_WEAPON_DAMAGE_EVENTS()
	
	iStartSuicides = GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].iSuicides
	
	//GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].g_bInMissionCreatorApartment = FALSE
	CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_g_bInMissionCreatorApartment)
	
	IF NOT bIsAnySpectator 
		SET_PLAYER_TEAM(LocalPlayer,MC_playerBD[iLocalPart].iteam)
		SETUP_TEAM_CHAT_AND_TARGETTING_RESTRICTIONS() // url:bugstar:2211980
		PRINTLN( "[RCC MISSION] MANAGE_LOCAL_PLAYER_INIT - SET_PLAYER_TEAM = ", MC_playerBD[iLocalPart].iteam )
	ELSE
		SET_SPECTATOR_RUNNING_CUTSCENE(FALSE)
	ENDIF
	
	SET_PED_RELATIONSHIP_GROUP_HASH( LocalPlayerPed, rgfm_PlayerTeam[ MC_playerBD[iLocalPart].iteam ] )		
	relMyFmGroup = GET_PED_RELATIONSHIP_GROUP_HASH(LocalPlayerPed)
		
	SET_LOCAL_PLAYER_CAN_COLLECT_PORTABLE_PICKUPS(TRUE)
	
	NETWORK_SET_FRIENDLY_FIRE_OPTION(FALSE)

	PRINTLN("MANAGE_LOCAL_PLAYER_INIT - g_FMMC_STRUCT.iTeamHealth[MC_playerBD[iLocalPart].iteam] = ",g_FMMC_STRUCT.iTeamHealth[MC_playerBD[iLocalPart].iteam])
	
	IF NOT IS_FAKE_MULTIPLAYER_MODE_SET(TRUE)
		IF NOT (IS_AN_AIRLOCK_STRAND_TRANSITION_RUNNING() OR g_bStartedInAirlockTransition)
			IF g_FMMC_STRUCT.iTeamHealth[MC_playerBD[iLocalPart].iteam] = ciTEAM_HEALTH_25
				GIVE_PLAYER_PROP_UNLOCKED_HEALTH_FREEMODE(0.25)
			ELIF g_FMMC_STRUCT.iTeamHealth[MC_playerBD[iLocalPart].iteam] = ciTEAM_HEALTH_50
				GIVE_PLAYER_PROP_UNLOCKED_HEALTH_FREEMODE(0.5)
			ELIF g_FMMC_STRUCT.iTeamHealth[MC_playerBD[iLocalPart].iteam] = ciTEAM_HEALTH_75
				GIVE_PLAYER_PROP_UNLOCKED_HEALTH_FREEMODE(0.75)
			ELIF g_FMMC_STRUCT.iTeamHealth[MC_playerBD[iLocalPart].iteam] =  ciTEAM_HEALTH_FULL 
				GIVE_PLAYER_PROP_UNLOCKED_HEALTH_FREEMODE(1.0)
			ENDIF
		ENDIF
		CLEAR_ADDITIONAL_TEXT(MISSION_TEXT_SLOT,TRUE)		
		CLEAR_ADDITIONAL_TEXT(MINIGAME_TEXT_SLOT,TRUE)
		CLEAR_ADDITIONAL_TEXT(DLC_MISSION_DIALOGUE_TEXT_SLOT,TRUE)
		CLEAR_ADDITIONAL_TEXT(DLC_MISSION_DIALOGUE_TEXT_SLOT2,TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciSTARTING_TEAM_INVENTORY_CLEAR_BODY_ARMOUR)
		SET_PED_ARMOUR(LocalPlayerPed, 0)
	ENDIF

	IF MC_serverBD.iNumberOfTeams = 1
	OR DO_ALL_TEAMS_LIKE_EACH_OTHER()	
		g_bOnCoopMission = TRUE 
		g_bVSMission = FALSE
		PRINTLN("[RCC MISSION] MANAGE_LOCAL_PLAYER_INIT - g_bOnCoopMission: ",g_bOnCoopMission)
	ELSE
		g_bVSMission = TRUE
		g_bOnCoopMission = FALSE
		SET_IDLE_KICK_TIME_OVERRIDDEN(180000)
		PRINTLN("[RCC MISSION] MANAGE_LOCAL_PLAYER_INIT - SET_IDLE_KICK_TIME_OVERRIDDEN: ",180000)
		PRINTLN("[RCC MISSION] MANAGE_LOCAL_PLAYER_INIT - g_bVSMission: ",g_bVSMission)
	ENDIF
	
	SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_WillFlyThroughWindscreen, FALSE) 
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].iTeamBitset3, ciBS3_TEAM_DISABLE_HELMET_ARMOUR_FROM_OUTFIT)
		PRINTLN("[RCC MISSION] MANAGE_LOCAL_PLAYER_INIT - ciBS3_TEAM_DISABLE_HELMET_ARMOUR_FROM_OUTFIT is set. Calling: PCF_DisableHelmetArmor")
		SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_DisableHelmetArmor, TRUE)
	ENDIF
	
	IF IS_HELP_MESSAGE_BEING_DISPLAYED()
		CLEAR_HELP()
	ENDIF
	CLEAR_PRINTS()
	CLEAR_ALL_FLOATING_HELP()
	
	//Bug 1921219
	IF IS_BIT_SET( g_FMMC_STRUCT.iOptionsMenuBitSet, ciSPAWN_INTO_VEH ) 
	AND ( IS_THIS_MODEL_A_PLANE(g_FMMC_STRUCT.mnVehicleModel[ MC_playerBD[iLocalPart].iteam ]) )
		PRINTLN("[RCC MISSION] MANAGE_LOCAL_PLAYER_INIT - Spawning into a plane, so reducing the Hint Cam interp time")
		sHintCam.iInterpOutLength = 500
	ENDIF

	IF IS_BIT_SET(MC_serverBD.iOptionsMenuBitSet, ciEMERGENCY_SERVICES_DISABLED) 
	 	ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, FALSE) 
	ENDIF 
	
	IF IS_MP_HEIST_GEAR_EQUIPPED(LocalPlayerPed, HEIST_GEAR_REBREATHER)
		PRINTLN("[RCC MISSION] MANAGE_LOCAL_PLAYER_INIT - Set to use non heist rebreathers during this mission and come from freemode with one on. Removing this as it will break MC rebreathers.")
		REMOVE_MP_HEIST_GEAR(LocalPlayerPed, HEIST_GEAR_REBREATHER)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciALLOW_NON_HEIST_REBREATHERS)
		IF GET_MP_INT_CHARACTER_STAT(MP_STAT_BREATHING_APPAR_BOUGHT) > 0
			sMissionLocalContinuityVars.iNumRebreathersFromFreemode = GET_MP_INT_CHARACTER_STAT(MP_STAT_BREATHING_APPAR_BOUGHT)
			PRINTLN("[RCC MISSION] MANAGE_LOCAL_PLAYER_INIT - iNumRebreathersFromFreemode: ", sMissionLocalContinuityVars.iNumRebreathersFromFreemode)
		ENDIF
		g_bResetRebreatherAir = TRUE
	ENDIF
	
	IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iTeam] < FMMC_MAX_RULES
		IF g_FMMC_STRUCT.iPerRuleVehicleRockets[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iTeam]][MC_playerBD[iLocalPart].iTeam] != 0
			iVehicleRockets = g_FMMC_STRUCT.iPerRuleVehicleRockets[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iTeam]][MC_playerBD[iLocalPart].iTeam]
			SET_BIT(iLocalBoolCheck15, LBOOL15_VEHICLE_MENU_WEAPON_ACTIVE_ROCKETS)
		ENDIF
		IF g_FMMC_STRUCT.iPerRuleVehicleBoost[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iTeam]][MC_playerBD[iLocalPart].iTeam] != 0
			iVehicleBoost = g_FMMC_STRUCT.iPerRuleVehicleBoost[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iTeam]][MC_playerBD[iLocalPart].iTeam]
			SET_BIT(iLocalBoolCheck15, LBOOL15_VEHICLE_MENU_WEAPON_ACTIVE_BOOST)
		ENDIF
		IF g_FMMC_STRUCT.iPerRuleVehicleRepair[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iTeam]][MC_playerBD[iLocalPart].iTeam] != 0
			iVehicleRepair = g_FMMC_STRUCT.iPerRuleVehicleRepair[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iTeam]][MC_playerBD[iLocalPart].iTeam]
			SET_BIT(iLocalBoolCheck15, LBOOL15_VEHICLE_MENU_WEAPON_ACTIVE_REPAIR)
		ENDIF
		IF g_FMMC_STRUCT.iPerRuleVehicleSpikes[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iTeam]][MC_playerBD[iLocalPart].iTeam] != 0
			iVehicleSpikes = g_FMMC_STRUCT.iPerRuleVehicleSpikes[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iTeam]][MC_playerBD[iLocalPart].iTeam]
			SET_BIT(iLocalBoolCheck15, LBOOL15_VEHICLE_MENU_WEAPON_ACTIVE_SPIKES)
		ENDIF
	ENDIF
	PRINTLN("[RCC MISSION] MANAGE_LOCAL_PLAYER_INIT - - Init iVehicleRockets = ", iVehicleRockets)
	PRINTLN("[RCC MISSION] MANAGE_LOCAL_PLAYER_INIT - - Init iVehicleBoost = ", iVehicleBoost)
	PRINTLN("[RCC MISSION] MANAGE_LOCAL_PLAYER_INIT - - Init iVehicleRepair = ", iVehicleRepair)
	PRINTLN("[RCC MISSION] MANAGE_LOCAL_PLAYER_INIT - - Init iVehicleSpikes = ", iVehicleSpikes)
	
	FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
	
		iPartForTeamHealthBar[iteam] = -1
	
		LocalMainCutscenePlayer[iteam] = INVALID_PLAYER_INDEX()
		
		iCurrentHighPriority[iteam]= FMMC_PRIORITY_IGNORE
		iOldHighPriority[iteam]= FMMC_PRIORITY_IGNORE
		iNumHighPriorityLoc[iteam] = 0
		iNumHighPriorityPed[iteam] = 0
		iNumHighPriorityDeadPed[iteam] = 0
		iNumHighPriorityVeh[iteam] = 0
		iNumHighPriorityObj[iteam] = 0
		
		ireasonObjEnd[iteam] = -1
		iObjEndPriority[iteam] = -1
		
		//Initialise these to -1 so it checks and changes stuff on the first objective:
		iOldPriority[iteam] = -1
		iOldMidpoint[iteam] = -1
		INT iLocLoop
		FOR iLocLoop = 0 TO (MC_serverBD.iNumLocCreated - 1)
			iCurrCapTime[iLocLoop][iteam] = -1
		ENDFOR
		
		FOR iRule = 0 TO (FMMC_MAX_RULES - 1)			
			IF g_FMMC_STRUCT.sFMMCEndConditions[iteam].fDropOffRadius[iRule] = 0.0
				g_FMMC_STRUCT.sFMMCEndConditions[iteam].fDropOffRadius[iRule] = 10.0
			ENDIF
			
			IF iteam = MC_playerBD[iLocalPart].iteam
				IF MC_serverBD_4.iPlayerRule[iRule][iteam]= FMMC_OBJECTIVE_LOGIC_GET_MASKS
				AND NOT IS_BIT_SET(iLocalBoolCheck4,LBOOL4_OWN_MASK)
					g_bAllowClothesShopInMission = TRUE
					PRINTLN("[RCC MISSION] MANAGE_LOCAL_PLAYER_INIT - Mask shop open for team: ",iteam)
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iteam].iRuleBitsetFifteen[iRule], ciBS_RULE15_ALLOW_REMOTE_EXPLOSIVE_VEHICLES)
					SET_BIT(iLocalBoolCheck29, LBOOL29_REMOTE_DETONATE_VEHICLE_IN_THIS_MISSION)
					PRINTLN("[RCC MISSION] MANAGE_LOCAL_PLAYER_INIT - Setting LBOOL29_REMOTE_DETONATE_VEHICLE_IN_THIS_MISSION for rule ", iRule)
				ENDIF
			ENDIF
		ENDFOR
	ENDFOR
		
	FOR i = 0 TO (FMMC_MAX_VEHICLES-1)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iPriority[MC_playerBD[iLocalPart].iteam] < FMMC_MAX_RULES
			IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn != DUMMY_MODEL_FOR_SCRIPT
			AND IS_MODEL_VALID(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn)
				IF CAN_MODEL_BE_SUPPRESSED(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn)
					PRINTLN("[RCC MISSION] MANAGE_LOCAL_PLAYER_INIT - suppressing vehicle model: ",ENUM_TO_INT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn))
					SET_VEHICLE_MODEL_IS_SUPPRESSED(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn,TRUE)
					IF g_iNumSupressed < MAX_NUM_SUPRESSED_MODELS
						mnSuppressedVehicles[g_iNumSupressed] = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn
					ENDIF
					VALIDATE_NUMBER_MODELS_SUPRESSED(1)
				ENDIF
			ENDIF
		ENDIF
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehRadio = ciRC_FIXER_LOWRIDER_MIX
			UNLOCK_RADIO_STATION_TRACK_LIST("RADIO_36_AUDIOPLAYER", "FIXER_AP_LOWRIDERS_MIX")
			FORCE_MUSIC_TRACK_LIST("RADIO_36_AUDIOPLAYER", "FIXER_AP_LOWRIDERS_MIX", MC_serverBD_3.iSyncedMediaPlayerStartPoint)
			PRINTLN("[RCC MISSION] MANAGE_LOCAL_PLAYER_INIT - Unlocking Media Player Lowrider Mix and starting from ", MC_serverBD_3.iSyncedMediaPlayerStartPoint)
		ENDIF
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn = AVENGER
			g_bMissionHasPlacedAvengerVehicle = TRUE
		ENDIF
		
		iVehPhotoTrackedPoint[i] = -1
		iVehPhotoTrackedPoint2[i] = -1
	ENDFOR
	
	FOR i = 0 TO FMMC_MAX_TEAMS - 1
		IF g_FMMC_STRUCT.mnVehicleModel[i] != DUMMY_MODEL_FOR_SCRIPT
			IF CAN_MODEL_BE_SUPPRESSED(g_FMMC_STRUCT.mnVehicleModel[i])
			AND IS_MODEL_VALID(g_FMMC_STRUCT.mnVehicleModel[i])
				PRINTLN("[RCC MISSION] MANAGE_LOCAL_PLAYER_INIT - team ", i, " suppressing vehicle model: ",ENUM_TO_INT(g_FMMC_STRUCT.mnVehicleModel[i]))
				SET_VEHICLE_MODEL_IS_SUPPRESSED( g_FMMC_STRUCT.mnVehicleModel[i], TRUE )
				IF g_iNumSupressed < MAX_NUM_SUPRESSED_MODELS
					mnSuppressedVehicles[g_iNumSupressed] = g_FMMC_STRUCT.mnVehicleModel[i]
				ENDIF
				VALIDATE_NUMBER_MODELS_SUPRESSED(1)
			ENDIF
		ENDIF
	ENDFOR
	
	RUN_LOCAL_PLAYER_PROOF_SETTING()
		
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciALLOW_SPRINTING_IN_INTERIORS)
		SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_IgnoreInteriorCheckForSprinting, TRUE)
		PRINTLN("[RCC MISSION] MANAGE_LOCAL_PLAYER_INIT - SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_IgnoreInteriorCheckForSprinting, TRUE)")
		
		bIgnoreInteriorCheckForSprinting = TRUE
	ENDIF
	
	// Giving players weapons at the start of the mission
	IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
		IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
			PRINTLN("MANAGE_LOCAL_PLAYER_INIT -[SET_CURRENT_PED_WEAPON] g_myStartingWeapon = ", GET_WEAPON_NAME(g_myStartingWeapon))
			
			REMOVE_MISSION_RESTRICTED_WEAPONS()
			
			IF SHOULD_CURRENT_PLAYER_HAVE_WEAPONS_REMOVED(MC_playerBD[iLocalPart].iteam)			
				PRINTLN("[RCC MISSION] MANAGE_LOCAL_PLAYER_INIT - [WeaponInventory] 1")
				IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciMISSION_OVERRIDE_AMMUNATION_FORCE_CLOSE)
					g_bMissionInventory = TRUE
				ENDIF
				REMOVE_ALL_WEAPONS()
				SET_BLOCK_AMMO_GLOBAL()
				
				IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_MP_PROPERTY_INT")) > 0				
					PRINTLN("POD:  MANAGE_LOCAL_PLAYER_INIT -: HOLSTERING Weapon as am_mp_property_int script is running")
					GIVE_LOCAL_PLAYER_WEAPON_INVENTORY(FMMC_INVENTORY_STARTING, WEAPONINHAND_FIRSTSPAWN_HOLSTERED, !IS_JOB_OWNED_WEAPONS_PLUS_PICKUPS())
				ELSE
					GIVE_LOCAL_PLAYER_WEAPON_INVENTORY(FMMC_INVENTORY_STARTING, WEAPONINHAND_LASTWEAPON_BOTH, !IS_JOB_OWNED_WEAPONS_PLUS_PICKUPS())
				ENDIF
				
				RESTORE_CONTINUITY_COLLECTED_WEAPONS()
				
			ELSE
				IF IS_JOB_FORCED_WEAPON_ONLY()
				OR IS_JOB_FORCED_WEAPON_PLUS_PICKUPS()
				OR DOES_TEAM_HAVE_AN_FMMC_GIVEN_INVENTORY(MC_playerBD[iLocalPart].iteam)
					PRINTLN("[RCC MISSION] MANAGE_LOCAL_PLAYER_INIT - [WeaponInventory] 2b")					
					GIVE_FORCED_WEAPON_AND_AMMO(TRUE, iDlcWeaponBitSet, wtWeaponToRemove, DOES_TEAM_HAVE_AN_FMMC_GIVEN_INVENTORY(MC_playerBD[iLocalPart].iteam))
					
					IF NOT IS_JOB_FORCED_WEAPON_ONLY() AND NOT IS_JOB_FORCED_WEAPON_PLUS_PICKUPS()
						IF g_myStartingWeapon != WEAPONTYPE_INVALID
							SET_CURRENT_PED_WEAPON(LocalPlayerPed, g_myStartingWeapon, TRUE)
							gweapon_type_CurrentlyHeldWeapon = g_myStartingWeapon
						ENDIF
					ENDIF
					IF MC_SHOULD_WE_START_FROM_CHECKPOINT()
					
						INT iCheckpoint = MC_GET_MISSION_STARTING_CHECKPOINT()
						
						IF g_TransitionSessionNonResetVars.bHasQuickRestarted
						AND IS_BIT_SET(g_FMMC_STRUCT.sTeamData[GET_LOCAL_PLAYER_TEAM()].iTeamBitSet, ciTEAM_BS_MID_MISSION_INVENTORY_RESTART_ONLY)
						AND g_FMMC_STRUCT.sFMMCEndConditions[GET_LOCAL_PLAYER_TEAM()].iMidMissionInventoryGiveOnTeamRule = g_FMMC_STRUCT.iRestartRule[GET_LOCAL_PLAYER_TEAM()][iCheckpoint]
						AND g_FMMC_STRUCT.sFMMCEndConditions[GET_LOCAL_PLAYER_TEAM()].iMidMissionInventoryGiveOnTeam = GET_LOCAL_PLAYER_TEAM()
						AND NOT IS_BIT_SET(iLocalBoolCheck4, LBOOL4_BEEN_GIVEN_MIDMISSION_INVENTORY) 
							GIVE_LOCAL_PLAYER_WEAPON_INVENTORY(FMMC_INVENTORY_MID_MISSION, WEAPONINHAND_LASTWEAPON_BOTH)
							IF IS_BIT_SET(g_FMMC_STRUCT.iRestartInCoverBitset, (MC_playerBD[iPartToUse].iteam * FMMC_MAX_RESTART_CHECKPOINTS) + iCheckpoint)
								TASK_PUT_PED_DIRECTLY_INTO_COVER(LocalPlayerPed, GET_ENTITY_COORDS(LocalPlayerPed), -1, TRUE) 
								FORCE_PED_AI_AND_ANIMATION_UPDATE(LocalPlayerPed, TRUE)
								SET_BIT(iLocalBoolCheck30, LBOOL30_SET_PLAYER_INTO_COVER_START_POSITION)
								PRINTLN("[RCC MISSION] - MANAGE_LOCAL_PLAYER_INIT - TASK_PUT_PED_DIRECTLY_INTO_COVER")									
							ENDIF
							SET_BIT(iLocalBoolCheck4, LBOOL4_BEEN_GIVEN_MIDMISSION_INVENTORY) 
						ENDIF
						
						IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iTeamBitset3, ciBS3_TEAM_UNARMED_RESTART_ON_CHECKPOINT_0 + iCheckpoint)
							PRINTLN("[RCC MISSION] MANAGE_LOCAL_PLAYER_INIT - MANAGE_LOCAL_PLAYER_INIT - ciBS3_TEAM_UNARMED_RESTART_ON_CHECKPOINT_",(iCheckpoint+1)," set for my team ",MC_playerBD[iPartToUse].iteam,", set player unarmed")
							SET_CURRENT_PED_WEAPON(LocalPlayerPed, WEAPONTYPE_UNARMED, TRUE)
							gweapon_type_CurrentlyHeldWeapon = WEAPONTYPE_UNARMED
						ENDIF
					ENDIF
				ELSE
					PRINTLN("[RCC MISSION] MANAGE_LOCAL_PLAYER_INIT - [WeaponInventory] 3")
					IF NOT RESTART_INTRO_IS_UNARMED(MC_playerBD[iLocalPart].iteam)
					AND NOT SHOULD_RESTART_HAVE_APARTMENT_ANIM(MC_playerBD[iLocalPart].iteam)
					AND NOT IS_CHECKPOINT_ANIM_GLOBAL_SET()
						IF g_myStartingWeapon = WEAPONTYPE_INVALID
							PRINTLN("[RCC MISSION] MANAGE_LOCAL_PLAYER_INIT - [WeaponInventory] 4")
							PUT_WEAPON_IN_HAND( WEAPONINHAND_LASTWEAPON_BOTH )
						ELIF HAS_FIRST_STRAND_MISSION_BEEN_PASSED()
							PRINTLN("[RCC MISSION] MANAGE_LOCAL_PLAYER_INIT - [WeaponInventory] 5")
							PUT_WEAPON_IN_HAND( WEAPONINHAND_LASTWEAPON_BOTH )
						ELSE
							
							IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciRESPAWN_WITH_LAST_WEAPON)
								IF HAS_PED_GOT_WEAPON( LocalPlayerPed, g_myStartingWeapon )
									PRINTLN("[RCC MISSION] MANAGE_LOCAL_PLAYER_INIT - [WeaponInventory] 6")
									
									iTeam = MC_playerBD[iLocalPart].iteam
									BOOL bForceInHand = TRUE
									INT iRuleToUse = 0
									IF MC_SHOULD_RESPAWN_AT_CHECKPOINT(iTeam, FALSE)
									OR (MC_SHOULD_WE_START_FROM_CHECKPOINT() AND SHOULD_USE_CUSTOM_START_POINT_CHECKPOINTS())
										INT iCheckpoint = MC_GET_MISSION_STARTING_CHECKPOINT()
										iRuleToUse = g_FMMC_STRUCT.iRestartRule[iTeam][iCheckpoint]
										PRINTLN("[JS] MANAGE_LOCAL_PLAYER_INIT - iRuleToUse: ", iRuleToUse, " iCheckpoint: ", iCheckpoint)
									ENDIF
									
									IF iRuleToUse < FMMC_MAX_RULES
										IF iRuleToUse > -1
										AND (IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFour[iRuleToUse], ciBS_RULE4_REMOVE_WEAPONS ) 
										OR IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFour[iRuleToUse], ciBS_RULE4_START_UNARMED ))
											SET_CURRENT_PED_WEAPON( LocalPlayerPed, WEAPONTYPE_UNARMED, bForceInHand )
											gweapon_type_CurrentlyHeldWeapon = WEAPONTYPE_UNARMED
											PRINTLN("[JS] MANAGE_LOCAL_PLAYER_INIT - Start with weapons holtered if the first rule has Disable All Weapons or Start Unarmed set")
										ELSE
											SET_CURRENT_PED_WEAPON( LocalPlayerPed, g_myStartingWeapon, bForceInHand )
											gweapon_type_CurrentlyHeldWeapon = g_myStartingWeapon
										ENDIF
									ENDIF
								ELSE
									PRINTLN("[RCC MISSION] MANAGE_LOCAL_PLAYER_INIT - [WeaponInventory] 6.25")
								ENDIF
							ELSE
								PRINTLN("[RCC MISSION] MANAGE_LOCAL_PLAYER_INIT - [WeaponInventory] 6a")
								IF GET_PLAYER_CURRENT_HELD_WEAPON() = WEAPONTYPE_UNARMED
								AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciRESPAWN_WITH_BEST_WEAPON)
									PRINTLN("[RCC MISSION] MANAGE_LOCAL_PLAYER_INIT - [WeaponInventory] 6b")
									SET_CURRENT_PED_WEAPON(LocalPlayerPed, GET_BEST_PED_WEAPON(LocalPlayerPed, FALSE), TRUE)
									gweapon_type_CurrentlyHeldWeapon = GET_BEST_PED_WEAPON(LocalPlayerPed, FALSE)
								ELSE
									PRINTLN("[RCC MISSION] MANAGE_LOCAL_PLAYER_INIT - [WeaponInventory] 6c")
									IF g_myStartingWeapon != WEAPONTYPE_INVALID
									AND HAS_PED_GOT_WEAPON(LocalPlayerPed, g_myStartingWeapon)
										SET_CURRENT_PED_WEAPON(LocalPlayerPed, g_myStartingWeapon, TRUE)
										gweapon_type_CurrentlyHeldWeapon = g_myStartingWeapon
										PRINTLN("[RCC MISSION] MANAGE_LOCAL_PLAYER_INIT - [WeaponInventory] 6d")
									ELIF DOES_PLAYER_HAVE_WEAPON(GET_PLAYER_CURRENT_HELD_WEAPON())
										PRINTLN("[RCC MISSION] MANAGE_LOCAL_PLAYER_INIT - [WeaponInventory] 6e")
										SET_CURRENT_PED_WEAPON(LocalPlayerPed, GET_PLAYER_CURRENT_HELD_WEAPON(), TRUE)
										gweapon_type_CurrentlyHeldWeapon = GET_PLAYER_CURRENT_HELD_WEAPON()
									ELSE
										IF IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[ MC_playerBD[iLocalPart].iteam ].iRuleBitsetFour[ 0 ], ciBS_RULE4_REMOVE_WEAPONS ) 
										OR IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[ MC_playerBD[iLocalPart].iteam ].iRuleBitsetFour[ 0 ], ciBS_RULE4_START_UNARMED )
											SET_CURRENT_PED_WEAPON( LocalPlayerPed, WEAPONTYPE_UNARMED, TRUE)
											gweapon_type_CurrentlyHeldWeapon = WEAPONTYPE_UNARMED
											PRINTLN("[RCC MISSION] MANAGE_LOCAL_PLAYER_INIT - [dsw] INVALID WEAPON - STARTING WITH WEAPONS HOLSTERED")
										ELSE
											SET_CURRENT_PED_WEAPON(LocalPlayerPed, GET_BEST_PED_WEAPON(LocalPlayerPed, FALSE), TRUE)
											gweapon_type_CurrentlyHeldWeapon = GET_BEST_PED_WEAPON(LocalPlayerPed)
											PRINTLN("[RCC MISSION] MANAGE_LOCAL_PLAYER_INIT - [dsw] INVALID WEAPON - EQUIPPING BEST WEAPON")
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			PRINTLN("[RCC MISSION] MANAGE_LOCAL_PLAYER_INIT - [WeaponInventory] 11")
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciRESPAWN_WITH_LAST_WEAPON)
				PRINTLN("[RCC MISSION] MANAGE_LOCAL_PLAYER_INIT - [WeaponInventory] 12")
				IF IS_BIT_SET(g_FMMC_STRUCT.sTeamData[GET_LOCAL_PLAYER_TEAM()].iTeamBitSet, ciTEAM_BS_SET_STARTING_WEAPON_ON_STRAND)
					INT iInventoryIndex = GET_INVENTORY_INDEX_FOR_LOCAL_TEAM(FMMC_INVENTORY_STARTING)
					IF iInventoryIndex = -1
						iInventoryIndex = GET_INVENTORY_INDEX_FOR_LOCAL_TEAM(FMMC_INVENTORY_MID_MISSION)
					ENDIF
					SET_STARTING_WEAPON_FROM_INVENTORY(g_FMMC_STRUCT.sPlayerWeaponInventories[iInventoryIndex])
				ELSE
					IF GET_PLAYER_CURRENT_HELD_WEAPON() != WEAPONTYPE_UNARMED
					AND GET_PLAYER_CURRENT_HELD_WEAPON() != WEAPONTYPE_INVALID
						PRINTLN("[RCC MISSION] MANAGE_LOCAL_PLAYER_INIT - [WeaponInventory] 13")
						SET_CURRENT_PED_WEAPON(LocalPlayerPed, GET_PLAYER_CURRENT_HELD_WEAPON(), TRUE)
						gweapon_type_CurrentlyHeldWeapon = GET_PLAYER_CURRENT_HELD_WEAPON()
					ELSE
						PRINTLN("[RCC MISSION] MANAGE_LOCAL_PLAYER_INIT - [WeaponInventory] 14")
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciRESPAWN_WITH_BEST_WEAPON)
							PRINTLN("[RCC MISSION] MANAGE_LOCAL_PLAYER_INIT - [WeaponInventory] 15")
							SET_CURRENT_PED_WEAPON(LocalPlayerPed, GET_BEST_PED_WEAPON(LocalPlayerPed, FALSE), TRUE)
							gweapon_type_CurrentlyHeldWeapon = GET_BEST_PED_WEAPON(LocalPlayerPed)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF ((NOT DOES_TEAM_HAVE_AN_FMMC_GIVEN_INVENTORY(GET_LOCAL_PLAYER_TEAM(),FALSE,TRUE))
		OR (g_FMMC_STRUCT.sFMMCEndConditions[GET_LOCAL_PLAYER_TEAM()].iMidMissionInventoryGiveOnTeam = -1)
		OR (g_FMMC_STRUCT.sFMMCEndConditions[GET_LOCAL_PLAYER_TEAM()].iMidMissionInventoryGiveOnTeamRule = -1))
		AND NOT IS_BIT_SET(g_FMMC_STRUCT.sTeamData[GET_LOCAL_PLAYER_TEAM()].iTeamBitSet, ciTEAM_BS_GIVE_MID_MISSION_INVENTORY_PICKUP_TEAM)
			GIVE_LOCAL_PLAYER_WEAPON_INVENTORY(FMMC_INVENTORY_STARTING, WEAPONINHAND_LASTWEAPON_BOTH)
			GIVE_LOCAL_PLAYER_WEAPON_INVENTORY(FMMC_INVENTORY_MID_MISSION, WEAPONINHAND_LASTWEAPON_BOTH)
			SET_BIT(iLocalBoolCheck4, LBOOL4_BEEN_GIVEN_MIDMISSION_INVENTORY)
		ELSE

			//The player will get their full inventory later in the mission - give them the starting inventory now:
			GIVE_LOCAL_PLAYER_WEAPON_INVENTORY(FMMC_INVENTORY_STARTING, WEAPONINHAND_LASTWEAPON_BOTH)
		ENDIF
	ENDIF

	REINIT_NET_TIMER(MC_playerBD[iLocalPart].tdMissionTime)

	REFRESH_ALL_OVERHEAD_DISPLAY()
	
	CLIENT_SET_UP_TEAM_ONLY_CHAT(0.0)
	
	IF IS_THIS_MODE_ALLOWING_SPECTATOR_CROSS_TALK()
		ALLOW_SPECTATORS_TO_CHAT(TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFive, ciOptionsBS25_EnableHeadsetBeep)
		SET_AUDIO_FLAG("EnableHeadsetBeep", TRUE)
		PRINTLN("[RCC MISSION] MANAGE_LOCAL_PLAYER_INIT - Calling SET_AUDIO_FLAG with EnableHeadsetBeep TRUE")
	ENDIF

	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyNine, ciOptionsBS29_DisableNPCHeadsetSpeechAttenuation)
		SET_AUDIO_FLAG("DisableNPCHeadsetSpeechAttenuation", TRUE)
		PRINTLN("[RCC MISSION] MANAGE_LOCAL_PLAYER_INIT - Calling SET_AUDIO_FLAG with DisableNPCHeadsetSpeechAttenuation TRUE")
	ENDIF
	
	//so that the kill ticker system uses competative tickers not the freemode.
	ACTIVATE_KILL_TICKERS(TRUE)
	
	SET_BIT(GlobalplayerBD_FM[NETWORK_PLAYER_ID_TO_INT()].iXPBitset, ciGLOBAL_XP_BIT_ON_JOB)

	SET_BIT(MC_playerBD[iLocalPart].iClientBitSet,PBBOOL_ALL_HEADSHOTS)
	
	SET_BIT(iLocalBoolCheck2,LBOOL2_WHOLE_UPDATE_DONE)

	SET_IGNORE_NO_GPS_FLAG_UNTIL_FIRST_NORMAL_NODE(TRUE)
	
	IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
		
		CHEAT_TRACKER_START_OF_MC_MISSION()
		
	
		IF NOT HAS_FIRST_STRAND_MISSION_BEEN_PASSED()
			PRINTLN("[RCC MISSION] MANAGE_LOCAL_PLAYER_INIT - Cache Accuracy Data")
			GET_LOCAL_PLAYER_ACCURACY_DATA(MC_playerBD[iLocalPart].iStartShots, MC_playerBD[iLocalPart].iStartHits)
			
			MC_playerBD[iLocalPart].iStartStealthKills = GET_LOCAL_PLAYER_STEALTH_KILLS_DATA()
			PRINTLN("[RCC MISSION] MANAGE_LOCAL_PLAYER_INIT - Cache Stealth Kill Data", MC_playerBD[iLocalPart].iStartStealthKills)
			
		ENDIF
		
		DEAL_WITH_FM_MATCH_START(FMMC_TYPE_MISSION,g_MissionControllerserverBD_LB.iMatchTimeID,g_MissionControllerserverBD_LB.iHashMAC, MC_serverBD.iDifficulty)
		IF IS_THIS_A_CONTACT_MISSION()
			SET_MP_BOOL_CHARACTER_STAT(MP_STAT_CL_PLAY_MISSION_CONTACT, TRUE)

			CLEAR_PLAYER_RECENTLY_PASSED_MISSION_FOR_CONTACT(Get_Current_Mission_Contact())
			IF Get_Current_Mission_Contact() = CHAR_TREVOR
				CLEAR_PLAYER_RECENTLY_PASSED_MISSION_FOR_CONTACT(CHAR_RON)
			ELIF Get_Current_Mission_Contact() = CHAR_RON
				CLEAR_PLAYER_RECENTLY_PASSED_MISSION_FOR_CONTACT(CHAR_TREVOR)
			ELIF Get_Current_Mission_Contact() = CHAR_MP_GERALD
				RESET_REMINDER_TIMER_FOR_ACTIVITY(FMMC_TYPE_GERALD_REQUEST_JOB)				
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
		IF HAS_CUTSCENE_LOADED()
			REMOVE_CUTSCENE()
		ENDIF
	ENDIF
	
	SET_GAME_STATE_ON_DEATH(AFTERLIFE_JUST_REJOIN)
	SET_KILLSTRIP_AUTO_RESPAWN(TRUE)				
	SET_KILLSTRIP_AUTO_RESPAWN_TIME(GET_DM_RESPAWN_TIME())

	IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam] < FMMC_MAX_RULES
	AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].iRuleBitSetTwo[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam]], ciBS_RULE2_RESPAWN_AT_START_POINT)					
		SET_START_CUSTOM_SPAWN_POINTS(GET_PLAYER_TEAM_FOR_INTRO())
		SET_MISSION_DEFAULT_RESPAWN_LOCATION(SPAWN_LOCATION_AT_SPECIFIC_COORDS)
	ELSE
		SET_MISSION_DEFAULT_RESPAWN_LOCATION(SPAWN_LOCATION_NEAR_DEATH)
	ENDIF
	
	TOGGLE_STRIPPER_AUDIO(FALSE)
	
	START_AUDIO_SCENE("MP_POSITIONED_RADIO_MUTE_SCENE")
	
	INTERIOR_INSTANCE_INDEX interior = GET_INTERIOR_FROM_ENTITY( LocalPlayerPed )
	
	VECTOR vPlayerPos = GET_ENTITY_COORDS( LocalPlayerPed )
	
	IF interior = GET_INTERIOR_AT_COORDS_with_type( vPlayerPos, "V_FIB01" )
	OR interior = GET_INTERIOR_AT_COORDS_with_type( vPlayerPos, "V_FIB02" )
	OR interior = GET_INTERIOR_AT_COORDS_with_type( vPlayerPos, "V_FIB03" )
	OR interior = GET_INTERIOR_AT_COORDS_with_type( vPlayerPos, "V_FIB04" ) 
		SET_AMBIENT_ZONE_STATE( "IZ_V_FIB01_V_FIB01_JAN_COMM1", FALSE, TRUE )
		SET_AMBIENT_ZONE_STATE( "IZ_v_fib01_V_FIB01_jan_comm2", FALSE, TRUE )
		SET_AMBIENT_ZONE_STATE( "IZ_V_FIB01_V_FIB01_JAN_OFFC1", FALSE, TRUE )
		SET_AMBIENT_ZONE_STATE( "IZ_v_fib01_V_FIB01_jan_offc2", FALSE, TRUE )
		SET_AMBIENT_ZONE_STATE( "IZ_V_FIB01_V_FIB01_JAN_OFFC3", FALSE, TRUE )
		SET_AMBIENT_ZONE_STATE( "IZ_V_FIB01_V_FIB01_JAN_PNTRY", FALSE, TRUE )
		
	ENDIF
		
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].iTeamBitset2, ciBS2_CUSTOM_WASTED_SHARD)
	AND g_FMMC_STRUCT.iCustomWastedShardType[MC_playerBD[iLocalPart].iteam] != ciCUSTOM_WASTED_HEALTH_DEPLETED
		PRINTLN("[KH] MANAGE_LOCAL_PLAYER_INIT - - CUSTOM WASTED SHARD IS ACTIVE")
		SET_BIG_MESSAGE_SUPPRESS_WASTED(TRUE)
		CHANGE_WASTED_SHARD_TEXT(TRUE)
	ENDIF

	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iTeamBitset2, ciBS2_EnableRadioOnly)
	AND NOT (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTen, ciSTOP_SCORE_OVERRIDING_RADIO) AND GET_PLAYER_RADIO_STATION_INDEX() != 255)
		IF NOT IS_BIT_SET(iLocalBoolCheck3, LBOOL3_MUSIC_PLAYING)
			IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
			AND NOT CHECK_CHECKPOINT_RESTART_POINT_AGAINST_BITSET(g_FMMC_STRUCT.iAudioBlockScoreStartingonCP)
			AND NOT IS_MISSION_MUSIC_BLOCKED_FOR_INTRO_CUTSCENE()
				PRINTLN("[Mission_Music] - MANAGE_LOCAL_PLAYER_INIT - SET_MISSION_MUSIC with g_FMMC_STRUCT.iAudioScore: ", g_FMMC_STRUCT.iAudioScore)
				
				SET_MISSION_MUSIC(g_FMMC_STRUCT.iAudioScore, g_FMMC_STRUCT.iAudioScoreStartingMood > MUSIC_DEFAULT)
				
				SET_BIT(iLocalBoolCheck3, LBOOL3_MUSIC_PLAYING)
				
				SET_BIT(iLocalBoolCheck, LBOOL_MUSIC_CHANGED) 
				
				IF g_FMMC_STRUCT.iAudioScoreStartingMood > MUSIC_DEFAULT
					SET_MUSIC_STATE(g_FMMC_STRUCT.iAudioScoreStartingMood)
				ENDIF
			ELSE
				REINIT_NET_TIMER(tdMusicTimer)
				IF IS_A_STRAND_MISSION_BEING_INITIALISED()
					PRINTLN("[Mission_Music] - MANAGE_LOCAL_PLAYER_INIT - IS_A_STRAND_MISSION_BEING_INITIALISED not playing music")
				ENDIF	
				IF CHECK_CHECKPOINT_RESTART_POINT_AGAINST_BITSET(g_FMMC_STRUCT.iAudioBlockScoreStartingonCP)
					PRINTLN("[Mission_Music] - MANAGE_LOCAL_PLAYER_INIT - CHECK_CHECKPOINT_RESTART_POINT_AGAINST_BITSET not playing music")
				ENDIF
				IF IS_MISSION_MUSIC_BLOCKED_FOR_INTRO_CUTSCENE()
					PRINTLN("[Mission_Music] - MANAGE_LOCAL_PLAYER_INIT - IS_MISSION_MUSIC_BLOCKED_FOR_INTRO_CUTSCENE not playing music")
				ENDIF
			ENDIF
			
			IF (AUDIO_IS_SCRIPTED_MUSIC_PLAYING() AND IS_A_STRAND_MISSION_BEING_INITIALISED())
				SET_BIT(iLocalBoolCheck3, LBOOL3_MUSIC_PLAYING)
				SET_BIT(iLocalBoolCheck, LBOOL_MUSIC_CHANGED) 
			ENDIF
		ELSE
			PRINTLN("[Mission_Music] - MANAGE_LOCAL_PLAYER_INIT - LBOOL3_MUSIC_PLAYING set already")
		ENDIF
	ENDIF
	
	//Clear Round Persistent Globals
	IF NOT (IS_THIS_A_ROUNDS_MISSION() AND g_sTransitionSessionData.sMissionRoundData.iNumberOfRoundsPlayed > 0)
		g_SpawnData.bRadioStationOverScore = FALSE
		PRINTLN("MANAGE_LOCAL_PLAYER_INIT - g_SpawnData.bRadioStationOverScore = FALSE")
		
		g_TransitionSessionNonResetVars.iSpawnGroupLastBitset = 0
		PRINTLN("MANAGE_LOCAL_PLAYER_INIT - iSpawnGroupLastBitset = 0")
		
		INT iSpawnGroup
		
		REPEAT ciMAX_NUM_FMMC_ENTITY_SUB_SPAWN_GROUPS iSpawnGroup
			g_TransitionSessionNonResetVars.iSpawnSubGroupLastBitset[iSpawnGroup] = 0
			PRINTLN("[RCC MISSION] MANAGE_LOCAL_PLAYER_INIT - CLEAR iSpawnSubGroupLastBitset[", iSpawnGroup, "] = 0")
		ENDREPEAT
	ENDIF
	
	SET_FMMC_PROP_MODELS_AS_NOT_NEEDED()

	RESET_CREATE_AND_ENTER_EOM_AMBIENT_VEHICLE_DATA()
	
	STORE_PLAYER_INTERACTION_ANIM_VALUES_FOR_CELEBRATION_SCREEN()
		
	APPLY_CORONA_ALCOHOL_AND_SMOKE_EFFECTS(FALSE, TRUE, FALSE)
	
	SET_FMMC_MODELS_AS_NOT_NEEDED()
					
	//If this is a strand mission then set that we're ready for the reset
	IF HAS_FIRST_STRAND_MISSION_BEEN_PASSED()
		SET_MISSION_CONTROLLER_READY_FOR_RESET()
	ENDIF
	
	DO_HEIST_HEAVY_ARMOUR_CHECKS()
	
	IF IS_FAKE_MULTIPLAYER_MODE_SET()
		SET_BIT( MC_playerBD[ iLocalPart ].iClientBitSet, PBBOOL_HEIST_HOST )
		
		INT iHeistLeaderPlayer =  NATIVE_TO_INT( NETWORK_GET_PLAYER_INDEX( INT_TO_PARTICIPANTINDEX( iLocalPart ) ) )
	
		GlobalplayerBD_FM_HeistPlanning[iHeistLeaderPlayer].iPreviousPropertyIndex = PROPERTY_HIGH_APT_1
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].iTeamBitset2, ciBS2_FORCE_KEEP_HELMETS_ON_RESPAWN)
		SET_PED_CONFIG_FLAG(localPlayerPed, PCF_DontTakeOffHelmet, TRUE)
	ENDIF
	
	WIPE_ROUNDS_TEAM_TOTALS()
	
	#IF IS_DEBUG_BUILD
	MAINTAIN_EOM_DATA_DUMP(TRUE)
	#ENDIF
		
	//Turn on the SecuroServ Hacking app if the first rule for the team gives access to it.
	
	iHackStage = ciSECUROSERV_HACK_STAGE_NO_SIGNAL
	iHackPercentage = 0
	tl15HackComplete = ""
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_SECUROSERV_HACKING)
		IF NOT IS_BIT_SET(BitSet_CellphoneTU, g_BSTU_ENABLE_MP_SECUROSERV_HACK_APP)
			PRINTLN("[SECUROHACK] MANAGE_LOCAL_PLAYER_INIT - Turning on")
			SET_BIT(BitSet_CellphoneTU, g_BSTU_ENABLE_MP_SECUROSERV_HACK_APP)
		ELSE
			PRINTLN("[SECUROHACK] MANAGE_LOCAL_PLAYER_INIT - App is already turned on")
		ENDIF
	ELSE
		IF IS_BIT_SET(BitSet_CellphoneTU, g_BSTU_ENABLE_MP_SECUROSERV_HACK_APP)
			PRINTLN("[SECUROHACK] MANAGE_LOCAL_PLAYER_INIT - Turning off App because it shouldn't be on in this mission")
			CLEAR_BIT(BitSet_CellphoneTU, g_BSTU_ENABLE_MP_SECUROSERV_HACK_APP)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciMISSION_BLOCK_PLAYER_DROPPED_AMMO)
		SET_PED_DROPS_WEAPONS_WHEN_DEAD(LocalPlayerPed, FALSE)
	ENDIF
	
	IF IS_BIT_SET(MPGlobalsAmbience.iAmbBitSet, iABI_BLOCK_COLLECTING_ARMOUR)
		SET_PLAYER_PERMITTED_TO_COLLECT_PICKUPS_OF_TYPE(LocalPlayer, PICKUP_ARMOUR_STANDARD, TRUE)
		CLEAR_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_BLOCK_COLLECTING_ARMOUR)
		
		PRINTLN("[RCC MISSION][KH] MANAGE_LOCAL_PLAYER_INIT - iABI_BLOCK_COLLECTING_ARMOUR IS SET - CLEARING")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_VEHICLE_WEAPON_MG)
		REQUEST_WEAPON_ASSET(WEAPONTYPE_VEHICLE_PLAYER_BULLET)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_VEHICLE_WEAPON_BULLETPROOF_PLAYER)
		PRINTLN("[KH] MANAGE_LOCAL_PLAYER_INIT - setting bulletproof")
		SET_ENTITY_PROOFS(LocalPlayerPed, TRUE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciDISABLE_AMBIENT_VFX)
		DISABLE_REGION_VFX(TRUE)
	ENDIF

	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySix,  ciOptionsBS26_BlockGettingParachutesFromPlanes)
		SET_AUTO_GIVE_PARACHUTE_WHEN_ENTER_PLANE(LocalPlayer, FALSE)
		PRINTLN("[Parachute] MANAGE_LOCAL_PLAYER_INIT - SET_AUTO_GIVE_PARACHUTE_WHEN_ENTER_PLANE(LocalPlayer, FALSE) - ciOptionsBS26_BlockGettingParachutesFromPlanes")
	ELSE		
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iTeamBitset, ciBS_FORCE_RESPAWN_WHEN_NOT_IN_RESPAWN_VEHICLE)
			SET_AUTO_GIVE_PARACHUTE_WHEN_ENTER_PLANE(LocalPlayer,FALSE)
		ELSE
			SET_AUTO_GIVE_PARACHUTE_WHEN_ENTER_PLANE(LocalPlayer, TRUE)
			PRINTLN("[Parachute] MANAGE_LOCAL_PLAYER_INIT - SET_AUTO_GIVE_PARACHUTE_WHEN_ENTER_PLANE(LocalPlayer, TRUE)")
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciALLOW_MISSION_AIRSTRIKES)
		PRINTLN("[RCC MISSION][AIRSTRIKE] MANAGE_LOCAL_PLAYER_INIT - - ciALLOW_MISSION_AIRSTRIKES is set")
		g_vAirstrikeCoords = <<0,0,0>>
	ENDIF
				
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciALLOW_MOC_ENTRY_THIS_MISSION)
		SET_ALLOW_INTERIOR_SCRIPT_FOR_MISSION(TRUE)
		PRINTLN("[RCC MISSION] MANAGE_LOCAL_PLAYER_INIT - MOC ENTRY IS ENABLED")
		#IF IS_DEBUG_BUILD
		INT iveh
		FOR iveh = 0 TO (FMMC_MAX_VEHICLES-1)
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetSix, ciFMMC_VEHICLE6_ALLOW_MOC_ENTRY)
				PRINTLN("[RCC MISSION] MANAGE_LOCAL_PLAYER_INIT - - Vehicle ", iveh, " has entry enabled.")
			ENDIF
		ENDFOR
		#ENDIF
		
		SET_KICK_OUT_PLAYER_FROM_ARMORY_TRUCK(FALSE)	
		CDEBUG1LN(DEBUG_SPAWNING, " MANAGE_LOCAL_PLAYER_INIT -Clearing BS_SIMPLE_INTERIOR_GLOBAL_DATA_KICK_PLAYERS_OUT_OF_ARMORY_TRUCK_INTERIOR")
		
		CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(LocalPlayer)].SimpleInteriorBD.iBSTwo,  BS_SIMPLE_INTERIOR_GLOBAL_BS_TWO_DATA_LEAVE_ARMORY_TRUCK_TRAILER_ON_DELIVER) 
		CDEBUG1LN(DEBUG_SPAWNING, " MANAGE_LOCAL_PLAYER_INIT -Clearing BS_SIMPLE_INTERIOR_GLOBAL_BS_TWO_DATA_LEAVE_ARMORY_TRUCK_TRAILER_ON_DELIVER")
		
		INITIALISE_BOSS_TRUCK_SECTIONS()
	ENDIF
	
	INT iveh
	FOR iveh = 0 TO (FMMC_MAX_VEHICLES-1)
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetSeven, ciFMMC_VEHICLE7_SNIPER_HIT_INDICATOR)
			// Spoke to connor. Duplicates don't matter, so long as it's not a huge number of duplicates.
			ADD_VALID_VEHICLE_HIT_HASH(ENUM_TO_INT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn))
			PRINTLN("[LM][MC_SET_UP_VEH] - ciFMMC_VEHICLE7_SNIPER_HIT_INDICATOR is set. Calling ADD_VALID_VEHICLE_HIT_HASH")
		ENDIF
	ENDFOR
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciENABLE_AVENGER_INTERIOR_ENTRY)
		PRINTLN("[RCC MISSION] MANAGE_LOCAL_PLAYER_INIT - AVENGER ENTRY IS ENABLED")
		SET_ALLOW_INTERIOR_SCRIPT_FOR_MISSION(TRUE)
	ENDIF
		
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciLOCAL_PLAYER_ARROW_TEAM_COLOUR)
		SET_LOCAL_PLAYER_ARROW_TO_CURRENT_HUD_COLOUR(TRUE)
	ENDIF

	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].iTeamBitset3, ciBS3_TEAM_PLAYER_PREVENT_FALLS_OUT_WHEN_KILLED)
		IF PREVENT_LOCAL_PLAYER_FALLING_OUT_WHEN_DEAD()
			IF GET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_FallsOutOfVehicleWhenKilled)
				SET_BIT(iLocalBoolCheck29, LBOOL29_SAVING_FALLOUT_FLAG)
			ELSE
				CLEAR_BIT(iLocalBoolCheck29, LBOOL29_SAVING_FALLOUT_FLAG)
			ENDIF
		ENDIF
	ENDIF
	
	SET_UP_LOCAL_PLAYER_PROOF_SETTINGS()
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciUSE_STUNT_RP_BONUS)
		PLAYSTATS_START_TRACKING_STUNTS()
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyThree, ciOptionsBS23_DisableHeightOnPlayerBlips)
		PRINTLN("[ML] MANAGE_LOCAL_PLAYER_INIT - Allowing height blips")
		ALLOW_PLAYER_HEIGHTS(TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyThree, ciOptionsBS23_EnableArenaVehicleAmmo)
		fVehicleWeaponShootingTime = g_FMMC_STRUCT.fVehicleWeaponFireDrainTime
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyThree, ciOptionsBS23_EnableWeaponsInFreemodeShops)
		ALLOW_WEAPONS_IN_SHOPS(TRUE)
	ENDIF
	
	IF g_FMMC_STRUCT.fFireSpreadMultiplier > -0.1
		PRINTLN("[LM][RCC MISSION] MANAGE_LOCAL_PLAYER_INIT, fFireSpreadMultiplier: ", g_FMMC_STRUCT.fFireSpreadMultiplier, " Calling SET_FLAMMABILITY_MULTIPLIER") 
		SET_FLAMMABILITY_MULTIPLIER(g_FMMC_STRUCT.fFireSpreadMultiplier)
	ENDIF
	
	MPSV_GET_DISPLAY_SLOT_FROM_SAVE_SLOT(g_iMyRaceModelChoice, MC_playerBD_1[iLocalPart].iDisplaySlot) 
	PRINTLN("[RCC MISSION] 5456221, MANAGE_LOCAL_PLAYER_INIT, iDisplaySlot: ", MC_playerBD_1[iLocalPart].iDisplaySlot, " g_iMyRaceModelChoice = ", g_iMyRaceModelChoice) 
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_EnableCasino_InteriorScripts)
	AND NOT g_SimpleInteriorData.bAllowInteriorScriptForActivitySession
		SET_ALLOW_INTERIOR_SCRIPT_FOR_MISSION(TRUE)
	ENDIF

	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_EnableDroneDeployment)	
		PRINTLN("[LM][RCC MISSION] MANAGE_LOCAL_PLAYER_INIT: DRONE AVAILABLE")
		SET_ENABLE_DRONE_USE_FROM_PI_MENU_IN_MISSION(TRUE)
	ELSE
		PRINTLN("[LM][RCC MISSION] MANAGE_LOCAL_PLAYER_INIT: DRONE NOT AVAILABLE")
		SET_ENABLE_DRONE_USE_FROM_PI_MENU_IN_MISSION(FALSE)
	ENDIF
	
	MC_playerBD[iLocalPart].iStartingOutfit = MC_playerBD[iLocalPart].iOutfit
	PRINTLN("[LM][RCC MISSION] MANAGE_LOCAL_PLAYER_INIT, MC_playerBD[iLocalPart].iStartingOutfit = ", MC_playerBD[iLocalPart].iStartingOutfit)
	
	PROCESS_RESTORE_GRABBED_CASH()
	
	SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_ForceBlipSecurityPedsIfPlayerIsWanted, IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciBLIP_SECURITY_PEDS_DURING_WANTED))
	
	SET_PED_CAN_LOSE_PROPS_ON_DAMAGE(LocalPlayerPed, NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyNine, ciOptionsBS29_PreventPlayerFromLosingHatOnDamage))
	
	SET_BIT(g_iBS1_Mission, ciBS1_Mission_AllowPVVehiclesInBunker)
	
ENDPROC

PROC PREDEFINE_INTERACTABLE_DATA()
	
	PRINTLN("[Interactables] PREDEFINE_INTERACTABLE_DATA")
	
	INT iInteractable
	FOR iInteractable = 0 TO FMMC_MAX_NUM_INTERACTABLES - 1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].mnInteractable_Model = DUMMY_MODEL_FOR_SCRIPT
			RELOOP
		ENDIF
		
		iInteractableCachedHackingStructIndexes[iInteractable] = CALCULATE_HACKING_STRUCT_INDEX_FOR_ENTITY(iInteractable, ciENTITY_TYPE_INTERACTABLES)
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].sConsequences.iInteractable_ConsequencesBS, ciInteractableConsequences_DeployEMP)
			iInteractable_EMPChargesRequired++
			PRINTLN("[Interactables][Interactable ", iInteractable, "] PREDEFINE_INTERACTABLE_DATA | Incrementing iInteractable_EMPChargesRequired to ", iInteractable_EMPChargesRequired)
		ENDIF
		
		iInteractable_CachedLinkedInteractable[iInteractable] = -1
				
		INT iLinkedInteractable
		FOR iLinkedInteractable = 0 TO FMMC_MAX_NUM_INTERACTABLES - 1
			IF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].sConsequences.iInteractable_LinkedInteractablesBS, iLinkedInteractable)
				CACHE_LINKED_INTERACTABLE(iInteractable, iLinkedInteractable)
				BREAKLOOP
			ENDIF
		ENDFOR
	ENDFOR
ENDPROC

PROC PROCESS_START_OF_MISSION_PLAYER_BD_CONTINUITY()
	
	INT i
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_PrerequisiteTracking)
		FOR i = 0 TO HBCA_BS_MISSION_CONTROLLER_MAX_LONG - 1
			MC_playerBD[iLocalPart].iPrerequisiteBS[i] = sMissionLocalContinuityVars.iPrerequisiteBS[i]
			PRINTLN("[CONTINUITY] - PROCESS_START_OF_MISSION_PLAYER_BD_CONTINUITY - Setting iPrerequisiteBS[", i, "] from sMissionLocalContinuityVars.iPrerequisiteBS[", i, "]: ", sMissionLocalContinuityVars.iPrerequisiteBS[i])
		ENDFOR
	ENDIF
	
	MC_Playerbd[iLocalPart].iNumPedKills			= sMissionLocalContinuityVars.iNumPedKills
	MC_Playerbd[iLocalPart].iAmbientCopsKilled		= sMissionLocalContinuityVars.iAmbientCopsKilled
	MC_Playerbd[iLocalPart].iNumHeadshots			= sMissionLocalContinuityVars.iNumHeadshots
	MC_Playerbd[iLocalPart].iStartHits				= sMissionLocalContinuityVars.iStartHits
	MC_Playerbd[iLocalPart].iStartShots				= sMissionLocalContinuityVars.iStartShots
	MC_Playerbd[iLocalPart].iCivilianKills			= sMissionLocalContinuityVars.iCivilianKills
	MC_Playerbd[iLocalPart].iNumPlayerDeaths		= sMissionLocalContinuityVars.iNumPlayerDeaths
	MC_Playerbd[iLocalPart].iHackTime				= sMissionLocalContinuityVars.iHackTime
	MC_Playerbd[iLocalPart].iNumHacks				= sMissionLocalContinuityVars.iNumHacks
	MC_Playerbd[iLocalPart].iWantedTime				= sMissionLocalContinuityVars.iWantedTime
	MC_Playerbd[iLocalPart].iNumWantedLose			= sMissionLocalContinuityVars.iNumWantedLose
	MC_Playerbd[iLocalPart].iMedalObjectives		= sMissionLocalContinuityVars.iMedalObjectives
	MC_Playerbd[iLocalPart].iMedalInteractables		= sMissionLocalContinuityVars.iMedalInteractables
	MC_Playerbd[iLocalPart].iMedalEquipment			= sMissionLocalContinuityVars.iMedalEquipment
	MC_Playerbd[iLocalPart].iStartStealthKills		= sMissionLocalContinuityVars.iStartStealthKills
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iTeamBitset4, ciBS4_TEAM_PER_PLAYER_THERMAL_CHARGES)
		IF((HAS_FIRST_STRAND_MISSION_BEEN_PASSED_NO_RESET() OR HAS_FIRST_STRAND_MISSION_BEEN_PASSED())
		OR ((IS_THIS_A_QUICK_RESTART_JOB() OR IS_CORONA_INITIALISING_A_QUICK_RESTART()) AND NOT IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_ClearOnQuickRestart)))
			MC_playerBD_1[iLocalPart].iThermitePerPlayerRemaining = g_sTransitionSessionData.sStrandMissionData.iPlayerThermalCharges
		ELSE
			g_sTransitionSessionData.sStrandMissionData.iPlayerThermalCharges = 0
			MC_playerBD_1[iLocalPart].iThermitePerPlayerRemaining = g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].iTeamThermalCharges //Use the creator setting
		ENDIF
		PRINTLN("[CONTINUITY] - PROCESS_START_OF_MISSION_PLAYER_BD_CONTINUITY - ciBS4_TEAM_PER_PLAYER_THERMAL_CHARGES enabled. Setting MC_playerBD_1.iThermitePerPlayerRemaining to: ", MC_playerBD_1[iLocalPart].iThermitePerPlayerRemaining)
	ENDIF

	IF NOT MC_SHOULD_WE_START_FROM_CHECKPOINT()
	AND NOT IS_MISSION_START_CHECKPOINT_SET(1)
		sMissionLocalContinuityVars.iTripSkipCost = 0
	ENDIF
	MC_playerBD[iLocalPart].TSPlayerData.iCost = sMissionLocalContinuityVars.iTripSkipCost

ENDPROC

PROC CLIENT_INITIALIZE_BROADCAST_DATA()
	
	PRINTLN("[LM][PROCESS_PRE_GAME][CLIENT_INITIALIZE_BROADCAST_DATA] Starting...")
	
	INT i = 0
	
	// Set up Heist/Contact Mission player BD.
	IF GET_CURRENT_HEIST_MISSION(TRUE) > -1
		MC_PlayerBD[iLocalPart].iCurrentHeistCompletionStat = GET_MP_INT_CHARACTER_STAT(GET_HEIST_MISSION_COMPLETION_STAT(GET_CURRENT_HEIST_MISSION(TRUE)))
		
		PRINTLN("[LM][PROCESS_PRE_GAME][CLIENT_INITIALIZE_BROADCAST_DATA] GET_CURRENT_HEIST_MISSION(): ", GET_CURRENT_HEIST_MISSION(), " Setting MC_PlayerBD[iLocalPart].iCurrentHeistCompletionStat: ", MC_PlayerBD[iLocalPart].iCurrentHeistCompletionStat, " for iLocalPart: ", iLocalPart, " GET_CURRENT_HEIST_COMPLETION_STAT_FOR_LEADER(): ", GET_CURRENT_HEIST_COMPLETION_STAT_FOR_LEADER(TRUE))
		 
		MP_INT_STATS mpStat 
		INT iBSTemp[HBCA_BS_MISSION_CONTROLLER_MAX_LONG]	
		FOR i = 0 TO HBCA_BS_MISSION_CONTROLLER_MAX-1
			IF i = HBCA_BS_MAX    
				RELOOP
			ENDIF
					
			mpStat = GET_HEIST_MISSION_COMPLETION_STAT(i)
		
			IF mpStat = INT_TO_ENUM(MP_INT_STATS, 0)
				RELOOP
			ENDIF
			
			IF i != HBCA_BS_MAX          
			AND GET_MP_INT_CHARACTER_STAT(mpStat) > 0
				FMMC_SET_LONG_BIT(iBSTemp, i)
			ENDIF			
		ENDFOR	
		
		FOR i = 0 TO HBCA_BS_MISSION_CONTROLLER_MAX_LONG-1
			MC_PlayerBD[iLocalPart].iSpecificHeistCompletionStatBS[i] = iBSTemp[i]
			PRINTLN("[LM][PROCESS_PRE_GAME][CLIENT_INITIALIZE_BROADCAST_DATA] Setting iSpecificHeistCompletionStatBS[", i, "]: ", MC_PlayerBD[iLocalPart].iSpecificHeistCompletionStatBS[i], " for iLocalPart: ", iLocalPart)
		ENDFOR
	ENDIF
	
	IF DOES_MISSION_USE_PLAYER_OWNED_SUBMARINE()
		CACHE_PLAYER_SUBMARINE_COLOURS()
	ENDIF
	
	INT iInteraction
	FOR iInteraction = 0 TO ciINTERACTABLE_MAX_ONGOING_INTERACTIONS - 1
		MC_playerBD[iLocalPart].iCurrentInteractables[iInteraction] = -1
	ENDFOR

	PROCESS_START_OF_MISSION_PLAYER_BD_CONTINUITY()
		
ENDPROC

PROC SET_SPAWN_GROUP_CAN_BE_ACTIVATED_BY_AN_ENTITY_MOD_BS(INT iSpawnGroupAdditionalFunctionalityBS, INT iModifySpawnGroupOnEvent, INT &iModifySpawnSubGroupOnEventBS[])
	
	IF NOT IS_BIT_SET(iSpawnGroupAdditionalFunctionalityBS, ciSPAWN_GROUP_ADDITIONAL_ENTITY_FUNCTIONALITY_ACTIVATE_ON_EVENT)
		EXIT
	ENDIF
	
	INT iSpawnGroup
	FOR iSpawnGroup = 0 TO FMMC_MAX_RANDOM_ENTITY_SPAWN_GROUPS-1
	
		IF IS_BIT_SET(iModifySpawnGroupOnEvent, iSpawnGroup)
			PRINTLN("[SpawnGroups] - SET_SPAWN_GROUP_CAN_BE_ACTIVATED_BY_AN_ENTITY_MOD_BS - iSpawnGroup: ", iSpawnGroup, " could be enabled later.")
			SET_BIT(iSpawnGroupMightSpawnLaterBS, iSpawnGroup)
		ENDIF
		
		INT iSubSpawnGroup
		FOR iSubSpawnGroup = 0 TO ciMAX_NUM_FMMC_ENTITY_SUB_SPAWN_GROUPS-1	
			IF IS_BIT_SET(iModifySpawnSubGroupOnEventBS[iSpawnGroup], iSubSpawnGroup)
				PRINTLN("[SpawnGroups] - SET_SPAWN_GROUP_CAN_BE_ACTIVATED_BY_AN_ENTITY_MOD_BS - iSubSpawnGroup: ", iSubSpawnGroup, " in SpawnGroup: ", iSpawnGroup, " could be enabled later.")
				SET_BIT(iSpawnSubGroupMightSpawnLaterBS[iSpawnGroup], iSubSpawnGroup)
			ENDIF
		ENDFOR
		
	ENDFOR
ENDPROC

PROC CLIENT_PRE_DEFINE_LOCAL_DATA()
	INT i, iPedsSync
	
	INITIALISE_INT_ARRAY(sLocalPlayerAbilities.sRideAlongData.iRideAlongPedIndexes, -1)
	INITIALISE_INT_ARRAY(sLocalPlayerAbilities.sAmbushData.iAmbushVehicleIndexes, -1)
	INITIALISE_INT_ARRAY(sLocalPlayerAbilities.sAmbushData.iAmbushPickupIndexes, -1)
	INITIALISE_INT_ARRAY(sLocalPlayerAbilities.sAmbushData.iAmbushPedIndexes, -1)
	INITIALISE_INT_ARRAY(sLocalPlayerAbilities.sDistractionData.iDistractionPedIndexes, -1)
	
	INITIALISE_INT_ARRAY(iObjectCachedHackingStructIndexes, -1)
	INITIALISE_INT_ARRAY(iInteractableCachedHackingStructIndexes, -1)
	
	FOR i = 0 TO (MAX_SOUNDS - 1)
		sHacking[i] = -1
	ENDFOR	
	FOR i = 0 TO ciTOTAL_PTFX-1
		FlareSoundID[i] = -1
	ENDFOR	
	FOR i = 0 TO (ci_TOTAL_DISTANCE_CHECK_BARS - 1)
		iGPSTrackingMeterPedIndex[i] = -1
	ENDFOR	
	INITIALISE_INT_ARRAY(iFuseMinigameObjIndexes, -1)
	FOR i = 0 TO (FMMC_MAX_NUM_OBJECTS -1)
		iInteractObject_AttachedVehicle[i] = -1
		iObjCachedCamIndexes[i] = -1
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectMinigame = ciFMMC_OBJECT_MINIGAME_TYPE__CHECK_FUSEBOX
			iFuseMinigameObjIndexes[ciFUSE_MINIAME_INDEXES__FUSEBOX] = i
		ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectMinigame = ciFMMC_OBJECT_MINIGAME_TYPE__INSERT_FUSE
			SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectInteractionAnim_AltAnimSet
				CASE ciIW_ALTANIMSET__INSERT_FUSE_1ST
					iFuseMinigameObjIndexes[ciFUSE_MINIAME_INDEXES__FUSE_1] = i
				BREAK
				CASE ciIW_ALTANIMSET__INSERT_FUSE_2ND
					iFuseMinigameObjIndexes[ciFUSE_MINIAME_INDEXES__FUSE_2] = i
				BREAK
				CASE ciIW_ALTANIMSET__INSERT_FUSE_3RD
					iFuseMinigameObjIndexes[ciFUSE_MINIAME_INDEXES__FUSE_3] = i
				BREAK
				CASE ciIW_ALTANIMSET__INSERT_FUSE_FINAL
					iFuseMinigameObjIndexes[ciFUSE_MINIAME_INDEXES__FUSE_4] = i
				BREAK
			ENDSWITCH
		ENDIF
		
	ENDFOR
	FOR i = 0 TO FMMC_MAX_NUM_DYNOPROPS - 1
		iDynoPropCachedCamIndexes[i] = -1
	ENDFOR
	FOR i = 0 TO (MAX_NUM_MC_PLAYERS-1)
		iVisualAidCachedTeam[i] = -1
	ENDFOR	
	FOR i = 0 TO (FMMC_MAX_WATER_CALMING_QUADS - 1)
		iWaterCalmingQuad[i] = -1
	ENDFOR		
	FOR i = 0 TO FMMC_MAX_WARP_PORTALS-1
		IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sWarpPortals[i].vStartCoord)
			sWarpPortal.iMaxNumberOfPortals = i+1 // Highest index value.
		ENDIF
		sWarpPortal.colPortalCached[i] = INT_TO_ENUM(HUD_COLOURS, -1)
	ENDFOR	
	
	PRINTLN("CLIENT_PRE_DEFINE_LOCAL_DATA - sWarpPortal.iMaxNumberOfPortals: ", sWarpPortal.iMaxNumberOfPortals)
	
	INITIALISE_INT_ARRAY(iAssociatedGOTOCustomScenarioIndex, -1)
	FOR i = 0 TO ciFMMC_MAX_CUSTOM_SCENARIOS_ACTIVE - 1
		INITIALISE_INT_ARRAY(iAssociatedGOTOCustomScenarioInt[i], -1)
	ENDFOR
	
	INITIALISE_INT_ARRAY(sInteractWithVars.iInteractWith_RenderTargetIDs, -1)
	INITIALISE_INT_ARRAY(iSlidingPropSoundIDs, -1)
	INITIALISE_INT_ARRAY(iSlidingPropSoundIDUserIndex, -1)
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfProps - 1
		IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].vPropSlideVector)
			INT iSlidingPropSoundIDIndex
			FOR iSlidingPropSoundIDIndex = 0 TO ciMAX_SLIDING_PROP_SOUND_IDS - 1
				IF iSlidingPropSoundIDUserIndex[iSlidingPropSoundIDIndex] > -1
					// This slot is already taken!
					RELOOP
				ENDIF
				
				iSlidingPropSoundIDUserIndex[iSlidingPropSoundIDIndex] = i
				PRINTLN("[Props][Prop ", i, "] CLIENT_PRE_DEFINE_LOCAL_DATA | Sliding prop set up to use iSlidingPropSoundIDs[", iSlidingPropSoundIDIndex, "]")
				BREAKLOOP
			ENDFOR
		ENDIF
	ENDFOR
	
	FOR i = 0 TO ciMAX_SHARED_ASSOCIATED_GOTO_TASK_DATA - 1
		sLocalSharedAssGotoTaskData.iProgressOverrideProgress[i] = ASSOCIATED_GOTO_TASK_POOL_START__STAY_ON_CURRENT
		sLocalSharedAssGotoTaskData.iProgressOverridePool[i] = ASSOCIATED_GOTO_TASK_POOL_INDEX__OFF
		sLocalSharedAssGotoTaskData.iProgress[i] = -1
	ENDFOR
	
	FOR i = 0 TO ciMAX_FMMC_DRONES_ACTIVE - 1
		sLocalFmmcDroneData[i].vTarget = ZERO_VECTOR()		
	ENDFOR
	
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfPeds - 1
		FOR iPedsSync = 0 TO FMMC_MAX_PED_SCENE_SYNCED_PEDS-1
			sMissionPedsLocalVars[i].iSyncedPeds[iPedsSync] = -1
		ENDFOR
		sMissionPedsLocalVars[i].iSyncScene = -1
		
		iPedGotoAssProgressOverrideProgress[i] = ASSOCIATED_GOTO_TASK_POOL_START__STAY_ON_CURRENT
		iPedGotoAssProgressOverridePool[i] = ASSOCIATED_GOTO_TASK_POOL_INDEX__OFF
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iVehicle > -1
			IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iVehicle].iVehicleInVehicle_ContainerVehicleIndex > -1
				iVehicleInVehicleEmergePeds++
				PRINTLN("[VehicleInVehicle][Ped ", i, "] CLIENT_PRE_DEFINE_LOCAL_DATA | Increasing iVehicleInVehicleEmergePeds to ", iVehicleInVehicleEmergePeds)
			ENDIF
		ENDIF
		
		IF IS_ENTITY_USING_THIS_SPAWN_CONDITION_FLAGS(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].eSpawnConditionFlag, SPAWN_CONDITION_FLAG_RIDE_ALONG_ABILITY_ACTIVATED)
			INT iRideAlongPeds = 0
			
			FOR iRideAlongPeds = 0 TO ciRIDE_ALONG_ABILITY_PEDS - 1
				IF sLocalPlayerAbilities.sRideAlongData.iRideAlongPedIndexes[iRideAlongPeds] > -1
					RELOOP // This slot is already full
				ENDIF
				
				sLocalPlayerAbilities.sRideAlongData.iRideAlongPedIndexes[iRideAlongPeds] = i
				PRINTLN("[PlayerAbilities][RideAlongAbility][Ped ", i, "] CLIENT_PRE_DEFINE_LOCAL_DATA | Caching iRideAlongPedIndexes[", iRideAlongPeds, "] as ", sLocalPlayerAbilities.sRideAlongData.iRideAlongPedIndexes[iRideAlongPeds])
				BREAKLOOP
			ENDFOR
		ENDIF
		
		IF IS_ENTITY_USING_THIS_SPAWN_CONDITION_FLAGS(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].eSpawnConditionFlag, SPAWN_CONDITION_FLAG_AMBUSH_ABILITY_ACTIVATED)
			INT iAmbushPeds = 0
			
			FOR iAmbushPeds = 0 TO ciAMBUSH_ABILITY_PEDS - 1
				IF sLocalPlayerAbilities.sAmbushData.iAmbushPedIndexes[iAmbushPeds] > -1
					RELOOP // This slot is already full
				ENDIF
				
				sLocalPlayerAbilities.sAmbushData.iAmbushPedIndexes[iAmbushPeds] = i
				PRINTLN("[PlayerAbilities][AmbushAbility][Ped ", i, "] CLIENT_PRE_DEFINE_LOCAL_DATA | Caching iAmbushPedIndexes[", iAmbushPeds, "] as ", sLocalPlayerAbilities.sAmbushData.iAmbushPedIndexes[iAmbushPeds])
				BREAKLOOP
			ENDFOR
		ENDIF
		
		IF IS_ENTITY_USING_THIS_SPAWN_CONDITION_FLAGS(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].eSpawnConditionFlag, SPAWN_CONDITION_FLAG_DISTRACTION_ABILITY_ACTIVATED)
			INT iDistractionPeds = 0
			
			FOR iDistractionPeds = 0 TO ciDISTRACTION_ABILITY_VEHICLES - 1
				IF sLocalPlayerAbilities.sDistractionData.iDistractionPedIndexes[iDistractionPeds] > -1
					RELOOP // This slot is already full
				ENDIF
				
				sLocalPlayerAbilities.sDistractionData.iDistractionPedIndexes[iDistractionPeds] = i
				PRINTLN("[PlayerAbilities][DistractionAbility][Ped ", i, "] CLIENT_PRE_DEFINE_LOCAL_DATA | Caching iDistractionPedIndexes[", iDistractionPeds, "] as ", sLocalPlayerAbilities.sDistractionData.iDistractionPedIndexes[iDistractionPeds])
				BREAKLOOP
			ENDFOR
		ENDIF
	ENDFOR
	
	INT iAmbushVehiclesFound = 0
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles - 1
		
		IF IS_ENTITY_USING_THIS_SPAWN_CONDITION_FLAGS(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].eSpawnConditionFlag, SPAWN_CONDITION_FLAG_AMBUSH_ABILITY_ACTIVATED)
			sLocalPlayerAbilities.sAmbushData.iAmbushVehicleIndexes[iAmbushVehiclesFound] = i
			PRINTLN("[PlayerAbilities][AmbushAbility][Vehicle ", i, "] CLIENT_PRE_DEFINE_LOCAL_DATA | Caching iAmbushVehicleIndexes[", iAmbushVehiclesFound, "] as ", sLocalPlayerAbilities.sAmbushData.iAmbushVehicleIndexes[iAmbushVehiclesFound])
			iAmbushVehiclesFound++
		ENDIF
		
		INT iLinkedPed = 0
		FOR iLinkedPed = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfPeds - 1			
			IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iLinkedPed].iSpookPedOnVehicleTouchedBS, i)
				RELOOP
			ENDIF
			
			SET_BIT(iVehTrackTouchedByPlayerBS, i)
			PRINTLN("CLIENT_PRE_DEFINE_LOCAL_DATA - iPed: ", iLinkedPed, " is tracking to see whether Vehicle: ", i, " gets touched by the Player. Setting iVehTrackTouchedByPlayerBS")			
		ENDFOR
		
		IF iCachedMOCIndex = -1 
		AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn = TRAILERLARGE
			iCachedMOCIndex = i
			PRINTLN("[Vehicles][Vehicle ", i, "] CLIENT_PRE_DEFINE_LOCAL_DATA - MOC Found")
		ENDIF
		
	ENDFOR
	
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfObjects - 1
		iObjectCachedHackingStructIndexes[i] = CALCULATE_HACKING_STRUCT_INDEX_FOR_ENTITY(i, ciENTITY_TYPE_OBJECT)
	ENDFOR
	
	FOR i = 0 TO MAX_NUM_CCTV_CAM-1
		iCCTVCameraSpottedPlayer[i] = -1
		iSoundAlarmCCTV[i] = -1
	ENDFOR
	
	INT iIslandAirDefenceTurrets = 0
	FOR iIslandAirDefenceTurrets = 0 TO ciIslandAirDefenceTurrets - 1
		iIslandAirDefenceTurretAudioID[iIslandAirDefenceTurrets] = -1
	ENDFOR
	
	INITIALISE_INT_ARRAY(iPartMedalRanks, -1)
	
	INT iAmbushPickupsFound = 0
	FOR i = 0 TO FMMC_MAX_WEAPONS - 1
		g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].id = NULL
		
		IF i >= g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons
			BREAKLOOP
		ENDIF
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iCustomPickupType = ciCUSTOM_PICKUP_TYPE__MISSION_EQUIPMENT
		AND g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iCustomPickupModel != -1
			iMissionEquipmentPickupsMax[g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iCustomPickupModel]++
			PRINTLN("[WEAPON PICKUPS][Weapon ", i, "] CLIENT_PRE_DEFINE_LOCAL_DATA | iMissionEquipmentPickupsMax[", g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iCustomPickupModel, "] is now ", iMissionEquipmentPickupsMax[g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iCustomPickupModel])
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iPlacedBitset, ciFMMC_WEP_MissionEquipmentXofXTicker)
			SET_BIT(iMissionEquipment_DisplayXofXTickersForEquipmentTypeBS, g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iCustomPickupModel)
			PRINTLN("[WEAPON PICKUPS][Weapon ", i, "] CLIENT_PRE_DEFINE_LOCAL_DATA | 'Collected X of X' tickers will now display when picking up pickups with mission equipment type ", g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iCustomPickupModel)
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iPlacedBitset, ciFMMC_WEP_MissionEquipmentCollectionHUD)
			SET_BIT(iMissionEquipment_DisplayCollectionHUDForEquipmentTypeBS, g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iCustomPickupModel)
			PRINTLN("[WEAPON PICKUPS][Weapon ", i, "] CLIENT_PRE_DEFINE_LOCAL_DATA | Collection HUD now display after picking up a pickup with mission equipment type ", g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iCustomPickupModel)
		ENDIF
		
		IF IS_ENTITY_USING_THIS_SPAWN_CONDITION_FLAGS(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].eSpawnConditionFlag, SPAWN_CONDITION_FLAG_AMBUSH_ABILITY_ACTIVATED)
			sLocalPlayerAbilities.sAmbushData.iAmbushPickupIndexes[iAmbushPickupsFound] = i
			PRINTLN("[PlayerAbilities][AmbushAbility][Weapon ", i, "] CLIENT_PRE_DEFINE_LOCAL_DATA | Caching iAmbushPickupIndexes[", iAmbushPickupsFound, "] as ", sLocalPlayerAbilities.sAmbushData.iAmbushPickupIndexes[iAmbushPickupsFound])
			iAmbushPickupsFound++
		ENDIF
	ENDFOR
	
	sMissionBodyScannerData.vAudioSource		= <<2132.9, 2919.7, -60.2>>
	sMissionBodyScannerData.vMin		   		= <<2134.1, 2919.6, -62.9>>
	sMissionBodyScannerData.vMax		  	 	= <<2131.8, 2919.8, -58.6>>
		
	SyncCameraAndSkyswoopDownStages = 0	
	iVehCapTeam = -1		
	INITIALISE_CACHED_BLIP_NAME_INDEXES()
	
	// Clear iZoneIndicesOfTypeBlockGangChaseSpawn
	INT iGangChaseZoneCacheIndex
	FOR iGangChaseZoneCacheIndex = 0 TO ciFMMC_BLOCK_GANG_CHASE_SPAWN_ZONES_MAX -1 
		iZoneIndicesOfTypeBlockGangChaseSpawn[iGangChaseZoneCacheIndex] = -1
	ENDFOR
	iGangChaseZoneCacheIndex = 0
	
	INITIALISE_INT_ARRAY(iZoneIndicesOfTypeRuleEntityPrereq, -1)
	
	INT iZone
	FOR iZone = 0 TO FMMC_MAX_NUM_ZONES - 1
		fZoneRadiusOverride[iZone] = -1
		fZoneSizeMultiplier[iZone] = 1.0
		
		iPopMultiArea[iZone] = -1
		bipopScenarioArea[iZone] = NULL
		iZoneWaterCalmingQuadID[iZone] = -1
		iSpawnOcclusionIndex[iZone] = -1
		iGPSDisabledZones[iZone] = -1
		iDispatchBlockZone[iZone] = -1
		iFMMCAirDefenceZoneArea[iZone] = -1
		iNavBlockerZone[iZone] = -1
		
		// Cache zone indices which block gang chase spawning
		IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iType = ciFMMC_ZONE_TYPE__BLOCK_GANG_CHASE_SPAWN
			IF iGangChaseZoneCacheIndex >= 0 
			AND iGangChaseZoneCacheIndex < ciFMMC_BLOCK_GANG_CHASE_SPAWN_ZONES_MAX
				iZoneIndicesOfTypeBlockGangChaseSpawn[iGangChaseZoneCacheIndex] = iZone
				iGangChaseZoneCacheIndex++
			ELSE
				ASSERTLN("[CLIENT_PRE_DEFINE_LOCAL_DATA | ZONES] iGangChaseZoneCacheIndex out of bounds: ", iGangChaseZoneCacheIndex)
			ENDIF
		ENDIF
		
		// Cache zone indices which set prereqs based on rule entities
		IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iType = ciFMMC_ZONE_TYPE__RULE_ENTITY_PREREQ_ZONE
			IF iRuleEntityPrereqZoneCount >= 0 
			AND iRuleEntityPrereqZoneCount < ciFMMC_RULE_ENTITY_PREREQ_ZONE_MAX
				iZoneIndicesOfTypeRuleEntityPrereq[iRuleEntityPrereqZoneCount] = iZone
				iRuleEntityPrereqZoneCount++
			ELSE
				ASSERTLN("[CLIENT_PRE_DEFINE_LOCAL_DATA | ZONES] iRuleEntityPrereqZoneCount out of bounds: ", iRuleEntityPrereqZoneCount)
			ENDIF
		ENDIF
		
	ENDFOR
	INT iZoneType
	FOR iZoneType = 0 TO ciFMMC_ZONE_TYPE__MAX - 1
		iCurrentTriggeringZone[iZoneType] = -1
	ENDFOR
	
	INT iZoneTeam
	FOR iZoneTeam = 0 TO FMMC_MAX_TEAMS - 1
		iCurrentDropOffZone[iZoneTeam] = -1
	ENDFOR
	
	INT iPlayerAbilityIterator = 0
	BOOL bAllAbilitiesAreLeaderOnly = TRUE
	FOR iPlayerAbilityIterator = 0 TO ENUM_TO_INT(PA_MAX) - 1
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.sPlayerAbilities.sAbilities[iPlayerAbilityIterator].iAbilityBitset, ciAbilityBitset_AbilityIsHeistLeaderOnly)
			bAllAbilitiesAreLeaderOnly = FALSE
		ENDIF
	ENDFOR
	
	IF bAllAbilitiesAreLeaderOnly
		FMMC_SET_LONG_BIT(MPGlobalsAmbience.iPlayerAbilitiesBitset, ciPlayerAbilitiesBS_AllAbilitiesAreLeaderOnly)
		PRINTLN("CLIENT_PRE_DEFINE_LOCAL_DATA | Abilities menu is for the leader only!")
	ELSE
		FMMC_CLEAR_LONG_BIT(MPGlobalsAmbience.iPlayerAbilitiesBitset, ciPlayerAbilitiesBS_AllAbilitiesAreLeaderOnly)
		PRINTLN("CLIENT_PRE_DEFINE_LOCAL_DATA | Abilities menu is available for all players!")
	ENDIF
	
	PREDEFINE_INTERACTABLE_DATA()
	
	INT iDialogue = 0
	FOR iDialogue = 0 TO g_FMMC_STRUCT.iNumberOfDialogueTriggers - 1
		IF g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iVehicleTrigger != -1
			PRINTLN("[CLIENT_PRE_DEFINE_LOCAL_DATA | Setting iVehUsesCleanupDelayForDialogue for Vehicle: ", g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iVehicleTrigger, " Because of iDialogue: ", iDialogue)
			SET_BIT(iVehUsesCleanupDelayForDialogue, g_FMMC_STRUCT.sDialogueTriggers[iDialogue].iVehicleTrigger)
		ENDIF
	ENDFOR	
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciPRE_GAME_EMP)
	OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciTURN_OFF_LIGHTS_AT_START)
		TURN_OFF_ARTIFICIAL_LIGHTS_LOCALLY(FALSE)
	ENDIF
	
	FOR i = 0 TO FMMC_MAX_ENTITY_WITH_SPAWN_GROUP_MODIFY-1		
		IF i < g_FMMC_STRUCT_ENTITIES.iNumberOfPeds
			SET_SPAWN_GROUP_CAN_BE_ACTIVATED_BY_AN_ENTITY_MOD_BS(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iSpawnGroupAdditionalFunctionalityBS, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iModifySpawnGroupOnEvent, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iModifySpawnSubGroupOnEventBS)
		ENDIF
		
		IF i < g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles
			SET_SPAWN_GROUP_CAN_BE_ACTIVATED_BY_AN_ENTITY_MOD_BS(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iSpawnGroupAdditionalFunctionalityBS, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iModifySpawnGroupOnEvent, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iModifySpawnSubGroupOnEventBS)
		ENDIF
		
		IF i < g_FMMC_STRUCT_ENTITIES.iNumberOfObjects
			SET_SPAWN_GROUP_CAN_BE_ACTIVATED_BY_AN_ENTITY_MOD_BS(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSpawnGroupAdditionalFunctionalityBS, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iModifySpawnGroupOnEvent, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iModifySpawnSubGroupOnEventBS)
		ENDIF
		
		IF i < g_FMMC_STRUCT_ENTITIES.iNumberOfInteractables
			SET_SPAWN_GROUP_CAN_BE_ACTIVATED_BY_AN_ENTITY_MOD_BS(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].iSpawnGroupAdditionalFunctionalityBS, g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].iModifySpawnGroupOnEvent, g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].iModifySpawnSubGroupOnEventBS)
		ENDIF
		
		IF i < g_FMMC_STRUCT_ENTITIES.iNumberOfGoToLocations[0]
			SET_SPAWN_GROUP_CAN_BE_ACTIVATED_BY_AN_ENTITY_MOD_BS(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].iSpawnGroupAdditionalFunctionalityBS, g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].iModifySpawnGroupOnEvent, g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].iModifySpawnSubGroupOnEventBS)
		ENDIF
	ENDFOR
	
	// Cache whether or not we're using certain bits of functionality
	CACHE__MISSION_CONTAINS_A_PLACED_YACHT()
		
	FOR i = 0 TO g_FMMC_STRUCT.iNumberOfTeams-1
		
		iInventory_Starting[i] = g_FMMC_STRUCT.sTeamData[i].iStartingInventoryIndex
		PRINTLN("[WeaponInventory] iInventory_Starting[",i,"] = ", iInventory_Starting[i])
		
		IF IS_BIT_SET(g_FMMC_STRUCT.sTeamData[i].iTeamBitSet, ciTEAM_BS_SET_STARTING_INVENTORY_CHOSEN_IN_LOBBY)
			PRINTLN("[WeaponInventory] - Assigning Index: ", g_iMissionLobbyStartingInventoryIndex[i], " From lobby selection")
			iInventory_Starting[i] = g_iMissionLobbyStartingInventoryIndex[i]
		ENDIF
	ENDFOR
	
	FOR i = 0 TO MAX_MOCAP_CUTSCENES-1		
		IF IS_BIT_SET(g_fmmc_struct.sMocapCutsceneData[i].iCutsceneBitset3, ci_CSBS3_UseCloneOfOriginalPed)
			SET_BIT(iLocalBoolCheck29, LBOOL29_USING_MODEL_SWAP_AND_CUTSCENE_REQUIRES_ORIGINAL_PED)
			PRINTLN("[Player Model Swap] - Setting LBOOL29_USING_MODEL_SWAP_AND_CUTSCENE_REQUIRES_ORIGINAL_PED")
		ENDIF
	ENDFOR
	FOR i = 0 TO _POST_CASINO_HEIST__MAX_SCRIPTED_CUTSCENES-1
		IF IS_BIT_SET(g_fmmc_struct.sScriptedCutsceneData[i].iCutsceneBitset3, ci_CSBS3_UseCloneOfOriginalPed)
			SET_BIT(iLocalBoolCheck29, LBOOL29_USING_MODEL_SWAP_AND_CUTSCENE_REQUIRES_ORIGINAL_PED)
			PRINTLN("[Player Model Swap] - Setting LBOOL29_USING_MODEL_SWAP_AND_CUTSCENE_REQUIRES_ORIGINAL_PED")
		ENDIF
	ENDFOR
		
	//Clean up passed through globals
	MC_PlayerBD_VARS_STRUCT sEmptyPlayerBDVars
	MC_serverBD_VARS_STRUCT sEmptyServerBDVars
	MC_LocalVariables_VARS_STRUCT sEmptyLocalVars
	gMC_PlayerBD_VARS = sEmptyPlayerBDVars
	gBG_MC_serverBD_VARS = sEmptyServerBDVars
	gMC_LocalVariables_VARS = sEmptyLocalVars
ENDPROC

/// PURPOSE:
///    [FMMC2020] Unsorted stuff that needs tidied up and added to CLIENT_PRE_DEFINE_SERVER_DATA
PROC CLIENT_PRE_DEFINE_SERVER_DATA_UNSORTED()
	INT i 
	INT iTeam
	INT iPedBitset[FMMC_MAX_PEDS_BITSET]
	INT iNumberOfTeams = GET_NUMBER_OF_TEAMS_FOR_PRE_GAME()
	
	PRINTLN("[RCC MISSION] CLIENT_PRE_DEFINE_SERVER_DATA")
	
	MC_serverBD.sServerFMMC_EOM.bAllowedToVote = FALSE
	
	MC_serverBD.iPolice = g_FMMC_STRUCT.iPolice
	MC_serverBD.iOptionsMenuBitSet= g_FMMC_STRUCT.iOptionsMenuBitSet
	MC_serverBD.fVehicleDensity= GET_MISSION_VEHICLE_DENSITY_FROM_INT(g_FMMC_STRUCT.iTraffic)
	MC_serverBD.fPedDensity = GET_PED_DENSITY_FROM_INT(g_FMMC_STRUCT.iPedDensity)
	MC_serverBD.iDifficulty = g_FMMC_STRUCT.iDifficulity	

	PRINTLN("[RCC MISSION] CLIENT_PRE_DEFINE_SERVER_DATA - MC_serverBD.iTimeOfDay = ",MC_serverBD.iTimeOfDay )		
	PRINTLN("[RCC MISSION] CLIENT_PRE_DEFINE_SERVER_DATA - iNumberOfTeams = ", iNumberOfTeams)

	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciSUDDEN_DEATH_THAT_ENDS_WHEN_MORE_PEOPLE_ARE_IN_LOC)
		PRINTLN("[RCC MISSION] CLIENT_PRE_DEFINE_SERVER_DATA - SUDDEN DEATH ends when more people in loc")
		SET_BIT(MC_serverBD.iServerBitSet2, SBBOOL2_SUDDEN_DEATH_MOREINLOC)
		
		#IF IS_DEBUG_BUILD
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciSUDDEN_DEATH_THAT_ENDS_WHEN_NO_ENEMY_TEAMS)
			PRINTLN("[RCC MISSION] CLIENT_PRE_DEFINE_SERVER_DATA - SUDDEN DEATH, kill all enemies is also set...")
		ENDIF
		#ENDIF
	ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciSUDDEN_DEATH_THAT_ENDS_WHEN_NO_ENEMY_TEAMS)
		PRINTLN("[RCC MISSION] CLIENT_PRE_DEFINE_SERVER_DATA - SUDDEN DEATH ends when all enemies are dead")
		SET_BIT(MC_serverBD.iServerBitSet2, SBBOOL2_SUDDEN_DEATH_KILLALLENEMIES) 
	ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciSUDDEN_DEATH_THAT_ENDS_WHEN_ONLY_ONE_TEAM_ALIVE)
		PRINTLN("[KH][SSDS] CLIENT_PRE_DEFINE_SERVER_DATA - SUDDEN DEATH ends when only one team is left alive")
	 	SET_BIT(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_SUMO_PENNED_IN)
	ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciSUDDEN_DEATH_ONE_HIT_KILL_LAST_TEAM_STANDING)
		PRINTLN("CLIENT_PRE_DEFINE_SERVER_DATA - SUDDEN DEATH ends when only one team is left alive (one hit kills)")
	 	SET_BIT(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_BE_MY_VALENTINES)
	ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciSUDDEN_DEATH_TARGET)
		PRINTLN("CLIENT_PRE_DEFINE_SERVER_DATA - SUDDEN DEATH ends when all players are out of bullets (and a team has a higher score)")
	 	SET_BIT(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_TARGET)
	ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciSUDDEN_DEATH_IN_AND_OUT)
		PRINTLN("[KH][SUDDEN DEATH] CLIENT_PRE_DEFINE_SERVER_DATA - SUDDEN DEATH ends when a package is either delivered or dropped")
		SET_BIT(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_IN_AND_OUT)
	ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciSUDDEN_DEATH_POWER_PLAY)
		PRINTLN("[KH][SUDDEN DEATH] CLIENT_PRE_DEFINE_SERVER_DATA - SUDDEN DEATH - POWER PLAY - ENABLED")
		SET_BIT(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_POWER_PLAY)
	ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciPACKAGE_HELD_SUDDEN_DEATH)
		PRINTLN("CLIENT_PRE_DEFINE_SERVER_DATA - SUDDEN DEATH ends when all players are out of bullets (and a team has a higher score)")
	 	SET_BIT(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_PACKAGE_HELD)
	ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciSUDDEN_DEATH_KILLALLENEMIES_PENNED_IN)
		PRINTLN("CLIENT_PRE_DEFINE_SERVER_DATA - SUDDEN DEATH ends when only team is left alive inside the sphere")
	 	SET_BIT(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_KILLALLENEMIES_PENNED_IN)
	ENDIF
	
	IF (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciOPTIONS4_MAINTAIN_SPAWN_GROUPS_ON_RESTART) AND (IS_THIS_A_QUICK_RESTART_JOB() OR IS_CORONA_INITIALISING_A_QUICK_RESTART()))
	OR (IS_A_STRAND_MISSION_BEING_INITIALISED()	AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFive, ciOptionsBS25_MaintainSpawnGroupOverStrand_LoadOnly))
		MC_serverBD_4.rsgSpawnSeed = g_TransitionSessionNonResetVars.rsgFMMCSpawnSeed
		PRINTLN("[RCC MISSION][ran][SpawnGroups] CLIENT_PRE_DEFINE_SERVER_DATA - This is a quick restart / strand, loading spawn group data assigning g_TransitionSessionNonResetVars.iEntitySpawnSeedBS ", g_TransitionSessionNonResetVars.rsgFMMCSpawnSeed.iEntitySpawnSeedBS, " to servre data.")
		FOR i = 0 TO ciMAX_NUM_FMMC_ENTITY_SUB_SPAWN_GROUPS-1 
			PRINTLN("[RCC MISSION][ran][SpawnGroups] CLIENT_PRE_DEFINE_SERVER_DATA - This is a quick restart / strand, loading spawn group data assigning g_TransitionSessionNonResetVars.rsgFMMCSpawnSeed.iEntitySpawnSeed_SubGroupBS[",i,"] = ", g_TransitionSessionNonResetVars.rsgFMMCSpawnSeed.iEntitySpawnSeed_SubGroupBS[i])
		ENDFOR
	ENDIF
	
	IF g_FMMC_STRUCT.iShowPackageHudForTeam != 0			
		MC_serverBD_1.iTotalPackages = MC_serverBD.iNumObjHighestPriorityHeld[g_FMMC_STRUCT.iShowPackageHudForTeam - 1]
		PRINTLN("[AW][OBJECTS_TO_DEFEND_HUD]MC_ServerBroadcastData1.iTotalPackages: ", MC_serverBD_1.iTotalPackages)
	ENDIF
	
	PARTICIPANT_INDEX tempPart
	PLAYER_INDEX tempPlayer
	INT iPlayer
	
	PRINTLN("[PLAYER_LOOP] - CLIENT_PRE_DEFINE_SERVER_DATA")
	REPEAT MAX_NUM_MC_PLAYERS i 
		
		IF NOT HAS_FIRST_STRAND_MISSION_BEEN_PASSED()
//			IF g_MissionControllerserverBD_LB.sleaderboard[i].bInitialise = FALSE 
			IF NOT IS_BIT_SET(g_MissionControllerserverBD_LB.sleaderboard[i].iLbdBitSet, ciLBD_BIT_INITIALISE)
				g_MissionControllerserverBD_LB.sleaderboard[i].playerID 		= INVALID_PLAYER_INDEX()
				g_MissionControllerserverBD_LB.sleaderboard[i].iParticipant 	= -1
				g_MissionControllerserverBD_LB.sleaderboard[i].iTeam            = -1
//				g_MissionControllerserverBD_LB.sleaderboard[i].bInitialise = TRUE
				SET_BIT(g_MissionControllerserverBD_LB.sleaderboard[i].iLbdBitSet, ciLBD_BIT_INITIALISE)
			ENDIF
		ENDIF
		
		IF MC_serverBD.iNumberOfTeams > 0
			tempPart = INT_TO_PARTICIPANTINDEX(i)
			
			IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
			
				tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
				
				IF IS_NET_PLAYER_OK(tempPlayer, FALSE, FALSE)
				AND NOT IS_PLAYER_SCTV(tempPlayer)
				AND NOT DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(tempPlayer)
					
					iPlayer = NATIVE_TO_INT(tempPlayer)
					
					IF iPlayer >= 0
					AND iPlayer < MAX_NUM_MC_PLAYERS
						
						iTeam = GlobalPlayerBD_FM[iPlayer].sClientCoronaData.iTeamChosen
						
						IF iTeam >= 0
						AND iTeam < MC_serverBD.iNumberOfTeams
							PRINTLN("[RCC MISSION] CLIENT_PRE_DEFINE_SERVER_DATA - Part ",i," is on team ",iTeam,"; setting team as active!")
							
							SET_BIT(MC_serverbd.iServerBitSet, SBBOOL_TEAM_0_ACTIVE + iTeam)
							SET_BIT(MC_serverbd.iServerBitSet1, SBBOOL1_TEAM_0_WAS_ACTIVE + iTeam)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
	ENDREPEAT
	
	REPEAT COUNT_OF(MC_serverBD_1.piTimeBarWinners) i
		MC_serverBD_1.piTimeBarWinners[i] = INVALID_PLAYER_INDEX()
		PRINTLN("PRE_GAME MC_serverBD_1.piTimeBarWinners[", i, "] = INVALID_PLAYER_INDEX()")
	ENDREPEAT
	
	FOR iTeam = 0 TO (iNumberOfTeams-1)
		
		//Part that spooked a ped who will cause an aggro fail:
		MC_serverBD.iPartCausingFail[iTeam] = -1
		
		MC_serverBD.iEntityCausingFail[iTeam] = -1
		
		MC_serverBD.iPriorityLocation[iTeam] = -1
		
		MC_serverBD.iNextObjective[iTeam] = -1
	
		MC_serverBD.iMissionCriticalVeh[iTeam] = g_FMMC_STRUCT.iMissionCriticalvehBS[iTeam]
		
		FMMC_COPY_LONG_BITSET(g_FMMC_STRUCT.iMissionCriticalPedDoubleBS[iTeam], MC_serverBD.iMissionCriticalPed[iTeam], TRUE)
		
		MC_serverBD.iMissionCriticalObj[iTeam] = g_FMMC_STRUCT.iMissionCriticalObjBS[iTeam]
		
		IF g_FMMC_STRUCT.iInitialPoints[iTeam] != 0
			MC_serverBD.iTeamScore[iTeam] = g_FMMC_STRUCT.iInitialPoints[iTeam]
			PRINTLN("CLIENT_PRE_DEFINE_SERVER_DATA - [RCC MISSION] team: ",iTeam, " score increased to: ", MC_serverBD.iTeamScore[iTeam])
		ENDIF
		
		MC_serverBD.iRequiredDeliveries[iTeam] = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRequiredDeliveries[0]
		PRINTLN("[RCC MISSION] CLIENT_PRE_DEFINE_SERVER_DATA - MC_serverBD.iRequiredDeliveries[iTeam] client initialised as ",MC_serverBD.iRequiredDeliveries[iTeam])
		
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iTeamBitset4, ciBS4_TEAM_PER_PLAYER_THERMAL_CHARGES)
			IF (HAS_FIRST_STRAND_MISSION_BEEN_PASSED_NO_RESET() OR HAS_FIRST_STRAND_MISSION_BEEN_PASSED())
			OR ((IS_THIS_A_QUICK_RESTART_JOB() OR IS_CORONA_INITIALISING_A_QUICK_RESTART()) AND NOT IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_ClearOnQuickRestart))
				MC_serverBD_1.iTeamThermalCharges[iTeam] = g_sTransitionSessionData.sStrandMissionData.iTeamThermalCharges[iTeam]
			ELSE	
				MC_serverBD_1.iTeamThermalCharges[iTeam] = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamThermalCharges
			ENDIF
					
			PRINTLN("[RCC MISSION] CLIENT_PRE_DEFINE_SERVER_DATA - MC_serverBD_1.iTeamThermalCharges[iTeam] client initialised as ", MC_serverBD_1.iTeamThermalCharges[iTeam])
		ENDIF
		
		MC_serverBD.iVariableEntityRespawns = (GET_TOTAL_STARTING_PLAYERS()-1)
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciALLOW_ACTUAL_PLAYER_NUM_VEHICLE_SPAWNS)
			IF MC_serverBD.iVariableEntityRespawns < 0
				MC_serverBD.iVariableEntityRespawns = 0
			ENDIF
			IF MC_serverBD.iVariableEntityRespawns > 4
				MC_serverBD.iVariableEntityRespawns = 4
			ENDIF
		ENDIF
		
		MC_serverBD_1.iVariableNumBackupAllowed[iTeam] = MC_serverBD.iNumStartingPlayers[iTeam]
		IF MC_serverBD_1.iVariableNumBackupAllowed[iTeam] < 2
			MC_serverBD_1.iVariableNumBackupAllowed[iTeam] = 2
		ENDIF
		
		//fail reasons
		MC_serverBD.iReasonForObjEnd[iTeam] = -1			
		
		IF g_FMMC_STRUCT.sFMMCEndConditions[0].iMissionTimeLimit != ciFMMC_MISSION_END_TIME_IGNORE // for time limited games
			REINIT_NET_TIMER(MC_serverBD_3.tdLimitTimer)
		ENDIF
		
		FOR i = 0 TO (FMMC_MAX_RULES - 1)
			
			MC_serverBD_4.iPlayerRulePriority[i][iTeam] = g_FMMC_STRUCT.sPlayerRuleData[i].iPriority[iTeam]
			
			MC_serverBD_4.iPlayerRule[i][iTeam] = g_FMMC_STRUCT.sPlayerRuleData[i].iRule[iTeam]
			
			MC_serverBD.iPlayerRuleLimit[i][iTeam] = -1
			
			PRINTLN("CLIENT_PRE_DEFINE_SERVER_DATA - MC_serverBD.iPlayerRuleLimit[",i,"][",iTeam,"] = ", MC_serverBD.iPlayerRuleLimit[i][iTeam], " MC_serverBD.iPlayerRulePriority[",i,"][",iTeam,"]: ", MC_serverBD_4.iPlayerRulePriority[i][iTeam])
			
			IF MC_serverBD_4.iPlayerRulePriority[i][iTeam] < FMMC_MAX_RULES
				IF MC_serverBD_4.iPlayerRule[i][iTeam] = FMMC_OBJECTIVE_LOGIC_SCRIPTED_CUTSCENE
					MC_serverBD.iPlayerRuleLimit[i][iTeam] = g_FMMC_STRUCT.sPlayerRuleData[i].iPlayerRuleLimit[iTeam]
				ELSE
					MC_serverBD.iPlayerRuleLimit[i][iTeam] = GET_FMMC_OBJECTIVE_CAPTURES_OR_KILLS_FROM_SELECTION(g_FMMC_STRUCT.sPlayerRuleData[i].iPlayerRuleLimit[iTeam])
				ENDIF
				
				PRINTLN("CLIENT_PRE_DEFINE_SERVER_DATA - MC_serverBD.iPlayerRuleLimit[",i,"][",iTeam,"] = ", MC_serverBD.iPlayerRuleLimit[i][iTeam])
				SET_SERVER_TARGETTYPES_FOR_ENTITY(i,iTeam,ci_TARGET_PLAYER)
			ENDIF
			
			MC_serverBD.iPlayerRuleBitset[i][iTeam] = g_FMMC_STRUCT.sPlayerRuleData[i].iBitset[iTeam]
			
			IF MC_serverBD_4.iPlayerRulePriority[i][iTeam] <= MC_serverBD_4.iCurrentHighestPriority[iTeam] 
			AND MC_serverBD_4.iPlayerRulePriority[i][iTeam] < FMMC_MAX_RULES
				MC_serverBD_4.iCurrentHighestPriority[iTeam] = MC_serverBD_4.iPlayerRulePriority[i][iTeam]
			ENDIF
		ENDFOR
		
		FOR i = 0 TO (FMMC_MAX_GO_TO_LOCATIONS-1)
			MC_serverBD.iLocOwner[i] =-1
			MC_serverBD_4.iGotoLocationDataPriority[i][iTeam] = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].iPriority[iTeam]
			MC_serverBD_4.iGotoLocationDataRule[i][iTeam] = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].iRule[iTeam]
			
			IF MC_serverBD_4.iGotoLocationDataPriority[i][iTeam] < FMMC_MAX_RULES
				SET_SERVER_TARGETTYPES_FOR_ENTITY(i,iTeam,ci_TARGET_LOCATION)
			ENDIF
			
			IF MC_DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP(i, eSGET_GotoLoc)							
				IF MC_serverBD_4.iGotoLocationDataPriority[i][iTeam] <= MC_serverBD_4.iCurrentHighestPriority[iTeam] 
				AND MC_serverBD_4.iGotoLocationDataPriority[i][iTeam] < FMMC_MAX_RULES
					MC_serverBD_4.iCurrentHighestPriority[iTeam] = MC_serverBD_4.iGotoLocationDataPriority[i][iTeam]
				ENDIF
				PRINTLN("CLIENT_PRE_DEFINE_SERVER_DATA - MC_serverBD_4.iGotoLocationDataRule[",i,"][",iTeam,"] = ", MC_serverBD_4.iGotoLocationDataRule[i][iTeam], " MC_serverBD_4.iGotoLocationDataPriority[",i,"][",iTeam,"]: ", MC_serverBD_4.iGotoLocationDataPriority[i][iTeam], " Spawn Group is okay.")
			ENDIF
		ENDFOR

		FOR i = 0 TO (FMMC_MAX_PEDS-1)
		
			MC_serverBD.TargetPlayerID[i] = INVALID_PLAYER_INDEX()
			MC_serverBD_2.iTargetID[i] = -1
			MC_serverBD.niTargetID[i] = NULL
			MC_serverBD_4.iPedPriority[i][iTeam] = g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPriority[iTeam]
			MC_serverBD_4.iPedRule[i][iTeam] = g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iRule[iTeam]
			MC_serverBD_2.iAssociatedAction[i] = g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAssociatedAction

			MC_serverBD_2.iAssociatedGotoPoolIndex[i] = ASSOCIATED_GOTO_TASK_POOL_INDEX__USE_PED_DATA
			
			IF MC_serverBD_4.iPedPriority[i][iTeam] < FMMC_MAX_RULES
				SET_SERVER_TARGETTYPES_FOR_ENTITY(i,iTeam,ci_TARGET_PED)
			ENDIF
			
			IF MC_IS_PED_A_SCRIPTED_COP(i)
				FMMC_SET_LONG_BIT(MC_serverBD.iCopPed, i)
				PRINTLN("[RCC MISSION] CLIENT_PRE_DEFINE_SERVER_DATA - MC_serverBD.iCopPed: ",i)
			ENDIF
			
			IF NOT IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[i])
			OR MC_IS_PED_GOING_TO_SPAWN_LATER(i)
				IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPriority[iTeam] <= MC_serverBD_4.iCurrentHighestPriority[iTeam] 
				AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPriority[iTeam] < FMMC_MAX_RULES
					MC_serverBD_4.iCurrentHighestPriority[iTeam] = g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPriority[iTeam] 
				ENDIF
				IF NOT FMMC_IS_LONG_BIT_SET(iPedBitset, i)
					MC_serverBD_2.iCurrentPedRespawnLives[i] = GET_ENTITY_STARTING_RESPAWN_LIVES(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedRespawnLives)
					FMMC_SET_LONG_BIT(iPedBitset, i)
				ENDIF
				
				PRINTLN("[RCC MISSION] CLIENT_PRE_DEFINE_SERVER_DATA - iPedRespawn: ", MC_serverBD_2.iCurrentPedRespawnLives[i])
				
				MC_serverBD_2.iPedState[i] = ciTASK_CHOOSE_NEW_TASK
				MC_serverBD_2.iOldPedState[i] = ciTASK_CHOOSE_NEW_TASK
				
				IF NOT IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[i])
					PRINTLN("[RCC MISSION] CLIENT_PRE_DEFINE_SERVER_DATA - Setting iObjSpawnPedBitset for ped ",i)
					FMMC_SET_LONG_BIT(MC_serverBD.iObjSpawnPedBitset, i)
				ENDIF
				
			ENDIF
			
		ENDFOR
		FOR i = 0 TO (FMMC_MAX_VEHICLES-1)
			MC_serverBD.iVehRequestPart[i] = -1
			MC_serverBD.iDriverPart[i] = -1
			MC_serverBD_4.iVehPriority[i][iTeam] = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iPriority[iTeam]
			MC_serverBD_4.ivehRule[i][iTeam] = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iRule[iTeam]
			
			IF MC_serverBD_4.iVehPriority[i][iTeam] < FMMC_MAX_RULES
				SET_SERVER_TARGETTYPES_FOR_ENTITY(i,iTeam,ci_TARGET_VEHICLE)
			ENDIF
			
			IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[i])
				IF IS_VEHICLE_DRIVEABLE(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i]))
					SET_BIT(MC_serverBD_2.iVehSpawnedOnce, i)
					IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iPriority[iTeam] <= MC_serverBD_4.iCurrentHighestPriority[iTeam] 
					AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iPriority[iTeam] < FMMC_MAX_RULES
						MC_serverBD_4.iCurrentHighestPriority[iTeam] = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iPriority[iTeam] 
					ENDIF
				ENDIF
			ELIF MC_IS_VEH_GOING_TO_SPAWN_LATER(i)
				IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iPriority[iTeam] <= MC_serverBD_4.iCurrentHighestPriority[iTeam] 
				AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iPriority[iTeam] < FMMC_MAX_RULES
					MC_serverBD_4.iCurrentHighestPriority[iTeam] = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iPriority[iTeam] 
				ENDIF
			ENDIF
		ENDFOR
		
		//Set all vault drill games to start at -1
		FOR i = 0 TO (FMMC_MAX_VAULT_DRILL_GAMES -1)
			MC_serverBD.iObjVaultDrillIDs[i] = -1
		ENDFOR
		
		FOR i = 0 TO (FMMC_MAX_NUM_OBJECTS-1)
			MC_serverBD.iObjDelTeam[i] = -1
			MC_serverBD.iObjCarrier[i] = -1
			MC_serverBD.iObjHackPart[i] = -1
			
			IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].mn = PROP_BOX_WOOD02A_PU
			OR g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].mn = PROP_BOX_WOOD02A
			OR g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].mn = Prop_Box_Wood02A_MWS
			OR g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].mn = Prop_Container_LD_PU
			OR g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_Prop_SR_BoxWood_01"))
				SET_BIT(MC_serverBD.iFragableCrate,i)
			ENDIF
			
			MC_serverBD_4.iObjPriority[i][iTeam] = g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iPriority[iTeam]
			MC_serverBD_4.iObjRule[i][iTeam] = g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iRule[iTeam]
			
			CHECK_IF_OBJECT_IS_CONTAINER_AND_SET_SERVER_DATA( iTeam, i )
			
			IF MC_serverBD_4.iObjPriority[i][iTeam] < FMMC_MAX_RULES
				SET_SERVER_TARGETTYPES_FOR_ENTITY(i,iTeam,ci_TARGET_OBJECT)
			ENDIF
						
			IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectMinigame = ciFMMC_OBJECT_MINIGAME_TYPE__VAULT_DRILLING
				IF MC_serverBD.iObjVaultDrillIDs[0]  = -1
					PRINTLN("[RCC MISSION] CLIENT_PRE_DEFINE_SERVER_DATA - SETTING MC_serverBD.iObjVaultDrillIDs[0]  = i ", i)
					MC_serverBD.iObjVaultDrillIDs[0]  = i
				ELIF MC_serverBD.iObjVaultDrillIDs[1]  = -1
					PRINTLN("[RCC MISSION] CLIENT_PRE_DEFINE_SERVER_DATA - SETTING MC_serverBD.iObjVaultDrillIDs[1]  = i ", i)
					MC_serverBD.iObjVaultDrillIDs[1]  = i
				ELSE
					ASSERTLN("[RCC MISSION] CLIENT_PRE_DEFINE_SERVER_DATA - MORE THAN TWO VAULT DRILL GAMES HAVE BEEN PLACED.")
				ENDIF
			ENDIF
			
			IF NETWORK_DOES_NETWORK_ID_EXIST(GET_OBJECT_NET_ID(i))
			OR MC_IS_OBJ_GOING_TO_SPAWN_LATER(i)
				IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iPriority[iTeam] <= MC_serverBD_4.iCurrentHighestPriority[iTeam] 
				AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iPriority[iTeam] < FMMC_MAX_RULES
					MC_serverBD_4.iCurrentHighestPriority[iTeam] = g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iPriority[iTeam] 
				ENDIF

				IF MC_serverBD_2.iCurrentObjRespawnLives[i] > 0
				OR MC_serverBD_2.iCurrentObjRespawnLives[i] = UNLIMITED_RESPAWNS
					IF MC_serverBD_4.iObjRule[i][iTeam] = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER
						SET_BIT(MC_serverBD.iAnyColObjRespawnAtPriority[iTeam],MC_serverBD_4.iObjPriority[i][iTeam])
					ENDIF
				ENDIF
				
				IF NETWORK_DOES_NETWORK_ID_EXIST(GET_OBJECT_NET_ID(i))
					SET_BIT(MC_serverBD_2.iObjSpawnedOnce, i)
				ENDIF
				
			ENDIF

		ENDFOR
		
		FOR i = 0 TO FMMC_MAX_NUM_INTERACTABLES - 1
			MC_serverBD_4.sIntServerData.iInteractable_InteractingParticipant[i] = -1
		ENDFOR
		
		FOR i = 0 TO (FMMC_MAX_DIALOGUES - 1)
			IF NOT IS_VECTOR_ZERO(MC_GET_DIALOGUE_TRIGGER_POSITION(i))
				IF NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[i].iDialogueBitset, iBS_DialogueNotHeardByTeam0 + iTeam)
				AND g_FMMC_STRUCT.sDialogueTriggers[i].iRule != -1
				AND g_FMMC_STRUCT.sDialogueTriggers[i].iTeam = iTeam
				AND NOT (IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[i].iDialogueBitset, iBS_DialogueSkipOnQuickRestart) AND IS_THIS_A_QUICK_RESTART_JOB())
				//And the flag to skip it on the second part quick restart is not set and it's not a quick restart
				AND NOT (IS_BIT_SET( g_FMMC_STRUCT.sDialogueTriggers[i].iDialogueBitset2, iBS_Dialogue2SkipOnQuickRestart1) AND FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(0))
				AND NOT (IS_BIT_SET( g_FMMC_STRUCT.sDialogueTriggers[i].iDialogueBitset2, iBS_Dialogue2SkipOnQuickRestart2) AND FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(1))
				AND NOT (IS_BIT_SET( g_FMMC_STRUCT.sDialogueTriggers[i].iDialogueBitset2, iBS_Dialogue2SkipOnQuickRestart3) AND FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(2))
				AND NOT (IS_BIT_SET( g_FMMC_STRUCT.sDialogueTriggers[i].iDialogueBitset2, iBS_Dialogue2SkipOnQuickRestart4) AND FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(3))
					IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[i].iDialogueBitset,iBS_DialogueTriggerMinigame)
					AND IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[i].iDialogueBitset, iBS_DialogueBlockProgress)
						PRINTLN("[RCC MISSION] CLIENT_PRE_DEFINE_SERVER_DATA - Setting iObjectiveProgressionHeldUpBitset to hold up progression due to circuit hack dialogue for team ",iTeam,", rule ",g_FMMC_STRUCT.sDialogueTriggers[i].iRule)
						SET_BIT(MC_serverBD_1.iObjectiveProgressionHeldUpBitset[iTeam], g_FMMC_STRUCT.sDialogueTriggers[i].iRule)
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
		
		
		FOR i = 0 TO (FMMC_MAX_RULES-1)
			IF MC_serverBD_4.iPlayerRulePriority[i][iTeam] <= MC_serverBD_4.iCurrentHighestPriority[iTeam] 
			AND MC_serverBD_4.iPlayerRulePriority[i][iTeam] < FMMC_MAX_RULES
				MC_serverBD_4.iPlayerRuleMissionLogic[iTeam]  = MC_serverBD_4.iPlayerRule[i][iTeam] 
			ENDIF
		ENDFOR
		
		
		FOR i = 0 TO (FMMC_MAX_GO_TO_LOCATIONS-1)
			IF MC_serverBD_4.iGotoLocationDataPriority[i][iTeam] <= MC_serverBD_4.iCurrentHighestPriority[iTeam] 
			AND MC_serverBD_4.iGotoLocationDataPriority[i][iTeam] < FMMC_MAX_RULES
				IF MC_serverBD_4.iGotoLocationDataRule[i][iTeam] != FMMC_OBJECTIVE_LOGIC_NONE
					MC_serverBD_4.iLocMissionLogic[iTeam] = MC_serverBD_4.iGotoLocationDataRule[i][iTeam] 
					
					IF MC_serverBD.iPriorityLocation[iTeam] = -1
						MC_serverBD.iPriorityLocation[iTeam] = i
					ENDIF
					
				ENDIF
			ENDIF
		ENDFOR
		MC_serverBD_2.iClobberedScore[iTeam] = -1
	ENDFOR
	
	FOR iTeam = 0 TO FMMC_MAX_TEAMS-1
		MC_serverBD.iEndCutsceneNum[iTeam] = -1
		MC_serverBD.iCutsceneID[iTeam] = -1
		MC_serverBD.iCutsceneStreamingIndex[iTeam] = -1
		FOR i = 0 TO FMMC_MAX_CUTSCENE_PLAYERS-1
			MC_serverBD.piCutscenePlayerIDs[iTeam][i] = INVALID_PLAYER_INDEX()
		ENDFOR
	ENDFOR
	
	IF IS_THIS_A_QUICK_RESTART_JOB()
	OR IS_CORONA_INITIALISING_A_QUICK_RESTART()
		IF g_TransitionSessionNonResetVars.iDoNotRespawnVehBitSet != 0
			MC_serverBD.iCleanedUpUndeliveredVehiclesBS = g_TransitionSessionNonResetVars.iDoNotRespawnVehBitSet 
		ENDIF
	ENDIF
	
	RESTORE_MISSION_CONTINUITY_VARS()
	
	FOR iTeam = 0 TO (FMMC_MAX_TEAMS-1)
		MC_serverBD.iOverriddenScriptedCutsceneRule[iTeam] = -1
		MC_serverBD.iOverriddenScriptedCutsceneIndex[iTeam] = -1
	ENDFOR
	
	INT iComp
	FOR iComp = 0 TO FMMC_MAX_COMPANIONS-1
		MC_serverBD_4.iCompanionPedIndex[iComp] = -1	
	ENDFOR
	
ENDPROC

PROC HANDLE_CORONA_DATA()
	
	PRINTLN("[RCC MISSION] HANDLE_CORONA_DATA being called - bIsLocalPlayerHost = ", bIsLocalPlayerHost)
	
	IF bIsLocalPlayerHost
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciSUDDEN_DEATH_THAT_ENDS_WHEN_MORE_PEOPLE_ARE_IN_LOC)
			PRINTLN("[RCC MISSION] HANDLE_CORONA_DATA SERVER - SUDDEN DEATH ends when more people in loc")
			SET_BIT(MC_serverBD.iServerBitSet2, SBBOOL2_SUDDEN_DEATH_MOREINLOC)
			
			#IF IS_DEBUG_BUILD
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciSUDDEN_DEATH_THAT_ENDS_WHEN_NO_ENEMY_TEAMS)
				PRINTLN("[RCC MISSION] HANDLE_CORONA_DATA SERVER - SUDDEN DEATH, kill all enemies is also set...")
			ENDIF
			#ENDIF
		ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciSUDDEN_DEATH_THAT_ENDS_WHEN_NO_ENEMY_TEAMS)
			PRINTLN("[RCC MISSION] HANDLE_CORONA_DATA SERVER - SUDDEN DEATH ends when all enemies are dead")
			SET_BIT(MC_serverBD.iServerBitSet2, SBBOOL2_SUDDEN_DEATH_KILLALLENEMIES)
		ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciSUDDEN_DEATH_THAT_ENDS_WHEN_ONLY_ONE_TEAM_ALIVE)
			PRINTLN("[RCC MISSION][KH][SSDS] HANDLE_CORONA_DATA - SUDDEN DEATH ends when only one team is left alive")
	 		SET_BIT(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_SUMO_PENNED_IN)
		ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciSUDDEN_DEATH_ONE_HIT_KILL_LAST_TEAM_STANDING)
			PRINTLN("[RCC MISSION] HANDLE_CORONA_DATA - SUDDEN DEATH ends when only one team is left alive (one hit kills)")
	 		SET_BIT(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_BE_MY_VALENTINES)
		ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciSUDDEN_DEATH_TARGET)
			PRINTLN("[RCC MISSION] HANDLE_CORONA_DATA - SUDDEN DEATH ends when all players are out of bullets (and a team has a higher score)")
	 		SET_BIT(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_TARGET)
		ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciSUDDEN_DEATH_IN_AND_OUT)
			PRINTLN("[RCC MISSION] HANDLE_CORONA_DATA - SUDDEN DEATH - IN AND OUT - ENABLED")
			SET_BIT(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_IN_AND_OUT)
		ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciSUDDEN_DEATH_POWER_PLAY)
			PRINTLN("[RCC MISSION] HANDLE_CORONA_DATA - SUDDEN DEATH - POWER PLAY - ENABLED")
			SET_BIT(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_POWER_PLAY)
		ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciPACKAGE_HELD_SUDDEN_DEATH)
	 		PRINTLN("[RCC MISSION] HANDLE_CORONA_DATA - SUDDEN DEATH - PACKAGE HELD SUDDEN DEATH - ENABLED")
			SET_BIT(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_PACKAGE_HELD)
		ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciSUDDEN_DEATH_KILLALLENEMIES_PENNED_IN)
			PRINTLN("[RCC MISSION] HANDLE_CORONA_DATA - SUDDEN DEATH - DAWN RAID/KILL ALL ENEMIES PENNED IN - ENABLED")
	 		SET_BIT(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_KILLALLENEMIES_PENNED_IN)
		ENDIF
	#IF IS_DEBUG_BUILD
	ELSE
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciSUDDEN_DEATH_THAT_ENDS_WHEN_MORE_PEOPLE_ARE_IN_LOC)
			PRINTLN("[RCC MISSION] HANDLE_CORONA_DATA CLIENT - SUDDEN DEATH ends when more people in loc")
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciSUDDEN_DEATH_THAT_ENDS_WHEN_NO_ENEMY_TEAMS)
				PRINTLN("[RCC MISSION] HANDLE_CORONA_DATA CLIENT - SUDDEN DEATH, kill all enemies is also set...")
			ENDIF
		ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciSUDDEN_DEATH_THAT_ENDS_WHEN_NO_ENEMY_TEAMS)
			PRINTLN("[RCC MISSION] HANDLE_CORONA_DATA CLIENT - SUDDEN DEATH ends when all enemies are dead")
		ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciSUDDEN_DEATH_THAT_ENDS_WHEN_ONLY_ONE_TEAM_ALIVE)
			PRINTLN("[RCC MISSION] HANDLE_CORONA_DATA CLIENT - SUDDEN DEATH ends in penned in")
		ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciSUDDEN_DEATH_ONE_HIT_KILL_LAST_TEAM_STANDING)
			PRINTLN("[RCC MISSION] HANDLE_CORONA_DATA CLIENT - SUDDEN DEATH ends when only one team is left alive (one hit kills)")	
		ENDIF
	#ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciSELECTABLE_TARGET_SCORES)
		
		INT iNewTargetScore = g_FMMC_STRUCT.sFMMCEndConditions[0].iMissionScoreLimit
		PRINTLN("[RCC MISSION] HANDLE_CORONA_DATA CLIENT - ciSELECTABLE_TARGET_SCORES is set, new target score = ",iNewTargetScore)
		
		INT iTeam
		
		FOR iTeam = 0 TO (FMMC_MAX_TEAMS - 1)
			IF bIsLocalPlayerHost
				IF MC_serverBD_2.iClobberedScore[iTeam] = -1
					MC_serverBD_2.iClobberedScore[iTeam] = 0 //make sure this is cleared
				ENDIF
			ENDIF
			
			IF MC_serverBD_2.iClobberedScore[iTeam] > 0
				PRINTLN("[RCC MISSION] HANDLE_CORONA_DATA CLIENT - clobber g_FMMC_STRUCT.sFMMCEndConditions[",iTeam,"].iMissionTargetScore of ",g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMissionTargetScore," with new target score (using clobbered score) ",MC_serverBD_2.iClobberedScore[iTeam])
				g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMissionTargetScore = MC_serverBD_2.iClobberedScore[iTeam] //this should keep jip spectate OK
			ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMissionTargetScore != 0
				PRINTLN("[RCC MISSION] HANDLE_CORONA_DATA CLIENT - clobber g_FMMC_STRUCT.sFMMCEndConditions[",iTeam,"].iMissionTargetScore of ",g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMissionTargetScore," with new target score ",iNewTargetScore)
				g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMissionTargetScore = iNewTargetScore
				IF bIsLocalPlayerHost
					MC_serverBD_2.iClobberedScore[iTeam] = iNewTargetScore
				ENDIF
			#IF IS_DEBUG_BUILD
			ELSE
				PRINTLN("[RCC MISSION] HANDLE_CORONA_DATA CLIENT - g_FMMC_STRUCT.sFMMCEndConditions[",iTeam,"].iMissionTargetScore is 0, don't bother updating it")
			#ENDIF
			ENDIF
		ENDFOR
	ENDIF
ENDPROC

PROC CLIENT_PRE_DEFINE_SERVER_DATA()
	
	CLIENT_PRE_DEFINE_SERVER_DATA_UNSORTED() //FMMC2020 - Remove once tidied
	
	INITIALISE_INT_ARRAY(MC_serverBD.iDriverPart, -1)
	
	INITIALISE_PLAYER_ARRAY(MC_serverBD_1.piTimeBarWinners)
	
	INITIALISE_INT_ARRAY(MC_serverBD.iCameraNumber, -1)
	
	INITIALISE_INT_ARRAY(MC_serverBD_4.iPreviousHighestPriority, FMMC_PRIORITY_IGNORE)
	
	INITIALISE_INT_ARRAY(MC_serverBD_3.iCurrentTrainLocation, -1)
	
	INITIALISE_INT_ARRAY(MC_serverBD_3.iTeamLivesOverride, -1)
	
	INITIALISE_INT_ARRAY(MC_serverBD.sFlashFade.iFlashFadePartEntIndex, -1)
	
	INITIALISE_INT_ARRAY(MC_serverBD.iObjCapturer, -1)

	INITIALISE_INT_ARRAY(MC_serverBD.iDrone_EntityIndex, -1)
	
	INITIALISE_INT_ARRAY(MC_serverBD.iDrone_EntityType, ciENTITY_TYPE_NONE)
	
	INITIALISE_INT_ARRAY(MC_serverBD.iDrone_AssociatedGotoPoolIndex, ASSOCIATED_GOTO_TASK_POOL_INDEX__USE_PED_DATA)
	
	INITIALISE_INT_ARRAY(MC_serverBD.iDrone_AssociatedGotoProgress, 0)
	
	INITIALISE_PLAYER_ARRAY(MC_serverBD_4.sPedCloningData.playerSource)
	
	INITIALISE_INT_ARRAY(MC_ServerBD.sWarpOnDamageTracker.iWarpFromDamageTracker_EntityType, CREATION_TYPE_NONE)
	
	INITIALISE_INT_ARRAY(MC_ServerBD.sWarpOnDamageTracker.iWarpFromDamageTracker_EntityIndex, -1)
	
	INITIALISE_INT_ARRAY(MC_ServerBD.sWarpOnDamageTracker.iWarpFromDamageTracker_WarpIndex, -1)	
	
ENDPROC


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Intro Cutscene / Sequence
// ##### Description: Handles the main functions involved in starting a cutscene or camera sequence intro shard.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL SERVER_CHECK_EVERYONE_REACHED_INTRO_CUTSCENE()

	INT iparticipant
	PARTICIPANT_INDEX tempPart
	PLAYER_INDEX playerId

	PRINTLN("[PLAYER_LOOP] - SERVER_CHECK_EVERYONE_REACHED_INTRO_CUTSCENE")
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iparticipant
		tempPart = INT_TO_PARTICIPANTINDEX(iparticipant)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
			playerId = NETWORK_GET_PLAYER_INDEX(tempPart)
			IF NOT IS_PLAYER_SPECTATOR_ONLY(playerId)
				IF IS_A_STRAND_MISSION_BEING_INITIALISED()
					IF NOT IS_BIT_SET(MC_playerBD[iparticipant].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_INI)
						PRINTLN("[RCC MISSION] SERVER_CHECK_EVERYONE_REACHED_INTRO_CUTSCENE - Returning false because Participant ", iparticipant ,"  has not reached Game State ini and is in game state ", GET_MC_CLIENT_GAME_STATE( iparticipant ) )
						RETURN FALSE
					ENDIF
				ELIF GET_MC_CLIENT_GAME_STATE(iparticipant) < GAME_STATE_INTRO_CUTSCENE
					PRINTLN("[RCC MISSION] SERVER_CHECK_EVERYONE_REACHED_INTRO_CUTSCENE - Returning FALSE because Participant ", iparticipant ,"  has not reached intro cutscee and is in game state ", GET_MC_CLIENT_GAME_STATE(iparticipant))
					RETURN FALSE
				ELSE
					PRINTLN("[RCC MISSION] SERVER_CHECK_EVERYONE_REACHED_INTRO_CUTSCENE returning true for Participant ", iparticipant ," is in game state ", GET_MC_CLIENT_GAME_STATE(iparticipant))
				ENDIF				
			ENDIF
		ENDIF
	
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

PROC START_CAMERA_BLEND_AUDIO_SCENES_AND_SOUNDS()
	
	INT i
	STRING stSoundSet
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciHWN_BREATHING_ENABLED)
		PRINTLN("[JJT][PROCESS_CLIENT][GAME_STATE_CAMERA_BLEND] Halloween breathing enabled.")						
			
		iHunterBreathingSoundID = GET_SOUND_ID()
		tlBreathingTrack = "Breathing_0"
		
		IF GET_RANDOM_INT_IN_RANGE(0, 1000) > 500
			tlBreathingTrack += 1 //Breathing_01
		ELSE
			tlBreathingTrack += 2 //Breathing_02
		ENDIF
		
		STRING sSoundSet 
		sSoundSet = "DLC_HALLOWEEN_FVJ_Sounds"
		
		IF MC_PlayerBD[iLocalPart].iteam != 0
			PLAY_SOUND_FROM_ENTITY(iHunterBreathingSoundID, tlBreathingTrack, GET_SCARY_PED_ON_TEAM_ZERO(), sSoundSet)
			PRINTLN("[JJT][PROCESS_CLIENT][GAME_STATE_CAMERA_BLEND] PLAY_SOUND_FROM_ENTITY(", tlBreathingTrack, ", ",sSoundSet,", on Scary Ped")								
		ELSE
			PLAY_SOUND_FROM_ENTITY(iHunterBreathingSoundID, tlBreathingTrack, LocalPlayerPed, sSoundSet)
			PRINTLN("[JJT][PROCESS_CLIENT][GAME_STATE_CAMERA_BLEND] PLAY_SOUND_FROM_ENTITY(", tlBreathingTrack, ", ",sSoundSet,", on self")
		ENDIF
	ENDIF
		
	IF NOT (bIsSCTV OR DID_I_JOIN_MISSION_AS_SPECTATOR())
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciENABLE_JUGGERNAUT_SOUNDS)
			PRINTLN("[JS][PROCESS_CLIENT][GAME_STATE_CAMERA_BLEND] JUGGERNAUT ROUND START SOUNDS")
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iTeamBitset2, ciBS2_JUGGERNAUTS)
				PRINTLN("[LM][PROCESS_CLIENT][GAME_STATE_CAMERA_BLEND] JUGGERNAUT SOUNDS - Starting scene DLC_GR_PM_JN_Player_Is_Attacker_Scene")
				START_AUDIO_SCENE("DLC_GR_PM_JN_Player_Is_JN_Scene")
			ELSE
				PRINTLN("[JS][PROCESS_CLIENT][GAME_STATE_CAMERA_BLEND] JUGGERNAUT SOUNDS - Starting scene DLC_IE_JN_Player_Is_Attacker_Scene")
				START_AUDIO_SCENE("DLC_IE_JN_Player_Is_Attacker_Scene")
			ENDIF
			FOR i = 0 TO (MAX_NUM_MC_PLAYERS - 1)
				IF ((NOT IS_BIT_SET(MC_playerBD[i].iClientBitSet, PBBOOL_ANY_SPECTATOR)) OR IS_BIT_SET(MC_playerBD[i].iClientBitSet, PBBOOL_HEIST_SPECTATOR))
					PRINTLN("[PLAYER_LOOP] - START_CAMERA_BLEND_AUDIO_SCENES_AND_SOUNDS")
					IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
						IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[i].iTeam].iTeamBitset2, ciBS2_JUGGERNAUTS)
							IF i = iLocalPart
								stSoundSet = "DLC_GR_PM_Juggernaut_Player_Sounds"
							ELIF DOES_TEAM_LIKE_TEAM(MC_playerBD[i].iTeam, MC_playerBD[iLocalPart].iTeam)
								stSoundSet = "DLC_IE_JN_Team_Sounds"
							ELSE
								stSoundSet = "DLC_IE_JN_Enemy_Sounds"
							ENDIF
							PLAY_SOUND_FROM_COORD(-1, "Round_Start_JN", GET_ENTITY_COORDS(GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i)))), stSoundSet)	
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL OK_TO_PLAY_RESTART_ANIM_ON_STRAND()
	
	PRINTLN("[RCC MISSION] OK_TO_PLAY_RESTART_ANIM_ON_STRAND, TRUE ")
	
	RETURN TRUE
	
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Mission Intro Cutscenes
// ##### Description: Pre-scripted intro cutscenes both run internally and externally outside of a rule or traditional cutscene sequence.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL WAITING_TO_SYNC_PLAYERS_FOR_MISSION_INTRO_SCENE()

	IF NOT IS_BIT_SET(MC_PlayerBD[iLocalPart].iClientBitSet4, PBBOOL4_CLIENT_READY_FOR_SPECIAL_MISSION_INTRO)
		SET_BIT(MC_PlayerBD[iLocalPart].iClientBitSet4, PBBOOL4_CLIENT_READY_FOR_SPECIAL_MISSION_INTRO)
		PRINTLN("[MCIntroSequence] - PROCESS_INTRO_CUTSCENE - Setting PBBOOL4_CLIENT_READY_FOR_SPECIAL_MISSION_INTRO")
	ENDIF
		
	IF NOT bIsLocalPlayerHost			
		IF MC_ServerBD.iIntroTimeStamp + 1000 <= NATIVE_TO_INT(GET_NETWORK_TIME_ACCURATE())
		AND MC_ServerBD.iIntroTimeStamp != 0
			RETURN FALSE
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF MC_ServerBD.iIntroTimeStamp + 1000 <= NATIVE_TO_INT(GET_NETWORK_TIME_ACCURATE())
	AND MC_ServerBD.iIntroTimeStamp != 0
		RETURN FALSE
	ENDIF
	
	INT iPlayersToCheck = MC_serverBD.iNumStartingPlayers[MC_PlayerBD[iPartToUse].iTeam]
	INT iPlayersReached
	
	PRINTLN("[MCIntroSequence] - PROCESS_INTRO_CUTSCENE - We need to check that all ", iPlayersToCheck, " of the starting players are ready.")
	
	INT iPart = 0
	FOR iPart = 0 TO (MAX_NUM_MC_PLAYERS - 1)
		PARTICIPANT_INDEX tempPart = INT_TO_PARTICIPANTINDEX(iPart)		
		IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
			PLAYER_INDEX playerIndex = NETWORK_GET_PLAYER_INDEX(tempPart)
			
			IF NOT DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(playerIndex)
			AND NOT IS_PLAYER_SCTV(playerIndex)
			AND IS_BIT_SET(MC_PlayerBD[iPart].iClientBitSet4, PBBOOL4_CLIENT_READY_FOR_SPECIAL_MISSION_INTRO)
				iPlayersReached++
				PRINTLN("[MCIntroSequence] - PROCESS_INTRO_CUTSCENE - iPart: ", iPart, " has reached. iPlayersReached = ", iPlayersReached)
			ENDIF
		ENDIF
	ENDFOR
		
	IF iPlayersReached >= iPlayersToCheck
	AND MC_ServerBD.iIntroTimeStamp = 0
		MC_ServerBD.iIntroTimeStamp = NATIVE_TO_INT(GET_NETWORK_TIME_ACCURATE())
		PRINTLN("[MCIntroSequence] - PROCESS_INTRO_CUTSCENE - Settinga 'future' iIntroTimeStamp to ", MC_ServerBD.iIntroTimeStamp)
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_INTRO_CUTSCENE_MOVE_TO_SOLO_PLAY()
	IF IS_THIS_A_CONTACT_MISSION()
	OR IS_BIT_SET( g_FMMC_STRUCT.iOptionsMenuBitSet, ciSPAWN_INTO_VEH )
	OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciDISABLE_INTRO_CUTSCENE)
	OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iPartToUse].iTeam].iRuleBitsetTen[0], ciBS_RULE10_ENTER_CLOSEST_PLACED_VEH_RULE_START)
	OR g_FMMC_STRUCT.eMissionIntroType != MISSION_INTRO_TYPE_NONE
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL HAS_FINISHED_MISSION_INTRO_SCENE_FOR_ARCADE_PROPERTY()
	IF NOT g_bFinishedExitingOfSMPLinteriorOnHeist
		PRINTLN("[MCIntroSequence][RCC MISSION] PROCESS_INTRO_CUTSCENE - Casino Heist exiting for walking out of the Arcade") 
		RETURN FALSE
	ELSE
		PRINTLN("[MCIntroSequence][RCC MISSION] PROCESS_INTRO_CUTSCENE - NOT g_bFinishedExitingOfSMPLinteriorOnHeist: ", BOOL_TO_STRING(NOT g_bFinishedExitingOfSMPLinteriorOnHeist))					
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL HAS_FINISHED_MISSION_INTRO_SCENE_FOR_GALAXY_YACHT()
	IF NOT IS_BIT_SET(iLocalBoolCheck33, LBOOL33_STARTED_MISSION_INTRO_SCENE)
		PRINTLN("[MCIntroSequence][RCC MISSION] PROCESS_INTRO_CUTSCENE - Starting Yacht Intro.")
		SET_BIT(iLocalBoolCheck33, LBOOL33_STARTED_MISSION_INTRO_SCENE)
		g_bRunGroupYachtExit = TRUE
	ENDIF
	
	IF g_bRunGroupYachtExit
	OR g_bRunningGroupYachtExit
		PRINTLN("[MCIntroSequence][RCC MISSION] PROCESS_INTRO_CUTSCENE - Waiting for Yacht intro to finish - g_bRunGroupYachtExit: ", g_bRunGroupYachtExit, " g_bRunningGroupYachtExit: ", g_bRunningGroupYachtExit)
		RETURN FALSE
	ELSE
		PRINTLN("[MCIntroSequence][RCC MISSION] PROCESS_INTRO_CUTSCENE - Yacht intro finished.")
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC SET_MISSION_INTRO_STAGE(MISSION_INTRO_STAGE eMissionIntroStageNew)	
	PRINTLN("[MCIntroSequence] PROCESS_INTRO_CUTSCENE - IS_MISSION_READY_TO_PROGRESS_AFTER_INTRO_SCENE - eMissionIntroStage: ", ENUM_TO_INT(eMissionIntroStage), " eMissionIntroStageNew: ", ENUM_TO_INT(eMissionIntroStageNew))
	eMissionIntroStage = eMissionIntroStageNew
ENDPROC

FUNC BOOL HAS_FINISHED_MISSION_INTRO_SCENE_FOR_TUNER()
	VEHICLE_INDEX vehPlayer
	IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		vehPlayer = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)		
	ENDIF
	
	PED_INDEX pedGangBoss	
	IF GB_GET_LOCAL_PLAYER_GANG_BOSS() != INVALID_PLAYER_INDEX()
		PLAYER_INDEX piGangBoss = GB_GET_LOCAL_PLAYER_GANG_BOSS()
		
		IF IS_NET_PLAYER_OK(piGangBoss)
			pedGangBoss = GET_PLAYER_PED(piGangBoss)
		ELSE
			PRINTLN("[MCIntroSequence] PROCESS_INTRO_CUTSCENE - IS_MISSION_READY_TO_PROGRESS_AFTER_INTRO_SCENE - HAS_FINISHED_MISSION_INTRO_SCENE_FOR_TUNER - Gang Boss invalid (1) - Falling back to localplayerped")
			pedGangBoss = LocalPlayerPed
		ENDIF
	ELSE
		PRINTLN("[MCIntroSequence] PROCESS_INTRO_CUTSCENE - IS_MISSION_READY_TO_PROGRESS_AFTER_INTRO_SCENE - HAS_FINISHED_MISSION_INTRO_SCENE_FOR_TUNER - Gang Boss invalid (2) - Falling back to localplayerped")
		pedGangBoss = LocalPlayerPed
	ENDIF
			
	SWITCH eMissionIntroStage
		CASE MISSION_INTRO_STAGE_INIT			
			
			TOGGLE_PAUSED_RENDERPHASES(FALSE)
			
			IF IS_VEHICLE_DRIVEABLE(vehPlayer)
				
				SET_BIT(iLocalBoolCheck32, LBOOL32_FORCED_MISSION_START_MOVING_FORWARD)
				
				IF NETWORK_HAS_CONTROL_OF_ENTITY(vehPlayer)
					PRINTLN("[MCIntroSequence] PROCESS_INTRO_CUTSCENE - IS_MISSION_READY_TO_PROGRESS_AFTER_INTRO_SCENE - HAS_FINISHED_MISSION_INTRO_SCENE_FOR_TUNER - unfrozen player vehicle and moving forward.")
					FREEZE_ENTITY_POSITION(vehPlayer, FALSE)
					SET_VEHICLE_ENGINE_ON(vehPlayer, TRUE, TRUE, FALSE)
					SET_VEHICLE_LIMIT_SPEED_WHEN_PLAYER_INACTIVE(vehPlayer, FALSE)
					SET_VEHICLE_STOP_INSTANTLY_WHEN_PLAYER_INACTIVE(vehPlayer, FALSE)
					IF NOT IS_SCREEN_FADED_IN()
						DO_SCREEN_FADE_IN(500)
					ENDIF
					
					vStartDriveToPosition = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(vehPlayer), GET_ENTITY_HEADING(vehPlayer), <<0.0, 50.0, 0.0>>)
					DRIVINGMODE dmFlags
					TASK_GO_TO_COORD_ANY_MEANS_FLAGS tgcamFlags
					tgcamFlags = TGCAM_IGNORE_VEHICLE_HEALTH | TGCAM_CONSIDER_ALL_NEARBY_VEHICLES | TGCAM_PROPER_IS_DRIVEABLE_CHECK
					dmFlags = dmFlags | DF_DriveIntoOncomingTraffic
					tgcamFlags = tgcamFlags | TGCAM_REMAIN_IN_VEHICLE_AT_DESTINATION
					tgcamFlags = tgcamFlags | TGCAM_NEVER_ABANDON_VEHICLE_IF_MOVING
					tgcamFlags = tgcamFlags | TGCAM_NEVER_ABANDON_VEHICLE
					dmFlags = dmFlags | DRIVINGMODE_PLOUGHTHROUGH
					dmFlags = dmFlags | DF_ForceStraightLine
					
					TASK_GO_TO_COORD_ANY_MEANS_EXTRA_PARAMS_WITH_CRUISE_SPEED(LocalPlayerPed, vStartDriveToPosition, PEDMOVEBLENDRATIO_RUN, vehPlayer, TRUE, dmFlags, DEFAULT, 0, 50.0, tgcamFlags, 7.0, 2.0)
										
					START_NET_TIMER(tdMissionIntroSyncSpeedDelay)					
				ELSE
					NETWORK_REQUEST_CONTROL_OF_ENTITY(vehPlayer)
					RETURN FALSE
				ENDIF
				
				SET_MISSION_INTRO_STAGE(MISSION_INTRO_STAGE_ONE)
			ELSE
				PRINTLN("[MCIntroSequence] PROCESS_INTRO_CUTSCENE - IS_MISSION_READY_TO_PROGRESS_AFTER_INTRO_SCENE - HAS_FINISHED_MISSION_INTRO_SCENE_FOR_TUNER - DIDN'T START IN A VEHICLE SOMETHING WENT WRONG!!")
				TOGGLE_PAUSED_RENDERPHASES(TRUE)
				SET_MISSION_INTRO_STAGE(MISSION_INTRO_STAGE_FINISHED)
			ENDIF
		BREAK
		
		CASE MISSION_INTRO_STAGE_ONE
			
			IF NOT HAS_NET_TIMER_EXPIRED_READ_ONLY(tdMissionIntroSyncSpeedDelay, ciMISSION_INTRO_SYNC_SPEED_DELAY)
				PRINTLN("[MCIntroSequence] PROCESS_INTRO_CUTSCENE - IS_MISSION_READY_TO_PROGRESS_AFTER_INTRO_SCENE - HAS_FINISHED_MISSION_INTRO_SCENE_FOR_TUNER - Waiting for speed sync..")
				RETURN FALSE
			ENDIF
			
			IF NOT IS_BIT_SET(iLocalBoolCheck29, LBOOL29_MISSION_INTRO_SCENE_READY_FOR_JOB_TEXT)
				SET_BIT(iLocalBoolCheck29, LBOOL29_MISSION_INTRO_SCENE_READY_FOR_JOB_TEXT)
			ENDIF
			
			IF NOT IS_BIT_SET(iLocalBoolCheck32, LBOOL32_TUNER_INTRO_SCENE_REV_1)
			AND HAS_NET_TIMER_EXPIRED_READ_ONLY(tdMissionIntroSyncSpeedDelay, ciMISSION_INTRO_SYNC_SPEED_REV_1_CUE)
				PLAY_SOUND_FROM_ENTITY(-1, "PT_FAKE_CAR_ONE_REV_SLOW", vehPlayer)
				SET_BIT(iLocalBoolCheck32, LBOOL32_TUNER_INTRO_SCENE_REV_1)
			ENDIF
			
			IF NOT IS_BIT_SET(iLocalBoolCheck32, LBOOL32_TUNER_INTRO_SCENE_REV_2)
			AND HAS_NET_TIMER_EXPIRED_READ_ONLY(tdMissionIntroSyncSpeedDelay, ciMISSION_INTRO_SYNC_SPEED_REV_2_CUE)
				PLAY_SOUND_FROM_ENTITY(-1, "PT_FAKE_CAR_ONE_REV_SLOW", vehPlayer)
				SET_BIT(iLocalBoolCheck32, LBOOL32_TUNER_INTRO_SCENE_REV_2)
			ENDIF
			
			IF NOT PROCESS_TUNER_INTRO_CUTSCENE(jobIntroData, pedGangBoss)
				PRINTLN("[MCIntroSequence] PROCESS_INTRO_CUTSCENE - IS_MISSION_READY_TO_PROGRESS_AFTER_INTRO_SCENE - HAS_FINISHED_MISSION_INTRO_SCENE_FOR_TUNER - Running Tuner style Intro..")
				RETURN FALSE
			ENDIF

			SET_MISSION_INTRO_STAGE(MISSION_INTRO_STAGE_TWO)
		BREAK
		
		CASE MISSION_INTRO_STAGE_TWO
					
			IF NOT HAS_NET_TIMER_STARTED(tdMissionIntroInterpTimer)
				IF GET_RANDOM_INT_IN_RANGE(0, 100) >= 50
					PLAY_SOUND_FROM_ENTITY(-1, "PT_FAKE_CAR_TWO_REVS_FAST", vehPlayer)
				ELSE
					PLAY_SOUND_FROM_ENTITY(-1, "PT_FAKE_CAR_THREE_QUICK_REVS", vehPlayer)
				ENDIF
				START_NET_TIMER(tdMissionIntroInterpTimer)
			ENDIF
			
			IF NOT PROCESS_TUNER_OUTRO_CUTSCENE(jobIntroData, 	ciMISSION_INTRO_TUNER_INTERP_TIME, ciMISSION_INTRO_TUNER_OUTRO_DELAY)
				PRINTLN("[MCIntroSequence] PROCESS_INTRO_CUTSCENE - IS_MISSION_READY_TO_PROGRESS_AFTER_INTRO_SCENE - HAS_FINISHED_MISSION_INTRO_SCENE_FOR_TUNER - Running Race style Outro..")
				RETURN FALSE
			ENDIF
			
			SET_VEHICLE_FORWARD_SPEED(vehPlayer, 5.0)
			
			CLEAR_PED_TASKS(LocalPlayerPed)
			
			SET_MISSION_INTRO_STAGE(MISSION_INTRO_STAGE_FINISHED)
		BREAK
		
		CASE MISSION_INTRO_STAGE_FINISHED
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_FINISHED_MISSION_INTRO_SCENE_RACE_GENERIC(BOOL bMoveForwardDuringIntro)
	VEHICLE_INDEX vehPlayer
	IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		vehPlayer = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)		
	ENDIF
	
	SWITCH eMissionIntroStage
		CASE MISSION_INTRO_STAGE_INIT
			IF IS_VEHICLE_DRIVEABLE(vehPlayer)
				
				SET_BIT(iLocalBoolCheck32, LBOOL32_FORCED_MISSION_START_MOVING_FORWARD)
				
				IF NETWORK_HAS_CONTROL_OF_ENTITY(vehPlayer)
					PRINTLN("[MCIntroSequence] PROCESS_INTRO_CUTSCENE - IS_MISSION_READY_TO_PROGRESS_AFTER_INTRO_SCENE - unfrozen player vehicle and moving forward.")
					FREEZE_ENTITY_POSITION(vehPlayer, FALSE)
					SET_VEHICLE_ENGINE_ON(vehPlayer, TRUE, TRUE, FALSE)
					IF bMoveForwardDuringIntro
						SET_VEHICLE_FORWARD_SPEED(vehPlayer, 5.0)
					ENDIF
					SET_VEHICLE_LIMIT_SPEED_WHEN_PLAYER_INACTIVE(vehPlayer, FALSE)
					SET_VEHICLE_STOP_INSTANTLY_WHEN_PLAYER_INACTIVE(vehPlayer, FALSE)
					IF NOT IS_SCREEN_FADED_IN()
						DO_SCREEN_FADE_IN(500)
					ENDIF
				ELSE
					NETWORK_REQUEST_CONTROL_OF_ENTITY(vehPlayer)
					RETURN FALSE
				ENDIF
				
				SET_MISSION_INTRO_STAGE(MISSION_INTRO_STAGE_ONE)
			ELSE
				PRINTLN("[MCIntroSequence] PROCESS_INTRO_CUTSCENE - IS_MISSION_READY_TO_PROGRESS_AFTER_INTRO_SCENE - DIDN'T START IN A VEHICLE SOMETHING WENT WRONG!!")
				TOGGLE_PAUSED_RENDERPHASES(TRUE)
				SET_MISSION_INTRO_STAGE(MISSION_INTRO_STAGE_FINISHED)
			ENDIF
		BREAK
		
		CASE MISSION_INTRO_STAGE_ONE
			
			TOGGLE_PAUSED_RENDERPHASES(TRUE)
			
			IF bMoveForwardDuringIntro
			AND IS_VEHICLE_DRIVEABLE(vehPlayer)
				SET_VEHICLE_FORWARD_SPEED(vehPlayer, 5.0)
			ENDIF
			
			SET_BIT(iLocalBoolCheck29, LBOOL29_MISSION_INTRO_SCENE_READY_FOR_JOB_TEXT)
			
			IF NOT PROCESS_RACE_INTRO_CUTSCENE(jobIntroData, FALSE, FALSE, TRUE)
				PRINTLN("[MCIntroSequence] PROCESS_INTRO_CUTSCENE - IS_MISSION_READY_TO_PROGRESS_AFTER_INTRO_SCENE - Running Race style Intro..")
				RETURN FALSE
			ENDIF
			
			SET_CAM_PARAMS(jobIntroData.jobIntroCam, GET_CAM_COORD(jobIntroData.jobIntroCam), GET_CAM_ROT(jobIntroData.jobIntroCam), 34.9554, 0, GRAPH_TYPE_VERY_SLOW_IN_VERY_SLOW_OUT)
			
			SET_MISSION_INTRO_STAGE(MISSION_INTRO_STAGE_TWO)
		BREAK
		
		CASE MISSION_INTRO_STAGE_TWO
			IF bMoveForwardDuringIntro
			AND IS_VEHICLE_DRIVEABLE(vehPlayer)
				SET_VEHICLE_FORWARD_SPEED(vehPlayer, 10.0)
			ENDIF
			
			jobIntroData.iTimeDuration = ciMISSION_INTRO_OUTRO_DELAY
			
			IF NOT HAS_NET_TIMER_STARTED(tdMissionIntroInterpTimer)
				START_NET_TIMER(tdMissionIntroInterpTimer)
			ENDIF
				
			IF NOT PROCESS_RACE_OUTRO_CUTSCENE(jobIntroData, ciMISSION_INTRO_INTERP_TIME)			
				PRINTLN("[MCIntroSequence] PROCESS_INTRO_CUTSCENE - IS_MISSION_READY_TO_PROGRESS_AFTER_INTRO_SCENE - Running Race style Outro..")
				RETURN FALSE
			ENDIF
						
			IF NOT HAS_NET_TIMER_EXPIRED_READ_ONLY(tdMissionIntroInterpTimer, ciMISSION_INTRO_OUTRO_DELAY+ciMISSION_INTRO_INTERP_TIME-ciMISSION_INTRO_OUTRO_DIFFERENCE_FOR_PLAYER_ACCEL)
				PRINTLN("[MCIntroSequence] PROCESS_INTRO_CUTSCENE - IS_MISSION_READY_TO_PROGRESS_AFTER_INTRO_SCENE - Gameplay Cam finishing interp..")
				RETURN FALSE
			ENDIF
			
			SET_MISSION_INTRO_STAGE(MISSION_INTRO_STAGE_FINISHED)
		BREAK
		
		CASE MISSION_INTRO_STAGE_FINISHED
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_MISSION_INTRO_PLAY_ON_QUICK_RESTARTS()
	SWITCH g_FMMC_STRUCT.eMissionIntroType	
		CASE MISSION_INTRO_TYPE_YACHT_SCENE
			IF IS_THIS_A_QUICK_RESTART_JOB()
				RETURN FALSE
			ENDIF
		BREAK	
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

// Cutscenes where we want the renderphase to co ntotinue paused until we are ready unpause it within cutscene.
FUNC BOOL SHOULD_MISSION_INTRO_TYPE_HANDLE_RENDERPHASES()
	SWITCH g_FMMC_STRUCT.eMissionIntroType	
		CASE MISSION_INTRO_TYPE_TUNER
		CASE MISSION_INTRO_TYPE_RACE_GENERIC
		CASE MISSION_INTRO_TYPE_RACE_GENERIC_MOVING
			RETURN TRUE
		BREAK	
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

// Internal, from Missions. #####
FUNC BOOL IS_MISSION_INTRO_TYPE_PLAYED_FROM_WITHIN_MISSION()
	SWITCH g_FMMC_STRUCT.eMissionIntroType	
		CASE MISSION_INTRO_TYPE_TUNER
		CASE MISSION_INTRO_TYPE_RACE_GENERIC
		CASE MISSION_INTRO_TYPE_RACE_GENERIC_MOVING
			RETURN TRUE
		BREAK	
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_MISSION_READY_TO_PROGRESS_AFTER_INTRO_SCENE(BOOL bQuickRestart)
	
	IF g_FMMC_STRUCT.eMissionIntroType = MISSION_INTRO_TYPE_NONE
	OR bIsSCTV
	OR DID_I_JOIN_MISSION_AS_SPECTATOR()	
		RETURN TRUE
	ENDIF
		
	IF WAITING_TO_SYNC_PLAYERS_FOR_MISSION_INTRO_SCENE()
	AND GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdSafetyTimer) < 15000
		PRINTLN("[MCIntroSequence] - PROCESS_INTRO_CUTSCENE - IS_FREEMODE_READY_TO_PROGRESS_AFTER_INTRO_SCENE - Waiting for players.")
		RETURN FALSE
	ENDIF
	
	IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdSafetyTimer) < 30000
		
		// It's okay to have an option to bypass strand checks if requested.
		IF NOT HAS_FIRST_STRAND_MISSION_BEEN_PASSED()
		OR NOT IS_THIS_IS_A_STRAND_MISSION()		
			
			IF bQuickRestart
			AND IS_PLAYER_SWITCH_IN_PROGRESS()	
				IF NOT IS_SKYCAM_HOVERING_ON_LAST_CUT_DESCENT()				
					PRINTLN("[MCIntroSequence] PROCESS_INTRO_CUTSCENE - IS_MISSION_READY_TO_PROGRESS_AFTER_INTRO_SCENE - Player Switch In Progress.")
					IF HAS_ROUNDS_AND_QUICK_RESTART_FINISHED()
						PRINTLN("[MCIntroSequence] PROCESS_INTRO_CUTSCENE - IS_MISSION_READY_TO_PROGRESS_AFTER_INTRO_SCENE - Skycam is not moving and we haven't finished quick restart..")					
					
						IF NOT SET_SKYSWOOP_DOWN(DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)	
							PRINTLN("[MCIntroSequence] PROCESS_INTRO_CUTSCENE - IS_MISSION_READY_TO_PROGRESS_AFTER_INTRO_SCENE - Skycam is not down, Calling SET_SKYSWOOP_DOWN")						
						ENDIF
					ENDIF
					
					RETURN FALSE
				ELSE
					IF NOT IS_BIT_SET(iLocalBoolCheck33, LBOOL33_QUICK_RESTART_MISSION_INTRO_SCENE_PAUSE)
						TOGGLE_PAUSED_RENDERPHASES(FALSE)
						SET_BIT(iLocalBoolCheck33, LBOOL33_QUICK_RESTART_MISSION_INTRO_SCENE_PAUSE)
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT IS_BIT_SET(iLocalBoolCheck33, LBOOL33_STARTED_MISSION_INTRO_SCENE)
				PRINTLN("[MCIntroSequence] PROCESS_INTRO_CUTSCENE - IS_MISSION_READY_TO_PROGRESS_AFTER_INTRO_SCENE - Starting Scene.")
				SET_BIT(iLocalBoolCheck33, LBOOL33_STARTED_MISSION_INTRO_SCENE)
			ENDIF
			
			SWITCH g_FMMC_STRUCT.eMissionIntroType
				CASE MISSION_INTRO_TYPE_TUNER
					IF NOT HAS_FINISHED_MISSION_INTRO_SCENE_FOR_TUNER()
						RETURN FALSE
					ENDIF
				BREAK
				CASE MISSION_INTRO_TYPE_RACE_GENERIC
				CASE MISSION_INTRO_TYPE_RACE_GENERIC_MOVING
					IF NOT HAS_FINISHED_MISSION_INTRO_SCENE_RACE_GENERIC(g_FMMC_STRUCT.eMissionIntroType = MISSION_INTRO_TYPE_RACE_GENERIC_MOVING)
						RETURN FALSE
					ENDIF
				BREAK
			ENDSWITCH
			
			PRINTLN("[MCIntroSequence] PROCESS_INTRO_CUTSCENE - IS_MISSION_READY_TO_PROGRESS_AFTER_INTRO_SCENE - Scene Finished.")
		ENDIF
	ENDIF
	
	IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdSafetyTimer) >= 30000
		PRINTLN("[MCIntroSequence] PROCESS_INTRO_CUTSCENE - IS_MISSION_READY_TO_PROGRESS_AFTER_INTRO_SCENE - tdSafetyTimer >= 30000 - Took too long, skipping external intro scenes.")
	ENDIF
	
	RETURN TRUE
ENDFUNC

// External, from Freemode. #####
FUNC BOOL IS_MISSION_INTRO_TYPE_PLAYED_FROM_FREEMODE()
	SWITCH g_FMMC_STRUCT.eMissionIntroType	
		CASE MISSION_INTRO_TYPE_ARCADE_PROPERTY
		CASE MISSION_INTRO_TYPE_YACHT_SCENE
			RETURN TRUE
		BREAK	
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_FREEMODE_READY_TO_PROGRESS_AFTER_INTRO_SCENE()
	
	IF g_FMMC_STRUCT.eMissionIntroType = MISSION_INTRO_TYPE_NONE
	OR bIsSCTV
	OR DID_I_JOIN_MISSION_AS_SPECTATOR()
	OR NOT SHOULD_MISSION_INTRO_PLAY_ON_QUICK_RESTARTS()
		RETURN TRUE
	ENDIF
	
	IF WAITING_TO_SYNC_PLAYERS_FOR_MISSION_INTRO_SCENE()
		PRINTLN("[MCIntroSequence] - PROCESS_INTRO_CUTSCENE - IS_FREEMODE_READY_TO_PROGRESS_AFTER_INTRO_SCENE - Player Switch In Progress.")
		RETURN FALSE
	ENDIF
	
	IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdSafetyTimer) < 30000
		IF NOT HAS_FIRST_STRAND_MISSION_BEEN_PASSED()
		OR NOT IS_THIS_IS_A_STRAND_MISSION()
		// It's okay to have an option to bypass strand checks if requested.
			
			IF IS_PLAYER_SWITCH_IN_PROGRESS()			
				IF NOT IS_SKYCAM_HOVERING_ON_LAST_CUT_DESCENT()				
					PRINTLN("[MCIntroSequence] - PROCESS_INTRO_CUTSCENE - IS_FREEMODE_READY_TO_PROGRESS_AFTER_INTRO_SCENE - Player Switch In Progress.")
					IF HAS_ROUNDS_AND_QUICK_RESTART_FINISHED()
						PRINTLN("[MCIntroSequence] - PROCESS_INTRO_CUTSCENE - IS_FREEMODE_READY_TO_PROGRESS_AFTER_INTRO_SCENE - Skycam is not moving and we haven't finished quick restart..")					
					
						IF NOT SET_SKYSWOOP_DOWN(DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)	
							PRINTLN("[MCIntroSequence] - PROCESS_INTRO_CUTSCENE - IS_FREEMODE_READY_TO_PROGRESS_AFTER_INTRO_SCENE - Skycam is not down, Calling SET_SKYSWOOP_DOWN")						
						ENDIF
					ENDIF
					
					RETURN FALSE
				ELSE
					IF NOT IS_BIT_SET(iLocalBoolCheck33, LBOOL33_QUICK_RESTART_MISSION_INTRO_SCENE_PAUSE)
						TOGGLE_PAUSED_RENDERPHASES(FALSE)
						SET_BIT(iLocalBoolCheck33, LBOOL33_QUICK_RESTART_MISSION_INTRO_SCENE_PAUSE)
					ENDIF
				ENDIF
			ENDIF
			
			SWITCH g_FMMC_STRUCT.eMissionIntroType	
				CASE MISSION_INTRO_TYPE_ARCADE_PROPERTY
					IF NOT HAS_FINISHED_MISSION_INTRO_SCENE_FOR_ARCADE_PROPERTY()
						RETURN FALSE
					ENDIF
				BREAK
				
				CASE MISSION_INTRO_TYPE_YACHT_SCENE
					IF NOT HAS_FINISHED_MISSION_INTRO_SCENE_FOR_GALAXY_YACHT()
						RETURN FALSE
					ENDIF
				BREAK
				
			ENDSWITCH
		ENDIF
	ENDIF
	
	IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdSafetyTimer) >= 30000
		PRINTLN("[MCIntroSequence] - PROCESS_INTRO_CUTSCENE - IS_FREEMODE_READY_TO_PROGRESS_AFTER_INTRO_SCENE - tdSafetyTimer >= 30000 - Took too long, skipping external intro scenes.")
	ENDIF
	
	RETURN TRUE
ENDFUNC
//End #######

PROC DO_INTRO_CUTSCENE_CAMERA(BOOL bQuickRestart)
	
	BOOL bContinue
	
	SET_ON_JOB_INTRO( TRUE )
	
	IF NOT HAS_NET_TIMER_STARTED(tdIntroTimer)
		PRINTLN("[MMacK][IntroSync] tdIntroTimer REINITIALISED")
		REINIT_NET_TIMER(tdIntroTimer)
		
		IF NOT IS_BIT_SET( iLocalBoolCheck5, LBOOL5_GIVEN_SPAWN_VEHICLE_STARTING_VELOCITY )
			IF IS_PED_IN_ANY_HELI(localPlayerPed)
				UNFREEZE_AND_PUNT_PLAYER_SPAWN_VEHICLE_FORWARDS(1.0)
				PRINTLN("[RCC MISSION] DO_INTRO_CUTSCENE_CAMERA - Calling SET_VEHICLE_FORWARD_SPEED with 1.0")
			ELSE
				UNFREEZE_AND_PUNT_PLAYER_SPAWN_VEHICLE_FORWARDS( 60.0 ) // This unfreezes the vehicle you're in - needed to start!
				SET_BIT( iLocalBoolCheck5, LBOOL5_GIVEN_SPAWN_VEHICLE_STARTING_VELOCITY )
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET( iLocalBoolCheck3, LBOOL3_CLEANED_UP_CAM )
	
		CLEAR_TRANSITION_FADE_OUT_FOR_INTERIOR_SWITCH()
	
		BOOL bCleanupCamera = TRUE
		
		CLEANUP_JOB_INTRO_CUTSCENE(jobIntroData, DEFAULT, DEFAULT, bCleanupCamera)
		CLEANUP_NEW_ANIMS(jobIntroData)
		CLEAN_UP_INTRO_STAGE_CAM(g_sTransitionSessionData.ciCam,FMMC_TYPE_MISSION,TRUE)
		
		IF NOT STRAND_MISSION_SHOULD_SKIP_INTRO_IN_MC()
		AND NOT IS_THIS_A_QUICK_RESTART_JOB()
		AND NOT IS_PED_IN_COVER(localPlayerPed)
		AND NOT IS_PED_GOING_INTO_COVER(localPlayerPed)
			SET_GAMEPLAY_CAM_RELATIVE_HEADING()
		ENDIF
		
		ANIMPOSTFX_STOP("MP_job_load")
		
		CLEANUP_ALL_CORONA_FX(FALSE)
		
		IF NOT SHOULD_MISSION_INTRO_TYPE_HANDLE_RENDERPHASES()
			PRINTLN("[IntroSync] Calling SET_SKYFREEZE_CLEAR") 	
			SET_SKYFREEZE_CLEAR(TRUE)
		ELSE
			PRINTLN("[IntroSync] Not calling SET_SKYFREEZE_CLEAR - clearing renderphase in the special mission intro procedures.")
		ENDIF
		
		IF bQuickRestart
			IF NOT STRAND_MISSION_SHOULD_SKIP_INTRO_IN_MC()
				ANIMPOSTFX_PLAY("MinigameTransitionOut", 0, FALSE)
			ENDIF
		ELSE
			IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
			AND NOT HAS_FIRST_STRAND_MISSION_BEEN_PASSED() // ?? @@
				ANIMPOSTFX_STOP("MP_job_load")
				ANIMPOSTFX_PLAY("MinigameTransitionOut", 0, FALSE)
			ENDIF
		ENDIF

		PLAY_SOUND_FRONTEND(-1,"Hit","RESPAWN_SOUNDSET", FALSE)
		SET_BIT(iLocalBoolCheck3,LBOOL3_CLEANED_UP_CAM)
	ENDIF

	PRINTLN("[MMacK][IntroSync] CURRENT TIME ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdIntroTimer)) 
		
	IF IS_MISSION_INTRO_TYPE_PLAYED_FROM_WITHIN_MISSION()
	AND (NOT bQuickRestart OR SHOULD_MISSION_INTRO_PLAY_ON_QUICK_RESTARTS())
		
		IF IS_BIT_SET(iLocalBoolCheck29, LBOOL29_MISSION_INTRO_SCENE_READY_FOR_JOB_TEXT)
		AND NOT bQuickRestart
			DRAW_JOB_TEXT(jobIntroData, -1, SHOULD_LOCAL_PLAYER_SEE_TAKE_IN_CELEBRATION_SCREENS(), DEFAULT, DEFAULT, DEFAULT, DEFAULT)
		ENDIF
			
		IF NOT IS_MISSION_READY_TO_PROGRESS_AFTER_INTRO_SCENE(bQuickRestart)
			EXIT
		ENDIF
		
		SET_BIT(iLocalBoolCheck33, LBOOL33_FINISHED_MISSION_INTRO_SCENE)
		
		bContinue = TRUE
		
	ELSE	
		IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
		AND NOT STRAND_MISSION_SHOULD_SKIP_INTRO_IN_MC()
		AND NOT bQuickRestart
			IF IS_SCREEN_FADED_IN()
				IF DRAW_JOB_TEXT(jobIntroData, -1, SHOULD_LOCAL_PLAYER_SEE_TAKE_IN_CELEBRATION_SCREENS(), DEFAULT, DEFAULT, DEFAULT, DEFAULT)
					bContinue = TRUE
				ELSE
					PROCESS_APPLY_INTRO_MOVE_FORWARD()
					PRINTLN("[MMacK][IntroSync] [RCC MISSION] - DO_INTRO_CUTSCENE_CAMERA - waiting for DRAW_JOB_TEXT")
				ENDIF
			ELSE
				PRINTLN("[MMacK][IntroSync] [RCC MISSION] - DO_INTRO_CUTSCENE_CAMERA - waiting for Screen to be faded in.")
				
				IF NOT IS_SCREEN_FADING_IN()
					PRINTLN("[MMacK][IntroSync] [RCC MISSION] - DO_INTRO_CUTSCENE_CAMERA - Calling to FadeIn.")
					DO_SCREEN_FADE_IN(500)
				ENDIF
			ENDIF
		ELSE
			bContinue = TRUE
		ENDIF
	ENDIF
	
	IF bContinue

		PRINTLN("[MMacK][IntroSync] GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME HAS EXPIRED")
	
		IF NOT bIsAnySpectator
			NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
		ENDIF
		
		IF SHOULD_TRANSITION_SESSIONS_HIDE_RADAR_UNTILL_LAUNCHED()
			CLEAR_TRANSITION_SESSIONS_HIDE_RADAR_UNTILL_LAUNCHED()
		ENDIF
		
		IF bQuickRestart
			IF DOES_ENTITY_EXIST(LocalPlayerPed)
				IF NOT IS_ENTITY_DEAD(LocalPlayerPed)
				AND NOT IS_BIT_SET(iLocalBoolCheck30, LBOOL30_SET_PLAYER_INTO_COVER_START_POSITION)
					FREEZE_ENTITY_POSITION(LocalPlayerPed, FALSE)
					CLEAR_PED_TASKS(LocalPlayerPed)
					PRINTLN("[MMacK][IntroSync] DO_INTRO_CUTSCENE_CAMERA FREEZE_ENTITY_POSITION(LocalPlayerPed, FALSE) & CLEAR_PED_TASKS ")
				ENDIF
			ENDIF
		ENDIF
		
		IF THEFEED_IS_PAUSED()
			THEFEED_RESUME()
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFive, ciOptionsBS25_AutoEquipMaskAtStart)
			g_bAutoMaskEquip = TRUE
			PRINTLN("[Mask] DO_INTRO_CUTSCENE_CAMERA - Setting g_bAutoMaskEquip")
		ENDIF
		
		CLEANUP_CELEBRATION_SCALEFORMS(jobIntroData)
		APPLY_CORONA_ALCOHOL_AND_SMOKE_EFFECTS(FALSE, FALSE, TRUE)
		DO_CORONA_POLICE_SETTINGS()
		SET_HIDOF_OVERRIDE(FALSE,FALSE,0,0,0,0)
		SET_ON_JOB_INTRO(FALSE)
		CHECK_EMERGENCY_CLEANUP()
		SyncCameraAndSkyswoopDownStages = 0
		SET_PLAYER_INTO_COVER_AFTER_INITIAL_SPAWNING(TRUE)
		
		IF HAS_FIRST_STRAND_MISSION_BEEN_PASSED_NO_RESET()
		AND IS_SCREEN_FADED_OUT()
			PRINTLN("DO_INTRO_CUTSCENE_CAMERA - Faded from transition - Fading in")
			DO_SCREEN_FADE_IN(500)
		ENDIF
		
		PRINTLN("Resseting SyncCameraAndSkyswoopDownStages")
		
		SET_BIT(MC_PlayerBD[iLocalPart].iClientBitset, PBBOOL_FINISHED_INTRO_CUTSCENE)
	ENDIF
ENDPROC

// If we quick warp or otherwise move the player during a PLAYER_SWITCH and this new position is beyond the 300m~ streaming distance then we are going to FadeOut, kill the player switch, Start a load scene, and then FadeIn.
FUNC BOOL SHOULD_USE_LOAD_SCENE_FOR_LONG_DISTNACE_SKY_CAMS()
	IF IS_BIT_SET(iLocalBoolCheck31, LBOOL31_SHOULD_USE_SKY_CAM_FADE_LOAD_SCENE)
		RETURN TRUE
	ENDIF
	
	VECTOR vPlayerSwitchPos = GET_FINAL_RENDERED_CAM_COORD()	
	GET_GROUND_Z_FOR_3D_COORD(vPlayerSwitchPos, vPlayerSwitchPos.z, TRUE, TRUE)
	
	IF IS_SKYCAM_HOVERING_ON_LAST_CUT_DESCENT()
	AND NOT IS_VECTOR_ZERO(vPlayerSwitchPos)
		VECTOR vPed
		vPed = GET_ENTITY_COORDS(localPlayerPed)
		
		FLOAT fDist, fMaxDist
		fDist = VDIST2(vPed, vPlayerSwitchPos)
		fMaxDist = POW(290, 2)
		
		PRINTLN("[LM][RCC MISSION][Init_Load_Scene] - vPlayerSwitchPos: ", vPlayerSwitchPos, " vPed: ", vPed, " fDist: ", SQRT(fDist), " fMaxDist: ", SQRT(fMaxDist))
		
		IF fDist > fMaxDist
			SET_BIT(iLocalBoolCheck31, LBOOL31_SHOULD_USE_SKY_CAM_FADE_LOAD_SCENE)
			RETURN TRUE
		ENDIF
	ENDIF	
	RETURN FALSE
ENDFUNC

FUNC BOOL CREATE_LOAD_SCENE_FOR_LONG_DISTANCE_SKY_CAMS(#IF IS_DEBUG_BUILD STRING sFunctionName #ENDIF)
	PRINTLN("[LM][RCC MISSION][Init_Load_Scene] ", sFunctionName, " Processing Load Scene ---------------------------------")
	
	VECTOR vPed		
	vPed = GET_ENTITY_COORDS(localPlayerPed)

	IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
		IF NOT IS_BIT_SET(iLocalBoolCheck31, LBOOL31_STARTED_LOAD_SCENE_FOR_SCRIPT_INIT)
			PRINTLN("[LM][RCC MISSION][Init_Load_Scene] ", sFunctionName, " - Starting the Load Scene.")
			SET_BIT(iLocalBoolCheck31, LBOOL31_STARTED_LOAD_SCENE_FOR_SCRIPT_INIT)
			START_NET_TIMER(tdProcessScriptInitLoadSceneTimeout)
			NEW_LOAD_SCENE_START_SPHERE(vPed, 100)
		ENDIF
	ELIF IS_NEW_LOAD_SCENE_LOADED()
		IF IS_BIT_SET(iLocalBoolCheck31, LBOOL31_STARTED_LOAD_SCENE_FOR_SCRIPT_INIT)
			PRINTLN("[LM][RCC MISSION][Init_Load_Scene] ", sFunctionName, " - Stopping the Load Scene - It has loaded")
			SET_BIT(iLocalBoolCheck31, LBOOL31_FINISHED_LOAD_SCENE_FOR_SCRIPT_INIT)
			NEW_LOAD_SCENE_STOP()
		ENDIF
	ELSE
		PRINTLN("[LM][RCC MISSION][Init_Load_Scene] ", sFunctionName, " - CREATE_LOAD_SCENE_FOR_LONG_DISTANCE_SKY_CAMS Returning FALSE.")
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck31, LBOOL31_STARTED_LOAD_SCENE_FOR_SCRIPT_INIT)
	AND NOT IS_BIT_SET(iLocalBoolCheck31, LBOOL31_FINISHED_LOAD_SCENE_FOR_SCRIPT_INIT)
		PRINTLN("[LM][RCC MISSION][Init_Load_Scene] ", sFunctionName, " - Waiting for load scene to finish or for Cam to have been pulled down.")
		
		IF HAS_NET_TIMER_STARTED(tdProcessScriptInitLoadSceneTimeout)
			IF NOT HAS_NET_TIMER_EXPIRED_READ_ONLY(tdProcessScriptInitLoadSceneTimeout, ciPROCESS_SCRIPT_INIT_LOAD_SCENE_TIMEOUT)						
				PRINTLN("[LM][RCC MISSION][Init_Load_Scene] ", sFunctionName, " - CREATE_LOAD_SCENE_FOR_LONG_DISTANCE_SKY_CAMS Returning False.")
				RETURN FALSE
			ELSE
				PRINTLN("[LM][RCC MISSION][Init_Load_Scene] ", sFunctionName, " - TIMED OUT !!!!")
				SET_BIT(iLocalBoolCheck31, LBOOL31_FINISHED_LOAD_SCENE_FOR_SCRIPT_INIT)
				NEW_LOAD_SCENE_STOP()
			ENDIF
		ENDIF
	ENDIF
	
	PRINTLN("[LM][RCC MISSION][Init_Load_Scene] ", sFunctionName, " - CREATE_LOAD_SCENE_FOR_LONG_DISTANCE_SKY_CAMS Returning True.")
	
	DO_SCREEN_FADE_IN(500)
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SYNC_PLAYERS_AND_SKYSWOOP_DOWN(INT& iStage)

	SWITCH iStage
		
		CASE 0
			IF NOT IS_SKYSWOOP_MOVING()
				IF HAS_ROUNDS_AND_QUICK_RESTART_FINISHED()
					iStage++
				ELSE
					PRINTLN("[RCC MISSION][ROUNDCAM] SYNC_PLAYERS_AND_SKYSWOOP_DOWN: HAS_ROUNDS_AND_QUICK_RESTART_FINISHED = FALSE") 
				ENDIF
			ELSE
				PRINTLN("[RCC MISSION][ROUNDCAM] SYNC_PLAYERS_AND_SKYSWOOP_DOWN: IS_SKYSWOOP_MOVING() = TRUE, not need to start the pan. Move on. ") 
				iStage++
			ENDIF
		BREAK
		
		CASE 1
			
			IF HAS_SERVER_TIME_PASSED_ROUND(6000)
			OR IS_BIT_SET(iLocalBoolCheck33, LBOOL33_FINISHED_MISSION_INTRO_SCENE)
				IF ARE_ALL_PLAYERS_READY_TO_PROCEED_AFTER_PERSONAL_VEHICLE_CREATION()						
					IF NOT OK_TO_PLAY_RESTART_ANIM_ON_STRAND()
						IF processEveryFramePlayerActionMode != NULL
							CALL processEveryFramePlayerActionMode( TRUE )
						ENDIF
						GIVE_LOCAL_PLAYER_WEAPON_INVENTORY(FMMC_INVENTORY_STARTING, WEAPONINHAND_LASTWEAPON_BOTH)
					ENDIF
					iStage++
				ELSE
					PRINTLN("[RCC MISSION][ROUNDCAM] SYNC_PLAYERS_AND_SKYSWOOP_DOWN: ARE_ALL_PLAYERS_READY_TO_PROCEED_AFTER_PERSONAL_VEHICLE_CREATION = FALSE ") 
				ENDIF
			ELSE
				PRINTLN("[RCC MISSION][ROUNDCAM] SYNC_PLAYERS_AND_SKYSWOOP_DOWN: HAS_SERVER_TIME_PASSED_ROUND = FALSE ") 
			ENDIF
			
		BREAK
		
		CASE 2
			PRINTLN("[RCC MISSION][ROUNDCAM] SYNC_PLAYERS_AND_SKYSWOOP_DOWN: waiting for the camera to finish ")
				
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_EnableCasino_InteriorScripts)
				IF NOT g_bInitCasinoPedsCreated
				AND IS_PLAYER_WITHIN_SIMPLE_INTERIOR_BOUNDS(SIMPLE_INTERIOR_CASINO)
					IF NOT IS_BIT_SET(iLocalBoolCheck31, LBOOL31_REQUEST_CASINO_INTERIOR_SCRIPT)
						REQUEST_RELAUCHING_OF_SIMPLE_INTERIOR_INT_SCRIPT(SIMPLE_INTERIOR_CASINO)
						SET_BIT(iLocalBoolCheck31, LBOOL31_REQUEST_CASINO_INTERIOR_SCRIPT)
						PRINTLN("[RCC MISSION][ROUNDCAM] SYNC_PLAYERS_AND_SKYSWOOP_DOWN: REQUEST_RELAUCHING_OF_SIMPLE_INTERIOR_INT_SCRIPT(SIMPLE_INTERIOR_CASINO), LBOOL31_REQUEST_CASINO_INTERIOR_SCRIPT SET")
					ENDIF
					
					PRINTLN("[RCC MISSION][ROUNDCAM] SYNC_PLAYERS_AND_SKYSWOOP_DOWN: waiting for the casino peds to be created")
					RETURN FALSE
				ENDIF
			ENDIF
			
			//url:bugstar:5831938
			IF SHOULD_USE_LOAD_SCENE_FOR_LONG_DISTNACE_SKY_CAMS()
				IF NOT IS_BIT_SET(iLocalBoolCheck31, LBOOL31_ENDED_SKY_CAM_FOR_LOAD_SCENE_FOR_SCRIPT_INIT)
					IF NOT IS_SCREEN_FADING_OUT()
					AND NOT IS_SCREEN_FADED_OUT()
						PRINTLN("[LM][RCC MISSION][ROUNDCAM] SYNC_PLAYERS_AND_SKYSWOOP_DOWN: Fading Screen Out...")
						DO_SCREEN_FADE_OUT(500)
					ENDIF				
					IF IS_SCREEN_FADED_OUT()
						PRINTLN("[LM][RCC MISSION][ROUNDCAM] SYNC_PLAYERS_AND_SKYSWOOP_DOWN:  - KILL_SKYCAM")
						KILL_SKYCAM()
						SET_BIT(iLocalBoolCheck31, LBOOL31_ENDED_SKY_CAM_FOR_LOAD_SCENE_FOR_SCRIPT_INIT)
					ENDIF
				ELSE
					IF CREATE_LOAD_SCENE_FOR_LONG_DISTANCE_SKY_CAMS(#IF IS_DEBUG_BUILD "SYNC_PLAYERS_AND_SKYSWOOP_DOWN" #ENDIF)
						PRINTLN("[RCC MISSION][ROUNDCAM] SYNC_PLAYERS_AND_SKYSWOOP_DOWN: We need to do a load scene first. ")
						IF IS_BIT_SET(iLocalBoolCheck31, LBOOL31_REQUEST_CASINO_INTERIOR_SCRIPT)
							CLEAR_BIT(iLocalBoolCheck31, LBOOL31_REQUEST_CASINO_INTERIOR_SCRIPT)
							PRINTLN("[RCC MISSION][ROUNDCAM] SYNC_PLAYERS_AND_SKYSWOOP_DOWN: LBOOL31_REQUEST_CASINO_INTERIOR_SCRIPT CLEAR 1")
						ENDIF
						RETURN TRUE
					ENDIF
				ENDIF
				
				RETURN FALSE
			ENDIF			
			
			IF SET_SKYSWOOP_DOWN(DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)				
				PRINTLN("[RCC MISSION][ROUNDCAM] SYNC_PLAYERS_AND_SKYSWOOP_DOWN: RETURN TRUE ") 
				IF IS_BIT_SET(iLocalBoolCheck31, LBOOL31_REQUEST_CASINO_INTERIOR_SCRIPT)
					CLEAR_BIT(iLocalBoolCheck31, LBOOL31_REQUEST_CASINO_INTERIOR_SCRIPT)
					PRINTLN("[RCC MISSION][ROUNDCAM] SYNC_PLAYERS_AND_SKYSWOOP_DOWN: LBOOL31_REQUEST_CASINO_INTERIOR_SCRIPT CLEAR 2")
				ENDIF
				RETURN TRUE
			ENDIF
			
			SET_PLAYER_INTO_COVER_AFTER_INITIAL_SPAWNING(TRUE)
		BREAK
		
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC


PROC PROCESS_INTRO_CUTSCENE()
	
	INT iTeam = MC_PlayerBD[iPartToUse].iTeam	
	IF iTeam = -1
		iTeam = GlobalplayerBD_FM[iPartToUse].sClientCoronaData.iTeamChosen
		IF iTeam = -1
			iTeam = 0
		ENDIF
	ENDIF
	
	g_b_TransitionActive = FALSE

	HIDE_HUD_AND_RADAR_THIS_FRAME()
	
	IF bLocalPlayerPedOk
		SET_PED_RESET_FLAG(LocalPlayerPed, PRF_CannotBeTargetedByAI, TRUE)
	ENDIF	

	IF NOT HAS_NET_TIMER_STARTED(tdSafetyTimer)
		REINIT_NET_TIMER(tdSafetyTimer)
		IF IS_PLAYLIST_SETTING_CHALLENGE_TIME()
			PRINT_HELP("FMMC_PL_STCL")
		ENDIF
	ENDIF
	
	SET_BIT(g_CoronaJobLaunchData.iBS_CoronaJobFlags, ciCORONA_FAKE_PED_CLEANUP)
	
	g_b_HasCheckedApartmentInfo = TRUE
	
	PRINTLN("[RCC MISSION] PROCESS_INTRO_CUTSCENE - g_b_HasCheckedApartmentInfo TRUE") 
	
	IF (HAS_SERVER_CHECKED_ALL_CLIENTS_READY_FOR_INTRO()
	OR GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdSafetyTimer) > 50000)
			
		IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdSafetyTimer) > 50000
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] PROCESS_INTRO_CUTSCENE - tdSafetyTimer has expired, doing intro things" )
		ENDIF
				
		IF SHOULD_MISSION_INTRO_TYPE_HANDLE_RENDERPHASES()
		AND GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdSafetyTimer) >= 30000
			PRINTLN("[IntroSync] Calling SET_SKYFREEZE_CLEAR - Safety Timer Expired.") 	
			SET_SKYFREEZE_CLEAR(TRUE)
		ENDIF
		
		IF GET_MISSION_VEHICLE_DENSITY_FROM_INT(g_FMMC_STRUCT.iTraffic) = 0
		AND GET_PED_DENSITY_FROM_INT(g_FMMC_STRUCT.iPedDensity) = 0
			CLEAR_AREA_LEAVE_VEHICLE_HEALTH(GET_ENTITY_COORDS(LocalPlayerPed), 5000, TRUE)
		ENDIF
		
		IF NOT bIsAnySpectator
			IF NOT IS_SCREEN_FADED_IN()
			AND NOT IS_SCREEN_FADING_IN()
			AND NOT SHOULD_USE_LOAD_SCENE_FOR_LONG_DISTNACE_SKY_CAMS()
				DO_SCREEN_FADE_IN(500)
			ENDIF
		ENDIF
		
		SETUP_TEAM_CHAT_AND_TARGETTING_RESTRICTIONS()			
	
		IF NOT IS_BIT_SET( iLocalBoolCheck5, LBOOL5_GIVEN_SPAWN_VEHICLE_STARTING_VELOCITY )
		AND IS_BIT_SET( iLocalBoolCheck28, LBOOL28_USING_PV )
			IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
				SET_VEHICLE_ON_GROUND_PROPERLY(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
				UNFREEZE_AND_PUNT_PLAYER_SPAWN_VEHICLE_FORWARDS( 0.1 )
				SET_BIT( iLocalBoolCheck5, LBOOL5_GIVEN_SPAWN_VEHICLE_STARTING_VELOCITY )
				PRINTLN("[RCC MISSION] PROCESS_INTRO_CUTSCENE - PV has been released from being frozen")
			ENDIF
		ENDIF	
		
		IF NOT SHOULD_INTRO_CUTSCENE_MOVE_TO_SOLO_PLAY()
			NET_NL()NET_PRINT("[ROUNDCAM] PROCESS_INTRO_CUTSCENE: IS_THIS_A_QUICK_RESTART_JOB = ")NET_PRINT_BOOL(IS_THIS_A_QUICK_RESTART_JOB())
			NET_NL()NET_PRINT("[ROUNDCAM] PROCESS_INTRO_CUTSCENE: IS_THIS_A_ROUNDS_MISSION = ")NET_PRINT_BOOL(IS_THIS_A_ROUNDS_MISSION())
											
			// This adds a point of sync where heli cutscene will wait for mission controller to be in this state
			g_OfficeHeliDockData.bMissionControllerInProcessIntro = TRUE
			
			IF NOT HAS_NET_TIMER_STARTED(tdSVMIntroSafetyTimer)
			OR (HAS_NET_TIMER_STARTED(tdSVMIntroSafetyTimer) AND NOT HAS_NET_TIMER_EXPIRED(tdSVMIntroSafetyTimer, g_sMPTunables.iSVMIntroSafetyTimeLimit))
				// Wait till the exit cutscene is done					
				IF NOT IS_THIS_A_QUICK_RESTART_JOB()
				OR IS_THIS_A_QUICK_RESTART_JOB() AND NOT (FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(1) OR FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(0))
					#IF IS_DEBUG_BUILD
					IF (bSkipSVMIntro AND NOT WAS_MISSION_LAUNCHED_THROUGH_Z_MENU())
					OR NOT bSkipSVMIntro
					#ENDIF
						IF IS_MISSION_INTRO_TYPE_PLAYED_FROM_FREEMODE()
						AND NOT IS_FREEMODE_READY_TO_PROGRESS_AFTER_INTRO_SCENE()
							EXIT
						ENDIF
					#IF IS_DEBUG_BUILD
					ENDIF
					#ENDIF
				#IF IS_DEBUG_BUILD
				ELSE
					PRINTLN("[RCC MISSION] PROCESS_INTRO_CUTSCENE - NOT IS_THIS_A_QUICK_RESTART_JOB(): ", (NOT IS_THIS_A_QUICK_RESTART_JOB()))
					PRINTLN("[RCC MISSION] PROCESS_INTRO_CUTSCENE - g_TransitionSessionNonResetVars.iRestartBitset: ", g_TransitionSessionNonResetVars.iRestartBitset)
				#ENDIF
				ENDIF
			ELSE
				CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] PROCESS_INTRO_CUTSCENE - Skipping the svm intro stuff we've been stuck for ", g_sMPTunables.iSVMIntroSafetyTimeLimit)
			ENDIF
			
			IF IS_MISSION_INTRO_TYPE_PLAYED_FROM_FREEMODE()
				SET_BIT(iLocalBoolCheck33, LBOOL33_FINISHED_MISSION_INTRO_SCENE)
			ENDIF
			
			IF IS_THIS_A_QUICK_RESTART_JOB()
			
				IF SYNC_PLAYERS_AND_SKYSWOOP_DOWN(SyncCameraAndSkyswoopDownStages) // SET_SKYSWOOP_DOWN() 
					CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][MMacK][IntroSync] PROCESS_INTRO_CUTSCENE - Calling DO_INTRO_CUTSCENE_CAMERA, IS_THIS_A_QUICK_RESTART_JOB & SET_SKYSWOOP_DOWN = TRUE")
					DO_INTRO_CUTSCENE_CAMERA( TRUE )
				ELSE // Else skyswoop is not down
					CPRINTLN( DEBUG_CONTROLLER,"[RCC MISSION] PROCESS_INTRO_CUTSCENE : STUCK - WAITING - SYNC_PLAYERS_AND_SKYSWOOP_DOWN 2 ") 
				ENDIF
			ELSE // Else this is not a quick restart job
				CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION][MMacK][IntroSync] PROCESS_INTRO_CUTSCENE - Calling DO_INTRO_CUTSCENE_CAMERA, IS_THIS_A_QUICK_RESTART_JOB = FALSE")
				
				DO_INTRO_CUTSCENE_CAMERA( FALSE )
			ENDIF
			
		ELSE // Else this isn't a heist or contact mission				
			IF IS_THIS_A_QUICK_RESTART_JOB()
			OR IS_THIS_A_ROUNDS_MISSION()
				IF IS_THIS_A_ROUNDS_MISSION() OR IS_THIS_A_QUICK_RESTART_JOB() OR SET_SKYSWOOP_DOWN()	
					
					IF NOT IS_BIT_SET( iLocalBoolCheck5, LBOOL5_GIVEN_SPAWN_VEHICLE_STARTING_VELOCITY )
						UNFREEZE_AND_PUNT_PLAYER_SPAWN_VEHICLE_FORWARDS( 60.0 ) // This unfreezes the vehicle you're in - needed to start!
						SET_BIT( iLocalBoolCheck5, LBOOL5_GIVEN_SPAWN_VEHICLE_STARTING_VELOCITY )
					ENDIF
					
					IF NOT DID_PLAYER_JOIN_MISSION_AS_SPECTATOR( LocalPlayer )
						SET_ENTITY_VISIBLE(LocalPlayerPed,TRUE)
					ENDIF
					SET_AMBIENT_PED_RANGE_MULTIPLIER_THIS_FRAME(1.5)
					SET_AMBIENT_VEHICLE_RANGE_MULTIPLIER_THIS_FRAME(1.5)
					SET_PAUSE_INTRO_DROP_AWAY(TRUE)
					PRINTLN("[RCC MISSION] PROCESS_INTRO_CUTSCENE : Moving from GAME_STATE_INTRO_CUTSCENE to GAME_STATE_SOLO_PLAY, this is a rounds mission or quick restart")					
				ELSE
					PRINTLN("[RCC MISSION]  PROCESS_INTRO_CUTSCENE : STUCK - WAITING - SET_SKYSWOOP_DOWN 1 ")
				ENDIF
			ELSE
				IF NOT DID_PLAYER_JOIN_MISSION_AS_SPECTATOR( LocalPlayer )
					SET_ENTITY_VISIBLE(LocalPlayerPed,TRUE)
				ENDIF
				SET_AMBIENT_PED_RANGE_MULTIPLIER_THIS_FRAME(1.5)
				SET_AMBIENT_VEHICLE_RANGE_MULTIPLIER_THIS_FRAME(1.5)
				PRINTLN("[RCC MISSION] PROCESS_INTRO_CUTSCENE : Moving from GAME_STATE_INTRO_CUTSCENE to GAME_STATE_SOLO_PLAY, this is NOT a rounds mission or quick restart")				
			ENDIF
		ENDIF
	
	ELSE
		CPRINTLN( DEBUG_CONTROLLER,"[RCC MISSION] HAS_EVERYONE_REACHED_CUTSCENE = FALSE")
	ENDIF

	//Play the crashed plane sound now that the plane is visiable if it exists.
	IF iCrashSoundProp > -1
		IF HAS_SOUND_FINISHED(iCrashSound)
			vCrashSoundPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(oiProps[iCrashSoundProp],<<0.0,6.5,0.0>>)
			PLAY_SOUND_FROM_COORD(iCrashSound, "Crashed_Plane_Ambience", vCrashSoundPos, "DLC_Apartments_Extraction_SoundSet", FALSE, 0,FALSE)
		ENDIF
	ENDIF

ENDPROC

// This function deals with the cutscene that displays during the first few seconds of the mission
PROC PROCESS_TEAM_CUTSCENE()
	
	BOOL bDidJobIntroText
	
	IF NOT HAS_NET_TIMER_STARTED(tdSafetyTimer)
		REINIT_NET_TIMER(tdSafetyTimer)
	ENDIF
	
	HIDE_HUD_AND_RADAR_THIS_FRAME()
	
	IF HAS_SERVER_CHECKED_ALL_CLIENTS_READY_FOR_INTRO()
	OR GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdSafetyTimer) > 50000
	
		IF GET_MISSION_VEHICLE_DENSITY_FROM_INT(g_FMMC_STRUCT.iTraffic) = 0
		AND GET_PED_DENSITY_FROM_INT(g_FMMC_STRUCT.iPedDensity) = 0
			CLEAR_AREA_LEAVE_VEHICLE_HEALTH(GET_ENTITY_COORDS(LocalPlayerPed), 5000, TRUE)
		ENDIF
		
		SETUP_TEAM_CHAT_AND_TARGETTING_RESTRICTIONS()
	
		IF NOT IS_THIS_A_VERSUS_MISSION()
			IF NOT IS_BIT_SET( iLocalBoolCheck5, LBOOL5_GIVEN_SPAWN_VEHICLE_STARTING_VELOCITY )
				UNFREEZE_AND_PUNT_PLAYER_SPAWN_VEHICLE_FORWARDS( 60.0 )
				SET_BIT( iLocalBoolCheck5, LBOOL5_GIVEN_SPAWN_VEHICLE_STARTING_VELOCITY )
			ENDIF
		
			IF NOT IS_SCREEN_FADED_IN()
			AND NOT IS_SCREEN_FADING_IN()
				DO_SCREEN_FADE_IN(500)
			ENDIF
			
			SET_ON_JOB_INTRO(TRUE)
			
			IF NOT HAS_NET_TIMER_STARTED(tdIntroTimer)
				REINIT_NET_TIMER(tdIntroTimer)
				IF IS_PLAYLIST_SETTING_CHALLENGE_TIME()
					PRINT_HELP("FMMC_PL_STCL")
				ENDIF
			ENDIF
			
			IF NOT IS_BIT_SET( iLocalBoolCheck3,LBOOL3_CLEANED_UP_CAM )
				CLEANUP_JOB_INTRO_CUTSCENE(jobIntroData) 
				CLEANUP_NEW_ANIMS(jobIntroData)
				CLEAN_UP_INTRO_STAGE_CAM(g_sTransitionSessionData.ciCam,FMMC_TYPE_MISSION,TRUE)
				ANIMPOSTFX_STOP("MP_job_load")
				CLEANUP_ALL_CORONA_FX(FALSE)
				SET_SKYFREEZE_CLEAR()
				SET_BIT(iLocalBoolCheck3,LBOOL3_CLEANED_UP_CAM)
			ENDIF
			
			IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
			AND NOT STRAND_MISSION_SHOULD_SKIP_INTRO_IN_MC()
				IF IS_SCREEN_FADED_IN()
					IF DRAW_JOB_TEXT(jobIntroData, -1, SHOULD_LOCAL_PLAYER_SEE_TAKE_IN_CELEBRATION_SCREENS(), DEFAULT, DEFAULT, DEFAULT, DEFAULT)
						bDidJobIntroText = TRUE
					ELSE
						PROCESS_APPLY_INTRO_MOVE_FORWARD()
						PRINTLN("[RCC MISSION] - PROCESS_TEAM_CUTSCENE - waiting for DRAW_JOB_TEXT")
					ENDIF
				ENDIF
			ELSE
				bDidJobIntroText = TRUE
			ENDIF
			
			IF bDidJobIntroText
				PRINTLN("[RCC MISSION] TEAM CUT DONE - MOVE TO RUNNING")
				RESET_NET_TIMER(tdSafetyTimer)
				RESET_NET_TIMER(RoundCutsceneTimeOut)	// [NG-INTEGRATE] Next-gen only.
				NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
				IF SHOULD_TRANSITION_SESSIONS_HIDE_RADAR_UNTILL_LAUNCHED()
					CLEAR_TRANSITION_SESSIONS_HIDE_RADAR_UNTILL_LAUNCHED()
				ENDIF
				
				IF THEFEED_IS_PAUSED()
					THEFEED_RESUME()
				ENDIF
				
				SET_PLAYER_INTO_COVER_AFTER_INITIAL_SPAWNING(TRUE)
				CLEANUP_JOB_INTRO_CUTSCENE(jobIntroData) 
				CLEANUP_CELEBRATION_SCALEFORMS(jobIntroData)
				APPLY_CORONA_ALCOHOL_AND_SMOKE_EFFECTS(FALSE, FALSE, TRUE)
				DO_CORONA_POLICE_SETTINGS()
				SET_HIDOF_OVERRIDE(FALSE,FALSE,0,0,0,0)
				SET_ON_JOB_INTRO(FALSE)
				CHECK_EMERGENCY_CLEANUP()
				
				SET_BIT(MC_PlayerBD[iLocalPart].iClientBitset, PBBOOL_FINISHED_INTRO_CUTSCENE)	
			ENDIF
			
		ELSE // ELSE we're not on a contact mission
			IF NOT IS_BIT_SET( iLocalBoolCheck5, LBOOL5_GIVEN_SPAWN_VEHICLE_STARTING_VELOCITY )
			AND IS_BIT_SET( iLocalBoolCheck28, LBOOL28_USING_PV )
				IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
					SET_VEHICLE_ON_GROUND_PROPERLY(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
					UNFREEZE_AND_PUNT_PLAYER_SPAWN_VEHICLE_FORWARDS(0.1)
					SET_BIT( iLocalBoolCheck5, LBOOL5_GIVEN_SPAWN_VEHICLE_STARTING_VELOCITY )
					PRINTLN("[RCC MISSION] PROCESS_TEAM_CUTSCENE - PV has been released from being frozen")
				ENDIF
			ENDIF			
			CLEAR_BIT(iLocalBoolCheck3,LBOOL3_TEAM_INTRO)
		ENDIF
	ELSE
		PRINTLN("[RCC MISSION][PROCESS_TEAM_CUTSCENE] HAS_EVERYONE_REACHED_TEAMCUTSCENE = FALSE") NET_NL()
	ENDIF

	//Play the crashed plane sound now that the plane is visiable if it exists.
	IF iCrashSoundProp > -1
		IF HAS_SOUND_FINISHED(iCrashSound)
			vCrashSoundPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(oiProps[iCrashSoundProp],<<0.0,6.5,0.0>>)
			PLAY_SOUND_FROM_COORD(iCrashSound, "Crashed_Plane_Ambience", vCrashSoundPos, "DLC_Apartments_Extraction_SoundSet", FALSE, 0,FALSE)
		ENDIF
	ENDIF
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_HALLOWEEN_ADVERSARY_2022(g_FMMC_STRUCT.iAdversaryModeType)
		PLAY_SOUND_FRONTEND(-1, "Match_Intro", "Halloween_Adversary_Sounds")
	ENDIF

ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Start-up Logic
// ##### Description: Main Functions involved in processing the startup of the mission and used in the main state machine. Where the player spawns is called here, as well as skyswoop etc.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC CHANGE_PLAYER_TEAM(INT iNewTeam, BOOL bMidMission)
	
	PRINTLN("[InitPosition] - CHANGE_PLAYER_TEAM - Changing to team ",iNewTeam," for my participant (",iLocalPart,") - bMidMission = ",bMidMission,", old team = ",MC_playerBD[iLocalPart].iteam)
		
	MC_playerBD[iLocalPart].iteam = iNewTeam
	g_i_Mission_team = iNewTeam
	
	IF bMidMission
		SET_PLAYER_TEAM(LocalPlayer, iNewTeam)
		GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].sClientCoronaData.iTeamChosen = iNewTeam //so respawn points work
		IF DECOR_EXIST_ON(GET_PLAYER_PED_SCRIPT_INDEX(LocalPlayer), "TeamId")
			DECOR_REMOVE(GET_PLAYER_PED_SCRIPT_INDEX(LocalPlayer), "TeamId")
			PRINTLN("[InitPosition] - CHANGE_PLAYER_TEAM - DECOR_REMOVE(GET_PLAYER_PED_SCRIPT_INDEX(LocalPlayer), TeamId)")
		#IF IS_DEBUG_BUILD
		ELSE
			PRINTLN("[InitPosition] - CHANGE_PLAYER_TEAM - We're changing team mid-mission, but the TeamId decorator doesn't exist on my player")
		#ENDIF
		ENDIF
	ENDIF
	
	IF bIsLocalPlayerHost
		MC_serverBD.iHostRefreshDpadValue++
	ENDIF
	
	MC_PlayerBD[iLocalPart].bSwapTeamStarted = TRUE
	MC_PlayerBD[iLocalPart].bSwapTeamFinished = TRUE
		
	SET_LOCAL_PLAYERS_FM_TEAM_DECORATOR(iNewTeam)
			
	IF bMidMission
		SET_BIT(iLocalBoolCheck16,LBOOL16_CHANGE_PLAYER_TEAM_ALIVE_VARS_SETTER)
		
		PRINTLN("[InitPosition] - CHANGE_PLAYER_TEAM - it's CLOBBERING TIME!")
	ENDIF
ENDPROC

FUNC BOOL GET_ANY_TEAM_GAPS()
	BOOL bLastTeamGap = FALSE
	INT iTeamLoop
	FOR iTeamLoop = 0 TO FMMC_MAX_TEAMS - 2 //Don't care about the last team
		INT iPlayerLoop
		PRINTLN("[PLAYER_LOOP] - GET_ANY_TEAM_GAPS")
		FOR iPlayerLoop = 0 TO MAX_NUM_MC_PLAYERS - 1
			PLAYER_INDEX piTempPlayer = INT_TO_NATIVE(PLAYER_INDEX, iPlayerLoop)
			IF NETWORK_IS_PLAYER_ACTIVE(piTempPlayer)
				IF NETWORK_IS_PLAYER_A_PARTICIPANT(piTempPlayer)
					IF NETWORK_IS_PARTICIPANT_ACTIVE(NETWORK_GET_PARTICIPANT_INDEX(piTempPlayer))
						IF GlobalplayerBD_FM[iPlayerLoop].sClientCoronaData.iTeamChosen = iTeamLoop
							PRINTLN("[InitPosition] - GET_ANY_TEAM_GAPS - Player ", iPlayerLoop," is on team ", iTeamLoop)  
							IF bLastTeamGap
								RETURN TRUE
							ENDIF
							bLastTeamGap = FALSE
							BREAKLOOP
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			IF iPlayerLoop = MAX_NUM_MC_PLAYERS - 1
				PRINTLN("[InitPosition] - GET_ANY_TEAM_GAPS - Team ", iTeamLoop," is a gap")  
				bLastTeamGap = TRUE
			ENDIF
		ENDFOR
	ENDFOR

	RETURN FALSE
ENDFUNC

PROC ASSIGN_PLAYER_TEAM()

	g_bFM_ON_TEAM_MISSION = TRUE
	g_iOverheadNamesState = TAGS_WHEN_INTEAM
	
	INT iTeam
	
	IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
		iTeam = GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].sClientCoronaData.iTeamChosen     
		PRINTLN("[InitPosition] - ASSIGN_PLAYER_TEAM - IS_FAKE_MULTIPLAYER_MODE_SET() = FALSE, setting MC_playerBD[iLocalPart].iteam = GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].sClientCoronaData.iTeamChosen : ",GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].sClientCoronaData.iTeamChosen) 
		IF bIsLocalPlayerHost
			IF GET_ANY_TEAM_GAPS()
				INT iTeamLoop
				FOR iTeamLoop = 0 TO FMMC_MAX_TEAMS - 1
					IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet,SSBOOL_TEAM0_FAILED + iTeamLoop)
						SET_BIT(MC_serverBD.iServerBitSet,SSBOOL_TEAM0_FINISHED + iTeamLoop)	
						SET_BIT(MC_serverBD.iServerBitSet,SSBOOL_TEAM0_FAILED + iTeamLoop)
						MC_serverBD.eCurrentTeamFail[iTeamLoop] = mFail_GAPS_IN_TEAMS
						PRINTLN("[InitPosition] - ASSIGN_PLAYER_TEAM - FAILING all teams and setting them to FINISHED - THis is because of gaps in the teams.") 
					ENDIF
				ENDFOR
			ENDIF
		ENDIF
	ELSE
		iTeam = g_FMMC_STRUCT.iTestMyTeam
		PRINTLN("[InitPosition] - ASSIGN_PLAYER_TEAM - IS_FAKE_MULTIPLAYER_MODE_SET() = TRUE, setting MC_playerBD[iLocalPart].iteam = g_FMMC_STRUCT.iTestMyTeam")
	ENDIF
	
	PRINTLN("[InitPosition] - ASSIGN_PLAYER_TEAM - PLAYER TEAM: ",MC_playerBD[iLocalPart].iteam)
	
	IF iTeam < 0
		SCRIPT_ASSERT("TEAM IS COMING THROUGH AS LESS THAN 0, ADD A BUG FOR ROBERT WRIGHT")
		iTeam = 0
	ENDIF
	
	IF iTeam >=FMMC_MAX_TEAMS
		SCRIPT_ASSERT("TEAM IS COMING THROUGH MORE THAN FMMC_MAX_TEAMS, ADD A BUG FOR ROBERT WRIGHT")
		iTeam = 3
	ENDIF
	
	CHANGE_PLAYER_TEAM(iTeam, FALSE)
	
ENDPROC

PROC SET_START_POSITION_WARPING_VEHICLE_VISIBLE()
	IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)		
		PRINTLN("[InitPosition] - SET_START_POSITION_WARPING_VEHICLE_VISIBLE - In a vehicle.")
				
		VEHICLE_INDEX veh = GET_VEHICLE_PED_IS_IN(localPlayerPed)
		
		IF DOES_ENTITY_EXIST(veh)
		AND NETWORK_HAS_CONTROL_OF_ENTITY(veh)
			PRINTLN("[InitPosition] - SET_START_POSITION_WARPING_VEHICLE_VISIBLE - Setting Visible")
			SET_ENTITY_VISIBLE(veh, TRUE)
		ELSE
			PRINTLN("[InitPosition] - SET_START_POSITION_WARPING_VEHICLE_VISIBLE - We do not have net control.")
		ENDIF
	ENDIF		
ENDPROC

FUNC BOOL WARP_START_POSITION_ON_VEHICLE_POST_ENTRY(BOOL bQuickWarp)
	
	IF IS_BIT_SET(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_POSITION_USES_VEH_POSITION)
		PRINTLN("[InitPosition] - WARP_START_POSITION_ON_VEHICLE_POST_ENTRY - Using Vehicle Position, returning TRUE.")
		RETURN TRUE
	ENDIF
	
	IF NOT IS_VECTOR_ZERO(sSetUpStartPosition.vVehiclePositionOverride)
				
		IF DOES_ENTITY_EXIST(sSetUpStartPosition.vehStartingVehicle)
		AND IS_VEHICLE_DRIVEABLE(sSetUpStartPosition.vehStartingVehicle)		
			IF NETWORK_HAS_CONTROL_OF_ENTITY(sSetUpStartPosition.vehStartingVehicle)
				VECTOR vPosition
				FLOAT fHeading 
				
				IF IS_BIT_SET(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_SEPARATE_VEH_POS_IS_RELATIVE)
					vPosition = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(sSetUpStartPosition.vStartPosition, sSetUpStartPosition.fStartHeading, sSetUpStartPosition.vVehiclePositionOverride)
					fHeading = sSetUpStartPosition.fVehicleHeadingOverride + sSetUpStartPosition.fStartHeading
				ELSE
					vPosition = sSetUpStartPosition.vVehiclePositionOverride
					fHeading = sSetUpStartPosition.fVehicleHeadingOverride
				ENDIF
				
				SET_ENTITY_COORDS(sSetUpStartPosition.vehStartingVehicle, vPosition)
				SET_ENTITY_HEADING(sSetUpStartPosition.vehStartingVehicle, fHeading)
				SET_ENTITY_VISIBLE(sSetUpStartPosition.vehStartingVehicle, TRUE)
				SET_VEHICLE_ON_GROUND_PROPERLY(sSetUpStartPosition.vehStartingVehicle)
				FREEZE_ENTITY_POSITION(sSetUpStartPosition.vehStartingVehicle, FALSE)
				IF NOT IS_VEHICLE_EMPTY(sSetUpStartPosition.vehStartingVehicle)
					PRINTLN("[InitPosition] - WARP_START_POSITION_ON_VEHICLE_POST_ENTRY - Vehicle not empty, activating physics.")
					ACTIVATE_PHYSICS(sSetUpStartPosition.vehStartingVehicle)
				ELSE
					PRINTLN("[InitPosition] - WARP_START_POSITION_ON_VEHICLE_POST_ENTRY - Vehicle empty, Freezing waiting collision.")
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(sSetUpStartPosition.vehStartingVehicle, TRUE)
				ENDIF
				
				PRINTLN("[InitPosition] - WARP_START_POSITION_ON_VEHICLE_POST_ENTRY - Setting vehicle position to: ", vPosition, " and heading to: ", fHeading)
			ELSE
				PRINTLN("[InitPosition] - WARP_START_POSITION_ON_VEHICLE_POST_ENTRY - We do not have control of our vehStartingVehicle. Requesting Control")
				NETWORK_REQUEST_CONTROL_OF_ENTITY(sSetUpStartPosition.vehStartingVehicle)
				RETURN FALSE
			ENDIF
		ELSE
			PRINTLN("[InitPosition] - WARP_START_POSITION_ON_VEHICLE_POST_ENTRY - sSetUpStartPosition.fVehicleHeadingOverride is set, but no vehicle exists to apply it to.")
		ENDIF		
		
	ENDIF
	
	IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		
		PRINTLN("[InitPosition] - WARP_START_POSITION_ON_VEHICLE_POST_ENTRY - In a vehicle.")
				
		VEHICLE_INDEX veh = GET_VEHICLE_PED_IS_IN(localPlayerPed)
		
		IF DOES_ENTITY_EXIST(veh)
		AND NETWORK_HAS_CONTROL_OF_ENTITY(veh)
			PRINTLN("[InitPosition] - WARP_START_POSITION_ON_VEHICLE_POST_ENTRY - Post Warping position being set on the vehicle. vStartPosition: ", sSetUpStartPosition.vStartPosition, " sSetUpStartPosition.fStartHeading: ", sSetUpStartPosition.fStartHeading)
			
			IF NET_WARP_TO_COORD(sSetUpStartPosition.vStartPosition, sSetUpStartPosition.fStartHeading, TRUE, FALSE, FALSE, FALSE, TRUE, bQuickWarp, FALSE)
				PRINTLN("[InitPosition] - WARP_START_POSITION_ON_VEHICLE_POST_ENTRY - Warp Finished")		
				
				IF IS_THIS_A_QUICK_RESTART_JOB()
					PRINTLN("[InitPosition] - WARP_START_POSITION_ON_VEHICLE_POST_ENTRY - Freezing Veh")
					FREEZE_ENTITY_POSITION(Veh, TRUE)
				ENDIF
				
				RETURN TRUE
			ENDIF
		ELSE
			PRINTLN("[InitPosition] - WARP_START_POSITION_ON_VEHICLE_POST_ENTRY - We do not have net control.")
		ENDIF
		
		RETURN FALSE
		
	ENDIF
			
	PRINTLN("[InitPosition] - WARP_START_POSITION_ON_VEHICLE_POST_ENTRY - Not in a Vehicle, returning TRUE.")
	RETURN TRUE
ENDFUNC

FUNC VEHICLE_INDEX GET_ASSOCIATED_SPAWN_POINT_START_VEHICLE()
	NETWORK_INDEX niVeh
	IF sSetUpStartPosition.iVehicleIndexToSpawnInto > -1 
		niVeh = MC_serverBD_1.sFMMC_SBD.niVehicle[sSetUpStartPosition.iVehicleIndexToSpawnInto]	
		IF NETWORK_DOES_NETWORK_ID_EXIST(niVeh)
			RETURN NET_TO_VEH(niVeh)
		ENDIF
	ENDIF
	
	RETURN NULL
ENDFUNC

FUNC VEHICLE_INDEX GET_SPAWN_POINT_START_VEHICLE_TO_CLONE_TO()	
	VEHICLE_INDEX vehIndex
	INT i = 0 
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles-1
		
		IF NOT IS_BIT_SET(iVehicleLocalPartNeedsToCloneBS, i)
			RELOOP
		ENDIF
		
		PRINTLN("[InitPosition] - START_POSITION_SET - GET_SPAWN_POINT_START_VEHICLE_TO_CLONE_TO - We are flagged to clone mods onto iVeh: ", i)
		
		IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[i])
			PRINTLN("[InitPosition] - START_POSITION_SET - GET_SPAWN_POINT_START_VEHICLE_TO_CLONE_TO - iVeh: ", i, " does not exist - Soemthing has likely gone wrong!")
			RELOOP
		ENDIF
		
		PRINTLN("[InitPosition] - START_POSITION_SET - GET_SPAWN_POINT_START_VEHICLE_TO_CLONE_TO - iVeh: ", i, " exists are is our target.")
				
		vehIndex = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i])
		BREAKLOOP
	ENDFOR	
	
	RETURN vehIndex
ENDFUNC

PROC SET_SPECIAL_MODS_TO_VEHICLE_BEFORE_START()
	
	VEHICLE_INDEX vehIndex
	IF NOT IS_VEHICLE_DRIVEABLE(vehIndex)
		vehIndex = GET_SPAWN_POINT_START_VEHICLE_TO_CLONE_TO()
	ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(vehIndex)
		IF NETWORK_HAS_CONTROL_OF_ENTITY(vehIndex)
			PRINTLN("[InitPosition] - START_POSITION_SET - SET_SPECIAL_MODS_TO_VEHICLE_BEFORE_START - Cloning details from lobby veh onto placed veh.")
			MP_SAVE_VEHICLE_APPLY_SAVED_DETAILS_TO_VEHICLE(vehIndex, g_iMyRaceModelChoice)
		ELSE
			PRINTLN("[InitPosition] - START_POSITION_SET - SET_SPECIAL_MODS_TO_VEHICLE_BEFORE_START - Could not apply vehicle settings, we did not have network control.")
		ENDIF
	ELSE		
		PRINTLN("[InitPosition] - START_POSITION_SET - SET_SPECIAL_MODS_TO_VEHICLE_BEFORE_START - NOT DRIVABLE")				
	ENDIF
ENDPROC 

FUNC BOOL IS_WARPING_INTO_PLACED_VEH()

	SWITCH(eRestartVehStage)
		CASE eRESTARTVEH_INIT
			IF (IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].iTeamBitset2, ciBS2_SPAWN_IN_PLACED_VEH_CHECKPOINT_0)
				AND FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(0))
			OR (IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].iTeamBitset2, ciBS2_SPAWN_IN_PLACED_VEH_CHECKPOINT_1)
				AND FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(1))
			OR (IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iTeamBitset3, ciBS3_Team_SpawnInPlacedVeh_Checkpoint_2)
				AND FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(2))
			OR (IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iTeamBitset3, ciBS3_Team_SpawnInPlacedVeh_Checkpoint_3)
				AND FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(3))
				IF GET_PLACED_VEH_FOR_QUICK_RESTART_SPAWN()
					PRINTLN("[InitPosition] - IS_WARPING_INTO_PLACED_VEH - going to spawn in veh: ", iVehToSpawnIn)
					SET_RESTART_VEH_STAGE(eRESTARTVEH_ENTER_VEH)
				ELSE
					SET_RESTART_VEH_STAGE(eRESTARTVEH_FINISHED)
					PRINTLN("[InitPosition] - IS_WARPING_INTO_PLACED_VEH - no valid car")
				ENDIF
			ELSE
				SET_RESTART_VEH_STAGE(eRESTARTVEH_FINISHED)
				PRINTLN("[InitPosition] - IS_WARPING_INTO_PLACED_VEH - not set to restart in a placed veh")
			ENDIF
		BREAK
		
		CASE eRESTARTVEH_ENTER_VEH
			IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iVehToSpawnIn])
				VEHICLE_INDEX viVehToSpawnIn
				viVehToSpawnIn = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVehToSpawnIn])
				IF NOT IS_ENTITY_DEAD(viVehToSpawnIn)
				AND IS_VEHICLE_DRIVEABLE(viVehToSpawnIn)
					IF IS_VEHICLE_SEAT_FREE(viVehToSpawnIn, VS_DRIVER, TRUE)
						PRINTLN("[InitPosition] - IS_WARPING_INTO_PLACED_VEH - WARPING into veh: ", iVehToSpawnIn)
						WarpPlayerIntoCar(LocalPlayerPed, viVehToSpawnIn, VS_DRIVER)
						IF NOT HAS_NET_TIMER_STARTED(tdVehWarpBailTime)
							REINIT_NET_TIMER(tdVehWarpBailTime)
						ELIF HAS_NET_TIMER_EXPIRED(tdVehWarpBailTime, ciVEH_WARP_TIMEOUT)
							SET_RESTART_VEH_STAGE(eRESTARTVEH_FINISHED)
							PRINTLN("[InitPosition] - IS_WARPING_INTO_PLACED_VEH - Bailing, unable to get into veh: ", iVehToSpawnIn)
						ENDIF
					ELIF IS_PED_IN_VEHICLE(LocalPlayerPed, viVehToSpawnIn, FALSE)
						FREEZE_ENTITY_POSITION(viVehToSpawnIn, FALSE)
						
						SET_RESTART_VEH_STAGE(eRESTARTVEH_FINISHED)
						PRINTLN("[InitPosition] - IS_WARPING_INTO_PLACED_VEH - We are in veh: ",iVehToSpawnIn)
					ELSE
						SET_RESTART_VEH_STAGE(eRESTARTVEH_FINISHED)
						PRINTLN("[InitPosition] - IS_WARPING_INTO_PLACED_VEH - Someone is already in veh: ",iVehToSpawnIn)
					ENDIF
				ELSE
					SET_RESTART_VEH_STAGE(eRESTARTVEH_FINISHED)
					PRINTLN("[InitPosition] - IS_WARPING_INTO_PLACED_VEH - veh ",iVehToSpawnIn," is undrivable")
				ENDIF
			ELSE
				SET_RESTART_VEH_STAGE(eRESTARTVEH_FINISHED)
				PRINTLN("[InitPosition] - IS_WARPING_INTO_PLACED_VEH - net ID for veh ",iVehToSpawnIn," does not exist")
			ENDIF
		BREAK
		
		CASE eRESTARTVEH_FINISHED
			RETURN FALSE
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

PROC SET_UP_PERSONAL_VEHICLE_START_SPAWN_DATA()
	VECTOR vPvStartPosition = GET_CUSTOM_PERSONAL_VEHICLE_START_POSITION(sSetUpStartPosition.iPlayerTeam, sSetUpStartPosition.iPlayerTeamSlot)		
	IF IS_VECTOR_ZERO(vPvStartPosition)
		SET_BIT(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_SPAWN_PERSONAL_VEHICLE_NEAR_PLAYER)
		PRINTLN("[InitPosition] - SET_UP_START_POSITION_DATA - BS_INIT_START_SPAWN_PERSONAL_VEHICLE_NEAR_PLAYER")
	ELSE
		sSetUpStartPosition.vPVStartPosition = vPvStartPosition
		sSetUpStartPosition.fPVStartHeading = GET_CUSTOM_PERSONAL_VEHICLE_START_HEADING(sSetUpStartPosition.iPlayerTeam, sSetUpStartPosition.iPlayerTeamSlot)
		PRINTLN("[InitPosition] - SET_UP_START_POSITION_DATA - vPVStartPosition: ", sSetUpStartPosition.vPVStartPosition)
		PRINTLN("[InitPosition] - SET_UP_START_POSITION_DATA - vPVStartHeaading: ", sSetUpStartPosition.fPVStartHeading)
	ENDIF
ENDPROC

FUNC INT GET_PARTICIPANT_TEAM_PLAYER_SLOT_FOR_SPAWNING(INT iTeam)
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyNine, ciOptionsBS29_SpawningLobbyLeaderAlwaysInFirstSlot)		
		INT i
		INT iParts[NUM_NETWORK_PLAYERS]	
		FILL_ACTIVE_PARTICIPANT_INDEX_FROM_NUMBER_IN_TEAM(iParts, iTeam, TRUE)		
		FOR i = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
			IF iParts[i] = iLocalPart
				RETURN i
			ENDIF
		ENDFOR
	ENDIF
	
	RETURN GET_PARTICIPANT_NUMBER_IN_TEAM()
	
ENDFUNC

/// PURPOSE:
///    We can cache data for use elsewhere in script here, such as the kind of startup we have, transitionvar getting etc.
PROC SET_UP_START_POSITION_DATA()
	
	sSetUpStartPosition.iPlayerTeam 					= MC_playerBD[iLocalPart].iteam
	sSetUpStartPosition.iStartRule						= GET_MC_TEAM_STARTING_RULE(sSetUpStartPosition.iPlayerTeam)  //MC_serverBD_4.iCurrentHighestPriority[sSetUpStartPosition.iPlayerTeam]
	
	IF sSetUpStartPosition.iPlayerTeamSlot = -1
		sSetUpStartPosition.iPlayerTeamSlot 			= GET_PARTICIPANT_TEAM_PLAYER_SLOT_FOR_SPAWNING(sSetUpStartPosition.iPlayerTeam)
	ENDIF

	PRINTLN("[InitPosition] - SET_UP_START_POSITION_DATA - being called.")		
	PRINTLN("[InitPosition] - SET_UP_START_POSITION_DATA - iPlayerTeamSlot: ", sSetUpStartPosition.iPlayerTeamSlot, " iPlayerTeam: ", sSetUpStartPosition.iPlayerTeam, " iStartRule: ", sSetUpStartPosition.iStartRule)
		
	//Starting Conditions ------------------------
	//If a starnd mission is being initilised then don't move the players
	IF sSetUpStartPosition.iStartRule < FMMC_MAX_RULES
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitsetSeventeen, ciENABLE_USE_START_POS_ON_HEIST_STRAND)
		OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[sSetUpStartPosition.iPlayerTeam].iRuleBitsetFourteen[sSetUpStartPosition.iStartRule], ciBS_RULE14_IGNORE_START_POSITION_ON_THIS_RULE)
			IF IS_A_STRAND_MISSION_BEING_INITIALISED()
				SET_BIT( ilocalboolcheck, LBOOL_MISSION_READY_TO_WARP_START_POSITION )
				SET_BIT( ilocalboolcheck2, LBOOL2_MISSION_START_STAY_STILL)
				PRINTLN("[InitPosition] - SET_UP_START_POSITION_DATA - IS_A_STRAND_MISSION_BEING_INITIALISED, Setting: LBOOL2_MISSION_START_STAY_STILL.")
				EXIT
			ENDIF
		ENDIF
	ENDIF
	
	IF SHOULD_SPAWN_IN_NON_PLACED_VEHICLE_AT_MISSION_START(sSetUpStartPosition.iPlayerTeam)
		SET_BIT(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_SPAWN_IN_TEAM_VEHICLE)
		PRINTLN("[InitPosition] - SET_UP_START_POSITION_DATA - Setting: BS_INIT_START_SPAWN_IN_TEAM_VEHICLE.")
	ENDIF
	
	IF SHOULD_PLAYER_ENTER_COVER_AFTER_START_POSITION()
		SET_BIT(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_POSITION_ENTER_COVER)
		PRINTLN("[InitPosition] - SET_UP_START_POSITION_DATA - Setting: BS_INIT_START_POSITION_ENTER_COVER.")
	ENDIF
	
	IF IS_PERSONAL_VEHICLE_ALLOWED_TO_SPAWN_AT_MISSION_START()
		PRINTLN("[InitPosition] - SET_UP_START_POSITION_DATA - Spawn the Personal Vehicle At Start - Set up via globals.")
		SET_BIT(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_SPAWN_PERSONAL_VEHICLE_AT_START)
	ELSE
		PRINTLN("[InitPosition] - SET_UP_START_POSITION_DATA - Globals are set up to NOT enable personal vehicle spawning at start.")
	ENDIF
	
	IF IS_PLAYER_SELECTING_CUSTOM_VEHICLE(LocalPlayer)
		PRINTLN("[InitPosition] - SET_UP_START_POSITION_DATA - Player has selected a Custom Vehicle / Personal Vehicle from the Lobby.")
	ELSE
		PRINTLN("[InitPosition] - SET_UP_START_POSITION_DATA - No Custom Vehicle / Personal Vehicle has been selected from the Lobby.")
	ENDIF
	
	//Start Point and Heading Type ------------------------	
	IF HAS_FIRST_STRAND_MISSION_BEEN_PASSED()
	AND (IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_LocationTracking) OR IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_ObjectTracking))
	AND NOT FMMC_CHECKPOINTS_IS_ANY_RETRY_START_POINT_SET()
		INT iSlot = GET_CONTINUITY_START_POINT_INDEX(sSetUpStartPosition.iPlayerTeam)
		IF iSlot > -1
			VECTOR vStart = g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][iSlot].vPos
			IF NOT IS_VECTOR_ZERO(vStart)						
				SET_BIT(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_POSITION_USING_CONTINUITY)
				SET_BIT(ilocalboolcheck, LBOOL_MISSION_READY_TO_WARP_START_POSITION)
				
				PRINTLN("[InitPosition] - SET_UP_START_POSITION_DATA - Setting: BS_INIT_START_POSITION_USING_CONTINUITY.")
			ENDIF
		ELSE
			PRINTLN("[InitPosition] - SET_UP_START_POSITION_DATA - Setting Nothing. Should have been continuity.")
		ENDIF
		
	ELIF SHOULD_USE_CUSTOM_START_POINT_CHECKPOINTS()	
	AND MC_SHOULD_WE_START_FROM_CHECKPOINT()
		SET_BIT(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_POSITION_USING_CUSTOM_START_POINT_CHECKPOINTS)
		SET_BIT(ilocalboolcheck, LBOOL_MISSION_READY_TO_WARP_START_POSITION)
		
		PRINTLN("[InitPosition] - SET_UP_START_POSITION_DATA - Setting: BS_INIT_START_POSITION_USING_CUSTOM_START_POINT_CHECKPOINTS.")
			
	ELIF MC_SHOULD_RESPAWN_AT_CHECKPOINT(sSetUpStartPosition.iPlayerTeam)
		SET_BIT(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_POSITION_USING_CHECKPOINT)
		SET_BIT(ilocalboolcheck, LBOOL_MISSION_READY_TO_WARP_START_POSITION)
		
		PRINTLN("[InitPosition] - SET_UP_START_POSITION_DATA - Setting: BS_INIT_START_POSITION_USING_CHECKPOINT.")
	
	ELIF SHOULD_USE_CUSTOM_START_SPAWN_POINTS()	
		SET_BIT(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_POSITION_USING_CUSTOM_START_POINTS)
		SET_BIT(ilocalboolcheck, LBOOL_MISSION_READY_TO_WARP_START_POSITION)
		
		PRINTLN("[InitPosition] - SET_UP_START_POSITION_DATA - Setting: BS_INIT_START_POSITION_USING_CUSTOM_START_POINTS.")
		
	// Fallback	---------------------------
	ELIF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sFMMCEndConditions[sSetUpStartPosition.iPlayerTeam].vStartPos)
		SET_BIT(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_POSITION_USING_SINGLE_START_POS)
		SET_BIT(ilocalboolcheck, LBOOL_MISSION_READY_TO_WARP_START_POSITION)	
	
		PRINTLN("[InitPosition] - SET_UP_START_POSITION_DATA - Setting: BS_INIT_START_POSITION_USING_SINGLE_START_POS.")
	
	ELIF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][sSetUpStartPosition.iPlayerTeamSlot].vPos)
		SET_BIT(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_POSITION_USING_TEAM_POINTS_SIMPLE)
		SET_BIT(ilocalboolcheck, LBOOL_MISSION_READY_TO_WARP_START_POSITION)
		
		PRINTLN("[InitPosition] - SET_UP_START_POSITION_DATA - Setting: BS_INIT_START_POSITION_USING_TEAM_POINTS_SIMPLE.")
		
	ENDIF
ENDPROC

FUNC INT GET_VALID_START_PERSONAL_VEHICLE_WITH_CLASS_RESTRICTIONS(INT iPVSlot)
	
	PRINTLN("[InitPosition] - SET_UP_START_POSITION_DATA - GET_VALID_START_PERSONAL_VEHICLE_WITH_CLASS_RESTRICTIONS - First Call, iPVSlot: ", iPVSlot, ", Name: ", FMMC_GET_MODEL_NAME_FOR_DEBUG(g_MpSavedVehicles[iPVSlot].vehicleSetupMP.VehicleSetup.eModel))
		
	IF NOT IS_PERSONAL_VEHICLE_MODEL_ALLOWED_IN_GENERIC_JOB_BY_ANY_LIBRARY(g_MpSavedVehicles[iPVSlot].vehicleSetupMP.VehicleSetup.eModel)
		
		PRINTLN("[InitPosition] - SET_UP_START_POSITION_DATA - GET_VALID_START_PERSONAL_VEHICLE_WITH_CLASS_RESTRICTIONS - iPVSlot: ", iPVSlot, ", Name: ", FMMC_GET_MODEL_NAME_FOR_DEBUG(g_MpSavedVehicles[iPVSlot].vehicleSetupMP.VehicleSetup.eModel), " is a restricted vehicle. LOOPING to find a valid one.")
				
		iPVSlot = -1
		
		INT i = 0
		FOR i = 0 TO MAX_MP_SAVED_VEHICLES-1
			PRINTLN("[InitPosition] - SET_UP_START_POSITION_DATA - GET_VALID_START_PERSONAL_VEHICLE_WITH_CLASS_RESTRICTIONS - Checking - i: ", i, " ")
			IF NOT IS_PERSONAL_VEHICLE_MODEL_ALLOWED_IN_GENERIC_JOB_BY_ANY_LIBRARY(g_MpSavedVehicles[i].vehicleSetupMP.VehicleSetup.eModel)
				PRINTLN("[InitPosition] - SET_UP_START_POSITION_DATA - GET_VALID_START_PERSONAL_VEHICLE_WITH_CLASS_RESTRICTIONS - i: ", i, ", Name: ", FMMC_GET_MODEL_NAME_FOR_DEBUG(g_MpSavedVehicles[i].vehicleSetupMP.VehicleSetup.eModel), " is a restricted class - bClassRestricted")
				RELOOP
			ELSE
				iPVSlot = i
				PRINTLN("[InitPosition] - SET_UP_START_POSITION_DATA - GET_VALID_START_PERSONAL_VEHICLE_WITH_CLASS_RESTRICTIONS - i: ", i, ", Name: ", FMMC_GET_MODEL_NAME_FOR_DEBUG(g_MpSavedVehicles[i].vehicleSetupMP.VehicleSetup.eModel), " is a SAFE TO USE")
				BREAKLOOP
			ENDIF
		ENDFOR
		
	ELSE
		PRINTLN("[InitPosition] - SET_UP_START_POSITION_DATA - GET_VALID_START_PERSONAL_VEHICLE_WITH_CLASS_RESTRICTIONS - iPVSlot: ", iPVSlot, ", Name: ", FMMC_GET_MODEL_NAME_FOR_DEBUG(g_MpSavedVehicles[iPVSlot].vehicleSetupMP.VehicleSetup.eModel), " is a SAFE TO USE")
	ENDIF
	
	PRINTLN("[InitPosition] - SET_UP_START_POSITION_DATA - GET_VALID_START_PERSONAL_VEHICLE_WITH_CLASS_RESTRICTIONS - Returning iPVSlot: ", iPVSlot)
		
	RETURN iPVSlot
ENDFUNC

PROC PROCESS_START_POSITION_STATE_POST_WARP_PERSONAL_VEHICLE_REQUEST()
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_DISABLE_PV_NODES_OUTSIDE_PROPERTY)				
		DISABLE_CUSTOM_VEHICLE_NODES_FOR_ALL_PROPERTIES(TRUE)
	ENDIF
	
	// Current personal vehicle
	INT iPVSlot = CURRENT_SAVED_VEHICLE_SLOT()
	
	PRINTLN("[InitPosition] - SET_UP_START_POSITION_DATA - Defaulting to CURRENT_SAVED_VEHICLE_SLOT iPVSlot: ", iPVSlot)
	
	// Lobby already filtered vehicles so it has highest priority.
	IF g_mnMyRaceModel != DUMMY_MODEL_FOR_SCRIPT
		iPVSlot = g_iMyRaceModelChoice
		PRINTLN("[InitPosition] - SET_UP_START_POSITION_DATA - Using Lobby selection instead, iPVSlot: ", iPVSlot)
	ELSE
		// Make sure current personal vehicle slot is valid.
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyEight, ciOptionsBS28_OnlyAllowLobbyClassRestrictedVehiclesFromPhone)		
			PRINTLN("[InitPosition] - SET_UP_START_POSITION_DATA - Checking personal vehicle restrictions ciOptionsBS28_OnlyAllowLobbyClassRestrictedVehiclesFromPhone")
			IF iPVSlot != -1
				iPVSlot = GET_VALID_START_PERSONAL_VEHICLE_WITH_CLASS_RESTRICTIONS(iPVSlot)
			ENDIF
		ENDIF
	ENDIF
	
	// An option to grab a vehicle that's NOT destroyed would be beneficial here.
	// ... url:bugstar:7419802
	// IF CAN_PERSONAL_VEHICLE_BE_SPAWNED() AND CREATOR OPTION 
	//		GET_VALID_START_PERSONAL_VEHICLE_WITH_CLASS_RESTRICTIONS
	// ENDIF
	
	CLEAR_CUSTOM_VEHICLE_NODES()
	
	IF NOT IS_VECTOR_ZERO(sSetUpStartPosition.vPVStartPosition)
		ADD_CUSTOM_VEHICLE_NODE(sSetUpStartPosition.vPVStartPosition, sSetUpStartPosition.fPVStartHeading)
		PRINTLN("[InitPosition] - SET_UP_START_POSITION_DATA - iPVSlot: ", iPVSlot, " Personal vehicle being created at sSetUpStartPosition.vPVStartPosition: ", sSetUpStartPosition.vPVStartPosition, " sSetUpStartPosition.fPVStartHeading: ", sSetUpStartPosition.fPVStartHeading)						
				
	ELIF IS_BIT_SET(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_SPAWN_PERSONAL_VEHICLE_NEAR_PLAYER)
		PRINTLN("[InitPosition] - SET_UP_START_POSITION_DATA - iPVSlot: ", iPVSlot, " Personal vehicle being created at nearest existing road node")
		
	ENDIF
	
	IF iPVSlot != -1
		iPersonalVehicleSlotCache = iPVSlot
		IF NOT IS_VECTOR_ZERO(sSetUpStartPosition.vPVStartPosition)
			CLEAR_REQUEST_TO_WARP_PERSONAL_VEHICLE_NEAR()
			REQUEST_PERSONAL_VEHICLE_SPAWN_AT_COORDS(sSetUpStartPosition.vPVStartPosition, sSetUpStartPosition.fPVStartHeading, iPVSlot, TRUE)
			MPGlobals.VehicleData.bSwitchWarpProcessed = TRUE
		ELSE
			REQUEST_PERSONAL_VEHICLE_SPAWN_NEAR(iPVSlot, TRUE)
			MPGlobals.VehicleData.bSwitchWarpProcessed = TRUE
		ENDIF		
	ENDIF
ENDPROC

PROC START_POSITION_SET_SPAWN_PERSONAL_VEHICLE_AT_START()

	IF NOT IS_BIT_SET(iLocalBoolCheck34, LBOOL34_INITIAL_REQUESTED_PERSONAL_VEHICLE)	
		PRINTLN("[InitPosition] - START_POSITION_SET_SPAWN_PLAYER_IN_PERSONAL_VEHICLE - Starting Personal Vehicle Request....")		
		DISABLE_PERSONAL_VEHICLE_CREATION_FOR_MISSION_STARTUP(FALSE)
		DISABLE_PERSONAL_VEHICLE_AMBIENT_CREATION_DURING_MISSION(FALSE)
		DISABLE_PERSONAL_VEHICLE_CREATION_FOR_MISSION(FALSE)
		SET_UP_PERSONAL_VEHICLE_START_SPAWN_DATA()				
		PROCESS_START_POSITION_STATE_POST_WARP_PERSONAL_VEHICLE_REQUEST()
		SET_BIT(iLocalBoolCheck34, LBOOL34_INITIAL_REQUESTED_PERSONAL_VEHICLE)
	ENDIF
	
ENDPROC

FUNC BOOL START_POSITION_SET_SPAWN_PLAYER_IN_PERSONAL_VEHICLE(VECTOR vStartPos, FLOAT fStartHeading)
	UNUSED_PARAMETER(vStartPos)
	UNUSED_PARAMETER(fStartHeading)
	
	IF NOT bLocalPlayerOK	
		RETURN FALSE
	ENDIF

	VEHICLE_INDEX veh
	
	START_POSITION_SET_SPAWN_PERSONAL_VEHICLE_AT_START()
	
	IF NOT HAS_NET_TIMER_STARTED(sSetUpStartPosition.tdPersonalVehicleFailSafeTimer)
		START_NET_TIMER(sSetUpStartPosition.tdPersonalVehicleFailSafeTimer)
		PRINTLN("[InitPosition] - START_POSITION_SET_SPAWN_PLAYER_IN_PERSONAL_VEHICLE - Starting Personal Vehicle Creation and Entry Safety Timer.")
	ENDIF
	
	veh = PERSONAL_VEHICLE_ID()

	IF NOT HAS_NET_TIMER_EXPIRED_READ_ONLY(sSetUpStartPosition.tdPersonalVehicleFailSafeTimer, ciPERSONAL_VEHICLE_FAIL_SAFE_TIME)
		
		IF NOT DOES_ENTITY_EXIST(veh)		
			PRINTLN("[InitPosition] - START_POSITION_SET_SPAWN_PLAYER_IN_PERSONAL_VEHICLE - Waiting for Personal Vehicle to Spawn.")
			RETURN FALSE				
		ENDIF
		
	ELSE
		#IF IS_DEBUG_BUILD
		IF NOT DOES_ENTITY_EXIST(veh)
			PRINTLN("[InitPosition] - START_POSITION_SET_SPAWN_PLAYER_IN_PERSONAL_VEHICLE - Safety Timer has expired!!! Was waiting for Vehicle to be created!!! g_iFailReason_SavedVehicleAvailableForCreation: ", g_iFailReason_SavedVehicleAvailableForCreation)							
		ELSE
			PRINTLN("[InitPosition] - START_POSITION_SET_SPAWN_PLAYER_IN_PERSONAL_VEHICLE - Safety Timer has expired!!! Was waiting for Player to be in the Vehicle!!! g_iFailReason_SavedVehicleAvailableForCreation: ", g_iFailReason_SavedVehicleAvailableForCreation)
		ENDIF
		#ENDIF
		
		#IF IS_DEBUG_BUILD
		IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
			ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_START_PV_DID_NOT_SPAWN, ENTITY_RUNTIME_ERROR_TYPE_WARNING, "Personal Vehicle not created at start (timed out) Fail Reason #", g_iFailReason_SavedVehicleAvailableForCreation)
		ELSE
			ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_START_PV_DID_NOT_SPAWN, ENTITY_RUNTIME_ERROR_TYPE_NOTIFICATION, "Personal Vehicle cannot be created at start (freemode not running)")		
		ENDIF
		#ENDIF
		
		RETURN TRUE
	ENDIF
	
	IF DOES_ENTITY_EXIST(veh)
		
		PRINTLN("[InitPosition] - START_POSITION_SET_SPAWN_PLAYER_IN_PERSONAL_VEHICLE - Calling Function with vStartPos: ", vStartPos, " fStartHeading: ", fStartHeading)
				
		VEHICLE_SEAT vsToEnter = VS_DRIVER
			
		IF GET_SCRIPT_TASK_STATUS(localPlayerPed, SCRIPT_TASK_ENTER_VEHICLE) != WAITING_TO_START_TASK
		AND GET_SCRIPT_TASK_STATUS(localPlayerPed, SCRIPT_TASK_ENTER_VEHICLE) != PERFORMING_TASK
		AND NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed, TRUE)
			PRINTLN("[InitPosition] - START_POSITION_SET_SPAWN_PLAYER_IN_PERSONAL_VEHICLE - Not performing task currently. ")
			
			IF IS_VEHICLE_DRIVEABLE(veh)							
				// Backup - Will prevent the player from not being placed in the vehicle and a long startup, but will not fix the issue that they were not put into the seat they were assigned to be in!
				IF NOT IS_VEHICLE_SEAT_FREE(veh, vsToEnter, TRUE)
					PRINTLN("[InitPosition] - START_POSITION_SET_SPAWN_PLAYER_IN_PERSONAL_VEHICLE Vehicle Seat is not free ENUM_TO_INT(vsToEnter): ", ENUM_TO_INT(vsToEnter), " Setting to use passenger seat")
					vsToEnter = VS_ANY_PASSENGER
				ENDIF
		
				SET_PED_VEHICLE_FORCED_SEAT_USAGE(LocalPlayerPed, veh, 0, 0, vsToEnter)
							
				IF NOT IS_THIS_A_QUICK_RESTART_JOB()
					FREEZE_ENTITY_POSITION( LocalPlayerPed, FALSE )
				ELSE
					FREEZE_ENTITY_POSITION( LocalPlayerPed, TRUE )
				ENDIF
				IF NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed, TRUE)
					SET_ENTITY_COLLISION( LocalPlayerPed, TRUE )
				ENDIF			
				
				TASK_ENTER_VEHICLE(LocalPlayerPed, veh, 1, vsToEnter, DEFAULT, ECF_RESUME_IF_INTERRUPTED | ECF_WARP_PED | ECF_WARP_IF_DOOR_IS_BLOCKED | ECF_BLOCK_SEAT_SHUFFLING | ECF_WARP_IF_SHUFFLE_LINK_IS_BLOCKED)
				
				#IF IS_DEBUG_BUILD
				VECTOR vVehPos = GET_ENTITY_COORDS(veh, FALSE)
				PRINTLN("[InitPosition] - START_POSITION_SET_SPAWN_PLAYER_IN_PERSONAL_VEHICLE Placing the local player into ENUM_TO_INT(vsToEnter): ", ENUM_TO_INT(vsToEnter), " Vehicle Position: ", vVehPos)						
				#ENDIF
			ELSE
				PRINTLN("[InitPosition] - START_POSITION_SET_SPAWN_PLAYER_IN_PERSONAL_VEHICLE iVehicleIndexToSpawnInto is valid, but the vehicle we're trying to move into is dead!" )
			ENDIF
		ELSE
			IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
				IF NETWORK_HAS_CONTROL_OF_ENTITY(veh)
					FREEZE_ENTITY_POSITION(veh, TRUE)
				ENDIF
				
				PRINTLN("[InitPosition] - START_POSITION_SET_SPAWN_PLAYER_IN_VEHICLE - vStartPos: ", vStartPos, " fStartHeading: ", fStartHeading)
				SET_BIT(iLocalBoolCheck8, LBOOL8_HAS_START_POSITION_WARPED)
				RETURN TRUE
			ELSE
				PRINTLN("[InitPosition] - START_POSITION_SET_SPAWN_PLAYER_IN_VEHICLE - Not yet in the vehicle, but we have the task.")
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_START_POSITION_SPAWN_USE_PUT_INTO_VEHICLE_NATIVE(INT iVehicleIndexToSpawnInto)
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVehicleIndexToSpawnInto].iVehBitsetNine, ciFMMC_VEHICLE9_IsDrivebySequenceVehicle)
		// This is a driveby vehicle and needs special attention to get the player into the special seats at the back
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL START_POSITION_SET_SPAWN_PLAYER_IN_PLACED_VEHICLE(INT iVehicleIndexToSpawnInto, INT iVehicleSeatToSpawnInto, VECTOR vStartPos, FLOAT fStartHeading)
	
	IF NOT bLocalPlayerOK	
		RETURN FALSE
	ENDIF
		
	PRINTLN("[InitPosition][Vehicle ", iVehicleIndexToSpawnInto, "] - START_POSITION_SET_SPAWN_PLAYER_IN_PLACED_VEHICLE - Calling Function with iVehicleIndexToSpawnInto: ", iVehicleIndexToSpawnInto, " iVehicleSeatToSpawnInto: ", iVehicleSeatToSpawnInto, " vStartPos: ", vStartPos, " fStartHeading: ", fStartHeading)
	
	SET_ARENA_SPAWN_FIX_COORDS(vStartPos, fStartHeading)

	VEHICLE_SEAT vsToEnter = VS_DRIVER
	
	IF iVehicleSeatToSpawnInto != -3
		vsToEnter = INT_TO_ENUM(VEHICLE_SEAT, iVehicleSeatToSpawnInto)
	ENDIF
	
	IF iVehicleIndexToSpawnInto != -1
		vsToEnter = GET_MISSION_CONTINUITY_VEHICLE_SEAT(iVehicleIndexToSpawnInto, vsToEnter)
	ENDIF
	
	NETWORK_INDEX niVeh	
	VEHICLE_INDEX veh
	
	IF iVehicleIndexToSpawnInto > -1 
		niVeh = MC_serverBD_1.sFMMC_SBD.niVehicle[iVehicleIndexToSpawnInto]
	
		IF NETWORK_DOES_NETWORK_ID_EXIST(niVeh)					
			veh = NET_TO_VEH(niVeh)
		ENDIF
		
	ENDIF
	
	IF NOT HAS_NET_TIMER_STARTED(sSetUpStartPosition.tdEnterVehicleFailSafeTimer)
		START_NET_TIMER(sSetUpStartPosition.tdEnterVehicleFailSafeTimer)
		PRINTLN("[InitPosition][Vehicle ", iVehicleIndexToSpawnInto, "] - START_POSITION_SET_SPAWN_PLAYER_IN_PLACED_VEHICLE - Starting Placed Vehicle Entry Safety Timer.")
	ENDIF
	
	IF NOT HAS_NET_TIMER_EXPIRED_READ_ONLY(sSetUpStartPosition.tdEnterVehicleFailSafeTimer, ciENTER_VEHICLE_FAIL_SAFE_TIME)
		
		IF NOT DOES_ENTITY_EXIST(veh)			
			PRINTLN("[InitPosition][Vehicle ", iVehicleIndexToSpawnInto, "] - START_POSITION_SET_SPAWN_PLAYER_IN_PLACED_VEHICLE - Waiting for Placed Vehicle to Spawn.")
			RETURN FALSE
		ENDIF
		
	ELSE
		#IF IS_DEBUG_BUILD
		IF NOT DOES_ENTITY_EXIST(veh)
			PRINTLN("[InitPosition][Vehicle ", iVehicleIndexToSpawnInto, "] - START_POSITION_SET_SPAWN_PLAYER_IN_PLACED_VEHICLE - Safety Timer has expired!!! Was waiting for Vehicle to be created!!!")							
		ELSE
			PRINTLN("[InitPosition][Vehicle ", iVehicleIndexToSpawnInto, "] - START_POSITION_SET_SPAWN_PLAYER_IN_PLACED_VEHICLE - Safety Timer has expired!!! Was waiting for Player to be in the Vehicle!!!")			
		ENDIF
		#ENDIF
			
		#IF IS_DEBUG_BUILD
		ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_PLACED_VEH_DID_NOT_SPAWN, ENTITY_RUNTIME_ERROR_TYPE_WARNING_VEH, "Placed Vehicle not created at start (timed out)")
		#ENDIF
		
		RETURN TRUE
	ENDIF
	
	IF GET_SCRIPT_TASK_STATUS(localPlayerPed, SCRIPT_TASK_ENTER_VEHICLE) != WAITING_TO_START_TASK
	AND GET_SCRIPT_TASK_STATUS(localPlayerPed, SCRIPT_TASK_ENTER_VEHICLE) != PERFORMING_TASK
	AND NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed, TRUE)
		PRINTLN("[InitPosition][Vehicle ", iVehicleIndexToSpawnInto, "] - START_POSITION_SET_SPAWN_PLAYER_IN_PLACED_VEHICLE - Not performing task currently. vehicle index ", iVehicleIndexToSpawnInto, " iVehicleSeatToSpawnInto: ", iVehicleSeatToSpawnInto, " ENUM_TO_INT(vsToEnter): ", ENUM_TO_INT(vsToEnter))
		
		IF IS_VEHICLE_DRIVEABLE(veh)
						
			// Backup - Will prevent the player from not being placed in the vehicle and a long startup, but will not fix the issue that they were not put into the seat they were assigned to be in!
			IF NOT IS_VEHICLE_SEAT_FREE(veh, vsToEnter, TRUE)
				PRINTLN("[InitPosition][Vehicle ", iVehicleIndexToSpawnInto, "] - START_POSITION_SET_SPAWN_PLAYER_IN_PLACED_VEHICLE Vehicle Driver Seat is not free for vehicle index ", iVehicleIndexToSpawnInto, " ENUM_TO_INT(vsToEnter): ", ENUM_TO_INT(vsToEnter), " Setting to use passenger seat")
				vsToEnter = VS_ANY_PASSENGER
			ENDIF
	
			SET_PED_VEHICLE_FORCED_SEAT_USAGE(LocalPlayerPed, veh, 0, 0, vsToEnter)
		
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(niVeh)
				FREEZE_ENTITY_POSITION( LocalPlayerPed, FALSE )
				IF NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed, TRUE)
					SET_ENTITY_COLLISION( LocalPlayerPed, TRUE )
				ENDIF
			ENDIF
			
			SET_BIT(MC_PlayerBD[iLocalPart].iClientBitSet4, PBBOOL4_SPAWNED_IN_A_PLACED_VEHICLE)
			
			IF SHOULD_START_POSITION_SPAWN_USE_PUT_INTO_VEHICLE_NATIVE(iVehicleIndexToSpawnInto)
				PRINTLN("[InitPosition][Vehicle ", iVehicleIndexToSpawnInto, "] - START_POSITION_SET_SPAWN_PLAYER_IN_PLACED_VEHICLE | Using SET_PED_INTO_VEHICLE!")
				FREEZE_ENTITY_POSITION(LocalPlayerPed, FALSE)
				SET_PED_INTO_VEHICLE(LocalPlayerPed, veh, vsToEnter)
			ELSE
				TASK_ENTER_VEHICLE(LocalPlayerPed, veh, 1, vsToEnter, DEFAULT, ECF_RESUME_IF_INTERRUPTED | ECF_WARP_PED | ECF_WARP_IF_DOOR_IS_BLOCKED | ECF_BLOCK_SEAT_SHUFFLING | ECF_WARP_IF_SHUFFLE_LINK_IS_BLOCKED)
			ENDIF
			
			#IF IS_DEBUG_BUILD
			VECTOR vVehPos = GET_ENTITY_COORDS(veh, FALSE)
			PRINTLN("[InitPosition][Vehicle ", iVehicleIndexToSpawnInto, "] - START_POSITION_SET_SPAWN_PLAYER_IN_PLACED_VEHICLE Placing the local player into vehicle index ", iVehicleIndexToSpawnInto, " ENUM_TO_INT(vsToEnter): ", ENUM_TO_INT(vsToEnter), " Vehicle Position: ", vVehPos)						
			#ENDIF
		ELSE
			PRINTLN("[InitPosition][Vehicle ", iVehicleIndexToSpawnInto, "] - START_POSITION_SET_SPAWN_PLAYER_IN_PLACED_VEHICLE iVehicleIndexToSpawnInto is valid, but the vehicle we're trying to move into is dead!" )			
		ENDIF
	ELSE
		IF DOES_ENTITY_EXIST(veh)
			IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
				IF NETWORK_HAS_CONTROL_OF_ENTITY(veh)
					FREEZE_ENTITY_POSITION(veh, TRUE)
				ENDIF
				
				BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_SetVehVisible, iVehicleIndexToSpawnInto)
			
				PRINTLN("[InitPosition][Vehicle ", iVehicleIndexToSpawnInto, "] - START_POSITION_SET_SPAWN_PLAYER_IN_PLACED_VEHICLE - vStartPos: ", vStartPos, " fStartHeading: ", fStartHeading)
				SET_BIT(iLocalBoolCheck8, LBOOL8_HAS_START_POSITION_WARPED)
				RETURN TRUE
			ELSE
				PRINTLN("[InitPosition][Vehicle ", iVehicleIndexToSpawnInto, "] - START_POSITION_SET_SPAWN_PLAYER_IN_PLACED_VEHICLE - Not yet in the vehicle, but we have the task.")
			ENDIF
		ELSE
			PRINTLN("[InitPosition][Vehicle ", iVehicleIndexToSpawnInto, "] - START_POSITION_SET_SPAWN_PLAYER_IN_PLACED_VEHICLE - NOT NETWORK_DOES_NETWORK_ID_EXIST...")
			IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			
				BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_SetVehVisible, iVehicleIndexToSpawnInto)
			
				PRINTLN("[InitPosition][Vehicle ", iVehicleIndexToSpawnInto, "] - START_POSITION_SET_SPAWN_PLAYER_IN_PLACED_VEHICLE - vStartPos: ", vStartPos, " fStartHeading: ", fStartHeading)						
				SET_BIT(iLocalBoolCheck8,LBOOL8_HAS_START_POSITION_WARPED)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL START_POSITION_SET_SPAWN_PLAYER_IN_TEAM_VEHICLE(VECTOR vStartPos, FLOAT fStartHeading, BOOL bIsQuickRestartJob)

	IF NOT bLocalPlayerOK
		RETURN FALSE
	ENDIF

	PRINTLN("[InitPosition] - START_POSITION_SET_SPAWN_PLAYER_IN_TEAM_VEHICLE - Calling Function with vStartPos: ", vStartPos, " fStartHeading: ", fStartHeading)
	
	SET_ARENA_SPAWN_FIX_COORDS(vStartPos, fStartHeading)

	IF NOT USING_MULTIPLE_PASSENGER_TEAM_VEHICLE(MC_playerBD[ iLocalPart].iteam) 
	OR IS_DRIVER_FOR_MULTIPLE_TEAM_VEHICLE(MC_playerBD[ iLocalPart].iteam)
		IF NET_WARP_TO_COORD( 	vStartPos,
								fStartHeading, 
								IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iTeamBitSet3, ciBS3_ENABLE_MULTIPLE_PLAYER_SAME_VEHICLE_RESPAWN), 
								FALSE, 
								DEFAULT, 
								DEFAULT, 
								DEFAULT, 
								bIsQuickRestartJob OR IS_PLAYER_USING_SEQUENTIAL_SPAWNING_PROCEDURES(),
								FALSE )	
			
			PRINTLN("[InitPosition] - START_POSITION_SET_SPAWN_PLAYER_IN_TEAM_VEHICLE - completed warp to ", vStartPos, " at heading ", fStartHeading)
			
			RETURN SPAWN_PLAYER_IN_TEAM_VEHICLE(vStartPos, fStartHeading)
		ENDIF
	ELSE
		PRINTLN("[InitPosition] - START_POSITION_SET_SPAWN_PLAYER_IN_TEAM_VEHICLE - We are a Passenger of a Team Respawn Vehicle. Do not warp. ")
		RETURN SPAWN_PLAYER_IN_TEAM_VEHICLE(vStartPos, fStartHeading)
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC SET_START_POSITION_WARPING_STATE(START_POSITION_WARPING_STATE eNewState)
	PRINTLN("[InitPosition] - SET_START_POSITION_WARPING_STATE - eNewState: ", eNewState, " OldState: ", sSetUpStartPosition.eStartPositionWarpingState)
	sSetUpStartPosition.eStartPositionWarpingState = eNewState
ENDPROC

PROC REPOSITION_TEAM_SPAWN_POINT_BASED_ON_ENTITY(VECTOR &vPos, FLOAT &fHead, INT iEntityID, INT iEntityType)
	IF iEntityType != ciENTITY_TYPE_NONE
		VECTOR vTemp = MC_GET_POSITION_FOR_ENTITY_INDEX(iEntityType, iEntityID)
		FLOAT fTemp = MC_GET_HEADING_FOR_ENTITY_INDEX(iEntityType, iEntityID)
		
		PRINTLN("[InitPosition] - START_POSITION_SET - REPOSITION_TEAM_SPAWN_POINT_BASED_ON_ENTITY - Modified Position and Heading vPos: ", vPos, " to ", vTemp)
		
		vPos = vTemp
		fHead = fTemp
	ENDIF
ENDPROC

PROC REPOSITION_TEAM_SPAWN_POINT_BASED_ON_NEAREST_ROAD_NODE(VECTOR &vPos, FLOAT &fHead, FLOAT fRadiusExclusion = 0.0, FLOAT fHeightExclusion = 0.0)
		
	#IF IS_DEBUG_BUILD
	BOOL bSuccess
	#ENDIF	
	
	VECTOR vTemp
	FLOAT fTemp
	FLOAT fDistance, fPrevDist
	INT iValid, iLanes, iNode
	VECTOR vAreaMin, vAreaMax
	
	INT iRoadNodeLoopMax = ci_ROAD_NODE_REPOSITION_LOOP_EXCLUSION_LOW_MAX
	IF fRadiusExclusion >= cf_ROAD_NODE_REPOSITION_EXCLUSION_HIGH_MAX		
		iRoadNodeLoopMax = ci_ROAD_NODE_REPOSITION_LOOP_EXCLUSION_HIGH_MAX
	ELIF fRadiusExclusion > cf_ROAD_NODE_REPOSITION_EXCLUSION_MEDIUM_MAX
		iRoadNodeLoopMax = ci_ROAD_NODE_REPOSITION_LOOP_EXCLUSION_MEDIUM_MAX
	ENDIF
	
	FLOAT fStartingHeight = vPos.z
			
	IF gMC_LocalVariables_VARS.iStartPointNodeSearchOverride != 0
		iRoadNodeLoopMax = gMC_LocalVariables_VARS.iStartPointNodeSearchOverride
		PRINTLN("[InitPosition] - START_POSITION_SET - REPOSITION_TEAM_SPAWN_POINT_BASED_ON_NEAREST_ROAD_NODE - USING BG OVERRIDE gMC_LocalVariables_VARS.iStartPointNodeSearchOverride")	
	ENDIF
	
	PRINTLN("[InitPosition] - START_POSITION_SET - REPOSITION_TEAM_SPAWN_POINT_BASED_ON_NEAREST_ROAD_NODE - fRadiusExclusion: ", fRadiusExclusion, " iRoadNodeLoopMax: ", iRoadNodeLoopMax)
	
	FOR iNode = 0 TO iRoadNodeLoopMax-1
		
		IF NOT GET_NTH_CLOSEST_VEHICLE_NODE_WITH_HEADING(vPos, iNode, vTemp, fTemp, iLanes, DEFAULT, 0.0, 99999.0)
			PRINTLN("[InitPosition] - START_POSITION_SET - REPOSITION_TEAM_SPAWN_POINT_BASED_ON_NEAREST_ROAD_NODE - iNode: ", iNode, " vTemp: ", vTemp, " is Vector Zero. or GET_NTH_CLOSEST_VEHICLE_NODE_WITH_HEADING is false. Relooping.")
			RELOOP
		ENDIF
		
		IF IS_POINT_IN_PROBLEM_NODE_AREA(vTemp)
			PRINTLN("[InitPosition] - START_POSITION_SET - REPOSITION_TEAM_SPAWN_POINT_BASED_ON_NEAREST_ROAD_NODE - iNode: ", iNode, " at vTemp: ", vTemp, " is a problem node. Relooping.")
			RELOOP
		ENDIF
		
		fPrevDist = fDistance
		fDistance = VDIST2(vPos, vTemp)
		
		IF fHeightExclusion > 0
		AND ABSF(fStartingHeight - vTemp.z) > fHeightExclusion
			PRINTLN("[InitPosition] - START_POSITION_SET - REPOSITION_TEAM_SPAWN_POINT_BASED_ON_NEAREST_ROAD_NODE - iNode: ", iNode, " at vTemp: ", vTemp, " New position is outside the height exclusion range: ", fHeightExclusion)
			RELOOP
		ENDIF
		
		IF fRadiusExclusion > 0
		AND fDistance <= POW(fRadiusExclusion, 2.0)
			PRINTLN("[InitPosition] - START_POSITION_SET - REPOSITION_TEAM_SPAWN_POINT_BASED_ON_NEAREST_ROAD_NODE - iNode: ", iNode, " at vTemp: ", vTemp, " Area is inside the Radius Exclusion Area.")
			RELOOP
		ENDIF
			
		vAreaMin = vTemp + <<2.0, 2.0, 1.0>>
		vAreaMax = vTemp + <<-2.0, -2.0, -1.0>>
		
		IF IS_AREA_OCCUPIED(vAreaMin, vAreaMax, FALSE, TRUE, TRUE, FALSE, FALSE, DEFAULT, TRUE)
			PRINTLN("[InitPosition] - START_POSITION_SET - REPOSITION_TEAM_SPAWN_POINT_BASED_ON_NEAREST_ROAD_NODE - iNode: ", iNode, " at vTemp: ", vTemp, " the area here is occupied by a ped or vehicle.")
			RELOOP
		ENDIF
		
		PRINTLN("[InitPosition] - START_POSITION_SET - REPOSITION_TEAM_SPAWN_POINT_BASED_ON_NEAREST_ROAD_NODE - iNode: ", iNode, " fDistance: ", fDistance, " fPrevDist: ", fPrevDist, " -----------" )	
		
		IF (ABSF(fPrevDist - fDistance) > POW(5.0, 2.0) AND (fPrevDist - fDistance) != 0.0)
		OR fPrevDist = 0.0
			
			IF iValid >= iLocalPart // So we do not all spawn at the same road node on top of eachother.	
				PRINTLN("[InitPosition] - START_POSITION_SET - REPOSITION_TEAM_SPAWN_POINT_BASED_ON_NEAREST_ROAD_NODE - Modified Position and Heading. vPos: ", vPos, " to ", vTemp)
				vPos = vTemp
				fHead = fTemp
				
				#IF IS_DEBUG_BUILD
				bSuccess = TRUE				
				#ENDIF
				
				BREAKLOOP
			ELSE
				PRINTLN("[InitPosition] - START_POSITION_SET - REPOSITION_TEAM_SPAWN_POINT_BASED_ON_NEAREST_ROAD_NODE - iLocalPart: ", iLocalPart, " iValid: ", iValid, " Belongs to another player")
				iValid++
			ENDIF

		ELSE
			PRINTLN("[InitPosition] - START_POSITION_SET - REPOSITION_TEAM_SPAWN_POINT_BASED_ON_NEAREST_ROAD_NODE - Distance too short from the last valid road node")
		ENDIF
	ENDFOR
	
	#IF IS_DEBUG_BUILD
	IF NOT bSuccess
		PRINTLN("[InitPosition] - START_POSITION_SET - REPOSITION_TEAM_SPAWN_POINT_BASED_ON_NEAREST_ROAD_NODE - A valid road node could not be found.")
	ENDIF
	#ENDIF
ENDPROC

PROC PROCESS_START_POSITION_WARPING_STATE_INIT_CONTINUITY()
	PRINTLN("[InitPosition][Continuity] - START_POSITION_SET - Using Continuity Spawn Point.")
	
	INT iSlot
	iSlot = GET_CONTINUITY_START_POINT_INDEX(sSetUpStartPosition.iPlayerTeam)
	
	IF iSlot > -1
		sSetUpStartPosition.vStartPosition = g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][iSlot].vPos
		sSetUpStartPosition.fStartHeading = g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][iSlot].fHead
		
		REPOSITION_TEAM_SPAWN_POINT_BASED_ON_ENTITY(sSetUpStartPosition.vStartPosition, sSetUpStartPosition.fStartHeading, g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][iSlot].iMoveToEntityIndex, g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][iSlot].iMoveToEntityType)
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][iSlot].iSpawnBitSet, ci_SpawnBS_RepositionToNearestRoadNode)
			PRINTLN("[InitPosition][Continuity] - START_POSITION_SET - Moving to nearest Road Node")
			REPOSITION_TEAM_SPAWN_POINT_BASED_ON_NEAREST_ROAD_NODE(sSetUpStartPosition.vStartPosition, sSetUpStartPosition.fStartHeading, g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][iSlot].fRoadNodeExclusionRadius, g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][iSlot].fRoadNodeExclusionHeightRange)
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][iSlot].iSpawnBitSet, ci_SpawnBS_Interior)
			PRINTLN("[InitPosition][Continuity] - START_POSITION_SET - Spawn Point Flagged as in Interior.")
			SET_BIT(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_CHECK_INTERIOR)
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][iSlot].iSpawnBitSet, ci_SpawnBS_SpawnInCover)
			PRINTLN("[InitPosition][Continuity] - START_POSITION_SET - Spawn in Cover Set.")
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][iSlot].iSpawnBitSet, ci_SpawnBS_SpawnInCoverFacingLeft)
				PRINTLN("[InitPosition][Continuity] - START_POSITION_SET - Spawn in Cover Set - Force Facing Left.")
				SET_BIT(iLocalBoolCheck33, LBOOL33_SET_PLAYER_INTO_COVER_START_POSITION_FORCE_LEFT)
			ENDIF
			SET_BIT(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_POSITION_ENTER_COVER)
		ENDIF
		
		sSetUpStartPosition.iVehicleIndexToSpawnInto = g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][iSlot].iVehicle
		sSetUpStartPosition.iVehicleSeatToSpawnInto = g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][iSlot].iSeat
		
		IF sSetUpStartPosition.iVehicleIndexToSpawnInto > -1
		AND IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][iSlot].vSpawnPositionSeparateVehicle)
			PRINTLN("[InitPosition][Continuity] - START_POSITION_SET - Spawning in Placed Vehicle: ", sSetUpStartPosition.iVehicleIndexToSpawnInto)
			SET_BIT(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_SPAWN_IN_PLACED_VEHICLE)
		ENDIF
		
		IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][iSlot].iSpawnBitSet, ci_SpawnBS_RepositionPlayerPostVehicle)
		AND IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][iSlot].vSpawnPositionSeparateVehicle)
			SET_BIT(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_POSITION_USES_VEH_POSITION)
		ELSE
			PRINTLN("[InitPosition][Continuity] - START_POSITION_SET - Repositioning Vehicle to Start Point after Warp.")
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][iSlot].iSpawnBitSet, ci_SpawnBS_Spawn_In_Nearest_Vehicle)
		AND IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][iSlot].vSpawnPositionSeparateVehicle)
			PRINTLN("[InitPosition][Continuity] - START_POSITION_SET - Spawn in the Nearest Vehicle At Start.")
			SET_BIT(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_SET_INTO_NEAREST_VEHICLE)
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][iSlot].iSpawnBitSet, ci_SpawnBS_AllowPersonalVehicleToSpawnAtStart)
			PRINTLN("[InitPosition][Continuity] - START_POSITION_SET - Spawn the Personal Vehicle At Start.")
			SET_BIT(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_SPAWN_PERSONAL_VEHICLE_AT_START)
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][iSlot].iSpawnBitSet, ci_SpawnBS_SpawnInPersonalVehicleAtStart)
		AND IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][iSlot].vSpawnPositionSeparateVehicle)
			PRINTLN("[InitPosition][Continuity] - START_POSITION_SET - Spawn in Personal Vehicle At Start.")
			SET_BIT(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_SPAWN_IN_PERSONAL_VEHICLE)
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][iSlot].iSpawnBitSet, ci_SpawnBS_ReEnablePersonalVehiclesIfLostStartingVehicle)
			PRINTLN("[InitPosition][Continuity] - START_POSITION_SET - Setting that Personal Vehicles should be re-enabled if we lose our starting one.")
			SET_BIT(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_RENABLE_PERSONAL_VEHICLES_IF_START_VEHICLE_IS_GONE)
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][iSlot].iSpawnBitSet, ci_SpawnBS_EnablePersonalVehiclesForMission)
			PRINTLN("[InitPosition][Continuity] - START_POSITION_SET - Setting that Personal Vehicle Systems should be Enabled NOW, because we chose this start point.")
			SET_BIT(g_iBS1_Mission, ciBS1_Mission_EnablePersonalVehicles)
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][iSlot].iSpawnBitSet, ci_SpawnBS_DisablePersonalVehiclesForMission)
			PRINTLN("[InitPosition][Continuity] - START_POSITION_SET - Setting that Personal Vehicle Systems should be Disabled NOW, because we chose this start point.")
			SET_BIT(g_iBS1_Mission, ciBS1_Mission_DisablePersonalVehicles)
		ENDIF
				
		IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][iSlot].vSpawnPositionSeparateVehicle)
			sSetUpStartPosition.vVehiclePositionOverride = g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][iSlot].vSpawnPositionSeparateVehicle
			sSetUpStartPosition.fVehicleHeadingOverride = g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][iSlot].fSpawnHeadingSeparateVehicle
			PRINTLN("[InitPosition][Continuity] - START_POSITION_SET - Overides set up for vehicles: sSetUpStartPosition.vVehiclePositionOverride: ", sSetUpStartPosition.vVehiclePositionOverride, " sSetUpStartPosition.fVehicleHeadingOverride: ", sSetUpStartPosition.fVehicleHeadingOverride)
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][iSlot].iSpawnBitSet, ci_SpawnBS_SeparateVehiclePositionIsRelative)
			PRINTLN("[InitPosition][Continuity] - START_POSITION_SET - Setting that Separate Vehicle Positions are Relative. BS_INIT_START_SEPARATE_VEH_POS_IS_RELATIVE.")		
			SET_BIT(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_SEPARATE_VEH_POS_IS_RELATIVE)
		ENDIF					
	ENDIF
	
	PRINTLN("[InitPosition][Continuity] - START_POSITION_SET - Continuity Spawn Point: ", iSlot, " is being used sSetUpStartPosition.iVehicleIndexToSpawnInto: " , sSetUpStartPosition.iVehicleIndexToSpawnInto, " Use Vehicle Position: ", BOOL_TO_STRING(IS_BIT_SET(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_POSITION_USES_VEH_POSITION)))
	
	SET_START_POSITION_WARPING_STATE(START_POSITION_WARPING_STATE_WARP)
ENDPROC

PROC PROCESS_START_POSITION_WARPING_STATE_INIT_START_POINT_CHECKPOINTS()
	PRINTLN("[InitPosition] - SET_UP_START_POSITION_DATA - Using Custom Start Point Checkpoints.")
	
	INT iCheckpoint
	iCheckpoint = -1
	
	IF sSetUpStartPosition.iStartRule < FMMC_MAX_RULES
		iCheckpoint = g_FMMC_STRUCT.sFMMCEndConditions[sSetUpStartPosition.iPlayerTeam].iRespawnOnRuleEndCheckpoint[sSetUpStartPosition.iStartRule]
		
		IF iCheckpoint = 0
			iCheckpoint = MC_GET_MISSION_STARTING_CHECKPOINT()
		ENDIF
	ENDIF
	
	USE_CUSTOM_SPAWN_POINTS(TRUE, default, default, default, default, default, default, default, default, default, default, TRUE)
	
	INT iValidCount
	INT i
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[sSetUpStartPosition.iPlayerTeam]-1
		IF MC_ServerBD_4.iCurrentHighestPriority[sSetUpStartPosition.iPlayerTeam] < FMMC_MAX_RULES
			
			IF IS_SPAWN_POINT_A_VALID_CHECKPOINT(i, sSetUpStartPosition.iPlayerTeam, DEFAULT, iCheckpoint)
				
				IF iValidCount = sSetUpStartPosition.iPlayerTeamSlot
				OR IS_SPAWN_POINT_BELONGING_TO_PARTICIPANT(i, sSetUpStartPosition.iPlayerTeam, iLocalPart)
				
					PRINTLN("[InitPosition][CustomCheckpoints] - START_POSITION_SET - adding point: ", i)
					sSetUpStartPosition.fStartHeading = g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][i].fHead
					sSetUpStartPosition.vStartPosition = GET_CUSTOM_RESPAWN_POSITION(sSetUpStartPosition.fStartHeading, i, sSetUpStartPosition.iPlayerTeam, -1, TRUE)
					REPOSITION_TEAM_SPAWN_POINT_BASED_ON_ENTITY(sSetUpStartPosition.vStartPosition, sSetUpStartPosition.fStartHeading, g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][i].iMoveToEntityIndex, g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][i].iMoveToEntityType)
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][i].iSpawnBitSet, ci_SpawnBS_RepositionToNearestRoadNode)							
						PRINTLN("[InitPosition][CustomCheckpoints] - START_POSITION_SET - Moving to nearest Road Node")
						REPOSITION_TEAM_SPAWN_POINT_BASED_ON_NEAREST_ROAD_NODE(sSetUpStartPosition.vStartPosition, sSetUpStartPosition.fStartHeading, g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][i].fRoadNodeExclusionRadius, g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][i].fRoadNodeExclusionHeightRange)							
					ENDIF
					ADD_CUSTOM_SPAWN_POINT(sSetUpStartPosition.vStartPosition, sSetUpStartPosition.fStartHeading)
					
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][i].iSpawnBitSet, ci_SpawnBS_SpawnInCover)
						PRINTLN("[InitPosition][CustomCheckpoints] - START_POSITION_SET - Spawn in Cover Set.")
						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][i].iSpawnBitSet, ci_SpawnBS_SpawnInCoverFacingLeft)
							PRINTLN("[InitPosition][CustomCheckpoints] - START_POSITION_SET - Spawn in Cover Set - Force Facing Left.")
							SET_BIT(iLocalBoolCheck33, LBOOL33_SET_PLAYER_INTO_COVER_START_POSITION_FORCE_LEFT)
						ENDIF
						SET_BIT(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_POSITION_ENTER_COVER)
					ENDIF
					
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][i].iSpawnBitSet, ci_SpawnBS_Interior)
						PRINTLN("[InitPosition][CustomCheckpoints] - START_POSITION_SET - Spawn Point Flagged as in Interior.")
						SET_BIT(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_CHECK_INTERIOR)
					ENDIF
					
					sSetUpStartPosition.iVehicleIndexToSpawnInto = g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][i].iVehicle
					sSetUpStartPosition.iVehicleSeatToSpawnInto = g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][i].iSeat
					
					IF sSetUpStartPosition.iVehicleIndexToSpawnInto > -1
					AND IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][i].vSpawnPositionSeparateVehicle)
						SET_BIT(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_SPAWN_IN_PLACED_VEHICLE)
						PRINTLN("[InitPosition][CustomCheckpoints] - START_POSITION_SET - Spawning in Placed Vehicle: ", sSetUpStartPosition.iVehicleIndexToSpawnInto)
					ENDIF
					
					IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][i].iSpawnBitSet, ci_SpawnBS_RepositionPlayerPostVehicle)						
					AND IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][i].vSpawnPositionSeparateVehicle)
						SET_BIT(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_POSITION_USES_VEH_POSITION)
					ELSE
						PRINTLN("[InitPosition][CustomCheckpoints] - START_POSITION_SET - Repositioning Vehicle to Start Point after Warp.")
					ENDIF
					
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][i].iSpawnBitSet, ci_SpawnBS_Spawn_In_Nearest_Vehicle)
					AND IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][i].vSpawnPositionSeparateVehicle)
						PRINTLN("[InitPosition][CustomCheckpoints] - START_POSITION_SET - Spawn in the Nearest Vehicle At Start.")
						SET_BIT(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_SET_INTO_NEAREST_VEHICLE)
					ENDIF
										
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][i].iSpawnBitSet, ci_SpawnBS_AllowPersonalVehicleToSpawnAtStart)
						PRINTLN("[InitPosition][CustomCheckpoints] - START_POSITION_SET - Spawn the Personal Vehicle At Start.")
						SET_BIT(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_SPAWN_PERSONAL_VEHICLE_AT_START)
					ENDIF
					
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][i].iSpawnBitSet, ci_SpawnBS_SpawnInPersonalVehicleAtStart)
					AND IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][i].vSpawnPositionSeparateVehicle)
						PRINTLN("[InitPosition][CustomCheckpoints] - START_POSITION_SET - Spawn in Personal Vehicle At Start.")
						SET_BIT(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_SPAWN_IN_PERSONAL_VEHICLE)
					ENDIF
					
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][i].iSpawnBitSet, ci_SpawnBS_ReEnablePersonalVehiclesIfLostStartingVehicle)
						PRINTLN("[InitPosition][CustomCheckpoints] - START_POSITION_SET - Setting that Personal Vehicles should be re-enabled if we lose our starting one.")
						SET_BIT(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_RENABLE_PERSONAL_VEHICLES_IF_START_VEHICLE_IS_GONE)
					ENDIF
					
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][i].iSpawnBitSet, ci_SpawnBS_EnablePersonalVehiclesForMission)
						PRINTLN("[InitPosition][CustomCheckpoints] - START_POSITION_SET - Setting that Personal Vehicle Systems should be Enabled NOW, because we chose this start point.")
						SET_BIT(g_iBS1_Mission, ciBS1_Mission_EnablePersonalVehicles)
					ENDIF
					
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][i].iSpawnBitSet, ci_SpawnBS_DisablePersonalVehiclesForMission)
						PRINTLN("[InitPosition][CustomCheckpoints] - START_POSITION_SET - Setting that Personal Vehicle Systems should be Disabled NOW, because we chose this start point.")
						SET_BIT(g_iBS1_Mission, ciBS1_Mission_DisablePersonalVehicles)
					ENDIF
					
					IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][i].vSpawnPositionSeparateVehicle)
						sSetUpStartPosition.vVehiclePositionOverride = g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][i].vSpawnPositionSeparateVehicle
						sSetUpStartPosition.fVehicleHeadingOverride = g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][i].fSpawnHeadingSeparateVehicle
						PRINTLN("[InitPosition][CustomCheckpoints] - START_POSITION_SET - Overides set up for vehicles: sSetUpStartPosition.vVehiclePositionOverride: ", sSetUpStartPosition.vVehiclePositionOverride, " sSetUpStartPosition.fVehicleHeadingOverride: ", sSetUpStartPosition.fVehicleHeadingOverride)
					ENDIF
										
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][i].iSpawnBitSet, ci_SpawnBS_SeparateVehiclePositionIsRelative)
						PRINTLN("[InitPosition][CustomCheckpoints] - START_POSITION_SET - Setting that Separate Vehicle Positions are Relative. BS_INIT_START_SEPARATE_VEH_POS_IS_RELATIVE.")		
						SET_BIT(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_SEPARATE_VEH_POS_IS_RELATIVE)
					ENDIF	
					
					PRINTLN("[InitPosition][CustomCheckpoints] - START_POSITION_SET - Spawn Point: ", i," being used with sSetUpStartPosition.iVehicleIndexToSpawnInto: " , sSetUpStartPosition.iVehicleIndexToSpawnInto, " sSetUpStartPosition.iVehicleSeatToSpawnInto: ", sSetUpStartPosition.iVehicleSeatToSpawnInto, " Use Vehicle Position: ", BOOL_TO_STRING(IS_BIT_SET(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_POSITION_USES_VEH_POSITION)))
					
					BREAKLOOP
				ELSE
					iValidCount++
					PRINTLN("[InitPosition][CustomCheckpoints] - START_POSITION_SET - iSpawnPoint: ", i, " not valid, incrementing validCount to: ", iValidCount, " iPlayerTeamSlot: ", sSetUpStartPosition.iPlayerTeamSlot)
				ENDIF
			ELSE
				PRINTLN("[InitPosition][CustomCheckpoints] - START_POSITION_SET - Team spawn point ", i, " isn't valid")
			ENDIF
		ENDIF
	ENDFOR
	
	SET_START_POSITION_WARPING_STATE(START_POSITION_WARPING_STATE_WARP)

ENDPROC

PROC PROCESS_START_POSITION_WARPING_STATE_INIT_CHECKPOINTS()
	PRINTLN("[InitPosition][Checkpoint] - SET_UP_START_POSITION_DATA - Using Checkpoints.")
	
	INT iCheckpoint
	iCheckpoint = -1
	
	IF sSetUpStartPosition.iStartRule < FMMC_MAX_RULES
		iCheckpoint = g_FMMC_STRUCT.sFMMCEndConditions[sSetUpStartPosition.iPlayerTeam].iRespawnOnRuleEndCheckpoint[sSetUpStartPosition.iStartRule]
		
		IF iCheckpoint = 0
			iCheckpoint = MC_GET_MISSION_STARTING_CHECKPOINT()
		ENDIF
		
		IF iCheckpoint >= 0
			sSetUpStartPosition.vStartPosition = MC_GET_TEAM_CHECKPOINT_QUICK_RESTART_START_POS(sSetUpStartPosition.iPlayerTeam, sSetUpStartPosition.iPlayerTeamSlot, iCheckpoint)
			sSetUpStartPosition.fStartHeading = GET_TEAM_CHECKPOINT_QUICK_RESTART_HEADING(sSetUpStartPosition.iPlayerTeam, sSetUpStartPosition.iPlayerTeamSlot, iCheckpoint)
						
			IF IS_BIT_SET(g_FMMC_STRUCT.iRestartInCoverBitset, (sSetUpStartPosition.iPlayerTeam * FMMC_MAX_RESTART_CHECKPOINTS) + iCheckpoint)
				SET_BIT(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_POSITION_ENTER_COVER)
				PRINTLN("[InitPosition][Checkpoint] - SET_UP_START_POSITION_DATA - BS_INIT_START_POSITION_ENTER_COVER is set.")
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iRestartInNearbyPlacedVehicleBitset, (sSetUpStartPosition.iPlayerTeam * FMMC_MAX_RESTART_CHECKPOINTS) + iCheckpoint)
				SET_BIT(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_SET_INTO_NEAREST_VEHICLE)
				PRINTLN("[InitPosition][Checkpoint] - SET_UP_START_POSITION_DATA - BS_INIT_START_SET_INTO_NEAREST_VEHICLE is set.")
			ENDIF
		ENDIF
	ENDIF
	
	PRINTLN("[InitPosition][Checkpoint] - SET_UP_START_POSITION_DATA - Checkpoint: ", iCheckpoint, " is being used")
	
	SET_START_POSITION_WARPING_STATE(START_POSITION_WARPING_STATE_WARP)
ENDPROC

PROC PROCESS_START_POSITION_WARPING_STATE_INIT_CUSTOM_START_POINTS()
	PRINTLN("[InitPosition][CustomStart] - START_POSITION_SET - Using Team Custom Respawn Points.")
				
	USE_CUSTOM_SPAWN_POINTS(TRUE, default, default, default, default, default, default, default, default, default, default, TRUE)
	
	INT i
	INT iValidCount
		
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[sSetUpStartPosition.iPlayerTeam]-1
		IF MC_ServerBD_4.iCurrentHighestPriority[sSetUpStartPosition.iPlayerTeam] < FMMC_MAX_RULES
			
			IF IS_SPAWN_POINT_A_VALID_START_POINT(i, sSetUpStartPosition.iPlayerTeam)
				
				IF iValidCount = sSetUpStartPosition.iPlayerTeamSlot
				OR IS_SPAWN_POINT_BELONGING_TO_PARTICIPANT(i, sSetUpStartPosition.iPlayerTeam, iLocalPart)				
				
					PRINTLN("[InitPosition][CustomStart] - START_POSITION_SET - adding point: ", i)
					sSetUpStartPosition.fStartHeading = g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][i].fHead
					sSetUpStartPosition.vStartPosition = GET_CUSTOM_RESPAWN_POSITION(sSetUpStartPosition.fStartHeading, i, sSetUpStartPosition.iPlayerTeam, -1, TRUE)
					REPOSITION_TEAM_SPAWN_POINT_BASED_ON_ENTITY(sSetUpStartPosition.vStartPosition, sSetUpStartPosition.fStartHeading, g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][i].iMoveToEntityIndex, g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][i].iMoveToEntityType)
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][i].iSpawnBitSet, ci_SpawnBS_RepositionToNearestRoadNode)
						PRINTLN("[InitPosition][CustomStart] - START_POSITION_SET - Moving to nearest Road Node")
						REPOSITION_TEAM_SPAWN_POINT_BASED_ON_NEAREST_ROAD_NODE(sSetUpStartPosition.vStartPosition, sSetUpStartPosition.fStartHeading, g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][i].fRoadNodeExclusionRadius, g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][i].fRoadNodeExclusionHeightRange)
					ENDIF
					ADD_CUSTOM_SPAWN_POINT(sSetUpStartPosition.vStartPosition, sSetUpStartPosition.fStartHeading)
									
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][i].iSpawnBitSet, ci_SpawnBS_SpawnInCover)
						PRINTLN("[InitPosition][CustomStart] - START_POSITION_SET - Spawn in Cover Set.")
						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][i].iSpawnBitSet, ci_SpawnBS_SpawnInCoverFacingLeft)
							PRINTLN("[InitPosition][CustomStart] - START_POSITION_SET - Spawn in Cover Set - Force Facing Left.")
							SET_BIT(iLocalBoolCheck33, LBOOL33_SET_PLAYER_INTO_COVER_START_POSITION_FORCE_LEFT)
						ENDIF
						SET_BIT(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_POSITION_ENTER_COVER)
					ENDIF
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][i].iSpawnBitSet, ci_SpawnBS_Interior)
						PRINTLN("[InitPosition][CustomStart] - START_POSITION_SET - Spawn Point Flagged as in Interior.")
						SET_BIT(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_CHECK_INTERIOR)
					ENDIF
					
					sSetUpStartPosition.iVehicleIndexToSpawnInto = g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][i].iVehicle
					sSetUpStartPosition.iVehicleSeatToSpawnInto = g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][i].iSeat
					
					IF sSetUpStartPosition.iVehicleIndexToSpawnInto > -1
					AND IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][i].vSpawnPositionSeparateVehicle)
						PRINTLN("[InitPosition][CustomStart] - START_POSITION_SET - Spawning in Placed Vehicle: ", sSetUpStartPosition.iVehicleIndexToSpawnInto)
						SET_BIT(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_SPAWN_IN_PLACED_VEHICLE)
					ENDIF
					
					IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][i].iSpawnBitSet, ci_SpawnBS_RepositionPlayerPostVehicle)
					AND IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][i].vSpawnPositionSeparateVehicle)
						SET_BIT(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_POSITION_USES_VEH_POSITION)
					ELSE
						PRINTLN("[InitPosition][CustomStart] - START_POSITION_SET - Repositioning Vehicle to Start Point after Warp.")
					ENDIF
					
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][i].iSpawnBitSet, ci_SpawnBS_Spawn_In_Nearest_Vehicle)
					AND IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][i].vSpawnPositionSeparateVehicle)
						PRINTLN("[InitPosition][CustomStart] - START_POSITION_SET - Spawn in the Nearest Vehicle At Start.")
						SET_BIT(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_SET_INTO_NEAREST_VEHICLE)							
					ENDIF
					
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][i].iSpawnBitSet, ci_SpawnBS_AllowPersonalVehicleToSpawnAtStart)
						PRINTLN("[InitPosition][CustomStart] - START_POSITION_SET - Spawn the Personal Vehicle At Start.")
						SET_BIT(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_SPAWN_PERSONAL_VEHICLE_AT_START)
					ENDIF
					
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][i].iSpawnBitSet, ci_SpawnBS_SpawnInPersonalVehicleAtStart)
					AND IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][i].vSpawnPositionSeparateVehicle)
						PRINTLN("[InitPosition][CustomStart] - START_POSITION_SET - Spawn in Personal Vehicle At Start.")
						SET_BIT(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_SPAWN_IN_PERSONAL_VEHICLE)
					ENDIF
					
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][i].iSpawnBitSet, ci_SpawnBS_ReEnablePersonalVehiclesIfLostStartingVehicle)
						PRINTLN("[InitPosition][CustomStart] - START_POSITION_SET - Setting that Personal Vehicles should be re-enabled if we lose our starting one.")
						SET_BIT(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_RENABLE_PERSONAL_VEHICLES_IF_START_VEHICLE_IS_GONE)
					ENDIF
										
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][i].iSpawnBitSet, ci_SpawnBS_EnablePersonalVehiclesForMission)
						PRINTLN("[InitPosition][CustomStart] - START_POSITION_SET - Setting that Personal Vehicle Systems should be Enabled NOW, because we chose this start point.")
						SET_BIT(g_iBS1_Mission, ciBS1_Mission_EnablePersonalVehicles)
					ENDIF
					
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][i].iSpawnBitSet, ci_SpawnBS_DisablePersonalVehiclesForMission)
						PRINTLN("[InitPosition][CustomStart] - START_POSITION_SET - Setting that Personal Vehicle Systems should be Disabled NOW, because we chose this start point.")
						SET_BIT(g_iBS1_Mission, ciBS1_Mission_DisablePersonalVehicles)
					ENDIF
					
					IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][i].vSpawnPositionSeparateVehicle)
						sSetUpStartPosition.vVehiclePositionOverride = g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][i].vSpawnPositionSeparateVehicle
						sSetUpStartPosition.fVehicleHeadingOverride = g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][i].fSpawnHeadingSeparateVehicle
						PRINTLN("[InitPosition][CustomStart] - START_POSITION_SET - Overides set up for vehicles: sSetUpStartPosition.vVehiclePositionOverride: ", sSetUpStartPosition.vVehiclePositionOverride, " sSetUpStartPosition.fVehicleHeadingOverride: ", sSetUpStartPosition.fVehicleHeadingOverride)
					ENDIF
					
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][i].iSpawnBitSet, ci_SpawnBS_SeparateVehiclePositionIsRelative)
						PRINTLN("[InitPosition][CustomStart] - START_POSITION_SET - Setting that Separate Vehicle Positions are Relative. BS_INIT_START_SEPARATE_VEH_POS_IS_RELATIVE.")		
						SET_BIT(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_SEPARATE_VEH_POS_IS_RELATIVE)
					ENDIF	
					
					PRINTLN("[InitPosition][CustomStart] - START_POSITION_SET - Spawn Point: ", i," being used with sSetUpStartPosition.iVehicleIndexToSpawnInto: " , sSetUpStartPosition.iVehicleIndexToSpawnInto, " sSetUpStartPosition.iVehicleSeatToSpawnInto: ", sSetUpStartPosition.iVehicleSeatToSpawnInto, " Use Vehicle Position: ", BOOL_TO_STRING(IS_BIT_SET(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_POSITION_USES_VEH_POSITION)))
					
					BREAKLOOP
				ELSE
					iValidCount++
					PRINTLN("[InitPosition][CustomStart] - START_POSITION_SET - iSpawnPoint: ", i, " not valid, incrementing validCount to: ", iValidCount, " iPlayerTeamSlot: ", sSetUpStartPosition.iPlayerTeamSlot)
				ENDIF
			ELSE
				PRINTLN("[InitPosition][CustomStart] - START_POSITION_SET - Team spawn point ", i, " isn't valid")
			ENDIF
		ENDIF
	ENDFOR
	
	SET_START_POSITION_WARPING_STATE(START_POSITION_WARPING_STATE_WARP)
ENDPROC

PROC PROCESS_START_POSITION_WARPING_STATE_INIT_TEAM_START_POINTS_SIMPLE()	
	PRINTLN("[InitPosition][SimpleStart] - START_POSITION_SET - Using Team Start Points (Simple) ------------------------")
	
	USE_CUSTOM_SPAWN_POINTS(TRUE, default, default, default, default, default, default, default, default, default, default, TRUE)
	
	sSetUpStartPosition.fStartHeading = g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][sSetUpStartPosition.iPlayerTeamSlot].fHead
	sSetUpStartPosition.vStartPosition = GET_CUSTOM_RESPAWN_POSITION(sSetUpStartPosition.fStartHeading, sSetUpStartPosition.iPlayerTeamSlot, sSetUpStartPosition.iPlayerTeam, -1, TRUE)
	REPOSITION_TEAM_SPAWN_POINT_BASED_ON_ENTITY(sSetUpStartPosition.vStartPosition, sSetUpStartPosition.fStartHeading, g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][sSetUpStartPosition.iPlayerTeamSlot].iMoveToEntityIndex, g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][sSetUpStartPosition.iPlayerTeamSlot].iMoveToEntityType)
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][sSetUpStartPosition.iPlayerTeamSlot].iSpawnBitSet, ci_SpawnBS_RepositionToNearestRoadNode)
		REPOSITION_TEAM_SPAWN_POINT_BASED_ON_NEAREST_ROAD_NODE(sSetUpStartPosition.vStartPosition, sSetUpStartPosition.fStartHeading, g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][sSetUpStartPosition.iPlayerTeamSlot].fRoadNodeExclusionRadius, g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][sSetUpStartPosition.iPlayerTeamSlot].fRoadNodeExclusionHeightRange)
	ENDIF
	ADD_CUSTOM_SPAWN_POINT(sSetUpStartPosition.vStartPosition, sSetUpStartPosition.fStartHeading)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][sSetUpStartPosition.iPlayerTeamSlot].iSpawnBitSet, ci_SpawnBS_Interior)
		PRINTLN("[InitPosition][SimpleStart] - START_POSITION_SET - Spawn Point Flagged as in Interior.")
		SET_BIT(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_CHECK_INTERIOR)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][sSetUpStartPosition.iPlayerTeamSlot].iSpawnBitSet, ci_SpawnBS_SpawnInCover)
		PRINTLN("[InitPosition][SimpleStart] - START_POSITION_SET - Spawn in Cover Set.")
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][sSetUpStartPosition.iPlayerTeamSlot].iSpawnBitSet, ci_SpawnBS_SpawnInCoverFacingLeft)
			PRINTLN("[InitPosition][SimpleStart] - START_POSITION_SET - Spawn in Cover Set - Force Facing Left.")
			SET_BIT(iLocalBoolCheck33, LBOOL33_SET_PLAYER_INTO_COVER_START_POSITION_FORCE_LEFT)
		ENDIF
		SET_BIT(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_POSITION_ENTER_COVER)
	ENDIF
	
	sSetUpStartPosition.iVehicleIndexToSpawnInto = g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][sSetUpStartPosition.iPlayerTeamSlot].iVehicle
	sSetUpStartPosition.iVehicleSeatToSpawnInto = g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][sSetUpStartPosition.iPlayerTeamSlot].iSeat
	
	IF sSetUpStartPosition.iVehicleIndexToSpawnInto > -1	
	AND IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][sSetUpStartPosition.iPlayerTeamSlot].vSpawnPositionSeparateVehicle)
		PRINTLN("[InitPosition][SimpleStart] - START_POSITION_SET - Spawning in Placed Vehicle: ", sSetUpStartPosition.iVehicleIndexToSpawnInto)
		SET_BIT(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_SPAWN_IN_PLACED_VEHICLE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][sSetUpStartPosition.iPlayerTeamSlot].iSpawnBitSet, ci_SpawnBS_Spawn_In_Nearest_Vehicle)
	AND IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][sSetUpStartPosition.iPlayerTeamSlot].vSpawnPositionSeparateVehicle)
		PRINTLN("[InitPosition][SimpleStart] - START_POSITION_SET - Spawn in the Nearest Vehicle At Start.")
		SET_BIT(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_SET_INTO_NEAREST_VEHICLE)							
	ENDIF
					
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][sSetUpStartPosition.iPlayerTeamSlot].iSpawnBitSet, ci_SpawnBS_RepositionPlayerPostVehicle)
	AND IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][sSetUpStartPosition.iPlayerTeamSlot].vSpawnPositionSeparateVehicle)
		SET_BIT(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_POSITION_USES_VEH_POSITION)	
	ELSE	
		PRINTLN("[InitPosition][SimpleStart] - START_POSITION_SET - Repositioning Vehicle to Start Point after Warp.")
	ENDIF
		
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][sSetUpStartPosition.iPlayerTeamSlot].iSpawnBitSet, ci_SpawnBS_AllowPersonalVehicleToSpawnAtStart)
		PRINTLN("[InitPosition][SimpleStart] - START_POSITION_SET - Spawn the Personal Vehicle At Start.")
		SET_BIT(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_SPAWN_PERSONAL_VEHICLE_AT_START)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][sSetUpStartPosition.iPlayerTeamSlot].iSpawnBitSet, ci_SpawnBS_SpawnInPersonalVehicleAtStart)
	AND IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][sSetUpStartPosition.iPlayerTeamSlot].vSpawnPositionSeparateVehicle)
		PRINTLN("[InitPosition][SimpleStart] - START_POSITION_SET - Spawn in Personal Vehicle At Start.")
		SET_BIT(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_SPAWN_IN_PERSONAL_VEHICLE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][sSetUpStartPosition.iPlayerTeamSlot].iSpawnBitSet, ci_SpawnBS_ReEnablePersonalVehiclesIfLostStartingVehicle)
		PRINTLN("[InitPosition][SimpleStart] - START_POSITION_SET - Setting that Personal Vehicles should be re-enabled if we lose our starting one.")
		SET_BIT(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_RENABLE_PERSONAL_VEHICLES_IF_START_VEHICLE_IS_GONE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][sSetUpStartPosition.iPlayerTeamSlot].iSpawnBitSet, ci_SpawnBS_EnablePersonalVehiclesForMission)
		PRINTLN("[InitPosition][SimpleStart] - START_POSITION_SET - Setting that Personal Vehicle Systems should be Enabled NOW, because we chose this start point.")
		SET_BIT(g_iBS1_Mission, ciBS1_Mission_EnablePersonalVehicles)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][sSetUpStartPosition.iPlayerTeamSlot].iSpawnBitSet, ci_SpawnBS_DisablePersonalVehiclesForMission)
		PRINTLN("[InitPosition][SimpleStart] - START_POSITION_SET - Setting that Personal Vehicle Systems should be Disabled NOW, because we chose this start point.")
		SET_BIT(g_iBS1_Mission, ciBS1_Mission_DisablePersonalVehicles)
	ENDIF

	IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][sSetUpStartPosition.iPlayerTeamSlot].vSpawnPositionSeparateVehicle)
		sSetUpStartPosition.vVehiclePositionOverride = g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][sSetUpStartPosition.iPlayerTeamSlot].vSpawnPositionSeparateVehicle
		sSetUpStartPosition.fVehicleHeadingOverride = g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][sSetUpStartPosition.iPlayerTeamSlot].fSpawnHeadingSeparateVehicle
		PRINTLN("[InitPosition][SimpleStart] - START_POSITION_SET - Overides set up for vehicles: sSetUpStartPosition.vVehiclePositionOverride: ", sSetUpStartPosition.vVehiclePositionOverride, " sSetUpStartPosition.fVehicleHeadingOverride: ", sSetUpStartPosition.fVehicleHeadingOverride)
	ENDIF
		
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][sSetUpStartPosition.iPlayerTeamSlot].iSpawnBitSet, ci_SpawnBS_SeparateVehiclePositionIsRelative)
		PRINTLN("[InitPosition][SimpleStart] - START_POSITION_SET - Setting that Separate Vehicle Positions are Relative. BS_INIT_START_SEPARATE_VEH_POS_IS_RELATIVE.")		
		SET_BIT(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_SEPARATE_VEH_POS_IS_RELATIVE)
	ENDIF	
	
	PRINTLN("[InitPosition][SimpleStart] - START_POSITION_SET - being used with sSetUpStartPosition.iVehicleIndexToSpawnInto: " , sSetUpStartPosition.iVehicleIndexToSpawnInto, " sSetUpStartPosition.iVehicleSeatToSpawnInto: ", sSetUpStartPosition.iVehicleSeatToSpawnInto, " Use Vehicle Position: ", BOOL_TO_STRING(IS_BIT_SET(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_POSITION_USES_VEH_POSITION)))
	
	SET_START_POSITION_WARPING_STATE(START_POSITION_WARPING_STATE_WARP)
ENDPROC

PROC PROCESS_START_POSITION_WARPING_STATE_INIT_SINGLE_START_POINT()
	PRINTLN("[InitPosition] - SET_UP_START_POSITION_DATA - Single Start Position being used")
	
	#IF IS_DEBUG_BUILD
	ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_FALLBACK_SPAWNING_USED, ENTITY_RUNTIME_ERROR_TYPE_CRITICAL, "Fallback Spawning has been used. There were no valid Start/Check points.")
	#ENDIF
	
	sSetUpStartPosition.vStartPosition = g_FMMC_STRUCT.sFMMCEndConditions[sSetUpStartPosition.iPlayerTeam].vStartPos
	sSetUpStartPosition.fStartHeading = GET_HEADING_FROM_COORDS_LA(sSetUpStartPosition.vStartPosition, GET_COORDS_OF_CLOSEST_OBJECTIVE())
	SET_START_POSITION_WARPING_STATE(START_POSITION_WARPING_STATE_WARP)
	
ENDPROC

PROC PROCESS_START_POSITION_WARPING_STATE_WARP(BOOL bQuickWarp)	
	IF NET_WARP_TO_COORD(sSetUpStartPosition.vStartPosition, sSetUpStartPosition.fStartHeading, FALSE, FALSE, FALSE, FALSE, TRUE, bQuickWarp, FALSE)
		PRINTLN("[InitPosition] - SET_UP_START_POSITION_DATA - Warp Succeeded.")
		SET_START_POSITION_WARPING_STATE(START_POSITION_WARPING_STATE_POST_WARP)
	ELSE
		PRINTLN("[InitPosition] - SET_UP_START_POSITION_DATA - Waiting for Warp to complete.")
	ENDIF
ENDPROC

FUNC BOOL CAN_PERSONAL_VEHICLE_BE_SPAWNED()

	// We won't know if it can be spawned until we attempt it due to certain freemode data needing set.
	IF NOT IS_BIT_SET(iLocalBoolCheck34, LBOOL34_INITIAL_REQUESTED_PERSONAL_VEHICLE)
		RETURN TRUE
	ENDIF
		
	IF CURRENT_SAVED_VEHICLE_SLOT() = -1
		PRINTLN("[InitPosition] - CAN_PERSONAL_VEHICLE_BE_SPAWNED - No current saved vehicle selected.")
		#IF IS_DEBUG_BUILD
		ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_START_PV_DID_NOT_SPAWN, ENTITY_RUNTIME_ERROR_TYPE_WARNING, "CAN_PERSONAL_VEHICLE_BE_SPAWNED = FALSE Personal Vehicle Slot is -1. No current saved vehicle selected.")
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF NOT IS_MP_SAVED_VEHICLE_AVAILABLE_FOR_CREATION()
		PRINTLN("[InitPosition] - CAN_PERSONAL_VEHICLE_BE_SPAWNED - CAN_PERSONAL_VEHICLE_BE_SPAWNED = FALSE - Personal Vehicle Fail Reason ", g_iFailReason_SavedVehicleAvailableForCreation)
		#IF IS_DEBUG_BUILD
		ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_START_PV_DID_NOT_SPAWN, ENTITY_RUNTIME_ERROR_TYPE_WARNING, "CAN_PERSONAL_VEHICLE_BE_SPAWNED = FALSE - Personal Vehicle Fail Reason #", g_iFailReason_SavedVehicleAvailableForCreation)
		#ENDIF
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL DOES_PERSONAL_VEHICLE_EXIST_AT_START()
	VEHICLE_INDEX veh
	
	START_POSITION_SET_SPAWN_PERSONAL_VEHICLE_AT_START()
	
	IF NOT HAS_NET_TIMER_STARTED(sSetUpStartPosition.tdPersonalVehicleFailSafeTimer)
		START_NET_TIMER(sSetUpStartPosition.tdPersonalVehicleFailSafeTimer)
		PRINTLN("[InitPosition] - DOES_PERSONAL_VEHICLE_EXIST_AT_START - Starting Personal Vehicle Creation and Entry Safety Timer.")
	ENDIF
	
	veh = PERSONAL_VEHICLE_ID()

	IF NOT HAS_NET_TIMER_EXPIRED_READ_ONLY(sSetUpStartPosition.tdPersonalVehicleFailSafeTimer, ciPERSONAL_VEHICLE_FAIL_SAFE_TIME)
		
		IF NOT DOES_ENTITY_EXIST(veh)
			PRINTLN("[InitPosition] - DOES_PERSONAL_VEHICLE_EXIST_AT_START - Waiting for Personal Vehicle to Spawn.")
			RETURN FALSE				
		ENDIF
		
	ELSE
		#IF IS_DEBUG_BUILD
		IF NOT DOES_ENTITY_EXIST(veh)
			PRINTLN("[InitPosition] - DOES_PERSONAL_VEHICLE_EXIST_AT_START - Safety Timer has expired!!! Was waiting for Vehicle to be created!!!")							
		ELSE
			PRINTLN("[InitPosition] - DOES_PERSONAL_VEHICLE_EXIST_AT_START - Safety Timer has expired!!! Was waiting for Player to be in the Vehicle!!!")			
		ENDIF
		#ENDIF
		
		#IF IS_DEBUG_BUILD
		IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
			ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_START_PV_DID_NOT_SPAWN, ENTITY_RUNTIME_ERROR_TYPE_WARNING, "Personal Vehicle not created at start (timed out) Fail Reason #", g_iFailReason_SavedVehicleAvailableForCreation)
		ELSE
			ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_START_PV_DID_NOT_SPAWN, ENTITY_RUNTIME_ERROR_TYPE_NOTIFICATION, "Personal Vehicle cannot be created at start (freemode not running)")		
		ENDIF
		#ENDIF
		
		RETURN TRUE
	ENDIF
	
	PRINTLN("[InitPosition] - DOES_PERSONAL_VEHICLE_EXIST_AT_START - Safety Timer has expired!!! Vehicle Exists!!!")
	
	IF NOT IS_VECTOR_ZERO(sSetUpStartPosition.vPVStartPosition)
		IF NOT IS_VEHICLE_FUCKED_MP(veh)
			CLEAR_REQUEST_TO_WARP_PERSONAL_VEHICLE_NEAR()
			MPGlobals.VehicleData.bSwitchWarpProcessed = TRUE
			PRINTLN("[InitPosition] - DOES_PERSONAL_VEHICLE_EXIST_AT_START - Using custom start position for Personal Vehicle, ", sSetUpStartPosition.vPVStartPosition, " heading: ", sSetUpStartPosition.fPVStartHeading)
			SET_ENTITY_COORDS(veh, sSetUpStartPosition.vPVStartPosition)
			SET_ENTITY_HEADING(veh, sSetUpStartPosition.fPVStartHeading)
			SET_VEHICLE_ON_GROUND_PROPERLY(veh)
			SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(veh, TRUE)			
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC PROCESS_START_POSITION_STATE_POST_WARP(BOOL bIsQuickRestartJob)
	
	//Create Personal Vehicle, but not spawn in it.
	IF IS_BIT_SET(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_SPAWN_PERSONAL_VEHICLE_AT_START)
	AND NOT IS_BIT_SET(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_SPAWN_IN_PERSONAL_VEHICLE)		
		IF NOT CAN_PERSONAL_VEHICLE_BE_SPAWNED()
			PRINTLN("[InitPosition] - SET_UP_START_POSITION_DATA - Personal Vehicle could not be created. Skipping to Validate.")					
		ELIF NOT DOES_PERSONAL_VEHICLE_EXIST_AT_START()
			EXIT
		ENDIF
	ENDIF
	
	//Personal Vehicle
	IF IS_BIT_SET(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_SPAWN_IN_PERSONAL_VEHICLE)
		
		IF NOT CAN_PERSONAL_VEHICLE_BE_SPAWNED()
			PRINTLN("[InitPosition] - SET_UP_START_POSITION_DATA - Personal Vehicle could not be created. Skipping to Validate.")			
			SET_START_POSITION_WARPING_STATE(START_POSITION_WARPING_STATE_VALIDATE)	
		ELIF START_POSITION_SET_SPAWN_PLAYER_IN_PERSONAL_VEHICLE(sSetUpStartPosition.vStartPosition, sSetUpStartPosition.fStartHeading)
			PRINTLN("[InitPosition] - SET_UP_START_POSITION_DATA - Player has been set into Personal Vehicle")
			SET_START_POSITION_WARPING_STATE(START_POSITION_WARPING_STATE_VALIDATE)
		ELSE
			PRINTLN("[InitPosition] - SET_UP_START_POSITION_DATA - Waiting for Player to be set into Personal Vehicle")
		ENDIF
	
	//Placed Vehicle
	ELIF IS_BIT_SET(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_SPAWN_IN_PLACED_VEHICLE)
		IF START_POSITION_SET_SPAWN_PLAYER_IN_PLACED_VEHICLE(sSetUpStartPosition.iVehicleIndexToSpawnInto, sSetUpStartPosition.iVehicleSeatToSpawnInto, sSetUpStartPosition.vStartPosition, sSetUpStartPosition.fStartHeading)
			PRINTLN("[InitPosition] - SET_UP_START_POSITION_DATA - Player has been set into Placed Vehicle: ", sSetUpStartPosition.iVehicleIndexToSpawnInto)
			SET_START_POSITION_WARPING_STATE(START_POSITION_WARPING_STATE_VALIDATE)				
		ELSE
			PRINTLN("[InitPosition] - SET_UP_START_POSITION_DATA - Waiting for Player to be set into Placed Vehicle: ", sSetUpStartPosition.iVehicleIndexToSpawnInto)
		ENDIF
		
	//Team Vehicle
	ELIF IS_BIT_SET(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_SPAWN_IN_TEAM_VEHICLE)
		IF START_POSITION_SET_SPAWN_PLAYER_IN_TEAM_VEHICLE(sSetUpStartPosition.vStartPosition, sSetUpStartPosition.fStartHeading, bIsQuickRestartJob)
			PRINTLN("[InitPosition] - SET_UP_START_POSITION_DATA - Player has been set into Team Vehicle")
			SET_START_POSITION_WARPING_STATE(START_POSITION_WARPING_STATE_VALIDATE)				
		ELSE
			PRINTLN("[InitPosition] - SET_UP_START_POSITION_DATA - Waiting for Player to be set into Team Vehicle")
		ENDIF
	
	//Nearest Vehicle
	ELIF IS_BIT_SET(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_SET_INTO_NEAREST_VEHICLE)
		PRINTLN("[InitPosition] - SET_UP_START_POSITION_DATA - Calling to place player into nearest placed vehicle.")
		SET_PLAYER_INTO_NEAREST_PLACED_VEHICLE_SIMPLE()
		SET_START_POSITION_WARPING_STATE(START_POSITION_WARPING_STATE_VALIDATE)
	
	//Go To Cover
	ELIF IS_BIT_SET(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_POSITION_ENTER_COVER)
		PRINTLN("[InitPosition] - SET_UP_START_POSITION_DATA - Set LBOOL30_SET_PLAYER_INTO_COVER_START_POSITION - Player should be in Cover.")
		SET_BIT(iLocalBoolCheck30, LBOOL30_SET_PLAYER_INTO_COVER_START_POSITION)
		SET_START_POSITION_WARPING_STATE(START_POSITION_WARPING_STATE_VALIDATE)
		
	ELSE		
		// Some separate team based options to spawn/warp to a particular vehicle. This is NOT controlled by team start points, but usually should be.
		IF IS_WARPING_INTO_PLACED_VEH()
			PRINTLN("[InitPosition] - START_POSITION_SET - waiting for IS_WARPING_INTO_PLACED_VEH")	
			EXIT
		ENDIF
		
		SET_START_POSITION_WARPING_STATE(START_POSITION_WARPING_STATE_VALIDATE)		
	ENDIF
ENDPROC

FUNC BOOL LOCAL_PLAYER_HAS_CONTROL_OF_VEHICLE_AS_DRIVER()
	
	BOOL bDriver
	VEHICLE_INDEX vehIndex
	IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		vehIndex = GET_VEHICLE_PED_IS_IN(LocalPlayerPed, TRUE)
		bDriver = TRUE
	ENDIF
		
	IF NOT IS_VEHICLE_DRIVEABLE(vehIndex)
		vehIndex = GET_ASSOCIATED_SPAWN_POINT_START_VEHICLE()
		bDriver = FALSE
	ENDIF
	
	IF NOT IS_VEHICLE_DRIVEABLE(vehIndex)
		vehIndex = GET_SPAWN_POINT_START_VEHICLE_TO_CLONE_TO()
		bDriver = FALSE
	ENDIF	
	
	IF NOT IS_PED_INJURED(LocalPlayerPed)
	AND IS_VEHICLE_DRIVEABLE(vehIndex)
		
		IF GET_PED_IN_VEHICLE_SEAT(vehIndex, VS_DRIVER) != LocalPlayerPed
		AND bDriver
			PRINTLN("[InitPosition] - START_POSITION_SET - LOCAL_PLAYER_HAS_CONTROL_OF_VEHICLE_AS_DRIVER - In a vehicle but we are not a driver.")
			RETURN TRUE
		ENDIF
		
		IF IS_VEHICLE_DRIVEABLE(vehIndex)
			IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(vehIndex)
				
				IF NOT IS_BIT_SET(iLocalBoolCheck30, LBOOL30_REQUESTED_MIGRATION_RIGHTS_RELEASED_FROM_HOST_FOR_VEH)
					INT iVeh = IS_ENTITY_A_MISSION_CREATOR_ENTITY(vehIndex)
					IF iVeh > -1
						BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_RequestVehicleCanMigrate, iVeh, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, FALSE)
					ENDIF
					
					SET_BIT(iLocalBoolCheck30, LBOOL30_REQUESTED_MIGRATION_RIGHTS_RELEASED_FROM_HOST_FOR_VEH)
				ENDIF
				
				NETWORK_REQUEST_CONTROL_OF_ENTITY(vehIndex)
				PRINTLN("[InitPosition] - START_POSITION_SET - LOCAL_PLAYER_HAS_CONTROL_OF_VEHICLE_AS_DRIVER - Waiting for Network Control.")
				RETURN FALSE
			ENDIF		
		ENDIF
		
	ELSE
		PRINTLN("[InitPosition] - START_POSITION_SET - LOCAL_PLAYER_HAS_CONTROL_OF_VEHICLE_AS_DRIVER - Vehicle does not exist or is damaged.")
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC SET_MISSION_SPAWN_POINT_STARTING_VEHICLE()
	
	IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed, TRUE)
		sSetUpStartPosition.vehStartingVehicle = GET_VEHICLE_PED_IS_IN(LocalPlayerPed, TRUE)
		PRINTLN("[InitPosition] - START_POSITION_SET - SET_MISSION_SPAWN_POINT_STARTING_VEHICLE - sSetUpStartPosition.vehStartingVehicle assigned from vehicle we are in.")
	ENDIF
		
	IF NOT DOES_ENTITY_EXIST(sSetUpStartPosition.vehStartingVehicle)
	OR NOT IS_VEHICLE_DRIVEABLE(sSetUpStartPosition.vehStartingVehicle)
		sSetUpStartPosition.vehStartingVehicle = GET_ASSOCIATED_SPAWN_POINT_START_VEHICLE()
		PRINTLN("[InitPosition] - START_POSITION_SET - SET_MISSION_SPAWN_POINT_STARTING_VEHICLE - sSetUpStartPosition.vehStartingVehicle assigned from vehicle we are associated with.")
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(sSetUpStartPosition.vehStartingVehicle)
	OR NOT IS_VEHICLE_DRIVEABLE(sSetUpStartPosition.vehStartingVehicle)
		sSetUpStartPosition.vehStartingVehicle = GET_SPAWN_POINT_START_VEHICLE_TO_CLONE_TO()
		PRINTLN("[InitPosition] - START_POSITION_SET - SET_MISSION_SPAWN_POINT_STARTING_VEHICLE - sSetUpStartPosition.vehStartingVehicle assigned from vehicle we are cloning to.")
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(sSetUpStartPosition.vehStartingVehicle)
	OR NOT IS_VEHICLE_DRIVEABLE(sSetUpStartPosition.vehStartingVehicle)
		sSetUpStartPosition.vehStartingVehicle = PERSONAL_VEHICLE_ID()
		PRINTLN("[InitPosition] - START_POSITION_SET - SET_MISSION_SPAWN_POINT_STARTING_VEHICLE - sSetUpStartPosition.vehStartingVehicle assigned from Personal Vehicle.")
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(sSetUpStartPosition.vehStartingVehicle)
	OR NOT IS_VEHICLE_DRIVEABLE(sSetUpStartPosition.vehStartingVehicle)
		EXIT
	ENDIF
		
	IF IS_BIT_SET(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_RENABLE_PERSONAL_VEHICLES_IF_START_VEHICLE_IS_GONE)
		PRINTLN("[InitPosition] - START_POSITION_SET - SET_MISSION_SPAWN_POINT_STARTING_VEHICLE - Personal Vehicles will be enabled when our starting vehicle is destroyed or cleaned up.")	
	ENDIF
	
ENDPROC

PROC PROCESS_START_POSITION_WARPING_STATE_VALIDATE()
	IF GET_STRAND_MISSION_TRANSITION_TYPE() = FMMC_BRANCHING_MISSION_TRANSITION_TYPE_FADEOUT
		IF IS_BIT_SET(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_CHECK_INTERIOR)
			INTERIOR_INSTANCE_INDEX Interior_SpawnAtStart_ID
			Interior_SpawnAtStart_ID = GET_INTERIOR_AT_COORDS(sSetUpStartPosition.vStartPosition)
			IF NOT IS_VALID_INTERIOR(Interior_SpawnAtStart_ID)
				PRINTLN("[InitPosition] - START_POSITION_SET - STRAND INTRO Waiting for Interior ", NATIVE_TO_INT(Interior_SpawnAtStart_ID), " to load in...")
				EXIT
			ENDIF
		ENDIF
	ENDIF
	
	// Check if we are in a vehicle and we are the driver. We need to grab control from the host.
	IF NOT LOCAL_PLAYER_HAS_CONTROL_OF_VEHICLE_AS_DRIVER()
		PRINTLN("[InitPosition] - START_POSITION_SET - Waiting for LOCAL_PLAYER_HAS_CONTROL_OF_VEHICLE_AS_DRIVER")
		EXIT
	ENDIF
	
	SET_SPECIAL_MODS_TO_VEHICLE_BEFORE_START()
			
	SET_MISSION_SPAWN_POINT_STARTING_VEHICLE()

	SET_START_POSITION_WARPING_STATE(START_POSITION_WARPING_STATE_ADJUST_VEHICLE)
ENDPROC

FUNC BOOL SHOULD_START_POSITION_USE_A_LOAD_SCENE()
	RETURN IS_BIT_SET(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_POSITION_USES_VEH_POSITION) AND NOT IS_THIS_A_QUICK_RESTART_JOB()
ENDFUNC

PROC PROCESS_START_POSITION_WARPING_STATE_ADJUST_VEHICLE(BOOL bQuickWarp)

	IF NOT WARP_START_POSITION_ON_VEHICLE_POST_ENTRY(bQuickWarp)
		PRINTLN("[InitPosition] - START_POSITION_SET - Waiting for WARP_START_POSITION_ON_VEHICLE_POST_ENTRY")
		EXIT
	ENDIF
		
	IF SHOULD_START_POSITION_USE_A_LOAD_SCENE()
	AND NOT IS_BIT_SET(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_LOAD_SCENE_STARTED)
		IF IS_NEW_LOAD_SCENE_ACTIVE()
			PRINTLN("[InitPosition] - START_POSITION_SET - Stopping old load scene.")
			NEW_LOAD_SCENE_STOP()
		ENDIF		
		NEW_LOAD_SCENE_START_SPHERE(GET_ENTITY_COORDS(LocalPlayerPed, FALSE), 200.0)
		SET_BIT(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_LOAD_SCENE_STARTED)
	ENDIF
	
	IF IS_BIT_SET(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_LOAD_SCENE_STARTED)
		IF IS_NEW_LOAD_SCENE_ACTIVE()
			IF NOT IS_NEW_LOAD_SCENE_LOADED()
				PRINTLN("[InitPosition] - START_POSITION_SET - Waiting for Load Scene to finish.")
				EXIT
			ELSE
				PRINTLN("[InitPosition] - START_POSITION_SET - Stopping the new load scene as it's finished.")
				NEW_LOAD_SCENE_STOP()
			ENDIF
		ENDIF
	ENDIF
		
	SET_START_POSITION_WARPING_VEHICLE_VISIBLE()
	
	SET_START_POSITION_WARPING_VEHICLE_ON_GROUND_PROPERLY()
	
	SET_START_POSITION_WARPING_STATE(START_POSITION_WARPING_STATE_FINISHED)
ENDPROC

// [FIX_2020_CONTROLLER] - A lot of the processing in here makes SET_UP_START_POSITION_DATA redundant. Could be worth making new content use a singular function that processes all this data in a much better and more readable format. This function can be a mess to debug sometimes.
FUNC BOOL START_POSITION_SET()
			
	FLOAT fGroundz
	VECTOR vPlayerCoords
	
	PRINTLN("[InitPosition] - START_POSITION_SET - being called.")
	
	BOOL bFailSafeActive
	BOOL bIsQuickRestartJob = IS_THIS_A_QUICK_RESTART_JOB()	
	
	//get out if strand mission int
	IF sSetUpStartPosition.iStartRule < FMMC_MAX_RULES
		IF (NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitsetSeventeen, ciENABLE_USE_START_POS_ON_HEIST_STRAND) OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[sSetUpStartPosition.iPlayerTeam].iRuleBitsetFourteen[sSetUpStartPosition.iStartRule], ciBS_RULE14_IGNORE_START_POSITION_ON_THIS_RULE))
		AND NOT g_bStartedInFadeTransition
			IF IS_A_STRAND_MISSION_BEING_INITIALISED()
			OR STRAND_MISSION_SHOULD_SKIP_INTRO_IN_MC()
				PRINTLN("[InitPosition] - START_POSITION_SET - ciENABLE_USE_START_POS_ON_HEIST_STRAND not set Returning true.")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(ilocalboolcheck2, LBOOL2_MISSION_START_STAY_STILL)
		PRINTLN("[InitPosition] - START_POSITION_SET - LBOOL2_MISSION_START_STAY_STILL bit is set.")
		IF DOES_ENTITY_EXIST(LocalPlayerPed)
			IF NOT IS_ENTITY_DEAD(LocalPlayerPed)
				IF NOT IS_PED_IN_ANY_VEHICLE_OR_WAITING_TO_START_TASK_ENTER_VEHICLE(LocalPlayerPed)
					vPlayerCoords = GET_ENTITY_COORDS(LocalPlayerPed)
					PRINTLN("[InitPosition] - START_POSITION_SET - player coords = ", vPlayerCoords)
					IF GET_GROUND_Z_FOR_3D_COORD(vPlayerCoords, fGroundz)
						PRINTLN("[InitPosition] - START_POSITION_SET - fGroundz from player coords = ", fGroundz)
						vPlayerCoords.z = fGroundz
						SET_ENTITY_COORDS(LocalPlayerPed, vPlayerCoords)
						PRINTLN("[InitPosition] - START_POSITION_SET - set player coords to ", vPlayerCoords)
					ELSE
						PRINTLN("[InitPosition] - START_POSITION_SET - fGroundz from player coords.")
					ENDIF
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciSPAWN_IN_COVER)
						SET_BIT(iLocalBoolCheck30, LBOOL30_SET_PLAYER_INTO_COVER_START_POSITION)
						PRINTLN("[InitPosition] - START_POSITION_SET - ciSPAWN_IN_COVER bit is set. Setting LBOOL30_SET_PLAYER_INTO_COVER_START_POSITION (1)")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		PRINTLN("[InitPosition] - START_POSITION_SET - Staying Still - Returning True.")
		RETURN TRUE
	ENDIF
	
	IF sSetUpStartPosition.iStartRule < FMMC_MAX_RULES
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitsetSeventeen, ciENABLE_USE_START_POS_ON_HEIST_STRAND)
		OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[sSetUpStartPosition.iPlayerTeam].iRuleBitsetFourteen[sSetUpStartPosition.iStartRule], ciBS_RULE14_IGNORE_START_POSITION_ON_THIS_RULE)
			IF NOT bIsQuickRestartJob
				IF HAS_FIRST_STRAND_MISSION_BEEN_PASSED()
					PRINTLN("[InitPosition] - START_POSITION_SET - HAS_FIRST_STRAND_MISSION_BEEN_PASSED = TRUE.")
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT HAS_NET_TIMER_STARTED(stStartPosSetFailSafe)
		START_NET_TIMER(stStartPosSetFailSafe)
	ELSE
		IF HAS_NET_TIMER_EXPIRED(stStartPosSetFailSafe, 45000)
			IF NOT IS_BIT_SET(ilocalboolcheck2, LBOOL2_SPAWN_SAFETY_FALLBACK)
				NEW_LOAD_SCENE_STOP()
				SET_BIT(ilocalboolcheck2, LBOOL2_SPAWN_SAFETY_FALLBACK)
				PRINTLN("[InitPosition] - START_POSITION_SET - set bit LBOOL2_SPAWN_SAFETY_FALLBACK.")
			ENDIF			
			bFailSafeActive = TRUE
			
			#IF IS_DEBUG_BUILD
			ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_SPAWNING_FINISHED_EARLY_FALLBACK, ENTITY_RUNTIME_ERROR_TYPE_WARNING_VEH, "Spawning got stuck and had to finish early. Waited # miliseconds.", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stStartPosSetFailSafe))
			#ENDIF
			
		ENDIF
	ENDIF
	
	IF bFailSafeActive
		PRINTLN("[InitPosition] - START_POSITION_SET - bFailSafeActive = TRUE.")
		IF NOT USING_MULTIPLE_PASSENGER_TEAM_VEHICLE(MC_playerBD[ iLocalPart].iteam) 
		OR IS_DRIVER_FOR_MULTIPLE_TEAM_VEHICLE(MC_playerBD[ iLocalPart].iteam)
				IF NET_WARP_TO_COORD( 	g_FMMC_STRUCT.sFMMCEndConditions[ MC_playerBD[iLocalPart].iteam].vStartPos,
									g_FMMC_STRUCT.sFMMCEndConditions[ MC_playerBD[iLocalPart].iteam].fStartHeading, 
									TRUE, 
									FALSE, 
									DEFAULT, 
									DEFAULT, 
									DEFAULT, 
									bIsQuickRestartJob OR IS_PLAYER_USING_SEQUENTIAL_SPAWNING_PROCEDURES())
				
				RETURN TRUE
			ENDIF
		ELSE
			PRINTLN("[InitPosition] - START_POSITION_SET - We are a Passenger of a Team Respawn Vehicle. Do not warp. ")
			RETURN SPAWN_PLAYER_IN_TEAM_VEHICLE( GET_ENTITY_COORDS( LocalPlayerPed ), GET_ENTITY_HEADING( LocalPlayerPed ) )
		ENDIF
	ENDIF
	
	IF bIsQuickRestartJob
		IF DOES_ENTITY_EXIST(LocalPlayerPed)
			IF NOT IS_ENTITY_DEAD(LocalPlayerPed)
				IF NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)				
					IF IS_PED_CLIMBING(LocalPlayerPed)
						CLEAR_PED_TASKS_IMMEDIATELY(LocalPlayerPed)
					ENDIF
					IF NOT IS_ENTITY_ATTACHED(LocalPlayerPed)
						FREEZE_ENTITY_POSITION(LocalPlayerPed, TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	BOOL bQuickWarp = bIsQuickRestartJob OR IS_PLAYER_USING_SEQUENTIAL_SPAWNING_PROCEDURES()
		
	SWITCH sSetUpStartPosition.eStartPositionWarpingState
		CASE START_POSITION_WARPING_STATE_INIT
			
			// Vehicles sometimes get hit by traffic if traffic is not already being blocked as they spawn too close to oncoming vehicles.
			PRINTLN("[InitPosition][Traffic] - START_POSITION_SET - Clearing Traffic before vehicle spawning.")
			MC_CLEAR_PED_AND_TRAFFIC_DENSITIES(iPopulationHandle, bVehicleGenerator)
			MC_SET_PED_AND_TRAFFIC_DENSITIES(iPopulationHandle, bVehicleGenerator, 0.0, 0.0)		
					
			IF IS_BIT_SET(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_POSITION_USING_CONTINUITY)
				PROCESS_START_POSITION_WARPING_STATE_INIT_CONTINUITY()
			
			ELIF IS_BIT_SET(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_POSITION_USING_CUSTOM_START_POINT_CHECKPOINTS)
				PROCESS_START_POSITION_WARPING_STATE_INIT_START_POINT_CHECKPOINTS()
				
			ELIF IS_BIT_SET(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_POSITION_USING_CHECKPOINT)
				PROCESS_START_POSITION_WARPING_STATE_INIT_CHECKPOINTS()
				
			ELIF IS_BIT_SET(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_POSITION_USING_CUSTOM_START_POINTS)
				PROCESS_START_POSITION_WARPING_STATE_INIT_CUSTOM_START_POINTS()
			
			ELIF IS_BIT_SET(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_POSITION_USING_TEAM_POINTS_SIMPLE)
				PROCESS_START_POSITION_WARPING_STATE_INIT_TEAM_START_POINTS_SIMPLE()
				
			ELIF IS_BIT_SET(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_POSITION_USING_SINGLE_START_POS)
				PROCESS_START_POSITION_WARPING_STATE_INIT_SINGLE_START_POINT()
				
			ENDIF
			
			PRINTLN("[InitPosition] - START_POSITION_SET - Details - iPlayerTeam: ", sSetUpStartPosition.iPlayerTeam, " iPlayerTeamSlot: ", sSetUpStartPosition.iPlayerTeamSlot, " vStartPosition: ", sSetUpStartPosition.vStartPosition, " fStartHeading: ", sSetUpStartPosition.fStartHeading, " iVehicleIndexToSpawnInto: ", sSetUpStartPosition.iVehicleIndexToSpawnInto, " iVehicleSeatToSpawnInto: ", sSetUpStartPosition.iVehicleSeatToSpawnInto)
		BREAK
		
		CASE START_POSITION_WARPING_STATE_WARP
			PROCESS_START_POSITION_WARPING_STATE_WARP(bQuickWarp)
		BREAK
				
		CASE START_POSITION_WARPING_STATE_POST_WARP
			PROCESS_START_POSITION_STATE_POST_WARP(bIsQuickRestartJob)
		BREAK
		
		CASE START_POSITION_WARPING_STATE_VALIDATE
			PROCESS_START_POSITION_WARPING_STATE_VALIDATE()
		BREAK
		
		CASE START_POSITION_WARPING_STATE_ADJUST_VEHICLE
			PROCESS_START_POSITION_WARPING_STATE_ADJUST_VEHICLE(bQuickWarp)
		BREAK
	
		CASE START_POSITION_WARPING_STATE_FINISHED
			CLEANUP_HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS(TRUE)
			SetPlayerOnGroundProperly()
			SetGameCamBehindPlayer(0, TRUE)
			
			PRINTLN("[InitPosition] - START_POSITION_SET - Returning True. Finished.")	
			RETURN TRUE			
		BREAK
	ENDSWITCH
	
	PRINTLN("[InitPosition] - START_POSITION_SET - Returning False. Not Finished Yet.")
	
	RETURN FALSE
	
ENDFUNC


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Init Server
// ##### Description: Functions called by INIT_SERVER_DATA from main
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
PROC PROCESS_SERVER_NON_RESET_TRANSITION_VARS_INITIALISATION()

	INT iTeam
	FOR iTeam = 0 TO FMMC_MAX_TEAMS - 1
		
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iTeamBitset4, ciBS4_TEAM_PER_PLAYER_THERMAL_CHARGES)
			IF (IS_THIS_IS_A_STRAND_MISSION())
			OR ((IS_THIS_A_QUICK_RESTART_JOB() OR IS_CORONA_INITIALISING_A_QUICK_RESTART()) AND NOT IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_ClearOnQuickRestart))
				MC_serverBD_1.iTeamThermalCharges[iTeam] = g_sTransitionSessionData.sStrandMissionData.iTeamThermalCharges[iTeam]
				PRINTLN("[INIT_SERVER] PROCESS_SERVER_NON_RESET_TRANSITION_VARS_INITIALISATION - Team ", iTeam, " Thermal Charges (Transition Vars): ", MC_serverBD_1.iTeamThermalCharges[iTeam])
			ELSE	
				MC_serverBD_1.iTeamThermalCharges[iTeam] = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamThermalCharges
				PRINTLN("[INIT_SERVER] PROCESS_SERVER_NON_RESET_TRANSITION_VARS_INITIALISATION - Team ", iTeam, " Thermal Charges (Creator): ", MC_serverBD_1.iTeamThermalCharges[iTeam])
			ENDIF
		ENDIF
		
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySeven, ciOptionsBS27_KeepRandomNextObjectiveSeed)
			IF g_TransitionSessionNonResetVars.iRandomNextObjectiveSeed[iTeam] != 0
			AND (IS_THIS_A_QUICK_RESTART_JOB() OR IS_CORONA_INITIALISING_A_QUICK_RESTART())
				MC_serverBD_1.iRandomNextObjectiveSeed[iTeam] = g_TransitionSessionNonResetVars.iRandomNextObjectiveSeed[iTeam]
				PRINTLN("[INIT_SERVER] PROCESS_SERVER_NON_RESET_TRANSITION_VARS_INITIALISATION - Team ", iTeam, " iRandomNextObjectiveSeed (transition vars): ", MC_serverBD_1.iRandomNextObjectiveSeed[iTeam])
			ELSE
				MC_serverBD_1.iRandomNextObjectiveSeed[iTeam] = (MC_serverBD.iSessionScriptEventKey + iTeam)
				PRINTLN("[INIT_SERVER] PROCESS_SERVER_NON_RESET_TRANSITION_VARS_INITIALISATION - Team ", iTeam, " iRandomNextObjectiveSeed: ", MC_serverBD_1.iRandomNextObjectiveSeed[iTeam])
			ENDIF
		ENDIF
		g_TransitionSessionNonResetVars.iRandomNextObjectiveSeed[iTeam] = 0
		
	ENDFOR
	
	IF g_TransitionSessionNonResetVars.iDoNotRespawnVehBitSet != 0
		IF IS_THIS_A_QUICK_RESTART_JOB()
		OR IS_CORONA_INITIALISING_A_QUICK_RESTART()
			MC_serverBD.iCleanedUpUndeliveredVehiclesBS = g_TransitionSessionNonResetVars.iDoNotRespawnVehBitSet 
			PRINTLN("[INIT_SERVER] PROCESS_SERVER_NON_RESET_TRANSITION_VARS_INITIALISATION - Do Not Respawn Vehicle Bitset: ", MC_serverBD.iCleanedUpUndeliveredVehiclesBS)
		ENDIF
	ENDIF
	
		
	IF NOT HAS_FIRST_STRAND_MISSION_BEEN_PASSED_NO_RESET()
	AND NOT HAS_FIRST_STRAND_MISSION_BEEN_PASSED()
	AND NOT IS_THIS_A_QUICK_RESTART_JOB() 
	AND NOT IS_CORONA_INITIALISING_A_QUICK_RESTART()	
		SET_RANDOM_SEED(NATIVE_TO_INT(GET_NETWORK_TIME_ACCURATE()))
		MC_serverBD_1.sMissionContinuityVars.iContinuitySeed = GET_RANDOM_INT_IN_RANGE()
		PRINTLN("[INIT_SERVER] PROCESS_SERVER_NON_RESET_TRANSITION_VARS_INITIALISATION - New mission - setting Continuity Seed: ", MC_serverBD_1.sMissionContinuityVars.iContinuitySeed)
	ENDIF
	
ENDPROC

PROC PROCESS_SERVER_TEAM_NUMBER_INITIALISATION()

	IF IS_FAKE_MULTIPLAYER_MODE_SET()
		MC_serverBD.iNumberOfTeams = g_FMMC_STRUCT.iTestNumberOfTeams
		PRINTLN("[INIT_SERVER] PROCESS_SERVER_TEAM_NUMBER_INITIALISATION - Number of teams (Test Mode): ", MC_serverBD.iNumberOfTeams)
		SET_BIT(MC_serverbd.iServerBitSet, SBBOOL_TEAM_0_ACTIVE + g_FMMC_STRUCT.iTestMyTeam)
		SET_BIT(MC_serverbd.iServerBitSet1, SBBOOL1_TEAM_0_WAS_ACTIVE + g_FMMC_STRUCT.iTestMyTeam)
	ELSE
		MC_serverBD.iNumberOfTeams = g_FMMC_STRUCT.iNumberOfTeams   
		PRINTLN("[INIT_SERVER] PROCESS_SERVER_TEAM_NUMBER_INITIALISATION - Number of teams: ", MC_serverBD.iNumberOfTeams)
	ENDIF
	
	MC_serverBD.iNumberOfPlayingTeams = MC_serverBD.iNumberOfTeams
	
	IF MC_serverBD.iNumberOfTeams <= 0
	OR MC_serverBD.iNumberOfTeams > FMMC_MAX_TEAMS
		ASSERTLN("[INIT_SERVER] PROCESS_SERVER_TEAM_NUMBER_INITIALISATION - Number of teams invalid - TERMINATING SCRIPT")
		#IF IS_DEBUG_BUILD
		ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_INVALID_TEAMS_FROM_LOBBY, ENTITY_RUNTIME_ERROR_TYPE_CRITICAL, "Number of teams invalid - TERMINATING SCRIPT")
		#ENDIF
		NETWORK_BAIL(ENUM_TO_INT(NETWORK_BAIL_TS_SAFETY_BAIL))
	ENDIF
	
ENDPROC

PROC SET_WEATHER_SERVER_DATA()
	
	IF MC_serverBD_1.sMissionContinuityVars.iWeather != -1
		
		MC_serverBD.iWeather = MC_serverBD_1.sMissionContinuityVars.iWeather
		PRINTLN("[INIT_SERVER][CONTINUITY] PROCESS_SERVER_CORONA_SETTINGS_INITIALISATION - SET_WEATHER_SERVER_DATA - Weather from MC_serverBD_1.sMissionContinuityVars.iWeather")
	
	ELSE
	
		MC_serverBD.iWeather = g_FMMC_STRUCT.iWeather
	
		IF g_FMMC_STRUCT.iAltWeather != 0
		AND g_FMMC_STRUCT.iAltWeatherChance != 0
		AND NOT IS_THIS_A_QUICK_RESTART_JOB()
			MC_serverBD.iWeather = g_FMMC_STRUCT.iWeather
			INT iWeatherChance = GET_RANDOM_INT_IN_RANGE(0, 1001)
			PRINTLN("[INIT_SERVER] PROCESS_SERVER_CORONA_SETTINGS_INITIALISATION - SET_WEATHER_SERVER_DATA- iWeatherChance: ", iWeatherChance)
			IF iWeatherChance < (g_FMMC_STRUCT.iAltWeatherChance * 10)
				MC_serverBD.iWeather = g_FMMC_STRUCT.iAltWeather
				PRINTLN("[INIT_SERVER] PROCESS_SERVER_CORONA_SETTINGS_INITIALISATION - SET_WEATHER_SERVER_DATA - Weather from g_FMMC_STRUCT.iAltWeather")
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_WeatherTracking)
		AND MC_ServerBD_1.sMissionContinuityVars.iWeather = -1
			MC_ServerBD_1.sMissionContinuityVars.iWeather = MC_serverBD.iWeather
			PRINTLN("[CONTINUITY] - SET_WEATHER_SERVER_DATA - MC_ServerBD_1.sMissionContinuityVars.iWeather set to ", MC_ServerBD_1.sMissionContinuityVars.iWeather)
		ENDIF
		
	ENDIF
	
	PRINTLN("[INIT_SERVER] PROCESS_SERVER_CORONA_SETTINGS_INITIALISATION - SET_WEATHER_SERVER_DATA - Weather: ", MC_serverBD.iWeather)
ENDPROC

PROC PROCESS_SERVER_CORONA_SETTINGS_INITIALISATION()

	SET_WEATHER_SERVER_DATA()	

	MC_serverBD.iTimeOfDay = g_FMMC_STRUCT.iTimeOfDay	
	PRINTLN("[INIT_SERVER] PROCESS_SERVER_CORONA_SETTINGS_INITIALISATION - Time of Day: ", MC_serverBD.iTimeOfDay)
	
	SET_BIT(MC_serverbd.iServerBitSet6, SBBOOL6_UPDATED_WEATHER_AND_TIME_FROM_CORONA)
	
	MC_serverBD.iPolice = g_FMMC_STRUCT.iPolice
	PRINTLN("[INIT_SERVER] PROCESS_SERVER_CORONA_SETTINGS_INITIALISATION - Police: ", MC_serverBD.iPolice)
	
	MC_serverBD.iDifficulty = g_FMMC_STRUCT.iDifficulity
	PRINTLN("[INIT_SERVER] PROCESS_SERVER_CORONA_SETTINGS_INITIALISATION - Difficulty: ", MC_serverBD.iDifficulty)
	
	MC_serverBD.fVehicleDensity = GET_MISSION_VEHICLE_DENSITY_FROM_INT(g_FMMC_STRUCT.iTraffic)
	PRINTLN("[INIT_SERVER] PROCESS_SERVER_CORONA_SETTINGS_INITIALISATION - Vehicle Density: ", MC_serverBD.fVehicleDensity)
	
	MC_serverBD.fPedDensity = GET_PED_DENSITY_FROM_INT(g_FMMC_STRUCT.iPedDensity)
	PRINTLN("[INIT_SERVER] PROCESS_SERVER_CORONA_SETTINGS_INITIALISATION - Ped Density: ", MC_serverBD.fPedDensity)
	
	MC_serverBD.iOptionsMenuBitSet = g_FMMC_STRUCT.iOptionsMenuBitSet
	PRINTLN("[INIT_SERVER] PROCESS_SERVER_CORONA_SETTINGS_INITIALISATION - Options Bitset: ", MC_serverBD.fPedDensity)
					
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitsetTen, ciMODE_RULE_BASED_SCORE_SCALING) 
		MC_ServerBD_4.iTargetScoreMultiplierSetting = g_FMMC_STRUCT.iTargetScoreMultiplierSetting
		PRINTLN("[INIT_SERVER] PROCESS_SERVER_CORONA_SETTINGS_INITIALISATION - Target Score Multiplier: ", MC_ServerBD_4.iTargetScoreMultiplierSetting)
	ENDIF
	
ENDPROC

DEBUGONLY PROC PROCESS_SERVER_DEBUG_INITIALISATION()

	#IF IS_DEBUG_BUILD
		IF g_bIgnoreOutOfLivesFailure
			SET_BIT(MC_serverBD.iDebugBitset, SBDEBUG_DONTFAILWHENOUTOFLIVES)
			PRINTLN("[INIT_SERVER] PROCESS_SERVER_DEBUG_INITIALISATION - DONT FAIL WHEN OUT OF LIVES - ON")
		ENDIF
	#ENDIF
	
ENDPROC

PROC PROCESS_SERVER_PARTICIPANT_INITIALISATION()
	
	INT i
	FOR i = 0 TO MAX_NUM_MC_PLAYERS - 1
		
		IF (NOT HAS_FIRST_STRAND_MISSION_BEEN_PASSED() AND NOT HAS_FIRST_STRAND_MISSION_BEEN_PASSED_NO_RESET())
		AND NOT IS_BIT_SET(g_MissionControllerserverBD_LB.sleaderboard[i].iLbdBitSet, ciLBD_BIT_INITIALISE)
			g_MissionControllerserverBD_LB.sleaderboard[i].playerID 		= INVALID_PLAYER_INDEX()
			g_MissionControllerserverBD_LB.sleaderboard[i].iParticipant 	= -1
			g_MissionControllerserverBD_LB.sleaderboard[i].iTeam            = -1
			SET_BIT(g_MissionControllerserverBD_LB.sleaderboard[i].iLbdBitSet, ciLBD_BIT_INITIALISE)
		ENDIF
		
		IF IS_FAKE_MULTIPLAYER_MODE_SET()
			RELOOP
		ENDIF
		
		IF MC_serverBD.iNumberOfTeams <= 0
			RELOOP
		ENDIF
		
		IF HAS_FIRST_STRAND_MISSION_BEEN_PASSED()
		OR HAS_FIRST_STRAND_MISSION_BEEN_PASSED_NO_RESET()
			MC_serverBD.iNumPlayerDeaths[i] = MC_Playerbd[i].iNumPlayerDeaths
			PRINTLN("[Players][Participant: ", i, "]  PROCESS_SERVER_PARTICIPANT_INITIALISATION -  deaths increased from prev mission to: ", MC_serverBD.iNumPlayerDeaths[i])
		ENDIF

		PARTICIPANT_INDEX tempPart = INT_TO_PARTICIPANTINDEX(i)
		IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
			RELOOP
		ENDIF
			
		PLAYER_INDEX tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
				
		IF NOT IS_NET_PLAYER_OK(tempPlayer, FALSE, FALSE)
			RELOOP
		ENDIF
		
		IF IS_PLAYER_SCTV(tempPlayer)
		OR DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(tempPlayer)
			RELOOP
		ENDIF
		
		INT iPlayer = NATIVE_TO_INT(tempPlayer)
		IF iPlayer < 0
		AND iPlayer >= MAX_NUM_MC_PLAYERS
			RELOOP
		ENDIF				
		
		INT iTeam = GlobalPlayerBD_FM[iPlayer].sClientCoronaData.iTeamChosen
						
		IF iTeam < 0
		AND iTeam >= MC_serverBD.iNumberOfTeams
			RELOOP
		ENDIF
		
		IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_TEAM_0_ACTIVE + iTeam)
		OR NOT IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_TEAM_0_WAS_ACTIVE + iTeam)
			PRINTLN("[INIT_SERVER] PROCESS_SERVER_PARTICIPANT_INITIALISATION - Found player (", i, ") on Team: ", iTeam, " setting as active!")
			SET_BIT(MC_serverBD.iServerBitSet, SBBOOL_TEAM_0_ACTIVE + iTeam)
			SET_BIT(MC_serverBD.iServerBitSet1, SBBOOL1_TEAM_0_WAS_ACTIVE + iTeam)
		ENDIF	
		
	ENDFOR
	
ENDPROC

PROC PROCESS_SERVER_START_POS_CLEAR_AREA_FOR_TEAM(INT iTeam)
	
	IF NOT NETWORK_IS_ACTIVITY_SESSION()
		EXIT
	ENDIF
	
	IF IS_VECTOR_ZERO(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vStartPos)
		EXIT
	ENDIF
	
	BOOL bClearAreaLeaveVehicleHealth = TRUE
	IF iTeam > 0
		INT i
		FOR i = 0 TO iTeam - 1
			IF ARE_VECTORS_EQUAL(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vStartPos, g_FMMC_STRUCT.sFMMCEndConditions[i].vStartPos)
				bClearAreaLeaveVehicleHealth = FALSE					
				BREAKLOOP
			ENDIF
		ENDFOR
	ENDIF
			
	IF !bClearAreaLeaveVehicleHealth
		EXIT
	ENDIF
	
	PRINTLN("[INIT_SERVER] PROCESS_SERVER_START_POS_CLEAR_AREA_FOR_TEAM - Calling Clear Area for Team: ", iTeam)
	CLEAR_AREA_LEAVE_VEHICLE_HEALTH(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vStartPos, 9999, TRUE, FALSE, FALSE, TRUE)
	
ENDPROC

PROC PROCESS_SERVER_LOCATION_PRE_PRIORITY_INITIALISATION_FOR_TEAM(INT iTeam, INT &iLocationBS)
	
	INT i
	FOR i = 0 TO FMMC_MAX_GO_TO_LOCATIONS - 1
		
		MC_serverBD.iLocOwner[i] = -1
				
		IF MC_DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP(i, eSGET_GotoLoc)
			
			MC_serverBD_4.iGotoLocationDataPriority[i][iTeam] = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].iPriority[iTeam]
			MC_serverBD_4.iGotoLocationDataRule[i][iTeam] = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].iRule[iTeam]
			
			IF MC_serverBD_4.iGotoLocationDataPriority[i][iTeam] < FMMC_MAX_RULES
				IF MC_serverBD_4.iGotoLocationDataPriority[i][iTeam] <= MC_serverBD_4.iCurrentHighestPriority[iTeam] 
					MC_serverBD_4.iCurrentHighestPriority[iTeam] = MC_serverBD_4.iGotoLocationDataPriority[i][iTeam]
				ENDIF
				SET_SERVER_TARGETTYPES_FOR_ENTITY(i, iTeam, ci_TARGET_LOCATION)
			ENDIF
			PRINTLN("CLIENT_PRE_DEFINE_SERVER_DATA - MC_serverBD_4.iGotoLocationDataRule[",i,"][",iTeam,"] = ", MC_serverBD_4.iGotoLocationDataRule[i][iTeam], " MC_serverBD_4.iGotoLocationDataPriority[",i,"][",iTeam,"]: ", MC_serverBD_4.iGotoLocationDataPriority[i][iTeam], " Spawn Group is okay.")
		ELSE
			CLEAR_BIT(MC_serverBD.iLocteamFailBitset[i], iTeam)
			MC_serverBD_4.iGotoLocationDataPriority[i][iTeam] = FMMC_PRIORITY_IGNORE
			MC_serverBD_4.iGotoLocationDataRule[i][iTeam] = FMMC_OBJECTIVE_LOGIC_NONE
		ENDIF
		
		PRINTLN("[INIT_SERVER] PROCESS_SERVER_LOCATION_INITIALISATION_FOR_TEAM - Team ", iTeam, " Loc ", i, " Priority: ", MC_serverBD_4.iGotoLocationDataPriority[i][iTeam])
		PRINTLN("[INIT_SERVER] PROCESS_SERVER_LOCATION_INITIALISATION_FOR_TEAM - Team ", iTeam, " Loc ", i, " Rule: ", MC_serverBD_4.iGotoLocationDataPriority[i][iTeam])
		
		IF NOT IS_BIT_SET(iLocationBS, i)
			IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].vLoc[iTeam])
				MC_serverBD.iNumLocCreated++
				SET_BIT(iLocationBS, i)
			ENDIF
		ENDIF
		
	ENDFOR
	
ENDPROC

PROC PROCESS_SERVER_PED_PRE_PRIORITY_INITIALISATION_FOR_TEAM(INT iTeam, INT &iPedBS[FMMC_MAX_PEDS_BITSET])
	
	INT i
	FOR i = 0 TO FMMC_MAX_PEDS - 1

		MC_serverBD.TargetPlayerID[i] = INVALID_PLAYER_INDEX()
		MC_serverBD_2.iTargetID[i] = -1
		MC_serverBD.niTargetID[i] = NULL
				
		IF NOT IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[i])
		OR MC_IS_PED_GOING_TO_SPAWN_LATER(i)
			MC_serverBD_4.iPedPriority[i][iTeam] = g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPriority[iTeam]
			MC_serverBD_4.iPedRule[i][iTeam] = g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iRule[iTeam]
			MC_serverBD_2.iAssociatedAction[i] = g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAssociatedAction
		ELSE
			MC_serverBD_4.iPedPriority[i][iTeam] = FMMC_PRIORITY_IGNORE
			MC_serverBD_4.iPedRule[i][iTeam] = FMMC_OBJECTIVE_LOGIC_NONE
			MC_serverBD_2.iAssociatedAction[i] = ciACTION_NOTHING
		ENDIF
				
		PRINTLN("[INIT_SERVER] PROCESS_SERVER_PED_INITIALISATION_FOR_TEAM - Team ", iTeam, " Ped ", i, " Priority: ", MC_serverBD_4.iPedPriority[i][iTeam])
		PRINTLN("[INIT_SERVER] PROCESS_SERVER_PED_INITIALISATION_FOR_TEAM - Team ", iTeam, " Ped ", i, " Rule: ", MC_serverBD_4.iPedRule[i][iTeam])
		PRINTLN("[INIT_SERVER] PROCESS_SERVER_PED_INITIALISATION_FOR_TEAM - Team ", iTeam, " Ped ", i, " AssociatedAction: ", MC_serverBD_2.iAssociatedAction[i])
		
		IF MC_serverBD_4.iPedPriority[i][iTeam] < FMMC_MAX_RULES
			SET_SERVER_TARGETTYPES_FOR_ENTITY(i, iTeam, ci_TARGET_PED)
		ENDIF
				
		IF FMMC_IS_LONG_BIT_SET(MC_serverBD.iMissionCriticalPed[iTeam], i)
			SET_BIT(MC_serverBD.iPedTeamFailBitset[i], iTeam)
			PRINTLN("[INIT_SERVER] PROCESS_SERVER_PED_INITIALISATION_FOR_TEAM - Team ", iTeam, " Ped ", i, " is critical.")
		ENDIF
				
		IF MC_IS_PED_A_SCRIPTED_COP(i)
			FMMC_SET_LONG_BIT(MC_serverBD.iCopPed, i)
			PRINTLN("[INIT_SERVER] PROCESS_SERVER_PED_INITIALISATION_FOR_TEAM - Team ", iTeam, " Ped ", i, " is a cop.")
		ENDIF
				
		BOOL bInjured = IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[i])
		BOOL bWillSpawnLater = MC_IS_PED_GOING_TO_SPAWN_LATER(i)
  
		PRINTLN("[INIT_SERVER] PROCESS_SERVER_PED_INITIALISATION_FOR_TEAM - Team ", iTeam, " Ped ", i, " Injured: ", BOOL_TO_STRING(bInjured), " Will Spawn Later: ", BOOL_TO_STRING(bWillSpawnLater))
  
		IF NOT bInjured
		OR bWillSpawnLater
			IF NOT bInjured
				FMMC_SET_LONG_BIT(MC_serverBD.iObjSpawnPedBitset, i)
			ENDIF
			
			IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPriority[iTeam] <= MC_serverBD_4.iCurrentHighestPriority[iTeam] 
			AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPriority[iTeam] < FMMC_MAX_RULES
				MC_serverBD_4.iCurrentHighestPriority[iTeam] = g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPriority[iTeam] 
			ENDIF
									
			MC_serverBD_2.iPedState[i] = ciTASK_CHOOSE_NEW_TASK
			MC_serverBD_2.iOldPedState[i] = ciTASK_CHOOSE_NEW_TASK						
		ELSE
			FMMC_CLEAR_LONG_BIT(MC_serverBD.iMissionCriticalPed[iTeam], i)
			CLEAR_BIT(MC_serverBD.iPedTeamFailBitset[i], iTeam)
		ENDIF
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].mn = DUMMY_MODEL_FOR_SCRIPT
			RELOOP
		ENDIF
		
		IF FMMC_IS_LONG_BIT_SET(iPedBS, i)
			RELOOP
		ENDIF
			
		IF NOT bInjured
		OR bWillSpawnLater
			MC_serverBD_2.iCurrentPedRespawnLives[i] = GET_ENTITY_STARTING_RESPAWN_LIVES(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedRespawnLives)
			IF bWillSpawnLater
				IF MC_serverBD_2.iCurrentPedRespawnLives[i] != UNLIMITED_RESPAWNS
			 		MC_serverBD_2.iCurrentPedRespawnLives[i]++
				ENDIF	
				FMMC_SET_LONG_BIT(MC_serverBD.iPedFirstSpawnBitset, i)
			ENDIF
			PRINTLN("[INIT_SERVER] PROCESS_SERVER_PED_INITIALISATION_FOR_TEAM - Team ", iTeam, " Ped ", i, " Respawn Lives: ", MC_serverBD_2.iCurrentPedRespawnLives[i])
		ENDIF
		
		MC_serverBD.iNumPedCreated++
		FMMC_SET_LONG_BIT(iPedBS, i)
		
	ENDFOR
	
ENDPROC

PROC PROCESS_SERVER_VEHICLE_PRE_PRIORITY_INITIALISATION_FOR_TEAM(INT iTeam, INT &iVehicleBS)		
	
	INT i
	FOR i = 0 TO FMMC_MAX_VEHICLES - 1
				
		BOOL bSpawnAtStart = MC_SHOULD_VEH_SPAWN_AT_START(i, FALSE)
		BOOL bSpawnLater = MC_IS_VEH_GOING_TO_SPAWN_LATER(i)
		
		MC_serverBD.iVehRequestPart[i] = -1
				
		IF bSpawnAtStart
		OR bSpawnLater
			MC_serverBD_4.iVehPriority[i][iTeam] = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iPriority[iTeam]
			MC_serverBD_4.iVehRule[i][iTeam] = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iRule[iTeam]
		ELSE
			MC_serverBD_4.iVehPriority[i][iTeam] = FMMC_PRIORITY_IGNORE
			MC_serverBD_4.iVehRule[i][iTeam] = FMMC_OBJECTIVE_LOGIC_NONE
		ENDIF
		
		PRINTLN("[INIT_SERVER] PROCESS_SERVER_VEHICLE_INITIALISATION_FOR_TEAM - Team ", iTeam, " Veh ", i, " Priority: ", MC_serverBD_4.iVehPriority[i][iTeam])
		PRINTLN("[INIT_SERVER] PROCESS_SERVER_VEHICLE_INITIALISATION_FOR_TEAM - Team ", iTeam, " Veh ", i, " Rule: ", MC_serverBD_4.iVehRule[i][iTeam])

		IF MC_serverBD_4.iVehPriority[i][iTeam] < FMMC_MAX_RULES
			SET_SERVER_TARGETTYPES_FOR_ENTITY(i, iTeam, ci_TARGET_VEHICLE)
		ENDIF
				
		IF IS_BIT_SET(MC_serverBD.iMissionCriticalVeh[iTeam], i)
			SET_BIT(MC_serverBD.iVehteamFailBitset[i], iTeam)
			PRINTLN("[INIT_SERVER] PROCESS_SERVER_VEHICLE_INITIALISATION_FOR_TEAM - Team ", iTeam, " Veh ", i, " is critical.")
		ENDIF
		
		PRINTLN("[INIT_SERVER] PROCESS_SERVER_VEHICLE_INITIALISATION_FOR_TEAM - Team ", iTeam, " Veh ", i, " Spawn at start: ", BOOL_TO_STRING(bSpawnAtStart), " Will Spawn Later: ", BOOL_TO_STRING(bSpawnLater))
		
		IF NOT bSpawnAtStart
		AND NOT bSpawnLater
			CLEAR_BIT(MC_serverBD.iMissionCriticalVeh[iTeam], i)
			CLEAR_BIT(MC_serverBD.iVehteamFailBitset[i], iTeam)
		ELSE
			IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iPriority[iTeam] <= MC_serverBD_4.iCurrentHighestPriority[iTeam] 
			AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iPriority[iTeam] < FMMC_MAX_RULES
				MC_serverBD_4.iCurrentHighestPriority[iTeam] = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iPriority[iTeam] 
			ENDIF
		ENDIF
		
		IF bSpawnAtStart
			IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[i])
			AND IS_VEHICLE_DRIVEABLE(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i]))
				SET_BIT(MC_serverBD.iVehSpawnBitset, i)
				SET_BIT(MC_serverBD_2.iVehSpawnedOnce, i)
			ENDIF
		ELIF bSpawnLater
			IF SHOULD_ENTITY_RESPAWN_THIS_RULE(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iRespawnOnRuleChangeBS, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iRespawnOnRule, -1)
				SET_BIT(MC_ServerBD.iServerBS_PassRuleVehAfterRespawn, i)
			ENDIF
		ENDIF
			
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn = DUMMY_MODEL_FOR_SCRIPT
			RELOOP
		ENDIF
		
		IF IS_BIT_SET(iVehicleBS, i)
			RELOOP
		ENDIF
		
		IF bSpawnLater
		OR (bSpawnAtStart AND NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[i]) AND IS_VEHICLE_DRIVEABLE(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i])))
			IF NOT IS_VEHICLE_USING_RESPAWN_POOL(i)
				MC_serverBD_2.iCurrentVehRespawnLives[i] = GET_ENTITY_STARTING_RESPAWN_LIVES(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehicleRespawnLives)
			ENDIF
		ENDIF
		IF bSpawnLater
			IF MC_serverBD_2.iCurrentVehRespawnLives[i] != UNLIMITED_RESPAWNS
		 		MC_serverBD_2.iCurrentVehRespawnLives[i]++
			ENDIF
		ENDIF
		PRINTLN("[INIT_SERVER] PROCESS_SERVER_VEHICLE_INITIALISATION_FOR_TEAM - Team ", iTeam, " Veh ", i, " Respawn Lives: ", MC_serverBD_2.iCurrentVehRespawnLives[i])
		
		MC_serverBD.iNumVehCreated++
		SET_BIT(iVehicleBS, i)
				
	ENDFOR
	
ENDPROC

PROC PROCESS_SERVER_TRAIN_PRE_PRIORITY_INITIALISATION_FOR_TEAM(INT iTeam, INT &iTrainBS)		
	
	INT i
	FOR i = 0 TO FMMC_MAX_TRAINS - 1
		
		//Hook up Ass Rule + Spawn Groups here
		BOOL bSpawnAtStart = SHOULD_TRAIN_SPAWN_AT_START(i)
		BOOL bSpawnLater = SHOULD_TRAIN_SPAWN_LATER(i)
		
		//Hook up train objects here (if ever needed)
		UNUSED_PARAMETER(iTeam)
		UNUSED_PARAMETER(bSpawnLater)
		
//		IF bSpawnAtStart
//		OR bSpawnLater
//			MC_serverBD_4.iTrainPriority[i][iTeam] = g_FMMC_STRUCT_ENTITIES.sPlacedTrains[i].iPriority[iTeam]
//			MC_serverBD_4.iTrainRule[i][iTeam] = g_FMMC_STRUCT_ENTITIES.sPlacedTrains[i].iRule[iTeam]
//		ELSE
//			MC_serverBD_4.iTrainPriority[i][iTeam] = FMMC_PRIORITY_IGNORE
//			MC_serverBD_4.iTrainRule[i][iTeam] = FMMC_OBJECTIVE_LOGIC_NONE
//		ENDIF
//		
//		PRINTLN("[INIT_SERVER] PROCESS_SERVER_TRAIN_PRE_PRIORITY_INITIALISATION_FOR_TEAM - Team ", iTeam, " Train ", i, " Priority: ", MC_serverBD_4.iTrainPriority[i][iTeam])
//		PRINTLN("[INIT_SERVER] PROCESS_SERVER_TRAIN_PRE_PRIORITY_INITIALISATION_FOR_TEAM - Team ", iTeam, " Train ", i, " Rule: ", MC_serverBD_4.iTrainRule[i][iTeam])
//
//		IF MC_serverBD_4.iTrainPriority[i][iTeam] < FMMC_MAX_RULES
//			SET_SERVER_TARGETTYPES_FOR_ENTITY(i, iTeam, ci_TARGET_TRAIN)
//		ENDIF
//				
//		IF IS_BIT_SET(MC_serverBD.iMissionCriticalTrain[iTeam], i)
//			SET_BIT(MC_serverBD.iTrainTeamFailBitset[i], iTeam)
//			PRINTLN("[INIT_SERVER] PROCESS_SERVER_TRAIN_PRE_PRIORITY_INITIALISATION_FOR_TEAM - Team ", iTeam, " Train ", i, " is critical.")
//		ENDIF
//		
//		PRINTLN("[INIT_SERVER] PROCESS_SERVER_TRAIN_PRE_PRIORITY_INITIALISATION_FOR_TEAM - Team ", iTeam, " Train ", i, " Spawn at start: ", BOOL_TO_STRING(bSpawnAtStart), " Will Spawn Later: ", BOOL_TO_STRING(bSpawnLater))
//		
//		IF NOT bSpawnAtStart
//		AND NOT bSpawnLater
//			CLEAR_BIT(MC_serverBD.iMissionCriticalTrain[iTeam], i)
//			CLEAR_BIT(MC_serverBD.iTrainTeamFailBitset[i], iTeam)
//		ELSE
//			IF g_FMMC_STRUCT_ENTITIES.sPlacedTrains[i].iPriority[iTeam] <= MC_serverBD_4.iCurrentHighestPriority[iTeam] 
//			AND g_FMMC_STRUCT_ENTITIES.sPlacedTrains[i].iPriority[iTeam] < FMMC_MAX_RULES
//				MC_serverBD_4.iCurrentHighestPriority[iTeam] = g_FMMC_STRUCT_ENTITIES.sPlacedTrains[i].iPriority[iTeam] 
//			ENDIF
//		ENDIF
		
		IF bSpawnAtStart
			IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niTrain[i])
			AND NOT IS_ENTITY_DEAD(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niTrain[i]))
				SET_BIT(MC_serverBD.iTrainSpawnBitset, i)
			ENDIF
		ENDIF
			
		IF IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedTrains[i].vPlacedTrainPos)
			RELOOP
		ENDIF
		
		IF IS_BIT_SET(iTrainBS, i)
			RELOOP
		ENDIF
		
		MC_serverBD.iNumTrainCreated++
		SET_BIT(iTrainBS, i)
				
	ENDFOR
	
ENDPROC

PROC PROCESS_SERVER_OBJECT_PRE_PRIORITY_INITIALISATION_FOR_TEAM(INT iTeam, INT &iObjectBS)		
	
	INT i
	FOR i = 0 TO FMMC_MAX_NUM_OBJECTS - 1
	
		MC_serverBD.iObjDelTeam[i] = -1
		MC_serverBD.iObjCarrier[i] = -1
		MC_serverBD.iObjHackPart[i] = -1
				
		IF IS_OBJECT_A_FRAGGABLE_CRATE(i)
			SET_BIT(MC_serverBD.iFragableCrate, i)
			PRINTLN("[INIT_SERVER] PROCESS_SERVER_OBJECT_INITIALISATION_FOR_TEAM - Team ", iTeam, " Object ", i, " is a fraggable crate")
		ENDIF
		
		BOOL bSpawnAtStart = MC_SHOULD_OBJ_SPAWN_AT_START(i, FALSE)
		BOOL bSpawnLater = MC_IS_OBJ_GOING_TO_SPAWN_LATER(i)
		
		PRINTLN("[INIT_SERVER] PROCESS_SERVER_OBJECT_INITIALISATION_FOR_TEAM - Team ", iTeam, " Object ", i, " Spawn at start: ", BOOL_TO_STRING(bSpawnAtStart), " Will Spawn Later: ", BOOL_TO_STRING(bSpawnLater))
				
		IF bSpawnAtStart
		OR bSpawnLater
			MC_serverBD_4.iObjPriority[i][iTeam] = g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iPriority[iTeam]
			MC_serverBD_4.iObjRule[i][iTeam] = g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iRule[iTeam]
		ELSE
			MC_serverBD_4.iObjPriority[i][iTeam] = FMMC_PRIORITY_IGNORE
			MC_serverBD_4.iObjRule[i][iTeam] = FMMC_OBJECTIVE_LOGIC_NONE
		ENDIF
		
		PRINTLN("[INIT_SERVER] PROCESS_SERVER_OBJECT_INITIALISATION_FOR_TEAM - Team ", iTeam, " Object ", i, " Priority: ", MC_serverBD_4.iObjPriority[i][iTeam])
		PRINTLN("[INIT_SERVER] PROCESS_SERVER_OBJECT_INITIALISATION_FOR_TEAM - Team ", iTeam, " Object ", i, " Rule: ", MC_serverBD_4.iObjRule[i][iTeam])

		IF MC_serverBD_4.iObjPriority[i][iTeam] < FMMC_MAX_RULES
			SET_SERVER_TARGETTYPES_FOR_ENTITY(i,iTeam,ci_TARGET_OBJECT)
		ENDIF
				
		IF IS_BIT_SET(MC_serverBD.iMissionCriticalObj[iTeam], i)
			SET_BIT(MC_serverBD.iObjteamFailBitset[i], iTeam)
			PRINTLN("[INIT_SERVER] PROCESS_SERVER_OBJECT_INITIALISATION_FOR_TEAM - Team ", iTeam, " Object ", i, " is critical.")
		ENDIF
				
		IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iRule[iTeam] = FMMC_OBJECTIVE_LOGIC_MINIGAME
			IF IS_MODEL_A_CASH_GRAB_MINIGAME_OBJECT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].mn)
				SET_BIT(MC_serverBD.iCashGrabRuleActive[iTeam], g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iPriority[iTeam])
				PRINTLN("[INIT_SERVER] PROCESS_SERVER_OBJECT_INITIALISATION_FOR_TEAM - Team ", iTeam, " Object ", i, " Cash Grab Priority: ", MC_serverBD_4.iObjPriority[i][iTeam])
			ENDIF
				
			IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectMinigame = ciFMMC_OBJECT_MINIGAME_TYPE__USE_THERMAL_CHARGE
				SET_BIT(MC_serverBD.iThermiteRuleActive[iTeam], g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iPriority[iTeam])
				PRINTLN("[INIT_SERVER] PROCESS_SERVER_OBJECT_INITIALISATION_FOR_TEAM - Team ", iTeam, " Object ", i, " Thermite Priority: ", MC_serverBD_4.iObjPriority[i][iTeam])
			ENDIF
		ENDIF
				
		IF bSpawnAtStart
		OR bSpawnLater
			IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iPriority[iTeam] <= MC_serverBD_4.iCurrentHighestPriority[iTeam] 
			AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iPriority[iTeam] < FMMC_MAX_RULES
				MC_serverBD_4.iCurrentHighestPriority[iTeam] = g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iPriority[iTeam] 
			ENDIF
					
			CHECK_IF_OBJECT_IS_CONTAINER_AND_SET_SERVER_DATA(iTeam, i)
					
			IF NETWORK_DOES_NETWORK_ID_EXIST(GET_OBJECT_NET_ID(i))
				SET_BIT(MC_serverBD_2.iObjSpawnedOnce, i)
				IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectAttachmentPed != -1
					SET_BIT(iAICarryBitset,i)
				ENDIF
			ENDIF
		ELSE
			CLEAR_BIT(MC_serverBD.iObjteamFailBitset[i], iTeam)
			CLEAR_BIT(MC_serverBD.iMissionCriticalObj[iTeam], i)
		ENDIF
	
		IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].mn = DUMMY_MODEL_FOR_SCRIPT
			RELOOP
		ENDIF
		
		IF NOT IS_BIT_SET(iObjectBS, i)
			
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sObjElectronicData.sCCTVData.iCCTVBS, ciCCTV_BS_EnableCCTVMovement)
			OR IS_THIS_MODEL_A_CCTV_CAMERA(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].mn)
				INT iCam
				FOR iCam = 0 TO MAX_NUM_CCTV_CAM - 1
					
					IF MC_serverBD.iCameraNumber[iCam] != -1
						RELOOP
					ENDIF
					
					MC_ServerBD.iCameraNumber[iCam] = i
					MC_ServerBD.eCameraType[iCam] = FMMC_ELECTRONIC_ENTITY_TYPE__OBJECT
					PRINTLN("[INIT_SERVER] PROCESS_SERVER_OBJECT_INITIALISATION_FOR_TEAM - Team ", iTeam, " Object ", i, " Camera Index: ", iCam)
					
					BREAKLOOP
					
				ENDFOR
				
			ENDIF
			
			MC_serverBD_2.iCurrentObjRespawnLives[i] = GET_ENTITY_STARTING_RESPAWN_LIVES(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectRespawnLives)
			IF bSpawnLater
			 	IF MC_serverBD_2.iCurrentObjRespawnLives[i] != UNLIMITED_RESPAWNS
					MC_serverBD_2.iCurrentObjRespawnLives[i]++
				ENDIF
			ENDIF
			PRINTLN("[INIT_SERVER] PROCESS_SERVER_OBJECT_INITIALISATION_FOR_TEAM - Team ", iTeam, " Object ", i, " Respawn Lives: ", MC_serverBD_2.iCurrentObjRespawnLives[i])

			MC_serverBD.iNumObjCreated++
			SET_BIT(iObjectBS, i)		
		ENDIF
		
		IF MC_serverBD_2.iCurrentObjRespawnLives[i] > 0
		OR MC_serverBD_2.iCurrentObjRespawnLives[i] = UNLIMITED_RESPAWNS
			IF MC_serverBD_4.iObjRule[i][iTeam] = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER
				SET_BIT(MC_serverBD.iAnyColObjRespawnAtPriority[iTeam], MC_serverBD_4.iObjPriority[i][iTeam])
			ENDIF
		ENDIF
				
	ENDFOR
	
ENDPROC

PROC PROCESS_SERVER_PRE_PRIORITY_INITIALISATION_FOR_TEAM(INT iTeam, INT &iLocationBS, INT &iPedBS[FMMC_MAX_PEDS_BITSET], INT &iVehicleBS, INT &iObjectBS, INT &iTrainBS)
	
	INT i
	
	//#### All team initialisation (even if they aren't set up for this mission)

	MC_serverBD_2.iClobberedScore[iTeam] = -1
		
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciCORONA_TEAM_LIVES_SETTING)
		MC_serverBD_1.iCoronaTeamLives[iTeam] = g_FMMC_STRUCT.sFMMCEndConditions[0].iPlayerLives //Intentionally team 0
		PRINTLN("[INIT_SERVER] PROCESS_SERVER_PRE_PRIORITY_INITIALISATION_FOR_TEAM - Team ", iTeam, " Corona Lives: ", MC_serverBD_1.iCoronaTeamLives[iTeam])
	ENDIF
	
	MC_serverBD.iEndCutsceneNum[iTeam] = -1
	MC_serverBD.iCutsceneID[iTeam] = -1
	MC_serverBD.iCutsceneStreamingIndex[iTeam] = -1
	FOR i = 0 TO FMMC_MAX_CUTSCENE_PLAYERS - 1
		MC_serverBD.piCutscenePlayerIDs[iTeam][i] = INVALID_PLAYER_INDEX()
	ENDFOR
		
	IF iTeam >= MC_serverBD.iNumberOfTeams
		EXIT
	ENDIF
	
	//#### Teams present in the mission only from here
	
	MC_serverBD.iNumberOfPlayingPlayers[iTeam] = MC_serverBD.iNumStartingPlayers[iTeam]
	MC_serverBD.iPartCausingFail[iTeam] = -1
	MC_serverBD.iEntityCausingFail[iTeam] = -1
	MC_serverBD.iPriorityLocation[iTeam] = -1
	MC_serverBD.iNextObjective[iTeam] = -1
			
	PROCESS_SERVER_START_POS_CLEAR_AREA_FOR_TEAM(iTeam)
			
	MC_serverBD.iMissionCriticalVeh[iTeam] = g_FMMC_STRUCT.iMissionCriticalvehBS[iTeam]
	MC_serverBD.iMissionCriticalObj[iTeam] = g_FMMC_STRUCT.iMissionCriticalObjBS[iTeam]
	
	FMMC_COPY_LONG_BITSET(g_FMMC_STRUCT.iMissionCriticalPedDoubleBS[iTeam], MC_serverBD.iMissionCriticalPed[iTeam], TRUE)
	
	IF g_FMMC_STRUCT.iInitialPoints[iTeam] != 0
		MC_serverBD.iTeamScore[iTeam] = g_FMMC_STRUCT.iInitialPoints[iTeam]
		PRINTLN("[INIT_SERVER] PROCESS_SERVER_PRE_PRIORITY_INITIALISATION_FOR_TEAM - Team ", iTeam, " Initial Score: ", MC_serverBD.iTeamScore[iTeam])
	ENDIF
			
	MC_serverBD.iRequiredDeliveries[iTeam] = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRequiredDeliveries[0]
	PRINTLN("[INIT_SERVER] PROCESS_SERVER_PRE_PRIORITY_INITIALISATION_FOR_TEAM - Team ", iTeam, " Initial Score: ", MC_serverBD.iRequiredDeliveries[iTeam])
	
	MC_serverBD_1.iVariableNumBackupAllowed[iTeam] = CLAMP_INT(MC_serverBD.iNumStartingPlayers[iTeam], ciMIN_GANG_CHASE_VARIABLE_BACKUP, MAX_NUM_MC_PLAYERS)
	PRINTLN("[INIT_SERVER] PROCESS_SERVER_PRE_PRIORITY_INITIALISATION_FOR_TEAM - Team ", iTeam, " Variable Backup: ", MC_serverBD_1.iVariableNumBackupAllowed[iTeam])

	MC_serverBD.iReasonForObjEnd[iTeam] = -1
			
	PROCESS_SERVER_LOCATION_PRE_PRIORITY_INITIALISATION_FOR_TEAM(iTeam, iLocationBS)	

	PROCESS_SERVER_PED_PRE_PRIORITY_INITIALISATION_FOR_TEAM(iTeam, iPedBS)		
	
	PROCESS_SERVER_VEHICLE_PRE_PRIORITY_INITIALISATION_FOR_TEAM(iTeam, iVehicleBS)	
	
	PROCESS_SERVER_TRAIN_PRE_PRIORITY_INITIALISATION_FOR_TEAM(iTeam, iTrainBS)	
	
	PROCESS_SERVER_OBJECT_PRE_PRIORITY_INITIALISATION_FOR_TEAM(iTeam, iObjectBS)	

ENDPROC

PROC PROCESS_SERVER_DIALOGUE_TRIGGERS_POST_PRIORITY_INITIALISATION_FOR_TEAM(INT iTeam)
	
	INT i
	FOR i = 0 TO g_FMMC_STRUCT.iNumberOfDialogueTriggers - 1
		
		IF IS_VECTOR_ZERO(MC_GET_DIALOGUE_TRIGGER_POSITION(i))
			RELOOP
		ENDIF
		
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[i].iDialogueBitset,iBS_DialogueTriggerMinigame)
			RELOOP
		ENDIF
		
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[i].iDialogueBitset, iBS_DialogueBlockProgress)
			RELOOP
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[i].iDialogueBitset, iBS_DialogueNotHeardByTeam0 + iTeam)
			RELOOP
		ENDIF
		
		IF g_FMMC_STRUCT.sDialogueTriggers[i].iRule = -1
			RELOOP
		ENDIF
		
		IF g_FMMC_STRUCT.sDialogueTriggers[i].iTeam != iTeam
			RELOOP
		ENDIF
		
		IF IS_BIT_SET(MC_serverBD_1.iObjectiveProgressionHeldUpBitset[iTeam], g_FMMC_STRUCT.sDialogueTriggers[i].iRule)
			RELOOP
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[i].iDialogueBitset, iBS_DialogueSkipOnQuickRestart) 
		AND IS_THIS_A_QUICK_RESTART_JOB()
			RELOOP
		ENDIF
		
		IF IS_BIT_SET( g_FMMC_STRUCT.sDialogueTriggers[i].iDialogueBitset2, iBS_Dialogue2SkipOnQuickRestart1) 
		AND FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(0)
			RELOOP
		ENDIF
		
		IF IS_BIT_SET( g_FMMC_STRUCT.sDialogueTriggers[i].iDialogueBitset2, iBS_Dialogue2SkipOnQuickRestart2) 
		AND FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(1)
			RELOOP
		ENDIF
		
		IF IS_BIT_SET( g_FMMC_STRUCT.sDialogueTriggers[i].iDialogueBitset2, iBS_Dialogue2SkipOnQuickRestart3) 
		AND FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(2)
			RELOOP
		ENDIF
		
		IF IS_BIT_SET( g_FMMC_STRUCT.sDialogueTriggers[i].iDialogueBitset2, iBS_Dialogue2SkipOnQuickRestart4) 
		AND FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(3)
			RELOOP
		ENDIF
		
		PRINTLN("[INIT_SERVER] PROCESS_SERVER_DIALOGUE_TRIGGERS_POST_PRIORITY_INITIALISATION_FOR_TEAM - Team ", iTeam, " Set dialogue to hold up progression for Rule: ", g_FMMC_STRUCT.sDialogueTriggers[i].iRule)
		SET_BIT(MC_serverBD_1.iObjectiveProgressionHeldUpBitset[iTeam], g_FMMC_STRUCT.sDialogueTriggers[i].iRule)
	
	ENDFOR
	
ENDPROC

PROC PROCESS_SERVER_PLAYER_RULES_POST_PRIORITY_INITIALISATION_FOR_TEAM(INT iTeam)
	
	INT i
	FOR i = 0 TO FMMC_MAX_RULES - 1
		IF MC_serverBD_4.iPlayerRulePriority[i][iTeam] <= MC_serverBD_4.iCurrentHighestPriority[iTeam] 
		AND MC_serverBD_4.iPlayerRulePriority[i][iTeam] < FMMC_MAX_RULES
			MC_serverBD_4.iPlayerRuleMissionLogic[iTeam] = MC_serverBD_4.iPlayerRule[i][iTeam] 
			MC_serverBD.iNumPlayerRuleHighestPriority[iTeam]++
		ENDIF
	ENDFOR
	
	PRINTLN("[INIT_SERVER] PROCESS_SERVER_PLAYER_RULES_POST_PRIORITY_INITIALISATION_FOR_TEAM - Team ", iTeam, " Player Rule Highest Priority: ", MC_serverBD.iNumPlayerRuleHighestPriority[iTeam])
	
ENDPROC

PROC PROCESS_SERVER_LOCATIONS_POST_PRIORITY_INITIALISATION_FOR_TEAM(INT iTeam)
	
	INT i
	FOR i = 0 TO FMMC_MAX_GO_TO_LOCATIONS - 1
		IF MC_serverBD_4.iGotoLocationDataPriority[i][iTeam] <= MC_serverBD_4.iCurrentHighestPriority[iTeam] 
		AND MC_serverBD_4.iGotoLocationDataPriority[i][iTeam] < FMMC_MAX_RULES
			IF MC_serverBD_4.iGotoLocationDataRule[i][iTeam] != FMMC_OBJECTIVE_LOGIC_NONE
				MC_serverBD_4.iLocMissionLogic[iTeam] = MC_serverBD_4.iGotoLocationDataRule[i][iTeam]
				MC_serverBD.iNumLocHighestPriority[iTeam]++
				
				IF MC_serverBD.iPriorityLocation[iTeam] = -1
					MC_serverBD.iPriorityLocation[iTeam] = i
					PRINTLN("[INIT_SERVER] PROCESS_SERVER_LOCATIONS_POST_PRIORITY_INITIALISATION_FOR_TEAM - Team ", iTeam, " Priority Location: ", MC_serverBD.iPriorityLocation[iTeam])
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	PRINTLN("[INIT_SERVER] PROCESS_SERVER_LOCATIONS_POST_PRIORITY_INITIALISATION_FOR_TEAM - Team ", iTeam, " Locations Highest Priority: ", MC_serverBD.iNumLocHighestPriority[iTeam])
			
ENDPROC

PROC PROCESS_SERVER_PEDS_POST_PRIORITY_INITIALISATION_FOR_TEAM(INT iTeam)
	
	INT i
	FOR i = 0 TO FMMC_MAX_PEDS - 1
	
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedBitset, ciPED_BS_RappelAtGoToDest)
			IF SHOULD_PED_SPAWN_IN_VEHICLE(i)
				SET_BIT(MC_serverBD.iVehicleRappelSeatsBitset[g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iVehicle], g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iVehicleSeat + 1) // Add one because the veh seat enum begins at -1. 
				PRINTLN("[INIT_SERVER] PROCESS_SERVER_PEDS_POST_PRIORITY_INITIALISATION_FOR_TEAM - Ped ", i, " set to rappel.")
			ELSE
				ASSERTLN("[INIT_SERVER] PROCESS_SERVER_PEDS_POST_PRIORITY_INITIALISATION_FOR_TEAM - Ped ", i, " set to rappel but not in a vehicle.")
				#IF IS_DEBUG_BUILD
				ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_PED_RAPPEL_NO_VEHICLE, ENTITY_RUNTIME_ERROR_TYPE_WARNING_PED, "set to rappel but not in a vehicle.", i)
				#ENDIF
			ENDIF
		ENDIF
		
		IF IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[i])
			RELOOP
		ENDIF
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPriority[iTeam] <= MC_serverBD_4.iCurrentHighestPriority[iTeam] 
		AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPriority[iTeam] < FMMC_MAX_RULES
			IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iRule[iTeam] != FMMC_OBJECTIVE_LOGIC_NONE
				MC_serverBD_4.iPedMissionLogic[iTeam] = g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iRule[iTeam] 
				MC_serverBD.iNumPedHighestPriority[iTeam]++
			ENDIF
		ENDIF
		
	ENDFOR
	
	PRINTLN("[INIT_SERVER] PROCESS_SERVER_PEDS_POST_PRIORITY_INITIALISATION_FOR_TEAM - Team ", iTeam, " Peds Highest Priority: ", MC_serverBD.iNumPedHighestPriority[iTeam])
	
ENDPROC

PROC PROCESS_SERVER_VEHICLES_POST_PRIORITY_INITIALISATION_FOR_TEAM(INT iTeam)
	
	INT i
	FOR i = 0 TO FMMC_MAX_VEHICLES - 1
	
		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[i])
			RELOOP
		ENDIF
		
		IF NOT IS_VEHICLE_DRIVEABLE(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i]))
			RELOOP
		ENDIF
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iPriority[iTeam] <= MC_serverBD_4.iCurrentHighestPriority[iTeam] 
		AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iPriority[iTeam] < FMMC_MAX_RULES
			IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iRule[iTeam] != FMMC_OBJECTIVE_LOGIC_NONE
				MC_serverBD_4.iVehMissionLogic[iTeam] = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iRule[iTeam] 
				MC_serverBD.iNumVehHighestPriority[iTeam]++
			ENDIF
		ENDIF
		
	ENDFOR
			
	PRINTLN("[INIT_SERVER] PROCESS_SERVER_VEHICLES_POST_PRIORITY_INITIALISATION_FOR_TEAM - Team ", iTeam, " Vehicles Highest Priority: ", MC_serverBD.iNumVehHighestPriority[iTeam])
			
ENDPROC

PROC PROCESS_SERVER_TRAINS_POST_PRIORITY_INITIALISATION_FOR_TEAM(INT iTeam)
	
	UNUSED_PARAMETER(iTeam)
	
	//Currently no train objectives
	
//	INT i
//	FOR i = 0 TO FMMC_MAX_TRAINS - 1
//	
//		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niTrain[i])
//			RELOOP
//		ENDIF
//		
//		IF NOT IS_ENTITY_DEAD(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehiniTraincle[i]))
//			RELOOP
//		ENDIF
//		
//		IF g_FMMC_STRUCT_ENTITIES.sPlacedTrains[i].iPriority[iTeam] <= MC_serverBD_4.iCurrentHighestPriority[iTeam] 
//		AND g_FMMC_STRUCT_ENTITIES.sPlacedTrains[i].iPriority[iTeam] < FMMC_MAX_RULES
//			IF g_FMMC_STRUCT_ENTITIES.sPlacedTrains[i].iRule[iTeam] != FMMC_OBJECTIVE_LOGIC_NONE
//				MC_serverBD_4.iTrainMissionLogic[iTeam] = g_FMMC_STRUCT_ENTITIES.sPlacedTrains[i].iRule[iTeam] 
//				MC_serverBD.iNumTrainHighestPriority[iTeam]++
//			ENDIF
//		ENDIF
//		
//	ENDFOR
//			
//	PRINTLN("[INIT_SERVER] PROCESS_SERVER_TRAINS_POST_PRIORITY_INITIALISATION_FOR_TEAM - Team ", iTeam, " Vehicles Highest Priority: ", MC_serverBD.iNumVehHighestPriority[iTeam])
			
ENDPROC

PROC PROCESS_SERVER_OBJECTS_POST_PRIORITY_INITIALISATION_FOR_TEAM(INT iTeam)
	
	INT i
	FOR i = 0 TO FMMC_MAX_NUM_OBJECTS - 1
				
		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(GET_OBJECT_NET_ID(i))
			RELOOP
		ENDIF
		
		IF IS_ENTITY_DEAD(NET_TO_OBJ(GET_OBJECT_NET_ID(i)))
			RELOOP
		ENDIF
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iPriority[iTeam] <= MC_serverBD_4.iCurrentHighestPriority[iTeam] 
		AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iPriority[iTeam] < FMMC_MAX_RULES
			IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iRule[iTeam] != FMMC_OBJECTIVE_LOGIC_NONE
				MC_serverBD_4.iObjMissionLogic[iTeam] = g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iRule[iTeam] 
				MC_serverBD_4.iObjMissionSubLogic[iTeam] = GET_HACK_SUBLOGIC_FROM_OBJECT(i)
				MC_serverBD.iNumObjHighestPriority[iTeam]++
				PRINTLN("[RCC MISSION] mission INIT MC_serverBD.iNumObjHighestPriority: ",MC_serverBD.iNumObjHighestPriority[iTeam])
			ENDIF
		ENDIF
	
		IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iLinkedDoor1 > -1
			SET_BIT(iDoorHasLinkedEntityBS, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iLinkedDoor1)
			PRINTLN("[INIT_SERVER][Doors][Door ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iLinkedDoor1, "] PROCESS_SERVER_OBJECTS_POST_PRIORITY_INITIALISATION_FOR_TEAM - Object: ", i, " Linked Primary Door: ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iLinkedDoor1)
		ENDIF
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iLinkedDoor2 > -1
			SET_BIT(iDoorHasLinkedEntityBS, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iLinkedDoor2)
			PRINTLN("[INIT_SERVER][Doors][Door ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iLinkedDoor2, "] PROCESS_SERVER_OBJECTS_POST_PRIORITY_INITIALISATION_FOR_TEAM - Object: ", i, " Linked Secondary Door: ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iLinkedDoor2)
		ENDIF
	
	ENDFOR
	
	PRINTLN("[INIT_SERVER] PROCESS_SERVER_OBJECTS_POST_PRIORITY_INITIALISATION_FOR_TEAM - Team ", iTeam, " Objects Highest Priority: ", MC_serverBD.iNumObjHighestPriority[iTeam])
	
ENDPROC

PROC PROCESS_SERVER_POST_PRIORITY_INITIALISATION_FOR_TEAM(INT iTeam)
	
	PROCESS_SERVER_DIALOGUE_TRIGGERS_POST_PRIORITY_INITIALISATION_FOR_TEAM(iTeam)
			
	PROCESS_SERVER_PLAYER_RULES_POST_PRIORITY_INITIALISATION_FOR_TEAM(iTeam)
			
	PROCESS_SERVER_LOCATIONS_POST_PRIORITY_INITIALISATION_FOR_TEAM(iTeam)		
			
	PROCESS_SERVER_PEDS_POST_PRIORITY_INITIALISATION_FOR_TEAM(iTeam)	
			
	PROCESS_SERVER_VEHICLES_POST_PRIORITY_INITIALISATION_FOR_TEAM(iTeam)
	
	PROCESS_SERVER_TRAINS_POST_PRIORITY_INITIALISATION_FOR_TEAM(iTeam)
	
	PROCESS_SERVER_OBJECTS_POST_PRIORITY_INITIALISATION_FOR_TEAM(iTeam)	
	
ENDPROC

PROC PROCESS_SERVER_PRE_RULE_INITIALISATION(INT iTeam)

	MC_serverBD_4.iFirstCheckpointRule[iTeam] = -1
	MC_serverBD_4.iSecondCheckpointRule[iTeam] = -1
	MC_serverBD_4.iThirdCheckpointRule[iTeam] = -1
	MC_serverBD_4.iFourthCheckpointRule[iTeam] = -1
	
	MC_serverBD.iProgressTime_Hours = g_FMMC_STRUCT.iTODOverrideHours
	MC_serverBD.iProgressTime_Minutes = g_FMMC_STRUCT.iTODOverrideMinutes
	
ENDPROC

PROC PROCESS_SERVER_RULE_INITIALISATION(INT iTeam, INT iRule, INT &iPlayerRuleBS)

	IF NOT IS_BIT_SET(MC_ServerBD.iServerBitSet7, SBBOOL7_USING_LIMITED_PLAYERS_IN_BOUNDS)
	AND g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sRuleBounds[iRule][0].iMaxPlayersAllowedInBounds > 0
		PRINTLN("[INIT_SERVER] PROCESS_SERVER_RULE_INITIALISATION - Using Limited Bounds")
		SET_BIT(MC_ServerBD.iServerBitSet7, SBBOOL7_USING_LIMITED_PLAYERS_IN_BOUNDS)
	ENDIF
	
	MC_serverBD_4.iPlayerRulePriority[iRule][iTeam] = g_FMMC_STRUCT.sPlayerRuleData[iRule].iPriority[iTeam]
	MC_serverBD_4.iPlayerRule[iRule][iTeam] = g_FMMC_STRUCT.sPlayerRuleData[iRule].iRule[iTeam]
	
	MC_serverBD.iPlayerRuleLimit[iRule][iTeam] = -1
	
	IF MC_serverBD_4.iPlayerRulePriority[iRule][iTeam] < FMMC_MAX_RULES
		
		IF MC_serverBD_4.iPlayerRule[iRule][iTeam] = FMMC_OBJECTIVE_LOGIC_SCRIPTED_CUTSCENE
		OR MC_serverBD_4.iPlayerRule[iRule][iTeam] = FMMC_OBJECTIVE_LOGIC_LOOT_THRESHOLD
		OR MC_serverBD_4.iPlayerRule[iRule][iTeam] = FMMC_OBJECTIVE_LOGIC_POINTS_THRESHOLD
			MC_serverBD.iPlayerRuleLimit[iRule][iTeam] = g_FMMC_STRUCT.sPlayerRuleData[iRule].iPlayerRuleLimit[iTeam]
			PRINTLN("[INIT_SERVER] PROCESS_SERVER_RULE_INITIALISATION - Team: ", iTeam, " Rule: ", iRule, " Player Rule Limit: ", MC_serverBD.iPlayerRuleLimit[iRule][iTeam])
		ELSE
			MC_serverBD.iPlayerRuleLimit[iRule][iTeam] = GET_FMMC_OBJECTIVE_CAPTURES_OR_KILLS_FROM_SELECTION(g_FMMC_STRUCT.sPlayerRuleData[iRule].iPlayerRuleLimit[iTeam])
			PRINTLN("[INIT_SERVER] PROCESS_SERVER_RULE_INITIALISATION - Team: ", iTeam, " Rule: ", iRule, " Player Rule Limit (Converted): ", MC_serverBD.iPlayerRuleLimit[iRule][iTeam])
		ENDIF
		
		IF NOT IS_BIT_SET(iPlayerRuleBS, iRule)
			MC_serverBD.iNumPlayerRuleCreated++
			SET_BIT(iPlayerRuleBS, iRule)
		ENDIF
		
		SET_SERVER_TARGETTYPES_FOR_ENTITY(iRule, iTeam, ci_TARGET_PLAYER)
	ENDIF
	
	LEGACY_INIT_CHECKPOINT_SERVER_DATA(iTeam, iRule)
	
	IF MC_serverBD_4.iPlayerRulePriority[iRule][iTeam] <= MC_serverBD_4.iCurrentHighestPriority[iTeam] 
	AND MC_serverBD_4.iPlayerRulePriority[iRule][iTeam] < FMMC_MAX_RULES
		MC_serverBD_4.iCurrentHighestPriority[iTeam] = MC_serverBD_4.iPlayerRulePriority[iRule][iTeam] 
	ENDIF
	
ENDPROC

PROC PROCESS_SERVER_DYNOPROP_INITIALISATION()

	INT i
	FOR i = 0 TO FMMC_MAX_NUM_DYNOPROPS - 1
		
		IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].sDynoPropElectronicData.sCCTVData.iCCTVBS, ciCCTV_BS_EnableCCTVMovement)
		OR NOT IS_THIS_MODEL_A_CCTV_CAMERA(GET_DYNOPROP_MODEL_TO_USE(i))
			RELOOP
		ENDIF
		
		INT iCam
		FOR iCam = 0 TO MAX_NUM_CCTV_CAM - 1
			
			IF MC_serverBD.iCameraNumber[iCam] != -1
				RELOOP
			ENDIF
			
			MC_ServerBD.iCameraNumber[iCam] = i
			MC_ServerBD.eCameraType[iCam] = FMMC_ELECTRONIC_ENTITY_TYPE__DYNOPROP
			PRINTLN("[INIT_SERVER] FMMC_MAX_NUM_DYNOPROPS - Dynoprop ", i, " Camera Index: ", iCam)
				
			BREAKLOOP
				
		ENDFOR
		
	ENDFOR
	
ENDPROC

PROC PROCESS_SERVER_MAX_LOOP_SIZE_INITIALISATION()
	
	PRINTLN("[INIT_SERVER] PROCESS_SERVER_MAX_LOOP_SIZE_INITIALISATION - Created Locations: ", MC_serverBD.iNumLocCreated)
	PRINTLN("[INIT_SERVER] PROCESS_SERVER_MAX_LOOP_SIZE_INITIALISATION - Created Player Rules: ", MC_serverBD.iNumPlayerRuleCreated)
	PRINTLN("[INIT_SERVER] PROCESS_SERVER_MAX_LOOP_SIZE_INITIALISATION - Created Peds: ", MC_serverBD.iNumPedCreated)
	PRINTLN("[INIT_SERVER] PROCESS_SERVER_MAX_LOOP_SIZE_INITIALISATION - Created Vehicles: ", MC_serverBD.iNumVehCreated)
	PRINTLN("[INIT_SERVER] PROCESS_SERVER_MAX_LOOP_SIZE_INITIALISATION - Created Objects: ", MC_serverBD.iNumObjCreated)
	PRINTLN("[INIT_SERVER] PROCESS_SERVER_MAX_LOOP_SIZE_INITIALISATION - Created Trains: ", MC_serverBD.iNumTrainCreated)
	
	MC_serverBD.iMaxLoopSize = 0
	
	IF MC_serverBD.iMaxLoopSize < MC_serverBD.iNumPlayerRuleCreated
		MC_serverBD.iMaxLoopSize = MC_serverBD.iNumPlayerRuleCreated
	ENDIF
	
	IF MC_serverBD.iMaxLoopSize < MC_serverBD.iNumLocCreated
		MC_serverBD.iMaxLoopSize = MC_serverBD.iNumLocCreated
	ENDIF
	
	IF MC_serverBD.iMaxLoopSize < MC_serverBD.iNumPedCreated
		MC_serverBD.iMaxLoopSize = MC_serverBD.iNumPedCreated
	ENDIF
	
	IF MC_serverBD.iMaxLoopSize < MC_serverBD.iNumVehCreated
		MC_serverBD.iMaxLoopSize = MC_serverBD.iNumVehCreated
	ENDIF
	
	IF MC_serverBD.iMaxLoopSize < MC_serverBD.iNumObjCreated
		MC_serverBD.iMaxLoopSize = MC_serverBD.iNumObjCreated
	ENDIF
	
	IF MC_serverBD.iMaxLoopSize < MC_serverBD.iNumTrainCreated
		MC_serverBD.iMaxLoopSize = MC_serverBD.iNumTrainCreated
	ENDIF
		
	PRINTLN("[INIT_SERVER] PROCESS_SERVER_MAX_LOOP_SIZE_INITIALISATION - Max Loop: ", MC_serverBD.iMaxLoopSize)
	
	#IF IS_DEBUG_BUILD
		IF MC_serverBD.iNumVehCreated != g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles
			PRINTLN("[INIT_SERVER] PROCESS_SERVER_MAX_LOOP_SIZE_INITIALISATION - Created vehicle count is off. MC_serverBD.iNumVehCreated: ", MC_serverBD.iNumVehCreated, " g_FMMC_STRUCT.iNumVehicles", g_FMMC_STRUCT.iNumVehicles)
			ASSERTLN("[INIT_SERVER] PROCESS_SERVER_MAX_LOOP_SIZE_INITIALISATION - Created vehicle count is off. MC_serverBD.iNumVehCreated: ", MC_serverBD.iNumVehCreated, " g_FMMC_STRUCT.iNumVehicles", g_FMMC_STRUCT.iNumVehicles)
		ENDIF
		IF MC_serverBD.iNumPedCreated != g_FMMC_STRUCT_ENTITIES.iNumberOfPeds
			PRINTLN("[INIT_SERVER] PROCESS_SERVER_MAX_LOOP_SIZE_INITIALISATION - Created ped count is off. MC_serverBD.iNumPedCreated: ", MC_serverBD.iNumPedCreated, " g_FMMC_STRUCT_ENTITIES.iNumberOfPeds", g_FMMC_STRUCT_ENTITIES.iNumberOfPeds)
			ASSERTLN("[INIT_SERVER] PROCESS_SERVER_MAX_LOOP_SIZE_INITIALISATION - Created ped count is off. MC_serverBD.iNumPedCreated: ", MC_serverBD.iNumPedCreated, " g_FMMC_STRUCT_ENTITIES.iNumberOfPeds", g_FMMC_STRUCT_ENTITIES.iNumberOfPeds)
		ENDIF
		IF MC_serverBD.iNumObjCreated != g_FMMC_STRUCT_ENTITIES.iNumberOfObjects
			PRINTLN("[INIT_SERVER] PROCESS_SERVER_MAX_LOOP_SIZE_INITIALISATION - Created obj count is off. MC_serverBD.iNumObjCreated: ", MC_serverBD.iNumObjCreated, " g_FMMC_STRUCT_ENTITIES.iNumberOfObjects", g_FMMC_STRUCT_ENTITIES.iNumberOfObjects)
			ASSERTLN("[INIT_SERVER] PROCESS_SERVER_MAX_LOOP_SIZE_INITIALISATION - Created obj count is off. MC_serverBD.iNumObjCreated: ", MC_serverBD.iNumObjCreated, " g_FMMC_STRUCT_ENTITIES.iNumberOfObjects", g_FMMC_STRUCT_ENTITIES.iNumberOfObjects)
		ENDIF
		IF MC_serverBD.iNumTrainCreated != g_FMMC_STRUCT_ENTITIES.iNumberOfTrains
			PRINTLN("[INIT_SERVER] PROCESS_SERVER_MAX_LOOP_SIZE_INITIALISATION - Created train count is off. MC_serverBD.iNumTrainCreated: ", MC_serverBD.iNumTrainCreated, " g_FMMC_STRUCT_ENTITIES.iNumberOfTrains", g_FMMC_STRUCT_ENTITIES.iNumberOfTrains)
			ASSERTLN("[INIT_SERVER] PROCESS_SERVER_MAX_LOOP_SIZE_INITIALISATION - Created train count is off. MC_serverBD.iNumTrainCreated: ", MC_serverBD.iNumTrainCreated, " g_FMMC_STRUCT_ENTITIES.iNumberOfTrains", g_FMMC_STRUCT_ENTITIES.iNumberOfTrains)
		ENDIF
	#ENDIF
	
ENDPROC				

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Main Initialization State Machine.									
// ##### Description: Manages the start up sequence for clients.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC STRING GET_PROCESS_SCRIPT_INIT_STATE_NAME(eIntroState eIntroProgressNew)
	SWITCH eIntroProgressNew
		CASE eIntroState_VALIDATE_LOBBY_LEADER					RETURN "eIntroState_VALIDATE_LOBBY_LEADER"
		CASE eIntroState_PLAYER_PED_MODEL_SWAP					RETURN "eIntroState_PLAYER_PED_MODEL_SWAP"
		CASE eIntroState_LOAD_ASSETS_AND_OUTFITS				RETURN "eIntroState_LOAD_ASSETS_AND_OUTFITS"
		CASE eIntroState_WAIT_FOR_CORONA						RETURN "eIntroState_WAIT_FOR_CORONA"
		CASE eIntroState_LOAD_ASSETS_AND_OUTFITS_POST_ALT_VARS	RETURN "eIntroState_LOAD_ASSETS_AND_OUTFITS_POST_ALT_VARS"
		CASE eIntroState_PROCESS_SPECTATOR						RETURN "eIntroState_PROCESS_SPECTATOR"
		CASE eIntroState_ASSIGN_TEAM							RETURN "eIntroState_ASSIGN_TEAM"
		CASE eIntroState_SELECT_START_POSITION					RETURN "eIntroState_SELECT_START_POSITION"
		CASE eIntroState_CREATE_ZONES							RETURN "eIntroState_CREATE_ZONES"
		CASE eIntroState_WARP_TO_START_POSITION					RETURN "eIntroState_WARP_TO_START_POSITION"
		CASE eIntroState_CREATE_LOCAL_ENTITIES					RETURN "eIntroState_CREATE_LOCAL_ENTITIES"
		CASE eIntroState_FINAL_INITIALISATION					RETURN "eIntroState_FINAL_INITIALISATION"
	ENDSWITCH
	RETURN "Invalid"
ENDFUNC

PROC SET_PROCESS_SCRIPT_INIT_STATE(eIntroState eIntroProgressNew)
	PRINTLN("[RCC MISSION] [SCRIPT INIT] PROCESS_SCRIPT_INIT SET_PROCESS_SCRIPT_INIT_STATE NewState = ", GET_PROCESS_SCRIPT_INIT_STATE_NAME(eIntroProgressNew), " OldState = ", GET_PROCESS_SCRIPT_INIT_STATE_NAME(eIntroProgress))
	eIntroProgress = eIntroProgressNew
ENDPROC

PROC PROCESS_SCRIPT_INIT_VALIDATE_LOBBY_LEADER()

	PRINTLN("[InitPosition] - PROCESS_SCRIPT_INIT_VALIDATE_LOBBY_LEADER - GET_FMMC_LOBBY_LEADER_PARTICIPANT_INDEX = ", GET_FMMC_LOBBY_LEADER_PARTICIPANT_INDEX())
	
	IF GET_FMMC_LOBBY_LEADER_PARTICIPANT_INDEX() = -1
		PRINTLN("[InitPosition] - PROCESS_SCRIPT_INIT_VALIDATE_LOBBY_LEADER - Waiting for Lobby Leader. Likely they are not yet flagged as a participant.")
		
		MC_START_NET_TIMER_WITH_TIMESTAMP(LobbyLeaderCacheFailSafeTimeStamp)
		IF MC_GET_NET_TIMER_DIFFERENCE_WITH_TIME_STAMP(LobbyLeaderCacheFailSafeTimeStamp) >= ciLOBBY_LEADER_CACHE_FAIL_SAFE_TIME
			PRINTLN("[InitPosition] - PROCESS_SCRIPT_INIT_VALIDATE_LOBBY_LEADER - Past Fail Safe Timer.")
			#IF IS_DEBUG_BUILD
			ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_FAILSAFE_WAITING_FOR_LOBBY_LEADER, ENTITY_RUNTIME_ERROR_TYPE_CRITICAL, "Waited # miliseconds for Lobby Leader to cache. Something went wrong, proceeding anyway.", ciLOBBY_LEADER_CACHE_FAIL_SAFE_TIME)	
			#ENDIF
		ELSE
			EXIT
		ENDIF
	ENDIF	
	
	SET_PROCESS_SCRIPT_INIT_STATE(eIntroState_PLAYER_PED_MODEL_SWAP)
	
ENDPROC

FUNC BOOL HAVE_ALL_PLAYERS_CACHED_PLAYER_PEDS_LOCALLY()
	
	SET_BIT(MC_PlayerBD[iLocalPart].iClientBitSet4, PBBOOL4_ASSIGNED_ALL_CACHED_PLAYER_PEDS)
	
	INT iPart	
	FOR iPart = 0 TO (MAX_NUM_MC_PLAYERS - 1)

		PARTICIPANT_INDEX tempPart = INT_TO_PARTICIPANTINDEX(iPart)
		
		IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)			
			RELOOP 
		ENDIF
		
		PLAYER_INDEX piPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
			
		IF DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(piPlayer)
		OR IS_PLAYER_SCTV(piPlayer)
			RELOOP
		ENDIF
		
		IF NOT IS_NET_PLAYER_OK(piPlayer, FALSE, FALSE)
			RELOOP
		ENDIF
		
		IF NOT IS_BIT_SET(MC_PlayerBD[iPart].iClientBitSet4, PBBOOL4_ASSIGNED_ALL_CACHED_PLAYER_PEDS)
			PRINTLN("[Player Model Swap] - HAVE_ALL_PLAYERS_CACHED_PLAYER_PEDS_LOCALLY Waiting for Participant: ", iPart, " to have created local original ped models.")
			RETURN FALSE
		ENDIF
	ENDFOR
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_CONTENT_WAIT_FOR_LOBBY_BEFORE_MODEL_SWAP()
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_SIEGE_MENTALITY_FL(g_FMMC_STRUCT.iAdversaryModeType)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_CONTENT_HAD_ROUNDS_DELAY_BEFORE_MODEL_SWAP()
	IF NOT HAS_NET_TIMER_STARTED(tdPlayerPedModelSwapDelay)
		REINIT_NET_TIMER(tdPlayerPedModelSwapDelay)
	ELIF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdPlayerPedModelSwapDelay, ciPlayerPedModelSwapDelayLength)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_SCRIPT_INIT_PLAYER_PED_MODEL_SWAP()
		
	IF SHOULD_CONTENT_WAIT_FOR_LOBBY_BEFORE_MODEL_SWAP()
		IF NOT IS_CORONA_READY_TO_START_WITH_JOB()
		AND NOT DID_I_JOIN_MISSION_AS_SPECTATOR()
		AND NOT bIsSCTV
			PRINTLN("[Player Model Swap] - PROCESS_SCRIPT_INIT_LOAD_ASSETS_AND_OUTFITS - Waiting for IS_CORONA_READY_TO_START_WITH_JOB.")
			EXIT
		ENDIF
	ENDIF
		
	IF SHOULD_PLAYER_PED_MODEL_BE_SWAPPED(GET_PRE_ASSIGNMENT_PLAYER_TEAM())
		
		IF IS_BIT_SET(iLocalBoolCheck29, LBOOL29_USING_MODEL_SWAP_AND_CUTSCENE_REQUIRES_ORIGINAL_PED)	
			REQUEST_MODEL(MP_F_FREEMODE_01)
			IF NOT HAS_MODEL_LOADED(MP_F_FREEMODE_01)
				PRINTLN("[Player Model Swap] - Waiting for MP_F_FREEMODE_01 to be loaded")
				EXIT
			ENDIF
			
			REQUEST_MODEL(MP_M_FREEMODE_01)
			IF NOT HAS_MODEL_LOADED(MP_M_FREEMODE_01)
				PRINTLN("[Player Model Swap] - Waiting for MP_M_FREEMODE_01 to be loaded")
				EXIT
			ENDIF
			
			IF NOT HAVE_ALL_PLAYERS_PROCESSED_PRE_GAME()
				PRINTLN("[Player Model Swap] - Waiting for HAVE_ALL_PLAYERS_PROCESSED_PRE_GAME to be true")
				EXIT
			ENDIF
			
			INT i = 0
			INT iPart = 0
			INT iTeam = 0
			FOR iTeam = 0 TO g_FMMC_STRUCT.iNumberOfTeams-1
								
				i = 0
				
				FOR iPart = 0 TO (MAX_NUM_MC_PLAYERS - 1)
					
					IF DOES_ENTITY_EXIST(g_sTransitionSessionData.piMocapOriginalPedClones[iTeam][i])
						SET_ENTITY_AS_MISSION_ENTITY(g_sTransitionSessionData.piMocapOriginalPedClones[iTeam][i], FALSE, TRUE)
						MocapCachedIntroCutscenePlayerPedClone[iTeam][i] = g_sTransitionSessionData.piMocapOriginalPedClones[iTeam][i]
						MocapCachedIntroCutscenePlayer[iTeam][i] = g_sTransitionSessionData.piMocapOriginalPlayer[iTeam][i]
						g_sTransitionSessionData.piMocapOriginalPedClones[iTeam][i] = NULL
						g_sTransitionSessionData.piMocapOriginalPlayer[iTeam][i] = INVALID_PLAYER_INDEX()
						PRINTLN("[Player Model Swap] - Adding team: ", iTeam, " ped: ", i, " from the transition globals.")
						i++
						RELOOP
					ENDIF
					
					IF i >= FMMC_MAX_CUTSCENE_PLAYERS
						PRINTLN("[Player Model Swap] - PROCESS_SCRIPT_INIT_LOAD_ASSETS_AND_OUTFITS BREAKLOOP")
						BREAKLOOP
					ENDIF
					
					PARTICIPANT_INDEX tempPart = INT_TO_PARTICIPANTINDEX(iPart)
					
					IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
						PRINTLN("[Player Model Swap] - PROCESS_SCRIPT_INIT_LOAD_ASSETS_AND_OUTFITS Participant: ", iPart, " is not active.")
						RELOOP 
					ENDIF
					
					PLAYER_INDEX piPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
						
					IF DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(piPlayer)
					OR IS_PLAYER_SCTV(piPlayer)
						PRINTLN("[Player Model Swap] - PROCESS_SCRIPT_INIT_LOAD_ASSETS_AND_OUTFITS Participant: ", iPart, " is Spectator.")
						RELOOP
					ENDIF
					
					IF NOT IS_NET_PLAYER_OK(piPlayer, FALSE, FALSE)
						PRINTLN("[Player Model Swap] - PROCESS_SCRIPT_INIT_LOAD_ASSETS_AND_OUTFITS Participant: ", iPart, " is not OK.")
						RELOOP
					ENDIF
							
					IF DOES_ENTITY_EXIST(MocapCachedIntroCutscenePlayerPedClone[iTeam][i])							
						i++
						RELOOP
					ENDIF
					
					PED_INDEX piPed = GET_PLAYER_PED(piPlayer)
					
					IF NOT HAS_PED_HEAD_BLEND_FINISHED(piPed)
						PRINTLN("[Player Model Swap] - PROCESS_SCRIPT_INIT_LOAD_ASSETS_AND_OUTFITS Participant: ", iPart, " waiting for HAS_PED_HEAD_BLEND_FINISHED.")
						RELOOP
					ENDIF
					
					MocapCachedIntroCutscenePlayer[iTeam][i] = piPlayer
					
					IF IS_PLAYER_PED_FEMALE(piPlayer)
						MocapCachedIntroCutscenePlayerPedClone[iTeam][i] = CREATE_PED(PEDTYPE_CIVFEMALE, MP_F_FREEMODE_01, <<0.0, 0.0, 0.0>>, 0.0, FALSE, FALSE)
					ELSE
						MocapCachedIntroCutscenePlayerPedClone[iTeam][i] = CREATE_PED(PEDTYPE_CIVMALE, MP_M_FREEMODE_01, <<0.0, 0.0, 0.0>>, 0.0, FALSE, FALSE)
					ENDIF
					
					CLONE_PED_TO_TARGET_ALT(piPed, MocapCachedIntroCutscenePlayerPedClone[iTeam][i])
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(MocapCachedIntroCutscenePlayerPedClone[iTeam][i], TRUE)
					SET_PED_RELATIONSHIP_GROUP_HASH(MocapCachedIntroCutscenePlayerPedClone[iTeam][i], rgFM_AiLike)
					SET_ENTITY_INVINCIBLE(MocapCachedIntroCutscenePlayerPedClone[iTeam][i], TRUE)
					SET_ENTITY_VISIBLE(MocapCachedIntroCutscenePlayerPedClone[iTeam][i], FALSE)
					SET_ENTITY_COLLISION(MocapCachedIntroCutscenePlayerPedClone[iTeam][i], FALSE)
					
					PRINTLN("[Player Model Swap] - PROCESS_SCRIPT_INIT_LOAD_ASSETS_AND_OUTFITS Adding player: ", GET_PLAYER_NAME(piPlayer), " at slot: ", i, " with Team: ", iTeam)
				
					i++
				ENDFOR
			ENDFOR
			
			IF NOT HAVE_ALL_PLAYERS_CACHED_PLAYER_PEDS_LOCALLY()
				PRINTLN("[Player Model Swap] - PROCESS_SCRIPT_INIT_LOAD_ASSETS_AND_OUTFITS Waiting for players to have created local original ped models.")				
				EXIT
			ENDIF
			
			SET_MODEL_AS_NO_LONGER_NEEDED(MP_F_FREEMODE_01)
			SET_MODEL_AS_NO_LONGER_NEEDED(MP_M_FREEMODE_01)
		ENDIF
		
		IF SHOULD_CONTENT_WAIT_FOR_LOBBY_BEFORE_MODEL_SWAP()
			IF IS_THIS_A_ROUNDS_MISSION()
			AND NOT HAS_CONTENT_HAD_ROUNDS_DELAY_BEFORE_MODEL_SWAP()
				PRINTLN("[Player Model Swap] - PROCESS_SCRIPT_INIT_LOAD_ASSETS_AND_OUTFITS - Waiting for HAS_CONTENT_HAD_ROUNDS_DELAY_BEFORE_MODEL_SWAP.")
				EXIT
			ENDIF
		ENDIF
		
		PRINTLN("[Player Model Swap] - PROCESS_SCRIPT_INIT_LOAD_ASSETS_AND_OUTFITS player model = ", GET_MODEL_NAME_FOR_DEBUG(GET_PLAYER_MODEL()))
		MODEL_NAMES mnPlayerModel = GET_FMMC_PLAYER_PED_MODEL(GET_PRE_ASSIGNMENT_PLAYER_TEAM(), TRUE)
		IF GET_PLAYER_MODEL() != mnPlayerModel
			REQUEST_MODEL(mnPlayerModel)
			IF HAS_MODEL_LOADED(mnPlayerModel)
				SET_CUSTOM_PLAYER_MODEL(mnPlayerModel, TRUE)
				LocalPlayerPed = PLAYER_PED_ID()
				LocalPlayer = PLAYER_ID()				
				INT iVariation = GET_FMMC_PLAYER_PED_MODEL_VARIATION(mnPlayerModel)
				IF iVariation != -1
					FMMC_SET_PED_VARIATION(localPlayerPed, mnPlayerModel, iVariation)
				ENDIF
								
				EXIT
			ELSE
				PRINTLN("[Player Model Swap] - PROCESS_SCRIPT_INIT_LOAD_ASSETS_AND_OUTFITS Waiting on new player model")
				EXIT
			ENDIF
		ENDIF
		
		IF GET_FMMC_PLAYER_PED_MODEL_HUD_COLOUR(mnPlayerModel) != -1
			SET_CUSTOM_MP_HUD_COLOR(GET_FMMC_PLAYER_PED_MODEL_HUD_COLOUR(mnPlayerModel))
		ENDIF
		
		SET_UP_PED_MODEL_SWAP_EXTRAS(mnPlayerModel)
		
	ENDIF
	
	SET_PROCESS_SCRIPT_INIT_STATE(eIntroState_LOAD_ASSETS_AND_OUTFITS)
	
ENDPROC

PROC PROCESS_SCRIPT_INIT_LOAD_ASSETS_AND_OUTFITS(BOOL bPostAltVars)
	
	IF NOT bPostAltVars
	AND (IS_MISSION_USING_ANY_ALT_VAR() AND NOT IS_FAKE_MULTIPLAYER_MODE_SET())
		PRINTLN("[RCC MISSION] [SCRIPT INIT] PROCESS_SCRIPT_INIT Assets NOT loaded due to ALT_VARS needing processed first, move to eIntroState_WAIT_FOR_CORONA" )
		SET_PROCESS_SCRIPT_INIT_STATE(eIntroState_WAIT_FOR_CORONA)
		EXIT
	ENDIF
	
	BOOL bAllLoaded, bProcessedOutfits, bLoadedLocalEntities

	bAllLoaded = TRUE
	bProcessedOutfits = TRUE
	
	bAllLoaded = LOADED_ALL_ASSETS() 
	bProcessedOutfits = PROCESSED_PLAYER_OUTFITS()
	bLoadedLocalEntities = MC_LOAD_AND_CREATE_ALL_LOCAL_ENTITIES()
	
	IF bProcessedOutfits
	AND bAllLoaded
	AND bLoadedLocalEntities
		PRINTLN("[RCC MISSION] [SCRIPT INIT] PROCESS_SCRIPT_INIT Assets loaded, move to eIntroState_WAIT_FOR_CORONA" )
		g_bAppliedMissionOutfit = TRUE
		
		// Has to be done before we do any warping stuff as we can start in cover.
		CREATE_COVER_POINTS()
		
		IF bPostAltVars
		AND IS_MISSION_USING_ANY_ALT_VAR()
		AND NOT IS_FAKE_MULTIPLAYER_MODE_SET()
			SET_PROCESS_SCRIPT_INIT_STATE(eIntroState_PROCESS_SPECTATOR)
		ELSE
			SET_PROCESS_SCRIPT_INIT_STATE(eIntroState_WAIT_FOR_CORONA)
		ENDIF
	ELSE
		PRINTLN("[RCC MISSION] [SCRIPT INIT] PROCESS_SCRIPT_INIT bAllLoaded = ", bAllLoaded)
		PRINTLN("[RCC MISSION] [SCRIPT INIT] PROCESS_SCRIPT_INIT bProcessedOutfits = ", bProcessedOutfits)
		PRINTLN("[RCC MISSION] [SCRIPT INIT] PROCESS_SCRIPT_INIT bLoadedLocalEntities = ", bLoadedLocalEntities)			
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_INIT_WAIT_FOR_CORONA()
	IF SHOULD_MOVE_TO_MISSION_RUNNING_STAGE()
		PRINTLN("[RCC MISSION] [SCRIPT INIT] PROCESS_SCRIPT_INIT ------------MISSION CONTROLLER SCRIPT STARTING------------- ")
		PRINTLN("[RCC MISSION] [SCRIPT INIT] PROCESS_SCRIPT_INIT MISSION NAME: ", g_FMMC_STRUCT.tl63DebugMissionName ) 
		PRINTLN("[RCC MISSION] [SCRIPT INIT] PROCESS_SCRIPT_INIT ----------------------------------------------------------- ")
		
		CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] [SCRIPT INIT] PROCESS_SCRIPT_INIT Corona ready, move to eIntroState_PROCESS_SPECTATOR" )
		
		HANDLE_CORONA_DATA()
		
		IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
			SET_SCRIPT_CLOUD_CONTENT_FIXES()
		ENDIF
		
		IF IS_MISSION_USING_ANY_ALT_VAR()
		AND NOT bIsLocalPlayerHost // Host done in Process Server.
			PRINTLN("[RCC MISSION][SCRIPT INIT] PROCESS_SCRIPT_INIT - Calling PROCESS_APPLY_MISSION_VARIATION_SETTINGS - Client.")
			PROCESS_APPLY_ALT_VAR_PRE_SPAWN_GROUP_SETTINGS() // Clients can call this at the same time no problem, as host has already cached the spawn groups settings. We only need to Cache into the Globals in case of migration.
			PROCESS_APPLY_ALT_VAR_SETTINGS()
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_EnableCasino_InteriorScripts)
			PRINTLN("[RCC MISSION][SCRIPT INIT] PROCESS_SCRIPT_INIT - Calling PROCESS_APPLY_MISSION_VARIATION_SETTINGS - Client.")
			SET_ALLOW_INTERIOR_SCRIPT_FOR_MISSION(TRUE)
		ENDIF
		
		IF NOT IS_MISSION_USING_ANY_ALT_VAR()
		OR IS_FAKE_MULTIPLAYER_MODE_SET()
			SET_PROCESS_SCRIPT_INIT_STATE(eIntroState_PROCESS_SPECTATOR)
		ELSE	
			SET_PROCESS_SCRIPT_INIT_STATE(eIntroState_LOAD_ASSETS_AND_OUTFITS_POST_ALT_VARS)
		ENDIF
	ENDIF	
ENDPROC

PROC PROCESS_SCRIPT_INIT_PROCESS_SPECTATOR()
	IF MC_CREATE_WEAPONS()
		IF NOT IS_BIT_SET( iLocalBoolCheck, CHECK_SPEC_STATUS )
			MANAGE_SPECTATOR_INIT() // This moves a spectator into running, otherwise does nothing
		ENDIF
	ENDIF

	IF IS_BIT_SET( iLocalBoolCheck, CHECK_SPEC_STATUS )
		IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet,PBBOOL_ANY_SPECTATOR)
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] [SCRIPT INIT] PROCESS_SCRIPT_INIT Spectator processed, move spectator to eIntroState_CREATE_LOCAL_ENTITIES" )
			SET_PROCESS_SCRIPT_INIT_STATE(eIntroState_CREATE_LOCAL_ENTITIES)
		ELSE
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] [SCRIPT INIT] PROCESS_SCRIPT_INIT Spectator processed, move non-spectator to eIntroState_ASSIGN_TEAM" )
			SET_PROCESS_SCRIPT_INIT_STATE(eIntroState_ASSIGN_TEAM)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_INIT_ASSIGN_TEAM()
	ASSIGN_PLAYER_TEAM()
	CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] [SCRIPT INIT] PROCESS_SCRIPT_INIT Team assigned, move to eIntroState_SELECT_START_POSITION" )	
	SET_PROCESS_SCRIPT_INIT_STATE(eIntroState_SELECT_START_POSITION)
ENDPROC

PROC PROCESS_SCRIPT_INIT_SELECT_START_POSITION()
	IF NOT IS_BIT_SET( ilocalboolcheck, LBOOL_MISSION_READY_TO_WARP_START_POSITION )		
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iTeamBitSet3, ciBS3_ENABLE_MULTIPLE_PLAYER_SAME_VEHICLE_RESPAWN)
			IF NOT IS_BIT_SET(iLocalBoolCheck25, LBOOL25_INITIALIZED_SEATING_DATA_FOR_MULTI_VEH)
				CACHE_AND_SETUP_VEHICLE_PARTNERS()
			ENDIF
		ENDIF
		IF (NOT IS_THREAD_ACTIVE( g_SpecificSpawnLocation.Thread_ID ) )
		OR (g_SpecificSpawnLocation.Thread_ID = GET_ID_OF_THIS_THREAD())
			SET_UP_START_POSITION_DATA()
		ELSE
			PRINTLN("[InitPosition] - PROCESS_SCRIPT_INIT_SELECT_START_POSITION Thread is not safe, waiting for it...")
		ENDIF
	ENDIF
	
	IF IS_BIT_SET( ilocalboolcheck, LBOOL_MISSION_READY_TO_WARP_START_POSITION )		
		CPRINTLN( DEBUG_CONTROLLER, "[InitPosition] - PROCESS_SCRIPT_INIT_SELECT_START_POSITION Start position set, move to eIntroState_WARP_TO_START_POSITION" )		
		SET_PROCESS_SCRIPT_INIT_STATE(eIntroState_WARP_TO_START_POSITION)
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_INIT_WARP_TO_START_POSITION()
		
	SET_PED_RESET_FLAG(LocalPlayerPed, PRF_SuppressInAirEvent, TRUE)
		
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_DISABLE_PV_NODES_OUTSIDE_PROPERTY)
		PRINTLN("PROCESS_SCRIPT_INIT_WARP_TO_START_POSITION - ciOptionsBS18_DISABLE_PV_NODES_OUTSIDE_PROPERTY bit is set.")
		DISABLE_CUSTOM_VEHICLE_NODES_FOR_ALL_PROPERTIES(TRUE)
	ENDIF
	
	IF IS_PLAYER_USING_SEQUENTIAL_SPAWNING_PROCEDURES()
		INT i
		INT iPlayerTeamSlot = iLocalPart
		INT iPlayerTeamSlotWaitFor = iPlayerTeamSlot
		
		BOOL bReady = TRUE
		
		CPRINTLN( DEBUG_CONTROLLER, "[InitPosition] - PROCESS_SCRIPT_INIT_WARP_TO_START_POSITION - [Sequential_Spawning] Checking to see if we are allowed to spawn yet.")
		
		IF iPlayerTeamSlot > 0
			PRINTLN("[PLAYER_LOOP] - PROCESS_SCRIPT_INIT_WARP_TO_START_POSITION")
			FOR i = 0 TO NUM_NETWORK_PLAYERS-1
				iPlayerTeamSlotWaitFor--
				
				CPRINTLN( DEBUG_CONTROLLER, "[InitPosition] - PROCESS_SCRIPT_INIT_WARP_TO_START_POSITION - [Sequential_Spawning] - checking iPart: ", iPlayerTeamSlotWaitFor)			
				
				IF iPlayerTeamSlotWaitFor > -1						
					PARTICIPANT_INDEX piPart = INT_TO_PARTICIPANTINDEX(iPlayerTeamSlotWaitFor)
					IF NETWORK_IS_PARTICIPANT_ACTIVE(piPart)
						
						CPRINTLN( DEBUG_CONTROLLER, "[InitPosition] - PROCESS_SCRIPT_INIT_WARP_TO_START_POSITION - [Sequential_Spawning] - They are active.")
						
						PLAYER_INDEX piPlayer = NETWORK_GET_PLAYER_INDEX(piPart)
						
						IF NOT DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(piPlayer)
						AND NOT IS_PLAYER_SCTV(piPlayer)
							CPRINTLN( DEBUG_CONTROLLER, "[InitPosition] - PROCESS_SCRIPT_INIT_WARP_TO_START_POSITION - [Sequential_Spawning] - They are NOT Spectating.")
							
							IF NOT IS_BIT_SET(MC_PlayerBD[iPlayerTeamSlotWaitFor].iClientBitset4, PBBOOL4_START_POSITION_SET)	
								bReady = FALSE
								CPRINTLN(DEBUG_CONTROLLER, "[InitPosition] - PROCESS_SCRIPT_INIT_WARP_TO_START_POSITION - [Sequential_Spawning] - bReady = FALSE Waiting on iPart: ", iPlayerTeamSlotWaitFor)
							ENDIF
							BREAKLOOP // Found a valid player.
						ENDIF
					ELSE 
					CPRINTLN( DEBUG_CONTROLLER, "[InitPosition] - PROCESS_SCRIPT_INIT_WARP_TO_START_POSITION - [Sequential_Spawning] - They are NOT Active.")		
					ENDIF
				ELSE
					CPRINTLN( DEBUG_CONTROLLER, "[InitPosition] - PROCESS_SCRIPT_INIT_WARP_TO_START_POSITION - [Sequential_Spawning] - Breaking out of the loop as iPlayerTeamSlotWaitFor = -1")
					BREAKLOOP	
				ENDIF
			ENDFOR
		ENDIF
		
		IF NOT bReady
			EXIT
		ENDIF
	ENDIF
	
	IF START_POSITION_SET()
		
		#IF IS_DEBUG_BUILD
		ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_NOTE_START_POSITION_TIME_ELAPSED, ENTITY_RUNTIME_ERROR_TYPE_NOTIFICATION, "Spawning and player position took # miliseconds to complete.", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stStartPosSetFailSafe))
		#ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySix, ciOptionsBS26_RetainVehicleWeaponsOnRespawn)
			g_SpawnData.bRememberVehicleWeaponOnSpawn = TRUE
		ENDIF
		
		IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
			ClearAreaAroundPointForSpawning(GET_PLAYER_COORDS(LocalPlayer), TRUE, TRUE)
		ENDIF
		
		IF NOT IS_FAKE_MULTIPLAYER_MODE_SET(TRUE)
			
			CLEAR_SPECIFIC_SPAWN_LOCATION()
			
			CLEAR_SPAWN_AREA( FALSE )

			IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
				STORE_VEHICLE_SPAWN_INFO_FROM_THIS_CAR(GET_VEHICLE_PED_IS_IN(LocalPlayerPed), GET_GAMER_HANDLE_PLAYER(LocalPlayer))	
			ENDIF
			
			IF (NOT IS_PLAYER_IN_ARMORY_TRUCK(LocalPlayer))
			OR (IS_PLAYER_IN_ARMORY_TRUCK(LocalPlayer) AND LocalPlayer != g_ownerOfArmoryTruckPropertyIAmIn)
				PRINTLN("[InitPosition] - PROCESS_SCRIPT_INIT_WARP_TO_START_POSITION - CLEANUP_MP_SAVED_TRUCK")
				CLEANUP_MP_SAVED_TRUCK(FALSE, FALSE, TRUE, FALSE, TRUE, FALSE, FALSE)
			ENDIF
			
			IF (NOT IS_PLAYER_IN_HACKER_TRUCK(LocalPlayer))
			OR (IS_PLAYER_IN_HACKER_TRUCK(LocalPlayer) AND LocalPlayer != GlobalPlayerBD[NATIVE_TO_INT(LocalPlayer)].SimpleInteriorBD.propertyOwner)
				PRINTLN("[InitPosition] - PROCESS_SCRIPT_INIT_WARP_TO_START_POSITION - CLEANUP_MP_SAVED_HACKERTRUCK")
				CLEANUP_MP_SAVED_HACKERTRUCK(FALSE, FALSE, TRUE, FALSE, TRUE, FALSE, FALSE)
			ENDIF
		ENDIF
		
		IF NOT bIsSCTV
		AND NOT DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(LocalPlayer)
		AND NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_PLAYER_OUT_OF_LIVES) //Player is rejoining early end spectate from a previous strand
			SET_ENTITY_VISIBLE( LocalPlayerPed, TRUE )
		ENDIF
					
		IF NOT IS_FAKE_MULTIPLAYER_MODE_SET(TRUE)
		AND NOT (IS_AN_AIRLOCK_STRAND_TRANSITION_RUNNING() OR g_bStartedInAirlockTransition)
			GIVE_PLAYER_ALL_UNLOCKED_HEALTH_FREEMODE(TRUE) // 1987206
		ENDIF
	
		// Clean dirty states like teleporting out of water / swimming
		IF NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed, TRUE)
		AND NOT IS_PED_IN_COVER(LocalPlayerPed)
		AND NOT IS_PED_FALLING(LocalPlayerPed)
		AND NOT IS_PED_SWIMMING(LocalPlayerPed)
			PRINTLN("[RCC MISSION] [SCRIPT INIT] PROCESS_SCRIPT_INIT Calling FORCE_PED_MOTION_STATE to fix potential dirty animations")
			FORCE_PED_MOTION_STATE(LocalPlayerPed, MS_ON_FOOT_IDLE, TRUE)
		ELSE
			PRINTLN("[RCC MISSION] [SCRIPT INIT] PROCESS_SCRIPT_INIT Cannot force motion - IS_PED_IN_ANY_VEHICLE: ", IS_PED_IN_ANY_VEHICLE(LocalPlayerPed, TRUE))
			PRINTLN("[RCC MISSION] [SCRIPT INIT] PROCESS_SCRIPT_INIT Cannot force motion - IS_PED_IN_COVER: ", IS_PED_IN_COVER(LocalPlayerPed))
			PRINTLN("[RCC MISSION] [SCRIPT INIT] PROCESS_SCRIPT_INIT Cannot force motion - IS_PED_FALLING: ", IS_PED_FALLING(LocalPlayerPed))
			PRINTLN("[RCC MISSION] [SCRIPT INIT] PROCESS_SCRIPT_INIT Cannot force motion - IS_PED_SWIMMING: ", IS_PED_SWIMMING(LocalPlayerPed))
		ENDIF
		
		FORCE_PED_AI_AND_ANIMATION_UPDATE(LocalPlayerPed, TRUE)
		
		SET_PROCESS_SCRIPT_INIT_STATE(eIntroState_CREATE_LOCAL_ENTITIES)
	ELSE
		PRINTLN("[RCC MISSION] [SCRIPT INIT] PROCESS_SCRIPT_INIT MISSION - TRANSITION TIME - WARPING  - FRAME COUNT = (", GET_FRAME_COUNT(), ") POSIX TIME = ", GET_CLOUD_TIME_AS_INT(), ")")
		PRINTLN("[RCC MISSION] [SCRIPT INIT] PROCESS_SCRIPT_INIT WARPING !!!!!!") 
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_INIT_CREATE_LOCAL_ENTITIES()
	
	IF NOT MC_CREATE_WEAPONS()
		PRINTLN("[RCC MISSION] [SCRIPT INIT] PROCESS_SCRIPT_INIT Creating Weapons / Pickups ")
		EXIT		
	ENDIF
		
	SET_BIT(MC_PlayerBD[iLocalPart].iClientbitSet4, PBBOOL4_START_POSITION_SET)
	
	SET_UP_DESPAWN_PROPS()
	
	INT iLocation
	FOR iLocation = 0 TO FMMC_MAX_GO_TO_LOCATIONS-1
		BOOL bApply
		VECTOR vTemp = <<0.0, 0.0, 0.0>>
		FLOAT fTemp = 0.0
		IF REPOSITION_COORD_BASED_ON_WARP_LOCATIONS_RANDOM_POOL(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLocation].sWarpLocationSettings, CREATION_TYPE_GOTO_LOC, TRUE, vTemp #IF IS_DEBUG_BUILD , iLocation #ENDIF )			
			bApply = TRUE
			REPOSITION_HEADING_BASED_ON_WARP_LOCATIONS_RANDOM_POOL(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLocation].sWarpLocationSettings, CREATION_TYPE_GOTO_LOC, TRUE, fTemp #IF IS_DEBUG_BUILD , iLocation #ENDIF )
			
		ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLocation].sWarpLocationSettings.iWarpLocationBS, ciWARP_LOCATION_BITSET_RepositionBasedOnSubSpawnGroup)
			MC_PROCESS_REPOSITION_ENTITY_BASED_ON_SUB_SPAWN_GROUP_SETTINGS(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLocation].sWarpLocationSettings, CREATION_TYPE_GOTO_LOC, iLocation, g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLocation].iSpawnGroup, g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLocation].iSpawnSubGroupBS, NULL)
		ENDIF	
		
		IF ASSIGN_CHECKPOINT_POSITION_FROM_WARP_LOCATION_SETTINGS(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLocation].sWarpLocationSettings, vTemp #IF IS_DEBUG_BUILD , CREATION_TYPE_GOTO_LOC, iLocation #ENDIF )
			bApply = TRUE
			ASSIGN_CHECKPOINT_HEADING_FROM_WARP_LOCATION_SETTINGS(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLocation].sWarpLocationSettings, fTemp #IF IS_DEBUG_BUILD , CREATION_TYPE_GOTO_LOC, iLocation #ENDIF )
		ENDIF		
	
		IF bApply 
			IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLocation].vloc1)
			AND NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLocation].vloc2)
				MC_PROCESS_OFFSET_FOR_AREA(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLocation].vLoc1, g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLocation].vLoc2, 1, vTemp)
			ELSE
				MC_PROCESS_OFFSET_FOR_AREA(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLocation].vLoc[0], g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLocation].vLoc1, 0, vTemp)
			ENDIF						
			g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLocation].fHeading[0] = fTemp
		
			vGoToLocateVectors[iLocation] = GET_INIT_LOCATION_VECTOR(iLocation)
			GET_INIT_LOCATION_SIZE_AND_HEIGHT(iLocation, fGoToLocateSize[iLocation], fGoToLocateHeight[iLocation])
			
		ENDIF
	ENDFOR
	
	PRINTLN("[RCC MISSION] [SCRIPT INIT] PROCESS_SCRIPT_INIT PROCESS_SCRIPT_INIT_CREATE_LOCAL_ENTITIES Done.")
	
	SET_PROCESS_SCRIPT_INIT_STATE(eIntroState_FINAL_INITIALISATION)
ENDPROC

PROC PROCESS_MC_SET_PED_AND_TRAFFIC_DENSITIES()
	
	IF IS_BIT_SET(iLocalBoolCheck35, LBOOL35_SET_UP_INITIAL_PED_AND_VEHICLE_DENSITIES)
		EXIT
	ENDIF
	
	IF iPopulationHandle > -1 //it exists, so better clear it
		PRINTLN("[Traffic] - PROCESS_MC_SET_PED_AND_TRAFFIC_DENSITIES - We have a global density iPopulationHandle of ",iPopulationHandle,", clear the existing densities before re-adding")
		MC_CLEAR_PED_AND_TRAFFIC_DENSITIES(iPopulationHandle, bVehicleGenerator)
		MC_SET_PED_AND_TRAFFIC_DENSITIES(iPopulationHandle, bVehicleGenerator, MC_serverBD.fPedDensity, MC_serverBD.fVehicleDensity)
	ELSE
		PRINTLN("[Traffic] - PROCESS_MC_SET_PED_AND_TRAFFIC_DENSITIES - We have a global density iPopulationHandle of ",iPopulationHandle,", just add the new global density")
		MC_SET_PED_AND_TRAFFIC_DENSITIES(iPopulationHandle, bVehicleGenerator, MC_serverBD.fPedDensity, MC_serverBD.fVehicleDensity)
	ENDIF
	
	SET_BIT(iLocalBoolCheck35, LBOOL35_SET_UP_INITIAL_PED_AND_VEHICLE_DENSITIES)
	
ENDPROC 

PROC PROCESS_SCRIPT_INIT_FINAL_INITIALISATION()
	IF IS_PLAYER_USING_SEQUENTIAL_SPAWNING_PROCEDURES()
		// might need to perofrm load scene, we shall see...
	ENDIF
	
	IF SHOULD_USE_LOAD_SCENE_FOR_LONG_DISTNACE_SKY_CAMS()
		IF NOT IS_BIT_SET(iLocalBoolCheck31, LBOOL31_ENDED_SKY_CAM_FOR_LOAD_SCENE_FOR_SCRIPT_INIT)
			IF NOT IS_SKYSWOOP_AT_GROUND()
				IF NOT IS_SCREEN_FADING_OUT()
				AND NOT IS_SCREEN_FADED_OUT()
					PRINTLN("[LM][RCC MISSION][Init_Load_Scene][SCRIPT INIT] PROCESS_SCRIPT_INIT Fading Screen Out...")
					DO_SCREEN_FADE_OUT(500)
				ENDIF				
				IF IS_SCREEN_FADED_OUT()
					PRINTLN("[LM][RCC MISSION][Init_Load_Scene][SCRIPT INIT] PROCESS_SCRIPT_INIT  - KILL_SKYCAM")
					KILL_SKYCAM()
					SET_BIT(iLocalBoolCheck31, LBOOL31_ENDED_SKY_CAM_FOR_LOAD_SCENE_FOR_SCRIPT_INIT)
				ENDIF
			ENDIF
			EXIT
		ELSE
			IF NOT CREATE_LOAD_SCENE_FOR_LONG_DISTANCE_SKY_CAMS(#IF IS_DEBUG_BUILD "PROCESS_SCRIPT_INIT" #ENDIF)
				PRINTLN("[LM][RCC MISSION][Init_Load_Scene][SCRIPT INIT] PROCESS_SCRIPT_INIT Exitting Process Scrit Init")
				EXIT
			ENDIF
		ENDIF
	ENDIF		

	#IF IS_DEBUG_BUILD
		IF GET_ENTITY_ALPHA( LocalPlayerPed ) >= 250
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] [SCRIPT INIT] GET_ENTITY_ALPHA( LocalPlayerPed ) >= 250 - TRUE" )
		ELSE
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] [SCRIPT INIT] GET_ENTITY_ALPHA( LocalPlayerPed ) >= 250 - FALSE" )
		ENDIF
		IF DID_I_JOIN_MISSION_AS_SPECTATOR()
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] [SCRIPT INIT] DID_I_JOIN_MISSION_AS_SPECTATOR() - TRUE" )
		ELSE
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] [SCRIPT INIT] DID_I_JOIN_MISSION_AS_SPECTATOR() - FALSE" )
		ENDIF
		IF IS_THIS_SPECTATOR_CAM_ACTIVE(g_BossSpecData.specCamData)
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] [SCRIPT INIT] IS_THIS_SPECTATOR_CAM_ACTIVE(g_BossSpecData.specCamData) - TRUE" )
		ELSE
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] [SCRIPT INIT] IS_THIS_SPECTATOR_CAM_ACTIVE(g_BossSpecData.specCamData) - FALSE" )
		ENDIF
		IF GET_CURRENT_TRANSITION_STATE() = TRANSITION_STATE_WAITING_FOR_EXTERNAL_TERMINATION_CALL
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] [SCRIPT INIT] GET_CURRENT_TRANSITION_STATE() = TRANSITION_STATE_WAITING_FOR_EXTERNAL_TERMINATION_CALL - TRUE" )
		ELSE
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] [SCRIPT INIT] GET_CURRENT_TRANSITION_STATE() = TRANSITION_STATE_WAITING_FOR_EXTERNAL_TERMINATION_CALL - FALSE" )
		ENDIF
		IF GET_CURRENT_TRANSITION_STATE() = TRANSITION_STATE_TERMINATE_MAINTRANSITION
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] [SCRIPT INIT] GET_CURRENT_TRANSITION_STATE() = TRANSITION_STATE_TERMINATE_MAINTRANSITION - TRUE" )
		ELSE
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] [SCRIPT INIT] GET_CURRENT_TRANSITION_STATE() = TRANSITION_STATE_TERMINATE_MAINTRANSITION - FALSE" )
		ENDIF
	#ENDIF
		
	// Bail Designed to Force the Spectator from the mission so that they don't get stuck on a Black Screen.
	IF NOT g_bDisableEndMocapSpectatorBail
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySix, ciOptionsBS26_DisableJIPSpectators)
		AND (bIsSCTV OR DID_I_JOIN_MISSION_AS_SPECTATOR())
		AND MC_ServerBD.iEndCutscene != ciMISSION_CUTSCENE_DEFAULT // maybe
		AND (HAS_NET_TIMER_STARTED(MC_ServerBD.tdCutsceneStartTime[0]) OR HAS_NET_TIMER_STARTED(MC_ServerBD.tdCutsceneStartTime[1]) 
			OR IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_CUTSCENE_TEAM0_PLAYERS_SELECTED + 0) OR IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_CUTSCENE_TEAM0_PLAYERS_SELECTED + 1))
			SET_TRANSITION_SESSIONS_JIP_FAILED()
			PRINTLN("[RCC MISSION] NETWORK_BAIL - too late for the specific content cut scene")
			DEBUG_PRINTCALLSTACK()
			NETWORK_BAIL(ENUM_TO_INT(NETWORK_BAIL_TS_SAFETY_BAIL)) 
		ENDIF
	ENDIF
	
	HANDLE_TEAM_START_WEAPONS(MC_playerBD[iLocalPart].iTeam)
							
	IF GET_ENTITY_ALPHA( LocalPlayerPed ) >= 250 // Wait for the player to become almost entirely visible
	OR DID_I_JOIN_MISSION_AS_SPECTATOR()		 //Removed running check as spectator never goes into running until the skyswoop is down and this holds up the skyswoop..			
		IF GET_CURRENT_TRANSITION_STATE() = TRANSITION_STATE_WAITING_FOR_EXTERNAL_TERMINATION_CALL
		OR GET_CURRENT_TRANSITION_STATE() = TRANSITION_STATE_TERMINATE_MAINTRANSITION
			IF HAS_ROUNDS_AND_QUICK_RESTART_FINISHED()
				CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] [SCRIPT INIT] Final initialisation done!" )
				
				PROCESS_MC_SET_PED_AND_TRAFFIC_DENSITIES()
				
				MC_SET_ALL_MISSION_ENTITIES_VISIBLITY(TRUE)
				
				IF NOT DID_I_JOIN_MISSION_AS_SPECTATOR()
					
					MC_SET_ALL_MISSION_ENTITIES_VISIBLITY(TRUE)

					IF NOT IS_BIT_SET(iLocalBoolCheck25, LBOOL25_MANAGE_LOCAL_PLAYER_INIT_CALLED)
						SET_BIT(iLocalBoolCheck25, LBOOL25_MANAGE_LOCAL_PLAYER_INIT_CALLED)
						MANAGE_LOCAL_PLAYER_INIT()
					ENDIF
					
					BOOL bReady = TRUE			
					INT iTeam = MC_playerBD[iLocalPart].iteam
					IF iTeam > -1
					AND iTeam < FMMC_MAX_TEAMS 
						INT iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]
						IF iRule < FMMC_MAX_RULES
							IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTen[iRule], ciBS_RULE10_ENTER_CLOSEST_PLACED_VEH_RULE_START)
								SET_PLAYER_INTO_NEAREST_PLACED_VEHICLE_SIMPLE()
							ENDIF
						ENDIF
					ELSE
						PRINTLN("[LM][RCC MISSION][START_NEAREST_PLACED_VEH] - iTeam: ", iTeam, " Might have to use Corona chosen team?")
					ENDIF
					
					IF NOT ARE_ALL_PLAYERS_READY_AFTER_START_POSITION_SET_INIT()
						PRINTLN("[RCC MISSION] PROCESS_SCRIPT_INIT - ARE_ALL_PLAYERS_READY_AFTER_START_POSITION_SET_INIT is false, bReady = FALSE")
						bReady = FALSE
					ELSE
						// Reset these variables to their default mission controller values before we start but after all clients have processed their stat positions.
						SET_PED_CONFIG_FLAG(localPlayerPed, PCF_NotAllowedToJackAnyPlayers, FALSE)
						SET_PED_CONFIG_FLAG(localPlayerPed, PCF_PlayerCanJackFriendlyPlayers, TRUE)
						SET_PED_CONFIG_FLAG(localPlayerPed, PCF_WillJackAnyPlayer, FALSE)
						SET_PED_CONFIG_FLAG(localPlayerPed, PCF_PreventAutoShuffleToDriversSeat, FALSE)
						SET_PED_CONFIG_FLAG(localPlayerPed, PCF_AllowAutoShuffleToDriversSeat, TRUE)
						SET_PED_CONFIG_FLAG(localPlayerPed, PCF_PreventAutoShuffleToTurretSeat, FALSE)
						CLEAR_ALL_PED_VEHICLE_FORCED_SEAT_USAGE(LocalPlayerPed)
					ENDIF
					
					IF NOT g_bInitCasinoPedsCreated
					AND IS_PLAYER_WITHIN_SIMPLE_INTERIOR_BOUNDS(SIMPLE_INTERIOR_CASINO)
					AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_EnableCasino_InteriorScripts)
						IF NOT HAS_NET_TIMER_STARTED(tdWaitForInteriorScriptsTimeout)
							START_NET_TIMER(tdWaitForInteriorScriptsTimeout)
						ENDIF
						IF NOT HAS_NET_TIMER_EXPIRED_READ_ONLY(tdWaitForInteriorScriptsTimeout, ciWAIT_FOR_INTERIOR_SCRIPTS_TIMEOUT)
							PRINTLN("[RCC MISSION] PROCESS_SCRIPT_INIT - Waiting for interior script to tell us it's ready. bReady = FALSE")
							bReady = FALSE
						ELSE
							RESET_NET_TIMER(tdWaitForInteriorScriptsTimeout)
							ASSERTLN("[LM] PROCESS_SCRIPT_INIT - We were waiting for interior scripts but it took too long!!!")
						ENDIF
					ENDIF
					
					IF bReady						
						PROGRESS_CONTROLLER_TO_NEXT_STAGE()
					ENDIF
				ELSE
					IF HAS_SERVER_CHECKED_ALL_CLIENTS_READY_FOR_INTRO()
						
						SET_SKYSWOOP_DOWN()
						
						IF GET_SKYSWOOP_STAGE() = SKYSWOOP_GOINGDOWN
							IF NOT bIsSCTV
								IF NOT (IS_SCREEN_FADED_OUT()
								OR IS_SCREEN_FADING_OUT())
									IF iPartToUse != -1
									AND MC_playerBD[iPartToUse].iteam != -1
										IF MC_serverBD_4.iCurrentHighestPriority[ MC_playerBD[iPartToUse].iteam ] < FMMC_MAX_RULES
											DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
											
											CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] [SCRIPT INIT] [ROUNDCAM] Specatator jipping in, do fade out as probably nothing is ready. " )
											
											SET_BIT(g_BossSpecData.specHUDData.iBitset, SPEC_HUD_BS_DO_FADE_IN_ONCE_SPEC_HUD_IS_RENDERING)	//This will let the spectator cam know to fade back in once it findds a valid player
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						IF IS_SCREEN_FADED_OUT()
							IF IS_PLAYER_SWITCH_IN_PROGRESS()
								CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] [SCRIPT INIT] DID_I_JOIN_MISSION_AS_SPECTATOR = TRUE, Stopping player Switch. (FORCED)")
								STOP_PLAYER_SWITCH()
							ELSE
								CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] [SCRIPT INIT] DID_I_JOIN_MISSION_AS_SPECTATOR = TRUE, Player Switch not in progress...")
							ENDIF
						ELSE
							CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] [SCRIPT INIT] DID_I_JOIN_MISSION_AS_SPECTATOR = TRUE, Screen is not faded out.")
						ENDIF
						
						IF IS_SKYSWOOP_AT_GROUND()							
							IF IS_TRANSITION_ACTIVE()
							AND GET_CURRENT_TRANSITION_STATE() = TRANSITION_STATE_WAITING_FOR_EXTERNAL_TERMINATION_CALL
								TAKE_CONTROL_OF_TRANSITION(FALSE)
							ENDIF
							CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] [SCRIPT INIT] DID_I_JOIN_MISSION_AS_SPECTATOR = TRUE, HAS_SERVER_CHECKED_ALL_CLIENTS_READY_FOR_INTRO returned TRUE call SET_SKYSWOOP_DOWN; IS_SKYSWOOP_AT_GROUND is TRUE, move on to GAME_STATE_RUNNING" )
							SET_BIT(MC_PlayerBD[iLocalPart].iClientBitset, PBBOOL_CLIENT_STATE_INITIALISED)
						ELSE								
							CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] [SCRIPT INIT] DID_I_JOIN_MISSION_AS_SPECTATOR = TRUE, HAS_SERVER_CHECKED_ALL_CLIENTS_READY_FOR_INTRO returned TRUE call SET_SKYSWOOP_DOWN; waiting for IS_SKYSWOOP_AT_GROUND, current state: ", ENUM_TO_INT(GET_SKYSWOOP_STAGE()))
						ENDIF
					ELSE
						CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] [SCRIPT INIT] DID_I_JOIN_MISSION_AS_SPECTATOR = TRUE, waiting for HAS_SERVER_CHECKED_ALL_CLIENTS_READY_FOR_INTRO" )
					ENDIF
				ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SPECTATOR, 	"=== MISSION === Not moving to spectate as GET_CURRENT_TRANSITION_STATE = ", 
										GET_TRANSITION_STATE_STRING(GET_CURRENT_TRANSITION_STATE()))
			#ENDIF
		ENDIF
	
		PRINTLN("[RCC MISSION] PROCESS_SCRIPT_INIT - Wait for the player to become almost entirely visible ... ELSE")
		PRINTLN("[RCC MISSION] PROCESS_SCRIPT_INIT - DID_I_JOIN_MISSION_AS_SPECTATOR() = ", DID_I_JOIN_MISSION_AS_SPECTATOR())
		PRINTLN("[RCC MISSION] PROCESS_SCRIPT_INIT - IS_THIS_SPECTATOR_CAM_ACTIVE(g_BossSpecData.specCamData) = ", IS_THIS_SPECTATOR_CAM_ACTIVE(g_BossSpecData.specCamData))
	
		IF NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
			INT fAlpha
			fAlpha = GET_ENTITY_ALPHA(LocalPlayerPed)
			PRINTLN("[RCC MISSION] PROCESS_SCRIPT_INIT - PLAYER NOT FADED IN !!!!!!")
			PRINTLN("[RCC MISSION] PLAYER NOT FADED IN !!!!!! - PLAYER ALPHA = ", fAlpha) 
			SET_ENTITY_ALPHA(LocalPlayerPed, 255, FALSE)
			SET_ENTITY_VISIBLE(LocalPlayerPed, TRUE)
		ENDIF
	ENDIF
		
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFive, ciOptionsBS25_AutomaticallyToggleNightvisionWithEMP)
		DISABLE_AMBIENT_NIGHT_VISION()
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_DISABLE_PV_NODES_OUTSIDE_PROPERTY)
		PRINTLN("[RCC MISSION] - [SCRIPT INIT] - ciOptionsBS18_DISABLE_PV_NODES_OUTSIDE_PROPERTY bit is set.")
		DISABLE_CUSTOM_VEHICLE_NODES_FOR_ALL_PROPERTIES(TRUE)
	ENDIF
	
	IF g_FMMC_STRUCT.wtVehicleWeaponToModifyDamage != WEAPONTYPE_INVALID
		PRINTLN("[RCC MISSION] - [SCRIPT INIT] - Setting Damage Modifier for Weapon: ", GET_WEAPON_NAME(g_FMMC_STRUCT.wtVehicleWeaponToModifyDamage), " to: ", g_FMMC_STRUCT.fVehicleWeaponDamageModifier)
		SET_WEAPON_DAMAGE_MODIFIER(g_FMMC_STRUCT.wtVehicleWeaponToModifyDamage, g_FMMC_STRUCT.fVehicleWeaponDamageModifier)
	ENDIF
	
	//apply the area of effect for the sticky bomb
	IF g_FMMC_STRUCT.fStickyBombAOE != 1.0
		SET_WEAPON_AOE_MODIFIER(WEAPONTYPE_STICKYBOMB, g_FMMC_STRUCT.fStickyBombAOE)
	ENDIF
ENDPROC

/// PURPOSE:
///    The very first initialisation function for the mission.
/// [FIX_2020_CONTROLLER] - [SCRIPT INIT] needs to be fully implemented across the various function calls.
PROC PROCESS_SCRIPT_INIT()

	IF bLocalPlayerPedOK
		SET_PED_RESET_FLAG(LocalPlayerPed, PRF_CannotBeTargetedByAI, TRUE)
		
		// Prevents peds jacking eachother or shuffling when placing them in the same vehicle. Normally only happens when the start position is delayed for some clients but not others. 
		// Clients late to process the seat/vehicle they are meant to be put into will often times find someone has decided to shuffle into it. These variables are reste when ALL players are through STAT_POSITION_SET.
		SET_PED_CONFIG_FLAG(localPlayerPed, PCF_NotAllowedToJackAnyPlayers, TRUE)
		SET_PED_CONFIG_FLAG(localPlayerPed, PCF_PlayerCanJackFriendlyPlayers, FALSE)
		SET_PED_CONFIG_FLAG(localPlayerPed, PCF_WillJackAnyPlayer, FALSE)
		SET_PED_CONFIG_FLAG(localPlayerPed, PCF_PreventAutoShuffleToDriversSeat, TRUE)
		SET_PED_CONFIG_FLAG(localPlayerPed, PCF_AllowAutoShuffleToDriversSeat, FALSE)
		SET_PED_CONFIG_FLAG(localPlayerPed, PCF_PreventAutoShuffleToTurretSeat, TRUE)
	ENDIF
	
	THEFEED_HIDE_THIS_FRAME()
	
	PROCESS_FREEZE_MICROPHONE_AT_PLAYER_FOR_MISSION()
	
	SWITCH eIntroProgress
		
		CASE eIntroState_VALIDATE_LOBBY_LEADER
			PROCESS_SCRIPT_INIT_VALIDATE_LOBBY_LEADER()
		BREAK
		
		CASE eIntroState_PLAYER_PED_MODEL_SWAP
			PROCESS_SCRIPT_INIT_PLAYER_PED_MODEL_SWAP()
		BREAK
		
		CASE eIntroState_LOAD_ASSETS_AND_OUTFITS
			PROCESS_SCRIPT_INIT_LOAD_ASSETS_AND_OUTFITS(FALSE)			
		BREAK
		
		CASE eIntroState_WAIT_FOR_CORONA
			PROCESS_SCRIPT_INIT_WAIT_FOR_CORONA()
		BREAK
		
		CASE eIntroState_LOAD_ASSETS_AND_OUTFITS_POST_ALT_VARS
			PROCESS_SCRIPT_INIT_LOAD_ASSETS_AND_OUTFITS(TRUE)
		BREAK
		
		CASE eIntroState_PROCESS_SPECTATOR
			PROCESS_SCRIPT_INIT_PROCESS_SPECTATOR()
		BREAK
		
		CASE eIntroState_ASSIGN_TEAM
			PROCESS_SCRIPT_INIT_ASSIGN_TEAM()
		BREAK
		
		CASE eIntroState_SELECT_START_POSITION
			PROCESS_SCRIPT_INIT_SELECT_START_POSITION()
		BREAK
		
		CASE eIntroState_WARP_TO_START_POSITION
			PROCESS_SCRIPT_INIT_WARP_TO_START_POSITION()
		BREAK
		
		CASE eIntroState_CREATE_LOCAL_ENTITIES
			PROCESS_SCRIPT_INIT_CREATE_LOCAL_ENTITIES()
		BREAK
		
		CASE eIntroState_FINAL_INITIALISATION
			PROCESS_SCRIPT_INIT_FINAL_INITIALISATION()
		BREAK
	ENDSWITCH
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Process Pre Game
// ##### Description: Before the game starts the clients and server need to pre define data and call up some pre game functions to set up the script and session.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC CREATE_FMMC_CUSTOM_RELATIONSHIP_GROUPS()
	CREATE_PLAYER_ABILITY_PED_RELATIONSHIP_GROUP()
ENDPROC

FUNC BOOL RUN_STAGGERED_RELATIONSHIP_GROUP_CREATION(INT& iRelativeGroupStaggerVars[], INT& iRelGroupTimesCalled)
	
	BOOL bReturn = TRUE
	
	INT iGroupsEveryFrame = ciMAX_PER_FRAME_RELGROUP_CREATES
	
	IF iGroupsEveryFrame > 2
		
		bReturn = FALSE
		
		INT iLoop
		
		FOR iLoop = 0 TO ((iGroupsEveryFrame / 2) - 1)
			IF CREATE_FM_MISSION_RELATIONSHIP_GROUPS_STAGGERED( iRelativeGroupStaggerVars, iRelGroupTimesCalled )
				iLoop = ((iGroupsEveryFrame / 2) - 1) // Break out!
				bReturn = TRUE
			ENDIF
		ENDFOR
	ELSE
		IF NOT CREATE_FM_MISSION_RELATIONSHIP_GROUPS_STAGGERED( iRelativeGroupStaggerVars, iRelGroupTimesCalled )
			bReturn = FALSE
		ENDIF
	ENDIF
	
	CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] RUN_STAGGERED_RELATIONSHIP_GROUP_CREATION - CREATE_FM_MISSION_RELATIONSHIP_GROUPS_STAGGERED called ", iRelGroupTimesCalled, " times." )
	
	RETURN bReturn
	
ENDFUNC

// [FMMC2020] is this necessary ??????????? Can't it be done in INIT
FUNC BOOL RUN_ALL_STAGGERED_MC_CLIENT_FUNCTIONS( INT& iRelativeGroupStaggerVars[], INT& iRelGroupTimesCalled )
	
	BOOL bReturn = TRUE
	
	CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] RUN_ALL_STAGGERED_MC_CLIENT_FUNCTIONS called ", iRelGroupTimesCalled / (ciMAX_PER_FRAME_RELGROUP_CREATES), " times." )
	
	IF NOT RUN_STAGGERED_RELATIONSHIP_GROUP_CREATION(iRelativeGroupStaggerVars, iRelGroupTimesCalled)
		bReturn = FALSE
	ENDIF
	
	IF bReturn
		CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Finishing RUN_ALL_STAGGERED_MC_CLIENT_FUNCTIONS after ", iRelGroupTimesCalled / (ciMAX_PER_FRAME_RELGROUP_CREATES), " calls." )
	ENDIF
	
	RETURN bReturn
ENDFUNC

// [FMMC2020] is this necessary ??????????? Can't it be done in INIT
///////////////////////		INITIALISE STAGGERED MC STUFF	//////////////////////////////
FUNC BOOL RUN_ALL_STAGGERED_MC_HOST_FUNCTIONS( INT& iCurrentStaggerVar, INT& iRelativeGroupStaggerVars[], INT& iRelGroupTimesCalled )
	
	BOOL bReturn = TRUE
	
	CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] RUN_ALL_STAGGERED_MC_HOST_FUNCTIONS called ", iCurrentStaggerVar, " times." )
	
	IF NOT REQUEST_LOAD_FMMC_MODELS_STAGGERED( iCurrentStaggerVar )
		bReturn = FALSE
	ENDIF
	
	IF NOT RUN_STAGGERED_RELATIONSHIP_GROUP_CREATION(iRelativeGroupStaggerVars, iRelGroupTimesCalled)
		bReturn = FALSE
	ENDIF
	
	iCurrentStaggerVar++
	
	IF bReturn
		CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Finishing RUN_ALL_STAGGERED_MC_HOST_FUNCTIONS after ", iCurrentStaggerVar, " calls." )
	ENDIF
	
	RETURN bReturn
ENDFUNC

PROC SET_UP_PRE_GAME_MC_TRANSITION_AND_CONTINUITY_VARS(BOOL bAfterScriptSetup)
	INT i
	IF NOT bAfterScriptSetup
			
		//If we're not setting up a strand mission then clear the global data
		IF NOT HAS_FIRST_STRAND_MISSION_BEEN_PASSED()
		
			CLEAR_THE_LEADERBOARD_STRUCT_SERVER_DATA()
			
			RESET_STRAND_MISSION_DATA()
			
			CLEAR_STRAND_MISSION_CELEBRATION_GLOBALS()
			
			IF NOT CHECK_TO_SEE_IF_THIS_IS_THE_LAST_STRAND_MISSION()
				SET_THIS_IS_A_STRAND_MISSION()
			ENDIF
			
			g_sTransitionSessionData.sStrandMissionData.tl63FirstMissionName = g_FMMC_STRUCT.tl63MissionName
			PRINTLN("[RCC MISSION] - COPY_CELEBRATION_DATA_TO_GLOBALS - tl63FirstMissionName 			= ", g_FMMC_STRUCT.tl63MissionName)
			
			//Cleanup the globals used in cash grab		
			IF (NOT (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFive, ciOptionsBS25_AllowCashTakeToPersistThroughTransition) 
			AND (IS_CORONA_INITIALISING_A_QUICK_RESTART() OR IS_THIS_A_QUICK_RESTART_JOB())))
			OR (IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_ClearOnQuickRestart)
			AND (IS_CORONA_INITIALISING_A_QUICK_RESTART() OR IS_THIS_A_QUICK_RESTART_JOB()))
				PRINTLN("PROCESS_PRE_GAME - Cleaning up cash grab globals as first strand mission has not been passed.")
				g_TransitionSessionNonResetVars.iTotalCashGrabTake 	= 0
				g_TransitionSessionNonResetVars.iTotalCashGrabDrop 	= 0
				PRINTLN("PROCESS_PRE_GAME - g_TransitionSessionNonResetVars.iTotalCashGrabTake = ", g_TransitionSessionNonResetVars.iTotalCashGrabTake)
				PRINTLN("PROCESS_PRE_GAME - g_TransitionSessionNonResetVars.iTotalCashGrabDrop = ", g_TransitionSessionNonResetVars.iTotalCashGrabDrop)
				
				g_TransitionSessionNonResetVars.bInvolvedInCashGrab 	= FALSE
				g_TransitionSessionNonResetVars.bDisplayCashGrabTake 	= FALSE
				PRINTLN("PROCESS_PRE_GAME - g_TransitionSessionNonResetVars.bInvolvedInCashGrab  = ", g_TransitionSessionNonResetVars.bInvolvedInCashGrab)
				PRINTLN("PROCESS_PRE_GAME - g_TransitionSessionNonResetVars.bDisplayCashGrabTake = ", g_TransitionSessionNonResetVars.bDisplayCashGrabTake)
				g_TransitionSessionNonResetVars.iLocalPlayerCashGrabbed = 0
				g_TransitionSessionNonResetVars.bDrawCokeGrabHud = FALSE
				g_TransitionSessionNonResetVars.iLocalPlayerCheckpoint1CashGrabbed = 0
				g_TransitionSessionNonResetVars.iLocalPlayerCheckpoint2CashGrabbed = 0
				g_TransitionSessionNonResetVars.iCheckpoint1CashGrabbed = 0
				g_TransitionSessionNonResetVars.iCheckpoint2CashGrabbed = 0
				g_TransitionSessionNonResetVars.iNumCokeGrabsCompleted = 0
			#IF IS_DEBUG_BUILD
			ELSE
				PRINTLN("PROCESS_PRE_GAME - Keeping global cash grab values ciOptionsBS25_AllowCashTakeToPersistThroughTransition. Quick restarting.")
			#ENDIF
			ENDIF
			
			g_TransitionSessionNonResetVars.iDailyCashStackValue = 0
			PRINTLN("PROCESS_PRE_GAME - Clearing g_TransitionSessionNonResetVars.iDailyCashStackValue")
		ENDIF
		
		//and it's not a rounds mission
		IF NOT IS_THIS_IS_A_ROUNDS_MISSION_INITIALISATION()
		AND g_sTransitionSessionData.sMissionRoundData.iNumberOfRounds = 0
			RESET_ROUND_MISSION_DATA()
			RESET_ROUNDS_MISSION_CELEBRATION_GLOBALS()
			g_MissionControllerserverBD_LB.iHighestRankInSession = 0	
		ENDIF

		IF IS_THIS_A_ROUNDS_MISSION()
		AND g_sTransitionSessionData.sMissionRoundData.iNumberOfRoundsPlayed = 0
			RESET_ROUNDS_MISSION_CELEBRATION_GLOBALS()
			PRINTLN("PROCESS_PRE_GAME - RESET_ROUNDS_MISSION_CELEBRATION_GLOBALS called. g_sTransitionSessionData.sMissionRoundData.iNumberOfRoundsPlayed = ", g_sTransitionSessionData.sMissionRoundData.iNumberOfRoundsPlayed)
		ENDIF
		
		
	//  ------- After the script has been set up -------
	ELSE		
		// Save out post mission spawn data.
		PRINTLN("[RCC MISSION] - [SAC] - [NETCELEBRATION] - printing out post mission creator spawn data. My team = ", GlobalplayerBD_FM[NATIVE_TO_INT(GET_PLAYER_INDEX())].sClientCoronaData.iTeamChosen)
		INT iTemp
		REPEAT FMMC_MAX_TEAMS iTemp
			PRINTLN("[RCC MISSION] - [SAC] - [NETCELEBRATION] -  g_FMMC_STRUCT.vPostMissionPos[", iTemp, "] = ", g_FMMC_STRUCT.vPostMissionPos[iTemp])
			PRINTLN("[RCC MISSION] - [SAC] - [NETCELEBRATION] -  g_FMMC_STRUCT.fPostMissionSpawnRadius[", iTemp, "] = ", g_FMMC_STRUCT.fPostMissionSpawnRadius[iTemp])
			PRINTLN("[RCC MISSION] - [SAC] - [NETCELEBRATION] -  g_FMMC_STRUCT.vPostMissionPOI[", iTemp, "] = ", g_FMMC_STRUCT.vPostMissionPOI[iTemp])
		ENDREPEAT	
		g_TransitionSessionNonResetVars.sPostMissionCleanupData.vPostMissionPos = g_FMMC_STRUCT.vPostMissionPos[GlobalplayerBD_FM[NATIVE_TO_INT(GET_PLAYER_INDEX())].sClientCoronaData.iTeamChosen]
		g_TransitionSessionNonResetVars.sPostMissionCleanupData.fPostMissionPosSearchRadius = g_FMMC_STRUCT.fPostMissionSpawnRadius[GlobalplayerBD_FM[NATIVE_TO_INT(GET_PLAYER_INDEX())].sClientCoronaData.iTeamChosen]
		g_TransitionSessionNonResetVars.sPostMissionCleanupData.vPostMissionPointOfInterest = g_FMMC_STRUCT.vPostMissionPOI[GlobalplayerBD_FM[NATIVE_TO_INT(GET_PLAYER_INDEX())].sClientCoronaData.iTeamChosen]
		g_TransitionSessionNonResetVars.sPostMissionCleanupData.bPostMissionPosIgnoreExclusionZones = IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySeven, ciOptionsBS27_IgnorePostMissionPosExclusionZones_Team_0 + GlobalplayerBD_FM[NATIVE_TO_INT(GET_PLAYER_INDEX())].sClientCoronaData.iTeamChosen)
		
		PRINTLN("[RCC MISSION] - [SAC] - [NETCELEBRATION] - saved out post mission spawn data:")
		PRINTLN("[RCC MISSION] - [SAC] - [NETCELEBRATION] - g_TransitionSessionNonResetVars.sPostMissionCleanupData.vPostMissionPos = ", g_TransitionSessionNonResetVars.sPostMissionCleanupData.vPostMissionPos)
		PRINTLN("[RCC MISSION] - [SAC] - [NETCELEBRATION] - g_TransitionSessionNonResetVars.sPostMissionCleanupData.fPostMissionPosSearchRadius = ", g_TransitionSessionNonResetVars.sPostMissionCleanupData.fPostMissionPosSearchRadius)
		PRINTLN("[RCC MISSION] - [SAC] - [NETCELEBRATION] - g_TransitionSessionNonResetVars.sPostMissionCleanupData.vPostMissionPointOfInterest = ", g_TransitionSessionNonResetVars.sPostMissionCleanupData.vPostMissionPointOfInterest)	
		PRINTLN("[RCC MISSION] - [SAC] - [NETCELEBRATION] - g_TransitionSessionNonResetVars.sPostMissionCleanupData.bPostMissionPosIgnoreExclusionZones = ", g_TransitionSessionNonResetVars.sPostMissionCleanupData.bPostMissionPosIgnoreExclusionZones)	
	
		PRINTLN("[PMC] - Setting the Post Mission Pos - Content Check is true.")	
		IF NOT IS_VECTOR_ZERO(g_TransitionSessionNonResetVars.sPostMissionCleanupData.vPostMissionPos)		
			PRINTLN("[PMC] - Setting the Post Mission Pos - bForcePostMissionWarpCreatorPosition = TRUE")
			g_TransitionSessionNonResetVars.sPostMissionCleanupData.bForcePostMissionWarpCreatorPosition = TRUE
		ELSE
			g_TransitionSessionNonResetVars.sPostMissionCleanupData.bForcePostMissionWarpCreatorPosition = FALSE
		ENDIF
		
		g_sTransitionSessionData.sStrandMissionData.bCutsceneEntityPassoverMustWaitForAllClients = FALSE
		
		g_sTransitionSessionData.sStrandMissionData.vCutsceneOverrideCoord = <<0.0, 0.0, 0.0>>
		g_sTransitionSessionData.sStrandMissionData.fCutsceneOverrideHeading = 0.0
		g_sTransitionSessionData.sStrandMissionData.iCutsceneMusicType = 0
		g_sTransitionSessionData.sStrandMissionData.iCutsceneMusicMood = 0
		g_sTransitionSessionData.sStrandMissionData.iEndCutsceneConcatBS = 0
		
		IF NOT IS_THIS_A_QUICK_RESTART_JOB()
		AND NOT IS_CORONA_INITIALISING_A_QUICK_RESTART()
			PRINTLN("[CLEANUP] - PROCESS_PRE_GAME - Clearing iAttachedVehicleTrailers")
			INT iVeh
			FOR iVeh = 0 TO (FMMC_MAX_VEHICLES-1)
				g_TransitionSessionNonResetVars.iAttachedVehicleTrailers[iVeh] = -1
			ENDFOR
			
			g_TransitionSessionNonResetVars.iDoNotRespawnVehBitSet = 0
			
			g_TransitionSessionNonResetVars.iPersonalVehicleIndexToUse = -1
			PRINTLN("[RCC MISSION] PROCESS_PRE_GAME - Clearing g_TransitionSessionNonResetVars.iPersonalVehicleIndexToUse")
			
			g_TransitionSessionNonResetVars.mnArenaVehicleChosen = DUMMY_MODEL_FOR_SCRIPT
			PRINTLN("[RCC MISSION] PROCESS_PRE_GAME - Clearing g_TransitionSessionNonResetVars.mnArenaVehicleChosen")
			
			g_TransitionSessionNonResetVars.iDestroyedTurretBitSet = 0
			PRINTLN("[RCC MISSION] PROCESS_PRE_GAME - Clearing g_TransitionSessionNonResetVars.iDestroyedTurretBitSet")
			FOR i = 0 TO FMMC_MAX_TEAMS-1
				g_TransitionSessionNonResetVars.iObjectsDestroyedForSpecialHUD[i] = 0
			ENDFOR
		ELSE
			IF bIsLocalPlayerHost
				IF g_TransitionSessionNonResetVars.iDestroyedTurretBitSet != 0
					MC_serverBD_4.iDestroyedTurretBitSet = g_TransitionSessionNonResetVars.iDestroyedTurretBitSet
					PRINTLN("[RCC MISSION] PROCESS_PRE_GAME - updating MC_serverBD_4.iDestroyedTurretBitSet to ", MC_serverBD_4.iDestroyedTurretBitSet)
				ENDIF
				FOR i = 0 TO FMMC_MAX_TEAMS-1
					IF g_TransitionSessionNonResetVars.iObjectsDestroyedForSpecialHUD[i] != 0
						PRINTLN("[RCC MISSION] PROCESS_PRE_GAME - g_TransitionSessionNonResetVars.iObjectsDestroyedForSpecialHUD[",i,"] = ",g_TransitionSessionNonResetVars.iObjectsDestroyedForSpecialHUD[i])
						MC_ServerBD_4.iObjectsDestroyedForSpecialHUD[i] = g_TransitionSessionNonResetVars.iObjectsDestroyedForSpecialHUD[i]
						PRINTLN("[RCC MISSION] PROCESS_PRE_GAME - updating MC_ServerBD_4.iObjectsDestroyedForSpecialHUD[",i,"] to ", MC_ServerBD_4.iObjectsDestroyedForSpecialHUD[i])
					ENDIF
				ENDFOR
			ENDIF
		ENDIF
			
		g_sTransitionSessionData.sMissionRoundData.piWinner = INT_TO_NATIVE(PLAYER_INDEX, -1)
			
		IF IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_DialogueTracking)
			FOR i = 0 TO (g_FMMC_STRUCT.iNumberOfDialogueTriggers - 1)
				IF g_FMMC_STRUCT.sDialogueTriggers[i].iContinuityId = -1
					RELOOP
				ENDIF
				IF NOT FMMC_IS_LONG_BIT_SET(sMissionLocalContinuityVars.iDialogueTrackingBitset, g_FMMC_STRUCT.sDialogueTriggers[i].iContinuityId)
					RELOOP
				ENDIF
				SET_BIT(iLocalDialoguePlayedBS[i/32],i%32)		
				PRINTLN("[CONTINUITY] PROCESS_PRE_GAME - Setting dialogue trigger ", i, " with continuity ID ", g_FMMC_STRUCT.sDialogueTriggers[i].iContinuityId, " as played on previous mission")
			ENDFOR
			FOR i = 0 TO (ciMAX_DIALOGUE_BIT_SETS - 1)
				MC_playerBD[iLocalPart].iDialoguePlayedBS[i] = iLocalDialoguePlayedBS[i]
			ENDFOR
		ENDIF

		// Checkpoints			
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].sClientCoronaData.iTeamChosen].iRuleBitset[0], ciBS_RULE_UPDATE_CHECKPOINT_0)
			FMMC_CHECKPOINTS_SET_RETRY_START_POINT(0)
			PRINTLN("[RCC MISSION] - ciBS_RULE_UPDATE_CHECKPOINT_0 set for rule 0 for my team.")
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].sClientCoronaData.iTeamChosen].iRuleBitsetThree[0], ciBS_RULE3_UPDATE_CHECKPOINT_1)
			FMMC_CHECKPOINTS_SET_RETRY_START_POINT(1)
			PRINTLN("[RCC MISSION] - ciBS_RULE3_UPDATE_CHECKPOINT_1 set for rule 0 for my team.")
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].sClientCoronaData.iTeamChosen].iRuleBitSetTen[0], ciBS_RULE10_UPDATE_CHECKPOINT_3)
			FMMC_CHECKPOINTS_SET_RETRY_START_POINT(2)
			PRINTLN("[RCC MISSION] - ciBS_RULE10_UPDATE_CHECKPOINT_3 set for rule 0 for my team.")
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].sClientCoronaData.iTeamChosen].iRuleBitSetTen[0], ciBS_RULE10_UPDATE_CHECKPOINT_4)
			FMMC_CHECKPOINTS_SET_RETRY_START_POINT(3)
			PRINTLN("[RCC MISSION] - ciBS_RULE10_UPDATE_CHECKPOINT_4 set for rule 0 for my team.")
		ENDIF	
		IF NOT IS_THIS_A_QUICK_RESTART_JOB()
		AND NOT IS_CORONA_INITIALISING_A_QUICK_RESTART()
			PRINTLN("[RCC MISSION] Init checkpoint - Reset checkpoint globals, IS_THIS_A_QUICK_RESTART_JOB & IS_CORONA_INITIALISING_A_QUICK_RESTART both FALSE")		
			MC_RESET_MISSION_CHECKPOINT_GLOBALS()
			
		ELIF HAS_TEAM_FAILED(GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].sClientCoronaData.iTeamChosen)
			
			PRINTLN("[RCC MISSION] Init checkpoint - Reset checkpoint globals, Team Has Failed")
			MC_RESET_MISSION_CHECKPOINT_GLOBALS()
			
		ELSE
			PRINTLN("[RCC MISSION] Init checkpoint - this is a quick restart job, leaving checkpoint globals set")
		ENDIF
		STORE_MISSION_START_CHECKPOINT()
		
		//Airlock				
		g_TransitionSessionNonResetVars.tl63AirlockObjectiveText = ""
		
		FOR i = 0 TO (FMMC_MAX_RULES-1)
			INT iTeamLoop
			FOR iTeamLoop = 0 TO FMMC_MAX_TEAMS - 1
				IF g_FMMC_STRUCT.sFMMCEndConditions[iTeamLoop].iLocateSwapTeam[i] > -1
					SET_BIT(iLocalBoolCheck23, LBOOL23_TEAM_SWAP_IN_MISSION)
					BREAKLOOP
				ENDIF
			ENDFOR
			
			IF MC_playerBD[iLocalPart].iteam > -1
			AND MC_playerBD[iLocalPart].iteam < FMMC_MAX_TEAMS
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].iRuleBitsetFourteen[i], ciBS_RULE14_CACHE_ALT_TEXT_FOR_AIRLOCK)
				AND IS_STRING_NULL_OR_EMPTY(g_TransitionSessionNonResetVars.tl63AirlockObjectiveText)	
				AND NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].tl63Objective2[i])
					g_TransitionSessionNonResetVars.tl63AirlockObjectiveText = g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].tl63Objective2[i]
					PRINTLN("[JS] PROCESS_PRE_GAME - tl63AirlockObjectiveText = ", g_TransitionSessionNonResetVars.tl63AirlockObjectiveText)
				ENDIF
			ENDIF
		ENDFOR
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFive, ciOptionsBS25_MaintainLivesAirlock)
			PRINTLN("[JS] PROCESS_PRE_GAME - Setting MaintainLivesAirlock")
			SET_BIT(g_TransitionSessionNonResetVars.iAirlockHUDBitset, ciFMMCAirlockHUDBS_ShowLives)
		ELSE
			CLEAR_BIT(g_TransitionSessionNonResetVars.iAirlockHUDBitset, ciFMMCAirlockHUDBS_ShowLives)
			g_TransitionSessionNonResetVars.iTeamLivesRemainingForHUD = -1
		ENDIF
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFive, ciOptionsBS25_MaintainThermiteAirlock)
			PRINTLN("[JS] PROCESS_PRE_GAME - Setting MaintainThermiteAirlock")
			SET_BIT(g_TransitionSessionNonResetVars.iAirlockHUDBitset, ciFMMCAirlockHUDBS_ShowThermite)
		ELSE
			g_TransitionSessionNonResetVars.iPlayerThermalChargesForHUD = -1
			CLEAR_BIT(g_TransitionSessionNonResetVars.iAirlockHUDBitset, ciFMMCAirlockHUDBS_ShowThermite)
		ENDIF
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFive, ciOptionsBS25_MaintainTakeAirlock)
			PRINTLN("[JS] PROCESS_PRE_GAME - Setting MaintainTakeAirlock")
			SET_BIT(g_TransitionSessionNonResetVars.iAirlockHUDBitset, ciFMMCAirlockHUDBS_ShowTake)
		ELSE
			CLEAR_BIT(g_TransitionSessionNonResetVars.iAirlockHUDBitset, ciFMMCAirlockHUDBS_ShowTake)
			g_TransitionSessionNonResetVars.iTotalCashGrabTakeForHUD = -1
		ENDIF
		IF IS_A_STRAND_MISSION_BEING_INITIALISED()
			SET_BIT(GlobalplayerBD_FM_2[NATIVE_TO_INT(LocalPlayer)].iMissionDataBitSetTwo, ciMISSION_DATA_TWO_ON_SECOND_PART_OF_STRAND)
		ELSE
			CLEAR_BIT(GlobalplayerBD_FM_2[NATIVE_TO_INT(LocalPlayer)].iMissionDataBitSetTwo, ciMISSION_DATA_TWO_ON_SECOND_PART_OF_STRAND)
		ENDIF	
	ENDIF
ENDPROC

PROC SET_UP_PRE_GAME_MC_GLOBALS()

	INT i
	FOR i = 0 TO (3 - 1)
		g_iFMMCPedAnimBreakoutBS[i] = 0
	ENDFOR		
	FOR i = 0 TO FMMC_MAX_NUM_OBJECTS - 1
		gBG_MC_serverBD_VARS.oiObjects[i] = NULL
	ENDFOR
	FOR i = 0 TO FMMC_MAX_VEHICLES - 1
		gBG_MC_serverBD_VARS.viVehicles[i] = NULL
	ENDFOR
	FOR i = 0 TO FMMC_MAX_PEDS - 1
		gBG_MC_serverBD_VARS.piPeds[i] = NULL
	ENDFOR
	
	g_vMCBGPINEntry = <<0,0,0>>
	
	gBG_MC_serverBD_VARS.iServerBitSet_ForceClear = 0
	gBG_MC_serverBD_VARS.iServerBitSet_ForceSet = 0
	gBG_MC_serverBD_VARS.iServerBitSet1_ForceClear = 0
	gBG_MC_serverBD_VARS.iServerBitSet1_ForceSet = 0
	gBG_MC_serverBD_VARS.iServerBitSet2_ForceClear = 0
	gBG_MC_serverBD_VARS.iServerBitSet2_ForceSet = 0
	gBG_MC_serverBD_VARS.iServerBitSet3_ForceClear = 0
	gBG_MC_serverBD_VARS.iServerBitSet3_ForceSet = 0
	gBG_MC_serverBD_VARS.iServerBitSet4_ForceClear = 0
	gBG_MC_serverBD_VARS.iServerBitSet4_ForceSet = 0
	gBG_MC_serverBD_VARS.iServerBitSet5_ForceClear = 0
	gBG_MC_serverBD_VARS.iServerBitSet5_ForceSet = 0
	gBG_MC_serverBD_VARS.iServerBitSet6_ForceClear = 0
	gBG_MC_serverBD_VARS.iServerBitSet6_ForceSet = 0
	gBG_MC_serverBD_VARS.iServerBitSet7_ForceClear = 0
	gBG_MC_serverBD_VARS.iServerBitSet7_ForceSet = 0
	gBG_MC_serverBD_VARS.iServerBitSet8_ForceClear = 0
	gBG_MC_serverBD_VARS.iServerBitSet8_ForceSet = 0
	
	gMC_PlayerBD_VARS.iClientBitSet_ForceClear = 0
	gMC_PlayerBD_VARS.iClientBitSet_ForceSet = 0
	gMC_PlayerBD_VARS.iClientBitSet2_ForceClear = 0
	gMC_PlayerBD_VARS.iClientBitSet2_ForceSet = 0
	gMC_PlayerBD_VARS.iClientBitSet3_ForceClear = 0
	gMC_PlayerBD_VARS.iClientBitSet3_ForceSet = 0
	gMC_PlayerBD_VARS.iClientBitSet4_ForceClear = 0
	gMC_PlayerBD_VARS.iClientBitSet4_ForceSet = 0
	
	g_iBS1_Mission = 0	
	g_iTeamNameBitSetPlayerName = 0
	g_iNumSupressed = 0
	g_mnOfLastVehicleWeWasIn = DUMMY_MODEL_FOR_SCRIPT
	g_iFMMCCurrentStaticCam = -1
	MPGlobalsAmbience.bIgnoreInvalidToSpectateBail = FALSE	
	g_bLastMissionWVM = FALSE
	g_bReBlockSeatSwapping = FALSE
	g_bFMMCCriticalFail = FALSE
	g_bBlockInfiniteRangeCheck = FALSE	
	g_bMCForceThroughRestartState = FALSE
	g_bLeavingVehInteriorForMCEnd = FALSE
	g_bMissionEnding = FALSE
	g_bMissionOver = FALSE
	RESET_GLOBAL_SPECTATOR_STRUCT()		
	g_bUsingTurretHeliCam = FALSE
	g_bInMissionControllerCutscene = FALSE
	g_bMissionHideRespawnBar = FALSE
	g_bMissionHideRespawnBarForRestart = FALSE
	g_iMissionObjectHasSpawnedBitset = 0
	g_iMissionPlayerLeftBS = 0
	
	FOR i = 0 TO FMMC_MAX_TEAMS-1
		CLEAR_POST_MISSION_CUTSCENE_ID_FROM_MISSION_DATA(i)
	ENDFOR
	
	IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
		g_bBlockScubaGear = FALSE
	ENDIF
	
	IF NOT IS_THIS_A_QUICK_RESTART_JOB()
	AND NOT IS_CORONA_INITIALISING_A_QUICK_RESTART()
		g_bHeistCheaterIncremented = FALSE
	ENDIF				
	IF g_sMenuData.bSubtitlesMoved
		RESET_HUD_COMPONENT_VALUES(NEW_HUD_SUBTITLE_TEXT)
		g_sMenuData.bSubtitlesMoved = FALSE
	ENDIF		
	DISABLE_HEIST_SPECTATE(TRUE)
	CLEAR_BIT(g_iFmNmhBitSet8, BI_FM_NMH8_VULK_HOVER_HLP)
	CLEAR_BIT(g_iFmNmhBitSet8, BI_FM_NMH8_VULK_HOVER_HLP2)
	PRINTLN("[dsw] Cleared hydra hover help flags")
				
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySix, ciOptionsBS26_BlockDeathTickers)
		g_bNoDeathTickers = TRUE
	ENDIF
	
	FOR i = 0 TO (FMMC_MAX_TEAMS-1)
		IF g_FMMC_STRUCT.iTeamColourOverride[i] != -1
			g_bTeamColoursOverridden = TRUE
		ENDIF
	ENDFOR
	
	IF IS_THIS_THE_FIRST_ROUND_OR_NOT_A_ROUNDS_MISSION()
	OR IS_LOCAL_PLAYER_ANY_SPECTATOR()
	OR IS_PLAYER_SPECTATOR_ONLY(LocalPlayer)
		PRINTLN("[MMacK][RoundsTimeBonus] g_iRoundsCombinedTime cleared!")
		g_iRoundsCombinedTime = 0
		g_iCachedMatchBonus = 0
		
		IF IS_BIT_SET(GlobalplayerBD_FM_2[NATIVE_TO_INT(LocalPlayer)].iMissionDataBitSetTwo, ciMISSION_DATA_TWO_HAS_JOINED_SMALLEST_TEAM)
			CPRINTLN(DEBUG_CONTROLLER, "[RCC MISSION] Clearing ciMISSION_DATA_TWO_HAS_JOINED_SMALLEST_TEAM in first round")
			CLEAR_BIT(GlobalplayerBD_FM_2[NATIVE_TO_INT(LocalPlayer)].iMissionDataBitSetTwo, ciMISSION_DATA_TWO_HAS_JOINED_SMALLEST_TEAM)
		ENDIF
	ENDIF
	
	IF IS_AN_AIRLOCK_STRAND_TRANSITION_RUNNING()
		PRINTLN("[MSRAND] PROCESS_PRE_GAME - Setting g_bStartedInAirlockTransition")
		g_bStartedInAirlockTransition = TRUE
	ENDIF
	
	IF GET_STRAND_MISSION_TRANSITION_TYPE() = FMMC_BRANCHING_MISSION_TRANSITION_TYPE_FADEOUT
		PRINTLN("[MSRAND] PROCESS_PRE_GAME - Setting g_bStartedInFadeTransition")
		g_bStartedInFadeTransition = TRUE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyEight, ciOptionsBS28_DisplayTakeUIFromMissionStart)
		g_TransitionSessionNonResetVars.bDisplayCashGrabTake = TRUE
		PRINTLN("[GrabbedCash] PROCESS_PRE_GAME - Setting bDisplayCashGrabTake immediately due to ciOptionsBS28_DisplayTakeUIFromMissionStart")
	ENDIF

ENDPROC
	
/// PURPOSE:
///    Only special case items should be called from here. For example a player might leave or disconnect during loading up of the mission os process pre game has to call 
///    NETWORK_DISABLE_LEAVE_REMOTE_PED_BEHIND as illustrated with option: ciREMOVE_PED_WHEN_PLAYER_LEAVES
PROC SET_UP_PRE_GAME_LOCAL_PLAYER_INIT()
	
	DISABLE_CUSTOM_WEAPON_LOADOUT_FOR_MISSIONS()

	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciREMOVE_PED_WHEN_PLAYER_LEAVES)
		PRINTLN("[RCC MISSION] PROCESS_PRE_GAME - Calling SET_PLAYER_LEAVE_PED_BEHIND(FALSE) & NETWORK_DISABLE_LEAVE_REMOTE_PED_BEHIND(TRUE)")
		SET_PLAYER_LEAVE_PED_BEHIND(LocalPlayer, FALSE)
		NETWORK_DISABLE_LEAVE_REMOTE_PED_BEHIND(TRUE)
	ENDIF
	
	FORCED_CAMERA_RESET()
	
	CLEAR_AREA_LEAVE_VEHICLE_HEALTH(GET_ENTITY_COORDS(LocalPlayerPed, FALSE), 5000, TRUE)
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciTURN_OFF_VEH_SPAWN)	
		PRINTLN("[RCC MISSION] PRE_GAME suppressing vehicle model SUPPRESS_ALL_HELIS")
		IF g_iNumSupressed < 13
			mnSuppressedVehicles[g_iNumSupressed] = FROGGER
			mnSuppressedVehicles[g_iNumSupressed+1] = FROGGER2
			mnSuppressedVehicles[g_iNumSupressed+2] = VALKYRIE
			mnSuppressedVehicles[g_iNumSupressed+3] = VALKYRIE2
			mnSuppressedVehicles[g_iNumSupressed+4] = SUPERVOLITO
			mnSuppressedVehicles[g_iNumSupressed+5] = SUPERVOLITO2
			mnSuppressedVehicles[g_iNumSupressed+6] = SWIFT
			mnSuppressedVehicles[g_iNumSupressed+7] = SWIFT2
			mnSuppressedVehicles[g_iNumSupressed+8] = SAVAGE
			mnSuppressedVehicles[g_iNumSupressed+9] = SKYLIFT
			mnSuppressedVehicles[g_iNumSupressed+10] = MAVERICK
			VALIDATE_NUMBER_MODELS_SUPRESSED(SUPPRESS_ALL_HELIS(TRUE))
		ELSE
			PRINTLN("[RCC MISSION] PRE_GAME CANNOT DO SUPPRESS_ALL_HELIS via ciTURN_OFF_VEH_SPAWN. TALK TO LUIS OR LUKE. (too many vehicles are being suppressed. ")	
		ENDIF
	ENDIF
	
	IF bIsLocalPlayerHost
		IF g_FMMC_STRUCT.iPedDensity = 0
		AND g_FMMC_STRUCT.iTraffic = 0
		AND MC_serverBD_3.scenBlockServer = NULL
			SERVER_BLOCK_ALL_SCENARIOS(MC_serverBD_3.scenBlockServer)
			PRINTLN("[RCC MISSION] BUG 2615732 SERVER_BLOCK_ALL_SCENARIOS(MC_serverBD_3.scenBlockServer blocked")
		ENDIF
	ENDIF

	SET_LOCAL_PLAYER_CAN_COLLECT_PORTABLE_PICKUPS(FALSE)
	
	IF iPopulationHandle = -1
		fNewPedDensity = GET_PED_DENSITY_FROM_INT(g_FMMC_STRUCT.iPedDensity)
		fNewVehicleDensity = GET_MISSION_VEHICLE_DENSITY_FROM_INT(g_FMMC_STRUCT.iTraffic)
		PRINTLN("[RCC MISSION] BUG 2615732 MC_serverBD.fVehicleDensity= ",fNewVehicleDensity)
		PRINTLN("[RCC MISSION] BUG 2615732 MC_serverBD.fPedDensity= ",fNewPedDensity)
		PRINTLN("[RCC MISSION] BUG 2615732 Setting ped and traffic densities")
		MC_SET_PED_AND_TRAFFIC_DENSITIES(iPopulationHandle, bVehicleGenerator, fNewPedDensity, fNewVehicleDensity)
	ELSE
		PRINTLN("[RCC MISSION] BUG 2615732 iPopulationHandle already exists.")
	ENDIF
ENDPROC

/// PURPOSE:
///    Runs on all clients at startup.  Used to initialize values and set/register variables and such
/// RETURNS:
///    FALSE if the script fails to receive an initial network broadcast.
FUNC BOOL PROCESS_PRE_GAME(MP_MISSION_DATA &fmmcMissionData)
	
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_AutoProfileMissionController") //Intentionally not cached!
		SET_PROFILING_OF_THIS_SCRIPT(TRUE)
	ENDIF
	#ENDIF
	
	NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()

	INT i
	
	PRINTLN("********************* [RCC MISSION] MISSION STARTING PRE-GAME:************************** ",g_FMMC_STRUCT.tl63DebugMissionName)
	
	PRINTLN("[JJTT] PROCESS_PRE_GAME ", MC_serverBD_3.iMultiObjectiveTimeLimit[0], ", ", g_FMMC_STRUCT.iMissionCustomMultiruleTime)
	
	//Cache Player and Ped IDs for use in this function
	PROCESS_CACHING_LOCAL_PLAYER_STATE()
	
	//  --- Anything that needs to be called in pre-game that affects the world or player should be called here ---
	SET_UP_PRE_GAME_LOCAL_PLAYER_INIT()	
	
	IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
		GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].iCurrentMissionType = FMMC_TYPE_MISSION
	ELSE
		g_TransitionSessionNonResetVars.sGlobalCelebrationData.bInTestMode = TRUE
	ENDIF
	
	IF NOT DID_I_JOIN_MISSION_AS_SPECTATOR()
		IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
			IF NOT IS_THIS_A_TEAM_MISSION_WITH_BALANCING_OFF(g_sTransitionSessionOptions)
				IF IS_CORONA_READY_TO_START_WITH_JOB()
					SET_SKYFREEZE_FROZEN()
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	CLEAR_GPS_PLAYER_WAYPOINT()
	
	POPULATE_WARP_LOCATIONS(vBuildingLocations, vPlayerWarpLocation, 0)

	FMMC_PROCESS_PRE_GAME_COMMON( fmmcMissionData, 
								MC_serverBD_1.sFMMC_SBD.iPlayerMissionToLoad, 
								MC_serverBD_1.sFMMC_SBD.iMissionVariation, 
								DEFAULT,
								FALSE, //dont reserve peds
								FALSE, // Don't reserve veh
								DEFAULT,
								MC_ServerBD.iIsHackContainer )
	
	//Cache Host and Particiant ID for use in this function
	PROCESS_CACHING_NETWORK_HOST()
	PROCESS_CACHING_LOCAL_PARTICIPANT_STATE()
	
	IF g_FMMC_STRUCT_ENTITIES.iNumberOfPeds > 0
	OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[0][0].iSpawnBitSet, ci_SpawnBS_AirSpawn)

		INT inumpeds
		IF MC_ServerBD.iEndCutscene != ciMISSION_CUTSCENE_DEFAULT
			inumpeds = (g_FMMC_STRUCT_ENTITIES.iNumberOfPeds+1)
		ELSE
			inumpeds = g_FMMC_STRUCT_ENTITIES.iNumberOfPeds
		ENDIF
		
		IF inumpeds > g_FMMC_STRUCT_ENTITIES.iPedSpawnCap
			PRINTLN("[RCC MISSION] PROCESS_PRE_GAME - Hit ped limit, want ", inumpeds, " peds but can only have ", g_FMMC_STRUCT_ENTITIES.iPedSpawnCap, ".")
			inumpeds = g_FMMC_STRUCT_ENTITIES.iPedSpawnCap
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[0][0].iSpawnBitSet, ci_SpawnBS_AirSpawn)
			inumpeds += 8
		ENDIF
		
		PRINTLN("[RCC MISSION] RESERVE_NETWORK_MISSION_PEDS(", inumpeds, ")")
		RESERVE_NETWORK_MISSION_PEDS(inumpeds)
	ENDIF
	
	INT iNumVeh = 0
	
	IF g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles > 0
	OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[0][0].iSpawnBitSet, ci_SpawnBS_AirSpawn)
		iNumVeh += g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[0][0].iSpawnBitSet, ci_SpawnBS_AirSpawn)
			iNumVeh += 8
		ENDIF
		
		IF USING_FMMC_YACHT()
			INT iYachtIndex = 0 
			FOR iYachtIndex = 0 TO ciYACHT_MAX_YACHTS - 1
				INT iExtraVeh = 0
				FOR iExtraVeh = 0 TO NUMBER_OF_PRIVATE_YACHT_VEHICLES - 1
					IF IS_MODEL_VALID(GET_PRIVATE_YACHT_SPAWN_VEHICLE_MODEL(iExtraVeh))
					AND SHOULD_FMMC_YACHT_EXTRA_VEHICLE_BE_SPAWNED(iYachtIndex, GET_PRIVATE_YACHT_SPAWN_VEHICLE_MODEL(iExtraVeh))
						iNumVeh++
						PRINTLN("Yacht Vehicle added to reserve vehicles pool!")
					ENDIF
				ENDFOR
			ENDFOR
		ENDIF

	ENDIF
	
	#IF IS_DEBUG_BUILD		
	INT iVehicle
	INT iLibrary
	MODEL_NAMES mn 
	REPEAT VEHICLE_LIBRARY_MAX iLibrary
		REPEAT ciMAX_VEHICLES_PER_CREATOR_LIBRARY iVehicle
			
			mn = GET_VEHICLE_MODEL_FROM_CREATOR_SELECTION(iLibrary, iVehicle)
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iAvailableCommonVehicles[0][iLibrary][GET_LONG_BITSET_INDEX(iVehicle)], GET_LONG_BITSET_BIT(iVehicle))				
				PRINTLN("[Personal_Vehicles_Check] - iLibrary: ", iLibrary, " iVehicle: ", iVehicle, ", Name: ", FMMC_GET_MODEL_NAME_FOR_DEBUG(mn), " IS ALLOWED!!")
			ELSE
				PRINTLN("[Personal_Vehicles_Check] - iLibrary: ", iLibrary, " iVehicle: ", iVehicle, ", Name: ", FMMC_GET_MODEL_NAME_FOR_DEBUG(mn), " IS RESTRICTED!!")
			ENDIF
			
		ENDREPEAT
	ENDREPEAT
	#ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.iNumberOfTrains > 0
		iNumVeh += g_FMMC_STRUCT_ENTITIES.iNumberOfTrains * (FMMC_MAX_NUM_OF_TRAIN_CARRIAGES + 1)
	ENDIF
	
	IF iNumVeh > 0
		PRINTLN("RESERVE_NETWORK_MISSION_VEHICLES(", iNumVeh, ")")
		RESERVE_NETWORK_MISSION_VEHICLES(iNumVeh)
	ENDIF
	
	IF GET_NETWORKED_OBJECT_COUNT() > 0
	OR g_FMMC_STRUCT_ENTITIES.iNumberOfVehicleCrates > 0
		INT iNumObj = GET_NETWORKED_OBJECT_COUNT() + g_FMMC_STRUCT_ENTITIES.iNumberOfVehicleCrates
		IF iNumObj > FMMC_MAX_NUM_SPAWNED_NETWORKED_OBJS
			PRINTLN("PROCESS_PRE_GAME - Attempting to reserve ", iNumObj, " objects, capping to ", FMMC_MAX_NUM_SPAWNED_NETWORKED_OBJS)
			iNumObj = FMMC_MAX_NUM_SPAWNED_NETWORKED_OBJS
		ENDIF
		PRINTLN("MC_RESERVE_NETWORK_MISSION_OBJECTS(", iNumObj, ")")
		MC_RESERVE_NETWORK_MISSION_OBJECTS(iNumObj, TRUE)
	ENDIF
		
	IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
		IF INCREMENT_STARTING_ACTIVITY_CHEAT_STAT_BY() > 0
			
			#IF IS_DEBUG_BUILD
			NET_NL()NET_PRINT("[BCCHEAT] PRE_GAME  ")
			NET_PRINT(" g_FMMC_STRUCT.iMissionType  = ")NET_PRINT_INT(g_FMMC_STRUCT.iMissionType)
			NET_PRINT(" g_FMMC_STRUCT.iMissionSubType  = ")NET_PRINT_INT(g_FMMC_STRUCT.iMissionSubType)
			#ENDIF			

			IF g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_CTF
				SET_JOB_ACTIVITY_PROFILE_SETTINGS(PSJA_CAPTURE_STARTED)
			ELIF g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_LTS
				SET_JOB_ACTIVITY_PROFILE_SETTINGS(PSJA_LTS_STARTED)
			ELIF g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_HEIST
				SET_JOB_ACTIVITY_PROFILE_SETTINGS(PSJA_HEIST_STARTED)
			ELSE
				SET_JOB_ACTIVITY_PROFILE_SETTINGS(PSJA_MC_STARTED)
			ENDIF
		ENDIF
	ENDIF
	
	//Call the staggered loading
	IF IS_A_STRAND_MISSION_BEING_INITIALISED()
	AND bIsLocalPlayerHost
		RUN_ALL_STAGGERED_MC_HOST_FUNCTIONS( iCurrentRequestStaggerForStrandMission, iRelGroupStaggerVars, iNumTimesCalledRelGroupFunc )
	ENDIF
	
	//  --- Set up the PRE GAME data BEFORE we have launched/registered as a Network script ---
	RESET_SERVER_NEXT_JOB_VARS()
	
	SET_UP_PRE_GAME_MC_TRANSITION_AND_CONTINUITY_VARS(FALSE)
	
	SET_UP_PRE_GAME_MC_GLOBALS()
		
	CLIENT_PRE_DEFINE_LOCAL_DATA()
	
	CLIENT_PRE_DEFINE_SERVER_DATA()	
	
	//Grab the hash macc and time
	INT iHashMAC = g_MissionControllerserverBD_LB.iHashMAC
	INT iMatchTimeID = g_MissionControllerserverBD_LB.iMatchTimeID 
	PRINTLN("[TS] [MSROUND][MSRAND] - g_MissionControllerserverBD_LB.iHashMAC     = ", g_MissionControllerserverBD_LB.iHashMAC)
	PRINTLN("[TS] [MSROUND][MSRAND] - g_MissionControllerserverBD_LB.iMatchTimeID = ", g_MissionControllerserverBD_LB.iMatchTimeID)
	
	//Clear them 
	g_MissionControllerserverBD_LB.iHashMAC = 0
	g_MissionControllerserverBD_LB.iMatchTimeID = 0 

	DELETE_ANY_OLD_PVS()
	DISABLE_PERSONAL_VEHICLE_CREATION_FOR_MISSION_STARTUP(FALSE) //Legacy bool
	DISABLE_PERSONAL_VEHICLE_AMBIENT_CREATION_DURING_MISSION(TRUE)
	DISABLE_PERSONAL_VEHICLE_CREATION_FOR_MISSION(TRUE)
	
	#IF IS_DEBUG_BUILD
	INT iSize = SIZE_OF(MC_serverBD)
	PRINTLN("[RCC MISSION][BD SIZE] SIZE OF MC_SERVERBD: ", (iSize * 8),"/28000")
	iSize = SIZE_OF(MC_serverBD_1)
	PRINTLN("[RCC MISSION][BD SIZE] SIZE OF MC_SERVERBD_1: ", (iSize * 8),"/13000")
	iSize = SIZE_OF(MC_serverBD_2)
	PRINTLN("[RCC MISSION][BD SIZE] SIZE OF MC_SERVERBD_2: ", (iSize * 8),"/13000")
	iSize = SIZE_OF(MC_serverBD_3)
	PRINTLN("[RCC MISSION][BD SIZE] SIZE OF MC_SERVERBD_3: ", (iSize * 8),"/13000")
	iSize = SIZE_OF(MC_serverBD_4)
	PRINTLN("[RCC MISSION][BD SIZE] SIZE OF MC_SERVERBD_4: ", (iSize * 8),"/28000")
	iSize = SIZE_OF(MC_playerBD) / MAX_NUM_MC_PLAYERS
	PRINTLN("[RCC MISSION][BD SIZE] SIZE OF MC_playerBD: ", (iSize * 8),"/2800")
	iSize = SIZE_OF(MC_playerBD_1) / MAX_NUM_MC_PLAYERS
	PRINTLN("[RCC MISSION][BD SIZE] SIZE OF MC_playerBD_1: ", (iSize * 8),"/2800")
	#ENDIF
	
	PRINTLN("[RCC MISSION][BD SIZE] size of MC_serverBD in ints: ",SIZE_OF(MC_serverBD))
	PRINTLN("[RCC MISSION][BD SIZE] size of MC_serverBD: ",(SIZE_OF(MC_serverBD)*8))
    #IF IS_DEBUG_BUILD
	iSize = SIZE_OF(MC_serverBD)
	IF (iSize * 8) >= 28000
		CLOGLN(DEBUG_MISSION, CHANNEL_SEVERITY_FATAL, "[RCC MISSION][BD SIZE] SIZE OF MC_SERVERBD EXCEEDS ITS LIMIT PLEASE ADD A BUG FOR ONLINE TOOLS")
		ASSERTLN("[RCC MISSION][BD SIZE] SIZE OF MC_SERVERBD EXCEEDS ITS LIMIT PLEASE ADD A BUG FOR ONLINE TOOLS ", (iSize * 8),"/28000")
		PRINTLN("[RCC MISSION][BD SIZE] SIZE OF MC_SERVERBD EXCEEDS ITS LIMIT PLEASE ADD A BUG FOR ONLINE TOOLS ", (iSize * 8),"/28000")
		#IF IS_DEBUG_BUILD
		ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_EXCEEDING_SERVER_BD_0, ENTITY_RUNTIME_ERROR_TYPE_CRITICAL, "MC_SERVERBD EXCEEDS LIMIT ADD A BUG FOR ONLINE TOOLS #/28000", (iSize * 8))
		#ENDIF
		RETURN FALSE
	ENDIF
	#ENDIF
	PRINTLN("[RCC MISSION][BD SIZE] size of MC_serverBD_1 in ints: ",SIZE_OF(MC_serverBD_1))
	PRINTLN("[RCC MISSION][BD SIZE] size of MC_serverBD_1: ",(SIZE_OF(MC_serverBD_1)*8))
    #IF IS_DEBUG_BUILD
	iSize = SIZE_OF(MC_serverBD_1)
	IF (iSize * 8) >= 13000
		CLOGLN(DEBUG_MISSION, CHANNEL_SEVERITY_FATAL, "[RCC MISSION][BD SIZE] SIZE OF MC_SERVERBD_1 EXCEEDS ITS LIMIT PLEASE ADD A BUG FOR ONLINE TOOLS")
		ASSERTLN("[RCC MISSION][BD SIZE] SIZE OF MC_SERVERBD_1 EXCEEDS ITS LIMIT PLEASE ADD A BUG FOR ONLINE TOOLS ", (iSize * 8),"/13000")
		PRINTLN("[RCC MISSION][BD SIZE] SIZE OF MC_SERVERBD_1 EXCEEDS ITS LIMIT PLEASE ADD A BUG FOR ONLINE TOOLS ", (iSize * 8),"/13000")
		#IF IS_DEBUG_BUILD
		ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_EXCEEDING_SERVER_BD_1, ENTITY_RUNTIME_ERROR_TYPE_CRITICAL, "MC_SERVERBD_1 EXCEEDS LIMIT ADD A BUG FOR ONLINE TOOLS #/13000", (iSize * 8))
		#ENDIF
		RETURN FALSE
	ENDIF
	#ENDIF
	PRINTLN("[RCC MISSION][BD SIZE] size of MC_serverBD_2 in ints: ",SIZE_OF(MC_serverBD_2))
	PRINTLN("[RCC MISSION][BD SIZE] size of MC_serverBD_2: ",(SIZE_OF(MC_serverBD_2)*8))
    #IF IS_DEBUG_BUILD
	iSize = SIZE_OF(MC_serverBD_2)
	IF (iSize * 8) >= 13000
		CLOGLN(DEBUG_MISSION, CHANNEL_SEVERITY_FATAL, "[RCC MISSION] SIZE OF MC_SERVERBD_2 EXCEEDS ITS LIMIT PLEASE ADD A BUG FOR ONLINE TOOLS")
		ASSERTLN("[RCC MISSION][BD SIZE] SIZE OF MC_SERVERBD_2 EXCEEDS ITS LIMIT PLEASE ADD A BUG FOR ONLINE TOOLS ", (iSize * 8),"/13000")
		PRINTLN("[RCC MISSION][BD SIZE] SIZE OF MC_SERVERBD_2 EXCEEDS ITS LIMIT PLEASE ADD A BUG FOR ONLINE TOOLS ", (iSize * 8),"/13000")
		#IF IS_DEBUG_BUILD
		ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_EXCEEDING_SERVER_BD_2, ENTITY_RUNTIME_ERROR_TYPE_CRITICAL, "MC_SERVERBD_2 EXCEEDS LIMIT ADD A BUG FOR ONLINE TOOLS #/13000", (iSize * 8))
		#ENDIF
		RETURN FALSE
	ENDIF
	#ENDIF
	PRINTLN("[RCC MISSION][BD SIZE] size of MC_serverBD_3 in ints: ",SIZE_OF(MC_serverBD_3))
	PRINTLN("[RCC MISSION][BD SIZE] size of MC_serverBD_3: ",(SIZE_OF(MC_serverBD_3)*8))
    #IF IS_DEBUG_BUILD
	iSize = SIZE_OF(MC_serverBD_3)
	IF (iSize * 8) >= 13000
		CLOGLN(DEBUG_MISSION, CHANNEL_SEVERITY_FATAL, "[RCC MISSION] SIZE OF MC_SERVERBD_3 EXCEEDS ITS LIMIT PLEASE ADD A BUG FOR ONLINE TOOLS")
		ASSERTLN("[RCC MISSION][BD SIZE] SIZE OF MC_SERVERBD_3 EXCEEDS ITS LIMIT PLEASE ADD A BUG FOR ONLINE TOOLS ", (iSize * 8),"/13000")
		PRINTLN("[RCC MISSION][BD SIZE] SIZE OF MC_SERVERBD_3 EXCEEDS ITS LIMIT PLEASE ADD A BUG FOR ONLINE TOOLS ", (iSize * 8),"/13000")
		#IF IS_DEBUG_BUILD
		ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_EXCEEDING_SERVER_BD_3, ENTITY_RUNTIME_ERROR_TYPE_CRITICAL, "MC_SERVERBD_3 EXCEEDS LIMIT ADD A BUG FOR ONLINE TOOLS #/13000", (iSize * 8))
		#ENDIF
		RETURN FALSE
	ENDIF
	#ENDIF
	PRINTLN("[RCC MISSION][BD SIZE] size of MC_serverBD_4 in ints: ",SIZE_OF(MC_serverBD_4))
	PRINTLN("[RCC MISSION][BD SIZE] size of MC_serverBD_4: ",(SIZE_OF(MC_serverBD_4)*8))
    #IF IS_DEBUG_BUILD
	iSize = SIZE_OF(MC_serverBD_4)
	IF (iSize * 8) >= 28000
		CLOGLN(DEBUG_MISSION, CHANNEL_SEVERITY_FATAL, "[RCC MISSION] SIZE OF MC_SERVERBD_4 EXCEEDS ITS LIMIT PLEASE ADD A BUG FOR ONLINE TOOLS")
		ASSERTLN("[RCC MISSION][BD SIZE] SIZE OF MC_SERVERBD_4 EXCEEDS ITS LIMIT PLEASE ADD A BUG FOR ONLINE TOOLS ", (iSize * 8),"/28000")
		PRINTLN("[RCC MISSION][BD SIZE] SIZE OF MC_SERVERBD_4 EXCEEDS ITS LIMIT PLEASE ADD A BUG FOR ONLINE TOOLS ", (iSize * 8),"/28000")
		#IF IS_DEBUG_BUILD
		ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_EXCEEDING_SERVER_BD_4, ENTITY_RUNTIME_ERROR_TYPE_CRITICAL, "MC_SERVERBD_4 EXCEEDS LIMIT ADD A BUG FOR ONLINE TOOLS #/28000", (iSize * 8))
		#ENDIF
		RETURN FALSE
	ENDIF
	#ENDIF
	
	PRINTLN("[RCC MISSION][BD SIZE] size of MC_playerBD in ints: ",SIZE_OF(MC_playerBD) / MAX_NUM_MC_PLAYERS)
	PRINTLN("[RCC MISSION][BD SIZE] size of MC_playerBD: ",((SIZE_OF(MC_playerBD) / MAX_NUM_MC_PLAYERS) * 8))
    #IF IS_DEBUG_BUILD
	iSize = SIZE_OF(MC_playerBD) / MAX_NUM_MC_PLAYERS
	IF (iSize * 8) >= 2800
		CLOGLN(DEBUG_MISSION, CHANNEL_SEVERITY_FATAL, "[RCC MISSION] SIZE OF MC_playerBD EXCEEDS ITS LIMIT PLEASE ADD A BUG FOR ONLINE TOOLS")
		ASSERTLN("[RCC MISSION][BD SIZE] SIZE OF MC_playerBD EXCEEDS ITS LIMIT PLEASE ADD A BUG FOR ONLINE TOOLS ", (iSize * 8),"/2800")
		PRINTLN("[RCC MISSION][BD SIZE] SIZE OF MC_playerBD EXCEEDS ITS LIMIT PLEASE ADD A BUG FOR ONLINE TOOLS ", (iSize * 8),"/2800")
		#IF IS_DEBUG_BUILD
		ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_EXCEEDING_PLAYER_BD_0, ENTITY_RUNTIME_ERROR_TYPE_CRITICAL, "MC_playerBD_0 EXCEEDS LIMIT ADD A BUG FOR ONLINE TOOLS #/2800", (iSize * 8))
		#ENDIF
		RETURN FALSE
	ENDIF
	#ENDIF
	PRINTLN("[RCC MISSION][BD SIZE] size of MC_playerBD_1 in ints: ",SIZE_OF(MC_playerBD_1) / MAX_NUM_MC_PLAYERS)
	PRINTLN("[RCC MISSION][BD SIZE] size of MC_playerBD_1: ",((SIZE_OF(MC_playerBD_1) / MAX_NUM_MC_PLAYERS) *8))
    #IF IS_DEBUG_BUILD
	iSize = SIZE_OF(MC_playerBD_1) / MAX_NUM_MC_PLAYERS
	IF (iSize * 8) >= 2800
		CLOGLN(DEBUG_MISSION, CHANNEL_SEVERITY_FATAL, "[RCC MISSION] SIZE OF MC_playerBD_1 EXCEEDS ITS LIMIT PLEASE ADD A BUG FOR ONLINE TOOLS")
		ASSERTLN("[RCC MISSION][BD SIZE] SIZE OF MC_playerBD_1 EXCEEDS ITS LIMIT PLEASE ADD A BUG FOR ONLINE TOOLS ", (iSize * 8),"/2800")
		PRINTLN("[RCC MISSION][BD SIZE] SIZE OF MC_playerBD_1 EXCEEDS ITS LIMIT PLEASE ADD A BUG FOR ONLINE TOOLS ", (iSize * 8),"/2800")
		#IF IS_DEBUG_BUILD
		ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_EXCEEDING_PLAYER_BD_1, ENTITY_RUNTIME_ERROR_TYPE_CRITICAL, "MC_playerBD_1 EXCEEDS LIMIT ADD A BUG FOR ONLINE TOOLS #/2800", (iSize * 8))
		#ENDIF
		RETURN FALSE
	ENDIF
	#ENDIF
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(MC_serverBD, SIZE_OF(MC_serverBD), "MC_serverBD")
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(MC_serverBD_1, SIZE_OF(MC_serverBD_1), "MC_serverBD_1")
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(g_sMC_serverBDEndJob, SIZE_OF(g_sMC_serverBDEndJob), "g_sMC_serverBDEndJob")
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(MC_serverBD_2, SIZE_OF(MC_serverBD_2), "MC_serverBD_2")
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(MC_serverBD_4, SIZE_OF(MC_serverBD_4), "MC_serverBD_4")
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(MC_serverBD_3, SIZE_OF(MC_serverBD_3), "MC_serverBD_3")
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(g_MissionControllerserverBD_LB, SIZE_OF(g_MissionControllerserverBD_LB), "g_MissionControllerserverBD_LB")
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(MC_playerBD, SIZE_OF(MC_playerBD), "MC_playerBD")
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(MC_playerBD_1, SIZE_OF(MC_playerBD_1), "MC_playerBD_1")
	
	// KGM: Wait for the first network broadcast before moving on
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		RETURN FALSE
	ENDIF
	
	IF ARE_JIP_SPECTATORS_BLOCKED_ON_CURRENT_MISSION()
	AND DID_I_JOIN_MISSION_AS_SPECTATOR() 
		SET_TRANSITION_SESSIONS_JIP_FAILED()
		PRINTLN("[RCC MISSION] NETWORK_BAIL - JIP Spectators are not allowed on this mission as it breaks the strand cutscene.")
		DEBUG_PRINTCALLSTACK()
		NETWORK_BAIL(ENUM_TO_INT(NETWORK_BAIL_TS_SAFETY_BAIL))
		RETURN FALSE
	ENDIF	
	
	CLIENT_INITIALIZE_BROADCAST_DATA()
	
	//If it's a stransd mission then set our team early
	IF IS_A_STRAND_MISSION_BEING_INITIALISED()
		IF bIsLocalPlayerHost
			RUN_ALL_STAGGERED_MC_HOST_FUNCTIONS( iCurrentRequestStaggerForStrandMission, iRelGroupStaggerVars, iNumTimesCalledRelGroupFunc )
		ENDIF
		IF g_TransitionSessionNonResetVars.previousMissionWeapon != WEAPONTYPE_INVALID
			SET_PLAYER_CURRENT_HELD_WEAPON(g_TransitionSessionNonResetVars.previousMissionWeapon)
			g_TransitionSessionNonResetVars.previousMissionWeapon = WEAPONTYPE_INVALID
		ENDIF		
	ENDIF
	
	CLEANUP_STAND_ALONE_CELEBRATION_SCREEN_GLOBALS()
	
	//Set the local BD data
	IF bIsLocalPlayerHost
		IF IS_THIS_A_ROUNDS_MISSION_FOR_CORONA()
			//reinitilise that data if it's a rounds mission
			g_MissionControllerserverBD_LB.iHashMAC = iHashMAC
			g_MissionControllerserverBD_LB.iMatchTimeID = iMatchTimeID 	
			PRINTLN("[TS] [MSROUND] - IS_THIS_A_ROUNDS_MISSION_FOR_CORONA - g_MissionControllerserverBD_LB.iHashMAC     = ", g_MissionControllerserverBD_LB.iHashMAC)
			PRINTLN("[TS] [MSROUND] - IS_THIS_A_ROUNDS_MISSION_FOR_CORONA - g_MissionControllerserverBD_LB.iMatchTimeID = ", g_MissionControllerserverBD_LB.iMatchTimeID)
			PRINTLN("[TS] [MSROUND] - Set Local BD data")
			PRINTLN("[PLAYER_LOOP] - PREGAME")
			FOR i = 0 TO (MAX_NUM_MC_PLAYERS - 1)
				g_MissionControllerserverBD_LB.sleaderboard[i].iRoundsScore = GlobalplayerBD_FM_2[i].sJobRoundData.iRoundScore
				#IF IS_DEBUG_BUILD
				IF g_MissionControllerserverBD_LB.sleaderboard[i].iRoundsScore != 0
					PRINTLN("[TS] [MSROUND] - MC_PlayerBD[i].iRoundsScore = g_MissionControllerserverBD_LB.sleaderboard[", i, "].iRoundsScore = ", g_MissionControllerserverBD_LB.sleaderboard[i].iRoundsScore)
				ENDIF
				#ENDIF
			ENDFOR
		ELIF IS_A_STRAND_MISSION_BEING_INITIALISED()
			//reinitilise that data if it's a rounds mission
			g_MissionControllerserverBD_LB.iHashMAC = iHashMAC
			g_MissionControllerserverBD_LB.iMatchTimeID = iMatchTimeID 	
			PRINTLN("[TS] [MSRAND] - IS_A_STRAND_MISSION_BEING_INITIALISED - g_MissionControllerserverBD_LB.iHashMAC     = ", g_MissionControllerserverBD_LB.iHashMAC)
			PRINTLN("[TS] [MSRAND] - IS_A_STRAND_MISSION_BEING_INITIALISED - g_MissionControllerserverBD_LB.iMatchTimeID = ", g_MissionControllerserverBD_LB.iMatchTimeID)
		ENDIF
	ENDIF
		
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[GET_PRE_ASSIGNMENT_PLAYER_TEAM()].iTeamBitset2, ciBS2_TEAM_ONE_PLAYER_RAND_SPAWN)
		INT iTeamSlot = 0
		FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[GET_PRE_ASSIGNMENT_PLAYER_TEAM()]-1
			INT iRandom = GET_RANDOM_INT_IN_RANGE(0, g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[GET_PRE_ASSIGNMENT_PLAYER_TEAM()])
			IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[GET_PRE_ASSIGNMENT_PLAYER_TEAM()][iRandom].vPos)
				PRINTLN("[RCC MISSION] SET_UP_CUSTOM_SPAWN_POINTS set random spawn point as ", iRandom)
				iTeamSlot = iRandom
			ELSE
				PRINTLN("[RCC MISSION] SET_UP_CUSTOM_SPAWN_POINTS invalid random spawn point as ", iRandom)
			ENDIF
		ENDFOR
		
		INT iCheck
		FOR iCheck = 0 TO 31
			IF IS_BIT_SET(MC_ServerBD.iSpawnPointsUsed[GET_PRE_ASSIGNMENT_PLAYER_TEAM()], iCheck)
				iTeamSlot = iCheck
			ENDIF
			IF IS_BIT_SET(LocalRandomSpawnBitset[GET_PRE_ASSIGNMENT_PLAYER_TEAM()], iCheck)
				CLEAR_BIT(LocalRandomSpawnBitset[GET_PRE_ASSIGNMENT_PLAYER_TEAM()], iCheck)
			ENDIF
		ENDFOR
		
		BROADCAST_RANDOM_SPAWN_SLOT_USED_FOR_TEAM(GET_PRE_ASSIGNMENT_PLAYER_TEAM(), iteamslot)
		SET_BIT(LocalRandomSpawnBitset[GET_PRE_ASSIGNMENT_PLAYER_TEAM()], iteamslot)
		
	ENDIF
	
	SET_FM_MISSION_LAUNCHED_SUCESS(fmmcMissionData.mdID.idCreator, fmmcMissionData.mdID.idVariation, fmmcMissionData.iInstanceId)
		
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTen, ciHUD_HIDE_EXTRA_HUD_AT_START)
		SET_BIT(iLocalBoolCheck19, LBOOL19_SHOULD_EXTRA_HUD_BE_SHOWING)
	ENDIF
	
	IF IS_THIS_A_QUICK_RESTART_JOB()
	OR NOT IS_A_STRAND_MISSION_BEING_INITIALISED()	
		CREATE_FM_MISSION_RELATIONSHIP_GROUPS()
		CREATE_FMMC_CUSTOM_RELATIONSHIP_GROUPS()
	ENDIF
		
	INT iRank = GET_HIGHEST_TRANSITION_PLAYER_RANK() 	
	//store out the rank if it's a rounds mission
	IF bIsLocalPlayerHost
	AND g_sTransitionSessionData.sMissionRoundData.iNumberOfRounds != 0
	AND NOT IS_THIS_A_ROUNDS_MISSION_FOR_CORONA()
		PRINTLN("[TS] [MSROUND] - g_MissionControllerserverBD_LB.iHighestRankInSession = ", iRank)
		g_MissionControllerserverBD_LB.iHighestRankInSession = iRank
	ENDIF
	
	PRINTLN("[MSROUND] iNumberOfRoundsPlayed = ", g_sTransitionSessionData.sMissionRoundData.iNumberOfRoundsPlayed)
	PRINTLN("[MSROUND] iNumberOfRounds = ", g_sTransitionSessionData.sMissionRoundData.iNumberOfRounds)

	SET_CHECK_RANK_CREATE_PICKUP_GLOBALS(iRank)
	
	PROCESS_AUDIO_INIT()
	
	//  --- Set up the PRE GAME data AFTER we have launched/registered as a Network script ---
	
	SET_UP_PRE_GAME_MC_TRANSITION_AND_CONTINUITY_VARS(TRUE)
	
	SET_USE_DLC_DIALOGUE(TRUE)
	
	LOAD_MC_TEXT_BLOCK()
	
	LOAD_PARTICLE_FX()
	
	LOAD_ACTION_ANIMS()	
	
	// Used for all missions classed as a Heist. Gangops/Casino included.
	#IF IS_DEBUG_BUILD
	IF bHeistHashedMac
		IF bIsLocalPlayerHost
			SEND_EVENT_HEIST_POSIX_AND_HASHED_MAC()
		ENDIF
	ENDIF
	#ENDIF
	
	INITIALISE_INT_ARRAY(iEveryFramePropIndices, -1)
	iNumEveryFrameProps = 0
	FOR i = 0 TO GET_FMMC_MAX_NUM_PROPS() - 1
		IF DOES_PROP_NEED_PROCESSED_EVERY_FRAME(i)
			iEveryFramePropIndices[iNumEveryFrameProps] = i
			iNumEveryFrameProps++
		ENDIF
		
		IF iNumEveryFrameProps >= ciMAX_EVERY_FRAME_PROPS
			ASSERTLN("TOO MANY EVERY FRAME PROPS - BUG ONLINE TOOLS")
			#IF IS_DEBUG_BUILD
			ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_TOO_MANY_EVERY_FRAME_PROPS, ENTITY_RUNTIME_ERROR_TYPE_WARNING_PROP, "TOO MANY EVERY FRAME PROPS - BUG ONLINE TOOLS!", iNumEveryFrameProps)
			#ENDIF
			BREAKLOOP
		ENDIF
	ENDFOR
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_TRANSFORM_ASSETS)
		PRINTLN("[RCC MISSION] PRE GAME - Beast Mode can be active")
		SET_BIT(iLocalBoolCheck2, LBOOL2_BEAST_MODE_ACTIVE_ON_MISSION)
	ELSE
		FOR i = 0 TO FMMC_MAX_TEAMS - 1
			INT iRule
			FOR iRule = 0 TO FMMC_MAX_RULES - 1
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[i].iRuleBitSetFive[iRule], ciBS_RULE5_ENABLE_BEAST_MODE)
					PRINTLN("[RCC MISSION] PRE GAME - Beast Mode can be active for Team: ", i)
					SET_BIT(iLocalBoolCheck2, LBOOL2_BEAST_MODE_ACTIVE_ON_MISSION)
					BREAKLOOP
				ENDIF
			ENDFOR
		ENDFOR
	ENDIF
	
	FOR i = 0 TO FMMC_MAX_GO_TO_LOCATIONS - 1
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].iLocBS2, ciLoc_BS2_CrossLineDistUseMe_T0)
		OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].iLocBS2, ciLoc_BS2_CrossLineDistUseMe_T1)
		OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].iLocBS2, ciLoc_BS2_CrossLineDistUseMe_T2)
		OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].iLocBS2, ciLoc_BS2_CrossLineDistUseMe_T3)
			PRINTLN("[RCC MISSION] PRE GAME - Line Marker can be active for Loc: ", i)
			SET_BIT(iLocalBoolCheck2, LBOOL2_LINE_MARKER_ACTIVE_ON_MISSION)
		ENDIF
		
		vGoToLocateVectors[i] = GET_INIT_LOCATION_VECTOR(i)
		GET_INIT_LOCATION_SIZE_AND_HEIGHT(i, fGoToLocateSize[i], fGoToLocateHeight[i])
	ENDFOR
		
	FOR i = 0 TO FMMC_MAX_VEHICLES - 1
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehBitsetSeven, ciFMMC_VEHICLE7_SPAWN_BURNING)
		AND NOT DOES_VEHICLE_MODEL_REQUIRE_ALTERNATIVE_SMOKE_VFX(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn)
			SET_BIT(iLocalBoolCheck27, LBOOL27_PROCESS_BURNING_VANS)
			PRINTLN("[RCC MISSION] PRE GAME - Vehicle: ", i, ". Burning vehicles will be processed.")
			BREAKLOOP
		ENDIF
	ENDFOR
	
	//Stop the PV system trying to put the players in the PVs once created.
	g_TransitionSessionNonResetVars.sEomAbmientVehicleData.bFinishedInPersonalVehicle = FALSE
	g_TransitionSessionNonResetVars.sEomAbmientVehicleData.bFinishedInSomeoneElsesPersonalVehicle = FALSE
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyNine, ciOptionsBS29_DisableInternetApp)
		BLOCK_INTERNET_APP(TRUE)
	ENDIF
	
	g_iFMMC_SkipPedScenario = -1
	
	SET_BIT(MC_PlayerBD[NATIVE_TO_INT(LocalPlayer)].iClientBitSet3, PBBOOL3_PROCESSED_PRE_GAME)
	
	RETURN TRUE
ENDFUNC
