// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### System: GTA V Online Mission Controller - Event Processing ------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Script Name: FM_Mission_Controller_2020.sc ------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Processing of events sent in the Mission Controller.		
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### This script was forked from FM_Mission_Controller.sc at the beginning of 2020		 																				  
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

USING "FM_Mission_Controller_Init_2020.sch"
USING "FM_Mission_Controller_LocalPlayer_2020.sch"
USING "FM_Mission_Controller_Rules_2020.sch"
USING "FM_Mission_Controller_Celebrations_2020.sch"
USING "FM_Mission_Controller_DialogueTriggers_2020.sch"
USING "FM_Mission_Controller_Players_2020.sch"
USING "FM_Mission_Controller_HUDProcessing_2020.sch"
USING "FM_Mission_Controller_GangChase_2020.sch"
USING "FM_Mission_Controller_Variations_2020.sch"

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Audio	 -------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Processes for Audio functionality  			--------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_PICKUP_RESPAWN_SOUND(INT iEventID)
	STRUCT_PICKUP_RESPAWN_EVENT pickup
	STRING sPickUpSoundSet

	sPickUpSoundSet = "POWER_PLAY_General_Soundset"
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, pickup, SIZE_OF(pickup))

		#IF IS_DEBUG_BUILD
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciENABLE_POWERPLAY_PICKUP_SOUNDS)
				VECTOR vTemp = GET_PICKUP_COORDS(pickup.PickupIndex)
				PRINTLN("[RCC MISSION] PROCESS_PICKUP_RESPAWN_SOUND - Pickup respawned at: ", vTemp, ". Playing sound.")
			ENDIF
		#ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciENABLE_POWERPLAY_PICKUP_SOUNDS)
			IF pickup.PickupType = PICKUP_CUSTOM_SCRIPT
				PRINTLN("[KH] PROCESS_PICKUP_RESPAWN_SOUND - pickup.PickupType = PICKUP_CUSTOM_SCRIPT - Playing power up respawn sound")
				PLAY_SOUND_FROM_COORD(-1, "Powerup_Respawn", GET_PICKUP_COORDS(pickup.PickupIndex), sPickUpSoundSet)
			ELSE
				PRINTLN("[KH] PROCESS_PICKUP_RESPAWN_SOUND - pickup.PickupType != PICKUP_CUSTOM_SCRIPT - Playing weapon respawn sound")
				PLAY_SOUND_FROM_COORD(-1, "Weapon_Respawn", GET_PICKUP_COORDS(pickup.PickupIndex), sPickUpSoundSet)
			ENDIF
		ENDIF
	ELSE
		SCRIPT_ASSERT("PROCESS_PICKUP_RESPAWN_SOUND - could not retreive data.")
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_START_PRE_COUNTDOWN_STOP(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_START_PRE_COUNTDOWN_STOP EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		PRINTLN("[RCC MISSION][AUDIO] PROCESS_SCRIPT_EVENT_FMMC_START_PRE_COUNTDOWN_STOP - Triggering APT_PRE_COUNTDOWN_STOP")
		TRIGGER_MUSIC_EVENT("APT_PRE_COUNTDOWN_STOP")
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_LOCATE_ALERT_SOUND(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_LOCATE_ALERT_SOUND EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.FromPlayerIndex != LocalPlayer
		AND NOT bIsAnySpectator
			PRINTLN("PROCESS_SCRIPT_EVENT_FMMC_LOCATE_ALERT_SOUND - Playing police_notification from DLC_AS_VNT_Sounds")
			PLAY_SOUND_FRONTEND(-1,"police_notification","DLC_AS_VNT_Sounds")
		ENDIF
	ENDIF	
ENDPROC	

PROC PROCESS_SCRIPT_EVENT_FMMC_PLAY_PULSE_SFX(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_PLAY_PULSE_SFX EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_PLAY_PULSE_SFX
			PRINTLN("[RCC MISSION] - Playing Bounds_Timer_Reset. (Client)")			
			PLAY_SOUND_FRONTEND(-1, "Bounds_Timer_Reset", "DLC_SM_VEHWA_Player_Sounds", FALSE)			
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_AUDIO_TRIGGER_ACTIVATED(INT iEventID)
	PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_AUDIO_TRIGGER_ACTIVATED] - Processing...")
	
	SCRIPT_EVENT_DATA_FMMC_AUDIO_TRIGGER_ACTIVATED EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_AUDIO_TRIGGER_ACTIVATED
			IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[EventData.iProp].iSTMSoundIDVar = -1
				g_FMMC_STRUCT_ENTITIES.sPlacedProp[EventData.iProp].iSTMSoundIDVar = GET_SOUND_ID()
			ENDIF
			
			PLAY_SOUND_FROM_COORD(g_FMMC_STRUCT_ENTITIES.sPlacedProp[EventData.iProp].iSTMSoundIDVar, GET_SOUND_STRING_FROM_SOUND_ID(EventData.iSoundID), EventData.vPlayFromPos, "DLC_Stunt_Race_Stinger_Sounds")
			PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_AUDIO_TRIGGER_ACTIVATED_RACES] - Trigger counter a : ", g_FMMC_STRUCT_ENTITIES.sPlacedProp[EventData.iProp].iSTMTriggerCounter)
			g_FMMC_STRUCT_ENTITIES.sPlacedProp[EventData.iProp].iSTMTriggerCounter++
			PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_AUDIO_TRIGGER_ACTIVATED_RACES] - Trigger counter b : ", g_FMMC_STRUCT_ENTITIES.sPlacedProp[EventData.iProp].iSTMTriggerCounter)
		ENDIF	
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_SET_BILLIONAIRE_PARTY_STATE(INT iCount)
	SCRIPT_EVENT_DATA_FMMC_SET_BILLIONAIRE_PARTY_STATE EventData
	IF NOT GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, EventData, SIZE_OF(EventData))
	OR EventData.Details.Type != SCRIPT_EVENT_FMMC_SET_BILLIONAIRE_PARTY_STATE
		EXIT
	ENDIF
	
	SET_BILLIONAIRE_PARTY_MUSIC_STATE(EventData.eNewState)
	
ENDPROC	


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Cutscenes		 -----------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Processes for cutscene functionality  				------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


PROC PROCESS_TEAM_HAS_FINISHED_CUTSCENE( int icount )

	SCRIPT_EVENT_DATA_FMMC_TEAM_HAS_FINISHED_CUTSCENE CutsceneData
	
	IF GET_EVENT_DATA( SCRIPT_EVENT_QUEUE_NETWORK, iCount, CutsceneData, SIZE_OF( CutsceneData ) )

		IF CutsceneData.Details.Type = SCRIPT_EVENT_FMMC_TEAM_HAS_FINISHED_CUTSCENE

			PRINTLN("[RCC MISSION] PROCESS_TEAM_HAS_FINISHED_CUTSCENE - CutsceneData.iTeam = ", CutsceneData.iTeam) 
			
			INT iSpectatorTargetPart = -1
			
			IF bIsAnySpectator
				iSpectatorTargetPart = GET_SPECTATOR_TARGET_PARTICIPANT_OF_PARTICIPANT( iLocalPart )
			ENDIF
			
			IF CutsceneData.iTeam = MC_PlayerBD[iLocalPart].iTeam
			OR (iSpectatorTargetPart != -1 AND CutsceneData.iTeam = MC_PlayerBD[iSpectatorTargetPart].iTeam)
				CLEAR_BIT(MC_PlayerBD[iLocalPart].iClientBitset2, PBBOOL2_STARTED_CUTSCENE)
				CLEAR_BIT(MC_PlayerBD[iLocalPart].iClientBitset2, PBBOOL2_FINISHED_CUTSCENE)
				CLEAR_BIT(MC_PlayerBD[iLocalPart].iClientBitset2, PBBOOL2_I_AM_SPECTATOR_WATCHING_CUTSCENE)
				CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_REQUEST_CUTSCENE_PLAYERS)
				PRINTLN("[RCC MISSION] PROCESS_TEAM_HAS_FINISHED_CUTSCENE - clearing bits for  CutsceneData.iTeam = ", CutsceneData.iTeam) 
			ENDIF

		ENDIF
	ENDIF

ENDPROC

//Process the players weapon for a cut scene event
PROC PROCESS_SCRIPT_EVENT_FMMC_MY_WEAPON_DATA( INT iCount )
	SCRIPT_EVENT_DATA_FMMC_MY_WEAPON_DATA sWeaponData	
	IF GET_EVENT_DATA( SCRIPT_EVENT_QUEUE_NETWORK, iCount, sWeaponData, SIZE_OF( sWeaponData ) )
		
		// Expand this for multiple players to set their own weapon here.
		INT i
		FOR i = 0 TO (ciMAX_CUT_PLAYERS - 1)
			sPlayerWeaponDetailsForCutscene[i].bHasThisWeapon = sWeaponData.bHasWeapon
			sPlayerWeaponDetailsForCutscene[i].sWeaponInfo	= sWeaponData.sWeaponInfo			
			sPlayerWeaponDetailsForCutscene[i].iParticipant = sWeaponData.iParticipant
			sPlayerWeaponDetailsForCutscene[i].bSet = TRUE
			PRINTLN("[Cutscene] - PROCESS_SCRIPT_EVENT_FMMC_MY_WEAPON_DATA - sPlayerWeaponDetailsForCutscene[", i, "], bHasWeapon = ", BOOL_TO_STRING( sWeaponData.bHasWeapon ), ", participant = ", sWeaponData.iParticipant )			
		ENDFOR
		
	ENDIF
ENDPROC

PROC PROCESS_FMMC_CUTSCENE_SPAWN_PROGRESS(INT iCount)

	SCRIPT_EVENT_DATA_CUTSCENE_SPAWN_PROGRESS CutsceneData
	
	IF bIsLocalPlayerHost
		IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, CutsceneData, SIZE_OF(CutsceneData))
			IF CutsceneData.Details.Type = SCRIPT_EVENT_FMMC_CUTSCENE_SPAWN_PROGRESS
				MC_serverBD.iSpawnScene = CutsceneData.iCutscene
				MC_serverBD.iSpawnShot = CutsceneData.iCutsceneCamshot
				PRINTLN("[RCC MISSION] cutscene : ",CutsceneData.iCutscene," SCRIPT_EVENT_DATA_CUTSCENE_SPAWN_PROGRESS cam shot: ",CutsceneData.iCutsceneCamshot)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_MOCAP_PLAYER_ANIMATION(INT iEventID)
	
	EVENT_STRUCT_MOCAP_PLAYER_ANIM Event
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))
	
		// Check if we are in same vote
		PRINTLN("[RCC MISSION]  PROCESS_MOCAP_PLAYER_ANIMATION - ")
		PRINTLN("[RCC MISSION] 			Player: ", NATIVE_TO_INT(Event.Details.FromPlayerIndex))
		PRINTLN("[RCC MISSION] 			Anim Type: ", Event.iAnimationType)
		PRINTLN("[RCC MISSION] 			Anim: ", Event.iAnimation)
		PRINTLN("[RCC MISSION] 			bPlaying: ", Event.bPlaying)
		
		INT iPlayerInt = NATIVE_TO_INT(Event.Details.FromPlayerIndex)
						
		// Update globals so we process new animations
		MPGlobalsInteractions.PedInteraction[iPlayerInt].iInteractionType =  Event.iAnimationType
		MPGlobalsInteractions.PedInteraction[iPlayerInt].iInteractionAnim = Event.iAnimation
		IF Event.bPlaying
			MPGlobalsInteractions.PedInteraction[iPlayerInt].bPlayInteractionAnim = Event.bPlaying
		ENDIF
		MPGlobalsInteractions.PedInteraction[iPlayerInt].bHoldLoop = Event.bPlaying

	ELSE
		SCRIPT_ASSERT("[RCC MISSION] PROCESS_MOCAP_PLAYER_ANIMATION - could not retrieve data")
	ENDIF
	
ENDPROC

PROC PROCESS_START_STREAMING_END_MOCAP_EVENT( INT iEventID )
	EVENT_STRUCT_STREAM_END_MOCAP_DATA Event
	
	IF GET_EVENT_DATA( SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF( Event ) )

		PRINTLN("[RCC MISSION]  PROCESS_START_STREAMING_END_MOCAP_EVENT - Setting bit ")
		SET_BIT( iLocalBoolCheck7, LBOOL7_TOGGLED_RENDERPHASES_FOR_DROPOFF )
		#IF IS_DEBUG_BUILD
		IF IS_NET_PLAYER_OK(event.Details.FromPlayerIndex)
			PRINTLN("[RCC MISSION]  PROCESS_START_STREAMING_END_MOCAP_EVENT recieved from = ",GET_PLAYER_NAME(event.Details.FromPlayerIndex))
		ELSE
			PRINTLN("[RCC MISSION]  PROCESS_START_STREAMING_END_MOCAP_EVENT recieved from = ",NATIVE_TO_INT(event.Details.FromPlayerIndex))
		ENDIF
		#ENDIF
	ELSE
		SCRIPT_ASSERT("[RCC MISSION] PROCESS_START_STREAMING_END_MOCAP_EVENT - could not retrieve data")
	ENDIF
ENDPROC

PROC PROCESS_CUTSCENE_PLAYER_REQUEST(INT iEventID)
	
	EVENT_STRUCT_MOCAP_PLAYER_REQUEST Event
	INT iteam
	
	IF bIsLocalPlayerHost
		IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))
			
			// Check if we are in same vote
			PRINTLN("[RCC MISSION]  PROCESS_CUTSCENE_PLAYER_REQUEST - ")
			PRINTLN("[RCC MISSION] 			Player: ", NATIVE_TO_INT(Event.Details.FromPlayerIndex))
			PRINTLN("[RCC MISSION] 			mocap team: ", Event.iTeam)
			
			SWITCH Event.iTeam
				CASE 0
					SET_BIT(MC_ServerBD.iServerBitSet1, SBBOOL1_CUTSCENE_REQUESTED_FOR_TEAM_0)
				BREAK
				CASE 1
					SET_BIT(MC_ServerBD.iServerBitSet1, SBBOOL1_CUTSCENE_REQUESTED_FOR_TEAM_1)
				BREAK
				CASE 2
					SET_BIT(MC_ServerBD.iServerBitSet1, SBBOOL1_CUTSCENE_REQUESTED_FOR_TEAM_2)
				BREAK
				CASE 3
					SET_BIT(MC_ServerBD.iServerBitSet1, SBBOOL1_CUTSCENE_REQUESTED_FOR_TEAM_3)
				BREAK
			ENDSWITCH
			
			SWITCH Event.eCutType
				
				CASE FMMCCUT_ENDMOCAP
					
					FOR iteam = 0 TO (FMMC_MAX_TEAMS-1)
						IF DOES_TEAM_LIKE_TEAM(iteam,Event.iTeam)
							IF NOT HAS_NET_TIMER_STARTED(MC_serverBD.tdCutsceneStartTime[iteam])
								REINIT_NET_TIMER(MC_serverBD.tdCutsceneStartTime[iteam],FALSE,TRUE)
								PRINTLN("[RCC MISSION] - MC_serverBD.tdCutsceneStartTime set at time: ",NATIVE_TO_INT(GET_NETWORK_TIME_ACCURATE()) )
								PRINTLN("[RCC MISSION] -HOST cutscene should start at : ",(NATIVE_TO_INT(GET_NETWORK_TIME_ACCURATE())+1000))
							ENDIF	
							SWITCH iteam
								CASE 0
									SET_BIT(MC_ServerBD.iServerBitSet1, SBBOOL1_CUTSCENE_REQUESTED_FOR_TEAM_0)
								BREAK
								CASE 1
									SET_BIT(MC_ServerBD.iServerBitSet1, SBBOOL1_CUTSCENE_REQUESTED_FOR_TEAM_1)
								BREAK
								CASE 2
									SET_BIT(MC_ServerBD.iServerBitSet1, SBBOOL1_CUTSCENE_REQUESTED_FOR_TEAM_2)
								BREAK
								CASE 3
									SET_BIT(MC_ServerBD.iServerBitSet1, SBBOOL1_CUTSCENE_REQUESTED_FOR_TEAM_3)
								BREAK
							ENDSWITCH
						ENDIF
					ENDFOR
				
				BREAK
				
			ENDSWITCH
		ELSE
			SCRIPT_ASSERT("[RCC MISSION] PROCESS_MOCAP_PLAYER_ANIMATION - could not retrieve data")
		ENDIF
	ENDIF
	
ENDPROC

//caches the delivered vehicle index into cutscene_vehicle[] so it can still be reffereced in order to to set 
//the vehicle visible in the cutscene after CLEANUP_NET_ID has been called on the vehicle.
PROC PROCESS_FMMC_ADD_CUTSCENE_VEH(INT iCount)

	SCRIPT_EVENT_DATA_FMMC_CUTSCENE_VEH CutscenVehData
	//BOOL bTicker = TRUE
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, CutscenVehData, SIZE_OF(CutscenVehData))

		IF CutscenVehData.Details.Type = SCRIPT_EVENT_FMMC_DELIVER_CUTSCENE_VEH
			PRINTLN("[RCC MISSION] CutscenVehData.iVehNetID = ",CutscenVehData.iVehNetID)
			PRINTLN("[RCC MISSION] CutscenVehData.iCSNumVeh = ",CutscenVehData.iCSNumVeh)
			IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[CutscenVehData.iVehNetID])
				cutscene_vehicle[CutscenVehData.iCSNumVeh] = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[CutscenVehData.iVehNetID])
				
				IF bIsLocalPlayerHost
					PRINTLN("[RCC MISSION] MC_serverBD.cutscene_vehicle_count = ",MC_serverBD.cutscene_vehicle_count)
					MC_serverBD.cutscene_vehicle_count++
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Dialogue		 ---------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Processes for dialogue functionality  				------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_FMMC_DIALOGUE_LOOK(INT iCount)

	SCRIPT_EVENT_DATA_FMMC_DIALOGUE_LOOK LookData
	PED_INDEX tempPed
	//BOOL bTicker = TRUE
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, LookData, SIZE_OF(LookData))

		IF LookData.Details.Type = SCRIPT_EVENT_FMMC_DIALOGUE_LOOK
			PRINTLN("[RCC MISSION] LookData.Details.FromPlayerIndex = ",NATIVE_TO_INT(LookData.Details.FromPlayerIndex))
			PRINTLN("[RCC MISSION] LookData.iped = ",LookData.iped)
			tempPed = GET_PLAYER_PED(LookData.Details.FromPlayerIndex)
			IF NOT IS_PED_INJURED(tempPed)
				DialoguePedToLookAt[LookData.iped] = tempPed
				IF DialoguePedToLookAt[LookData.iped] = NULL
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF

ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Doors		 ---------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Processes for door functionality  				----------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
PROC PROCESS_SCRIPT_EVENT_FMMC_DIRECTIONAL_DOOR_EVENT(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_DIRECTIONAL_DOOR_EVENT EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_DIRECTIONAL_DOOR_EVENT
		
			PRINTLN("[DirDoorLock][Door ", EventData.iDoor, "] PROCESS_SCRIPT_EVENT_FMMC_DIRECTIONAL_DOOR_EVENT | Directional Door event received! EventData.iDoor: ", EventData.iDoor, " | EventData.bUnlock: ", EventData.bUnlock, " | EventData.bRelock: ", EventData.bRelock)
			
			IF EventData.bUnlock
				IF bIsLocalPlayerHost
					SET_BIT(MC_serverBD_4.sDoorServerData.iHostDirectionalDoorUnlockedBS, EventData.iDoor)
					PRINTLN("[DirDoorLock][Door ", EventData.iDoor, "] PROCESS_SCRIPT_EVENT_FMMC_DIRECTIONAL_DOOR_EVENT | UNLOCKING - Host setting iHostDirectionalDoorUnlockedBS ", EventData.iDoor)
				ENDIF
				
				PRINTLN("[DirDoorLock][Door ", EventData.iDoor, "] PROCESS_SCRIPT_EVENT_FMMC_DIRECTIONAL_DOOR_EVENT | UNLOCKING - Setting iDirectionalDoorLocallyUnlockedBS ", EventData.iDoor)
			ENDIF
			
			IF EventData.bRelock
				
				IF bIsLocalPlayerHost
					CLEAR_BIT(MC_serverBD_4.sDoorServerData.iHostDirectionalDoorUnlockedBS, EventData.iDoor)
					PRINTLN("[DirDoorLock][Door ", EventData.iDoor, "] PROCESS_SCRIPT_EVENT_FMMC_DIRECTIONAL_DOOR_EVENT | RE-LOCKING - Host clearing iHostDirectionalDoorUnlockedBS ", EventData.iDoor)
				ENDIF
				
				CLEAR_BIT(iDirectionalDoorLocallyUnlockedBS, EventData.iDoor) // Clear this on everyone so it can be re-set if needed
				PRINTLN("[DirDoorLock][Door ", EventData.iDoor, "] PROCESS_SCRIPT_EVENT_FMMC_DIRECTIONAL_DOOR_EVENT | RE-LOCKING - Clearing iDirectionalDoorLocallyUnlockedBS ", EventData.iDoor)
			ENDIF
			
			IF EventData.fAutomaticDistance != -1.0
				fDirectionalDoorAutoDistance[EventData.iDoor] = EventData.fAutomaticDistance
				PRINTLN("[DirDoorLock][Door ", EventData.iDoor, "] PROCESS_SCRIPT_EVENT_FMMC_DIRECTIONAL_DOOR_EVENT | Setting fDirectionalDoorAutoDistance[", EventData.iDoor, "] to ", fDirectionalDoorAutoDistance[EventData.iDoor])
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_UPDATE_DOOR_OFF_RULE_EVENT(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_UPDATE_DOOR_OFF_RULE_EVENT EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_UPDATE_DOOR_OFF_RULE_EVENT
			PRINTLN("[UpdateDoorOffRule] Update Door Off Rule Event Received! EventData.bSetUpdateDoorOffRule: ", EventData.bSetUpdateDoorOffRule, " EventData.iDoor: ", EventData.iDoor)
			
			IF bIsLocalPlayerHost
				IF NOT IS_BIT_SET(MC_serverBD_1.iDoorUpdateOffRuleBS, EventData.iDoor)
					IF EventData.bSetUpdateDoorOffRule
						SET_BIT(MC_serverBD_1.iDoorUpdateOffRuleBS, EventData.iDoor)
						PRINTLN("[UpdateDoorOffRule]- PROCESS_SCRIPT_EVENT_FMMC_UPDATE_DOOR_OFF_RULE_EVENT - Set iDoorUpdateOffRuleBS for door: ", EventData.iDoor)
					ENDIF
				ELIF NOT EventData.bSetUpdateDoorOffRule 
					CLEAR_BIT(MC_serverBD_1.iDoorUpdateOffRuleBS, EventData.iDoor)
					PRINTLN("[UpdateDoorOffRule]- PROCESS_SCRIPT_EVENT_FMMC_UPDATE_DOOR_OFF_RULE_EVENT - Cleared iDoorUpdateOffRuleBS for door: ", EventData.iDoor)
				ENDIF
			ENDIF
			
			IF EventData.iMethodUsed = ciUPDATE_DOOR_OFF_RULE_METHOD__KEYCARD
				SET_BIT(iLocalBoolCheck32, LBOOL32_UNLOCKED_DOOR_WITH_SWIPECARD)
				PRINTLN("[UpdateDoorOffRule][InteractWith][InteractWith_Dialogue] PROCESS_SCRIPT_EVENT_FMMC_UPDATE_DOOR_OFF_RULE_EVENT - Setting LBOOL32_UNLOCKED_DOOR_WITH_SWIPECARD")
			ENDIF
			
		ENDIF
	ENDIF
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Ending		 ---------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Processes for ending functionality  				----------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
PROC PROCESS_FMMC_TEAM_HAS_EARLY_CELEBRATION( INT iCount )
	
	SCRIPT_EVENT_DATA_FMMC_TEAM_HAS_EARLY_CELEBRATION EarlyCelebrationData
	
	IF GET_EVENT_DATA( SCRIPT_EVENT_QUEUE_NETWORK, iCount, EarlyCelebrationData, SIZE_OF( EarlyCelebratioNData ) ) 
		IF EarlyCelebrationData.Details.Type = SCRIPT_EVENT_FMMC_TEAM_HAS_EARLY_CELEBRATION
		
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] PROCESS_FMMC_TEAM_HAS_EARLY_CELEBRATION - EarlyCelebrationData.iTeam = ", EarlyCelebrationData.iTeam) 
			
			END_MISSION_TIMERS()
			iEarlyCelebrationTeamPoints[ EarlyCelebrationData.iTeam ] = EarlyCelebrationData.iWinningPoints
			
			IF HAS_TEAM_PASSED_MISSION( EarlyCelebrationData.iTeam )
				SET_BIT( iLocalBoolCheck11, LBOOL11_STOP_MISSION_FAILING_DUE_TO_EARLY_CELEBRATION )
				CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] PROCESS_FMMC_TEAM_HAS_EARLY_CELEBRATION - Team ", EarlyCelebrationData.iTeam, " has passed mission - no-one on this team can fail any longer!") 
			ELSE
				CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] PROCESS_FMMC_TEAM_HAS_EARLY_CELEBRATION - EarlyCelebrationData.iTeam = ", EarlyCelebrationData.iTeam, " hasn't passed the mission. We're gonna fail.") 
			ENDIF
			
		ENDIF	
	ENDIF
	
ENDPROC

PROC PROCESS_SCRIPT_EVENT_BLOCK_SAVE_EOM_CAR_BEEN_DELIVERED(INT iCount)

	SCRIPT_EVENT_DATA_FMMC_NETWORK_INDEX Event
	
	PRINTLN("[RCC MISSION] - PROCESS_SCRIPT_EVENT_BLOCK_SAVE_EOM_CAR_BEEN_DELIVERED - received event SCRIPT_EVENTBLOCK_SAVE_EOM_CAR_BEEN_DELIVERED.")
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, Event, SIZE_OF(Event))
		
		PRINTLN("[RCC MISSION] - PROCESS_SCRIPT_EVENT_BLOCK_SAVE_EOM_CAR_BEEN_DELIVERED - got event data.")
		
		IF DOES_ENTITY_EXIST(LocalPlayerPed)
			IF NOT IS_ENTITY_DEAD(LocalPlayerPed)
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(Event.vehNetID)
					IF IS_ENTITY_A_VEHICLE(NET_TO_ENT(Event.vehNetID))
						IF IS_VEHICLE_DRIVEABLE(NET_TO_VEH(Event.vehNetID))
							IF IS_PED_IN_VEHICLE(LocalPlayerPed, NET_TO_VEH(Event.vehNetID), TRUE)
								SET_BIT(iLocalBoolCheck6, LBOOL6_BLOCK_SAVE_EOM_CAR_WAS_DELIVERED)
								PRINTLN("[RCC MISSION] - PROCESS_SCRIPT_EVENT_BLOCK_SAVE_EOM_CAR_BEEN_DELIVERED - I am in delivered vehicle, setting bool LBOOL6_BLOCK_SAVE_EOM_CAR_WAS_DELIVERED.")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF

ENDPROC

PROC PROCESS_EVENT_SYNC_SPECTATOR_CELEBRATION_GLOBALS_1(INT iEventID)

	SCRIPT_EVENT_DATA_SYNC_SPECTATOR_CELEBRATION_GLOBALS_1 Event
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))
		
		IF bIsSCTV
		OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bJoinedMissionAsSpectator)///GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].bJoinedMissionAsSpectator
		
			#IF IS_DEBUG_BUILD
				NET_NL() NET_PRINT("PROCESS_EVENT_SYNC_SPECTATOR_CELEBRATION_GLOBALS_1_COMPLETE - called...") NET_NL()
			#ENDIF
			
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.iRpGained = Event.iRpGained
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.iRpStarted = Event.iRpStarted
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.iCurrentLevelStartPoints = Event.iCurrentLevelStartPoints
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.iCurrentLevelEndPoints = Event.iCurrentLevelEndPoints
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.iCurrentLevel = Event.iCurrentLevel
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.iNextLevel = Event.iNextLevel
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.iMaximumTake = Event.iMaximumTake
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.iActualTake = Event.iActualTake
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.iMyCutPercentage = Event.iMyCutPercentage
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.iMyTake = Event.iMyTake
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDifficultyHighEnoughForEliteChallenge = Event.bDifficultyHighEnoughForEliteChallenge
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.iNumEliteChallengeComponents = Event.iNumEliteChallengeComponents
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bEliteChallengeComplete = Event.bEliteChallengeComplete
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.iEliteChallengeBonusValue = Event.iEliteChallengeBonusValue
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bFirstTimeBonusShouldBeShown = Event.bFirstTimeBonusShouldBeShown
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.iFirstTimeBonusValue = Event.iFirstTimeBonusValue
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bOrderBonusShouldBeShown = Event.bOrderBonusShouldBeShown
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.iOrderBonusValue = Event.iOrderBonusValue
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bSameTeamBonusShouldBeShown = Event.bSameTeamBonusShouldBeShown
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.iSameTeamBonusValue = Event.iSameTeamBonusValue
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bUltimateBonusShouldBeShown = Event.bUltimateBonusShouldBeShown
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.iUltimateBonusValue = Event.iUltimateBonusValue
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bMemberBonusShouldBeShown = Event.bMemberBonusShouldBeShown
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.iMemberBonusValue = Event.iMemberBonusValue
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bIAmHeistLeader = Event.bIAmHeistLeader
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.iNumJobPoints = Event.iNumJobPoints
			
		ENDIF
		
	ELSE	
		SCRIPT_ASSERT("PROCESS_EVENT_SYNC_SPECTATOR_CELEBRATION_GLOBALS_1_COMPLETE - could not retrieve data.")
	ENDIF
	
ENDPROC

PROC PROCESS_EVENT_SYNC_SPECTATOR_CELEBRATION_GLOBALS_2(INT iEventID)
	
	SCRIPT_EVENT_DATA_SYNC_SPECTATOR_CELEBRATION_GLOBALS_2 Event
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))
		
		IF bIsSCTV
		OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bJoinedMissionAsSpectator)//GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].bJoinedMissionAsSpectator
		
			#IF IS_DEBUG_BUILD
				NET_NL() NET_PRINT("PROCESS_EVENT_SYNC_SPECTATOR_CELEBRATION_GLOBALS_2_COMPLETE - called...") NET_NL()
			#ENDIF
			
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentMissionTime = Event.bShowEliteComponentMissionTime
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.iTargetMissionTime = Event.iTargetMissionTime
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.iMissionTime = Event.iMissionTime
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bMissionTimeChallengeComplete = Event.bMissionTimeChallengeComplete
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentVehicleDamage = Event.bShowEliteComponentVehicleDamage
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.iVehicleDamagePercentageTarget = Event.iVehicleDamagePercentageTarget
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.iVehicleDamagePercentageActual = Event.iVehicleDamagePercentageActual
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bVehicleDamagePercentagePassed = Event.bVehicleDamagePercentagePassed
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentRachDamage = Event.bShowEliteComponentRachDamage
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.iRachDamagePercentageTarget = Event.iRachDamagePercentageTarget
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.iRachDamagePercentageActual = Event.iRachDamagePercentageActual
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bRachDamagePercentagePassed = Event.bRachDamagePercentagePassed
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentExtractionTime = Event.bShowEliteComponentExtractionTime
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.iTargetExtractionTime = Event.iTargetExtractionTime
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.iExtractionTime = Event.iExtractionTime
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bExtractionTimeChallengeComplete = Event.bExtractionTimeChallengeComplete
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentNumPedKills = Event.bShowEliteComponentNumPedKills
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.iNumPedKillsTarget = Event.iNumPedKillsTarget
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.iNumPedKills = Event.iNumPedKills
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bNumPedKillsChallengeComplete = Event.bNumPedKillsChallengeComplete
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentNooseCalled  = Event.bShowEliteComponentNooseCalled
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bNooseCalled = Event.bNooseCalled
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentHealthDamage = Event.bShowEliteComponentHealthDamage
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.iHealthDamagePercentageTarget = Event.iHealthDamagePercentageTarget
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.iHealthDamagePercentageActual = Event.iHealthDamagePercentageActual
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bHealthDamagePercentagePassed = Event.bHealthDamagePercentagePassed
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentLivesLost = Event.bShowEliteComponentLivesLost
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bNumLivesLostChallengeComplete = Event.bNumLivesLostChallengeComplete
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentTripSkip = Event.bShowEliteComponentTripSkip
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentDroppedToDirect = Event.bShowEliteComponentDroppedToDirect
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentDidQuickRestart = Event.bShowEliteComponentDidQuickRestart
			
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentNumPedHeadshots = Event.bShowEliteComponentNumPedHeadshots
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.iNumPedHeadshotsTarget = Event.iNumPedHeadshotsTarget
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.iNumPedHeadshots = Event.iNumPedHeadshots
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bNumPedHeadshotsChallengeComplete = Event.bNumPedHeadshotsChallengeComplete

			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bShowEliteComponentHackFailed = Event.bShowEliteComponentHackFailed
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.iNumHacksFailedTarget = Event.iNumHacksFailedTarget
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.iNumHacksFailed = Event.iNumHacksFailed
			g_TransitionSessionNonResetVars.sGlobalCelebrationData.bNumHackFailedChallengeComplete = Event.bNumHackFailedChallengeComplete
			
		ENDIF	
		
	ELSE	
		SCRIPT_ASSERT("PROCESS_EVENT_SYNC_SPECTATOR_CELEBRATION_GLOBALS_2_COMPLETE - could not retrieve data.")
	ENDIF
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: HUD		 -------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Processes for HUD functionality  				--------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_ARROW_EVENT(INT iEventID)
	SCRIPT_EVENT_DATA_RALLY_ARROWS Event
	INT iSenderTeam, iLocalTeam
	IF IS_NET_PLAYER_OK(LocalPlayer, TRUE)
		IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))
			iSenderTeam = GET_PLAYER_TEAM(Event.Details.FromPlayerIndex)
			iLocalTeam = GET_PLAYER_TEAM(LocalPlayer)
			IF iSenderTeam != iLocalTeam
			AND AM_I_DRIVING_THE_NAVIGATOR(Event.Details.FromPlayerIndex)
		
				#IF IS_DEBUG_BUILD
				STRING sSender = GET_PLAYER_NAME(Event.Details.FromPlayerIndex)
				PRINTLN("[CS_ARR] PROCESS_RALLY_ARROW_EVENT, sSender = ", sSender, " direction  = ", GET_ARROW_DIRECTION_NAME(event.iArrowDirection))
				#ENDIF
				
				SWITCH event.iArrowDirection
					CASE ciRALLY_ARROW_LEFT 							
						SET_ARROW_BITS(ciRALLY_ARROW_LEFT, FALSE)
						DO_ARROW_SOUNDS(ciRALLY_ARROW_LEFT)
						PRINTLN("[CS_ARR] PROCESS_RALLY_ARROW_EVENT, ciRALLY_ARROW_LEFT ")
					BREAK
					
					CASE ciRALLY_ARROW_RIGHT
						SET_ARROW_BITS(ciRALLY_ARROW_RIGHT, FALSE)
						DO_ARROW_SOUNDS(ciRALLY_ARROW_RIGHT)
						PRINTLN("[CS_ARR] PROCESS_RALLY_ARROW_EVENT,  ciRALLY_ARROW_RIGHT ")
					BREAK
					
					CASE ciRALLY_ARROW_UP
						SET_ARROW_BITS(ciRALLY_ARROW_UP, FALSE)
						DO_ARROW_SOUNDS(ciRALLY_ARROW_UP)
						PRINTLN("[CS_ARR] PROCESS_RALLY_ARROW_EVENT,  ciRALLY_ARROW_UP ")
					BREAK
					
					CASE ciRALLY_ARROW_DOWN
						SET_ARROW_BITS(ciRALLY_ARROW_DOWN, FALSE)
						DO_ARROW_SOUNDS(ciRALLY_ARROW_DOWN)
						PRINTLN("[CS_ARR] PROCESS_RALLY_ARROW_EVENT,  ciRALLY_ARROW_DOWN ")
					BREAK
				ENDSWITCH
			ELSE
				PRINTLN("[CS_ARR] ERROR, PROCESS_RALLY_ARROW_EVENT, TEAMS iSenderTeam = ", iSenderTeam, " iLocalTeam = ", iLocalTeam)
			ENDIF
		ELSE
			PRINTLN("[CS_ARR] ERROR, PROCESS_RALLY_ARROW_EVENT,  EVENT DATA ")
		ENDIF
	ELSE
		PRINTLN("[CS_ARR] ERROR, PROCESS_RALLY_ARROW_EVENT,  OK CHECK ")
	ENDIF
ENDPROC

PROC PROCESS_FMMC_STOLEN_FLAG_SHARD(INT iCount)
	SCRIPT_EVENT_DATA_FMMC_STOLEN_FLAG_SHARD Event
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, Event, SIZE_OF(Event))
		IF Event.Details.Type = SCRIPT_EVENT_FMMC_STOLEN_FLAG_SHARD
			PRINTLN("[RCC MISSION][PROCESS_FMMC_STOLEN_FLAG_SHARD] Calling PROCESS_FMMC_STOLEN_FLAG_SHARD")
			PRINTLN("[RCC MISSION][PROCESS_FMMC_STOLEN_FLAG_SHARD] Team Stolen from: ", Event.iTeamStolenFrom," Player Stolen from: ",Event.iPreviousOwner, " Player Stealing: ", NATIVE_TO_INT(Event.PlayerThatStole), " Team Stealing: ", Event.iTeamThatStole)
			IF Event.iTeamStolenFrom = MC_PlayerBD[iPartToUse].iteam
				IF NETWORK_IS_PARTICIPANT_ACTIVE(NETWORK_GET_PARTICIPANT_INDEX(Event.PlayerThatStole))
					INT iStealPart
					iStealPart = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(Event.PlayerThatStole)) 
					IF iStealPart != -1
					AND MC_PlayerBD[iStealPart].iteam != MC_PlayerBD[iPartToUse].iteam
						PRINTLN("[RCC MISSION][PROCESS_FMMC_STOLEN_FLAG_SHARD] Flag stolen from team: ", Event.iTeamStolenFrom, " Stolen by player: ", NATIVE_TO_INT(Event.PlayerThatStole))
						SETUP_NEW_BIG_MESSAGE_WITH_PLAYER(BIG_MESSAGE_GENERIC_TEXT,Event.PlayerThatStole, DEFAULT,"S_SF_PN", "S_STOLEN",GET_HUD_COLOUR_FOR_FMMC_TEAM(Event.iTeamThatStole, Event.PlayerThatStole))
						IF Event.iPreviousOwner = iLocalPart
							IF MC_PlayerBD[iLocalPart].iPackagesAtHolding > 0
								MC_PlayerBD[iLocalPart].iPackagesAtHolding--
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_FMMC_CAPTURED_FLAG_SHARD(INT iCount)
	SCRIPT_EVENT_DATA_FMMC_CAPTURED_FLAG_SHARD Event
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, Event, SIZE_OF(Event))
		IF Event.Details.Type = SCRIPT_EVENT_FMMC_CAPTURED_FLAG_SHARD
		AND Event.Details.FromPlayerIndex != LocalPlayer
			TEXT_LABEL_15 sTagLine
			IF bIsAnySpectator
				sTagLine = "ACTF_STC" //A flag
			ELIF Event.iTeamThatCaptured != MC_PlayerBD[iPartToUse].iteam
				sTagLine = "ACTF_YTC" //Your flag
			ELSE
				sTagLine = "ACTF_ETC" //Enemy flag
			ENDIF
			sTagLine += Event.iTeamThatCaptured
			SETUP_NEW_BIG_MESSAGE_WITH_PLAYER(BIG_MESSAGE_GENERIC_TEXT, Event.Details.FromPlayerIndex, DEFAULT, sTagLine, "ACTF_CAP",GET_HUD_COLOUR_FOR_FMMC_TEAM(Event.iTeamThatCaptured, Event.Details.FromPlayerIndex))
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_FMMC_RETURNED_FLAG_SHARD(INT iCount)
	SCRIPT_EVENT_DATA_FMMC_RETURNED_FLAG_SHARD Event
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, Event, SIZE_OF(Event))
		IF Event.Details.Type = SCRIPT_EVENT_FMMC_RETURNED_FLAG_SHARD
			TEXT_LABEL_15 tlTitle = "ACTF_RTRN"
			IF Event.bUseShuntTitle
				tlTitle = "ACTF_SHNT"
			ENDIF
			TEXT_LABEL_15 tlTag
			IF Event.piPlayerThatReturned = LocalPlayer
				tlTag = "ACTF_SF_YS"
				tlTag += Event.iTeamThatReturned
				SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GENERIC_TEXT, tlTitle, tlTag)
			ELIF Event.iTeamThatReturned = MC_playerBD[iPartToUse].iteam
			AND NOT bIsAnySpectator
				tlTag = "ACTF_SF_SFF"
				tlTag += Event.iTeamThatReturned
				SETUP_NEW_BIG_MESSAGE_WITH_PLAYER(BIG_MESSAGE_GENERIC_TEXT, Event.piPlayerThatReturned, DEFAULT, tlTag, tlTitle, GET_HUD_COLOUR_FOR_FMMC_TEAM(Event.iTeamThatReturned, Event.piPlayerThatReturned)) //Your flag
			ELSE
				tlTag = "ACTF_SF_SFY"
				tlTag += Event.iTeamThatReturned
				SETUP_NEW_BIG_MESSAGE_WITH_PLAYER(BIG_MESSAGE_GENERIC_TEXT, Event.piPlayerThatReturned, DEFAULT, tlTag, tlTitle, GET_HUD_COLOUR_FOR_FMMC_TEAM(Event.iTeamThatReturned, Event.piPlayerThatReturned)) //Their flag
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_FMMC_TAGGED_ENTITY(INT iCount)
	SCRIPT_EVENT_DATA_FMMC_TAGGED_ENTITY Event
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, Event, SIZE_OF(Event))
		IF Event.Details.Type = SCRIPT_EVENT_FMMC_TAGGED_ENTITY
			IF bIsLocalPlayerHost
				PRINTLN("[RCC MISSION][PROCESS_FMMC_STOLEN_FLAG_SHARD] Calling PROCESS_FMMC_TAGGED_ENTITY")
				SET_BIT(MC_serverBD_3.iTaggedEntityBitset, Event.iTaggedIndex)
				MC_serverBD_3.iTaggedEntityType[Event.iTaggedIndex] = Event.iTaggedEntityType
				MC_serverBD_3.iTaggedEntityIndex[Event.iTaggedIndex] = Event.iTaggedEntityIndex
				MC_serverBD_3.iTaggedEntityGangChaseUnit[Event.iTaggedIndex] = Event.iTaggedGangChaseUnit
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_DISTANCE_GAINED_TICKER(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_DISTANCE_GAINED EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_DISTANCE_GAINED
			IF EventData.Details.FromPlayerIndex != LocalPlayer
				HUD_COLOURS hcPlayerTeamColour = GET_HUD_COLOUR_FOR_FMMC_TEAM(EventData.iTeam, EventData.Details.FromPlayerIndex)
				IF SHOULD_USE_METRIC_MEASUREMENTS()
					IF EventData.bGainedDistance
						PRINT_TICKER_WITH_PLAYER_NAME_WITH_HUD_COLOUR_AND_INT("IBI_TICK1", EventData.Details.FromPlayerIndex, hcPlayerTeamColour, EventData.iDistanceMeters)
					ELSE
						PRINT_TICKER_WITH_PLAYER_NAME_WITH_HUD_COLOUR_AND_INT("IBI_TICK2", EventData.Details.FromPlayerIndex, hcPlayerTeamColour, EventData.iDistanceMeters)
					ENDIF
				ELSE
					IF EventData.bGainedDistance
						PRINT_TICKER_WITH_PLAYER_NAME_WITH_HUD_COLOUR_AND_INT("IBI_TICK5", EventData.Details.FromPlayerIndex, hcPlayerTeamColour, EventData.iDistanceFeet)
					ELSE
						PRINT_TICKER_WITH_PLAYER_NAME_WITH_HUD_COLOUR_AND_INT("IBI_TICK6", EventData.Details.FromPlayerIndex, hcPlayerTeamColour, EventData.iDistanceFeet)
					ENDIF
				ENDIF
			ELSE
				IF SHOULD_USE_METRIC_MEASUREMENTS()
					IF EventData.bGainedDistance
						PRINT_TICKER_WITH_INT("IBI_TICK3", EventData.iDistanceMeters)
					ELSE
						PRINT_TICKER_WITH_INT("IBI_TICK4", EventData.iDistanceMeters)
					ENDIF
				ELSE
					IF EventData.bGainedDistance
						PRINT_TICKER_WITH_INT("IBI_TICK7", EventData.iDistanceFeet)
					ELSE
						PRINT_TICKER_WITH_INT("IBI_TICK8", EventData.iDistanceFeet)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_FMMC_VIP_MARKER_POSITION(INT iCount)

	SCRIPT_EVENT_DATA_VIP_MARKER  EventData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_VIP_MARKER_POSITION
			IF bIsLocalPlayerHost
				IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, EventData, SIZE_OF(EventData))					
					IF NOT IS_VECTOR_ZERO(EventData.vVIPMarkerPosition)
						MC_serverBD_3.vVIPMarker = EventData.vVIPMarkerPosition
						PRINTLN("[RCC MISSION] PROCESS_FMMC_VIP_MARKER_POSITION - MC_serverBD.vVIPMarker" , EventData.vVIPMarkerPosition )
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_FMMC_DISPLAY_CUSTOM_OBJECT_PICKUP_SHARD(INT iCount)
	
	SCRIPT_EVENT_DATA_FMMC_CUSTOM_OBJECT_PICKUP_SHARD EventData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_DISPLAY_CUSTOM_OBJECT_PICKUP_SHARD
			TEXT_LABEL_15 tlShardTitle, tlShardStrapline
			SWITCH EventData.iShardIndex
				CASE 1
					tlShardTitle = g_FMMC_STRUCT.tlCustomShardTitle
					tlShardStrapline = g_FMMC_STRUCT.tlCustomShardStrapline
				BREAK
				CASE 2
					tlShardTitle = g_FMMC_STRUCT.tlCustomShardTitle2
					tlShardStrapline = g_FMMC_STRUCT.tlCustomShardStrapline2
				BREAK
				CASE 3
					tlShardTitle = g_FMMC_STRUCT.tlCustomShardTitle3
					tlShardStrapline = g_FMMC_STRUCT.tlCustomShardStrapline3
				BREAK
			ENDSWITCH
			IF bPlayerToUseOK
				SETUP_NEW_BIG_MESSAGE_WITH_PLAYER(BIG_MESSAGE_GENERIC_TEXT, EventData.ObjPlayer, DEFAULT, tlShardStrapline, tlShardTitle, GET_HUD_COLOUR_FOR_FMMC_TEAM(EventData.iTeam, EventData.ObjPlayer))
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_SCRIPT_EVENT_SUDDEN_DEATH_REASON(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_SUDDEN_DEATH_REASON EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_SUDDEN_DEATH_REASON
		
			STRING sMsgTitle
			STRING tlTeamName = TEXT_LABEL_TO_STRING(g_sMission_TeamName[ EventData.iTeam ])
			HUD_COLOURS hudColour = GET_HUD_COLOUR_FOR_FMMC_TEAM(EventData.iTeam, PlayerToUse)

			IF EventData.iTeam = MC_playerBD[iPartToUse].iteam
				sMsgTitle = "IBI_ST_WIN"
			ELSE
				sMsgTitle = "IBI_ST_LOSE"
			ENDIF
			
			IF IS_ANY_BIG_MESSAGE_BEING_DISPLAYED()
			AND NOT IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_WASTED)
				PRINTLN("[KH] PROCESS_SCRIPT_EVENT_SUDDEN_DEATH_REASON - big message is already being displayed, clearing before displaying end reason shard")
				CLEAR_ALL_BIG_MESSAGES()
			ENDIF

			SWITCH(EventData.iReason)
				CASE ciSUDDEN_DEATH_REASON_KILLS		
					SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GENERIC_TEXT, sMsgTitle, "IBI_S_KILLS", tlTeamName, hudColour)
				BREAK
				
				CASE ciSUDDEN_DEATH_REASON_POINTS
					SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GENERIC_TEXT, sMsgTitle, "IBI_S_POINTS", tlTeamName, hudColour)
				BREAK
				
				CASE ciSUDDEN_DEATH_REASON_DEATHS
					SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GENERIC_TEXT, sMsgTitle, "IBI_S_DEATHS", tlTeamName, hudColour)
				BREAK
				
				CASE ciSUDDEN_DEATH_REASON_HSHOTS
					SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GENERIC_TEXT, sMsgTitle, "IBI_S_HSHOTS", tlTeamName, hudColour)
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_REMOVE_DUMMY_BLIP(INT iEventID)

	SCRIPT_EVENT_DATA_FMMC_REMOVE_DUMMY_BLIP EventData
	IF NOT GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		EXIT
	ENDIF
	
	IF EventData.Details.Type != SCRIPT_EVENT_FMMC_REMOVE_DUMMY_BLIP
		EXIT
	ENDIF
	
	IF DOES_BLIP_EXIST(DummyBlip[EventData.iDummyBlipIndex])
		PRINTLN("[RCC MISSION][Dummy Blip] PROCESS_SCRIPT_EVENT_REMOVE_DUMMY_BLIP - Removing dummy blip at index ", EventData.iDummyBlipIndex, ".")
		SET_BIT(iDummyBlipBitset[EventData.iDummyBlipIndex], ciDBBS_Remove_On_Hide)
		REMOVE_BLIP(DummyBlip[EventData.iDummyBlipIndex])
	ENDIF
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Locations
// ##### Description: Processes for Locations functionality
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
PROC PROCESS_FMMC_LOCATION_INPUT_TRIGGERED(INT iCount)
	
	IF NOT bIsLocalPlayerHost
		EXIT
	ENDIF
	
	SCRIPT_EVENT_DATA_FMMC_LOCATION_INPUT_TRIGGERED Event
	
	IF NOT GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, Event, SIZE_OF(Event))
	OR Event.Details.Type != SCRIPT_EVENT_FMMC_LOCATION_INPUT_TRIGGERED
		EXIT
	ENDIF
	
	IF IS_NET_PLAYER_OK(Event.Details.FromPlayerIndex)
		PARTICIPANT_INDEX piPart = NETWORK_GET_PARTICIPANT_INDEX(Event.Details.FromPlayerIndex)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(piPart)
			iPartTriggeredLocateLast = NATIVE_TO_INT(piPart)
		ENDIF
	ELSE
		iPartTriggeredLocateLast = -1
	ENDIF
	
	PRINTLN("PROCESS_FMMC_LOCATION_INPUT_TRIGGERED - iPartTriggeredLocateLast: ", iPartTriggeredLocateLast)
	
	SET_BIT(MC_serverBD_4.iLocationInputTriggeredBitset, Event.iLoc)
	PRINTLN("PROCESS_FMMC_LOCATION_INPUT_TRIGGERED - Set bit ", Event.iLoc, " in MC_serverBD_4.iLocationInputTriggeredBitset")
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Minigames	 ---------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Processes for minigame functionality  			----------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_HEIST_BEGIN_HACK_MINIGAME(INT iCount)

	IF bIsLocalPlayerHost
	
		SCRIPT_EVENT_DATA_HEIST_BEGIN_HACK_MINIGAME EventData
		
		IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, EventData, SIZE_OF(EventData))
			IF EventData.Details.Type = SCRIPT_EVENT_HEIST_BEGIN_HACK_MINIGAME
				IF MC_serverBD_1.iPlayerForHackingMG = -1
				AND NOT IS_BIT_SET(MC_serverBD_1.iServerHackMGBitset, SBBOOL_HACK_FIND_PLAYER)
					PROCESS_SERVER_CIRCUIT_HACK_MG_INIT(EventData.iDialogueCausingHack)		
				ENDIF
				PRINTLN("    ----->    PROCESS_HEIST_BEGIN_HACK_MINIGAME - SENT FROM ", GET_PLAYER_NAME(EventData.Details.FromPlayerIndex))
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_HEIST_END_HACK_MINIGAME(INT iCount)

	IF bIsLocalPlayerHost
	
		SCRIPT_EVENT_DATA_HEIST_END_HACK_MINIGAME EventData
		
		IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, EventData, SIZE_OF(EventData))
			IF EventData.Details.Type = SCRIPT_EVENT_HEIST_END_HACK_MINIGAME
				PROCESS_SERVER_CIRCUIT_HACK_MG_END(EventData.iTeam, EventData.bPass)
				PRINTLN("    ----->    PROCESS_HEIST_END_HACK_MINIGAME - SENT FROM ", GET_PLAYER_NAME(EventData.Details.FromPlayerIndex))
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_DRILL_ASSET(INT iEventID)
	IF bIsLocalPlayerHost
		SCRIPT_EVENT_DATA_FMMC_DRILL_ASSET Event
		
		PRINTLN("[RCC MISSION] - PROCESS_SCRIPT_EVENT_FMMC_DRILL_ASSET - has been received.")

		IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))
			IF Event.iEventType = ciEVENT_DRILL_ASSET_REQUEST
				SET_BIT(MC_serverBD.iServerBitSet2, SBBOOL2_HEIST_DRILL_ASSET_REQUEST)
			ELSE
				CLEAR_BIT(MC_serverBD.iServerBitSet2, SBBOOL2_HEIST_DRILL_ASSET_REQUEST)
			ENDIF

			PRINTLN("[RCC MISSION] - PROCESS_SCRIPT_EVENT_FMMC_DRILL_ASSET - iEventType =  ", Event.iEventType)
		ENDIF
	ENDIF
ENDPROC


PROC PROCESS_SCRIPT_EVENT_VAULT_DRILL_ASSET(INT iEventID)
	IF bIsLocalPlayerHost
		SCRIPT_EVENT_DATA_VAULT_DRILL_ASSET Event
		
		PRINTLN("[RCC MISSION] - PROCESS_SCRIPT_EVENT_VAULT_DRILL_ASSET - has been received.")

		IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))
			IF Event.iEventType = ciEVENT_VAULT_DRILL_ASSET_REQUEST
			OR Event.iEventType = ciEVENT_VAULT_DRILL_ASSET_REQUEST_LASER
				SET_BIT(MC_serverBD.iServerBitSet8, SBBOOL8_VAULT_DRILL_ASSET_REQUEST)
				
				IF Event.iEventType = ciEVENT_VAULT_DRILL_ASSET_REQUEST_LASER
					SET_BIT(MC_serverBD.iServerBitSet8, SBBOOL8_VAULT_DRILL_ASSET_REQUEST_LASER)
				ENDIF
			ELSE
				CLEAR_BIT(MC_serverBD.iServerBitSet8, SBBOOL8_VAULT_DRILL_ASSET_REQUEST)
				CLEAR_BIT(MC_serverBD.iServerBitSet8, SBBOOL8_VAULT_DRILL_ASSET_REQUEST_LASER)
			ENDIF

			PRINTLN("[RCC MISSION] - PROCESS_SCRIPT_EVENT_VAULT_DRILL_ASSET - iEventType =  ", Event.iEventType)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_INTERACT_WITH_EVENT(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_INTERACT_WITH_EVENT EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_INTERACT_WITH_EVENT
		
			// Exploding/clearing up all persistent props
			IF EventData.bDeletePersistentProps
				INTERACT_WITH_DESTROY_ALL_PERSISTENT_PROPS(FALSE)
			ENDIF
			
			IF EventData.bExplodePersistentProps
				INTERACT_WITH_DESTROY_ALL_PERSISTENT_PROPS(TRUE)
			ENDIF
			
			// Planting explosives
			IF EventData.bPlantedLeftSideBomb
				IF bIsLocalPlayerHost
					MC_serverBD_1.iCurrentVaultDoorPlantedExplosives_LeftSide++
					PRINTLN("[InteractWith][PlantExplosives] PROCESS_SCRIPT_EVENT_FMMC_INTERACT_WITH_EVENT | iCurrentVaultDoorPlantedExplosives_LeftSide is now ", MC_serverBD_1.iCurrentVaultDoorPlantedExplosives_LeftSide)
				ENDIF
				
			ELIF EventData.bPlantedRightSideBomb
				IF bIsLocalPlayerHost
					MC_serverBD_1.iCurrentVaultDoorPlantedExplosives_RightSide++
					PRINTLN("[InteractWith][PlantExplosives] PROCESS_SCRIPT_EVENT_FMMC_INTERACT_WITH_EVENT | iCurrentVaultDoorPlantedExplosives_RightSide is now ", MC_serverBD_1.iCurrentVaultDoorPlantedExplosives_RightSide)
				ENDIF
				
			ENDIF
			
			IF EventData.iEndObjectiveASAP_Team > -1
			AND EventData.iEndObjectiveASAP_Rule > -1
				IF bIsLocalPlayerHost
					PRINTLN("[InteractWith] PROCESS_SCRIPT_EVENT_FMMC_INTERACT_WITH_EVENT | Passing rule ", EventData.iEndObjectiveASAP_Rule, ", for team ", EventData.iEndObjectiveASAP_Team, " ASAP!")
					
					IF EventData.iInteractWithParticipant > -1
						SET_CUTSCENE_PRIMARY_PLAYER(EventData.iInteractWithParticipant, EventData.iEndObjectiveASAP_Team)
					ENDIF
					
					INVALIDATE_SINGLE_OBJECTIVE(EventData.iEndObjectiveASAP_Team, EventData.iEndObjectiveASAP_Rule, DEFAULT, TRUE)
					RECALCULATE_OBJECTIVE_LOGIC(EventData.iEndObjectiveASAP_Team)
				ELSE
					PRINTLN("[InteractWith] PROCESS_SCRIPT_EVENT_FMMC_INTERACT_WITH_EVENT | Host is passing rule ASAP!")
				ENDIF
			ENDIF
			
			IF EventData.iBombPlanterIndex > -1
				IF bIsLocalPlayerHost
					MC_serverBD_4.iInteractWith_BombPlantParticipant = EventData.iBombPlanterIndex
					PRINTLN("[InteractWith][CasinoTunnelExplosion] PROCESS_SCRIPT_EVENT_FMMC_INTERACT_WITH_EVENT | iInteractWith_BombPlantParticipant is now ", MC_serverBD_4.iInteractWith_BombPlantParticipant)
				ENDIF
			ENDIF
			
			IF EventData.Details.FromPlayerIndex != INVALID_PLAYER_INDEX()
				PED_INDEX piPed = GET_PLAYER_PED(EventData.Details.FromPlayerIndex)
				PARTICIPANT_INDEX piPart
				
				IF NETWORK_IS_PLAYER_A_PARTICIPANT(EventData.Details.FromPlayerIndex)
					piPart = NETWORK_GET_PARTICIPANT_INDEX(EventData.Details.FromPlayerIndex)
				ENDIF
				
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(EventData.niPropBag)
				AND EventData.Details.FromPlayerIndex != LocalPlayer
					OBJECT_INDEX oiBag = NET_TO_OBJ(EventData.niPropBag)
					
					IF EventData.bSwapInPropBagLocally
						PRINTLN("[InteractWith_Bags] PROCESS_SCRIPT_EVENT_FMMC_INTERACT_WITH_EVENT | Swapping in remote player's bag prop")
						SET_ENTITY_ALPHA(oiBag, 255, FALSE)
						SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_HAND, 0, 0)
						CLEAR_BIT(iInteractWith_SwapOutParticipantPropBagBS, NATIVE_TO_INT(piPart))
					ENDIF
					
					IF EventData.bSwapOutPropBagLocally
						PRINTLN("[InteractWith_Bags] PROCESS_SCRIPT_EVENT_FMMC_INTERACT_WITH_EVENT | Swapping out remote player's bag prop")
						SET_BIT(iInteractWith_SwapOutParticipantPropBagBS, NATIVE_TO_INT(piPart))
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_VAULT_DRILL_PROGRESS(INT iEventID)
	IF bIsLocalPlayerHost
		SCRIPT_EVENT_DATA_VAULT_DRILL_PROGRESS Event
		
		IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))
			INT i
			
			FOR i = 0 TO FMMC_MAX_VAULT_DRILL_GAMES -1
				IF Event.iObj = MC_serverBD.iObjVaultDrillIDs[i]
					MC_serverBD.iObjVaultDrillDiscs[i] 		= Event.iDiscs
					MC_serverBD.iObjVaultDrillProgress[i]	= Event.iProgress
					MC_serverBD.iObjHackPart[Event.iObj] 			= -1
					BREAKLOOP
				ENDIF			
			ENDFOR
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_THERMITE_EFFECT_EVENT(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_THERMITE_EFFECT_EVENT EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_THERMITE_EFFECT_EVENT
			PRINTLN("[THERMITE] Thermite Effect Event Received! EventData.iObj: ", EventData.iObj, " EventData.bStartThermite: ", EventData.bStartThermite, " EventData.bStartSparkDieOff: ", EventData.bStartSparkDieOff, " EventData.bStartProcessing: ", EventData.bStartProcessing, 
				" EventData.bBreakKeypadObj: ", EventData.bBreakKeypadObj, " EventData.bClearProcessing: ", EventData.bClearProcessing, " EventData.bReduceChargeAmount: ", EventData.bReduceChargeAmount)
			
			INT iOrderedPart = EventData.iOrderedParticipant 
			
			IF EventData.bStartThermite
				oiLocalThermiteTarget[iOrderedPart] = NET_TO_OBJ(EventData.niObj)
				vThermiteLocalDripOffset[iOrderedPart] = EventData.vDripOffset
				vThermiteLocalSparkOffset[iOrderedPart] = EventData.vSparkOffset
				iThermiteLocalSparkTimer[iOrderedPart] = GET_GAME_TIMER()
				
				SET_BIT(iThermiteLocalBitset[iOrderedPart], THERMITE_LOCAL_BITSET_CREATE_LOCAL_THERMITE_PTFX)
				CDEBUG1LN(DEBUG_MISSION, "[THERMITE] PROCESS_SCRIPT_EVENT_FMMC_THERMITE_EFFECT_EVENT Setting THERMITE_LOCAL_BITSET_CREATE_LOCAL_THERMITE_PTFX for Ordered Participant: ", iOrderedPart)
			ENDIF
			
			IF EventData.bReduceChargeAmount
				
				INT iTeam = MC_playerBD[iPartToUse].iteam
				IF MC_serverBD_1.iTeamThermalCharges[iTeam] = 1
					CDEBUG1LN(DEBUG_MISSION, "[THERMITE] PROCESS_SCRIPT_EVENT_FMMC_THERMITE_EFFECT_EVENT Team ", iTeam, " is using their final remaining charge. Setting iThermiteKeypadWasLastTeamCharge for ", EventData.iObj)
					SET_BIT(iThermiteKeypadWasLastTeamCharge[iTeam], EventData.iObj)			
				ENDIF
			
				//If using limited team thermal charges then reduce the total amount here.
				IF bIsLocalPlayerHost
					IF MC_serverBD_1.iTeamThermalCharges[iTeam] > 0
						MC_serverBD_1.iTeamThermalCharges[iTeam] = MC_serverBD_1.iTeamThermalCharges[iTeam] - 1
						CDEBUG1LN(DEBUG_MISSION, "[THERMITE] PROCESS_SCRIPT_EVENT_FMMC_THERMITE_EFFECT_EVENT Team ", iTeam, " is using limited thermal charges and has just used one. ", MC_serverBD_1.iTeamThermalCharges[iTeam], " charges remain.")
					ENDIF
				ENDIF
			
			ENDIF
			
			IF EventData.bStartSparkDieOff
				iThermiteLocalSparkTimer[iOrderedPart] = GET_GAME_TIMER()
				SET_BIT(iThermiteLocalBitset[iOrderedPart], THERMITE_LOCAL_BITSET_START_LOCAL_SPARK_DIE_OFF_EVO)
				CDEBUG1LN(DEBUG_MISSION, "[THERMITE] PROCESS_SCRIPT_EVENT_FMMC_THERMITE_EFFECT_EVENT Setting THERMITE_LOCAL_BITSET_START_LOCAL_SPARK_DIE_OFF_EVO for Ordered Participant: ", iOrderedPart)
			ENDIF
			
			IF EventData.bStartProcessing
				OBJECT_INDEX tempObj = NET_TO_OBJ(EventData.niObj)
				
				//Stop the keypad from being able to be damaged externally once the thermite minigame starts.
				IF NETWORK_HAS_CONTROL_OF_ENTITY(tempObj)
					IF GET_ENTITY_CAN_BE_DAMAGED(tempObj)
						SET_ENTITY_CAN_BE_DAMAGED(tempObj, FALSE)
					ENDIF
				ENDIF
				
				//Block other players from processing any other thermite whilst another is ongoing.
				IF EventData.Details.FromPlayerIndex != LocalPlayer
					SET_BIT(iThermiteLocalBitset[iOrderedPart], THERMITE_LOCAL_BITSET_IS_ANY_THERMITE_PROCESSING)
					CDEBUG1LN(DEBUG_MISSION, "[THERMITE] PROCESS_SCRIPT_EVENT_FMMC_THERMITE_EFFECT_EVENT Setting THERMITE_LOCAL_BITSET_IS_ANY_THERMITE_PROCESSING for Ordered Participant: ", iOrderedPart)
				ENDIF
			ENDIF
			
			IF EventData.bBreakKeypadObj
				OBJECT_INDEX tempObj = NET_TO_OBJ(EventData.niObj)
				
				IF NETWORK_HAS_CONTROL_OF_ENTITY(tempObj)
					IF GET_IS_ENTITY_A_FRAG(tempObj)
						SET_DISABLE_FRAG_DAMAGE(tempObj, FALSE)
					ENDIF
					
					IF NOT IS_OBJECT_BROKEN_AND_VISIBLE(tempObj)
						DAMAGE_OBJECT_FRAGMENT_CHILD (tempObj, 0, -100)
						CDEBUG1LN(DEBUG_MISSION, "[THERMITE] PROCESS_SCRIPT_EVENT_FMMC_THERMITE_EFFECT_EVENT Damaging fragment for keypad minigame object.")
					ENDIF
				ENDIF
				
			ENDIF
			
			IF EventData.bClearProcessing
				//Force clear other players from blocking processing any other thermite whilst another is ongoing.
				IF EventData.Details.FromPlayerIndex != LocalPlayer
					CLEAR_BIT(iThermiteLocalBitset[iOrderedPart], THERMITE_LOCAL_BITSET_IS_ANY_THERMITE_PROCESSING)
					CDEBUG1LN(DEBUG_MISSION, "[THERMITE] PROCESS_SCRIPT_EVENT_FMMC_THERMITE_EFFECT_EVENT Clearing THERMITE_LOCAL_BITSET_IS_ANY_THERMITE_PROCESSING for Ordered Participant: ", iOrderedPart)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_SCRIPT_EVENT_GRAB_CASH(INT iEventID)

	//handle updating server cash grab take value
	IF bIsLocalPlayerHost
		SCRIPT_EVENT_DATA_FMMC_CASH_GRAB Event
		
		PRINTLN("[RCC MISSION][CashGrab][Interactable ", Event.iobjIndex, "] PROCESS_SCRIPT_EVENT_GRAB_CASH - has been received for host.")

		IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))
			INT iCashType = FMMC_CASH_ORNATE_BANK
			IF g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[Event.iobjIndex].sConsequences.iInteractable_CashConsequence != 0
				iCashType = g_FMMC_STRUCT.iCashReward
			ENDIF
			IF GET_TOTAL_CASH_GRAB_TAKE() + Event.iCashGrabbed > GET_HEIST_MODIFIED_TAKE_FOR_CASH_GRAB(iCashType)
				SET_TOTAL_CASH_GRAB_TAKE(GET_HEIST_MODIFIED_TAKE_FOR_CASH_GRAB(iCashType))
				PRINTLN("[RCC MISSION][CashGrab][Interactable ", Event.iobjIndex, "] - PROCESS_SCRIPT_EVENT_GRAB_CASH - Attempting to increase cash over the max (1) - ", GET_HEIST_MODIFIED_TAKE_FOR_CASH_GRAB(iCashType))
			ELSE
				SET_TOTAL_CASH_GRAB_TAKE(GET_TOTAL_CASH_GRAB_TAKE() + Event.iCashGrabbed)
			ENDIF
			IF MC_ServerBD.sCashGrab.iCashGrabTotal[Event.iobjIndex] + Event.iCashGrabbed > GET_HEIST_MODIFIED_TAKE_FOR_CASH_GRAB(iCashType)
				MC_ServerBD.sCashGrab.iCashGrabTotal[Event.iobjIndex] = GET_HEIST_MODIFIED_TAKE_FOR_CASH_GRAB(iCashType)
				PRINTLN("[RCC MISSION][CashGrab][Interactable ", Event.iobjIndex, "] - PROCESS_SCRIPT_EVENT_GRAB_CASH - Attempting to increase cash over the max (2) - ", GET_HEIST_MODIFIED_TAKE_FOR_CASH_GRAB(iCashType))
			ELSE
				MC_ServerBD.sCashGrab.iCashGrabTotal[Event.iobjIndex] += Event.iCashGrabbed
			ENDIF
			IF Event.bIsBonusCash
				MC_ServerBD.sCashGrab.iCashGrabBonusPilesGrabbed[Event.iobjIndex] += 1
			ELSE
				MC_ServerBD.sCashGrab.iCashGrabBasePilesGrabbed[Event.iobjIndex] += 1
			ENDIF
			
			#IF FEATURE_HEIST_ISLAND
			SET_ISLAND_HEIST_SECONDARY_LOOT_GRABBED(Event.iobjIndex)
			#ENDIF
			
			PRINTLN("[RCC MISSION][CashGrab][Interactable ", Event.iobjIndex, "] - PROCESS_SCRIPT_EVENT_GRAB_CASH - grabbed cash pile ", Event.iCurrentPile)
			IF Event.bIsBonusCash
				PRINTLN("[RCC MISSION] - PROCESS_SCRIPT_EVENT_GRAB_CASH - bonus pile grabbed ", MC_ServerBD.sCashGrab.iCashGrabBonusPilesGrabbed[Event.iobjIndex])
			ELSE
				PRINTLN("[RCC MISSION] - PROCESS_SCRIPT_EVENT_GRAB_CASH - not a bonus pile ", Event.iCurrentPile)
				PRINTLN("[RCC MISSION] - PROCESS_SCRIPT_EVENT_GRAB_CASH - base piles grabbed ", MC_ServerBD.sCashGrab.iCashGrabBasePilesGrabbed[Event.iobjIndex])
			ENDIF
			PRINTLN("[RCC MISSION] - PROCESS_SCRIPT_EVENT_GRAB_CASH - adding cash ", Event.iCashGrabbed, " for object ", Event.iobjIndex)
			PRINTLN("[RCC MISSION] - PROCESS_SCRIPT_EVENT_GRAB_CASH - new total ", MC_ServerBD.sCashGrab.iCashGrabTotal[Event.iobjIndex], " for object ", Event.iobjIndex)
			PRINTLN("[RCC MISSION] - PROCESS_SCRIPT_EVENT_GRAB_CASH - cash grab total take for mission ", GET_TOTAL_CASH_GRAB_TAKE())
			
		ENDIF
	ENDIF
	
	
	//handle playing sound each time take increases	
	SCRIPT_EVENT_DATA_FMMC_CASH_GRAB Event
	
	PRINTLN("[RCC MISSION] - PROCESS_SCRIPT_EVENT_GRAB_CASH - has been received for all players.")
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))
	
		IF Event.Details.FromPlayerIndex = LocalPlayer
			//sounds played on the player who just grabbed some cash
			IF Event.bFinalCashPile
				//when all cash in the cash grab is collected
				PLAY_SOUND_FRONTEND(-1, "LOCAL_PLYR_CASH_COUNTER_COMPLETE", "DLC_HEISTS_GENERAL_FRONTEND_SOUNDS", FALSE)
				PRINTLN("[RCC MISSION] - PROCESS_SCRIPT_EVENT_GRAB_CASH - Playing sound LOCAL_PLYR_CASH_COUNTER_COMPLETE.")
			ELSE
				//when any cash pile is collected
				PLAY_SOUND_FRONTEND(-1, "LOCAL_PLYR_CASH_COUNTER_INCREASE", "DLC_HEISTS_GENERAL_FRONTEND_SOUNDS", FALSE)
				PRINTLN("[RCC MISSION] - PROCESS_SCRIPT_EVENT_GRAB_CASH - Playing sound LOCAL_PLYR_CASH_COUNTER_INCREASE.")
			ENDIF
		ELSE
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_OnlyPlayLocalCashGrabSounds)
				//sounds played on any other player
				IF Event.bFinalCashPile
					//when all cash in the cash grab is collected
					PLAY_SOUND_FRONTEND(-1, "REMOTE_PLYR_CASH_COUNTER_COMPLETE", "DLC_HEISTS_GENERAL_FRONTEND_SOUNDS", FALSE)
					PRINTLN("[RCC MISSION] - PROCESS_SCRIPT_EVENT_GRAB_CASH - Playing sound REMOTE_PLYR_CASH_COUNTER_COMPLETE.")
				ELSE
					//when any cash pile is collected
					PLAY_SOUND_FRONTEND(-1, "REMOTE_PLYR_CASH_COUNTER_INCREASE", "DLC_HEISTS_GENERAL_FRONTEND_SOUNDS", FALSE)
					PRINTLN("[RCC MISSION] - PROCESS_SCRIPT_EVENT_GRAB_CASH - Playing sound REMOTE_PLYR_CASH_COUNTER_INCREASE.")
				ENDIF
			ENDIF
		ENDIF
	
		//no longer playing this sound, replaced with new ones, B*2216312
		//play ticker sound for take counter incrementing, B*2084562
		//PLAY_SOUND_FRONTEND(-1, "ROBBERY_MONEY_TOTAL", "HUD_FRONTEND_CUSTOM_SOUNDSET", FALSE)
	ENDIF
ENDPROC

PROC PROCESS_FMMC_ADD_GRABBED_CASH(INT iEventID)
	
	SCRIPT_EVENT_DATA_FMMC_ADD_GRABBED_CASH Event
	
	IF NOT GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))
		EXIT
	ENDIF
	
	IF NOT g_TransitionSessionNonResetVars.bDisplayCashGrabTake
		g_TransitionSessionNonResetVars.bDisplayCashGrabTake = TRUE
		PRINTLN("[RCC MISSION][GrabbedCash] PROCESS_FMMC_ADD_GRABBED_CASH - Turning on bDisplayCashGrabTake")
	ENDIF
	
	IF bIsLocalPlayerHost
		SET_TOTAL_CASH_GRAB_TAKE(GET_TOTAL_CASH_GRAB_TAKE() + Event.iCashToAdd)
		PRINTLN("[RCC MISSION][GrabbedCash] PROCESS_FMMC_ADD_GRABBED_CASH - Gaining cash: ", Event.iCashToAdd, " / iGrabbedCashTotalTake: ", GET_TOTAL_CASH_GRAB_TAKE())
	ENDIF
	
	SWITCH Event.iCashSource
		CASE ciGRABBED_CASH_SOURCE__EXTRA_TAKE_CASH_PICKUPS
			iExtraTakeFromPlacedCashPickups += Event.iCashToAdd
			PRINTLN("[RCC MISSION][GrabbedCash] PROCESS_FMMC_ADD_GRABBED_CASH - Adding ", Event.iCashToAdd, " to iExtraTakeFromPlacedCashPickups which is now ", iExtraTakeFromPlacedCashPickups)
		BREAK
	ENDSWITCH
	
ENDPROC

PROC PROCESS_FMMC_REMOVE_GRABBED_CASH(INT iEventID)
	
	SCRIPT_EVENT_DATA_FMMC_REMOVE_GRABBED_CASH Event
	
	IF NOT GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))
		EXIT
	ENDIF
	
	IF bIsLocalPlayerHost
		//update server data
		SET_TOTAL_CASH_GRAB_DROPPED(GET_TOTAL_CASH_GRAB_DROPPED() + Event.iCashToRemove)
		SET_TOTAL_CASH_GRAB_TAKE(CLAMP_INT(GET_TOTAL_CASH_GRAB_TAKE() - Event.iCashToRemove, 0, GET_TOTAL_CASH_GRAB_TAKE()))
		
		PRINTLN("[RCC MISSION][GrabbedCash] PROCESS_FMMC_REMOVE_GRABBED_CASH - Dropping cash ", Event.iCashToRemove)
		PRINTLN("[RCC MISSION][GrabbedCash] PROCESS_FMMC_REMOVE_GRABBED_CASH - Total drop ", GET_TOTAL_CASH_GRAB_DROPPED())
		PRINTLN("[RCC MISSION][GrabbedCash] PROCESS_FMMC_REMOVE_GRABBED_CASH - Cash grab total take after this drop ", GET_TOTAL_CASH_GRAB_TAKE())
	ENDIF
	
	bDrawCashGrabTakeRed 		= TRUE
	iDrawCashGrabTakeRedTime	= 0
	iDrawCashGrabTakeRedDuration = Event.iRedFlashTime
	
ENDPROC

PROC PROCESS_SCRIPT_EVENT_GRAB_CASH_DISPLAY_TAKE(INT iEventID)

	SCRIPT_EVENT_DATA_FMMC_JUST_EVENT Event
	
	PRINTLN("[RCC MISSION] - PROCESS_SCRIPT_EVENT_GRAB_CASH_DISPLAY_TAKE - has been received.")

	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))
	
		g_TransitionSessionNonResetVars.bDisplayCashGrabTake = TRUE
		
		PRINTLN("[RCC MISSION] - PROCESS_SCRIPT_EVENT_DISPLAY_CASH_GRAB_TAKE - setting global g_TransitionSessionNonResetVars.bDisplayCashGrabTake to ", g_TransitionSessionNonResetVars.bDisplayCashGrabTake)
	ENDIF

ENDPROC

PROC PROCESS_FMMC_CONTAINER_BEING_INVESTIGATED(INT iCount)

	SCRIPT_EVENT_DATA_CONTAINER_INVESTIGATED  EventData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_INVESTIGATE_CONTAINER
			IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, EventData, SIZE_OF(EventData))
				PRINTLN("[RCC MISSION] PROCESS_FMMC_CONTAINER_BEING_INVESTIGATED = ", EventData.iObj, " BY = ", EventData.iPart)
				IF EventData.iObj > -1
					IF iTrackifyTargets[EventData.iObj] > -1
						IF bIsLocalPlayerHost
							IF EventData.bInvestigationInterrupted
								IF IS_BIT_SET(MC_serverBD.iServerObjectBeingInvestigatedBitSet, EventData.iObj)
									PRINTLN("[RCC MISSION] - bInvestigationInterrupted for iObj - ", EventData.iObj)
									CLEAR_BIT(MC_serverBD.iServerObjectBeingInvestigatedBitSet, EventData.iObj)
									
									IF IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_MINIGAME_OBJ_TARGET_BEING_INVESTIGATED)
										CLEAR_BIT(MC_serverBD.iServerBitSet3, SBBOOL3_MINIGAME_OBJ_TARGET_BEING_INVESTIGATED)
									ENDIF
									
									IF IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_MINIGAME_SHOULD_SHOW_COLLECTABLE)
										CLEAR_BIT(MC_serverBD.iServerBitSet3, SBBOOL3_MINIGAME_SHOULD_SHOW_COLLECTABLE)
										PRINTLN("[RCC MISSION] PROCESS_FMMC_CONTAINER_BEING_INVESTIGATED ... Interrupted ... CLEAR_BIT(MC_serverBD.iServerBitSet3, SBBOOL3_MINIGAME_SHOULD_SHOW_COLLECTABLE)")
									ENDIF
									
									MC_serverBD.iObjHackPart[iTrackifyTargets[EventData.iObj]] = - 1
									PRINTLN("[RCC MISSION] - MC_serverBD.iObjHackPart[", iTrackifyTargets[EventData.iObj], "] = -1")
								ENDIF
							ELSE
								SET_BIT(MC_serverBD.iServerObjectBeingInvestigatedBitSet, EventData.iObj)
								
								IF MC_serverBD.iObjHackPart[iTrackifyTargets[EventData.iObj]] = -1
									MC_serverBD.iObjHackPart[iTrackifyTargets[EventData.iObj]] = EventData.iPart
								ELSE
									PRINTLN("[RCC MISSION] PROCESS_FMMC_CONTAINER_BEING_INVESTIGATED iObjHackPart is already assigned ... iObjHackPart = ", MC_serverBD.iObjHackPart[iTrackifyTargets[EventData.iObj]])
								ENDIF
								
								IF EventData.bTarget = TRUE
									SET_BIT(MC_serverBD.iServerBitSet3, SBBOOL3_MINIGAME_OBJ_TARGET_BEING_INVESTIGATED)
									PRINTLN("[RCC MISSION] PROCESS_FMMC_CONTAINER_BEING_INVESTIGATED is Target")
								ELSE
									//CLEAR_BIT(MC_serverBD.iServerBitSet3, SBBOOL3_MINIGAME_OBJ_TARGET_BEING_INVESTIGATED)
									PRINTLN("[RCC MISSION] PROCESS_FMMC_CONTAINER_BEING_INVESTIGATED is Empty")
								ENDIF

								IF EventData.bShowCollectable = TRUE
									SET_BIT(MC_serverBD.iServerBitSet3, SBBOOL3_MINIGAME_SHOULD_SHOW_COLLECTABLE)
									PRINTLN("[RCC MISSION] PROCESS_FMMC_CONTAINER_BEING_INVESTIGATED = ", EventData.iObj, " BY = ", EventData.iPart , " SHOW COLLECTABLE")
								ELSE
									CLEAR_BIT(MC_serverBD.iServerBitSet3, SBBOOL3_MINIGAME_SHOULD_SHOW_COLLECTABLE)
									PRINTLN("[RCC MISSION] PROCESS_FMMC_CONTAINER_BEING_INVESTIGATED ... CLEAR_BIT(MC_serverBD.iServerBitSet3, SBBOOL3_MINIGAME_SHOULD_SHOW_COLLECTABLE)")
								ENDIF
								
								IF EventData.bTarget = TRUE
								AND EventData.bInvestigationFinished = TRUE
									SET_BIT(MC_serverBD.iServerBitSet3, SBBOOL3_MINIGAME_TARGET_FOUND)
									PRINTLN("[RCC MISSION] PROCESS_FMMC_CONTAINER_BEING_INVESTIGATED = TARGET FOUND")
								ENDIF
							ENDIF
						ENDIF
						IF NOT EventData.bInvestigationInterrupted
						AND EventData.bTarget
							g_iDawnRaidPickupCarrierPart = EventData.iPart
							PRINTLN("[RCC MISSION] PROCESS_FMMC_CONTAINER_BEING_INVESTIGATED - g_iDawnRaidPickupCarrierPart = ", EventData.iPart)
						ENDIF
					ELSE
						PRINTLN("[RCC MISSION] - Obj has no read object index - ", EventData.iObj)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_SCRIPT_EVENT_SET_CUT_OUT_PAINTING_SUBANIM_STATE(INT iEventID)
	IF (bIsLocalPlayerHost)
		SCRIPT_EVENT_DATA_CASINO_UPDATED_STEAL_PAINTING_STATE EventData
		IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
			// Set a value in serverBD for the cut stage of the specific painting			
			MC_serverBD_1.iCurrentCutPaintingSubAnim[EventData.iCurrentPaintingIndex] = EventData.iCurrentCutPaintingState
			PRINTLN("[CutPainting] PROCESS_SCRIPT_EVENT_SET_CUT_OUT_PAINTING_SUBANIM_STATE | Setting MC_serverBD_1.iCurrentCutPaintingSubAnim[", EventData.iCurrentPaintingIndex, "] to ", MC_serverBD_1.iCurrentCutPaintingSubAnim[EventData.iCurrentPaintingIndex])
		ENDIF
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Interactables	 -----------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Processes for Interactable functionality  			------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

//PURPOSE: To perform any actions that this Interactable is set up to do according to its sConsequences data
PROC PROCESS_HOST_INTERACTABLE_ONESHOT_CONSEQUENCES(INT iInteractable, FMMC_INTERACTABLE_CONSEQUENCES& sConsequences, INT iParticipant)
	
	//Only used for prints
	UNUSED_PARAMETER(iInteractable)
	
	// Trigger Completion Model Swap //
	IF FMMC_IS_LONG_BIT_SET(iInteractable_CompletionModelSwapRegisteredBS, iInteractable)
		PRINTLN("[ModelSwaps][Interactables][Interactable ", iInteractable, "] PROCESS_HOST_INTERACTABLE_ONESHOT_CONSEQUENCES | Triggering Interactable's completion model swap")
		TRIGGER_INTERACTABLE_COMPLETION_MODEL_SWAP(iInteractable)
	ELSE
		PRINTLN("[ModelSwaps][Interactables_SPAM][Interactable_SPAM ", iInteractable, "] PROCESS_HOST_INTERACTABLE_ONESHOT_CONSEQUENCES | Interactable hasn't registered a completion model swap")
	ENDIF
	
	//Give points to the completing team if required
	IF sConsequences.iInteractable_PointsForCompletion > 0
		PRINTLN("[Interactables][Interactable ", iInteractable, "] PROCESS_HOST_INTERACTABLE_ONESHOT_CONSEQUENCES | Triggering Interactable's points for completion")
		INT iTeam = MC_playerBD[iParticipant].iTeam		
		IF iTeam >= 0 AND iTeam < FMMC_MAX_TEAMS
			INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
			IF iRule >= 0 AND iRule < FMMC_MAX_RULES
				INCREMENT_SERVER_TEAM_SCORE(iTeam, iRule, sConsequences.iInteractable_PointsForCompletion)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

//PURPOSE: To undo any one-shot consequences that would've been done when this Interactable was previously completed
PROC PROCESS_HOST_REVERSING_INTERACTABLE_ONESHOT_CONSEQUENCES(INT iInteractable, FMMC_INTERACTABLE_CONSEQUENCES& sConsequences)
	
	UNUSED_PARAMETER(iInteractable) // Currently just used for prints
	UNUSED_PARAMETER(sConsequences)
	
ENDPROC

PROC PROCESS_CLIENT_INTERACTABLE_ONESHOT_ATTACHED_ENTITY_CONSEQUENCES(INT iInteractable, FMMC_INTERACTABLE_CONSEQUENCES& sConsequences)

	INT iAttachedEntityType = g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iInteractable_AttachedEntityType
	INT iAttachedEntityIndex = g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iInteractable_AttachedEntityIndex
	
	SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iInteractable_InteractionType
		CASE ciInteractableInteraction_SwitchOutfit
			IF iAttachedEntityType = ciENTITY_TYPE_PED
				PED_INDEX piAttachedPed
				piAttachedPed = GET_FMMC_ENTITY_PED(iAttachedEntityIndex)
				
				IF NETWORK_HAS_CONTROL_OF_ENTITY(piAttachedPed)
					PRINTLN("[Interactables][Interactable ", iInteractable, "] PROCESS_CLIENT_INTERACTABLE_ONESHOT_ATTACHED_ENTITY_CONSEQUENCES | Setting ped variation to ", sConsequences.iInteractable_AttachedEntityConsequenceGenericInt)
					FMMC_SET_PED_VARIATION(piAttachedPed, GET_ENTITY_MODEL(piAttachedPed), sConsequences.iInteractable_AttachedEntityConsequenceGenericInt)
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC PROCESS_CLIENT_INTERACTABLE_ONESHOT_CONSEQUENCES(INT iInteractable, FMMC_INTERACTABLE_CONSEQUENCES& sConsequences, INT iParticipant, BOOL bCompletedByContinuity = FALSE)

	IF HAS_NET_TIMER_STARTED(stInteractableCompletedTimers[iInteractable])
	AND NOT IS_INTERACTABLE_MULTI_USE(iInteractable)
		PRINTLN("[Interactables][Interactable ", iInteractable, "] PROCESS_CLIENT_INTERACTABLE_ONESHOT_CONSEQUENCES | Blocking completion event due to stInteractableCompletedTimers")
		EXIT
	ENDIF
	
	OBJECT_INDEX oiInteractable
	BOOL bBlockCompletionTicker = FALSE
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GET_INTERACTABLE_NET_ID(iInteractable))
		oiInteractable = NET_TO_OBJ(GET_INTERACTABLE_NET_ID(iInteractable))
	ENDIF
	
	FMMC_SET_LONG_BIT(iInteractable_WasCompletedBS, iInteractable)
	REINIT_NET_TIMER(stInteractableCompletedTimers[iInteractable])
	FMMC_CLEAR_LONG_BIT(iInteractable_SentCompletionTimerExpiryEventBS, iInteractable)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].sConsequences.iInteractable_StayCompleteDuration = ciInteractableDuration_StayCompleteForever
		IF NOT FMMC_IS_LONG_BIT_SET(iInteractable_StayCompleteForeverBS, iInteractable)
			FMMC_SET_LONG_BIT(iInteractable_StayCompleteForeverBS, iInteractable)
			PRINTLN("[Interactables][Interactable ", iInteractable, "] PROCESS_CLIENT_INTERACTABLE_ONESHOT_CONSEQUENCES | Setting iInteractable_StayCompleteForeverBS for Interactable ", iInteractable)
		ELSE
			PRINTLN("[Interactables][Interactable ", iInteractable, "] PROCESS_CLIENT_INTERACTABLE_ONESHOT_CONSEQUENCES | iInteractable_StayCompleteForeverBS is already set for Interactable ", iInteractable)
		ENDIF
	ENDIF
	
	// Linked Interactables //
	IF NOT IS_LONG_BITSET_EMPTY(sConsequences.iInteractable_LinkedInteractablesBS)
		INT iLinkedInteractable
		FOR iLinkedInteractable = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfInteractables - 1
			IF NOT FMMC_IS_LONG_BIT_SET(sConsequences.iInteractable_LinkedInteractablesBS, iLinkedInteractable)
				// These Interactables aren't linked
				RELOOP
			ENDIF
			
			IF bIsLocalPlayerHost
				IF NOT IS_INTERACTABLE_COMPLETE(iLinkedInteractable)
					PRINTLN("[Interactables][Interactable ", iInteractable, "] PROCESS_CLIENT_INTERACTABLE_ONESHOT_CONSEQUENCES | Host sending event to complete linked Interactable ", iLinkedInteractable)
					
					IF bCompletedByContinuity
						BROADCAST_FMMC_INTERACTABLE_EVENT(FMMC_INTERACTABLE_EVENT_TYPE__INTERACTION_COMPLETED_ON_PREVIOUS_MISSION, iLinkedInteractable)
					ELSE
						BROADCAST_FMMC_INTERACTABLE_EVENT(FMMC_INTERACTABLE_EVENT_TYPE__INTERACTION_COMPLETE, iLinkedInteractable)
					ENDIF
				ENDIF
			ENDIF
			
			#IF IS_DEBUG_BUILD
			IF FMMC_IS_LONG_BIT_SET(iInteractable_StayCompleteForeverBS, iInteractable)
				PRINTLN("[Interactables][Interactable ", iInteractable, "] PROCESS_CLIENT_INTERACTABLE_ONESHOT_CONSEQUENCES | Setting iInteractable_StayCompleteForeverBS for linked Interactable ", iLinkedInteractable)
			ENDIF
			#ENDIF
			
			FMMC_SET_LONG_BIT(iInteractable_StayCompleteForeverBS, iLinkedInteractable)
			
		ENDFOR
	ENDIF
	
	// Attached Entity //
	IF g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iInteractable_AttachedEntityType != ciENTITY_TYPE_NONE
		PROCESS_CLIENT_INTERACTABLE_ONESHOT_ATTACHED_ENTITY_CONSEQUENCES(iInteractable, sConsequences)
	ENDIF
	
	// Attach Object //
	IF g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].sConsequences.iInteractable_AttachObj != -1
		SET_OBJECT_TO_BE_PICKED_UP_ASAP(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].sConsequences.iInteractable_AttachObj)
	ENDIF
	
	// Complete PreReq //
	IF sConsequences.iInteractable_PreReqToComplete != ciPREREQ_None
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].sConsequences.iInteractable_ConsequencesBS, ciInteractableConsequences_CompletePreReqLocalOnly)
			IF iParticipant = iLocalPart
			OR iParticipant = -1
				PRINTLN("[Interactables][Interactable ", iInteractable, "] PROCESS_CLIENT_INTERACTABLE_ONESHOT_CONSEQUENCES | Completing PreReq ", sConsequences.iInteractable_PreReqToComplete, " because I'm the interaction completer (", iParticipant, ")!")
				SET_PREREQUISITE_COMPLETED(sConsequences.iInteractable_PreReqToComplete)
			ELSE
				PRINTLN("[Interactables][Interactable ", iInteractable, "] PROCESS_CLIENT_INTERACTABLE_ONESHOT_CONSEQUENCES | PreReq ", sConsequences.iInteractable_PreReqToComplete, " is set to be complete for the completer (", iParticipant, ") only!")
			ENDIF
		ELSE
			PRINTLN("[Interactables][Interactable ", iInteractable, "] PROCESS_CLIENT_INTERACTABLE_ONESHOT_CONSEQUENCES | Completing PreReq ", sConsequences.iInteractable_PreReqToComplete)
			SET_PREREQUISITE_COMPLETED(sConsequences.iInteractable_PreReqToComplete)
		ENDIF
	ENDIF
	
	// Clear PreReq //
	IF sConsequences.iInteractable_PreReqToClear != ciPREREQ_None
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].sConsequences.iInteractable_ConsequencesBS, ciInteractableConsequences_CompletePreReqLocalOnly)
			IF iParticipant = iLocalPart
			OR iParticipant = -1
				PRINTLN("[Interactables][Interactable ", iInteractable, "] PROCESS_CLIENT_INTERACTABLE_ONESHOT_CONSEQUENCES | Completing PreReq ", sConsequences.iInteractable_PreReqToClear, " because I'm the interaction completer (", iParticipant, ")!")
				RESET_PREREQUISITE_COMPLETED(sConsequences.iInteractable_PreReqToClear)
			ELSE
				PRINTLN("[Interactables][Interactable ", iInteractable, "] PROCESS_CLIENT_INTERACTABLE_ONESHOT_CONSEQUENCES | PreReq ", sConsequences.iInteractable_PreReqToClear, " is set to be reset for the completer (", iParticipant, ") only!")
			ENDIF
		ELSE
			PRINTLN("[Interactables][Interactable ", iInteractable, "] PROCESS_CLIENT_INTERACTABLE_ONESHOT_CONSEQUENCES | Resetting PreReq ", sConsequences.iInteractable_PreReqToClear)
			RESET_PREREQUISITE_COMPLETED(sConsequences.iInteractable_PreReqToClear)
		ENDIF
	ENDIF
		
	// Deploy EMP //
	IF IS_BIT_SET(sConsequences.iInteractable_ConsequencesBS, ciInteractableConsequences_DeployEMP)
	
		BOOL bCanDeployEMP = NOT ARE_LIGHTS_TURNED_OFF()
		
		IF IS_BIT_SET(sConsequences.iInteractable_ConsequencesBS, ciInteractableConsequences_DeployEMP_RequireAll)
			iInteractable_EMPChargesRequired--
			PRINTLN("[Interactables][Interactable ", iInteractable, "] PROCESS_CLIENT_INTERACTABLE_ONESHOT_CONSEQUENCES | Charging EMP! iInteractable_EMPChargesRequired: ", iInteractable_EMPChargesRequired)
			
			IF iInteractable_EMPChargesRequired > 0
				bCanDeployEMP = FALSE
				bBlockCompletionTicker = TRUE // Don't show the ticker for this Interactable
			ENDIF
		ENDIF
		
		IF bCanDeployEMP
		AND NOT bCompletedByContinuity
			iInteractable_EMPCauserIndex = iInteractable
			PRINTLN("[Interactables][Interactable ", iInteractable, "] PROCESS_CLIENT_INTERACTABLE_ONESHOT_CONSEQUENCES | Setting iInteractable_EMPCauserIndex to ", iInteractable_EMPCauserIndex)
				
			IF bIsLocalPlayerHost
				PRINTLN("[Interactables][Interactable ", iInteractable, "] PROCESS_CLIENT_INTERACTABLE_ONESHOT_CONSEQUENCES | Deploying EMP now!")
				BROADCAST_FMMC_SET_ARTIFICIAL_LIGHTS(eARTIFICIAL_LIGHTS_STATE__OFF)
			ENDIF
		ENDIF
	ENDIF
	
	// Unlock Doors //
	IF sConsequences.iInteractable_DoorsToUnlockBS != 0
		INT iDoor
		FOR iDoor = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfDoors - 1
			IF IS_BIT_SET(sConsequences.iInteractable_DoorsToUnlockBS, iDoor)
				SET_DOOR_AS_UNLOCKED_LOCAL(iDoor)
			ENDIF
		ENDFOR
	ENDIF
	
	// Lock Doors //
	IF sConsequences.iInteractable_DoorsToLockBS != 0
		INT iDoor
		FOR iDoor = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfDoors - 1
			IF IS_BIT_SET(sConsequences.iInteractable_DoorsToLockBS, iDoor)
				SET_DOOR_AS_LOCKED_LOCAL(iDoor)
			ENDIF
		ENDFOR
	ENDIF
	
	// Doors To Switch Config //
	IF sConsequences.iInteractable_DoorsToSwitchConfigBS != 0
		INT iDoor
		FOR iDoor = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfDoors - 1
			IF IS_BIT_SET(sConsequences.iInteractable_DoorsToSwitchConfigBS, iDoor)
				SET_DOOR_TO_USE_ALT_CONFIG_LOCAL(iDoor)
			ENDIF
		ENDFOR
	ENDIF
	
	// Kill Interactable //
	IF IS_BIT_SET(sConsequences.iInteractable_ConsequencesBS, ciInteractableConsequences_KillInteractable)
		IF DOES_ENTITY_EXIST(oiInteractable)
		AND NETWORK_HAS_CONTROL_OF_ENTITY(oiInteractable)
			PRINTLN("[Interactables][Interactable ", iInteractable, "] PROCESS_CLIENT_INTERACTABLE_ONESHOT_CONSEQUENCES | Killing Interactable due to ciInteractableConsequences_KillInteractable")
			SET_ENTITY_HEALTH(oiInteractable, 0)
			
			DAMAGE_OBJECT_FRAGMENT_CHILD(oiInteractable, 0, 0.0)
			BREAK_OBJECT_FRAGMENT_CHILD(oiInteractable, 0, FALSE)
		ENDIF
	ENDIF
	
	// Give Cash //
	IF NOT bCompletedByContinuity
		IF SHOULD_INTERACTABLE_GIVE_CASH_WHEN_COMPLETED(iInteractable)
			IF iParticipant = iLocalPart
				PRINTLN("[Interactables][Interactable ", iInteractable, "] PROCESS_CLIENT_INTERACTABLE_ONESHOT_CONSEQUENCES | Giving cash now! Adding Cash: ", sConsequences.iInteractable_CashConsequence)
				BROADCAST_FMMC_ADD_GRABBED_CASH(sConsequences.iInteractable_CashConsequence, ciGRABBED_CASH_SOURCE__INTERACTABLE_CONSEQUENCE)
				ADD_TO_LOCAL_CASH_GRABBED(sConsequences.iInteractable_CashConsequence)
				BAG_CAPACITY__ADD_LOOT(BAG_CAPACITY__GET_LOOT_WEIGHT_FOR_GRAB(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].mnInteractable_Model), GET_LOOT_TYPE_FROM_MODEL(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].mnInteractable_Model))
				PROCESS_ISLAND_HEIST_CASH_GRAB_TELEMETRY(GET_LOOT_TYPE_FROM_MODEL(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].mnInteractable_Model), 1, sConsequences.iInteractable_CashConsequence, ROUND(BAG_CAPACITY__GET_LOOT_WEIGHT_FOR_GRAB(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].mnInteractable_Model)))
			ENDIF
		ENDIF
	ENDIF
	
	// Ticker Message //
	IF IS_CUSTOM_STRING_LIST_STRING_VALID(sConsequences.iInteractable_StringListTickerMessage_Standard)
	AND NOT bCompletedByContinuity
		IF bBlockCompletionTicker
			PRINTLN("[Interactables][Interactable ", iInteractable, "] PROCESS_CLIENT_INTERACTABLE_ONESHOT_CONSEQUENCES | Ticker was blocked due to bBlockCompletionTicker!")
			
		ELSE
			IF IS_CUSTOM_STRING_LIST_STRING_VALID(sConsequences.iInteractable_StringListTickerMessage_CompleterOnly)
				// Using different text labels for the completer ("You completed the interaction.") and other players ("~a~ completed the Interaction.")
				IF iParticipant = iLocalPart
					PRINTLN("[Interactables][Interactable ", iInteractable, "] PROCESS_CLIENT_INTERACTABLE_ONESHOT_CONSEQUENCES | Printing completer-only ticker message from string list (Custom String ", sConsequences.iInteractable_StringListTickerMessage_CompleterOnly, ")")
					TEXT_LABEL_63 tlTickerMessage = GET_CUSTOM_STRING_LIST_TEXT_LABEL(sConsequences.iInteractable_StringListTickerMessage_CompleterOnly)
					PRINT_TICKER(tlTickerMessage)
				ELSE
					PRINTLN("[Interactables][Interactable ", iInteractable, "] PROCESS_CLIENT_INTERACTABLE_ONESHOT_CONSEQUENCES | Printing standard ticker message from string list (Custom String ", sConsequences.iInteractable_StringListTickerMessage_CompleterOnly, ")")
					TEXT_LABEL_63 tlTickerMessage = GET_CUSTOM_STRING_LIST_TEXT_LABEL(sConsequences.iInteractable_StringListTickerMessage_Standard)
					PRINT_TICKER_WITH_PLAYER_NAME(tlTickerMessage, GET_PARTICIPANT_PLAYER(iParticipant))
				ENDIF
				
			ELSE
				// Use the same text label for the player who completes the Interaction and remote players
				PRINTLN("[Interactables][Interactable ", iInteractable, "] PROCESS_CLIENT_INTERACTABLE_ONESHOT_CONSEQUENCES | Printing ticker message from string list (Custom String ", sConsequences.iInteractable_StringListTickerMessage_Standard, ")")
				TEXT_LABEL_63 tlTickerMessage = GET_CUSTOM_STRING_LIST_TEXT_LABEL(sConsequences.iInteractable_StringListTickerMessage_Standard)
				
				IF IS_BIT_SET(sConsequences.iInteractable_ConsequencesBS, ciInteractableConsequences_InsertCompleterNameIntoTicker)
					// Insert "You" into the completer's text label, and the completer's name into the text labels for all other players.
					IF iParticipant = iLocalPart
					AND GET_CURRENT_LANGUAGE() = LANGUAGE_ENGLISH // This sentence structure only works in English
						PRINT_TICKER_WITH_STRING(tlTickerMessage, "INT_TCK_YOU")
						
					ELSE
						IF GET_PARTICIPANT_PLAYER(iParticipant) != INVALID_PLAYER_INDEX()
							PRINT_TICKER_WITH_PLAYER_NAME(tlTickerMessage, GET_PARTICIPANT_PLAYER(iParticipant))
						ENDIF
						
					ENDIF
					
				ELSE
					// Print a basic ticker for all players
					PRINT_TICKER(tlTickerMessage)
					
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF iParticipant = iLocalPart
		//Give Inventory//
		IF sConsequences.iInteractable_InventoryToGive != -1
			UPDATE_CUSTOM_MID_MISSION_INVENTORY(sConsequences.iInteractable_InventoryToGive)
			GIVE_LOCAL_PLAYER_WEAPON_INVENTORY(FMMC_INVENTORY_CUSTOM_MID_MISSION, WEAPONINHAND_LASTWEAPON_BOTH)
		ENDIF
		#IF FEATURE_DLC_1_2022
		IF IS_BIT_SET(sConsequences.iInteractable_ConsequencesBS, ciInteractableConsequences_IncreaseTimeByOnRuleStartAmount)
			INT iIncrementTime = GET_AMOUNT_OF_TIME_TO_ADD_FOR_TEAM(GET_LOCAL_PLAYER_TEAM(), GET_LOCAL_PLAYER_CURRENT_RULE())
			BROADCAST_FMMC_INCREMENT_REMAINING_TIME(GET_LOCAL_PLAYER_TEAM(), iIncrementTime, TRUE, -1)
		ENDIF
		#ENDIF
		IF sConsequences.iInteractable_HeistGearConsequence != -1
			INT iTeam = GET_LOCAL_PLAYER_TEAM()
			CACHE_HEIST_GEAR(sConsequences.iInteractable_HeistGearConsequence)
			IF IS_HEIST_GEAR_A_BAG(sApplyOutfitData.eGear)
				SET_BAG_FOR_HEIST(iTeam, DEFAULT, sConsequences.iInteractable_HeistGearConsequence)
			ENDIF
			SET_MP_HEIST_GEAR(LocalPlayerPed, sApplyOutfitData.eGear)
			IF IS_HEIST_GEAR_A_BAG(sApplyOutfitData.eGear)
				SET_CACHED_HEIST_GEAR_BAG()
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_REVERSING_CLIENT_INTERACTABLE_ONESHOT_CONSEQUENCES(INT iInteractable, FMMC_INTERACTABLE_CONSEQUENCES& sConsequences, INT iParticipant)

	IF NOT HAS_NET_TIMER_STARTED(stInteractableCompletedTimers[iInteractable])
		EXIT
	ENDIF
	
	OBJECT_INDEX oiInteractable
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GET_INTERACTABLE_NET_ID(iInteractable))
		oiInteractable = NET_TO_OBJ(GET_INTERACTABLE_NET_ID(iInteractable))
	ENDIF
	
	RESET_NET_TIMER(stInteractableCompletedTimers[iInteractable])
	FMMC_CLEAR_LONG_BIT(iInteractable_SentCompletionTimerExpiryEventBS, iInteractable)
	
	// Linked Interactables //
	IF NOT IS_LONG_BITSET_EMPTY(sConsequences.iInteractable_LinkedInteractablesBS)
		INT iLinkedInteractable
		FOR iLinkedInteractable = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfInteractables - 1
			IF bIsLocalPlayerHost
				IF FMMC_IS_LONG_BIT_SET(sConsequences.iInteractable_LinkedInteractablesBS, iLinkedInteractable)
				AND IS_INTERACTABLE_COMPLETE(iLinkedInteractable, DEFAULT, FALSE)
					PRINTLN("[Interactables][Interactable ", iInteractable, "] PROCESS_REVERSING_CLIENT_INTERACTABLE_ONESHOT_CONSEQUENCES | Host sending event to un-complete linked Interactable ", iLinkedInteractable)
					BROADCAST_FMMC_INTERACTABLE_EVENT(FMMC_INTERACTABLE_EVENT_TYPE__INTERACTION_NO_LONGER_COMPLETE, iLinkedInteractable)
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	
	// Complete PreReq //
	IF sConsequences.iInteractable_PreReqToComplete != ciPREREQ_None
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].sConsequences.iInteractable_ConsequencesBS, ciInteractableConsequences_CompletePreReqLocalOnly)
			IF iParticipant = iLocalPart
			OR iParticipant = -1
				RESET_PREREQUISITE_COMPLETED(sConsequences.iInteractable_PreReqToComplete)
			ELSE
				PRINTLN("[Interactables][Interactable ", iInteractable, "] PROCESS_REVERSING_CLIENT_INTERACTABLE_ONESHOT_CONSEQUENCES | PreReq ", sConsequences.iInteractable_PreReqToComplete, " is set to be reset for the uncompleter (", iParticipant, ") only!")
			ENDIF
		ELSE
			RESET_PREREQUISITE_COMPLETED(sConsequences.iInteractable_PreReqToComplete)
		ENDIF
	ENDIF
	
	// Clear PreReq //
	IF sConsequences.iInteractable_PreReqToClear != ciPREREQ_None
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].sConsequences.iInteractable_ConsequencesBS, ciInteractableConsequences_CompletePreReqLocalOnly)
			IF iParticipant = iLocalPart
			OR iParticipant = -1
				SET_PREREQUISITE_COMPLETED(sConsequences.iInteractable_PreReqToClear)
			ELSE
				PRINTLN("[Interactables][Interactable ", iInteractable, "] PROCESS_REVERSING_CLIENT_INTERACTABLE_ONESHOT_CONSEQUENCES | PreReq ", sConsequences.iInteractable_PreReqToClear, " is set to be set for the uncompleter (", iParticipant, ") only!")
			ENDIF
		ELSE
			SET_PREREQUISITE_COMPLETED(sConsequences.iInteractable_PreReqToClear)
		ENDIF
	ENDIF
	
	// Deploy EMP //
	IF IS_BIT_SET(sConsequences.iInteractable_ConsequencesBS, ciInteractableConsequences_DeployEMP)
		BOOL bCanReverseEMP = ARE_LIGHTS_TURNED_OFF()
		
		IF IS_BIT_SET(sConsequences.iInteractable_ConsequencesBS, ciInteractableConsequences_DeployEMP_RequireAll)
			IF iInteractable_EMPCauserIndex != iInteractable
			AND iInteractable_EMPCauserIndex != -1
				// This Interactable isn't the one that deployed the EMP in the first place - let that Interactable handle this when its own timer runs out
				bCanReverseEMP = FALSE
				PRINTLN("[Interactables][Interactable ", iInteractable, "] PROCESS_CLIENT_INTERACTABLE_ONESHOT_CONSEQUENCES | This Interactable isn't the EMP causer! iInteractable_EMPCauserIndex: ", iInteractable_EMPCauserIndex)
			ENDIF
			
			iInteractable_EMPChargesRequired++
			PRINTLN("[Interactables][Interactable ", iInteractable, "] PROCESS_REVERSING_CLIENT_INTERACTABLE_ONESHOT_CONSEQUENCES | Reverse-charging EMP! iInteractable_EMPChargesRequired: ", iInteractable_EMPChargesRequired)
		ENDIF
		
		IF bCanReverseEMP
			iInteractable_EMPCauserIndex = -1
			PRINTLN("[Interactables][Interactable ", iInteractable, "] PROCESS_REVERSING_CLIENT_INTERACTABLE_ONESHOT_CONSEQUENCES | Clearing iInteractable_EMPCauserIndex!")
			
			IF bIsLocalPlayerHost
				PRINTLN("[Interactables][Interactable ", iInteractable, "] PROCESS_REVERSING_CLIENT_INTERACTABLE_ONESHOT_CONSEQUENCES | Reversing EMP now!")
				BROADCAST_FMMC_SET_ARTIFICIAL_LIGHTS(eARTIFICIAL_LIGHTS_STATE__DEFAULT)
			ENDIF
		ENDIF
	ENDIF
	
	// Unlock Doors //
	IF sConsequences.iInteractable_DoorsToUnlockBS != 0
		INT iDoor
		FOR iDoor = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfDoors - 1
			IF IS_BIT_SET(sConsequences.iInteractable_DoorsToUnlockBS, iDoor)
				UNSET_DOOR_AS_UNLOCKED_LOCAL(iDoor)
			ENDIF
		ENDFOR
	ENDIF
	
	// Lock Doors //
	IF sConsequences.iInteractable_DoorsToLockBS != 0
		INT iDoor
		FOR iDoor = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfDoors - 1
			IF IS_BIT_SET(sConsequences.iInteractable_DoorsToLockBS, iDoor)
				UNSET_DOOR_AS_LOCKED_LOCAL(iDoor)
			ENDIF
		ENDFOR
	ENDIF
	
	// Doors To Switch Config //
	IF sConsequences.iInteractable_DoorsToSwitchConfigBS != 0
		INT iDoor
		FOR iDoor = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfDoors - 1
			IF IS_BIT_SET(sConsequences.iInteractable_DoorsToSwitchConfigBS, iDoor)
				UNSET_DOOR_TO_USE_ALT_CONFIG_LOCAL(iDoor)
			ENDIF
		ENDFOR
	ENDIF
	
	// Kill Interactable //
	IF IS_BIT_SET(sConsequences.iInteractable_ConsequencesBS, ciInteractableConsequences_KillInteractable)
		IF DOES_ENTITY_EXIST(oiInteractable)
		AND NETWORK_HAS_CONTROL_OF_ENTITY(oiInteractable)
			PRINTLN("[Interactables][Interactable ", iInteractable, "] PROCESS_REVERSING_CLIENT_INTERACTABLE_ONESHOT_CONSEQUENCES | Setting Interactable health to max due to ", ciInteractableConsequences_KillInteractable)
			SET_ENTITY_HEALTH(oiInteractable, GET_ENTITY_MAX_HEALTH(oiInteractable))
			FIX_OBJECT_FRAGMENT(oiInteractable)
		ENDIF
	ENDIF
	
	// Give Cash //
	// No reversal hooked up for Cash
	
ENDPROC
PROC PROCESS_INTERACTABLE_COMPLETED(INT iInteractable, FMMC_INTERACTABLE_CONSEQUENCES& sConsequences, INT iParticipant, BOOL bCompletedByContinuity = FALSE)

	PRINTLN("[Interactables][Interactable ", iInteractable, "] PROCESS_FMMC_INTERACTABLE_EVENT | Interactable ", iInteractable, " has been completed by participant ", iParticipant)
	
	IF iParticipant = iLocalPart
		FMMC_SET_LONG_BIT(MC_playerBD[iParticipant].iInteractablesLocallyCompletedBS, iInteractable)
	ENDIF
	SET_BIT(iInteractable_CompletedInteractionTypeBS, g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iInteractable_InteractionType)
	
	IF NOT IS_INTERACTABLE_READY_TO_BE_COMPLETED(iInteractable, iParticipant)
		// Not all requirements have been reached!
		PRINTLN("[Interactables][Interactable ", iInteractable, "] PROCESS_FMMC_INTERACTABLE_EVENT | Interactable ", iInteractable, " isn't ready to be completed")
		EXIT
	ENDIF
	
	IF bIsLocalPlayerHost
		IF IS_INTERACTABLE_COMPLETE(iInteractable)
			// Already processed the interaction's completion
			EXIT
		ENDIF
		
		FMMC_SET_LONG_BIT(MC_serverBD_4.sIntServerData.iInteractable_CompletedBS, iInteractable)
		
		PROCESS_HOST_INTERACTABLE_ONESHOT_CONSEQUENCES(iInteractable, sConsequences, iParticipant)
	
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iSpawnGroupAdditionalFunctionalityBS, ciSPAWN_GROUP_ADDITIONAL_ENTITY_FUNCTIONALITY_EVENT_IS_COMPLETION)
			PRINTLN("[Interactables][SpawnGroups] PROCESS_INTERACTABLE_COMPLETED - INTERACTABLE: ", iInteractable, " flagged as complete with spawn groups to be modified on this event.")
			MODIFY_SPAWN_GROUP_DURING_GAMEPLAY_FROM_BITSET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iSpawnGroupAdditionalFunctionalityBS, g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iModifySpawnGroupOnEvent, g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iModifySpawnSubGroupOnEventBS)
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_InteractableTracking)
		AND g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iContinuityId > -1
		AND NOT FMMC_IS_LONG_BIT_SET(MC_serverBD_1.sMissionContinuityVars.iInteractablesCompleteBitset, g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iContinuityId)
			PRINTLN("[CONTINUITY][Interactables][Interactable ", iInteractable, "] PROCESS_INTERACTABLE_COMPLETED | Marking as complete.")
			FMMC_SET_LONG_BIT(MC_serverBD_1.sMissionContinuityVars.iInteractablesCompleteBitset, g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iContinuityId)
		ENDIF
	ENDIF
	
	PROCESS_CLIENT_INTERACTABLE_ONESHOT_CONSEQUENCES(iInteractable, sConsequences, iParticipant, bCompletedByContinuity)
	
ENDPROC

PROC PROCESS_INTERACTABLE_NO_LONGER_COMPLETED(INT iInteractable, FMMC_INTERACTABLE_CONSEQUENCES& sConsequences, INT iParticipant)

	PRINTLN("[Interactables][Interactable ", iInteractable, "] PROCESS_FMMC_INTERACTABLE_EVENT | Interaction is no longer completed!")
	
	IF iParticipant = iLocalPart
		FMMC_CLEAR_LONG_BIT(MC_playerBD[iParticipant].iInteractablesLocallyCompletedBS, iInteractable)
	ENDIF
	CLEAR_BIT(iInteractable_CompletedInteractionTypeBS, g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iInteractable_InteractionType)
	
	IF bIsLocalPlayerHost
		IF NOT IS_INTERACTABLE_COMPLETE(iInteractable, DEFAULT, FALSE)
			// This interaction wasn't completed in the first place
			EXIT
		ENDIF
		FMMC_CLEAR_LONG_BIT(MC_serverBD_4.sIntServerData.iInteractable_CompletedBS, iInteractable)
		
		PROCESS_HOST_REVERSING_INTERACTABLE_ONESHOT_CONSEQUENCES(iInteractable, sConsequences)
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_InteractableTracking)
		AND g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iContinuityId > -1
		AND FMMC_IS_LONG_BIT_SET(MC_serverBD_1.sMissionContinuityVars.iInteractablesCompleteBitset, g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iContinuityId)
			PRINTLN("[CONTINUITY][Interactables][Interactable ", iInteractable, "] PROCESS_INTERACTABLE_NO_LONGER_COMPLETED | Clearing marked as complete.")
			FMMC_CLEAR_LONG_BIT(MC_serverBD_1.sMissionContinuityVars.iInteractablesCompleteBitset, g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iContinuityId)
		ENDIF
		
	ENDIF
	
	PROCESS_REVERSING_CLIENT_INTERACTABLE_ONESHOT_CONSEQUENCES(iInteractable, sConsequences, iParticipant)
	
ENDPROC

PROC PROCESS_INTERACTABLE_SYNCLOCK_ENGAGED_EVENT(SCRIPT_EVENT_DATA_FMMC_INTERACTABLE_EVENT& Event)
	IF IS_SYNC_LOCK_PANEL_RELEVANT_TO_LOCAL_PLAYER(Event.iInteractable)
		IF NOT HAS_NET_TIMER_STARTED(stSyncLockEngageTimer)
			PRINTLN("[Interactables][Interactable ", Event.iInteractable, "][Interactable ", GET_FOREGROUND_INTERACTABLE_INDEX(), "][SyncLock] PROCESS_INTERACTABLE_SYNCLOCK_ENGAGED_EVENT | Starting stSyncLockEngageTimer")
			REINIT_NET_TIMER(stSyncLockEngageTimer)
		ENDIF
		
		INT iMySyncLockInteraction = GET_ONGOING_INTERACTION_INDEX(GET_FOREGROUND_INTERACTABLE_INDEX())
		IF iMySyncLockInteraction > -1
			IF sOngoingInteractionVars[iMySyncLockInteraction].iInteractable_LoopingSoundID = -1
				sOngoingInteractionVars[iMySyncLockInteraction].iInteractable_LoopingSoundID = GET_SOUND_ID()
				PRINTLN("[SyncLock] PROCESS_INTERACTABLE_SYNCLOCK_ENGAGED_EVENT | Starting looping sound!")
				PLAY_SOUND_FROM_ENTITY(sOngoingInteractionVars[iMySyncLockInteraction].iInteractable_LoopingSoundID, "Keycard_Wait_Loop", GET_FOREGROUND_INTERACTION_INTERACTABLE_OBJECT(), "Twin_Card_Entry_Sounds")
			ELSE
				STOP_INTERACTABLE_LOOPING_SOUND(sOngoingInteractionVars[iMySyncLockInteraction])
			ENDIF
		ENDIF
	ENDIF
	
	FMMC_SET_LONG_BIT(iInteractable_SyncLockEngagedBS, Event.iInteractable)
	PRINTLN("[Interactables][Interactable ", Event.iInteractable, "][Interactable ", GET_FOREGROUND_INTERACTABLE_INDEX(), "][SyncLock] PROCESS_INTERACTABLE_SYNCLOCK_ENGAGED_EVENT | Setting ", Event.iInteractable, " in iInteractable_SyncLockEngagedBS")
ENDPROC
PROC PROCESS_INTERACTABLE_SYNCLOCK_DISENGAGED_EVENT(SCRIPT_EVENT_DATA_FMMC_INTERACTABLE_EVENT& Event)
	IF IS_SYNC_LOCK_PANEL_RELEVANT_TO_LOCAL_PLAYER(Event.iInteractable)
		IF HAS_NET_TIMER_STARTED(stSyncLockEngageTimer)
		AND IS_BIT_SET(Event.iInteractableEventBS, ciINTERACTABLE_EVENT_BS__RESET_SYNCLOCK_TIMER)
			PRINTLN("[Interactables][Interactable ", Event.iInteractable, "][Interactable ", GET_FOREGROUND_INTERACTABLE_INDEX(), "][SyncLock] PROCESS_INTERACTABLE_SYNCLOCK_DISENGAGED_EVENT | Resetting stSyncLockEngageTimer")
			RESET_NET_TIMER(stSyncLockEngageTimer)
		ENDIF
	ENDIF
	
	FMMC_CLEAR_LONG_BIT(iInteractable_SyncLockEngagedBS, Event.iInteractable)
	PRINTLN("[Interactables][Interactable ", Event.iInteractable, "][Interactable ", GET_FOREGROUND_INTERACTABLE_INDEX(), "][SyncLock] PROCESS_INTERACTABLE_SYNCLOCK_DISENGAGED_EVENT | Clearing ", Event.iInteractable, " in iInteractable_SyncLockEngagedBS")
	
ENDPROC

PROC PROCESS_FMMC_INTERACTABLE_EVENT(INT iEventIndex)
	
	SCRIPT_EVENT_DATA_FMMC_INTERACTABLE_EVENT Event
	
	IF NOT GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventIndex, Event, SIZE_OF(Event))
	OR Event.Details.Type != SCRIPT_EVENT_FMMC_INTERACTABLE
		EXIT
	ENDIF
	
	SWITCH Event.eInteractableEventType
		CASE FMMC_INTERACTABLE_EVENT_TYPE__PLAYER_STARTED_INTERACTING
			IF bIsLocalPlayerHost
				MC_serverBD_4.sIntServerData.iInteractable_InteractingParticipant[Event.iInteractable] = Event.iEventSenderParticipant
			ENDIF
			
			IF Event.iEventSenderParticipant != iLocalPart
				REINIT_NET_TIMER(stInteractableUsageCooldownTimer[Event.iInteractable])
			ENDIF
			PRINTLN("[Interactables][Interactable ", Event.iInteractable, "] PROCESS_FMMC_INTERACTABLE_EVENT | Participant ", Event.iEventSenderParticipant, " is now interacting with Interactable ", Event.iInteractable)
		BREAK
		
		CASE FMMC_INTERACTABLE_EVENT_TYPE__PLAYER_STOPPED_INTERACTING
			IF bIsLocalPlayerHost
				IF GET_INTERACTABLE_CURRENT_USER(Event.iInteractable) = Event.iEventSenderParticipant
					MC_serverBD_4.sIntServerData.iInteractable_InteractingParticipant[Event.iInteractable] = -1
				ENDIF
			ENDIF
			
			REINIT_NET_TIMER(stInteractableUsageCooldownTimer[Event.iInteractable])
			PRINTLN("[Interactables][Interactable ", Event.iInteractable, "] PROCESS_FMMC_INTERACTABLE_EVENT | Participant ", Event.iEventSenderParticipant, " is no longer interacting with Interactable ", Event.iInteractable)
		BREAK
		
		CASE FMMC_INTERACTABLE_EVENT_TYPE__INTERACTION_COMPLETE
			PROCESS_INTERACTABLE_COMPLETED(Event.iInteractable, g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[Event.iInteractable].sConsequences, Event.iEventSenderParticipant)
		BREAK
		
		CASE FMMC_INTERACTABLE_EVENT_TYPE__INTERACTION_COMPLETED_ON_PREVIOUS_MISSION
			PROCESS_INTERACTABLE_COMPLETED(Event.iInteractable, g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[Event.iInteractable].sConsequences, Event.iEventSenderParticipant, TRUE)
		BREAK
		
		CASE FMMC_INTERACTABLE_EVENT_TYPE__INTERACTION_NO_LONGER_COMPLETE
			PROCESS_INTERACTABLE_NO_LONGER_COMPLETED(Event.iInteractable, g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[Event.iInteractable].sConsequences, Event.iEventSenderParticipant)
		BREAK
		
		CASE FMMC_INTERACTABLE_EVENT_TYPE__RESET_INTERACTABLE
			PRINTLN("[Interactables][Interactable ", Event.iInteractable, "] PROCESS_FMMC_INTERACTABLE_EVENT | Received FMMC_INTERACTABLE_EVENT_TYPE__RESET_INTERACTABLE event from participant ", Event.iEventSenderParticipant)
			RESET_INTERACTABLE(Event.iInteractable)
		BREAK
		
		// Interaction-Specific Events
		CASE FMMC_INTERACTABLE_EVENT_TYPE__SYNCLOCK_ENGAGED
			PROCESS_INTERACTABLE_SYNCLOCK_ENGAGED_EVENT(Event)
		BREAK
		CASE FMMC_INTERACTABLE_EVENT_TYPE__SYNCLOCK_DISENGAGED
			PROCESS_INTERACTABLE_SYNCLOCK_DISENGAGED_EVENT(Event)
		BREAK
		
	ENDSWITCH
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Objects	 -------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Processes for object functionality  			--------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
PROC PROCESS_FMMC_CCTV_EVENT(INT iCount)
	SCRIPT_EVENT_DATA_FMMC_CCTV_EVENT Event
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, Event, SIZE_OF(Event))
	AND Event.Details.Type = SCRIPT_EVENT_FMMC_CCTV_EVENT
		
		INT iPartTriggered = -1
		IF Event.PlayerSpotted != INVALID_PLAYER_INDEX()
			iPartTriggered = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(Event.PlayerSpotted))
		ENDIF
				
		BOOL bShouldShowSpottedHelptext = NOT HAS_ANY_TEAM_TRIGGERED_AGGRO(Event.iAggroBS)
		
		SWITCH Event.eEventType
			CASE CCTV_EVENT_TYPE__SPOTTED_PLAYER
				
				// Camera spotted a player!
				PRINTLN("[RCC MISSION][CCTV][CAM ", Event.iCam, "] CCTV_EVENT_TYPE__SPOTTED_PLAYER | iPlayerPart = ", NATIVE_TO_INT(Event.PlayerSpotted), " iCam: ", Event.iCam)
				SET_CCTV_CAMERA_SPOTTED_PLAYER(Event.iCam)
				
				IF Event.PlayerSpotted != INVALID_PLAYER_INDEX()
					iCCTVCameraSpottedPlayer[Event.iCam] = iPartTriggered
				ENDIF
				
				IF bIsLocalPlayerHost
					INT iPlayerTeam
					iPlayerTeam = GET_PLAYER_TEAM(Event.PlayerSpotted)
				
					PRINTLN("[RCC MISSION][CCTV] CCTV_EVENT_TYPE__SPOTTED_PLAYER | Setting SET_CCTV_CAMERAS_WITH_AGGRO_BS_BEEN_ALERTED_BY_PLAYER ... ", GET_PLAYER_NAME(Event.PlayerSpotted), " was spotted by a camera! Their team is: ", iPlayerTeam)
					
					SET_CCTV_CAMERAS_WITH_AGGRO_BS_BEEN_ALERTED_BY_PLAYER(iPlayerTeam, Event.iAggroBS)
					
					IF iPlayerTeam > -1
					AND iPlayerTeam < FMMC_MAX_TEAMS
					
						INT iPlayerPart
						iPlayerPart = NATIVE_TO_INT(Event.PlayerSpotted)
						PRINTLN("[RCC MISSION][CCTV] CCTV_EVENT_TYPE__SPOTTED_PLAYER | iPlayerPart = ", iPlayerPart)
						
						IF iPlayerPart > -1
							PRINTLN("[RCC MISSION][CCTV] CCTV_EVENT_TYPE__SPOTTED_PLAYER | Setting MC_serverBD.iPartCausingFail[", iPlayerTeam, "] to ", iPlayerPart)
							MC_serverBD.iPartCausingFail[iPlayerTeam] = iPlayerPart
						ENDIF
						
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFive, ciOptionsBS25_TriggerCCTVAggroWithoutPeds)
							SET_TEAM_AS_TRIGGERED_AGGRO_FROM_BITSET(iPlayerTeam, Event.iAggroBS)
							PRINTLN("[RCC MISSION][CCTV] CCTV_EVENT_TYPE__SPOTTED_PLAYER | Setting SET_TEAM_AS_TRIGGERED_AGGRO_FROM_BITSET Player Team", iPlayerTeam)
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE CCTV_EVENT_TYPE__SPOTTED_PED_BODY
				// Camera spotted a ped body!
				PRINTLN("[RCC MISSION][CCTV][CAM ", Event.iCam, "] CCTV_EVENT_TYPE__SPOTTED_PED_BODY | Ped Body = ", Event.iPedBodySpotted, " iCam: ", Event.iCam)
				SET_CCTV_CAMERA_SPOTTED_BODY(Event.iCam)
				
				IF bIsLocalPlayerHost
					PRINTLN("[RCC MISSION][CCTV] CCTV_EVENT_TYPE__SPOTTED_PED_BODY | Setting SET_CCTV_CAMERAS_WITH_AGGRO_BS_SPOTTED_DEAD_BODY...")
					
					INT iPlayerTeam
					
					IF Event.PlayerSpotted != NULL
					AND NETWORK_IS_PLAYER_ACTIVE(Event.PlayerSpotted)					
						
						iPlayerTeam = GET_PLAYER_TEAM(Event.PlayerSpotted)
						
						// Aggro needs a team, use default mission team.
						IF iPlayerTeam = -1
							iPlayerTeam = 0
						ENDIF
					
						SET_CCTV_CAMERAS_WITH_AGGRO_BS_SPOTTED_DEAD_BODY(iPlayerTeam, Event.iAggroBS)	
						
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFive, ciOptionsBS25_TriggerCCTVAggroWithoutPeds)
							SET_TEAM_AS_TRIGGERED_AGGRO_FROM_BITSET(iPlayerTeam, Event.iAggroBS)
							PRINTLN("[RCC MISSION][CCTV] CCTV_EVENT_TYPE__SPOTTED_PED_BODY | From Player ", GET_PLAYER_NAME(Event.PlayerSpotted), " | Setting SET_TEAM_AS_TRIGGERED_AGGRO_FROM_BITSET Player Team", iPlayerTeam)
						ENDIF
						
					ELSE
					
						FOR iPlayerTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
							SET_CCTV_CAMERAS_WITH_AGGRO_BS_SPOTTED_DEAD_BODY(iPlayerTeam, Event.iAggroBS)
							
							IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFive, ciOptionsBS25_TriggerCCTVAggroWithoutPeds)
								SET_TEAM_AS_TRIGGERED_AGGRO_FROM_BITSET(iPlayerTeam, Event.iAggroBS)
								PRINTLN("[RCC MISSION][CCTV] CCTV_EVENT_TYPE__SPOTTED_PED_BODY | All Teams | Setting SET_TEAM_AS_TRIGGERED_AGGRO_FROM_BITSET Player Team", iPlayerTeam)
							ENDIF
						ENDFOR
						
					ENDIF
				ENDIF
			BREAK
			
			CASE CCTV_EVENT_TYPE__REPORTED_BY_PED
				SET_CCTV_CAMERA_REPORTED_BY_PED(Event.iCam)
				
				IF bIsLocalPlayerHost
					PRINTLN("[RCC MISSION][CCTV] CCTV_EVENT_TYPE__REPORTED_BY_PED | ...")
					INT iPlayerTeam
					iPlayerTeam = GET_PLAYER_TEAM(Event.PlayerSpotted)
					
					// Aggro needs a team, use default mission team.
					IF iPlayerTeam = -1
						iPlayerTeam = 0
					ENDIF
					
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFive, ciOptionsBS25_TriggerCCTVAggroWithoutPeds)
						SET_TEAM_AS_TRIGGERED_AGGRO_FROM_BITSET(iPlayerTeam, Event.iAggroBS)
						PRINTLN("[RCC MISSION][CCTV] CCTV_EVENT_TYPE__REPORTED_BY_PED | Setting SET_TEAM_AS_TRIGGERED_AGGRO_FROM_BITSET Player Team", iPlayerTeam)
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
		
		IF HAS_CCTV_CAMERA_SPOTTED_SOMETHING(Event.iCam)
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[GET_LOCAL_PLAYER_TEAM(TRUE)].iRuleBitsetThirteen[GET_LOCAL_PLAYER_CURRENT_RULE(TRUE)], ciBS_RULE13_RULE_SHOW_PLAYER_SPOTTED_HELPTEXT)
				IF bShouldShowSpottedHelptext
					DISPLAY_CCTV_SPOTTED_HELP_TEXT(Event.iCam, Event.iAggroBS)
				ELSE
					PRINTLN("[ML MISSION][CCTV] CCTV_EVENT_TYPE__SPOTTED_PLAYER | Spotted helptext was blocked as the player had already triggered aggro!")
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_TriggerAllCCTVCameras)
				PRINTLN("[ML MISSION][CCTV] CCTV_EVENT_TYPE__SPOTTED_PLAYER | Triggering all CCTV cameras, triggered by participant ", iPartTriggered)
				TRIGGER_ALL_CCTV_CAMERAS(iPartTriggered)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_CCTV_REACHED_DESTINATION(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_CCTV_REACHED_DESTINATION EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_CCTV_REACHED_DESTINATION
			IF NOT IS_BIT_SET(iCCTVReachedDestinationBS, EventData.iCCTVIndex)
				SET_BIT(iCCTVReachedDestinationBS, EventData.iCCTVIndex)
				PRINTLN("[CCTV][CAM ", EventData.iCCTVIndex, "][RCC MISSION] - iCam: ", EventData.iCCTVIndex, " PROCESS_SCRIPT_EVENT_FMMC_CCTV_REACHED_DESTINATION | Setting iCCTVReachedDestinationBS")
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_UPDATE_DOOR_LINKED_ENTITIES(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_UPDATE_DOOR_LINKED_OBJS EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_UPDATE_DOOR_LINKED_ENTITIES
			iDoorHasLinkedEntityBS = EventData.iDoorHasLinkedEntityBS
			PRINTLN("[Doors] PROCESS_SCRIPT_EVENT_FMMC_UPDATE_DOOR_LINKED_ENTITIES | iDoorHasLinkedEntityBS is now ", iDoorHasLinkedEntityBS)
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_OBJECT_CARRIER_ALERT(INT iEventID)
	
	SCRIPT_EVENT_DATA_FMMC_OBJECT_CARRIER_ALERT EventData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
	AND EventData.Details.Type = SCRIPT_EVENT_FMMC_OBJECT_CARRIER_ALERT
		
		IF EventData.piCarrier = LocalPlayer
			PRINT_TICKER(GET_OBJECT_CARRIER_TICKER_TEXT(TRUE))
		ELSE
			PRINT_TICKER_WITH_PLAYER_NAME(GET_OBJECT_CARRIER_TICKER_TEXT(FALSE), EventData.piCarrier)
		ENDIF
		
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Peds	 -----------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Processes for ped functionality  			------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_FMMC_PED_GIVEN_GUNONRULE(INT iCount)
	
	IF bIsLocalPlayerHost
		
		SCRIPT_EVENT_DATA_FMMC_PED_ID ObjectiveData
		IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))
			IF ObjectiveData.Details.Type = SCRIPT_EVENT_FMMC_PED_GIVEN_GUNONRULE
				PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_GIVEN_GUNONRULE - ObjectiveData.iPedID = ",ObjectiveData.iPedid)
				FMMC_SET_LONG_BIT(MC_serverBD.iPedGunOnRuleGiven, ObjectiveData.iPedid)
			ENDIF
		ENDIF

	ENDIF
	
ENDPROC

PROC PROCESS_FMMC_PED_COMBAT_STYLE_CHANGED(INT iCount)
	
	IF bIsLocalPlayerHost
		SCRIPT_EVENT_DATA_FMMC_PED_ID ObjectiveData
		IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))
			IF ObjectiveData.Details.Type = SCRIPT_EVENT_FMMC_PED_COMBAT_STYLE_CHANGED
				PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_COMBAT_STYLE_CHANGED - ObjectiveData.iPedID = ",ObjectiveData.iPedid)
				FMMC_SET_LONG_BIT(MC_serverBD.iPedCombatStyleChanged, ObjectiveData.iPedid)
			ENDIF
		ENDIF

	ENDIF
	
ENDPROC

PROC PROCESS_FMMC_PED_WAITING_AFTER_GOTO(INT iCount)
	
	SCRIPT_EVENT_DATA_FMMC_PED_ID ObjectiveData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))
		IF ObjectiveData.Details.Type = SCRIPT_EVENT_FMMC_PED_WAITING_AFTER_GOTO
			PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_WAITING_AFTER_GOTO - ObjectiveData.ipedid = ",ObjectiveData.ipedid)
			FMMC_SET_LONG_BIT(iPedGotoWaitBitset, ObjectiveData.ipedid)
			START_NET_TIMER(tdAssociatedGOTOWaitIdleTimer[ObjectiveData.ipedid])
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_FMMC_PED_DONE_WAITING_AFTER_GOTO(INT iCount)
	
	SCRIPT_EVENT_DATA_FMMC_PED_ID ObjectiveData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))
		IF ObjectiveData.Details.Type = SCRIPT_EVENT_FMMC_PED_DONE_WAITING_AFTER_GOTO
			PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_DONE_WAITING_AFTER_GOTO - ObjectiveData.ipedid = ",ObjectiveData.ipedid)
			FMMC_SET_LONG_BIT(iPedGotoAssCompletedWaitBitset, ObjectiveData.ipedid)
			RESET_NET_TIMER(tdAssociatedGOTOWaitIdleTimer[ObjectiveData.ipedid])
			FMMC_CLEAR_LONG_BIT(iPedGotoAssCompletedWaitPlayerVehExitBitset, ObjectiveData.ipedid)
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_FMMC_PED_CLEAR_GOTO_WAITING_STATE(INT iCount)
	
	SCRIPT_EVENT_DATA_FMMC_PED_ID ObjectiveData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))
		IF ObjectiveData.Details.Type = SCRIPT_EVENT_FMMC_PED_CLEAR_GOTO_WAITING_STATE
			
			INT iped = ObjectiveData.ipedid
			
			PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_CLEAR_GOTO_WAITING_STATE - ObjectiveData.ipedid = ",iped)
			CLEAR_PED_ASS_GOTO_DATA(iPed)
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_FMMC_PED_DEFENSIVE_AREA_GOTO_CLEANED_UP(INT iCount)
	
	IF bIsLocalPlayerHost
	
		SCRIPT_EVENT_DATA_FMMC_PED_ID ObjectiveData
		
		IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))
			IF ObjectiveData.Details.Type = SCRIPT_EVENT_FMMC_PED_DEFENSIVE_AREA_GOTO_CLEANED_UP
				
				INT iped = ObjectiveData.ipedid
				
				PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_DEFENSIVE_AREA_GOTO_CLEANED_UP - ObjectiveData.ipedid = ",iped)
				FMMC_CLEAR_LONG_BIT(MC_serverBD.iPedDoneDefensiveAreaGoto, iPed)
			ENDIF
		ENDIF
		
	ENDIF
	
ENDPROC

PROC PROCESS_FMMC_PED_DIRTY_FLAG_SET(INT iCount)
	
	SCRIPT_EVENT_DATA_FMMC_PED_ID ObjectiveData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))

		IF ObjectiveData.Details.Type = SCRIPT_EVENT_FMMC_PED_DIRTY_FLAG_SET
			
			PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_DIRTY_FLAG_SET - ObjectiveData.ipedid = ",ObjectiveData.ipedid)
			FMMC_SET_LONG_BIT(sRetaskDirtyFlagData.iDirtyFlagPedNeedsRetask, ObjectiveData.ipedid)
			
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_FMMC_ANIM_STARTED_EVENT(int icount)

	SCRIPT_EVENT_DATA_FMMC_PED_ANIM_STARTED ObjectiveData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))
		
		IF ObjectiveData.Details.Type = SCRIPT_EVENT_FMMC_ANIM_STARTED
			
			#IF IS_DEBUG_BUILD
			IF ObjectiveData.Details.FromPlayerIndex != INVALID_PLAYER_INDEX()
			AND IS_NET_PLAYER_OK(ObjectiveData.Details.FromPlayerIndex, FALSE)
				INT iPartSender = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(ObjectiveData.Details.FromPlayerIndex))			
				PRINTLN("[RCC MISSION] PROCESS_FMMC_ANIM_STARTED_EVENT iPartSender: ", iPartSender, " ObjectiveData.ipedid = ",ObjectiveData.ipedid," - special ",IS_BIT_SET(ObjectiveData.iAnimStarted, 0),", breakout ",IS_BIT_SET(ObjectiveData.iAnimStarted, 1))
			ENDIF
			#ENDIF
			
			FMMC_SET_LONG_BIT(ipedAnimBitset, ObjectiveData.ipedid)
			
			IF IS_BIT_SET(ObjectiveData.iAnimStarted, 0) // Special anim
				FMMC_SET_LONG_BIT(iPedPerformingSpecialAnimBS, ObjectiveData.ipedid)
			ENDIF
			
			IF IS_BIT_SET(ObjectiveData.iAnimStarted, 1) // Breakout anim
				FMMC_SET_LONG_BIT(g_iFMMCPedAnimBreakoutBS, ObjectiveData.ipedid)
			ENDIF
			
		ENDIF
	ENDIF

ENDPROC

PROC PROCESS_FMMC_ANIM_STOPPED_EVENT(INT iCount)
	
	SCRIPT_EVENT_DATA_FMMC_PED_ANIM_STARTED ObjectiveData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))
		
		IF ObjectiveData.Details.Type = SCRIPT_EVENT_FMMC_ANIM_STOPPED
			
			PRINTLN("[RCC MISSION] PROCESS_FMMC_ANIM_STOPPED_EVENT ObjectiveData.ipedid = ",ObjectiveData.ipedid," - special ",IS_BIT_SET(ObjectiveData.iAnimStarted, 0),", breakout ",IS_BIT_SET(ObjectiveData.iAnimStarted, 1))
			
			FMMC_CLEAR_LONG_BIT(ipedAnimBitset, ObjectiveData.ipedid)
			
			IF IS_BIT_SET(ObjectiveData.iAnimStarted, 0) // Special anim
				FMMC_CLEAR_LONG_BIT(iPedPerformingSpecialAnimBS, ObjectiveData.ipedid)
			ENDIF
			
			IF IS_BIT_SET(ObjectiveData.iAnimStarted, 1) // Breakout anim
				FMMC_CLEAR_LONG_BIT(g_iFMMCPedAnimBreakoutBS, ObjectiveData.ipedid)
			ENDIF
			
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_FMMC_SECONDARY_ANIM_CLEANUP_EVENT(INT iEventID)
	
	SCRIPT_EVENT_DATA_FMMC_SECONDARY_ANIM_CLEANUP Event
	PRINTLN("[RCC MISSION] PROCESS_FMMC_SECONDARY_ANIM_CLEANUP_EVENT Called.")

	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))
	
		IF Event.bRunCleanup = TRUE
			FMMC_SET_LONG_BIT(iPedSecondaryAnimCleanupBitset, Event.iPedNumber)
			PRINTLN("[RCC MISSION] PROCESS_FMMC_SECONDARY_ANIM_CLEANUP_EVENT Setting bit iPedSecondaryAnimCleanupBitset.")
		ELSE
			FMMC_CLEAR_LONG_BIT(iPedSecondaryAnimCleanupBitset, Event.iPedNumber)
			PRINTLN("[RCC MISSION] PROCESS_FMMC_SECONDARY_ANIM_CLEANUP_EVENT Clearing bit iPedSecondaryAnimCleanupBitset.")
		ENDIF

	ENDIF

ENDPROC

PROC PROCESS_FMMC_REMOVE_PED_FROM_GROUP(INT iCount)
	
	SCRIPT_EVENT_DATA_FMMC_PED_ID ObjectiveData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))
		IF ObjectiveData.Details.Type = SCRIPT_EVENT_FMMC_REMOVE_PED_FROM_GROUP
			
			INT iped = ObjectiveData.iPedid
			PRINTLN("[RCC MISSION] PROCESS_FMMC_REMOVE_PED_FROM_GROUP - ObjectiveData.ipedid = ",iped)
			
			IF NOT IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[iped])
			AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niPed[iped])
				PRINTLN("[RCC MISSION] PROCESS_FMMC_REMOVE_PED_FROM_GROUP - Remote remove ped from group, ped ",iped)
				
				REMOVE_FMMC_PED_FROM_GROUP(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iped]), iped)
				
				IF ObjectiveData.Details.FromPlayerIndex != LocalPlayer // Unlikely that control will have migrated back, but worth a check
					BROADCAST_FMMC_PED_SUCCESSFULLY_REMOVED_FROM_GROUP(iped, ObjectiveData.Details.FromPlayerIndex)
				ELSE
					FMMC_CLEAR_LONG_BIT(MC_playerBD[iPartToUse].iPedFollowingBitset, iped)
				ENDIF
				
			ENDIF
			
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_FMMC_PED_SUCCESSFULLY_REMOVED_FROM_GROUP(INT iCount)
	
	SCRIPT_EVENT_DATA_FMMC_PED_ID ObjectiveData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))
		IF ObjectiveData.Details.Type = SCRIPT_EVENT_FMMC_PED_SUCCESSFULLY_REMOVED_FROM_GROUP
			PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_SUCCESSFULLY_REMOVED_FROM_GROUP - ObjectiveData.ipedid = ",ObjectiveData.iPedid)
			FMMC_CLEAR_LONG_BIT(MC_playerBD[iPartToUse].iPedFollowingBitset, ObjectiveData.iPedid)
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_FMMC_PED_HEARING(INT iCount)
	
	SCRIPT_EVENT_DATA_FMMC_PED_ID ObjectiveData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))
		IF ObjectiveData.Details.Type = SCRIPT_EVENT_FMMC_PED_HEARING
			PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_HEARING - ObjectiveData.ipedid = ",ObjectiveData.ipedid)
			
			#IF IS_DEBUG_BUILD
			IF IS_NET_PLAYER_OK(ObjectiveData.Details.FromPlayerIndex, FALSE, FALSE)
				PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_HEARING - Sent by part ",NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(ObjectiveData.Details.FromPlayerIndex)))
			ENDIF
			#ENDIF
			
			FMMC_SET_LONG_BIT(iPedHeardBitset, ObjectiveData.ipedid)
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_FMMC_PED_SPOOKED(INT iCount)
	
	SCRIPT_EVENT_DATA_FMMC_PED_SPOOKED ObjectiveData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))
		IF ObjectiveData.Details.Type = SCRIPT_EVENT_FMMC_PED_SPOOKED
			INT iped = ObjectiveData.iPed
			PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_SPOOKED - ObjectiveData.ipedid = ",iped)
			
			#IF IS_DEBUG_BUILD
			IF IS_NET_PLAYER_OK(ObjectiveData.Details.FromPlayerIndex, FALSE, FALSE)
				PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_SPOOKED - Sent by part ",NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(ObjectiveData.Details.FromPlayerIndex)))
				PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_SPOOKED - Sent by player: ", GET_PLAYER_NAME(ObjectiveData.Details.FromPlayerIndex))
			ENDIF
			#ENDIF
			
			CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] PROCESS_FMMC_PED_SPOOKED - ObjectiveData.iSpookedReason = ", ObjectiveData.iSpookedReason)
			CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] PROCESS_FMMC_PED_SPOOKED - ObjectiveData.iPartCausingFail = ", ObjectiveData.iPartCausingFail)
			CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] PROCESS_FMMC_PED_SPOOKED - MC_serverBD_2.iPedState[iped] = ", MC_serverBD_2.iPedState[iped])
			CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] PROCESS_FMMC_PED_SPOOKED - g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iFlee = ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iFlee)
			CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION] PROCESS_FMMC_PED_SPOOKED - g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iIdleAnim = ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iIdleAnim)
			
			#IF IS_DEBUG_BUILD
				IF ObjectiveData.iPartCausingFail != -1
					PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_SPOOKED - Player causing fail: ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(ObjectiveData.iPartCausingFail)))
				ENDIF
			#ENDIF
			
			#IF IS_DEBUG_BUILD 
			IF NOT FMMC_IS_LONG_BIT_SET(iPedLocalSpookedBitset, iPed)
				SET_PED_IS_SPOOKED_DEBUG(iPed, DEFAULT, ObjectiveData.iSpookedReason)
			ENDIF
			#ENDIF
			
			FMMC_SET_LONG_BIT(iPedLocalSpookedBitset, iPed)
			
			FMMC_PED_STATE sPedState
			FILL_FMMC_PED_STATE_STRUCT(sPedState, iPed)
			
			IF bIsLocalPlayerHost
			AND NOT sPedState.bInjured				
				IF MC_serverBD_2.iPedState[iped] != ciTASK_FLEE
					IF NOT DOES_PED_HAVE_VALID_GOTO(iped)
						IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iFlee = ciPED_FLEE_ON
							IF MC_serverBD_2.iPartPedFollows[iped] =-1
								IF SHOULD_PED_BE_GIVEN_TASK(sPedState.pedIndex, iped, SCRIPT_TASK_PLAY_ANIM, TRUE)
								AND SHOULD_PED_BE_GIVEN_TASK(sPedState.pedIndex, iped, SCRIPT_TASK_SYNCHRONIZED_SCENE, TRUE)
								OR NOT DOES_ANIMATION_NEED_SPECIAL_BREAKOUT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iIdleAnim)
									SET_PED_STATE(iped, ciTASK_FLEE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//Aggro:
				IF ObjectiveData.iSpookedReason != ciSPOOK_NONE
					IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFour,ciPED_BSFour_StayInTasks_On_Aggro)
						IF ObjectiveData.iSpookedReason = ciSPOOK_PLAYER_TOUCHING_ME
						OR ObjectiveData.iSpookedReason = ciSPOOK_PLAYER_HEARD
							TRIGGER_SERVER_AGGRO_HUNTING_ON_PED(sPedState, ObjectiveData.iPartCausingFail)
						ELIF ObjectiveData.iSpookedReason = ciSPOOK_SEEN_BODY
							TRIGGER_SERVER_AGGRO_DEAD_BODY_RESPONSE_ON_PED(sPedState, ObjectiveData.iPartCausingFail)
						ELSE
							TRIGGER_AGGRO_ON_PED(sPedState, ObjectiveData.iPartCausingFail)
						ENDIF
					ELSE
						TRIGGER_AGGRO_ON_PED(sPedState, ObjectiveData.iPartCausingFail)
					ENDIF
				ENDIF
				
			ENDIF
					
			SET_AGGRO_HELP_TEXT_DATA(ObjectiveData.iSpookedReason, ObjectiveData.iPartCausingFail, iPed)
			
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_FMMC_PED_UNSPOOKED(INT iCount)
	
	SCRIPT_EVENT_DATA_FMMC_PED_ID ObjectiveData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))
		IF ObjectiveData.Details.Type = SCRIPT_EVENT_FMMC_PED_UNSPOOKED
			PRINTLN("[RCC MISSION][SpookAggro] PROCESS_FMMC_PED_UNSPOOKED - ObjectiveData.ipedid = ",ObjectiveData.ipedid)
			
			#IF IS_DEBUG_BUILD
			IF IS_NET_PLAYER_OK(ObjectiveData.Details.FromPlayerIndex, FALSE, FALSE)
				PRINTLN("[RCC MISSION][SpookAggro] PROCESS_FMMC_PED_UNSPOOKED - Sent by part ",NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(ObjectiveData.Details.FromPlayerIndex)))
			ENDIF
			#ENDIF
			
			INT iped = ObjectiveData.ipedid
			
			FMMC_CLEAR_LONG_BIT(iPedLocalSpookedBitset, iped)
			FMMC_CLEAR_LONG_BIT(iPedZoneAggroed, iped)
			FMMC_CLEAR_LONG_BIT(iPedHeardBitset, iPed)
			
			IF bIsLocalPlayerHost
				IF FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedSpookedBitset, iPed)
					//Unspooked:
					FMMC_CLEAR_LONG_BIT(MC_serverBD.iPedSpookedBitset, iped)
					
					
					FMMC_PED_STATE sPedState
					FILL_FMMC_PED_STATE_STRUCT(sPedState, iPed)
					
					IF NOT sPedState.bInjured
						PROCESS_PED_TASK_NOTHING_BRAIN(sPedState)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_FMMC_PED_SPOOK_HURTORKILLED(INT iCount)
	
	SCRIPT_EVENT_DATA_FMMC_PED_ID ObjectiveData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))
		IF ObjectiveData.Details.Type = SCRIPT_EVENT_FMMC_PED_SPOOK_HURTORKILLED
			
			INT iped = ObjectiveData.ipedid
			
			PRINTLN("[RCC MISSION][SpookAggro] PROCESS_FMMC_PED_SPOOK_HURTORKILLED - ObjectiveData.ipedid = ",iped)
			
			#IF IS_DEBUG_BUILD
			IF IS_NET_PLAYER_OK(ObjectiveData.Details.FromPlayerIndex, FALSE, FALSE)
				PRINTLN("[RCC MISSION][SpookAggro] PROCESS_FMMC_PED_SPOOK_HURTORKILLED - Sent by part ",NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(ObjectiveData.Details.FromPlayerIndex)))
			ENDIF
			#ENDIF
			
			FMMC_SET_LONG_BIT(iPedLocalReceivedEvent_HurtOrKilled, iped)
			
			FMMC_PED_STATE sPedState
			FILL_FMMC_PED_STATE_STRUCT(sPedState, iPed)
			
			IF NOT sPedState.bInjured
			AND sPedState.bHasControl
				
				INT iTeamLoop
				INT iAggroTeam = -1
				
				FOR iTeamLoop = 0 TO (MC_serverBD.iNumberOfTeams - 1)
					IF SHOULD_AGRO_FAIL_FOR_TEAM(iped, iTeamLoop)
						iAggroTeam = iTeamLoop
						iTeamLoop = MC_serverBD.iNumberOfTeams // Break out!
					ENDIF
				ENDFOR
				
				IF iAggroTeam != -1
					PROCESS_PED_BODY_AGGRO_INIT(sPedState, iAggroTeam, sPedState.bIsInAnyVehicle, TRUE)
				ELSE
					FMMC_SET_LONG_BIT(iPedLocalSpookedBitset, iPed)
					BROADCAST_FMMC_PED_SPOOKED(iped)
				ENDIF
			ENDIF
			
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_FMMC_PED_GIVEN_SPOOKED_GOTO_TASK(INT iCount)
	
	SCRIPT_EVENT_DATA_FMMC_PED_ID ObjectiveData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))
	AND ObjectiveData.Details.Type = SCRIPT_EVENT_FMMC_PED_GIVEN_SPOOKED_GOTO_TASK
		
		INT iped = ObjectiveData.ipedid
		
		PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_GIVEN_SPOOKED_GOTO_TASK - ObjectiveData.ipedid = ",iped)
		
		#IF IS_DEBUG_BUILD
		IF IS_NET_PLAYER_OK(ObjectiveData.Details.FromPlayerIndex, FALSE, FALSE)
			PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_GIVEN_SPOOKED_GOTO_TASK - Sent by part ",NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(ObjectiveData.Details.FromPlayerIndex)))
		ENDIF
		#ENDIF
		
		FMMC_SET_LONG_BIT(iPedGivenSpookedGotoTaskBS, iPed)
		
		#IF IS_DEBUG_BUILD
		IF NOT FMMC_IS_LONG_BIT_SET(iPedGivenSpookedGotoTaskBS, iPed)
			PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_GIVEN_SPOOKED_GOTO_TASK - Set iPedGivenSpookedGotoTaskBS for ped ",iped)
		ELSE
			PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_GIVEN_SPOOKED_GOTO_TASK - Bit already set for ped ",iped)
		ENDIF
		#ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_FMMC_PED_GIVEN_UNSPOOKED_GOTO_TASK(INT iCount)
	
	SCRIPT_EVENT_DATA_FMMC_PED_ID ObjectiveData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))
	AND ObjectiveData.Details.Type = SCRIPT_EVENT_FMMC_PED_GIVEN_UNSPOOKED_GOTO_TASK
		
		INT iped = ObjectiveData.ipedid
		
		PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_GIVEN_UNSPOOKED_GOTO_TASK - ObjectiveData.ipedid = ",iped)
		
		#IF IS_DEBUG_BUILD
		IF IS_NET_PLAYER_OK(ObjectiveData.Details.FromPlayerIndex, FALSE, FALSE)
			PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_GIVEN_UNSPOOKED_GOTO_TASK - Sent by part ",NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(ObjectiveData.Details.FromPlayerIndex)))
		ENDIF
		#ENDIF
		
		#IF IS_DEBUG_BUILD
		IF FMMC_IS_LONG_BIT_SET(iPedGivenSpookedGotoTaskBS, iPed)
			PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_GIVEN_UNSPOOKED_GOTO_TASK - Clear iPedGivenSpookedGotoTaskBS for ped ",iped)
		ELSE
			PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_GIVEN_UNSPOOKED_GOTO_TASK - Bit already not set for ped ",iped)
		ENDIF
		#ENDIF
		
		FMMC_CLEAR_LONG_BIT(iPedGivenSpookedGotoTaskBS, iPed)
	ENDIF
	
ENDPROC

PROC PROCESS_FMMC_PED_UPDATE_TASKED_GOTO_PROGRESS(INT iCount)
	
	SCRIPT_EVENT_DATA_FMMC_UPDATE_TASKED_GOTO_PROGRESS ObjectiveData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))
	AND ObjectiveData.Details.Type = SCRIPT_EVENT_FMMC_PED_UPDATE_TASKED_GOTO_PROGRESS
		
		INT iped = ObjectiveData.ipedid
		
		PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_UPDATE_TASKED_GOTO_PROGRESS - ObjectiveData.ipedid = ",iped)
		
		#IF IS_DEBUG_BUILD
		IF IS_NET_PLAYER_OK(ObjectiveData.Details.FromPlayerIndex, FALSE, FALSE)
			PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_UPDATE_TASKED_GOTO_PROGRESS - Sent by part ",NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(ObjectiveData.Details.FromPlayerIndex)))
		ENDIF
		#ENDIF
		
		IF iPedTaskedGotoProgress[iped] != ObjectiveData.iGotoProgress
			
			PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_UPDATE_TASKED_GOTO_PROGRESS - Update ped ",iped," iPedTaskedGotoProgress to ",ObjectiveData.iGotoProgress)
			iPedTaskedGotoProgress[iped] = ObjectiveData.iGotoProgress
			
		#IF IS_DEBUG_BUILD
		ELSE
			PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_UPDATE_TASKED_GOTO_PROGRESS - Ped ",iped," tasked goto progress already set to ",ObjectiveData.iGotoProgress)
		#ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_FMMC_FMMC_AGGROED_SCRIPT_FORCED_PED(INT iCount)

	SCRIPT_EVENT_DATA_FMMC_AGGROED_PED ObjectiveData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))
		IF ObjectiveData.Details.Type = SCRIPT_EVENT_FMMC_AGGROED_SCRIPT_FORCED_PED
			PRINTLN("[RCC MISSION][SpookAggro] PROCESS_FMMC_FMMC_AGGROED_SCRIPT_FORCED_PED - ObjectiveData.iPedID = ",ObjectiveData.iPedID)			
			FMMC_SET_LONG_BIT(iPedAggroedWithScriptTrigger, ObjectiveData.iPedID)			
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_FMMC_PED_AGGROED(INT iCount)
	
	SCRIPT_EVENT_DATA_FMMC_AGGROED_PED ObjectiveData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))
		IF ObjectiveData.Details.Type = SCRIPT_EVENT_FMMC_PED_AGGROED
			
			PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_AGGROED - ObjectiveData.iPedID = ",ObjectiveData.iPedID)
			PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_AGGROED - ObjectiveData.iPartCausingFail = ",ObjectiveData.iPartCausingFail)
			IF IS_NET_PLAYER_OK(ObjectiveData.Details.FromPlayerIndex, FALSE, FALSE)
				PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_AGGROED - Sent by part ",NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(ObjectiveData.Details.FromPlayerIndex)))
			ENDIF
			
			INT iPed = ObjectiveData.iPedID
			
			IF FMMC_IS_LONG_BIT_SET(iPedHitByStunGun, iPed)
				PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_AGGROED - Blocked by stungun for  ", iped)
				EXIT
			ENDIF
			
			FMMC_SET_LONG_BIT(iPedPerceptionBitset, iPed)
			FMMC_CLEAR_LONG_BIT(iPedHeardBitset, iPed)
				
			FMMC_PED_STATE sPedState
			FILL_FMMC_PED_STATE_STRUCT(sPedState, iPed)
			
			IF NOT sPedState.bInjured
			
				IF sPedState.bHasControl
				AND ObjectiveData.iPartCausingFail = -3 // This means the ped hasn't been tasked yet
					TRIGGER_AGGRO_TASKS_ON_PED(sPedState, ciSPOOK_NONE, FALSE)
				ENDIF
				
			ENDIF
			
			IF bIsLocalPlayerHost
			AND (ObjectiveData.iPartCausingFail >= -1) //-1 and above mean that the server hasn't handled this stuff yet:
			AND (ObjectiveData.iPedID > -1)
				TRIGGER_AGGRO_SERVERDATA_ON_PED(sPedState, ObjectiveData.iPartCausingFail)
			ENDIF
			
			#IF IS_DEBUG_BUILD 
			IF NOT FMMC_IS_LONG_BIT_SET(iPedLocalAggroedBitset, iPed)
				SET_PED_IS_AGGROD_DEBUG(iPed)
			ENDIF
			#ENDIF
			
			FMMC_SET_LONG_BIT(iPedLocalAggroedBitset, iPed)
			
			IF ObjectiveData.bZoneForced
				PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_AGGROED - iPed = ", iPed, " ObjectiveData.bZoneForced: ", ObjectiveData.bZoneForced)
				FMMC_SET_LONG_BIT(iPedZoneAggroed, iPed)
			ENDIF
			
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_FMMC_PED_SPEECH_AGGROED(INT iCount)
	
	SCRIPT_EVENT_DATA_FMMC_AGGROED_PED ObjectiveData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))
		IF ObjectiveData.Details.Type = SCRIPT_EVENT_FMMC_PED_SPEECH_AGGROED
			
			PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_AGGROED - ObjectiveData.iPedID = ",ObjectiveData.iPedID)
			PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_AGGROED - ObjectiveData.iPartCausingFail = ",ObjectiveData.iPartCausingFail)
			IF IS_NET_PLAYER_OK(ObjectiveData.Details.FromPlayerIndex, FALSE, FALSE)
				PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_AGGROED - Sent by part ",NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(ObjectiveData.Details.FromPlayerIndex)))
			ENDIF
			
			INT iPed = ObjectiveData.iPedID
			
			FMMC_SET_LONG_BIT(iPedPerceptionBitset, iPed)
			FMMC_CLEAR_LONG_BIT(iPedHeardBitset, iPed)
					
			FMMC_PED_STATE sPedState
			FILL_FMMC_PED_STATE_STRUCT(sPedState, iPed)
			
			IF NOT sPedState.bInjured
				IF sPedState.bHasControl
				AND ObjectiveData.iPartCausingFail = -3 // This means the ped hasn't been tasked yet
					TRIGGER_AGGRO_TASKS_ON_PED(sPedState, ciSPOOK_NONE, FALSE)
				ENDIF
			ENDIF
			
			IF bIsLocalPlayerHost
			AND (ObjectiveData.iPartCausingFail >= -1) //-1 and above mean that the server hasn't handled this stuff yet:
			AND (ObjectiveData.iPedID > -1)
				TRIGGER_SPEECH_AGGRO_SERVERDATA_ON_PED(sPedState.pedIndex, ObjectiveData.iPedID, ObjectiveData.iPartCausingFail)
			ENDIF
			
			FMMC_SET_LONG_BIT(iPedLocalAggroedBitset, iPed)
			
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_FMMC_PED_CANCEL_TASKS(INT iCount)
	
	SCRIPT_EVENT_DATA_FMMC_PED_ID ObjectiveData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))
		IF ObjectiveData.Details.Type = SCRIPT_EVENT_FMMC_PED_CANCEL_TASKS
			INT iped = ObjectiveData.ipedid
			PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_CANCEL_TASKS - ObjectiveData.ipedid = ",iped)
			IF IS_NET_PLAYER_OK(ObjectiveData.Details.FromPlayerIndex, FALSE, FALSE)
				PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_CANCEL_TASKS - Sent by part ",NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(ObjectiveData.Details.FromPlayerIndex)))
			ENDIF
			
			FMMC_SET_LONG_BIT(iPedLocalCancelTasksBitset, iped)
			
			IF bIsLocalPlayerHost
				PROCESS_SERVER_PED_CANCEL_TASKS(iped)
			ENDIF
			
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_FMMC_PED_SPEECH_AGRO_BITSET(INT iCount)
	
	SCRIPT_EVENT_DATA_FMMC_PED_ID ObjectiveData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))
		IF ObjectiveData.Details.Type = SCRIPT_EVENT_FMMC_PED_SPEECH_AGRO_BITSET
			
			INT iped = ObjectiveData.ipedid
			
			PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_SPEECH_AGRO_BITSET - ObjectiveData.ipedid = ",iped)
			FMMC_SET_LONG_BIT(iPedSpeechAgroBitset, iped)
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_TRIGGER_AGGRO_FOR_TEAM(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_TRIGGER_AGGRO_FOR_TEAM EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_TRIGGER_AGGRO_FOR_TEAM
			PRINTLN("SCRIPT_EVENT_FMMC_TRIGGER_AGGRO_FOR_TEAM event received! EventData.iTeam: ", EventData.iTeam)
			
			IF bIsLocalPlayerHost
				IF NOT HAS_TEAM_TRIGGERED_AGGRO(EventData.iTeam, EventData.iAggroBS)
					SET_TEAM_AS_TRIGGERED_AGGRO_FROM_BITSET(EventData.iTeam, EventData.iAggroBS)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_BLOCK_WANTED_CONE_RESPONSE_EVENT(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_BLOCK_WANTED_CONE_RESPONSE_EVENT EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_BLOCK_WANTED_CONE_RESPONSE_EVENT
			PRINTLN("[BlockWantedConeResponse] Block Wanted Cone Response Event Received! EventData.bSetBlockWantedConeResponse: ", EventData.bSetBlockWantedConeResponse, "EventData.bClearBlockWantedConeResponse: ", EventData.bClearBlockWantedConeResponse, " EventData.iVeh: ", EventData.iVeh, " EventData.bSeenIllegal: ", EventData.bSeenIllegal)
			
			IF bIsLocalPlayerHost
				IF NOT IS_BIT_SET(MC_serverBD_1.iVehicleBlockedWantedConeResponseBS, EventData.iVeh)
					IF EventData.bSetBlockWantedConeResponse
						SET_BIT(MC_serverBD_1.iVehicleBlockedWantedConeResponseBS, EventData.iVeh)
						PRINTLN("[BlockWantedConeResponse]- PROCESS_SCRIPT_EVENT_FMMC_BLOCK_WANTED_CONE_RESPONSE_EVENT - Set iVehicleBlockedWantedConeResponseBS for vehicle: ", EventData.iVeh)
					ENDIF
				ELSE
					IF EventData.bClearBlockWantedConeResponse
						CLEAR_BIT(MC_serverBD_1.iVehicleBlockedWantedConeResponseBS, EventData.iVeh)
						PRINTLN("[BlockWantedConeResponse]- PROCESS_SCRIPT_EVENT_FMMC_BLOCK_WANTED_CONE_RESPONSE_EVENT - Cleared iVehicleBlockedWantedConeResponseBS for vehicle: ", EventData.iVeh)
					ENDIF
				ENDIF

				IF NOT IS_BIT_SET(MC_serverBD_1.iVehicleBlockedWantedConeResponseSeenIllegalBS, EventData.iVeh)
					IF EventData.bSeenIllegal
						SET_BIT(MC_serverBD_1.iVehicleBlockedWantedConeResponseSeenIllegalBS, EventData.iVeh)
						PRINTLN("[BlockWantedConeResponse]- PROCESS_SCRIPT_EVENT_FMMC_BLOCK_WANTED_CONE_RESPONSE_EVENT - Set iVehicleBlockedWantedConeResponseSeenIllegalBS for vehicle: ", EventData.iVeh)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_COP_DECOY_EVENT(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_COP_DECOY_EVENT EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_COP_DECOY_EVENT
			PRINTLN("[CopDecoy][PlayerAbilities][DistractionAbility] Cop Decoy Event Received! EventData.iPed: ", EventData.iPed, " EventData.bStartedDecoy: ", EventData.bStartedDecoy, " EventData.bDecoyExpired: ", EventData.bDecoyExpired, " EventData.bDecoyShouldFlee: ", EventData.bDecoyShouldFlee)
			
			IF EventData.bStartedDecoy
				SET_DISPATCH_SPAWN_LOCATION(g_FMMC_STRUCT_ENTITIES.sPlacedPed[EventData.iPed].vPos)
			ELIF EventData.bDecoyExpired
			OR EventData.bDecoyShouldFlee
				RESET_DISPATCH_SPAWN_LOCATION()
				IF DOES_BLIP_EXIST(biCopDecoyBlip)
					REMOVE_BLIP(biCopDecoyBlip)
					PRINTLN("[CopDecoy][PlayerAbilities][DistractionAbility] Removing Decoy Blip via PROCESS_SCRIPT_EVENT_FMMC_COP_DECOY_EVENT.")
				ENDIF
			ENDIF
			
			IF bIsLocalPlayerHost
				IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet8, SBBOOL8_COP_DECOY_STARTED)
					IF EventData.bStartedDecoy
					
						INT iCopDecoyStarDeductRandom = GET_RANDOM_INT_IN_RANGE(0, 101) //Get a random number to calculate the 2 star wanted level deduction chance against.
				
						IF iCopDecoyStarDeductRandom <= g_FMMC_STRUCT_ENTITIES.sPlacedPed[EventData.iPed].iPedCopDecoyLose2StarChance
							PRINTLN("[CopDecoy][PlayerAbilities][DistractionAbility] PROCESS_SCRIPT_EVENT_FMMC_COP_DECOY_EVENT() Decoy will deduct 2 stars!")
							SET_BIT(MC_serverBD.iServerBitSet8, SBBOOL8_COP_DECOY_DEDUCT_2_STAR)
						ELSE
							PRINTLN("[CopDecoy][PlayerAbilities][DistractionAbility] PROCESS_SCRIPT_EVENT_FMMC_COP_DECOY_EVENT() Decoy will deduct 1 star!")
						ENDIF
						PRINTLN("[CopDecoy][PlayerAbilities][DistractionAbility] PROCESS_SCRIPT_EVENT_FMMC_COP_DECOY_EVENT() 2 Star chance was: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[EventData.iPed].iPedCopDecoyLose2StarChance, " Random number was: ", iCopDecoyStarDeductRandom)	
						
						REINIT_NET_TIMER(MC_serverBD_1.stCopDecoyActiveTimer)
						SET_BIT(MC_serverBD.iServerBitSet8, SBBOOL8_COP_DECOY_STARTED)
						
						PRINTLN("[CopDecoy][PlayerAbilities][DistractionAbility]- PROCESS_SCRIPT_EVENT_FMMC_COP_DECOY_EVENT - Set SBBOOL8_COP_DECOY_STARTED and started stCopDecoyActiveTimer.") 
					ENDIF
				ENDIF
				
				IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet8, SBBOOL8_COP_DECOY_EXPIRED)
					IF EventData.bDecoyExpired
						SET_BIT(MC_serverBD.iServerBitSet8, SBBOOL8_COP_DECOY_EXPIRED)
						PRINTLN("[CopDecoy][PlayerAbilities][DistractionAbility]- PROCESS_SCRIPT_EVENT_FMMC_COP_DECOY_EVENT - Set SBBOOL8_COP_DECOY_EXPIRED.") 
					ENDIF
				ENDIF
				
				IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet8, SBBOOL8_COP_DECOY_FLEEING)
					IF EventData.bDecoyShouldFlee
						SET_BIT(MC_serverBD.iServerBitSet8, SBBOOL8_COP_DECOY_FLEEING)
						PRINTLN("[CopDecoy][PlayerAbilities][DistractionAbility]- PROCESS_SCRIPT_EVENT_FMMC_COP_DECOY_EVENT - Set SBBOOL8_COP_DECOY_FLEEING.") 
						
						MC_serverBD_2.iCurrentPedRespawnLives[EventData.iPed] = 0
						PRINTLN("[CopDecoy][PlayerAbilities][DistractionAbility] ped ",EventData.iPed," getting cleaned up early - set MC_serverBD_2.iCurrentPedRespawnLives[", EventData.iPed, "] = 0. Call 1.")
			
						FMMC_CLEAR_LONG_BIT(MC_serverBD.iPedCleanup_NeedOwnershipBS, EventData.iPed)
					ENDIF
				ENDIF
				
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_COP_BEHAVIOUR_OVERRIDE_EVENT(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_COP_BEHAVIOUR_OVERRIDE_EVENT EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_COP_BEHAVIOUR_OVERRIDE_EVENT
			PRINTLN("[CopOverride] Cop Behaviour Override Event Received! EventData.iPed: ", EventData.iPed, " EventData.bForceCopOverride: ", EventData.bForceCopOverride)
			
			IF bIsLocalPlayerHost
				IF EventData.bForceCopOverride
					FMMC_SET_LONG_BIT(MC_serverBD.iCopPed, EventData.iPed)
					PRINTLN("[CopOverride] PROCESS_SCRIPT_EVENT_FMMC_COP_BEHAVIOUR_OVERRIDE_EVENT - MC_serverBD.iCopPed: ",EventData.iPed)
				ENDIF	
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_FMMC_PED_SEEN_DEAD_PED(INT iCount)
	
	INT iPed
	STRUCT_DEAD_PED_SEEN sDeadPedSeen
		
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_AI,iCount,sDeadPedSeen,SIZE_OF(sDeadPedSeen))
		IF DOES_ENTITY_EXIST(sDeadPedSeen.FindingPedId)
		AND NOT IS_PED_INJURED(sDeadPedSeen.FindingPedId)
		AND DOES_ENTITY_EXIST(sDeadPedSeen.DeadPedId)
			iPed = IS_ENTITY_A_MISSION_CREATOR_ENTITY(sDeadPedSeen.FindingPedId)			
			IF iPed >= 0
			AND iPed < FMMC_MAX_PEDS
				INT iDeadPed = IS_ENTITY_A_MISSION_CREATOR_ENTITY(sDeadPedSeen.DeadPedId)
	
				IF iDeadPed >= 0
				AND iDeadPed < FMMC_MAX_PEDS
					PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_SEEN_DEAD_PED - ped ",iped, " has seen a dead ped (",iDeadPed,")! Setting bit.")
					FMMC_SET_LONG_BIT(iPedSeenDeadBodyBitset, iPed)
					iDeadPedIDSeen[iped] = NATIVE_TO_INT(sDeadPedSeen.DeadPedId)
				ELSE
					PRINTLN("[RCC MISSION] PROCESS_FMMC_PED_SEEN_DEAD_PED - ped ",iped, " has seen a dead ped but it is not a creator entity - ", NATIVE_TO_INT(sDeadPedSeen.DeadPedId))
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_ENEMY_FIRED_PROJECTILE(INT iCount)
	
	STRUCT_NETWORK_FIRED_DUMMY_PROJECTILE_EVENT sEventInfo
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, sEventInfo, SIZE_OF(sEventInfo))
		
		INT iTeam = MC_playerBD[iPartToUse].iTeam
		INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
		IF iRule < FMMC_MAX_RULES
		AND g_FMMC_STRUCT.sFMMCEndConditions[iTeam].fExplodeNearbyRocketsChance[iRule] > 0.0
			IF GET_RANDOM_FLOAT_IN_RANGE(0, 1) < g_FMMC_STRUCT.sFMMCEndConditions[iTeam].fExplodeNearbyRocketsChance[iRule]
				PRINTLN("[EXPPROJ] PROCESS_ENEMY_FIRED_PROJECTILE - Projectile found but not adding due to ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].fExplodeNearbyRocketsChance[iRule], "% chance of ignoring!")
				EXIT
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(sEventInfo.nFiredProjectileIndex)
			
			eiNearbyRocketsList[iExplodeNearbyRocketsListIndex] = sEventInfo.nFiredProjectileIndex
			iExplodeNearbyRocketsListIndex++
			PRINTLN("[EXPPROJ] PROCESS_ENEMY_FIRED_PROJECTILE - Grabbed missile from event! Adding to slot ", iExplodeNearbyRocketsListIndex, " || Projectile type: ", sEventInfo.nWeaponType)
		ELSE
			PRINTLN("[EXPPROJ] PROCESS_ENEMY_FIRED_PROJECTILE - Got event but entity doesn't exist! || Projectile type: ", sEventInfo.nWeaponType)
		ENDIF
		
		IF iExplodeNearbyRocketsListIndex >= ciNearbyRocketsListSize
			iExplodeNearbyRocketsListIndex = 0
		ENDIF
	ELSE
		PRINTLN("[EXPPROJ] PROCESS_ENEMY_FIRED_PROJECTILE - Couldn't get event data!")
	ENDIF
ENDPROC

PROC PROCESS_PLAYER_COLLECTED_PORTABLE_PICKUP(INT iCount)
	
	STRUCT_PORTABLE_PICKUP_EVENT sEventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, sEventData, SIZE_OF(sEventData))
		
		PRINTLN("[OBJ] PROCESS_PLAYER_COLLECTED_PORTABLE_PICKUP - Processing...")
		
		INT iObj
		FOR iObj = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfObjects - 1
		
			NETWORK_INDEX niTempObj = GET_OBJECT_NET_ID(iObj)
			PRINTLN("[OBJ] PROCESS_PLAYER_COLLECTED_PORTABLE_PICKUP - Object ", iObj, " net id: ", NATIVE_TO_INT(niTempObj))
			
			IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niTempObj)
				PRINTLN("[OBJ] PROCESS_PLAYER_COLLECTED_PORTABLE_PICKUP - Object ", iObj, " - net id invalid")
				RELOOP
			ENDIF
			
			//For some reason the net id from the event is the one that matches up to the object id
			IF NATIVE_TO_INT(niTempObj) != NATIVE_TO_INT(sEventData.PickupID)
				PRINTLN("[OBJ] PROCESS_PLAYER_COLLECTED_PORTABLE_PICKUP - Object ", iObj, " - ids not equal")
				RELOOP
			ENDIF
			
			PRINTLN("[OBJ] PROCESS_PLAYER_COLLECTED_PORTABLE_PICKUP - Object ", iObj, " is valid")
			
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetFive, cibsOBJ5_IgnoreBlipRangeOnCollection)
				PRINTLN("[OBJ] PROCESS_PLAYER_COLLECTED_PORTABLE_PICKUP - Setting ignore blip range bitset for object ", iObj)
				SET_BIT(iIgnoreObjectBlipRangeBS, iObj)
			ENDIF
			
		ENDFOR
		
	ELSE
		PRINTLN("[OBJ] PROCESS_PLAYER_COLLECTED_PORTABLE_PICKUP - Failed to get event data.")
	ENDIF
	
ENDPROC
   
PROC PROCESS_SCRIPT_EVENT_FMMC_SPAWN_PED_AMMO_PICKUP(INT iCount)
	SCRIPT_EVENT_DATA_FMMC_SPAWN_PED_AMMO_PICKUP EventData
	IF NOT GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, EventData, SIZE_OF(EventData))
	OR EventData.Details.Type != SCRIPT_EVENT_FMMC_SPAWN_PED_AMMO_PICKUP
		EXIT
	ENDIF
	
	PICKUP_TYPE puType = PICKUP_CUSTOM_SCRIPT
	
	IF puType = PICKUP_TYPE_INVALID
		EXIT
	ENDIF
	
	PLACEMENT_FLAG ePlacementFlags = PLACEMENT_FLAG_FIXED | PLACEMENT_FLAG_ORIENT_TO_GROUND | PLACEMENT_FLAG_SNAP_TO_GROUND | PLACEMENT_FLAG_UPRIGHT
	
	sLocalPedAmmoDrop[EventData.iIndex].piAmmoPickup = CREATE_PICKUP(puType, EventData.vSpawnpos, ENUM_TO_INT(ePlacementFlags), g_FMMC_STRUCT.sPedAmmoDrop.iAmount, FALSE, mnPedAmmoDropModel)
	REINIT_NET_TIMER(sLocalPedAmmoDrop[EventData.iIndex].tdLifeSpan)
	PRINTLN("PROCESS_SCRIPT_EVENT_FMMC_SPAWN_PED_AMMO_PICKUP - Created ped ammo pickup ", EventData.iIndex, " ID: ", NATIVE_TO_INT(sLocalPedAmmoDrop[EventData.iIndex].piAmmoPickup))
	
ENDPROC
   
PROC PROCESS_SCRIPT_EVENT_FMMC_REMOVE_PED_AMMO_PICKUP(INT iCount)
	SCRIPT_EVENT_DATA_FMMC_REMOVE_PED_AMMO_PICKUP EventData
	IF NOT GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, EventData, SIZE_OF(EventData))
	OR EventData.Details.Type != SCRIPT_EVENT_FMMC_REMOVE_PED_AMMO_PICKUP
		EXIT
	ENDIF
	
	IF DOES_PICKUP_EXIST(sLocalPedAmmoDrop[EventData.iIndex].piAmmoPickup)
		REMOVE_PICKUP(sLocalPedAmmoDrop[EventData.iIndex].piAmmoPickup)
	ENDIF
	RESET_NET_TIMER(sLocalPedAmmoDrop[EventData.iIndex].tdLifeSpan)
	PRINTLN("PROCESS_SCRIPT_EVENT_FMMC_SPAWN_PED_AMMO_PICKUP - Removed ped ammo pickup ", EventData.iIndex)
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Players	 -------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Processes for player functionality  			--------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_SCRIPT_EVENT_FMMC_PLAY_SYNC_SCENE_FACIAL_ANIM(int iCount)
		
	EVENT_STRUCT_FMMC_PLAY_SYNC_SCENE_FACIAL_ANIM EventData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, EventData, SIZE_OF(EventData))

		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_PLAY_SYNC_SCENE_FACIAL_ANIM
			
			IF EventData.iPart != iLocalPart
				PRINTLN("PROCESS_SCRIPT_EVENT_FMMC_PLAY_SYNC_SCENE_FACIAL_ANIM - Exitting, event meant for iPart: ", EventData.iPart)
				EXIT				
			ENDIF
			
			IF IS_PED_INJURED(LocalPlayerPed)
				PRINTLN("PROCESS_SCRIPT_EVENT_FMMC_PLAY_SYNC_SCENE_FACIAL_ANIM - Exitting, We are injured.")
				EXIT
			ENDIF
			
			STRING animDict = GET_ANIM_DICT_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE(EventData.eType, EventData.bUseAlternate)	
			STRING animFaceName = GET_ANIM_NAME_FACE_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE_PLAYER(EventData.eType, EventData.iIndex, EventData.bUseAlternate)
			
			PLAY_FACIAL_ANIM(LocalPlayerPed, animFaceName, animDict)
			
			PRINTLN("PROCESS_SCRIPT_EVENT_FMMC_PLAY_SYNC_SCENE_FACIAL_ANIM - Calling PLAY_FACIAL_ANIM, with animFaceName: ", animFaceName, " animDict: ", animDict)
		ENDIF
		
	ENDIF
	
ENDPROC

PROC PROCESS_FMMC_PLAYER_SWITCHED_TEAMS_EVENT(int iCount)
		
	SCRIPT_EVENT_DATA_FMMC_PLAYER_SWITCHED_TEAMS ObjectiveData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))

		IF ObjectiveData.Details.Type = SCRIPT_EVENT_FMMC_PLAYER_SWITCHED_TEAMS
			PLAYER_INDEX piPlayer
			
			PRINTLN("[RCC MISSION] PROCESS_FMMC_PLAYER_SWITCHED_TEAMS_EVENT - ObjectiveData.Details.FromPlayerIndex = ", GET_PLAYER_NAME(ObjectiveData.Details.FromPlayerIndex))
			PRINTLN("[RCC MISSION] PROCESS_FMMC_PLAYER_SWITCHED_TEAMS_EVENT - From team ",ObjectiveData.iTeam, ", to team ", ObjectiveData.iNewTeam)
			PRINTLN("[RCC MISSION] PROCESS_FMMC_PLAYER_SWITCHED_TEAMS_EVENT - LocalPlayer = ", GET_PLAYER_NAME(LocalPlayer))
								
			// ????? Plug the issue from this end ?????
			// If the data points to the spectator, redirect it to the player we're spectating.
			//IF IS_PLAYER_SPECTATOR_ONLY(ObjectiveData.Details.FromPlayerIndex)
			//	ObjectiveData.Details.FromPlayerIndex = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPartToUse))
			//ENDIF			
			
			// Inherit the spectated target's tickers.
			IF bIsAnySpectator
				piPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPartToUse))
				PRINTLN("[RCC MISSION][PROCESS_FMMC_PLAYER_SWITCHED_TEAMS_EVENT] - Switched Teams On This Event, I am a Spectator")
			ELSE
				piPlayer = LocalPlayer
				PRINTLN("[RCC MISSION][PROCESS_FMMC_PLAYER_SWITCHED_TEAMS_EVENT] - Switched Teams On This Event")
			ENDIF
			
			IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] > -1
			AND MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
				PROCESS_MY_PLAYER_BLIP_VISIBILITY(MC_playerBD[iPartToUse].iteam, MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam])
			ENDIF
			
			TEXT_LABEL_63 tlNewTeamName = GET_CREATOR_TEAM_NAME_FOR_RESULTS_SCREEN(ObjectiveData.iNewTeam)	
			HUD_COLOURS hcPlayerTeamColour = GET_HUD_COLOUR_FOR_FMMC_TEAM(ObjectiveData.iNewTeam, piPlayer)
			
			IF ObjectiveData.Details.FromPlayerIndex != LocalPlayer
				PRINT_TICKER_WITH_CUSTOM_STRING_AND_PLAYER_NAME_CUSTOM_HUD_COLOUR("TS_TICK", tlNewTeamName, ObjectiveData.Details.FromPlayerIndex, hcPlayerTeamColour)
				REFRESH_ALL_OVERHEAD_DISPLAY()
			ENDIF
			
			IF ObjectiveData.bShowShard
				IF ObjectiveData.Details.FromPlayerIndex = LocalPlayer
					SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_IN_STRAPLINE(BIG_MESSAGE_GENERIC_TEXT, "GG_S_LSL", tlNewTeamName, DEFAULT, DEFAULT, "GG_S_ST")
				ELSE
					SETUP_NEW_BIG_MESSAGE_WITH_PLAYER_AND_LITERAL_STRING(BIG_MESSAGE_GENERIC_TEXT, ObjectiveData.Details.FromPlayerIndex, "GG_S_RSL", tlNewTeamName, "GG_S_ST")
				ENDIF
			ENDIF
			
			IF bIsLocalPlayerHost
				SET_BIT(MC_serverBD.iServerBitSet, SBBOOL_SKIP_PARTICIPANT_UPDATE)
			ENDIF	
		ENDIF	
	ENDIF
ENDPROC

PROC PROCESS_FMMC_PLAYER_REQUEST_TEAM_SWITCH_EVENT(INT iCount)
	SCRIPT_EVENT_DATA_FMMC_PLAYER_TEAM_SWITCH_REQUEST ObjectiveData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))

		IF ObjectiveData.Details.Type = SCRIPT_EVENT_FMMC_PLAYER_TEAM_SWITCH_REQUEST
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciCLOBBER_PLAYER_ON_TEAM_SWAP)
				IF ObjectiveData.iEventSessionKey = MC_ServerBD.iSessionScriptEventKey
					IF bIsLocalPlayerHost
						
						PROCESS_PLAYER_TEAM_SWITCH(ObjectiveData.iVictimPart, ObjectiveData.piKiller, ObjectiveData.bSuicide, ObjectiveData.bNoVictim)
					ENDIF	
				ELSE
					PRINTLN("[JS][TeamSwaps] PROCESS_FMMC_PLAYER_REQUEST_TEAM_SWITCH_EVENT - Session Keys dont match")
				ENDIF
			ELSE
				PRINTLN("[JS][TeamSwaps] PROCESS_FMMC_PLAYER_REQUEST_TEAM_SWITCH_EVENT - Creator setting not on, this shouldn't be called")
			ENDIF
		ENDIF	
	ENDIF
ENDPROC

PROC PROCESS_FMMC_SERVER_AUTHORISED_TEAM_SWAP_EVENT(INT iCount)
	SCRIPT_EVENT_DATA_SERVER_AUTHORISED_TEAM_SWAP ObjectiveData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))
		IF ObjectiveData.Details.Type = SCRIPT_EVENT_SERVER_AUTHORISED_TEAM_SWAP
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciCLOBBER_PLAYER_ON_TEAM_SWAP)
				IF ObjectiveData.iEventSessionKey = MC_ServerBD.iSessionScriptEventKey
					IF ObjectiveData.iKillerPart = iLocalPart
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciTEAM_SWITCH_SHARDS)
							PRINTLN("[RCC MISSION] Playing Winning_Team_Shard 1")
							PRINTLN("[RCC MISSION] Killer iPart: ", ObjectiveData.iKillerPart)
							
							STRING sWinText = "TS_WIN"

							sWinText= "TS_WIN"

							PLAY_SOUND_FRONTEND(-1, "Winning_Team_Shard", "DLC_Exec_TP_SoundSet", FALSE)
								PLAY_SOUND_FRONTEND(-1, "Winning_Team_Shard", "DLC_Exec_TP_SoundSet", FALSE)//Should this be called twice? 
							SETUP_NEW_BIG_MESSAGE_WITH_STRING(BIG_MESSAGE_GENERIC_TEXT, "", sWinText, "TGIG_WINNER")
						ENDIF
						CHANGE_PLAYER_TEAM(ObjectiveData.iKillerNewTeam, TRUE)
						BROADCAST_FMMC_OBJECTIVE_PLAYER_SWITCHED_TEAMS(MC_playerBD[iLocalPart].iteam, ObjectiveData.iKillerNewTeam)

					ELIF ObjectiveData.iVictimPart = iLocalPart
						PRINTLN("[RCC MISSION] Victim iPart: ", ObjectiveData.iVictimPart)
						IF NOT IS_BIT_SET(iLocalBoolCheck16, LBOOL16_SHOULD_SHOW_TEAM_SWITCH_SHARD)
							SET_BIT(iLocalBoolCheck16, LBOOL16_SHOULD_SHOW_TEAM_SWITCH_SHARD)
						ENDIF
						CHANGE_PLAYER_TEAM(ObjectiveData.iVictimNewTeam, TRUE)
						BROADCAST_FMMC_OBJECTIVE_PLAYER_SWITCHED_TEAMS(MC_playerBD[iLocalPart].iteam, ObjectiveData.iVictimNewTeam)
					ENDIF
								
					IF bIsLocalPlayerHost
						SET_BIT(MC_serverBD.iServerBitSet, SBBOOL_SKIP_PARTICIPANT_UPDATE)
					ENDIF
				ELSE
					PRINTLN("[JS][TeamSwaps] PROCESS_FMMC_SERVER_AUTHORISED_TEAM_SWAP_EVENT - Session Keys dont match")
				ENDIF
			ELSE
				PRINTLN("[JS][TeamSwaps] PROCESS_FMMC_SERVER_AUTHORISED_TEAM_SWAP_EVENT - Creator setting not on, this shouldn't be called")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_FMMC_CLIENT_TEAM_SWAP_COMPLETE(INT iCount)
	SCRIPT_EVENT_DATA_CLIENT_TEAM_SWAP_COMPLETE ObjectiveData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))
		IF ObjectiveData.Details.Type = SCRIPT_EVENT_CLIENT_TEAM_SWAP_COMPLETE
			IF bIsLocalPlayerHost
				CLEAR_BIT(MC_serverBD_4.iTeamSwapLockFlag, ObjectiveData.iPartToClear)
				MC_serverBD.iHostRefreshDpadValue++
				PRINTLN("[MMacK][TeamSwaps] iTeamSwapLockFlag = CLEAR 3", ObjectiveData.iPartToClear)
			ENDIF
		ENDIF
	ENDIF	
ENDPROC

PROC PROCESS_FMMC_PLAYER_COMMITED_SUICIDE_EVENT(int iCount)
	
	SCRIPT_EVENT_DATA_FMMC_PLAYER_COMMITED_SUICIDE ObjectiveData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))

		IF ObjectiveData.Details.Type = SCRIPT_EVENT_FMMC_PLAYER_COMMITED_SUICIDE

			//[KH] Don't display it if the local player sent it as they will have already done this locally
			IF ObjectiveData.Details.FromPlayerIndex != LocalPlayer
				PRINTLN("[PROCESS_FMMC_PLAYER_COMMITED_SUICIDE_EVENT][RH] localPlayer is: ", iLocalPart, " partToUse is ", iPartToUse)
				HUD_COLOURS hcPlayerTeamColour = GET_HUD_COLOUR_FOR_FMMC_TEAM(ObjectiveData.iTeam, PlayerToUse)
				PRINT_TICKER_WITH_PLAYER_NAME_WITH_HUD_COLOUR("OOBSUI_TICK", ObjectiveData.Details.FromPlayerIndex, hcPlayerTeamColour)
			ENDIF
			
			INT iDeadPart = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(ObjectiveData.Details.FromPlayerIndex))

			IF bIsLocalPlayerHost
				
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciNEGATIVE_SUICIDE_SCORE)
					INT igamestage = GET_MC_CLIENT_MISSION_STAGE(iDeadPart)
					PRINTLN("[RCC MISSION] PROCESS_FMMC_PLAYER_COMMITED_SUICIDE_EVENT - GET_MC_CLIENT_MISSION_STAGE: ",igamestage) 
					PRINTLN("[RCC MISSION] PROCESS_FMMC_PLAYER_COMMITED_SUICIDE_EVENT - host subtracting team kills ") 
					IF igamestage = CLIENT_MISSION_STAGE_KILL_PLAYERS
						IF MC_serverBD.iTeamPlayerKills[ObjectiveData.iTeam][ObjectiveData.iTeam] > 0
							MC_serverBD.iTeamPlayerKills[ObjectiveData.iTeam][ObjectiveData.iTeam]--
							PRINTLN("[RCC MISSION] PROCESS_FMMC_PLAYER_COMMITED_SUICIDE_EVENT - MC_serverBD.iTeamPlayerKills decreased")
						ENDIF
					ELIF igamestage = CLIENT_MISSION_STAGE_KILL_TEAM0
					OR igamestage = CLIENT_MISSION_STAGE_KILL_TEAM1
					OR igamestage = CLIENT_MISSION_STAGE_KILL_TEAM2
					OR igamestage = CLIENT_MISSION_STAGE_KILL_TEAM3
						IF MC_serverBD.iTeamPlayerKills[ObjectiveData.iTeam][ObjectiveData.iTeam] > 0
							MC_serverBD.iTeamPlayerKills[ObjectiveData.iTeam][ObjectiveData.iTeam]--
							PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - MC_serverBD.iTeamPlayerKills of team ",ObjectiveData.iTeam," decreased ") 
						ENDIF
					ENDIF
				ENDIF
			
				INT iVictimRule = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iDeadPart].iTeam]
				IF iVictimRule < FMMC_MAX_RULES
					IF g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iDeadPart].iTeam].iDeathPenalty[iVictimRule] > 0
						INT iCurrentTimer
						INT iPenalty = g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iDeadPart].iTeam].iDeathPenalty[iVictimRule] * 1000
						
						IF IS_OBJECTIVE_TIMER_RUNNING(MC_playerBD[iDeadPart].iTeam)
							iCurrentTimer = GET_OBJECTIVE_TIMER_TIME_REMAINING(MC_playerBD[iDeadPart].iTeam, iVictimRule)
							IF (iCurrentTimer - iPenalty) > (g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iDeadPart].iTeam].iDeathPenaltyThreshold[iVictimRule] * 1000)
								PRINTLN("[JS] - PROCESS_FMMC_PLAYER_COMMITED_SUICIDE_EVENT - Decrementing team ", MC_playerBD[iDeadPart].iTeam,"'s timer due to player death")
								MC_serverBD_3.iTimerPenalty[MC_playerBD[iDeadPart].iTeam] += iPenalty
							ENDIF
						ENDIF
					ENDIF
					
				ENDIF
			ENDIF
		ENDIF	
	ENDIF
ENDPROC

PROC PROCESS_RANDOM_SPAWN_SLOT_FOR_TEAM(int iCount)

	SCRIPT_EVENT_RANDOM_SPAWN_POS_DETAILS ObjectiveData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))
		IF ObjectiveData.Details.Type = SCRIPT_EVENT_RANDOM_SPAWN_SLOT_USED
			IF bIsLocalPlayerHost
				IF NOT IS_BIT_SET(iTeamPropSpawns, ObjectiveData.iTeam)
					SET_BIT(MC_ServerBD.iSpawnPointsUsed[ObjectiveData.iTeam], ObjectiveData.iSpawnSlot)
					SET_BIT(iTeamPropSpawns, ObjectiveData.iTeam)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_REMOTE_VEHICLE_SWAP(int iCount)

	SCRIPT_EVENT_DATA_FMMC_REMOTE_VEHICLE_SWAP ObjectiveData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))
		IF ObjectiveData.Details.Type = SCRIPT_EVENT_FMMC_REMOTE_VEHICLE_SWAP
			IF NETWORK_IS_PLAYER_ACTIVE(ObjectiveData.playerIndex)
				PED_INDEX tempPed = GET_PLAYER_PED(ObjectiveData.playerIndex)
				IF DOES_ENTITY_EXIST(tempPed)
					IF IS_PED_IN_ANY_VEHICLE(tempPed)
						IF DOES_ENTITY_EXIST(GET_VEHICLE_PED_IS_IN(tempPed))
							SET_ENTITY_MOTION_BLUR(GET_VEHICLE_PED_IS_IN(tempPed), FALSE)
							PRINTLN("[PROCESS_REMOTE_VEHICLE_SWAP] Setting motion blur on entity")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_ALPHA_CHANGE(INT iEventID)
	PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_ALPHA_CHANGE] - Processing...")
	
	SCRIPT_EVENT_DATA_FMMC_ALPHA_CHANGE EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_ALPHA_CHANGE

			PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_ALPHA_CHANGE] - iAlphaPlayer: ", EventData.iAlphaPlayer, " iVehicleAlpha", EventData.iAlphaVehicle, " iPart: ", EventData.iPart, " iNetVehIndex: ", EventData.iNetVehIndex)
			
			IF EventData.iPart > -1
				IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(EventData.iPart))
					PED_INDEX piPed =  GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(EventData.iPart)))
			
					IF NOT IS_PED_INJURED(piPed)
						IF EventData.iAlphaPlayer > -1
							IF EventData.iAlphaPlayer = 255							
								PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_ALPHA_CHANGE] - Resetting Alpha ")
								RESET_ENTITY_ALPHA(piPed)
							ELSE
								PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_ALPHA_CHANGE] - Setting alpha ")
								SET_ENTITY_ALPHA(piPed, EventData.iAlphaPlayer, FALSE)						
							ENDIF
						ENDIF
						
						IF EventData.iAlphaVehicle > -1
							IF EventData.iAlphaVehicle = 255							
								PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_ALPHA_CHANGE] - Resetting Alpha ")
								IF IS_PED_IN_ANY_VEHICLE(piPed)
								AND DOES_ENTITY_EXIST(GET_VEHICLE_PED_IS_IN(piPed))
									PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_ALPHA_CHANGE] - Resetting Alpha veh")
									RESET_ENTITY_ALPHA(GET_VEHICLE_PED_IS_IN(piPed))
								ENDIF
								IF EventData.iNetVehIndex > 0
									NETWORK_INDEX netVehID = INT_TO_NATIVE(NETWORK_INDEX, EventData.iNetVehIndex)
									IF NETWORK_DOES_NETWORK_ID_EXIST(netVehID)
										VEHICLE_INDEX vehToChange = NET_TO_VEH(netVehID)
										PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_ALPHA_CHANGE] - Resetting alpha veh (passed in)")
										RESET_ENTITY_ALPHA(vehToChange)
									ELSE
										PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_ALPHA_CHANGE] - Net id does not exist...")
									ENDIF
								ENDIF
							ELSE
								PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_ALPHA_CHANGE] - Setting alpha ")
								IF IS_PED_IN_ANY_VEHICLE(piPed)
								AND DOES_ENTITY_EXIST(GET_VEHICLE_PED_IS_IN(piPed))
									PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_ALPHA_CHANGE] - Setting alpha veh")
									SET_ENTITY_ALPHA(GET_VEHICLE_PED_IS_IN(piPed), EventData.iAlphaVehicle, FALSE)
								ENDIF
								IF EventData.iNetVehIndex > 0
									NETWORK_INDEX netVehID = INT_TO_NATIVE(NETWORK_INDEX, EventData.iNetVehIndex)
									IF NETWORK_DOES_NETWORK_ID_EXIST(netVehID)
										VEHICLE_INDEX vehToChange = NET_TO_VEH(netVehID)
										PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_ALPHA_CHANGE] - Setting alpha veh (passed in)")
										SET_ENTITY_ALPHA(vehToChange, EventData.iAlphaVehicle, FALSE)
									ELSE
										PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_ALPHA_CHANGE] - Net id does not exist...")
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF	
	ENDIF
ENDPROC

//Clears the bit for all other players. So that the player that sends out the event doesn't get to be on the small team twice in a row.
PROC PROCESS_SCRIPT_EVENT_FMMC_CLEAR_HAS_BEEN_ON_SMALLEST_TEAM(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_CLEAR_HAS_BEEN_ON_SMALLEST_TEAM EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF IS_BIT_SET(GlobalplayerBD_FM_2[NATIVE_TO_INT(LocalPlayer)].iMissionDataBitSetTwo, ciMISSION_DATA_TWO_HAS_JOINED_SMALLEST_TEAM)
			CLEAR_BIT(GlobalplayerBD_FM_2[NATIVE_TO_INT(LocalPlayer)].iMissionDataBitSetTwo, ciMISSION_DATA_TWO_HAS_JOINED_SMALLEST_TEAM)
			PRINTLN("PROCESS_SCRIPT_EVENT_FMMC_CLEAR_HAS_BEEN_ON_SMALLEST_TEAM - Clearing ciMISSION_DATA_TWO_HAS_JOINED_SMALLEST_TEAM")
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_PHONE_EMP_EVENT(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_PHONE_EMP_EVENT EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_PHONE_EMP_EVENT
			PRINTLN("[PhoneEMP] Phone EMP Event Received! EventData.bActivateEMP: ", EventData.bActivateEMP, " | EventData.bDeactivateEMP: ", EventData.bDeactivateEMP)
			
			IF bIsLocalPlayerHost
			AND IS_BIT_SET(g_FMMC_Struct.iContinuityBitset, ciMISSION_CONTINUITY_BS_EMPUsedTracking)
				IF NOT HAS_PHONE_EMP_BEEN_USED()
					SET_BIT(MC_serverBD_1.sMissionContinuityVars.iGenericTrackingBitset, ciContinuityGenericTrackingBS_EMPUsed)
					PRINTLN("[PhoneEMP][CONTINUITY] - PROCESS_SCRIPT_EVENT_FMMC_PHONE_EMP_EVENT - EMP has been used")
				ENDIF
			ENDIF
			
			IF EventData.bActivateEMP
				ACTIVATE_PHONE_EMP()
			ENDIF
			IF EventData.bDeactivateEMP
				DEACTIVATE_PHONE_EMP()
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_KILL_PLAYER_NOT_AT_LOCATE(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_KILL_PLAYER_NOT_AT_LOCATE EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_KILL_PLAYER_NOT_AT_LOCATE
			IF EventData.iEventPart > -1
				IF EventData.iEventPart = iLocalPart
					IF NOT IS_ENTITY_DEAD(LocalPlayerPed)
						IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
							PRINTLN("[JT BPKP] BLEEP_TEST_KILL_PLAYERS_NOT_AT_LOCATE - in a vehicle")
							VEHICLE_INDEX tempVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
							IF NETWORK_HAS_CONTROL_OF_ENTITY(tempVeh)
								IF IS_ENTITY_ALIVE(tempVeh) 
									NETWORK_EXPLODE_VEHICLE(tempVeh, TRUE,TRUE)
								ENDIF
							ELSE
								NETWORK_REQUEST_CONTROL_OF_ENTITY(tempVeh)
							ENDIF
						ELSE
							SET_ENTITY_HEALTH(LocalPlayerPed, 0)	//here for bug 3318679
							PRINTLN("[JT BPKP] Killing participant ", iLocalPart)	
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_BROADCAST_PREREQS(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_BROADCAST_PREREQS EventData
	IF NOT GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		EXIT
	ENDIF
	
	IF EventData.Details.Type != SCRIPT_EVENT_FMMC_BROADCAST_PREREQS
		EXIT
	ENDIF
	
	IF EventData.Details.FromPlayerIndex = LocalPlayer
		EXIT
	ENDIF
			
	IF EventData.bClear
		PRINTLN("PROCESS_SCRIPT_EVENT_FMMC_BROADCAST_PREREQS - Clearing")
		RESET_PREREQUISITES_COMPLETED_FROM_LONG_BITSET(EventData.iPreReqBS, FALSE)
	ELSE
		PRINTLN("PROCESS_SCRIPT_EVENT_FMMC_BROADCAST_PREREQS - Setting")
		SET_PREREQUISITES_COMPLETED_FROM_LONG_BITSET(EventData.iPreReqBS, FALSE)
	ENDIF
	
ENDPROC


PROC PROCESS_SCRIPT_EVENT_FMMC_SPAWN_PROTECTION_ALPHA(INT iEventID)
	PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_ZONE_SPAWN_PROTECTION_ALPHA] - Processing...")
	
	SCRIPT_EVENT_DATA_FMMC_SPAWN_PROTECTION_ALPHA EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_SPAWN_PROTECTION_ALPHA
			
			PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_ZONE_SPAWN_PROTECTION_ALPHA] - bActivate: ", EventData.bActivate, " On Participant: ", EventData.iPart)
			
			IF iLocalPart = EventData.iPart
			
			ELSE
				IF EventData.iPart > -1
					IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(EventData.iPart))
						PED_INDEX piPed =  GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(EventData.iPart)))
				
						IF NOT IS_PED_INJURED(piPed)
							IF EventData.bActivate
								PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_ZONE_SPAWN_PROTECTION_ALPHA] - Setting alpha to 50 ")
								SET_ENTITY_ALPHA(piPed, 50, FALSE)
							ELSE
								PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_ZONE_SPAWN_PROTECTION_ALPHA] - Resetting Alpha ")
								RESET_ENTITY_ALPHA(piPed)	
								IF IS_PED_IN_ANY_VEHICLE(piPed)
								AND DOES_ENTITY_EXIST(GET_VEHICLE_PED_IS_IN(piPed))
									RESET_ENTITY_ALPHA(GET_VEHICLE_PED_IS_IN(piPed))
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF	
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_INITIATE_SEAT_SWAP_TEAM_VEHICLE(INT iEventID)
	PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_SCRIPT_EVENT_FMMC_INITIATE_SEAT_SWAP_TEAM_VEHICLE][SWAP_SEAT] - Processing...")
	
	SCRIPT_EVENT_DATA_FMMC_INITIATE_SEAT_SWAP_TEAM_VEHICLE EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_INITIATE_SEAT_SWAP_TEAM_VEHICLE
			
			BOOL bShard = FALSE
			
			INT i
			FOR i = 0 TO MAX_VEHICLE_PARTNERS-1
				IF iLocalPart = EventData.iTeamVehiclePartners[i]
				AND LocalPlayer != EventData.Details.FromPlayerIndex
					PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_SCRIPT_EVENT_FMMC_INITIATE_SEAT_SWAP_TEAM_VEHICLE][SWAP_SEAT] - I am a Passenger of this vehicle which wants to swap/cycle Seats. Setting Bit: PBBOOL3_INITIATED_VEHICLE_SEAT_SWAP")
					SET_BIT(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_INITIATED_VEHICLE_SEAT_SWAP)
					bShard = TRUE
				ENDIF
			ENDFOR
										
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iTeamBitSet3, ciBS3_ENABLE_TEAM_VEHICLE_SWAP_REQUIRES_CONSENT)
				IF bShard
					//SETUP_NEW_BIG_MESSAGE_WITH_PLAYER(BIG_MESSAGE_GENERIC_TEXT, EventData.Details.FromPlayerIndex, DEFAULT, "SHD_CMVSS", "SHD_CONSET", GET_HUD_COLOUR_FOR_FMMC_TEAM(MC_playerBD[iPartToUse].iteam, PlayerToUse), ci_VEH_SEAT_SWAP_CONSENT_TIME)
					PRINT_HELP_WITH_PLAYER_NAME("CONSET_HELP", GET_PLAYER_NAME(EventData.Details.FromPlayerIndex), HUD_COLOUR_WHITE, ci_VEH_SEAT_SWAP_CONSENT_TIME)
				ELIF LocalPlayer = EventData.Details.FromPlayerIndex
					PRINT_HELP("CONSET_HELPb", ci_VEH_SEAT_SWAP_CONSENT_TIME)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_MANUAL_RESPAWN_TO_PASSENGERS(INT iEventID)
	PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_MANUAL_RESPAWN_TO_PASSENGERS] - Broadcasting...")
	
	SCRIPT_EVENT_DATA_FMMC_MANUAL_RESPAWN_TO_PASSENGERS EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_MANUAL_RESPAWN_TO_PASSENGERS
			IF EventData.iEventLocalPartDriver = iLocalPart
				PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_MANUAL_RESPAWN_TO_PASSENGERS] - I am the Driver")
			ENDIF
		ENDIF	
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_LEFT_LOCATE_DELAY_RECAPTURE(INT iEventID)

	SCRIPT_EVENT_DATA_FMMC_LEFT_LOCATE_DELAY_RECAPTURE EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_LEFT_LOCATE_DELAY_RECAPTURE
			IF MC_PlayerBD[EventData.iPartToBlock].iteam > -1
			AND EventData.iLoc < FMMC_MAX_GO_TO_LOCATIONS
				RESET_NET_TIMER(tdDisableLocateRecaptureTimer[EventData.iPartToBlock])
				START_NET_TIMER(tdDisableLocateRecaptureTimer[EventData.iPartToBlock])
				
				IF bIsLocalPlayerHost
					IF MC_serverBD_1.inumberOfTeamInArea[EventData.iLoc][MC_PlayerBD[EventData.iPartToBlock].iTeam] >= 1
						MC_serverBD_1.inumberOfTeamInArea[EventData.iLoc][MC_PlayerBD[EventData.iPartToBlock].iTeam]--
					ENDIF
					
					IF iStaggeredPlayerIterator > EventData.iPartToBlock
					AND tempNumberOfTeamInArea[EventData.iLoc][MC_PlayerBD[EventData.iPartToBlock].iTeam] >= 1
						tempNumberOfTeamInArea[EventData.iLoc][MC_PlayerBD[EventData.iPartToBlock].iTeam]--
					ENDIF
				ENDIF
			ENDIF
		ENDIF		
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_PLAYER_ABILITY_ACTIVATED(INT iEventID)

	SCRIPT_EVENT_DATA_FMMC_PLAYER_ABILITY_ACTIVATED EventData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
	AND EventData.Details.Type = SCRIPT_EVENT_FMMC_PLAYER_ABILITY_ACTIVATED
		
		IF NOT (EventData.piActivator = LocalPlayer)
			PRINT_TICKER_WITH_PLAYER_NAME("FMMC_PAA_R", EventData.piActivator)
		ENDIF
		
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Model Swap
// ##### Description: Processes for model swap events
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC ADD_FMMC_MODEL_SWAP(FMMC_MODEL_SWAP_CONFIG& sModelSwapConfig)
	
	INT iNewModelSwapSlot = GET_FIRST_FREE_ACTIVE_MODEL_SWAP_INDEX()
	IF iNewModelSwapSlot = -1
		PRINTLN("[ModelSwaps][ModelSwap ", iNewModelSwapSlot, "] ADD_FMMC_MODEL_SWAP | Failed to add new model swap! Hash: ", sModelSwapConfig.iModelSwapHash)
		EXIT
	ENDIF
	
	PRINTLN("[ModelSwaps][ModelSwap ", iNewModelSwapSlot, "] ADD_FMMC_MODEL_SWAP | Adding new model swap with hash ", sModelSwapConfig.iModelSwapHash)
	sActiveModelSwaps[iNewModelSwapSlot].eModelSwapState = MODEL_SWAP_STATE__PREPARING
	sActiveModelSwaps[iNewModelSwapSlot].sModelSwapConfig = sModelSwapConfig
	iCurrentActiveModelSwaps++
ENDPROC

PROC START_FMMC_MODEL_SWAP(INT iModelSwapHash)
	INT iModelSwapSlot = GET_ACTIVE_MODEL_SWAP_INDEX(iModelSwapHash)
	
	IF iModelSwapSlot = -1
		PRINTLN("[ModelSwaps][ModelSwap ", iModelSwapSlot, "] START_FMMC_MODEL_SWAP | Couldn't find registered model swap! Hash: ", iModelSwapHash)
		EXIT
	ENDIF
	
	PRINTLN("[ModelSwaps][ModelSwap ", iModelSwapSlot, "] START_FMMC_MODEL_SWAP | Received event to start model swap with hash ", iModelSwapHash)
	sActiveModelSwaps[iModelSwapSlot].bPerformModelSwapNow = TRUE
ENDPROC

PROC PREPARE_TO_REVERSE_FMMC_MODEL_SWAP(INT iModelSwapHash)
	INT iModelSwapSlot = GET_ACTIVE_MODEL_SWAP_INDEX(iModelSwapHash)
	
	IF iModelSwapSlot = -1
		PRINTLN("[ModelSwaps][ModelSwap ", iModelSwapSlot, "] PREPARE_TO_REVERSE_FMMC_MODEL_SWAP | Couldn't find registered model swap! Hash: ", iModelSwapHash)
		EXIT
	ENDIF
	
	PRINTLN("[ModelSwaps][ModelSwap ", iModelSwapSlot, "] PREPARE_TO_REVERSE_FMMC_MODEL_SWAP | Received event to reverse model swap with hash ", iModelSwapHash)
	sActiveModelSwaps[iModelSwapSlot].bPerformModelSwapNow = FALSE
	sActiveModelSwaps[iModelSwapSlot].bReverse = TRUE
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_MODEL_SWAP(INT iCount)

	SCRIPT_EVENT_DATA_FMMC_MODEL_SWAP EventData
	IF NOT GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, EventData, SIZE_OF(EventData))
	OR EventData.Details.Type != SCRIPT_EVENT_FMMC_MODEL_SWAP
		EXIT
	ENDIF
	
	SWITCH EventData.eModelSwapEventType
		CASE MODEL_SWAP_EVENT_TYPE__ADD
			ADD_FMMC_MODEL_SWAP(EventData.sModelSwapConfig)
		BREAK
		
		CASE MODEL_SWAP_EVENT_TYPE__PERFORM
			START_FMMC_MODEL_SWAP(EventData.sModelSwapConfig.iModelSwapHash)
		BREAK
		
		CASE MODEL_SWAP_EVENT_TYPE__REVERSE
			PREPARE_TO_REVERSE_FMMC_MODEL_SWAP(EventData.sModelSwapConfig.iModelSwapHash)
		BREAK
	ENDSWITCH
	
ENDPROC	
				
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Props
// ##### Description: Processes for prop functionality
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


PROC PROCESS_SCRIPT_EVENT_FMMC_LOCH_SANTOS_MONSTER(INT iCount)
	SCRIPT_EVENT_DATA_FMMC_LOCH_SANTOS_MONSTER EventData
	IF NOT GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, EventData, SIZE_OF(EventData))
	OR EventData.Details.Type != SCRIPT_EVENT_FMMC_LOCH_SANTOS_MONSTER
		EXIT
	ENDIF
	
	IF NOT bIsLocalPlayerHost
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_LOCH_SANTOS_MONSTER_SPOTTED)
		SET_BIT(MC_serverBD.iServerBitSet2, SBBOOL2_LOCH_SANTOS_MONSTER_SPOTTED)
		PRINTLN("PROCESS_SCRIPT_EVENT_FMMC_LOCH_SANTOS_MONSTER - Bit set for LSM processing!")
	ENDIF
	
ENDPROC				

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Rules	 -------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Processes for rules functionality  			--------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


PROC PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT_PREREQ_SETTING(INT iEntityType, INT iEntityID)
	
	INT iPrereq = ciPREREQ_None
	BOOL bNetwork = FALSE
	
	SWITCH iEntityType
		CASE ciFMMC_ENTITY_LINK_TYPE_LOCATION
			iPrereq = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iEntityID].iObjectivePrereq
			bNetwork = IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iEntityID].iLocBS3, ciLoc_BS3_BroadcastObjectivePrereq)
		BREAK
		CASE ciFMMC_ENTITY_LINK_TYPE_OBJECT
			iPrereq = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iEntityID].iObjectivePrereq
			bNetwork = IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iEntityID].iObjectBitSetFive, cibsOBJ5_BroadcastObjectivePrereq)
		BREAK
		CASE ciFMMC_ENTITY_LINK_TYPE_VEHICLE
			iPrereq = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iEntityID].iObjectivePrereq
			bNetwork = IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iEntityID].iVehBitsetTen, ciFMMC_VEHICLE10_BroadcastObjectivePrereq)
		BREAK
		CASE ciFMMC_ENTITY_LINK_TYPE_PED
			iPrereq = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iEntityID].iObjectivePrereq
			bNetwork = IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iEntityID].iPedBitsetTwentyTwo, ciPED_BSTwentyTwo_BroadcastObjectivePrereq)
		BREAK
	ENDSWITCH
	
	IF iPrereq != ciPREREQ_None
		PRINTLN("PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT_PREREQ_SETTING - Setting Prereq ", iPrereq," as Entity ", iEntityID, " of type ", iEntityType, "'s objective was complete. (Networked: ", BOOL_TO_STRING(bNetwork), ")")
		SET_PREREQUISITE_COMPLETED(iPrereq, bNetwork)
	ENDIF

ENDPROC

//fmmc2020 - Can be functionalised
PROC PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT(INT iCount)

	INT iteam,irepeat,iteamrepeat
	INT ivehNumber
	INT ipriority[FMMC_MAX_TEAMS]
	BOOL bwasobjective[FMMC_MAX_TEAMS]
	BOOL bobjectivefails[FMMC_MAX_TEAMS]
	PARTICIPANT_INDEX tempPart
	PLAYER_INDEX tempPlayer

	SCRIPT_EVENT_DATA_FMMC_OBJECTIVE_COMPLETE ObjectiveData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))
		
		IF ObjectiveData.Details.Type = SCRIPT_EVENT_FMMC_OBJECTIVE_COMPLETE
			
			PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iObjectiveID = ",ObjectiveData.iObjectiveID)
			PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iParticipant = ",ObjectiveData.iparticipant) 
			PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iEntityID = ",ObjectiveData.iEntityID)
			PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iPriority = ",ObjectiveData.iPriority)
			PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iRevalidates = ",ObjectiveData.iRevalidates)

			IF ObjectiveData.iPriority < FMMC_MAX_RULES
			AND ObjectiveData.iParticipant = iLocalPart
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[ObjectiveData.iParticipant].iteam].iRuleBitsetNine[ObjectiveData.iPriority], ciBS_RULE9_FINAL_RULE_RP_BONUS)
					GIVE_LOCAL_PLAYER_FINAL_RP_REWARD_BONUS()
				ENDIF
				
				IF ObjectiveData.iEntityID != -1
					INT i
					SWITCH ObjectiveData.iObjectiveID
					
						CASE JOB_COMP_PHOTO_LOC
						CASE JOB_COMP_ARRIVE_LOC
							PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT_PREREQ_SETTING(ciFMMC_ENTITY_LINK_TYPE_LOCATION, ObjectiveData.iEntityID)
						BREAK
						CASE JOB_COMP_PHOTO_PED
						CASE JOB_COMP_ARRIVE_PED
							PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT_PREREQ_SETTING(ciFMMC_ENTITY_LINK_TYPE_PED, ObjectiveData.iEntityID)
						BREAK
						CASE JOB_COMP_DELIVER_VEH
						CASE JOB_COMP_ARRIVE_VEH
						CASE JOB_COMP_PHOTO_VEH
							PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT_PREREQ_SETTING(ciFMMC_ENTITY_LINK_TYPE_VEHICLE, ObjectiveData.iEntityID)
						BREAK
						CASE JOB_COMP_ARRIVE_OBJ
						CASE JOB_COMP_PHOTO_OBJ
						CASE JOB_COMP_HACK_OBJ
							PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT_PREREQ_SETTING(ciFMMC_ENTITY_LINK_TYPE_OBJECT, ObjectiveData.iEntityID)
						BREAK
						CASE JOB_COMP_DELIVER_OBJ
							FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfObjects - 1
								IF IS_BIT_SET(ObjectiveData.iEntityID, i)
									PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT_PREREQ_SETTING(ciFMMC_ENTITY_LINK_TYPE_OBJECT, i)
								ENDIF
							ENDFOR
						BREAK
						CASE JOB_COMP_DELIVER_PED
							FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfPeds - 1
								IF FMMC_IS_LONG_BIT_SET(ObjectiveData.iBitsetArrayPassed, i)
									PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT_PREREQ_SETTING(ciFMMC_ENTITY_LINK_TYPE_PED, i)
								ENDIF
							ENDFOR
						BREAK
					ENDSWITCH
				ENDIF
			ENDIF

			IF NOT bIsLocalPlayerHost
				
				SWITCH ObjectiveData.iObjectiveID
					
					CASE JOB_COMP_ARRIVE_LOC
						
						IF ObjectiveData.iEntityID != -1
						AND MC_playerBD[ObjectiveData.iparticipant].iteam = MC_PlayerBD[ iPartToUse ].iTeam
							IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFIve, ciBEAST_SFX)
								STRING sSoundSet
								sSoundSet = GET_BEAST_MODE_SOUNDSET()
								PLAY_SOUND_FRONTEND(-1, "Beast_Checkpoint_NPC",sSoundSet, FALSE)
							ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciSOUND_ON_TEAMMATE_CHECKPOINT)
								IF ObjectiveData.iParticipant != iPartToUse
										PLAY_SOUND_FRONTEND(-1, "Checkpoint_Teammate", "GTAO_Shepherd_Sounds", FALSE)
								ENDIF
							ENDIF
							
							IF DOES_BLIP_EXIST(LocBlip[ObjectiveData.iEntityID])
								// url:bugstar:2185410
								IF DOES_BLIP_HAVE_GPS_ROUTE(LocBlip[ObjectiveData.iEntityID])
									SET_IGNORE_NO_GPS_FLAG(FALSE)
									PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT called SET_IGNORE_NO_GPS_FLAG FALSE (A)")
								ENDIF
							
								CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION] Removing blip for location ", ObjectiveData.iEntityID, " as a ARRIVE location job is complete.")
								REMOVE_BLIP(LocBlip[ObjectiveData.iEntityID])
							ENDIF
													
						ENDIF
						
					BREAK // End of JOB_COMP_ARRIVE_LOC
					
					CASE JOB_COMP_PHOTO_LOC
						IF ObjectiveData.iEntityID != -1
							IF DOES_BLIP_EXIST(LocBlip[ObjectiveData.iEntityID])
								CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION] Removing blip for location ", ObjectiveData.iEntityID, " as a PHOTO location job is complete.")
								REMOVE_BLIP(LocBlip[ObjectiveData.iEntityID])
							ENDIF
						ENDIF
						
					BREAK // End of JOB_COMP_PHOTO_LOC
					
					CASE JOB_COMP_ARRIVE_OBJ
					CASE JOB_COMP_PHOTO_OBJ
					CASE JOB_COMP_HACK_OBJ
						IF ObjectiveData.iEntityID != -1
							CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION] Removing blip for package ", ObjectiveData.iEntityID, " as an ARRIVE, PHOTO, or HACK package job is complete.")
							REMOVE_OBJECT_BLIP(ObjectiveData.iEntityID)
							
							IF ObjectiveData.iObjectiveID = JOB_COMP_HACK_OBJ
								IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[ObjectiveData.iEntityID].mn = HEI_P_ATTACHE_CASE_SHUT_S
									RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_MPHEIST/HEIST_CARD_SCANNER")
								ENDIF
							ENDIF
							
							IF ObjectiveData.iObjectiveID = JOB_COMP_ARRIVE_OBJ
								IF ObjectiveData.Details.FromPlayerIndex != LocalPlayer
								AND GET_DISTANCE_BETWEEN_PEDS(GET_PLAYER_PED(ObjectiveData.Details.FromPlayerIndex),LocalPlayerPed) <= 30.0
									PLAY_SOUND_FRONTEND(-1, "Friend_Pick_Up", "HUD_FRONTEND_MP_COLLECTABLE_SOUNDS", FALSE)
								ENDIF
							ENDIF
													
						ENDIF
					BREAK // End of JOB_COMP_ARRIVE_OBJ, JOB_COMP_PHOTO_OBJ and JOB_COMP_HACK_OBJ
					
					CASE JOB_COMP_ARRIVE_PED
					CASE JOB_COMP_PHOTO_PED
					
						IF ObjectiveData.iEntityID != -1
							IF DOES_BLIP_EXIST(biPedBlip[ObjectiveData.iEntityID])
								CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION] Removing blip for ped ", ObjectiveData.iEntityID, " as an ARRIVE, PHOTO, or CHARM ped job is complete.")
								REMOVE_BLIP(biPedBlip[ObjectiveData.iEntityID])
							ENDIF
						ENDIF
						
					BREAK // End of JOB_COMP_ARRIVE_PED and JOB_COMP_PHOTO_PED
					
					CASE JOB_COMP_ARRIVE_VEH
					CASE JOB_COMP_PHOTO_VEH
				
						IF ObjectiveData.iEntityID != -1
							IF DOES_BLIP_EXIST(biVehBlip[ObjectiveData.iEntityID])
							AND NOT SHOULD_VEHICLE_HAVE_CUSTOM_BLIP(ObjectiveData.iEntityID)
								CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION] Removing blip for vehicle ", ObjectiveData.iEntityID, " as an ARRIVE or PHOTO veh job is complete.")
								REMOVE_VEHICLE_BLIP(ObjectiveData.iEntityID)
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - JOB_COMP_ARRIVE_VEH objective complete removing blip for veh: ",ObjectiveData.iEntityID)
							ENDIF
						ENDIF
						
					BREAK // ENd of JOB_COMP_ARRIVE_VEH and JOB_COMP_PHOTO_VEH
					
					CASE JOB_COMP_DELIVER_OBJ
						
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciSHOW_LAST_DELIVERER_SHARD)
							piLastDeliverer = ObjectiveData.Details.FromPlayerIndex
						ENDIF
						
						FOR irepeat = 0 TO (FMMC_MAX_NUM_OBJECTS-1)
							IF NETWORK_DOES_NETWORK_ID_EXIST(GET_OBJECT_NET_ID(irepeat))
								IF IS_BIT_SET(ObjectiveData.iEntityID, irepeat)
									CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION] Removing blip for package ", irepeat, " as a delivery job is complete.")
									REMOVE_OBJECT_BLIP(irepeat)
								ENDIF
							ENDIF
						ENDFOR

					BREAK

					CASE JOB_COMP_DELIVER_PED
					
						FOR irepeat = 0 TO (FMMC_MAX_PEDS-1)
							IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[irepeat])
								IF NOT SHOULD_PED_HAVE_BLIP(irepeat)
									IF DOES_BLIP_EXIST(biPedBlip[irepeat])
										CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION] Removing blip for ped ", irepeat, " as a delivery job is complete.")
										REMOVE_BLIP(biPedBlip[irepeat])
									ENDIF
								ENDIF
							ENDIF
						ENDFOR
						
					BREAK // End of JOB_COMP_DELIVER_PED
					
					CASE JOB_COMP_DELIVER_VEH
						
						IF ObjectiveData.iEntityID != -1
														
							IF g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[ObjectiveData.iParticipant].iteam].iDropOffStopTimer[ObjectiveData.iPriority] = -1
								IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[ObjectiveData.iEntityID])
									VEHICLE_INDEX tempVeh
									tempVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[ObjectiveData.iEntityID])
									IF IS_PED_IN_VEHICLE(LocalPlayerPed, tempVeh)
										SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_PreventAutoShuffleToDriversSeat, TRUE)
										
										bStopDurationBlockSeatShuffle = TRUE
									ENDIF
								ENDIF
							ENDIF
							
							IF DOES_BLIP_EXIST(biVehBlip[ObjectiveData.iEntityID])
							AND NOT SHOULD_VEHICLE_HAVE_CUSTOM_BLIP(ObjectiveData.iEntityID)
								CDEBUG1LN(DEBUG_MC_BLIP, "[RCC MISSION] Removing blip for vehicle ", ObjectiveData.iEntityID, " as a delivery job is complete.")
								REMOVE_VEHICLE_BLIP(ObjectiveData.iEntityID)
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - JOB_COMP_DELIVER_VEH objective complete removing blip for veh: ",ObjectiveData.iEntityID) 
							ENDIF
							
						ENDIF
						
					BREAK // End of JOB_COMP_DELIVER_VEH
					
				ENDSWITCH
			
			ELSE // Else the person is the Host of this script, the above are not
				
				
				IF NOT IS_BIT_SET(MC_serverBD.iProcessJobCompBitset,ObjectiveData.iparticipant)
				
					SWITCH ObjectiveData.iObjectiveID
						
						// this is where the server will update its data based upon the player broadcast data MC_playerBD[iPartToUse].iObjectiveTypeCompleted
						
						CASE JOB_COMP_ARRIVE_LOC
							
							PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ************************************ ")
							PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - CASE = JOB_COMP_ARRIVE_LOC")
								
							IF ObjectiveData.iEntityID != -1
								
								ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam] = MC_serverBD_4.iGotoLocationDataPriority[ObjectiveData.iEntityID][MC_playerBD[ObjectiveData.iparticipant].iteam] 
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iPriority = ", ObjectiveData.iPriority)
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iparticipant = ", ObjectiveData.iparticipant)
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - MC_playerBD[ObjectiveData.iparticipant].iteam = ", MC_playerBD[ObjectiveData.iparticipant].iteam)
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam] = ", ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - MC_serverBD_4.iTimesRevalidatedObjectives[MC_playerBD[ObjectiveData.iparticipant].iteam] = ", MC_serverBD_4.iTimesRevalidatedObjectives[MC_playerBD[ObjectiveData.iparticipant].iteam])
								
								IF ObjectiveData.iPriority = ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam]
								AND MC_serverBD_4.iGotoLocationDataRule[ObjectiveData.iEntityID][MC_playerBD[ObjectiveData.iparticipant].iteam] = FMMC_OBJECTIVE_LOGIC_GO_TO
								AND ObjectiveData.iRevalidates = MC_serverBD_4.iTimesRevalidatedObjectives[MC_playerBD[ObjectiveData.iparticipant].iteam]
									
									PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iPriority = ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam]")
									
									IF ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam] < FMMC_MAX_RULES
										IF DOES_THIS_LOCATION_HAVE_A_SPECIFIC_ENTRY_HEADING( ObjectiveData.iEntityID )
										AND NOT IS_PARTICIPANT_FACING_CORRECT_DIRECTION_FOR_LOCATION( ObjectiveData.iEntityID, ObjectiveData.iparticipant )
											
											PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - iLoc: ", ObjectiveData.iEntityID, " In here (1)")
											
											IF NOT IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ ObjectiveData.iEntityID ].iLocBS2, ciLoc_BS2_DirectionRestrictPass )
												MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_LOCATE_WRONG_ANGLE
												SET_TEAM_OBJECTIVE_OUTCOME(iTeam, MC_serverBD_4.iCurrentHighestPriority[iTeam], FALSE)
												SET_LOCAL_OBJECTIVE_END_MESSAGE(iTeam, MC_serverBD_4.iCurrentHighestPriority[iTeam], MC_serverBD.iReasonForObjEnd[iTeam], TRUE)
												BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[iTeam], iTeam, MC_serverBD_4.iCurrentHighestPriority[iTeam], DID_TEAM_PASS_OBJECTIVE(iTeam, MC_serverBD_4.iCurrentHighestPriority[iTeam]), TRUE)
												SET_TEAM_FAILED(iTeam, mFail_LOCATE_WRONG_ANGLE)
											ENDIF
										ELSE // Else this is a valid locate :)
										
											PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - iLoc: ", ObjectiveData.iEntityID, " In here (2)")											
											
											IF NOT IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[MC_playerBD[ObjectiveData.iparticipant].iteam],ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
												PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - Setting Midpoint iObjectiveMidPointBitset for Team arrive loc ", MC_playerBD[ObjectiveData.iparticipant].iteam, " on Rule ", ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
												SET_BIT(MC_serverBD.iObjectiveMidPointBitset[MC_playerBD[ObjectiveData.iparticipant].iteam],ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
												SET_BIT(iLocalBoolCheck11, LBOOL11_HOST_APPLIED_NEW_MIDPOINT_MIDFRAME)
											ENDIF
										
											SET_TEAM_OBJECTIVE_OUTCOME(MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam],TRUE)
											
											MC_serverBD.iReasonForObjEnd[MC_playerBD[ObjectiveData.iparticipant].iteam] = OBJ_END_REASON_ARV_LOC
											
											MC_ServerBD.iLastCompletedGoToLocation = ObjectiveData.iEntityID
											
											PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_LOCATION,ObjectiveData.iEntityID,MC_playerBD[ObjectiveData.iparticipant].iteam,TRUE)
											
											IF IS_THIS_OBJECTIVE_OVER(ci_TARGET_LOCATION,MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
												PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - iLoc: ", ObjectiveData.iEntityID, " In here (3) - IS_THIS_OBJECTIVE_OVER")
												
												SET_LOCAL_OBJECTIVE_END_MESSAGE(MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam],MC_serverBD.iReasonForObjEnd[MC_playerBD[ObjectiveData.iparticipant].iteam])
												BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[MC_playerBD[ObjectiveData.iparticipant].iteam],MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam],DID_TEAM_PASS_OBJECTIVE(MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam]))
											ENDIF
											
											IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ObjectiveData.iEntityID].iWholeTeamAtLocation[MC_playerBD[ObjectiveData.iparticipant].iteam] != ciGOTO_LOCATION_INDIVIDUAL
												INCREMENT_SERVER_TEAM_SCORE(MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam],GET_FMMC_POINTS_FOR_TEAM(MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam]))
											ENDIF
											
											IF MC_playerBD[ObjectiveData.iparticipant].iteam = MC_PlayerBD[ iPartToUse ].iTeam
												IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciSOUND_ON_TEAMMATE_CHECKPOINT)
													IF ObjectiveData.iParticipant != iPartToUse
														PLAY_SOUND_FRONTEND(-1, "Checkpoint_Teammate", "GTAO_Shepherd_Sounds", FALSE)
													ENDIF
												ENDIF
												
												IF DOES_BLIP_EXIST(LocBlip[ObjectiveData.iEntityID])
													// url:bugstar:2185410
													IF DOES_BLIP_HAVE_GPS_ROUTE(LocBlip[ObjectiveData.iEntityID])
														SET_IGNORE_NO_GPS_FLAG(FALSE)
														PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT called SET_IGNORE_NO_GPS_FLAG FALSE (B)")
													ENDIF
												
													REMOVE_BLIP(LocBlip[ObjectiveData.iEntityID])
												ENDIF

											ENDIF

											IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ObjectiveData.iEntityID].iLocBS3, ciLoc_BS3_IncrementTimeRemaining)
												IF (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_ALL_TEAMS_SHARE_LOCATES)
												AND IS_PLAYER_IN_LOCATION(GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_NATIVE(PARTICIPANT_INDEX, ObjectiveData.iparticipant))), ObjectiveData.iEntityID, iTeam))
												OR NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_ALL_TEAMS_SHARE_LOCATES)
													INT iIncrementTime
													iIncrementTime = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ObjectiveData.iEntityID].iIncrementTime * 1000
												
													BROADCAST_FMMC_INCREMENT_REMAINING_TIME(MC_playerBD[ObjectiveData.iparticipant].iteam, iIncrementTime, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ObjectiveData.iEntityID].iLocBS3, ciLoc_BS3_IncrementTimeTeamOnly), ObjectiveData.iparticipant)
												ENDIF
											ENDIF
											
											tempPart = INT_TO_PARTICIPANTINDEX(ObjectiveData.iparticipant)
											
											IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
												tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
												IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ObjectiveData.iEntityID].iWholeTeamAtLocation[MC_playerBD[ObjectiveData.iparticipant].iteam] = ciGOTO_LOCATION_INDIVIDUAL
													INCREMENT_SERVER_REMOTE_PLAYER_SCORE(tempPlayer, GET_FMMC_POINTS_FOR_TEAM(MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam]),MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
												ENDIF
											ENDIF
											
											SET_CUTSCENE_PRIMARY_PLAYER(ObjectiveData.iparticipant, MC_playerBD[ObjectiveData.iparticipant].iteam)

											PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - MC_playerBD[iparticipant].iCurrentLoc : ",ObjectiveData.iEntityID)
											PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - server processing JOB_COMP_ARRIVE_LOC from part: ",ObjectiveData.iparticipant)
										ENDIF
									ENDIF
								#IF IS_DEBUG_BUILD	
								ELSE
									
									PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - priorities or revalidates don't match up!")
								
								#ENDIF
								
								ENDIF
								
							ENDIF
							
						BREAK // End of JOB_COMP_ARRIVE_LOC
						
						
						CASE JOB_COMP_ARRIVE_PED
						
							IF ObjectiveData.iEntityID != -1
								
								ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam] = MC_serverBD_4.iPedPriority[ObjectiveData.iEntityID][MC_playerBD[ObjectiveData.iparticipant].iteam]
								
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ************************************ ")
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - CASE = JOB_COMP_ARRIVE_PED")
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iPriority = ", ObjectiveData.iPriority)
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iparticipant = ", ObjectiveData.iparticipant)
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - MC_playerBD[ObjectiveData.iparticipant].iteam = ", MC_playerBD[ObjectiveData.iparticipant].iteam)
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam] = ", ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - MC_serverBD_4.iTimesRevalidatedObjectives[MC_playerBD[ObjectiveData.iparticipant].iteam] = ", MC_serverBD_4.iTimesRevalidatedObjectives[MC_playerBD[ObjectiveData.iparticipant].iteam])
								
								IF ObjectiveData.iPriority = ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam]
								AND MC_serverBD_4.iPedRule[ObjectiveData.iEntityID][MC_playerBD[ObjectiveData.iparticipant].iteam] = FMMC_OBJECTIVE_LOGIC_GO_TO
								AND ObjectiveData.iRevalidates = MC_serverBD_4.iTimesRevalidatedObjectives[MC_playerBD[ObjectiveData.iparticipant].iteam]
									
									PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iPriority = ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam]")
									
									IF ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam] < FMMC_MAX_RULES
										
										IF NOT IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[MC_playerBD[ObjectiveData.iparticipant].iteam],ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
											PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - Setting Midpoint iObjectiveMidPointBitset for Team arrive ped ", MC_playerBD[ObjectiveData.iparticipant].iteam, " on Rule ", ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
											SET_BIT(MC_serverBD.iObjectiveMidPointBitset[MC_playerBD[ObjectiveData.iparticipant].iteam],ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
											SET_BIT(iLocalBoolCheck11, LBOOL11_HOST_APPLIED_NEW_MIDPOINT_MIDFRAME)
										ENDIF
										
										SET_TEAM_OBJECTIVE_OUTCOME(MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam],TRUE)
										MC_serverBD.iReasonForObjEnd[MC_playerBD[ObjectiveData.iparticipant].iteam] = OBJ_END_REASON_ARV_PED
										
										PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_PED,ObjectiveData.iEntityID,MC_playerBD[ObjectiveData.iparticipant].iteam,TRUE)
										
										IF IS_THIS_OBJECTIVE_OVER(ci_TARGET_PED,MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
											SET_LOCAL_OBJECTIVE_END_MESSAGE(MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam],MC_serverBD.iReasonForObjEnd[MC_playerBD[ObjectiveData.iparticipant].iteam])
											BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[MC_playerBD[ObjectiveData.iparticipant].iteam],MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam],DID_TEAM_PASS_OBJECTIVE(MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam]))
										ENDIF
										
										IF NOT SHOULD_PED_HAVE_BLIP(ObjectiveData.iEntityID)
											IF DOES_BLIP_EXIST(biPedBlip[ObjectiveData.iEntityID])
												REMOVE_BLIP(biPedBlip[ObjectiveData.iEntityID])
											ENDIF
										ENDIF

										PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iEntityID: ",ObjectiveData.iEntityID)
										PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - server processing JOB_COMP_ARRIVE_PED from part: ",ObjectiveData.iparticipant)
										
									ENDIF
									
								#IF IS_DEBUG_BUILD	
								ELSE
									
									PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - priorities or revalidates don't match up!")
								
								#ENDIF
									
								ENDIF
								
							ENDIF
							
						BREAK // End of JOB_COMP_ARRIVE_PED
						
						CASE JOB_COMP_ARRIVE_VEH
						
							IF ObjectiveData.iEntityID != -1
							
								ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam] = MC_serverBD_4.iVehPriority[ObjectiveData.iEntityID][MC_playerBD[ObjectiveData.iparticipant].iteam]
								
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ************************************ ")
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - CASE = JOB_COMP_ARRIVE_VEH")
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iPriority = ", ObjectiveData.iPriority)
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iparticipant = ", ObjectiveData.iparticipant)
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - MC_playerBD[ObjectiveData.iparticipant].iteam = ", MC_playerBD[ObjectiveData.iparticipant].iteam)
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam] = ", ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - MC_serverBD_4.iTimesRevalidatedObjectives[MC_playerBD[ObjectiveData.iparticipant].iteam] = ", MC_serverBD_4.iTimesRevalidatedObjectives[MC_playerBD[ObjectiveData.iparticipant].iteam])
								
								IF ObjectiveData.iPriority = ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam]
								AND MC_serverBD_4.ivehRule[ObjectiveData.iEntityID][MC_playerBD[ObjectiveData.iparticipant].iteam] = FMMC_OBJECTIVE_LOGIC_GO_TO
								AND ObjectiveData.iRevalidates = MC_serverBD_4.iTimesRevalidatedObjectives[MC_playerBD[ObjectiveData.iparticipant].iteam]
									
									PRINTLN("[RCC MISSION] - PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT: ObjectiveData.iPriority = ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam]")
									
									IF ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam] < FMMC_MAX_RULES
										//url:bugstar:3219654
										INCREMENT_SERVER_REMOTE_PLAYER_SCORE(ObjectiveData.Details.FromPlayerIndex, GET_FMMC_POINTS_FOR_TEAM(MC_playerBD[ObjectiveData.iparticipant].iteam, ObjectiveData.iPriority), MC_playerBD[ObjectiveData.iparticipant].iteam, ObjectiveData.iPriority)
										IF NOT IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[MC_playerBD[ObjectiveData.iparticipant].iteam],ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
											PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - Setting Midpoint iObjectiveMidPointBitset arrive veh for Team: ", MC_playerBD[ObjectiveData.iparticipant].iteam, " on Rule ", ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
											SET_BIT(MC_serverBD.iObjectiveMidPointBitset[MC_playerBD[ObjectiveData.iparticipant].iteam],ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
											SET_BIT(iLocalBoolCheck11, LBOOL11_HOST_APPLIED_NEW_MIDPOINT_MIDFRAME)
										ENDIF
										
										INT iRule
										iRule = MC_serverBD_4.ivehRule[ObjectiveData.iEntityID][MC_playerBD[ObjectiveData.iparticipant].iteam]
										
										BOOL bCheckOnRule
										bCheckOnRule = TRUE
										INT iTeamBS
										iTeamBS = GET_VEHICLE_GOTO_TEAMS(MC_playerBD[ObjectiveData.iparticipant].iteam,ObjectiveData.iEntityID,bCheckOnRule)
										
										FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
											ipriority[iteam] = MC_serverBD_4.iVehPriority[ObjectiveData.iEntityID][iTeam]
											IF ipriority[iteam] < FMMC_MAX_RULES
											AND ((NOT bCheckOnRule) OR (ipriority[iteam] <= MC_serverBD_4.iCurrentHighestPriority[iTeam]))
											AND MC_serverBD_4.ivehRule[ObjectiveData.iEntityID][iTeam] = iRule
												IF IS_BIT_SET(iTeamBS,iTeam)
												OR (iTeam = MC_playerBD[ObjectiveData.iparticipant].iteam)
													SET_TEAM_OBJECTIVE_OUTCOME(iTeam,ipriority[iteam],TRUE)
													MC_serverBD.iReasonForObjEnd[iteam] = OBJ_END_REASON_ARV_VEH
													PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_VEHICLE,ObjectiveData.iEntityID,iTeam,TRUE)
													
													IF (iTeam != MC_playerBD[ObjectiveData.iparticipant].iteam)
													AND NOT bCheckOnRule
														INCREMENT_SERVER_TEAM_SCORE(iteam,ipriority[iTeam],GET_FMMC_POINTS_FOR_TEAM(iteam,ipriority[iTeam]))
													ENDIF
													
												ENDIF
											ENDIF
										ENDFOR
										
										IF IS_THIS_OBJECTIVE_OVER(ci_TARGET_VEHICLE,MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
											SET_LOCAL_OBJECTIVE_END_MESSAGE(MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam],MC_serverBD.iReasonForObjEnd[MC_playerBD[ObjectiveData.iparticipant].iteam])
											BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[MC_playerBD[ObjectiveData.iparticipant].iteam],MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam],DID_TEAM_PASS_OBJECTIVE(MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam]))
										ENDIF
										
										IF DOES_BLIP_EXIST(biVehBlip[ObjectiveData.iEntityID])
										AND NOT SHOULD_VEHICLE_HAVE_CUSTOM_BLIP(ObjectiveData.iEntityID)
											REMOVE_VEHICLE_BLIP(ObjectiveData.iEntityID)
											PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - JOB_COMP_ARRIVE_VEH objective complete removing blip for veh HOST: ",ObjectiveData.iEntityID)
										ENDIF
										
										PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iEntityID: ",ObjectiveData.iEntityID) 
										PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - server processing JOB_COMP_ARRIVE_VEH from part: ",ObjectiveData.iparticipant) 
									
									ENDIF
									
								#IF IS_DEBUG_BUILD	
								ELSE
									
									PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - priorities or revalidates don't match up!")
								
								#ENDIF
									
								ENDIF
								
							ENDIF
							
						BREAK // End of JOB_COMP_ARRIVE_VEH
						
						CASE JOB_COMP_ARRIVE_OBJ
						
							IF ObjectiveData.iEntityID != -1
							
								ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam] = MC_serverBD_4.iObjPriority[ObjectiveData.iEntityID][MC_playerBD[ObjectiveData.iparticipant].iteam] 
								
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ************************************ ")
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - CASE = JOB_COMP_ARRIVE_OBJ")
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iPriority = ", ObjectiveData.iPriority)
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iparticipant = ", ObjectiveData.iparticipant)
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iEntityID = ", ObjectiveData.iEntityID)
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - MC_playerBD[ObjectiveData.iparticipant].iteam = ", MC_playerBD[ObjectiveData.iparticipant].iteam)
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam] = ", ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - MC_serverBD_4.iTimesRevalidatedObjectives[MC_playerBD[ObjectiveData.iparticipant].iteam] = ", MC_serverBD_4.iTimesRevalidatedObjectives[MC_playerBD[ObjectiveData.iparticipant].iteam])
								
								IF ObjectiveData.iPriority = ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam]
								AND MC_serverBD_4.iObjRule[ObjectiveData.iEntityID][MC_playerBD[ObjectiveData.iparticipant].iteam] = FMMC_OBJECTIVE_LOGIC_GO_TO
								AND ObjectiveData.iRevalidates = MC_serverBD_4.iTimesRevalidatedObjectives[MC_playerBD[ObjectiveData.iparticipant].iteam]
									
									PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iPriority = ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam]")
									
									IF ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam] < FMMC_MAX_RULES
										
										IF NOT IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[MC_playerBD[ObjectiveData.iparticipant].iteam],ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
											PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - Setting Midpoint iObjectiveMidPointBitset for Team arrive obj ", MC_playerBD[ObjectiveData.iparticipant].iteam, " on Rule ", ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
											SET_BIT(MC_serverBD.iObjectiveMidPointBitset[MC_playerBD[ObjectiveData.iparticipant].iteam],ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
											SET_BIT(iLocalBoolCheck11, LBOOL11_HOST_APPLIED_NEW_MIDPOINT_MIDFRAME)
										ENDIF
										
										IF NETWORK_DOES_NETWORK_ID_EXIST(GET_OBJECT_NET_ID(ObjectiveData.iEntityID))
										AND SHOULD_OBJ_CLEAN_UP_ON_DELIVERY(ObjectiveData.iEntityID, MC_playerBD[ObjectiveData.iparticipant].iteam)
											PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - JOB_COMP_ARRIVE_OBJ object should clean up!")
											IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(GET_OBJECT_NET_ID(ObjectiveData.iEntityID))
												DELETE_NET_ID(MC_serverBD_1.sFMMC_SBD.niNetworkedObject[GET_OBJECT_NET_ID_INDEX(ObjectiveData.iEntityID)])
											ELSE
												CLEANUP_NET_ID(MC_serverBD_1.sFMMC_SBD.niNetworkedObject[GET_OBJECT_NET_ID_INDEX(ObjectiveData.iEntityID)])
											ENDIF
										ENDIF
										
										IF ObjectiveData.Details.FromPlayerIndex != LocalPlayer
										AND GET_DISTANCE_BETWEEN_PEDS(GET_PLAYER_PED(ObjectiveData.Details.FromPlayerIndex),LocalPlayerPed) <= 30.0
											PLAY_SOUND_FRONTEND(-1, "Friend_Pick_Up", "HUD_FRONTEND_MP_COLLECTABLE_SOUNDS", FALSE)
										ENDIF
										
										IF MC_serverBD_2.iCurrentObjRespawnLives[ObjectiveData.iEntityID] <= 0
											IF SHOULD_OBJ_CLEAN_UP_ON_DELIVERY(ObjectiveData.iEntityID,MC_playerBD[ObjectiveData.iparticipant].iteam)
												FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
													IF iteam = MC_playerBD[ObjectiveData.iparticipant].iteam
														IF IS_BIT_SET(MC_serverBD.iObjteamFailBitset[ObjectiveData.iEntityID],iteam)
															CLEAR_BIT(MC_serverBD.iObjteamFailBitset[ObjectiveData.iEntityID],iteam)
															CLEAR_BIT(MC_serverBD.iMissionCriticalObj[iteam], ObjectiveData.iEntityID)
															bwasobjective[iTeam] = TRUE
														ENDIF
													ELSE
														IF DOES_TEAM_LIKE_TEAM(iteam,MC_playerBD[ObjectiveData.iparticipant].iteam)
															IF IS_BIT_SET(MC_serverBD.iObjteamFailBitset[ObjectiveData.iEntityID],iteam)
																CLEAR_BIT(MC_serverBD.iObjteamFailBitset[ObjectiveData.iEntityID],iteam)
																CLEAR_BIT(MC_serverBD.iMissionCriticalObj[iteam], ObjectiveData.iEntityID)
																bwasobjective[iTeam] =TRUE
															ENDIF
														ENDIF
													ENDIF
												ENDFOR
											ENDIF
											
											FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
												
												ipriority[iteam] = MC_serverBD_4.iObjPriority[ObjectiveData.iEntityID][iteam]
												
												IF iteam != MC_playerBD[ObjectiveData.iparticipant].iteam
													
													IF SHOULD_OBJ_CLEAN_UP_ON_DELIVERY(ObjectiveData.iEntityID,MC_playerBD[ObjectiveData.iparticipant].iteam)
														IF IS_BIT_SET(MC_serverBD.iObjteamFailBitset[ObjectiveData.iEntityID],iteam)
															IF ipriority[iteam] < FMMC_MAX_RULES
																IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iteam].iTeamFailBitset,ipriority[iteam])
																	bobjectivefails[iTeam] = TRUE
																	SET_TEAM_FAILED(iTeam,mFail_ARV_OBJ)
																	MC_serverBD.iEntityCausingFail[iteam] = ObjectiveData.iEntityID
																	PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - MISSION FAILED FOR TEAM:",iteam," BECAUSE OBJ GONE TO: ",ObjectiveData.iEntityID)
																ENDIF
															ENDIF
															bwasobjective[iTeam] = TRUE
															CLEAR_BIT(MC_serverBD.iObjteamFailBitset[ObjectiveData.iEntityID],iteam)
														ENDIF
													ENDIF
													
													PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - team ", iTeam, " - obj's primary rule: ",MC_serverBD_4.iObjPriority[ObjectiveData.iEntityID][iTeam])
													PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - team ", iTeam, " - MC_serverBD_4.iCurrentHighestPriority[iTeam]: ",MC_serverBD_4.iCurrentHighestPriority[iTeam])
													PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iPriority = ", ObjectiveData.iPriority)
													
													IF ( ipriority[iTeam] = MC_serverBD_4.iCurrentHighestPriority[iTeam]
														OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[ObjectiveData.iEntityID].iObjectBitSet, cibsOBJ_InvalidateAllTeams) )
													AND ipriority[iteam] < FMMC_MAX_RULES
														IF DOES_TEAM_LIKE_TEAM(iteam,MC_playerBD[ObjectiveData.iparticipant].iteam)
															IF MC_serverBD_4.iObjRule[ObjectiveData.iEntityID][iteam] = FMMC_OBJECTIVE_LOGIC_PROTECT
																PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - Protect Obj rule adding points = ", GET_FMMC_POINTS_FOR_TEAM(iteam,ipriority[iTeam]))
																INCREMENT_SERVER_TEAM_SCORE(iteam,ipriority[iTeam],GET_FMMC_POINTS_FOR_TEAM(iteam,ipriority[iTeam]))
																//BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_TEAM_PRO_OBJ,0,iteam,-1,INVALID_PLAYER_INDEX(),ci_TARGET_OBJECT,ObjectiveData.iEntityID)
															ENDIF
															SET_TEAM_OBJECTIVE_OUTCOME(iTeam,ipriority[iteam], TRUE)
															PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_OBJECT,ObjectiveData.iEntityID,iTeam,TRUE)	
														ELSE
															SET_TEAM_OBJECTIVE_OUTCOME(iTeam,ipriority[iteam], FALSE)
															PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_OBJECT,ObjectiveData.iEntityID,iTeam,FALSE)
														ENDIF
														MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_ARV_OBJ
													ENDIF
												ELSE
													PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ipriority[iteam] team ", iTeam, " - priority: ",ipriority[iteam])
													PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iPriority = ", ObjectiveData.iPriority)
													
													SET_TEAM_OBJECTIVE_OUTCOME(iTeam,ipriority[iteam], TRUE)
													PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_OBJECT,ObjectiveData.iEntityID,iTeam,TRUE)	
													MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_ARV_OBJ
												ENDIF
												
											ENDFOR
											
										ENDIF
										
										FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
											IF bwasobjective[iTeam]
												IF IS_THIS_OBJECTIVE_OVER(ci_TARGET_OBJECT,iteam,ipriority[iteam])
													SET_LOCAL_OBJECTIVE_END_MESSAGE(iTeam,ipriority[iteam],MC_serverBD.iReasonForObjEnd[iteam],bobjectivefails[iTeam])
													BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[iteam],iteam,ipriority[iteam],DID_TEAM_PASS_OBJECTIVE(iteam,ipriority[iteam]),bobjectivefails[iTeam])
												ENDIF
											ENDIF
										ENDFOR
										
										REMOVE_OBJECT_BLIP(ObjectiveData.iEntityID)
										
										SET_CUTSCENE_PRIMARY_PLAYER(ObjectiveData.iparticipant, MC_playerBD[ObjectiveData.iparticipant].iteam)
										
										PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iEntityID: ",ObjectiveData.iEntityID) 
										PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - server processing JOB_COMP_ARRIVE_OBJ from part: ",ObjectiveData.iparticipant)
										
									ENDIF
									
								#IF IS_DEBUG_BUILD	
								ELSE
									
									PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - priorities or revalidates don't match up!")
								
								#ENDIF
									
								ENDIF
								
							ENDIF
							
						BREAK // End of JOB_COMP_ARRIVE_OBJ
						
						CASE JOB_COMP_PHOTO_LOC
						
							IF ObjectiveData.iEntityID != -1
								
								ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam] = MC_serverBD_4.iGotoLocationDataPriority[ObjectiveData.iEntityID][MC_playerBD[ObjectiveData.iparticipant].iteam] 
								
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ************************************ ")
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - CASE = JOB_COMP_PHOTO_LOC")
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iPriority = ", ObjectiveData.iPriority)
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iparticipant = ", ObjectiveData.iparticipant)
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - MC_playerBD[ObjectiveData.iparticipant].iteam = ", MC_playerBD[ObjectiveData.iparticipant].iteam)
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam] = ", ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - MC_serverBD_4.iTimesRevalidatedObjectives[MC_playerBD[ObjectiveData.iparticipant].iteam] = ", MC_serverBD_4.iTimesRevalidatedObjectives[MC_playerBD[ObjectiveData.iparticipant].iteam])
								
								IF ObjectiveData.iPriority = ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam]
								AND MC_serverBD_4.iGotoLocationDataRule[ObjectiveData.iEntityID][MC_playerBD[ObjectiveData.iparticipant].iteam] = FMMC_OBJECTIVE_LOGIC_PHOTO
								AND ObjectiveData.iRevalidates = MC_serverBD_4.iTimesRevalidatedObjectives[MC_playerBD[ObjectiveData.iparticipant].iteam]
									
									PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iPriority = ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam]")
									
									IF ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam] < FMMC_MAX_RULES
										
										IF NOT IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[MC_playerBD[ObjectiveData.iparticipant].iteam],ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
											PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - Setting Midpoint iObjectiveMidPointBitset for Team photo loc ", MC_playerBD[ObjectiveData.iparticipant].iteam, " on Rule ", ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
											SET_BIT(MC_serverBD.iObjectiveMidPointBitset[MC_playerBD[ObjectiveData.iparticipant].iteam],ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
											SET_BIT(iLocalBoolCheck11, LBOOL11_HOST_APPLIED_NEW_MIDPOINT_MIDFRAME)
										ENDIF
										
										SET_TEAM_OBJECTIVE_OUTCOME(MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam], TRUE)
										MC_serverBD.iReasonForObjEnd[MC_playerBD[ObjectiveData.iparticipant].iteam] = OBJ_END_REASON_PHOTO_LOC

										PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_LOCATION,ObjectiveData.iEntityID,MC_playerBD[ObjectiveData.iparticipant].iteam,TRUE)
										
										IF IS_THIS_OBJECTIVE_OVER(ci_TARGET_LOCATION,MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
											SET_LOCAL_OBJECTIVE_END_MESSAGE(MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam],MC_serverBD.iReasonForObjEnd[MC_playerBD[ObjectiveData.iparticipant].iteam])
											BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[MC_playerBD[ObjectiveData.iparticipant].iteam],MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam],DID_TEAM_PASS_OBJECTIVE(MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam]))
										ENDIF
										
										IF DOES_BLIP_EXIST(LocBlip[ObjectiveData.iEntityID])
											REMOVE_BLIP(LocBlip[ObjectiveData.iEntityID])
										ENDIF
										
										IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[ObjectiveData.iparticipant].iteam].iRuleBitset[ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam]], ciBS_RULE_INVALIDATE_OTHER_TEAMS)
											FOR iteamrepeat = 0 TO (FMMC_MAX_TEAMS-1)
												IF iteamrepeat != MC_playerBD[ObjectiveData.iparticipant].iteam
													IF MC_serverBD_4.iGotoLocationDataRule[ObjectiveData.iEntityID][iteamrepeat] = FMMC_OBJECTIVE_LOGIC_PHOTO
														PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_LOCATION,ObjectiveData.iEntityID,iteamrepeat,TRUE)
														PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - photo loc iInvalidateTargetForAll call for team: ",iteamrepeat)
													ENDIF
												ENDIF
											ENDFOR
										ENDIF

										PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - MC_playerBD[iparticipant].iLocPhoto : ",ObjectiveData.iEntityID) 
										PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - server processing JOB_COMP_PHOTO_LOC from part: ",ObjectiveData.iparticipant)
									
									ENDIF
									
								#IF IS_DEBUG_BUILD	
								ELSE
									
									PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - priorities or revalidates don't match up!")
								
								#ENDIF
									
								ENDIF
								
							ENDIF
							
						BREAK // End of JOB_COMP_PHOTO_LOC
						
						CASE JOB_COMP_PHOTO_PED
						
							IF ObjectiveData.iEntityID != -1
							
								ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam] = MC_serverBD_4.iPedPriority[ObjectiveData.iEntityID][MC_playerBD[ObjectiveData.iparticipant].iteam]
								
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ************************************ ")
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - CASE = JOB_COMP_PHOTO_PED")
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iPriority = ", ObjectiveData.iPriority)
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iparticipant = ", ObjectiveData.iparticipant)
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - MC_playerBD[ObjectiveData.iparticipant].iteam = ", MC_playerBD[ObjectiveData.iparticipant].iteam)
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam] = ", ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - MC_serverBD_4.iTimesRevalidatedObjectives[MC_playerBD[ObjectiveData.iparticipant].iteam] = ", MC_serverBD_4.iTimesRevalidatedObjectives[MC_playerBD[ObjectiveData.iparticipant].iteam])
								
								IF ObjectiveData.iPriority = ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam]
								AND MC_serverBD_4.iPedRule[ObjectiveData.iEntityID][MC_playerBD[ObjectiveData.iparticipant].iteam] = FMMC_OBJECTIVE_LOGIC_PHOTO
								AND ObjectiveData.iRevalidates = MC_serverBD_4.iTimesRevalidatedObjectives[MC_playerBD[ObjectiveData.iparticipant].iteam]
									
									PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iPriority = ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam]")
									
									IF ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam] < FMMC_MAX_RULES
									
										IF NOT IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[MC_playerBD[ObjectiveData.iparticipant].iteam],ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
											PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - Setting Midpoint iObjectiveMidPointBitset for Team photo ped ", MC_playerBD[ObjectiveData.iparticipant].iteam, " on Rule ", ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
											SET_BIT(MC_serverBD.iObjectiveMidPointBitset[MC_playerBD[ObjectiveData.iparticipant].iteam],ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
											SET_BIT(iLocalBoolCheck11, LBOOL11_HOST_APPLIED_NEW_MIDPOINT_MIDFRAME)
										ENDIF
									
										SET_TEAM_OBJECTIVE_OUTCOME(MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam],TRUE)
										MC_serverBD.iReasonForObjEnd[MC_playerBD[ObjectiveData.iparticipant].iteam] = OBJ_END_REASON_PHOTO_PED
										
										PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_PED,ObjectiveData.iEntityID,MC_playerBD[ObjectiveData.iparticipant].iteam,TRUE)
										
										IF IS_THIS_OBJECTIVE_OVER(ci_TARGET_PED,MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
											SET_LOCAL_OBJECTIVE_END_MESSAGE(MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam],MC_serverBD.iReasonForObjEnd[MC_playerBD[ObjectiveData.iparticipant].iteam])
											BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[MC_playerBD[ObjectiveData.iparticipant].iteam],MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam],DID_TEAM_PASS_OBJECTIVE(MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam]))
										ENDIF
									
										IF DOES_BLIP_EXIST(biPedBlip[ObjectiveData.iEntityID])
											REMOVE_BLIP(biPedBlip[ObjectiveData.iEntityID])
										ENDIF

										FMMC_CLEAR_LONG_BIT(MC_serverBD.iDeadPedPhotoBitset[MC_playerBD[ObjectiveData.iparticipant].iteam], ObjectiveData.iEntityID)
										
										IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[ObjectiveData.iparticipant].iteam].iRuleBitset[ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam]], ciBS_RULE_INVALIDATE_OTHER_TEAMS)
											FOR iteamrepeat = 0 TO (FMMC_MAX_TEAMS-1)
												IF iteamrepeat != MC_playerBD[ObjectiveData.iparticipant].iteam
													IF MC_serverBD_4.iPedRule[ObjectiveData.iEntityID][iteamrepeat] = FMMC_OBJECTIVE_LOGIC_PHOTO
														PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_PED,ObjectiveData.iEntityID,iteamrepeat,TRUE)
														FMMC_CLEAR_LONG_BIT(MC_serverBD.iDeadPedPhotoBitset[iteamrepeat], ObjectiveData.iEntityID)
														PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - photo ped iInvalidateTargetForAll call for team: ",iteamrepeat)
													ENDIF
												ENDIF
											ENDFOR
										ENDIF
										
										PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iEntityID: ",ObjectiveData.iEntityID) 
										PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - server processing JOB_COMP_PHOTO_PED from part: ",ObjectiveData.iparticipant)
									ENDIF
									
								#IF IS_DEBUG_BUILD	
								ELSE
									
									PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - priorities or revalidates don't match up!")
								
								#ENDIF
								
								ENDIF
								
							ENDIF
							
						BREAK // End of JOB_COMP_PHOTO_PED
						
						CASE JOB_COMP_PHOTO_VEH
						
							IF ObjectiveData.iEntityID != -1

								ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam] = MC_serverBD_4.iVehPriority[ObjectiveData.iEntityID][MC_playerBD[ObjectiveData.iparticipant].iteam]
								
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ************************************ ")
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - CASE = JOB_COMP_PHOTO_VEH")
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iPriority = ", ObjectiveData.iPriority)
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iparticipant = ", ObjectiveData.iparticipant)
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - MC_playerBD[ObjectiveData.iparticipant].iteam = ", MC_playerBD[ObjectiveData.iparticipant].iteam)
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam] = ", ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - MC_serverBD_4.iTimesRevalidatedObjectives[MC_playerBD[ObjectiveData.iparticipant].iteam] = ", MC_serverBD_4.iTimesRevalidatedObjectives[MC_playerBD[ObjectiveData.iparticipant].iteam])
								
								IF ObjectiveData.iPriority = ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam]
								AND MC_serverBD_4.ivehRule[ObjectiveData.iEntityID][MC_playerBD[ObjectiveData.iparticipant].iteam] = FMMC_OBJECTIVE_LOGIC_PHOTO
								AND ObjectiveData.iRevalidates = MC_serverBD_4.iTimesRevalidatedObjectives[MC_playerBD[ObjectiveData.iparticipant].iteam]
									
									PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iPriority = ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam]")
									
									IF ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam] < FMMC_MAX_RULES
										
										IF NOT IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[MC_playerBD[ObjectiveData.iparticipant].iteam],ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
											PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - Setting Midpoint iObjectiveMidPointBitset for Team photo veh ", MC_playerBD[ObjectiveData.iparticipant].iteam, " on Rule ", ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
											SET_BIT(MC_serverBD.iObjectiveMidPointBitset[MC_playerBD[ObjectiveData.iparticipant].iteam],ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
											SET_BIT(iLocalBoolCheck11, LBOOL11_HOST_APPLIED_NEW_MIDPOINT_MIDFRAME)
										ENDIF
										
										tempPart = INT_TO_PARTICIPANTINDEX(ObjectiveData.iparticipant)
											
										IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
											tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
										
											INCREMENT_SERVER_REMOTE_PLAYER_SCORE(tempPlayer, GET_FMMC_POINTS_FOR_TEAM(MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam]), MC_playerBD[ObjectiveData.iparticipant].iteam, ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
										ENDIF
										
										SET_TEAM_OBJECTIVE_OUTCOME(MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam],TRUE)
										MC_serverBD.iReasonForObjEnd[MC_playerBD[ObjectiveData.iparticipant].iteam] = OBJ_END_REASON_PHOTO_VEH
										
										PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_VEHICLE,ObjectiveData.iEntityID,MC_playerBD[ObjectiveData.iparticipant].iteam,TRUE)
										
										IF IS_THIS_OBJECTIVE_OVER(ci_TARGET_VEHICLE,MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
											SET_LOCAL_OBJECTIVE_END_MESSAGE(MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam],MC_serverBD.iReasonForObjEnd[MC_playerBD[ObjectiveData.iparticipant].iteam])
											BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[MC_playerBD[ObjectiveData.iparticipant].iteam],MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam],DID_TEAM_PASS_OBJECTIVE(MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam]))
										ENDIF
										
										IF DOES_BLIP_EXIST(biVehBlip[ObjectiveData.iEntityID])
										AND NOT SHOULD_VEHICLE_HAVE_CUSTOM_BLIP(ObjectiveData.iEntityID)
											REMOVE_VEHICLE_BLIP(ObjectiveData.iEntityID)
											PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - JOB_COMP_PHOTO_VEH objective complete removing blip for veh HOST: ",ObjectiveData.iEntityID)
										ENDIF
										
										IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[ObjectiveData.iparticipant].iteam].iRuleBitset[ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam]], ciBS_RULE_INVALIDATE_OTHER_TEAMS)
											FOR iteamrepeat = 0 TO (FMMC_MAX_TEAMS-1)
												IF iteamrepeat != MC_playerBD[ObjectiveData.iparticipant].iteam
													IF MC_serverBD_4.ivehRule[ObjectiveData.iEntityID][iteamrepeat] = FMMC_OBJECTIVE_LOGIC_PHOTO
														PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_VEHICLE,ObjectiveData.iEntityID,iteamrepeat,TRUE)
														PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - photo veh iInvalidateTargetForAll call for team: ",iteamrepeat)
													ENDIF
												ENDIF
											ENDFOR
										ENDIF

										PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iEntityID: ",ObjectiveData.iEntityID)
										PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - server processing JOB_COMP_PHOTO_VEH from part: ",ObjectiveData.iparticipant)
									ENDIF
									
								#IF IS_DEBUG_BUILD	
								ELSE
									
									PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - priorities or revalidates don't match up!")
								
								#ENDIF	
								
								ENDIF
								
							ENDIF
							
						BREAK // End of JOB_COMP_PHOTO_VEH
						
						CASE JOB_COMP_PHOTO_OBJ
						
							IF ObjectiveData.iEntityID != -1
								
								ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam] = MC_serverBD_4.iObjPriority[ObjectiveData.iEntityID][MC_playerBD[ObjectiveData.iparticipant].iteam] 
								
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ************************************ ")
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - CASE = JOB_COMP_PHOTO_OBJ")
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iPriority = ", ObjectiveData.iPriority)
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iparticipant = ", ObjectiveData.iparticipant)
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - MC_playerBD[ObjectiveData.iparticipant].iteam = ", MC_playerBD[ObjectiveData.iparticipant].iteam)
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam] = ", ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - MC_serverBD_4.iTimesRevalidatedObjectives[MC_playerBD[ObjectiveData.iparticipant].iteam] = ", MC_serverBD_4.iTimesRevalidatedObjectives[MC_playerBD[ObjectiveData.iparticipant].iteam])
								
								IF ObjectiveData.iPriority = ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam]
								AND MC_serverBD_4.iObjRule[ObjectiveData.iEntityID][MC_playerBD[ObjectiveData.iparticipant].iteam] = FMMC_OBJECTIVE_LOGIC_PHOTO
								AND ObjectiveData.iRevalidates = MC_serverBD_4.iTimesRevalidatedObjectives[MC_playerBD[ObjectiveData.iparticipant].iteam]
									
									PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iPriority = ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam]")
									
									IF ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam] < FMMC_MAX_RULES
										
										IF NOT IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[MC_playerBD[ObjectiveData.iparticipant].iteam],ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
											PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - Setting Midpoint iObjectiveMidPointBitset for Team photo Obj ", MC_playerBD[ObjectiveData.iparticipant].iteam, " on Rule ", ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
											SET_BIT(MC_serverBD.iObjectiveMidPointBitset[MC_playerBD[ObjectiveData.iparticipant].iteam],ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
											SET_BIT(iLocalBoolCheck11, LBOOL11_HOST_APPLIED_NEW_MIDPOINT_MIDFRAME)
										ENDIF
										
										SET_TEAM_OBJECTIVE_OUTCOME(MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam],TRUE)
										MC_serverBD.iReasonForObjEnd[MC_playerBD[ObjectiveData.iparticipant].iteam] = OBJ_END_REASON_PHOTO_OBJ
										
										PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_OBJECT,ObjectiveData.iEntityID,MC_playerBD[ObjectiveData.iparticipant].iteam,TRUE)
										
										IF IS_THIS_OBJECTIVE_OVER(ci_TARGET_OBJECT,MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
											SET_LOCAL_OBJECTIVE_END_MESSAGE(MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam],MC_serverBD.iReasonForObjEnd[MC_playerBD[ObjectiveData.iparticipant].iteam])
											BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[MC_playerBD[ObjectiveData.iparticipant].iteam],MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam],DID_TEAM_PASS_OBJECTIVE(MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam]))
										ENDIF
										REMOVE_OBJECT_BLIP(ObjectiveData.iEntityID)
										
										IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[ObjectiveData.iparticipant].iteam].iRuleBitset[ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam]], ciBS_RULE_INVALIDATE_OTHER_TEAMS)
											FOR iteamrepeat = 0 TO (FMMC_MAX_TEAMS-1)
												IF iteamrepeat != MC_playerBD[ObjectiveData.iparticipant].iteam
													IF MC_serverBD_4.iObjRule[ObjectiveData.iEntityID][iteamrepeat] = FMMC_OBJECTIVE_LOGIC_PHOTO
														PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_OBJECT,ObjectiveData.iEntityID,iteamrepeat,TRUE)
														PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - photo Obj iInvalidateTargetForAll call for team: ",iteamrepeat)
													ENDIF
												ENDIF
											ENDFOR
										ENDIF
										
										PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iEntityID: ",ObjectiveData.iEntityID) 
										PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - server processing JOB_COMP_PHOTO_OBJ from part: ",ObjectiveData.iparticipant)
									ENDIF
									
								#IF IS_DEBUG_BUILD	
								ELSE
									
									PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - priorities or revalidates don't match up!")
								
								#ENDIF
									
								ENDIF
								
							ENDIF
							
						BREAK // End of JOB_COMP_PHOTO_OBJ
						
						CASE JOB_COMP_HACK_OBJ
						
							IF ObjectiveData.iEntityID != -1
								
								ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam] = MC_serverBD_4.iObjPriority[ObjectiveData.iEntityID][MC_playerBD[ObjectiveData.iparticipant].iteam] 
								
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ************************************ ")
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - CASE = JOB_COMP_HACK_OBJ")
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iPriority = ", ObjectiveData.iPriority)
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iparticipant = ", ObjectiveData.iparticipant)
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - MC_playerBD[ObjectiveData.iparticipant].iteam = ", MC_playerBD[ObjectiveData.iparticipant].iteam)
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam] = ", ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - MC_serverBD_4.iTimesRevalidatedObjectives[MC_playerBD[ObjectiveData.iparticipant].iteam] = ", MC_serverBD_4.iTimesRevalidatedObjectives[MC_playerBD[ObjectiveData.iparticipant].iteam])
								
								IF ObjectiveData.iPriority = ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam]
								AND MC_serverBD_4.iObjRule[ObjectiveData.iEntityID][MC_playerBD[ObjectiveData.iparticipant].iteam] = FMMC_OBJECTIVE_LOGIC_MINIGAME
								AND ObjectiveData.iRevalidates = MC_serverBD_4.iTimesRevalidatedObjectives[MC_playerBD[ObjectiveData.iparticipant].iteam]
									
									PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iPriority = ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam]")
									
									IF ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam] < FMMC_MAX_RULES
										
										PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ipriority: ",ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam]) 
										
										IF NOT IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[MC_playerBD[ObjectiveData.iparticipant].iteam],ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
											PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - Setting Midpoint iObjectiveMidPointBitset for Team hack Obj ", MC_playerBD[ObjectiveData.iparticipant].iteam, " on Rule ", ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
											SET_BIT(MC_serverBD.iObjectiveMidPointBitset[MC_playerBD[ObjectiveData.iparticipant].iteam],ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
											SET_BIT(iLocalBoolCheck11, LBOOL11_HOST_APPLIED_NEW_MIDPOINT_MIDFRAME)
										ENDIF
										
										SET_TEAM_OBJECTIVE_OUTCOME(MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam],TRUE)
										MC_serverBD.iReasonForObjEnd[MC_playerBD[ObjectiveData.iparticipant].iteam] = OBJ_END_REASON_HACK_OBJ
										
										IF SHOULD_OBJECT_MINIGAME_BE_PASSED_FOR_ALL_TEAMS_AT_ONCE(ObjectiveData.iEntityID)
											PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - Looping through all teams") 
										
											FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
												PRINTLN("[RCC MISSION] Processing team ", iteam, ".") 
												IF MC_serverBD_4.iObjRule[ObjectiveData.iEntityID][iteam] = FMMC_OBJECTIVE_LOGIC_MINIGAME
													PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - Team ", iteam, " objective rule for object is FMMC_OBJECTIVE_LOGIC_MINIGAME.") 
													PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_OBJECT,ObjectiveData.iEntityID,iteam,TRUE)
												ELSE
													PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - Team ", iteam, " objective rule for object is not FMMC_OBJECTIVE_LOGIC_MINIGAME.") 
												ENDIF
											ENDFOR
										ELSE
											PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_OBJECT,ObjectiveData.iEntityID,MC_playerBD[ObjectiveData.iparticipant].iteam,TRUE)
										ENDIF
										
										IF IS_THIS_OBJECTIVE_OVER(ci_TARGET_OBJECT,MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
											SET_LOCAL_OBJECTIVE_END_MESSAGE(MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam],MC_serverBD.iReasonForObjEnd[MC_playerBD[ObjectiveData.iparticipant].iteam])
											BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[MC_playerBD[ObjectiveData.iparticipant].iteam],MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam],DID_TEAM_PASS_OBJECTIVE(MC_playerBD[ObjectiveData.iparticipant].iteam,ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam]))
										ENDIF
										
										REMOVE_OBJECT_BLIP(ObjectiveData.iEntityID)
										
										IF ObjectiveData.iObjectiveID = JOB_COMP_HACK_OBJ
											IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[ObjectiveData.iEntityID].mn = HEI_P_ATTACHE_CASE_SHUT_S
												RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_MPHEIST/HEIST_CARD_SCANNER")
											ENDIF
										ENDIF
										
										//Destroy object after minigame
										IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[ObjectiveData.iEntityID].iObjectBitSetTwo, cibsOBJ2_DestroyAfterMinigame)
											FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
												IF iteam = MC_playerBD[ObjectiveData.iparticipant].iteam
													IF IS_BIT_SET(MC_serverBD.iObjteamFailBitset[ObjectiveData.iEntityID],iteam)
														CLEAR_BIT(MC_serverBD.iPedteamFailBitset[ObjectiveData.iEntityID],iteam)
														CLEAR_BIT(MC_serverBD.iMissionCriticalObj[iTeam], ObjectiveData.iEntityID)
														
														PRINTLN("[DestroyAfterMinigame] Clearing iPedteamFailBitset and iMissionCriticalObj for team ", iteam, " on obj ", ObjectiveData.iEntityID)
													ENDIF
												ELSE
													IF DOES_TEAM_LIKE_TEAM(iteam,MC_playerBD[ObjectiveData.iparticipant].iteam)
														IF IS_BIT_SET(MC_serverBD.iPedteamFailBitset[ObjectiveData.iEntityID],iteam)
															CLEAR_BIT(MC_serverBD.iPedteamFailBitset[ObjectiveData.iEntityID],iteam)
															CLEAR_BIT(MC_serverBD.iMissionCriticalObj[iTeam], ObjectiveData.iEntityID)
															
															PRINTLN("[DestroyAfterMinigame] Clearing iPedteamFailBitset and iMissionCriticalObj for team ", iteam, " on obj ", ObjectiveData.iEntityID)
														ENDIF
													ENDIF
												ENDIF
											ENDFOR
											
											PRINTLN("[DestroyAfterMinigame] Setting iObjCleanup_NeedOwnershipBS for minigame object ", ObjectiveData.iEntityID, " because of cibsOBJ2_DestroyAfterMinigame")
											SET_BIT(MC_serverBD.iObjCleanup_NeedOwnershipBS, ObjectiveData.iEntityID)
										ENDIF
										
										IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[ObjectiveData.iEntityID].iObjectBitSetTwo, cibsOBJ2_UnblipAfterMinigame)
											SET_BIT(MC_serverBD_4.iObjectDoNotBlipBS, ObjectiveData.iEntityID)
											PRINTLN("[JS] - PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - Setting iObjectDoNotBlipBS for minigame object ", ObjectiveData.iEntityID)
										ENDIF	
										
										IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[ObjectiveData.iEntityID].iObjectBitSetTwo, cibsOBJ2_ClearUpMissionCriticalVeh)
											PRINTLN("[JS] - PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - cibsOBJ2_ClearUpMissionCriticalVeh set, clearing veh ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[ObjectiveData.iEntityID].iAttachParent, " mission critical")
											CLEAR_BIT(MC_serverBD.iMissionCriticalVeh[MC_playerBD[ObjectiveData.iparticipant].iteam], g_FMMC_STRUCT_ENTITIES.sPlacedObject[ObjectiveData.iEntityID].iAttachParent)
										ENDIF
										
										IF SHOULD_OBJECT_ASSIGN_CUTSCENE_PRIMARY_PLAYER(ObjectiveData.iEntityID)
											SET_CUTSCENE_PRIMARY_PLAYER(ObjectiveData.iparticipant, MC_playerBD[ObjectiveData.iparticipant].iteam)
										ENDIF										
										
										PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iEntityID: ",ObjectiveData.iEntityID) 
										PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - server processing JOB_COMP_HACK_OBJ from part: ",ObjectiveData.iparticipant)
									ENDIF
									
								#IF IS_DEBUG_BUILD	
								ELSE
									
									PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - priorities or revalidates don't match up!")
								
								#ENDIF
								
								ENDIF
								
							ENDIF
							
						BREAK // End of JOB_COMP_HACK_OBJ
						
						
						CASE JOB_COMP_DELIVER_PED
							
							PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - MC_serverBD_4.iTimesRevalidatedObjectives[MC_playerBD[ObjectiveData.iparticipant].iteam] = ", MC_serverBD_4.iTimesRevalidatedObjectives[MC_playerBD[ObjectiveData.iparticipant].iteam])
							
							IF ObjectiveData.iRevalidates = MC_serverBD_4.iTimesRevalidatedObjectives[MC_playerBD[ObjectiveData.iparticipant].iteam]
								
								FOR irepeat = 0 TO (FMMC_MAX_PEDS-1)
									IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[irepeat])
										IF FMMC_IS_LONG_BIT_SET(ObjectiveData.iBitsetArrayPassed, irepeat)
											
											
											IF ObjectiveData.iPriority = MC_serverBD_4.iPedPriority[irepeat][MC_playerBD[ObjectiveData.iparticipant].iteam]
											AND MC_serverBD_4.iPedRule[irepeat][MC_playerBD[ObjectiveData.iparticipant].iteam] = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER
												
												IF SHOULD_PED_CLEAN_UP_ON_DELIVERY(irepeat,MC_playerBD[ObjectiveData.iparticipant].iteam)
													PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - JOB_COMP_DELIVER_PED, cleaning up ped ",irepeat," on delivery")
													FMMC_CLEAR_LONG_BIT(MC_serverBD.iPedCleanup_NeedOwnershipBS, irepeat)
													CLEANUP_NET_ID(MC_serverBD_1.sFMMC_SBD.niPed[irepeat])
												ELSE
													SET_PED_STATE(irepeat,ciTASK_CHOOSE_NEW_TASK)
												ENDIF
												// Only remove this ped's blip if they're no longer needed
												IF NOT SHOULD_PED_HAVE_BLIP(irepeat)
													IF DOES_BLIP_EXIST( biPedBlip[irepeat] )
														PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - Removing a blip on delivery ped  ", irepeat )
														REMOVE_BLIP( biPedBlip[irepeat] )
													ENDIF
												ENDIF
												
												IF MC_serverBD_2.iCurrentPedRespawnLives[irepeat] <= 0
													IF SHOULD_PED_CLEAN_UP_ON_DELIVERY(irepeat,MC_playerBD[ObjectiveData.iparticipant].iteam)
														FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
															IF iteam = MC_playerBD[ObjectiveData.iparticipant].iteam
																IF IS_BIT_SET(MC_serverBD.iPedteamFailBitset[irepeat],iteam)
																	CLEAR_BIT(MC_serverBD.iPedteamFailBitset[irepeat],iteam)
																	FMMC_CLEAR_LONG_BIT(MC_serverBD.iMissionCriticalPed[iTeam], irepeat)
																	bwasobjective[iTeam] = TRUE
																ENDIF
															ELSE
																IF DOES_TEAM_LIKE_TEAM(iteam,MC_playerBD[ObjectiveData.iparticipant].iteam)
																	IF IS_BIT_SET(MC_serverBD.iPedteamFailBitset[irepeat],iteam)
																		CLEAR_BIT(MC_serverBD.iPedteamFailBitset[irepeat],iteam)
																		FMMC_CLEAR_LONG_BIT(MC_serverBD.iMissionCriticalPed[iTeam], irepeat)
																		bwasobjective[iTeam] =TRUE
																	ENDIF	
																ENDIF
															ENDIF
														ENDFOR
													ENDIF
													FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
														
														ipriority[iteam] = MC_serverBD_4.iPedPriority[irepeat][iteam]
														
														IF iteam != MC_playerBD[ObjectiveData.iparticipant].iteam
															
															IF SHOULD_PED_CLEAN_UP_ON_DELIVERY(irepeat,MC_playerBD[ObjectiveData.iparticipant].iteam)
																IF IS_BIT_SET(MC_serverBD.iPedteamFailBitset[irepeat],iteam)
																	IF ipriority[iteam] < FMMC_MAX_RULES
																	OR FMMC_IS_LONG_BIT_SET(MC_serverBD.iMissionCriticalPed[iTeam], irepeat)
																		IF ((ipriority[iteam] < FMMC_MAX_RULES) AND (IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iteam].iTeamFailBitset,ipriority[iteam])))
																		OR FMMC_IS_LONG_BIT_SET(MC_serverBD.iMissionCriticalPed[iTeam], irepeat)
																			bobjectivefails[iTeam] = TRUE																		
																			SET_TEAM_FAILED(iTeam,mfail_PED_DELIVERED)
																			PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - MISSION FAILED FOR TEAM:",iteam," BECAUSE PED DELIVERED: ",irepeat)
																			MC_serverBD.iEntityCausingFail[iteam] = irepeat
																		ENDIF
																	ENDIF
																	CLEAR_BIT(MC_serverBD.iPedteamFailBitset[irepeat],iteam)
																	
																	bwasobjective[iTeam] = TRUE
																ENDIF
															ENDIF
															
															PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - team ", iTeam, " - ped's primary rule: ",MC_serverBD_4.iPedPriority[irepeat][iTeam])
															PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - team ", iTeam, " - MC_serverBD_4.iCurrentHighestPriority[iTeam]: ",MC_serverBD_4.iCurrentHighestPriority[iTeam])
															PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iPriority = ", ObjectiveData.iPriority)
															
															IF ipriority[iteam] = MC_serverBD_4.iCurrentHighestPriority[iTeam]
															AND ipriority[iteam] < FMMC_MAX_RULES
																IF DOES_TEAM_LIKE_TEAM(iteam,MC_playerBD[ObjectiveData.iparticipant].iteam)
																	IF MC_serverBD_4.iPedRule[irepeat][iteam] = FMMC_OBJECTIVE_LOGIC_PROTECT
																		INCREMENT_SERVER_TEAM_SCORE(iteam,ipriority[iTeam],GET_FMMC_POINTS_FOR_TEAM(iteam,ipriority[iTeam]))
																		PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - Protect ped rule adding points = ", GET_FMMC_POINTS_FOR_TEAM(iteam,ipriority[iTeam]))
																		//BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_TEAM_PRO_PED,0,iteam,-1,INVALID_PLAYER_INDEX(),ci_TARGET_PED,irepeat)
																	ENDIF
																	SET_TEAM_OBJECTIVE_OUTCOME(iTeam,ipriority[iteam],TRUE)
																	PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_PED,irepeat,iTeam,TRUE)													
																ELSE
																	SET_TEAM_OBJECTIVE_OUTCOME(iTeam,ipriority[iteam],FALSE)
																	PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_PED,irepeat,iTeam,FALSE)
																ENDIF
																MC_serverBD.iReasonForObjEnd[iteam] = OBJ_END_REASON_PED_DELIVERED
															ENDIF
														ELSE
															PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ipriority[iteam] team ", iTeam, " - priority: ",ipriority[iteam])
															PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iPriority = ", ObjectiveData.iPriority)
															
															IF NOT IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[MC_playerBD[ObjectiveData.iparticipant].iteam],ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
																PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - Setting Midpoint iObjectiveMidPointBitset for Team deliver ped ", MC_playerBD[ObjectiveData.iparticipant].iteam, " on Rule ", ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
																SET_BIT(MC_serverBD.iObjectiveMidPointBitset[MC_playerBD[ObjectiveData.iparticipant].iteam],ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
																SET_BIT(iLocalBoolCheck11, LBOOL11_HOST_APPLIED_NEW_MIDPOINT_MIDFRAME)
															ENDIF
															MC_serverBD.iReasonForObjEnd[iteam] = OBJ_END_REASON_PED_DELIVERED
															SET_TEAM_OBJECTIVE_OUTCOME(iTeam,ipriority[iteam],TRUE)
															PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_PED,irepeat,iTeam,TRUE)
														ENDIF
													ENDFOR
												ENDIF
												
												FMMC_SET_LONG_BIT(MC_serverBD.iPedDelivered, irepeat)
												
											ENDIF
												
										ENDIF // END of IS_BIT_SET(ObjectiveData.iEntityID,GET_LONG_BITSET_BIT(irepeat))
									ENDIF // END of NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[irepeat])
								ENDFOR
								FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
									IF bwasobjective[iTeam]
										IF IS_THIS_OBJECTIVE_OVER(ci_TARGET_PED,iteam,ipriority[iteam])
											SET_LOCAL_OBJECTIVE_END_MESSAGE(iteam,ipriority[iteam],MC_serverBD.iReasonForObjEnd[iteam],bobjectivefails[iTeam])
											BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[iteam],iteam,ipriority[iteam],DID_TEAM_PASS_OBJECTIVE(iteam,ipriority[iteam]),bobjectivefails[iTeam])
										ENDIF
									ENDIF
								ENDFOR
								
								tempPart= INT_TO_PARTICIPANTINDEX(ObjectiveData.iparticipant)
								
								SET_CUTSCENE_PRIMARY_PLAYER(ObjectiveData.iparticipant, MC_playerBD[ObjectiveData.iparticipant].iteam)
								
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - server processing JOB_COMP_DELIVER_PED from part: ",ObjectiveData.iparticipant)
								
							ELSE
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - revalidates don't match up!")
							ENDIF
							
						BREAK // End of JOB_COMP_DELIVER_PED
						
						CASE JOB_COMP_DELIVER_VEH
							
							PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - MC_serverBD_4.iTimesRevalidatedObjectives[MC_playerBD[ObjectiveData.iparticipant].iteam] = ", MC_serverBD_4.iTimesRevalidatedObjectives[MC_playerBD[ObjectiveData.iparticipant].iteam])
							
							IF ObjectiveData.iRevalidates = MC_serverBD_4.iTimesRevalidatedObjectives[MC_playerBD[ObjectiveData.iparticipant].iteam]
						
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - DELIVER_VEH FOR TEAM:", MC_playerBD[ObjectiveData.iparticipant].iteam)
								
								IF ObjectiveData.iEntityID != -1
									IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[ObjectiveData.iEntityID])						
										
										IF ObjectiveData.iPriority > -1 
										AND ObjectiveData.iPriority < FMMC_MAX_RULES
											IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[ObjectiveData.iParticipant].iteam].iRuleBitsetNine[ObjectiveData.iPriority], ciBS_RULE9_CLEANUP_UNDELIVERED_VEHICLES)
												IF IS_BIT_SET(MC_serverBD.iUndeliveredVehCleanupBS[MC_playerBD[ObjectiveData.iParticipant].iteam], ObjectiveData.iEntityID)
													PRINTLN("[JS] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - Veh ",ObjectiveData.iEntityID," was delivered this wont be cleaned up")
													CLEAR_BIT(MC_serverBD.iUndeliveredVehCleanupBS[MC_playerBD[ObjectiveData.iParticipant].iteam], ObjectiveData.iEntityID)
												ENDIF
											ENDIF
										ENDIF
													
										IF ObjectiveData.iPriority = MC_serverBD_4.iVehPriority[ObjectiveData.iEntityID][MC_playerBD[ObjectiveData.iParticipant].iteam]
										AND MC_serverBD_4.ivehRule[ObjectiveData.iEntityID][MC_playerBD[ObjectiveData.iparticipant].iteam] = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER	
											
											INCREMENT_SERVER_REMOTE_PLAYER_SCORE(ObjectiveData.Details.FromPlayerIndex, GET_FMMC_POINTS_FOR_TEAM(MC_playerBD[ObjectiveData.iparticipant].iteam, ObjectiveData.iPriority), MC_playerBD[ObjectiveData.iparticipant].iteam, ObjectiveData.iPriority)
											
											cutscene_vehicle[ivehNumber] = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[ObjectiveData.iEntityID])

											STRING strDecorName
											SWITCH MC_playerBD[ObjectiveData.iParticipant].iteam
												CASE 0	strDecorName = "MC_Team0_VehDeliveredRules"		BREAK
												CASE 1	strDecorName = "MC_Team1_VehDeliveredRules"		BREAK
												CASE 2	strDecorName = "MC_Team2_VehDeliveredRules"		BREAK
												CASE 3	strDecorName = "MC_Team3_VehDeliveredRules"		BREAK
											ENDSWITCH
											IF DECOR_IS_REGISTERED_AS_TYPE(strDecorName, DECOR_TYPE_INT)
												INT iDecorValue
												iDecorValue = 0
												IF DECOR_EXIST_ON(cutscene_vehicle[ivehNumber], strDecorName)
													iDecorValue = DECOR_GET_INT(cutscene_vehicle[ivehNumber], strDecorName)
												ENDIF
												SET_BIT(iDecorValue, ObjectiveData.iPriority)
												DECOR_SET_INT(cutscene_vehicle[ivehNumber], strDecorName, iDecorValue)
												CPRINTLN(DEBUG_MC_BLIP, "[RCC MISSION][VEH] Updated decorator [", strDecorName, "] to remember veh ", ivehNumber, " was delivered on rule ", ObjectiveData.iPriority, ".")
											ENDIF

											IF MC_serverBD.cutscene_vehicle_count < ciTOTAL_NUMBER_OF_MC_CUT_VEHCILES
												ivehNumber = MC_serverBD.cutscene_vehicle_count
											ENDIF
											cutscene_vehicle[ivehNumber] = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[ObjectiveData.iEntityID])
											
											BROADCAST_FMMC_DELIVERED_CUTSCENE_VEH(ObjectiveData.iEntityID,ivehNumber)

											IF SHOULD_VEH_CLEAN_UP_ON_DELIVERY(ObjectiveData.iEntityID,MC_playerBD[ObjectiveData.iparticipant].iteam)
												PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - JOB_COMP_DELIVER_VEH, cleaning up vehicle ",ObjectiveData.iEntityID," on delivery")
												CLEANUP_NET_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[ObjectiveData.iEntityID])
											ENDIF

											IF GET_VEHICLE_RESPAWNS(ObjectiveData.iEntityID) <=0	
											OR ((ObjectiveData.iPriority > -1 AND ObjectiveData.iPriority < FMMC_MAX_RULES) AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[ObjectiveData.iparticipant].iteam].iRuleBitsetNine[ObjectiveData.iPriority], ciBS_RULE9_PROGRESS_VEHICLES_WITH_RESPAWNS))
												IF SHOULD_VEH_CLEAN_UP_ON_DELIVERY(ObjectiveData.iEntityID,MC_playerBD[ObjectiveData.iparticipant].iteam)
													FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
														IF iteam = MC_playerBD[ObjectiveData.iparticipant].iteam
															IF IS_BIT_SET(MC_serverBD.iVehteamFailBitset[ObjectiveData.iEntityID],iteam)
																CLEAR_BIT(MC_serverBD.iVehteamFailBitset[ObjectiveData.iEntityID],iteam)
																CLEAR_BIT(MC_serverBD.iMissionCriticalVeh[iTeam], ObjectiveData.iEntityID)
																bwasobjective[iTeam] =TRUE
															ENDIF
														ELSE
															IF DOES_TEAM_LIKE_TEAM(iteam,MC_playerBD[ObjectiveData.iparticipant].iteam)
																IF IS_BIT_SET(MC_serverBD.iVehteamFailBitset[ObjectiveData.iEntityID],iteam)
																	CLEAR_BIT(MC_serverBD.iVehteamFailBitset[ObjectiveData.iEntityID],iteam)
																	CLEAR_BIT(MC_serverBD.iMissionCriticalVeh[iTeam], ObjectiveData.iEntityID)
																	bwasobjective[iTeam] =TRUE
																ENDIF
															ENDIF
														ENDIF
													ENDFOR
												ENDIF
												FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
												
													ipriority[iteam] = MC_serverBD_4.iVehPriority[ObjectiveData.iEntityID][iteam]
													
													IF iteam != MC_playerBD[ObjectiveData.iparticipant].iteam
														
														IF ipriority[iteam] < FMMC_MAX_RULES
														AND NOT IS_BIT_SET(MC_serverBD_1.iObjectiveProgressionHeldUpBitset[iteam], ipriority[iteam])
															
															IF SHOULD_DELIVER_VEH_BE_PROGRESSED_BY_LIKED_TEAM(ObjectiveData.iEntityID,iTeam,ipriority[iteam],MC_playerBD[ObjectiveData.iparticipant].iteam,MC_serverBD_4.iVehPriority[ObjectiveData.iEntityID][MC_playerBD[ObjectiveData.iparticipant].iteam])
																
																IF SHOULD_VEH_CLEAN_UP_ON_DELIVERY(ObjectiveData.iEntityID,MC_playerBD[ObjectiveData.iparticipant].iteam)
																	IF IS_BIT_SET(MC_serverBD.iVehteamFailBitset[ObjectiveData.iEntityID],iteam)
																		IF ipriority[iteam] < FMMC_MAX_RULES
																		OR IS_BIT_SET(MC_serverBD.iMissionCriticalVeh[iTeam], ObjectiveData.iEntityID)
																			IF SHOULD_VEHICLE_CAUSE_MISSION_FAILURE(ObjectiveData.iEntityID, iTeam, iPriority[iTeam])
																				bobjectivefails[iTeam] = TRUE																		
																				SET_TEAM_FAILED(iTeam,mFail_VEH_DELIVERED)
																				MC_serverBD.iEntityCausingFail[iteam] = ObjectiveData.iEntityID
																				PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - MISSION FAILED FOR TEAM:",iteam," BECAUSE VEH DELIVERED: ",ObjectiveData.iEntityID)
																			ENDIF
																		ENDIF
																		bwasobjective[iTeam] =TRUE
																		CLEAR_BIT(MC_serverBD.iVehteamFailBitset[ObjectiveData.iEntityID],iteam)
																	ENDIF
																ENDIF
																
																PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - team ", iTeam, " - veh's primary rule: ",MC_serverBD_4.iVehPriority[ObjectiveData.iEntityID][iTeam])
																PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - team ", iTeam, " - MC_serverBD_4.iCurrentHighestPriority[iTeam]: ",MC_serverBD_4.iCurrentHighestPriority[iTeam])
																PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iPriority = ", ObjectiveData.iPriority)
																
																IF ipriority[iTeam] = MC_serverBD_4.iCurrentHighestPriority[iTeam]
																AND ipriority[iteam] < FMMC_MAX_RULES
																	IF DOES_TEAM_LIKE_TEAM(iteam,MC_playerBD[ObjectiveData.iparticipant].iteam)
																		IF MC_serverBD_4.ivehRule[ObjectiveData.iEntityID][iteam] = FMMC_OBJECTIVE_LOGIC_PROTECT
																			INCREMENT_SERVER_TEAM_SCORE(iteam,ipriority[iTeam],GET_FMMC_POINTS_FOR_TEAM(iteam,ipriority[iTeam]))
																			PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - Protect veh rule adding points = ", GET_FMMC_POINTS_FOR_TEAM(iteam,ipriority[iTeam]))
																			//BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_TEAM_PRO_VEH,0,iteam,-1,INVALID_PLAYER_INDEX(),ci_TARGET_VEHICLE,ObjectiveData.iEntityID)
																		ENDIF
																		SET_TEAM_OBJECTIVE_OUTCOME(iTeam,ipriority[iteam],TRUE)
																		PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_VEHICLE,ObjectiveData.iEntityID,iTeam,TRUE)												
																	ELSE
																		SET_TEAM_OBJECTIVE_OUTCOME(iTeam,ipriority[iteam],FALSE)
																		PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_VEHICLE,ObjectiveData.iEntityID,iTeam,FALSE)
																	ENDIF
																	MC_serverBD.iReasonForObjEnd[iteam] = OBJ_END_REASON_VEH_DELIVERED
																ENDIF
															ELSE
																PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - Team ", iTeam, " DELIVER_VEHICLE not progressed as different coords or radius")
															ENDIF
														ELSE
															PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - Team ", iTeam, " progression is held up by hack minigame blocking objective progression")
														ENDIF
													ELSE
													
														PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ipriority[iteam] team ", iTeam, " - priority: ",ipriority[iteam])
														PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iPriority = ", ObjectiveData.iPriority)
														
														IF NOT IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[MC_playerBD[ObjectiveData.iparticipant].iteam],ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
															PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - Setting Midpoint iObjectiveMidPointBitset for Team deliver veh ", MC_playerBD[ObjectiveData.iparticipant].iteam, " on Rule ", ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
															SET_BIT(MC_serverBD.iObjectiveMidPointBitset[MC_playerBD[ObjectiveData.iparticipant].iteam],ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
															SET_BIT(iLocalBoolCheck11, LBOOL11_HOST_APPLIED_NEW_MIDPOINT_MIDFRAME)
														ENDIF
														
														SET_TEAM_OBJECTIVE_OUTCOME(iTeam,ipriority[iteam],TRUE)
														PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_VEHICLE,ObjectiveData.iEntityID,iTeam,TRUE)
														MC_serverBD.iReasonForObjEnd[iteam] = OBJ_END_REASON_VEH_DELIVERED
													ENDIF
													
												ENDFOR
											ENDIF
											IF g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[ObjectiveData.iParticipant].iteam].iDropOffStopTimer[ObjectiveData.iPriority] = -1
												IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[ObjectiveData.iEntityID])
													VEHICLE_INDEX tempVeh
													
													tempVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[ObjectiveData.iEntityID])
													
													IF IS_PED_IN_VEHICLE(LocalPlayerPed, tempVeh)
														SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_PreventAutoShuffleToDriversSeat, TRUE)
														
														bStopDurationBlockSeatShuffle = TRUE
													ENDIF
												ENDIF
											ENDIF
											
											PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - vehicle delivered on host") 
											
											IF DOES_BLIP_EXIST(biVehBlip[ObjectiveData.iEntityID])
											AND NOT SHOULD_VEHICLE_HAVE_CUSTOM_BLIP(ObjectiveData.iEntityID)
												REMOVE_VEHICLE_BLIP(ObjectiveData.iEntityID)
												PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - JOB_COMP_DELIVER_VEH objective complete removing blip for veh HOST: ",ObjectiveData.iEntityID) 
											ENDIF
											
											IF iDeliveryVehForcingOut = ObjectiveData.iEntityID
												iDeliveryVehForcingOut = -1
												iPartSending_DeliveryVehForcingOut = -1
												RESET_NET_TIMER(tdForceEveryoneOutBackupTimer)
											ENDIF
										ENDIF
									ENDIF
									
									FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
										IF bwasobjective[iTeam]
											IF IS_THIS_OBJECTIVE_OVER(ci_TARGET_VEHICLE,iteam,ipriority[iteam])
												SET_LOCAL_OBJECTIVE_END_MESSAGE(iteam,ipriority[iteam],MC_serverBD.iReasonForObjEnd[iteam],bobjectivefails[iTeam])
												BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[iteam],iteam,ipriority[iteam],DID_TEAM_PASS_OBJECTIVE(iteam,ipriority[iteam]),bobjectivefails[iTeam])
											ENDIF
										ENDIF
									ENDFOR
									
									PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iEntityID: ",ObjectiveData.iEntityID) 
									PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - server processing JOB_COMP_DELIVER_VEH from part: ",ObjectiveData.iparticipant)
									
									tempPart= INT_TO_PARTICIPANTINDEX(ObjectiveData.iparticipant)
									
									SET_CUTSCENE_PRIMARY_PLAYER(ObjectiveData.iparticipant, MC_playerBD[ObjectiveData.iparticipant].iteam)
								ENDIF
							ELSE
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - revalidates don't match up!")
							ENDIF
							
						BREAK // End of JOB_COMP_DELIVER_VEH
						
						CASE JOB_COMP_DELIVER_OBJ

							PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - MC_serverBD_4.iTimesRevalidatedObjectives[MC_playerBD[ObjectiveData.iparticipant].iteam] = ", MC_serverBD_4.iTimesRevalidatedObjectives[MC_playerBD[ObjectiveData.iparticipant].iteam])
							
							IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciSHOW_LAST_DELIVERER_SHARD)
								piLastDeliverer = ObjectiveData.Details.FromPlayerIndex
							ENDIF	
							
							IF ObjectiveData.iRevalidates = MC_serverBD_4.iTimesRevalidatedObjectives[MC_playerBD[ObjectiveData.iparticipant].iteam]
								
								FOR irepeat = 0 TO (FMMC_MAX_NUM_OBJECTS-1)
									IF NETWORK_DOES_NETWORK_ID_EXIST(GET_OBJECT_NET_ID(irepeat))
										IF IS_BIT_SET(ObjectiveData.iEntityID,irepeat)
											
											IF ObjectiveData.iPriority = MC_serverBD_4.iObjPriority[irepeat][MC_playerBD[ObjectiveData.iparticipant].iteam]
											AND MC_serverBD_4.iObjRule[irepeat][MC_playerBD[ObjectiveData.iparticipant].iteam] = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER
												IF MC_serverBD_4.iObjPriority[irepeat][MC_playerBD[ObjectiveData.iparticipant].iteam] < FMMC_MAX_RULES
													
													IF NOT IS_BIT_SET(MC_serverBD_1.iObjDeliveredOnceBS, irepeat)
														PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - JOB_COMP_DELIVER_OBJ, setting MC_serverBD_1.iObjDeliveredOnceBS for object ",irepeat)
														SET_BIT(MC_serverBD_1.iObjDeliveredOnceBS, irepeat)
													ENDIF
													
													MC_serverBD.iNumObjDelAtPriority[MC_playerBD[ObjectiveData.iparticipant].iteam][MC_serverBD_4.iObjPriority[irepeat][MC_playerBD[ObjectiveData.iparticipant].iteam]]++
													MC_serverBD.iObjDelTeam[irepeat]=MC_playerBD[ObjectiveData.iparticipant].iteam
													REMOVE_OBJECT_BLIP(irepeat)
													
													IF SHOULD_OBJ_CLEAN_UP_ON_DELIVERY(irepeat, MC_playerBD[ObjectiveData.iparticipant].iteam)
														PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - JOB_COMP_DELIVER_OBJ, cleaning up object ", irepeat, " on delivery")
														CLEANUP_NET_ID(MC_serverBD_1.sFMMC_SBD.niNetworkedObject[GET_OBJECT_NET_ID_INDEX(irepeat)])
													ENDIF
													
													IF MC_serverBD_2.iCurrentObjRespawnLives[irepeat] <= 0
														IF SHOULD_OBJ_CLEAN_UP_ON_DELIVERY(irepeat,MC_playerBD[ObjectiveData.iparticipant].iteam)
															FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
																IF iteam=MC_playerBD[ObjectiveData.iparticipant].iteam
																	IF IS_BIT_SET(MC_serverBD.iObjteamFailBitset[irepeat],iteam)
																		CLEAR_BIT(MC_serverBD.iObjteamFailBitset[irepeat],iteam)
																		CLEAR_BIT(MC_serverBD.iMissionCriticalObj[iTeam], irepeat)
																		bwasobjective[iTeam] = TRUE
																	ENDIF
																ELSE
																	IF DOES_TEAM_LIKE_TEAM(iteam,MC_playerBD[ObjectiveData.iparticipant].iteam)
																		IF IS_BIT_SET(MC_serverBD.iObjteamFailBitset[irepeat],iteam)
																			CLEAR_BIT(MC_serverBD.iObjteamFailBitset[irepeat],iteam)
																			CLEAR_BIT(MC_serverBD.iMissionCriticalObj[iTeam], irepeat)
																			bwasobjective[iTeam] =TRUE
																		ENDIF	
																	ENDIF
																ENDIF
															ENDFOR
														ENDIF
														FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
															
															ipriority[iteam] = MC_serverBD_4.iObjPriority[irepeat][iteam]
															
															IF iteam!=MC_playerBD[ObjectiveData.iparticipant].iteam
																
																IF SHOULD_OBJ_CLEAN_UP_ON_DELIVERY(irepeat,MC_playerBD[ObjectiveData.iparticipant].iteam)
																	IF IS_BIT_SET(MC_serverBD.iObjteamFailBitset[irepeat],iteam)
																		IF ipriority[iteam] < FMMC_MAX_RULES
																		OR IS_BIT_SET(MC_serverBD.iMissionCriticalObj[iTeam], irepeat)
																			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iteam].iTeamFailBitset,ipriority[iteam])
																			OR IS_BIT_SET(MC_serverBD.iMissionCriticalObj[iTeam], irepeat)
																				bobjectivefails[iTeam] = TRUE
																				SET_TEAM_FAILED(iTeam,mFail_OBJ_DELIVERED)
																				MC_serverBD.iEntityCausingFail[iteam] = irepeat
																				PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - MISSION FAILED FOR TEAM:",iteam," BECAUSE OBJ DELIVERED: ",irepeat)
																			ENDIF
																		ENDIF
																		bwasobjective[iTeam] = TRUE
																		CLEAR_BIT(MC_serverBD.iObjteamFailBitset[irepeat],iteam)
																	ENDIF
																ENDIF
																
																PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - team ", iTeam, " - obj's primary rule: ",MC_serverBD_4.iObjPriority[irepeat][iTeam])
																PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - team ", iTeam, " - MC_serverBD_4.iCurrentHighestPriority[iTeam]: ",MC_serverBD_4.iCurrentHighestPriority[iTeam])
																PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iPriority = ", ObjectiveData.iPriority)
																
																IF (ipriority[iTeam] = MC_serverBD_4.iCurrentHighestPriority[iTeam]
																	OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[irepeat].iObjectBitSet, cibsOBJ_InvalidateAllTeams))
																AND ipriority[iteam] < FMMC_MAX_RULES
																	IF DOES_TEAM_LIKE_TEAM(iteam,MC_playerBD[ObjectiveData.iparticipant].iteam)
																		IF MC_serverBD_4.iObjRule[irepeat][iteam] = FMMC_OBJECTIVE_LOGIC_PROTECT
																			PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - Protect Obj rule adding points = ", GET_FMMC_POINTS_FOR_TEAM(iteam,ipriority[iTeam]))
																			INCREMENT_SERVER_TEAM_SCORE(iteam,ipriority[iTeam],GET_FMMC_POINTS_FOR_TEAM(iteam,ipriority[iTeam]))
																		ENDIF
																		SET_TEAM_OBJECTIVE_OUTCOME(iTeam,ipriority[iteam], TRUE)
																		PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_OBJECT,irepeat,iTeam,TRUE)	
																	ELSE
																		SET_TEAM_OBJECTIVE_OUTCOME(iTeam,ipriority[iteam], FALSE)
																		PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_OBJECT,irepeat,iTeam,FALSE)
																	ENDIF
																	MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_OBJ_DELIVERED
																ENDIF
															ELSE
																PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ipriority[iteam] team ", iTeam, " - priority: ",ipriority[iteam])
																PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - ObjectiveData.iPriority = ", ObjectiveData.iPriority)
																
																IF NOT IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[MC_playerBD[ObjectiveData.iparticipant].iteam],ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
																	PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - Setting Midpoint iObjectiveMidPointBitset for Team deliver obj ", MC_playerBD[ObjectiveData.iparticipant].iteam, " on Rule ", ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
																	SET_BIT(MC_serverBD.iObjectiveMidPointBitset[MC_playerBD[ObjectiveData.iparticipant].iteam],ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam])
																	SET_BIT(iLocalBoolCheck11, LBOOL11_HOST_APPLIED_NEW_MIDPOINT_MIDFRAME)
																ENDIF
																
																SET_TEAM_OBJECTIVE_OUTCOME(iTeam,ipriority[iteam], TRUE)
																PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_OBJECT,irepeat,iTeam,TRUE)
																MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_OBJ_DELIVERED
															ENDIF
															
														ENDFOR

													ENDIF
													
												ENDIF
												IF ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam] < FMMC_MAX_RULES
													IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[ObjectiveData.iparticipant].iteam].iRuleBitSetSix[ipriority[MC_playerBD[ObjectiveData.iparticipant].iteam]], ciBS_RULE6_GET_DELIVER_DONT_DROP_ON_DELIVER)
														MC_serverBD.iObjCarrier[irepeat] = -1
													ENDIF
												ENDIF
											
											ENDIF
										ENDIF
									ENDIF
								ENDFOR
								FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
									IF bwasobjective[iTeam]
										IF IS_THIS_OBJECTIVE_OVER(ci_TARGET_OBJECT,iteam,ipriority[iteam])
											SET_LOCAL_OBJECTIVE_END_MESSAGE(iTeam,ipriority[iteam],MC_serverBD.iReasonForObjEnd[iteam],bobjectivefails[iTeam])
											BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[iteam],iteam,ipriority[iteam],DID_TEAM_PASS_OBJECTIVE(iteam,ipriority[iteam]),bobjectivefails[iTeam])
										ENDIF
									ENDIF
								ENDFOR
								
								SET_CUTSCENE_PRIMARY_PLAYER(ObjectiveData.iparticipant, MC_playerBD[ObjectiveData.iparticipant].iteam)
								
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - server processing JOB_COMP_DELIVER_OBJ from part: ",ObjectiveData.iparticipant) 
								
							ELSE
								PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - revalidates don't match up!")
							ENDIF
						BREAK // End of JOB_COMP_DELIVER_OBJ
						
					ENDSWITCH
					
					FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
						IF SHOULD_PROGRESS_OBJECTIVE_FOR_TEAM(iteam)
							PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - SHOULD_PROGRESS_OBJECTIVE_FOR_TEAM returned true for team ",iteam)
							
							BOOL bPreviousObjective = FALSE
							INT iObjectiveToProgressTo = GET_OBJECTIVE_TO_PROGRESS_TO_FOR_TEAM(iTeam, bPreviousObjective)
							
							INVALIDATE_UP_TO_AND_INCLUDING_OBJECTIVE(iteam, iObjectiveToProgressTo, TRUE, DEFAULT, bPreviousObjective)
							RECALCULATE_OBJECTIVE_LOGIC(iteam)
							SET_PROGRESS_OBJECTIVE_FOR_TEAM(iteam,FALSE)
						ELSE
							PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - SHOULD_PROGRESS_OBJECTIVE_FOR_TEAM returned false for team ",iteam)
							RECALCULATE_OBJECTIVE_LOGIC(iTeam)
						ENDIF
					ENDFOR
					
					SET_BIT(MC_serverBD.iServerBitSet,SBBOOL_SKIP_TEAM_0_UPDATE)
					SET_BIT(MC_serverBD.iServerBitSet,SBBOOL_SKIP_TEAM_1_UPDATE)
					SET_BIT(MC_serverBD.iServerBitSet,SBBOOL_SKIP_TEAM_2_UPDATE)
					SET_BIT(MC_serverBD.iServerBitSet,SBBOOL_SKIP_TEAM_3_UPDATE)
					
					SET_BIT(MC_serverBD.iProcessJobCompBitset,ObjectiveData.iparticipant)
					PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - MC_serverBD.iProcessJobCompBitset set for part: ",ObjectiveData.iparticipant)
					
				ELSE
					PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT - part ",ObjectiveData.iparticipant," already has iProcessJobCompBitset set")
				ENDIF
			ENDIF // End of IF is this person in control of this script
		ENDIF
	ENDIF
				
ENDPROC

PROC PROCESS_FMMC_OBJECTIVE_END_MESSAGE_EVENT(int icount)

	SCRIPT_EVENT_DATA_FMMC_OBJECTIVE_END_MESSAGE ObjectiveData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))

		IF ObjectiveData.Details.Type = SCRIPT_EVENT_FMMC_OBJECTIVE_END_MESSAGE
			
			PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_END_MESSAGE_EVENT - ObjectiveData.iObjectiveID = ",ObjectiveData.iObjectiveID)
			PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_END_MESSAGE_EVENT - ObjectiveData.iTeam = ",ObjectiveData.iTeam) 
			PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_END_MESSAGE_EVENT - ObjectiveData.iPriority = ",ObjectiveData.iPriority)
			PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_END_MESSAGE_EVENT - ObjectiveData.bpass = ",ObjectiveData.bpass)
			PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_END_MESSAGE_EVENT - ObjectiveData.bFailMission = ",ObjectiveData.bFailMission)
			 
			IF ObjectiveData.iObjectiveID !=-1
				
				IF ObjectiveData.bFailMission
				AND (ireasonObjEnd[ObjectiveData.iTeam] = -1)
					iObjEndPriority[ObjectiveData.iTeam] = ObjectiveData.iPriority
					ireasonObjEnd[ObjectiveData.iTeam] = ObjectiveData.iObjectiveID
					
					PRINTLN("[RCC MISSION] PROCESS_FMMC_OBJECTIVE_END_MESSAGE_EVENT - Setting fail reason for team ",ObjectiveData.iTeam,", priority",iObjEndPriority[ObjectiveData.iTeam],", reason ",ireasonObjEnd[ObjectiveData.iTeam])
					
				ENDIF
				
			ENDIF
			
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_FMMC_OBJECTIVE_MID_POINT(INT iCount)

	SCRIPT_EVENT_DATA_FMMC_OBJECTIVE_MID_POINT ObjectiveData
	
	IF bIsLocalPlayerHost
		IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))
			IF ObjectiveData.Details.Type = SCRIPT_EVENT_FMMC_OBJECTIVE_MID_POINT
				IF ObjectiveData.iObjective < FMMC_MAX_RULES
				AND NOT IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[ObjectiveData.iTeam],ObjectiveData.iObjective)
					SET_BIT(MC_serverBD.iObjectiveMidPointBitset[ObjectiveData.iTeam],ObjectiveData.iObjective)
					SET_BIT(iLocalBoolCheck11, LBOOL11_HOST_APPLIED_NEW_MIDPOINT_MIDFRAME)
					PRINTLN("[RCC MISSION] TEAM : ",ObjectiveData.iTeam," HAS HIT THE MIDPOINT iObjectiveMidPointBitset OF OBJECTIVE: ",ObjectiveData.iObjective)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_DECREMENT_REMAINING_TIME(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_DECREMENT_REMAINING_TIME EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF bIsLocalPlayerHost
			INT i, iAmountToDecrement
			FOR i = 0 TO FMMC_MAX_TEAMS-1
								
				IF EventData.bTeamOnly
					IF i != EventData.iTeam
						PRINTLN("PROCESS_SCRIPT_EVENT_FMMC_DECREMENT_REMAINING_TIME - Skipping team: ", i, " as not the team that sent request")
						RELOOP
					ENDIF
				ENDIF
							
				IF IS_MULTIRULE_TIMER_RUNNING(i)
					MC_serverBD_3.tdMultiObjectiveLimitTimer[i].Timer = GET_TIME_OFFSET(MC_serverBD_3.tdMultiObjectiveLimitTimer[i].Timer, -EventData.iAmountToDecrement)
					PRINTLN("PROCESS_SCRIPT_EVENT_FMMC_DECREMENT_REMAINING_TIME - Decremented Multirule timer for team: ", i, " by ", EventData.iAmountToDecrement)
					IF GET_LOCAL_PLAYER_MULTIRULE_TIMER_TIME_REMAINING() <= 30000
						SET_BIT(MC_serverBD.iServerBitSet6, SBBOOL6_TIME_DEDUCTED_INTO_30S)
					ENDIF
				ENDIF
				
				IF IS_OBJECTIVE_TIMER_RUNNING(i)
					iAmountToDecrement = EventData.iAmountToDecrement
					
					MC_serverBD_3.tdObjectiveLimitTimer[i].Timer = GET_TIME_OFFSET(MC_serverBD_3.tdObjectiveLimitTimer[i].Timer, -iAmountToDecrement)
					PRINTLN("PROCESS_SCRIPT_EVENT_FMMC_DECREMENT_REMAINING_TIME - Decremented Rule timer for team: ", i, " by ", iAmountToDecrement)
					IF MC_serverBD_4.iCurrentHighestPriority[i] < FMMC_MAX_RULES
						IF GET_CURRENT_OBJECTIVE_TIMER_TIME_REMAINING(i) <= 30000
							SET_BIT(MC_serverBD.iServerBitSet6, SBBOOL6_TIME_DEDUCTED_INTO_30S)
						ENDIF
					ENDIF
				ENDIF
			ENDFOR	
			IF HAS_NET_TIMER_STARTED(MC_serverBD.tdMissionLengthTimer)
				MC_serverBD.tdMissionLengthTimer.Timer = GET_TIME_OFFSET(MC_serverBD.tdMissionLengthTimer.Timer, -EventData.iAmountToDecrement)
				PRINTLN("PROCESS_SCRIPT_EVENT_FMMC_DECREMENT_REMAINING_TIME - Decremented Mission length timer by ", EventData.iAmountToDecrement)
			ENDIF
		ENDIF

		INT iTimeToDisplay = (EventData.iAmountToDecrement / 1000)
		IF NOT EventData.bStealingPoints
			IF EventData.bDestroyed
				PRINT_TICKER_WITH_PLAYER_NAME_AND_INT("DT_DEST", EventData.Details.FromPlayerIndex, iTimeToDisplay)
			ELSE
				PRINT_TICKER_WITH_PLAYER_NAME_AND_INT("VNJOB_MRSPWN", EventData.Details.FromPlayerIndex, iTimeToDisplay)
			ENDIF
		ENDIF
		fRedTimerTime = 5.0
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_INCREMENT_REMAINING_TIME(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_INCREMENT_REMAINING_TIME EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF bIsLocalPlayerHost
			INT i
			FOR i = 0 TO FMMC_MAX_TEAMS-1
			
				IF EventData.bTeamOnly
					IF i != EventData.iTeam
						PRINTLN("PROCESS_SCRIPT_EVENT_FMMC_INCREMENT_REMAINING_TIME - Skipping team: ", i, " as not the team that sent request")
						RELOOP
					ENDIF
				ENDIF
				
				IF IS_MULTIRULE_TIMER_RUNNING(i)
					MC_serverBD_3.tdMultiObjectiveLimitTimer[i].Timer = GET_TIME_OFFSET(MC_serverBD_3.tdMultiObjectiveLimitTimer[i].Timer, EventData.iAmountToIncrement)
					PRINTLN("PROCESS_SCRIPT_EVENT_FMMC_INCREMENT_REMAINING_TIME - Incremented Multirule timer for team: ", i, " by ", EventData.iAmountToIncrement)
				ENDIF
				
				IF IS_OBJECTIVE_TIMER_RUNNING(i)
					MC_serverBD_3.tdObjectiveLimitTimer[i].Timer = GET_TIME_OFFSET(MC_serverBD_3.tdObjectiveLimitTimer[i].Timer, EventData.iAmountToIncrement)
					PRINTLN("PROCESS_SCRIPT_EVENT_FMMC_INCREMENT_REMAINING_TIME - Incremented Rule timer for team: ", i, " by ", EventData.iAmountToIncrement)
				ENDIF
			ENDFOR	
			IF HAS_NET_TIMER_STARTED(MC_serverBD.tdMissionLengthTimer)
				MC_serverBD.tdMissionLengthTimer.Timer = GET_TIME_OFFSET(MC_serverBD.tdMissionLengthTimer.Timer, EventData.iAmountToIncrement)
				PRINTLN("PROCESS_SCRIPT_EVENT_FMMC_INCREMENT_REMAINING_TIME - Incremented Mission length timer by ", EventData.iAmountToIncrement)
			ENDIF
		ENDIF
		
		INT iTimeToDisplay = (EventData.iAmountToIncrement / 1000)
		
		IF EventData.bStealingPoints
			//PRINT_TICKER_WITH_PLAYER_NAME_AND_INT("AT_INC_STEAL", EventData.Details.FromPlayerIndex, iTimeToDisplay)
			PRINT_TICKER_WITH_TWO_PLAYER_NAMES_AND_TWO_INTS("AT_INC_STEAL", EventData.Details.FromPlayerIndex, INT_TO_PLAYERINDEX(EventData.iPlayerIndexVictim), iTimeToDisplay)
			
		ELSE
			IF EventData.iParticipantIndex != -1
				PRINT_TICKER_WITH_PLAYER_NAME_AND_INT("AT_INCT", NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(EventData.iParticipantIndex)) , iTimeToDisplay)
			ENDIF
		ENDIF
		
		IF MC_playerBD[iLocalPart].iteam = EventData.iTeam
			VECTOR vPos = GET_POINTS_COORDS(LocalPlayerPed)
			HUD_COLOURS eTextColour = GET_HUD_COLOUR_FOR_FMMC_TEAM(EventData.iTeam, LocalPlayer)

			TARGET_ADD_FLOATING_SCORE(iTimeToDisplay, vPos,  eTextColour, sFloatingScore)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_NETWORK_INDEX_FOR_LATER_RULE(INT iCount)

	SCRIPT_EVENT_DATA_NETWORK_INDEX_FOR_LATER_RULE LaterObjectiveData

	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, LaterObjectiveData, SIZE_OF(LaterObjectiveData))
		IF LaterObjectiveData.Details.Type = SCRIPT_EVENT_NETWORK_INDEX_FOR_LATER_RULE
			PRINTLN("[RCC MISSION] LaterObjectiveData.NetIdForLaterObjective = ",NATIVE_TO_INT(LaterObjectiveData.NetIdForLaterObjective))
			PRINTLN("[RCC MISSION] LaterObjectiveData.iLaterObjectiveType = ",LaterObjectiveData.iLaterObjectiveType)
			PRINTLN("[RCC MISSION] LaterObjectiveData.iLaterObjectiveEntityIndex = ",LaterObjectiveData.iLaterObjectiveEntityIndex)
			IF bIsLocalPlayerHost
				SWITCH LaterObjectiveData.iLaterObjectiveType
					
					CASE ci_TARGET_PED
					
						MC_serverBD_1.sFMMC_SBD.niPed[LaterObjectiveData.iLaterObjectiveEntityIndex] = LaterObjectiveData.NetIdForLaterObjective
						PRINTLN("[RCC MISSION] PROCESS_NETWORK_INDEX_FOR_LATER_RULE: MC_serverBD_1.sFMMC_SBD.niPed = ",NATIVE_TO_INT(MC_serverBD_1.sFMMC_SBD.niPed[LaterObjectiveData.iLaterObjectiveEntityIndex]))
											

					BREAK
					
					CASE ci_TARGET_VEHICLE

						MC_serverBD_1.sFMMC_SBD.niVehicle[LaterObjectiveData.iLaterObjectiveEntityIndex] = LaterObjectiveData.NetIdForLaterObjective

						SET_BIT(MC_serverBD.iAmbientOverrideVehicle,LaterObjectiveData.iLaterObjectiveEntityIndex)
						PRINTLN("[RCC MISSION] PROCESS_NETWORK_INDEX_FOR_LATER_RULE: MC_serverBD_1.sFMMC_SBD.niVehicle = ",NATIVE_TO_INT(MC_serverBD_1.sFMMC_SBD.niVehicle[LaterObjectiveData.iLaterObjectiveEntityIndex]))
						
					BREAK
					
					CASE ci_TARGET_OBJECT
					
						MC_serverBD_1.sFMMC_SBD.niNetworkedObject[LaterObjectiveData.iLaterObjectiveEntityIndex] = LaterObjectiveData.NetIdForLaterObjective
						PRINTLN("[RCC MISSION] PROCESS_NETWORK_INDEX_FOR_LATER_RULE: MC_serverBD_1.sFMMC_SBD.niObject = ", NATIVE_TO_INT(GET_OBJECT_NET_ID(LaterObjectiveData.iLaterObjectiveEntityIndex)))
							
					BREAK
				ENDSWITCH
			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC PROCESS_FMMC_OBJECTIVE_REVALIDATE(INT iCount)
	
	SCRIPT_EVENT_DATA_FMMC_OBJECTIVE_REVALIDATE EventData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_OBJECTIVE_REVALIDATE
			
			PROCESS_CLIENT_OBJECTIVE_REVALIDATION(EventData.iTeam, EventData.iLocResetBS)
			
		ENDIF
	ENDIF
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Spawning --------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Processes for rules functionality  			--------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_FMMC_REMOTELY_ADD_RESPAWN_POINTS(INT iCount)

	SCRIPT_EVENT_DATA_FMMC_REMOTELY_ADD_RESPAWN_POINTS EventData

	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_REMOTELY_ADD_RESPAWN_POINTS
			IF EventData.iTeam = MC_PlayerBD[iLocalPart].iTeam
			OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[EventData.iTeam][EventData.iRespawnPoint].iSpawnBitSet, ci_SpawnBS_ActivateAllTeams)
				
				PRINTLN("[MC_CustomSpawning][PROCESS_FMMC_REMOTELY_ADD_RESPAWN_POINTS] - Activating point: ", EventData.iRespawnPoint, " for Team: ", EventData.iTeam, " REMOTELY")
				
				ACTIVATE_LINKED_CUSTOM_RESPAWN_POINTS(EventData.iTeam, EventData.iRespawnPoint)
				FMMC_SET_LONG_BIT(iBSRespawnActiveFromRadius[MC_PlayerBD[iLocalPart].iTeam], EventData.iRespawnPoint)
				FMMC_CLEAR_LONG_BIT(iBSRespawnDeactivated[MC_PlayerBD[iLocalPart].iTeam], EventData.iRespawnPoint)
				SET_BIT(iLocalBoolCheck28, LBOOL28_OTHER_TEAM_ACTIVATED_MY_RESPAWN_POINTS)
				IF EventData.bDeactivateOthers
					iRespawnPointIndexToKeep = EventData.iRespawnPoint
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Vehicles	 -------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Processes for vehicle functionality  			--------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


PROC PROCESS_FMMC_EXIT_MISSION_MOC(INT iCount)
	SCRIPT_EVENT_DATA_FMMC_EXIT_MISSION_MOC Event
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, Event, SIZE_OF(Event))
		IF Event.Details.Type = SCRIPT_EVENT_FMMC_EXIT_MISSION_MOC
		
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciALLOW_MOC_ENTRY_THIS_MISSION)
				IF DOES_ENTITY_EXIST(MPGlobalsAmbience.vehCreatorTrailer)
				AND GET_OWNER_OF_CREATOR_TRAILER(MPGlobalsAmbience.vehCreatorTrailer) = LocalPlayer
					IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(LocalPlayer)].SimpleInteriorBD.iBSTwo,  BS_SIMPLE_INTERIOR_GLOBAL_BS_TWO_DATA_LEAVE_ARMORY_TRUCK_TRAILER_ON_DELIVER)
						SET_BIT(GlobalplayerBD[NATIVE_TO_INT(LocalPlayer)].SimpleInteriorBD.iBSTwo,  BS_SIMPLE_INTERIOR_GLOBAL_BS_TWO_DATA_LEAVE_ARMORY_TRUCK_TRAILER_ON_DELIVER) 
						CDEBUG1LN(DEBUG_SPAWNING, "Clearing BS_SIMPLE_INTERIOR_GLOBAL_DATA_KICK_PLAYERS_OUT_OF_ARMORY_TRUCK_INTERIOR")
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_DELIVER_VEHICLE_STOP(INT iEventID)
	PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_DELIVER_VEHICLE_STOP] - Processing...")
	
	SCRIPT_EVENT_DATA_FMMC_DELIVER_VEHICLE_STOP EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_DELIVER_VEHICLE_STOP
			IF bIsLocalPlayerHost
				PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_DELIVER_VEHICLE_STOP] - Host Processing - iVeh: ", EventData.iVehicle, " iTimeToStop: ", EventData.iTimeToStop)
				IF EventData.iVehicle > -1				
					IF EventData.iTimeToStop > 0						
						MC_serverBD_1.iVehStopTimeToWait = EventData.iTimeToStop
						SET_BIT(MC_serverBD_1.iBSVehicleStopDuration, EventData.iVehicle)	
						RESET_NET_TIMER(MC_serverBD_1.timeVehicleStop)
						START_NET_TIMER(MC_serverBD_1.timeVehicleStop)			
					ELSE						
						SET_BIT(MC_serverBD_1.iBSVehicleStopForever, EventData.iVehicle)
					ENDIF
				ENDIF
			ENDIF
		ENDIF	
	ENDIF
ENDPROC

PROC PROCESS_FMMC_FORCING_EVERYONE_FROM_VEH(INT iCount)
	
	SCRIPT_EVENT_DATA_FMMC_FORCING_EVERYONE_FROM_VEH EventData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_FORCING_EVERYONE_FROM_VEH
			
			IF iDeliveryVehForcingOut = -1
				
				INT iveh = EventData.iveh
				
				IF iveh >= 0 AND iveh < FMMC_MAX_VEHICLES
					
					NETWORK_INDEX niVeh = MC_serverBD_1.sFMMC_SBD.niVehicle[iveh]
					
					IF NETWORK_DOES_NETWORK_ID_EXIST(niVeh)
						
						VEHICLE_INDEX tempVeh = NET_TO_VEH(niVeh)
						
						IF IS_PED_IN_VEHICLE(LocalPlayerPed, tempVeh)
							iDeliveryVehForcingOut = iveh
							iPartSending_DeliveryVehForcingOut = EventData.iPart
							PRINTLN("[RCC MISSION] PROCESS_FMMC_FORCING_EVERYONE_FROM_VEH - Local player is in the same vehicle! Set iDeliveryVehForcingOut = ",iveh)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_SCRIPT_EVENT_NOTIFY_WANTED_SET_FOR_VEHICLE(INT iEventID)

	IF (bIsLocalPlayerHost)
	
		SCRIPT_EVENT_DATA_NOTIFY_WANTED_SET_FOR_VEHICLE EventData
		IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
			IF EventData.Details.Type = SCRIPT_EVENT_NOTIFY_WANTED_SET_FOR_VEHICLE
				PRINTLN("[MJL] PROCESS_SCRIPT_EVENT_NOTIFY_WANTED_SET_FOR_VEHICLE")		
				SET_BIT(MC_serverBD_1.iVehicleTheftWantedTriggered, EventData.iVeh)
				PRINTLN("    ----->    MARKING VEHICLE ", EventData.iVeh, " as having been stolen")
			ENDIF
		ENDIF
	
	ENDIF
ENDPROC

   
PROC PROCESS_SCRIPT_EVENT_FMMC_SET_VEHICLE_IMMOVABLE(INT iCount)
	SCRIPT_EVENT_DATA_FMMC_SET_VEHICLE_IMMOVABLE EventData
	IF NOT GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, EventData, SIZE_OF(EventData))
	OR EventData.Details.Type != SCRIPT_EVENT_FMMC_SET_VEHICLE_IMMOVABLE
		EXIT
	ENDIF
	
	IF !bIsLocalPlayerHost
		EXIT
	ENDIF
	
	IF EventData.bImmovable
		SET_BIT(MC_serverBD_4.iVehicleImmovableBitset, EventData.iVehicle)
	ELSE
		CLEAR_BIT(MC_serverBD_4.iVehicleImmovableBitset, EventData.iVehicle)
	ENDIF
	
	IF EventData.bHasChanged
		SET_BIT(MC_serverBD_4.iVehicleHadImmovableSetBitset, EventData.iVehicle)
	ENDIF
	
ENDPROC

   
PROC PROCESS_SCRIPT_EVENT_FMMC_REMOTE_VEHICLE_EXPLOSION(INT iCount)
	
	SCRIPT_EVENT_DATA_FMMC_REMOTE_VEHICLE_EXPLOSION EventData
	
	IF NOT GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, EventData, SIZE_OF(EventData))
	OR EventData.Details.Type != SCRIPT_EVENT_FMMC_REMOTE_VEHICLE_EXPLOSION
		EXIT
	ENDIF
	
	IF iRemoteVehicleExplosionInitiator = -1
		iRemoteVehicleExplosionInitiator = EventData.iInitiator
		PRINTLN("[RemoteExplosive] PROCESS_SCRIPT_EVENT_FMMC_REMOTE_VEHICLE_EXPLOSION - iRemoteVehicleExplosionInitiator set to: ", iRemoteVehicleExplosionInitiator)
	ENDIF
	
ENDPROC	

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: World	 -------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Processes for world functionality  			--------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_SCRIPT_EVENT_FMMC_DETONATE_PROP(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_DETONATE_PROP EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_DETONATE_PROP
			IF DOES_ENTITY_EXIST(oiProps[EventData.iProp])	
				PRINTLN("[KH][VEHICLE WEAPONS][LM] PROCESS_SCRIPT_EVENT_FMMC_DETONATE_PROP - Set Bit")
				// We have to set this to avoid using multiple timers to control fading/flashing of Props
				INT iPropEF = 0
				FOR iPropEF = 0 TO FMMC_MAX_NUM_FADEOUT_PROPS_EVERY_FRAME-1
					IF iPropFadeoutEveryFrameIndex[iPropEF] = -1
						iPropFadeoutEveryFrameIndex[iPropEF] = EventData.iProp
						iFlashToggle[iPropEF] = 1
						iFlashToggleTime[iPropEF] = (GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdMissionLengthTimer)/1000)						
						FMMC_SET_LONG_BIT(iPropCleanedupTriggeredBS, EventData.iprop)
						PRINTLN("[LM][PROCESS_PROPS] - ciFMMC_PROP_Cleanup_Triggered has been set on iProp: ", EventData.iProp)
						BREAKLOOP
					ENDIF
				ENDFOR
					
				IF NOT HAS_NET_TIMER_STARTED(tdFlashPropTimer)
					START_NET_TIMER(tdFlashPropTimer)
				ENDIF
				
				FMMC_SET_LONG_BIT(iPropDetonateTriggeredBS, EventData.iprop)
				PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_DETONATE_PROP] - Prop Index: ", EventData.iProp)
			ENDIF
		ENDIF	
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_PROP_DESTROYED(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_PROP_DESTROYED EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF bIsLocalPlayerHost
			#IF IS_DEBUG_BUILD
			IF NOT FMMC_IS_LONG_BIT_SET(MC_serverBD_3.iPropDestroyedBS, EventData.iProp)
				PRINTLN("PROCESS_SCRIPT_EVENT_FMMC_PROP_DESTROYED - Prop ", EventData.iProp, " has been destroyed! Setting iPropDestroyedBS[",GET_LONG_BITSET_INDEX(EventData.iProp),"], ",GET_LONG_BITSET_BIT(EventData.iProp),". This prop has been destroyed.")
			ENDIF
			#ENDIF
			FMMC_SET_LONG_BIT(MC_serverBD_3.iPropDestroyedBS, EventData.iProp)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_MOVE_FMMC_YACHT(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_MOVE_FMMC_YACHT EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_MOVE_FMMC_YACHT
			IF bIsLocalPlayerHost
				SET_BIT(MC_serverBD.iServerBitSet7, SBBOOL7_YACHT_CUTSCENE_RUNNING)
				PRINTLN("[MYACHT] Starting yacht cutscene!")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_SET_ARTIFICIAL_LIGHTS(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_SET_ARTIFICIAL_LIGHTS EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type != SCRIPT_EVENT_FMMC_SET_ARTIFICIAL_LIGHTS
			EXIT
		ENDIF
		
		PRINTLN("[ArtificialLights] PROCESS_SCRIPT_EVENT_FMMC_SET_ARTIFICIAL_LIGHTS")
		
		IF EventData.eStateToSet = eARTIFICIAL_LIGHTS_STATE__OFF
			IF NOT IS_VECTOR_ZERO(EventData.vEMPRestrictedInteriorOrigin)
				intEMPSpecifiedInterior = GET_INTERIOR_AT_COORDS(EventData.vEMPRestrictedInteriorOrigin)
				PRINTLN("[ArtificialLights] PROCESS_SCRIPT_EVENT_FMMC_SET_ARTIFICIAL_LIGHTS | Setting EMP specified interior to ", NATIVE_TO_INT(intEMPSpecifiedInterior))
			ENDIF
		ELSE
			IF intEMPSpecifiedInterior != NULL
				intEMPSpecifiedInterior = NULL
				PRINTLN("[ArtificialLights] PROCESS_SCRIPT_EVENT_FMMC_SET_ARTIFICIAL_LIGHTS | Clearing EMP specified interior")
			ENDIF
		ENDIF
		
		IF EventData.iEMPDuration > -1
			iMaxEMPDuration = EventData.iEMPDuration
			PRINTLN("[ArtificialLights] PROCESS_SCRIPT_EVENT_FMMC_SET_ARTIFICIAL_LIGHTS | Setting iMaxEMPDuration to ", iMaxEMPDuration)
		ELSE
			iMaxEMPDuration = -1
		ENDIF
		
		SWITCH EventData.eStateToSet
			CASE eARTIFICIAL_LIGHTS_STATE__ON
				TURN_ON_ARTIFICIAL_LIGHTS_LOCALLY(EventData.bPlaySound)
			BREAK
			CASE eARTIFICIAL_LIGHTS_STATE__OFF
				TURN_OFF_ARTIFICIAL_LIGHTS_LOCALLY(EventData.bPlaySound)
			BREAK
			CASE eARTIFICIAL_LIGHTS_STATE__DEFAULT
				RESET_ARTIFICIAL_LIGHTS_LOCALLY(EventData.bPlaySound)
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

PROC PROCESS_EVENT_FMMC_INTERIOR_DESTRUCTION(int iCount)

	SCRIPT_EVENT_DATA_FMMC_INTERIOR_DESTRUCTION_EVENT sEventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, sEventData, SIZE_OF(sEventData))
		INITIALISE_INTERIOR_DESTRUCTION(sEventData.iInteriorDestructionToStart)
	ENDIF
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Zones	 -------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Processes for zone functionality  			--------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_EVENT_FMMC_TRIGGERED_BODY_SCANNER(int iCount)

	SCRIPT_EVENT_DATA_FMMC_TRIGGERED_BODY_SCANNER ObjectiveData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))
		IF ObjectiveData.Details.Type = SCRIPT_EVENT_FMMC_TRIGGERED_BODY_SCANNER
			PRINTLN("[RCC MISSION] starting tdMissionBodyScannerTimer (event)")
			START_NET_TIMER(tdMissionBodyScannerTimer)	
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_FMMC_REQUEST_RESTRICTION_ROCKETS(INT iCount)
	
	SCRIPT_EVENT_DATA_FMMC_JUST_EVENT ObjectiveData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))
		IF ObjectiveData.Details.Type = SCRIPT_EVENT_FMMC_REQUEST_RESTRICTION_ROCKETS
			IF ObjectiveData.Details.FromPlayerIndex != NULL
				SET_BIT(iLocalBoolCheck6, LBOOL6_SOMEONE_NEEDS_ZONE_ROCKETS)
				PRINTLN("[ZoneRockets] Setting LBOOL6_SOMEONE_NEEDS_ZONE_ROCKETS")
			ELSE
				CLEAR_BIT(iLocalBoolCheck6, LBOOL6_SOMEONE_NEEDS_ZONE_ROCKETS)
				PRINTLN("[ZoneRockets] Clearing LBOOL6_SOMEONE_NEEDS_ZONE_ROCKETS")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_METAL_DETECTOR_ALERTED(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_METAL_DETECTOR EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_METAL_DETECTOR_ALERTED
			PRINTLN("[MetalDetector] Metal Detector Alerted event received! EventData.iZone: ", EventData.iZone, " / EventData.iDetectedPart: ", EventData.iDetectedPart, " / EventData.bExtremeWeapon: ", EventData.bExtremeWeapon)
			SET_METAL_DETECTOR_AS_ALERTED(EventData.iZone, EventData.vDetectionPos, EventData.bExtremeWeapon)
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_ZONE_TIMER(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_ZONE_TIMER EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_ZONE_TIMER
			PRINTLN("[ZoneTimer] Zone Timer event received! EventData.iZone: ", EventData.iZone, " / EventData.bStartTimer: ", EventData.bStartTimer, " / EventData.bStopTimer: ", EventData.bStopTimer)
			
			IF EventData.bStartTimer
				IF NOT HAS_NET_TIMER_STARTED(stZoneTimers[EventData.iZone])
					REINIT_NET_TIMER(stZoneTimers[EventData.iZone])
					PRINTLN("[ZoneTimer] Starting Zone Timer! Zone: ", EventData.iZone)
				ELSE
					PRINTLN("[ZoneTimer] Zone Timer already started! Zone: ", EventData.iZone)
				ENDIF
			ENDIF
			
			IF EventData.bStopTimer
				IF HAS_NET_TIMER_STARTED(stZoneTimers[EventData.iZone])
					RESET_NET_TIMER(stZoneTimers[EventData.iZone])
					PRINTLN("[ZoneTimer] Stopping Zone Timer! Zone: ", EventData.iZone)
				ELSE
					PRINTLN("[ZoneTimer] Requested to stop the zone timer, but this Zone Timer isn't running anyway! Zone: ", EventData.iZone)
				ENDIF
			ENDIF
			
			IF EventData.bMarkTimerAsComplete
				PRINTLN("[ZoneTimer] Marking Zone Timer as complete! Zone: ", EventData.iZone)
				SET_ZONE_TIMER_AS_COMPLETED(EventData.iZone)
			ENDIF
			
			IF EventData.bSkipTimer
				PRINTLN("[ZoneTimer] Skipping Zone Timer! Zone: ", EventData.iZone)
				SET_ZONE_TIMER_AS_COMPLETED(EventData.iZone)
			ENDIF
			
			IF EventData.bHideTimer
				PRINTLN("[ZoneTimer] Hiding Zone Timer! Zone: ", EventData.iZone)
				SET_ZONE_TIMER_AS_HIDDEN(EventData.iZone)
			ENDIF
			
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_ZONE_AIR_DEFENCE_SHOT(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_ZONE_AIR_DEFENCE_SHOT EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type != SCRIPT_EVENT_FMMC_ZONE_AIR_DEFENCE_SHOT
			EXIT
		ENDIF
		
		IF DOES_AIR_DEFENCE_SPHERE_EXIST(iFMMCAirDefenceZoneArea[EventData.iZone])
			PRINTLN("[Zones][Zone ", EventData.iZone, "] PROCESS_SCRIPT_EVENT_FMMC_ZONE_AIR_DEFENCE_SHOT | Firing air defence weapon at ", EventData.vShotPos)
			FIRE_AIR_DEFENCE_SPHERE_WEAPON_AT_POSITION(iFMMCAirDefenceZoneArea[EventData.iZone], EventData.vShotPos)
		ELSE
			PRINTLN("[Zones][Zone ", EventData.iZone, "] PROCESS_SCRIPT_EVENT_FMMC_ZONE_AIR_DEFENCE_SHOT | Creating air defence explosion at ", EventData.vShotPos)
			ADD_EXPLOSION(EventData.vShotPos, EXP_TAG_AIR_DEFENCE, 1.0)
		ENDIF
		
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_SOUND_ZONE(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_SOUND_ZONE EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type != SCRIPT_EVENT_FMMC_SOUND_ZONE
			EXIT
		ENDIF
		
		REINIT_NET_TIMER(stZoneResetTimer)
		
		IF !bIsLocalPlayerHost
			EXIT
		ENDIF
		
		SWITCH eventData.iSoundType
			CASE ciFMMC_ZONE_SOUND_PANTHER
				PLAY_SOUND_FROM_COORD(-1, "Distant_Roar", EventData.vSoundLocation, "DLC_H4_EscPan_Sounds", TRUE, EventData.iRange)
			BREAK
		ENDSWITCH
			
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Damage Event	 ---------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Processes for Damage event functionality  	--------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_BEAST_DESTOYED_ANOTHER_PLAYER(PED_INDEX pedVictim, PED_INDEX pedKiller #IF IS_DEBUG_BUILD , INT iPartVictim #ENDIF)
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iLocalPart].iteam].iRuleBitSetFive[MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam]], ciBS_RULE5_ENABLE_BEAST_MODE)
	OR IS_PARTICIPANT_A_BEAST(iLocalPart)
		IF pedVictim != LocalPlayerPed
			IF pedKiller = LocalPlayerPed
				PRINTLN("[MCDamageEvent][Player][Destroyed][", iPartVictim, "] - Beast killed another player")
				bBeastKilledPlayer = TRUE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_HEALTH_REGEN_FROM_PLAYER_DEATH(PED_INDEX pedVictim #IF IS_DEBUG_BUILD, INT iPartVictim #ENDIF)
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iLocalPart].iteam].iRuleBitSetSix[MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam]], ciBS_RULE6_ENABLE_HEALTH_DRAIN)
		IF pedVictim != LocalPlayerPed
			INT iDamage
			IF NETWORK_GET_ASSISTED_KILL_OF_ENTITY(LocalPlayer, pedVictim, iDamage)
				PRINTLN("[MCDamageEvent][Player][Destroyed][", iPartVictim, "] - [HEALTHDRAIN] - PROCESS_HEALTH_DRAIN_DAMAGE_EVENTS - Assisted Kill")
				fDrainHealPercentage += g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iLocalPart].iteam].sDrainStruct[MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam]].fVehicleHeal
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_TEAM_TIME_DECREMENT_FROM_PLAYER_DEATH(INT iPartVictim)
	INT iVictimRule = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartVictim].iTeam]
	IF iVictimRule < FMMC_MAX_RULES
		IF g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartVictim].iTeam].iDeathPenalty[iVictimRule] > 0
			INT iCurrentTimer
			INT iPenalty = g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartVictim].iTeam].iDeathPenalty[iVictimRule] * 1000
			
			IF IS_OBJECTIVE_TIMER_RUNNING(MC_playerBD[iPartVictim].iTeam)
				iCurrentTimer = GET_OBJECTIVE_TIMER_TIME_REMAINING(MC_playerBD[iPartVictim].iTeam, iVictimRule)
				IF (iCurrentTimer - iPenalty) > (g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartVictim].iTeam].iDeathPenaltyThreshold[iVictimRule] * 1000)
					PRINTLN("[MCDamageEvent][Player][Destroyed][", iPartVictim, "] - Decrementing team ", MC_playerBD[iPartVictim].iTeam,"'s timer due to player death")
					MC_serverBD_3.iTimerPenalty[MC_playerBD[iPartVictim].iTeam] += iPenalty
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_PLAYER_DEATH_AS_A_SUICIDE(STRUCT_ENTITY_DAMAGE_EVENT sEntityID, INT iPartVictim)
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciNEGATIVE_SUICIDE_DROWNING_IS_SUICIDE)
	OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciNEGATIVE_SUICIDE_FALL_IS_SUICIDE)
	OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciNEGATIVE_SUICIDE_UNKNOWN_SOURCES_IS_SUICIDE)
		PRINTLN("[MCDamageEvent][Player][Destroyed][", iPartVictim, "] - [LM][Drown] - iPartVictim is: ", 							iPartVictim)
		
		BOOL bOverrideDamager = FALSE				
		// If we died by drowning and there is no damager entity, then fudge it to make it appear as a suicide.
		IF iPartVictim = iLocalPart
		AND NOT DOES_ENTITY_EXIST(sEntityID.DamagerIndex)
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciNEGATIVE_SUICIDE_DROWNING_IS_SUICIDE)
				IF sEntityID.WeaponUsed = HASH("WEAPON_DROWNING")
				OR sEntityID.WeaponUsed = HASH("WEAPON_DROWNING_IN_VEHICLE")
					PRINTLN("[MCDamageEvent][Player][Destroyed][", iPartVictim, "] - [LM][Drown] Overriding damage due to detection of: WEAPON_DROWNING or WEAPON_DROWNING_IN_VEHICLE")
					bOverrideDamager = TRUE
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciNEGATIVE_SUICIDE_FALL_IS_SUICIDE)
				IF sEntityID.WeaponUsed = HASH("WEAPON_FALL")
					PRINTLN("[MCDamageEvent][Player][Destroyed][", iPartVictim, "] - [LM][Drown] Overriding damage due to detection of: WEAPON_FALL")
					bOverrideDamager = TRUE
				ENDIF
			ENDIF
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciNEGATIVE_SUICIDE_UNKNOWN_SOURCES_IS_SUICIDE)
				IF sEntityID.WeaponUsed = HASH("WEAPON_INVALID")
					PRINTLN("[MCDamageEvent][Player][Destroyed][", iPartVictim, "] - [LM][Drown] Overriding damage due to detection of: WEAPON_INVALID")
					bOverrideDamager = TRUE
				ENDIF
			ENDIF
		ENDIF
		
		IF bOverrideDamager
			PRINTLN("[MCDamageEvent][Player][Destroyed][", iPartVictim, "] - [LM][Drown] - Everything checks out. Forcing the player as the killer ")
			sEntityID.DamagerIndex  = localPlayerPed
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SWITCH_TEAM_ON_DEATH(INT iPartVictim, INT iPartKiller)
	IF iPartVictim = iLocalPart
	AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciCLOBBER_PLAYER_ON_TEAM_SWAP)
		IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
			INT iNewTeam = g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iSwapToTeamOnDeath[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iTeam]]
			
			IF iNewTeam != -1
			AND iNewTeam != MC_playerBD[iLocalPart].iteam
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iDontSwitchOnSuicide[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iTeam]], ciDYNAMIC_MENU_DONT_SWITCH_ON_SUICIDE)
					IF MC_playerBD[iLocalPart].iteam != MC_playerBD[iPartKiller].iteam 
						IF NOT IS_BIT_SET(iLocalBoolCheck16, LBOOL16_SHOULD_SHOW_TEAM_SWITCH_SHARD)
							SET_BIT(iLocalBoolCheck16, LBOOL16_SHOULD_SHOW_TEAM_SWITCH_SHARD)
						ENDIF
						CHANGE_PLAYER_TEAM(iNewTeam, TRUE)
						BROADCAST_FMMC_OBJECTIVE_PLAYER_SWITCHED_TEAMS(MC_playerBD[iLocalPart].iteam, iNewTeam)
						PRINTLN("[MCDamageEvent][Player][Destroyed][", iPartVictim, "] - Died (iDontSwitchOnSuicide is enabled), swapping to team ", iNewTeam)
					ELSE
						PRINTLN("[MCDamageEvent][Player][Destroyed][", iPartVictim, "] - Died (suicide), not switching team")
					ENDIF
				ELSE
					IF NOT IS_BIT_SET(iLocalBoolCheck16, LBOOL16_SHOULD_SHOW_TEAM_SWITCH_SHARD)
						SET_BIT(iLocalBoolCheck16, LBOOL16_SHOULD_SHOW_TEAM_SWITCH_SHARD)
					ENDIF
					PRINTLN("[MCDamageEvent][Player][Destroyed][", iPartVictim, "] - Died, swapping to team ", iNewTeam)
					CHANGE_PLAYER_TEAM(iNewTeam, TRUE)
					BROADCAST_FMMC_OBJECTIVE_PLAYER_SWITCHED_TEAMS(MC_playerBD[iLocalPart].iteam, iNewTeam)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_CUSTOM_WASTED_SHARD_KILLED_BY_DATA(INT iPartVictim, INT iPartKiller)
	IF MC_playerBD[iPartVictim].iteam >= 0
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartVictim].iteam].iTeamBitset2, ciBS2_CUSTOM_WASTED_SHARD)
			IF g_FMMC_STRUCT.iCustomWastedShardType[MC_playerBD[iPartVictim].iteam] = ciCUSTOM_WASTED_SHARD_BE_MY_VALENTINE
				IF MC_playerBD[iPartVictim].iteam = MC_playerBD[iLocalPart].iteam
					IF iPartVictim != iLocalPart
						IF iPartVictim != iPartKiller
							PRINTLN("[MCDamageEvent][Player][Destroyed][", iPartVictim, "] - TEAM MATE KILLED BY: ", iPartKiller)
							iPartTeammateWasKilledBy = iPartKiller
						ELSE
							//your team mate committed suicide
							PRINTLN("[MCDamageEvent][Player][Destroyed][", iPartVictim, "] - TEAM MATE COMMITTED SUICIDE: ", iPartKiller)
							iPartTeammateWasKilledBy = -3
						ENDIF
					ENDIF
				ENDIF
				IF iPartVictim = iLocalPart
					PRINTLN("[MCDamageEvent][Player][Destroyed][", iPartVictim, "] - YOU WERE KILLED BY - PLAYING WASTED SHARD: ", iPartKiller) 
					IF iPartTeammateWasKilledBy >= 0
						PRINTLN("[MCDamageEvent][Player][Destroyed][", iPartVictim, "] - YOU WERE KILLED BUT YOUR TEAM MATE WAS KILLED BY: ", iPartTeammateWasKilledBy) 
						REQUEST_PLAY_CUSTOM_SHARD(PLAY_SHARD_TYPE_CUSTOM_WASTED, MC_playerBD[iPartVictim].iteam, iPartVictim, iPartTeammateWasKilledBy)
					ELSE	
						iPartTeammateWasKilledBy = iPartKiller
						#IF IS_DEBUG_BUILD 
							IF iPartVictim = iPartKiller
								PRINTLN("[MCDamageEvent][Player][Destroyed][", iPartVictim, "] - YOU COMMITED SUICIDE")
							ENDIF
						#ENDIF
						
						PRINTLN("[MCDamageEvent][Player][Destroyed][", iPartVictim, "] - YOU WERE KILLED BY - PLAYING WASTED SHARD: ", iPartKiller) 
						REQUEST_PLAY_CUSTOM_SHARD(PLAY_SHARD_TYPE_CUSTOM_WASTED, MC_playerBD[iPartVictim].iteam, iPartVictim, iPartKiller)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC	

PROC PROCESS_SET_PED_HAS_BEEN_HIT(STRUCT_ENTITY_DAMAGE_EVENT sEntityID, INT iPedVictim, ENTITY_INDEX eiDamagerIndex)
	IF sEntityID.IsHeadShot
		IF NOT IS_BIT_SET(sMissionPedsLocalVars[iPedVictim].iPedBS, ci_MissionPedBS_HasBeenShotInTheHead)
			PRINTLN("[MCDamageEvent][Ped][Destroyed][", iPedVictim, "] - [MCDamageEvent][Ped][Damaged][", iPedVictim, "] - Headshot has been performed on iPed: ", iPedVictim)
			SET_BIT(sMissionPedsLocalVars[iPedVictim].iPedBS, ci_MissionPedBS_HasBeenShotInTheHead)
		ENDIF
	ENDIF
	sMissionPedsLocalVars[iPedVictim].eiLastHitBy = eiDamagerIndex
ENDPROC

PROC PROCESS_HEALTH_BOOST_ON_KILL(INT iPartKiller #IF IS_DEBUG_BUILD , INT iPartVictim #ENDIF)
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciHEALTH_BOOST_ON_KILL)
	AND NOT (ARE_WE_CURRENTLY_IN_SUDDEN_DEATH() AND IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_BE_MY_VALENTINES))	//Don't give players health back in sudden death
		IF MC_playerBD[iLocalPart].iteam = MC_playerBD[iPartKiller].iteam 
			IF bLocalPlayerPedOK
				INT iMaxHealth 
				INT iHealthToAdd
				iMaxHealth = GET_ENTITY_MAX_HEALTH(LocalPlayerPed)
				iHealthToAdd = ROUND(iMaxHealth / 3.0)
				PRINTLN("[MCDamageEvent][Player][Destroyed][", iPartVictim, "] - ciHEALTH_BOOST_ON_KILL - Giving Health to local player")
				SET_ENTITY_HEALTH(LocalPlayerPed, GET_ENTITY_HEALTH(LocalPlayerPed) + iHealthToAdd)
			ENDIF
		ELSE
			PRINTLN("[MCDamageEvent][Player][Destroyed][", iPartVictim, "] - ciHEALTH_BOOST_ON_KILL in on but teams don't match. Local Player Team: " , MC_playerBD[iLocalPart].iteam , "Killer Team: " , MC_playerBD[iPartKiller].iteam)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_JUGGERNAUT_PTFX_ON_KILL(INT iPartKiller, INT iPartVictim)
	IF IS_PARTICIPANT_A_JUGGERNAUT(iLocalPart)
	AND bLocalPlayerPedOK
		IF iLocalPart = iPartKiller
		AND iPartKiller != iPartVictim
			USE_PARTICLE_FX_ASSET("scr_impexp_jug")
			START_NETWORKED_PARTICLE_FX_NON_LOOPED_ON_PED_BONE("scr_ie_jug_mask_flame", LocalPlayerPed, vJuggernautVFXFire1Pos, vJuggernautVFXFire1Rot, BONETAG_HEAD)
			USE_PARTICLE_FX_ASSET("scr_impexp_jug")
			START_NETWORKED_PARTICLE_FX_NON_LOOPED_ON_PED_BONE("scr_ie_jug_mask_flame", LocalPlayerPed, vJuggernautVFXFire2Pos, vJuggernautVFXFire2Rot, BONETAG_HEAD)
			REINIT_NET_TIMER(tdJuggernautVFX)
		ENDIF
	ENDIF
ENDPROC

PROC ATTEMPT_PICKUP_DROP_FOR_PED(INT iPedVictim, PED_INDEX PedVictim)
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedVictim].iPedBitsetSixteen, ciPED_BSSixteen_SetPedPickupDropsAsHostAuthorative)
	OR bIsLocalPlayerHost
						
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedVictim].iDropPickupIndex <= -1
			PRINTLN("[MCDamageEvent][Ped][Destroyed][", iPedVictim, "] - Attempt Drop Pickup is set but no index was selected.")
			EXIT
		ENDIF
				
		IF iPickupRespawns[g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedVictim].iDropPickupIndex] <= 0
		AND g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedVictim].iDropPickupIndex].iWepRespawnCount != 0		
			PRINTLN("[MCDamageEvent][Ped][Destroyed][", iPedVictim, "] - Attempt Drop Pickup: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedVictim].iDropPickupIndex, " is set but no the pickup has no more respawns left.")
			EXIT
		ENDIF
		
		IF FMMC_IS_LONG_BIT_SET(iDroppedPickupBS, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedVictim].iDropPickupIndex)
			PRINTLN("[MCDamageEvent][Ped][Destroyed][", iPedVictim, "] - Attempt Drop Pickup cannot be performed. Pickup has already been dropped and has not de-spawned.")
			EXIT
		ENDIF
		
		INT iRoll = GET_RANDOM_INT_IN_RANGE(0, 100)
		PRINTLN("[MCDamageEvent][Ped][Destroyed][", iPedVictim, "] - Attempt Drop Pickup is set with index: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedVictim].iDropPickupIndex, " and a percent chance of: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedVictim].iDropPickupChance, " We rolled an: ", iRoll)
		IF iRoll <= g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedVictim].iDropPickupChance
		OR g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedVictim].iDropPickupChance = 0
		OR FMMC_IS_LONG_BIT_SET(iPedForceDropPickup, iPedVictim)
			
			//Might neeed to find a way to adjust vDropLocation, but don't ground Z because collision might not be loaded in on entities dying far from the local player!
			VECTOR vDropLocation = GET_ENTITY_COORDS(pedVictim, FALSE)
			PRINTLN("[MCDamageEvent][Ped][Destroyed][", iPedVictim, "] - Setting Pickup to be dropped at death location: ", vDropLocation)
				
			FMMC_SET_LONG_BIT(iDroppedPickupBS, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedVictim].iDropPickupIndex)
			vDroppedPickupLocation[g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedVictim].iDropPickupIndex] = vDropLocation
				
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedVictim].iPedBitsetSixteen, ciPED_BSSixteen_SetPedPickupDropsAsHostAuthorative)										
				BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_PedDroppedPickupHostEvent, DEFAULT, iPedVictim, DEFAULT, DEFAULT, DEFAULT, DEFAULT, FALSE, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedVictim].iDropPickupIndex, DEFAULT, DEFAULT, DEFAULT, vDropLocation.x, vDropLocation.y, vDropLocation.z)
			ELSE					
				BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_ClearForcePedDropPickupIndexChance, DEFAULT, iPedVictim, DEFAULT, DEFAULT, DEFAULT, DEFAULT, FALSE, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedVictim].iDropPickupIndex)
			ENDIF
		ENDIF
		
	ENDIF
ENDPROC

PROC PRIVATE_PROCESS_ENTITY_SUICIDE_EVENT(PLAYER_INDEX VictimPlayerID, INT iVictimPart, INT iKillerPart = -1)
	PRINTLN("[RCC MISSION] PRIVATE_PROCESS_ENTITY_SUICIDE_EVENT - Victim: ",iVictimPart, " Killer: ", iKillerPart) 
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciCLOBBER_PLAYER_ON_TEAM_SWAP)
	AND LocalPlayer = VictimPlayerID 
	AND iKillerPart = -1
		PRINTLN("[RCC MISSION][MMacK][ServerSwitch] SUICIDE SQUAD")
		IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam] < FMMC_MAX_RULES
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iDontSwitchOnSuicide[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]], ciDYNAMIC_MENU_DONT_SWITCH_ON_SUICIDE)
				PLAYER_INDEX piTempKiller = GET_PLAYER_FROM_LOSING_TEAM_FOR_SWAP()
				
				INT iNewTeam = g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iSwapToTeamOnDeath[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]]	
				
				IF iNewTeam != -1
				AND iNewTeam != MC_playerBD[iPartToUse].iteam
				AND VictimPlayerID != piTempKiller
					IF bIsLocalPlayerHost
						PRINTLN("[RCC MISSION][MMacK][ServerSwitch] Processing Inside Damage Event")
						PROCESS_PLAYER_TEAM_SWITCH(iVictimPart, piTempKiller, TRUE)
					ELSE
						BROADCAST_FMMC_PLAYER_TEAM_SWITCH_REQUEST(MC_ServerBD.iSessionScriptEventKey, MC_playerBD[iPartToUse].iteam, iNewTeam, iVictimPart, piTempKiller, TRUE)
						PRINTLN("[RCC MISSION][MMacK][ServerSwitch] Broadcasting Request")
					ENDIF
					
					MC_PlayerBD[iLocalPart].bSwapTeamStarted = TRUE
					REINIT_NET_TIMER(tdTeamSwapFailSafe)
				ELSE
					IF VictimPlayerID = piTempKiller
						PRINTLN("[RCC MISSION][MMacK][ServerSwitch] NO VALID PLAYER TO SWAP WITH")
					ELSE
						PRINTLN("[RCC MISSION][MMacK][ServerSwitch] SWAP IS NOT VALID", iNewTeam)
					ENDIF
				ENDIF
				iPartLastDamager = -1
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_TEAM_SUICIDE_EVENT(INT iTeam, PLAYER_INDEX piKiller, INT iPartVictim, INT iPartKiller)
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciNEGATIVE_SUICIDE_SCORE)
	AND MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES	
		IF bIsLocalPlayerHost
			INT igamestage = GET_MC_CLIENT_MISSION_STAGE(iPartKiller)
			PRINTLN("[MCDamageEvent][Player][Destroyed][", iPartVictim, "] - GET_MC_CLIENT_MISSION_STAGE: ",igamestage) 
			PRINTLN("[MCDamageEvent][Player][Destroyed][", iPartVictim, "] - host subtracting team kills ") 
			IF igamestage = CLIENT_MISSION_STAGE_KILL_PLAYERS
				IF MC_serverBD.iTeamPlayerKills[MC_playerBD[iPartKiller].iteam][MC_playerBD[iPartVictim].iteam] > 0
					MC_serverBD.iTeamPlayerKills[MC_playerBD[iPartKiller].iteam][MC_playerBD[iPartVictim].iteam]--
					PRINTLN("[MCDamageEvent][Player][Destroyed][", iPartVictim, "] - MC_serverBD.iTeamPlayerKills decreased")
				ENDIF
			ELIF igamestage = CLIENT_MISSION_STAGE_KILL_TEAM0
				IF MC_playerBD[iPartVictim].iteam = 0
				AND MC_serverBD.iTeamPlayerKills[MC_playerBD[iPartKiller].iteam][MC_playerBD[iPartVictim].iteam] > 0
					MC_serverBD.iTeamPlayerKills[MC_playerBD[iPartKiller].iteam][MC_playerBD[iPartVictim].iteam]--
					PRINTLN("[MCDamageEvent][Player][Destroyed][", iPartVictim, "] - MC_serverBD.iTeamPlayerKills of team 0 decreased for team: ",MC_playerBD[iPartKiller].iteam) 
				ENDIF
			ELIF igamestage = CLIENT_MISSION_STAGE_KILL_TEAM1
				IF MC_playerBD[iPartVictim].iteam = 1
				AND MC_serverBD.iTeamPlayerKills[MC_playerBD[iPartKiller].iteam][MC_playerBD[iPartVictim].iteam] > 0
					MC_serverBD.iTeamPlayerKills[MC_playerBD[iPartKiller].iteam][MC_playerBD[iPartVictim].iteam]--
					PRINTLN("[MCDamageEvent][Player][Destroyed][", iPartVictim, "] - MC_serverBD.iTeamPlayerKills of team 1 decreased for team: ",MC_playerBD[iPartKiller].iteam)
				ENDIF
			ELIF igamestage = CLIENT_MISSION_STAGE_KILL_TEAM2
				IF MC_playerBD[iPartVictim].iteam = 2
				AND MC_serverBD.iTeamPlayerKills[MC_playerBD[iPartKiller].iteam][MC_playerBD[iPartVictim].iteam] > 0
					MC_serverBD.iTeamPlayerKills[MC_playerBD[iPartKiller].iteam][MC_playerBD[iPartVictim].iteam]--
					PRINTLN("[MCDamageEvent][Player][Destroyed][", iPartVictim, "] - MC_serverBD.iTeamPlayerKills of team 2 decreased for team: ",MC_playerBD[iPartKiller].iteam)
				ENDIF
			ELIF igamestage = CLIENT_MISSION_STAGE_KILL_TEAM3
				IF MC_playerBD[iPartVictim].iteam = 3
				AND MC_serverBD.iTeamPlayerKills[MC_playerBD[iPartKiller].iteam][MC_playerBD[iPartVictim].iteam] > 0
					MC_serverBD.iTeamPlayerKills[MC_playerBD[iPartKiller].iteam][MC_playerBD[iPartVictim].iteam]--
					PRINTLN("[MCDamageEvent][Player][Destroyed][", iPartVictim, "] - MC_serverBD.iTeamPlayerKills of team 3 decreased for team: ",MC_playerBD[iPartKiller].iteam) 
				ENDIF
			ENDIF
		ENDIF
		
		IF LocalPlayer = piKiller
			PRINTLN("[MCDamageEvent][Player][Destroyed][", iPartVictim, "] - We are the Killer and Victim. PlayerScore: ", MC_playerBD[iPartToUse].iPlayerScore, " PlayerKills: ", MC_Playerbd[iPartToUse].iKillScore) 
																
			IF MC_serverBD.iNumPlayerRuleHighestPriority[MC_playerBD[iPartToUse].iteam] > 0 
				PRINTLN("[MCDamageEvent][Player][Destroyed][", iPartVictim, "] - more than 0 highest ") 
				IF MC_serverBD_4.iPlayerRuleMissionLogic[MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_KILL_PLAYERS // this high priority kill player objective
					IF MC_playerBD[iPartToUse].iPlayerScore > 0
						PRINTLN("[MCDamageEvent][Player][Destroyed][", iPartVictim, "] - [JS] - SUICIDE POINTS - decrementing local player's score (1)")
						INCREMENT_LOCAL_PLAYER_SCORE_FOR_CURRENT_OBJECTIVE(-1)
					ENDIF
				ELIF MC_serverBD_4.iPlayerRuleMissionLogic[MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_KILL_TEAM0	
				OR MC_serverBD_4.iPlayerRuleMissionLogic[MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_KILL_TEAM1	
				OR MC_serverBD_4.iPlayerRuleMissionLogic[MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_KILL_TEAM2
				OR MC_serverBD_4.iPlayerRuleMissionLogic[MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_KILL_TEAM3
					IF MC_playerBD[iPartToUse].iPlayerScore > 0
					AND MC_Playerbd[iPartToUse].iKillScore > 0
						PRINTLN("[MCDamageEvent][Player][Destroyed][", iPartVictim, "] - [JS] - SUICIDE POINTS - decrementing local player's score (2)")
						INCREMENT_LOCAL_PLAYER_SCORE_FOR_CURRENT_OBJECTIVE(-1,TRUE)
					ENDIF
				ENDIF	
			ELSE
				IF g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iPointsPerKill > 0
					IF MC_playerBD[iPartToUse].iPlayerScore >= g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iPointsPerKill
						PRINTLN("[MCDamageEvent][Player][Destroyed][", iPartVictim, "] - [JS] - SUICIDE POINTS - iPoints per kill = ", g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iPointsPerKill)
						INCREMENT_LOCAL_PLAYER_SCORE_BY((g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iPointsPerKill * -1),TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF 
ENDPROC

FUNC BOOL IS_FLIPPED_ALREADY_IN_ARRAY(VEHICLE_INDEX FlippedVeh)
	INT i
	FOR i = 0 TO DUNE_MAX_FLIPS -1
		IF viFlippedVeh[i] = FlippedVeh
			RETURN TRUE
		ENDIF
	ENDFOR
	RETURN FALSE
ENDFUNC
PROC DUNE_FLIP_MARK_FOR_EXPLOSION(ENTITY_INDEX VictimIndex, VEHICLE_INDEX VictimVehicle, ENTITY_INDEX DamagerIndex)
	IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		IF DOES_ENTITY_EXIST(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
			IF IS_VEHICLE_MODEL(GET_VEHICLE_PED_IS_IN(LocalPlayerPed, TRUE),INT_TO_ENUM(MODEL_NAMES, HASH("DUNE4")))
			AND GET_DAMAGER_PARTICIPANT_INDEX(DamagerIndex) = iLocalPart
				IF DOES_ENTITY_EXIST(VictimIndex)
					IF IS_ENTITY_A_VEHICLE(VictimIndex)
						INT i
						FOR i = 0 TO DUNE_MAX_FLIPS -1
							IF (viFlippedVeh[i] != VictimVehicle AND viFlippedVeh[i] = NULL)
							AND NOT HAS_NET_TIMER_STARTED(stFlipTimer[i])
							AND NOT IS_FLIPPED_ALREADY_IN_ARRAY(VictimVehicle)
							AND NOT DOES_VEHICLE_RESIST_DUNE_FLIP(VictimVehicle)	
								viFlippedVeh[i] = VictimVehicle
								NETWORK_REQUEST_CONTROL_OF_ENTITY(viFlippedVeh[i])
								PRINTLN("[JT FLIP] FLIPPED NEW VEHICLE! FLIP COUNT: ", i, " Flipped vehicle: ", NATIVE_TO_INT(viFlippedVeh[i]))
								BREAKLOOP
							ENDIF
						ENDFOR
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SWAP_TEAM_DAMAGE_EVENTS(INT iCount) //fmmc2020 can be added to damage event function or deleted
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciSWAP_TO_KILLERS_TEAM)
		EXIT
	ENDIF
	
	STRUCT_ENTITY_DAMAGE_EVENT Event
	PED_INDEX piVictim
	PED_INDEX piDamager
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, Event, SIZE_OF(Event))
		
		INT iKillersTeam
		
		IF DOES_ENTITY_EXIST(Event.VictimIndex)
		AND IS_ENTITY_A_PED(Event.VictimIndex)
			piVictim = GET_PED_INDEX_FROM_ENTITY_INDEX(Event.VictimIndex)
			IF IS_PED_A_PLAYER(piVictim)
				IF piVictim != LocalPlayerPed
				AND GET_PLAYER_TEAM(NETWORK_GET_PLAYER_INDEX_FROM_PED(piVictim)) = GET_PLAYER_TEAM(LocalPlayer)
					IF DOES_ENTITY_EXIST(Event.DamagerIndex)
					AND IS_ENTITY_A_PED(Event.DamagerIndex)
						piDamager = GET_PED_INDEX_FROM_ENTITY_INDEX(Event.DamagerIndex)
						IF IS_PED_A_PLAYER(piDamager)
							IF piDamager != LocalPlayerPed
								IF Event.VictimDestroyed
									PRINTLN("[JS] PROCESS_SWAP_TEAM_DAMAGE_EVENTS - Someone on my team was killed")
									iKillersTeam = GET_PLAYER_TEAM(NETWORK_GET_PLAYER_INDEX_FROM_PED(piDamager))
									
									IF iKillersTeam != -1
									AND iKillersTeam != MC_playerBD[iLocalPart].iteam
										PRINTLN("[JS] PROCESS_SWAP_TO_KILLERS_TEAM - Died, swapping to team ", iKillersTeam)
										CHANGE_PLAYER_TEAM(iKillersTeam, TRUE)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_PED_ON_DEATH_PTFX_SPAWNING_WITH_PLAYER_INSTIGATOR(INT iPlayerTeam, INT iPlayerRule, PED_INDEX &pedVictim)
	//On Rule Options
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iPlayerTeam].iRuleBitsetSixteen[iPlayerRule], ciBS_RULE16_PED_SPAWN_CLOWN_DEATH_PTFX)
		USE_PARTICLE_FX_ASSET("scr_rcbarry2")
		START_NETWORKED_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_clown_death", GET_ENTITY_COORDS(pedVictim, FALSE), GET_ENTITY_ROTATION(pedVictim), 3.0)
		PRINTLN("PROCESS_PED_ON_DEATH_PTFX_SPAWNING_WITH_PLAYER_INSTIGATOR - Calling START_NETWORKED_PARTICLE_FX_NON_LOOPED_AT_COORD with \"scr_clown_death\"")
	ELIF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iPlayerTeam].iRuleBitsetSixteen[iPlayerRule], ciBS_RULE16_PED_SPAWN_ALIEN_DEATH_PTFX)
		USE_PARTICLE_FX_ASSET("scr_rcbarry1")
		START_NETWORKED_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_alien_teleport", GET_ENTITY_COORDS(pedVictim, FALSE), GET_ENTITY_ROTATION(pedVictim), 3.0)
		PRINTLN("PROCESS_PED_ON_DEATH_PTFX_SPAWNING_WITH_PLAYER_INSTIGATOR - Calling START_NETWORKED_PARTICLE_FX_NON_LOOPED_AT_COORD with \"scr_alien_teleport\"")
	ENDIF
ENDPROC

PROC PROCESS_PED_ON_DEATH_AUDIO_WITH_PLAYER_INSTIGATOR(INT iPlayerTeam, INT iPlayerRule)
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iPlayerTeam].iRuleBitsetSixteen[iPlayerRule], ciBS_RULE16_PED_PLAY_CLOWN_DEATH_AUDIO)
		PLAY_SOUND(-1, "HOORAY", "BARRY_02_SOUNDSET", TRUE, 5000)
		PRINTLN("PROCESS_PED_ON_DEATH_AUDIO_WITH_PLAYER_INSTIGATOR - Calling PLAY_SOUND with \"HOORAY\"")
	ENDIF
ENDPROC

PROC PROCESS_ENTITY_DESTROYED_EVENT_PED(STRUCT_ENTITY_DAMAGE_EVENT sEntityID, INT iPedVictim, PED_INDEX pedVictim)
	
	PRINTLN("[MCDamageEvent][Ped][Destroyed][", iPedVictim, "] - Has been Destroyed")
	
	INT iTeam	
	
	// Host Check
	IF bIsLocalPlayerHost
		IF iPedVictim >= 0
		AND NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[iPedVictim])
			PROCESS_SET_PED_HAS_BEEN_HIT(sEntityID, iPedVictim, sEntityID.DamagerIndex)
			
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedVictim].iPedBitsetFive, ciPED_BSFive_AggroWhenHurtOrKilled)
			AND NOT FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedSpookedBitset, iPedVictim)
			AND NOT FMMC_IS_LONG_BIT_SET(iPedHitByStunGun, iPedVictim)
				PRINTLN("[MCDamageEvent][Ped][Destroyed][", iPedVictim, "] - SPOOKED as ped killed had ciPED_BSFive_AggroWhenHurtOrKilled set")
				
				#IF IS_DEBUG_BUILD SET_PED_IS_SPOOKED_DEBUG(iPedVictim) #ENDIF
				FMMC_SET_LONG_BIT(iPedLocalSpookedBitset, iPedVictim) // broadcast
				FMMC_SET_LONG_BIT(MC_serverBD.iPedSpookedBitset, iPedVictim)
				BROADCAST_FMMC_PED_SPOOKED(iPedVictim)
												
				FMMC_PED_STATE sPedState
				FILL_FMMC_PED_STATE_STRUCT(sPedState, iPedVictim)

				FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
					IF SHOULD_AGRO_FAIL_FOR_TEAM(iPedVictim, iTeam)
						FMMC_SET_LONG_BIT(iPedLocalAggroedBitset, iPedVictim)
						TRIGGER_AGGRO_ON_PED(sPedState)
						iTeam = MC_serverBD.iNumberOfTeams // Break out, we found an aggro team!
					ENDIF
				ENDFOR
				
				FMMC_SET_LONG_BIT(iPedLocalReceivedEvent_HurtOrKilled, iPedVictim)
				BROADCAST_FMMC_PED_SPOOK_HURTORKILLED(iPedVictim)
			ENDIF
			
			IF sEntityID.WeaponUsed = ENUM_TO_INT(WEAPONTYPE_DLC_TRANQUILIZER)
				IF IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_PedSpecialTracking)
				AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedVictim].iContinuityId > -1
				AND g_FMMC_STRUCT.eContinuitySpecialPedType = FMMC_MISSION_CONTINUITY_PED_TRACKING_TYPE_TRANQUILIZED
				AND NOT FMMC_IS_LONG_BIT_SET(MC_serverBD_1.sMissionContinuityVars.iPedSpecialBitset, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedVictim].iContinuityId)
					FMMC_SET_LONG_BIT(MC_serverBD_1.sMissionContinuityVars.iPedSpecialBitset, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedVictim].iContinuityId)
					PRINTLN("[MCDamageEvent][Ped][Destroyed][", iPedVictim, "] - [JS][CONTINUITY] - with continuityID ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedVictim].iContinuityId, " was tranqed")
				ENDIF
			ELSE	
				IF IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_PedDeathTracking)
				AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedVictim].iContinuityId > -1
					FMMC_SET_LONG_BIT(MC_serverBD_1.sMissionContinuityVars.iPedDeathBitset, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedVictim].iContinuityId)
					PRINTLN("[MCDamageEvent][Ped][Destroyed][", iPedVictim, "] - [JS][CONTINUITY] - with continuityID ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedVictim].iContinuityId, " was killed")
				ENDIF
			ENDIF
			
			FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
				IF NOT HAS_TEAM_FAILED(iTeam)
				AND MC_serverBD.iNumPedHighestPriority[iteam] = 1 // This has recalculate_objective_logic in it, so we don't want to go crazy on massive kill rules
				AND MC_serverBD_4.iPedPriority[iPedVictim][iteam] <= MC_serverBD_4.iCurrentHighestPriority[iteam]
				AND (MC_serverBD_4.iPedRule[iPedVictim][iteam] = FMMC_OBJECTIVE_LOGIC_KILL OR MC_serverBD_4.iPedRule[iPedVictim][iteam] = FMMC_OBJECTIVE_LOGIC_LEAVE_ENTITY)
					RUN_TEAM_PED_FAIL_CHECKS(iPedVictim,iteam,MC_serverBD_4.iPedPriority[iPedVictim][iteam])
					RECALCULATE_OBJECTIVE_LOGIC(iteam)
				ENDIF
			ENDFOR
			
			IF NOT FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iPedKilledBS, iPedVictim)
				PRINTLN("[MCDamageEvent][Ped][Destroyed][", iPedVictim, "] - Setting ped killed bit ", iPedVictim)
				FMMC_SET_LONG_BIT(MC_serverBD_4.iPedKilledBS, iPedVictim)
			ENDIF
			
			IF g_FMMC_STRUCT.sPedAmmoDrop.iAmmoPickupType != FMMC_PED_AMMO_DROP_TYPE_INVALID
			AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedVictim].iPedBitsetSeventeen, ciPed_BSSeventeen_DisableAmmoDrop)
				DROP_AMMO_FROM_PED(pedVictim)
			ENDIF

			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedVictim].iSpawnGroupAdditionalFunctionalityBS, ciSPAWN_GROUP_ADDITIONAL_ENTITY_FUNCTIONALITY_EVENT_IS_DEATH)			
				MODIFY_SPAWN_GROUP_DURING_GAMEPLAY_FROM_BITSET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedVictim].iSpawnGroupAdditionalFunctionalityBS, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedVictim].iModifySpawnGroupOnEvent, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedVictim].iModifySpawnSubGroupOnEventBS)
			ENDIF
		ENDIF
	ENDIF
	
	IF iPedVictim >= 0
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedVictim].iPedBitsetFifteen, ciPED_BSFifteen_AttemptDropPickupOnDeath)		
			ATTEMPT_PICKUP_DROP_FOR_PED(iPedVictim, pedVictim)
		ENDIF
		CLEAR_PED_ASS_GOTO_DATA(iPedVictim)
	ENDIF
	
	IF DOES_ENTITY_EXIST(sEntityID.DamagerIndex)
		IF IS_ENTITY_A_PED(sEntityID.DamagerIndex)
			PED_INDEX Killerped = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.DamagerIndex)
			IF IS_PED_A_PLAYER(Killerped)
				IF LocalPlayerPed = Killerped
					IF MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam] < FMMC_MAX_RULES
						IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iLocalPart].iteam].iRuleBitSetSix[MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam]], ciBS_RULE6_ENABLE_HEALTH_DRAIN)
							PRINTLN("[MCDamageEvent][Ped][Destroyed][", iPedVictim, "] - [JS] [HEALTHDRAIN] - PROCESS_HEALTH_DRAIN_DAMAGE_EVENTS - Killed a non-player ped")
							fDrainHealPercentage += g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iLocalPart].iteam].sDrainStruct[ MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam] ].fPedHeal
						ENDIF
					ENDIF
					
					IF iPedVictim >= 0
					
						PROCESS_SET_PED_HAS_BEEN_HIT(sEntityID, iPedVictim, sEntityID.DamagerIndex)
						
						BOOL bGiveRP = TRUE
						
						IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedVictim].iTeam[MC_playerBD[iPartToUse].iteam] = ciPED_RELATION_SHIP_HATE
						AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedVictim].iPedBitsetFourteen, ciPED_BSFourteen_PedIsACivilian)
							IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
								IF NOT IS_MODEL_AMBIENT_COP_FOR_RP(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedVictim].mn)

									IF MC_playerBD[iPartToUse].iNumPedKills < g_sMPTunables.iMISSION_AI_KILL_AMOUNT_CAP
									AND bGiveRP
										PRINTLN("[MCDamageEvent][Ped][Destroyed][", iPedVictim, "] - GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION, 1 ")
										GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD,pedVictim, "XPT_KAIE",XPTYPE_ACTION,XPCATEGORY_ACTION_KILLS, g_sMPTunables.iMISSION_AI_KILL_RP, 1)
									ELSE
										PRINTLN("[MCDamageEvent][Ped][Destroyed][", iPedVictim, "] - GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION, FAIL 1, iNumPedKills = ", MC_playerBD[iPartToUse].iNumPedKills, " CAP = ", g_sMPTunables.iMISSION_AI_KILL_AMOUNT_CAP)
									ENDIF

								ENDIF
							ENDIF
							
							PRINTLN("[MCDamageEvent][Ped][Destroyed][", iPedVictim, "] - MC_playerBD[iPartToUse].iNumPedKills test 0 MC_playerBD[iPartToUse].iNumPedKills BEFORE ", MC_playerBD[iPartToUse].iNumPedKills) 
							
							MC_playerBD[iPartToUse].iNumPedKills++
							
							PRINTLN("[MCDamageEvent][Ped][Destroyed][", iPedVictim, "] - MC_playerBD[iPartToUse].iNumPedKills test 0 - iNumPedKills = ", MC_playerBD[iPartToUse].iNumPedKills)
							
							INT iPlayerInstigatorTeam = GET_PLAYER_TEAM(GET_PLAYER_INDEX_FROM_ENTITY(sEntityID.DamagerIndex))
							INT iPlayerInstigatorRule = GET_TEAM_CURRENT_RULE(iPlayerInstigatorTeam)		
							
							IF  IS_TEAM_INDEX_VALID(iPlayerInstigatorTeam) 
							AND IS_RULE_INDEX_VALID(iPlayerInstigatorRule)
								PROCESS_PED_ON_DEATH_PTFX_SPAWNING_WITH_PLAYER_INSTIGATOR(iPlayerInstigatorTeam, iPlayerInstigatorRule, pedVictim)
								PROCESS_PED_ON_DEATH_AUDIO_WITH_PLAYER_INSTIGATOR(iPlayerInstigatorTeam, iPlayerInstigatorRule)
							ENDIF
						ELIF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedVictim].iTeam[MC_playerBD[iPartToUse].iteam] = ciPED_RELATION_SHIP_DISLIKE										
							IF WILL_PED_EVER_BE_HOSTILE(iPedVictim)
							AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedVictim].iPedBitsetFourteen, ciPED_BSFourteen_PedIsACivilian)
								IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
									IF NOT IS_MODEL_AMBIENT_COP_FOR_RP(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedVictim].mn)

										IF MC_playerBD[iPartToUse].iNumPedKills < g_sMPTunables.iMISSION_AI_KILL_AMOUNT_CAP
										AND bGiveRP	
											PRINTLN("[MCDamageEvent][Ped][Destroyed][", iPedVictim, "] - GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION, 2 ")
											GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD,pedVictim, "XPT_KAIE",XPTYPE_ACTION,XPCATEGORY_ACTION_KILLS, g_sMPTunables.iMISSION_AI_KILL_RP, 1)
										ELSE
											PRINTLN("[MCDamageEvent][Ped][Destroyed][", iPedVictim, "] - GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION, FAIL 2, iNumPedKills = ", MC_playerBD[iPartToUse].iNumPedKills, " CAP = ", g_sMPTunables.iMISSION_AI_KILL_RP)
										ENDIF

									ENDIF
								ENDIF
								
								MC_playerBD[iPartToUse].iNumPedKills++
								
								PRINTLN("[MCDamageEvent][Ped][Destroyed][", iPedVictim, "] - MC_playerBD[iPartToUse].iNumPedKills test 1")
								
							ELSE
								PRINTLN("[MCDamageEvent][Ped][Destroyed][", iPedVictim, "] - MC_playerBD[iPartToUse].iNumPedKills test 4")
								MC_playerBD[iPartToUse].iCivilianKills++
							ENDIF
							
							PRINTLN("[MCDamageEvent][Ped][Destroyed][", iPedVictim, "] - MC_playerBD[iPartToUse].iNumPedKills test 5")
							
						ENDIF
						
						IF DOES_BLIP_EXIST(biHostilePedBlip[iPedVictim].BlipID)
							PRINTLN("[MCDamageEvent][Ped][Destroyed][", iPedVictim, "] - Clearing Blip biHostilePedBlip: ", iPedVictim)
							CLEANUP_AI_PED_BLIP(biHostilePedBlip[iPedVictim])
						ELIF DOES_BLIP_EXIST(biPedBlip[iPedVictim])
							PRINTLN("[MCDamageEvent][Ped][Destroyed][", iPedVictim, "] - Clearing Blip biPedBlip: ", iPedVictim)
							REMOVE_BLIP(biPedBlip[iPedVictim])
						ENDIF
						
						PRINTLN("[MCDamageEvent][Ped][Destroyed][", iPedVictim, "] - MC_playerBD[iPartToUse].iNumPedKills = ", MC_playerBD[iPartToUse].iNumPedKills)
						
						IF MC_serverBD_4.iPedPriority[iPedVictim][MC_playerBD[iPartToUse].iteam] = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]

							PRINTLN("[MCDamageEvent][Ped][Destroyed][", iPedVictim, "] - ped is current objective: ", iPedVictim)
							IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedVictim].iTeam[MC_playerBD[iPartToUse].iteam] = ciPED_RELATION_SHIP_DISLIKE
							OR g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedVictim].iTeam[MC_playerBD[iPartToUse].iteam] = ciPED_RELATION_SHIP_HATE
								BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_KILL_PED,0,MC_playerBD[iPartToUse].iteam,-1,INVALID_PLAYER_INDEX(),ci_TARGET_PED,iPedVictim)
							ELSE
								BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_KILL_FRIENDLY,0,MC_playerBD[iPartToUse].iteam,-1,INVALID_PLAYER_INDEX(),ci_TARGET_PED,iPedVictim)
							ENDIF
							
							IF MC_serverBD_4.iPedPriority[iPedVictim][MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
								IF  MC_serverBD_4.iPedRule[iPedVictim][MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_KILL
									BROADCAST_OBJECTIVE_MID_POINT(MC_serverBD_4.iPedPriority[iPedVictim][MC_playerBD[iPartToUse].iTeam],MC_playerBD[iPartToUse].iteam)
									PRINTLN("[MCDamageEvent][Ped][Destroyed][", iPedVictim, "] - ped is kill objective: ",iPedVictim) 
									IF DOES_PED_MEET_SCORING_REQUIREMENTS_FOR_CURRENT_RULE(iPedVictim, MC_playerBD[iPartToUse].iTeam)
										INCREMENT_LOCAL_PLAYER_SCORE_FOR_CURRENT_OBJECTIVE(DEFAULT, DEFAULT, DEFAULT, iPedVictim)
									ENDIF
								ELSE
									GIVE_TEAM_POINTS_FOR_PED_KILL(iPedVictim,pedVictim,Killerped)												
								ENDIF
							ELSE
								GIVE_TEAM_POINTS_FOR_PED_KILL(iPedVictim,pedVictim,Killerped)
							ENDIF
						ELSE
							GIVE_TEAM_POINTS_FOR_PED_KILL(iPedVictim,pedVictim,Killerped)
							PRINTLN("[MCDamageEvent][Ped][Destroyed][", iPedVictim, "] - ped is not current objective: ",iPedVictim)
							IF MC_serverBD_4.iPedPriority[iPedVictim][MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
								IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedVictim].iTeam[MC_playerBD[iPartToUse].iteam] = ciPED_RELATION_SHIP_DISLIKE
								OR g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedVictim].iTeam[MC_playerBD[iPartToUse].iteam] = ciPED_RELATION_SHIP_HATE
									PRINTLN("[MCDamageEvent][Ped][Destroyed][", iPedVictim, "] - ped hates youe: ") NET_PRINT_INT(iPedVictim) NET_NL()
									BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_KILL_PED,0,MC_playerBD[iPartToUse].iteam,-1,INVALID_PLAYER_INDEX(),ci_TARGET_PED,iPedVictim)
								ELSE
									BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_KILL_FRIENDLY,0,MC_playerBD[iPartToUse].iteam,-1,INVALID_PLAYER_INDEX(),ci_TARGET_PED,iPedVictim)
								ENDIF
							ENDIF
						ENDIF
						
						PRINTLN("[MCDamageEvent][Ped][Destroyed][", iPedVictim, "] - MC_playerBD[iPartToUse].iNumHeadshots test 0")
						IF sEntityID.IsHeadShot
							MC_playerBD[iPartToUse].iNumHeadshots++
							PRINTLN("[MCDamageEvent][Ped][Destroyed][", iPedVictim, "] - MC_playerBD[iPartToUse].iNumHeadshots test 1", MC_playerBD[iPartToUse].iNumHeadshots)
						ELSE
							PRINTLN("[MCDamageEvent][Ped][Destroyed][", iPedVictim, "] - MC_playerBD[iPartToUse].iNumHeadshots test 2", MC_playerBD[iPartToUse].iNumHeadshots)
							CLEAR_BIT(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_ALL_HEADSHOTS)
						ENDIF
						
						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedVictim].iPedBitsetTwelve, ciPed_BSTwelve_UseDamageScoreContribution)
							MC_playerBD_1[iPartToUse].iDamageToPedsForMedal += ROUND(sEntityID.Damage)
							PRINTLN("[MCDamageEvent][Ped][Destroyed][", iPedVictim, "] - damaged specific ped (dead): ", iPedVictim, " Total: ", MC_playerBD_1[iPartToUse].iDamageToPedsForMedal)
						ENDIF
					ENDIF
				ELSE
					GIVE_TEAM_POINTS_FOR_PED_KILL(iPedVictim,pedVictim,Killerped)
				ENDIF
			ELSE
				GIVE_TEAM_POINTS_FOR_PED_KILL(iPedVictim,pedVictim)
			ENDIF
		ELSE
			GIVE_TEAM_POINTS_FOR_PED_KILL(iPedVictim,pedVictim)
		ENDIF
	ELSE
		GIVE_TEAM_POINTS_FOR_PED_KILL(iPedVictim,pedVictim)
	ENDIF
	
	SET_BIT(iLocalBoolCheck31, LBOOL31_ANY_PED_DESTROYED_EVENT_RECEIVED_FOR_DIALOGUE)
	
	IF iPedVictim >= 0
		IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxFlickeringInvisiblePedEffects[iPedVictim])
			REMOVE_PARTICLE_FX(ptfxFlickeringInvisiblePedEffects[iPedVictim])
			PRINTLN("[MCDamageEvent][Ped][Destroyed][", iPedVictim, "] - because it's dead")
		ENDIF		
	ENDIF
	
	MC_PLAY_DEATH_SOUND_FOR_PED(iPedVictim, pedVictim)
	
ENDPROC

PROC PROCESS_ENTITY_DESTROYED_EVENT_VEH(STRUCT_ENTITY_DAMAGE_EVENT sEntityID, INT iVehVictim, VEHICLE_INDEX vehVictim)
	
	PRINTLN("[MCDamageEvent][Vehicle][Destroyed][", iVehVictim, "] - has been destroyed.")
		
	INT iteam
		
	IF iVehVictim >= 0			
		// Host Check
		IF bIsLocalPlayerHost					
			IF IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_VehicleDestroyedTracking)
			AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVehVictim].iContinuityId > -1
				SET_BIT(MC_serverBD_1.sMissionContinuityVars.iVehicleDestroyedTrackingBitset, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVehVictim].iContinuityId)
				PRINTLN("[MCDamageEvent][Vehicle][Destroyed][", iVehVictim, "] - [JS][CONTINUITY] - with continuityID ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVehVictim].iContinuityId, " was killed")
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVehVictim].iSpawnGroupAdditionalFunctionalityBS, ciSPAWN_GROUP_ADDITIONAL_ENTITY_FUNCTIONALITY_EVENT_IS_DEATH)			
				MODIFY_SPAWN_GROUP_DURING_GAMEPLAY_FROM_BITSET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVehVictim].iSpawnGroupAdditionalFunctionalityBS, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVehVictim].iModifySpawnGroupOnEvent, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVehVictim].iModifySpawnSubGroupOnEventBS)
			ENDIF
		ENDIF
		
		IF NOT IS_BIT_SET(iVehHasBeenDamagedOrHarmedBS, iVehVictim)
			PRINTLN("[MCDamageEvent][Vehicle][Destroyed][", iVehVictim, "] - Setting iVehHasBeenDamagedOrHarmedBS for iVehVictim: ", iVehVictim)
			SET_BIT(iVehHasBeenDamagedOrHarmedBS, iVehVictim)
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(sEntityID.DamagerIndex)
		PRINTLN("[MCDamageEvent][Vehicle][Destroyed][", iVehVictim, "] - DOES_ENTITY_EXIST(sEntityID.DamagerIndex)")
		IF IS_ENTITY_A_PED(sEntityID.DamagerIndex)
			PED_INDEX pedKiller = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.DamagerIndex)
			IF IS_PED_A_PLAYER(pedKiller)				
				IF LocalPlayerPed = pedKiller
					IF MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam] < FMMC_MAX_RULES
						IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iLocalPart].iteam].iRuleBitSetSix[MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam]], ciBS_RULE6_ENABLE_HEALTH_DRAIN)
							PRINTLN("[MCDamageEvent][Vehicle][Destroyed][", iVehVictim, "] - [JS] [HEALTHDRAIN] - PROCESS_HEALTH_DRAIN_DAMAGE_EVENTS - Destroyed a vehicle")
							fDrainHealPercentage += g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iLocalPart].iteam].sDrainStruct[ MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam] ].fVehicleHeal
						ENDIF
					ENDIF
					
					IF iVehVictim >= 0
						PRINTLN("[MCDamageEvent][Vehicle][Destroyed][", iVehVictim, "] - >= 0") 
						IF NOT IS_BIT_SET(MC_serverBD_2.iVehIsDestroyed, iVehVictim)
					
							IF MC_serverBD_4.iVehPriority[iVehVictim][MC_playerBD[iPartToUse].iteam] = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]
								PRINTLN("[MCDamageEvent][Vehicle][Destroyed][", iVehVictim, "] - veh is the current priority") 
								BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_KILL_VEH,0,MC_playerBD[iPartToUse].iteam,-1,INVALID_PLAYER_INDEX(),ci_TARGET_VEHICLE, iVehVictim)
								IF MC_serverBD_4.iVehPriority[iVehVictim][MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
									IF MC_serverBD_4.ivehRule[iVehVictim][MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_KILL
										IF NOT IS_BIT_SET(iCountThisVehAsDestroyedBS, iVehVictim)
											PRINTLN("[MCDamageEvent][Vehicle][Destroyed][", iVehVictim, "] - logic is kill") 
											BROADCAST_OBJECTIVE_MID_POINT(MC_serverBD_4.iVehPriority[iVehVictim][MC_playerBD[iPartToUse].iteam], MC_playerBD[iPartToUse].iteam)

											INCREMENT_LOCAL_PLAYER_SCORE_FOR_CURRENT_OBJECTIVE()

											SET_BIT(iCountThisVehAsDestroyedBS, iVehVictim)
										ELSE
											PRINTLN("[MCDamageEvent][Vehicle][Destroyed][", iVehVictim, "] - iCountThisVehAsDestroyedBS is set") 
										ENDIF
									ELSE
										PRINTLN("[MCDamageEvent][Vehicle][Destroyed][", iVehVictim, "] - GIVE_TEAM_POINTS_FOR_VEH_KILL") 
										GIVE_TEAM_POINTS_FOR_VEH_KILL(iVehVictim, vehVictim, pedKiller)
									ENDIF
								ELSE
									PRINTLN("[MCDamageEvent][Vehicle][Destroyed][", iVehVictim, "] - GIVE_TEAM_POINTS_FOR_VEH_KILL") 
									GIVE_TEAM_POINTS_FOR_VEH_KILL(iVehVictim, vehVictim, pedKiller)
								ENDIF
							ELSE
								PRINTLN("[MCDamageEvent][Vehicle][Destroyed][", iVehVictim, "] - GIVE_TEAM_POINTS_FOR_VEH_KILL") 
								GIVE_TEAM_POINTS_FOR_VEH_KILL(iVehVictim, vehVictim, pedKiller)
								BOOL bIgnore
								FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
									IF MC_serverBD_4.iVehPriority[iVehVictim][iteam] >= FMMC_MAX_RULES
										bIgnore = TRUE
									ENDIF
								ENDFOR
								IF NOT bIgnore
									PRINTLN("[MCDamageEvent][Vehicle][Destroyed][", iVehVictim, "] - veh not current prior: ", iVehVictim) 
									BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_KILL_VEH, 0, MC_playerBD[iPartToUse].iteam, -1, INVALID_PLAYER_INDEX(), ci_TARGET_VEHICLE, iVehVictim)
								ENDIF
							ENDIF
							
							//If it's a hostile vehicle:
							IF IS_BIT_SET(iEnemyVehBitSet, iVehVictim)
								MC_playerBD[iPartToUse].iVehDestroyed++																			
								PRINTLN("[MCDamageEvent][Vehicle][Destroyed][", iVehVictim, "] - iVehVictim >= 0 MC_playerBD[iPartToUse].iVehDestroyed 1 = ", MC_playerBD[iPartToUse].iVehDestroyed)
							ENDIF
							
							IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVehVictim].iVehBitsetEight, ciFMMC_VEHICLE8_USE_DAMAGE_SCORE_CONTRIBUTION)
								MC_playerBD_1[iPartToUse].iDamageToVehsForMedal += ROUND(sEntityID.Damage)
								PRINTLN("[MCDamageEvent][Vehicle][Destroyed][", iVehVictim, "] - damaged specific veh (dead): ", iVehVictim, " Total: ", MC_playerBD_1[iPartToUse].iDamageToVehsForMedal)
							ENDIF
							
						ELSE
							PRINTLN("[MCDamageEvent][Vehicle][Destroyed][", iVehVictim, "] - IS_BIT_SET(MC_serverBD_2.iVehIsDestroyed, iVehVictim)") 
						ENDIF
													
						IF NOT IS_BIT_SET(iVehHasBeenDamagedOrHarmedByPlayerBS, iVehVictim)
							PRINTLN("[MCDamageEvent][Vehicle][Destroyed][", iVehVictim, "] - Setting iVehHasBeenDamagedOrHarmedByPlayerBS for iVeh: ", iVehVictim)
							SET_BIT(iVehHasBeenDamagedOrHarmedByPlayerBS, iVehVictim)
						ENDIF		
					ELSE
						PRINTLN("[MCDamageEvent][Vehicle][Destroyed][", iVehVictim, "] - >= 0 - FALSE")
						//Gang backup vehicles:
						IF IS_VEHICLE_A_GANG_CHASE_VEHICLE(vehVictim)
							MC_playerBD[iPartToUse].iVehDestroyed++
						ENDIF
					ENDIF
					
				ELSE
					GIVE_TEAM_POINTS_FOR_VEH_KILL(iVehVictim, vehVictim, pedKiller)
				ENDIF
			ELSE
				GIVE_TEAM_POINTS_FOR_VEH_KILL(iVehVictim, vehVictim)
			ENDIF
		ELSE
			GIVE_TEAM_POINTS_FOR_VEH_KILL(iVehVictim, vehVictim)
		ENDIF
	ELSE
		PRINTLN("[MCDamageEvent][Vehicle][Destroyed][", iVehVictim, "] - VEHICLE DESTROYED ITSELF")
		GIVE_TEAM_POINTS_FOR_VEH_KILL(iVehVictim, vehVictim)
	ENDIF
	
	CLEAR_BIT(iLocalBoolCheck29, LBOOL29_REMOTE_DETONATE_VEHICLE_CHECKED_THIS_RULE)
	PRINTLN("[MCDamageEvent][Vehicle][Destroyed][", iVehVictim, "] - Clearing LBOOL29_REMOTE_DETONATE_VEHICLE_CHECKED_THIS_RULE")
		
ENDPROC

PROC PROCESS_ENTITY_DESTROYED_EVENT_OBJ(STRUCT_ENTITY_DAMAGE_EVENT sEntityID, INT iObjVictim, OBJECT_INDEX oiVictim)
	INT iTeam
	INT iMyTeam = MC_playerBD[iPartToUse].iteam	
	BOOL bnotignore
	
	PRINTLN("[MCDamageEvent][Object][Destroyed][", iObjVictim, "] - has been Destroyed")
		
	// Host Check
	IF bIsLocalPlayerHost
		IF iObjVictim >= 0
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjVictim].iObjectBitSetTwo, cibsOBJ2_DontSpawnIfDestroyed)
				IF NOT IS_BIT_SET(MC_serverBD_4.iDestroyedTurretBitSet, iObjVictim)
					SET_BIT(MC_serverBD_4.iDestroyedTurretBitSet, iObjVictim)
					PRINTLN("[MCDamageEvent][Object][Destroyed][", iObjVictim, "] - Setting MC_serverBD_4.iDestroyedTurretBitSet for object: ", iObjVictim)
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjVictim].iSpawnGroupAdditionalFunctionalityBS, ciSPAWN_GROUP_ADDITIONAL_ENTITY_FUNCTIONALITY_EVENT_IS_DEATH)		
			MODIFY_SPAWN_GROUP_DURING_GAMEPLAY_FROM_BITSET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjVictim].iSpawnGroupAdditionalFunctionalityBS, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjVictim].iModifySpawnGroupOnEvent, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjVictim].iModifySpawnSubGroupOnEventBS)
		ENDIF
	ENDIF
	
	HANDLE_OBJECT_DESTROY_HUD(oiVictim)
		
	IF DOES_ENTITY_EXIST(sEntityID.DamagerIndex)
		IF IS_ENTITY_A_PED(sEntityID.DamagerIndex)
			PED_INDEX Killerped = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.DamagerIndex)
			IF IS_PED_A_PLAYER(Killerped)		
				IF LocalPlayerPed = Killerped
					IF MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam] < FMMC_MAX_RULES
						IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iLocalPart].iteam].iRuleBitSetSix[MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam]], ciBS_RULE6_ENABLE_HEALTH_DRAIN)
							PRINTLN("[MCDamageEvent][Object][Destroyed][", iObjVictim, "] - [JS] [HEALTHDRAIN] - PROCESS_HEALTH_DRAIN_DAMAGE_EVENTS - Destroyed an object adding ",g_FMMC_STRUCT.sFMMCEndConditions[ MC_PlayerBD[iLocalPart].iteam ].sDrainStruct[ MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam] ].fObjectTimer, " seconds to the current timer (",(iStopDrainTime/1000) ,"seconds)" )
							iStopDrainTime = ROUND(g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iLocalPart].iteam].sDrainStruct[MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam]].fObjectTimer * 1000)
						ENDIF
					ENDIF
				
					IF iObjVictim >= 0
						IF MC_serverBD_4.iObjPriority[iObjVictim][iMyTeam] = MC_serverBD_4.iCurrentHighestPriority[ iMyTeam ]
							PRINTLN("[MCDamageEvent][Object][Destroyed][", iObjVictim, "] - is my team ",iMyTeam,"'s current objective")
							BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_KILL_OBJ, 0, iMyTeam, -1, localPlayer, ci_TARGET_OBJECT, iObjVictim)
							IF MC_serverBD_4.iObjPriority[iObjVictim][iMyTeam] < FMMC_MAX_RULES
								IF MC_serverBD_4.iObjRule[iObjVictim][iMyTeam] = FMMC_OBJECTIVE_LOGIC_KILL
									BROADCAST_OBJECTIVE_MID_POINT(MC_serverBD_4.iObjPriority[iObjVictim][iMyTeam], iMyTeam )
									PRINTLN("[MCDamageEvent][Object][Destroyed][", iObjVictim, "] - current objective is kill for my team ", iMyTeam)
									//MC_playerBD[iPartToUse].iNumObjKills++
									
									INCREMENT_LOCAL_PLAYER_SCORE_FOR_CURRENT_OBJECTIVE()
								ELSE
									PRINTLN("[MCDamageEvent][Object][Destroyed][", iObjVictim, "] - GIVE_TEAM_POINTS_FOR_OBJ_KILL, object not on a kill rule for my team ",iMyTeam," iObjRule = ",MC_serverBD_4.iObjRule[iObjVictim][ iMyTeam ])
									GIVE_TEAM_POINTS_FOR_OBJ_KILL(iObjVictim, oiVictim, Killerped)
								ENDIF
							ELSE
								PRINTLN("[MCDamageEvent][Object][Destroyed][", iObjVictim, "] - GIVE_TEAM_POINTS_FOR_OBJ_KILL, object priority > FMMC_MAX_RULES for my team ",iMyTeam)
								GIVE_TEAM_POINTS_FOR_OBJ_KILL(iObjVictim, oiVictim,  Killerped)
							ENDIF								
						ELSE
							PRINTLN("[MCDamageEvent][Object][Destroyed][", iObjVictim, "] - GIVE_TEAM_POINTS_FOR_OBJ_KILL, object priority ",MC_serverBD_4.iObjPriority[iObjVictim][ iMyTeam ]," for my team ",iMyTeam," is not current")
							GIVE_TEAM_POINTS_FOR_OBJ_KILL(iObjVictim, oiVictim,  Killerped)
							FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
								IF MC_serverBD_4.iObjPriority[iObjVictim][iteam] < FMMC_MAX_RULES
									bnotignore = TRUE
								ENDIF
							ENDFOR
							IF bnotignore
								PRINTLN("[MCDamageEvent][Object][Destroyed][", iObjVictim, "] - obj is not current objective")
								BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_KILL_OBJ,0, iMyTeam ,-1,localPlayer,ci_TARGET_OBJECT, iObjVictim)
							ENDIF
						ENDIF
						
						FOR iTeam = 0 TO FMMC_MAX_TEAMS - 1
							IF MC_serverBD_4.iObjPriority[iObjVictim][iTeam] < FMMC_MAX_RULES
								RUN_TEAM_OBJ_FAIL_CHECKS(iObjVictim, iTeam, MC_serverBD_4.iObjPriority[iObjVictim][iTeam])
							ENDIF
						ENDFOR
					#IF IS_DEBUG_BUILD
					ELSE
						PRINTLN("[MCDamageEvent][Object][Destroyed][", iObjVictim, "] - obj is not a mission creator object")
					#ENDIF
					ENDIF
					
				ELSE
					PRINTLN("[MCDamageEvent][Object][Destroyed][", iObjVictim, "] - GIVE_TEAM_POINTS_FOR_OBJ_KILL, damager is not the local player")
					GIVE_TEAM_POINTS_FOR_OBJ_KILL(iObjVictim, oiVictim, Killerped)
				ENDIF
			ELSE
				PRINTLN("[MCDamageEvent][Object][Destroyed][", iObjVictim, "] - GIVE_TEAM_POINTS_FOR_OBJ_KILL, damager is not a player")
				GIVE_TEAM_POINTS_FOR_OBJ_KILL(iObjVictim, oiVictim)
			ENDIF
		ELSE
			PRINTLN("[MCDamageEvent][Object][Destroyed][", iObjVictim, "] - GIVE_TEAM_POINTS_FOR_OBJ_KILL, damager is not a ped")
			GIVE_TEAM_POINTS_FOR_OBJ_KILL(iObjVictim, oiVictim)
		ENDIF
	ELSE
		PRINTLN("[MCDamageEvent][Object][Destroyed][", iObjVictim, "] - GIVE_TEAM_POINTS_FOR_OBJ_KILL, damager does not exist")
		GIVE_TEAM_POINTS_FOR_OBJ_KILL(iObjVictim, oiVictim)
	ENDIF
	
	IF IS_MODEL_A_SEA_MINE(GET_ENTITY_MODEL(oiVictim))
		VECTOR vMinePos = GET_ENTITY_COORDS(oiVictim)
		
		IF NOT IS_EXPLOSION_IN_SPHERE(EXP_TAG_MINE_UNDERWATER, vMinePos, 4)
			ADD_EXPLOSION(vMinePos, EXP_TAG_MINE_UNDERWATER, 8)
			PRINTLN("[MCDamageEvent][Object][Destroyed][", iObjVictim, "] - Adding explosion for sea mine that has been attacked")
		ELSE
			PRINTLN("[MCDamageEvent][Object][Destroyed][", iObjVictim, "] - NOT adding explosion for sea mine that has been attacked - it seems to already be exploding itself")
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_USE_CUSTOM_DEATH_EXPLOSION_PTFX(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjVictim].mn)
		PRINTLN("[MCDamageEvent][Object][Destroyed][", iObjVictim, "][SCRIPT_OBJECT_", NETWORK_ENTITY_GET_OBJECT_ID(oiVictim), "] PROCESS_ENTITY_DESTROYED_EVENT_OBJ | should play custom explosion")
		// This object has a custom death explosion effect!
		CREATE_CUSTOM_DEATH_EXPLOSION_PTFX_FOR_ENTITY(oiVictim)
	ELIF SHOULD_DRONE_SYSTEMS_BE_ACTIVE(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjVictim].sObjDroneData)
		PLAY_TASER_PTFX_ON_ENTITY(oiVictim, iObjVictim, sObjTaserVars)		
	ENDIF
	
	IF IS_SCRIPTED_FLAMMABLE_ENTITY_DAMAGE_SCRIPT_CONTROLLED(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjVictim].mn)
		INT iAudioSlot = GET_ENTITY_LOOPING_AUDIO_INDEX_FOR_ENTITY(oiVictim)
		IF iAudioSlot > -1
			PRINTLN("[MCDamageEvent][Object][Destroyed][", iObjVictim, "][FlammableObject][SCRIPT_OBJECT_", NETWORK_ENTITY_GET_OBJECT_ID(oiVictim), "] PROCESS_ENTITY_DESTROYED_EVENT_OBJ | Ending fire audio now! iAudioSlot: ", iAudioSlot)
			CLEANUP_ENTITY_LOOPING_AUDIO(iAudioSlot)
		ENDIF
	ENDIF
	
	IF SHOULD_DRONE_SYSTEMS_BE_ACTIVE(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjVictim].sObjDroneData)
		INT iDrone = GET_DRONE_INDEX_FROM_ENTITY_ID_AND_TYPE(iObjVictim, CREATION_TYPE_OBJECTS)
		IF NETWORK_HAS_CONTROL_OF_ENTITY(oiVictim)			
			STOP_FMMC_DRONE_FLIGHT_LOOP_SFX(iDrone)
			PLAY_FMMC_DRONE_DESTROYED_SFX(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjVictim].mn, GET_ENTITY_COORDS(oiVictim, FALSE))
		ENDIF
		SET_FMMC_DRONE_IS_NOW_DEAD(iDrone)
	ENDIF
	
	IF GET_MODEL_COMPONENTS_TO_DAMAGE_WHEN_FRAGGING(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjVictim].mn) > 0
	OR GET_MODEL_COMPONENTS_TO_BREAK_WHEN_FRAGGING(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjVictim].mn) > 0
		PRINTLN("[MCDamageEvent][Objects][Object ", iObjVictim, "][Destroyed][", iObjVictim, "] - Force-fragging object now")
		FRAG_ENTITY_COMPLETELY(oiVictim)
	ENDIF
	
ENDPROC

PROC PROCESS_ENTITY_DESTROYED_EVENT_DYNOPROP(STRUCT_ENTITY_DAMAGE_EVENT sEntityID, INT iDynoProp, OBJECT_INDEX oiVictim)
		
	PRINTLN("[MCDamageEvent][DynoProp][Destroyed][", iDynoProp, "] - has been Destroyed")
		
	IF DOES_ENTITY_EXIST(sEntityID.DamagerIndex)
		IF IS_ENTITY_A_PED(sEntityID.DamagerIndex)
			PED_INDEX Killerped = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.DamagerIndex)
			IF IS_PED_A_PLAYER(Killerped)		
				IF LocalPlayerPed = Killerped
					IF MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam] < FMMC_MAX_RULES
						IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iLocalPart].iteam].iRuleBitSetSix[MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam]], ciBS_RULE6_ENABLE_HEALTH_DRAIN)
							PRINTLN("[JS] [HEALTHDRAIN] - PROCESS_HEALTH_DRAIN_DAMAGE_EVENTS - Destroyed an object adding ",g_FMMC_STRUCT.sFMMCEndConditions[ MC_PlayerBD[iLocalPart].iteam ].sDrainStruct[ MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam] ].fObjectTimer, " seconds to the current timer (",(iStopDrainTime/1000) ,"seconds)" )
							iStopDrainTime = ROUND(g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iLocalPart].iteam].sDrainStruct[MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam]].fObjectTimer * 1000)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF	
	
	IF IS_MODEL_A_SEA_MINE(GET_ENTITY_MODEL(oiVictim))
		VECTOR vMinePos = GET_ENTITY_COORDS(oiVictim)
		
		IF NOT IS_EXPLOSION_IN_SPHERE(EXP_TAG_MINE_UNDERWATER, vMinePos, 4)
			ADD_EXPLOSION(vMinePos, EXP_TAG_MINE_UNDERWATER, 8)
			PRINTLN("[MCDamageEvent][DynoProp][Destroyed][", iDynoProp, "] - Adding explosion for sea mine that has been attacked")
		ELSE
			PRINTLN("[MCDamageEvent][DynoProp][Destroyed][", iDynoProp, "] - NOT adding explosion for sea mine that has been attacked - it seems to already be exploding itself")
		ENDIF
	ENDIF
			
	IF iDynoProp > -1
	AND iDynoProp < FMMC_MAX_NUM_DYNOPROPS
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_DynoPropDestroyedTracking)
		AND g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].iContinuityId > -1
		AND NOT IS_BIT_SET(MC_serverBD_1.sMissionContinuityVars.iDynoPropDestroyedTrackingBitset, g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].iContinuityId)
			SET_BIT(MC_serverBD_1.sMissionContinuityVars.iDynoPropDestroyedTrackingBitset, g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].iContinuityId)
			PRINTLN("[MCDamageEvent][DynoProp][Destroyed][", iDynoProp, "] - [JS][CONTINUITY] - with continuityID ", g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].iContinuityId, " was destroyed")
		ENDIF
		
	ENDIF
	
	IF NOT IS_STRING_EMPTY(GET_PTFX_ASSET_NAME_FOR_ENTITY_CUSTOM_DEATH_EXPLOSION(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].mn))
		// This dynoprop has a custom death explosion effect!
		CREATE_CUSTOM_DEATH_EXPLOSION_PTFX_FOR_ENTITY(oiVictim)
	ENDIF
	
ENDPROC

FUNC BOOL SHOULD_INTERACTABLE_DISAPPEAR_UPON_DEATH(INT iInteractable)
	IF g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].mnInteractable_Model = INT_TO_ENUM(MODEL_NAMES, HASH("h4_Prop_h4_Chain_Lock_01a"))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_ENTITY_DESTROYED_EVENT_INTERACTABLE(STRUCT_ENTITY_DAMAGE_EVENT sEntityID, INT iInteractable, OBJECT_INDEX oiVictim)
	
	UNUSED_PARAMETER(oiVictim)
	PRINTLN("[MCDamageEvent][Interactables][Interactable ", iInteractable, "][Destroyed][", iInteractable, "] - has been Destroyed")
		
	IF iInteractable != -1
	AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iSpawnGroupAdditionalFunctionalityBS, ciSPAWN_GROUP_ADDITIONAL_ENTITY_FUNCTIONALITY_EVENT_IS_DEATH)
		MODIFY_SPAWN_GROUP_DURING_GAMEPLAY_FROM_BITSET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iSpawnGroupAdditionalFunctionalityBS, g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iModifySpawnGroupOnEvent, g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iModifySpawnSubGroupOnEventBS)
	ENDIF
		
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iInteractableBS, ciInteractableBS_ActivateConsequencesOnDeath)
		BOOL bAPlayerKilledInteractable = FALSE
		BOOL bLocalPlayerKilledInteractable = FALSE
		
		IF DOES_ENTITY_EXIST(sEntityID.DamagerIndex)
			IF IS_ENTITY_A_PED(sEntityID.DamagerIndex)
				PED_INDEX Killerped = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.DamagerIndex)
				
				IF IS_PED_A_PLAYER(Killerped)
					bAPlayerKilledInteractable = TRUE
					bLocalPlayerKilledInteractable = (Killerped = LocalPlayerPed)
				ENDIF
			ENDIF
		ENDIF
		
		IF bAPlayerKilledInteractable
			IF bLocalPlayerKilledInteractable
				PRINTLN("[MCDamageEvent][Interactable ", iInteractable, "][Destroyed][", iInteractable, "] - Completing Interactable because I killed it!")
				BROADCAST_FMMC_INTERACTABLE_EVENT(FMMC_INTERACTABLE_EVENT_TYPE__INTERACTION_COMPLETE, iInteractable)
			ELSE
				PRINTLN("[MCDamageEvent][Interactable ", iInteractable, "][Destroyed][", iInteractable, "] - A player killed the Interactable but it wasn't me!")
			ENDIF
		ELSE
			IF bIsLocalPlayerHost
				PRINTLN("[MCDamageEvent][Interactable ", iInteractable, "][Destroyed][", iInteractable, "] - Host completing Interactable due to it being killed by another non-player entity!")
				BROADCAST_FMMC_INTERACTABLE_EVENT(FMMC_INTERACTABLE_EVENT_TYPE__INTERACTION_COMPLETE, iInteractable)
			ELSE
				PRINTLN("[MCDamageEvent][Interactable ", iInteractable, "][Destroyed][", iInteractable, "] - A non-player entity killed this Interactable! Leaving the Complete event for the Host to broadcast")
			ENDIF
		ENDIF
	ENDIF
	
	IF SHOULD_INTERACTABLE_DISAPPEAR_UPON_DEATH(iInteractable)
		IF DOES_ENTITY_EXIST(sEntityID.VictimIndex)
			SET_ENTITY_VISIBLE(sEntityID.VictimIndex, FALSE)
			SET_ENTITY_COMPLETELY_DISABLE_COLLISION(sEntityID.VictimIndex, FALSE)
			PRINTLN("[MCDamageEvent][Interactable ", iInteractable, "][Destroyed][", iInteractable, "] - Making Interactable disappear due to SHOULD_INTERACTABLE_DISAPPEAR_UPON_DEATH!")
		ENDIF
	ENDIF
	
	IF NOT IS_STRING_EMPTY(GET_PTFX_ASSET_NAME_FOR_ENTITY_CUSTOM_DEATH_EXPLOSION(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].mnInteractable_Model))
		// This interactable has a custom death explosion effect!
		CREATE_CUSTOM_DEATH_EXPLOSION_PTFX_FOR_ENTITY(sEntityID.VictimIndex)
	ENDIF
	
	// May be worth implementing later
//	IF iInteractable > -1
//	AND iInteractable < FMMC_MAX_NUM_INTERACTABLES
//		IF IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_InteractableDestroyedTracking)
//		AND g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iContinuityId > -1
//		AND NOT IS_BIT_SET(MC_serverBD_1.sMissionContinuityVars.iInteractableDestroyedTrackingBitset, g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iContinuityId)
//			SET_BIT(MC_serverBD_1.sMissionContinuityVars.iInteractableDestroyedTrackingBitset, g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iContinuityId)
//			PRINTLN("[MCDamageEvent][Interactable ", iInteractable, "][Destroyed][", iInteractable, "] - [JS][CONTINUITY] - with continuityID ", g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iContinuityId, " was destroyed")
//		ENDIF
//	ENDIF
ENDPROC

PROC PROCESS_ENTITY_DESTROYED_EVENT_PLAYER(STRUCT_ENTITY_DAMAGE_EVENT sEntityID, INT iPartVictim, PED_INDEX pedVictim, PLAYER_INDEX piVictim, PARTICIPANT_INDEX piPartVictim, INT iPartKiller, PED_INDEX pedKiller, PLAYER_INDEX piKiller, PARTICIPANT_INDEX piPartKiller)
	
	UNUSED_PARAMETER(piPartVictim)
	UNUSED_PARAMETER(piPartKiller)
	
	PRINTLN("[MCDamageEvent][Player][Destroyed][", iPartVictim, "] - has been destroyed by iPartKiller: ", iPartKiller)
	
	INT iTeam
	
	IF iPartVictim != -1
		IF MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam] < FMMC_MAX_RULES
			
			PROCESS_BEAST_DESTOYED_ANOTHER_PLAYER(pedVictim, pedKiller #IF IS_DEBUG_BUILD , iPartVictim #ENDIF)
			
			PROCESS_HEALTH_REGEN_FROM_PLAYER_DEATH(pedVictim #IF IS_DEBUG_BUILD , iPartVictim #ENDIF)
			
		ENDIF
		
		IF bIsLocalPlayerHost
		AND NOT IS_BIT_SET(MC_serverBD.iPartDeathEventBS, iPartVictim)
			PRINTLN("[MCDamageEvent][Player][Destroyed][", iPartVictim, "] - Part ",iPartVictim," has died, set iPartDeathEventBS")
			SET_BIT(MC_serverBD.iPartDeathEventBS, iPartVictim)
		ENDIF
		
		IF iPartVictim = iLocalPart
		AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartVictim].iteam].iTeamBitset2, ciBS2_EXPLODE_ON_DEATH)
			PRINTLN("[MCDamageEvent][Player][Destroyed][", iPartVictim, "] - EXPLODING")
			ADD_EXPLOSION(GET_PLAYER_COORDS(LocalPlayer), EXP_TAG_PETROL_PUMP, DEFAULT, FALSE, DEFAULT, DEFAULT, TRUE)
		ENDIF
		
		IF bIsLocalPlayerHost
			PROCESS_TEAM_TIME_DECREMENT_FROM_PLAYER_DEATH(iPartVictim)		
		ENDIF
		
		PROCESS_PLAYER_DEATH_AS_A_SUICIDE(sEntityID, iPartVictim)
		
		IF DOES_ENTITY_EXIST(pedKiller)					
			IF iPartKiller !=-1
				
				IF iPartKiller = iLocalPart
				AND iPartKiller != iPartVictim
					IF MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam] < FMMC_MAX_RULES
						IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iLocalPart].iteam].iRuleBitSetSix[MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam]], ciBS_RULE6_ENABLE_HEALTH_DRAIN)
							PRINTLN("[MCDamageEvent][Player][Destroyed][", iPartVictim, "] - [HEALTHDRAIN] - PROCESS_HEALTH_DRAIN_DAMAGE_EVENTS - Killed another player")
							fDrainHealPercentage += g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iLocalPart].iteam].sDrainStruct[ MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam] ].fPlayerHeal
						ENDIF
					ENDIF
				ENDIF
				IF iPartKiller != iPartVictim
					IF MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iPartKiller].iteam] < FMMC_MAX_RULES
						FLOAT fCaptureBoost = g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iPartKiller].iteam].fCaptureIncrease[MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iPartKiller].iteam]]
						IF fCaptureBoost > 0
							ASSERTLN("fCaptureIncrease Not implemented in the 2020 controller - Add a bug for DEFAULT ONLINE TOOLS")
							PRINTLN("fCaptureIncrease Not implemented in the 2020 controller - Add a bug for DEFAULT ONLINE TOOLS")
						ENDIF
					ENDIF
				ENDIF
				
				PROCESS_HEALTH_BOOST_ON_KILL(iPartKiller #IF IS_DEBUG_BUILD , iPartVictim #ENDIF)
				
				PROCESS_JUGGERNAUT_PTFX_ON_KILL(iPartKiller, iPartVictim)
				
				PROCESS_CUSTOM_WASTED_SHARD_KILLED_BY_DATA(iPartVictim, iPartKiller)
				
				PROCESS_SWITCH_TEAM_ON_DEATH(iPartVictim, iPartKiller)

				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciCLOBBER_PLAYER_ON_TEAM_SWAP)
					//FMMC2020 - Functionalise
					IF LocalPlayer = piKiller
						IF LocalPlayer != piVictim 
							PRINTLN("[MCDamageEvent][Player][Destroyed][", iPartVictim, "] - Team swap, not a suicide. Adding kills")
							MC_playerBD[iPartToUse].iNumPlayerKills++
							
							MC_playerBD[iPartToUse].iPureKills++
								
							IF sEntityID.IsHeadShot
								MC_playerBD[iPartToUse].iNumHeadshots++
							ELSE
								CLEAR_BIT(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_ALL_HEADSHOTS)
							ENDIF
						ENDIF
						PRINTLN("[MCDamageEvent][Player][Destroyed][", iPartVictim, "] - [ServerSwitch] ITS CLOBBERING TIME")
						
						IF (LocalPlayer != piVictim 
						OR NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iDontSwitchOnSuicide[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]], ciDYNAMIC_MENU_DONT_SWITCH_ON_SUICIDE))
							IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam] < FMMC_MAX_RULES
								INT iNewTeam = g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iSwapToTeamOnKill[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]]
								PLAYER_INDEX piTempKiller = LocalPlayer
									
								IF LocalPlayer = piVictim 
								AND LocalPlayer = piKiller
								AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iDontSwitchOnSuicide[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]], ciDYNAMIC_MENU_DONT_SWITCH_ON_SUICIDE)
									piTempKiller = GET_PLAYER_FROM_LOSING_TEAM_FOR_SWAP()
									iNewTeam = g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iSwapToTeamOnDeath[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]]
								ENDIF
								
								IF iNewTeam != -1
								AND iNewTeam != MC_playerBD[iPartToUse].iteam
								AND piVictim != piTempKiller
									IF bIsLocalPlayerHost
										PRINTLN("[MCDamageEvent][Player][Destroyed][", iPartVictim, "] - [ServerSwitch] Processing Inside Damage Event")
										PROCESS_PLAYER_TEAM_SWITCH(iPartVictim, piTempKiller, LocalPlayer = piVictim)
									ELSE
										BROADCAST_FMMC_PLAYER_TEAM_SWITCH_REQUEST(MC_ServerBD.iSessionScriptEventKey, MC_playerBD[iPartToUse].iteam, iNewTeam, iPartVictim, piTempKiller, LocalPlayer = piVictim)
										PRINTLN("[MCDamageEvent][Player][Destroyed][", iPartVictim, "] - [ServerSwitch] Broadcasting Request")
									ENDIF
									
									MC_PlayerBD[iLocalPart].bSwapTeamStarted = TRUE
									PRINTLN("[MCDamageEvent][Player][Destroyed][", iPartVictim, "] - [TeamSwaps] bSwapTeamLock = TRUE 1")
									REINIT_NET_TIMER(tdTeamSwapFailSafe)
								ELSE
									IF piVictim = piTempKiller
										PRINTLN("[MCDamageEvent][Player][Destroyed][", iPartVictim, "] - [ServerSwitch] NO VALID PLAYER TO SWAP WITH")
									ELSE
										PRINTLN("[MCDamageEvent][Player][Destroyed][", iPartVictim, "] - [ServerSwitch] SWAP IS NOT VALID", iNewTeam)
									ENDIF
								ENDIF
								
								IF LocalPlayer = piVictim 
									iPartLastDamager = -1
								ENDIF
							ENDIF
						ENDIF
					ELIF LocalPlayer = piVictim
						MC_PlayerBD[iPartToUse].bSwapTeamStarted = TRUE
						PRINTLN("[MCDamageEvent][Player][Destroyed][", iPartVictim, "] - [TeamSwaps] bSwapTeamLock = TRUE 2")
						REINIT_NET_TIMER(tdTeamSwapFailSafe)
					ENDIF
				ELIF MC_playerBD[iPartVictim].iteam != MC_playerBD[iPartKiller].iteam 
				AND NOT DOES_TEAM_LIKE_TEAM(MC_playerBD[iPartVictim].iteam, MC_playerBD[iPartKiller].iteam )
				AND MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
					//FMMC2020 - Functionalise
					IF bIsLocalPlayerHost
						INT igamestage = GET_MC_CLIENT_MISSION_STAGE(iPartKiller)
						PRINTLN("[MCDamageEvent][Player][Destroyed][", iPartVictim, "] - GET_MC_CLIENT_MISSION_STAGE: ",igamestage) 
						PRINTLN("[MCDamageEvent][Player][Destroyed][", iPartVictim, "] - host adding to team kills ") 
						IF igamestage = CLIENT_MISSION_STAGE_KILL_PLAYERS
							MC_serverBD.iTeamPlayerKills[MC_playerBD[iPartKiller].iteam][MC_playerBD[iPartVictim].iteam]++
						ELIF igamestage = CLIENT_MISSION_STAGE_KILL_TEAM0
							IF MC_playerBD[iPartVictim].iteam = 0
								MC_serverBD.iTeamPlayerKills[MC_playerBD[iPartKiller].iteam][MC_playerBD[iPartVictim].iteam]++
								PRINTLN("[MCDamageEvent][Player][Destroyed][", iPartVictim, "] - MC_serverBD.iTeamPlayerKills of team 0 increased for team: ",MC_playerBD[iPartKiller].iteam) 
							ENDIF
						ELIF igamestage = CLIENT_MISSION_STAGE_KILL_TEAM1
							IF MC_playerBD[iPartVictim].iteam = 1
								MC_serverBD.iTeamPlayerKills[MC_playerBD[iPartKiller].iteam][MC_playerBD[iPartVictim].iteam]++
								PRINTLN("[MCDamageEvent][Player][Destroyed][", iPartVictim, "] - MC_serverBD.iTeamPlayerKills of team 1 increased for team: ",MC_playerBD[iPartKiller].iteam)
							ENDIF
						ELIF igamestage = CLIENT_MISSION_STAGE_KILL_TEAM2
							IF MC_playerBD[iPartVictim].iteam = 2
								MC_serverBD.iTeamPlayerKills[MC_playerBD[iPartKiller].iteam][MC_playerBD[iPartVictim].iteam]++
								PRINTLN("[MCDamageEvent][Player][Destroyed][", iPartVictim, "] - MC_serverBD.iTeamPlayerKills of team 2 increased for team: ",MC_playerBD[iPartKiller].iteam)
							ENDIF
						ELIF igamestage = CLIENT_MISSION_STAGE_KILL_TEAM3
							IF MC_playerBD[iPartVictim].iteam = 3
								MC_serverBD.iTeamPlayerKills[MC_playerBD[iPartKiller].iteam][MC_playerBD[iPartVictim].iteam]++
								PRINTLN("[MCDamageEvent][Player][Destroyed][", iPartVictim, "] - MC_serverBD.iTeamPlayerKills of team 3 increased for team: ",MC_playerBD[iPartKiller].iteam) 
							ENDIF
						ENDIF
					ENDIF
					
					IF LocalPlayer = piKiller
					
						MC_playerBD[iLocalPart].iNumPlayerKills++

						MC_playerBD[iLocalPart].iPureKills++
						
						PRINTLN("[MCDamageEvent][Player][Destroyed][", iPartVictim, "] - iNumPlayerKills: ", MC_playerBD[iPartToUse].iNumPlayerKills, " iPureKills: ", MC_playerBD[iPartToUse].iPureKills) 
						
						IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iRuleBitsetFifteen[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iTeam]], ciBS_RULE15_RESPAWN_ALL_PLAYERS_ON_KILL)
							BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_SpawnAllTeamMembers, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE, MC_playerBD[iLocalPart].iTeam)
							PRINTLN("[SPECTATE] PROCESS_PLAYER_KILL - respawn dead players on this team due to player ", GET_PLAYER_NAME(LocalPlayer), " getting a kill.")
						ENDIF
						
						BOOL bGiveTeamOptionPoints = TRUE
						
						IF NOT ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
							IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam] < FMMC_MAX_RULES
							AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].iRuleBitsetSeven[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam]], ciBS_RULE7_SUBTRACT_POINTS_FROM_TEAM)

								IF ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
									PRINTLN("[MCDamageEvent][Player][Destroyed][", iPartVictim, "] - Sudden Death, incrementing by 1 ") 
									INCREMENT_LOCAL_PLAYER_SCORE_BY(1,FALSE)
								ENDIF
								PRINTLN("[MCDamageEvent][Player][Destroyed][", iPartVictim, "] - skipping incrementing local player score, decrementing other team ")
							ELSE
								IF MC_serverBD.iNumPlayerRuleHighestPriority[MC_playerBD[iLocalPart].iteam] > 0 
									PRINTLN("[MCDamageEvent][Player][Destroyed][", iPartVictim, "] - more than 0 higherst ") 
									IF MC_serverBD_4.iPlayerRuleMissionLogic[MC_playerBD[iLocalPart].iteam] = FMMC_OBJECTIVE_LOGIC_KILL_PLAYERS // this high priority kill player objective
										INCREMENT_LOCAL_PLAYER_SCORE_FOR_CURRENT_OBJECTIVE(DEFAULT, DEFAULT, iPartVictim)

										bGiveTeamOptionPoints = FALSE
									ELIF MC_serverBD_4.iPlayerRuleMissionLogic[MC_playerBD[iLocalPart].iteam] >= FMMC_OBJECTIVE_LOGIC_KILL_TEAM0
									AND MC_serverBD_4.iPlayerRuleMissionLogic[MC_playerBD[iLocalPart].iteam] <= FMMC_OBJECTIVE_LOGIC_KILL_TEAM3
										
										IF MC_playerBD[iPartVictim].iteam = (MC_serverBD_4.iPlayerRuleMissionLogic[MC_playerBD[iLocalPart].iteam] - FMMC_OBJECTIVE_LOGIC_KILL_TEAM0)
											INCREMENT_LOCAL_PLAYER_SCORE_FOR_CURRENT_OBJECTIVE(1, TRUE, iPartVictim)
											bGiveTeamOptionPoints = FALSE
										ELSE
											bGiveTeamOptionPoints = FALSE
										ENDIF
									ENDIF
								ENDIF
								
								IF ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
								AND (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciTRANSFORM_TO_JUGGERNAUT_ON_SUDDEN_DEATH)
								OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciTRANSFORM_TO_BEAST_ON_SUDDEN_DEATH))
									PRINTLN("[MCDamageEvent][Player][Destroyed][", iPartVictim, "] - Sudden Death (transformed), incrementing by 1 ") 
									INCREMENT_LOCAL_PLAYER_SCORE_BY(1,FALSE)
								ENDIF
								
								IF bGiveTeamOptionPoints // If we haven't already handed out points
									IF g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].iPointsPerKill > 0
										INCREMENT_LOCAL_PLAYER_SCORE_BY(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].iPointsPerKill,TRUE)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iTeam] < FMMC_MAX_RULES
							INT iNewTeam = g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iSwapToTeamOnKill[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iTeam]]
							
							IF iNewTeam != -1
							AND iNewTeam != MC_playerBD[iLocalPart].iteam
								PRINTLN("[MCDamageEvent][Player][Destroyed][", iPartVictim, "] - Killed a player, swapping to team ", iNewTeam)
								IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciTEAM_SWITCH_SHARDS)
									PRINTLN("[MCDamageEvent][Player][Destroyed][", iPartVictim, "] - Playing Winning_Team_Shard 2")
									STRING sWinText

									sWinText= "TS_WIN"

									PLAY_SOUND_FRONTEND(-1, "Winning_Team_Shard", "DLC_Exec_TP_SoundSet", FALSE)

										PLAY_SOUND_FRONTEND(-1, "Winning_Team_Shard", "DLC_Exec_TP_SoundSet", FALSE)//Played twice?
									SETUP_NEW_BIG_MESSAGE_WITH_STRING(BIG_MESSAGE_GENERIC_TEXT, "", sWinText, "TGIG_WINNER")

								ENDIF
								CHANGE_PLAYER_TEAM(iNewTeam, TRUE)
								BROADCAST_FMMC_OBJECTIVE_PLAYER_SWITCHED_TEAMS(MC_playerBD[iLocalPart].iteam, iNewTeam)
							ENDIF
							
							START_PLAYER_MODEL_SWAP(GET_LOCAL_PLAYER_TEAM(), g_FMMC_STRUCT.sFMMCEndConditions[GET_LOCAL_PLAYER_TEAM()].sRulePlayerModelSwap[GET_LOCAL_PLAYER_CURRENT_RULE()].iPlayerModelToSwapToOnKill)
							
						ENDIF
						
						IF sEntityID.IsHeadShot
							MC_playerBD[iLocalPart].iNumHeadshots++
						ELSE
							CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet,PBBOOL_ALL_HEADSHOTS)
						ENDIF						
						
					ELIF LocalPlayer = piVictim
						IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartKiller].iteam] < FMMC_MAX_RULES
						AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartKiller].iteam].iRuleBitsetSeven[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartKiller].iteam]], ciBS_RULE7_SUBTRACT_POINTS_FROM_TEAM)
							IF MC_playerBD[iPartToUse].iPlayerScore > 0 AND NOT ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
								IF g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartKiller].iteam].iObjectiveScore[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartKiller].iteam]]  > 0
									IF MC_playerBD[iPartToUse].iPlayerScore >= g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartKiller].iteam].iObjectiveScore[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartKiller].iteam]]
										PRINTLN("[MCDamageEvent][Player][Destroyed][", iPartVictim, "] - decrementing team point by ", g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartKiller].iteam].iPointsPerKill) 
										INCREMENT_LOCAL_PLAYER_SCORE_BY((g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartKiller].iteam].iObjectiveScore[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartKiller].iteam]] * -1),FALSE)
									ELSE
										PRINTLN("[MCDamageEvent][Player][Destroyed][", iPartVictim, "] - score too low decrementing team point by ", MC_playerBD[iPartToUse].iPlayerScore) 
										INCREMENT_LOCAL_PLAYER_SCORE_BY((MC_playerBD[iPartToUse].iPlayerScore * -1),FALSE)
									ENDIF
								ELSE
									PRINTLN("[MCDamageEvent][Player][Destroyed][", iPartVictim, "] - points not set, decrementing by 1 ") 
									INCREMENT_LOCAL_PLAYER_SCORE_BY(-1,FALSE)
								ENDIF
							ELSE
								IF NOT ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
									//Show help
									BROADCAST_FMMC_CANT_DECREMENT_SCORE(iPartKiller)															
								ENDIF															
							ENDIF
						ENDIF
						
					ENDIF
				ELIF MC_playerBD[iPartVictim].iteam = MC_playerBD[iPartKiller].iteam
										
					PROCESS_TEAM_SUICIDE_EVENT(iTeam, piKiller, iPartVictim, iPartKiller)
					
					IF iPartVictim = iPartKiller
						PRIVATE_PROCESS_ENTITY_SUICIDE_EVENT(piVictim, iPartVictim, iPartKiller)
					ENDIF
				ENDIF
			ELSE
				IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
					INT iNewTeam = g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iSwapToTeamOnDeath[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iTeam]]
					
					IF iNewTeam != -1
					AND iNewTeam != MC_playerBD[iLocalPart].iteam
						IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iDontSwitchOnSuicide[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iTeam]], ciDYNAMIC_MENU_DONT_SWITCH_ON_SUICIDE)
							IF NOT IS_BIT_SET(iLocalBoolCheck16, LBOOL16_SHOULD_SHOW_TEAM_SWITCH_SHARD)
								SET_BIT(iLocalBoolCheck16, LBOOL16_SHOULD_SHOW_TEAM_SWITCH_SHARD)
							ENDIF
							CHANGE_PLAYER_TEAM(iNewTeam, TRUE)
							BROADCAST_FMMC_OBJECTIVE_PLAYER_SWITCHED_TEAMS(MC_playerBD[iLocalPart].iteam, iNewTeam)
							PRINTLN("[MCDamageEvent][Player][Destroyed][", iPartVictim, "] - Died, swapping to team ", iNewTeam)
						ELSE
							PRINTLN("[MCDamageEvent][Player][Destroyed][", iPartVictim, "] - Died (no killer, not suicide), not switching team")
						ENDIF
					ENDIF
				ENDIF

				IF MC_playerBD[iPartVictim].iteam >= 0
					IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartVictim].iteam].iTeamBitset2, ciBS2_CUSTOM_WASTED_SHARD)
						IF iPartVictim = PARTICIPANT_ID_TO_INT()
							REQUEST_PLAY_CUSTOM_SHARD(PLAY_SHARD_TYPE_CUSTOM_WASTED, MC_playerBD[iPartVictim].iteam, iPartVictim, iPartKiller)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			//did we drown maybe?
			PRIVATE_PROCESS_ENTITY_SUICIDE_EVENT(piVictim, iPartVictim)
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_ENTITY_DAMAGED_EVENT_PED(STRUCT_ENTITY_DAMAGE_EVENT sEntityID)
			
	IF NOT DOES_ENTITY_EXIST(sEntityID.VictimIndex)
		EXIT
	ENDIF
	
	INT iPedVictim = IS_ENTITY_A_MISSION_CREATOR_ENTITY(sEntityID.VictimIndex)
	
	IF iPedVictim = -1
		EXIT
	ENDIF
	
	PRINTLN("[MCDamageEvent][Ped][Damaged][", iPedVictim, "] - Has been damaged.")
	
	PED_INDEX pedVictim
	IF DOES_ENTITY_EXIST(sEntityID.VictimIndex)
		IF IS_ENTITY_A_PED(sEntityID.VictimIndex)	
			pedVictim = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.VictimIndex)
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(sMissionPedsLocalVars[iPedVictim].iPedBS, ci_MissionPedBS_HasBeenHit)
		SET_BIT(sMissionPedsLocalVars[iPedVictim].iPedBS, ci_MissionPedBS_HasBeenHit)
		PRINTLN("[MCDamageEvent][Ped][Damaged][", iPedVictim, "] - Has been hit.")							
	ENDIF
	
	PROCESS_SET_PED_HAS_BEEN_HIT(sEntityID, iPedVictim, sEntityID.DamagerIndex)
	
	IF bIsLocalPlayerHost
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedVictim].iPedBitsetFive, ciPED_BSFive_AggroWhenHurtOrKilled)
		AND NOT FMMC_IS_LONG_BIT_SET(iPedLocalReceivedEvent_HurtOrKilled, iPedVictim)
			PRINTLN("[MCDamageEvent][Ped][Damaged][", iPedVictim, "] - SPOOKED as ped hurt had ciPED_BSFive_AggroWhenHurtOrKilled set")
			FMMC_SET_LONG_BIT(iPedLocalReceivedEvent_HurtOrKilled, iPedVictim)
			BROADCAST_FMMC_PED_SPOOK_HURTORKILLED(iPedVictim)
		ENDIF
		
		IF DOES_ENTITY_EXIST(sEntityID.DamagerIndex)
			IF sEntityID.WeaponUsed = ENUM_TO_INT(WEAPONTYPE_DLC_TRANQUILIZER)
				IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet8, SBBOOL8_A_PED_HAS_BEEN_TRANQUILIZED)
					SET_BIT(MC_serverBD.iServerBitSet8, SBBOOL8_A_PED_HAS_BEEN_TRANQUILIZED)
					PRINTLN("[MCDamageEvent][Ped][Damaged][", iPedVictim, "] - Setting SBBOOL8_A_PED_HAS_BEEN_TRANQUILIZED")
				ENDIF
			ENDIF
			
			IF sEntityID.WeaponUsed = ENUM_TO_INT(WEAPONTYPE_STUNGUN)
			OR sEntityID.WeaponUsed = ENUM_TO_INT(WEAPONTYPE_DLC_STUNGUNG_MP)
				IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet8, SBBOOL8_A_PED_HAS_BEEN_STUNNED)
					SET_BIT(MC_serverBD.iServerBitSet8, SBBOOL8_A_PED_HAS_BEEN_STUNNED)
					PRINTLN("[MCDamageEvent][Ped][Damaged][", iPedVictim, "] - Setting SBBOOL8_A_PED_HAS_BEEN_STUNNED")
				ENDIF
			ENDIF
			
			IF sEntityID.WeaponUsed = ENUM_TO_INT(WEAPONTYPE_DLC_EMPLAUNCHER)
				IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet8, SBBOOL8_A_PED_HAS_BEEN_EMPED)
					SET_BIT(MC_serverBD.iServerBitSet8, SBBOOL8_A_PED_HAS_BEEN_EMPED)
					PRINTLN("[MCDamageEvent][Ped][Damaged][", iPedVictim, "] - Setting SBBOOL8_A_PED_HAS_BEEN_EMPED")
				ENDIF
			ENDIF			
		ENDIF
				
		SET_WARP_ON_DAMAGE_DATA_FOR_ENTITY(CREATION_TYPE_PEDS, iPedVictim)
	ENDIF
	
	IF DOES_ENTITY_EXIST(sEntityID.DamagerIndex)
	AND NOT IS_PED_INJURED(pedVictim)
		IF sEntityID.WeaponUsed = ENUM_TO_INT(WEAPONTYPE_STUNGUN)
		AND NOT FMMC_IS_LONG_BIT_SET(iPedHitByStunGun, iPedVictim)		
			PRINTLN("[MCDamageEvent][Ped][Damaged][", iPedVictim, "] - Setting iPedHitByStunGun on: ", iPedVictim )
			FMMC_SET_LONG_BIT(iPedHitByStunGun, iPedVictim)
			IF NETWORK_HAS_CONTROL_OF_ENTITY(pedVictim)
				PRINTLN("[MCDamageEvent][Ped][Damaged][", iPedVictim, "] - Setting PCF_DisableInjuredCryForHelpEvents on: ", iPedVictim )
				SET_PED_CONFIG_FLAG(pedVictim, PCF_DisableInjuredCryForHelpEvents, TRUE)
			ELSE
				PRINTLN("[MCDamageEvent][Ped][Damaged][", iPedVictim, "] - Not Owner, preventing PCF_DisableInjuredCryForHelpEvents from being set on: ", iPedVictim )
			ENDIF			
		ENDIF
	ENDIF

	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedVictim].iPedBitsetFive, ciPED_BSFive_OneHitKill)
	AND (sEntityID.WeaponUsed = ENUM_TO_INT(WEAPONTYPE_STUNGUN)
	#IF FEATURE_COPS_N_CROOKS
	OR sEntityID.WeaponUsed = ENUM_TO_INT(WEAPONTYPE_DLC_STUNGUNCNC)
	#ENDIF
	OR sEntityID.WeaponUsed = ENUM_TO_INT(WEAPONTYPE_DLC_TRANQUILIZER))
		IF NETWORK_HAS_CONTROL_OF_ENTITY(pedVictim)
			IF NOT IS_PED_DEAD_OR_DYING(pedVictim)
				SET_ENTITY_HEALTH(pedVictim, ciPED_OneHitKill_Health)
				PRINTLN("[MCDamageEvent][Ped][Damaged][", iPedVictim, "] - Setting ped ", iPedVictim, " health to ", ciPED_OneHitKill_Health, " aka ciPED_OneHitKill_Health due to ciPED_BSFive_OneHitKill")
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedVictim].iPedBitsetTwelve, ciPed_BSTwelve_UseDamageScoreContribution)
		IF DOES_ENTITY_EXIST(sEntityID.DamagerIndex)
			IF IS_ENTITY_A_PED(sEntityID.DamagerIndex)	
				PED_INDEX Killerped = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.DamagerIndex)
				IF LocalPlayerPed = Killerped
					MC_playerBD_1[iPartToUse].iDamageToPedsForMedal += ROUND(sEntityID.Damage)
					PRINTLN("[MCDamageEvent][Ped][Damaged][", iPedVictim, "] - damaged specific ped (not dead): ", iPedVictim, " Total: ", MC_playerBD_1[iPartToUse].iDamageToPedsForMedal)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedVictim].iPedBitsetFifteen, ciPED_BSFifteen_AttemptDropPickupOnHurt)		
		ATTEMPT_PICKUP_DROP_FOR_PED(iPedVictim, pedVictim)
	ENDIF
	
	IF sEntityID.VictimDestroyed
		PROCESS_ENTITY_DESTROYED_EVENT_PED(sEntityID, iPedVictim, pedVictim)
	ENDIF
ENDPROC

PROC PROCESS_ENTITY_DESTROYED_EVENT_GANG_PED(STRUCT_ENTITY_DAMAGE_EVENT sEntityID, PED_INDEX pedVictim #IF IS_DEBUG_BUILD, INT iChasePed #ENDIF)

	PRINTLN("[MCDamageEvent][Ped][Destroyed][", iChasePed, "] - killed chase ped: ", iChasePed)
	
	IF NOT DOES_ENTITY_EXIST(sEntityID.DamagerIndex)
		EXIT
	ENDIF
		
	IF NOT IS_ENTITY_A_PED(sEntityID.DamagerIndex)
		EXIT
	ENDIF
	PED_INDEX Killerped = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.DamagerIndex)
	
	IF NOT IS_PED_A_PLAYER(Killerped)
		EXIT
	ENDIF
	
	IF LocalPlayerPed = Killerped
		IF NOT IS_MODEL_AMBIENT_COP_FOR_RP(GET_ENTITY_MODEL(pedVictim))
			IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()

				IF MC_playerBD[iLocalPart].iNumPedKills < g_sMPTunables.iMISSION_AI_KILL_AMOUNT_CAP
					PRINTLN("[MCDamageEvent][Ped][Destroyed][iChasePed: ", iChasePed, "] - GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION, 3 ")	
					GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD, pedVictim, "XPT_KAIE",XPTYPE_ACTION,XPCATEGORY_ACTION_KILLS, g_sMPTunables.iMISSION_AI_KILL_RP, 1, -1, "", TRUE)
				ELSE
					PRINTLN("[MCDamageEvent][Ped][Destroyed][iChasePed: ", iChasePed, "] - GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION, FAIL 3, iNumPedKills = ", MC_playerBD[iPartToUse].iNumPedKills, " CAP = ", g_sMPTunables.iMISSION_AI_KILL_AMOUNT_CAP)
				ENDIF

			ENDIF
		ENDIF
	
		MC_playerBD[iLocalPart].iNumPedKills++
	ENDIF
ENDPROC

PROC PROCESS_ENTITY_DAMAGED_EVENT_GANG_PED(STRUCT_ENTITY_DAMAGE_EVENT sEntityID)
	
	PED_INDEX pedVictim = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.VictimIndex)
	
	INT iChasePed = IS_ENTITY_A_CHASE_PED(pedVictim)
	
	IF iChasePed = -1
		EXIT
	ENDIF
	
	IF sEntityID.VictimDestroyed
		PROCESS_ENTITY_DESTROYED_EVENT_GANG_PED(sEntityID, pedVictim #IF IS_DEBUG_BUILD , iChasePed #ENDIF)
	ENDIF
ENDPROC

PROC PROCESS_ENTITY_DESTROYED_EVENT_AMBIENT_PED(STRUCT_ENTITY_DAMAGE_EVENT sEntityID, PED_INDEX pedVictim)
	
	IF NOT DOES_ENTITY_EXIST(sEntityID.DamagerIndex)
		EXIT
	ENDIF
		
	IF NOT IS_ENTITY_A_PED(sEntityID.DamagerIndex)
		EXIT
	ENDIF
	PED_INDEX Killerped = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.DamagerIndex)
	
	IF NOT IS_PED_A_PLAYER(Killerped)
		EXIT
	ENDIF
	
	IF LocalPlayerPed = Killerped
		IF NOT IS_MODEL_AMBIENT_COP_FOR_RP(GET_ENTITY_MODEL(pedVictim))
			MC_playerBD[iLocalPart].iCivilianKills++
			PRINTLN("[MCDamageEvent][Ped][Destroyed][Ambient] - iCivilianKills increased to ",MC_playerBD[iLocalPart].iCivilianKills)
		ELSE
			PRINTLN("[MCDamageEvent][Ped][Destroyed][Ambient] - killed ambient cop")
			MC_playerBD[iLocalPart].iAmbientCopsKilled++
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_ENTITY_DAMAGED_EVENT_AMBIENT_PED(STRUCT_ENTITY_DAMAGE_EVENT sEntityID)
	
	PED_INDEX pedVictim = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.VictimIndex)
		
	IF IS_ENTITY_A_MISSION_CREATOR_ENTITY(sEntityID.VictimIndex) > -1
	OR IS_ENTITY_A_CHASE_PED(pedVictim) > -1
		EXIT
	ENDIF
	
	IF sEntityID.VictimDestroyed
		PROCESS_ENTITY_DESTROYED_EVENT_AMBIENT_PED(sEntityID, pedVictim)
	ENDIF
ENDPROC

PROC PROCESS_ENTITY_DAMAGED_EVENT_VEH(STRUCT_ENTITY_DAMAGE_EVENT sEntityID)
	
	IF NOT DOES_ENTITY_EXIST(sEntityID.VictimIndex)
		EXIT
	ENDIF
	
	INT iVehVictim = IS_ENTITY_A_MISSION_CREATOR_ENTITY(sEntityID.VictimIndex)
	VEHICLE_INDEX VehVictim = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(sEntityID.VictimIndex)
		
	PRINTLN("[MCDamageEvent][Vehicle][Damaged][", iVehVictim, "] - has been damaged.")
		
	// Owner Check
	IF NETWORK_HAS_CONTROL_OF_ENTITY(sEntityID.VictimIndex)
		IF IS_BIT_SET(iLocalBoolCheck3, LBOOL3_EXPLOSIVE_ZONE_ACTIVE)
			IF IS_VEHICLE_EMPTY(VehVictim, TRUE)
				INT iZone
				
				FOR iZone = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfZones - 1)
					IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iType = ciFMMC_ZONE_TYPE__EXPLOSION_AREA
					AND DOES_ZONE_EXIST(iZone)
					
						PRINTLN("[MCDamageEvent][Vehicle][Damaged][", iVehVictim, "] - took damage: ",sEntityID.Damage," for vehicle: ",NATIVE_TO_INT(VehVictim), " Zone: ",iZone," zone threshold is: ",g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].fZoneValue)
						IF sEntityID.Damage >= g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].fZoneValue
							#IF IS_DEBUG_BUILD
							VECTOR vtemp = GET_ENTITY_COORDS(VehVictim)
							PRINTLN("[MCDamageEvent][Vehicle][Damaged][", iVehVictim, "] - vehicle coords: ",vtemp, " Zone: ",iZone," zone coords 0: ",g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[0]," zone coords 1: ",g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[1])
							#ENDIF
							
							IF IS_ENTITY_IN_FMMC_ZONE(VehVictim, iZone, TRUE)
								NETWORK_EXPLODE_VEHICLE(VehVictim)
								PRINTLN("[MCDamageEvent][Vehicle][Damaged][", iVehVictim, "] - ciFMMC_ZONE_TYPE__EXPLOSION_AREA  exploded  for vehicle: ",NATIVE_TO_INT(VehVictim))
							ENDIF
						ENDIF
					ENDIF
				ENDFOR
			ENDIF
		ENDIF
	ENDIF

	IF GET_PED_IN_VEHICLE_SEAT(VehVictim) = LocalPlayerPed
		IF DOES_ENTITY_EXIST(sEntityID.DamagerIndex)
			IF IS_ENTITY_A_PED(sEntityID.DamagerIndex)
				IF IS_PED_A_PLAYER(GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.DamagerIndex))
					MC_Playerbd[iPartToUse].iVehDamage = MC_Playerbd[iPartToUse].iVehDamage + ROUND(sEntityID.Damage)
					PRINTLN("[MCDamageEvent][Vehicle][Damaged][", iVehVictim, "] - adding vehicle damage: ",sEntityID.Damage," making a total of: ",MC_Playerbd[iPartToUse].iVehDamage)					
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF iVehVictim > -1 AND iVehVictim < FMMC_MAX_VEHICLES	
		
		BOOL bPlayer
		IF DOES_ENTITY_EXIST(sEntityID.DamagerIndex)
			IF IS_ENTITY_A_PED(sEntityID.DamagerIndex)
				IF IS_PED_A_PLAYER(GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.DamagerIndex))
					bPlayer = TRUE
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVehVictim].iVehBitsetSeven, ciFMMC_VEHICLE7_SPAWN_BURNING)
			IF sEntityID.WeaponUsed = ENUM_TO_INT(WEAPONTYPE_DLC_VEHICLE_BOMB_WATER)
				IF NOT IS_BIT_SET(MC_serverBD_4.sBurningVehicle.iBurningVehicleExtinguishedBitset, iVehVictim)
				AND IS_BIT_SET(MC_serverBD_4.sBurningVehicle.iBurningVehiclePTFXBitset, iVehVictim)
					PRINTLN("[MCDamageEvent][Vehicle][Damaged][", iVehVictim, "] - [BURNING_VEHICLE] PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - Vehicle: ", iVehVictim, " Extinguished")
					SET_BIT(MC_serverBD_4.sBurningVehicle.iBurningVehicleExtinguishedBitset, iVehVictim)
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVehVictim].iVehBitsetEight, ciFMMC_VEHICLE8_USE_DAMAGE_SCORE_CONTRIBUTION)
			IF DOES_ENTITY_EXIST(sEntityID.DamagerIndex)
				IF IS_ENTITY_A_PED(sEntityID.DamagerIndex)
					IF LocalPlayerPed = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.DamagerIndex)
						MC_playerBD_1[iPartToUse].iDamageToVehsForMedal += ROUND(sEntityID.Damage)
						PRINTLN("[MCDamageEvent][Vehicle][Damaged][", iVehVictim, "] - damaged specific veh (not dead): ", iVehVictim, " Total: ", MC_playerBD_1[iPartToUse].iDamageToVehsForMedal)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF bPlayer
			IF NOT IS_BIT_SET(iVehHasBeenDamagedOrHarmedByPlayerBS, iVehVictim)
				PRINTLN("[MCDamageEvent][Vehicle][Damaged][", iVehVictim, "] - Setting iVehHasBeenDamagedOrHarmedByPlayerBS for iVeh: ", iVehVictim)
				SET_BIT(iVehHasBeenDamagedOrHarmedByPlayerBS, iVehVictim)
			ENDIF
			
			IF sEntityID.WeaponUsed = ENUM_TO_INT(WEAPONTYPE_RAMMEDBYVEHICLE)
			OR sEntityID.WeaponUsed = ENUM_TO_INT(WEAPONTYPE_RUNOVERBYVEHICLE)
			OR sEntityID.IsResponsibleForCollision
				PED_INDEX pedDamager = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.DamagerIndex)
				IF DOES_ENTITY_EXIST(GET_PED_IN_VEHICLE_SEAT(VehVictim))
				AND NOT IS_PED_INJURED(pedDamager)
					RELATIONSHIP_TYPE eRelationshipBetweenPlayerAndDriver = GET_RELATIONSHIP_BETWEEN_PEDS(GET_PED_IN_VEHICLE_SEAT(VehVictim), GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.DamagerIndex))
					
					IF eRelationshipBetweenPlayerAndDriver = ACQUAINTANCE_TYPE_PED_DISLIKE
					OR eRelationshipBetweenPlayerAndDriver = ACQUAINTANCE_TYPE_PED_WANTED
					OR eRelationshipBetweenPlayerAndDriver = ACQUAINTANCE_TYPE_PED_HATE
						IF sEntityID.DamagerSpeed < cfPlayerCrashedIntoEnemyVehMinSpeed // If the player was going too slow...
						AND sEntityID.Damage < cfPlayerCrashedIntoEnemyVehMaxDamage // AND didn't somehow manage to do a bunch of damage anyway
							PRINTLN("[MCDamageEvent][Vehicle][Vehicle ", iPlayerCrashedIntoEnemyVehIndex, "][Damaged][", iVehVictim, "] - A player crashed into vehicle ", iVehVictim, " but wasn't going fast enough to be worth noting! sEntityID.DamagerSpeed: ", sEntityID.DamagerSpeed)
							
						ELIF sEntityID.Damage < cfPlayerCrashedIntoEnemyVehMinDamage // If the player didn't do much damage...
						AND sEntityID.DamagerSpeed < cfPlayerCrashedIntoEnemyVehMaxSpeed // AND were going too slow to expect a reaction
							PRINTLN("[MCDamageEvent][Vehicle][Vehicle ", iPlayerCrashedIntoEnemyVehIndex, "][Damaged][", iVehVictim, "] - A player crashed into vehicle ", iVehVictim, " but didn't deal enough damage to be worth noting! sEntityID.Damage: ", sEntityID.Damage)
							
						ELSE
							iPlayerCrashedIntoEnemyVehIndex = iVehVictim
							iPlayerCrashedIntoEnemyVehFrameCount = GET_FRAME_COUNT()
							SET_BIT(ciDialogueInstantLoopConditionBS, ciDialogueInstantLoopCondition_CrashedIntoEnemyVehicle)
							PRINTLN("[MCDamageEvent][Vehicle][Vehicle ", iPlayerCrashedIntoEnemyVehIndex, "][Damaged][", iVehVictim, "] - Setting iPlayerCrashedIntoEnemyVehIndex to ", iPlayerCrashedIntoEnemyVehIndex, " & iPlayerCrashedIntoEnemyVehFrameCount to ", iPlayerCrashedIntoEnemyVehFrameCount)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
			
		PRINTLN("[MCDamageEvent][Vehicle][Damaged][", iVehVictim, "] - Setting ciDialogueInstantLoopCondition_CrashedIntoAnEntity for iVeh: ", iVehVictim)
		SET_BIT(ciDialogueInstantLoopConditionBS, ciDialogueInstantLoopCondition_CrashedIntoAnEntity)
		
		
		IF NOT IS_BIT_SET(iVehHasBeenDamagedOrHarmedBS, iVehVictim)
			PRINTLN("[MCDamageEvent][Vehicle][Damaged][", iVehVictim, "] - Setting iVehHasBeenDamagedOrHarmedBS for iVeh: ", iVehVictim)
			SET_BIT(iVehHasBeenDamagedOrHarmedBS, iVehVictim)
		ENDIF
		
		IF bIsLocalPlayerHost
			SET_WARP_ON_DAMAGE_DATA_FOR_ENTITY(CREATION_TYPE_VEHICLES, iVehVictim)
		ENDIF
		
	ENDIF
						
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_DUNE_EXPLOSIONS)
		IF DOES_ENTITY_EXIST(VehVictim)
			IF DOES_ENTITY_EXIST(GET_PED_IN_VEHICLE_SEAT(VehVictim))
				IF NOT IS_PED_A_PLAYER(GET_PED_IN_VEHICLE_SEAT(VehVictim))
					DUNE_FLIP_MARK_FOR_EXPLOSION(sEntityID.VictimIndex, VehVictim, sEntityID.DamagerIndex)
				ENDIF
			ELSE
				DUNE_FLIP_MARK_FOR_EXPLOSION(sEntityID.VictimIndex, VehVictim, sEntityID.DamagerIndex)
			ENDIF
		ENDIF
	ENDIF
		
	IF sEntityID.VictimDestroyed
	OR (IS_ENTITY_A_VEHICLE(sEntityID.VictimIndex) AND (NOT IS_VEHICLE_DRIVEABLE(GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(sEntityID.VictimIndex))))
		PROCESS_ENTITY_DESTROYED_EVENT_VEH(sEntityID, iVehVictim, VehVictim)
	ENDIF
ENDPROC

PROC PROCESS_ENTITY_DAMAGED_EVENT_OBJ(STRUCT_ENTITY_DAMAGE_EVENT sEntityID)
	
	IF NOT DOES_ENTITY_EXIST(sEntityID.VictimIndex)
		EXIT
	ENDIF
	
	OBJECT_INDEX oiVictim = GET_OBJECT_INDEX_FROM_ENTITY_INDEX(sEntityID.VictimIndex)
	INT iObjVictim = IS_OBJ_A_MISSION_CREATOR_OBJ(oiVictim)
	
	// Is a dyno prop.
	IF iObjVictim <= -1
		EXIT
	ENDIF
	
	PRINTLN("[MCDamageEvent][Object][Damaged][", iObjVictim, "] - has been damaged.")
	
	MODEL_NAMES mnObjectModel = GET_ENTITY_MODEL(oiVictim)

	IF sEntityID.WeaponUsed = ENUM_TO_INT(WEAPONTYPE_STUNGUN)
	#IF FEATURE_COPS_N_CROOKS
	OR sEntityID.WeaponUsed = ENUM_TO_INT(WEAPONTYPE_DLC_STUNGUNCNC)	
	#ENDIF
	OR sEntityID.WeaponUsed = ENUM_TO_INT(WEAPONTYPE_DLC_STUNGUNG_MP)
	OR sEntityID.WeaponUsed = ENUM_TO_INT(WEAPONTYPE_DLC_EMPLAUNCHER)
	
		PRINTLN("[MCDamageEvent][Object][Damaged][", iObjVictim, "] - [Taser] DAMAGE EVENT - Object has been tased! iObjVictim: ", iObjVictim)						
		
		SET_ENTITY_HIT_BY_TASER(iObjVictim, oiVictim, FMMC_ELECTRONIC_ENTITY_TYPE__OBJECT, TRUE, SHOULD_DRONE_SYSTEMS_BE_ACTIVE(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjVictim].sObjDroneData))	
		
		IF bIsLocalPlayerHost
		AND SHOULD_DRONE_SYSTEMS_BE_ACTIVE(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjVictim].sObjDroneData)
			SET_FMMC_DRONE_RETASK_WITH_ENTITY_ID_AND_TYPE(iObjVictim, CREATION_TYPE_OBJECTS)
		ENDIF
		
	ELSE		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjVictim].iObjectBitSetFour, cibsOBJ4_DieInOneHit)
		AND sEntityID.Damage > 5.0
			IF NETWORK_HAS_CONTROL_OF_ENTITY(oiVictim)
				SET_ENTITY_HEALTH(oiVictim, 0, sEntityID.DamagerIndex)				
				PRINTLN("[MCDamageEvent][Object][Damaged][", iObjVictim, "] - [CCTV] DAMAGE EVENT - Setting ", iObjVictim, " health to 0 because cibsOBJ4_DieInOneHit is set and the object was hit!")				
			ELSE
				PRINTLN("[MCDamageEvent][Object][Damaged][", iObjVictim, "] - [CCTV] DAMAGE EVENT - object ", iObjVictim, " has cibsOBJ4_DieInOneHit set but we are not the owner of the object")
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_THIS_MODEL_A_SCRIPTED_FLAMMABLE_ENTITY_MODEL(mnObjectModel)
		IF IS_SCRIPTED_FLAMMABLE_ENTITY_DAMAGE_SCRIPT_CONTROLLED(mnObjectModel)
			IF IS_BIT_SET(iFlammableObjScriptedDamageStartedBS, iObjVictim)
				// We've already started the burning process on this object
			ELSE
				IF NETWORK_HAS_CONTROL_OF_ENTITY(oiVictim)
					// The faked fire (PROCESS_SCRIPTED_FLAMMABLE_ENTITY) will take it from here - let us control the damage from now on
					SET_ENTITY_PROOFS(oiVictim, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE)
					PRINTLN("[FlammableObject][Objects][Object ", iObjVictim, "][SCRIPT_OBJECT_", NETWORK_ENTITY_GET_OBJECT_ID(oiVictim), "] DAMAGE EVENT | Setting entity to be immune to real damage now that it's already been burned, so we can handle it from here in script instead!")
					
					FLOAT fStartingHealth = TO_FLOAT(GET_ENTITY_MAX_HEALTH(oiVictim)) * 0.9
					SET_ENTITY_HEALTH(oiVictim, ROUND(fStartingHealth), NULL)
					SET_BIT(iFlammableObjScriptedDamageStartedBS, iObjVictim)
					PRINTLN("[FlammableObject][Objects][Object ", iObjVictim, "][SCRIPT_OBJECT_", NETWORK_ENTITY_GET_OBJECT_ID(oiVictim), "] DAMAGE EVENT | Setting entity health to ", fStartingHealth, " to ensure it burns consistently with other burning entities!")
				ELSE
					PRINTLN("[FlammableObject][Objects][Object ", iObjVictim, "][SCRIPT_OBJECT_", NETWORK_ENTITY_GET_OBJECT_ID(oiVictim), "] DAMAGE EVENT | Remote player has control")
					
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF bIsLocalPlayerHost
		SET_WARP_ON_DAMAGE_DATA_FOR_ENTITY(CREATION_TYPE_OBJECTS, iObjVictim)
	ENDIF
	
	IF iObjVictim > -1
		IF SHOULD_DRONE_SYSTEMS_BE_ACTIVE(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjVictim].sObjDroneData)
			PROCESS_FMMC_DRONE_HEALTH_THRESHOLD_TASK_ACTIVATION(oiVictim, MC_ServerBD.iObjectHealthThreshActivatedBS, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjVictim].sObjDroneData.iHealthThresholdPercentage, iObjVictim, CREATION_TYPE_OBJECTS)		
		ENDIF
	ENDIF
	
	IF sEntityID.VictimDestroyed
	OR (IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjVictim].iObjectBitSetFour, cibsOBJ4_DieInOneHit) AND sEntityID.Damage > 5.0)
		PROCESS_ENTITY_DESTROYED_EVENT_OBJ(sEntityID, iObjVictim, oiVictim)
	ENDIF
ENDPROC

PROC PROCESS_ENTITY_DAMAGED_EVENT_DYNOPROP(STRUCT_ENTITY_DAMAGE_EVENT sEntityID)
	
	IF NOT DOES_ENTITY_EXIST(sEntityID.VictimIndex)
		EXIT
	ENDIF
	
	OBJECT_INDEX oiVictim = GET_OBJECT_INDEX_FROM_ENTITY_INDEX(sEntityID.VictimIndex)	
	INT iDynoProp = IS_OBJ_A_MISSION_CREATOR_DYNOPROP(oiVictim)
	
	IF iDynoProp <= -1
		EXIT
	ENDIF
		
	PRINTLN("[MCDamageEvent][DynoProp][Damaged][", iDynoProp, "] - has been damaged")
	
	IF sEntityID.WeaponUsed = ENUM_TO_INT(WEAPONTYPE_STUNGUN)
	#IF FEATURE_COPS_N_CROOKS
	OR sEntityID.WeaponUsed = ENUM_TO_INT(WEAPONTYPE_DLC_STUNGUNCNC)
	#ENDIF
		SET_ENTITY_HIT_BY_TASER(iDynoProp, oiVictim, FMMC_ELECTRONIC_ENTITY_TYPE__DYNOPROP)
		PRINTLN("[MCDamageEvent][DynoProp][Damaged][", iDynoProp, "] - [Taser] DAMAGE EVENT - Dynoprop has been tased! iDynoProp: ", iDynoProp)
	ELSE
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].iDynopropBitset, ciFMMC_DYNOPROP_DestroyInOneHit)
		AND sEntityID.Damage > 5.0
			IF NETWORK_HAS_CONTROL_OF_ENTITY(oiVictim)
				DESTROY_DYNOPROP_IN_ONE_HIT(oiVictim, sEntityID.DamagerIndex)
				PRINTLN("[MCDamageEvent][DynoProp][Damaged][", iDynoProp, "] - [CCTV] DAMAGE EVENT - Setting ", iDynoProp, " health to 0 because ciFMMC_DYNOPROP_DestroyInOneHit is set and the dynoprop was hit!")
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_OBJECT_BROKEN_AND_VISIBLE(oiVictim)
		IF IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_DynoPropDestroyedTracking)
		AND g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].iContinuityId > -1
		AND NOT IS_BIT_SET(MC_serverBD_1.sMissionContinuityVars.iDynoPropDestroyedTrackingBitset, g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].iContinuityId)
			SET_BIT(MC_serverBD_1.sMissionContinuityVars.iDynoPropDestroyedTrackingBitset, g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].iContinuityId)
			PRINTLN("[MCDamageEvent][DynoProp][Damaged][", iDynoProp, "] - [JS][CONTINUITY] - PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - dynoprop ", iDynoProp, " with continuityID ", g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].iContinuityId, " was destroyed")
		ENDIF
	ENDIF
	
	IF sEntityID.VictimDestroyed
		PROCESS_ENTITY_DESTROYED_EVENT_DYNOPROP(sEntityID, iDynoProp, oiVictim)
	ENDIF
ENDPROC

PROC PROCESS_ENTITY_DAMAGED_EVENT_INTERACTABLE(STRUCT_ENTITY_DAMAGE_EVENT sEntityID)
	
	IF NOT DOES_ENTITY_EXIST(sEntityID.VictimIndex)
		EXIT
	ENDIF
	
	OBJECT_INDEX oiVictim = GET_OBJECT_INDEX_FROM_ENTITY_INDEX(sEntityID.VictimIndex)	
	INT iInteractable = IS_OBJ_A_MISSION_CREATOR_INTERACTABLE(oiVictim)
	
	IF iInteractable <= -1
		EXIT
	ENDIF
		
	PRINTLN("[MCDamageEvent][Interactable ", iInteractable, "][Damaged][", iInteractable, "] - has been damaged")
		
	IF bIsLocalPlayerHost
		SET_WARP_ON_DAMAGE_DATA_FOR_ENTITY(CREATION_TYPE_INTERACTABLE, iInteractable)
	ENDIF
	
	// May be worth implementing later
//	IF IS_OBJECT_BROKEN_AND_VISIBLE(oiVictim)
//		IF IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_InteractableDestroyedTracking)
//		AND g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iContinuityId > -1
//		AND NOT IS_BIT_SET(MC_serverBD_1.sMissionContinuityVars.iInteractableDestroyedTrackingBitset, g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iContinuityId)
//			SET_BIT(MC_serverBD_1.sMissionContinuityVars.iInteractableDestroyedTrackingBitset, g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iContinuityId)
//			PRINTLN("[MCDamageEvent][Interactable][Damaged][", iInteractable, "] - [JS][CONTINUITY] - PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT - dynoprop ", iInteractable, " with continuityID ", g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iContinuityId, " was destroyed")
//		ENDIF
//	ENDIF
	
	IF sEntityID.VictimDestroyed
		PROCESS_ENTITY_DESTROYED_EVENT_INTERACTABLE(sEntityID, iInteractable, oiVictim)
	ENDIF
ENDPROC

PROC PROCESS_ENTITY_DAMAGED_EVENT_PLAYER(STRUCT_ENTITY_DAMAGE_EVENT sEntityID)
			
	INT iPartVictim = -1
	PED_INDEX pedVictim = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.VictimIndex)
	PLAYER_INDEX piVictim = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedVictim)				
	PARTICIPANT_INDEX piPartVictim
	IF NETWORK_IS_PLAYER_A_PARTICIPANT(piVictim)
		piPartVictim = NETWORK_GET_PARTICIPANT_INDEX(piVictim)
		iPartVictim = NATIVE_TO_INT(piPartVictim)
	ENDIF
		
	PED_INDEX pedKiller
	IF DOES_ENTITY_EXIST(sEntityID.DamagerIndex)
		IF IS_ENTITY_A_PED(sEntityID.DamagerIndex)
			pedKiller = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.DamagerIndex)
		ENDIF
	ENDIF
	INT iPartKiller = -1	
	PLAYER_INDEX piKiller
	PARTICIPANT_INDEX piPartKiller
	IF DOES_ENTITY_EXIST(pedKiller)
		IF IS_PED_A_PLAYER(pedKiller)
			piKiller = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedKiller)
			IF NETWORK_IS_PLAYER_A_PARTICIPANT(piKiller)
				piPartKiller = NETWORK_GET_PARTICIPANT_INDEX(piKiller)
				IF NETWORK_IS_PARTICIPANT_ACTIVE(piPartKiller)
					iPartKiller = NATIVE_TO_INT(piPartKiller)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	PRINTLN("[MCDamageEvent][Player][Damaged][", iPartVictim, "] - has been damaged by iPartKiller: ", iPartKiller)
	
	IF iPartVictim = -1
		EXIT
	ENDIF
		
	IF (LocalPlayerPed = pedVictim)
		IF MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam] < FMMC_MAX_RULES
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iLocalPart].iteam].iRuleBitSetFive[MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam]], ciBS_RULE5_ENABLE_BEAST_MODE)
			OR IS_PARTICIPANT_A_BEAST(iLocalPart)
				IF pedKiller != LocalPlayerPed
				AND DOES_ENTITY_EXIST(pedKiller)
					PRINTLN("[MCDamageEvent][Player][Damaged][", iPartVictim, "] - [BEASTMODE] - Beast was damaged while stealthed")
					bBeastBeenDamaged = TRUE
					IF sEntityID.VictimDestroyed
						//Play death sound
						STRING sSoundSet = GET_BEAST_MODE_SOUNDSET()
						IF IS_ENTITY_ALIVE(localPlayerPed)
							PLAY_SOUND_FROM_ENTITY(-1, "Beast_Die", localPlayerPed, sSoundSet, TRUE, 60)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		MC_Playerbd[iLocalPart].iPlayerDamage = MC_Playerbd[iLocalPart].iPlayerDamage + ROUND(sEntityID.Damage)
		PRINTLN("[MCDamageEvent][Player][Damaged][", iPartVictim, "] - adding player damage: ",sEntityID.Damage," making a total of: ",MC_Playerbd[iLocalPart].iPlayerDamage) 
		
		IF iPartKiller != iLocalPart
		AND iPartKiller != -1
		AND iLocalPart != -1
			IF MC_playerBD[iPartKiller].iTeam != MC_playerBD[iLocalPart].iTeam
				PRINTLN("[[MCDamageEvent][Player][Damaged][", iPartVictim, "] - iPartLastDamager = ", iPartLastDamager)
				iPartLastDamager = iPartKiller
				
				//Store the time since the local player got shot
				IF NOT sEntityID.IsWithMeleeWeapon
					iLastTimePlayerGotShotInSec = GET_CLOUD_TIME_AS_INT()
					PRINTLN("[[MCDamageEvent][Player][Damaged][", iPartVictim, "] - iLastTimePlayerGotShotInSec = ", iLastTimePlayerGotShotInSec)
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF LocalPlayerPed = pedKiller
		AND LocalPlayerPed != pedVictim
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartVictim].iTeam].iTeamBitset2, ciBS2_JUGGERNAUTS)
				MC_Playerbd[iLocalPart].iDamageToJuggernaut += ROUND(sEntityID.Damage)
				PRINTLN("[MCDamageEvent][Player][Damaged][", iPartVictim, "] - adding juggernaut damage: ",ROUND(sEntityID.Damage)," making a total of: ",MC_Playerbd[iLocalPart].iDamageToJuggernaut) 
			ENDIF
			IF MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam] < FMMC_MAX_RULES
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iLocalPart].iteam].iRuleBitSetSix[MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam]], ciBS_RULE6_ENABLE_HEALTH_DRAIN)
					IF NOT IS_DAMAGE_FROM_SCRIPT(sEntityID.WeaponUsed)
						PRINTLN("[MCDamageEvent][Player][Damaged][", iPartVictim, "] - Healing the attacker: ", (sEntityID.Damage * g_FMMC_STRUCT.sFMMCEndConditions[ MC_PlayerBD[iLocalPart].iteam ].sDrainStruct[ MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam] ].fHealthDmgRestore))
						SET_ENTITY_HEALTH(pedKiller, ROUND((sEntityID.Damage * g_FMMC_STRUCT.sFMMCEndConditions[ MC_PlayerBD[iLocalPart].iteam ].sDrainStruct[ MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam] ].fHealthDmgRestore)))													
						RESET_NET_TIMER(tdDamageDrainRateTimer)
						START_NET_TIMER(tdDamageDrainRateTimer)												
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF LocalPlayerPed != pedVictim
	AND DOES_ENTITY_EXIST(sEntityID.DamagerIndex)
	AND IS_ENTITY_A_PED(sEntityID.DamagerIndex)
	AND GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.DamagerIndex) = LocalPlayerPed
		IF HAS_NET_TIMER_STARTED(td_MissedShotTakeDamageTimer)
			PRINTLN("[MCDamageEvent][iMissedShotPerc] Hit someone, reset timer (1)")
			RESET_NET_TIMER(td_MissedShotTakeDamageTimer)
		ENDIF
		
		IF g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].fCachedVehicleHealingPercentage[GET_TEAM_CURRENT_RULE(MC_playerBD[iLocalPart].iTeam)] > 0
		AND IS_BIT_SET(iLocalBoolCheck27, LBOOL27_CAN_PROCESS_CACHED_VEH_HEALING_EVENTS)
		AND NOT IS_ENTITY_DEAD(sEntityID.DamagerIndex)
			FLOAT fHealingPercentage = g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].fCachedVehicleHealingPercentage[GET_TEAM_CURRENT_RULE(MC_playerBD[iLocalPart].iTeam)]
			INT iHealthIncrease = CEIL((sEntityID.Damage * fHealingPercentage) / 100)
			PRINTLN("[MCDamageEvent][iMissedShotPerc] Gain back ", fHealingPercentage, "% of health due to damage caused to another entity! Damage caused = ", sEntityID.Damage, ". Increasing health by ", iHealthIncrease)
			PRINTLN("[MCDamageEvent][iMissedShotPerc] Player health before the increase = ", GET_ENTITY_HEALTH(sEntityID.DamagerIndex))
			SET_ENTITY_HEALTH(sEntityID.DamagerIndex, GET_ENTITY_HEALTH(sEntityID.DamagerIndex) + iHealthIncrease)
			PRINTLN("[MCDamageEvent][iMissedShotPerc] Player health after the increase = ", GET_ENTITY_HEALTH(sEntityID.DamagerIndex))
		ENDIF
	ENDIF
	
	IF sEntityID.VictimDestroyed
		PROCESS_ENTITY_DESTROYED_EVENT_PLAYER(sEntityID, iPartVictim, pedVictim, piVictim, piPartVictim, iPartKiller, pedKiller, piKiller, piPartKiller)
	ENDIF
ENDPROC

PROC PROCESS_ENTITY_DAMAGED_EVENT(STRUCT_ENTITY_DAMAGE_EVENT &sEntityID)
	// ---------- What kind of Entity Damage Event needs to be processed ---------- 
	IF IS_ENTITY_A_PED(sEntityID.VictimIndex)	
		PED_INDEX pedVictim = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.VictimIndex)
		
		IF IS_PED_A_PLAYER(pedVictim)
			PROCESS_ENTITY_DAMAGED_EVENT_PLAYER(sEntityID)
		ELSE				
			PROCESS_ENTITY_DAMAGED_EVENT_PED(sEntityID)
			PROCESS_ENTITY_DAMAGED_EVENT_GANG_PED(sEntityID)
			PROCESS_ENTITY_DAMAGED_EVENT_AMBIENT_PED(sEntityID)
		ENDIF
		
	ELIF IS_ENTITY_A_VEHICLE(sEntityID.VictimIndex)					
		PROCESS_ENTITY_DAMAGED_EVENT_VEH(sEntityID)
		
	ELIF IS_ENTITY_AN_OBJECT(sEntityID.VictimIndex)
		PROCESS_ENTITY_DAMAGED_EVENT_OBJ(sEntityID)
		PROCESS_ENTITY_DAMAGED_EVENT_DYNOPROP(sEntityID)
		PROCESS_ENTITY_DAMAGED_EVENT_INTERACTABLE(sEntityID)
		
	ENDIF
	// ---------- End Main Damage Event Functions ---------- 		
ENDPROC

PROC PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT_FAKE(INT iCount)
	
	SCRIPT_EVENT_DATA_FMMC_ENTITY_DAMAGED_EVENT_FAKE sEventData
	
	GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, sEventData, SIZE_OF(sEventData))
	
	IF sEventData.iPartToExclude = -1
		PRINTLN("[FAKE][MCDamageEvent] Damage Event Exitting, iPartToExclude is -1")
		EXIT
	ENDIF
	
	IF iLocalPart = sEventData.iPartToExclude
		PRINTLN("[FAKE][MCDamageEvent] Damage Event Exitting, iPartToExclude is me; ", sEventData.iPartToExclude)
		EXIT
	ENDIF
	
	// (VICTIM) Process the NET data into local data, this will treat players as a higher priority than entities. If unexpected results occur review the parameters being passed into BROADCAST_FMMC_EVENT_ENTITY_DAMAGED_EVENT_FAKE
	IF sEventData.piPlayerVictim != INVALID_PLAYER_INDEX()
		sEventData.sEntityID.VictimIndex = GET_PLAYER_PED(sEventData.piPlayerVictim)
	ELIF NETWORK_DOES_NETWORK_ID_EXIST(sEventData.VictimIndex)
		sEventData.sEntityID.VictimIndex = NET_TO_ENT(sEventData.VictimIndex)
	ELSE
		PRINTLN("[FAKE][MCDamageEvent] Damage Event has no VICTIM")
	ENDIF
	
	// (DAMAGER) Process the NET data into local data, this will treat players as a higher priority than entities. If unexpected results occur review the parameters being passed into BROADCAST_FMMC_EVENT_ENTITY_DAMAGED_EVENT_FAKE
	IF sEventData.piPlayerDamager != INVALID_PLAYER_INDEX()
		sEventData.sEntityID.DamagerIndex = GET_PLAYER_PED(sEventData.piPlayerDamager)
	ELIF NETWORK_DOES_NETWORK_ID_EXIST(sEventData.DamagerIndex)
		sEventData.sEntityID.DamagerIndex = NET_TO_ENT(sEventData.DamagerIndex)
	ELSE
		PRINTLN("[FAKE][MCDamageEvent] Damage Event has no DAMAGER")
	ENDIF
	
	// Process event.
	IF DOES_ENTITY_EXIST(sEventData.sEntityID.VictimIndex)		
		PRINTLN("[FAKE][MCDamageEvent] Damage Event Triggered iCount: ", iCount, " ------------------------------ START ---")
		PROCESS_ENTITY_DAMAGED_EVENT(sEventData.sEntityID)
	ENDIF
	
ENDPROC

//FMMC2020 - Most of this can be functionalised
PROC PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT(INT iCount)

	STRUCT_ENTITY_DAMAGE_EVENT sEntityID

	// Grab the event data.
	GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, sEntityID, SIZE_OF(sEntityID))
		
	// Process event.
	IF DOES_ENTITY_EXIST(sEntityID.VictimIndex)		
		PRINTLN("[MCDamageEvent] Damage Event Triggered iCount: ", iCount, " ------------------------------ START ---")	
		PROCESS_ENTITY_DAMAGED_EVENT(sEntityID)
	ENDIF
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Pickups	 -------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Process functions for pickup events		  		----------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_COLLECTED_PICKUP_EVENT(INT iCount)
	
	STRUCT_PICKUP_EVENT PickupData
	TEXT_LABEL_15 labelTemp
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, PickupData, SIZE_OF(PickupData))
	
		INT iPickup = -1
		
		IF PickupData.PlayerIndex = LocalPlayer // Only process this stuff for the local player because we can't rely on distant remote players getting this event.
			INT i
			FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons - 1
				
				IF i >= FMMC_MAX_WEAPONS
					BREAKLOOP
				ENDIF
				
				IF PickupData.PickupIndex != pipickup[i]
					RELOOP
				ENDIF
				
				iPickup = i
				PRINTLN("[Pickups][Pickup ", iPickup, "] PROCESS_COLLECTED_PICKUP_EVENT - I've collected Pickup ", iPickup, "!")
				BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_FMMCPickupCollected, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, FALSE, iPickup)

				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iPlacedBitset, ciFMMC_WEP_DestroyLocallyWhenCollected)				
					PRINTLN("[Pickups][Pickup ",i,"] PROCESS_COLLECTED_PICKUP_EVENT - Setting iPickupCollectedCleanupLocally because I picked it up")
					FMMC_SET_LONG_BIT(iPickupCollectedCleanupLocally, i)
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iPlacedBitset, ciFMMC_WEP_DestroyGloballyWhenCollected)
					PRINTLN("[Pickups][Pickup ",i,"] PROCESS_COLLECTED_PICKUP_EVENT - Setting iPickupCollectedCleanupLocally due to ciFMMC_WEP_DestroyGloballyWhenCollected and broadcasting g_ciInstancedcontentEventType_CleanupPickupForAllPlayers")
					FMMC_SET_LONG_BIT(iPickupCollectedCleanupLocally, i)
					BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_CleanupPickupForAllPlayers, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, FALSE, i)
				ENDIF
				
				IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iHelpTextOnPickup != -1
					PRINTLN("[Pickups][Pickup ",i,"] PROCESS_COLLECTED_PICKUP_EVENT - Printing help, string list index: ", g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iHelpTextOnPickup)
					TEXT_LABEL_63 tl63HelpText = GET_CUSTOM_STRING_LIST_TEXT_LABEL(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iHelpTextOnPickup)
					PRINT_HELP(tl63HelpText)
				ENDIF
				
				BREAKLOOP
			ENDFOR
			
			IF iPickup = -1
				PRINTLN("[Pickups][Pickup X] PROCESS_COLLECTED_PICKUP_EVENT - A pickup has been collected which doesn't match up with any placed pickups!")
			ENDIF
		ENDIF

		IF IS_NET_PLAYER_OK(PickupData.PlayerIndex)
    		IF PickupData.PlayerIndex = LocalPlayer
        		IF NETWORK_IS_PLAYER_A_PARTICIPANT(PickupData.PlayerIndex)
                	IF IS_PICKUP_A_WEAPON(PickupData.PickupType)
					OR IS_PICKUP_PARACHUTE(PickupData.PickupType)
						labelTemp = GET_CORRECT_TICKER_STRING(PickupData.PickupType)
	              		IF NOT IS_STRING_NULL_OR_EMPTY(labelTemp)
							PRINT_TICKER(labelTemp) 
						ENDIF 
						
						IF GET_CURRENT_GAMEMODE() = GAMEMODE_CREATOR
							INT i
							FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons - 1
								IF PickupData.PickupIndex = g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].id
									IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iPlacedBitset, ciFORCE_WEAPON_INTO_HAND)
										SET_CURRENT_PED_WEAPON(LocalPlayerPed, GET_WEAPON_TYPE_FROM_PICKUP_TYPE(PickupData.PickupType), TRUE)
									ENDIF
								ENDIF
							ENDFOR
						ENDIF
						
						IF IS_PICKUP_PARACHUTE(PickupData.PickupType)
						AND iPickup != -1
						AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickup].iPlacedBitset, ciFMMC_WEP_UseTeamParachuteColour)
							SET_PLAYER_PARACHUTE_CANOPY(LocalPlayer, g_FMMC_STRUCT.iTeamParachuteTint[GET_LOCAL_PLAYER_TEAM()])
						ENDIF
						
					ELIF PickupData.PickupType = PICKUP_CUSTOM_SCRIPT
						IF iPickup != -1
							INT iModelHash = ENUM_TO_INT(PickupData.PickupCustomModel)
							
							IF iModelHash = HASH("HEI_PROP_HEIST_BINBAG")
							OR iModelHash = HASH("PROP_CS_DUFFEL_01")
								IF IS_BIT_SET(g_FMMC_STRUCT.sTeamData[GET_LOCAL_PLAYER_TEAM()].iTeamBitSet, ciTEAM_BS_GIVE_MID_MISSION_INVENTORY_PICKUP_TEAM)
									IF PickupData.PlayerIndex = LocalPlayer
										GIVE_LOCAL_PLAYER_WEAPON_INVENTORY(FMMC_INVENTORY_MID_MISSION, WEAPONINHAND_DONTCARE)
										PRINT_TICKER("CAS_BAG_PKUPU")
										IF IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_WeaponPickupTracking)
											SET_BIT(sMissionLocalContinuityVars.iGeneralLocalTrackingBitset, ciContinuityGenericTrackingBS_MidMissionInventoryCollected)
											PRINTLN("[CONTINUITY] PROCESS_COLLECTED_PICKUP_EVENT - PICKUP_CUSTOM_SCRIPT - Setting ciContinuityGenericTrackingBS_MidMissionInventoryCollected")
										ENDIF
										#IF FEATURE_HEIST_ISLAND
										SET_ISLAND_HEIST_TELEMETRY_SUPPORT_CREW_USED(ciContinuityTelemetry_SupportCrewBS_WeaponStashUsed)
										#ENDIF
									ELSE
										PRINTLN("PROCESS_COLLECTED_PICKUP_EVENT - PICKUP_CUSTOM_SCRIPT - PickupData.PlayerIndex != LocalPlayer")
										PRINT_TICKER_WITH_PLAYER_NAME("CAS_BAG_PKUP", PickupData.PlayerIndex)
									ENDIF
								ELSE
									PRINTLN("PROCESS_COLLECTED_PICKUP_EVENT - PICKUP_CUSTOM_SCRIPT - Option to receive mid mission inventory isn't on for this team")
								ENDIF
							ENDIF
											
							IF iModelHash = HASH("h4_Prop_h4_Keys_Jail_01a")
								PRINTLN("PROCESS_COLLECTED_PICKUP_EVENT - PICKUP_CUSTOM_SCRIPT - Collected Key Ring - Playing Sound Effect FRONTEND")
								PLAY_SOUND_FRONTEND(-1, "Pickup_Keyring", "dlc_h4_heist_finale_sounds_soundset")
							ENDIF				
							
							IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickup].iCustomPickupType = ciCUSTOM_PICKUP_TYPE__MISSION_EQUIPMENT
								BROADCAST_FMMC_MISSION_EQUIPMENT(PickupData.PlayerIndex, iPickup)
								
							ELIF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickup].iCustomPickupType = ciCUSTOM_PICKUP_TYPE__EXTRA_TAKE
								// Give the player some money
								INT iGrabbedCashValue = GET_MONEY_VALUE_OF_CASH_PICKUP_MODEL(PickupData.PickupCustomModel)
								BROADCAST_FMMC_ADD_GRABBED_CASH(iGrabbedCashValue, ciGRABBED_CASH_SOURCE__EXTRA_TAKE_CASH_PICKUPS)
								
								#IF FEATURE_HEIST_ISLAND
								COMPLETE_ISLAND_HEIST_ITEM_REWARD(STEAL_FROM_SECONDARY_TARGET)
								#ENDIF
							ELIF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickup].iCustomPickupType = ciCUSTOM_PICKUP_TYPE__FREEMODE_COLLECTABLE
								IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickup].iCustomPickupModel = ciCUSTOM_FMMC_FREEMODECOLLECTABLE_MODEL__PIRATE_RADIO
									#IF FEATURE_TUNER
									SET_COLLECTABLE_COLLECTED(COLLECTABLE_USB_PIRATE_RADIO, ENUM_TO_INT(USB_COLLECTABLE_LOCATIONS_ISLAND_COMPOUND_FINALE), TRUE, FALSE)
									//GET_VALID_USB_MUSIC_MIX_TO_BE_UNLOCKED()
									
									#ENDIF
								ENDIF
							ENDIF
						ELSE
							IF g_FMMC_STRUCT.sPedAmmoDrop.iAmmoPickupType != FMMC_PED_AMMO_DROP_TYPE_INVALID
								INT i
								FOR i = 0 TO ciFMMC_MAX_PED_AMMO_DROPS-1
									IF PickupData.PickupIndex = sLocalPedAmmoDrop[i].piAmmoPickup
										BROADCAST_FMMC_REMOVE_PED_AMMO_PICKUP(i)
										
										ADD_PED_AMMO_BY_TYPE(LocalPlayerPed, GET_PED_DROP_AMMO_TYPE(g_FMMC_STRUCT.sPedAmmoDrop.iAmmoPickupType), g_FMMC_STRUCT.sPedAmmoDrop.iAmount)
										SET_AMMO_REWARD_FOR_ENTITY(LocalPlayerPed, g_FMMC_STRUCT.sPedAmmoDrop.iAmount)
										BREAKLOOP
									ENDIF
								ENDFOR
							ENDIF
						ENDIF
						
					ELIF IS_PICKUP_AMMO(PickupData.PickupType)
						
						IF iPickup != -1
							IF SHOULD_PICKUP_BE_SPAWNED_AS_AMMO(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickup].iPlacedBitset)
								IF GET_AMMO_TYPE_FROM_WEAPON_TYPE(GET_WEAPON_TYPE_FROM_PICKUP_TYPE(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickup].pt)) = PICKUP_AMMO_PISTOL
									AMMO_TYPE eAmmoType = INT_TO_ENUM(AMMO_TYPE, -1)
									IF HAS_PED_GOT_WEAPON(LocalPlayerPed, WEAPONTYPE_DLC_PISTOL_MK2)
										IF GET_WEAPON_AMMO_TYPE_FROM_WEAPON_TYPE(WEAPONTYPE_DLC_PISTOL_MK2, eAmmoType)
											ADD_PED_AMMO_BY_TYPE(LocalPlayerPed, eAmmoType, GET_WEAPON_CLIP_SIZE(WEAPONTYPE_DLC_PISTOL_MK2) * g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickup].iClips)
											PRINTLN("PROCESS_COLLECTED_PICKUP_EVENT - Giving ", GET_WEAPON_CLIP_SIZE(WEAPONTYPE_DLC_SNSPISTOL_MK2) * g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickup].iClips, " MkII Pistol Ammo (", eAmmoType, ") for pistol ammo, pickup: ", iPickup)
										ENDIF
									ELIF HAS_PED_GOT_WEAPON(LocalPlayerPed, WEAPONTYPE_DLC_SNSPISTOL_MK2)
										IF GET_WEAPON_AMMO_TYPE_FROM_WEAPON_TYPE(WEAPONTYPE_DLC_SNSPISTOL_MK2, eAmmoType)
											ADD_PED_AMMO_BY_TYPE(LocalPlayerPed, eAmmoType, GET_WEAPON_CLIP_SIZE(WEAPONTYPE_DLC_SNSPISTOL_MK2) * g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickup].iClips)
											PRINTLN("PROCESS_COLLECTED_PICKUP_EVENT - Giving ", GET_WEAPON_CLIP_SIZE(WEAPONTYPE_DLC_SNSPISTOL_MK2) * g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickup].iClips, " MkII SNS Pistol Ammo (", eAmmoType, ") for pistol ammo, pickup: ", iPickup)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
					ENDIF
         		ENDIF
      		ENDIF
			
			IF iPickup != -1
			AND g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickup].iPickupPrerequisiteToCompleteOnPickup != ciPREREQ_None
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickup].iPlacedBitset, ciFMMC_WEP_CompletePreReqLocalPlayerOnly)
					IF PickupData.PlayerIndex = LocalPlayer
						SET_PREREQUISITE_COMPLETED(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickup].iPickupPrerequisiteToCompleteOnPickup)
						PRINTLN("[MissionEquipment] PROCESS_COLLECTED_PICKUP_EVENT | Setting PreReq ", g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickup].iPickupPrerequisiteToCompleteOnPickup, " as complete due to us being the one to pick up pickup ", iPickup)
					ELSE
						PRINTLN("[MissionEquipment] PROCESS_COLLECTED_PICKUP_EVENT | Pickup ", iPickup, " is set to only complete prereq ", g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickup].iPickupPrerequisiteToCompleteOnPickup, " for the player who picks it up!")
					ENDIF
				ELSE
					SET_PREREQUISITE_COMPLETED(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickup].iPickupPrerequisiteToCompleteOnPickup)
					PRINTLN("[MissionEquipment] PROCESS_COLLECTED_PICKUP_EVENT | Setting PreReq ", g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickup].iPickupPrerequisiteToCompleteOnPickup, " as complete due to someone picking up pickup ", iPickup)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_PICKED_UP_LIVES_PICKUP(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_PICKED_UP_LIVES_PICKUP EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_PICKED_UP_LIVES_PICKUP
			INT iTeam = EventData.iTeam
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciPLAYER_LIVES_PICKUP_TYPE)
				//Team Lives
				IF bIsLocalPlayerHost
					INT i
					FOR i = 0 TO FMMC_MAX_TEAMS - 1
						MC_serverBD_3.iAdditionalTeamLives[i] = g_FMMC_STRUCT.iLivesToTeam[iTeam][i]
					ENDFOR
				ENDIF
			ELSE
				//PlayerLives
				IF IS_MISSION_COOP_TEAM_LIVES()
					IF bIsLocalPlayerHost
						MC_serverBD_3.iAdditionalTeamLives[0] += g_FMMC_STRUCT.iLivesToTeam[0][0]
					ENDIF
				ELSE
					IF EventData.Details.FromPlayerIndex = LocalPlayer
						MC_playerBD[iLocalPart].iAdditionalPlayerLives += g_FMMC_STRUCT.iLivesToTeam[0][0]
					ENDIF
				ENDIF	
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_MISSION_EQUIPMENT(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_MISSION_EQUIPMENT EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type != SCRIPT_EVENT_FMMC_MISSION_EQUIPMENT
			EXIT
		ENDIF
		
		INT iMissionEquipmentType = g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[EventData.iPickupIndex].iCustomPickupModel
		
		PRINTLN("[MissionEquipment] PROCESS_SCRIPT_EVENT_FMMC_MISSION_EQUIPMENT - Mission Equipment picked up. Equipment type: ", iMissionEquipmentType)
		iMissionEquipmentPickupsCollected[iMissionEquipmentType]++
		
		SWITCH iMissionEquipmentType
			CASE ciCUSTOM_MISSIONEQUIPMENT_MODEL__BOLTCUTTERS
				IF EventData.piEquipmentCollector = LocalPlayer
					PRINT_TICKER("FMMC_ME_BC_L")
				ELSE
					PRINT_TICKER_WITH_PLAYER_NAME("FMMC_ME_BC_R", EventData.piEquipmentCollector)
				ENDIF
			BREAK
			
			CASE ciCUSTOM_MISSIONEQUIPMENT_MODEL__GRAPPLEHOOK
				IF EventData.piEquipmentCollector = LocalPlayer
					PRINT_TICKER("FMMC_ME_GH_L")
				ELSE
					PRINT_TICKER_WITH_PLAYER_NAME("FMMC_ME_GH_R", EventData.piEquipmentCollector)
				ENDIF
			BREAK
			
			CASE ciCUSTOM_MISSIONEQUIPMENT_MODEL__DUFFEL
				IF EventData.piEquipmentCollector = LocalPlayer
					PRINT_TICKER("FMMC_ME_DF_L")
				ELSE
					PRINT_TICKER_WITH_PLAYER_NAME("FMMC_ME_DF_R", EventData.piEquipmentCollector)
				ENDIF
			BREAK
			
			CASE ciCUSTOM_MISSIONEQUIPMENT_MODEL__ENVELOPE
				IF EventData.piEquipmentCollector = LocalPlayer
					PRINT_TICKER("FMMC_ME_EN_L")
				ELSE
					PRINT_TICKER_WITH_PLAYER_NAME("FMMC_ME_EN_R", EventData.piEquipmentCollector)
				ENDIF
			BREAK
			
			CASE ciCUSTOM_MISSIONEQUIPMENT_MODEL__KEYCARD_A
			CASE ciCUSTOM_MISSIONEQUIPMENT_MODEL__KEYCARD_B
				IF EventData.piEquipmentCollector = LocalPlayer
					PRINT_TICKER("FMMC_ME_KC_L")
				ELSE
					PRINT_TICKER_WITH_PLAYER_NAME("FMMC_ME_KC_R", EventData.piEquipmentCollector)
				ENDIF
			BREAK
			CASE ciCUSTOM_MISSIONEQUIPMENT_MODEL__KEY_RING
				IF EventData.piEquipmentCollector = LocalPlayer
					PRINT_TICKER("FMMC_ME_KR_L")
				ELSE
					PRINT_TICKER_WITH_PLAYER_NAME("FMMC_ME_KR_R", EventData.piEquipmentCollector)
				ENDIF
			BREAK								
			CASE ciCUSTOM_MISSIONEQUIPMENT_MODEL__PAPER_CODES
				IF EventData.piEquipmentCollector = LocalPlayer
					PRINT_TICKER("FMMC_ME_PC_L")
				ELSE
					PRINT_TICKER_WITH_PLAYER_NAME("FMMC_ME_PC_R", EventData.piEquipmentCollector)
				ENDIF
			BREAK
		ENDSWITCH
		
		IF IS_BIT_SET(iMissionEquipment_DisplayXofXTickersForEquipmentTypeBS, iMissionEquipmentType)
			STRING sPickupName = GET_PUBLIC_FACING_NAME_FOR_MISSION_EQUIPMENT(iMissionEquipmentType, TRUE)
			PRINT_TICKER_WITH_STRING_AND_TWO_INTS("FMMC_ME_CLNM", sPickupName, iMissionEquipmentPickupsCollected[iMissionEquipmentType], iMissionEquipmentPickupsMax[iMissionEquipmentType])
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_SPAWN_PICKUP(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_SPAWN_PICKUP EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type != SCRIPT_EVENT_FMMC_SPAWN_PICKUP
			EXIT
		ENDIF
		
		PRINTLN("[MissionEquipment] PROCESS_SCRIPT_EVENT_FMMC_SPAWN_PICKUP - Pickup spawn, Pickup Id: ", EventData.iPickupId)
		// if spawning player is not local player, but is on same team
		IF EventData.Details.FromPlayerIndex != PLAYER_ID()
		AND GET_PLAYER_TEAM(PLAYER_ID()) = GET_PLAYER_TEAM(EventData.Details.FromPlayerIndex)
			// mark pickup for spawn
			FMMC_SET_LONG_BIT(iDroppedPickupBS, EventData.iPickupId)
			vDroppedPickupLocation[EventData.iPickupId] = EventData.vPickupLocation
		ENDIF
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: RP (Rockstar Points)	 -------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Process functions for RP events		  		--------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_STUNT_RP_EVENT(INT iCount)
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciUSE_STUNT_RP_BONUS)
		EXIT
	ENDIF
	
	STRUCT_STUNT_PERFORMED_EVENT seiStunt
	
	IF NOT GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, seiStunt, SIZE_OF(seiStunt))
		EXIT
	ENDIF
	
	PRINTLN("[RCC MISSION] EVENT_NETWORK_STUNT_PERFORMED ")
	
	FLOAT fValue = seiStunt.m_Value
	TrackedStuntType eSTUNT = INT_TO_ENUM(TrackedStuntType, seiStunt.m_StuntType)
	
	INT iRP
	XPCATEGORY XPCat
	
	BOOL bAwardRP, bBoost

	SWITCH eSTUNT
	    CASE ST_FRONTFLIP
			iRP = g_sMPTunables.iSTUNT_RP_BONUS_FRONTFLIP
			XPCat = XPCATEGORY_SKILL_RACE_STUNT_FFLIP
			bAwardRP = TRUE
			PRINTLN("[RCC MISSION] [STUNTS]  PROCESS_STUNT_RP_EVENT, ST_FRONTFLIP ")
			bBoost = TRUE
		BREAK
	    CASE ST_BACKFLIP
			iRP = g_sMPTunables.iSTUNT_RP_BONUS_BACKFLIP
			XPCat = XPCATEGORY_SKILL_RACE_STUNT_BFLIP
			bAwardRP = TRUE
			PRINTLN("[RCC MISSION] [STUNTS] PROCESS_STUNT_RP_EVENT, ST_BACKFLIP ")
			bBoost = TRUE
		BREAK
	    CASE ST_SPIN
			IF fValue >= g_sMPTunables.fStuntSpinTwoAndHalf
				iRP = g_sMPTunables.iSTUNT_RP_BONUS_900FLIP
				XPCat = XPCATEGORY_SKILL_RACE_STUNT_SPIN900
				bAwardRP = TRUE
				bBoost = TRUE
				PRINTLN("[RCC MISSION] [STUNTS] PROCESS_STUNT_RP_EVENT, XPCATEGORY_SKILL_RACE_STUNT_SPIN900 ")
			ELIF fValue >= g_sMPTunables.fStuntSpinTwo
				iRP = g_sMPTunables.iSTUNT_RP_BONUS_720FLIP
				XPCat = XPCATEGORY_SKILL_RACE_STUNT_SPIN720	
				bAwardRP = TRUE
				bBoost = TRUE
				PRINTLN("[RCC MISSION] [STUNTS] PROCESS_STUNT_RP_EVENT, XPCATEGORY_SKILL_RACE_STUNT_SPIN720 ")
			ELIF fValue >= g_sMPTunables.fStuntSpinOneAndHalf
				iRP = g_sMPTunables.iSTUNT_RP_BONUS_540FLIP
				XPCat = XPCATEGORY_SKILL_RACE_STUNT_SPIN540	
				bAwardRP = TRUE
				bBoost = TRUE
				PRINTLN("[RCC MISSION] [STUNTS] PROCESS_STUNT_RP_EVENT, XPCATEGORY_SKILL_RACE_STUNT_SPIN540 ")
			ELIF fValue >= g_sMPTunables.fStuntSpinOne
				iRP = g_sMPTunables.iSTUNT_RP_BONUS_360FLIP
				XPCat = XPCATEGORY_SKILL_RACE_STUNT_SPIN360	
				bAwardRP = TRUE
				bBoost = TRUE
				PRINTLN("[RCC MISSION] [STUNTS] PROCESS_STUNT_RP_EVENT, XPCATEGORY_SKILL_RACE_STUNT_SPIN360 ")
			ELSE
				PRINTLN("[RCC MISSION] [STUNTS] PROCESS_STUNT_RP_EVENT, XPCATEGORY_SKILL_RACE_STUNT_SPIN but fValue = ", fValue)
				EXIT
			ENDIF
		BREAK
	    CASE ST_WHEELIE
			IF fValue >= 3
				iRP = g_sMPTunables.iSTUNT_RP_BONUS_WHEELIE	
				XPCat = XPCATEGORY_SKILL_RACE_STUNT_WHEELIE
				bAwardRP = TRUE
				bBoost = TRUE
				PRINTLN("[RCC MISSION] [STUNTS] PROCESS_STUNT_RP_EVENT, ST_WHEELIE ")
			ENDIF
		BREAK
	    CASE ST_STOPPIE
			IF fValue >= 1
				iRP = g_sMPTunables.iSTUNT_RP_BONUS_STOPPIE	
				XPCat = XPCATEGORY_SKILL_RACE_STUNT_STOPPIE
				bAwardRP = TRUE
				bBoost = TRUE
				PRINTLN("[RCC MISSION] [STUNTS] PROCESS_STUNT_RP_EVENT, ST_STOPPIE ")
			ENDIF
		BREAK
		CASE ST_BOWLING_PIN
			iRP = g_sMPTunables.iSTUNT_RP_BONUS_PIN	
			XPCat = XPCATEGORY_SKILL_RACE_STUNT_BPIN
			bAwardRP = TRUE
			PRINTLN("[RCC MISSION] [STUNTS] PROCESS_STUNT_RP_EVENT, ST_BOWLING_PIN ")
		BREAK
		CASE ST_FOOTBALL
			iRP = g_sMPTunables.iSTUNT_RP_BONUS_BALL	
			XPCat = XPCATEGORY_SKILL_RACE_STUNT_FBALL
			bAwardRP = TRUE
			PRINTLN("[RCC MISSION] [STUNTS] PROCESS_STUNT_RP_EVENT, ST_FOOTBALL ")
		BREAK
		CASE ST_ROLL			
			XPCat = XPCATEGORY_SKILL_RACE_STUNT_ROLL
			IF fValue >= 0.85
				IF fValue < 1.0
					fValue = 1.0
				ENDIF
				//Add to it so if you do 90% of a roll it is fair
				fValue+=0.15
				iRP = g_sMPTunables.iSTUNT_RP_BONUS_BARRELROLL * FLOOR(fValue)
				bAwardRP = TRUE
				bBoost = TRUE
				PRINTLN("[RCC MISSION] [STUNTS] PROCESS_STUNT_RP_EVENT, ST_ROLL, iRP = g_sMPTunables.iSTUNT_RP_BONUS_BARRELROLL * FLOOR(fValue)  - So ", 
						iRP, "  = ", g_sMPTunables.iSTUNT_RP_BONUS_BARRELROLL, " * FLOOR(", fValue, ")")
			ELSE
				PRINTLN("[RCC MISSION] [STUNTS] PROCESS_STUNT_RP_EVENT, XPCATEGORY_SKILL_RACE_STUNT_ROLL but fValue = ", fValue)
				EXIT
			ENDIF
		BREAK
	ENDSWITCH
	
	IF bAwardRP
	AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciUSE_STUNT_RP_BONUS)
		
		// Store stunt RP
		iStuntRPEarned += ( iRP * ROUND(g_sMPTunables.xpMultiplier) )

		// Cap RP
		IF iStuntRPEarned > g_sMPtunables.imaxrpforstunts
			PRINTLN("[RCC MISSION] [STUNTS] PROCESS_STUNT_RP_EVENT, [CAP] iStuntRPEarned    = ", iStuntRPEarned)
			PRINTLN("[RCC MISSION] [STUNTS] PROCESS_STUNT_RP_EVENT, [CAP] g_sMPtunables.imaxrpforstunts = ", g_sMPtunables.imaxrpforstunts)
			iRP = ( g_sMPtunables.imaxrpforstunts - iStuntRPEarned )
			PRINTLN("[RCC MISSION] [STUNTS] PROCESS_STUNT_RP_EVENT, [CAP] CAPPING RP TO GIVE AT ", iRP)
		ENDIF
		
		IF iRP > 0
		
			// Give RP
			GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD, LocalPlayerPed, "XPT_RCE_STNT", XPTYPE_SKILL, XPCat, iRP, 1, DEFAULT, DEFAULT, DEFAULT, g_sMPtunables.istuntrpduration)
			
			PRINTLN("[RCC MISSION] [STUNTS] PROCESS_STUNT_RP_EVENT, iRP to give = ", iRP)
			PRINTLN("[RCC MISSION] [STUNTS] PROCESS_STUNT_RP_EVENT, RP total now = ", iStuntRPEarned)
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciRACE_BACKFLIP_BOOST)
		AND bBoost
			PRINTLN("[RCC MISSION] [STUNTS] PROCESS_STUNT_RP_EVENT - START_VEHICLE_BOOST")
			START_VEHICLE_BOOST(g_FMMC_STRUCT.fStuntBoost)
		ENDIF
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Generic Render Targets
// ##### Description: Functions related to the shared render target system which can be used by all entity types.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
PROC PROCESS_SCRIPT_EVENT_FMMC_SHARED_RENDER_TARGET_EVENT(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_SHARED_RENDERTARGET_EVENT EventData
	IF NOT GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		EXIT
	ENDIF
	
	IF EventData.Details.Type != SCRIPT_EVENT_FMMC_SHARED_RENDERTARGET_EVENT
		EXIT
	ENDIF
	
	INT iSharedRTIndex = GET_SHARED_RENDERTARGET_INDEX_FOR_ENTITY(EventData.iEntityType, EventData.iEntityIndex)
	PRINTLN("[SharedRTs][RT ", iSharedRTIndex, "] PROCESS_SCRIPT_EVENT_FMMC_SHARED_RENDER_TARGET_EVENT | Received event of type ", EventData.iSharedRTEventType)
	
	IF iSharedRTIndex = -1
	AND EventData.iSharedRTEventType != ciSHARED_RT_EVENT_TYPE__REGISTER_RENDERTARGET
		PRINTLN("[SharedRTs][RT ", iSharedRTIndex, "] PROCESS_SCRIPT_EVENT_FMMC_SHARED_RENDER_TARGET_EVENT | Couldn't find a registered Shared Render Target for entity type ", EventData.iEntityType, " and index ", EventData.iEntityIndex)
		ASSERTLN("[SharedRTs][RT ", iSharedRTIndex, "] PROCESS_SCRIPT_EVENT_FMMC_SHARED_RENDER_TARGET_EVENT | Couldn't find a registered Shared Render Target for entity type ", EventData.iEntityType, " and index ", EventData.iEntityIndex)
		EXIT
	ENDIF
	
	SWITCH EventData.iSharedRTEventType
	
		CASE ciSHARED_RT_EVENT_TYPE__REGISTER_RENDERTARGET
			PRINTLN("[SharedRTs][RT ", iSharedRTIndex, "] PROCESS_SCRIPT_EVENT_FMMC_SHARED_RENDER_TARGET_EVENT | Processing ciSHARED_RT_EVENT_TYPE__REGISTER_RENDERTARGET event!")
			ADD_SHARED_RENDERTARGET(EventData.mnEntityModel, EventData.iEntityType, EventData.iEntityIndex)
		BREAK
		
		CASE ciSHARED_RT_EVENT_TYPE__PERFORM_SCALEFORM_METHOD
			IF sSharedRTs[iSharedRTIndex].iSharedRT_State = ciSHARED_RT_STATE__PROCESS
				SWITCH EventData.iScaleformMethodType
					CASE ciSHARED_RT_EVENT_METHOD__NAS_MONITOR_SHOW_DEVICE_DETECTED_SCREEN
						PRINTLN("[SharedRTs][RT ", iSharedRTIndex, "] PROCESS_SCRIPT_EVENT_FMMC_SHARED_RENDER_TARGET_EVENT | Performing ciSHARED_RT_EVENT_METHOD__NAS_MONITOR_SHOW_DEVICE_DETECTED_SCREEN")
						BEGIN_SCALEFORM_MOVIE_METHOD(sSharedRTs[iSharedRTIndex].siScaleformIndex, "SHOW_WAITING_SCREEN")
						END_SCALEFORM_MOVIE_METHOD()
					BREAK
				ENDSWITCH
			ENDIF
		BREAK
		
		CASE ciSHARED_RT_EVENT_TYPE__GENERIC_START
			PERFORM_SHARED_RENDERTARGET_START_FUNCTIONALITY(sSharedRTs[iSharedRTIndex], iSharedRTIndex)
		BREAK
		
		CASE ciSHARED_RT_EVENT_TYPE__GENERIC_COMPLETE
			PERFORM_SHARED_RENDERTARGET_COMPLETE_FUNCTIONALITY(sSharedRTs[iSharedRTIndex], iSharedRTIndex)
		BREAK
		
	ENDSWITCH
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Score	 -------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Process functions for score events		  		----------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_FMMC_INCREMENT_REMOTE_PLAYER_SCORE(INT iCount)

	SCRIPT_EVENT_DATA_FMMC_PLAYER_SCORE ScoreData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ScoreData, SIZE_OF(ScoreData))
		
		IF ScoreData.Details.Type = SCRIPT_EVENT_FMMC_PLAYER_SCORE
			IF ScoreData.iServerKey = MC_ServerBD.iSessionScriptEventKey
				PRINTLN("[RCC MISSION] ScoreData.PlayerToIncrease = ",NATIVE_TO_INT(ScoreData.PlayerToIncrease))
				PRINTLN("[RCC MISSION] ScoreData.iamount = ",ScoreData.iamount) 
				PRINTLN("[RCC MISSION] player team: ",MC_playerBD[iPartToUse].iteam)
				PRINTLN("[RCC MISSION] player score: ",MC_Playerbd[iPartToUse].iPlayerScore) 
				IF g_FMMC_STRUCT.iMissionEndType != ciMISSION_SCORING_TYPE_TIME
					PRINTLN("[RCC MISSION] Mission type is points") 
					MC_Playerbd[iPartToUse].iPlayerScore= MC_Playerbd[iPartToUse].iPlayerScore + ScoreData.iamount
				ELSE
					PRINTLN("[RCC MISSION] Mission type is time") 
					MC_Playerbd[iPartToUse].tdMissionTime.Timer =GET_TIME_OFFSET(MC_Playerbd[iPartToUse].tdMissionTime.Timer,(ci_TIME_PER_POINT*1000*ScoreData.iamount))
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_FMMC_INCREMENTED_LOCAL_PLAYER_SCORE(INT iCount)
	
	SCRIPT_EVENT_DATA_FMMC_INCREMENTED_LOCAL_PLAYER_SCORE Event
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, Event, SIZE_OF(Event))
		IF Event.Details.Type = SCRIPT_EVENT_FMMC_INCREMENTED_LOCAL_PLAYER_SCORE
			IF bIsLocalPlayerHost
				PRINTLN("[RCC MISSION] PROCESS_FMMC_INCREMENTED_LOCAL_PLAYER_SCORE - Event.iTeam = ",Event.iTeam)
				PRINTLN("[RCC MISSION] PROCESS_FMMC_INCREMENTED_LOCAL_PLAYER_SCORE - Event.iPriority = ",Event.iPriority)
				PRINTLN("[RCC MISSION] PROCESS_FMMC_INCREMENTED_LOCAL_PLAYER_SCORE - Event.iScore = ",Event.iScore)
				PRINTLN("[RCC MISSION] PROCESS_FMMC_INCREMENTED_LOCAL_PLAYER_SCORE - Event.iVictimPart = ",Event.iVictimPart)
				
				IF Event.iTeam < FMMC_MAX_TEAMS
					IF Event.iPriority < FMMC_MAX_RULES
					AND Event.iPriority = MC_serverBD_4.iCurrentHighestPriority[Event.iTeam]
						MC_serverBD.iScoreOnThisRule[Event.iTeam] += Event.iScore
						PRINTLN("[RCC MISSION] PROCESS_FMMC_INCREMENTED_LOCAL_PLAYER_SCORE - total team score on rule = ",MC_serverBD.iScoreOnThisRule[Event.iTeam])
					ENDIF
				ENDIF
			ENDIF

		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_FMMC_LOCATE_CAPTURE_PLAYER_SCORE(INT iCount)
	SCRIPT_EVENT_DATA_FMMC_LOCATE_CAPTURE_PLAYER_SCORE Event
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, Event, SIZE_OF(Event))
		IF Event.Details.Type = SCRIPT_EVENT_FMMC_LOCATE_CAPTURE_PLAYER_SCORE
			PRINTLN("[RCC MISSION][PROCESS_FMMC_LOCATE_CAPTURE_PLAYER_SCORE] Calling PROCESS_FMMC_LOCATE_CAPTURE_PLAYER_SCORE")
			IF LocalPlayer = event.PlayerThatOwnsLocate
			AND MC_PlayerBD[iLocalPart].iteam = event.iPlayersTeam
				PRINTLN("[RCC MISSION][PROCESS_FMMC_LOCATE_CAPTURE_PLAYER_SCORE] This is the correct player")
				MC_playerBD[iLocalPart].iPersonalLocatePoints++
				PRINTLN("[RCC MISSION][PROCESS_FMMC_LOCATE_CAPTURE_PLAYER_SCORE] Incremented MC_playerBD[iLocalPart].iPersonalLocatePoints to: ", MC_playerBD[iLocalPart].iPersonalLocatePoints)
			ELSE
				PRINTLN("[RCC MISSION][PROCESS_FMMC_LOCATE_CAPTURE_PLAYER_SCORE] This is the incorrect player to process this event. Should be player ", NATIVE_TO_INT(event.PlayerThatOwnsLocate))
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_CANT_DECREMENT_SCORE(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_CANT_DECREMENT_SCORE EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_CANT_DECREMENT_SCORE
			PRINTLN("[JS] PROCESS_SCRIPT_EVENT_FMMC_CANT_DECREMENT_SCORE - Killer = ", EventData.iKillingPart)
			IF EventData.iKillingPart = iPartToUse
				PRINT_HELP("TLAD_HELP1")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Generic Event	 -----------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Process function for the generic reusable	event	  		--------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_FOR_INSTANCED_CONTENT EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT
			PARTICIPANT_INDEX piPart
			PLAYER_INDEX piPlayer
			PED_INDEX pedPlayer
			PED_INDEX pedPlaced
			VEHICLE_INDEX vehIndex
			INT iPed
			INT iCompanionIndex
			INT iSubBS[FMMC_MAX_RANDOM_ENTITY_SPAWN_GROUPS]
			
			SWITCH EventData.iType
				CASE g_ciInstancedContentEventType_AllPlayersToggleConsiderVehicle
					PRINTLN("[g_ciInstancedContentEventType_AllPlayersToggleConsiderVehicle] - iVeh: ", EventData.iVeh)
					NETWORK_INDEX netVeh
					VEHICLE_INDEX viVeh					
					netVeh = MC_serverBD_1.sFMMC_SBD.niVehicle[EventData.iVeh]	
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(netVeh)
						viVeh = NET_TO_VEH(netVeh)
					ELSE
						PRINTLN("[g_ciInstancedContentEventType_AllPlayersToggleConsiderVehicle] - NetID does not exist.")
						EXIT
					ENDIF					
					IF DOES_ENTITY_EXIST(viVeh)
					AND IS_VEHICLE_DRIVEABLE(viVeh)
						PRINTLN("[g_ciInstancedContentEventType_AllPlayersToggleConsiderVehicle] - Setting vehicle considered with bool: ", EventData.bToggle)
						SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(viVeh, EventData.bToggle)
					ENDIF
						
					IF EventData.bToggle
						IF NETWORK_HAS_CONTROL_OF_ENTITY(viVeh)
							SET_VEHICLE_HANDBRAKE(viVeh, FALSE)
						ENDIF
						SET_BIT(iVehDeliveryStoppedCancelled, EventData.iVeh)
					ELSE
						CLEAR_BIT(iVehDeliveryStoppedCancelled, EventData.iVeh)
					ENDIF
				BREAK
				
				CASE g_ciInstancedContentEventType_HeliCamLookingAtCar
					IF bIsLocalPlayerHost
						IF NOT IS_BIT_SET(MC_ServerBD_1.iPartLookingAtCar, EventData.iPart)
							PRINTLN("[g_ciInstancedContentEventType_HeliCamLookingAtCar] - Setting iPart: ", EventData.iPart, " is Looking at Car.")
							SET_BIT(MC_ServerBD_1.iPartLookingAtCar, EventData.iPart)
						ENDIF
					ENDIF
				BREAK
				
				CASE g_ciInstancedContentEventType_HeliCamStoppedLookingAtCar
					IF bIsLocalPlayerHost
						IF IS_BIT_SET(MC_ServerBD_1.iPartLookingAtCar, EventData.iPart)
							PRINTLN("[g_ciInstancedContentEventType_HeliCamStoppedLookingAtCar] - Setting iPart: ", EventData.iPart, " Has stopped looking at car")
							CLEAR_BIT(MC_ServerBD_1.iPartLookingAtCar, EventData.iPart)
						ENDIF
					ENDIF
				BREAK
				
				CASE g_ciInstancedContentEventType_CarnageBarKilledPed		
					IF bIsLocalPlayerHost
						PRINTLN("[g_ciInstancedContentEventType_CarnageBarKilledPed] - Setting iPart: ", EventData.iPart, " has killed a ped.")
						MC_ServerBD_1.fCarnageBar_PedKilledForFill++
						PRINTLN("[LM][CARNAGE_BAR] - Killed Ped fCarnageBar_PedKilledForFill: ", MC_ServerBD_1.fCarnageBar_PedKilledForFill)
					ENDIF					
				BREAK
				
				CASE g_ciInstancedContentEventType_CarnageBarDestroyedVehicle
					IF bIsLocalPlayerHost
						PRINTLN("[g_ciInstancedContentEventType_CarnageBarDestroyedVehicle] - Setting iPart: ", EventData.iPart, " has destroyed a vehicle.")
						MC_ServerBD_1.fCarnageBar_VehDestroyedForFill++
						PRINTLN("[LM][CARNAGE_BAR] - Destroyed Veh fCarnageBar_VehDestroyedForFill: ", MC_ServerBD_1.fCarnageBar_VehDestroyedForFill)
					ENDIF	
				BREAK
				
				CASE g_ciInstancedContentEventType_CarnageBarPerformedStuntAir
					IF bIsLocalPlayerHost
						PRINTLN("[g_ciInstancedContentEventType_CarnageBarPerformedStuntAir] - Setting iPart: ", EventData.iPart, " has performed a stunt..")
						MC_ServerBD_1.fCarnageBar_StuntPerformedForFill++
						PRINTLN("[LM][CARNAGE_BAR] - Destroyed Veh fCarnageBar_StuntPerformedForFill: ", MC_ServerBD_1.fCarnageBar_StuntPerformedForFill)
					ENDIF	
				BREAK
				
				CASE g_ciInstancedContentEventType_ActivateZoneRadiusChange
					SET_BIT(iZoneBS_RadiusChangeTriggered, EventData.iZone)
				BREAK
				
				CASE g_ciInstancedContentEventType_PedClearedFixationSettings
					PRINTLN("[g_ciInstancedContentEventType_PedClearedFixationSettings] - Setting iPart: ", EventData.iPart, " has reset fixation settings for iPed: ", EventData.iPed)
					FMMC_SET_LONG_BIT(iPedFixationStateCleanedUpBS, EventData.iPed)
				BREAK
				
				CASE g_ciInstancedcontentEventType_TrailerDetachedForPed					
					PRINTLN("[g_ciInstancedcontentEventType_TrailerDetachedForPed] - Setting iPart: ", EventData.iPart, " Trailer Detached for iPed: ", EventData.iPed)
					FMMC_SET_LONG_BIT(iPedTrailerDetachedBitset, EventData.iPed)
				BREAK
				
				CASE g_ciInstancedcontentEventType_HasTrailerattachedForPed
					PRINTLN("[g_ciInstancedcontentEventType_HasTrailerattachedForPed] - Setting iPart: ", EventData.iPart, " Trailer Detached for iPed: ", EventData.iPed)
					FMMC_SET_LONG_BIT(iPedHasTrailerBitset, EventData.iPed)
				BREAK
							
				CASE g_ciInstancedcontentEventType_WarpedPedOnRuleStart
					PRINTLN("[g_ciInstancedcontentEventType_WarpedPedOnRuleStart] - Setting iPart: ", EventData.iPart, " Warped Ped on rule start iPed: ", EventData.iGenericInt, " Setting iPedWarpedOnThisRule.")
					FMMC_SET_LONG_BIT(iPedWarpedOnThisRule, EventData.iGenericInt)
					
					IF sMissionPedsLocalVars[EventData.iGenericInt].ePedSyncSceneState > PED_SYNC_SCENE_STATE_INIT
					AND sMissionPedsLocalVars[EventData.iGenericInt].ePedSyncSceneState < PED_SYNC_SCENE_STATE_CLEANUP							
						CLEAR_BIT(sMissionPedsLocalVars[EventData.iGenericInt].iPedBS, ci_MissionPedBS_iSyncSceneAnimationReady)				
						SET_PED_SYNC_SCENE_STATE(EventData.iGenericInt, PED_SYNC_SCENE_STATE_INIT)						
					ENDIF					
				BREAK
				CASE g_ciInstancedcontentEventType_WarpedVehicleOnRuleStart
					PRINTLN("[g_ciInstancedcontentEventType_WarpedVehicleOnRuleStart] - Setting iPart: ", EventData.iPart, " Warped Vehicle on rule start iVeh: ", EventData.iGenericInt, " Setting iVehicleWarpedOnThisRule.")
					FMMC_SET_LONG_BIT(iVehicleWarpedOnThisRule, EventData.iGenericInt)
					CLEAR_BIT(iVehicleHasBeenPlacedIntoInteriorBS, EventData.iGenericInt)
				BREAK	
				CASE g_ciInstancedcontentEventType_WarpedObjectOnRuleStart
					PRINTLN("[g_ciInstancedcontentEventType_WarpedObjectOnRuleStart] - Setting iPart: ", EventData.iPart, " Warped Object on rule start iObj: ", EventData.iGenericInt, " Setting iObjectWarpedOnThisRule.")
					FMMC_SET_LONG_BIT(iObjectWarpedOnThisRule, EventData.iGenericInt)
					CLEAR_BIT(iObjectHasBeenPlacedIntoInteriorBS, EventData.iGenericInt)
				BREAK
				CASE g_ciInstancedcontentEventType_WarpedGotoLocateOnRuleStart
					PRINTLN("[g_ciInstancedcontentEventType_WarpedGotoLocateOnRuleStart] - Setting iPart: ", EventData.iPart, " Warped Goto-Locate on rule start iLoc: ", EventData.iGenericInt, " Setting iGoToLocationWarpedOnThisRule.")
					FMMC_SET_LONG_BIT(iGoToLocationWarpedOnThisRule, EventData.iGenericInt)
				BREAK
				CASE g_ciInstancedcontentEventType_WarpedInteractableOnRuleStart
					PRINTLN("[g_ciInstancedcontentEventType_WarpedInteractableOnRuleStart] - Setting iPart: ", EventData.iPart, " Warped Interactable on rule start iInteractable: ", EventData.iGenericInt, " Setting iInteractableWarpedOnThisRule.")
					FMMC_SET_LONG_BIT(iInteractableWarpedOnThisRule, EventData.iGenericInt)
				BREAK
				
				CASE g_ciInstancedcontentEventType_SyncSceneBroadcast
					PRINTLN("[g_ciInstancedcontentEventType_SyncSceneBroadcast] - Setting iPart: ", EventData.iPart, " assigning iPed: ", EventData.iPed, " to iSyncScene: ", EventData.iGenericInt)
					sMissionPedsLocalVars[EventData.iPed].iSyncScene = EventData.iGenericInt
				BREAK
				
				CASE g_ciInstancedcontentEventType_SetBitset_SyncScene
					PRINTLN("[g_ciInstancedcontentEventType_SetBitset_SyncScene] - Setting iPart: ", EventData.iPart, " assigning iPed: ", EventData.iPed, " Setting BITSET: ", EventData.iGenericInt)
					SET_BIT(sMissionPedsLocalVars[EventData.iPed].iPedBS, EventData.iGenericInt)
				BREAK
				
				CASE g_ciInstancedcontentEventType_SetPedSyncSceneState
				CASE g_ciInstancedcontentEventType_SetPedSyncSceneStateWithEarlyCleanup
				
					IF INT_TO_ENUM(PED_SYNC_SCENE_STATE, EventData.iGenericInt) = PED_SYNC_SCENE_STATE_INIT
						PRINTLN("[g_ciInstancedcontentEventType_SetBitset_PedFinishedBreakoutAnims][Sync_anims: ", EventData.iPed, "] - Setting iPart: ", EventData.iPart, " assigning iPed: ", EventData.iPed, " Resetting Sync Scene Data.")
						SET_BIT(sMissionPedsLocalVars[EventData.iPed].iPedBS, ci_MissionPedBS_iSyncSceneAnimationNeedResync)
						CLEAR_BIT(sMissionPedsLocalVars[EventData.iPed].iPedBS, ci_MissionPedBS_iSyncSceneAnimationReady)
						CLEAR_BIT(sMissionPedsLocalVars[EventData.iPed].iPedBS, ci_MissionPedBS_iSyncSceneAnimationFinished)
						CLEAR_BIT(sMissionPedsLocalVars[EventData.iPed].iPedBS, ci_MissionPedBS_SyncScene_CleanupSyncSceneEarly)
						SET_PED_SYNC_SCENE_STATE(EventData.iPed, INT_TO_ENUM(PED_SYNC_SCENE_STATE, EventData.iGenericInt))
						EXIT
					ENDIF
				
					IF EventData.iPart != iLocalPart // Sender/Host will set this locally. We only care about broadcasting this to sync data in case of a host migration.
					OR EventData.bToggle // bToggle can be set to have this set even for the event sender
						PRINTLN("[g_ciInstancedcontentEventType_SetBitset_PedFinishedBreakoutAnims][Sync_anims: ", EventData.iPed, "] - Setting iPart: ", EventData.iPart, " assigning iPed: ", EventData.iPed, " Setting ci_MissionPedBS_iSyncSceneAnimationBreakoutFinished.")
						SET_PED_SYNC_SCENE_STATE(EventData.iPed, INT_TO_ENUM(PED_SYNC_SCENE_STATE, EventData.iGenericInt))
					ENDIF
					
					IF EventData.iType = g_ciInstancedcontentEventType_SetPedSyncSceneStateWithEarlyCleanup
						SET_BIT(sMissionPedsLocalVars[EventData.iPed].iPedBS, ci_MissionPedBS_SyncScene_CleanupSyncSceneEarly)
					ENDIF
				BREAK
				
				CASE g_ciInstancedcontentEventType_RequestProgressToRule
					IF bIsLocalPlayerHost
						PRINTLN("[g_ciInstancedcontentEventType_RequestProgressToRule] - Setting iPart: ", EventData.iPart, " Progressing to iRule: ", EventData.iGenericInt)
						SET_PROGRESS_OBJECTIVE_FOR_TEAM(MC_PlayerBD[EventData.iPart].iTeam, TRUE, EventData.iGenericInt)
					ENDIF
				BREAK
				
				CASE g_ciInstancedcontentEventType_RequestFailRule
					IF bIsLocalPlayerHost
						PRINTLN("[g_ciInstancedcontentEventType_RequestFailRule] - Setting iPart: ", EventData.iPart, " Failing iRule: ", EventData.iGenericInt)
						INVALIDATE_SINGLE_OBJECTIVE(MC_PlayerBD[EventData.iPart].iTeam, EventData.iGenericInt, FALSE, FALSE)
						SET_IGNORE_CHECKPOINT_PROCESSING_FOR_RULE_SKIP(MC_PlayerBD[EventData.iPart].iTeam)
					ENDIF
				BREAK
				
				CASE g_ciInstancedcontentEventType_SyncScenePedOwnerBroadcast
					PRINTLN("[g_ciInstancedcontentEventType_SyncScenePedOwnerBroadcast][Sync_anims: ", EventData.iPed, "] - Setting iPart: ", EventData.iPart, " as the anim owner for assigning iPed: ", EventData.iPed)
					IF EventData.iPart != iLocalPart
						sMissionPedsLocalVars[EventData.iPed].iSyncPedPartOwner = EventData.iPart
					ENDIF
				BREAK
				
				CASE g_ciInstancedcontentEventType_ClearRoomForPlayerPed
					PRINTLN("[g_ciInstancedcontentEventType_ClearRoomForPlayerPed] - Setting iPart: ", EventData.iPart, " requesting room key be cleared...")
					
					IF EventData.iPart != iLocalPart
						PRINTLN("[g_ciInstancedcontentEventType_ClearRoomForPlayerPed] - Performing Validity Checks.")
						
						piPart = INT_TO_PARTICIPANTINDEX(EventData.iPart)
						
						IF NETWORK_IS_PARTICIPANT_ACTIVE(piPart)							
							piPlayer = NETWORK_GET_PLAYER_INDEX(piPart)
							pedPlayer = GET_PLAYER_PED(piPlayer)
							
							IF NOT IS_PED_INJURED(pedPlayer)
							
								PRINTLN("[LM][RCC MISSION]  [MISSION START POS] - CLEAR_ROOM_AFTER_START_POSITION_WARP - Calling CLEAR_ROOM_FOR_ENTITY for Ped.")
								
								IF IS_PED_IN_ANY_VEHICLE(pedPlayer, TRUE)
									VEHICLE_INDEX vehTemp
									vehTemp = GET_VEHICLE_PED_IS_IN(pedPlayer, TRUE)
									
									IF IS_VEHICLE_DRIVEABLE(vehTemp)
										PRINTLN("[LM][RCC MISSION]  [MISSION START POS] - CLEAR_ROOM_AFTER_START_POSITION_WARP - Calling CLEAR_ROOM_FOR_ENTITY for Vehicle.")										
										
										IF NETWORK_HAS_CONTROL_OF_ENTITY(vehTemp)
											PRINTLN("[LM][RCC MISSION]  [MISSION START POS] - CLEAR_ROOM_AFTER_START_POSITION_WARP - Calling SET_ENTITY_VISIBLE for Vehicle.")
											CLEAR_ROOM_FOR_ENTITY(vehTemp)
											SET_ENTITY_VISIBLE(vehTemp, TRUE)
										ENDIF
									ENDIF
								ENDIF
								
								IF NETWORK_HAS_CONTROL_OF_ENTITY(pedPlayer)
									PRINTLN("[LM][RCC MISSION]  [MISSION START POS] - CLEAR_ROOM_AFTER_START_POSITION_WARP - Calling SET_ENTITY_VISIBLE for Ped.")
									SET_ENTITY_VISIBLE(pedPlayer, TRUE)
									CLEAR_ROOM_FOR_ENTITY(pedPlayer)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE g_ciInstancedcontentEventType_SetVehVisible
					PRINTLN("[g_ciInstancedcontentEventType_SetVehVisible] called with iVeh: ", EventData.iVeh)
					IF EventData.iVeh > -1
						NETWORK_INDEX niVeh
						niVeh = MC_serverBD_1.sFMMC_SBD.niVehicle[EventData.iVeh]
						IF NETWORK_DOES_NETWORK_ID_EXIST(niVeh)
							VEHICLE_INDEX veh
							veh = NET_TO_VEH(niVeh)
							IF IS_VEHICLE_DRIVEABLE(veh)
								IF NETWORK_HAS_CONTROL_OF_ENTITY(veh)
									PRINTLN("[g_ciInstancedcontentEventType_SetVehVisible] Setting iVeh: ", EventData.iVeh, " as visible")
									SET_ENTITY_VISIBLE(veh, TRUE)
								ELSE
									PRINTLN("[g_ciInstancedcontentEventType_SetVehVisible] cant set iVeh: ", EventData.iVeh, " as visible as we do not have control")
								ENDIF
							ELSE
								PRINTLN("[g_ciInstancedcontentEventType_SetVehVisible] iVeh: ", EventData.iVeh, " is not driveable")
							ENDIF
						ELSE
							PRINTLN("[g_ciInstancedcontentEventType_SetVehVisible] iVeh: ", EventData.iVeh, " Does not exist")
						ENDIF
					ENDIF
				BREAK
				
				CASE g_ciInstancedcontentEventType_HideHeistBagForPart
					PRINTLN("[g_ciInstancedcontentEventType_HideHeistBagForPart] iPart: ", EventData.iPart, " Having their duffel bag hidden")
					
					piPart = INT_TO_PARTICIPANTINDEX(EventData.iPart)					
					IF NETWORK_IS_PARTICIPANT_ACTIVE(piPart)
						piPlayer = NETWORK_GET_PLAYER_INDEX(piPart)
						pedPlayer = GET_PLAYER_PED(piPlayer)
						
						IF NOT IS_PED_INJURED(pedPlayer)
							IF EventData.iPart = iLocalPart
								SET_PED_COMPONENT_VARIATION(pedPlayer, PED_COMP_HAND, 0, 0)
							ELSE
								SET_PED_COMPONENT_VARIATION(pedPlayer, PED_COMP_HAND, 0, 0)
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE g_ciInstancedcontentEventType_ShowHeistBagForPart
					PRINTLN("[g_ciInstancedcontentEventType_ShowHeistBagForPart] iPart: ", EventData.iPart, " Having their duffel bag shown")
					
					piPart = INT_TO_PARTICIPANTINDEX(EventData.iPart)					
					IF NETWORK_IS_PARTICIPANT_ACTIVE(piPart)
						piPlayer = NETWORK_GET_PLAYER_INDEX(piPart)						
						pedPlayer = GET_PLAYER_PED(piPlayer)
						
						IF NOT IS_PED_INJURED(pedPlayer)
							IF EventData.iPart = iLocalPart
								APPLY_DUFFEL_BAG_HEIST_GEAR(pedPlayer, iLocalPart)
							ELSE
								APPLY_DUFFEL_BAG_HEIST_GEAR(pedPlayer, EventData.iPart)
							ENDIF
						ENDIF
					ENDIF
				BREAK
				CASE g_ciInstancedcontentEventType_PlayDialogueTriggerIndexOnRemoteClients
					IF iLocalPart != EventData.iPart
					AND EventData.iGenericInt != -1
						PRINTLN("[g_ciInstancedcontentEventType_PlayDialogueTriggerIndexOnRemoteClients] iPart: ", EventData.iPart, " Having Dialogue Trigger: ", EventData.iGenericInt, " Set to priority play remotely by: ", EventData.iPart)
						SET_BIT(iLocalDialoguePlayRemotelyWithPriority[EventData.iGenericInt/32],EventData.iGenericInt%32)
					ENDIF
				BREAK
				
				CASE g_ciInstancedcontentEventType_CleanupPickupForAllPlayers
					IF iLocalPart != EventData.iPart
					AND EventData.iGenericInt != -1
						PRINTLN("[g_ciInstancedcontentEventType_CleanupPickupForAllPlayers] iPart: ", EventData.iPart, " Having everyone cleanup Pickup Index: ", EventData.iGenericInt)
						IF NOT FMMC_IS_LONG_BIT_SET(iPickupCollectedCleanupLocally, EventData.iGenericInt)
							FMMC_SET_LONG_BIT(iPickupCollectedCleanupLocally, EventData.iGenericInt)
							iPickupRespawns[EventData.iGenericInt]--
							PRINTLN("[g_ciInstancedcontentEventType_CleanupPickupForAllPlayers] iPart: ", EventData.iPart, " iPickupRespawns[", EventData.iGenericInt, "]: ", iPickupRespawns[EventData.iGenericInt])
						ENDIF
					ENDIF
				BREAK
				
				CASE g_ciInstancedcontentEventType_ForcePedDropPickupIndexChance
					PRINTLN("[g_ciInstancedcontentEventType_ForcePedDropPickupIndexChance] iPart: ", EventData.iPart, " Having everyone Force Ped: ", EventData.iPed, " to drop their pickup index")
					FMMC_SET_LONG_BIT(iPedForceDropPickup, EventData.iPed)
				BREAK
				
				// Ped Ass Goto Data start 
				CASE g_ciInstancedcontentEventType_DATA_SET_PedGotoAssCompletedWaitBitset	
					PRINTLN("[g_ciInstancedcontentEventType_DATA_SET_PedGotoAssCompletedWaitBitset] iPart: ", EventData.iPart, " Having everyone Set Ped: ", EventData.iPed, " bitset.")
					FMMC_SET_LONG_BIT(iPedGotoAssCompletedWaitBitset, EventData.iPed)
				BREAK			
				CASE g_ciInstancedcontentEventType_DATA_SET_PedGotoAssCompletedWaitPlayerVehExitBitset	
					PRINTLN("[g_ciInstancedcontentEventType_DATA_SET_PedGotoAssCompletedWaitPlayerVehExitBitset] iPart: ", EventData.iPart, " Having everyone Set Ped: ", EventData.iPed, " bitset.")
					FMMC_SET_LONG_BIT(iPedGotoAssCompletedWaitPlayerVehExitBitset, EventData.iPed)
				BREAK
				CASE g_ciInstancedcontentEventType_DATA_SET_PedGotoAssCompletedAchieveHeadingBitset	
					PRINTLN("[g_ciInstancedcontentEventType_DATA_SET_PedGotoAssCompletedAchieveHeadingBitset] iPart: ", EventData.iPart, " Having everyone Set Ped: ", EventData.iPed, " bitset.")
					FMMC_SET_LONG_BIT(iPedGotoAssCompletedAchieveHeadingBitset, EventData.iPed)
				BREAK
				CASE g_ciInstancedcontentEventType_DATA_SET_PedGotoAssCompletedDefAreaRefreshBitset
					PRINTLN("[g_ciInstancedcontentEventType_DATA_SET_PedGotoAssCompletedDefAreaRefreshBitset] iPart: ", EventData.iPart, " Having everyone Set Ped: ", EventData.iPed, " bitset.")
					FMMC_SET_LONG_BIT(iPedGotoAssCompletedCombatGotoBitset, EventData.iPed)
				BREAK
				CASE g_ciInstancedcontentEventType_DATA_SET_PedGotoAssCompletedWaitPlayIdleBitset		
					PRINTLN("[g_ciInstancedcontentEventType_DATA_SET_PedGotoAssCompletedWaitPlayIdleBitset] iPart: ", EventData.iPart, " Having everyone Set Ped: ", EventData.iPed, " bitset.")
					FMMC_SET_LONG_BIT(iPedGotoAssCompletedWaitPlayIdleBitset, EventData.iPed)
				BREAK
				CASE g_ciInstancedcontentEventType_DATA_SET_PedGotoAssCompletedLeaveVehicleBitset	
					PRINTLN("[g_ciInstancedcontentEventType_DATA_SET_PedGotoAssCompletedLeaveVehicleBitset] iPart: ", EventData.iPart, " Having everyone Set Ped: ", EventData.iPed, " bitset.")
					FMMC_SET_LONG_BIT(iPedGotoAssCompletedLeaveVehicleBitset, EventData.iPed)
				BREAK	
				CASE g_ciInstancedcontentEventType_DATA_SET_PedGotoAssCompletedLandVehicleBitset	
					PRINTLN("[g_ciInstancedcontentEventType_DATA_SET_PedGotoAssCompletedLandVehicleBitset] iPart: ", EventData.iPart, " Having everyone Set Ped: ", EventData.iPed, " bitset.")
					FMMC_SET_LONG_BIT(iPedGotoAssCompletedLandVehicleBitset, EventData.iPed)
				BREAK	
				CASE g_ciInstancedcontentEventType_DATA_SET_PedGotoAssCompletedRappelFromVehBitset		
					PRINTLN("[g_ciInstancedcontentEventType_DATA_SET_PedGotoAssCompletedRappelFromVehBitset] iPart: ", EventData.iPart, " Having everyone Set Ped: ", EventData.iPed, " bitset.")
					FMMC_SET_LONG_BIT(iPedGotoAssCompletedRappelFromVehBitset, EventData.iPed)
				BREAK
				CASE g_ciInstancedcontentEventType_DATA_SET_PedGotoAssCompletedHoverHeliBitset		
					PRINTLN("[g_ciInstancedcontentEventType_DATA_SET_PedGotoAssCompletedHoverHeliBitset] iPart: ", EventData.iPart, " Having everyone Set Ped: ", EventData.iPed, " bitset.")
					FMMC_SET_LONG_BIT(iPedGotoAssCompletedHoverHeliBitset, EventData.iPed)
				BREAK	
				CASE g_ciInstancedcontentEventType_DATA_SET_PedGotoAssCompletedEnterVehicleBitset		
					PRINTLN("[g_ciInstancedcontentEventType_DATA_SET_PedGotoAssCompletedEnterVehicleBitset] iPart: ", EventData.iPart, " Having everyone Set Ped: ", EventData.iPed, " bitset.")
					FMMC_SET_LONG_BIT(iPedGotoAssCompletedEnterVehicleBitset, EventData.iPed)
				BREAK
				CASE g_ciInstancedcontentEventType_DATA_SET_PedGotoAssCompletedCleanupBitset
					PRINTLN("[g_ciInstancedcontentEventType_DATA_SET_PedGotoAssCompletedCleanupBitset] iPart: ", EventData.iPart, " Having everyone Set Ped: ", EventData.iPed, " bitset.")
					FMMC_SET_LONG_BIT(iPedGotoAssCompletedCleanupBitset, EventData.iPed)
				BREAK	
				CASE g_ciInstancedcontentEventType_DATA_SET_PedGotoAssCompletedCustomScenartioBitset
					PRINTLN("[g_ciInstancedcontentEventType_DATA_SET_PedGotoAssCompletedCustomScenartioBitset] iPart: ", EventData.iPart, " Having everyone Set Ped: ", EventData.iPed, " bitset.")
					FMMC_SET_LONG_BIT(iPedGotoAssCompletedCustomScenarioBitset, EventData.iPed)
				BREAK
				CASE g_ciInstancedcontentEventType_DATA_SET_PedGotoAssDisableLockOnBitset
					PRINTLN("[g_ciInstancedcontentEventType_DATA_SET_PedGotoAssDisableLockOnBitset] iPart: ", EventData.iPart, " Having everyone Set Ped: ", EventData.iPed, " bitset.")
					FMMC_SET_LONG_BIT(iPedGotoAssCompletedDisableLockOnBitset, EventData.iPed)
				BREAK
				CASE g_ciInstancedcontentEventType_StartCustomScenarioTimerForPed
					IF iLocalPart != EventData.iPart
						INT iScenarioIndex 
						iScenarioIndex = GET_CUSTOM_SCENARIO_INDEX_FOR_PED(EventData.iPed)
						IF iScenarioIndex != -1
							PRINTLN("[g_ciInstancedcontentEventType_StartCustomScenarioTimerForPed] iPart: ", EventData.iPart, " Having everyone start Ped: ", EventData.iPed, "'s custom scenario timer ", EventData.iGenericInt)
							REINIT_NET_TIMER(tdAssociatedGOTOCustomScenarioTimer[iScenarioIndex][ciFMMC_CUSTOM_SCENARIO_TIMER_SKIP])
						ENDIF
					ENDIF
				BREAK
				CASE g_ciInstancedcontentEventType_SetCustomScenarioIntForPed
					IF iLocalPart != EventData.iPart
						INT iScenarioIndex 
						iScenarioIndex = GET_CUSTOM_SCENARIO_INDEX_FOR_PED(EventData.iPed)
						IF iScenarioIndex != -1
							PRINTLN("[g_ciInstancedcontentEventType_SetCustomScenarioIntForPed] iPart: ", EventData.iPart, " Having everyone Set Ped: ", EventData.iPed, "'s custom scenario int to ", EventData.iGenericInt)
							iAssociatedGOTOCustomScenarioInt[iScenarioIndex][EventData.iLoc] = EventData.iGenericInt //Needed a second INT and iLoc comes straight after iGenericInt
						ENDIF
					ENDIF
				BREAK
				CASE g_ciInstancedcontentEventType_DATA_SET_PedGotoAssCompleteCombatGotoBitset
					PRINTLN("[g_ciInstancedcontentEventType_DATA_SET_PedGotoAssCompleteCombatGotoBitset] iPart: ", EventData.iPart, " Having everyone Set Ped: ", EventData.iPed, " bitset.")					
					FMMC_SET_LONG_BIT(iPedGotoAssCompletedCombatGotoBitset, EventData.iPed)
				BREAK
				CASE g_ciInstancedcontentEventType_DATA_SET_PedGotoAssCompletedWaitCombatBitset		
					PRINTLN("[g_ciInstancedcontentEventType_DATA_SET_PedGotoAssCompletedWaitCombatBitset] iPart: ", EventData.iPart, " Having everyone Set Ped: ", EventData.iPed, " bitset.")					
					FMMC_SET_LONG_BIT(iPedGotoAssUsedWaitCombatBitset, EventData.iPed)
					FMMC_SET_LONG_BIT(iPedGotoAssCompletedWaitCombatBitset, EventData.iPed)
				BREAK
				CASE g_ciInstancedcontentEventType_DATA_SET_PedGotoAssCleanedUpWaitCombatBitset		
					PRINTLN("[g_ciInstancedcontentEventType_DATA_SET_PedGotoAssCleanedUpWaitCombatBitset] iPart: ", EventData.iPart, " Having everyone Set Ped: ", EventData.iPed, " bitset.")					
					FMMC_CLEAR_LONG_BIT(iPedGotoAssUsedWaitCombatBitset, EventData.iPed)					
				BREAK
				// Ped Ass Goto Data end
				
				CASE g_ciInstancedcontentEventType_FMMCPickupCollected
					PRINTLN("[g_ciInstancedcontentEventType_FMMCPickupCollected][Pickups][Pickup ", EventData.iGenericInt, "] Pickup ", EventData.iGenericInt, " has been collected by participant ", EventData.iPart)
					PROCESS_COLLECTED_PICKUP_PREREQS(EventData.iGenericInt, EventData.Details.FromPlayerIndex)
					
					IF IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_WeaponPickupTracking)
						IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[EventData.iGenericInt].iContinuityId != -1
							IF NOT FMMC_IS_LONG_BIT_SET(sMissionLocalContinuityVars.iPickupCollectedTrackingBitset, g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[EventData.iGenericInt].iContinuityId)
								FMMC_SET_LONG_BIT(sMissionLocalContinuityVars.iPickupCollectedTrackingBitset, g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[EventData.iGenericInt].iContinuityId)
								PRINTLN("[PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_FMMCPickupCollected][Pickups][Pickup ", EventData.iGenericInt, "] Pickup ", EventData.iGenericInt, " with continuity ID ", g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[EventData.iGenericInt].iContinuityId, " collected")
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE g_ciInstancedcontentEventType_FMMCObjectDroppedOnDeath
					PRINTLN("[PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_FMMCObjectDroppedOnDeath][Objects][Object ", EventData.iGenericInt, "] Object ", EventData.iGenericInt, " was dropped when part ", EventData.iPart, " died holding it!")
					SET_OBJECT_NEEDS_TO_RE_CACHE_OUT_OF_BOUNDS(EventData.iGenericInt)
					
					IF ((MC_serverBD_4.iObjRule[EventData.iGenericInt][GET_LOCAL_PLAYER_TEAM(TRUE)] = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER
					OR MC_serverBD_4.iObjRule[EventData.iGenericInt][GET_LOCAL_PLAYER_TEAM(TRUE)] = FMMC_OBJECTIVE_LOGIC_PROTECT
					OR MC_serverBD_4.iObjRule[EventData.iGenericInt][GET_LOCAL_PLAYER_TEAM(TRUE)] = FMMC_OBJECTIVE_LOGIC_CAPTURE
					OR MC_serverBD_4.iObjRule[EventData.iGenericInt][GET_LOCAL_PLAYER_TEAM(TRUE)] = FMMC_OBJECTIVE_LOGIC_GET_AND_HOLD)
					AND MC_serverBD_4.iObjPriority[EventData.iGenericInt][GET_LOCAL_PLAYER_TEAM(TRUE)] <= GET_LOCAL_PLAYER_CURRENT_RULE(TRUE))
					OR (MC_serverBD_4.iCurrentHighestPriority[GET_LOCAL_PLAYER_TEAM(TRUE)] < FMMC_MAX_RULES
					AND g_FMMC_STRUCT.sFMMCEndConditions[GET_LOCAL_PLAYER_TEAM(TRUE)].iObjRequiredForRulePass[MC_serverBD_4.iCurrentHighestPriority[GET_LOCAL_PLAYER_TEAM(TRUE)]] = EventData.iGenericInt)
							
						// Set data to tell the dialogue system that this object was dropped
						SET_BIT(iDialogueObjectiveObjectDroppedBS, EventData.iGenericInt)
						
						// Set data to say when the ObjectDropped bitset should be cleared again in the dialogue system
						iDialogueObjectiveObjectDroppedAtStaggeredIndex = iDialogueProgress - 1
						IF iDialogueObjectiveObjectDroppedAtStaggeredIndex = -1
							iDialogueObjectiveObjectDroppedAtStaggeredIndex = g_FMMC_STRUCT.iNumberOfDialogueTriggers - 1
						ENDIF
						
						PRINTLN("[PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_FMMCObjectDroppedOnDeath][Objects][Object ", EventData.iGenericInt, "] Setting ", EventData.iGenericInt, " in iDialogueObjectiveObjectDroppedBS and setting iDialogueObjectiveObjectDroppedAtStaggeredIndex to ", iDialogueObjectiveObjectDroppedAtStaggeredIndex)

					ENDIF
				BREAK
				
				CASE g_ciInstancedcontentEventType_PickedUpObject
					iPlayerCollectedObjectID = EventData.iGenericInt
					iPlayerCollectedObjectTeam = MC_playerBD[EventData.iPart].iTeam
					PRINTLN("[PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_PickedUpObject][Objects][Object ", EventData.iGenericInt, "]")
				BREAK
				
				CASE g_ciInstancedcontentEventType_ClearVehicleDamageRecord
					FMMC_SET_LONG_BIT(iCleanVehicleDamageRecordBS, EventData.iVeh)
					PRINTLN("[PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_ClearVehicleDamageRecord][Vehicle ", EventData.iVeh, "] - Vehicle's damage record will be cleared.")
				BREAK
			
				CASE g_ciInstancedcontentEventType_HackingFail
					PRINTLN("[PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_HackingFail] - Hacking Fail just happened - caused by iPart: ", EventData.iPart)
					
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySeven, ciOptionsBS27_WorldAlarmAssetIsland)
						piPart = INT_TO_PARTICIPANTINDEX(EventData.iPart)					
						IF NETWORK_IS_PARTICIPANT_ACTIVE(piPart)
							PRINTLN("[PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_HackingFail] - Position being cached for Island Alarms")
							piPlayer = NETWORK_GET_PLAYER_INDEX(piPart)
							pedPlayer = GET_PLAYER_PED(piPlayer)
							vWorldAlarmPos_1 = GET_ENTITY_COORDS(pedPlayer, FALSE)
						ELSE
							PRINTLN("[PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_HackingFail] - Player is not active. Cannot Cache position.")
						ENDIF
					ENDIF
				BREAK
				
				CASE g_ciInstancedcontentEventType_ShareGlassCutProgress
					sGlassCuttingData.fGlassCuttingProgress = TO_FLOAT(EventData.iGenericInt)
					sGlassCuttingData.niPlacedGlassCutter = EventData.niNetworkIndex
					PRINTLN("[PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_ShareGlassCutProgress] - Updating progress to ", sGlassCuttingData.fGlassCuttingProgress, " and caching cutter net ID!")
				BREAK
				
				CASE g_ciInstancedcontentEventType_ShareInterrogationProgress
					PRINTLN("[PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_ShareInterrogationProgress] - Updating progress")
					SET_INTERROGATION_PROGRESS(sInterrogationVars.iInterrogationSubAnimProgress, FALSE)
				BREAK
				
				CASE g_ciInstancedcontentEventType_HideProp
					PRINTLN("[PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_HideProp] - Received event to hide prop ", EventData.iGenericInt)
					HIDE_PROP(EventData.iGenericInt, FALSE)
				BREAK
				
				CASE g_ciInstancedcontentEventType_ShareSafeCombinationProgress
				
					IF EventData.Details.FromPlayerIndex = LocalPlayer
						// No need to update - I'm the one playing
						EXIT
					ENDIF
					
					IF sEnterSafeCombinationData.iCurrentlySelectedDisplayPanel != EventData.iGenericInt
					OR EventData.bToggle // bToggle here means a remote player is initialising the minigame
						sEnterSafeCombinationData.iCurrentlySelectedDisplayPanel = EventData.iGenericInt
						PRINTLN("[PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_ShareSafeCombinationProgress][SafeCombination] - Updating iCurrentlySelectedDisplayPanel to ", sEnterSafeCombinationData.iCurrentlySelectedDisplayPanel)
						SET_ENTER_SAFE_COMBINATION_MINIGAME_DISPLAY_STATE(ENTER_SAFE_COMBINATION_DISPLAY_PANEL_STATE__NORMAL)
						SET_ENTER_SAFE_COMBINATION_MINIGAME_SELECTED_DISPLAY()
					ENDIF
					
					IF sEnterSafeCombinationData.sEnterSafePanels[sEnterSafeCombinationData.iCurrentlySelectedDisplayPanel].fCurrentSafeValue != EventData.fGenericFloat
					OR EventData.bToggle // bToggle here means a remote player is initialising the minigame
						sEnterSafeCombinationData.sEnterSafePanels[sEnterSafeCombinationData.iCurrentlySelectedDisplayPanel].fCurrentSafeValue = EventData.fGenericFloat
						PRINTLN("[PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_ShareSafeCombinationProgress][SafeCombination] - Updating sEnterSafeCombinationData.sEnterSafePanels[", sEnterSafeCombinationData.iCurrentlySelectedDisplayPanel, "].fCurrentSafeValue to ", sEnterSafeCombinationData.sEnterSafePanels[sEnterSafeCombinationData.iCurrentlySelectedDisplayPanel].fCurrentSafeValue)
						SET_ENTER_SAFE_COMBINATION_MINIGAME_DISPLAY_VALUE(sEnterSafeCombinationData.iCurrentlySelectedDisplayPanel, ROUND(sEnterSafeCombinationData.sEnterSafePanels[sEnterSafeCombinationData.iCurrentlySelectedDisplayPanel].fCurrentSafeValue))
					ENDIF
				BREAK
				
				CASE g_ciInstancedcontentEventType_SafeCombinationSuccess
					
					IF EventData.Details.FromPlayerIndex = LocalPlayer
						// No need to update - I'm the one playing
						EXIT
					ENDIF
					
					SET_ENTER_SAFE_COMBINATION_MINIGAME_DISPLAY_STATE(ENTER_SAFE_COMBINATION_DISPLAY_PANEL_STATE__OPEN)
					PRINTLN("[PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_SafeCombinationSuccess][SafeCombination] - Updating Safe Combination progress || Remote player succeeded!")
				BREAK
				
				CASE g_ciInstancedcontentEventType_SafeCombinationFailed
				
					IF EventData.Details.FromPlayerIndex = LocalPlayer
						// No need to update - I'm the one playing
						EXIT
					ENDIF
					
					RESET_ENTER_SAFE_COMBINATION_MINIGAME()
					SET_ENTER_SAFE_COMBINATION_MINIGAME_DISPLAY_STATE(ENTER_SAFE_COMBINATION_DISPLAY_PANEL_STATE__ERROR)
					PRINTLN("[PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_SafeCombinationFailed][SafeCombination] - Updating Safe Combination progress || Remote player entered the wrong code!")
				BREAK
				
				CASE g_ciInstancedcontentEventType_IslandHeistTelemetry_SupportCrewUsed
					IF !bIsLocalPlayerHost
						EXIT
					ENDIF
					PRINTLN("[PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_IslandHeistTelemetry_SupportCrewUsed] - Received used support crew event: ", EventData.iGenericInt)
					SET_BIT(MC_serverBD_1.sMissionContinuityVars.iTelemetryInts[ciContinuityTelemetryInts_IslandHeist_SupportCrewBS], EventData.iGenericInt)
				BREAK
				
				CASE g_ciInstancedcontentEventType_IslandHeistTelemetry_InterestItemUsed
					IF !bIsLocalPlayerHost
						EXIT
					ENDIF
					PRINTLN("[PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_IslandHeistTelemetry_InterestItemUsed] - Received used interest item event: ", EventData.iGenericInt)
					SET_BIT(MC_serverBD_1.sMissionContinuityVars.iTelemetryInts[ciContinuityTelemetryInts_IslandHeist_InterestItemBS], EventData.iGenericInt)
				BREAK
				
				CASE g_ciInstancedcontentEventType_IslandHeistTelemetry_SecLootGrabbed
					IF !bIsLocalPlayerHost
						EXIT
					ENDIF
					PRINTLN("[PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_IslandHeistTelemetry_SecLootGrabbed] - Received loot grabbed event: ", EventData.iGenericInt)
					SET_BIT(MC_serverBD_1.sMissionContinuityVars.iTelemetryInts[ciContinuityTelemetryInts_IslandHeist_SecLootBS], EventData.iGenericInt)
				BREAK
				
				CASE g_ciInstancedcontentEventType_PlayerAbilities_StartDeploymentHeavyLoadoutContainer
					IF !bIsLocalPlayerHost
						EXIT
					ENDIF
						
					PRINTLN("[PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_PlayerAbilities_StartDeploymentHeavyLoadoutContainer] - Received start deploy heavy loadout container event: ", EventData.iGenericInt)	
					
					// If local player is not using the ability
					IF sLocalPlayerAbilities.sAbilities[PA_HEAVY_LOADOUT].eState != PAS_ACTIVE
						SET_BIT(sLocalPlayerAbilities.sHeavyLoadoutData.iBS_HeavyLoadoutBitset, ciHeavyLoadoutBS_ProcessingContainerDeployment)
						// set the player coords of the broadcaster (ability user)
						piPlayer = PLAYER_ID()
						piPart = INT_TO_PARTICIPANTINDEX(EventData.iPart)
						IF piPart != PARTICIPANT_ID()							
							piPlayer = NETWORK_GET_PLAYER_INDEX(piPart)
						ENDIF
						sLocalPlayerAbilities.sHeavyLoadoutData.vPlayerCoords = GET_PLAYER_COORDS(piPlayer)
						sLocalPlayerAbilities.sHeavyLoadoutData.vPlayerForward = GET_ENTITY_FORWARD_VECTOR(pedPlayer)
						
					ENDIF
				BREAK
				
				CASE g_ciInstancedcontentEventType_PlayerAbilities_FinishedDeploymentHeavyLoadoutContainer
					PRINTLN("[PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_PlayerAbilities_FinishedDeploymentHeavyLoadoutContainer] - Received finish deploy heavy loadout container event: ", EventData.iGenericInt)	
					// IF LOCAL PLAYER HAS HEAVY LOADOUT ABILITY ACTIVE
					IF sLocalPlayerAbilities.sAbilities[PA_HEAVY_LOADOUT].eState = PAS_ACTIVE
						// SET WAITING FOR DEPLOYMENT TO FALSE
						CLEAR_BIT(sLocalPlayerAbilities.sHeavyLoadoutData.iBS_HeavyLoadoutBitset, ciHeavyLoadoutBS_WaitingContainerDeployment)
					ENDIF
				BREAK
				
				CASE g_ciInstancedcontentEventType_PlayerAbilities_ActivatedVehicleRepair
					
					sLocalPlayerAbilities.sVehicleRepairData.vVehicleRepairCoords.x = EventData.fGenericVecX
					sLocalPlayerAbilities.sVehicleRepairData.vVehicleRepairCoords.y = EventData.fGenericVecY
					sLocalPlayerAbilities.sVehicleRepairData.vVehicleRepairCoords.z = EventData.fGenericVecZ
					sLocalPlayerAbilities.sVehicleRepairData.fVehicleRepairHeading = EventData.fGenericFloat
					PRINTLN("[PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_PlayerAbilities_ActivatedVehicleRepair][PlayerAbilities][RepairAbility] - vVehicleRepairCoords: ", sLocalPlayerAbilities.sVehicleRepairData.vVehicleRepairCoords, " / fVehicleRepairHeading: ", sLocalPlayerAbilities.sVehicleRepairData.fVehicleRepairHeading)
					
					PRINTLN("[PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_PlayerAbilities_ActivatedVehicleRepair][PlayerAbilities][RepairAbility] - iMechanicPedIndex: ", g_FMMC_STRUCT.sPlayerAbilities.sVehicleRepair.iMechanicPedIndex, " / iDynopropIndex: ", g_FMMC_STRUCT.sPlayerAbilities.sVehicleRepair.iDynopropIndex, " / iRepairZoneIndex: ", g_FMMC_STRUCT.sPlayerAbilities.sVehicleRepair.iRepairZoneIndex)
					FMMC_SET_LONG_BIT(iDroppedPedBS, g_FMMC_STRUCT.sPlayerAbilities.sVehicleRepair.iMechanicPedIndex)
					
					FMMC_SET_LONG_BIT(iDroppedDynoPropBS, g_FMMC_STRUCT.sPlayerAbilities.sVehicleRepair.iDynopropIndex)
					vDroppedDynoPropLocation[g_FMMC_STRUCT.sPlayerAbilities.sVehicleRepair.iDynopropIndex] = sLocalPlayerAbilities.sVehicleRepairData.vVehicleRepairCoords + <<0,0,2.0>>
					
					ADD_NAVMESH_BLOCKING_OBJECT(sLocalPlayerAbilities.sVehicleRepairData.vVehicleRepairCoords, (<<17, 12, 15>>), DEG_TO_RAD(sLocalPlayerAbilities.sVehicleRepairData.fVehicleRepairHeading), DEFAULT, BLOCKING_OBJECT_ALLPATHS)	
					
					SET_BIT(iZonesWaitingForCreationBS, g_FMMC_STRUCT.sPlayerAbilities.sVehicleRepair.iRepairZoneIndex)
					
				BREAK
				
				CASE g_ciInstancedcontentEventType_PlayerAbilities_ActivatedAmbush
					sLocalPlayerAbilities.sAmbushData.iWarpPositionToUse = EventData.iGenericInt
					PRINTLN("[g_ciInstancedcontentEventType_PlayerAbilities_ActivatedAmbush][PlayerAbilities][AmbushAbility] - Setting iWarpPositionToUse to ", sLocalPlayerAbilities.sAmbushData.iWarpPositionToUse)
				BREAK
				
				CASE g_ciInstancedcontentEventType_PlayerAbilities_CleanUpAmbush
					PRINTLN("[g_ciInstancedcontentEventType_PlayerAbilities_CleanUpAmbush][PlayerAbilities][AmbushAbility] - Received Ambush cleanup event!")
					CLEAN_UP_AMBUSH_PLAYER_ABILITY()
				BREAK
				
				CASE g_ciInstancedcontentEventType_PlayerAbilities_ActivatedDistraction
					IF bIsLocalPlayerHost
						REINIT_NET_TIMER(MC_serverBD_1.stCopDecoyActiveTimer)
					ENDIF
					
					PRINTLN("[g_ciInstancedcontentEventType_PlayerAbilities_ActivatedDistraction][PlayerAbilities][DistractionAbility] - Host starting stCopDecoyActiveTimer")
				BREAK
				
				CASE g_ciInstancedcontentEventType_RunPedBodySpookedAndCancelTasksChecks
					PRINTLN("[g_ciInstancedcontentEventType_RunPedBodySpookedAndCancelTasksChecks][PlayerAbilities] - Setting iPart: ", EventData.iPart, " iPed: ", EventData.iPed)
					FMMC_SET_LONG_BIT(iPedForceSpookCancelTaskChecks, EventData.iPed)
				BREAK
				
				CASE g_ciInstancedcontentEventType_PedDroppedPickupHostEvent
					
					PRINTLN("[g_ciInstancedcontentEventType_PedDroppedPickupHostEvent] - iPart: ", EventData.iPart, " has had iPed: ", EventData.iPed, " drop their pickup index: ", EventData.iGenericInt)
					
					IF NOT FMMC_IS_LONG_BIT_SET(iDroppedPickupBS, EventData.iGenericInt)
						FMMC_SET_LONG_BIT(iDroppedPickupBS, EventData.iGenericInt)
						vDroppedPickupLocation[EventData.iGenericInt].x = EventData.fGenericVecX
						vDroppedPickupLocation[EventData.iGenericInt].y = EventData.fGenericVecY
						vDroppedPickupLocation[EventData.iGenericInt].z = EventData.fGenericVecZ
					ELSE
						PRINTLN("[g_ciInstancedcontentEventType_PedDroppedPickupHostEvent] - iPart: ", EventData.iPart, " has called for iPed: ", EventData.iPed, " to drop their pickup index: ", EventData.iGenericInt, " but IT ALREADY HAS DROPPED!")
					ENDIF
					
					FOR iPed = 0 TO MC_serverBD.iNumPedCreated - 1 
						IF EventData.iGenericInt = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iDropPickupIndex
							PRINTLN("[g_ciInstancedcontentEventType_PedDroppedPickupHostEvent] - iPed: ", iPed, " is meant to drop the same pickup, so flagging as no longer forced.")
							FMMC_CLEAR_LONG_BIT(iPedForceDropPickup, EventData.iPed)
						ENDIF
					ENDFOR
				BREAK
				
				CASE g_ciInstancedcontentEventType_ClearForcePedDropPickupIndexChance
					PRINTLN("[g_ciInstancedcontentEventType_ClearForcePedDropPickupIndexChance] - iPart: ", EventData.iPart, " has had iPed: ", EventData.iPed, " drop their pickup index: ", EventData.iGenericInt)
					
					FOR iPed = 0 TO MC_serverBD.iNumPedCreated - 1 
						IF EventData.iGenericInt = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iDropPickupIndex
							PRINTLN("[g_ciInstancedcontentEventType_ClearForcePedDropPickupIndexChance] - iPed: ", iPed, " is meant to drop the same pickup, so flagging as no longer forced.")
							FMMC_CLEAR_LONG_BIT(iPedForceDropPickup, EventData.iPed)
						ENDIF
					ENDFOR
				BREAK	
				CASE g_ciInstancedcontentEventType_ToggleVehicleInvincibleByFlee
					IF bIsLocalPlayerHost
						IF EventData.bToggle
							SET_BIT(MC_serverBD_4.iVehMadeInvincibleBS, EventData.iVeh)
							PRINTLN("[PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_ToggleVehicleInvincibleByFlee] - Vehicle ", EventData.iVeh, " is now invincible")
						ELSE
							CLEAR_BIT(MC_serverBD_4.iVehMadeInvincibleBS, EventData.iVeh)
							PRINTLN("[PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_ToggleVehicleInvincibleByFlee] - Vehicle ", EventData.iVeh, " is no longer invincible")
						ENDIF
					ENDIF
				BREAK
				CASE g_ciInstancedcontentEventType_ObjectReturnedToSafePosition
					VECTOR vSafePoint
					vSafePoint = <<EventData.fGenericVecX, EventData.fGenericVecY, EventData.fGenericVecZ>>
					PRINTLN("[PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_ReturnObjectToSafePosition][ObjectSafePoint][Objects][Object ", EventData.iObj, "] - Object ", EventData.iObj, " was returned to its last safe position (", vSafePoint, ") by someone else just now! Dealing with our local data accordingly and starting the cooldown timer")
					REINIT_NET_TIMER(stObjectReturnToSafeDropPointCooldownTimer[EventData.iObj])
					vObjectLastSafeDropPoint[EventData.iObj][0] = vSafePoint
					vObjectLastSafeDropPoint[EventData.iObj][1] = vSafePoint
					CLEAR_BIT(iObjectInDangerousPositionBS, EventData.iObj)
					CLEAR_BIT(iObjOnGroundOutOfBoundsBS, EventData.iObj)
					CLEAR_BIT(iObjNeedsToCheckOutOfBoundsBS, EventData.iObj)
				BREAK
				CASE g_ciInstancedcontentEventType_RequestPedIsSetToCower
					
					PRINTLN("[PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_RequestPedIsSetToCower][iPed ", EventData.iPed, "] - iPart: ", EventData.iPart, " Has requested the ped to COWER. (This Event should only be used by special systems)")
					
					IF bIsLocalPlayerHost
						SET_PED_STATE(EventData.iPed, ciTASK_COWER)
					ENDIF
				BREAK
				CASE g_ciInstancedContentEventType_VehicleInVehicle_StateChange
					iVehicleInVehicleState = EventData.iGenericInt
					RESET_NET_TIMER(tdVehicleExitingVehicleStateTimer)
					
					SWITCH iVehicleInVehicleState 
						CASE ciVEHICLE_IN_VEHICLE_STATE__WAITING_TO_CLOSE_DOOR
						CASE ciVEHICLE_IN_VEHICLE_STATE__EMERGE_ENDING
							SET_PROPERTIES_ON_ALL_CONTROLLED_PEDS_IN_VEHICLE(GET_FMMC_ENTITY_VEHICLE(EventData.iVeh), FALSE, TRUE)
						BREAK
					ENDSWITCH
					#IF IS_DEBUG_BUILD
					PRINTLN("[PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedContentEventType_VehicleInVehicle_StateChange][VehicleInVehicle][Vehicle ", EventData.iVeh, "] - Changing iVehicleInVehicleState to ", GET_VEHICLE_IN_VEHICLE_EMERGE_STATE_NAME(iVehicleInVehicleState))
					#ENDIF
				BREAK
				CASE g_ciInstancedcontentEventType_ResetLocalSpawnGroupCachedData
					IF bIsLocalPlayerHost						
						SET_BIT(MC_serverBD_4.rsgSpawnSeed.iMiscBS, ciRandomSpawnGroupBS_RequestResetPending)
						PRINTLN("[PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_ResetLocalSpawnGroupCachedData][SpawnGroups] - I am  the host - Setting ciRandomSpawnGroupBS_RequestResetPending")
					ELSE
						PRINTLN("[PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_ResetLocalSpawnGroupCachedData][SpawnGroups] - I am not host, but they have been told to reset spawn group cached values!")
					ENDIF
				BREAK
				
				CASE g_ciInstancedcontentEventType_CompletePreReq
					IF bIsLocalPlayerHost
						SET_BIT(MC_ServerBD.iServerBitSet8, SBBOOL8_PREREQ_CHANGE_TRIGGERED_AGGRO_INDEX_PREREQS_CHECK)	
						CHECK_ALL_AGGRO_INDEX_PREREQUISITES()
					ENDIF
					
					SET_PREREQUISITE_COMPLETED(EventData.iGenericInt, FALSE)
				BREAK
				CASE g_ciInstancedcontentEventType_ResetPreReq
					IF bIsLocalPlayerHost
						SET_BIT(MC_ServerBD.iServerBitSet8, SBBOOL8_PREREQ_CHANGE_TRIGGERED_AGGRO_INDEX_PREREQS_CHECK)
						CHECK_ALL_AGGRO_INDEX_PREREQUISITES()
					ENDIF
					
					RESET_PREREQUISITE_COMPLETED(EventData.iGenericInt, FALSE)
				BREAK
				CASE g_ciInstancedcontentEventType_HostCheckAggroPrereqs
					IF bIsLocalPlayerHost
						SET_BIT(MC_ServerBD.iServerBitSet8, SBBOOL8_ZONE_TRIGGERED_AGGRO_INDEX_PREREQS_CHECK)
						CHECK_ALL_AGGRO_INDEX_PREREQUISITES()
					ENDIF
				BREAK
				
				CASE g_ciInstancedcontentEventType_PlayerBagFull
					PRINTLN("[PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_PlayerBagFull][SpawnGroups] - Received a 'bag full' event")
					
					IF EventData.Details.FromPlayerIndex = LocalPlayer
						PRINT_TICKER("FMMC_BAG_L_FL")
					ELSE
						PRINT_TICKER_WITH_PLAYER_NAME("FMMC_BAG_R_FL", EventData.Details.FromPlayerIndex)
					ENDIF
				BREAK
				
				CASE g_ciInstancedcontentEventType_Override_PMC
					PRINTLN("[PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_Override_PMC] - Overriding the PMC Cutscene to: ", EventData.iGenericInt)
					SET_POST_MISSION_CUTSCENE_ID_FROM_MISSION_DATA(0, EventData.iGenericInt) 
					SET_POST_MISSION_CUTSCENE_ID_FROM_MISSION_DATA(1, EventData.iGenericInt) 
					SET_POST_MISSION_CUTSCENE_ID_FROM_MISSION_DATA(2, EventData.iGenericInt) 
					SET_POST_MISSION_CUTSCENE_ID_FROM_MISSION_DATA(3, EventData.iGenericInt) 
				BREAK
				
				CASE g_ciInstancedcontentEventType_RequestVehicleCanMigrate					
					IF EventData.iVeh > -1
						NETWORK_INDEX niVeh
						niVeh = MC_serverBD_1.sFMMC_SBD.niVehicle[EventData.iVeh]
						IF NETWORK_DOES_NETWORK_ID_EXIST(niVeh)
							IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(niVeh)
								PRINTLN("[PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_RequestVehicleCanMigrate] - Vehicle: ", EventData.iVeh, " has player starting in it, releasing migration rights.")
								SET_NETWORK_ID_CAN_MIGRATE(niVeh, TRUE)
							ELSE
								PRINTLN("[PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_RequestVehicleCanMigrate] - Vehicle: ", EventData.iVeh, " We don't have control of vehicle anymore...")
							ENDIF
						ELSE
							PRINTLN("[PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_RequestVehicleCanMigrate] - Vehicle: ", EventData.iVeh, " Does not exist...")
						ENDIF
					ENDIF
				BREAK
				
				CASE g_ciInstancedcontentEventType_ToggleGotoInvincible
					IF EventData.iPed > -1
						IF EventData.bToggle
							PRINTLN("[PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_ToggleGotoInvincible] - Ped: ", EventData.iPed, " Setting data from goto that ped is invincible.")
							FMMC_SET_LONG_BIT(iPedGotoAssInvincibilityBitset, EventData.iPed)
						ELSE
							PRINTLN("[PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_ToggleGotoInvincible] - Ped: ", EventData.iPed, " Clearing data from goto that ped is invincible.")
							FMMC_CLEAR_LONG_BIT(iPedGotoAssInvincibilityBitset, EventData.iPed)
						ENDIF
					ENDIF
				BREAK
				
				CASE g_ciInstancedcontentEventType_ChopSatInVehicle
					PRINTLN("[PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_ChopSatInVehicle] - iPed: ", EventData.iPed)
					
					pedPlaced = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[EventData.iPed])
					
					IF IS_PED_INJURED(pedPlaced)
						PRINTLN("[PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_ChopSatInVehicle] - Companion dead or does not exist.")
						EXIT
					ENDIF
					
					iCompanionIndex = GET_PED_COMPANION_INDEX(EventData.iPed)	
					IF iCompanionIndex = -1
						PRINTLN("[PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_ChopSatInVehicle] - iCompanionIndex = -1")
						EXIT
					ENDIF
					
					vehIndex = NET_TO_VEH(MC_playerBD_1[EventData.iPart].sPedCompanionTaskData[iCompanionIndex].targetEntityFriendVehicle)
					
					IF NOT IS_VEHICLE_DRIVEABLE(vehIndex)
						PRINTLN("[PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_ChopSatInVehicle] - Vehicle not driveable")
						vehIndex = NET_TO_VEH(MC_playerBD_1[EventData.iPart].sPedCompanionTaskData[iCompanionIndex].targetEntityFriendVehicleCached)
							
						IF NOT IS_VEHICLE_DRIVEABLE(vehIndex)
							PRINTLN("[PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_ChopSatInVehicle] - Cached Vehicle not driveable")						
							EXIT
						ENDIF
					ENDIF
					
					SET_PED_VEHICLE_FORCED_SEAT_USAGE(LocalPlayerPed, vehIndex, 0, ENUM_TO_INT(VC_FORCE_USE_FRONT_SEATS))
					
				BREAK
				
				CASE g_ciInstancedcontentEventType_ChopLeftVehicle
					PRINTLN("[Peds][Ped ", EventData.iPed, "][Task][CLIENT][Companion] - g_ciInstancedcontentEventType_ChopLeftVehicle - Clearing Forced Seat Usage")										
					CLEAR_ALL_PED_VEHICLE_FORCED_SEAT_USAGE(LocalPlayerPed)
				BREAK
				
				CASE g_ciInstancedcontentEventType_ChopToggleVehicleDoor
					PRINTLN("[Peds][Ped ", EventData.iPed, "][Task][CLIENT][Companion] - g_ciInstancedcontentEventType_ChopToggleVehicleDoor - Calling event.")
										
					pedPlaced = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[EventData.iPed])
					
					IF IS_PED_INJURED(pedPlaced)
						PRINTLN("[Peds][Ped ", EventData.iPed, "][Task][CLIENT][Companion] - g_ciInstancedcontentEventType_ChopToggleVehicleDoor - Companion dead or does not exist.")
						EXIT
					ENDIF
					
					iCompanionIndex = GET_PED_COMPANION_INDEX(EventData.iPed)	
					IF iCompanionIndex = -1
						PRINTLN("[Peds][Ped ", EventData.iPed, "][Task][CLIENT][Companion] - g_ciInstancedcontentEventType_ChopToggleVehicleDoor - iCompanionIndex = -1")
						EXIT
					ENDIF
					
					vehIndex = NET_TO_VEH(MC_playerBD_1[EventData.iPart].sPedCompanionTaskData[iCompanionIndex].targetEntityFriendVehicle)
					
					IF NOT IS_VEHICLE_DRIVEABLE(vehIndex)
						PRINTLN("[Peds][Ped ", EventData.iPed, "][Task][CLIENT][Companion] - g_ciInstancedcontentEventType_ChopToggleVehicleDoor - Vehicle not driveable")
						vehIndex = NET_TO_VEH(MC_playerBD_1[EventData.iPart].sPedCompanionTaskData[iCompanionIndex].targetEntityFriendVehicleCached)
						
						IF NOT IS_VEHICLE_DRIVEABLE(vehIndex)
							PRINTLN("[Peds][Ped ", EventData.iPed, "][Task][CLIENT][Companion] - g_ciInstancedcontentEventType_ChopToggleVehicleDoor - Cached Vehicle not driveable")						
							EXIT
						ENDIF
					ENDIF

					IF NETWORK_HAS_CONTROL_OF_ENTITY(vehIndex)
						SC_DOOR_LIST scDoor
						scDoor = GET_SUITABLE_DOOR_TYPE_FOR_CHOP(pedPlaced, vehIndex)
						
						IF NOT IS_VEHICLE_DOOR_DAMAGED(vehIndex, scDoor)							
							IF EventData.bToggle
								PRINTLN("[Peds][Ped ", EventData.iPed, "][Task][CLIENT][Companion] - g_ciInstancedcontentEventType_ChopToggleVehicleDoor - Open Door")
								SET_VEHICLE_DOOR_OPEN(vehIndex, scDoor)								
							ELSE
								PRINTLN("[Peds][Ped ", EventData.iPed, "][Task][CLIENT][Companion] - g_ciInstancedcontentEventType_ChopToggleVehicleDoor - Close Door")
								SET_VEHICLE_DOOR_SHUT(vehIndex, scDoor)
							ENDIF
						ENDIF
					ENDIF
				BREAK
				CASE g_ciInstancedcontentEventType_CompanionPassOwnershipToTarget
					pedPlaced = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[EventData.iPed])
					
					IF IS_PED_INJURED(pedPlaced)
						PRINTLN("[Peds][Ped ", EventData.iPed, "][Task][CLIENT][Companion] - g_ciInstancedcontentEventType_CompanionPassOwnershipToTarget - Companion dead or does not exist.")
						EXIT
					ENDIF
					
					iCompanionIndex = GET_PED_COMPANION_INDEX(EventData.iPed)	
					IF iCompanionIndex = -1
						PRINTLN("[Peds][Ped ", EventData.iPed, "][Task][CLIENT][Companion] - g_ciInstancedcontentEventType_CompanionPassOwnershipToTarget - iCompanionIndex = -1")
						EXIT
					ENDIF
					
					IF MC_playerBD_1[EventData.iPart].sPedCompanionTaskData[iCompanionIndex].targetEntityFriend != LocalPlayer
						EXIT
					ENDIF
					
					PRINTLN("[Peds][Ped ", EventData.iPed, "][Task][CLIENT][Companion] - g_ciInstancedcontentEventType_CompanionPassOwnershipToTarget - Requesting Control")
					
					NETWORK_REQUEST_CONTROL_OF_ENTITY(pedPlaced)
					
					MC_playerBD_1[iLocalPart].sPedCompanionTaskData[iCompanionIndex] = MC_playerBD_1[EventData.iPart].sPedCompanionTaskData[iCompanionIndex]
				BREAK
				
				CASE g_ciInstancedcontentEventType_SwapDoorWithDynoprop
					PRINTLN("[RCC MISSION][Doors][Door ", EventData.iGenericInt, "][DoorDynoprops] g_ciInstancedcontentEventType_SwapDoorWithDynoprop | Received Door/Dynoprop swap event! EventData.bToggle/bMakeDynopropActive: ", EventData.bToggle)
					PERFORM_LOCAL_DOOR_ANIMATION_DYNOPROP_SWAP(EventData.iGenericInt, EventData.bToggle)
				BREAK
				
				CASE g_ciInstancedcontentEventType_MarkPedAsInvolvedInInteractWith
					IF EventData.bToggle
						IF NOT FMMC_IS_LONG_BIT_SET(iPedInvolvedInInteractWithBS, EventData.iPed)
							FMMC_SET_LONG_BIT(iPedInvolvedInInteractWithBS, EventData.iPed)
							PRINTLN("[Peds][Ped ", EventData.iPed, "][InteractWith_LinkedPeds][InteractWith_NearbyPeds] g_ciInstancedcontentEventType_MarkPedAsInvolvedInInteractWith | Ped is now involved in an Interact-With!")
						ENDIF
					ELSE
						IF FMMC_IS_LONG_BIT_SET(iPedInvolvedInInteractWithBS, EventData.iPed)
							FMMC_CLEAR_LONG_BIT(iPedInvolvedInInteractWithBS, EventData.iPed)
							PRINTLN("[Peds][Ped ", EventData.iPed, "][InteractWith_LinkedPeds][InteractWith_NearbyPeds] g_ciInstancedcontentEventType_MarkPedAsInvolvedInInteractWith | Ped is no longer involved in an Interact-With!")
						ENDIF
					ENDIF
				BREAK
				
				CASE g_ciInstancedContentEventType_MarkPedAsKilled
					IF !bIsLocalPlayerHost
						EXIT
					ENDIF
					FMMC_SET_LONG_BIT(MC_serverBD_4.iPedKilledBS, EventData.iPed)
					PRINTLN("[PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedContentEventType_MarkPedAsKilled] - Marking ped as killed: ", EventData.iPed, ". FMMC_SET_LONG_BIT(MC_serverBD_4.iPedKilledBS, EventData.iPed)")
				BREAK
				
				CASE g_ciInstancedContentEventType_FlagPedToCleanup
					PRINTLN("[PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedContentEventType_FlagPedToCleanup] - Setting ped ", EventData.iPed, " to clean up via iPedShouldBeForcedToCleanup")
					FMMC_SET_LONG_BIT(iPedShouldBeForcedToCleanup, EventData.iPed)
				BREAK
				
				CASE g_ciInstancedcontentEventType_SyncScene_AssignObjectIndex
					IF NETWORK_DOES_NETWORK_ID_EXIST(EventData.niNetworkIndex)
						PRINTLN("[PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_SyncScene_AssignObjectIndex] - Assigning sync scene objects for ped ", EventData.iPed)
						sMissionPedsLocalVars[EventData.iPed].niAnimObjs[ciMissionPedSyncSceneObjects_DJ_Bag] = EventData.niNetworkIndex
						sMissionPedsLocalVars[EventData.iPed].oiAnimObjs[ciMissionPedSyncSceneObjects_DJ_Bag] = NET_TO_OBJ(EventData.niNetworkIndex)
					ELSE
						PRINTLN("[PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_SyncScene_AssignObjectIndex] - Object didn't exist for assignment for sync scene ped ", EventData.iPed)
						#IF IS_DEBUG_BUILD
						ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_IGNORE, ENTITY_RUNTIME_ERROR_TYPE_WARNING_PED, "Object didn't exist for assignment for sync scene!", EventData.iPed)
						#ENDIF
					ENDIF
				BREAK
				CASE g_ciInstancedcontentEventType_SyncScene_AssignAnimStage
					IF iLocalPart != EventData.iPart
						PRINTLN("[PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_SyncScene_AssignAnimStage] - Assigning Anim Stage: ", EventData.iGenericInt, " for Ped: ", EventData.iPed)
						sMissionPedsLocalVars[EventData.iPed].iRandomAnimLastPlayed = EventData.iGenericInt
						SET_BIT(sMissionPedsLocalVars[EventData.iPed].iPedBS, ci_MissionPedBS_SyncScene_ProgressedAnimStageThisRule)
					ENDIF
				BREAK
				CASE g_ciInstancedcontentEventType_TriggerMusicEvent_DreSpecial
					PRINTLN("[PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_TriggerMusicEvent_DreSpecial] - Assigning Anim Stage: ", EventData.iGenericInt, " for Ped: ", EventData.iPed)
					IF NOT IS_BIT_SET(sMissionPedsLocalVars[EventData.iPed].iPedBS, ci_MissionPedBS_SyncScene_TriggeredMusicEvent)
						SET_BIT(sMissionPedsLocalVars[EventData.iPed].iPedBS, ci_MissionPedBS_SyncScene_TriggeredMusicEvent)
						TRIGGER_MUSIC_EVENT("FITB_SHOOTOUT_TRACK_START")
					ENDIF
					SET_BIT(sMissionPedsLocalVars[EventData.iPed].iPedBS, ci_MissionPedBS_iSyncScenePlayedIntroDialogue)
					SET_BIT(iLocalBoolCheck25, LBOOL25_MUSIC_STARTED_DRE_SHOOTOUT)
				BREAK
				CASE g_ciInstancedcontentEventType_TellHostToSkipCutscene
					IF !bIsLocalPlayerHost
						EXIT
					ENDIF
					PRINTLN("[PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_TellHostToSkipCutscene] - Part: ", EventData.iPart, " Triggered cutscene skip")
					SET_BIT(MC_ServerBD.iServerBitSet8, SBBOOL8_SKIP_CUTSCENE_TRIGGERED)
				BREAK
				CASE g_ciInstancedcontentEventType_PedStartsInContainer					
					IF EventData.iPart = iLocalPart
						EXIT
					ENDIF
					PRINTLN("[PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_PedStartsInContainer] - Part: ", EventData.iPart, " Triggered ped creation. iPed: ", EventData.iPed, " flagged ci_MissionPedBS_StartedInContainerVehicleWithInvincibility")
					SET_BIT(sMissionPedsLocalVars[EventData.iPed].iPedBS, ci_MissionPedBS_StartedInContainerVehicleWithInvincibility)
				BREAK
				CASE g_ciInstancedcontentEventType_OverridePedAssGotoProgress
					PRINTLN("[PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_OverridePedAssGotoProgress] - Part: ", EventData.iPart, " Triggered ped creation. iPed: ", EventData.iPed, " iPool: ", EventData.iGenericInt, " iStartingIndex: ", EventData.iLoc, " bInterrupt: ", EventData.bToggle)
					FMMC_SET_LONG_BIT(iPedGotoAssProgressOverrideEventFired, EventData.iPed)										
					iPedGotoAssProgressOverridePool[EventData.iPed] = EventData.iGenericInt
					iPedGotoAssProgressOverrideProgress[EventData.iPed] = EventData.iLoc
					IF EventData.bToggle
						FMMC_SET_LONG_BIT(iPedGotoAssProgressOverrideInterrupt, EventData.iPed)
					ENDIF
				BREAK
				CASE g_ciInstancedcontentEventType_ClearOverridePedAssGotoProgress
					iPedGotoAssProgressOverrideProgress[EventData.iPed] = ASSOCIATED_GOTO_TASK_POOL_START__STAY_ON_CURRENT
					iPedGotoAssProgressOverridePool[EventData.iPed] = ASSOCIATED_GOTO_TASK_POOL_INDEX__OFF
					FMMC_CLEAR_LONG_BIT(iPedGotoAssProgressOverrideInterrupt, EventData.iPed)
					FMMC_SET_LONG_BIT(iPedGotoAssProgressOverrideEventComplete, EventData.iPed)
				BREAK
				CASE g_ciInstancedcontentEventType_SpawnAllTeamMembers
					IF !bIsLocalPlayerHost
						EXIT
					ENDIF
					
					IF EventData.bToggle	
						IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[EventData.iGenericInt].iRuleBitsetFifteen[MC_serverBD_4.iCurrentHighestPriority[EventData.iGenericInt]], ciBS_RULE15_RESPAWN_ALL_PLAYERS_ON_KILL)
							SET_BIT(MC_serverBD.iServerBitSet3, SBBOOL3_RESPAWN_ALL_TEAM_MEMBERS_TEAM_0 + EventData.iGenericInt)
							PRINTLN("[PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_SpawnAllTeamMembers] - Set_BIT(MC_serverBD.iServerBitSet3, SBBOOL3_RESPAWN_ALL_TEAM_MEMBERS_TEAM_)", EventData.iGenericInt)
						ENDIF
					ELSE
						IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[EventData.iGenericInt].iRuleBitsetFifteen[MC_serverBD_4.iCurrentHighestPriority[EventData.iGenericInt]], ciBS_RULE15_RESPAWN_ALL_PLAYERS_ON_KILL)
							CLEAR_BIT(MC_serverBD.iServerBitSet3, SBBOOL3_RESPAWN_ALL_TEAM_MEMBERS_TEAM_0 + EventData.iGenericInt)
							PRINTLN("[PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_SpawnAllTeamMembers] - CLEAR_BIT(MC_serverBD.iServerBitSet3, SBBOOL3_RESPAWN_ALL_TEAM_MEMBERS_TEAM_)", EventData.iGenericInt)
						ENDIF
					ENDIF
				BREAK
				CASE g_ciInstancedcontentEventType_FlashFadeEntityStart
					IF !bIsLocalPlayerHost
						EXIT
					ENDIF
					IF NETWORK_DOES_NETWORK_ID_EXIST(EventData.niNetworkIndex)
						INT i
						//Check if the entity already exists
						FOR i = 0 TO MAX_FLASH_FADE_ENTITIES - 1
							IF MC_serverBD.sFlashFade.niEntitiesDoingFlashFade[i] = EventData.niNetworkIndex
								SET_BIT(MC_serverBD.sFlashFade.iEntityDoingFlashFadeBS, i)
								SET_BIT(MC_serverBD.sFlashFade.iPlayersDoingFlashFadeBS[i], EventData.iPart)
								MC_serverBD.sFlashFade.iFlashFadeEndAlpha[i] = EventData.iGenericInt
								MC_serverBD.sFlashFade.iFlashFadePartEntIndex[EventData.iPart] = i
								EXIT
							ENDIF
						ENDFOR
						
						//If the entity wasn't already added, add the entity to the array
						FOR i = 0 TO MAX_FLASH_FADE_ENTITIES - 1
							IF MC_serverBD.sFlashFade.niEntitiesDoingFlashFade[i] = NULL
								MC_serverBD.sFlashFade.niEntitiesDoingFlashFade[i] = EventData.niNetworkIndex
								MC_serverBD.sFlashFade.iFlashFadeEndAlpha[i] = EventData.iGenericInt
								MC_serverBD.sFlashFade.iNumOfEntsUsingFlashFade++
								MC_serverBD.sFlashFade.iFlashFadePartEntIndex[EventData.iPart] = i
								SET_BIT(MC_serverBD.sFlashFade.iEntityDoingFlashFadeBS, i)
								SET_BIT(MC_serverBD.sFlashFade.iPlayersDoingFlashFadeBS[i], EventData.iPart)
								PRINTLN("[PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_FlashFadeEntityStart][FLASH FADE] - Adding a new entity to flash fade at index ", i)
								PRINTLN("[PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_FlashFadeEntityStart][FLASH FADE] - Begin flash fade")
								EXIT
							ENDIF
						ENDFOR
						ASSERTLN("[PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_FlashFadeEntityStart][FLASH FADE] - Unable to add the entity as the flash fade array is full!")
					ENDIF
				BREAK
				CASE g_ciInstancedcontentEventType_FlashFadeEntityEnd
					IF !bIsLocalPlayerHost
						EXIT
					ENDIF
					IF EventData.iGenericInt > -1 AND EventData.iGenericInt < MAX_FLASH_FADE_ENTITIES
						CLEAR_BIT(MC_serverBD.sFlashFade.iEntityDoingFlashFadeBS, EventData.iGenericInt)
						PRINTLN("[PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_FlashFadeEntityEnd][FLASH FADE] - Clear iEntityDoingFlashFadeBS for the entity")
					ENDIF
				BREAK
				CASE g_ciInstancedcontentEventType_FlashFadeEntityClear
					IF !bIsLocalPlayerHost
						EXIT
					ENDIF
					MC_serverBD.sFlashFade.niEntitiesDoingFlashFade[EventData.iGenericInt] = NULL
					MC_serverBD.sFlashFade.iNumOfEntsUsingFlashFade--
					MC_serverBD.sFlashFade.iFlashFadePartEntIndex[EventData.iPart] = -1
					PRINTLN("[PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_FlashFadeEntityClear][FLASH FADE] - Entity Index = ", EventData.iGenericInt, ", Part = ", EventData.iPart)
				BREAK
				CASE g_ciInstancedcontentEventType_SetPlayerDoingFlashFade
					IF !bIsLocalPlayerHost
						EXIT
					ENDIF
					IF EventData.bToggle
						SET_BIT(MC_serverBD.sFlashFade.iPlayersDoingFlashFadeBS[EventData.iGenericInt], EventData.iPart)					
						PRINTLN("[PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_SetPlayerDoingFlashFade][FLASH FADE] - Set True - Entity Index = ", EventData.iGenericInt, ", Part = ", EventData.iPart)
					ELSE
						CLEAR_BIT(MC_serverBD.sFlashFade.iPlayersDoingFlashFadeBS[EventData.iGenericInt], EventData.iPart)					
						PRINTLN("[PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_SetPlayerDoingFlashFade][FLASH FADE] - Set False - Entity Index = ", EventData.iGenericInt, ", Part = ", EventData.iPart)
					ENDIF
				BREAK
				CASE g_ciInstancedcontentEventType_OverrideDroneAssGotoProgress
					PRINTLN("[FMMC_DRONES ", EventData.iPed, "][g_ciInstancedcontentEventType_OverrideDroneAssGotoProgress] - Part: ", EventData.iPart, " Triggered Drone Override. iDrone: ", EventData.iPed, " iPool: ", EventData.iGenericInt, " iStartingIndex: ", EventData.iLoc, " bInterrupt: ", EventData.bToggle)
					FMMC_SET_LONG_BIT(sLocalSharedAssGotoTaskData.iProgressOverrideEventFired, EventData.iPed)										
					sLocalSharedAssGotoTaskData.iProgressOverridePool[EventData.iPed] = EventData.iGenericInt
					sLocalSharedAssGotoTaskData.iProgressOverrideProgress[EventData.iPed] = EventData.iLoc
					IF EventData.bToggle
						FMMC_SET_LONG_BIT(sLocalSharedAssGotoTaskData.iProgressOverrideInterrupt, EventData.iPed)
					ENDIF
				BREAK				
				CASE g_ciInstancedcontentEventType_ClearOverrideDroneAssGotoProgress
					PRINTLN("[FMMC_DRONES ", EventData.iPed, "][g_ciInstancedcontentEventType_ClearOverrideDroneAssGotoProgress] - Part: ", EventData.iPart, " Triggered Clear Drone Override. iPed: ", EventData.iPed)
				 	sLocalSharedAssGotoTaskData.iProgressOverrideProgress[EventData.iPed] = ASSOCIATED_GOTO_TASK_POOL_START__STAY_ON_CURRENT
					sLocalSharedAssGotoTaskData.iProgressOverridePool[EventData.iPed] = ASSOCIATED_GOTO_TASK_POOL_INDEX__OFF
					FMMC_CLEAR_LONG_BIT(sLocalSharedAssGotoTaskData.iProgressOverrideInterrupt, EventData.iPed)
					FMMC_SET_LONG_BIT(sLocalSharedAssGotoTaskData.iProgressOverrideEventComplete, EventData.iPed)
				BREAK
				CASE g_ciInstancedcontentEventType_DATA_SET_DroneAssGotoProgress_Arrived
					IF EventData.iPart = iLocalPart
						EXIT
					ENDIF
					PRINTLN("[FMMC_DRONES ", EventData.iGenericInt, "][g_ciInstancedcontentEventType_DATA_SET_DroneAssGotoProgress_Arrived] - Part: ", EventData.iPart, " Triggered Drone Arrival. iDrone: ", EventData.iGenericInt)										
					FMMC_SET_LONG_BIT(sLocalSharedAssGotoTaskData.iCompletedArrivedBitset, EventData.iGenericInt)
				BREAK
				CASE g_ciInstancedcontentEventType_DATA_SET_DroneAssGotoProgress_StartGotoData
					IF EventData.iPart = iLocalPart				
						EXIT
					ENDIF
					PRINTLN("[FMMC_DRONES ", EventData.iGenericInt, "][g_ciInstancedcontentEventType_DATA_SET_DroneAssGotoProgress_StartGotoData] - Part: ", EventData.iPart, " Triggered Drone clear data. iDrone: ", EventData.iGenericInt)					
					CLEAR_FMMC_DRONE_ASS_GOTO_DATA(EventData.iGenericInt)	
					SET_FMMC_DRONE_CLIENT_PROGRESS(EventData.iGenericInt)
			
					VECTOR vPos
					ENTITY_INDEX eiEntity
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(EventData.niNetworkIndex)
						eiEntity = NETWORK_GET_ENTITY_FROM_NETWORK_ID(EventData.niNetworkIndex)
						IF DOES_ENTITY_EXIST(eiEntity)
							vPos = GET_ENTITY_COORDS(eiEntity, FALSE)
						ENDIF
					ENDIF
					
					SET_FMMC_DRONE_CLIENT_VECTOR_TARGET(EventData.iGenericInt, (<<EventData.fGenericVecX, EventData.fGenericVecY, EventData.fGenericVecZ>>), vPos)
					FMMC_SET_LONG_BIT(sLocalSharedAssGotoTaskData.iStartedGotoBitset, EventData.iGenericInt)
				BREAK
				CASE g_ciInstancedcontentEventType_DATA_SET_DroneAssGotoProgress_ReassignPos
					PRINTLN("[FMMC_DRONES ", EventData.iGenericInt, "][PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_DATA_SET_DroneAssGotoProgress_ReassignPos] - Part: ", EventData.iPart, " Triggered Drone Reassign data. iDrone: ", EventData.iGenericInt)
					FMMC_CLEAR_LONG_BIT(sLocalSharedAssGotoTaskData.iStartedGotoBitset, EventData.iGenericInt)
					FMMC_SET_LONG_BIT(sLocalSharedAssGotoTaskData.iReassignBitset, EventData.iGenericInt)
				BREAK
				
				CASE g_ciInstancedcontentEventType_DATA_SET_DroneAssGotoProgress_AchieveHeading
					PRINTLN("[FMMC_DRONES ", EventData.iGenericInt, "][g_ciInstancedcontentEventType_DATA_SET_DroneAssGotoProgress_AchieveHeading] Part: ", EventData.iPart, " Completed Drone Achieve Heading. iDrone: ", EventData.iGenericInt)
					FMMC_SET_LONG_BIT(sLocalSharedAssGotoTaskData.iCompletedAchieveHeadingBitset, EventData.iGenericInt)
				BREAK
				CASE g_ciInstancedcontentEventType_DATA_SET_DroneAssGotoProgress_WaitTime
					PRINTLN("[FMMC_DRONES ", EventData.iGenericInt, "][g_ciInstancedcontentEventType_DATA_SET_DroneAssGotoProgress_WaitTime] Part: ", EventData.iPart, " Completed Drone Wait Time. iDrone: ", EventData.iGenericInt)
					FMMC_SET_LONG_BIT(sLocalSharedAssGotoTaskData.iCompletedWaitBitset, EventData.iGenericInt)
				BREAK
				CASE g_ciInstancedcontentEventType_Drone_Cleanup
					PRINTLN("[FMMC_DRONES ", EventData.iGenericInt, "][g_ciInstancedcontentEventType_Drone_Cleanup] Part: ", EventData.iPart, " Cleaning up iDrone: ", EventData.iGenericInt)
					CLEAR_FMMC_DRONE_ASS_GOTO_DATA(EventData.iGenericInt)
					LOCAL_FMMC_DRONE_DATA sDroneEmpty
					COPY_SCRIPT_STRUCT(sLocalFmmcDroneData[EventData.iGenericInt], sDroneEmpty, SIZE_OF(LOCAL_FMMC_DRONE_DATA))
				BREAK
				CASE g_ciInstancedcontentEventType_Drone_DefensivePoint
					PRINTLN("[FMMC_DRONES ", EventData.iGenericInt, "][g_ciInstancedcontentEventType_Drone_DefensivePoint] - Part: ", EventData.iPart, " iDrone: ", EventData.iGenericInt, " Setting a defensive point: ", (<<EventData.fGenericVecX, EventData.fGenericVecY, EventData.fGenericVecZ>>))
					sLocalFmmcDroneData[EventData.iGenericInt].vDefensivePoint = <<EventData.fGenericVecX, EventData.fGenericVecY, EventData.fGenericVecZ>>
				BREAK
				
				CASE g_ciInstancedcontentEventType_PedAnimSecondary_Started
					PRINTLN("[g_ciInstancedcontentEventType_PedAnimSecondary_Started] - Part: ", EventData.iPart, " iPed: ", EventData.iPed, " Secondary Anim Started.")
					SET_TASK_ANIMATION_STARTED_SECONDARY_ANIM(EventData.bToggle, EventData.iPed)
				BREAK
				CASE g_ciInstancedcontentEventType_PedAnimSecondary_Completed
					PRINTLN("[g_ciInstancedcontentEventType_PedAnimSecondary_Completed] - Part: ", EventData.iPart, " iPed: ", EventData.iPed, " Secondary Anim Completed.")
					SET_TASK_ANIMATION_COMPLETED_SECONDARY_ANIM(EventData.bToggle, EventData.iPed)
				BREAK				
				CASE g_ciInstancedcontentEventType_OverrideCutsceneInterpTimeToFinish
					
					IF iCutsceneInterpTimeOverride > -1
						EXIT
					ENDIF
					
					IF iLocalPart = EventData.iPart
						EXIT
					ENDIF
					
					PRINTLN("[g_ciInstancedcontentEventType_OverrideCutsceneInterpTimeToFinish] - Part: ", EventData.iPart, " iCutsceneInterpTimeOverride: ", EventData.iGenericInt, " Overriding.")
					REINIT_NET_TIMER(tdScriptedCutsceneTimer)
					iCutsceneInterpTimeOverride = EventData.iGenericInt					
				BREAK
				CASE g_ciInstancedcontentEventType_PlayTickerOnShot
					
					IF iLocalPart = EventData.iPart
						EXIT
					ENDIF
					
					IF IS_BIT_SET(iLocalBoolCheck35, LBOOL35_PLAYED_CUTSCENE_SHOT_REACHED_TICKER)
						EXIT
					ENDIF
					
					THEFEED_SHOW()
					THEFEED_RESUME()
					THEFEED_FORCE_RENDER_ON()	
					
					SET_BIT(iLocalBoolCheck35, LBOOL35_PLAYED_CUTSCENE_SHOT_REACHED_TICKER)
					
					PRINTLN("[g_ciInstancedcontentEventType_PlayTickerOnShot] - Part: ", EventData.iPart, " EventData.iPed: ", EventData.iPed, " EventData.iVeh ", EventData.iVeh, " EventData.iObj: ", EventData.iObj)
					
					IF EventData.iObj > -1
						TEXT_LABEL_63 tlCustom_Ticker
						tlCustom_Ticker = GET_CUSTOM_STRING_LIST_TEXT_LABEL(EventData.iObj)
						IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[EventData.iVeh].iCutsceneShotBitSet[EventData.iPed], ciCSS_BS_ThisShotCustomStringTickerInsertPlayerName)	
							piPart = INT_TO_PARTICIPANTINDEX(EventData.iPart)
							IF NETWORK_IS_PARTICIPANT_ACTIVE(piPart)
								piPlayer = NETWORK_GET_PLAYER_INDEX(piPart)
								PRINT_TICKER_WITH_PLAYER_NAME(tlCustom_Ticker, piPlayer)
							ELSE
								PRINTLN("[g_ciInstancedcontentEventType_PlayTickerOnShot] - Part: ", EventData.iPart, " is INACTIVE!")
							ENDIF
						ELSE
							PRINT_TICKER(tlCustom_Ticker)
						ENDIF
					ENDIF
				BREAK
				
				CASE g_ciInstancedcontentEventType_SetHalloweenAdversary2022ProximitySounds
					IF EventData.iGenericInt < 0
						EXIT
					ENDIF
					
					//Hunted
					IF MC_playerBD[iLocalPart].iTeam = ciHALLOWEEN_ADVERSARY_HUNTED_TEAM AND MC_playerBD[EventData.iPart].iTeam = ciHALLOWEEN_ADVERSARY_HUNTER_TEAM
						IF EventData.bToggle
							//Add the sound effect
							ENTITY_INDEX eiTarget 
							eiTarget = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(EventData.iPart)))
							IF iEnemyCloseSoundLoop[EventData.iGenericInt] = 0
							AND IS_ENTITY_ALIVE(eiTarget)
								iEnemyCloseSoundLoop[EventData.iGenericInt] = GET_SOUND_ID()
								IF g_FMMC_STRUCT.iRiderFlameEffect[ciHALLOWEEN_ADVERSARY_HUNTER_TEAM][EventData.iGenericInt]   = 1
									PLAY_SOUND_FROM_ENTITY(iEnemyCloseSoundLoop[EventData.iGenericInt], "Rider_War_Loop", eiTarget, "Halloween_Adversary_Sounds")
									PRINTLN("[PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_SetHalloweenAdversary2022ProximitySounds][PROCESS_HALLOWEEN_ADVERSARY_2022_SOUNDS_HUNTED] - PLAY_SOUND_FROM_ENTITY with Sound ID = ", iEnemyCloseSoundLoop[EventData.iGenericInt], " (Rider_War_Loop), Hunter Part = ", EventData.iGenericInt)
								ELIF g_FMMC_STRUCT.iRiderFlameEffect[ciHALLOWEEN_ADVERSARY_HUNTER_TEAM][EventData.iGenericInt] = 2
									PLAY_SOUND_FROM_ENTITY(iEnemyCloseSoundLoop[EventData.iGenericInt], "Rider_Death_Loop", eiTarget, "Halloween_Adversary_Sounds")
									PRINTLN("[PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_SetHalloweenAdversary2022ProximitySounds][PROCESS_HALLOWEEN_ADVERSARY_2022_SOUNDS_HUNTED] - PLAY_SOUND_FROM_ENTITY with Sound ID = ", iEnemyCloseSoundLoop[EventData.iGenericInt], " (Rider_Death_Loop), Hunter Part = ", EventData.iGenericInt)
								ELIF g_FMMC_STRUCT.iRiderFlameEffect[ciHALLOWEEN_ADVERSARY_HUNTER_TEAM][EventData.iGenericInt] = 3
									PLAY_SOUND_FROM_ENTITY(iEnemyCloseSoundLoop[EventData.iGenericInt], "Rider_Pestilence_Loop", eiTarget, "Halloween_Adversary_Sounds")
									PRINTLN("[PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_SetHalloweenAdversary2022ProximitySounds][PROCESS_HALLOWEEN_ADVERSARY_2022_SOUNDS_HUNTED] - PLAY_SOUND_FROM_ENTITY with Sound ID = ", iEnemyCloseSoundLoop[EventData.iGenericInt], " (Rider_Pestilence_Loop), Hunter Part = ", EventData.iGenericInt)
								ELSE
									PLAY_SOUND_FROM_ENTITY(iEnemyCloseSoundLoop[EventData.iGenericInt], "Rider_Famine_Loop", eiTarget, "Halloween_Adversary_Sounds")
									PRINTLN("[PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_SetHalloweenAdversary2022ProximitySounds][PROCESS_HALLOWEEN_ADVERSARY_2022_SOUNDS_HUNTED] - PLAY_SOUND_FROM_ENTITY with Sound ID = ", iEnemyCloseSoundLoop[EventData.iGenericInt], " (Rider_Famine_Loop), Hunter Part = ", EventData.iGenericInt)
								ENDIF
							ENDIF
						ELIF iEnemyCloseSoundLoop[EventData.iGenericInt] > 0
							//Remove the sound effect
							STOP_SOUND(iEnemyCloseSoundLoop[EventData.iGenericInt])
							RELEASE_SOUND_ID(iEnemyCloseSoundLoop[EventData.iGenericInt])
							iEnemyCloseSoundLoop[EventData.iGenericInt] = 0
							PRINTLN("[PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_SetHalloweenAdversary2022ProximitySounds][PROCESS_HALLOWEEN_ADVERSARY_2022_SOUNDS_HUNTED] - STOP_SOUND with Sound ID = ", iEnemyCloseSoundLoop[EventData.iGenericInt], ", Hunter Part = ", EventData.iGenericInt)
						ENDIF
					//Hunter
					ELIF MC_playerBD[iLocalPart].iTeam = ciHALLOWEEN_ADVERSARY_HUNTER_TEAM AND MC_playerBD[EventData.iPart].iTeam = ciHALLOWEEN_ADVERSARY_HUNTED_TEAM
						IF EventData.bToggle
							//Add the sound effect
							ENTITY_INDEX eiTarget 
							eiTarget = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(EventData.iPart)))
							IF iEnemyCloseSoundLoop[EventData.iGenericInt] = 0
							AND IS_ENTITY_ALIVE(eiTarget)
								iEnemyCloseSoundLoop[EventData.iGenericInt] = GET_SOUND_ID()
								PLAY_SOUND_FROM_ENTITY(iEnemyCloseSoundLoop[EventData.iGenericInt], "Hunted_Heartbeat", eiTarget, "Halloween_Adversary_Sounds")
								PRINTLN("[PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_SetHalloweenAdversary2022ProximitySounds][PROCESS_HALLOWEEN_ADVERSARY_2022_SOUNDS_HUNTED] - PLAY_SOUND_FROM_ENTITY with Sound ID = ", iEnemyCloseSoundLoop[EventData.iGenericInt], ", Hunted Part = ", EventData.iGenericInt)
							ENDIF
						ELIF iEnemyCloseSoundLoop[EventData.iGenericInt] > 0
							//Remove the sound effect
							STOP_SOUND(iEnemyCloseSoundLoop[EventData.iGenericInt])
							RELEASE_SOUND_ID(iEnemyCloseSoundLoop[EventData.iGenericInt])
							iEnemyCloseSoundLoop[EventData.iGenericInt] = 0
							PRINTLN("[PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_SetHalloweenAdversary2022ProximitySounds][PROCESS_HALLOWEEN_ADVERSARY_2022_SOUNDS_HUNTED] - STOP_SOUND with Sound ID = ", iEnemyCloseSoundLoop[EventData.iGenericInt], ", Hunted Part = ", EventData.iGenericInt)
						ENDIF
					ENDIF
				BREAK
				CASE g_ciInstancedcontentEventType_PlayHalloweenAdversary2022HunterDeathSound
					VECTOR vHunterDeathLoc
					vHunterDeathLoc.X = EventData.fGenericVecX
					vHunterDeathLoc.Y = EventData.fGenericVecY
					vHunterDeathLoc.Z = EventData.fGenericVecZ
					PLAY_SOUND_FROM_COORD(-1, "Rider_Die", vHunterDeathLoc, "Halloween_Adversary_Sounds")
					PRINTLN("[PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_PlayHalloweenAdversary2022HunterDeathSound][PROCESS_PLAYER_LIVES] - PLAY_SOUND_FROM_COORD - <<", vHunterDeathLoc, ">> - Rider_Die")
				BREAK
				CASE g_ciInstancedcontentEventType_ResetHalloweenAdversary2022ProximitySounds
					IF MC_playerBD[iLocalPart].iTeam != MC_playerBD[EventData.iPart].iTeam
						CLEAR_BIT(iLocalBoolCheck29, LBOOL29_HalloweenAdversary2022ProximitySoundsBroadcasted)
						PRINTLN("[PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_ResetHalloweenAdversary2022ProximitySounds][PROCESS_PLAYER_LIVES] - Clearing LBOOL29_HalloweenAdversary2022ProximitySoundsBroadcasted flag!")
					ENDIF
				BREAK
				CASE g_ciInstancedcontentEventType_SetPedPlayedDialogueNonReset					
					PRINTLN("[PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_SetPedPlayedDialogueNonReset] - Setting iPedSpeechTriggeredBitsetNonReset on ped: ", EventData.iPed)
					FMMC_SET_LONG_BIT(iPedSpeechTriggeredBitsetNonReset, EventData.iPed)					
				BREAK
				CASE g_ciInstancedcontentEventType_SetCutsceneEntityBSWarped
					PRINTLN("[PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_SetCutsceneEntityBSWarped] - Setting iWarpCutsceneEntityBS for EntityIndex: ", EventData.iGenericInt)
					SET_BIT(iWarpCutsceneEntityBS, EventData.iGenericInt)
				BREAK
				CASE g_ciInstancedcontentEventType_ApplyCachedPlayerHeadBlendDataToPlayerPedClones
					IF NOT EventData.bToggle
						//Apply the blend now
						IF NETWORK_DOES_NETWORK_ID_EXIST(EventData.niNetworkIndex)
							PRINTLN("[PlayerPedClone][PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_ApplyCachedPlayerHeadBlendDataToPlayerPedClones] - Event Triggered!")
							PED_INDEX pedTarget
							pedTarget = NET_TO_PED(EventData.niNetworkIndex)
							PLAYER_INDEX playerSource
							playerSource = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(EventData.iPart))
							IF DOES_ENTITY_EXIST(pedTarget) AND DOES_ENTITY_EXIST(GET_PLAYER_PED(playerSource))
								PRINTLN("[PlayerPedClone][PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_ApplyCachedPlayerHeadBlendDataToPlayerPedClones] - Calling NETWORK_APPLY_CACHED_PLAYER_HEAD_BLEND_DATA. Target - SCRIPT_PED_", NETWORK_ENTITY_GET_OBJECT_ID(pedTarget), ", Source - ", GET_PLAYER_NAME(playerSource))
								NETWORK_APPLY_CACHED_PLAYER_HEAD_BLEND_DATA(pedTarget, playerSource)
							ENDIF
						ENDIF
					ELSE //Tell the players to use the cached data from the server array
						IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet5, PBBOOL5_APPLY_SERVER_CACHED_CLONE_HEAD_BLEND)
							PRINTLN("[PlayerPedClone][PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT][g_ciInstancedcontentEventType_ApplyServerCachedPlayerHeadBlendDataToPlayerPedClones][Part ", iLocalPart, "] - Enabling PBBOOL5_APPLY_SERVER_CACHED_CLONE_HEAD_BLEND flag for ", GET_PLAYER_NAME(LocalPlayer))
							SET_BIT(MC_playerBD[iLocalPart].iClientBitSet5, PBBOOL5_APPLY_SERVER_CACHED_CLONE_HEAD_BLEND)
						ENDIF
					ENDIF
				BREAK
				
				CASE g_ciInstancedcontentEventType_WarpedPedOnDamageStart
					PRINTLN("[g_ciInstancedcontentEventType_WarpedPedOnDamageStart] - Setting iPart: ", EventData.iPart, " Warped Ped on rule start Ped: ", EventData.iGenericInt, " Setting iPedWarpedOnThisRule.")
					FMMC_SET_LONG_BIT(iPedWarpedOnThisRule, EventData.iGenericInt)
					CLEAR_BIT(iVehicleHasBeenPlacedIntoInteriorBS, EventData.iGenericInt)
				BREAK	
				CASE g_ciInstancedcontentEventType_WarpedVehicleOnDamageStart
					PRINTLN("[g_ciInstancedcontentEventType_WarpedVehicleOnDamageStart] - Setting iPart: ", EventData.iPart, " Warped Vehicle on rule start Veh: ", EventData.iGenericInt, " Setting iVehicleWarpedOnThisRule.")
					FMMC_SET_LONG_BIT(iVehicleWarpedOnDamage, EventData.iGenericInt)
					CLEAR_BIT(iVehicleHasBeenPlacedIntoInteriorBS, EventData.iGenericInt)
				BREAK	
				CASE g_ciInstancedcontentEventType_WarpedObjectOnDamageStart
					PRINTLN("[g_ciInstancedcontentEventType_WarpedObjectOnDamageStart] - Setting iPart: ", EventData.iPart, " Warped Object on rule start Obj: ", EventData.iGenericInt, " Setting iObjectWarpedOnThisRule.")
					FMMC_SET_LONG_BIT(iObjectWarpedOnThisRule, EventData.iGenericInt)
					CLEAR_BIT(iVehicleHasBeenPlacedIntoInteriorBS, EventData.iGenericInt)
				BREAK	
				CASE g_ciInstancedcontentEventType_WarpedGoToLocateOnDamageStart
					PRINTLN("[g_ciInstancedcontentEventType_WarpedGoToLocateOnDamageStart] - Setting iPart: ", EventData.iPart, " Warped Location on rule start Loc: ", EventData.iGenericInt, " Setting iGoToLocationWarpedOnDamage.")
					FMMC_SET_LONG_BIT(iGoToLocationWarpedOnDamage, EventData.iGenericInt)
					CLEAR_BIT(iVehicleHasBeenPlacedIntoInteriorBS, EventData.iGenericInt)
				BREAK	
				CASE g_ciInstancedcontentEventType_WarpedInteractableDamageStart
					PRINTLN("[g_ciInstancedcontentEventType_WarpedInteractableDamageStart] - Setting iPart: ", EventData.iPart, " Warped Interactable on rule start Interactable: ", EventData.iGenericInt, " Setting iInteractableWarpedOnThisRule.")
					FMMC_SET_LONG_BIT(iInteractableWarpedOnThisRule, EventData.iGenericInt)
					CLEAR_BIT(iVehicleHasBeenPlacedIntoInteriorBS, EventData.iGenericInt)
				BREAK				
				CASE g_ciInstancedcontentEventType_ShouldWarpEntityOnDamageStart
					PRINTLN("[g_ciInstancedcontentEventType_ShouldWarpEntityOnDamageStart] - Setting that Entity Type: ", EventData.iVeh, " Entity Index: ", EventData.iGenericInt, " should warp due to damage event.")
					SET_SHOULD_WARP_ON_DAMAGE_FOR_ENTITY(EventData.iVeh, EventData.iGenericInt)				
				BREAK
				CASE g_ciInstancedcontentEventType_ToggleSpawnGroup
					PRINTLN("[g_ciInstancedcontentEventType_ToggleSpawnGroup] - Setting that Entity Type: ", EventData.iVeh, " Entity Index: ", EventData.iGenericInt, " should warp due to damage event.")
					IF bIsLocalPlayerHost
						INT iSpawnGroupAdditionalFunctionalityBS, iBS
				
						SET_BIT(iBS, EventData.iGenericInt)
						
						IF NOT IS_BIT_SET(MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeedBS, EventData.iGenericInt)
							SET_BIT(iSpawnGroupAdditionalFunctionalityBS, ciSPAWN_GROUP_ADDITIONAL_ENTITY_FUNCTIONALITY_ACTIVATE_ON_EVENT)
						ELSE
							SET_BIT(iSpawnGroupAdditionalFunctionalityBS, ciSPAWN_GROUP_ADDITIONAL_ENTITY_FUNCTIONALITY_DEACTIVATE_ON_EVENT)
						ENDIF
						
						MODIFY_SPAWN_GROUP_DURING_GAMEPLAY_FROM_BITSET(iSpawnGroupAdditionalFunctionalityBS, iBS, iSubBS)
					ENDIF
				BREAK
				CASE g_ciInstancedcontentEventType_ToggleSubpawnGroup
					PRINTLN("[g_ciInstancedcontentEventType_ToggleSubpawnGroup] - Setting that Entity Type: ", EventData.iVeh, " Entity Index: ", EventData.iGenericInt, " should warp due to damage event.")
					IF bIsLocalPlayerHost
						INT iSpawnGroupAdditionalFunctionalityBS, iBS
				
						SET_BIT(iSubBS[EventData.iGenericInt], EventData.iVeh)
						
						IF NOT IS_BIT_SET(MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeed_SubGroupBS[EventData.iGenericInt], EventData.iVeh)
							SET_BIT(iSpawnGroupAdditionalFunctionalityBS, ciSPAWN_GROUP_ADDITIONAL_ENTITY_FUNCTIONALITY_ACTIVATE_ON_EVENT)
						ELSE
							SET_BIT(iSpawnGroupAdditionalFunctionalityBS, ciSPAWN_GROUP_ADDITIONAL_ENTITY_FUNCTIONALITY_DEACTIVATE_ON_EVENT)
						ENDIF
						
						MODIFY_SPAWN_GROUP_DURING_GAMEPLAY_FROM_BITSET(iSpawnGroupAdditionalFunctionalityBS, iBS, iSubBS)
					ENDIF
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_START_SCRIPTED_ANIM_CONVERSATION(INT iEventID)

	SCRIPT_EVENT_DATA_FMMC_EVENT_CONVERSATION_FOR_PED EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_START_SCRIPTED_ANIM_CONVERSATION
		
			IF EventData.Details.FromPlayerIndex = LocalPlayer
				EXIT
			ENDIF
			
			PRINTLN("[Dialogue][ScriptedAnimConversation] PROCESS_SCRIPT_EVENT_FMMC_START_SCRIPTED_ANIM_CONVERSATION | Received scripted anim conversation data! Root: ", EventData.sScriptedAnimConversationEventData.tl23_Root)
			
			CLEAR_SCRIPTED_ANIM_CONVERSATION(sCurrentScriptedAnimConversationData)
			sCurrentScriptedAnimConversationData = EventData.sScriptedAnimConversationEventData	
			
			SET_SCRIPTED_ANIM_CONVERSATION_STATE(ciScriptedAnimConversationState_WaitingToStart)
		ENDIF
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Debug	 -------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Process functions for score events		  		----------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#IF IS_DEBUG_BUILD
PROC PROCESS_SCRIPT_EVENT_FMMC_PAUSE_ALL_TIMERS(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_PAUSE_ALL_TIMERS EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_PAUSE_ALL_TIMERS
			IF EventData.Details.FromPlayerIndex != LocalPlayer
				bWdPauseGameTimer = EventData.bPauseToggle
			ENDIF
			PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_PAUSE_ALL_TIMERS] - Player ", GET_PLAYER_NAME(EventData.Details.FromPlayerIndex), "  EventData.bPauseToggle: ",  EventData.bPauseToggle)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_EDIT_ALL_TIMERS(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_EDIT_ALL_TIMERS EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_EDIT_ALL_TIMERS
			IF EventData.bIncrement
				bWdIncrementTimer = TRUE
			ELSE
				bWdDecrementTimer = TRUE
			ENDIF
			
			PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_EDIT_ALL_TIMERS] - Player ", GET_PLAYER_NAME(EventData.Details.FromPlayerIndex), "  EventData.bIncrement: ",  EventData.bIncrement)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_FMMC_OVERRIDE_LIVES_FAIL(INT iCount)

	SCRIPT_EVENT_DATA_FMMC_OVERRIDE_LIVES_FAIL ObjectiveData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, ObjectiveData, SIZE_OF(ObjectiveData))
		IF ObjectiveData.Details.Type = SCRIPT_EVENT_FMMC_OVERRIDE_LIVES_FAIL
			
			PRINTLN("[RCC MISSION][FailOnDeath] PROCESS_FMMC_OVERRIDE_LIVES_FAIL - ObjectiveData.bIgnoreOutOfLivesFail = ",ObjectiveData.bIgnoreOutOfLivesFail)
			
			IF ObjectiveData.bIgnoreOutOfLivesFail
				PRINT_TICKER_WITH_PLAYER_NAME_AND_STRING("TICK_CHEATED", ObjectiveData.Details.FromPlayerIndex, "TICK_CHEAT_OOL1")
			ELSE
				PRINT_TICKER_WITH_PLAYER_NAME_AND_STRING("TICK_CHEATED", ObjectiveData.Details.FromPlayerIndex, "TICK_CHEAT_OOL0")
			ENDIF
			
			IF g_bIgnoreOutOfLivesFailure != ObjectiveData.bIgnoreOutOfLivesFail
				g_bIgnoreOutOfLivesFailure = ObjectiveData.bIgnoreOutOfLivesFail
				
				PRINTLN("[RCC MISSION][FailOnDeath] PROCESS_FMMC_OVERRIDE_LIVES_FAIL - g_bIgnoreOutOfLivesFailure = ",g_bIgnoreOutOfLivesFailure)
				
				IF bIsLocalPlayerHost
					IF g_bIgnoreOutOfLivesFailure
						PRINTLN("[RCC MISSION][FailOnDeath] PROCESS_FMMC_OVERRIDE_LIVES_FAIL - Server set bit SBDEBUG_DONTFAILWHENOUTOFLIVES")
						SET_BIT(MC_serverBD.iDebugBitSet, SBDEBUG_DONTFAILWHENOUTOFLIVES)
					ELSE
						PRINTLN("[RCC MISSION][FailOnDeath] PROCESS_FMMC_OVERRIDE_LIVES_FAIL - Server clear bit SBDEBUG_DONTFAILWHENOUTOFLIVES")
						CLEAR_BIT(MC_serverBD.iDebugBitSet, SBDEBUG_DONTFAILWHENOUTOFLIVES)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC PROCESS_DEBUG_OBJECTIVE_SKIP_FOR_TEAM(INT iCount)

	SCRIPT_EVENT_DATA_DEBUG_OBJECTIVE_SKIP_FOR_TEAM EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_DEBUG_OBJECTIVE_SKIP_FOR_TEAM
			
			bUsedDebugMenu = TRUE
			
			IF bIsLocalPlayerHost
				IF EventData.iNewTeam = -1
					INT iObjective = EventData.iSkipObjective
					BOOL bPreviousObjective = FALSE
					
					IF iObjective <= MC_serverBD_4.iCurrentHighestPriority[EventData.iTeam]
						PRINTLN("[RCC MISSION] PROCESS_DEBUG_OBJECTIVE_SKIP_FOR_TEAM - team = ",EventData.iTeam, " objective to skip back to: ",iObjective)
						bPreviousObjective = TRUE
					ELSE
						PRINTLN("[RCC MISSION] PROCESS_DEBUG_OBJECTIVE_SKIP_FOR_TEAM - team = ",EventData.iTeam, " objective to skip to: ",iObjective)
						iObjective--
					ENDIF
					
					INVALIDATE_UP_TO_AND_INCLUDING_OBJECTIVE(EventData.iTeam,iObjective,FALSE,TRUE,bPreviousObjective)
				ELSE
					PRINTLN("[RCC MISSION] PROCESS_DEBUG_OBJECTIVE_SKIP_FOR_TEAM - player on team ",EventData.iTeam," has swapped to team ",EventData.iNewTeam)
					STOP_MISSION_FAILING_AS_TEAM_SWAPS(EventData.iTeam, EventData.iNewTeam)
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC
#ENDIF

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Script Event Processing	 ---------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Processing script driven events		  		--------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


/// PURPOSE:
///    Processes a script event
/// PARAMS:
///    iCount - The index of the event in the queue to process
//fmmc2020 - potentiall group cases into mini switches. Peds/players etc
PROC PROCESS_FMMC_SCRIPT_EVENT(INT iCount)

    STRUCT_EVENT_COMMON_DETAILS Details
                            
    GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, Details, SIZE_OF(Details))
    
    SWITCH Details.Type
	
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Processing Audio	 -----------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Processing audio event functionality  		--------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		CASE SCRIPT_EVENT_FMMC_START_PRE_COUNTDOWN_STOP
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_START_PRE_COUNTDOWN_STOP
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION] - Mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_START_PRE_COUNTDOWN_STOP")
			PROCESS_SCRIPT_EVENT_FMMC_START_PRE_COUNTDOWN_STOP(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_LOCATE_ALERT_SOUND
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_LOCATE_ALERT_SOUND
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION] - Mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_LOCATE_ALERT_SOUND")
			PROCESS_SCRIPT_EVENT_FMMC_LOCATE_ALERT_SOUND(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_PLAY_PULSE_SFX
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PLAY_PULSE_SFX
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_PLAY_PULSE_SFX, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_PLAY_PULSE_SFX(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_AUDIO_TRIGGER_ACTIVATED
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_AUDIO_TRIGGER_ACTIVATED_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_AUDIO_TRIGGER_ACTIVATED, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_AUDIO_TRIGGER_ACTIVATED(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_SET_BILLIONAIRE_PARTY_STATE
			PRINTLN("[RCC MISSION]  - Mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_SET_BILLIONAIRE_PARTY_STATE, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_SET_BILLIONAIRE_PARTY_STATE(iCount)
		BREAK
		
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Processing Cutscenes	 -------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Processing cutscene event functionality  			----------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		CASE SCRIPT_EVENT_FMMC_TEAM_HAS_FINISHED_CUTSCENE
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_TEAM_HAS_FINISHED_CUTSCENE_BLOCKED
				BREAK
			ENDIF
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_TEAM_HAS_FINISHED_CUTSCENE, processing event...")
			PROCESS_TEAM_HAS_FINISHED_CUTSCENE( iCount )
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_MY_WEAPON_DATA
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_MY_WEAPON_DATA_BLOCKED
				BREAK
			ENDIF
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_MY_WEAPON_DATA, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_MY_WEAPON_DATA( iCount )
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_CUTSCENE_SPAWN_PROGRESS
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_CUTSCENE_SPAWN_PROGRESS_BLOCKED
				BREAK
			ENDIF
            PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - PROCESS_FMMC_CUTSCENE_SPAWN_PROGRESS, processing event...")
            PROCESS_FMMC_CUTSCENE_SPAWN_PROGRESS(iCount)
        BREAK
		
		CASE SCRIPT_EVENT_FMMC_MOCAP_PLAYER_ANIMATION
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_MOCAP_PLAYER_ANIMATION_BLOCKED
				BREAK
			ENDIF
            PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - PROCESS_MOCAP_PLAYER_ANIMATION, processing event...")
            PROCESS_MOCAP_PLAYER_ANIMATION(iCount)
        BREAK
		
		CASE SCRIPT_EVENT_FMMC_START_STREAMING_END_MOCAP
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_START_STREAMING_END_MOCAP_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - PROCESS_START_STREAMING_END_MOCAP_EVENT, processing event...")
			PROCESS_START_STREAMING_END_MOCAP_EVENT(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_CUTSCENE_PLAYER_REQUEST
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_CUTSCENE_PLAYER_REQUEST_BLOCKED
				BREAK
			ENDIF
            PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - PROCESS_CUTSCENE_PLAYER_REQUEST, processing event...")
            PROCESS_CUTSCENE_PLAYER_REQUEST(iCount)
        BREAK
		
		CASE SCRIPT_EVENT_FMMC_DELIVER_CUTSCENE_VEH
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_DELIVER_CUTSCENE_VEH_BLOCKED
				BREAK
			ENDIF
            PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_DELIVER_CUTSCENE_VEH, processing event...")
            PROCESS_FMMC_ADD_CUTSCENE_VEH(iCount)
        BREAK
		
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Processing Dialogue	 -------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Processing dialogue event functionality  			----------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

		CASE SCRIPT_EVENT_FMMC_DIALOGUE_LOOK
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_DIALOGUE_LOOK_BLOCKED
				BREAK
			ENDIF
            PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_DIALOGUE_LOOK, processing event...")
            PROCESS_FMMC_DIALOGUE_LOOK(iCount)
        BREAK

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Processing Doors	 -----------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Processing door event functionality  			--------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		CASE SCRIPT_EVENT_FMMC_UPDATE_DOOR_OFF_RULE_EVENT
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_UPDATE_DOOR_OFF_RULE_EVENT
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_UPDATE_DOOR_OFF_RULE_EVENT, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_UPDATE_DOOR_OFF_RULE_EVENT(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_DIRECTIONAL_DOOR_EVENT
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_DIRECTIONAL_DOOR_EVENT
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_DIRECTIONAL_DOOR_EVENT, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_DIRECTIONAL_DOOR_EVENT(iCount)
		BREAK
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Processing Ending	 -------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Processing ending event functionality  			----------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		CASE SCRIPT_EVENT_FMMC_TEAM_HAS_EARLY_CELEBRATION
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_TEAM_HAS_EARLY_CELEBRATION_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_TEAM_HAS_EARLY_CELEBRATION, processing event...")
            PROCESS_FMMC_TEAM_HAS_EARLY_CELEBRATION( iCount )
		BREAK
		
		CASE SCRIPT_EVENT_BLOCK_SAVE_EOM_CAR_BEEN_DELIVERED
			IF g_sBlockedEvents.bSCRIPT_EVENT_BLOCK_SAVE_EOM_CAR_BEEN_DELIVERED_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - PROCESS_SCRIPT_EVENT_BLOCK_SAVE_EOM_CAR_BEEN_DELIVERED, processing event...")
			PROCESS_SCRIPT_EVENT_BLOCK_SAVE_EOM_CAR_BEEN_DELIVERED(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_SYNC_SPECTATOR_CELEBRATION_GLOBALS_1
			IF g_sBlockedEvents.bSCRIPT_EVENT_SYNC_SPECTATOR_CELEBRATION_GLOBALS_1_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_SYNC_SPECTATOR_CELEBRATION_GLOBALS_1, processing event...")
			PROCESS_EVENT_SYNC_SPECTATOR_CELEBRATION_GLOBALS_1(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_SYNC_SPECTATOR_CELEBRATION_GLOBALS_2
			IF g_sBlockedEvents.bSCRIPT_EVENT_SYNC_SPECTATOR_CELEBRATION_GLOBALS_2_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_SYNC_SPECTATOR_CELEBRATION_GLOBALS_2, processing event...")
			PROCESS_EVENT_SYNC_SPECTATOR_CELEBRATION_GLOBALS_2(iCount)
		BREAK
		
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Processing HUD	 -----------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Processing HUD event functionality  			--------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

		CASE SCRIPT_EVENT_RACE_RALLY_ARROWS
			IF g_sBlockedEvents.bSCRIPT_EVENT_RACE_RALLY_ARROWS_BLOCKED
				BREAK
			ENDIF
			PROCESS_ARROW_EVENT(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_STOLEN_FLAG_SHARD
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_STOLEN_FLAG_SHARD_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_STOLEN_FLAG_SHARD, processing event...")
			PROCESS_FMMC_STOLEN_FLAG_SHARD(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_CAPTURED_FLAG_SHARD
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_CAPTURED_FLAG_SHARD_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - Mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_CAPTURED_FLAG_SHARD, processing event...")
			PROCESS_FMMC_CAPTURED_FLAG_SHARD(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_RETURNED_FLAG_SHARD
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_RETURNED_FLAG_SHARD_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - Mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_RETURNED_FLAG_SHARD, processing event...")
			PROCESS_FMMC_RETURNED_FLAG_SHARD(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_TAGGED_ENTITY
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_TAGGED_ENTITY_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_TAGGED_ENTITY, processing event...")
			PROCESS_FMMC_TAGGED_ENTITY(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_DISTANCE_GAINED
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_DISTANCE_GAINED_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_DISTANCE_GAINED, processing event...")
			PROCESS_SCRIPT_EVENT_DISTANCE_GAINED_TICKER(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_VIP_MARKER_POSITION
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_VIP_MARKER_POSITION_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_VIP_MARKER_POSITION, processing event...")
			PROCESS_FMMC_VIP_MARKER_POSITION( iCount )
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_DISPLAY_CUSTOM_OBJECT_PICKUP_SHARD
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_DISPLAY_CUSTOM_OBJECT_PICKUP_SHARD_BLOCKED
				BREAK
			ENDIF
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_DISPLAY_CUSTOM_OBJECT_PICKUP_SHARD, processing event...")
			PROCESS_FMMC_DISPLAY_CUSTOM_OBJECT_PICKUP_SHARD(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_SUDDEN_DEATH_REASON
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_SUDDEN_DEATH_REASON_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_SUDDEN_DEATH_REASON, processing event...")
			PROCESS_SCRIPT_EVENT_SUDDEN_DEATH_REASON(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_REMOVE_DUMMY_BLIP
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_REMOVE_DUMMY_BLIP
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_REMOVE_DUMMY_BLIP, processing event...")
			PROCESS_SCRIPT_EVENT_REMOVE_DUMMY_BLIP(iCount)
		BREAK
				
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Locations
// ##### Description: Processing location event functionality
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

		CASE SCRIPT_EVENT_FMMC_LOCATION_INPUT_TRIGGERED
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_LOCATION_INPUT_TRIGGERED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_LOCATION_INPUT_TRIGGERED, processing event...")
			PROCESS_FMMC_LOCATION_INPUT_TRIGGERED(iCount)
		BREAK
		
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Processing Minigames	 -------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Processing minigame event functionality  			----------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

		CASE SCRIPT_EVENT_HEIST_BEGIN_HACK_MINIGAME
			IF g_sBlockedEvents.bSCRIPT_EVENT_HEIST_BEGIN_HACK_MINIGAME_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_HEIST_BEGIN_HACK_MINIGAME, processing event...")
			PROCESS_HEIST_BEGIN_HACK_MINIGAME(iCount)
		BREAK
		CASE SCRIPT_EVENT_HEIST_END_HACK_MINIGAME
			IF g_sBlockedEvents.bSCRIPT_EVENT_HEIST_END_HACK_MINIGAME_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_HEIST_END_HACK_MINIGAME, processing event...")
			PROCESS_HEIST_END_HACK_MINIGAME(iCount)
		BREAK
		CASE SCRIPT_EVENT_FMMC_DRILL_ASSET
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_DRILL_ASSET_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_DRILL_ASSET, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_DRILL_ASSET(iCount)
		BREAK
		CASE SCRIPT_EVENT_VAULT_DRILL_ASSET_REQUEST_EVENT
			IF g_sBlockedEvents.bSCRIPT_EVENT_VAULT_DRILL_ASSET_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_VAULT_DRILL_ASSET_REQUEST_EVENT, processing event...")
			PROCESS_SCRIPT_EVENT_VAULT_DRILL_ASSET(iCount)	
		BREAK
		CASE SCRIPT_EVENT_VAULT_DRILL_PROGRESS_EVENT
			IF g_sBlockedEvents.bSCRIPT_EVENT_VAULT_DRILL_PROGRESS_EVENT_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_VAULT_DRILL_PROGRESS_EVENT, processing event...")
			PROCESS_SCRIPT_EVENT_VAULT_DRILL_PROGRESS(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_INTERACT_WITH_EVENT
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_INTERACT_WITH_EVENT 
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_INTERACT_WITH_EVENT, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_INTERACT_WITH_EVENT(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_THERMITE_EFFECT_EVENT
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_THERMITE_EFFECT_EVENT
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_THERMITE_EFFECT_EVENT, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_THERMITE_EFFECT_EVENT(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_GRAB_CASH
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_GRAB_CASH_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_GRAB_CASH, processing event...")
			PROCESS_SCRIPT_EVENT_GRAB_CASH(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_GRAB_CASH_DISPLAY_TAKE
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_GRAB_CASH_DISPLAY_TAKE_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_GRAB_CASH_DISPLAY_TAKE, processing event...")
			PROCESS_SCRIPT_EVENT_GRAB_CASH_DISPLAY_TAKE(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_ADD_GRABBED_CASH
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_REMOVE_GRABBED_CASH_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - Mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_ADD_GRABBED_CASH, processing event...")
			PROCESS_FMMC_ADD_GRABBED_CASH(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_REMOVE_GRABBED_CASH
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_REMOVE_GRABBED_CASH_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - Mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_REMOVE_GRABBED_CASH, processing event...")
			PROCESS_FMMC_REMOVE_GRABBED_CASH(iCount)
		BREAK
				
		CASE SCRIPT_EVENT_FMMC_INVESTIGATE_CONTAINER
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_INVESTIGATE_CONTAINER_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_INVESTIGATE_CONTAINER, processing event...")
			PROCESS_FMMC_CONTAINER_BEING_INVESTIGATED( iCount )
		BREAK
		
		CASE SCRIPT_EVENT_CASINO_UPDATED_STEAL_PAINTING_STATE
			PRINTLN("[RCC MISSION] - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_CASINO_UPDATED_STEAL_PAINTING_STATE, processing event...")
			PROCESS_SCRIPT_EVENT_SET_CUT_OUT_PAINTING_SUBANIM_STATE(iCount)
		BREAK
		
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Processing Interactables	 ---------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Processing Interactable event functionality  			------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		CASE SCRIPT_EVENT_FMMC_INTERACTABLE
			PROCESS_FMMC_INTERACTABLE_EVENT(iCount)
		BREAK
		
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Processing Objects	 -------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Processing object event functionality  			----------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		CASE SCRIPT_EVENT_FMMC_CCTV_EVENT
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_CCTV_EVENT, processing event...")
			PROCESS_FMMC_CCTV_EVENT(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_CCTV_REACHED_DESTINATION
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_CCTV_REACHED_DESTINATION
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_CCTV_REACHED_DESTINATION, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_CCTV_REACHED_DESTINATION(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_UPDATE_DOOR_LINKED_ENTITIES
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_UPDATE_DOOR_OBJS
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_UPDATE_DOOR_LINKED_ENTITIES, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_UPDATE_DOOR_LINKED_ENTITIES(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_OBJECT_CARRIER_ALERT
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_OBJECT_CARRIER_ALERT
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - Mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_OBJECT_CARRIER_ALERT, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_OBJECT_CARRIER_ALERT(iCount)
		BREAK

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Processing Peds	 -----------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Processing rule ped functionality  			--------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

		CASE SCRIPT_EVENT_FMMC_PED_GIVEN_GUNONRULE
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PED_GIVEN_GUNONRULE_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_PED_GIVEN_GUNONRULE, processing event...")
			PROCESS_FMMC_PED_GIVEN_GUNONRULE(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_PED_COMBAT_STYLE_CHANGED
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PED_COMBAT_STYLE_CHANGED_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_PED_COMBAT_STYLE_CHANGED, processing event...")
			PROCESS_FMMC_PED_COMBAT_STYLE_CHANGED(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_PED_WAITING_AFTER_GOTO
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PED_WAITING_AFTER_GOTO_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_PED_WAITING_AFTER_GOTO, processing event...")
			PROCESS_FMMC_PED_WAITING_AFTER_GOTO(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_PED_DONE_WAITING_AFTER_GOTO
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PED_DONE_WAITING_AFTER_GOTO_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_PED_DONE_WAITING_AFTER_GOTO, processing event...")
			PROCESS_FMMC_PED_DONE_WAITING_AFTER_GOTO(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_PED_CLEAR_GOTO_WAITING_STATE
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PED_CLEAR_GOTO_WAITING_STATE_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_PED_CLEAR_GOTO_WAITING_STATE, processing event...")
			PROCESS_FMMC_PED_CLEAR_GOTO_WAITING_STATE(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_PED_DEFENSIVE_AREA_GOTO_CLEANED_UP
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PED_DEFENSIVE_AREA_GOTO_CLEANED_UP_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_PED_DEFENSIVE_AREA_GOTO_CLEANED_UP, processing event...")
			PROCESS_FMMC_PED_DEFENSIVE_AREA_GOTO_CLEANED_UP(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_PED_DIRTY_FLAG_SET
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PED_DIRTY_FLAG_SET_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_PED_DIRTY_FLAG_SET, processing event...")
			PROCESS_FMMC_PED_DIRTY_FLAG_SET(iCount)
		BREAK
		
		ENDSWITCH
		//FMMC2020 - Do this better
   		SWITCH Details.Type
		
		
        CASE SCRIPT_EVENT_FMMC_ANIM_STARTED
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_ANIM_STARTED_BLOCKED
				BREAK
			ENDIF
            PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_ANIM_STARTED, processing event...")
            PROCESS_FMMC_ANIM_STARTED_EVENT(iCount)
        BREAK  
		
		CASE SCRIPT_EVENT_FMMC_ANIM_STOPPED
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_ANIM_STOPPED_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_ANIM_STOPPED, processing event...")
            PROCESS_FMMC_ANIM_STOPPED_EVENT(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_SECONDARY_ANIM_CLEANUP
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_SECONDARY_ANIM_CLEANUP_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_SECONDARY_ANIM_CLEANUP, processing event...")
            PROCESS_FMMC_SECONDARY_ANIM_CLEANUP_EVENT(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_REMOVE_PED_FROM_GROUP
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_REMOVE_PED_FROM_GROUP_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_CUTSCENE_PED, processing event...")
			PROCESS_FMMC_REMOVE_PED_FROM_GROUP(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_PED_SUCCESSFULLY_REMOVED_FROM_GROUP
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PED_SUCCESSFULLY_REMOVED_FROM_GROUP_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_PED_SUCCESSFULLY_REMOVED_FROM_GROUP, processing event...")
			PROCESS_FMMC_PED_SUCCESSFULLY_REMOVED_FROM_GROUP(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_PED_HEARING
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PED_HEARING_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_PED_HEARING, processing event...")
			PROCESS_FMMC_PED_HEARING(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_PED_SPOOKED
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PED_SPOOKED_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_PED_SPOOKED, processing event...")
			PROCESS_FMMC_PED_SPOOKED(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_PED_UNSPOOKED
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PED_UNSPOOKED_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_PED_UNSPOOKED, processing event...")
			PROCESS_FMMC_PED_UNSPOOKED(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_PED_SPOOK_HURTORKILLED
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PED_SPOOK_HURTORKILLED_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_PED_SPOOK_HURTORKILLED, processing event...")
			PROCESS_FMMC_PED_SPOOK_HURTORKILLED(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_PED_GIVEN_SPOOKED_GOTO_TASK
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PED_GIVEN_SPOOKED_GOTO_TASK
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_PED_GIVEN_SPOOKED_GOTO_TASK, processing event...")
			PROCESS_FMMC_PED_GIVEN_SPOOKED_GOTO_TASK(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_PED_GIVEN_UNSPOOKED_GOTO_TASK
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PED_GIVEN_UNSPOOKED_GOTO_TASK
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_PED_GIVEN_UNSPOOKED_GOTO_TASK, processing event...")
			PROCESS_FMMC_PED_GIVEN_UNSPOOKED_GOTO_TASK(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_PED_UPDATE_TASKED_GOTO_PROGRESS
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PED_UPDATE_TASKED_GOTO_PROGRESS
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_PED_UPDATE_TASKED_GOTO_PROGRESS, processing event...")
			PROCESS_FMMC_PED_UPDATE_TASKED_GOTO_PROGRESS(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_AGGROED_SCRIPT_FORCED_PED
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_AGGROED_SCRIPT_FORCED_PED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_AGGROED_SCRIPT_FORCED_PED, processing event...")
			PROCESS_FMMC_FMMC_AGGROED_SCRIPT_FORCED_PED(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_PED_AGGROED
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PED_AGGROED_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_PED_AGGROED, processing event...")
			PROCESS_FMMC_PED_AGGROED(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_PED_SPEECH_AGGROED
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PED_SPEECH_AGGROED_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_PED_SPEECH_AGGROED, processing event...")
			PROCESS_FMMC_PED_SPEECH_AGGROED(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_PED_CANCEL_TASKS
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PED_CANCEL_TASKS_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_PED_CANCEL_TASKS, processing event...")
			PROCESS_FMMC_PED_CANCEL_TASKS(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_PED_SPEECH_AGRO_BITSET
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PED_SPEECH_AGRO_BITSET_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_PED_SPEECH_AGRO_BITSET, processing event...")
			PROCESS_FMMC_PED_SPEECH_AGRO_BITSET(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_TRIGGER_AGGRO_FOR_TEAM
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_TRIGGER_AGGRO_FOR_TEAM
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_TRIGGER_AGGRO_FOR_TEAM, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_TRIGGER_AGGRO_FOR_TEAM(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_PLAY_SYNC_SCENE_FACIAL_ANIM
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PLAY_SYNC_SCENE_FACIAL_ANIM
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_PLAY_SYNC_SCENE_FACIAL_ANIM, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_PLAY_SYNC_SCENE_FACIAL_ANIM(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_BLOCK_WANTED_CONE_RESPONSE_EVENT
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_BLOCK_WANTED_CONE_RESPONSE_EVENT
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_BLOCK_WANTED_CONE_RESPONSE_EVENT, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_BLOCK_WANTED_CONE_RESPONSE_EVENT(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_COP_DECOY_EVENT
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_COP_DECOY_EVENT
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_COP_DECOY_EVENT, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_COP_DECOY_EVENT(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_COP_BEHAVIOUR_OVERRIDE_EVENT
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_COP_BEHAVIOUR_OVERRIDE_EVENT
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_COP_BEHAVIOUR_OVERRIDE_EVENT, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_COP_BEHAVIOUR_OVERRIDE_EVENT(iCount)
		BREAK
		CASE SCRIPT_EVENT_FMMC_SPAWN_PED_AMMO_PICKUP
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_SPAWN_PED_AMMO_PICKUP
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - Mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_SPAWN_PED_AMMO_PICKUP, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_SPAWN_PED_AMMO_PICKUP(iCount)
		BREAK
		CASE SCRIPT_EVENT_FMMC_REMOVE_PED_AMMO_PICKUP
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_REMOVE_PED_AMMO_PICKUP
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - Mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_REMOVE_PED_AMMO_PICKUP, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_REMOVE_PED_AMMO_PICKUP(iCount)
		BREAK
		
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Processing Players	 -------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Processing player event functionality  			----------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

		CASE SCRIPT_EVENT_FMMC_PLAYER_SWITCHED_TEAMS
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PLAYER_SWITCHED_TEAMS_BLOCKED
				BREAK
			ENDIF
            PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - PROCESS_FMMC_PLAYER_SWITCHED_TEAMS, processing event...")
            PROCESS_FMMC_PLAYER_SWITCHED_TEAMS_EVENT(iCount)
        BREAK
		
		CASE SCRIPT_EVENT_FMMC_PLAYER_TEAM_SWITCH_REQUEST
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PLAYER_TEAM_SWITCH_REQUEST_BLOCKED
				BREAK
			ENDIF
            PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_PLAYER_TEAM_SWITCH_REQUEST, processing event...")
            PROCESS_FMMC_PLAYER_REQUEST_TEAM_SWITCH_EVENT(iCount)
        BREAK
		
		CASE SCRIPT_EVENT_SERVER_AUTHORISED_TEAM_SWAP
			IF g_sBlockedEvents.bSCRIPT_EVENT_SERVER_AUTHORISED_TEAM_SWAP_BLOCKED
				BREAK
			ENDIF
		 	PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_SERVER_AUTHORISED_TEAM_SWAP, processing event...")
            PROCESS_FMMC_SERVER_AUTHORISED_TEAM_SWAP_EVENT(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_CLIENT_TEAM_SWAP_COMPLETE
			IF g_sBlockedEvents.bSCRIPT_EVENT_CLIENT_TEAM_SWAP_COMPLETE_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_CLIENT_TEAM_SWAP_COMPLETE, processing event...")
            PROCESS_FMMC_CLIENT_TEAM_SWAP_COMPLETE(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_PLAYER_COMMITED_SUICIDE
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PLAYER_COMMITED_SUICIDE_BLOCKED
				BREAK
			ENDIF
            PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - PROCESS_FMMC_PLAYER_COMMITED_SUICIDE_EVENT, processing event...")
            PROCESS_FMMC_PLAYER_COMMITED_SUICIDE_EVENT(iCount)
        BREAK
		
		CASE SCRIPT_EVENT_RANDOM_SPAWN_SLOT_USED
			IF g_sBlockedEvents.bSCRIPT_EVENT_RANDOM_SPAWN_SLOT_USED_BLOCKED
				BREAK
			ENDIF
            PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS -  SCRIPT_EVENT_RANDOM_SPAWN_SLOT_USED, processing event...")
            PROCESS_RANDOM_SPAWN_SLOT_FOR_TEAM(iCount)
        BREAK
		
		CASE SCRIPT_EVENT_FMMC_REMOTE_VEHICLE_SWAP
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_REMOTE_VEHICLE_SWAP_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS -  SCRIPT_EVENT_FMMC_REMOTE_VEHICLE_SWAP, processing event...")
            PROCESS_REMOTE_VEHICLE_SWAP(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_ALPHA_CHANGE
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_CHANGE_ALPHA_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - bSCRIPT_EVENT_FMMC_CHANGE_ALPHA_BLOCKED, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_ALPHA_CHANGE(iCount)				
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_CLEAR_HAS_BEEN_ON_SMALLEST_TEAM
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_DECREMENT_REMAINING_TIME
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION] - Mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_CLEAR_HAS_BEEN_ON_SMALLEST_TEAM")
			PROCESS_SCRIPT_EVENT_FMMC_CLEAR_HAS_BEEN_ON_SMALLEST_TEAM(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_PHONE_EMP_EVENT
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PHONE_EMP_EVENT
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_PHONE_EMP_EVENT, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_PHONE_EMP_EVENT(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_KILL_PLAYER_NOT_AT_LOCATE
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_KILL_PLAYER_NOT_AT_LOCATE_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_KILL_PLAYER_NOT_AT_LOCATE, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_KILL_PLAYER_NOT_AT_LOCATE(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_SPAWN_PROTECTION_ALPHA
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_SPAWN_PROTECTION_ALPHA_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_SPAWN_PROTECTION_ALPHA, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_SPAWN_PROTECTION_ALPHA(iCount)
		BREAK
		
		CASE  SCRIPT_EVENT_FMMC_BROADCAST_PREREQS
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_BROADCAST_PREREQS
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_BROADCAST_PREREQS, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_BROADCAST_PREREQS(iCount)	
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_INITIATE_SEAT_SWAP_TEAM_VEHICLE
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_INITIATE_SEAT_SWAP_TEAM_VEHICLE_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_INITIATE_SEAT_SWAP_TEAM_VEHICLE, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_INITIATE_SEAT_SWAP_TEAM_VEHICLE(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_MANUAL_RESPAWN_TO_PASSENGERS
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_MANUAL_RESPAWN_TO_PASSENGERS_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_MANUAL_RESPAWN_TO_PASSENGERS, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_MANUAL_RESPAWN_TO_PASSENGERS(iCount)
		BREAK

		CASE SCRIPT_EVENT_FMMC_LEFT_LOCATE_DELAY_RECAPTURE
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_LEFT_LOCATE_DELAY_RECAPTURE
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_LEFT_LOCATE_DELAY_RECAPTURE, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_LEFT_LOCATE_DELAY_RECAPTURE(iCount)
		BREAK

#IF FEATURE_HEIST_ISLAND		
		CASE SCRIPT_EVENT_FMMC_PLAYER_ABILITY_ACTIVATED
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PLAYER_ABILITY_ACTIVATED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_PLAYER_ABILITY_ACTIVATED, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_PLAYER_ABILITY_ACTIVATED(iCount)
		BREAK
#ENDIF		
		
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Model Swap
// ##### Description: Processing model swap event functionality
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
		CASE SCRIPT_EVENT_FMMC_MODEL_SWAP
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_MODEL_SWAP
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_MODEL_SWAP, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_MODEL_SWAP(iCount)
		BREAK
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Props
// ##### Description: Processing prop event functionality
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

		CASE SCRIPT_EVENT_FMMC_LOCH_SANTOS_MONSTER
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_LOCH_SANTOS_MONSTER
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_LOCH_SANTOS_MONSTER, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_LOCH_SANTOS_MONSTER(iCount)
		BREAK
		
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Processing Rules	 -----------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Processing rule event functionality  			--------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		
		CASE SCRIPT_EVENT_FMMC_OBJECTIVE_COMPLETE
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_OBJECTIVE_COMPLETE_BLOCKED
				BREAK
			ENDIF
            PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT, processing event...")
            PROCESS_FMMC_OBJECTIVE_COMPLETE_EVENT(iCount)
        BREAK
		
		CASE SCRIPT_EVENT_FMMC_OBJECTIVE_END_MESSAGE
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_OBJECTIVE_END_MESSAGE_BLOCKED
				BREAK
			ENDIF
            PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - PROCESS_FMMC_OBJECTIVE_END_MESSAGE, processing event...")
            PROCESS_FMMC_OBJECTIVE_END_MESSAGE_EVENT(iCount)
        BREAK
		
		CASE SCRIPT_EVENT_FMMC_OBJECTIVE_MID_POINT
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_OBJECTIVE_MID_POINT_BLOCKED
				BREAK
			ENDIF
             PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_OBJECTIVE_MID_POINT, processing event...")
             PROCESS_FMMC_OBJECTIVE_MID_POINT(iCount)
        BREAK
		
		CASE SCRIPT_EVENT_FMMC_DECREMENT_REMAINING_TIME
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_DECREMENT_REMAINING_TIME
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION] - Mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_DECREMENT_REMAINING_TIME")
			PROCESS_SCRIPT_EVENT_FMMC_DECREMENT_REMAINING_TIME(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_INCREMENT_REMAINING_TIME
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_INCREMENT_REMAINING_TIME
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION] - Mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_INCREMENT_REMAINING_TIME")
			PROCESS_SCRIPT_EVENT_FMMC_INCREMENT_REMAINING_TIME(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_NETWORK_INDEX_FOR_LATER_RULE
			IF g_sBlockedEvents.bSCRIPT_EVENT_NETWORK_INDEX_FOR_LATER_RULE_BLOCKED
				BREAK
			ENDIF
            PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_NETWORK_INDEX_FOR_LATER_RULE, processing event...")
            PROCESS_NETWORK_INDEX_FOR_LATER_RULE(iCount)
        BREAK
		
		CASE SCRIPT_EVENT_FMMC_OBJECTIVE_REVALIDATE
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_OBJECTIVE_REVALIDATE_BLOCKED
				BREAK
			ENDIF
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_OBJECTIVE_REVALIDATE, processing event...")
			PROCESS_FMMC_OBJECTIVE_REVALIDATE(iCount)
		BREAK

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Processing Spawning	 -------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Processing spawning event functionality  			----------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		
		CASE SCRIPT_EVENT_FMMC_REMOTELY_ADD_RESPAWN_POINTS
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_REMOTELY_ADD_RESPAWN_POINTS
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_REMOTELY_ADD_RESPAWN_POINTS, processing event...")
			PROCESS_FMMC_REMOTELY_ADD_RESPAWN_POINTS(iCount)
		BREAK

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Processing Vehicles	 -------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Processing vehicle event functionality  			----------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		CASE SCRIPT_EVENT_FMMC_EXIT_MISSION_MOC
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_EXIT_MISSION_MOC_BLOCKED
				BREAK
			ENDIF
			 PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_EXIT_MISSION_MOC, processing event...")
            PROCESS_FMMC_EXIT_MISSION_MOC(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_DELIVER_VEHICLE_STOP
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_DELIVER_VEHICLE_STOP
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - bSCRIPT_EVENT_FMMC_DELIVER_VEHICLE_STOP, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_DELIVER_VEHICLE_STOP(iCount)				
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_FORCING_EVERYONE_FROM_VEH
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_FORCING_EVERYONE_FROM_VEH_BLOCKED
				BREAK
			ENDIF
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_FORCING_EVERYONE_FROM_VEH, processing event...")
			PROCESS_FMMC_FORCING_EVERYONE_FROM_VEH(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_NOTIFY_WANTED_SET_FOR_VEHICLE
			IF g_sBlockedEvents.bSCRIPT_EVENT_NOTIFY_WANTED_SET_FOR_VEHICLE_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[MJL][EVENT] - PROCESS_EVENTS - SCRIPT_EVENT_NOTIFY_WANTED_SET_FOR_VEHICLE, processing event...")
			PROCESS_SCRIPT_EVENT_NOTIFY_WANTED_SET_FOR_VEHICLE(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_SET_VEHICLE_IMMOVABLE
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_SET_VEHICLE_IMMOVABLE
				BREAK
			ENDIF
			PRINTLN("[EVENT] - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_SET_VEHICLE_IMMOVABLE, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_SET_VEHICLE_IMMOVABLE(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_REMOTE_VEHICLE_EXPLOSION
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_REMOTE_VEHICLE_EXPLOSION
				BREAK
			ENDIF
			PRINTLN("[EVENT] - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_REMOTE_VEHICLE_EXPLOSION, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_REMOTE_VEHICLE_EXPLOSION(iCount)
		BREAK

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Processing World	 -----------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Processing world event functionality  			----------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		CASE SCRIPT_EVENT_FMMC_DETONATE_PROP
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_DETONATE_PROP_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_DETONATE_PROP, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_DETONATE_PROP(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_PROP_DESTROYED
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PROP_DESTROYED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION] - Mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_PROP_DESTROYED")
			PROCESS_SCRIPT_EVENT_FMMC_PROP_DESTROYED(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_MOVE_FMMC_YACHT
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_MOVE_FMMC_YACHT, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_MOVE_FMMC_YACHT(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_SET_ARTIFICIAL_LIGHTS
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_SET_ARTIFICIAL_LIGHTS, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_SET_ARTIFICIAL_LIGHTS(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_INTERIOR_DESTRUCTION
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_INTERIOR_DESTRUCTION, processing event...")
			PROCESS_EVENT_FMMC_INTERIOR_DESTRUCTION(iCount)
		BREAK
		
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Processing Zones	 -----------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Processing zone event functionality  			--------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

		
		CASE SCRIPT_EVENT_FMMC_TRIGGERED_BODY_SCANNER
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_TRIGGERED_BODY_SCANNER
				BREAK
			ENDIF
            PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS -  SCRIPT_EVENT_FMMC_TRIGGERED_BODY_SCANNER, processing event...")
            PROCESS_EVENT_FMMC_TRIGGERED_BODY_SCANNER(iCount)
        BREAK
		
		CASE SCRIPT_EVENT_FMMC_REQUEST_RESTRICTION_ROCKETS
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_REQUEST_RESTRICTION_ROCKETS_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_REQUEST_RESTRICTION_ROCKETS, processing event...")
			PROCESS_FMMC_REQUEST_RESTRICTION_ROCKETS(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_METAL_DETECTOR_ALERTED
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_METAL_DETECTOR_ALERTED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_METAL_DETECTOR_ALERTED, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_METAL_DETECTOR_ALERTED(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_ZONE_TIMER
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_ZONE_TIMER
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_ZONE_TIMER, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_ZONE_TIMER(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_ZONE_AIR_DEFENCE_SHOT
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_ZONE_AIR_DEFENCE_SHOT
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_ZONE_AIR_DEFENCE_SHOT, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_ZONE_AIR_DEFENCE_SHOT(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_SOUND_ZONE
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_SOUND_ZONE
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - Mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_SOUND_ZONE, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_SOUND_ZONE(iCount)
		BREAK

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Processing Pickups ----------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Processing pickup ped functionality  			--------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		
		CASE SCRIPT_EVENT_FMMC_PICKED_UP_LIVES_PICKUP
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PICKED_UP_LIVES_PICKUP_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_PICKED_UP_LIVES_PICKUP, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_PICKED_UP_LIVES_PICKUP(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_MISSION_EQUIPMENT
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_MISSION_EQUIPMENT, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_MISSION_EQUIPMENT(iCount)
		BREAK

		CASE SCRIPT_EVENT_FMMC_SPAWN_PICKUP
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - PROCESS_SCRIPT_EVENT_FMMC_SPAWN_PICKUP, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_SPAWN_PICKUP(iCount)		
		BREAK

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Processing Scoring	 -------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Processing score event functionality  			----------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

		
		CASE SCRIPT_EVENT_FMMC_PLAYER_SCORE
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PLAYER_SCORE_BLOCKED
				BREAK
			ENDIF
            PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - PROCESS_FMMC_INCREMENT_REMOTE_PLAYER_SCORE, processing event...")
            PROCESS_FMMC_INCREMENT_REMOTE_PLAYER_SCORE(iCount)
        BREAK
		
		CASE SCRIPT_EVENT_FMMC_INCREMENTED_LOCAL_PLAYER_SCORE
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_INCREMENTED_LOCAL_PLAYER_SCORE_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_INCREMENTED_LOCAL_PLAYER_SCORE, processing event...")
            PROCESS_FMMC_INCREMENTED_LOCAL_PLAYER_SCORE(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_LOCATE_CAPTURE_PLAYER_SCORE
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_LOCATE_CAPTURE_PLAYER_SCORE
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_LOCATE_CAPTURE_PLAYER_SCORE, processing event...")
			PROCESS_FMMC_LOCATE_CAPTURE_PLAYER_SCORE(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_CANT_DECREMENT_SCORE
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_CANT_DECREMENT_SCORE_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_CANT_DECREMENT_SCORE, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_CANT_DECREMENT_SCORE(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_START_SCRIPTED_ANIM_CONVERSATION		
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_START_SCRIPTED_ANIM_CONVERSATION
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - bSCRIPT_EVENT_FMMC_START_SCRIPTED_ANIM_CONVERSATION, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_START_SCRIPTED_ANIM_CONVERSATION(iCount)	
		BREAK
		
		CASE SCRIPT_EVENT_FMMC_ENTITY_DAMAGED_EVENT_FAKE
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_ENTITY_DAMAGED_EVENT_FAKE
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - bSCRIPT_EVENT_FMMC_ENTITY_DAMAGED_EVENT_FAKE, processing event...")
			PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT_FAKE(iCount)
		BREAK
		
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Generic Render Targets
// ##### Description: Functions related to the shared render target system which can be used by all entity types.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

		CASE SCRIPT_EVENT_FMMC_SHARED_RENDERTARGET_EVENT
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_SHARED_RENDERTARGET_EVENT, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_SHARED_RENDER_TARGET_EVENT(iCount)
		BREAK

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Processing Generic Event	 ---------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Processing the generic reusable event  			----------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		
		CASE SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - bSCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_FOR_INSTANCED_CONTENT(iCount)				
		BREAK
		
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Processing Debug	 -----------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Processing debug event functionality  			----------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#IF IS_DEBUG_BUILD
		CASE SCRIPT_EVENT_FMMC_PAUSE_ALL_TIMERS
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PAUSE_ALL_TIMERS_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_PAUSE_ALL_TIMER, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_PAUSE_ALL_TIMERS(iCount)
		BREAK 

		CASE SCRIPT_EVENT_FMMC_EDIT_ALL_TIMERS
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_EDIT_ALL_TIMERS_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_EDIT_ALL_TIMER, processing event...")
			PROCESS_SCRIPT_EVENT_FMMC_EDIT_ALL_TIMERS(iCount)
		BREAK 
		
		CASE SCRIPT_EVENT_FMMC_OVERRIDE_LIVES_FAIL
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_OVERRIDE_LIVES_FAIL_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_FMMC_OVERRIDE_LIVES_FAIL, processing event...")
			PROCESS_FMMC_OVERRIDE_LIVES_FAIL(iCount)
		BREAK
		
		CASE SCRIPT_EVENT_DEBUG_OBJECTIVE_SKIP_FOR_TEAM
			IF g_sBlockedEvents.bSCRIPT_EVENT_DEBUG_OBJECTIVE_SKIP_FOR_TEAM_BLOCKED
				BREAK
			ENDIF
			PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - SCRIPT_EVENT_DEBUG_OBJECTIVE_SKIP_FOR_TEAM, processing event...")
			PROCESS_DEBUG_OBJECTIVE_SKIP_FOR_TEAM(iCount)
		BREAK	
#ENDIF
		
	ENDSWITCH
	
ENDPROC


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Event Processing			 ---------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Processing event queue	  					--------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


/// PURPOSE:
///    Process all the events received in the events queues
PROC PROCESS_EVENTS()

	INT iCount
	EVENT_NAMES ThisScriptEvent
	
	//Network Events
	REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_NETWORK) iCount
	
		ThisScriptEvent = GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_NETWORK, iCount)
		SWITCH ThisScriptEvent
		
			CASE EVENT_NETWORK_PICKUP_RESPAWNED
				PROCESS_PICKUP_RESPAWN_SOUND(iCount)	
			BREAK
		
			CASE EVENT_NETWORK_SCRIPT_EVENT 
				PROCESS_FMMC_SCRIPT_EVENT(iCount)
			BREAK
			
			CASE EVENT_NETWORK_DAMAGE_ENTITY
				PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT(iCount)
				PROCESS_SWAP_TEAM_DAMAGE_EVENTS(iCount)
			BREAK
			
			CASE EVENT_NETWORK_PLAYER_COLLECTED_PICKUP
				IF NOT bIsAnySpectator
					PROCESS_COLLECTED_PICKUP_EVENT(iCount)
				ENDIF
			BREAK
			
			CASE EVENT_NETWORK_STUNT_PERFORMED
				PROCESS_STUNT_RP_EVENT(iCount)
			BREAK
			
			CASE EVENT_NETWORK_FIRED_DUMMY_PROJECTILE
				PROCESS_ENEMY_FIRED_PROJECTILE(iCount)
			BREAK
			
			CASE EVENT_NETWORK_PLAYER_COLLECTED_PORTABLE_PICKUP
				PROCESS_PLAYER_COLLECTED_PORTABLE_PICKUP(iCount)
			BREAK
			
		ENDSWITCH
	ENDREPEAT
	
	//fmmc2020 - Is this still needed? or can it be moved to ped header?
	REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_AI) iCount
		
		ThisScriptEvent = GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_AI, iCount)
		
		SWITCH ThisScriptEvent
			
			CASE EVENT_PED_SEEN_DEAD_PED
				PROCESS_FMMC_PED_SEEN_DEAD_PED(iCount)
			BREAK
			
		ENDSWITCH
		
	ENDREPEAT
	
ENDPROC
