// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### System: GTA V Online Mission Controller - Rules -------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Script: FM_Mission_Controller_2020.sc -----------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: 
// ##### Processes moving from one rule to another, including skips and going back to previous rules
// ##### Processes mission end conditions
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### This script was forked from FM_Mission_Controller.sc at the beginning of 2020 -------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 

USING "FM_Mission_Controller_Entities_2020.sch"
USING "FM_Mission_Controller_SuddenDeath_2020.sch"

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Team Failure ----------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Processes Failing Teams
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL IS_FAIL_REASON_TEAM_QUIT(eFailMissionEnum eFail)
	
	SWITCH eFail
		CASE mFail_HEIST_TEAM_GONE
		CASE mFail_HEIST_TEAM_GONE_T0
		CASE mFail_HEIST_TEAM_GONE_T1
		CASE mFail_HEIST_TEAM_GONE_T2
		CASE mFail_HEIST_TEAM_GONE_T3
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_FAIL_REASON_TEAM_DEATH(eFailMissionEnum eFail)
	
	SWITCH eFail
		CASE mFail_T0MEMBER_DIED
		CASE mFail_T1MEMBER_DIED
		CASE mFail_T2MEMBER_DIED
		CASE mFail_T3MEMBER_DIED
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_FAIL_REASON_PED_AGGROD(eFailMissionEnum eFail)
	
	SWITCH eFail
		CASE mFail_PED_AGRO_T0
		CASE mFail_PED_AGRO_T1
		CASE mFail_PED_AGRO_T2
		CASE mFail_PED_AGRO_T3
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL SHOULD_UPDATE_TEAM_FAIL_REASON(INT iTeam, eFailMissionEnum eFail, BOOL bForceChange)
	
	IF MC_serverBD.eCurrentTeamFail[iTeam] = mFail_none
		RETURN TRUE
	ENDIF
	
	IF IS_FAIL_REASON_TEAM_DEATH(eFail)
		RETURN TRUE
	ENDIF
	
	IF bForceChange
	AND NOT IS_FAIL_REASON_TEAM_DEATH(MC_serverBD.eCurrentTeamFail[iTeam]) //Do not force overwrite team death
		RETURN TRUE
	ENDIF
		
	RETURN FALSE
		
ENDFUNC

PROC SET_TEAM_FAIL_REASON(INT iTeam, eFailMissionEnum eFail, BOOL bForceChange = TRUE, INT iForcePartCauseFail = -1, INT iEntityCausingFail = -1)
	
	IF NOT SHOULD_UPDATE_TEAM_FAIL_REASON(iTeam, eFail, bForceChange)
		EXIT
	ENDIF
	
	PRINTLN("[MISSION_RESULTS] SET_TEAM_FAIL_REASON - Called with Team: ", iTeam)
	
	INT i	
	FOR i = 0 TO MC_serverBD.iNumberOfTeams - 1
		
		IF NOT DOES_TEAM_LIKE_TEAM(i, iTeam)
			RELOOP
		ENDIF

		IF MC_serverBD.iEntityCausingFail[i] = -1
		AND iEntityCausingFail != -1
			MC_serverBD.iEntityCausingFail[i] = iEntityCausingFail
			PRINTLN("[MISSION_RESULTS] SET_TEAM_FAIL_REASON - Team: ", i, " Entity Causing Fail: ", MC_serverBD.iEntityCausingFail[i])
		ENDIF
	
		IF iForcePartCauseFail != -1	
			MC_serverBD.iPartCausingFail[i] = iForcePartCauseFail
			PRINTLN("[MISSION_RESULTS] SET_TEAM_FAIL_REASON - Team: ", i, " Part Causing Fail: ", MC_serverBD.iPartCausingFail[i])
		ELIF IS_FAIL_REASON_TEAM_DEATH(eFail)
		OR IS_FAIL_REASON_PED_AGGROD(eFail)											
			IF MC_serverBD.iPartCausingFail[iTeam] != -1
				MC_serverBD.iPartCausingFail[i] = MC_serverBD.iPartCausingFail[iTeam]
				PRINTLN("[MISSION_RESULTS] SET_TEAM_FAIL_REASON - Team: ", i, " Part Causing Fail: ", MC_serverBD.iPartCausingFail[i], " (Death or Aggro)")
			ENDIF
		ENDIF
		
		IF IS_FAIL_REASON_TEAM_DEATH(eFail)
		OR IS_FAIL_REASON_TEAM_QUIT(eFail)
			MC_serverBD.eCurrentTeamFail[i] = eFail
			PRINTLN("[MISSION_RESULTS] SET_TEAM_FAIL_REASON - Team: ", i, " Fail Reason: ", MC_serverBD.eCurrentTeamFail[i], " (Death or Quit)")
		ELIF IS_FAIL_REASON_PED_AGGROD(eFail)
		AND MC_serverBD.iPartCausingFail[i] != -1
			MC_serverBD.eCurrentTeamFail[i] = INT_TO_ENUM(eFailMissionEnum, ENUM_TO_INT(mFail_PED_AGRO_T1) + MC_playerBD[MC_serverBD.iPartCausingFail[i]].iTeam)
			PRINTLN("[MISSION_RESULTS] SET_TEAM_FAIL_REASON - Team: ", i, " Fail Reason: ", MC_serverBD.eCurrentTeamFail[i], " (Ped Aggro)")
		ELIF eFail != MC_serverBD.eCurrentTeamFail[i] 
		AND i != iTeam
			MC_serverBD.eCurrentTeamFail[i] = INT_TO_ENUM(eFailMissionEnum, ENUM_TO_INT(mFail_HEIST_TEAM_T0_FAILED) + iTeam) 
			PRINTLN("[MISSION_RESULTS] SET_TEAM_FAIL_REASON - Team: ", i, " Fail Reason: ", MC_serverBD.eCurrentTeamFail[i], " (Friendly Team Failed)")
		ELSE
			MC_serverBD.eCurrentTeamFail[i] = eFail
			PRINTLN("[MISSION_RESULTS] SET_TEAM_FAIL_REASON - Team: ", i, " Fail Reason: ", MC_serverBD.eCurrentTeamFail[i])
		ENDIF
		
	ENDFOR		
	
ENDPROC

PROC SET_TEAM_FAILED(INT iTeam, eFailMissionEnum eFail = mFail_none, BOOL bForceReasonUpdate = TRUE, INT iPartCausingFail = -1, INT iEntityCausingFail = -1)
	
	DEBUG_PRINTCALLSTACK()
	PRINTLN("[MISSION_RESULTS] SET_TEAM_FAILED - Team: ", iTeam, " Fail Reason: ", ENUM_TO_INT(eFail), " Force Update: ", BOOL_TO_STRING(bForceReasonUpdate), " Part Causing Fail: ", iPartCausingFail, " Entity Causing Fail: ", iEntityCausingFail)
	
	IF IS_BIT_SET(iLocalBoolCheck11, LBOOL11_STOP_MISSION_FAILING_DUE_TO_EARLY_CELEBRATION)	
		PRINTLN("[MISSION_RESULTS] SET_TEAM_FAILED - Team: ", iTeam, " set to fail but someone is on an early celebration screen!")
		EXIT
	ENDIF
	
	SET_TEAM_FAIL_REASON(iTeam, eFail, bForceReasonUpdate, iPartCausingFail, iEntityCausingFail)
		
	IF MC_serverBD.iFailTeam = FMMC_MAX_TEAMS
		MC_serverBD.iFailTeam = iTeam
		PRINTLN("[MISSION_RESULTS] SET_TEAM_FAILED - Setting first team failed: ", MC_serverBD.iFailTeam)
	ENDIF
		
	IF IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
		EXIT
	ENDIF
			
	END_TEAM_MISSION_TIMER(iTeam)
	
	IF IS_BIT_SET(MC_serverBD.iServerBitSet, SSBOOL_TEAM0_FAILED + iTeam)
		EXIT
	ENDIF
	
	SET_BIT(MC_serverBD.iServerBitSet, SSBOOL_TEAM0_FINISHED + iTeam)	
	SET_BIT(MC_serverBD.iServerBitSet, SSBOOL_TEAM0_FAILED + iTeam)
	PRINTLN("[MISSION_RESULTS] SET_TEAM_FAILED - Setting team failed: ", iTeam)
	
ENDPROC

PROC SET_TEAM_FAILED_FROM_REQUEST(INT iTeam)
	
	PRINTLN("[MISSION_RESULTS] SET_TEAM_FAILED_FROM_REQUEST - Processing request for Team: ", iTeam, " Data: ", MC_serverBD.iTeamFailRequests[iTeam])
	
	IF NOT IS_BIT_SET(MC_serverBD.iTeamFailRequests[iTeam], ciSET_TEAM_FAIL_REQUEST_BS_ENABLED)
		ASSERTLN("[MISSION_RESULTS] SET_TEAM_FAILED_FROM_REQUEST - Team: ", iTeam, " Request has data but isn't enabled. Something has gone wrong.")
		EXIT
	ENDIF
	
	BOOL bForceReasonUpdate = IS_BIT_SET(MC_serverBD.iTeamFailRequests[iTeam], ciSET_TEAM_FAIL_REQUEST_BS_FORCE_UPDATE_REASON)
	
	INT iRequestData = SHIFT_RIGHT(MC_serverBD.iTeamFailRequests[iTeam], ciSET_TEAM_FAIL_REQUEST_SHIFT)

	eFailMissionEnum mFail = INT_TO_ENUM(eFailMissionEnum, GET_PACKED_BITFILED_VALUE(iRequestData, ciSET_TEAM_FAIL_REQUEST_FAIL_REASON, ciSET_TEAM_FAIL_BIT_WIDTH))
	
	INT iPartCausingFail = GET_PACKED_BITFILED_VALUE(iRequestData, ciSET_TEAM_FAIL_REQUEST_FAIL_PART, ciSET_TEAM_FAIL_BIT_WIDTH)
	iPartCausingFail = PICK_INT(iPartCausingFail = ciSET_TEAM_FAIL_REQUEST_INVALID, -1, iPartCausingFail)
	
	INT iEntityCausingFail = GET_PACKED_BITFILED_VALUE(iRequestData, ciSET_TEAM_FAIL_REQUEST_FAIL_ENTITY, ciSET_TEAM_FAIL_BIT_WIDTH)
	iEntityCausingFail = PICK_INT(iEntityCausingFail = ciSET_TEAM_FAIL_REQUEST_INVALID, -1, iEntityCausingFail)
		
	SET_TEAM_FAILED(iTeam, mFail, bForceReasonUpdate, iPartCausingFail, iEntityCausingFail)
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Restart Checkpoint Rules ----------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Functions used for setting up the first rule on a quick restart.
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC INVALIDATE_PREVIOUS_CUTSCENE_RULES(INT iTeam)

	INT iPriorityToCheck = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	PRINTLN("[INIT_SERVER] INVALIDATE_PREVIOUS_CUTSCENE_RULES - Called for Team: ", iTeam, " Checking Rule: ", iPriorityToCheck)

	IF iPriorityToCheck >= FMMC_MAX_RULES
		EXIT
	ENDIF

	INT iPlayerRule = 0
	FOR iPlayerRule = 0 TO FMMC_MAX_RULES - 1
	
		IF MC_serverBD_4.iPlayerRulePriority[iPlayerRule][iTeam] >= iPriorityToCheck
			RELOOP
		ENDIF
		
		IF MC_serverBD_4.iPlayerRule[iPlayerRule][iTeam] != FMMC_OBJECTIVE_LOGIC_SCRIPTED_CUTSCENE
			RELOOP
		ENDIF

		IF MC_serverBD.iPlayerRuleLimit[iPlayerRule][iTeam] < FMMC_GET_MAX_SCRIPTED_CUTSCENES()
		OR MC_serverBD.iPlayerRuleLimit[iPlayerRule][iTeam] >= FMMC_GET_MAX_CUTSCENES_WITH_MOCAP()
			RELOOP
		ENDIF
		
		IF IS_BIT_SET(MC_serverBD.iCutsceneFinished[iTeam], MC_serverBD_4.iPlayerRulePriority[iPlayerRule][iTeam])
			RELOOP
		ENDIF
		
		INT iMocap = MC_serverBD.iPlayerRuleLimit[iPlayerRule][iTeam] - FMMC_GET_MAX_SCRIPTED_CUTSCENES()
		SET_BIT(MC_serverBD.iCutsceneFinished[iTeam], MC_serverBD_4.iPlayerRulePriority[iPlayerRule][iTeam])
		PRINTLN("[INIT_SERVER] INVALIDATE_PREVIOUS_CUTSCENE_RULES - Invalidating Rule: ", MC_serverBD_4.iPlayerRulePriority[iPlayerRule][iTeam], " Mocap: ", iMocap)
	
	ENDFOR
	
ENDPROC

PROC REVALIDATE_NEXT_CUTSCENE_RULES(INT iTeam)
	
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	IF iRule >= FMMC_MAX_RULES
		EXIT
	ENDIF
		
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFourteen[iRule], ciBS_RULE14_REVALIDATE_CUTSCENES_AFTER_THIS_RULE)
		EXIT
	ENDIF
	
	PRINTLN("REVALIDATE_NEXT_CUTSCENE_RULES - Called for Team: ", iTeam, " Checking Rule: ", iRule)

	FOR iRule = 0 TO FMMC_MAX_RULES - 1
					
		IF MC_serverBD_4.iPlayerRule[iRule][iTeam] != FMMC_OBJECTIVE_LOGIC_SCRIPTED_CUTSCENE
			RELOOP
		ENDIF
		
		PRINTLN("REVALIDATE_NEXT_CUTSCENE_RULES - Revalidating Cutscene on Rule Index: ", iRule, " Bit: ", MC_serverBD_4.iPlayerRulePriority[iRule][iTeam])		
		
		IF bIsLocalPlayerHost
			IF IS_BIT_SET(MC_serverBD.iCutsceneFinished[iTeam], iRule)
				PRINTLN("REVALIDATE_NEXT_CUTSCENE_RULES - Clearing iCutsceneFinished[", iTeam, "] on Rule Index: ", iRule)
				CLEAR_BIT(MC_serverBD.iCutsceneFinished[iTeam], iRule)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(MC_playerBD[iLocalPart].iCutsceneFinishedBitset, iRule)
			PRINTLN("REVALIDATE_NEXT_CUTSCENE_RULES - Clearing iCutsceneFinishedBitset on Rule Index: ", iRule)
			CLEAR_BIT(MC_playerBD[iLocalPart].iCutsceneFinishedBitset, iRule)	
		ENDIF
		
	ENDFOR
	
ENDPROC

PROC CHECK_IF_NEXT_RULE_IS_CUTSCENE_RULE_FOR_ANY_FRIENDLY_TEAM(INT iTeam)
	
	INVALIDATE_PREVIOUS_CUTSCENE_RULES(iTeam)
	
	REVALIDATE_NEXT_CUTSCENE_RULES(iTeam)
	
	INT iPriorityToCheck = MC_serverBD_4.iCurrentHighestPriority[iTeam] + 1

	IF MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThirteen[MC_serverBD_4.iCurrentHighestPriority[iTeam]], ciBS_RULE13_DO_NOT_PRE_STREAM_MOCAPS)
			PRINTLN("[INIT_SERVER] CHECK_IF_NEXT_RULE_IS_CUTSCENE_RULE_FOR_ANY_FRIENDLY_TEAM - Not Pre-streaming")
			iPriorityToCheck = MC_serverBD_4.iCurrentHighestPriority[iTeam]
		ENDIF
		
		IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iSpecificPrestreamRule[MC_serverBD_4.iCurrentHighestPriority[iTeam]] > -1
			iPriorityToCheck = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iSpecificPrestreamRule[MC_serverBD_4.iCurrentHighestPriority[iTeam]]
			PRINTLN("[INIT_SERVER] CHECK_IF_NEXT_RULE_IS_CUTSCENE_RULE_FOR_ANY_FRIENDLY_TEAM - Using set pre-streaming rule: ", iPriorityToCheck)
		ENDIF
	ENDIF
	
	IF iPriorityToCheck >= FMMC_MAX_RULES
		EXIT
	ENDIF
	
	PRINTLN("[INIT_SERVER] CHECK_IF_NEXT_RULE_IS_CUTSCENE_RULE_FOR_ANY_FRIENDLY_TEAM - Checking rule: ", iPriorityToCheck)

	INT iPlayerRule = 0
	FOR iPlayerRule = 0 TO FMMC_MAX_RULES - 1
	
		IF MC_serverBD_4.iPlayerRulePriority[iPlayerRule][iTeam] > iPriorityToCheck
			PRINTLN("[INIT_SERVER_SPAM] CHECK_IF_NEXT_RULE_IS_CUTSCENE_RULE_FOR_ANY_FRIENDLY_TEAM - RELOOPING - Player rule ", iPlayerRule, "'s priority (", MC_serverBD_4.iPlayerRulePriority[iPlayerRule][iTeam], ") is too high!")
			RELOOP
		ENDIF
	
		IF MC_serverBD_4.iPlayerRule[iPlayerRule][iTeam] != FMMC_OBJECTIVE_LOGIC_SCRIPTED_CUTSCENE
			PRINTLN("[INIT_SERVER_SPAM] CHECK_IF_NEXT_RULE_IS_CUTSCENE_RULE_FOR_ANY_FRIENDLY_TEAM - RELOOPING - Player rule ", iPlayerRule, " isn't a cutscene!")
			RELOOP
		ENDIF

		IF  MC_serverBD.iPlayerRuleLimit[iPlayerRule][iTeam] < FMMC_GET_MAX_SCRIPTED_CUTSCENES()
		OR MC_serverBD.iPlayerRuleLimit[iPlayerRule][iTeam] >= FMMC_GET_MAX_CUTSCENES_WITH_MOCAP()
			PRINTLN("[INIT_SERVER_SPAM] CHECK_IF_NEXT_RULE_IS_CUTSCENE_RULE_FOR_ANY_FRIENDLY_TEAM - RELOOPING - Player rule ", iPlayerRule, "'s iPlayerRuleLimit is invalid! MC_serverBD.iPlayerRuleLimit[", iPlayerRule, "][", iTeam, "]:", MC_serverBD.iPlayerRuleLimit[iPlayerRule][iTeam])
			RELOOP
		ENDIF
		
		IF IS_BIT_SET(MC_serverBD.iCutsceneFinished[iTeam], MC_serverBD_4.iPlayerRulePriority[iPlayerRule][iTeam])
			PRINTLN("[INIT_SERVER_SPAM] CHECK_IF_NEXT_RULE_IS_CUTSCENE_RULE_FOR_ANY_FRIENDLY_TEAM - RELOOPING - iCutsceneFinished is already set for Player rule ", iPlayerRule)
			RELOOP
		ENDIF
		
		INT iMocap = MC_serverBD.iPlayerRuleLimit[iPlayerRule][iTeam] - FMMC_GET_MAX_SCRIPTED_CUTSCENES()
						
		IF IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sMocapCutsceneData[iMocap].tlName)
		OR MC_serverBD.iCutsceneStreamingIndex[iTeam] != -1
			PRINTLN("[INIT_SERVER_SPAM] CHECK_IF_NEXT_RULE_IS_CUTSCENE_RULE_FOR_ANY_FRIENDLY_TEAM - RELOOPING - Player rule ", iPlayerRule, " || g_FMMC_STRUCT.sMocapCutsceneData[iMocap].tlName: ", g_FMMC_STRUCT.sMocapCutsceneData[iMocap].tlName, " / MC_serverBD.iCutsceneStreamingIndex[", iTeam, "]: ", MC_serverBD.iCutsceneStreamingIndex[iTeam])
			RELOOP
		ENDIF
		
		MC_serverBD.iCutsceneStreamingIndex[iTeam] = MC_serverBD.iPlayerRuleLimit[iPlayerRule][iTeam]
		
		PRINTLN("[INIT_SERVER] CHECK_IF_NEXT_RULE_IS_CUTSCENE_RULE_FOR_ANY_FRIENDLY_TEAM - Pre-streaming mocap: ", g_FMMC_STRUCT.sMocapCutsceneData[iMocap].tlName, " Index: ", MC_serverBD.iCutsceneStreamingIndex[iTeam])
		
		BREAKLOOP
		
	ENDFOR
ENDPROC

PROC SETUP_RULES_FOR_CHECKPOINT(INT iTeam, INT iCheckpoint)
	
	INT iRestartRule = g_FMMC_Struct.iRestartRule[iTeam][iCheckpoint]
	
	PRINTLN("[INIT_SERVER] SETUP_RULES_FOR_CHECKPOINT - Restart Rule: ", iRestartRule)
	
	IF iRestartRule <= 0	
		EXIT
	ENDIF
	
	INVALIDATE_UP_TO_AND_INCLUDING_OBJECTIVE(iTeam, iRestartRule - 1, TRUE, TRUE) //-1 so we start on the objective set in creator
	
	MC_serverBD_4.iCurrentHighestPriority[iTeam] = iRestartRule		
	
	SET_PROGRESS_OBJECTIVE_FOR_TEAM(iteam, FALSE)		
	
	CHECK_IF_NEXT_RULE_IS_CUTSCENE_RULE_FOR_ANY_FRIENDLY_TEAM(iteam)
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Scoring ----------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: 
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL SHOULD_INCREMENT_SCORE_FOR_THIS_RULE(INT iTeam, INT iPriority)
	
	IF g_FMMC_STRUCT.iMissionEndType = ciMISSION_SCORING_TYPE_TIME
		RETURN FALSE
	ENDIF
	
	IF iPriority >= FMMC_MAX_RULES
		RETURN FALSE
	ENDIF
	
	IF iTeam >= FMMC_MAX_TEAMS
		RETURN FALSE
	ENDIF
	
	IF iPriority != MC_serverBD_4.iCurrentHighestPriority[iTeam]
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

PROC INCREMENT_SERVER_REMOTE_PLAYER_SCORE(PLAYER_INDEX piPlayer, INT iScore, INT iTeam, INT iPriority)
	
	DEBUG_PRINTCALLSTACK()
	PRINTLN("[SCORE] INCREMENT_SERVER_REMOTE_PLAYER_SCORE - Player: ", NATIVE_TO_INT(piPlayer), ", Score: ", iScore, ", Team: ", iTeam, ", Priority: ", iPriority)
	
	IF SHOULD_INCREMENT_SCORE_FOR_THIS_RULE(iTeam, iPriority)
		MC_serverBD.iScoreOnThisRule[iTeam] += iScore
		PRINTLN("[SCORE] INCREMENT_SERVER_REMOTE_PLAYER_SCORE - Score on This Rule updated to ", MC_serverBD.iScoreOnThisRule[iTeam])
	ENDIF
	
	BROADCAST_FMMC_INCREASE_REMOTE_PLAYER_SCORE(piPlayer, iScore, MC_serverBD.iSessionScriptEventKey, GET_FRAME_COUNT())
	
ENDPROC

PROC INCREMENT_SERVER_REMOTE_PLAYER_SCORE_FROM_REQUEST(INT iRequest)
	
	PRINTLN("[SCORE] INCREMENT_SERVER_REMOTE_PLAYER_SCORE_FROM_REQUEST - Processing request ", iRequest," Data: ", MC_serverBD.iIncrementRemotePlayerScoreRequests[iRequest])
	
	INT iParticipant = GET_PACKED_BITFILED_VALUE(MC_serverBD.iIncrementRemotePlayerScoreRequests[iRequest], ciSERVER_INCREMENT_SCORE_PARTICIPANT, ciSERVER_INCREMENT_SCORE_BIT_WIDTH)
	
	INT iScore = GET_PACKED_BITFILED_VALUE(MC_serverBD.iIncrementRemotePlayerScoreRequests[iRequest], ciSERVER_INCREMENT_SCORE_SCORE, ciSERVER_INCREMENT_SCORE_BIT_WIDTH)
	IF IS_BIT_SET(MC_serverBD.iIncrementRemotePlayerScoreRequests[iRequest], ciSERVER_INCREMENT_SCORE_NEGATIVE)
		iScore *= -1
		CLEAR_BIT(MC_serverBD.iIncrementRemotePlayerScoreRequests[iRequest], ciSERVER_INCREMENT_SCORE_NEGATIVE)
	ENDIF
	
	INT iTeam = GET_PACKED_BITFILED_VALUE(MC_serverBD.iIncrementRemotePlayerScoreRequests[iRequest], ciSERVER_INCREMENT_SCORE_TEAM, ciSERVER_INCREMENT_SCORE_BIT_WIDTH)
	
	INT iPriority = GET_PACKED_BITFILED_VALUE(MC_serverBD.iIncrementRemotePlayerScoreRequests[iRequest], ciSERVER_INCREMENT_SCORE_PRIORITY, ciSERVER_INCREMENT_SCORE_BIT_WIDTH)
	iPriority = PICK_INT(iPriority = ciSERVER_INCREMENT_SCORE_MAX, FMMC_PRIORITY_IGNORE, iPriority)
	
	PRINTLN("[SCORE] INCREMENT_SERVER_REMOTE_PLAYER_SCORE_FROM_REQUEST - Participant: ", iParticipant, ", Score: ", iScore, ",  Team: ", iTeam, ", Priority: ", iPriority) 
	
	PARTICIPANT_INDEX piParticipant
	piParticipant = INT_TO_PARTICIPANTINDEX(iParticipant)
	
	IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(piParticipant)
		ASSERTLN("[SCORE] INCREMENT_SERVER_REMOTE_PLAYER_SCORE_FROM_REQUEST - FAILED: Invalid Participant")
		EXIT
	ENDIF
	
	PLAYER_INDEX piPlayer = NETWORK_GET_PLAYER_INDEX(piParticipant)	
	
	INCREMENT_SERVER_REMOTE_PLAYER_SCORE(piPlayer, iScore, iTeam, iPriority)
	
ENDPROC

PROC INCREMENT_LOCAL_PLAYER_SCORE_FOR_CURRENT_OBJECTIVE(INT iMultiplier = 1, BOOL bKillPoints = FALSE, INT iVictimPart = -1, INT iIdentifier = -1)
	
	DEBUG_PRINTCALLSTACK()
	PRINTLN("[SCORE] INCREMENT_LOCAL_PLAYER_SCORE_FOR_CURRENT_OBJECTIVE - Multiplier: ", iMultiplier, ", Kill Points: ", BOOL_TO_STRING(bKillPoints), ", Victim Part: ", iVictimPart, ", Identifier: ", iIdentifier)
	
	IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam] >= FMMC_MAX_RULES
		EXIT
	ENDIF
	
	IF IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
		EXIT
	ENDIF
	
	IF GET_FMMC_POINTS_FOR_TEAM(MC_playerBD[iLocalPart].iteam) <= 0
		EXIT
	ENDIF
	
	INT iTeam = MC_playerBD[iLocalPart].iTeam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iteam]
	INT iScore = GET_FMMC_POINTS_FOR_TEAM(MC_playerBD[iLocalPart].iTeam) * iMultiplier
	
	IF CHECK_SUICIDE_DECREMENT_DOES_NOT_EXCEED_PREV_RULE(iRule, iTeam, iMultiplier)
		PRINTLN("[SCORE] INCREMENT_LOCAL_PLAYER_SCORE_FOR_CURRENT_OBJECTIVE - Can't decrement score as it exceeds previous rule's score") 
		EXIT
	ENDIF
	
	IF g_FMMC_STRUCT.iMissionEndType = ciMISSION_SCORING_TYPE_TIME
		PRINTLN("[SCORE] INCREMENT_LOCAL_PLAYER_SCORE_FOR_CURRENT_OBJECTIVE - Updating timer based on score: ", iScore)
		MC_PlayerBD[iLocalPart].tdMissionTime.Timer = GET_TIME_OFFSET(MC_playerBD[iLocalPart].tdMissionTime.Timer, (ci_TIME_PER_POINT * 1000 * iMultiplier * GET_FMMC_POINTS_FOR_TEAM(MC_playerBD[iLocalPart].iteam)))
		EXIT
	ENDIF
	
	MC_playerBD[iLocalPart].iPlayerScore += iScore
	PRINTLN("[SCORE] INCREMENT_LOCAL_PLAYER_SCORE_FOR_CURRENT_OBJECTIVE - Updating Player Score by: ", iScore, ". New Score: ", MC_playerBD[iLocalPart].iPlayerScore)
			
	IF bKillPoints
		MC_playerBD[iLocalPart].iKillScore += iScore
		PRINTLN("[SCORE] INCREMENT_LOCAL_PLAYER_SCORE_FOR_CURRENT_OBJECTIVE - Updating Player Kill Score by: ", iScore, ". New Score: ", MC_playerBD[iLocalPart].iKillScore)
	ENDIF
				
	BROADCAST_FMMC_INCREMENTED_LOCAL_PLAYER_SCORE(iTeam, iRule, iScore, iVictimPart, iIdentifier)
	
ENDPROC

PROC INCREMENT_LOCAL_PLAYER_SCORE_BY(INT iScore, BOOL bKillPoints = FALSE, INT iIdentifier = -1)
	
	DEBUG_PRINTCALLSTACK()
	PRINTLN("[SCORE] INCREMENT_LOCAL_PLAYER_SCORE_BY - Score: ", iScore, ", Kill Points: ", BOOL_TO_STRING(bKillPoints), ", Identifier: ", iIdentifier)
	
	IF CHECK_SUICIDE_DECREMENT_DOES_NOT_EXCEED_PREV_RULE(MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam], MC_playerBD[iLocalPart].iteam, DEFAULT, iScore)
		PRINTLN("[SCORE] INCREMENT_LOCAL_PLAYER_SCORE_BY - Can't decrement score as it exceeds previous rule's score") 
		EXIT
	ENDIF
	
	IF g_FMMC_STRUCT.iMissionEndType = ciMISSION_SCORING_TYPE_TIME
		PRINTLN("[SCORE] INCREMENT_LOCAL_PLAYER_SCORE_BY - Updating timer based on score: ", iScore)
		MC_PlayerBD[iLocalPart].tdMissionTime.Timer = GET_TIME_OFFSET(MC_playerBD[iLocalPart].tdMissionTime.Timer, (ci_TIME_PER_POINT * 1000 * iScore))
		EXIT
	ENDIF
	
	MC_playerBD[iLocalPart].iPlayerScore += iScore
	PRINTLN("[SCORE] INCREMENT_LOCAL_PLAYER_SCORE_BY - Updating Player Score by: ", iScore, ". New Score: ", MC_playerBD[iLocalPart].iPlayerScore)
			
	IF bKillPoints
		MC_playerBD[iLocalPart].iKillScore += iScore
		PRINTLN("[SCORE] INCREMENT_LOCAL_PLAYER_SCORE_BY - Updating Player Kill Score by: ", iScore, ". New Score: ", MC_playerBD[iLocalPart].iKillScore)
	ENDIF
	
	BROADCAST_FMMC_INCREMENTED_LOCAL_PLAYER_SCORE(MC_playerBD[iLocalPart].iTeam, FMMC_PRIORITY_IGNORE, iScore, DEFAULT, iIdentifier)
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Rule Progression and Skipping ----------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: 
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL SHOULD_PROGRESS_OBJECTIVE_FOR_TEAM(INT iTeam)

	RETURN IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_PROGRESS_OBJECTIVE_FOR_TEAM_0 + iTeam)
	
ENDFUNC

PROC SERVER_PROCESS_TEAM_MIDPOINTS_FOR_OTHER_TEAMS(INT iTeam)
	
	IF MC_serverBD_4.iCurrentHighestPriority[iTeam] >= FMMC_MAX_RULES
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iTeam], MC_serverBD_4.iCurrentHighestPriority[iTeam])
		EXIT
	ENDIF
	
	INT iOtherTeam, iOtherRule, iArray
	FOR iOtherTeam = 0 TO MC_serverBD.iNumberOfTeams - 1
		IF iOtherTeam = iTeam
			RELOOP
		ENDIF
			
		IF iOtherTeam > iteam
			iArray = iOtherTeam - 1
		ELSE
			iArray = iOtherTeam
		ENDIF
			
		iOtherRule = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjectiveMidPointProgressOtherTeamLogic[MC_serverBD_4.iCurrentHighestPriority[iTeam]][iArray]
			
		IF iOtherRule >= 0
		AND iOtherRule < FMMC_MAX_RULES
		AND NOT IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iOtherTeam], iOtherRule)
			PRINTLN("[OBJECTIVE_PROGRESSION] SERVER_PROCESS_TEAM_MIDPOINTS_DUE_TO_OTHER_TEAMS - Setting midpoint iObjectiveMidPointBitset for team ", iOtherTeam, " rule ", iOtherRule, ", as other team ", iTeam, " has hit midpoint of rule ", MC_serverBD_4.iCurrentHighestPriority[iTeam])
			SET_BIT(MC_serverBD.iObjectiveMidPointBitset[iOtherTeam], iOtherRule)
			SET_BIT(iLocalBoolCheck11, LBOOL11_HOST_APPLIED_NEW_MIDPOINT_MIDFRAME)
		ENDIF
	ENDFOR
	
ENDPROC

PROC PROCESS_SINGLE_FRAME_SERVER_LOGIC_LOOP(INT iTeam)

	PRINTLN("[OBJECTIVE_PROGRESSION] PROCESS_SINGLE_FRAME_SERVER_LOGIC_LOOP - Called on Team ", iTeam)
	
	PROCESS_SERVER_PRE_OBJECTIVE_ENTITY_STAGGERED_LOOP_FOR_TEAM(iTeam)
	
	FMMC_OBJECT_STATE sObjState
	
	INT i		
	FOR i = 0 TO MC_serverBD.iMaxLoopSize - 1
		
		IF MC_serverBD.iNumPlayerRuleCreated > 0
		AND MC_serverBD.iNumPlayerRuleCreated <= FMMC_MAX_RULES
		AND i < MC_serverBD.iNumPlayerRuleCreated
			PROCESS_PLAYER_OBJECTIVES_SERVER_FOR_TEAM(i, iTeam)
		ENDIF
		
		IF MC_serverBD.iNumLocCreated > 0
		AND MC_serverBD.iNumLocCreated <= FMMC_MAX_GO_TO_LOCATIONS
		AND i < MC_serverBD.iNumLocCreated
			PROCESS_LOCATION_OBJECTIVES_SERVER(i, iTeam)
		ENDIF
		
		IF MC_serverBD.iNumPedCreated > 0
		AND MC_serverBD.iNumPedCreated <= FMMC_MAX_PEDS
		AND i < MC_serverBD.iNumPedCreated	
			PROCESS_PED_OBJECTIVES_SERVER(i, iTeam)
		ENDIF
		
		IF MC_serverBD.iNumVehCreated > 0
		AND MC_serverBD.iNumVehCreated <= FMMC_MAX_VEHICLES
		AND i < MC_serverBD.iNumVehCreated	
			PROCESS_VEH_OBJECTIVES_SERVER(i, iTeam)
		ENDIF
		
		IF MC_serverBD.iNumTrainCreated > 0
		AND MC_serverBD.iNumTrainCreated <= FMMC_MAX_TRAINS
		AND i < MC_serverBD.iNumTrainCreated	
			PROCESS_TRAIN_OBJECTIVES_SERVER(i, iTeam)
		ENDIF
		
		IF MC_serverBD.iNumObjCreated > 0	
		AND MC_serverBD.iNumObjCreated <= FMMC_MAX_NUM_OBJECTS
		AND i < MC_serverBD.iNumObjCreated
			FILL_FMMC_OBJECT_STATE_STRUCT(sObjState, i, TRUE)
			PROCESS_OBJ_OBJECTIVES_SERVER(sObjState, iTeam)
		ENDIF
		
	ENDFOR
	
	PROCESS_SERVER_POST_OBJECTIVE_ENTITY_STAGGERED_LOOP_FOR_TEAM(iTeam)
	
	SET_BIT(MC_serverBD.iServerBitSet, SBBOOL_SKIP_TEAM_0_UPDATE + iTeam)
	
ENDPROC

PROC RECALCULATE_OBJECTIVE_LOGIC(INT iTeam, INT iDepth = 0)
	
	IF IS_FAKE_MULTIPLAYER_MODE_SET()
	AND iTeam != MC_playerBD[iLocalPart].iTeam
		//Don't move on other teams' objectives in test mode
		EXIT
	ENDIF
	
	DEBUG_PRINTCALLSTACK()
	PRINTLN("[OBJECTIVE_PROGRESSION] RECALCULATE_OBJECTIVE_LOGIC - Called on Team ", iTeam, ", Current Highest Priority: ", MC_serverBD_4.iCurrentHighestPriority[iTeam], " Depth: ", iDepth)
	
	INT iOldHighestPriority = MC_ServerBD_4.iCurrentHighestPriority[iTeam]
	
	PROCESS_SINGLE_FRAME_SERVER_LOGIC_LOOP(iTeam)
	
	PRINTLN("[OBJECTIVE_PROGRESSION] RECALCULATE_OBJECTIVE_LOGIC - Team: ", iTeam, " Old Highest Priority: ", iOldHighestPriority, " Current Highest Priority: ", MC_serverBD_4.iCurrentHighestPriority[iTeam])
	
	IF iOldHighestPriority = MC_serverBD_4.iCurrentHighestPriority[iTeam]
		EXIT
	ENDIF
		
	SET_BIT(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam)
		
	IF iOldHighestPriority < 0
	OR iOldHighestPriority >= FMMC_MAX_RULES
		EXIT
	ENDIF
	
	IF iDepth != 0
		EXIT
	ENDIF

	INT iNewRule = GET_RANDOM_RULE_TO_MOVE_TO_FOR_TEAM(iTeam, iOldHighestPriority)
		
	INT iJumpToRule = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iConditionalObjectiveJumpRule[iOldHighestPriority]					
	IF iJumpToRule != -1
		//FMMC2020 - Could extend thsi to get rid of hard coded teams
		INT iScoreCondition = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iConditionalObjectiveJumpCondition[iOldHighestPriority]
		INT iScoreDiff = ABSI(MC_serverBD.iTeamScore[0] - MC_serverBD.iTeamScore[1]) 
		IF iScoreCondition = 0
		AND iScoreDiff = 0
			iNewRule = iJumpToRule
			SET_BIT(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH)
			SET_BIT(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_KILLALLENEMIES_PENNED_IN)
		ELSE
			INT iRoundsLeft = iScoreCondition - MC_ServerBD_4.iCurrentHighestPriority[iTeam]
			IF iRoundsLeft + MC_serverBD.iTeamScore[0] < MC_serverBD.iTeamScore[1]
			OR iRoundsLeft + MC_serverBD.iTeamScore[1] < MC_serverBD.iTeamScore[0]
				iNewRule = iJumpToRule
			ENDIF
		ENDIF
		PRINTLN("[OBJECTIVE_PROGRESSION] RECALCULATE_OBJECTIVE_LOGIC - team0: ", MC_serverBD.iTeamScore[0], " team1: ", MC_serverBD.iTeamScore[1], " team: ", iTeam, " score condition: ", iScoreCondition, " current round: ", MC_ServerBD_4.iCurrentHighestPriority[iTeam])
	ENDIF
		
	IF iNewRule < FMMC_MAX_RULES AND iNewRule > -1
		IF iNewRule > MC_ServerBD_4.iCurrentHighestPriority[iTeam]
			PRINTLN("[OBJECTIVE_PROGRESSION] RECALCULATE_OBJECTIVE_LOGIC - Picked new rule for team ", iTeam, " just finishing objective: ", iOldHighestPriority, " new rule = ", iNewRule)
			INVALIDATE_UP_TO_AND_INCLUDING_OBJECTIVE(iTeam, iNewRule - 1)
			RECALCULATE_OBJECTIVE_LOGIC(iTeam, iDepth + 1)
		ELIF iNewRule < MC_ServerBD_4.iCurrentHighestPriority[iTeam]
			PRINTLN("[OBJECTIVE_PROGRESSION] RECALCULATE_OBJECTIVE_LOGIC - Picked new rule for team ", iTeam, " just finishing objective: ", iOldHighestPriority, " new rule (RETURNING TO AN OLDER RULE!) = ", iNewRule)
			INVALIDATE_UP_TO_AND_INCLUDING_OBJECTIVE(iTeam, iNewRule, DEFAULT, DEFAULT, TRUE)
			RECALCULATE_OBJECTIVE_LOGIC(iTeam, iDepth + 1)
		#IF IS_DEBUG_BUILD
		ELSE
			PRINTLN("[OBJECTIVE_PROGRESSION] RECALCULATE_OBJECTIVE_LOGIC - Failed to apply new rule for team ", iTeam, " just finishing objective: ", iOldHighestPriority, " new unapplied rule = ", iNewRule, ", current rule = ", MC_serverBD_4.iCurrentHighestPriority[iTeam])
		#ENDIF
		ENDIF
	ENDIF
		
	PRINTLN("OBJECTIVE_PROGRESSION] RECALCULATE_OBJECTIVE_LOGIC - Finished for Team ", iTeam, ", new objective ", MC_serverBD_4.iCurrentHighestPriority[iTeam])
	
ENDPROC

FUNC INT GET_OBJECTIVE_TO_PROGRESS_TO_FOR_TEAM(INT iTeam, BOOL &bPreviousObjective)
	
	bPreviousObjective = FALSE
	
	IF MC_serverBD.iNextObjective[iTeam] >= 0
		IF MC_serverBD.iNextObjective[iTeam] < MC_serverBD_4.iCurrentHighestPriority[iTeam]
			bPreviousObjective = TRUE
			RETURN MC_serverBD.iNextObjective[iTeam]
		ELIF MC_serverBD.iNextObjective[iTeam] > 0
			RETURN MC_serverBD.iNextObjective[iTeam] - 1
		ENDIF
	ENDIF
	
	RETURN MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
ENDFUNC

PROC SERVER_PROCESS_TEAM_PROGRESSION_DUE_TO_OTHER_TEAMS(INT iTeam)
	
	IF MC_serverBD_4.iCurrentHighestPriority[iTeam] >= FMMC_MAX_RULES
		EXIT
	ENDIF
	
	INT iOtherTeam
	
	FOR iOtherTeam = 0 TO MC_serverBD.iNumberOfTeams - 1
		IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjectiveProgressionLogic[MC_serverBD_4.iCurrentHighestPriority[iTeam]][iOtherTeam] = -1
			PRINTLN("[OBJECTIVE_PROGRESSION] SERVER_PROCESS_TEAM_PROGRESSION_DUE_TO_OTHER_TEAMS - First else iObjectiveProgressionLogic = -1")
			PRINTLN("[OBJECTIVE_PROGRESSION] SERVER_PROCESS_TEAM_PROGRESSION_DUE_TO_OTHER_TEAMS - Other team: ", iOtherTeam, " other team rule: ", MC_serverBD_4.iCurrentHighestPriority[iOtherTeam])
			RELOOP
		ENDIF
			
		PRINTLN("[OBJECTIVE_PROGRESSION] SERVER_PROCESS_TEAM_PROGRESSION_DUE_TO_OTHER_TEAMS - Team: ", iTeam, " Current Rule: ", MC_serverBD_4.iCurrentHighestPriority[iTeam], ". Other Team: ", iOtherTeam, " current rule: ", MC_serverBD_4.iCurrentHighestPriority[iTeam])
		
		IF MC_serverBD_4.iCurrentHighestPriority[iOtherTeam] < g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjectiveProgressionLogic[MC_serverBD_4.iCurrentHighestPriority[iTeam]][iOtherTeam]
			RELOOP
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjectiveProgMidPointBitset[MC_serverBD_4.iCurrentHighestPriority[iTeam]], ciObjProgBS_MoveOnTeamMidpoint_T0 + iOtherTeam)
			IF MC_serverBD_4.iCurrentHighestPriority[iOtherTeam] < FMMC_MAX_RULES
				IF IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iOtherTeam],MC_serverBD_4.iCurrentHighestPriority[iOtherTeam])
				OR ((MC_serverBD_4.iCurrentHighestPriority[iOtherTeam] > g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjectiveProgressionLogic[MC_serverBD_4.iCurrentHighestPriority[iTeam]][iOtherTeam])
					 AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjectiveProgMidPointBitset[MC_serverBD_4.iCurrentHighestPriority[iTeam]], ciObjProgBS_MoveOnlyWhenTeamOnSpecificRule_T0 + iOtherTeam) )
					PRINTLN("[OBJECTIVE_PROGRESSION] SERVER_PROCESS_TEAM_PROGRESSION_DUE_TO_OTHER_TEAMS - Team ", iTeam, " should progress objective ",MC_serverBD_4.iCurrentHighestPriority[iTeam], ", due to other team ",iOtherTeam," passing objective ",g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjectiveProgressionLogic[MC_serverBD_4.iCurrentHighestPriority[iTeam]][iOtherTeam]," + midpoint - iOtherTeam's iCurrentHighestPriority = ",MC_serverBD_4.iCurrentHighestPriority[iOtherTeam])
					SET_PROGRESS_OBJECTIVE_FOR_TEAM(iTeam, TRUE)
				ENDIF
			ELIF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjectiveProgMidPointBitset[MC_serverBD_4.iCurrentHighestPriority[iTeam]], ciObjProgBS_MoveOnlyWhenTeamOnSpecificRule_T0 + iOtherTeam)
				PRINTLN("[OBJECTIVE_PROGRESSION] SERVER_PROCESS_TEAM_PROGRESSION_DUE_TO_OTHER_TEAMS - Team ", iTeam, " should progress objective ",MC_serverBD_4.iCurrentHighestPriority[iTeam], ", due to other team ",iOtherTeam," passing objective ",g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjectiveProgressionLogic[MC_serverBD_4.iCurrentHighestPriority[iTeam]][iOtherTeam]," + midpoint - iOtherTeam's iCurrentHighestPriority = ",MC_serverBD_4.iCurrentHighestPriority[iOtherTeam])
				SET_PROGRESS_OBJECTIVE_FOR_TEAM(iTeam, TRUE)
			ENDIF
		ELSE
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjectiveProgMidPointBitset[MC_serverBD_4.iCurrentHighestPriority[iTeam]], ciObjProgBS_MoveOnlyWhenTeamOnSpecificRule_T0 + iOtherTeam)
				PRINTLN("[OBJECTIVE_PROGRESSION] SERVER_PROCESS_TEAM_PROGRESSION_DUE_TO_OTHER_TEAMS - Team ", iTeam, " should progress objective ",MC_serverBD_4.iCurrentHighestPriority[iTeam], ", due to other team ",iOtherTeam," reaching objective ",g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjectiveProgressionLogic[MC_serverBD_4.iCurrentHighestPriority[iTeam]][iOtherTeam]," - iOtherTeam's iCurrentHighestPriority = ",MC_serverBD_4.iCurrentHighestPriority[iOtherTeam])
				SET_PROGRESS_OBJECTIVE_FOR_TEAM(iTeam, TRUE)
			ELIF MC_serverBD_4.iCurrentHighestPriority[iOtherTeam] = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjectiveProgressionLogic[MC_serverBD_4.iCurrentHighestPriority[iTeam]][iOtherTeam]
				PRINTLN("[OBJECTIVE_PROGRESSION] SERVER_PROCESS_TEAM_PROGRESSION_DUE_TO_OTHER_TEAMS - Team ", iTeam, " should progress objective ", MC_serverBD_4.iCurrentHighestPriority[iTeam], ", due to other team ",iOtherTeam," reaching objective ",g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjectiveProgressionLogic[MC_serverBD_4.iCurrentHighestPriority[iTeam]][iOtherTeam]," - iOtherTeam's iCurrentHighestPriority = ",MC_serverBD_4.iCurrentHighestPriority[iOtherTeam])
				SET_PROGRESS_OBJECTIVE_FOR_TEAM(iTeam, TRUE)
			ENDIF
		ENDIF
	ENDFOR
	
	IF NOT SHOULD_PROGRESS_OBJECTIVE_FOR_TEAM(iTeam)
		EXIT
	ENDIF
	
	PRINTLN("[OBJECTIVE_PROGRESSION] SERVER_PROCESS_TEAM_PROGRESSION_DUE_TO_OTHER_TEAMS - Team ", iTeam, " should progress objective")
	
	BOOL bPreviousObjective = FALSE
	INT iObjectiveToProgressTo = GET_OBJECTIVE_TO_PROGRESS_TO_FOR_TEAM(iTeam, bPreviousObjective)
	
	INVALIDATE_UP_TO_AND_INCLUDING_OBJECTIVE(iTeam, iObjectiveToProgressTo, TRUE, DEFAULT, bPreviousObjective)
	RECALCULATE_OBJECTIVE_LOGIC(iTeam)
	SET_PROGRESS_OBJECTIVE_FOR_TEAM(iTeam, FALSE)
		
ENDPROC

PROC PROCESS_OBJECTIVE_PROGRESSION()
	
	INT iTeam
	INT iObjective[FMMC_MAX_TEAMS]
	
	FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams-1)
		iObjective[iTeam] = MC_serverBD_4.iCurrentHighestPriority[iTeam]
		PRINTLN("[OBJECTIVE_PROGRESSION] PROCESS_OBJECTIVE_PROGRESSION - Init, team ", iTeam, " has priority ", iObjective[iTeam])
	ENDFOR
	
	INT i
	INT iMaxLoops = FMMC_MAX_RULES * 4 //The max amount of times this would need to run
	FOR i = 0 TO iMaxLoops - 1
		BOOL bQuit = TRUE
		
		FOR iTeam = 0 TO MC_serverBD.iNumberOfTeams - 1
			IF MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
				SERVER_PROCESS_TEAM_PROGRESSION_DUE_TO_OTHER_TEAMS(iTeam)
				//If new priority is not the same as it was before running this loop:
				IF MC_serverBD_4.iCurrentHighestPriority[iTeam] != iObjective[iTeam]
					bQuit = FALSE
					iObjective[iTeam] = MC_serverBD_4.iCurrentHighestPriority[iTeam]
					PRINTLN("[RCC MISSION] PROCESS_OBJECTIVE_PROGRESSION - Team ", iTeam, " has a new objective, priority ", iObjective[iTeam])
				ENDIF
			ENDIF
		ENDFOR
		
		IF bQuit
			PRINTLN("[RCC MISSION] PROCESS_OBJECTIVE_PROGRESSION - Nobody has a new objective after this cycle, team objectives: team 0 has ", iObjective[0], ", team 1 has ", iObjective[1], ", team 2 has ", iObjective[2], ", team 3 has ", iObjective[3])
			BREAKLOOP
		ENDIF
	ENDFOR
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Client Mission Stage ----------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: 
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC SET_MC_CLIENT_MISSION_STAGE(INT iStage)
	
	IF iStage = CLIENT_MISSION_STAGE_NONE
		PRINTLN("[OBJECTIVE_PROGRESSION] SET_MC_CLIENT_MISSION_STAGE - Between objectives, waiting for a valid client mission stage.")
		REQUEST_OBJECTIVE_TEXT_DELAY(ciOBJECTIVE_TEXT_DELAY_ALL, FALSE #IF IS_DEBUG_BUILD , "SET_MC_CLIENT_MISSION_STAGE - waiting for valid stage" #ENDIF )
		EXIT
	ENDIF
	
	IF MC_playerBD[iLocalPart].iClientLogicStage = iStage
		EXIT
	ENDIF
	
	PRINTLN("[OBJECTIVE_PROGRESSION] SET_MC_CLIENT_MISSION_STAGE - Moving from stage (", GET_MC_CLIENT_MISSION_STAGE(iLocalPart), ") ", GET_MC_CLIENT_MISSION_STAGE_STRING_FOR_DEBUG(GET_MC_CLIENT_MISSION_STAGE(iLocalPart)), " to (",iStage,") ", GET_MC_CLIENT_MISSION_STAGE_STRING_FOR_DEBUG(iStage))
		
	MC_playerBD[iLocalPart].iClientLogicStage = iStage
	
ENDPROC

PROC PROCESS_SERVER_MISSION_OBJECTIVE_LOGIC()
	
	IF !bIsLocalPlayerHost
		EXIT
	ENDIF
	
	INT iTeam = 0
	BOOL bUpdateObjectives
	
	FOR iTeam = 0 TO MC_serverBD.iNumberOfTeams - 1
	
		IF SHOULD_PROGRESS_OBJECTIVE_FOR_TEAM(iTeam)
		
			BOOL bPreviousObjective = FALSE
			INT iObjectiveToProgressTo = GET_OBJECTIVE_TO_PROGRESS_TO_FOR_TEAM(iTeam, bPreviousObjective)
			
			PRINTLN("[OBJECTIVE_PROGRESSION] PROCESS_SERVER_MISSION_OBJECTIVE_LOGIC - Team ", iTeam, " should progress to objective: ", iObjectiveToProgressTo)
			
			INVALIDATE_UP_TO_AND_INCLUDING_OBJECTIVE(iTeam, iObjectiveToProgressTo, TRUE, DEFAULT, bPreviousObjective)
			
			RECALCULATE_OBJECTIVE_LOGIC(iTeam)
			
			SET_PROGRESS_OBJECTIVE_FOR_TEAM(iTeam, FALSE)
			
		ENDIF
			
		IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam)
			bUpdateObjectives = TRUE
		ENDIF
		
		IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_MIDPOINT_THIS_FRAME + iTeam)
			bUpdateObjectives = TRUE
			SERVER_PROCESS_TEAM_MIDPOINTS_FOR_OTHER_TEAMS(iTeam)
		ENDIF
			
	ENDFOR
		
	IF bUpdateObjectives
		PROCESS_OBJECTIVE_PROGRESSION()
	ENDIF

ENDPROC

FUNC INT GET_PLAYER_CLIENT_MISSION_STAGE_FROM_MISSION_LOGIC(INT iTeam = -1)
	
	IF iTeam = -1
		iTeam = MC_playerBD[iPartToUse].iTeam
	ENDIF
	
	SWITCH MC_serverBD_4.iPlayerRuleMissionLogic[iTeam]
		CASE FMMC_OBJECTIVE_LOGIC_KILL_PLAYERS 			RETURN CLIENT_MISSION_STAGE_KILL_PLAYERS
		CASE FMMC_OBJECTIVE_LOGIC_KILL_TEAM0 			RETURN CLIENT_MISSION_STAGE_KILL_TEAM0
		CASE FMMC_OBJECTIVE_LOGIC_KILL_TEAM1 			RETURN CLIENT_MISSION_STAGE_KILL_TEAM1
		CASE FMMC_OBJECTIVE_LOGIC_KILL_TEAM2 			RETURN CLIENT_MISSION_STAGE_KILL_TEAM2
		CASE FMMC_OBJECTIVE_LOGIC_KILL_TEAM3 			RETURN CLIENT_MISSION_STAGE_KILL_TEAM3
		CASE FMMC_OBJECTIVE_LOGIC_GET_MASKS 			RETURN CLIENT_MISSION_STAGE_GET_MASK
		CASE FMMC_OBJECTIVE_LOGIC_SCRIPTED_CUTSCENE 	RETURN CLIENT_MISSION_STAGE_SCRIPTED_CUTSCENE
		CASE FMMC_OBJECTIVE_LOGIC_PHONE_BULLSHARK 		RETURN CLIENT_MISSION_STAGE_PHONE_BULLSHARK
		CASE FMMC_OBJECTIVE_LOGIC_PHONE_AIRSTRIKE 		RETURN CLIENT_MISSION_STAGE_PHONE_AIRSTRIKE
		CASE FMMC_OBJECTIVE_LOGIC_GO_TO 				RETURN CLIENT_MISSION_STAGE_GOTO_ANY_PLAYER
		CASE FMMC_OBJECTIVE_LOGIC_GO_TO_TEAM0 			RETURN CLIENT_MISSION_STAGE_GOTO_TEAM0
		CASE FMMC_OBJECTIVE_LOGIC_GO_TO_TEAM1 			RETURN CLIENT_MISSION_STAGE_GOTO_TEAM1
		CASE FMMC_OBJECTIVE_LOGIC_GO_TO_TEAM2 			RETURN CLIENT_MISSION_STAGE_GOTO_TEAM2
		CASE FMMC_OBJECTIVE_LOGIC_GO_TO_TEAM3 			RETURN CLIENT_MISSION_STAGE_GOTO_TEAM3
		CASE FMMC_OBJECTIVE_LOGIC_LOOT_THRESHOLD		RETURN CLIENT_MISSION_STAGE_LOOT_THRESHOLD
		CASE FMMC_OBJECTIVE_LOGIC_POINTS_THRESHOLD		RETURN CLIENT_MISSION_STAGE_POINTS_THRESHOLD
		CASE FMMC_OBJECTIVE_LOGIC_HOLDING_RULE			RETURN CLIENT_MISSION_STAGE_HOLDING_RULE
		
	ENDSWITCH
	
	PRINTLN("[OBJECTIVE_PROGRESSION] GET_PED_CLIENT_MISSION_STAGE_FROM_MISSION_LOGIC - Invalid Mission Logic - ", MC_serverBD_4.iPlayerRuleMissionLogic[iTeam])
	RETURN CLIENT_MISSION_STAGE_NONE
	
ENDFUNC

FUNC INT GET_LOC_CLIENT_MISSION_STAGE_FROM_MISSION_LOGIC(INT iTeam = -1)
	
	IF iTeam = -1
		iTeam = MC_playerBD[iPartToUse].iTeam
	ENDIF
	
	SWITCH MC_serverBD_4.iLocMissionLogic[iTeam]
		CASE FMMC_OBJECTIVE_LOGIC_GO_TO 				RETURN CLIENT_MISSION_STAGE_GOTO_LOC
		CASE FMMC_OBJECTIVE_LOGIC_CAPTURE 				RETURN CLIENT_MISSION_STAGE_CAPTURE_AREA
		CASE FMMC_OBJECTIVE_LOGIC_PHOTO 				RETURN CLIENT_MISSION_STAGE_PHOTO_LOC
		CASE FMMC_OBJECTIVE_LOGIC_LEAVE_LOCATION 		RETURN CLIENT_MISSION_STAGE_LEAVE_LOC
	ENDSWITCH

	PRINTLN("[OBJECTIVE_PROGRESSION] GET_LOC_CLIENT_MISSION_STAGE_FROM_MISSION_LOGIC - Invalid Mission Logic - ", MC_serverBD_4.iLocMissionLogic[iTeam])
	RETURN CLIENT_MISSION_STAGE_NONE
	
ENDFUNC

FUNC INT GET_VEH_CLIENT_MISSION_STAGE_FROM_MISSION_LOGIC(INT iTeam = -1)
	
	IF iTeam = -1
		iTeam = MC_playerBD[iPartToUse].iTeam
	ENDIF
	
	SWITCH MC_serverBD_4.iVehMissionLogic[iTeam]
		CASE FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER		
		CASE FMMC_OBJECTIVE_LOGIC_GET_AND_HOLD 
			IF (amIWaitingForPlayersToEnterVeh != NULL AND NOT CALL amIWaitingForPlayersToEnterVeh())
			AND (MC_playerBD[iPartToUse].iVehNear != -1)
				IF MC_serverBD.iRequiredDeliveries[iTeam] <= 1
				OR MC_serverBD.iNumVehHighestPriorityHeld[iTeam] >= MC_serverBD.iRequiredDeliveries[iTeam]
					RETURN CLIENT_MISSION_STAGE_DELIVER_VEH
				ENDIF
			ENDIF
			
			RETURN CLIENT_MISSION_STAGE_COLLECT_VEH
		BREAK
		CASE FMMC_OBJECTIVE_LOGIC_KILL 					RETURN CLIENT_MISSION_STAGE_KILL_VEH
		CASE FMMC_OBJECTIVE_LOGIC_CAPTURE 				RETURN CLIENT_MISSION_STAGE_CAPTURE_VEH
		CASE FMMC_OBJECTIVE_LOGIC_PHOTO 				RETURN CLIENT_MISSION_STAGE_PHOTO_VEH
		CASE FMMC_OBJECTIVE_LOGIC_GO_TO 				RETURN CLIENT_MISSION_STAGE_GOTO_VEH
		CASE FMMC_OBJECTIVE_LOGIC_PROTECT 				RETURN CLIENT_MISSION_STAGE_PROTECT_VEH
		CASE FMMC_OBJECTIVE_LOGIC_LEAVE_ENTITY 			RETURN CLIENT_MISSION_STAGE_LEAVE_VEH 
		CASE FMMC_OBJECTIVE_LOGIC_DAMAGE	 			RETURN CLIENT_MISSION_STAGE_DAMAGE_VEH
	ENDSWITCH
	
	PRINTLN("[OBJECTIVE_PROGRESSION] GET_VEH_CLIENT_MISSION_STAGE_FROM_MISSION_LOGIC - Invalid Mission Logic - ", MC_serverBD_4.iVehMissionLogic[iTeam])
	RETURN CLIENT_MISSION_STAGE_NONE
	
ENDFUNC

FUNC INT GET_OBJ_CLIENT_MISSION_STAGE_FROM_MISSION_LOGIC(INT iTeam = -1)
	
	IF iTeam = -1
		iTeam = MC_playerBD[iPartToUse].iTeam
	ENDIF
	
	SWITCH MC_serverBD_4.iObjMissionLogic[iTeam]
		CASE FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER		
		CASE FMMC_OBJECTIVE_LOGIC_GET_AND_HOLD 
			IF MC_playerBD[iPartToUse].iObjCarryCount > 0
			OR IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_WITH_CARRIER)
				IF MC_serverBD.iRequiredDeliveries[iTeam] <= 1
				OR MC_serverBD.iNumObjHighestPriorityHeld[iTeam] >= MC_serverBD.iRequiredDeliveries[iTeam]
					RETURN CLIENT_MISSION_STAGE_DELIVER_OBJ
				ENDIF
			ENDIF
				
			RETURN CLIENT_MISSION_STAGE_COLLECT_OBJ
		BREAK
		CASE FMMC_OBJECTIVE_LOGIC_MINIGAME 				
			SWITCH MC_serverBD_4.iObjMissionSubLogic[iTeam]
				CASE ci_HACK_SUBLOGIC_HACK
				CASE ci_HACK_SUBLOGIC_HACK_2
				CASE ci_HACK_SUBLOGIC_BRUTE_FORCE_ULP
					RETURN CLIENT_MISSION_STAGE_HACK_COMP
				BREAK
				CASE ci_HACK_SUBLOGIC_ORDER_UNLOCK
				CASE ci_HACK_SUBLOGIC_FINGERPRINT_CLONE
					RETURN CLIENT_MISSION_STAGE_HACK_FINGERPRINT_KEYPAD
				BREAK
				CASE ci_HACK_SUBLOGIC_DRILLING						RETURN CLIENT_MISSION_STAGE_HACK_DRILL
				CASE ci_HACK_SUBLOGIC_LESTER_SNAKE					RETURN CLIENT_MISSION_STAGE_HACK_COMP
				CASE ci_HACK_SUBLOGIC_BEAM_HACK						RETURN CLIENT_MISSION_STAGE_HACKING
				CASE ci_HACK_SUBLOGIC_HOTWIRE						RETURN CLIENT_MISSION_STAGE_HACKING
				CASE ci_HACK_SUBLOGIC_INTERACT_WITH					RETURN CLIENT_MISSION_STAGE_INTERACT_WITH
				CASE ci_HACK_SUBLOGIC_GLASS_CUTTING					RETURN CLIENT_MISSION_STAGE_GLASS_CUTTING
				CASE ci_HACK_SUBLOGIC_UNDERWATER_WELDING			RETURN CLIENT_MISSION_STAGE_UNDERWATER_WELDING
				CASE ci_HACK_SUBLOGIC_ENTER_SAFE_COMBINATION 		RETURN CLIENT_MISSION_STAGE_ENTER_SAFE_COMBINATION
				CASE ci_HACK_SUBLOGIC_INTERROGATION 				RETURN CLIENT_MISSION_STAGE_INTERROGATION
				CASE ci_HACK_SUBLOGIC_BEAMHACK_VEH 					RETURN CLIENT_MISSION_STAGE_BEAMHACK_VEH
				CASE ci_HACK_SUBLOGIC_CHECK_FUSEBOX 				RETURN CLIENT_MISSION_STAGE_INTERACT_WITH
				CASE ci_HACK_SUBLOGIC_INSERT_FUSE 					RETURN CLIENT_MISSION_STAGE_INSERT_FUSE
			ENDSWITCH
		BREAK
		CASE FMMC_OBJECTIVE_LOGIC_KILL 					RETURN CLIENT_MISSION_STAGE_KILL_OBJ
		CASE FMMC_OBJECTIVE_LOGIC_CAPTURE 				RETURN CLIENT_MISSION_STAGE_CAPTURE_OBJ
		CASE FMMC_OBJECTIVE_LOGIC_PHOTO 				RETURN CLIENT_MISSION_STAGE_PHOTO_OBJ
		CASE FMMC_OBJECTIVE_LOGIC_GO_TO 				RETURN CLIENT_MISSION_STAGE_GOTO_OBJ
		CASE FMMC_OBJECTIVE_LOGIC_PROTECT 				RETURN CLIENT_MISSION_STAGE_PROTECT_OBJ
		CASE FMMC_OBJECTIVE_LOGIC_LEAVE_ENTITY 			RETURN CLIENT_MISSION_STAGE_LEAVE_OBJ
		CASE FMMC_OBJECTIVE_LOGIC_DAMAGE	 			RETURN CLIENT_MISSION_STAGE_DAMAGE_OBJ
	ENDSWITCH
	
	PRINTLN("[OBJECTIVE_PROGRESSION] GET_OBJ_CLIENT_MISSION_STAGE_FROM_MISSION_LOGIC - Invalid Mission Logic - ", MC_serverBD_4.iObjMissionLogic[iTeam])
	RETURN CLIENT_MISSION_STAGE_NONE
	
ENDFUNC

FUNC INT GET_PED_CLIENT_MISSION_STAGE_FROM_MISSION_LOGIC(INT iTeam = -1)
	
	IF iTeam = -1
		iTeam = MC_playerBD[iPartToUse].iTeam
	ENDIF
	
	SWITCH MC_serverBD_4.iPedMissionLogic[iTeam]
		CASE FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER		
		CASE FMMC_OBJECTIVE_LOGIC_GET_AND_HOLD 
			IF MC_playerBD[iPartToUse].iPedCarryCount > 0
			OR IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_WITH_CARRIER)
				IF (MC_serverBD.iRequiredDeliveries[iTeam] <= 1
				OR MC_serverBD.iNumPedHighestPriorityHeld[iTeam] >= MC_serverBD.iRequiredDeliveries[iTeam])
					RETURN CLIENT_MISSION_STAGE_DELIVER_PED
				ENDIF
			ENDIF
			
			RETURN CLIENT_MISSION_STAGE_COLLECT_PED
		BREAK
		CASE FMMC_OBJECTIVE_LOGIC_KILL 					RETURN CLIENT_MISSION_STAGE_KILL_PED
		CASE FMMC_OBJECTIVE_LOGIC_DAMAGE				RETURN CLIENT_MISSION_STAGE_DAMAGE_PED
		CASE FMMC_OBJECTIVE_LOGIC_CAPTURE 				RETURN CLIENT_MISSION_STAGE_CAPTURE_PED
		CASE FMMC_OBJECTIVE_LOGIC_PHOTO 				RETURN CLIENT_MISSION_STAGE_PHOTO_PED
		CASE FMMC_OBJECTIVE_LOGIC_GO_TO 				RETURN CLIENT_MISSION_STAGE_GOTO_PED
		CASE FMMC_OBJECTIVE_LOGIC_PROTECT 				RETURN CLIENT_MISSION_STAGE_PROTECT_PED
		CASE FMMC_OBJECTIVE_LOGIC_LEAVE_ENTITY 			RETURN CLIENT_MISSION_STAGE_LEAVE_PED
		CASE FMMC_OBJECTIVE_LOGIC_PED_GOTO_LOCATION		RETURN CLIENT_MISSION_STAGE_PED_GOTO_LOCATION
	ENDSWITCH

	PRINTLN("[OBJECTIVE_PROGRESSION] GET_PED_CLIENT_MISSION_STAGE_FROM_MISSION_LOGIC - Invalid Mission Logic")
	RETURN CLIENT_MISSION_STAGE_NONE
	
ENDFUNC

PROC PROCESS_CLIENT_MISSION_OBJECTIVE_LOGIC()
	
	IF IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
		SET_MC_CLIENT_MISSION_STAGE(CLIENT_MISSION_STAGE_RESULTS)
		EXIT
	ENDIF
	
	IF MC_playerBD[iLocalPart].iObjectiveTypeCompleted != -1
		EXIT
	ENDIF
	
	IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_FINISHED)
		SET_MC_CLIENT_MISSION_STAGE(CLIENT_MISSION_STAGE_RESULTS)
		EXIT
	ENDIF
	
	IF MC_serverBD.iNumPlayerRuleHighestPriority[MC_playerBD[iPartToUse].iTeam] > 0 
			
		SET_MC_CLIENT_MISSION_STAGE(GET_PLAYER_CLIENT_MISSION_STAGE_FROM_MISSION_LOGIC())
		
		IF MC_serverBD_4.iPlayerRuleMissionLogic[MC_playerBD[iPartToUse].iTeam] = FMMC_OBJECTIVE_LOGIC_GET_MASKS	
			g_MaskObjectiveActive = TRUE
		ENDIF
			
	ELIF MC_serverBD.iNumLocHighestPriority[MC_playerBD[iPartToUse].iTeam] > 0 
		
		SET_MC_CLIENT_MISSION_STAGE(GET_LOC_CLIENT_MISSION_STAGE_FROM_MISSION_LOGIC())
		
	ELIF MC_serverBD.iNumPedHighestPriority[MC_playerBD[iPartToUse].iTeam] > 0 
		
		SET_MC_CLIENT_MISSION_STAGE(GET_PED_CLIENT_MISSION_STAGE_FROM_MISSION_LOGIC())
		
		IF MC_serverBD_4.iPedMissionLogic[MC_playerBD[iPartToUse].iTeam]  = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER 
		OR MC_serverBD_4.iPedMissionLogic[MC_playerBD[iPartToUse].iTeam] = FMMC_OBJECTIVE_LOGIC_GET_AND_HOLD 
		OR MC_serverBD_4.iPedMissionLogic[MC_playerBD[iPartToUse].iTeam]  = FMMC_OBJECTIVE_LOGIC_CAPTURE
			IF MC_playerBD[iPartToUse].iPedCarryCount > 0
			OR IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_WITH_CARRIER)
				IF NOT IS_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITITCAL_ENTITY()
					SET_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITICAL_ENTITY(TRUE)
				ENDIF
			ENDIF
		ENDIF

	ELIF MC_serverBD.iNumVehHighestPriority[MC_playerBD[iPartToUse].iTeam] > 0 

		SET_MC_CLIENT_MISSION_STAGE(GET_VEH_CLIENT_MISSION_STAGE_FROM_MISSION_LOGIC())

		IF MC_serverBD_4.iVehMissionLogic[MC_playerBD[iPartToUse].iTeam] = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER 
		OR MC_serverBD_4.iVehMissionLogic[MC_playerBD[iPartToUse].iTeam] = FMMC_OBJECTIVE_LOGIC_GET_AND_HOLD  // this high priority vehicle is a collect target
			IF (amIWaitingForPlayersToEnterVeh != NULL AND NOT CALL amIWaitingForPlayersToEnterVeh())
			AND (MC_playerBD[iPartToUse].iVehNear != -1)
				IF NOT IS_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITITCAL_ENTITY()
					SET_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITICAL_ENTITY(TRUE)
				ENDIF
				g_iCriticalVehicleID = MC_playerBD[iPartToUse].iVehNear
			ENDIF
		ELIF MC_serverBD_4.iVehMissionLogic[MC_playerBD[iPartToUse].iTeam]  = FMMC_OBJECTIVE_LOGIC_CAPTURE
			IF MC_playerBD[iPartToUse].iVehCarryCount > 0  
			OR IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_WITH_CARRIER)
				IF MC_playerBD[iPartToUse].iVehNear != -1
					g_iCriticalVehicleID = MC_playerBD[iPartToUse].iVehNear
				ENDIF
				IF NOT IS_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITITCAL_ENTITY()
					SET_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITICAL_ENTITY(TRUE)
				ENDIF
			ENDIF
		ENDIF
		
	ELIF MC_serverBD.iNumObjHighestPriority[MC_playerBD[iPartToUse].iTeam] > 0 
		
		SET_MC_CLIENT_MISSION_STAGE(GET_OBJ_CLIENT_MISSION_STAGE_FROM_MISSION_LOGIC())
		
		IF MC_serverBD_4.iObjMissionLogic[MC_playerBD[iPartToUse].iTeam] = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER 
		OR MC_serverBD_4.iObjMissionLogic[MC_playerBD[iPartToUse].iTeam] = FMMC_OBJECTIVE_LOGIC_GET_AND_HOLD
		OR MC_serverBD_4.iObjMissionLogic[MC_playerBD[iPartToUse].iTeam]  = FMMC_OBJECTIVE_LOGIC_CAPTURE
			IF MC_playerBD[iPartToUse].iObjCarryCount > 0
			OR IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_WITH_CARRIER)
				IF NOT IS_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITITCAL_ENTITY()
					SET_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITICAL_ENTITY(TRUE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_CLIENT_OBJECTIVE_BLOCKER()
	IF IS_OBJECTIVE_BLOCKED()
		IF IS_BIT_SET(iLocalBoolCheck30, LBOOL30_OBJECTIVE_BLOCKER_REQUESTED_THIS_FRAME)
			// Keep the objective blocked for now
		ELSE
			// We no longer want the objective to be blocked! Nothing has tried to block it this frame
			CLEAR_OBJECTIVE_BLOCKER()
		ENDIF
	ENDIF
	
	CLEAR_BIT(iLocalBoolCheck30, LBOOL30_OBJECTIVE_BLOCKER_REQUESTED_THIS_FRAME)
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Repair Vehicle Rule Processing
// ##### Description: Processing of rules where a Vehicle must be above a certain health threshold to pass.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL DOES_VEHICLE_NEED_REPAIRED(VEHICLE_INDEX viVeh, INT iHealthThreshold)

	IF IS_ANY_VEHICLE_DOOR_DAMAGED(viVeh)
		RETURN TRUE
	ENDIF

	INT iMissionEntityID = IS_ENTITY_A_MISSION_CREATOR_ENTITY(viVeh)
	FLOAT fPercentage		
	IF iMissionEntityID != -1
		fPercentage = MC_GET_VEHICLE_TOTAL_HEALTH_PERCENTAGE(viVeh, iMissionEntityID, GET_TOTAL_STARTING_PLAYERS())
	ELSE
		fPercentage = GET_VEHICLE_HEALTH_PERCENTAGE(viVeh)
	ENDIF
		
	RETURN fPercentage <= iHealthThreshold
	
ENDFUNC

FUNC BOOL DOES_PLAYER_NEED_TO_REPAIR_THEIR_VEHICLE_FOR_RULE()
	
	INT iTeam = MC_playerBD[iPartToUse].iTeam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	IF iRule >= FMMC_MAX_RULES
		RETURN FALSE
	ENDIF
	
	IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMinVehHealth[iRule] <= 0
		RETURN FALSE
	ENDIF
	
	INT iClientStage = GET_MC_CLIENT_MISSION_STAGE(iPartToUse)
	
	IF iClientStage != CLIENT_MISSION_STAGE_DELIVER_VEH
	AND iClientStage != CLIENT_MISSION_STAGE_COLLECT_VEH
		RETURN FALSE
	ENDIF
	
	VEHICLE_INDEX viTempVeh
	
	IF iClientStage = CLIENT_MISSION_STAGE_DELIVER_VEH
		IF bPedToUseOk
		AND IS_PED_IN_ANY_VEHICLE(PlayerPedToUse)
			viTempVeh = GET_VEHICLE_PED_IS_IN(PlayerPedToUse)
			IF DOES_VEHICLE_NEED_REPAIRED(viTempVeh, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMinVehHealth[iRule])
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
			
	IF MC_playerBD_1[iPartToUse].iVehicleNeededToRepair = -1
		RETURN FALSE
	ENDIF
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[MC_playerBD_1[iPartToUse].iVehicleNeededToRepair])
		viTempVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[MC_playerBD_1[iPartToUse].iVehicleNeededToRepair])
		IF DOES_VEHICLE_NEED_REPAIRED(viTempVeh, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMinVehHealth[iRule])
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
	
ENDFUNC

PROC PROCESS_CLIENT_REPAIR_VEHICLE_RULE_PROGRESSION()
	
	BOOL bRepairVeh = DOES_PLAYER_NEED_TO_REPAIR_THEIR_VEHICLE_FOR_RULE()
	
	IF bRepairVeh = IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet4, PBBOOL4_IS_CURRENT_OBJECTIVE_REPAIR_CAR)
		EXIT
	ENDIF
	
	PRINTLN("[REPAIR_VEH] PROCESS_CLIENT_REPAIR_VEHICLE_RULE_PROGRESSION - Lose wanted objective changed (", BOOL_TO_STRING(bRepairVeh))
	
	IF bRepairVeh
		SET_BIT(MC_playerBD[iPartToUse].iClientBitSet4, PBBOOL4_IS_CURRENT_OBJECTIVE_REPAIR_CAR)
	ELSE
		CLEAR_BIT(MC_playerBD[iPartToUse].iClientBitSet4, PBBOOL4_IS_CURRENT_OBJECTIVE_REPAIR_CAR)
	ENDIF
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Wanted Rule Processing
// ##### Description: Processing of rules where a Wanted level must be lost to pass.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL DOES_PLAYERS_TEAM_NEED_TO_LOSE_WANTED()

	INT iTeam = MC_playerBD[iPartToUse].iTeam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	IF iRule >= FMMC_MAX_RULES
		RETURN FALSE
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iLoseWantedBitset, iRule)
		RETURN FALSE
	ENDIF
	
	INT iClientStage = GET_MC_CLIENT_MISSION_STAGE(iPartToUse)
	
	IF (iClientStage != CLIENT_MISSION_STAGE_COLLECT_OBJ
	AND iClientStage != CLIENT_MISSION_STAGE_COLLECT_VEH
	AND iClientStage != CLIENT_MISSION_STAGE_COLLECT_PED)
		
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iWantedAtMidBitset, iRule)
			
			IF IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iTeam], iRule)
				RETURN TRUE
			ENDIF
			
			RETURN FALSE
			
		ENDIF
		
		RETURN TRUE
		
	ENDIF
	
	IF IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThree[iRule], ciBS_RULE3_OVERRIDE_COLLECTION_TEXT)
		RETURN TRUE
	ENDIF
	
	//Check if we're holding anyone else up by having a wanted level
	IF MC_playerBD[iPartToUse].iWanted = 0
		RETURN FALSE
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitset[iRule], ciBS_RULE_WANTED_TEAM_OBJ)
	AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitset[iRule], ciBS_RULE_WANTED_COOP_TEAMS_OBJ)
		RETURN FALSE
	ENDIF
							
	SWITCH iClientStage
		CASE CLIENT_MISSION_STAGE_COLLECT_OBJ
			IF MC_serverBD.iNumObjHighestPriorityHeld[iTeam] >= MC_serverBD.iNumObjHighestPriority[iTeam]
				RETURN TRUE
			ENDIF
		BREAK
		CASE CLIENT_MISSION_STAGE_COLLECT_VEH
			IF MC_serverBD.iNumVehHighestPriorityHeld[iTeam] >= MC_serverBD.iNumVehHighestPriority[iTeam]
				RETURN TRUE
			ENDIF
		BREAK
		CASE CLIENT_MISSION_STAGE_COLLECT_PED
			IF MC_serverBD.iNumPedHighestPriorityHeld[iTeam] >= MC_serverBD.iNumPedHighestPriority[iTeam]
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE 
	
ENDFUNC

FUNC BOOL DOES_PLAYER_NEED_TO_LOSE_WANTED()

	IF NOT DOES_PLAYERS_TEAM_NEED_TO_LOSE_WANTED()
		RETURN FALSE
	ENDIF
	
	IF MC_playerBD[iPartToUse].iWanted > 0
		RETURN TRUE
	ENDIF
	
	INT iClientStage = GET_MC_CLIENT_MISSION_STAGE(iPartToUse)
	INT iTeam = MC_playerBD[iPartToUse].iTeam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam] //Already safety checked above
	
	VEHICLE_INDEX viVehicle
	IF iClientStage = CLIENT_MISSION_STAGE_COLLECT_VEH
		IF IS_PED_IN_ANY_VEHICLE(PlayerPedToUse)
			viVehicle = GET_VEHICLE_PED_IS_IN(PlayerPedToUse)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitset[iRule], ciBS_RULE_WANTED_TEAM_OBJ)
		
		//[FMMC2020] - Look at moving this into a general player loop
		
		BOOL bVehAlive = IS_ENTITY_ALIVE(viVehicle)
		
		INT iPart = -1
		DO_PARTICIPANT_LOOP_FLAGS eFlags = GET_PARTICIPANT_LOOP_FLAGS_TO_CHECK_SINGLE_TEAM(iTeam)
		IF bVehAlive
			eFlags = eFlags | DPLF_FILL_PLAYER_IDS
		ENDIF
		WHILE DO_PARTICIPANT_LOOP(iPart, eFlags | DPLF_EXCLUDE_LOCAL_PART)

			IF MC_playerBD[iPart].iWanted = 0
				RELOOP
			ENDIF
			
			IF bVehAlive
				IF IS_PED_IN_VEHICLE(piParticipantLoop_PedIndex, viVehicle)
					//The player is in the same vehicle as the local player who does not have a wanted level
					RETURN FALSE
				ENDIF
			ENDIF
			
			RETURN TRUE
				
		ENDWHILE
		
		RETURN FALSE
		
	ENDIF
	
	IF IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitset[iRule], ciBS_RULE_WANTED_COOP_TEAMS_OBJ)
		
		INT iTeamLoop
		FOR iTeamLoop = 0 TO FMMC_MAX_TEAMS - 1
		
			IF NOT DOES_TEAM_LIKE_TEAM(iTeam, iTeamLoop)
				RELOOP
			ENDIF
			
			IF MC_serverBD.iNumOfWanted[iTeamLoop] = 0
				RELOOP
			ENDIF
				
			RETURN TRUE
			
		ENDFOR
		
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC PROCESS_CLIENT_WANTED_RULE_PROGRESSION()
	
	BOOL bLoseCops = DOES_PLAYER_NEED_TO_LOSE_WANTED()
			
	IF bLoseCops != IS_BIT_SET(iLocalBoolCheck4, LBOOL4_CURRENT_OBJECTIVE_LOSE_WANTED)
		PRINTLN("[WANTED] PROCESS_CLIENT_WANTED_RULE_PROGRESSION - Lose wanted objective changed (", BOOL_TO_STRING(bLoseCops), "): requesting objective text update")
		REQUEST_OBJECTIVE_TEXT_DELAY(ciOBJECTIVE_TEXT_DELAY_WANTED, FALSE #IF IS_DEBUG_BUILD , "PROCESS_CLIENT_WANTED_RULE_PROGRESSION - wanted changed" #ENDIF )
	ENDIF
	
	IF bLoseCops
		SET_BIT(iLocalBoolCheck4, LBOOL4_CURRENT_OBJECTIVE_LOSE_WANTED)
	ELSE
		CLEAR_BIT(iLocalBoolCheck4, LBOOL4_CURRENT_OBJECTIVE_LOSE_WANTED)
	ENDIF

ENDPROC

PROC PROCESS_SERVER_WANTED_RULE_PROGRESSION()
	
	IF !bIsLocalPlayerHost
		EXIT
	ENDIF
	
	//[FMMC2020] Move logic from PROCESS_OBJECTIVE_LIMITS here
	
ENDPROC


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Obj Limits
// ##### Description: ....
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL IS_TEAM_LEADING_ON_POINTS(INT iTeam)
	
	INT i
	FOR i = 0 TO MC_serverBD.iNumActiveTeams - 1
		
		IF i = iTeam
			RELOOP
		ENDIF
		
		IF IS_TEAM_ACTIVE(i)
			RELOOP
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciUSE_LIVES_REMAINING_FOR_WINNER) 
			IF (GET_FMMC_MISSION_LIVES_FOR_TEAM(iTeam) - GET_TEAM_DEATHS(iTeam))  <= (GET_FMMC_MISSION_LIVES_FOR_TEAM(i) - GET_TEAM_DEATHS(i))
				PRINTLN("[OBJECTIVE_LIMITS] - IS_TEAM_LEADING_ON_POINTS - Team ", iTeam, "'s Lives Left is not higher than Team: ", i)
				RETURN FALSE
			ENDIF
			
			RELOOP
		ENDIF
		
		IF MC_serverBD.iTeamScore[iTeam] <= MC_serverBD.iTeamScore[i]
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciSUDDEN_DEATH_POWER_PLAY_CAPTURE)
			AND MC_serverBD.iTeamScore[iTeam] = MC_serverBD.iTeamScore[i]	
				IF MC_serverBD.iGranularCurrentPoints[iTeam] <= MC_serverBD.iGranularCurrentPoints[i]
					PRINTLN("[OBJECTIVE_LIMITS] - IS_TEAM_LEADING_ON_POINTS - Team ", iTeam, "'s Score (including granular capture) is not higher than Team: ", i)
					RETURN FALSE
				ENDIF
				
				RELOOP
			ENDIF
			
			PRINTLN("[OBJECTIVE_LIMITS] - IS_TEAM_LEADING_ON_POINTS - Team ", iTeam, "'s Score is not higher than Team: ", i)
			RETURN FALSE
		ENDIF
		
	ENDFOR
	
	RETURN TRUE
	
ENDFUNC

PROC PROCESS_PER_RULE_TIMER_RESYNC()
	
	IF !bIsLocalPlayerHost
		EXIT
	ENDIF
	
	IF IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_RESYNC_MAIN_TIMERS_EVERY_RULE)
		EXIT
	ENDIF
	
	INT iTeamTimers = 0
	INT iLocalTimer[FMMC_MAX_TEAMS]
	
	FOR iTeamTimers = 0 TO MC_serverBD.iNumberOfTeams - 1
		IF MC_serverBD_4.iCurrentHighestPriority[iTeamTimers] >= FMMC_MAX_RULES
			RELOOP
		ENDIF
		
		IF IS_BIT_SET(iLocalBoolCheck16, LBOOL16_TEAM_0_IS_PAST_THEIR_FIRST_RULE + iTeamTimers)
			iLocalTimer[iTeamTimers] = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdObjectiveLimitTimer[iTeamTimers])
		ENDIF
		
		IF MC_serverBD_3.iRuleObjectiveTimeLimitKeepSyncCache[iTeamTimers] != 0 //We have some value cached
		AND iLocalTimer[iTeamTimers] < MC_serverBD_3.iRuleObjectiveTimeLimitKeepSyncCache[iTeamTimers] // The new timer is smaller ( The timer was resetted )
		AND IS_OBJECTIVE_TIMER_RUNNING(iTeamTimers) // The actual objective timer was initialized
			INT iNewSum = MC_serverBD_3.iRuleObjectiveTimeLimitKeepSyncCache[iTeamTimers]
			INT iDiff = iNewSum - GET_OBJECTIVE_TIME_LIMIT_FOR_CURRENT_RULE(iTeamTimers) // How much I need to go back
			PRINTLN("[LM][RESYNC_TIMERS] Caching Times for Team: ", iTeamTimers, " Before MultiRuleDiff: ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdMultiObjectiveLimitTimer[iTeamTimers])," Diff: ",iDiff)
			MC_serverBD_3.tdMultiObjectiveLimitTimer[iTeamTimers].Timer = GET_TIME_OFFSET(MC_serverBD_3.tdMultiObjectiveLimitTimer[iTeamTimers].Timer, iDiff)
			PRINTLN("[LM][RESYNC_TIMERS] Caching Times for Team: ", iTeamTimers, " After MultiRuleDiff: ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdMultiObjectiveLimitTimer[iTeamTimers]))
			MC_serverBD_3.iRuleObjectiveTimeLimitKeepSyncCache[iTeamTimers] = 0
		ELIF iLocalTimer[iTeamTimers] != 0
		AND MC_serverBD_3.iRuleObjectiveTimeLimitKeepSyncCache[iTeamTimers] != iLocalTimer[iTeamTimers]
		AND iLocalTimer[iTeamTimers] > GET_OBJECTIVE_TIME_LIMIT_FOR_CURRENT_RULE(iTeamTimers)
			MC_serverBD_3.iRuleObjectiveTimeLimitKeepSyncCache[iTeamTimers] = iLocalTimer[iTeamTimers]
			PRINTLN("[LM][RESYNC_TIMERS] Caching Times for Team: ", iTeamTimers," Rule: ", MC_serverBD_3.iRuleObjectiveTimeLimitKeepSyncCache[iTeamTimers])
		ENDIF
	ENDFOR
	
ENDPROC

PROC PROCESS_INCREMENT_HOLDING_PACKAGE_DRAWING_SCORES(INT iRule)
	
	INT iTeam, iHighestScore, iNumTeamObjects
	FOR iTeam = 0 TO MC_serverBD.iNumberOfTeams - 1
		IF NOT IS_TEAM_ACTIVE(iTeam)
			RELOOP
		ENDIF
		
		iNumTeamObjects = GET_NUMBER_OF_OBJECTS_AT_TEAM_HOLDING(iTeam)
		IF iNumTeamObjects > iHighestScore
			iHighestScore = iNumTeamObjects
		ENDIF
	ENDFOR
	
	FOR iTeam = 0 TO MC_serverBD.iNumberOfTeams - 1
		IF NOT IS_TEAM_ACTIVE(iTeam)
			RELOOP
		ENDIF
		IF GET_NUMBER_OF_OBJECTS_AT_TEAM_HOLDING(iTeam) = iHighestScore
			INCREMENT_SERVER_TEAM_SCORE(iTeam, iRule, GET_FMMC_POINTS_FOR_TEAM(iTeam, iRule))
		ENDIF
	ENDFOR
	
ENDPROC

PROC PROCESS_HOLDING_PACKAGE_SCORING()

	IF IS_BIT_SET(iLocalBoolCheck25, LBOOL25_ALTERNATIVE_GD_SCORE_ADDED)
		EXIT
	ENDIF
	
	INT iWinningTeam = GET_TEAM_WITH_MOST_OBJECTS_AT_HOLDING()
	INT iRule = MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iTeam]
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciGIVE_POINT_FOR_EACH_PACKAGE)
		INT iTeam
		FOR iTeam = 0 TO MC_serverBD.iNumberOfTeams - 1
			IF NOT IS_TEAM_ACTIVE(iTeam)
				RELOOP
			ENDIF
			
			IF MC_serverBD_4.iObjMissionLogic[iTeam] = FMMC_OBJECTIVE_LOGIC_GET_AND_HOLD
			AND MC_serverBD_4.iObjMissionLogic[iTeam] != FMMC_OBJECTIVE_LOGIC_PROTECT
				INCREMENT_SERVER_TEAM_SCORE(iTeam, iRule, GET_NUMBER_OF_OBJECTS_AT_TEAM_HOLDING(iTeam))	
				PRINTLN("PROCESS_HOLDING_PACKAGE_SCORING - ciGIVE_POINT_FOR_EACH_PACKAGE - Incrementing score for team: ", iWinningTeam)
				SET_BIT(iLocalBoolCheck25, LBOOL25_ALTERNATIVE_GD_SCORE_ADDED)
			ENDIF
		ENDFOR
	ELSE
		IF iWinningTeam > -1
			INCREMENT_SERVER_TEAM_SCORE(iWinningTeam, iRule, GET_FMMC_POINTS_FOR_TEAM(iWinningTeam, iRule))	
			PRINTLN("PROCESS_HOLDING_PACKAGE_SCORING - Incrementing score for team: ", iWinningTeam)
			SET_BIT(iLocalBoolCheck25, LBOOL25_ALTERNATIVE_GD_SCORE_ADDED)
		ELSE
			PROCESS_INCREMENT_HOLDING_PACKAGE_DRAWING_SCORES(iRule)
			PRINTLN("PROCESS_HOLDING_PACKAGE_SCORING - Incrementing multiple team scores")
			SET_BIT(iLocalBoolCheck25, LBOOL25_ALTERNATIVE_GD_SCORE_ADDED)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_OBJECTIVE_LIMITS()

	PROCESS_PER_RULE_TIMER_RESYNC()
	
	#IF IS_DEBUG_BUILD
	PROCESS_PAUSED_TIMERS()
	#ENDIF

	PROCESS_TIME_REMAINING_CACHE()

	IF !bIsLocalPlayerHost
		EXIT
	ENDIF
		
	INT iTeam
	BOOL bObjectiveOver[FMMC_MAX_TEAMS]
	BOOL bNoPlayersInTeam[FMMC_MAX_TEAMS]
	BOOL bObjectivePassed[FMMC_MAX_TEAMS] 
	INT iScoreLimit
	BOOL bFailMission[FMMC_MAX_TEAMS]
	BOOL bRuleTimerExpired, bMultiTimerExpired
	INT iTeamAliveAsSuddenDeathStarts = -1

	FOR iTeam = 0 TO MC_serverBD.iNumberOfTeams - 1
		
		bRuleTimerExpired = FALSE
		bMultiTimerExpired = FALSE
			
		IF IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
			BREAKLOOP
		ENDIF	
			
		IF MC_serverBD_4.iCurrentHighestPriority[iTeam] >= FMMC_MAX_RULES
			RELOOP
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwelve[MC_ServerBD_4.iCurrentHighestPriority[iTeam]], ciBS_RULE12_DONT_START_TIMER_UNTIL_SCORED_POINT_RULE)
		AND MC_serverBD.iScoreOnThisRule[iTeam] < g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTimerPointsRequired[MC_ServerBD_4.iCurrentHighestPriority[iTeam]]
			#IF IS_DEBUG_BUILD
			IF NOT IS_BIT_SET(MC_ServerBD.iServerBitSet7, SBBOOL7_BLOCK_RULE_TIMER_START_DUE_TO_SCORE_FOR_TEAM_0 + iTeam)
				PRINTLN("[OBJECTIVE_LIMITS] PROCESS_OBJECTIVE_LIMITS - Team: ", iTeam, " Setting Block Timer bit.")
			ENDIF
			#ENDIF
			SET_BIT(MC_ServerBD.iServerBitSet7, SBBOOL7_BLOCK_RULE_TIMER_START_DUE_TO_SCORE_FOR_TEAM_0 + iTeam)
		ELSE
			#IF IS_DEBUG_BUILD
			IF IS_BIT_SET(MC_ServerBD.iServerBitSet7, SBBOOL7_BLOCK_RULE_TIMER_START_DUE_TO_SCORE_FOR_TEAM_0 + iTeam)
				PRINTLN("[OBJECTIVE_LIMITS] PROCESS_OBJECTIVE_LIMITS - Team: ", iTeam, " Clearing Block Timer bit.")
			ENDIF
			#ENDIF
			CLEAR_BIT(MC_ServerBD.iServerBitSet7, SBBOOL7_BLOCK_RULE_TIMER_START_DUE_TO_SCORE_FOR_TEAM_0 + iTeam)
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwelve[MC_ServerBD_4.iCurrentHighestPriority[iTeam]], ciBS_RULE12_DONT_START_TIMER_UNTIL_SCORED_POINT_MULTI)
		AND MC_serverBD.iScoreOnThisRule[iTeam] < g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTimerPointsRequired[MC_ServerBD_4.iCurrentHighestPriority[iTeam]]
			#IF IS_DEBUG_BUILD
			IF NOT IS_BIT_SET(MC_ServerBD.iServerBitSet7, SBBOOL7_BLOCK_MULTI_TIMER_START_DUE_TO_SCORE_FOR_TEAM_0 + iTeam)
				PRINTLN("[OBJECTIVE_LIMITS] PROCESS_OBJECTIVE_LIMITS - Team: ", iTeam, " Setting Block MultiRule Timer bit.")
			ENDIF
			#ENDIF
			SET_BIT(MC_ServerBD.iServerBitSet7, SBBOOL7_BLOCK_MULTI_TIMER_START_DUE_TO_SCORE_FOR_TEAM_0 + iTeam)
		ELSE
			#IF IS_DEBUG_BUILD
			IF IS_BIT_SET(MC_ServerBD.iServerBitSet7, SBBOOL7_BLOCK_MULTI_TIMER_START_DUE_TO_SCORE_FOR_TEAM_0 + iTeam)
				PRINTLN("[OBJECTIVE_LIMITS] PROCESS_OBJECTIVE_LIMITS - Team: ", iTeam, " Clearing Block MultiRule Timer bit.")
			ENDIF
			#ENDIF
			CLEAR_BIT(MC_ServerBD.iServerBitSet7, SBBOOL7_BLOCK_MULTI_TIMER_START_DUE_TO_SCORE_FOR_TEAM_0 + iTeam)
		ENDIF

		IF NOT IS_BIT_SET(g_FMMC_STRUCT.iTimerStop[iTeam], MC_ServerBD_4.iCurrentHighestPriority[iTeam])
			IF IS_BIT_SET(MC_serverBD.iCheckpointBitset, SB_CP_QUICK_RESTART_MRT_FOR_TEAM_0 + iTeam)
				
				PRINTLN("[OBJECTIVE_LIMITS] PROCESS_OBJECTIVE_LIMITS - Team: ", iTeam, " Checkpoint Multi rule timer started on rule: ", MC_ServerBD_4.iCurrentHighestPriority[iTeam], " Time stamp: ", g_TransitionSessionNonResetVars.iRetryMultiRuleTimeStamp[iTeam])
				
				REINIT_NET_TIMER(MC_serverBD_3.tdMultiObjectiveLimitTimer[iTeam])
				SET_BIT(MC_serverBD.tdMultiTimerStartBitset[iTeam], MC_ServerBD_4.iCurrentHighestPriority[iTeam])
				
				MC_serverBD_3.iMultiObjectiveTimeLimit[iTeam] = g_TransitionSessionNonResetVars.iRetryMultiRuleTimeStamp[iTeam]
				CLEAR_BIT(MC_serverBD.iCheckpointBitset, SB_CP_QUICK_RESTART_MRT_FOR_TEAM_0 + iTeam)
				
			ELIF IS_BIT_SET(g_FMMC_STRUCT.iTimerStart[iTeam], MC_ServerBD_4.iCurrentHighestPriority[iTeam])
			AND NOT IS_BIT_SET(MC_ServerBD.iServerBitSet7, SBBOOL7_BLOCK_MULTI_TIMER_START_DUE_TO_SCORE_FOR_TEAM_0 + iTeam)
				IF NOT IS_BIT_SET(MC_serverBD.tdMultiTimerStartBitset[iTeam], MC_ServerBD_4.iCurrentHighestPriority[iTeam])

					REINIT_NET_TIMER(MC_serverBD_3.tdMultiObjectiveLimitTimer[iTeam])
					SET_BIT(MC_serverBD.tdMultiTimerStartBitset[iTeam], MC_ServerBD_4.iCurrentHighestPriority[iTeam])
					
					#IF IS_DEBUG_BUILD
					IF bFMMCOverrideMultiRuleTimer
						MC_serverBD_3.iMultiObjectiveTimeLimit[iTeam] = GET_COMMANDLINE_PARAM_INT("sc_FMMCOverrideMultiRuleTimer")
						PRINTLN("[OBJECTIVE_LIMITS] PROCESS_OBJECTIVE_LIMITS - Team: ", iTeam, " Multi rule timer started on rule: ", MC_ServerBD_4.iCurrentHighestPriority[iTeam], " Time limit from commandline: ", MC_serverBD_3.iMultiObjectiveTimeLimit[iTeam])
					ELSE
					#ENDIF
						MC_serverBD_3.iMultiObjectiveTimeLimit[iTeam] = GET_MULTIRULE_TIMER_TIME_LIMIT_FOR_RULE(iTeam, MC_ServerBD_4.iCurrentHighestPriority[iTeam])
						PRINTLN("[OBJECTIVE_LIMITS] PROCESS_OBJECTIVE_LIMITS - Team: ", iTeam, " Multi rule timer started on rule: ", MC_ServerBD_4.iCurrentHighestPriority[iTeam], " Time limit : ", MC_serverBD_3.iMultiObjectiveTimeLimit[iTeam])
					#IF IS_DEBUG_BUILD
					ENDIF
					#ENDIF
				ENDIF
			ENDIF
		ELSE
			IF IS_MULTIRULE_TIMER_RUNNING(iTeam)
				PRINTLN("[OBJECTIVE_LIMITS] PROCESS_OBJECTIVE_LIMITS - Team: ", iTeam, " Stopping Multi Rule Timer.")
				RESET_NET_TIMER(MC_serverBD_3.tdMultiObjectiveLimitTimer[iTeam])
				MC_serverBD_3.iMultiObjectiveTimeLimit[iTeam] = 0
			ENDIF
			
			#IF IS_DEBUG_BUILD
			IF IS_BIT_SET(MC_ServerBD.iCheckpointBitset, SB_CP_QUICK_RESTART_MRT_FOR_TEAM_0 + iTeam)
				PRINTLN("[OBJECTIVE_LIMITS] PROCESS_OBJECTIVE_LIMITS - Team: ", iTeam, " Clearing Checkpoint Restart MRT Bit.")
			ENDIF
			#ENDIF
			CLEAR_BIT(MC_ServerBD.iCheckpointBitset, SB_CP_QUICK_RESTART_MRT_FOR_TEAM_0 + iTeam)
		ENDIF
		
		IF IS_MULTIRULE_TIMER_RUNNING(iTeam)
		AND HAS_MULTIRULE_TIMER_EXPIRED(iTeam)
			IF SHOULD_MULTIRULE_TIMER_WAIT_FOR_OBJECTIVE_TIMER(iTeam)
				bMultiTimerExpired = FALSE
				PRINTLN("[OBJECTIVE_LIMITS] PROCESS_OBJECTIVE_LIMITS - Team: ", iTeam, " Waiting for Objective timer before moving on.")
			ELSE	
				bMultiTimerExpired = TRUE
				PRINTLN("[OBJECTIVE_LIMITS] PROCESS_OBJECTIVE_LIMITS - Team: ", iTeam, " Multi Rule Timer has expired.")
			ENDIF
		ENDIF
			
		IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjectiveTimeLimitRule[MC_ServerBD_4.iCurrentHighestPriority[iTeam]] != 0
		AND NOT IS_BIT_SET(MC_ServerBD.iServerBitSet7, SBBOOL7_BLOCK_RULE_TIMER_START_DUE_TO_SCORE_FOR_TEAM_0 + iTeam)
			IF NOT IS_OBJECTIVE_TIMER_RUNNING(iTeam)
				PRINTLN("[OBJECTIVE_LIMITS] PROCESS_OBJECTIVE_LIMITS - Team: ", iTeam, " Starting Objective Limit Timer.")
				REINIT_NET_TIMER(MC_serverBD_3.tdObjectiveLimitTimer[iTeam])
				MC_serverBD_3.iTimerPenalty[iTeam] = 0
			ENDIF
								
			IF HAS_OBJECTIVE_TIMER_EXPIRED(iTeam, MC_ServerBD_4.iCurrentHighestPriority[iTeam])
				bRuleTimerExpired = TRUE
			ENDIF
		ENDIF
		
		IF NOT IS_BIT_SET(MC_serverBD.iOptionsMenuBitSet, ciUseCTFAllPoints)
		AND (bRuleTimerExpired OR bMultiTimerExpired)
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciALTERNATIVE_HOLDING_PACKAGE_SCORING)
				PROCESS_HOLDING_PACKAGE_SCORING()
			ENDIF
			
			BOOL bHasSuddenDeathEnding
			IF DOES_MISSION_HAVE_SUDDEN_DEATH_ENDING()
				IF (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciMULTIRULE_TIMER_SUDDEN_DEATH) AND bMultiTimerExpired)
				OR ((NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciMULTIRULE_TIMER_SUDDEN_DEATH)) AND bRuleTimerExpired)
					PRINTLN("[OBJECTIVE_LIMITS] PROCESS_OBJECTIVE_LIMITS - Team: ", iTeam, " Mission has a sudden death ending.")
					bHasSuddenDeathEnding = TRUE
				ENDIF				
			ENDIF
			
			BOOL bShouldProcessSuddenDeath = FALSE
			IF bHasSuddenDeathEnding					
				IF NOT ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
					IF SHOULD_PROGRESS_TO_SUDDEN_DEATH(iTeam, iTeamAliveAsSuddenDeathStarts)
						bShouldProcessSuddenDeath = TRUE
						PRINTLN("[OBJECTIVE_LIMITS] PROCESS_OBJECTIVE_LIMITS - Team: ", iTeam, " Progress to Sudden Death (1).")
					ENDIF
				ELSE
					//Already in Sudden Death so True
					bShouldProcessSuddenDeath = TRUE
				ENDIF
			ENDIF
								
			IF bShouldProcessSuddenDeath
				PROCESS_SUDDEN_DEATH_OBJECTIVE_LOGIC(iTeam, MC_ServerBD_4.iCurrentHighestPriority[iTeam], bObjectiveOver[iTeam], bObjectivePassed[iTeam], IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciMULTIRULE_TIMER_SUDDEN_DEATH))
			ELSE
				MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_TIME_EXPIRED
				IF NOT bMultiTimerExpired
					PRINTLN("[OBJECTIVE_LIMITS] PROCESS_OBJECTIVE_LIMITS - Team: ", iTeam, " Rule timer (", MC_ServerBD_4.iCurrentHighestPriority[iTeam], ") Expired - Objective Over")
					bObjectiveOver[iTeam] = TRUE
				ELSE
					IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitsetSeven, ciMULTIRULE_TIMER_WINNING_TEAM_STILL_WINS)
					OR NOT IS_TEAM_LEADING_ON_POINTS(iTeam)
					AND iTeamAliveAsSuddenDeathStarts != iTeam
						bFailMission[iTeam] = TRUE
						SET_TEAM_OBJECTIVE_OUTCOME(iTeam, MC_ServerBD_4.iCurrentHighestPriority[iTeam], FALSE)
						IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwelve[MC_ServerBD_4.iCurrentHighestPriority[iTeam]], ciBS_RULE12_DONT_FAIL_ON_MULTI_RULE) 
							PRINTLN("[OBJECTIVE_LIMITS] PROCESS_OBJECTIVE_LIMITS - Team: ", iTeam, " MultiRule timer (", MC_ServerBD_4.iCurrentHighestPriority[iTeam], ") Expired - Objective Over")
							bObjectiveOver[iTeam] = TRUE
						ELSE
							PRINTLN("[OBJECTIVE_LIMITS] PROCESS_OBJECTIVE_LIMITS - Team: ", iTeam, " MultiRule timer (", MC_ServerBD_4.iCurrentHighestPriority[iTeam], ") Expired - Mission Failed")
							REQUEST_SET_TEAM_FAILED(iTeam, mFail_MULTI_TIME_EXPIRED)
						ENDIF
					ELSE
						PRINTLN("[OBJECTIVE_LIMITS] PROCESS_OBJECTIVE_LIMITS - Team: ", iTeam, " MultiRule timer (", MC_ServerBD_4.iCurrentHighestPriority[iTeam], ") Expired - Winning On Points")
						bObjectiveOver[iTeam] = TRUE
						bObjectivePassed[iTeam] = TRUE
						GIVE_TEAM_ENOUGH_POINTS_TO_PASS(iTeam)
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwenty, ciOptionsBS20_END_ROUND_RESTART_MULTIRULE_TIME)
							SET_BIT(MC_ServerBD.iServerbitSet7, SBBOOL7_MULTI_RULE_TIMER_ROUND_RESTART_ENDED)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF

		IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTargetScore[MC_ServerBD_4.iCurrentHighestPriority[iTeam]] != FMMC_TARGET_SCORE_OFF
			IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTargetScore[MC_ServerBD_4.iCurrentHighestPriority[iTeam]] = FMMC_TARGET_SCORE_PLAYER
				IF MC_serverBD.iNumberOfPlayingPlayers[iTeam] > 0
					iScoreLimit = MC_serverBD.iNumberOfPlayingPlayers[iTeam]
				ELSE
					bNoPlayersInTeam[iTeam] = TRUE
				ENDIF
			ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTargetScore[MC_ServerBD_4.iCurrentHighestPriority[iTeam]] = FMMC_TARGET_SCORE_PLAYER_HALF
				IF MC_serverBD.iNumberOfPlayingPlayers[iTeam] > 0
					iScoreLimit = IMAX(FLOOR(MC_serverBD.iNumberOfPlayingPlayers[iTeam] / 2.0), 1)
				ELSE
					bNoPlayersInTeam[iTeam] = TRUE
				ENDIF
			ELSE
				iScoreLimit = ROUND(GET_FMMC_OBJECTIVE_SCORE_LIMIT_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTargetScore[MC_ServerBD_4.iCurrentHighestPriority[iTeam]], MC_ServerBD.iNumberOfPlayingPlayers[iTeam], MC_serverBD.iMaxNumPriorityEntitiesThisRule[iTeam])*GET_MULTIPLAYER_BASED_ON_PLAYERS_AND_CORONA_SETTING(iTeam, MC_ServerBD_4.iCurrentHighestPriority[iTeam]))
			ENDIF
								
			IF NOT bNoPlayersInTeam[iTeam]
				IF GET_TEAM_SCORE_ON_CURRENT_RULE(iTeam) >= iScoreLimit
					PRINTLN("[OBJECTIVE_LIMITS] PROCESS_OBJECTIVE_LIMITS - Team: ", iTeam, " Score Limit (", iScoreLimit, ") for rule ", MC_ServerBD_4.iCurrentHighestPriority[iTeam], " Expired - Objective Over")
					MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_TARGET_SCORE
					bObjectiveOver[iTeam] = TRUE
					bObjectivePassed[iTeam] = TRUE
				ENDIF
			ENDIF
		ENDIF
	
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iLoseWantedBitset, MC_serverBD_4.iCurrentHighestPriority[iTeam])
		AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitset[ MC_serverBD_4.iCurrentHighestPriority[iTeam]], ciBS_RULE_WANTED_END_OBJ)
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitset[MC_ServerBD_4.iCurrentHighestPriority[iTeam]], ciBS_RULE_WANTED_TEAM_OBJ)
				IF MC_serverBD.iNumOfWanted[iTeam] = 0
					IF NOT ARE_ANY_PLAYERS_ON_TEAM_DEAD_OR_BLOCKING_OBJECTIVE(iTeam)
						bObjectiveOver[iTeam] = TRUE
						bObjectivePassed[iTeam] = TRUE
						MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_LOST_COPS
						PRINTLN("[OBJECTIVE_LIMITS] PROCESS_OBJECTIVE_LIMITS - Team: ", iTeam, " Lost Wanted - Objective Over")
					ELSE
						PRINTLN("[OBJECTIVE_LIMITS] PROCESS_OBJECTIVE_LIMITS - Team: ", iTeam, " Lost Wanted (although some people are dead) - Waiting")
					ENDIF
				ENDIF
			ELIF IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitset[MC_ServerBD_4.iCurrentHighestPriority[iTeam]], ciBS_RULE_WANTED_COOP_TEAMS_OBJ)
				BOOL bWanted
				INT iTeamLoop
				FOR iTeamLoop = 0 TO MC_serverBD.iNumberOfTeams - 1
					IF DOES_TEAM_LIKE_TEAM(iTeam, iTeamLoop)
						IF MC_serverBD.iNumOfWanted[iTeamLoop] > 0
							bWanted = TRUE
						ENDIF
					ENDIF
				ENDFOR
				
				IF NOT bWanted
					bObjectiveOver[iTeam] = TRUE
					bObjectivePassed[iTeam] = TRUE
					MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_LOST_COPS
						PRINTLN("[OBJECTIVE_LIMITS] PROCESS_OBJECTIVE_LIMITS - Team: ", iTeam, " Lost Wanted (Coop) - Objective Over")
				ENDIF
			ELIF MC_serverBD.iNumOfWanted[iTeam] < MC_serverBD.iNumberOfPlayingPlayers[iTeam]
				bObjectiveOver[iTeam] = TRUE
				bObjectivePassed[iTeam] = TRUE
				MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_LOST_COPS
				PRINTLN("[OBJECTIVE_LIMITS] PROCESS_OBJECTIVE_LIMITS - Team: ", iTeam, " Lost Wanted (Single Player) - Objective Over")
			ENDIF
		ENDIF

		IF bObjectiveOver[iTeam] = TRUE
			BOOL bLimitForceEnd, bForcePass
			
			IF MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_LOST_COPS
				INCREMENT_SERVER_TEAM_SCORE(iTeam, MC_ServerBD_4.iCurrentHighestPriority[iTeam], GET_FMMC_POINTS_FOR_TEAM(iTeam, MC_ServerBD_4.iCurrentHighestPriority[iTeam]))
				bLimitForceEnd = TRUE
				bForcePass = TRUE
			ELIF MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_TIME_EXPIRED
				bLimitForceEnd = NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamFailBitset, MC_ServerBD_4.iCurrentHighestPriority[iTeam])
			ELSE
				bLimitForceEnd = TRUE
			ENDIF
			
			INT iRuleToJumpTo = MC_ServerBD_4.iCurrentHighestPriority[iTeam]
			IF MC_serverBD.iNextObjective[iTeam] > -1
			AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwelve[MC_ServerBD_4.iCurrentHighestPriority[iTeam]], ciBS_RULE12_DONT_FAIL_ON_MULTI_RULE)
				iRuleToJumpTo = MC_serverBD.iNextObjective[iTeam]-1
				PRINTLN("[OBJECTIVE_LIMITS] PROCESS_OBJECTIVE_LIMITS - Team: ", iTeam, "Progressing Objective from ", MC_ServerBD_4.iCurrentHighestPriority[iTeam], " to ", iRuleToJumpTo)
				IF INVALIDATE_UP_TO_AND_INCLUDING_OBJECTIVE(iTeam, iRuleToJumpTo, bLimitForceEnd, bForcePass)
					bObjectivePassed[iTeam] = TRUE
				ENDIF
			ELSE
				PRINTLN("[OBJECTIVE_LIMITS] PROCESS_OBJECTIVE_LIMITS - Team: ", iTeam, " Progressing Objective ", MC_ServerBD_4.iCurrentHighestPriority[iTeam])
				INVALIDATE_SINGLE_OBJECTIVE(iTeam, iRuleToJumpTo, bLimitForceEnd, bForcePass)
			ENDIF
		ENDIF
	ENDFOR

	FOR iTeam = 0 TO MC_serverBD.iNumberOfTeams - 1
		IF MC_serverBD_4.iCurrentHighestPriority[iTeam] >= FMMC_MAX_RULES
			RELOOP
		ENDIF
		
		IF !bObjectiveOver[iTeam]
			RELOOP
		ENDIF
		
		BOOL bNoOneInLocate = TRUE
				
		IF bObjectivePassed[iTeam]
			SET_TEAM_OBJECTIVE_OUTCOME(iTeam, MC_ServerBD_4.iCurrentHighestPriority[iTeam], TRUE)
			PRINTLN("[OBJECTIVE_LIMITS] PROCESS_OBJECTIVE_LIMITS - Team: ", iTeam, " Passed Objective")
		ELSE
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamFailBitset, MC_ServerBD_4.iCurrentHighestPriority[iTeam])
				bFailMission[iTeam] = TRUE
				REQUEST_SET_TEAM_FAILED(iTeam, mFail_TIME_EXPIRED)
				PRINTLN("[OBJECTIVE_LIMITS] PROCESS_OBJECTIVE_LIMITS - Team: ", iTeam, " Mission Failed because Objective Limit expired.")
			ELIF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEight[MC_ServerBD_4.iCurrentHighestPriority[iTeam]], ciBS_RULE8_KILL_PLAYERS_NOT_AT_LOCATE)	
				INT iPart = -1
				WHILE DO_PARTICIPANT_LOOP(iPart)
					IF MC_playerBD[iPart].iCurrentLoc = -1
						PRINTLN("[OBJECTIVE_LIMITS] PROCESS_OBJECTIVE_LIMITS - Team: ", iTeam, " Participant ", iPart, " not at locate")
						BROADCAST_FMMC_KILL_PLAYER_NOT_AT_LOCATE(iPart)
					ELSE
						bNoOneInLocate = FALSE
					ENDIF
				ENDWHILE
				
				IF bNoOneInLocate
					bFailMission[iTeam] = TRUE
					REQUEST_SET_TEAM_FAILED(iTeam, mFail_TIME_EXPIRED)
					PRINTLN("[OBJECTIVE_LIMITS] PROCESS_OBJECTIVE_LIMITS - Team: ", iTeam, " Mission Failed because Objective Limit expired and no one in locate.")
				ENDIF
			ENDIF
			
			PRINTLN("[OBJECTIVE_LIMITS] PROCESS_OBJECTIVE_LIMITS - Team: ", iTeam, " Failed Objective")
			SET_TEAM_OBJECTIVE_OUTCOME(iTeam, MC_ServerBD_4.iCurrentHighestPriority[iTeam], FALSE)	
		ENDIF
				
		RESET_NET_TIMER(MC_serverBD_3.tdObjectiveLimitTimer[iTeam])
		MC_serverBD_3.iTimerPenalty[iTeam] = 0
		SET_LOCAL_OBJECTIVE_END_MESSAGE(iTeam, MC_ServerBD_4.iCurrentHighestPriority[iTeam], MC_serverBD.iReasonForObjEnd[iTeam], bFailMission[iTeam])
		BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[iTeam], iTeam, MC_ServerBD_4.iCurrentHighestPriority[iTeam], DID_TEAM_PASS_OBJECTIVE(iTeam, MC_ServerBD_4.iCurrentHighestPriority[iTeam]), bFailMission[iTeam])
		
		RECALCULATE_OBJECTIVE_LOGIC(iTeam)
		
	ENDFOR

ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Team Position Sorting
// ##### Description: Functions for comparing teams in competitive modes to determine the current winning team.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------		

FUNC eTeamOutcome COMPARE_TEAMS_ON_CAPTURE_BARS(INT iTeam1, INT iTeam2)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitsetEight, ciInclude_Capture_Progress_When_Sorting_Teams)
		RETURN TO_Draw
	ENDIF
		
	FLOAT fCaptureProgress[2]
	
	INT iTeamLoop, iArray
	
	FOR iTeamLoop = 0 TO MC_serverBD.iNumberOfTeams - 1
		
		IF iTeamLoop = iTeam1
			iArray = 0
		ELIF iTeamLoop = iTeam2
			iArray = 1
		ELSE
			RELOOP
		ENDIF
		
		// Currently vehicles are the only entities that have capture progress that is retained, so they're the only ones we can check when players aren't actively capturing them:
		INT iVeh
		FOR iVeh = 0 TO (MC_serverBD.iNumVehCreated - 1)
			IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iPriority[iTeamLoop] < FMMC_MAX_RULES
				IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iRule[iTeamLoop] = FMMC_OBJECTIVE_LOGIC_CAPTURE
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFour, ciFMMC_VEHICLE4_VEHICLE_RETAIN_CAPTURE_PROGRESS)
						fCaptureProgress[iArray] += TO_FLOAT(iStoredVehCaptureMS[iTeamLoop][iVeh]) / TO_FLOAT(MC_serverBD.iVehTakeoverTime[iTeamLoop][g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iPriority[iTeamLoop]])
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciSUMO_ZONE_CAPTURE_BAR_WINNER)
			INT iLoc
			FOR iLoc = 0 TO MC_serverBD.iNumLocCreated - 1
				IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iPriority[iTeamLoop] < FMMC_MAX_RULES
					IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iRule[iTeamLoop] = FMMC_OBJECTIVE_LOGIC_CAPTURE
						fCaptureProgress[iArray] += iCurrCapTime[iLoc][iTeamLoop]
					ENDIF
				ENDIF
			ENDFOR
		ENDIF
		
	ENDFOR
	
	IF fCaptureProgress[0] > fCaptureProgress[1]
		RETURN TO_Win
	ELIF fCaptureProgress[0] < fCaptureProgress[1]
		RETURN TO_Lose
	ELSE
		RETURN TO_Draw
	ENDIF
	
ENDFUNC

FUNC BOOL IS_TEAM_BEATING_TEAM_ON_MISSION_TIME(INT iTeam1, INT iTeam2)
	
	INT iTeam1Time = MC_serverBD_3.iTeamMissionTime[iTeam1]
	INT iTeam2Time = MC_serverBD_3.iTeamMissionTime[iTeam2]
	
	BOOL bFail1 = HAS_TEAM_FAILED(iTeam1)
	BOOL bFail2 = HAS_TEAM_FAILED(iTeam2)
	
	IF iTeam2Time = 0 //Team 2 is still playing
		IF iTeam1Time != 0
		AND NOT bFail1
			//Team 1 already finished successfully
			RETURN TRUE
		ELSE
			//Either Team 1 is also still going, or we've failed
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF iTeam1Time = 0 //Team 1 is still playing, and Team 2 has finished
		IF NOT bFail2
			//Team 2 has finished before Team 1
			RETURN FALSE
		ELSE
			//Team 2 has failed before Team 1
			RETURN TRUE
		ENDIF
	ENDIF
	
	//Both teams have finished/failed
	
	IF iTeam1Time < iTeam2Time
		IF bFail1 AND bFail2
			// Team 1 failed faster
			RETURN FALSE
		ELSE
			// Team 1 finished faster
			RETURN TRUE
		ENDIF
	ELSE
		IF bFail1 AND bFail2
			//Team 2 failed faster
			RETURN TRUE
		ELSE
			//Team 2 finished faster
			RETURN FALSE
		ENDIF
	ENDIF
	
ENDFUNC

FUNC FLOAT GET_KDR_FOR_TEAM_COMPARISON(INT iTeam)

	FLOAT fKillDeathRatio = 0.0
	
	IF GET_TEAM_DEATHS(iTeam) > 0
	
		fKillDeathRatio = TO_FLOAT(GET_TEAM_KILLS(iTeam)) / TO_FLOAT(GET_TEAM_DEATHS(iTeam))
	
	ELIF GET_TEAM_DEATHS(iTeam) = 0
		IF GET_TEAM_KILLS(iTeam) > 0
			fKillDeathRatio = TO_FLOAT(GET_TEAM_KILLS(iTeam))
		ENDIF
	ENDIF
	
	IF fKillDeathRatio < 0.0
		fKillDeathRatio = 0.0
	ENDIF
	
	RETURN fKillDeathRatio
	
ENDFUNC

FUNC BOOL IS_TEAM_BEATING_TEAM(INT iTeam1, INT iTeam2)
	
	INT iTeamScores[FMMC_MAX_TEAMS]
	IF g_FMMC_STRUCT.iMissionEndType = ciMISSION_SCORING_TYPE_TIME
		COPY_INT_ARRAY_VALUES(MC_serverBD.iAverageMissionTime, iTeamScores, -1)
	ELSE
		COPY_INT_ARRAY_VALUES(MC_serverBD.iTeamScore, iTeamScores)
	ENDIF
	
	IF HAS_TEAM_FAILED(iTeam1) AND NOT HAS_TEAM_FAILED(iTeam2)
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_TEAM_FAILED(iTeam1) AND HAS_TEAM_FAILED(iTeam2)
		RETURN TRUE
	ENDIF
	
	IF iTeamScores[iTeam1] < iTeamScores[iTeam2]
		RETURN FALSE
	ENDIF
	
	IF iTeamScores[iTeam1] > iTeamScores[iTeam2]
		RETURN TRUE
	ENDIF
	
	//Scores are equal, do a tie breaker
	
	//Capture Progress
	eTeamOutcome eCapOutcome = COMPARE_TEAMS_ON_CAPTURE_BARS(iTeam1, iTeam2)
	IF eCapOutcome = TO_Lose
		RETURN FALSE
	ELIF eCapOutcome = TO_Win
		RETURN TRUE
	ENDIF
	
	//Kills
	IF GET_TEAM_KILLS(iTeam1) < GET_TEAM_KILLS(iTeam2)
		RETURN FALSE
	ELIF GET_TEAM_KILLS(iTeam1) > GET_TEAM_KILLS(iTeam2)
		RETURN TRUE	
	ENDIF	
	
	//Deaths
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciIGNORE_DEATHS_WHEN_SORTING_TEAMS)
		IF GET_TEAM_DEATHS(iTeam1) > GET_TEAM_DEATHS(iTeam2)
			RETURN FALSE
		ELIF GET_TEAM_DEATHS(iTeam1) < GET_TEAM_DEATHS(iTeam2)
			RETURN TRUE
		ENDIF
	ENDIF	
	
	//Headshots
	IF GET_TEAM_HEADSHOTS(iTeam1) < GET_TEAM_HEADSHOTS(iTeam2)
		RETURN FALSE
	ELIF GET_TEAM_HEADSHOTS(iTeam1) > GET_TEAM_HEADSHOTS(iTeam2)
		RETURN TRUE
	ENDIF
	
	//Kill/Death Ratio
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciIGNORE_DEATHS_WHEN_SORTING_TEAMS)
		FLOAT fKillDeathRatio1 = GET_KDR_FOR_TEAM_COMPARISON(iTeam1)
		FLOAT fKillDeathRatio2 = GET_KDR_FOR_TEAM_COMPARISON(iTeam2)

		IF fKillDeathRatio1 < fKillDeathRatio2
			RETURN FALSE
		ELIF fKillDeathRatio1 > fKillDeathRatio2
			RETURN TRUE
		ENDIF
	ENDIF
	
	//Mission Time
	IF IS_TEAM_BEATING_TEAM_ON_MISSION_TIME(iTeam1, iTeam2)
		RETURN TRUE
	ELIF MC_serverBD_3.iTeamMissionTime[iTeam1] = MC_serverBD_3.iTeamMissionTime[iTeam2]
		//Still drawing... Lowest team index wins.
		IF iTeam1 < iTeam2
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    Sorts the playing teams into winning order on all vs 1 scoring missions.
///    Note: This function looks a mess but this is more efficient than a loop that compares every team to every other team and this is called every frame on the host.
PROC SORT_ALL_VS_1_TEAMS(INT &iTeamPositions[FMMC_MAX_TEAMS])
	
	BOOL bTeam0Win = FALSE
	
	IF NOT HAS_TEAM_FAILED(0)
		
		INT iPassScore
		
		IF g_FMMC_STRUCT.sFMMCEndConditions[0].iMissionTargetScore = -1
			IF MC_serverBD.iNumberOfPlayingPlayers[0] > 0
				iPassScore = MC_serverBD.iNumberOfPlayingPlayers[0]
			ELSE
				iPassScore = 1
			ENDIF
		ELIF g_FMMC_STRUCT.sFMMCEndConditions[0].iMissionTargetScore != 0
			iPassScore = g_FMMC_STRUCT.sFMMCEndConditions[0].iMissionTargetScore
		ELSE
			iPassScore = 1
		ENDIF
		
		IF ((MC_serverBD.iTeamScore[0] + iEarlyCelebrationTeamPoints[0]) >= iPassScore)
			bTeam0Win = TRUE
		ENDIF
		
	ENDIF
	
	IF MC_serverBD.iNumActiveTeams = 4
		IF IS_TEAM_BEATING_TEAM(2,1)
			IF IS_TEAM_BEATING_TEAM(1,3)
				IF IS_TEAM_BEATING_TEAM(3,2)
					IF bTeam0Win
						iTeamPositions[TEAM_WINNER] = 0
						iTeamPositions[TEAM_SECOND] = 3
						iTeamPositions[TEAM_THIRD] = 2
						iTeamPositions[TEAM_LOSER] = 1
					ELSE
						iTeamPositions[TEAM_WINNER] = 3
						iTeamPositions[TEAM_SECOND] = 2
						iTeamPositions[TEAM_THIRD] = 1
						iTeamPositions[TEAM_LOSER] = 0
					ENDIF
				ELSE
					IF bTeam0Win
						iTeamPositions[TEAM_WINNER] = 0
						iTeamPositions[TEAM_SECOND] = 2
						iTeamPositions[TEAM_THIRD] = 3
						iTeamPositions[TEAM_LOSER] = 1
					ELSE
						iTeamPositions[TEAM_WINNER] = 2
						iTeamPositions[TEAM_SECOND] = 3
						iTeamPositions[TEAM_THIRD] = 1
						iTeamPositions[TEAM_LOSER] = 0
					ENDIF
				ENDIF
			ELSE
				IF bTeam0Win
					iTeamPositions[TEAM_WINNER] = 0
					iTeamPositions[TEAM_SECOND] = 2
					iTeamPositions[TEAM_THIRD] = 1
					iTeamPositions[TEAM_LOSER] = 3
				ELSE
					iTeamPositions[TEAM_WINNER] = 2
					iTeamPositions[TEAM_SECOND] = 1
					iTeamPositions[TEAM_THIRD] = 3
					iTeamPositions[TEAM_LOSER] = 0
				ENDIF
			ENDIF	
		ELSE
			IF IS_TEAM_BEATING_TEAM(3,2)
				IF IS_TEAM_BEATING_TEAM(3,1)
					IF bTeam0Win
						iTeamPositions[TEAM_WINNER] = 0
						iTeamPositions[TEAM_SECOND] = 3
						iTeamPositions[TEAM_THIRD] = 1
						iTeamPositions[TEAM_LOSER] = 2
					ELSE
						iTeamPositions[TEAM_WINNER] = 3
						iTeamPositions[TEAM_SECOND] = 1
						iTeamPositions[TEAM_THIRD] = 2
						iTeamPositions[TEAM_LOSER] = 0
					ENDIF
				ELSE
					IF bTeam0Win
						iTeamPositions[TEAM_WINNER] = 0
						iTeamPositions[TEAM_SECOND] = 1
						iTeamPositions[TEAM_THIRD] = 3
						iTeamPositions[TEAM_LOSER] = 2
					ELSE
						iTeamPositions[TEAM_WINNER] = 1
						iTeamPositions[TEAM_SECOND] = 3
						iTeamPositions[TEAM_THIRD] = 2
						iTeamPositions[TEAM_LOSER] = 0
					ENDIF
				ENDIF
			ELSE
				IF bTeam0Win
					iTeamPositions[TEAM_WINNER] = 0
					iTeamPositions[TEAM_SECOND] = 1
					iTeamPositions[TEAM_THIRD] = 2
					iTeamPositions[TEAM_LOSER] = 3
				ELSE
					iTeamPositions[TEAM_WINNER] = 1
					iTeamPositions[TEAM_SECOND] = 2
					iTeamPositions[TEAM_THIRD] = 3
					iTeamPositions[TEAM_LOSER] = 0
				ENDIF
			ENDIF
		ENDIF
	ELIF MC_serverBD.iNumActiveTeams = 3
		IF IS_TEAM_BEATING_TEAM(2,1)
			IF bTeam0Win
				iTeamPositions[TEAM_WINNER] = 0
				iTeamPositions[TEAM_SECOND] = 2
				iTeamPositions[TEAM_LOSER] = 1
			ELSE
				iTeamPositions[TEAM_WINNER] = 2
				iTeamPositions[TEAM_SECOND] = 1
				iTeamPositions[TEAM_LOSER] = 0
			ENDIF
		ELSE
			IF bTeam0Win
				iTeamPositions[TEAM_WINNER] = 0
				iTeamPositions[TEAM_SECOND] = 1
				iTeamPositions[TEAM_LOSER] = 2
			ELSE
				iTeamPositions[TEAM_WINNER] = 1
				iTeamPositions[TEAM_SECOND] = 2
				iTeamPositions[TEAM_LOSER] = 0
			ENDIF
		ENDIF
	ELIF MC_serverBD.iNumActiveTeams = 2
		IF bTeam0Win
			iTeamPositions[TEAM_WINNER] = 0
			iTeamPositions[TEAM_LOSER] = 1
		ELSE
			iTeamPositions[TEAM_WINNER] = 1
			iTeamPositions[TEAM_LOSER] = 0
		ENDIF
	ELSE
		iTeamPositions[TEAM_WINNER] = 0
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Sorts the playing teams into winning order.
///    Note: This function looks a mess but this is more efficient than a loop that compares every team to every other team and this is called every frame on the host.
PROC SORT_MISSION_WINNING_TEAMS(INT &iTeamPositions[FMMC_MAX_TEAMS])
		
	INT iTempLow0, iTempLow1, iTempHigh0, iTempHigh1, iTempMiddle0, iTempMiddle1
	
	IF MC_serverBD.iNumActiveTeams = 4
		
		//Four teams requires us to find the two worst teams and the two best teams then compare them to fill each position
		IF IS_TEAM_BEATING_TEAM(1, 0)
			iTempLow0 = 0
			iTempHigh0 = 1
		ELSE
			iTempLow0 = 1
			iTempHigh0 = 0
		ENDIF
		
		IF IS_TEAM_BEATING_TEAM(3, 2)
			iTempLow1 = 2
			iTempHigh1 = 3
		ELSE
			iTempLow1 = 3
			iTempHigh1 = 2
		ENDIF
		
		//Set the losing team to the worst of the lows
		IF IS_TEAM_BEATING_TEAM(iTempLow1, iTempLow0)
			iTeamPositions[TEAM_LOSER] = iTempLow0
			iTempMiddle0 = iTempLow1
		ELSE
			iTeamPositions[TEAM_LOSER] = iTempLow1
			iTempMiddle0 = iTempLow0
		ENDIF
		
		//Set the winning team to the best of the highs
		IF IS_TEAM_BEATING_TEAM(iTempHigh0, iTempHigh1)
			iTeamPositions[TEAM_WINNER] = iTempHigh0
			iTempMiddle1 = iTempHigh1
		ELSE
			iTeamPositions[TEAM_WINNER] = iTempHigh1
			iTempMiddle1 = iTempHigh0
		ENDIF
		
		//Set the middle teams
		IF IS_TEAM_BEATING_TEAM(iTempMiddle0, iTempMiddle1)
			iTeamPositions[TEAM_SECOND] = iTempMiddle0
			iTeamPositions[TEAM_THIRD] = iTempMiddle1
		ELSE
			iTeamPositions[TEAM_SECOND] = iTempMiddle1
			iTeamPositions[TEAM_THIRD] = iTempMiddle0
		ENDIF
		
	ELIF MC_serverBD.iNumActiveTeams = 3
		
		//Compare 1 and 0 to get an initial high and low team
		IF IS_TEAM_BEATING_TEAM(1, 0)
			iTempLow0 = 0
			iTempHigh0 = 1
		ELSE
			iTempLow0 = 1
			iTempHigh0 = 0
		ENDIF
		
		//Compare the high and low teams to the remaining team
		IF IS_TEAM_BEATING_TEAM(2, iTempLow0)
			IF IS_TEAM_BEATING_TEAM(2, iTempHigh0)
				//2 is the winner
				iTeamPositions[TEAM_WINNER] = 2
				iTeamPositions[TEAM_SECOND] = iTempHigh0
				iTeamPositions[TEAM_LOSER] = iTempLow0
			ELSE
				//2 is in the middle
				iTeamPositions[TEAM_WINNER] = iTempHigh0
				iTeamPositions[TEAM_SECOND] = 2
				iTeamPositions[TEAM_LOSER] = iTempLow0
			ENDIF
		ELSE 
			//2 is in last place
			iTeamPositions[TEAM_WINNER] = iTempHigh0
			iTeamPositions[TEAM_SECOND] = iTempLow0
			iTeamPositions[TEAM_LOSER] = 2
		ENDIF
		
	ELIF MC_serverBD.iNumActiveTeams = 2
		
		//Two teams so only one comparison is required
		IF IS_TEAM_BEATING_TEAM(1, 0)
			iTeamPositions[TEAM_WINNER] = 1
			iTeamPositions[TEAM_LOSER] = 0
		ELSE
			iTeamPositions[TEAM_WINNER] = 0
			iTeamPositions[TEAM_LOSER] = 1
		ENDIF
		
	ELSE
	
		//Only one team playing so set the winner as the only team
		iTeamPositions[TEAM_WINNER] = MC_playerBD[iPartToUse].iTeam
		
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Called every frame to maintain the team positions.
PROC PROCESS_SERVER_SORTING_WINNING_TEAMS()

	INT iTeamPositions[FMMC_MAX_TEAMS]
	INITIALISE_INT_ARRAY(iTeamPositions, TEAM_INVALID)
	
	IF g_FMMC_STRUCT.iMissionEndType = ciMISSION_SCORING_TYPE_ALL_VS_1
		SORT_ALL_VS_1_TEAMS(iTeamPositions)
	ELSE
		SORT_MISSION_WINNING_TEAMS(iTeamPositions)
	ENDIF
	
	IF MC_serverBD.iWinningTeam != iTeamPositions[TEAM_WINNER]
		PRINTLN("[TEAM_SORTING] PROCESS_SERVER_SORTING_WINNING_TEAMS - Winning Team updated from ", MC_serverBD.iWinningTeam, " to ", iTeamPositions[TEAM_WINNER])
		MC_serverBD.iWinningTeam = iTeamPositions[TEAM_WINNER]		
	ENDIF
	
	IF MC_serverBD.iSecondTeam != iTeamPositions[TEAM_SECOND]
		PRINTLN("[TEAM_SORTING] PROCESS_SERVER_SORTING_WINNING_TEAMS - Second Team updated from ", MC_serverBD.iSecondTeam, " to ", iTeamPositions[TEAM_SECOND])
		MC_serverBD.iSecondTeam = iTeamPositions[TEAM_SECOND]		
	ENDIF
	
	IF MC_serverBD.iThirdTeam != iTeamPositions[TEAM_THIRD]
		PRINTLN("[TEAM_SORTING] PROCESS_SERVER_SORTING_WINNING_TEAMS - Third Team updated from ", MC_serverBD.iThirdTeam, " to ", iTeamPositions[TEAM_THIRD])
		MC_serverBD.iThirdTeam = iTeamPositions[TEAM_THIRD]		
	ENDIF
	
	IF MC_serverBD.iLosingTeam != iTeamPositions[TEAM_LOSER]
		PRINTLN("[TEAM_SORTING] PROCESS_SERVER_SORTING_WINNING_TEAMS - Losing Team updated from ", MC_serverBD.iLosingTeam, " to ", iTeamPositions[TEAM_LOSER])
		MC_serverBD.iLosingTeam = iTeamPositions[TEAM_LOSER]		
	ENDIF
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Server Request Handlers
// ##### Description: Functions that handle running logic to action requests from other areas of the controller.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Sets a team as failed if SET_TEAM_FAILED was called elsewhere
PROC PROCESS_FAIL_MISSION_REQUEST_FOR_TEAM(INT iTeam)
	
	IF MC_serverBD.iTeamFailRequests[iTeam] = 0
		EXIT
	ENDIF
	
	SET_TEAM_FAILED_FROM_REQUEST(iTeam)
	MC_serverBD.iTeamFailRequests[iTeam] = 0

ENDPROC

/// PURPOSE:
///    Recalculate's a team's objective if REQUEST_RECALCULATE_OBJECTIVE_LOGIC_FOR_TEAM was called elsewhere
PROC PROCESS_RECALCULATE_OBJECTIVE_LOGIC_REQUEST_FOR_TEAM(INT iTeam)
	
	IF NOT SHOULD_RECALCULATE_OBJECTIVE_FOR_TEAM(iTeam)
		EXIT
	ENDIF
	
	RECALCULATE_OBJECTIVE_LOGIC(iTeam)
	CLEAR_RECALCULATE_OBJECTIVE_FOR_TEAM(iTeam)
	
ENDPROC

PROC PROCESS_SERVER_SCORE_REQUESTS()

	INT i
	FOR i = 0 TO ciMAX_SERVER_INCREMENT_SCORE_REQUESTS - 1
		IF MC_serverBD.iIncrementRemotePlayerScoreRequests[i] = 0
			RELOOP
		ENDIF
		
		INCREMENT_SERVER_REMOTE_PLAYER_SCORE_FROM_REQUEST(i)
		MC_serverBD.iIncrementRemotePlayerScoreRequests[i] = 0
	ENDFOR
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: End Conditions
// ##### Description: Functions used to decide if the mission should end.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

#IF IS_DEBUG_BUILD
FUNC BOOL SHOULD_SKIP_CRITICAL_NUMBER_END_CONDITIONS()
	
	IF bAllowAllMissionsWithTwoPlayers
		RETURN TRUE
	ENDIF
	
	IF IS_FAKE_MULTIPLAYER_MODE_SET()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC
#ENDIF

FUNC BOOL HAS_PLAYER_BOUNDS_CAUSED_FAIL(INT iTeam)
	
	INT iPart
	FOR iPart = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION() - 1
		
		PARTICIPANT_INDEX tempPart = INT_TO_PARTICIPANTINDEX(iPart)
		IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
			RELOOP
		ENDIF
		
		IF MC_playerBD[iPart].iTeam != iTeam
			RELOOP
		ENDIF
		
		IF IS_BIT_SET(MC_playerBD[iPart].iClientBitSet2, PBBOOL2_PLAYER_LEAVE_AREA_FAIL)
			PRINTLN("[END_CONDITIONS] HAS_PLAYER_BOUNDS_CAUSED_FAIL - Participant: ", iPart, " has caused a leave area fail.")
			SET_TEAM_FAIL_REASON(iTeam, mFail_LVE_LOC, TRUE, iPart)
			RETURN TRUE
		ENDIF
		
		IF IS_BIT_SET(MC_playerBD[iPart].iClientBitSet2, PBBOOL2_PLAYER_OUT_BOUNDS_FAIL)
			PRINTLN("[END_CONDITIONS] HAS_PLAYER_BOUNDS_CAUSED_FAIL - Participant: ", iPart, " has caused a bounds fail.")
					SET_TEAM_FAIL_REASON(iTeam, mFail_OUT_OF_BOUNDS, TRUE, iPart)
			RETURN TRUE
		ENDIF

	ENDFOR
	
	RETURN FALSE

ENDFUNC

FUNC BOOL HAS_PLAYER_DEATH_CAUSED_FAIL(INT iTeam, BOOL bActiveCheck = TRUE, BOOL bIncludeSpectator = FALSE)
	
	INT iTeamlives = GET_FMMC_MISSION_LIVES_FOR_TEAM(iTeam)
	IF iTeamlives = ciFMMC_UNLIMITED_LIVES
		RETURN FALSE
	ENDIF
	
	INT iPart
	FOR iPart = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION() - 1
		
		PARTICIPANT_INDEX tempPart = INT_TO_PARTICIPANTINDEX(iPart)
		IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
		AND bActiveCheck
			RELOOP
		ENDIF
		
		IF MC_playerBD[iPart].iTeam != iTeam
			RELOOP
		ENDIF
		
		IF IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_START_SPECTATOR)
		AND !bIncludeSpectator
			RELOOP
		ENDIF
		
		IF IS_BIT_SET(MC_playerBD[iPart].iClientBitSet2, PBBOOL2_PLAYER_DEATH_CAUSED_FAIL)
			PRINTLN("[END_CONDITIONS] HAS_PLAYER_DEATH_CAUSED_FAIL - Participant: ", iPart, " has caused a fail due to death.")
			RETURN TRUE
		ENDIF

	ENDFOR
	
	RETURN FALSE	
ENDFUNC

FUNC BOOL DOES_ANY_PLAYER_ON_TEAM_HAVE_LIVES_REMAINING(INT iTeam)
	
	#IF IS_DEBUG_BUILD
	IF IS_BIT_SET(MC_serverBD.iDebugBitset, SBDEBUG_DONTFAILWHENOUTOFLIVES)
		RETURN TRUE
	ENDIF
	#ENDIF
	
	INT iTeamlives = GET_FMMC_MISSION_LIVES_FOR_TEAM(iTeam)
	IF iTeamlives = ciFMMC_UNLIMITED_LIVES
		RETURN TRUE
	ENDIF
	
	INT iNumPlayers = MC_serverBD.iNumberOfPlayingPlayers[iTeam]
	INT iNumPlayersChecked = 0
	
	INT iPart
	FOR iPart = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION() - 1
		
		PARTICIPANT_INDEX tempPart = INT_TO_PARTICIPANTINDEX(iPart)
		IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
			RELOOP
		ENDIF
		
		IF MC_playerBD[iPart].iTeam != iTeam
			RELOOP
		ENDIF
		
		IF IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_START_SPECTATOR)
			RELOOP
		ENDIF
		
		IF MC_playerBD[iPart].iNumPlayerDeaths - MC_playerBD[iPart].iAdditionalPlayerLives < iTeamlives
			RETURN TRUE
		ENDIF
	
		iNumPlayersChecked++
		IF iNumPlayersChecked >= iNumPlayers
			BREAKLOOP
		ENDIF
		
	ENDFOR
	
	RETURN FALSE
								
ENDFUNC

FUNC BOOL HAVE_GANG_BOSS_END_CONDITIONS_BEEN_MET()
	
	#IF IS_DEBUG_BUILD
		IF bAllowGangOpsWithoutBoss
			RETURN FALSE
		ENDIF
		
		IF IS_FAKE_MULTIPLAYER_MODE_SET()
			RETURN FALSE
		ENDIF
	#ENDIF
	
	IF NOT IS_THIS_A_GANG_MISSION()
		RETURN FALSE
	ENDIF
	
	IF MC_ServerBD_4.piGangBossID != INVALID_PLAYER_INDEX()
	AND NETWORK_IS_PLAYER_ACTIVE(MC_ServerBD_4.piGangBossID)
		RETURN FALSE
	ENDIF
	
	PRINTLN("[END_CONDITIONS] HAVE_GANG_BOSS_END_CONDITIONS_BEEN_MET - TRUE")
	
	INT iTeam = 0
	FOR iTeam = 0 TO FMMC_MAX_TEAMS - 1
		IF iBossType = ciBOSSTYPE_MC_PRESIDENT
			REQUEST_SET_TEAM_FAILED(iTeam, mFail_MC_PRESIDENT_GONE, FALSE)
		ELSE
			REQUEST_SET_TEAM_FAILED(iTeam, mFail_CEO_GONE, FALSE)
		ENDIF
	ENDFOR
	
	//Block Quick Restarts
	SET_BIT(MC_serverBD.iServerBitSet2, SBBOOL2_MISSION_ENDING_CRITICAL_PLAYER_LEFT)
	
	RETURN TRUE
			
ENDFUNC

FUNC BOOL HAVE_LOBBY_HOST_END_CONDITIONS_BEEN_MET()
	
	IF IS_FAKE_MULTIPLAYER_MODE_SET()
		RETURN FALSE
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySeven, ciOptionsBS27_CriticalLobbyHost)
		RETURN FALSE
	ENDIF
	
	PLAYER_INDEX piLobbyLeader = GET_FMMC_LOBBY_LEADER()
	IF piLobbyLeader != INVALID_PLAYER_INDEX()
	AND NETWORK_IS_PLAYER_ACTIVE(piLobbyLeader)
		RETURN FALSE
	ENDIF
	
	PRINTLN("[END_CONDITIONS] HAVE_LOBBY_HOST_END_CONDITIONS_BEEN_MET - TRUE")
	
	INT iTeam = 0
	FOR iTeam = 0 TO FMMC_MAX_TEAMS - 1
		REQUEST_SET_TEAM_FAILED(iTeam, mFail_LOBBY_LEADER_GONE, FALSE)
	ENDFOR
	
	//Block Quick Restarts
	SET_BIT(MC_serverBD.iServerBitSet2, SBBOOL2_MISSION_ENDING_CRITICAL_PLAYER_LEFT)
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL HAVE_SPECIFIC_PLAYER_GONE_END_CONDITIONS_BEEN_MET()
	
	IF HAVE_GANG_BOSS_END_CONDITIONS_BEEN_MET()
		RETURN TRUE
	ENDIF
	
	IF HAVE_LOBBY_HOST_END_CONDITIONS_BEEN_MET()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC PROCESS_TEAM_LIVES_FAILURE_CHECKS(INT iTeam)

	INT iTeamLives = GET_FMMC_MISSION_LIVES_FOR_TEAM(iTeam)
	IF iTeamLives = ciFMMC_UNLIMITED_LIVES
		EXIT
	ENDIF
	
	IF MC_serverBD.iNumberOfPlayingPlayers[iTeam] != 0
		EXIT
	ENDIF
	
	IF (IS_THIS_A_HEIST_MISSION() OR IS_THIS_A_GANG_MISSION())
	AND HAS_PLAYER_BOUNDS_CAUSED_FAIL(iTeam)
		PRINTLN("[END_CONDITIONS] PROCESS_TEAM_LIVES_FAILURE_CHECKS - Bounds Caused Fail")
	ELIF IS_THIS_A_HEIST_MISSION()
		IF GET_TEAM_DEATHS(iTeam) > iTeamLives
			PRINTLN("[END_CONDITIONS] PROCESS_TEAM_LIVES_FAILURE_CHECKS - Out of Lives (Heist)")
			MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_OUT_OF_LIVES
			REQUEST_SET_TEAM_FAILED(iTeam, INT_TO_ENUM(eFailMissionEnum, ENUM_TO_INT(mFail_T0MEMBER_DIED) + iTeam), TRUE)
		ELSE
			PRINTLN("[END_CONDITIONS] PROCESS_TEAM_LIVES_FAILURE_CHECKS - Team Left (Heist)")
			MC_serverBD.iReasonForObjEnd[iTeam] = (OBJ_END_REASON_HEIST_TEAM_GONE_T0 + iTeam)
			REQUEST_SET_TEAM_FAILED(iTeam, INT_TO_ENUM(eFailMissionEnum, ENUM_TO_INT(mFail_HEIST_TEAM_GONE_T0) + iTeam), FALSE)							
		ENDIF
	ELIF GET_TEAM_DEATHS(iTeam) > iTeamLives
		PRINTLN("[END_CONDITIONS] PROCESS_TEAM_LIVES_FAILURE_CHECKS - Out of Lives")
		MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_OUT_OF_LIVES
		REQUEST_SET_TEAM_FAILED(iTeam, INT_TO_ENUM(eFailMissionEnum, ENUM_TO_INT(mFail_T0MEMBER_DIED) + iTeam), TRUE)
	ELSE
		PRINTLN("[END_CONDITIONS] PROCESS_TEAM_LIVES_FAILURE_CHECKS - Team Left")
		MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_TEAM_GONE
		REQUEST_SET_TEAM_FAILED(iTeam, mFail_TEAM_GONE)
	ENDIF

	SET_LOCAL_OBJECTIVE_END_MESSAGE(iTeam, MC_serverBD_4.iCurrentHighestPriority[iTeam], MC_serverBD.iReasonForObjEnd[iTeam], TRUE)
	BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[iTeam], iTeam, MC_serverBD_4.iCurrentHighestPriority[iTeam], FALSE, TRUE)
	
ENDPROC

PROC PROCESS_COOP_LIVES_FAILURE_CHECKS(INT iTeam)
	
	INT iTeamLives = GET_FMMC_MISSION_LIVES_FOR_COOP_TEAMS()
	IF iTeamLives = ciFMMC_UNLIMITED_LIVES
		EXIT
	ENDIF
	
	IF MC_serverBD.iNumberOfPlayingPlayers[iTeam] != 0
		EXIT
	ENDIF
	
	IF (IS_THIS_A_HEIST_MISSION() OR IS_THIS_A_GANG_MISSION())
	AND HAS_PLAYER_BOUNDS_CAUSED_FAIL(iTeam)
		PRINTLN("[END_CONDITIONS] PROCESS_COOP_LIVES_FAILURE_CHECKS - Bounds Caused Fail")
	ELIF IS_THIS_A_HEIST_MISSION()
	
		IF GET_COOP_TEAMS_TOTAL_DEATHS(iTeam) > iTeamLives	
			INT iBlameTeam = -1
			IF MC_serverBD.iPartCausingFail[iTeam] != -1
				iBlameTeam = MC_playerBD[MC_serverBD.iPartCausingFail[iTeam]].iTeam
			ENDIF
			
			PRINTLN("[END_CONDITIONS] PROCESS_PLAYER_LIVES_FAILURE_CHECKS - Out of Lives (Heist) - Blame Team: ", iBlameTeam)
			
			IF iBlameTeam != -1
				MC_serverBD.iReasonForObjEnd[iTeam] = (OBJ_END_REASON_HEIST_TEAM_GONE_T0 + iBlameTeam)
				REQUEST_SET_TEAM_FAILED(iTeam, INT_TO_ENUM(eFailMissionEnum, ENUM_TO_INT(mFail_T0MEMBER_DIED) + iBlameTeam), TRUE)
			ELSE
				MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_OUT_OF_LIVES
				REQUEST_SET_TEAM_FAILED(iTeam,mFail_OUT_OF_LIVES, TRUE)
			ENDIF
		ELSE
			PRINTLN("[END_CONDITIONS] PROCESS_PLAYER_LIVES_FAILURE_CHECKS - Team left (Heist)")
			MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_HEIST_TEAM_GONE_T0 + iTeam
			REQUEST_SET_TEAM_FAILED(iTeam, INT_TO_ENUM(eFailMissionEnum, ENUM_TO_INT(mFail_HEIST_TEAM_GONE_T0) + iTeam), FALSE)							
		ENDIF
		
	ELIF GET_COOP_TEAMS_TOTAL_DEATHS(iTeam) > iTeamLives 
		
		INT iBlameTeam = -1
		IF MC_serverBD.iPartCausingFail[iTeam] != -1
			iBlameTeam = MC_playerBD[MC_serverBD.iPartCausingFail[iTeam]].iTeam
		ENDIF
		
		PRINTLN("[END_CONDITIONS] PROCESS_PLAYER_LIVES_FAILURE_CHECKS - Out of Lives - Blame Team: ", iBlameTeam)
		
		MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_OUT_OF_LIVES
		IF iBlameTeam != -1
			REQUEST_SET_TEAM_FAILED(iTeam, INT_TO_ENUM(eFailMissionEnum, ENUM_TO_INT(mFail_T0MEMBER_DIED) + iBlameTeam), TRUE)
		ELSE
			REQUEST_SET_TEAM_FAILED(iTeam, mFail_OUT_OF_LIVES, TRUE)
		ENDIF
	ELSE 
		PRINTLN("[END_CONDITIONS] PROCESS_PLAYER_LIVES_FAILURE_CHECKS - Team Left")
		MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_TEAM_GONE
		IF NOT HAVE_SPECIFIC_PLAYER_GONE_END_CONDITIONS_BEEN_MET()
			REQUEST_SET_TEAM_FAILED(iTeam, mFail_TEAM_GONE)
		ENDIF
	ENDIF
	
	SET_LOCAL_OBJECTIVE_END_MESSAGE(iTeam, MC_serverBD_4.iCurrentHighestPriority[iTeam], MC_serverBD.iReasonForObjEnd[iTeam], TRUE)
	BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[iTeam], iTeam, MC_serverBD_4.iCurrentHighestPriority[iTeam], FALSE, TRUE)

ENDPROC

PROC PROCESS_PLAYER_LIVES_FAILURE_CHECKS(INT iTeam)

	IF MC_serverBD.iNumberOfPlayingPlayers[iTeam] != 0
		EXIT
	ENDIF
	
	IF (IS_THIS_A_HEIST_MISSION() OR IS_THIS_A_GANG_MISSION())
	AND HAS_PLAYER_BOUNDS_CAUSED_FAIL(iTeam)
		PRINTLN("[END_CONDITIONS] PROCESS_PLAYER_LIVES_FAILURE_CHECKS - Bounds Caused Fail")
	ELIF IS_THIS_A_HEIST_MISSION()
		IF HAS_PLAYER_DEATH_CAUSED_FAIL(iTeam, FALSE)	
			PRINTLN("[END_CONDITIONS] PROCESS_PLAYER_LIVES_FAILURE_CHECKS - Death Caused Fail (Heist)")
			MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_OUT_OF_LIVES
			REQUEST_SET_TEAM_FAILED(iTeam, mFail_OUT_OF_LIVES, TRUE)
		ELSE
			PRINTLN("[END_CONDITIONS] PROCESS_PLAYER_LIVES_FAILURE_CHECKS - Team Left (Heist)")
			MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_TEAM_GONE
			REQUEST_SET_TEAM_FAILED(iTeam, INT_TO_ENUM(eFailMissionEnum, ENUM_TO_INT(mFail_HEIST_TEAM_GONE_T0) + iTeam), FALSE)
		ENDIF
	ELIF DOES_ANY_PLAYER_ON_TEAM_HAVE_LIVES_REMAINING(iTeam)	
		PRINTLN("[END_CONDITIONS] PROCESS_PLAYER_LIVES_FAILURE_CHECKS - Team Left")
		MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_TEAM_GONE
		REQUEST_SET_TEAM_FAILED(iTeam, mFail_TEAM_GONE)
	ELSE
		PRINTLN("[END_CONDITIONS] PROCESS_PLAYER_LIVES_FAILURE_CHECKS - Out Of Lives")
		MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_OUT_OF_LIVES
		REQUEST_SET_TEAM_FAILED(iTeam, mFail_OUT_OF_LIVES)
	ENDIF
		
	SET_LOCAL_OBJECTIVE_END_MESSAGE(iTeam, MC_serverBD_4.iCurrentHighestPriority[iTeam], MC_serverBD.iReasonForObjEnd[iTeam], TRUE)
	BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[iTeam], iTeam, MC_serverBD_4.iCurrentHighestPriority[iTeam], FALSE, TRUE)
	
ENDPROC

PROC PROCESS_MISSION_LIVES_FAILURE_CHECKS(INT iTeam)
	
	IF HAS_TEAM_FINISHED(iTeam)
		EXIT
	ENDIF

	IF NOT WAS_TEAM_EVER_ACTIVE(iTeam)
		EXIT
	ENDIF
	
	IF IS_MISSION_TEAM_LIVES()
		PROCESS_TEAM_LIVES_FAILURE_CHECKS(iTeam)
	ELIF IS_MISSION_COOP_TEAM_LIVES()
		PROCESS_COOP_LIVES_FAILURE_CHECKS(iTeam)
	ELSE
		PROCESS_PLAYER_LIVES_FAILURE_CHECKS(iTeam)
	ENDIF
	
ENDPROC

FUNC BOOL SHOULD_PROCESS_FIRST_FINISH_AND_HEIST_END_CONDITIONS()

	IF IS_THIS_A_HEIST_MISSION()
		RETURN TRUE
	ENDIF
	
	IF IS_THIS_A_GANG_MISSION()
		RETURN TRUE
	ENDIF
	
	IF g_FMMC_STRUCT.iMissionEndType = ciMISSION_SCORING_TYPE_FIRST_FINISH
		RETURN TRUE
	ENDIF
		
	RETURN FALSE
	
ENDFUNC

PROC TRIGGER_TEAM_CRITICAL_NUMBERS_FAIL(INT iTeam)
	
	PRINTLN("[END_CONDITIONS] TRIGGER_TEAM_CRITICAL_NUMBERS_FAIL - Team: ", iTeam)
	
	IF HAS_PLAYER_BOUNDS_CAUSED_FAIL(iTeam)
		PRINTLN("[END_CONDITIONS] TRIGGER_TEAM_CRITICAL_NUMBERS_FAIL - Bounds Caused Fail")
		EXIT
	ENDIF
	
	INT iTeamLoop
	IF HAS_PLAYER_DEATH_CAUSED_FAIL(iTeam,false)
		
		PRINTLN("[END_CONDITIONS] TRIGGER_TEAM_CRITICAL_NUMBERS_FAIL - Player Death Caused Fail")
		
		MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_OUT_OF_LIVES
		FOR iTeamLoop = 0 TO FMMC_MAX_TEAMS - 1
			REQUEST_SET_TEAM_FAILED(iTeamLoop, INT_TO_ENUM(eFailMissionEnum, ENUM_TO_INT(mFail_T0MEMBER_DIED) + iTeam))
		ENDFOR
		
		EXIT
		
	ENDIF	
	
	PRINTLN("[END_CONDITIONS] TRIGGER_TEAM_CRITICAL_NUMBERS_FAIL - Player Left")
		
	MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_TEAM_GONE
	IF IS_THIS_A_HEIST_MISSION()
		FOR iTeamLoop = 0 TO FMMC_MAX_TEAMS - 1
			REQUEST_SET_TEAM_FAILED(iTeamLoop, mFail_HEIST_TEAM_GONE, FALSE)
		ENDFOR
	ELIF NOT HAVE_SPECIFIC_PLAYER_GONE_END_CONDITIONS_BEEN_MET()
		FOR iTeamLoop = 0 TO FMMC_MAX_TEAMS - 1
			REQUEST_SET_TEAM_FAILED(iTeamLoop, mFail_TEAM_GONE, FALSE)
		ENDFOR
	ENDIF

ENDPROC

FUNC BOOL IS_PARTICIPANT_VALID_FOR_FINAL_ACTIVE_PLAYER_COUNT(INT iParticipant)
	
	IF IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet, PBBOOL_ANY_SPECTATOR)
	AND NOT IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet, PBBOOL_HEIST_SPECTATOR)
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet, PBBOOL_PLAYER_FAIL)
		RETURN FALSE
	ENDIF
	
	IF MC_playerBD[iParticipant].iTeam < 0
	OR MC_playerBD[iParticipant].iTeam >= FMMC_MAX_TEAMS
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL IS_PARTICIPANT_VALID_FOR_FINAL_TOTAL_PLAYER_COUNT(INT iParticipant, PLAYER_INDEX playerIndex)
	
	IF MC_playerBD[iParticipant].iTeam < 0
	OR MC_playerBD[iParticipant].iTeam >= FMMC_MAX_TEAMS
		RETURN FALSE
	ENDIF
	
	IF NOT IS_NET_PLAYER_OK(playerIndex, FALSE)
		RETURN FALSE
	ENDIF
		
	IF IS_PLAYER_SPECTATOR_ONLY(playerIndex)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL SHOULD_PROCESS_FINAL_CRITICAL_FAIL_CHECKS_FOR_TEAM(INT iTeam)
		
	IF g_FMMC_STRUCT.iMissionEndType != ciMISSION_SCORING_TYPE_LAST_ALIVE
		RETURN TRUE
	ENDIF
	
	IF iTeam != MC_serverBD.iLastTeamAlive
		RETURN TRUE
	ENDIF
		
	IF IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH)
		IF IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_SUDDEN_DEATH_KILLALLENEMIES)
		OR IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_BE_MY_VALENTINES)
		OR IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_KILLALLENEMIES_PENNED_IN)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    Called on the frame HAVE_MC_END_CONDITIONS_BEEN_MET returns true
///    to decide if the mission should fail due some player count condition/state
PROC PROCESS_FINAL_PLAYER_COUNT_FAILURE_CHECKS()
	
	IF HAVE_LOBBY_HOST_END_CONDITIONS_BEEN_MET()
		EXIT
	ENDIF
	
	INT iTempNumberOfPlayers[FMMC_MAX_TEAMS] //Does not include failed players
	INT iPlayersLeftInMission[FMMC_MAX_TEAMS] //Includes failed players in spectator.
	INT iTeamActiveBitset
	
	INT iParticipant
	FOR iParticipant = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION() - 1
		
		PARTICIPANT_INDEX tempPart = INT_TO_PARTICIPANTINDEX(iParticipant)
		
		IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
			RELOOP
		ENDIF
		
		IF IS_PARTICIPANT_VALID_FOR_FINAL_ACTIVE_PLAYER_COUNT(iParticipant)
			
			iTempNumberOfPlayers[MC_playerBD[iParticipant].iTeam]++
			PRINTLN("[END_CONDITIONS] PROCESS_FINAL_PLAYER_COUNT_FAILURE_CHECKS - Participant ", iParticipant, " is active. Number of Active Players on Team ", MC_playerBD[iParticipant].iTeam, ": ", iTempNumberOfPlayers[MC_playerBD[iParticipant].iTeam])
			
			IF NOT IS_BIT_SET(iTeamActiveBitset, MC_playerBD[iParticipant].iTeam)
				SET_BIT(iTeamActiveBitset, MC_playerBD[iParticipant].iTeam)
				PRINTLN("[END_CONDITIONS] PROCESS_FINAL_PLAYER_COUNT_FAILURE_CHECKS - Setting team ", MC_playerBD[iParticipant].iTeam, " as active")
			ENDIF
			
		ENDIF
		
		PLAYER_INDEX tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
			
		IF NOT IS_PARTICIPANT_VALID_FOR_FINAL_TOTAL_PLAYER_COUNT(iParticipant, tempPlayer)
			RELOOP
		ENDIF
		
		iPlayersLeftInMission[MC_playerBD[iParticipant].iTeam]++
		PRINTLN("[END_CONDITIONS] PROCESS_FINAL_PLAYER_COUNT_FAILURE_CHECKS - Participant ", iParticipant, " was active. Total Number of Players on Team ", MC_playerBD[iParticipant].iTeam, ": ", iPlayersLeftInMission[MC_playerBD[iParticipant].iTeam])
		
	ENDFOR
	
	INT iTeam
	FOR iTeam = 0 TO MC_serverBD.iNumberOfTeams - 1
		
		IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_MISSION_ENDING_BECAUSE_PLAYERS_LEFT)
			IF iPlayersLeftInMission[iTeam] < GET_CRITICAL_MINIMUM_FOR_TEAM(iTeam)
			OR iPlayersLeftInMission[iTeam] = 0 AND IS_TEAM_SET_AS_CRITICAL(iTeam)
				PRINTLN("[END_CONDITIONS] PROCESS_FINAL_PLAYER_COUNT_FAILURE_CHECKS - Failing for critical minimum. Team: ", iTeam)
				SET_BIT(MC_serverBD.iServerBitSet2, SBBOOL2_MISSION_ENDING_BECAUSE_PLAYERS_LEFT)
			ENDIF
		ENDIF
		
		IF NOT SHOULD_PROCESS_FINAL_CRITICAL_FAIL_CHECKS_FOR_TEAM(iTeam)
			RELOOP
		ENDIF
		
		IF IS_BIT_SET(iTeamActiveBitset, iTeam)
			
			IF GET_CRITICAL_MINIMUM_FOR_TEAM(iTeam) = 0
				RELOOP
			ENDIF
					
			IF iTempNumberOfPlayers[iTeam] >= GET_CRITICAL_MINIMUM_FOR_TEAM(iTeam)
				RELOOP
			ENDIF
			
			#IF IS_DEBUG_BUILD
				IF SHOULD_SKIP_CRITICAL_NUMBER_END_CONDITIONS()
					RELOOP
				ENDIF
			#ENDIF
							
			PRINTLN("[END_CONDITIONS] PROCESS_FINAL_PLAYER_COUNT_FAILURE_CHECKS - Team ", iTeam, " has less players than the critical minimum (", GET_CRITICAL_MINIMUM_FOR_TEAM(iTeam), ") - failing.")
			
			IF SHOULD_PROCESS_FIRST_FINISH_AND_HEIST_END_CONDITIONS()
				TRIGGER_TEAM_CRITICAL_NUMBERS_FAIL(iTeam)
			ELSE
				IF HAS_TEAM_FAILED(iTeam, TRUE)
					RELOOP
				ENDIF
				
				IF HAS_PLAYER_DEATH_CAUSED_FAIL(iTeam, FALSE)
					MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_OUT_OF_LIVES
					REQUEST_SET_TEAM_FAILED(iTeam, INT_TO_ENUM(eFailMissionEnum, ENUM_TO_INT(mFail_T0MEMBER_DIED) + iTeam))
				ELSE
					MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_TEAM_GONE
					REQUEST_SET_TEAM_FAILED(iTeam, mFail_TEAM_GONE, FALSE)
				ENDIF
			ENDIF
			
		ELSE
		
			IF NOT WAS_TEAM_EVER_ACTIVE(iTeam)
			AND NOT IS_TEAM_SET_AS_CRITICAL(iTeam)
				RELOOP
			ENDIF
		
			IF IS_THIS_A_HEIST_MISSION()
						
				#IF IS_DEBUG_BUILD
					IF SHOULD_SKIP_CRITICAL_NUMBER_END_CONDITIONS()
						RELOOP
					ENDIF
				#ENDIF
				
				PRINTLN("[END_CONDITIONS] PROCESS_FINAL_PLAYER_COUNT_FAILURE_CHECKS - Team ", iTeam, " is not active (Heist Type Mission) - Failing all other teams.")
				
				INT iSecondaryTeam = 0
				FOR iSecondaryTeam = 0 TO FMMC_MAX_TEAMS - 1
					MC_serverBD.iReasonForObjEnd[iSecondaryTeam] = OBJ_END_REASON_HEIST_TEAM_GONE_T0 + iTeam								
					REQUEST_SET_TEAM_FAILED(iSecondaryTeam, mFail_HEIST_TEAM_GONE, FALSE)
					SET_LOCAL_OBJECTIVE_END_MESSAGE(iSecondaryTeam, MC_serverBD_4.iCurrentHighestPriority[iSecondaryTeam], MC_serverBD.iReasonForObjEnd[iSecondaryTeam], TRUE)
					BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[iSecondaryTeam], iSecondaryTeam, MC_serverBD_4.iCurrentHighestPriority[iSecondaryTeam], FALSE, TRUE)
				ENDFOR	
				
			ELSE
			
				PRINTLN("[END_CONDITIONS] PROCESS_FINAL_PLAYER_COUNT_FAILURE_CHECKS - Team ", iTeam, " is not active - Failing Team.")
				
				IF NOT HAVE_SPECIFIC_PLAYER_GONE_END_CONDITIONS_BEEN_MET()
					IF HAS_PLAYER_DEATH_CAUSED_FAIL(iTeam, FALSE)
						MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_OUT_OF_LIVES
						REQUEST_SET_TEAM_FAILED(iTeam, INT_TO_ENUM(eFailMissionEnum, ENUM_TO_INT(mFail_T0MEMBER_DIED) + iTeam))
					ELSE
						MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_TEAM_GONE
						REQUEST_SET_TEAM_FAILED(iTeam, mFail_TEAM_GONE, FALSE)
					ENDIF
				ENDIF
				
				SET_LOCAL_OBJECTIVE_END_MESSAGE(iTeam, MC_serverBD_4.iCurrentHighestPriority[iTeam], MC_serverBD.iReasonForObjEnd[iTeam], TRUE)
				BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[iTeam], iTeam, MC_serverBD_4.iCurrentHighestPriority[iTeam], FALSE, TRUE)
			
			ENDIF
			
		ENDIF
	ENDFOR							

ENDPROC

PROC SET_ALL_TEAMS_END_CONDITIONS_MET(INT &iEndConditionsMetBitset)
	iEndConditionsMetBitset = ciALL_TEAMS_BITSET_VALUE
ENDPROC

FUNC BOOL PROCESS_INACTIVE_TEAM_END_CONDITIONS(INT iTeam, INT &iEndConditionsMetBitset)

	IF iTeam = MC_serverBD.iLastTeamAlive
	AND g_FMMC_STRUCT.iMissionEndType = ciMISSION_SCORING_TYPE_LAST_ALIVE
		SET_ALL_TEAMS_END_CONDITIONS_MET(iEndConditionsMetBitset)
		PRINTLN("[END_CONDITIONS] PROCESS_INACTIVE_TEAM_END_CONDITIONS - Team: ", iTeam, ". End Type is Last Alive and the last team has died.")
		RETURN TRUE
	ENDIF
	
	PROCESS_MISSION_LIVES_FAILURE_CHECKS(iTeam)
	
	PRINTLN("[END_CONDITIONS] PROCESS_INACTIVE_TEAM_END_CONDITIONS - Team: ", iTeam, " is not active - setting end conditions met.")
	SET_BIT(iEndConditionsMetBitset, iTeam)
	
	IF SHOULD_PROCESS_FIRST_FINISH_AND_HEIST_END_CONDITIONS()
	AND (NOT IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH)
	OR IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_SUDDEN_DEATH_KILLALLENEMIES))
	
		IF NOT WAS_TEAM_EVER_ACTIVE(iTeam)
		AND NOT IS_TEAM_SET_AS_CRITICAL(iteam)
			RETURN FALSE
		ENDIF

		IF IS_THIS_A_HEIST_MISSION()
		OR IS_THIS_A_GANG_MISSION()
			#IF IS_DEBUG_BUILD
			IF NOT SHOULD_SKIP_CRITICAL_NUMBER_END_CONDITIONS()
			#ENDIF
			
				PRINTLN("[END_CONDITIONS] PROCESS_INACTIVE_TEAM_END_CONDITIONS - Heist type mission - Failing all teams")
			
				INT iTeamLoop
				FOR iTeamLoop = 0 TO FMMC_MAX_TEAMS - 1
					IF HAS_TEAM_FAILED(iTeamLoop, TRUE)
						//Don't change the team who failed's fail reason
						RELOOP
					ENDIF
					
					MC_serverBD.iReasonForObjEnd[iTeamLoop] = OBJ_END_REASON_HEIST_TEAM_GONE_T0 + iTeam
					REQUEST_SET_TEAM_FAILED(iTeamLoop, mFail_HEIST_TEAM_GONE, FALSE)
					SET_LOCAL_OBJECTIVE_END_MESSAGE(iTeamLoop, MC_serverBD_4.iCurrentHighestPriority[iTeamLoop], MC_serverBD.iReasonForObjEnd[iTeamLoop], TRUE)
					BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[iTeamLoop], iTeamLoop, MC_serverBD_4.iCurrentHighestPriority[iTeamLoop], FALSE, TRUE)
				ENDFOR
				
			#IF IS_DEBUG_BUILD
			ENDIF
			#ENDIF
		ELSE
			IF NOT HAS_TEAM_FAILED(iTeam, TRUE)
				PRINTLN("[END_CONDITIONS] PROCESS_INACTIVE_TEAM_END_CONDITIONS - First Finish - Failing this team (", iTeam, ") only")
				REQUEST_SET_TEAM_FAILED(iTeam, mFail_TEAM_GONE, FALSE)
				SET_LOCAL_OBJECTIVE_END_MESSAGE(iTeam, MC_serverBD_4.iCurrentHighestPriority[iTeam], MC_serverBD.iReasonForObjEnd[iTeam], TRUE)
				BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[iTeam], iTeam, MC_serverBD_4.iCurrentHighestPriority[iTeam], FALSE, TRUE)
			ENDIF
		ENDIF
			
		IF g_FMMC_STRUCT.iMinNumberOfTeams = 1
		OR NOT (GET_NUM_ACTIVE_TEAMS() >= g_FMMC_STRUCT.iMinNumberOfTeams OR IS_TEAM_SET_AS_CRITICAL(iTeam))
			#IF IS_DEBUG_BUILD
			IF SHOULD_SKIP_CRITICAL_NUMBER_END_CONDITIONS()
				RETURN FALSE
			ENDIF
			#ENDIF
			PRINTLN("[END_CONDITIONS] PROCESS_INACTIVE_TEAM_END_CONDITIONS - Critical team not active - Setting all teams' end conditions met.")
			
			SET_ALL_TEAMS_END_CONDITIONS_MET(iEndConditionsMetBitset)
			RETURN TRUE
		ENDIF
		
	ELIF g_FMMC_STRUCT.iMissionEndType = ciMISSION_SCORING_TYPE_ALL_VS_1
	AND NOT IS_FAKE_MULTIPLAYER_MODE_SET()
	
		IF iTeam = 0
		OR IS_TEAM_SET_AS_CRITICAL(iTeam)
			PRINTLN("[END_CONDITIONS] PROCESS_INACTIVE_TEAM_END_CONDITIONS - Scoring Type All Vs 1 - Setting all teams' end conditions met.")
			SET_ALL_TEAMS_END_CONDITIONS_MET(iEndConditionsMetBitset)
			RETURN TRUE
		ENDIF
		
	ELIF IS_TEAM_SET_AS_CRITICAL(iteam)
	AND NOT IS_FAKE_MULTIPLAYER_MODE_SET()
	
		PRINTLN("[END_CONDITIONS] PROCESS_INACTIVE_TEAM_END_CONDITIONS - Critical team not active (other mission type) - Setting all teams' end conditions met.")
		SET_ALL_TEAMS_END_CONDITIONS_MET(iEndConditionsMetBitset)
		RETURN TRUE
		
	ENDIF	
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL PROCESS_ON_VALID_RULE_END_CONDITIONS_FOR_TEAM(INT iTeam, INT &iEndConditionsMetBitset)
	
	IF MC_serverBD.iNumberOfPlayingTeams = 1
	AND MC_serverBD.iLastTeamAlive = -1
	AND g_FMMC_STRUCT.iMissionEndType = ciMISSION_SCORING_TYPE_LAST_ALIVE
		MC_serverBD.iLastTeamAlive = iTeam
		PRINTLN("[END_CONDITIONS] PROCESS_ON_VALID_RULE_END_CONDITIONS_FOR_TEAM - Team: ", iTeam, " is the last alive!")
	ENDIF
	
	IF GET_CRITICAL_MINIMUM_FOR_TEAM(iTeam) = 0
	OR IS_FAKE_MULTIPLAYER_MODE_SET()
	OR IS_BIT_SET( iLocalBoolCheck11, LBOOL11_STOP_MISSION_FAILING_DUE_TO_EARLY_CELEBRATION)
		RETURN FALSE
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF SHOULD_SKIP_CRITICAL_NUMBER_END_CONDITIONS()
		RETURN FALSE
	ENDIF
	#ENDIF
	
	IF GET_PLAYER_COUNT_FOR_CRITICAL_CHECK(iTeam) >= GET_CRITICAL_MINIMUM_FOR_TEAM(iTeam)
		RETURN FALSE
	ENDIF
	
	IF SHOULD_PROCESS_FIRST_FINISH_AND_HEIST_END_CONDITIONS()
		PRINTLN("[END_CONDITIONS] PROCESS_ON_VALID_RULE_END_CONDITIONS_FOR_TEAM - First Finish or Heist - Critical Minimum Player Fail")
		TRIGGER_TEAM_CRITICAL_NUMBERS_FAIL(iteam)
		SET_ALL_TEAMS_END_CONDITIONS_MET(iEndConditionsMetBitset)
		RETURN TRUE
	ENDIF	
	
	IF g_FMMC_STRUCT.iMissionEndType = ciMISSION_SCORING_TYPE_ALL_VS_1 
	AND iTeam = 0
		PRINTLN("[END_CONDITIONS] PROCESS_ON_VALID_RULE_END_CONDITIONS_FOR_TEAM - All Vs 1 - Critical Minimum Player Fail")
		IF HAS_PLAYER_DEATH_CAUSED_FAIL(iTeam, FALSE)
			MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_OUT_OF_LIVES
			REQUEST_SET_TEAM_FAILED(iTeam, mFail_T0MEMBER_DIED)
		ELSE
			MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_TEAM_GONE
			REQUEST_SET_TEAM_FAILED(iTeam, mFail_TEAM_GONE, FALSE)
		ENDIF
		SET_ALL_TEAMS_END_CONDITIONS_MET(iEndConditionsMetBitset)
		RETURN TRUE
	ENDIF
		
	PRINTLN("[END_CONDITIONS] PROCESS_ON_VALID_RULE_END_CONDITIONS_FOR_TEAM - Other Mission Type - Critical Minimum Player Fail")
		
	IF HAS_PLAYER_DEATH_CAUSED_FAIL(iTeam, FALSE)
		MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_OUT_OF_LIVES
		REQUEST_SET_TEAM_FAILED(iTeam, INT_TO_ENUM(eFailMissionEnum, (ENUM_TO_INT(mFail_T0MEMBER_DIED) + iTeam)))
	ELSE
		MC_serverBD.iReasonForObjEnd[iteam] = OBJ_END_REASON_TEAM_GONE
		REQUEST_SET_TEAM_FAILED(iTeam, mFail_TEAM_GONE, FALSE)
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL PROCESS_ACTIVE_TEAM_SCORE_LIMIT_END_CONDITION(INT iTeam, INT &iEndConditionsMetBitset)
	
	IF g_FMMC_STRUCT.sFMMCEndConditions[0].iMissionScoreLimit = ciFMMC_MISSION_END_POINTS_IGNORE //Intentionally Checking Team 0
		RETURN FALSE
	ENDIF
	
	IF MC_serverBD.iTeamScore[iTeam] < g_FMMC_STRUCT.sFMMCEndConditions[0].iMissionScoreLimit
		RETURN FALSE
	ENDIF
	
	PRINTLN("[END_CONDITIONS] PROCESS_ACTIVE_TEAM_SCORE_LIMIT_END_CONDITION - Team: ", iTeam, " has reached the score limit: ", g_FMMC_STRUCT.sFMMCEndConditions[0].iMissionScoreLimit)
	SET_ALL_TEAMS_END_CONDITIONS_MET(iEndConditionsMetBitset)
	RETURN TRUE
	
ENDFUNC

PROC PROCESS_ACTIVE_TEAM_RULE_ATTEMPTS_END_CONDITION(INT iTeam, INT &iEndConditionsMetBitset)

	IF MC_serverBD.iNumTeamRuleAttempts[iTeam] <= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamRuleAttempts
		EXIT
	ENDIF
	
	ASSERTLN("[FMMC2020] - PROCESS_ACTIVE_TEAM_RULE_ATTEMPTS_END_CONDITION - This functionality requires further work before it can be used.")

	PRINTLN("[END_CONDITIONS] PROCESS_ACTIVE_TEAM_RULE_ATTEMPTS_END_CONDITION - Team: ", iTeam, " has exceeded the max rule attempts: ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamRuleAttempts)
	
	SET_BIT(iEndConditionsMetBitset, iTeam)
	MC_serverBD.iReasonForObjEnd[iTeam] = (OBJ_END_REASON_OUT_OF_LIVES) //[FMMC2020] - Add a new end reason
	REQUEST_SET_TEAM_FAILED(iTeam, mFail_OUT_OF_LIVES, FALSE) //[FMMC2020] - Add a new fail reason
	SET_LOCAL_OBJECTIVE_END_MESSAGE(iTeam, MC_serverBD_4.iCurrentHighestPriority[iTeam], MC_serverBD.iReasonForObjEnd[iTeam], TRUE)
	BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[iTeam], iTeam, MC_serverBD_4.iCurrentHighestPriority[iTeam], FALSE, TRUE)

ENDPROC

FUNC BOOL PROCESS_ACTIVE_TEAM_OBJECT_OWNERSHIP_END_CONDITION(INT iTeam, INT &iEndConditionsMetBitset)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitsetFifteen, ciENABLE_PASS_MISSION_WHEN_TEAM_OWNS_ALL_COLLECT_OBJ)
		RETURN FALSE
	ENDIF
	
	IF MC_ServerBD.iNumObjHighestPriority[iTeam] <= 0
		RETURN FALSE
	ENDIF
	
	INT iAmount = MC_serverBD.iScoreOnThisRule[iTeam]
	INT iAmountMax = MC_ServerBD.iNumObjHighestPriority[iTeam]
		
	IF iAmount < iAmountMax
		RETURN FALSE
	ENDIF
								
	PRINTLN("[END_CONDITIONS] PROCESS_ACTIVE_TEAM_OBJECT_OWNERSHIP_END_CONDITION - Team: ", iTeam, " has ownership of all the objects.")
	
	SET_ALL_TEAMS_END_CONDITIONS_MET(iEndConditionsMetBitset)		
		
	INT iTeamToFail
	FOR iTeamToFail = 0 TO FMMC_MAX_TEAMS - 1	
		IF iTeamToFail = iTeam
			RELOOP
		ENDIF
		
		IF NOT IS_TEAM_ACTIVE(iTeamToFail)
			RELOOP
		ENDIF
		
		PRINTLN("[END_CONDITIONS] PROCESS_ACTIVE_TEAM_OBJECT_OWNERSHIP_END_CONDITION - Failing team ", iTeamToFail)
		REQUEST_SET_TEAM_FAILED(iTeamToFail, mFail_OTHER_TEAM_HAS_ALL_OBJECTS)
	ENDFOR
				
	RETURN TRUE
	
ENDFUNC

PROC PROCESS_ACTIVE_TEAM_MISSION_TIME_END_CONDITION(INT iTeam, INT &iEndConditionsMetBitset)

	IF NOT HAS_NET_TIMER_STARTED(MC_serverBD_3.tdLimitTimer)
		EXIT
	ENDIF
	
	//Intentionally only checking team 0's time limit
	IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdLimitTimer) <= GET_MISSION_END_TIME_FROM_MENU_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[0].iMissionTimeLimit)
		EXIT
	ENDIF	
	
	PRINTLN("[END_CONDITIONS] PROCESS_ACTIVE_TEAM_MISSION_TIME_END_CONDITION - Mission Time Limit expired for Team ", iTeam)
	SET_BIT(iEndConditionsMetBitset, iTeam)

ENDPROC

FUNC INT GET_TEAM_OBJECTIVE_END_REASON_OUT_OF_LIVES(INT iTeam)
	
	IF IS_THIS_A_HEIST_MISSION()
		RETURN OBJ_END_REASON_HEIST_TEAM_GONE_T0 + iTeam
	ENDIF
	
	RETURN OBJ_END_REASON_OUT_OF_LIVES
	
ENDFUNC

PROC PROCESS_ACTIVE_TEAM_MISSION_TEAM_LIVES_END_CONDITIONS(INT iTeam, INT &iEndConditionsMetBitset)
	
	INT iTeamlives = GET_FMMC_MISSION_LIVES_FOR_TEAM(iTeam)
	
	IF iTeamlives = ciFMMC_UNLIMITED_LIVES
		EXIT
	ENDIF

	IF GET_TEAM_DEATHS(iTeam) <= iTeamlives 
		EXIT
	ENDIF
	
	IF MC_serverBD.iNumberOfPlayingPlayers[iTeam] = 0
	OR (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciEND_ROUND_ON_ZERO_LIVES)
	AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH))
	
		PRINTLN("[END_CONDITIONS] PROCESS_ACTIVE_TEAM_MISSION_TEAM_LIVES_END_CONDITIONS - Out of Lives. Team ", iTeam)

		MC_serverBD.iReasonForObjEnd[iTeam] = GET_TEAM_OBJECTIVE_END_REASON_OUT_OF_LIVES(iTeam)
		REQUEST_SET_TEAM_FAILED(iTeam, INT_TO_ENUM(eFailMissionEnum, ENUM_TO_INT(mFail_T0MEMBER_DIED) + iTeam))
		SET_LOCAL_OBJECTIVE_END_MESSAGE(iTeam, MC_serverBD_4.iCurrentHighestPriority[iTeam], MC_serverBD.iReasonForObjEnd[iTeam], TRUE)
		BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[iTeam], iTeam, MC_serverBD_4.iCurrentHighestPriority[iTeam], FALSE, TRUE)
		
		SET_BIT(iEndConditionsMetBitset, iTeam)
		
	ENDIF
	
ENDPROC

PROC PROCESS_ACTIVE_TEAM_MISSION_COOP_LIVES_END_CONDITIONS(INT iTeam, INT &iEndConditionsMetBitset)

	INT iTeamlives = GET_FMMC_MISSION_LIVES_FOR_COOP_TEAMS()
	
	IF iTeamlives = ciFMMC_UNLIMITED_LIVES
		EXIT
	ENDIF
	
	IF GET_COOP_TEAMS_TOTAL_DEATHS(iTeam) <= iTeamlives 
		EXIT
	ENDIF
	
	IF MC_serverBD.iTotalPlayingCoopPlayers[iTeam] != 0
		EXIT
	ENDIF
	
	INT iBlameTeam = -1
	IF MC_serverBD.iPartCausingFail[iTeam] != -1
		iBlameTeam = MC_playerBD[MC_serverBD.iPartCausingFail[iTeam]].iTeam
	ENDIF
	
	PRINTLN("[END_CONDITIONS] PROCESS_ACTIVE_TEAM_MISSION_COOP_LIVES_END_CONDITIONS - Out of Lives. Team ", iTeam, " Blame Team: ", iBlameTeam)
					
	IF iBlameTeam != -1
		MC_serverBD.iReasonForObjEnd[iTeam] = GET_TEAM_OBJECTIVE_END_REASON_OUT_OF_LIVES(iBlameTeam)
		REQUEST_SET_TEAM_FAILED(iTeam, INT_TO_ENUM(eFailMissionEnum, ENUM_TO_INT(mFail_T0MEMBER_DIED) + iBlameTeam))
	ELSE
		MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_OUT_OF_LIVES
		REQUEST_SET_TEAM_FAILED(iTeam, mFail_OUT_OF_LIVES)
	ENDIF
					
	SET_LOCAL_OBJECTIVE_END_MESSAGE(iTeam, MC_serverBD_4.iCurrentHighestPriority[iTeam], MC_serverBD.iReasonForObjEnd[iTeam], TRUE)
	BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[iTeam], iTeam, MC_serverBD_4.iCurrentHighestPriority[iTeam], FALSE, TRUE)
	SET_BIT(iEndConditionsMetBitset, iTeam)

ENDPROC

PROC PROCESS_ACTIVE_TEAM_MISSION_PLAYER_LIVES_END_CONDITIONS(INT iTeam, INT &iEndConditionsMetBitset)

	INT iTeamlives = GET_FMMC_MISSION_LIVES_FOR_TEAM(iTeam)
	
	IF iTeamlives = ciFMMC_UNLIMITED_LIVES
		EXIT
	ENDIF
	
	IF MC_serverBD.iNumberOfPlayingPlayers[iTeam] != 0
		EXIT
	ENDIF
	
	IF DOES_ANY_PLAYER_ON_TEAM_HAVE_LIVES_REMAINING(iTeam)
		EXIT
	ENDIF
	
	PRINTLN("[END_CONDITIONS] PROCESS_ACTIVE_TEAM_MISSION_PLAYER_LIVES_END_CONDITIONS - Out of Lives. Team ", iTeam)
	
	MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_OUT_OF_LIVES
	REQUEST_SET_TEAM_FAILED(iTeam, mFail_OUT_OF_LIVES)
	SET_LOCAL_OBJECTIVE_END_MESSAGE(iTeam, MC_serverBD_4.iCurrentHighestPriority[iTeam], MC_serverBD.iReasonForObjEnd[iTeam], TRUE)
	BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[iTeam], iTeam, MC_serverBD_4.iCurrentHighestPriority[iTeam], FALSE, TRUE)

	SET_BIT(iEndConditionsMetBitset, iTeam)

ENDPROC

PROC PROCESS_ACTIVE_TEAM_MISSION_LIVES_END_CONDITIONS(INT iTeam, INT &iEndConditionsMetBitset)

	#IF IS_DEBUG_BUILD
		IF NOT IS_BIT_SET(MC_serverBD.iDebugBitset, SBDEBUG_DONTFAILWHENOUTOFLIVES)
			EXIT
		ENDIF
	#ENDIF

	IF IS_MISSION_TEAM_LIVES()
		PROCESS_ACTIVE_TEAM_MISSION_TEAM_LIVES_END_CONDITIONS(iTeam, iEndConditionsMetBitset)
	ELIF IS_MISSION_COOP_TEAM_LIVES()
		PROCESS_ACTIVE_TEAM_MISSION_COOP_LIVES_END_CONDITIONS(iTeam, iEndConditionsMetBitset)
	ELIF IS_MISSION_PLAYER_LIVES()
		PROCESS_ACTIVE_TEAM_MISSION_PLAYER_LIVES_END_CONDITIONS(iTeam, iEndConditionsMetBitset)
	ENDIF

ENDPROC

PROC PROCESS_ACTIVE_TEAM_PED_AGGRO_END_CONDITION(INT iTeam, INT &iEndConditionsMetBitset)
	
	IF NOT HAS_TEAM_TRIGGERED_AGGRO(iTeam, ciMISSION_AGGRO_INDEX_ALL) 
	OR NOT IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_AGGRO_WILL_FAIL_FOR_TEAM_0 + iTeam)
		EXIT
	ENDIF
	
	IF NOT HAS_AGGRO_INDEX_TIMER_STARTED_FROM_BITSET(iTeam, ciMISSION_AGGRO_INDEX_DEFAULT)
	OR NOT HAS_AGGRO_INDEX_TIMER_EXPIRED_FROM_BITSET(iTeam, ciMISSION_AGGRO_INDEX_DEFAULT, ciPED_AGGRO_FAIL_DELAY)
		EXIT
	ENDIF
				
	BOOL bPedsLeft = FALSE
	
	INT iPedLoop
	FOR iPedLoop = 0 TO (MC_serverBD.iNumPedCreated - 1)
		
		IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedLoop].iPedBitsetThree, ciPED_BSThree_AggroFailHasDelay)
			RELOOP
		ENDIF
				
		IF NOT SHOULD_AGRO_FAIL_FOR_TEAM(iPedLoop, iTeam)
			RELOOP
		ENDIF
		
		IF NOT IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[iPedLoop])
			bPedsLeft = TRUE
			BREAKLOOP
		ENDIF
		
	ENDFOR
	
	IF !bPedsLeft
	
		PRINTLN("[END_CONDITIONS] PROCESS_ACTIVE_TEAM_PED_AGGRO_END_CONDITION - Team ", iTeam, "'s aggro timer expired the aggrod ped's no longer exist. Resetting timer!")
		
		MC_serverBD.iPartCausingFail[iTeam] = -1
		MC_serverBD.iEntityCausingFail[iteam] = -1
		MC_serverBD.iAggroCountdownTimerSlack[iTeam] = 0		
		STOP_AGGRO_INDEX_TIMER_FROM_BITSET(iTeam, ciMISSION_AGGRO_INDEX_MAX_BITSET)
		CLEAR_BIT(MC_serverBD.iServerBitSet1, SBBOOL1_AGGRO_WILL_FAIL_FOR_TEAM_0 + iTeam)
		
		EXIT
		
	ENDIF

	IF MC_serverBD.iNumTeamRuleAttempts[iTeam] < g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamRuleAttempts
	OR g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamRuleAttempts != 1	
		
		ASSERTLN("[FMMC2020] - PROCESS_ACTIVE_TEAM_PED_AGGRO_END_CONDITION - This functionality requires further work before it can be used.")
		
		IF IS_BIT_SET(MC_serverBD.iServerBitSet5, SBBOOL5_RETRY_CURRENT_RULE_TEAM_0 + iTeam)
			EXIT
		ENDIF
		
		MC_serverBD.iNumTeamRuleAttempts[iTeam]++
		
		IF MC_serverBD.iNumTeamRuleAttempts[iTeam] < g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamRuleAttempts
			CLEAR_TEAM_AS_TRIGGERED_AGGRO_FROM_BITSET(iTeam, ciMISSION_AGGRO_INDEX_ALL)
			CLEAR_BIT(MC_serverBD.iServerBitSet1,SBBOOL1_AGGRO_WILL_FAIL_FOR_TEAM_0 + iTeam)
			STOP_AGGRO_INDEX_TIMER_FROM_BITSET(iTeam, ciMISSION_AGGRO_INDEX_MAX_BITSET)
			SET_BIT(MC_serverBD.iServerBitSet5, SBBOOL5_RETRY_CURRENT_RULE_TEAM_0 + iTeam)
			MC_serverBD.iAggroCountdownTimerSlack[iTeam] = 0
			MC_serverBD.iPartCausingFail[iTeam] = -1
			MC_serverBD.iEntityCausingFail[iteam] = -1
			PRINTLN("[END_CONDITIONS] PROCESS_ACTIVE_TEAM_PED_AGGRO_END_CONDITION - Team ", iTeam, " set to restart rule. Current attempts: ", MC_serverBD.iNumTeamRuleAttempts[iTeam])
		ENDIF
		
		EXIT
		
	ENDIF
	
	PRINTLN("[END_CONDITIONS] PROCESS_ACTIVE_TEAM_PED_AGGRO_END_CONDITION - Team ", iTeam, "'s aggro timer expired - setting as failed.")
	
	IF IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_AGGRO_FAIL_DUE_TO_BODY_FOUND_TEAM_0 + iTeam)
		MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_PED_FOUND_BODY
		REQUEST_SET_TEAM_FAILED(iTeam, mFail_PED_FOUND_BODY)
	ELSE
		MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_PED_AGRO
		REQUEST_SET_TEAM_FAILED(iTeam, INT_TO_ENUM(eFailMissionEnum, ENUM_TO_INT(mFail_PED_AGRO_T0) + iTeam), FALSE, MC_serverBD.iPartCausingFail[iTeam])
	ENDIF
		
	SET_TEAM_OBJECTIVE_OUTCOME(iTeam, MC_serverBD_4.iCurrentHighestPriority[iTeam], FALSE)
	SET_LOCAL_OBJECTIVE_END_MESSAGE(iTeam, MC_serverBD_4.iCurrentHighestPriority[iTeam], MC_serverBD.iReasonForObjEnd[iTeam], TRUE)
	BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[iTeam], iTeam, MC_serverBD_4.iCurrentHighestPriority[iTeam], DID_TEAM_PASS_OBJECTIVE(iTeam, MC_serverBD_4.iCurrentHighestPriority[iTeam]), TRUE)
		
	SET_BIT(iEndConditionsMetBitset, iTeam)
		
ENDPROC

PROC PROCESS_ACTIVE_TEAM_COP_SPOTTED_END_CONDITION(INT iTeam, INT &iEndConditionsMetBitset)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFour[MC_serverBD_4.iCurrentHighestPriority[iTeam]], ciBS_RULE4_FAIL_IF_DETECTED_BY_POLICE)
		EXIT
	ENDIF
	
	IF NOT HAS_NET_TIMER_STARTED(MC_serverBD.tdCopSpottedFailTimer)
	OR NOT HAS_NET_TIMER_EXPIRED(MC_serverBD.tdCopSpottedFailTimer, ciCOP_SPOTTED_FAIL_DELAY)
		EXIT
	ENDIF
	
	PRINTLN("[END_CONDITIONS] PROCESS_ACTIVE_TEAM_COP_SPOTTED_END_CONDITION - Team ", iTeam, "'s cop spotted timer expired - setting as failed.")
			
	MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_GAINED_WANTED
	SET_TEAM_OBJECTIVE_OUTCOME(iTeam, MC_serverBD_4.iCurrentHighestPriority[iTeam], FALSE)
	SET_LOCAL_OBJECTIVE_END_MESSAGE(iTeam, MC_serverBD_4.iCurrentHighestPriority[iTeam], OBJ_END_REASON_GAINED_WANTED, TRUE)
	BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(OBJ_END_REASON_GAINED_WANTED, iTeam, MC_serverBD_4.iCurrentHighestPriority[iTeam], FALSE, TRUE)
	REQUEST_SET_TEAM_FAILED(iTeam, mFail_GAINED_WANTED)
	
	SET_BIT(iEndConditionsMetBitset, iTeam)
	
ENDPROC

FUNC BOOL PROCESS_ACTIVE_TEAM_END_CONDITIONS(INT iTeam, INT &iEndConditionsMetBitset)

	IF MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
	AND NOT HAS_TEAM_FAILED(iTeam, TRUE)
		IF PROCESS_ON_VALID_RULE_END_CONDITIONS_FOR_TEAM(iTeam, iEndConditionsMetBitset)
			RETURN TRUE
		ENDIF
	ELSE
		IF SHOULD_PROCESS_FIRST_FINISH_AND_HEIST_END_CONDITIONS()
			PRINTLN("[END_CONDITIONS] PROCESS_ACTIVE_TEAM_END_CONDITIONS - First Finish or Heist - Out of rules or team (", iTeam, ") has failed")
			
			SET_ALL_TEAMS_END_CONDITIONS_MET(iEndConditionsMetBitset)
			RETURN TRUE
		ENDIF
		
		IF g_FMMC_STRUCT.iMissionEndType = ciMISSION_SCORING_TYPE_ALL_VS_1 
		AND iTeam = 0
			PRINTLN("[END_CONDITIONS] PROCESS_ACTIVE_TEAM_END_CONDITIONS - All Vs 1 - Out of rules or team (", iTeam, ") has failed")
			
			SET_ALL_TEAMS_END_CONDITIONS_MET(iEndConditionsMetBitset)
			RETURN TRUE
		ENDIF
		
		PRINTLN("[END_CONDITIONS] PROCESS_ACTIVE_TEAM_END_CONDITIONS - Other Mission Type - Out of rules or team (", iTeam, ") has failed")
		
		SET_BIT(iEndConditionsMetBitset, iTeam)
	ENDIF
	
	IF PROCESS_ACTIVE_TEAM_SCORE_LIMIT_END_CONDITION(iTeam, iEndConditionsMetBitset)
		RETURN TRUE
	ENDIF
	
	PROCESS_ACTIVE_TEAM_RULE_ATTEMPTS_END_CONDITION(iTeam, iEndConditionsMetBitset)
		
	IF PROCESS_ACTIVE_TEAM_OBJECT_OWNERSHIP_END_CONDITION(iTeam, iEndConditionsMetBitset)	
		RETURN TRUE
	ENDIF
	
	IF MC_serverBD_4.iCurrentHighestPriority[iTeam] >= FMMC_MAX_RULES
		RETURN FALSE
	ENDIF
	
	PROCESS_ACTIVE_TEAM_MISSION_TIME_END_CONDITION(iTeam, iEndConditionsMetBitset)
	
	IF IS_BIT_SET(MC_ServerBD.iServerbitSet7, SBBOOL7_MULTI_RULE_TIMER_ROUND_RESTART_ENDED)
		PRINTLN("[END_CONDITIONS] PROCESS_ACTIVE_TEAM_END_CONDITIONS - Multirule Timer Round Restart Ended is set - RETURNING TRUE")
		SET_ALL_TEAMS_END_CONDITIONS_MET(iEndConditionsMetBitset)
		RETURN TRUE
	ENDIF
	
	PROCESS_ACTIVE_TEAM_MISSION_LIVES_END_CONDITIONS(iTeam, iEndConditionsMetBitset)
	
	PROCESS_ACTIVE_TEAM_PED_AGGRO_END_CONDITION(iTeam, iEndConditionsMetBitset)
	
	PROCESS_ACTIVE_TEAM_COP_SPOTTED_END_CONDITION(iTeam, iEndConditionsMetBitset)
	
	RETURN FALSE
	
ENDFUNC

PROC PROCESS_CRITICAL_LIVES_END_CONDITIONS_FOR_TEAM(INT iTeam, INT &iEndConditionsMetBitset)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitset4, ciBS4_TEAM_CRITICAL_LIVES_WILL_FAIL_TEAM)
		EXIT
	ENDIF
	
	IF NOT HAS_PLAYER_DEATH_CAUSED_FAIL(iTeam, TRUE, TRUE)
		EXIT
	ENDIF
	
	PRINTLN("[END_CONDITIONS] PROCESS_CRITICAL_LIVES_END_CONDITIONS_FOR_TEAM - Team: ", iTeam, " ran out of team lives.")
	
	MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_OUT_OF_LIVES
	REQUEST_SET_TEAM_FAILED(iTeam, mFail_OUT_OF_LIVES)
	SET_LOCAL_OBJECTIVE_END_MESSAGE(iTeam, MC_serverBD_4.iCurrentHighestPriority[iTeam], MC_serverBD.iReasonForObjEnd[iTeam], TRUE)
	BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[iTeam], iTeam, MC_serverBD_4.iCurrentHighestPriority[iTeam], FALSE, TRUE)			
	SET_BIT(iEndConditionsMetBitset, iTeam)
	
ENDPROC

FUNC BOOL PROCESS_END_CONDITIONS_FOR_TEAM(INT iTeam, INT &iEndConditionsMetBitset)
			
	IF IS_TEAM_ACTIVE(iTeam)
		IF PROCESS_ACTIVE_TEAM_END_CONDITIONS(iTeam, iEndConditionsMetBitset)
			RETURN TRUE
		ENDIF
	ELSE
		IF PROCESS_INACTIVE_TEAM_END_CONDITIONS(iTeam, iEndConditionsMetBitset)
			RETURN TRUE
		ENDIF
	ENDIF
				
	PROCESS_CRITICAL_LIVES_END_CONDITIONS_FOR_TEAM(iTeam, iEndConditionsMetBitset)
	
	RETURN FALSE
				
ENDFUNC

FUNC BOOL SHOULD_MISSION_END_DUE_TO_CRITICAL_TEAM_NUMBER()
	
	IF g_FMMC_STRUCT.iCriticalTeamNumber = 0
		RETURN FALSE
	ENDIF
	
	IF MC_serverBD.iNumberOfPlayingTeams > g_FMMC_STRUCT.iCriticalTeamNumber
		RETURN FALSE
	ENDIF
	
	PRINTLN("[END_CONDITIONS] SHOULD_MISSION_END_DUE_TO_CRITICAL_TEAM_NUMBER - Number of playing teams ", MC_serverBD.iNumberOfPlayingTeams , " is not greater than critical min: ", g_FMMC_STRUCT.iCriticalTeamNumber, " - RETURNING TRUE")
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL SHOULD_MISSION_END_DUE_TO_TOO_FEW_TEAMS()
	
	IF SHOULD_MISSION_END_DUE_TO_CRITICAL_TEAM_NUMBER()
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(MC_serverBD.iServerBitset2, SBBOOL2_SERVER_INITIALISATION_TIMED_OUT)
		PRINTLN("[END_CONDITIONS] SHOULD_MISSION_END_DUE_TO_TOO_FEW_TEAMS - Server Initialisation Timed Out - RETURNING TRUE")
		RETURN TRUE
	ENDIF
	
	IF NOT IS_THIS_A_VERSUS_MISSION()
		RETURN FALSE
	ENDIF
	
	IF g_FMMC_STRUCT.iMaxNumberOfTeams = 1
		RETURN TRUE
	ENDIF
	
	IF MC_serverBD.iNumActiveTeams = 1 
	AND NOT IS_FAKE_MULTIPLAYER_MODE_SET() 
	 	#IF IS_DEBUG_BUILD
	  		IF NOT IS_TRANSITION_DEBUG_LAUNCH_VAR_SET()
				RETURN TRUE
			ENDIF
	  	#ENDIF
		
		#IF NOT IS_DEBUG_BUILD
			RETURN TRUE
		#ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC PROCESS_TEAM_COUNT_END_CONDITIONS(INT &iEndConditionsMetBitset)
	
	#IF IS_DEBUG_BUILD
		IF SHOULD_SKIP_CRITICAL_NUMBER_END_CONDITIONS()
			EXIT
		ENDIF
	#ENDIF
	
	IF NOT SHOULD_MISSION_END_DUE_TO_TOO_FEW_TEAMS()
		EXIT
	ENDIF
	
	INT iTeam
	FOR iTeam = 0 TO FMMC_MAX_TEAMS - 1
	
		MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_TEAM_GONE
		
		SET_BIT(iEndConditionsMetBitset, iTeam)
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ci_PASS_CRITICAL_TEAM_SIZE)
			RELOOP
		ENDIF
		
		REQUEST_SET_TEAM_FAILED(iTeam, mFail_TEAM_GONE)
		SET_LOCAL_OBJECTIVE_END_MESSAGE(iTeam, MC_serverBD_4.iCurrentHighestPriority[iTeam], MC_serverBD.iReasonForObjEnd[iTeam], TRUE)
		BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[iTeam], iTeam, MC_serverBD_4.iCurrentHighestPriority[iTeam], FALSE, TRUE)
		
	ENDFOR

ENDPROC

PROC PROCESS_NON_EXISTENT_TEAMS_END_CONDITIONS(INT &iEndConditionsMetBitset)
	
	//If a team can never exist set it's end conditions as met
	
	IF MC_serverbd.iNumberOfTeams < 4
		SET_BIT(iEndConditionsMetBitset, 3)
	ENDIF
	
	IF MC_serverbd.iNumberOfTeams < 3
		SET_BIT(iEndConditionsMetBitset, 2)
	ENDIF
	
	IF MC_serverbd.iNumberOfTeams < 2
		SET_BIT(iEndConditionsMetBitset, 1)
	ENDIF
	
ENDPROC

FUNC BOOL HAVE_MC_END_CONDITIONS_BEEN_MET()

	#IF IS_DEBUG_BUILD
		IF HAS_QUICK_RESTART_DEBUG_BEEN_ACTIVATED()
			PRINTLN("[END_CONDITIONS] HAVE_MC_END_CONDITIONS_BEEN_MET - QUICK RESTART DEBUG ACTIVE - RETURNING TRUE")
			RETURN TRUE
		ENDIF
	#ENDIF
	
	IF IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_MULTIRULE_TIMER_ENDED_GAME)
		PRINTLN("[END_CONDITIONS] HAVE_MC_END_CONDITIONS_BEEN_MET - Multirule Timer Ended Game - RETURNING TRUE")
		RETURN TRUE
	ENDIF

	INT iEndConditionsMetBitset = 0
	INT iTeam

	IF IS_BIT_SET(MC_serverbd.iServerBitSet, SBBOOL_FIRST_UPDATE_DONE)
		
		IF HAVE_SPECIFIC_PLAYER_GONE_END_CONDITIONS_BEEN_MET()
			RETURN TRUE
		ENDIF
		
		FOR iTeam = 0 TO MC_serverBD.iNumberOfTeams - 1
			IF PROCESS_END_CONDITIONS_FOR_TEAM(iTeam, iEndConditionsMetBitset)
				BREAKLOOP
			ENDIF
		ENDFOR
		
		PROCESS_TEAM_COUNT_END_CONDITIONS(iEndConditionsMetBitset)
		
	ENDIF
	
	PROCESS_NON_EXISTENT_TEAMS_END_CONDITIONS(iEndConditionsMetBitset)
	
	BOOL bAllFinished = TRUE
	
	FOR iTeam = 0 TO FMMC_MAX_TEAMS - 1
		
		IF NOT IS_BIT_SET(iEndConditionsMetBitset, iTeam)
			bAllFinished = FALSE
			RELOOP
		ENDIF
		
		IF IS_BIT_SET(MC_serverBD.iServerBitSet, SSBOOL_TEAM0_FINISHED + iTeam)
			RELOOP
		ENDIF
		
		PRINTLN("[END_CONDITIONS] HAVE_MC_END_CONDITIONS_BEEN_MET - Team: ", iTeam, " is finished.")
		SET_BIT(MC_serverBD.iServerBitSet, SSBOOL_TEAM0_FINISHED + iTeam)
		END_TEAM_MISSION_TIMER(iTeam)
		
	ENDFOR
	
	IF bAllFinished
		PRINTLN("[END_CONDITIONS] HAVE_MC_END_CONDITIONS_BEEN_MET - All teams have finished - RETURNING TRUE")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC SET_MISSION_OVER()
	
	PRINTLN("[END_CONDITIONS] SET_MISSION_OVER - Mission is ending!")
	
	INT iTeam
	FOR iTeam = 0 TO MC_serverBD.iNumberOfTeams - 1
		PROCESS_FAIL_MISSION_REQUEST_FOR_TEAM(iTeam)
	ENDFOR
	
	PROCESS_FINAL_VEHICLE_FAILURE_CHECKS()
					
	PROCESS_FINAL_PED_FAILURE_CHECKS()
	
	PROCESS_FINAL_PLAYER_COUNT_FAILURE_CHECKS()
		
	END_MISSION_TIMERS()
	
	PROCESS_SERVER_END_OF_MISSION_CONTINUITY()
	
	SET_BIT(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)

ENDPROC

PROC PROCESS_SERVER_END_CONDITIONS()

	IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_ALL_JOINED_START)
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_EVERYONE_RUNNING)
		EXIT
	ENDIF
	
	IF IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)				
		EXIT
	ENDIF
			
	IF HAVE_MC_END_CONDITIONS_BEEN_MET()
		SET_MISSION_OVER()							
	ENDIF
			
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Client Request Handlers
// ##### Description: Functions that handle running logic to action requests from other areas of the controller.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_CLIENT_SCORE_REQUESTS()
	
	INT i = 0
	FOR i = 0 TO ciMAX_INCREMENT_SCORE_REQUESTS - 1
		IF IS_INCREMENT_SCORE_DATA_ARRAY_FREE(i)
			RELOOP
		ENDIF
		
		IF IS_BIT_SET(sIncrementScoreData[i].iBitset, INCREMENT_SCORE_BITSET_TYPE_CURRENT_OBJECTIVE)
			INCREMENT_LOCAL_PLAYER_SCORE_FOR_CURRENT_OBJECTIVE(sIncrementScoreData[i].iScore, 
						IS_BIT_SET(sIncrementScoreData[i].iBitset, INCREMENT_SCORE_BITSET_GENERAL_KILL_POINTS),
						sIncrementScoreData[i].iVictimID,
						sIncrementScoreData[i].iIdentifier)
		ELIF IS_BIT_SET(sIncrementScoreData[i].iBitset, INCREMENT_SCORE_BITSET_TYPE_FORCED_AMOUNT)
			INCREMENT_LOCAL_PLAYER_SCORE_BY(sIncrementScoreData[i].iScore,
						IS_BIT_SET(sIncrementScoreData[i].iBitset, INCREMENT_SCORE_BITSET_GENERAL_KILL_POINTS),
						sIncrementScoreData[i].iIdentifier)
		#IF IS_DEBUG_BUILD
		ELSE
			ASSERTLN("[SCORE] PROCESS_CLIENT_SCORE_REQUESTS - Request: ", i, " is invalid!")
		#ENDIF
		ENDIF

		CLEAR_INCREMENT_SCORE_DATA_INDEX(i)
	ENDFOR
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Process Rules and Objectives
// ##### Description: The main functions that call all client and server rule and objective processing.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_CLIENT_RULES_AND_OBJECTIVES()
	
	PROCESS_CLIENT_OBJECTIVE_BLOCKER()
	
	HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE() //FMMC2020 - JS TODO
	
	PROCESS_OBJECTIVE_LIMITS()
	PROCESS_CLIENT_SUDDEN_DEATH_LOGIC()
	
	PROCESS_CLIENT_WANTED_RULE_PROGRESSION()
	PROCESS_CLIENT_REPAIR_VEHICLE_RULE_PROGRESSION()
	
	PROCESS_CLIENT_SCORE_REQUESTS()
	
	PROCESS_CLIENT_MISSION_OBJECTIVE_LOGIC()
		
ENDPROC
#IF FEATURE_DLC_1_2022
FUNC INT GET_SCALING_BAND_FROM_NUMBER_OF_PLAYERS(INT iPlayersToCheckType, INT iTeam = -1, INT iBandSize = ciFMMC_ON_RULE_PLAYER_SCALING_BAND_SIZE_DEFAULT)
	
	INT iNumberOfPlayers
	
	PRINTLN("GET_SCALING_BAND_FROM_NUMBER_OF_PLAYERS - iPlayersToCheckType: ", iPlayersToCheckType)
	
	SWITCH iPlayersToCheckType
		CASE ciFMMC_ON_RULE_PLAYER_SCALE_TYPE__TEAM
			IF iTeam != -1
				iNumberOfPlayers = MC_serverBD.iNumberOfPlayersAndSpectators[iTeam]
			ENDIF
		BREAK
		CASE ciFMMC_ON_RULE_PLAYER_SCALE_TYPE__GAME
			iNumberOfPlayers = GET_NUMBER_OF_PLAYERS_AND_SPECTATORS()
		BREAK
	ENDSWITCH
	
	IF iNumberOfPlayers <= 0
		iNumberOfPlayers = 1
		PRINTLN("GET_SCALING_BAND_FROM_NUMBER_OF_PLAYERS - There were <0 players counted in server data, changing number of players to 1")
		ASSERTLN("GET_SCALING_BAND_FROM_NUMBER_OF_PLAYERS - There were <0 players counted in server data, changing number of players to 1")
		   
		#IF IS_DEBUG_BUILD
		ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_INVALID_PLAYER_COUNT_USED, ENTITY_RUNTIME_ERROR_TYPE_CRITICAL, "Invalid player count used for getting scaling band, forcing value to 1!!")	
		#ENDIF
		
	ENDIF
	
	PRINTLN("GET_SCALING_BAND_FROM_NUMBER_OF_PLAYERS - iNumberOfPlayers: ",iNumberOfPlayers, " iBandSize: ",iBandSize)
	PRINTLN("GET_SCALING_BAND_FROM_NUMBER_OF_PLAYERS - Band to use: ", CEIL(TO_FLOAT(iNumberOfPlayers)/iBandSize) - 1)
	
	RETURN CEIL(TO_FLOAT(iNumberOfPlayers)/iBandSize) - 1
	
ENDFUNC

FUNC INT GET_NUMBER_OF_LIVES_TO_ADD_TO_TEAM(INT iTeam, INT iRule)
		
	IF NOT IS_RULE_INDEX_VALID(iRule)
		RETURN 0
	ENDIF
	
	INT iPlayerBandToCheck = GET_SCALING_BAND_FROM_NUMBER_OF_PLAYERS(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sOnRuleStart[iRule].sLivesStruct.iLivesPlayersToCheck, iTeam)
	
	RETURN g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sOnRuleStart[iRule].sLivesStruct.iLivesIncreasePlayerBands[iPlayerBandToCheck]

ENDFUNC

PROC PROCESS_SERVER_ADDITIONAL_TEAM_LIVES_ON_RULE(INT iTeam)
	
	INT iCurrentTeamRule = GET_TEAM_CURRENT_RULE(iTeam)
	
	IF NOT IS_RULE_INDEX_VALID(iCurrentTeamRule)
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sOnRuleStart[iCurrentTeamRule].iOnRuleBitSet,  ciOnRuleStartBS_GiveLives)
		EXIT
	ENDIF
	
	INT iLivesToAdd = GET_NUMBER_OF_LIVES_TO_ADD_TO_TEAM(iTeam, iCurrentTeamRule)
	
	SWITCH g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sOnRuleStart[iCurrentTeamRule].sLivesStruct.iLivesType
		CASE ciFMMC_ON_RULE_LIVES_TYPE__UPDATE
			MC_serverBD_3.iTeamLivesOverride[iTeam] = (iLivesToAdd + GET_TEAM_DEATHS(iTeam))
			PRINTLN("PROCESS_SERVER_ADDITIONAL_TEAM_LIVES_ON_RULE - Just overrode team lives to ", iLivesToAdd, " + ",GET_TEAM_DEATHS(iTeam),"(deaths) for team ", iTeam, " team lives override = ", MC_serverBD_3.iTeamLivesOverride[iTeam])
		BREAK
		CASE ciFMMC_ON_RULE_LIVES_TYPE__ADDITIONAL
			MC_serverBD_3.iAdditionalTeamLives[iTeam] += iLivesToAdd
			PRINTLN("PROCESS_SERVER_ADDITIONAL_TEAM_LIVES_ON_RULE - Just added ", iLivesToAdd, " lives to team ", iTeam, " additional lives = ", MC_serverBD_3.iAdditionalTeamLives[iTeam])
		BREAK
	ENDSWITCH
	
ENDPROC

FUNC INT GET_AMOUNT_OF_TIME_TO_ADD_FOR_TEAM(INT iTeam, INT iRule)
		
	IF NOT IS_RULE_INDEX_VALID(iRule)
		RETURN 0
	ENDIF
	
	INT iPlayerBandToCheck = GET_SCALING_BAND_FROM_NUMBER_OF_PLAYERS(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sOnRuleStart[iRule].sTimeStruct.iTimePlayersToCheck, iTeam)
	
	RETURN GET_FMMC_OBJECTIVE_TIME_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sOnRuleStart[iRule].sTimeStruct.iTimeIncreasePlayerBands[iPlayerBandToCheck], MC_serverBD.iTotalNumStartingPlayers)

ENDFUNC

PROC PROCESS_SERVER_ADDITIONAL_TIME_ON_RULE(INT iTeam)
	
	INT iCurrentTeamRule = GET_TEAM_CURRENT_RULE(iTeam)
	
	IF NOT IS_RULE_INDEX_VALID(iCurrentTeamRule)
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sOnRuleStart[iCurrentTeamRule].iOnRuleBitSet,  ciOnRuleStartBS_GiveTime)
		EXIT
	ENDIF
	
	INT iIncrementTime = GET_AMOUNT_OF_TIME_TO_ADD_FOR_TEAM(iTeam, iCurrentTeamRule)
	
	SWITCH g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sOnRuleStart[iCurrentTeamRule].sTimeStruct.iTimeType
		CASE ciFMMC_ON_RULE_TIME_TYPE__ANY_ACTIVE
			BROADCAST_FMMC_INCREMENT_REMAINING_TIME(iTeam, iIncrementTime, TRUE, -1)
			PRINTLN("PROCESS_SERVER_ADDITIONAL_TIME_ON_RULE - Incrementing time by ", iIncrementTime, " for team ", iTeam)
		BREAK
	ENDSWITCH
	
ENDPROC

PROC PROCESS_SERVER_TEAM_NEW_RULE(INT iTeam)

	IF NOT DOES_TEAM_HAVE_NEW_PRIORITY_THIS_FRAME(iTeam)
		EXIT
	ENDIF
	
	PRINTLN("PROCESS_SERVER_TEAM_NEW_RULE - New rule for team ", iTeam)
	
	PROCESS_SERVER_ADDITIONAL_TEAM_LIVES_ON_RULE(iTeam)
	PROCESS_SERVER_ADDITIONAL_TIME_ON_RULE(iTeam)
	
ENDPROC
#ENDIF

PROC PROCESS_SERVER_RULES_AND_OBJECTIVES()

	IF !bIsLocalPlayerHost
		EXIT
	ENDIF
	
	INT iTeam
	FOR iTeam = 0 TO MC_serverBD.iNumberOfTeams - 1
		
		PROCESS_RECALCULATE_OBJECTIVE_LOGIC_REQUEST_FOR_TEAM(iTeam)
		PROCESS_FAIL_MISSION_REQUEST_FOR_TEAM(iTeam)
		#IF FEATURE_DLC_1_2022
		PROCESS_SERVER_TEAM_NEW_RULE(iTeam)
		#ENDIF
		
	ENDFOR
	
	PROCESS_SERVER_SCORE_REQUESTS()
	
	PROCESS_SERVER_MISSION_OBJECTIVE_LOGIC()
	
ENDPROC
