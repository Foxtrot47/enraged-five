// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### System: GTA V Online Mission Controller - Assets ------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Script Name: FM_Mission_Controller_2020.sc ------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Loading and unloading assets. As well as runtime checks relating to assets. 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### This script was forked from FM_Mission_Controller.sc at the beginning of 2020		 																				  
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

USING "FM_Mission_Controller_Rewards_2020.sch"

// ##### ------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Specifc Asset Type Check ---------------------------------------------------------------------------------
// ##### Description: Checks to see if an entity is of a specific type. Eg: IS_VEHICLE_A_TRAILER/IS_THIS_MODEL_A_FUSEBOX  ------------
// ##### ------------------------------------------------------------------------------------------------------------------------
// ##### ------------------------------------------------------------------------------------------------------------------------

FUNC BOOL IS_VEHICLE_A_TRAILER(VEHICLE_INDEX vehToCheck)
	IF DOES_ENTITY_EXIST(vehToCheck)
		IF GET_ENTITY_MODEL(vehToCheck) = INT_TO_ENUM(MODEL_NAMES, HASH("TRAILERLARGE"))
		OR GET_ENTITY_MODEL(vehToCheck) = INT_TO_ENUM(MODEL_NAMES, HASH("TRAILERSMALL2"))
		OR GET_ENTITY_MODEL(vehToCheck) = INT_TO_ENUM(MODEL_NAMES, HASH("TRAILERS"))
		OR GET_ENTITY_MODEL(vehToCheck) = INT_TO_ENUM(MODEL_NAMES, HASH("BOATTRAILER"))
		OR GET_ENTITY_MODEL(vehToCheck) = INT_TO_ENUM(MODEL_NAMES, HASH("TANKER"))
		OR GET_ENTITY_MODEL(vehToCheck) = INT_TO_ENUM(MODEL_NAMES, HASH("TANKER2"))
		OR GET_ENTITY_MODEL(vehToCheck) = INT_TO_ENUM(MODEL_NAMES, HASH("TRFLAT"))
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CAN_MODEL_BE_SUPPRESSED(MODEL_NAMES mn)
	INT iModel	
	FOR iModel = 0 TO g_iNumSupressed
		IF mnSuppressedVehicles[iModel] = mn
			RETURN FALSE
		ENDIF
	ENDFOR
	RETURN TRUE
ENDFUNC

PROC VALIDATE_NUMBER_MODELS_SUPRESSED(INT iAdd)
	g_iNumSupressed += iAdd	
	IF g_iNumSupressed < 0
		g_iNumSupressed = 0
	ENDIF
	#IF IS_DEBUG_BUILD
		IF g_iNumSupressed >= MAX_NUM_SUPRESSED_MODELS
			ASSERTLN(" No space left to add another model in SET_VEHICLE_MODEL_IS_SUPPRESSED CURRENT: ",g_iNumSupressed," MAX: ",MAX_NUM_SUPRESSED_MODELS)			
			
			#IF IS_DEBUG_BUILD
			ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_TOO_MANY_SUPPRESS_VEHICLE_MODELS, ENTITY_RUNTIME_ERROR_TYPE_WARNING, "Over # vehicle models has been suppressed, and cannot do any more.", MAX_NUM_SUPRESSED_MODELS)
			#ENDIF
		ENDIF
	#ENDIF
ENDPROC

FUNC BOOL IS_WEAPONTYPE_A_STUN_GUN(WEAPON_TYPE wtWeaponType)
	IF wtWeaponType = WEAPONTYPE_STUNGUN
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_OBJECT_A_CONTAINER( OBJECT_INDEX tempObj )
	BOOL bReturn = FALSE
	
	IF DOES_ENTITY_EXIST(tempObj)
		IF GET_ENTITY_MODEL(tempObj) = PROP_CONTR_03B_LD
		OR GET_ENTITY_MODEL(tempObj) = prop_container_ld_pu
			bReturn = TRUE
		ENDIF
	ENDIF
	
	RETURN bReturn
ENDFUNC

FUNC VECTOR GET_NEAREST_DOOR_POSITION(VECTOR vPos)
	
	FLOAT fDistance, fNewDistance
	VECTOR vDoorPosition
	INT iDoor	
	
	FOR iDoor = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfDoors-1
		
		fNewDistance = VDIST2(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].vPos, vPos)
		
		IF fNewDistance <= fDistance
		OR fDistance = 0.0
			fDistance = fNewDistance
			vDoorPosition = g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].vPos
			PRINTLN("[Cutscene] - Found a closer position - vPos: ", vPos, " Door Position: ", g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].vPos," fDistance: ", fDistance)
		ENDIF
		
	ENDFOR
	
	RETURN vDoorPosition
ENDFUNC

PROC STOP_FAKE_MOVING_ELEVATOR_SFX()
	IF iElevatorMovingSFX != -1
	AND NOT HAS_SOUND_FINISHED(iElevatorMovingSFX)
		STOP_SOUND(iElevatorMovingSFX)					
		RELEASE_SOUND_ID(iElevatorMovingSFX)
	ENDIF
	iElevatorMovingSFX = -1
ENDPROC

FUNC BOOL IS_MODEL_A_DOOR(MODEL_NAMES mn)
	SWITCH(mn)
		CASE PROP_FNCLINK_03GATE1  
		CASE PROP_FNCLINK_03GATE1_L1  
		CASE PROP_FNCLINK_03GATE1_L2
		CASE PROP_FNCLINK_03GATE2  
		CASE PROP_FNCLINK_03GATE2_L1
		CASE PROP_FNCLINK_03GATE2_L2 
		CASE PROP_FNCLINK_03GATE3   
		CASE PROP_FNCLINK_03GATE3_L1
		CASE PROP_FNCLINK_03GATE4    
		CASE PROP_FNCLINK_03GATE4_L1   
		CASE PROP_FNCLINK_03GATE4_L2 
		CASE PROP_FNCLINK_03GATE5   
		CASE PROP_FNCLINK_03GATE5_L1 
		CASE PROP_FNCLINK_03GATE5_L2
		CASE PROP_LRGGATE_04A
		CASE PROP_LRGGATE_04A_L1
		//Add more doors as they assert...
		//"Cannot create doors in MP, it is currently not supported."
			RETURN TRUE
	ENDSWITCH
	
	IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_casino_door_01d"))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_COMBAT_VEHICLE(VEHICLE_INDEX tempVeh)
	
	IF NOT DOES_ENTITY_EXIST(tempVeh)
		PRINTLN("IS_COMBAT_VEHICLE - This vehicle doesn't exist! Returning FALSE")
		DEBUG_PRINTCALLSTACK()
		RETURN FALSE
	ENDIF
	
	SWITCH GET_ENTITY_MODEL(tempVeh)
		
		CASE RHINO
		CASE LAZER
		CASE BUZZARD
		CASE ANNIHILATOR
		CASE SAVAGE
		CASE TAMPA3
		CASE HUNTER
		CASE VALKYRIE
		CASE THRUSTER
		CASE BUZZARD2
			RETURN TRUE
			
	ENDSWITCH
	
	RETURN FALSE

ENDFUNC

// ##### ------------------------------------------------------------------------------------------------------------------------
// ##### Large Section Name: LOADING AND UNLOADING ------------------------------------------------------------------------------------
// ##### Description: Section of functions used to load and unload assets.  -----------------------------------------------------
// ##### ------------------------------------------------------------------------------------------------------------------------
// ##### ------------------------------------------------------------------------------------------------------------------------

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Player Flare Attachment
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC VECTOR GET_FLARE_ATTACHMENT_OFFSET_FOR_PLAYER()	
	IF GET_ULP_MISSION_FROM_ROOT_CONTENT_ID_HASH(g_FMMC_STRUCT.iRootContentIDHash) = ciULP_MISSION_6_CLEANUP
		IF IS_PED_FEMALE(localPlayerPed)
			RETURN <<-0.046, -0.10, -0.165>>
		ENDIF	
	ENDIF
	
	RETURN <<-0.060, -0.120, -0.165>>
ENDFUNC
FUNC STRING GET_FLARE_ATTACHMENT_PTFX_ASSET_NAME()
	RETURN "scr_biolab_heist"
ENDFUNC

PROC DESTROY_FLARE_ATTACHMENT_FOR_PLAYER()
	PRINTLN("[AttachedFlare] - DESTROY_FLARE_ATTACHMENT_FOR_PLAYER - Destroying Flare")
	IF DOES_PARTICLE_FX_LOOPED_EXIST(players_underwater_flare)
		STOP_PARTICLE_FX_LOOPED(players_underwater_flare)
	ENDIF
	IF DOES_ENTITY_EXIST(objFlare)
		DETACH_ENTITY(objFlare)
		DELETE_OBJECT(objFlare)
		SET_OBJECT_AS_NO_LONGER_NEEDED(objFlare)
	ENDIF	
	CLEAR_BIT(iLocalBoolCheck12, LBOOL12_OBJ_RES_UNDERWATER_FLARE)
	RESET_NET_TIMER(stFlarePTFXDelayTimer)
ENDPROC

PROC UNLOAD_FLARE_ATTACHMENT_ASSETS()
	IF NOT DOES_ENTITY_EXIST(objFlare) 
		IF IS_BIT_SET(iLocalBoolCheck10, LBOOL10_HUMANE_FLARE_CREATED)
			IF HAVE_ALL_STREAMING_REQUESTS_COMPLETED(LocalPlayerPed)
			AND HAS_PED_HEAD_BLEND_FINISHED(LocalPlayerPed)			
				FINALIZE_HEAD_BLEND(LocalPlayerPed)
				SET_BIT(iLocalBoolCheck10, LBOOL10_HUMANE_FLARE_PROCESSING_DONE)
				PRINTLN("[AttachedFlare] - UNLOAD_FLARE_ATTACHMENT_ASSETS - Remove Flare (1)")
			ENDIF
		ENDIF
		
		EXIT
	ENDIF	
	
	DESTROY_FLARE_ATTACHMENT_FOR_PLAYER()
		
	REMOVE_NAMED_PTFX_ASSET(GET_FLARE_ATTACHMENT_PTFX_ASSET_NAME())	
		
	REMOVE_MP_HEIST_GEAR(LocalPlayerPed, HEIST_GEAR_REBREATHER)
	
	PRINTLN("[AttachedFlare] - UNLOAD_FLARE_ATTACHMENT_ASSETS - Remove Flare (2)")
ENDPROC

// ##### ------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Warp Portals ---------------------------------------------------------------------------------------------
// ##### Description: Assets related to Warp Portals.  --------------------------------------------------------------------------
// ##### ------------------------------------------------------------------------------------------------------------------------
// ##### ------------------------------------------------------------------------------------------------------------------------

FUNC BOOL IS_WARP_PORTAL_USING_TRANSFORM(INT iPortal)
	IF IS_MODEL_VALID(g_FMMC_STRUCT.sWarpPortals[iPortal].sVehicleSwap.mnVehicleModelSwap)
	AND g_FMMC_STRUCT.sWarpPortals[iPortal].sVehicleSwap.mnVehicleModelSwap != DUMMY_MODEL_FOR_SCRIPT
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ANY_WARP_PORTALS_USING_TRANSFORM()
	
	IF IS_BIT_SET(iLocalBoolCheck29, LBOOL29_WARP_PORTALS_ARE_USING_TRANSFORMS_CHECKED)
		RETURN IS_BIT_SET(iLocalBoolCheck29, LBOOL29_WARP_PORTALS_ARE_USING_TRANSFORMS)
	ENDIF
	
	INT iPortal
	FOR iPortal = 0 TO sWarpPortal.iMaxNumberOfPortals-1
		IF IS_MODEL_VALID(g_FMMC_STRUCT.sWarpPortals[iPortal].sVehicleSwap.mnVehicleModelSwap)
		AND g_FMMC_STRUCT.sWarpPortals[iPortal].sVehicleSwap.mnVehicleModelSwap != DUMMY_MODEL_FOR_SCRIPT
			SET_BIT(iLocalBoolCheck29, LBOOL29_WARP_PORTALS_ARE_USING_TRANSFORMS_CHECKED)
			SET_BIT(iLocalBoolCheck29, LBOOL29_WARP_PORTALS_ARE_USING_TRANSFORMS)
			RETURN TRUE
		ENDIF
	ENDFOR
	
	SET_BIT(iLocalBoolCheck29, LBOOL29_WARP_PORTALS_ARE_USING_TRANSFORMS_CHECKED)
	
	RETURN FALSE
	
ENDFUNC

// ##### ------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Effects --------------------------------------------------------------------------------------------------
// ##### Description: Loading and unloading PTFX, POSTFX, etc.  -----------------------------------------------------------------
// ##### ------------------------------------------------------------------------------------------------------------------------
// ##### ------------------------------------------------------------------------------------------------------------------------

FUNC FLOAT GET_BEAST_PARTICLE_COLOUR(INT iRGBA, BOOL bForceColour = FALSE)
	INT iR, iG, iB, iA
	HUD_COLOURS tempColour
	SWITCH(g_FMMC_STRUCT.iRATCBeastColour)
		CASE 0
			GET_RGB_FROM_INT(GET_INT_FROM_HUD_COLOUR(HUD_COLOUR_WHITE), iR, iG, iB, iA)
		BREAK
		
		CASE 1
			tempColour = GET_HUD_COLOUR_FOR_FMMC_TEAM(MC_playerBD[iLocalPart].iteam, LocalPlayer)
			GET_RGB_FROM_INT(GET_INT_FROM_HUD_COLOUR(tempColour), iR, iG, iB, iA)
		BREAK
			
		CASE 2
			GET_RGB_FROM_INT(GET_INT_FROM_HUD_COLOUR(HUD_COLOUR_BLACK), iR, iG, iB, iA)
		BREAK
		
		CASE 3
			GET_RGB_FROM_INT(GET_INT_FROM_HUD_COLOUR(HUD_COLOUR_RED), iR, iG, iB, iA)
		BREAK
	ENDSWITCH
	
	IF bForceColour
		tempColour = GET_HUD_COLOUR_FOR_FMMC_TEAM(MC_playerBD[iLocalPart].iteam, LocalPlayer)
		GET_RGB_FROM_INT(GET_INT_FROM_HUD_COLOUR(tempColour), iR, iG, iB, iA)
	ENDIF
	
	SWITCH(iRGBA)
		CASE 0
			RETURN TO_FLOAT(iR) / 255
		CASE 1
			RETURN TO_FLOAT(iG) / 255
		CASE 2
			RETURN TO_FLOAT(iB) / 255
		CASE 3
			RETURN TO_FLOAT(iA) / 255
		DEFAULT
			RETURN 0.0
	ENDSWITCH
ENDFUNC

PROC UNLOAD_MISSION_SPECIFIC_POSTFX()
	PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - UNLOAD_MISSION_SPECIFIC_POSTFX ################################")

	ANIMPOSTFX_STOP("MP_Celeb_Win")
	ANIMPOSTFX_STOP("MP_Celeb_Lose")
	
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPHotwire")
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPBeamhack")
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("helicopterhud")
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPBeamhackFG")
			
	IF ANIMPOSTFX_IS_RUNNING("CrossLine")
		PRINTLN("[MMacK][LocVFX] SCRIPT CLEAN")
		ANIMPOSTFX_STOP("CrossLine")
		ANIMPOSTFX_PLAY("CrossLineOut", 0, FALSE)
	ENDIF
ENDPROC

FUNC BOOL SHOULD_LOAD_ELECTRONIC_ASSETS()
	INT iObj
	FOR iObj = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfObjects - 1
		IF IS_THIS_MODEL_A_FUSEBOX(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].mn)
		OR IS_THIS_MODEL_A_CCTV_CAMERA(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].mn)
		OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].sObjDroneData.iBS, ciFMMC_DroneBS_Enable)
			RETURN TRUE
		ENDIF
	ENDFOR
	
	INT iDynoprop
	FOR iDynoprop = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps - 1
		IF IS_THIS_MODEL_A_FUSEBOX(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoprop].mn)
		OR IS_THIS_MODEL_A_CCTV_CAMERA(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoprop].mn)
			RETURN TRUE
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC

PROC CLEAN_UP_MANSION_SPEAKERS()
	SET_STATIC_EMITTER_ENABLED("SE_DLC_Fixer_Data_Leak_Mansion_Speaker_01", FALSE)
	SET_STATIC_EMITTER_ENABLED("SE_DLC_Fixer_Data_Leak_Mansion_Speaker_02", FALSE)
	SET_STATIC_EMITTER_ENABLED("SE_DLC_Fixer_Data_Leak_Mansion_Speaker_03", FALSE)
	SET_STATIC_EMITTER_ENABLED("SE_DLC_Fixer_Data_Leak_Mansion_Speaker_04", FALSE)
	SET_STATIC_EMITTER_ENABLED("SE_DLC_Fixer_Data_Leak_Mansion_Speaker_05", FALSE)
	SET_STATIC_EMITTER_ENABLED("SE_DLC_Fixer_Data_Leak_Mansion_Speaker_06", FALSE)
	SET_STATIC_EMITTER_ENABLED("SE_DLC_Fixer_Data_Leak_Mansion_Speaker_07", FALSE)
	SET_STATIC_EMITTER_ENABLED("SE_DLC_Fixer_Data_Leak_Mansion_Speaker_08", FALSE)
	SET_STATIC_EMITTER_ENABLED("SE_DLC_Fixer_Data_Leak_Mansion_Speaker_09", FALSE)
	SET_STATIC_EMITTER_ENABLED("SE_DLC_Fixer_Data_Leak_Mansion_Speaker_10", FALSE)
	SET_AMBIENT_ZONE_STATE("AZ_DLC_Fixer_Data_Leak_Mansion_Party_Area_01", FALSE)
ENDPROC

PROC UNDERWATER_TUNNEL_CLEAN_UP_ALL_BREAK_PTFX()
	INT iPTFX
	FOR iPTFX = 0 TO ciUnderwaterTunnelWeld_NumPTFX-1
		IF DOES_PARTICLE_FX_LOOPED_EXIST(ptUnderwaterTunnelWeldPTFX_Break[iPTFX])
			STOP_PARTICLE_FX_LOOPED(ptUnderwaterTunnelWeldPTFX_Break[iPTFX])
			PRINTLN("UNDERWATER_TUNNEL_CLEAN_UP_ALL_BREAK_PTFX - Cleaning up ptfx ", iPTFX)
		ENDIF
	ENDFOR
ENDPROC

PROC UNDERWATER_TUNNEL_WELD_CLEANUP_SOUND_AND_EFFECTS()
	IF DOES_PARTICLE_FX_LOOPED_EXIST(ptUnderwaterTunnelWeldPTFX_Singe)
		STOP_PARTICLE_FX_LOOPED(ptUnderwaterTunnelWeldPTFX_Singe)
	ENDIF
	
	IF DOES_PARTICLE_FX_LOOPED_EXIST(ptUnderwaterTunnelWeldPTFX_Weld)
		STOP_PARTICLE_FX_LOOPED(ptUnderwaterTunnelWeldPTFX_Weld)
	ENDIF
	
	IF DOES_PARTICLE_FX_LOOPED_EXIST(ptUnderwaterTunnelWeldPTFX_Bubble)
		STOP_PARTICLE_FX_LOOPED(ptUnderwaterTunnelWeldPTFX_Bubble)
	ENDIF	
	
	IF NOT HAS_SOUND_FINISHED(iUnderwaterTunnelWeldSound_BlowtorchCutLoop)
		STOP_SOUND(iUnderwaterTunnelWeldSound_BlowtorchCutLoop)
	ENDIF
	
	IF NOT HAS_SOUND_FINISHED(iUnderwaterTunnelWeldSound_BlowtorchLoop) 
		STOP_SOUND(iUnderwaterTunnelWeldSound_BlowtorchLoop)
	ENDIF	
	
	IF DOES_PARTICLE_FX_LOOPED_EXIST(ptUnderwaterTunnelWeldPTFX_IdleBlueFlame)
		STOP_PARTICLE_FX_LOOPED(ptUnderwaterTunnelWeldPTFX_IdleBlueFlame)
	ENDIF 
		
ENDPROC

PROC UNLOAD_MISSION_SPECIFIC_PTFX()
	PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - UNLOAD_MISSION_SPECIFIC_PTFX ################################")
	INT i

	IF iRadioEmitterPropCheck != -1
		SET_STATIC_EMITTER_ENABLED("SE_Script_Placed_Prop_Emitter_Boombox", FALSE)
		CLEAN_UP_MANSION_SPEAKERS()
		iRadioEmitterPropCheck = -1
	ENDIF

	//Box Pile
	IF IS_BIT_SET(iLocalBoolCheck21, LBOOL21_PARTICLE_FX_OVERRIDE_BOX_PILE)
		PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP RESET_PARTICLE_FX_OVERRIDE \"ent_dst_gen_cardboard\"")
		RESET_PARTICLE_FX_OVERRIDE("ent_dst_gen_cardboard")
		CLEAR_BIT(iLocalBoolCheck21, LBOOL21_PARTICLE_FX_OVERRIDE_BOX_PILE)
	ENDIF
	
	IF iTitanRotorParticlesBitset != 0
		REMOVE_NAMED_PTFX_ASSET("scr_gr_def")
	ENDIF
	
	INT iPropEF
	FOR iPropEF = 0 TO FMMC_MAX_NUM_FADEOUT_PROPS_EVERY_FRAME-1
		IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfx_PropFadeoutEveryFrameIndex[iPropEF])
			REMOVE_PARTICLE_FX(ptfx_PropFadeoutEveryFrameIndex[iPropEF], TRUE)
		ENDIF
	ENDFOR

	IF players_underwater_flare != NULL
		IF DOES_PARTICLE_FX_LOOPED_EXIST( players_underwater_flare )
			STOP_PARTICLE_FX_LOOPED(players_underwater_flare)
		ENDIF
		players_underwater_flare= NULL
	ENDIF
	IF ptfxThermite[0] != NULL
		IF DOES_PARTICLE_FX_LOOPED_EXIST( ptfxThermite[0] )
			STOP_PARTICLE_FX_LOOPED(ptfxThermite[0])
		ENDIF
		ptfxThermite[0]= NULL
	ENDIF
	IF ptfxThermite[1] != NULL
		IF DOES_PARTICLE_FX_LOOPED_EXIST( ptfxThermite[1] )
			STOP_PARTICLE_FX_LOOPED(ptfxThermite[1])
		ENDIF
		ptfxThermite[1]= NULL
	ENDIF
	i = 0
	REPEAT ciMAX_LOCAL_THERMITE_PTFX i
		IF ptfxThermiteLocalDrip[i] != NULL
			IF DOES_PARTICLE_FX_LOOPED_EXIST( ptfxThermiteLocalDrip[i] )
				STOP_PARTICLE_FX_LOOPED(ptfxThermiteLocalDrip[i])
			ENDIF
			ptfxThermiteLocalDrip[i]= NULL
		ENDIF
		IF ptfxThermiteLocalSpark[i] != NULL
			IF DOES_PARTICLE_FX_LOOPED_EXIST( ptfxThermiteLocalSpark[i] )
				STOP_PARTICLE_FX_LOOPED(ptfxThermiteLocalSpark[i])
			ENDIF
			ptfxThermiteLocalSpark[i]= NULL
		ENDIF
	ENDREPEAT
	IF DrillptfxIndex != NULL
		IF DOES_PARTICLE_FX_LOOPED_EXIST( DrillptfxIndex )
			STOP_PARTICLE_FX_LOOPED(DrillptfxIndex)
		ENDIF
		DrillptfxIndex= NULL
	ENDIF
	IF OverheatptfxIndex != NULL
		IF DOES_PARTICLE_FX_LOOPED_EXIST( OverheatptfxIndex )
			STOP_PARTICLE_FX_LOOPED(OverheatptfxIndex)
		ENDIF
		OverheatptfxIndex= NULL
	ENDIF
	
	IF DOES_PARTICLE_FX_LOOPED_EXIST(trans_lights_ptfx)
		STOP_PARTICLE_FX_LOOPED(trans_lights_ptfx)
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck23, LBOOL23_TEAM_SWAP_IN_MISSION)
		REMOVE_NAMED_PTFX_ASSET("scr_as_trans")
	ENDIF
	
	IF IS_ANY_WARP_PORTALS_USING_TRANSFORM()
		REMOVE_NAMED_PTFX_ASSET("scr_as_trans")
	ENDIF
	
	IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxHeliDamageSmoke1)
		REMOVE_PARTICLE_FX(ptfxHeliDamageSmoke1)
	ENDIF
	IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxHeliDamageSmoke2)
		REMOVE_PARTICLE_FX(ptfxHeliDamageSmoke2)
	ENDIF
	
	FOR i = 0 TO (MC_serverBD.iNumVehCreated - 1)
		IF IS_THIS_MODEL_A_HELI(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn)
		AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehBitsetEight, ciFMMC_VEHICLE8_ENGINE_SMOKE_ON_LOW_BODY_HEALTH)
			PRINTLN("UNLOAD_MISSION_SPECIFIC_PTFX - Unloading helicopter smoke particle effects")
			REMOVE_NAMED_PTFX_ASSET("scr_vw_finale")
			BREAKLOOP
		ENDIF
	ENDFOR
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_UseBigExplosions)
		RESET_PARTICLE_FX_OVERRIDE("exp_grd_plane")
		PRINTLN("UNLOAD_MISSION_SPECIFIC_PTFX - Unloading big explosion override effect")
		REMOVE_NAMED_PTFX_ASSET("scr_vw_oil")
	ENDIF
	
	FOR i = 0 TO FMMC_MAX_NUM_DYNOPROPS - 1
	
		IF i < FMMC_MAX_NUM_OBJECTS
			IF DOES_PARTICLE_FX_LOOPED_EXIST(sObjTaserVars.ptLoopingTaserFX[i])
				REMOVE_PARTICLE_FX(sObjTaserVars.ptLoopingTaserFX[i], TRUE)
			ENDIF
		ENDIF
		
		IF DOES_PARTICLE_FX_LOOPED_EXIST(sDynoPropTaserVars.ptLoopingTaserFX[i])
			REMOVE_PARTICLE_FX(sDynoPropTaserVars.ptLoopingTaserFX[i], TRUE)
		ENDIF
		
		IF DOES_PARTICLE_FX_LOOPED_EXIST(ptObjectPTFX[i])
			REMOVE_PARTICLE_FX(ptObjectPTFX[i], TRUE)
		ENDIF
	ENDFOR

	IF SHOULD_LOAD_ELECTRONIC_ASSETS()
		REMOVE_NAMED_PTFX_ASSET("scr_ch_finale")
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck31, LBOOL31_USED_AIR_DEFENCE_PTFX)
		IF HAS_NAMED_PTFX_ASSET_LOADED("scr_apartment_mp")
			REMOVE_NAMED_PTFX_ASSET("scr_apartment_mp")	
		ENDIF
	ENDIF
	
	UNDERWATER_TUNNEL_CLEAN_UP_ALL_BREAK_PTFX()
	
	UNDERWATER_TUNNEL_WELD_CLEANUP_SOUND_AND_EFFECTS()

	REMOVE_NAMED_PTFX_ASSET("scr_mp_creator")
	
ENDPROC

FUNC BOOL HANDLE_LOADING_DEFENSE_ZONE_EFFECTS()
	
	BOOL bLoad
	INT i
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfZones - 1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iType = ciFMMC_ZONE_TYPE__AIR_DEFENCE
			bLoad = TRUE
			PRINTLN("[HANDLE_LOADING_DEFENSE_ZONE_EFFECTS] - i: ", i, " is air defense zone. bLoad = TRUE")
		ENDIF
	ENDFOR
	
	IF bLoad
		REQUEST_NAMED_PTFX_ASSET("scr_apartment_mp")
		SET_BIT(iLocalBoolCheck31, LBOOL31_USED_AIR_DEFENCE_PTFX)
		IF NOT HAS_NAMED_PTFX_ASSET_LOADED("scr_apartment_mp")
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE

ENDFUNC

FUNC BOOL HANDLE_LOADING_SEAMINE_EXPLOSION_EFFECTS()
	IF (MC_serverBD.iNumObjCreated > 0 AND MC_serverBD.iNumObjCreated <= FMMC_MAX_NUM_OBJECTS)
		INT i = 0
		
		FOR i = 0 TO (MC_serverBD.iNumObjCreated - 1)
			IF IS_MODEL_A_SEA_MINE(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].mn)
				REQUEST_NAMED_PTFX_ASSET("scr_xm_submarine")
				PRINTLN("[RCC MISSION] LOAD_PARTICLE_FX() - requested scr_xm_submarine effect")
				
				IF NOT HAS_NAMED_PTFX_ASSET_LOADED("scr_xm_submarine")
					RETURN FALSE
				ELSE
					PRINTLN("[RCC MISSION] LOAD_PARTICLE_FX() - seamine stuff loaded!")
					RETURN TRUE
				ENDIF
				
				i = MC_serverBD.iNumObjCreated // Break out!
			ENDIF
		ENDFOR
	ENDIF
	
	RETURN TRUE // No sea mines to worry about
ENDFUNC

FUNC BOOL HANDLE_LOADING_INVISIBLE_PED_FLICKER_EFFECTS()
	IF (g_FMMC_STRUCT_ENTITIES.iNumberOfPeds > 0 AND MC_serverBD.iNumPedCreated <= FMMC_MAX_PEDS)
		INT i = 0
		
		FOR i = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfPeds - 1)
			IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iBecomeCloakedOnRule > -1
				REQUEST_NAMED_PTFX_ASSET("scr_xm_heat")
				PRINTLN("[RCC MISSION] LOAD_PARTICLE_FX() - requested scr_xm_heat effect ", i)
				
				IF NOT HAS_NAMED_PTFX_ASSET_LOADED("scr_xm_heat")
					PRINTLN("[RCC MISSION] LOAD_PARTICLE_FX() - scr_xm_heat not loaded yet!", i)
					RETURN FALSE
				ELSE
					PRINTLN("[RCC MISSION] LOAD_PARTICLE_FX() - scr_xm_heat loaded!", i)
					RETURN TRUE
				ENDIF
				
				i = g_FMMC_STRUCT_ENTITIES.iNumberOfPeds // Break out!
			ENDIF
		ENDFOR
	ENDIF
	
	RETURN TRUE // No invisible peds to worry about
ENDFUNC

FUNC BOOL PROCESS_LOADING_ELECTRONIC_VFX()
	
	BOOL bReady = TRUE
	
	IF SHOULD_LOAD_ELECTRONIC_ASSETS()
		REQUEST_NAMED_PTFX_ASSET("scr_ch_finale")
		
		IF NOT HAS_NAMED_PTFX_ASSET_LOADED("scr_ch_finale")
			PRINTLN("[Taser] PROCESS_LOADING_ELECTRONIC_VFX - Waiting for scr_ch_finale")
			bReady = FALSE
		ENDIF
	ENDIF
	
	RETURN bReady
ENDFUNC

FUNC STRING GET_BURNING_VEHICLE_PTFX_FRONT(MODEL_NAMES mnVehicle)
	
	SWITCH mnVehicle
		CASE FELON2						RETURN "scr_sum_gy_felon_fire_front"
		CASE GAUNTLET4					RETURN "scr_sum_gy_gauntlet_fire_front"
		CASE TEMPESTA					RETURN "scr_sum_gy_tempesta_fire_front"
		CASE YOUGA4						RETURN "scr_sec_weed_burn_van"
	ENDSWITCH
	
	RETURN "scr_sum_gy_mule4_fire_front" // Can re-use "scr_xm_riotvan_fire_front" if this is used on smaller vehicles again
ENDFUNC

FUNC STRING GET_BURNING_VEHICLE_PTFX_BACK(MODEL_NAMES mnVehicle, INT iVeh)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iBurningVehicleBS, ciFMMC_BURNING_VEH_SINGLE_FIRE_PTFX)
		RETURN ""
	ENDIF
	
	SWITCH mnVehicle
		CASE FELON2		RETURN "scr_sum_gy_felon_fire_back"
		CASE GAUNTLET4	RETURN "scr_sum_gy_gauntlet_fire_back"
		CASE TEMPESTA	RETURN "scr_sum_gy_tempesta_fire_back"
		CASE YOUGA4		RETURN ""
	ENDSWITCH
	
	RETURN "scr_sum_gy_mule4_fire_back" // Can re-use "scr_xm_riotvan_fire_back" if this is used on smaller vehicles again
ENDFUNC

FUNC STRING GET_BURNING_VEHICLE_PTFX_ASSET(MODEL_NAMES mnVehicle = DUMMY_MODEL_FOR_SCRIPT)
	
	SWITCH mnVehicle
		CASE YOUGA4
			RETURN "scr_sec"
	ENDSWITCH
	
	RETURN "scr_sum_gy" // Can re-use "scr_xm_riotvan" if this is used on smaller vehicles again
ENDFUNC

FUNC BOOL DOES_VEHICLE_MODEL_REQUIRE_ALTERNATIVE_SMOKE_VFX(MODEL_NAMES mnVehicle)
	SWITCH mnVehicle
		CASE YOUGA4
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_LOAD_BURNING_VEHICLE_PTFX()
	RETURN IS_BIT_SET(iLocalBoolCheck27, LBOOL27_PROCESS_BURNING_VANS)
ENDFUNC

FUNC BOOL PROCESS_LOADING_BURNING_VEHICLE_PTFX()
	
	IF NOT SHOULD_LOAD_BURNING_VEHICLE_PTFX()
		RETURN TRUE
	ENDIF

	REQUEST_NAMED_PTFX_ASSET(GET_BURNING_VEHICLE_PTFX_ASSET()) 
		
	IF NOT HAS_NAMED_PTFX_ASSET_LOADED(GET_BURNING_VEHICLE_PTFX_ASSET())
		PRINTLN("[BURNING_VEHICLE] LOAD_PARTICLE_FX - PROCESS_LOADING_BURNING_VEHICLE_PTFX - loading")
		RETURN FALSE
	ENDIF
		
	RETURN TRUE
	
ENDFUNC

FUNC STRING GET_HELI_SMOKE_PTFX_ASSET(INT iVeh)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetTen, ciFMMC_VEHICLE10_UseFixerEngineSmoke)
		RETURN "scr_sec"
	ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetTen, ciFMMC_VEHICLE10_UseULPMissionEngineSmoke)
		RETURN "scr_sum2_agent"
	ENDIF
	
	RETURN "scr_vw_finale"
	
ENDFUNC

FUNC STRING GET_HELI_SMOKE_PTFX_NAME(INT iVeh)

	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetTen, ciFMMC_VEHICLE10_UseFixerEngineSmoke)
		RETURN "scr_sec_helicopter_damage"
	ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetTen, ciFMMC_VEHICLE10_UseULPMissionEngineSmoke)
		RETURN "scr_sum2_agent_helicopter_damage"
	ENDIF
	
	RETURN "scr_vw_finale_heli_smoke"
	
ENDFUNC

FUNC BOOL PROCESS_LOADING_VEHICLE_PTFX()
	INT i

	FOR i = 0 TO (MC_serverBD.iNumVehCreated - 1)
		IF IS_THIS_MODEL_A_HELI(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn)
		AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehBitsetEight, ciFMMC_VEHICLE8_ENGINE_SMOKE_ON_LOW_BODY_HEALTH)
		
			REQUEST_NAMED_PTFX_ASSET(GET_HELI_SMOKE_PTFX_ASSET(i))
			PRINTLN("[RCC MISSION] LOAD_PARTICLE_FX - PROCESS_LOADING_VEHICLE_PTFX - requested scr_vw_finale smoke effect")
			
			IF NOT HAS_NAMED_PTFX_ASSET_LOADED(GET_HELI_SMOKE_PTFX_ASSET(i))
				PRINTLN("[RCC MISSION] LOAD_PARTICLE_FX - PROCESS_LOADING_VEHICLE_PTFX - scr_vw_finale not loaded yet")
				RETURN FALSE
			ENDIF
			
			BREAKLOOP
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehBitsetSeven, ciFMMC_VEHICLE7_SPAWN_BURNING)
		AND DOES_VEHICLE_MODEL_REQUIRE_ALTERNATIVE_SMOKE_VFX(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn)
			REQUEST_NAMED_PTFX_ASSET(GET_BURNING_VEHICLE_PTFX_ASSET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn))
			
			IF NOT HAS_NAMED_PTFX_ASSET_LOADED(GET_BURNING_VEHICLE_PTFX_ASSET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn))
				PRINTLN("[RCC MISSION] LOAD_PARTICLE_FX - PROCESS_LOADING_VEHICLE_PTFX - Smoke not loaded yet")
				RETURN FALSE
			ENDIF
			
			BREAKLOOP
		ENDIF
	ENDFOR
			
	RETURN TRUE
ENDFUNC

FUNC BOOL HANDLE_LOADING_BIG_EXPLOSION_PTFX()
	REQUEST_NAMED_PTFX_ASSET("scr_vw_oil")
	PRINTLN("[ML MISSION] LOAD_PARTICLE_FX() - requested scr_vw_oil big explosion effect")
	
	IF NOT HAS_NAMED_PTFX_ASSET_LOADED("scr_vw_oil")
		PRINTLN("[ML MISSION] LOAD_PARTICLE_FX() - scr_vw_oil not loaded yet")
		RETURN FALSE
	ELSE	
		PRINTLN("[ML MISSION] LOAD_PARTICLE_FX() - scr_vw_oil has loaded successfully")
		RETURN TRUE
	ENDIF
ENDFUNC

FUNC FLOAT GET_PTFX_SCALE_FOR_ENTITY_CUSTOM_DEATH_EXPLOSION(MODEL_NAMES mnEntityModel)
	IF mnEntityModel = INT_TO_ENUM(MODEL_NAMES, HASH("tr_Prop_TR_Break_Dev_01a"))
		RETURN 1.0
	ENDIF
	IF mnEntityModel = reh_prop_reh_drone_02a
	OR mnEntityModel = REH_PROP_REH_DRONE_BRK_02A
		RETURN 2.0
	ENDIF
	RETURN 1.0
ENDFUNC

FUNC STRING GET_PTFX_ASSET_NAME_FOR_ENTITY_CUSTOM_DEATH_EXPLOSION(MODEL_NAMES mnEntityModel)
	IF mnEntityModel = INT_TO_ENUM(MODEL_NAMES, HASH("tr_Prop_TR_Break_Dev_01a"))
		RETURN "scr_tn_tr"
	ENDIF
	IF mnEntityModel = reh_prop_reh_drone_02a
	OR mnEntityModel = REH_PROP_REH_DRONE_BRK_02A
		RETURN "scr_fm_mp_missioncreator"
	ENDIF
	IF mnEntityModel = reh_prop_reh_drone_02a
	OR mnEntityModel = REH_PROP_REH_DRONE_BRK_02A
		RETURN "scr_mp_elec_dst"
	ENDIF
	RETURN ""
ENDFUNC

FUNC STRING GET_PTFX_FX_NAME_FOR_ENTITY_CUSTOM_DEATH_EXPLOSION(MODEL_NAMES mnEntityModel)
	IF mnEntityModel = INT_TO_ENUM(MODEL_NAMES, HASH("tr_Prop_TR_Break_Dev_01a"))
		RETURN "scr_exp_train_brake"
	ENDIF
	IF mnEntityModel = reh_prop_reh_drone_02a
	OR mnEntityModel = REH_PROP_REH_DRONE_BRK_02A
		RETURN "scr_mp_elec_dst"
	ENDIF

	RETURN ""
ENDFUNC

FUNC BOOL DOES_ENTITY_USE_CUSTOM_DEATH_EXPLOSION_PTFX(MODEL_NAMES mnEntityModel)	
	RETURN NOT IS_STRING_NULL_OR_EMPTY(GET_PTFX_ASSET_NAME_FOR_ENTITY_CUSTOM_DEATH_EXPLOSION(mnEntityModel))
ENDFUNC

FUNC STRING GET_LOCH_SANTOS_MONSTER_PTFX_ASSET()
	RETURN "scr_sum_gy"
ENDFUNC

FUNC BOOL HAS_LOADED_ALL_PLACED_PTFX_ASSETS()
	
	BOOL bReturn = TRUE
	INT iPTFX
	FOR iPTFX = 0 TO g_FMMC_STRUCT.iNumPlacedPTFX-1
		IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sPlacedPtfx[iPTFX].vStartCoord)
			IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sPlacedPtfx[iPTFX].tl63AssetName)
				REQUEST_NAMED_PTFX_ASSET(g_FMMC_STRUCT.sPlacedPtfx[iPTFX].tl63AssetName)
				IF NOT HAS_NAMED_PTFX_ASSET_LOADED(g_FMMC_STRUCT.sPlacedPtfx[iPTFX].tl63AssetName)
					PRINTLN("[LM][Placed_PTFX] LOAD_PARTICLE_FX - iPTFX: ", iPTFX, " requested: ", g_FMMC_STRUCT.sPlacedPtfx[iPTFX].tl63AssetName, " Asset Name, but has not loaded yet.")
					bReturn = FALSE
				ELSE
					PRINTLN("[LM][Placed_PTFX] LOAD_PARTICLE_FX - iPTFX: ", iPTFX, " requested: ", g_FMMC_STRUCT.sPlacedPtfx[iPTFX].tl63AssetName, " Asset Name, and has loaded! Effect Name is: ", g_FMMC_STRUCT.sPlacedPtfx[iPTFX].tl63EffectName)					
					SET_BIT(sPlacedPtfxLocal[iPTFX].iPtfxBS, ci_PLACED_PTFX_Loaded)
					// add option for fx override. SET_PARTICLE_FX_OVERRIDE
				ENDIF
			ELSE
				// Uses Mission Controller Asset Name. Presume it's loaded.
				IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sPlacedPTFX[iPTFX].tl63EffectName)
					IF NOT IS_BIT_SET(sPlacedPtfxLocal[iPTFX].iPtfxBS, ci_PLACED_PTFX_Loaded)
						PRINTLN("[LM][Placed_PTFX] LOAD_PARTICLE_FX - iPTFX: ", iPTFX, " using scr_mp_controller asset name (default) setting as loaded.")
						SET_BIT(sPlacedPtfxLocal[iPTFX].iPtfxBS, ci_PLACED_PTFX_Loaded)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN bReturn
ENDFUNC

FUNC BOOL LOAD_EXTRA_PTFX_FOR_ENTITY_MODEL(MODEL_NAMES mnEntityModel)

	BOOL bAllLoaded = TRUE
	
	STRING sEntityCustomExplosionPTFXAsset = GET_PTFX_ASSET_NAME_FOR_ENTITY_CUSTOM_DEATH_EXPLOSION(mnEntityModel)
	IF NOT IS_STRING_EMPTY(sEntityCustomExplosionPTFXAsset)
		REQUEST_NAMED_PTFX_ASSET(sEntityCustomExplosionPTFXAsset)
		IF NOT HAS_NAMED_PTFX_ASSET_LOADED(sEntityCustomExplosionPTFXAsset)	
			PRINTLN("[RCC MISSION][EntityExplosion] LOAD_PARTICLE_FX() || LOAD_EXTRA_PTFX_FOR_ENTITY_MODEL | Loading custom death explosion FX: ", sEntityCustomExplosionPTFXAsset)
			bAllLoaded = FALSE
		ENDIF
	ENDIF
	
	IF NOT IS_STRING_EMPTY(GET_SCRIPTED_FLAMMABLE_ENTITY_PTFX_ASSET(mnEntityModel))
		REQUEST_NAMED_PTFX_ASSET(GET_SCRIPTED_FLAMMABLE_ENTITY_PTFX_ASSET(mnEntityModel))
		IF NOT HAS_NAMED_PTFX_ASSET_LOADED(GET_SCRIPTED_FLAMMABLE_ENTITY_PTFX_ASSET(mnEntityModel))	
			PRINTLN("[RCC MISSION][FlammableObject] LOAD_PARTICLE_FX() || LOAD_EXTRA_PTFX_FOR_ENTITY_MODEL | Loading scripted flammable object FX: ", GET_SCRIPTED_FLAMMABLE_ENTITY_PTFX_ASSET(mnEntityModel))
			bAllLoaded = FALSE
		ENDIF
	ENDIF
	
	RETURN bAllLoaded
ENDFUNC

PROC RELEASE_EXTRA_PTFX_FOR_ENTITY_MODEL(MODEL_NAMES mnEntityModel)

	STRING sEntityCustomExplosionPTFXAsset = GET_PTFX_ASSET_NAME_FOR_ENTITY_CUSTOM_DEATH_EXPLOSION(mnEntityModel)
	
	IF NOT IS_STRING_EMPTY(sEntityCustomExplosionPTFXAsset)
		PRINTLN("[RCC MISSION][EntityExplosion] LOAD_EXTRA_PTFX_FOR_ENTITY_MODEL | Releasing custom death explosion FX: ", sEntityCustomExplosionPTFXAsset)
		REMOVE_NAMED_PTFX_ASSET(sEntityCustomExplosionPTFXAsset)
	ENDIF
	
	IF NOT IS_STRING_EMPTY(GET_SCRIPTED_FLAMMABLE_ENTITY_PTFX_ASSET(mnEntityModel))
		PRINTLN("[RCC MISSION][FlammableObject] LOAD_EXTRA_PTFX_FOR_ENTITY_MODEL | Releasing scripted flammable object FX: ", GET_SCRIPTED_FLAMMABLE_ENTITY_PTFX_ASSET(mnEntityModel))
		REMOVE_NAMED_PTFX_ASSET(GET_SCRIPTED_FLAMMABLE_ENTITY_PTFX_ASSET(mnEntityModel))
	ENDIF
	
ENDPROC

FUNC STRING GET_PTFX_NAME_FROM_ENTITY_PTFX_TYPE(FMMC_ENTITY_PTFX_TYPE eEntPtfxType)
	SWITCH eEntPtfxType
		CASE FMMC_ENTITY_PTFX_SMOKE_POWERPLAY_BEAST_APPEAR RETURN "scr_powerplay_beast_appear"
		CASE FMMC_ENTITY_PTFX_SMOKE_POWERPLAY_BEAST_VANISH RETURN "scr_powerplay_beast_vanish"
		CASE FMMC_ENTITY_PTFX_SPARKS					   RETURN "scr_alien_teleport"
	ENDSWITCH
	RETURN ""
ENDFUNC

FUNC STRING GET_PTFX_ASSET_NAME_FROM_ENTITY_PTFX_TYPE(FMMC_ENTITY_PTFX_TYPE eEntPtfxType)
	SWITCH eEntPtfxType
		CASE FMMC_ENTITY_PTFX_SMOKE_POWERPLAY_BEAST_APPEAR RETURN "scr_powerplay"
		CASE FMMC_ENTITY_PTFX_SMOKE_POWERPLAY_BEAST_VANISH RETURN "scr_powerplay"
		CASE FMMC_ENTITY_PTFX_SPARKS					   RETURN "scr_rcbarry1"
	ENDSWITCH
	RETURN ""
ENDFUNC

PROC USE_ENTITY_PTFX_ASSET(ENTITY_PTFX_STRUCT &sEntFXData)
	USE_PARTICLE_FX_ASSET(GET_PTFX_ASSET_NAME_FROM_ENTITY_PTFX_TYPE(sEntFXData.ePTFXType))
ENDPROC

PROC SETUP_NON_LOOPED_ENTITY_PTFX_ASSET(ENTITY_PTFX_STRUCT &sEntFXData)
	SET_PARTICLE_FX_NON_LOOPED_COLOUR(sEntFXData.sColour.R, sEntFXData.sColour.G, sEntFXData.sColour.B)
	SET_PARTICLE_FX_NON_LOOPED_ALPHA(sEntFXData.sColour.A)
	SET_PARTICLE_FX_NON_LOOPED_SCALE(sEntFXData.fSize)
ENDPROC

PROC SETUP_LOOPED_ENTITY_PTFX_ASSET(ENTITY_PTFX_STRUCT &sEntFXData, PTFX_ID &ptxId)
	SET_PARTICLE_FX_LOOPED_COLOUR(ptxId, sEntFXData.sColour.R, sEntFXData.sColour.G, sEntFXData.sColour.B)
	SET_PARTICLE_FX_LOOPED_ALPHA(ptxId, sEntFXData.sColour.A)
	SET_PARTICLE_FX_LOOPED_SCALE(ptxId, sEntFXData.fSize)
ENDPROC

FUNC BOOL IS_ENTITY_PTFX_TYPE_IN_USE(FMMC_ENTITY_PTFX_TYPE eFXType)
	RETURN IS_BIT_SET(iEntityPTFXTypeInUseBS, ENUM_TO_INT(eFXType))
ENDFUNC

FUNC BOOL START_ENTITY_PTFX_AT_COORDS(ENTITY_PTFX_STRUCT &sEntFXDat, VECTOR vCoords, VECTOR vRotation)
	IF sEntFXDat.ePTFXType != FMMC_ENTITY_PTFX_NONE
		STRING sFXName = GET_PTFX_NAME_FROM_ENTITY_PTFX_TYPE(sEntFXDat.ePTFXType)
		USE_ENTITY_PTFX_ASSET(sEntFXDat)
		SETUP_NON_LOOPED_ENTITY_PTFX_ASSET(sEntFXDat)
		PRINTLN("START_ENTITY_PTFX_AT_COORDS - Calling START_NETWORKED_PARTICLE_FX_NON_LOOPED_AT_COORD with \"", sFXName, "\", Scale = ", FLOAT_TO_STRING(sEntFXDat.fSize, 1), ", Colour = ", RGBA_TO_STRING(sEntFXDat.sColour))
		RETURN START_NETWORKED_PARTICLE_FX_NON_LOOPED_AT_COORD(sFXName, vCoords, vRotation, sEntFXDat.fSize)
	ENDIF
	//TODO: Check if the asset is no longer needed so we can delete it
	PRINTLN("START_ENTITY_PTFX_AT_COORDS - Unable to start the particle effect as none was selected! (ePTFXType = FMMC_ENTITY_PTFX_NONE)")
	RETURN FALSE
ENDFUNC

PROC HANDLE_LOADING_ENTITY_PTFX_ASSETS()
	INT i
	FMMC_ENTITY_PTFX_TYPE eFXTypeSpawn, eFXTypeDespawn, eFXTypeWarp
	
	//PEDS
	i = 0
	REPEAT FMMC_MAX_PEDS i
		eFXTypeSpawn    = g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sEntitySpawnPTFX.ePTFXType
		eFXTypeDespawn  = g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sEntityDespawnPTFX.ePTFXType
		eFXTypeWarp    = g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sWarpLocationSettings.sEntityWarpPTFX.ePTFXType
		
		IF eFXTypeSpawn != FMMC_ENTITY_PTFX_NONE
			SET_BIT(iEntityPTFXTypeInUseBS, ENUM_TO_INT(eFXTypeSpawn))
		ENDIF
		
		IF eFXTypeDespawn != FMMC_ENTITY_PTFX_NONE
			SET_BIT(iEntityPTFXTypeInUseBS, ENUM_TO_INT(eFXTypeDespawn))
		ENDIF
		
		IF eFXTypeWarp != FMMC_ENTITY_PTFX_NONE
			SET_BIT(iEntityPTFXTypeInUseBS, ENUM_TO_INT(eFXTypeWarp))
		ENDIF	
	ENDREPEAT
	
	//VEHS
	i = 0
	REPEAT FMMC_MAX_VEHICLES i
		eFXTypeSpawn    = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].sEntitySpawnPTFX.ePTFXType
		eFXTypeDespawn  = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].sEntityDespawnPTFX.ePTFXType
		eFXTypeWarp    = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].sWarpLocationSettings.sEntityWarpPTFX.ePTFXType
		
		IF eFXTypeSpawn != FMMC_ENTITY_PTFX_NONE
			SET_BIT(iEntityPTFXTypeInUseBS, ENUM_TO_INT(eFXTypeSpawn))
		ENDIF
		
		IF eFXTypeDespawn != FMMC_ENTITY_PTFX_NONE
			SET_BIT(iEntityPTFXTypeInUseBS, ENUM_TO_INT(eFXTypeDespawn))
		ENDIF
		
		IF eFXTypeWarp != FMMC_ENTITY_PTFX_NONE
			SET_BIT(iEntityPTFXTypeInUseBS, ENUM_TO_INT(eFXTypeWarp))
		ENDIF	
	ENDREPEAT
	
	//OBJECTS
	i = 0
	REPEAT FMMC_MAX_NUM_OBJECTS i
		eFXTypeSpawn    = g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sEntitySpawnPTFX.ePTFXType
		eFXTypeDespawn  = g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sEntityDespawnPTFX.ePTFXType
		eFXTypeWarp    = g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sWarpLocationSettings.sEntityWarpPTFX.ePTFXType		
		
		IF eFXTypeSpawn != FMMC_ENTITY_PTFX_NONE
			SET_BIT(iEntityPTFXTypeInUseBS, ENUM_TO_INT(eFXTypeSpawn))
		ENDIF
		
		IF eFXTypeDespawn != FMMC_ENTITY_PTFX_NONE
			SET_BIT(iEntityPTFXTypeInUseBS, ENUM_TO_INT(eFXTypeDespawn))
		ENDIF
		
		IF eFXTypeWarp != FMMC_ENTITY_PTFX_NONE
			SET_BIT(iEntityPTFXTypeInUseBS, ENUM_TO_INT(eFXTypeWarp))
		ENDIF		
	ENDREPEAT
	
	//DYNO PROPS
	i = 0
	REPEAT FMMC_MAX_NUM_DYNOPROPS i
		eFXTypeSpawn    = g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].sEntitySpawnPTFX.ePTFXType
		eFXTypeDespawn  = g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].sEntityDespawnPTFX.ePTFXType		
		
		IF eFXTypeSpawn != FMMC_ENTITY_PTFX_NONE
			SET_BIT(iEntityPTFXTypeInUseBS, ENUM_TO_INT(eFXTypeSpawn))
		ENDIF
		
		IF eFXTypeDespawn != FMMC_ENTITY_PTFX_NONE
			SET_BIT(iEntityPTFXTypeInUseBS, ENUM_TO_INT(eFXTypeDespawn))
		ENDIF		
	ENDREPEAT
	
	//Locations
	i = 0
	REPEAT FMMC_MAX_GO_TO_LOCATIONS i
		eFXTypeWarp    = g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].sWarpLocationSettings.sEntityWarpPTFX.ePTFXType		
		
		IF eFXTypeWarp != FMMC_ENTITY_PTFX_NONE
			SET_BIT(iEntityPTFXTypeInUseBS, ENUM_TO_INT(eFXTypeWarp))
		ENDIF
	ENDREPEAT
	
	//Interactables
	i = 0
	REPEAT FMMC_MAX_NUM_INTERACTABLES i
		eFXTypeWarp    = g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].sWarpLocationSettings.sEntityWarpPTFX.ePTFXType		
		
		IF eFXTypeWarp != FMMC_ENTITY_PTFX_NONE
			SET_BIT(iEntityPTFXTypeInUseBS, ENUM_TO_INT(eFXTypeWarp))
		ENDIF
	ENDREPEAT	
		
	//Loading
	i = 0
	REPEAT COUNT_OF(FMMC_ENTITY_PTFX_TYPE) i
		IF i < 0
			RELOOP
		ENDIF
		
		//Reloop if this PTFX type is not used by any entity
		IF NOT IS_BIT_SET(iEntityPTFXTypeInUseBS, i)
			RELOOP
		ENDIF
		
		eFXTypeSpawn = INT_TO_ENUM(FMMC_ENTITY_PTFX_TYPE, i)
		REQUEST_NAMED_PTFX_ASSET(GET_PTFX_ASSET_NAME_FROM_ENTITY_PTFX_TYPE(eFXTypeSpawn))
		
		IF NOT HAS_NAMED_PTFX_ASSET_LOADED(GET_PTFX_ASSET_NAME_FROM_ENTITY_PTFX_TYPE(eFXTypeSpawn))	
			PRINTLN("[RCC MISSION] HANDLE_LOADING_ENTITY_PTFX_ASSETS() - ", GET_PTFX_ASSET_NAME_FROM_ENTITY_PTFX_TYPE(eFXTypeSpawn) ," not loaded!")
		ENDIF
	ENDREPEAT
ENDPROC

PROC REMOVE_ENTITY_PTFX_ASSETS()
	INT i
	REPEAT COUNT_OF(FMMC_ENTITY_PTFX_TYPE) i
		IF i < 0
			RELOOP
		ENDIF
		
		//Reloop if this PTFX type is not used by any entity
		IF NOT IS_BIT_SET(iEntityPTFXTypeInUseBS, i)
			RELOOP
		ENDIF
		
		FMMC_ENTITY_PTFX_TYPE eFXType = INT_TO_ENUM(FMMC_ENTITY_PTFX_TYPE, i)
		REMOVE_NAMED_PTFX_ASSET(GET_PTFX_ASSET_NAME_FROM_ENTITY_PTFX_TYPE(eFXType))
		PRINTLN("[RCC MISSION] REMOVE_ENTITY_PTFX_ASSETS() - removing ", GET_PTFX_ASSET_NAME_FROM_ENTITY_PTFX_TYPE(eFXType))
	ENDREPEAT
	
	iEntityPTFXTypeInUseBS = 0
ENDPROC

PROC PLAY_WARP_ENTITY_TO_WARP_LOCATION_PTFX(VECTOR vPos, ENTITY_PTFX_STRUCT sEntityWarpPTFX, BOOL bPlay)
	
	IF NOT bPlay
		EXIT
	ENDIF
	
	START_ENTITY_PTFX_AT_COORDS(sEntityWarpPTFX, vPos, <<0.0, 0.0, 0.0>>)
	
ENDPROC

/// PURPOSE: Load all particle effects required this mission
FUNC BOOL LOAD_PARTICLE_FX()

	BOOL bReturn = TRUE

	REQUEST_NAMED_PTFX_ASSET("scr_fm_mp_missioncreator")
	IF NOT HAS_NAMED_PTFX_ASSET_LOADED("scr_fm_mp_missioncreator")	
		PRINTLN("[RCC MISSION] LOAD_PARTICLE_FX() - scr_fm_mp_missioncreator not loaded...")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_UseBigExplosions)
		SET_PARTICLE_FX_OVERRIDE("exp_grd_plane", "scr_vw_oil_tanker_explosion")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciCASH_EXPLOSION_ON_TEAM_SWAP)
		REQUEST_NAMED_PTFX_ASSET("scr_tplaces")
		PRINTLN("[RCC MISSION] LOAD_PARTICLE_FX() - requested scr_tplaces cash explosion effect")
		
		IF NOT HAS_NAMED_PTFX_ASSET_LOADED("scr_tplaces")
			bReturn = FALSE
		ENDIF
	ENDIF
	
	//Load Entity PTFX Assets
	HANDLE_LOADING_ENTITY_PTFX_ASSETS()
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_TRANSFORM_ASSETS)
		REQUEST_NAMED_PTFX_ASSET("scr_impexp_jug")
		PRINTLN("[RCC MISSION] LOAD_PARTICLE_FX() - requested scr_impexp_jug juggernaut explosion effect")
		
		IF NOT HAS_NAMED_PTFX_ASSET_LOADED("scr_impexp_jug")
			bReturn = FALSE
		ENDIF
	ENDIF
	
	IF NOT HANDLE_LOADING_SEAMINE_EXPLOSION_EFFECTS() // Loads seamine explosion effects only if it detects that there are seamines
		bReturn = FALSE
	ENDIF
	
	IF NOT PROCESS_LOADING_BURNING_VEHICLE_PTFX()
		bReturn = FALSE
	ENDIF
	
	IF NOT HANDLE_LOADING_INVISIBLE_PED_FLICKER_EFFECTS() // Loads invisible ped flicker effects if invisible peds are in this mission
		bReturn = FALSE
	ENDIF
	
	IF NOT PROCESS_LOADING_ELECTRONIC_VFX()
		bReturn = FALSE
	ENDIF
		
	INT iDynopropLoop	
	FOR iDynopropLoop = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps-1
		IF IS_THIS_MODEL_A_TRAP_DYNO_PROP(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynopropLoop].mn)
			IF NOT LOAD_ARENA_TRAP_ASSETS()
				bReturn = FALSE
			ENDIF
			BREAKLOOP
		ENDIF
		
		IF NOT LOAD_EXTRA_PTFX_FOR_ENTITY_MODEL(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynopropLoop].mn)
			bReturn = FALSE
		ENDIF
	ENDFOR
	
	INT iObjectLoop	
	FOR iObjectLoop = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfObjects-1
		IF NOT LOAD_EXTRA_PTFX_FOR_ENTITY_MODEL(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjectLoop].mn)
			bReturn = FALSE
		ENDIF
	ENDFOR
	
	INT iInteractableLoop	
	FOR iInteractableLoop = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfInteractables-1
		IF NOT LOAD_EXTRA_PTFX_FOR_ENTITY_MODEL(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractableLoop].mnInteractable_Model)
			bReturn = FALSE
		ENDIF
	ENDFOR
	
	IF NOT PROCESS_LOADING_VEHICLE_PTFX()
		bReturn = FALSE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_UseBigExplosions)
		IF NOT HANDLE_LOADING_BIG_EXPLOSION_PTFX()
			bReturn = FALSE
		ENDIF
	ENDIF
	
	IF NOT HAS_LOADED_ALL_PLACED_PTFX_ASSETS()
		PRINTLN("[LM][Placed_PTFX] LOAD_PARTICLE_FX returning false.")
		bReturn = FALSE
	ENDIF
	
	IF NOT HANDLE_LOADING_DEFENSE_ZONE_EFFECTS()
		PRINTLN("[LM][HANDLE_LOADING_DEFENSE_ZONE_EFFECTS] LOAD_PARTICLE_FX returning false.")
		bReturn = FALSE
	ENDIF
	
	RETURN bReturn
	
ENDFUNC

// ##### ------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Audio ----------------------------------------------------------------------------------------------------
// ##### Description: Loading and unloading Audio.  -----------------------------------------------------------------------------
// ##### ------------------------------------------------------------------------------------------------------------------------
// ##### ------------------------------------------------------------------------------------------------------------------------

FUNC BOOL SHOULD_MUSIC_LOCKER_MUSIC_CUE_SPECIAL_CONDITIONS_TRIGGER_MOOD()
	
	IF GET_FIXER_STORY_MISSION_FROM_ROOT_CONTENT_ID(g_FMMC_STRUCT.iRootContentIDHash) = FSM_PARTY_PROMOTER_MISSION		
		IF iLocalPlayerCurrentInteriorHash = ciINTERIOR_HASH_MUSIC_LOCKER
			INT iTeam = MC_PlayerBD[iPartToUse].iteam
			INT iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]
			IF iRule != 15
				PRINTLN("[MusicSwap] - Returning False - Not on correct rule.")
				RETURN FALSE
			ENDIF
			
			IF IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iTeam], iRule)
				SET_BIT(iLocalBoolCheck25, LBOOL25_FORCE_INTERIOR_ALT_1_MUSIC_TO_PLAY)				
				PRINTLN("[MusicSwap] - Returning False - On midpoint, still in locker.")
				RETURN FALSE
			ELSE
				PRINTLN("[MusicSwap] - Returning False - Not on Midpoint.")
				RETURN FALSE
			ENDIF
		ENDIF
		
		IF NOT IS_BIT_SET(iLocalBoolCheck25, LBOOL25_FORCE_INTERIOR_ALT_1_MUSIC_TO_PLAY)
			PRINTLN("[MusicSwap] - Returning False - LBOOL25_FORCE_INTERIOR_ALT_1_MUSIC_TO_PLAY not set.")
			RETURN FALSE
		ELSE
			CLEAR_BIT(iLocalBoolCheck29, LBOOL29_MUSIC_LOCKER_MUSIC_STARTED)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC INT GET_VOICE_INT_FROM_PED_MODEL_NAME(MODEL_NAMES mn)
	IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("A_C_Panther"))		RETURN 13	ENDIF
	
	RETURN 0
ENDFUNC

FUNC STRING GET_RANDOM_SPEECH_CONTEXT_FOR_PED_MODEL_NAME(MODEL_NAMES mn)
	IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("A_C_Panther"))
		INT iRandom = GET_RANDOM_INT_IN_RANGE(0, 6)
		
		SWITCH iRandom
			CASE 0		RETURN "CHUFF"
			CASE 1		RETURN "GROWL"
			CASE 2		RETURN "ROAR"
			CASE 3		RETURN "SNIFF"
			CASE 4		RETURN "PURR"
			CASE 5		RETURN "AGITATED"
		ENDSWITCH
	ENDIF
	
	RETURN ""
ENDFUNC

FUNC STRING GET_ALTITUDE_FAIL_SOUNDSET()
	IF GET_YACHT_MISSION_FROM_ROOT_CONTENT_ID(g_FMMC_STRUCT.iRootContentIDHash) != eYACHT_MISSION_INVALID
		RETURN "dlc_sum20_yacht_missions_dd_sounds"
	ENDIF
	
	RETURN "GTAO_Speed_Race_Sounds"
ENDFUNC
FUNC STRING GET_ALTITUTDE_FAIL_ARMRED_SOUND_NAME()
	IF GET_YACHT_MISSION_FROM_ROOT_CONTENT_ID(g_FMMC_STRUCT.iRootContentIDHash) != eYACHT_MISSION_INVALID
		RETURN ""
	ENDIF
	
	RETURN "Armed"
ENDFUNC
FUNC STRING GET_ALTITUTDE_FAIL_COUNTDOWN_SOUND_NAME()
	IF GET_YACHT_MISSION_FROM_ROOT_CONTENT_ID(g_FMMC_STRUCT.iRootContentIDHash) != eYACHT_MISSION_INVALID
		RETURN "altitude_warning"
	ENDIF
	
	RETURN "Countdown"
ENDFUNC
FUNC STRING GET_ALTITUTDE_FAIL_COUNTDOWN_STOP_SOUND_NAME()
	IF GET_YACHT_MISSION_FROM_ROOT_CONTENT_ID(g_FMMC_STRUCT.iRootContentIDHash) != eYACHT_MISSION_INVALID
		RETURN ""
	ENDIF
	RETURN "Count_Stop"
ENDFUNC

FUNC BOOL SAFE_TO_UNLOAD_THIS_AUDIO_BANK_MID_MISSION(STRING sAudioBank)

	IF ARE_STRINGS_EQUAL(sAudioBank, "DLC_SUM20/SUM20_Yacht_LAS_01")
	OR ARE_STRINGS_EQUAL(sAudioBank, "DLC_VINEWOOD/VW_CASINO_FINALE")
	OR ARE_STRINGS_EQUAL(sAudioBank, "DLC_HEIST3/CASINO_HEIST_FINALE_GENERAL_01")
		RETURN FALSE
	ENDIF

	RETURN TRUE
ENDFUNC

PROC CLEANUP_WORLD_SOUND(INT iWorldSound)	
	SWITCH iWorldSound
		CASE 1
			IF iWorldSoundID_1 != -1			
				PRINTLN("[RCC MISSION][CLEANUP_WORLD_SOUND] Cleaning up iWorldSoundID_1: ", iWorldSoundID_1)
				IF NOT HAS_SOUND_FINISHED(iWorldSoundID_1)
					STOP_SOUND(iWorldSoundID_1)
				ENDIF
				RELEASE_SOUND_ID(iWorldSoundID_1)
				iWorldSoundID_1 = -1	
			ENDIF
		BREAK		
		CASE 2
			IF iWorldSoundID_2 != -1
				PRINTLN("[RCC MISSION][CLEANUP_WORLD_SOUND] Cleaning up iWorldSoundID_2: ", iWorldSoundID_2)
				IF NOT HAS_SOUND_FINISHED(iWorldSoundID_2)				
					STOP_SOUND(iWorldSoundID_2)
				ENDIF
				RELEASE_SOUND_ID(iWorldSoundID_2)
				iWorldSoundID_2 = -1	
			ENDIF
		BREAK
		CASE 3
			IF iWorldSoundID_3 != -1
				PRINTLN("[RCC MISSION][CLEANUP_WORLD_SOUND] Cleaning up iWorldSoundID_3: ", iWorldSoundID_3)
				IF NOT HAS_SOUND_FINISHED(iWorldSoundID_3)				
					STOP_SOUND(iWorldSoundID_3)
				ENDIF				
				RELEASE_SOUND_ID(iWorldSoundID_3)
				iWorldSoundID_3 = -1	
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC PROCESS_EMITTER_CLEANUP()
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyNine, ciOptionsBS29_DisablePBMansionEmitters)
		SET_STATIC_EMITTER_ENABLED("SE_PB_MANSION_HOUSE", TRUE)
		SET_STATIC_EMITTER_ENABLED("SE_PB_MANSION_GROTTO", TRUE)
		PRINTLN("PROCESS_EMITTER_CLEANUP - Cleaning up - renabling emitters for mansion")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyNine, ciOptionsBS29_DisableMusicStudioEmitters)
		SET_STATIC_EMITTER_ENABLED("SE_sf_dlc_studio_sec_studio_01", TRUE)
		SET_STATIC_EMITTER_ENABLED("SE_sf_dlc_studio_sec_studio_02", TRUE)
		SET_STATIC_EMITTER_ENABLED("SE_sf_dlc_studio_sec_lobby_01", TRUE)
		SET_STATIC_EMITTER_ENABLED("SE_sf_dlc_studio_sec_writers_01", TRUE)
		PRINTLN("PROCESS_EMITTER_CLEANUP - Enabling emitters for music studio")
	ENDIF
	
ENDPROC

FUNC STRING GET_SOUND_SCENE_NAME_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE(SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE eType)
	
	SWITCH eType
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_IAA_INTRO_SCENE
			RETURN "ULP1_Item_Recovery_Swear_Mixscene"
		BREAK
	ENDSWITCH
	
	RETURN ""
	
ENDFUNC

PROC UNLOAD_CUSTOM_SYNC_SCENE_AUDIO_SCENE(FMMC_CUTSCENE_SYNC_SCENE_DATA &sCutsceneSyncSceneData)
	
	STRING sSceneName = GET_SOUND_SCENE_NAME_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE(sCutsceneSyncSceneData.eType)
	IF NOT IS_STRING_NULL_OR_EMPTY(sSceneName)	
		PRINTLN("[LM][Cutscenes][ScriptedAnimSyncScene] - UNLOAD_CUSTOM_SYNC_SCENE_AUDIO_SCENE - Stopping Audio Scene: ", sSceneName)
		STOP_AUDIO_SCENE(sSceneName)		
	ENDIF
	
ENDPROC

PROC CLEANUP_FAKE_FLIGHT_LOOP_SOUND()
	
	IF iDroneCutsceneSoundLoop != -1
		IF NOT HAS_SOUND_FINISHED(iDroneCutsceneSoundLoop)
			STOP_SOUND(iDroneCutsceneSoundLoop)
		ENDIF
		RELEASE_SOUND_ID(iDroneCutsceneSoundLoop)
		iDroneCutsceneSoundLoop = -1
	ENDIF
	
ENDPROC

PROC STOP_SCRIPTED_CUTSCENE_LIFT_AUDIO__MOVE_LOOP()
	
	SWITCH eCSLift				
		CASE eCSLift_ConstructionSkyscraper_SouthA_Bottom	
		CASE eCSLift_ConstructionSkyscraper_SouthA_Mid		
		CASE eCSLift_ConstructionSkyscraper_SouthB_Bottom	
		CASE eCSLift_ConstructionSkyscraper_SouthB_Mid 		
		CASE eCSLift_ConstructionSkyscraper_NorthA_Bottom	
		CASE eCSLift_ConstructionSkyscraper_NorthA_Mid		
		CASE eCSLift_ConstructionSkyscraper_NorthA_Top		
		CASE eCSLift_ConstructionSkyscraper_NorthB_Bottom	
		CASE eCSLift_ConstructionSkyscraper_NorthB_Mid		
		CASE eCSLift_ConstructionSkyscraper_NorthB_Top	
			IF IS_AUDIO_SCENE_ACTIVE(("ULP5_Deal_Breaker_Ride_Lift_Scene"))
				STOP_AUDIO_SCENE("ULP5_Deal_Breaker_Ride_Lift_Scene")
			ENDIF
		BREAK
	ENDSWITCH
	
	IF iElevatorMovementSound > -1
		PRINTLN("[Cutscenes][CSLift] PROCESS_SCRIPTED_CUTSENE_LIFT_MOVING - iElevatorDescendSound > -1 The lift has stopped moving (Stop Sound). elevator_descend_loop")
		STOP_SOUND(iElevatorMovementSound)
		SCRIPT_RELEASE_SOUND_ID(iElevatorMovementSound)
	ENDIF
ENDPROC

PROC CLEANUP_SILO_INTERIOR_ULP_AMBIENT_AUDIO()
	IF IS_BIT_SET(iLocalBoolCheck35, LBOOL35_STARTED_ULP_BUNKER_AMBIENT_AUDIO)
		PRINTLN("CLEANUP_SILO_INTERIOR_ULP_AMBIENT_AUDIO - Cleaning up")
		SET_AMBIENT_ZONE_LIST_STATE("AZL_xm_x17dlc_int_lab_Extra_Zones", FALSE, TRUE)
		CLEAR_BIT(iLocalBoolCheck35, LBOOL35_STARTED_ULP_BUNKER_AMBIENT_AUDIO)
	ENDIF
ENDPROC

PROC UNLOAD_MISSION_SPECIFIC_AUDIO(BOOL bFinalCleanup)
	PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - UNLOAD_MISSION_SPECIFIC_AUDIO ################################")
	INT i
	
	IF sInteractWithVars.iInteractWith_AudioLoopID > -1
		IF NOT HAS_SOUND_FINISHED(sInteractWithVars.iInteractWith_AudioLoopID)
			STOP_SOUND(sInteractWithVars.iInteractWith_AudioLoopID)
		ENDIF

		PRINTLN("MC_SCRIPT_CLEANUP - Releasing sInteractWithVars.iInteractWith_AudioLoopID")
		SCRIPT_RELEASE_SOUND_ID(sInteractWithVars.iInteractWith_AudioLoopID)
	ENDIF
	
	IF iHalloweenSoundID > -1
	AND NOT HAS_SOUND_FINISHED(iHalloweenSoundID)
		STOP_SOUND(iHalloweenSoundID)
	ENDIF
	
	STOP_SCRIPTED_CUTSCENE_LIFT_AUDIO__MOVE_LOOP()
	
	IF iSoundIDVehBeacon > -1
		STOP_SOUND(iSoundIDVehBeacon)
		PRINTLN("MC_SCRIPT_CLEANUP - Releasing iSoundIDVehBeacon")
		SCRIPT_RELEASE_SOUND_ID(iSoundIDVehBeacon)
	ENDIF
	
	IF IS_SOUND_ID_VALID(iCrashSound)
	AND NOT HAS_SOUND_FINISHED(iCrashSound)
		STOP_SOUND(iCrashSound)
	ENDIF
	
	IF IS_SOUND_ID_VALID(iBoundsTimerSound)
	AND NOT HAS_SOUND_FINISHED(iBoundsTimerSound)
		STOP_SOUND(iBoundsTimerSound)
	ENDIF
	
	IF iStartCargoBobSound != - 1
		STOP_SOUND(SoundIDCargoBob)
		IF NOT IS_STRING_NULL_OR_EMPTY(sCargoBobAudioScene)
			IF IS_AUDIO_SCENE_ACTIVE(sCargoBobAudioScene)
				STOP_AUDIO_SCENE(sCargoBobAudioScene) 
			ENDIF
		ENDIF
		PRINTLN("[CARGO_AUDIO]STOP_AUDIO_SCENE = DLC_Apartments_Drop_Zone_Helicopter_Scen")
		iStartCargoBobSound = - 1
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck14, LBOOL14_HAS_STARTED_APT_COUNTDOWN_MUSIC)
	AND NOT IS_BIT_SET(iLocalBoolCheck14, LBOOL14_HAS_KILLED_APT_COUNTDOWN_MUSIC)
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciUSE_FREEMODE_30S_COUNTDOWN)
			CANCEL_MUSIC_EVENT("FM_COUNTDOWN_30S_KILL")
		ELSE
			CANCEL_MUSIC_EVENT("APT_COUNTDOWN_30S_KILL")
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck15, LBOOL15_HAS_STARTED_VAL_COUNTDOWN_MUSIC)
	AND NOT IS_BIT_SET(iLocalBoolCheck14, LBOOL15_HAS_KILLED_VAL_COUNTDOWN_MUSIC)
		CANCEL_MUSIC_EVENT("VAL2_COUNTDOWN_30S_KILL")
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck20, LBOOL20_HAS_STARTED_IE_COUNTDOWN_MUSIC)
	AND NOT IS_BIT_SET(iLocalBoolCheck20, LBOOL20_HAS_KILLED_IE_COUNTDOWN_MUSIC)
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciUSE_IE_30S_COUNTDOWN)
			CANCEL_MUSIC_EVENT("IE_COUNTDOWN_30S_KILL")
			PRINTLN("CANCEL_MUSIC_EVENT : IE_COUNTDOWN_30S_KILL")
		ENDIF
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("MP_Reduce_Score_For_Emitters_Scene")
		STOP_AUDIO_SCENE("MP_Reduce_Score_For_Emitters_Scene")
		PRINTLN("STOP_AUDIO_SCENE =  MC_SCRIPT_CLEANUP MP_Reduce_Score_For_Emitters_Scene")
	ENDIF
	
	SET_AUDIO_FLAG("AllowScriptedSpeechInSlowMo", FALSE) 
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[0][0].iSpawnBitSet, ci_SpawnBS_AirSpawn)
		PRINTLN("[JJT] MC_SCRIPT_CLEANUP - Releasing iDZCargobobRampSoundID")
		SCRIPT_RELEASE_SOUND_ID(iDZCargobobRampSoundID)
	ENDIF
	
	//Halloween cleanup
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciHWN_BREATHING_ENABLED)
	OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciPRE_GAME_EMP)
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciHWN_BREATHING_ENABLED)
			SCRIPT_RELEASE_SOUND_ID(iHunterBreathingSoundID)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_ENABLE_ALTERNATE_LIGHTS_OFF_AUDIO)
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_CHRISTMAS2017/XM_STEALAVG")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciENABLE_GO_AIRHORN)
		PRINTLN("[RCC MISSION] RELEASE_NAMED_SCRIPT_AUDIO_BANK(DLC_HALLOWEEN/TG_01) within MC_SCRIPT_CLEANUP")
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_HALLOWEEN/TG_01")
	ENDIF
	IF IS_BIT_SET(iLocalBoolCheck26, LBOOL26_INTERIOR_AUDIO_MOUNTAINBASE)
		PRINTLN("[LM][RCC MISSION] - [Audio] - [MountainBase] - clearing mountain base Audio (MC_SCRIPT_CLEANUP)")
		SET_AMBIENT_ZONE_LIST_STATE("dlc_xm_int_base_zones", false, false)
		STOP_AUDIO_SCENE("dlc_xm_mountain_base_scene")
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck26, LBOOL26_INTERIOR_AUDIO_FACILITY_PROPERTY)
		PRINTLN("[LM][RCC MISSION] - [Audio] - [Facility Proper] - clearing Player Facility Audio Scene (MC_SCRIPT_CLEANUP)")
		STOP_AUDIO_SCENE("dlc_xm_facility_property_scene")
	ENDIF
	
	IF NOT IS_THIS_IS_A_STRAND_MISSION()
	AND ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
		TRIGGER_MUSIC_EVENT( "FM_SUDDEN_DEATH_STOP_MUSIC" )  
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFive, ciOptionsBS25_EnableHeadsetBeep)
		PRINTLN("[RCC MISSION] MANAGE_LOCAL_PLAYER_INIT - Calling SET_AUDIO_FLAG with EnableHeadsetBeep FALSE")
		SET_AUDIO_FLAG("EnableHeadsetBeep", FALSE)	
	ENDIF

	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyNine, ciOptionsBS29_DisableNPCHeadsetSpeechAttenuation)
		SET_AUDIO_FLAG("DisableNPCHeadsetSpeechAttenuation", FALSE)
		PRINTLN("[RCC MISSION] MANAGE_LOCAL_PLAYER_INIT - Calling SET_AUDIO_FLAG with DisableNPCHeadsetSpeechAttenuation FALSE")
	ENDIF
	
	//Stop any audio mixes
	STOP_ANY_CURRENTLY_PLAYING_AUDIO_MIX()
	
	IF IS_SOUND_ID_VALID(iSoundIDCountdown)
		PRINTLN("[MC_SCRIPT_CLEANUP] STOP_SOUND(iSoundIDCountdown")
		STOP_SOUND(iSoundIDCountdown)
		RELEASE_SOUND_ID(iSoundIDCountdown)
		iSoundIDCountdown = -1
	ENDIF

	IF IS_SOUND_ID_VALID(iSoundIDVehBomb)
	AND NOT HAS_SOUND_FINISHED(iSoundIDVehBomb)
		STOP_SOUND(iSoundIDVehBomb)
	ENDIF
	IF IS_SOUND_ID_VALID(iSoundIDVehBomb2)
	AND NOT HAS_SOUND_FINISHED(iSoundIDVehBomb2)
		STOP_SOUND(iSoundIDVehBomb2)
	ENDIF
	
	IF IS_SOUND_ID_VALID(iSoundIDVehBombCountdown)
	AND NOT HAS_SOUND_FINISHED(iSoundIDVehBombCountdown)
		STOP_SOUND(iSoundIDVehBombCountdown)
	ENDIF
	
	SCRIPT_RELEASE_SOUND_ID(iSoundIDVehBomb)
	PRINTLN("[MC_SCRIPT_CLEANUP] SCRIPT_RELEASE_SOUND_ID iSoundIDVehBomb")
	SCRIPT_RELEASE_SOUND_ID(iSoundIDVehBomb2)
	PRINTLN("[MC_SCRIPT_CLEANUP] SCRIPT_RELEASE_SOUND_ID iSoundIDVehBomb2")
	SCRIPT_RELEASE_SOUND_ID(iLightsOffSoundID)
	PRINTLN("[MC_SCRIPT_CLEANUP] SCRIPT_RELEASE_SOUND_ID iLightsOffSoundID")
	SCRIPT_RELEASE_SOUND_ID(iLightsBackOnSoundID)
	PRINTLN("[MC_SCRIPT_CLEANUP] SCRIPT_RELEASE_SOUND_ID iLightsBackOnSoundID")
	SCRIPT_RELEASE_SOUND_ID(SoundIDCargoBob)
	PRINTLN("[MC_SCRIPT_CLEANUP] SCRIPT_RELEASE_SOUND_ID SoundIDCargoBob")
	SCRIPT_RELEASE_SOUND_ID(iCrashSound)
	PRINTLN("[MC_SCRIPT_CLEANUP] SCRIPT_RELEASE_SOUND_ID iCrashSound")
	SCRIPT_RELEASE_SOUND_ID(iSoundIDCamBackground)
	PRINTLN("[MC_SCRIPT_CLEANUP] SCRIPT_RELEASE_SOUND_ID iSoundIDCamBackground")
	SCRIPT_RELEASE_SOUND_ID(iSoundIDCountdown)
	PRINTLN("[MC_SCRIPT_CLEANUP] SCRIPT_RELEASE_SOUND_ID iSoundIDCountdown")
	SCRIPT_RELEASE_SOUND_ID(iSoundPan)
	PRINTLN("[MC_SCRIPT_CLEANUP] SCRIPT_RELEASE_SOUND_ID iSoundPan")
	SCRIPT_RELEASE_SOUND_ID(iSoundZoom)
	PRINTLN("[MC_SCRIPT_CLEANUP] SCRIPT_RELEASE_SOUND_ID iSoundZoom")
	SCRIPT_RELEASE_SOUND_ID(iCamSound)
	PRINTLN("[MC_SCRIPT_CLEANUP] SCRIPT_RELEASE_SOUND_ID iCamSound")
	SCRIPT_RELEASE_SOUND_ID(garage_door_sound_id)
	PRINTLN("[MC_SCRIPT_CLEANUP] SCRIPT_RELEASE_SOUND_ID garage_door_sound_id")
	SCRIPT_RELEASE_SOUND_ID(iBeastAttackSoundID)
	PRINTLN("[MC_SCRIPT_CLEANUP] SCRIPT_RELEASE_SOUND_ID iBeastAttackSoundID")
	SCRIPT_RELEASE_SOUND_ID(iBeastSprintSoundID)
	PRINTLN("[MC_SCRIPT_CLEANUP] SCRIPT_RELEASE_SOUND_ID iBeastSprintSoundID")
	SCRIPT_RELEASE_SOUND_ID(iThermalSoundID)
	PRINTLN("[MC_SCRIPT_CLEANUP] SCRIPT_RELEASE_SOUND_ID iThermalSoundID")
	SCRIPT_RELEASE_SOUND_ID(iNightVisSoundID)
	PRINTLN("[MC_SCRIPT_CLEANUP] SCRIPT_RELEASE_SOUND_ID iNightVisSoundID")
	SCRIPT_RELEASE_SOUND_ID(iBoundsTimerSound)
	PRINTLN("[MC_SCRIPT_CLEANUP] SCRIPT_RELEASE_SOUND_ID iBoundsTimerSound")
	SCRIPT_RELEASE_SOUND_ID(iSoundIDLastAlive)
	PRINTLN("[MC_SCRIPT_CLEANUP] SCRIPT_RELEASE_SOUND_ID iSoundIDLastAlive")
	SCRIPT_RELEASE_SOUND_ID(iTrailSound0)
	PRINTLN("[MC_SCRIPT_CLEANUP] SCRIPT_RELEASE_SOUND_ID iTrailSound0")
	SCRIPT_RELEASE_SOUND_ID(iTrailSound1)
	PRINTLN("[MC_SCRIPT_CLEANUP] SCRIPT_RELEASE_SOUND_ID iTrailSound1")
	SCRIPT_RELEASE_SOUND_ID(iTrailSound2)
	PRINTLN("[MC_SCRIPT_CLEANUP] SCRIPT_RELEASE_SOUND_ID iTrailSound2")
	SCRIPT_RELEASE_SOUND_ID(iTrailSound3)
	PRINTLN("[MC_SCRIPT_CLEANUP] SCRIPT_RELEASE_SOUND_ID iTrailSound3")
	SCRIPT_RELEASE_SOUND_ID(iStealthSoundID)
	PRINTLN("[MC_SCRIPT_CLEANUP] SCRIPT_RELEASE_SOUND_ID iStealthSoundID")	

	IF bFinalCleanup
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciTURN_OFF_PRISON_YARD_AMB)
			SET_AMBIENT_ZONE_STATE("AZ_COUNTRYSIDE_PRISON_01_ANNOUNCER_ALARM",TRUE,TRUE)
			SET_AMBIENT_ZONE_STATE("AZ_COUNTRYSIDE_PRISON_01_ANNOUNCER_WARNING",TRUE,TRUE)
			SET_AMBIENT_ZONE_STATE("AZ_COUNTRYSIDE_PRISON_01_ANNOUNCER_GENERAL",TRUE,TRUE)
			PRINTLN("TURNING BACK ON PRISON AMBIENCE")
		ENDIF
		
		STRING sBank
		INT iAlarm
		FOR iAlarm = 0 TO ciALARM_SOUND_MAX-1
			sBank = GET_FMMC_ALARM_BANK_FROM_SELECTION(iAlarm)
			
			IF NOT IS_STRING_NULL_OR_EMPTY(sBank)
				PRINTLN("MC_SCRIPT_CLEANUP - RELEASE_NAMED_SCRIPT_AUDIO_BANK - sBank: ", sBank)
				RELEASE_NAMED_SCRIPT_AUDIO_BANK(sBank)
			ENDIF
		ENDFOR
	ENDIF
	
	IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
		IF NOT g_bCelebrationScreenIsActive
		AND NOT IS_THIS_A_VERSUS_MISSION()
			MC_SCRIPT_TRIGGER_MUSIC_EVENT_STOP()
		ENDIF
	ENDIF
	
	STOP_AUDIO_SCENE("MP_POSITIONED_RADIO_MUTE_SCENE")
	
	STOP_AUDIO_SCENE("MP_CELEB_SCREEN_SCENE")
	
	IF IS_AUDIO_SCENE_ACTIVE("MP_LEADERBOARD_SCENE")
		STOP_AUDIO_SCENE("MP_LEADERBOARD_SCENE")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("MP_LEADERBOARD_PP_SCENE")
		STOP_AUDIO_SCENE("MP_LEADERBOARD_PP_SCENE")
	ENDIF
	SET_AUDIO_FLAG("DisableFlightMusic", FALSE) 
	SET_AUDIO_FLAG("WantedMusicDisabled", FALSE)
	SET_AUDIO_FLAG("AllowScoreAndRadio", TRUE)
	SET_AUDIO_FLAG("IsPlayerOnMissionForSpeech",FALSE)
	REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(LocalPlayerPed, 0.0)
	SET_PED_INTERIOR_WALLA_DENSITY(1,0)
	
	IF NOT g_bCelebrationScreenIsActive
		TRIGGER_MUSIC_EVENT("MP_GLOBAL_RADIO_FADE_IN")
	ENDIF
	
	STOP_LBD_AUDIO_SCENE()
	TOGGLE_STRIPPER_AUDIO(TRUE)
	
	STOP_STREAM()
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciMUTE_AMBIENT_CITY_SOUNDS)
		IF IS_AUDIO_SCENE_ACTIVE("GTAO_Mute_Traffic_Ambience_Scene")
			STOP_AUDIO_SCENE("GTAO_Mute_Traffic_Ambience_Scene")
			PRINTLN("[LM] - MC_SCRIPT_CLEANUP - STOP_AUDIO_SCENE: GTAO_Mute_Traffic_Ambience_Scene")
		ENDIF
	ENDIF
	
	RELEASE_MISSION_AUDIO_BANK()
	RELEASE_NAMED_SCRIPT_AUDIO_BANK("SCRIPT\\LIFTS")
	
	PRINTLN("iOutAreaSound = ", iOutAreaSound)
	//stop penned in countdown sounds
	IF IS_SOUND_ID_VALID(iOutAreaSound)
		IF NOT HAS_SOUND_FINISHED(iOutAreaSound)
			STOP_SOUND(iOutAreaSound)
		ENDIF
	ENDIF
	
	SCRIPT_RELEASE_SOUND_ID(iOutAreaSound)
	
	IF iDoorAlarmSoundID != -1
		STOP_SOUND(iDoorAlarmSoundID)
		SCRIPT_RELEASE_SOUND_ID(iDoorAlarmSoundID)
	ENDIF
	
	IF iFakeAmmoRechargeSoundID > -1
		STOP_SOUND(iFakeAmmoRechargeSoundID)
		RELEASE_SOUND_ID(iFakeAmmoRechargeSoundID)
		iFakeAmmoRechargeSoundID = -1
	ENDIF
	
	IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
		FOR i = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfProps-1)
		    IF isoundid[i] <> -1
				PRINTLN("[RCC MISSION] [ALARM] STOP_SOUND(isoundid[", i, "]")
	            STOP_SOUND(isoundid[i])
	        ENDIF
		ENDFOR
		
		// turn off the casino alarm zones
		PRINTLN("[JS][ALARM] - UNLOAD_MISSION_SPECIFIC_AUDIO - Clearing Casino Alarm Zones")
		SET_AMBIENT_ZONE_STATE_PERSISTENT("AZ_H3_Casino_Alarm_Zone_01_Exterior", FALSE, TRUE)
		SET_AMBIENT_ZONE_STATE_PERSISTENT("AZ_H3_Casino_Alarm_Zone_02_Interior", FALSE, TRUE)
	ENDIF
		
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_CCTVAlertSpottedSounds)
		INT iSnd = 0
		FOR iSnd = 0 TO MAX_NUM_CCTV_CAM-1
			IF iSoundAlarmCCTV[iSnd] > -1
				SCRIPT_RELEASE_SOUND_ID(iSoundAlarmCCTV[iSnd])
			ENDIF
		ENDFOR
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("SCRIPT/ALARM_BELL_01") 
		PRINTLN("[RCC MISSION][MC_SCRIPT_CLEANUP] Releasing SCRIPT/ALARM_BELL_01")
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_UseCasinoCCTVAudio)
			RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_VINEWOOD/VW_CASINO_FINALE") 
			PRINTLN("[RCC MISSION][MC_SCRIPT_CLEANUP] Releasing DLC_VINEWOOD/VW_CASINO_FINALE")	
		ELSE	
			RELEASE_NAMED_SCRIPT_AUDIO_BANK("SCRIPT/ALARM_KLAXON_04") 
			PRINTLN("[RCC MISSION][MC_SCRIPT_CLEANUP] Releasing SCRIPT/ALARM_KLAXON_04")
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFive, ciOptionsBS25_AmbientCasinoFloorZone)
		PRINTLN("MC_SCRIPT_CLEANUP | Turning off casino floor ambient zone")
		SET_AMBIENT_ZONE_STATE("IZ_ch_dlc_casino_heist_rm_GamingFloor_01", FALSE, TRUE)
		SET_AMBIENT_ZONE_STATE("IZ_ch_dlc_casino_heist_rm_GamingFloor_02", FALSE, TRUE)
	ENDIF	
	
	IF GET_YACHT_MISSION_FROM_ROOT_CONTENT_ID(g_FMMC_STRUCT.iRootContentIDHash) = eYACHT_MISSION_FIVE__PLAINSAILING
		IF IS_AUDIO_SCENE_ACTIVE("dlc_sum20_yacht_missions_las_scene")			
			PRINTLN("STOP_AUDIO_SCENE(dlc_sum20_yacht_missions_las_scene)")
			STOP_AUDIO_SCENE("dlc_sum20_yacht_missions_las_scene")
		ENDIF
	ENDIF	
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions, IPL_ISLAND)
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_HEI4/DLCHEI4_GENERIC_01") 
		
		INT iIslandAirDefenceTurret
		FOR iIslandAirDefenceTurret = 0 TO ciIslandAirDefenceTurrets - 1
			IF iIslandAirDefenceTurretAudioID[iIslandAirDefenceTurret] > -1
				STOP_SOUND(iIslandAirDefenceTurretAudioID[iIslandAirDefenceTurret])
				RELEASE_SOUND_ID(iIslandAirDefenceTurretAudioID[iIslandAirDefenceTurret])
				iIslandAirDefenceTurretAudioID[iIslandAirDefenceTurret] = -1
			ENDIF
		ENDFOR
	ENDIF
	
	CLEANUP_WORLD_SOUND(1)
	CLEANUP_WORLD_SOUND(2)
	CLEANUP_WORLD_SOUND(3)
	
	PROCESS_EMITTER_CLEANUP()
	
	IF bFinalCleanup
		IF IS_AUDIO_SCENE_ACTIVE("DLC_H4_Island_Finale_Stealth_Scene")
			STOP_AUDIO_SCENE("DLC_H4_Island_Finale_Stealth_Scene")
			PRINTLN("[RCC MISSION][MC_SCRIPT_CLEANUP] STOP_AUDIO_SCENE: DLC_H4_Island_Finale_Stealth_Scene")			
		ENDIF	
		
		IF IS_AUDIO_SCENE_ACTIVE("DLC_H4_Island_Finale_Action_Scene")
			STOP_AUDIO_SCENE("DLC_H4_Island_Finale_Action_Scene")
			PRINTLN("[RCC MISSION][MC_SCRIPT_CLEANUP] STOP_AUDIO_SCENE: DLC_H4_Island_Finale_Action_Scene")
		ENDIF	
		
		IF IS_AUDIO_SCENE_ACTIVE("DLC_Fixer_DL3_Hood_Pass_Scene")
			STOP_AUDIO_SCENE("DLC_Fixer_DL3_Hood_Pass_Scene")
			PRINTLN("[RCC MISSION][MC_SCRIPT_CLEANUP] STOP_AUDIO_SCENE: DLC_Fixer_DL3_Hood_Pass_Scene")
		ENDIF	
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("TAIL_HELICOPTER_SCENE")
		PRINTLN("[RCC MISSION][MC_SCRIPT_CLEANUP] STOP_AUDIO_SCENE: TAIL_HELICOPTER_SCENE")
		STOP_AUDIO_SCENE("TAIL_HELICOPTER_SCENE")
	ENDIF	
	
	IF IS_AUDIO_SCENE_ACTIVE("DLC_FIXER_Short_Trip_01_Back_Of_Truck_Scene")
		PRINTLN("[RCC MISSION][MC_SCRIPT_CLEANUP] STOP_AUDIO_SCENE: DLC_FIXER_Short_Trip_01_Back_Of_Truck_Scene")
		STOP_AUDIO_SCENE("DLC_FIXER_Short_Trip_01_Back_Of_Truck_Scene")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("DLC_Fixer_DL5_Music_Moment_Scene")
		PRINTLN("[RCC MISSION][MC_SCRIPT_CLEANUP][AUDIO] STOP_AUDIO_SCENE: DLC_Fixer_DL5_Music_Moment_Scene")
		STOP_AUDIO_SCENE("DLC_Fixer_DL5_Music_Moment_Scene")
	ENDIF

	UNBLOCK_SPEECH_CONTEXT_GROUP("BLOCKED_CONTEXTS_MISSION_CAR_CRASH")
	
	CLEANUP_SILO_INTERIOR_ULP_AMBIENT_AUDIO()
	
	CLEANUP_FAKE_FLIGHT_LOOP_SOUND()
	
	UNLOAD_CUSTOM_SYNC_SCENE_AUDIO_SCENE(sCutsceneSyncSceneDataGameplay)
	
	IF IS_THIS_ROOT_CONTENT_ID_A_ULP_MISSION(g_FMMC_STRUCT.iRootContentIDHash)
		PRINTLN("MC_SCRIPT_CLEANUP - Calling SET_AMBIENT_ZONE_STATE with TRUE on AZ_LOS_SANTOS_CONSTRUCTION_SITE_02 and AZ_LOS_SANTOS_CONSTRUCTION_SITE_02_HIGH")
		SET_AMBIENT_ZONE_STATE("AZ_LOS_SANTOS_CONSTRUCTION_SITE_02", TRUE, TRUE)
		SET_AMBIENT_ZONE_STATE("AZ_LOS_SANTOS_CONSTRUCTION_SITE_02_HIGH", TRUE, TRUE)
	ENDIF
	
ENDPROC

///PURPOSE: Requests the drilling audio banks for non-drilling players so they can hear the drilling sound effects.
PROC MANAGE_DRILLING_AUDIO_REQUESTS()
	IF IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_HEIST_DRILL_ASSET_REQUEST)
		IF NOT IS_BIT_SET(iLocalBoolCheck8, LBOOL8_HEIST_DRILL_ASSET_LOADED)
			REQUEST_ANIM_DICT("anim@heists@fleeca_bank@drilling")
			REQUEST_MODEL(hei_prop_heist_drill)
			REQUEST_MODEL(MC_playerBD_1[iLocalPart].mnHeistGearBag)
		
			IF REQUEST_SCRIPT_AUDIO_BANK("DLC_MPHEIST\\HEIST_FLEECA_DRILL")
			AND REQUEST_SCRIPT_AUDIO_BANK("DLC_MPHEIST\\HEIST_FLEECA_DRILL_2")
			AND HAS_ANIM_DICT_LOADED("anim@heists@fleeca_bank@drilling")
			AND HAS_MODEL_LOADED(hei_prop_heist_drill)
			AND HAS_MODEL_LOADED(MC_playerBD_1[iLocalPart].mnHeistGearBag)
				SET_BIT(iLocalBoolCheck8, LBOOL8_HEIST_DRILL_ASSET_LOADED)
			ENDIF
		ENDIF
	ELSE
		IF IS_BIT_SET(iLocalBoolCheck8, LBOOL8_HEIST_DRILL_ASSET_LOADED)
			RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_MPHEIST\\HEIST_FLEECA_DRILL")
			RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_MPHEIST\\HEIST_FLEECA_DRILL_2")
			REMOVE_ANIM_DICT("anim@heists@fleeca_bank@drilling")
			SET_MODEL_AS_NO_LONGER_NEEDED(hei_prop_heist_drill)
			SET_MODEL_AS_NO_LONGER_NEEDED(MC_playerBD_1[iLocalPart].mnHeistGearBag)
			
			CLEAR_BIT(iLocalBoolCheck8, LBOOL8_HEIST_DRILL_ASSET_LOADED)
		ENDIF
	ENDIF
ENDPROC

PROC LOAD_BEAST_SFX()

	IF NOT IS_BIT_SET(iLocalBoolCheck14, LBOOL14_BEAST_SFX_LOADED)
		IF REQUEST_SCRIPT_AUDIO_BANK("DLC_APARTMENT/APT_BEAST")
			PRINTLN("[JS] REQUEST_SCRIPT_AUDIO_BANK(\"DLC_APARTMENT/APT_BEAST\") loaded successfully.")
			SET_BIT(iLocalBoolCheck14, LBOOL14_BEAST_SFX_LOADED)
		ELSE
			PRINTLN("[JS] REQUEST_SCRIPT_AUDIO_BANK(\"DLC_APARTMENT/APT_BEAST\") currently loading.")
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL LOAD_ALL_CONTENT_SPECIFIC_AUDIO_BANKS()
	
	BOOL bAllLoaded = TRUE
		
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciENABLE_VEHICULAR_WARFARE_SOUNDS)
		IF REQUEST_SCRIPT_AUDIO_BANK("DLC_SMUGGLER/SM_VEHWA_01")
			PRINTLN("[JS] LOAD_ALL_CONTENT_SPECIFIC_AUDIO_BANKS - REQUEST_SCRIPT_AUDIO_BANK(\"DLC_SMUGGLER/SM_VEHWA_01\") loaded successfully.")
		ELSE
			PRINTLN("[JS] LOAD_ALL_CONTENT_SPECIFIC_AUDIO_BANKS - REQUEST_SCRIPT_AUDIO_BANK(\"DLC_SMUGGLER/SM_VEHWA_01\") currently loading.")
			bAllLoaded = FALSE
		ENDIF
		IF REQUEST_SCRIPT_AUDIO_BANK("DLC_SMUGGLER/SM_VEHWA_02")
			PRINTLN("[JS] LOAD_ALL_CONTENT_SPECIFIC_AUDIO_BANKS - REQUEST_SCRIPT_AUDIO_BANK(\"DLC_SMUGGLER/SM_VEHWA_2\") loaded successfully.")
			IF NOT IS_AUDIO_SCENE_ACTIVE("DLC_SM_VEHWA_General_Scene")
				PRINTLN("LOAD_ALL_CONTENT_SPECIFIC_AUDIO_BANKS - START_AUDIO_SCENE(DLC_SM_VEHWA_General_Scene)")
				START_AUDIO_SCENE("DLC_SM_VEHWA_General_Scene")
			ENDIF
		ELSE
			PRINTLN("[JS] LOAD_ALL_CONTENT_SPECIFIC_AUDIO_BANKS - REQUEST_SCRIPT_AUDIO_BANK(\"DLC_SMUGGLER/SM_VEHWA_02\") currently loading.")
			bAllLoaded = FALSE
		ENDIF		
	ENDIF
	
	IF GET_YACHT_MISSION_FROM_ROOT_CONTENT_ID(g_FMMC_STRUCT.iRootContentIDHash) = eYACHT_MISSION_FIVE__PLAINSAILING
		IF NOT IS_AUDIO_SCENE_ACTIVE("dlc_sum20_yacht_missions_las_scene")
			PRINTLN("LOAD_ALL_CONTENT_SPECIFIC_AUDIO_BANKS - Audio Scene has not loaded; dlc_sum20_yacht_missions_las_scene")
			bAllLoaded = FALSE
			PRINTLN("LOAD_ALL_CONTENT_SPECIFIC_AUDIO_BANKS - START_AUDIO_SCENE(dlc_sum20_yacht_missions_las_scene)")
			START_AUDIO_SCENE("dlc_sum20_yacht_missions_las_scene")
		ENDIF
	ENDIF
	
	IF GET_YACHT_MISSION_FROM_ROOT_CONTENT_ID(g_FMMC_STRUCT.iRootContentIDHash) = eYACHT_MISSION_FIVE__PLAINSAILING
		IF NOT IS_AUDIO_SCENE_ACTIVE("dlc_sum20_yacht_missions_las_scene")
			PRINTLN("LOAD_ALL_CONTENT_SPECIFIC_AUDIO_BANKS - Audio Scene has not loaded; dlc_sum20_yacht_missions_las_scene")
			bAllLoaded = FALSE
			PRINTLN("LOAD_ALL_CONTENT_SPECIFIC_AUDIO_BANKS - START_AUDIO_SCENE(dlc_sum20_yacht_missions_las_scene)")
			START_AUDIO_SCENE("dlc_sum20_yacht_missions_las_scene")
		ENDIF
	ENDIF
		
	IF IS_THIS_MISSION_A_TUNER_ROBBERY_FINALE(g_FMMC_STRUCT.iRootContentIDHash)
		IF REQUEST_SCRIPT_AUDIO_BANK("DLC_TUNER/DLC_Tuner_Generic")
			PRINTLN("LOAD_ALL_CONTENT_SPECIFIC_AUDIO_BANKS - REQUEST_SCRIPT_AUDIO_BANK(\"DLC_TUNER/DLC_Tuner_Generic\" loaded successfully.")
		ELSE
			PRINTLN("LOAD_ALL_CONTENT_SPECIFIC_AUDIO_BANKS - REQUEST_SCRIPT_AUDIO_BANK(\"DLC_TUNER/DLC_Tuner_Generic\") currently loading.")
			bAllLoaded = FALSE
		ENDIF					
	ENDIF	
	
	IF IS_CURRENT_MISSION_THIS_TUNER_ROBBERY_FINALE(TR_UNION_DEPOSITORY)
		IF REQUEST_SCRIPT_AUDIO_BANK("DLC_HEI4/DLCHEI4_GENERIC_01")
			PRINTLN("LOAD_ALL_CONTENT_SPECIFIC_AUDIO_BANKS - REQUEST_SCRIPT_AUDIO_BANK(\"DLC_HEI4/DLCHEI4_GENERIC_01c\" loaded successfully.")
		ELSE
			PRINTLN("LOAD_ALL_CONTENT_SPECIFIC_AUDIO_BANKS - REQUEST_SCRIPT_AUDIO_BANK(\"DLC_HEI4/DLCHEI4_GENERIC_01\") currently loading.")
			bAllLoaded = FALSE
		ENDIF					
	ENDIF	
	
	IF GET_FIXER_STORY_MISSION_FROM_ROOT_CONTENT_ID(g_FMMC_STRUCT.iRootContentIDHash) = FSM_FIRE_BOOTH_MISSION // "Fire in the Booth"
		IF REQUEST_SCRIPT_AUDIO_BANK("DLC_SECURITY/DLC_Sec_Studio")
			PRINTLN("LOAD_ALL_CONTENT_SPECIFIC_AUDIO_BANKS - REQUEST_SCRIPT_AUDIO_BANK(\"DLC_SECURITY/DLC_Sec_Studio\" loaded successfully.")
		ELSE
			PRINTLN("LOAD_ALL_CONTENT_SPECIFIC_AUDIO_BANKS - REQUEST_SCRIPT_AUDIO_BANK(\"DLC_SECURITY/DLC_Sec_Studio\") currently loading.")
			bAllLoaded = FALSE
		ENDIF
	ELIF GET_FIXER_STORY_MISSION_FROM_ROOT_CONTENT_ID(g_FMMC_STRUCT.iRootContentIDHash) = FSM_PARTY_PROMOTER_MISSION
		IF REQUEST_SCRIPT_AUDIO_BANK("DLC_SECURITY/Dlc_Sec_Promoter_party_music")
			PRINTLN("LOAD_ALL_CONTENT_SPECIFIC_AUDIO_BANKS - REQUEST_SCRIPT_AUDIO_BANK(\"DLC_SECURITY/Dlc_Sec_Promoter_party_music\" loaded successfully.")
		ELSE
			PRINTLN("LOAD_ALL_CONTENT_SPECIFIC_AUDIO_BANKS - REQUEST_SCRIPT_AUDIO_BANK(\"DLC_SECURITY/Dlc_Sec_Promoter_party_music\") currently loading.")
			bAllLoaded = FALSE
		ENDIF
	ENDIF
	
	IF IS_THIS_A_FIXER_SHORT_TRIP()
		IF REQUEST_SCRIPT_AUDIO_BANK("DLC_SECURITY/DLC_Sec_Short_Trips")
			PRINTLN("LOAD_ALL_CONTENT_SPECIFIC_AUDIO_BANKS - REQUEST_SCRIPT_AUDIO_BANK(\"DLC_SECURITY/DLC_Sec_Short_Trips\" loaded successfully.")
		ELSE
			PRINTLN("LOAD_ALL_CONTENT_SPECIFIC_AUDIO_BANKS - REQUEST_SCRIPT_AUDIO_BANK(\"DLC_SECURITY/DLC_Sec_Short_Trips\") currently loading.")
			bAllLoaded = FALSE
		ENDIF
	ENDIF
	
	IF IS_THIS_A_FIXER_STORY_MISSION()
		IF REQUEST_SCRIPT_AUDIO_BANK("DLC_SECURITY/DLC_Sec_Generic")
			PRINTLN("LOAD_ALL_CONTENT_SPECIFIC_AUDIO_BANKS - REQUEST_SCRIPT_AUDIO_BANK(\"DLC_SECURITY/DLC_Sec_Generic\" loaded successfully.")
		ELSE
			PRINTLN("LOAD_ALL_CONTENT_SPECIFIC_AUDIO_BANKS - REQUEST_SCRIPT_AUDIO_BANK(\"DLC_SECURITY/DLC_Sec_Generic\") currently loading.")
			bAllLoaded = FALSE
		ENDIF
	ENDIF
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_SIEGE_MENTALITY_FL(g_FMMC_STRUCT.iAdversaryModeType)
	AND NOT IS_AUDIO_SCENE_ACTIVE("DLC_Security_Double_Down_Scene")
		PRINTLN("LOAD_ALL_CONTENT_SPECIFIC_AUDIO_BANKS - Audio Scene has not loaded; DLC_Security_Double_Down_Scene")
		bAllLoaded = FALSE
		PRINTLN("LOAD_ALL_CONTENT_SPECIFIC_AUDIO_BANKS - START_AUDIO_SCENE(DLC_Security_Double_Down_Scene)")
		START_AUDIO_SCENE("DLC_Security_Double_Down_Scene")
	ENDIF
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_HALLOWEEN_ADVERSARY_2022(g_FMMC_STRUCT.iAdversaryModeType)
		IF REQUEST_SCRIPT_AUDIO_BANK("DLC_MPSUM2/Halloween_Adversary")
			PRINTLN("LOAD_ALL_CONTENT_SPECIFIC_AUDIO_BANKS - REQUEST_SCRIPT_AUDIO_BANK(\"DLC_MPSUM2/Halloween_Adversary\" loaded successfully.")
		ELSE
			PRINTLN("LOAD_ALL_CONTENT_SPECIFIC_AUDIO_BANKS - REQUEST_SCRIPT_AUDIO_BANK(\"DLC_MPSUM2/Halloween_Adversary\") currently loading.")
			bAllLoaded = FALSE
		ENDIF
	ENDIF

	IF IS_THIS_ROOT_CONTENT_ID_A_ULP_MISSION(g_FMMC_STRUCT.iRootContentIDHash)
		IF REQUEST_SCRIPT_AUDIO_BANK("DLC_MPSUM2/MPSUM2_Generic")
			PRINTLN("LOAD_ALL_CONTENT_SPECIFIC_AUDIO_BANKS - REQUEST_SCRIPT_AUDIO_BANK(\"DLC_MPSUM2/MPSUM2_Generic\" loaded successfully.")				
			PRINTLN("LOAD_ALL_CONTENT_SPECIFIC_AUDIO_BANKS - Calling SET_AMBIENT_ZONE_STATE with FALSE on AZ_LOS_SANTOS_CONSTRUCTION_SITE_02 and AZ_LOS_SANTOS_CONSTRUCTION_SITE_02_HIGH")
			IF GET_ULP_MISSION_FROM_ROOT_CONTENT_ID_HASH(g_FMMC_STRUCT.iRootContentIDHash) = ciULP_MISSION_5_OPERATION_PAPER_TRAIL
				SET_AMBIENT_ZONE_STATE("AZ_LOS_SANTOS_CONSTRUCTION_SITE_02", FALSE, TRUE)
				SET_AMBIENT_ZONE_STATE("AZ_LOS_SANTOS_CONSTRUCTION_SITE_02_HIGH", FALSE, TRUE)
			ENDIF
		ELSE
			PRINTLN("LOAD_ALL_CONTENT_SPECIFIC_AUDIO_BANKS - REQUEST_SCRIPT_AUDIO_BANK(\"DLC_MPSUM2/MPSUM2_Generic\") currently loading.")
			bAllLoaded = FALSE
		ENDIF
	ENDIF	
	
	RETURN bAllLoaded
ENDFUNC

FUNC BOOL LOAD_ALL_AUDIO_BANKS()
	
	BOOL bAllLoaded = TRUE
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciHWN_BREATHING_ENABLED)
	OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciHWN_HEARTBEAT_VIBRATE_ENABLED)
		IF REQUEST_SCRIPT_AUDIO_BANK("DLC_HALLOWEEN/FVJ_01")
			PRINTLN("[JJT] REQUEST_SCRIPT_AUDIO_BANK(DLC_HALLOWEEN/FVJ_01) loaded successfully.")
		ELSE
			PRINTLN("[JJT] REQUEST_SCRIPT_AUDIO_BANK(DLC_HALLOWEEN/FVJ_01) currently loading.")
			#IF IS_DEBUG_BUILD
				PRINTLN("[JJT] Requesting due to: ",
					  "ciHWN_BREATHING_ENABLED = ", IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciHWN_BREATHING_ENABLED),
					  ", ciHWN_HEARTBEAT_VIBRATE_ENABLED = ", IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciHWN_HEARTBEAT_VIBRATE_ENABLED))
			#ENDIF
			
			bAllLoaded = FALSE
		ENDIF
	ENDIF
		
	IF (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFIve, ciBEAST_SFX)
	OR g_FMMC_STRUCT.iRATCBeastTimer > 0
	OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_TRANSFORM_ASSETS))
		IF REQUEST_SCRIPT_AUDIO_BANK("DLC_APARTMENT/APT_BEAST")
			PRINTLN("[JS] REQUEST_SCRIPT_AUDIO_BANK(\"DLC_APARTMENT/APT_BEAST\") loaded successfully.")
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFIve, ciBEAST_SFX)
				IF NOT IS_AUDIO_SCENE_ACTIVE("GTAO_BvS_Gameplay_Scene")
					START_AUDIO_SCENE("GTAO_BvS_Gameplay_Scene")
				ENDIF
				SET_AMBIENT_ZONE_STATE("AZ_COUNTRY_SAWMILL",FALSE,TRUE)
				SET_AMBIENT_ZONE_STATE("AZ_SAWMILL_CONVEYOR_01",FALSE,TRUE)
				SET_STATIC_EMITTER_ENABLED("SE_COUNTRY_SAWMILL_MAIN_BUILDING", FALSE)
			ENDIF
		ELSE
			PRINTLN("[JS] REQUEST_SCRIPT_AUDIO_BANK(\"DLC_APARTMENT/APT_BEAST\") currently loading.")
			bAllLoaded = FALSE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciENABLE_GO_AIRHORN)
		IF REQUEST_SCRIPT_AUDIO_BANK("DLC_HALLOWEEN/TG_01")
			PRINTLN("[JJT] REQUEST_SCRIPT_AUDIO_BANK(DLC_HALLOWEEN/TG_01) loaded successfully.")
		ELSE
			PRINTLN("[JJT] REQUEST_SCRIPT_AUDIO_BANK(DLC_HALLOWEEN/TG_01) currently loading.")
			bAllLoaded = FALSE
		ENDIF
	ENDIF

	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_TRANSFORM_ASSETS)
		
		IF REQUEST_SCRIPT_AUDIO_BANK("DLC_IMPORTEXPORT/IE_JN_01")
			PRINTLN("[JS] REQUEST_SCRIPT_AUDIO_BANK(DLC_IMPORTEXPORT/IE_JN_01) loaded successfully.")
		ELSE
			PRINTLN("[JS] REQUEST_SCRIPT_AUDIO_BANK(DLC_IMPORTEXPORT/IE_JN_01) currently loading.")
			bAllLoaded = FALSE
		ENDIF
		
		IF REQUEST_SCRIPT_AUDIO_BANK("DLC_IMPORTEXPORT/IE_JN_02")
			PRINTLN("[JS] REQUEST_SCRIPT_AUDIO_BANK(DLC_IMPORTEXPORT/IE_JN_02) loaded successfully.")
		ELSE
			PRINTLN("[JS] REQUEST_SCRIPT_AUDIO_BANK(DLC_IMPORTEXPORT/IE_JN_02) currently loading.")
			bAllLoaded = FALSE
		ENDIF
		
		IF REQUEST_SCRIPT_AUDIO_BANK("DLC_IMPORTEXPORT/IE_JN_03")
			PRINTLN("[JS] REQUEST_SCRIPT_AUDIO_BANK(DLC_IMPORTEXPORT/IE_JN_03) loaded successfully.")
		ELSE
			PRINTLN("[JS] REQUEST_SCRIPT_AUDIO_BANK(DLC_IMPORTEXPORT/IE_JN_03) currently loading.")
			bAllLoaded = FALSE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_UseCasinoCCTVAudio)
		IF REQUEST_SCRIPT_AUDIO_BANK("DLC_VINEWOOD/VW_CASINO_FINALE")
			PRINTLN("[ML] REQUEST_SCRIPT_AUDIO_BANK(\"DLC_VINEWOOD/VW_CASINO_FINALE.\" loaded successfully.")
		ELSE
			PRINTLN("[ML] REQUEST_SCRIPT_AUDIO_BANK(\"DLC_VINEWOOD/VW_CASINO_FINALE\") currently loading.")
			bAllLoaded = FALSE
		ENDIF					
	ENDIF	
		
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySeven, ciOptionsBS27_WorldAlarmAssetIsland)
		IF NOT IS_AUDIO_SCENE_ACTIVE("DLC_H4_Island_Finale_Action_Scene")
			IF NOT IS_AUDIO_SCENE_ACTIVE("DLC_H4_Island_Finale_Stealth_Scene")
				PRINTLN("START_AUDIO_SCENE(DLC_H4_Island_Finale_Stealth_Scene)")
				START_AUDIO_SCENE("DLC_H4_Island_Finale_Stealth_Scene")
				bAllLoaded = FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT LOAD_ALL_CONTENT_SPECIFIC_AUDIO_BANKS()
		bAllLoaded = FALSE	
	ENDIF
	
	RETURN bAllLoaded
ENDFUNC

FUNC STRING GET_HELI_LOW_HEALTH_SOUND_NAME(INT iVeh)

	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetTen, ciFMMC_VEHICLE10_UseULPMissionExplosionSound)
		RETURN "Helicopter_Damaged"
	ENDIF
	
	RETURN ""
	
ENDFUNC

FUNC STRING GET_HELI_LOW_HEALTH_SOUND_SET(INT iVeh)

	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetTen, ciFMMC_VEHICLE10_UseULPMissionExplosionSound)
		RETURN "DLC_MPSum2_ULP5_Deal_Breaker_Sounds"
	ENDIF
	
	RETURN ""
	
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Drone UI
// ##### Description: 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC SCALEFORM_SET_ZOOM(INT iZoom)
	BEGIN_SCALEFORM_MOVIE_METHOD(sfDroneCutscene, "SET_ZOOM")
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iZoom)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC SCALEFORM_SET_ELEMENT_VISIBLE(STRING sMethodName, BOOL bVisible)
	BEGIN_SCALEFORM_MOVIE_METHOD(sfDroneCutscene, sMethodName)
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bVisible)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC SCALEFORM_SET_ZOOM_LEVEL(INT iZoomIndex, STRING sTextLabel)
	BEGIN_SCALEFORM_MOVIE_METHOD(sfDroneCutscene, "SET_ZOOM_LABEL")
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iZoomIndex)
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sTextLabel)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC MANAGE_DRONE_SCALEFORM_ZOOM_LEVEL(INT iZoomLevel)
	SCALEFORM_SET_ZOOM(iZoomLevel)
ENDPROC 

PROC SCALEFORM_SET_HEADING(INT iHeadingAngle)
	BEGIN_SCALEFORM_MOVIE_METHOD(sfDroneCutscene, "SET_HEADING")
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iHeadingAngle)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

FUNC BOOL LOAD_FAKE_DRONE_SCALEFORM()

	IF NOT HAS_SCALEFORM_MOVIE_LOADED(sfDroneCutscene)
		sfDroneCutscene = REQUEST_SCALEFORM_MOVIE("DRONE_CAM")
		PRINTLN("[Cutscenes][Drone] - Requesting Scaleform Moving 'DRONE_CAM'")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

PROC PRELOAD_FAKE_DRONE_SCALEFORM(INT iCutsceneToUse)
	
	INT iShot
	FOR iShot = 0 TO g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iNumCamShots-1
		IF g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].eDroneUIPreset[iShot] != MC_DRONE_UI_PRESET_NONE
			LOAD_FAKE_DRONE_SCALEFORM()
		ENDIF
	ENDFOR
	
ENDPROC

PROC CLEANUP_FAKE_DRONE_SCALEFORM()
	
	IF HAS_SCALEFORM_MOVIE_LOADED(sfDroneCutscene)
		PRINTLN("[Cutscenes][Drone] - Setting Scaleform Moving 'DRONE_CAM' as no longer needed.")
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sfDroneCutscene)
		DISABLE_SPECTATOR_FILTER_OPTION(FALSE)
	ENDIF
		
	IF IS_AUDIO_SCENE_ACTIVE("ULP5_Deal_Breaker_Drone_View_Scene")
		PRINTLN("[Cutscenes][Drone] - Calling STOP_AUDIO_SCENE with ULP5_Deal_Breaker_Drone_View_Scene")
		STOP_AUDIO_SCENE("ULP5_Deal_Breaker_Drone_View_Scene")
	ENDIF
	
ENDPROC

PROC PLAY_FAKE_FLIGHT_LOOP_SOUND(VECTOR vPos)
	
	IF iDroneCutsceneSoundLoop = -1
	OR HAS_SOUND_FINISHED(iDroneCutsceneSoundLoop)
		iDroneCutsceneSoundLoop = GET_SOUND_ID()
		PLAY_SOUND_FROM_COORD(iDroneCutsceneSoundLoop, "Flight_Loop", vPos, "DLC_BTL_Drone_Sounds")
		SET_VARIABLE_ON_SOUND(iDroneCutsceneSoundLoop, "DroneRotationalSpeed", 0.0)
	ELSE
		SET_VARIABLE_ON_SOUND(iDroneCutsceneSoundLoop, "DroneRotationalSpeed", 0.0)
		IF IS_SOUND_ID_VALID(iDroneCutsceneSoundLoop)
		AND NOT HAS_SOUND_FINISHED(iDroneCutsceneSoundLoop)
			UPDATE_SOUND_COORD(iDroneCutsceneSoundLoop, vPos)
		ENDIF
	ENDIF
	
ENDPROC

PROC DRAW_FAKE_DRONE_SCALEFORM(INT iZoomLevel = 1, FLOAT fHeading = 0.0)
	
	IF NOT LOAD_FAKE_DRONE_SCALEFORM()		
		EXIT
	ENDIF
	
	DISABLE_SPECTATOR_FILTER_OPTION(TRUE)
	
	IF NOT IS_AUDIO_SCENE_ACTIVE("ULP5_Deal_Breaker_Drone_View_Scene")
		PRINTLN("[Cutscenes][Drone] - Calling START_AUDIO_SCENE with ULP5_Deal_Breaker_Drone_View_Scene")
		START_AUDIO_SCENE("ULP5_Deal_Breaker_Drone_View_Scene")
	ENDIF
	
	SCALEFORM_SET_ELEMENT_VISIBLE("SET_DETONATE_METER_IS_VISIBLE", FALSE)
	
	SCALEFORM_SET_ELEMENT_VISIBLE("SET_SHOCK_METER_IS_VISIBLE", FALSE)
	
	SCALEFORM_SET_ELEMENT_VISIBLE("SET_EMP_METER_IS_VISIBLE", FALSE)
	
	SCALEFORM_SET_ELEMENT_VISIBLE("SET_RETICLE_IS_VISIBLE", TRUE)
	
	SCALEFORM_SET_ELEMENT_VISIBLE("SET_HEADING_METER_IS_VISIBLE", TRUE)
	
	SCALEFORM_SET_ELEMENT_VISIBLE("SET_ZOOM_METER_IS_VISIBLE", TRUE)
	
	SCALEFORM_SET_ELEMENT_VISIBLE("SET_BOOST_METER_IS_VISIBLE", FALSE)
	
	SCALEFORM_SET_ELEMENT_VISIBLE("SET_TRANQUILIZE_METER_IS_VISIBLE", FALSE)
	
	SCALEFORM_SET_ELEMENT_VISIBLE("SET_MISSILE_METER_IS_VISIBLE", FALSE) 
	
	SCALEFORM_SET_ELEMENT_VISIBLE("SET_BOTTOM_LEFT_CORNER_IS_VISIBLE", FALSE)
	
	SCALEFORM_SET_ELEMENT_VISIBLE("SET_SOUND_WAVE_IS_VISIBLE", FALSE)
	
	SCALEFORM_SET_ELEMENT_VISIBLE("SET_INFO_LIST_IS_VISIBLE", FALSE)
	
	SCALEFORM_SET_ZOOM_LEVEL(0, "DRONE_ZOOM_1")
	SCALEFORM_SET_ZOOM_LEVEL(1, "")
	SCALEFORM_SET_ZOOM_LEVEL(2, "DRONE_ZOOM_2")
	SCALEFORM_SET_ZOOM_LEVEL(3, "")
	SCALEFORM_SET_ZOOM_LEVEL(4, "DRONE_ZOOM_3")
	
	// Drone zoom level
	MANAGE_DRONE_SCALEFORM_ZOOM_LEVEL(iZoomLevel)
		
	// Drone heading
	SCALEFORM_SET_HEADING(ROUND(fHeading))
			
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)
	DRAW_SCALEFORM_MOVIE_FULLSCREEN(sfDroneCutscene, 255, 255, 255, 0)
	
ENDPROC

FUNC BOOL IS_USING_CUTSCENE_DRONE_SCALEFORM(MC_DRONE_UI_PRESET eDroneUIPreset)
	RETURN eDroneUIPreset != MC_DRONE_UI_PRESET_NONE
ENDFUNC

PROC DRAW_CUTSCENE_DRONE_SCALEFORM(MC_DRONE_UI_PRESET eDroneUIPreset, CAMERA_INDEX camCutscene)
	
	INT iZoom = 0
	FLOAT fHeading	
	
	VECTOR vRot = GET_CAM_ROT(camCutscene)
	fHeading = (vRot.z - 180)
			
	SWITCH eDroneUIPreset
		CASE MC_DRONE_UI_PRESET_NONE
			EXIT
		BREAK
		CASE MC_DRONE_UI_PRESET_ZOOM_1
			iZoom = 0
		BREAK		
		CASE MC_DRONE_UI_PRESET_ZOOM_2
			iZoom = 2
		BREAK
		CASE MC_DRONE_UI_PRESET_ZOOM_3
			iZoom = 4
		BREAK
	ENDSWITCH
	
	DRAW_FAKE_DRONE_SCALEFORM(iZoom, fHeading)
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Interiors
// ##### Description: 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL SHOULD_CONSTRUCTION_SITE_HIGH_RISE_ROOFTOP_SHADOW_TIMECYCLE_MOD_BE_USED()
	IF GET_ULP_MISSION_FROM_ROOT_CONTENT_ID_HASH(g_FMMC_STRUCT.iRootContentIDHash) = ciULP_MISSION_5_OPERATION_PAPER_TRAIL
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC VECTOR GET_IAA_BASE_LOCATION()
	
	IF IS_CURRENT_MISSION_THIS_TUNER_ROBBERY_FINALE(TR_IAA_RAID)
		RETURN <<148.868, -609.587, 18.6063>>
	ENDIF
	
	RETURN <<2047.0, 2942.0, -61.9>>
ENDFUNC

FUNC VECTOR GET_BUNKER_LOCATION()
	
	IF IS_CURRENT_MISSION_THIS_TUNER_ROBBERY_FINALE(TR_BUNKER)
		INT iGangBoss = GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT()
		IF iGangBoss != -1
			IF GlobalPlayerBD_Flow[iGangBoss].Tuner.iBunkerIndex = 0 //ciTUNER_BUNKER_LOCATION_GRAND_SENORA
				RETURN <<2110.0190, 3326.1201, 44.3526>> //SIMPLE_INTERIOR_BUNKER_5
			ELIF GlobalPlayerBD_Flow[iGangBoss].Tuner.iBunkerIndex = 1 //ciTUNER_BUNKER_LOCATION_GRAPESEED
				RETURN <<1801.2729, 4705.4829, 38.8253>> //SIMPLE_INTERIOR_BUNKER_7
			ELIF GlobalPlayerBD_Flow[iGangBoss].Tuner.iBunkerIndex = 2 //ciTUNER_BUNKER_LOCATION_RATON_CANYON
				RETURN <<-388.8392, 4340.1094, 55.1741>> //SIMPLE_INTERIOR_BUNKER_10
			ELIF GlobalPlayerBD_Flow[iGangBoss].Tuner.iBunkerIndex = 3 //ciTUNER_BUNKER_LOCATION_HARMONY
				RETURN <<39.5967, 2930.5063, 54.8034>> //SIMPLE_INTERIOR_BUNKER_3
			ENDIF
		ENDIF
	ENDIF
	
	RETURN <<938.307678, -3196.111572, -100.000000>>
	
ENDFUNC

FUNC VECTOR GET_MOC_INTERIOR_LOCATION()
	IF iCachedMOCIndex != -1
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iCachedMOCIndex])
			RETURN GET_ENTITY_COORDS(NETWORK_GET_ENTITY_FROM_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iCachedMOCIndex]), FALSE)
		ENDIF
	ENDIF
	
	RETURN <<1103.562378, -3000.0, -40.0>>
ENDFUNC

// ##### ------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: HUD ------------------------------------------------------------------------------------------------------
// ##### Description: Loading and unloading HUD elements, scaleform etc.  -------------------------------------------------------
// ##### ------------------------------------------------------------------------------------------------------------------------
// ##### ------------------------------------------------------------------------------------------------------------------------

PROC CLEANUP_CCTV_ASSETS()
	SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(SI_SecurityCam)
ENDPROC

PROC RELEASE_COUNTDOWN_UI(MISSION_INTRO_COUNTDOWN_UI &uiToRelease)
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iTeamBitset2, ciBS2_STOP_GO_HUD)
	AND HAS_SCALEFORM_MOVIE_LOADED(uiToRelease.uiCountdown)
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(uiToRelease.uiCountdown)
		PRINTLN("RELEASE_COUNTDOWN_UI")
	ENDIF
	PRINTLN("RELEASE_NAMED_SCRIPT_AUDIO_BANK - HUD_321_GO Called from fm_mission_controller_hud.sch 1")
ENDPROC

PROC UNLOAD_IN_GAME_MISSION_SPECIFIC_SCALEFORM()
	PRINTLN("[RCC MISSION] UNLOAD_IN_GAME_MISSION_SPECIFIC_SCALEFORM")
	IF HAS_SCALEFORM_MOVIE_LOADED(sfiPPGenericMovie)
		PRINTLN("[RCC MISSION] - Cleanup - Unloading sfiPPGenericMovie")
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sfiPPGenericMovie)
	ENDIF
	
	RELEASE_COUNTDOWN_UI(cduiIntro)
ENDPROC

PROC UNLOAD_MISSION_SPECIFIC_SCALEFORM()
	PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - UNLOAD_MISSION_SPECIFIC_SCALEFORM ################################")
	
	UNLOAD_IN_GAME_MISSION_SPECIFIC_SCALEFORM()
	
	IF HAS_SCALEFORM_MOVIE_LOADED(sfiDownloadScreen)
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sfiDownloadScreen)
	ENDIF			

	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iTeamBitset2, ciBS2_STOP_GO_HUD)
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(cduiIntro.uiCountdown)
	ENDIF
	IF HAS_SCALEFORM_MOVIE_LOADED(scLB_scaleformID)
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(scLB_scaleformID)
		PRINTLN("CLEANUP_SOCIAL_CLUB_LEADERBOARD marking SCLB scaleform movie as no longer needed (FM_mission_controler)") 
	ENDIF
	
	IF HAS_SCALEFORM_MOVIE_LOADED(scaleformDpadMovie)
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(scaleformDpadMovie)
	ENDIF
	
	CLEANUP_ARROW_MOVIE(siRallyArrow)
	
	CLEANUP_FAKE_DRONE_SCALEFORM()
	
ENDPROC

PROC REQUEST_CCTV_ASSETS()
	SI_SecurityCam = REQUEST_SCALEFORM_MOVIE("SECURITY_CAM")
	CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Requesting scaleform movie SECURITY_CAM" )
ENDPROC

FUNC BOOL HAVE_CCTV_ASSETS_LOADED()
	IF HAS_SCALEFORM_MOVIE_LOADED(SI_SecurityCam)
		CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] scaleform movie SECURITY_CAM has loaded!" )
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

// ##### ------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Peds -----------------------------------------------------------------------------------------------------
// ##### Description: Loading and unloading animations.  ------------------------------------------------------------------------
// ##### ------------------------------------------------------------------------------------------------------------------------
// ##### ------------------------------------------------------------------------------------------------------------------------

FUNC STRING GET_ANIMAL_SIT_IN_VEHICLE_ANIM(MODEL_NAMES mn)
	
	IF mn = A_C_CHOP
	OR mn = INT_TO_ENUM(MODEL_NAMES, HASH("A_C_CHOP_02"))
			RETURN "sit"
	ENDIF
	
	RETURN "INVALID"
ENDFUNC
FUNC STRING GET_ANIMAL_SIT_IN_VEHICLE_DICT(MODEL_NAMES mn)
	
	IF mn = A_C_CHOP
	OR mn = INT_TO_ENUM(MODEL_NAMES, HASH("A_C_CHOP_02"))
		RETURN "ANIM@SCRIPTED@SHORT_TRIP@FIXF_TRIP1_IG4_CHOP_IN_CAR@"
	ENDIF
	
	RETURN "INVALID"
ENDFUNC
FUNC STRING GET_ANIMAL_GET_OUT_VEHICLE_ANIM(MODEL_NAMES mn)
	
	IF mn = A_C_CHOP
	OR mn = INT_TO_ENUM(MODEL_NAMES, HASH("A_C_CHOP_02"))
		RETURN "get_out"
	ENDIF
	
	RETURN "INVALID"
ENDFUNC
FUNC STRING GET_ANIMAL_GET_OUT_VEHICLE_DICT(MODEL_NAMES mn)
	
	IF mn = A_C_CHOP
	OR mn = INT_TO_ENUM(MODEL_NAMES, HASH("A_C_CHOP_02"))
		RETURN "ANIM@SCRIPTED@SHORT_TRIP@FIXF_TRIP1_IG4_CHOP_IN_CAR@"
	ENDIF
	
	RETURN "INVALID"
ENDFUNC
FUNC STRING GET_ANIMAL_GET_IN_VEHICLE_ANIM(MODEL_NAMES mn)
	
	IF mn = A_C_CHOP
	OR mn = INT_TO_ENUM(MODEL_NAMES, HASH("A_C_CHOP_02"))
		RETURN "get_in"
	ENDIF
	
	RETURN "INVALID"
ENDFUNC
FUNC STRING GET_ANIMAL_GET_IN_VEHICLE_DICT(MODEL_NAMES mn)
	
	IF mn = A_C_CHOP
	OR mn = INT_TO_ENUM(MODEL_NAMES, HASH("A_C_CHOP_02"))
		RETURN "ANIM@SCRIPTED@SHORT_TRIP@FIXF_TRIP1_IG4_CHOP_IN_CAR@"
	ENDIF	
	
	
	RETURN "INVALID"
ENDFUNC

// IS_PED_AN_ANIMAL only checks that the "player" ped is not their normal freemode model...
FUNC BOOL MC_IS_PED_AN_ANIMAL(MODEL_NAMES mn)
	SWITCH mn
		CASE a_c_rhesus		
		CASE A_C_BOAR		
		CASE A_C_CAT_01		
		CASE A_C_COW		
		CASE A_C_COYOTE		
		CASE A_C_DEER		
		CASE A_C_HUSKY		
		CASE A_C_MTLION		
		CASE A_C_PIG  		
		CASE A_C_POODLE 	
		CASE A_C_PUG 		
		CASE A_C_RABBIT_01	
		CASE A_C_RETRIEVER	
		CASE A_C_ROTTWEILER	
		CASE A_C_SHEPHERD  	
		CASE A_C_WESTY 		
		CASE A_C_CHICKENHAWK
		CASE A_C_CORMORANT	
		CASE A_C_CROW		
		CASE A_C_HEN		
		CASE A_C_PIGEON		
		CASE A_C_SEAGULL	
		CASE A_C_DOLPHIN	
		CASE A_C_FISH		 
		CASE A_C_KILLERWHALE
		CASE A_C_SHARKHAMMER
		CASE A_C_SHARKTIGER	
		CASE A_C_STINGRAY 	
		CASE IG_ORLEANS		
		CASE A_C_CHOP		
		CASE A_C_HUMPBACK	
			RETURN TRUE
	ENDSWITCH
	
	IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("A_C_CHOP_02"))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC VEHICLE_SEAT GET_SUITABLE_SEAT_TYPE_FOR_CHOP(PED_INDEX pedIndex, VEHICLE_INDEX vehIndex)
		
	VEHICLE_SEAT vsType = VS_BACK_RIGHT
	
	IF IS_PED_IN_ANY_VEHICLE(pedIndex)
		vsType = GET_SEAT_PED_IS_IN(pedIndex)
		IF vsType = VS_BACK_RIGHT
		OR vsType = VS_FRONT_RIGHT
			RETURN vsType
		ENDIF
	ENDIF
	
	IF NOT IS_VEHICLE_SEAT_FREE(vehIndex, vsType, TRUE)
		vsType = VS_FRONT_RIGHT
		
		IF NOT IS_VEHICLE_SEAT_FREE(vehIndex, vsType, TRUE)	
			vsType = VS_ANY_PASSENGER
		ENDIF
	ENDIF
	
	RETURN vsType
ENDFUNC

FUNC SC_DOOR_LIST GET_SUITABLE_DOOR_TYPE_FOR_CHOP(PED_INDEX pedIndex, VEHICLE_INDEX vehIndex)
	
	IF IS_PED_IN_ANY_VEHICLE(pedIndex)
		VEHICLE_SEAT vsType = GET_SEAT_PED_IS_IN(pedIndex)
		IF vsType = VS_BACK_RIGHT
			RETURN SC_DOOR_REAR_RIGHT
		ELIF vsType = VS_FRONT_RIGHT
			RETURN SC_DOOR_FRONT_RIGHT			
		ENDIF
	ENDIF
	
	SC_DOOR_LIST scType = SC_DOOR_REAR_RIGHT
	
	IF NOT IS_VEHICLE_SEAT_FREE(vehIndex, VS_BACK_RIGHT, TRUE)
		scType = SC_DOOR_FRONT_RIGHT
		
		IF NOT IS_VEHICLE_SEAT_FREE(vehIndex, VS_FRONT_RIGHT, TRUE)	
			scType = SC_DOOR_INVALID
		ENDIF		
	ENDIF
	
	RETURN scType
ENDFUNC

FUNC STRING GET_SUITABLE_DOOR_BONE_FOR_CHOP(PED_INDEX pedIndex, VEHICLE_INDEX vehIndex)
		
	IF IS_PED_IN_ANY_VEHICLE(pedIndex)
		VEHICLE_SEAT vsType = GET_SEAT_PED_IS_IN(pedIndex)
		IF vsType = VS_BACK_RIGHT
			RETURN "seat_pside_r"
		ELIF vsType = VS_FRONT_RIGHT
			RETURN "seat_pside_f"			
		ENDIF
	ENDIF
	
	STRING sBone = "seat_pside_r"
	SC_DOOR_LIST scType = GET_SUITABLE_DOOR_TYPE_FOR_CHOP(pedIndex, vehIndex)
	IF scType = SC_DOOR_FRONT_RIGHT
		sBone = "seat_pside_f"	
	ENDIF	
	
	RETURN sBone
ENDFUNC

PROC PROCESS_TASK_COMPANION_LEAVE_VEHICLE_CHOP(FMMC_PED_STATE &sPedState, PED_COMPANION_TASK_DATA &sPedCompanionData)
	// Special Case for Animal Companions.
	IF IS_PED_IN_ANY_VEHICLE(sPedState.pedIndex)
	OR IS_BIT_SET(sPedCompanionData.iBS, ciCOMPANION_BITSET_INSIDE_VEHICLE)
		PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][CLIENT][Companion] - PROCESS_TASK_COMPANION_LEAVE_VEHICLE - Leaving Vehicle")	
		
		SET_BIT(sPedCompanionData.iBS, ciCOMPANION_BITSET_DOING_SPECIAL_LEAVE_VEH)
		
		VEHICLE_INDEX viVeh
		IF NETWORK_DOES_NETWORK_ID_EXIST(sPedCompanionData.targetEntityFriendVehicleCached)
			viVeh = NET_TO_VEH(sPedCompanionData.targetEntityFriendVehicleCached)
		ELIF IS_PED_IN_ANY_VEHICLE(sPedState.pedIndex)
			viVeh = GET_VEHICLE_PED_IS_IN(sPedState.pedIndex)
		ENDIF
		
		IF DOES_ENTITY_EXIST(viVeh)
			REQUEST_ANIM_DICT(GET_ANIMAL_GET_OUT_VEHICLE_DICT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].mn))
			IF HAS_ANIM_DICT_LOADED(GET_ANIMAL_GET_OUT_VEHICLE_DICT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].mn))
				
				IF NOT IS_BIT_SET(sPedCompanionData.iBS, ciCOMPANION_BITSET_OPEN_DOOR_FOR_EXIT)
					BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_ChopToggleVehicleDoor, DEFAULT, sPedState.iIndex, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)					
					MC_REINIT_NET_TIMER_WITH_TIMESTAMP(sPedCompanionData.iTimeStamp_EnterVeh)
					SET_BIT(sPedCompanionData.iBS, ciCOMPANION_BITSET_OPEN_DOOR_FOR_EXIT)
					PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][CLIENT][Companion] - PROCESS_TASK_COMPANION_LEAVE_VEHICLE - Open Door")
				ENDIF
				
				IF sPedCompanionData.iSyncScene = -1 
				AND MC_HAS_NET_TIMER_EXPIRED_WITH_TIME_STAMP(sPedCompanionData.iTimeStamp_EnterVeh, 1250)
					PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][CLIENT][Companion] - PROCESS_TASK_COMPANION_LEAVE_VEHICLE - Start Sync Scene.")			
					sPedCompanionData.iSyncScene = NETWORK_CREATE_SYNCHRONISED_SCENE(<<0,0,0>>,<<0,0,0>>)
					NETWORK_ATTACH_SYNCHRONISED_SCENE_TO_ENTITY(sPedCompanionData.iSyncScene, viVeh, GET_ENTITY_BONE_INDEX_BY_NAME(viVeh, GET_SUITABLE_DOOR_BONE_FOR_CHOP(sPedState.pedIndex, viVeh)))
					NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(sPedState.pedIndex, sPedCompanionData.iSyncScene, GET_ANIMAL_GET_OUT_VEHICLE_DICT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].mn), GET_ANIMAL_GET_OUT_VEHICLE_ANIM(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].mn), INSTANT_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_TAG_SYNC_OUT|SYNCED_SCENE_ON_ABORT_STOP_SCENE)
					NETWORK_START_SYNCHRONISED_SCENE(sPedCompanionData.iSyncScene)
					CLEAR_PED_TASKS_IMMEDIATELY(sPedState.pedIndex)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(sPedState.pedIndex)
					PLAY_FACIAL_ANIM(sPedState.pedIndex, GET_ANIMAL_GET_OUT_VEHICLE_DICT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].mn), GET_ANIMAL_GET_OUT_VEHICLE_ANIM(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].mn))
					SET_BIT(sPedCompanionData.iBS, ciCOMPANION_BITSET_CLOSE_DOOR_AFTER_SYNC_SCENE)
					CLEAR_BIT(sPedCompanionData.iBS, ciCOMPANION_BITSET_INSIDE_VEHICLE)
					MC_REINIT_NET_TIMER_WITH_TIMESTAMP(sPedCompanionData.iTimeStamp_EnterVeh)
					BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_ChopLeftVehicle, DEFAULT, sPedState.iIndex)					
				ENDIF
			ENDIF
		ELSE
			PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][CLIENT][Companion] - PROCESS_TASK_COMPANION_LEAVE_VEHICLE - Vehicle does not exist suddenly. Might have been cleaned up. Consider as left vehicle.")
			CLEAR_PED_TASKS_IMMEDIATELY(sPedState.pedIndex)
			FORCE_PED_AI_AND_ANIMATION_UPDATE(sPedState.pedIndex)
			CLEAR_BIT(sPedCompanionData.iBS, ciCOMPANION_BITSET_INSIDE_VEHICLE)
			MC_REINIT_NET_TIMER_WITH_TIMESTAMP(sPedCompanionData.iTimeStamp_EnterVeh)
			BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_ChopLeftVehicle, DEFAULT, sPedState.iIndex)		
		ENDIF
	ELSE				
		IF MC_HAS_NET_TIMER_EXPIRED_WITH_TIME_STAMP(sPedCompanionData.iTimeStamp_EnterVeh, 2000)
		AND IS_BIT_SET(sPedCompanionData.iBS, ciCOMPANION_BITSET_CLOSE_DOOR_AFTER_SYNC_SCENE)
			BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_ChopToggleVehicleDoor, DEFAULT, sPedState.iIndex, DEFAULT, DEFAULT, DEFAULT, DEFAULT, FALSE)
			CLEAR_BIT(sPedCompanionData.iBS, ciCOMPANION_BITSET_CLOSE_DOOR_AFTER_SYNC_SCENE)
		ENDIF
		
		IF IS_BIT_SET(sPedCompanionData.iBS, ciCOMPANION_BITSET_CLOSE_DOOR_AFTER_SYNC_SCENE)			
		AND MC_HAS_NET_TIMER_EXPIRED_WITH_TIME_STAMP(sPedCompanionData.iTimeStamp_EnterVeh, 3000)
			PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][CLIENT][Companion] - PROCESS_TASK_COMPANION_LEAVE_VEHICLE - Cleanup Exit Veh (Shut Door)")
			
			CLEAR_PED_TASKS_IMMEDIATELY(sPedState.pedIndex)
			CLEAR_BIT(sPedCompanionData.iBS, ciCOMPANION_BITSET_OPEN_DOOR_FOR_EXIT)
			CLEAR_BIT(sPedCompanionData.iBS, ciCOMPANION_BITSET_DOING_SPECIAL_LEAVE_VEH)
			sPedCompanionData.targetEntityFriendVehicleCached = NULL
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sPedState.pedIndex, FALSE)
			MC_STOP_NET_TIMER_WITH_TIMESTAMP(sPedCompanionData.iTimeStamp_VehFailsafe)
			MC_STOP_NET_TIMER_WITH_TIMESTAMP(sPedCompanionData.iTimeStamp_EnterVeh)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_TASK_COMPANION_ENTER_VEHICLE_CHOP(FMMC_PED_STATE &sPedState, PED_COMPANION_TASK_DATA &sPedCompanionData)

	VEHICLE_INDEX viVeh = NET_TO_VEH(sPedCompanionData.targetEntityFriendVehicle)
		
	// Special Case for Animal Companions.
	IF NOT IS_BIT_SET(sPedCompanionData.iBS, ciCOMPANION_BITSET_INSIDE_VEHICLE)
		
		IF MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(sPedCompanionData.iTimeStamp_VehFailsafe)
		AND MC_HAS_NET_TIMER_EXPIRED_WITH_TIME_STAMP(sPedCompanionData.iTimeStamp_VehFailsafe, 10000)
			PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][CLIENT][Companion] - PROCESS_TASK_COMPANION_ENTER_VEHICLE - Fail Safe Hit.")			
			SET_BIT(sPedCompanionData.iBS, ciCOMPANION_BITSET_INSIDE_VEHICLE)
			sPedCompanionData.targetEntityFriendVehicleCached = sPedCompanionData.targetEntityFriendVehicle						
			BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_ChopSatInVehicle, DEFAULT, sPedState.iIndex)			
			MC_REINIT_NET_TIMER_WITH_TIMESTAMP(sPedCompanionData.iTimeStamp_EnterVeh)
			EXIT
		ENDIF
		
		SET_BIT(sPedCompanionData.iBS, ciCOMPANION_BITSET_DOING_SPECIAL_ENTER_VEH)
		
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sPedState.pedIndex, TRUE)
		
		PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][CLIENT][Companion] - PROCESS_TASK_COMPANION_ENTER_VEHICLE - Entering Vehicle")
		
		VECTOR vOffset = GET_ENTITY_BONE_POSTION(viVeh, GET_ENTITY_BONE_INDEX_BY_NAME(viVeh, GET_SUITABLE_DOOR_BONE_FOR_CHOP(sPedState.pedIndex, viVeh)))
		vOffset = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vOffset, GET_ENTITY_HEADING(viVeh), <<0.75, -0.15, 0.0>>)
		FLOAT fDist = VDIST2(GET_FMMC_PED_COORDS(sPedState), vOffset)
		
		IF NOT IS_BIT_SET(sPedCompanionData.iBS, ciCOMPANION_BITSET_START_GOTO_FOR_SYNC_SCENE)
			PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][CLIENT][Companion] - PROCESS_TASK_COMPANION_ENTER_VEHICLE - Start Goto Task")
			SET_BIT(sPedCompanionData.iBS, ciCOMPANION_BITSET_START_GOTO_FOR_SYNC_SCENE)
			TASK_GO_TO_COORD_ANY_MEANS_EXTRA_PARAMS(sPedState.pedIndex, vOffset, PEDMOVEBLENDRATIO_RUN, NULL, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 7500)			
			MC_STOP_NET_TIMER_WITH_TIMESTAMP(sPedCompanionData.iTimeStamp_VehFailsafe)
			MC_STOP_NET_TIMER_WITH_TIMESTAMP(sPedCompanionData.iTimeStamp_VehFailsafe)			
		ENDIF
		
		IF fDist <= POW(1.25, 2.0)
			IF NOT IS_BIT_SET(sPedCompanionData.iBS, ciCOMPANION_BITSET_AT_DOOR_FOR_SYNC_SCENE)
				BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_ChopToggleVehicleDoor, DEFAULT, sPedState.iIndex, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)			
				CLEAR_PED_TASKS_IMMEDIATELY(sPedState.pedIndex)
				SET_BIT(sPedCompanionData.iBS, ciCOMPANION_BITSET_AT_DOOR_FOR_SYNC_SCENE)
				MC_REINIT_NET_TIMER_WITH_TIMESTAMP(sPedCompanionData.iTimeStamp_EnterVeh)
				PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][CLIENT][Companion] - PROCESS_TASK_COMPANION_ENTER_VEHICLE - Delay")
			ENDIF			
		ELSE
			IF IS_BIT_SET(sPedCompanionData.iBS, ciCOMPANION_BITSET_START_GOTO_FOR_SYNC_SCENE)
			AND NOT IS_BIT_SET(sPedCompanionData.iBS, ciCOMPANION_BITSET_AT_DOOR_FOR_SYNC_SCENE)						
			AND NOT IS_PED_PERFORMING_TASK(sPedState.pedIndex, SCRIPT_TASK_GO_TO_COORD_ANY_MEANS_EXTRA_PARAMS)
				CLEAR_BIT(sPedCompanionData.iBS, ciCOMPANION_BITSET_START_GOTO_FOR_SYNC_SCENE)
				PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][CLIENT][Companion] - PROCESS_TASK_COMPANION_ENTER_VEHICLE - Task failed. Retasking.")
			ENDIF
		ENDIF
		
		IF (IS_BIT_SET(sPedCompanionData.iBS, ciCOMPANION_BITSET_AT_DOOR_FOR_SYNC_SCENE)
		AND MC_HAS_NET_TIMER_EXPIRED_WITH_TIME_STAMP(sPedCompanionData.iTimeStamp_EnterVeh, 1000))
			REQUEST_ANIM_DICT(GET_ANIMAL_GET_IN_VEHICLE_DICT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].mn))
			IF HAS_ANIM_DICT_LOADED(GET_ANIMAL_GET_IN_VEHICLE_DICT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].mn))					
				IF sPedCompanionData.iSyncScene = -1
					PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][CLIENT][Companion] - PROCESS_TASK_COMPANION_ENTER_VEHICLE - Start Sync Scene.")											
					CLEAR_PED_TASKS_IMMEDIATELY(sPedState.pedIndex)
					sPedCompanionData.iSyncScene = NETWORK_CREATE_SYNCHRONISED_SCENE((<<0,0,0>>), (<<0,0,0>>), DEFAULT, TRUE)
					NETWORK_ATTACH_SYNCHRONISED_SCENE_TO_ENTITY(sPedCompanionData.iSyncScene, viVeh, GET_ENTITY_BONE_INDEX_BY_NAME(viVeh, GET_SUITABLE_DOOR_BONE_FOR_CHOP(sPedState.pedIndex, viVeh)))						
					NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(sPedState.pedIndex, sPedCompanionData.iSyncScene, GET_ANIMAL_GET_IN_VEHICLE_DICT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].mn), GET_ANIMAL_GET_IN_VEHICLE_ANIM(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].mn), INSTANT_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_TAG_SYNC_OUT|SYNCED_SCENE_ON_ABORT_STOP_SCENE)
					NETWORK_START_SYNCHRONISED_SCENE(sPedCompanionData.iSyncScene)
					PLAY_FACIAL_ANIM(sPedState.pedIndex, GET_ANIMAL_GET_IN_VEHICLE_DICT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].mn), GET_ANIMAL_GET_IN_VEHICLE_ANIM(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].mn))
					
					FORCE_PED_AI_AND_ANIMATION_UPDATE(sPedState.pedIndex)
					SET_BIT(sPedCompanionData.iBS, ciCOMPANION_BITSET_CLOSE_DOOR_AFTER_SYNC_SCENE)
					SET_BIT(sPedCompanionData.iBS, ciCOMPANION_BITSET_INSIDE_VEHICLE)
					sPedCompanionData.targetEntityFriendVehicleCached = sPedCompanionData.targetEntityFriendVehicle						
					BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_ChopSatInVehicle, DEFAULT, sPedState.iIndex)
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF ((IS_SYNCHRONIZED_SCENE_RUNNING(sPedCompanionData.iSyncScene)
		AND GET_SYNCHRONIZED_SCENE_PHASE(sPedCompanionData.iSyncScene) >= 0.99)
		OR NOT IS_SYNCHRONIZED_SCENE_RUNNING(sPedCompanionData.iSyncScene))
		AND NOT IS_PED_PERFORMING_TASK(sPedState.pedIndex, SCRIPT_TASK_PLAY_ANIM)
			PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][CLIENT][Companion] - PROCESS_TASK_COMPANION_ENTER_VEHICLE - Task Ped to Sit.")
			IF NOT IS_PED_IN_ANY_VEHICLE(sPedState.pedIndex)
				SET_PED_INTO_VEHICLE(sPedState.pedIndex, viVeh, GET_SUITABLE_SEAT_TYPE_FOR_CHOP(sPedState.pedIndex, viVeh))
			ENDIF
			TASK_PLAY_ANIM(sPedState.pedIndex, GET_ANIMAL_SIT_IN_VEHICLE_DICT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].mn), GET_ANIMAL_SIT_IN_VEHICLE_ANIM(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].mn), NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
			PLAY_FACIAL_ANIM(sPedState.pedIndex, GET_ANIMAL_SIT_IN_VEHICLE_DICT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].mn), GET_ANIMAL_SIT_IN_VEHICLE_ANIM(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].mn))
			FORCE_PED_AI_AND_ANIMATION_UPDATE(sPedState.pedIndex)
		ENDIF
		
		IF IS_BIT_SET(sPedCompanionData.iBS, ciCOMPANION_BITSET_CLOSE_DOOR_AFTER_SYNC_SCENE)
		AND MC_HAS_NET_TIMER_EXPIRED_WITH_TIME_STAMP(sPedCompanionData.iTimeStamp_EnterVeh, 3000)
		AND IS_PED_IN_ANY_VEHICLE(sPedState.pedIndex)
			PRINTLN("[Peds][Ped ", sPedState.iIndex, "][Task][CLIENT][Companion] - PROCESS_TASK_COMPANION_ENTER_VEHICLE - Cleanup Enter Veh (Shut Door)")
			
			BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_ChopToggleVehicleDoor, DEFAULT, sPedState.iIndex, DEFAULT, DEFAULT, DEFAULT, DEFAULT, FALSE)
			
			CLEAR_BIT(sPedCompanionData.iBS, ciCOMPANION_BITSET_CLOSE_DOOR_AFTER_SYNC_SCENE)
			CLEAR_BIT(sPedCompanionData.iBS, ciCOMPANION_BITSET_AT_DOOR_FOR_SYNC_SCENE)
			CLEAR_BIT(sPedCompanionData.iBS, ciCOMPANION_BITSET_START_GOTO_FOR_SYNC_SCENE)
			CLEAR_BIT(sPedCompanionData.iBS, ciCOMPANION_BITSET_OPEN_DOOR_FOR_EXIT)
			MC_STOP_NET_TIMER_WITH_TIMESTAMP(sPedCompanionData.iTimeStamp_VehFailsafe)
			MC_STOP_NET_TIMER_WITH_TIMESTAMP(sPedCompanionData.iTimeStamp_EnterVeh)			
		ENDIF
	ENDIF
ENDPROC

PROC INIT_COMPANION(FMMC_PED_STATE &sPedState)
	
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sPedState.pedIndex, FALSE)
	
	IF MC_IS_PED_AN_ANIMAL(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].mn)
		REMOVE_ALL_PED_WEAPONS(sPedState.pedIndex)
		SET_PED_CAN_PLAY_AMBIENT_ANIMS(sPedState.pedIndex, FALSE)
		SET_PED_CAN_PLAY_AMBIENT_BASE_ANIMS(sPedState.pedIndex, FALSE)
		GIVE_WEAPON_TO_PED(sPedState.PedIndex, WEAPONTYPE_ANIMAL, INFINITE_AMMO, TRUE, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(sPedState.pedIndex, CA_USE_COVER, FALSE)
		
		SET_PED_FLEE_ATTRIBUTES(sPedState.pedIndex, FA_DISABLE_FLEE_FROM_INDIRECT_THREATS, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(sPedState.pedIndex, CA_DISABLE_FLEE_FROM_COMBAT, TRUE)
		SET_PED_FLEE_ATTRIBUTES(sPedState.pedIndex, FA_DISABLE_COWER, TRUE)		
		SET_PED_COMBAT_ATTRIBUTES(sPedState.pedIndex, CA_CAN_FIGHT_ARMED_PEDS_WHEN_NOT_ARMED, TRUE)		
	ENDIF	
	
ENDPROC

PROC PROCESS_CLEANUP_BLOCK_SPEECH_CONTEXT_GROUP_FOR_PED(FMMC_PED_STATE &sPedState)

	IF NOT sPedState.bInjured
	OR sPedState.bExists
		EXIT
	ENDIF
	
	IF FMMC_IS_LONG_BIT_SET(iPedCausedSpeechContextBlock_CAR_CRASH, sPedState.iIndex)
		UNBLOCK_SPEECH_CONTEXT_GROUP("BLOCKED_CONTEXTS_MISSION_CAR_CRASH")
		FMMC_CLEAR_LONG_BIT(iPedCausedSpeechContextBlock_CAR_CRASH, sPedState.iIndex)
	ENDIF
	
ENDPROC

PROC PROCESS_SET_BLOCK_SPEECH_CONTEXT_GROUP_FOR_PED(FMMC_PED_STATE &sPedState)

	IF sPedState.bInjured
		EXIT
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].mn = INT_TO_ENUM(MODEL_NAMES, HASH("ig_ary_02"))		
		IF NOT FMMC_IS_LONG_BIT_SET(iPedCausedSpeechContextBlock_CAR_CRASH, sPedState.iIndex)
		AND IS_BIT_SET(iLocalBoolCheck25, LBOOL25_MUSIC_STARTED_DRE_TRACK)
			BLOCK_SPEECH_CONTEXT_GROUP("BLOCKED_CONTEXTS_MISSION_CAR_CRASH", AUD_CONTEXT_BLOCK_BUDDYS)
			FMMC_SET_LONG_BIT(iPedCausedSpeechContextBlock_CAR_CRASH, sPedState.iIndex)
		ENDIF
	ENDIF

ENDPROC

// ##### ------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Animation ------------------------------------------------------------------------------------------------
// ##### Description: Loading and unloading animations.  ------------------------------------------------------------------------
// ##### ------------------------------------------------------------------------------------------------------------------------
// ##### ------------------------------------------------------------------------------------------------------------------------

FUNC STRING GET_WALK_STYLE_CLIPSET(INT iWalkStyle)

	BOOL bIsFemale = IS_PED_FEMALE(LocalPlayerPed)
	
	SWITCH iWalkStyle
		CASE ciFMMC_WALK_STYLE__DEFAULT			RETURN ""
		CASE ciFMMC_WALK_STYLE__FEMME			RETURN PICK_STRING(bIsFemale, "MOVE_F@FEMME@", "MOVE_M@FEMME@")
		CASE ciFMMC_WALK_STYLE__GANGSTER		RETURN PICK_STRING(bIsFemale, "MOVE_F@GANGSTER@NG", "MOVE_M@GANGSTER@NG")
		CASE ciFMMC_WALK_STYLE__POSH			RETURN PICK_STRING(bIsFemale, "MOVE_F@POSH@", "MOVE_M@POSH@")
		CASE ciFMMC_WALK_STYLE__TOUGH_GUY		RETURN PICK_STRING(bIsFemale, "MOVE_F@TOUGH_GUY@", "MOVE_M@TOUGH_GUY@")
		CASE ciFMMC_WALK_STYLE__GROOVING		RETURN PICK_STRING(bIsFemale, "ANIM@MOVE_F@GROOVING@", "ANIM@MOVE_M@GROOVING@")
	ENDSWITCH
	
	RETURN ""
ENDFUNC

PROC UNLOAD_ALL_WALK_STYLE_CLIPSETS()
	INT iClipset
	FOR iClipset = ciFMMC_WALK_STYLE__DEFAULT TO ciFMMC_WALK_STYLE__MAX - 1
		IF iClipset = ciFMMC_WALK_STYLE__DEFAULT
			RELOOP
		ENDIF
		
		REMOVE_CLIP_SET(GET_WALK_STYLE_CLIPSET(iClipset))
	ENDFOR
ENDPROC

FUNC BOOL LOAD_WALK_STYLE_CLIPSET(INT iWalkStyle)
	STRING sClipset = GET_WALK_STYLE_CLIPSET(iWalkStyle)
	REQUEST_CLIP_SET(sClipset)
	
	IF HAS_CLIP_SET_LOADED(sClipset)
		PRINTLN("[WalkStyles] LOAD_WALK_STYLE_CLIPSET | Loaded ", sClipset)
		RETURN TRUE
	ELSE
		PRINTLN("[WalkStyles] LOAD_WALK_STYLE_CLIPSET | Still loading ", sClipset, "...")
		RETURN FALSE
	ENDIF
ENDFUNC

FUNC BOOL APPLY_WALK_STYLE_CLIPSET(PED_INDEX piPed, INT iWalkStyle, FLOAT fBlendIn = 0.25)
	IF iWalkStyle = ciFMMC_WALK_STYLE__DEFAULT
		PRINTLN("[WalkStyles] APPLY_WALK_STYLE_CLIPSET | Resetting ped movement clipset!")
		RESET_PED_MOVEMENT_CLIPSET(piPed, fBlendIn)
		RETURN TRUE
	ENDIF
	
	IF LOAD_WALK_STYLE_CLIPSET(iWalkStyle)
		STRING sClipset = GET_WALK_STYLE_CLIPSET(iWalkStyle)
		PRINTLN("[WalkStyles] APPLY_WALK_STYLE_CLIPSET | Applying clipset ", sClipset, " now with fBlendIn ", fBlendIn)
		SET_PED_MOVEMENT_CLIPSET(piPed, sClipset, fBlendIn)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC STRING GET_PED_SYNC_SCENE_ENGLISH_DAVE_MEDITATION_YOGA_IDLE_ANIMATION(INT iPed)
	INT iRand = GET_RANDOM_INT_IN_RANGE(0, 100)

	IF iRand <= 40
		iRand = GET_RANDOM_INT_IN_RANGE(0, 6)
		WHILE iRand = sMissionPedsLocalVars[iPed].iRandomAnimLastPlayed 
			iRand = GET_RANDOM_INT_IN_RANGE(0, 6)
		ENDWHILE
		sMissionPedsLocalVars[iPed].iRandomAnimLastPlayed = iRand
		
		SWITCH iRand
			CASE 0			RETURN "IDLE_A"
			CASE 1			RETURN "IDLE_B"
			CASE 2			RETURN "IDLE_C"
			CASE 3			RETURN "IDLE_D"
			CASE 4			RETURN "IDLE_E"
			CASE 5			RETURN "IDLE_F"
		ENDSWITCH
	ELSE
		iRand = GET_RANDOM_INT_IN_RANGE(0, 4)
		WHILE iRand = sMissionPedsLocalVars[iPed].iRandomAnimLastPlayed 
			iRand = GET_RANDOM_INT_IN_RANGE(0, 4)
		ENDWHILE
		sMissionPedsLocalVars[iPed].iRandomAnimLastPlayed = iRand
		
		SWITCH iRand
			CASE 0			RETURN "Need_A_Mantra"		
			CASE 1			RETURN "Ohm_Fucking_Ohm"		
			CASE 2			RETURN "Everythings_Cool"		
			CASE 3			RETURN "Bring_It_Down_Baby"
		ENDSWITCH
	ENDIF
	
	RETURN ""
ENDFUNC

FUNC STRING GET_PED_SYNC_SCENE_ENGLISH_DAVE_MEDITATION_BARS_IDLE_ANIMATION(INT iPed)
	INT iRand = GET_RANDOM_INT_IN_RANGE(0, 100)
	
	IF iRand <= 40
		iRand = GET_RANDOM_INT_IN_RANGE(0, 5)
		
		SWITCH iRand
			CASE 0			RETURN "IDLE_A"
			CASE 1			RETURN "IDLE_B"
			CASE 2			RETURN "IDLE_C"
			CASE 3			RETURN "IDLE_D"
			CASE 4			RETURN "IDLE_E"
		ENDSWITCH
	ELSE
		iRand = GET_RANDOM_INT_IN_RANGE(0, 4)
		WHILE iRand = sMissionPedsLocalVars[iPed].iRandomAnimLastPlayed 
			iRand = GET_RANDOM_INT_IN_RANGE(0, 4)
		ENDWHILE
		
		sMissionPedsLocalVars[iPed].iRandomAnimLastPlayed = iRand
		
		SWITCH iRand
			CASE 0			RETURN "A_God_A_Deity"
			CASE 1			RETURN "He_Sees_Everything"
			CASE 2			RETURN "Rob_A_Genius"
			CASE 3			RETURN "Sun_Moon_And_Stars"
		ENDSWITCH
	ENDIF
	
	RETURN ""
ENDFUNC

FUNC STRING GET_PED_SYNC_SCENE_ENGLISH_DAVE_MEDITATION_TRANSITION_TO_YOGA_ANIMATION()
	INT iRand = 0
	
	SWITCH iRand
		CASE 0			RETURN "ENTER_YOGA"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC STRING GET_PED_SYNC_SCENE_ENGLISH_DAVE_MEDITATION_TRANSITION_TO_BARS_ANIMATION(INT iPed, BOOL bForceBreakout)
	INT iRand = GET_RANDOM_INT_IN_RANGE(0, 8)
	WHILE iRand = sMissionPedsLocalVars[iPed].iRandomAnimLastPlayed 
		iRand = GET_RANDOM_INT_IN_RANGE(0, 8)
	ENDWHILE
	
	IF bForceBreakout
		iRand = GET_RANDOM_INT_IN_RANGE(4, 8)
		WHILE iRand = sMissionPedsLocalVars[iPed].iRandomAnimLastPlayed 
			iRand = GET_RANDOM_INT_IN_RANGE(4, 8)
		ENDWHILE
	ENDIF
	
	SWITCH iRand
		CASE 0																	RETURN "ENTER_BARS"		// Deliberate repeats.
		CASE 1																	RETURN "ENTER_BARS"		
		CASE 2																	RETURN "ENTER_BARS"		
		CASE 3																	RETURN "ENTER_BARS"
		CASE 4		sMissionPedsLocalVars[iPed].iRandomAnimLastPlayed = iRand	RETURN "Im_An_Englishman"
		CASE 5		sMissionPedsLocalVars[iPed].iRandomAnimLastPlayed = iRand	RETURN "Kill_For_A_Fag"
		CASE 6		sMissionPedsLocalVars[iPed].iRandomAnimLastPlayed = iRand	RETURN "Im_A_Brit"
		CASE 7		sMissionPedsLocalVars[iPed].iRandomAnimLastPlayed = iRand	RETURN "Mind_Sharper_Than_Ever"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC STRING GET_PED_SYNC_SCENE_ENGLISH_DAVE_PACING_ANIMATION(INT iPed, BOOL bForceBreakout)
	INT iRand = GET_RANDOM_INT_IN_RANGE(0, 19)
	WHILE iRand = sMissionPedsLocalVars[iPed].iRandomAnimLastPlayed
		iRand = GET_RANDOM_INT_IN_RANGE(0, 19)
	ENDWHILE
	
	IF bForceBreakout
		iRand = GET_RANDOM_INT_IN_RANGE(6, 19)
		WHILE iRand = sMissionPedsLocalVars[iPed].iRandomAnimLastPlayed
			iRand = GET_RANDOM_INT_IN_RANGE(6, 19)
		ENDWHILE
	ENDIF
	
	SWITCH iRand
		CASE 0																	RETURN "BASE_IDLE"
		CASE 1																	RETURN "IDLE_A"
		CASE 2																	RETURN "IDLE_A"
		CASE 3																	RETURN "BASE_IDLE"
		CASE 4																	RETURN "IDLE_B"
		CASE 5																	RETURN "IDLE_B"
		CASE 6																	RETURN "BASE_IDLE"
		CASE 7																	RETURN "IDLE_A"
		CASE 8																	RETURN "IDLE_A"
		CASE 9																	RETURN "BASE_IDLE"
		CASE 10																	RETURN "IDLE_B"
		CASE 11																	RETURN "IDLE_B"
		CASE 12		sMissionPedsLocalVars[iPed].iRandomAnimLastPlayed = iRand	RETURN "We're_All_His_Children"
		CASE 13		sMissionPedsLocalVars[iPed].iRandomAnimLastPlayed = iRand	RETURN "I'm_Sorry_Daddy"
		CASE 14		sMissionPedsLocalVars[iPed].iRandomAnimLastPlayed = iRand	RETURN "It's_Fucked"
		CASE 15		sMissionPedsLocalVars[iPed].iRandomAnimLastPlayed = iRand	RETURN "Cage_Ain't_Keeping_Me_In"
		CASE 16		sMissionPedsLocalVars[iPed].iRandomAnimLastPlayed = iRand	RETURN "Left_Right"
		CASE 17		sMissionPedsLocalVars[iPed].iRandomAnimLastPlayed = iRand	RETURN "Marching_Powder"
		CASE 18		sMissionPedsLocalVars[iPed].iRandomAnimLastPlayed = iRand	RETURN "Don't_Help_Me"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC STRING GET_PED_SYNC_SCENE_ENGLISH_DAVE_PANTHER_ESCAPED_ANIMATION(INT iPed, BOOL bForceBreakout)
	INT iRand = GET_RANDOM_INT_IN_RANGE(0, 15)	
	WHILE iRand = sMissionPedsLocalVars[iPed].iRandomAnimLastPlayed 
		iRand = GET_RANDOM_INT_IN_RANGE(0, 15)
	ENDWHILE
	
	IF bForceBreakout
		iRand = GET_RANDOM_INT_IN_RANGE(11, 15)
		WHILE iRand = sMissionPedsLocalVars[iPed].iRandomAnimLastPlayed 
			iRand = GET_RANDOM_INT_IN_RANGE(11, 15)
		ENDWHILE
	ENDIF
	
	PRINTLN("[LM][RCC MISSION][OWNER][Sync_anims: ", iPed, "] - GET_PED_SYNC_SCENE_ENGLISH_DAVE_PANTHER_ESCAPED_ANIMATION - iRand: ", iRand)
	
	SWITCH iRand
		CASE 0																	RETURN "BASE_IDLE" // deliberate
		CASE 1																	RETURN "BASE_IDLE"
		CASE 2																	RETURN "IDLE_A"
		CASE 3																	RETURN "IDLE_B"
		CASE 4																	RETURN "IDLE_C"
		CASE 5																	RETURN "IDLE_D"
		CASE 6																	RETURN "IDLE_A"
		CASE 7																	RETURN "IDLE_B"
		CASE 8																	RETURN "IDLE_C"
		CASE 9																	RETURN "IDLE_D"
		CASE 10		sMissionPedsLocalVars[iPed].iRandomAnimLastPlayed = iRand	RETURN "Here_Kitty_Kitty"
		CASE 11		sMissionPedsLocalVars[iPed].iRandomAnimLastPlayed = iRand	RETURN "Where_Is_The_Fucking_Panther"
		CASE 12		sMissionPedsLocalVars[iPed].iRandomAnimLastPlayed = iRand	RETURN "Turn_Around"
		CASE 13		sMissionPedsLocalVars[iPed].iRandomAnimLastPlayed = iRand	RETURN "MEOW_HERE_PUSSY_PUSS"
		CASE 14		sMissionPedsLocalVars[iPed].iRandomAnimLastPlayed = iRand	RETURN "WHAT_DAY_IS_IT"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC TEXT_LABEL_63 GET_PED_SYNC_SCENE_DICTIONARY_FROM_SYNC_SCENE_ANIMATION(INT iAnim, TEXT_LABEL_63 sAnim, TEXT_LABEL_63 sDict)
	SWITCH iAnim
		CASE ciPED_IDLE_ANIM__ENGLISH_DAVE_IN_A_CAGE_MEDITATING
			IF ARE_STRINGS_EQUAL(sAnim, "Need_A_Mantra")
			OR ARE_STRINGS_EQUAL(sAnim, "Ohm_Fucking_Ohm")
			OR ARE_STRINGS_EQUAL(sAnim, "Everythings_Cool")
			OR ARE_STRINGS_EQUAL(sAnim, "Bring_It_Down_Baby")
			OR ARE_STRINGS_EQUAL(sAnim, "A_God_A_Deity")
			OR ARE_STRINGS_EQUAL(sAnim, "He_Sees_Everything")
			OR ARE_STRINGS_EQUAL(sAnim, "Rob_A_Genius")
			OR ARE_STRINGS_EQUAL(sAnim, "Sun_Moon_And_Stars")
			OR ARE_STRINGS_EQUAL(sAnim, "Im_An_Englishman")
			OR ARE_STRINGS_EQUAL(sAnim, "Kill_For_A_Fag")
			OR ARE_STRINGS_EQUAL(sAnim, "Im_A_Brit")
			OR ARE_STRINGS_EQUAL(sAnim, "Mind_Sharper_Than_Ever")
				sDict = "ANIM@SCRIPTED@ISLAND@SPECIAL_PEDS@DAVE@HS4_DAVE_IG1"
			ENDIF
			IF ARE_STRINGS_EQUAL(sAnim, "ENTER_BARS")
				sDict = "ANIM@SCRIPTED@ISLAND@SPECIAL_PEDS@DAVE@HS4_DAVE_IG1_YOGA"
			ENDIF			
			IF ARE_STRINGS_EQUAL(sAnim, "ENTER_YOGA")
				sDict = "ANIM@SCRIPTED@ISLAND@SPECIAL_PEDS@DAVE@HS4_DAVE_IG1_BARS"
			ENDIF
		BREAK
		
		CASE ciPED_IDLE_ANIM__ENGLISH_DAVE_IN_A_CAGE_PACING
		BREAK
		
		CASE ciPED_IDLE_ANIM__ENGLISH_DAVE_IN_A_CAGE_PANTHER_SCARY
		BREAK
	ENDSWITCH
	RETURN sDict
ENDFUNC

FUNC TEXT_LABEL_63  GET_PED_SYNC_SCENE_VOICE_NAME_FROM_SYNC_SCENE_ANIMATION(INT iAnim, TEXT_LABEL_63 sAnim)
	TEXT_LABEL_63 tl63_Return
	SWITCH iAnim
		CASE ciPED_IDLE_ANIM__ENGLISH_DAVE_IN_A_CAGE_MEDITATING
			IF ARE_STRINGS_EQUAL(sAnim, "Need_A_Mantra")
				tl63_Return = "BTL_DAVE"
			ELIF ARE_STRINGS_EQUAL(sAnim, "Ohm_Fucking_Ohm")
				tl63_Return = "BTL_DAVE"
			ELIF ARE_STRINGS_EQUAL(sAnim, "Everythings_Cool")
				tl63_Return = "BTL_DAVE"
			ELIF ARE_STRINGS_EQUAL(sAnim, "Bring_It_Down_Baby")
				tl63_Return = "BTL_DAVE"
			ELIF ARE_STRINGS_EQUAL(sAnim, "A_God_A_Deity")
				tl63_Return = "BTL_DAVE"
			ELIF ARE_STRINGS_EQUAL(sAnim, "He_Sees_Everything")
				tl63_Return = "BTL_DAVE"
			ELIF ARE_STRINGS_EQUAL(sAnim, "Rob_A_Genius")
				tl63_Return = "BTL_DAVE"
			ELIF ARE_STRINGS_EQUAL(sAnim, "Sun_Moon_And_Stars")
				tl63_Return = "BTL_DAVE"
			ELIF ARE_STRINGS_EQUAL(sAnim, "Im_An_Englishman")
				tl63_Return = "BTL_DAVE"
			ELIF ARE_STRINGS_EQUAL(sAnim, "Kill_For_A_Fag")
				tl63_Return = "BTL_DAVE"
			ELIF ARE_STRINGS_EQUAL(sAnim, "Im_A_Brit")
				tl63_Return = "BTL_DAVE"
			ELIF ARE_STRINGS_EQUAL(sAnim, "Mind_Sharper_Than_Ever")
				tl63_Return = "BTL_DAVE"
			ENDIF
		BREAK
		
		CASE ciPED_IDLE_ANIM__ENGLISH_DAVE_IN_A_CAGE_PACING
			IF ARE_STRINGS_EQUAL(sAnim, "We're_All_His_Children")
				tl63_Return = "BTL_DAVE"
			ELIF ARE_STRINGS_EQUAL(sAnim, "Cage_Ain't_Keeping_Me_In")
				tl63_Return = "BTL_DAVE"
			ELIF ARE_STRINGS_EQUAL(sAnim, "Don't_Help_Me")
				tl63_Return = "BTL_DAVE"
			ELIF ARE_STRINGS_EQUAL(sAnim, "I'm_Sorry_Daddy")
				tl63_Return = "BTL_DAVE"
			ELIF ARE_STRINGS_EQUAL(sAnim, "It's_Fucked")
				tl63_Return = "BTL_DAVE"
			ELIF ARE_STRINGS_EQUAL(sAnim, "Left_Right")
				tl63_Return = "BTL_DAVE"
			ELIF ARE_STRINGS_EQUAL(sAnim, "Marching_Powder")
				tl63_Return = "BTL_DAVE"
			ENDIF
		BREAK
		
		CASE ciPED_IDLE_ANIM__ENGLISH_DAVE_IN_A_CAGE_PANTHER_SCARY
			IF ARE_STRINGS_EQUAL(sAnim, "Here_Kitty_Kitty")
				tl63_Return = "BTL_DAVE"
			ELIF ARE_STRINGS_EQUAL(sAnim, "Where_Is_The_Fucking_Panther")
				tl63_Return = "BTL_DAVE"		
			ELIF ARE_STRINGS_EQUAL(sAnim, "Turn_Around")
				tl63_Return = "BTL_DAVE"
			ELIF ARE_STRINGS_EQUAL(sAnim, "MEOW_HERE_PUSSY_PUSS")
				tl63_Return = "BTL_DAVE"
			ENDIF
		BREAK
		
		CASE ciPED_IDLE_ANIM__CHOPPER_CRAWL
			tl63_Return = "FIX_RICH1"
		BREAK
		CASE ciPED_IDLE_ANIM__STU_DRE_INSTRUCTIONS
			tl63_Return = "HS4_PRODUCER"
		BREAK
	ENDSWITCH
	
	RETURN tl63_Return
ENDFUNC

FUNC TEXT_LABEL_63  GET_PED_SYNC_SCENE_ROOT_FROM_SYNC_SCENE_ANIMATION(INT iAnim, TEXT_LABEL_63 sAnim)
	TEXT_LABEL_63 tl63_Return
	SWITCH iAnim
		CASE ciPED_IDLE_ANIM__ENGLISH_DAVE_IN_A_CAGE_MEDITATING
			IF ARE_STRINGS_EQUAL(sAnim, "Need_A_Mantra")
				tl63_Return = "HS4FI_IG1_1A"
			ELIF ARE_STRINGS_EQUAL(sAnim, "Ohm_Fucking_Ohm")
				tl63_Return = "HS4FI_IG1_1B"
			ELIF ARE_STRINGS_EQUAL(sAnim, "Everythings_Cool")
				tl63_Return = "HS4FI_IG1_1C"
			ELIF ARE_STRINGS_EQUAL(sAnim, "Bring_It_Down_Baby")
				tl63_Return = "HS4FI_IG1_1D"
			ELIF ARE_STRINGS_EQUAL(sAnim, "A_God_A_Deity")
				tl63_Return = "HS4FI_IG1_2C"
			ELIF ARE_STRINGS_EQUAL(sAnim, "He_Sees_Everything")
				tl63_Return = "HS4FI_IG1_2B"
			ELIF ARE_STRINGS_EQUAL(sAnim, "Rob_A_Genius")
				tl63_Return = "HS4FI_IG1_2D"
			ELIF ARE_STRINGS_EQUAL(sAnim, "Sun_Moon_And_Stars")
				tl63_Return = "HS4FI_IG1_2A"
			ELIF ARE_STRINGS_EQUAL(sAnim, "Im_An_Englishman")
				tl63_Return = "HS4FI_IG1_1E"
			ELIF ARE_STRINGS_EQUAL(sAnim, "Kill_For_A_Fag")
				tl63_Return = "HS4FI_IG1_1F"
			ELIF ARE_STRINGS_EQUAL(sAnim, "Im_A_Brit")
				tl63_Return = "HS4FI_IG1_1H"
			ELIF ARE_STRINGS_EQUAL(sAnim, "Mind_Sharper_Than_Ever")
				tl63_Return = "HS4FI_IG1_1G"
			ENDIF
		BREAK
		
		CASE ciPED_IDLE_ANIM__ENGLISH_DAVE_IN_A_CAGE_PACING
			IF ARE_STRINGS_EQUAL(sAnim, "We're_All_His_Children")
				tl63_Return = "HS4FI_IG2_1D"
			ELIF ARE_STRINGS_EQUAL(sAnim, "Cage_Ain't_Keeping_Me_In")
				tl63_Return = "HS4FI_IG2_1B"
			ELIF ARE_STRINGS_EQUAL(sAnim, "Don't_Help_Me")
				tl63_Return = "HS4FI_IG2_2C"
			ELIF ARE_STRINGS_EQUAL(sAnim, "I'm_Sorry_Daddy")
				tl63_Return = "HS4FI_IG2_1C"
			ELIF ARE_STRINGS_EQUAL(sAnim, "It's_Fucked")
				tl63_Return = "HS4FI_IG2_1A"
			ELIF ARE_STRINGS_EQUAL(sAnim, "Left_Right")
				tl63_Return = "HS4FI_IG2_2B"
			ELIF ARE_STRINGS_EQUAL(sAnim, "Marching_Powder")
				tl63_Return = "HS4FI_IG2_2A"
			ENDIF
		BREAK
		
		CASE ciPED_IDLE_ANIM__ENGLISH_DAVE_IN_A_CAGE_PANTHER_SCARY
			IF ARE_STRINGS_EQUAL(sAnim, "Here_Kitty_Kitty")
				tl63_Return = "HS4FI_IG3_C"
			ELIF ARE_STRINGS_EQUAL(sAnim, "Where_Is_The_Fucking_Panther")
				tl63_Return = "HS4FI_IG3_B"		
			ELIF ARE_STRINGS_EQUAL(sAnim, "Turn_Around")
				tl63_Return = "HS4FI_IG3_A"
			ELIF ARE_STRINGS_EQUAL(sAnim, "MEOW_HERE_PUSSY_PUSS")
				tl63_Return = "HS4FI_IG3_D"
			ENDIF
		BREAK
		
		CASE ciPED_IDLE_ANIM__CHOPPER_CRAWL
			tl63_Return = "FXIG_BIL2_1A"
		BREAK
		CASE ciPED_IDLE_ANIM__STU_DRE_INSTRUCTIONS				
			// Start.
			IF ARE_STRINGS_EQUAL(sAnim, "STAGE_01_ANIM_01")
				tl63_Return = "FXIG_ST1_1A"
				
			// Stage 1
			ELIF ARE_STRINGS_EQUAL(sAnim, "STAGE_01_ANIM_02")
				tl63_Return = "FXIG_ST1_1B"
				
			// Stage 2
			ELIF ARE_STRINGS_EQUAL(sAnim, "STAGE_02_ANIM_01")
				tl63_Return = ""
			
			ELIF ARE_STRINGS_EQUAL(sAnim, "STAGE_02_ANIM_02")
				tl63_Return = "FXIG_ST1_1B"
								
			ELIF ARE_STRINGS_EQUAL(sAnim, "STAGE_02_ANIM_03")
				tl63_Return = "FXIG_ST1_1C"
				
			// Stage 3 
			ELIF ARE_STRINGS_EQUAL(sAnim, "STAGE_03_ANIM_01")
				tl63_Return = ""
			
			ELIF ARE_STRINGS_EQUAL(sAnim, "STAGE_03_ANIM_02")
				tl63_Return = "FXIG_ST1_1B"
				
			ELIF ARE_STRINGS_EQUAL(sAnim, "STAGE_03_ANIM_03")
				tl63_Return = "FXIG_ST1_1C"
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN tl63_Return
ENDFUNC

FUNC TEXT_LABEL_63  GET_PED_SYNC_SCENE_CONTEXT_FROM_SYNC_SCENE_ANIMATION(INT iAnim, TEXT_LABEL_63 sAnim)
	TEXT_LABEL_63 tl63_Return
	SWITCH iAnim
		CASE ciPED_IDLE_ANIM__ENGLISH_DAVE_IN_A_CAGE_MEDITATING
			IF ARE_STRINGS_EQUAL(sAnim, "Need_A_Mantra")
				tl63_Return = "HS4FI_ALAA"
			ELIF ARE_STRINGS_EQUAL(sAnim, "Ohm_Fucking_Ohm")
				tl63_Return = "HS4FI_AMAA"
			ELIF ARE_STRINGS_EQUAL(sAnim, "Everythings_Cool")
				tl63_Return = "HS4FI_ANAA"
			ELIF ARE_STRINGS_EQUAL(sAnim, "Bring_It_Down_Baby")
				tl63_Return = "HS4FI_AOAA"
			ELIF ARE_STRINGS_EQUAL(sAnim, "A_God_A_Deity")
				tl63_Return = "HS4FI_AVAA"
			ELIF ARE_STRINGS_EQUAL(sAnim, "He_Sees_Everything")
				tl63_Return = "HS4FI_AUAA"
			ELIF ARE_STRINGS_EQUAL(sAnim, "Rob_A_Genius")
				tl63_Return = "HS4FI_AWAA"
			ELIF ARE_STRINGS_EQUAL(sAnim, "Sun_Moon_And_Stars")
				tl63_Return = "HS4FI_ATAA"
			ELIF ARE_STRINGS_EQUAL(sAnim, "Im_An_Englishman")
				tl63_Return = "HS4FI_APAA"
			ELIF ARE_STRINGS_EQUAL(sAnim, "Kill_For_A_Fag")
				tl63_Return = "HS4FI_AQAA"
			ELIF ARE_STRINGS_EQUAL(sAnim, "Im_A_Brit")
				tl63_Return = "HS4FI_ASAA"
			ELIF ARE_STRINGS_EQUAL(sAnim, "Mind_Sharper_Than_Ever")
				tl63_Return = "HS4FI_ARAA"
			ENDIF
		BREAK
		
		CASE ciPED_IDLE_ANIM__ENGLISH_DAVE_IN_A_CAGE_PACING
			IF ARE_STRINGS_EQUAL(sAnim, "We're_All_His_Children")
				tl63_Return = "HS4FI_ADAA"
			ELIF ARE_STRINGS_EQUAL(sAnim, "Cage_Ain't_Keeping_Me_In")
				tl63_Return = "HS4FI_ABAA"
			ELIF ARE_STRINGS_EQUAL(sAnim, "Don't_Help_Me")
				tl63_Return = "HS4FI_AGAA"
			ELIF ARE_STRINGS_EQUAL(sAnim, "I'm_Sorry_Daddy")
				tl63_Return = "HS4FI_ACAA"
			ELIF ARE_STRINGS_EQUAL(sAnim, "It's_Fucked")
				tl63_Return = "HS4FI_AAAA"
			ELIF ARE_STRINGS_EQUAL(sAnim, "Left_Right")
				tl63_Return = "HS4FI_AFAA"	
			ELIF ARE_STRINGS_EQUAL(sAnim, "Marching_Powder")
				tl63_Return = "HS4FI_AEAA"
			ENDIF
		BREAK
		
		CASE ciPED_IDLE_ANIM__ENGLISH_DAVE_IN_A_CAGE_PANTHER_SCARY
			IF ARE_STRINGS_EQUAL(sAnim, "Here_Kitty_Kitty")
				tl63_Return = "HS4FI_AJAA"
			ELIF ARE_STRINGS_EQUAL(sAnim, "Where_Is_The_Fucking_Panther")
				tl63_Return = "HS4FI_AIAA"	
			ELIF ARE_STRINGS_EQUAL(sAnim, "Turn_Around")
				tl63_Return = "HS4FI_AHAA"
			ELIF ARE_STRINGS_EQUAL(sAnim, "MEOW_HERE_PUSSY_PUSS")
				tl63_Return = "HS4FI_AKAA"
			ENDIF
		BREAK
		
		CASE ciPED_IDLE_ANIM__CHOPPER_CRAWL
			IF ARE_STRINGS_EQUAL(sAnim, "FIX_BIL_IG2_CHOPPER_CRAWL_LOOP_PED0")
				tl63_Return = "FXIG_DUAA"
			ELIF ARE_STRINGS_EQUAL(sAnim, "FIX_BIL_IG2_CHOPPER_CRAWL_LOOP_PED1")
				tl63_Return = "FXIG_DUAB"
			ELIF ARE_STRINGS_EQUAL(sAnim, "FIX_BIL_IG2_CHOPPER_CRAWL_LOOP_PED2")
				tl63_Return = "FXIG_DUAC"
			ELIF ARE_STRINGS_EQUAL(sAnim, "FIX_BIL_IG2_CHOPPER_CRAWL_LOOP_PED3")
				tl63_Return = "FXIG_DUAD"
			ELIF ARE_STRINGS_EQUAL(sAnim, "FIX_BIL_IG2_CHOPPER_CRAWL_LOOP_PED4")
				tl63_Return = "FXIG_DUAE"
			ELIF ARE_STRINGS_EQUAL(sAnim, "FIX_BIL_IG2_CHOPPER_CRAWL_LOOP_PED5")
				tl63_Return = "FXIG_DUAF"
			ELIF ARE_STRINGS_EQUAL(sAnim, "FIX_BIL_IG2_CHOPPER_CRAWL_LOOP_PED6")
				tl63_Return = "FXIG_DUAG"
			ELIF ARE_STRINGS_EQUAL(sAnim, "FIX_BIL_IG2_CHOPPER_CRAWL_LOOP_PED7")
				tl63_Return = "FXIG_DUAH"
			ELIF ARE_STRINGS_EQUAL(sAnim, "FIX_BIL_IG2_CHOPPER_CRAWL_LOOP_PED8")
				tl63_Return = "FXIG_DUAI"
			ELIF ARE_STRINGS_EQUAL(sAnim, "FIX_BIL_IG2_CHOPPER_CRAWL_LOOP_PED9")
				tl63_Return = "FXIG_DUAJ"
			ENDIF
		BREAK
		
		CASE ciPED_IDLE_ANIM__STU_DRE_INSTRUCTIONS
			// Start.
			IF ARE_STRINGS_EQUAL(sAnim, "STAGE_01_ANIM_01_0")
				tl63_Return = "FXIG_ACAA"
			ELIF ARE_STRINGS_EQUAL(sAnim, "STAGE_01_ANIM_01_1")
				tl63_Return = "FXIG_ACAB"
				
			// Stage 1
			ELIF ARE_STRINGS_EQUAL(sAnim, "STAGE_01_ANIM_02_0")
				tl63_Return = "FXIG_BVAA_01"
			ELIF ARE_STRINGS_EQUAL(sAnim, "STAGE_01_ANIM_02_1")
				tl63_Return = "FXIG_BVAA_02"
			
			ELIF ARE_STRINGS_EQUAL(sAnim, "STAGE_01_ANIM_03_0")
				tl63_Return = ""
				
				
			// Stage 2
			ELIF ARE_STRINGS_EQUAL(sAnim, "STAGE_02_ANIM_01_0")
				tl63_Return = ""
			
			ELIF ARE_STRINGS_EQUAL(sAnim, "STAGE_02_ANIM_02_0")
				tl63_Return = "FXIG_BVAA_04"
			ELIF ARE_STRINGS_EQUAL(sAnim, "STAGE_02_ANIM_02_1")
				tl63_Return = "FXIG_BVAA_05"
								
			ELIF ARE_STRINGS_EQUAL(sAnim, "STAGE_02_ANIM_03_0")
				tl63_Return = "FXIG_BWAA_01"
			ELIF ARE_STRINGS_EQUAL(sAnim, "STAGE_02_ANIM_03_1")
				tl63_Return = "FXIG_BWAA_02"
			ELIF ARE_STRINGS_EQUAL(sAnim, "STAGE_02_ANIM_03_2")
				tl63_Return = "FXIG_BWAA_03"
			ELIF ARE_STRINGS_EQUAL(sAnim, "STAGE_02_ANIM_03_3")
				tl63_Return = "FXIG_BWAA_04"
				
			// Stage 3 
			ELIF ARE_STRINGS_EQUAL(sAnim, "STAGE_03_ANIM_01_0")
				tl63_Return = ""
			
			ELIF ARE_STRINGS_EQUAL(sAnim, "STAGE_03_ANIM_02_0")
				tl63_Return = "FXIG_BVAA_06"
			ELIF ARE_STRINGS_EQUAL(sAnim, "STAGE_03_ANIM_02_1")
				tl63_Return = "FXIG_BVAA_07"
				
			ELIF ARE_STRINGS_EQUAL(sAnim, "STAGE_03_ANIM_03_0")
				tl63_Return = "FXIG_BWAA_05"
			ELIF ARE_STRINGS_EQUAL(sAnim, "STAGE_03_ANIM_03_1")
				tl63_Return = "FXIG_BWAA_06"
			ELIF ARE_STRINGS_EQUAL(sAnim, "STAGE_03_ANIM_03_2")
				tl63_Return = "FXIG_BWAA_07"
			ELIF ARE_STRINGS_EQUAL(sAnim, "STAGE_03_ANIM_03_3")
				tl63_Return = "FXIG_BWAA_08"
			ENDIF
			
		BREAK
	ENDSWITCH
	
	RETURN tl63_Return
ENDFUNC

FUNC STRING GET_PED_CUSTOM_VEHICLE_CRASH_REACT_DICT(INT iPed)
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].mn = INT_TO_ENUM(MODEL_NAMES, HASH("p_franklin_02"))
		RETURN "anim@scripted@data_leak@fix_golf_ig1_franklin_golfbuggy@"
	ENDIF
	
	RETURN ""
ENDFUNC

FUNC STRING GET_PED_CUSTOM_VEHICLE_CRASH_REACT_ANIM_NAME(FMMC_PED_STATE &sPedState)
	IF sPedState.mn = INT_TO_ENUM(MODEL_NAMES, HASH("p_franklin_02"))
		SWITCH GET_RANDOM_INT_IN_RANGE(0, 8)
			CASE 0	RETURN "Line_07"
			CASE 1	RETURN "Line_08"
			CASE 2	RETURN "Line_09"
			CASE 3	RETURN "Line_10"
			CASE 4	RETURN "Line_11"
			CASE 5	RETURN "Line_12"
			CASE 6	RETURN "Line_13"
			CASE 7	RETURN "Line_14"
		ENDSWITCH
	ENDIF
	
	RETURN "INVALID"
ENDFUNC

FUNC STRING GET_PED_CUSTOM_VEHICLE_CRASH_REACT_VOICE_LINE(FMMC_PED_STATE &sPedState)
	IF sPedState.mn = INT_TO_ENUM(MODEL_NAMES, HASH("p_franklin_02"))
		SWITCH GET_RANDOM_INT_IN_RANGE(0, 14)
			CASE 0	RETURN "FXIG_AAAA"
			CASE 1	RETURN "FXIG_AVAA"
			CASE 2	RETURN "FXIG_AWAA"
			CASE 3	RETURN "FXIG_AXAA"
			CASE 4	RETURN "FXIG_AYAA"
			CASE 5	RETURN "FXIG_AZAA"
			CASE 6	RETURN "FXIG_BAAA"
			CASE 7	RETURN "FXIG_BBAA"
			CASE 8	RETURN "FXIG_BCAA"
			CASE 9	RETURN "FXIG_BDAA"
			CASE 10	RETURN "FXIG_BEAA"
			CASE 11	RETURN "FXIG_BFAA"
			CASE 12	RETURN "FXIG_BGAA"
			CASE 13	RETURN "FXIG_BHAA"
		ENDSWITCH
	ENDIF
	
	RETURN ""
ENDFUNC

FUNC STRING GET_PED_CUSTOM_VEHICLE_CRASH_REACT_VOICE_NAME(FMMC_PED_STATE &sPedState)
	IF sPedState.mn = INT_TO_ENUM(MODEL_NAMES, HASH("p_franklin_02"))
		RETURN "FIX_FRANKLIN"
	ENDIF
	
	RETURN ""
ENDFUNC

PROC CLEANUP_GOLF_CLUB_FROM_PED_ANIM(BOOL bDelete = TRUE)
	IF NETWORK_DOES_NETWORK_ID_EXIST(niGolfClub)
	AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(niGolfClub)
		OBJECT_INDEX oiGolfClub = NET_TO_OBJ(niGolfClub)
		IF bDelete
			DELETE_OBJECT(oiGolfClub)
		ELSE
			SET_OBJECT_AS_NO_LONGER_NEEDED(oiGolfClub)
		ENDIF
		PRINTLN("CLEANUP_GOLF_CLUB_FROM_PED_ANIM | Delete niGolfClub Object")
	ENDIF
ENDPROC

PROC UNLOAD_MISSION_SPECIFIC_ANIMS()
	PRINTLN("[RCC MISSION] MC_SCRIPT_CLEANUP - UNLOAD_MISSION_SPECIFIC_ANIMS ################################")
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_PhoneEMPAvailable_CasinoPrep)
		REMOVE_ANIM_DICT("AMB@CODE_HUMAN_POLICE_INVESTIGATE@idle_a")
		REMOVE_ANIM_DICT("AMB@CODE_HUMAN_POLICE_INVESTIGATE@idle_b")
		REMOVE_ANIM_DICT("anim@weapons@flashlight@stealth")
	ENDIF
	
	IF IS_BIT_SET(sInteractWithVars.iInteractWith_Bitset, ciInteractWith_Bitset__HidPlayerWeaponForEndCutscene)
		SET_PED_CURRENT_WEAPON_VISIBLE(LocalPlayerPed, TRUE, FALSE)
		PRINTLN("[InteractWith] Setting player's weapon to be visible in cleanup due to ciInteractWith_Bitset__HidPlayerWeaponForEndCutscene")
	ENDIF
	
	INT iPed
	FOR iPed = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfPeds - 1
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetEighteen, ciPed_BSEighteen_CustomReactToCrashingIntoEnemyVehicle)
			PRINTLN("[Peds][Ped ", iPed, "] UNLOAD_MISSION_SPECIFIC_ANIMS | Releasing anim dict ", GET_PED_CUSTOM_VEHICLE_CRASH_REACT_DICT(iPed))
			REMOVE_ANIM_DICT(GET_PED_CUSTOM_VEHICLE_CRASH_REACT_DICT(iPed))
		ENDIF
	ENDFOR
	
	IF IS_BIT_SET(iLocalBoolCheck31, LBOOL31_APPLIED_CUSTOM_ZONE_WALK_STYLE)
		UNLOAD_ALL_WALK_STYLE_CLIPSETS()
	ENDIF
	
	// Created from Anims ---
	CLEANUP_GOLF_CLUB_FROM_PED_ANIM()
ENDPROC

PROC REMOVE_REQUESTED_MC_ASSETS()
	
	REMOVE_INTRO_ANIM(jobIntroData)
		
	CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Cleaning up requested assets before we do the end cutscene" )
ENDPROC

FUNC BOOL LOAD_ACTION_ANIMS()


	REQUEST_ACTION_MODE_ASSET("TREVOR_ACTION")
	IF NOT HAS_ACTION_MODE_ASSET_LOADED("TREVOR_ACTION")
		PRINTLN(" LOAD_ACTION_ANIMS - requesting TREVOR_ACTION")
		RETURN FALSE
	ENDIF
	
	REQUEST_ACTION_MODE_ASSET("MICHAEL_ACTION")
	IF NOT HAS_ACTION_MODE_ASSET_LOADED("MICHAEL_ACTION")
		PRINTLN(" LOAD_ACTION_ANIMS - requesting MICHAEL_ACTION")
		RETURN FALSE
	ENDIF
	
	REQUEST_ACTION_MODE_ASSET("FRANKLIN_ACTION")
	IF NOT HAS_ACTION_MODE_ASSET_LOADED("FRANKLIN_ACTION")
		PRINTLN(" LOAD_ACTION_ANIMS - requesting FRANKLIN_ACTION")
		RETURN FALSE
	ENDIF	

	RETURN TRUE

ENDFUNC

FUNC STRING GET_PED_CUSTOM_VEHICLE_CONTEXT_CLIPSET(MODEL_NAMES mnPedModel, VEHICLE_INDEX viPedVehicle)

	IF mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("p_franklin_02"))
		IF GET_ENTITY_MODEL(viPedVehicle) = CADDY
			//RETURN "FRANKLIN CUSTOM CADDY CONTEXT CLIPSET"
		ENDIF
	ENDIF
	
	RETURN ""
ENDFUNC

FUNC STRING GET_PED_CUSTOM_VEHICLE_CONTEXT(MODEL_NAMES mnPedModel, VEHICLE_INDEX viPedVehicle)

	IF mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("p_franklin_02"))
		IF GET_ENTITY_MODEL(viPedVehicle) = CADDY
			//RETURN "FRANKLIN CUSTOM CADDY CONTEXT"
		ENDIF
	ENDIF
	
	RETURN ""
ENDFUNC

// ##### ------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Text -----------------------------------------------------------------------------------------------------
// ##### Description: Loading and unloading Text.  ------------------------------------------------------------------------------
// ##### ------------------------------------------------------------------------------------------------------------------------
// ##### ------------------------------------------------------------------------------------------------------------------------

FUNC BOOL LOAD_MC_TEXT_BLOCK()
	REQUEST_ADDITIONAL_TEXT("MC_PLAY", MISSION_TEXT_SLOT)
	REQUEST_ADDITIONAL_TEXT("HACK", MINIGAME_TEXT_SLOT)
	IF HAS_THIS_ADDITIONAL_TEXT_LOADED("MC_PLAY", MISSION_TEXT_SLOT)
	AND HAS_ADDITIONAL_TEXT_LOADED(MINIGAME_TEXT_SLOT)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

// Attempt to Pre-load Dialogue Trigger Text for Peds Ambient Voices.
FUNC BOOL LOAD_MC_DIALOGUE_TEXT_BLOCK()
	BOOL bTextReady = TRUE
	TEXT_LABEL_23 tl23 
	INT i = 0
	FOR i = 0 TO (g_FMMC_STRUCT.iNumberOfDialogueTriggers-1)
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[i].iDialogueBitset, iBS_DialogueHelpText)
		AND NOT IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[i].iDialogueBitset2, iBS_Dialogue2HelpTextNGOnly)
			IF NOT ARE_STRINGS_EQUAL(g_FMMC_STRUCT.sDialogueTriggers[i].tlBlock, g_FMMC_STRUCT.sDialogueTriggers[i].tlRoot)
			AND NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sDialogueTriggers[i].tlBlock)
				tl23 = g_FMMC_STRUCT.sDialogueTriggers[i].tlBlock
			ENDIF
			
			IF IS_STRING_NULL_OR_EMPTY(tl23)
			OR IS_STRING_BLANK_SPACES(tl23, GET_LENGTH_OF_LITERAL_STRING(tl23))
				tl23 = GET_DIALOGUE_BLOCK_FROM_ID(g_FMMC_STRUCT.sDialogueTriggers[i].iFileID)
			ENDIF
		ENDIF
		
		IF DOES_TEXT_BLOCK_EXIST(tl23)			
			IF IS_STRING_NULL_OR_EMPTY(tlt63_DialogueBlock1)
				PRINTLN("[RCC MISSION] [DLG] LOAD_MC_DIALOGUE_TEXT_BLOCK - Assigning sBlock: ", tl23, " to DLC_MISSION_DIALOGUE_TEXT_SLOT - (FIRST BLOCK)")
				tlt63_DialogueBlock1 = tl23
			ENDIF
			
			IF NOT IS_STRING_NULL_OR_EMPTY(tlt63_DialogueBlock1)
			AND NOT ARE_STRINGS_EQUAL(tl23, tlt63_DialogueBlock1)
			AND IS_STRING_NULL_OR_EMPTY(tlt63_DialogueBlock2)
				PRINTLN("[RCC MISSION] [DLG] LOAD_MC_DIALOGUE_TEXT_BLOCK - Assigning sBlock: ", tl23, " to DLC_MISSION_DIALOGUE_TEXT_SLOT2 - (SECOND BLOCK)")
				tlt63_DialogueBlock2 = tl23
			ENDIF
						
			IF ARE_STRINGS_EQUAL(tl23, tlt63_DialogueBlock1)			
				REQUEST_ADDITIONAL_TEXT_FOR_DLC(tl23, DLC_MISSION_DIALOGUE_TEXT_SLOT)
				IF HAS_THIS_ADDITIONAL_TEXT_LOADED(tl23, DLC_MISSION_DIALOGUE_TEXT_SLOT)
					PRINTLN("[RCC MISSION] [DLG] LOAD_MC_DIALOGUE_TEXT_BLOCK - HAS_THIS_ADDITIONAL_TEXT_LOADED(", tl23, ", DLC_MISSION_DIALOGUE_TEXT_SLOT) TRUE")					
				ELSE
					bTextReady = FALSE
					PRINTLN("[RCC MISSION] [DLG] LOAD_MC_DIALOGUE_TEXT_BLOCK - REQUEST_ADDITIONAL_TEXT_FOR_DLC(", tl23, ", DLC_MISSION_DIALOGUE_TEXT_SLOT) Waiting")
				ENDIF
			ELIF ARE_STRINGS_EQUAL(tl23, tlt63_DialogueBlock2)
				REQUEST_ADDITIONAL_TEXT_FOR_DLC(tl23, DLC_MISSION_DIALOGUE_TEXT_SLOT2)
				IF HAS_THIS_ADDITIONAL_TEXT_LOADED(tl23, DLC_MISSION_DIALOGUE_TEXT_SLOT2)
					PRINTLN("[RCC MISSION] [DLG] LOAD_MC_DIALOGUE_TEXT_BLOCK - HAS_THIS_ADDITIONAL_TEXT_LOADED(", tl23, ", DLC_MISSION_DIALOGUE_TEXT_SLOT2) TRUE")					
				ELSE
					bTextReady = FALSE
					PRINTLN("[RCC MISSION] [DLG] LOAD_MC_DIALOGUE_TEXT_BLOCK - REQUEST_ADDITIONAL_TEXT_FOR_DLC(", tl23, ", DLC_MISSION_DIALOGUE_TEXT_SLOT2) Waiting")
				ENDIF
			ELSE
				PRINTLN("[RCC MISSION] [DLG] LOAD_MC_DIALOGUE_TEXT_BLOCK - sBlock: ", tl23, " A THIRD TEXT BLOCK IS BEING USED FOR DIALOGUE, THIS SHOULD NOT BE HAPPENING NOT ATTEMPTING TEXT LOAD!")		
			ENDIF
			
		ELIF NOT IS_STRING_NULL_OR_EMPTY(tl23)
			#IF IS_DEBUG_BUILD
			SCRIPT_ASSERT("[RCC MISSION] [DLG] LOAD_MC_DIALOGUE_TEXT_BLOCK - INVALID NAME SET UP IN THE CREATOR - TEXT BLOCK DOES NOT EXIST IN FILES.")				
			ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_NON_EXISTENT_TEXT_BLOCK_LOAD, ENTITY_RUNTIME_ERROR_TYPE_CRITICAL_DIALOGUE_TRIGGER, "INVALID BLOCK SET UP IN THE CREATOR - DOES NOT EXIST IN FILES!", i)
			#ENDIF
		ENDIF
		
	ENDFOR
	
	RETURN bTextReady
ENDFUNC

FUNC INT GET_TUNER_VEHICLE_REGION_FROM_MODEL_NAME_FOR_MISSION(MODEL_NAMES mnVeh)
	SWITCH mnVeh
		CASE DOMINATOR7
		CASE DOMINATOR8
			RETURN ciDT_TUNER_VEHICLE_MUSCLE
		BREAK
		
		CASE COMET6
		CASE WARRENER2
		CASE GROWLER
		CASE TAILGATER2
		CASE CYPHER
			RETURN ciDT_TUNER_VEHICLE_EURO
		BREAK	
		
		CASE ZR350
		CASE CALICO
		CASE JESTER4
		CASE REMUS
		CASE SULTAN3
		CASE EUROS
		CASE PREVION
		CASE VECTRE
		CASE RT3000
		CASE FUTO2
			RETURN ciDT_TUNER_VEHICLE_JDM
		BREAK
	ENDSWITCH 
	
	RETURN -1
	
ENDFUNC

FUNC STRING GET_CHARACTER_TO_APPEND_FOR_TUNER_VEHICLE_REGION()
	
	IF IS_PED_INJURED(PlayerPedToUse)
		RETURN ""
	ENDIF
	
	IF NOT IS_PED_IN_ANY_VEHICLE(PlayerPedToUse, TRUE)
		RETURN ""
	ENDIF
	
	VEHICLE_INDEX viVeh = GET_VEHICLE_PED_IS_IN(PlayerPedToUse)
	
	IF NOT IS_VEHICLE_DRIVEABLE(viVeh)
		RETURN ""
	ENDIF
	
	MODEL_NAMES mnVeh = GET_ENTITY_MODEL(viVeh)
		
	SWITCH GET_TUNER_VEHICLE_REGION_FROM_MODEL_NAME_FOR_MISSION(mnVeh)
		CASE ciDT_TUNER_VEHICLE_MUSCLE
			RETURN "1"
		BREAK
		CASE ciDT_TUNER_VEHICLE_EURO
			RETURN "2"
		BREAK
		CASE ciDT_TUNER_VEHICLE_JDM
			RETURN "3"
		BREAK
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC STRING GET_CHARACTER_TO_APPEND_FOR_TUNER_VEHICLE_MODEL_NAME()

	IF IS_PED_INJURED(PlayerPedToUse)
		RETURN ""
	ENDIF
	
	IF NOT IS_PED_IN_ANY_VEHICLE(PlayerPedToUse, TRUE)
		RETURN ""
	ENDIF
	
	VEHICLE_INDEX viVeh = GET_VEHICLE_PED_IS_IN(PlayerPedToUse)
	
	IF NOT IS_VEHICLE_DRIVEABLE(viVeh)
		RETURN ""
	ENDIF
	
	MODEL_NAMES mnVeh = GET_ENTITY_MODEL(viVeh)
		
	SWITCH mnVeh
		CASE ZR350			RETURN "A"
		CASE CALICO			RETURN "B"
		CASE JESTER4		RETURN "C"
		CASE REMUS			RETURN "D"
		CASE COMET6			RETURN "E"
		CASE SULTAN3		RETURN "F"
		CASE EUROS			RETURN "G"
		CASE PREVION		RETURN "H"
		CASE VECTRE			RETURN "I"
		CASE WARRENER2		RETURN "J"
		CASE RT3000			RETURN "K"
		CASE GROWLER		RETURN "L"
		CASE TAILGATER2		RETURN "M"
		CASE CYPHER			RETURN "N"
		CASE DOMINATOR7		RETURN "O"
		CASE FUTO2			RETURN "P"
		CASE DOMINATOR8		RETURN "Q"		
	ENDSWITCH
		
	RETURN ""
ENDFUNC

// ##### ------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Models ---------------------------------------------------------------------------------------------------
// ##### Description: Loading and unloading specific models.  -------------------------------------------------------------------
// ##### ------------------------------------------------------------------------------------------------------------------------
// ##### ------------------------------------------------------------------------------------------------------------------------

PROC SET_VEHICLE_WEAPON_MODELS_THAT_SHOULD_LOAD()
	INT i
	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons i
		IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iVehicleWeaponPickupType > -1
			IF NOT IS_BIT_SET(iVehicleWepModelsToLoadBS, g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iVehicleWeaponPickupType)
				PRINTLN("[KH] SET_VEHICLE_WEAPON_MODELS_THAT_SHOULD_LOAD - Will load vehicle weapon model: ", g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iVehicleWeaponPickupType)
				SET_BIT(iVehicleWepModelsToLoadBS, g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iVehicleWeaponPickupType)
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

FUNC BOOL LOAD_ALL_VEHICLE_WEAPON_MODELS()
	BOOL bAllLoaded = TRUE
	
	IF iVehicleWepModelsToLoadBS = 0
		SET_VEHICLE_WEAPON_MODELS_THAT_SHOULD_LOAD()
	ENDIF
	
	IF IS_BIT_SET(iVehicleWepModelsToLoadBS, ciVEH_WEP_ROCKETS)
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Rock")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Rock")))
			bAllLoaded = FALSE
		ENDIF
		
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Rock_P")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Rock_P")))
			bAllLoaded = FALSE
		ENDIF
		
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Rock_P")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Rock_P")))
			bAllLoaded = FALSE
		ENDIF
		
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Rock_P")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Rock_P")))
			bAllLoaded = FALSE
		ENDIF
		
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Rock_PK")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Rock_PK")))
			bAllLoaded = FALSE
		ENDIF
		
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Rock_G")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Rock_G")))
			bAllLoaded = FALSE
		ENDIF
		
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Rock_WH")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Rock_WH")))
			bAllLoaded = FALSE
		ENDIF
		
	ENDIF
	
	IF IS_BIT_SET(iVehicleWepModelsToLoadBS, ciVEH_WEP_SPEED_BOOST)
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Boost")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Boost")))
			bAllLoaded = FALSE
		ENDIF
		
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Boost_P")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Boost_P")))
			bAllLoaded = FALSE
		ENDIF
		
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Boost_PK")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Boost_PK")))
			bAllLoaded = FALSE
		ENDIF
		
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Boost_G")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Boost_G")))
			bAllLoaded = FALSE
		ENDIF
		
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Boost_WH")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Boost_WH")))
			bAllLoaded = FALSE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iVehicleWepModelsToLoadBS, ciVEH_WEP_GHOST)
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Ghost")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Ghost")))
			bAllLoaded = FALSE
		ENDIF
		
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Ghost_P")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Ghost_P")))
			bAllLoaded = FALSE
		ENDIF
		
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Ghost_PK")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Ghost_PK")))
			bAllLoaded = FALSE
		ENDIF
		
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Ghost_G")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Ghost_G")))
			bAllLoaded = FALSE
		ENDIF
		
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Ghost_WH")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Ghost_WH")))
			bAllLoaded = FALSE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iVehicleWepModelsToLoadBS, ciVEH_WEP_BEAST)	
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Arm")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Arm")))
			bAllLoaded = FALSE
		ENDIF
		
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Arm_P")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Arm_P")))
			bAllLoaded = FALSE
		ENDIF

		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Arm_PK")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Arm_PK")))
			bAllLoaded = FALSE
		ENDIF
		
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Arm_G")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Arm_G")))
			bAllLoaded = FALSE
		ENDIF
		
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Arm_WH")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Arm_WH")))
			bAllLoaded = FALSE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iVehicleWepModelsToLoadBS, ciVEH_WEP_RANDOM_SPECIAL_VEH)
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Special_Vehicle")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Special_Vehicle")))
			bAllLoaded = FALSE
		ENDIF
		
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Special_Vehicle_P")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Special_Vehicle_P")))
			bAllLoaded = FALSE
		ENDIF

		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Special_Vehicle_PK")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Special_Vehicle_PK")))
			bAllLoaded = FALSE
		ENDIF
		
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Special_Vehicle_G")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Special_Vehicle_G")))
			bAllLoaded = FALSE
		ENDIF
		
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Special_Vehicle_WH")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Special_Vehicle_WH")))
			bAllLoaded = FALSE
		ENDIF
	ENDIF
	IF IS_BIT_SET(iVehicleWepModelsToLoadBS, ciVEH_WEP_EXTRA_LIFE)			
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_lifeinc")))
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_lifeincL")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_lifeinc")))
		OR NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_lifeincL")))
			bAllLoaded = FALSE
		ENDIF
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_lifeinc")))
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_lifeincL")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_lifeinc")))
		OR NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_lifeincL")))
			bAllLoaded = FALSE
		ENDIF
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_lifeinc_green")))
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_lifeinc_greenL")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_lifeinc_green")))
		OR NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_lifeinc_greenL")))
			bAllLoaded = FALSE
		ENDIF
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_lifeinc_orange")))
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_lifeinc_orngL")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_lifeinc_orange")))
		OR NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_lifeinc_orngL")))
			bAllLoaded = FALSE
		ENDIF
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_lifeinc_pink")))
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_lifeinc_pinkL")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_lifeinc_pink")))
		OR NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_lifeinc_pinkL")))
			bAllLoaded = FALSE
		ENDIF
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_lifeinc_purple")))
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_lifeinc_purpL")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_lifeinc_purple")))
		OR NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_lifeinc_purpL")))
			bAllLoaded = FALSE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iVehicleWepModelsToLoadBS, ciVEH_WEP_BOMB_LENGTH)
		
		
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_blastdec")))
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_blastdecL")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_blastdec")))
		OR NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_blastdecL")))
			bAllLoaded = FALSE
		ENDIF
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_blastinc")))
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_blastincL")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_blastinc")))
		OR NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_blastincL")))
			bAllLoaded = FALSE
		ENDIF
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_blastinc_green")))
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_blastinc_greenL")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_blastinc_green")))
		OR NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_blastinc_greenL")))
			bAllLoaded = FALSE
		ENDIF
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_blastinc_orange")))
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_blastinc_orngL")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_blastinc_orange")))
		OR NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_blastinc_orngL")))
			bAllLoaded = FALSE
		ENDIF
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_blastinc_pink")))
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_blastinc_pinkL")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_blastinc_pink")))
		OR NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_blastinc_pinkL")))
			bAllLoaded = FALSE
		ENDIF
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_blastinc_purple")))
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_blastinc_purpL")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_blastinc_purple")))
		OR NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_blastinc_purpL")))
			bAllLoaded = FALSE
		ENDIF
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_blastdec")))
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_blastdecL")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_blastdec")))
		OR NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_blastdecL")))
			bAllLoaded = FALSE
		ENDIF
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_blastinc")))
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_blastincL")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_blastinc")))
		OR NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_blastincL")))
			bAllLoaded = FALSE
		ENDIF
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_blastdec_green")))
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_blastdec_greenL")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_blastdec_green")))
		OR NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_blastdec_greenL")))
			bAllLoaded = FALSE
		ENDIF
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_blastdec_orange")))
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_blastdec_orngL")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_blastdec_orange")))
		OR NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_blastdec_orngL")))
			bAllLoaded = FALSE
		ENDIF
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_blastdec_pink")))
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_blastdec_pinkL")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_blastdec_pink")))
		OR NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_blastdec_pinkL")))
			bAllLoaded = FALSE
		ENDIF
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_blastdec_purple")))
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_blastdec_purpL")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_blastdec_purple")))
		OR NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_blastdec_purpL")))
			bAllLoaded = FALSE
		ENDIF		
	ENDIF
	
	IF IS_BIT_SET(iVehicleWepModelsToLoadBS, ciVEH_WEP_BOMB_MAX)
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_bombdec")))
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_bombdecL")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_bombdec")))
		OR NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_bombdecL")))
			bAllLoaded = FALSE
		ENDIF
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_bombinc")))
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_bombincL")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_bombinc")))
		OR NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_bombincL")))
			bAllLoaded = FALSE
		ENDIF	
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_bombdec_green")))
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_bombdec_greenL")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_bombdec_green")))
		OR NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_bombdec_greenL")))
			bAllLoaded = FALSE
		ENDIF	
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_bombdec_orange")))
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_bombdec_orngL")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_bombdec_orange")))
		OR NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_bombdec_orngL")))
			bAllLoaded = FALSE
		ENDIF	
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_bombdec_pink")))
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_bombdec_pinkL")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_bombdec_pink")))
		OR NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_bombdec_pinkL")))
			bAllLoaded = FALSE
		ENDIF	
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_bombdec_purple")))
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_bombdec_purpL")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_bombdec_purple")))
		OR NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_bombdec_purpL")))
			bAllLoaded = FALSE
		ENDIF	
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_bombinc")))
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_bombincL")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_bombinc")))
		OR NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_bombincL")))
			bAllLoaded = FALSE
		ENDIF	
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_bombinc_green")))
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_bombinc_greenL")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_bombinc_green")))
		OR NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_bombinc_greenL")))
			bAllLoaded = FALSE
		ENDIF	
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_bombinc_orange")))
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_bombinc_orngL")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_bombinc_orange")))
		OR NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_bombinc_orngL")))
			bAllLoaded = FALSE
		ENDIF	
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_bombinc_pink")))
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_bombinc_pinkL")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_bombinc_pink")))
		OR NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_bombinc_pinkL")))
			bAllLoaded = FALSE
		ENDIF
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_bombinc_purple")))
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_bombinc_purpL")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_bombinc_purple")))
		OR NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_bombinc_purpL")))
			bAllLoaded = FALSE
		ENDIF
		
	ENDIF
	IF IS_BIT_SET(iVehicleWepModelsToLoadBS, ciVEH_WEP_RAMP_SPECIAL_VEH)
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Special_Buggy")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Special_Buggy")))
			bAllLoaded = FALSE
		ENDIF
		
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Special_Buggy_P")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Special_Buggy_P")))
			bAllLoaded = FALSE
		ENDIF

		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Special_Buggy_PK")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Special_Buggy_PK")))
			bAllLoaded = FALSE
		ENDIF
		
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Special_Buggy_G")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Special_Buggy_G")))
			bAllLoaded = FALSE
		ENDIF
		
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Special_Buggy_WH")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Special_Buggy_WH")))
			bAllLoaded = FALSE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iVehicleWepModelsToLoadBS, ciVEH_WEP_RUINER_SPECIAL_VEH)
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_special_ruiner")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_special_ruiner")))
			bAllLoaded = FALSE
		ENDIF
		
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_special_ruiner_P")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_special_ruiner_P")))
			bAllLoaded = FALSE
		ENDIF

		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_special_ruiner_PK")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_special_ruiner_PK")))
			bAllLoaded = FALSE
		ENDIF
		
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_special_ruiner_G")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_special_ruiner_G")))
			bAllLoaded = FALSE
		ENDIF
		
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_special_ruiner_WH")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_special_ruiner_WH")))
			bAllLoaded = FALSE
		ENDIF
		
	ENDIF	
		
	IF IS_BIT_SET(iVehicleWepModelsToLoadBS, ciVEH_WEP_FORCE_ACCELERATE)
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Accel")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Accel")))
			bAllLoaded = FALSE
		ENDIF
		
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Acce_P")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Acce_P")))
			bAllLoaded = FALSE
		ENDIF
		
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Accel_PK")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Accel_PK")))
			bAllLoaded = FALSE
		ENDIF
	
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Accel_G")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Accel_G")))
			bAllLoaded = FALSE
		ENDIF
		
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Acce_WH")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Acce_WH")))
			bAllLoaded = FALSE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iVehicleWepModelsToLoadBS, ciVEH_WEP_FLIPPED_CONTROLS)
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_SWAP")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_SWAP")))
			bAllLoaded = FALSE
		ENDIF
		
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_SWAP_P")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_SWAP_P")))
			bAllLoaded = FALSE
		ENDIF
		
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_SWAP_PK")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_SWAP_PK")))
			bAllLoaded = FALSE
		ENDIF
		
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_SWAP_G")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_SWAP_G")))
			bAllLoaded = FALSE
		ENDIF
			
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_SWAP_WH")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_SWAP_WH")))
			bAllLoaded = FALSE
		ENDIF	
	ENDIF
	
	IF IS_BIT_SET(iVehicleWepModelsToLoadBS, ciVEH_WEP_ZONED)
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_B_TIME")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_B_TIME")))
			bAllLoaded = FALSE
		ENDIF
		
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_B_TIME_P")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_B_TIME_P")))
			bAllLoaded = FALSE
		ENDIF
		
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_B_TIME_PK")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_B_TIME_PK")))
			bAllLoaded = FALSE
		ENDIF
		
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_B_TIME_G")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_B_TIME_G")))
			bAllLoaded = FALSE
		ENDIF
		
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_B_TIME_WH")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_B_TIME_WH")))
			bAllLoaded = FALSE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iVehicleWepModelsToLoadBS, ciVEH_WEP_DETONATE)
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Deton")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Deton")))
			bAllLoaded = FALSE
		ENDIF
		
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Deton_P")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Deton_P")))
			bAllLoaded = FALSE
		ENDIF
		
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Deton_PK")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Deton_PK")))
			bAllLoaded = FALSE
		ENDIF
		
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Deton_G")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Deton_G")))
			bAllLoaded = FALSE
		ENDIF
		
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Deton_WH")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Deton_WH")))
			bAllLoaded = FALSE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iVehicleWepModelsToLoadBS, ciVEH_WEP_BOMB)
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Bomb")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Bomb")))
			bAllLoaded = FALSE
		ENDIF
				
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Bomb_P")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Bomb_P")))
			bAllLoaded = FALSE
		ENDIF
		
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Bomb_P")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Bomb_P")))
			bAllLoaded = FALSE
		ENDIF
		
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Bomb_PK")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Bomb_PK")))
			bAllLoaded = FALSE
		ENDIF
		
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Bomb_G")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Bomb_G")))
			bAllLoaded = FALSE
		ENDIF
		
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Bomb_WH")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Bomb_WH")))
			bAllLoaded = FALSE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iVehicleWepModelsToLoadBS, ciVEH_WEP_BOUNCE)
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Hop")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Hop")))
			bAllLoaded = FALSE
		ENDIF
		
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Hop_P")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Hop_P")))
			bAllLoaded = FALSE
		ENDIF
		
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Hop_PK")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Hop_PK")))
			bAllLoaded = FALSE
		ENDIF
		
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Hop_G")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Hop_G")))
			bAllLoaded = FALSE
		ENDIF
		
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Hop_WH")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Hop_WH")))
			bAllLoaded = FALSE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iVehicleWepModelsToLoadBS, ciVEH_WEP_REPAIR)
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Repair")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Repair")))
			bAllLoaded = FALSE
		ENDIF
		
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Repair_P")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Repair_P")))
			bAllLoaded = FALSE
		ENDIF
		
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Repair_PK")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Repair_PK")))
			bAllLoaded = FALSE
		ENDIF
		
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Repair_G")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Repair_G")))
			bAllLoaded = FALSE
		ENDIF
		
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Repair_WH")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Repair_WH")))
			bAllLoaded = FALSE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iVehicleWepModelsToLoadBS, ciVEH_WEP_RANDOM)
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_RANDOM")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_RANDOM")))
			bAllLoaded = FALSE
		ENDIF
		
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_RANDOM_P")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_RANDOM_P")))
			bAllLoaded = FALSE
		ENDIF
		
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_RANDOM_PK")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_RANDOM_PK")))
			bAllLoaded = FALSE
		ENDIF
		
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_RANDOM_G")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_RANDOM_G")))
			bAllLoaded = FALSE
		ENDIF
		
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_RANDOM_WH")))
		IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_RANDOM_WH")))
			bAllLoaded = FALSE
		ENDIF
	ENDIF
	IF IS_BIT_SET(iVehicleWepModelsToLoadBS, ciVEH_WEP_MACHINE_GUN)
		INT i
		FOR i = -1 to FMMC_MAX_TEAMS-1
			REQUEST_MODEL(VEHICLE_GUN_PICKUP_MODEL(i))
			IF NOT HAS_MODEL_LOADED(VEHICLE_GUN_PICKUP_MODEL(i))
				bAllLoaded = FALSE
			ENDIF
		ENDFOR
	ENDIF
	
	RETURN bAllLoaded
	
ENDFUNC

FUNC BOOL HAVE_PROP_ASSETS_LOADED(INT iPropToSpawn)
	
	IF NOT REQUEST_LOAD_MODEL(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropToSpawn].mn)
		RETURN FALSE
	ENDIF  
	
	RETURN TRUE

ENDFUNC

// ##### ------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Interiors ------------------------------------------------------------------------------------------------
// ##### Description: Checking Interiors have loaded.  --------------------------------------------------------------------------
// ##### ------------------------------------------------------------------------------------------------------------------------
// ##### ------------------------------------------------------------------------------------------------------------------------

// This is for special cases. We should be using ADD_INTERIOR_TO_CHECK inside of SET_UP_IPL_SWAP_FOR_MISSION_CONTROLLER if we need to stall progression waiting on interiors.
FUNC BOOL HAVE_SPECIAL_CASE_INTERIORS_LOADED()
	BOOL bReady = TRUE

	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_UNION_DEPOSITORY)	
		INTERIOR_INSTANCE_INDEX iInteriorIndex		
		INTERIOR_DATA_STRUCT structInteriorData	
		structInteriorData = GET_INTERIOR_DATA( INTERIOR_DT1_03_CARPARK )			
		iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		
		IF NOT IS_VALID_INTERIOR(iInteriorIndex)
			//RETURN TRUE
		ENDIF
		IF NOT IS_INTERIOR_READY(iInteriorIndex)
			bReady = FALSE
		ENDIF
	ENDIF
	
	IF CONTENT_IS_USING_ARENA()
		IF NOT LOAD_UGC_ARENA(g_ArenaInterior, iArenaInterior_VIPLoungeIndex)
			bReady = FALSE
			PRINTLN("[RCC MISSION] HAVE_SPECIAL_CASE_INTERIORS_LOADED - Arena not loaded yet.")
		ENDIF
	ENDIF
	
	IF bReady
		PRINTLN("[RCC MISSION] HAVE_SPECIAL_CASE_INTERIORS_LOADED - Interiors are ready/unneeded! ")
	ELSE
		PRINTLN("[RCC MISSION] HAVE_SPECIAL_CASE_INTERIORS_LOADED - Still Loading Interiors... ")
	ENDIF
	
	RETURN bReady
ENDFUNC

// ##### ------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Textures -------------------------------------------------------------------------------------------------
// ##### Description: Loading and unloading Textures.  --------------------------------------------------------------------------
// ##### ------------------------------------------------------------------------------------------------------------------------
// ##### ------------------------------------------------------------------------------------------------------------------------

PROC REQUEST_TEXTURE_STREAM_ASSETS()
	INT i = 0
	FOR i = 0 TO FMMC_MAX_GO_TO_LOCATIONS-1
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].iLocBS2, ciLoc_BS2_CrossLineDistUseMe_T0)
		OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].iLocBS2, ciLoc_BS2_CrossLineDistUseMe_T1)
		OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].iLocBS2, ciLoc_BS2_CrossLineDistUseMe_T2)
		OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].iLocBS2, ciLoc_BS2_CrossLineDistUseMe_T3)
			REQUEST_STREAMED_TEXTURE_DICT("CrossTheLine")
			i = FMMC_MAX_GO_TO_LOCATIONS
			PRINTLN("[RCC MISSION][LOADED_ALL_ASSETS] - REQUEST_STREAMED_TEXTURE_DICT called for CrossTheLine")
		ENDIF
	ENDFOR
ENDPROC

// ##### ------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Outfits --------------------------------------------------------------------------------------------------
// ##### Description: Load required outfits.  -----------------------------------------------------------------------------------
// ##### ------------------------------------------------------------------------------------------------------------------------
// ##### ------------------------------------------------------------------------------------------------------------------------


PROC LOAD_HEIST_GEAR_FOR_PLAYER_OUTFITS(INT iTeam, BOOL &bAllLoaded)
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_ONLY_GANG_BOSS_HAS_BAG)
	OR (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_ONLY_GANG_BOSS_HAS_BAG) AND GB_GET_LOCAL_PLAYER_GANG_BOSS() = LocalPlayer)
		IF IS_HEIST_GEAR_A_BAG(sApplyOutfitData.eGear)
			REQUEST_CLIP_SET("move_m@bag")
			IF NOT HAS_CLIP_SET_LOADED("move_m@bag")
				bAllLoaded = FALSE
				PRINTLN("[RCC MISSION][PROCESSED_PLAYER_OUTFITS] PROCESS_SCRIPT_INIT - HAS_CLIP_SET_LOADED(move_m@bag) FALSE.")
			ELSE
				IF bLocalPlayerPedOK
					SET_PED_MOVEMENT_CLIPSET(LocalPlayerPed, "move_m@bag")
					PRINTLN("[RCC MISSION][PROCESSED_PLAYER_OUTFITS] PROCESS_SCRIPT_INIT - Setting player ped movement clipset to move_m@bag.")
				ENDIF
			ENDIF
		ENDIF		
	ENDIF	
	
	//--------set ear piece to use----
	INT iGearEar
	REPEAT Enum_TO_INT(GEAR_MAX_AMOUNT) iGearEar
		IF iGearEar > 0
			IF IS_LONG_BIT_SET(g_FMMC_STRUCT.biGearAvailableBitset[iTeam], iGearEar-1) //Used Externally IS_LONG_BIT_SET
				IF INT_TO_ENUM(MP_HEIST_GEAR_ENUM, iGearEar-1) = HEIST_GEAR_EARPIECE_1
				OR INT_TO_ENUM(MP_HEIST_GEAR_ENUM, iGearEar-1) = HEIST_GEAR_EARPIECE_2
				OR INT_TO_ENUM(MP_HEIST_GEAR_ENUM, iGearEar-1) = HEIST_GEAR_EARPIECE_3
				OR INT_TO_ENUM(MP_HEIST_GEAR_ENUM, iGearEar-1) = HEIST_GEAR_RADIO_HEADSET
					sApplyOutfitData.eGearEar = INT_TO_ENUM(MP_HEIST_GEAR_ENUM, iGearEar-1)
				ENDIF									
			ENDIF	
		ENDIF
	ENDREPEAT
	
ENDPROC

// ##### ------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Outfits --------------------------------------------------------------------------------------------------
// ##### Description: Functions that load the outfit data/assets 
// ##### ------------------------------------------------------------------------------------------------------------------------
// ##### ------------------------------------------------------------------------------------------------------------------------

FUNC INT FMMC_GET_LOCAL_DEFAULT_MASK(INT iTeam)
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitset4, ciBS4_TEAM_4_GiveBalaclavaWithNoMask)
		IF GET_LOCAL_DEFAULT_MASK(iTeam) = -1
		AND NOT IS_FAKE_MULTIPLAYER_MODE_SET()
			SET_LOCAL_DEFAULT_MASK(iTeam, ENUM_TO_INT(OUTFIT_MASK_AMATEUR_3))
			PRINTLN("[Mask] FMMC_GET_LOCAL_DEFAULT_MASK - Default mask has been set to basic balaclava")
		ENDIF
	ENDIF
	
	RETURN GET_LOCAL_DEFAULT_MASK(iTeam)
	
ENDFUNC

FUNC BOOL SHOULD_CONTENT_BLOCK_VIP_OUTFITS()
	#IF FEATURE_HEIST_ISLAND	
	IF HEIST_ISLAND_FLOW_IS_CURRENT_MISSION_HEIST_ISLAND_FINALE()
		RETURN TRUE
	ENDIF
	#ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyNine, ciOptionsBS29_BlockOrganisationOutfits)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL MC_HAVE_WE_PASSED_THE_FIRST_CHECKPOINT()
	
	IF ( HAS_FIRST_STRAND_MISSION_BEEN_PASSED_NO_RESET()
	OR HAS_FIRST_STRAND_MISSION_BEEN_PASSED() )
	AND IS_BIT_SET(g_TransitionSessionNonResetVars.iRestartBitset, FMMC_RESTART_BITSET_RETRY_STRAND_PASS_START_POINT)
		PRINTLN("[MMacK][Checkpoint] MC_HAVE_WE_PASSED_THE_FIRST_CHECKPOINT - bRetryStrandPassStartPoint set, return TRUE")
		RETURN TRUE
	ENDIF
	
	IF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(4)
		PRINTLN("[MMacK][Checkpoint] MC_HAVE_WE_PASSED_THE_FIRST_CHECKPOINT - bRetryFourthStartPoint set, return FALSE")
		RETURN FALSE
	ENDIF
	
	IF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(3)
		PRINTLN("[MMacK][Checkpoint] MC_HAVE_WE_PASSED_THE_FIRST_CHECKPOINT - bRetryThirdStartPoint set, return FALSE")
		RETURN FALSE
	ENDIF
	
	IF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(1)
		PRINTLN("[MMacK][Checkpoint] MC_HAVE_WE_PASSED_THE_FIRST_CHECKPOINT - bRetrySecondStartPoint set, return FALSE")
		RETURN FALSE
	ENDIF
	
	IF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(0)
		PRINTLN("[MMacK][Checkpoint] MC_HAVE_WE_PASSED_THE_FIRST_CHECKPOINT - bRetryStartPoint set, return TRUE")
		RETURN TRUE
	ENDIF
	
	PRINTLN("[MMacK][Checkpoint] MC_HAVE_WE_PASSED_THE_FIRST_CHECKPOINT - no checkpoints set, return FALSE")
	RETURN FALSE
	
ENDFUNC

FUNC BOOL SHOULD_LOCAL_PLAYER_WEAR_LAST_WORN_MASK()
	
	IF g_iEquippedMask = -1
		PRINTLN("[Mask] SHOULD_LOCAL_PLAYER_WEAR_LAST_WORN_MASK - not applying mask g_iEquippedMask = -1")
		RETURN FALSE
	ENDIF
	
	INT iTeam = GET_LOCAL_PLAYER_TEAM()

	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitset4, ciBS4_TEAM_MASK_DONT_REAPPLY)
		PRINTLN("[Mask] SHOULD_LOCAL_PLAYER_WEAR_LAST_WORN_MASK - not applying mask due to ciBS4_TEAM_MASK_DONT_REAPPLY")
		RETURN FALSE
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitset4, ciBS4_TEAM_MASK_DONT_REAPPLY_CP_0)
	AND MC_HAVE_WE_PASSED_THE_FIRST_CHECKPOINT()
		PRINTLN("[Mask] SHOULD_LOCAL_PLAYER_WEAR_LAST_WORN_MASK - not applying mask due to ciBS4_TEAM_MASK_DONT_REAPPLY_CP_0")
		RETURN FALSE
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitset4, ciBS4_TEAM_MASK_DONT_REAPPLY_CP_1)
	AND FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(1)
		PRINTLN("[Mask] SHOULD_LOCAL_PLAYER_WEAR_LAST_WORN_MASK - not applying mask due to ciBS4_TEAM_MASK_DONT_REAPPLY_CP_1")
		RETURN FALSE
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitset4, ciBS4_TEAM_MASK_DONT_REAPPLY_CP_2)
	AND FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(2)
		PRINTLN("[Mask] SHOULD_LOCAL_PLAYER_WEAR_LAST_WORN_MASK - not applying mask due to ciBS4_TEAM_MASK_DONT_REAPPLY_CP_2")
		RETURN FALSE
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitset4, ciBS4_TEAM_MASK_DONT_REAPPLY_CP_3)
	AND FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(3)
		PRINTLN("[Mask] SHOULD_LOCAL_PLAYER_WEAR_LAST_WORN_MASK - not applying mask due to ciBS4_TEAM_MASK_DONT_REAPPLY_CP_3")
		RETURN FALSE
	ENDIF
	IF IS_AN_AIRLOCK_STRAND_TRANSITION_RUNNING()
		PRINTLN("[Mask] SHOULD_LOCAL_PLAYER_WEAR_LAST_WORN_MASK - not applying mask due to IS_AN_AIRLOCK_STRAND_TRANSITION_RUNNING")
		RETURN FALSE
	ENDIF
		
	PRINTLN("[Mask] SHOULD_LOCAL_PLAYER_WEAR_LAST_WORN_MASK - Returning TRUE to apply mask ", g_iEquippedMask)
	
	SET_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_BLOCK_MASKS_AND_GEAR)
	
	RETURN TRUE
	
ENDFUNC

/// PURPOSE:
///    This function applies outfits to the player, if required
/// RETURNS:
///    TRUE when outfits have been applied
FUNC BOOL PROCESSED_PLAYER_OUTFITS()

	IF IS_PLAYER_SPECTATOR_ONLY(LocalPlayer)
	OR DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(LocalPlayer)
		RETURN TRUE
	ENDIF
	
	BOOL bAllLoaded = TRUE

	IF NOT IS_BIT_SET(iLocalBoolCheck7, LBOOL12_DONE_HELMET_VISOR_UP_ON_INIT)
		SET_PLAYER_VISUAL_AID(VISUALAID_OFF, FALSE, VISUALAID_SOUND_OFF)
		SET_BIT(iLocalBoolCheck7, LBOOL12_DONE_HELMET_VISOR_UP_ON_INIT)
	ENDIF
	
	IF NOT HAS_FIRST_STRAND_MISSION_BEEN_PASSED()
		IF IS_CORONA_READY_TO_START_WITH_JOB()
		OR IS_FAKE_MULTIPLAYER_MODE_SET()
		OR (SHOULD_TRANSITION_SESSION_JOINED_LAUNCHED_MISSION() AND bIsSCTV OR DID_I_JOIN_MISSION_AS_SPECTATOR())
			
			IF SHOULD_PLAYER_PED_MODEL_BE_SWAPPED(GET_PRE_ASSIGNMENT_PLAYER_TEAM())
				PRINTLN("PROCESSED_PLAYER_OUTFITS - Returning TRUE straight away due to ped model swap")
				RETURN TRUE
			ENDIF
			
			sApplyOutfitData.pedID = LocalPlayerPed // set player ped as the ped we are assigning clothes to
			
			IF sApplyOutfitData.bOutfitPrevSet
				//If it's not a quick restart, the Planning Board will have already applied the outfits
				CPRINTLN(DEBUG_MISSION,"[AMEC][PROCESSED_PLAYER_OUTFITS] [HEIST_LAUNCH] - PROCESS_SCRIPT_INIT - Outfit already loaded by planning board, skipping...")
				MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(sApplyOutfitData.eOutfit)
				INT iTeam = GET_PRE_ASSIGNMENT_PLAYER_TEAM()
				
				IF g_FMMC_STRUCT.iGearDefault[iTeam] != -1
					IF IS_LONG_BIT_SET(g_FMMC_STRUCT.biGearAvailableBitset[iTeam], g_FMMC_STRUCT.iGearDefault[iTeam]) //Used Externally IS_LONG_BIT_SET
						CACHE_HEIST_GEAR(g_FMMC_STRUCT.iGearDefault[iTeam])
					ENDIF
					IF GIVE_AND_CACHE_LOCAL_PLAYER_HEIST_BAG(iTeam)
						PRINTLN("[PROCESSED_PLAYER_OUTFITS] - Applied sports bag")
					ENDIF
					APPLY_NIGHTVISION_FROM_TEAM_GEAR_IF_AVAILABLE(iTeam)
				ENDIF
				PRINTLN("[RCC MISSION][PROCESSED_PLAYER_OUTFITS] Outfits - MC_playerBD[",iLocalPart,"].iOutfit set as ",MC_playerBD[iLocalPart].iOutfit)
			ELSE
			
				PRINTLN("[JS][PROCESSED_PLAYER_OUTFITS] - Doing Gear (1) - g_iMyRaceOutfitSelection: ", g_iMyRaceOutfitSelection)
								
				INT iTeam = GET_PRE_ASSIGNMENT_PLAYER_TEAM()				

				IF iTeam > -1
				
					//--------set gear to use----	
					IF g_FMMC_STRUCT.iGearDefault[iTeam] != -1
						IF IS_LONG_BIT_SET(g_FMMC_STRUCT.biGearAvailableBitset[iTeam], g_FMMC_STRUCT.iGearDefault[iTeam]) //Used Externally IS_LONG_BIT_SET
							IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_ONLY_GANG_BOSS_HAS_BAG)								
							OR (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_ONLY_GANG_BOSS_HAS_BAG) AND GB_GET_LOCAL_PLAYER_GANG_BOSS() = LocalPlayer 
								AND IS_HEIST_GEAR_A_BAG(INT_TO_ENUM(MP_HEIST_GEAR_ENUM, g_FMMC_STRUCT.iGearDefault[iTeam])))
								
								CACHE_HEIST_GEAR(g_FMMC_STRUCT.iGearDefault[iTeam])
								IF IS_HEIST_GEAR_A_BAG(sApplyOutfitData.eGear)
									SET_BAG_FOR_HEIST(iTeam)
									SET_CACHED_HEIST_GEAR_BAG()
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					LOAD_HEIST_GEAR_FOR_PLAYER_OUTFITS(iTeam, bAllLoaded)
					
					//--------set outfit to use----
					sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, GET_LOCAL_DEFAULT_OUTFIT(iTeam))
					
					IF sMissionLocalContinuityVars.iOutfit != -1 
					AND (IS_THIS_A_QUICK_RESTART_JOB() OR IS_CORONA_INITIALISING_A_QUICK_RESTART())
						sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, sMissionLocalContinuityVars.iOutfit)
					ENDIF
										
					PRINTLN("[JS][PROCESSED_PLAYER_OUTFITS] - sApplyOutfitData.eOutfit: ", ENUM_TO_INT(sApplyOutfitData.eOutfit))
					
					//--------set mask to use----
					sApplyOutfitData.eMask   = INT_TO_ENUM(MP_OUTFIT_MASK_ENUM, FMMC_GET_LOCAL_DEFAULT_MASK(iTeam))
					BOOL bMaskOn, bApplyDefaultMask 			
					IF sApplyOutfitData.eOutfit != OUTFIT_MP_DEFAULT
						
						PRINTLN("[JS][PROCESSED_PLAYER_OUTFITS] - eOutfit not set to default")
						
						//---- outfit loading, setting and streaming requests have finished ----
						//Apply masks if they are pilot helmets
						
						bMaskOn = FALSE
						IF sApplyOutfitData.eOutfit = OUTFIT_HEIST_FLIGHT_SUIT_0
						OR sApplyOutfitData.eOutfit = OUTFIT_HEIST_STEALTH_PILOT_3	
							bMaskOn = TRUE
						ENDIF
						
						IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[GET_LOCAL_PLAYER_TEAM()].iTeamBitset4, ciBS4_TEAM_4_DontStartWithMask)
							IF IS_PED_WEARING_A_MASK(LocalPlayerPed)
							AND (GET_LOCAL_DEFAULT_OUTFIT(iTeam) = ENUM_TO_INT(OUTFIT_MP_DEFAULT)
							OR GET_LOCAL_DEFAULT_OUTFIT(iTeam) = ENUM_TO_INT(OUTFIT_MP_FREEMODE))
								bApplyDefaultMask = TRUE 
								bMaskOn = TRUE									
								sApplyOutfitData.eMask = OUTFIT_MASK_FREEMODE
								PRINTLN("[MMacK][PROCESSED_PLAYER_OUTFITS][Mask] SETUP OK")
							ENDIF
						ENDIF
						
						IF SHOULD_LOCAL_PLAYER_WEAR_LAST_WORN_MASK()
							sApplyOutfitData.eMask = INT_TO_ENUM(MP_OUTFIT_MASK_ENUM, g_iEquippedMask)
							bMaskOn = TRUE
							PRINTLN("[Mask][PROCESSED_PLAYER_OUTFITS] Adding equipped mask")
						ENDIF
						
						IF SHOULD_LOCAL_PLAYER_WEAR_LAST_WORN_MASK()
							sApplyOutfitData.eMask = INT_TO_ENUM(MP_OUTFIT_MASK_ENUM, g_iEquippedMask)
							bMaskOn = TRUE
							PRINTLN("[MMacK][PROCESSED_PLAYER_OUTFITS][Mask] Adding equipped mask")
						ENDIF
						
						IF SHOULD_LOCAL_PLAYER_WEAR_LAST_WORN_MASK()
							sApplyOutfitData.eMask = INT_TO_ENUM(MP_OUTFIT_MASK_ENUM, g_iEquippedMask)
							bMaskOn = TRUE
							PRINTLN("[MMacK][PROCESSED_PLAYER_OUTFITS][Mask] Adding equipped mask")
						ENDIF
						
						IF IS_CURRENT_OUTFIT_MASK_NIGHTVISION()
						AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[GET_LOCAL_PLAYER_TEAM()].iTeamBitset4, ciBS4_TEAM_4_AutoEquipNightVision)
							g_bAutoMaskEquip = TRUE									
							PRINTLN("[Mask] PROCESSED_PLAYER_OUTFITS - Setting g_bAutoMaskEquip for NIGHT VISION - ", sApplyOutfitData.eMask)
						ENDIF

						IF ((GB_GET_LOCAL_PLAYER_GANG_BOSS() = INVALID_PLAYER_INDEX()
						OR (NOT GB_OUTFITS_IS_PLAYER_WEARING_GANG_OUTFIT(LocalPlayer) AND g_sMagnateOutfitStruct.iBossStyle = ENUM_TO_INT(GB_BOSS_STYLE_NONE))))
						OR SHOULD_CONTENT_BLOCK_VIP_OUTFITS()
							
							IF NOT SET_PED_MP_OUTFIT(sApplyOutfitData, bMaskOn, FALSE, DEFAULT, bApplyDefaultMask) 
								bAllLoaded = FALSE
								PRINTLN("[RCC MISSION][PROCESSED_PLAYER_OUTFITS] MISSION - TRANSITION TIME - SET_PED_MP_OUTFIT  - FRAME COUNT = (", GET_FRAME_COUNT(), ") POSIX TIME = ", GET_CLOUD_TIME_AS_INT(), ")")
								PRINTLN("[RCC MISSION][PROCESSED_PLAYER_OUTFITS] SET_PED_MP_OUTFIT FALSE !!!!!!")
							ELSE
								MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(sApplyOutfitData.eOutfit)
								PRINTLN("[RCC MISSION][PROCESSED_PLAYER_OUTFITS] Outfits - WVM Work Dispute MC_playerBD[",iLocalPart,"].iOutfit set as ",MC_playerBD[iLocalPart].iOutfit)
								
								IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitset4, ciBS4_TEAM_4_RemoveScubaOnQuickRestart)
								AND (IS_THIS_A_QUICK_RESTART_JOB() OR IS_CORONA_INITIALISING_A_QUICK_RESTART())
								AND IS_PED_WEARING_SCUBA_TANK(LocalPlayerPed)
									PRINTLN("[RCC MISSION][PROCESSED_PLAYER_OUTFITS] Outfits - Removing scuba tank on quick restart")
									CLEAR_PED_SCUBA_GEAR_VARIATION(LocalPlayerPed)
								ENDIF
							ENDIF												
						ENDIF
													
						APPLY_NIGHTVISION_FROM_TEAM_GEAR_IF_AVAILABLE(iTeam)
						
					ELSE
						PRINTLN("[JS][PROCESSED_PLAYER_OUTFITS] - eOutfit is set to default")
						
						IF IS_PED_WEARING_A_MASK(LocalPlayerPed)
							PRINTLN("[PROCESSED_PLAYER_OUTFITS][Mask] Ped wearing a mask")
							sApplyOutfitData.eOutfit = OUTFIT_MP_FREEMODE
							sApplyOutfitData.eMask = OUTFIT_MASK_FREEMODE
							PRINTLN("[PROCESSED_PLAYER_OUTFITS][Mask] Setting sApplyOutfitData.eOutfit to OUTFIT_MP_DEFAULT due to the current outfit (, ", sApplyOutfitData.eOutfit, ") having a mask")
							
							IF NOT SET_PED_MP_OUTFIT(sApplyOutfitData, bMaskOn, FALSE, DEFAULT, bApplyDefaultMask)
								PRINTLN("[PROCESSED_PLAYER_OUTFITS][Mask] SETTING OUTFIT")
								bAllLoaded = FALSE
							ELSE
								PRINTLN("[PROCESSED_PLAYER_OUTFITS][Mask] OUTFIT SUCCESS")
								MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(sApplyOutfitData.eOutfit)
							ENDIF
						ENDIF
					ENDIF
				ELSE
					CPRINTLN(DEBUG_MISSION,"[RCC MISSION][PROCESSED_PLAYER_OUTFITS] Outfits - Iteam = ",iTeam,", no outfit needed.")
					REMOVE_PED_HELMET(LocalPlayerPed,TRUE)
				ENDIF	
			ENDIF			
		ELSE
			PRINTLN("[RCC MISSION][PROCESSED_PLAYER_OUTFITS] MISSION - TRANSITION TIME - IS_CORONA_READY_TO_START_WITH_JOB  - FRAME COUNT = (", GET_FRAME_COUNT(), ") POSIX TIME = ", GET_CLOUD_TIME_AS_INT(), ")")
			PRINTLN("[RCC MISSION][PROCESSED_PLAYER_OUTFITS] IS_CORONA_READY_TO_START_WITH_JOB FALSE !!!!!!")
		
			bAllLoaded = FALSE
		ENDIF
	
	ELSE
		sApplyOutfitData.pedID = LocalPlayerPed // set player ped as the ped we are assigning clothes to
		INT iTeam = GET_PRE_ASSIGNMENT_PLAYER_TEAM()
		
		CPRINTLN(DEBUG_MISSION,"[RCC MISSION] PROCESSED_PLAYER_OUTFITS() - PROCESS_SCRIPT_INIT - Re-apply .iOutfit for a non-first stage")
		IF g_TransitionSessionNonResetVars.sMissionLocalContinuityVars.iOutfit != -1
			MC_playerBD[iLocalPart].iOutfit = g_TransitionSessionNonResetVars.sMissionLocalContinuityVars.iOutfit
			IF IS_BIT_SET(g_TransitionSessionNonResetVars.sMissionLocalContinuityVars.iGeneralLocalTrackingBitset, ciContinuityGenericTrackingBS_DisguiseUsed)
				SET_BIT(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_CHANGED_INTO_DISGUISE)
			ENDIF
			PRINTLN("[RCC MISSION] MC_playerBD[iLocalPart].iOutfit - ", MC_playerBD[iLocalPart].iOutfit, " Set from continuity")
		ELSE
			MC_playerBD[iLocalPart].iOutfit 	= GET_LOCAL_DEFAULT_OUTFIT(iTeam)
			PRINTLN("[RCC MISSION] MC_playerBD[iLocalPart].iOutfit - ", MC_playerBD[iLocalPart].iOutfit, " Set from GET_LOCAL_DEFAULT_OUTFIT")
		ENDIF
		
		IF iTeam > -1
			IF GIVE_AND_CACHE_LOCAL_PLAYER_HEIST_BAG(iTeam)
				PRINTLN("[RCC MISSION][PROCESSED_PLAYER_OUTFITS] Applying bag on strand mission")
			ENDIF
			LOAD_HEIST_GEAR_FOR_PLAYER_OUTFITS(iTeam, bAllLoaded)	
		ENDIF
		
		#IF IS_DEBUG_BUILD
			MP_OUTFIT_ENUM ePlayerOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, MC_playerBD[iLocalPart].iOutfit)
			STRING stringPrint = GET_MP_OUTFIT_NAME(ePlayerOutfit)
			PRINTLN("[RCC MISSION] PROCESSED_PLAYER_OUTFITS() GET_LOCAL_DEFAULT_OUTFIT : ",  GET_MP_OUTFIT_NAME(INT_TO_ENUM(MP_OUTFIT_ENUM, GET_LOCAL_DEFAULT_OUTFIT(iTeam))),
						" - sApplyOutfitData.eOutfit ", GET_MP_OUTFIT_NAME(sApplyOutfitData.eOutfit), ". Player currently wearing: ", stringPrint)			
		#ENDIF
	ENDIF
	
	SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_DisableAutoEquipHelmetsInBikes, GET_MP_INT_CHARACTER_STAT(MP_STAT_AUTO_EQUIP_HELMET)=0)
	CPRINTLN(DEBUG_AMBIENT, "[RCC MISSION][PROCESSED_PLAYER_OUTFITS] default PCF_DisableAutoEquipHelmetsInBikes=",GET_STRING_FROM_BOOL(GET_MP_INT_CHARACTER_STAT(MP_STAT_AUTO_EQUIP_HELMET)=0))
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_TRANSFORM_ASSETS)
		REQUEST_ANIM_SET("ANIM_GROUP_MOVE_BALLISTIC")   
		REQUEST_ANIM_SET("MOVE_STRAFE_BALLISTIC")
		REQUEST_CLIP_SET("move_ballistic_2h")
	    IF NOT HAS_ANIM_SET_LOADED("ANIM_GROUP_MOVE_BALLISTIC")
	    OR NOT HAS_ANIM_SET_LOADED("MOVE_STRAFE_BALLISTIC")
	    OR NOT HAS_CLIP_SET_LOADED("move_ballistic_2h")
			PRINTLN("[RCC MISSION][PROCESSED_PLAYER_OUTFITS] Haven't loaded juggernaut movement anims yet.")
			bAllLoaded = FALSE		
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_ONLY_GANG_BOSS_HAS_BAG)
	AND GB_GET_LOCAL_PLAYER_GANG_BOSS() != LocalPlayer
		IF IS_MP_HEIST_GEAR_EQUIPPED(LocalPlayerPed, MC_playerBD_1[iLocalPart].eHeistGearBagType)
			PRINTLN("[RCC MISSION][PROCESSED_PLAYER_OUTFITS] Outfits - ciOptionsBS18_ONLY_GANG_BOSS_HAS_BAG is set. Removing bag as not gang boss/")
			REMOVE_DUFFEL_BAG_HEIST_GEAR(LocalPlayerPed, iLocalPart)
		ENDIF
	ENDIF
	
	RETURN bAllLoaded
ENDFUNC

// ##### ------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Systems --------------------------------------------------------------------------------------------------
// ##### Description: Functions that load multiple things for an entire system.  ------------------------------------------------
// ##### ------------------------------------------------------------------------------------------------------------------------
// ##### ------------------------------------------------------------------------------------------------------------------------

//Drilling
PROC MANAGE_VAULT_DRILLING_ASSET_REQUESTS__FMMC()
	IF IS_BIT_SET(MC_serverBD.iServerBitSet8, SBBOOL8_VAULT_DRILL_ASSET_REQUEST)
		IF NOT IS_BIT_SET(iLocalBoolCheck32, LBOOL32_VAULT_DRILL_ASSET_LOADED)
			IF REMOTE_ASSETS_REQUEST_AND_LOAD(IS_BIT_SET(MC_serverBD.iServerBitSet8, SBBOOL8_VAULT_DRILL_ASSET_REQUEST_LASER), MC_playerBD_1[iLocalPart].mnHeistGearBag)
				SET_BIT(iLocalBoolCheck32, LBOOL32_VAULT_DRILL_ASSET_LOADED)
			ENDIF
		ENDIF
	ELSE
		IF IS_BIT_SET(MC_serverBD.iServerBitSet8, SBBOOL8_VAULT_DRILL_ASSET_REQUEST)
			REMOTE_CLEAR_ASSETS(IS_BIT_SET(MC_serverBD.iServerBitSet8, SBBOOL8_VAULT_DRILL_ASSET_REQUEST_LASER), MC_playerBD_1[iLocalPart].mnHeistGearBag)
			CLEAR_BIT(iLocalBoolCheck32, LBOOL32_VAULT_DRILL_ASSET_LOADED)
		ENDIF
	ENDIF
ENDPROC

//Thermite
PROC REQUEST_THERMITE_ASSETS(BOOL bIsKeypadObject = FALSE)
	

	PRINTLN("[THERMITE] - [RCC MISSION] REQUEST_THERMITE_ASSETS() PARTICLES - Using patched PTFX")
	sPTFXThermalAsset = "pat_heist"
	REQUEST_NAMED_PTFX_ASSET("scr_ornate_heist")
	
	IF bIsKeypadObject
		sPTFXThermalAsset = "scr_ch_finale"
		PRINTLN("[THERMITE] - [RCC MISSION] REQUEST_THERMITE_ASSETS() PARTICLES - Using keypad PTFX")
	ENDIF
	
	REQUEST_MODEL(MC_playerBD_1[iLocalPart].mnHeistGearBag)
	REQUEST_MODEL(HEI_PROP_HEIST_THERMITE)
	REQUEST_MODEL(HEI_PROP_HEIST_THERMITE_FLASH)
	REQUEST_NAMED_PTFX_ASSET(sPTFXThermalAsset)
		
	IF bIsKeypadObject
		REQUEST_ANIM_DICT("anim_heist@hs3f@ig13_thermal_charge@thermal_charge@male@")
	ENDIF
	
	REQUEST_ANIM_DICT("anim@heists@ornate_bank@thermal_charge")

	IF IS_PED_WEARING_HIGH_HEELS(LocalPlayerPed)
		IF bIsKeypadObject
			REQUEST_ANIM_DICT("anim_heist@hs3f@ig13_thermal_charge@thermal_charge@female@")
		ENDIF
		
		REQUEST_ANIM_DICT("anim@heists@ornate_bank@thermal_charge_heels")
	ENDIF
ENDPROC

FUNC BOOL HAVE_THERMITE_ASSETS_LOADED(BOOL bIsKeypadObject = FALSE)
	BOOL bHeelAnimsReady = TRUE
	BOOL bKeypadAnimsReady = TRUE
	
	IF IS_PED_WEARING_HIGH_HEELS(LocalPlayerPed)
		bHeelAnimsReady = HAS_ANIM_DICT_LOADED("anim@heists@ornate_bank@thermal_charge_heels")
		IF bIsKeypadObject
			bKeypadAnimsReady = HAS_ANIM_DICT_LOADED("anim_heist@hs3f@ig13_thermal_charge@thermal_charge@female@")
		ENDIF
	ELSE
		IF bIsKeypadObject
			bKeypadAnimsReady = HAS_ANIM_DICT_LOADED("anim_heist@hs3f@ig13_thermal_charge@thermal_charge@male@")
		ENDIF
	ENDIF
	
	IF HAS_MODEL_LOADED(MC_playerBD_1[iLocalPart].mnHeistGearBag)
	AND HAS_MODEL_LOADED(HEI_PROP_HEIST_THERMITE)
	AND HAS_MODEL_LOADED(HEI_PROP_HEIST_THERMITE_FLASH)
	AND HAS_NAMED_PTFX_ASSET_LOADED(sPTFXThermalAsset)
	AND HAS_NAMED_PTFX_ASSET_LOADED("scr_ornate_heist")
	AND HAS_ANIM_DICT_LOADED("anim@heists@ornate_bank@thermal_charge")
	AND bHeelAnimsReady
	AND bKeypadAnimsReady
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC REMOVE_THERMITE_ASSETS(BOOL bIsKeypadObject = FALSE)
	
	PRINTLN("[THERMITE] - [RCC MISSION] REMOVE_THERMITE_ASSETS() PARTICLES - Using patched PTFX")
	REMOVE_NAMED_PTFX_ASSET("scr_ornate_heist")
	
	SET_MODEL_AS_NO_LONGER_NEEDED(MC_playerBD_1[iLocalPart].mnHeistGearBag)
	SET_MODEL_AS_NO_LONGER_NEEDED(HEI_PROP_HEIST_THERMITE)
	SET_MODEL_AS_NO_LONGER_NEEDED(HEI_PROP_HEIST_THERMITE_FLASH)
	REMOVE_NAMED_PTFX_ASSET(sPTFXThermalAsset)
	
	IF bIsKeypadObject
		REMOVE_ANIM_DICT("anim_heist@hs3f@ig13_thermal_charge@thermal_charge@male@")
		REMOVE_ANIM_DICT("anim_heist@hs3f@ig13_thermal_charge@thermal_charge@female@")
	ENDIF
	
	REMOVE_ANIM_DICT("anim@heists@ornate_bank@thermal_charge")
	REMOVE_ANIM_DICT("anim@heists@ornate_bank@thermal_charge_heels")
ENDPROC

//Container Minigame
PROC REQUEST_ASSETS_FOR_CONTAINER_MINIGAME()
	REQUEST_ANIM_DICT("anim@mp_mission@dr_objective")
	REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("imp_prop_adv_hdsec")))
	SET_BIT(iLocalBoolCheck17, LBOOL17_DR_REQUESTED_ANIM)
ENDPROC

FUNC BOOL HAVE_CONTAINER_MINIGAME_ASSETS_LOADED()
	IF IS_BIT_SET(iLocalBoolCheck17, LBOOL17_DR_REQUESTED_ANIM)
	AND HAS_ANIM_DICT_LOADED("anim@mp_mission@dr_objective")
	AND HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("imp_prop_adv_hdsec")))
		PRINTLN("[OPEN] - [RCC MISSION] HAVE_CONTAINER_MINIGAME_ASSETS_LOADED - loaded")
		RETURN TRUE
	ELSE
		REQUEST_ASSETS_FOR_CONTAINER_MINIGAME()
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC CLEAN_UP_ASSETS_FOR_CONTAINER_MINIGAME()
	REMOVE_ANIM_DICT("anim@mp_mission@dr_objective")
	CLEAR_BIT(iLocalBoolCheck17,LBOOL17_DR_REQUESTED_ANIM)
ENDPROC

// ##### ------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Cutscenes --------------------------------------------------------------------------------------------
// ##### Description: Assets and data for specific cutscenes.  ----------------------------------------------------------------------
// ##### ------------------------------------------------------------------------------------------------------------------------
// ##### ------------------------------------------------------------------------------------------------------------------------


FUNC BOOL GET_CONVERSATION_SHOULD_PLAY_OVER_NETWORK_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE(SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE eType)
	SWITCH eType	
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_IAA_INTRO_SCENE
			RETURN FALSE
		BREAK
	ENDSWITCH
	RETURN TRUE
ENDFUNC

FUNC FLOAT GET_ONE_SHOT_SOUND_PHASE_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE(SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE eType)
	FLOAt fReturn = 0.0
	SWITCH eType
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_DOOR_TRANS_HINGE_L
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_DOOR_TRANS_HINGE_R
			fReturn = 0.28
		BREAK
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_STEAL_YOUGA_VAN
			fReturn = 0.0
		BREAK
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_GARAGE_DOOR_SOLO
			fReturn = 0.85
		BREAK
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_GARAGE_BOTH_HANDS
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_GARAGE_LEFT_HAND
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_GARAGE_RIGHT_HAND
			fReturn = 0.5
		BREAK
	ENDSWITCH
	
	RETURN fReturn
ENDFUNC

FUNC STRING GET_ONE_SHOT_SOUND_NAME_FOR_START_SCRIPTED_CUTSCENE_SYNC_SCENE(SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE eType)
	STRING tlReturn
	
	SWITCH eType
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_DOOR_TRANS_HINGE_R
			tlReturn = "Generic_Door_Push"
		BREAK
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_DOOR_TRANS_HINGE_L
			tlReturn = "Push"
		BREAK
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_DOOR_TRANS_HINGE_L_CHARGE
			tlReturn = "Barge_Door_Metal"
		BREAK
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_STEAL_YOUGA_VAN
			tlReturn = "Garage_Door_Open"
		BREAK
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_GARAGE_DOOR_SOLO
			tlReturn = "Garage_Door_Close"
		BREAK
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_GARAGE_BOTH_HANDS
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_GARAGE_LEFT_HAND
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_GARAGE_RIGHT_HAND
			tlReturn = "Garage_Door_Open"
		BREAK
	ENDSWITCH
	
	RETURN tlReturn
ENDFUNC

FUNC STRING GET_ONE_SHOT_SOUND_SET_FOR_START_SCRIPTED_CUTSCENE_SYNC_SCENE(SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE eType)
	STRING tlReturn
	
	SWITCH eType
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_DOOR_TRANS_HINGE_R
			tlReturn = "GTAO_Script_Doors_Sounds"
		BREAK
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_DOOR_TRANS_HINGE_L		
			tlReturn = "GTAO_APT_DOOR_ROOF_METAL_SOUNDS"
		BREAK
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_DOOR_TRANS_HINGE_L_CHARGE
			tlReturn = "dlc_h4_Prep_FC_Sounds"
		BREAK
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_STEAL_YOUGA_VAN
			tlReturn = "GTAO_Script_Doors_Faded_Screen_Sounds"
		BREAK
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_GARAGE_DOOR_SOLO
			tlReturn = "GTAO_Script_Doors_Faded_Screen_Sounds"
		BREAK
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_GARAGE_BOTH_HANDS
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_GARAGE_LEFT_HAND
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_GARAGE_RIGHT_HAND
			tlReturn = "GTAO_Script_Doors_Faded_Screen_Sounds"
		BREAK
	ENDSWITCH
	
	RETURN tlReturn
ENDFUNC

FUNC STRING GET_ANIM_NAME_FACE_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE_PLAYER(SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE eType, INT iIndex, BOOL bUseAlternate)

	SWITCH eType
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_V1_NO_THREAT_LEFT
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_V1_NO_THREAT_RIGHT						
			// Franklin First. Lamar Second.
			IF NOT bUseAlternate 
				SWITCH iIndex
					CASE 0	RETURN "ENTER_FL_V1_FRANKLIN_facial"
					CASE 1	RETURN "ENTER_FL_V1_LAMAR_facial"
				ENDSWITCH
			ELSE
				SWITCH iIndex
					CASE 0	RETURN "ENTER_LF_V1_FRANKLIN_facial"
					CASE 1	RETURN "ENTER_LF_V1_LAMAR_facial"
				ENDSWITCH
			ENDIF
		BREAK
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_V2_THREAT_ENDED_LEFT
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_V2_THREAT_ENDED_RIGHT
			// Franklin First. Lamar Second.
			IF NOT bUseAlternate 
				SWITCH iIndex
					CASE 0	RETURN "ENTER_FL_V2_FRANKLIN_facial"
					CASE 1	RETURN "ENTER_FL_V2_LAMAR_facial"
				ENDSWITCH
			ELSE
				SWITCH iIndex
					CASE 0	RETURN "ENTER_LF_V2_FRANKLIN_facial"
					CASE 1	RETURN "ENTER_LF_V2_LAMAR_facial"
				ENDSWITCH
			ENDIF
		BREAK
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_V3_THREAT_PRESENT_LEFT
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_V3_THREAT_PRESENT_RIGHT
			// Franklin First. Lamar Second.
			IF NOT bUseAlternate 
				SWITCH iIndex
					CASE 0	RETURN "ENTER_FL_V3_FRANKLIN_facial"
					CASE 1	RETURN "ENTER_FL_V3_LAMAR_facial"
				ENDSWITCH
			ELSE
				SWITCH iIndex
					CASE 0	RETURN "ENTER_LF_V3_FRANKLIN_facial"
					CASE 1	RETURN "ENTER_LF_V3_LAMAR_facial"
				ENDSWITCH
			ENDIF
		BREAK
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_V4_UNDER_FIRE_LEFT
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_V4_UNDER_FIRE_RIGHT
			// Franklin First. Lamar Second.
			IF NOT bUseAlternate 
				SWITCH iIndex
					CASE 0	RETURN "ENTER_FL_V4_FRANKLIN_facial"
					CASE 1	RETURN "ENTER_FL_V4_LAMAR_facial"
				ENDSWITCH
			ELSE
				SWITCH iIndex
					CASE 0	RETURN "ENTER_LF_V4_FRANKLIN_facial"
					CASE 1	RETURN "ENTER_LF_V4_LAMAR_facial"
				ENDSWITCH
			ENDIF
		BREAK		
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_IAA_INTRO_SCENE
			SWITCH iIndex
				CASE 0	
					IF bUseAlternate
						RETURN "ACTION_HEELED_Facial"
					ELSE
						RETURN "ACTION_MALE_Facial"
					ENDIF
				BREAK
				CASE 1	RETURN ""
				CASE 2	RETURN ""
				CASE 3	RETURN ""
			ENDSWITCH
		BREAK		
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_GOING_UNDERGROUND_HATCH_EXIT
			SWITCH iIndex
				CASE 0	RETURN "EXIT_PLAYER0_facial"
				CASE 1	RETURN "EXIT_PLAYER1_facial"
				CASE 2	RETURN "EXIT_PLAYER2_facial"
				CASE 3	RETURN "EXIT_PLAYER3_facial"
			ENDSWITCH
		BREAK
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_GOING_UNDERGROUND_HATCH_ENTER
			SWITCH iIndex
				CASE 0	RETURN "ENTER_PLAYER0_facial"
				CASE 1	RETURN "ENTER_PLAYER1_facial"
				CASE 2	RETURN "ENTER_PLAYER2_facial"
				CASE 3	RETURN "ENTER_PLAYER3_facial"
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN ""
ENDFUNC

FUNC VEHICLE_SEAT GET_FORCED_VEHICLE_SEAT_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE_PLAYER(SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE eType)
	
	SWITCH eType
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_STEAL_YOUGA_VAN
			RETURN GET_SEAT_PED_IS_IN(localPlayerPed, TRUE)
		BREAK
	ENDSWITCH
	
	RETURN VS_ANY_PASSENGER
	
ENDFUNC

FUNC INT GET_ADJUSTED_INDEX_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE_PLAYER(SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE eType, INT iIndex, BOOL bUseAlternate, PED_INDEX &pedCutscene[])
	SWITCH eType
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_V1_NO_THREAT_LEFT
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_V1_NO_THREAT_RIGHT
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_V2_THREAT_ENDED_LEFT
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_V2_THREAT_ENDED_RIGHT
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_V3_THREAT_PRESENT_LEFT
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_V3_THREAT_PRESENT_RIGHT
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_V4_UNDER_FIRE_LEFT
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_V4_UNDER_FIRE_RIGHT
			IF bUseAlternate 
				SWITCH iIndex
					CASE 0
						IF GET_ENTITY_MODEL(pedCutscene[iIndex]) = INT_TO_ENUM(MODEL_NAMES, HASH("P_Franklin_02"))
							RETURN 1
						ENDIF
					BREAK
				
					CASE 1
						IF GET_ENTITY_MODEL(pedCutscene[iIndex]) = INT_TO_ENUM(MODEL_NAMES, HASH("IG_LamarDavis_02"))
							RETURN 0
						ENDIF
					BREAK
				ENDSWITCH				
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN iIndex
ENDFUNC

FUNC STRING GET_ANIM_NAME_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE_PLAYER(SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE eType, INT iIndex, BOOL bUseAlternate)
	SWITCH eType
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_CASINO_GRUPPE_SECHS_ENTRY
			SWITCH iIndex
				CASE 0	RETURN "MISSING_DELIVERY_PED1"
				CASE 1	RETURN "MISSING_DELIVERY_PED2"
				CASE 2	RETURN "MISSING_DELIVERY_PED3"
				CASE 3	RETURN "MISSING_DELIVERY_PED4"
			ENDSWITCH
		BREAK
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_CASINO_APARTMENT_TRANS_LEFT 
			SWITCH iIndex
				CASE 0	RETURN "ext_player"
				CASE 1	RETURN "ext_a"
				CASE 2	RETURN "ext_b"
				CASE 3	RETURN "ext_c"
			ENDSWITCH
		BREAK
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_CASINO_APARTMENT_TRANS_RIGHT
			SWITCH iIndex
				CASE 0	RETURN "ext_player"
				CASE 1	RETURN "ext_a"
				CASE 2	RETURN "ext_b"
				CASE 3	RETURN "ext_c"
			ENDSWITCH
		BREAK
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_DOOR_TRANS_HINGE_L
			SWITCH iIndex
				CASE 0	RETURN "walk_player1"
				CASE 1	RETURN "walk_player2"
				CASE 2	RETURN "walk_player3"
				CASE 3	RETURN "walk_player4"
			ENDSWITCH
		BREAK	
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_DOOR_TRANS_HINGE_R
			SWITCH iIndex
				CASE 0	RETURN "walk_player1"
				CASE 1	RETURN "walk_player2"
				CASE 2	RETURN "walk_player3"
				CASE 3	RETURN "walk_player4"
			ENDSWITCH
		BREAK
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_DOOR_TRANS_HINGE_L_CHARGE
			SWITCH iIndex
				CASE 0	RETURN "charge_player1"
				CASE 1	RETURN "charge_player2"
				CASE 2	RETURN "charge_player3"
				CASE 3	RETURN "charge_player4"
			ENDSWITCH
		BREAK
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_STEAL_YOUGA_VAN
			IF NOT bUseAlternate
				SWITCH iIndex
					CASE 0	RETURN "driver_fixf_trip2_ig1_van_exit"
					CASE 1	RETURN "passenger_fixf_trip2_ig1_van_exit"
				ENDSWITCH
			ELSE
				SWITCH iIndex
					CASE 0	RETURN "passenger_fixf_trip2_ig1_van_exit"
					CASE 1	RETURN "driver_fixf_trip2_ig1_van_exit"
				ENDSWITCH
			ENDIF
		BREAK
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_V1_NO_THREAT_LEFT
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_V1_NO_THREAT_RIGHT						
			// Franklin First. Lamar Second.
			IF NOT bUseAlternate 
				SWITCH iIndex
					CASE 0	RETURN "ENTER_FL_V1_FRANKLIN"
					CASE 1	RETURN "ENTER_FL_V1_LAMAR"
				ENDSWITCH
			ELSE
				SWITCH iIndex
					CASE 0	RETURN "ENTER_LF_V1_FRANKLIN"
					CASE 1	RETURN "ENTER_LF_V1_LAMAR"
				ENDSWITCH
			ENDIF
		BREAK
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_V2_THREAT_ENDED_LEFT
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_V2_THREAT_ENDED_RIGHT
			// Franklin First. Lamar Second.
			IF NOT bUseAlternate 
				SWITCH iIndex
					CASE 0	RETURN "ENTER_FL_V2_FRANKLIN"
					CASE 1	RETURN "ENTER_FL_V2_LAMAR"
				ENDSWITCH
			ELSE
				SWITCH iIndex
					CASE 0	RETURN "ENTER_LF_V2_FRANKLIN"
					CASE 1	RETURN "ENTER_LF_V2_LAMAR"
				ENDSWITCH
			ENDIF
		BREAK
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_V3_THREAT_PRESENT_LEFT
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_V3_THREAT_PRESENT_RIGHT
			// Franklin First. Lamar Second.
			IF NOT bUseAlternate 
				SWITCH iIndex
					CASE 0	RETURN "ENTER_FL_V3_FRANKLIN"
					CASE 1	RETURN "ENTER_FL_V3_LAMAR"
				ENDSWITCH
			ELSE
				SWITCH iIndex
					CASE 0	RETURN "ENTER_LF_V3_FRANKLIN"
					CASE 1	RETURN "ENTER_LF_V3_LAMAR"
				ENDSWITCH
			ENDIF
		BREAK
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_V4_UNDER_FIRE_LEFT
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_V4_UNDER_FIRE_RIGHT
			// Franklin First. Lamar Second.
			IF NOT bUseAlternate 
				SWITCH iIndex
					CASE 0	RETURN "ENTER_FL_V4_FRANKLIN"
					CASE 1	RETURN "ENTER_FL_V4_LAMAR"
				ENDSWITCH
			ELSE
				SWITCH iIndex
					CASE 0	RETURN "ENTER_LF_V4_FRANKLIN"
					CASE 1	RETURN "ENTER_LF_V4_LAMAR"
				ENDSWITCH
			ENDIF
		BREAK
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_GARAGE_DOOR_SOLO
			SWITCH iIndex				
				CASE 0	RETURN "gar_open_1_left"
				CASE 1	RETURN ""
				CASE 2	RETURN ""
				CASE 3	RETURN ""
			ENDSWITCH
		BREAK
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_ELEVATOR_BUTTON_PRESS_LEFT
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_ELEVATOR_BUTTON_PRESS_RIGHT
			SWITCH iIndex
				CASE 0	RETURN "elev_1"
				CASE 1	RETURN ""
				CASE 2	RETURN ""
				CASE 3	RETURN ""
			ENDSWITCH
		BREAK
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_IAA_INTRO_SCENE			
			SWITCH iIndex
				CASE 0	
					IF bUseAlternate
						RETURN "ACTION_HEELED"
					ELSE
						RETURN "ACTION_MALE"
					ENDIF
				BREAK
				CASE 1	RETURN ""
				CASE 2	RETURN ""
				CASE 3	RETURN ""
			ENDSWITCH
		BREAK				
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_GARAGE_BOTH_HANDS
			SWITCH iIndex
				CASE 0	RETURN "KNEEL_DOWN_01"
				CASE 1	RETURN ""
				CASE 2	RETURN ""
				CASE 3	RETURN ""
			ENDSWITCH
		BREAK
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_GARAGE_LEFT_HAND
			SWITCH iIndex
				CASE 0	RETURN "KNEEL_DOWN_02"
				CASE 1	RETURN ""
				CASE 2	RETURN ""
				CASE 3	RETURN ""
			ENDSWITCH
		BREAK
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_GARAGE_RIGHT_HAND
			SWITCH iIndex
				CASE 0	RETURN "KNEEL_DOWN_03"
				CASE 1	RETURN ""
				CASE 2	RETURN ""
				CASE 3	RETURN ""
			ENDSWITCH
		BREAK
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_GOING_UNDERGROUND_HATCH_EXIT
			SWITCH iIndex
				CASE 0	RETURN "EXIT_PLAYER0"
				CASE 1	RETURN "EXIT_PLAYER1"
				CASE 2	RETURN "EXIT_PLAYER2"
				CASE 3	RETURN "EXIT_PLAYER3"
			ENDSWITCH
		BREAK
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_GOING_UNDERGROUND_HATCH_ENTER
			SWITCH iIndex
				CASE 0	RETURN "ENTER_PLAYER0"
				CASE 1	RETURN "ENTER_PLAYER1"
				CASE 2	RETURN "ENTER_PLAYER2"
				CASE 3	RETURN "ENTER_PLAYER3"
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN ""
ENDFUNC

FUNC STRING GET_ANIM_NAME_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE_PED(SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE eType, INT iIndex)
	SWITCH eType
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_CASINO_GRUPPE_SECHS_ENTRY
			SWITCH iIndex
				CASE 0 RETURN ""
			ENDSWITCH
		BREAK
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_IAA_INTRO_SCENE
			SWITCH iIndex
				CASE 0 RETURN "ACTION_FIB_AGENT"
			ENDSWITCH
		BREAK		
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_GOING_UNDERGROUND_HATCH_EXIT
			SWITCH iIndex
				CASE 0	RETURN "EXIT_PILOT"
				CASE 1	RETURN "EXIT_COPILOT"
			ENDSWITCH
		BREAK
	ENDSWITCH	
	RETURN ""
ENDFUNC

FUNC STRING GET_ANIM_NAME_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE_OBJECT(SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE eType, INT iIndex)
	SWITCH eType
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_CASINO_GRUPPE_SECHS_ENTRY
			SWITCH iIndex
				CASE 0 RETURN ""
			ENDSWITCH
		BREAK				
	ENDSWITCH	
	RETURN ""
ENDFUNC

FUNC STRING GET_ANIM_NAME_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE_PROP(SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE eType, INT iIndex)
	SWITCH eType
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_CASINO_GRUPPE_SECHS_ENTRY
			SWITCH iIndex
				CASE 0 RETURN ""
			ENDSWITCH
		BREAK
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_IAA_INTRO_SCENE
			SWITCH iIndex
				CASE 0 RETURN "ACTION_CHAIR"
				CASE 1 RETURN "ACTION_DESK_FLAG"				
				CASE 2 RETURN "ACTION_DOCUMENT_02"
				CASE 3 RETURN "ACTION_PHONE"
				CASE 4 RETURN "ACTION_TABLE"
			ENDSWITCH
		BREAK
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_GOING_UNDERGROUND_HATCH_EXIT
			SWITCH iIndex
				CASE 0	RETURN "EXIT_HATCH"		
			ENDSWITCH
		BREAK
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_GOING_UNDERGROUND_HATCH_ENTER
			SWITCH iIndex
				CASE 0	RETURN "ENTER_HATCH"		
			ENDSWITCH
		BREAK
	ENDSWITCH	
	RETURN ""
ENDFUNC

FUNC STRING GET_ANIM_NAME_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE_VEHICLE(SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE eType, INT iIndex)
	SWITCH eType
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_CASINO_GRUPPE_SECHS_ENTRY
			SWITCH iIndex
				CASE 0 RETURN "MISSING_DELIVERY_Stockade"
			ENDSWITCH
		BREAK
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_STEAL_YOUGA_VAN
			RETURN "van_fixf_trip2_ig1_van_exit"
		BREAK
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_GOING_UNDERGROUND_HATCH_EXIT
			SWITCH iIndex
				CASE 0	RETURN "EXIT_HELICOPTER"		
			ENDSWITCH
		BREAK
	ENDSWITCH	
	RETURN ""
ENDFUNC

FUNC STRING GET_ANIM_NAME_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE_CAM(SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE eType, BOOL bForceLeft, BOOL bUseAlternate)
	STRING tlReturn
	
	// Special Cases:
	SWITCH eType
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_CASINO_GRUPPE_SECHS_ENTRY 	
			tlReturn = "MISSING_DELIVERY_Cam"
		BREAK
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_DOOR_TRANS_HINGE_L 	
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_DOOR_TRANS_HINGE_R 	
			IF bForceLeft
				tlReturn = "WALK_CAM_LEFT"
			ELSE
				tlReturn = "WALK_CAM_RIGHT"
			ENDIF
		BREAK
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_DOOR_TRANS_HINGE_L_CHARGE 	
			IF bForceLeft
				tlReturn = "CHARGE_CAM_LEFT"
			ELSE
				tlReturn = "CHARGE_CAM_RIGHT"
			ENDIF
		BREAK
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_STEAL_YOUGA_VAN
			tlReturn = "fixf_trip2_ig1_van_exit_cam"
		BREAK
		
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_SPECIAL_LEFT
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_SPECIAL_RIGHT
		BREAK
		
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_V1_NO_THREAT_LEFT
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_V1_NO_THREAT_RIGHT
			// Franklin First. Lamar Second.
			IF NOT bUseAlternate
				IF bForceLeft
					tlReturn = "ENTER_FL_V1_CAM_LEFT"
				ELSE
					tlReturn = "ENTER_FL_V1_CAM_RIGHT"
				ENDIF
			ELSE
				IF bForceLeft
					tlReturn = "ENTER_LF_V1_CAM_LEFT"
				ELSE
					tlReturn = "ENTER_LF_V1_CAM_RIGHT"
				ENDIF
			ENDIF
		BREAK
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_V2_THREAT_ENDED_LEFT
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_V2_THREAT_ENDED_RIGHT
			// Franklin First. Lamar Second.
			IF NOT bUseAlternate
				IF bForceLeft
					tlReturn = "ENTER_FL_V2_CAM_LEFT"
				ELSE
					tlReturn = "ENTER_FL_V2_CAM_RIGHT"
				ENDIF
			ELSE
				IF bForceLeft
					tlReturn = "ENTER_LF_V2_CAM_LEFT"
				ELSE
					tlReturn = "ENTER_LF_V2_CAM_RIGHT"
				ENDIF
			ENDIF
		BREAK
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_V3_THREAT_PRESENT_LEFT
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_V3_THREAT_PRESENT_RIGHT
			// Franklin First. Lamar Second.
			IF NOT bUseAlternate
				IF bForceLeft
					tlReturn = "ENTER_FL_V3_CAM_LEFT"
				ELSE
					tlReturn = "ENTER_FL_V3_CAM_RIGHT"
				ENDIF
			ELSE
				IF bForceLeft
					tlReturn = "ENTER_LF_V3_CAM_LEFT"
				ELSE
					tlReturn = "ENTER_LF_V3_CAM_RIGHT"
				ENDIF
			ENDIF
		BREAK
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_V4_UNDER_FIRE_LEFT
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_V4_UNDER_FIRE_RIGHT
			// Franklin First. Lamar Second.
			IF NOT bUseAlternate
				IF bForceLeft
					tlReturn = "ENTER_FL_V4_CAM_LEFT"
				ELSE
					tlReturn = "ENTER_FL_V4_CAM_RIGHT"
				ENDIF
			ELSE
				IF bForceLeft
					tlReturn = "ENTER_LF_V4_CAM_LEFT"
				ELSE
					tlReturn = "ENTER_LF_V4_CAM_RIGHT"
				ENDIF
			ENDIF
		BREAK
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_ELEVATOR_BUTTON_PRESS_RIGHT
			tlReturn = "elev_1_cam"
		BREAK
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_IAA_INTRO_SCENE
			tlReturn = "ACTION_CAM"
		BREAK			
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_GARAGE_BOTH_HANDS
			tlReturn = "KNEEL_DOWN_01_CAM"
		BREAK
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_GARAGE_LEFT_HAND
			tlReturn = "KNEEL_DOWN_02_CAM"
		BREAK
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_GARAGE_RIGHT_HAND
			tlReturn = "KNEEL_DOWN_03_CAM"
		BREAK
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_GOING_UNDERGROUND_HATCH_EXIT
			tlReturn = "EXIT_CAM"
		BREAK
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_GOING_UNDERGROUND_HATCH_ENTER
			tlReturn = "ENTER_CAM"
		BREAK
	ENDSWITCH	
	RETURN tlReturn
ENDFUNC

FUNC STRING GET_ANIM_DICT_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE(SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE eType, BOOL bAlternate)
	SWITCH eType
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_CASINO_GRUPPE_SECHS_ENTRY					RETURN "ANIM@CASINO@ANIMATED_CAMS@"
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_CASINO_APARTMENT_TRANS_LEFT 					RETURN "ANIM@APT_TRANS@HINGE_L"
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_CASINO_APARTMENT_TRANS_RIGHT					RETURN "ANIM@APT_TRANS@HINGE_R"
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_DOOR_TRANS_HINGE_L 							RETURN "anim@door_trans@hinge_l@"
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_DOOR_TRANS_HINGE_R 							RETURN "anim@door_trans@hinge_r@"
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_DOOR_TRANS_HINGE_L_CHARGE					RETURN "anim@door_trans@hinge_l@"
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_STEAL_YOUGA_VAN								RETURN "anim@scripted@short_trip@fixf_trip2_ig1_van_exit@"
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_SPECIAL_LEFT				RETURN "ANIM@SCRIPTED@SHORT_TRIP@FIXF_TRIP_ENTRY@"
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_SPECIAL_RIGHT				RETURN "ANIM@SCRIPTED@SHORT_TRIP@FIXF_TRIP_ENTRY@"
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_V1_NO_THREAT_LEFT			RETURN "ANIM@SCRIPTED@SHORT_TRIP@FIXF_TRIP_ENTRY@"
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_V1_NO_THREAT_RIGHT		RETURN "ANIM@SCRIPTED@SHORT_TRIP@FIXF_TRIP_ENTRY@"
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_V2_THREAT_ENDED_LEFT		RETURN "ANIM@SCRIPTED@SHORT_TRIP@FIXF_TRIP_ENTRY@"
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_V2_THREAT_ENDED_RIGHT		RETURN "ANIM@SCRIPTED@SHORT_TRIP@FIXF_TRIP_ENTRY@"
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_V3_THREAT_PRESENT_LEFT	RETURN "ANIM@SCRIPTED@SHORT_TRIP@FIXF_TRIP_ENTRY@"
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_V3_THREAT_PRESENT_RIGHT   RETURN "ANIM@SCRIPTED@SHORT_TRIP@FIXF_TRIP_ENTRY@"
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_V4_UNDER_FIRE_LEFT		RETURN "ANIM@SCRIPTED@SHORT_TRIP@FIXF_TRIP_ENTRY@"
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_V4_UNDER_FIRE_RIGHT		RETURN "ANIM@SCRIPTED@SHORT_TRIP@FIXF_TRIP_ENTRY@"
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_GARAGE_DOOR_SOLO								RETURN "anim@apt_trans@garage"	
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_ELEVATOR_BUTTON_PRESS_LEFT					RETURN "anim@apt_trans@elevator"
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_ELEVATOR_BUTTON_PRESS_RIGHT					RETURN "anim@apt_trans@elevator"			
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_IAA_INTRO_SCENE								
			RETURN "ANIM@SCRIPTED@ULP_MISSIONS@IAA_INTRO@"
		BREAK
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_GARAGE_BOTH_HANDS
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_GARAGE_LEFT_HAND
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_GARAGE_RIGHT_HAND
			IF bAlternate
				RETURN "ANIM@SCRIPTED@ULP_MISSIONS@GARAGEOPEN@HEELED@"
			ELSE
				RETURN "ANIM@SCRIPTED@ULP_MISSIONS@GARAGEOPEN@MALE@"
			ENDIF
		BREAK
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_GOING_UNDERGROUND_HATCH_EXIT					RETURN "ANIM@SCRIPTED@ULP_MISSIONS@HATCH@HATCH_EXIT@MALE@"		
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_GOING_UNDERGROUND_HATCH_ENTER				RETURN "ANIM@SCRIPTED@ULP_MISSIONS@HATCH@HATCH_ENTER@MALE@"		
	ENDSWITCH
	RETURN ""
ENDFUNC

FUNC FLOAT GET_PHASE_START_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE(SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE eType)
	FLOAT fReturn
	
	SWITCH eType
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_ELEVATOR_BUTTON_PRESS_LEFT
			fReturn = 0.14
		BREAK
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_ELEVATOR_BUTTON_PRESS_RIGHT
			fReturn = 0.621
		BREAK
		DEFAULT
			fReturn = 0.0
		BREAK
	ENDSWITCH
	
	RETURN fReturn
ENDFUNC

FUNC FLOAT GET_PHASE_END_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE(SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE eType)
	FLOAT fReturn
	
	SWITCH eType	
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_ELEVATOR_BUTTON_PRESS_LEFT
			fReturn = 0.34
		BREAK		
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_ELEVATOR_BUTTON_PRESS_RIGHT
			fReturn = 0.9
		BREAK
		DEFAULT
			fReturn = 1.0
		BREAK
	ENDSWITCH
	
	RETURN fReturn
ENDFUNC

FUNC BOOL SHOULD_SCRIPTED_CUTSCENE_SYNC_SCENE_BREAKOUT(SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE eType, PED_INDEX &pedCutscene[], INT iSyncScene)
	
	IF NOT IS_SYNC_SCENE_RUNNING(iSyncScene)
		RETURN FALSE
	ENDIF
	
	FLOAT fPhase = GET_SYNC_SCENE_PHASE(iSyncScene)
	INT iPlayersInCutscene
	INT i
	FOR i = 0 TO FMMC_MAX_CUTSCENE_PLAYERS-1		
		IF DOES_ENTITY_EXIST(pedCutscene[i])
			iPlayersInCutscene++
		ENDIF
	ENDFOR
		
	SWITCH eType
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_DOOR_TRANS_HINGE_L
			SWITCH iPlayersInCutscene
				CASE 1
					IF fPhase >= 0.56						
						RETURN TRUE
					ENDIF
				BREAK
				CASE 2
					IF fPhase >= 0.71
						RETURN TRUE
					ENDIF
				BREAK
				CASE 3
					IF fPhase >= 0.858
						RETURN TRUE
					ENDIF
				BREAK
				CASE 4
					IF fPhase >= 0.90
						RETURN TRUE
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_DOOR_TRANS_HINGE_R
			SWITCH iPlayersInCutscene
				CASE 1
					IF fPhase >= 0.56
						RETURN TRUE
					ENDIF
				BREAK
				CASE 2
					IF fPhase >= 0.71
						RETURN TRUE
					ENDIF
				BREAK
				CASE 3
					IF fPhase >= 0.858
						RETURN TRUE
					ENDIF
				BREAK
				CASE 4
					IF fPhase >= 0.90
						RETURN TRUE
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_DOOR_TRANS_HINGE_L_CHARGE
			SWITCH iPlayersInCutscene
				CASE 1
					IF fPhase >= 0.56
						RETURN TRUE
					ENDIF
				BREAK
				CASE 2
					IF fPhase >= 0.71
						RETURN TRUE
					ENDIF
				BREAK
				CASE 3
					IF fPhase >= 0.858
						RETURN TRUE
					ENDIF
				BREAK
				CASE 4
					IF fPhase >= 0.90
						RETURN TRUE
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_STEAL_YOUGA_VAN
			IF fPhase >= 0.75
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_SPECIAL_LEFT
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_SPECIAL_RIGHT
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_V1_NO_THREAT_LEFT
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_V1_NO_THREAT_RIGHT
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_V2_THREAT_ENDED_LEFT
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_V2_THREAT_ENDED_RIGHT
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_V3_THREAT_PRESENT_LEFT
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_V3_THREAT_PRESENT_RIGHT
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_V4_UNDER_FIRE_LEFT
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_V4_UNDER_FIRE_RIGHT
			IF fPhase >= 0.90
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_GARAGE_DOOR_SOLO
			IF fPhase >= 0.9
				RETURN TRUE
			ENDIF
		BREAK
			
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_GOING_UNDERGROUND_HATCH_EXIT
			IF fPhase >= 0.975
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_GARAGE_BOTH_HANDS
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_GARAGE_LEFT_HAND
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_GARAGE_RIGHT_HAND
			IF fPhase >= 0.9
				RETURN TRUE
			ENDIF
		BREAK		
	ENDSWITCH
	
	RETURN fPhase >= 1.0
ENDFUNC

FUNC FLOAT GET_SCRIPTED_CUTSCENE_SYNC_SCENE_HEADING_OFFSET(SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE eType)
	
	SWITCH eType
		
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_GARAGE_DOOR_SOLO
			RETURN 90.0
		BREAK
		
	ENDSWITCH
	
	RETURN 0.0
ENDFUNC

FUNC BOOL SHOULD_SCRIPTED_CUTSCENE_SYNC_SCENE_HOLD_LAST_FRAME(SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE eType)
	
	SWITCH eType
		
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_GARAGE_DOOR_SOLO
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_IAA_INTRO_SCENE
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_GOING_UNDERGROUND_HATCH_EXIT
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_GOING_UNDERGROUND_HATCH_ENTER
			RETURN TRUE
		BREAK
		
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC STRING GET_CONVERSATION_BLOCK_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE(SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE eType)
	STRING tlReturn
	
	SWITCH eType	
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_IAA_INTRO_SCENE
			tlReturn = "SM2ULAU"
		BREAK
	ENDSWITCH
	
	RETURN tlReturn
ENDFUNC

FUNC STRING GET_CONVERSATION_ROOT_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE(SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE eType)
	STRING tlReturn
	
	SWITCH eType	
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_IAA_INTRO_SCENE
			tlReturn = "SM2UL_SIS_1A"
		BREAK
	ENDSWITCH
	
	RETURN tlReturn
ENDFUNC

FUNC STRING GET_CONVERSATION_PED_NAME_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE(SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE eType, INT iIndex)
	STRING tlReturn
	
	SWITCH eType	
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_IAA_INTRO_SCENE
			SWITCH iIndex
				CASE 0	tlReturn = "UNITEDPAPER"		BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN tlReturn
ENDFUNC

FUNC INT GET_CONVERSATION_VOICE_SPEAKER_ID_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE(SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE eType, INT iIndex)
	SWITCH eType	
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_IAA_INTRO_SCENE
			SWITCH iIndex
				CASE 0		RETURN 2
			ENDSWITCH
		BREAK
	ENDSWITCH	
	RETURN -1
ENDFUNC

FUNC INT GET_CONVERSATION_PED_INDEX_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE(SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE eType, INT iIndex, INT iCutscene)	
	SWITCH eType		
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_IAA_INTRO_SCENE
			INT iPed
			INT i
			STRING animDict, animName
			FOR i = 0 TO FMMC_MAX_CUTSCENE_ENTITIES-1
				IF g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].iType = -1
				OR g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].iIndex = -1
					RELOOP
				ENDIF
				IF g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].iType != CREATION_TYPE_PEDS				
					RELOOP
				ENDIF
			
				animDict = GET_ANIM_DICT_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE(eType, FALSE)
				animName = GET_ANIM_NAME_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE_PED(eType, iPed)
				IF IS_STRING_NULL_OR_EMPTY(animName)
				OR IS_STRING_NULL_OR_EMPTY(animDict)
					RELOOP
				ENDIF
				IF iIndex = iPed
					RETURN g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].iIndex
				ENDIF
				iPed++
			ENDFOR
		BREAK
	ENDSWITCH
	RETURN -1
ENDFUNC

FUNC CUTSCENE_PLAYBACK_FLAGS GET_CUTSCENE_PLAYBACK_FLAGS()
	
	CUTSCENE_PLAYBACK_FLAGS eFlags = CUTSCENE_REQUESTED_IN_MISSION
	
	IF g_bFMMC_ForceCutsceneFlag
		eFlags = eFlags | CUTSCENE_PLAYBACK_FORCE_LOAD_AUDIO_EVENT // Fixes the cutscene pausing playback - url:bugstar:7442946 - Short Trip 2 - Fire It Up - When players approached the mid mission cutscene after Lamar won the mission did not progress
		PRINTLN("[Cutscenes_Mocap] GET_CUTSCENE_PLAYBACK_FLAGS - Flag added. ", ENUM_TO_INT(eFlags))
	ENDIF
	
	RETURN eFlags
	
ENDFUNC

FUNC BOOL SHOULD_SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_USE_A_VEHICLE(SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE eType)

	SWITCH eType
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_CASINO_GRUPPE_SECHS_ENTRY
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_STEAL_YOUGA_VAN
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_SCRIPTED_CUTSCENE_SCYN_SCENE_TYPE_USE_ALTERNATE_VARIATION(SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE eType, INT iCutsceneToUse, PED_INDEX pedIndex = NULL)

	SWITCH eType
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_SPECIAL_LEFT
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_SPECIAL_RIGHT
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_V1_NO_THREAT_LEFT
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_V1_NO_THREAT_RIGHT
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_V2_THREAT_ENDED_LEFT
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_V2_THREAT_ENDED_RIGHT
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_V3_THREAT_PRESENT_LEFT
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_V3_THREAT_PRESENT_RIGHT
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_V4_UNDER_FIRE_LEFT
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_V4_UNDER_FIRE_RIGHT
			// Team 1 is Lamar
			IF iPartTriggeredLocateLast != -1
			AND MC_PlayerBD[iPartTriggeredLocateLast].iTeam = 1
				PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - SHOULD_SCRIPTED_CUTSCENE_SCYN_SCENE_TYPE_USE_ALTERNATE_VARIATION - TRUE")
				RETURN TRUE			
			ENDIF			
		BREAK
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_STEAL_YOUGA_VAN
			IF GET_SEAT_PED_IS_IN(localPlayerPed, TRUE) = VS_DRIVER
			AND MC_PlayerBD[iLocalPart].iTeam = 1
				RETURN TRUE
			ENDIF
			IF GET_SEAT_PED_IS_IN(localPlayerPed, TRUE) = VS_FRONT_RIGHT
			AND MC_PlayerBD[iLocalPart].iTeam = 0
				RETURN TRUE
			ENDIF
		BREAK
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_IAA_INTRO_SCENE
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_GARAGE_BOTH_HANDS
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_GARAGE_LEFT_HAND
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_GARAGE_RIGHT_HAND
			IF DOES_ENTITY_EXIST(pedIndex)
			AND NOT IS_PED_INJURED(pedIndex)
			AND IS_PED_WEARING_HIGH_HEELS(pedIndex)
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].iCutsceneBitSet3, ci_CSBS3_ForceAlternateVersion)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC UNLOAD_CUSTOM_SYNC_SCENE_DICTIONARIES(FMMC_CUTSCENE_SYNC_SCENE_DATA &sCutsceneSyncSceneData)

	STRING animDict = GET_ANIM_DICT_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE(sCutsceneSyncSceneData.eType, FALSE)	
	IF NOT IS_STRING_NULL_OR_EMPTY(animDict)		
		PRINTLN("[LM][Cutscenes][ScriptedAnimSyncScene] - PROCESS_SCRIPTED_CUTSCENE - eType: ", ENUM_TO_INT(sCutsceneSyncSceneData.eType), " Removing animDict: ", animDict)
		REMOVE_ANIM_DICT(animDict)		
	ENDIF
	
	animDict = GET_ANIM_DICT_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE(sCutsceneSyncSceneData.eType, TRUE)																
	IF NOT IS_STRING_NULL_OR_EMPTY(animDict)
		PRINTLN("[LM][Cutscenes][ScriptedAnimSyncScene] - PROCESS_SCRIPTED_CUTSCENE - eType: ", ENUM_TO_INT(sCutsceneSyncSceneData.eType), " Removing animDict: ", animDict, "(alt)")
		REMOVE_ANIM_DICT(animDict)		
	ENDIF
	
ENDPROC

FUNC BOOL PRELOAD_CUSTOM_SYNC_SCENE_DICTIONARIES(FMMC_CUTSCENE_SYNC_SCENE_DATA &sCutsceneSyncSceneData)
				
	STRING animDict = GET_ANIM_DICT_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE(sCutsceneSyncSceneData.eType, FALSE)					
	REQUEST_ANIM_DICT(animDict)
	IF NOT HAS_ANIM_DICT_LOADED(animDict)
		PRINTLN("[LM][Cutscenes][ScriptedAnimSyncScene] - PROCESS_SCRIPTED_CUTSCENE - eType: ", ENUM_TO_INT(sCutsceneSyncSceneData.eType), " Loading animDict: ", animDict)
		RETURN FALSE
	ENDIF
	
	STRING animDictAlt = GET_ANIM_DICT_FOR_SCRIPTED_CUTSCENE_SYNC_SCENE(sCutsceneSyncSceneData.eType, TRUE)					
	IF NOT ARE_STRINGS_EQUAL(animDict, animDictAlt)
		REQUEST_ANIM_DICT(animDictAlt)
		IF NOT HAS_ANIM_DICT_LOADED(animDictAlt)
			PRINTLN("[LM][Cutscenes][ScriptedAnimSyncScene] - PROCESS_SCRIPTED_CUTSCENE - eType: ", ENUM_TO_INT(sCutsceneSyncSceneData.eType), " Loading animDictAlt: ", animDictAlt)
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE GET_MODIFIED_CUTSCENE_SYNC_SCENE_TYPE_BASED_ON_GAMEPLAY(SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE eType)
	
	INT i
	INT iPedIndexes[5]
	PED_INDEX pedIndexes[5]
	
	SWITCH eType
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_SPECIAL_LEFT
		CASE SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_SPECIAL_RIGHT
			PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - GET_MODIFIED_CUTSCENE_SYNC_SCENE_TYPE_BASED_ON_GAMEPLAY - Using a dynamic scene type - SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_SPECIAL")
			
			INT iThreatLevel
			VECTOR vPlayerPos
			
			FOR i = 0 TO COUNT_OF(iPedIndexes)-1
				iPedIndexes[i] = i
			ENDFOR
			
			vPlayerPos = GET_ENTITY_COORDS(LocalPlayerPed)
			
			GET_NTH_ARRAY_CLOSEST_PED_TO_COORD(vPlayerPos, iPedIndexes)
			
			NETWORK_INDEX netPed
			FOR i = 0 TO COUNT_OF(iPedIndexes)-1
												
				IF iPedIndexes[i] = -1
					PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - GET_MODIFIED_CUTSCENE_SYNC_SCENE_TYPE_BASED_ON_GAMEPLAY - Array i: ", i, " Ped didn't get a reference. Is: ", iPedIndexes[i])
					RELOOP
				ENDIF
				
				PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - GET_MODIFIED_CUTSCENE_SYNC_SCENE_TYPE_BASED_ON_GAMEPLAY - Checking Ped Index: ", iPedIndexes[i], " ---------------------")
								
				netPed = MC_serverBD_1.sFMMC_SBD.niPed[iPedIndexes[i]]
				
				IF NOT NETWORK_DOES_NETWORK_ID_EXIST(netPed)
					PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - GET_MODIFIED_CUTSCENE_SYNC_SCENE_TYPE_BASED_ON_GAMEPLAY - Reloop NOT NETWORK_DOES_NETWORK_ID_EXIST.")
					RELOOP
				ENDIF
				
				pedIndexes[i] = NET_TO_PED(netPed)
				
				IF VDIST2(GET_ENTITY_COORDS(pedIndexes[i], FALSE), vPlayerPos) >= POW(40.0, 2.0)					
					PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - GET_MODIFIED_CUTSCENE_SYNC_SCENE_TYPE_BASED_ON_GAMEPLAY - Reloop > 40m away.")
					RELOOP
				ENDIF
				
				// Literally just to prevent an assert... We do want to check a corpse here. Tested and GET_RELATIONSHIP_BETWEEN_PEDS returns the expected value with corpses even when set as no longer needed.
				#IF IS_DEBUG_BUILD
				IF IS_PED_INJURED(pedIndexes[i])
				
				ENDIF
				#ENDIF
				
				RELATIONSHIP_TYPE RelType
				RelType = GET_RELATIONSHIP_BETWEEN_PEDS(LocalPlayerPed, pedIndexes[i])
				IF RelType = ACQUAINTANCE_TYPE_PED_RESPECT
				OR RelType = ACQUAINTANCE_TYPE_PED_LIKE
					PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - GET_MODIFIED_CUTSCENE_SYNC_SCENE_TYPE_BASED_ON_GAMEPLAY - Nearby Ped is a friend.")
					RELOOP
				ENDIF
				
				IF iThreatLevel < 2
					PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - GET_MODIFIED_CUTSCENE_SYNC_SCENE_TYPE_BASED_ON_GAMEPLAY - Upping Threat Level to 1. There are nearby peds or dead peds we have killed.")
					iThreatLevel = 1
				ENDIF
				
				IF IS_PED_INJURED(pedIndexes[i])
					PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - GET_MODIFIED_CUTSCENE_SYNC_SCENE_TYPE_BASED_ON_GAMEPLAY - Ped is dead.")
					RELOOP 
				ENDIF
				
				IF iThreatLevel < 3
					PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - GET_MODIFIED_CUTSCENE_SYNC_SCENE_TYPE_BASED_ON_GAMEPLAY - Upping Threat Level to 2. There are nearby alive enemy peds.")
					iThreatLevel = 2
				ENDIF
				
				/* url:bugstar:7439500
				IF NOT HAS_ENTITY_CLEAR_LOS_TO_ENTITY(pedIndexes[i], LocalPlayerPed, SCRIPT_INCLUDE_MOVER|SCRIPT_INCLUDE_VEHICLE|SCRIPT_INCLUDE_PED|SCRIPT_INCLUDE_RAGDOLL|SCRIPT_INCLUDE_OBJECT|SCRIPT_INCLUDE_FOLIAGE)					
					PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - GET_MODIFIED_CUTSCENE_SYNC_SCENE_TYPE_BASED_ON_GAMEPLAY - No LOS.")
					RELOOP
				ENDIF
				*/
				
				IF IS_PED_SHOOTING(pedIndexes[i])
				OR GET_PED_CONFIG_FLAG(pedIndexes[i], PCF_IsAimingGun)
					PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - GET_MODIFIED_CUTSCENE_SYNC_SCENE_TYPE_BASED_ON_GAMEPLAY - Upping Threat Level to 3. Ped is shooting at us..")
					iThreatLevel = 3
				ENDIF
			ENDFOR
			
			SWITCH iThreatLevel
				// No Threat
				CASE 0
					PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - GET_MODIFIED_CUTSCENE_SYNC_SCENE_TYPE_BASED_ON_GAMEPLAY - Using V1 - No Threat.")
					IF eType = SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_SPECIAL_LEFT
						eType = SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_V1_NO_THREAT_LEFT
					ELSE
						eType = SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_V1_NO_THREAT_RIGHT
					ENDIF
				BREAK				
				
				// Threat Ended
				CASE 1
					PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - GET_MODIFIED_CUTSCENE_SYNC_SCENE_TYPE_BASED_ON_GAMEPLAY - Using V2 - Threat Ended.")
					IF eType = SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_SPECIAL_LEFT
						eType = SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_V2_THREAT_ENDED_LEFT
					ELSE
						eType = SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_V2_THREAT_ENDED_RIGHT
					ENDIF
				BREAK				
				
				// Threat Present
				CASE 2
					PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - GET_MODIFIED_CUTSCENE_SYNC_SCENE_TYPE_BASED_ON_GAMEPLAY - Using V3 - Threat Present.")
					IF eType = SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_SPECIAL_LEFT
						eType = SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_V3_THREAT_PRESENT_LEFT
					ELSE
						eType = SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_V3_THREAT_PRESENT_RIGHT
					ENDIF
				BREAK
				
				// Under Fire
				CASE 3
					PRINTLN("[Cutscene_Scripted] PROCESS_SCRIPTED_CUTSCENE - GET_MODIFIED_CUTSCENE_SYNC_SCENE_TYPE_BASED_ON_GAMEPLAY - Using V4 - Under Fire.")
					IF eType = SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_SPECIAL_LEFT						
						eType = SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_V4_UNDER_FIRE_LEFT
					ELSE
						eType = SCRIPTED_CUTSCENE_SYNC_SCENE_TYPE_FRANKLIN_AND_LAMAR_V4_UNDER_FIRE_RIGHT					
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN eType
ENDFUNC

PROC SET_CUTSCENE_ANIM_EVENT_STAGE(CUTSCENE_ANIM_EVENT_STAGE eCutsceneAnimEventStageNew)
	PRINTLN("[Cutscenes] - SET_CUTSCENE_ANIM_EVENT_STAGE - Old: ", ENUM_TO_INT(eCutsceneAnimEventStage), " New: ", ENUM_TO_INT(eCutsceneAnimEventStageNew))
	eCutsceneAnimEventStage = eCutsceneAnimEventStageNew
ENDPROC

PROC PROCESS_CUTSCENE_EVENTS_FOR_FIX_TRIP3_EXT()
	PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_TRIP3_EXT Processing Events.")
	
	ENTITY_INDEX eiLamarVan =  GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("Lamar_Weed_Van")
	IF NOT IS_BIT_SET(iSpecificCutsceneBitset2, ciSpecificCutsceneBitset2_FIX_TRIP3_EXT_FixVan)
	AND IS_ENTITY_ALIVE(eiLamarVan)
		VEHICLE_INDEX vehIndex = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(eiLamarVan)		
		IF NOT NETWORK_GET_ENTITY_IS_NETWORKED(vehIndex)
		OR NETWORK_HAS_CONTROL_OF_ENTITY(vehIndex)
			SET_VEHICLE_FIXED(vehIndex)	
			PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_TRIP3_EXT Fixing VAN!")
		ENDIF
		SET_BIT(iSpecificCutsceneBitset2, ciSpecificCutsceneBitset2_FIX_TRIP3_EXT_FixVan)
	ENDIF
							
	ENTITY_INDEX oiPhoneCutscene 
	ENTITY_INDEX eiLamar =  GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("FIX_LAMAR")
	IF IS_ENTITY_ALIVE(eiLamar)
		IF HAS_ANIM_EVENT_FIRED(eiLamar, GET_HASH_KEY("Heist4CameraFlash"))		
		AND NOT IS_BIT_SET(iSpecificCutsceneBitset, ciSpecificCutsceneBitset_FIX_TRIP3_EXT_Flash1)		
			ANIMPOSTFX_PLAY("Heist4CameraFlash", 0, FALSE)				
			TOGGLE_PAUSED_RENDERPHASES(FALSE)
			PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_TRIP3_EXT Calling ANIMPOSTFX_PLAY('Heist4CameraFlash').")
			SET_BIT(iSpecificCutsceneBitset, ciSpecificCutsceneBitset_FIX_TRIP3_EXT_Flash1)
			MC_START_NET_TIMER_WITH_TIMESTAMP(iCutscenePostFXTimeStamp)
		ENDIF
		IF HAS_ANIM_EVENT_FIRED(eiLamar, GET_HASH_KEY("Heist4CameraFlash2"))	
		AND NOT IS_BIT_SET(iSpecificCutsceneBitset, ciSpecificCutsceneBitset_FIX_TRIP3_EXT_Flash2)						
			ANIMPOSTFX_PLAY("Heist4CameraFlash", 0, FALSE)			
			TOGGLE_PAUSED_RENDERPHASES(FALSE)
			PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_TRIP3_EXT Calling ANIMPOSTFX_PLAY('Heist4CameraFlash2').")
			SET_BIT(iSpecificCutsceneBitset, ciSpecificCutsceneBitset_FIX_TRIP3_EXT_Flash2)
			MC_START_NET_TIMER_WITH_TIMESTAMP(iCutscenePostFXTimeStamp)
		ENDIF	
		IF HAS_ANIM_EVENT_FIRED(eiLamar, GET_HASH_KEY("Heist4CameraFlash3"))
		AND NOT IS_BIT_SET(iSpecificCutsceneBitset, ciSpecificCutsceneBitset_FIX_TRIP3_EXT_Flash3)			
			ANIMPOSTFX_PLAY("Heist4CameraFlash", 0, FALSE)				
			TOGGLE_PAUSED_RENDERPHASES(FALSE)
			PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_TRIP3_EXT Calling ANIMPOSTFX_PLAY('Heist4CameraFlash3').")
			SET_BIT(iSpecificCutsceneBitset, ciSpecificCutsceneBitset_FIX_TRIP3_EXT_Flash3)
			MC_START_NET_TIMER_WITH_TIMESTAMP(iCutscenePostFXTimeStamp)	
		ENDIF
		
		IF MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(iCutscenePostFXTimeStamp)
		AND MC_HAS_NET_TIMER_EXPIRED_WITH_TIME_STAMP(iCutscenePostFXTimeStamp, 1300)
			MC_STOP_NET_TIMER_WITH_TIMESTAMP(iCutscenePostFXTimeStamp)
			TOGGLE_PAUSED_RENDERPHASES(TRUE)
			ANIMPOSTFX_STOP_ALL()
			PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_TRIP3_EXT Calling ANIMPOSTFX_STOP_ALL")
		ENDIF
	ENDIF
	
	// PHONE 0 - render target	
	IF NOT DOES_ENTITY_EXIST(eiSpecificCutsceneEntities[ciSpecificCutsceneEntityIndexes_FIX_TRIP3_EXT_PhoneScreen0])
		PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_TRIP3_EXT - Phone 0 - Requesting: sf_Prop_SF_Scrn_TR_01a")
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_Scrn_TR_01a")))
		IF HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_Scrn_TR_01a")))
			eiSpecificCutsceneEntities[ciSpecificCutsceneEntityIndexes_FIX_TRIP3_EXT_PhoneScreen0] = CREATE_OBJECT(INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_Scrn_TR_01a")), <<10, 10, 73>>, FALSE, FALSE)
			SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_Scrn_TR_01a")))
			PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_TRIP3_EXT - Phone 0 - Created phone. sf_Prop_SF_Scrn_TR_01a")
		ELSE
			PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_TRIP3_EXT - Phone 0 - Waiting for phone model. sf_Prop_SF_Scrn_TR_01a")
		ENDIF
	ENDIF		
	oiPhoneCutscene = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("Crowd1_Phone", INT_TO_ENUM(MODEL_NAMES, HASH("Prop_Phone_ING")))		
	IF DOES_ENTITY_EXIST(oiPhoneCutscene)
	AND DOES_ENTITY_EXIST(eiSpecificCutsceneEntities[ciSpecificCutsceneEntityIndexes_FIX_TRIP3_EXT_PhoneScreen0])
	
		PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_TRIP3_EXT - Phone 0 - Phone exists.")
					
		IF NOT IS_ENTITY_ATTACHED_TO_ENTITY(eiSpecificCutsceneEntities[ciSpecificCutsceneEntityIndexes_FIX_TRIP3_EXT_PhoneScreen0], oiPhoneCutscene)	
			SET_ENTITY_COORDS(eiSpecificCutsceneEntities[ciSpecificCutsceneEntityIndexes_FIX_TRIP3_EXT_PhoneScreen0], GET_ENTITY_COORDS(oiPhoneCutscene, FALSE))
			ATTACH_ENTITY_TO_ENTITY(eiSpecificCutsceneEntities[ciSpecificCutsceneEntityIndexes_FIX_TRIP3_EXT_PhoneScreen0], oiPhoneCutscene, 0, <<0.0, 0.0, 0.0>>, <<90.0, 0.0, 0.0>>)
			PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_TRIP3_EXT - Phone 0 - Attaching phone screen to phone cutscene entity")
		ENDIF
		
	ELSE
		PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_TRIP3_EXT - Phone 0 - Phone Entity Does not Exist.")
	ENDIF	
	#IF IS_DEBUG_BUILD
	IF IS_DEBUG_KEY_PRESSED(KEY_7, KEYBOARD_MODIFIER_NONE, "")
		DRAW_DEBUG_TEXT_ABOVE_ENTITY(oiPhoneCutscene, "!! PHONE 0 IS HERE !!", 1.0)
	ENDIF
	#ENDIF
	
	
	
	// PHONE 1 #################
	IF NOT DOES_ENTITY_EXIST(eiSpecificCutsceneEntities[ciSpecificCutsceneEntityIndexes_FIX_TRIP3_EXT_PhoneScreen1])
		PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_TRIP3_EXT - Phone 1 - Requesting: sf_Prop_SF_Scrn_TR_02a ")
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_Scrn_TR_02a")))
		IF HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_Scrn_TR_02a")))
			eiSpecificCutsceneEntities[ciSpecificCutsceneEntityIndexes_FIX_TRIP3_EXT_PhoneScreen1] = CREATE_OBJECT(INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_Scrn_TR_02a")), <<10, 10, 73>>, FALSE, FALSE)
			SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_Scrn_TR_02a")))
			PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_TRIP3_EXT - Phone 1 - Created phone. sf_Prop_SF_Scrn_TR_02a")
		ELSE
			PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_TRIP3_EXT - Phone 1 - Waiting for phone model. sf_Prop_SF_Scrn_TR_02a")
		ENDIF
	ENDIF		
	oiPhoneCutscene = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("Crowd2_Phone", INT_TO_ENUM(MODEL_NAMES, HASH("Prop_Phone_ING")))		
	IF DOES_ENTITY_EXIST(oiPhoneCutscene)
	AND DOES_ENTITY_EXIST(eiSpecificCutsceneEntities[ciSpecificCutsceneEntityIndexes_FIX_TRIP3_EXT_PhoneScreen1])
	
		PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_TRIP3_EXT - Phone 1 - Phone exists.")
					
		IF NOT IS_ENTITY_ATTACHED_TO_ENTITY(eiSpecificCutsceneEntities[ciSpecificCutsceneEntityIndexes_FIX_TRIP3_EXT_PhoneScreen1], oiPhoneCutscene)	
			SET_ENTITY_COORDS(eiSpecificCutsceneEntities[ciSpecificCutsceneEntityIndexes_FIX_TRIP3_EXT_PhoneScreen1], GET_ENTITY_COORDS(oiPhoneCutscene, FALSE))
			ATTACH_ENTITY_TO_ENTITY(eiSpecificCutsceneEntities[ciSpecificCutsceneEntityIndexes_FIX_TRIP3_EXT_PhoneScreen1], oiPhoneCutscene, 0, <<0.0, 0.0, 0.0>>, <<90.0, 0.0, 0.0>>)
			PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_TRIP3_EXT - Phone 1 - Attaching phone screen to phone cutscene entity")
		ENDIF
		
	ELSE
		PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_TRIP3_EXT - Phone 1 - Phone Entity Does not Exist.")
	ENDIF	
	#IF IS_DEBUG_BUILD
	IF IS_DEBUG_KEY_PRESSED(KEY_7, KEYBOARD_MODIFIER_NONE, "")
		DRAW_DEBUG_TEXT_ABOVE_ENTITY(oiPhoneCutscene, "!! PHONE 1 IS HERE !!", 1.0)
	ENDIF
	#ENDIF
			
			
			
	// PHONE 2 #################
	IF NOT DOES_ENTITY_EXIST(eiSpecificCutsceneEntities[ciSpecificCutsceneEntityIndexes_FIX_TRIP3_EXT_PhoneScreen2])
		PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_TRIP3_EXT - Phone 2 - Requesting: sf_Prop_SF_Scrn_TR_03a ")
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_Scrn_TR_03a")))
		IF HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_Scrn_TR_03a")))
			eiSpecificCutsceneEntities[ciSpecificCutsceneEntityIndexes_FIX_TRIP3_EXT_PhoneScreen2] = CREATE_OBJECT(INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_Scrn_TR_03a")), <<10, 10, 73>>, FALSE, FALSE)
			SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_Scrn_TR_03a")))
			PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_TRIP3_EXT - Phone 2 - Created phone. sf_Prop_SF_Scrn_TR_03a")
		ELSE
			PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_TRIP3_EXT - Phone 2 - Waiting for phone model. sf_Prop_SF_Scrn_TR_03a")
		ENDIF
	ENDIF		
	oiPhoneCutscene = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("Crowd3_Phone", INT_TO_ENUM(MODEL_NAMES, HASH("Prop_Phone_ING")))		
	IF DOES_ENTITY_EXIST(oiPhoneCutscene)
	AND DOES_ENTITY_EXIST(eiSpecificCutsceneEntities[ciSpecificCutsceneEntityIndexes_FIX_TRIP3_EXT_PhoneScreen2])
	
		PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_TRIP3_EXT - Phone 2 - Phone exists.")
					
		IF NOT IS_ENTITY_ATTACHED_TO_ENTITY(eiSpecificCutsceneEntities[ciSpecificCutsceneEntityIndexes_FIX_TRIP3_EXT_PhoneScreen2], oiPhoneCutscene)	
			SET_ENTITY_COORDS(eiSpecificCutsceneEntities[ciSpecificCutsceneEntityIndexes_FIX_TRIP3_EXT_PhoneScreen2], GET_ENTITY_COORDS(oiPhoneCutscene, FALSE))
			ATTACH_ENTITY_TO_ENTITY(eiSpecificCutsceneEntities[ciSpecificCutsceneEntityIndexes_FIX_TRIP3_EXT_PhoneScreen2], oiPhoneCutscene, 0, <<0.0, 0.0, 0.0>>, <<90.0, 0.0, 0.0>>)
			PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_TRIP3_EXT - Phone 2 - Attaching phone screen to phone cutscene entity")
		ENDIF
		
	ELSE
		PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_TRIP3_EXT - Phone 2 - Phone Entity Does not Exist.")
	ENDIF	
	#IF IS_DEBUG_BUILD
	IF IS_DEBUG_KEY_PRESSED(KEY_7, KEYBOARD_MODIFIER_NONE, "")
		DRAW_DEBUG_TEXT_ABOVE_ENTITY(oiPhoneCutscene, "!! PHONE 2 IS HERE !!", 1.0)
	ENDIF
	#ENDIF
	
			
			
	// PHONE 3 #################
	IF NOT DOES_ENTITY_EXIST(eiSpecificCutsceneEntities[ciSpecificCutsceneEntityIndexes_FIX_TRIP3_EXT_PhoneScreen3])
		PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_TRIP3_EXT - Phone 3 - Requesting: sf_Prop_SF_Scrn_TR_04a ")
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_Scrn_TR_04a")))
		IF HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_Scrn_TR_04a")))
			eiSpecificCutsceneEntities[ciSpecificCutsceneEntityIndexes_FIX_TRIP3_EXT_PhoneScreen3] = CREATE_OBJECT(INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_Scrn_TR_04a")), <<10, 10, 73>>, FALSE, FALSE)
			SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_Scrn_TR_04a")))
			PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_TRIP3_EXT - Phone 3 - Created phone. sf_Prop_SF_Scrn_TR_04a")
		ELSE
			PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_TRIP3_EXT - Phone 3 - Waiting for phone model. sf_Prop_SF_Scrn_TR_04a")
		ENDIF
	ENDIF		
	oiPhoneCutscene = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("Crowd4_Phone", INT_TO_ENUM(MODEL_NAMES, HASH("Prop_Phone_ING")))		
	IF DOES_ENTITY_EXIST(oiPhoneCutscene)
	AND DOES_ENTITY_EXIST(eiSpecificCutsceneEntities[ciSpecificCutsceneEntityIndexes_FIX_TRIP3_EXT_PhoneScreen3])
	
		PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_TRIP3_EXT - Phone 3 - Phone exists.")
					
		IF NOT IS_ENTITY_ATTACHED_TO_ENTITY(eiSpecificCutsceneEntities[ciSpecificCutsceneEntityIndexes_FIX_TRIP3_EXT_PhoneScreen3], oiPhoneCutscene)	
			SET_ENTITY_COORDS(eiSpecificCutsceneEntities[ciSpecificCutsceneEntityIndexes_FIX_TRIP3_EXT_PhoneScreen3], GET_ENTITY_COORDS(oiPhoneCutscene, FALSE))
			ATTACH_ENTITY_TO_ENTITY(eiSpecificCutsceneEntities[ciSpecificCutsceneEntityIndexes_FIX_TRIP3_EXT_PhoneScreen3], oiPhoneCutscene, 0, <<0.0, 0.0, 0.0>>, <<90.0, 0.0, 0.0>>)
			PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_TRIP3_EXT - Phone 3 - Attaching phone screen to phone cutscene entity")
		ENDIF
		
	ELSE
		PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_TRIP3_EXT - Phone 3 - Phone Entity Does not Exist.")
	ENDIF	
	#IF IS_DEBUG_BUILD
	IF IS_DEBUG_KEY_PRESSED(KEY_7, KEYBOARD_MODIFIER_NONE, "")
		DRAW_DEBUG_TEXT_ABOVE_ENTITY(oiPhoneCutscene, "!! PHONE 3 IS HERE !!", 1.0)
	ENDIF
	#ENDIF
	
		
	// PHONE 4 #################
	IF NOT DOES_ENTITY_EXIST(eiSpecificCutsceneEntities[ciSpecificCutsceneEntityIndexes_FIX_TRIP3_EXT_PhoneScreen4])
		PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_TRIP3_EXT - Phone 4 - Requesting: sf_Prop_SF_Scrn_TR_02a")
		REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_Scrn_TR_02a")))
		IF HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_Scrn_TR_02a")))
			eiSpecificCutsceneEntities[ciSpecificCutsceneEntityIndexes_FIX_TRIP3_EXT_PhoneScreen4] = CREATE_OBJECT(INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_Scrn_TR_02a")), <<10, 10, 73>>, FALSE, FALSE)
			SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_Scrn_TR_02a")))
			PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_TRIP3_EXT - Phone 4 - Created phone. sf_Prop_SF_Scrn_TR_02a")
		ELSE
			PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_TRIP3_EXT - Phone 4 - Waiting for phone model. sf_Prop_SF_Scrn_TR_02a")
		ENDIF
	ENDIF
	
	oiPhoneCutscene = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("Crowd5_Phone", INT_TO_ENUM(MODEL_NAMES, HASH("Prop_Phone_ING")))	
	IF DOES_ENTITY_EXIST(oiPhoneCutscene)
		IF DOES_ENTITY_EXIST(eiSpecificCutsceneEntities[ciSpecificCutsceneEntityIndexes_FIX_TRIP3_EXT_PhoneScreen4])
		
			PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_TRIP3_EXT - Phone 4 - Phone exists.")
						
			IF NOT IS_ENTITY_ATTACHED_TO_ENTITY(eiSpecificCutsceneEntities[ciSpecificCutsceneEntityIndexes_FIX_TRIP3_EXT_PhoneScreen4], oiPhoneCutscene)	
				SET_ENTITY_COORDS(eiSpecificCutsceneEntities[ciSpecificCutsceneEntityIndexes_FIX_TRIP3_EXT_PhoneScreen4], GET_ENTITY_COORDS(oiPhoneCutscene, FALSE))
				ATTACH_ENTITY_TO_ENTITY(eiSpecificCutsceneEntities[ciSpecificCutsceneEntityIndexes_FIX_TRIP3_EXT_PhoneScreen4], oiPhoneCutscene, 0, <<0.0, 0.0, 0.0>>, <<90.0, 0.0, 0.0>>)
				PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_TRIP3_EXT - Phone 4 - Attaching phone screen to phone cutscene entity")
			ENDIF
			
		ELSE
			PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_TRIP3_EXT - Phone 4 - oiPhoneCutscene Phone Entity Does not Exist.")
		ENDIF
	ELSE
		PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_TRIP3_EXT - Phone 4 - ciSpecificCutsceneEntityIndexes_FIX_TRIP3_EXT_PhoneScreen4 Phone Entity Does not Exist.")
	ENDIF		
	#IF IS_DEBUG_BUILD
	IF IS_DEBUG_KEY_PRESSED(KEY_7, KEYBOARD_MODIFIER_NONE, "")
		DRAW_DEBUG_TEXT_ABOVE_ENTITY(oiPhoneCutscene, "!! PHONE 4 IS HERE !!", 1.0)
	ENDIF
	#ENDIF

	ENTITY_INDEX entPlayer = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("FIX_FRANKLIN")	
	
	sCutsceneRenderTarget = "Scrn_TR_01a"
	STRING sBinkName
	FLOAT fCentreX = 0.5
	FLOAT fCentreY = 0.5
	FLOAT fWidth = 1.333
	FLOAT fHeight = 1.0
	
	//PRINT_LUKE_ON_SCREEN_DEBUG_TEXT("Time: ", "", vBlankDbg, GET_CUTSCENE_TIME())
	
	SWITCH eCutsceneAnimEventStage
		CASE CUTSCENE_ANIM_EVENT_STAGE_0
			
			//PRINT_LUKE_ON_SCREEN_DEBUG_TEXT("CUTSCENE_ANIM_EVENT_STAGE_0", "", vBlankDbg)
			//PRINT_LUKE_ON_SCREEN_DEBUG_TEXT(sBinkName, "", vBlankDbg)
			
			IF HAS_ANIM_EVENT_FIRED(entPlayer, GET_HASH_KEY("BINK_1"))	
			#IF IS_DEBUG_BUILD OR IS_DEBUG_KEY_PRESSED(KEY_NUMPAD5, KEYBOARD_MODIFIER_NONE, "asdfadsf") #ENDIF
				SET_CUTSCENE_ANIM_EVENT_STAGE(CUTSCENE_ANIM_EVENT_STAGE_1)
				PROCESS_CUTSCENE_EVENTS_FOR_FIX_TRIP3_EXT()
			ENDIF
		BREAK
		
		CASE CUTSCENE_ANIM_EVENT_STAGE_1	
			sBinkName = "TRIP3_PHONE1_BINK1"
				
			//PRINT_LUKE_ON_SCREEN_DEBUG_TEXT("CUTSCENE_ANIM_EVENT_STAGE_1", "", vBlankDbg)
			//PRINT_LUKE_ON_SCREEN_DEBUG_TEXT(sBinkName, "", vBlankDbg)
			
			IF NOT IS_NAMED_RENDERTARGET_REGISTERED(sCutsceneRenderTarget)
				
				PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_TRIP3_EXT - CUTSCENE_ANIM_EVENT_STAGE_1 - sCutsceneRenderTarget: ", sCutsceneRenderTarget, " Registering...")	
				
				IF REGISTER_NAMED_RENDERTARGET(sCutsceneRenderTarget)
				
					IF NOT IS_NAMED_RENDERTARGET_LINKED(INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_Scrn_TR_01a")))
					
						LINK_NAMED_RENDERTARGET(INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_Scrn_TR_01a")))
						
						iCutsceneRenderTargetID = GET_NAMED_RENDERTARGET_RENDER_ID(sCutsceneRenderTarget)
						
						PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_TRIP3_EXT - CUTSCENE_ANIM_EVENT_STAGE_1 - iCutsceneRenderTargetID: ", iCutsceneRenderTargetID)
						
						SET_BIT(iSpecificCutsceneBitset, ciSpecificCutsceneBitset_FIX_TRIP3_EXT_Play_Bink_1)							
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(iSpecificCutsceneBitset, ciSpecificCutsceneBitset_FIX_TRIP3_EXT_Play_Bink_1)
				IF bmi_Cutscene = null
					bmi_Cutscene = SET_BINK_MOVIE(sBinkName)						
					PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_TRIP3_EXT - CUTSCENE_ANIM_EVENT_STAGE_1 - Calling SET_BINK_MOVIE with sBinkName: ", sBinkName)						
				ENDIF
				IF bmi_Cutscene != null
					PLAY_BINK_MOVIE(bmi_Cutscene)
					SET_CUTSCENE_ANIM_EVENT_STAGE(CUTSCENE_ANIM_EVENT_STAGE_2)
				ENDIF
			ENDIF
		BREAK
		CASE CUTSCENE_ANIM_EVENT_STAGE_2
			sBinkName = "TRIP3_PHONE1_BINK1"
			
			//PRINT_LUKE_ON_SCREEN_DEBUG_TEXT("CUTSCENE_ANIM_EVENT_STAGE_2", "", vBlankDbg)
			//PRINT_LUKE_ON_SCREEN_DEBUG_TEXT(sBinkName, "", vBlankDbg)
			
			IF HAS_ANIM_EVENT_FIRED(entPlayer, GET_HASH_KEY("BINK_2"))	
			#IF IS_DEBUG_BUILD OR IS_DEBUG_KEY_PRESSED(KEY_NUMPAD5, KEYBOARD_MODIFIER_NONE, "asdfadsf") #ENDIF
				IF bmi_Cutscene != null
					RELEASE_BINK_MOVIE(bmi_Cutscene)
					bmi_Cutscene = NULL
					PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_TRIP3_EXT - CUTSCENE_ANIM_EVENT_STAGE_2 - Calling RELEASE_BINK_MOVIE with sBinkName: ", sBinkName)
				ENDIF
				SET_CUTSCENE_ANIM_EVENT_STAGE(CUTSCENE_ANIM_EVENT_STAGE_3)
				PROCESS_CUTSCENE_EVENTS_FOR_FIX_TRIP3_EXT()
			ENDIF
		BREAK
		
		CASE CUTSCENE_ANIM_EVENT_STAGE_3	
			sBinkName = "TRIP3_PHONE1_BINK1B"
				
			//PRINT_LUKE_ON_SCREEN_DEBUG_TEXT("CUTSCENE_ANIM_EVENT_STAGE_3", "", vBlankDbg)
			//PRINT_LUKE_ON_SCREEN_DEBUG_TEXT(sBinkName, "", vBlankDbg)
						
			IF bmi_Cutscene = null
				bmi_Cutscene = SET_BINK_MOVIE(sBinkName)
				PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_TRIP3_EXT - CUTSCENE_ANIM_EVENT_STAGE_3 - Calling SET_BINK_MOVIE with sBinkName: ", sBinkName)
			ENDIF
			IF bmi_Cutscene != null
				PLAY_BINK_MOVIE(bmi_Cutscene)
				SET_CUTSCENE_ANIM_EVENT_STAGE(CUTSCENE_ANIM_EVENT_STAGE_4)
			ENDIF
		BREAK		
		CASE CUTSCENE_ANIM_EVENT_STAGE_4
			sBinkName = "TRIP3_PHONE1_BINK1B"
			
			//PRINT_LUKE_ON_SCREEN_DEBUG_TEXT("CUTSCENE_ANIM_EVENT_STAGE_4", "", vBlankDbg)
			//PRINT_LUKE_ON_SCREEN_DEBUG_TEXT(sBinkName, "", vBlankDbg)
			
			IF HAS_ANIM_EVENT_FIRED(entPlayer, GET_HASH_KEY("BINK_3"))	
			#IF IS_DEBUG_BUILD OR IS_DEBUG_KEY_PRESSED(KEY_NUMPAD5, KEYBOARD_MODIFIER_NONE, "asdfadsf") #ENDIF
				IF bmi_Cutscene != null
					RELEASE_BINK_MOVIE(bmi_Cutscene)
					bmi_Cutscene = NULL
					PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_TRIP3_EXT - CUTSCENE_ANIM_EVENT_STAGE_4 - Calling RELEASE_BINK_MOVIE with sBinkName: ", sBinkName)
				ENDIF				
				SET_CUTSCENE_ANIM_EVENT_STAGE(CUTSCENE_ANIM_EVENT_STAGE_5)				
				PROCESS_CUTSCENE_EVENTS_FOR_FIX_TRIP3_EXT()
			ENDIF
		BREAK
		
		CASE CUTSCENE_ANIM_EVENT_STAGE_5	
			sBinkName = "TRIP3_PHONE1_BINK2"
				
			//PRINT_LUKE_ON_SCREEN_DEBUG_TEXT("CUTSCENE_ANIM_EVENT_STAGE_5", "", vBlankDbg)
			//PRINT_LUKE_ON_SCREEN_DEBUG_TEXT(sBinkName, "", vBlankDbg)
		
			IF bmi_Cutscene = null
				bmi_Cutscene = SET_BINK_MOVIE(sBinkName)
				PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_TRIP3_EXT - CUTSCENE_ANIM_EVENT_STAGE_5 - Calling SET_BINK_MOVIE with sBinkName: ", sBinkName)
			ENDIF
			IF bmi_Cutscene != null
				PLAY_BINK_MOVIE(bmi_Cutscene)
				SET_CUTSCENE_ANIM_EVENT_STAGE(CUTSCENE_ANIM_EVENT_STAGE_6)
			ENDIF
		BREAK
		
		CASE CUTSCENE_ANIM_EVENT_STAGE_6
			sBinkName = "TRIP3_PHONE1_BINK2"
			
			//PRINT_LUKE_ON_SCREEN_DEBUG_TEXT("CUTSCENE_ANIM_EVENT_STAGE_6", "", vBlankDbg)
			//PRINT_LUKE_ON_SCREEN_DEBUG_TEXT(sBinkName, "", vBlankDbg)
			
			IF HAS_ANIM_EVENT_FIRED(entPlayer, GET_HASH_KEY("BINK_4"))	
			#IF IS_DEBUG_BUILD OR IS_DEBUG_KEY_PRESSED(KEY_NUMPAD5, KEYBOARD_MODIFIER_NONE, "asdfadsf") #ENDIF
				IF bmi_Cutscene != null
					RELEASE_BINK_MOVIE(bmi_Cutscene)
					bmi_Cutscene = NULL
					PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_TRIP3_EXT - CUTSCENE_ANIM_EVENT_STAGE_6 - Calling RELEASE_BINK_MOVIE with sBinkName: ", sBinkName)
				ENDIF
				SET_CUTSCENE_ANIM_EVENT_STAGE(CUTSCENE_ANIM_EVENT_STAGE_7)
				PROCESS_CUTSCENE_EVENTS_FOR_FIX_TRIP3_EXT()
			ENDIF
		BREAK
		
		CASE CUTSCENE_ANIM_EVENT_STAGE_7
			sBinkName = "TRIP3_PHONE1_BINK3"
				
			//PRINT_LUKE_ON_SCREEN_DEBUG_TEXT("CUTSCENE_ANIM_EVENT_STAGE_7", "", vBlankDbg)
			//PRINT_LUKE_ON_SCREEN_DEBUG_TEXT(sBinkName, "", vBlankDbg)
			
			IF bmi_Cutscene = null
				bmi_Cutscene = SET_BINK_MOVIE(sBinkName)
				PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_TRIP3_EXT - CUTSCENE_ANIM_EVENT_STAGE_7 - Calling SET_BINK_MOVIE with sBinkName: ", sBinkName)
			ENDIF
			IF bmi_Cutscene != null
				PLAY_BINK_MOVIE(bmi_Cutscene)
				SET_CUTSCENE_ANIM_EVENT_STAGE(CUTSCENE_ANIM_EVENT_STAGE_8)
			ENDIF
		BREAK
		CASE CUTSCENE_ANIM_EVENT_STAGE_8
			sBinkName = "TRIP3_PHONE1_BINK3"
			
			//PRINT_LUKE_ON_SCREEN_DEBUG_TEXT("CUTSCENE_ANIM_EVENT_STAGE_8", "", vBlankDbg)
			//PRINT_LUKE_ON_SCREEN_DEBUG_TEXT(sBinkName, "", vBlankDbg)
		BREAK
	ENDSWITCH
	
	IF NOT IS_STRING_NULL_OR_EMPTY(sBinkName)
		IF bmi_Cutscene != null
			IF IS_NAMED_RENDERTARGET_LINKED(INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_Scrn_TR_01a")))
				//PRINT_LUKE_ON_SCREEN_DEBUG_TEXT("DRAWING BINK", "", vBlankDbg)
				PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_TRIP3_EXT - sBinkName: ", sBinkName, " Drawing")
				SET_TEXT_RENDER_ID(iCutsceneRenderTargetID)		
				SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)		
				SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)										
				DRAW_BINK_MOVIE(bmi_Cutscene, fCentreX, fCentreY, fWidth, fHeight, 0.0, 255, 255, 255, 255)		
				SET_TEXT_RENDER_ID(GET_DEFAULT_SCRIPT_RENDERTARGET_RENDER_ID())
			ELSE
				PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_TRIP3_EXT - sBinkName: ", sBinkName, " Waiting for: IS_NAMED_RENDERTARGET_LINKED")
			ENDIF
		ELSE
			PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_TRIP3_EXT - sBinkName: ", sBinkName, " Waiting for: bmi_Cutscene != null")
		ENDIF
	ELSE
		PRINTLN("[Cutscenes] - LISTEN_FOR_CUTSCENE_EVENTS - FIX_TRIP3_EXT - sBinkName: ", sBinkName, " EMPTY")
	ENDIF
	
	
ENDPROC

// This function should return true by default but does not in earlier 2020 controller content.
FUNC BOOL SHOULD_OBJECT_ASSIGN_CUTSCENE_PRIMARY_PLAYER(INT iObject)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObject].iObjectMinigame = ciFMMC_OBJECT_MINIGAME_TYPE__VAULT_DRILLING
	OR IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_FIXER) 
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_MISSION_USE_PLAYER_OWNED_SUBMARINE()
	
	#IF FEATURE_HEIST_ISLAND
	IF HEIST_ISLAND_FLOW_IS_CURRENT_MISSION_HEIST_ISLAND_FINALE()
		//Save doing the checks below
		RETURN TRUE
	ENDIF
	#ENDIF

	INT i
	FOR i = 0 TO MAX_MOCAP_CUTSCENES - 1
		IF ARE_STRINGS_EQUAL(g_FMMC_STRUCT.sMocapCutsceneData[i].tlName, "HS4F_APP_SUB")
			RETURN TRUE
		ENDIF
	ENDFOR
	
	FOR i = 0 TO GET_FMMC_MAX_NUM_PROPS() - 1
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].iPropBitSet2, ciFMMC_PROP2_HavePropMatchLobbyHostVehicle)
		AND g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn = INT_TO_ENUM(MODEL_NAMES, HASH("h4_prop_h4_sub_kos"))
			RETURN TRUE
		ENDIF
	ENDFOR
	
	RETURN FALSE
	
ENDFUNC

PROC CACHE_PLAYER_SUBMARINE_COLOURS()
	MC_playerBD_1[iLocalPart].iLobbyHostOwnedVehicle_Colour = GET_LOCAL_PLAYER_SUBMARINE_COLOUR()
	MC_playerBD_1[iLocalPart].iLobbyHostOwnedVehicle_Extra = GET_LOCAL_PLAYER_SUBMARINE_FLAG()
	PRINTLN("[Kosatka][LobbyHostVehicle] CACHE_PLAYER_SUBMARINE_COLOURS | Setting iLobbyHostOwnedVehicle_Colour to ", MC_playerBD_1[iLocalPart].iLobbyHostOwnedVehicle_Colour)
	PRINTLN("[Kosatka][LobbyHostVehicle] CACHE_PLAYER_SUBMARINE_COLOURS | Setting iLobbyHostOwnedVehicle_Extra to ", MC_playerBD_1[iLocalPart].iLobbyHostOwnedVehicle_Extra)
ENDPROC
		
FUNC BOOL SHOULD_CUTSCENE_HOLD_RENDERPHASE_LONGER(STRING sMocapName)
	IF IS_STRING_NULL_OR_EMPTY(sMocapName)
		RETURN FALSE
	ENDIF
	
	IF ARE_STRINGS_EQUAL(sMocapName, "FIX_TRIP1_INT")
	OR ARE_STRINGS_EQUAL(sMocapName, "FIX_TRIP2_INT")
	OR ARE_STRINGS_EQUAL(sMocapName, "FIX_TRIP3_INT")
		IF IS_CUTSCENE_PLAYING()
		AND GET_CUTSCENE_TIME() < 500
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC
			
PROC CLEANUP_SPECIFIC_CUTSCENE_REFERENCES()
	INT i
	FOR i = 0 TO ciSpecificCutsceneEntityIndexes_MAX - 1
		IF DOES_ENTITY_EXIST(eiSpecificCutsceneEntities[i])
			IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(eiSpecificCutsceneEntities[i], FALSE) // Some references were assigned by grabbing the cutscene entity index (not a script registered one).
				DELETE_ENTITY(eiSpecificCutsceneEntities[i])
			ENDIF
		ENDIF
		eiSpecificCutsceneEntities[i] = NULL
	ENDFOR
	
	iSpecificCutsceneBitset = 0
	iSpecificCutsceneBitset2 = 0
ENDPROC

PROC CLEANUP_SPECIFIC_CUTSCENE_FIX_BIL_MCS1()

	IF IS_BIT_SET(iSpecificCutsceneBitset, ciSpecificCutsceneBitset_HIDE_bh1_36_ballus08_l1_and_bh1_36_ballus09_l1)	
		PRINTLN("[Cutscenes_Mocap] PROCESS_SPECIFIC_CUTSCENE_FIX_BIL_MCS1 - Removing model hide fopr bh1_36_ballus09_l1 and bh1_36_ballus08_l1")		
		REMOVE_MODEL_HIDE(<<-1525, 75, 60>>, 10.0, INT_TO_ENUM(MODEL_NAMES, HASH("bh1_36_ballus09_l1")))
		REMOVE_MODEL_HIDE(<<-1525, 75, 60>>, 10.0, INT_TO_ENUM(MODEL_NAMES, HASH("bh1_36_ballus08_l1")))
		CLEAR_BIT(iSpecificCutsceneBitset, ciSpecificCutsceneBitset_HIDE_bh1_36_ballus08_l1_and_bh1_36_ballus09_l1)	
	ENDIF
	
ENDPROC

PROC CLEANUP_SPECIFIC_CUTSCENE_FIX_PRO_MCS1()
	
	IF IS_BIT_SET(iSpecificCutsceneBitset, ciSpecificCutsceneBitset_Hide_Spa_door)
		REMOVE_MODEL_HIDE(<<951.86, 3.55, 117.43>>, 1.0, INT_TO_ENUM(MODEL_NAMES, (HASH("vw_vwint02_bar_spa_doors_closed"))))
		PRINTLN("[Cutscenes_Mocap] PROCESS_SPECIFIC_CUTSCENE_FIX_BIL_MCS1 - Removing model hide for vw_vwint02_bar_spa_doors_closed")
	ENDIF
	
ENDPROC

PROC SHOW_OR_HIDE_ENTITIES_FOR_FIX_STU_EXT(BOOL bShow, INT iHideStage)
	SWITCH iHideStage
		CASE 0
			SET_ENTITY_VISIBLE(GET_CLOSEST_OBJECT_OF_TYPE(<<-998.3971, -66.6414, -99.1225>>, cfPreciseModelHideSize, INT_TO_ENUM(MODEL_NAMES, (HASH("sf_prop_sf_keyboard_01a"))), FALSE, FALSE, FALSE), bShow)
			SET_ENTITY_VISIBLE(GET_CLOSEST_OBJECT_OF_TYPE(<<-998.5144, -66.4518, -99.1225>>, cfPreciseModelHideSize, INT_TO_ENUM(MODEL_NAMES, (HASH("sf_prop_sf_monitor_s_02a"))), FALSE, FALSE, FALSE), bShow)
			SET_ENTITY_VISIBLE(GET_CLOSEST_OBJECT_OF_TYPE(<<-998.5144, -66.4518, -99.1225>>, cfPreciseModelHideSize, INT_TO_ENUM(MODEL_NAMES, (HASH("sf_prop_sf_s_scrn_01a"))), FALSE, FALSE, FALSE), bShow)
			SET_ENTITY_VISIBLE(GET_CLOSEST_OBJECT_OF_TYPE(<<-998.4810, -66.356, -100.0>>, cfPreciseModelHideSize, INT_TO_ENUM(MODEL_NAMES, (HASH("sf_prop_sf_rack_audio_01a"))), FALSE, FALSE, FALSE), bShow)
		BREAK
		
		CASE 1
			SET_ENTITY_VISIBLE(GET_CLOSEST_OBJECT_OF_TYPE(<<-998.1146, -66.1955, -99.1225>>, cfPreciseModelHideSize, INT_TO_ENUM(MODEL_NAMES, (HASH("sf_prop_sf_track_mouse_01a"))), FALSE, FALSE, FALSE), bShow)
		BREAK
	ENDSWITCH
ENDPROC

PROC CLEANUP_SPECIFIC_CUTSCENE_FIX_STU_EXT()
	
	IF IS_BIT_SET(iSpecificCutsceneBitset, ciSpecificCutsceneBitset_FIX_STU_EXT_HiddenDeskAndBlood)
		PRINTLN("[Cutscenes_Mocap] CLEANUP_SPECIFIC_CUTSCENE_FIX_STU_EXT - Removing precise model hides")
		
		// The table next to the mixing desk
		REMOVE_MODEL_HIDE(<<-996.5, -67.5, -100.0>>, 5.0, INT_TO_ENUM(MODEL_NAMES, (HASH("sf_prop_sf_table_studio_01a"))), FALSE)
		REMOVE_MODEL_HIDE(<<-996.5, -67.5, -100.0>>, 5.0, INT_TO_ENUM(MODEL_NAMES, (HASH("prop_dyn_pc_02"))), FALSE)
		
		// The desk with PC further from the mixing desk
		SHOW_OR_HIDE_ENTITIES_FOR_FIX_STU_EXT(TRUE, 0)
		SHOW_OR_HIDE_ENTITIES_FOR_FIX_STU_EXT(TRUE, 1)
	ENDIF
	
ENDPROC

PROC CLEANUP_SPECIFIC_CUTSCENE_FIX_TRIP_INT(STRING sMocapName)	
	IF NOT IS_STRING_NULL_OR_EMPTY(sMocapName)
		IF ARE_STRINGS_EQUAL(sMocapName, "FIX_TRIP2_INT")
		OR ARE_STRINGS_EQUAL(sMocapName, "FIX_TRIP3_INT")
			CLEAR_TIMECYCLE_MODIFIER()
		ENDIF
	ENDIF
ENDPROC

PROC CLEANUP_SPECIFIC_CUTSCENE_FUNCTIONALITY(STRING sMocapName)
	
	UNUSED_PARAMETER(sMocapName)
	
	// Use mocap string for cutscenes where we necessary, where it's not necessary leave it out of these checks for MC_SCRIPT_CLEANUP calls.
	/*IF ARE_STRINGS_EQUAL(sMocapName, "FIX_BIL_MCS1")
					
	ENDIF*/
	
	CLEANUP_SPECIFIC_CUTSCENE_FIX_BIL_MCS1()
	
	CLEANUP_SPECIFIC_CUTSCENE_FIX_PRO_MCS1()
	
	CLEANUP_SPECIFIC_CUTSCENE_FIX_STU_EXT()
	
	IF NOT IS_STRING_NULL_OR_EMPTY(sMocapName)
	AND ARE_STRINGS_EQUAL(sMocapName, "FIX_TRIP1_EXT")
	AND IS_BIT_SET(iSpecificCutsceneBitset, ciSpecificCutsceneBitset_FIX_TRIP1_EXT_OverridePTFX)
		RESET_PARTICLE_FX_OVERRIDE("exp_grd_vehicle")
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_UseBigExplosions)
			REMOVE_NAMED_PTFX_ASSET("scr_vw_oil")
		ENDIF
		CLEAR_BIT(iSpecificCutsceneBitset, ciSpecificCutsceneBitset_FIX_TRIP1_EXT_OverridePTFX)
	ENDIF
	
	CLEANUP_SPECIFIC_CUTSCENE_FIX_TRIP_INT(sMocapName)
	
ENDPROC

PROC PROCESS_SPECIFIC_CUTSCENE_HS4F_APP_SUB()
	
	IF NOT DOES_ENTITY_EXIST(eiSpecificCutsceneEntities[ciSpecificCutsceneEntityIndexes_HS4F_APP_SUB_Delta_Sub])
		eiSpecificCutsceneEntities[ciSpecificCutsceneEntityIndexes_HS4F_APP_SUB_Delta_Sub] = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("Delta_Sub")
	ENDIF
	
	IF NOT IS_BIT_SET(iSpecificCutsceneBitset, ciSpecificCutsceneBitset_HS4F_APP_SUB_Delta_Sub_Modded)
		IF GET_FMMC_LOBBY_LEADER_PARTICIPANT_INDEX() > -1
			IF DOES_ENTITY_EXIST(eiSpecificCutsceneEntities[ciSpecificCutsceneEntityIndexes_HS4F_APP_SUB_Delta_Sub])
			AND IS_ENTITY_ALIVE(eiSpecificCutsceneEntities[ciSpecificCutsceneEntityIndexes_HS4F_APP_SUB_Delta_Sub])
				VEHICLE_INDEX viDeltaSub = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(eiSpecificCutsceneEntities[ciSpecificCutsceneEntityIndexes_HS4F_APP_SUB_Delta_Sub])
				PRINTLN("[Cutscenes_Mocap] PROCESS_SPECIFIC_CUTSCENE_HS4F_APP_SUB - Setting Sub Colour (", MC_playerBD_1[GET_FMMC_LOBBY_LEADER_PARTICIPANT_INDEX()].iLobbyHostOwnedVehicle_Colour, ") and Flag(", MC_playerBD_1[GET_FMMC_LOBBY_LEADER_PARTICIPANT_INDEX()].iLobbyHostOwnedVehicle_Extra, ").")
				IF GET_NUM_MOD_KITS(viDeltaSub) > 0
					SET_VEHICLE_MOD_KIT(viDeltaSub, 0)
				ENDIF
				SET_SUBMARINE_PROPERTY_FLAG(viDeltaSub, MC_playerBD_1[GET_FMMC_LOBBY_LEADER_PARTICIPANT_INDEX()].iLobbyHostOwnedVehicle_Extra)
				SET_SUBMARINE_PROPERTY_COLOURS(viDeltaSub, MC_playerBD_1[GET_FMMC_LOBBY_LEADER_PARTICIPANT_INDEX()].iLobbyHostOwnedVehicle_Colour)
				SET_BIT(iSpecificCutsceneBitset, ciSpecificCutsceneBitset_HS4F_APP_SUB_Delta_Sub_Modded)
			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC PROCESS_SPECIFIC_CUTSCENE_TUNF_UNI_SWAT()

	IF NOT DOES_ENTITY_EXIST(eiSpecificCutsceneEntities[ciSpecificCutsceneEntityIndexes_TUNF_UNI_SWAT_Riot_1])
		eiSpecificCutsceneEntities[ciSpecificCutsceneEntityIndexes_TUNF_UNI_SWAT_Riot_1] = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("Riot_1")
		PRINTLN("[Cutscenes_Mocap] PROCESS_SPECIFIC_CUTSCENE_TUNF_UNI_SWAT - Assigning Riot_1 to eiSpecificCutsceneEntities")
	ENDIF
	IF NOT DOES_ENTITY_EXIST(eiSpecificCutsceneEntities[ciSpecificCutsceneEntityIndexes_TUNF_UNI_SWAT_Riot_2])
		eiSpecificCutsceneEntities[ciSpecificCutsceneEntityIndexes_TUNF_UNI_SWAT_Riot_2] = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("Riot_2")
		PRINTLN("[Cutscenes_Mocap] PROCESS_SPECIFIC_CUTSCENE_TUNF_UNI_SWAT - Assigning Riot_2 to eiSpecificCutsceneEntities")
	ENDIF
	IF NOT DOES_ENTITY_EXIST(eiSpecificCutsceneEntities[ciSpecificCutsceneEntityIndexes_TUNF_UNI_SWAT_Riot_3])
		eiSpecificCutsceneEntities[ciSpecificCutsceneEntityIndexes_TUNF_UNI_SWAT_Riot_3] = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("Riot_3")
		PRINTLN("[Cutscenes_Mocap] PROCESS_SPECIFIC_CUTSCENE_TUNF_UNI_SWAT - Assigning Riot_3 to eiSpecificCutsceneEntities")
	ENDIF
	
	INT i = 0
	FOR i = 0 TO ciSpecificCutsceneEntityIndexes_MAX-1
		IF NOT DOES_ENTITY_EXIST(eiSpecificCutsceneEntities[i])
		OR NOT IS_ENTITY_ALIVE(eiSpecificCutsceneEntities[i])
			RELOOP
		ENDIF
		
		IF NOT IS_BIT_SET(iSpecificCutsceneBitset, ciSpecificCutsceneBitset_TUNF_UNI_SWAT_SirensSet_1 + i)
			VEHICLE_INDEX viRiotVan = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(eiSpecificCutsceneEntities[i])
			
			SET_SIREN_WITH_NO_DRIVER(viRiotVan, TRUE)
			TRIGGER_SIREN_AUDIO(viRiotVan)
			
			SET_VEHICLE_SIREN(viRiotVan, TRUE)
			
			SET_VEHICLE_LIGHTS(viRiotVan, FORCE_VEHICLE_LIGHTS_ON)
			
			// SET_VEHICLE_ACT_AS_IF_HAS_SIREN_ON
			PRINTLN("[Cutscenes_Mocap] PROCESS_SPECIFIC_CUTSCENE_TUNF_UNI_SWAT - Riot_", i, " Alive/Exists - ")		
			SET_BIT(iSpecificCutsceneBitset, ciSpecificCutsceneBitset_TUNF_UNI_SWAT_SirensSet_1 + i)
		ENDIF
	ENDFOR
	
ENDPROC

PROC PROCESS_SPECIFIC_CUTSCENE_FIX_BIL_MCS1()
	
	IF NOT IS_BIT_SET(iSpecificCutsceneBitset, ciSpecificCutsceneBitset_HIDE_bh1_36_ballus08_l1_and_bh1_36_ballus09_l1)	
		PRINTLN("[Cutscenes_Mocap] PROCESS_SPECIFIC_CUTSCENE_FIX_BIL_MCS1 - Hiding models bh1_36_ballus09_l1 and bh1_36_ballus08_l1")		
		CREATE_MODEL_HIDE(<<-1525, 75, 60>>, 10.0, INT_TO_ENUM(MODEL_NAMES, HASH("bh1_36_ballus09_l1")), TRUE)	
		CREATE_MODEL_HIDE(<<-1525, 75, 60>>, 10.0, INT_TO_ENUM(MODEL_NAMES, HASH("bh1_36_ballus08_l1")), TRUE)
		SET_BIT(iSpecificCutsceneBitset, ciSpecificCutsceneBitset_HIDE_bh1_36_ballus08_l1_and_bh1_36_ballus09_l1)	
	ENDIF
	
ENDPROC

PROC PROCESS_SPECIFIC_CUTSCENE_FIX_PRO_MCS1()
	
	IF NOT IS_BIT_SET(iSpecificCutsceneBitset, ciSpecificCutsceneBitset_Hide_Spa_door)
	AND GET_CUTSCENE_TIME() > 0
		PRINTLN("[Cutscenes_Mocap] PROCESS_SPECIFIC_CUTSCENE_FIX_PRO_MCS1 - Hiding models ciSpecificCutsceneBitset_Hide_Spa_door and bh1_36_ballus08_l1")		
		CREATE_MODEL_HIDE(<<951.86, 3.55, 117.43>>, 1.0, INT_TO_ENUM(MODEL_NAMES, (HASH("vw_vwint02_bar_spa_doors_closed"))), FALSE)
		SET_BIT(iSpecificCutsceneBitset, ciSpecificCutsceneBitset_Hide_Spa_door)
	ENDIF
	
	IF GET_CUTSCENE_TIME() > 400
	AND NOT IS_BIT_SET(iSpecificCutsceneBitset, ciSpecificCutsceneBitset_Stop_Party_Music)
		SET_BIT(iSpecificCutsceneBitset, ciSpecificCutsceneBitset_Stop_Party_Music)
		SET_EMITTER_RADIO_STATION("se_vw_dlc_casino_apart_Apart_Party_Music_01", "OFF")
		SET_EMITTER_RADIO_STATION("se_vw_dlc_casino_apart_Apart_Party_Music_02", "OFF")
		SET_EMITTER_RADIO_STATION("se_vw_dlc_casino_apart_Apart_Party_Music_03", "OFF")
	ENDIF
ENDPROC

PROC PROCESS_SPECIFIC_CUTSCENE_FIX_FIX_STU_EXT()

	IF NOT IS_BIT_SET(iSpecificCutsceneBitset, ciSpecificCutsceneBitset_FIX_STU_EXT_HiddenMouse)
		IF GET_CUTSCENE_TIME() >= 95000 // During the "time skip" section
			PRINTLN("[Cutscenes_Mocap] PROCESS_SPECIFIC_CUTSCENE_FIX_FIX_STU_EXT - Hiding mouse separately!")
			SHOW_OR_HIDE_ENTITIES_FOR_FIX_STU_EXT(FALSE, 1)
			SET_BIT(iSpecificCutsceneBitset, ciSpecificCutsceneBitset_FIX_STU_EXT_HiddenMouse)
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(iSpecificCutsceneBitset, ciSpecificCutsceneBitset_FIX_STU_EXT_HiddenDeskAndBlood)
		IF GET_CUTSCENE_TIME() >= 70000 // During the "time skip" section
			PRINTLN("[Cutscenes_Mocap] PROCESS_SPECIFIC_CUTSCENE_FIX_FIX_STU_EXT - Doing precise model hides!")
			
			// The table next to the mixing desk
			CREATE_MODEL_HIDE(<<-996.5, -67.5, -100.0>>, 1.0, INT_TO_ENUM(MODEL_NAMES, (HASH("sf_prop_sf_table_studio_01a"))), FALSE)
			CREATE_MODEL_HIDE(<<-996.5, -67.5, -100.0>>, 1.0, INT_TO_ENUM(MODEL_NAMES, (HASH("prop_dyn_pc_02"))), FALSE)
			
			// The desk with PC further from the mixing desk
			//CREATE_MODEL_HIDE(<<-998.3971, -66.6414, -99.1225>> + vStudioDeskModelHideOffset, cfPreciseModelHideSize, INT_TO_ENUM(MODEL_NAMES, (HASH("sf_prop_sf_keyboard_01a"))), FALSE)
			//CREATE_MODEL_HIDE(<<-998.5144, -66.4518, -99.1225>> + vStudioDeskModelHideOffset, cfPreciseModelHideSize, INT_TO_ENUM(MODEL_NAMES, (HASH("sf_prop_sf_monitor_s_02a"))), FALSE)
			//CREATE_MODEL_HIDE(<<-998.4810, -66.356, -100.0>> + vStudioDeskModelHideOffset, cfPreciseModelHideSize, INT_TO_ENUM(MODEL_NAMES, (HASH("sf_prop_sf_rack_audio_01a"))), FALSE)
			//CREATE_MODEL_HIDE(<<-998.1146, -66.1955, -99.1225>> + vStudioDeskModelHideOffset, cfPreciseModelHideSize, INT_TO_ENUM(MODEL_NAMES, (HASH("sf_prop_sf_track_mouse_01a"))), FALSE)
			//CREATE_MODEL_HIDE(<<-998.5144, -66.4518, -99.1225>> + vStudioDeskModelHideOffset, cfPreciseModelHideSize, INT_TO_ENUM(MODEL_NAMES, (HASH("sf_prop_sf_s_scrn_01a"))), FALSE)
			
			SHOW_OR_HIDE_ENTITIES_FOR_FIX_STU_EXT(FALSE, 0)
			
			
			//CREATE_MODEL_HIDE(<<-998.4810, -66.356, -100.0>> + vStudioDeskModelHideOffset, cfPreciseModelHideSize, INT_TO_ENUM(MODEL_NAMES, (HASH("sf_prop_sf_rack_audio_01a"))), FALSE)
			
			//CREATE_MODEL_HIDE(<<-998.5, -66.6, -99.12>>, 0.15, INT_TO_ENUM(MODEL_NAMES, (HASH("sf_int3_screen_music_004"))), FALSE)
			
			// Remove any blood etc
			REMOVE_DECALS_IN_RANGE(<<-996.5, -67.5, -100.0>>, 10.0)
			
			SET_BIT(iSpecificCutsceneBitset, ciSpecificCutsceneBitset_FIX_STU_EXT_HiddenDeskAndBlood)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SPECIFIC_CUTSCENE_FIXF_FIN_MCS3()
	
	IF IS_BIT_SET(iSpecificCutsceneBitset, ciSpecificCutsceneBitset_StopDreDriveMusicChecked)
		EXIT
	ENDIF	
	
	IF GET_CUTSCENE_TIME() > 0
		
		INT iMusicPlayTime = GET_MUSIC_PLAYTIME()
		PRINTLN("[Cutscenes_Mocap] PROCESS_SPECIFIC_CUTSCENE_FIXF_FIN_MCS3 - iMusicPlayTime: ", iMusicPlayTime)
		IF IS_INT_IN_RANGE(iMusicPlayTime, 128000, 199000)
			PRINTLN("[Cutscenes_Mocap] PROCESS_SPECIFIC_CUTSCENE_FIXF_FIN_MCS3 - Stop that music!")
			TRIGGER_MUSIC_EVENT("DFWD_CAR_TRACK_STOP")
		ENDIF
		
		SET_BIT(iSpecificCutsceneBitset, ciSpecificCutsceneBitset_StopDreDriveMusicChecked)
	ENDIF
	
ENDPROC

PROC PROCESS_SPECIFIC_CUTSCENE_FUNCTIONALITY(STRING sMocapName)
	
	
	IF ARE_STRINGS_EQUAL(sMocapName, "HS4F_APP_SUB")
	
		PROCESS_SPECIFIC_CUTSCENE_HS4F_APP_SUB()
	
	ELIF ARE_STRINGS_EQUAL(sMocapName, "TUNF_UNI_SWAT")
	
		PROCESS_SPECIFIC_CUTSCENE_TUNF_UNI_SWAT()
	
	//Add others as an ELIF as required
	ELIF ARE_STRINGS_EQUAL(sMocapName, "FIX_BIL_MCS1")
		
		PROCESS_SPECIFIC_CUTSCENE_FIX_BIL_MCS1()
	
	ELIF ARE_STRINGS_EQUAL(sMocapName, "FIX_PRO_MCS1")
		
		PROCESS_SPECIFIC_CUTSCENE_FIX_PRO_MCS1()
	
	ELIF ARE_STRINGS_EQUAL(sMocapName, "FIX_STU_EXT")
		
		PROCESS_SPECIFIC_CUTSCENE_FIX_FIX_STU_EXT()
		
	ELIF ARE_STRINGS_EQUAL(sMocapName, "FIX_TRIP1_EXT")			
		
		IF HANDLE_LOADING_BIG_EXPLOSION_PTFX()		
			IF NOT IS_BIT_SET(iSpecificCutsceneBitset, ciSpecificCutsceneBitset_FIX_TRIP1_EXT_OverridePTFX)
				SET_PARTICLE_FX_OVERRIDE("exp_grd_vehicle", "scr_vw_oil_tanker_explosion")
				SET_BIT(iSpecificCutsceneBitset, ciSpecificCutsceneBitset_FIX_TRIP1_EXT_OverridePTFX)
			ENDIF
		ENDIF
		
	ELIF ARE_STRINGS_EQUAL(sMocapName, "FIXF_FIN_MCS3")
	
		PROCESS_SPECIFIC_CUTSCENE_FIXF_FIN_MCS3()
		
	ENDIF	
	
	IF IS_BIT_SET(iLocalBoolCheck28, LBOOL28_DISABLE_CASINO_PEDS_DURING_CUTSCENE)
	AND GET_CUTSCENE_TIME() > 0 
		g_bTurnOnMissionPenthousePartyPeds = FALSE
		PRINTLN("[Cutscene_Mocap] - g_bTurnOnMissionPenthousePartyPeds = FALSE")
		CLEAR_BIT(iLocalBoolCheck28, LBOOL28_DISABLE_CASINO_PEDS_DURING_CUTSCENE)
	ENDIF	
	
ENDPROC

FUNC BOOL SHOULD_CUTSCENE_IGNORE_WARP_RANGE()
	SWITCH MC_ServerBD.iEndCutscene
		CASE ciMISSION_CUTSCENE_FIXER_FIX_GOLF_EXT RETURN TRUE		
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

// ##### ------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Cutscene Lift --------------------------------------------------------------------------------------------
// ##### Description: Assets and data for lift cutscenes.  ----------------------------------------------------------------------
// ##### ------------------------------------------------------------------------------------------------------------------------
// ##### ------------------------------------------------------------------------------------------------------------------------

PROC GET_SCRIPTED_CUTSCENE_LIFT_MODELS(eCutsceneLift eLiftType, MODEL_NAMES &mnBase, MODEL_NAMES &mnDoorL, MODEL_NAMES &mnDoorR, MODEL_NAMES &mnDoorLO, MODEL_NAMES &mnDoorRO, MODEL_NAMES &mnExtras1)
	
	SWITCH eLiftType
		CASE eCSLift_ServerFarm
		CASE eCSLift_IAABase
			mnBase = INT_TO_ENUM(MODEL_NAMES, HASH("XM_Prop_IAA_BASE_Elevator"))
			mnDoorL = INT_TO_ENUM(MODEL_NAMES, HASH("XM_Prop_AGT_CIA_Door_El_R"))
			mnDoorR = INT_TO_ENUM(MODEL_NAMES, HASH("XM_Prop_AGT_CIA_Door_El_L"))
			mnDoorLO = INT_TO_ENUM(MODEL_NAMES, HASH("XM_Prop_AGT_CIA_Door_El_02_R"))
			mnDoorRO = INT_TO_ENUM(MODEL_NAMES, HASH("XM_Prop_AGT_CIA_Door_El_02_L"))
		BREAK
		CASE eCSLift_ConstructionSkyscraper_SouthA_Bottom		
		CASE eCSLift_ConstructionSkyscraper_SouthA_Mid			
		CASE eCSLift_ConstructionSkyscraper_SouthB_Bottom		
		CASE eCSLift_ConstructionSkyscraper_SouthB_Mid 			
		CASE eCSLift_ConstructionSkyscraper_NorthA_Bottom		
		CASE eCSLift_ConstructionSkyscraper_NorthA_Mid			
		CASE eCSLift_ConstructionSkyscraper_NorthA_Top			
		CASE eCSLift_ConstructionSkyscraper_NorthB_Bottom		
		CASE eCSLift_ConstructionSkyscraper_NorthB_Mid			
		CASE eCSLift_ConstructionSkyscraper_NorthB_Top			
			mnBase = prop_conslift_lift
			mnExtras1 = REH_PROP_REH_KEYPAD_01A
		BREAK
	ENDSWITCH
	
ENDPROC

FUNC BOOL LOAD_SCRIPTED_CUTSCENE_LIFT_ASSETS(INT iCutsceneToUse)
	
	eCSLift = g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].eCSLiftData
	
	BOOL bLoaded = TRUE
	BOOL bLoadLiftModels
	BOOL bLoadSound
	
	IF g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].eCSLiftData != eCSLift_None
		
		bLoadLiftModels = TRUE
		bLoadSound = TRUE
		eCSLift = g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneToUse].eCSLiftData
	
	#IF IS_DEBUG_BUILD
	ELSE
		PRINTLN("[RCC MISSION][CSLift] LOAD_SCRIPTED_CUTSCENE_LIFT_ASSETS - No cutscene lift setup")
	#ENDIF
	ENDIF
	
	IF bLoadSound
		SWITCH eCSLift		
			CASE eCSLift_ConstructionSkyscraper_SouthA_Bottom	
			CASE eCSLift_ConstructionSkyscraper_SouthA_Mid		
			CASE eCSLift_ConstructionSkyscraper_SouthB_Bottom	
			CASE eCSLift_ConstructionSkyscraper_SouthB_Mid 		
			CASE eCSLift_ConstructionSkyscraper_NorthA_Bottom	
			CASE eCSLift_ConstructionSkyscraper_NorthA_Mid		
			CASE eCSLift_ConstructionSkyscraper_NorthA_Top		
			CASE eCSLift_ConstructionSkyscraper_NorthB_Bottom	
			CASE eCSLift_ConstructionSkyscraper_NorthB_Mid		
			CASE eCSLift_ConstructionSkyscraper_NorthB_Top	
				IF NOT REQUEST_SCRIPT_AUDIO_BANK("Freight_Elevator")
					PRINTLN("[RCC MISSION][CSLift] LOAD_SCRIPTED_CUTSCENE_LIFT_ASSETS - Loading Sound Bank: Freight_Elevator")
					bLoaded = FALSE
				ENDIF
			BREAK
		ENDSWITCH	
	ENDIF
	
	IF bLoadLiftModels
		
		MODEL_NAMES mnBase, mnDoorL, mnDoorR, mnDoorLO, mnDoorRO, mnExtras1
		GET_SCRIPTED_CUTSCENE_LIFT_MODELS(eCSLift, mnBase, mnDoorL, mnDoorR, mnDoorLO, mnDoorRO, mnExtras1)
		
		PRINTLN("[RCC MISSION][CSLift] LOAD_SCRIPTED_CUTSCENE_LIFT_ASSETS - Loading assets for lift ",GET_SCRIPTED_CUTSCENE_LIFT_DEBUG_NAME(eCSLift))		
		
		IF IS_MODEL_VALID(mnBase)
			REQUEST_MODEL(mnBase)
			
			IF NOT HAS_MODEL_LOADED(mnBase)
				PRINTLN("[RCC MISSION][CSLift] LOAD_SCRIPTED_CUTSCENE_LIFT_ASSETS - Waiting for Lift Base model")
				bLoaded = FALSE
			ELSE
				PRINTLN("[RCC MISSION][CSLift] LOAD_SCRIPTED_CUTSCENE_LIFT_ASSETS - Lift Base model has loaded")
			ENDIF
		ELSE
			PRINTLN("[RCC MISSION][CSLift] LOAD_SCRIPTED_CUTSCENE_LIFT_ASSETS - Lift Base model is not valid, skipping.")
		ENDIF
			
		IF IS_MODEL_VALID(mnDoorL)
			REQUEST_MODEL(mnDoorL)
			
			IF NOT HAS_MODEL_LOADED(mnDoorL)
				PRINTLN("[RCC MISSION][CSLift] LOAD_SCRIPTED_CUTSCENE_LIFT_ASSETS - Waiting for Lift Left Door model")
				bLoaded = FALSE
			ELSE
				PRINTLN("[RCC MISSION][CSLift] LOAD_SCRIPTED_CUTSCENE_LIFT_ASSETS - Lift Left Door model has loaded")
			ENDIF
		ELSE
			PRINTLN("[RCC MISSION][CSLift] LOAD_SCRIPTED_CUTSCENE_LIFT_ASSETS - Lift Left Door model is not valid, skipping.")
		ENDIF		
		
		IF IS_MODEL_VALID(mnDoorR)
			REQUEST_MODEL(mnDoorR)
			
			IF NOT HAS_MODEL_LOADED(mnDoorR)
				PRINTLN("[RCC MISSION][CSLift] LOAD_SCRIPTED_CUTSCENE_LIFT_ASSETS - Waiting for Lift Right Door model")
				bLoaded = FALSE
			ELSE
				PRINTLN("[RCC MISSION][CSLift] LOAD_SCRIPTED_CUTSCENE_LIFT_ASSETS - Lift Right Door model has loaded")
			ENDIF
		ELSE
			PRINTLN("[RCC MISSION][CSLift] LOAD_SCRIPTED_CUTSCENE_LIFT_ASSETS - Lift Right Door model is not valid, skipping.")
		ENDIF
		
		
		IF IS_MODEL_VALID(mnDoorLO)
			REQUEST_MODEL(mnDoorLO)
			
			IF NOT HAS_MODEL_LOADED(mnDoorLO)
				PRINTLN("[RCC MISSION][CSLift] LOAD_SCRIPTED_CUTSCENE_LIFT_ASSETS - Waiting for Lift Outer Left Door model")
				bLoaded = FALSE
			ELSE
				PRINTLN("[RCC MISSION][CSLift] LOAD_SCRIPTED_CUTSCENE_LIFT_ASSETS - Lift Outer Left Door model has loaded")
			ENDIF
		ELSE
			PRINTLN("[RCC MISSION][CSLift] LOAD_SCRIPTED_CUTSCENE_LIFT_ASSETS - Lift Outer Left Door model is not valid, skipping.")
		ENDIF
		
		IF IS_MODEL_VALID(mnDoorRO)
			REQUEST_MODEL(mnDoorRO)
			
			IF NOT HAS_MODEL_LOADED(mnDoorRO)
				PRINTLN("[RCC MISSION][CSLift] LOAD_SCRIPTED_CUTSCENE_LIFT_ASSETS - Waiting for Lift Outer Right Door model")
				bLoaded = FALSE
			ELSE
				PRINTLN("[RCC MISSION][CSLift] LOAD_SCRIPTED_CUTSCENE_LIFT_ASSETS - Lift Outer Right Door model has loaded")
			ENDIF
		ELSE
			PRINTLN("[RCC MISSION][CSLift] LOAD_SCRIPTED_CUTSCENE_LIFT_ASSETS - Lift Outer Right Door model is not valid, skipping.")
		ENDIF
		
		IF IS_MODEL_VALID(mnExtras1)
			REQUEST_MODEL(mnExtras1)
			
			IF NOT HAS_MODEL_LOADED(mnExtras1)
				PRINTLN("[RCC MISSION][CSLift] LOAD_SCRIPTED_CUTSCENE_LIFT_ASSETS - Waiting for Lift mnExtras1 Door model")
				bLoaded = FALSE
			ELSE
				PRINTLN("[RCC MISSION][CSLift] LOAD_SCRIPTED_CUTSCENE_LIFT_ASSETS - Lift mnExtras1 has loaded")
			ENDIF
		ELSE
			PRINTLN("[RCC MISSION][CSLift] LOAD_SCRIPTED_CUTSCENE_LIFT_ASSETS - Lift mnExtras1 model is not valid, skipping.")
		ENDIF
		
	ENDIF
	
	RETURN bLoaded
	
ENDFUNC

PROC UNLOAD_SCRIPTED_CUTSCENE_LIFT_ASSETS(VECTOR vLift)
	
	IF eCSLift = eCSLift_None
		EXIT
	ENDIF
	
	PRINTLN("[RCC MISSION][CSLift] UNLOAD_SCRIPTED_CUTSCENE_LIFT_ASSETS - Unloading assets for lift ",GET_SCRIPTED_CUTSCENE_LIFT_DEBUG_NAME(eCSLift))
	
	IF DOES_ENTITY_EXIST(objCutsceneLift_Body)
		PRINTLN("[RCC MISSION][CSLift] UNLOAD_SCRIPTED_CUTSCENE_LIFT_ASSETS - Deleting lift body")
		DELETE_OBJECT(objCutsceneLift_Body)
	ENDIF
	IF DOES_ENTITY_EXIST(objCutsceneLift_DoorL)
		PRINTLN("[RCC MISSION][CSLift] UNLOAD_SCRIPTED_CUTSCENE_LIFT_ASSETS - Deleting lift left door")
		DELETE_OBJECT(objCutsceneLift_DoorL)
	ENDIF
	IF DOES_ENTITY_EXIST(objCutsceneLift_DoorR)
		PRINTLN("[RCC MISSION][CSLift] UNLOAD_SCRIPTED_CUTSCENE_LIFT_ASSETS - Deleting lift right door")
		DELETE_OBJECT(objCutsceneLift_DoorR)
	ENDIF
	IF DOES_ENTITY_EXIST(objCutsceneLift_DoorLO)
		PRINTLN("[RCC MISSION][CSLift] UNLOAD_SCRIPTED_CUTSCENE_LIFT_ASSETS - Deleting lift outer left door")
		DELETE_OBJECT(objCutsceneLift_DoorLO)
	ENDIF
	IF DOES_ENTITY_EXIST(objCutsceneLift_DoorRO)
		PRINTLN("[RCC MISSION][CSLift] UNLOAD_SCRIPTED_CUTSCENE_LIFT_ASSETS - Deleting lift outer right door")
		DELETE_OBJECT(objCutsceneLift_DoorRO)
	ENDIF
	IF DOES_ENTITY_EXIST(objCutsceneLift_Body_Extras1)
		PRINTLN("[RCC MISSION][CSLift] UNLOAD_SCRIPTED_CUTSCENE_LIFT_ASSETS - Deleting lift Extras1")
		DELETE_OBJECT(objCutsceneLift_Body_Extras1)
	ENDIF
		
	MODEL_NAMES mnBase, mnDoorL, mnDoorR, mnDoorLO, mnDoorRO, mnExtras1
	GET_SCRIPTED_CUTSCENE_LIFT_MODELS(eCSLift, mnBase, mnDoorL, mnDoorR, mnDoorLO, mnDoorRO, mnExtras1)
	
	IF IS_MODEL_VALID(mnBase)
		SET_MODEL_AS_NO_LONGER_NEEDED(mnBase)
		REMOVE_MODEL_HIDE(vLift, 0.5, mnBase)
	ENDIF
	
	IF IS_MODEL_VALID(mnDoorL)
		SET_MODEL_AS_NO_LONGER_NEEDED(mnDoorL)
		REMOVE_MODEL_HIDE(vLift, 0.5, mnDoorL)
	ENDIF
	
	IF IS_MODEL_VALID(mnDoorR)
		SET_MODEL_AS_NO_LONGER_NEEDED(mnDoorR)
		REMOVE_MODEL_HIDE(vLift, 0.5, mnDoorR)
	ENDIF
	
	IF IS_MODEL_VALID(mnDoorLO)
		SET_MODEL_AS_NO_LONGER_NEEDED(mnDoorLO)
		REMOVE_MODEL_HIDE(vLift, 0.5, mnDoorLO)
	ENDIF
	
	IF IS_MODEL_VALID(mnDoorRO)
		SET_MODEL_AS_NO_LONGER_NEEDED(mnDoorRO)
		REMOVE_MODEL_HIDE(vLift, 0.5, mnDoorRO)
	ENDIF
	
	IF IS_MODEL_VALID(mnExtras1)
		SET_MODEL_AS_NO_LONGER_NEEDED(mnExtras1)	
		REMOVE_MODEL_HIDE(vLift, 0.5, mnExtras1)	
	ENDIF
	
	PRINTLN("[RCC MISSION][CSLift] UNLOAD_SCRIPTED_CUTSCENE_LIFT_ASSETS - Removed cutscene lift model hides from vLift ",vLift)
	
	eCSLift = eCSLift_None
	
ENDPROC


// ##### ------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Load All -------------------------------------------------------------------------------------------------
// ##### Description: Keep this at the bottom of loading section. Handles loading all the assets required on the mission.  ------
// ##### ------------------------------------------------------------------------------------------------------------------------
// ##### ------------------------------------------------------------------------------------------------------------------------


/// PURPOSE:
///    This function loads all common assets (and mission-specific ones) that are required
/// RETURNS:
///    TRUE when everything has loaded

// fmmc2020 clean this up into functions
FUNC BOOL LOADED_ALL_ASSETS()
	BOOL bAllLoaded = TRUE
	
	//Load the ptfx
	IF NOT LOAD_PARTICLE_FX()
		PRINTLN("[RCC MISSION][LOADED_ALL_ASSETS] - TRANSITION TIME - LOAD_PARTICLE_FX  - FRAME COUNT = (", GET_FRAME_COUNT(), ") POSIX TIME = ", GET_CLOUD_TIME_AS_INT(), ")")
		PRINTLN("[RCC MISSION][LOADED_ALL_ASSETS] not loaded PTFX !!!!!!")
		bAllLoaded = FALSE
	ENDIF

	//Load anims
	IF NOT LOAD_ACTION_ANIMS()
		PRINTLN("[RCC MISSION][LOADED_ALL_ASSETS] MISSION - TRANSITION TIME - LOAD_ACTION_ANIMS  - FRAME COUNT = (", GET_FRAME_COUNT(), ") POSIX TIME = ", GET_CLOUD_TIME_AS_INT(), ")")
		PRINTLN("[RCC MISSION][LOADED_ALL_ASSETS] not loaded action anims !!!!!!")
		bAllLoaded = FALSE
	ENDIF
	
	
	//load audio
	IF NOT LOAD_ALL_AUDIO_BANKS()
		CPRINTLN(DEBUG_MISSION,"[RCC MISSION][LOADED_ALL_ASSETS] MISSION - TRANSITION TIME - LOAD_ALL_AUDIO_BANKS  - FRAME COUNT = (", GET_FRAME_COUNT(), ") POSIX TIME = ", GET_CLOUD_TIME_AS_INT(), ")")
		CPRINTLN(DEBUG_MISSION,"[RCC MISSION][LOADED_ALL_ASSETS] not loaded audio banks !!!!!!")
		bAllLoaded = FALSE
	ENDIF
	
	//Load the text
	IF NOT LOAD_MC_TEXT_BLOCK()
		CPRINTLN(DEBUG_MISSION,"[RCC MISSION][LOADED_ALL_ASSETS] MISSION - TRANSITION TIME - LOAD_MC_TEXT_BLOCK  - FRAME COUNT = (", GET_FRAME_COUNT(), ") POSIX TIME = ", GET_CLOUD_TIME_AS_INT(), ")")
		CPRINTLN(DEBUG_MISSION,"[RCC MISSION][LOADED_ALL_ASSETS] not loaded text block !!!!!!")
		bAllLoaded = FALSE
	ENDIF
	
	//Load Additional Text
	IF NOT LOAD_MC_DIALOGUE_TEXT_BLOCK()	
		CPRINTLN(DEBUG_MISSION,"[RCC MISSION][LOADED_ALL_ASSETS] MISSION - TRANSITION TIME - LOAD_MC_DIALOGUE_TEXT_BLOCK  - FRAME COUNT = (", GET_FRAME_COUNT(), ") POSIX TIME = ", GET_CLOUD_TIME_AS_INT(), ")")
		CPRINTLN(DEBUG_MISSION,"[RCC MISSION][LOADED_ALL_ASSETS] not loaded DIALOGUE text block !!!!!!")
		bAllLoaded = FALSE
	ENDIF
	
	REQUEST_TEXTURE_STREAM_ASSETS()
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciOPTION_VEHICLE_WEAPON_ENABLED)
	OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciDISABLE_SPECIAL_VEHICLE_WEAPONS_EXCEPT_PICKUPS)
		IF NOT LOAD_ALL_VEHICLE_WEAPON_MODELS()
			bAllLoaded = FALSE
			PRINTLN("[RCC MISSION][LOADED_ALL_ASSETS] Not Loaded Vehicle Weapon models")
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciOPTION_VEHICLE_WEAPON_ENABLED)
	OR IS_BIT_SET(iLocalBoolCheck23, LBOOL23_TEAM_SWAP_IN_MISSION)
	OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwenty, ciOptionsBS20_ENABLE_OBJECT_CONTROLLED_VEHICLE_SWAPS)
		REQUEST_NAMED_PTFX_ASSET("scr_powerplay")
		IF NOT HAS_NAMED_PTFX_ASSET_LOADED("scr_powerplay")
			bAllLoaded = FALSE
			PRINTLN("[RCC MISSION][LOADED_ALL_ASSETS] Not Loaded beast explosion effects")
		ENDIF
	ENDIF
			
	REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_lives_bottle")))
	IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_lives_bottle")))
		PRINTLN("[RCC MISSION][LOADED_ALL_ASSETS] Not Loaded stt_prop_lives_bottle")
		bAllLoaded = FALSE
	ENDIF
		
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_TRANSFORM_ASSETS)
		REQUEST_NAMED_PTFX_ASSET("scr_powerplay")
		IF NOT HAS_NAMED_PTFX_ASSET_LOADED("scr_powerplay")
			bAllLoaded = FALSE
			PRINTLN("[RCC MISSION][LOADED_ALL_ASSETS] Not Loaded beast explosion effects")
		ENDIF
		REQUEST_ANIM_DICT("ANIM@MP_FM_EVENT@INTRO")
		IF NOT HAS_ANIM_DICT_LOADED("ANIM@MP_FM_EVENT@INTRO")
			bAllLoaded = FALSE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_TRANSFORM_ASSETS)
		REQUEST_ANIM_SET("ANIM_GROUP_MOVE_BALLISTIC")   
	    REQUEST_ANIM_SET("MOVE_STRAFE_BALLISTIC")
	    REQUEST_CLIP_SET("move_ballistic_2h")
	    IF NOT HAS_ANIM_SET_LOADED("ANIM_GROUP_MOVE_BALLISTIC")
	    OR NOT HAS_ANIM_SET_LOADED("MOVE_STRAFE_BALLISTIC")
	    OR NOT HAS_CLIP_SET_LOADED("move_ballistic_2h")
			PRINTLN("[RCC MISSION][LOADED_ALL_ASSETS] Haven't loaded juggernaut movement anims yet.")
			bAllLoaded = FALSE
	    ENDIF
	ENDIF
	
//	//2073102 - If this heist contains a drilling minigame then we need to create the wall in advance.
	IF bIsLocalPlayerHost
	AND NOT (NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_2.niDrillMinigameWall) AND NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_2.niDrillMinigameDoor))
		INT iCurrentTeam = 0
		INT iCurrentObj = 0
		
		FOR iCurrentTeam = 0 TO (FMMC_MAX_TEAMS-1)
			FOR iCurrentObj = 0 TO (FMMC_MAX_NUM_OBJECTS-1)
				IF MC_serverBD_4.iObjPriority[iCurrentObj][iCurrentTeam] < FMMC_MAX_RULES
					IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iCurrentObj].iRule[iCurrentTeam] = FMMC_OBJECTIVE_LOGIC_MINIGAME
						IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iCurrentObj].iObjectMinigame = ciFMMC_OBJECT_MINIGAME_TYPE__DRILLING
							CDEBUG1LN(DEBUG_MISSION, "[RCC MISSION][LOADED_ALL_ASSETS] [DRILLING] CREATE_DRILL_MINIGAME_PROPS - Found drill minigame object - iCurrentObj = ", iCurrentObj, " iPlayersTeam = ", iCurrentTeam)
							ASSERTLN("FMMC2020 - LOADED_ALL_ASSETS - CREATE_DRILL_MINIGAME_PROPS should be loaded elsewhere")
							IF createDrillMinigameProps != NULL AND NOT CALL createDrillMinigameProps(iCurrentObj)
								PRINTLN("[RCC MISSION][LOADED_ALL_ASSETS] [DRILLING] MISSION - TRANSITION TIME - CREATE_DRILL_MINIGAME_PROPS  - FRAME COUNT = (", GET_FRAME_COUNT(), ") POSIX TIME = ", GET_CLOUD_TIME_AS_INT(), ")")
								PRINTLN("[RCC MISSION][LOADED_ALL_ASSETS] [DRILLING] not created drill props !!!!!!")
								bAllLoaded = FALSE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
		ENDFOR
	ENDIF
		
	//Load suicide anim if needed
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciCRIT_TEAM_KILL_SELF)
		REQUEST_ANIM_DICT("MP_SUICIDE")
		IF NOT HAS_ANIM_DICT_LOADED("MP_SUICIDE")
			bAllLoaded = FALSE
		ENDIF
	ENDIF
	
	IF IS_GRATE_CUTTING_BEING_USED()
		REQUEST_ANIM_DICT("mini@biotech@blowtorch_def")
		REQUEST_ANIM_DICT("mini@biotech@blowtorch_str")
		IF NOT HAS_ANIM_DICT_LOADED("mini@biotech@blowtorch_def")
		AND NOT HAS_ANIM_DICT_LOADED("mini@biotech@blowtorch_str")
			PRINTLN("[RCC MISSION][LOADED_ALL_ASSETS] Not loaded grate cutting anim asset yet")
	 		bAllLoaded = FALSE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciENABLE_SMOKE_TRAILS)
		REQUEST_NAMED_PTFX_ASSET("scr_ar_planes")
		IF NOT HAS_NAMED_PTFX_ASSET_LOADED("scr_ar_planes")
			PRINTLN("[RCC MISSION][LOADED_ALL_ASSETS] Not loaded Smoke Trail effect asset yet")
			bAllLoaded = FALSE	
		ENDIF
	ENDIF
	
	IF HAS_ANY_VEHICLE_SWAPS_BEEN_SET()
	OR IS_BIT_SET(iLocalBoolCheck23, LBOOL23_TEAM_SWAP_IN_MISSION)
		REQUEST_NAMED_PTFX_ASSET("scr_powerplay")
		IF NOT HAS_NAMED_PTFX_ASSET_LOADED("scr_powerplay")
			bAllLoaded = FALSE
			PRINTLN("[RCC MISSION][LOADED_ALL_ASSETS] Not Loaded beast explosion effects")
		ENDIF
		REQUEST_NAMED_PTFX_ASSET("scr_sr_tr")
		IF NOT HAS_NAMED_PTFX_ASSET_LOADED("scr_sr_tr")
			bAllLoaded = FALSE
			PRINTLN("[RCC MISSION][LOADED_ALL_ASSETS] Not Loaded beast explosion effects")
		ENDIF
		REQUEST_NAMED_PTFX_ASSET("scr_as_trans")
		IF NOT HAS_NAMED_PTFX_ASSET_LOADED("scr_as_trans")
			bAllLoaded = FALSE
			PRINTLN("[RCC MISSION][LOADED_ALL_ASSETS] Not Loaded transform effects")
		ENDIF
	ENDIF
	
	IF IS_ANY_WARP_PORTALS_USING_TRANSFORM()
		REQUEST_NAMED_PTFX_ASSET("scr_as_trans")
		IF NOT HAS_NAMED_PTFX_ASSET_LOADED("scr_as_trans")
			bAllLoaded = FALSE
			PRINTLN("[RCC MISSION][LOADED_ALL_ASSETS] Not Loaded transform effects")
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciENABLE_SMOKE_ON_OUTFIT_CHANGE)
		REQUEST_NAMED_PTFX_ASSET("scr_sm_trans")
		IF NOT HAS_NAMED_PTFX_ASSET_LOADED("scr_sm_trans")
			bAllLoaded = FALSE
			PRINTLN("[RCC MISSION][LOADED_ALL_ASSETS] Not Loaded transform effects")
		ENDIF
	ENDIF
	
	IF IS_BEAM_HACK_BEING_USED()
		REQUEST_STREAMED_TEXTURE_DICT("MPBeamHack")
		IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED("MPBeamHack")
			bAllLoaded = FALSE
			PRINTLN("[RCC MISSION][LOADED_ALL_ASSETS] Not Loaded MPBeamHack")
		ENDIF
		REQUEST_STREAMED_TEXTURE_DICT("MPBeamHack_lvl0")
		IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED("MPBeamHack_lvl0")
			bAllLoaded = FALSE
			PRINTLN("[RCC MISSION][LOADED_ALL_ASSETS] Not Loaded MPBeamHack_lvl0")
		ENDIF
		REQUEST_STREAMED_TEXTURE_DICT("MPBeamHackFG")
		IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED("MPBeamHackFG")
			bAllLoaded = FALSE
			PRINTLN("[RCC MISSION][LOADED_ALL_ASSETS] Not Loaded MPBeamHackFG")
		ENDIF
		IF NOT REQUEST_SCRIPT_AUDIO_BANK("DLC_CHRISTMAS2017/XM_Silo_Laser_Hack")
			bAllLoaded = FALSE
			PRINTLN("[RCC MISSION][LOADED_ALL_ASSETS] Not Loaded audio bank for beam hack")
		ENDIF
	ENDIF
	
	IF IS_HOTWIRE_BEING_USED()
		REQUEST_STREAMED_TEXTURE_DICT("MPHotwire")
		IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED("MPHotwire")
			bAllLoaded = FALSE
			PRINTLN("[RCC MISSION][LOADED_ALL_ASSETS] Not Loaded MPHotwire")
		ENDIF
	ENDIF
	
	IF IS_ORDER_UNLOCK_BEING_USED()
	OR IS_FINGERPRINT_CLONE_BEING_USED()
		REQUEST_STREAMED_TEXTURE_DICT("MPHotwire")
		IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED("MPHotwire")
			bAllLoaded = FALSE
			PRINTLN("[RCC MISSION][LOADED_ALL_ASSETS] Not Loaded MPHotwire TEMP")
		ENDIF
	ENDIF
	
	IF IS_ORBITAL_CANNON_BEING_USED()
		
		REQUEST_STREAMED_TEXTURE_DICT("helicopterhud")
		
		IF NOT g_bMissionPlacedOrbitalCannon
			PRINTLN("[RCC MISSION][LOADED_ALL_ASSETS] - Setting g_bMissionPlacedOrbitalCannon as TRUE")
			g_bMissionPlacedOrbitalCannon = TRUE // Set the bool saying we need to run the am_mp_orbital_cannon script
		ENDIF
		
		IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED("helicopterhud")
			bAllLoaded = FALSE
			PRINTLN("[RCC MISSION][LOADED_ALL_ASSETS] Not Loaded helicopterhud")
		ENDIF
	ENDIF
	
	IF IS_VOLTAGE_BEING_USED()
		REQUEST_STREAMED_TEXTURE_DICT("MPIsland_Voltage")
		IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED("MPIsland_Voltage")
			bAllLoaded = FALSE
			PRINTLN("[RCC MISSION][LOADED_ALL_ASSETS] Not Loaded MPIsland_Voltage")
		ENDIF
	ENDIF
		
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_CCTVAlertSpottedSounds)
	AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFive, ciOptionsBS25_EnableCasinoHeistAudioBank)
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_UseCasinoCCTVAudio)
		
			IF NOT REQUEST_SCRIPT_AUDIO_BANK("SCRIPT/ALARM_BELL_01") 
				PRINTLN("[RCC MISSION][LOADED_ALL_ASSETS] Waiting for SCRIPT/ALARM_BELL_01")
				bAllLoaded = FALSE
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_UseCasinoCCTVAudio)
			IF NOT REQUEST_SCRIPT_AUDIO_BANK("DLC_VINEWOOD/VW_CASINO_FINALE") 
				PRINTLN("[RCC MISSION][LOADED_ALL_ASSETS] Waiting for DLC_VINEWOOD/VW_CASINO_FINALE")
				bAllLoaded = FALSE			
			ENDIF
		ELSE
			IF NOT REQUEST_SCRIPT_AUDIO_BANK("SCRIPT/ALARM_KLAXON_04") 
				PRINTLN("[RCC MISSION][LOADED_ALL_ASSETS] Waiting for SCRIPT/ALARM_KLAXON_04")
				bAllLoaded = FALSE			
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions, IPL_ISLAND)
		IF NOT REQUEST_SCRIPT_AUDIO_BANK("DLC_HEI4/DLCHEI4_GENERIC_01")
			PRINTLN("[RCC MISSION][LOADED_ALL_ASSETS] Waiting for DLC_HEI4/DLCHEI4_GENERIC_01")
			bAllLoaded = FALSE
		ENDIF
	ENDIF
	
	REQUEST_NAMED_PTFX_ASSET("scr_mp_creator")
	IF NOT HAS_NAMED_PTFX_ASSET_LOADED("scr_mp_creator")
		bAllLoaded = FALSE
		PRINTLN("[RCC MISSION][LOADED_ALL_ASSETS] Not Loaded scr_mp_creator")
	ENDIF
	
	IF g_FMMC_STRUCT.sPedAmmoDrop.iAmmoPickupType != FMMC_PED_AMMO_DROP_TYPE_INVALID
		REQUEST_MODEL(mnPedAmmoDropModel)
		IF NOT HAS_MODEL_LOADED(mnPedAmmoDropModel)
			bAllLoaded = FALSE
			PRINTLN("[RCC MISSION][LOADED_ALL_ASSETS] Not Loaded Ped Drop Ammo Model")
		ELSE
			PRINTLN("[RCC MISSION][LOADED_ALL_ASSETS] Loaded Ped Drop Ammo Model")
		ENDIF
	ENDIF
	
	IF SHOULD_PLAYER_PED_MODEL_BE_SWAPPED(GET_PRE_ASSIGNMENT_PLAYER_TEAM())
		REQUEST_MODEL(GET_FMMC_PLAYER_PED_MODEL(GET_PRE_ASSIGNMENT_PLAYER_TEAM()))
		IF NOT HAS_MODEL_LOADED(GET_FMMC_PLAYER_PED_MODEL(GET_PRE_ASSIGNMENT_PLAYER_TEAM()))
			bAllLoaded = FALSE
			PRINTLN("[RCC MISSION][LOADED_ALL_ASSETS][Player Model Swap] Not Loaded special ped model")
		ENDIF
	ENDIF
	
	IF NOT bAllLoaded
		PRINTLN("[RCC MISSION][LOADED_ALL_ASSETS] - Still waiting...")
	ENDIF
	
	RETURN bAllLoaded
ENDFUNC


// ##### ------------------------------------------------------------------------------------------------------------------------
// ##### Large Section Name: RUNTIME --------------------------------------------------------------------------------------------
// ##### Description: Section of functions used during the mission to get asset information.  -----------------------------------
// ##### ------------------------------------------------------------------------------------------------------------------------
// ##### ------------------------------------------------------------------------------------------------------------------------

// ##### ------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Runtime Ped Animations -----------------------------------------------------------------------------------
// ##### Description: Animation functions used for NPC ped animations at runtime.  ----------------------------------------------
// ##### ------------------------------------------------------------------------------------------------------------------------
// ##### ------------------------------------------------------------------------------------------------------------------------

FUNC STRING GET_DICT_FOR_PED_PHONE_EMP_REACTION(INT iPed, PED_INDEX piPed, INT iRandomInt, BOOL bUseFlashlight)

	UNUSED_PARAMETER(iPed)
	UNUSED_PARAMETER(piPed)
	
	IF bUseFlashlight
		SWITCH iRandomInt
			CASE 1				RETURN "anim@weapons@flashlight@stealth"
		ENDSWITCH
	ELSE
		SWITCH iRandomInt
			CASE 1				RETURN "AMB@CODE_HUMAN_POLICE_INVESTIGATE@idle_a"
			CASE 2				RETURN "AMB@CODE_HUMAN_POLICE_INVESTIGATE@idle_b"
		ENDSWITCH
	ENDIF
	
	RETURN "INVALID"
ENDFUNC

FUNC STRING GET_ANIM_FOR_PED_PHONE_EMP_REACTION(INT iPed, PED_INDEX piPed, INT iRandomInt, BOOL bUseFlashlight)

	UNUSED_PARAMETER(iPed)
	UNUSED_PARAMETER(piPed)
	
	IF bUseFlashlight
		SWITCH iRandomInt
			CASE 1				RETURN "aim_med_loop"
		ENDSWITCH
	ELSE
		SWITCH iRandomInt
			CASE 1				RETURN "idle_a"
			CASE 2				RETURN "idle_d"
		ENDSWITCH
	ENDIF
	
	RETURN "INVALID"
ENDFUNC

// ##### ------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Runtime Audio --------------------------------------------------------------------------------------------
// ##### Description: Audio functions used during runtime.  ---------------------------------------------------------------------
// ##### ------------------------------------------------------------------------------------------------------------------------
// ##### ------------------------------------------------------------------------------------------------------------------------

/// PURPOSE: Get the correct sound bank ID for the alarm prop passed in, as well as setting up the alarm timer
PROC REQUEST_ALARM_SOUND_BANK_AND_SOUND_ID( INT iAlarmPropIndex )
	STRING sAlarmBank
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iAlarmPropIndex].iPropBitSet2, ciFMMC_PROP2_TriggerAlarmViaCCTV) 
	OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iAlarmPropIndex].iPropBitSet2, ciFMMC_PROP2_TriggerAlarmViaCCTV) AND HAVE_CCTV_CAMERAS_WITH_AGGRO_BS_BEEN_ALERTED(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iAlarmPropIndex].iAggroIndexBS_Entity_Prop, MC_PlayerBD[iPartToUse].iTeam)
		IF NOT HAS_NET_TIMER_STARTED( AlarmTimer[ iAlarmPropIndex ] )
			START_NET_TIMER( AlarmTimer[ iAlarmPropIndex ] )
		ENDIF
		
		INT iDelayFromAlarmProp = g_FMMC_STRUCT_ENTITIES.sPlacedProp[ iAlarmPropIndex ].iAlarmDelay

		IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME( AlarmTimer[ iAlarmPropIndex ] ) >= iDelayFromAlarmProp
			// Gettin the correct sound bank string for this alarm
			sAlarmBank = GET_FMMC_ALARM_BANK_FROM_SELECTION( g_FMMC_STRUCT_ENTITIES.sPlacedProp[ iAlarmPropIndex ].iAlarmSound )
			IF NOT IS_STRING_NULL_OR_EMPTY( sAlarmBank )
				IF REQUEST_SCRIPT_AUDIO_BANK( sAlarmBank )
					PRINTLN("[RCC MISSION] [ALARM] ALARM team at objective 1: ",g_FMMC_STRUCT_ENTITIES.sPlacedProp[ iAlarmPropIndex ].iAssociatedObjective )
					isoundid[ iAlarmPropIndex ] = GET_SOUND_ID()
					IF IS_THIS_IS_A_STRAND_MISSION()
					AND NOT HAS_FIRST_STRAND_MISSION_BEEN_PASSED()
						PASS_OVER_CUT_SCENE_ALARM(oiProps[ iAlarmPropIndex ], isoundid[ iAlarmPropIndex ], g_FMMC_STRUCT_ENTITIES.sPlacedProp[ iAlarmPropIndex ].iAlarmSound)
					ENDIF
				ELSE
					PRINTLN( "[RCC MISSION] requesting audio bank" )
				ENDIF
			ELSE
				PRINTLN( "[RCC MISSION] [ALARM] ALARM team at objective: ", g_FMMC_STRUCT_ENTITIES.sPlacedProp[ iAlarmPropIndex ].iAssociatedObjective )
				isoundid[ iAlarmPropIndex ] = GET_SOUND_ID()
				IF IS_THIS_IS_A_STRAND_MISSION()
				AND NOT HAS_FIRST_STRAND_MISSION_BEEN_PASSED()
					PASS_OVER_CUT_SCENE_ALARM(oiProps[ iAlarmPropIndex ], isoundid[ iAlarmPropIndex ], g_FMMC_STRUCT_ENTITIES.sPlacedProp[ iAlarmPropIndex ].iAlarmSound)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC STRING GET_BEAST_MODE_SOUNDSET()
	STRING sSoundSet = "APT_BvS_Soundset"
	
	PRINTLN("[RCC MISSION] GET_BEAST_MODE_SOUNDSET - Returning soundset: ", sSoundSet)
	RETURN sSoundSet
ENDFUNC

FUNC STRING GET_LIGHTS_OFF_AUDIO_SCENE_NAME()

	IF USING_FMMC_YACHT()
		RETURN ""
	ENDIF
	
	RETURN "dlc_ch_heist_finale_emp_scene"
ENDFUNC

FUNC STRING GET_ARTIFICIAL_LIGHTS_AUDIOBANK__LIGHTS_OFF()
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_ENABLE_ALTERNATE_LIGHTS_OFF_AUDIO)
		RETURN "DLC_CHRISTMAS2017/XM_STEALAVG"
	ENDIF
	
	IF USING_FMMC_YACHT()
		RETURN "DLC_SUM20/SUM20_Yacht_LAS_01"
	ENDIF
	
	RETURN "DLC_HALLOWEEN/FVJ_01"
ENDFUNC
FUNC STRING GET_ARTIFICIAL_LIGHTS_SOUNDSET__LIGHTS_OFF()

	IF IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions, IPL_ISLAND)
		RETURN "DLC_H4_scripted_island_power_sounds"
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_ENABLE_ALTERNATE_LIGHTS_OFF_AUDIO)
		RETURN "dlc_xm_stealavg_sounds"
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciENABLE_SLASHERS_AUDIO)
		RETURN "dlc_xm_sls_Sounds"
	ENDIF
	
	IF USING_FMMC_YACHT()
		RETURN "dlc_sum20_yacht_missions_lost_at_sea_sounds"
	ENDIF
				
	RETURN "DLC_HALLOWEEN_FVJ_Sounds"
ENDFUNC
FUNC STRING GET_ARTIFICIAL_LIGHTS_SOUNDNAME__LIGHTS_OFF()

	IF IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions, IPL_ISLAND)
		RETURN "PowerDown"
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_ENABLE_ALTERNATE_LIGHTS_OFF_AUDIO)
		RETURN "lights_off"
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySix, ciOptionsBS26_UseHalloweenLightsOff)
	OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciENABLE_SLASHERS_AUDIO)
		RETURN "Breaker_02"
	ENDIF
	
	IF USING_FMMC_YACHT()
		RETURN "lights_off"
	ENDIF
		
	RETURN "EMP"
ENDFUNC


FUNC STRING GET_ARTIFICIAL_LIGHTS_AUDIOBANK__LIGHTS_BACK_ON()
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_ENABLE_ALTERNATE_LIGHTS_OFF_AUDIO)
		RETURN "DLC_CHRISTMAS2017/XM_STEALAVG"
	ENDIF	
	
	IF USING_FMMC_YACHT()
		RETURN "DLC_SUM20/SUM20_Yacht_LAS_01"
	ENDIF
	
	RETURN "DLC_HALLOWEEN/FVJ_01"
ENDFUNC
FUNC STRING GET_ARTIFICIAL_LIGHTS_SOUNDSET__LIGHTS_BACK_ON()
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_ENABLE_ALTERNATE_LIGHTS_OFF_AUDIO)
		RETURN "dlc_xm_stealavg_sounds"
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciENABLE_SLASHERS_AUDIO)
		RETURN "dlc_xm_sls_Sounds"
	ENDIF
	
	IF USING_FMMC_YACHT()
		RETURN "dlc_sum20_yacht_missions_lost_at_sea_sounds"
	ENDIF
	
	IF GET_ULP_MISSION_FROM_ROOT_CONTENT_ID_HASH(g_FMMC_STRUCT.iRootContentIDHash) = ciULP_MISSION_6_CLEANUP
		RETURN "ULP_Mission_Going_Underground_Sounds"
	ENDIF
	
	RETURN ""
ENDFUNC
FUNC STRING GET_ARTIFICIAL_LIGHTS_SOUNDNAME__LIGHTS_BACK_ON()

	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_ENABLE_ALTERNATE_LIGHTS_OFF_AUDIO)
		RETURN "lights_on"
	ENDIF
	
	IF USING_FMMC_YACHT()
		RETURN "lights_on"
	ENDIF
	
	IF GET_ULP_MISSION_FROM_ROOT_CONTENT_ID_HASH(g_FMMC_STRUCT.iRootContentIDHash) = ciULP_MISSION_6_CLEANUP
		RETURN "lights_on"
	ENDIF
		
	RETURN ""
ENDFUNC

// ##### ------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Runtime Weapon Assets ------------------------------------------------------------------------------------
// ##### Description: Weapon Asset functions used during runtime.  --------------------------------------------------------------
// ##### ------------------------------------------------------------------------------------------------------------------------
// ##### ------------------------------------------------------------------------------------------------------------------------
FUNC WEAPON_TYPE GET_FIRE_ROCKETS_ZONE_WEAPON_TYPE(INT iZone)
	UNUSED_PARAMETER(iZone) // May create a per-zone rocket type option in the future
	RETURN WEAPONTYPE_RPG
ENDFUNC

// ##### ------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Runtime Timecycle Modifiers ------------------------------------------------------------------------------
// ##### Description: Timecyle Modifier functions used during runtime.  ---------------------------------------------------------
// ##### ------------------------------------------------------------------------------------------------------------------------
// ##### ------------------------------------------------------------------------------------------------------------------------
FUNC STRING GET_LIGHTS_OFF_TIMECYCLE_MODIFIER()
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySix, ciOptionsBS26_UseHalloweenLightsOff)
		RETURN "grdlc_int_02"
	ENDIF
	
	IF USING_FMMC_YACHT()
		RETURN "Yacht_Mission_NoLig"
	ENDIF
	
	IF CONTENT_IS_USING_CASINO_HEIST_INTERIOR()
		RETURN "Casino_Lightsoff"
	ENDIF
	
	IF GET_HASH_KEY("xs_x18_int_mod2") = iLocalPlayerCurrentInteriorHash
		RETURN "MP_Sum2_Warehouse_No_Ambient"
	ENDIF
	
	RETURN "NoPedLight" 
ENDFUNC

FUNC STRING GET_LIGHTS_OFF_EXTRA_TIMECYCLE_MODIFIER()
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySix, ciOptionsBS26_UseHalloweenLightsOff)
		RETURN "NoPedLight"
	ENDIF
	
	RETURN "" 
ENDFUNC

// ##### ------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Runtime Effects ------------------------------------------------------------------------------------------
// ##### Description: Effects functions used during runtime.  -------------------------------------------------------------------
// ##### ------------------------------------------------------------------------------------------------------------------------
// ##### ------------------------------------------------------------------------------------------------------------------------

FUNC STRING GET_PTFX_DISSOLVE_NAME_BASED_ON_PROP(MODEL_NAMES mn)
	IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("STT_PROP_STUNT_TRACK_SHORT"))
	OR mn = INT_TO_ENUM(MODEL_NAMES, HASH("STT_PROP_STUNT_TRACK_STRAIGHT"))
	OR mn = INT_TO_ENUM(MODEL_NAMES, HASH("STT_PROP_TRACK_STRAIGHT_S"))
	OR mn = INT_TO_ENUM(MODEL_NAMES, HASH("STT_PROP_TRACK_STRAIGHT_M"))
	OR mn = INT_TO_ENUM(MODEL_NAMES, HASH("STT_PROP_TRACK_STRAIGHT_LM"))
	OR mn = INT_TO_ENUM(MODEL_NAMES, HASH("STT_PROP_TRACK_STRAIGHT_L"))
	OR mn = INT_TO_ENUM(MODEL_NAMES, HASH("STT_PROP_STUNT_BLOCK_HUGE_03"))
	OR mn = INT_TO_ENUM(MODEL_NAMES, HASH("ba_Prop_Track_Straight_LM"))
		RETURN "scr_as_trap_zone_rectangle"
	ENDIF
	
	IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("AS_PROP_AS_STUNT_TARGET"))
	OR mn = INT_TO_ENUM(MODEL_NAMES, HASH("AS_PROP_AS_STUNT_TARGET_SMALL"))
		RETURN "scr_as_trap_zone_circle"
	ENDIF
	
	RETURN "scr_as_trap_zone_rectangle"
ENDFUNC

FUNC FLOAT GET_PTFX_DISSOLVE_LENGTH_BASED_ON_PROP(MODEL_NAMES mn)
	IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("STT_PROP_STUNT_TRACK_SHORT"))
	OR mn = INT_TO_ENUM(MODEL_NAMES, HASH("STT_PROP_STUNT_TRACK_STRAIGHT"))
		RETURN 0.6
	ENDIF
	
	IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("STT_PROP_TRACK_STRAIGHT_S"))
		RETURN 0.1
	ENDIF
	
	IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("STT_PROP_TRACK_STRAIGHT_M"))
		RETURN 0.31
	ENDIF
	
	IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("ba_Prop_Track_Straight_LM"))
		RETURN 0.31
	ENDIF
	
	IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("STT_PROP_TRACK_STRAIGHT_LM"))	
		RETURN 0.63
	ENDIF
	
	IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("STT_PROP_TRACK_STRAIGHT_L"))
		RETURN 0.95
	ENDIF
	
	IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("STT_PROP_STUNT_BLOCK_HUGE_03"))
		RETURN 0.5
	ENDIF
	
	IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("AS_PROP_AS_STUNT_TARGET"))
	OR mn = INT_TO_ENUM(MODEL_NAMES, HASH("AS_PROP_AS_STUNT_TARGET_SMALL"))
		RETURN 1.0
	ENDIF
	RETURN 0.4
ENDFUNC

FUNC FLOAT GET_PTFX_DISSOLVE_WIDTH_BASED_ON_PROP(MODEL_NAMES mn)
	IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("STT_PROP_STUNT_TRACK_SHORT"))
	OR mn = INT_TO_ENUM(MODEL_NAMES, HASH("STT_PROP_STUNT_TRACK_STRAIGHT"))
		RETURN 0.08
	ENDIF
	
	IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("STT_PROP_TRACK_STRAIGHT_S"))
	OR mn = INT_TO_ENUM(MODEL_NAMES, HASH("STT_PROP_TRACK_STRAIGHT_M"))
	OR mn = INT_TO_ENUM(MODEL_NAMES, HASH("STT_PROP_TRACK_STRAIGHT_LM"))
	OR mn = INT_TO_ENUM(MODEL_NAMES, HASH("STT_PROP_TRACK_STRAIGHT_L"))
	OR mn = INT_TO_ENUM(MODEL_NAMES, HASH("ba_Prop_Track_Straight_LM"))
		RETURN 0.135
	ENDIF
	
	IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("STT_PROP_STUNT_BLOCK_HUGE_03"))
		RETURN 0.4
	ENDIF
	
	IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("AS_PROP_AS_STUNT_TARGET"))
	OR mn = INT_TO_ENUM(MODEL_NAMES, HASH("AS_PROP_AS_STUNT_TARGET_SMALL"))
		RETURN 1.0
	ENDIF
	RETURN 0.5
ENDFUNC

// ##### ------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Runtime Models -------------------------------------------------------------------------------------------
// ##### Description: Models functions used during runtime.  --------------------------------------------------------------------
// ##### ------------------------------------------------------------------------------------------------------------------------
// ##### ------------------------------------------------------------------------------------------------------------------------

FUNC MODEL_NAMES MC_GET_MODEL_FOR_CUSTOM_PICKUP(INT iType, INT iTeam, INT iCustomModelIndex)
	PRINTLN("[JS] MC_GET_MODEL_FOR_CUSTOM_PICKUP - iType = ", iType)
	PRINTLN("[JS] MC_GET_MODEL_FOR_CUSTOM_PICKUP - iTeam = ", iTeam)
	PRINTLN("[JS] MC_GET_MODEL_FOR_CUSTOM_PICKUP - iCustomModelIndex = ", iCustomModelIndex)
	
	//using white models for spectators
	IF bIsAnySpectator
		iTeam = -1
	ENDIF
	
	IF iType = ciCUSTOM_PICKUP_TYPE__PLAYER_LIVES
		RETURN INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_lives_bottle"))
	ELIF iType = ciCUSTOM_PICKUP_TYPE__WEAPON_BAG
		RETURN GET_MODEL_NAME_FOR_WEAPON_BAG(iCustomModelIndex)
	ELIF iType = ciCUSTOM_PICKUP_TYPE__MISSION_EQUIPMENT
		RETURN GET_MODEL_NAME_FOR_MISSION_EQUIPMENT_PICKUP(iCustomModelIndex)
	ELIF iType = ciCUSTOM_PICKUP_TYPE__EXTRA_TAKE
		RETURN INT_TO_ENUM(MODEL_NAMES, HASH("h4_Prop_h4_Cash_Stack_02a"))
	ELIF iType = ciCUSTOM_PICKUP_TYPE__FREEMODE_COLLECTABLE
		RETURN GET_MODEL_NAME_FOR_FMMC_FREEMODE_COLLECTABLE_PICKUP(iCustomModelIndex)
	ENDIF
	
	SWITCH(iTeam)
	CASE -1
		SWITCH(iType)
			CASE ciCUSTOM_PICKUP_TYPE__BEAST_MODE 
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_BMD_WH"))
			CASE ciCUSTOM_PICKUP_TYPE__BULLSHARK_TEST	
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_B_SHARK_WH"))
			CASE ciCUSTOM_PICKUP_TYPE__THERMAL_VISION
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_MK_THERMAL_WH"))
			CASE ciCUSTOM_PICKUP_TYPE__SWAP	
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_SWAP_WH"))
			CASE ciCUSTOM_PICKUP_TYPE__FILTER	
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_WEED_WH"))
			CASE ciCUSTOM_PICKUP_TYPE__SLOW_DOWN 
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_MK_S_TIME_WH"))
			CASE ciCUSTOM_PICKUP_TYPE__BULLET_TIME
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_B_TIME_WH"))
			CASE ciCUSTOM_PICKUP_TYPE__HIDE_BLIPS
				//[JS] Add blip model
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_HIDDEN_WH"))
			CASE ciCUSTOM_PICKUP_TYPE__RANDOM
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_RANDOM_WH"))
			DEFAULT
				RETURN DUMMY_MODEL_FOR_SCRIPT
		ENDSWITCH
		BREAK
	CASE 0
		SWITCH(iType)
			CASE ciCUSTOM_PICKUP_TYPE__BEAST_MODE 
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_BMD"))
			CASE ciCUSTOM_PICKUP_TYPE__BULLSHARK_TEST	
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_B_SHARK"))
			CASE ciCUSTOM_PICKUP_TYPE__THERMAL_VISION
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_MK_THERMAL"))
			CASE ciCUSTOM_PICKUP_TYPE__SWAP	
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_SWAP_P"))
			CASE ciCUSTOM_PICKUP_TYPE__FILTER	
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_WEED_P"))
			CASE ciCUSTOM_PICKUP_TYPE__SLOW_DOWN 
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_MK_S_TIME"))
			CASE ciCUSTOM_PICKUP_TYPE__BULLET_TIME
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_B_TIME"))
			CASE ciCUSTOM_PICKUP_TYPE__HIDE_BLIPS
				//[JS] Add blip model
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_HIDDEN"))
			CASE ciCUSTOM_PICKUP_TYPE__RANDOM
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_RANDOM"))
			DEFAULT
				RETURN DUMMY_MODEL_FOR_SCRIPT
		ENDSWITCH
	BREAK
	CASE 1
		SWITCH(iType)
			CASE ciCUSTOM_PICKUP_TYPE__BEAST_MODE 
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_BMD_P"))
			CASE ciCUSTOM_PICKUP_TYPE__BULLSHARK_TEST	
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_B_SHARK_P"))
			CASE ciCUSTOM_PICKUP_TYPE__THERMAL_VISION
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_MK_THERMAL"))
			CASE ciCUSTOM_PICKUP_TYPE__SWAP	
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_SWAP"))
			CASE ciCUSTOM_PICKUP_TYPE__FILTER	
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_WEED"))
			CASE ciCUSTOM_PICKUP_TYPE__SLOW_DOWN 
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_MK_S_TIME"))
			CASE ciCUSTOM_PICKUP_TYPE__BULLET_TIME
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_B_TIME_P"))
			CASE ciCUSTOM_PICKUP_TYPE__HIDE_BLIPS
				//[JS] Add blip model
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_HIDDEN_P"))
			CASE ciCUSTOM_PICKUP_TYPE__RANDOM
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_RANDOM_P"))
			DEFAULT
				RETURN DUMMY_MODEL_FOR_SCRIPT
		ENDSWITCH
	BREAK
	DEFAULT
		RETURN DUMMY_MODEL_FOR_SCRIPT
	ENDSWITCH
	
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

//fmmc2020 - potential delete
FUNC MODEL_NAMES GET_MODEL_FOR_VEHICLE_WEAPON(INT iType, INT iTeam, INT iWeaponIndex)
	UNUSED_PARAMETER(iWeaponIndex)
	PRINTLN("[KH] GET_MODEL_FOR_VEHICLE_WEAPON - iType: ", iType, " - iTeam: ", iTeam)
	
	//using white models for spectators
	IF bIsAnySpectator
	OR IS_PLAYER_IN_PROCESS_OF_JOINING_MISSION_AS_SPECTATOR( LocalPlayer )
	OR DID_PLAYER_JOIN_MISSION_AS_SPECTATOR( LocalPlayer )
		iTeam = -1
	ENDIF
		
	SWITCH(iTeam)
	CASE -1
		SWITCH(iType)
			CASE ciVEH_WEP_ROCKETS
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Rock_WH"))
			BREAK
			CASE ciVEH_WEP_SPEED_BOOST	
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Boost_WH"))
			CASE ciVEH_WEP_GHOST
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Ghost_WH"))
			CASE ciVEH_WEP_BEAST
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Arm_WH"))
			CASE ciVEH_WEP_PRON
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_DeadL_WH"))
			BREAK
			CASE ciVEH_WEP_FORCE_ACCELERATE	
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Acce_WH"))
			CASE ciVEH_WEP_FLIPPED_CONTROLS
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_SWAP_WH"))
			BREAK
			CASE ciVEH_WEP_ZONED
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_B_TIME_WH"))
			CASE ciVEH_WEP_DETONATE
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Deton_WH"))
			BREAK
			CASE ciVEH_WEP_BOMB
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Bomb_WH"))
			BREAK
			CASE ciVEH_WEP_BOUNCE
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Hop_WH"))
			CASE ciVEH_WEP_REPAIR
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Repair_WH"))
			CASE ciVEH_WEP_RANDOM
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_RANDOM_WH"))
			BREAK
			CASE ciVEH_WEP_MACHINE_GUN
				RETURN VEHICLE_GUN_PICKUP_MODEL(iTeam)
				
			CASE ciVEH_WEP_RANDOM_SPECIAL_VEH
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Special_Vehicle_WH"))
			BREAK
			CASE ciVEH_WEP_RUINER_SPECIAL_VEH
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Special_Ruiner_WH"))
			BREAK
			CASE ciVEH_WEP_RAMP_SPECIAL_VEH
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Special_Buggy_WH"))
			BREAK
			CASE ciVEH_WEP_BOMB_LENGTH
				IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWeaponIndex].iVehBombLengthIncrease < 0
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_blastdec"))
				ENDIF
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_blastinc"))
			BREAK	
			CASE ciVEH_WEP_BOMB_MAX
				IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWeaponIndex].iVehBombMaxIncrease < 0
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_bombdec"))
				ENDIF
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_bombinc"))
			BREAK
			CASE ciVEH_WEP_EXTRA_LIFE
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_lifeinc"))
			DEFAULT
				RETURN DUMMY_MODEL_FOR_SCRIPT
		ENDSWITCH
	BREAK
	CASE 0
		SWITCH(iType)
			CASE ciVEH_WEP_ROCKETS
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Rock"))
			BREAK
			CASE ciVEH_WEP_SPEED_BOOST	
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Boost"))
			CASE ciVEH_WEP_GHOST
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Ghost"))
			CASE ciVEH_WEP_BEAST
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Arm"))
			CASE ciVEH_WEP_PRON
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_DeadL"))
			BREAK
			CASE ciVEH_WEP_FORCE_ACCELERATE	
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Acce_P"))
			CASE ciVEH_WEP_FLIPPED_CONTROLS
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_SWAP_P"))			
			BREAK
			CASE ciVEH_WEP_ZONED
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_B_TIME"))
			CASE ciVEH_WEP_DETONATE
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Deton"))
			CASE ciVEH_WEP_BOMB
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Bomb"))
				BREAK
			CASE ciVEH_WEP_BOUNCE
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Hop"))
			CASE ciVEH_WEP_REPAIR
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Repair"))
			CASE ciVEH_WEP_RANDOM
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_RANDOM"))
				BREAK
			CASE ciVEH_WEP_MACHINE_GUN
				RETURN VEHICLE_GUN_PICKUP_MODEL(iTeam)
				
			CASE ciVEH_WEP_RANDOM_SPECIAL_VEH
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Special_Vehicle"))
				BREAK
			BREAK
			CASE ciVEH_WEP_RUINER_SPECIAL_VEH
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Special_Ruiner"))
			BREAK
			CASE ciVEH_WEP_RAMP_SPECIAL_VEH
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Special_Buggy"))
			BREAK
			CASE ciVEH_WEP_BOMB_LENGTH
				IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWeaponIndex].iVehBombLengthIncrease < 0
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_blastdec_orange"))
				ENDIF
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_blastinc_orange"))
			BREAK	
			CASE ciVEH_WEP_BOMB_MAX
				IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWeaponIndex].iVehBombMaxIncrease < 0
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_bombdec_orange"))
				ENDIF
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_bombinc_orange"))
			BREAK
			CASE ciVEH_WEP_EXTRA_LIFE
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_lifeinc_orange"))
			DEFAULT
				RETURN DUMMY_MODEL_FOR_SCRIPT
		ENDSWITCH
	BREAK
	CASE 1
		SWITCH(iType)
			CASE ciVEH_WEP_ROCKETS
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Rock_P"))
			CASE ciVEH_WEP_SPEED_BOOST	
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Boost_P"))
			CASE ciVEH_WEP_GHOST
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Ghost_P"))
			CASE ciVEH_WEP_BEAST
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Arm_P"))
			CASE ciVEH_WEP_PRON
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_DeadL_P"))
			CASE ciVEH_WEP_FORCE_ACCELERATE	
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Accel"))
			CASE ciVEH_WEP_FLIPPED_CONTROLS
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_SWAP"))
				BREAK
			CASE ciVEH_WEP_ZONED
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_B_TIME_P"))
			CASE ciVEH_WEP_DETONATE
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Deton_P"))
			CASE ciVEH_WEP_BOMB
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Bomb_P"))
			CASE ciVEH_WEP_BOUNCE
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Hop_P"))
			CASE ciVEH_WEP_REPAIR
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Repair_P"))
			CASE ciVEH_WEP_RANDOM
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_RANDOM_P"))
				BREAK
			CASE ciVEH_WEP_MACHINE_GUN
				RETURN VEHICLE_GUN_PICKUP_MODEL(iTeam)
				
			CASE ciVEH_WEP_RANDOM_SPECIAL_VEH
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Special_Vehicle_P"))
			BREAK
			CASE ciVEH_WEP_RUINER_SPECIAL_VEH
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Special_Ruiner_P"))
			BREAK
			CASE ciVEH_WEP_RAMP_SPECIAL_VEH
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Special_Buggy_P"))
			BREAK
			CASE ciVEH_WEP_BOMB_LENGTH
				IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWeaponIndex].iVehBombLengthIncrease < 0
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_blastdec_purple"))
				ENDIF
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_blastinc_purple"))
			BREAK	
			CASE ciVEH_WEP_BOMB_MAX
				IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWeaponIndex].iVehBombMaxIncrease < 0
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_bombdec_purple"))
				ENDIF
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_bombinc_purple"))
			BREAK
			CASE ciVEH_WEP_EXTRA_LIFE
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_lifeinc_purple"))
			DEFAULT
				RETURN DUMMY_MODEL_FOR_SCRIPT
		ENDSWITCH
	BREAK
	CASE 2
		SWITCH(iType)
			CASE ciVEH_WEP_ROCKETS
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Rock_PK"))
				BREAK
			CASE ciVEH_WEP_SPEED_BOOST	
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Boost_PK"))
			CASE ciVEH_WEP_GHOST
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Ghost_PK"))
			CASE ciVEH_WEP_BEAST
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Arm_PK"))
			CASE ciVEH_WEP_PRON
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_DeadL_PK"))
				BREAK
			CASE ciVEH_WEP_FORCE_ACCELERATE	
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Accel_PK"))
			CASE ciVEH_WEP_FLIPPED_CONTROLS
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_SWAP_PK"))
				BREAK
			CASE ciVEH_WEP_ZONED
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_B_TIME_PK"))
			CASE ciVEH_WEP_DETONATE
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Deton_PK"))
			CASE ciVEH_WEP_BOMB
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Bomb_PK"))
				BREAK
			CASE ciVEH_WEP_BOUNCE
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Hop_PK"))
			CASE ciVEH_WEP_REPAIR
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Repair_PK"))
			CASE ciVEH_WEP_RANDOM
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_RANDOM_PK"))
				BREAK
			CASE ciVEH_WEP_MACHINE_GUN
				RETURN VEHICLE_GUN_PICKUP_MODEL(iTeam)
			
			CASE ciVEH_WEP_RANDOM_SPECIAL_VEH
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Special_Vehicle_PK"))
			BREAK
			CASE ciVEH_WEP_RUINER_SPECIAL_VEH
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Special_Ruiner_PK"))
			BREAK
			CASE ciVEH_WEP_RAMP_SPECIAL_VEH
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Special_Buggy_PK"))
			BREAK
			CASE ciVEH_WEP_BOMB_LENGTH
				IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWeaponIndex].iVehBombLengthIncrease < 0
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_blastdec_pink"))
				ENDIF
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_blastinc_pink"))
			BREAK	
			CASE ciVEH_WEP_BOMB_MAX
				IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWeaponIndex].iVehBombMaxIncrease < 0
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_bombdec_pink"))
				ENDIF
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_bombinc_pink"))
			BREAK
			CASE ciVEH_WEP_EXTRA_LIFE
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_lifeinc_pink"))
			DEFAULT
				RETURN DUMMY_MODEL_FOR_SCRIPT
		ENDSWITCH
	BREAK
	CASE 3
		SWITCH(iType)
			CASE ciVEH_WEP_ROCKETS
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Rock_G"))
				BREAK
			CASE ciVEH_WEP_SPEED_BOOST	
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Boost_G"))
			CASE ciVEH_WEP_GHOST
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Ghost_G"))
			CASE ciVEH_WEP_BEAST
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Arm_G"))
			CASE ciVEH_WEP_PRON
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_DeadL_G"))
			BREAK
			CASE ciVEH_WEP_FORCE_ACCELERATE	
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Accel_G"))
			CASE ciVEH_WEP_FLIPPED_CONTROLS
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_SWAP_G"))
			BREAK
			CASE ciVEH_WEP_ZONED
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_B_TIME_G"))
			CASE ciVEH_WEP_DETONATE
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Deton_G"))
			CASE ciVEH_WEP_BOMB
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Bomb_G"))
			BREAK
			CASE ciVEH_WEP_BOUNCE
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Hop_G"))
			CASE ciVEH_WEP_REPAIR
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Repair_G"))
			CASE ciVEH_WEP_RANDOM
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_RANDOM_G"))
			BREAK
			CASE ciVEH_WEP_MACHINE_GUN
				RETURN VEHICLE_GUN_PICKUP_MODEL(iTeam)
		
			CASE ciVEH_WEP_RANDOM_SPECIAL_VEH
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Special_Vehicle_G"))
			BREAK
			CASE ciVEH_WEP_RUINER_SPECIAL_VEH
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Special_Ruiner_G"))
			BREAK
			CASE ciVEH_WEP_RAMP_SPECIAL_VEH
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Special_Buggy_G"))
			BREAK
			CASE ciVEH_WEP_BOMB_LENGTH
				IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWeaponIndex].iVehBombLengthIncrease < 0
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_blastdec_green"))
				ENDIF
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_blastinc_green"))
			BREAK	
			CASE ciVEH_WEP_BOMB_MAX
				IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWeaponIndex].iVehBombMaxIncrease < 0
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_bombdec_green"))
				ENDIF
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_bombinc_green"))
			BREAK
			CASE ciVEH_WEP_EXTRA_LIFE
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_lifeinc_green"))
			DEFAULT
				RETURN DUMMY_MODEL_FOR_SCRIPT
		ENDSWITCH
	BREAK
	DEFAULT
		RETURN DUMMY_MODEL_FOR_SCRIPT
	ENDSWITCH

	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

// ##### ------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Train Assets ---------------------------------------------------------------------------------------------
// ##### Description: Specific train configs etc that are mission-specific		  -----------------------------------------------
// ##### ------------------------------------------------------------------------------------------------------------------------
// ##### ------------------------------------------------------------------------------------------------------------------------
PROC APPLY_TRAIN_MISSION_SPECIFIC_CONFIG(VEHICLE_INDEX viTrain, INT iTrain)
	UNUSED_PARAMETER(iTrain)
	
	IF IS_CURRENT_MISSION_THIS_TUNER_ROBBERY_FINALE(TR_FREIGHT_TRAIN)
		PRINTLN("[Trains][Train ", iTrain, "] APPLY_TRAIN_MISSION_SPECIFIC_CONFIG | Applying Tuner Train custom config")
		
		VEHICLE_SETUP_STRUCT_MP sData
		sData.VehicleSetup.eModel = FREIGHT // FREIGHT
		sData.VehicleSetup.tlPlateText = "21RQL186"
		sData.VehicleSetup.iColour1 = 0
		sData.VehicleSetup.iColour2 = 43
		sData.VehicleSetup.iColourExtra1 = 111
		sData.VehicleSetup.iColourExtra2 = 0
		sData.iColour5 = 1
		sData.iColour6 = 132
		sData.iLivery2 = 0
		sData.VehicleSetup.iTyreR = 255
		sData.VehicleSetup.iTyreG = 255
		sData.VehicleSetup.iTyreB = 255
		sData.VehicleSetup.iNeonR = 255
		sData.VehicleSetup.iNeonB = 255
		
		SET_VEHICLE_SETUP_MP(viTrain, sData, FALSE, TRUE)
	ENDIF
ENDPROC

// ##### ------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Interactable Assets --------------------------------------------------------------------------------------
// ##### Description: Extra Anims, models or audio etc. for various Interactions  -----------------------------------------------
// ##### ------------------------------------------------------------------------------------------------------------------------
// ##### ------------------------------------------------------------------------------------------------------------------------

FUNC BOOL MC_LOAD_INTERACTABLE_EXTRA_ASSETS(INT iInteractable)

	IF g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iInteractable_InteractionType = ciInteractableInteraction_InteractWith
	AND g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iInteractable_InteractWithAnimPreset = ciINTERACT_WITH_PRESET__PICK_UP_GUN
		IF NOT REQUEST_SCRIPT_AUDIO_BANK("DLC_HEI4/DLC_HEI4_Collectibles")
			PRINTLN("[Interactables][Interactable ", iInteractable, "] MC_LOAD_INTERACTABLE_EXTRA_ASSETS | Loading DLC_HEI4/DLC_HEI4_Collectibles")
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC MC_UNLOAD_INTERACTABLE_EXTRA_ASSETS(INT iInteractable)

	SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iInteractable_InteractionType
		CASE ciInteractableInteraction_InteractWith
			IF g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iInteractable_InteractWithAnimPreset = ciINTERACT_WITH_PRESET__PICK_UP_GUN
				RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_HEI4/DLC_HEI4_Collectibles")
				PRINTLN("[Interactables][Interactable ", iInteractable, "] MC_UNLOAD_INTERACTABLE_EXTRA_ASSETS | ciInteractableInteraction_InteractWith | Releasing DLC_HEI4/DLC_HEI4_Collectibles")
			ENDIF
		BREAK
		
		CASE ciInteractableInteraction_MultiSolutionLock
			PRINTLN("[Interactable][Interactable ", iInteractable, "] CLEANUP_INTERACTABLE_INTERACTIONS | ciInteractableInteraction_MultiSolutionLock | Removing asset for scr_ih_fin")
			REMOVE_NAMED_PTFX_ASSET("scr_ih_fin")
		BREAK
		
		CASE ciInteractableInteraction_PlantThermalCharge
			PRINTLN("[Interactable][Interactable ", iInteractable, "] CLEANUP_INTERACTABLE_INTERACTIONS | ciInteractableInteraction_PlantThermalCharge | Removing asset for scr_ih_fin")
			REMOVE_NAMED_PTFX_ASSET("scr_ch_finale")
			
			PRINTLN("[Interactable][Interactable ", iInteractable, "] CLEANUP_INTERACTABLE_INTERACTIONS | ciInteractableInteraction_PlantThermalCharge | Removing asset for scr_ornate_heist")
			REMOVE_NAMED_PTFX_ASSET("scr_ornate_heist")
			
			PRINTLN("[Interactable][Interactable ", iInteractable, "] CLEANUP_INTERACTABLE_INTERACTIONS | ciInteractableInteraction_PlantThermalCharge | Removing asset for pat_heist")
			REMOVE_NAMED_PTFX_ASSET("pat_heist")
		BREAK
	ENDSWITCH
	
ENDPROC

// ##### ------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Interact-With Assets ------------------------------------------------------------------------------------
// ##### Description: Anims, models etc. for Interact-With minigames  -------------------------------------------------------------
// ##### ------------------------------------------------------------------------------------------------------------------------
// ##### ------------------------------------------------------------------------------------------------------------------------

FUNC STRING GET_INTERACT_WITH_PLANT_VAULT_DOOR_EXPLOSIVE_ANIM_NAME(INTERACT_WITH_PLANT_CASINO_DOOR_EXPLOSIVE_STATE eState, BOOL bGetPlayerAnim = FALSE, BOOL bGetBombAnim = FALSE, INT iBombIndex = -1)
	
	INT iProgress = -1

	iProgress = sInteractWithVars.iInteractWith_ObjectsPlacedLocalCounter
	
	SWITCH eState
		CASE INTERACT_WITH_PLANT_CASINO_DOOR_EXPLOSIVE_STATE__INIT
			IF bGetPlayerAnim
				RETURN "PLAYER_IG8_VAULT_EXPLOSIVE_ENTER"
			ELIF bGetBombAnim
				SWITCH iBombIndex
					CASE 0		RETURN "SEMTEX_A_IG8_VAULT_EXPLOSIVE_ENTER"
					CASE 1		RETURN "SEMTEX_B_IG8_VAULT_EXPLOSIVE_ENTER"
					CASE 2		RETURN "SEMTEX_C_IG8_VAULT_EXPLOSIVE_ENTER"
				ENDSWITCH
			ENDIF
		BREAK
		CASE INTERACT_WITH_PLANT_CASINO_DOOR_EXPLOSIVE_STATE__IDLE
			IF bGetPlayerAnim
				RETURN "PLAYER_IG8_VAULT_EXPLOSIVE_IDLE"
			ELIF bGetBombAnim
				SWITCH iBombIndex
					CASE 0		RETURN "SEMTEX_A_IG8_VAULT_EXPLOSIVE_IDLE"
					CASE 1		RETURN "SEMTEX_A_IG8_VAULT_EXPLOSIVE_IDLE"
					CASE 2		RETURN "SEMTEX_A_IG8_VAULT_EXPLOSIVE_IDLE"
				ENDSWITCH
			ENDIF
		BREAK
		CASE INTERACT_WITH_PLANT_CASINO_DOOR_EXPLOSIVE_STATE__PLANTING
			IF bGetPlayerAnim
				SWITCH iProgress
					CASE 0		RETURN "PLAYER_IG8_VAULT_EXPLOSIVE_PLANT_A"
					CASE 1		RETURN "PLAYER_IG8_VAULT_EXPLOSIVE_PLANT_B"
					CASE 2		RETURN "PLAYER_IG8_VAULT_EXPLOSIVE_PLANT_C"
				ENDSWITCH
				
			ELIF bGetBombAnim
				SWITCH iProgress
					CASE 0
						SWITCH iBombIndex
							CASE 0		RETURN "SEMTEX_A_IG8_VAULT_EXPLOSIVE_PLANT_A"
							CASE 1		RETURN "SEMTEX_B_IG8_VAULT_EXPLOSIVE_PLANT_A"
							CASE 2		RETURN "SEMTEX_C_IG8_VAULT_EXPLOSIVE_PLANT_A"
						ENDSWITCH
					BREAK
					
					CASE 1
						SWITCH iBombIndex
							CASE 0		RETURN "SEMTEX_A_IG8_VAULT_EXPLOSIVE_PLANT_B"
							CASE 1		RETURN "SEMTEX_B_IG8_VAULT_EXPLOSIVE_PLANT_B"
							CASE 2		RETURN "SEMTEX_C_IG8_VAULT_EXPLOSIVE_PLANT_B"
						ENDSWITCH
					BREAK
					
					CASE 2
						SWITCH iBombIndex
							CASE 0		RETURN "SEMTEX_A_IG8_VAULT_EXPLOSIVE_PLANT_C"
							CASE 1		RETURN "SEMTEX_B_IG8_VAULT_EXPLOSIVE_PLANT_C"
							CASE 2		RETURN "SEMTEX_C_IG8_VAULT_EXPLOSIVE_PLANT_C"
						ENDSWITCH
					BREAK
				ENDSWITCH
			ENDIF
			
		BREAK
		CASE INTERACT_WITH_PLANT_CASINO_DOOR_EXPLOSIVE_STATE__EXITING
			IF bGetPlayerAnim
				RETURN "PLAYER_IG8_VAULT_EXPLOSIVE_ENTER"
				
			ELIF bGetBombAnim
				RETURN "SEMTEX_A_IG8_VAULT_EXPLOSIVE_IDLE"
				
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN "INVALID"
ENDFUNC

FUNC STRING GET_INTERACT_WITH_PLAYER_FAILSAFE_ANIM_DICT(INTERACT_WITH_PARAMS& sInteractWithParams)
	SWITCH sInteractWithParams.iInteractWith_AnimPreset
		CASE ciINTERACT_WITH_PRESET__PUSH_BUTTON			RETURN "anim@mp_radio@high_life_apment"
		CASE ciINTERACT_WITH_PRESET__RUMMAGE_AROUND			RETURN "ANIM@HEISTS@PRISON_HEISTSTATION@"
		CASE ciINTERACT_WITH_PRESET__BOMB_PLANT				RETURN  "ANIM@MP_FIREWORKS"
		CASE ciINTERACT_WITH_PRESET__SEARCH_BODY_ON_FLOOR	RETURN  "ANIM@MP_FIREWORKS"
	ENDSWITCH
	
	RETURN "ANIM@HEISTS@PRISON_HEISTSTATION@"
ENDFUNC

FUNC STRING GET_INTERACT_WITH_PLAYER_FAILSAFE_ANIM_NAME(INTERACT_WITH_PARAMS& sInteractWithParams)
	SWITCH sInteractWithParams.iInteractWith_AnimPreset
		CASE ciINTERACT_WITH_PRESET__PUSH_BUTTON			RETURN "action_a_bedroom"
		CASE ciINTERACT_WITH_PRESET__RUMMAGE_AROUND			RETURN "pickup_bus_schedule"
		CASE ciINTERACT_WITH_PRESET__BOMB_PLANT				RETURN "PLACE_FIREWORK_3_BOX"
		CASE ciINTERACT_WITH_PRESET__SEARCH_BODY_ON_FLOOR	RETURN "PLACE_FIREWORK_3_BOX"
	ENDSWITCH
	
	RETURN "pickup_bus_schedule"
ENDFUNC

FUNC STRING GET_INTERACT_WITH_CHANGE_CLOTHES_ANIMATION(INTERACT_WITH_PARAMS& sInteractWithParams)
	SWITCH sInteractWithParams.iClothesChangeOutfit
		CASE ciChangeClothesNOOSE
			IF IS_PED_FEMALE(LocalPlayerPed)
				RETURN "CHANGE_NOOSE_FEMALE"
			ELSE
				RETURN "CHANGE_NOOSE_MALE"
			ENDIF
		BREAK
		CASE ciChangeClothesFireman
			IF IS_PED_FEMALE(LocalPlayerPed)
				RETURN "CHANGE_FIRE_FEMALE"
			ELSE
				RETURN "CHANGE_FIRE_MALE"
			ENDIF
		BREAK
		CASE ciChangeClothesHighroller
		CASE ciChangeClothesIslandGuard
		CASE ciChangeClothesMaintenance
			IF IS_PED_FEMALE(LocalPlayerPed)
				RETURN "CHANGE_HIGHROLLER_FEMALE"
			ELSE
				RETURN "CHANGE_HIGHROLLER_MALE"
			ENDIF
		BREAK
	ENDSWITCH
	
	IF IS_PED_FEMALE(LocalPlayerPed)
		RETURN "CHANGE_HIGHROLLER_FEMALE"
	ELSE
		RETURN "CHANGE_HIGHROLLER_MALE"
	ENDIF
ENDFUNC

FUNC STRING GET_INTERACT_WITH_AUDIO_BANK(INTERACT_WITH_PARAMS& sInteractWithParams)
	SWITCH sInteractWithParams.iInteractWith_AnimPreset
		CASE ciINTERACT_WITH_PRESET__USE_KEYCARD			RETURN "DLC_MPHEIST/HEIST_USE_KEYPAD"
		CASE ciINTERACT_WITH_PRESET__CUT_OUT_PAINTING		RETURN "DLC_HEIST3/HEIST_FINALE_STEAL_PAINTINGS"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC BOOL DOES_INTERACT_WITH_PRESET_ATTACH_SCENE_TO_ENTITY(INT iPreset)
	IF iPreset = ciINTERACT_WITH_PRESET__BEAMHACK_IN_VEHICLE
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

// ONLY ADD TO THIS IF YOUR ANIMATION NEEDS TO ALIGN TO AN OBJECT OTHER THAN THE MINIGAME OBJECT ITSELF
FUNC MODEL_NAMES GET_MODEL_FOR_INTERACT_WITH_SCENE_ALIGNMENT_ENTITY(INTERACT_WITH_PARAMS& sInteractWithParams)

	SWITCH sInteractWithParams.iInteractWith_AnimPreset
		CASE ciINTERACT_WITH_PRESET__BOMB_PLANT	
			SWITCH sInteractWithParams.iInteractWith_AltAnimSet
				CASE ciIW_ALTANIMSET__PLANTBOMB_IN_FLOOR
					RETURN DUMMY_MODEL_FOR_SCRIPT
				BREAK
			ENDSWITCH
		BREAK
		CASE ciINTERACT_WITH_PRESET__SEARCH_BODY_ON_TABLE					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("v_med_cor_emblmtable"))
		CASE ciINTERACT_WITH_PRESET__CRATE_IN_VAN							RETURN SPEEDO
		CASE ciINTERACT_WITH_PRESET__SEARCH_BODY_ON_FLOOR					RETURN DUMMY_MODEL_FOR_SCRIPT
		CASE ciINTERACT_WITH_PRESET__PLANT_RADIO_OVERRIDE					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("vw_prop_vw_elecbox_01a")) // Radio box
		CASE ciINTERACT_WITH_PRESET__PLANT_VAULT_EXPLOSIVE__LEFT			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_vaultdoor01x")) // Casino Vault Door
		CASE ciINTERACT_WITH_PRESET__PLANT_VAULT_EXPLOSIVE__RIGHT			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_vaultdoor01x")) // Casino Vault Door
		//CASE ciINTERACT_WITH_PRESET__CUT_OUT_PAINTING						RETURN INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_CH_Sec_Cabinet_02a")) // Casino Vault Painting Security Cabinet
		CASE ciINTERACT_WITH_PRESET__PLANT_CASINO_TUNNEL_EXPLOSIVE			RETURN DUMMY_MODEL_FOR_SCRIPT 	 // Tunnel debris
		CASE ciINTERACT_WITH_PRESET__USE_KEY_ON_DOOR						
			SWITCH sInteractWithParams.iInteractWith_AltAnimSet
				CASE ciIW_ALTANIMSET__USEKEYONDOOR_ISLAND_GATE				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("h4_Prop_h4_Gate_02a")) // The gate
				//CASE ciIW_ALTANIMSET__USEKEYONDOOR_GENERIC 				// The invisible Interactable should be placed on the lock and used as the scene origin here
			ENDSWITCH
		BREAK
		CASE ciINTERACT_WITH_PRESET__PICK_UP_GUN
			SWITCH sInteractWithParams.iInteractWith_AltAnimSet
				CASE ciIW_ALTANIMSET__PICKUPGUN_SHOTGUN
					// The Interact-With object is the shotgun itself which is the alignment entity
				BREAK
				
				CASE ciIW_ALTANIMSET__PICKUPGUN_GOLDEN_GUN 						RETURN INT_TO_ENUM(MODEL_NAMES, HASH("h4_prop_office_desk_01")) // The desk
			ENDSWITCH
		BREAK
		
		CASE ciINTERACT_WITH_PRESET__OPEN_VEHICLE_TRUNK	
			IF DOES_ENTITY_EXIST(GET_INTERACT_WITH_LINKED_VEHICLE_FROM_PARAMS(sInteractWithParams))
				RETURN GET_ENTITY_MODEL(GET_INTERACT_WITH_LINKED_VEHICLE_FROM_PARAMS(sInteractWithParams))
			ELSE
				ASSERTLN("[InteractWith] GET_MODEL_FOR_INTERACT_WITH_SCENE_ALIGNMENT_ENTITY | GET_INTERACT_WITH_LINKED_VEHICLE_FROM_PARAMS doesn't exist!")
				RETURN DUMMY_MODEL_FOR_SCRIPT
			ENDIF
		BREAK
		
		CASE ciINTERACT_WITH_PRESET__PASS_GUARD								RETURN DUMMY_MODEL_FOR_SCRIPT // This one uses a nearby ped
		
		CASE ciINTERACT_WITH_PRESET__BEAMHACK_IN_VEHICLE					RETURN DUMMY_MODEL_FOR_SCRIPT // This one uses a linked vehicle
		
		CASE ciINTERACT_WITH_PRESET__DEAL_WITH_CHAIN_LOCK
			SWITCH sInteractWithParams.iInteractWith_AltAnimSet
				CASE ciIW_ALTANIMSET__DEALWITHCHAIN_CUTTER_AND_CONTAINER
					RETURN DUMMY_MODEL_FOR_SCRIPT // Attach the scene to the linked dynoprop
				BREAK
			ENDSWITCH
		BREAK
		
		CASE ciINTERACT_WITH_PRESET__INTERROGATION
			SWITCH sInteractWithParams.iInteractWith_AltAnimSet
				CASE ciIW_ALTANIMSET__INTERROGATION_GOLFCLUB
					RETURN DUMMY_MODEL_FOR_SCRIPT // Attach the scene to the linked ped
				BREAK
			ENDSWITCH
		BREAK
		
		CASE ciINTERACT_WITH_PRESET__PICK_UP_INJURED_PED			RETURN DUMMY_MODEL_FOR_SCRIPT
		
		CASE ciINTERACT_WITH_PRESET__STEAL_LAPTOP_FROM_DJ_DESK      RETURN INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_DJ_Desk_02a"))		
		
		CASE ciINTERACT_WITH_PRESET__CUTSCENE_LEAD_OUT			RETURN DUMMY_MODEL_FOR_SCRIPT
		CASE ciINTERACT_WITH_PRESET__INSERT_FUSE				RETURN DUMMY_MODEL_FOR_SCRIPT
	ENDSWITCH
	
	IF DOES_ENTITY_EXIST(sInteractWithParams.oiInteractWithEntity)
		RETURN GET_ENTITY_MODEL(sInteractWithParams.oiInteractWithEntity)
	ELSE
		RETURN DUMMY_MODEL_FOR_SCRIPT
	ENDIF
ENDFUNC

FUNC MODEL_NAMES GET_KEYCARD_MODEL_FOR_CASINO_FINGERPRINT_SCANNER(INTERACT_WITH_PARAMS& sInteractWithParams)
	SWITCH sInteractWithParams.iKeypadAccessRequired
		CASE INTERACT_WITH_KEYPAD_CLEARANCE_REQUIRED__INSIDE_MAN_LVL1		RETURN INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Swipe_Card_01a"))
		CASE INTERACT_WITH_KEYPAD_CLEARANCE_REQUIRED__INSIDE_MAN_LVL2		RETURN INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Swipe_Card_01b"))
		CASE INTERACT_WITH_KEYPAD_CLEARANCE_REQUIRED__INSIDE_MAN_LVL3		RETURN INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Swipe_Card_01c"))
		CASE INTERACT_WITH_KEYPAD_CLEARANCE_REQUIRED__INSIDE_MAN_LVL4		RETURN INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Swipe_Card_01d"))
	ENDSWITCH
	
	RETURN INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Swipe_Card_01a"))
ENDFUNC

FUNC MODEL_NAMES GET_INTERACT_WITH_KEYCARD_PROP_MODEL(INTERACT_WITH_PARAMS& sInteractWithParams, MODEL_NAMES mnDefaultModel = DUMMY_MODEL_FOR_SCRIPT)
	SWITCH sInteractWithParams.iKeycardProp
		CASE INTERACT_WITH_KEYCARD_PROP__SWIPE_CARD				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("tr_Prop_TR_Swipe_Card_01a"))
		CASE INTERACT_WITH_KEYCARD_PROP__PASS_CARD				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("tr_Prop_TR_Acc_Pass_01a"))
		CASE INTERACT_WITH_KEYCARD_PROP__SECURITY_KEYCARD		RETURN INT_TO_ENUM(MODEL_NAMES, HASH("h4_Prop_h4_SecurityCard_01a"))
		CASE INTERACT_WITH_KEYCARD_PROP__FRIEDLANDER_KEYCARD	RETURN xm3_prop_xm3_swipe_card_01a
	ENDSWITCH
	
	RETURN mnDefaultModel
ENDFUNC

FUNC VECTOR GET_ATTACH_POSITION_OFFSET_FOR_KEYCARD_PROP(INTERACT_WITH_PARAMS &sInteractWithParams)
	SWITCH sInteractWithParams.iKeycardProp
		CASE INTERACT_WITH_KEYCARD_PROP__SWIPE_CARD				RETURN <<0.165, 0.03, 0.035>>
		CASE INTERACT_WITH_KEYCARD_PROP__PASS_CARD				RETURN <<0.165, 0.03, 0.035>>
		CASE INTERACT_WITH_KEYCARD_PROP__SECURITY_KEYCARD		RETURN <<0.165, 0.03, 0.035>>
		CASE INTERACT_WITH_KEYCARD_PROP__FRIEDLANDER_KEYCARD 	RETURN <<0.165, 0.035, 0.035>>
	ENDSWITCH
	
	RETURN ZERO_VECTOR()
ENDFUNC

FUNC VECTOR GET_ATTACH_ROTATION_OFFSET_FOR_KEYCARD_PROP(INTERACT_WITH_PARAMS &sInteractWithParams)
	SWITCH sInteractWithParams.iKeycardProp
		CASE INTERACT_WITH_KEYCARD_PROP__SWIPE_CARD				RETURN <<-98.835, 182.6, 183.0>>
		CASE INTERACT_WITH_KEYCARD_PROP__PASS_CARD				RETURN <<-188.835, 262.6, 273.0>>
		CASE INTERACT_WITH_KEYCARD_PROP__SECURITY_KEYCARD		RETURN <<-188.835, 262.6, 273.0>>
		CASE INTERACT_WITH_KEYCARD_PROP__FRIEDLANDER_KEYCARD	RETURN <<-168.835, 354.6, 003.0>>
	ENDSWITCH
	
	RETURN ZERO_VECTOR()
ENDFUNC

FUNC BOOL IS_KEYCARD_PROP_VALID(INTERACT_WITH_PARAMS& sInteractWithParams)
	IF sInteractWithParams.iKeycardProp > INTERACT_WITH_KEYCARD_PROP__NONE
	AND sInteractWithParams.iKeycardProp < INTERACT_WITH_KEYCARD_PROP__MAX
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ANGLE_GRINDER_PROP_VALID(INTERACT_WITH_PARAMS& sInteractWithParams)
	IF sInteractWithParams.iAngleGrinderProp > INTERACT_WITH_ANGLE_GRINDER_PROP__NONE
	AND sInteractWithParams.iAngleGrinderProp < INTERACT_WITH_ANGLE_GRINDER_PROP__MAX
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC MODEL_NAMES GET_INTERACT_WITH_ANGLE_GRINDER_PROP_MODEL(INTERACT_WITH_PARAMS &sInteractWithParams, MODEL_NAMES mnDefaultModel = DUMMY_MODEL_FOR_SCRIPT)
	SWITCH sInteractWithParams.iAngleGrinderProp
		CASE INTERACT_WITH_ANGLE_GRINDER_PROP__YELLOW	RETURN tr_prop_tr_grinder_01a
		CASE INTERACT_WITH_ANGLE_GRINDER_PROP__GREY		RETURN xm3_prop_xm3_grinder_02a
	ENDSWITCH
	
	RETURN mnDefaultModel
ENDFUNC

FUNC MODEL_NAMES GET_MODEL_NAME_FOR_CURRENT_INTERACT_WITH_SPAWNED_PROP(INTERACT_WITH_PARAMS& sInteractWithParams, INT iSpawnedProp)
	UNUSED_PARAMETER(sInteractWithParams)
	RETURN sInteractWithVars.sIWAnims.sSpawnedPropAnims[iSpawnedProp].mnIW_ModelName
ENDFUNC

FUNC INT GET_CURRENT_INTERACT_WITH_NUM_PROPS_TO_SPAWN(INTERACT_WITH_PARAMS& sInteractWithParams)
	UNUSED_PARAMETER(sInteractWithParams)
	RETURN sInteractWithVars.sIWAnims.iNumPropsToSpawn
ENDFUNC

FUNC INT GET_CURRENT_INTERACT_WITH_NUM_LINKED_OBJECTS(INTERACT_WITH_PARAMS& sInteractWithParams)
	UNUSED_PARAMETER(sInteractWithParams)
	RETURN sInteractWithVars.sIWAnims.iNumLinkedObjectsToUse
ENDFUNC

FUNC BOOL SHOULD_CURRENT_INTERACT_WITH_SPAWNED_PROP_BE_PERSISTENT(INT iSpawnedProp)
	RETURN sInteractWithVars.sIWAnims.sSpawnedPropAnims[iSpawnedProp].bPersistent
ENDFUNC

FUNC BOOL SHOULD_INTERACT_WITH_OBJECT_HIDE_AFTER_ANIMATION(INTERACT_WITH_PARAMS& sInteractWithParams)
	SWITCH sInteractWithParams.iInteractWith_AnimPreset
		CASE ciINTERACT_WITH_PRESET__PICK_UP_GUN
		CASE ciINTERACT_WITH_PRESET__PICK_UP_HDD
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_INTERACT_WITH_OBJECT_DISABLE_COLLISION_DURING_ANIMATION(INTERACT_WITH_PARAMS& sInteractWithParams)
	SWITCH sInteractWithParams.iInteractWith_AnimPreset
		CASE ciINTERACT_WITH_PRESET__PICK_UP_GUN
		CASE ciINTERACT_WITH_PRESET__PICK_UP_HDD
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_INTERACT_WITH_OBJECT_DISABLE_OBJECTIVE_TEXT_DURING_ANIMATION(INTERACT_WITH_PARAMS& sInteractWithParams)
	SWITCH sInteractWithParams.iInteractWith_AnimPreset
		CASE ciINTERACT_WITH_PRESET__INTERROGATION
			IF sInteractWithVars.iInteractWith_SubAnimPreset = ciIW_SUBANIM__INTERROGATION_STAGE_APOLOGY_A
			OR sInteractWithVars.iInteractWith_SubAnimPreset = ciIW_SUBANIM__INTERROGATION_STAGE_APOLOGY_B
				RETURN TRUE
			ENDIF
			
			IF sInteractWithVars.iInteractWith_SubAnimPreset = ciIW_SUBANIM__INTERROGATION_STAGE_3_HIT_4
			AND sInteractWithVars.eInteractWith_CurrentAnimStage = IW_ANIM_STAGE__LOOPING
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_INTERACT_WITH_HIDE_WEAPON_DURING_APPROACH(INTERACT_WITH_PARAMS& sInteractWithParams)
	SWITCH sInteractWithParams.iInteractWith_AnimPreset
		CASE ciINTERACT_WITH_PRESET__INTERROGATION
			RETURN FALSE
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_INTERACT_WITH_MAKE_SCREEN_FADE_IN(INTERACT_WITH_PARAMS& sInteractWithParams)
	SWITCH sInteractWithParams.iInteractWith_AnimPreset
		CASE ciINTERACT_WITH_PRESET__CUTSCENE_LEAD_OUT
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_INTERACT_WITH_END_WITH_INPUT_GAIT(INTERACT_WITH_PARAMS& sInteractWithParams)
	SWITCH sInteractWithParams.iInteractWith_AnimPreset
		CASE ciINTERACT_WITH_PRESET__CUTSCENE_LEAD_OUT
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_INTERACT_WITH_PRESET_PROMPT_BE_BLOCKED_IF_POSITION_IS_OCCUPIED(INT iInteractWithPreset)
	SWITCH iInteractWithPreset
		CASE ciINTERACT_WITH_PRESET__DIG_SAND_PILE
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_INTERACT_WITH_PRESET_BE_SKIPPED_IF_POSITION_IS_OCCUPIED(INT iInteractWithPreset)
	SWITCH iInteractWithPreset
		CASE ciINTERACT_WITH_PRESET__SEARCH_BODY_ON_FLOOR
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_INTERACT_WITH_OBJECT_ANIMATED()
	IF NOT IS_STRING_NULL_OR_EMPTY(sInteractWithVars.sIWAnims.sInteractWithEntityAnims.sIW_Enter)
	OR NOT IS_STRING_NULL_OR_EMPTY(sInteractWithVars.sIWAnims.sInteractWithEntityAnims.sIW_Looping)
	OR NOT IS_STRING_NULL_OR_EMPTY(sInteractWithVars.sIWAnims.sInteractWithEntityAnims.sIW_Exit)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_INTERACT_WITH_SYNC_SCENE_BE_ALIGNED_BASED_ON_PLAYER_POSITION(INTERACT_WITH_PARAMS& sInteractWithParams, BOOL bForOrientation = FALSE)
	SWITCH sInteractWithParams.iInteractWith_AnimPreset
		CASE ciINTERACT_WITH_PRESET__CLOTHES
		CASE ciINTERACT_WITH_PRESET__SEARCH_BODY_ON_FLOOR
		CASE ciINTERACT_WITH_PRESET__DIG_SAND_PILE
			RETURN TRUE
		BREAK
		
		CASE ciINTERACT_WITH_PRESET__GRAB_FUSE
			RETURN bForOrientation
		BREAK
		
		CASE ciINTERACT_WITH_PRESET__INTERROGATION
			IF sInteractWithParams.iInteractWith_AltAnimSet = ciIW_ALTANIMSET__INTERROGATION_GOLFCLUB
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_INTERACT_WITH_SYNC_SCENE_BE_ALIGNMENT_INCLUDE_OFFSET_FOR_ROTATION(INTERACT_WITH_PARAMS& sInteractWithParams)
	SWITCH sInteractWithParams.iInteractWith_AnimPreset
		CASE ciINTERACT_WITH_PRESET__GRAB_FUSE
			RETURN FALSE
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_IW_CAMERA_USE_TARGET_POS_RATHER_THAN_SYNC_SCENE_POS(INTERACT_WITH_PARAMS& sInteractWithParams)
	SWITCH sInteractWithParams.iInteractWith_AnimPreset
		CASE ciINTERACT_WITH_PRESET__CUT_OUT_PAINTING
		CASE ciINTERACT_WITH_PRESET__CLOTHES
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC VECTOR GET_POS_FOR_IW_CAMERA(INTERACT_WITH_PARAMS& sInteractWithParams, BOOL bSyncScene, BOOL bUsePlayerStartPos = FALSE)
	VECTOR vForward, vRight, vUp, vPos
	
	IF bSyncScene
		IF SHOULD_IW_CAMERA_USE_TARGET_POS_RATHER_THAN_SYNC_SCENE_POS(sInteractWithParams)
			vPos = sInteractWithVars.vInteractWith_CachedInitialPlayerStartPos
		ELSE
			vPos = sInteractWithVars.vInteractWith_CachedSyncScenePos
		ENDIF
		
		vUp = <<0, 0, 1>>
		vForward = ROTATE_VECTOR_ABOUT_Z(<<0, 1, 0>>, sInteractWithVars.vInteractWith_CachedInitialPlayerStartRot.z)
		vRight = CROSS_PRODUCT(vForward, vUp)
	ELSE
		IF bUsePlayerStartPos
			PRINTLN("[InteractWith_Camera] GET_POS_FOR_IW_CAMERA | bUsePlayerStartPos = TRUE, so setting vPos = sInteractWithVars.vInteractWith_PlayerStartPos")
			vPos = sInteractWithVars.vInteractWith_PlayerStartPos
		ELSE
			GET_ENTITY_MATRIX(LocalPlayerPed, vForward, vRight, vUp, vPos)
		ENDIF
	ENDIF
	
	VECTOR vLocalOffset = sInteractWithVars.sIWAnims.sCameraVars.vOffsetCoords
	VECTOR vResult
	
	#IF IS_DEBUG_BUILD
	IF IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD5)
		DRAW_DEBUG_LINE(vPos, vPos + vForward, 0, 0, 0)
		DRAW_DEBUG_LINE(vPos, vPos + vRight, 255, 0, 0)
		DRAW_DEBUG_LINE(vPos, vPos + vUp, 0, 255, 0)
	ENDIF
	#ENDIF
	
	vResult = vPos
	vResult += vRight * vLocalOffset.x
	vResult += vForward * vLocalOffset.y
	vResult += vUp * vLocalOffset.z
	
	PRINTLN("[InteractWith_Camera] GET_POS_FOR_IW_CAMERA || vLocalOffset = ", vLocalOffset, " | Result: ", vResult)
	RETURN vResult
ENDFUNC

FUNC VECTOR GET_ROT_FOR_IW_CAMERA(INTERACT_WITH_PARAMS& sInteractWithParams, BOOL bSyncScene)
	
	IF bSyncScene
		IF SHOULD_IW_CAMERA_USE_TARGET_POS_RATHER_THAN_SYNC_SCENE_POS(sInteractWithParams)
			RETURN sInteractWithVars.vInteractWith_PlayerStartRot
		ELSE
			RETURN sInteractWithVars.vInteractWith_CachedSyncSceneRot
		ENDIF
	ENDIF
	
	VECTOR vReturn = sInteractWithVars.sIWAnims.sCameraVars.vRot
	FLOAT fHeadingToUse = GET_ENTITY_HEADING(localPlayerPed)
	
	vReturn.z += fHeadingToUse
	RETURN vReturn
ENDFUNC

PROC SET_CHANGE_CLOTHES_CAMERA_VARS(INTERACT_WITH_PARAMS& sInteractWithParams, INTERACT_WITH_ANIM_ASSETS& sIWAnims)
	
	sIWAnims.sCameraVars.fFOV = 44.6382
	sIWAnims.sCameraVars.fPhaseEnd = 0.95
	sIWAnims.sCameraVars.iLerpIn = 0
	sIWAnims.sCameraVars.iLerpOut = 0
	sIWAnims.sCameraVars.bIsAnimated = TRUE
	
	sIWAnims.sCameraVars.vOffsetCoords = <<0.0, 0.0, -1.0>>
	sIWAnims.sCameraVars.vRot = <<0.0, 0.0, 0.0>>
	
	SWITCH sInteractWithParams.iClothesChangeOutfit
		CASE ciChangeClothesNOOSE
			sIWAnims.sCameraAnims.sIW_Enter = "CHANGE_NOOSE_CAM"
		BREAK
		CASE ciChangeClothesFireman
			sIWAnims.sCameraAnims.sIW_Enter = "CHANGE_FIRE_CAM"
		BREAK
		CASE ciChangeClothesHighroller
		CASE ciChangeClothesIslandGuard
		CASE ciChangeClothesMaintenance
			sIWAnims.sCameraAnims.sIW_Enter = "CHANGE_HIGHROLLER_CAM"
		BREAK
	ENDSWITCH
ENDPROC

FUNC STRING GET_CUT_PAINTING_CAMERA_ENTER_SUFFIX()

	FLOAT fCam_Heading = GET_GAMEPLAY_CAM_RELATIVE_HEADING()
	FLOAT fCam_Pitch = GET_GAMEPLAY_CAM_RELATIVE_PITCH()
	PRINTLN("GET_CAM_RELATIVE_POSITION_TO_PAINTING | Cam heading = ", fCam_Heading)
	PRINTLN("GET_CAM_RELATIVE_POSITION_TO_PAINTING | Cam pitch = ", fCam_Pitch)
	
	IF fCam_Heading < -25.0
	AND fCam_Pitch < -25.0
		PRINTLN("GET_CAM_RELATIVE_POSITION_TO_PAINTING | Cam is above left of target")
		RETURN "_TLE"
	ELIF fCam_Heading > 25.0
	AND fCam_Pitch < -25.0
		PRINTLN("GET_CAM_RELATIVE_POSITION_TO_PAINTING | Cam is above right of target")
		RETURN "_TRE"
	ELIF fCam_Heading > 25.0
	AND fCam_Pitch > 25.0
		PRINTLN("GET_CAM_RELATIVE_POSITION_TO_PAINTING | Cam is below right of target")
		RETURN "_BRE"	
	ELIF fCam_Heading < -25.0
	AND fCam_Pitch > 25.0
		PRINTLN("GET_CAM_RELATIVE_POSITION_TO_PAINTING | Cam is below left of target")
		RETURN "_BLE"
	ELSE 
		PRINTLN("GET_CAM_RELATIVE_POSITION_TO_PAINTING | Cam is tight behind target")
		RETURN "_RE"
	ENDIF	
ENDFUNC

PROC GET_ANIM_ASSET_NAMES_FOR_CUT_PAINTING_INTERACT_WITH(INTERACT_WITH_PARAMS& sInteractWithParams, INTERACT_WITH_ANIM_ASSETS& sIWAnims, INT iSubAnimPresetToUse)
	UNUSED_PARAMETER(sInteractWithParams)
	
	sIWAnims.sIW_AnimDict = "ANIM_HEIST@HS3F@IG11_STEAL_PAINTING@MALE@"
	
	sIWAnims.sInteractWithEntityAnims.bPersistent = TRUE
	
	sIWAnims.iNumPropsToSpawn++
	sIWAnims.sSpawnedPropAnims[0].mnIW_ModelName = INT_TO_ENUM(MODEL_NAMES, HASH("W_ME_Switchblade"))
	
	sIWAnims.iNumPropsToSpawn++
	sIWAnims.sSpawnedPropAnims[1].mnIW_ModelName = GET_INTERACT_WITH_BAG_MODEL_TO_USE()
	
	sIWAnims.sCameraVars.fFOV = 28.0
	sIWAnims.sCameraVars.fPhaseEnd = 1.0
	sIWAnims.sCameraVars.iLerpIn = 0
	sIWAnims.sCameraVars.iLerpOut = 0
	sIWAnims.sCameraVars.bIsAnimated = TRUE
	sIWAnims.sCameraVars.vOffsetCoords = <<0.0, 0.7, -1.0>>
	sIWAnims.sCameraVars.vRot = <<0.0, 0.0, 0.0>>
	
	BOOL bReEntering = (eCutPaintingInteractionState = CUTPAINTING_INTERACTION_STATE__INIT)
	
	SWITCH sInteractWithParams.iInteractWith_AltAnimSet
		CASE ciIW_ALTANIMSET__CUTPAINTING_RIGHTHAND
			SWITCH iSubAnimPresetToUse
				CASE ciIW_SUBANIM__CUTPAINTING_TOPLEFT
					sIWAnims.sPlayerAnims.sIW_Enter = "VER_01_TOP_LEFT_ENTER"
					sIWAnims.sPlayerAnims.sIW_Looping = "VER_01_CUTTING_TOP_LEFT_IDLE"
					sIWAnims.sPlayerAnims.sIW_Exit = "VER_01_TOP_LEFT_EXIT"
					
					sIWAnims.sInteractWithEntityAnims.sIW_Enter = "VER_01_TOP_LEFT_ENTER_ch_Prop_vault_painting_01a"
					sIWAnims.sInteractWithEntityAnims.sIW_Looping = "VER_01_CUTTING_TOP_LEFT_IDLE_ch_Prop_vault_painting_01a"
					sIWAnims.sInteractWithEntityAnims.sIW_Exit = "VER_01_TOP_LEFT_EXIT_ch_Prop_vault_painting_01a"
					
					sIWAnims.sSpawnedPropAnims[0].sIW_Enter = "VER_01_TOP_LEFT_ENTER_W_ME_SWITCHBLADE"
					sIWAnims.sSpawnedPropAnims[0].sIW_Looping = "VER_01_CUTTING_TOP_LEFT_IDLE_W_ME_SWITCHBLADE"
					sIWAnims.sSpawnedPropAnims[0].sIW_Exit = "VER_01_TOP_LEFT_EXIT_W_ME_SWITCHBLADE"
					
					sIWAnims.sSpawnedPropAnims[1].sIW_Enter = "VER_01_TOP_LEFT_ENTER_hei_p_m_bag_var22_arm_s"
					sIWAnims.sSpawnedPropAnims[1].sIW_Looping = "VER_01_CUTTING_TOP_LEFT_IDLE_hei_p_m_bag_var22_arm_s"
					sIWAnims.sSpawnedPropAnims[1].sIW_Exit = "VER_01_TOP_LEFT_EXIT_hei_p_m_bag_var22_arm_s"
					
					sIWAnims.sCameraAnims.sIW_Enter = "VER_01_TOP_LEFT_ENTER_CAM"
					sIWAnims.sCameraAnims.sIW_Enter += GET_CUT_PAINTING_CAMERA_ENTER_SUFFIX()
					
					sIWAnims.sCameraAnims.sIW_Looping = "VER_01_CUTTING_TOP_LEFT_IDLE_CAM"
					sIWAnims.sCameraAnims.sIW_Exit = "VER_01_TOP_LEFT_EXIT_CAM"
				BREAK
				
				CASE ciIW_SUBANIM__CUTPAINTING_TOPRIGHT
					IF bReEntering
						sIWAnims.sPlayerAnims.sIW_Enter = "VER_01_TOP_RIGHT_ENTER"
						sIWAnims.sInteractWithEntityAnims.sIW_Enter = "VER_01_TOP_RIGHT_ENTER_ch_Prop_vault_painting_01a"
						sIWAnims.sSpawnedPropAnims[0].sIW_Enter = "VER_01_TOP_RIGHT_ENTER_W_ME_SWITCHBLADE"
						sIWAnims.sSpawnedPropAnims[1].sIW_Enter = "VER_01_TOP_RIGHT_ENTER_hei_p_m_bag_var22_arm_s"
						sIWAnims.sCameraAnims.sIW_Enter = "VER_01_TOP_RIGHT_ENTER_CAM"
						sIWAnims.sCameraAnims.sIW_Enter += GET_CUT_PAINTING_CAMERA_ENTER_SUFFIX()
					ELSE
						sIWAnims.sPlayerAnims.sIW_Enter = "VER_01_CUTTING_TOP_LEFT_TO_RIGHT"
						sIWAnims.sInteractWithEntityAnims.sIW_Enter = "VER_01_CUTTING_TOP_LEFT_TO_RIGHT_ch_Prop_vault_painting_01a"
						sIWAnims.sSpawnedPropAnims[0].sIW_Enter = "VER_01_CUTTING_TOP_LEFT_TO_RIGHT_W_ME_SWITCHBLADE"
						sIWAnims.sSpawnedPropAnims[1].sIW_Enter = "VER_01_CUTTING_TOP_LEFT_TO_RIGHT_hei_p_m_bag_var22_arm_s"
						sIWAnims.sCameraAnims.sIW_Enter = "VER_01_CUTTING_TOP_LEFT_TO_RIGHT_CAM"
					ENDIF
					
					sIWAnims.sPlayerAnims.sIW_Looping = "VER_01_CUTTING_TOP_RIGHT_IDLE"
					sIWAnims.sPlayerAnims.sIW_Exit = "VER_01_TOP_RIGHT_EXIT"
					
					sIWAnims.sInteractWithEntityAnims.sIW_Looping = "VER_01_CUTTING_TOP_RIGHT_IDLE_ch_Prop_vault_painting_01a"
					sIWAnims.sInteractWithEntityAnims.sIW_Exit = "VER_01_TOP_RIGHT_EXIT_ch_Prop_vault_painting_01a"
					
					sIWAnims.sSpawnedPropAnims[0].sIW_Looping = "VER_01_CUTTING_TOP_RIGHT_IDLE_W_ME_SWITCHBLADE"
					sIWAnims.sSpawnedPropAnims[0].sIW_Exit = "VER_01_TOP_RIGHT_EXIT_W_ME_SWITCHBLADE"
					
					sIWAnims.sSpawnedPropAnims[1].sIW_Looping = "VER_01_CUTTING_TOP_RIGHT_IDLE_hei_p_m_bag_var22_arm_s"
					sIWAnims.sSpawnedPropAnims[1].sIW_Exit = "VER_01_TOP_RIGHT_EXIT_hei_p_m_bag_var22_arm_s"
					
					sIWAnims.sCameraAnims.sIW_Looping = "VER_01_CUTTING_TOP_RIGHT_IDLE_CAM"
					sIWAnims.sCameraAnims.sIW_Exit = "VER_01_TOP_RIGHT_EXIT_CAM"
				BREAK
				
				CASE ciIW_SUBANIM__CUTPAINTING_BOTTOMRIGHT
					IF bReEntering
						sIWAnims.sPlayerAnims.sIW_Enter = "VER_01_BOTTOM_RIGHT_ENTER"
						sIWAnims.sInteractWithEntityAnims.sIW_Enter = "VER_01_BOTTOM_RIGHT_ENTER_ch_Prop_vault_painting_01a"
						sIWAnims.sSpawnedPropAnims[0].sIW_Enter = "VER_01_BOTTOM_RIGHT_ENTER_W_ME_SWITCHBLADE"
						sIWAnims.sSpawnedPropAnims[1].sIW_Enter = "VER_01_BOTTOM_RIGHT_ENTER_hei_p_m_bag_var22_arm_s"
						sIWAnims.sCameraAnims.sIW_Enter = "VER_01_BOTTOM_RIGHT_ENTER_CAM"
						sIWAnims.sCameraAnims.sIW_Enter += GET_CUT_PAINTING_CAMERA_ENTER_SUFFIX()
					ELSE
						sIWAnims.sPlayerAnims.sIW_Enter = "VER_01_CUTTING_RIGHT_TOP_TO_BOTTOM"
						sIWAnims.sInteractWithEntityAnims.sIW_Enter = "VER_01_CUTTING_RIGHT_TOP_TO_BOTTOM_ch_Prop_vault_painting_01a"
						sIWAnims.sSpawnedPropAnims[0].sIW_Enter = "VER_01_CUTTING_RIGHT_TOP_TO_BOTTOM_W_ME_SWITCHBLADE"
						sIWAnims.sSpawnedPropAnims[1].sIW_Enter = "VER_01_CUTTING_RIGHT_TOP_TO_BOTTOM_hei_p_m_bag_var22_arm_s"
						sIWAnims.sCameraAnims.sIW_Enter = "VER_01_CUTTING_RIGHT_TOP_TO_BOTTOM_CAM"
					ENDIF
					
					sIWAnims.sPlayerAnims.sIW_Looping = "VER_01_CUTTING_BOTTOM_RIGHT_IDLE"
					sIWAnims.sPlayerAnims.sIW_Exit = "VER_01_BOTTOM_RIGHT_EXIT"
					
					sIWAnims.sInteractWithEntityAnims.sIW_Looping = "VER_01_CUTTING_BOTTOM_RIGHT_IDLE_ch_Prop_vault_painting_01a"
					sIWAnims.sInteractWithEntityAnims.sIW_Exit = "VER_01_BOTTOM_RIGHT_EXIT_ch_Prop_vault_painting_01a"
					
					sIWAnims.sSpawnedPropAnims[0].sIW_Looping = "VER_01_CUTTING_BOTTOM_RIGHT_IDLE_W_ME_SWITCHBLADE"
					sIWAnims.sSpawnedPropAnims[0].sIW_Exit = "VER_01_BOTTOM_RIGHT_EXIT_W_ME_SWITCHBLADE"
					
					sIWAnims.sSpawnedPropAnims[1].sIW_Looping = "VER_01_CUTTING_BOTTOM_RIGHT_IDLE_hei_p_m_bag_var22_arm_s"
					sIWAnims.sSpawnedPropAnims[1].sIW_Exit = "VER_01_BOTTOM_RIGHT_EXIT_hei_p_m_bag_var22_arm_s"
					
					sIWAnims.sCameraAnims.sIW_Looping = "VER_01_CUTTING_BOTTOM_RIGHT_IDLE_CAM"
					sIWAnims.sCameraAnims.sIW_Exit = "VER_01_BOTTOM_RIGHT_EXIT_CAM"
				BREAK
				
				CASE ciIW_SUBANIM__CUTPAINTING_BOTTOMLEFT
				
					IF bReEntering
						sIWAnims.sPlayerAnims.sIW_Enter = "VER_01_TOP_LEFT_RE-ENTER"
						sIWAnims.sInteractWithEntityAnims.sIW_Enter = "VER_01_TOP_LEFT_RE-ENTER_ch_Prop_vault_painting_01a"
						sIWAnims.sSpawnedPropAnims[0].sIW_Enter = "VER_01_TOP_LEFT_RE-ENTER_W_ME_SWITCHBLADE"
						sIWAnims.sSpawnedPropAnims[1].sIW_Enter = "VER_01_TOP_LEFT_RE-ENTER_hei_p_m_bag_var22_arm_s"
						sIWAnims.sCameraAnims.sIW_Enter = "VER_01_TOP_LEFT_ENTER_CAM"
						sIWAnims.sCameraAnims.sIW_Enter += GET_CUT_PAINTING_CAMERA_ENTER_SUFFIX()
					ELSE
						sIWAnims.sPlayerAnims.sIW_Enter = "VER_01_CUTTING_BOTTOM_RIGHT_TO_LEFT"
						sIWAnims.sInteractWithEntityAnims.sIW_Enter = "VER_01_CUTTING_BOTTOM_RIGHT_TO_LEFT_ch_Prop_vault_painting_01a"
						sIWAnims.sSpawnedPropAnims[0].sIW_Enter = "VER_01_CUTTING_BOTTOM_RIGHT_TO_LEFT_W_ME_SWITCHBLADE"
						sIWAnims.sSpawnedPropAnims[1].sIW_Enter = "VER_01_CUTTING_BOTTOM_RIGHT_TO_LEFT_hei_p_m_bag_var22_arm_s"
						sIWAnims.sCameraAnims.sIW_Enter = "VER_01_CUTTING_BOTTOM_RIGHT_TO_LEFT_CAM"
					ENDIF
					
					sIWAnims.sPlayerAnims.sIW_Looping = "VER_01_CUTTING_TOP_LEFT_IDLE"
					sIWAnims.sPlayerAnims.sIW_Exit = "VER_01_TOP_LEFT_EXIT"
					
					// The painting itself needs to play the BOTTOM_LEFT variants to ensure it keeps looking half-cut out
					sIWAnims.sInteractWithEntityAnims.sIW_Looping = "VER_01_CUTTING_BOTTOM_LEFT_IDLE_ch_Prop_vault_painting_01a"
					sIWAnims.sInteractWithEntityAnims.sIW_Exit = "VER_01_BOTTOM_LEFT_EXIT_ch_Prop_vault_painting_01a"
					
					sIWAnims.sSpawnedPropAnims[0].sIW_Looping = "VER_01_CUTTING_TOP_LEFT_IDLE_W_ME_SWITCHBLADE"
					sIWAnims.sSpawnedPropAnims[0].sIW_Exit = "VER_01_TOP_LEFT_EXIT_W_ME_SWITCHBLADE"
					
					sIWAnims.sSpawnedPropAnims[1].sIW_Looping = "VER_01_CUTTING_TOP_LEFT_IDLE_hei_p_m_bag_var22_arm_s"
					sIWAnims.sSpawnedPropAnims[1].sIW_Exit = "VER_01_TOP_LEFT_EXIT_hei_p_m_bag_var22_arm_s"
					
					sIWAnims.sCameraAnims.sIW_Looping = "VER_01_CUTTING_TOP_LEFT_IDLE_CAM"
					sIWAnims.sCameraAnims.sIW_Exit = "VER_01_TOP_LEFT_EXIT_CAM"
				BREAK
				
				CASE ciIW_SUBANIM__CUTPAINTING_FINALCUTANDROLLUP
				
					sIWAnims.sPlayerAnims.sIW_Enter = "VER_01_CUTTING_LEFT_TOP_TO_BOTTOM"
					sIWAnims.sInteractWithEntityAnims.sIW_Enter = "VER_01_CUTTING_LEFT_TOP_TO_BOTTOM_ch_Prop_vault_painting_01a"
					sIWAnims.sSpawnedPropAnims[0].sIW_Enter = "VER_01_CUTTING_LEFT_TOP_TO_BOTTOM_W_ME_SWITCHBLADE"
					sIWAnims.sSpawnedPropAnims[1].sIW_Enter = "VER_01_CUTTING_LEFT_TOP_TO_BOTTOM_hei_p_m_bag_var22_arm_s"
					sIWAnims.sCameraAnims.sIW_Enter = "VER_01_CUTTING_LEFT_TOP_TO_BOTTOM_CAM"
					
					sIWAnims.sPlayerAnims.sIW_Exit = "VER_01_WITH_PAINTING_EXIT"
					sIWAnims.sInteractWithEntityAnims.sIW_Exit = "VER_01_WITH_PAINTING_EXIT_ch_Prop_vault_painting_01a"
					sIWAnims.sSpawnedPropAnims[0].sIW_Exit = "VER_01_WITH_PAINTING_EXIT_W_ME_SWITCHBLADE"
					sIWAnims.sSpawnedPropAnims[1].sIW_Exit = "VER_01_WITH_PAINTING_EXIT_hei_p_m_bag_var22_arm_s"
					sIWAnims.sCameraAnims.sIW_Exit = "VER_01_WITH_PAINTING_EXIT_CAM_RE1"
				BREAK
			ENDSWITCH
		BREAK
	
		CASE ciIW_ALTANIMSET__CUTPAINTING_LEFTHAND
			SWITCH iSubAnimPresetToUse
				CASE ciIW_SUBANIM__CUTPAINTING_TOPLEFT
					sIWAnims.sPlayerAnims.sIW_Enter = "VER_02_TOP_LEFT_ENTER"
					sIWAnims.sPlayerAnims.sIW_Looping = "VER_02_CUTTING_TOP_LEFT_IDLE"
					sIWAnims.sPlayerAnims.sIW_Exit = "VER_02_TOP_LEFT_EXIT"
					
					sIWAnims.sInteractWithEntityAnims.sIW_Enter = "VER_02_TOP_LEFT_ENTER_ch_Prop_vault_painting_01a"
					sIWAnims.sInteractWithEntityAnims.sIW_Looping = "VER_02_CUTTING_TOP_LEFT_IDLE_ch_Prop_vault_painting_01a"
					sIWAnims.sInteractWithEntityAnims.sIW_Exit = "VER_02_TOP_LEFT_EXIT_ch_Prop_vault_painting_01a"
					
					sIWAnims.sSpawnedPropAnims[0].sIW_Enter = "VER_02_TOP_LEFT_ENTER_W_ME_SWITCHBLADE"
					sIWAnims.sSpawnedPropAnims[0].sIW_Looping = "VER_02_CUTTING_TOP_LEFT_IDLE_W_ME_SWITCHBLADE"
					sIWAnims.sSpawnedPropAnims[0].sIW_Exit = "VER_02_TOP_LEFT_EXIT_W_ME_SWITCHBLADE"
					
					sIWAnims.sSpawnedPropAnims[1].sIW_Enter = "VER_02_TOP_LEFT_ENTER_hei_p_m_bag_var22_arm_s"
					sIWAnims.sSpawnedPropAnims[1].sIW_Looping = "VER_02_CUTTING_TOP_LEFT_IDLE_hei_p_m_bag_var22_arm_s"
					sIWAnims.sSpawnedPropAnims[1].sIW_Exit = "VER_02_TOP_LEFT_EXIT_hei_p_m_bag_var22_arm_s"
					
					sIWAnims.sCameraAnims.sIW_Enter = "VER_02_TOP_LEFT_ENTER_CAM"
					sIWAnims.sCameraAnims.sIW_Enter += GET_CUT_PAINTING_CAMERA_ENTER_SUFFIX()
					
					sIWAnims.sCameraAnims.sIW_Looping = "VER_02_CUTTING_TOP_LEFT_IDLE_CAM"
					sIWAnims.sCameraAnims.sIW_Exit = "VER_02_TOP_LEFT_EXIT_CAM"
				BREAK
				
				CASE ciIW_SUBANIM__CUTPAINTING_TOPRIGHT
					IF bReEntering
						sIWAnims.sPlayerAnims.sIW_Enter = "VER_02_TOP_RIGHT_ENTER"
						sIWAnims.sInteractWithEntityAnims.sIW_Enter = "VER_02_TOP_RIGHT_ENTER_ch_Prop_vault_painting_01a"
						sIWAnims.sSpawnedPropAnims[0].sIW_Enter = "VER_02_TOP_RIGHT_ENTER_W_ME_SWITCHBLADE"
						sIWAnims.sSpawnedPropAnims[1].sIW_Enter = "VER_02_TOP_RIGHT_ENTER_hei_p_m_bag_var22_arm_s"
						sIWAnims.sCameraAnims.sIW_Enter = "VER_02_TOP_RIGHT_ENTER_CAM"
						sIWAnims.sCameraAnims.sIW_Enter += GET_CUT_PAINTING_CAMERA_ENTER_SUFFIX()
					ELSE
						sIWAnims.sPlayerAnims.sIW_Enter = "VER_02_CUTTING_TOP_LEFT_TO_RIGHT"
						sIWAnims.sInteractWithEntityAnims.sIW_Enter = "VER_02_CUTTING_TOP_LEFT_TO_RIGHT_ch_Prop_vault_painting_01a"
						sIWAnims.sSpawnedPropAnims[0].sIW_Enter = "VER_02_CUTTING_TOP_LEFT_TO_RIGHT_W_ME_SWITCHBLADE"
						sIWAnims.sSpawnedPropAnims[1].sIW_Enter = "VER_02_CUTTING_TOP_LEFT_TO_RIGHT_hei_p_m_bag_var22_arm_s"
						sIWAnims.sCameraAnims.sIW_Enter = "VER_02_CUTTING_TOP_LEFT_TO_RIGHT_CAM"
					ENDIF
					
					sIWAnims.sPlayerAnims.sIW_Looping = "VER_02_CUTTING_TOP_RIGHT_IDLE"
					sIWAnims.sPlayerAnims.sIW_Exit = "VER_02_TOP_RIGHT_EXIT"
					
					sIWAnims.sInteractWithEntityAnims.sIW_Looping = "VER_02_CUTTING_TOP_RIGHT_IDLE_ch_Prop_vault_painting_01a"
					sIWAnims.sInteractWithEntityAnims.sIW_Exit = "VER_02_TOP_RIGHT_EXIT_ch_Prop_vault_painting_01a"
					
					sIWAnims.sSpawnedPropAnims[0].sIW_Looping = "VER_02_CUTTING_TOP_RIGHT_IDLE_W_ME_SWITCHBLADE"
					sIWAnims.sSpawnedPropAnims[0].sIW_Exit = "VER_02_TOP_RIGHT_EXIT_W_ME_SWITCHBLADE"
					
					sIWAnims.sSpawnedPropAnims[1].sIW_Looping = "VER_02_CUTTING_TOP_RIGHT_IDLE_hei_p_m_bag_var22_arm_s"
					sIWAnims.sSpawnedPropAnims[1].sIW_Exit = "VER_02_TOP_RIGHT_EXIT_hei_p_m_bag_var22_arm_s"
					
					sIWAnims.sCameraAnims.sIW_Looping = "VER_02_CUTTING_TOP_RIGHT_IDLE_CAM"
					sIWAnims.sCameraAnims.sIW_Exit = "VER_02_TOP_RIGHT_EXIT_CAM"
				BREAK
				
				CASE ciIW_SUBANIM__CUTPAINTING_BOTTOMRIGHT
					IF bReEntering
						sIWAnims.sPlayerAnims.sIW_Enter = "VER_02_BOTTOM_RIGHT_ENTER"
						sIWAnims.sInteractWithEntityAnims.sIW_Enter = "VER_02_BOTTOM_RIGHT_ENTER_ch_Prop_vault_painting_01a"
						sIWAnims.sSpawnedPropAnims[0].sIW_Enter = "VER_02_BOTTOM_RIGHT_ENTER_W_ME_SWITCHBLADE"
						sIWAnims.sSpawnedPropAnims[1].sIW_Enter = "VER_02_BOTTOM_RIGHT_ENTER_hei_p_m_bag_var22_arm_s"
						sIWAnims.sCameraAnims.sIW_Enter = "VER_02_BOTTOM_RIGHT_ENTER_CAM"
						sIWAnims.sCameraAnims.sIW_Enter += GET_CUT_PAINTING_CAMERA_ENTER_SUFFIX()
					ELSE
						sIWAnims.sPlayerAnims.sIW_Enter = "VER_02_CUTTING_RIGHT_TOP_TO_BOTTOM"
						sIWAnims.sInteractWithEntityAnims.sIW_Enter = "VER_02_CUTTING_RIGHT_TOP_TO_BOTTOM_ch_Prop_vault_painting_01a"
						sIWAnims.sSpawnedPropAnims[0].sIW_Enter = "VER_02_CUTTING_RIGHT_TOP_TO_BOTTOM_W_ME_SWITCHBLADE"
						sIWAnims.sSpawnedPropAnims[1].sIW_Enter = "VER_02_CUTTING_RIGHT_TOP_TO_BOTTOM_hei_p_m_bag_var22_arm_s"
						sIWAnims.sCameraAnims.sIW_Enter = "VER_02_CUTTING_RIGHT_TOP_TO_BOTTOM_CAM"
					ENDIF
					
					sIWAnims.sPlayerAnims.sIW_Looping = "VER_02_CUTTING_BOTTOM_RIGHT_IDLE"
					sIWAnims.sPlayerAnims.sIW_Exit = "VER_02_BOTTOM_RIGHT_EXIT"
					
					sIWAnims.sInteractWithEntityAnims.sIW_Looping = "VER_02_CUTTING_BOTTOM_RIGHT_IDLE_ch_Prop_vault_painting_01a"
					sIWAnims.sInteractWithEntityAnims.sIW_Exit = "VER_02_BOTTOM_RIGHT_EXIT_ch_Prop_vault_painting_01a"
					
					sIWAnims.sSpawnedPropAnims[0].sIW_Looping = "VER_02_CUTTING_BOTTOM_RIGHT_IDLE_W_ME_SWITCHBLADE"
					sIWAnims.sSpawnedPropAnims[0].sIW_Exit = "VER_02_BOTTOM_RIGHT_EXIT_W_ME_SWITCHBLADE"
					
					sIWAnims.sSpawnedPropAnims[1].sIW_Looping = "VER_02_CUTTING_BOTTOM_RIGHT_IDLE_hei_p_m_bag_var22_arm_s"
					sIWAnims.sSpawnedPropAnims[1].sIW_Exit = "VER_02_BOTTOM_RIGHT_EXIT_hei_p_m_bag_var22_arm_s"
					
					sIWAnims.sCameraAnims.sIW_Looping = "VER_02_CUTTING_BOTTOM_RIGHT_IDLE_CAM"
					sIWAnims.sCameraAnims.sIW_Exit = "VER_02_BOTTOM_RIGHT_EXIT_CAM"
				BREAK
				
				CASE ciIW_SUBANIM__CUTPAINTING_BOTTOMLEFT
				
					IF bReEntering
						sIWAnims.sPlayerAnims.sIW_Enter = "VER_02_TOP_LEFT_RE-ENTER"
						sIWAnims.sInteractWithEntityAnims.sIW_Enter = "VER_02_TOP_LEFT_RE-ENTER_ch_Prop_vault_painting_01a"
						sIWAnims.sSpawnedPropAnims[0].sIW_Enter = "VER_02_TOP_LEFT_RE-ENTER_W_ME_SWITCHBLADE"
						sIWAnims.sSpawnedPropAnims[1].sIW_Enter = "VER_02_TOP_LEFT_RE-ENTER_hei_p_m_bag_var22_arm_s"
						sIWAnims.sCameraAnims.sIW_Enter = "VER_02_TOP_LEFT_ENTER_CAM"
						sIWAnims.sCameraAnims.sIW_Enter += GET_CUT_PAINTING_CAMERA_ENTER_SUFFIX()
					ELSE
						sIWAnims.sPlayerAnims.sIW_Enter = "VER_02_CUTTING_BOTTOM_RIGHT_TO_LEFT"
						sIWAnims.sInteractWithEntityAnims.sIW_Enter = "VER_02_CUTTING_BOTTOM_RIGHT_TO_LEFT_ch_Prop_vault_painting_01a"
						sIWAnims.sSpawnedPropAnims[0].sIW_Enter = "VER_02_CUTTING_BOTTOM_RIGHT_TO_LEFT_W_ME_SWITCHBLADE"
						sIWAnims.sSpawnedPropAnims[1].sIW_Enter = "VER_02_CUTTING_BOTTOM_RIGHT_TO_LEFT_hei_p_m_bag_var22_arm_s"
						sIWAnims.sCameraAnims.sIW_Enter = "VER_02_CUTTING_BOTTOM_RIGHT_TO_LEFT_CAM"
					ENDIF
					
					sIWAnims.sPlayerAnims.sIW_Looping = "VER_02_CUTTING_TOP_LEFT_IDLE"
					sIWAnims.sPlayerAnims.sIW_Exit = "VER_02_TOP_LEFT_EXIT"
					
					// The painting itself needs to play the BOTTOM_LEFT variants to ensure it keeps looking half-cut out
					sIWAnims.sInteractWithEntityAnims.sIW_Looping = "VER_02_CUTTING_BOTTOM_LEFT_IDLE_ch_Prop_vault_painting_01a"
					sIWAnims.sInteractWithEntityAnims.sIW_Exit = "VER_02_BOTTOM_LEFT_EXIT_ch_Prop_vault_painting_01a"
					
					sIWAnims.sSpawnedPropAnims[0].sIW_Looping = "VER_02_CUTTING_TOP_LEFT_IDLE_W_ME_SWITCHBLADE"
					sIWAnims.sSpawnedPropAnims[0].sIW_Exit = "VER_02_TOP_LEFT_EXIT_W_ME_SWITCHBLADE"
					
					sIWAnims.sSpawnedPropAnims[1].sIW_Looping = "VER_02_CUTTING_TOP_LEFT_IDLE_hei_p_m_bag_var22_arm_s"
					sIWAnims.sSpawnedPropAnims[1].sIW_Exit = "VER_02_TOP_LEFT_EXIT_hei_p_m_bag_var22_arm_s"
					
					sIWAnims.sCameraAnims.sIW_Looping = "VER_02_CUTTING_TOP_LEFT_IDLE_CAM"
					sIWAnims.sCameraAnims.sIW_Exit = "VER_02_TOP_LEFT_EXIT_CAM"
				BREAK
				
				CASE ciIW_SUBANIM__CUTPAINTING_FINALCUTANDROLLUP
				
					sIWAnims.sPlayerAnims.sIW_Enter = "VER_02_CUTTING_LEFT_TOP_TO_BOTTOM"
					sIWAnims.sInteractWithEntityAnims.sIW_Enter = "VER_02_CUTTING_LEFT_TOP_TO_BOTTOM_ch_Prop_vault_painting_01a"
					sIWAnims.sSpawnedPropAnims[0].sIW_Enter = "VER_02_CUTTING_LEFT_TOP_TO_BOTTOM_W_ME_SWITCHBLADE"
					sIWAnims.sSpawnedPropAnims[1].sIW_Enter = "VER_02_CUTTING_LEFT_TOP_TO_BOTTOM_hei_p_m_bag_var22_arm_s"
					sIWAnims.sCameraAnims.sIW_Enter = "VER_02_CUTTING_LEFT_TOP_TO_BOTTOM_CAM"
					
					sIWAnims.sPlayerAnims.sIW_Exit = "VER_02_WITH_PAINTING_EXIT"
					sIWAnims.sInteractWithEntityAnims.sIW_Exit = "VER_02_WITH_PAINTING_EXIT_ch_Prop_vault_painting_01a"
					sIWAnims.sSpawnedPropAnims[0].sIW_Exit = "VER_02_WITH_PAINTING_EXIT_W_ME_SWITCHBLADE"
					sIWAnims.sSpawnedPropAnims[1].sIW_Exit = "VER_02_WITH_PAINTING_EXIT_hei_p_m_bag_var22_arm_s"
					sIWAnims.sCameraAnims.sIW_Exit = "VER_02_WITH_PAINTING_EXIT_CAM"
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL SHOULD_PLAYER_DO_BACK_SIDE_INTERACT_WITH(INTERACT_WITH_PARAMS& sInteractWithParams)

	ENTITY_INDEX eiEntityToCheck = sInteractWithParams.oiInteractWithEntity
	
	IF sInteractWithParams.iNetworkedObjectIndex > -1
	AND DOES_ENTITY_EXIST(sInteractWithVars.eiInteractWith_CachedSceneAlignmentEntities[sInteractWithParams.iNetworkedObjectIndex])
		eiEntityToCheck = sInteractWithVars.eiInteractWith_CachedSceneAlignmentEntities[sInteractWithParams.iNetworkedObjectIndex]
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(eiEntityToCheck)
		PRINTLN("[InteractWith] SHOULD_PLAYER_DO_BACK_SIDE_INTERACT_WITH | eiEntityToCheck doesn't exist!")
		RETURN FALSE
	ENDIF
	
	VECTOR vCoordsToCheckFrom
	IF sInteractWithVars.eInteractWith_CurrentState >= IW_STATE_WALKING
		vCoordsToCheckFrom = sInteractWithVars.vInteractWith_CachedInitialPlayerStartPos
	ELSE
		vCoordsToCheckFrom = GET_ENTITY_COORDS(LocalPlayerPed, FALSE)
	ENDIF
		
	VECTOR vPositionRelativeToInteractionObject = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(eiEntityToCheck, vCoordsToCheckFrom)
	
	#IF IS_DEBUG_BUILD
	IF bInteractWithDebug
		TEXT_LABEL_63 tlLocalpos = "LP: "
		tlLocalpos += FLOAT_TO_STRING(vPositionRelativeToInteractionObject.x)
		tlLocalpos += ", "
		tlLocalpos += FLOAT_TO_STRING(vPositionRelativeToInteractionObject.y)
		tlLocalpos += ", "
		tlLocalpos += FLOAT_TO_STRING(vPositionRelativeToInteractionObject.z)
		DRAW_DEBUG_TEXT(tlLocalpos, GET_ENTITY_COORDS(eiEntityToCheck, FALSE), 255, 255, 0, 255)
	ENDIF
	#ENDIF
	
	RETURN vPositionRelativeToInteractionObject.y < 0.0
ENDFUNC

PROC MAKE_INTERACT_WITH_ANIMS_MATCH_WITH_SUFFIX(INTERACT_WITH_ANIM_ENTITY_INFO& sBaseAnims, INTERACT_WITH_ANIM_ENTITY_INFO& sAnimsToEdit, STRING sSuffix)
	sAnimsToEdit.sIW_Enter = sBaseAnims.sIW_Enter
	sAnimsToEdit.sIW_Looping = sBaseAnims.sIW_Looping
	sAnimsToEdit.sIW_Exit = sBaseAnims.sIW_Exit
	
	IF NOT IS_STRING_NULL_OR_EMPTY(sAnimsToEdit.sIW_Enter)
		sAnimsToEdit.sIW_Enter += sSuffix
	ENDIF
	
	IF NOT IS_STRING_NULL_OR_EMPTY(sAnimsToEdit.sIW_Looping)
		sAnimsToEdit.sIW_Looping += sSuffix
	ENDIF
	
	IF NOT IS_STRING_NULL_OR_EMPTY(sAnimsToEdit.sIW_Exit)
		sAnimsToEdit.sIW_Exit += sSuffix
	ENDIF
ENDPROC

PROC GET_ANIM_INFO_FOR_INTERACT_WITH(INTERACT_WITH_PARAMS& sInteractWithParams, INTERACT_WITH_ANIM_ASSETS& sIWAnims, INT iSubAnimPresetToUse)
	
	// Clear the struct here so we get a 100% up to date set of data by the end of this
	INTERACT_WITH_ANIM_ASSETS sEmpty
	COPY_SCRIPT_STRUCT(sIWAnims, sEmpty, SIZE_OF(sIWAnims))
	
	SWITCH sInteractWithParams.iInteractWith_AnimPreset
		
		CASE ciINTERACT_WITH_PRESET__NONE							
			sIWAnims = sInteractWithParams.sCustomIWAnims
			
			IF NOT IS_STRING_NULL_OR_EMPTY(sInteractWithVars.sInteractWith_SubEnterAnim)
				sIWAnims.sPlayerAnims.sIW_Enter = sInteractWithVars.sInteractWith_SubEnterAnim
			ENDIF
			IF NOT IS_STRING_NULL_OR_EMPTY(sInteractWithVars.sInteractWith_SubLoopingAnim)
				sIWAnims.sPlayerAnims.sIW_Looping = sInteractWithVars.sInteractWith_SubLoopingAnim
			ENDIF
			IF NOT IS_STRING_NULL_OR_EMPTY(sInteractWithVars.sInteractWith_SubExitAnim)
				sIWAnims.sPlayerAnims.sIW_Exit = sInteractWithVars.sInteractWith_SubExitAnim
			ENDIF
		BREAK
		
		CASE ciINTERACT_WITH_PRESET__PUSH_BUTTON
			SWITCH sInteractWithParams.iInteractWith_AltAnimSet
				CASE ciIW_ALTANIMSET__PUSHBUTTON_LEGACY
					sIWAnims.sIW_AnimDict = "anim@mp_radio@high_life_apment"
					sIWAnims.sPlayerAnims.sIW_Enter = "action_a_bedroom"
				BREAK
				
				CASE ciIW_ALTANIMSET__PUSHBUTTON_RUSTYBUTTON
					sIWAnims.sIW_AnimDict = "ANIM@SCRIPTED@HEIST@IG3_BUTTON_PRESS@MALE@"
					sIWAnims.sPlayerAnims.sIW_Enter = "BUTTON_PRESS"
					
					sIWAnims.sInteractWithEntityAnims.sIW_Enter = "BUTTON_PRESS_h4_Prop_h4_Casino_Button_01a"
				BREAK
			ENDSWITCH
		BREAK
		
		CASE ciINTERACT_WITH_PRESET__RUMMAGE_AROUND
			sIWAnims.sIW_AnimDict = "ANIM@HEISTS@PRISON_HEISTSTATION@"
			sIWAnims.sPlayerAnims.sIW_Enter = "pickup_bus_schedule"
		BREAK
		
		CASE ciINTERACT_WITH_PRESET__BOMB_PLANT
			SWITCH sInteractWithParams.iInteractWith_AltAnimSet
				CASE ciIW_ALTANIMSET__PLANTBOMB_IN_FLOOR
					sIWAnims.sIW_AnimDict = "ANIM@MP_FIREWORKS"
					sIWAnims.sPlayerAnims.sIW_Enter = "PLACE_FIREWORK_3_BOX"
				BREAK
				
				CASE ciIW_ALTANIMSET__PLANTBOMB_ON_WALL_PRE_CUTSCENE
					sIWAnims.sIW_AnimDict = "ANIM@SCRIPTED@HEIST@IG11_BOMB_PLANT@MALE@"
					sIWAnims.sPlayerAnims.sIW_Enter = "ENTER"
					
					sIWAnims.sInteractWithEntityAnims.fPropHideUntilPhase = 0.5
					
					sIWAnims.iNumPropsToSpawn++
					sIWAnims.sSpawnedPropAnims[0].mnIW_ModelName = INT_TO_ENUM(MODEL_NAMES, HASH("h4_Prop_h4_LD_Bomb_02a"))
					sIWAnims.sSpawnedPropAnims[0].sIW_Enter = "ENTER_Bomb"
					
					// Finish the rule straight away when the anim reaches its completion point, allowing the anim to cut straight into the cutscene following this rule if needed
					SET_BIT(sInteractWithParams.iInteractWithParamsBS, ciINTERACT_WITH_PARAMS_BS__EnableInteractWithEarlyEndForObjectiveMinigames)
					CLEAR_BIT(sInteractWithParams.iInteractWithParamsBS, ciINTERACT_WITH_PARAMS_BS__OnlyReturnTrueAfterCleanup)
				BREAK
				
				CASE ciIW_ALTANIMSET__PLANTBOMB_ON_WALL_STANDARD
					IF IS_PED_WEARING_HIGH_HEELS(LocalPlayerPed)
						sIWAnims.sIW_AnimDict = "ANIM@SCRIPTED@PLAYER@MISSION@TUN_BOMB_PLANT@HEELED@"
					ELSE
						sIWAnims.sIW_AnimDict = "ANIM@SCRIPTED@PLAYER@MISSION@TUN_BOMB_PLANT@MALE@"
					ENDIF
					
					sIWAnims.sPlayerAnims.sIW_Enter = "ENTER"
					sIWAnims.sPlayerFaceAnims.sIW_Enter = "ENTER_Facial"
					
					sIWAnims.sInteractWithEntityAnims.fPropHideUntilPhase = 0.5
					
					sIWAnims.iNumPropsToSpawn++
					sIWAnims.sSpawnedPropAnims[0].mnIW_ModelName = GET_INTERACT_WITH_BAG_MODEL_TO_USE()
					sIWAnims.sSpawnedPropAnims[0].sIW_Enter = "ENTER_Bag"
					
					sIWAnims.iNumPropsToSpawn++
					sIWAnims.sSpawnedPropAnims[1].mnIW_ModelName = INT_TO_ENUM(MODEL_NAMES, HASH("h4_Prop_h4_LD_Bomb_02a"))
					sIWAnims.sSpawnedPropAnims[1].sIW_Enter = "ENTER_Bomb"
				BREAK
			ENDSWITCH
		BREAK
		
		CASE ciINTERACT_WITH_PRESET__SEARCH_BODY_ON_TABLE
			sIWAnims.sIW_AnimDict = "anim@GangOps@Morgue@Table@"
			sIWAnims.sPlayerAnims.sIW_Enter = "Player_Search"
			
			sIWAnims.iNumPropsToSpawn++
			sIWAnims.sSpawnedPropAnims[0].sIW_Enter = "Device_Search"
			sIWAnims.sSpawnedPropAnims[0].fPropHideUntilPhase = 0.55
		BREAK
		
		CASE ciINTERACT_WITH_PRESET__SEARCH_BODY_ON_FLOOR
			SWITCH sInteractWithParams.iInteractWith_AltAnimSet
				CASE ciIW_ALTANIMSET__SEARCHBODYONFLOOR_GRABUSBSTICK
					sIWAnims.sIW_AnimDict = "anim@GangOps@Facility@Servers@BodySearch@"
					sIWAnims.sPlayerAnims.sIW_Enter = "Player_Search"
					
					sIWAnims.iNumPropsToSpawn++
					sIWAnims.sSpawnedPropAnims[0].mnIW_ModelName = HEI_PROP_HST_USB_DRIVE
					sIWAnims.sSpawnedPropAnims[0].sIW_Enter = "Device_Search"
					sIWAnims.sSpawnedPropAnims[0].fPropHideUntilPhase = 0.725
				BREAK
				
				CASE ciIW_ALTANIMSET__SEARCHBODYONFLOOR_GRABPHONE
					sIWAnims.sIW_AnimDict = "anim@GangOps@Facility@Servers@BodySearch@"
					sIWAnims.sPlayerAnims.sIW_Enter = "Player_Search"
					
					sIWAnims.iNumPropsToSpawn++
					sIWAnims.sSpawnedPropAnims[0].mnIW_ModelName = prop_npc_phone
					sIWAnims.sSpawnedPropAnims[0].sIW_Enter = "Device_Search"
					sIWAnims.sSpawnedPropAnims[0].fPropHideUntilPhase = 0.725
				BREAK
			ENDSWITCH
			
		BREAK
		
		CASE ciINTERACT_WITH_PRESET__POWER_BOX_CRANK
			sIWAnims.sIW_AnimDict = "anim@GangOps@Hanger@FUSE_BOX@"
			sIWAnims.sPlayerAnims.sIW_Enter = "SWITCH_ON"
			sIWAnims.sInteractWithEntityAnims.sIW_Enter = "SWITCH_ON_BOX"
		BREAK
		
		CASE ciINTERACT_WITH_PRESET__HACK_LAPTOP_USB
			SWITCH sInteractWithParams.iInteractWith_AltAnimSet
				CASE ciIW_ALTANIMSET__HACKLAPTOPUSB_ONE_SHOT
					sIWAnims.sIW_AnimDict = "anim@GangOps@Morgue@Office@Laptop@"
					sIWAnims.sPlayerAnims.sIW_Enter = "ENTER"
					sIWAnims.sPlayerAnims.sIW_Exit = "EXIT"
					
					sIWAnims.iNumPropsToSpawn++
					sIWAnims.sSpawnedPropAnims[0].mnIW_ModelName = HEI_PROP_HST_USB_DRIVE
					SWITCH iSubAnimPresetToUse
						CASE ciIW_SUBANIM__LAPTOP_ENTER sIWAnims.sSpawnedPropAnims[0].sIW_Enter = "ENTER_USB"	BREAK
						CASE ciIW_SUBANIM__LAPTOP_EXIT 	sIWAnims.sSpawnedPropAnims[0].sIW_Exit = "EXIT_USB"		BREAK
					ENDSWITCH
				BREAK
				
				CASE ciIW_ALTANIMSET__HACKLAPTOPUSB_LOOPING
					sIWAnims.sIW_AnimDict = "anim@GangOps@Morgue@Office@Laptop@"
					sIWAnims.sPlayerAnims.sIW_Enter = "ENTER"
					sIWAnims.sPlayerAnims.sIW_Looping = "IDLE"
					sIWAnims.sPlayerAnims.sIW_Exit = "EXIT"
					
					sIWAnims.iNumPropsToSpawn++
					sIWAnims.sSpawnedPropAnims[0].mnIW_ModelName = HEI_PROP_HST_USB_DRIVE
					SWITCH iSubAnimPresetToUse
						CASE ciIW_SUBANIM__LAPTOP_ENTER sIWAnims.sSpawnedPropAnims[0].sIW_Enter = "ENTER_USB"	BREAK
						CASE ciIW_SUBANIM__LAPTOP_IDLE 	sIWAnims.sSpawnedPropAnims[0].sIW_Looping = "IDLE_USB"	BREAK
						CASE ciIW_SUBANIM__LAPTOP_EXIT 	sIWAnims.sSpawnedPropAnims[0].sIW_Exit = "EXIT_USB"		BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE ciINTERACT_WITH_PRESET__CRATE_IN_VAN
			sIWAnims.sIW_AnimDict = "anim@GangOps@VAN@DRIVE_GRAB@"
			sIWAnims.sPlayerAnims.sIW_Enter = "grab_drive"
			
			sIWAnims.iNumPropsToSpawn++
			sIWAnims.sSpawnedPropAnims[0].mnIW_ModelName = PROP_CS_SERVER_DRIVE
			sIWAnims.sSpawnedPropAnims[0].sIW_Enter = "drive_grab_drive"
		BREAK
		
		CASE ciINTERACT_WITH_PRESET__PLANT_RADIO_OVERRIDE
			sIWAnims.sIW_AnimDict = "weapons@projectile@sticky_bomb"
			sIWAnims.sPlayerAnims.sIW_Enter = "plant_vertical"
			
			sIWAnims.iNumPropsToSpawn++
			sIWAnims.sSpawnedPropAnims[0].mnIW_ModelName = INT_TO_ENUM(MODEL_NAMES, HASH("vw_Prop_VW_EX_PE_01a"))
		BREAK
		
		CASE ciINTERACT_WITH_PRESET__USE_KEYCARD
			SWITCH sInteractWithParams.iInteractWith_AltAnimSet
				CASE ciIW_ALTANIMSET__USEKEYCARD_SWIPECASINOCARD
					sIWAnims.sIW_AnimDict = "ANIM_HEIST@HS3F@IG3_CARDSWIPE@male@"
					sIWAnims.sPlayerAnims.sIW_Enter = "SUCCESS_VAR01"
					
					sIWAnims.iNumPropsToSpawn++
					sIWAnims.sSpawnedPropAnims[0].mnIW_ModelName = GET_KEYCARD_MODEL_FOR_CASINO_FINGERPRINT_SCANNER(sInteractWithParams)
					sIWAnims.sSpawnedPropAnims[0].sIW_Enter = "SUCCESS_VAR01_CARD"
				BREAK
				
				CASE ciIW_ALTANIMSET__USEKEYCARD_SYNCLOCKSTYLE_A
					IF IS_PED_WEARING_HIGH_HEELS(LocalPlayerPed)
						sIWAnims.sIW_AnimDict = "anim@heists@humane_labs@finale@keycards_heels"
					ELSE
						sIWAnims.sIW_AnimDict = "anim@heists@humane_labs@finale@keycards"
					ENDIF
					
					sIWAnims.sPlayerAnims.sIW_Enter = "ped_b_intro_b"
					sIWAnims.sPlayerAnims.sIW_Exit = "ped_b_pass"
					
					sIWAnims.iNumPropsToSpawn++
					sIWAnims.sSpawnedPropAnims[0].mnIW_ModelName = INT_TO_ENUM(MODEL_NAMES, HASH("tr_Prop_TR_Acc_Pass_01a"))
				BREAK
				
				CASE ciIW_ALTANIMSET__USEKEYCARD_SYNCLOCKSTYLE_B
					sIWAnims.sIW_AnimDict = "anim@scripted@player@mission@tun_cardswipe_insync@"
					
					sIWAnims.sPlayerAnims.sIW_Enter = "ped_b_intro_b"
					sIWAnims.sPlayerAnims.sIW_Exit = "ped_b_pass"
					
					sIWAnims.iNumPropsToSpawn++
					sIWAnims.sSpawnedPropAnims[0].mnIW_ModelName = INT_TO_ENUM(MODEL_NAMES, HASH("tr_Prop_TR_Swipe_Card_01a"))
					
				BREAK
				
				CASE ciIW_ALTANIMSET__USEKEYCARD_SYNCLOCKSTYLE_IAA
					IF IS_PED_WEARING_HIGH_HEELS(LocalPlayerPed)
						sIWAnims.sIW_AnimDict = "anim@heists@humane_labs@finale@keycards_heels"
					ELSE
						sIWAnims.sIW_AnimDict = "anim@heists@humane_labs@finale@keycards"
					ENDIF
					
					sIWAnims.sPlayerAnims.sIW_Enter = "ped_a_intro_b"
					sIWAnims.sPlayerAnims.sIW_Exit = "ped_a_pass"
					
					sIWAnims.iNumPropsToSpawn++
					sIWAnims.sSpawnedPropAnims[0].mnIW_ModelName = INT_TO_ENUM(MODEL_NAMES, HASH("tr_Prop_TR_Acc_Pass_01a"))
				BREAK
				
				CASE ciIW_ALTANIMSET__USEKEYCARD_SMALL_INSERT
					IF IS_PED_WEARING_HIGH_HEELS(LocalPlayerPed)
						sIWAnims.sIW_AnimDict = "anim@heists@humane_labs@finale@keycards_heels"
					ELSE
						sIWAnims.sIW_AnimDict = "anim@heists@humane_labs@finale@keycards"
					ENDIF
					
					sIWAnims.sPlayerAnims.sIW_Enter = "ped_a_intro_b"
					sIWAnims.sPlayerAnims.sIW_Exit = "ped_a_pass"
										
					sIWAnims.iNumPropsToSpawn++
					sIWAnims.sSpawnedPropAnims[0].mnIW_ModelName = GET_INTERACT_WITH_KEYCARD_PROP_MODEL(sInteractWithParams, INT_TO_ENUM(MODEL_NAMES, HASH("h4_Prop_h4_SecurityCard_01a")))
				BREAK
			ENDSWITCH
		BREAK
		
		CASE ciINTERACT_WITH_PRESET__CLOTHES
			SWITCH sInteractWithParams.iInteractWith_AltAnimSet
				CASE ciIW_ALTANIMSET__CLOTHES_DEFAULT
					sIWAnims.sIW_AnimDict = "ANIM_HEIST@HS3F@IG12_CHANGE_CLOTHES@"
					sIWAnims.sPlayerAnims.sIW_Enter = GET_INTERACT_WITH_CHANGE_CLOTHES_ANIMATION(sInteractWithParams)
					SET_CHANGE_CLOTHES_CAMERA_VARS(sInteractWithParams, sIWAnims)
				BREAK
				CASE ciIW_ALTANIMSET__CLOTHES_ULP
				
					IF IS_PED_FEMALE(LocalPlayerPed)
						sIWAnims.sIW_AnimDict = "ANIM@SCRIPTED@ULP_MISSIONS@CHANGE_CLOTHES@FEMALE@"
					ELSE
						sIWAnims.sIW_AnimDict = "ANIM@SCRIPTED@ULP_MISSIONS@CHANGE_CLOTHES@MALE@"
					ENDIF
					
					sIWAnims.sPlayerAnims.sIW_Enter = "CHANGE_HIGHROLLER"
					sIWAnims.sPlayerFaceAnims.sIW_Enter = "CHANGE_HIGHROLLER_FACIAL"
					
					SET_CHANGE_CLOTHES_CAMERA_VARS(sInteractWithParams, sIWAnims)
				
				BREAK
			ENDSWITCH
		BREAK
		
		CASE ciINTERACT_WITH_PRESET__HOLD_BUTTON
			sIWAnims.sIW_AnimDict = "ANIM_HEIST@HS3F@IG1_daily_storage@male@"
			sIWAnims.sPlayerAnims.sIW_Enter = "ENTER"
			sIWAnims.sPlayerAnims.sIW_Looping = "LOOP"
			sIWAnims.sPlayerAnims.sIW_Exit = "EXIT"
		BREAK
		
		CASE ciINTERACT_WITH_PRESET__CUT_OUT_PAINTING	
			GET_ANIM_ASSET_NAMES_FOR_CUT_PAINTING_INTERACT_WITH(sInteractWithParams, sIWAnims, iSubAnimPresetToUse)
		BREAK
		
		CASE ciINTERACT_WITH_PRESET__PUSH_DAILY_CASH_BUTTON
			sIWAnims.sIW_AnimDict = "ANIM_HEIST@HS3F@IG6_PUSH_BUTTON@"
			sIWAnims.sPlayerAnims.sIW_Enter = "push_button"
		BREAK
		
		CASE ciINTERACT_WITH_PRESET__PLANT_VAULT_EXPLOSIVE__LEFT
			sIWAnims.sIW_AnimDict = "ANIM_HEIST@HS3F@IG8_VAULT_EXPLOSIVES@LEFT@MALE@"
			sIWAnims.sPlayerAnims.sIW_Enter = GET_INTERACT_WITH_PLANT_VAULT_DOOR_EXPLOSIVE_ANIM_NAME(MC_playerBD_1[iLocalPart].eiInteractWith_PlantExplosivesState, TRUE)
			sIWAnims.sPlayerAnims.sIW_Exit = "PLAYER_IG8_VAULT_EXPLOSIVE_IDLE"
			
			sIWAnims.iNumPropsToSpawn = 2
			sIWAnims.sSpawnedPropAnims[0].mnIW_ModelName = INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Ch_Explosive_01a"))
			sIWAnims.sSpawnedPropAnims[1].mnIW_ModelName = INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Ch_Explosive_01a"))
			sIWAnims.sSpawnedPropAnims[0].sIW_Enter = GET_INTERACT_WITH_PLANT_VAULT_DOOR_EXPLOSIVE_ANIM_NAME(MC_playerBD_1[iLocalPart].eiInteractWith_PlantExplosivesState, FALSE, TRUE, 0)
			sIWAnims.sSpawnedPropAnims[1].sIW_Enter	= GET_INTERACT_WITH_PLANT_VAULT_DOOR_EXPLOSIVE_ANIM_NAME(MC_playerBD_1[iLocalPart].eiInteractWith_PlantExplosivesState, FALSE, TRUE, 1)
		BREAK
		
		CASE ciINTERACT_WITH_PRESET__PLANT_VAULT_EXPLOSIVE__RIGHT
			sIWAnims.sIW_AnimDict = "ANIM_HEIST@HS3F@IG8_VAULT_EXPLOSIVES@RIGHT@MALE@"
			sIWAnims.sPlayerAnims.sIW_Enter = GET_INTERACT_WITH_PLANT_VAULT_DOOR_EXPLOSIVE_ANIM_NAME(MC_playerBD_1[iLocalPart].eiInteractWith_PlantExplosivesState, TRUE)
			sIWAnims.sPlayerAnims.sIW_Exit = "PLAYER_IG8_VAULT_EXPLOSIVE_IDLE"
			
			sIWAnims.iNumPropsToSpawn = 3
			sIWAnims.sSpawnedPropAnims[0].mnIW_ModelName = INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Ch_Explosive_01a"))
			sIWAnims.sSpawnedPropAnims[1].mnIW_ModelName = INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Ch_Explosive_01a"))
			sIWAnims.sSpawnedPropAnims[2].mnIW_ModelName = INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Ch_Explosive_01a"))
			sIWAnims.sSpawnedPropAnims[0].sIW_Enter = GET_INTERACT_WITH_PLANT_VAULT_DOOR_EXPLOSIVE_ANIM_NAME(MC_playerBD_1[iLocalPart].eiInteractWith_PlantExplosivesState, FALSE, TRUE, 0)
			sIWAnims.sSpawnedPropAnims[1].sIW_Enter	= GET_INTERACT_WITH_PLANT_VAULT_DOOR_EXPLOSIVE_ANIM_NAME(MC_playerBD_1[iLocalPart].eiInteractWith_PlantExplosivesState, FALSE, TRUE, 1)
			sIWAnims.sSpawnedPropAnims[2].sIW_Enter	= GET_INTERACT_WITH_PLANT_VAULT_DOOR_EXPLOSIVE_ANIM_NAME(MC_playerBD_1[iLocalPart].eiInteractWith_PlantExplosivesState, FALSE, TRUE, 2)
		BREAK
		
		CASE ciINTERACT_WITH_PRESET__PLANT_CASINO_TUNNEL_EXPLOSIVE	
			sIWAnims.sIW_AnimDict = "ANIM_HEIST@HS3F@IG7_PLANT_BOMB@MALE@"
			sIWAnims.sPlayerAnims.sIW_Enter = "plant_bomb"
			
			sIWAnims.iNumPropsToSpawn++
			sIWAnims.sSpawnedPropAnims[0].mnIW_ModelName = INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Ch_LD_Bomb_01a"))
			sIWAnims.sSpawnedPropAnims[0].sIW_Enter = "PLANT_BOMB_PROP"
		BREAK
		
		CASE ciINTERACT_WITH_PRESET__SEARCH_FLOOR_GENERIC	
			sIWAnims.sIW_AnimDict = "anim@GangOps@Facility@Servers@BodySearch@"
			sIWAnims.sPlayerAnims.sIW_Enter = "Player_Search"
		BREAK
		
		CASE ciINTERACT_WITH_PRESET__OPEN_CRATE
			SWITCH sInteractWithParams.iInteractWith_AltAnimSet
				CASE ciIW_ALTANIMSET__OPENCRATE_DEFAULT
					sIWAnims.sIW_AnimDict = "anim@mp_mission@dr_objective"
					sIWAnims.sPlayerAnims.sIW_Enter = "player_fail"
				BREAK
				
				CASE ciIW_ALTANIMSET__OPENCRATE_MONEY_PLATE
					IF IS_PED_WEARING_HIGH_HEELS(LocalPlayerPed)
						sIWAnims.sIW_AnimDict = "anim@scripted@player@mission@tunf_conv_ig1_monyplate@heeled@"
					ELSE
						sIWAnims.sIW_AnimDict = "anim@scripted@player@mission@tunf_conv_ig1_monyplate@male@"
					ENDIF
					
					sIWAnims.sPlayerAnims.sIW_Enter = "action"
					sIWAnims.sPlayerFaceAnims.sIW_Enter = "action_facial"
					
					sIWAnims.sInteractWithEntityAnims.sIW_Enter = "action_case"
					
					sIWAnims.iNumPropsToSpawn++
					sIWAnims.sSpawnedPropAnims[0].mnIW_ModelName = GET_INTERACT_WITH_BAG_MODEL_TO_USE()
					sIWAnims.sSpawnedPropAnims[0].sIW_Enter = "action_bag"
					
					sIWAnims.iNumPropsToSpawn++
					sIWAnims.sSpawnedPropAnims[1].mnIW_ModelName = INT_TO_ENUM(MODEL_NAMES, HASH("tr_Prop_TR_Carry_Box_01a"))
					sIWAnims.sSpawnedPropAnims[1].sIW_Enter = "action_carry_box"
				BREAK
				
				CASE ciIW_ALTANIMSET__OPENCRATE_SEARCH_EMPTY_CRATE
					IF IS_PED_WEARING_HIGH_HEELS(LocalPlayerPed)
						sIWAnims.sIW_AnimDict = "ANIM@SCRIPTED@ULP_MISSIONS@EMPTY_CRATE@HEELED@"
					ELSE
						sIWAnims.sIW_AnimDict = "ANIM@SCRIPTED@ULP_MISSIONS@EMPTY_CRATE@MALE@"
					ENDIF
					
					sIWAnims.sPlayerAnims.sIW_Enter = "ACTION"
					sIWAnims.sInteractWithEntityAnims.sIW_Enter = "ACTION_CRATE"
				BREAK
				
				CASE ciIW_ALTANIMSET__OPENCRATE_SEARCH_WOODEN_CRATE
					//THIS IS PLACEHOLDER AWAITING ANIMS FOR XMAS 2022 - 14/09/22
					IF IS_PED_WEARING_HIGH_HEELS(LocalPlayerPed)
						sIWAnims.sIW_AnimDict = "ANIM@SCRIPTED@ULP_MISSIONS@EMPTY_CRATE@HEELED@"
					ELSE
						sIWAnims.sIW_AnimDict = "ANIM@SCRIPTED@ULP_MISSIONS@EMPTY_CRATE@MALE@"
					ENDIF
					
					sIWAnims.sPlayerAnims.sIW_Enter = "ACTION"
					sIWAnims.sInteractWithEntityAnims.sIW_Enter = "ACTION_CRATE"
				BREAK
			ENDSWITCH
		BREAK
		
		CASE ciINTERACT_WITH_PRESET__SYNC_LOCK_PANEL
			IF IS_PED_WEARING_HIGH_HEELS(LocalPlayerPed)
				sIWAnims.sIW_AnimDict = "anim@heists@humane_labs@finale@keycards_heels"
			ELSE
				sIWAnims.sIW_AnimDict = "anim@heists@humane_labs@finale@keycards"
			ENDIF
			
			sIWAnims.iNumPropsToSpawn++
			sIWAnims.sSpawnedPropAnims[0].mnIW_ModelName = INT_TO_ENUM(MODEL_NAMES, HASH("h4_Prop_h4_SecurityCard_01a"))
			
			SWITCH sInteractWithParams.iInteractWith_AltAnimSet
				CASE ciIW_ALTANIMSET__SYNCLOCK_LEFT
					SWITCH iSubAnimPresetToUse
						CASE ciIW_SUBANIM__SYNCLOCK_ENTER
							sIWAnims.sPlayerAnims.sIW_Enter = "ped_a_enter"
							sIWAnims.sPlayerAnims.sIW_Looping = "ped_a_enter_loop"
							sIWAnims.sPlayerAnims.sIW_Exit = "ped_a_exit"
						BREAK
						
						CASE ciIW_SUBANIM__SYNCLOCK_ENGAGE
							sIWAnims.sPlayerAnims.sIW_Enter = "ped_a_intro_b"
							sIWAnims.sPlayerAnims.sIW_Looping = "ped_a_loop"
						BREAK
						
						CASE ciIW_SUBANIM__SYNCLOCK_FAIL
							sIWAnims.sPlayerAnims.sIW_Enter = "ped_a_fail_d"
							sIWAnims.sPlayerAnims.sIW_Looping = "ped_a_enter_loop"
							sIWAnims.sPlayerAnims.sIW_Exit = "ped_a_exit"
						BREAK
						
						CASE ciIW_SUBANIM__SYNCLOCK_SUCCESS
							sIWAnims.sPlayerAnims.sIW_Enter = "ped_a_pass"
						BREAK
					ENDSWITCH
				BREAK
				
				CASE ciIW_ALTANIMSET__SYNCLOCK_RIGHT
					SWITCH iSubAnimPresetToUse
						CASE ciIW_SUBANIM__SYNCLOCK_ENTER
							sIWAnims.sPlayerAnims.sIW_Enter = "ped_b_enter"
							sIWAnims.sPlayerAnims.sIW_Looping = "ped_b_enter_loop"
							sIWAnims.sPlayerAnims.sIW_Exit = "ped_b_exit"
						BREAK
						
						CASE ciIW_SUBANIM__SYNCLOCK_ENGAGE
							sIWAnims.sPlayerAnims.sIW_Enter = "ped_b_intro_b"
							sIWAnims.sPlayerAnims.sIW_Looping = "ped_b_loop"
						BREAK
						
						CASE ciIW_SUBANIM__SYNCLOCK_FAIL
							sIWAnims.sPlayerAnims.sIW_Enter = "ped_b_fail_b"
							sIWAnims.sPlayerAnims.sIW_Looping = "ped_b_enter_loop"
							sIWAnims.sPlayerAnims.sIW_Exit = "ped_b_exit"
						BREAK
						
						CASE ciIW_SUBANIM__SYNCLOCK_SUCCESS
							sIWAnims.sPlayerAnims.sIW_Enter = "ped_b_pass"
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE ciINTERACT_WITH_PRESET__POWER_BOX_LEVER	
			sIWAnims.sIW_AnimDict = "ANIM@SCRIPTED@HEIST@IG2_HAND_LEVER@MALE@"
			sIWAnims.sPlayerAnims.sIW_Enter = "ENTER"
			sIWAnims.sPlayerAnims.sIW_Looping = "IDLE"
			sIWAnims.sPlayerAnims.sIW_Exit = "EXIT"
			sIWAnims.sPlayerFaceAnims.sIW_Enter = "ENTER_MALE_FACIAL"
			sIWAnims.sPlayerFaceAnims.sIW_Looping = "IDLE_MALE_FACIAL"
			sIWAnims.sPlayerFaceAnims.sIW_Exit = "EXIT_MALE_FACIAL"
			
			sIWAnims.sInteractWithEntityAnims.sIW_Enter = "ENTER_LEVER"
			sIWAnims.sInteractWithEntityAnims.sIW_Looping = "IDLE_LEVER"
			sIWAnims.sInteractWithEntityAnims.sIW_Exit = "EXIT_LEVER"
		BREAK
		
		CASE ciINTERACT_WITH_PRESET__USE_KEY_ON_DOOR
			
			SWITCH sInteractWithParams.iInteractWith_AltAnimSet
				CASE ciIW_ALTANIMSET__USEKEYONDOOR_ISLAND_GATE
					IF SHOULD_PLAYER_DO_BACK_SIDE_INTERACT_WITH(sInteractWithParams)
						sIWAnims.sIW_AnimDict = "ANIM@SCRIPTED@HEIST@IG13_JAILOR_KEY_TURN@MALE@"
					ELSE
						sIWAnims.sIW_AnimDict = "ANIM@SCRIPTED@HEIST@IG13_JAILOR_KEY_TURN_REVERSE@MALE@"
					ENDIF
					
					sIWAnims.sPlayerAnims.sIW_Enter = "ACTION"
					
					sIWAnims.iNumPropsToSpawn++
					sIWAnims.sSpawnedPropAnims[0].mnIW_ModelName = INT_TO_ENUM(MODEL_NAMES, HASH("h4_Prop_h4_Keys_Jail_01a"))
					sIWAnims.sSpawnedPropAnims[0].sIW_Enter = "ACTION_Keys"
				BREAK
				
				CASE ciIW_ALTANIMSET__USEKEYONDOOR_GENERIC
					IF SHOULD_PLAYER_DO_BACK_SIDE_INTERACT_WITH(sInteractWithParams)
						sIWAnims.sIW_AnimDict = "ANIM@SCRIPTED@HEIST@IG13_JAILOR_KEY_TURN@GENERIC@MALE@"
					ELSE
						sIWAnims.sIW_AnimDict = "ANIM@SCRIPTED@HEIST@IG13_JAILOR_KEY_TURN_REVERSE@GENERIC@MALE@"
					ENDIF
					
					sIWAnims.sPlayerAnims.sIW_Enter = "ACTION"
					
					sIWAnims.iNumPropsToSpawn++
					sIWAnims.sSpawnedPropAnims[0].mnIW_ModelName = INT_TO_ENUM(MODEL_NAMES, HASH("h4_Prop_h4_Keys_Jail_01a"))
					sIWAnims.sSpawnedPropAnims[0].sIW_Enter = "ACTION_Keys"
				BREAK
			ENDSWITCH
		BREAK
		
		CASE ciINTERACT_WITH_PRESET__DEAL_WITH_CHAIN_LOCK
		
			sIWAnims.sInteractWithEntityAnims.bPersistent = TRUE
			
			SWITCH sInteractWithParams.iInteractWith_AltAnimSet
				CASE ciIW_ALTANIMSET__DEALWITHCHAIN_EXPLOSIVE
					sIWAnims.sIW_AnimDict = "ANIM@SCRIPTED@HEIST@IG6_EXPLOSIVE_PLANT@MALE@"
					sIWAnims.sPlayerAnims.sIW_Enter = "PLANT_male"
					
					sIWAnims.sInteractWithEntityAnims.sIW_Enter = "plant_chain_lock"
					sIWAnims.sInteractWithEntityAnims.bPersistent = TRUE
					
					sIWAnims.iNumPropsToSpawn++
					sIWAnims.sSpawnedPropAnims[0].mnIW_ModelName = INT_TO_ENUM(MODEL_NAMES, HASH("h4_Prop_h4_Exp_Device_01a"))
					sIWAnims.sSpawnedPropAnims[0].sIW_Enter = "plant_exp_device"
					sIWAnims.sSpawnedPropAnims[0].bPersistent = TRUE
				BREAK
				
				CASE ciIW_ALTANIMSET__DEALWITHCHAIN_BOLT_CUTTERS
					sIWAnims.sIW_AnimDict = "ANIM@SCRIPTED@HEIST@IG4_BOLT_CUTTERS@MALE@"
					sIWAnims.sPlayerAnims.sIW_Enter = "ACTION_MALE"
					
					sIWAnims.sInteractWithEntityAnims.sIW_Enter = "ACTION_CHAIN"
					sIWAnims.sInteractWithEntityAnims.bPersistent = TRUE
					
					sIWAnims.iNumPropsToSpawn++
					sIWAnims.sSpawnedPropAnims[0].mnIW_ModelName = INT_TO_ENUM(MODEL_NAMES, HASH("h4_Prop_h4_Bolt_Cutter_01a"))
					sIWAnims.sSpawnedPropAnims[0].sIW_Enter = "ACTION_CUTTER"
					
					sIWAnims.iNumPropsToSpawn++
					sIWAnims.sSpawnedPropAnims[1].mnIW_ModelName = GET_INTERACT_WITH_BAG_MODEL_TO_USE()
					sIWAnims.sSpawnedPropAnims[1].sIW_Enter = "ACTION_BAG"
				BREAK
				
				CASE ciIW_ALTANIMSET__DEALWITHCHAIN_TORCH
					sIWAnims.sIW_AnimDict = "ANIM@SCRIPTED@HEIST@IG5_TORCH_LOCK@MALE@"
					sIWAnims.sPlayerAnims.sIW_Enter = "ACTION_MALE"
					
					sIWAnims.sInteractWithEntityAnims.sIW_Enter = "ACTION_CHAINLOCK"
					sIWAnims.sInteractWithEntityAnims.bPersistent = TRUE
						
					sIWAnims.iNumPropsToSpawn++
					sIWAnims.sSpawnedPropAnims[0].mnIW_ModelName = INT_TO_ENUM(MODEL_NAMES, HASH("h4_Prop_h4_GasCutter_01a"))
					sIWAnims.sSpawnedPropAnims[0].sIW_Enter = "ACTION_GASCUTTER"
					
					sIWAnims.iNumPropsToSpawn++
					sIWAnims.sSpawnedPropAnims[1].mnIW_ModelName = GET_INTERACT_WITH_BAG_MODEL_TO_USE()
					sIWAnims.sSpawnedPropAnims[1].sIW_Enter = "ACTION_BAG"
				BREAK
				
				CASE ciIW_ALTANIMSET__DEALWITHCHAIN_CUTTER_AND_CONTAINER
				
					IF IS_PED_WEARING_HIGH_HEELS(LocalPlayerPed)
						sIWAnims.sIW_AnimDict = "ANIM@SCRIPTED@PLAYER@MISSION@TUNF_TRAIN_IG1_CONTAINER_P1@HEELED@"
					ELSE
						sIWAnims.sIW_AnimDict = "ANIM@SCRIPTED@PLAYER@MISSION@TUNF_TRAIN_IG1_CONTAINER_P1@MALE@"
					ENDIF
					
					sIWAnims.sPlayerAnims.sIW_Enter = "ACTION"
					
					sIWAnims.sInteractWithEntityAnims.sIW_Enter = "ACTION_LOCK"
					sIWAnims.sInteractWithEntityAnims.bPersistent = TRUE
										
					sIWAnims.iNumPropsToSpawn++
					sIWAnims.sSpawnedPropAnims[0].mnIW_ModelName = GET_INTERACT_WITH_ANGLE_GRINDER_PROP_MODEL(sInteractWithParams, tr_prop_tr_grinder_01a)
					sIWAnims.sSpawnedPropAnims[0].sIW_Enter = "ACTION_ANGLE_GRINDER"
					
					sIWAnims.iNumPropsToSpawn++
					sIWAnims.sSpawnedPropAnims[1].mnIW_ModelName = GET_INTERACT_WITH_BAG_MODEL_TO_USE() 	
					sIWAnims.sSpawnedPropAnims[1].sIW_Enter = "ACTION_BAG"
					
					sIWAnims.sInteractWithLinkedDynopropAnims.bPersistent = TRUE
					sIWAnims.sInteractWithLinkedDynopropAnims.bOptional = TRUE
					sIWAnims.sInteractWithLinkedDynopropAnims.sIW_Enter = "action_container"
					
					sIWAnims.sInteractWithIncludedPTFXAsset = "scr_tn_tr"
					
				BREAK
			ENDSWITCH
		BREAK
		
		CASE ciINTERACT_WITH_PRESET__OPEN_SAFE_DOOR	
			sIWAnims.sIW_AnimDict = "ANIM@AMB@OFFICE@BOSS@VAULT@LEFT@MALE@"
			sIWAnims.sPlayerAnims.sIW_Enter = "OPEN"
			
			sIWAnims.sInteractWithEntityAnims.sIW_Enter = "OPEN_DOOR"
			sIWAnims.sInteractWithEntityAnims.bPersistent = TRUE
		BREAK
		
		CASE ciINTERACT_WITH_PRESET__PICK_UP_GUN
			SWITCH sInteractWithParams.iInteractWith_AltAnimSet
				CASE ciIW_ALTANIMSET__PICKUPGUN_SHOTGUN
					sIWAnims.sIW_AnimDict = "ANIM@SCRIPTED@HEIST@IG23_SHOTGUN@MALE@"
					sIWAnims.sPlayerAnims.sIW_Enter = "ACTION"
					sIWAnims.sPlayerFaceAnims.sIW_Enter = "ACTION_FACIAL"
					
					sIWAnims.sCameraAnims.sIW_Enter = "ACTION_CAMERA"
					sIWAnims.sCameraVars.iLerpIn = 0
					sIWAnims.sCameraVars.iLerpOut = 0
			
					sIWAnims.sInteractWithEntityAnims.sIW_Enter = "ACTION_SHOTGUN"
				BREAK
				
				CASE ciIW_ALTANIMSET__PICKUPGUN_GOLDEN_GUN
					sIWAnims.sIW_AnimDict = "ANIM@SCRIPTED@HEIST@IG22_GOLDEN_GUN@MALE@"
					sIWAnims.sPlayerAnims.sIW_Enter = "ACTION"
					
					sIWAnims.sInteractWithEntityAnims.sIW_Enter = "ACTION_W_PI_SINGLESHOTH4"
					
					sIWAnims.iNumDynopropsToUse++
					sIWAnims.sNearbyDynopropAnims[0].mnIW_ModelName = INT_TO_ENUM(MODEL_NAMES, HASH("h4_prop_office_desk_01"))
					sIWAnims.sNearbyDynopropAnims[0].sIW_Enter = "ACTION_H4_PROP_OFFICE_DESK_01"
					
					sIWAnims.sCameraAnims.sIW_Enter = "ACTION_CAMERA"
					sIWAnims.sCameraVars.iLerpIn = 0
					sIWAnims.sCameraVars.iLerpOut = 0
					
					sIWAnims.iNumPropsToSpawn++
					sIWAnims.sSpawnedPropAnims[0].mnIW_ModelName = INT_TO_ENUM(MODEL_NAMES, HASH("h4_Prop_h4_Key_Desk_01"))
					sIWAnims.sSpawnedPropAnims[0].sIW_Enter = "ACTION_H4_PROP_H4_KEY_DESK_01"
				BREAK
			ENDSWITCH
		BREAK
		
		CASE ciINTERACT_WITH_PRESET__VOLTAGE_MINIGAME
			IF IS_PED_WEARING_HIGH_HEELS(LocalPlayerPed)
				sIWAnims.sIW_AnimDict = "anim@scripted@player@mission@tun_control_tower@heeled@"
			ELSE
				sIWAnims.sIW_AnimDict = "anim@scripted@player@mission@tun_control_tower@male@"
			ENDIF
			
			sIWAnims.sPlayerAnims.sIW_Enter = "ENTER"
			sIWAnims.sPlayerAnims.sIW_Looping = "loop"
			sIWAnims.sPlayerAnims.sIW_Exit = "EXIT"		
			sIWAnims.sPlayerFaceAnims.sIW_Looping = "loop_facial"
			sIWAnims.sInteractWithEntityAnims.sIW_Enter = "enter_electric_box"
			sIWAnims.sInteractWithEntityAnims.sIW_Looping = "loop_electric_box"
			sIWAnims.sInteractWithEntityAnims.sIW_Exit = "exit_electric_box"			
		BREAK
		
		CASE ciINTERACT_WITH_PRESET__FINGERPRINT_MINIGAME
			sIWAnims.sIW_AnimDict = "anim@scripted@heist@ig14_elevator_hack@male@"
			sIWAnims.sPlayerAnims.sIW_Enter = "enter"
			sIWAnims.sPlayerAnims.sIW_Looping = "hack_loop"
			sIWAnims.sPlayerAnims.sIW_Exit = "exit"
			
			sIWAnims.sPlayerFaceAnims.sIW_Enter = "enter_facial"
			sIWAnims.sPlayerFaceAnims.sIW_Looping = "hack_loop_facial"
			sIWAnims.sPlayerFaceAnims.sIW_Exit = "exit_facial"
			
			// Camera anims disabled for now - they just put the camera into the wall and were likely designed around a different location
			//sIWAnims.sCameraAnims.sIW_Enter = "enter_cam"
			//sIWAnims.sCameraAnims.sIW_Looping = "hack_loop_cam"
			//sIWAnims.sCameraAnims.sIW_Exit = "exit_cam"
			
			sIWAnims.sInteractWithEntityAnims.sIW_Enter = "enter_hei_prop_hei_keypad_01"
			sIWAnims.sInteractWithEntityAnims.sIW_Looping = "hack_loop_hei_prop_hei_keypad_01"
			sIWAnims.sInteractWithEntityAnims.sIW_Exit = "exit_hei_prop_hei_keypad_01"
			
			sIWAnims.iNumPropsToSpawn++
			sIWAnims.sSpawnedPropAnims[0].mnIW_ModelName = INT_TO_ENUM(MODEL_NAMES, HASH("h4_Prop_h4_Card_Hack_01a"))
			sIWAnims.sSpawnedPropAnims[0].sIW_Enter = "ENTER_h4_Prop_h4_Card_Hack_01a"
			sIWAnims.sSpawnedPropAnims[0].sIW_Looping = "hack_loop_h4_Prop_h4_Card_Hack_01a"
			sIWAnims.sSpawnedPropAnims[0].sIW_Exit = "exit_h4_Prop_h4_Card_Hack_01a"		
			
			sIWAnims.iNumPropsToSpawn++
			sIWAnims.sSpawnedPropAnims[1].mnIW_ModelName = INT_TO_ENUM(MODEL_NAMES, HASH("prop_phone_ing"))
			sIWAnims.sSpawnedPropAnims[1].sIW_Enter = "enter_prop_phone_ing"
			sIWAnims.sSpawnedPropAnims[1].sIW_Looping = "hack_loop_prop_phone_ing"
			sIWAnims.sSpawnedPropAnims[1].sIW_Exit = "exit_prop_phone_ing"
		BREAK
		
		CASE ciINTERACT_WITH_PRESET__ENTER_SAFE_COMBINATION_MINIGAME
			sIWAnims.sIW_AnimDict = "ANIM@SCRIPTED@HEIST@IG15_SAFE_CRACK@MALE@"
			
			sIWAnims.iNumPropsToSpawn++
			sIWAnims.sSpawnedPropAnims[0].mnIW_ModelName = GET_INTERACT_WITH_BAG_MODEL_TO_USE()
				
			SWITCH sInteractWithParams.iInteractWith_AltAnimSet
				CASE ciIW_ALTANIMSET__ENTERSAFECOMBINATION_METAL_CYLINDER
					sIWAnims.iNumPropsToSpawn++
					sIWAnims.sSpawnedPropAnims[1].mnIW_ModelName = INT_TO_ENUM(MODEL_NAMES, HASH("h4_Prop_h4_File_Cylinder_01a"))
				BREAK
				
				CASE ciIW_ALTANIMSET__ENTERSAFECOMBINATION_DOCUMENTS
					sIWAnims.iNumPropsToSpawn++
					sIWAnims.sSpawnedPropAnims[1].mnIW_ModelName = INT_TO_ENUM(MODEL_NAMES, HASH("h4_Prop_h4_Cash_Bon_01a"))
				BREAK
			ENDSWITCH
			
			sIWAnims.sSpawnedPropAnims[1].bHideDuringThisSubAnim = TRUE // Only unhide it for the very last anims
			
			sIWAnims.sCameraVars.iLerpOut = 1000
			
			SWITCH iSubAnimPresetToUse
				CASE ciIW_SUBANIM__SAFECRACK_ENTER
					sIWAnims.sPlayerAnims.sIW_Enter = "enter_player"
					sIWAnims.sPlayerAnims.sIW_Looping = "idle_player"	
					
					sIWAnims.sPlayerFaceAnims.sIW_Enter = "enter_player_facial"
					sIWAnims.sPlayerFaceAnims.sIW_Looping = "idle_player_facial"
					
					sIWAnims.sInteractWithEntityAnims.sIW_Enter = "enter_safe"
					sIWAnims.sInteractWithEntityAnims.sIW_Looping = "idle_safe"
					
					sIWAnims.sCameraAnims.sIW_Enter = "enter_cam"
					sIWAnims.sCameraAnims.sIW_Looping = "idle_cam"					
					
					sIWAnims.sSpawnedPropAnims[0].sIW_Enter = "enter_bag"		
					sIWAnims.sSpawnedPropAnims[0].sIW_Looping = "idle_bag"	
				BREAK
				CASE ciIW_SUBANIM__SAFECRACK_IDLE
					sIWAnims.sPlayerAnims.sIW_Enter = "idle_player"
					sIWAnims.sPlayerAnims.sIW_Looping = "idle_player"	
					
					sIWAnims.sPlayerFaceAnims.sIW_Enter = "idle_player_facial"
					sIWAnims.sPlayerFaceAnims.sIW_Looping = "idle_player_facial"
					
					sIWAnims.sInteractWithEntityAnims.sIW_Enter = "idle_safe"
					sIWAnims.sInteractWithEntityAnims.sIW_Looping = "idle_safe"
					
					sIWAnims.sCameraAnims.sIW_Enter = "idle_cam"
					sIWAnims.sCameraAnims.sIW_Looping = "idle_cam"			
					
					sIWAnims.sSpawnedPropAnims[0].sIW_Enter = "idle_bag"		
					sIWAnims.sSpawnedPropAnims[0].sIW_Looping = "idle_bag"	
				BREAK
				CASE ciIW_SUBANIM__SAFECRACK_INPUT_SAFE_CODE_LEFT
					sIWAnims.sPlayerAnims.sIW_Enter = "input_safe_code_left_player"
					sIWAnims.sPlayerAnims.sIW_Looping = "idle_player"					
									
					sIWAnims.sPlayerFaceAnims.sIW_Enter = "input_safe_code_left_player_facial"
					sIWAnims.sPlayerFaceAnims.sIW_Looping = "idle_player_facial"
					
					sIWAnims.sInteractWithEntityAnims.sIW_Enter = "input_safe_code_left_safe"
					sIWAnims.sInteractWithEntityAnims.sIW_Looping = "idle_safe"
					
					sIWAnims.sCameraAnims.sIW_Enter = "input_safe_code_left_cam"
					sIWAnims.sCameraAnims.sIW_Looping = "idle_cam"
					
					sIWAnims.sSpawnedPropAnims[0].sIW_Enter = "input_safe_code_left_bag"		
					sIWAnims.sSpawnedPropAnims[0].sIW_Looping = "idle_bag"			
				BREAK
				CASE ciIW_SUBANIM__SAFECRACK_INPUT_SAFE_CODE_RIGHT
					sIWAnims.sPlayerAnims.sIW_Enter = "input_safe_code_right_player"
					sIWAnims.sPlayerAnims.sIW_Looping = "idle_player"
					
					sIWAnims.sPlayerFaceAnims.sIW_Enter = "input_safe_code_right_player_facial"
					sIWAnims.sPlayerFaceAnims.sIW_Looping = "idle_player_facial"
					
					sIWAnims.sInteractWithEntityAnims.sIW_Enter = "input_safe_code_right_safe"
					sIWAnims.sInteractWithEntityAnims.sIW_Looping = "idle_safe"
					
					sIWAnims.sCameraAnims.sIW_Enter = "input_safe_code_right_cam"
					sIWAnims.sCameraAnims.sIW_Looping = "idle_cam"
							
					sIWAnims.sSpawnedPropAnims[0].sIW_Enter = "input_safe_code_right_bag"		
					sIWAnims.sSpawnedPropAnims[0].sIW_Looping = "idle_bag"	
				BREAK		
				CASE ciIW_SUBANIM__SAFECRACK_EXIT
					sIWAnims.sPlayerAnims.sIW_Enter = "exit_player"
					sIWAnims.sPlayerFaceAnims.sIW_Enter = "exit_player_facial"
					sIWAnims.sCameraAnims.sIW_Enter = "exit_cam"
					
					sIWAnims.sSpawnedPropAnims[0].sIW_Enter = "exit_bag"	
				BREAK
				
				CASE ciIW_SUBANIM__SAFECRACK_FAIL
					sIWAnims.sPlayerAnims.sIW_Enter = "fail_player"
					sIWAnims.sPlayerFaceAnims.sIW_Enter = "fail_player_facial"					
					sIWAnims.sInteractWithEntityAnims.sIW_Enter  = "fail_safe"
					sIWAnims.sCameraAnims.sIW_Enter = "fail_cam"					
					sIWAnims.sSpawnedPropAnims[0].sIW_Enter = "fail_bag"		
				BREAK
				
				CASE ciIW_SUBANIM__SAFECRACK_SUCCESS
					SWITCH sInteractWithParams.iInteractWith_AltAnimSet
						CASE ciIW_ALTANIMSET__ENTERSAFECOMBINATION_METAL_CYLINDER
							sIWAnims.sPlayerAnims.sIW_Enter = "door_open_player"
							sIWAnims.sPlayerAnims.sIW_Exit = "success_with_metal_cylinder_player"					
								
							sIWAnims.sPlayerFaceAnims.sIW_Enter = "idle_player_facial"
							sIWAnims.sPlayerFaceAnims.sIW_Exit = "success_with_metal_cylinder_player_facial"
							
							sIWAnims.sInteractWithEntityAnims.bPersistent = TRUE
							sIWAnims.sInteractWithEntityAnims.sIW_Enter = "door_open_safe"
							sIWAnims.sInteractWithEntityAnims.sIW_Exit = "success_with_metal_cylinder_safe"		
							
							sIWAnims.sCameraAnims.sIW_Enter = "door_open_cam"
							sIWAnims.sCameraAnims.sIW_Exit = "success_with_metal_cylinder_cam"			
							
							sIWAnims.sSpawnedPropAnims[0].sIW_Enter = "door_open_bag" 
							sIWAnims.sSpawnedPropAnims[0].sIW_Exit = "success_with_metal_cylinder_bag"					
						
							sIWAnims.sSpawnedPropAnims[1].sIW_Enter = "door_open_file_cylinder"
							sIWAnims.sSpawnedPropAnims[1].sIW_Exit = "success_with_metal_cylinder_file_cylinder"
							sIWAnims.sSpawnedPropAnims[1].bHideDuringThisSubAnim = FALSE // Only unhide it for the very last anims
						BREAK
						
						CASE ciIW_ALTANIMSET__ENTERSAFECOMBINATION_DOCUMENTS
							sIWAnims.sPlayerAnims.sIW_Enter = "door_open_player"
							sIWAnims.sPlayerAnims.sIW_Exit = "success_with_stack_bonds_player"					
								
							sIWAnims.sPlayerFaceAnims.sIW_Enter = "door_open_player_facial"
							sIWAnims.sPlayerFaceAnims.sIW_Exit = "success_with_stack_bonds_player_facial"
												
							sIWAnims.sInteractWithEntityAnims.bPersistent = TRUE
							sIWAnims.sInteractWithEntityAnims.sIW_Enter = "door_open_safe"
							sIWAnims.sInteractWithEntityAnims.sIW_Exit = "success_with_stack_bonds_safe"		
							
							sIWAnims.sCameraAnims.sIW_Enter = "door_open_cam"
							sIWAnims.sCameraAnims.sIW_Exit = "success_with_stack_bonds_cam"						
							
							sIWAnims.sSpawnedPropAnims[0].sIW_Enter = "door_open_bag"
							sIWAnims.sSpawnedPropAnims[0].sIW_Exit = "success_with_stack_bonds_bag"		
							
							sIWAnims.sSpawnedPropAnims[1].sIW_Enter = "door_open_cash_bond"
							sIWAnims.sSpawnedPropAnims[1].sIW_Exit = "success_with_stack_bonds_cash_bond"
							sIWAnims.sSpawnedPropAnims[1].bHideDuringThisSubAnim = FALSE // Only unhide it for the very last anims
						BREAK
					ENDSWITCH
				BREAK

			ENDSWITCH
		BREAK
		
		CASE ciINTERACT_WITH_PRESET__GLASS_CUTTING
			sIWAnims.sIW_AnimDict = "ANIM@SCRIPTED@HEIST@IG16_GLASS_CUT@MALE@"
			
			sIWAnims.iNumPropsToSpawn++
			sIWAnims.sSpawnedPropAnims[0].mnIW_ModelName = GET_INTERACT_WITH_BAG_MODEL_TO_USE()
				
			sIWAnims.iNumPropsToSpawn++
			sIWAnims.sSpawnedPropAnims[1].mnIW_ModelName = INT_TO_ENUM(MODEL_NAMES, HASH("h4_Prop_h4_Cutter_01a"))
			sIWAnims.sSpawnedPropAnims[1].bPersistent = TRUE
			
			sIWAnims.iNumPropsToSpawn++
			sIWAnims.sSpawnedPropAnims[2].mnIW_ModelName = INT_TO_ENUM(MODEL_NAMES, HASH("h4_Prop_h4_Glass_Cut_01a"))
			sIWAnims.sSpawnedPropAnims[2].bHideDuringThisSubAnim = TRUE // Only unhide it for the very last anims
			
			//sIWAnims.iNumPropsToSpawn++
			//sIWAnims.sSpawnedPropAnims[3].mnIW_ModelName = INT_TO_ENUM(MODEL_NAMES, HASH("h4_Prop_h4_Glass_Disp_01b"))
			//sIWAnims.sSpawnedPropAnims[3].bHideDuringThisSubAnim = TRUE // Set to FALSE at the end
						
			SWITCH iSubAnimPresetToUse
				
				CASE ciIW_SUBANIM__GLASSCUT_ENTER
					sIWAnims.sPlayerAnims.sIW_Enter = "enter"
					
					sIWAnims.sPlayerFaceAnims.sIW_Enter = "enter_facial"
					
					sIWAnims.sInteractWithEntityAnims.sIW_Enter = "enter_glass_display"
					
					sIWAnims.sCameraAnims.sIW_Enter = "enter_cam"
						
					sIWAnims.sSpawnedPropAnims[0].sIW_Enter = "enter_bag"
					sIWAnims.sSpawnedPropAnims[1].sIW_Enter = "enter_cutter"		
					//sIWAnims.sSpawnedPropAnims[2].sIW_Enter = "enter_glass_piece"	
				BREAK
				
				CASE ciIW_SUBANIM__GLASSCUT_ENTER_ALT
					sIWAnims.sPlayerAnims.sIW_Enter = "enter_alt"
					
					sIWAnims.sPlayerFaceAnims.sIW_Enter = "enter_alt_facial"
					
					sIWAnims.sInteractWithEntityAnims.sIW_Enter = "enter_alt_glass_display"
					
					sIWAnims.sCameraAnims.sIW_Enter = "enter_alt_cam"
						
					sIWAnims.sSpawnedPropAnims[0].sIW_Enter = "enter_alt_bag"		
					
					sIWAnims.sSpawnedPropAnims[1].sIW_Enter = "enter_alt_cutter"		
					
					//sIWAnims.sSpawnedPropAnims[2].sIW_Enter = "enter_alt_glass_piece"	
				BREAK
				
				CASE ciIW_SUBANIM__GLASSCUT_IDLE
					sIWAnims.sPlayerAnims.sIW_Enter = "idle"
					sIWAnims.sPlayerAnims.sIW_Looping = "idle"	
					
					sIWAnims.sPlayerFaceAnims.sIW_Enter = "idle_facial"
					sIWAnims.sPlayerFaceAnims.sIW_Looping = "idle_facial"
					
					sIWAnims.sInteractWithEntityAnims.sIW_Enter = "idle_glass_display"
					sIWAnims.sInteractWithEntityAnims.sIW_Looping = "idle_glass_display"
					
					sIWAnims.sCameraAnims.sIW_Enter = "idle_cam"
					sIWAnims.sCameraAnims.sIW_Looping = "idle_cam"			
					
					sIWAnims.sSpawnedPropAnims[0].sIW_Enter = "idle_bag"		
					sIWAnims.sSpawnedPropAnims[0].sIW_Looping = "idle_bag"	
					
					sIWAnims.sSpawnedPropAnims[1].sIW_Enter = "idle_cutter"		
					sIWAnims.sSpawnedPropAnims[1].sIW_Looping = "idle_cutter"	
					
					//sIWAnims.sSpawnedPropAnims[2].sIW_Enter = "idle_glass_piece"		
					//sIWAnims.sSpawnedPropAnims[2].sIW_Looping = "idle_glass_piece"
				BREAK
				
				CASE ciIW_SUBANIM__GLASSCUT_CUTTING
					sIWAnims.sPlayerAnims.sIW_Enter = "CUTTING_LOOP"
					sIWAnims.sPlayerAnims.sIW_Looping = "CUTTING_LOOP"	
					
					sIWAnims.sPlayerFaceAnims.sIW_Enter = "cutting_loop_facial"
					sIWAnims.sPlayerFaceAnims.sIW_Looping = "cutting_loop_facial"
					
					sIWAnims.sInteractWithEntityAnims.sIW_Enter = "cutting_loop_glass_display"
					sIWAnims.sInteractWithEntityAnims.sIW_Looping = "cutting_loop_glass_display"
					
					sIWAnims.sCameraAnims.sIW_Enter = "cutting_loop_cam"
					sIWAnims.sCameraAnims.sIW_Looping = "cutting_loop_cam"			
					
					sIWAnims.sSpawnedPropAnims[0].sIW_Enter = "cutting_loop_bag"		
					sIWAnims.sSpawnedPropAnims[0].sIW_Looping = "cutting_loop_bag"	
					
					sIWAnims.sSpawnedPropAnims[1].sIW_Enter = "cutting_loop_cutter"		
					sIWAnims.sSpawnedPropAnims[1].sIW_Looping = "cutting_loop_cutter"	
					
					//sIWAnims.sSpawnedPropAnims[2].sIW_Enter = "cutting_loop_glass_piece"		
					//sIWAnims.sSpawnedPropAnims[2].sIW_Looping = "cutting_loop_glass_piece"	
				BREAK
				
				CASE ciIW_SUBANIM__GLASSCUT_SUCCESS
					sIWAnims.sPlayerAnims.sIW_Enter = "success"
					sIWAnims.sPlayerFaceAnims.sIW_Enter = "success_facial"
					
					//sIWAnims.sInteractWithEntityAnims.sIW_Enter = "success_glass_display_cut"
					
					sIWAnims.sCameraAnims.sIW_Enter = "success_cam"
					sIWAnims.sCameraVars.iLerpOut = 2500
					
					sIWAnims.sSpawnedPropAnims[0].sIW_Enter = "success_bag"		
					
					sIWAnims.sSpawnedPropAnims[1].sIW_Enter = "success_cutter"	
					sIWAnims.sSpawnedPropAnims[1].bPersistent = FALSE
					
					sIWAnims.sSpawnedPropAnims[2].sIW_Enter = "success_glass_piece"	
					sIWAnims.sSpawnedPropAnims[2].bHideDuringThisSubAnim = FALSE // Only unhide it for the very last anims
				BREAK
				
				CASE ciIW_SUBANIM__GLASSCUT_EXIT
					sIWAnims.sPlayerAnims.sIW_Enter = "exit"
					sIWAnims.sPlayerFaceAnims.sIW_Enter = "exit_facial"
					sIWAnims.sInteractWithEntityAnims.sIW_Enter  = "exit_glass_display"
					sIWAnims.sCameraAnims.sIW_Enter = "exit_cam"					
					sIWAnims.sSpawnedPropAnims[0].sIW_Enter = "exit_bag"		
					sIWAnims.sSpawnedPropAnims[1].sIW_Enter = "exit_cutter"
					//sIWAnims.sSpawnedPropAnims[2].sIW_Enter = "exit_glass_piece"	
				BREAK
				
				CASE ciIW_SUBANIM__GLASSCUT_OVERHEAT
					SWITCH GET_RANDOM_INT_IN_RANGE(0, 3)
						CASE 0
							sIWAnims.sPlayerAnims.sIW_Enter = "OVERHEAT_REACT_01"
							sIWAnims.sPlayerFaceAnims.sIW_Enter = "OVERHEAT_REACT_01_facial"
							sIWAnims.sInteractWithEntityAnims.sIW_Enter  = "OVERHEAT_REACT_01_glass_display"
							sIWAnims.sCameraAnims.sIW_Enter = "OVERHEAT_REACT_01_cam"					
							sIWAnims.sSpawnedPropAnims[0].sIW_Enter = "OVERHEAT_REACT_01_bag"		
							sIWAnims.sSpawnedPropAnims[1].sIW_Enter = "OVERHEAT_REACT_01_cutter"
							//sIWAnims.sSpawnedPropAnims[2].sIW_Enter = "OVERHEAT_REACT_01_glass_piece"
						BREAK
						
						CASE 1
							sIWAnims.sPlayerAnims.sIW_Enter = "OVERHEAT_REACT_02"
							sIWAnims.sPlayerFaceAnims.sIW_Enter = "OVERHEAT_REACT_02_facial"
							sIWAnims.sInteractWithEntityAnims.sIW_Enter  = "OVERHEAT_REACT_01_glass_display"
							sIWAnims.sCameraAnims.sIW_Enter = "OVERHEAT_REACT_02_cam"					
							sIWAnims.sSpawnedPropAnims[0].sIW_Enter = "OVERHEAT_REACT_02_bag"		
							sIWAnims.sSpawnedPropAnims[1].sIW_Enter = "OVERHEAT_REACT_02_cutter"
							//sIWAnims.sSpawnedPropAnims[2].sIW_Enter = "OVERHEAT_REACT_02_glass_piece"
						BREAK
						
						CASE 2
							sIWAnims.sPlayerAnims.sIW_Enter = "OVERHEAT_REACT_03"
							sIWAnims.sPlayerFaceAnims.sIW_Enter = "OVERHEAT_REACT_03_facial"
							sIWAnims.sInteractWithEntityAnims.sIW_Enter  = "OVERHEAT_REACT_03_glass_display"
							sIWAnims.sCameraAnims.sIW_Enter = "OVERHEAT_REACT_03_cam"					
							sIWAnims.sSpawnedPropAnims[0].sIW_Enter = "OVERHEAT_REACT_03_bag"		
							sIWAnims.sSpawnedPropAnims[1].sIW_Enter = "OVERHEAT_REACT_03_cutter"
							//sIWAnims.sSpawnedPropAnims[2].sIW_Enter = "OVERHEAT_REACT_03_glass_piece"
						BREAK
					ENDSWITCH
				BREAK
				
			ENDSWITCH
		BREAK 
		
		CASE ciINTERACT_WITH_PRESET__OPEN_VEHICLE_TRUNK	
			SWITCH sInteractWithParams.iInteractWith_AltAnimSet
				CASE ciIW_ALTANIMSET__OPENVEHICLETRUNK_GENERIC
					sIWAnims.sIW_AnimDict = "anim_heist@hs3f@ig14_open_car_trunk@male@"
					sIWAnims.sPlayerAnims.sIW_Enter = "open_trunk_rushed"
				BREAK
				
				CASE ciIW_ALTANIMSET__OPENVEHICLETRUNK_FELON2
					sIWAnims.sIW_AnimDict = "anim_heist@hs3f@ig14_open_car_trunk@male@"
					sIWAnims.sPlayerAnims.sIW_Enter = "open_trunk_rushed"
					//sIWAnims.sInteractWithLinkedVehicleAnims.sIW_Enter = "open_trunk_rushed_felon2" | Vehicles in sync scenes seem to be impossible or will need to be revisited later
				BREAK
			ENDSWITCH
		BREAK
		
		CASE ciINTERACT_WITH_PRESET__USB_UPLOAD_VIRUS_KEYPAD
			IF IS_PED_WEARING_HIGH_HEELS(LocalPlayerPed)
				sIWAnims.sIW_AnimDict = "anim@scripted@player@mission@tunf_hack_keypad@heeled@"
			ELSE
				sIWAnims.sIW_AnimDict = "anim@scripted@player@mission@tunf_hack_keypad@male@"
			ENDIF
			
			sIWAnims.sPlayerAnims.sIW_Enter = "action"
			sIWAnims.sPlayerAnims.sIW_Looping = "hack_loop"
			sIWAnims.sPlayerAnims.sIW_Exit = "exit"
			
			sIWAnims.iNumPropsToSpawn++
			sIWAnims.sSpawnedPropAnims[0].mnIW_ModelName = PROP_PHONE_ING
			sIWAnims.sSpawnedPropAnims[0].sIW_Enter = "action_phone"
			sIWAnims.sSpawnedPropAnims[0].sIW_Looping = "hack_loop_phone"
			sIWAnims.sSpawnedPropAnims[0].sIW_Exit = "exit_phone"
			
			sIWAnims.iNumPropsToSpawn++
			sIWAnims.sSpawnedPropAnims[1].mnIW_ModelName = INT_TO_ENUM(MODEL_NAMES, HASH("tr_Prop_TR_USB_Drive_01a"))
			sIWAnims.sSpawnedPropAnims[1].sIW_Enter = "action_usb_drive"
			sIWAnims.sSpawnedPropAnims[1].sIW_Looping = "hack_loop_usb_drive"
			sIWAnims.sSpawnedPropAnims[1].sIW_Exit = "exit_usb_drive"
			
		BREAK
		
		CASE ciINTERACT_WITH_PRESET__PLANT_THERMAL_CHARGE
			
			sIWAnims.iNumPropsToSpawn++
			sIWAnims.sSpawnedPropAnims[0].mnIW_ModelName = GET_INTERACT_WITH_BAG_MODEL_TO_USE()
				
			sIWAnims.iNumPropsToSpawn++
			sIWAnims.sSpawnedPropAnims[1].mnIW_ModelName = HEI_PROP_HEIST_THERMITE
			sIWAnims.sSpawnedPropAnims[1].bPersistent = TRUE
			
			IF IS_PED_WEARING_HIGH_HEELS(LocalPlayerPed)
				sIWAnims.sIW_AnimDict = "anim_heist@hs3f@ig13_thermal_charge@thermal_charge@female@"
				sIWAnims.sPlayerAnims.sIW_Enter = "thermal_charge_female_female"
				sIWAnims.sSpawnedPropAnims[0].sIW_Enter = "thermal_charge_female_p_m_bag_var22_arm_s"
				sIWAnims.sSpawnedPropAnims[1].sIW_Enter = "thermal_charge_female_hei_prop_heist_thermite"
			ELSE
				sIWAnims.sIW_AnimDict = "anim_heist@hs3f@ig13_thermal_charge@thermal_charge@male@"
				sIWAnims.sPlayerAnims.sIW_Enter = "thermal_charge_male_male"
				sIWAnims.sSpawnedPropAnims[0].sIW_Enter = "thermal_charge_male_p_m_bag_var22_arm_s"
				sIWAnims.sSpawnedPropAnims[1].sIW_Enter = "thermal_charge_male_hei_prop_heist_thermite"
			ENDIF
			
		BREAK
		
		CASE ciINTERACT_WITH_PRESET__DIG_SAND_PILE
		
			IF IS_PED_WEARING_HIGH_HEELS(LocalPlayerPed)
				sIWAnims.sIW_AnimDict = "anim@scripted@player@mission@tun_iaa_dig@heeled@"
			ELSE
				sIWAnims.sIW_AnimDict = "anim@scripted@player@mission@tun_iaa_dig@male@"
			ENDIF
			
			sIWAnims.sInteractWithIncludedPTFXAsset = "scr_tn_ia"
			
			sIWAnims.sPlayerAnims.sIW_Enter = "action"
			sIWAnims.sPlayerFaceAnims.sIW_Enter = "action_facial"
			
			sIWAnims.sInteractWithEntityAnims.fPropHideAfterPhase = 0.15
			
			sIWAnims.iNumPropsToSpawn++
			sIWAnims.sSpawnedPropAnims[0].mnIW_ModelName = GET_INTERACT_WITH_BAG_MODEL_TO_USE()
			sIWAnims.sSpawnedPropAnims[0].sIW_Enter = "action_bag"
			
			sIWAnims.iNumPropsToSpawn++
			sIWAnims.sSpawnedPropAnims[1].mnIW_ModelName = INT_TO_ENUM(MODEL_NAMES, HASH("h4_Prop_h4_Box_Ammo_01a"))
			sIWAnims.sSpawnedPropAnims[1].sIW_Enter = "ACTION_AMMO_BOX"
			sIWAnims.sSpawnedPropAnims[1].bPersistent = TRUE
				
			sIWAnims.iNumPropsToSpawn++
			sIWAnims.sSpawnedPropAnims[2].mnIW_ModelName = INT_TO_ENUM(MODEL_NAMES, HASH("tr_Prop_TR_File_Cylinder_01a"))
			sIWAnims.sSpawnedPropAnims[2].sIW_Enter = "ACTION_FILE"
			
			sIWAnims.iNumPropsToSpawn++
			IF DOES_ENTITY_EXIST(sInteractWithParams.oiInteractWithEntity)
				IF GET_ENTITY_MODEL(sInteractWithParams.oiInteractWithEntity) = INT_TO_ENUM(MODEL_NAMES, HASH("tr_Prop_TR_Sand_01a"))
					sIWAnims.sSpawnedPropAnims[3].mnIW_ModelName = INT_TO_ENUM(MODEL_NAMES, HASH("tr_Prop_TR_Sand_01b"))
				ELIF GET_ENTITY_MODEL(sInteractWithParams.oiInteractWithEntity) = INT_TO_ENUM(MODEL_NAMES, HASH("tr_Prop_TR_Sand_CS_01a"))
					sIWAnims.sSpawnedPropAnims[3].mnIW_ModelName = INT_TO_ENUM(MODEL_NAMES, HASH("tr_Prop_TR_Sand_CS_01b"))
				ENDIF
			ENDIF
			sIWAnims.sSpawnedPropAnims[3].bPersistent = TRUE
			sIWAnims.sSpawnedPropAnims[3].fPropHideUntilPhase = 0.15
			
			sIWAnims.sCameraAnims.sIW_Enter = "ACTION_CAM"
			sIWAnims.sCameraVars.iLerpIn = 0
			sIWAnims.sCameraVars.iLerpOut = 0
			
		BREAK
		
		CASE ciINTERACT_WITH_PRESET__PICK_UP_HDD
			IF IS_PED_WEARING_HIGH_HEELS(LocalPlayerPed)
				sIWAnims.sIW_AnimDict = "anim@scripted@player@mission@tun_bunk_ig1_hdd_server@heeled@"
			ELSE
				sIWAnims.sIW_AnimDict = "anim@scripted@player@mission@tun_bunk_ig1_hdd_server@male@"
			ENDIF
			
			sIWAnims.sPlayerAnims.sIW_Enter = "ACTION"
			sIWAnims.sPlayerFaceAnims.sIW_Enter = "action_facial"
			
			sIWAnims.sInteractWithEntityAnims.sIW_Enter = "ACTION_DRIVE"
			
			sIWAnims.iNumPropsToSpawn++
			sIWAnims.sSpawnedPropAnims[0].mnIW_ModelName = GET_INTERACT_WITH_BAG_MODEL_TO_USE()
			sIWAnims.sSpawnedPropAnims[0].sIW_Enter = "ACTION_BAG"
		BREAK
		
		CASE ciINTERACT_WITH_PRESET__PASS_GUARD
			SWITCH sInteractWithParams.iInteractWith_AltAnimSet
				CASE ciIW_ALTANIMSET__PASSGUARD_SHOWIDCARD
					IF IS_PED_WEARING_HIGH_HEELS(LocalPlayerPed)
						sIWAnims.sIW_AnimDict = "anim@scripted@player@mission@tunf_uni_ig1_elevator_pass_p1@heeled@"
					ELSE
						sIWAnims.sIW_AnimDict = "anim@scripted@player@mission@tunf_uni_ig1_elevator_pass_p1@male@"
					ENDIF
					
					sIWAnims.sPlayerAnims.sIW_Enter = "ACTION"
					
					sIWAnims.sInteractWithLinkedPedAnims.sIW_Enter = "ACTION_GUARD"
					
					sIWAnims.iNumPropsToSpawn++
					sIWAnims.sSpawnedPropAnims[0].mnIW_ModelName = INT_TO_ENUM(MODEL_NAMES, HASH("tr_Prop_TR_Acc_Pass_01a"))
					sIWAnims.sSpawnedPropAnims[0].sIW_Enter = "ACTION_CARD"
				BREAK
				
				CASE ciIW_ALTANIMSET__PASSGUARD_SHOWVEHICLE
					sIWAnims.sIW_AnimDict = "anim@scripted@player@mission@tunf_uni_ig1_elevator_pass_p1@male@"
					
					sIWAnims.sInteractWithLinkedPedAnims.sIW_Enter = "ACTION_GUARD"
					
				BREAK
			ENDSWITCH
		BREAK
		
		CASE ciINTERACT_WITH_PRESET__INSERT_HDD
			IF IS_PED_WEARING_HIGH_HEELS(LocalPlayerPed)
				sIWAnims.sIW_AnimDict = "anim@scripted@player@mission@tunf_bunk_ig2_hdd_nas@heeled@"
			ELSE
				sIWAnims.sIW_AnimDict = "anim@scripted@player@mission@tunf_bunk_ig2_hdd_nas@male@"
			ENDIF
			
			sIWAnims.sPlayerAnims.sIW_Enter = "PLAYER_PLUGING_IN"
			sIWAnims.sPlayerFaceAnims.sIW_Enter = "PLAYER_PLUGING_IN_FACIAL"
			
			sIWAnims.sInteractWithLinkedPedAnims.sIW_Enter = "PLAYER_PLUGING_IN_NAS"
			
			sIWAnims.iNumPropsToSpawn++
			sIWAnims.sSpawnedPropAnims[0].mnIW_ModelName = GET_INTERACT_WITH_BAG_MODEL_TO_USE()
			sIWAnims.sSpawnedPropAnims[0].sIW_Enter = "PLAYER_PLUGING_IN_BAG"
			
			sIWAnims.iNumPropsToSpawn++
			sIWAnims.sSpawnedPropAnims[1].mnIW_ModelName = Prop_CS_Server_Drive
			sIWAnims.sSpawnedPropAnims[1].sIW_Enter = "PLAYER_PLUGING_IN_DRIVE_1"
			
			sIWAnims.iNumPropsToSpawn++
			sIWAnims.sSpawnedPropAnims[2].mnIW_ModelName = Prop_CS_Server_Drive
			sIWAnims.sSpawnedPropAnims[2].sIW_Enter = "PLAYER_PLUGING_IN_DRIVE_2"
			
			sIWAnims.iNumPropsToSpawn++
			sIWAnims.sSpawnedPropAnims[3].mnIW_ModelName = Prop_CS_Server_Drive
			sIWAnims.sSpawnedPropAnims[3].sIW_Enter = "PLAYER_PLUGING_IN_DRIVE_3"
			
			sIWAnims.iNumPropsToSpawn++
			sIWAnims.sSpawnedPropAnims[4].mnIW_ModelName = Prop_CS_Server_Drive
			sIWAnims.sSpawnedPropAnims[4].sIW_Enter = "PLAYER_PLUGING_IN_DRIVE_4"
		BREAK
		
		CASE ciINTERACT_WITH_PRESET__INTERROGATION
		
			sIWAnims.sIW_AnimDict = "anim@scripted@data_leak@fix_golf_ig2_golfclub_intimidation@"
			
			sIWAnims.iNumPropsToSpawn++
			sIWAnims.sSpawnedPropAnims[0].mnIW_ModelName = INT_TO_ENUM(MODEL_NAMES, HASH("W_ME_GClub"))
			
			SET_BIT(sIWAnims.iInteractWithAnimConfigBS, ciINTERACT_WITH_PARAMS_BS__DontGrabControlOfLinkedPed)
			
			SWITCH sInteractWithParams.iInteractWith_AltAnimSet
				CASE ciIW_ALTANIMSET__INTERROGATION_GOLFCLUB
					SWITCH iSubAnimPresetToUse
					
						// STAGE 1 IS BEFORE THE INTERACT-WITH //
						
						// STAGE 2 //
						CASE ciIW_SUBANIM__INTERROGATION_STAGE_2_ENTER
							sIWAnims.sPlayerAnims.sIW_Enter = "stage_2_trans"
							sIWAnims.sPlayerAnims.sIW_Looping = "stage_2_base"
						BREAK
						
						CASE ciIW_SUBANIM__INTERROGATION_STAGE_2_HIT_1
							sIWAnims.sPlayerAnims.sIW_Enter = "STAGE_2_HIT_LINE_3"
							sIWAnims.sPlayerAnims.sIW_Looping = "stage_2_base"
						BREAK
						
						CASE ciIW_SUBANIM__INTERROGATION_STAGE_2_HIT_2
							sIWAnims.sPlayerAnims.sIW_Enter = "STAGE_2_HIT_LINE_3"
							sIWAnims.sPlayerAnims.sIW_Looping = "stage_2_base"
						BREAK
						
						CASE ciIW_SUBANIM__INTERROGATION_STAGE_2_HIT_3
							sIWAnims.sPlayerAnims.sIW_Enter = "STAGE_2_HIT_LINE_3"
							sIWAnims.sPlayerAnims.sIW_Looping = "stage_2_base"
						BREAK
						
						// STAGE 3 //
						CASE ciIW_SUBANIM__INTERROGATION_STAGE_3_ENTER
							sIWAnims.sPlayerAnims.sIW_Enter = "STAGE_3_HIT_LINE_5"
							sIWAnims.sPlayerAnims.sIW_Looping = "stage_3_base"
						BREAK
						
						CASE ciIW_SUBANIM__INTERROGATION_STAGE_3_HIT_1
							sIWAnims.sPlayerAnims.sIW_Enter = "STAGE_3_HIT_LINE_4"
							sIWAnims.sPlayerAnims.sIW_Looping = "stage_3_base"
						BREAK
						
						CASE ciIW_SUBANIM__INTERROGATION_STAGE_3_HIT_2
							sIWAnims.sPlayerAnims.sIW_Enter = "STAGE_3_HIT_LINE_5"
							sIWAnims.sPlayerAnims.sIW_Looping = "stage_3_base"
						BREAK
						
						CASE ciIW_SUBANIM__INTERROGATION_STAGE_3_HIT_3
							sIWAnims.sPlayerAnims.sIW_Enter = "STAGE_3_HIT_LINE_4"
							sIWAnims.sPlayerAnims.sIW_Looping = "stage_3_base"
						BREAK
						
						CASE ciIW_SUBANIM__INTERROGATION_STAGE_3_HIT_4
							sIWAnims.sPlayerAnims.sIW_Enter = "STAGE_3_HIT_LINE_5"
							sIWAnims.sPlayerAnims.sIW_Looping = "stage_3_base"
						BREAK
						
						// STAGE 4//
						CASE ciIW_SUBANIM__INTERROGATION_STAGE_APOLOGY_A
							sIWAnims.sPlayerAnims.sIW_Enter = "stage_4_apology_var1"
							sIWAnims.sPlayerAnims.sIW_Looping = "stage_4_base"
							
							sInteractWithParams.iInteractWithEndConversationLine = 3
							SET_BIT(sInteractWithParams.iInteractWithParamsBS, ciINTERACT_WITH_PARAMS_BS__LeadIntoCutscene)
							SET_BIT(sInteractWithParams.iInteractWithParamsBS, ciINTERACT_WITH_PARAMS_BS__LeadIntoCutsceneShouldWaitUntilLoop)
							SET_BIT(sInteractWithParams.iInteractWithParamsBS, ciINTERACT_WITH_PARAMS_BS__DontFinishAnimUntilConversationsEnd)
							CLEAR_BIT(sInteractWithParams.iInteractWithParamsBS, ciINTERACT_WITH_PARAMS_BS__OnlyReturnTrueAfterCleanup)
						BREAK
						
						CASE ciIW_SUBANIM__INTERROGATION_STAGE_APOLOGY_B
							sIWAnims.sPlayerAnims.sIW_Enter = "stage_4_apology_var2"
							sIWAnims.sPlayerAnims.sIW_Looping = "stage_4_base"
							
							sInteractWithParams.iInteractWithEndConversationLine = 1
							SET_BIT(sInteractWithParams.iInteractWithParamsBS, ciINTERACT_WITH_PARAMS_BS__LeadIntoCutscene)
							SET_BIT(sInteractWithParams.iInteractWithParamsBS, ciINTERACT_WITH_PARAMS_BS__LeadIntoCutsceneShouldWaitUntilLoop)
							SET_BIT(sInteractWithParams.iInteractWithParamsBS, ciINTERACT_WITH_PARAMS_BS__DontFinishAnimUntilConversationsEnd)
							CLEAR_BIT(sInteractWithParams.iInteractWithParamsBS, ciINTERACT_WITH_PARAMS_BS__OnlyReturnTrueAfterCleanup)
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE ciINTERACT_WITH_PRESET__PLAYER_IN_CAR_ANIM
			SWITCH sInteractWithParams.iInteractWith_AltAnimSet
				CASE ciIW_ALTANIMSET__PLAYERINCAR_PRESSBUTTONS
					sIWAnims.sIW_AnimDict = "veh@low@front_ds@base"
					sIWAnims.sPlayerAnims.sIW_Enter = "change_station"
				BREAK
			ENDSWITCH
		BREAK
		
		CASE ciINTERACT_WITH_PRESET__PICK_UP_INJURED_PED
			SWITCH sInteractWithParams.iInteractWith_AltAnimSet
				CASE ciIW_ALTANIMSET__PICKUPPED_JOHNNYGUNZ
					IF IS_PED_WEARING_HIGH_HEELS(LocalPlayerPed)
						sIWAnims.sIW_AnimDict = "anim@scripted@data_leak@fixf_fin_ig3_grab_johnnyguns@heeled@"
					ELSE
						sIWAnims.sIW_AnimDict = "anim@scripted@data_leak@fixf_fin_ig3_grab_johnnyguns@male@"
					ENDIF
					
					IF NOT SHOULD_PLAYER_DO_BACK_SIDE_INTERACT_WITH(sInteractWithParams)
						sIWAnims.sPlayerAnims.sIW_Enter = "ACTION_FRONT_PLAYER"
						sIWAnims.sPlayerAnims.sIW_Looping = "FRONT_LOOP_PLAYER"
						
						sIWAnims.sInteractWithLinkedPedAnims.sIW_Enter = "ACTION_FRONT_JOHNNY"
						sIWAnims.sInteractWithLinkedPedAnims.sIW_Looping = "FRONT_LOOP_JOHNNY"
					ELSE
						sIWAnims.sPlayerAnims.sIW_Enter = "ACTION_BACK_PLAYER"
						sIWAnims.sPlayerAnims.sIW_Looping = "BACK_LOOP_PLAYER"
						
						sIWAnims.sInteractWithLinkedPedAnims.sIW_Enter = "ACTION_BACK_JOHNNY"
						sIWAnims.sInteractWithLinkedPedAnims.sIW_Looping = "BACK_LOOP_JOHNNY"
					ENDIF
					
					// Finish the rule straight away when the anim reaches its completion point, allowing the anim to cut straight into the cutscene following this rule if needed
					SET_BIT(sInteractWithParams.iInteractWithParamsBS, ciINTERACT_WITH_PARAMS_BS__EnableInteractWithEarlyEndForObjectiveMinigames)
					SET_BIT(sInteractWithParams.iInteractWithParamsBS, ciINTERACT_WITH_PARAMS_BS__LeadIntoCutscene)
					
					CLEAR_BIT(sInteractWithParams.iInteractWithParamsBS, ciINTERACT_WITH_PARAMS_BS__OnlyReturnTrueAfterCleanup)
				BREAK
			ENDSWITCH
		BREAK
		
		CASE ciINTERACT_WITH_PRESET__STEAL_LAPTOP_FROM_DJ_DESK
			sIWAnims.sIW_AnimDict = "ANIM@SCRIPTED@DATA_LEAK@FIX_PRO_IG4_PRO_LAPTOP@"
			
			sIWAnims.sPlayerAnims.sIW_Enter = "FIX_PRO_IG4_PRO_LAPTOP_GRAB"
			sIWAnims.sPlayerAnims.sIW_Looping = "FIX_PRO_IG4_PRO_LAPTOP_LOOP"
			sIWAnims.sNearbyPedAnims.mnIW_ModelName = INT_TO_ENUM(MODEL_NAMES, HASH("IG_Party_Promo"))
			sIWAnims.sNearbyDynopropAnims[0].mnIW_ModelName = Prop_NPC_Phone
			
			MAKE_INTERACT_WITH_ANIMS_MATCH_WITH_SUFFIX(sIWAnims.sPlayerAnims, sIWAnims.sNearbyPedAnims, "_PROMOTER")
			MAKE_INTERACT_WITH_ANIMS_MATCH_WITH_SUFFIX(sIWAnims.sPlayerAnims, sIWAnims.sNearbyDynopropAnims[0], "_PHONE")
			MAKE_INTERACT_WITH_ANIMS_MATCH_WITH_SUFFIX(sIWAnims.sPlayerAnims, sIWAnims.sInteractWithEntityAnims, "_LAPTOP")
			MAKE_INTERACT_WITH_ANIMS_MATCH_WITH_SUFFIX(sIWAnims.sPlayerAnims, sIWAnims.sPlayerAnims, "_PLAYER")	
			
			// Finish the rule straight away when the anim reaches its completion point, allowing the anim to cut straight into the cutscene following this rule if needed
			SET_BIT(sInteractWithParams.iInteractWithParamsBS, ciINTERACT_WITH_PARAMS_BS__EnableInteractWithEarlyEndForObjectiveMinigames)
			SET_BIT(sInteractWithParams.iInteractWithParamsBS, ciINTERACT_WITH_PARAMS_BS__LeadIntoCutscene)
					
			CLEAR_BIT(sInteractWithParams.iInteractWithParamsBS, ciINTERACT_WITH_PARAMS_BS__OnlyReturnTrueAfterCleanup)
		BREAK
		
		CASE ciINTERACT_WITH_PRESET__CUTSCENE_LEAD_OUT
			SWITCH sInteractWithParams.iInteractWith_AltAnimSet
				CASE ciIW_ALTANIMSET__CUTSCENELEADOUT_FRANKLINANDLAMAR
					sIWAnims.sIW_AnimDict = "anim@scripted@short_trip@fix_trip1_int_leadout@"
					
					SWITCH GET_LOCAL_PLAYER_TEAM(TRUE)
						CASE 0
							sIWAnims.sPlayerAnims.sIW_Enter = "leadout_franklin"
							sIWAnims.sPlayerFaceAnims.sIW_Enter = "leadout_franklin_facial"
							sIWAnims.sCameraAnims.sIW_Enter = "leadout_franklin_cam"
						BREAK
						CASE 1
							sIWAnims.sPlayerAnims.sIW_Enter = "leadout_lamar"
							sIWAnims.sPlayerFaceAnims.sIW_Enter = "leadout_lamar_facial"
							sIWAnims.sCameraAnims.sIW_Enter = "leadout_lamar_cam"
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
			
			SET_BIT(sInteractWithParams.iInteractWithParamsBS, ciINTERACT_WITH_PARAMS_BS__LeadOutOfCutscene)
			sIWAnims.sCameraVars.iLerpIn = 5
			sIWAnims.sCameraVars.iLerpOut = 4200
			sIWAnims.sCameraVars.fPhaseEnd = 0.6
		BREAK
		
		CASE ciINTERACT_WITH_PRESET__SEARCH_BROKEN_LAPTOP
			IF IS_PED_WEARING_HIGH_HEELS(LocalPlayerPed)
				sIWAnims.sIW_AnimDict = "ANIM@SCRIPTED@ULP_MISSIONS@BROKEN_LAPTOP@HEELED@"
			ELSE
				sIWAnims.sIW_AnimDict = "ANIM@SCRIPTED@ULP_MISSIONS@BROKEN_LAPTOP@MALE@"
			ENDIF
			sIWAnims.sPlayerAnims.sIW_Enter = "ACTION"
			sIWAnims.sInteractWithEntityAnims.sIW_Enter = "ACTION_LAPTOP"
		BREAK
		
		CASE ciINTERACT_WITH_PRESET__SEARCH_PAPERWORK
			IF IS_PED_WEARING_HIGH_HEELS(LocalPlayerPed)
				sIWAnims.sIW_AnimDict = "ANIM@SCRIPTED@ULP_MISSIONS@PAPERWORK@HEELED@"
			ELSE
				sIWAnims.sIW_AnimDict = "ANIM@SCRIPTED@ULP_MISSIONS@PAPERWORK@MALE@"
			ENDIF
			sIWAnims.sPlayerAnims.sIW_Enter = "ACTION"
			sIWAnims.sInteractWithEntityAnims.sIW_Enter = "ACTION_FOLDER"	
		BREAK
		
		CASE ciINTERACT_WITH_PRESET__BEAMHACK_IN_VEHICLE
			
			IF NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
				EXIT
			ENDIF
			
			VEHICLE_SEAT vsSeat 
			vsSeat = GET_PED_VEHICLE_SEAT(LocalPlayerPed, GET_VEHICLE_PED_IS_IN(LocalPlayerPed)) 
			IF vsSeat = VS_BACK_RIGHT
			OR vsSeat = VS_EXTRA_RIGHT_1
			OR vsSeat = VS_EXTRA_RIGHT_2
			OR vsSeat = VS_EXTRA_RIGHT_3
				IF IS_PED_WEARING_HIGH_HEELS(LocalPlayerPed)
					sIWAnims.sIW_AnimDict = "ANIM@SCRIPTED@ULP_MISSIONS@PLAYERS_HACKING@PASSENGER_BACK@HEELED@"
				ELSE
					sIWAnims.sIW_AnimDict = "ANIM@SCRIPTED@ULP_MISSIONS@PLAYERS_HACKING@PASSENGER_BACK@MALE@"
				ENDIF
			ELSE
				IF IS_PED_WEARING_HIGH_HEELS(LocalPlayerPed)
					sIWAnims.sIW_AnimDict = "ANIM@SCRIPTED@ULP_MISSIONS@PLAYERS_HACKING@DRIVER_BACK@HEELED@"
				ELSE
					sIWAnims.sIW_AnimDict = "ANIM@SCRIPTED@ULP_MISSIONS@PLAYERS_HACKING@DRIVER_BACK@MALE@"
				ENDIF
			ENDIF
			
			sIWAnims.iNumPropsToSpawn++
			sIWAnims.sSpawnedPropAnims[0].mnIW_ModelName = PROP_PHONE_ING
			
			SWITCH iSubAnimPresetToUse
				CASE ciIW_SUBANIM____BEAMHACK_IN_VEHICLE_ENTER
					sIWAnims.sPlayerAnims.sIW_Enter = "PLAYER_ENTER"
					sIWAnims.sPlayerAnims.sIW_Looping = "PLAYER_HACKING_LOOP"
					
					MAKE_INTERACT_WITH_ANIMS_MATCH_WITH_SUFFIX(sIWAnims.sPlayerAnims, sIWAnims.sSpawnedPropAnims[0], "_PHONE")
					MAKE_INTERACT_WITH_ANIMS_MATCH_WITH_SUFFIX(sIWAnims.sPlayerAnims, sIWAnims.sPlayerFaceAnims, "_Facial")
				BREAK
				
				CASE ciIW_SUBANIM____BEAMHACK_IN_VEHICLE_HACKING_LOOP
					sIWAnims.sPlayerAnims.sIW_Enter = "PLAYER_HACKING_LOOP"
					sIWAnims.sPlayerAnims.sIW_Looping = "PLAYER_HACKING_LOOP"

					MAKE_INTERACT_WITH_ANIMS_MATCH_WITH_SUFFIX(sIWAnims.sPlayerAnims, sIWAnims.sSpawnedPropAnims[0], "_PHONE")
					MAKE_INTERACT_WITH_ANIMS_MATCH_WITH_SUFFIX(sIWAnims.sPlayerAnims, sIWAnims.sPlayerFaceAnims, "_Facial")
				BREAK
				
				CASE ciIW_SUBANIM____BEAMHACK_IN_VEHICLE_ERROR_REACT
					sIWAnims.sPlayerAnims.sIW_Enter = "PLAYER_ERROR_REACT"
					sIWAnims.sPlayerAnims.sIW_Looping = "PLAYER_HACKING_LOOP"

					MAKE_INTERACT_WITH_ANIMS_MATCH_WITH_SUFFIX(sIWAnims.sPlayerAnims, sIWAnims.sSpawnedPropAnims[0], "_PHONE")
					MAKE_INTERACT_WITH_ANIMS_MATCH_WITH_SUFFIX(sIWAnims.sPlayerAnims, sIWAnims.sPlayerFaceAnims, "_Facial")
				BREAK
				
				CASE ciIW_SUBANIM____BEAMHACK_IN_VEHICLE_NORMAL_EXIT
					sIWAnims.sPlayerAnims.sIW_Enter = "PLAYER_NORMAL_EXIT"
					
					MAKE_INTERACT_WITH_ANIMS_MATCH_WITH_SUFFIX(sIWAnims.sPlayerAnims, sIWAnims.sSpawnedPropAnims[0], "_PHONE")
					MAKE_INTERACT_WITH_ANIMS_MATCH_WITH_SUFFIX(sIWAnims.sPlayerAnims, sIWAnims.sPlayerFaceAnims, "_Facial")
				BREAK
				
				CASE ciIW_SUBANIM____BEAMHACK_IN_VEHICLE_SUCCESS_EXIT
					sIWAnims.sPlayerAnims.sIW_Enter = "PLAYER_SUCCESS_EXIT"
					
					MAKE_INTERACT_WITH_ANIMS_MATCH_WITH_SUFFIX(sIWAnims.sPlayerAnims, sIWAnims.sSpawnedPropAnims[0], "_PHONE")
					MAKE_INTERACT_WITH_ANIMS_MATCH_WITH_SUFFIX(sIWAnims.sPlayerAnims, sIWAnims.sPlayerFaceAnims, "_Facial")
				BREAK
			ENDSWITCH

		BREAK
		
		CASE ciINTERACT_WITH_PRESET__BRUTEFORCE
			IF IS_PED_WEARING_HIGH_HEELS(LocalPlayerPed)
				sIWAnims.sIW_AnimDict = "ANIM@SCRIPTED@ULP_MISSIONS@COMPUTERHACK@HEELED@"
			ELSE
				sIWAnims.sIW_AnimDict = "ANIM@SCRIPTED@ULP_MISSIONS@COMPUTERHACK@MALE@"
			ENDIF
			
			sIWAnims.sPlayerAnims.sIW_Enter = "ENTER"
			sIWAnims.sPlayerAnims.sIW_Looping = "HACKING_LOOP"
			sIWAnims.sPlayerAnims.sIW_Exit = "EXIT"
			
			MAKE_INTERACT_WITH_ANIMS_MATCH_WITH_SUFFIX(sIWAnims.sPlayerAnims, sIWAnims.sInteractWithEntityAnims, "_DESK")
		BREAK
		
		CASE ciINTERACT_WITH_PRESET__CHECK_FUSE_BOX
			
			sIWAnims.sIW_AnimDict = "ANIM@SCRIPTED@ULP_MISSIONS@FUSE@MALE@"
			
			sIWAnims.sPlayerAnims.sIW_Enter = "ENTER"
			
			MAKE_INTERACT_WITH_ANIMS_MATCH_WITH_SUFFIX(sIWAnims.sPlayerAnims, sIWAnims.sInteractWithEntityAnims, "_FUSEBOX")
			
			sIWAnims.iNumLinkedObjectsToUse = 4
			MAKE_INTERACT_WITH_ANIMS_MATCH_WITH_SUFFIX(sIWAnims.sPlayerAnims, sIWAnims.sLinkedObjectAnims[0], "_BROKEN_FUSE_1")
			sIWAnims.sLinkedObjectAnims[0].bPersistent = TRUE
			MAKE_INTERACT_WITH_ANIMS_MATCH_WITH_SUFFIX(sIWAnims.sPlayerAnims, sIWAnims.sLinkedObjectAnims[1], "_BROKEN_FUSE_2")
			sIWAnims.sLinkedObjectAnims[1].bPersistent = TRUE
			MAKE_INTERACT_WITH_ANIMS_MATCH_WITH_SUFFIX(sIWAnims.sPlayerAnims, sIWAnims.sLinkedObjectAnims[2], "_BROKEN_FUSE_3")
			sIWAnims.sLinkedObjectAnims[2].bPersistent = TRUE
			MAKE_INTERACT_WITH_ANIMS_MATCH_WITH_SUFFIX(sIWAnims.sPlayerAnims, sIWAnims.sLinkedObjectAnims[3], "_BROKEN_FUSE_4")
			sIWAnims.sLinkedObjectAnims[3].bPersistent = TRUE
			
		BREAK
		
		CASE ciINTERACT_WITH_PRESET__INSERT_FUSE
			
			sIWAnims.sIW_AnimDict = "ANIM@SCRIPTED@ULP_MISSIONS@FUSE@MALE@"
		
			SWITCH sInteractWithParams.iInteractWith_AltAnimSet
				CASE ciIW_ALTANIMSET__INSERT_FUSE_1ST
				
					sIWAnims.sPlayerAnims.sIW_Enter = "REPLACE_FUSE_01"
					
					MAKE_INTERACT_WITH_ANIMS_MATCH_WITH_SUFFIX(sIWAnims.sPlayerAnims, sIWAnims.sInteractWithEntityAnims, "_BROKEN_FUSE_1")
					
					sIWAnims.iNumPropsToSpawn++
					sIWAnims.sSpawnedPropAnims[0].mnIW_ModelName = REH_PROP_REH_FUSE_01A
					MAKE_INTERACT_WITH_ANIMS_MATCH_WITH_SUFFIX(sIWAnims.sPlayerAnims, sIWAnims.sSpawnedPropAnims[0], "_FRESH_FUSE_1")
					sIWAnims.sSpawnedPropAnims[0].bPersistent = TRUE
					
					sIWAnims.iNumLinkedObjectsToUse++
					MAKE_INTERACT_WITH_ANIMS_MATCH_WITH_SUFFIX(sIWAnims.sPlayerAnims, sIWAnims.sLinkedObjectAnims[0], "_FUSEBOX")
					
				BREAK
				CASE ciIW_ALTANIMSET__INSERT_FUSE_2ND
					sIWAnims.sPlayerAnims.sIW_Enter = "REPLACE_FUSE_02"
					
					MAKE_INTERACT_WITH_ANIMS_MATCH_WITH_SUFFIX(sIWAnims.sPlayerAnims, sIWAnims.sInteractWithEntityAnims, "_BROKEN_FUSE_2")
					
					sIWAnims.iNumPropsToSpawn++
					sIWAnims.sSpawnedPropAnims[0].mnIW_ModelName = REH_PROP_REH_FUSE_01A
					MAKE_INTERACT_WITH_ANIMS_MATCH_WITH_SUFFIX(sIWAnims.sPlayerAnims, sIWAnims.sSpawnedPropAnims[0], "_FRESH_FUSE_2")
					sIWAnims.sSpawnedPropAnims[0].bPersistent = TRUE
					
					sIWAnims.iNumLinkedObjectsToUse++
					MAKE_INTERACT_WITH_ANIMS_MATCH_WITH_SUFFIX(sIWAnims.sPlayerAnims, sIWAnims.sLinkedObjectAnims[0], "_FUSEBOX")
					
				BREAK
				CASE ciIW_ALTANIMSET__INSERT_FUSE_3RD
					sIWAnims.sPlayerAnims.sIW_Enter = "REPLACE_FUSE_03"
					
					MAKE_INTERACT_WITH_ANIMS_MATCH_WITH_SUFFIX(sIWAnims.sPlayerAnims, sIWAnims.sInteractWithEntityAnims, "_BROKEN_FUSE_3")
					
					sIWAnims.iNumPropsToSpawn++
					sIWAnims.sSpawnedPropAnims[0].mnIW_ModelName = REH_PROP_REH_FUSE_01A
					MAKE_INTERACT_WITH_ANIMS_MATCH_WITH_SUFFIX(sIWAnims.sPlayerAnims, sIWAnims.sSpawnedPropAnims[0], "_FRESH_FUSE_3")
					sIWAnims.sSpawnedPropAnims[0].bPersistent = TRUE
					
					sIWAnims.iNumLinkedObjectsToUse++
					MAKE_INTERACT_WITH_ANIMS_MATCH_WITH_SUFFIX(sIWAnims.sPlayerAnims, sIWAnims.sLinkedObjectAnims[0], "_FUSEBOX")
				BREAK
				CASE ciIW_ALTANIMSET__INSERT_FUSE_FINAL
					sIWAnims.sPlayerAnims.sIW_Enter = "REPLACE_FUSE_04"
					
					MAKE_INTERACT_WITH_ANIMS_MATCH_WITH_SUFFIX(sIWAnims.sPlayerAnims, sIWAnims.sInteractWithEntityAnims, "_BROKEN_FUSE_4")
					
					sIWAnims.sPlayerAnims.sIW_Exit = "SUCCESS"
					
					sIWAnims.iNumPropsToSpawn++
					sIWAnims.sSpawnedPropAnims[0].mnIW_ModelName = REH_PROP_REH_FUSE_01A
					MAKE_INTERACT_WITH_ANIMS_MATCH_WITH_SUFFIX(sIWAnims.sPlayerAnims, sIWAnims.sSpawnedPropAnims[0], "_FRESH_FUSE_4")
					sIWAnims.sSpawnedPropAnims[0].bPersistent = TRUE
					
					sIWAnims.iNumLinkedObjectsToUse++
					MAKE_INTERACT_WITH_ANIMS_MATCH_WITH_SUFFIX(sIWAnims.sPlayerAnims, sIWAnims.sLinkedObjectAnims[0], "_FUSEBOX")
					
				BREAK
			ENDSWITCH
		BREAK
		
		CASE ciINTERACT_WITH_PRESET__GRAB_FUSE
		
			SWITCH sInteractWithParams.iInteractWith_AltAnimSet
			
				CASE ciIW_ALTANIMSET__GRAB_FUSE_LOW
					IF IS_PED_WEARING_HIGH_HEELS(LocalPlayerPed)
						sIWAnims.sIW_AnimDict = "anim@scripted@player@freemode@tun_prep_ig1_grab_low@heeled@"
					ELSE
						sIWAnims.sIW_AnimDict = "anim@scripted@player@freemode@tun_prep_ig1_grab_low@male@"
					ENDIF
					
					sIWAnims.sPlayerAnims.sIW_Enter = "grab_low"
					sIWAnims.sPlayerFaceAnims.sIW_Enter = "grab_low_facial"
					
					sIWAnims.sPlayerAnims.fPropHideAfterPhase = 0.275
				BREAK
				
				CASE ciIW_ALTANIMSET__GRAB_FUSE_MID
					IF IS_PED_WEARING_HIGH_HEELS(LocalPlayerPed)
						sIWAnims.sIW_AnimDict = "anim@scripted@player@freemode@tun_prep_grab_midd_ig3@heeled@"
					ELSE
						sIWAnims.sIW_AnimDict = "anim@scripted@player@freemode@tun_prep_grab_midd_ig3@male@"
					ENDIF
					
					sIWAnims.sPlayerAnims.sIW_Enter = "tun_prep_grab_midd_ig3"
					sIWAnims.sPlayerFaceAnims.sIW_Enter = "tun_prep_grab_midd_ig3_facial"
					
					sIWAnims.sPlayerAnims.fPropHideAfterPhase = 0.325
				BREAK
				
				CASE ciIW_ALTANIMSET__GRAB_FUSE_HIGH
					IF IS_PED_WEARING_HIGH_HEELS(LocalPlayerPed)
						sIWAnims.sIW_AnimDict = "anim@scripted@player@freemode@tun_prep_ig3_grab_high@heeled@"
					ELSE
						sIWAnims.sIW_AnimDict = "anim@scripted@player@freemode@tun_prep_ig3_grab_high@male@"
					ENDIF
					
					sIWAnims.sPlayerAnims.sIW_Enter = "tun_prep_ig3_grab_high"
					
					sIWAnims.sPlayerAnims.fPropHideAfterPhase = 0.275
				BREAK
			
			ENDSWITCH
			
			sIWAnims.sInteractWithEntityAnims.mnIW_ModelName = REH_PROP_REH_FUSE_01A
			sIWAnims.sInteractWithEntityAnims.bUsePlayerAnimPhase = TRUE
			
		BREAK
		
	ENDSWITCH
ENDPROC

PROC CACHE_ANIM_INFO_FOR_INTERACT_WITH(INTERACT_WITH_PARAMS& sInteractWithParams)
	GET_ANIM_INFO_FOR_INTERACT_WITH(sInteractWithParams, sInteractWithVars.sIWAnims, sInteractWithVars.iInteractWith_SubAnimPreset)
ENDPROC

FUNC BOOL IS_INTERACT_WITH_ONLY_COMPLETE_WHEN_LOOPING(INTERACT_WITH_PARAMS& sInteractWithParams)
	
	IF IS_INTERACT_WITH_ANIM_PRESET_A_HOLD_INPUT(sInteractWithParams.iInteractWith_AnimPreset, sInteractWithParams.iInteractWith_AltAnimSet)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_CURRENT_INTERACT_WITH_HAVE_A_LOOPING_SECTION(INTERACT_WITH_PARAMS& sInteractWithParams)
	
	IF IS_INTERACT_WITH_ANIM_PRESET_A_HOLD_INPUT(sInteractWithParams.iInteractWith_AnimPreset, sInteractWithParams.iInteractWith_AltAnimSet)
		RETURN TRUE
	ENDIF

	RETURN NOT IS_STRING_NULL_OR_EMPTY(sInteractWithVars.sIWAnims.sPlayerAnims.sIW_Looping)
ENDFUNC

FUNC BOOL DOES_CURRENT_INTERACT_WITH_HAVE_AN_EXIT_SECTION(INTERACT_WITH_PARAMS& sInteractWithParams)
	UNUSED_PARAMETER(sInteractWithParams)
	RETURN NOT IS_STRING_NULL_OR_EMPTY(sInteractWithVars.sIWAnims.sPlayerAnims.sIW_Exit)
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Misc Minigame Functions -----------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Functions related to minigames that don't need their own full section in this header.  ---------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC STRING GET_UNDERWATER_TUNNEL_WELD_ANIM_DICT()
	RETURN "ANIM@SCRIPTED@HEIST@IG12_UNDERWATER_TORCH@MALE@"
ENDFUNC

FUNC STRING GET_ENTER_SAFE_COMBINATION_ANIM_DICT()
	RETURN "ANIM@SCRIPTED@HEIST@IG15_SAFE_CRACK@MALE@"	
ENDFUNC
	
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Text Labels
// ##### Description: Wrappers for returnign content specific text labels
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC STRING GET_LOBBY_LEADER_GONE_FAIL_TEXT()
	IF FMMC_CURRENTLY_USING_PLAYER_OWNED_YACHT()
	OR IS_BIT_SET(iLocalBoolCheck12, LBOOL12_LOBBY_LEADER_QUIT_YACHT_CHANGED)
		RETURN "LCLHQY"
	ENDIF
	
	RETURN "LCLHQ"
ENDFUNC

FUNC STRING GET_OBJECT_CARRIER_TICKER_TEXT(BOOL bLocalPlayer)
	
	#IF FEATURE_HEIST_ISLAND	
	IF HEIST_ISLAND_FLOW_IS_CURRENT_MISSION_HEIST_ISLAND_FINALE()
		IF bLocalPlayer
		RETURN "FMMC_TRGT_LI"
	ELSE
		RETURN "FMMC_TRGT_RI"
	ENDIF
	ENDIF
	#ENDIF
	
	IF bLocalPlayer
		RETURN "FMMC_TRGT_L"
	ELSE
		RETURN "FMMC_TRGT_R"
	ENDIF
	
ENDFUNC

FUNC STRING GET_HELP_TEXT_FOR_SECUROSERV_HACK(BOOL bPhoneOpen = FALSE)
	
	IF GET_ULP_MISSION_FROM_ROOT_CONTENT_ID_HASH(g_FMMC_STRUCT.iRootContentIDHash) = ciULP_MISSION_2_COUNTERINTELLIGENCE
		IF bPhoneOpen
			RETURN "SECUROHELP2_RD"
		ELSE
			RETURN "SECUROHELP1_RD"
		ENDIF
	ENDIF
	
	IF GET_ULP_MISSION_FROM_ROOT_CONTENT_ID_HASH(g_FMMC_STRUCT.iRootContentIDHash) = ciULP_MISSION_6_CLEANUP
		IF bPhoneOpen
			RETURN "SECUROHELP2_GU"
		ELSE
			RETURN "SECUROHELP1_GU"
		ENDIF
	ENDIF
	
	IF bPhoneOpen
		RETURN "SECUROHELP2"
	ELSE
		RETURN "SECUROHELP1"
	ENDIF
	
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Generic Render Targets
// ##### Description: Functions related to the shared render target system which can be used by all entity types.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL DOES_SCALEFORM_FEATURE_A_FILLABLE_BAR(SHARED_RT_VARS& sRTVars)
	RETURN IS_BIT_SET(sRTVars.iSharedRTBitset, ciSHARED_RT_BITSET__FEATURES_FILLABLE_BAR)
ENDFUNC

FUNC BOOL DOES_SCALEFORM_FEATURE_A_TIMER(SHARED_RT_VARS& sRTVars)
	RETURN IS_BIT_SET(sRTVars.iSharedRTBitset, ciSHARED_RT_BITSET__FEATURES_TIMER)
ENDFUNC

PROC FILL_RENDER_TARGET_VARS_FROM_MODEL_NAME(SHARED_RT_VARS& sRTVars, MODEL_NAMES mnModel)
	
	IF mnModel = PROP_MONITOR_01A
		sRTVars.sTextureDict = ""
	ENDIF
	
	IF mnModel = INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_trailer_tv"))
		sRTVars.sRenderTargetName = "gr_trailerTV_01"
		sRTVars.sTextureDict = "Prop_Screen_GR_Login"
		sRTVars.sTextureName = "Prop_Screen_GR_Login"
	ENDIF
	
	IF mnModel = INT_TO_ENUM(MODEL_NAMES, HASH("h4_prop_h4_safe_01a"))
		sRTVars.sRenderTargetName = "safe_01a"
		sRTVars.sScaleformName = "DIGITAL_SAFE_DISPLAY"
		sRTVars.fCentreX = 0.205
		sRTVars.fCentreY = 0.170
		sRTVars.fWidth = 0.4
		sRTVars.fHeight = 0.4
	ENDIF
	
	IF mnModel = INT_TO_ENUM(MODEL_NAMES, HASH("h4_Prop_h4_LD_Bomb_02a"))
		sRTVars.sRenderTargetName = "Prop_h4_LD_Bomb_02a"
		sRTVars.sScaleformName = "BOMB_COUNTDOWN"
		sRTVars.sUpdateTimerMethodName = "SET_TIME"
		
		IF sRTVars.iMatchInteriorDestructionTimerIndex > -1
			sRTVars.iSplitRenderTargetIndex = sRTVars.iMatchInteriorDestructionTimerIndex
		ELSE
			sRTVars.iSplitRenderTargetIndex = 0
		ENDIF
		
		SWITCH sRTVars.iSplitRenderTargetIndex
			CASE 0
				sRTVars.fCentreX = 0.420
				sRTVars.fCentreY = 0.400
			BREAK
			CASE 1
				sRTVars.fCentreX = -0.035
				sRTVars.fCentreY = 0.400
			BREAK
			CASE 2
				sRTVars.fCentreX = 0.420
				sRTVars.fCentreY = -0.060
			BREAK
			CASE 3
				sRTVars.fCentreX = -0.035
				sRTVars.fCentreY = -0.060
			BREAK
		ENDSWITCH
		
		sRTVars.fWidth = 0.9
		sRTVars.fHeight = 0.9
		
		SET_BIT(sRTVars.iSharedRTBitset, ciSHARED_RT_BITSET__FEATURES_TIMER)
	ENDIF
	
	IF mnModel = REH_PROP_REH_BOMB_TECH_01A
		sRTVars.sRenderTargetName = "Bomb_Tech_01a"
		sRTVars.sScaleformName = "BOMB_COUNTDOWN"
		sRTVars.sUpdateTimerMethodName = "SET_TIME"
		sRTVars.iSplitRenderTargetIndex = 0
		
		sRTVars.fCentreX = 0.420
		sRTVars.fCentreY = 0.400
		
		
		sRTVars.fWidth = 0.9
		sRTVars.fHeight = 0.9
		
		SET_BIT(sRTVars.iSharedRTBitset, ciSHARED_RT_BITSET__DONT_CARE_IF_ON_SCREEN)
		SET_BIT(sRTVars.iSharedRTBitset, ciSHARED_RT_BITSET__FEATURES_TIMER)
	ENDIF
	
	IF mnModel = INT_TO_ENUM(MODEL_NAMES, HASH("tr_Prop_TR_Monitor_01a"))
		sRTVars.sRenderTargetName = "Prop_TR_Monitor_01a"
		sRTVars.sScaleformName = "NAS_UPLOAD"
		sRTVars.sInitMethodName = "SHOW_CODE_SCREEN"
		sRTVars.sUpdateFillableBarMethodName = "UPDATE_PROGRESS"
		sRTVars.sProgressBarTitle = "IW_UPL"
		
		sRTVars.fCentreX = 0.2
		sRTVars.fCentreY = 0.36
		sRTVars.fWidth = 0.4
		sRTVars.fHeight = 0.7
		
		SET_BIT(sRTVars.iSharedRTBitset, ciSHARED_RT_BITSET__FEATURES_FILLABLE_BAR)
	ENDIF
	
	IF mnModel = INT_TO_ENUM(MODEL_NAMES, HASH("tr_Prop_TR_Monitor_01b"))
		sRTVars.sRenderTargetName = "Prop_TR_Monitor_01b"
		sRTVars.sTextureDict = "Prop_screen_TR_NAS_Upload_B"
		sRTVars.sTextureName = "Prop_screen_TR_NAS_Upload_B_02"
		
		sRTVars.fCentreX = 0.5
		sRTVars.fCentreY = 0.5
		sRTVars.fWidth = 1.0
		sRTVars.fHeight = 1.0
	ENDIF
	
ENDPROC

FUNC BOOL DOES_MODEL_HAVE_A_SHARED_RENDER_TARGET(MODEL_NAMES mnModel)
	SHARED_RT_VARS sTemp
	FILL_RENDER_TARGET_VARS_FROM_MODEL_NAME(sTemp, mnModel)
	
	IF NOT IS_STRING_NULL_OR_EMPTY(sTemp.sRenderTargetName)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Poison Gas Screen FX
// ##### Description: Used in the Health Drain Zone and in the Players Wake Up Sequence
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Apply or remove a poison gas screen effect, where the camera shakes and the screen turns slightly green
/// PARAMS:
///    bApply - Set true to enable the screen effect, Set false to remove the currently active screen effect
///    bShakeGameplayCam - Set true to make the gameplay cam shake while the poison effect is on screen
///    bUseShortTransition - Set true to make the screen effect appear faster
///    bActivatedByScript - Set true if calling this function directly via script
PROC APPLY_OR_REMOVE_POISON_GAS_SCREEN_EFFECT(BOOL bApply, BOOL bShakeGameplayCam = TRUE, FLOAT fTransitionTime = cfPoisonGasFXTransitionTimeNormal, BOOL bActivatedByScript = FALSE)
	
	IF (bApply AND IS_BIT_SET(iPoisonGasBS, ciPoisonGasAppliedScreenFX))
	OR (!bApply AND NOT IS_BIT_SET(iPoisonGasBS, ciPoisonGasAppliedScreenFX))
		EXIT
	ENDIF
	
	IF GET_TIMECYCLE_TRANSITION_MODIFIER_INDEX() > -1
	OR GET_IS_TIMECYCLE_TRANSITIONING_OUT()
		EXIT
	ENDIF
	
	IF bApply
	
		PRINTLN("[SCREEN_FX] APPLY_OR_REMOVE_POISON_GAS_SCREEN_EFFECT - Applying screen effect")
		
		IF bShakeGameplayCam
			PRINTLN("[SCREEN_FX] APPLY_OR_REMOVE_POISON_GAS_SCREEN_EFFECT - Shaking cam")
			SHAKE_GAMEPLAY_CAM("DRUNK_SHAKE", 1.0)
		ENDIF
		
		PRINTLN("[SCREEN_FX] APPLY_OR_REMOVE_POISON_GAS_SCREEN_EFFECT - Applying filter")
		SET_TRANSITION_TIMECYCLE_MODIFIER("spectator6", fTransitionTime)
		
		IF bActivatedByScript
		AND NOT IS_BIT_SET(iPoisonGasBS, ciPoisonGasActivatedByScript)
			SET_BIT(iPoisonGasBS, ciPoisonGasActivatedByScript)
		ENDIF
		
		SET_BIT(iPoisonGasBS, ciPoisonGasAppliedScreenFX)
		
	ELSE
		
		PRINTLN("[SCREEN_FX] APPLY_OR_REMOVE_POISON_GAS_SCREEN_EFFECT - Removing screen effect")
		
		IF bShakeGameplayCam
			PRINTLN("[SCREEN_FX] APPLY_OR_REMOVE_POISON_GAS_SCREEN_EFFECT - Stopping shake")
			STOP_GAMEPLAY_CAM_SHAKING()
		ENDIF
		
		IF GET_TIMECYCLE_MODIFIER_INDEX() != -1
			PRINTLN("[SCREEN_FX] APPLY_OR_REMOVE_POISON_GAS_SCREEN_EFFECT - Removing filter")
			SET_TRANSITION_OUT_OF_TIMECYCLE_MODIFIER(fTransitionTime)
		ENDIF
				
		IF bActivatedByScript
		AND IS_BIT_SET(iPoisonGasBS, ciPoisonGasActivatedByScript)
			CLEAR_BIT(iPoisonGasBS, ciPoisonGasActivatedByScript)
		ENDIF
		
		CLEAR_BIT(iPoisonGasBS, ciPoisonGasAppliedScreenFX)
		
	ENDIF
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: React to Smoke
// ##### Description: 
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC STRING GET_COUGH_SOUND_EFFECT()

	SWITCH GET_PARTICIPANT_NUMBER_IN_TEAM()
	
		CASE 0
			IF IS_PLAYER_FEMALE()
				RETURN "Female_01"
			ELSE
				RETURN "Male_01"
			ENDIF
		BREAK
		
		CASE 1
			IF IS_PLAYER_FEMALE()
				RETURN "Female_02"
			ELSE
				RETURN "Male_02"
			ENDIF
		BREAK
		
		CASE 2
			IF IS_PLAYER_FEMALE()
				RETURN "Female_03"
			ELSE
				RETURN "Male_03"
			ENDIF
		BREAK
		
		CASE 3
			IF IS_PLAYER_FEMALE()
				RETURN "Female_04"
			ELSE
				RETURN "Male_04"
			ENDIF
		BREAK
		
	ENDSWITCH

	IF IS_PLAYER_FEMALE()
		RETURN "Female_01"
	ELSE
		RETURN "Male_01"
	ENDIF
	
ENDFUNC

FUNC BOOL LOAD_REACT_TO_SMOKE_ANIMATION()

	IF IS_BIT_SET(iLocalBoolCheck35, LBOOL35_LOADED_REACT_TO_SMOKE_ANIM)
		RETURN TRUE
	ENDIF

	STRING sAnimDictName = PICK_STRING(IS_PED_WEARING_HIGH_HEELS(LocalPlayerPed), "anim@scripted@ulp_missions@react_to_smoke@heeled@", "anim@scripted@ulp_missions@react_to_smoke@male@")

	sReactToSmokeAnimData.type = APT_SINGLE_ANIM
	sReactToSmokeAnimData.phase0 = 0.0
	sReactToSmokeAnimData.rate0 = 1.0
	sReactToSmokeAnimData.dictionary0 = sAnimDictName
	sReactToSmokeAnimData.flags = (AF_DEFAULT)
	
	// Currently only selected action_02, change the range to (0, 2) if you want
	// to include action_01
	iReactToSmokeAnim = GET_RANDOM_INT_IN_RANGE(1, 2)
	
	SWITCH iReactToSmokeAnim
		CASE 0
			sReactToSmokeAnimData.anim0 = "action_01"
			sReactToSmokeAnimData.anim1 = "action_01_facial"
		BREAK
		CASE 1
			sReactToSmokeAnimData.anim0 = "action_02"
			sReactToSmokeAnimData.anim1 = "action_02_facial"
		BREAK
	ENDSWITCH
	
	IF REQUEST_AND_LOAD_ANIM_DICT(sAnimDictName)
		SET_BIT(iLocalBoolCheck35, LBOOL35_LOADED_REACT_TO_SMOKE_ANIM)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC GENERATE_REACT_TO_SMOKE_REACT_VARIATION()
	iReactToSmokeReactVariation = GET_RANDOM_INT_IN_RANGE(0, 2500)
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Players Wake Up
// ##### Description: 
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL LOAD_PLAYERS_WAKE_UP_ANIMATION()

	IF IS_BIT_SET(iLocalBoolCheck35, LBOOL35_LOADED_PLAYERS_WAKE_UP_ANIM)
		RETURN TRUE
	ENDIF
		
	IF NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		RETURN FALSE
	ENDIF

	STRING sAnimDictName = PICK_STRING(IS_PED_WEARING_HIGH_HEELS(LocalPlayerPed), "anim@scripted@ulp_missions@players_wake_up@heeled@", "anim@scripted@ulp_missions@players_wake_up@male@")

	sPlayersWakeUpAnimData.type = APT_SINGLE_ANIM
	sPlayersWakeUpAnimData.phase0 = 0.0
	sPlayersWakeUpAnimData.rate0 = 1.0
	sPlayersWakeUpAnimData.dictionary0 = sAnimDictName
	sPlayersWakeUpAnimData.flags = (AF_DEFAULT)
	
	SWITCH GET_SEAT_PED_IS_IN(LocalPlayerPed)
		CASE VS_BACK_LEFT 	sPlayersWakeUpAnimData.anim0 = "action_player_0" BREAK
		CASE VS_DRIVER 		sPlayersWakeUpAnimData.anim0 = "action_player_1" BREAK
		CASE VS_BACK_RIGHT  sPlayersWakeUpAnimData.anim0 = "action_player_2" BREAK
		CASE VS_FRONT_RIGHT sPlayersWakeUpAnimData.anim0 = "action_player_3" BREAK
	ENDSWITCH
	
	IF REQUEST_AND_LOAD_ANIM_DICT(sAnimDictName)
		SET_BIT(iLocalBoolCheck35, LBOOL35_LOADED_PLAYERS_WAKE_UP_ANIM)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

PROC PWU_CACHE_LOCAL_PLAYER_CAMERA_VIEW_MODE()

	IF NOT IS_PED_SITTING_IN_FRONT_OF_VEHICLE(LocalPlayerPed)
		EXIT
	ENDIF
	
	PRINTLN("[PLAYERS_WAKE_UP] PWU_RESTORE_LOCAL_PLAYER_CAMERA_VIEW_MODE - Caching local players camera view mode!")
	ePlayersWakeUpCachedCameraView = GET_CAM_VIEW_MODE_FOR_CONTEXT(GET_CAM_ACTIVE_VIEW_MODE_CONTEXT())
	
ENDPROC

PROC PWU_RESTORE_LOCAL_PLAYER_CAMERA_VIEW_MODE()

	IF NOT IS_PED_SITTING_IN_FRONT_OF_VEHICLE(LocalPlayerPed)
		EXIT
	ENDIF

	PRINTLN("[PLAYERS_WAKE_UP] PWU_RESTORE_LOCAL_PLAYER_CAMERA_VIEW_MODE - Restoring local players cached camera view mode!")
	SET_CAM_VIEW_MODE_FOR_CONTEXT(GET_CAM_ACTIVE_VIEW_MODE_CONTEXT(), ePlayersWakeUpCachedCameraView)
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Cargo Plane Finale
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_CACHE_CARGO_PLANE(VEHICLE_INDEX viVehicle)
	viCargoPlane = viVehicle
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Camera Shake
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL SHOULD_BLOCK_SHAKE_WHILE_AIMING(INT iTeam, INT iRule)

	BOOL bBlockShakeWhileAiming
	IF GET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_IsAimingGun, FALSE)
	AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSixteen[iRule], ciBS_RULE16_ALLOW_CAMERA_SHAKE_WHILE_AIMING)
		bBlockShakeWhileAiming = TRUE
	ENDIF
	
	RETURN bBlockShakeWhileAiming
ENDFUNC

FUNC FLOAT GET_CAMERA_SHAKE_INTENSITY(INT iTeam, INT iRule)

	FLOAT fCameraShakeIntensity = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].fCameraShakeIntensity[iRule]
	IF NOT IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_DLC_2_2022)
		fCameraShakeIntensity = fCameraShakeIntensity / 10
	ENDIF
	
	RETURN fCameraShakeIntensity
ENDFUNC

FUNC INT GET_SECONDARY_CAMERA_SHAKE_INTERVAL(INT iTeam, INT iRule)
	RETURN g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iSecondaryCameraShakeInterval[iRule] * 1000 + GET_RANDOM_INT_IN_RANGE(ciCameraShakeIntervalVariationMin, ciCameraShakeIntervalVariationMax)
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Audio Scene
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC STRING GET_AUDIO_SCENE_NAME_FROM_SELECTION(INT iSelection)

	SWITCH iSelection
		CASE AUDIO_SCENE_ROGUE_DRONES_DRUGGED
			RETURN "ULP2_Rogue_Drones_Drugged_Scene"
		BREAK
		CASE AUDIO_SCENE_ROGUE_DRONES_MOTEL_INTERIOR
			RETURN "ULP2_Rogue_Drones_Motel_Interior_Scene"
		BREAK
		CASE AUDIO_SCENE_ROGUE_DRONES_VAN_INTERIOR
			RETURN "ULP2_Rogue_Drones_Van_Interior_Scene"
		BREAK
	ENDSWITCH
	
	RETURN ""

ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Entity Audio
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL IS_ENTITY_AUDIO_VALID(INT iAudio)
	
	IF iAudio > -1 
	AND iAudio < ciMAX_CUSTOM_STRING_LIST_LENGTH
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL CHECK_ENTITY_AUDIO_BITSETS_ON_CREATE(INT iCreationType, INT iEntIndex)

	SWITCH iCreationType
		CASE CREATION_TYPE_OBJECTS
			IF IS_BIT_SET(iObjectCachedAudioBitset_PROCESSED_ON_CREATE, iEntIndex)
				RETURN FALSE
			ENDIF
		BREAK
		CASE CREATION_TYPE_INTERACTABLE
			IF IS_LONG_BIT_SET(iInteractableCachedAudioBitset_PROCESSED_ON_CREATE, iEntIndex)
				RETURN FALSE
			ENDIF
		BREAK
		DEFAULT
			ASSERTLN("[ENT_AUDIO] CHECK_ENTITY_AUDIO_BITSETS_ON_CREATE - Unrecognised Creation Type. Please add Creation Type to SWITCH statement.")
			RETURN FALSE
		BREAK
	ENDSWITCH
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL CHECK_ENTITY_AUDIO_BITSETS_ON_DESTROY(INT iCreationType, INT iEntIndex)

	SWITCH iCreationType
		CASE CREATION_TYPE_OBJECTS
			IF IS_BIT_SET(iObjectCachedAudioBitset_PROCESSED_ON_DESTROY, iEntIndex)
			OR NOT IS_BIT_SET(iObjectCachedAudioBitset_PROCESSED_ON_CREATE, iEntIndex)
				RETURN FALSE
			ENDIF
		BREAK
		CASE CREATION_TYPE_INTERACTABLE
			IF IS_LONG_BIT_SET(iInteractableCachedAudioBitset_PROCESSED_ON_DESTROY, iEntIndex)
			OR NOT IS_LONG_BIT_SET(iInteractableCachedAudioBitset_PROCESSED_ON_CREATE, iEntIndex)
				RETURN FALSE
			ENDIF
		BREAK
		DEFAULT
			ASSERTLN("[ENT_AUDIO] CHECK_ENTITY_AUDIO_BITSETS_ON_DESTROY - Unrecognised Creation Type. Please add Creation Type to SWITCH statement.")
			RETURN FALSE
		BREAK
	ENDSWITCH
	
	RETURN TRUE
	
ENDFUNC

PROC PROCESS_SET_ENTITY_AUDIO_BITSETS_ON_CREATE(INT iCreationType, INT iEntIndex)
	
	SWITCH iCreationType
		CASE CREATION_TYPE_OBJECTS
			SET_BIT(iObjectCachedAudioBitset_PROCESSED_ON_CREATE, iEntIndex)
		BREAK
		CASE CREATION_TYPE_INTERACTABLE
			FMMC_SET_LONG_BIT(iInteractableCachedAudioBitset_PROCESSED_ON_CREATE, iEntIndex)
		BREAK
	ENDSWITCH
	
ENDPROC

PROC PROCESS_SET_ENTITY_AUDIO_BITSETS_ON_DESTROY(INT iCreationType, INT iEntIndex)

	SWITCH iCreationType
		CASE CREATION_TYPE_OBJECTS
			SET_BIT(iObjectCachedAudioBitset_PROCESSED_ON_DESTROY, iEntIndex)
		BREAK
		CASE CREATION_TYPE_INTERACTABLE
			FMMC_SET_LONG_BIT(iInteractableCachedAudioBitset_PROCESSED_ON_DESTROY, iEntIndex)
		BREAK
	ENDSWITCH
	
ENDPROC

PROC PROCESS_ENTITY_AUDIO_PLAY_ON_CREATE(FMMC_ENTITY_AUDIO_DATA &sEntAudioData, INT iEntIndex, ENTITY_INDEX eiEntIndex)
	
	UNUSED_PARAMETER(iEntIndex)
	
	INT iSoundName = sEntAudioData.iPlayOnCreate
	INT iSoundSet = sEntAudioData.iPlayOnCreateSoundSet
	
	IF IS_ENTITY_AUDIO_VALID(iSoundName)
	AND IS_ENTITY_AUDIO_VALID(iSoundSet)
	
		TEXT_LABEL_63 tlSoundName = GET_CUSTOM_STRING_LIST_TEXT_LABEL(iSoundName)
		TEXT_LABEL_63 tlSetName = GET_CUSTOM_STRING_LIST_TEXT_LABEL(iSoundSet)
		
		PRINTLN("[ENT_AUDIO] PROCESS_ENTITY_AUDIO_ON_CREATE - Entity [", iEntIndex, "] Playing 'Play on Create' audio! Sound name: ", tlSoundName)
		PLAY_SOUND_FROM_ENTITY(-1, tlSoundName, eiEntIndex, tlSetName, FALSE)
		
	ENDIF

ENDPROC

PROC PROCESS_ENTITY_AUDIO_START_LOOP(FMMC_ENTITY_AUDIO_DATA &sEntAudioData, INT iEntIndex, ENTITY_INDEX eiEntIndex)

	UNUSED_PARAMETER(iEntIndex)
	
	INT iSoundName = sEntAudioData.iLoop
	INT iSoundSet = sEntAudioData.iLoopSoundSet
	
	IF IS_ENTITY_AUDIO_VALID(iSoundName)
	AND IS_ENTITY_AUDIO_VALID(iSoundSet)
	
		TEXT_LABEL_63 tlSoundName = GET_CUSTOM_STRING_LIST_TEXT_LABEL(iSoundName)
		TEXT_LABEL_63 tlSetName = GET_CUSTOM_STRING_LIST_TEXT_LABEL(iSoundSet)
		
		PRINTLN("[ENT_AUDIO] PROCESS_ENTITY_AUDIO_ON_CREATE - Entity [", iEntIndex, "] Starting audio loop! Sound name: ", tlSoundName)
		START_ENTITY_LOOPING_AUDIO(eiEntIndex, ZERO_VECTOR(), tlSoundName, tlSetName, FALSE)
		
	ENDIF

ENDPROC

PROC PROCESS_ENTITY_AUDIO_ON_CREATE(INT iEntIndex, ENTITY_INDEX eiEntIndex, BOOL bEntAlive, INT iCreationType)
			
	IF NOT bEntAlive
		EXIT
	ENDIF
	
	IF NOT CHECK_ENTITY_AUDIO_BITSETS_ON_CREATE(iCreationType, iEntIndex)
		EXIT
	ENDIF
	
	SWITCH iCreationType
		CASE CREATION_TYPE_OBJECTS
			PROCESS_ENTITY_AUDIO_PLAY_ON_CREATE(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iEntIndex].sEntAudioData, iEntIndex, eiEntIndex)
			PROCESS_ENTITY_AUDIO_START_LOOP(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iEntIndex].sEntAudioData, iEntIndex, eiEntIndex)
		BREAK
		CASE CREATION_TYPE_INTERACTABLE
			PROCESS_ENTITY_AUDIO_PLAY_ON_CREATE(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iEntIndex].sEntAudioData, iEntIndex, eiEntIndex)
			PROCESS_ENTITY_AUDIO_START_LOOP(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iEntIndex].sEntAudioData, iEntIndex, eiEntIndex)
		BREAK
	ENDSWITCH
	
	PROCESS_SET_ENTITY_AUDIO_BITSETS_ON_CREATE(iCreationType, iEntIndex)
	
ENDPROC

PROC PROCESS_ENTITY_AUDIO_STOP_LOOP(FMMC_ENTITY_AUDIO_DATA &sEntAudioData, INT iEntIndex, ENTITY_INDEX eiEntIndex)
	
	UNUSED_PARAMETER(iEntIndex)
	
	INT iSoundName = sEntAudioData.iLoop
	
	IF IS_ENTITY_AUDIO_VALID(iSoundName)
		
		INT iAudioSlot = GET_ENTITY_LOOPING_AUDIO_INDEX_FOR_ENTITY(eiEntIndex)
		
		IF IS_ENTITY_AUDIO_VALID(iAudioSlot)
			
			PRINTLN("[ENT_AUDIO] PROCESS_ENTITY_AUDIO_ON_DESTROY - Entity [", iEntIndex, "] Ending audio loop!")
			CLEANUP_ENTITY_LOOPING_AUDIO(iAudioSlot)
			
		ENDIF
		
	ENDIF

ENDPROC

PROC PROCESS_ENTITY_AUDIO_PLAY_ON_DESTROY(FMMC_ENTITY_AUDIO_DATA &sEntAudioData, INT iEntIndex, ENTITY_INDEX eiEntIndex)
	
	UNUSED_PARAMETER(iEntIndex)
	
	INT iSoundName = sEntAudioData.iPlayOnDestroy
	INT iSoundSet = sEntAudioData.iPlayOnDestroySoundSet
	
	IF IS_ENTITY_AUDIO_VALID(iSoundName)
	AND IS_ENTITY_AUDIO_VALID(iSoundSet)
	
		TEXT_LABEL_63 tlSoundName = GET_CUSTOM_STRING_LIST_TEXT_LABEL(iSoundName)
		TEXT_LABEL_63 tlSetName = GET_CUSTOM_STRING_LIST_TEXT_LABEL(iSoundSet)
		
		PRINTLN("[ENT_AUDIO] PROCESS_ENTITY_AUDIO_ON_DESTROY - Entity [", iEntIndex, "] Playing 'Play on Destroy' audio! Sound name: ", tlSoundName)
		PLAY_SOUND_FROM_ENTITY(-1, tlSoundName, eiEntIndex, tlSetName, FALSE)
		
	ENDIF
	
ENDPROC

PROC PROCESS_ENTITY_AUDIO_ON_DESTROY(INT iEntIndex, ENTITY_INDEX eiEntIndex, BOOL bEntAlive, INT iCreationType)

	IF bEntAlive
		EXIT
	ENDIF
	
	IF NOT CHECK_ENTITY_AUDIO_BITSETS_ON_DESTROY(iCreationType, iEntIndex)
		EXIT
	ENDIF

	SWITCH iCreationType
		CASE CREATION_TYPE_OBJECTS
			PROCESS_ENTITY_AUDIO_STOP_LOOP(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iEntIndex].sEntAudioData, iEntIndex, eiEntIndex)
			PROCESS_ENTITY_AUDIO_PLAY_ON_DESTROY(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iEntIndex].sEntAudioData, iEntIndex, eiEntIndex)
		BREAK
		CASE CREATION_TYPE_INTERACTABLE
			PROCESS_ENTITY_AUDIO_STOP_LOOP(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iEntIndex].sEntAudioData, iEntIndex, eiEntIndex)
			PROCESS_ENTITY_AUDIO_PLAY_ON_DESTROY(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iEntIndex].sEntAudioData, iEntIndex, eiEntIndex)
		BREAK
	ENDSWITCH
	
	PROCESS_SET_ENTITY_AUDIO_BITSETS_ON_DESTROY(iCreationType, iEntIndex)
	
ENDPROC

PROC PROCESS_ENTITY_AUDIO(INT iEntIndex, ENTITY_INDEX eiEntIndex, BOOL bEntAlive, INT iCreationType)

	PROCESS_ENTITY_AUDIO_ON_CREATE(iEntIndex, eiEntIndex, bEntAlive, iCreationType)
	
	PROCESS_ENTITY_AUDIO_ON_DESTROY(iEntIndex, eiEntIndex, bEntAlive, iCreationType)
	
ENDPROC
