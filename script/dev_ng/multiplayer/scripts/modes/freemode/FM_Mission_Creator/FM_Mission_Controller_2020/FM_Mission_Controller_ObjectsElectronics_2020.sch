// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### System: GTA V Online Mission Controller - Drones -----------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Script Name: FM_Mission_Controller_2020.sc ------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Contains the processing and helper functions for Entities using Drone Functionality.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### This script was forked from FM_Mission_Controller.sc at the beginning of 2020		 																				  
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

USING "FM_Mission_Controller_ObjectsMinigames_2020.sch"

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: FMMC Drones
// ##### Description: Helpers and wrappers for FMMC Drones Systems
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

// ###########################
// Drone Asset Helpers ###########################
// ###########################

FUNC BOOL LOAD_FMMC_DRONE_MUZZLE_FLASH()
	REQUEST_NAMED_PTFX_ASSET("scr_ie_vv")
	IF NOT HAS_NAMED_PTFX_ASSET_LOADED("scr_ie_vv")		
		RETURN FALSE
	ENDIF	
	USE_PARTICLE_FX_ASSET("scr_ie_vv")	
	RETURN TRUE
ENDFUNC

FUNC STRING GET_FMMC_DRONE_SOUND_SET()
	RETURN "DLC_MPSUM2_Drone_Sounds"
ENDFUNC


PROC STOP_FMMC_DRONE_FLIGHT_LOOP_SFX(INT iDrone)
	
	IF iDrone = -1
		EXIT
	ENDIF
	
	IF sLocalFmmcDroneData[iDrone].iFlightLoopSFX = -1
		EXIT
	ENDIF
	
	IF HAS_SOUND_FINISHED(sLocalFmmcDroneData[iDrone].iFlightLoopSFX)
		RELEASE_SOUND_ID(sLocalFmmcDroneData[iDrone].iFlightLoopSFX)
		sLocalFmmcDroneData[iDrone].iFlightLoopSFX = -1
		EXIT
	ENDIF
	
	STOP_SOUND(sLocalFmmcDroneData[iDrone].iFlightLoopSFX)
	RELEASE_SOUND_ID(sLocalFmmcDroneData[iDrone].iFlightLoopSFX)
	sLocalFmmcDroneData[iDrone].iFlightLoopSFX = -1
	
ENDPROC 

PROC PLAY_FMMC_DRONE_FLIGHT_LOOP_SFX(FMMC_DRONE_STATE &sDroneState, OBJECT_INDEX oiDrone)
	
	IF sDroneState.iIndex = -1
		EXIT
	ENDIF
		
	SWITCH sDroneState.mn
		CASE REH_PROP_REH_DRONE_02A
			IF sLocalFmmcDroneData[sDroneState.iIndex].iFlightLoopSFX != -1
				IF NOT HAS_SOUND_FINISHED(sLocalFmmcDroneData[sDroneState.iIndex].iFlightLoopSFX)
					EXIT
				ELSE
					RELEASE_SOUND_ID(sLocalFmmcDroneData[sDroneState.iIndex].iFlightLoopSFX)
					sLocalFmmcDroneData[sDroneState.iIndex].iFlightLoopSFX = -1
				ENDIF
			ENDIF
				
			sLocalFmmcDroneData[sDroneState.iIndex].iFlightLoopSFX = GET_SOUND_ID()
			
			PRINTLN("PLAY_FMMC_DRONE_DESTROYED_SFX - Starting Loop")
			
			PLAY_SOUND_FROM_ENTITY(sLocalFmmcDroneData[sDroneState.iIndex].iFlightLoopSFX, "Flight_loop", oiDrone, GET_FMMC_DRONE_SOUND_SET(), TRUE, 9999) // Looping sound, players can come into proximity of it from a unknown distance.		
		BREAK
	ENDSWITCH
			
ENDPROC

PROC PLAY_FMMC_DRONE_DESTROYED_SFX(MODEL_NAMES mn, VECTOR vPos)
	
	SWITCH mn
		CASE REH_PROP_REH_DRONE_02A
			PLAY_SOUND_FROM_COORD(-1, "Destroyed", vPos, GET_FMMC_DRONE_SOUND_SET(), TRUE, 50)
		BREAK
	ENDSWITCH
	
ENDPROC

PROC PLAY_FMMC_DRONE_BUMP_SFX(MODEL_NAMES mn, VECTOR vPos)
	
	SWITCH mn
		CASE REH_PROP_REH_DRONE_02A
			PLAY_SOUND_FROM_COORD(-1, "Bump", vPos, GET_FMMC_DRONE_SOUND_SET(), TRUE, 50)
		BREAK
	ENDSWITCH
			
ENDPROC

FUNC MODEL_NAMES GET_MODEL_SWAP_WHEN_IT_IS_DESROYED(OBJECT_INDEX oiObject)
	MODEL_NAMES mn = GET_ENTITY_MODEL(oiObject)
	
	SWITCH mn
		CASE REH_PROP_REH_DRONE_02A
			RETURN REH_PROP_REH_DRONE_BRK_02A
		BREAK
	ENDSWITCH
	
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

FUNC BOOL SHOULD_MODEL_SWAP_WHEN_IT_IS_DESROYED(OBJECT_INDEX oiObject)
	RETURN GET_MODEL_SWAP_WHEN_IT_IS_DESROYED(oiObject) != DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

// ###########################
// Shared ###########################
// ###########################

FUNC STRING GET_FMMC_DRONE_STATE_NAME(INT iState)
	SWITCH iState
		CASE ciTASK_DRONE_CHOOSE_NEW_TASK				RETURN "ciTASK_DRONE_CHOOSE_NEW_TASK"
		CASE ciTASK_DRONE_GOTO_COORDS					RETURN "ciTASK_DRONE_GOTO_COORDS"
		CASE ciTASK_DRONE_ATTACK_PLAYERS				RETURN "ciTASK_DRONE_ATTACK_PLAYERS"
		CASE ciTASK_DRONE_SELF_DESTRUCT_CHARGE_PLAYERS	RETURN "ciTASK_DRONE_SELF_DESTRUCT_CHARGE_PLAYERS"
	ENDSWITCH
	
	RETURN "Invalid"
ENDFUNC

PROC SET_FMMC_DRONE_IS_NOW_DEAD(INT iDrone)
	IF iDrone = -1
		EXIT
	ENDIF	
	PRINTLN("[FMMC_DRONES ", iDrone, "][CLIENT] - SET_FMMC_DRONE_IS_NOW_DEAD - Drone has been flagged as dead")
	SET_BIT(sLocalFmmcDroneData[iDrone].iBS, ciFMMCDRONE_BS_Dead)
ENDPROC

FUNC BOOL IS_FMMC_DRONE_IN_A_DEAD_STATE(FMMC_DRONE_STATE &sDroneState)
	
	IF IS_BIT_SET(sLocalFmmcDroneData[sDroneState.iIndex].iBS, ciFMMCDRONE_BS_Dead)
	OR NOT sDroneState.bAlive
	OR sDroneState.bBroken
	OR GET_ENTITY_HEALTH(sDroneState.oiIndex) <= 0
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_FREE_DRONE_INDEX()
	
	INT i = 0
	FOR i = 0 TO ciMAX_FMMC_DRONES_ACTIVE-1
		IF MC_ServerBD.iDrone_EntityIndex[i] = -1
		AND MC_ServerBD.iDrone_EntityType[i] = ciENTITY_TYPE_NONE
			RETURN i
		ENDIF
	ENDFOR
	
	RETURN -1
ENDFUNC

FUNC INT GET_DRONE_INDEX_FROM_ENTITY_ID_AND_TYPE(INT iEntityID, INT iEntityType)
	
	INT i = 0
	FOR i = 0 TO ciMAX_FMMC_DRONES_ACTIVE-1
		IF MC_ServerBD.iDrone_EntityIndex[i] = iEntityID
		AND MC_ServerBD.iDrone_EntityType[i] = iEntityType
			RETURN i
		ENDIF
	ENDFOR
	
	RETURN -1
ENDFUNC

FUNC VECTOR GET_FMMC_DRONE_MOVEMENT_WOBBLE_VECTOR(LOCAL_FMMC_DRONE_DATA &sDroneDataLocal, FMMC_DRONE_STATE &sDroneState)
	
	IF IS_VECTOR_ZERO(sDroneDataLocal.vHoverWobbleDir)
		sDroneDataLocal.vHoverWobbleDir.x = GET_RANDOM_FLOAT_IN_RANGE(-0.25, 0.25)
		sDroneDataLocal.vHoverWobbleDir.y = GET_RANDOM_FLOAT_IN_RANGE(-0.25, 0.25)
		sDroneDataLocal.vHoverWobbleDir.z = GET_RANDOM_FLOAT_IN_RANGE(-0.25, 0.25)
	ELSE
		IF NOT ARE_VECTORS_ALMOST_EQUAL(sDroneDataLocal.vTarget, sDroneState.vPos, ciFMMCDRONE_WOBBLE_RESET_THRESHOLD)
			sDroneDataLocal.vHoverWobbleDir = NORMALISE_VECTOR(sDroneDataLocal.vTarget - sDroneState.vPos)
			sDroneDataLocal.vHoverWobbleDir *= 0.2
			sDroneDataLocal.vHoverWobbleDir.x += GET_RANDOM_FLOAT_IN_RANGE(0.01, 0.03)
			sDroneDataLocal.vHoverWobbleDir.y += GET_RANDOM_FLOAT_IN_RANGE(0.01, 0.03)
			sDroneDataLocal.vHoverWobbleDir.z += GET_RANDOM_FLOAT_IN_RANGE(0.01, 0.03)
		ENDIF
	ENDIF	
	
	VECTOR vWobble = sDroneDataLocal.vHoverWobbleDir
	vWobble.x = sDroneDataLocal.vHoverWobbleDir.x * GET_RANDOM_FLOAT_IN_RANGE(0.005, 0.02)
	vWobble.y = sDroneDataLocal.vHoverWobbleDir.y * GET_RANDOM_FLOAT_IN_RANGE(0.005, 0.02)
	vWobble.z = sDroneDataLocal.vHoverWobbleDir.z * GET_RANDOM_FLOAT_IN_RANGE(0.005, 0.02)
	
	RETURN vWobble
	
ENDFUNC

FUNC FLOAT GET_FMMC_DRONE_BASE_MOVEMENT_SPEED(FMMC_DRONE_DATA &eDroneData, FMMC_DRONE_STATE &sDroneState)
	FLOAT fSpeed = (ciFMMCDRONE_BASE_SPEED * eDroneData.fDroneSpeedMultiplier)
	
	SWITCH MC_ServerBD.iDrone_State[sDroneState.iIndex]
		CASE ciTASK_DRONE_GOTO_COORDS
			INT iCreatorMultiplier
			FLOAT fCreatorMultiplier
			iCreatorMultiplier = GET_ASSOCIATED_GOTO_TASK_DATA__VEHICLE_GOTO_SPEED(MC_serverBD.iDrone_AssociatedGotoPoolIndex[sDroneState.iIndex], g_FMMC_STRUCT.sAssociatedGotoPool[eDroneData.iAssociatedPoolStartingIndex].sAssociatedGotoTaskData, MC_serverBD.iDrone_AssociatedGotoProgress[sDroneState.iIndex])			
			fCreatorMultiplier = TO_FLOAT(iCreatorMultiplier)/100
			fSpeed *= 1.0+fCreatorMultiplier
		BREAK
	ENDSWITCH
	
	RETURN fSpeed
ENDFUNC

PROC CLEAR_FMMC_DRONE_ASS_GOTO_DATA(INT iDrone)
	PRINTLN("[FMMC_DRONES ", iDrone, "][CLIENT] - SET_FMMC_DRONE_SYSTEMS_CLIENT_VECTOR_TARGET - CLEAR_PED_ASS_GOTO_DATA - Clearing Data")
			
	INT iBitset = iDrone / 32
	INT iBit = iDrone % 32	
	
	CLEAR_BIT(sLocalSharedAssGotoTaskData.iStartedGotoBitset[iBitset], iBit)
	CLEAR_BIT(sLocalSharedAssGotoTaskData.iCompletedArrivedBitset[iBitset], iBit)
	CLEAR_BIT(sLocalSharedAssGotoTaskData.iCompletedWaitBitset[iBitset], iBit)
	CLEAR_BIT(sLocalSharedAssGotoTaskData.iCompletedAchieveHeadingBitset[iBitset], iBit)
	CLEAR_BIT(sLocalSharedAssGotoTaskData.iReassignBitset[iBitset], iBit)
	
	RESET_NET_TIMER(sLocalSharedAssGotoTaskData.tdWaitTimeStamp[iDrone])
	
ENDPROC

FUNC BOOL ARE_ANY_FMMC_DRONE_ASSOCIATED_GOTO_ACTIONS_SET(FMMC_DRONE_DATA &eDroneData, FMMC_DRONE_STATE &sDroneState)
	
	IF MC_serverBD.iDrone_AssociatedGotoProgress[sDroneState.iIndex] >= MAX_ASSOCIATED_GOTO_TASKS
		RETURN FALSE
	ENDIF
		
	IF IS_ASSOCIATED_GOTO_TASK_DATA__BIT_SET(MC_serverBD.iDrone_AssociatedGotoPoolIndex[sDroneState.iIndex], g_FMMC_STRUCT.sAssociatedGotoPool[eDroneData.iAssociatedPoolStartingIndex].sAssociatedGotoTaskData, MC_serverBD.iDrone_AssociatedGotoProgress[sDroneState.iIndex], PED_ASSOCIATED_GOTO_TASK_TYPE_ACHIEVEHEADING)
	OR IS_ASSOCIATED_GOTO_TASK_DATA__WAIT_TIME_SET(MC_serverBD.iDrone_AssociatedGotoPoolIndex[sDroneState.iIndex], g_FMMC_STRUCT.sAssociatedGotoPool[eDroneData.iAssociatedPoolStartingIndex].sAssociatedGotoTaskData, MC_serverBD.iDrone_AssociatedGotoProgress[sDroneState.iIndex])
		RETURN TRUE	
	ENDIF
	
	RETURN FALSE
ENDFUNC 

FUNC BOOL SHOULD_FMMC_DRONE_SPEED_DOWN(FMMC_DRONE_DATA &eDroneData, FMMC_DRONE_STATE &sDroneState, FLOAT fPercentageProgress)
	
	SWITCH MC_ServerBD.iDrone_State[sDroneState.iIndex]		
		CASE ciTASK_DRONE_SELF_DESTRUCT_CHARGE_PLAYERS
			RETURN FALSE
		BREAK
	ENDSWITCH
	
	IF NOT IS_ASSOCIATED_GOTO_TASK_DATA__BIT_SET(MC_serverBD.iDrone_AssociatedGotoPoolIndex[sDroneState.iIndex], g_FMMC_STRUCT.sAssociatedGotoPool[eDroneData.iAssociatedPoolStartingIndex].sAssociatedGotoTaskData, sLocalSharedAssGotoTaskData.iProgress[sDroneState.iIndex], PED_ASSOCIATED_GOTO_TASK_TYPE_NOSLOWDOWN) 
		RETURN fPercentageProgress > 0.8
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC FLOAT GET_FMMC_DRONE_MINIMUM_DISTANCE_FROM_TARGET(FMMC_DRONE_DATA &eDroneData, FMMC_DRONE_STATE &sDroneState)
	
	SWITCH MC_ServerBD.iDrone_State[sDroneState.iIndex]
		CASE ciTASK_DRONE_ATTACK_PLAYERS
			RETURN eDroneData.fMinimumCombatDistanceToTarget
		BREAK
		CASE ciTASK_DRONE_SELF_DESTRUCT_CHARGE_PLAYERS
			RETURN 0.0
		BREAK
	ENDSWITCH
	
	RETURN 0.0
	
ENDFUNC

// ###########################
// Server ###########################
// ###########################

PROC SERVER_FMMC_DRONE_SWAP_OBJECT_MODEL_WHEN_IT_IS_DESTROYED(FMMC_DRONE_STATE &sDroneState, LOCAL_FMMC_DRONE_DATA &sDroneDataLocal)
	
	IF NOT IS_FMMC_DRONE_IN_A_DEAD_STATE(sDroneState)
		EXIT
	ENDIF
	
	IF NOT SHOULD_MODEL_SWAP_WHEN_IT_IS_DESROYED(sDroneState.oiIndex)
		EXIT
	ENDIF
	
	IF NOT MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(sDroneDataLocal.iTimeStampSwapModel)			
		MC_START_NET_TIMER_WITH_TIMESTAMP(sDroneDataLocal.iTimeStampSwapModel)	
	ENDIF
	
	IF NOT MC_HAS_NET_TIMER_EXPIRED_WITH_TIME_STAMP(sDroneDataLocal.iTimeStampSwapModel, 500)		
		PRINTLN("[FMMC_DRONES ", sDroneState.iIndex, "][SERVER] - SWAP_OBJECT_MODEL_WHEN_IT_IS_DESTROYED - Waiting for events and model to settle.")
		EXIT
	ENDIF
	
	IF sDroneState.bHaveControl
		PRINTLN("[FMMC_DRONES ", sDroneState.iIndex, "][SERVER] - SWAP_OBJECT_MODEL_WHEN_IT_IS_DESTROYED - We need to swap it - processing...")
		
		IF sDroneState.iEntityType = CREATION_TYPE_OBJECTS			
			REQUEST_MODEL(GET_MODEL_SWAP_WHEN_IT_IS_DESROYED(sDroneState.oiIndex))			
			IF HAS_MODEL_LOADED(GET_MODEL_SWAP_WHEN_IT_IS_DESROYED(sDroneState.oiIndex))				
				IF NOT IS_BIT_SET(sDroneDataLocal.iBS, ciFMMCDRONE_BS_Host_ModelSwapReserveObj)		
					MC_RESERVE_NETWORK_MISSION_OBJECTS(1)
					SET_BIT(sDroneDataLocal.iBS, ciFMMCDRONE_BS_Host_ModelSwapReserveObj)		
				ENDIF
				NETWORK_INDEX niCache = MC_serverBD_1.sFMMC_SBD.niNetworkedObject[GET_OBJECT_NET_ID_INDEX(sDroneState.iEntityID)]
				MODEL_NAMES mnCachce = GET_MODEL_SWAP_WHEN_IT_IS_DESROYED(sDroneState.oiIndex)
				IF FMMC_CREATE_NET_OBJ(MC_serverBD_1.sFMMC_SBD.niNetworkedObject[GET_OBJECT_NET_ID_INDEX(sDroneState.iEntityID)], GET_MODEL_SWAP_WHEN_IT_IS_DESROYED(sDroneState.oiIndex), GET_ENTITY_COORDS(sDroneState.oiIndex), TRUE, TRUE, TRUE, FALSE, DEFAULT, TRUE, FALSE, TRUE)
					PRINTLN("[FMMC_DRONES ", sDroneState.iIndex, "][SERVER] - SWAP_OBJECT_MODEL_WHEN_IT_IS_DESTROYED - object is dead - We need to swap it - SWAPPED!!")
					DELETE_NET_ID(niCache)
					
					sDroneState.oiIndex = NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niNetworkedObject[GET_OBJECT_NET_ID_INDEX(sDroneState.iEntityID)])
					
					// Drone is dead.
					FREEZE_ENTITY_POSITION(sDroneState.oiIndex, FALSE)
					ACTIVATE_PHYSICS(sDroneState.oiIndex)
					SET_ENTITY_DYNAMIC(sDroneState.oiIndex, TRUE)
					SET_ENTITY_HAS_GRAVITY(sDroneState.oiIndex, TRUE)
															
					SET_MODEL_AS_NO_LONGER_NEEDED(mnCachce)
					MC_RESERVE_NETWORK_MISSION_OBJECTS(-1)
					CLEAR_BIT(sDroneDataLocal.iBS, ciFMMCDRONE_BS_Host_ModelSwapReserveObj)
					
					SET_ENTITY_HEALTH(sDroneState.oiIndex, 0)
				ENDIF
			ELSE
				PRINTLN("[FMMC_DRONES ", sDroneState.iIndex, "][SERVER] - SWAP_OBJECT_MODEL_WHEN_IT_IS_DESTROYED - Waiting for model to load.")
			ENDIF
		ENDIF
		
	ELSE
		NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(sDroneState.niIndex)
	ENDIF
ENDPROC

PROC SET_FMMC_DRONE_RETASK(FMMC_DRONE_STATE &sDroneState)	
	
	IF NOT bIsLocalPlayerHost
		PRINTLN("[FMMC_DRONES ", sDroneState.iIndex, "][SERVER] - SET_FMMC_DRONE_RETASK - Called from client. Ignoring..")
		// ##
		// Broadcast Event if we need to call this from Client.
		// ##
		
		EXIT
	ENDIF
	
	PRINTLN("[FMMC_DRONES ", sDroneState.iIndex, "][SERVER] - SET_FMMC_DRONE_RETASK - Should retask.")		
	
	SET_BIT(MC_ServerBD.iDrone_Retask, sDroneState.iIndex)
	
ENDPROC

PROC SET_FMMC_DRONE_RETASK_WITH_ENTITY_ID_AND_TYPE(INT iEntityID, INT iEntityType)	
		
	INT iIndex = GET_DRONE_INDEX_FROM_ENTITY_ID_AND_TYPE(iEntityID, iEntityType)
	
	IF iIndex = -1
		PRINTLN("[FMMC_DRONES ", iIndex, "][SERVER] - SET_FMMC_DRONE_RETASK - Invalid drone index. No Drone exists for EntityID: ", iEntityID, " iEntityType: ", iEntityType)
		EXIT
	ENDIF
	
	IF NOT bIsLocalPlayerHost
		PRINTLN("[FMMC_DRONES ", iIndex, "][SERVER] - SET_FMMC_DRONE_RETASK_WITH_ENTITY_ID_AND_TYPE - Called from client. Ignoring..")
		// ##
		// Broadcast Event if we need to call this from Client.
		// ##
		
		EXIT
	ENDIF
	
	PRINTLN("[FMMC_DRONES ", iIndex, "][SERVER] - SET_FMMC_DRONE_RETASK - Should retask.")		
	
	SET_BIT(MC_ServerBD.iDrone_Retask, iIndex)
	
ENDPROC

PROC CLEAR_DRONE_INDEX(INT iEntityID, INT iEntityType)	
	
	IF NOT bIsLocalPlayerHost
		EXIT
	ENDIF
	
	INT iIndex = GET_DRONE_INDEX_FROM_ENTITY_ID_AND_TYPE(iEntityID, iEntityType)

	IF iIndex = -1
		EXIT
	ENDIF
	
	DEBUG_PRINTCALLSTACK()

	PRINTLN("[FMMC_DRONES ", iIndex, "][SERVER] - CLEAR_DRONE_INDEX - CLEARING | iIndex: ", iIndex, " | Entity ID: ", MC_ServerBD.iDrone_EntityIndex[iIndex], " | Entity Type: ", MC_ServerBD.iDrone_EntityType[iIndex])
	
	MC_ServerBD.iDrone_EntityIndex[iIndex] = -1
	MC_ServerBD.iDrone_EntityType[iIndex] = ciENTITY_TYPE_NONE
		
	BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_Drone_Cleanup, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, iIndex)
	
ENDPROC

PROC SET_DRONE_INDEX(INT iEntityID, INT iEntityType)
	
	INT iIndex = GET_FREE_DRONE_INDEX()
	
	IF iIndex = -1
		PRINTLN("[FMMC_DRONES ", iIndex, "][SERVER] - SET_DRONE_INDEX - SETTING | iIndex: ", iIndex, " NO FREE INDEXES!!!")
		EXIT
	ENDIF
	
	PRINTLN("[FMMC_DRONES ", iIndex, "][SERVER] - SET_DRONE_INDEX - SETTING | iIndex: ", iIndex, " | Entity ID: ", iEntityID, " | Entity Type: ", iEntityType)
	
	MC_ServerBD.iDrone_EntityIndex[iIndex] = iEntityID
	MC_ServerBD.iDrone_EntityType[iIndex] = iEntityType
	
ENDPROC

PROC SET_FMMC_DRONE_STATE(FMMC_DRONE_STATE &sDroneState, INT iNewState)
	PRINTLN("[FMMC_DRONES ", sDroneState.iIndex, "][SERVER] - SET_FMMC_DRONE_STATE - SETTING to State: ", GET_FMMC_DRONE_STATE_NAME(iNewState), " From State: ", GET_FMMC_DRONE_STATE_NAME(MC_ServerBD.iDrone_State[sDroneState.iIndex]))
	MC_ServerBD.iDrone_State[sDroneState.iIndex] = iNewState
ENDPROC

PROC PROCESS_FMMC_DRONE_HEALTH_THRESHOLD_TASK_ACTIVATION(OBJECT_INDEX oiObject, INT &iTaskBS, INT iHealthThreshold, INT iEntityID, INT iEntityType)
	
	IF NOT bIsLocalPlayerHost
		EXIT
	ENDIF
	
	IF iHealthThreshold <= 0
		EXIT
	ENDIF

	IF IS_BIT_SET(iTaskBS, iEntityID)
		EXIT
	ENDIF
	
	FLOAT fHealthPercent = TO_FLOAT(GET_ENTITY_HEALTH(oiObject)) / TO_FLOAT(GET_ENTITY_MAX_HEALTH(oiObject))
	IF fHealthPercent*100 < iHealthThreshold		
		PRINTLN("[FMMC_DRONES][SERVER] - PROCESS_FMMC_DRONE_HEALTH_THRESHOLD_TASK_ACTIVATION - iEntityID: ", iEntityID, " iEntityType: ", iEntityType, " | fHealthPercent: ", fHealthPercent, " / ", iHealthThreshold, " | Activating Task Threshold.")
		SET_BIT(MC_ServerBD.iObjectHealthThreshActivatedBS, iEntityID)
		SET_FMMC_DRONE_RETASK_WITH_ENTITY_ID_AND_TYPE(iEntityID, iEntityType)		
	ELSE
		PRINTLN("[FMMC_DRONES][SERVER] - PROCESS_FMMC_DRONE_HEALTH_THRESHOLD_TASK_ACTIVATION - iEntityID: ", iEntityID, " iEntityType: ", iEntityType, " | fHealthPercent: ", fHealthPercent, " / ", iHealthThreshold, " | health is too high")
	ENDIF
	
ENDPROC
				
PROC PROCESS_FMMC_DRONE_SHOULD_MOVE_ON_TO_SPECIFIC_GOTO_PROGRESS_INDEX_SERVER(FMMC_DRONE_STATE &sDroneState)
	
	IF FMMC_IS_LONG_BIT_SET(sLocalSharedAssGotoTaskData.iProgressOverrideEventComplete, sDroneState.iIndex)
		IF NOT DOES_ANY_TEAM_HAVE_NEW_PRIORITY_THIS_FRAME()
			EXIT
		ELSE
			FMMC_CLEAR_LONG_BIT(sLocalSharedAssGotoTaskData.iProgressOverrideEventComplete, sDroneState.iIndex)
		ENDIF
	ENDIF
	
	IF NOT bIsLocalPlayerHost
		EXIT
	ENDIF
	
	IF NOT FMMC_IS_LONG_BIT_SET(sLocalSharedAssGotoTaskData.iProgressOverrideEventFired, sDroneState.iIndex)
		EXIT
	ENDIF
	
	IF NOT sDroneState.bExists
		EXIT
	ENDIF
	
	IF sLocalSharedAssGotoTaskData.iProgressOverridePool[sDroneState.iIndex] = ASSOCIATED_GOTO_TASK_POOL_INDEX__OFF
		EXIT
	ENDIF
	
	PRINTLN("[FMMC_DRONES ", sDroneState.iIndex, "][SERVER] - PROCESS_PED_SHOULD_MOVE_ON_TO_SPECIFIC_GOTO_PROGRESS_INDEX_SERVER - Broadcasting event to override Drones current gotos. iProgressOverrideProgress: ",
		sLocalSharedAssGotoTaskData.iProgressOverrideProgress[sDroneState.iIndex], " iProgressOverridePool: ", sLocalSharedAssGotoTaskData.iProgressOverridePool[sDroneState.iIndex],
		" Interrupt/Retask: ", FMMC_IS_LONG_BIT_SET(sLocalSharedAssGotoTaskData.iProgressOverrideInterrupt, sDroneState.iIndex))
		
	IF sLocalSharedAssGotoTaskData.iProgressOverrideProgress[sDroneState.iIndex] >= ASSOCIATED_GOTO_TASK_POOL_START__0
		MC_serverBD.iDrone_AssociatedGotoProgress[sDroneState.iIndex] = sLocalSharedAssGotoTaskData.iProgressOverrideProgress[sDroneState.iIndex]
	ENDIF
	
	MC_serverBD.iDrone_AssociatedGotoPoolIndex[sDroneState.iIndex] = sLocalSharedAssGotoTaskData.iProgressOverridePool[sDroneState.iIndex]
	
	CLEAR_BIT(MC_ServerBD.iDrone_AssociatedGotoComplete, sDroneState.iIndex)
	
	IF FMMC_IS_LONG_BIT_SET(sLocalSharedAssGotoTaskData.iProgressOverrideInterrupt, sDroneState.iIndex)
		BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_DATA_SET_DroneAssGotoProgress_ReassignPos, DEFAULT, sDroneState.iIndex, DEFAULT, DEFAULT, DEFAULT, DEFAULT)
	ENDIF
	
	FMMC_SET_LONG_BIT(sLocalSharedAssGotoTaskData.iProgressOverrideEventComplete, sDroneState.iIndex)
	BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_ClearOverrideDroneAssGotoProgress, DEFAULT, sDroneState.iIndex, DEFAULT, DEFAULT, DEFAULT, DEFAULT)
	
	SET_FMMC_DRONE_RETASK(sDroneState)
	
ENDPROC 
		
FUNC BOOL SHOULD_FMMC_DRONE_MOVE_ON_FROM_GOTO_COORDS_STATE(FMMC_DRONE_DATA &eDroneData, FMMC_DRONE_STATE &sDroneState, BOOL &bGoToIsLooped, BOOL bProgressServerData = FALSE)
	
	BOOL bShouldMoveOn = TRUE
	INT iTemp
		
	PRINTLN("[FMMC_DRONES ", sDroneState.iIndex, "][SERVER] - SHOULD_FMMC_DRONE_MOVE_ON_FROM_GOTO_COORDS_STATE - arrived at goto loc (just completed MC_serverBD.iDrone_AssociatedGotoProgress = ", MC_serverBD.iDrone_AssociatedGotoProgress[sDroneState.iIndex], ", bProgressServerData = ", bProgressServerData,")")
	
	INT iTeam = MC_PlayerBD[iLocalPart].iTeam // g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iAssociatedTeam	 (CACHE THE TEAM IN sDroneState
	INT iRule = -1	
	IF iTeam != -1
		iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]
	ENDIF
	
	INT iLoop	
	FOR iLoop = 0 TO (MAX_ASSOCIATED_GOTO_TASKS - 1) // Max number of times we might need to do this
		
		INT iStart = MC_serverBD.iDrone_AssociatedGotoProgress[sDroneState.iIndex]		
		IF bProgressServerData
			IF IS_ASSOCIATED_GOTO_LOOPED_ON_RULE(MC_serverBD.iDrone_AssociatedGotoPoolIndex[sDroneState.iIndex], MC_serverBD.iDrone_AssociatedGotoProgress[sDroneState.iIndex], g_FMMC_STRUCT.sAssociatedGotoPool[eDroneData.iAssociatedPoolStartingIndex].sAssociatedGotoTaskData.sPointData, iRule)
			AND NOT IS_ASSOCIATED_GOTO_LOOPED_ON_RULE(MC_serverBD.iDrone_AssociatedGotoPoolIndex[sDroneState.iIndex], MC_serverBD.iDrone_AssociatedGotoProgress[sDroneState.iIndex]+1, g_FMMC_STRUCT.sAssociatedGotoPool[eDroneData.iAssociatedPoolStartingIndex].sAssociatedGotoTaskData.sPointData, iRule)							
				MC_serverBD.iDrone_AssociatedGotoProgress[sDroneState.iIndex] = GET_FIRST_ASSOCIATED_GOTO_LOOP_INDEX_FOR_RULE(MC_serverBD.iDrone_AssociatedGotoPoolIndex[sDroneState.iIndex], g_FMMC_STRUCT.sAssociatedGotoPool[eDroneData.iAssociatedPoolStartingIndex].sAssociatedGotoTaskData.sPointData, iRule)
				PRINTLN("[FMMC_DRONES ", sDroneState.iIndex, "][SERVER] - SHOULD_FMMC_DRONE_MOVE_ON_FROM_GOTO_COORDS_STATE - Reached end of looped rule goto. Starting back on: ", MC_serverBD.iDrone_AssociatedGotoProgress[sDroneState.iIndex])	
			ELSE
				WHILE SHOULD_INCREMENT_ASSOCIATED_GOTO(MC_serverBD.iDrone_AssociatedGotoPoolIndex[sDroneState.iIndex], MC_serverBD.iDrone_AssociatedGotoProgress[sDroneState.iIndex], g_FMMC_STRUCT.sAssociatedGotoPool[eDroneData.iAssociatedPoolStartingIndex].sAssociatedGotoTaskData.sPointData, iRule, iStart, IS_ASSOCIATED_GOTO_TASK_DATA__ENTIRE_BIT_SET(MC_serverBD.iDrone_AssociatedGotoPoolIndex[sDroneState.iIndex], g_FMMC_STRUCT.sAssociatedGotoPool[eDroneData.iAssociatedPoolStartingIndex].sAssociatedGotoTaskData, ciENTIRE_TASK_BITSET_SKIP_RULE_GOTOS_WHEN_RULE_INVALID)) AND MC_serverBD.iDrone_AssociatedGotoProgress[sDroneState.iIndex] < MAX_ASSOCIATED_GOTO_TASKS
					MC_serverBD.iDrone_AssociatedGotoProgress[sDroneState.iIndex] += 1
					PRINTLN("[FMMC_DRONES ", sDroneState.iIndex, "][SERVER] - SHOULD_FMMC_DRONE_MOVE_ON_FROM_GOTO_COORDS_STATE - Incrementing to: ", MC_serverBD.iDrone_AssociatedGotoProgress[sDroneState.iIndex])	
				ENDWHILE
			ENDIF
			
			iTemp = MC_serverBD.iDrone_AssociatedGotoProgress[sDroneState.iIndex]
		ENDIF
		
		IF iTemp < GET_ASSOCIATED_GOTO_TASK_DATA__NUMBER_OF_GOTOS(MC_serverBD.iDrone_AssociatedGotoPoolIndex[sDroneState.iIndex], g_FMMC_STRUCT.sAssociatedGotoPool[eDroneData.iAssociatedPoolStartingIndex].sAssociatedGotoTaskData, FALSE)
			
			IF NOT IS_VECTOR_ZERO(GET_ASSOCIATED_GOTO_TASK_DATA__POSITION(MC_serverBD.iDrone_AssociatedGotoPoolIndex[sDroneState.iIndex], g_FMMC_STRUCT.sAssociatedGotoPool[eDroneData.iAssociatedPoolStartingIndex].sAssociatedGotoTaskData, iTemp))
				PRINTLN("[FMMC_DRONES ", sDroneState.iIndex, "][SERVER] - SHOULD_FMMC_DRONE_MOVE_ON_FROM_GOTO_COORDS_STATE - next goto found (MC_serverBD.iDrone_AssociatedGotoPoolIndex[sDroneState.iIndex] = ",iTemp,") is non-zero & valid")
				bShouldMoveOn = FALSE
				iLoop = MAX_ASSOCIATED_GOTO_TASKS // Break out, this vector is non-zero (use it)
			#IF IS_DEBUG_BUILD
			ELSE
				PRINTLN("[FMMC_DRONES ", sDroneState.iIndex, "][SERVER] - SHOULD_FMMC_DRONE_MOVE_ON_FROM_GOTO_COORDS_STATE - next goto found (MC_serverBD.iDrone_AssociatedGotoPoolIndex[sDroneState.iIndex] = ",iTemp,") had a zero vector!")
				CASSERTLN(DEBUG_CONTROLLER,"[FMMC_DRONES ", sDroneState.iIndex, "][SERVER] - SHOULD_FMMC_DRONE_MOVE_ON_FROM_GOTO_COORDS_STATE - next goto found (MC_serverBD.iDrone_AssociatedGotoPoolIndex[sDroneState.iIndex] = ",iTemp,") had a zero vector!")
			#ENDIF
			ENDIF
			
		ELSE
			PRINTLN("[FMMC_DRONES ", sDroneState.iIndex, "][SERVER] - SHOULD_FMMC_DRONE_MOVE_ON_FROM_GOTO_COORDS_STATE - has reached placed number of gotos - MC_serverBD.iDrone_AssociatedGotoProgress[sDroneState.iIndex] = ",iTemp,", number of placed gotos = ", GET_ASSOCIATED_GOTO_TASK_DATA__NUMBER_OF_GOTOS(MC_serverBD.iDrone_AssociatedGotoPoolIndex[sDroneState.iIndex], g_FMMC_STRUCT.sAssociatedGotoPool[eDroneData.iAssociatedPoolStartingIndex].sAssociatedGotoTaskData, FALSE))
			iLoop = MAX_ASSOCIATED_GOTO_TASKS // Break out, we're out of vectors
		ENDIF
		
	ENDFOR
	
	IF IS_ASSOCIATED_GOTO_TASK_DATA__ENTIRE_BIT_SET(MC_serverBD.iDrone_AssociatedGotoPoolIndex[sDroneState.iIndex], g_FMMC_STRUCT.sAssociatedGotoPool[eDroneData.iAssociatedPoolStartingIndex].sAssociatedGotoTaskData, ciENTIRE_TASK_BITSET_LOOP_GOTO)
	AND bShouldMoveOn
		bGoToIsLooped = TRUE
		bShouldMoveOn = FALSE
	ENDIF
	
	RETURN bShouldMoveOn
			
ENDFUNC				

FUNC BOOL SHOULD_FMMC_DRONE_GO_TO_ATTACK_PLAYERS_STATE(FMMC_DRONE_DATA &eDroneData, FMMC_DRONE_STATE &sDroneState, INT &iPriority)
	
	// Health Action
	IF sDroneState.bHealthThreshActivated
		IF eDroneData.iHealthThresholdAction = ciTASK_DRONE_ATTACK_PLAYERS
			iPriority = ciTASK_PRIORITY_HEALTH_THRESHOLD
			RETURN TRUE
		ENDIF
	ENDIF
	
	// Associated Action
	IF sDroneState.bActionActivated
		IF eDroneData.iAssociatedRuleAction = ciTASK_DRONE_ATTACK_PLAYERS
			iPriority = ciTASK_PRIORITY_ASSOCIATED_RULE
			RETURN TRUE
		ENDIF
	ENDIF
	
	// Starting Actions
	IF eDroneData.iStartingAction = ciTASK_DRONE_ATTACK_PLAYERS
		iPriority = ciTASK_PRIORITY_STARTING_TASK
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL SHOULD_FMMC_DRONE_GO_TO_GOTO_COORDS_STATE(FMMC_DRONE_DATA &eDroneData, FMMC_DRONE_STATE &sDroneState, INT &iPriority)
	
	// Health Action
	IF sDroneState.bHealthThreshActivated
		IF eDroneData.iHealthThresholdAction = ciTASK_DRONE_GOTO_COORDS
			iPriority = ciTASK_PRIORITY_HEALTH_THRESHOLD
			RETURN TRUE
		ENDIF
	ENDIF
	
	// Associated Action
	IF sDroneState.bActionActivated
		IF eDroneData.iAssociatedRuleAction = ciTASK_DRONE_GOTO_COORDS		
		AND NOT IS_BIT_SET(MC_ServerBD.iDrone_AssociatedGotoComplete, sDroneState.iIndex)
			iPriority = ciTASK_PRIORITY_ASSOCIATED_RULE
			RETURN TRUE
		ENDIF
	ENDIF
	
	// Other Triggered Actions
	IF MC_ServerBD.iDrone_AssociatedGotoPoolIndex[sDroneState.iIndex] > ASSOCIATED_GOTO_TASK_POOL_INDEX__USE_PED_DATA
	AND NOT IS_BIT_SET(MC_ServerBD.iDrone_AssociatedGotoComplete, sDroneState.iIndex)
		iPriority = ciTASK_PRIORITY_RULE_TRIGGERED
		RETURN TRUE	
	ENDIF
	
	// Starting Actions
	IF eDroneData.iStartingAction = ciTASK_DRONE_GOTO_COORDS
		IF eDroneData.iAssociatedPoolStartingIndex != -1
		AND NOT IS_BIT_SET(MC_ServerBD.iDrone_AssociatedGotoComplete, sDroneState.iIndex)
			iPriority = ciTASK_PRIORITY_STARTING_TASK
			RETURN TRUE
		ENDIF
	ENDIF
			
	RETURN FALSE
	
ENDFUNC

FUNC BOOL SHOULD_FMMC_DRONE_GO_TO_SELF_DESTRUCT_CHARGE_PLAYERS_STATE(FMMC_DRONE_DATA &eDroneData, FMMC_DRONE_STATE &sDroneState, INT &iPriority)
	
	// Health Action
	IF sDroneState.bHealthThreshActivated
		IF eDroneData.iHealthThresholdAction = ciTASK_DRONE_SELF_DESTRUCT_CHARGE_PLAYERS
			iPriority = ciTASK_PRIORITY_HEALTH_THRESHOLD
			RETURN TRUE
		ENDIF
	ENDIF
	
	// Associated Action
	IF sDroneState.bActionActivated
		IF eDroneData.iAssociatedRuleAction = ciTASK_DRONE_SELF_DESTRUCT_CHARGE_PLAYERS
			iPriority = ciTASK_PRIORITY_ASSOCIATED_RULE
			RETURN TRUE
		ENDIF
	ENDIF
	
	// Other Triggered Actions
	
	// Starting Actions
	IF eDroneData.iStartingAction = ciTASK_DRONE_SELF_DESTRUCT_CHARGE_PLAYERS
		iPriority = ciTASK_PRIORITY_STARTING_TASK
		RETURN TRUE
	ENDIF
			
	RETURN FALSE
	
ENDFUNC

// ########## CHOOSE TASK
PROC PROCESS_FMMC_DRONE_CHOOSE_NEW_TASK_SEVER(FMMC_DRONE_DATA &eDroneData, FMMC_DRONE_STATE &sDroneState)
		
	IF IS_FMMC_DRONE_IN_A_DEAD_STATE(sDroneState)
		PRINTLN("[FMMC_DRONES ", sDroneState.iIndex, "][SERVER] - PROCESS_FMMC_DRONE_CHOOSE_NEW_TASK_SEVER - Cannot receive a task as the Drone is broken or not alive.")
		EXIT
	ENDIF
	
	INT iTask = ciTASK_DRONE_CHOOSE_NEW_TASK
	INT iPriority = -1
	INT iPriorityTemp = -1
	
	// Self Destruct Checks
	IF SHOULD_FMMC_DRONE_GO_TO_SELF_DESTRUCT_CHARGE_PLAYERS_STATE(eDroneData, sDroneState, iPriorityTemp)
		IF iPriorityTemp > iPriority
			iPriority = iPriorityTemp
			iTask = ciTASK_DRONE_SELF_DESTRUCT_CHARGE_PLAYERS
			PRINTLN("[FMMC_DRONES ", sDroneState.iIndex, "][SERVER] - PROCESS_FMMC_DRONE_CHOOSE_NEW_TASK_SEVER - State: ", GET_FMMC_DRONE_STATE_NAME(iTask), " is valid, iPriority: ", iPriority)
		ENDIF		
	ENDIF
	
	// Combat Checks
	IF SHOULD_FMMC_DRONE_GO_TO_ATTACK_PLAYERS_STATE(eDroneData, sDroneState, iPriorityTemp)
		IF iPriorityTemp > iPriority
			iPriority = iPriorityTemp
			iTask = ciTASK_DRONE_ATTACK_PLAYERS
			PRINTLN("[FMMC_DRONES ", sDroneState.iIndex, "][SERVER] - PROCESS_FMMC_DRONE_CHOOSE_NEW_TASK_SEVER - State: ", GET_FMMC_DRONE_STATE_NAME(iTask), " is valid, iPriority: ", iPriority)
		ENDIF	
	ENDIF
	
	// Goto Checks
	IF SHOULD_FMMC_DRONE_GO_TO_GOTO_COORDS_STATE(eDroneData, sDroneState, iPriorityTemp)
		IF iPriorityTemp > iPriority
			iPriority = iPriorityTemp
			iTask = ciTASK_DRONE_GOTO_COORDS
			PRINTLN("[FMMC_DRONES ", sDroneState.iIndex, "][SERVER] - PROCESS_FMMC_DRONE_CHOOSE_NEW_TASK_SEVER - State: ", GET_FMMC_DRONE_STATE_NAME(iTask), " is valid, iPriority: ", iPriority)
		ENDIF	
	ENDIF
	
	IF iTask !=	ciTASK_DRONE_CHOOSE_NEW_TASK
		PRINTLN("[FMMC_DRONES ", sDroneState.iIndex, "][SERVER] - PROCESS_FMMC_DRONE_CHOOSE_NEW_TASK_SEVER - Server has decided to choose a new task...")
		SET_FMMC_DRONE_STATE(sDroneState, iTask)
	ENDIF
	
ENDPROC


// ########## GOTO TASK
FUNC BOOL HAS_FMMC_DRONE_ASSOCIATED_GOTO_ACTIONS_COMPLETED(FMMC_DRONE_STATE &sDroneState)
	
	IF NOT FMMC_IS_LONG_BIT_SET(sLocalSharedAssGotoTaskData.iStartedGotoBitset, sDroneState.iIndex)
		PRINTLN("[FMMC_DRONES ", sDroneState.iIndex, "][SERVER][Progress ", sLocalSharedAssGotoTaskData.iProgress[sDroneState.iIndex], "] - HAS_FMMC_DRONE_ASSOCIATED_GOTO_ACTIONS_COMPLETED - Waiting for Player to start the Goto.")
		RETURN FALSE	
	ENDIF
	
	IF NOT FMMC_IS_LONG_BIT_SET(sLocalSharedAssGotoTaskData.iCompletedArrivedBitset, sDroneState.iIndex)
		PRINTLN("[FMMC_DRONES ", sDroneState.iIndex, "][SERVER][Progress ", sLocalSharedAssGotoTaskData.iProgress[sDroneState.iIndex], "] - HAS_FMMC_DRONE_ASSOCIATED_GOTO_ACTIONS_COMPLETED - Waiting for Drone to arrive at destination.")
		RETURN FALSE	
	ENDIF
	
	PRINTLN("[FMMC_DRONES ", sDroneState.iIndex, "][SERVER][Progress ", sLocalSharedAssGotoTaskData.iProgress[sDroneState.iIndex], "] - HAS_FMMC_DRONE_ASSOCIATED_GOTO_ACTIONS_COMPLETED - Actions Complete.")
	
	RETURN TRUE
	
ENDFUNC

PROC PROCESS_FMMC_DRONE_ASSOCIATED_GOTO_DATA_SERVER(FMMC_DRONE_DATA &eDroneData, FMMC_DRONE_STATE &sDroneState)
		
	IF NOT HAS_FMMC_DRONE_ASSOCIATED_GOTO_ACTIONS_COMPLETED(sDroneState)
		EXIT
	ENDIF	
	
	IF sLocalSharedAssGotoTaskData.iProgress[sDroneState.iIndex] != MC_serverBD.iDrone_AssociatedGotoProgress[sDroneState.iIndex]
		#IF IS_DEBUG_BUILD IF bFMMcDroneDebugServer PRINT_ON_SCREEN_DEBUG_TEXT_WITH_VECTOR("Waiting for Progress to sync: ", sDroneState.vPos) ENDIF #ENDIF
		PRINTLN("[FMMC_DRONES ", sDroneState.iIndex, "][SERVER] - PROCESS_FMMC_DRONE_ASSOCIATED_GOTO_DATA_SERVER - Waiting for Progress to sync.")		
		EXIT
	ENDIF
	
	BOOL bGoToIsLooped	
	IF SHOULD_FMMC_DRONE_MOVE_ON_FROM_GOTO_COORDS_STATE(eDroneData, sDroneState, bGoToIsLooped, TRUE)
		PRINTLN("[FMMC_DRONES ", sDroneState.iIndex, "][SERVER] - PROCESS_FMMC_DRONE_ASSOCIATED_GOTO_DATA_SERVER - finished all gotos and not looping. We should exit out from Path state.")
		SET_BIT(MC_ServerBD.iDrone_AssociatedGotoComplete, sDroneState.iIndex)
		SET_FMMC_DRONE_STATE(sDroneState, ciTASK_DRONE_CHOOSE_NEW_TASK)
	ELSE
		IF bGoToIsLooped
			INT iIndex = 0
			IF GET_ASSOCIATED_GOTO_TASK_DATA__ENTIRE_LOOP_GOTO_START_INDEX(MC_serverBD.iDrone_AssociatedGotoPoolIndex[sDroneState.iIndex], g_FMMC_STRUCT.sAssociatedGotoPool[eDroneData.iAssociatedPoolStartingIndex].sAssociatedGotoTaskData) > -1
				iIndex = GET_ASSOCIATED_GOTO_TASK_DATA__ENTIRE_LOOP_GOTO_START_INDEX(MC_serverBD.iDrone_AssociatedGotoPoolIndex[sDroneState.iIndex], g_FMMC_STRUCT.sAssociatedGotoPool[eDroneData.iAssociatedPoolStartingIndex].sAssociatedGotoTaskData)
			ENDIF
			MC_serverBD.iDrone_AssociatedGotoProgress[sDroneState.iIndex] = iIndex
			PRINTLN("[FMMC_DRONES ", sDroneState.iIndex, "][SERVER] - PROCESS_FMMC_DRONE_ASSOCIATED_GOTO_DATA_SERVER - finished all gotos but set to loop (setting iDrone_AssociatedGotoProgress back to: ", iIndex, ")")
		ENDIF
	ENDIF
		
ENDPROC

// ########## ATTACK PLAYERS TASK
PROC PROCESS_FMMC_DRONE_ASSOCIATED_ATTACK_PLAYERS_SERVER(FMMC_DRONE_DATA &eDroneData, FMMC_DRONE_STATE &sDroneState)

	UNUSED_PARAMETER(eDroneData)
	UNUSED_PARAMETER(sDroneState)
	
ENDPROC

// ########## SELF DESTRUCT / CHARGE PLAYERS TASK
PROC PROCESS_FMMC_DRONE_SELF_DESTRUCT_CHARGE_PLAYERS_SERVER(FMMC_DRONE_DATA &eDroneData, FMMC_DRONE_STATE &sDroneState)

	UNUSED_PARAMETER(eDroneData)
	UNUSED_PARAMETER(sDroneState)
	
ENDPROC

// ########## DEAD TASK
PROC PROCESS_FMMC_DRONE_DEAD_SERVER(FMMC_DRONE_DATA &eDroneData, FMMC_DRONE_STATE &sDroneState)

	UNUSED_PARAMETER(eDroneData)
	UNUSED_PARAMETER(sDroneState)
	
ENDPROC

PROC PROCESS_FMMC_DRONE_RETASK(FMMC_DRONE_STATE &sDroneState)
	IF IS_BIT_SET(MC_ServerBD.iDrone_Retask, sDroneState.iIndex)		
		CLEAR_BIT(MC_ServerBD.iDrone_Retask, sDroneState.iIndex)
		PRINTLN("[FMMC_DRONES ", sDroneState.iIndex, "][SERVER] - PROCESS_FMMC_DRONE_RETASK - Retasking.")
		SET_FMMC_DRONE_STATE(sDroneState, ciTASK_DRONE_CHOOSE_NEW_TASK)
	ENDIF
ENDPROC

PROC PROCESS_FMMC_DRONE_SYSTEMS_SERVER(FMMC_DRONE_DATA &eDroneData, LOCAL_FMMC_DRONE_DATA &sDroneDataLocal, FMMC_DRONE_STATE &sDroneState)
		
	PROCESS_FMMC_DRONE_RETASK(sDroneState)
	
	PROCESS_FMMC_DRONE_SHOULD_MOVE_ON_TO_SPECIFIC_GOTO_PROGRESS_INDEX_SERVER(sDroneState)
	
	SERVER_FMMC_DRONE_SWAP_OBJECT_MODEL_WHEN_IT_IS_DESTROYED(sDroneState, sDroneDataLocal)
	
	SWITCH MC_ServerBD.iDrone_State[sDroneState.iIndex]
		CASE ciTASK_DRONE_CHOOSE_NEW_TASK
			PROCESS_FMMC_DRONE_CHOOSE_NEW_TASK_SEVER(eDroneData, sDroneState)
		BREAK
		CASE ciTASK_DRONE_GOTO_COORDS
			PROCESS_FMMC_DRONE_ASSOCIATED_GOTO_DATA_SERVER(eDroneData, sDroneState)
		BREAK
		CASE ciTASK_DRONE_ATTACK_PLAYERS
			PROCESS_FMMC_DRONE_ASSOCIATED_ATTACK_PLAYERS_SERVER(eDroneData, sDroneState)
		BREAK
		CASE ciTASK_DRONE_SELF_DESTRUCT_CHARGE_PLAYERS
			PROCESS_FMMC_DRONE_SELF_DESTRUCT_CHARGE_PLAYERS_SERVER(eDroneData, sDroneState)
		BREAK
	ENDSWITCH	
	
ENDPROC

// ###########################
// Client ###########################
// ###########################

// EMP launcher does not give damage events for the drone.
PROC PROCESS_CLIENT_FMMC_DRONE_DETECT_EMP_LAUNCHER_EXPLOSIONS(FMMC_DRONE_STATE &sDroneState)
	
	IF IS_FMMC_DRONE_IN_A_DEAD_STATE(sDroneState)
		EXIT
	ENDIF
	
	IF IS_EXPLOSION_IN_SPHERE(EXP_TAG_EMPLAUNCHER_EMP, sDroneState.vPos, 7.5)
		PRINTLN("[FMMC_DRONES ", sDroneState.iIndex, "][CLIENT] - PROCESS_CLIENT_FMMC_DRONE_DETECT_EMP_LAUNCHER_EXPLOSIONS - Detected EMP explosion. Killing drone.")
		SET_ENTITY_HEALTH(sDroneState.oiIndex, 1, sDroneState.oiIndex)
		ADD_EXPLOSION(sDroneState.vPos, EXP_TAG_BARREL, 0.5, FALSE, TRUE, 0.0, FALSE)
	ENDIF
	
ENDPROC

PROC SET_FMMC_DRONE_ROTATION(FMMC_DRONE_DATA &eDroneData, LOCAL_FMMC_DRONE_DATA &sDroneDataLocal, FMMC_DRONE_STATE &sDroneState, FLOAT fTarget)
	
	IF IS_BIT_SET(sDroneDataLocal.iBS, ciFMMCDRONE_BS_RotationAdjusted)
		EXIT
	ENDIF
	
	SET_BIT(sDroneDataLocal.iBS, ciFMMCDRONE_BS_RotationAdjusted)
	
	VECTOR vRotation = GET_ENTITY_ROTATION(sDroneState.oiIndex)		
	vRotation.z +=720
	vRotation.z = NORMALIZE_ANGLE_POSITIVE(vRotation.z)			
	fTarget += 720		
	fTarget = NORMALIZE_ANGLE_POSITIVE(fTarget)
		
	FLOAT fAdjustmentYaw = (fTarget-vRotation.z)*(1.5*GET_FRAME_TIME())									// todo- Adjust based on frame time if no physics way of doing it.
	fLOAT fAdjustmentRoll
		
	IF eDroneData.iAmbientlySpinSpeed = 0
		IF ABSF(fTarget-vRotation.z) > 180
		AND fAdjustmentYaw > 0
			fAdjustmentYaw = -fAdjustmentYaw
		ENDIF
		IF fAdjustmentYaw > 0		
			fAdjustmentRoll = -1.65*GET_FRAME_TIME()
		ELSE
			fAdjustmentRoll = 1.65*GET_FRAME_TIME()
		ENDIF
	ENDIF
		
	IF eDroneData.iAmbientlySpinSpeed != 0.0		
		fAdjustmentYaw = TO_FLOAT(eDroneData.iAmbientlySpinSpeed)*GET_FRAME_TIME()
	ENDIF
	
	// YAW - Look at target.
	IF eDroneData.iAmbientlySpinSpeed != 0
		vRotation.z += fAdjustmentYaw		
		vRotation.y = 0.0		
		SET_ENTITY_ROTATION(sDroneState.oiIndex, vRotation)
				
	ELIF NOT IS_HEADING_ACCEPTABLE(vRotation.z, fTarget, ciFMMCDRONE_ACCEPTABLE_HEADING)
		vRotation.z += fAdjustmentYaw		
		vRotation.y += fAdjustmentRoll
		vRotation.y = CLAMP(vRotation.y, -15.0, 15.0)
		SET_ENTITY_ROTATION(sDroneState.oiIndex, vRotation)
				
		#IF IS_DEBUG_BUILD IF bFMMcDroneDebugPosition PRINT_ON_SCREEN_DEBUG_TEXT("Adjusting Yaw/Roll") ENDIF #ENDIF
	
	// ROLL RESET - When we're not adjusting the heading.
	ELIF vRotation.y != 0
		
		#IF IS_DEBUG_BUILD IF bFMMcDroneDebugPosition PRINT_ON_SCREEN_DEBUG_TEXT("Resetting Roll") ENDIF #ENDIF
		
		IF vRotation.y <= 1.0
		AND vRotation.y >= -1.0
			vRotation.y = 0
		ELIF vRotation.y > 0
			vRotation.y += -1.0
		ELSE
			vRotation.y += 1.0	
		ENDIF
		
		SET_ENTITY_ROTATION(sDroneState.oiIndex, vRotation)
	ENDIF
	
	// PITCH - Based on acceleration/forward movement.
	IF eDroneData.iAmbientlySpinSpeed != 0		
		vRotation.x = 0.0
		SET_ENTITY_ROTATION(sDroneState.oiIndex, vRotation)
	ELIF sDroneDataLocal.fAccel > 0.4
		#IF IS_DEBUG_BUILD IF bFMMcDroneDebugPosition PRINT_ON_SCREEN_DEBUG_TEXT("Adjusting Pitch") ENDIF #ENDIF
		
		vRotation.x = (14.0*sDroneDataLocal.fAccel)
		SET_ENTITY_ROTATION(sDroneState.oiIndex, vRotation)
	ELSE
		#IF IS_DEBUG_BUILD IF bFMMcDroneDebugPosition PRINT_ON_SCREEN_DEBUG_TEXT("Resetting Pitch") ENDIF #ENDIF
		
		IF vRotation.x <= 1.0
		AND vRotation.x >= -1.0
			vRotation.x = 0
		ELIF vRotation.x > 0
			vRotation.x += -1.0
		ELSE
			vRotation.x += 1.0
		ENDIF
		SET_ENTITY_ROTATION(sDroneState.oiIndex, vRotation)
	ENDIF
ENDPROC

PROC PROCESS_FMMC_DRONE_FROZEN_ON_GROUND(LOCAL_FMMC_DRONE_DATA &sDroneDataLocal, FMMC_DRONE_STATE &sDroneState)
	
	IF IS_BIT_SET(sDroneDataLocal.iBS, ciFMMCDRONE_BS_DroneOnFloor)
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(sDroneDataLocal.iBS, ciFMMCDRONE_BS_DroneOnFloor)
		PRINTLN("[FMMC_DRONES ", sDroneState.iIndex, "][CLIENT] - PROCESS_FMMC_DRONE_FROZEN_ON_GROUND - Drone is dead or deactivated. Activating Physcs.")		
		FREEZE_ENTITY_POSITION(sDroneState.oiIndex, FALSE)
		ACTIVATE_PHYSICS(sDroneState.oiIndex)
		SET_ENTITY_DYNAMIC(sDroneState.oiIndex, TRUE)
		SET_ENTITY_HAS_GRAVITY(sDroneState.oiIndex, TRUE)		
	ENDIF
	
	IF GET_MODEL_SWAP_WHEN_IT_IS_DESROYED(sDroneState.oiIndex) != GET_ENTITY_MODEL(sDroneState.oiIndex)
	AND GET_MODEL_SWAP_WHEN_IT_IS_DESROYED(sDroneState.oiIndex) != DUMMY_MODEL_FOR_SCRIPT
		EXIT
	ENDIF
	
	IF VDIST2(sDroneState.vPos, GET_ENTITY_COORDS(localPlayerPed, FALSE)) > POW(100.0, 2.0) // To ensure an accurate result with GET_GROUND_Z_EXCLUDING_OBJECTS_FOR_3D_COORD.
		PRINTLN("[FMMC_DRONES ", sDroneState.iIndex, "][CLIENT] - PROCESS_FMMC_DRONE_FROZEN_ON_GROUND - Destroyed Drone is a bit too far away to freeze on the ground yet. Waiting.")
		EXIT
	ENDIF
	
	FLOAT fGroundZ
	VECTOR vVel = GET_ENTITY_VELOCITY(sDroneState.oiIndex)
	
	IF IS_VECTOR_ZERO(vVel)
		VECTOR vNewPos = GET_ENTITY_COORDS(sDroneState.oiIndex, FALSE)		
		GET_GROUND_Z_EXCLUDING_OBJECTS_FOR_3D_COORD(sDroneState.vPos, fGroundZ)
		vNewPos.z = fGroundZ
		
		VECTOR vMin, vMax
		GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(sDroneState.oiIndex),  vMin, vMax)
		vNewPos.z += vMax.z
		
		PLAY_FMMC_DRONE_BUMP_SFX(sDroneState.mn, vNewPos)
		SET_ENTITY_COORDS(sDroneState.oiIndex, vNewPos, FALSE)
		FREEZE_ENTITY_POSITION(sDroneState.oiIndex, TRUE)
		SET_BIT(sDroneDataLocal.iBS, ciFMMCDRONE_BS_DroneOnFloor)
	ENDIF
		
ENDPROC

PROC CLEAR_FMMC_DRONE_WAYPOINTS_TO_TARGET(LOCAL_FMMC_DRONE_DATA &sDroneDataLocal)
		
	LOCAL_FMMC_DRONE_PATHFINDING_DATA sPathBlank
		
	INT iPath = 0	
	FOR iPath = 0 TO ciFMMCDRONE_PATHS_MAX-1
		sDroneDataLocal.sPathfinding[iPath] = sPathBlank		
	ENDFOR
	
	sDroneDataLocal.sPath = sPathBlank
	
ENDPROC

PROC PROCESS_FMMC_DRONE_CURRENT_WAYPOINT(FMMC_DRONE_STATE &sDroneState, LOCAL_FMMC_DRONE_DATA &sDroneDataLocal, VECTOR &vAdjustedTarget)
	
	IF sDroneDataLocal.iCurrentWaypoint <= -1
	OR sDroneDataLocal.iCurrentWaypoint >= ciFMMCDRONE_WAYPOINTS_MAX
		EXIT
	ENDIF
		
	VECTOR vCurrentWaypoint = sDroneDataLocal.sPath.vDroneWaypoints[sDroneDataLocal.iCurrentWaypoint]
	
	FLOAT fDistance = VDIST2(vCurrentWaypoint, sDroneState.vPos)
	FLOAT fArrivalRadius = POW(0.15, 2.0)
	fArrivalRadius = CLAMP(fArrivalRadius, 0.5, 999)
		
	IF fDistance <= fArrivalRadius			
		sDroneDataLocal.iCurrentWaypoint++
		PROCESS_FMMC_DRONE_CURRENT_WAYPOINT(sDroneState, sDroneDataLocal, vAdjustedTarget)
		EXIT
	ENDIF
		
	vAdjustedTarget = vCurrentWaypoint
	
ENDPROC

FUNC VECTOR POPULATE_FMMC_DRONE_WAYPOINT_POSITION(LOCAL_FMMC_DRONE_PATHFINDING_DATA &sPath, FMMC_DRONE_STATE &sDroneState, VECTOR vPosFrom, VECTOR vPosTo, FLOAT &fAdjustmentAngle, FLOAT &fPrevAdjustmentAngle, INT iType)
		
	VECTOR vTemp, vPosHit
	ENTITY_INDEX eiHit
	SHAPETEST_STATUS stStatus
	INT iHitSomething
	
	IF sPath.stiDroneWaypointChecks = NULL		
		VECTOR vWaypointTargetPosition				
		VECTOR vDirection = NORMALISE_VECTOR(vPosTo-vPosFrom)		
		
		IF fAdjustmentAngle != 0.0
			IF fPrevAdjustmentAngle != 0.0				
				fAdjustmentAngle = fPrevAdjustmentAngle
				fPrevAdjustmentAngle = 0.0
			ENDIF
			vDirection = ROTATE_VECTOR_ABOUT_Z(vDirection, fAdjustmentAngle)
		ENDIF
		
		FLOAT fWaypointDist
		FLOAT fDistFromTarget = VDIST2(vPosFrom, vPosTo)
		IF fDistFromTarget > POW(8.0, 2.0)
			fWaypointDist = GET_RANDOM_FLOAT_IN_RANGE(6.0, 10.0)
		ELIF fDistFromTarget > POW(4.0, 2.0)
			fWaypointDist = 2.5
		ELIF fDistFromTarget > POW(2.0, 2.0)
			fWaypointDist = 1.5
		ELSE
			fWaypointDist = fDistFromTarget
			sPath.bCompletedPath = TRUE
		ENDIF
		
		vWaypointTargetPosition = vPosFrom + (vDirection*fWaypointDist)
		sPath.vLastShapetestWaypointCollision = vWaypointTargetPosition
		
		PRINTLN("[DronePath] - vPosFrom: ", vPosFrom, " vWaypointTargetPosition: ", vWaypointTargetPosition, " Starting shapetest. vPosTo: ", vPosTo)
		
		INT iLOSFlags = SCRIPT_INCLUDE_MOVER | SCRIPT_INCLUDE_VEHICLE | SCRIPT_INCLUDE_PED | SCRIPT_INCLUDE_RAGDOLL | SCRIPT_INCLUDE_OBJECT | SCRIPT_INCLUDE_PICKUP | SCRIPT_INCLUDE_GLASS
		sPath.stiDroneWaypointChecks = START_SHAPE_TEST_CAPSULE(vPosFrom, vWaypointTargetPosition, 0.3, iLOSFlags, sDroneState.oiIndex)				
		
		#IF IS_DEBUG_BUILD IF bFMMcDroneDebugPosition DRAW_DEBUG_LINE(vPosFrom, vWaypointTargetPosition, 255, 0, 0) ENDIF #ENDIF		
	ELSE
		stStatus = GET_SHAPE_TEST_RESULT(sPath.stiDroneWaypointChecks, iHitSomething, vPosHit, vTemp, eiHit)
		IF stStatus = SHAPETEST_STATUS_RESULTS_READY
			IF IS_VECTOR_ZERO(vPosHit)
				PRINTLN("[DronePath] - vPosHit: ", vPosHit, " Position ok.")				
				sPath.stiDroneWaypointChecks = NULL
				RETURN sPath.vLastShapetestWaypointCollision
			ELSE
				PRINTLN("[DronePath] - vPosHit: ", vPosHit, " Position NOT ok.")
				sPath.stiDroneWaypointChecks = NULL
				SWITCH iType
					CASE ciFMMCDRONE_PATHS_RIGHT
						fAdjustmentAngle += 12.5
					BREAK
					CASE ciFMMCDRONE_PATHS_LEFT
						fAdjustmentAngle -= 12.5
					BREAK
				ENDSWITCH
				PRINTLN("[DronePath] - ADJUSTING ANGLE fAdjustmentAngle")
			ENDIF
		ELIF stStatus = SHAPETEST_STATUS_NONEXISTENT
			sPath.stiDroneWaypointChecks = NULL
		ENDIF
	ENDIF
	
	RETURN ZERO_VECTOR()
	
ENDFUNC

PROC ASSIGN_CLOSEST_WAYPOINT_AND_PATH_TO_CURRENT_POSITION(LOCAL_FMMC_DRONE_DATA &sDroneDataLocal, FMMC_DRONE_STATE &sDroneState, INT &iPathToAssign, INT &iWaypointToAssign)
		
	INT iWaypoints[ciFMMCDRONE_WAYPOINTS_MAX]
	FLOAT fDist[ciFMMCDRONE_WAYPOINTS_MAX]
	
	INT iPaths[ciFMMCDRONE_PATHS_MAX]
	INT iWaypointsBestForPath[ciFMMCDRONE_PATHS_MAX]
	FLOAT fPathTotalDist[ciFMMCDRONE_PATHS_MAX]	
	FLOAT fPathTotalDist_Temp[ciFMMCDRONE_PATHS_MAX]
	BOOL bCompletedPath[ciFMMCDRONE_PATHS_MAX]
		
	INT iPath
	INT iWaypoint = 0
	
	FOR iPath = 0 TO ciFMMCDRONE_PATHS_MAX-1
		iPaths[iPath] = iPath
		bCompletedPath[iPath] = sDroneDataLocal.sPathfinding[iPath].bCompletedPath
		FOR iWaypoint = 0 TO ciFMMCDRONE_WAYPOINTS_MAX-1
			IF NOT IS_VECTOR_ZERO(sDroneDataLocal.sPathfinding[iPath].vDroneWaypoints[iWaypoint])		
				fDist[iWaypoint] = VDIST2(sDroneDataLocal.sPathfinding[iPath].vDroneWaypoints[iWaypoint], sDroneState.vPos)
				iWaypoints[iWaypoint] = iWaypoint
				fPathTotalDist[iPath] += fDist[iWaypoint]				
			ENDIF
		ENDFOR
		QUICK_SORT_FLOAT_WITH_INDEXES(fDist, iWaypoints, 0, ciFMMCDRONE_WAYPOINTS_MAX-1)
		iWaypointsBestForPath[iPath] = iWaypoints[0]
		fPathTotalDist_Temp[iPath] = fPathTotalDist[iPath]
	ENDFOR
	
	// If some paths are incomplete versus others that are complete then disqualify them.
	INT iPathCompare_A, iPathCompare_B
	FOR iPathCompare_A = 0 TO ciFMMCDRONE_PATHS_MAX-1
		FOR iPathCompare_B = 0 TO ciFMMCDRONE_PATHS_MAX-1
			IF NOT sDroneDataLocal.sPathfinding[iPathCompare_A].bCompletedPath
			AND bCompletedPath[iPathCompare_B]
				fPathTotalDist_Temp[iPathCompare_A] = 20999999
				fPathTotalDist[iPathCompare_A] = 20999999
			ENDIF
		ENDFOR
	ENDFOR
	
	// Print Sorted BEFORE:
	#IF IS_DEBUG_BUILD 
	IF bFMMcDroneDebugPathfinding
	PRINTLN("---Path Info---")
	PRINTLN("Before Sorting:")
	FOR iPath = 0 TO ciFMMCDRONE_PATHS_MAX-1		
		PRINTLN("fPathTotalDist[", iPath, "]: ", fPathTotalDist[iPath])
		PRINTLN("iPaths[", iPath, "]: ", iPaths[iPath])		
	ENDFOR
	ENDIF
	#ENDIF
	
	QUICK_SORT_FLOAT_WITH_INDEXES(fPathTotalDist, iPaths, 0, ciFMMCDRONE_PATHS_MAX-1)
	QUICK_SORT_FLOAT_WITH_INDEXES(fPathTotalDist_Temp, iWaypointsBestForPath, 0, ciFMMCDRONE_PATHS_MAX-1)
	
	PRINTLN("")
	PRINTLN("After Sorting:")
	// Print Sorted AFTER:
	#IF IS_DEBUG_BUILD 
	IF bFMMcDroneDebugPathfinding
	FOR iPath = 0 TO ciFMMCDRONE_PATHS_MAX-1		
		PRINTLN("fPathTotalDist[", iPath, "]: ", fPathTotalDist[iPath])
		PRINTLN("iPaths[", iPath, "]: ", iPaths[iPath])		
		PRINTLN("iWaypointsBestForPath[", iPath, "]: ", iWaypointsBestForPath[iPath])		
	ENDFOR
	ENDIF
	#ENDIF
	
	iPathToAssign = iPaths[0]
	iWaypointToAssign = iWaypointsBestForPath[0]
	sDroneDataLocal.sPath = sDroneDataLocal.sPathfinding[iPathToAssign]
	
ENDPROC

PROC RESET_FMMC_DRONE_PATH_AND_WAYPOINTS(LOCAL_FMMC_DRONE_DATA &sDroneDataLocal)
	MC_STOP_NET_TIMER_WITH_TIMESTAMP(sDroneDataLocal.iTimeStampValidWaypoints)
	CLEAR_FMMC_DRONE_WAYPOINTS_TO_TARGET(sDroneDataLocal)	
	sDroneDataLocal.iCurrentPath = -1
	sDroneDataLocal.iCurrentWaypoint = -1
ENDPROC

PROC PROCESS_FMMC_DRONE_WAYPOINT_GENERATION_TO_TARGET(LOCAL_FMMC_DRONE_DATA &sDroneDataLocal, FMMC_DRONE_STATE &sDroneState, VECTOR vTarget)
	
	IF MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(sDroneDataLocal.iTimeStampValidWaypoints)
	AND NOT MC_HAS_NET_TIMER_EXPIRED_WITH_TIME_STAMP(sDroneDataLocal.iTimeStampValidWaypoints, ciFMMCDRONE_PATHFINDING_DATA_OUT_OF_DATE_TIME)
		EXIT
	ENDIF	
	
	IF MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(sDroneDataLocal.iTimeStampValidWaypoints)	
	AND MC_HAS_NET_TIMER_EXPIRED_WITH_TIME_STAMP(sDroneDataLocal.iTimeStampValidWaypoints, ciFMMCDRONE_PATHFINDING_DATA_OUT_OF_DATE_TIME)
		RESET_FMMC_DRONE_PATH_AND_WAYPOINTS(sDroneDataLocal)
	ENDIF
	
	BOOL bGenerating
	INT iPath = 0
	INT iWayPoint = 0
	VECTOR vPrevPoint
	FLOAT fPrevAngle
	FOR iPath = 0 TO ciFMMCDRONE_PATHS_MAX-1	
		FOR iWayPoint = 0 TO ciFMMCDRONE_WAYPOINTS_MAX-1
			IF NOT IS_VECTOR_ZERO(sDroneDataLocal.sPathfinding[iPath].vDroneWaypoints[iWayPoint])
				RELOOP
			ENDIF
			bGenerating = TRUE
			IF iWayPoint = 0
				vPrevPoint = sDroneState.vPos
				fPrevAngle = 0.0
			ELSE
				vPrevPoint = sDroneDataLocal.sPathfinding[iPath].vDroneWaypoints[iWayPoint-1]
				fPrevAngle = sDroneDataLocal.sPathfinding[iPath].fAdjustmentAngle[iWayPoint-1]
			ENDIF
			PRINTLN("[DronePath] - Trying to grab Waypoint i: ", iWayPoint, " ------------------ From: ", vPrevPoint, " vTarget: ", vTarget)
			VECTOR vWaypoint = POPULATE_FMMC_DRONE_WAYPOINT_POSITION(sDroneDataLocal.sPathfinding[iPath], sDroneState, vPrevPoint, vTarget, sDroneDataLocal.sPathfinding[iPath].fAdjustmentAngle[iWayPoint], fPrevAngle, iPath)
			IF IS_VECTOR_ZERO(vWaypoint)			
				PRINTLN("[DronePath] - ")
				BREAKLOOP
			ENDIF
			sDroneDataLocal.sPathfinding[iPath].vDroneWaypoints[iWayPoint] = vWaypoint		
			PRINTLN("[DronePath] - Assigning vWaypoint: ", vWaypoint)
			PRINTLN("[DronePath] - ")
			RELOOP
		ENDFOR
	ENDFOR
	
	IF bGenerating						
		#IF IS_DEBUG_BUILD
		IF bFMMcDroneDebugPathfinding
			FOR iPath = 0 TO ciFMMCDRONE_PATHS_MAX-1
				FOR iWayPoint = 0 TO ciFMMCDRONE_WAYPOINTS_MAX-1		
					IF NOT IS_VECTOR_ZERO(sDroneDataLocal.sPathfinding[iPath].vDroneWaypoints[iWayPoint])
						#IF IS_DEBUG_BUILD IF bFMMcDroneDebugPathfinding DRAW_MARKER(MARKER_SPHERE, sDroneDataLocal.sPathfinding[iPath].vDroneWaypoints[iWayPoint], (<<0.0, 0.0, 0.0>>), (<<0.0, 0.0, 0.0>>), (<<0.25, 0.25, 0.25>>), 0, 175, 175, 100) ENDIF #ENDIF
						IF iWayPoint < ciFMMCDRONE_WAYPOINTS_MAX-1
							IF NOT IS_VECTOR_ZERO(sDroneDataLocal.sPathfinding[iPath].vDroneWaypoints[iWayPoint+1])
								#IF IS_DEBUG_BUILD IF bFMMcDroneDebugPathfinding DRAW_DEBUG_LINE(sDroneDataLocal.sPathfinding[iPath].vDroneWaypoints[iWayPoint], sDroneDataLocal.sPathfinding[iPath].vDroneWaypoints[iWayPoint+1], 255, 0, 0) ENDIF #ENDIF		
							ENDIF
						ENDIF
					ENDIF
				ENDFOR
			ENDFOR		
		ENDIF
		#ENDIF
		
		EXIT
	ENDIF
	
	ASSIGN_CLOSEST_WAYPOINT_AND_PATH_TO_CURRENT_POSITION(sDroneDataLocal, sDroneState, sDroneDataLocal.iCurrentPath, sDroneDataLocal.iCurrentWaypoint)	
	MC_REINIT_NET_TIMER_WITH_TIMESTAMP(sDroneDataLocal.iTimeStampValidWaypoints)
	
ENDPROC

FUNC VECTOR GET_FMMC_DRONE_ADJUSTED_TARGET_POSITION(FMMC_DRONE_DATA &eDroneData, LOCAL_FMMC_DRONE_DATA &sDroneDataLocal, FLOAT &fGround)
	
	VECTOR vTarget = sDroneDataLocal.vTarget
		
	VECTOR vAdjusted = vTarget
	vTarget.z += 1.0
	IF GET_GROUND_Z_FOR_3D_COORD(vTarget, fGround)
		IF vAdjusted.z <= fGround + eDroneData.fMinimumHoverHeight
			vAdjusted.z = fGround + eDroneData.fMinimumHoverHeight
		ENDIF
	ENDIF	
	
	RETURN vAdjusted
ENDFUNC

FUNC FLOAT GET_FMMC_DRONE_ACCELERATE_UP_VALUE(FMMC_DRONE_STATE &sDroneState)
	SWITCH MC_ServerBD.iDrone_State[sDroneState.iIndex]
		CASE ciTASK_DRONE_CHOOSE_NEW_TASK
			RETURN ciFMMCDRONE_BASE_SPEED_UP_VALUE
		BREAK
		CASE ciTASK_DRONE_GOTO_COORDS
			RETURN ciFMMCDRONE_BASE_SPEED_UP_VALUE
		BREAK
		CASE ciTASK_DRONE_ATTACK_PLAYERS
			RETURN ciFMMCDRONE_COMBAT_SPEED_UP_VALUE
		BREAK
		CASE ciTASK_DRONE_SELF_DESTRUCT_CHARGE_PLAYERS
			RETURN ciFMMCDRONE_SELF_DESTRUCT_SPEED_UP_VALUE
		BREAK
	ENDSWITCH	
	RETURN ciFMMCDRONE_BASE_SPEED_UP_VALUE
ENDFUNC
FUNC FLOAT GET_FMMC_DRONE_ACCELERATE_DOWN_VALUE(FMMC_DRONE_STATE &sDroneState)
	SWITCH MC_ServerBD.iDrone_State[sDroneState.iIndex]
		CASE ciTASK_DRONE_CHOOSE_NEW_TASK
			RETURN ciFMMCDRONE_BASE_SPEED_DOWN_VALUE
		BREAK
		CASE ciTASK_DRONE_GOTO_COORDS
			RETURN ciFMMCDRONE_BASE_SPEED_DOWN_VALUE
		BREAK
		CASE ciTASK_DRONE_ATTACK_PLAYERS
			RETURN ciFMMCDRONE_COMBAT_SPEED_DOWN_VALUE
		BREAK
		CASE ciTASK_DRONE_SELF_DESTRUCT_CHARGE_PLAYERS
			RETURN ciFMMCDRONE_SELF_DESTRUCT_SPEED_DOWN_VALUE
		BREAK
	ENDSWITCH	
	RETURN ciFMMCDRONE_BASE_SPEED_DOWN_VALUE
ENDFUNC

FUNC BOOL SHOULD_FMMC_DRONE_USE_PATHFINDING(FMMC_DRONE_DATA &eDroneData, LOCAL_FMMC_DRONE_DATA &sDroneDataLocal, FMMC_DRONE_STATE &sDroneState)
	IF IS_BIT_SET(eDroneData.iBS, ciFMMC_DroneBS_DisablePathfinding)
		RETURN FALSE
	ENDIF
		
	SWITCH MC_ServerBD.iDrone_State[sDroneState.iIndex]
		CASE ciTASK_DRONE_CHOOSE_NEW_TASK
			RETURN FALSE
		BREAK
		CASE ciTASK_DRONE_GOTO_COORDS
			RETURN TRUE
		BREAK
		CASE ciTASK_DRONE_ATTACK_PLAYERS			
			IF IS_BIT_SET(sDroneDataLocal.iBS, ciFMMCDRONE_BS_Lost_LOS)
				MC_REINIT_NET_TIMER_WITH_TIMESTAMP(sDroneDataLocal.iTimeStampContinueWithPath)
				RETURN TRUE
			ENDIF
			IF MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(sDroneDataLocal.iTimeStampContinueWithPath)
			AND NOT MC_HAS_NET_TIMER_EXPIRED_WITH_TIME_STAMP(sDroneDataLocal.iTimeStampContinueWithPath, 2500)
				RETURN TRUE
			ENDIF
		BREAK
		CASE ciTASK_DRONE_SELF_DESTRUCT_CHARGE_PLAYERS
			RETURN TRUE
		BREAK
	ENDSWITCH
		
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_FMMC_DRONE_TASK_ASSUME_PATH(FMMC_DRONE_STATE &sDroneState)
	
	SWITCH MC_ServerBD.iDrone_State[sDroneState.iIndex]
		CASE ciTASK_DRONE_CHOOSE_NEW_TASK
			RETURN FALSE
		BREAK
		CASE ciTASK_DRONE_GOTO_COORDS
			RETURN TRUE
		BREAK
		CASE ciTASK_DRONE_ATTACK_PLAYERS			
			RETURN FALSE
		BREAK
		CASE ciTASK_DRONE_SELF_DESTRUCT_CHARGE_PLAYERS
			RETURN TRUE
		BREAK
	ENDSWITCH
		
	RETURN FALSE
ENDFUNC

PROC PROCESS_FMMC_DRONE_MOVEMENT(FMMC_DRONE_DATA &eDroneData, LOCAL_FMMC_DRONE_DATA &sDroneDataLocal, FMMC_DRONE_STATE &sDroneState)
	
	IF IS_FMMC_DRONE_IN_A_DEAD_STATE(sDroneState)
		PROCESS_FMMC_DRONE_FROZEN_ON_GROUND(sDroneDataLocal, sDroneState)
		STOP_FMMC_DRONE_FLIGHT_LOOP_SFX(sDroneState.iIndex)
		EXIT
	ENDIF
	
	IF NOT HAS_COLLISION_LOADED_AROUND_ENTITY(sDroneState.oiIndex)
		SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(sDroneState.oiIndex, TRUE)
		STOP_FMMC_DRONE_FLIGHT_LOOP_SFX(sDroneState.iIndex)
		EXIT
	ENDIF
	
	PLAY_FMMC_DRONE_FLIGHT_LOOP_SFX(sDroneState, sDroneState.oiIndex)
	
	// Position -----	
	FREEZE_ENTITY_POSITION(sDroneState.oiIndex, FALSE)	
	SET_ENTITY_HAS_GRAVITY(sDroneState.oiIndex, FALSE)	
	
	FLOAT fGround
	FLOAT fGroundTarget
	FLOAT fBaseSpeed = GET_FMMC_DRONE_BASE_MOVEMENT_SPEED(eDroneData, sDroneState)
	VECTOR vAdjustedTarget = GET_FMMC_DRONE_ADJUSTED_TARGET_POSITION(eDroneData, sDroneDataLocal, fGroundTarget)	
	VECTOR vFinalTarget = vAdjustedTarget
	 
	// Pathfinding. (sometimes a slight rubber banding. Fix will be to assign a direction to each waypoint so that we can tell which we should start at based on the direction the drone is travelling when we finished generating them)
	IF SHOULD_FMMC_DRONE_USE_PATHFINDING(eDroneData, sDroneDataLocal, sDroneState) 
		
		PROCESS_FMMC_DRONE_WAYPOINT_GENERATION_TO_TARGET(sDroneDataLocal, sDroneState, vAdjustedTarget)
			
		#IF IS_DEBUG_BUILD IF bFMMcDroneDebugPathfinding PRINT_ON_SCREEN_DEBUG_TEXT_WITH_INT("iCurrentPath: ", sDroneDataLocal.iCurrentPath) ENDIF #ENDIF
		#IF IS_DEBUG_BUILD IF bFMMcDroneDebugPathfinding PRINT_ON_SCREEN_DEBUG_TEXT_WITH_INT("Waypoint: ", sDroneDataLocal.iCurrentWaypoint) ENDIF #ENDIF
		
		// If we can't assign it yet to a waypoint then just stay stationary.
		IF NOT SHOULD_FMMC_DRONE_TASK_ASSUME_PATH(sDroneState) 
			vAdjustedTarget = sDroneState.vPos
		ENDIF
		
		IF NOT IS_VECTOR_ZERO(sDroneDataLocal.sPath.vDroneWaypoints[0])
			
			// Draw debug for current path.
			#IF IS_DEBUG_BUILD 
			IF bFMMcDroneDebugPathfinding 
				INT iWayPoint
				FOR iWayPoint = 0 TO ciFMMCDRONE_WAYPOINTS_MAX-1
					IF NOT IS_VECTOR_ZERO(sDroneDataLocal.sPath.vDroneWaypoints[iWayPoint])
						#IF IS_DEBUG_BUILD IF bFMMcDroneDebugPathfinding DRAW_MARKER(MARKER_SPHERE, sDroneDataLocal.sPath.vDroneWaypoints[iWayPoint], (<<0.0, 0.0, 0.0>>), (<<0.0, 0.0, 0.0>>), (<<0.25, 0.25, 0.25>>), 0, 175, 175, 100) ENDIF #ENDIF
						IF iWayPoint < ciFMMCDRONE_WAYPOINTS_MAX-1
							IF NOT IS_VECTOR_ZERO(sDroneDataLocal.sPath.vDroneWaypoints[iWayPoint+1])
								#IF IS_DEBUG_BUILD IF bFMMcDroneDebugPathfinding DRAW_DEBUG_LINE(sDroneDataLocal.sPath.vDroneWaypoints[iWayPoint], sDroneDataLocal.sPath.vDroneWaypoints[iWayPoint+1], 255, 0, 0) ENDIF #ENDIF		
							ENDIF
						ENDIF
					ENDIF
				ENDFOR
			ENDIF
			#ENDIF
							
			PROCESS_FMMC_DRONE_CURRENT_WAYPOINT(sDroneState, sDroneDataLocal, vAdjustedTarget)
		ENDIF
	ELSE			
		// Makes sure the drone isn't pressed up against something constantly.
		VECTOR vTemp, vPos
		ENTITY_INDEX eiHit
		SHAPETEST_STATUS stStatus
		INT iHitSomething
	  			
		IF sDroneDataLocal.stiDroneCollisionChecks = NULL
			IF NOT MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(sDroneDataLocal.iTimeStampShapetestTimer)
			OR MC_HAS_NET_TIMER_EXPIRED_WITH_TIME_STAMP(sDroneDataLocal.iTimeStampShapetestTimer, 1000)
				INT iLOSFlags = SCRIPT_INCLUDE_MOVER | SCRIPT_INCLUDE_VEHICLE | SCRIPT_INCLUDE_PED | SCRIPT_INCLUDE_RAGDOLL | SCRIPT_INCLUDE_OBJECT | SCRIPT_INCLUDE_PICKUP | SCRIPT_INCLUDE_GLASS
				sDroneDataLocal.stiDroneCollisionChecks = START_SHAPE_TEST_CAPSULE(vAdjustedTarget, sDroneState.vPos, 0.5, iLOSFlags, sDroneState.oiIndex)				
				MC_REINIT_NET_TIMER_WITH_TIMESTAMP(sDroneDataLocal.iTimeStampShapetestTimer)
			ENDIF
		ELSE
			stStatus = GET_SHAPE_TEST_RESULT(sDroneDataLocal.stiDroneCollisionChecks, iHitSomething, vPos, vTemp, eiHit)
			IF stStatus = SHAPETEST_STATUS_RESULTS_READY						
				sDroneDataLocal.vLastShapetestCollision = vPos			
			ELIF stStatus = SHAPETEST_STATUS_NONEXISTENT
				sDroneDataLocal.stiDroneCollisionChecks = NULL
			ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD IF bFMMcDroneDebugPosition DRAW_DEBUG_LINE(vAdjustedTarget, sDroneState.vPos, 255, 0, 0) ENDIF #ENDIF		
		
		IF NOT IS_VECTOR_ZERO(sDroneDataLocal.vLastShapetestCollision)	
			#IF IS_DEBUG_BUILD IF bFMMcDroneDebugPosition DRAW_MARKER(MARKER_SPHERE, sDroneDataLocal.vLastShapetestCollision, (<<0.0, 0.0, 0.0>>), (<<0.0, 0.0, 0.0>>), (<<0.5, 0.5, 0.5>>), 0, 255, 255, 200) ENDIF #ENDIF		
			VECTOR vDir = NORMALISE_VECTOR(vAdjustedTarget - sDroneDataLocal.vLastShapetestCollision)
			vAdjustedTarget = (sDroneDataLocal.vLastShapetestCollision - vDir)
		ENDIF	
	ENDIF
		
	VECTOR vDirection = NORMALISE_VECTOR(vAdjustedTarget-sDroneState.vPos)		
	FLOAT fHeadingTarget = GET_HEADING_FROM_COORDS_LA(sDroneState.vPos, sDroneDataLocal.vTargetLookAt)
	FLOAT fPercentageProgress = 1.0-(sDroneDataLocal.fDistToGoto / sDroneDataLocal.fDistToStart)	
	
	fPercentageProgress = CLAMP(fPercentageProgress, 0.0, 1.0)

	#IF IS_DEBUG_BUILD IF bFMMcDroneDebugPosition PRINT_ON_SCREEN_DEBUG_TEXT_WITH_FLOAT("fPercentageProgress: ", fPercentageProgress) ENDIF #ENDIF
	#IF IS_DEBUG_BUILD IF bFMMcDroneDebugPosition PRINT_ON_SCREEN_DEBUG_TEXT_WITH_FLOAT("fDistToGoto: ", sDroneDataLocal.fDistToGoto) ENDIF #ENDIF
	#IF IS_DEBUG_BUILD IF bFMMcDroneDebugPosition PRINT_ON_SCREEN_DEBUG_TEXT_WITH_FLOAT("fArrivalRadius: ", sDroneDataLocal.fArrivalRadius) ENDIF #ENDIF
	#IF IS_DEBUG_BUILD IF bFMMcDroneDebugPosition PRINT_ON_SCREEN_DEBUG_TEXT_WITH_FLOAT("sDroneDataLocal.fAccel: ", sDroneDataLocal.fAccel) ENDIF #ENDIF
	
	VECTOR vVelocity
	
	IF ARE_VECTORS_ALMOST_EQUAL(sDroneState.vPos, vFinalTarget, ciFMMCDRONE_ARRIVAL_THRESHOLD, TRUE)
		#IF IS_DEBUG_BUILD IF bFMMcDroneDebugPosition PRINT_ON_SCREEN_DEBUG_TEXT("Reset Accel, at At location. (Resting)") ENDIF #ENDIF
		
		sDroneDataLocal.fAccel -= (0.1*GET_FRAME_TIME())
		
		sDroneDataLocal.fAccel = CLAMP(sDroneDataLocal.fAccel, 0.00, 1.0)
		
		vVelocity = GET_FMMC_DRONE_MOVEMENT_WOBBLE_VECTOR(sDroneDataLocal, sDroneState)
		
	ELIF sDroneDataLocal.fDistToGoto <= sDroneDataLocal.fArrivalRadius
		#IF IS_DEBUG_BUILD IF bFMMcDroneDebugPosition PRINT_ON_SCREEN_DEBUG_TEXT("Reset Accel, at At location. (slowing)") ENDIF #ENDIF
		
		sDroneDataLocal.fAccel -= (GET_FMMC_DRONE_ACCELERATE_DOWN_VALUE(sDroneState)*GET_FRAME_TIME())
		
		sDroneDataLocal.fAccel = CLAMP(sDroneDataLocal.fAccel, 0.1, 1.0)
		
		vVelocity = ((vDirection*fBaseSpeed)*sDroneDataLocal.fAccel)
	ELSE
		IF SHOULD_FMMC_DRONE_SPEED_DOWN(eDroneData, sDroneState, fPercentageProgress)
			#IF IS_DEBUG_BUILD IF bFMMcDroneDebugPosition PRINT_ON_SCREEN_DEBUG_TEXT("Should Slow Down") ENDIF #ENDIF
			sDroneDataLocal.fAccel -= (GET_FMMC_DRONE_ACCELERATE_DOWN_VALUE(sDroneState)*GET_FRAME_TIME())
		ELSE
			sDroneDataLocal.fAccel += (GET_FMMC_DRONE_ACCELERATE_UP_VALUE(sDroneState)*GET_FRAME_TIME())
		ENDIF
		
		sDroneDataLocal.fAccel = CLAMP(sDroneDataLocal.fAccel, 0.4, 1.0)
		
		vVelocity = ((vDirection*fBaseSpeed)*sDroneDataLocal.fAccel)
	ENDIF
	
	// Keep it floating	or equalise our height within a ciFMMCDRONE_ASCEND_DESCEND_HEIGHT_EQUALISATION_THRESHOLD meter distance.	
	IF GET_GROUND_Z_FOR_3D_COORD(sDroneState.vPos, fGround)
	AND sDroneState.vPos.z <= fGround+eDroneData.fMinimumHoverHeight
		vVelocity.z += (ciFMMCDRONE_ASCEND_DESCEND_QUICKLY_VELOCITY*GET_FRAME_TIME())
		#IF IS_DEBUG_BUILD IF bFMMcDroneDebugPosition PRINT_ON_SCREEN_DEBUG_TEXT("Climb (G)") ENDIF #ENDIF		
		
	ELIF sDroneState.vPos.z > vFinalTarget.z+ciFMMCDRONE_ASCEND_DESCEND_HEIGHT_EQUALISATION_THRESHOLD
		vVelocity.z -= (ciFMMCDRONE_ASCEND_DESCEND_GENTLY_VELOCITY*GET_FRAME_TIME())		
		#IF IS_DEBUG_BUILD IF bFMMcDroneDebugPosition PRINT_ON_SCREEN_DEBUG_TEXT("Lower (E)") ENDIF #ENDIF
		
	ELIF sDroneState.vPos.z < vFinalTarget.z-ciFMMCDRONE_ASCEND_DESCEND_HEIGHT_EQUALISATION_THRESHOLD
		vVelocity.z += (ciFMMCDRONE_ASCEND_DESCEND_GENTLY_VELOCITY*GET_FRAME_TIME())		
		#IF IS_DEBUG_BUILD IF bFMMcDroneDebugPosition PRINT_ON_SCREEN_DEBUG_TEXT("Climb (E)") ENDIF #ENDIF
	
	ENDIF
	
	#IF IS_DEBUG_BUILD IF bFMMcDroneDebugPosition PRINT_ON_SCREEN_DEBUG_TEXT_WITH_FLOAT("fGround: ", fGround) ENDIF #ENDIF
	#IF IS_DEBUG_BUILD IF bFMMcDroneDebugPosition PRINT_ON_SCREEN_DEBUG_TEXT_WITH_FLOAT("sDroneState.vPos.z: ", sDroneState.vPos.z) ENDIF #ENDIF		
	#IF IS_DEBUG_BUILD IF bFMMcDroneDebugPosition OR bFMMcDroneDebugPathfinding DRAW_MARKER(MARKER_SPHERE, vFinalTarget, (<<0.0, 0.0, 0.0>>), (<<0.0, 0.0, 0.0>>), (<<0.7, 0.7, 0.7>>)) ENDIF #ENDIF
	#IF IS_DEBUG_BUILD IF bFMMcDroneDebugPosition OR bFMMcDroneDebugPathfinding DRAW_MARKER(MARKER_SPHERE, vAdjustedTarget, (<<0.0, 0.0, 0.0>>), (<<0.0, 0.0, 0.0>>), (<<0.7, 0.7, 0.7>>)) ENDIF #ENDIF
		
	SET_ENTITY_VELOCITY(sDroneState.oiIndex, vVelocity)
		
	// Heading -----
	SET_FMMC_DRONE_ROTATION(eDroneData, sDroneDataLocal, sDroneState, fHeadingTarget-180)
	
	CLEAR_BIT(sDroneDataLocal.iBS, ciFMMCDRONE_BS_RotationAdjusted)
	
	IF HAS_ENTITY_COLLIDED_WITH_ANYTHING(sDroneState.oiIndex)
		IF NOT MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(sDroneDataLocal.iTimeStampCollision)
		OR MC_HAS_NET_TIMER_EXPIRED_WITH_TIME_STAMP(sDroneDataLocal.iTimeStampCollision, 500)
			MC_REINIT_NET_TIMER_WITH_TIMESTAMP(sDroneDataLocal.iTimeStampCollision)
			PLAY_FMMC_DRONE_BUMP_SFX(sDroneState.mn, sDroneState.vPos)
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL SHOULD_FMMC_DRONE_WEAPON_RELOAD(LOCAL_FMMC_DRONE_DATA &sDroneDataLocal, INT iWeap)
	IF sDroneDataLocal.sWeaponLocal[iWeap].iAmmo <= 0
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_FMMC_DRONE_WEAPON_RELOADED(FMMC_DRONE_DATA &eDroneData, LOCAL_FMMC_DRONE_DATA &sDroneDataLocal, INT iWeap)
	
	IF NOT MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(sDroneDataLocal.sWeaponLocal[iWeap].iReloadingTimeStamp)
		MC_START_NET_TIMER_WITH_TIMESTAMP(sDroneDataLocal.sWeaponLocal[iWeap].iReloadingTimeStamp)
		RETURN FALSE
	ENDIF
	
	IF MC_HAS_NET_TIMER_EXPIRED_WITH_TIME_STAMP(sDroneDataLocal.sWeaponLocal[iWeap].iReloadingTimeStamp, eDroneData.sWeaponData[iWeap].iShotReloadTime)		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_FMMC_DRONE_SHOT_READY(FMMC_DRONE_DATA &eDroneData, LOCAL_FMMC_DRONE_DATA &sDroneDataLocal, INT iWeap)
	
	IF NOT MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(sDroneDataLocal.sWeaponLocal[iWeap].iShotTimeStamp)
		MC_START_NET_TIMER_WITH_TIMESTAMP(sDroneDataLocal.sWeaponLocal[iWeap].iShotTimeStamp)
		RETURN FALSE
	ENDIF
	
	IF MC_HAS_NET_TIMER_EXPIRED_WITH_TIME_STAMP(sDroneDataLocal.sWeaponLocal[iWeap].iShotTimeStamp, eDroneData.sWeaponData[iWeap].iShotIntervalTime)
		MC_STOP_NET_TIMER_WITH_TIMESTAMP(sDroneDataLocal.sWeaponLocal[iWeap].iShotTimeStamp)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC VECTOR GET_FMMC_DRONE_TARGET_SHOT_END_POSITION(FMMC_DRONE_DATA &eDroneData, LOCAL_FMMC_DRONE_DATA &sDroneDataLocal, FMMC_DRONE_STATE &sDroneState, INT iWeap)
		
	IF eDroneData.sWeaponData[iWeap].iShotAccuracy < 100				
		IF GET_RANDOM_INT_IN_RANGE(0, 101) > eDroneData.sWeaponData[iWeap].iShotAccuracy
			FLOAT fX = GET_RANDOM_FLOAT_IN_RANGE(1.0, 3.0)
			FLOAT fZ = GET_RANDOM_FLOAT_IN_RANGE(1.0, 3.0)
		
			IF GET_RANDOM_INT_IN_RANGE(0, 101) > 50
				fX = -fX
				fZ = -fZ
			ENDIF
			
			RETURN GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(sDroneDataLocal.vTargetLookAt, sDroneState.fHead, <<fX, 0.0, fZ>>)
		ENDIF
	ENDIF
	
	RETURN sDroneDataLocal.vTargetLookAt
ENDFUNC

PROC PROCESS_FMMC_DRONE_WEAPON_FIRE_TASER(FMMC_DRONE_DATA &eDroneData, LOCAL_FMMC_DRONE_DATA &sDroneDataLocal, FMMC_DRONE_STATE &sDroneState, INT iWeap)
		
	IF NOT LOAD_FMMC_DRONE_MUZZLE_FLASH()
		EXIT
	ENDIF
	START_NETWORKED_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_ie_vv_muzzle_flash", sDroneState.oiIndex, <<0.0, -0.20, 0.0>>, <<180, 0.0, 0.0>>, 0.4, FALSE)
	
	VECTOR vShotEnd = GET_FMMC_DRONE_TARGET_SHOT_END_POSITION(eDroneData, sDroneDataLocal, sDroneState, iWeap)
	
	SHOOT_SINGLE_BULLET_BETWEEN_COORDS_IGNORE_ENTITY_NEW(sDroneState.vPos, vShotEnd, 1, FALSE, WEAPONTYPE_STUNGUN, NULL, TRUE, FALSE, DEFAULT, sDroneState.oiIndex, TRUE)
	
	PLAY_SOUND_FROM_ENTITY(-1, "Shock_Fire", sDroneState.oiIndex, GET_FMMC_DRONE_SOUND_SET(), TRUE)
	
	sDroneDataLocal.sWeaponLocal[iWeap].iAmmo--
	
ENDPROC

PROC PROCESS_FMMC_DRONE_WEAPON_FIRE_MACHINEGUN(FMMC_DRONE_DATA &eDroneData, LOCAL_FMMC_DRONE_DATA &sDroneDataLocal, FMMC_DRONE_STATE &sDroneState, INT iWeap)
			
	IF NOT LOAD_FMMC_DRONE_MUZZLE_FLASH()
		EXIT
	ENDIF	
	START_NETWORKED_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_ie_vv_muzzle_flash", sDroneState.oiIndex, <<0.0, -0.20, 0.0>>, <<180, 0.0, 0.0>>, 0.4, FALSE)

	VECTOR vShotEnd = GET_FMMC_DRONE_TARGET_SHOT_END_POSITION(eDroneData, sDroneDataLocal, sDroneState, iWeap)
	
	SHOOT_SINGLE_BULLET_BETWEEN_COORDS_IGNORE_ENTITY_NEW(sDroneState.vPos, vShotEnd, 5, FALSE, WEAPONTYPE_PISTOL, NULL, TRUE, FALSE, DEFAULT, sDroneState.oiIndex, TRUE)
		
	PLAY_SOUND_FROM_ENTITY(-1, "Gun_Fire", sDroneState.oiIndex, GET_FMMC_DRONE_SOUND_SET(), TRUE)
	
	sDroneDataLocal.sWeaponLocal[iWeap].iAmmo--
	
ENDPROC

PROC PROCESS_FMMC_DRONE_WEAPON_FIRE_MISSILE(FMMC_DRONE_DATA &eDroneData, LOCAL_FMMC_DRONE_DATA &sDroneDataLocal, FMMC_DRONE_STATE &sDroneState, INT iWeap)
			
	IF NOT LOAD_FMMC_DRONE_MUZZLE_FLASH()
		EXIT
	ENDIF
	START_NETWORKED_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_ie_vv_muzzle_flash", sDroneState.oiIndex, <<0.0, -0.20, 0.0>>, <<180, 0.0, 0.0>>, 0.4, FALSE)

	VECTOR vShotEnd = GET_FMMC_DRONE_TARGET_SHOT_END_POSITION(eDroneData, sDroneDataLocal, sDroneState, iWeap)
	
	SHOOT_SINGLE_BULLET_BETWEEN_COORDS_IGNORE_ENTITY_NEW(sDroneState.vPos, vShotEnd, 40, FALSE, WEAPONTYPE_RPG, NULL, TRUE, FALSE, DEFAULT, sDroneState.oiIndex, TRUE)
		
	PLAY_SOUND_FROM_ENTITY(-1, "Gun_Fire", sDroneState.oiIndex, GET_FMMC_DRONE_SOUND_SET(), TRUE)
	
	sDroneDataLocal.sWeaponLocal[iWeap].iAmmo--
	
ENDPROC

PROC PROCESS_FMMC_DRONE_WEAPON_FIRE_HEAVY_GUN(FMMC_DRONE_DATA &eDroneData, LOCAL_FMMC_DRONE_DATA &sDroneDataLocal, FMMC_DRONE_STATE &sDroneState, INT iWeap)
	
	IF NOT LOAD_FMMC_DRONE_MUZZLE_FLASH()
		EXIT
	ENDIF
	START_NETWORKED_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_ie_vv_muzzle_flash", sDroneState.oiIndex, <<0.0, -0.20, 0.0>>, <<180, 0.0, 0.0>>, 0.4, FALSE)

	VECTOR vShotEnd = GET_FMMC_DRONE_TARGET_SHOT_END_POSITION(eDroneData, sDroneDataLocal, sDroneState, iWeap)
	
	SHOOT_SINGLE_BULLET_BETWEEN_COORDS_IGNORE_ENTITY_NEW(sDroneState.vPos, vShotEnd, 15, FALSE, WEAPONTYPE_DLC_VEHICLE_RCTANK_GUN, NULL, TRUE, FALSE, DEFAULT, sDroneState.oiIndex, TRUE)
		
	PLAY_SOUND_FROM_ENTITY(-1, "Gun_Fire", sDroneState.oiIndex, GET_FMMC_DRONE_SOUND_SET(), TRUE)
	
	sDroneDataLocal.sWeaponLocal[iWeap].iAmmo--
	
ENDPROC

PROC PROCESS_FMMC_DRONE_WEAPON(FMMC_DRONE_DATA &eDroneData, LOCAL_FMMC_DRONE_DATA &sDroneDataLocal, FMMC_DRONE_STATE &sDroneState, INT iWeap)
	
	#IF IS_DEBUG_BUILD IF bFMMcDroneDebugCombat PRINT_ON_SCREEN_DEBUG_TEXT_WITH_INT("iWeaponType: ", eDroneData.sWeaponData[iWeap].iWeaponType) ENDIF #ENDIF	
	#IF IS_DEBUG_BUILD IF bFMMcDroneDebugCombat PRINT_ON_SCREEN_DEBUG_TEXT_WITH_INT("AmmoMax: ", eDroneData.sWeaponData[iWeap].iAmmoMax) ENDIF #ENDIF	
	#IF IS_DEBUG_BUILD IF bFMMcDroneDebugCombat PRINT_ON_SCREEN_DEBUG_TEXT_WITH_INT("Ammo: ", sDroneDataLocal.sWeaponLocal[iWeap].iAmmo) ENDIF #ENDIF
	
	IF SHOULD_FMMC_DRONE_WEAPON_RELOAD(sDroneDataLocal, iWeap)
		#IF IS_DEBUG_BUILD IF bFMMcDroneDebugCombat PRINT_ON_SCREEN_DEBUG_TEXT_WITH_INT("Reload Progress: ", eDroneData.sWeaponData[iWeap].iShotReloadTime - MC_GET_NET_TIMER_DIFFERENCE_WITH_TIME_STAMP(sDroneDataLocal.sWeaponLocal[iWeap].iReloadingTimeStamp)) ENDIF #ENDIF
		
		IF HAS_FMMC_DRONE_WEAPON_RELOADED(eDroneData, sDroneDataLocal, iWeap)
			sDroneDataLocal.sWeaponLocal[iWeap].iAmmo = eDroneData.sWeaponData[iWeap].iAmmoMax	
			MC_STOP_NET_TIMER_WITH_TIMESTAMP(sDroneDataLocal.sWeaponLocal[iWeap].iReloadingTimeStamp)
		ELSE
			EXIT
		ENDIF
	ENDIF
	
	IF NOT IS_FMMC_DRONE_SHOT_READY(eDroneData, sDroneDataLocal, iWeap)
		EXIT
	ENDIF
	
	SWITCH eDroneData.sWeaponData[iWeap].iWeaponType
		CASE ciFMMC_DroneWeapon_Taser
			PROCESS_FMMC_DRONE_WEAPON_FIRE_TASER(eDroneData, sDroneDataLocal, sDroneState, iWeap)
		BREAK
		
		CASE ciFMMC_DroneWeapon_Machinegun
			PROCESS_FMMC_DRONE_WEAPON_FIRE_MACHINEGUN(eDroneData, sDroneDataLocal, sDroneState, iWeap)
		BREAK
		
		CASE ciFMMC_DroneWeapon_Missile
			PROCESS_FMMC_DRONE_WEAPON_FIRE_MISSILE(eDroneData, sDroneDataLocal, sDroneState, iWeap)
		BREAK
		
		CASE ciFMMC_DroneWeapon_HeavyGun
			PROCESS_FMMC_DRONE_WEAPON_FIRE_HEAVY_GUN(eDroneData, sDroneDataLocal, sDroneState, iWeap)
		BREAK
	ENDSWITCH
	
ENDPROC

PROC PROCESS_FMMC_DRONE_WEAPONS(FMMC_DRONE_DATA &eDroneData, LOCAL_FMMC_DRONE_DATA &sDroneDataLocal, FMMC_DRONE_STATE &sDroneState)	
	
	IF IS_FMMC_DRONE_IN_A_DEAD_STATE(sDroneState)
		EXIT
	ENDIF
	
	INT iWeap
	FOR iWeap = 0 TO ciMAX_FMMC_DRONE_WEAPONS-1
		IF eDroneData.sWeaponData[iWeap].iWeaponType = ciFMMC_DroneWeapon_None
			RELOOP
		ENDIF		
		PROCESS_FMMC_DRONE_WEAPON(eDroneData, sDroneDataLocal, sDroneState, iWeap)		
	ENDFOR
	
ENDPROC

PROC SET_FMMC_DRONE_CLIENT_VECTOR_TARGET(INT iDrone, VECTOR vTargetPos, VECTOR vDronePos)
	IF IS_VECTOR_ZERO(vTargetPos)
		EXIT
	ENDIF
	IF ARE_VECTORS_ALMOST_EQUAL(sLocalFmmcDroneData[iDrone].vTarget, vTargetPos, ciFMMCDRONE_WOBBLE_RESET_THRESHOLD+0.1)
		EXIT
	ENDIF
	PRINTLN("[FMMC_DRONES ", iDrone, "][CLIENT] - SET_FMMC_DRONE_CLIENT_VECTOR_TARGET - New Target: ", vTargetPos, " Old Target: ", sLocalFmmcDroneData[iDrone].vTarget)
	
	sLocalFmmcDroneData[iDrone].vTarget = vTargetPos
	sLocalFmmcDroneData[iDrone].fDistToStart = VDIST2(vDronePos, vTargetPos)
ENDPROC

PROC SET_FMMC_DRONE_CLIENT_VECTOR_TARGET_LOOK_AT(LOCAL_FMMC_DRONE_DATA &sDroneDataLocal, FMMC_DRONE_STATE &sDroneState, VECTOR vTargetPos)
	#IF IS_DEBUG_BUILD
	IF NOT ARE_VECTORS_ALMOST_EQUAL(sDroneDataLocal.vTargetLookAt, vTargetPos, 0.25)
		PRINTLN("[FMMC_DRONES ", sDroneState.iIndex, "][CLIENT] - SET_FMMC_DRONE_CLIENT_VECTOR_TARGET_LOOK_AT - New Look Target: ", vTargetPos, " Old Look Target: ", sDroneDataLocal.vTargetLookAt)
	ENDIF	
	#ENDIF	
	UNUSED_PARAMETER(sDroneState)
	sDroneDataLocal.vTargetLookAt = vTargetPos
ENDPROC

PROC SET_FMMC_DRONE_CLIENT_VECTOR_DEFENSIVE_POINT(INT iDrone, VECTOR vTargetPos)
	IF IS_VECTOR_ZERO(vTargetPos)
		EXIT
	ENDIF
	IF ARE_VECTORS_ALMOST_EQUAL(sLocalFmmcDroneData[iDrone].vDefensivePoint, vTargetPos, 1.0)
		EXIT
	ENDIF	
	PRINTLN("[FMMC_DRONES ", iDrone, "][CLIENT] - SET_FMMC_DRONE_CLIENT_VECTOR_DEFENSIVE_POINT - New Target: ", vTargetPos, " Old Target: ", sLocalFmmcDroneData[iDrone].vDefensivePoint)
	
	sLocalFmmcDroneData[iDrone].vDefensivePoint = vTargetPos	
	BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_Drone_DefensivePoint, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, iDrone, DEFAULT, DEFAULT, DEFAULT, vTargetPos.x, vTargetPos.y, vTargetPos.z)	
ENDPROC

PROC PROCESS_SETTING_FMMC_DRONE_CLIENT_VECTOR_DEFENSIVE_POINT(FMMC_DRONE_DATA &eDroneData, LOCAL_FMMC_DRONE_DATA &sDroneDataLocal, FMMC_DRONE_STATE &sDroneState)
	
	IF eDroneData.fDefensiveRadius = 0.0
		EXIT
	ENDIF
	
	SWITCH MC_ServerBD.iDrone_State[sDroneState.iIndex]		
		CASE ciTASK_DRONE_ATTACK_PLAYERS
			IF NOT IS_BIT_SET(eDroneData.iBS, ciFMMC_DroneBS_DefensiveRadiusStartsInCombat)
				EXIT
			ENDIF			
			IF NOT IS_VECTOR_ZERO(sDroneDataLocal.vDefensivePoint)
				EXIT
			ENDIF
			SET_FMMC_DRONE_CLIENT_VECTOR_DEFENSIVE_POINT(sDroneState.iIndex, sDroneState.vPos)
		BREAK
		
		DEFAULT
			IF IS_BIT_SET(eDroneData.iBS, ciFMMC_DroneBS_DefensiveRadiusStartsInCombat)
				EXIT
			ENDIF
			IF NOT IS_VECTOR_ZERO(sDroneDataLocal.vDefensivePoint)
				EXIT
			ENDIF
			SET_FMMC_DRONE_CLIENT_VECTOR_DEFENSIVE_POINT(sDroneState.iIndex, sDroneState.vPos)
		BREAK
	ENDSWITCH
	
ENDPROC

// ###### GOTO TASK
PROC SET_FMMC_DRONE_CLIENT_PROGRESS(INT iDrone)
	PRINTLN("[FMMC_DRONES ", iDrone, "][CLIENT] - SET_FMMC_DRONE_CLIENT_PROGRESS - New Progress Index: ", MC_serverBD.iDrone_AssociatedGotoProgress[iDrone], " Old Progress Index: ", sLocalSharedAssGotoTaskData.iProgress[iDrone])
	sLocalSharedAssGotoTaskData.iProgress[iDrone] = MC_serverBD.iDrone_AssociatedGotoProgress[iDrone]	
ENDPROC

PROC PROCESS_FMMC_DRONE_SHOULD_MOVE_ON_TO_SPECIFIC_GOTO_PROGRESS_INDEX_CLIENT(FMMC_DRONE_STATE &sDroneState)
	
	IF bIsAnySpectator
		EXIT
	ENDIF
		
	IF FMMC_IS_LONG_BIT_SET(sLocalSharedAssGotoTaskData.iProgressOverrideEventFired, sDroneState.iIndex)
		IF NOT DOES_ANY_TEAM_HAVE_NEW_PRIORITY_THIS_FRAME()
			EXIT
		ELSE
			FMMC_CLEAR_LONG_BIT(sLocalSharedAssGotoTaskData.iProgressOverrideEventFired, sDroneState.iIndex)
		ENDIF
	ENDIF
	
	IF NOT sDroneState.bExists
		EXIT
	ENDIF
		
	IF NOT sDroneState.bHaveControl
		EXIT
	ENDIF
		
	INT iTeam = MC_PlayerBD[iLocalPart].iTeam
	INT iRule = GET_LOCAL_PLAYER_CURRENT_RULE()
	
	IF iRule >= FMMC_MAX_RULES
	OR iRule <= -1
		EXIT
	ENDIF
	
	INT iAssignmentIndex = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDroneAssGotoTask_RulePoolAssignmentIndex[iRule][sDroneState.iIndex]	
	INT iAssignmentStartingGoto = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDroneAssGotoTask_RulePoolAssignmentStartingGoto[iRule][sDroneState.iIndex]
	
	IF iAssignmentIndex = ASSOCIATED_GOTO_TASK_POOL_INDEX__OFF
		EXIT
	ENDIF
	
	PRINTLN("[FMMC_DRONES ", sDroneState.iIndex, "][CLIENT] - PROCESS_PED_SHOULD_MOVE_ON_TO_SPECIFIC_GOTO_PROGRESS_INDEX_CLIENT - Broadcasting event to override Drone current gotos. iDroneAssGotoTask_RulePoolAssignmentIndex: ", iAssignmentIndex, " iDroneAssGotoTask_RulePoolAssignmentStartingGoto: ", GET_INDEX_FROM_MULTIPLE_INDEX_OPTION(iAssignmentStartingGoto, ASSOCIATED_GOTO_TASK_POOL_START__AMOUNT_OF_OPTIONS, TRUE))
		
	BOOL bInterrupt = (iAssignmentStartingGoto % 2 = 1) OR (iAssignmentStartingGoto = ASSOCIATED_GOTO_TASK_POOL_START__STAY_ON_CURRENT_AND_INTERRUPT)

	FMMC_SET_LONG_BIT(sLocalSharedAssGotoTaskData.iProgressOverrideEventFired, sDroneState.iIndex)
	BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_OverrideDroneAssGotoProgress, DEFAULT, sDroneState.iIndex, DEFAULT, DEFAULT, DEFAULT, DEFAULT, bInterrupt, iAssignmentIndex, GET_INDEX_FROM_MULTIPLE_INDEX_OPTION(iAssignmentStartingGoto, ASSOCIATED_GOTO_TASK_POOL_START__AMOUNT_OF_OPTIONS, TRUE))
	
ENDPROC

FUNC BOOL HAS_COMPLETED_FMMC_DRONE_ASSOCIATED_GOTO_ACTIONS(FMMC_DRONE_DATA &eDroneData, LOCAL_FMMC_DRONE_DATA &sDroneDataLocal, FMMC_DRONE_STATE &sDroneState)

	IF IS_ASSOCIATED_GOTO_TASK_DATA__BIT_SET(MC_serverBD.iDrone_AssociatedGotoPoolIndex[sDroneState.iIndex], g_FMMC_STRUCT.sAssociatedGotoPool[eDroneData.iAssociatedPoolStartingIndex].sAssociatedGotoTaskData, MC_serverBD.iDrone_AssociatedGotoProgress[sDroneState.iIndex], PED_ASSOCIATED_GOTO_TASK_TYPE_ACHIEVEHEADING)
		IF NOT FMMC_IS_LONG_BIT_SET(sLocalSharedAssGotoTaskData.iCompletedAchieveHeadingBitset, sDroneState.iIndex)
			FLOAT fTarget = GET_ASSOCIATED_GOTO_TASK_DATA__ACHIEVE_FINAL_HEADING(MC_serverBD.iDrone_AssociatedGotoPoolIndex[sDroneState.iIndex], g_FMMC_STRUCT.sAssociatedGotoPool[eDroneData.iAssociatedPoolStartingIndex].sAssociatedGotoTaskData, MC_serverBD.iDrone_AssociatedGotoProgress[sDroneState.iIndex])
			SET_FMMC_DRONE_ROTATION(eDroneData, sDroneDataLocal, sDroneState, fTarget)
			
			IF IS_HEADING_ACCEPTABLE(sDroneState.fHead, fTarget, ciFMMCDRONE_ACCEPTABLE_HEADING)
				PRINTLN("[FMMC_DRONES ", sDroneState.iIndex, "][CLIENT][Progress ", sLocalSharedAssGotoTaskData.iProgress[sDroneState.iIndex], "] - HAS_COMPLETED_FMMC_DRONE_ASSOCIATED_GOTO_ACTIONS - Heading Achieved")
				FMMC_SET_LONG_BIT(sLocalSharedAssGotoTaskData.iCompletedAchieveHeadingBitset, sDroneState.iIndex)
				BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_DATA_SET_DroneAssGotoProgress_AchieveHeading, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, sDroneState.iIndex)
			#IF IS_DEBUG_BUILD
			ELSE
				PRINTLN("[FMMC_DRONES ", sDroneState.iIndex, "][CLIENT][Progress ", sLocalSharedAssGotoTaskData.iProgress[sDroneState.iIndex], "] - HAS_COMPLETED_FMMC_DRONE_ASSOCIATED_GOTO_ACTIONS - Waiting for Heading to be Achieved")
			#ENDIF
			ENDIF

			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_ASSOCIATED_GOTO_TASK_DATA__WAIT_TIME_SET(MC_serverBD.iDrone_AssociatedGotoPoolIndex[sDroneState.iIndex], g_FMMC_STRUCT.sAssociatedGotoPool[eDroneData.iAssociatedPoolStartingIndex].sAssociatedGotoTaskData, MC_serverBD.iDrone_AssociatedGotoProgress[sDroneState.iIndex])
		PRINTLN("[FMMC_DRONES ", sDroneState.iIndex, "][CLIENT][Progress ", sLocalSharedAssGotoTaskData.iProgress[sDroneState.iIndex], "] - HAS_COMPLETED_FMMC_DRONE_ASSOCIATED_GOTO_ACTIONS - Processing")
		
		IF NOT FMMC_IS_LONG_BIT_SET(sLocalSharedAssGotoTaskData.iCompletedWaitBitset, sDroneState.iIndex)
			
			IF NOT HAS_NET_TIMER_STARTED(sLocalSharedAssGotoTaskData.tdWaitTimeStamp[sDroneState.iIndex])
				START_NET_TIMER(sLocalSharedAssGotoTaskData.tdWaitTimeStamp[sDroneState.iIndex])
				PRINTLN("[FMMC_DRONES ", sDroneState.iIndex, "][CLIENT][Progress ", sLocalSharedAssGotoTaskData.iProgress[sDroneState.iIndex], "] - HAS_COMPLETED_FMMC_DRONE_ASSOCIATED_GOTO_ACTIONS - Starting Timer.")
			ENDIF
			
			IF IS_ASSOCIATED_GOTO_TASK_DATA__WAIT_TIME_FINISHED(MC_serverBD.iDrone_AssociatedGotoPoolIndex[sDroneState.iIndex], g_FMMC_STRUCT.sAssociatedGotoPool[eDroneData.iAssociatedPoolStartingIndex].sAssociatedGotoTaskData, MC_serverBD.iDrone_AssociatedGotoProgress[sDroneState.iIndex], sLocalSharedAssGotoTaskData.tdWaitTimeStamp[sDroneState.iIndex])
				PRINTLN("[FMMC_DRONES ", sDroneState.iIndex, "][CLIENT][Progress ", sLocalSharedAssGotoTaskData.iProgress[sDroneState.iIndex], "] - HAS_COMPLETED_FMMC_DRONE_ASSOCIATED_GOTO_ACTIONS - Completing GotoWait Timer.")
				RESET_NET_TIMER(sLocalSharedAssGotoTaskData.tdWaitTimeStamp[sDroneState.iIndex])
				FMMC_SET_LONG_BIT(sLocalSharedAssGotoTaskData.iCompletedWaitBitset, sDroneState.iIndex)
				BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_DATA_SET_DroneAssGotoProgress_WaitTime, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, sDroneState.iIndex)
			#IF IS_DEBUG_BUILD
			ELSE
				PRINTLN("[FMMC_DRONES ", sDroneState.iIndex, "][CLIENT][Progress ", sLocalSharedAssGotoTaskData.iProgress[sDroneState.iIndex], "] - HAS_COMPLETED_FMMC_DRONE_ASSOCIATED_GOTO_ACTIONS - Waiting until GotoWait is over.")
			#ENDIF
			ENDIF
			
			RETURN FALSE
			
		ENDIF
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

PROC PROCESS_FMMC_DRONE_ASSOCIATED_GOTO_DATA_CLIENT(FMMC_DRONE_DATA &eDroneData, LOCAL_FMMC_DRONE_DATA &sDroneDataLocal, FMMC_DRONE_STATE &sDroneState)
		
	// Update Progress Index and Vector
	IF (sLocalSharedAssGotoTaskData.iProgress[sDroneState.iIndex] != MC_serverBD.iDrone_AssociatedGotoProgress[sDroneState.iIndex]
	OR NOT FMMC_IS_LONG_BIT_SET(sLocalSharedAssGotoTaskData.iStartedGotoBitset, sDroneState.iIndex))
	OR FMMC_IS_LONG_BIT_SET(sLocalSharedAssGotoTaskData.iReassignBitset, sDroneState.iIndex)
		CLEAR_FMMC_DRONE_ASS_GOTO_DATA(sDroneState.iIndex)	
		SET_FMMC_DRONE_CLIENT_PROGRESS(sDroneState.iIndex)
		
		FMMC_SET_LONG_BIT(sLocalSharedAssGotoTaskData.iStartedGotoBitset, sDroneState.iIndex)
		VECTOR vTarget = GET_ASSOCIATED_GOTO_TASK_DATA__POSITION(MC_serverBD.iDrone_AssociatedGotoPoolIndex[sDroneState.iIndex], g_FMMC_STRUCT.sAssociatedGotoPool[eDroneData.iAssociatedPoolStartingIndex].sAssociatedGotoTaskData, sLocalSharedAssGotoTaskData.iProgress[sDroneState.iIndex])
		BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_DATA_SET_DroneAssGotoProgress_StartGotoData, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, sDroneState.iIndex, DEFAULT, sDroneState.niIndex, DEFAULT, vTarget.x, vTarget.y, vTarget.z)		
		SET_FMMC_DRONE_CLIENT_VECTOR_TARGET(sDroneState.iIndex, vTarget, sDroneState.vPos)
		RESET_FMMC_DRONE_PATH_AND_WAYPOINTS(sDroneDataLocal)
	ENDIF	
		
	// Arrived
	FLOAT fVDIST2_Distance = VDIST2(sDroneDataLocal.vTarget, sDroneState.vPos)
	FLOAT fDistance = fVDIST2_Distance // - eDroneData.fMinimumHoverHeight
	FLOAT fArrivalRadius = POW(GET_ASSOCIATED_GOTO_TASK_DATA__CUSTOM_ARRIVAL_RADIUS(MC_serverBD.iDrone_AssociatedGotoPoolIndex[sDroneState.iIndex], g_FMMC_STRUCT.sAssociatedGotoPool[eDroneData.iAssociatedPoolStartingIndex].sAssociatedGotoTaskData, sLocalSharedAssGotoTaskData.iProgress[sDroneState.iIndex]), 2.0)	
		
	IF fVDIST2_Distance > POW(1.5, 2.0)
		SET_FMMC_DRONE_CLIENT_VECTOR_TARGET_LOOK_AT(sDroneDataLocal, sDroneState, sDroneDataLocal.vTarget)
	ENDIF
	
	fArrivalRadius = CLAMP(fArrivalRadius, 8.0, 999)	
	sDroneDataLocal.fDistToGoto = fDistance
	sDroneDataLocal.fArrivalRadius = fArrivalRadius
	
	IF fDistance <= fArrivalRadius		
		PRINTLN("[FMMC_DRONES ", sDroneState.iIndex, "][CLIENT][Progress ", sLocalSharedAssGotoTaskData.iProgress[sDroneState.iIndex], "] - PROCESS_FMMC_DRONE_ASSOCIATED_GOTO_DATA_CLIENT - Arrived at: ", sDroneDataLocal.vTarget)
		
		IF ARE_ANY_FMMC_DRONE_ASSOCIATED_GOTO_ACTIONS_SET(eDroneData, sDroneState)
			IF NOT HAS_COMPLETED_FMMC_DRONE_ASSOCIATED_GOTO_ACTIONS(eDroneData, sDroneDataLocal, sDroneState)
				PRINTLN("[FMMC_DRONES ", sDroneState.iIndex, "][CLIENT][Progress ", sLocalSharedAssGotoTaskData.iProgress[sDroneState.iIndex], "] - PROCESS_FMMC_DRONE_ASSOCIATED_GOTO_DATA_CLIENT - Performing Actions.")
				EXIT
			ENDIF
		ENDIF
		
		IF NOT FMMC_IS_LONG_BIT_SET(sLocalSharedAssGotoTaskData.iCompletedArrivedBitset, sDroneState.iIndex)
			PRINTLN("[FMMC_DRONES ", sDroneState.iIndex, "][CLIENT][Progress ", sLocalSharedAssGotoTaskData.iProgress[sDroneState.iIndex], "] - PROCESS_FMMC_DRONE_ASSOCIATED_GOTO_DATA_CLIENT - Finished Actions, broadcasting point completion.")
			FMMC_SET_LONG_BIT(sLocalSharedAssGotoTaskData.iCompletedArrivedBitset, sDroneState.iIndex)			
			BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_DATA_SET_DroneAssGotoProgress_Arrived, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, sDroneState.iIndex)			
		ENDIF
		EXIT
	ENDIF
	
	// Travelling
	PRINTLN("[FMMC_DRONES ", sDroneState.iIndex, "][CLIENT][Progress ", sLocalSharedAssGotoTaskData.iProgress[sDroneState.iIndex], "] - PROCESS_FMMC_DRONE_ASSOCIATED_GOTO_DATA_CLIENT - Travelling to: ", sDroneDataLocal.vTarget, " Currently at: ", sDroneState.vPos, " fDistance: ", fDistance, " fArrivalRadius: ", fArrivalRadius)
	
ENDPROC

FUNC BOOL GET_FMMC_DRONE_PLAYER_TARGET(LOCAL_FMMC_DRONE_DATA &sDroneDataLocal, FMMC_DRONE_STATE &sDroneState)
	
	IF sDroneDataLocal.piPlayerTarget != INVALID_PLAYER_INDEX()
	AND NETWORK_IS_PLAYER_ACTIVE(sDroneDataLocal.piPlayerTarget)
	AND NOT IS_PED_INJURED(sDroneDataLocal.pedPlayerTarget)
		
		IF NOT MC_RUN_NET_TIMER_WITH_TIMESTAMP(sDroneDataLocal.iTimeStampTargetSwitch, ciFMMCDRONE_COMBAT_TARGET_SWITCH)	
			#IF IS_DEBUG_BUILD IF bFMMcDroneDebugCombat PRINT_ON_SCREEN_DEBUG_TEXT("Have Target - Waiting for Switch Timer") ENDIF #ENDIF		
			RETURN TRUE
		ENDIF
		
	ENDIF
	
	#IF IS_DEBUG_BUILD IF bFMMcDroneDebugCombat PRINT_ON_SCREEN_DEBUG_TEXT("Seeking Target") ENDIF #ENDIF
	
	PLAYER_INDEX piPlayerTemp
	PED_INDEX pedPlayerTemp
	
	// Use Native first for cheapest method of grabbing nearest player.
	piPlayerTemp = INT_TO_NATIVE(PLAYER_INDEX, GET_NEAREST_PLAYER_TO_ENTITY(sDroneState.oiIndex)) // GET_NEAREST_PLAYER_TO_ENTITY_ON_TEAM Hostile Team Option?		 
	IF NETWORK_IS_PLAYER_ACTIVE(piPlayerTemp)			
		pedPlayerTemp = GET_PLAYER_PED(piPlayerTemp)		
		IF NOT IS_PED_INJURED(pedPlayerTemp)
		AND NOT IS_PLAYER_A_SPECTATOR(piPlayerTemp)
			sDroneDataLocal.piPlayerTarget = piPlayerTemp 
			sDroneDataLocal.pedPlayerTarget = pedPlayerTemp
			
			MC_REINIT_NET_TIMER_WITH_TIMESTAMP(sDroneDataLocal.iTimeStampTargetSwitch)
			RETURN TRUE
		ENDIF
	ENDIF
	
	// Loop through participants to find a suitable player target as the above native failed.
	INT iPart = -1
	DO_PARTICIPANT_LOOP_FLAGS eFlags = DPLF_CHECK_PLAYER_OK | DPLF_CHECK_PED_ALIVE | DPLF_FILL_PLAYER_IDS 
	WHILE DO_PARTICIPANT_LOOP(iPart, eFlags)
			
		IF IS_PARTICIPANT_A_SPECTATOR(iPart)
			RELOOP
		ENDIF
	
		sDroneDataLocal.piPlayerTarget = piParticipantLoop_PlayerIndex 
		sDroneDataLocal.pedPlayerTarget = piParticipantLoop_PedIndex
		
		MC_REINIT_NET_TIMER_WITH_TIMESTAMP(sDroneDataLocal.iTimeStampTargetSwitch)
		RETURN TRUE
		
	ENDWHILE
	
	RETURN FALSE
	
ENDFUNC

PROC PROCESS_FMMC_DRONE_DEFENSIVE_AREA_ADJUST_TARGET_COORD(FMMC_DRONE_DATA &eDroneData, LOCAL_FMMC_DRONE_DATA &sDroneDataLocal, VECTOR &vTargetCoord, VECTOR vTargetDirection)
	FLOAT fDefensiveRadius = eDroneData.fDefensiveRadius
	IF fDefensiveRadius > 0.0
		FLOAT fDistanceToDefensivePoint = VDIST2(vTargetCoord, sDroneDataLocal.vDefensivePoint)
		IF fDistanceToDefensivePoint > POW(fDefensiveRadius, 2.0)
			vTargetCoord = sDroneDataLocal.vDefensivePoint + (vTargetDirection*fDefensiveRadius)
		ENDIF
	ENDIF
ENDPROC

// ###### ATTACK PLAYER TASK
PROC PROCESS_FMMC_DRONE_ATTACK_PLAYERS_CLIENT(FMMC_DRONE_DATA &eDroneData, LOCAL_FMMC_DRONE_DATA &sDroneDataLocal, FMMC_DRONE_STATE &sDroneState)
		
	IF NOT GET_FMMC_DRONE_PLAYER_TARGET(sDroneDataLocal, sDroneState)
		PRINTLN("[FMMC_DRONES ", sDroneState.iIndex, "][CLIENT][Progress ", sLocalSharedAssGotoTaskData.iProgress[sDroneState.iIndex], "] - PROCESS_FMMC_DRONE_ATTACK_PLAYERS_CLIENT - No Ped Target could be found.")
		EXIT
	ENDIF
	
	sDroneDataLocal.fArrivalRadius = POW(ciFMMCDRONE_COMBAT_ARRIVAL_RADIUS, 2.0)
	
	VECTOR vPlayerCoord = GET_ENTITY_COORDS(sDroneDataLocal.pedPlayerTarget)
	VECTOR vTargetDirection = NORMALISE_VECTOR(vPlayerCoord - sDroneState.vPos)	
	
	IF NOT HAS_ENTITY_CLEAR_LOS_TO_ENTITY(sDroneDataLocal.pedPlayerTarget, sDroneState.oiIndex, SCRIPT_INCLUDE_MOVER|SCRIPT_INCLUDE_VEHICLE|SCRIPT_INCLUDE_PED|SCRIPT_INCLUDE_RAGDOLL|SCRIPT_INCLUDE_OBJECT|SCRIPT_INCLUDE_FOLIAGE)
		vTargetDirection.x = -vTargetDirection.x
		vTargetDirection.y = -vTargetDirection.y
		vTargetDirection.z = -vTargetDirection.z
		IF NOT IS_BIT_SET(sDroneDataLocal.iBS, ciFMMCDRONE_BS_Lost_LOS)
			IF NOT MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(sDroneDataLocal.iTimeStampLostLOS)
			OR MC_HAS_NET_TIMER_EXPIRED_WITH_TIME_STAMP(sDroneDataLocal.iTimeStampLostLOS, ciFMMCDRONE_COMBAT_LOS_LOST_PATHFINDING_TIMER)
				MC_REINIT_NET_TIMER_WITH_TIMESTAMP(sDroneDataLocal.iTimeStampLostLOS)
				RESET_FMMC_DRONE_PATH_AND_WAYPOINTS(sDroneDataLocal)
				SET_BIT(sDroneDataLocal.iBS, ciFMMCDRONE_BS_Lost_LOS)
			ENDIF
		ENDIF
	ELSE
		CLEAR_BIT(sDroneDataLocal.iBS, ciFMMCDRONE_BS_Lost_LOS)
	ENDIF
	
	VECTOR vTargetCoord
		
	FLOAT fMinimumCombatDistanceToTarget = GET_FMMC_DRONE_MINIMUM_DISTANCE_FROM_TARGET(eDroneData, sDroneState)
	vTargetCoord = vPlayerCoord - (vTargetDirection*fMinimumCombatDistanceToTarget)
	
	// Apply a defensive radius restriction.
	PROCESS_FMMC_DRONE_DEFENSIVE_AREA_ADJUST_TARGET_COORD(eDroneData, sDroneDataLocal, vTargetCoord, vTargetDirection)	
	
	FLOAT fGround	
	IF GET_GROUND_Z_FOR_3D_COORD(vTargetCoord, fGround)
		IF vTargetCoord.z >= (vPlayerCoord.z+2.0)
		OR (MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(sDroneDataLocal.iTimeStampCorrectHeight) AND NOT MC_HAS_NET_TIMER_EXPIRED_WITH_TIME_STAMP(sDroneDataLocal.iTimeStampCorrectHeight, 3000))
			
			vTargetCoord.z = fGround + eDroneData.fMinimumHoverHeight						
			
			IF vTargetCoord.z < vPlayerCoord.z
				vTargetCoord.z = vPlayerCoord.z + 1.0
			ENDIF
			
			IF NOT MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(sDroneDataLocal.iTimeStampCorrectHeight)
			OR MC_HAS_NET_TIMER_EXPIRED_WITH_TIME_STAMP(sDroneDataLocal.iTimeStampCorrectHeight, 3000)
				MC_REINIT_NET_TIMER_WITH_TIMESTAMP(sDroneDataLocal.iTimeStampCorrectHeight)
			ENDIF
		ENDIF
	ENDIF	
	sDroneDataLocal.fDistToGoto = VDIST2(vTargetCoord, sDroneState.vPos)
	
	SET_FMMC_DRONE_CLIENT_VECTOR_TARGET(sDroneState.iIndex, vTargetCoord, sDroneState.vPos)
	
	#IF IS_DEBUG_BUILD IF bFMMcDroneDebugPosition PRINT_ON_SCREEN_DEBUG_TEXT_WITH_FLOAT("Assigning Position, fGround: ", fGround) ENDIF  #ENDIF
	#IF IS_DEBUG_BUILD IF bFMMcDroneDebugPosition PRINT_ON_SCREEN_DEBUG_TEXT_WITH_VECTOR("Assigning Position, vTargetCoord: ", vTargetCoord) ENDIF  #ENDIF	
	#IF IS_DEBUG_BUILD IF bFMMcDroneDebugPosition DRAW_MARKER(MARKER_SPHERE, vTargetCoord, (<<0.0, 0.0, 0.0>>), (<<0.0, 0.0, 0.0>>), (<<0.3, 0.3, 0.3>>), 200, 200)	 ENDIF #ENDIF
	
	SET_FMMC_DRONE_CLIENT_VECTOR_TARGET_LOOK_AT(sDroneDataLocal, sDroneState, vPlayerCoord)
		
	// Firing Procedures
	PROCESS_FMMC_DRONE_WEAPONS(eDroneData, sDroneDataLocal, sDroneState)
	
ENDPROC

// ########## SELF DESTRUCT / CHARGE PLAYERS TASK
PROC PROCESS_FMMC_DRONE_SELF_DESTRUCT_CHARGE_PLAYERS(FMMC_DRONE_DATA &eDroneData, LOCAL_FMMC_DRONE_DATA &sDroneDataLocal, FMMC_DRONE_STATE &sDroneState)
			
	IF NOT GET_FMMC_DRONE_PLAYER_TARGET(sDroneDataLocal, sDroneState)
		PRINTLN("[FMMC_DRONES ", sDroneState.iIndex, "][CLIENT][Progress ", sLocalSharedAssGotoTaskData.iProgress[sDroneState.iIndex], "] - PROCESS_FMMC_DRONE_SELF_DESTRUCT_CHARGE_PLAYERS - No Ped Target could be found.")
		EXIT
	ENDIF
	
	VECTOR vPlayerCoord = GET_ENTITY_COORDS(sDroneDataLocal.pedPlayerTarget)	
	VECTOR vTargetCoord = vPlayerCoord
	
	sDroneDataLocal.fArrivalRadius = POW(ciFMMCDRONE_COMBAT_ARRIVAL_RADIUS, 2.0)	
	
	sDroneDataLocal.fDistToGoto = VDIST2(vTargetCoord, sDroneState.vPos)
	SET_FMMC_DRONE_CLIENT_VECTOR_TARGET(sDroneState.iIndex, vTargetCoord, sDroneState.vPos)	
		
	SET_FMMC_DRONE_CLIENT_VECTOR_TARGET_LOOK_AT(sDroneDataLocal, sDroneState, vPlayerCoord)
			
	IF NOT IS_SOUND_ID_VALID(sDroneDataLocal.iSoundID_SelfDestruction)
	OR HAS_SOUND_FINISHED(sDroneDataLocal.iSoundID_SelfDestruction)
		IF NOT IS_SOUND_ID_VALID(sDroneDataLocal.iSoundID_SelfDestruction)
		OR HAS_SOUND_FINISHED(sDroneDataLocal.iSoundID_SelfDestruction)
			sDroneDataLocal.iSoundID_SelfDestruction = GET_SOUND_ID()
		ENDIF
		PLAY_SOUND_FROM_ENTITY(sDroneDataLocal.iSoundID_SelfDestruction, "Self_Destruct", sDroneState.oiIndex, GET_FMMC_DRONE_SOUND_SET(), TRUE, 30)
		SET_VARIABLE_ON_SOUND(sDroneDataLocal.iSoundID_SelfDestruction, "Time", 1.0)
	ENDIF
	
	IF sDroneDataLocal.fDistToGoto <= sDroneDataLocal.fArrivalRadius
		IF sDroneState.bAlive
			PRINTLN("[FMMC_DRONES ", sDroneState.iIndex, "][CLIENT][Progress ", sLocalSharedAssGotoTaskData.iProgress[sDroneState.iIndex], "] - PROCESS_FMMC_DRONE_SELF_DESTRUCT_CHARGE_PLAYERS - Self destructing")
			SET_ENTITY_HEALTH(sDroneState.oiIndex, 1, sDroneState.oiIndex)
						
			IF IS_BIT_SET(eDroneData.iBS, ciFMMC_DroneBS_SelfDestructFieryExplosion)
				ADD_EXPLOSION(sDroneState.vPos, EXP_TAG_BARREL, 0.5, TRUE, FALSE, 0.5, FALSE)
			ELIF IS_BIT_SET(eDroneData.iBS, ciFMMC_DroneBS_SelfDestructEMPExplosion)
				ADD_EXPLOSION(sDroneState.vPos, EXP_TAG_RAYGUN, 1.0, TRUE, FALSE, 0.0, FALSE)
			ELSE
				ADD_EXPLOSION(sDroneState.vPos, EXP_TAG_STICKYBOMB, 0.2, FALSE, TRUE, 0.0, FALSE)
			ENDIF
			
			IF IS_SOUND_ID_VALID(sDroneDataLocal.iSoundID_SelfDestruction)
			AND NOT HAS_SOUND_FINISHED(sDroneDataLocal.iSoundID_SelfDestruction)
				STOP_SOUND(sDroneDataLocal.iSoundID_SelfDestruction)
			ENDIF
		ENDIF
	ENDIF
		
ENDPROC

PROC PROCESS_FMMC_DRONE_SYSTEMS_CLIENT(FMMC_DRONE_DATA &eDroneData, LOCAL_FMMC_DRONE_DATA &sDroneDataLocal, FMMC_DRONE_STATE &sDroneState)
	
	PROCESS_CLIENT_FMMC_DRONE_DETECT_EMP_LAUNCHER_EXPLOSIONS(sDroneState)
	
	PROCESS_FMMC_DRONE_SHOULD_MOVE_ON_TO_SPECIFIC_GOTO_PROGRESS_INDEX_CLIENT(sDroneState)
	
	PROCESS_SETTING_FMMC_DRONE_CLIENT_VECTOR_DEFENSIVE_POINT(eDroneData, sDroneDataLocal, sDroneState)
	
	SWITCH MC_ServerBD.iDrone_State[sDroneState.iIndex]
		CASE ciTASK_DRONE_CHOOSE_NEW_TASK
			PRINTLN("[FMMC_DRONES ", sDroneState.iIndex, "][CLIENT] - PROCESS_FMMC_DRONE_SYSTEMS_CLIENT - Waiting for new task.")			
			SET_FMMC_DRONE_CLIENT_VECTOR_TARGET(sDroneState.iIndex, sDroneState.vPos, sDroneState.vPos)
		BREAK
		CASE ciTASK_DRONE_GOTO_COORDS
			PROCESS_FMMC_DRONE_ASSOCIATED_GOTO_DATA_CLIENT(eDroneData, sDroneDataLocal, sDroneState)
		BREAK
		CASE ciTASK_DRONE_ATTACK_PLAYERS
			PROCESS_FMMC_DRONE_ATTACK_PLAYERS_CLIENT(eDroneData, sDroneDataLocal, sDroneState)
		BREAK
		CASE ciTASK_DRONE_SELF_DESTRUCT_CHARGE_PLAYERS
			PROCESS_FMMC_DRONE_SELF_DESTRUCT_CHARGE_PLAYERS(eDroneData, sDroneDataLocal, sDroneState)
		BREAK
	ENDSWITCH
		
	PROCESS_FMMC_DRONE_MOVEMENT(eDroneData, sDroneDataLocal, sDroneState)
	
	#IF IS_DEBUG_BUILD IF bFMMcDroneDebugCombat MAINTAIN_DRONES_DEBUG(sDroneState.oiIndex, eDroneData, sDroneDataLocal.vDefensivePoint) ENDIF #ENDIF
	
ENDPROC

// ###########################
// Main ###########################
// ###########################

PROC PROCESS_FMMC_DRONE_SYSTEMS(FMMC_DRONE_DATA &eDroneData, FMMC_DRONE_STATE &sDroneState)
	
	INT iIndex = GET_DRONE_INDEX_FROM_ENTITY_ID_AND_TYPE(sDroneState.iEntityID, sDroneState.iEntityType)	
	
	#IF IS_DEBUG_BUILD IF bFMMcDroneDebugServer IF iIndex != -1 PRINT_ON_SCREEN_DEBUG_TEXT(GET_FMMC_DRONE_STATE_NAME(MC_ServerBD.iDrone_State[iIndex])) ENDIF ENDIF #ENDIF			
	#IF IS_DEBUG_BUILD IF bFMMcDroneDebugCombat PRINT_ON_SCREEN_DEBUG_TEXT_WITH_FLOAT("Health Percent: ", (TO_FLOAT(GET_ENTITY_HEALTH(sDroneState.oiIndex)) / TO_FLOAT(GET_ENTITY_MAX_HEALTH(sDroneState.oiIndex)))) ENDIF #ENDIF
	#IF IS_DEBUG_BUILD IF bFMMcDroneDebugPosition IF iIndex != -1 PRINT_ON_SCREEN_DEBUG_TEXT_WITH_VECTOR("sDroneState.vPos: ", sDroneState.vPos) ENDIF ENDIF  #ENDIF
	#IF IS_DEBUG_BUILD IF bFMMcDroneDebugPosition IF iIndex != -1 PRINT_ON_SCREEN_DEBUG_TEXT_WITH_VECTOR("vTarget: ", sLocalFmmcDroneData[iIndex].vTarget) ENDIF ENDIF #ENDIF	
	#IF IS_DEBUG_BUILD IF bFMMcDroneDebugPosition IF iIndex != -1 DRAW_MARKER(MARKER_SPHERE, sLocalFmmcDroneData[iIndex].vTarget, (<<0.0, 0.0, 0.0>>), (<<0.0, 0.0, 0.0>>), (<<0.7, 0.7, 0.7>>)) ENDIF ENDIF #ENDIF
	
	IF bIsLocalPlayerHost
		IF iIndex = -1
			SET_DRONE_INDEX(sDroneState.iEntityID, sDroneState.iEntityType)
			EXIT
		ENDIF
		sDroneState.iIndex = iIndex
		PROCESS_FMMC_DRONE_SYSTEMS_SERVER(eDroneData, sLocalFmmcDroneData[iIndex], sDroneState)
	ENDIF
		
	IF sDroneState.bHaveControl
		IF iIndex = -1
			PRINTLN("[FMMC_DRONES ", sDroneState.iIndex, "][CLIENT][iEntityID: ", sDroneState.iEntityID, "][iEntityType ", sDroneState.iEntityType, "] - Drone Index is -1. Waiting for Server to assign.")
			EXIT
		ENDIF
		sDroneState.iIndex = iIndex
		PROCESS_FMMC_DRONE_SYSTEMS_CLIENT(eDroneData, sLocalFmmcDroneData[iIndex], sDroneState)			
	ELSE
		STOP_FMMC_DRONE_FLIGHT_LOOP_SFX(iIndex)
	ENDIF
	
ENDPROC


