// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### System: GTA V Online Mission Controller - Variations -------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Script: FM_Mission_Controller_2020.sc -----------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: All logic relating to the Alternate Variables System                                                                                                                                                                                                                                                             
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### This script was forked from FM_Mission_Controller.sc at the beginning of 2020                                                                                       
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

USING "FM_Mission_Controller_Variations_IslandHeist_2020.sch"
USING "FM_Mission_Controller_Variations_Tuner_2020.sch"
USING "FM_Mission_Controller_Variations_Fixer_2020.sch"

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: AltVars Helper Functions ------------------------------------------------------------------------------------------------------------------------
// ##### Description: This is called to decide certain variables before all the variations globals are applied to the mission globals. Done only by Server.		------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL IS_MISSION_USING_ANY_ALT_VAR()
	IF g_FMMC_STRUCT.sPoolVars.eConfig != FMMC_ALT_VAR_CONFIG_NONE
	AND NOT IS_FAKE_MULTIPLAYER_MODE_SET()
		RETURN TRUE
	ENDIF	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ALT_VAR_SERVER_WAITING_FOR_CLIENT_BD_SYNC()
	INT iPart

	PRINTLN("[LM][AltVars][SHOULD_MOVE_TO_MISSION_RUNNING_STAGE] - IS_ALT_VAR_SERVER_WAITING_FOR_CLIENT_BD_SYNC temp calling func.")
	
	PARTICIPANT_INDEX tempPart
	PLAYER_INDEX tempPlayer
	FOR iPart = 0 TO NUM_NETWORK_PLAYERS-1 
		
		tempPlayer = INT_TO_PLAYERINDEX(iPart)
		
		IF NOT IS_NET_PLAYER_OK(tempPlayer, FALSE,TRUE)
			RELOOP
		ENDIF
		
		tempPart = INT_TO_PARTICIPANTINDEX(iPart)
		
		IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
			RELOOP
		ENDIF
		
		PRINTLN("[LM][AltVars][SHOULD_MOVE_TO_MISSION_RUNNING_STAGE] - IS_ALT_VAR_SERVER_WAITING_FOR_CLIENT_BD_SYNC Checking iPart: ", iPart)
		
		IF NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitset4, PBBOOL4_CLIENT_ASSIGNED_LATE_CORONA_DATA)
			PRINTLN("[LM][AltVars][SHOULD_MOVE_TO_MISSION_RUNNING_STAGE] - IS_ALT_VAR_SERVER_WAITING_FOR_CLIENT_BD_SYNC Waiting for iPart: ", iPart, " to set PBBOOL4_CLIENT_ASSIGNED_LATE_CORONA_DATA")
			RETURN TRUE
		ELSE
			PRINTLN("[LM][AltVars][SHOULD_MOVE_TO_MISSION_RUNNING_STAGE] - IS_ALT_VAR_SERVER_WAITING_FOR_CLIENT_BD_SYNC iPart: ", iPart, " is ready to go.")
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_ALT_VAR_GANG_BOSS_BEEN_SET_YET()	
	
	IF NOT HAS_NET_TIMER_STARTED(tdAltVars_SafetyTimerGang)
		START_NET_TIMER(tdAltVars_SafetyTimerGang)
	ELIF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdAltVars_SafetyTimerGang, ciALT_VARS_SAFETY_TIMER_GANG)
		PRINTLN("[LM][AltVars][SHOULD_MOVE_TO_MISSION_RUNNING_STAGE] - HAS_ALT_VAR_SERVER_DATA_BEEN_SET_YET tdAltVars_SafetyTimerGang has kicked in (4 seconds). Returning True.")
		RETURN TRUE
	ENDIF
		
	IF GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT() != -1		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_ALT_VAR_SERVER_DATA_BEEN_SET_YET_SERVER()	
	
	IF NOT HAS_NET_TIMER_STARTED(tdAltVars_SafetyTimer)
		START_NET_TIMER(tdAltVars_SafetyTimer)
	ELIF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdAltVars_SafetyTimer, ciALT_VARS_SAFETY_TIMER)
		PRINTLN("[LM][AltVars][SHOULD_MOVE_TO_MISSION_RUNNING_STAGE] - HAS_ALT_VAR_SERVER_DATA_BEEN_SET_YET tdAltVars_SafetyTimer has kicked in (8 seconds). Returning True.")
		RETURN TRUE
	ENDIF
	
	INT i	
	FOR i = 0 TO ciFMMC_ALT_VAR_MAX_BS-1
		IF MC_ServerBD_4.iAltVarsBS[i] != 0
			RETURN TRUE
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_ALT_VAR_SERVER_DATA_BEEN_SET_YET()	
	
	IF NOT HAS_NET_TIMER_STARTED(tdAltVars_SafetyTimerClient)
		START_NET_TIMER(tdAltVars_SafetyTimerClient)
	ELIF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdAltVars_SafetyTimerClient, ciALT_VARS_SAFETY_TIMER_CLIENT)
		PRINTLN("[LM][AltVars][SHOULD_MOVE_TO_MISSION_RUNNING_STAGE] - HAS_ALT_VAR_SERVER_DATA_BEEN_SET_YET tdAltVars_SafetyTimer has kicked in (20 seconds). Returning True.")
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(MC_serverBD.iServerBitset8, SBBOOL8_HOST_MISSION_VARIATION_DATA_APPLIED)
	AND IS_BIT_SET(MC_serverBD_4.rsgSpawnSeed.iMiscBS, ciRandomSpawnGroupBS_HasBeenSet)	
		INT i	
		FOR i = 0 TO ciFMMC_ALT_VAR_MAX_BS-1
			IF MC_ServerBD_4.iAltVarsBS[i] != 0
				RETURN TRUE
			ENDIF
		ENDFOR
	ELSE	
		PRINTLN("[LM][AltVars][SHOULD_MOVE_TO_MISSION_RUNNING_STAGE] - SBBOOL8_HOST_MISSION_VARIATION_DATA_APPLIED is not yet set.")		
	ENDIF
	
	RETURN FALSE
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: AltVars Start up Process Server ------------------------------------------------------------------------------------------------------------------------
// ##### Description: This is called to decide certain variables before all the variations globals are applied to the mission globals. Done only by Server.		------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA()	
	PRINTLN("[SCRIPT INIT][AltVarsSystem] - (Server-Define) (Server) Starting Spew ----------------------------------------------------------------------------------------------------------------------------------")
		
	#IF IS_DEBUG_BUILD	
	IF bXMLOverride_AltVars
		PRINTLN("[SCRIPT INIT][AltVarsSystem] - (Server-Define) (Server) Early Exit - Loaded from XML config")
		PRINTLN("[SCRIPT INIT][AltVarsSystem] - (Server-Define) (Server) Finished Spew ----------------------------------------------------------------------------------------------------------------------------------")
		EXIT
	ENDIF	
	#ENDIF	
		
	SWITCH g_FMMC_STRUCT.sPoolVars.eConfig
		CASE FMMC_ALT_VAR_CONFIG_ISLAND_HEIST
			PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_ISLAND_HEIST()
		BREAK
		CASE FMMC_ALT_VAR_CONFIG_TUNER
			PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_TUNER()
		BREAK
		CASE FMMC_ALT_VAR_CONFIG_FIXER
			PROCESS_DEFINE_ALT_VAR_SETTINGS_SERVER_DATA_FIXER()
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
		DUMP_XML_OVERRIDES(XMLDUMP_ALTVARS)
	#ENDIF
	
	PRINTLN("[SCRIPT INIT][AltVarsSystem] - (Server-Define) (Server) Finished Spew ----------------------------------------------------------------------------------------------------------------------------------")
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: AltVars Start up Process -------------------------------------------------------------------------------------------------------------------------------
// ##### Description: This is called to apply all the variations to the mission globals. Done by all clients.											 		------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_APPLY_ALT_VAR_PRE_SPAWN_GROUP_SETTINGS()
	PRINTLN("[SCRIPT INIT][AltVarsSystem] - (Application) (Pre Spawn Groups) Starting Spew ----------------------------------------------------------------------------------------------------------------------------------")
	
	PRINTLN("[SCRIPT INIT][AltVarsSystem] - (Application) (Pre Spawn Groups) - SBBOOL8_HOST_MISSION_VARIATION_DATA_PRE_APPLIED: ", IS_BIT_SET(MC_serverBD.iServerBitSet8, SBBOOL8_HOST_MISSION_VARIATION_DATA_PRE_APPLIED))	
	
	#IF IS_DEBUG_BUILD
	INT i	
	FOR i = 0 TO ciFMMC_ALT_VAR_MAX_BS-1
		IF MC_ServerBD_4.iAltVarsBS[i] != 0
			PRINTLN("[SCRIPT INIT][AltVarsSystem] - (Application) (Pre Spawn Groups) - iAltVarsBS[",i,"]: ", MC_ServerBD_4.iAltVarsBS[i])
		ENDIF
	ENDFOR
	#ENDIF
	
	SWITCH g_FMMC_STRUCT.sPoolVars.eConfig
		CASE FMMC_ALT_VAR_CONFIG_ISLAND_HEIST
			PROCESS_APPLY_ALT_VAR_PRE_SPAWN_GROUP_SETTINGS_ISLAND_HEIST()
		BREAK
		CASE FMMC_ALT_VAR_CONFIG_TUNER
			PROCESS_APPLY_ALT_VAR_PRE_SPAWN_GROUP_SETTINGS_TUNER()
		BREAK
		CASE FMMC_ALT_VAR_CONFIG_FIXER
			PROCESS_APPLY_ALT_VAR_PRE_SPAWN_GROUP_SETTINGS_FIXER()
		BREAK
	ENDSWITCH
	
	PRINTLN("[SCRIPT INIT][AltVarsSystem] - (Application) (Pre Spawn Groups) Finished Spew ----------------------------------------------------------------------------------------------------------------------------------")
ENDPROC

PROC PROCESS_APPLY_ALT_VAR_SETTINGS()
	PRINTLN("[SCRIPT INIT][AltVarsSystem] - (Application) Starting Spew ----------------------------------------------------------------------------------------------------------------------------------")
	
	PRINTLN("[SCRIPT INIT][AltVarsSystem] - (Application) - SBBOOL8_HOST_MISSION_VARIATION_DATA_APPLIED: ", IS_BIT_SET(MC_serverBD.iServerBitSet8, SBBOOL8_HOST_MISSION_VARIATION_DATA_APPLIED))	
	
	#IF IS_DEBUG_BUILD
	INT i	
	FOR i = 0 TO ciFMMC_ALT_VAR_MAX_BS-1
		IF MC_ServerBD_4.iAltVarsBS[i] != 0
			PRINTLN("[SCRIPT INIT][AltVarsSystem] - (Application) - iAltVarsBS[",i,"]: ", MC_ServerBD_4.iAltVarsBS[i])
		ENDIF
	ENDFOR
	#ENDIF
	
	SWITCH g_FMMC_STRUCT.sPoolVars.eConfig
		CASE FMMC_ALT_VAR_CONFIG_ISLAND_HEIST
			PROCESS_APPLY_ALT_VAR_SETTINGS_ISLAND_HEIST()
		BREAK
		CASE FMMC_ALT_VAR_CONFIG_TUNER
			PROCESS_APPLY_ALT_VAR_SETTINGS_TUNER()
		BREAK
		CASE FMMC_ALT_VAR_CONFIG_FIXER
			PROCESS_APPLY_ALT_VAR_SETTINGS_FIXER()
		BREAK
	ENDSWITCH
	
	SET_BIT(iLocalBoolCheck34, LBOOL34_ALT_VARS_APPLIED)
	
	PRINTLN("[SCRIPT INIT][AltVarsSystem] - (Application) Finished Spew ----------------------------------------------------------------------------------------------------------------------------------")
ENDPROC
