// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### System: GTA V Online Mission Controller - Props -------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Script: FM_Mission_Controller_2020.sc -----------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Handles prop and dynoprop behaviour.                                                                                                                     
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### This script was forked from FM_Mission_Controller.sc at the beginning of 2020		 																				  
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

USING "FM_Mission_Controller_EntityObjectives_2020.sch"

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Alarms ----------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Functions related to alarm props.  -------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
FUNC BOOL SHOULD_ALARM_PLAY_FOR_TEAM(INT iPropIndex, INT iTeam)
	IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropIndex].iAlarmTeam != -1
		IF iTeam = g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropIndex].iAlarmTeam
			RETURN TRUE
		ELSE
			RETURN FALSE
		ENDIF
	ENDIF
	RETURN TRUE
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Blimps ----------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Processing for the animated blimps that have text on them that can be set in the Creator.  -----------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
PROC PROCESS_CLIENT_BLIMP_SIGNS()
	DO_BLIMP_SIGNS(sBlimpSign, g_bMissionOver)
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Shared Prop Processing ------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Functions that can be used in both the staggered and every-frame prop loops.  ------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
PROC CLEAN_UP_MC_PROP(INT iProp)
	IF isoundid[iProp] <> -1
		PRINTLN("[RCC MISSION] [ALARM] Stopping alarm as prop is getting cleaned up ", iProp)
        STOP_SOUND(isoundid[iProp])
    ENDIF
	IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].mn = PROP_FLARE_01
		CLEAN_UP_FLARE_SFX_FOR_THIS_PROP(iProp)
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropBitset, ciFMMC_PROP_IgnoreVisCheckCleanup)
		DELETE_OBJECT(oiProps[iProp])
		PRINTLN("[PRP_BUG] [RCC MISSION] Prop ", iProp, " getting cleaned up early - delete, objective: ", g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iCleanupObjective," team: ",g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iCleanupTeam)
	ELSE
		SET_OBJECT_AS_NO_LONGER_NEEDED(oiProps[iProp])
		PRINTLN("[PRP_BUG] [RCC MISSION] Prop ", iProp, " getting cleaned up early - no longer needed, objective: ", g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iCleanupObjective," team: ",g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iCleanupTeam)
	ENDIF
ENDPROC

PROC CACHE_PROP_SHOULD_CLEANUP_THIS_FRAME(INT i)
	FMMC_SET_LONG_BIT(iPropShouldCleanupThisFrameBS, i)
ENDPROC

PROC CACHE_PROP_SHOULD_NOT_CLEANUP_THIS_FRAME(INT i)
	FMMC_SET_LONG_BIT(iPropShouldNotCleanupThisFrameBS, i)
ENDPROC

FUNC BOOL SHOULD_CLEANUP_PROP(INT iProp, OBJECT_INDEX  tempProp)
	
	IF FMMC_IS_LONG_BIT_SET(iPropShouldCleanupThisFrameBS, iProp)
		RETURN TRUE
	ENDIF
	IF FMMC_IS_LONG_BIT_SET(iPropShouldNotCleanupThisFrameBS, iProp)
		RETURN FALSE
	ENDIF
	
	IF GET_MC_SERVER_GAME_STATE() = GAME_STATE_END
	AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropBitset, ciFMMC_PROP_CleanupAtMissionEnd)
		CACHE_PROP_SHOULD_CLEANUP_THIS_FRAME(iProp)
		RETURN TRUE
	ENDIF
	
	// Round Restarts. If we return to an earlier rule where the prop wouldn't have yet spawned, we need this to clean it up...
	IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iAssociatedTeam > -1
		PRINTLN("[RCC MISSION][SHOULD_CLEANUP_PROP] - iProp: ", iProp, " Has an associated Rule/Team.")
		IF NOT HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(CREATION_TYPE_PROPS, iProp, g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iAssociatedSpawn, g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iAssociatedObjective, DEFAULT, DEFAULT, DEFAULT, g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iAggroIndexBS_Entity_Prop)
			PRINTLN("[RCC MISSION][SHOULD_CLEANUP_PROP] - iProp: ", iProp, " Has not reached those spawn conditions.")
			IF DOES_ENTITY_EXIST(tempProp)
				PRINTLN("[RCC MISSION][SHOULD_CLEANUP_PROP] - iProp: ", iProp, " Still exists.")
				IF ((g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iCleanupObjective != -1) AND (g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iCleanupTeam != -1))					
					PRINTLN("[RCC MISSION][SHOULD_CLEANUP_PROP] - Prop exists, but is not meant to be spawned yet. Cleaning up iProp: ", iProp)
					CLEAN_UP_MC_PROP(iProp)
					CACHE_PROP_SHOULD_CLEANUP_THIS_FRAME(iProp)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	IF FMMC_IS_LONG_BIT_SET(iPropCleanedupBS, iProp)
		//Prop is already cleaned up
		CACHE_PROP_SHOULD_CLEANUP_THIS_FRAME(iProp)
		RETURN TRUE
	ENDIF
	
	BOOL bCheckEarlyCleanup
	
	IF NOT MC_DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP(iProp, eSGET_Prop)
		PRINTLN("[RCC MISSION][SHOULD_CLEANUP_PROP] - iProp: ", iProp, ", Prop no longer has a valid spawn group active.") 
		bCheckEarlyCleanup = TRUE
	ENDIF	
			
	IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iCleanupObjective != -1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iCleanupTeam != -1
			IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iCleanupObjective <= MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iCleanupTeam]
				bCheckEarlyCleanup = TRUE				
			ENDIF
		ENDIF
	ENDIF
	
	BOOL bAtMidpoint
	IF bCheckEarlyCleanup
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropBitset, ciFMMC_PROP_CleanupAtMidpoint)
			IF IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iCleanupTeam], g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iCleanupObjective)
				IF(tempProp = NULL)
					CACHE_PROP_SHOULD_CLEANUP_THIS_FRAME(iProp)
					RETURN TRUE
				ENDIF
				
				IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iCleanupRange <= 0
					bAtMidpoint = TRUE
				ELSE
					INT iClosestPlayer = GET_NEAREST_PLAYER_TO_ENTITY(tempProp)
					PLAYER_INDEX ClosestPlayer = INT_TO_PLAYERINDEX(iClosestPlayer)
					PED_INDEX playerPed = GET_PLAYER_PED(ClosestPlayer)
					IF tempProp != NULL
						IF GET_DISTANCE_BETWEEN_ENTITIES(playerPed,tempProp) > g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iCleanupRange
							bAtMidpoint = TRUE
						ENDIF
					ELSE
						IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(playerPed,g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].vPos) > g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iCleanupRange
							bAtMidpoint = TRUE
						ENDIF
					ENDIF
					
				ENDIF
			ENDIF
		ELSE
			IF(tempProp = NULL)
				CACHE_PROP_SHOULD_CLEANUP_THIS_FRAME(iProp)
				RETURN TRUE
			ENDIF
		
			//Midpoint doesn't matter
			IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iCleanupRange <= 0
				bAtMidpoint = TRUE
			ELSE
				INT iClosestPlayer = GET_NEAREST_PLAYER_TO_ENTITY(tempProp)
				PLAYER_INDEX ClosestPlayer = INT_TO_PLAYERINDEX(iClosestPlayer)
				PED_INDEX playerPed = GET_PLAYER_PED(ClosestPlayer)
				IF tempProp != NULL
					IF GET_DISTANCE_BETWEEN_ENTITIES(playerPed,tempProp) > g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iCleanupRange
						bAtMidpoint = TRUE
					ENDIF
				ELSE
					IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(playerPed,g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].vPos) > g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iCleanupRange
						bAtMidpoint = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
			
		IF bAtMidpoint
			CLEAN_UP_MC_PROP(iProp)
			CACHE_PROP_SHOULD_CLEANUP_THIS_FRAME(iProp)
			RETURN TRUE
		ENDIF
	ENDIF
	
	CACHE_PROP_SHOULD_NOT_CLEANUP_THIS_FRAME(iProp)
	
	RETURN FALSE

ENDFUNC

PROC CACHE_PROP_RESPAWN_BLOCKED(INT i)
	IF NOT IS_BIT_SET(MC_serverBD_4.rsgSpawnSeed.iMiscBS, ciRandomSpawnGroupBS_HasBeenSet)
		EXIT
	ENDIF	
	FMMC_SET_LONG_BIT(iPropRespawnIsBlockedBS, i)
	PRINTLN("[MCSpawning][Props][Prop ", i, "] SHOULD_PROP_RESPAWN_NOW - CACHE_OBJ_RESPAWN_BLOCKED - Caching that the Prop has been blocked from respawning.")
ENDPROC

PROC CACHE_PROP_SHOULD_NOT_RESPAWN_THIS_FRAME(INT i)
	FMMC_SET_LONG_BIT(iPropRespawnIsBlockedThisFrameBS, i)
ENDPROC

PROC CACHE_PROP_SHOULD_RESPAWN_THIS_FRAME(INT i)
	FMMC_SET_LONG_BIT(iPropShouldRespawnNowBS, i)
ENDPROC

FUNC BOOL SHOULD_PROP_RESPAWN_NOW(INT iProp)
	
	IF FMMC_IS_LONG_BIT_SET(iPropRespawnIsBlockedBS, iProp)
		RETURN FALSE
	ENDIF	
	IF FMMC_IS_LONG_BIT_SET(iPropRespawnIsBlockedThisFrameBS, iProp)
		RETURN FALSE
	ENDIF	
	IF FMMC_IS_LONG_BIT_SET(iPropShouldRespawnNowBS, iProp)
		RETURN TRUE
	ENDIF	
	
	IF SHOULD_CLEANUP_PROP(iProp, NULL)
		RETURN FALSE
	ENDIF
	
	IF MC_SHOULD_BLOCK_SPAWN_BASED_ON_SPAWN_CONDITION_FLAGS(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].eSpawnConditionFlag, FALSE, FALSE, g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iAggroIndexBS_Entity_Prop, MC_PlayerBD[iPartToUse].iTeam)
		PRINTLN("[MCSpawning][Props][Prop ", iProp, "] SHOULD_PROP_RESPAWN_NOW - Returning FALSE due to MC_SHOULD_BLOCK_SPAWN_BASED_ON_SPAWN_CONDITION_FLAGS")		
		IF MC_ARE_SPAWN_CONDITION_FLAGS_MET_DURING_GAMEPLAY(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].eSpawnConditionFlag)			
			CACHE_PROP_SHOULD_NOT_RESPAWN_THIS_FRAME(iProp)
		ELSE
			CACHE_PROP_RESPAWN_BLOCKED(iProp)
		ENDIF
		RETURN FALSE
	ENDIF
	
	IF FMMC_IS_LONG_BIT_SET(iPropCleanedupBS, iProp)
		PRINTLN("[MCSpawning][Props][Prop ", iProp, "] SHOULD_PROP_RESPAWN_NOW - should not respawn now as iPropCleanedupBS")
		CACHE_PROP_RESPAWN_BLOCKED(iProp)
		RETURN FALSE
	ENDIF
		
	IF NOT MC_DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP(iProp, eSGET_Prop)
		PRINTLN("[MCSpawning][Props][Prop ", iProp, "] SHOULD_PROP_RESPAWN_NOW - RETURNING FALSE - MC_DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP = FALSE")
		CACHE_PROP_RESPAWN_BLOCKED(iProp)
		RETURN FALSE
	ENDIF	
	
	IF FMMC_IS_LONG_BIT_SET(iPropRespawnNowBS, iProp)
		PRINTLN("[MCSpawning][Props][Prop ", iProp, "] SHOULD_PROP_RESPAWN_NOW - should respawn now")
		CACHE_PROP_SHOULD_RESPAWN_THIS_FRAME(iProp)
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropBitset, ciFMMC_PROP_AssociatedSpawn)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropSpawnNumber > -1
			IF IS_BIT_SET(MC_ServerBD.iSpawnPointsUsed[g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropTeamNumber], g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropSpawnNumber)
			OR IS_BIT_SET(LocalRandomSpawnBitset[g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropTeamNumber], g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropSpawnNumber)
				CACHE_PROP_SHOULD_RESPAWN_THIS_FRAME(iProp)
				RETURN TRUE
			ELSE
				CACHE_PROP_SHOULD_NOT_RESPAWN_THIS_FRAME(iProp)
				PRINTLN("[MCSpawning][Props][Prop ", iProp, "] SHOULD_PROP_RESPAWN_NOW - prop spawn number")
				RETURN FALSE
			ENDIF
		ELSE
			IF MC_ServerBD.iNumberOfTeams > g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropTeamNumber
				CACHE_PROP_SHOULD_RESPAWN_THIS_FRAME(iProp)
				RETURN TRUE
			ELSE
				CACHE_PROP_SHOULD_NOT_RESPAWN_THIS_FRAME(iProp)
				PRINTLN("[MCSpawning][Props][Prop ", iProp, "] SHOULD_PROP_RESPAWN_NOW - prop spawn team number")
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iAssociatedTeam > -1
		IF HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(CREATION_TYPE_PROPS, iProp, g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iAssociatedSpawn, g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iAssociatedObjective, DEFAULT, DEFAULT, DEFAULT, g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iAggroIndexBS_Entity_Prop)
			CACHE_PROP_SHOULD_RESPAWN_THIS_FRAME(iProp)
			RETURN TRUE
		ENDIF
	ELSE
		CACHE_PROP_SHOULD_RESPAWN_THIS_FRAME(iProp)
		RETURN TRUE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iSecondAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iSecondAssociatedTeam > -1
		IF HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(CREATION_TYPE_PROPS, iProp, g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iSecondAssociatedSpawn, g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iSecondAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iSecondAssociatedObjective, DEFAULT, DEFAULT, DEFAULT, g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iAggroIndexBS_Entity_Prop)
			CACHE_PROP_SHOULD_RESPAWN_THIS_FRAME(iProp)
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iThirdAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iThirdAssociatedTeam > -1
		IF HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(CREATION_TYPE_PROPS, iProp, g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iThirdAssociatedSpawn, g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iThirdAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iThirdAssociatedObjective, DEFAULT, DEFAULT, DEFAULT, g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iAggroIndexBS_Entity_Prop)
			CACHE_PROP_SHOULD_RESPAWN_THIS_FRAME(iProp)
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iFourthAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iFourthAssociatedTeam > -1
		IF HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(CREATION_TYPE_PROPS, iProp, g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iFourthAssociatedSpawn, g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iFourthAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iFourthAssociatedObjective, DEFAULT, DEFAULT, DEFAULT, g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iAggroIndexBS_Entity_Prop)
			CACHE_PROP_SHOULD_RESPAWN_THIS_FRAME(iProp)
			RETURN TRUE
		ENDIF
	ENDIF
	
	CACHE_PROP_SHOULD_NOT_RESPAWN_THIS_FRAME(iProp)
	
	RETURN FALSE
	
ENDFUNC

PROC PROCESS_PARTICLE_FX_OVERRIDE_DYNOPROP(FMMC_DYNOPROP_STATE& sDynopropState)
	IF IS_BIT_SET(iLocalBoolCheck21, LBOOL21_PARTICLE_FX_OVERRIDE_BOX_PILE)
		// Override already set
		EXIT
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[sDynopropState.iIndex].mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_prop_sr_boxpile_03"))
		REQUEST_NAMED_PTFX_ASSET("scr_sr_adversary")
		IF NOT HAS_NAMED_PTFX_ASSET_LOADED("scr_sr_adversary")			
			PRINTLN("[PROCESS_PARTICLE_FX_OVERRIDE_DYNOPROP] Not loaded scr_sr_adversary effect asset yet")
		ELSE
			PRINTLN("[PROCESS_PARTICLE_FX_OVERRIDE_DYNOPROP] Loaded scr_sr_adversary effect asset")
			PRINTLN("[PROCESS_PARTICLE_FX_OVERRIDE_DYNOPROP] SET_PARTICLE_FX_OVERRIDE \"ent_dst_gen_cardboard\" with \"scr_sr_dst_cardboard\"")
			SET_PARTICLE_FX_OVERRIDE("ent_dst_gen_cardboard", "scr_sr_dst_cardboard")
			SET_BIT(iLocalBoolCheck21, LBOOL21_PARTICLE_FX_OVERRIDE_BOX_PILE)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL HAS_ANY_TEAM_HIT_PROP_ACTION_ACTIVATION(INT iProp)
	
	BOOL bHit = FALSE
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iAssociatedTeam != -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iAssociatedObjective != -1
		IF HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(CREATION_TYPE_PROPS, iProp, g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iAssociatedActionStart, g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iAssociatedObjective, TRUE, g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iAssociatedScoreRequired, DEFAULT, g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iAggroIndexBS_Entity_Prop)
			bHit = TRUE
		ENDIF
	ELSE
		IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropBitset, ciFMMC_PROP_AssociatedSpawn)
			bHit = TRUE
		ENDIF
	ENDIF
	
	IF NOT bHit // If we already know we've hit it, we don't need to keep checking
	AND g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iSecondAssociatedTeam != -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iSecondAssociatedObjective != -1
		IF HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(CREATION_TYPE_PROPS, iProp, g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iSecondActionStart, g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iSecondAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iSecondAssociatedObjective, TRUE, g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iSecondAssociatedScoreRequired, DEFAULT, g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iAggroIndexBS_Entity_Prop)
			bHit = TRUE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iThirdAssociatedTeam != -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iThirdAssociatedObjective != -1
		IF HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(CREATION_TYPE_PROPS, iProp, g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iThirdActionStart, g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iThirdAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iThirdAssociatedObjective, TRUE, g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iThirdAssociatedScoreRequired, DEFAULT, g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iAggroIndexBS_Entity_Prop)
			bHit = TRUE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iFourthAssociatedTeam != -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iFourthAssociatedObjective != -1
		IF HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(CREATION_TYPE_PROPS, iProp, g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iFourthActionStart, g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iFourthAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iFourthAssociatedObjective, TRUE, g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iFourthAssociatedScoreRequired, DEFAULT, g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iAggroIndexBS_Entity_Prop)
			bHit = TRUE
		ENDIF
	ENDIF
	
	RETURN bHit

ENDFUNC

FUNC BOOL DOES_PROP_HAVE_AN_ASSOCIATED_RULE(INT iProp)
	RETURN g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iAssociatedObjective != -1
		OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropBitset, ciFMMC_PROP_AssociatedSpawn)
		OR FMMC_IS_LONG_BIT_SET(iPropRespawnNowBS, iProp)
ENDFUNC

PROC RESPAWN_MISSION_PROP(INT iProp)

	IF HAVE_PROP_ASSETS_LOADED(iProp)
		isoundid[iProp] = -1

		//visibility check? -IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropBitset,ciFMMC_PROP_IgnoreVisCheckCleanup)
		IF DOES_PROP_HAVE_A_SUB_COMPONENT(iProp)
			oiProps[iProp] = CREATE_PROP_WITH_SUB_COMPONENT(iProp, oiPropsChildren, FALSE)
		ELSE
		
			oiProps[iProp] = CREATE_OBJECT(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].mn, 
													g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].vPos, 
													FALSE, 			// Not a network object?	
													FALSE,			// This script is not the host object
													IS_MODEL_A_DOOR(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].mn)) //Don't force this to be an object unless its a door to stop asserts
		ENDIF
										
		FMMC_CLEAR_LONG_BIT(iPropCleanedupBS, iProp)
		FMMC_CLEAR_LONG_BIT(iPropRespawnNowBS, iProp)
		
		// Sets up the correct rotation/position for the prop passed, but will ALWAYS make the alarm invisible!
		MC_SET_UP_PROP(oiProps[iProp], iProp, DEFAULT)
		
		PRINTLN("[MCSpawning][Props][Prop ", iProp, "] RESPAWN_MISSION_PROP - ")
		
		IF(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].mn = PROP_FLARE_01)
			CPRINTLN(DEBUG_CONTROLLER, "[MCSpawning][Props][Prop ", iProp, "] RESPAWN_MISSION_PROP - [PTFX] Requesting flare assets - BACKUP")
			REQUEST_NAMED_PTFX_ASSET("scr_biolab_heist")
		ENDIF
		
		IF GET_PROP_LOD_OVERRIDE_DISTANCE(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].mn) != -1
			SET_ENTITY_LOD_DIST(oiProps[iProp],GET_PROP_LOD_OVERRIDE_DISTANCE(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].mn))
			PRINTLN("[MCSpawning][Props][Prop ", iProp, "] RESPAWN_MISSION_PROP - SETTING PROP LOD DISTANCE: ", GET_PROP_LOD_OVERRIDE_DISTANCE(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].mn))
		ENDIF
		
		SET_MODEL_AS_NO_LONGER_NEEDED(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].mn)
		
		FMMC_CLEAR_LONG_BIT(iPropRespawnIsBlockedBS, iProp)		
		FMMC_CLEAR_LONG_BIT(iPropRespawnIsBlockedThisFrameBS, iProp)
		FMMC_CLEAR_LONG_BIT(iPropShouldRespawnNowBS, iProp)
		
	ENDIF
	
ENDPROC

PROC PROCESS_PROP_RESPAWNING(INT iProp)

	IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_FIRST_UPDATE_DONE)
		EXIT
	ENDIF
	
	IF DOES_ENTITY_EXIST(oiProps[iProp])
		EXIT
	ENDIF
	
	IF NOT DOES_PROP_HAVE_AN_ASSOCIATED_RULE(iProp)
		EXIT
	ENDIF
	
	IF NOT SHOULD_PROP_RESPAWN_NOW(iProp)
		EXIT
	ENDIF
	
	RESPAWN_MISSION_PROP(iProp)
ENDPROC

FUNC INT GET_TEAM_ASSOCIATED_OBJECTIVE(INT iProp, INT iTeam)
	IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iAssociatedTeam = iTeam
		RETURN g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iAssociatedObjective
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iSecondAssociatedTeam = iTeam
		RETURN g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iSecondAssociatedObjective
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iThirdAssociatedTeam = iTeam
		RETURN g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iThirdAssociatedObjective
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iFourthAssociatedTeam = iTeam
		RETURN g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iFourthAssociatedObjective
	ENDIF
	
	RETURN -1
ENDFUNC

FUNC BOOL IS_FLASHING_STATE_TOGGLE(INT iMiliseconds, INT iIncrementer, INT iTime, SCRIPT_TIMER timerToUse)
	RETURN GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(timerToUse) > (iTime*1000 + (iMiliseconds*(iIncrementer+1)))
ENDFUNC

PROC VV_FORCE_CLEANUP_PROCESS_ALPHA_DESPAWN_PROP(INT iProp, BOOL bFlash, INT iMiliseconds)
	INT iPropActual = iPropFadeoutEveryFrameIndex[iProp]

	IF DOES_ENTITY_EXIST(oiProps[iPropActual])
		SCRIPT_TIMER timerToUse			
		IF IS_LOCAL_PLAYER_MULTIRULE_TIMER_RUNNING()
			timerToUse = MC_serverBD_3.tdMultiObjectiveLimitTimer[MC_playerBD[iPartToUse].iteam]
		ELIF IS_LOCAL_PLAYER_OBJECTIVE_TIMER_RUNNING()
			timerToUse = MC_serverBD_3.tdObjectiveLimitTimer[MC_playerBD[iPartToUse].iteam]
		ENDIF
		
		PRINTLN("[LM][VV_FORCE_CLEANUP_PROCESS_ALPHA_DESPAWN_PROP] - Flashes counter: ", iFlashToggle[iProp])
		IF iFlashToggle[iProp] < 7
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropActual].iPropBitSet2, ciFMMC_PROP2_CleanUpVFX)
				IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfx_PropFadeoutEveryFrameIndex[iProp])
					STOP_PARTICLE_FX_LOOPED(ptfx_PropFadeoutEveryFrameIndex[iProp], TRUE)
				ENDIF
				MODEL_NAMES mn = GET_ENTITY_MODEL(oiProps[iPropActual])
				IF IS_MODEL_VALID(mn)
				AND NOT DOES_PARTICLE_FX_LOOPED_EXIST(ptfx_PropFadeoutEveryFrameIndex[iProp])				
					VECTOR vDimensionsMin
					VECTOR vDimensionsMax
					FLOAT fHeight
					
					GET_MODEL_DIMENSIONS(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropActual].mn, vDimensionsMin, vDimensionsMax)
					
					fHeight = vDimensionsMax.z
					
					VECTOR vSpawn
					
					vSpawn = GET_ENTITY_COORDS(oiProps[iPropActual])
					vSpawn.z += fHeight
					
					IF HAS_NAMED_PTFX_ASSET_LOADED("scr_as_trap")
						USE_PARTICLE_FX_ASSET("scr_as_trap")
						ptfx_PropFadeoutEveryFrameIndex[iProp] = START_PARTICLE_FX_LOOPED_AT_COORD(GET_PTFX_DISSOLVE_NAME_BASED_ON_PROP(mn), vSpawn, GET_ENTITY_ROTATION(oiProps[iPropActual]), 1.0, DEFAULT, DEFAULT, DEFAULT, TRUE)					
					ELSE
						REQUEST_NAMED_PTFX_ASSET("scr_as_trap")
					ENDIF 
					
					PRINTLN("[LM][VV_FORCE_CLEANUP_PROCESS_ALPHA_DESPAWN_PROP] - Creating PTFX for iPropEF: ", iProp)
					SET_BIT(iCleanUpPropEF_BS[iProp], ci_CleanUpPropEF_BS_ColourNotSet)
				ENDIF
				IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfx_PropFadeoutEveryFrameIndex[iProp])
				AND IS_BIT_SET(iCleanUpPropEF_BS[iProp], ci_CleanUpPropEF_BS_ColourNotSet)
					SET_PARTICLE_FX_LOOPED_COLOUR(ptfx_PropFadeoutEveryFrameIndex[iProp], 0.8, 0.2, 0.2, TRUE)
					SET_PARTICLE_FX_LOOPED_EVOLUTION(ptfx_PropFadeoutEveryFrameIndex[iProp], "ScaleX", GET_PTFX_DISSOLVE_LENGTH_BASED_ON_PROP(mn), TRUE)
					SET_PARTICLE_FX_LOOPED_EVOLUTION(ptfx_PropFadeoutEveryFrameIndex[iProp], "ScaleY", GET_PTFX_DISSOLVE_WIDTH_BASED_ON_PROP(mn), TRUE)
					SET_PARTICLE_FX_LOOPED_EVOLUTION(ptfx_PropFadeoutEveryFrameIndex[iProp], "Intensity", 0.5, TRUE)				
					CLEAR_BIT(iCleanUpPropEF_BS[iProp], ci_CleanUpPropEF_BS_ColourNotSet)
				ENDIF
			ENDIF
			
			#IF IS_DEBUG_BUILD
			PRINTLN("[LM][VV_FORCE_CLEANUP_PROCESS_ALPHA_DESPAWN_PROP] - Flashing not finished for iProp: ", iProp)
			PRINTLN("[LM][VV_FORCE_CLEANUP_PROCESS_ALPHA_DESPAWN_PROP] - Flashing not finished for iPropActual: ", iPropActual)
			PRINTLN("[LM][VV_FORCE_CLEANUP_PROCESS_ALPHA_DESPAWN_PROP] - Flashing Toggle Time: ", iFlashToggleTime[iProp])
			PRINTLN("[LM][VV_FORCE_CLEANUP_PROCESS_ALPHA_DESPAWN_PROP] - prop time: ", g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropActual].iForceCleanupPropAfterTime*1000)
			PRINTLN("[LM][VV_FORCE_CLEANUP_PROCESS_ALPHA_DESPAWN_PROP] - prop time + calc: ", g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropActual].iForceCleanupPropAfterTime*1000 + (iMiliseconds*iFlashToggle[iProp]))			
			PRINTLN("[LM][VV_FORCE_CLEANUP_PROCESS_ALPHA_DESPAWN_PROP] - Timer to use: ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(timerToUse))
			PRINTLN("[LM][VV_FORCE_CLEANUP_PROCESS_ALPHA_DESPAWN_PROP] - Mission Length: ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdMissionLengthTimer))
			#ENDIF
			
			IF bFlash
				IF IS_FLASHING_STATE_TOGGLE(iMiliseconds, iFlashToggle[iProp], iFlashToggleTime[iProp], timerToUse)
					IF GET_ENTITY_ALPHA(oiProps[iPropActual]) = 150
						SET_ENTITY_ALPHA(oiProps[iPropActual], 255, FALSE)
					ELIF GET_ENTITY_ALPHA(oiProps[iPropActual]) = 255
						SET_ENTITY_ALPHA(oiProps[iPropActual], 150, FALSE)
					ENDIF
					
					iFlashToggle[iProp]++
					PRINTLN("[LM][VV_FORCE_CLEANUP_PROCESS_ALPHA_DESPAWN_PROP] - iFlashToggle++ ")
					PRINTLN("[LM][VV_FORCE_CLEANUP_PROCESS_ALPHA_DESPAWN_PROP] - Alpha is now set to: ", GET_ENTITY_ALPHA(oiProps[iPropActual]))
				ENDIF
			ELSE
				IF IS_FLASHING_STATE_TOGGLE(iMiliseconds, iFlashToggle[iProp], iFlashToggleTime[iProp], timerToUse)
					IF GET_ENTITY_ALPHA(oiProps[iPropActual])-25 >= 0
						SET_ENTITY_ALPHA(oiProps[iPropActual], GET_ENTITY_ALPHA(oiProps[iPropActual])-25, FALSE)
					ENDIF
					iFlashToggle[iProp]++
					
					PRINTLN("[LM][VV_FORCE_CLEANUP_PROCESS_ALPHA_DESPAWN_PROP] - iFadeToggle++ ")
					PRINTLN("[LM][VV_FORCE_CLEANUP_PROCESS_ALPHA_DESPAWN_PROP] - Alpha is now set to: ", GET_ENTITY_ALPHA(oiProps[iPropActual]))
				ENDIF
			ENDIF
		ENDIF
		
		IF iFlashToggle[iProp] >= 7
		OR (iFlashToggle[iProp] > 1 AND NOT HAS_NET_TIMER_STARTED(timerToUse))
			PRINTLN("[LM][VV_FORCE_CLEANUP_PROCESS_ALPHA_DESPAWN_PROP] - Flashing finished for iProp: ", iPropActual)
			
			SET_ENTITY_ALPHA(oiProps[iPropActual], 0, FALSE)
			SET_ENTITY_COLLISION(oiProps[iPropActual], FALSE)
			REMOVE_DECALS_FROM_OBJECT(oiProps[iPropActual])
			SET_OBJECT_AS_NO_LONGER_NEEDED(oiProps[iPropActual])
			IF bIsLocalPlayerHost
				SET_BIT(MC_serverBD_3.iPropDestroyedBS[iPropActual / 32], iPropActual % 32)
			ENDIF
			
			iPropFadeoutEveryFrameIndex[iProp] = -1
			iFlashToggle[iProp] = 1
			iFlashToggleTime[iProp] = 0
			CLEAR_BIT(iCleanUpPropEF_BS[iProp], ci_CleanUpPropEF_BS_ColourNotSet)
			IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfx_PropFadeoutEveryFrameIndex[iProp])
				SET_PARTICLE_FX_LOOPED_EVOLUTION(ptfx_PropFadeoutEveryFrameIndex[iProp], "Intensity", 1.0, TRUE)
				PRINTLN("[LM][VV_FORCE_CLEANUP_PROCESS_ALPHA_DESPAWN_PROP] - Timer Expired for iPropEF: ", iProp)
				START_NET_TIMER(tdFlashToggleDissolvePTFX[iProp])
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_PROP_CLEAN_UP_WITH_FADE(INT iProp)
	
	INT iPropActual = iPropFadeoutEveryFrameIndex[iProp]
	
	IF iPropActual > -1
	AND iPropActual < GET_FMMC_MAX_NUM_PROPS()
		IF FMMC_IS_LONG_BIT_SET(iPropCleanedupTriggeredBS, iPropActual)
			PRINTLN("[LM][PROCESS_PROP_CLEAN_UP_WITH_FADE] - iPropCleanedupTriggeredBS set on iProp: ", iPropActual)
			IF (g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropActual].iForceCleanupPropAfterTime > 0 // 0 means off.
			AND NOT FMMC_IS_LONG_BIT_SET(iPropDetonateTriggeredBS, iPropActual))
			OR FMMC_IS_LONG_BIT_SET(iPropDetonateTriggeredBS, iPropActual)
				IF HAS_NET_TIMER_STARTED(MC_serverBD.tdMissionLengthTimer)
					PRINTLN("[LM][PROCESS_PROP_CLEAN_UP_WITH_FADE] - Calling VV ForceCleanup: ", iPropActual)
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropActual].iPropBitset, ciFMMC_PROP_Cleanup_Flash)
						VV_FORCE_CLEANUP_PROCESS_ALPHA_DESPAWN_PROP(iProp, TRUE, ci_FLASH_TIMER_INTERVAL)
						PRINTLN("[LM][PROCESS_PROP_CLEAN_UP_WITH_FADE] - Flash")
					ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iPropActual].iPropBitset, ciFMMC_PROP_Cleanup_Fade)
						VV_FORCE_CLEANUP_PROCESS_ALPHA_DESPAWN_PROP(iProp, FALSE, ci_FLASH_TIMER_INTERVAL)
						PRINTLN("[LM][PROCESS_PROP_CLEAN_UP_WITH_FADE] - Fade")
					ELIF FMMC_IS_LONG_BIT_SET(iPropDetonateTriggeredBS, iPropActual)
						VV_FORCE_CLEANUP_PROCESS_ALPHA_DESPAWN_PROP(iProp, TRUE, ci_FLASH_TIMER_INTERVAL)
						PRINTLN("[LM][PROCESS_PROP_CLEAN_UP_WITH_FADE] - Detonate")
					ELSE
						VV_FORCE_CLEANUP_PROCESS_ALPHA_DESPAWN_PROP(iProp, TRUE, ci_FLASH_TIMER_INTERVAL)
						PRINTLN("[LM][PROCESS_PROP_CLEAN_UP_WITH_FADE] - Default")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(tdFlashToggleDissolvePTFX[iProp])
		IF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdFlashToggleDissolvePTFX[iProp], ci_FlashToggleDissolveTime)
			PRINTLN("[LM][VV_FORCE_CLEANUP_PROCESS_ALPHA_DESPAWN_PROP] - Timer Expired for iPropEF: ", iProp)
			IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfx_PropFadeoutEveryFrameIndex[iProp])
				STOP_PARTICLE_FX_LOOPED(ptfx_PropFadeoutEveryFrameIndex[iProp], TRUE)
				PRINTLN("[LM][VV_FORCE_CLEANUP_PROCESS_ALPHA_DESPAWN_PROP] - Stopping ptfx for iPropEF: ", iProp)
			ENDIF
			RESET_NET_TIMER(tdFlashToggleDissolvePTFX[iProp])
		ENDIF
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Staggered Prop Processing ---------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Staggered prop processing.  --------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_FLARE_PROP_STAGGERED(INT iProp, INT iPropAssociatedObjective)

	STRING strFlareFX, strPTFXAsset
	INT iR, iG, iB, iA

	IF iPropAssociatedObjective = -1
	OR iPropAssociatedObjective <= GET_LOCAL_PLAYER_CURRENT_RULE(TRUE)
				
		IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iFlareVFX = ciFLARE_VFX_SHOW_WINNER
		OR g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iFlareVFX = ciFLARE_VFX_CAPTURE_OWNER
		OR g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iFlareVFX = ciFLARE_VFX_CUSTOM_COLOUR
			PRINTLN("[RCC MISSION][AW SMOKE] Changing colour flare loading in -scr_lowrider ")
			strPTFXAsset = "scr_lowrider"
		ELSE
			PRINTLN("[RCC MISSION][AW SMOKE] Standard flare loading in -scr_biolab_heist ")
			strPTFXAsset = "scr_biolab_heist"
		ENDIF
						
		REQUEST_NAMED_PTFX_ASSET(strPTFXAsset)
		IF HAS_NAMED_PTFX_ASSET_LOADED(strPTFXAsset)
		AND iNumCreatedFlarePTFX < ciTOTAL_PTFX
		AND NOT FMMC_IS_LONG_BIT_SET(iCreatedFlaresBitset, iProp)
			SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iFlareVFX
			
				CASE ciFLARE_VFX_OUTDOORS
					strFlareFX = "scr_heist_biolab_flare"
				BREAK
				
				CASE ciFLARE_VFX_INTERIOR
					strFlareFX = "scr_heist_biolab_flare"
				BREAK
				
				CASE ciFLARE_VFX_UNDERWATER
					strFlareFX = "scr_heist_biolab_flare_underwater"
				BREAK
				
				CASE ciFLARE_VFX_SHOW_WINNER
					strFlareFX = "scr_lowrider_flare"
				BREAK
				
				CASE ciFLARE_VFX_CAPTURE_OWNER
					strFlareFX = "scr_lowrider_flare"
				BREAK
				
				CASE ciFLARE_VFX_CUSTOM_COLOUR
					strFlareFX = "scr_lowrider_flare"
				BREAK
				
			ENDSWITCH

			IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(ptfxFlareIndex[iNumCreatedFlarePTFX])
			AND DOES_ENTITY_EXIST(oiProps[iProp])
			
				IF NOT ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
					USE_PARTICLE_FX_ASSET(strPTFXAsset) 
					
					ptfxFlareIndex[iNumCreatedFlarePTFX] = START_PARTICLE_FX_LOOPED_ON_ENTITY(strFlareFX, oiProps[iProp], <<0,0,0.12>>, <<0,0,0>>, 1.0)							
					
					//Sound
					FlareSoundID[iNumCreatedFlarePTFX] = -1
					FlareSoundID[iNumCreatedFlarePTFX] = GET_SOUND_ID()
					FlareSoundIDPropNum[iNumCreatedFlarePTFX] = iProp
					PLAY_SOUND_FROM_ENTITY(FlareSoundID[iNumCreatedFlarePTFX], "Flare", oiProps[iProp], "DLC_GR_DR_Player_Sounds")
					
					IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iFlareVFX = ciFLARE_VFX_CUSTOM_COLOUR
						IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropColouring != -1
						
							GET_HUD_COLOUR(INT_TO_ENUM(HUD_COLOURS,g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropColouring), iR, iG, iB, iA)
							PRINTLN("[RCC MISSION][AW SMOKE]iNumCreatedFlarePTFX: ",iNumCreatedFlarePTFX)
							PRINTLN("[RCC MISSION][AW SMOKE] g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropColouring: ", g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropColouring)
							PRINTLN("[RCC MISSION][AW SMOKE] iR:", iR)
							PRINTLN("[RCC MISSION][AW SMOKE] iG:", iG)
							PRINTLN("[RCC MISSION][AW SMOKE] iB:", iB)
							
							FLOAT fR, fG, fB
							fR = TO_FLOAT(iR)				
							fR  = fR  / 255.0
							
							fG = TO_FLOAT(iG)
							fG  = fG  / 255.0
							
							fB = TO_FLOAT(iB)
							fB  = fB  / 255.0
							
							PRINTLN("[RCC MISSION][AW SMOKE] fR:",fR)
							PRINTLN("[RCC MISSION][AW SMOKE] fG:",fG)
							PRINTLN("[RCC MISSION][AW SMOKE] fB:",fB)
							
							//unref
							IF iA = 0
							
							ENDIF
							
							SET_PARTICLE_FX_LOOPED_COLOUR(ptfxFlareIndex[iNumCreatedFlarePTFX],fR, fG, fB,TRUE)
							
						ENDIF
					ENDIF
					
					FREEZE_ENTITY_POSITION(oiProps[iProp], TRUE)	//Stop Flares being moved causing a de-sync in coordinates (as they're not networked).
					
					iNumCreatedFlarePTFX++
					FMMC_SET_LONG_BIT(iCreatedFlaresBitset, iProp)
					PRINTLN("[RCC MISSION][AW SMOKE] Creating flare etc ")
					CPRINTLN(DEBUG_CONTROLLER, "[RCC MISSION] [PTFX] Created flare #", iNumCreatedFlarePTFX, " with string: ", strFlareFX)
					
					IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iFlareVFX = ciFLARE_VFX_CAPTURE_OWNER
						PRINTLN("[RCC MISSION][AW SMOKE] INITALISE_FLARE_DATA - setting to yellow")
						INITALISE_FLARE_DATA(iNumCreatedDynamicFlarePTFX)
						SET_LOCAL_FLARE_NEW_RGB(iNumCreatedDynamicFlarePTFX, 1.0, 1.0, 0)
						IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iFlareConnectedLocate > -1
							flare_data[iNumCreatedDynamicFlarePTFX].iConnectedLocate = g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iFlareConnectedLocate
						ENDIF
						iNumCreatedDynamicFlarePTFX ++
					ENDIF
					
					
					
					g_iTheWinningTeam = - 1
					iMaxCaptureAmount = -1
					iWinningState = -1			
				ENDIF
				
			ENDIF
		ENDIF
	ENDIF
	
	INT iPTFX
	INT iHighestScore
	
	FOR iPTFX = 0 TO iNumCreatedFlarePTFX -1
		IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxFlareIndex[iPTFX])
			PRINTLN("[RCC MISSION][AW SMOKE] Particle FX Looped Exists  ")
			IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iFlareVFX = ciFLARE_VFX_CAPTURE_OWNER
			
				
				// Logic for this is now handled in - PROCESS_EVERY_FRAME_CAPTURE_LOGIC()
				
				IF NOT IS_BIT_SET(iLocalBoolCheck14, LBOOL14_VFX_CAPTURE_OWNER)
					SET_BIT(iLocalBoolCheck14, LBOOL14_VFX_CAPTURE_OWNER)
				ENDIF
			ELIF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iFlareVFX = ciFLARE_VFX_SHOW_WINNER
				PRINTLN("[RCC MISSION][AW SMOKE] ciFLARE_VFX_SHOW_WINNER ")
				
				
				INT iWinningTeam
				INT i
					
				FOR i = 0 TO MC_ServerBD.iNumberOfTeams -1
					IF MC_serverBD.iTeamScore[i] > iHighestScore
						iHighestScore = MC_serverBD.iTeamScore[i]
						iWinningTeam = i
						PRINTLN("[RCC MISSION][AW SMOKE] iHighestScore: ", iHighestScore)
						PRINTLN("[RCC MISSION][AW SMOKE] iWinningTeam: ",  iWinningTeam)
					ENDIF
				ENDFOR
				
				IF GET_LOCAL_PLAYER_TEAM(TRUE) != iWinningTeam 
					IF iHighestScore =  MC_serverBD.iTeamScore[i]
						//Show Yellow = Tied
						PRINTLN("[RCC MISSION][AW SMOKE] YELLOW TIED -1 new")
						SET_PARTICLE_FX_LOOPED_COLOUR(ptfxFlareIndex[iPTFX],255,255,0,TRUE)
					ELSE
						//Show Red = Losing
						PRINTLN("[RCC MISSION][AW SMOKE] RED LOSING ")
						SET_PARTICLE_FX_LOOPED_COLOUR(ptfxFlareIndex[iPTFX],255,0,0,TRUE)
					ENDIF
				ELSE
					IF iHighestScore =  MC_serverBD.iTeamScore[i]
						//Show Yellow = Tied
						PRINTLN("[RCC MISSION][AW SMOKE] YELLOW TIED -2 new")
						SET_PARTICLE_FX_LOOPED_COLOUR(ptfxFlareIndex[iPTFX],255,255,0,TRUE)
					ELSE
						//Show Blue = Winning
						PRINTLN("[RCC MISSION][AW SMOKE] BLUE WINNING ")
						SET_PARTICLE_FX_LOOPED_COLOUR(ptfxFlareIndex[iPTFX],0,0,255,TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

PROC PROCESS_LOCH_SANTOS_MONSTER_STAGGERED(INT iProp)
	IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iCleanupRange <= 0
	OR IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_LOCH_SANTOS_MONSTER_SPOTTED)
		EXIT
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(oiProps[iProp])
		EXIT
	ENDIF
	
	IF VDIST2(vLocalPlayerPosition, GET_ENTITY_COORDS(oiProps[iProp])) <= POW(TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iCleanupRange), 2)
		BROADCAST_FMMC_LOCH_SANTOS_MONSTER()
	ENDIF
ENDPROC

PROC CLEAN_UP_LOCH_SANTOS_MONSTER(INT iProp)
	IF DOES_ENTITY_EXIST(oiProps[iProp])
		DELETE_OBJECT(oiProps[iProp])
		FMMC_SET_LONG_BIT(iPropCleanedupBS, iProp)
		PRINTLN("CLEAN_UP_LOCH_SANTOS_MONSTER - Cleaned up prop ", iProp)
		REMOVE_NAMED_PTFX_ASSET(GET_LOCH_SANTOS_MONSTER_PTFX_ASSET())
	ENDIF
ENDPROC

PROC PROCESS_LOCH_SANTOS_MONSTER_EVERY_FRAME(INT iProp)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].mn != INT_TO_ENUM(MODEL_NAMES, HASH("h4_Prop_h4_Loch_Monster"))
		EXIT
	ENDIF
	
	IF MC_serverBD.iWeather = ciFMMC_WEATHER_OPTION_THUNDER
	OR MC_serverBD.iWeather = ciFMMC_WEATHER_OPTION_RAIN_THUNDER
		CLEAN_UP_LOCH_SANTOS_MONSTER(iProp)
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(oiProps[iProp])
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(iLocalBoolCheck34, LBOOL34_LOCH_SANTOS_MONSTER_CLEANED_UP)
		
		VECTOR vCoord = GET_ENTITY_COORDS(oiProps[iProp])
		
		SWITCH iLSM_State
			CASE ciLSM_State_Init
				REQUEST_NAMED_PTFX_ASSET(GET_LOCH_SANTOS_MONSTER_PTFX_ASSET())
		
				IF NOT HAS_NAMED_PTFX_ASSET_LOADED(GET_LOCH_SANTOS_MONSTER_PTFX_ASSET())
					PRINTLN("PROCESS_LOCH_SANTOS_MONSTER_EVERY_FRAME - Requesting PTFX")
					EXIT
				ENDIF
				
				iLSM_State = ciLSM_State_Idle
				PRINTLN("PROCESS_LOCH_SANTOS_MONSTER_EVERY_FRAME - Moving to ciLSM_State_Idle for LSM")
			BREAK
			CASE ciLSM_State_Idle
				IF IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_LOCH_SANTOS_MONSTER_SPOTTED)
					REINIT_NET_TIMER(tdLSM_Timer)
					PRINTLN("PROCESS_LOCH_SANTOS_MONSTER_EVERY_FRAME - Starting timer for LSM - ciLSM_State_Idle")
					iLSM_State = ciLSM_State_Splash
					PRINTLN("PROCESS_LOCH_SANTOS_MONSTER_EVERY_FRAME - Moving to ciLSM_State_Splash for LSM - Loch Ness Monster is gunna need about tree-fiddy")
				ENDIF
			BREAK
			CASE ciLSM_State_Splash
				IF bIsLocalPlayerHost
					VECTOR vSplash
					vSplash = GET_OFFSET_FROM_COORD_IN_WORLD_COORDS(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].vPos, GET_ENTITY_HEADING(oiProps[iProp]), <<0.0, -4.0, -30.0>>)
					ADD_EXPLOSION(vSplash, EXP_TAG_BOMB_WATER_SECONDARY, 0.5, FALSE, FALSE, DEFAULT, TRUE)
				ENDIF
				SET_BIT(iLocalBoolCheck34, LBOOL34_LOCH_SANTOS_MONSTER_DIVING)
				iLSM_State = ciLSM_State_Descending
				PRINTLN("PROCESS_LOCH_SANTOS_MONSTER_EVERY_FRAME - Moving to ciLSM_State_Descending for LSM")
			BREAK
			CASE ciLSM_State_Descending
				IF NOT HAS_NET_TIMER_STARTED(tdLSM_Timer)
					REINIT_NET_TIMER(tdLSM_Timer)
					PRINTLN("PROCESS_LOCH_SANTOS_MONSTER_EVERY_FRAME - Starting timer for LSM - ciLSM_State_Descending")
				ELSE
					FLOAT fTime
					fTime = TO_FLOAT(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdLSM_Timer)) / ciLSM_DescendTime
					
					VECTOR vOffset, vForward, vRight, vUp, vPos
					GET_ENTITY_MATRIX(oiProps[iProp], vForward, vRight, vUp, vPos)
					
					vOffSet.x = vRight.x * (fTime*2)
					vOffSet.y = vRight.y * (fTime*2)
					vOffSet.z = fTime
					
					SET_ENTITY_COORDS(oiProps[iProp], vCoord - vOffset)
					
					VECTOR vRot
					vRot = GET_ENTITY_ROTATION(oiProps[iProp])
					vRot.y -= (ciLSM_DescendRotationRate * GET_FRAME_TIME())
					SET_ENTITY_ROTATION(oiProps[iProp], vRot)
					
					IF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdLSM_Timer, ciLSM_DescendTime)
						iLSM_State = ciLSM_State_Cleanup
						PRINTLN("PROCESS_LOCH_SANTOS_MONSTER_EVERY_FRAME - Moving to ciLSM_State_Cleanup for LSM")
					ENDIF
				ENDIF
			BREAK
			CASE ciLSM_State_Cleanup
				CLEAN_UP_LOCH_SANTOS_MONSTER(iProp)
				RESET_NET_TIMER(tdLSM_Timer)
				SET_BIT(iLocalBoolCheck34, LBOOL34_LOCH_SANTOS_MONSTER_CLEANED_UP)
				PRINTLN("PROCESS_LOCH_SANTOS_MONSTER_EVERY_FRAME - Set LBOOL34_LOCH_SANTOS_MONSTER_CLEANED_UP - Loch Ness Monster got it's tree-fiddy")
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

PROC PROCESS_PROP_BEHAVIOUR(INT iProp)
	
	IF NOT DOES_PROP_HAVE_AN_ASSOCIATED_RULE(iProp)
		EXIT
	ENDIF
	
	INT iTeam = MC_playerBD[iLocalPart].iteam
	INT iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]
	
	INT iPropAssociatedObjective = GET_TEAM_ASSOCIATED_OBJECTIVE(iProp, iTeam)	//g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iAssociatedObjective
		
	SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].mn
		
		CASE PROP_LD_ALARM_01
			
			// If the sound ID is -1, a valid alarm sound hasn't been set yet - if not, this initialises it
			IF isoundid[iProp] = -1
				IF HAS_ANY_TEAM_HIT_PROP_ACTION_ACTIVATION(iProp)
				AND SHOULD_ALARM_PLAY_FOR_TEAM(iProp, iTeam)
					REQUEST_ALARM_SOUND_BANK_AND_SOUND_ID(iProp)
				ENDIF
			ENDIF
			
			IF isoundid[iProp] != -1 // Make some noise! We have a valid sound ID for this prop now
				MAINTAIN_PROP_LD_ALARM_01_SOUND(oiProps[iProp], isoundid[iProp], g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iAlarmSound,iNonRepeatingAlarmPlayedBS, iSiloAlarmLoopSound)
			ENDIF
			
		BREAK
		
		CASE PROP_FLARE_01
			PROCESS_FLARE_PROP_STAGGERED(iProp, iPropAssociatedObjective)
		BREAK
		
		CASE IND_PROP_FIREWORK_01
			PRINTLN("[FW] Prop ", iProp, " || Looking at a IND_PROP_FIREWORK_01!")
			IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropBitSet2, ciFMMC_PROP2_UsePlacedFireworkZone) 
				IF iPropAssociatedObjective <= iRule OR iPropAssociatedObjective = -1
					IF NOT FMMC_IS_LONG_BIT_SET(iCreatedFireworkBitset, iProp)
						IF CREATE_FIREWORK_FX(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iAlarmDelay, oiProps[iProp], g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iAlarmSound)
							FMMC_SET_LONG_BIT(iCreatedFireworkBitset, iProp)
						ELSE
							PRINTLN("[FW] Prop ", iProp, " || CREATE_FIREWORK_FX is returning false!")
						ENDIF
					ELSE
						PRINTLN("[FW] Prop ", iProp, " || iCreatedFireworkBitset is set for this one")
					ENDIF
				ELSE
					PRINTLN("[FW] Prop ", iProp, " || This one doesn't have a valid objective: ", iPropAssociatedObjective)
				ENDIF
			ELSE
				PRINTLN("[FW] Prop ", iProp, " || ciFMMC_PROP2_UsePlacedFireworkZone is set for this one")
			ENDIF
		BREAK
		// Add more special props behaviour in here
		
	ENDSWITCH // End of the switch over the special prop model names
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].mn = INT_TO_ENUM(MODEL_NAMES, HASH("h4_Prop_h4_Loch_Monster"))
		PROCESS_LOCH_SANTOS_MONSTER_STAGGERED(iProp)
	ENDIF
	
	IF DOES_ENTITY_EXIST(oiProps[iProp])
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropBitset, ciFMMC_PROP_EmitCrashSound)
			IF HAS_SOUND_FINISHED(iCrashSound)
				vCrashSoundPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(oiProps[iProp],<<0.0,6.5,0.0>>)
				PLAY_SOUND_FROM_COORD(iCrashSound, "Crashed_Plane_Ambience", vCrashSoundPos, "DLC_Apartments_Extraction_SoundSet", FALSE, 0,FALSE)
			ELSE
				IF NOT ARE_VECTORS_EQUAL(vCrashSoundPos, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(oiProps[iProp],<<0.0,6.5,0.0>>))
					STOP_SOUND(iCrashSound)
					vCrashSoundPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(oiProps[iProp],<<0.0,6.5,0.0>>)
					PLAY_SOUND_FROM_COORD(iCrashSound, "Crashed_Plane_Ambience", vCrashSoundPos, "DLC_Apartments_Extraction_SoundSet", FALSE, 0,FALSE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_PROP_PRE_STAGGERED_CLIENT()

ENDPROC

PROC PROCESS_PROP_STAGGERED_CLIENT(INT iProp)
	
	PROCESS_PROP_RESPAWNING(iProp)
	
	IF NOT DOES_ENTITY_EXIST(oiProps[iProp])
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropBitset, ciFMMC_PROP_EmitCrashSound)
			IF NOT HAS_SOUND_FINISHED(iCrashSound)
				STOP_SOUND(iCrashSound)
			ENDIF
		ENDIF
		
		EXIT
	ENDIF
	
	IF SHOULD_CLEANUP_PROP(iProp, oiProps[iProp])
		PRINTLN("[PROPS] PROCESS_PROP_STAGGERED_CLIENT - SHOULD_CLEANUP_PROP, iProp = ", iProp)
		FMMC_SET_LONG_BIT(iPropCleanedupBS, iProp)
		EXIT
	ENDIF
	
	PROCESS_PROP_BEHAVIOUR(iProp)
	
	PROCESS_ENTITY_INTERIOR(iProp, oiProps[iProp], IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropBitSet2, ciFMMC_PROP2_PlacedInAnInterior), g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].vPropSpecifiedInterior, iPropHasBeenPlacedIntoInteriorBS[iProp / 32], g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropSpecifiedRoomKey)

	VECTOR vPlayerCoords 
	IF bLocalPlayerPedOk
		vPlayerCoords = GET_ENTITY_COORDS(LocalPlayerPed)
	ENDIF
	MAINTAIN_PLACED_AUDIO_TRIGGER_PROPS(iProp, vPlayerCoords)	
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropLodDist != -1
		IF GET_ENTITY_LOD_DIST(oiProps[iProp]) != g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropLodDist
			SET_ENTITY_LOD_DIST(oiProps[iProp], g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropLodDist)
			PRINTLN("[PROPS] PROCESS_PROP_STAGGERED_CLIENT - iProp = ", iProp , " SET_ENTITY_LOD_DIST(oiProps[iProp], g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropLodDist) = ", g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropLodDist)
		ENDIF
	ENDIF
	
ENDPROC	

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Every-Frame Prop Processing -------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Every-frame prop processing.  ------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_PROP_FOR_CUTSCENES(INT iProp)
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropBitSet2, ciFMMC_PROP2_HidePropInCutscene)
		IF DOES_ENTITY_EXIST(oiProps[iProp])
			IF g_bInMissionControllerCutscene
				IF NOT FMMC_IS_LONG_BIT_SET(iPropHiddenForCutsceneBS, iProp)
					SET_ENTITY_ALPHA(oiProps[iProp], 0, FALSE)
					FMMC_SET_LONG_BIT(iPropHiddenForCutsceneBS, iProp)
					PRINTLN("[LM][RCC MISSION][PROCESS_PROP_FOR_CUTSCENES] iProp ", iProp, " is flagged to be hidden in cutscenes. We are in a cutscene, setting prop alpha to 0.")
				ENDIF
			ELSE
				IF FMMC_IS_LONG_BIT_SET(iPropHiddenForCutsceneBS, iProp)
					RESET_ENTITY_ALPHA(oiProps[iProp])
					FMMC_CLEAR_LONG_BIT(iPropHiddenForCutsceneBS, iProp)
					PRINTLN("[LM][RCC MISSION][PROCESS_PROP_FOR_CUTSCENES] iProp ", iProp, " is flagged to be hidden in cutscenes. We are no longer playing a cutscene, resetting prop alpha.")
				ENDIF
			ENDIF
		ELSE
			FMMC_CLEAR_LONG_BIT(iPropHiddenForCutsceneBS, iProp)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_SLIDING_PROP_SLIDE_NOW(INT iProp, INT iPropSlideRule, INT iPropSlideTeam)
	UNUSED_PARAMETER(iProp)
	
	IF HAVE_PLAYERS_PROGRESSED_TO_RULE(iPropSlideRule, iPropSlideTeam)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_SLIDING_PROP_SOUND_ID_INDEX(INT iProp)
	INT iSlidingPropSoundIDIndex = 0
	
	FOR iSlidingPropSoundIDIndex = 0 TO ciMAX_SLIDING_PROP_SOUND_IDS - 1
		IF iSlidingPropSoundIDUserIndex[iSlidingPropSoundIDIndex] = iProp
			RETURN iSlidingPropSoundIDIndex
		ENDIF
	ENDFOR
	
	RETURN -1
ENDFUNC

FUNC BOOL GET_SLIDING_PROP_SOUNDS(INT iProp, SLIDING_PROP_SOUNDS& sSlidingPropSounds)
	IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].mn = INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_inttruck_door_01"))
		sSlidingPropSounds.sSlidingOutLoop = "Door_Opening"
		sSlidingPropSounds.sSlidingBackLoop = "Door_Closing"
		sSlidingPropSounds.sSlideComplete = "Door_Opened"
		sSlidingPropSounds.sSlideReturned = "Door_Closed"
		sSlidingPropSounds.sSlideSoundset = "Military_Convoy_HiJack_Sounds"
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_SLIDING_PROPS(INT iProp)

	VECTOR vPropSlideVector = g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].vPropSlideVector
	FLOAT fInterpSpeed = g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].fPropSlideSpeed
	INT iPropSlideRule = g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropSlideRule
	INT iPropSlideTeam = g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropSlideTeam
	
	IF iPropSlideRule < 0
		EXIT
	ENDIF
	
	ENTITY_INDEX eiProp = GET_FMMC_ENTITY(ciENTITY_TYPE_PROPS, iProp)
	VECTOR vTargetPosition = g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].vPos
	BOOL bSlidingOut = FALSE
	
	IF NOT DOES_ENTITY_EXIST(eiProp)
		EXIT
	ENDIF
	
	SLIDING_PROP_SOUNDS sSlidingPropSounds
	GET_SLIDING_PROP_SOUNDS(iProp, sSlidingPropSounds)
	INT iSoundIDIndex = GET_SLIDING_PROP_SOUND_ID_INDEX(iProp)
	
	IF SHOULD_SLIDING_PROP_SLIDE_NOW(iProp, iPropSlideRule, iPropSlideTeam)
		VECTOR vForward, vRight, vUp, vCoords
		GET_ENTITY_MATRIX(eiProp, vForward, vRight, vUp, vCoords)
		
		vTargetPosition += vRight * vPropSlideVector.x
		vTargetPosition += vForward * vPropSlideVector.y
		vTargetPosition += vUp * vPropSlideVector.z
		
		IF NOT FMMC_IS_LONG_BIT_SET(iPropStartedSlidingOutBS, iProp)
			PRINTLN("[Props][Prop ", iProp, "] PROCESS_SLIDING_PROPS | Prop has started sliding!")
			
			IF iSoundIDIndex > -1
				IF iSlidingPropSoundIDs[iSoundIDIndex] > -1
					STOP_SOUND(iSlidingPropSoundIDs[iSoundIDIndex])
				ELSE
					iSlidingPropSoundIDs[iSoundIDIndex] = GET_SOUND_ID()
				ENDIF
				PLAY_SOUND_FROM_ENTITY(iSlidingPropSoundIDs[iSoundIDIndex], sSlidingPropSounds.sSlidingOutLoop, eiProp, sSlidingPropSounds.sSlideSoundset)
			ENDIF
			
			FMMC_SET_LONG_BIT(iPropStartedSlidingOutBS, iProp)
		ENDIF
		
		bSlidingOut = TRUE
	ELSE
		IF FMMC_IS_LONG_BIT_SET(iPropSlidingCompleteBS, iProp)
			PRINTLN("[Props][Prop ", iProp, "] PROCESS_SLIDING_PROPS | Prop has started sliding back to where it started!")
			
			IF iSoundIDIndex > -1
				IF iSlidingPropSoundIDs[iSoundIDIndex] > -1
					STOP_SOUND(iSlidingPropSoundIDs[iSoundIDIndex])
				ELSE
					iSlidingPropSoundIDs[iSoundIDIndex] = GET_SOUND_ID()
				ENDIF
				PLAY_SOUND_FROM_ENTITY(iSlidingPropSoundIDs[iSoundIDIndex], sSlidingPropSounds.sSlidingBackLoop, eiProp, sSlidingPropSounds.sSlideSoundset)
			ENDIF
			
			FMMC_CLEAR_LONG_BIT(iPropSlidingCompleteBS, iProp)
		ENDIF
	ENDIF
	
	VECTOR vNewPos = SMOOTH_DAMP_VECTOR(GET_ENTITY_COORDS(eiProp), vTargetPosition, fInterpSpeed)
	SET_ENTITY_COORDS(eiProp, vNewPos)
	
	IF VDIST2(vNewPos, vTargetPosition) < 0.1
		// The prop has reached its target location!
		IF bSlidingOut
			IF NOT FMMC_IS_LONG_BIT_SET(iPropSlidingCompleteBS, iProp)
				PRINTLN("[Props][Prop ", iProp, "] PROCESS_SLIDING_PROPS | Prop has completed its slide outwards!")
				PLAY_SOUND_FROM_ENTITY(-1, sSlidingPropSounds.sSlideComplete, eiProp, sSlidingPropSounds.sSlideSoundset)
				FMMC_SET_LONG_BIT(iPropSlidingCompleteBS, iProp)
			ENDIF
		ELSE
			IF FMMC_IS_LONG_BIT_SET(iPropStartedSlidingOutBS, iProp)
				PRINTLN("[Props][Prop ", iProp, "] PROCESS_SLIDING_PROPS | Prop has completed its slide back to where it started!")
				PLAY_SOUND_FROM_ENTITY(-1, sSlidingPropSounds.sSlideReturned, eiProp, sSlidingPropSounds.sSlideSoundset)
				FMMC_CLEAR_LONG_BIT(iPropStartedSlidingOutBS, iProp)
			ENDIF
		ENDIF
		
		IF iSoundIDIndex > -1
			IF iSlidingPropSoundIDs[iSoundIDIndex] > -1
				PRINTLN("[Props][Prop ", iProp, "] PROCESS_SLIDING_PROPS | Stopping looping sound!")
				STOP_SOUND(iSlidingPropSoundIDs[iSoundIDIndex])
				RELEASE_SOUND_ID(iSlidingPropSoundIDs[iSoundIDIndex])
				iSlidingPropSoundIDs[iSoundIDIndex] = -1
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_CLEANUP_PROPS_EARLY()

	// Likely do not want to affect Co-op missions.
	IF g_FMMC_STRUCT.iAdversaryModeType = 0
		EXIT
	ENDIF
	
	// Skycam has just transitioned to be fully in the air.
	IF GET_SKYSWOOP_STAGE() = SKYSWOOP_GOINGUP
		// Flares persisted in the air.
		IF iNumCreatedDynamicFlarePTFX > 0
		OR iNumCreatedFlarePTFX > 0
			PRINTLN("[RCC MISSION][PROCESS_CLEANUP_PROPS_EARLY][LM] Calling early prop cleanups.")
			CLEANUP_DYNAMIC_FLARES()
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_PROP_CLEANUP_LOGIC()
	// Do not need to process anything here.
	IF NOT bLocalPlayerPedOk
	OR IS_PLAYER_RESPAWNING(LocalPlayer)
	OR bIsAnySpectator
	OR NOT IS_BIT_SET(iLocalBoolCheck28, LBOOL28_USING_PROP_CLEANUP_FUNCTIONALITY)
	OR IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_PLAYER_FAIL)
		CLEAR_BIT(iLocalBoolCheck28, LBOOL28_TRAP_DOOR_LEAVE_AREA_SHARD)
		IF DOES_BLIP_EXIST(blipSafeProp)
			PRINTLN("[RCC MISSION][PROCESS_PROP_CLEANUP_LOGIC] - clearing blip. (fail/dead)")
			REMOVE_BLIP(blipSafeProp)
		ENDIF
		EXIT
	ENDIF
	
	// Timer for the cover one.
	IF IS_PED_FALLING(localPlayerPed) 
		IF NOT HAS_NET_TIMER_STARTED(tdFallingTimer)
			START_NET_TIMER(tdFallingTimer)
		ENDIF
	ELSE
		IF HAS_NET_TIMER_STARTED(tdFallingTimer)
			RESET_NET_TIMER(tdFallingTimer)
		ENDIF
	ENDIF
	
	SET_PED_RESET_FLAG(localPlayerPed, PRF_DisableDropDowns, TRUE)
	
	TEXT_LABEL_15 tl15a = GET_BIG_MESSAGE_TEXT_BEING_DRAWN()
	TEXT_LABEL_15 tl15b = "MC_TRPDR_OF"
	
	// Play Shard and Blip
	IF MC_PlayerBD[iLocalPart].iCurrentPropHit[2] > -1	
		IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[MC_PlayerBD[iLocalPart].iCurrentPropHit[2]].iColourChange > -1
			IF DOES_ENTITY_EXIST(oiProps[MC_PlayerBD[iLocalPart].iCurrentPropHit[2]])							
				IF GET_OBJECT_TINT_INDEX(oiProps[MC_PlayerBD[iLocalPart].iCurrentPropHit[2]]) = g_FMMC_STRUCT_ENTITIES.sPlacedProp[MC_PlayerBD[iLocalPart].iCurrentPropHit[2]].iColourChange					
					IF NOT IS_PED_FALLING(localPlayerPed)
					AND NOT IS_PED_JUMPING(localPlayerPed)
					AND (HAS_NET_TIMER_EXPIRED_READ_ONLY(tdCoverTimerForShard, ci_COVER_TIME_FOR_SHARD) OR NOT HAS_NET_TIMER_STARTED(tdCoverTimerForShard))
						
						IF iSafeBlippedProp > -1
							IF NOT DOES_BLIP_EXIST(blipSafeProp)
								blipSafeProp = ADD_BLIP_FOR_COORD(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iSafeBlippedProp].vPos)
								SET_BLIP_COLOUR(blipSafeProp, BLIP_COLOUR_YELLOW)
								SET_BLIP_SCALE(blipSafeProp, 1.0)		
							ENDIF
						ENDIF
						
						IF NOT IS_BIT_SET(iLocalBoolCheck28, LBOOL28_TRAP_DOOR_LEAVE_AREA_SHARD)
						AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciOPTIONSBS17_DISABLE_RED_PLATFORM_SHARD)
							PRINTLN("[RCC MISSION][PROCESS_PROP_CLEANUP_LOGIC] - On top of a dangerous prop. Setting Bit and playing Shard. iColourChange: ", g_FMMC_STRUCT_ENTITIES.sPlacedProp[MC_PlayerBD[iLocalPart].iCurrentPropHit[2]].iColourChange)
							SET_BIT(iLocalBoolCheck28, LBOOL28_TRAP_DOOR_LEAVE_AREA_SHARD)			
							IF NOT ARE_STRINGS_EQUAL(tl15a, tl15b)
								CLEAR_ALL_BIG_MESSAGES()
								SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GENERIC_TEXT, tl15b, DEFAULT, DEFAULT, 5000)
							ENDIF
						ENDIF
					ENDIF					
				ELSE
					IF IS_BIT_SET(iLocalBoolCheck28, LBOOL28_TRAP_DOOR_LEAVE_AREA_SHARD)
						PRINTLN("[RCC MISSION][PROCESS_PROP_CLEANUP_LOGIC] - No longer on a dangerous prop. Clearing Bit. (1)")
						CLEAR_BIT(iLocalBoolCheck28, LBOOL28_TRAP_DOOR_LEAVE_AREA_SHARD)				
					ENDIF
					IF DOES_BLIP_EXIST(blipSafeProp)
						PRINTLN("[RCC MISSION][PROCESS_PROP_CLEANUP_LOGIC] - clearing blip. (1)")
						REMOVE_BLIP(blipSafeProp)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF IS_BIT_SET(iLocalBoolCheck28, LBOOL28_TRAP_DOOR_LEAVE_AREA_SHARD)
				PRINTLN("[RCC MISSION][PROCESS_PROP_CLEANUP_LOGIC] - No longer on a dangerous prop. Clearing Bit. (2)")
				CLEAR_BIT(iLocalBoolCheck28, LBOOL28_TRAP_DOOR_LEAVE_AREA_SHARD)
			ENDIF
			IF DOES_BLIP_EXIST(blipSafeProp)
				PRINTLN("[RCC MISSION][PROCESS_PROP_CLEANUP_LOGIC] - clearing blip. (2)")
				REMOVE_BLIP(blipSafeProp)
			ENDIF
		ENDIF
	ENDIF
	
	// Stop taking cover from playing the shard.
	IF IS_PED_GOING_INTO_COVER(localPlayerPed)
	OR IS_PED_IN_COVER(localPlayerPed)
		IF NOT HAS_NET_TIMER_STARTED(tdCoverTimerForShard)
			START_NET_TIMER(tdCoverTimerForShard)
		ENDIF
	ELSE
		IF HAS_NET_TIMER_STARTED(tdCoverTimerForShard)
		AND HAS_NET_TIMER_EXPIRED_READ_ONLY(tdCoverTimerForShard, ci_COVER_TIME_FOR_SHARD)
			RESET_NET_TIMER(tdCoverTimerForShard)
		ENDIF
	ENDIF
	
	// Show Locate
	IF iSafeBlippedProp > -1
		IF DOES_BLIP_EXIST(blipSafeProp)
			VECTOR vMarkerPos
			vMarkerPos = g_FMMC_STRUCT_ENTITIES.sPlacedProp[iSafeBlippedProp].vPos
			
			DRAW_MARKER(MARKER_CYLINDER,
			vMarkerPos,
			(<<0,0,0>>),
			(<<0,0,0>>),
			(<<10.0,10.0,75.0>>),
			190,
			200,
			90,
			135)
		ENDIF
	ENDIF
	
	// Clear Shard if we are falling
	IF ARE_STRINGS_EQUAL(tl15a, tl15b)
	AND (HAS_NET_TIMER_STARTED(tdFallingTimer) AND HAS_NET_TIMER_EXPIRED_READ_ONLY(tdFallingTimer, 1500))
	AND NOT IS_PED_CLIMBING(localPlayerPed)
	AND NOT IS_PED_JUMPING(localPlayerPed)
		PRINTLN("[RCC MISSION][PROCESS_PROP_CLEANUP_LOGIC] - Falling, clearing Exit Area Shard")
		IF ARE_STRINGS_EQUAL(tl15a, tl15b)
			CLEAR_ALL_BIG_MESSAGES()
		ENDIF
		IF DOES_BLIP_EXIST(blipSafeProp)
			PRINTLN("[RCC MISSION][PROCESS_PROP_CLEANUP_LOGIC] - clearing blip.")
			REMOVE_BLIP(blipSafeProp)
		ENDIF
	ENDIF		
	
	// Timer for the cover one.
	IF MC_PlayerBD[iLocalPart].iCurrentPropHit[0] = -1
		IF NOT HAS_NET_TIMER_STARTED(tdClearCoverTaskProp)
			START_NET_TIMER(tdClearCoverTaskProp)
		ENDIF
	ELSE
		IF HAS_NET_TIMER_STARTED(tdClearCoverTaskProp)
			RESET_NET_TIMER(tdClearCoverTaskProp)
		ENDIF
	ENDIF
	
	// Cancel Cover Task.	
	IF IS_PED_IN_COVER(LocalPlayerPed, FALSE)	
	AND NOT IS_PED_GOING_INTO_COVER(LocalPlayerPed)
	AND (NOT IS_PED_AIMING_FROM_COVER(localPlayerPed) OR (MC_PlayerBD[iLocalPart].iCurrentPropHit[1] = -1 AND MC_PlayerBD[iLocalPart].iCurrentPropHit[2] = -1))
		IF (MC_PlayerBD[iLocalPart].iCurrentPropHit[0] = -1 AND (HAS_NET_TIMER_STARTED(tdClearCoverTaskProp) AND HAS_NET_TIMER_EXPIRED_READ_ONLY(tdClearCoverTaskProp, 1500)))
		OR (MC_PlayerBD[iLocalPart].iCurrentPropHit[1] = -1 AND MC_PlayerBD[iLocalPart].iCurrentPropHit[2] = -1)
			PRINTLN("[RCC MISSION][PROCESS_PROP_CLEANUP_LOGIC] - Clearing Tasks as we are in cover and there is no prop under us.")
			
			IF GET_SCRIPT_TASK_STATUS(LocalPlayerPed, SCRIPT_TASK_EXIT_COVER) != PERFORMING_TASK
			AND GET_SCRIPT_TASK_STATUS(LocalPlayerPed, SCRIPT_TASK_EXIT_COVER) != WAITING_TO_START_TASK			
				TASK_EXIT_COVER(LocalPlayerPed, IDLE_COVER_EXIT, GET_ENTITY_COORDS(LocalPlayerPed))
				IF HAS_NET_TIMER_STARTED(tdClearCoverTaskProp)
					RESET_NET_TIMER(tdClearCoverTaskProp)
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF HAS_NET_TIMER_STARTED(tdClearCoverTaskProp)
			RESET_NET_TIMER(tdClearCoverTaskProp)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_PROP_EVERY_FRAME_CLIENT(INT iProp)

	IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iRespawnOnRuleChangeBS != 0
		INT iTeam
		FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
			IF MC_serverBD_4.iCurrentHighestPriority[iTeam] > 0
				IF IS_BIT_SET(iLocalBoolCheck25, LBOOL25_RESET_ALL_PROPS_ON_RENDERPHASE_PAUSE)
				AND NOT GET_TOGGLE_PAUSED_RENDERPHASES_STATUS() 
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iRespawnOnRuleChangeBS, iTeam)
						PRINTLN("[LM][PROCESS_PROP_EVERY_FRAME][GET_TOGGLE_PAUSED_RENDERPHASES_STATUS] - Resetting Prop: ", iProp)
						DELETE_OBJECT(oiProps[iProp])
						FMMC_CLEAR_LONG_BIT(iPropCleanedupBS, iProp)
						FMMC_SET_LONG_BIT(iPropRespawnNowBS, iProp)
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
	ENDIF

	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropBitset, ciFMMC_PROP_Requires_Alpha_Flash)
		MAINTAIN_SPEED_BOOST_ALPHA_FLASH(oiProps[iProp], iProp)
	ENDIF
	
	PROCESS_PROP_FOR_CUTSCENES(iProp)
	
	PROCESS_SLIDING_PROPS(iProp)
	
	IF SHOULD_SHOW_TAGGED_TARGET()
	AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropBitset2, ciFMMC_PROP2_CAN_BE_TAGGED)
	AND DOES_ENTITY_EXIST(oiProps[iProp])
		IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam] < FMMC_MAX_RULES
			IF IS_IT_SAFE_TO_ADD_TAG_BLIP(MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam])
				INT iTagged
				FOR iTagged = 0 TO FMMC_MAX_TAGGED_ENTITIES-1
					IF MC_serverBD_3.iTaggedEntityType[iTagged] = ENUM_TO_INT(GET_ENTITY_TYPE(oiProps[iProp]))
						IF MC_serverBD_3.iTaggedEntityIndex[iTagged] = iProp
							IF IS_ENTITY_ALIVE(oiProps[iProp])
								VECTOR vEntityCoords = GET_ENTITY_COORDS(oiProps[iProp])
								vEntityCoords.z = (vEntityCoords.z + 1.0)
								
								DRAW_TAGGED_MARKER(vEntityCoords, oiProps[iProp], ENUM_TO_INT(GET_ENTITY_TYPE(oiProps[iProp])), iProp)
								
								IF NOT DOES_BLIP_EXIST(biTaggedEntity[iTagged])
								AND NOT DOES_BLIP_EXIST(GET_BLIP_FROM_ENTITY(oiProps[iProp]))
									ADD_TAGGED_BLIP(oiProps[iProp], ENUM_TO_INT(GET_ENTITY_TYPE(oiProps[iProp])), iProp, iTagged)
								ENDIF
							ELSE
								REMOVE_TAGGED_BLIP(3, iProp)
							ENDIF
						ENDIF
					ENDIF
				ENDFOR
			ENDIF
		ENDIF
	ENDIF
		
	IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iForceCleanupPropAfterTime > 0
	OR FMMC_IS_LONG_BIT_SET(iPropDetonateTriggeredBS, iProp)
		IF NOT HAS_NET_TIMER_STARTED(tdFlashPropTimer)
			START_NET_TIMER(tdFlashPropTimer)
		ENDIF
	ENDIF
	
	// Force Prop Cleanup and Prop detonation.
	IF NOT FMMC_IS_LONG_BIT_SET(iPropDetonateTriggeredBS, iProp)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iForceCleanupPropAfterTime > 0 // 0 means off.
		AND NOT FMMC_IS_LONG_BIT_SET(iPropCleanedupTriggeredBS, iProp)
			
			SCRIPT_TIMER timerToUse
			
			IF IS_LOCAL_PLAYER_MULTIRULE_TIMER_RUNNING()
				timerToUse = MC_serverBD_3.tdMultiObjectiveLimitTimer[MC_playerBD[iPartToUse].iteam]
			ELIF IS_LOCAL_PLAYER_OBJECTIVE_TIMER_RUNNING()
				timerToUse = MC_serverBD_3.tdObjectiveLimitTimer[MC_playerBD[iPartToUse].iteam]
			ENDIF
			
			IF HAS_NET_TIMER_STARTED(timerToUse)
				IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(timerToUse) > ((g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iForceCleanupPropAfterTime*1000) - 2500)
					PRINTLN("[LM][PROCESS_PROP_EVERY_FRAME] - Beginning the loop to assign this prop to every frame loop. iProp: ", iProp)
					INT iPropEF = 0
					FOR iPropEF = 0 TO FMMC_MAX_NUM_FADEOUT_PROPS_EVERY_FRAME-1
						IF iPropFadeoutEveryFrameIndex[iPropEF] = -1
							iPropFadeoutEveryFrameIndex[iPropEF] = iProp
							iFlashToggle[iPropEF] = 1
							iFlashToggleTime[iPropEF] = (GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(timerToUse)/1000)							
							FMMC_SET_LONG_BIT(iPropCleanedupTriggeredBS, iProp)	
							IF DOES_ENTITY_EXIST(oiProps[iProp])
								SET_ENTITY_ALPHA(oiProps[iProp], 150, FALSE)
							ENDIF
							PRINTLN("[LM][PROCESS_PROP_EVERY_FRAME] - ciFMMC_PROP_Cleanup_Triggered has been set on iProp: ", iProp)
							BREAKLOOP
						ENDIF
					ENDFOR
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropBitSet2,  ciFMMC_PROP2_BlipAsSafeProp)
		iSafeBlippedProp = iProp
	ENDIF
	
	IF DOES_ENTITY_EXIST(oiProps[iProp])
		IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iColourChange > -1
			INT iTeam = MC_PlayerBD[iPartToUse].iteam
			INT iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]
			IF iRule > -1 AND iRule < FMMC_MAX_RULES
				INT iColourToUse = -1
				
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iColourChangeOnRuleBS, iRule)
					iColourToUse = g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iColourChange
				ELSE 
					iColourToUse = g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropColour
				ENDIF
				IF iColourToUse > -1
					BOOL bReady = FALSE
					IF g_FMMC_STRUCT.iPropColourChangeDelay = 0
						bReady = TRUE
					ELSE
						IF NOT HAS_NET_TIMER_STARTED(tdPropColourChangeDelayTimer)
						AND GET_OBJECT_TINT_INDEX(oiProps[iProp]) != iColourToUse
							PRINTLN("[LM][PROCESS_PROP_EVERY_FRAME] - Starting the Prop Colour Change Delay via iProp: ", iProp)
							START_NET_TIMER(tdPropColourChangeDelayTimer)
						ENDIF
						
						IF HAS_NET_TIMER_STARTED(tdPropColourChangeDelayTimer)
						AND HAS_NET_TIMER_EXPIRED_READ_ONLY(tdPropColourChangeDelayTimer, (g_FMMC_STRUCT.iPropColourChangeDelay*1000))
							bReady = TRUE
						ENDIF
					ENDIF
					
					IF bReady = TRUE
						IF GET_OBJECT_TINT_INDEX(oiProps[iProp]) != iColourToUse
							PRINTLN("[LM][PROCESS_PROP_EVERY_FRAME] - Applying the Colour change ",iColourToUse," to iProp: ", iProp)
							SET_OBJECT_TINT_INDEX(oiProps[iProp], iColourToUse)
							RESET_NET_TIMER(tdPropColourChangeDelayTimer)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_THIS_MODEL_A_TRAP_PRESSURE_PAD(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].mn)
			PROCESS_ARENA_TRAP_PRESSUREPAD(oiProps[iProp], iProp, MC_ServerBD_2.sTrapInfo_Host, sTrapInfo_Local)
		ENDIF
		
		MAINTAIN_PLACED_PROPS_PARTICLE_EFFECTS(iProp, oiProps[iProp])
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].mn =  IND_PROP_FIREWORK_01
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropBitSet2, ciFMMC_PROP2_UsePlacedFireworkZone)
			AND NOT FMMC_IS_LONG_BIT_SET(iCreatedFireworkBitset, iProp)
				FLOAT fDist
				fDist = VDIST2(GET_ENTITY_COORDS(LocalPlayerPed), g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].vFireworkTriggerPos)				
				IF fDist <= POW(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].fFireworkTriggerSize, 2)
				AND ((MC_playerBD[iLocalPart].iteam = g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iFireworkTeam) OR (g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iFireworkTeam = -1))
					IF CREATE_FIREWORK_FX(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iAlarmDelay, oiProps[iProp], g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iAlarmSound)
						FMMC_SET_LONG_BIT(iCreatedFireworkBitset, iProp)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	PROCESS_LOCH_SANTOS_MONSTER_EVERY_FRAME(iProp)
	
ENDPROC	


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Shared DynoProp Processing --------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Functions that can be used in both the staggered and every-frame dynoprop loops.  --------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC CLEANUP_DYNOPROP_EARLY(FMMC_DYNOPROP_STATE& sDynopropState)

	PRINTLN("[Dynoprops][Dynoprop ", sDynopropState.iIndex, "] CLEANUP_DYNOPROP_EARLY")

	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[sDynopropState.iIndex].iDynoPropBitset,ciFMMC_DYNOPROP_IgnoreVisCheckCleanup)
		IF NOT sDynopropState.bHaveControlOfDynoprop
			NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(sDynopropState.niIndex)
		ELSE
			START_ENTITY_PTFX_AT_COORDS(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[sDynopropState.iIndex].sEntityDespawnPTFX, GET_FMMC_DYNOPROP_COORDS(sDynopropState), GET_ENTITY_ROTATION(sDynopropState.objIndex))
			DELETE_NET_ID(sDynopropState.niIndex)
			PRINTLN("[RCC MISSION][Dynoprops][Dynoprop ", sDynopropState.iIndex, "] CLEANUP_DYNOPROP_EARLY | DynoProp getting cleaned up early - delete, objective: ", g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[sDynopropState.iIndex].iCleanupObjective, " team: ", g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[sDynopropState.iIndex].iCleanupTeam)
		ENDIF
	ELSE
		IF sDynopropState.bHaveControlOfDynoprop
			SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(sDynopropState.objIndex, FALSE)
		ENDIF
		
		CLEANUP_NET_ID(sDynopropState.niIndex)
		PRINTLN("[RCC MISSION][Dynoprops][Dynoprop ", sDynopropState.iIndex, "] CLEANUP_DYNOPROP_EARLY | DynoProp getting cleaned up early - no longer needed, objective: ", g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[sDynopropState.iIndex].iCleanupObjective, " team: ", g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[sDynopropState.iIndex].iCleanupTeam)
	ENDIF
ENDPROC

PROC CREATE_BLIP_FOR_DYNO_PROP(FMMC_DYNOPROP_STATE& sDynopropState)

	CREATE_BLIP_WITH_CREATOR_SETTINGS(
		FMMC_BLIP_DYNO_PROP, 
		biDynoPropBlips[sDynopropState.iIndex], 
		g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[sDynopropState.iIndex].sDynoPropBlipStruct, 
		sMissionDynoPropsLocalVars[sDynopropState.iIndex].sBlipRuntimeVars,
		sDynopropState.objIndex, 
		sDynopropState.iIndex, 
		EMPTY_VEC(), 
		g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[sDynopropState.iIndex].iAggroIndexBS_Entity_DynoProp
	)
	
	IF NOT DOES_BLIP_EXIST(biDynoPropBlips[sDynopropState.iIndex])
		EXIT
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[sDynopropState.iIndex].sDynoPropElectronicData.sCCTVData.iCCTVBS, ciCCTV_BS_OverrideBlipColour)
	
		INT iDefaultColour
		IF DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_CCTV_CAMERA(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[sDynopropState.iIndex].sDynoPropElectronicData.sCCTVData)
			iDefaultColour = BLIP_COLOUR_WHITE
		ELSE
			iDefaultColour = BLIP_COLOUR_RED
		ENDIF
		SET_BLIP_COLOUR(biDynoPropBlips[sDynopropState.iIndex], iDefaultColour)
	ENDIF

	SET_BLIP_MARKER_LONG_DISTANCE(biDynoPropBlips[sDynopropState.iIndex], FALSE)
	SET_BLIP_AS_SHORT_RANGE(biDynoPropBlips[sDynopropState.iIndex], TRUE)
	
	PRINTLN("[Dynoprops][Dynoprop ", sDynopropState.iIndex, "] PROCESS_DYNOPROP_BLIP - Blip creation finished for dyno prop: ", sDynopropState.iIndex)
ENDPROC

PROC PROCESS_DYNOPROP_BLIP(FMMC_DYNOPROP_STATE& sDynopropState)
	
	IF NOT SHOULD_DYNOPROP_BE_BLIPPED(sDynopropState)
		REMOVE_DYNOPROP_BLIP(sDynopropState.iIndex)
		EXIT
	ENDIF
	
	IF NOT DOES_BLIP_EXIST(biDynoPropBlips[sDynopropState.iIndex])
		CREATE_BLIP_FOR_DYNO_PROP(sDynopropState)
	ENDIF
ENDPROC

PROC PROCESS_STAGGERED_DYNOPROP_BLIP(FMMC_DYNOPROP_STATE& sDynopropState)
	
	IF NOT sDynopropState.bDynopropExists
		REMOVE_DYNOPROP_BLIP(sDynopropState.iIndex)
		EXIT
	ENDIF
	
	PROCESS_ENTITY_BLIP(biDynoPropBlips[sDynopropState.iIndex], g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[sDynopropState.iIndex].sDynoPropBlipStruct, GET_FMMC_DYNOPROP_COORDS(sDynopropState), g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[sDynopropState.iIndex].iAggroIndexBS_Entity_DynoProp)
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: DynoProp Every Frame Processing
// ##### Description: Client and Server DynoProp Every Frame Processing
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


FUNC BOOL RESPAWN_MISSION_DYNO_PROP(FMMC_DYNOPROP_STATE& sDynopropState)
	
	INT iDynoprop = sDynopropState.iIndex
	
	IF GET_DYNOPROP_MODEL_TO_USE(iDynoprop) = DUMMY_MODEL_FOR_SCRIPT
		PRINTLN("[MCRespawnDynoProp: ", iDynoprop, "][Dynoprops] RESPAWN_MISSION_DYNO_PROP - MODEL IS INVALID")
		RETURN FALSE
	ENDIF
	
	REQUEST_MODEL(GET_DYNOPROP_MODEL_TO_USE(iDynoprop))				
	IF NOT HAS_MODEL_LOADED(GET_DYNOPROP_MODEL_TO_USE(iDynoprop))
		PRINTLN("[MCRespawnDynoProp: ", iDynoprop, "][Dynoprops] RESPAWN_MISSION_DYNO_PROP - iDynoprop = ", iDynoprop, " Waiting for Prop to load. Returning FALSE.")
		RETURN FALSE
	ENDIF
	
	IF NOT CAN_REGISTER_MISSION_OBJECTS(1)
		PRINTLN("[MCRespawnDynoProp: ", iDynoprop, "][Dynoprops] RESPAWN_MISSION_DYNO_PROP - iDynoprop = ", iDynoprop, " Cannot register any more objects. Returning FALSE.")
		RETURN FALSE
	ENDIF
	
	IF FMMC_CREATE_NET_OBJ(MC_serverBD_1.sFMMC_SBD.niNetworkedObject[GET_DYNOPROP_NET_ID_INDEX(iDynoprop)], 
					   GET_DYNOPROP_MODEL_TO_USE(iDynoprop), 
					   GET_DYNO_PROP_SPAWN_LOCATION(iDynoprop), TRUE, TRUE,	TRUE, DEFAULT, DEFAULT, TRUE, DEFAULT, TRUE)
					   
		PRINTLN("[MCRespawnDynoProp: ", iDynoprop, "][Dynoprops] RESPAWN_MISSION_DYNO_PROP - iDynoprop = ", iDynoprop, " Has respawned.")			
		
		sDynopropState.niIndex = MC_serverBD_1.sFMMC_SBD.niNetworkedObject[GET_DYNOPROP_NET_ID_INDEX(iDynoprop)]
		
		MC_SET_UP_DYNOPROP(NET_TO_OBJ(sDynopropState.niIndex), 
								    iDynoprop, 
								    GET_DYNOPROP_MODEL_TO_USE(iDynoprop), 
									TRUE)
		
		SET_MODEL_AS_NO_LONGER_NEEDED(GET_DYNOPROP_MODEL_TO_USE(iDynoprop))
		
		FMMC_CLEAR_LONG_BIT(iDroppedDynoPropBS, iDynoprop)
	ENDIF
	
	START_ENTITY_PTFX_AT_COORDS(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoprop].sEntitySpawnPTFX, GET_FMMC_DYNOPROP_COORDS(sDynopropState), GET_ENTITY_COORDS(sDynopropState.objIndex))
	
	RETURN TRUE
ENDFUNC

PROC PROCESS_DYNO_PROP_RESPAWNING(FMMC_DYNOPROP_STATE& sDynopropState)
	
	IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_FIRST_UPDATE_DONE)
		EXIT
	ENDIF
	
	INT iDynoProp = sDynopropState.iIndex
	
	IF SHOULD_ENTITY_RESPAWN_THIS_RULE(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].iRespawnOnRuleChangeBS, g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].iRespawnOnRule, -1)
		INT iTeam
		FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
			IF MC_serverBD_4.iCurrentHighestPriority[iTeam] > 0
				IF (MC_serverBD_4.iCurrentHighestPriority[iTeam] > 0
				AND IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam))
				OR IS_BIT_SET(MC_serverBD.iServerBitSet5, SBBOOL5_RETRY_CURRENT_RULE_TEAM_0 + iTeam)
					IF SHOULD_ENTITY_RESPAWN_THIS_RULE(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].iRespawnOnRuleChangeBS, g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].iRespawnOnRule, iTeam, FALSE)
						SET_BIT(MC_serverBD.iDynoPropCleanup_NeedOwnershipBS, iDynoProp)
						SET_BIT(MC_serverBD.iDynoPropShouldRespawnNowBS, iDynoProp)
						PRINTLN("[MCRespawnDynoProp: ", iDynoProp, "][Dynoprops] PROCESS_DYNO_PROP_RESPAWNING - | Cleaning up dynoprop to respawn on rule change ", iDynoProp)
						BREAKLOOP
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	
	IF sDynopropState.bDynopropExists
		EXIT
	ENDIF

	IF SHOULD_DYNOPROP_SPAWN_NOW(sDynopropState) 
		IF RESPAWN_MISSION_DYNO_PROP(sDynopropState)
			CLEAR_BIT(MC_serverBD.iDynoPropShouldRespawnNowBS, iDynoprop)
			CLEAR_BIT(iDynoShouldRespawnNowBS, iDynoProp)
		ENDIF
	ENDIF
	
	PROCESS_DYNO_PROP_RESPAWNING_SPECIAL(sDynopropState)
	
ENDPROC

PROC PROCESS_DYNOPROP_EXPLODE_ON_TOUCH(FMMC_DYNOPROP_STATE& sDynopropState)

	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[sDynopropState.iIndex].iDynopropBitset, ciFMMC_DYNOPROP_ExplodeOnTouch)
	OR NOT sDynopropState.bDynopropAlive
	OR NOT DOES_MODEL_EXPLODE(sDynopropState.mn)
		EXIT
	ENDIF
	
	INT iPlayerIndex = GET_NEAREST_PLAYER_TO_ENTITY(sDynopropState.objIndex)			
	PLAYER_INDEX player = INT_TO_PLAYERINDEX(iPlayerIndex)
	PED_INDEX ped = GET_PLAYER_PED(player)
	
	ASSERTLN("[FMMC2020] PROCESS_DYNOPROP_EXPLODE_ON_TOUCH needs to be optimised!")
	
	IF IS_NET_PLAYER_OK(player)
		IF MC_IS_ENTITY_TOUCHING_ENTITY(sDynopropState.objIndex, ped, 15.0)
			// This is temporary as it is not networked 
			SET_ENTITY_INVINCIBLE(sDynopropState.objIndex, FALSE)
			SET_ENTITY_PROOFS(sDynopropState.objIndex, FALSE, FALSE, FALSE, FALSE, FALSE)
			ADD_EXPLOSION(GET_FMMC_DYNOPROP_COORDS(sDynopropState), EXP_TAG_BARREL, 2.0)
		ENDIF		
	ENDIF
ENDPROC

FUNC BOOL SHOULD_DYNOPROP_BREAK_ON_GROUND(FMMC_DYNOPROP_STATE& sDynopropState)
	
	IF g_FMMC_STRUCT.sPlayerAbilities.sHeavyLoadout.iContainerIndex = sDynopropState.iIndex
		// Break the Heavy Loadout crate on collision with the ground (if it doesn't break naturally)
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

PROC PROCESS_DYNOPROP_BREAK_ON_GROUND(FMMC_DYNOPROP_STATE& sDynopropState)
	
	IF NOT SHOULD_DYNOPROP_BREAK_ON_GROUND(sDynopropState)
		EXIT
	ENDIF
	
	IF IS_BIT_SET(iBreakOnFloorDynopropBrokenBS, sDynopropState.iIndex)
		// Already broken!
		EXIT
	ENDIF
	
	VECTOR vVelocity = FMMC_GET_ENTITY_CURRENT_VELOCITY(sDynopropState.objIndex)
	
	IF NOT IS_BIT_SET(iBreakOnFloorDynopropInMotionBS, sDynopropState.iIndex)
		// Wait here until the dynoprop has started moving. We only want to break the dynoprop if it starts falling and then stops
		IF vVelocity.z < -2.5
			SET_BIT(iBreakOnFloorDynopropInMotionBS, sDynopropState.iIndex)
			PRINTLN("[Dynoprops][Dynoprop ", sDynopropState.iIndex, "] PROCESS_DYNOPROP_BREAK_ON_GROUND | Setting iBreakOnFloorDynopropInMotionBS now! vVelocity: ", vVelocity)
		ENDIF
	
		EXIT
	ENDIF
	
	IF NOT IS_ENTITY_IN_AIR(sDynopropState.objIndex)
	AND ABSF(vVelocity.z) < 0.25
		
		// The dynoprop has hit the floor - frag it now
		IF sDynopropState.bHaveControlOfDynoprop
			PRINTLN("[Dynoprops][Dynoprop ", sDynopropState.iIndex, "] PROCESS_DYNOPROP_BREAK_ON_GROUND | Breaking dynoprop now because it's no longer in the air!")
			FRAG_ENTITY_COMPLETELY(sDynopropState.objIndex, FALSE)
			
		ELSE
			PRINTLN("[Dynoprops][Dynoprop ", sDynopropState.iIndex, "] PROCESS_DYNOPROP_BREAK_ON_GROUND | Dynoprop has stopped falling but we're not the participant who'll break it")
			
		ENDIF
		
		SET_BIT(iBreakOnFloorDynopropBrokenBS, sDynopropState.iIndex)
	ENDIF
	
ENDPROC

PROC PROCESS_DYNOPROP_KAMIKAZI_INVINCIBILITY(FMMC_DYNOPROP_STATE& sDynopropState)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[sDynopropState.iIndex].iDynopropBitset, ciFMMC_DYNOPROP_KamakaziInvincibility)
	OR NOT sDynopropState.bDynopropAlive
		EXIT
	ENDIF
		
	INT iPlayer = GET_NEAREST_PLAYER_TO_ENTITY_ON_TEAM(sDynopropState.objIndex, g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[sDynopropState.iIndex].iPropKamaTeam)
	PLAYER_INDEX ClosestPlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPlayer))
	PED_INDEX ClosestPlayerPedId = GET_PLAYER_PED(ClosestPlayerId)
	
	ASSERTLN("[FMMC2020] PROCESS_DYNOPROP_KAMIKAZI_INVINCIBILITY needs to be optimised!")
	
	IF VDIST2(GET_ENTITY_COORDS(sDynopropState.objIndex),GET_ENTITY_COORDS(ClosestPlayerPedId)) < 40
		SET_ENTITY_INVINCIBLE(sDynopropState.objIndex, FALSE)
		SET_ENTITY_PROOFS(sDynopropState.objIndex, FALSE, FALSE, FALSE, FALSE, FALSE)
		SET_DISABLE_FRAG_DAMAGE(sDynopropState.objIndex, FALSE)
	ELSE
		SET_ENTITY_INVINCIBLE(sDynopropState.objIndex, TRUE)
		SET_ENTITY_PROOFS(sDynopropState.objIndex, TRUE, TRUE, TRUE, TRUE, TRUE)
		SET_DISABLE_FRAG_DAMAGE(sDynopropState.objIndex, TRUE)
	ENDIF
	
ENDPROC

PROC DESTROY_DYNOPROP_IN_ONE_HIT(OBJECT_INDEX oiDynoProp, ENTITY_INDEX eiDamagerIndex)
	
	IF NOT DOES_ENTITY_EXIST(oiDynoProp)
		EXIT
	ENDIF
	
	FLOAT fExplosionScalar = 1.0
	MODEL_NAMES mnDynoPropModel = GET_ENTITY_MODEL(oiDynoProp)
	
	SET_ENTITY_HEALTH(oiDynoProp, 0, eiDamagerIndex)
		
	// This is temporary as it is not networked 
	SET_ENTITY_INVINCIBLE(oiDynoProp, FALSE)
	SET_ENTITY_PROOFS(oiDynoProp, FALSE, FALSE, FALSE, FALSE, FALSE)
	
	FRAG_ENTITY_COMPLETELY(oiDynoProp, TRUE)

	IF mnDynoPropModel = INT_TO_ENUM(MODEL_NAMES, HASH("imp_prop_groupbarrel_01"))
	OR mnDynoPropModel = INT_TO_ENUM(MODEL_NAMES, HASH("imp_prop_groupbarrel_02"))
	OR mnDynoPropModel = INT_TO_ENUM(MODEL_NAMES, HASH("imp_prop_groupbarrel_03"))
	OR mnDynoPropModel = INT_TO_ENUM(MODEL_NAMES, HASH("sum_prop_ac_aircon_02a"))
		fExplosionScalar = 2.0
	ENDIF
	
	IF DOES_MODEL_EXPLODE(mnDynoPropModel)
		ADD_EXPLOSION(GET_ENTITY_COORDS(oiDynoProp), EXP_TAG_BARREL, 0.5 * fExplosionScalar)
	ENDIF
ENDPROC


PROC PROCESS_DYNOPROP_STINGER_FUNCTIONALITY(FMMC_DYNOPROP_STATE& sDynopropState)

	IF sDynopropState.mn != p_ld_stinger_s
		EXIT
	ENDIF
	
	VECTOR vPlayerVehicleCoords
	VEHICLE_INDEX viPlayerVehicle
	
	IF DOES_ENTITY_EXIST(LocalPlayerPed)
	AND IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		viPlayerVehicle = GET_VEHICLE_PED_IS_USING(LocalPlayerPed)
		
		IF DOES_ENTITY_EXIST(viPlayerVehicle)
		
			IF IS_VEHICLE_TYRE_BURST(viPlayerVehicle, SC_WHEEL_CAR_FRONT_LEFT)
			AND IS_VEHICLE_TYRE_BURST(viPlayerVehicle, SC_WHEEL_CAR_FRONT_RIGHT)
				//Forget doing the rest of this if the player's tyres are already popped
				EXIT
			ENDIF
				
			IF NETWORK_HAS_CONTROL_OF_ENTITY(viPlayerVehicle)
				
				//Adds a bit of an offset to make it more like the distance is being measured from the front tyres rather than the middle of the car
				VECTOR vRoughFrontTyreOffset = GET_ENTITY_FORWARD_VECTOR(viPlayerVehicle)
				FLOAT fRoughFrontTyreOffsetDistance = 1.8
				
				vRoughFrontTyreOffset.x *= fRoughFrontTyreOffsetDistance
				vRoughFrontTyreOffset.y *= fRoughFrontTyreOffsetDistance
				
				vPlayerVehicleCoords = GET_ENTITY_COORDS(viPlayerVehicle) + vRoughFrontTyreOffset
				
				FLOAT fPopDistance = 3.5
					
				IF VDIST2(vPlayerVehicleCoords, GET_FMMC_DYNOPROP_COORDS(sDynopropState)) <= fPopDistance
					PRINTLN("[Dynoprops][Dynoprop ", sDynopropState.iIndex, "] PROCESS_DYNOPROP_STINGER_FUNCTIONALITY | Popped the wheels of the local player")
					
					SET_VEHICLE_TYRES_CAN_BURST(viPlayerVehicle, TRUE)
					
					SET_VEHICLE_TYRE_BURST(viPlayerVehicle, SC_WHEEL_CAR_FRONT_LEFT, TRUE)
					SET_VEHICLE_TYRE_BURST(viPlayerVehicle, SC_WHEEL_CAR_FRONT_RIGHT, TRUE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_DYNOPROP_ARENA_TRAPS(FMMC_DYNOPROP_STATE& sDynopropState)
	
	IF NOT CAN_PROCESS_TRAPS()
		EXIT
	ENDIF
	
	IF NOT IS_THIS_MODEL_A_TRAP_DYNO_PROP(sDynopropState.mn)
		EXIT
	ENDIF
	
	IF sDynopropState.bDynopropExists
		PROCESS_ARENA_TRAPS(sDynopropState.objIndex, sDynopropState.iIndex, TRUE, MC_serverBD_2.sTrapInfo_Host, sTrapInfo_Local)
		SET_BIT(g_iCurrentSpectatorTrapSpawned, sDynopropState.iIndex)
	ELSE	
		IF IS_BIT_SET(g_iCurrentSpectatorTrapSpawned, sDynopropState.iIndex)
			CLEANUP_TRAP(sDynopropState.iIndex, MC_serverBD_2.sTrapInfo_Host, sTrapInfo_Local)
			CLEAR_BIT(g_iCurrentSpectatorTrapSpawned, sDynopropState.iIndex)
		ENDIF
	ENDIF
	
	//[FMMC2020] Trap respawning currently not supported in the 2020 controller (potentially never needed again, could just be done with a generic dynoprop system)
	//PROCESS_TRAP_RESPAWNING(sDynopropState.iIndex, MC_serverBD_1.sFMMC_SBD.niDynoProps, sTrapInfo_Local, MC_serverBD_2.sTrapInfo_Host)
ENDPROC

PROC FILL_ELECTRONIC_PARAMS_FROM_DYNOPROP(FMMC_DYNOPROP_STATE& sDynopropState, ELECTRONIC_PARAMS& sElectronicParams)
	sElectronicParams.iIndex = sDynopropState.iIndex
	sElectronicParams.oiElectronic = sDynopropState.objIndex
	sElectronicParams.biElectronicBlip = biDynoPropBlips[sDynopropState.iIndex]
	IF sDynopropState.bDynopropExists
		sElectronicParams.vElectronicCoords = GET_FMMC_DYNOPROP_COORDS(sDynopropState)
	ENDIF
	
	sElectronicParams.eEntityType = FMMC_ELECTRONIC_ENTITY_TYPE__DYNOPROP
	sElectronicParams.iCamIndex = iDynopropCachedCamIndexes[sDynopropState.iIndex]
	
ENDPROC

PROC PROCESS_DYNOPROPS_PRE_EVERY_FRAME()
	
	#IF IS_DEBUG_BUILD
	iTempProfilerActiveDynoProps = 0
	#ENDIF	
	
ENDPROC

PROC PROCESS_DYNOPROPS_POST_EVERY_FRAME()
	
	#IF IS_DEBUG_BUILD
	iProfilerActiveDynoProps = iTempProfilerActiveDynoProps
	#ENDIF	
	
ENDPROC		

PROC PROCESS_DYNOPROP_EVERY_FRAME_CLIENT(INT iDynoProp)
	
	FMMC_DYNOPROP_STATE sDynopropState
	FILL_FMMC_DYNOPROP_STATE_STRUCT(sDynopropState, iDynoprop)
	FILL_ELECTRONIC_PARAMS_FROM_DYNOPROP(sDynopropState, sDynopropState.sDynopropElectronicParams)
	
	PROCESS_DYNOPROP_BLIP(sDynopropState)
	
	PROCESS_PARTICLE_FX_OVERRIDE_DYNOPROP(sDynopropState)
	
	PROCESS_DYNOPROP_ARENA_TRAPS(sDynopropState)
	
	PROCESS_REMOVING_TARGETTABLE_FROM_DYNO_PROP(sDynopropState.objIndex)
	
	PROCESS_CCTV_MOVING_CAMERAS(sDynopropState.sDynopropElectronicParams, g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].sDynoPropElectronicData.sCCTVData, iDynopropCachedCamIndexes[iDynoProp], sDynopropState.bDynopropExists, sDynopropState.bDynopropAlive, sDynopropState.bHaveControlOfDynoprop, g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].iAggroIndexBS_Entity_DynoProp)
	
	IF sDynopropState.bDynopropExists
	
		#IF IS_DEBUG_BUILD
		iTempProfilerActiveDynoProps++
		#ENDIF	
		
		IF sDynopropState.bHaveControlOfDynoprop
			PROCESS_DYNOPROP_EXPLODE_ON_TOUCH(sDynopropState)
			
			PROCESS_DYNOPROP_KAMIKAZI_INVINCIBILITY(sDynopropState)
			
			PROCESS_DYNOPROP_STINGER_FUNCTIONALITY(sDynopropState)
		ENDIF
		
		IF sDynopropState.bHaveControlOfDynoprop
			BOOL bCanAttach = TRUE
			
			IF sInteractWithVars.oiInteractWith_LinkedDynoprop = sDynopropState.objIndex
				bCanAttach = FALSE
			ENDIF
			
			PROCESS_TRAIN_ATTACHMENT(sDynopropState.objIndex, g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[sDynopropState.iIndex].sDynopropTrainAttachmentData, bCanAttach)
		ENDIF
		
		PROCESS_TASERED_ENTITY(sDynoPropTaserVars, g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoprop].sDynoPropElectronicData, sDynopropState.sDynopropElectronicParams)
		
		PROCESS_DYNOPROP_BREAK_ON_GROUND(sDynopropState)
	ENDIF
ENDPROC	

PROC PROCESS_DYNOPROP_EVERY_FRAME_SERVER(INT iDynoprop)
	
	IF NOT bIsLocalPlayerHost
		EXIT
	ENDIF
	
	FMMC_DYNOPROP_STATE sDynopropState
	FILL_FMMC_DYNOPROP_STATE_STRUCT(sDynopropState, iDynoprop)
	
	PROCESS_DYNO_PROP_RESPAWNING(sDynopropState)
	 
	IF sDynopropState.bDynopropExists
		IF IS_MODEL_SPECIAL_RACES_DESTRUCTIBLE_PROP(sDynopropState.mn)
			IF CHECK_ALL_PLAYERS_EXCEED_DISTANCE_FROM_COORDS(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[sDynopropState.iIndex].vPos, ciSPECIAL_RACES_DESTRUCTIBLE_PROP_RESPAWN_DISTANCE)
				IF IS_OBJECT_BROKEN_AND_VISIBLE(sDynopropState.objIndex)
					DELETE_NET_ID(sDynopropState.niIndex)
					CLEAR_AREA(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[sDynopropState.iIndex].vPos, 10.0, FALSE)
				ENDIF
			ENDIF
		ENDIF
		
		IF SHOULD_CLEANUP_DYNOPROP(sDynopropState)
			CLEANUP_DYNOPROP_EARLY(sDynopropState)
			EXIT
		ENDIF
		
		IF SHOULD_CLEANUP_DUE_TO_PLAYER_PROXIMITY(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[sDynopropState.iIndex].sProximitySpawningData, sDynopropState.iIndex, MC_serverBD_3.iProximitySpawning_DynoPropCleanupBS)
			CLEANUP_DYNOPROP_EARLY(sDynopropState)
			PRINTLN("[PROXSPAWN_SERVER][DynoProp: ", iDynoprop, "] PROCESS_DYNOPROP_EVERY_FRAME_SERVER - Cleaning up dynoprop due to proximity!")
			IF sDynopropState.bDynopropAlive
				SET_BIT(MC_serverBD.iDynoPropShouldRespawnNowBS, iDynoProp)
				PRINTLN("[PROXSPAWN_SERVER][DynoProp: ", iDynoprop, "] PROCESS_DYNOPROP_EVERY_FRAME_SERVER - Allowing Dynoprop to respawn.")
			ENDIF
			EXIT
		ENDIF
		
		IF IS_BIT_SET(MC_serverBD.iDynoPropCleanup_NeedOwnershipBS, sDynopropState.iIndex)
			IF NOT sDynopropState.bHaveControlOfDynoprop
				NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(sDynopropState.niIndex)
			ELSE
				PRINTLN("[RCC MISSION][Dynoprops][Dynoprop ", sDynopropState.iIndex, "] DynoProp getting cleaned up")
				DELETE_NET_ID(sDynopropState.niIndex)
			ENDIF
			
			CLEAR_BIT(MC_serverBD.iDynoPropCleanup_NeedOwnershipBS, iDynoprop)
		ENDIF		
	ENDIF
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: DynoProp Staggered Processing
// ##### Description: Client and Server DynoProp Staggered Processing
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

#IF IS_DEBUG_BUILD
PROC PROCESS_DYNOPROP_PROXIMITY_SPOOFING(INT iDynoProp)
	
	INT i
	FOR i = 0 TO ciProxSpawnDebug_MAX_PLAYERS - 1
		
		IF bProxSpawnDebug_PlayerOverrideUpdate[i]
			vProxSpawnDebug_PlayerOverride[i] = vLocalPlayerPosition
		ENDIF
		
		PROCESS_CLIENT_PROXIMITY_SPAWNING(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].sProximitySpawningData, g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].vPos, iDynoProp, sProxSpawnDebug_PlayerOverrideData[i].iProximitySpawning_DynoPropSpawnRangeBS, sProxSpawnDebug_PlayerOverrideData[i].iProximitySpawning_DynoPropCleanupRangeBS, vProxSpawnDebug_PlayerOverride[i] #IF IS_DEBUG_BUILD , "DynoProp" #ENDIF )
	
	ENDFOR
	
ENDPROC
#ENDIF

PROC PROCESS_DYNOPROP_STAGGERED_CLIENT(INT iDynoProp)

	FMMC_DYNOPROP_STATE sDynopropState
	FILL_FMMC_DYNOPROP_STATE_STRUCT(sDynopropState, iDynoprop)
	
	#IF IS_DEBUG_BUILD
	IF bProxSpawnDebug_EnablePlayerPositionSpoofing
		PROCESS_DYNOPROP_PROXIMITY_SPOOFING(iDynoProp)
	ELSE
	#ENDIF
		PROCESS_CLIENT_PROXIMITY_SPAWNING(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].sProximitySpawningData, g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].vPos, iDynoProp, MC_playerBD_1[iLocalPart].iProximitySpawning_DynoPropSpawnRangeBS, MC_playerBD_1[iLocalPart].iProximitySpawning_DynoPropCleanupRangeBS, vLocalPlayerPosition #IF IS_DEBUG_BUILD , "DynoProp" #ENDIF )
	#IF IS_DEBUG_BUILD
	ENDIF
	#ENDIF
	
	PROCESS_STAGGERED_DYNOPROP_BLIP(sDynopropState)
	
	PROCESS_SCRIPTED_FLAMMABLE_ENTITY(sDynopropState.objIndex, sDynopropState.mn, ptDynopropPTFX[sDynopropState.iIndex])
	
	IF sDynopropState.bDynopropExists
		PROCESS_ENTITY_INTERIOR(iDynoProp, sDynopropState.objIndex, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[sDynopropState.iIndex].iDynopropBitset, ciFMMC_DYNOPROP_InAnInterior), <<0,0,0>>, iDynopropHasBeenPlacedIntoInteriorBS)
	ENDIF
	
ENDPROC

//PROC PROCESS_DYNOPROP_STAGGERED_SERVER(INT iDynoProp)
//
//ENDPROC

