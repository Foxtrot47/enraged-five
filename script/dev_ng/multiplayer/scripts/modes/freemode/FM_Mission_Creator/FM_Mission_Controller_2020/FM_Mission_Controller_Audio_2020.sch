// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### System: GTA V Online Mission Controller - Audio -------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Script: FM_Mission_Controller_2020.sc -----------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: All logic for enemy gang backup called in against the player.  
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### This script was forked from FM_Mission_Controller.sc at the beginning of 2020		 																				  
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

USING "FM_Mission_Controller_Assets_2020.sch"



// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Init
// ##### Description: Functions for anything audio related done on script init
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_AUDIO_FLAG_INIT()
	//Setup the music flags for this mission:
	SET_AUDIO_FLAG("DisableFlightMusic", TRUE)
	SET_AUDIO_FLAG("WantedMusicDisabled", TRUE)
	SET_AUDIO_FLAG("AllowScoreAndRadio", FALSE)
	SET_AUDIO_FLAG("IsPlayerOnMissionForSpeech",TRUE)
ENDPROC

PROC PROCESS_CAR_PARK_RADIO_INIT()
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciDRIVEBY_SHOOTOUT_CAR_PARK_RADIO)
		EXIT
	ENDIF
	
	IF NOT IS_AUDIO_SCENE_ACTIVE("MP_Reduce_Score_For_Emitters_Scene")
		START_AUDIO_SCENE("MP_Reduce_Score_For_Emitters_Scene")
		PRINTLN("[RCC MISSION] START_AUDIO_SCENE = MP_Reduce_Score_For_Emitters_Scene")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("MP_POSITIONED_RADIO_MUTE_SCENE")
		SET_AUDIO_SCENE_VARIABLE("MP_POSITIONED_RADIO_MUTE_SCENE", "apply", 0)
	ENDIF
	
	SET_EMITTER_RADIO_STATION("SE_LR_Car_Park_Radio_01", "RADIO_09_HIPHOP_OLD")
	PRINTLN("[RCC MISSION] SET_EMITTER_RADIO_STATION = \"RADIO_09_HIPHOP_OLD\"")
	
	SET_STATIC_EMITTER_ENABLED("SE_LR_Car_Park_Radio_01", TRUE)

ENDPROC

PROC PROCESS_EMITTER_DISABLING()
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyNine, ciOptionsBS29_DisablePBMansionEmitters)
		SET_STATIC_EMITTER_ENABLED("SE_PB_MANSION_HOUSE", FALSE)
		SET_STATIC_EMITTER_ENABLED("SE_PB_MANSION_GROTTO", FALSE)
		PRINTLN("PROCESS_EMITTER_DISABLING - Disabling emitters for mansion")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyNine, ciOptionsBS29_DisableMusicStudioEmitters)
		SET_STATIC_EMITTER_ENABLED("SE_sf_dlc_studio_sec_studio_01", FALSE)
		SET_STATIC_EMITTER_ENABLED("SE_sf_dlc_studio_sec_studio_02", FALSE)
		SET_STATIC_EMITTER_ENABLED("SE_sf_dlc_studio_sec_lobby_01", FALSE)
		SET_STATIC_EMITTER_ENABLED("SE_sf_dlc_studio_sec_writers_01", FALSE)
		PRINTLN("PROCESS_EMITTER_DISABLING - Disabling emitters for music studio")
	ENDIF
		
ENDPROC

PROC MISSION_SPECIFIC_AUDIO_INIT()
	
	IF GET_FIXER_STORY_MISSION_FROM_ROOT_CONTENT_ID(g_FMMC_STRUCT.iRootContentIDHash) = FSM_HOOD_PASS_MISSION
		START_AUDIO_SCENE("DLC_Fixer_DL3_Hood_Pass_Scene")
	ELIF GET_FIXER_STORY_MISSION_FROM_ROOT_CONTENT_ID(g_FMMC_STRUCT.iRootContentIDHash) = FSM_PARTY_PROMOTER_MISSION
		SET_BIT(iMissionSpecificScriptedAudio, ciMissionSpecificScriptedAudio_Party_Promoter)
		PRINTLN("MISSION_SPECIFIC_AUDIO_INIT - Setting ciMissionSpecificScriptedAudio_Party_Promoter")
	ELIF GET_FIXER_STORY_MISSION_FROM_ROOT_CONTENT_ID(g_FMMC_STRUCT.iRootContentIDHash) = FSM_BILLIONAIRE_GAMES_MISSION
		SET_BIT(iMissionSpecificScriptedAudio, ciMissionSpecificScriptedAudio_Billionaire)
		PRINTLN("MISSION_SPECIFIC_AUDIO_INIT - Setting ciMissionSpecificScriptedAudio_Billionaire")
	ELIF GET_FIXER_STORY_MISSION_FROM_ROOT_CONTENT_ID(g_FMMC_STRUCT.iRootContentIDHash) = FSM_FINALE
		SET_BIT(iMissionSpecificScriptedAudio, ciMissionSpecificScriptedAudio_DontFuckWithDre)
		PRINTLN("MISSION_SPECIFIC_AUDIO_INIT - Setting ciMissionSpecificScriptedAudio_DontFuckWithDre")
	ELIF GET_FIXER_SHORT_TRIP_FROM_ROOT_CONTENT_ID(g_FMMC_STRUCT.iRootContentIDHash) = FST_SHORT_TRIP_1
		SET_BIT(iMissionSpecificScriptedAudio, ciMissionSpecificScriptedAudio_SeedCapital)
		PRINTLN("MISSION_SPECIFIC_AUDIO_INIT - Setting ciMissionSpecificScriptedAudio_SeedCapital")
	ELIF GET_ULP_MISSION_FROM_ROOT_CONTENT_ID_HASH(g_FMMC_STRUCT.iRootContentIDHash) = ciULP_MISSION_2_COUNTERINTELLIGENCE
		SET_BIT(iMissionSpecificScriptedAudio, ciMissionSpecificScriptedAudio_RogueDrones)
		PRINTLN("MISSION_SPECIFIC_AUDIO_INIT - Setting ciMissionSpecificScriptedAudio_RogueDrones")
	ENDIF
	
ENDPROC

PROC PROCESS_AUDIO_INIT()
	
	PROCESS_AUDIO_FLAG_INIT()
	
	PROCESS_CAR_PARK_RADIO_INIT()
	
	PROCESS_EMITTER_DISABLING()
	
	MISSION_SPECIFIC_AUDIO_INIT()
	
ENDPROC


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: AUDIO MIX  ------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Functions relating to the player target of the gang chase, and deciding where to spawn the gang backup -----------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_PLANE_NOISE()
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciLOUD_PLANES)
		IF NOT IS_AUDIO_SCENE_ACTIVE("Speed_Race_Airport_Scene")
			START_AUDIO_SCENE("Speed_Race_Airport_Scene")
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_AUDIO_MIX()

	INT iTeam = MC_playerBD[iPartToUse].iTeam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	PROCESS_PLANE_NOISE()
	
	IF NOT IS_CUTSCENE_PLAYING()
		IF iRule < FMMC_MAX_RULES AND iRule >= 0
			
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFifteen[iRule], ciBS_RULE15_DISABLE_ALL_MUSIC)
				STOP_ANY_CURRENTLY_PLAYING_AUDIO_MIX()
				iLocalAudioMixPriority = -1
				EXIT
			ENDIF
			
			IF iLocalAudioMixPriority != iRule
		
				PRINTLN("[Mission_Music] - New rule, checking audio mixes, Rule: ", iRule)
				
				STRING sAudioMix = GET_AUDIO_SCENE_FROM_MIX_INDEX(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iAudioMix[iRule])										
				IF NOT ARE_STRINGS_EQUAL("", sAudioMix)
					PRINTLN("[Mission_Music] - Audio Mix for rule is ", sAudioMix)
					IF NOT IS_AUDIO_SCENE_ACTIVE(sAudioMix)
						STOP_ANY_CURRENTLY_PLAYING_AUDIO_MIX()
						START_AUDIO_SCENE(sAudioMix)
						iLocalCurrentAudioMix = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iAudioMix[iRule]
						PRINTLN("[Mission_Music] - START_AUDIO_SCENE : ", sAudioMix,": ",iLocalCurrentAudioMix)
					ENDIF
				ELSE
					STOP_ANY_CURRENTLY_PLAYING_AUDIO_MIX()
				ENDIF
				
				iLocalAudioMixPriority = iRule
				CLEAR_BIT(iLocalBoolCheck9, LBOOL9_MIDPOINT_AUDIO_MIX_PROCESSED)

			ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMidPointAudioMix[iRule] >= 0
			AND IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iTeam],iRule)
			AND NOT IS_BIT_SET(iLocalBoolCheck9, LBOOL9_MIDPOINT_AUDIO_MIX_PROCESSED)
			
				PRINTLN("[Mission_Music] - New midpoint, checking audio mixes, Rule: ",iRule)
				
				STRING sAudioMix = GET_AUDIO_SCENE_FROM_MIX_INDEX(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMidPointAudioMix[iRule])		
					
				PRINTLN("[Mission_Music] - Audio Mix for rule midpoint is ",sAudioMix)
				
				IF NOT ARE_STRINGS_EQUAL("", sAudioMix)
					IF NOT IS_AUDIO_SCENE_ACTIVE(sAudioMix)
						STOP_ANY_CURRENTLY_PLAYING_AUDIO_MIX()
						START_AUDIO_SCENE(sAudioMix)
						iLocalCurrentAudioMix = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMidPointAudioMix[iRule]
						PRINTLN("[Mission_Music] - START_AUDIO_SCENE : ", sAudioMix,": ",iLocalCurrentAudioMix)
					ENDIF
				ELSE
					STOP_ANY_CURRENTLY_PLAYING_AUDIO_MIX()
				ENDIF

				SET_BIT(iLocalBoolCheck9,LBOOL9_MIDPOINT_AUDIO_MIX_PROCESSED)
				
			ENDIF
		ENDIF
	ELSE
		//Turn off audio mixes during cutscenes and reset afterwards
		STOP_ANY_CURRENTLY_PLAYING_AUDIO_MIX()
		iLocalAudioMixPriority = -1
	ENDIF
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: MUSIC  ----------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Functions relating to the player target of the gang chase, and deciding where to spawn the gang backup -----------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL REINIT_MUSIC_FOR_SPECTATOR()
	/*	When going into spectator mode it will clear LBOOL3_MUSIC_PLAYING but will 
		not clear LBOOL_MUSIC_CHANGED which PROCESS_MUSIC needs to trigger a score	*/
	IF iSpectatorTarget != -1
	AND NOT DID_I_JOIN_MISSION_AS_SPECTATOR()
		IF NOT IS_BIT_SET(iLocalBoolCheck13, LBOOL13_REINITTED_MUSIC_FOR_ENTERING_SPECTATE)
			SET_BIT(iLocalBoolCheck13, LBOOL13_REINITTED_MUSIC_FOR_ENTERING_SPECTATE)
			CLEAR_BIT(iLocalBoolCheck3,LBOOL3_MUSIC_PLAYING)
			CLEAR_BIT(iLocalBoolCheck, LBOOL_MUSIC_CHANGED)
			PRINTLN("[Mission_Music] - Force initialising music for spectator.")
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

// ON FOOT
FUNC BOOL IS_LOCAL_PLAYER_ON_FOOT_FOR_MUSIC_MOOD()
	
	IF IS_PED_IN_ANY_VEHICLE(PlayerPedToUse)
		RETURN FALSE
	ENDIF
	
	IF IS_PED_SWIMMING(PlayerPedToUse)
		RETURN FALSE
	ENDIF
	
	IF IS_PED_FALLING(PlayerPedToUse) // might need a timer
	AND NOT IS_PED_JUMPING(PlayerPedToUse)	
		RETURN FALSE
	ENDIF
	
	IF IS_PED_IN_PARACHUTE_FREE_FALL(PlayerPedToUse)
		RETURN FALSE
	ENDIF
		
	RETURN TRUE
ENDFUNC

// IN AIRCRAFT
FUNC BOOL IS_LOCAL_PLAYER_IN_AN_AIRCRAFT_FOR_MUSIC_MOOD()
	
	IF NOT IS_PED_IN_ANY_VEHICLE(localPlayerPed)
		RETURN FALSE
	ENDIF
	
	IF IS_PED_IN_ANY_PLANE(PlayerPedToUse)
	OR IS_PED_IN_ANY_HELI(PlayerPedToUse)
	// Check for flying vehicles like Deluxo / Oppressor?
		RETURN TRUE
	ENDIF	
	
	RETURN FALSE
ENDFUNC

// PARACHUTING
FUNC BOOL IS_LOCAL_PLAYER_USING_A_PARACHUTE_FOR_MUSIC_MOOD()

	IF IS_PED_IN_PARACHUTE_FREE_FALL(PlayerPedToUse)
	OR GET_PED_PARACHUTE_STATE(PlayerPedToUse) = PPS_PARACHUTING
	OR GET_PED_PARACHUTE_STATE(PlayerPedToUse) = PPS_DEPLOYING
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

// SWIMMING
FUNC BOOL IS_LOCAL_PLAYER_SWIMMING_FOR_MUSIC_MOOD()
	
	IF IS_PED_IN_ANY_VEHICLE(PlayerPedToUse)
	AND NOT IS_PED_IN_ANY_SUB(PlayerPedToUse)
		RETURN FALSE
	ENDIF
	
	IF IS_PED_SWIMMING(PlayerPedToUse)
	OR IS_ENTITY_IN_WATER(PlayerPedToUse)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

// BOATING
FUNC BOOL IS_LOCAL_PLAYER_IN_A_BOAT_FOR_MUSIC_MOOD()
	
	IF NOT IS_PED_IN_ANY_VEHICLE(PlayerPedToUse)
		RETURN FALSE
	ENDIF
	
	IF IS_PED_IN_ANY_BOAT(PlayerPedToUse)
	OR IS_PED_IN_ANY_JETSKI(PlayerPedToUse)	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

// DRIVING / LAND VEH
FUNC BOOL IS_LOCAL_PLAYER_IN_A_LAND_VEHICLE_FOR_MUSIC_MOOD()
	
	IF NOT IS_PED_IN_ANY_VEHICLE(PlayerPedToUse)
		RETURN FALSE
	ENDIF
	
	IF IS_PED_IN_ANY_BOAT(PlayerPedToUse)
	OR IS_PED_IN_ANY_JETSKI(PlayerPedToUse)	
	OR IS_PED_IN_ANY_SUB(PlayerPedToUse)
	OR IS_PED_IN_ANY_PLANE(PlayerPedToUse)
	OR IS_PED_IN_ANY_HELI(PlayerPedToUse)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

// IN INTERIOR
FUNC BOOL IS_LOCAL_PLAYER_IN_AN_INTERIOR_FOR_MUSIC_MOOD()
	
	IF IS_LOCAL_PLAYER_IN_ANY_SIMPLE_INTERIOR()
	OR GET_INTERIOR_FROM_ENTITY(PlayerPedToUse) != NULL
		PRINTLN("[Mission_Music] - GET_CUSTOM_MUSIC_MOOD_FOR_OBJECTIVE - g_SimpleInteriorData.eCurrentInteriorID; ", ENUM_TO_INT(g_SimpleInteriorData.eCurrentInteriorID), " GET_INTERIOR_FROM_ENTITY: ", NATIVE_TO_INT(GET_INTERIOR_FROM_ENTITY(PlayerPedToUse)))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

// IN INTERIOR - ALT 1
FUNC BOOL IS_LOCAL_PLAYER_IN_AN_INTERIOR_ALT_1_FOR_MUSIC_MOOD()
		
	IF GET_INTERIOR_FROM_ENTITY(PlayerPedToUse) = NULL		
		IF IS_BIT_SET(iLocalBoolCheck25, LBOOL25_FORCE_INTERIOR_ALT_1_MUSIC_TO_PLAY)
			CLEAR_BIT(iLocalBoolCheck29, LBOOL29_MUSIC_LOCKER_MUSIC_STARTED)
			PRINTLN("[MusicSwap] - Returning TRUE - Conditions Met.")
			RETURN TRUE
		ENDIF
		RETURN FALSE
	ENDIF
	
	INT iHash = GET_ROOM_KEY_FROM_ENTITY(LocalPlayerPed)
		
	// Island Compound Underground Area (Vault)
	IF iHash = ciROOM_KEY_ISLAND_HEIST_VAULT
		RETURN TRUE
	ENDIF
	
	// Music Locker	
	IF SHOULD_MUSIC_LOCKER_MUSIC_CUE_SPECIAL_CONDITIONS_TRIGGER_MOOD()
		RETURN TRUE
	ENDIF
		
	RETURN FALSE
ENDFUNC

// IN INTERIOR - ALT 2
FUNC BOOL IS_LOCAL_PLAYER_IN_AN_INTERIOR_ALT_2_FOR_MUSIC_MOOD()
	
	IF GET_INTERIOR_FROM_ENTITY(PlayerPedToUse) = NULL		
		RETURN FALSE
	ENDIF
		
	//INT iHash = GET_ROOM_KEY_FROM_ENTITY(LocalPlayerPed)
	
	
	RETURN FALSE
ENDFUNC

// IS INTERACTING
FUNC BOOL IS_LOCAL_PLAYER_INTERACTING_FOR_MUSIC_MOOD()
	
	IF MC_playerBD[iPartToUse].iObjHacking > -1
	OR GET_FOREGROUND_INTERACTABLE_INDEX() > -1
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_CUSTOM_MUSIC_MOOD_FOR_OBJECTIVE()
	
	INT iCustomMusicMoodReturn = 0
	INT iTeam = MC_PlayerBD[iPartToUse].iteam
	INT iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]

	IF iRule < FMMC_MAX_RULES
		BOOL bAggroed = HAS_ANY_TEAM_TRIGGERED_AGGRO(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iAggroIndexBS_Music[iRule])
		
		// ON FOOT
		IF bAggroed
		AND g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScore_OnFoot_Aggro[iRule]	!= 0	
			IF IS_LOCAL_PLAYER_ON_FOOT_FOR_MUSIC_MOOD()
				PRINTLN("[Mission_Music] - GET_CUSTOM_MUSIC_MOOD_FOR_OBJECTIVE - iMusicScore_OnFoot_Aggro Set to: ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScore_OnFoot_Aggro[iRule])
				iCustomMusicMoodReturn = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScore_OnFoot_Aggro[iRule]
			ENDIF
		ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScore_OnFoot[iRule] != 0			
			IF IS_LOCAL_PLAYER_ON_FOOT_FOR_MUSIC_MOOD()
				PRINTLN("[Mission_Music] - GET_CUSTOM_MUSIC_MOOD_FOR_OBJECTIVE - iMusicScore_OnFoot Set to: ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScore_OnFoot[iRule])
				iCustomMusicMoodReturn = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScore_OnFoot[iRule]
			ENDIF
		ENDIF
		
		// IN AIRCRAFT
		IF bAggroed
		AND g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScore_InAircraft_Aggro[iRule] != 0	
			IF IS_LOCAL_PLAYER_IN_AN_AIRCRAFT_FOR_MUSIC_MOOD()
				PRINTLN("[Mission_Music] - GET_CUSTOM_MUSIC_MOOD_FOR_OBJECTIVE - iMusicScore_InAircraft_Aggro Set to: ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScore_InAircraft_Aggro[iRule])
				iCustomMusicMoodReturn = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScore_InAircraft_Aggro[iRule]
			ENDIF
		ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScore_InAircraft[iRule] != 0
			IF IS_LOCAL_PLAYER_IN_AN_AIRCRAFT_FOR_MUSIC_MOOD()
				PRINTLN("[Mission_Music] - GET_CUSTOM_MUSIC_MOOD_FOR_OBJECTIVE - iMusicScore_InAircraft Set to: ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScore_InAircraft[iRule])
				iCustomMusicMoodReturn = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScore_InAircraft[iRule]
			ENDIF
		ENDIF
		
		// PARACHUTING
		IF bAggroed
		AND g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScore_Parachuting_Aggro[iRule] != 0	
			IF IS_LOCAL_PLAYER_USING_A_PARACHUTE_FOR_MUSIC_MOOD()
				PRINTLN("[Mission_Music] - GET_CUSTOM_MUSIC_MOOD_FOR_OBJECTIVE - iMusicScore_Parachuting_Aggro Set to: ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScore_Parachuting_Aggro[iRule])
				iCustomMusicMoodReturn = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScore_Parachuting_Aggro[iRule]
			ENDIF
		ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScore_Parachuting[iRule] != 0
			IF IS_LOCAL_PLAYER_USING_A_PARACHUTE_FOR_MUSIC_MOOD()
				PRINTLN("[Mission_Music] - GET_CUSTOM_MUSIC_MOOD_FOR_OBJECTIVE - iMusicScore_Parachuting Set to: ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScore_Parachuting[iRule])
				iCustomMusicMoodReturn = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScore_Parachuting[iRule]
			ENDIF
		ENDIF
		
		// SWIMMING
		IF bAggroed
		AND g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScore_Swimming_Aggro[iRule] != 0	
			IF IS_LOCAL_PLAYER_SWIMMING_FOR_MUSIC_MOOD()
				PRINTLN("[Mission_Music] - GET_CUSTOM_MUSIC_MOOD_FOR_OBJECTIVE - iMusicScore_Swimming_Aggro Set to: ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScore_Swimming_Aggro[iRule])
				iCustomMusicMoodReturn = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScore_Swimming_Aggro[iRule]
			ENDIF
		ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScore_Swimming[iRule] != 0
			IF IS_LOCAL_PLAYER_SWIMMING_FOR_MUSIC_MOOD()
				PRINTLN("[Mission_Music] - GET_CUSTOM_MUSIC_MOOD_FOR_OBJECTIVE - iMusicScore_Swimming Set to: ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScore_Swimming[iRule])
				iCustomMusicMoodReturn = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScore_Swimming[iRule]
			ENDIF
		ENDIF
		
		// BOATING
		IF bAggroed
		AND g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScore_Boating_Aggro[iRule] != 0	
			IF IS_LOCAL_PLAYER_IN_A_BOAT_FOR_MUSIC_MOOD()
				PRINTLN("[Mission_Music] - GET_CUSTOM_MUSIC_MOOD_FOR_OBJECTIVE - iMusicScore_Boating_Aggro Set to: ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScore_Boating_Aggro[iRule])
				iCustomMusicMoodReturn = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScore_Boating_Aggro[iRule]
			ENDIF
		ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScore_Boating[iRule] != 0
			IF IS_LOCAL_PLAYER_IN_A_BOAT_FOR_MUSIC_MOOD()
				PRINTLN("[Mission_Music] - GET_CUSTOM_MUSIC_MOOD_FOR_OBJECTIVE - iMusicScore_Boating Set to: ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScore_Boating[iRule])
				iCustomMusicMoodReturn = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScore_Boating[iRule]
			ENDIF
		ENDIF
		
		// DRIVING / LAND VEH
		IF bAggroed
		AND g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScore_Driving_Aggro[iRule] != 0	
			IF IS_LOCAL_PLAYER_IN_A_LAND_VEHICLE_FOR_MUSIC_MOOD()
				PRINTLN("[Mission_Music] - GET_CUSTOM_MUSIC_MOOD_FOR_OBJECTIVE - iMusicScore_Driving_Aggro Set to: ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScore_Driving_Aggro[iRule])
				iCustomMusicMoodReturn = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScore_Driving_Aggro[iRule]
			ENDIF
		ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScore_Driving[iRule] != 0
			IF IS_LOCAL_PLAYER_IN_A_LAND_VEHICLE_FOR_MUSIC_MOOD()
				PRINTLN("[Mission_Music] - GET_CUSTOM_MUSIC_MOOD_FOR_OBJECTIVE - iMusicScore_Driving Set to: ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScore_Driving[iRule])
				iCustomMusicMoodReturn = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScore_Driving[iRule]
			ENDIF
		ENDIF
			
		// CHASED BY GANG (local player)
		IF bAggroed
		AND g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScore_IsBeingChasedByGang_Aggro[iRule] != 0	
			IF IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_CHASE_TARGET)
				PRINTLN("[Mission_Music] - GET_CUSTOM_MUSIC_MOOD_FOR_OBJECTIVE - iMusicScore_IsBeingChasedByGang_Aggro Set to: ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScore_IsBeingChasedByGang_Aggro[iRule])
				iCustomMusicMoodReturn = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScore_IsBeingChasedByGang_Aggro[iRule]
			ENDIF
		ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScore_IsBeingChasedByGang[iRule] != 0
			IF IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_CHASE_TARGET)
				PRINTLN("[Mission_Music] - GET_CUSTOM_MUSIC_MOOD_FOR_OBJECTIVE - iMusicScore_IsBeingChasedByGang Set to: ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScore_IsBeingChasedByGang[iRule])
				iCustomMusicMoodReturn = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScore_IsBeingChasedByGang[iRule]
			ENDIF
		ENDIF
			
		// WANTED 
		IF bAggroed
		AND g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScore_WantedLevelGreaterThan1_Aggro[iRule] != 0	
			IF MC_PlayerBD[iLocalPart].iWanted > 0
				PRINTLN("[Mission_Music] - GET_CUSTOM_MUSIC_MOOD_FOR_OBJECTIVE - iMusicScore_WantedLevelGreaterThan1_Aggro Set to: ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScore_WantedLevelGreaterThan1_Aggro[iRule])
				iCustomMusicMoodReturn = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScore_WantedLevelGreaterThan1_Aggro[iRule]
			ENDIF
		ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScore_WantedLevelGreaterThan1[iRule] != 0
			IF MC_PlayerBD[iLocalPart].iWanted > 0
				PRINTLN("[Mission_Music] - GET_CUSTOM_MUSIC_MOOD_FOR_OBJECTIVE - iMusicScore_WantedLevelGreaterThan1 Set to: ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScore_WantedLevelGreaterThan1[iRule])
				iCustomMusicMoodReturn = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScore_WantedLevelGreaterThan1[iRule]
			ENDIF
		ENDIF
				
		// IN INTERIOR
		IF bAggroed
		AND g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScore_InteriorSpace_Aggro[iRule] != 0	
			IF IS_LOCAL_PLAYER_IN_AN_INTERIOR_FOR_MUSIC_MOOD()
				PRINTLN("[Mission_Music] - GET_CUSTOM_MUSIC_MOOD_FOR_OBJECTIVE - iMusicScore_InteriorSpace_Aggro Set to: ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScore_InteriorSpace_Aggro[iRule])
				iCustomMusicMoodReturn = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScore_InteriorSpace_Aggro[iRule]
			ENDIF
		ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScore_InteriorSpace[iRule] != 0
			IF IS_LOCAL_PLAYER_IN_AN_INTERIOR_FOR_MUSIC_MOOD()
				PRINTLN("[Mission_Music] - GET_CUSTOM_MUSIC_MOOD_FOR_OBJECTIVE - iMusicScore_InteriorSpace Set to: ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScore_InteriorSpace[iRule])
				iCustomMusicMoodReturn = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScore_InteriorSpace[iRule]
			ENDIF
		ENDIF
		
		// IS INTERACTING
		IF bAggroed
		AND g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScore_IsInteracting_Aggro[iRule] != 0	
			IF IS_LOCAL_PLAYER_INTERACTING_FOR_MUSIC_MOOD()
				PRINTLN("[Mission_Music] - GET_CUSTOM_MUSIC_MOOD_FOR_OBJECTIVE - iMusicScore_IsInteracting_Aggro Set to: ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScore_IsInteracting_Aggro[iRule])
				iCustomMusicMoodReturn = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScore_IsInteracting_Aggro[iRule]
			ENDIF
		ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScore_IsInteracting[iRule] != 0
			IF IS_LOCAL_PLAYER_INTERACTING_FOR_MUSIC_MOOD()
				PRINTLN("[Mission_Music] - GET_CUSTOM_MUSIC_MOOD_FOR_OBJECTIVE - iMusicScore_IsInteracting Set to: ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScore_IsInteracting[iRule])
				iCustomMusicMoodReturn = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScore_IsInteracting[iRule]
			ENDIF
		ENDIF
		
		// IN INTERIOR - ALT 1
		IF bAggroed
		AND g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScore_InteriorSpace_Aggro_Alt_1[iRule] != 0	
			IF IS_LOCAL_PLAYER_IN_AN_INTERIOR_ALT_1_FOR_MUSIC_MOOD()
				PRINTLN("[Mission_Music] - GET_CUSTOM_MUSIC_MOOD_FOR_OBJECTIVE - iMusicScore_InteriorSpace_Aggro_Alt_1 Set to: ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScore_InteriorSpace_Aggro_Alt_1[iRule])
				iCustomMusicMoodReturn = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScore_InteriorSpace_Aggro_Alt_1[iRule]
			ENDIF
		ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScore_InteriorSpace_Alt_1[iRule] != 0
			IF IS_LOCAL_PLAYER_IN_AN_INTERIOR_ALT_1_FOR_MUSIC_MOOD()
				PRINTLN("[Mission_Music] - GET_CUSTOM_MUSIC_MOOD_FOR_OBJECTIVE - iMusicScore_InteriorSpace_Alt_1 Set to: ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScore_InteriorSpace_Alt_1[iRule])
				iCustomMusicMoodReturn = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScore_InteriorSpace_Alt_1[iRule]
			ENDIF
		ENDIF
		
		// IN INTERIOR - ALT 2
		IF bAggroed
		AND g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScore_InteriorSpace_Aggro_Alt_2[iRule] != 0	
			IF IS_LOCAL_PLAYER_IN_AN_INTERIOR_ALT_2_FOR_MUSIC_MOOD()
				PRINTLN("[Mission_Music] - GET_CUSTOM_MUSIC_MOOD_FOR_OBJECTIVE - iMusicScore_InteriorSpace_Aggro_Alt_2 Set to: ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScore_InteriorSpace_Aggro_Alt_2[iRule])
				iCustomMusicMoodReturn = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScore_InteriorSpace_Aggro_Alt_2[iRule]
			ENDIF
		ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScore_InteriorSpace_Alt_2[iRule] != 0
			IF IS_LOCAL_PLAYER_IN_AN_INTERIOR_ALT_2_FOR_MUSIC_MOOD()
				PRINTLN("[Mission_Music] - GET_CUSTOM_MUSIC_MOOD_FOR_OBJECTIVE - iMusicScore_InteriorSpace_Alt_2 Set to: ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScore_InteriorSpace_Alt_2[iRule])
				iCustomMusicMoodReturn = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScore_InteriorSpace_Alt_2[iRule]
			ENDIF
		ENDIF
				
	ENDIF
	
	IF iCustomMusicMoodReturn != iPrevCustomMusicMoodForObjective
		PRINTLN("[Mission_Music] - GET_CUSTOM_MUSIC_MOOD_FOR_OBJECTIVE - | Returning iCustomMusicMoodReturn: ", iCustomMusicMoodReturn)
	ENDIF
	
	RETURN iCustomMusicMoodReturn
ENDFUNC

FUNC INT GET_MUSIC_MOOD_FOR_OBJECTIVE()

	INT iTeam = MC_PlayerBD[iPartToUse].iteam
	INT iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]

	IF iRule < FMMC_MAX_RULES
		IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScoreReachedMood[iRule] > 0
			PRINTLN("[Mission_Music] - GET_MUSIC_MOOD_FOR_OBJECTIVE - iMusicScoreReachedMood: ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScoreReachedMood[iRule], " On irule: ", iRule)				
			IF MC_serverBD.iScoreOnThisRule[iTeam] >= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScoreReachedMood[iRule]
				PRINTLN("[Mission_Music] - GET_MUSIC_MOOD_FOR_OBJECTIVE - iMusicScoreReachedMood Set to: ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScoreReachedMood[iRule], " and iScoreOnThisRule is: ", MC_serverBD.iScoreOnThisRule[iTeam], " Overriding Music Cue to the midpoint: ",  g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicMid[iRule])
				RETURN g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicMid[iRule]
			ENDIF
		ENDIF
		IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScoreAggroMood[iRule] > 0
			PRINTLN("[Mission_Music] - GET_MUSIC_MOOD_FOR_OBJECTIVE - iMusicScoreAggroMood: ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScoreAggroMood[iRule], " On irule: ", iRule)
			IF HAS_ANY_TEAM_TRIGGERED_AGGRO(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iAggroIndexBS_Music[iRule])
				PRINTLN("[Mission_Music] - GET_MUSIC_MOOD_FOR_OBJECTIVE - iMusicScoreAggroMood Set to: ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScoreAggroMood[iRule])
		 		RETURN g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScoreAggroMood[iRule]
			ENDIF
		ENDIF
	ENDIF
	
	PRINTLN("[Mission_Music] - GET_MUSIC_MOOD_FOR_OBJECTIVE | Returning g_FMMC_STRUCT.sFMMCEndConditions[", iTeam, "].iMusicStart[", iRule, "] which is ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicStart[iRule])
	RETURN g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicStart[iRule]
ENDFUNC

FUNC BOOL CAN_MUSIC_STATE_BE_CHANGED()
	IF IS_BIT_SET(iLocalBoolCheck25, LBOOL25_MUSIC_STARTED_DRE_TRACK)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC SET_MUSIC_STATE(INT istate)
	
	IF NOT CAN_MUSIC_STATE_BE_CHANGED()
		EXIT
	ENDIF
	
	IF MC_PlayerBD[iLocalPart].iMusicState != istate
	OR REINIT_MUSIC_FOR_SPECTATOR()	
		
		PRINTLN("[Mission_Music] - SET_MUSIC_STATE - Called, istate: ", istate)
		
		IF MC_PlayerBD[iPartToUse].iMusicState = MUSIC_SILENT
			IF istate = MUSIC_SUSPENSE
			OR istate = MUSIC_ACTION
			OR istate = MUSIC_VEHICLE_CHASE
			OR istate = MUSIC_AIRBORNE
				IF NOT IS_BIT_SET(iLocalBoolCheck29, LBOOL29_MUSIC_LOCKER_MUSIC_STARTED)
				AND NOT IS_BIT_SET(iLocalBoolCheck25, LBOOL25_FORCE_INTERIOR_ALT_1_MUSIC_TO_PLAY)
					TRIGGER_MUSIC_EVENT("MP_MC_RADIO_OUT_SCORE_IN") 
					PRINTLN("[Mission_Music] - SET_MUSIC_STATE - MUSIC CALLED : MP_MC_RADIO_OUT_SCORE_IN ")
				ENDIF
			ENDIF
		ENDIF
		
		//-- For score help text
		IF NOT IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet9, BI_FM_NMH9_LISTEN_SCORE)
			IF istate = MUSIC_SILENT
				IF IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet9, BI_FM_NMH9_DO_SCORE_HELP)
					CLEAR_BIT(MPGlobalsAmbience.iFmNmhBitSet9, BI_FM_NMH9_DO_SCORE_HELP)
					PRINTLN("[Mission_Music] - SET_MUSIC_STATE - listen To Score help - cleared BI_FM_NMH9_DO_SCORE_HELP ")
				ENDIF
			ELSE
				IF NOT IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet9, BI_FM_NMH9_DO_SCORE_HELP)
					SET_BIT(MPGlobalsAmbience.iFmNmhBitSet9, BI_FM_NMH9_DO_SCORE_HELP)
					PRINTLN("[Mission_Music] - SET_MUSIC_STATE - listen To Score help - Set BI_FM_NMH9_DO_SCORE_HELP istate = ", istate)
				ENDIF
			ENDIF
		ENDIF
		
		IF iSpectatorTarget = -1
			MC_PlayerBD[iLocalPart].iMusicState = istate
		ENDIF
		
		REINIT_NET_TIMER(tdMusicTimer)
		CLEAR_BIT(iLocalBoolCheck, LBOOL_MUSIC_CHANGED) 
		PRINTLN("[Mission_Music] - SET_MUSIC_STATE - MUSIC STATE CHANGE TO: ", GET_MUSIC_STATE_NAME(istate))
		DEBUG_PRINTCALLSTACK()
	ENDIF
	
ENDPROC

FUNC BOOL IS_MISSION_MUSIC_BLOCKED_FOR_INTRO_CUTSCENE()

	IF IS_INTRO_CUTSCENE_RULE_ACTIVE()
	AND g_FMMC_STRUCT.iAudioScoreStartingMood = MUSIC_SILENT
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_COUNTDOWN_TIMER_SOUNDS()

	IF NOT IS_SOUND_ID_VALID(iSoundIDCountdown)
		iSoundIDCountdown = GET_SOUND_ID()
		PRINTLN("[Mission_Music] - [PROCESS_COUNTDOWN_TIMER_SOUNDS] - GET_SOUND_ID iSoundIDCountdown ",iSoundIDCountdown)
	ENDIF
		
	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iPriority = GET_TEAM_CURRENT_RULE(iTeam)
	
	IF iPriority < FMMC_MAX_RULES
		INT iTimeLeft = GET_OBJECTIVE_TIMER_TIME_REMAINING(iTeam, iPriority)
				
		IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam)
			PRINTLN("[Mission_Music] - [PCTS] Rule Changed to: ", iPriority, ". Stopping the countdown music sound from playing.")
			
			IF IS_SOUND_ID_VALID(iSoundIDCountdown)
			AND NOT HAS_SOUND_FINISHED(iSoundIDCountdown)
				PRINTLN("[Mission_Music] - [PCTS] Stopping iSoundIDCountdown: ", iSoundIDCountdown)
				STOP_SOUND(iSoundIDCountdown)
			ELSE
				PRINTLN("[Mission_Music] - [PCTS] Already Stopped/Finished?")
			ENDIF
			
			iTimeleft = GET_OBJECTIVE_TIMER_TIME_REMAINING(iTeam, iPriority)
			CLEAR_BIT(iLocalBoolCheck5, LBOOL5_STARTED_COUNTDOWN_TIMER_SOUNDS)
			
			PRINTLN("[Mission_Music] - [PCTS] iTimeleft: ", iTimeleft)
			EXIT
		ENDIF
		
		IF iTimeLeft <= 0
		AND IS_OBJECTIVE_TIMER_RUNNING(iTeam)				
			PRINTLN("[Mission_Music] - [PCTS] iTimeleft: ", iTimeleft, " Exitting, as we are <= 0.")
			EXIT
		ENDIF
		
		IF NOT IS_BIT_SET(iLocalBoolCheck5, LBOOL5_STARTED_COUNTDOWN_TIMER_SOUNDS)
		AND (IS_OBJECTIVE_TIMER_RUNNING(iTeam) OR IS_MULTIRULE_TIMER_RUNNING(iTeam))

			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSix[iPriority], ciBS_RULE6_COUNTDOWN_TIMER_SOUND_THIRTYSECONDS)
			OR (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTen, ciALLOW_30S_COUNTDOWN_END_OF_ANY_ROUND)
				AND (MC_serverBD_3.iMultiObjectiveTimeLimit[iTeam] - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdMultiObjectiveLimitTimer[iTeam])< 60000))
		
				IF HAS_NET_TIMER_STARTED( MC_serverBD_3.tdMultiObjectiveLimitTimer[iTeam] )
				AND MC_serverBD_3.iMultiObjectiveTimeLimit[iTeam] - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdMultiObjectiveLimitTimer[iTeam]) > 50
					iTimeLeft = MC_serverBD_3.iMultiObjectiveTimeLimit[iTeam] - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdMultiObjectiveLimitTimer[iTeam])
				ENDIF
				
				IF IS_BIT_SET(iLocalBoolCheck15, LBOOL15_RESET_30S_CD_BOOLS_NEXT_RULE)
				AND iTimeleft > 30000
					PRINTLN("[Mission_Music] - [PCTS] LBOOL14_APT_STARTED_PRE_COUNTDOWN: > 30000 time left. (30S_KILL) ...")
					
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciUSE_FREEMODE_30S_COUNTDOWN)
						PRINTLN("[Mission_Music] - [PCTS] Triggering FM_COUNTDOWN_30S_KILL")
						TRIGGER_MUSIC_EVENT("FM_COUNTDOWN_30S_KILL")
					ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciUSE_IE_30S_COUNTDOWN)
						PRINTLN("[Mission_Music] - [PCTS] Triggering IE_COUNTDOWN_30S_KILL")
						TRIGGER_MUSIC_EVENT("IE_COUNTDOWN_30S_KILL")
					ELSE
						PRINTLN("[Mission_Music] - [PCTS] Triggering APT_COUNTDOWN_30S_KILL")
						TRIGGER_MUSIC_EVENT("APT_COUNTDOWN_30S_KILL")
					ENDIF
					
					CLEAR_BIT(iLocalBoolCheck, LBOOL_MUSIC_CHANGED)
					SET_MISSION_MUSIC(g_FMMC_STRUCT.iAudioScore)
					SET_BIT(iLocalBoolCheck3, LBOOL3_MUSIC_PLAYING)
					
					CLEAR_BIT(iLocalBoolCheck14, LBOOL14_HAS_STARTED_APT_COUNTDOWN_MUSIC)
					CLEAR_BIT(iLocalBoolCheck15, LBOOL15_RESET_30S_CD_BOOLS_NEXT_RULE)
				ENDIF

				IF iTimeleft <= 35000
				AND iTimeleft > 10000 //Hacky fix for 2884001, seems like we were getting into here when iTimeleft = 788 :(
					IF NOT IS_BIT_SET(iLocalBoolCheck14, LBOOL14_APT_STARTED_PRE_COUNTDOWN)
						SET_BIT(iLocalBoolCheck14, LBOOL14_APT_STARTED_PRE_COUNTDOWN)
						
						PRINTLN("[Mission_Music] - [PCTS] LBOOL14_APT_STARTED_PRE_COUNTDOWN: playing countdown music for < 35000 time left.")
						
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciUSE_FREEMODE_30S_COUNTDOWN)
							TRIGGER_MUSIC_EVENT("FM_PRE_COUNTDOWN_30S")
							PRINTLN("[Mission_Music] - [PCTS] Starting 35 second pre-countdown for fm countdown")
						ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciUSE_IE_30S_COUNTDOWN)
							TRIGGER_MUSIC_EVENT("IE_PRE_COUNTDOWN_STOP")
							PRINTLN("[Mission_Music] - [PCTS] Starting 35 second pre-countdown for IE_PRE_COUNTDOWN_STOP")
						ELSE
							IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet6, SBBOOL6_TIME_DEDUCTED_INTO_30S)
								PRINTLN("[Mission_Music] - [PCTS] Starting 35 second pre-countdown for apt")
								TRIGGER_MUSIC_EVENT("APT_PRE_COUNTDOWN_STOP")
							ENDIF
						ENDIF
						
						PRINTLN("[Mission_Music] - [PCTS] 1 iPriority = ", iPriority, ", iTimeleft = ", iTimeleft,", LBOOL14_HAS_STARTED_APT_COUNTDOWN_MUSIC = ", IS_BIT_SET(iLocalBoolCheck14, LBOOL14_HAS_STARTED_APT_COUNTDOWN_MUSIC),
								", LBOOL15_HAS_STARTED_VAL_COUNTDOWN_MUSIC = ", IS_BIT_SET(iLocalBoolCheck15, LBOOL15_HAS_STARTED_VAL_COUNTDOWN_MUSIC))
					ENDIF
				ENDIF
				
				IF iTimeleft <= 30000
				AND iTimeleft > 10000 //Hacky fix for 2884001, seems like we were getting into here when iTimeleft = 788 :(
				AND NOT IS_BIT_SET(iLocalBoolCheck14, LBOOL14_HAS_STARTED_APT_COUNTDOWN_MUSIC)
				AND NOT IS_BIT_SET(iLocalBoolCheck15, LBOOL15_HAS_STARTED_VAL_COUNTDOWN_MUSIC)
				AND NOT IS_BIT_SET(iLocalBoolCheck20, LBOOL20_HAS_STARTED_IE_COUNTDOWN_MUSIC)
					#IF IS_DEBUG_BUILD
						PRINTLN("[Mission_Music] - *-*-*-*-*-*-*-* [PCTS] PROCESS_COUNTDOWN_TIMER_SOUNDS - Starting 30 second countdown timer *-*-*-*-*-*-*-*")
						PRINTLN("[Mission_Music] - *-*-* [PCTS] PROCESS_COUNTDOWN_TIMER_SOUNDS - iTimeleft = ", iTimeleft)
						PRINTLN("[Mission_Music] - *-*-* [PCTS] LBOOL14_HAS_STARTED_APT_COUNTDOWN_MUSIC = ", IS_BIT_SET(iLocalBoolCheck14, LBOOL14_HAS_STARTED_APT_COUNTDOWN_MUSIC))
						PRINTLN("[Mission_Music] - *-*-* [PCTS] LBOOL15_HAS_STARTED_VAL_COUNTDOWN_MUSIC = ", IS_BIT_SET(iLocalBoolCheck14, LBOOL15_HAS_STARTED_VAL_COUNTDOWN_MUSIC))
						PRINTLN("[Mission_Music] - *-*-* [PCTS] LBOOL20_HAS_STARTED_IE_COUNTDOWN_MUSIC = ", IS_BIT_SET(iLocalBoolCheck20, LBOOL20_HAS_STARTED_IE_COUNTDOWN_MUSIC))
						PRINTLN("[Mission_Music] - *-*-* [PCTS] iPriority = ", iPriority)
						PRINTLN("[Mission_Music] - *-*-* [PCTS] iTeam = ", iTeam)
					#ENDIF
					
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciUSE_FREEMODE_30S_COUNTDOWN)
						PRINTLN("[Mission_Music] - [PCTS] Starting 30s countdown Freemode: FM_COUNTDOWN_30S")
						TRIGGER_MUSIC_EVENT("FM_COUNTDOWN_30S")
						PREPARE_MUSIC_EVENT("FM_COUNTDOWN_30S_KILL")
						SET_BIT(iLocalBoolCheck14, LBOOL14_HAS_STARTED_APT_COUNTDOWN_MUSIC)
						
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciALLOW_30S_CD_MUSIC_FOR_MULTIPLE_RULES)
							PRINTLN("[Mission_Music] - [PCTS] Setting LBOOL15_RESET_30S_CD_BOOLS_NEXT_RULE so we can reset the CD next rule.")
							SET_BIT(iLocalBoolCheck15, LBOOL15_RESET_30S_CD_BOOLS_NEXT_RULE)
						ENDIF
					ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciUSE_IE_30S_COUNTDOWN)
						PRINTLN("[Mission_Music] - [PCTS] Starting 30s countdown IE: IE_COUNTDOWN_30S")
						TRIGGER_MUSIC_EVENT("IE_COUNTDOWN_30S")
						PREPARE_MUSIC_EVENT("IE_COUNTDOWN_30S_KILL")
						SET_BIT(iLocalBoolCheck20, LBOOL20_HAS_STARTED_IE_COUNTDOWN_MUSIC)
						
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciALLOW_30S_CD_MUSIC_FOR_MULTIPLE_RULES)
							PRINTLN("[Mission_Music] - [PCTS] Setting LBOOL15_RESET_30S_CD_BOOLS_NEXT_RULE so we can reset the CD next rule.")
							SET_BIT(iLocalBoolCheck15, LBOOL15_RESET_30S_CD_BOOLS_NEXT_RULE)
						ENDIF
					ELSE
						PRINTLN("[Mission_Music] - [PCTS] Starting 30s countdown APT: APT_COUNTDOWN_30S")
						TRIGGER_MUSIC_EVENT("APT_COUNTDOWN_30S")
						PREPARE_MUSIC_EVENT("APT_COUNTDOWN_30S_KILL")
						SET_BIT(iLocalBoolCheck14, LBOOL14_HAS_STARTED_APT_COUNTDOWN_MUSIC)
						
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciALLOW_30S_CD_MUSIC_FOR_MULTIPLE_RULES)
							PRINTLN("[Mission_Music] - [PCTS] Setting LBOOL15_RESET_30S_CD_BOOLS_NEXT_RULE so we can reset the CD next rule.")
							SET_BIT(iLocalBoolCheck15, LBOOL15_RESET_30S_CD_BOOLS_NEXT_RULE)
						ENDIF
					ENDIF
					
				#IF IS_DEBUG_BUILD	
				ELSE
					IF GET_FRAME_COUNT() % 10 = 0
						PRINTLN("[Mission_Music] - [PCTS] 2 iPriority = ", iPriority, ", iTimeleft = ", iTimeleft,", LBOOL14_HAS_STARTED_APT_COUNTDOWN_MUSIC = ", IS_BIT_SET(iLocalBoolCheck14, LBOOL14_HAS_STARTED_APT_COUNTDOWN_MUSIC),
								", LBOOL15_HAS_STARTED_VAL_COUNTDOWN_MUSIC = ", IS_BIT_SET(iLocalBoolCheck15, LBOOL15_HAS_STARTED_VAL_COUNTDOWN_MUSIC), "LBOOL20_HAS_STARTED_IE_COUNTDOWN_MUSIC = ",IS_BIT_SET(iLocalBoolCheck20, LBOOL20_HAS_STARTED_IE_COUNTDOWN_MUSIC))
					ENDIF
				#ENDIF
				ENDIF
				
				//Turn on Radio Control etc.
				IF iTimeleft <= 27000
				AND NOT IS_BIT_SET(iLocalBoolCheck15,LBOOL15_TURN_BACK_ON_RADIO_CONTROL)	
					SET_USER_RADIO_CONTROL_ENABLED(TRUE)
					SET_AUDIO_FLAG("AllowScoreAndRadio", FALSE)
					SET_BIT(iLocalBoolCheck15,LBOOL15_TURN_BACK_ON_RADIO_CONTROL)
				ENDIF
				
				IF iTimeleft <= 5000
					IF NOT IS_BIT_SET(iLocalBoolCheck14, LBOOL14_PLAYED_5S_COUNTDOWN_ON_30S_SETTING_CD)
						SET_BIT(iLocalBoolCheck14, LBOOL14_PLAYED_5S_COUNTDOWN_ON_30S_SETTING_CD)
						PRINTLN("[Mission_Music] - [PCTS] Playing 5s countdown: 5S, MP_MISSION_COUNTDOWN_SOUNDSET")
						PLAY_SOUND_FRONTEND(iSoundIDCountdown, "5S","MP_MISSION_COUNTDOWN_SOUNDSET", FALSE)
					ENDIF
				ENDIF
				
				IF NOT IS_BIT_SET(iLocalBoolCheck14, LBOOL14_COUNTDOWN_EARLY_MISSION_END) AND iTimeleft >= 1500
					PRINTLN("[Mission_Music] - [PCTS] Setting LBOOL14_COUNTDOWN_EARLY_MISSION_END")
					SET_BIT(iLocalBoolCheck14, LBOOL14_COUNTDOWN_EARLY_MISSION_END)
				ELIF IS_BIT_SET(iLocalBoolCheck14, LBOOL14_COUNTDOWN_EARLY_MISSION_END) AND iTimeleft < 1500
					PRINTLN("[Mission_Music] - [PCTS] Clearing LBOOL14_COUNTDOWN_EARLY_MISSION_END")
					CLEAR_BIT(iLocalBoolCheck14, LBOOL14_COUNTDOWN_EARLY_MISSION_END)					
				ENDIF
			ELIF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwo[iPriority], ciBS_RULE2_COUNTDOWN_TIMER_SOUND_TENSECOND)
				IF iTimeleft <= 10 * 1000
					IF IS_SOUND_ID_VALID(iSoundIDCountdown)
					AND HAS_SOUND_FINISHED(iSoundIDCountdown)
					AND iTimeleft > 0 //Added to stop a timer playing on both sides of a rule if there are multiple 10 second countdowns occurring during the mission.
						PRINTLN("[Mission_Music] - [PCTS] 10S, MP_MISSION_COUNTDOWN_SOUNDSET")
						PLAY_SOUND_FRONTEND(iSoundIDCountdown,"10S","MP_MISSION_COUNTDOWN_SOUNDSET", FALSE)
						PRINTLN("[Mission_Music] - [PCTS] Playing 10 second countdown sound...")						
						SET_BIT(iLocalBoolCheck5, LBOOL5_STARTED_COUNTDOWN_TIMER_SOUNDS)
					ELSE
						PRINTLN("[Mission_Music] - [PCTS] 10s Sound stopped due to it being finished, no time left, or the ID is now invalid.")
						STOP_SOUND(iSoundIDCountdown)
					ENDIF
				ELSE
					IF IS_SOUND_ID_VALID(iSoundIDCountdown)
					AND NOT HAS_SOUND_FINISHED(iSoundIDCountdown)
						PRINTLN("[Mission_Music] - [PCTS] 10s Sound stopped, Valid ID and it has not finished. Because Time ran out. Time left: ", iTimeleft)
						STOP_SOUND(iSoundIDCountdown)
					ENDIF
				ENDIF
			ELIF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwo[iPriority], ciBS_RULE2_COUNTDOWN_TIMER_SOUND_FIVESECOND)
				IF iTimeleft <= 5 * 1000
					IF IS_SOUND_ID_VALID(iSoundIDCountdown)
					AND HAS_SOUND_FINISHED(iSoundIDCountdown)
						PRINTLN("[Mission_Music] - [PCTS] Playing 5 second countdown sound...")						
						PRINTLN("[Mission_Music] - [PCTS] 5S, MP_MISSION_COUNTDOWN_SOUNDSET")
						PLAY_SOUND_FRONTEND(iSoundIDCountdown,"5S","MP_MISSION_COUNTDOWN_SOUNDSET", FALSE)
							
						SET_BIT(iLocalBoolCheck5, LBOOL5_STARTED_COUNTDOWN_TIMER_SOUNDS)
					ELSE
						PRINTLN("[Mission_Music] - [PCTS] 5s Sound stopped due to it being finished, no time left, or the ID is now invalid.")
						STOP_SOUND(iSoundIDCountdown)
					ENDIF 
				ELSE
					IF IS_SOUND_ID_VALID(iSoundIDCountdown)
					AND NOT HAS_SOUND_FINISHED(iSoundIDCountdown)
						PRINTLN("[Mission_Music] - [PCTS] 5s Sound stopped, Valid ID and it has not finished. Because Time ran out. Time left: ", iTimeleft)
						STOP_SOUND(iSoundIDCountdown)
					ENDIF
				ENDIF
			ELIF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwo[iPriority], ciBS_RULE2_COUNTDOWN_TIMER_SOUND_ONESHOT)
				IF iTimeleft <= 1000
					IF IS_SOUND_ID_VALID(iSoundIDCountdown)
					AND HAS_SOUND_FINISHED(iSoundIDCountdown)
						PRINTLN("[Mission_Music] - [PCTS] Playing one shot countdown sound")
						PLAY_SOUND_FRONTEND(-1,"Oneshot_Final","MP_MISSION_COUNTDOWN_SOUNDSET", FALSE)
						SET_BIT(iLocalBoolCheck5, LBOOL5_STARTED_COUNTDOWN_TIMER_SOUNDS)
					ELSE
						PRINTLN("[Mission_Music] - [PCTS] Stopping sound Oneshot_Final as the ID is invalid or it's finished.")
						STOP_SOUND(iSoundIDCountdown)
					ENDIF
				ELSE
					IF IS_SOUND_ID_VALID(iSoundIDCountdown)
					AND NOT HAS_SOUND_FINISHED(iSoundIDCountdown)
						PRINTLN("[Mission_Music] - [PCTS] Oneshot_Final Sound stopped, Valid ID and it has not finished. Because Time ran out. Time left: ", iTimeleft)
						STOP_SOUND(iSoundIDCountdown)
					ENDIF
				ENDIF
			ELSE
				IF IS_SOUND_ID_VALID(iSoundIDCountdown)
				AND NOT HAS_SOUND_FINISHED(iSoundIDCountdown)
					PRINTLN("[Mission_Music] - [PCTS] calling stop_sound as no bitset was set to decide a countdown timer sound.")
					STOP_SOUND(iSoundIDCountdown)
				ENDIF
			ENDIF
			
		ENDIF
		
	ENDIF
	
	IF IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
		IF IS_SOUND_ID_VALID(iSoundIDCountdown)
		AND NOT HAS_SOUND_FINISHED(iSoundIDCountdown)
			PRINTLN("[Mission_Music] - [PCTS] Mission is over. Sound is still playing, stop the sound: ", iSoundIDCountdown)
			STOP_SOUND(iSoundIDCountdown)
		ENDIF
	ENDIF
ENDPROC

PROC SET_SUDDEN_DEATH_MUSIC_STATE(eSuddenDeathState eNewState)
	PRINTLN("[Mission_Music] - SET_SUDDEN_DEATH_MUSIC_STATE - Setting to state ", ENUM_TO_INT(eNewState), " Previous state was: ", ENUM_TO_INT(sudden_death_music_state))
	sudden_death_music_state = eNewState
ENDPROC

PROC PROCESS_SUDDEN_DEATH_MUSIC()

	SWITCH( sudden_death_music_state )
	
		CASE eSuddenDeathState_WAIT_TO_START
			IF ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
				START_NET_TIMER( stSuddenDeathMusic )
				SET_SUDDEN_DEATH_MUSIC_STATE(eSuddenDeathState_START)
			ENDIF
		BREAK
		
		CASE eSuddenDeathState_START
			IF NOT IS_SOUND_ID_VALID(iSoundIDCountdown)
				iSoundIDCountdown = GET_SOUND_ID()
				PRINTLN("[Mission_Music] - PROCESS_SUDDEN_DEATH_MUSIC - GET_SOUND_ID iSoundIDCountdown ",iSoundIDCountdown)
			ENDIF
			IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME( stSuddenDeathMusic ) > 3000
				IF NOT IS_BIT_SET(iLocalBoolCheck, ciPOPULATE_RESULTS) // Once populate results is called the music should stop
					
					IF IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_PACKAGE_HELD)
						PRINTLN("[Mission_Music] - PROCESS_SUDDEN_DEATH_MUSIC - Started apartment mode APT_SUDDEN_DEATH_START_MUSIC")
						SET_BIT(iLocalBoolCheck14, LBOOL14_HAS_STARTED_APT_SUDDEN_DEATH_MUSIC)
						TRIGGER_MUSIC_EVENT( "APT_SUDDEN_DEATH_START_MUSIC" )
					ELSE
						TRIGGER_MUSIC_EVENT( "FM_SUDDEN_DEATH_START_MUSIC" )
						PRINTLN("[Mission_Music] - Starting sudden death music - FM_SUDDEN_DEATH_START_MUSIC")
					ENDIF

				#IF IS_DEBUG_BUILD
				ELSE
					PRINTLN("[Mission_Music] - We would start the sudden death music, but ciPOPULATE_RESULTS is set already!" )
				#ENDIF
				ENDIF

				RESET_NET_TIMER( stSuddenDeathMusic )
				SET_SUDDEN_DEATH_MUSIC_STATE(eSuddenDeathState_WAIT_FOR_END)
			ENDIF
		BREAK
	ENDSWITCH	

	IF sudden_death_music_state = eSuddenDeathState_WAIT_FOR_END
		INT iTeam = MC_playerBD[iPartToUse].iTeam
		INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]		
		INT iTimeRemaining = GET_SUDDEN_DEATH_TIME_REMAINING(iTeam, iRule)
		
		PRINTLN("[Mission_Music] - PROCESS_SUDDEN_DEATH_MUSIC - setting time remaining to: ", iTimeRemaining)
	
		IF iRule < FMMC_MAX_RULES 
		AND iTeam < FMMC_MAX_TEAMS
			PRINTLN("[Mission_Music] - PROCESS_SUDDEN_DEATH_MUSIC: iTimeRemaining = ", iTimeRemaining)
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSix[iRule], ciBS_RULE6_SD_CD_10_AND_5_SECONDS)
				IF iTimeRemaining <= 10000 
					IF NOT IS_BIT_SET(iLocalBoolCheck16, LBOOL16_PLAYED_10S_SD_SFX)
						SET_BIT(iLocalBoolCheck16, LBOOL16_PLAYED_10S_SD_SFX)
						PLAY_SOUND_FRONTEND(iSoundIDCountdown,"10S","MP_MISSION_COUNTDOWN_SOUNDSET", FALSE)
						PRINTLN("[Mission_Music] - PROCESS_SUDDEN_DEATH_MUSIC - Playing 10s Sudden Death CD SFX (1) - iTimeRemaining = ", iTimeRemaining)
					ENDIF
				ENDIF
				IF iTimeRemaining <= 5000 
					IF NOT IS_BIT_SET(iLocalBoolCheck16, LBOOL16_PLAYED_5S_SD_SFX)
						SET_BIT(iLocalBoolCheck16, LBOOL16_PLAYED_5S_SD_SFX)
						PLAY_SOUND_FRONTEND(iSoundIDCountdown,"5S","MP_MISSION_COUNTDOWN_SOUNDSET", FALSE)
						PRINTLN("[Mission_Music] - PROCESS_SUDDEN_DEATH_MUSIC - Playing 5s Sudden Death CD SFX (1) - iTimeRemaining = ", iTimeRemaining)
					ENDIF
				ENDIF
			ELIF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSix[iRule], ciBS_RULE6_SD_CD_10_SECONDS)
				IF iTimeRemaining <= 10000 + (GET_FRAME_TIME() * 1000)
					IF NOT IS_BIT_SET(iLocalBoolCheck16, LBOOL16_PLAYED_10S_SD_SFX)					
						SET_BIT(iLocalBoolCheck16, LBOOL16_PLAYED_10S_SD_SFX)
						PLAY_SOUND_FRONTEND(iSoundIDCountdown,"10S","MP_MISSION_COUNTDOWN_SOUNDSET", FALSE)
						PRINTLN("[Mission_Music] - PROCESS_SUDDEN_DEATH_MUSIC - Playing 10s Sudden Death CD SFX (2) - iTimeRemaining = ", iTimeRemaining,
								", SBBOOL3_SUDDEN_DEATH_PACKAGE_HELD = ", IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_PACKAGE_HELD))
					ENDIF
				ENDIF
			ELIF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSix[iRule], ciBS_RULE6_SD_CD_5_SECONDS)
				IF iTimeRemaining <= 5000 
					IF NOT IS_BIT_SET(iLocalBoolCheck16, LBOOL16_PLAYED_5S_SD_SFX)
						SET_BIT(iLocalBoolCheck16, LBOOL16_PLAYED_5S_SD_SFX)
						PLAY_SOUND_FRONTEND(iSoundIDCountdown,"5S","MP_MISSION_COUNTDOWN_SOUNDSET", FALSE)
					ENDIF
				ENDIF
			ENDIF

			IF IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_PACKAGE_HELD)
				IF iTimeRemaining <= 10000
					IF NOT IS_BIT_SET(iLocalBoolCheck15, LBOOL15_HAS_STARTED_APT_SUDDEN_DEATH_END)
						SET_BIT(iLocalBoolCheck15, LBOOL15_HAS_STARTED_APT_SUDDEN_DEATH_END)
						PRINTLN("[Mission_Music] - PROCESS_SUDDEN_DEATH_MUSIC - TRIGGER_MUSIC_EVENT('APT_SUDDEN_DEATH_MUSIC_END')")
						TRIGGER_MUSIC_EVENT("APT_SUDDEN_DEATH_MUSIC_END")
					ENDIF
				ENDIF
				
			ELIF IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_BE_MY_VALENTINES)	
				IF iTimeRemaining <= 10000
					IF NOT IS_BIT_SET(iLocalBoolCheck15, LBOOL15_HAS_STARTED_VAL_SUDDEN_DEATH_END)
						SET_BIT(iLocalBoolCheck15, LBOOL15_HAS_STARTED_VAL_SUDDEN_DEATH_END)
						PRINTLN("[Mission_Music] - PROCESS_SUDDEN_DEATH_MUSIC - VAL - PLAY_SOUND_FRONTEND(10s,MP_MISSION_COUNTDOWN_SOUNDSET)")
						PLAY_SOUND_FRONTEND(-1, "10s",  "MP_MISSION_COUNTDOWN_SOUNDSET", FALSE)
					ENDIF
				ENDIF

				IF iTimeRemaining <= 5000
					IF NOT IS_BIT_SET(iLocalBoolCheck15, LBOOL15_HAS_STARTED_VAL_SUDDEN_DEATH_FADE)
						SET_BIT(iLocalBoolCheck15, LBOOL15_HAS_STARTED_VAL_SUDDEN_DEATH_FADE)
						PRINTLN("[Mission_Music] - PROCESS_SUDDEN_DEATH_MUSIC - VAL/PP - TRIGGER_MUSIC_EVENT(FM_SUDDEN_DEATH_STOP_MUSIC)")
						TRIGGER_MUSIC_EVENT("FM_SUDDEN_DEATH_STOP_MUSIC")
					ENDIF
				ENDIF
			ELSE
				IF iTimeRemaining <= 10000
					IF NOT IS_BIT_SET(iLocalBoolCheck15, LBOOL15_HAS_STARTED_APT_SUDDEN_DEATH_END)
						SET_BIT(iLocalBoolCheck15, LBOOL15_HAS_STARTED_APT_SUDDEN_DEATH_END)
						PRINTLN("[Mission_Music] - PROCESS_SUDDEN_DEATH_MUSIC - TRIGGER_MUSIC_EVENT('APT_SUDDEN_DEATH_MUSIC_END') for all modes.")
						TRIGGER_MUSIC_EVENT("APT_SUDDEN_DEATH_MUSIC_END")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_MUSIC()
	
	PED_INDEX tempped
	BOOL bUseDefault, bUseDefaultHigh
	BOOL bStart
	BOOL bMid
	INT ms
	
	IF IS_MISSION_MUSIC_BLOCKED_FOR_INTRO_CUTSCENE()
		PRINTLN("[Mission_Music] - PROCESS_MUSIC - Exitting main process function as intro cutscene rule requires silence.")
		EXIT
	ENDIF
	
	IF (IS_BIT_SET(MC_Playerbd[iPartToUse].iClientBitSet,PBBOOL_TERMINATED_SCRIPT)
	OR IS_BIT_SET(MC_PlayerBD[iPartToUse].iClientBitSet,PBBOOL_ON_LEADERBOARD)
	OR GET_MC_SERVER_GAME_STATE() = GAME_STATE_END
	OR IS_BIT_SET(iLocalBoolCheck34, LBOOL34_PLAYING_END_CUTSCENE_MUSIC))
	
		IF NOT HAS_NET_TIMER_STARTED(tdEndMissionStopMusicDelay)
			START_NET_TIMER(tdEndMissionStopMusicDelay)
		ENDIF		
		IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
		AND HAS_NET_TIMER_EXPIRED_READ_ONLY(tdEndMissionStopMusicDelay, 2000) // Sometimes we get in here before the next strand was being initialized, meaning music cut out during cutscenes.
			IF MC_PlayerBD[iLocalPart].iMusicState != MUSIC_STOP
			OR NOT IS_BIT_SET(iLocalBoolCheck16, LBOOL16_HAS_STOPPED_MUSIC_ON_LEADERBOARD)
			
				#IF IS_DEBUG_BUILD
					IF MC_PlayerBD[iLocalPart].iMusicState != MUSIC_STOP
						PRINTLN("[Mission_Music] - Got in bc MC_PlayerBD[iPartToUse].iMusicState != MUSIC_STOP")
					ELIF NOT IS_BIT_SET(iLocalBoolCheck16, LBOOL16_HAS_STOPPED_MUSIC_ON_LEADERBOARD)
						PRINTLN("[Mission_Music] - Got in bc NOT IS_BIT_SET(iLocalBoolCheck16, LBOOL16_HAS_STOPPED_MUSIC_ON_LEADERBOARD)")
					ENDIF
				#ENDIF
			
				SET_BIT(iLocalBoolCheck16, LBOOL16_HAS_STOPPED_MUSIC_ON_LEADERBOARD)
				
				IF NOT IS_BIT_SET(iLocalBoolCheck34, LBOOL34_PLAYING_END_CUTSCENE_MUSIC)
					MC_PlayerBD[iLocalPart].iMusicState = MUSIC_STOP
				ENDIF
				
				STOP_AUDIO_SCENE("MP_POSITIONED_RADIO_MUTE_SCENE")
				
				IF IS_THIS_A_VERSUS_MISSION()
				OR ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
					MC_SCRIPT_TRIGGER_MUSIC_EVENT_STOP()
					PRINTLN("TRIGGER_MUSIC_EVENT(\"MP_MC_STOP\") - PROCESS_MUSIC")
				ELSE
					PRINTLN("[Mission_Music] - Skipping TRIGGER_MUSIC_EVENT(\"MP_MC_STOP\") if in an Adversary mode")
				ENDIF
				
				SET_AUDIO_FLAG("AllowScoreAndRadio", TRUE)
				PRINTLN("[Mission_Music] - Setting music state to stop bc either PBBOOL_TERMINATED_SCRIPT or PBBOOL_ON_LEADERBOARD")
				PRINTLN("[Mission_Music] - IS_A_STRAND_MISSION_BEING_INITIALISED(): ", IS_A_STRAND_MISSION_BEING_INITIALISED() )
			ENDIF
		ENDIF	
		EXIT
	ENDIF
	
	IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] = g_FMMC_STRUCT.iDroneSoundsRule
		IF iDroneCutsceneSound = -1
			PRINTLN("[Mission_Music] - Starting audio scene for camera drone cutscene")
			START_AUDIO_SCENE("DLC_GR_APC_Drone_View_Scene")
			iDroneCutsceneSound = GET_SOUND_ID()
			PLAY_SOUND_FRONTEND(iDroneCutsceneSound, "Drone_View", "DLC_GR_WVM_APC_Sounds")
		ENDIF
	ELSE
		IF iDroneCutsceneSound != -1
			PRINTLN("[Mission_Music] - Ending audio scene for camera drone cutscene")
			STOP_SOUND(iDroneCutsceneSound)
			STOP_AUDIO_SCENE("DLC_GR_APC_Drone_View_Scene")
			RELEASE_SOUND_ID(iDroneCutsceneSound)
			iDroneCutsceneSound = -1
		ENDIF
	ENDIF
	
	IF g_bMissionOver	
		IF iHalloweenSoundID > -1
		AND NOT HAS_SOUND_FINISHED(iHalloweenSoundID)
			STOP_SOUND(iHalloweenSoundID)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iTeamBitset2, ciBS2_EnableRadioOnly)
	OR (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTen, ciSTOP_SCORE_OVERRIDING_RADIO) AND GET_PLAYER_RADIO_STATION_INDEX() != 255)
		EXIT
	ENDIF
	
	IF IS_RULE_INDEX_VALID(GET_LOCAL_PLAYER_CURRENT_RULE(TRUE))
	AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[GET_LOCAL_PLAYER_TEAM(TRUE)].iRuleBitsetFifteen[GET_LOCAL_PLAYER_CURRENT_RULE(TRUE)], ciBS_RULE15_DISABLE_ALL_MUSIC)
		
		PRINTLN("[Mission_Music] - ciBS_RULE15_DISABLE_ALL_MUSIC enabled.")
		
		IF IS_BIT_SET(iLocalBoolCheck3, LBOOL3_MUSIC_PLAYING)
			CLEAR_BIT(iLocalBoolCheck3, LBOOL3_MUSIC_PLAYING)
			MC_SCRIPT_TRIGGER_MUSIC_EVENT_STOP()
		ENDIF
		
		EXIT
	ENDIF
	
	
	IF HAS_MISSION_GOT_SUDDEN_DEATH()
	AND NOT g_bMissionOver
		PROCESS_SUDDEN_DEATH_MUSIC()
	ENDIF
	
	IF IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
	OR g_bMissionOver
		IF IS_SOUND_ID_VALID(iSoundIDCountdown)
		AND NOT HAS_SOUND_FINISHED(iSoundIDCountdown)
			PRINTLN("[Mission_Music] - PROCESS_SUDDEN_DEATH_MUSIC Mission is over. Sound is still playing, stop the sound: ", iSoundIDCountdown)
			STOP_SOUND(iSoundIDCountdown)
		ENDIF
	ENDIF
	
	IF ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
		EXIT
	ENDIF
	
	IF iSpectatorTarget = -1
	AND bLocalPlayerOK
		IF MC_playerBD[iPartToUse].bHasPlayerReceivedBattleEvent != GET_PLAYER_RECEIVED_BATTLE_EVENT_RECENTLY(LocalPlayer,100,FALSE)
			MC_playerBD[iPartToUse].bHasPlayerReceivedBattleEvent = GET_PLAYER_RECEIVED_BATTLE_EVENT_RECENTLY(LocalPlayer,100,FALSE)
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF bEnableScriptAudioSpammyPrints
		PRINTLN("[Mission_Music] - MC_PlayerBD[iPartToUse].iMusicState: ", MC_PlayerBD[iPartToUse].iMusicState)
		PRINTLN("[Mission_Music] - g_FMMC_STRUCT.iAudioScore: 			 ", g_FMMC_STRUCT.iAudioScore)
		
		IF mc_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
			PRINTLN("[Mission_Music] - Rule Setting: 						 ", g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iMusicStart[mc_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]])
		ENDIF
		PRINTLN("[Mission_Music] - Rule: 								 ", mc_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam])
	ENDIF
	#ENDIF
	
	INT iTeam = MC_PlayerBD[iPartToUse].iteam
	INT iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]
		
	BOOL bOverride, bOverrideWithAggro

	IF iRule < FMMC_MAX_RULES
		IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScoreAggroMood[iRule] > 0
			IF HAS_ANY_TEAM_TRIGGERED_AGGRO(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iAggroIndexBS_Music[iRule])
				bOverride = TRUE
				bOverrideWithAggro = TRUE
			ELSE
				PRINTLN("[Mission_Music] - iMusicScoreAggroMood Set to: ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScoreAggroMood[iRule], " But we have not yet aggroed.")
			ENDIF
		ENDIF
	ENDIF
		
	IF IS_LOCAL_PLAYER_ANY_SPECTATOR()
	AND iSpectatorTarget != -1
	AND iPartToUse != iLocalPart
	AND MC_PlayerBD[iLocalPart].iMusicState != MC_PlayerBD[iPartToUse].iMusicState
		SET_MUSIC_STATE(MC_PlayerBD[iPartToUse].iMusicState)
		PRINTLN("[Mission_Music] - SET_MUSIC_STATE - Spectator's Music (Music: ", MC_PlayerBD[iLocalPart].iMusicState, ") is not the same as iPartToUse: ", iPartToUse, " (Music: ", MC_PlayerBD[iPartToUse].iMusicState,")")
	ELSE
		IF MC_PlayerBD[iPartToUse].iMusicState != MUSIC_CTF_ENDING
		AND MC_PlayerBD[iPartToUse].iMusicState != MUSIC_MID_COUNTDOWN
			
			IF mc_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
				IF NOT IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[MC_playerBD[iPartToUse].iteam], mc_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam])
					IF g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iMusicStart[mc_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]] = MUSIC_DEFAULT
						#IF IS_DEBUG_BUILD
						IF bEnableScriptAudioSpammyPrints
							PRINTLN("[Mission_Music] - 1b - bUseDefault = TRUE")
						ENDIF
						#ENDIF
							
						bUseDefault = TRUE
					ELIF g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iMusicStart[mc_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]] = MUSIC_DEFAULT_HIGH
						#IF IS_DEBUG_BUILD
						IF bEnableScriptAudioSpammyPrints
							PRINTLN("[Mission_Music] - 1c - bUseDefaultHigh = TRUE")
						ENDIF
						#ENDIF
						
						bUseDefaultHigh = TRUE
					ELSE
						#IF IS_DEBUG_BUILD
						IF bEnableScriptAudioSpammyPrints
						ENDIF
						#ENDIF
						
						bStart = TRUE
					ENDIF
				ELSE
					IF g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iMusicMid[mc_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]] = MUSIC_DEFAULT
						#IF IS_DEBUG_BUILD
						IF bEnableScriptAudioSpammyPrints
							PRINTLN("[Mission_Music] - 2a - bUseDefault = TRUE")
						ENDIF
						#ENDIF
						
						bUseDefault = TRUE						
					ELIF g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iMusicMid[mc_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]] = MUSIC_DEFAULT_HIGH
						#IF IS_DEBUG_BUILD
						IF bEnableScriptAudioSpammyPrints
							PRINTLN("[Mission_Music] - 2b - bUseDefaultHigh = TRUE")
						ENDIF
						#ENDIF
						
						bUseDefaultHigh = TRUE
					ELSE
						#IF IS_DEBUG_BUILD
						IF bEnableScriptAudioSpammyPrints
							PRINTLN("[Mission_Music] - 2c - bMid = TRUE")
						ENDIF
						#ENDIF
						
						bMid = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			INT iCustomMusicMoodForObjective = GET_CUSTOM_MUSIC_MOOD_FOR_OBJECTIVE()
			IF iCustomMusicMoodForObjective = 0
				iPrevCustomMusicMoodForObjective = 0
			ENDIF
			
			IF iCustomMusicMoodForObjective != 0
				
				IF NOT HAS_NET_TIMER_STARTED(tdMusicMoodTransitionDelayTimer)
					START_NET_TIMER(tdMusicMoodTransitionDelayTimer)
				ENDIF
				
				IF HAS_NET_TIMER_STARTED(tdMusicMoodTransitionDelayTimer)
				AND HAS_NET_TIMER_EXPIRED_READ_ONLY(tdMusicMoodTransitionDelayTimer, 2500)	
					IF iPrevCustomMusicMoodForObjective != iCustomMusicMoodForObjective
						iPrevCustomMusicMoodForObjective = iCustomMusicMoodForObjective
						RESET_NET_TIMER(tdMusicMoodTransitionDelayTimer)
						PRINTLN("[Mission_Music] - IS_USING_CUSTOM_MUSIC_CUE_OVERRIDES is TRUE Using iCustomMusicMoodForObjective: ", iCustomMusicMoodForObjective)
						SET_MUSIC_STATE(iCustomMusicMoodForObjective)
					ENDIF				
				ENDIF
				
			ELIF bOverrideWithAggro		
				PRINTLN("[Mission_Music] - iMusicScoreAggroMood Set to: ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScoreAggroMood[iRule], " Overriding the Music now!")
				SET_MUSIC_STATE(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScoreAggroMood[iRule])
					
			ELIF bUseDefault
			OR bUseDefaultHigh		
				#IF IS_DEBUG_BUILD
				IF bEnableScriptAudioSpammyPrints
					PRINTLN("[Mission_Music] - bUseDefaultHigh: 					", bUseDefaultHigh, "	SET_MUSIC_STATE:...")
					PRINTLN("[Mission_Music] - bHasPlayerReceivedBattleEvent:  	", MC_playerBD[iPartToUse].bHasPlayerReceivedBattleEvent)
					PRINTLN("[Mission_Music] - iWanted: 							", MC_playerBD[iPartToUse].iWanted)
					PRINTLN("[Mission_Music] - iFakeWanted:						", MC_playerBD[iPartToUse].iFakeWanted)
					PRINTLN("[Mission_Music] - Gang Chasers:						", GET_TOTAL_NUMBER_OF_GANG_CHASE_CHASING_ALL_TEAMS())
				ENDIF
				#ENDIF
				
				IF iRule < FMMC_MAX_RULES
					IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScoreReachedMood[iRule] > 0
						IF MC_serverBD.iScoreOnThisRule[iTeam] >= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScoreReachedMood[iRule]
							PRINTLN("[Mission_Music] - iMusicScoreReachedMood Set to: ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicScoreReachedMood[iRule], " and iScoreOnThisRule is: ", MC_serverBD.iScoreOnThisRule[iTeam], " Overriding Music Cue to the midpoint: ",  g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicMid[iRule])
							SET_MUSIC_STATE(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMusicMid[iRule])
							bOverride = TRUE
						ENDIF
					ENDIF				
				ENDIF
				
				IF NOT bOverride
					IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdMusicTimer) > 15000
						IF bPlayerToUseOK
							tempped = PlayerPedToUse
							IF NOT IS_PED_INJURED(tempped)
								IF IS_PED_IN_ANY_VEHICLE(tempped)
									IF IS_PED_IN_ANY_PLANE(tempped)
									OR IS_PED_IN_ANY_HELI(tempped)
										IF bUseDefaultHigh
											SET_MUSIC_STATE(MUSIC_ACTION)
										ELSE
											SET_MUSIC_STATE(MUSIC_AIRBORNE)
										ENDIF
									ELSE
										IF  MC_playerBD[iPartToUse].bHasPlayerReceivedBattleEvent
										OR 	MC_playerBD[iPartToUse].iWanted > 0
										OR  MC_playerBD[iPartToUse].iFakeWanted > 0
										OR GET_TOTAL_NUMBER_OF_GANG_CHASE_CHASING_ALL_TEAMS() > 0
											IF 	MC_playerBD[iPartToUse].iWanted > 3 
											OR  MC_playerBD[iPartToUse].iFakeWanted > 3 
											OR GET_TOTAL_NUMBER_OF_GANG_CHASE_CHASING_ALL_TEAMS() > 0
											OR bUseDefaultHigh
												SET_MUSIC_STATE(MUSIC_VEHICLE_CHASE)
											ELSE
												SET_MUSIC_STATE(MUSIC_ACTION)
											ENDIF
										ELSE
											IF bUseDefaultHigh
												SET_MUSIC_STATE(MUSIC_ACTION)
											ELSE
												SET_MUSIC_STATE(MUSIC_SUSPENSE)
											ENDIF
										ENDIF
									ENDIF
								ELSE
									IF  MC_playerBD[iPartToUse].bHasPlayerReceivedBattleEvent
									OR 	MC_playerBD[iPartToUse].iWanted > 0
									OR  MC_playerBD[iPartToUse].iFakeWanted > 0
										IF 	MC_playerBD[iPartToUse].iWanted > 3 
										OR  MC_playerBD[iPartToUse].iFakeWanted > 3 
										OR bUseDefaultHigh
											SET_MUSIC_STATE(MUSIC_VEHICLE_CHASE)
										ELSE
											SET_MUSIC_STATE(MUSIC_ACTION)
										ENDIF
									ELSE
										IF bUseDefaultHigh
											SET_MUSIC_STATE(MUSIC_ACTION)
										ELSE
											SET_MUSIC_STATE(MUSIC_SUSPENSE)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						Bool bReinit = FALSE
						
						IF MC_PlayerBD[iPartToUse].iMusicState = MUSIC_ACTION
							IF  MC_playerBD[iPartToUse].bHasPlayerReceivedBattleEvent
							OR 	MC_playerBD[iPartToUse].iWanted > 0
							OR  MC_playerBD[iPartToUse].iFakeWanted > 0
							OR GET_TOTAL_NUMBER_OF_GANG_CHASE_CHASING_ALL_TEAMS() > 0
								IF 	MC_playerBD[iPartToUse].iWanted > 3
								OR  MC_playerBD[iPartToUse].iFakeWanted > 3
								OR GET_TOTAL_NUMBER_OF_GANG_CHASE_CHASING_ALL_TEAMS() > 0
								OR bUseDefaultHigh
									#IF IS_DEBUG_BUILD
									IF bEnableScriptAudioSpammyPrints
										PRINTLN("[Mission_Music] - Setting from MUSIC_ACTION to MUSIC_VEHICLE_CHASE")
									ENDIF
									#ENDIF
									SET_MUSIC_STATE(MUSIC_VEHICLE_CHASE)
								ELSE
									bReinit = TRUE
								ENDIF
							ENDIF
						ENDIF 
						
						IF MC_PlayerBD[iPartToUse].iMusicState = MUSIC_VEHICLE_CHASE
							IF  MC_playerBD[iPartToUse].bHasPlayerReceivedBattleEvent
							OR 	MC_playerBD[iPartToUse].iWanted > 0
							OR  MC_playerBD[iPartToUse].iFakeWanted > 0
							OR GET_TOTAL_NUMBER_OF_GANG_CHASE_CHASING_ALL_TEAMS() > 0
								IF 	MC_playerBD[iPartToUse].iWanted > 3 
								OR  MC_playerBD[iPartToUse].iFakeWanted > 3 
								OR GET_TOTAL_NUMBER_OF_GANG_CHASE_CHASING_ALL_TEAMS() > 0
								OR bUseDefaultHigh
									bReinit = TRUE
								ELSE
									#IF IS_DEBUG_BUILD
									IF bEnableScriptAudioSpammyPrints
										PRINTLN("[Mission_Music] - Setting from MUSIC_VEHICLE_CHASE to MUSIC_ACTION")
									ENDIF
									#ENDIF	
									SET_MUSIC_STATE(MUSIC_ACTION)
								ENDIF
							ENDIF
						ENDIF
						
						IF bReinit
							#IF IS_DEBUG_BUILD
							IF bEnableScriptAudioSpammyPrints
								PRINTLN("[Mission_Music] - Reinitiatizing Timer")
							ENDIF
							#ENDIF						
							REINIT_NET_TIMER(tdMusicTimer)
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF mc_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES 
					IF NOT bOverrideWithAggro
						IF bStart
							#IF IS_DEBUG_BUILD
							IF bEnableScriptAudioSpammyPrints
								PRINTLN("[Mission_Music] - Else 1")
								PRINTLN("[Mission_Music] - Calling SET_MUSIC_STATE passing in GET_MUSIC_MOOD_FOR_OBJECTIVE: ", GET_MUSIC_MOOD_FOR_OBJECTIVE())
							ENDIF
							#ENDIF
							
							SET_MUSIC_STATE(GET_MUSIC_MOOD_FOR_OBJECTIVE())
						ELIF bMid
							#IF IS_DEBUG_BUILD
							IF bEnableScriptAudioSpammyPrints
								PRINTLN("[Mission_Music] - Else 2")
								PRINTLN("[Mission_Music] - Calling SET_MUSIC_STATE passing in iMusicMid set rule")
							ENDIF
							#ENDIF
							
							SET_MUSIC_STATE(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iMusicMid[mc_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]])
						ENDIF
					ENDIF
				ENDIF
			ENDIF		

			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciUSE_COUNTDOWN)
			
				IF mc_serverBD_4.iCurrentHighestPriority[GET_LOCAL_PLAYER_TEAM()] < FMMC_MAX_RULES
					IF HAS_NET_TIMER_STARTED( MC_serverBD_3.tdObjectiveLimitTimer[GET_LOCAL_PLAYER_TEAM()])
					OR HAS_NET_TIMER_STARTED( MC_serverBD_3.tdMultiObjectiveLimitTimer[GET_LOCAL_PLAYER_TEAM()])
						
						IF IS_LOCAL_PLAYER_OBJECTIVE_TIMER_RUNNING()
					  	 	ms = GET_CURRENT_OBJECTIVE_TIMER_TIME_REMAINING(GET_LOCAL_PLAYER_TEAM())
						ELSE
							ms = GET_CURRENT_MULTIRULE_TIMER_TIME_REMAINING(GET_LOCAL_PLAYER_TEAM())
						ENDIF
						
						IF ms < (iCountdownBleepTime * 1000)
						AND iCountdownBleepTime > 1
							PLAY_SOUND_FRONTEND(-1,"5_Second_Timer","DLC_HEISTS_GENERAL_FRONTEND_SOUNDS", FALSE)
							PRINTLN("[Mission_Music] - PROCESS_MUSIC Play 5_Second_Timer iCountdownBleepTime: ", iCountdownBleepTime)
							iCountdownBleepTime--
						ENDIF
						
						// If we're using specific ones don't call to change the music_mid_countdown.
						IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciUSE_FREEMODE_30S_COUNTDOWN)
							IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
								//MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]]
								IF ms < 30000
									
									IF IS_RULE_FINAL_RULE( MC_serverBD_4.iCurrentHighestPriority[mc_playerBD[iPartToUse].iteam] )
										SET_MUSIC_STATE(MUSIC_CTF_ENDING)
										
										SET_BIT(ilocalboolcheck,LBOOL_TRIGGER_CTF_END_MUSIC)
										SET_BIT(iLocalBoolCheck19, LBOOL19_MUSIC_COUNTDOWN_INITIATED)
										
										PRINTLN("[Mission_Music] - Triggering End Rule mission countdown score on rule: ", MC_serverBD_4.iCurrentHighestPriority[mc_playerBD[iPartToUse].iteam] )
									ELSE
										iMidCountdownRuleNum = MC_serverBD_4.iCurrentHighestPriority[mc_playerBD[iPartToUse].iteam]
										iMidCountdownLastScore = MC_PlayerBD[iPartToUse].iMusicState
										SET_MUSIC_STATE(MUSIC_MID_COUNTDOWN)
										SET_BIT(iLocalBoolCheck19, LBOOL19_MUSIC_COUNTDOWN_INITIATED)
										PRINTLN("[Mission_Music] - Triggering mid mission countdown score on rule: ", iMidCountdownRuleNum )
									ENDIF
									
									REINIT_NET_TIMER( tdMusicTimer )

								ELIF ms < 32000
									IF IS_RULE_FINAL_RULE( MC_serverBD_4.iCurrentHighestPriority[mc_playerBD[iPartToUse].iteam] )
										IF NOT IS_BIT_SET(ilocalboolcheck,LBOOL_TRIGGER_CTF_END_MUSIC)
											TRIGGER_MUSIC_EVENT("MP_PRE_COUNTDOWN_RADIO")
											PRINTLN("[Mission_Music] - TRIGGER_MUSIC_EVENT(\"MP_PRE_COUNTDOWN_RADIO\")")
											SET_BIT(ilocalboolcheck,LBOOL_TRIGGER_CTF_END_MUSIC)
											SET_BIT(iLocalBoolCheck19, LBOOL19_MUSIC_COUNTDOWN_INITIATED)
										ENDIF
									ENDIF
									
									REINIT_NET_TIMER(tdMusicTimer)
								ELIF ms < 35000
									IF IS_RULE_FINAL_RULE( MC_serverBD_4.iCurrentHighestPriority[mc_playerBD[iPartToUse].iteam] )
										IF NOT IS_BIT_SET(ilocalboolcheck,LBOOL_TRIGGER_CTF_PRE_END_MUSIC)
											SET_AUDIO_FLAG("AllowScoreAndRadio", TRUE)
											TRIGGER_MUSIC_EVENT("PRE_MP_DM_COUNTDOWN_30_SEC")
											PRINTLN("[Mission_Music] - TRIGGER_MUSIC_EVENT(\"MP_PRE_COUNTDOWN_RADIO\")")
											SET_BIT(ilocalboolcheck,LBOOL_TRIGGER_CTF_PRE_END_MUSIC)
											SET_BIT(iLocalBoolCheck19, LBOOL19_MUSIC_COUNTDOWN_INITIATED)
										ENDIF
									ENDIF
									
									REINIT_NET_TIMER(tdMusicTimer)
								ENDIF
							ELSE
								IF ms < 30000 
								
									IF IS_RULE_FINAL_RULE( MC_serverBD_4.iCurrentHighestPriority[mc_playerBD[iPartToUse].iteam] )
										iMidCountdownLastScore = MC_PlayerBD[iPartToUse].iMusicState
										SET_MUSIC_STATE(MUSIC_CTF_ENDING)
										SET_BIT(ilocalboolcheck,LBOOL_TRIGGER_CTF_END_MUSIC)
										SET_BIT(iLocalBoolCheck19, LBOOL19_MUSIC_COUNTDOWN_INITIATED)
										PRINTLN("[Mission_Music] - Triggering End Rule mission countdown score on rule: ", MC_serverBD_4.iCurrentHighestPriority[mc_playerBD[iPartToUse].iteam] )
									ELSE
										iMidCountdownRuleNum = MC_serverBD_4.iCurrentHighestPriority[mc_playerBD[iPartToUse].iteam]
										SET_MUSIC_STATE(MUSIC_MID_COUNTDOWN)
										SET_BIT(iLocalBoolCheck19, LBOOL19_MUSIC_COUNTDOWN_INITIATED)
										PRINTLN("[Mission_Music] - Triggering mid mission countdown score on rule: ", iMidCountdownRuleNum )
									ENDIF							
									
									REINIT_NET_TIMER(tdMusicTimer)
								ELIF ms < 35000
									IF IS_RULE_FINAL_RULE( MC_serverBD_4.iCurrentHighestPriority[mc_playerBD[iPartToUse].iteam] )
										IF NOT IS_BIT_SET(ilocalboolcheck,LBOOL_TRIGGER_CTF_PRE_END_MUSIC)

											SET_AUDIO_FLAG("AllowScoreAndRadio", TRUE)
											TRIGGER_MUSIC_EVENT("PRE_MP_DM_COUNTDOWN_30_SEC")
											PRINTLN("[Mission_Music] - TRIGGER_MUSIC_EVENT(\"PRE_MP_DM_COUNTDOWN_30_SEC\")")
											SET_BIT(ilocalboolcheck,LBOOL_TRIGGER_CTF_PRE_END_MUSIC)
											SET_BIT(iLocalBoolCheck19, LBOOL19_MUSIC_COUNTDOWN_INITIATED)
										ENDIF
									ENDIF
									
									REINIT_NET_TIMER(tdMusicTimer)
								ENDIF
							ENDIF
						ELSE
							PRINTLN("[Mission_Music] - ms < 30000 CountDown music should start but we are already playing creator set versions.")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF MC_PlayerBD[iPartToUse].iMusicState = MUSIC_MID_COUNTDOWN
				IF MC_serverBD_4.iCurrentHighestPriority[mc_playerBD[iPartToUse].iteam] > iMidCountdownRuleNum
					PRINTLN("[Mission_Music] - Changing the music state back to ", iMidCountdownLastScore)
					
					CLEAR_BIT(iLocalBoolCheck3, LBOOL3_MUSIC_PLAYING)
					SET_MUSIC_STATE(iMidCountdownLastScore)
				ENDIF			
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(iLocalBoolCheck, LBOOL_MUSIC_CHANGED)	
	AND NOT (IS_BIT_SET(iLocalBoolCheck14, LBOOL14_HAS_STARTED_APT_COUNTDOWN_MUSIC) // Countdown musics have an inbuilt stop event.
	AND NOT IS_BIT_SET(iLocalBoolCheck15, LBOOL15_HAS_STARTED_VAL_COUNTDOWN_MUSIC)
	AND NOT IS_BIT_SET(iLocalBoolCheck20, LBOOL20_HAS_STARTED_IE_COUNTDOWN_MUSIC))
		BOOL bNotChanged
			
		SWITCH MC_PlayerBD[iPartToUse].iMusicState
			
			CASE MUSIC_SUSPENSE
			CASE MUSIC_VEHICLE_CHASE
			CASE MUSIC_AIRBORNE
			CASE MUSIC_ACTION
			CASE MUSIC_DRIVING     
			CASE MUSIC_MED_INTENSITY 
			CASE MUSIC_SUSPENSE_LOW  
			CASE MUSIC_SUSPENSE_HIGH
			CASE MUSIC_SAFE_ROOM
			CASE MUSIC_SAFE_ROOM_AGGRO
			CASE MUSIC_VEHICLE_CHASE_ALT
				IF AUDIO_IS_SCRIPTED_MUSIC_PLAYING()
				AND NOT IS_BIT_SET(iLocalBoolCheck3, LBOOL3_MUSIC_PLAYING)
					IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
						MC_SCRIPT_TRIGGER_MUSIC_EVENT_STOP()
						PRINTLN("[Mission_Music] - TRIGGER_MUSIC_EVENT(MP_MC_STOP) - A")
					ENDIF					
					bNotChanged = TRUE		
				ELSE
					IF NOT IS_BIT_SET(iLocalBoolCheck3, LBOOL3_MUSIC_PLAYING)
						PRINTLN( "[PROCESS_MUSIC] [JJT][MUSIC] No music playing, setting music to ", g_FMMC_STRUCT.iAudioScore )
						SET_MISSION_MUSIC(g_FMMC_STRUCT.iAudioScore, IS_BIT_SET(iLocalBoolCheck13, LBOOL13_REINITTED_MUSIC_FOR_ENTERING_SPECTATE))
						SET_BIT(iLocalBoolCheck3, LBOOL3_MUSIC_PLAYING)
					ENDIF
					IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
						IF NOT IS_BIT_SET(iLocalBoolCheck19, LBOOL19_MUSIC_COUNTDOWN_INITIATED)
						AND NOT IS_BIT_SET(iLocalBoolCheck14, LBOOL14_HAS_STARTED_APT_COUNTDOWN_MUSIC)
							START_AUDIO_SCENE("MP_POSITIONED_RADIO_MUTE_SCENE")
							TRIGGER_MUSIC_EVENT(GET_MOOD_NAME_FOR_SONG(g_FMMC_STRUCT.iAudioScore, MC_PlayerBD[iPartToUse].iMusicState))
							PRINTLN("[Mission_Music] - TRIGGER_MUSIC_EVENT(", GET_MOOD_NAME_FOR_SONG(g_FMMC_STRUCT.iAudioScore, MC_PlayerBD[iLocalPart].iMusicState), ")")
							SET_AUDIO_FLAG("AllowScoreAndRadio", FALSE)
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE MUSIC_SILENT
				IF AUDIO_IS_SCRIPTED_MUSIC_PLAYING()
				AND NOT IS_BIT_SET(iLocalBoolCheck3,LBOOL3_MUSIC_PLAYING)
					MC_SCRIPT_TRIGGER_MUSIC_EVENT_STOP()
					bNotChanged = TRUE
					PRINTLN("[Mission_Music] - TRIGGER_MUSIC_EVENT(MP_MC_STOP) - B")
					PRINTLN("[Mission_Music] - TRIGGER_MUSIC_EVENT(MP_MC_STOP)")
				ELSE
					IF NOT IS_BIT_SET(iLocalBoolCheck3,LBOOL3_MUSIC_PLAYING)
						SET_MISSION_MUSIC(g_FMMC_STRUCT.iAudioScore, FALSE)
						SET_BIT(iLocalBoolCheck3,LBOOL3_MUSIC_PLAYING)
						PRINTLN("[Mission_Music] - SET_MISSION_MUSIC(", g_FMMC_STRUCT.iAudioScore, ")")
					ENDIF
					CLEAR_BIT(iLocalBoolCheck29, LBOOL29_MUSIC_LOCKER_MUSIC_STARTED)
					STOP_AUDIO_SCENE("MP_POSITIONED_RADIO_MUTE_SCENE")
					PRINTLN("[Mission_Music] - STOP_AUDIO_SCENE(MP_POSITIONED_RADIO_MUTE_SCENE)")
					TRIGGER_MUSIC_EVENT(GET_MOOD_NAME_FOR_SONG(g_FMMC_STRUCT.iAudioScore, MC_PlayerBD[iPartToUse].iMusicState))
					PRINTLN("[PROCESS_MUSIC] TRIGGER_MUSIC_EVENT(", GET_MOOD_NAME_FOR_SONG(g_FMMC_STRUCT.iAudioScore, MC_PlayerBD[iPartToUse].iMusicState), ")")
					PRINTLN("[Mission_Music] - TRIGGER_MUSIC_EVENT(", GET_MOOD_NAME_FOR_SONG(g_FMMC_STRUCT.iAudioScore, MC_PlayerBD[iPartToUse].iMusicState), ")")
					SET_AUDIO_FLAG("AllowScoreAndRadio", TRUE)					
					PRINTLN("[Mission_Music] - SET_AUDIO_FLAG(AllowScoreAndRadio, TRUE)")
				ENDIF
			BREAK
			
			CASE MUSIC_STOP
				STOP_AUDIO_SCENE("MP_POSITIONED_RADIO_MUTE_SCENE")
				CLEAR_BIT(iLocalBoolCheck29, LBOOL29_MUSIC_LOCKER_MUSIC_STARTED)
				MC_SCRIPT_TRIGGER_MUSIC_EVENT_STOP()
				PRINTLN("[Mission_Music] - TRIGGER_MUSIC_EVENT(\"MP_MC_STOP\")")
				SET_AUDIO_FLAG("AllowScoreAndRadio", TRUE)
				PRINTLN("[Mission_Music] - TRIGGER_MUSIC_EVENT(MP_MC_STOP) - C")
			BREAK
			CASE MUSIC_FAIL
				STOP_AUDIO_SCENE("MP_POSITIONED_RADIO_MUTE_SCENE")
				IF NOT IS_BIT_SET(iLocalBoolCheck14, LBOOL14_HAS_STARTED_APT_COUNTDOWN_MUSIC)
					SCRIPT_TRIGGER_MUSIC_EVENT_FAIL()
				ENDIF
				PRINTLN("[Mission_Music] - TRIGGER_MUSIC_EVENT(\"MP_MC_FAIL\") Pm1")
				SET_AUDIO_FLAG("AllowScoreAndRadio", TRUE)
			BREAK
			
			CASE MUSIC_MID_COUNTDOWN			
			CASE MUSIC_CTF_ENDING
				
				START_AUDIO_SCENE("MP_POSITIONED_RADIO_MUTE_SCENE")
				TRIGGER_MUSIC_EVENT("MP_DM_COUNTDOWN_30_SEC")
				PRINTLN("[Mission_Music] - TRIGGER_MUSIC_EVENT(\"MP_DM_COUNTDOWN_30_SEC\")")
				SET_AUDIO_FLAG("AllowScoreAndRadio", FALSE)
				
			BREAK
			
		/*	
			CASE MUSIC_ALT_COUNTDOWN
				START_AUDIO_SCENE("MP_POSITIONED_RADIO_MUTE_SCENE")
				TRIGGER_MUSIC_EVENT("FM_COUNTDOWN_30S")
				PRINTLN("[RCC MISSION] TRIGGER_MUSIC_EVENT(\"FM_COUNTDOWN_30S\")")
				SET_AUDIO_FLAG("AllowScoreAndRadio", FALSE)
			BREAK
		*/	
			CASE MUSIC_COCK_SONG
				IF AUDIO_IS_SCRIPTED_MUSIC_PLAYING()
					MC_SCRIPT_TRIGGER_MUSIC_EVENT_STOP()
					bNotChanged = TRUE
					PRINTLN("[Mission_Music] - MUSIC - TRIGGER_MUSIC_EVENT(MP_MC_STOP) - D")
					PRINTLN("[Mission_Music] - TRIGGER_MUSIC_EVENT(\"MP_MC_STOP\")")
				ELSE
					CLEAR_BIT(iLocalBoolCheck3, LBOOL3_MUSIC_PLAYING)
					START_AUDIO_SCENE("MP_POSITIONED_RADIO_MUTE_SCENE")
					TRIGGER_MUSIC_EVENT("MP_MC_START_COCK_SONG_1")
					PRINTLN("[Mission_Music] - TRIGGER_MUSIC_EVENT(\"MP_MC_START_COCK_SONG_1\")")
					TRIGGER_MUSIC_EVENT("MP_MC_GENERAL_1")
					PRINTLN("[Mission_Music] - TRIGGER_MUSIC_EVENT(\"MP_DM_COUNMP_MC_GENERAL_1TDOWN_30_SEC\")")
					SET_AUDIO_FLAG("AllowScoreAndRadio", FALSE)
				ENDIF
			BREAK
			
			CASE MUSIC_WAVERY
				IF AUDIO_IS_SCRIPTED_MUSIC_PLAYING()
					MC_SCRIPT_TRIGGER_MUSIC_EVENT_STOP()
					bNotChanged = TRUE
					PRINTLN("[Mission_Music] - TRIGGER_MUSIC_EVENT(MP_MC_STOP) - E")
				ELSE
					CLEAR_BIT(iLocalBoolCheck3, LBOOL3_MUSIC_PLAYING)
					START_AUDIO_SCENE("MP_POSITIONED_RADIO_MUTE_SCENE")
					TRIGGER_MUSIC_EVENT("MP_MC_START_WAVERY_1")
					PRINTLN("[Mission_Music] - TRIGGER_MUSIC_EVENT(\"MP_MC_START_WAVERY_1\")")
					TRIGGER_MUSIC_EVENT("MP_MC_GENERAL_1")
					PRINTLN("[Mission_Music] - TRIGGER_MUSIC_EVENT(\"MP_MC_GENERAL_1\")")
					SET_AUDIO_FLAG("AllowScoreAndRadio", FALSE)
				ENDIF
			BREAK

 			CASE MUSIC_SEA_RACE	
				IF AUDIO_IS_SCRIPTED_MUSIC_PLAYING()
					MC_SCRIPT_TRIGGER_MUSIC_EVENT_STOP()
					PRINTLN("[Mission_Music] - TRIGGER_MUSIC_EVENT(\"MP_MC_STOP\")")
					bNotChanged = TRUE
					PRINTLN("[Mission_Music] - TRIGGER_MUSIC_EVENT(MP_MC_STOP) - F")
				ELSE
					CLEAR_BIT(iLocalBoolCheck3, LBOOL3_MUSIC_PLAYING)
					START_AUDIO_SCENE("MP_POSITIONED_RADIO_MUTE_SCENE")
					PRINTLN("[Mission_Music] -TRIGGER_MUSIC_EVENT(\"MP_MC_START_TOUGHT_SEA_RACE_1\")")
					TRIGGER_MUSIC_EVENT("MP_MC_START_TOUGHT_SEA_RACE_1")
					TRIGGER_MUSIC_EVENT("MP_MC_GENERAL_1")
					PRINTLN("[Mission_Music] - TRIGGER_MUSIC_EVENT(\"MP_MC_GENERAL_1\")")
					SET_AUDIO_FLAG("AllowScoreAndRadio", FALSE)
				ENDIF
			BREAK
			
			CASE MUSIC_MOOD_NONE
				IF AUDIO_IS_SCRIPTED_MUSIC_PLAYING()
					PRINTLN("[Mission_Music] - PROCESS_MUSIC - No music mood, not doing anything")
					//PRINTLN("[PROCESS_MUSIC] TRIGGER_MUSIC_EVENT(\"MP_MC_STOP\")")
					bNotChanged = FALSE
					//PRINTLN("[PROCESS_MUSIC] [RCC MISSION] MUSIC - TRIGGER_MUSIC_EVENT(MP_MC_STOP) - E")
				ELSE
					CLEAR_BIT(iLocalBoolCheck3, LBOOL3_MUSIC_PLAYING)
				ENDIF
			BREAK
			
		ENDSWITCH
		
		IF NOT bNotChanged
			PRINTLN("[Mission_Music] - NEW MUSIC SETTING APPLIED")
			SET_BIT(iLocalBoolCheck, LBOOL_MUSIC_CHANGED) 
		ENDIF
		
	ENDIF

	//If we aren't showing the timer, don't play the countdown timer bleeps
	IF NOT IS_SPECTATOR_HUD_HIDDEN()
	AND NOT IS_SPECTATOR_SHOWING_NEWS_HUD() 
	AND (MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES) //To stop an array overrun on the next line
	AND ((NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitset[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE_HIDE_HUD))
		  OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitset[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE_OVERRIDE_HIDEHUD_SHOW_MULTIRULE_TIMER)
		  OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciUSE_CIRCLE_HUD_TIMER))
		
		//Countdown sounds:	
		PROCESS_COUNTDOWN_TIMER_SOUNDS()
	ENDIF
	
	IF MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
	
		//[FMMC2020] Requires renaming to make it sound as generic as it is rather than being named after Drop Zone
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitsetFive[MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE5_MUSIC_CAPTURE_BAR_CUES)
			INT iCapProgress = MC_ServerBD.iCaptureBarPercentage[MC_playerBD[iPartToUse].iteam]
			VECTOR vTempPos = GET_ENTITY_COORDS(LocalPlayerPed)
			IF VDIST(vTempPos, vDZPlayerSpawnLoc) < 100
			AND iDropZoneMusicProgress = 2
				iDropZoneMusicProgress = 0
			ENDIF
			SWITCH iDropZoneMusicProgress
				CASE 0
					IF iDropZoneMusicEvent != ciDROPZONE_HELI
						TRIGGER_MUSIC_EVENT("DROPZONE_HELI")
						iDropZoneMusicEvent = ciDROPZONE_HELI
					ENDIF
					IF IS_PED_IN_PARACHUTE_FREE_FALL(LocalPlayerPed)
						iDropZoneMusicProgress = 1
					ENDIF
				BREAK
				CASE 1
					IF iDropZoneMusicEvent != ciDROPZONE_JUMP
						TRIGGER_MUSIC_EVENT("DROPZONE_JUMP")
						iDropZoneMusicEvent = ciDROPZONE_JUMP
					ENDIF
					IF NOT IS_ENTITY_IN_AIR(LocalPlayerPed)
						iDropZoneMusicProgress = 2
					ENDIF
				BREAK
				CASE 2
					IF iCapProgress < 60
						IF iDropZoneMusicEvent != ciDROPZONE_LAND
							TRIGGER_MUSIC_EVENT("DROPZONE_LAND")
							iDropZoneMusicEvent = ciDROPZONE_LAND
						ENDIF
					ELIF iCapProgress >= 60 AND iCapProgress < 75
						IF iDropZoneMusicEvent != ciDROPZONE_ACTION
							TRIGGER_MUSIC_EVENT("DROPZONE_ACTION")
							iDropZoneMusicEvent = ciDROPZONE_ACTION
						ENDIF
					ELIF iCapProgress >= 75
						IF iDropZoneMusicEvent != ciDROPZONE_ACTION_HIGH
							TRIGGER_MUSIC_EVENT("DROPZONE_ACTION_HIGH")
							iDropZoneMusicEvent = ciDROPZONE_ACTION_HIGH
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF

ENDPROC


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: SFX  ------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Functions relating to the player target of the gang chase, and deciding where to spawn the gang backup -----------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC MAINTAIN_DEFUNCT_BASE_BODY_SCANNER()
	
	IF NOT HAS_NET_TIMER_STARTED(tdMissionBodyScannerTimer)
		// Check if the local player is inside the body scanner. This check 
		// is quicker than staggering through all players in the facility.
						
		IF IS_ENTITY_IN_ANGLED_AREA(LocalPlayerPed, sMissionBodyScannerData.vMin, sMissionBodyScannerData.vMax, sMissionBodyScannerData.fWidth)
			START_NET_TIMER(tdMissionBodyScannerTimer)	
						
			PRINTLN("[RCC MISSION] starting tdMissionBodyScannerTimer")

			PLAY_SOUND_FROM_COORD(-1, "scanner_alarm_os", sMissionBodyScannerData.vAudioSource, "dlc_xm_iaa_player_facility_sounds", TRUE, 100, FALSE)
			
			BROADCAST_EVENT_FMMC_TRIGGERED_BODY_SCANNER()
		ELSE
			PRINTLN("[LM][RCC MISSION] - [Audio] - [Facility Property] - temp - not in area")
		ENDIF
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(tdMissionBodyScannerTimer)
		IF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdMissionBodyScannerTimer, 3500)
			RESET_NET_TIMER(tdMissionBodyScannerTimer)
		ENDIF
	ENDIF
ENDPROC

PROC PLAY_RED_FILTER_SOUND()
	STRING sSoundSet

	sSoundSet = "DLC_Apartments_Drop_Zone_Sounds"
	PLAY_SOUND_FRONTEND(-1,"Enter_Capture_Zone",sSoundSet,FALSE)
	PRINTLN("[RCC MISSION] PLAY_RED_FILTER_SOUND - Playing sound from Soundset: ", sSoundSet)

	SET_BIT(iLocalBoolCheck27, LBOOL27_RED_FILTER_SOUND_PLAYED)
ENDPROC

PROC PLAY_MIN_SPEED_SOUND(INT soundId, STRING soundName, ENTITY_INDEX ei, INT iCooldown = 0)
	STRING sSoundSet = "GTAO_Speed_Race_Sounds"
	BOOL bSoundsOverNetwork = TRUE
	INT iSoundNetworkRange = 20
	
	IF g_bMissionEnding
	OR NOT GET_TOGGLE_PAUSED_RENDERPHASES_STATUS()
		PRINTLN("PLAY_MIN_SPEED_SOUND - Exiting early because the mission is ending or the renderphase is paused")
		EXIT
	ENDIF
	
	IF NOT HAS_NET_TIMER_STARTED(stMinSpeedSndCooldown)
	OR HAS_NET_TIMER_EXPIRED(stMinSpeedSndCooldown, iCooldown)
	
		PLAY_SOUND_FROM_ENTITY(soundId, soundName, ei, sSoundSet, bSoundsOverNetwork, iSoundNetworkRange)
		
		IF iCooldown > 0
			REINIT_NET_TIMER(stMinSpeedSndCooldown)
		ENDIF
	ENDIF
	
	PRINTLN("PLAY_MIN_SPEED_SOUND - soundId: ", soundId, " soundName: ", soundName)
	DEBUG_PRINTCALLSTACK()
ENDPROC

PROC PROCESS_SPEED_BAR_SOUNDS(BOOL bInDangerZone, VEHICLE_INDEX viSpeedVehicle, STRING sSoundSet)
	IF bInDangerZone
		IF NOT IS_BIT_SET(iLocalBoolCheck13, LBOOL13_MIN_SPEED_EXPLODE_BAR_INCREASING)
			IF IS_SOUND_ID_VALID(iSoundIDVehBomb)
				STOP_SOUND(iSoundIDVehBomb)
			ENDIF
			IF IS_SOUND_ID_VALID(iSoundIDVehBomb2)
				STOP_SOUND(iSoundIDVehBomb2)
			ENDIF
			
			IF IS_PLAYER_IN_CREATOR_TRAILER(PlayerToUse)
			OR IS_PLAYER_IN_CREATOR_AIRCRAFT(PlayerToUse)
				IF NOT HAS_NET_TIMER_STARTED(stMinSpeedSndCooldown)
				OR HAS_NET_TIMER_EXPIRED(stMinSpeedSndCooldown, 2500)
					PLAY_SOUND_FRONTEND(iSoundIDVehBomb, "Armed", sSoundSet)
					REINIT_NET_TIMER(stMinSpeedSndCooldown)
				ENDIF
			ELSE
				PLAY_MIN_SPEED_SOUND(iSoundIDVehBomb, "Armed", viSpeedVehicle, 2500)
			ENDIF
			
			SET_BIT(iLocalBoolCheck13, LBOOL13_MIN_SPEED_EXPLODE_BAR_INCREASING)
		ELSE
			IF IS_SOUND_ID_VALID(iSoundIDVehBomb)
			AND HAS_SOUND_FINISHED(iSoundIDVehBomb)
				IF IS_SOUND_ID_VALID(iSoundIDVehBomb2)
				AND HAS_SOUND_FINISHED(iSoundIDVehBomb2)
					IF IS_PLAYER_IN_CREATOR_TRAILER(PlayerToUse)
					OR IS_PLAYER_IN_CREATOR_AIRCRAFT(PlayerToUse)
						PLAY_SOUND_FRONTEND(iSoundIDVehBomb2, "Countdown", sSoundSet)
					ELSE
						PLAY_MIN_SPEED_SOUND(iSoundIDVehBomb2, "Countdown", viSpeedVehicle)
					ENDIF						
				ENDIF
				SET_VARIABLE_ON_SOUND(iSoundIDVehBomb2, "Ctrl", SQRT(fDisplayExplodeProgress / 100.0))
			ENDIF
		ENDIF
		
		RESET_NET_TIMER(tdMOCCountdownStopTimer)
	ELSE
		IF IS_BIT_SET(iLocalBoolCheck13, LBOOL13_MIN_SPEED_EXPLODE_BAR_INCREASING)
			IF NOT HAS_NET_TIMER_STARTED(tdMOCCountdownStopTimer)
				START_NET_TIMER(tdMOCCountdownStopTimer)
			ENDIF
			
			IF HAS_NET_TIMER_STARTED(tdMOCCountdownStopTimer)
				IF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdMOCCountdownStopTimer, ciMOC_COUNTDOWN_STOP_TIME)
					IF IS_SOUND_ID_VALID(iSoundIDVehBomb)
						STOP_SOUND(iSoundIDVehBomb)
					ENDIF
					IF IS_SOUND_ID_VALID(iSoundIDVehBomb2)
						STOP_SOUND(iSoundIDVehBomb2)
					ENDIF
					
					IF IS_PLAYER_IN_CREATOR_TRAILER(PlayerToUse)
					OR IS_PLAYER_IN_CREATOR_AIRCRAFT(PlayerToUse)
						IF NOT HAS_NET_TIMER_STARTED(stMinSpeedSndCooldown)
						OR HAS_NET_TIMER_EXPIRED(stMinSpeedSndCooldown, 1000)
							PLAY_SOUND_FRONTEND(-1, "Count_Stop", sSoundSet)
							REINIT_NET_TIMER(stMinSpeedSndCooldown)
						ENDIF
					ELSE
						PLAY_MIN_SPEED_SOUND(-1, "Count_Stop", viSpeedVehicle, 1000)
					ENDIF											
					CLEAR_BIT(iLocalBoolCheck13, LBOOL13_MIN_SPEED_EXPLODE_BAR_INCREASING)
					RESET_NET_TIMER(tdMOCCountdownStopTimer)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF fDisplayExplodeProgress >= 100
		IF IS_SOUND_ID_VALID(iSoundIDVehBomb)
			STOP_SOUND(iSoundIDVehBomb)
		ENDIF
		IF IS_SOUND_ID_VALID(iSoundIDVehBomb2)
			STOP_SOUND(iSoundIDVehBomb2)
		ENDIF
		
		IF IS_PLAYER_IN_CREATOR_TRAILER(PlayerToUse)
		OR IS_PLAYER_IN_CREATOR_AIRCRAFT(PlayerToUse)
			IF NOT IS_BIT_SET(iLocalBoolCheck23, LBOOL23_SPEED_BAR_AIRSTRIKE_CALLED)
			AND NOT g_bMissionEnding
				PLAY_SOUND_FRONTEND(iSoundIDVehBeacon, "Beacon", sSoundSet)
				SET_BIT(iLocalBoolCheck23, LBOOL23_SPEED_BAR_AIRSTRIKE_CALLED)
				MC_serverBD_4.fExplodeProgress = 100.0
				MC_serverBD_4.iExplodeUpdateIter++
			ENDIF
		ENDIF
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: HALLOWEEN  ------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Functions relating to Halloween. heartbeat controller vibration---------------------------------------------------------------------------------------------------
// ##### Hunted player: heartbeat controller vibration
// ##### Hunted player: friendly vibration (in radius)
// ##### Hunter player: heartbeat sounds from hunted players
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL private_DO_RUN_HALLOWEEN_LOGIC()		//	[ML] Should this be in audio?

	IF NOT IS_BIT_SET(MC_serverbd.iServerBitSet, SBBOOL_FIRST_UPDATE_DONE)
		RETURN FALSE
	ENDIF
	
	//Hard block, lets us print a message once and then not come into here again
	IF IS_BIT_SET( iHrtbBitSet, ciHRTB_MODE_NOT_ACTIVE )
		RETURN FALSE
	ELSE	
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciHWN_HEARTBEAT_VIBRATE_ENABLED) 
		AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciHWN_FRIENDLY_VIBRATE_ENABLED)
			PRINTLN("[JJT] Mission is not a halloween mission.")
			SET_BIT( iHrtbBitSet, ciHRTB_MODE_NOT_ACTIVE )
			RETURN FALSE
		ENDIF
	ENDIF

	IF bIsAnySpectator
		IF iPartToUse >-1
			IF GET_MC_CLIENT_GAME_STATE( iPartToUse ) != GAME_STATE_RUNNING
			OR GET_MC_CLIENT_GAME_STATE( iPartToUse ) = GAME_STATE_END
				PRINTLN("[JJT] PROCESS_HALLOWEEN_MODE_LOGIC mission over (state=", GET_MC_CLIENT_GAME_STATE( iLocalPart ), 
						",missionending=", g_bMissionEnding, " 2)")
				RETURN FALSE
			ENDIF
		ENDIF
	ELSE
		//Stop running if the game has ended
		IF GET_MC_CLIENT_GAME_STATE( iLocalPart ) = GAME_STATE_END
		OR GET_MC_CLIENT_GAME_STATE( iLocalPart ) != GAME_STATE_RUNNING
		OR g_bMissionEnding
			#IF IS_DEBUG_BUILD
				PRINTLN("[JJT] PROCESS_HALLOWEEN_MODE_LOGIC mission over (state=", GET_MC_CLIENT_GAME_STATE( iLocalPart ), 
						",missionending=", g_bMissionEnding, " 1)")
			#ENDIF
			RETURN FALSE
		ENDIF
	ENDIF
	
	//Exit if spectating the jason
	IF iSpectatorTarget != -1
		IF iSpectatorTarget < MAX_NUM_MC_PLAYERS
		AND iSpectatorTarget >= 0
			IF MC_PlayerBD[iSpectatorTarget].iteam = 0
				PRINTLN("[JJT] Spectating the scary ped so not running halloween logic.")
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC FLOAT private_GET_SECOND_HEARTBEAT_TIME(FLOAT distToScaredPed = -1.0)
	FLOAT mod = fHrtbDistanceFromScaryPed
	
	//So the scary ped can use this as a guage to scared peds
	IF distToScaredPed != -1.0
		mod = distToScaredPed
	ENDIF
	
	IF mod < 5.5
		mod = 5.5
	ENDIF	
	RETURN ciHRTB_SECOND_BEAT_BASE_TIME - ( (ciHRTB_PULSE_INCREASE_RADIUS + ( ciHRTB_PULSE_MINIMUM_RADIUS / 2 )
										- mod) * 10 )
ENDFUNC

FUNC FLOAT private_GET_RELOOP_HEARTBEAT_TIME(FLOAT distToScaredPed = -1.0)
	FLOAT mod = fHrtbDistanceFromScaryPed
	
	//So the scary ped can use this as a guage to scared peds
	IF distToScaredPed != -1.0
		mod = distToScaredPed
	ENDIF
	
	IF mod < 5.5
		mod = 5.5
	ENDIF
	RETURN ciHRTB_RELOOP_PULSE_BASE_TIME - ( (ciHRTB_PULSE_INCREASE_RADIUS + ( ciHRTB_PULSE_MINIMUM_RADIUS / 2 )
										 - mod) * 125 )
ENDFUNC

PROC private_HALLOWEEN_INIT_FIND_SCARY_PED()		//	[ML] Should this be in audio?
	//Get the scary ped which should always be on team 0
	IF MC_PlayerBD[iPartToUse].iteam != 0
		piHrtbScaryPedRef = GET_PLAYER_PED( 
								INT_TO_PLAYERINDEX( 
									GET_NEAREST_PLAYER_TO_ENTITY_ON_TEAM( GET_PLAYER_PED( PlayerToUse ), 0 )) )							
		
		IF NOT DOES_ENTITY_EXIST( piHrtbScaryPedRef )
			#IF IS_DEBUG_BUILD
				PRINTLN( "[JJT][Hrtb] Found scary ped, but they don't exist!" )
			#ENDIF
			EXIT
		ENDIF
		
		#IF IS_DEBUG_BUILD
			PRINTLN( "[JJT][Hrtb] Ped isn't scary, and has got a handle to the scary ped." )
		#ENDIF
		
		SET_BIT( iHrtbBitSet, ciHRTB_HAS_HANDLE_TO_SCARY_PED )
	ELSE
		#IF IS_DEBUG_BUILD
			PRINTLN( "[JJT][Hrtb] This local player is the scary ped." )
		#ENDIF
		
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciHWN_ENABLE_HEARTBEAT_BOTH_TEAMS)		
			SET_BIT( iHrtbBitSet, ciHRTB_LOCAL_PED_IS_SCARY_PED )
		ENDIF
		
		EXIT
	ENDIF
ENDPROC

PROC private_FIND_OTHER_TEAM_FOR_HEARTBEAT()		//	[ML] Should this be in audio?
	IF MC_PlayerBD[iPartToUse].iteam = 0
		piHrtbScaryPedRef = GET_PLAYER_PED(INT_TO_PLAYERINDEX(GET_NEAREST_PLAYER_TO_ENTITY_ON_TEAM( GET_PLAYER_PED( PlayerToUse ), 1)))
		SET_BIT( iHrtbBitSet, ciHRTB_HAS_HANDLE_TO_SCARY_PED )
		IF IS_BIT_SET(iHrtbBitSet, ciHRTB_LOCAL_PED_IS_SCARY_PED)
			PRINTLN("[JT HEART] YOU ARE SCARY, NO VIBRATIONS FOR YOU!")
		ENDIF
		PRINTLN("[JT HEART] Team 0 Scary heartbeat")
		IF NOT DOES_ENTITY_EXIST( piHrtbScaryPedRef )
			#IF IS_DEBUG_BUILD
				PRINTLN( "[JT HEART] Found scary ped, but they don't exist!" )
			#ENDIF
			EXIT
		ENDIF
	ELIF MC_PlayerBD[iPartToUse].iteam = 1
		piHrtbScaryPedRef = GET_PLAYER_PED(INT_TO_PLAYERINDEX(GET_NEAREST_PLAYER_TO_ENTITY_ON_TEAM( GET_PLAYER_PED( PlayerToUse ), 0)))
		SET_BIT( iHrtbBitSet, ciHRTB_HAS_HANDLE_TO_SCARY_PED )
		PRINTLN("[JT HEART] Team 1 Scary heartbeat")
		IF IS_BIT_SET(iHrtbBitSet, ciHRTB_LOCAL_PED_IS_SCARY_PED)
			PRINTLN("[JT HEART] YOU ARE SCARY, NO VIBRATIONS FOR YOU!")
		ENDIF
		IF NOT DOES_ENTITY_EXIST( piHrtbScaryPedRef )
			#IF IS_DEBUG_BUILD
				PRINTLN( "[JT][Hrtb] Found scary ped, but they don't exist!" )
			#ENDIF
			EXIT
		ENDIF
	ELSE
		PRINTLN("[JT HEART] Player is not on a valid team")
	ENDIF
ENDPROC

/// PURPOSE:
///    Hunted player logic
PROC private_HALLOWEEN_PROCESS_SCARED_PED()		//	[ML] Should this be in audio?
	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]

	IF iRule < FMMC_MAX_RULES 
	AND iTeam < FMMC_MAX_TEAMS
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciHWN_HEARTBEAT_VIBRATE_ENABLED)
		AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFive[iRule], ciBS_RULE5_DISABLE_HWN_HEARTBEAT)
			
			PED_INDEX tempPedToUse
			IF iSpectatorTarget != -1 //Is a spectator
				tempPedToUse = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iSpectatorTarget)))
			ELSE
				tempPedToUse = LocalPlayerPed
			ENDIF
			
			IF DOES_ENTITY_EXIST( tempPedToUse )
			AND DOES_ENTITY_EXIST( piHrtbScaryPedRef )
			
				IF NOT IS_PED_INJURED( tempPedToUse )
				AND NOT IS_PED_INJURED( piHrtbScaryPedRef )

					//Distance update every 1/6th of a second. For framerate easing
					iHrtbDistanceUpdateTimer++
					IF iHrtbDistanceUpdateTimer > 10
						fHrtbDistanceFromScaryPed = VDIST( GET_ENTITY_COORDS( piHrtbScaryPedRef ), 
														   GET_ENTITY_COORDS( tempPedToUse ) )
														   
						PRINTLN("[JJT] Distance to scary ped is: ", fHrtbDistanceFromScaryPed )								   				   
						iHrtbDistanceUpdateTimer = 0
					ENDIF

					IF fHrtbDistanceFromScaryPed < g_FMMC_STRUCT.iFvjHeartbeatDistance
						FLOAT mod = fHrtbDistanceFromScaryPed
						IF mod < g_FMMC_STRUCT.iFvjHeartbeatMinDistance
							mod = TO_FLOAT( g_FMMC_STRUCT.iFvjHeartbeatMinDistance )
						ENDIF

						IF NOT HAS_NET_TIMER_STARTED( stHrtbVibrationTimer )
							START_NET_TIMER( stHrtbVibrationTimer )
							CLEAR_BIT( iHrtbBitSet, ciHRTB_DONE_SECOND_HEARTBEAT )		
							
							PRINTLN( "[JJT] Heartbeat 1" )
							SET_CONTROL_SHAKE( PLAYER_CONTROL, 
											   100 - FLOOR(ciHRTB_PULSE_INCREASE_RADIUS - mod), 
											   200 )
						ELSE
							IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME( stHrtbVibrationTimer ) > private_GET_SECOND_HEARTBEAT_TIME()
							AND NOT IS_BIT_SET( iHrtbBitSet, ciHRTB_DONE_SECOND_HEARTBEAT )
							
								PRINTLN( "[JJT] Heartbeat 2" )
								SET_CONTROL_SHAKE( PLAYER_CONTROL, 
												   100 - FLOOR(ciHRTB_PULSE_INCREASE_RADIUS - mod), 
												   200 )
								SET_BIT( iHrtbBitSet, ciHRTB_DONE_SECOND_HEARTBEAT )			
							ENDIF

							IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME( stHrtbVibrationTimer ) > private_GET_RELOOP_HEARTBEAT_TIME()
								PRINTLN( "[JJT] Heartbeat reset" )
								RESET_NET_TIMER( stHrtbVibrationTimer )
							ENDIF
						ENDIF
					ENDIF
				ELSE
					PRINTLN("[JJT] Ped injured! : ", (IS_PED_INJURED( tempPedToUse )), 
							", ", (IS_PED_INJURED( piHrtbScaryPedRef )) )
				ENDIF
			ELSE
				PRINTLN("[JJT] Error: entity doesn't exist: ", (DOES_ENTITY_EXIST(tempPedToUse)), 
							", ", (DOES_ENTITY_EXIST( piHrtbScaryPedRef )) )
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciHWN_FRIENDLY_VIBRATE_ENABLED)	
		AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFive[iRule], ciBS_RULE5_DISABLE_HWN_FRIENDLY_VIBRATION)
			IF NOT HAS_NET_TIMER_STARTED( stFrndVibrationTimer )
				START_NET_TIMER( stFrndVibrationTimer )
			ELSE
				//Roughly ten checks for new players every second to lessen computational impact
				IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME( stFrndVibrationTimer ) > 200
					
//					INT tempPedInt = GET_NEAREST_PLAYER_TO_ENTITY_ON_TEAM( GET_PLAYER_PED( PlayerToUse ), MC_PlayerBD[iPartToUse].iteam ) 
//					PLAYER_INDEX tempPedIndex = INT_TO_PLAYERINDEX( tempPedInt )
//					PED_INDEX tempPed = GET_PLAYER_PED( tempPedIndex )
					
					//LOOP THROUGH ALL MISSION PLAYERS
					INT iParticipant
					FOR iParticipant = 0 TO (ciHRTB_MAX_PLAYER_CHECK - 1)
						PARTICIPANT_INDEX participantIndex = INT_TO_PARTICIPANTINDEX(iParticipant)
						
						IF iParticipant < 8
							IF NETWORK_IS_PARTICIPANT_ACTIVE(participantIndex)
								
								PLAYER_INDEX 	playerIndex = NETWORK_GET_PLAYER_INDEX(participantIndex)
								PED_INDEX		playerPedIndex = GET_PLAYER_PED( playerIndex )
								
								IF DOES_ENTITY_EXIST( playerPedIndex )
								AND DOES_ENTITY_EXIST( GET_PLAYER_PED( PlayerToUse ) )
								
									IF NOT IS_PED_INJURED( playerPedIndex )
									AND NOT IS_PED_INJURED( GET_PLAYER_PED( PlayerToUse ) )

										IF VDIST( GET_ENTITY_COORDS( playerPedIndex ), 
										  	      GET_ENTITY_COORDS( GET_PLAYER_PED( PlayerToUse ) ) ) < g_FMMC_STRUCT.iFvjFriendlyVibrDistance
										
											IF NOT IS_BIT_SET( iFrndVibCacheBitSet, iParticipant )
												
												PRINTLN("[JJT] Entered radius of participant ", iParticipant, ". Radius = ", VDIST( GET_ENTITY_COORDS( playerPedIndex ), 
										  	      GET_ENTITY_COORDS( GET_PLAYER_PED( PlayerToUse ) ) ))
												
												SET_CONTROL_SHAKE( PLAYER_CONTROL, 69, 50 )
												RESET_NET_TIMER( stFrndVibrationCDTimer[iParticipant] )

												SET_BIT( iFrndVibCacheBitSet, iParticipant )
											ENDIF
										ELSE
											IF IS_BIT_SET( iFrndVibCacheBitSet, iParticipant )
												IF NOT HAS_NET_TIMER_STARTED( stFrndVibrationCDTimer[iParticipant] )
													START_NET_TIMER( stFrndVibrationCDTimer[iParticipant] )
												ELSE
													IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME( stFrndVibrationCDTimer[iParticipant] ) 
													> g_FMMC_STRUCT.iFvjFriendlyVibrCooldown
														
														PRINTLN("[JJT] Left radius of participant ", iParticipant, ". Radius = ", 
																VDIST( GET_ENTITY_COORDS( playerPedIndex ), 
										  	      				GET_ENTITY_COORDS( GET_PLAYER_PED( PlayerToUse ) ) ))
														
														CLEAR_BIT( iFrndVibCacheBitSet, iParticipant )
														RESET_NET_TIMER( stFrndVibrationCDTimer[iParticipant] )
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ELSE
										PRINTLN("[JJT] Ped injured! : ", (IS_PED_INJURED( playerPedIndex )), 
												", ", (IS_PED_INJURED( GET_PLAYER_PED( PlayerToUse ) )) )
									ENDIF
								ELSE
									PRINTLN("[JJT] Error: entity doesn't exist: ", (DOES_ENTITY_EXIST(playerPedIndex)), 
												", ", (DOES_ENTITY_EXIST( GET_PLAYER_PED( PlayerToUse ) )) )
								ENDIF
							ENDIF
						ELSE
							PRINTLN( "[JJT] participantIndex IS MORE THAN 7: ", iParticipant )
						ENDIF
					ENDFOR
					
					REINIT_NET_TIMER( stFrndVibrationTimer )
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC private_PROCESS_HALLOWEEN_COUNTDOWN_SOUND()

	INT iTeam = GET_LOCAL_PLAYER_TEAM()
	INT iRule = GET_TEAM_CURRENT_RULE(iTeam)
	
	IF NOT IS_RULE_INDEX_VALID(iRule) 
	OR NOT IS_TEAM_INDEX_VALID(iTeam)
		EXIT
	ENDIF
	
	IF IS_LOCAL_PLAYER_OBJECTIVE_TIMER_RUNNING()
	OR ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
	
		INT iTimeRemaining = GET_OBJECTIVE_TIMER_TIME_REMAINING(iTeam, iRule)
		
		IF iTimeRemaining <= 2000
			iTimeRemaining = GET_SUDDEN_DEATH_TIME_REMAINING(iTeam, iRule)
			
			IF IS_BIT_SET( iLocalBoolCheck13, LBOOL13_HWN_SWITCHOVER_COUNTDOWN_PLAYED )
				SET_BIT( iLocalBoolCheck13, LBOOL13_HWN_COUNTDOWN_SWITCHED )
				PRINTLN("private_PROCESS_HALLOWEEN_COUNTDOWN_SOUND - Clearing LBOOL13_HWN_SWITCHOVER_COUNTDOWN_PLAYED so it can be played again")
				CLEAR_BIT(iLocalBoolCheck13, LBOOL13_HWN_SWITCHOVER_COUNTDOWN_PLAYED)
			ENDIF
		ENDIF
		
		STRING sSoundSet = "DLC_HALLOWEEN_FVJ_Sounds"
		
		IF iTimeRemaining < 10001
		AND iTimeRemaining > 2000
			IF NOT IS_BIT_SET( iLocalBoolCheck13, LBOOL13_HWN_SWITCHOVER_COUNTDOWN_PLAYED )
				iHalloweenSoundID = GET_SOUND_ID()
				PLAY_SOUND_FRONTEND(iHalloweenSoundID,"Timer_10s",sSoundSet,FALSE)
				PRINTLN("[JJT] PLAY_SOUND_FRONTEND(Timer_10s, ", sSoundSet, ") Halloween switchover.")
				
				SET_BIT( iLocalBoolCheck13, LBOOL13_HWN_SWITCHOVER_COUNTDOWN_PLAYED )
			ELIF NOT IS_BIT_SET( iLocalBoolCheck13, LBOOL13_HWN_ENDGAME_COUNTDOWN_PLAYED )
			AND IS_BIT_SET( iLocalBoolCheck13, LBOOL13_HWN_COUNTDOWN_SWITCHED )
				iHalloweenSoundID = GET_SOUND_ID()
				PLAY_SOUND_FRONTEND(iHalloweenSoundID,"Timer_10s",sSoundSet,FALSE)
				PRINTLN("[JJT] PLAY_SOUND_FRONTEND(Timer_10s, ", sSoundSet, ") Halloween end.")
				
				SET_BIT( iLocalBoolCheck13, LBOOL13_HWN_ENDGAME_COUNTDOWN_PLAYED )
			ENDIF
		ENDIF	
	ENDIF
ENDPROC

PROC PROCESS_HALLOWEEN_MODE_LOGIC()		//	[ML] Should this be in audio?
	
	// Mission is Over. We Don't want vibrations or whatever playing.
	IF IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
		EXIT
	ENDIF
	
	IF NOT private_DO_RUN_HALLOWEEN_LOGIC()
		EXIT
	ENDIF
	
	private_PROCESS_HALLOWEEN_COUNTDOWN_SOUND()
	
	//Don't run if this player is a scary ped
	IF IS_BIT_SET( iHrtbBitSet, ciHRTB_LOCAL_PED_IS_SCARY_PED )	
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET( iHrtbBitSet, ciHRTB_HAS_HANDLE_TO_SCARY_PED )
	AND NOT IS_BIT_SET( iHrtbBitSet, ciHRTB_LOCAL_PED_IS_SCARY_PED )	
		private_HALLOWEEN_INIT_FIND_SCARY_PED()
	ELSE
		private_HALLOWEEN_PROCESS_SCARED_PED()
	ENDIF
ENDPROC

PROC PROCESS_SILO_INTERIOR_ULP_AMBIENT_AUDIO(INTERIOR_INSTANCE_INDEX iiInterior, INT iInteriorGroupId)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirty, ciOptionsBS30_EnableULPSiloAmbientAudio)
		EXIT
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SILO_01)
	AND NOT IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SILO_02)
	AND NOT IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SILO_03)
	AND NOT IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SERVER_FARM)
		EXIT
	ENDIF
	
	//If we're not in an interior or we're in the wrong interior
	IF iiInterior = NULL
	OR iInteriorGroupId != 47
		CLEANUP_SILO_INTERIOR_ULP_AMBIENT_AUDIO()
		EXIT
	ENDIF
	
	IF NOT ARE_LIGHTS_TURNED_ON()
		CLEANUP_SILO_INTERIOR_ULP_AMBIENT_AUDIO()
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(iLocalBoolCheck35, LBOOL35_STARTED_ULP_BUNKER_AMBIENT_AUDIO)
		PRINTLN("PROCESS_SILO_INTERIOR_ULP_AMBIENT_AUDIO - Enabling AZL_xm_x17dlc_int_lab_Extra_Zones as lights are switched on")
		SET_AMBIENT_ZONE_LIST_STATE("AZL_xm_x17dlc_int_lab_Extra_Zones", TRUE, TRUE)
		SET_BIT(iLocalBoolCheck35, LBOOL35_STARTED_ULP_BUNKER_AMBIENT_AUDIO)
	ENDIF
	
ENDPROC

PROC PROCESS_EVERY_FRAME_INTERIOR_AUDIO(INTERIOR_INSTANCE_INDEX interior, INT interiorNameHash, INT interiorGroupId)

	// MOUNTAIN BASE
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SILO_ENTRANCE)
	OR IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SILO_TUNNEL)
	OR IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SILO_BASE)
	OR IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SILO_LOOP)
	OR IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SILO_01)
	OR IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SILO_02)
	OR IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SILO_03)
		
		IF NOT IS_BIT_SET(iLocalBoolCheck26, LBOOL26_INTERIOR_AUDIO_MOUNTAINBASE)

			IF interior != NULL
			AND interiorGroupId = 47
				PRINTLN("[LM][RCC MISSION] - [Audio] - [MountainBase] - Player is inside the Mountain Base Interior. Creating Ambient Audio Scene.")	
				SET_BIT(iLocalBoolCheck26, LBOOL26_INTERIOR_AUDIO_MOUNTAINBASE)
				SET_AMBIENT_ZONE_LIST_STATE("dlc_xm_int_base_zones", true, false)
				START_AUDIO_SCENE("dlc_xm_mountain_base_scene")
			ENDIF
			
		ELSE
			
			IF interior = NULL
			OR interiorGroupId != 47
				PRINTLN("[LM][RCC MISSION] - [Audio] - [MountainBase] - Player is NOT inside the Mountain Base Interior. Clearing Ambient Audio Scene.")
				CLEAR_BIT(iLocalBoolCheck26, LBOOL26_INTERIOR_AUDIO_MOUNTAINBASE)
				SET_AMBIENT_ZONE_LIST_STATE("dlc_xm_int_base_zones", false, false)
				STOP_AUDIO_SCENE("dlc_xm_mountain_base_scene")
			ENDIF
			
		ENDIF
	ENDIF
	
	PROCESS_SILO_INTERIOR_ULP_AMBIENT_AUDIO(interior, interiorGroupId)
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_IAA)
		MAINTAIN_DEFUNCT_BASE_BODY_SCANNER()
	ENDIF
	
	// FACILITY PROPERTY
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciENABLE_INTERIOR_PLAYER_FACILITY_AUDIO_MIXER)
		IF NOT IS_BIT_SET(iLocalBoolCheck26, LBOOL26_INTERIOR_AUDIO_FACILITY_PROPERTY)
		
			IF interior != NULL				
			AND interiorNameHash = HASH("xm_x17dlc_int_02")
				PRINTLN("[LM][RCC MISSION] - [Audio] - [Facility Property] - Player is inside the Facility Proper. Creating Audio Mixer.")	
				SET_BIT(iLocalBoolCheck26, LBOOL26_INTERIOR_AUDIO_FACILITY_PROPERTY)
				START_AUDIO_SCENE("dlc_xm_facility_property_scene")
			ENDIF
			
		ELSE
		
			IF interior = NULL
			OR interiorNameHash != HASH("xm_x17dlc_int_02")
				PRINTLN("[LM][RCC MISSION] - [Audio] - [Facility Property] - Player is NOT inside the Facility Proper. Clearing Audio Mixer.")
				CLEAR_BIT(iLocalBoolCheck26, LBOOL26_INTERIOR_AUDIO_FACILITY_PROPERTY)
				STOP_AUDIO_SCENE("dlc_xm_facility_property_scene")
			ENDIF
			
		ENDIF
	ENDIF
	
	// SUBMARINE
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SUB)
		
		IF NOT IS_BIT_SET(iLocalBoolCheck26, LBOOL26_INTERIOR_AUDIO_SUBMARINE)
		
			IF interior != NULL
			AND interiorNameHash = HASH("xm_x17dlc_int_sub")
				PRINTLN("[RCC MISSION] - [Audio] - [Submarine] - Player is in sub; ENABLE sub related audio.")
				SET_AMBIENT_ZONE_LIST_STATE("azl_xm_int_sub_zones", TRUE, FALSE)
				SET_STATIC_EMITTER_ENABLED("se_xm_x17dlc_int_sub_stream", TRUE)
				START_AUDIO_SCENE("dlc_xm_submarine_finale_sub_interior_scene")
				SET_BIT(iLocalBoolCheck26, LBOOL26_INTERIOR_AUDIO_SUBMARINE)
			ENDIF
		
		ELSE
		
			IF interior = NULL
			OR interiorNameHash != HASH("xm_x17dlc_int_sub")
				PRINTLN("[RCC MISSION] - [Audio] - [Submarine] - Player is NOT in the sub; DISABLE sub related audio.")
				SET_AMBIENT_ZONE_LIST_STATE("azl_xm_int_sub_zones", FALSE, FALSE)
				SET_STATIC_EMITTER_ENABLED("se_xm_x17dlc_int_sub_stream", FALSE)
				STOP_AUDIO_SCENE("dlc_xm_submarine_finale_sub_interior_scene")
				CLEAR_BIT(iLocalBoolCheck26, LBOOL26_INTERIOR_AUDIO_SUBMARINE)
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_MUSIC_LOCKER)
	AND IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_MUSIC_LOCKER_MUSIC)
	AND NOT IS_BIT_SET(iLocalBoolCheck29, LBOOL29_MUSIC_LOCKER_MUSIC_STARTED)
		IF GET_INTERIOR_FROM_ENTITY(PlayerPedToUse) = iInteriorIndexMusicLocker						
			SET_STATIC_EMITTER_ENABLED("SE_h4_dlc_int_02_h4_Bogs", TRUE)
			SET_STATIC_EMITTER_ENABLED("SE_h4_dlc_int_02_h4_Entrance_Doorway", TRUE)
			SET_STATIC_EMITTER_ENABLED("SE_h4_dlc_int_02_h4_lobby", TRUE)
			SET_STATIC_EMITTER_ENABLED("SE_h4_dlc_int_02_h4_main_bar", TRUE)
			SET_STATIC_EMITTER_ENABLED("SE_h4_dlc_int_02_h4_main_front_01", TRUE)
			SET_STATIC_EMITTER_ENABLED("SE_h4_dlc_int_02_h4_main_front_02", TRUE)
			
			STRING sRadio = "HIDDEN_RADIO_ML_PROMOTER"
			SET_EMITTER_RADIO_STATION("SE_h4_dlc_int_02_h4_Bogs", sRadio)
			SET_EMITTER_RADIO_STATION("SE_h4_dlc_int_02_h4_Entrance_Doorway", sRadio)
			SET_EMITTER_RADIO_STATION("SE_h4_dlc_int_02_h4_lobby", sRadio)
			SET_EMITTER_RADIO_STATION("SE_h4_dlc_int_02_h4_main_bar", sRadio)
			SET_EMITTER_RADIO_STATION("SE_h4_dlc_int_02_h4_main_front_01", sRadio)
			SET_EMITTER_RADIO_STATION("SE_h4_dlc_int_02_h4_main_front_02", sRadio)
			PRINTLN("[AUDIO] PROCESS_EVERY_FRAME_INTERIOR_AUDIO - Starting Music Locker Music")
			IF IS_AUDIO_SCENE_ACTIVE("MP_POSITIONED_RADIO_MUTE_SCENE")
				SET_AUDIO_SCENE_VARIABLE("MP_POSITIONED_RADIO_MUTE_SCENE", "apply", 0)
			ELSE
				START_AUDIO_SCENE("MP_POSITIONED_RADIO_MUTE_SCENE")
				SET_AUDIO_SCENE_VARIABLE("MP_POSITIONED_RADIO_MUTE_SCENE", "apply", 0)
			ENDIF
			SET_BIT(iLocalBoolCheck29, LBOOL29_MUSIC_LOCKER_MUSIC_STARTED)
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_DISABLED_RADIO()
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTen, ciDISABLE_RADIO)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_NEXT_RADIO)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PREV_RADIO)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_NEXT_RADIO_TRACK)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PREV_RADIO_TRACK)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_RADIO_WHEEL)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_RADIO_WHEEL_LR)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_RADIO_WHEEL_UD)
		
		IF GET_PLAYER_RADIO_STATION_INDEX() != 255
			SET_RADIO_TO_STATION_INDEX(255)
			IF IS_PED_IN_ANY_VEHICLE(localPlayerPed)
			AND NETWORK_HAS_CONTROL_OF_ENTITY(GET_VEHICLE_PED_IS_IN(localPlayerPed))
				PRINTLN("[RCC MISSION] Disabling Radio")
				SET_VEHICLE_RADIO_ENABLED(GET_VEHICLE_PED_IS_IN(localPlayerPed), FALSE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_RADIO_EMITTER_OBJECT(OBJECT_INDEX oiEmitter)
	
	IF NOT DOES_ENTITY_EXIST(oiEmitter)
		PRINTLN("[Emitters][Emitter ", iStaggeredRadioEmitter,"] PROCESS_RADIO_EMITTER_OBJECT - Object doesn't exist")
		EXIT
	ENDIF

	FLOAT fDistGameCamToEmitterMax = 150.0
	FLOAT fDistGameCamToEmitter = GET_DISTANCE_BETWEEN_COORDS(GET_GAMEPLAY_CAM_COORD(), GET_ENTITY_COORDS(oiEmitter))
	
	MODEL_NAMES mnProp = GET_ENTITY_MODEL(oiEmitter)
	TEXT_LABEL_63 sEmitter = GET_EMITTER_FOR_MODEL(mnProp, iStaggeredRadioEmitter)
	
	IF fDistGameCamToEmitter <= fDistGameCamToEmitterMax
		
		IF NOT IS_BIT_SET(iRadioEmitterBS, iStaggeredRadioEmitter)
			
			SET_STATIC_EMITTER_ENABLED(sEmitter, TRUE)
			
			LINK_STATIC_EMITTER_TO_ENTITY(sEmitter, oiEmitter)
			
			SET_EMITTER_RADIO_STATION(sEmitter, GET_RADIO_STATION_NAME_FOR_EMITTER(MC_serverBD_3.sRadioEmitters[iStaggeredRadioEmitter].iRadioStation))
						
			PRINTLN("[Emitters][Emitter ", iStaggeredRadioEmitter,"] PROCESS_SPEAKER_PROP_AUDIO - Emitter ",sEmitter," set up on radio " ,GET_RADIO_STATION_NAME_FOR_EMITTER(MC_serverBD_3.sRadioEmitters[iStaggeredRadioEmitter].iRadioStation))
			
			SET_BIT(iRadioEmitterBS, iStaggeredRadioEmitter)
			
			IF IS_AUDIO_SCENE_ACTIVE("MP_POSITIONED_RADIO_MUTE_SCENE")
				SET_AUDIO_SCENE_VARIABLE("MP_POSITIONED_RADIO_MUTE_SCENE", "apply", 0)
				PRINTLN("[Emitters][Emitter ", iStaggeredRadioEmitter,"] PROCESS_SPEAKER_PROP_AUDIO - Setting MP_POSITIONED_RADIO_MUTE_SCENE to 0")
			ENDIF
			
			ENABLE_AMBIENT_ZONE_FOR_EMITTER_FROM_MODEL(mnProp)
			
		ENDIF
		
	ELSE
		
		SET_STATIC_EMITTER_ENABLED(sEmitter, FALSE)
		CLEAR_BIT(iRadioEmitterBS, iStaggeredRadioEmitter)
		
	ENDIF
	
ENDPROC

FUNC OBJECT_INDEX GET_EMITTER_OBJECT_TO_PROCESS()

	IF MC_serverBD_3.sRadioEmitters[iStaggeredRadioEmitter].iRadioEmitterObjectIndex = -1
		RETURN NULL
	ENDIF
	
	SWITCH MC_serverBD_3.sRadioEmitters[iStaggeredRadioEmitter].iRadioEmitterObjectType
		CASE RADIO_EMITTER_TYPE_OBJECT
			NETWORK_INDEX niObj 
			niObj = MC_serverBD_1.sFMMC_SBD.niNetworkedObject[GET_OBJECT_NET_ID_INDEX(MC_serverBD_3.sRadioEmitters[iStaggeredRadioEmitter].iRadioEmitterObjectIndex)]
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(niObj)
				RETURN NET_TO_OBJ(niObj)
			ENDIF
		BREAK
		
		CASE RADIO_EMITTER_TYPE_PROP
			RETURN oiProps[MC_serverBD_3.sRadioEmitters[iStaggeredRadioEmitter].iRadioEmitterObjectIndex]
		BREAK
	ENDSWITCH
	
	RETURN NULL
ENDFUNC

PROC PROCESS_STAGGERED_RADIO_EMITTERS()
	
	OBJECT_INDEX oiRadioObject = GET_EMITTER_OBJECT_TO_PROCESS()
	
	IF oiRadioObject != NULL
		PROCESS_RADIO_EMITTER_OBJECT(oiRadioObject)
	ENDIF
	
	iStaggeredRadioEmitter++
	IF iStaggeredRadioEmitter >= FMMC_MAX_NUM_RADIO_EMITTERS
		iStaggeredRadioEmitter = 0
	ENDIF
	
ENDPROC

PROC CLEANUP_LOCAL_HALLOWEEN_ADVERSARY_2022_SOUNDS()
	INT i
	FOR i = 0 TO ciHALLOWEEN_ADVERSARY_MAX_PLAYERS_PER_TEAM - 1
		IF iEnemyCloseSoundLoop[i] > 0
			STOP_SOUND(iEnemyCloseSoundLoop[i])
			RELEASE_SOUND_ID(iEnemyCloseSoundLoop[i])
			iEnemyCloseSoundLoop[i] = 0
		ENDIF
	ENDFOR
	
	IF MC_PlayerBD[iLocalPart].iTeam = ciHALLOWEEN_ADVERSARY_HUNTER_TEAM 
	AND IS_AUDIO_SCENE_ACTIVE("Halloween_Adversary_Hunted_Heart_MixScene")
		IF eiCurrentClosestHuntedPlayer != NULL
			REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(eiCurrentClosestHuntedPlayer)
		ENDIF
		STOP_AUDIO_SCENE("Halloween_Adversary_Hunted_Heart_MixScene")
	ENDIF
	
	CLEAR_BIT(iLocalBoolCheck29, LBOOL29_HalloweenAdversary2022ProximitySoundsBroadcasted)
	
	IF iHunterThermalVisionSoundLoop > -1
		STOP_SOUND(iHunterThermalVisionSoundLoop)
		RELEASE_SOUND_ID(iHunterThermalVisionSoundLoop)
		iHunterThermalVisionSoundLoop = 0
	ENDIF
	
	PRINTLN("[PROCESS_HALLOWEEN_ADVERSARY_2022_SOUNDS] - CLEANUP_LOCAL_HALLOWEEN_ADVERSARY_2022_SOUNDS - Clear local proximity sounds")
ENDPROC

PROC REQUEST_REBROADCAST_OF_HALLOWEEN_ADVERSARY_2022_PROXIMITY_SOUNDS_IF_NEEDED()
	IF IS_BIT_SET(iLocalBoolCheck29, LBOOL29_HalloweenAdversary2022ResetProximitySounds)
	AND IS_SCREEN_FADED_IN()
	AND NOT IS_PART_WAITING_IN_SPECTATE(iLocalPart)
		BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_ResetHalloweenAdversary2022ProximitySounds)
		CLEAR_BIT(iLocalBoolCheck29, LBOOL29_HalloweenAdversary2022ResetProximitySounds)
		PRINTLN("[PROCESS_HALLOWEEN_ADVERSARY_2022_SOUNDS] - REQUEST_HALLOWEEN_ADVERSARY_2022_SOUNDS_PROXIMITY_SOUNDS_REBROADCAST_IF_NEEDED - Request proximity sounds re-broadcast from the enemy team")
	ENDIF
ENDPROC

PROC PROCESS_HALLOWEEN_ADVERSARY_2022_CONTROLLER_SHAKE(FLOAT &fCurrentDist, FLOAT fMinDist, INT iMinPauseInMS)
	IF fCurrentDist < fMinDist
		IF NOT HAS_NET_TIMER_STARTED(stHrtbVibrationTimer)
			CLEAR_CONTROL_SHAKE_SUPPRESSED_ID(PLAYER_CONTROL)
			SET_CONTROL_SHAKE(PLAYER_CONTROL, 100 - CEIL(fCurrentDist * 2.0), 200)
			START_NET_TIMER(stHrtbVibrationTimer)
			SET_CONTROL_SHAKE_SUPPRESSED_ID(PLAYER_CONTROL, 0)
		ELIF HAS_NET_TIMER_EXPIRED(stHrtbVibrationTimer, iMinPauseInMS - CEIL(fCurrentDist * 2.0))		
			RESET_NET_TIMER(stHrtbVibrationTimer)
		ENDIF
	ELIF HAS_NET_TIMER_STARTED(stHrtbVibrationTimer)
		STOP_CONTROL_SHAKE(PLAYER_CONTROL)
		SET_CONTROL_SHAKE_SUPPRESSED_ID(PLAYER_CONTROL, 0)
		RESET_NET_TIMER(stHrtbVibrationTimer)
	ENDIF
ENDPROC

PROC PROCESS_HALLOWEEN_ADVERSARY_2022_SOUNDS(INT iRule, INT iTeam, INT iTeamPart)	
	//Round 0 - Start
	IF iRule = 0 AND DOES_LOCAL_PLAYER_HAVE_NEW_PRIORITY_THIS_FRAME()
		IF NOT IS_AUDIO_SCENE_ACTIVE("Halloween_Adversary_Main_Mixscene")
			START_AUDIO_SCENE("Halloween_Adversary_Main_Mixscene")
		ENDIF
		
		IF iTeam = ciHALLOWEEN_ADVERSARY_HUNTER_TEAM
			PLAY_SOUND_FRONTEND(-1, "Riders_Start", "Halloween_Adversary_Sounds")
		ELSE
			PLAY_SOUND_FRONTEND(-1, "Hunted_Start", "Halloween_Adversary_Sounds")
		ENDIF
		
		STOP_CONTROL_SHAKE(PLAYER_CONTROL)
		SET_CONTROL_SHAKE_SUPPRESSED_ID(PLAYER_CONTROL, 0)
	ENDIF
	
	//Proximity Sounds
	IF iRule = 0
		IF bLocalPlayerOK
			IF NOT IS_BIT_SET(iLocalBoolCheck29, LBOOL29_HalloweenAdversary2022ProximitySoundsBroadcasted)
			AND IS_SCREEN_FADED_IN()
				BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_SetHalloweenAdversary2022ProximitySounds, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE, iTeamPart)
				SET_BIT(iLocalBoolCheck29, LBOOL29_HalloweenAdversary2022ProximitySoundsBroadcasted)
				PRINTLN("[PROCESS_HALLOWEEN_ADVERSARY_2022_SOUNDS] - Broadcast proximity sound to the enemy team")
			ENDIF
			
			IF iTeam = ciHALLOWEEN_ADVERSARY_HUNTER_TEAM
			AND NOT IS_AUDIO_SCENE_ACTIVE("Halloween_Adversary_Hunted_Heart_MixScene")
				START_AUDIO_SCENE("Halloween_Adversary_Hunted_Heart_MixScene")
			ENDIF 
			
			REQUEST_REBROADCAST_OF_HALLOWEEN_ADVERSARY_2022_PROXIMITY_SOUNDS_IF_NEEDED()
		ELIF !bLocalPlayerOK 
			IF IS_BIT_SET(iLocalBoolCheck29, LBOOL29_HalloweenAdversary2022ProximitySoundsBroadcasted)
				BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_SetHalloweenAdversary2022ProximitySounds, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, FALSE, iTeamPart)
				CLEANUP_LOCAL_HALLOWEEN_ADVERSARY_2022_SOUNDS()
			ENDIF
		ENDIF
	//Cleanup on next rule
	ELSE
		CLEANUP_LOCAL_HALLOWEEN_ADVERSARY_2022_SOUNDS()
	ENDIF
	
	//Audio scene
	IF iRule = 0
		INT iEnemyTeam = ciHALLOWEEN_ADVERSARY_HUNTED_TEAM
		
		IF iTeam = ciHALLOWEEN_ADVERSARY_HUNTED_TEAM
			iEnemyTeam = ciHALLOWEEN_ADVERSARY_HUNTER_TEAM
		ENDIF
		
		ENTITY_INDEX eiClosestHuntedPlayer = GET_CLOSEST_PLAYER_IN_TEAM(iEnemyTeam)
		IF NOT IS_ENTITY_ALIVE(eiClosestHuntedPlayer)
			EXIT
		ENDIF
		
		FLOAT iDistBetweenEnts = GET_DISTANCE_BETWEEN_ENTITIES(LocalPlayerPed, eiClosestHuntedPlayer)
		
		PROCESS_HALLOWEEN_ADVERSARY_2022_CONTROLLER_SHAKE(iDistBetweenEnts, 20.0, 1000)
	
		IF iTeam = ciHALLOWEEN_ADVERSARY_HUNTER_TEAM	
		AND IS_AUDIO_SCENE_ACTIVE("Halloween_Adversary_Hunted_Heart_MixScene")			
			IF iDistBetweenEnts < 20.0		
				IF eiCurrentClosestHuntedPlayer != eiClosestHuntedPlayer			
					//Remove the previous closest enemy player from the audio mix group
					IF eiCurrentClosestHuntedPlayer != NULL
						REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(eiCurrentClosestHuntedPlayer)
					ENDIF
					
					ADD_ENTITY_TO_AUDIO_MIX_GROUP(eiClosestHuntedPlayer, "Halloween_Adversary_Hunted_Heart_MixGroup")
				ENDIF				
			//There is no enemy player within the set range, therfore remove the current closest enemy player from the audio mix group
			ELIF eiCurrentClosestHuntedPlayer != NULL
				REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(eiCurrentClosestHuntedPlayer)	
			ENDIF			
		ENDIF
	ELSE
		IF IS_AUDIO_SCENE_ACTIVE("Halloween_Adversary_Hunted_Heart_MixScene")
			STOP_AUDIO_SCENE("Halloween_Adversary_Hunted_Heart_MixScene")
		ENDIF
		IF IS_AUDIO_SCENE_ACTIVE("Halloween_Adversary_Thermal_Vison_Mixscene")
			STOP_AUDIO_SCENE("Halloween_Adversary_Thermal_Vison_Mixscene")
		ENDIF
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Mission Specific Scripted Audio
// ##### Description: Functions for special case audio in particular missions. For tighter one off audio processing.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC SET_CASINO_PENTHOUSE_MUSIC_STATE(CASINO_PENTHOUSE_PARTY_MUSIC eNewState)
	PRINTLN("SET_CASINO_PENTHOUSE_MUSIC_STATE - Changing state from ", ePenthousePartyMusic, " to ", eNewState)
	ePenthousePartyMusic = eNewState
ENDPROC

PROC PROCESS_CASINO_PENTHOUSE_PARTY_MUSIC()
	
	IF NOT IS_BIT_SET(iMissionSpecificScriptedAudio, ciMissionSpecificScriptedAudio_Party_Promoter)
		EXIT
	ENDIF
	
	IF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(1)
	OR FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(2)
		EXIT
	ENDIF
	
	IF IS_THIS_A_QUICK_RESTART_JOB()
	AND ePenthousePartyMusic < CPPM_TRANSITION_PARTY
	AND NATIVE_TO_INT(LocalPlayerCurrentInterior) = ciPenthouseInterior
		SET_CASINO_PENTHOUSE_MUSIC_STATE(CPPM_TRANSITION_PARTY)
	ENDIF
	
	SWITCH ePenthousePartyMusic
		CASE CPPM_LOAD
			IF LOAD_STREAM("intro_stream", "dlc_sec_promoter_music_moment")
				SET_CASINO_PENTHOUSE_MUSIC_STATE(CPPM_START_HALLWAY)
			ENDIF
		BREAK
		CASE CPPM_START_HALLWAY
			
			IF NATIVE_TO_INT(LocalPlayerCurrentInterior) != ciCasinoHotelFloor
				EXIT
			ENDIF
			
			IF IS_SCREEN_FADED_IN()
			OR IS_SCREEN_FADING_IN()
				IF NOT IS_STREAM_PLAYING()
					IF LOAD_STREAM("intro_stream", "dlc_sec_promoter_music_moment")
						PRINTLN("PROCESS_CASINO_PENTHOUSE_PARTY_MUSIC - Starting Stream")
						PLAY_STREAM_FROM_POSITION(vPenthouseDoorCoord)
					ENDIF
				ELSE
					SET_CASINO_PENTHOUSE_MUSIC_STATE(CPPM_PLAYING_HALLWAY)
				ENDIF
			ENDIF
		BREAK
		CASE CPPM_PLAYING_HALLWAY
			IF IS_SCREEN_FADED_OUT()
				PRINTLN("PROCESS_CASINO_PENTHOUSE_PARTY_MUSIC - Faded out in hallway")
				SET_CASINO_PENTHOUSE_MUSIC_STATE(CPPM_TRANSITION_PARTY)
			ENDIF
		BREAK
		CASE CPPM_TRANSITION_PARTY
			IF NOT IS_SCREEN_FADING_IN()
			AND NOT IS_SCREEN_FADED_IN()
				EXIT
			ENDIF
			FLOAT playtimeInMilliseconds
			playtimeInMilliseconds = TO_FLOAT(GET_STREAM_PLAY_TIME())
			PRINTLN("PROCESS_CASINO_PENTHOUSE_PARTY_MUSIC - Stream Play Time playtimeInMilliseconds: ", playtimeInMilliseconds)
			playtimeInMilliseconds = playTimeInMilliseconds % 4102.0
			PRINTLN("PROCESS_CASINO_PENTHOUSE_PARTY_MUSIC - Wrapped playtimeInMilliseconds: ", playtimeInMilliseconds)
			iPartySoundID = GET_SOUND_ID()
			PLAY_SOUND_FROM_COORD(iPartySoundID, "song", vPenthouseMusicCoord, "dlc_sec_promoter_music_moment")
			SET_VARIABLE_ON_SOUND(iPartySoundID, "startOffset", playtimeInMilliseconds/1000)
			STOP_STREAM()
			
			PRINTLN("PROCESS_CASINO_PENTHOUSE_PARTY_MUSIC - Starting sound from playtimeInMilliseconds: ", playtimeInMilliseconds, " iPartySoundID ", iPartySoundID)
			SET_CASINO_PENTHOUSE_MUSIC_STATE(CPPM_PLAYING_PARTY)
		BREAK
		CASE CPPM_PLAYING_PARTY
			
			IF NATIVE_TO_INT(LocalPlayerCurrentInterior) != ciPenthouseInterior
				EXIT
			ENDIF
			
			IF IS_CUTSCENE_PLAYING()
				PRINTLN("PROCESS_CASINO_PENTHOUSE_PARTY_MUSIC - Cutscene playing, cut off music")
				STOP_SOUND(iPartySoundID)
				RELEASE_SOUND_ID(iPartySoundID)
				SET_CASINO_PENTHOUSE_MUSIC_STATE(CPPM_MUSIC_ENDED)
			ENDIF
		BREAK
		CASE CPPM_MUSIC_ENDED
			
		BREAK
	ENDSWITCH
	
ENDPROC

PROC SET_BILLIONAIRE_PARTY_MUSIC_STATE(BILLIONAIRE_PARTY_MUSIC eNewState)
	IF eNewState <= eBillionairePartyMusic
		EXIT
	ENDIF
	PRINTLN("SET_BILLIONAIRE_PARTY_MUSIC_STATE - Changing state from ", eBillionairePartyMusic, " to ", eNewState)
	eBillionairePartyMusic = eNewState
ENDPROC

PROC PROCESS_BILLIONAIRE_PARTY_MUSIC()
	
	IF NOT IS_BIT_SET(iMissionSpecificScriptedAudio, ciMissionSpecificScriptedAudio_Billionaire)
		EXIT
	ENDIF
	
	IF eBillionairePartyMusic <= BPM_PLAY
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			VEHICLE_INDEX viVeh
			viVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
			IF NETWORK_HAS_CONTROL_OF_ENTITY(viVeh)
				SET_VEHICLE_RADIO_ENABLED(viVeh, FALSE)
			ENDIF
		ENDIF
	ENDIF
	
	SWITCH eBillionairePartyMusic
		CASE BPM_FREEZE
			FREEZE_RADIO_STATION("HIDDEN_RADIO_SEC_BILLIONAIRE")
			SET_BILLIONAIRE_PARTY_MUSIC_STATE(BPM_TRIGGER)
		BREAK
		CASE BPM_TRIGGER
			IF GET_LOCAL_PLAYER_CURRENT_RULE(TRUE) >= 2
				VECTOR vPlayerPos
				vPlayerPos = GET_ENTITY_COORDS(PlayerPedToUse)
				IF VDIST2(vPlayerPos, vBillionaireMansionCoord) < POW(fBillionaireUnpauseRange, 2)
					BROADCAST_FMMC_SET_BILLIONAIRE_PARTY_STATE(BPM_PLAY)
				ENDIF
			ENDIF
		BREAK
		CASE BPM_PLAY
			TRIGGER_MUSIC_EVENT("DATA_LEAK_BILLIONAIRE_FORCE_RADIO")
			SET_BILLIONAIRE_PARTY_MUSIC_STATE(BPM_PLAYING)
		BREAK
		CASE BPM_PLAYING
			IF GET_LOCAL_PLAYER_CURRENT_RULE(TRUE) >= 7
				SET_BILLIONAIRE_PARTY_MUSIC_STATE(BPM_END_MUSIC)
			ENDIF
		BREAK
		CASE BPM_END_MUSIC
			PLAY_SOUND_FROM_COORD(-1, "Music_Cut", vBillionaireMansionCoord, "DLC_Security_Data_Leak_2_Sounds")
			SET_BILLIONAIRE_PARTY_MUSIC_STATE(BPM_IDLE)
		BREAK
		CASE BPM_IDLE
			
		BREAK
	ENDSWITCH	
	
ENDPROC

PROC PROCESS_DFWD_AUDIO()
	
	IF NOT IS_BIT_SET(iMissionSpecificScriptedAudio, ciMissionSpecificScriptedAudio_DontFuckWithDre)
		EXIT
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck29, LBOOL29_DreScriptedVoiceLinePlayed)
		EXIT
	ENDIF
	
	IF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(3)
		IF NOT IS_BIT_SET(iLocalBoolCheck29, LBOOL29_MissionTrackStartedRestart)
			SET_MUSIC_STATE(MUSIC_SILENT)
			SET_BIT(iLocalBoolCheck, LBOOL_MUSIC_CHANGED) 
			TRIGGER_MUSIC_EVENT("DFWD_CAR_TRACK_START")
			START_AUDIO_SCENE("DLC_Fixer_DL5_Music_Moment_Scene")
			SET_BIT(iLocalBoolCheck25, LBOOL25_MUSIC_STARTED_DRE_TRACK)
			SET_BIT(iLocalBoolCheck29, LBOOL29_MissionTrackStartedRestart)
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(iLocalBoolCheck25, LBOOL25_MUSIC_STARTED_DRE_TRACK)
		EXIT
	ENDIF
	
	IF iDrePedForScriptedDialogue = -1
		INT iPlacedPeds
		FOR iPlacedPeds = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfPeds-1
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niPed[iPlacedPeds])
				IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPlacedPeds].mn = INT_TO_ENUM(MODEL_NAMES, HASH("IG_ARY_02"))
					iDrePedForScriptedDialogue = iPlacedPeds
					PRINTLN("PROCESS_DFWD_AUDIO - iDrePedForScriptedDialogue = ", iDrePedForScriptedDialogue)
					BREAKLOOP
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	
	IF GET_MUSIC_PLAYTIME() >= 192553//192639
	AND iDrePedForScriptedDialogue != -1
		PRINTLN("PROCESS_DFWD_AUDIO - Play Dre line at ", GET_MUSIC_PLAYTIME())
		TEXT_LABEL_23 tl23_Root = "FXDL5AU"
		TEXT_LABEL_23 tl23_Conv = "FXDL5_19_1"
		TEXT_LABEL_23 tl23_Voice = ""
		IF FMMC_CREATE_SINGLE_PED_SCRIPTED_ANIM_CONVERSATION(iDrePedForScriptedDialogue, tl23_Root, tl23_Conv, tl23_Voice)
			SET_BIT(iLocalBoolCheck29, LBOOL29_DreScriptedVoiceLinePlayed)
			PRINTLN("PROCESS_DFWD_AUDIO - Playing Dre's voice line")
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_SEED_CAPITAL_MUSIC()
	
	IF NOT IS_BIT_SET(iMissionSpecificScriptedAudio, ciMissionSpecificScriptedAudio_SeedCapital)
		EXIT
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck29, LBOOL29_MissionTrackStartedRestart)
		EXIT
	ENDIF
	
	IF NOT FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(2)
	AND NOT FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(3)
		EXIT
	ENDIF
	
	TRIGGER_MUSIC_EVENT("MP_SEED_CAPITAL_MULE_SONG_START")
	SET_BIT(iLocalBoolCheck29, LBOOL29_MissionTrackStartedRestart)
	
	PRINTLN("PROCESS_SEED_CAPITAL_MUSIC - Starting music on restart")
	
ENDPROC

PROC PROCESS_ULP_VAN_INTERIOR_AUDIO_SCENE()

	IF NOT IS_BIT_SET(iMissionSpecificScriptedAudio, ciMissionSpecificScriptedAudio_RogueDrones)
		EXIT
	ENDIF
	
	IF NOT IS_ENTITY_ALIVE(LocalPlayerPed)
		EXIT
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("ULP2_Rogue_Drones_Van_Interior_Scene")
				
		IF NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		OR NOT IS_LOCAL_PLAYER_IN_FIRST_PERSON_MODE()
		OR IS_PED_SITTING_IN_FRONT_OF_VEHICLE(LocalPlayerPed)
			PRINTLN("[AUDIO_SCENE] PROCESS_STOP_VAN_INTERIOR_AUDIO_SCENE_ON_VEHICLE_EXIT - Stopping van interior audio scene!")
			STOP_AUDIO_SCENE("ULP2_Rogue_Drones_Van_Interior_Scene")
		ENDIF
		
	ENDIF
	
ENDPROC

PROC MC_PLAY_DEATH_SOUND_FOR_PED(INT iPed, PED_INDEX pedVictim)
	IF iPed = -1
		EXIT
	ENDIF
	
	MODEL_NAMES mn = GET_ENTITY_MODEL(pedVictim)
	
	STRING sSoundSet
	STRING sSoundName		
	
	// Can't switch / case with INT_TO_ENUM.
	IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("U_M_Y_Juggernaut_01"))
		sSoundSet = "DLC_MPSUM2_Juggernaut_NPC_Sounds"
		
		IF IS_BIT_SET(sMissionPedsLocalVars[iPed].iPedBS, ci_MissionPedBS_SyncScene_CleanupSyncSceneEarly)
		OR (sMissionPedsLocalVars[iPed].ePedSyncSceneState > PED_SYNC_SCENE_STATE_INIT
		AND sMissionPedsLocalVars[iPed].ePedSyncSceneState < PED_SYNC_SCENE_STATE_CLEANUP)
			sSoundName = "JN_Death_Inactive"			
		ELSE
			IF GET_RANDOM_INT_IN_RANGE(0, 101) < 25
				sSoundName = "JN_Bark"
			ELSE
				sSoundName = "JN_Death"
			ENDIF
		ENDIF
	ELSE
		EXIT
	ENDIF
	
	PLAY_SOUND_FROM_COORD(-1, sSoundName, GET_ENTITY_COORDS(pedVictim, FALSE), sSoundSet)
ENDPROC
	

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Audio Scenes ----------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL IS_FMMC_AUDIO_SCENE_VALID(INT iAudioSceneIndex)
	
	IF g_FMMC_STRUCT.iAudioScene[iAudioSceneIndex] > -1
	AND g_FMMC_STRUCT.iAudioScene[iAudioSceneIndex] < AUDIO_SCENE_MAX
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC PROCESS_AUDIO_SCENE(INT iAudioSceneIndex)

	IF NOT IS_FMMC_AUDIO_SCENE_VALID(iAudioSceneIndex)
		EXIT
	ENDIF
	
	INT iTeam = MC_playerBD[iLocalPart].iTeam
	IF g_FMMC_STRUCT.iAudioSceneTeam[iAudioSceneIndex] != -1
	AND g_FMMC_STRUCT.iAudioSceneTeam[iAudioSceneIndex] != iTeam
		EXIT
	ENDIF
	
	STRING stAudioScene = GET_AUDIO_SCENE_NAME_FROM_SELECTION(g_FMMC_STRUCT.iAudioScene[iAudioSceneIndex])
	
	INT iRule = GET_TEAM_CURRENT_RULE(iTeam)
	IF g_FMMC_STRUCT.iAudioSceneStartRule[iAudioSceneIndex] = iRule
		IF NOT IS_AUDIO_SCENE_ACTIVE(stAudioScene)
			PRINTLN("[AUDIO_SCENE] PROCESS_AUDIO_SCENE - Starting audio scene! Audio Scene: ", stAudioScene, " Team: ", iTeam, " Rule: ", iRule)
			START_AUDIO_SCENE(stAudioScene)
		ENDIF
		
	ELIF g_FMMC_STRUCT.iAudioSceneEndRule[iAudioSceneIndex] = iRule
		IF IS_AUDIO_SCENE_ACTIVE(stAudioScene)
			PRINTLN("[AUDIO_SCENE] PROCESS_AUDIO_SCENE - Stopping audio scene! Audio Scene: ", stAudioScene, " Team: ", iTeam, " Rule: ", iRule)
			STOP_AUDIO_SCENE(stAudioScene)
		ENDIF
		
	ENDIF
	
ENDPROC

PROC PROCESS_AUDIO_SCENES()
	
	INT iAudioScene
	FOR iAudioScene = 0 TO FMMC_MAX_AUDIO_SCENES-1
		PROCESS_AUDIO_SCENE(iAudioScene)
	ENDFOR
	
ENDPROC

PROC PROCESS_MISSION_SPECIFIC_SCRIPTED_AUDIO()
	
	IF g_bFMMCBlockCustomAudioScript
		EXIT
	ENDIF

	PROCESS_CASINO_PENTHOUSE_PARTY_MUSIC()
	
	PROCESS_BILLIONAIRE_PARTY_MUSIC()
	
	PROCESS_DFWD_AUDIO()
	
	PROCESS_SEED_CAPITAL_MUSIC()
	
	PROCESS_ULP_VAN_INTERIOR_AUDIO_SCENE()
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: <NAME>
// ##### Description: <DESCRIPTION>
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_AUDIO()
	
	PROCESS_MUSIC() // Changes the current playing music scene in the score depending on mission objective settings and combat/driving/flying state, as well as end countdowns

	PROCESS_AUDIO_MIX()
	
	PROCESS_AUDIO_SCENES()
	
	PROCESS_STAGGERED_RADIO_EMITTERS()
	
	PROCESS_MISSION_SPECIFIC_SCRIPTED_AUDIO()
	
ENDPROC
