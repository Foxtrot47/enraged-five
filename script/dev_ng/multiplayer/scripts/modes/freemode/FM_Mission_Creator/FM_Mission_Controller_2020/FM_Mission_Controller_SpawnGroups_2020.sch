// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### System: GTA V Online Mission Controller - Spawn Groups  ------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Script Name: FM_Mission_Controller_2020.sc ------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Holds all the functions related to handling Spawn Groups.
// ##### Peds, Vehicles, Objects, Props, and non physical entities like dummy blips etc.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### This script was forked from FM_Mission_Controller.sc at the beginning of 2020		 																				  
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

USING "FM_Mission_Controller_Assets_2020.sch"


// ##### -------------------------------------------------------------------------
// ##### Section Name: Spawn Group Repositioning - 
// ##### Description: Various helper functions & wrappers to do with Spawn Groups repositioning logic.
// ##### -------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------

PROC MC_PROCESS_REPOSITION_PED_BASED_ON_SUB_SPAWN_GROUP_SETTINGS(WARP_LOCATION_SETTINGS &sWarpLocationSettings, INT iSubSpawnGroupIndex, INT iPed, PED_INDEX pedPassed)
	
	VECTOR vPos
	FLOAT fHeading
	PRINTLN("[Reposition][", GET_ENTITY_TYPE_PRINT_TAG(CREATION_TYPE_PEDS), "s][", GET_ENTITY_TYPE_PRINT_TAG(CREATION_TYPE_PEDS), iPed, "] - MC_PROCESS_REPOSITION_PED_BASED_ON_SUB_SPAWN_GROUP_SETTINGS - Using Vector: ", sWarpLocationSettings.vPosition[iSubSpawnGroupIndex])

	vPos = sWarpLocationSettings.vPosition[iSubSpawnGroupIndex]
	fHeading = sWarpLocationSettings.fHeading[iSubSpawnGroupIndex]
	
	VECTOR vMin = <<0.0, 0.0, 0.0>>
	VECTOR vMax = <<0.0, 0.0, 0.0>>
	GET_MODEL_DIMENSIONS(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].mn, vMin, vMax)
	
	PRINTLN("[Reposition][", GET_ENTITY_TYPE_PRINT_TAG(CREATION_TYPE_PEDS), "s][", GET_ENTITY_TYPE_PRINT_TAG(CREATION_TYPE_PEDS), iPed, "] - MC_PROCESS_REPOSITION_PED_BASED_ON_SUB_SPAWN_GROUP_SETTINGS - Model vMin: ", vMin, " vMax: ", vMax)
	
	IF vMin.z <= -0.1
		FLOAT fHeight = ABSF(vMin.z)
		PRINTLN("[Reposition][", GET_ENTITY_TYPE_PRINT_TAG(CREATION_TYPE_PEDS), "s][", GET_ENTITY_TYPE_PRINT_TAG(CREATION_TYPE_PEDS), iPed, "] - MC_PROCESS_REPOSITION_PED_BASED_ON_SUB_SPAWN_GROUP_SETTINGS - Adding on the Z: ", fHeight)
		vPos.z += fHeight
	ENDIF
		
	CLEAR_PED_TASKS_IMMEDIATELY(pedPassed)
	SET_ENTITY_COORDS_NO_OFFSET(pedPassed, vPos, FALSE, FALSE, TRUE)
	SET_ENTITY_HEADING(pedPassed, fHeading)
	FORCE_PED_AI_AND_ANIMATION_UPDATE(pedPassed)
	PRINTLN("[Reposition][", GET_ENTITY_TYPE_PRINT_TAG(CREATION_TYPE_PEDS), "s][", GET_ENTITY_TYPE_PRINT_TAG(CREATION_TYPE_PEDS), iPed, "] - MC_PROCESS_REPOSITION_PED_BASED_ON_SUB_SPAWN_GROUP_SETTINGS - Setting vPos: ", vPos, " Heading: ", fHeading)
	
ENDPROC

PROC MC_PROCESS_REPOSITION_VEH_BASED_ON_SUB_SPAWN_GROUP_SETTINGS(WARP_LOCATION_SETTINGS &sWarpLocationSettings, INT iSubSpawnGroupIndex, INT iVeh, VEHICLE_INDEX vehPassed)

	VECTOR vPos
	FLOAT fHeading
	PRINTLN("[Reposition][", GET_ENTITY_TYPE_PRINT_TAG(CREATION_TYPE_VEHICLES), "s][", GET_ENTITY_TYPE_PRINT_TAG(CREATION_TYPE_VEHICLES), iVeh, "] - MC_PROCESS_REPOSITION_VEH_BASED_ON_SUB_SPAWN_GROUP_SETTINGS - Using Vector: ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sWarpLocationSettings.vPosition[iSubSpawnGroupIndex])
		
	vPos = sWarpLocationSettings.vPosition[iSubSpawnGroupIndex]
	fHeading = sWarpLocationSettings.fHeading[iSubSpawnGroupIndex]
	
	VECTOR vMin = <<0.0, 0.0, 0.0>>
	VECTOR vMax = <<0.0, 0.0, 0.0>>
	GET_MODEL_DIMENSIONS(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn, vMin, vMax)
	
	PRINTLN("[Reposition][", GET_ENTITY_TYPE_PRINT_TAG(CREATION_TYPE_VEHICLES), "s][", GET_ENTITY_TYPE_PRINT_TAG(CREATION_TYPE_VEHICLES), iVeh, "] - MC_PROCESS_REPOSITION_VEH_BASED_ON_SUB_SPAWN_GROUP_SETTINGS - Model vMin: ", vMin, " vMax: ", vMax)
	
	IF vMin.z <= -0.1
		FLOAT fHeight = ABSF(vMin.z)
		PRINTLN("[Reposition][", GET_ENTITY_TYPE_PRINT_TAG(CREATION_TYPE_VEHICLES), "s][", GET_ENTITY_TYPE_PRINT_TAG(CREATION_TYPE_VEHICLES), iVeh, "] - MC_PROCESS_REPOSITION_VEH_BASED_ON_SUB_SPAWN_GROUP_SETTINGS - Adding on the Z: ", fHeight)
		vPos.z += fHeight
	ENDIF
	
	SET_ENTITY_COORDS_NO_OFFSET(vehPassed, vPos, FALSE, FALSE, FALSE)
	SET_ENTITY_HEADING(vehPassed, fHeading)
	SET_VEHICLE_ON_GROUND_PROPERLY(vehPassed)
	PRINTLN("[Reposition][", GET_ENTITY_TYPE_PRINT_TAG(CREATION_TYPE_VEHICLES), "s][", GET_ENTITY_TYPE_PRINT_TAG(CREATION_TYPE_VEHICLES), iVeh, "] - MC_PROCESS_REPOSITION_VEH_BASED_ON_SUB_SPAWN_GROUP_SETTINGS - Setting vPos: ", vPos, " Heading: ", fHeading)
	
ENDPROC

PROC MC_PROCESS_REPOSITION_OBJ_BASED_ON_SUB_SPAWN_GROUP_SETTINGS(WARP_LOCATION_SETTINGS &sWarpLocationSettings, INT iSubSpawnGroupIndex, INT iObj, OBJECT_INDEX oiPassed)

	VECTOR vPos
	FLOAT fHeading

	PRINTLN("[Reposition][", GET_ENTITY_TYPE_PRINT_TAG(CREATION_TYPE_OBJECTS), "s][", GET_ENTITY_TYPE_PRINT_TAG(CREATION_TYPE_OBJECTS), iObj, "] - MC_PROCESS_REPOSITION_OBJ_BASED_ON_SUB_SPAWN_GROUP_SETTINGS - Using Vector: ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].sWarpLocationSettings.vPosition[iSubSpawnGroupIndex])
	vPos = sWarpLocationSettings.vPosition[iSubSpawnGroupIndex]
	fHeading = sWarpLocationSettings.fHeading[iSubSpawnGroupIndex]
	
	VECTOR vMin = <<0.0, 0.0, 0.0>>
	VECTOR vMax = <<0.0, 0.0, 0.0>>
	GET_MODEL_DIMENSIONS(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].mn, vMin, vMax)
	
	PRINTLN("[Reposition][", GET_ENTITY_TYPE_PRINT_TAG(CREATION_TYPE_OBJECTS), "s][", GET_ENTITY_TYPE_PRINT_TAG(CREATION_TYPE_OBJECTS), iObj, "] - MC_PROCESS_REPOSITION_OBJ_BASED_ON_SUB_SPAWN_GROUP_SETTINGS - Model vMin: ", vMin, " vMax: ", vMax)
	
	IF vMin.z <= -0.1
		FLOAT fHeight = ABSF(vMin.z)
		PRINTLN("[Reposition][", GET_ENTITY_TYPE_PRINT_TAG(CREATION_TYPE_OBJECTS), "s][", GET_ENTITY_TYPE_PRINT_TAG(CREATION_TYPE_OBJECTS), iObj, "] - MC_PROCESS_REPOSITION_OBJ_BASED_ON_SUB_SPAWN_GROUP_SETTINGS - Adding on the Z: ", fHeight)
		vPos.z += fHeight
	ENDIF
			
	SET_ENTITY_COORDS_NO_OFFSET(oiPassed, vPos, FALSE, FALSE, FALSE)
	SET_ENTITY_HEADING(oiPassed, fHeading)
	PRINTLN("[Reposition][", GET_ENTITY_TYPE_PRINT_TAG(CREATION_TYPE_OBJECTS), "s][", GET_ENTITY_TYPE_PRINT_TAG(CREATION_TYPE_OBJECTS), iObj, "] - MC_PROCESS_REPOSITION_OBJ_BASED_ON_SUB_SPAWN_GROUP_SETTINGS - Setting vPos: ", vPos, " Heading: ", fHeading)		
	
ENDPROC

PROC MC_PROCESS_REPOSITION_LOC_BASED_ON_SUB_SPAWN_GROUP_SETTINGS(WARP_LOCATION_SETTINGS &sWarpLocationSettings, INT iSubSpawnGroupIndex, INT iLoc)
	VECTOR vPos
	FLOAT fHeading

	PRINTLN("[Reposition][", GET_ENTITY_TYPE_PRINT_TAG(CREATION_TYPE_GOTO_LOC), "s][", GET_ENTITY_TYPE_PRINT_TAG(CREATION_TYPE_GOTO_LOC), iLoc, "] - MC_PROCESS_REPOSITION_LOC_BASED_ON_SUB_SPAWN_GROUP_SETTINGS - Using Vector: ", g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].sWarpLocationSettings.vPosition[iSubSpawnGroupIndex])
	vPos = sWarpLocationSettings.vPosition[iSubSpawnGroupIndex]
	fHeading = sWarpLocationSettings.fHeading[iSubSpawnGroupIndex]

	IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].vloc1)
	AND NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].vloc2)
		MC_PROCESS_OFFSET_FOR_AREA(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].vLoc1, g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].vLoc2, 1, vPos)
	ELSE
		MC_PROCESS_OFFSET_FOR_AREA(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].vLoc[0], g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].vLoc1, 0, vPos)
	ENDIF
	g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].fHeading[0] = fHeading
	
	vGoToLocateVectors[iLoc] = GET_INIT_LOCATION_VECTOR(iLoc)
	GET_INIT_LOCATION_SIZE_AND_HEIGHT(iLoc, fGoToLocateSize[iLoc], fGoToLocateHeight[iLoc])

	PRINTLN("[Reposition][", GET_ENTITY_TYPE_PRINT_TAG(CREATION_TYPE_GOTO_LOC), "s][", GET_ENTITY_TYPE_PRINT_TAG(CREATION_TYPE_GOTO_LOC), iLoc, "] - MC_PROCESS_REPOSITION_LOC_BASED_ON_SUB_SPAWN_GROUP_SETTINGS - Setting vPos: ", vPos, " Heading: ", fHeading)	
ENDPROC

PROC SET_UP_INTERACTABLE_ROTATION(INT iInteractable, OBJECT_INDEX oiInteractable, FLOAT fHeadingToUse)
	IF IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].vInteractable_Rotation)
		SET_ENTITY_HEADING(oiInteractable, fHeadingToUse)
		PRINTLN("[Interactables][Interactable ", iInteractable, "] SET_UP_INTERACTABLE_ROTATION | Setting Interactable's heading to ", g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].fInteractable_Heading)
	ELSE
		IF DOES_INTERACTABLE_HAVE_CUSTOM_PLACEMENT_ROTATION(oiInteractable)
			VECTOR vCustomRotation = g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].vInteractable_Rotation
			vCustomRotation.z = fHeadingToUse
			SET_ENTITY_ROTATION(oiInteractable, vCustomRotation)
			PRINTLN("[Interactables][Interactable ", iInteractable, "] SET_UP_INTERACTABLE_ROTATION | Setting Interactable's rotation to ", vCustomRotation, " due to DOES_INTERACTABLE_HAVE_CUSTOM_PLACEMENT_ROTATION")
		ELSE
			SET_ENTITY_ROTATION(oiInteractable, g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].vInteractable_Rotation)
			PRINTLN("[Interactables][Interactable ", iInteractable, "] SET_UP_INTERACTABLE_ROTATION | Setting Interactable's rotation to ", g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].vInteractable_Rotation)
		ENDIF
	ENDIF
ENDPROC

PROC MC_PROCESS_REPOSITION_INTERACTABLE_BASED_ON_SUB_SPAWN_GROUP_SETTINGS(WARP_LOCATION_SETTINGS &sWarpLocationSettings, INT iSubSpawnGroupIndex, INT iInteractable, OBJECT_INDEX oiPassed)
	VECTOR vPos
	FLOAT fHeading
	
	PRINTLN("[Reposition][", GET_ENTITY_TYPE_PRINT_TAG(CREATION_TYPE_INTERACTABLE), "s][", GET_ENTITY_TYPE_PRINT_TAG(CREATION_TYPE_INTERACTABLE), iInteractable, "] - MC_PROCESS_REPOSITION_INTERACTABLE_BASED_ON_SUB_SPAWN_GROUP_SETTINGS - Using Vector: ", g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].sWarpLocationSettings.vPosition[iSubSpawnGroupIndex])
	vPos = sWarpLocationSettings.vPosition[iSubSpawnGroupIndex]
	fHeading = sWarpLocationSettings.fHeading[iSubSpawnGroupIndex]
	
	VECTOR vMin = <<0.0, 0.0, 0.0>>
	VECTOR vMax = <<0.0, 0.0, 0.0>>
	GET_MODEL_DIMENSIONS(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].mnInteractable_Model, vMin, vMax)
	
	PRINTLN("[Reposition][", GET_ENTITY_TYPE_PRINT_TAG(CREATION_TYPE_INTERACTABLE), "s][", GET_ENTITY_TYPE_PRINT_TAG(CREATION_TYPE_INTERACTABLE), iInteractable, "] - MC_PROCESS_REPOSITION_INTERACTABLE_BASED_ON_SUB_SPAWN_GROUP_SETTINGS - Model vMin: ", vMin, " vMax: ", vMax)
	
	IF vMin.z <= -0.1
		FLOAT fHeight = ABSF(vMin.z)
		PRINTLN("[Reposition][", GET_ENTITY_TYPE_PRINT_TAG(CREATION_TYPE_INTERACTABLE), "s][", GET_ENTITY_TYPE_PRINT_TAG(CREATION_TYPE_INTERACTABLE), iInteractable, "] - MC_PROCESS_REPOSITION_INTERACTABLE_BASED_ON_SUB_SPAWN_GROUP_SETTINGS - Adding on the Z: ", fHeight)
		vPos.z += fHeight
	ENDIF
	
	SET_ENTITY_COORDS_NO_OFFSET(oiPassed, vPos, FALSE, FALSE, FALSE)
	SET_UP_INTERACTABLE_ROTATION(iInteractable, oiPassed, fHeading)
	PRINTLN("[Reposition][", GET_ENTITY_TYPE_PRINT_TAG(CREATION_TYPE_INTERACTABLE), "s][", GET_ENTITY_TYPE_PRINT_TAG(CREATION_TYPE_INTERACTABLE), iInteractable, "] - MC_PROCESS_REPOSITION_INTERACTABLE_BASED_ON_SUB_SPAWN_GROUP_SETTINGS - Setting vPos: ", vPos, " Heading: ", fHeading)	
ENDPROC

PROC MC_PROCESS_REPOSITION_END_CUTSCENE_BASED_ON_SUB_SPAWN_GROUP_SETTINGS(WARP_LOCATION_SETTINGS &sWarpLocationSettings, INT iSubSpawnGroupIndex)
	
	PRINTLN("[EndCutscene] - MC_PROCESS_REPOSITION_END_CUTSCENE_BASED_ON_SUB_SPAWN_GROUP_SETTINGS - Setting vPos: ", sWarpLocationSettings.vPosition[iSubSpawnGroupIndex], " Heading: ", sWarpLocationSettings.fHeading[iSubSpawnGroupIndex])
	
	MC_ServerBD.vEndCutsceneOverridePosition = sWarpLocationSettings.vPosition[iSubSpawnGroupIndex]
	MC_ServerBD.fEndCutsceneOverrideHeading = sWarpLocationSettings.fHeading[iSubSpawnGroupIndex]
	
ENDPROC

FUNC BOOL SHOULD_REPOSITION_ENTITY_BASED_ON_SUB_SPAWN_GROUP_SETTINGS(WARP_LOCATION_SETTINGS &sWarpLocationSettings)
	
	IF NOT IS_BIT_SET(sWarpLocationSettings.iWarpLocationBS, ciWARP_LOCATION_BITSET_RepositionBasedOnSubSpawnGroup)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC	

FUNC BOOL MC_PROCESS_REPOSITION_ENTITY_BASED_ON_SUB_SPAWN_GROUP_SETTINGS(WARP_LOCATION_SETTINGS &sWarpLocationSettings, INT iEntityType, INT iEntityIndex, INT iSpawnGroupBS, INT &iSubSpawnGroupBS[], ENTITY_INDEX eiEntity)
	
	IF NOT SHOULD_REPOSITION_ENTITY_BASED_ON_SUB_SPAWN_GROUP_SETTINGS(sWarpLocationSettings)
		RETURN FALSE
	ENDIF
	
	// not set up correctly.
	IF iSpawnGroupBS = 0
		PRINTLN("[Reposition][", GET_ENTITY_TYPE_PRINT_TAG(iEntityType), "s][", GET_ENTITY_TYPE_PRINT_TAG(iEntityType)," ", iEntityIndex, "] - MC_PROCESS_REPOSITION_ENTITY_BASED_ON_SUB_SPAWN_GROUP_SETTINGS - We do not have a spawn group set up!")
		RETURN FALSE
	ENDIF
	
	INT iSpawnGroupIndex = GET_FIRST_MATCH_SPAWN_GROUP_FROM_SERVER(iSpawnGroupBS)
			
	IF iSpawnGroupIndex = -1
		PRINTLN("[Reposition][", GET_ENTITY_TYPE_PRINT_TAG(iEntityType), "s][", GET_ENTITY_TYPE_PRINT_TAG(iEntityType)," ", iEntityIndex, "] - MC_PROCESS_REPOSITION_ENTITY_BASED_ON_SUB_SPAWN_GROUP_SETTINGS - iSpawnGroup: ", iSpawnGroupBS, " rsgSpawnSeed.iEntitySpawnSeedBS: ",  MC_ServerBD_4.rsgSpawnSeed.iEntitySpawnSeedBS, " iSpawnGroupIndex Selected is -1, invalid. Unable to reposition.")
		RETURN FALSE
	ENDIF
	
	PRINTLN("[Reposition][", GET_ENTITY_TYPE_PRINT_TAG(iEntityType), "s][", GET_ENTITY_TYPE_PRINT_TAG(iEntityType)," ", iEntityIndex, "] - MC_PROCESS_REPOSITION_ENTITY_BASED_ON_SUB_SPAWN_GROUP_SETTINGS - iSpawnGroup: ", iSpawnGroupBS, " rsgSpawnSeed.iEntitySpawnSeedBS: ",  MC_ServerBD_4.rsgSpawnSeed.iEntitySpawnSeedBS, " iSpawnGroupIndex Selected: ", iSpawnGroupIndex)
			
	INT iSubSpawnGroupIndex = GET_FIRST_MATCH_SUB_SPAWN_GROUP_FROM_SERVER(iSpawnGroupIndex, iSubSpawnGroupBS[iSpawnGroupIndex])
	IF iSubSpawnGroupIndex = -1
		PRINTLN("[Reposition][", GET_ENTITY_TYPE_PRINT_TAG(iEntityType), "s][", GET_ENTITY_TYPE_PRINT_TAG(iEntityType)," ", iEntityIndex, "] - MC_PROCESS_REPOSITION_ENTITY_BASED_ON_SUB_SPAWN_GROUP_SETTINGS - iSubSpawnGroup: ", iSubSpawnGroupBS[iSpawnGroupIndex], " rsgSpawnSeed.iEntitySpawnSeed_SubGroupBS: ",  MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeed_SubGroupBS[iSpawnGroupIndex], " iSubSpawnGroupIndex Selected is -1, invalid. Unable to reposition.")
		RETURN FALSE
	ENDIF

	PRINTLN("[Reposition][", GET_ENTITY_TYPE_PRINT_TAG(iEntityType), "s][", GET_ENTITY_TYPE_PRINT_TAG(iEntityType)," ", iEntityIndex, "] - MC_PROCESS_REPOSITION_ENTITY_BASED_ON_SUB_SPAWN_GROUP_SETTINGS - iSubSpawnGroup: ", iSubSpawnGroupBS[iSpawnGroupIndex], " rsgSpawnSeed.iEntitySpawnSeed_SubGroupBS: ",  MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeed_SubGroupBS[iSpawnGroupIndex], " iSubSpawnGroupIndex Selected: ", iSubSpawnGroupIndex)
	
	IF iSubSpawnGroupIndex >= FMMC_MAX_RULE_VECTOR_WARPS
		PRINTLN("[Reposition][", GET_ENTITY_TYPE_PRINT_TAG(iEntityType), "s][", GET_ENTITY_TYPE_PRINT_TAG(iEntityType)," ", iEntityIndex, "] - MC_PROCESS_REPOSITION_ENTITY_BASED_ON_SUB_SPAWN_GROUP_SETTINGS - iSubSpawnGroup: ", iSubSpawnGroupBS[iSpawnGroupIndex], " rsgSpawnSeed.iEntitySpawnSeed_SubGroupBS: ",  MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeed_SubGroupBS[iSpawnGroupIndex], " iSubSpawnGroupIndex Selected: ", iSubSpawnGroupIndex, " OVERRUN PREVENTION")
		RETURN FALSE
	ENDIF
		
	IF IS_VECTOR_ZERO(sWarpLocationSettings.vPosition[iSubSpawnGroupIndex])
		PRINTLN("[Reposition][", GET_ENTITY_TYPE_PRINT_TAG(iEntityType), "s][", GET_ENTITY_TYPE_PRINT_TAG(iEntityType)," ", iEntityIndex, "] - MC_PROCESS_REPOSITION_ENTITY_BASED_ON_SUB_SPAWN_GROUP_SETTINGS - Warp Location: ", iSubSpawnGroupIndex, " is ZERO.")
		RETURN FALSE
	ENDIF
	
	SWITCH iEntityType
		CASE CREATION_TYPE_PEDS
			MC_PROCESS_REPOSITION_PED_BASED_ON_SUB_SPAWN_GROUP_SETTINGS(sWarpLocationSettings, iSubSpawnGroupIndex, iEntityIndex, GET_PED_INDEX_FROM_ENTITY_INDEX(eiEntity))
			RETURN TRUE
		BREAK
		
		CASE CREATION_TYPE_VEHICLES
			MC_PROCESS_REPOSITION_VEH_BASED_ON_SUB_SPAWN_GROUP_SETTINGS(sWarpLocationSettings, iSubSpawnGroupIndex, iEntityIndex, GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(eiEntity))
			RETURN TRUE
		BREAK
		
		CASE CREATION_TYPE_OBJECTS		
			MC_PROCESS_REPOSITION_OBJ_BASED_ON_SUB_SPAWN_GROUP_SETTINGS(sWarpLocationSettings, iSubSpawnGroupIndex, iEntityIndex, GET_OBJECT_INDEX_FROM_ENTITY_INDEX(eiEntity))
			RETURN TRUE
		BREAK
		
		CASE CREATION_TYPE_GOTO_LOC
			MC_PROCESS_REPOSITION_LOC_BASED_ON_SUB_SPAWN_GROUP_SETTINGS(sWarpLocationSettings, iSubSpawnGroupIndex, iEntityIndex)
			RETURN TRUE
		BREAK
		
		CASE CREATION_TYPE_INTERACTABLE	
			MC_PROCESS_REPOSITION_INTERACTABLE_BASED_ON_SUB_SPAWN_GROUP_SETTINGS(sWarpLocationSettings, iSubSpawnGroupIndex, iEntityIndex, GET_OBJECT_INDEX_FROM_ENTITY_INDEX(eiEntity))
			RETURN TRUE
		BREAK
		
		CASE CREATION_TYPE_END_CUTSCENE	
			MC_PROCESS_REPOSITION_END_CUTSCENE_BASED_ON_SUB_SPAWN_GROUP_SETTINGS(sWarpLocationSettings, iSubSpawnGroupIndex)
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

// ##### -------------------------------------------------------------------------
// ##### Section Name: Seeding System - Spawn Groups (sub section start)
// ##### Description: Various helper functions & wrappers to do with Spawn Groups
// ##### -------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------

PROC CACHE_SERVER_SEEDED_SPAWN_GROUPS_AT_START()
	MC_serverBD_4.rsgSpawnSeedCachedMissionStart = MC_serverBD_4.rsgSpawnSeed
	
	PRINTLN("[LM][SpawnGroups] - Calling CACHE_SERVER_SEEDED_SPAWN_GROUPS_AT_START - rsgFMMCSpawnSeedCachedMissionStart.iEntitySpawnSeedBS: ", MC_serverBD_4.rsgSpawnSeedCachedMissionStart.iEntitySpawnSeedBS)	
	INT i = 0
	FOR i = 0 TO ciMAX_NUM_FMMC_ENTITY_SUB_SPAWN_GROUPS-1 
		PRINTLN("[LM][SpawnGroups] - Calling CACHE_SERVER_SEEDED_SPAWN_GROUPS_AT_START - rsgFMMCSpawnSeedCachedMissionStart.iEntitySpawnSeed_SubGroupBS[",i,"] = ", MC_serverBD_4.rsgSpawnSeedCachedMissionStart.iEntitySpawnSeed_SubGroupBS[i])
	ENDFOR
ENDPROC

PROC CLEAR_ENTITY_CACHED_SPAWN_GROUP_RESULT_DATA()

	PRINTLN("[LM][SpawnGroups] - Calling CLEAR_ENTITY_CACHED_SPAWN_GROUP_RESULT_DATA")
	
	SET_BIT(iLocalBoolCheck34, LBOOL34_SPAWN_GROUPS_CHANGED_REVALIDATE_SPAWN_POINTS)
	SET_BIT(iLocalBoolCheck34, LBOOL34_SPAWN_GROUPS_CHANGED_REVALIDATE_ZONES)
	
	INT i, iTeam
	
	FOR i = 0 TO FMMC_MAX_PEDS_BITSET-1
		iSpawnGroupBS_CheckedSpawnPed[i] = 0
		iSpawnGroupBS_CanSpawnPed[i] = 0
		
		iPedRespawnIsBlockedBS[i] = 0
		iPedRespawnIsBlockedThisFrameBS[i] = 0
		
		iPedShouldCleanupThisFrameBS[i] = 0
		iPedShouldNotCleanupThisFrameBS[i] = 0
	ENDFOR
	FOR i = 0 TO FMMC_MAX_PROP_BITSET-1
		iSpawnGroupBS_CheckedSpawnProp[i] = 0
		iSpawnGroupBS_CanSpawnProp[i] = 0
		
		iPropRespawnIsBlockedBS[i] = 0
		iPropRespawnIsBlockedThisFrameBS[i] = 0
		
		iPropShouldCleanupThisFrameBS[i] = 0
		iPropShouldNotCleanupThisFrameBS[i] = 0
	ENDFOR
	FOR i = 0 TO FMMC_MAX_WEAPON_BITSET-1
		iSpawnGroupBS_CheckedSpawnWep[i] = 0
		iSpawnGroupBS_CanSpawnWep[i] = 0
	ENDFOR
	FOR i = 0 TO FMMC_MAX_INTERACTABLE_BITSET-1
		iSpawnGroupBS_CheckedSpawnInteractable[i] = 0
		iSpawnGroupBS_CanSpawnInteractable[i] = 0
	ENDFOR
	FOR i = 0 TO FMMC_ZONES_LONG_BS_LENGTH-1
		iSpawnGroupBS_CheckedSpawnZone[i] = 0
		iSpawnGroupBS_CanSpawnZone[i] = 0
	ENDFOR
	FOR iTeam = 0 TO FMMC_MAX_TEAMS-1
		FOR i = 0 TO FMMC_MAX_DIALOGUE_TRIGGER_BITSET-1
			iSpawnGroupBS_CheckedSpawnDialogueTrigger[iTeam][i] = 0
			iSpawnGroupBS_CanSpawnDialogueTrigger[iTeam][i] = 0
		ENDFOR
	ENDFOR
	FOR iTeam = 0 TO FMMC_MAX_TEAMS-1
		FOR i = 0 TO FMMC_MAX_TEAM_SPAWN_POINT_BITSET-1
			iSpawnGroupBS_CheckedSpawnTeamSpawnPoint[iTeam][i] = 0
			iSpawnGroupBS_CanSpawnTeamSpawnPoint[iTeam][i] = 0
			
			iRespawnPointIsBlockedBS[iTeam][i] = 0
			iRespawnPointIsBlockedThisFrameBS[iTeam][i] = 0
		ENDFOR
	ENDFOR
	
	FOR i = 0 TO g_FMMC_STRUCT.iNumberOfDialogueTriggers-1
		IF IS_BIT_SET(iLocalDialogueBlockedTempBS[i/32], i%32)
			CLEAR_BIT(iLocalDialogueBlockedTempBS[i/32], i%32)
			CLEAR_BIT(MC_playerBD[iLocalPart].iDialoguePlayedBS[i/32], i%32)
			CLEAR_BIT(iLocalDialoguePlayedBS[i/32], i%32)
			FMMC_CLEAR_LONG_BIT(sMissionLocalContinuityVars.iDialogueTrackingBitset, g_FMMC_STRUCT.sDialogueTriggers[i].iContinuityId)
			
			PRINTLN("[DIALOGUE] - CLEAR_ENTITY_CACHED_SPAWN_GROUP_RESULT_DATA - Dialogue: ", i, " clearing blocked flag. Will need checking again.")
		ENDIF
	ENDFOR
	SET_BIT(iLocalBoolCheck29,  LBOOL29_REFRESH_INSTANT_DIALOGUE_LOOP)
					
	iSpawnGroupBS_CheckedSpawnVeh = 0
	iSpawnGroupBS_CanSpawnVeh = 0
	iSpawnGroupBS_CheckedSpawnObj = 0
	iSpawnGroupBS_CanSpawnObj = 0
	iSpawnGroupBS_CheckedSpawnDynoProp = 0
	iSpawnGroupBS_CanSpawnDynoProp = 0
	iSpawnGroupBS_CheckedSpawnLoc = 0
	iSpawnGroupBS_CanSpawnLoc = 0
	iSpawnGroupBS_CheckedSpawnDummyBlip = 0
	iSpawnGroupBS_CanSpawnDummyBlip = 0
	iSpawnGroupBS_CheckedSpawnTrain = 0
	iSpawnGroupBS_CanSpawnTrain = 0
	
	iVehRespawnIsBlockedBS = 0
	iVehRespawnIsBlockedThisFrameBS = 0
	iObjRespawnIsBlockedBS = 0
	iObjRespawnIsBlockedThisFrameBS = 0
	iDynoRespawnIsBlockedBS = 0 
	iDynoRespawnIsBlockedThisFrameBS = 0		
	iZoneRespawnIsBlockedBS = 0
	iZoneRespawnIsBlockedThisFrameBS = 0
	iZoneShouldRespawnNowBS = 0
		
	iVehShouldCleanupThisFrameBS = 0
	iVehShouldNotCleanupThisFrameBS = 0
	iObjShouldCleanupThisFrameBS = 0
	iObjShouldNotCleanupThisFrameBS = 0
	iDynoShouldCleanupThisFrameBS = 0
	iDynoShouldNotCleanupThisFrameBS = 0
	iZoneShouldCleanupThisFrameBS = 0
	iZoneShouldNotCleanupThisFrameBS = 0
	
ENDPROC

PROC PROCESS_SPAWN_GROUP_DATA_RESET_CLIENT()

	IF NOT IS_BIT_SET(MC_serverBD_4.rsgSpawnSeed.iMiscBS, ciRandomSpawnGroupBS_RequiresReset)		
		IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_RESET_SPAWN_GROUP_CACHED_VARS_ON_THIS_REQUEST)	
			PRINTLN("[SpawnGroups] - PROCESS_SPAWN_GROUP_DATA_RESET_CLIENT - Clearing PBBOOL4_RESET_SPAWN_GROUP_CACHED_VARS_ON_THIS_REQUEST")	
			CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_RESET_SPAWN_GROUP_CACHED_VARS_ON_THIS_REQUEST)
		ENDIF
		
		EXIT
	ENDIF
		
	IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_RESET_SPAWN_GROUP_CACHED_VARS_ON_THIS_REQUEST)
		PRINTLN("[SpawnGroups] - PROCESS_SPAWN_GROUP_DATA_RESET_CLIENT - Restting some local spawn group cache data.")
		SET_BIT(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_RESET_SPAWN_GROUP_CACHED_VARS_ON_THIS_REQUEST)
		CLEAR_ENTITY_CACHED_SPAWN_GROUP_RESULT_DATA()
	ENDIF
	
ENDPROC

FUNC INT GET_NUMBER_OF_RANDOM_ENTITY_SPAWN_GROUPS_TO_SPAWN(INT iSubGroupArray = -1)
	
	INT iNum = 0
	INT iBS
	
	IF iSubGroupArray = -1
		iNum = RETURN_INT_CUSTOM_VARIABLE_VALUE(g_FMMC_STRUCT.iRandomEntitySpawn_RandomNumbersToUse)
		iBS = g_FMMC_STRUCT.iRandomEntitySpawn_Bitset
	ELSE
		iNum = RETURN_INT_CUSTOM_VARIABLE_VALUE(g_FMMC_STRUCT.iRandomEntitySpawnSub_RandomNumbersToUse[iSubGroupArray])
		iBS = g_FMMC_STRUCT.iRandomEntitySpawnSub_Bitset[iSubGroupArray]
	ENDIF
	
	PRINTLN("[RCC MISSION] GET_NUMBER_OF_RANDOM_ENTITY_SPAWN_GROUPS_TO_SPAWN - iSubGroupArray ",iSubGroupArray," iNum = ",iNum)
	
	IF iNum = ciNUMBER_OF_RANDOM_SPAWNS_PLAYER_NUM
		PRINTLN("[RCC MISSION] GET_NUMBER_OF_RANDOM_ENTITY_SPAWN_GROUPS_TO_SPAWN - player num! iBS = ",iBS)
		
		IF iBS != 0
			INT iTeam
			iNum = 0
			
			FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
				IF IS_BIT_SET(iBS, ciRandomSpawn_SpawnNumberOfPlayers_T0 + iTeam)
					iNum += MC_serverBD.iNumStartingPlayers[iTeam]
					PRINTLN("[RCC MISSION] GET_NUMBER_OF_RANDOM_ENTITY_SPAWN_GROUPS_TO_SPAWN - BS is set for team ",iTeam,", add on ", MC_serverBD.iNumStartingPlayers[iTeam]," players for a current total of ",iNum)
				ENDIF
			ENDFOR
		ELSE
			iNum = GET_TOTAL_STARTING_PLAYERS()
			PRINTLN("[RCC MISSION] GET_NUMBER_OF_RANDOM_ENTITY_SPAWN_GROUPS_TO_SPAWN - BS is empty, just return total starting players of ", GET_TOTAL_STARTING_PLAYERS())
		ENDIF
	ENDIF
	
	RETURN iNum
	
ENDFUNC

PROC ADD_NEW_SPAWN_GROUP_TO_BUILD_IN_PLAY(INT iSpawnGroupToAdd)
	
	IF iSpawnGroupToAdd <= 0
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_BuildSpawnGroupsDuringPlay)
		EXIT
	ENDIF
	
	IF MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeedBS != 0
		PRINTLN("[RCC MISSION][ran] ADD_NEW_SPAWN_GROUP_TO_BUILD_IN_PLAY - Bail, MC_serverBD_4.rsgSpawnSeed is already set up")
		EXIT
	ENDIF
	
	PRINTLN("[RCC MISSION][ran] ADD_NEW_SPAWN_GROUP_TO_BUILD_IN_PLAY - Called w iSpawnGroupToAdd ",iSpawnGroupToAdd,", callstack:")
	DEBUG_PRINTCALLSTACK()
	
	IF IS_BIT_SET(MC_serverBD_4.iSpawnGroupsBuiltInPlay_TempSpawnSeedBS, iSpawnGroupToAdd)
		PRINTLN("[RCC MISSION][ran] ADD_NEW_SPAWN_GROUP_TO_BUILD_IN_PLAY - iSpawnGroupsBuiltInPlay_TempSpawnSeedBS bit ",iSpawnGroupToAdd," is already set")
		EXIT
	ENDIF
	
	PRINTLN("[RCC MISSION][ran] ADD_NEW_SPAWN_GROUP_TO_BUILD_IN_PLAY - Set iSpawnGroupsBuiltInPlay_TempSpawnSeedBS bit ",(iSpawnGroupToAdd - 1))
	SET_BIT(MC_serverBD_4.iSpawnGroupsBuiltInPlay_TempSpawnSeedBS, iSpawnGroupToAdd - 1)
	
	INT iNumberOfSetBits = COUNT_SET_BITS(MC_serverBD_4.iSpawnGroupsBuiltInPlay_TempSpawnSeedBS)
	
	IF iNumberOfSetBits < GET_NUMBER_OF_RANDOM_ENTITY_SPAWN_GROUPS_TO_SPAWN()
		PRINTLN("[RCC MISSION][ran] ADD_NEW_SPAWN_GROUP_TO_BUILD_IN_PLAY - Not enough bits set yet, iNumberOfSetBits = ",iNumberOfSetBits," < ",GET_NUMBER_OF_RANDOM_ENTITY_SPAWN_GROUPS_TO_SPAWN())
		EXIT
	ENDIF
		
	PRINTLN("[RCC MISSION][ran] ADD_NEW_SPAWN_GROUP_TO_BUILD_IN_PLAY - iNumberOfSetBits = ",iNumberOfSetBits," >= ",GET_NUMBER_OF_RANDOM_ENTITY_SPAWN_GROUPS_TO_SPAWN(),", set the spawn seed!")
	
	MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeedBS = MC_serverBD_4.iSpawnGroupsBuiltInPlay_TempSpawnSeedBS
	PRINTLN("[RCC MISSION][ran] ADD_NEW_SPAWN_GROUP_TO_BUILD_IN_PLAY - MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeedBS = ",MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeedBS,", remove any invalid entities")
	
	INT i, iTeam
	
	FOR i = 0 TO (MC_serverBD.iNumLocCreated - 1)
		IF NOT MC_DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP(i, eSGET_GotoLoc)
			PRINTLN("[RCC MISSION][ran] ADD_NEW_SPAWN_GROUP_TO_BUILD_IN_PLAY - Remove loc ",i," rules & FailBitset")
			FOR iTeam = 0 TO (FMMC_MAX_TEAMS - 1)
				MC_serverBD_4.iGotoLocationDataPriority[i][iTeam] = FMMC_PRIORITY_IGNORE
				MC_serverBD_4.iGotoLocationDataRule[i][iTeam] = FMMC_OBJECTIVE_LOGIC_NONE
				CLEAR_BIT(MC_serverBD.iLocteamFailBitset[i], iteam)
			ENDFOR
		ENDIF
	ENDFOR
	
	FOR i = 0 TO (MC_serverBD.iNumPedCreated - 1)
		IF NOT MC_DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP(i, eSGET_Ped)
			PRINTLN("[RCC MISSION][ran] ADD_NEW_SPAWN_GROUP_TO_BUILD_IN_PLAY - Remove ped ",i," rules & Crit & FailBitset")
			FOR iTeam = 0 TO (FMMC_MAX_TEAMS - 1)
				MC_serverBD_4.iPedPriority[i][iTeam] = FMMC_PRIORITY_IGNORE
				MC_serverBD_4.iPedRule[i][iTeam] = FMMC_OBJECTIVE_LOGIC_NONE
				FMMC_CLEAR_LONG_BIT(MC_serverBD.iMissionCriticalPed[iTeam], i)
				CLEAR_BIT(MC_serverBD.iPedteamFailBitset[i], iTeam)
			ENDFOR
		ENDIF
	ENDFOR
	
	FOR i = 0 TO (MC_serverBD.iNumVehCreated - 1)
		IF NOT MC_DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP(i, eSGET_Vehicle)
			PRINTLN("[RCC MISSION][ran] ADD_NEW_SPAWN_GROUP_TO_BUILD_IN_PLAY - Remove vehicle ",i," rules & Crit & FailBitset")
			FOR iTeam = 0 TO (FMMC_MAX_TEAMS - 1)
				MC_serverBD_4.iVehPriority[i][iTeam] = FMMC_PRIORITY_IGNORE
				MC_serverBD_4.ivehRule[i][iTeam] = FMMC_OBJECTIVE_LOGIC_NONE
				CLEAR_BIT(MC_serverBD.iMissionCriticalVeh[iteam], i)
				CLEAR_BIT(MC_serverBD.iVehteamFailBitset[i], iteam)
			ENDFOR
		ENDIF
	ENDFOR
	
	FOR i = 0 TO (MC_serverBD.iNumObjCreated - 1)
		IF NOT MC_DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP(i, eSGET_Object)
			PRINTLN("[RCC MISSION][ran] ADD_NEW_SPAWN_GROUP_TO_BUILD_IN_PLAY - Remove object ",i," rules & Crit & FailBitset")
			FOR iTeam = 0 TO (FMMC_MAX_TEAMS - 1)
				MC_serverBD_4.iObjPriority[i][iteam] = FMMC_PRIORITY_IGNORE
				MC_serverBD_4.iObjRule[i][iteam] = FMMC_OBJECTIVE_LOGIC_NONE
				CLEAR_BIT(MC_serverBD.iObjteamFailBitset[i], iteam)
				CLEAR_BIT(MC_serverBD.iMissionCriticalObj[iTeam], i)
			ENDFOR
		ENDIF
	ENDFOR
	
ENDPROC

FUNC BOOL HAS_SPAWN_GROUP_PLAYTHROUGH_SETTING_BEEN_REACHED(SPAWN_GROUP_DEFINE_ACTIVATION eSpawnGroupDefineAction)
	
	INT iPlaythrough
	IF GET_CURRENT_HEIST_MISSION(TRUE) > -1
		iPlayThrough = GET_CURRENT_HEIST_COMPLETION_STAT_FOR_LEADER()
		IF iPlaythrough = -1
			iPlaythrough = 0
		ENDIF
		iPlaythrough += 1
	ENDIF
	SWITCH eSpawnGroupDefineAction
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_PERCENT_CHANCE_PLAYTHROUGH_FIRST	
			RETURN (iPlaythrough = 1)
		BREAK		
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_PERCENT_CHANCE_PLAYTHROUGH_SECOND	
			RETURN (iPlaythrough = 2)
		BREAK		
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_PERCENT_CHANCE_PLAYTHROUGH_THIRD	
			RETURN (iPlaythrough = 3)
		BREAK		
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_PERCENT_CHANCE_PLAYTHROUGH_MULTIPLE
			RETURN (iPlaythrough > 1)
		BREAK	
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_SUB_SPAWN_GROUP_TURNED_OFF(INT iSpawnGroup, INT iSpawnSubGroup)
	
	SPAWN_GROUP_DEFINE_ACTIVATION eDefineActivation = RETURN_ENUM_SPAWN_GROUP_DEFINE_ACTIVATION_CUSTOM_VARIABLE_VALUE(g_FMMC_STRUCT.eRandomEntitySpawnSub_DefineActivation[iSpawnGroup][iSpawnSubGroup])
	
	// Assigned as FORCED OFF.
	IF eDefineActivation = SPAWN_GROUP_DEFINE_ACTIVATION_OFF
		RETURN TRUE
	ENDIF
	
	// Other FORCED OFF style options:
	SWITCH eDefineActivation
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_PERCENT_CHANCE_PLAYTHROUGH_FIRST
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_PERCENT_CHANCE_PLAYTHROUGH_SECOND
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_PERCENT_CHANCE_PLAYTHROUGH_THIRD
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_PERCENT_CHANCE_PLAYTHROUGH_MULTIPLE
			IF NOT HAS_SPAWN_GROUP_PLAYTHROUGH_SETTING_BEEN_REACHED(eDefineActivation)
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_SUB_SPAWN_GROUP_CONSIDERED_FOR_RANDOM_SELECTION(INT iSpawnGroup, INT iSpawnSubGroup)
	
	SPAWN_GROUP_DEFINE_ACTIVATION eDefineActivation = RETURN_ENUM_SPAWN_GROUP_DEFINE_ACTIVATION_CUSTOM_VARIABLE_VALUE(g_FMMC_STRUCT.eRandomEntitySpawnSub_DefineActivation[iSpawnGroup][iSpawnSubGroup])
	
	SWITCH eDefineActivation
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONSIDER
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_PERCENT_CHANCE_5
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_PERCENT_CHANCE_10
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_PERCENT_CHANCE_15
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_PERCENT_CHANCE_20
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_PERCENT_CHANCE_25
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_PERCENT_CHANCE_30
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_PERCENT_CHANCE_35
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_PERCENT_CHANCE_40
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_PERCENT_CHANCE_45
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_PERCENT_CHANCE_50
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_PERCENT_CHANCE_55
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_PERCENT_CHANCE_60
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_PERCENT_CHANCE_65
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_PERCENT_CHANCE_70
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_PERCENT_CHANCE_75
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_PERCENT_CHANCE_80
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_PERCENT_CHANCE_85
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_PERCENT_CHANCE_90
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_PERCENT_CHANCE_95
			RETURN TRUE
		BREAK
	ENDSWITCH
	RETURN FALSE	
ENDFUNC

FUNC BOOL IS_SUB_SPAWN_GROUP_FORCED_ACTIVE(INT iSpawnGroup, INT iSpawnSubGroup)
	
	SPAWN_GROUP_DEFINE_ACTIVATION eDefineActivation = RETURN_ENUM_SPAWN_GROUP_DEFINE_ACTIVATION_CUSTOM_VARIABLE_VALUE(g_FMMC_STRUCT.eRandomEntitySpawnSub_DefineActivation[iSpawnGroup][iSpawnSubGroup])
	
	// Assigned as FORCED ON.
	IF eDefineActivation = SPAWN_GROUP_DEFINE_ACTIVATION_FORCED_ON
		RETURN TRUE
	ENDIF
	
	// Other FORCED ON style options:
	SWITCH eDefineActivation
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_PERCENT_CHANCE_PLAYTHROUGH_FIRST
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_PERCENT_CHANCE_PLAYTHROUGH_SECOND
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_PERCENT_CHANCE_PLAYTHROUGH_THIRD
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_PERCENT_CHANCE_PLAYTHROUGH_MULTIPLE
			IF HAS_SPAWN_GROUP_PLAYTHROUGH_SETTING_BEEN_REACHED(eDefineActivation)
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_SPAWN_GROUP_TURNED_OFF(INT i)
	
	SPAWN_GROUP_DEFINE_ACTIVATION eDefineActivation = RETURN_ENUM_SPAWN_GROUP_DEFINE_ACTIVATION_CUSTOM_VARIABLE_VALUE(g_FMMC_STRUCT.eRandomEntitySpawn_DefineActivation[i])
	
	// Assigned as FORCED OFF.
	IF eDefineActivation = SPAWN_GROUP_DEFINE_ACTIVATION_OFF
		RETURN TRUE
	ENDIF
	
	// Other FORCED OFF style options:
	SWITCH eDefineActivation
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_PERCENT_CHANCE_PLAYTHROUGH_FIRST
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_PERCENT_CHANCE_PLAYTHROUGH_SECOND
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_PERCENT_CHANCE_PLAYTHROUGH_THIRD
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_PERCENT_CHANCE_PLAYTHROUGH_MULTIPLE
			IF NOT HAS_SPAWN_GROUP_PLAYTHROUGH_SETTING_BEEN_REACHED(eDefineActivation)
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_SPAWN_GROUP_CONSIDERED_FOR_RANDOM_SELECTION(INT iSpawnGroup)	
	SPAWN_GROUP_DEFINE_ACTIVATION eDefineActivation = RETURN_ENUM_SPAWN_GROUP_DEFINE_ACTIVATION_CUSTOM_VARIABLE_VALUE(g_FMMC_STRUCT.eRandomEntitySpawn_DefineActivation[iSpawnGroup])	
	SWITCH eDefineActivation
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONSIDER
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_PERCENT_CHANCE_5
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_PERCENT_CHANCE_10
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_PERCENT_CHANCE_15
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_PERCENT_CHANCE_20
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_PERCENT_CHANCE_25
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_PERCENT_CHANCE_30
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_PERCENT_CHANCE_35
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_PERCENT_CHANCE_40
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_PERCENT_CHANCE_45
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_PERCENT_CHANCE_50
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_PERCENT_CHANCE_55
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_PERCENT_CHANCE_60
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_PERCENT_CHANCE_65
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_PERCENT_CHANCE_70
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_PERCENT_CHANCE_75
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_PERCENT_CHANCE_80
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_PERCENT_CHANCE_85
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_PERCENT_CHANCE_90
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_PERCENT_CHANCE_95
			RETURN TRUE
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_SPAWN_GROUP_FORCED_ACTIVE(INT iSpawnGroup)
	
	SPAWN_GROUP_DEFINE_ACTIVATION eDefineActivation = RETURN_ENUM_SPAWN_GROUP_DEFINE_ACTIVATION_CUSTOM_VARIABLE_VALUE(g_FMMC_STRUCT.eRandomEntitySpawn_DefineActivation[iSpawnGroup])
	
	// Assigned as FORCED ON.
	IF eDefineActivation = SPAWN_GROUP_DEFINE_ACTIVATION_FORCED_ON
		RETURN TRUE
	ENDIF
		
	// Other FORCED ON style options:
	SWITCH eDefineActivation
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_PERCENT_CHANCE_PLAYTHROUGH_FIRST
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_PERCENT_CHANCE_PLAYTHROUGH_SECOND
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_PERCENT_CHANCE_PLAYTHROUGH_THIRD
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_PERCENT_CHANCE_PLAYTHROUGH_MULTIPLE
			IF HAS_SPAWN_GROUP_PLAYTHROUGH_SETTING_BEEN_REACHED(eDefineActivation)
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_SPAWN_GROUP_BEING_OVERRIDDEN_BY_CONTINUITY(INT iSpawnGroup)
	RETURN IS_BIT_SET(g_FMMC_STRUCT.iContinuitySpawnGroupBitset, iSpawnGroup)
ENDFUNC

FUNC INT GET_CUSTOM_SPAWN_GROUP_ACTIVATION_PROBABILITY_CHANCE(SPAWN_GROUP_DEFINE_ACTIVATION eSpawnFlag)
	
	SWITCH eSpawnFlag
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_PERCENT_CHANCE_5	RETURN 5
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_PERCENT_CHANCE_10	RETURN 10
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_PERCENT_CHANCE_15	RETURN 15
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_PERCENT_CHANCE_20	RETURN 20
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_PERCENT_CHANCE_25	RETURN 25
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_PERCENT_CHANCE_30	RETURN 30
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_PERCENT_CHANCE_35	RETURN 35
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_PERCENT_CHANCE_40	RETURN 40
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_PERCENT_CHANCE_45	RETURN 45
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_PERCENT_CHANCE_50	RETURN 50
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_PERCENT_CHANCE_55	RETURN 55
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_PERCENT_CHANCE_60	RETURN 60
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_PERCENT_CHANCE_65	RETURN 65
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_PERCENT_CHANCE_70	RETURN 70
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_PERCENT_CHANCE_75	RETURN 75
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_PERCENT_CHANCE_80	RETURN 80
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_PERCENT_CHANCE_85	RETURN 85
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_PERCENT_CHANCE_90	RETURN 90
		CASE SPAWN_GROUP_DEFINE_ACTIVATION_CONDITION_PERCENT_CHANCE_95	RETURN 95
	ENDSWITCH
	
	RETURN 101
ENDFUNC

FUNC BOOL HAS_SUB_SPAWN_GROUP_PASSED_ITS_ACTIVATION_CHANCE_CHECK(INT iSpawnGroup, INT iSubSpawnGroup)
	INT iRand = GET_RANDOM_INT_IN_RANGE(0, 101)	// won't be higher than 100
	INT iProbability = GET_CUSTOM_SPAWN_GROUP_ACTIVATION_PROBABILITY_CHANCE(RETURN_ENUM_SPAWN_GROUP_DEFINE_ACTIVATION_CUSTOM_VARIABLE_VALUE(g_FMMC_STRUCT.eRandomEntitySpawnSub_DefineActivation[iSpawnGroup][iSubSpawnGroup]))
	IF iRand > iProbability
		PRINTLN("[SpawnGroups] INIT_SERVER_DATA / SEED_SPAWN_SUBGROUP - iSpawnGroup: ", iSpawnGroup, " / iSubSpawnGroup: ", iSubSpawnGroup, " / iRand: ", iRand, " / iProbability: ", iProbability, " WILL NOT BE ACTIVATED - THE SPAWN GROUP FAILED IT'S CHANCE OF SPAWNING")
		RETURN FALSE
	ELIF iProbability != 101
		PRINTLN("[SpawnGroups] INIT_SERVER_DATA / SEED_SPAWN_SUBGROUP - iSpawnGroup: ", iSpawnGroup, " / iSubSpawnGroup: ", iSubSpawnGroup, " / iRand: ", iRand, " / iProbability: ", iProbability, " This Spawn Group was activated via the Percent Chance flag.")
	ENDIF
	RETURN TRUE
ENDFUNC

FUNC BOOL HAS_SPAWN_GROUP_PASSED_ITS_ACTIVATION_CHANCE_CHECK(INT iSpawnGroup)	
	INT iRand = GET_RANDOM_INT_IN_RANGE(0, 101)	// won't be higher than 100
	INT iProbability = GET_CUSTOM_SPAWN_GROUP_ACTIVATION_PROBABILITY_CHANCE(RETURN_ENUM_SPAWN_GROUP_DEFINE_ACTIVATION_CUSTOM_VARIABLE_VALUE(g_FMMC_STRUCT.eRandomEntitySpawn_DefineActivation[iSpawnGroup]))
	IF iRand > iProbability
		PRINTLN("[SpawnGroups] INIT_SERVER_DATA / SEED_SPAWN_GROUPS - iSpawnGroup: ", iSpawnGroup, " / iRand: ", iRand, " / iProbability: ", iProbability, " WILL NOT BE ACTIVATED - THE SPAWN GROUP FAILED IT'S CHANCE OF SPAWNING")
		RETURN FALSE
	ELIF iProbability != 101
		PRINTLN("[SpawnGroups] INIT_SERVER_DATA / SEED_SPAWN_GROUPS - iSpawnGroup: ", iSpawnGroup, " / iRand: ", iRand, " / iProbability: ", iProbability, " This Spawn Group was activated via the Percent Chance flag.")
	ENDIF
	RETURN TRUE
ENDFUNC


FUNC BOOL HAS_CONTINUITY_CONDITION_BEEN_MET_FOR_SPAWN_GROUP(INT iSpawnGroup)
	
	IF NOT IS_SPAWN_GROUP_BEING_OVERRIDDEN_BY_CONTINUITY(iSpawnGroup)
		RETURN TRUE
	ENDIF
	
	IF g_FMMC_STRUCT.iContinuitySpawnGroupContinuityId[iSpawnGroup] = -1
		PRINTLN("CONTINUITY] HAS_CONTINUITY_CONDITION_BEEN_MET_FOR_SPAWN_GROUP - Spawn group ", iSpawnGroup, " set to be activated by continuity but no ID set")
		RETURN FALSE
	ENDIF
	
	IF g_FMMC_STRUCT.iContinuitySpawnGroupCondition[iSpawnGroup] = ciMISSION_CONTINUITY_SPAWN_GROUP_CONDITION_LOCATE_COMPLETE
		
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_LocationTracking)
			PRINTLN("CONTINUITY] HAS_CONTINUITY_CONDITION_BEEN_MET_FOR_SPAWN_GROUP - Spawn group ", iSpawnGroup, " set to be activated by location continuity but tracking is off")
			RETURN FALSE
		ENDIF
		
		IF NOT IS_BIT_SET(g_TransitionSessionNonResetVars.sMissionContinuityVars.iLocationTrackingBitset, g_FMMC_STRUCT.iContinuitySpawnGroupContinuityId[iSpawnGroup])
			PRINTLN("CONTINUITY] HAS_CONTINUITY_CONDITION_BEEN_MET_FOR_SPAWN_GROUP - Spawn group ", iSpawnGroup, " set to be activated by location continuity and location continuity ID ", g_FMMC_STRUCT.iContinuitySpawnGroupContinuityId[iSpawnGroup]," was not set")
			RETURN FALSE
		ENDIF
		
	ELIF g_FMMC_STRUCT.iContinuitySpawnGroupCondition[iSpawnGroup] = ciMISSION_CONTINUITY_SPAWN_GROUP_CONDITION_OBJECT_COMPLETE
		
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_ObjectTracking)
			PRINTLN("CONTINUITY] HAS_CONTINUITY_CONDITION_BEEN_MET_FOR_SPAWN_GROUP - Spawn group ", iSpawnGroup, " set to be activated by object continuity but tracking is off")
			RETURN FALSE
		ENDIF
		
		IF NOT FMMC_IS_LONG_BIT_SET(g_TransitionSessionNonResetVars.sMissionContinuityVars.iObjectTrackingBitset, g_FMMC_STRUCT.iContinuitySpawnGroupContinuityId[iSpawnGroup])
			PRINTLN("CONTINUITY] HAS_CONTINUITY_CONDITION_BEEN_MET_FOR_SPAWN_GROUP - Spawn group ", iSpawnGroup, " set to be activated by location continuity and object continuity ID ", g_FMMC_STRUCT.iContinuitySpawnGroupContinuityId[iSpawnGroup]," was not set")
			RETURN FALSE
		ENDIF
		
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC MC_ASSIGN_PMC_BASED_ON_FIRST_ACTIVE_SUB_SPAWN_GROUP(INT iSpawnGroup, INT iTeam)
	
	IF iSpawnGroup = -1	
		EXIT
	ENDIF
	
	INT iIndex = -1
	INT iPMC
	INT i
	FOR i = 0 TO ciMAX_NUM_FMMC_ENTITY_SUB_SPAWN_GROUPS-1
		IF IS_BIT_SET(MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeed_SubGroupBS[iSpawnGroup], i)			
			iIndex = i
			PRINTLN("[PMC] - MC_ASSIGN_PMC_BASED_ON_FIRST_ACTIVE_SUB_SPAWN_GROUP - iSpawnGroup: ", iSpawnGroup, " iIndex: ",  iIndex, " Selected.")
			BREAKLOOP
		ENDIF
	ENDFOR
	
	IF iIndex = -1	
		PRINTLN("[PMC] - MC_ASSIGN_PMC_BASED_ON_FIRST_ACTIVE_SUB_SPAWN_GROUP - iSpawnGroup: ", iSpawnGroup, " iIndex: ",  iIndex, " No Subs were active. Exitting and not overriding.")
		EXIT
	ENDIF
	
	IF iIndex >= FMMC_MAX_PMC_OVERRIDES
		PRINTLN("[PMC] - MC_ASSIGN_PMC_BASED_ON_FIRST_ACTIVE_SUB_SPAWN_GROUP - iSpawnGroup: ", iSpawnGroup, " iIndex: ",  iIndex, " Max: ", FMMC_MAX_PMC_OVERRIDES, " Override was out of range. Exitting.")
		EXIT
	ENDIF
	
	iPMC = g_FMMC_STRUCT.iPostMissionSceneId_Overrides[iTeam][iIndex]
	
	PRINTLN("[PMC] - MC_ASSIGN_PMC_BASED_ON_FIRST_ACTIVE_SUB_SPAWN_GROUP - iPMC: ", iPMC, " Using and broadcasting this override.")
	
	BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_Override_PMC, DEFAULT, DEFAULT, DEFAULT, DEFAULt, DEFAULT, DEFAULt, DEFAULT, iPMC)
	
ENDPROC

PROC MODIFY_SPAWN_GROUP_DURING_GAMEPLAY_FROM_BITSET(INT iSpawnGroupAdditionalFunctionalityBS, INT iSpawnGroupBS, INT &iSubSpawnGroupBS[])
	
	PRINTLN("[SpawnGroups] MODIFY_SPAWN_GROUP_DURING_GAMEPLAY_FROM_BITSET - Calling function - Modifying SPAWN GROUPS / SUB SPAWN GROUPS during gameplay! ###################")
	
	INT i, ii	
	FOR i = 0 TO FMMC_MAX_RANDOM_ENTITY_SPAWN_GROUPS-1
		IF IS_BIT_SET(iSpawnGroupBS, i)			
			IF IS_BIT_SET(iSpawnGroupAdditionalFunctionalityBS, ciSPAWN_GROUP_ADDITIONAL_ENTITY_FUNCTIONALITY_ACTIVATE_ON_EVENT)
				PRINTLN("[SpawnGroups] MODIFY_SPAWN_GROUP_DURING_GAMEPLAY_FROM_BITSET - Spawn group ", i, " has been flagged for addition, setting bit in iEntitySpawnSeedBS")
				SET_BIT(MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeedBS, i)
			ELIF IS_BIT_SET(iSpawnGroupAdditionalFunctionalityBS, ciSPAWN_GROUP_ADDITIONAL_ENTITY_FUNCTIONALITY_DEACTIVATE_ON_EVENT)
				PRINTLN("[SpawnGroups] MODIFY_SPAWN_GROUP_DURING_GAMEPLAY_FROM_BITSET - Spawn group ", i, " has been flagged for removal, clearing bit in iEntitySpawnSeedBS")
				CLEAR_BIT(MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeedBS, i)
			ENDIF
		ENDIF
		
		FOR ii = 0 TO ciMAX_NUM_FMMC_ENTITY_SUB_SPAWN_GROUPS-1
			IF IS_BIT_SET(iSubSpawnGroupBS[i], ii)
				IF IS_BIT_SET(iSpawnGroupAdditionalFunctionalityBS, ciSPAWN_GROUP_ADDITIONAL_ENTITY_FUNCTIONALITY_ACTIVATE_ON_EVENT)
					PRINTLN("[SpawnGroups] MODIFY_SPAWN_GROUP_DURING_GAMEPLAY_FROM_BITSET - Spawn group ", i, " Sub Spawn Group ", ii, " has been flagged for addition, setting bit in iEntitySpawnSeedBS")
					SET_BIT(MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeed_SubGroupBS[i], ii)
				ELIF IS_BIT_SET(iSpawnGroupAdditionalFunctionalityBS, ciSPAWN_GROUP_ADDITIONAL_ENTITY_FUNCTIONALITY_DEACTIVATE_ON_EVENT)
					PRINTLN("[SpawnGroups] MODIFY_SPAWN_GROUP_DURING_GAMEPLAY_FROM_BITSET - Spawn group ", i, " Sub Spawn Group ", ii, " has been flagged for removal, clearing bit in iEntitySpawnSeedBS")
					CLEAR_BIT(MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeed_SubGroupBS[i], ii)
				ENDIF
			ENDIF
		ENDFOR
	ENDFOR
		
	MC_PROCESS_REPOSITION_ENTITY_BASED_ON_SUB_SPAWN_GROUP_SETTINGS(g_FMMC_STRUCT.sWarpLocationSettings_EndCutscene, CREATION_TYPE_END_CUTSCENE, 0, g_FMMC_STRUCT.iEndCutsceneSpawnGroup, g_FMMC_STRUCT.iEndCutsceneSpawnSubGroupBS, NULL)
	
	INT iTeam = 0
	FOR iTeam = 0 TO FMMC_MAX_TEAMS-1 // server num not init yet.
		MC_ASSIGN_PMC_BASED_ON_FIRST_ACTIVE_SUB_SPAWN_GROUP(g_FMMC_STRUCT.iPostMissionSceneId_OverrideWithSpawnGroup[iTeam], iTeam)
	ENDFOR
	
	SET_BIT(MC_serverBD_4.rsgSpawnSeed.iMiscBS, ciRandomSpawnGroupBS_RequestResetPending)
	
	#IF IS_DEBUG_BUILD
	ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_GAMEPLAY_MODIFIED, ENTITY_RUNTIME_ERROR_TYPE_NOTIFICATION, "Spawn Groups have been modified during gameplay!")	
	#ENDIF
	
	PRINTLN("[SpawnGroups] MODIFY_SPAWN_GROUP_DURING_GAMEPLAY_FROM_BITSET - Setting ciRandomSpawnGroupBS_RequestResetPending")
	
ENDPROC

PROC SEED_SPAWN_SUBGROUP(INT iSpawnGroupArray)

	IF ARE_SPAWN_GROUP_SETTINGS_VALID(iSpawnGroupArray)
		
		INT i = 0
		INT iActiveSubSpawnGroups = 0
		INT iMaxAttempts = 500 // Safer than While Loop in case contenet is set up poorly.
		INT iMaxNum = ciMAX_NUM_FMMC_ENTITY_SUB_SPAWN_GROUPS
		INT iNumToChoose = g_FMMC_STRUCT.iRandomEntitySpawnSub_RandomNumbersToUse[iSpawnGroupArray]
		INT iSpawnGroupChecked
		
		PRINTLN("[SpawnGroups] INIT_SERVER_DATA / SEED_SPAWN_SUBGROUP - Getting randomised entity subgroup spawn seeds for iSpawnGroupArray ", iSpawnGroupArray, ": number of available variations = ", iMaxNum,", number of randoms to seed = ", iNumToChoose)		
		PRINTLN("[SpawnGroups] INIT_SERVER_DATA / SEED_SPAWN_SUBGROUP, (BEFORE) - MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeed_SubGroupBS[",iSpawnGroupArray,"] = ",MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeed_SubGroupBS[iSpawnGroupArray])
		
		// Fine Tuned Selection:
		IF ARE_SPAWN_GROUPS_USING_CREATOR_DEFINE_SETTINGS()
			iMaxNum = ciMAX_NUM_FMMC_ENTITY_SUB_SPAWN_GROUPS
			
			PRINTLN("[SpawnGroups] INIT_SERVER_DATA / SEED_SPAWN_SUBGROUP - Using Pre-defined Creator Settings")
			
			FOR i = 0 TO (iMaxNum - 1)
				IF (ARE_SPAWN_GROUPS_USING_CREATOR_DEFINE_SETTINGS() AND IS_SUB_SPAWN_GROUP_FORCED_ACTIVE(iSpawnGroupArray, i))
					SET_BIT(MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeed_SubGroupBS[iSpawnGroupArray], i)
					PRINTLN("[SpawnGroups] INIT_SERVER_DATA / SEED_SPAWN_SUBGROUP - Forcing Spawngroup: ", i, " as active")
				ENDIF
			ENDFOR
		ENDIF
		
		FOR i = 0 TO iMaxAttempts-1 // for safety, adding a max number of attempts.
			
			// Breakloop - Met the number of sub spawn groups required to be activated.
			IF iActiveSubSpawnGroups >= iNumToChoose
				PRINTLN("[SpawnGroups] INIT_SERVER_DATA / SEED_SPAWN_SUBGROUP - Attempt: ", i, " requirement for number of spawn groups has been met: ", iActiveSubSpawnGroups, " >= ", iNumToChoose, " Breaking out of loop.")
				BREAKLOOP
			ENDIF
						
			INT iRandom = GET_RANDOM_INT_IN_RANGE(0, iMaxNum)
						
			IF IS_BIT_SET(iSpawnGroupChecked, iRandom)
				RELOOP
			ENDIF
			
			IF IS_BIT_SET(iSpawnGroupChecked, iRandom)
				PRINTLN("[SpawnGroups] INIT_SERVER_DATA / SEED_SPAWN_SUBGROUP - Attempt: ", i, " iSpawnGroupArray ", iSpawnGroupArray, " i ", i, " Spawn Sub Group: ", iRandom, " has already been set. Relooping.")				
				RELOOP
			ENDIF
			
			// If this option is set we don't want to choose the same sub group as in the previous round
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciDONT_REPEAT_SPAWN_SUB_GROUP)
			AND IS_BIT_SET(g_TransitionSessionNonResetVars.iSpawnSubGroupLastBitset[iSpawnGroupArray], iRandom - 1)
				PRINTLN("[SpawnGroups] INIT_SERVER_DATA / SEED_SPAWN_SUBGROUP - Attempt: ", i, " iSpawnGroupArray ", iSpawnGroupArray, " i ", i, " Spawn Group ", iRandom, " Don't Repeat Groups turned on, and we have selected a repeat.")
				RELOOP
			ENDIF
			
			IF IS_SPAWN_GROUP_BEING_OVERRIDDEN_BY_CONTINUITY(iRandom)
			AND NOT HAS_CONTINUITY_CONDITION_BEEN_MET_FOR_SPAWN_GROUP(iRandom)
				PRINTLN("[SpawnGroups] INIT_SERVER_DATA / SEED_SPAWN_SUBGROUP - Attempt: ", i, " iSpawnGroupArray ", iSpawnGroupArray, " i ", i," Spawn Group ", iRandom, " continuity condition not met.")
				RELOOP
			ENDIF
			
			IF ARE_SPAWN_GROUPS_USING_CREATOR_DEFINE_SETTINGS()
				IF IS_SUB_SPAWN_GROUP_FORCED_ACTIVE(iSpawnGroupArray, iRandom)
					RELOOP
				ENDIF
				IF IS_SUB_SPAWN_GROUP_TURNED_OFF(iSpawnGroupArray, iRandom)
					RELOOP
				ENDIF
			ENDIF
			
			IF NOT IS_SUB_SPAWN_GROUP_CONSIDERED_FOR_RANDOM_SELECTION(iSpawnGroupArray, iRandom)
				PRINTLN("[SpawnGroups] INIT_SERVER_DATA / SEED_SPAWN_SUBGROUP - Attempt: ", i, " iSpawnGroupArray ", iSpawnGroupArray, " i ", i," Spawn Group ", iRandom, " Not set to be considered for random selection")
				RELOOP
			ENDIF
			
			iActiveSubSpawnGroups++
			
			IF HAS_SUB_SPAWN_GROUP_PASSED_ITS_ACTIVATION_CHANCE_CHECK(iSpawnGroupArray, iRandom)
				SET_BIT(MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeed_SubGroupBS[iSpawnGroupArray], iRandom)
				PRINTLN("[SpawnGroups] INIT_SERVER_DATA / SEED_SPAWN_SUBGROUP - Attempt: ", i, " iSpawnGroupArray ", iSpawnGroupArray, " Setting Sub Spawn Group: ", iRandom, " iActiveSubSpawnGroups: ", iActiveSubSpawnGroups)
			ENDIF
			
			SET_BIT(iSpawnGroupChecked, iRandom)
		ENDFOR
			
		// Assert
		IF i >= iMaxAttempts
			PRINTLN("[SpawnGroups] INIT_SERVER_DATA / SEED_SPAWN_SUBGROUP - Attempt: ", i, " iSpawnGroupArray ", iSpawnGroupArray, " Got stuck in a loop Bailing.....")
			ASSERTLN("SEED_SPAWN_SUBGROUP - COULD NOT COMPLETE SPAWN GROUP SEEDING. INCORRECT SETTINGS. CHECK RUN ERRORS IN 6 MENU.")
			
			#IF IS_DEBUG_BUILD
			ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_SPAWN_GROUPS_LOOP, ENTITY_RUNTIME_ERROR_TYPE_CRITICAL, "Sub Spawn Group Loop exceeded #. Check Spawn Group # settings in the creator!", iMaxAttempts, iSpawnGroupArray)	
			#ENDIF
		ENDIF
		
		PRINTLN("[SpawnGroups] INIT_SERVER_DATA / SEED_SPAWN_SUBGROUP, (AFTER) MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeed_SubGroupBS[",iSpawnGroupArray,"] = ",MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeed_SubGroupBS[iSpawnGroupArray], " iActiveSubSpawnGroups: ", iActiveSubSpawnGroups)
	ENDIF
	
ENDPROC

PROC LOAD_SPAWN_GROUPS(BOOL &bCanAddToSpawnGroups)
	IF (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciOPTIONS4_MAINTAIN_SPAWN_GROUPS_ON_RESTART) AND (IS_THIS_A_QUICK_RESTART_JOB() OR IS_CORONA_INITIALISING_A_QUICK_RESTART()))
	OR (IS_A_STRAND_MISSION_BEING_INITIALISED()	AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFive, ciOptionsBS25_MaintainSpawnGroupOverStrand_LoadOnly))
		MC_serverBD_4.rsgSpawnSeed = g_TransitionSessionNonResetVars.rsgFMMCSpawnSeed
		PRINTLN("[SpawnGroups] INIT_SERVER_DATA / SEED_SPAWN_GROUPS - This is a quick restart / strand mission, loading spawn group data, g_TransitionSessionNonResetVars.rsgFMMCSpawnSeed.iEntitySpawnSeedBS: ", g_TransitionSessionNonResetVars.rsgFMMCSpawnSeed.iEntitySpawnSeedBS)
		INT i = 0
		FOR i = 0 TO ciMAX_NUM_FMMC_ENTITY_SUB_SPAWN_GROUPS-1 
			PRINTLN("[SpawnGroups] INIT_SERVER_DATA / SEED_SPAWN_GROUPS - This is a quick restart / strand mission, loading spawn group data, g_TransitionSessionNonResetVars.rsgFMMCSpawnSeed.iEntitySpawnSeed_SubGroupBS[",i,"] = ", g_TransitionSessionNonResetVars.rsgFMMCSpawnSeed.iEntitySpawnSeed_SubGroupBS[i])
		ENDFOR
		
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySeven, ciOptionsBS27_MaintainSpawnGroupOverStrand_AllowAddingToLoaded)
		OR (IS_THIS_A_QUICK_RESTART_JOB() OR IS_CORONA_INITIALISING_A_QUICK_RESTART())
			bCanAddToSpawnGroups = FALSE
		ENDIF
	ELSE
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFive, ciOptionsBS25_MaintainSpawnGroupOverStrand_DontClearSaved)
			CLEAR_SPAWN_GROUP_STRUCT(g_TransitionSessionNonResetVars.rsgFMMCSpawnSeed)
		ELSE
			PRINTLN("[SpawnGroups] INIT_SERVER_DATA / SEED_SPAWN_GROUPS - NOT CLEARING g_TransitionSessionNonResetVars.rsgFMMCSpawnSeed: ", g_TransitionSessionNonResetVars.rsgFMMCSpawnSeed.iEntitySpawnSeedBS, " as ciOptionsBS25_MaintainSpawnGroupOverStrand_DontClearSaved is set.")
		ENDIF
	ENDIF
ENDPROC

PROC SEED_SPAWN_GROUPS()
	
	CLEAR_ENTITY_CACHED_SPAWN_GROUP_RESULT_DATA()
	
	// Force override spawn groups with debug.
	#IF IS_DEBUG_BUILD
	IF g_iMissionForcedSpawnGroup <> 0
		MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeedBS = g_iMissionForcedSpawnGroup
		PRINTLN("[LM][SpawnGroups] - [SpwnGrpDbg] Forcing Spawn Group Bitset: ", g_iMissionForcedSpawnGroup)
		INT i = 0 
		FOR i = 0 TO ciMAX_NUM_FMMC_ENTITY_SUB_SPAWN_GROUPS-1
			IF g_iMissionForcedSpawnSubGroup[i] <> 0
				PRINTLN("[LM][SpawnGroups] - [SpwnGrpDbg] Forcing Spawn Sub Group Bitset", g_iMissionForcedSpawnSubGroup[i], " For Spawn Group ", i)
				MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeed_SubGroupBS[i] = g_iMissionForcedSpawnSubGroup[i]
			ENDIF
		ENDFOR
		MC_ServerBD_4.rsgSpawnSeed.bForceValidate = TRUE
		EXIT
	ENDIF
	#ENDIF
	
	BOOL bCanSeedSpawnGroups = TRUE
	
	// Load and check whether we can add onto loaded groups.
	LOAD_SPAWN_GROUPS(bCanSeedSpawnGroups)	
	
	IF NOT ARE_SPAWN_GROUP_SETTINGS_VALID()
		bCanSeedSpawnGroups = FALSE
		PRINTLN("[SpawnGroups] INIT_SERVER_DATA / SEED_SPAWN_GROUPS - Spawn group Settings are not valid.")
	ENDIF
	
	IF bCanSeedSpawnGroups
		PRINTLN("[SpawnGroups] INIT_SERVER_DATA / SEED_SPAWN_GROUPS - Able to seed spawn groups")
		
		INT i = 0
		INT iMaxAttempts = 500 // Safer than While Loop in case contenet is set up poorly.
		INT iActiveSpawnGroups = 0
		INT iMaxNum = g_FMMC_STRUCT.iRandomEntitySpawn_MaxNumber
		INT iNumToChoose = GET_NUMBER_OF_RANDOM_ENTITY_SPAWN_GROUPS_TO_SPAWN()			
		INT iSpawnGroupChecked
				
		// Fine Tuned Selection:
		IF ARE_SPAWN_GROUPS_USING_CREATOR_DEFINE_SETTINGS()		
			iMaxNum = FMMC_MAX_RANDOM_ENTITY_SPAWN_GROUPS
			
			PRINTLN("[SpawnGroups] INIT_SERVER_DATA / SEED_SPAWN_GROUPS - Using Pre-defined Creator Settings")
			
			FOR i = 0 TO (iMaxNum - 1)
				IF (ARE_SPAWN_GROUPS_USING_CREATOR_DEFINE_SETTINGS() AND IS_SPAWN_GROUP_FORCED_ACTIVE(i))
				OR (IS_SPAWN_GROUP_BEING_OVERRIDDEN_BY_CONTINUITY(i) AND HAS_CONTINUITY_CONDITION_BEEN_MET_FOR_SPAWN_GROUP(i))
					SET_BIT(MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeedBS, i)
					PRINTLN("[SpawnGroups] INIT_SERVER_DATA / SEED_SPAWN_GROUPS - Forcing Spawngroup: ", i, " as active")
					SEED_SPAWN_SUBGROUP(i)
				ENDIF
			ENDFOR
		ENDIF
		
		IF (iMaxNum > 0 AND iNumToChoose > 0)
			PRINTLN("[SpawnGroups] INIT_SERVER_DATA / SEED_SPAWN_GROUPS - Attempting to activate randomised entity spawn groups up to SpawnGroup: ", iMaxNum, ", Spawn groups to be active: ",iNumToChoose)
			
			FOR i = 0 TO iMaxAttempts-1 // for safety, adding a max number of attempts.
				
				// Breakloop - Met the number of spawn groups required to be activated.
				IF iActiveSpawnGroups >= iNumToChoose
					PRINTLN("[SpawnGroups] INIT_SERVER_DATA / SEED_SPAWN_GROUPS - Attempt: ", i, " requirement for number of spawn groups has been met: ", iActiveSpawnGroups, " >= ", iNumToChoose, " Breaking out of loop.")
					BREAKLOOP
				ENDIF
				
				INT iRandom = GET_RANDOM_INT_IN_RANGE(0, iMaxNum)
									
				IF IS_BIT_SET(iSpawnGroupChecked, iRandom)
					RELOOP
				ENDIF				
				
				IF IS_BIT_SET(MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeedBS, iRandom)
					RELOOP
				ENDIF
				
				// If this option is set we don't want to choose the same group as in the previous round
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciDONT_REPEAT_SPAWN_GROUP)
				AND IS_BIT_SET(g_TransitionSessionNonResetVars.iSpawnGroupLastBitset, iRandom)
					PRINTLN("[SpawnGroups] INIT_SERVER_DATA / SEED_SPAWN_GROUPS - Attempt: ", i, " Spawn Group ", iRandom, " Don't Repeat Groups turned on, and we have selected a repeat.")
					RELOOP
				ENDIF
				
				IF ARE_SPAWN_GROUPS_USING_CREATOR_DEFINE_SETTINGS()
					IF IS_SPAWN_GROUP_FORCED_ACTIVE(iRandom)
						RELOOP
					ENDIF
					IF IS_SPAWN_GROUP_TURNED_OFF(iRandom)
						RELOOP
					ENDIF
				ENDIF
				
				IF NOT IS_SPAWN_GROUP_CONSIDERED_FOR_RANDOM_SELECTION(iRandom)
					PRINTLN("[SpawnGroups] INIT_SERVER_DATA / SEED_SPAWN_GROUPS - Attempt: ", i, " Spawn Group: ", iRandom, " Not set to be considered for random selection")
					RELOOP
				ENDIF
				
				iActiveSpawnGroups++
				
				IF HAS_SPAWN_GROUP_PASSED_ITS_ACTIVATION_CHANCE_CHECK(iRandom)
					SET_BIT(MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeedBS, iRandom)
					PRINTLN("[SpawnGroups] INIT_SERVER_DATA / SEED_SPAWN_GROUPS - Attempt: ", i, " Setting Spawn Group: ", iRandom, " iActiveSpawnGroups: ", iActiveSpawnGroups)
					SEED_SPAWN_SUBGROUP(iRandom)
				ENDIF
				
				SET_BIT(iSpawnGroupChecked, iRandom)
			ENDFOR
		
			// Assert
			IF i >= iMaxAttempts
				PRINTLN("[SpawnGroups] INIT_SERVER_DATA / SEED_SPAWN_GROUPS - Attempt: ", i, " Got stuck in a loop Bailing.....")
				ASSERTLN("SEED_SPAWN_GROUPS - COULD NOT COMPLETE SPAWN GROUP SEEDING. INCORRECT SETTINGS. CHECK RUN ERRORS IN 6 MENU.")
				
				#IF IS_DEBUG_BUILD
				ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_SPAWN_GROUPS_LOOP, ENTITY_RUNTIME_ERROR_TYPE_CRITICAL, "Spawn Group Loop exceeded #. Check your settings in the creator!", iMaxAttempts)	
				#ENDIF
			ENDIF
			
		ENDIF
	ENDIF
	
	PRINTLN("[SpawnGroups] INIT_SERVER_DATA / SEED_SPAWN_GROUPS - MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeedBS = ",MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeedBS)
	INT i = 0
	FOR i = 0 TO ciMAX_NUM_FMMC_ENTITY_SUB_SPAWN_GROUPS-1 
		PRINTLN("[SpawnGroups] INIT_SERVER_DATA / SEED_SPAWN_GROUPS - MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeed_SubGroupBS[",i,"] = ", MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeed_SubGroupBS[i])
	ENDFOR
ENDPROC

