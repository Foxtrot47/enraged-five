// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### System: GTA V Online Mission Controller - Debug Profiler ----------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Script: FM_Mission_Controller_2020.sc -----------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: System for tracking Mission Controller script performance.
// ##### Partially adapted from singleplayer\include\debug\profiler.sch
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### This script was forked from FM_Mission_Controller.sc at the beginning of 2020		 																				  
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

#IF IS_DEBUG_BUILD

USING "FM_Mission_Controller_Utility_2020.sch"
				
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Drawing
// ##### Description: Functions to draw the profiler menu
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC VECTOR_2D GET_PROFILER_WINDOW_CENTRE_POS(VECTOR_2D &vScale)
	RETURN INIT_VECTOR_2D(fProfilerPosX - (vScale.x / 2), fProfilerPosY + (vScale.y / 2))
ENDFUNC

FUNC VECTOR_2D GET_PROFILER_WINDOW_SCALE(INT iProfilerCount)
	
	FLOAT fY = (fProfilerTextPaddingY * 2) + (fProfilerTextTitleSpacingY * ciFMMC_PROFILER_DEFAULT_ROWS)
	fY += (iProfilerCount * 2) * fProfilerTextSpacingY
	
	RETURN INIT_VECTOR_2D(fProfilerScaleX, fY)
ENDFUNC

PROC DRAW_PROFILER_TEXT(VECTOR_2D &vPos, VECTOR_2D &vScale, STRING stText, eTextJustification eJustification = FONT_LEFT, BOOL bIncrementNumDrawn = TRUE, BOOL bColour = TRUE)
	IF bColour
		SET_TEXT_COLOUR(225, 225, 225, 255)
	ENDIF
	SET_TEXT_JUSTIFICATION(eJustification)
	FLOAT fX = vPos.x
	FLOAT fY = vPos.y + fProfilerTextPaddingY - (vScale.y / 2) 
	IF iProfilerRowsDrawn <= ciFMMC_PROFILER_DEFAULT_ROWS
		fY += (fProfilerTextTitleSpacingY * iProfilerRowsDrawn)
	ELSE
		fY += fProfilerTextTitleSpacingY * ciFMMC_PROFILER_DEFAULT_ROWS
		fY += fProfilerTextSpacingY * (iProfilerRowsDrawn - ciFMMC_PROFILER_DEFAULT_ROWS)
	ENDIF
	IF eJustification = FONT_LEFT
		fX += fProfilerTextPaddingX - (vScale.x / 2)
	ELIF eJustification = FONT_RIGHT
		fX += (vScale.x / 2) - fProfilerTextPaddingX
	ENDIF
	DISPLAY_TEXT_WITH_LITERAL_STRING(fX, fY, "STRING", stText)
	IF bIncrementNumDrawn
		iProfilerRowsDrawn++
	ENDIF
ENDPROC

FUNC STRING GET_PROFILER_TITLE(INT iProfilerIndex)
	SWITCH iProfilerIndex
		CASE ciFMMC_PROFILER_OVERALL 		RETURN "Overall:"
		CASE ciFMMC_PROFILER_PLAYERS 		RETURN "Players:"
		CASE ciFMMC_PROFILER_LOCAL_PLAYER	RETURN "Local Player:"
		CASE ciFMMC_PROFILER_ENTITIES 		RETURN "Entities:"
		CASE ciFMMC_PROFILER_PEDS 			RETURN "Peds:"
		CASE ciFMMC_PROFILER_VEHICLES 		RETURN "Vehicles:"
		CASE ciFMMC_PROFILER_TRAINS 		RETURN "Trains:"
		CASE ciFMMC_PROFILER_LOCATIONS 		RETURN "Locations:"
		CASE ciFMMC_PROFILER_OBJECTS 		RETURN "Objects:"
		CASE ciFMMC_PROFILER_DYNOPROPS 		RETURN "DynoProps:"
		CASE ciFMMC_PROFILER_INTERACTABLES 	RETURN "Interactables:"
		CASE ciFMMC_PROFILER_GANGCHASE 		RETURN "GangChase:"
		CASE ciFMMC_PROFILER_DIALOGUE 		RETURN "Dialogue:"
		CASE ciFMMC_PROFILER_ZONES 			RETURN "Zones:"
		CASE ciFMMC_PROFILER_BOUNDS 		RETURN "Bounds:"
		CASE ciFMMC_PROFILER_PROPS 			RETURN "Props:"
		CASE ciFMMC_PROFILER_DOORS 			RETURN "Doors:"
		CASE ciFMMC_PROFILER_DUMMYBLIPS 	RETURN "DummyBlips:"
		CASE ciFMMC_PROFILER_PICKUPS 		RETURN "Pickups:"
		CASE ciFMMC_PROFILER_CUSTOM 		RETURN "Custom:"
	ENDSWITCH
	RETURN ""
ENDFUNC

PROC DRAW_PROFILER_BAR(VECTOR_2D &vPos, VECTOR_2D &vScale, INT iProfilerIndex)
	
	IF sProfilerData[ciFMMC_PROFILER_OVERALL].fProfilerUpdatePeak = 0.0
	OR sProfilerData[iProfilerIndex].iProfilerUpdateCount = 0
		iProfilerRowsDrawn++
		EXIT
	ENDIF
	
	FLOAT fX = vPos.x + fProfilerTextPaddingX
	FLOAT fY = vPos.y + fProfilerTextPaddingY - (vScale.y / 2) + fProfilerBarSpacingY
	fY += fProfilerTextTitleSpacingY * ciFMMC_PROFILER_DEFAULT_ROWS
	fY += fProfilerTextSpacingY * (iProfilerRowsDrawn - ciFMMC_PROFILER_DEFAULT_ROWS)
	
	FLOAT fScaleX = (vScale.x / 2) - (fProfilerTextPaddingX * 2)
	
	DRAW_RECT_FROM_CORNER(fX, fY, fScaleX, fProfilerBarHeight, 0, 0, 0, 255)
	
	FLOAT fUpdateScaleX = fScaleX * (sProfilerData[iProfilerIndex].fProfilerUpdate / sProfilerData[ciFMMC_PROFILER_OVERALL].fProfilerUpdatePeak)
	DRAW_RECT_FROM_CORNER(fX, fY, fUpdateScaleX, fProfilerBarHeight, 255, 0, 0, 255)
	
	FLOAT fAvgScaleX = fScaleX * ((sProfilerData[iProfilerIndex].fProfilerUpdateSum / sProfilerData[iProfilerIndex].iProfilerUpdateCount) / sProfilerData[ciFMMC_PROFILER_OVERALL].fProfilerUpdatePeak)
	DRAW_RECT(fX + (fAvgScaleX), fY + (fProfilerBarHeight / 2), fProfilerBarIndicatorWidth, fProfilerBarIndicatorHeight, 255, 255, 255, 255)
	
	FLOAT fPeakScaleX = fScaleX * (sProfilerData[iProfilerIndex].fProfilerUpdatePeak / sProfilerData[ciFMMC_PROFILER_OVERALL].fProfilerUpdatePeak)
	DRAW_RECT(fX + (fPeakScaleX), fY + (fProfilerBarHeight / 2), fProfilerBarIndicatorWidth, fProfilerBarIndicatorHeight, 255, 0, 0, 255)
	
	iProfilerRowsDrawn++
	
ENDPROC

FUNC BOOL DOES_PROFILER_TRACK_ENTITIES(INT iProfilerIndex)
	SWITCH iProfilerIndex
		CASE ciFMMC_PROFILER_PEDS
		CASE ciFMMC_PROFILER_VEHICLES
		CASE ciFMMC_PROFILER_TRAINS
		CASE ciFMMC_PROFILER_OBJECTS
		CASE ciFMMC_PROFILER_DYNOPROPS
		CASE ciFMMC_PROFILER_INTERACTABLES
			RETURN TRUE
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC TEXT_LABEL_31 GET_TRACKED_ENTITIES_LABEL(INT iProfilerIndex)
	TEXT_LABEL_31 tl31 = "Active: "

	SWITCH iProfilerIndex
		CASE ciFMMC_PROFILER_PEDS
			tl31 += iProfilerActivePeds
		BREAK
		CASE ciFMMC_PROFILER_VEHICLES
			tl31 += iProfilerActiveVehicles
		BREAK
		CASE ciFMMC_PROFILER_TRAINS
			tl31 += iProfilerActiveTrains
		BREAK
		CASE ciFMMC_PROFILER_OBJECTS
			tl31 += iProfilerActiveObjects
		BREAK
		CASE ciFMMC_PROFILER_DYNOPROPS
			tl31 += iProfilerActiveDynoProps
		BREAK
		CASE ciFMMC_PROFILER_INTERACTABLES
			tl31 += iProfilerActiveInteractables
		BREAK
	ENDSWITCH
	
	tl31 += ", Placed: "
	
	SWITCH iProfilerIndex
		CASE ciFMMC_PROFILER_PEDS
			tl31 += MC_serverBD.iNumPedCreated
		BREAK
		CASE ciFMMC_PROFILER_VEHICLES
			tl31 += MC_serverBD.iNumVehCreated
		BREAK
		CASE ciFMMC_PROFILER_TRAINS
			tl31 += MC_serverBD.iNumTrainCreated
		BREAK
		CASE ciFMMC_PROFILER_OBJECTS
			tl31 += MC_serverBD.iNumObjCreated
		BREAK
		CASE ciFMMC_PROFILER_DYNOPROPS
			tl31 += g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps
		BREAK
		CASE ciFMMC_PROFILER_INTERACTABLES
			tl31 += g_FMMC_STRUCT_ENTITIES.iNumberOfInteractables
		BREAK
	ENDSWITCH
	
	RETURN tl31
ENDFUNC

PROC DRAW_PROFILER_STATS(VECTOR_2D &vPos, VECTOR_2D &vScale, INT iProfilerIndex)
	
	TEXT_LABEL_63 tl63 = ""
	IF sProfilerData[iProfilerIndex].iProfilerUpdateCount > 0
		IF sProfilerData[iProfilerIndex].fProfilerUpdate / 1000 < 10.0
			tl63+= "0"
		ENDIF
		tl63 += GET_STRING_FROM_FLOAT(sProfilerData[iProfilerIndex].fProfilerUpdate / 1000, 2)
		tl63 += " / "
		IF (sProfilerData[iProfilerIndex].fProfilerUpdateSum / sProfilerData[iProfilerIndex].iProfilerUpdateCount) / 1000 < 10.0
			tl63+= "0"
		ENDIF
		tl63 += GET_STRING_FROM_FLOAT((sProfilerData[iProfilerIndex].fProfilerUpdateSum / sProfilerData[iProfilerIndex].iProfilerUpdateCount) / 1000, 2)
		tl63 += " / "
		IF sProfilerData[iProfilerIndex].fProfilerUpdatePeak / 1000 < 10.0
			tl63+= "0"
		ENDIF
		tl63 += GET_STRING_FROM_FLOAT(sProfilerData[iProfilerIndex].fProfilerUpdatePeak / 1000, 2)
	ENDIF
	
	SET_TEXT_COLOUR(225, 225, 225, 255)
	SET_TEXT_FONT(FONT_STYLE_FIXED_WIDTH_NUMBERS)
	SET_TEXT_SCALE(fProfilerTextScale, fProfilerTextScale)
	DRAW_PROFILER_TEXT(vPos, vScale, tl63, FONT_RIGHT, FALSE, FALSE)
	
	SET_TEXT_SCALE(fProfilerTextTitleScale, fProfilerTextTitleScale)
	DRAW_PROFILER_TEXT(vPos, vScale, GET_PROFILER_TITLE(iProfilerIndex))
	
	IF DOES_PROFILER_TRACK_ENTITIES(iProfilerIndex)
		SET_TEXT_COLOUR(225, 225, 225, 255)
		SET_TEXT_SCALE(fProfilerTextScale, fProfilerTextScale)
		TEXT_LABEL_31 tl31 = GET_TRACKED_ENTITIES_LABEL(iProfilerIndex)
		DRAW_PROFILER_TEXT(vPos, vScale, tl31, FONT_LEFT, FALSE, FALSE)
	ENDIF
	
	DRAW_PROFILER_BAR(vPos, vScale, iProfilerIndex)
	
ENDPROC

PROC PROCESS_DRAWING_PROFILER()
	
	IF !bShowProfiler
		EXIT
	ENDIF
	
	//Init Drawing Vars
	iProfilerRowsDrawn = 0
	INT i, iNumDrawn
	FOR i = 0 TO ciFMMC_PROFILER_MAX - 1
		IF sProfilerData[i].iProfilerUpdateCount = 0
			RELOOP
		ENDIF
		iNumDrawn++
	ENDFOR
	
	VECTOR_2D vScale = GET_PROFILER_WINDOW_SCALE(iNumDrawn)
	VECTOR_2D vPos = GET_PROFILER_WINDOW_CENTRE_POS(vScale)
	
	//Background 
	DRAW_RECT(vPos.x, vPos.y, vScale.x, vScale.y, 0, 0, 0, iProfilerBGAlpha)
	
	//Title
	SET_TEXT_SCALE(fProfilerTextTitleScale, fProfilerTextTitleScale)
	DRAW_PROFILER_TEXT(vPos, vScale, "Mission Controller Performance", FONT_CENTRE)
	SET_TEXT_SCALE(fProfilerTextScale, fProfilerTextScale)
	TEXT_LABEL_63 tl63 = "Reset Interval: "
	tl63 += ciFMMC_PROFILER_RESET_INTERVAL
	tl63 += " frames"
	DRAW_PROFILER_TEXT(vPos, vScale, tl63, FONT_LEFT, FALSE)
	SET_TEXT_SCALE(fProfilerTextScale, fProfilerTextScale)
	DRAW_PROFILER_TEXT(vPos, vScale, "Frame / Average / Peak (ms)", FONT_RIGHT)
	
	//Profiling
	FOR i = 0 TO ciFMMC_PROFILER_MAX - 1
		IF sProfilerData[i].iProfilerUpdateCount = 0
			RELOOP
		ENDIF
		DRAW_PROFILER_STATS(vPos, vScale, i)
	ENDFOR
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Profiling Points
// ##### Description: Functions for starting and ending sections to be profiled
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Starts profiling a section of script
/// PARAMS:
///    iProfilerIndex - The section to be profiled
PROC START_MISSION_CONTROLLER_PROFILING(INT iProfilerIndex)
	
	IF !bMissionControllerProfilerEnabled
		EXIT
	ENDIF
	
	IF MC_playerBD[iLocalPart].iGameState != GAME_STATE_RUNNING
		//Only profile during Running!
		sProfilerData[iProfilerIndex].fProfilerUpdate = 0.0
		sProfilerData[iProfilerIndex].fProfilerCurrentFrameTime = 0.0
		EXIT
	ENDIF
	
	IF !sProfilerData[iProfilerIndex].bStarted
		sProfilerData[iProfilerIndex].fProfilerUpdate = 0.0
		sProfilerData[iProfilerIndex].bStarted = TRUE
	ENDIF
	
	sProfilerData[iProfilerIndex].fProfilerCurrentFrameTime = GET_SCRIPT_TIME_WITHIN_FRAME_IN_MICROSECONDS()
	
ENDPROC

/// PURPOSE:
///    Pauses profiling a section of script
///    START_MISSION_CONTROLLER_PROFILING must have been called first
/// PARAMS:
///    iProfilerIndex - The section to be profiled
PROC PAUSE_MISSION_CONTROLLER_PROFILING(INT iProfilerIndex)
	
	IF !bMissionControllerProfilerEnabled
		EXIT
	ENDIF

	IF MC_playerBD[iLocalPart].iGameState != GAME_STATE_RUNNING
		//Only profile during Running!
		sProfilerData[iProfilerIndex].fProfilerUpdate = 0.0
		sProfilerData[iProfilerIndex].fProfilerCurrentFrameTime = 0.0
		EXIT
	ENDIF
	
	IF !sProfilerData[iProfilerIndex].bStarted
		PRINTLN("[MC_PROFILER] PAUSE_MISSION_CONTROLLER_PROFILING - ", GET_PROFILER_TITLE(iProfilerIndex), " paused before being started!")
		EXIT
	ENDIF
	
	sProfilerData[iProfilerIndex].fProfilerUpdate += GET_SCRIPT_TIME_WITHIN_FRAME_IN_MICROSECONDS() - sProfilerData[iProfilerIndex].fProfilerCurrentFrameTime
	sProfilerData[iProfilerIndex].fProfilerCurrentFrameTime = 0.0
	
ENDPROC

/// PURPOSE:
///    Ends profiling a section of script
///    START_MISSION_CONTROLLER_PROFILING must have been called first
///    Shouldn't be called outside of PROCESS_MISSION_CONTROLLER_PROFILING
/// PARAMS:
///    iProfilerIndex - The section to be profiled
PROC END_MISSION_CONTROLLER_PROFILING(INT iProfilerIndex)
  
  	IF !bMissionControllerProfilerEnabled
		EXIT
	ENDIF
  
  	IF MC_playerBD[iLocalPart].iGameState != GAME_STATE_RUNNING
	OR sProfilerData[iProfilerIndex].fProfilerUpdate = 0.0
		sProfilerData[iProfilerIndex].fProfilerCurrentFrameTime = 0.0
		//Only profile during Running!
		EXIT
	ENDIF
	
	IF !sProfilerData[iProfilerIndex].bStarted
		EXIT
	ENDIF
	
	sProfilerData[iProfilerIndex].bStarted = FALSE
	
	IF GET_FRAME_COUNT() % ciFMMC_PROFILER_RESET_INTERVAL = 0
		IF bMissionControllerProfilerPeakPrints
			PRINTLN("[MC_PROFILER] ", GET_PROFILER_TITLE(iProfilerIndex), 
					" Peak: ", sProfilerData[iProfilerIndex].fProfilerUpdatePeak,
					" Frame: ", sProfilerData[iProfilerIndex].iPeakFrame,
					" Average: ", sProfilerData[iProfilerIndex].fProfilerUpdateSum / sProfilerData[iProfilerIndex].iProfilerUpdateCount)
		ENDIF
		sProfilerData[iProfilerIndex].fProfilerUpdateSum = 0
		sProfilerData[iProfilerIndex].iProfilerUpdateCount = 0
		sProfilerData[iProfilerIndex].fProfilerUpdatePeak = 0
	ENDIF
	
	sProfilerData[iProfilerIndex].fProfilerUpdateSum += sProfilerData[iProfilerIndex].fProfilerUpdate
	
	sProfilerData[iProfilerIndex].iProfilerUpdateCount++
	
	IF sProfilerData[iProfilerIndex].fProfilerUpdate > sProfilerData[iProfilerIndex].fProfilerUpdatePeak
		sProfilerData[iProfilerIndex].fProfilerUpdatePeak = sProfilerData[iProfilerIndex].fProfilerUpdate
		IF bMissionControllerProfilerPeakPrints
			sProfilerData[iProfilerIndex].iPeakFrame = GET_FRAME_COUNT()
		ENDIF
	ENDIF
					
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Main update
// ##### Description: The main update function for the profiler
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_MISSION_CONTROLLER_PROFILING()
	
	IF !bMissionControllerProfilerEnabled
		EXIT
	ENDIF
	
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_1, KEYBOARD_MODIFIER_CTRL_SHIFT, "Show Mission Controller Profiler")
		bShowProfiler = !bShowProfiler
	ENDIF
	
	//End all profiling for this frame
	INT i
	FOR i = 0 TO ciFMMC_PROFILER_MAX - 1
		END_MISSION_CONTROLLER_PROFILING(i)
	ENDFOR
	
	PROCESS_DRAWING_PROFILER()
	
ENDPROC

#ENDIF
