// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### System: GTA V Online Mission Controller - Utility -----------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Script: FM_Mission_Controller_2020.sc -----------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Contains various helper functions and small bits of functionality that need to be accessed from anywhere in the Mission Controller.                                                                                                                             
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### This script was forked from FM_Mission_Controller.sc at the beginning of 2020                                                                                       
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

USING "FM_Mission_Controller_Using_2020.sch"

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Game States
// ##### Description: Various helper functions to get the game/mission state
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Helper function to get the servers game/mission state
FUNC INT GET_MC_SERVER_GAME_STATE()
	RETURN MC_serverBD.iServerGameState
ENDFUNC	

/// PURPOSE:
///    Gets the local players client mission stage.
FUNC INT GET_MC_CLIENT_MISSION_STAGE(INT iPart)
	RETURN MC_playerBD[iPart].iClientLogicStage
ENDFUNC

/// PURPOSE:
///    Helper function to get a clients game state
FUNC INT GET_MC_CLIENT_GAME_STATE(INT iPart)
	RETURN MC_playerBD[iPart].iGameState
ENDFUNC

FUNC BOOL ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
	IF IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Do Participant Loop
// ##### Description: Wrappers for a generic participant loop which bails after processing all active players
// ##### See DO_PARTICIPANT_LOOP_FLAGS for ways to configure this
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC INT GET_MAXIMUM_PLAYERS_ON_MISSION()
	
	IF g_bForceMissionControllerMaxPlayerCount
		RETURN MAX_NUM_MC_PLAYERS
	ENDIF
	
	RETURN g_FMMC_STRUCT.iNumParticipants + ciMAX_TRANSITION_SESSION_SPECTATORS
	
ENDFUNC

/// PURPOSE:
///    Returns the flags required to configure DO_PARTICIPANT_LOOP to exclude the team iTeam.
FUNC DO_PARTICIPANT_LOOP_FLAGS GET_PARTICIPANT_LOOP_FLAGS_TO_EXCLUDE_SINGLE_TEAM(INT iTeam)
	
	SWITCH iTeam
		CASE 0 RETURN DPLF_IGNORE_TEAM_0
		CASE 1 RETURN DPLF_IGNORE_TEAM_1
		CASE 2 RETURN DPLF_IGNORE_TEAM_2
		CASE 3 RETURN DPLF_IGNORE_TEAM_3
	ENDSWITCH
	
	RETURN DPLF_NONE
	
ENDFUNC

/// PURPOSE:
///    Returns the flags required to configure DO_PARTICIPANT_LOOP to only include the teams set in iTeamsBitset.
FUNC DO_PARTICIPANT_LOOP_FLAGS GET_PARTICIPANT_LOOP_FLAGS_FROM_VALID_TEAM_BITSET(INT iTeamsBitset)
	
	DO_PARTICIPANT_LOOP_FLAGS eFlags = DPLF_NONE
	
	IF iTeamsBitset = ciALL_TEAMS_BITSET_VALUE
		RETURN eFlags
	ENDIF
	
	INT iTeam
	FOR iTeam = 0 TO FMMC_MAX_TEAMS -1 
		IF NOT IS_BIT_SET(iTeamsBitset, iTeam)
			eFlags = eFlags | GET_PARTICIPANT_LOOP_FLAGS_TO_EXCLUDE_SINGLE_TEAM(iTeam)
		ENDIF
	ENDFOR

	RETURN eFlags
	
ENDFUNC

/// PURPOSE:
///    Returns the flags required to configure DO_PARTICIPANT_LOOP to only include the team iTeam.
FUNC DO_PARTICIPANT_LOOP_FLAGS GET_PARTICIPANT_LOOP_FLAGS_TO_CHECK_SINGLE_TEAM(INT iTeam)
	
	INT iBitset = 0
	SET_BIT(iBitset, iTeam)
	RETURN GET_PARTICIPANT_LOOP_FLAGS_FROM_VALID_TEAM_BITSET(iBitset)
	
ENDFUNC

/// PURPOSE:
///    WARNING: Should only ever be called from  DO_PARTICIPANT_LOOP
///    Checks if a team should be included in this participant loop.
FUNC BOOL IS_TEAM_VALID_FOR_PARTICIPANT_LOOP(INT iTeam, DO_PARTICIPANT_LOOP_FLAGS eFlags)
	
	IF eFlags = DPLF_NONE
		RETURN TRUE
	ENDIF
	
	SWITCH iTeam
		CASE 0	RETURN (ENUM_TO_INT(eFlags) & ENUM_TO_INT(DPLF_IGNORE_TEAM_0)) = 0
		CASE 1	RETURN (ENUM_TO_INT(eFlags) & ENUM_TO_INT(DPLF_IGNORE_TEAM_1)) = 0
		CASE 2	RETURN (ENUM_TO_INT(eFlags) & ENUM_TO_INT(DPLF_IGNORE_TEAM_2)) = 0
		CASE 3	RETURN (ENUM_TO_INT(eFlags) & ENUM_TO_INT(DPLF_IGNORE_TEAM_3)) = 0
	ENDSWITCH
	
	RETURN TRUE
	
ENDFUNC

/// PURPOSE:
///    WARNING: Should only ever be called from  DO_PARTICIPANT_LOOP
///    Returns the number of players on the teams to be checked in the participant loop.
FUNC INT GET_NUMBER_OF_PLAYING_PLAYERS_FOR_PARTICIPANT_LOOP(DO_PARTICIPANT_LOOP_FLAGS eFlags)
	
	INT iNumberOfPlayersToCheck = 0
	
	INT iTeam
	FOR iTeam = 0 TO FMMC_MAX_TEAMS - 1
		IF IS_TEAM_VALID_FOR_PARTICIPANT_LOOP(iTeam, eFlags)
			IF (ENUM_TO_INT(eFlags) & ENUM_TO_INT(DPLF_EARLY_END_SPECTATORS)) != 0
				iNumberOfPlayersToCheck += MC_serverBD.iNumberOfPlayersAndSpectators[iTeam]
			ELSE
				iNumberOfPlayersToCheck += MC_serverBD.iNumberOfPlayingPlayers[iTeam]
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN iNumberOfPlayersToCheck
	
ENDFUNC

/// PURPOSE:
///    WARNING: Should only ever be called from  DO_PARTICIPANT_LOOP
///    Checks if a player is valid to be counted toward the players checked in a participant loop.
FUNC BOOL IS_PARTICIPANT_VALID_FOR_PARTICIPANT_LOOP_COUNT(INT iPart, DO_PARTICIPANT_LOOP_FLAGS eFlags)
	
	IF (ENUM_TO_INT(eFlags) & ENUM_TO_INT(DPLF_IGNORE_ACTIVE_CHECKS)) != 0
		RETURN TRUE
	ENDIF
		
	IF NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
		RETURN FALSE
	ENDIF
	
	IF (ENUM_TO_INT(eFlags) & ENUM_TO_INT(DPLF_EARLY_END_SPECTATORS)) = 0
		IF IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_ANY_SPECTATOR)
		AND NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_HEIST_SPECTATOR)
			RETURN FALSE
		ENDIF
		
		IF IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_PLAYER_FAIL)
			RETURN FALSE
		ENDIF
		
		IF IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_FINISHED)
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF NOT IS_TEAM_VALID_FOR_PARTICIPANT_LOOP(MC_playerBD[iPart].iTeam, eFlags)
		RETURN FALSE
	ENDIF
	
	IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPart))
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

/// PURPOSE:
///    WARNING: Should only ever be called from  DO_PARTICIPANT_LOOP
///    Checks additional valid player states based on the loop's config flags.
///    These do not affect the "Players Checked" count.
FUNC BOOL IS_PARTICIPANT_VALID_FOR_PARTICIPANT_LOOP_PROCESSING(INT iPart, DO_PARTICIPANT_LOOP_FLAGS eFlags)
	
	IF eFlags = DPLF_NONE
		RETURN TRUE
	ENDIF
	
	IF (ENUM_TO_INT(eFlags) & ENUM_TO_INT(DPLF_IGNORE_ACTIVE_CHECKS)) != 0
		RETURN TRUE
	ENDIF
	
	IF (ENUM_TO_INT(eFlags) & ENUM_TO_INT(DPLF_EXCLUDE_LOCAL_PART)) != 0
		IF iPart = iPartToUse
			RETURN FALSE
		ENDIF
	ENDIF
	
	//Flags that require Player/Part/Ped IDs
	BOOL bCheckAlive = (ENUM_TO_INT(eFlags) & ENUM_TO_INT(DPLF_CHECK_PED_ALIVE)) != 0
	BOOL bCheckOK = (ENUM_TO_INT(eFlags) & ENUM_TO_INT(DPLF_CHECK_PLAYER_OK)) != 0
	BOOL bFillIDs = (ENUM_TO_INT(eFlags) & ENUM_TO_INT(DPLF_FILL_PLAYER_IDS)) != 0
	
	IF bCheckAlive OR bCheckOK OR bFillIDs
		PARTICIPANT_INDEX piPart = INT_TO_PARTICIPANTINDEX(iPart)
		PLAYER_INDEX piPlayer = NETWORK_GET_PLAYER_INDEX(piPart)
		
		IF bCheckOK
			IF NOT IS_NET_PLAYER_OK(piPlayer)
				RETURN FALSE
			ENDIF
		ENDIF
		
		PED_INDEX piPed = GET_PLAYER_PED(piPlayer)
		
		IF bCheckAlive
			IF IS_PED_INJURED(piPed)
				RETURN FALSE
			ENDIF
		ENDIF
		
		IF bFillIDs
			//piParticipantLoop_ParticipantIndex = piPart - Unreferenced, uncomment if needed.
			piParticipantLoop_PlayerIndex = piPlayer
			piParticipantLoop_PedIndex = piPed
		ENDIF
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

/// PURPOSE:
///    WARNING: Should only ever be called from  DO_PARTICIPANT_LOOP
FUNC BOOL GET_NEXT_VALID_PARTICIPANT_FOR_LOOP(INT &iPart, DO_PARTICIPANT_LOOP_FLAGS eFlags)
	
	IF iPart >= GET_MAXIMUM_PLAYERS_ON_MISSION()
		//Invalid Part
		RETURN FALSE
	ENDIF
	
	IF iParticipantLoop_NumParticipantsChecked >= iParticipantLoop_NumParticipantsToCheck
		//Checked all active players
		RETURN FALSE
	ENDIF
	
	IF IS_PARTICIPANT_VALID_FOR_PARTICIPANT_LOOP_COUNT(iPart, eFlags)
		//Passed in part is valid for count
		iParticipantLoop_NumParticipantsChecked++
		IF IS_PARTICIPANT_VALID_FOR_PARTICIPANT_LOOP_PROCESSING(iPart, eFlags)
			//Passed in part is valid for processing
			RETURN TRUE
		ENDIF
	ENDIF
	
	//Try the next Part
	iPart++
	RETURN GET_NEXT_VALID_PARTICIPANT_FOR_LOOP(iPart, eFlags)
	
ENDFUNC

/// PURPOSE:
///    Call as a WHILE loop condition to run a participant loop.
///    Use DO_PARTICIPANT_LOOP_FLAGS to configure what teams it looks at and specify extra checks to be done on the players.
///    While this returns true iPart will always be an active participant.
///    IMPORTANT: iPart needs to be initialised to -1 to start a new loop!
/// PARAMS:
///    iPart - The participant iterator - pass in -1 to start the loop then use this as the participant index to process in the WHILE loop.
///    eFlags - Config flags - see DO_PARTICIPANT_LOOP_FLAGS.
/// RETURNS:
///    TRUE if there is a player to process, FALSE if their are no remaining valid players and the loop should end.
FUNC BOOL DO_PARTICIPANT_LOOP(INT &iPart, DO_PARTICIPANT_LOOP_FLAGS eFlags = DPLF_NONE)
	
	IF iPart = -1
		//New Loop - Init vars
		iParticipantLoop_PreviousPartChecked = -1
		iParticipantLoop_NumParticipantsChecked = 0
		IF (ENUM_TO_INT(eFlags) & ENUM_TO_INT(DPLF_IGNORE_ACTIVE_CHECKS)) = 0
			iParticipantLoop_NumParticipantsToCheck = GET_NUMBER_OF_PLAYING_PLAYERS_FOR_PARTICIPANT_LOOP(eFlags)
		ELSE
			IF eFlags != DPLF_IGNORE_ACTIVE_CHECKS
				ASSERTLN("DO_PARTICIPANT_LOOP - Called with DPLF_IGNORE_ACTIVE_CHECKS and other flags set!")
				PRINTLN("[PARTICIPANT_LOOP] DO_PARTICIPANT_LOOP - Called with DPLF_IGNORE_ACTIVE_CHECKS and other flags set!")
				DEBUG_PRINTCALLSTACK()
				eFlags = DPLF_IGNORE_ACTIVE_CHECKS
			ENDIF
			
			iParticipantLoop_NumParticipantsToCheck = GET_MAXIMUM_PLAYERS_ON_MISSION()
		ENDIF
		iPart = 0
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF iParticipantLoop_PreviousPartChecked > iPart
		ASSERTLN("DO_PARTICIPANT_LOOP - Called without intialising iPart to -1!")
		PRINTLN("[PARTICIPANT_LOOP] DO_PARTICIPANT_LOOP - Called without intialising iPart to -1!")
		DEBUG_PRINTCALLSTACK()
	ENDIF
	#ENDIF
	
	IF iPart = iParticipantLoop_PreviousPartChecked
		//Check the next participant
		iPart++
	ENDIF

	IF GET_NEXT_VALID_PARTICIPANT_FOR_LOOP(iPart, eFlags)
		iParticipantLoop_PreviousPartChecked = iPart
		RETURN TRUE
	ENDIF
	
	//No more parts to check
	RETURN FALSE
	
ENDFUNC		

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: MC Timer Helper Functions.
// ##### Description: Various helper functions to allow timers that do not need the Bool + time datatype. Useful for saving broadcast data.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(INT iTimeStamp)	
	RETURN iTimeStamp != 0
ENDFUNC

PROC MC_STOP_NET_TIMER_WITH_TIMESTAMP(INT &iTimeStamp)	
	iTimeStamp = 0
ENDPROC 

PROC MC_REINIT_NET_TIMER_WITH_TIMESTAMP(INT &iTimeStamp)	
	iTimeStamp = NATIVE_TO_INT(GET_NETWORK_TIME_ACCURATE())
ENDPROC

PROC MC_START_NET_TIMER_WITH_TIMESTAMP(INT &iTimeStamp)	
	IF iTimeStamp = 0
		iTimeStamp = NATIVE_TO_INT(GET_NETWORK_TIME_ACCURATE())
	ENDIF
ENDPROC

FUNC INT MC_GET_NET_TIMER_DIFFERENCE_WITH_TIME_STAMP(INT iTimeStamp)
	IF NOT MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(iTimeStamp)
		DEBUG_PRINTCALLSTACK()
		PRINTLN("[NET_TIMER] MC_GET_NET_TIMER_DIFFERENCE_WITH_TIME_STAMP - Called on a stopped timer!")
		ASSERTLN("[NET_TIMER] MC_GET_NET_TIMER_DIFFERENCE_WITH_TIME_STAMP - Called on a stopped timer!")
		RETURN 0
	ENDIF
	
	RETURN NATIVE_TO_INT(GET_NETWORK_TIME_ACCURATE()) - iTimeStamp
ENDFUNC

FUNC BOOL MC_HAS_NET_TIMER_EXPIRED_WITH_TIME_STAMP(INT iTimeStamp, INT iTimeDifference)	
	RETURN MC_GET_NET_TIMER_DIFFERENCE_WITH_TIME_STAMP(iTimeStamp) > iTimeDifference
ENDFUNC

FUNC BOOL MC_RUN_NET_TIMER_WITH_TIMESTAMP(INT &iTimeStamp, INT iExpiryTime, BOOL bReturnTrueWhenStarted = FALSE)
	IF NOT MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(iTimeStamp)
		PRINTLN("[Timers] Starting timer with an expiry time of: ", iExpiryTime)
		MC_REINIT_NET_TIMER_WITH_TIMESTAMP(iTimeStamp)
		IF bReturnTrueWhenStarted
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN MC_HAS_NET_TIMER_EXPIRED_WITH_TIME_STAMP(iTimeStamp, iExpiryTime)
ENDFUNC

// ##### Sub-Section: MC Pausable Timers

/// PURPOSE: Returns whether this MC_PAUSABLE_TIMER has been started (paused or running)
FUNC BOOL MC_HAS_PAUSABLE_TIMER_STARTED(MC_PAUSABLE_TIMER &sTimer)	
	RETURN sTimer.iTimeStamp != 0 OR sTimer.iOffset != 0 
ENDFUNC

/// PURPOSE: Returns whether this MC_PAUSABLE_TIMER is running
FUNC BOOL MC_IS_PAUSABLE_TIMER_RUNNING(MC_PAUSABLE_TIMER &sTimer)	
	RETURN sTimer.iTimeStamp != 0
ENDFUNC

/// PURPOSE: Resets a MC_PAUSABLE_TIMER so it can be used again
PROC MC_RESET_PAUSABLE_TIMER(MC_PAUSABLE_TIMER &sTimer)	
	sTimer.iTimeStamp = 0
	sTimer.iOffset = 0
ENDPROC 

/// PURPOSE: Pauses a MC_PAUSABLE_TIMER. Call MC_START_PAUSABLE_TIMER to Resume from where it was paused.
///    Asserts if the timer has not been started
PROC MC_PAUSE_PAUSABLE_TIMER(MC_PAUSABLE_TIMER &sTimer)	
	IF NOT MC_IS_PAUSABLE_TIMER_RUNNING(sTimer)
		DEBUG_PRINTCALLSTACK()
		PRINTLN("[PAUSABLE_TIMER] MC_PAUSE_NET_TIMER_WITH_TIMESTAMP - Called on a stopped timer!")
		ASSERTLN("[PAUSABLE_TIMER] MC_PAUSE_NET_TIMER_WITH_TIMESTAMP - Called on a stopped timer!")
		EXIT
	ENDIF
	sTimer.iOffset += NATIVE_TO_INT(GET_NETWORK_TIME_ACCURATE()) - sTimer.iTimeStamp
	sTimer.iTimeStamp = 0
ENDPROC 

/// PURPOSE: Starts or resumes a MC_PAUSABLE_TIMER. Call MC_PAUSE_PAUSABLE_TIMER to pause it.
///    Asserts if the timer is already running
PROC MC_START_PAUSABLE_TIMER(MC_PAUSABLE_TIMER &sTimer)
	IF MC_IS_PAUSABLE_TIMER_RUNNING(sTimer)
		DEBUG_PRINTCALLSTACK()
		PRINTLN("[PAUSABLE_TIMER] MC_START_PAUSABLE_TIMER - Called on a running timer!")
		ASSERTLN("[PAUSABLE_TIMER] MC_START_PAUSABLE_TIMER - Called on a running timer!")
		EXIT
	ENDIF
	sTimer.iTimeStamp = NATIVE_TO_INT(GET_NETWORK_TIME_ACCURATE())
ENDPROC

/// PURPOSE: Checks if a MC_PAUSABLE_TIMER has been running for longer than iExpiryTime
///    Asserts if the timer has not been started
FUNC INT MC_GET_PAUSABLE_TIMER_TIME_ELAPSED(MC_PAUSABLE_TIMER &sTimer)
	IF NOT MC_HAS_PAUSABLE_TIMER_STARTED(sTimer)
		DEBUG_PRINTCALLSTACK()
		PRINTLN("[PAUSABLE_TIMER] MC_GET_PAUSABLE_TIMER_TIME_ELAPSED - Called on a stopped timer!")
		ASSERTLN("[PAUSABLE_TIMER] MC_GET_PAUSABLE_TIMER_TIME_ELAPSED - Called on a stopped timer!")
		RETURN 0
	ENDIF
	RETURN MC_GET_NET_TIMER_DIFFERENCE_WITH_TIME_STAMP(sTimer.iTimeStamp) + sTimer.iOffset
ENDFUNC

/// PURPOSE: Checks if a MC_PAUSABLE_TIMER has been running for longer than iExpiryTime
///    Asserts if the timer has not been started
FUNC BOOL MC_HAS_PAUSABLE_TIMER_EXPIRED(MC_PAUSABLE_TIMER &sTimer, INT iExpiryTime)
	RETURN MC_GET_PAUSABLE_TIMER_TIME_ELAPSED(sTimer) > iExpiryTime
ENDFUNC

/// PURPOSE: Adds time (in ms) to a running MC_PAUSABLE_TIMER
///    Asserts if the timer has not been started
PROC MC_ADD_TIME_TO_PAUSABLE_TIMER(MC_PAUSABLE_TIMER &sTimer, INT iTimeToAdd)
	IF NOT MC_HAS_PAUSABLE_TIMER_STARTED(sTimer)
		DEBUG_PRINTCALLSTACK()
		PRINTLN("[PAUSABLE_TIMER] MC_ADD_TIME_TO_PAUSABLE_TIMER - Called on a stopped timer!")
		ASSERTLN("[PAUSABLE_TIMER] MC_ADD_TIME_TO_PAUSABLE_TIMER - Called on a stopped timer!")
		EXIT
	ENDIF
	sTimer.iOffset += iTimeToAdd
ENDPROC
		
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Continuity Server Data Wrappers
// ##### Description: Wrappers to return values which are stored in continuity data for the purpose of passing to another mission in a strand.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC INT GET_TEAM_DEATHS(INT iTeam)
	RETURN MC_serverBD_1.sMissionContinuityVars.iTeamDeaths[iTeam]
ENDFUNC

PROC SET_TEAM_DEATHS(INT iTeam, INT iDeaths)
	PRINTLN("SET_TEAM_DEATHS - Updating Team: ", iTeam, " Deaths from ", GET_TEAM_DEATHS(iTeam), " to ", iDeaths)
	MC_serverBD_1.sMissionContinuityVars.iTeamDeaths[iTeam] = iDeaths
ENDPROC

FUNC INT GET_COOP_TEAMS_TOTAL_DEATHS(INT iTeam)

	INT iTeamRepeat
	INT iTotalDeaths

	FOR iTeamRepeat = 0 TO FMMC_MAX_TEAMS - 1
		IF DOES_TEAM_LIKE_TEAM(iTeam, iTeamRepeat)
			iTotalDeaths += GET_TEAM_DEATHS(iTeamRepeat)
		ENDIF
	ENDFOR
	
	RETURN iTotalDeaths

ENDFUNC

FUNC INT GET_TEAM_KILLS(INT iTeam)
	RETURN MC_serverBD_1.sMissionContinuityVars.iTeamKills[iTeam]
ENDFUNC

PROC SET_TEAM_KILLS(INT iTeam, INT iKills)
	PRINTLN("SET_TEAM_KILLS - Updating Team: ", iTeam, " Kills from ", GET_TEAM_KILLS(iTeam), " to ", iKills)
	MC_serverBD_1.sMissionContinuityVars.iTeamKills[iTeam] = iKills
ENDPROC

FUNC INT GET_VARIABLE_TEAM_LIVES(INT iTeam)
	RETURN MC_serverBD_1.sMissionContinuityVars.iVariableTeamLives[iTeam]
ENDFUNC

PROC SET_VARIABLE_TEAM_LIVES(INT iTeam, INT iLives)
	PRINTLN("SET_VARIABLE_TEAM_LIVES - Updating Team: ", iTeam, " Variable Lives from ", GET_VARIABLE_TEAM_LIVES(iTeam), " to ", iLives)
	MC_serverBD_1.sMissionContinuityVars.iVariableTeamLives[iTeam] = iLives
ENDPROC

FUNC INT GET_HACK_FAILS()
	RETURN MC_serverBD_1.sMissionContinuityVars.iHackFails
ENDFUNC

PROC SET_HACK_FAILS(INT iHackFails)
	PRINTLN("SET_HACK_FAILS - Updating Hack Fails from ", GET_HACK_FAILS(), " to ", iHackFails)
	MC_serverBD_1.sMissionContinuityVars.iHackFails = iHackFails
ENDPROC

FUNC INT GET_TEAM_HEADSHOTS(INT iTeam)
	RETURN MC_serverBD_1.sMissionContinuityVars.iTeamHeadshots[iTeam]
ENDFUNC

PROC SET_TEAM_HEADSHOTS(INT iTeam, INT iHeadshots)
	PRINTLN("SET_TEAM_HEADSHOTS - Updating Team: ", iTeam, " Headshots from ", GET_TEAM_HEADSHOTS(iTeam), " to ", iHeadshots)
	MC_serverBD_1.sMissionContinuityVars.iTeamHeadshots[iTeam] = iHeadshots
ENDPROC

FUNC INT GET_TOTAL_CASH_GRAB_TAKE()
	RETURN MC_serverBD_1.sMissionContinuityVars.iGrabbedCashTotalTake
ENDFUNC

PROC SET_TOTAL_CASH_GRAB_TAKE(INT iCash)
	PRINTLN("SET_TOTAL_CASH_GRAB_TAKE - Updating Cash Grab Total Take from ", GET_TOTAL_CASH_GRAB_TAKE(), " to ", iCash)
	MC_serverBD_1.sMissionContinuityVars.iGrabbedCashTotalTake = iCash
ENDPROC

FUNC INT GET_TOTAL_CASH_GRAB_DROPPED()
	RETURN MC_serverBD_1.sMissionContinuityVars.iGrabbedCashTotalDropped
ENDFUNC

PROC SET_TOTAL_CASH_GRAB_DROPPED(INT iCash)
	PRINTLN("SET_TOTAL_CASH_GRAB_DROPPED - Updating Cash Grab Total Dropped from ", GET_TOTAL_CASH_GRAB_DROPPED(), " to ", iCash)
	MC_serverBD_1.sMissionContinuityVars.iGrabbedCashTotalDropped = iCash
ENDPROC


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Creator Lookups
// ##### Description: Mapping creator data to actual usable data (these should be refactored)
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC INT GET_MISSION_END_TIME_FROM_MENU_SELECTION(INT iSelection)

    SWITCH iSelection
		CASE ciFMMC_MISSION_END_TIME_IGNORE             RETURN -1
        CASE ciFMMC_MISSION_END_TIME_1_MINUTE           RETURN (60*1000)
        CASE ciFMMC_MISSION_END_TIME_2_MINUTES          RETURN (2*60*1000)
        CASE ciFMMC_MISSION_END_TIME_5_MINUTES          RETURN (5*60*1000)
        CASE ciFMMC_MISSION_END_TIME_10_MINUTES         RETURN (10*60*1000)
        CASE ciFMMC_MISSION_END_TIME_15_MINUTES         RETURN (15*60*1000)
        CASE ciFMMC_MISSION_END_TIME_20_MINUTES         RETURN (20*60*1000)
        CASE ciFMMC_MISSION_END_TIME_30_MINUTES         RETURN (30*60*1000)
        CASE ciFMMC_MISSION_END_TIME_1_HOUR             RETURN (60*60*1000)
        CASE ciFMMC_MISSION_END_TIME_2_HOURS            RETURN (120*60*1000)
    ENDSWITCH
	
    RETURN -1
ENDFUNC

FUNC INT GET_WANTED_CHANGE_FROM_MENU_SELECTION(INT imenuitem)

	SWITCH imenuitem
	
		CASE FMMC_WANTED_RULE_SET_0 	RETURN 0 BREAK
		CASE FMMC_WANTED_RULE_SET_1 	RETURN 1 BREAK
		CASE FMMC_WANTED_RULE_SET_2 	RETURN 2 BREAK
		CASE FMMC_WANTED_RULE_SET_3 	RETURN 3 BREAK
		CASE FMMC_WANTED_RULE_SET_4 	RETURN 4 BREAK
		CASE FMMC_WANTED_RULE_SET_5 	RETURN 5 BREAK
		CASE FMMC_WANTED_RULE_UP_1 		RETURN 1 BREAK
		CASE FMMC_WANTED_RULE_UP_2 		RETURN 2 BREAK
		CASE FMMC_WANTED_RULE_UP_3 		RETURN 3 BREAK
		CASE FMMC_WANTED_RULE_UP_4 		RETURN 4 BREAK
		CASE FMMC_WANTED_RULE_UP_5 		RETURN 5 BREAK
		CASE FMMC_WANTED_RULE_DOWN_1 	RETURN -1 BREAK
		CASE FMMC_WANTED_RULE_DOWN_2 	RETURN -2 BREAK
		CASE FMMC_WANTED_RULE_DOWN_3 	RETURN -3 BREAK
		CASE FMMC_WANTED_RULE_DOWN_4 	RETURN -4 BREAK
		
	ENDSWITCH
	
	RETURN 0

ENDFUNC

FUNC INT GET_POLICE_MAX_WANTED_FROM_MENU_SETTING(INT iwanted)

INT imaxwanted = 5

	SWITCH iwanted
		CASE ciMISSION_MAX_WANTED_1
			imaxwanted = 1
		BREAK
		CASE ciMISSION_MAX_WANTED_2
			imaxwanted = 2
		BREAK
		CASE ciMISSION_MAX_WANTED_3
			imaxwanted = 3
		BREAK
		CASE ciMISSION_MAX_WANTED_4
			imaxwanted = 4
		BREAK
		CASE ciMISSION_MAX_WANTED_5
			imaxwanted = 5
		BREAK
	ENDSWITCH
	
	RETURN imaxwanted
	
ENDFUNC

FUNC INT GET_CAPTURE_TIME_FOR_CURRENT_RULE(INT iTeam, INT iPriority)
    RETURN g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTakeoverTime[iPriority]
ENDFUNC

FUNC STRING GET_VEHICLE_CAPTURE_HELP_TEXT(INT iVeh)
	
	IF IS_CUSTOM_STRING_LIST_STRING_VALID(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCapturePromptCustomString)
		RETURN GET_CUSTOM_STRING_LIST_STRING(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCapturePromptCustomString)
	ENDIF
	
	RETURN "CAP_VEH"
	
ENDFUNC

FUNC INT GET_ENTITY_STARTING_RESPAWN_LIVES(INT iRespawnLivesSetting)
	
	SWITCH iRespawnLivesSetting
		CASE NUMBER_OF_RESPAWNS_VEHICLE_POOL
			RETURN MC_serverBD.iVehicleRespawnPool
		BREAK
		
		CASE NUMBER_OF_RESPAWNS_PLAYER_NUM
			RETURN MC_serverBD.iVariableEntityRespawns
		BREAK
		
		CASE NUMBER_OF_RESPAWNS_UNLIMITED
			RETURN UNLIMITED_RESPAWNS
		BREAK
	ENDSWITCH

    RETURN iRespawnLivesSetting
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Custom Variables
// ##### Description: Functions related to the Custom Variables system that allow options to dynamically change their value based on gameplay data.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL COMPARE_CUSTOM_VARIABLE_VALUES(INT iComparisonType, INT iComparisonValue, INT iDifficultyLevel, INT iDataToCompare)
	
	// Instantly return false if we fail the difficulty check.
	SWITCH iDifficultyLevel
		CASE ciCUSTOM_VARIABLE_DIFFICULTY_LEVEL__ANY 
			// Allow
		BREAK
		CASE ciCUSTOM_VARIABLE_DIFFICULTY_LEVEL__EASY 
			IF g_FMMC_STRUCT.iDifficulity != DIFF_EASY
				RETURN FALSE
			ENDIF
		BREAK
		CASE ciCUSTOM_VARIABLE_DIFFICULTY_LEVEL__EASY_OR_MEDIUM 
			IF g_FMMC_STRUCT.iDifficulity != DIFF_EASY
			AND g_FMMC_STRUCT.iDifficulity != DIFF_NORMAL
				RETURN FALSE
			ENDIF
		BREAK
		CASE ciCUSTOM_VARIABLE_DIFFICULTY_LEVEL__MEDIUM 
			IF g_FMMC_STRUCT.iDifficulity != DIFF_NORMAL			
				RETURN FALSE
			ENDIF
		BREAK
		CASE ciCUSTOM_VARIABLE_DIFFICULTY_LEVEL__MEDIUM_OR_HARD 
			IF g_FMMC_STRUCT.iDifficulity != DIFF_NORMAL
			AND g_FMMC_STRUCT.iDifficulity != DIFF_HARD
				RETURN FALSE
			ENDIF
		BREAK
		CASE ciCUSTOM_VARIABLE_DIFFICULTY_LEVEL__HARD 
			IF g_FMMC_STRUCT.iDifficulity != DIFF_HARD			
				RETURN FALSE
			ENDIF
		BREAK
		CASE ciCUSTOM_VARIABLE_DIFFICULTY_LEVEL__HARD_OR_EASY 
			IF g_FMMC_STRUCT.iDifficulity != DIFF_HARD
			AND g_FMMC_STRUCT.iDifficulity != DIFF_EASY
				RETURN FALSE
			ENDIF
		BREAK
	ENDSWITCH
	
	SWITCH iComparisonType
		CASE ciCUSTOM_VARIABLE_COMPARISON_TYPE__EQUAL_TO
			RETURN iDataToCompare = iComparisonValue
		BREAK
		CASE ciCUSTOM_VARIABLE_COMPARISON_TYPE__GREATER_THAN
			RETURN iDataToCompare > iComparisonValue
		BREAK
		CASE ciCUSTOM_VARIABLE_COMPARISON_TYPE__LESS_THAN
			RETURN iDataToCompare < iComparisonValue
		BREAK
	ENDSWITCH
	
	RETURN FALSE	
ENDFUNC

FUNC BOOL COMPARE_CUSTOM_VARIABLE_VALUES_IN_PRESET_AND_RETURN_INDEX_TO_USE(INT &iIndexToUse, MC_CUSTOM_VARIABLE_DATA &sData[], INT iDataToCompare)
	INT i
	FOR i = 0 TO COUNT_OF(sData)-1		
		IF COMPARE_CUSTOM_VARIABLE_VALUES(sData[i].iComparisonType, sData[i].iComparisonValue, sData[i].iDifficultyLevel, iDataToCompare)
			iIndexToUse = i
			RETURN TRUE
		ENDIF	
	ENDFOR	
	RETURN FALSE
ENDFUNC

PROC MODIFY_VARIABLE_WITH_CUSTOM_VARIABLE_VALUE(INT &iValue, INT iEntityType = CREATION_TYPE_NONE, INT iEntityIndex = -1)
	
	INT iPresetIndex = GET_CUSTOM_VARIABLE_LIST_PRESET_INDEX_FROM_CREATOR_OPTION_VALUE(iValue)
	INT iType = GET_CUSTOM_VARIABLE_LIST_TYPE_FROM_CREATOR_OPTION_VALUE(iValue)
	
	// Not always necessary but some types require us to check the data on an entity, for example CUSTOM_VARIABLE_LIST_TYPE__ENTITY_RESPAWNED_COUNT.
	IF DOES_CUSTOM_VARIABLE_LIST_TYPE_REQUIRE_ENTITY_INDEX_OR_TYPE(iType)
		IF iEntityIndex = -1
		OR iEntityType = CREATION_TYPE_NONE			
			PRINTLN("[CustomVar][", GET_CUSTOM_VARIABLE_LIST_TYPE_NAME(iType),"] - MODIFY_VARIABLE_WITH_CUSTOM_VARIABLE_VALUE = iPresetIndex: ", iPresetIndex)				
			#IF IS_DEBUG_BUILD
			ASSERTLN("MODIFY_VARIABLE_WITH_CUSTOM_VARIABLE_VALUE | Custom Vars Type: ", iType, " is being used without an Entity ID or Type!!")
			ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_INVALID_CUSTOM_VARS_SETUP__REQUIRES_ENTITY, ENTITY_RUNTIME_ERROR_TYPE_CRITICAL, "Custom Vars Type # is being used without Entity ID/Type!", iType)
			#ENDIF
		ENDIF
	ENDIF
	
	IF iPresetIndex <= -1
	OR iPresetIndex >= GET_CUSTOM_VARIABLE_LIST_TYPE_PRESET_MAX_LENGTH(iType)
		EXIT
	ENDIF

	PRINTLN("[CustomVar][", GET_CUSTOM_VARIABLE_LIST_TYPE_NAME(iType),"] - MODIFY_VARIABLE_WITH_CUSTOM_VARIABLE_VALUE = iPresetIndex: ", iPresetIndex)	
	
	INT iTypeComparisonValue, iDataIndex
	SWITCH iType
	
		// Player Numbers ##############
		CASE CUSTOM_VARIABLE_LIST_TYPE__PLAYER_NUMBERS			
			iTypeComparisonValue = MC_ServerBD.iNumStartingPlayers[0] + MC_ServerBD.iNumStartingPlayers[1] + MC_ServerBD.iNumStartingPlayers[2] + MC_ServerBD.iNumStartingPlayers[3]						
			IF iTypeComparisonValue <= 0
				iTypeComparisonValue = 1
			ENDIF
			
			IF COMPARE_CUSTOM_VARIABLE_VALUES_IN_PRESET_AND_RETURN_INDEX_TO_USE(iDataIndex, g_FMMC_STRUCT.sCustomVariables.sPlayerNumbers[iPresetIndex].sData, iTypeComparisonValue)
				iValue = g_FMMC_STRUCT.sCustomVariables.sPlayerNumbers[iPresetIndex].sData[iDataIndex].iReturnValue				
				PRINTLN("[CustomVar][", GET_CUSTOM_VARIABLE_LIST_TYPE_NAME(iType),"] - MODIFY_VARIABLE_WITH_CUSTOM_VARIABLE_VALUE = iDataIndex: ", iDataIndex, " iValue: ", iValue, " iComparisonValue: ", g_FMMC_STRUCT.sCustomVariables.sPlayerNumbers[iPresetIndex].sData[iDataIndex].iComparisonValue, " ComparisonType: ", GET_CUSTOM_VARIABLE_LIST_COMPARISON_TYPE(g_FMMC_STRUCT.sCustomVariables.sPlayerNumbers[iPresetIndex].sData[iDataIndex].iComparisonType), " Difficulty: ", GET_CUSTOM_VARIABLE_LIST_DIFFICULTY_NAME(g_FMMC_STRUCT.sCustomVariables.sPlayerNumbers[iPresetIndex].sData[iDataIndex].iDifficultyLevel))
			ELSE
				PRINTLN("[CustomVar][", GET_CUSTOM_VARIABLE_LIST_TYPE_NAME(iType),"] - MODIFY_VARIABLE_WITH_CUSTOM_VARIABLE_VALUE = No valid custom var to assign yet.")
			ENDIF
					
		BREAK
		
		// Entity Respawn Count ##############
		CASE CUSTOM_VARIABLE_LIST_TYPE__ENTITY_RESPAWNED_COUNT						
			INT iStartingRespawns
			INT iCurrentRespawns
			
			// Note: If we want unlimited respawns to work (UNLIMITED_RESPAWNS) then we need to track actual deaths in the ServerBD.
			SWITCH iEntityType
				CASE CREATION_TYPE_PEDS					
					iStartingRespawns = GET_ENTITY_STARTING_RESPAWN_LIVES(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iEntityIndex].iPedRespawnLives)
					iCurrentRespawns = PICK_INT(MC_serverBD.iServerGameState = GAME_STATE_RUNNING, MC_serverBD_2.iCurrentPedRespawnLives[iEntityIndex], iStartingRespawns)
					IF NOT FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedFirstSpawnBitset, iEntityIndex)
						iCurrentRespawns--
					ENDIF
				BREAK
				
				CASE CREATION_TYPE_VEHICLES					
					iStartingRespawns = GET_ENTITY_STARTING_RESPAWN_LIVES(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iEntityIndex].iVehicleRespawnLives)					
					iCurrentRespawns = PICK_INT(MC_serverBD.iServerGameState = GAME_STATE_RUNNING, MC_serverBD_2.iCurrentVehRespawnLives[iEntityIndex], iStartingRespawns) 
					IF IS_BIT_SET(MC_serverBD_2.iVehSpawnedOnce, iEntityIndex)
						iCurrentRespawns--	
					ENDIF
				BREAK
				
				CASE CREATION_TYPE_OBJECTS					
					iStartingRespawns = GET_ENTITY_STARTING_RESPAWN_LIVES(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iEntityIndex].iObjectRespawnLives)					
					iCurrentRespawns = PICK_INT(MC_serverBD.iServerGameState = GAME_STATE_RUNNING, MC_serverBD_2.iCurrentObjRespawnLives[iEntityIndex], iStartingRespawns) 
					IF IS_BIT_SET(MC_serverBD_2.iObjSpawnedOnce, iEntityIndex)
						iCurrentRespawns--	
					ENDIF
				BREAK
				
				DEFAULT
					#IF IS_DEBUG_BUILD
					ASSERTLN("MODIFY_VARIABLE_WITH_CUSTOM_VARIABLE_VALUE | Custom Vars Type: ", iType, " is not yet supported for this entity type!!")
					ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_INVALID_CUSTOM_VARS_SETUP__NOT_SUPPORTED, ENTITY_RUNTIME_ERROR_TYPE_CRITICAL, "Custom Vars Type # is being used without Entity ID/Type!", iType)
					#ENDIF
				BREAK
			ENDSWITCH
			
			iTypeComparisonValue = (iStartingRespawns - iCurrentRespawns)
			
			IF COMPARE_CUSTOM_VARIABLE_VALUES_IN_PRESET_AND_RETURN_INDEX_TO_USE(iDataIndex, g_FMMC_STRUCT.sCustomVariables.sEntityRespawnCount[iPresetIndex].sData, iTypeComparisonValue)
				iValue = g_FMMC_STRUCT.sCustomVariables.sEntityRespawnCount[iPresetIndex].sData[iDataIndex].iReturnValue				
				PRINTLN("[CustomVar][", GET_CUSTOM_VARIABLE_LIST_TYPE_NAME(iType),"] - MODIFY_VARIABLE_WITH_CUSTOM_VARIABLE_VALUE = iDataIndex: ", iDataIndex, " iValue: ", iValue, " iComparisonValue: ", g_FMMC_STRUCT.sCustomVariables.sEntityRespawnCount[iPresetIndex].sData[iDataIndex].iComparisonValue, " ComparisonType: ", GET_CUSTOM_VARIABLE_LIST_COMPARISON_TYPE(g_FMMC_STRUCT.sCustomVariables.sEntityRespawnCount[iPresetIndex].sData[iDataIndex].iComparisonType), " Difficulty: ", GET_CUSTOM_VARIABLE_LIST_DIFFICULTY_NAME(g_FMMC_STRUCT.sCustomVariables.sEntityRespawnCount[iPresetIndex].sData[iDataIndex].iDifficultyLevel))
			ELSE
				PRINTLN("[CustomVar][", GET_CUSTOM_VARIABLE_LIST_TYPE_NAME(iType),"] - MODIFY_VARIABLE_WITH_CUSTOM_VARIABLE_VALUE = No valid custom var to assign yet.")
			ENDIF
			
		BREAK
		
		// Rule Time Elapsed ##############
		CASE CUSTOM_VARIABLE_LIST_TYPE__RULE_TIME_ELAPSED
			INT iTeam
			FOR iTeam = 0 TO MC_serverBD.iNumberOfTeams - 1				
				IF NOT HAS_NET_TIMER_STARTED(MC_serverBD_3.tdObjectiveLimitTimer[iTeam])
					RELOOP
				ENDIF
				
				iTypeComparisonValue = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdObjectiveLimitTimer[iTeam])
				
				// The first valid custom var is used.
				IF COMPARE_CUSTOM_VARIABLE_VALUES_IN_PRESET_AND_RETURN_INDEX_TO_USE(iDataIndex, g_FMMC_STRUCT.sCustomVariables.sRuleTimeElapsed[iPresetIndex].sData, iTypeComparisonValue)
					iValue = g_FMMC_STRUCT.sCustomVariables.sRuleTimeElapsed[iPresetIndex].sData[iDataIndex].iReturnValue				
					PRINTLN("[CustomVar][", GET_CUSTOM_VARIABLE_LIST_TYPE_NAME(iType),"] - MODIFY_VARIABLE_WITH_CUSTOM_VARIABLE_VALUE = iDataIndex: ", iDataIndex, " iValue: ", iValue, " iComparisonValue: ", g_FMMC_STRUCT.sCustomVariables.sRuleTimeElapsed[iPresetIndex].sData[iDataIndex].iComparisonValue, " ComparisonType: ", GET_CUSTOM_VARIABLE_LIST_COMPARISON_TYPE(g_FMMC_STRUCT.sCustomVariables.sRuleTimeElapsed[iPresetIndex].sData[iDataIndex].iComparisonType), " Difficulty: ", GET_CUSTOM_VARIABLE_LIST_DIFFICULTY_NAME(g_FMMC_STRUCT.sCustomVariables.sRuleTimeElapsed[iPresetIndex].sData[iDataIndex].iDifficultyLevel))
					BREAKLOOP
				ELSE
					PRINTLN("[CustomVar][", GET_CUSTOM_VARIABLE_LIST_TYPE_NAME(iType),"] - MODIFY_VARIABLE_WITH_CUSTOM_VARIABLE_VALUE = No valid custom var to assign yet.")
				ENDIF			
			ENDFOR			
		BREAK
		
		// Difficulty Level ##############
		CASE CUSTOM_VARIABLE_LIST_TYPE__DIFFICULTY_LEVEL
			// The first valid custom var is used.
			IF COMPARE_CUSTOM_VARIABLE_VALUES_IN_PRESET_AND_RETURN_INDEX_TO_USE(iDataIndex, g_FMMC_STRUCT.sCustomVariables.sDifficultyLevel[iPresetIndex].sData, 0)  // We don't care about the comparison value here, so pass in 0, the comparison type and val should be equal to and 0 (forced).
				iValue = g_FMMC_STRUCT.sCustomVariables.sDifficultyLevel[iPresetIndex].sData[iDataIndex].iReturnValue				
				PRINTLN("[CustomVar][", GET_CUSTOM_VARIABLE_LIST_TYPE_NAME(iType),"] - MODIFY_VARIABLE_WITH_CUSTOM_VARIABLE_VALUE = iDataIndex: ", iDataIndex, " iValue: ", iValue, " iComparisonValue: ", g_FMMC_STRUCT.sCustomVariables.sDifficultyLevel[iPresetIndex].sData[iDataIndex].iComparisonValue, " ComparisonType: ", GET_CUSTOM_VARIABLE_LIST_COMPARISON_TYPE(g_FMMC_STRUCT.sCustomVariables.sDifficultyLevel[iPresetIndex].sData[iDataIndex].iComparisonType), " Difficulty: ", GET_CUSTOM_VARIABLE_LIST_DIFFICULTY_NAME(g_FMMC_STRUCT.sCustomVariables.sDifficultyLevel[iPresetIndex].sData[iDataIndex].iDifficultyLevel))
			ELSE
				PRINTLN("[CustomVar][", GET_CUSTOM_VARIABLE_LIST_TYPE_NAME(iType),"] - MODIFY_VARIABLE_WITH_CUSTOM_VARIABLE_VALUE = No valid custom var to assign yet.")
			ENDIF		
		BREAK
	ENDSWITCH
	
ENDPROC

// For values saved as an int (*10) so that the int can represent decimal values to the first decimal place. Divides the saved value by 10 before returning it. Variables would be saved as 25, to be returned as 2.5.
FUNC FLOAT RETURN_FLOAT_CUSTOM_VARIABLE_VALUE_DIV_10(FLOAT fValue, INT iEntityType = CREATION_TYPE_NONE, INT iEntityIndex = -1)
	INT iValue = ROUND(fValue)
	MODIFY_VARIABLE_WITH_CUSTOM_VARIABLE_VALUE(iValue, iEntityType, iEntityIndex)		
	iValue /= 10	
	RETURN fValue
ENDFUNC

FUNC SPAWN_GROUP_DEFINE_ACTIVATION RETURN_ENUM_SPAWN_GROUP_DEFINE_ACTIVATION_CUSTOM_VARIABLE_VALUE(SPAWN_GROUP_DEFINE_ACTIVATION eValue, INT iEntityType = CREATION_TYPE_NONE, INT iEntityIndex = -1)
	INT iValue = ENUM_TO_INT(eValue)
	MODIFY_VARIABLE_WITH_CUSTOM_VARIABLE_VALUE(iValue, iEntityType, iEntityIndex)
	RETURN INT_TO_ENUM(SPAWN_GROUP_DEFINE_ACTIVATION, iValue)
ENDFUNC

FUNC INT RETURN_INT_CUSTOM_VARIABLE_VALUE(INT iValue, INT iEntityType = CREATION_TYPE_NONE, INT iEntityIndex = -1)
	MODIFY_VARIABLE_WITH_CUSTOM_VARIABLE_VALUE(iValue, iEntityType, iEntityIndex)	
	RETURN iValue
ENDFUNC

// ----------------------------------
// Pool Helper functions:
// ----------------------------------

FUNC WEAPON_TYPE RETURN_WEAPON_TYPE_FROM_POOL_USING_CUSTOM_VARIABLE_LIST_VALUE(INT iValue, INT iEntityType = CREATION_TYPE_NONE, INT iEntityIndex = -1)
	MODIFY_VARIABLE_WITH_CUSTOM_VARIABLE_VALUE(iValue, iEntityType, iEntityIndex)	
		
	IF iValue <= -1
		#IF IS_DEBUG_BUILD
		ASSERTLN("RETURN_WEAPON_TYPE_FROM_POOL_USING_CUSTOM_VARIABLE_LIST_VALUE | Value returned from Custom Vars was out of range (<= -1)")
		ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_INVALID_CUSTOM_VARS_SETUP__WEAPON_POOL, ENTITY_RUNTIME_ERROR_TYPE_CRITICAL, "Value returned from Custom Vars was out of range (<= -1)")
		#ENDIF
		RETURN WEAPONTYPE_UNARMED
	ENDIF
	
	IF iValue >= ciMAX_CUSTOM_VARIABLE_POOL__WEAPON_MAX
		#IF IS_DEBUG_BUILD
		ASSERTLN("RETURN_WEAPON_TYPE_FROM_POOL_USING_CUSTOM_VARIABLE_LIST_VALUE | Value returned from Custom Vars was out of range (>= ciMAX_CUSTOM_VARIABLE_POOL__WEAPON_MAX)")
		ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_INVALID_CUSTOM_VARS_SETUP__WEAPON_POOL, ENTITY_RUNTIME_ERROR_TYPE_CRITICAL, "Value returned from Custom Vars was out of range (>= ciMAX_CUSTOM_VARIABLE_POOL__WEAPON_MAX)")
		#ENDIF
		RETURN WEAPONTYPE_UNARMED
	ENDIF
			
	RETURN g_FMMC_STRUCT.sCustomVariables.sWeaponPool[iValue].wt_Weapon
ENDFUNC

// ----------------------------------
// Specific Entity or System functions:
// ----------------------------------
FUNC WEAPON_TYPE GET_PED_WEAPON_TYPE_FROM_CREATOR_OPTION(INT iPed)	
	IF NOT IS_OPTION_SET_UP_IN_CUSTOM_VARIABLE_MODE(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iWeapon)
		RETURN g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].gun
	ENDIF	
	RETURN RETURN_WEAPON_TYPE_FROM_POOL_USING_CUSTOM_VARIABLE_LIST_VALUE(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iWeapon, CREATION_TYPE_PEDS, iPed)
ENDFUNC
FUNC INT GET_PED_HEALTH_FROM_CREATOR_OPTION(INT iPed)	
	IF NOT IS_OPTION_SET_UP_IN_CUSTOM_VARIABLE_MODE(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iHealth)
		RETURN GET_PED_HEALTH_FROM_MENU(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iHealth)
	ENDIF	
	RETURN RETURN_INT_CUSTOM_VARIABLE_VALUE(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iHealth, CREATION_TYPE_PEDS, iPed)+100
ENDFUNC	
FUNC INT GET_PED_ACCURACY_FROM_CREATOR_OPTION(INT iPed)	
	IF NOT IS_OPTION_SET_UP_IN_CUSTOM_VARIABLE_MODE(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iAccuracy)
		RETURN g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iAccuracy
	ENDIF	
	RETURN RETURN_INT_CUSTOM_VARIABLE_VALUE(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iAccuracy, CREATION_TYPE_PEDS, iPed)
ENDFUNC
	
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Data Structure Helpers
// ##### Description: Helpers and wrappers for common operations on various data structures
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Networked Objects
// ##### Description: Various helper functions & wrappers to do with the shared pool of networked objects (Objective objects, Dynoprops and Interactables)
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC INT GET_OBJECT_NET_ID_INDEX(INT iObj)
	RETURN iObj
ENDFUNC

FUNC INT GET_DYNOPROP_NET_ID_INDEX(INT iDynoprop)
	RETURN g_FMMC_STRUCT_ENTITIES.iNumberOfObjects + iDynoprop
ENDFUNC

FUNC INT GET_INTERACTABLE_NET_ID_INDEX(INT iInteractable)
	RETURN g_FMMC_STRUCT_ENTITIES.iNumberOfObjects + g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps + iInteractable
ENDFUNC

FUNC NETWORK_INDEX GET_OBJECT_NET_ID(INT iObj)
	RETURN MC_serverBD_1.sFMMC_SBD.niNetworkedObject[GET_OBJECT_NET_ID_INDEX(iObj)]
ENDFUNC

FUNC NETWORK_INDEX GET_DYNOPROP_NET_ID(INT iDynoprop)
	RETURN MC_serverBD_1.sFMMC_SBD.niNetworkedObject[GET_DYNOPROP_NET_ID_INDEX(iDynoprop)]
ENDFUNC

FUNC NETWORK_INDEX GET_INTERACTABLE_NET_ID(INT iInteractable)
	RETURN MC_serverBD_1.sFMMC_SBD.niNetworkedObject[GET_INTERACTABLE_NET_ID_INDEX(iInteractable)]
ENDFUNC

FUNC INT GET_NUMBER_OF_SPAWNED_NETWORKED_OBJECTS(BOOL bUseNative = TRUE)

	IF bUseNative
		RETURN GET_NUM_CREATED_MISSION_OBJECTS(TRUE)
	ENDIF
	
	INT iTotal = 0
	INT i
	
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfObjects - 1
		IF NETWORK_DOES_NETWORK_ID_EXIST(GET_OBJECT_NET_ID(i))
			iTotal++
		ENDIF
	ENDFOR
	
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps - 1
		IF NETWORK_DOES_NETWORK_ID_EXIST(GET_DYNOPROP_NET_ID(i))
			iTotal++
		ENDIF
	ENDFOR
	
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfInteractables - 1
		IF NETWORK_DOES_NETWORK_ID_EXIST(GET_INTERACTABLE_NET_ID(i))
			iTotal++
		ENDIF
	ENDFOR
	
	PRINTLN("GET_NUMBER_OF_SPAWNED_NETWORKED_OBJECTS | Total is ", iTotal)
	RETURN iTotal
ENDFUNC

FUNC BOOL CAN_WE_SPAWN_A_NETWORKED_OBJECT()
	RETURN GET_NUMBER_OF_SPAWNED_NETWORKED_OBJECTS() < FMMC_MAX_NUM_SPAWNED_NETWORKED_OBJS
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Coord Visibility
// ##### Description: 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL IS_COORD_VISIBLE_TO_PLAYER(PLAYER_INDEX piPlayer, VECTOR vPos)	
	RETURN IS_SPHERE_VISIBLE_TO_PLAYER(piPlayer, vPos, 0.5)
ENDFUNC

FUNC BOOL IS_COORD_VISIBLE_TO_ANY_PLAYER(VECTOR vPos)
	
	DO_PARTICIPANT_LOOP_FLAGS eFlags = DPLF_CHECK_PED_ALIVE | DPLF_CHECK_PLAYER_OK
	INT iPart = -1
	WHILE DO_PARTICIPANT_LOOP(iPart, eFlags)
		IF IS_COORD_VISIBLE_TO_PLAYER(piParticipantLoop_PlayerIndex, vPos)
			PRINTLN("IS_COORD_VISIBLE_TO_ANY_PLAYER - iPart: ", iPart, " can see coord: ", vPos, " RETURNING TRUE")
			RETURN TRUE
		ENDIF
	ENDWHILE
	
	RETURN FALSE
	
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Entity Getters
// ##### Description: Helpers and wrappers for easily grabbing entity references from data
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC ENTITY_INDEX GET_FMMC_ENTITY(INT iEntityType = ciENTITY_TYPE_NONE, INT iEntityIndex = -1, INT iTrainCarriage = -1)
	
	NETWORK_INDEX niEntity
	
	IF iEntityIndex = -1
		ASSERTLN("GET_FMMC_ENTITY | Trying to get entity ", iEntityIndex, " of type ", iEntityType)
		RETURN NULL
	ENDIF
	
	SWITCH iEntityType
		CASE ciENTITY_TYPE_NONE
			RETURN NULL
		BREAK
		
		CASE ciENTITY_TYPE_PED
			IF iEntityIndex < FMMC_MAX_PEDS
				niEntity = MC_serverBD_1.sFMMC_SBD.niPed[iEntityIndex]
			ENDIF
		BREAK
		
		CASE ciENTITY_TYPE_VEHICLE
			IF iEntityIndex < FMMC_MAX_VEHICLES
				niEntity = MC_serverBD_1.sFMMC_SBD.niVehicle[iEntityIndex]
			ENDIF
		BREAK
		
		CASE ciENTITY_TYPE_OBJECT
			IF iEntityIndex < FMMC_MAX_NUM_OBJECTS
				niEntity = GET_OBJECT_NET_ID(iEntityIndex)
			ENDIF
		BREAK
		
		CASE ciENTITY_TYPE_PROPS
			IF iEntityIndex < GET_FMMC_MAX_NUM_PROPS()
				ENTITY_INDEX tempProp
				tempProp = oiProps[iEntityIndex]
				RETURN tempProp
			ENDIF
		BREAK
		
		CASE ciENTITY_TYPE_KILL_PLAYER // TEAM
		
			IF iEntityIndex = -1
			OR iEntityIndex >= FMMC_MAX_TEAMS			
				PRINTLN("GET_FMMC_ENTITY - Checking Team but it's out of range: ", iEntityIndex)			
				RETURN NULL
			ENDIF
			
			INT iClosestPlayer
			PLAYER_INDEX tempPlayer
			PED_INDEX tempPed
			
			iClosestPlayer = GET_NEAREST_PLAYER_TO_ENTITY_ON_TEAM(localPlayerPed, iEntityIndex)
			tempPlayer = INT_TO_PLAYERINDEX(iClosestPlayer)
			
			PRINTLN("GET_FMMC_ENTITY - Checking Team: ", iEntityIndex)
			
			IF (iClosestPlayer != -1 AND iClosestPlayer != iLocalPart)
			AND IS_NET_PLAYER_OK(tempPlayer)
				tempPed = GET_PLAYER_PED(tempPlayer)
				RETURN GET_ENTITY_FROM_PED_OR_VEHICLE(tempPed)
			ENDIF
		
			PRINTLN("GET_FMMC_ENTITY - Closest Player is not OK, or not valid, going to manual method of finding closest player")
			INT iClosestPart
			iClosestPart = -1
			FLOAT fClosestDist
			fClosestDist = 999999999
			FLOAT fDistance
			
			DO_PARTICIPANT_LOOP_FLAGS eFlags
			eFlags = DPLF_FILL_PLAYER_IDS
			eFlags = eFlags | GET_PARTICIPANT_LOOP_FLAGS_TO_CHECK_SINGLE_TEAM(iEntityIndex)				
			INT iPart
			iPart = -1
			WHILE DO_PARTICIPANT_LOOP(iPart, DPLF_FILL_PLAYER_IDS)
			
				IF NOT DOES_ENTITY_EXIST(piParticipantLoop_PedIndex)
					RELOOP
				ENDIF
				
				PRINTLN("GET_FMMC_ENTITY - Checking part ", iPart)
				
				fDistance = VDIST2(GET_PLAYER_COORDS(piParticipantLoop_PlayerIndex), GET_PLAYER_COORDS(localPlayer))
				PRINTLN("GET_FMMC_ENTITY- this participant is fine, fDistance2: ", fDistance)
				IF fDistance < fClosestDist
					fClosestDist = fDistance
					iClosestPart = iPart
					PRINTLN("GET_FMMC_ENTITY - Updating: ", fClosestDist, " iClosestPart: ", iClosestPart)
				ENDIF
				
			ENDWHILE
			
			IF iClosestPart != -1
				tempPed = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iClosestPart)))
				RETURN GET_ENTITY_FROM_PED_OR_VEHICLE(tempPed)
			ENDIF
		BREAK
		
		CASE ciENTITY_TYPE_GOTO
			RETURN NULL
		BREAK
		
		CASE ciENTITY_TYPE_DYNO_PROPS
			IF iEntityIndex < FMMC_MAX_NUM_DYNOPROPS
				niEntity = GET_DYNOPROP_NET_ID(iEntityIndex)
			ENDIF
		BREAK
		
		CASE ciENTITY_TYPE_INTERACTABLES
			IF iEntityIndex < FMMC_MAX_NUM_INTERACTABLES
				niEntity = GET_INTERACTABLE_NET_ID(iEntityIndex)
			ENDIF
		BREAK
		
		CASE ciENTITY_TYPE_TRAIN
			IF iEntityIndex < FMMC_MAX_TRAINS
				niEntity = MC_serverBD_1.sFMMC_SBD.niTrain[iEntityIndex]
				
				IF iTrainCarriage > -1
					VEHICLE_INDEX viTrain
					viTrain = NET_TO_VEH(niEntity)
					
					IF IS_ENTITY_ALIVE(viTrain)
						VEHICLE_INDEX viCarriage
						viCarriage = GET_TRAIN_CARRIAGE(viTrain, iTrainCarriage)
						
						IF DOES_ENTITY_EXIST(viCarriage)
							ENTITY_INDEX eiCarriage
							eiCarriage = viCarriage
							RETURN eiCarriage
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		DEFAULT
			ASSERTLN("GET_FMMC_ENTITY | iEntityType: ", iEntityType, " isn't set up in this function!")
			PRINTLN("GET_FMMC_ENTITY | iEntityType: ", iEntityType, " isn't set up in this function!")
		BREAK
	ENDSWITCH
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(niEntity)
		RETURN NET_TO_ENT(niEntity)
	ELSE
		PRINTLN("GET_FMMC_ENTITY | No network index exists! iEntityType: ", iEntityType, " / iEntityIndex: ", iEntityIndex)
	ENDIF
	
	RETURN NULL
ENDFUNC

FUNC PED_INDEX GET_FMMC_ENTITY_PED(INT iPedIndex)
	ENTITY_INDEX eiPed = GET_FMMC_ENTITY(ciENTITY_TYPE_PED, iPedIndex)
	
	IF DOES_ENTITY_EXIST(eiPed)
		RETURN GET_PED_INDEX_FROM_ENTITY_INDEX(eiPed)
	ENDIF
	
	RETURN NULL
ENDFUNC

FUNC VEHICLE_INDEX GET_FMMC_ENTITY_VEHICLE(INT iVehicleIndex)
	ENTITY_INDEX eiVeh = GET_FMMC_ENTITY(ciENTITY_TYPE_VEHICLE, iVehicleIndex)
	
	IF DOES_ENTITY_EXIST(eiVeh)
		RETURN GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(eiVeh)
	ENDIF
	
	RETURN NULL
ENDFUNC

FUNC OBJECT_INDEX GET_FMMC_ENTITY_OBJECT(INT iObjectIndex)
	ENTITY_INDEX eiObj = GET_FMMC_ENTITY(ciENTITY_TYPE_OBJECT, iObjectIndex)
	
	IF DOES_ENTITY_EXIST(eiObj)
		RETURN GET_OBJECT_INDEX_FROM_ENTITY_INDEX(eiObj)
	ENDIF
	
	RETURN NULL
ENDFUNC

FUNC OBJECT_INDEX GET_FMMC_ENTITY_DYNOPROP(INT iDynopropIndex)
	ENTITY_INDEX eiObj = GET_FMMC_ENTITY(ciENTITY_TYPE_DYNO_PROPS, iDynopropIndex)
	
	IF DOES_ENTITY_EXIST(eiObj)
		RETURN GET_OBJECT_INDEX_FROM_ENTITY_INDEX(eiObj)
	ENDIF
	
	RETURN NULL
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Shared Entity Functions
// ##### Description: Helpers and wrappers for easily grabbing entity references from data
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC VECTOR MC_GET_POSITION_OFFSET_RELATIVE_TO_ENTITY(VECTOR vPositionIn, INT iEntityType, INT iEntityID, BOOL bOnGround) // EDIT CREATOR_GET_POSITION_OFFSET_RELATIVE_TO_ENTITY if you are modifying this.
	VECTOR vPos
	
	IF iEntityType = ciENTITY_TYPE_NONE
		vPos = vPositionIn
		
	ELIF iEntityID != -1
		SWITCH iEntityType
			CASE ciENTITY_TYPE_PED
				IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[iEntityID])
					PED_INDEX pedIndex
					pedIndex = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iEntityID])
					vPos = GET_ENTITY_COORDS(pedIndex, FALSE)
					vPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPos, GET_ENTITY_HEADING(pedIndex), vPositionIn)
				ENDIF
			BREAK
			CASE ciENTITY_TYPE_VEHICLE
				IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iEntityID])
					VEHICLE_INDEX vehIndex
					vehIndex = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iEntityID])
					vPos = GET_ENTITY_COORDS(vehIndex, FALSE)
					vPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPos, GET_ENTITY_HEADING(vehIndex), vPositionIn)
				ENDIF
			BREAK
			CASE ciENTITY_TYPE_OBJECT
				IF NETWORK_DOES_NETWORK_ID_EXIST(GET_OBJECT_NET_ID(iEntityID))
					OBJECT_INDEX ObjIndex
					ObjIndex = NET_TO_OBJ(GET_OBJECT_NET_ID(iEntityID))
					vPos = GET_ENTITY_COORDS(ObjIndex, FALSE)
					vPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPos, GET_ENTITY_HEADING(ObjIndex), vPositionIn)
				ENDIF
			BREAK
			CASE ciENTITY_TYPE_TRAIN
				IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niTrain[iEntityID])
					VEHICLE_INDEX vehIndex
					vehIndex = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niTrain[iEntityID])
					vPos = GET_ENTITY_COORDS(vehIndex, FALSE)
					vPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPos, GET_ENTITY_HEADING(vehIndex), vPositionIn)
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
	
	IF bOnGround
		FLOAT fDist = 0.0
		INT iAttempts
		VECTOR vTemp = vPos
		WHILE iAttempts < 4
			
			vTemp = vPos
			vTemp.z += (2.0*iAttempts)
			
			IF GET_GROUND_Z_FOR_3D_COORD(vTemp, vTemp.z, FALSE)				
				
				// Only assign the position that's closest to the origin, but a valid/successful ground grab with GET_GROUND_Z_FOR_3D_COORD
				IF ABSF(vPos.z-vTemp.z) <= fDist
				OR fDist = 0.0
					fDist = ABSF(vPos.z-vTemp.z)				
					vPos = vTemp
				ENDIF
				
			ENDIF
			
			iAttempts++
		ENDWHILE
	ENDIF
		
	RETURN vPos
ENDFUNC

FUNC FLOAT MC_GET_HEADING_OFFSET_RELATIVE_TO_ENTITY(FLOAT fHeadingIn, INT iEntityType, INT iEntityID) // EDIT CREATOR_GET_HEADING_OFFSET_RELATIVE_TO_ENTITY if you are modifying this.
	FLOAT fHeading
	
	IF iEntityType = ciENTITY_TYPE_NONE
		fHeading = fHeadingIn
		
	ELIF iEntityID != -1
		SWITCH iEntityType
			CASE ciENTITY_TYPE_PED
				IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[iEntityID])
					PED_INDEX pedIndex
					pedIndex = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iEntityID])
					fHeading = GET_ENTITY_HEADING(pedIndex)
					fHeading += fHeadingIn
				ENDIF
			BREAK
			CASE ciENTITY_TYPE_VEHICLE
				IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iEntityID])
					VEHICLE_INDEX vehIndex
					vehIndex = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iEntityID])
					fHeading = GET_ENTITY_HEADING(vehIndex)
					fHeading += fHeadingIn
				ENDIF
			BREAK
			CASE ciENTITY_TYPE_OBJECT
				IF NETWORK_DOES_NETWORK_ID_EXIST(GET_OBJECT_NET_ID(iEntityID))
					OBJECT_INDEX ObjIndex
					ObjIndex = NET_TO_OBJ(GET_OBJECT_NET_ID(iEntityID))
					fHeading = GET_ENTITY_HEADING(ObjIndex)
					fHeading += fHeadingIn
				ENDIF
			BREAK
			CASE ciENTITY_TYPE_TRAIN
				IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niTrain[iEntityID])
					VEHICLE_INDEX vehIndex
					vehIndex = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niTrain[iEntityID])
					fHeading = GET_ENTITY_HEADING(vehIndex)
					fHeading += fHeadingIn
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
			
	RETURN fHeading
ENDFUNC

FUNC VECTOR MC_GET_POSITION_FOR_ENTITY_INDEX(INT iEntityType, INT iEntityID)
	VECTOR vPos
	
	IF iEntityType = ciENTITY_TYPE_NONE
		RETURN vPos
		
	ELIF iEntityID != -1
		SWITCH iEntityType
			CASE ciENTITY_TYPE_PED
				IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[iEntityID])
					PED_INDEX pedIndex
					pedIndex = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iEntityID])
					vPos = GET_ENTITY_COORDS(pedIndex, FALSE)
				ENDIF
			BREAK
			CASE ciENTITY_TYPE_VEHICLE
				IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iEntityID])
					VEHICLE_INDEX vehIndex
					vehIndex = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iEntityID])
					vPos = GET_ENTITY_COORDS(vehIndex, FALSE)
				ENDIF
			BREAK
			CASE ciENTITY_TYPE_OBJECT
				IF NETWORK_DOES_NETWORK_ID_EXIST(GET_OBJECT_NET_ID(iEntityID))
					OBJECT_INDEX ObjIndex
					ObjIndex = NET_TO_OBJ(GET_OBJECT_NET_ID(iEntityID))
					vPos = GET_ENTITY_COORDS(ObjIndex, FALSE)
				ENDIF
			BREAK
			CASE ciENTITY_TYPE_TRAIN
				IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niTrain[iEntityID])
					VEHICLE_INDEX vehIndex
					vehIndex = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niTrain[iEntityID])
					vPos = GET_ENTITY_COORDS(vehIndex, FALSE)
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
		
	RETURN vPos
ENDFUNC

FUNC FLOAT MC_GET_HEADING_FOR_ENTITY_INDEX(INT iEntityType, INT iEntityID)
	FLOAT fHeading
	
	IF iEntityType = ciENTITY_TYPE_NONE
		RETURN fHeading
		
	ELIF iEntityID != -1
		SWITCH iEntityType
			CASE ciENTITY_TYPE_PED
				IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[iEntityID])
					PED_INDEX pedIndex
					pedIndex = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iEntityID])
					fHeading = GET_ENTITY_HEADING(pedIndex)
				ENDIF
			BREAK
			CASE ciENTITY_TYPE_VEHICLE
				IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iEntityID])
					VEHICLE_INDEX vehIndex
					vehIndex = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iEntityID])
					fHeading = GET_ENTITY_HEADING(vehIndex)
				ENDIF
			BREAK
			CASE ciENTITY_TYPE_OBJECT
				IF NETWORK_DOES_NETWORK_ID_EXIST(GET_OBJECT_NET_ID(iEntityID))
					OBJECT_INDEX ObjIndex
					ObjIndex = NET_TO_OBJ(GET_OBJECT_NET_ID(iEntityID))
					fHeading = GET_ENTITY_HEADING(ObjIndex)
				ENDIF
			BREAK
			CASE ciENTITY_TYPE_TRAIN
				IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niTrain[iEntityID])
					VEHICLE_INDEX vehIndex
					vehIndex = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niTrain[iEntityID])
					fHeading = GET_ENTITY_HEADING(vehIndex)
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
			
	RETURN fHeading
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Warp Portals
// ##### Description: Various helper functions for Warp Portals
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL SHOULD_VEHICLE_STOP_OR_BRING_TO_HALT_WAIT()
	IF sWarpPortal.eWarpPortalState != eWarpPortalState_Idle
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Wanted
// ##### Description: Wanted and Cop helpers that need accessed from all headers
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
FUNC BOOL IS_CURRENT_OBJECTIVE_LOSE_WANTED()
	RETURN IS_BIT_SET(iLocalBoolCheck4, LBOOL4_CURRENT_OBJECTIVE_LOSE_WANTED)
ENDFUNC		

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Gang Chase
// ##### Description: Gang Chase helpers that need accessed from all headers
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
FUNC BOOL IS_CURRENT_OBJECTIVE_LOSE_GANG_CHASE()
	RETURN IS_BIT_SET(iLocalBoolCheck, LBOOL_CURRENT_OBJECTIVE_LOSE_GANG_CHASE)
ENDFUNC

FUNC INT GET_TOTAL_NUMBER_OF_GANG_CHASE_CHASING_ALL_TEAMS()

	INT iTeam, iTotal
	FOR iTeam = 0 TO MC_serverBD.iNumberOfTeams - 1
		iTotal += MC_serverBD_1.iNumGangChaseUnitsChasing[iTeam]
	ENDFOR
	
	RETURN iTotal
	
ENDFUNC

FUNC BOOL ARE_MULTIPLE_GANG_CHASE_UNIT_TYPES_AVAILABLE_FOR_TEAM(INT iTeam)
	
	IF MC_serverBD_4.iCurrentHighestPriority[iTeam] >= FMMC_MAX_RULES
		RETURN FALSE
	ENDIF
	
	INT i
	FOR i = 0 TO FMMC_MAX_GANG_CHASE_UNIT_TYPES - 1
		IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sMultipleGangChaseSettings[MC_serverBD_4.iCurrentHighestPriority[iTeam]][i].iType != ciBACKUP_TYPE_NONE
			RETURN TRUE
		ENDIF
	ENDFOR
	
	RETURN FALSE
	
ENDFUNC

FUNC INT GET_GANG_CHASE_UNIT_GANG_TYPE(INT iTeam, INT iRule, INT iGangChaseUnitIndex, INT iChosenType = -1)
	
	IF iRule >= FMMC_MAX_RULES
		RETURN ciBACKUP_TYPE_NONE
	ENDIF
	
	// check for single unit type
	IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iGangBackupType[iRule] != ciBACKUP_TYPE_NONE
		RETURN g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iGangBackupType[iRule]
	ENDIF
	
	// count number of multiple unit types, if any
	IF ARE_MULTIPLE_GANG_CHASE_UNIT_TYPES_AVAILABLE_FOR_TEAM(iTeam)
		IF IS_BIT_SET(MC_serverBD_1.sGangChase[iGangChaseUnitIndex].iBitset, SBBBOOL_SELECTED_GANG_TYPE)
			RETURN MC_serverBD_1.sGangChase[iGangChaseUnitIndex].iGangType
		ELIF iChosenType > -1
			PRINTLN("[GANG_CHASE] GET_GANG_CHASE_UNIT_GANG_TYPE - Unit: ", iGangChaseUnitIndex, " Using target chosen type: ", iChosenType)
			RETURN iChosenType
		ENDIF
	ENDIF

	// no gang chase type has been set, return none
	RETURN ciBACKUP_TYPE_NONE
ENDFUNC

/// PURPOSE:
///    Obtains an index for accessing the g_FMMC_STRUCT.sGangChaseUnitConfigs from a gang type
/// PARAMS:
///    iInGangType - The gang type for which a config index is required
///    iOutIndex - The obtained index
/// RETURNS:
///    True if a valid index was obtained
FUNC BOOL GET_GANG_CHASE_UNIT_CREATOR_CONFIG_INDEX_FROM_GANG_TYPE(INT iInGangType, INT& iOutIndex)
	iOutIndex = -1
	
	SWITCH iInGangType
		CASE ciBACKUP_TYPE_CUSTOM_1
			iOutIndex = 0
		BREAK
		CASE ciBACKUP_TYPE_CUSTOM_2					
			iOutIndex = 1
		BREAK
		CASE ciBACKUP_TYPE_CUSTOM_3						
			iOutIndex = 2
		BREAK
		CASE ciBACKUP_TYPE_CUSTOM_4					
			iOutIndex = 3
		BREAK
		DEFAULT BREAK
	ENDSWITCH
	
	IF (iOutIndex >= 0) 
	AND (iOutIndex < FMMC_MAX_GANG_CHASE_UNIT_CONFIGS)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC		

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Objective Text
// ##### Description: Helpers and wrappers for Objective Text functionality 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC OVERRIDE_OBJECTIVE_TEXT_WITH_CUSTOM_STRING_THIS_FRAME(INT iCustomStringIndex)
	
	IF NOT IS_CUSTOM_STRING_LIST_STRING_VALID(iCustomStringIndex)
		EXIT
	ENDIF
	
	SET_BIT(iLocalBoolResetCheck1, LBOOL_RESET1_OBJECTIVE_TEXT_OVERRIDDEN)
	
	IF iCustomObjectiveTextOverride != iCustomStringIndex
		iCustomObjectiveTextOverride = iCustomStringIndex
		PRINTLN("[ObjectiveText] OVERRIDE_OBJECTIVE_TEXT_WITH_CUSTOM_STRING_THIS_FRAME - Overriding Current Objective with Custom String: ", iCustomObjectiveTextOverride)
		DEBUG_PRINTCALLSTACK()
	ENDIF
	
ENDPROC

PROC PROCESS_RESETTING_OVERRIDDEN_OBJECTIVE_TEXT_DATA()
	IF NOT IS_BIT_SET(iLocalBoolResetCheck1, LBOOL_RESET1_OBJECTIVE_TEXT_OVERRIDDEN)
	AND iCustomObjectiveTextOverride != -1
		PRINTLN("[ObjectiveText] PROCESS_OVERRIDDEN_OBJECTIVE_TEXT_DATA - No Longer Overriding Current Objective with Custom String: ", iCustomObjectiveTextOverride)
		iCustomObjectiveTextOverride = -1
	ENDIF
ENDPROC

#IF IS_DEBUG_BUILD
FUNC STRING GET_OBJECTIVE_TEXT_DELAY_TYPE_STRING(INT iDelayType)
	SWITCH iDelayType
		CASE ciOBJECTIVE_TEXT_DELAY_ALL			RETURN "All"
		CASE ciOBJECTIVE_TEXT_DELAY_VEHICLE		RETURN "Vehicle"
		CASE ciOBJECTIVE_TEXT_DELAY_PED			RETURN "Ped"
		CASE ciOBJECTIVE_TEXT_DELAY_OBJECT		RETURN "Object"
		CASE ciOBJECTIVE_TEXT_DELAY_PLAYER		RETURN "Player Rule"
		CASE ciOBJECTIVE_TEXT_DELAY_WANTED		RETURN "Wanted"
		CASE ciOBJECTIVE_TEXT_DELAY_LOCATION	RETURN "Location"
		CASE ciOBJECTIVE_TEXT_DELAY_OVERRIDE	RETURN "Override"
	ENDSWITCH
	
	PRINTLN("[ObjectiveText_Delay] GET_OBJECTIVE_TEXT_DELAY_TYPE_STRING - Invalid delay type: ", iDelayType)
	ASSERTLN("[ObjectiveText_Delay] GET_OBJECTIVE_TEXT_DELAY_TYPE_STRING - Invalid delay type: ", iDelayType)
	RETURN "Invalid"
ENDFUNC
#ENDIF

PROC REQUEST_OBJECTIVE_TEXT_DELAY(INT iDelayType, BOOL bOnlyIfNotActive #IF IS_DEBUG_BUILD , STRING stPrint #ENDIF )
	
	IF iDelayType = ciOBJECTIVE_TEXT_DELAY_ALL
	
		IF bOnlyIfNotActive
			INT iExludeActive = ciOBJECTIVE_TEXT_DELAY_ALL ^ iObjectiveTextActiveDelayBitset
			IF iExludeActive != 0
				PRINTLN("[ObjectiveText_Delay] REQUEST_OBJECTIVE_TEXT_DELAY - Type: All (not active) - ", iExludeActive, ", Reason: ", stPrint)
				iObjectiveTextRequestDelayBitset += iExludeActive
			ENDIF
		ELSE
			PRINTLN("[ObjectiveText_Delay] REQUEST_OBJECTIVE_TEXT_DELAY - Type: All, Reason: ", stPrint)
			iObjectiveTextRequestDelayBitset = ciOBJECTIVE_TEXT_DELAY_ALL
		ENDIF
		
		EXIT
	ENDIF
	
	IF bOnlyIfNotActive
	AND IS_BIT_SET(iObjectiveTextActiveDelayBitset, iDelayType)
		EXIT
	ENDIF
	
	PRINTLN("[ObjectiveText_Delay] REQUEST_OBJECTIVE_TEXT_DELAY - Type: ", GET_OBJECTIVE_TEXT_DELAY_TYPE_STRING(iDelayType), ", Reason: ", stPrint)
	SET_BIT(iObjectiveTextRequestDelayBitset, iDelayType)
	
ENDPROC

PROC CLEAR_REQUEST_OBJECTIVE_TEXT_DELAY(INT iDelayType)
	
	PRINTLN("[ObjectiveText_Delay] CLEAR_REQUEST_OBJECTIVE_TEXT_DELAY - Type: ", GET_OBJECTIVE_TEXT_DELAY_TYPE_STRING(iDelayType))

	IF iDelayType = ciOBJECTIVE_TEXT_DELAY_ALL
		iObjectiveTextRequestDelayBitset = 0
		EXIT
	ENDIF
	
	CLEAR_BIT(iObjectiveTextRequestDelayBitset, iDelayType)
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Minigames/Hacking
// ##### Description: Helpers and wrappers for general minigame/hacking functionality 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC INT CALCULATE_HACKING_STRUCT_INDEX_FOR_ENTITY(INT iIndex, INT iEntityType = ciENTITY_TYPE_OBJECT)
	INT iLoopIndex
	INT iHackingStructIndex = 0
	
	PRINTLN("[Hacking] CALCULATE_HACKING_STRUCT_INDEX_FOR_ENTITY | Looking for hacking struct index for entity || Type: ", iEntityType, " | Index: ", iIndex)
	
	// Loop through placed entities of this type to count up how many share the same minigame/interaction
	FOR iLoopIndex = 0 TO (iIndex - 1)
		SWITCH iEntityType
			CASE ciENTITY_TYPE_OBJECT
				IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iIndex].iObjectMinigame = ciFMMC_OBJECT_MINIGAME_TYPE__NONE
					// This Object won't be using any hacking structs
					RETURN 0
				ENDIF
				
				// Compare minigame settings
				IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iIndex].iObjectMinigame = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iLoopIndex].iObjectMinigame
					iHackingStructIndex++
					PRINTLN("[Hacking] CALCULATE_HACKING_STRUCT_INDEX_FOR_ENTITY | iHackingStructIndex incremented to ", iHackingStructIndex, " for Object ", iIndex, " || Minigame: ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[iIndex].iObjectMinigame)
				ENDIF
			BREAK
			
			CASE ciENTITY_TYPE_INTERACTABLES
				IF NOT DOES_INTERACTABLE_HAVE_AN_INTERACTION_SET(iIndex)
					// This Interactable won't be using any hacking structs
					RETURN 0
				ENDIF
				
				// Compare Interaction settings
				IF g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iIndex].iInteractable_InteractionType = g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iLoopIndex].iInteractable_InteractionType
					iHackingStructIndex++
					PRINTLN("[Hacking] CALCULATE_HACKING_STRUCT_INDEX_FOR_ENTITY | iHackingStructIndex incremented to ", iHackingStructIndex, " for Interactable ", iIndex, " || Interaction: ", g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iIndex].iInteractable_InteractionType)
				ENDIF
			BREAK
		ENDSWITCH
	ENDFOR
	
	RETURN iHackingStructIndex
ENDFUNC

FUNC INT GET_HACKING_STRUCT_INDEX_TO_USE(INT iIndex, INT iEntityType = ciENTITY_TYPE_OBJECT)
	SWITCH iEntityType
		CASE ciENTITY_TYPE_OBJECT
			RETURN iObjectCachedHackingStructIndexes[iIndex]
		BREAK
		CASE ciENTITY_TYPE_INTERACTABLES
			RETURN iInteractableCachedHackingStructIndexes[iIndex]
		BREAK
	ENDSWITCH
	
	RETURN -1
ENDFUNC

PROC SET_OBJECT_HACKING(INT iObject)
	IF MC_playerBD[iLocalPart].iObjHacking != iObject
		MC_playerBD[iLocalPart].iObjHacking = iObject
		PRINTLN("[Hacking][Objects][Object ", iObject, "] SET_OBJECT_HACKING | Setting iObjHacking to ", MC_playerBD[iPartToUse].iObjHacking)
	ENDIF
ENDPROC

PROC CLEAR_OBJECT_HACKING()
	IF MC_playerBD[iLocalPart].iObjHacking > -1
		PRINTLN("[Hacking][Objects][Object ", MC_playerBD[iLocalPart].iObjHacking, "] CLEAR_OBJECT_HACKING | Clearing iObjHacking and iCachedCurrentHackingStructIndex")
		MC_playerBD[iLocalPart].iObjHacking = -1
		DEBUG_PRINTCALLSTACK()
	ENDIF
ENDPROC

PROC CLEAR_OBJ_HACKING_IF_NEEDED(INT iObj)
	IF MC_playerBD[iLocalPart].iObjHacking = iObj
		CLEAR_OBJECT_HACKING()
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Drones
// ##### Description: Various helper functions for drones that are required for all headers.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL SHOULD_DRONE_SYSTEMS_BE_ACTIVE(FMMC_DRONE_DATA &eDroneData)
	
	IF NOT IS_BIT_SET(eDroneData.iBS, ciFMMC_DroneBS_Enable)	
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Objects
// ##### Description: Helpers and wrappers for object rule functionality 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL IS_CURRENT_OBJECTIVE_COLLECT_OBJECT()
	
	IF NOT IS_BIT_SET(iLocalBoolCheck8, LBOOL8_OBJECT_CONTINUITY_INIT_DONE)
		//We don't know if we have the object yet.
		RETURN FALSE
	ENDIF
	
	IF NOT IS_BIT_SET(MC_serverbd.iServerBitSet, SBBOOL_FIRST_UPDATE_DONE)
		//We don't know who has the object yet
		RETURN FALSE
	ENDIF

	INT iTeam = MC_playerBD[iPartToUse].iTeam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	IF iRule < FMMC_MAX_RULES
		IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjRequiredForRulePass[iRule] > -1
		AND MC_serverBD.iObjCarrier[g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjRequiredForRulePass[iRule]] = -1
			RETURN TRUE
		ENDIF
	ENDIF
	
	
	RETURN FALSE
ENDFUNC	

PROC CACHE_OBJECT_SAFE_DROP_POSITION(INT iObj, VECTOR vCachePos, BOOL bIsInitialPosition = FALSE)
	
	IF bIsInitialPosition
		// The coords we're caching now should be considered the start point of this object as if it wasn't ever anywhere else, so we're clobbering the old cached points to reflect that
		INT iObjSafePoint
		FOR iObjSafePoint = 0 TO ciObjectSafeDropPoints - 1
			vObjectLastSafeDropPoint[iObj][iObjSafePoint] = vCachePos
		ENDFOR
		
	ELSE
		// Shuffle the older positions down the queue
		INT iObjSafePoint
		FOR iObjSafePoint = ciObjectSafeDropPoints - 1 TO 1
			vObjectLastSafeDropPoint[iObj][iObjSafePoint] = vObjectLastSafeDropPoint[iObj][iObjSafePoint - 1]
		ENDFOR
	ENDIF
	
	vObjectLastSafeDropPoint[iObj][0] = vCachePos
	PRINTLN("[ObjectSafePoint][Objects_SPAM][Object_SPAM ", iObj, "] CACHE_OBJECT_SAFE_DROP_POSITION | Caching object ", iObj, "'s safe point as ", vObjectLastSafeDropPoint[iObj][0], " || bIsInitialPosition: ", bIsInitialPosition)
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Locations
// ##### Description: Helpers and wrappers for location rule functionality 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC INT GET_PLAYERS_REQUIRED_TO_LEAVE_LOCATION(INT iLoc, INT iTeam)
	
	SWITCH g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iWholeTeamAtLocation[iTeam]
	
		CASE ciGOTO_LOCATION_INDIVIDUAL		RETURN MC_serverBD.iNumberOfPlayingPlayers[iTeam] - 1
		
		CASE ciGOTO_LOCATION_WHOLE_TEAM
		CASE ciGOTO_LOCATION_ALL_PLAYERS
		CASE ciGOTO_LOCATION_CUSTOM_TEAMS	
			RETURN 0
		BREAK
		
		DEFAULT
			//Custom no. players need to leave the locate:
			RETURN MC_serverBD.iNumberOfPlayingPlayers[0] + MC_serverBD.iNumberOfPlayingPlayers[1] + MC_serverBD.iNumberOfPlayingPlayers[2] + MC_serverBD.iNumberOfPlayingPlayers[3] - (2 + (g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iWholeTeamAtLocation[iTeam] - ciGOTO_LOCATION_2_PLAYERS))
		BREAK
		
	ENDSWITCH
	
	RETURN -1
	
ENDFUNC

FUNC BOOL IS_GOTO_LOCATE_BEING_CONTESTED(INT iLoc, BOOL bCheckForOwner = TRUE, BOOL bUnCapturedCheck = FALSE)
	INT i
	FOR i = 0 TO MC_serverBD.iNumActiveTeams-1
		IF NOT DOES_TEAM_LIKE_TEAM(MC_serverBD.iLocOwner[iLoc], i)
		AND MC_serverBD.iLocOwner[iLoc] != i
		AND (MC_serverBD.iLocOwner[iLoc] != -1 OR NOT bCheckForOwner)
			IF NOT bUnCapturedCheck
				IF MC_serverBD_1.inumberOfTeamInArea[iLoc][i] > 0
					RETURN TRUE
				ENDIF
			ELSE
				IF i != MC_playerBD[iPartToUse].iteam
					IF MC_serverBD_1.inumberOfTeamInArea[iLoc][i] > 0
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_PLAYERS_IN_LEAVE_LOCATION(INT iLoc, INT iTeam)
	
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	IF iRule >= FMMC_MAX_RULES
		RETURN -1
	ENDIF
	
	INT i, iTotal
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFourteen[iRule], ciBS_RULE14_LEAVE_LOCATES_REQUIRE_ALL_OUT)
		SWITCH g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iWholeTeamAtLocation[iTeam]
			
			CASE ciGOTO_LOCATION_INDIVIDUAL
			CASE ciGOTO_LOCATION_WHOLE_TEAM		
				RETURN MC_serverBD_1.iNumberOfTeamInAnyLeaveAreas[iTeam]
			BREAK
			
			CASE ciGOTO_LOCATION_CUSTOM_TEAMS
				iTotal = 0
				FOR i = 0 TO MC_serverBD.iNumberOfTeams - 1
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS, ciLoc_BS_CustomTeams_T0_Needs_T0 + (i * 4) + i)
						iTotal += MC_serverBD_1.iNumberOfTeamInAnyLeaveAreas[i]
					ENDIF
				ENDFOR
				RETURN iTotal
			BREAK
			
			CASE ciGOTO_LOCATION_ALL_PLAYERS
			FALLTHRU //Go to default
			
			DEFAULT //Check all teams
				iTotal = 0
				FOR i = 0 TO MC_serverBD.iNumberOfTeams - 1
					iTotal += MC_serverBD_1.iNumberOfTeamInAnyLeaveAreas[i]
				ENDFOR
				RETURN iTotal
			BREAK
			
		ENDSWITCH
	ELSE
		SWITCH g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iWholeTeamAtLocation[iTeam]
			
			CASE ciGOTO_LOCATION_INDIVIDUAL		
			CASE ciGOTO_LOCATION_WHOLE_TEAM
				RETURN MC_serverBD_1.iNumberOfTeamInLeaveArea[iLoc][iTeam]
			BREAK
			
			CASE ciGOTO_LOCATION_CUSTOM_TEAMS
				iTotal = 0
				FOR i = 0 TO MC_serverBD.iNumberOfTeams - 1
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS, ciLoc_BS_CustomTeams_T0_Needs_T0 + (i * 4) + i)
						iTotal += MC_serverBD_1.iNumberOfTeamInLeaveArea[iLoc][i]
					ENDIF
				ENDFOR
				RETURN iTotal
			BREAK
			
			CASE ciGOTO_LOCATION_ALL_PLAYERS
			FALLTHRU //Go to default
			
			DEFAULT //Check all teams
				iTotal = 0
				FOR i = 0 TO MC_serverBD.iNumberOfTeams - 1
					iTotal += MC_serverBD_1.iNumberOfTeamInLeaveArea[iLoc][i]
				ENDFOR
				RETURN iTotal
			BREAK
		ENDSWITCH
	ENDIF
	
	RETURN -1
	
ENDFUNC
				

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Misc Helper Functions
// ##### Description: Various helper functions
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC SET_POST_MISSION_SPAWN_SEQUENCE(POST_MISSION_SPAWN_SCENARIO ePostMissionSpawnScenario)
	IF IS_FAKE_MULTIPLAYER_MODE_SET()
		EXIT
	ENDIF
	IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.PostMissionSpawnScenario != ePostMissionSpawnScenario
		PRINTLN("SET_POST_MISSION_SPAWN_SEQUENCE - ePostMissionSpawnScenario: ", ePostMissionSpawnScenario)		
		g_TransitionSessionNonResetVars.sPostMissionCleanupData.PostMissionSpawnScenario = ePostMissionSpawnScenario
	ENDIF
ENDPROC

FUNC BOOL DID_RULE_CHANGE_DURING_STAGGERED_LOCAL_PLAYER_PROCESSING()
	RETURN iRuleChangedDuringStaggeredLocalPlayerIteratorTracker > -1
ENDFUNC

FUNC BOOL HAS_MISSION_GOT_SUDDEN_DEATH()
	IF IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_SUDDEN_DEATH_MOREINLOC)
	OR IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_SUDDEN_DEATH_KILLALLENEMIES)
	OR IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_BE_MY_VALENTINES)
	OR IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_TARGET)
	OR IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_PACKAGE_HELD)
	OR IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_KILLALLENEMIES_PENNED_IN)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_RULE_INDEX_VALID(INT iRule)
	RETURN iRule >= 0 AND iRule < FMMC_MAX_RULES
ENDFUNC

FUNC BOOL IS_TEAM_INDEX_VALID(INT iTeam)
	RETURN iTeam >= 0 AND iTeam < FMMC_MAX_TEAMS
ENDFUNC

FUNC BOOL HAS_SERVER_CHECKED_ALL_CLIENTS_READY_FOR_INTRO()
	RETURN IS_BIT_SET( MC_serverBD.iServerBitSet, SSBOOL_ALL_READY_INTRO_CUT )
ENDFUNC

//FMMC2020 - THIS REALLY SHOULDNT BE HERE
PROC SET_TEAM_OBJECTIVE_OUTCOME(INT iteam,INT ipriority,BOOL bpass)

	IF ipriority < FMMC_MAX_RULES
		IF bpass
			SET_BIT(MC_serverBD.iServerPassBitSet[iteam],ipriority)
		ELSE
			CLEAR_BIT(MC_serverBD.iServerPassBitSet[iteam],ipriority)
		ENDIF
	ENDIF

ENDPROC

FUNC BOOL IS_IT_SAFE_TO_LOAD_NAVMESH()
	
	// Only one script can use ADD_NAVMESH_REQUIRED_REGION at a time, so we must let other scripts use it when they need it
	
	IF NOT bLocalPlayerPedOk
	OR (IS_PLAYER_RESPAWNING(LocalPlayer) AND NOT IS_FAKE_MULTIPLAYER_MODE_SET(TRUE))
		// The respawning system needs navmesh
		PRINTLN("IS_IT_SAFE_TO_LOAD_NAVMESH - Returning FALSE, bLocalPlayerPedOk = ",bLocalPlayerPedOk,", IS_PLAYER_RESPAWNING = ",IS_PLAYER_RESPAWNING(LocalPlayer))
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(LocalPlayer)
		// The simple interior loading/unloading requires a player warp, which uses navmesh
		PRINTLN("IS_IT_SAFE_TO_LOAD_NAVMESH - Returning FALSE, IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR is TRUE")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

//Sets WASTED shard text to be changed
PROC CHANGE_WASTED_SHARD_TEXT(BOOL bSet = FALSE)
	IF bSet
		PRINTLN("[KH] - WASTED TEXT CHANGED")
	ELSE
		PRINTLN("[KH] - WASTED TEXT CHANGED BACK TO ORIGINAL")
	ENDIF
	g_ChangeWastedShardText = bSet
ENDPROC

PROC SET_ALL_PLAYER_BLIPS_LONG_RANGE(BOOL bIsLongRange)
	IF NETWORK_IS_ACTIVITY_SESSION()
		INT i
		PRINTLN("[PLAYER_LOOP] - SET_ALL_PLAYER_BLIPS_LONG_RANGE	")
		FOR i = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1			
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
				SET_PLAYER_BLIP_AS_LONG_RANGE(INT_TO_PLAYERINDEX(i),bIsLongRange)
			ENDIF
		ENDFOR
	ENDIF	
ENDPROC

PROC BUILD_PLACED_PTFX_ASSOCIATED_SOUND_FX(TEXT_LABEL_63 tl63_EffectName, TEXT_LABEL_63 &tl63_SoundBank, TEXT_LABEL_63 &tl63_SoundName, TEXT_LABEL_63 &tl63_SoundSet)
	IF NOT IS_STRING_NULL_OR_EMPTY(tl63_EffectName)
		IF ARE_STRINGS_EQUAL(tl63_EffectName, "scr_ch_finale_bug_infestation")
			tl63_SoundBank = "DLC_HEIST3/CASINO_HEIST_FINALE_GENERAL_01"
			tl63_SoundName = "bugs_infestation"
			tl63_SoundSet = "dlc_ch_heist_finale_sounds"			
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SET_PLAYER_VISUAL_AID(VISUAL_AID_MODES eVisualAid, BOOL bOverride = FALSE, VISUAL_AID_SOUND bUseSound = VISUALAID_SOUND_ON)
	
	// Default always just clears override
	IF eVisualAid = VISUALAID_DEFAULT
	
		IF g_sFMMCVisualAid.eOverride != VISUALAID_DEFAULT
		AND g_sFMMCVisualAid.bMaintainOverride

			PRINTLN("[PLAYER VISUAL AIDS] - SET_PLAYER_VISUAL_AID - CLEARING override - eVisualAid = VISUALAID_DEFAULT")
			DEBUG_PRINTCALLSTACK()
			
			g_sFMMCVisualAid.eOverride = VISUALAID_DEFAULT
			g_sFMMCVisualAid.bMaintainOverride = FALSE
			g_sFMMCVisualAid.tl31OverrideScriptName = ""
			g_sFMMCVisualAid.eUseSound = bUseSound
			RETURN TRUE
		
		ENDIF

	// Fire & Forget
	ELIF NOT bOverride
	
		PRINTLN("[PLAYER VISUAL AIDS] - SET_PLAYER_VISUAL_AID - SETTING(fire & forget) to ", GET_VISUAL_AID_MODE_NAME(eVisualAid), " and CLEARING override of ", GET_VISUAL_AID_MODE_NAME(g_sFMMCVisualAid.eOverride))
		DEBUG_PRINTCALLSTACK()
		
		// fire and forgets should clear our override
		g_sFMMCVisualAid.eOverride = eVisualAid
		g_sFMMCVisualAid.bMaintainOverride = FALSE
		g_sFMMCVisualAid.tl31OverrideScriptName = ""
		g_sFMMCVisualAid.eUseSound = bUseSound
		RETURN TRUE
	
	// Override
	ELSE
	
		IF g_sFMMCVisualAid.eOverride != eVisualAid
			
			PRINTLN("[PLAYER VISUAL AIDS] - SET_PLAYER_VISUAL_AID - OVERRIDING with ", GET_VISUAL_AID_MODE_NAME(eVisualAid))
			DEBUG_PRINTCALLSTACK()
			g_sFMMCVisualAid.eOverride = eVisualAid
			
			g_sFMMCVisualAid.bMaintainOverride = TRUE
			g_sFMMCVisualAid.tl31OverrideScriptName = GET_THIS_SCRIPT_NAME()
			g_sFMMCVisualAid.eUseSound = bUseSound
			RETURN TRUE
			
		ENDIF

	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC STRING GET_AUDIO_SCENE_FROM_MIX_INDEX(INT iAudioMix)

	SWITCH(INT_TO_ENUM(eAUDIO_MIXES,iAudioMix))			
		CASE AUDIO_MIX_GUNFIGHT		RETURN "DLC_HEIST_MISSION_GUNFIGHT_SCENE"
		CASE AUDIO_MIX_CAR_CHASE	RETURN "DLC_HEIST_MISSION_DRIVING_SCENE"
	ENDSWITCH
	
	RETURN ""

ENDFUNC

PROC STOP_ANY_CURRENTLY_PLAYING_AUDIO_MIX()
	IF iLocalCurrentAudioMix != -1
		STRING sAudio = GET_AUDIO_SCENE_FROM_MIX_INDEX(iLocalCurrentAudioMix)	
		IF IS_AUDIO_SCENE_ACTIVE(sAudio)
			STOP_AUDIO_SCENE(sAudio)
			PRINTLN("[RCC MISSION][MJM] STOP_ANY_CURRENTLY_PLAYING_AUDIO_MIX - STOP_AUDIO_SCENE :",sAudio)
		ENDIF
		iLocalCurrentAudioMix = -1
		PRINTLN("[RCC MISSION][MJM] STOP_ANY_CURRENTLY_PLAYING_AUDIO_MIX - iLocalCurrentAudioMix = -1 :")
	ENDIF
ENDPROC

PROC TURN_OFF_NON_ACTIVE_FILTERS(INT iFilter)

	IF ANIMPOSTFX_IS_RUNNING("InchPickup")
	AND iFilter != 11
		ANIMPOSTFX_STOP("InchPickup")
		ANIMPOSTFX_PLAY("InchPickupOut", 0, FALSE)
	
	ELIF ANIMPOSTFX_IS_RUNNING("PPOrange")
	AND iFilter != 12
		ANIMPOSTFX_STOP("PPOrange")
		ANIMPOSTFX_PLAY("PPOrangeOut", 0, FALSE)
	
	ELIF ANIMPOSTFX_IS_RUNNING("PPPurple")
	AND iFilter != 13
		ANIMPOSTFX_STOP("PPPurple")
		ANIMPOSTFX_PLAY("PPPurpleOut", 0, FALSE)
	
	ELIF ANIMPOSTFX_IS_RUNNING("PPGreen")
	AND iFilter != 14
		ANIMPOSTFX_STOP("PPGreen")
		ANIMPOSTFX_PLAY("PPGreenOut", 0, FALSE)
	
	ELIF ANIMPOSTFX_IS_RUNNING("PPPink")
	AND iFilter != 15
		ANIMPOSTFX_STOP("PPPink")
		ANIMPOSTFX_PLAY("PPPinkOut", 0, FALSE)
		
	ELIF ANIMPOSTFX_IS_RUNNING("DeadlineNeon")
	AND iFilter != 16
		ANIMPOSTFX_STOP("DeadlineNeon")
		
	ELIF GET_TIMECYCLE_MODIFIER_INDEX() != -1
	AND iFilter < 1
	AND iFilter > 10
		SET_TRANSITION_OUT_OF_TIMECYCLE_MODIFIER(1.0)
	
	ENDIF

ENDPROC

PROC TURN_OFF_ACTIVE_FILTERS()
	IF ANIMPOSTFX_IS_RUNNING("InchPickup")
		ANIMPOSTFX_STOP("InchPickup")
	ELIF ANIMPOSTFX_IS_RUNNING("PPOrange")
		ANIMPOSTFX_STOP("PPOrange")
	ELIF ANIMPOSTFX_IS_RUNNING("PPPurple")
		ANIMPOSTFX_STOP("PPPurple")
	ELIF ANIMPOSTFX_IS_RUNNING("PPGreen")
		ANIMPOSTFX_STOP("PPGreen")
	ELIF ANIMPOSTFX_IS_RUNNING("PPPink")
		ANIMPOSTFX_STOP("PPPink")
	ELIF ANIMPOSTFX_IS_RUNNING("DeadlineNeon")
		ANIMPOSTFX_STOP("DeadlineNeon")
	ELIF GET_TIMECYCLE_MODIFIER_INDEX() != -1
		SET_TRANSITION_OUT_OF_TIMECYCLE_MODIFIER(1.0)
	ENDIF	
ENDPROC

/// PURPOSE:
///    Pass over selected variables to globals for the BG script
PROC PROCESS_BG_GLOBALS_PASS_OVER()

	INT iLoop
	
	IF ARE_VECTORS_EQUAL(g_vMCBGPINCode, g_vMCBGPINEntry)
		   
		PRINTLN("PROCESS_BG_GLOBALS_PASS_OVER - BG SCRIPT Globals Handshake Active!")
		
		//Add anything we might want to set in a BG script here
		
		//Copied from RDR
		//Force Clears:
		// By doing a bitwise AND with the bitwise NOT of the bits we want to clear, we end up with what we want here!
		// 	For eg: we have a bitset of 0110, and we want to clear bits 0101. The NOT of those bits we want to clear
		//	is 1010, and this ANDed with our bitset results in: 0010.
		//Force Sets: Just an OR	
			
		//Server Bitsets
		IF bIsLocalPlayerHost
			IF gBG_MC_serverBD_VARS.iServerBitSet_ForceClear != 0
				MC_serverBD.iServerBitSet &= (gBG_MC_serverBD_VARS.iServerBitSet_ForceClear ^ -1)
			ENDIF
			IF gBG_MC_serverBD_VARS.iServerBitSet1_ForceClear != 0
				MC_serverBD.iServerBitSet1 &= (gBG_MC_serverBD_VARS.iServerBitSet1_ForceClear ^ -1)
			ENDIF
			IF gBG_MC_serverBD_VARS.iServerBitSet2_ForceClear != 0
				MC_serverBD.iServerBitSet2 &= (gBG_MC_serverBD_VARS.iServerBitSet2_ForceClear ^ -1)
			ENDIF
			IF gBG_MC_serverBD_VARS.iServerBitSet3_ForceClear != 0
				MC_serverBD.iServerBitSet3 &= (gBG_MC_serverBD_VARS.iServerBitSet3_ForceClear ^ -1)
			ENDIF
			IF gBG_MC_serverBD_VARS.iServerBitSet4_ForceClear != 0
				MC_serverBD.iServerBitSet4 &= (gBG_MC_serverBD_VARS.iServerBitSet4_ForceClear ^ -1)
			ENDIF
			IF gBG_MC_serverBD_VARS.iServerBitSet5_ForceClear != 0
				MC_serverBD.iServerBitSet5 &= (gBG_MC_serverBD_VARS.iServerBitSet5_ForceClear ^ -1)
			ENDIF
			IF gBG_MC_serverBD_VARS.iServerBitSet6_ForceClear != 0
				MC_serverBD.iServerBitSet6 &= (gBG_MC_serverBD_VARS.iServerBitSet6_ForceClear ^ -1)
			ENDIF
			IF gBG_MC_serverBD_VARS.iServerBitSet7_ForceClear != 0
				MC_serverBD.iServerBitSet7 &= (gBG_MC_serverBD_VARS.iServerBitSet7_ForceClear ^ -1)
			ENDIF
			IF gBG_MC_serverBD_VARS.iServerBitSet8_ForceClear != 0
				MC_serverBD.iServerBitSet8 &= (gBG_MC_serverBD_VARS.iServerBitSet8_ForceClear ^ -1)
			ENDIF
			
			IF gBG_MC_serverBD_VARS.iServerBitSet_ForceSet != 0
				MC_serverBD.iServerBitSet |= gBG_MC_serverBD_VARS.iServerBitSet_ForceSet
			ENDIF
			IF gBG_MC_serverBD_VARS.iServerBitSet1_ForceSet != 0
				MC_serverBD.iServerBitSet1 |= gBG_MC_serverBD_VARS.iServerBitSet1_ForceSet
			ENDIF
			IF gBG_MC_serverBD_VARS.iServerBitSet2_ForceSet != 0
				MC_serverBD.iServerBitSet2 |= gBG_MC_serverBD_VARS.iServerBitSet2_ForceSet
			ENDIF
			IF gBG_MC_serverBD_VARS.iServerBitSet3_ForceSet != 0
				MC_serverBD.iServerBitSet3 |= gBG_MC_serverBD_VARS.iServerBitSet3_ForceSet
			ENDIF
			IF gBG_MC_serverBD_VARS.iServerBitSet4_ForceSet != 0
				MC_serverBD.iServerBitSet4 |= gBG_MC_serverBD_VARS.iServerBitSet4_ForceSet
			ENDIF
			IF gBG_MC_serverBD_VARS.iServerBitSet5_ForceSet != 0
				MC_serverBD.iServerBitSet5 |= gBG_MC_serverBD_VARS.iServerBitSet5_ForceSet
			ENDIF
			IF gBG_MC_serverBD_VARS.iServerBitSet6_ForceSet != 0
				MC_serverBD.iServerBitSet6 |= gBG_MC_serverBD_VARS.iServerBitSet6_ForceSet
			ENDIF
			IF gBG_MC_serverBD_VARS.iServerBitSet7_ForceSet != 0
				MC_serverBD.iServerBitSet7 |= gBG_MC_serverBD_VARS.iServerBitSet7_ForceSet
			ENDIF
			IF gBG_MC_serverBD_VARS.iServerBitSet8_ForceSet != 0
				MC_serverBD.iServerBitSet8 |= gBG_MC_serverBD_VARS.iServerBitSet8_ForceSet
			ENDIF
		ENDIF
		
		//Player Bitsets
		
		IF gMC_PlayerBD_VARS.iClientBitSet_ForceClear != 0
			MC_playerBD[iLocalPart].iClientBitSet &= (gMC_PlayerBD_VARS.iClientBitSet_ForceClear ^ -1)
		ENDIF
		IF gMC_PlayerBD_VARS.iClientBitSet2_ForceClear != 0
			MC_playerBD[iLocalPart].iClientBitSet2 &= (gMC_PlayerBD_VARS.iClientBitSet2_ForceClear ^ -1)
		ENDIF
		IF gMC_PlayerBD_VARS.iClientBitSet3_ForceClear != 0
			MC_playerBD[iLocalPart].iClientBitSet3 &= (gMC_PlayerBD_VARS.iClientBitSet3_ForceClear ^ -1)
		ENDIF
		IF gMC_PlayerBD_VARS.iClientBitSet4_ForceClear != 0
			MC_playerBD[iLocalPart].iClientBitSet4 &= (gMC_PlayerBD_VARS.iClientBitSet4_ForceClear ^ -1)
		ENDIF
		
		IF gMC_PlayerBD_VARS.iClientBitSet_ForceSet != 0
			MC_playerBD[iLocalPart].iClientBitSet |= gMC_PlayerBD_VARS.iClientBitSet_ForceSet
		ENDIF
		IF gMC_PlayerBD_VARS.iClientBitSet2_ForceSet != 0
			MC_playerBD[iLocalPart].iClientBitSet2 |= gMC_PlayerBD_VARS.iClientBitSet2_ForceSet
		ENDIF
		IF gMC_PlayerBD_VARS.iClientBitSet3_ForceSet != 0
			MC_playerBD[iLocalPart].iClientBitSet3 |= gMC_PlayerBD_VARS.iClientBitSet3_ForceSet
		ENDIF
		IF gMC_PlayerBD_VARS.iClientBitSet4_ForceSet != 0
			MC_playerBD[iLocalPart].iClientBitSet4 |= gMC_PlayerBD_VARS.iClientBitSet4_ForceSet
		ENDIF
	ENDIF
	
	//The local variables
	gMC_LocalVariables_VARS.eScriptedCutsceneProgress		= eScriptedCutsceneProgress
	gMC_LocalVariables_VARS.iScriptedCutsceneProgress		= iScriptedCutsceneProgress
	gMC_LocalVariables_VARS.iMocapCutsceneProgress			= iMocapCutsceneProgress
	gMC_LocalVariables_VARS.eMocapRunningCutsceneProgress	= eMocapRunningCutsceneProgress
	gMC_LocalVariables_VARS.iScriptedCutsceneTeam			= iScriptedCutsceneTeam
	gMC_LocalVariables_VARS.iScriptedCutscenePlaying		= g_iFMMCScriptedCutscenePlaying
	gMC_LocalVariables_VARS.iLocalBoolCheck					= iLocalBoolCheck
	gMC_LocalVariables_VARS.iLocalBoolCheck2				= iLocalBoolCheck2
	gMC_LocalVariables_VARS.iLocalBoolCheck3				= iLocalBoolCheck3
	gMC_LocalVariables_VARS.iLocalBoolCheck4				= iLocalBoolCheck4
	gMC_LocalVariables_VARS.iLocalBoolCheck5				= iLocalBoolCheck5
	gMC_LocalVariables_VARS.iLocalBoolCheck6				= iLocalBoolCheck6
	gMC_LocalVariables_VARS.iLocalBoolCheck7				= iLocalBoolCheck7
	gMC_LocalVariables_VARS.iLocalBoolCheck8				= iLocalBoolCheck8
	gMC_LocalVariables_VARS.iLocalBoolCheck9				= iLocalBoolCheck9
	gMC_LocalVariables_VARS.iLocalBoolCheck10				= iLocalBoolCheck10
	gMC_LocalVariables_VARS.iLocalBoolCheck11				= iLocalBoolCheck11
	gMC_LocalVariables_VARS.iLocalBoolCheck12				= iLocalBoolCheck12
	gMC_LocalVariables_VARS.iLocalBoolCheck13				= iLocalBoolCheck13
	gMC_LocalVariables_VARS.iLocalBoolCheck14				= iLocalBoolCheck14
	gMC_LocalVariables_VARS.iLocalBoolCheck15				= iLocalBoolCheck15
	gMC_LocalVariables_VARS.iLocalBoolCheck16				= iLocalBoolCheck16
	gMC_LocalVariables_VARS.iLocalBoolCheck17				= iLocalBoolCheck17
	gMC_LocalVariables_VARS.iLocalBoolCheck18				= iLocalBoolCheck18
	gMC_LocalVariables_VARS.iLocalBoolCheck19				= iLocalBoolCheck19
	gMC_LocalVariables_VARS.iLocalBoolCheck20				= iLocalBoolCheck20
	gMC_LocalVariables_VARS.iLocalBoolCheck21				= iLocalBoolCheck21
	gMC_LocalVariables_VARS.iLocalBoolCheck22				= iLocalBoolCheck22
	gMC_LocalVariables_VARS.iLocalBoolCheck23				= iLocalBoolCheck23
	gMC_LocalVariables_VARS.iLocalBoolCheck24				= iLocalBoolCheck24
	gMC_LocalVariables_VARS.iLocalBoolCheck25				= iLocalBoolCheck25
	gMC_LocalVariables_VARS.iLocalBoolCheck26				= iLocalBoolCheck26
	gMC_LocalVariables_VARS.iLocalBoolCheck27				= iLocalBoolCheck27
	gMC_LocalVariables_VARS.iLocalBoolCheck28				= iLocalBoolCheck28
	gMC_LocalVariables_VARS.iLocalBoolCheck29				= iLocalBoolCheck29
	gMC_LocalVariables_VARS.iLocalBoolCheck30				= iLocalBoolCheck30
	gMC_LocalVariables_VARS.iLocalBoolCheck31				= iLocalBoolCheck31
	gMC_LocalVariables_VARS.iLocalBoolCheck32				= iLocalBoolCheck32
	gMC_LocalVariables_VARS.iLocalBoolCheck33				= iLocalBoolCheck33
	gMC_LocalVariables_VARS.iLocalBoolCheck34				= iLocalBoolCheck34
	gMC_LocalVariables_VARS.iLocalBoolCheck35				= iLocalBoolCheck35
	gMC_LocalVariables_VARS.iLocalBoolCheck36				= iLocalBoolCheck36
	
	gMC_LocalVariables_VARS.iLocalBoolResetCheck1			= iLocalBoolResetCheck1
	
	gMC_LocalVariables_VARS.iLastDeliveredContinuityObjectThisRule = iLastDeliveredContinuityObjectThisRule
	
	//The player BD variables
	IF iLocalPart != -1
		gMC_PlayerBD_VARS.iClientLogicStage						= MC_playerBD[iLocalPart].iClientLogicStage
		gMC_PlayerBD_VARS.iGameState							= MC_playerBD[iLocalPart].iGameState
		gMC_PlayerBD_VARS.iCurrentLoc							= MC_playerBD[iLocalPart].iCurrentLoc
		gMC_PlayerBD_VARS.iInAnyLoc 							= MC_playerBD[iLocalPart].iInAnyLoc 
		gMC_PlayerBD_VARS.iLeaveLoc								= MC_playerBD[iLocalPart].iLeaveLoc
		gMC_PlayerBD_VARS.iPedNear								= MC_playerBD[iLocalPart].iPedNear
		gMC_PlayerBD_VARS.iVehNear								= MC_playerBD[iLocalPart].iVehNear
		gMC_PlayerBD_VARS.iObjNear								= MC_playerBD[iLocalPart].iObjNear
		gMC_PlayerBD_VARS.iObjHacking							= MC_playerBD[iLocalPart].iObjHacking
		gMC_PlayerBD_VARS.iPedCarryCount						= MC_playerBD[iLocalPart].iPedCarryCount
		gMC_PlayerBD_VARS.iVehCarryCount						= MC_playerBD[iLocalPart].iVehCarryCount
		gMC_PlayerBD_VARS.iObjCarryCount						= MC_playerBD[iLocalPart].iObjCarryCount
		gMC_PlayerBD_VARS.iNumPlayerDeaths						= MC_playerBD[iLocalPart].iNumPlayerDeaths
		gMC_PlayerBD_VARS.iPlayerScore							= MC_playerBD[iLocalPart].iPlayerScore
		gMC_PlayerBD_VARS.iClientBitSet							= MC_playerBD[iLocalPart].iClientBitSet
		gMC_PlayerBD_VARS.iClientBitSet2 						= MC_playerBD[iLocalPart].iClientBitSet2
		gMC_PlayerBD_VARS.iClientBitSet3						= MC_playerBD[iLocalPart].iClientBitSet3
		gMC_PlayerBD_VARS.iClientBitSet4						= MC_playerBD[iLocalPart].iClientBitSet4
				
		FOR iLoop = 0 TO ciPREREQ_Bitsets-1
			gMC_PlayerBD_VARS.iPrerequisiteBS[iLoop] = MC_playerBD[iLocalPart].iPrerequisiteBS[iLoop]
		ENDFOR
		
		gMC_PlayerBD_VARS.iAbilityPrerequisite_AirStrike = g_FMMC_STRUCT.sPlayerAbilities.sAbilities[PA_AIR_STRIKE].iRequiredPrerequisite
		gMC_PlayerBD_VARS.iAbilityPrerequisite_HeavyLoadout = g_FMMC_STRUCT.sPlayerAbilities.sAbilities[PA_HEAVY_LOADOUT].iRequiredPrerequisite
		gMC_PlayerBD_VARS.iAbilityPrerequisite_HeliBackup = g_FMMC_STRUCT.sPlayerAbilities.sAbilities[PA_HELI_BACKUP].iRequiredPrerequisite
		gMC_PlayerBD_VARS.iAbilityPrerequisite_ReconDrone = g_FMMC_STRUCT.sPlayerAbilities.sAbilities[PA_RECON_DRONE].iRequiredPrerequisite
		gMC_PlayerBD_VARS.iAbilityPrerequisite_SupportSniper = g_FMMC_STRUCT.sPlayerAbilities.sAbilities[PA_SUPPORT_SNIPER].iRequiredPrerequisite
		
	ENDIF
	
	//The server BD globals
	gBG_MC_serverBD_VARS.iServerGameState					= MC_serverBD.iServerGameState
	gBG_MC_serverBD_VARS.iServerBitSet						= MC_serverBD.iServerBitSet
	gBG_MC_serverBD_VARS.iServerBitSet1						= MC_serverBD.iServerBitSet1
	gBG_MC_serverBD_VARS.iServerBitSet2						= MC_serverBD.iServerBitSet2
	gBG_MC_serverBD_VARS.iServerBitSet3						= MC_serverBD.iServerBitSet3
	gBG_MC_serverBD_VARS.iServerBitSet4						= MC_serverBD.iServerBitSet4
	gBG_MC_serverBD_VARS.iServerBitSet5						= MC_serverBD.iServerBitSet5
	gBG_MC_serverBD_VARS.iServerBitSet6						= MC_serverBD.iServerBitSet6
	gBG_MC_serverBD_VARS.iServerBitSet7						= MC_serverBD.iServerBitSet7
	gBG_MC_serverBD_VARS.iServerBitSet8						= MC_serverBD.iServerBitSet8
	gBG_MC_serverBD_VARS.iOptionsMenuBitSet					= MC_serverBD.iOptionsMenuBitSet
	gBG_MC_serverBD_VARS.iNumberOfTeams					 	= MC_serverBD.iNumberOfTeams
	gBG_MC_serverBD_VARS.iNumActiveTeams					= MC_serverBD.iNumActiveTeams
	gBG_MC_serverBD_VARS.iEndCutscene					 	= MC_serverBD.iEndCutscene
	gBG_MC_serverBD_VARS.iServerEventKey					= MC_ServerBD.iSessionScriptEventKey
	
	IF (GET_FRAME_COUNT() % 30) = 0
	
		IF gMC_LocalVariables_VARS.iLocalAssignStagger = 0		
			gBG_MC_serverBD_VARS.iNumPeds = 0
			FOR iLoop = 0 TO FMMC_MAX_PEDS-1
				IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[iLoop])
					gBG_MC_serverBD_VARS.piPeds[gBG_MC_serverBD_VARS.iNumPeds] = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iLoop])
					gBG_MC_serverBD_VARS.iNumPeds++
				ENDIF
			ENDFOR
			gMC_LocalVariables_VARS.iLocalAssignStagger = 1		
			
		ELIF gMC_LocalVariables_VARS.iLocalAssignStagger = 1		
			gBG_MC_serverBD_VARS.iNumVehicles = 0
			FOR iLoop = 0 TO FMMC_MAX_VEHICLES-1
				IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iLoop])
					gBG_MC_serverBD_VARS.viVehicles[gBG_MC_serverBD_VARS.iNumVehicles]			= NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iLoop])
					gBG_MC_serverBD_VARS.iNumVehicles++
				ENDIF
			ENDFOR	
			gMC_LocalVariables_VARS.iLocalAssignStagger = 2
			
		ELIF gMC_LocalVariables_VARS.iLocalAssignStagger = 2
			gBG_MC_serverBD_VARS.iNumObjects = 0
			FOR iLoop = 0 TO FMMC_MAX_NUM_OBJECTS-1
				IF NETWORK_DOES_NETWORK_ID_EXIST(GET_OBJECT_NET_ID(iLoop))
					gBG_MC_serverBD_VARS.oiObjects[gBG_MC_serverBD_VARS.iNumObjects]			= NET_TO_OBJ(GET_OBJECT_NET_ID(iLoop))
					gBG_MC_serverBD_VARS.iNumObjects++
				ENDIF
			ENDFOR			
			gMC_LocalVariables_VARS.iLocalAssignStagger = 0	
			
		ENDIF
	ENDIF
	
	iLoop = 0
	
	REPEAT FMMC_MAX_TEAMS iLoop
		gBG_MC_serverBD_VARS.iCurrentHighestPriority[iLoop]			= MC_serverBD_4.iCurrentHighestPriority[iLoop]
		gBG_MC_serverBD_VARS.iMaxObjectives[iLoop]					= MC_serverBD.iMaxObjectives[iLoop]
		gBG_MC_serverBD_VARS.iNumPlayerRuleHighestPriority[iLoop]	= MC_serverBD.iNumPlayerRuleHighestPriority[iLoop]
		gBG_MC_serverBD_VARS.iNumLocHighestPriority[iLoop]		 	= MC_serverBD.iNumLocHighestPriority[iLoop]
		gBG_MC_serverBD_VARS.iNumPedHighestPriority[iLoop]		 	= MC_serverBD.iNumPedHighestPriority[iLoop]
		gBG_MC_serverBD_VARS.iNumVehHighestPriority[iLoop]		 	= MC_serverBD.iNumVehHighestPriority[iLoop]
		gBG_MC_serverBD_VARS.iNumObjHighestPriority[iLoop]		 	= MC_serverBD.iNumObjHighestPriority[iLoop]
		gBG_MC_serverBD_VARS.iNumPedHighestPriorityHeld[iLoop]		= MC_serverBD.iNumPedHighestPriorityHeld[iLoop]
		gBG_MC_serverBD_VARS.iNumVehHighestPriorityHeld[iLoop]		= MC_serverBD.iNumVehHighestPriorityHeld[iLoop]
		gBG_MC_serverBD_VARS.iNumObjHighestPriorityHeld[iLoop]		= MC_serverBD.iNumObjHighestPriorityHeld[iLoop]
		gBG_MC_serverBD_VARS.iPlayerRuleMissionLogic[iLoop]		 	= MC_serverBD_4.iPlayerRuleMissionLogic[iLoop]
		gBG_MC_serverBD_VARS.iLocMissionLogic[iLoop]		 		= MC_serverBD_4.iLocMissionLogic[iLoop]
		gBG_MC_serverBD_VARS.iPedMissionLogic[iLoop]				= MC_serverBD_4.iPedMissionLogic[iLoop]
		gBG_MC_serverBD_VARS.iVehMissionLogic[iLoop]				= MC_serverBD_4.iVehMissionLogic[iLoop]
		gBG_MC_serverBD_VARS.iObjMissionLogic[iLoop]		 		= MC_serverBD_4.iObjMissionLogic[iLoop]
		gBG_MC_serverBD_VARS.iObjMissionSubLogic[iLoop]		 		= MC_serverBD_4.iObjMissionSubLogic[iLoop]
		gBG_MC_serverBD_VARS.iNumberOfPlayingPlayers[iLoop]		 	= MC_serverBD.iNumberOfPlayingPlayers[iLoop]
		gBG_MC_serverBD_VARS.iTeamScore[iLoop]		 				= MC_serverBD.iTeamScore[iLoop]
		gBG_MC_serverBD_VARS.iScoreOnThisRule[iLoop]		 		= MC_serverBD.iScoreOnThisRule[iLoop]
		gBG_MC_serverBD_VARS.iTeamDeaths[iLoop]		 				= GET_TEAM_DEATHS(iLoop)
		gBG_MC_serverBD_VARS.iCutsceneID[iLoop]						= MC_serverBD.iCutsceneID[iLoop]
	ENDREPEAT
	
ENDPROC

FUNC BOOL IS_RAPPELLING_ALLOWED()
	RETURN IS_BIT_SET(MC_serverBD.iServerBitSet8, SBBOOL8_ALLOW_RAPPELLING_FROM_HELICOPTERS)
ENDFUNC

FUNC BOOL IS_RAPPELLING_FORCED()
	RETURN IS_BIT_SET(MC_serverBD.iServerBitSet8, SBBOOL8_FORCE_RAPPELLING_FROM_HELICOPTERS)
ENDFUNC

FUNC BOOL HAS_DRONE_BEEN_USED()
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_DroneUsageTracking)
		//Not tracking drone usage
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(sMissionLocalContinuityVars.iGeneralLocalTrackingBitset, ciContinuityGenericTrackingBS_DroneUsed)
		RETURN TRUE		
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC PROCESS_CONTROLLER_LIGHT(INT& iTeam, INT& iRule)
	IF NOT IS_PLAYSTATION_PLATFORM()
		EXIT
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSeven[iRule], ciBS_RULE7_ENABLE_LIGHT_BAR_OPTIONS)
    	SET_CONTROL_LIGHT_EFFECT_COLOR(PLAYER_CONTROL, 
		g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sLightBarStruct[iRule].iLBred,
		g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sLightBarStruct[iRule].iLBgreen,
		g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sLightBarStruct[iRule].iLBblue)
		
		IS_BIT_SET(iLocalBoolCheck13, LBOOL13_CONTROL_PAD_COLOUR_SHOULD_BE_RESET)
    ENDIF
ENDPROC

//IF arg1 = TRUE, RETURN arg2, ELSE, return arg3
FUNC BOOL TERNARY_BOOL(BOOL bEval, BOOL bTrueReturn, BOOL bFalseReturn)
	IF bEval
		RETURN bTrueReturn
	ELSE
		RETURN bFalseReturn
	ENDIF
ENDFUNC

FUNC INT COUNT_SET_BITS_IN_RANGE(INT iBitSet, INT iFirstBit = 0, INT iLastBit = 31)
	INT iCount = 0
	
	INT i
	
	FOR i = iFirstBit TO iLastBit
		IF IS_BIT_SET(iBitSet, i)
			iCount++
		ENDIF
	ENDFOR
	
	RETURN iCount
ENDFUNC

FUNC FLOAT MOD_FLOAT(FLOAT fVal, FLOAT fMod)
	#IF IS_DEBUG_BUILD
	IF(fMod < 0.0)
		SCRIPT_ASSERT("MOD_FLOAT - fMod must be >= 0.0")
		RETURN fVal
	ENDIF
	#ENDIF

	IF(fVal >= 0.0)
		IF(fVal <= fMod)
			RETURN fVal
		ELSE // > fMod
			RETURN MOD_FLOAT(fVal - fMod, fMod)
		ENDIF
	ELSE // < 0.0
		RETURN MOD_FLOAT(fVal + fMod, fMod)
	ENDIF
ENDFUNC

FUNC BOOL IS_DAMAGE_FROM_SCRIPT(INT weapType)
	PRINTLN("[LM][IS_DAMAGE_FROM_SCRIPT] - Is the damage type from Script: ", weapType = HASH("WEAPONTYPE_SCRIPT_HEALTH_CHANGE"))
	RETURN weapType = HASH("WEAPONTYPE_SCRIPT_HEALTH_CHANGE")
ENDFUNC

PROC CREATE_COVER_POINTS()	
	INT i = 0
	FOR i = 0 TO (FMMC_MAX_NUM_COVER-1)
		IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedCoverPoints[i].vPos)			
			cpPlacedCoverPoints[i] = ADD_COVER_POINT((g_FMMC_STRUCT_ENTITIES.sPlacedCoverPoints[i].vPos), g_FMMC_STRUCT_ENTITIES.sPlacedCoverPoints[i].fDirection, INT_TO_ENUM(COVERPOINT_USAGE, g_FMMC_STRUCT_ENTITIES.sPlacedCoverPoints[i].iCoverUsage),INT_TO_ENUM(COVERPOINT_HEIGHT, g_FMMC_STRUCT_ENTITIES.sPlacedCoverPoints[i].iCoverHeight),INT_TO_ENUM(COVERPOINT_ARC, g_FMMC_STRUCT_ENTITIES.sPlacedCoverPoints[i].iCoverArc))
			PRINTLN("[RCC MISSION] [SCRIPT INIT] PROCESS_SCRIPT_INIT - CREATE_COVER_POINTS - ADDING COVER POINT ", i, " AT: ",g_FMMC_STRUCT_ENTITIES.sPlacedCoverPoints[i].vPos," direction: ",g_FMMC_STRUCT_ENTITIES.sPlacedCoverPoints[i].fDirection," cover type (left/right) ",g_FMMC_STRUCT_ENTITIES.sPlacedCoverPoints[i].iCoverUsage," cover height ",g_FMMC_STRUCT_ENTITIES.sPlacedCoverPoints[i].iCoverHeight)
		ENDIF
	ENDFOR
ENDPROC

FUNC COVERPOINT_INDEX GET_CLOSEST_COVER_POINT_TO_LOCATION(VECTOR vOrigin, VECTOR &vCoverPos)
	COVERPOINT_INDEX cpIndex
	FLOAT fDist = -1
	
	#IF IS_DEBUG_BUILD
	INT iReturn = -1
	#ENDIF
	
	INT i	
	FOR i = 0 TO (FMMC_MAX_NUM_COVER-1)
		IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedCoverPoints[i].vPos)
			
			PRINTLN("[LM][GET_CLOSEST_COVER_POINT_TO_LOCATION] - vOrigin: ", vOrigin, " vPos: ", g_FMMC_STRUCT_ENTITIES.sPlacedCoverPoints[i].vPos, " Distance: ", VDIST2(vOrigin, g_FMMC_STRUCT_ENTITIES.sPlacedCoverPoints[i].vPos), " Radius: ", POW(3.0, 2.0))
			
			IF VDIST2(vOrigin, g_FMMC_STRUCT_ENTITIES.sPlacedCoverPoints[i].vPos) < POW(3.0, 2.0)
				IF fDist = -1
				OR VDIST2(vOrigin, g_FMMC_STRUCT_ENTITIES.sPlacedCoverPoints[i].vPos) < fDist
					PRINTLN("[LM][GET_CLOSEST_COVER_POINT_TO_LOCATION] - Cover Point i: ", i, " is close to player.")
					cpIndex = cpPlacedCoverPoints[i]
					fDist = VDIST2(vOrigin, g_FMMC_STRUCT_ENTITIES.sPlacedCoverPoints[i].vPos)
					vCoverPos = g_FMMC_STRUCT_ENTITIES.sPlacedCoverPoints[i].vPos
					
					#IF IS_DEBUG_BUILD
					iReturn = i
					#ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	PRINTLN("[LM][GET_CLOSEST_COVER_POINT_TO_LOCATION] - Returning Cover Point: ", iReturn)
	
	RETURN cpIndex
ENDFUNC

FUNC INT GET_CURRENT_HEIST_COMPLETION_STAT_FOR_LEADER(#IF IS_DEBUG_BUILD BOOL bPrint = FALSE #ENDIF)
	
	#IF IS_DEBUG_BUILD
	IF g_iHeistPlaythroughs != -1
		
		#IF IS_DEBUG_BUILD
		IF bPrint
			PRINTLN("GET_CURRENT_HEIST_COMPLETION_STAT_FOR_LEADER - Returning Debug value. g_iHeistPlaythroughs: ", g_iHeistPlaythroughs)
		ENDIF
		#ENDIF
		
		RETURN g_iHeistPlaythroughs
	ENDIF
	#ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_TrackLeaderHeistCompletionStat)
	AND g_TransitionSessionNonResetVars.sMissionContinuityVars.iCurrentHeistCompletionStat != -2
		#IF IS_DEBUG_BUILD
		IF bPrint
			PRINTLN("GET_CURRENT_HEIST_COMPLETION_STAT_FOR_LEADER - Returning contiuinty stat value. iCurrentHeistCompletionStat: ", g_TransitionSessionNonResetVars.sMissionContinuityVars.iCurrentHeistCompletionStat)
		ENDIF
		#ENDIF
		RETURN g_TransitionSessionNonResetVars.sMissionContinuityVars.iCurrentHeistCompletionStat
	ENDIF
	
	INT iPart
	IF DOES_CURRENT_MISSION_USE_GANG_BOSS()
	AND GB_GET_LOCAL_PLAYER_GANG_BOSS() != INVALID_PLAYER_INDEX()	
		IF NETWORK_IS_PLAYER_ACTIVE(GB_GET_LOCAL_PLAYER_GANG_BOSS())
		AND NETWORK_IS_PLAYER_A_PARTICIPANT(GB_GET_LOCAL_PLAYER_GANG_BOSS())
			iPart = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(GB_GET_LOCAL_PLAYER_GANG_BOSS()))
			
			#IF IS_DEBUG_BUILD
			IF bPrint
				PRINTLN("GET_CURRENT_HEIST_COMPLETION_STAT_FOR_LEADER - Returning iPart: ", iPart, " MC_PlayerBD[iPart].iCurrentHeistCompletionStat: ", MC_PlayerBD[iPart].iCurrentHeistCompletionStat)
			ENDIF	
			#ENDIF
			
			IF iPart > -1
				RETURN MC_PlayerBD[iPart].iCurrentHeistCompletionStat
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
			IF bPrint		
				PRINTLN("GET_CURRENT_HEIST_COMPLETION_STAT_FOR_LEADER - But they are not active.")
			ENDIF	
			#ENDIF
		ENDIF
	ELIF NATIVE_TO_INT(NETWORK_GET_HOST_OF_THIS_SCRIPT()) > -1
		
		iPart = NATIVE_TO_INT(NETWORK_GET_HOST_OF_THIS_SCRIPT())

		#IF IS_DEBUG_BUILD
		IF bPrint
			PRINTLN("GET_CURRENT_HEIST_COMPLETION_STAT_FOR_LEADER - No Valid Gang Boss Player")		
			PRINTLN("GET_CURRENT_HEIST_COMPLETION_STAT_FOR_LEADER - Returning iPart: ", iPart, " MC_PlayerBD[iPart].iCurrentHeistCompletionStat: ", MC_PlayerBD[iPart].iCurrentHeistCompletionStat)
		ENDIF	
		#ENDIF
		
		RETURN MC_PlayerBD[iPart].iCurrentHeistCompletionStat
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF bPrint
		PRINTLN("GET_CURRENT_HEIST_COMPLETION_STAT_FOR_LEADER - Default -1")
	ENDIF	
	#ENDIF
	
	RETURN -1
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Prerequisites
// ##### Description: Functions for the Prerequisite system. For example, variables used to track if the player has picked up the required equipment to perform an Interactable.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL ARE_PREREQUISITES_SET_UP_IN_LONG_BITSET(INT &iPrereqs[])
	INT i
	FOR i = 0 TO ciPREREQ_Bitsets - 1		
		IF iPrereqs[i] != 0
			RETURN TRUE
		ENDIF
	ENDFOR
	RETURN FALSE
ENDFUNC

FUNC BOOL ARE_ALL_PREREQUSITES_IN_LONG_BITSET_COMPLETED(INT &iPrereqs[])

	#IF IS_DEBUG_BUILD
	IF bAllFMMCPrereqsSet
	AND NOT IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD2)
		RETURN TRUE
	ENDIF
	#ENDIF

	INT i
	FOR i = 0 TO ciPREREQ_Bitsets - 1
		
		IF iPrereqs[i] = 0  //None to check
			RELOOP
		ENDIF
		
		INT iTempBS = iPrereqs[i] & MC_playerBD[iLocalPart].iPrerequisiteBS[i]
		
		IF iTempBS != iPrereqs[i]
			RETURN FALSE
		ENDIF
		
	ENDFOR

	RETURN TRUE

ENDFUNC

FUNC BOOL ARE_ANY_PREREQUSITES_IN_LONG_BITSET_COMPLETED(INT &iPrereqs[])
	
	#IF IS_DEBUG_BUILD
	IF bAllFMMCPrereqsSet
	AND NOT IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD2)
		RETURN TRUE
	ENDIF
	#ENDIF

	INT i
	FOR i = 0 TO ciPREREQ_Bitsets - 1
		
		IF iPrereqs[i] = 0 //None to check
			RELOOP
		ENDIF
		
		INT iTempBS = iPrereqs[i] & MC_playerBD[iLocalPart].iPrerequisiteBS[i]
		
		IF iTempBS != 0
			RETURN TRUE
		ENDIF
		
	ENDFOR

	RETURN FALSE
	
ENDFUNC

FUNC BOOL ARE_X_PREREQUSITES_IN_LONG_BITSET_COMPLETED(INT &iPrereqs[], INT iX)
	
	#IF IS_DEBUG_BUILD
	IF bAllFMMCPrereqsSet
	AND NOT IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD2)
		RETURN TRUE
	ENDIF
	#ENDIF

	INT iTempBS[ciPREREQ_Bitsets]
	
	INT i
	FOR i = 0 TO ciPREREQ_Bitsets - 1
		
		IF iPrereqs[i] = 0 //None to check
			RELOOP
		ENDIF
		
		iTempBS[i] = iPrereqs[i] & MC_playerBD[iLocalPart].iPrerequisiteBS[i]
		
	ENDFOR
	
	RETURN FMMC_ARE_X_LONG_BITS_SET(iTempBS, iX)

ENDFUNC

FUNC BOOL IS_PREREQUISITE_COMPLETED(INT iPrerequisite = ciPREREQ_None)

	#IF IS_DEBUG_BUILD
	IF bAllFMMCPrereqsSet
	AND NOT IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD2)
		RETURN TRUE
	ENDIF
	#ENDIF
	
	IF iPrerequisite = ciPREREQ_None
		RETURN TRUE
	ENDIF
	
	RETURN FMMC_IS_LONG_BIT_SET(MC_playerBD[iLocalPart].iPrerequisiteBS, iPrerequisite)
ENDFUNC

// This is an optimised method for resetting Prereqs on a rule change.
PROC RESET_PREREQUISITES_COMPLETED_FROM_LONG_BITSET(INT &iPrereqBS[], BOOL bBroadcastToAllPlayers = FALSE)	
	INT iTempBS
	INT iPreReq
	FOR iPreReq = 0 TO ciPREREQ_Bitsets - 1
		PRINTLN("[PreReqs] RESET_PREREQUISITES_COMPLETED_FROM_LONG_BITSET | What we want to clear - iPrereqBS[", iPreReq, "]: ", iPrereqBS[iPreReq])		
		iTempBS = MC_playerBD[iLocalPart].iPrerequisiteBS[iPreReq] & iPrereqBS[iPreReq]
		MC_playerBD[iLocalPart].iPrerequisiteBS[iPreReq] = MC_playerBD[iLocalPart].iPrerequisiteBS[iPreReq] ^ iTempBS
		PRINTLN("[PreReqs] RESET_PREREQUISITES_COMPLETED_FROM_LONG_BITSET | Result - MC_playerBD[iLocalPart].iPrerequisiteBS[", iPreReq, "]: ", MC_playerBD[iLocalPart].iPrerequisiteBS[iPreReq])
	ENDFOR
	
	IF bBroadcastToAllPlayers
		BROADCAST_FMMC_BROADCAST_PREREQS(iPrereqBS, TRUE)
	ENDIF
ENDPROC

// This is an optimised method for setting Prereqs on a rule change.
PROC SET_PREREQUISITES_COMPLETED_FROM_LONG_BITSET(INT &iPrereqBS[], BOOL bBroadcastToAllPlayers = FALSE)	
	INT iPreReq
	FOR iPreReq = 0 TO ciPREREQ_Bitsets - 1
		PRINTLN("[PreReqs] SET_PREREQUISITES_COMPLETED_FROM_LONG_BITSET | What we want to set - iPrereqBS[", iPreReq, "]: ", iPrereqBS[iPreReq])
		MC_playerBD[iLocalPart].iPrerequisiteBS[iPreReq] = MC_playerBD[iLocalPart].iPrerequisiteBS[iPreReq] | iPrereqBS[iPreReq]
		PRINTLN("[PreReqs] SET_PREREQUISITES_COMPLETED_FROM_LONG_BITSET | Result - MC_playerBD[iLocalPart].iPrerequisiteBS[", iPreReq, "]: ", MC_playerBD[iLocalPart].iPrerequisiteBS[iPreReq])
	ENDFOR
	
	IF bBroadcastToAllPlayers
		BROADCAST_FMMC_BROADCAST_PREREQS(iPrereqBS, FALSE)
	ENDIF
ENDPROC

PROC SET_PREREQUISITE_COMPLETED(INT iPrerequisite = ciPREREQ_None, BOOL bBroadcastToAllPlayers = FALSE)

	IF iPrerequisite = ciPREREQ_None
		EXIT
	ENDIF
	
	IF FMMC_IS_LONG_BIT_SET(MC_playerBD[iLocalPart].iPrerequisiteBS, iPrerequisite)
		// Already complete!
		EXIT
	ENDIF
	
	IF bBroadcastToAllPlayers
		BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_CompletePreReq, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, iPrerequisite)
	ENDIF
	
	FMMC_SET_LONG_BIT(MC_playerBD[iLocalPart].iPrerequisiteBS, iPrerequisite)
	PRINTLN("[PreReqs] SET_PREREQUISITE_COMPLETED | Marking prerequisite as completed: ", iPrerequisite)
ENDPROC

PROC RESET_PREREQUISITE_COMPLETED(INT iPrerequisite = ciPREREQ_None, BOOL bBroadcastToAllPlayers = FALSE)

	IF iPrerequisite = ciPREREQ_None
		EXIT
	ENDIF
	
	IF NOT FMMC_IS_LONG_BIT_SET(MC_playerBD[iLocalPart].iPrerequisiteBS, iPrerequisite)
		// Not complete anyway!
		EXIT
	ENDIF
	
	IF bBroadcastToAllPlayers
		BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_ResetPreReq, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, iPrerequisite)
	ENDIF
	
	FMMC_CLEAR_LONG_BIT(MC_playerBD[iLocalPart].iPrerequisiteBS, iPrerequisite)
	PRINTLN("[PreReqs] RESET_PREREQUISITE_COMPLETED | Prerequisite no longer completed: ", iPrerequisite)
ENDPROC

PROC PROCESS_RUNTIME_PREREQ_COUNTER(INT iPreReqCounterIndex, RUNTIME_PREREQ_COUNTER& sPreReqCounterData)
	
	UNUSED_PARAMETER(iPreReqCounterIndex)
	
	IF iPreReqCounterStaggeredBitIndex = 0
		// Reset the count - the loop has restarted
		IF sPreReqCounterData.iPreReqCount != sPreReqCounterData.iPreReqCount_Temp
			sPreReqCounterData.iPreReqCount = sPreReqCounterData.iPreReqCount_Temp
			PRINTLN("[PreReqCounters][PreReqCounter ", iPreReqCounterIndex, "] REGISTER_PREREQ_COUNTER | iPreReqCount is now ", sPreReqCounterData.iPreReqCount)
		ENDIF
		
		sPreReqCounterData.iPreReqCount_Temp = 0
	ENDIF
	
	BOOL bPreReqIsRelevant = FALSE
	
	IF sPreReqCounterData.iDialogueTriggerIndex > -1
		IF IS_LONG_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[sPreReqCounterData.iDialogueTriggerIndex].iDialoguePreReqCounterBS, iPreReqCounterStaggeredBitIndex)
			// The linked dialogue trigger wants to count this PreReq!
			bPreReqIsRelevant = TRUE
		ENDIF
	ENDIF
	
	IF bPreReqIsRelevant
		IF IS_PREREQUISITE_COMPLETED(iPreReqCounterStaggeredBitIndex)
			sPreReqCounterData.iPreReqCount_Temp++
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_RUNTIME_PREREQ_COUNTERS()
	INT iCounter
	FOR iCounter = 0 TO iActiveRuntimePreReqCounters - 1
		PROCESS_RUNTIME_PREREQ_COUNTER(iCounter, sActivePreReqCounterList[iCounter])
	ENDFOR
	
	iPreReqCounterStaggeredBitIndex++
	IF iPreReqCounterStaggeredBitIndex >= ciPREREQ_Max
		iPreReqCounterStaggeredBitIndex = 0
	ENDIF
ENDPROC

FUNC BOOL REGISTER_PREREQ_COUNTER(RUNTIME_PREREQ_COUNTER sPreReqCounterData)
	IF iActiveRuntimePreReqCounters >= ciMAX_RUNTIME_PREREQ_COUNTERS
		#IF IS_DEBUG_BUILD
		PRINTLN("[PreReqCounters] REGISTER_PREREQ_COUNTER | Trying to register too many PreReq counters!!")
		ASSERTLN("[PreReqCounters] REGISTER_PREREQ_COUNTER | Trying to register too many PreReq counters!!")
		ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_TOO_MANY_PRE_REQ_COUNTERS, ENTITY_RUNTIME_ERROR_TYPE_CRITICAL, "Trying to register too many PreReq counters!")
		#ENDIF
		RETURN FALSE
	ENDIF
	
	sActivePreReqCounterList[iActiveRuntimePreReqCounters] = sPreReqCounterData
	PRINTLN("[PreReqCounters][PreReqCounter ", iActiveRuntimePreReqCounters, "] REGISTER_PREREQ_COUNTER | Successfully registered new PreReq counter!")
	iActiveRuntimePreReqCounters++
	
	DEBUG_PRINTCALLSTACK()
	RETURN TRUE
	
ENDFUNC

FUNC BOOL REGISTER_PREREQ_COUNTER_FOR_DIALOGUE_TRIGGER(INT iDialogueTriggerIndex)
	RUNTIME_PREREQ_COUNTER sNewCounter
	sNewCounter.iDialogueTriggerIndex = iDialogueTriggerIndex
	
	RETURN REGISTER_PREREQ_COUNTER(sNewCounter)
ENDFUNC

FUNC INT GET_REGISTERED_PREREQ_COUNTER_FOR_DIALOGUE_TRIGGER(INT iDialogueTriggerIndex)
	INT iCounter
	FOR iCounter = 0 TO iActiveRuntimePreReqCounters - 1
		IF sActivePreReqCounterList[iCounter].iDialogueTriggerIndex = iDialogueTriggerIndex
			RETURN iCounter
		ENDIF
	ENDFOR
	
	RETURN -1
ENDFUNC

FUNC BOOL IS_PREREQ_COUNTER_COMPLETED(INT iPreReqCounterIndex, INT iMaxCount)
	IF iPreReqCounterIndex >= iActiveRuntimePreReqCounters
		RETURN FALSE
	ENDIF
	
	RETURN sActivePreReqCounterList[iPreReqCounterIndex].iPreReqCount >= iMaxCount
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Spectators
// ##### Description: Various helper functions & wrappers to do with specators
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
FUNC BOOL IS_PARTICIPANT_A_NON_HEIST_SPECTATOR( INT iParticipant )
	IF IS_BIT_SET(MC_playerBD[ iParticipant ].iClientBitSet, PBBOOL_ANY_SPECTATOR )
	AND NOT IS_BIT_SET(MC_playerBD[ iParticipant ].iClientBitSet, PBBOOL_HEIST_SPECTATOR )
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_ANY_SPECTATOR(BOOL bSkipSCTVCheck = FALSE)

	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF IS_BIT_SET(iLocalBoolCheck3,LBOOL3_LATE_SPECTATE)
		OR IS_BIT_SET(iLocalBoolCheck3,LBOOL3_WAIT_END_SPECTATE)
		OR IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_ANY_SPECTATOR)
		OR (!bSkipSCTVCheck AND IS_PLAYER_SCTV(LocalPlayer))
		OR USING_HEIST_SPECTATE()
		OR IS_THIS_SPECTATOR_CAM_ACTIVE(g_BossSpecData.specCamData)
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_PLAYER_SPECTATOR_ONLY(PLAYER_INDEX playerId)

	IF IS_PLAYER_IN_PROCESS_OF_JOINING_MISSION_AS_SPECTATOR(playerID)
		RETURN TRUE
	ENDIF
	
	IF DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(playerId)
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_SCTV(playerId)
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

PROC FIND_PREFERRED_SPECTATOR_TARGET()
	
	IF g_FMMC_STRUCT.iDefaultSpectatorTeam != -1
		
		CLEAR_BIT(MPSpecGlobals.SCTVData.iBitSet, SCTV_BIT_ALLOW_SPECTATING_RIVAL_TEAMS)
		MPSpecGlobals.iPreferredSpectatorPlayerID = -1
		INT iPart
		
		PRINTLN("[PLAYER_LOOP] - FIND_PREFERRED_SPECTATOR_TARGET	")
		FOR iPart = 0 TO (GET_MAXIMUM_PLAYERS_ON_MISSION() - 1)
			IF (MC_playerBD[iPart].iteam = g_FMMC_STRUCT.iDefaultSpectatorTeam)
			AND NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPart))
				PLAYER_INDEX tempPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPart))
				IF IS_NET_PLAYER_OK(tempPlayer)
					SET_BIT(MPSpecGlobals.SCTVData.iBitSet, SCTV_BIT_ALLOW_SPECTATING_RIVAL_TEAMS)
					MPSpecGlobals.iPreferredSpectatorPlayerID = NATIVE_TO_INT(tempPlayer)
					CPRINTLN(DEBUG_SPECTATOR, " - MISSION CONTROLLER - FIND_PREFERRED_SPECTATOR_TARGET - SCTV_BIT_ALLOW_SPECTATING_RIVAL_TEAMS")
				ENDIF
			ENDIF
		ENDFOR
		
	ENDIF
	
ENDPROC

FUNC BOOL IS_PLAYER_A_SPECTATOR(PLAYER_INDEX thisPlayer)

	IF ( thisPlayer != INVALID_PLAYER_INDEX() )
		// The important check - we don't want to change the scores if the player that just joined is spectating
		IF IS_PLAYER_SPECTATING( thisPlayer )
		OR IS_PLAYER_IN_PROCESS_OF_JOINING_MISSION_AS_SPECTATOR( thisPlayer )
		OR DID_PLAYER_JOIN_MISSION_AS_SPECTATOR( thisPlayer )
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE

ENDFUNC

FUNC BOOL IS_PARTICIPANT_A_SPECTATOR(INT iParticipant)

	PARTICIPANT_INDEX thisParticipant = INT_TO_PARTICIPANTINDEX( iParticipant )
	IF NETWORK_IS_PARTICIPANT_ACTIVE( thisParticipant )
		PLAYER_INDEX thisPlayer = NETWORK_GET_PLAYER_INDEX( thisParticipant )
		// And if they exist
		IF ( thisPlayer != INVALID_PLAYER_INDEX() )
			// The important check - we don't want to change the scores if the player that just joined is spectating
			IF IS_PLAYER_SPECTATING( thisPlayer )
			OR IS_PLAYER_IN_PROCESS_OF_JOINING_MISSION_AS_SPECTATOR( thisPlayer )
			OR DID_PLAYER_JOIN_MISSION_AS_SPECTATOR( thisPlayer )
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE

ENDFUNC

FUNC INT GET_SPECTATOR_TARGET_PARTICIPANT_OF_PARTICIPANT( INT iSpectatorPart )
	
	INT iReturnTarget = -1
	
	PARTICIPANT_INDEX partSpectator = INT_TO_PARTICIPANTINDEX( iSpectatorPart ) 
	
	IF NETWORK_IS_PARTICIPANT_ACTIVE( partSpectator )
	AND IS_NET_PLAYER_OK( NETWORK_GET_PLAYER_INDEX( partSpectator ) )
		iReturnTarget = MC_playerBD[iSpectatorPart].iPartIAmSpectating
	ENDIF
	
	RETURN iReturnTarget
ENDFUNC


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Native Caching
// ##### Description: Helpers for caching natives to variables
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_CACHING_NETWORK_HOST()
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		bIsLocalPlayerHost = NETWORK_IS_HOST_OF_THIS_SCRIPT()
	ENDIF
	
ENDPROC

PROC PROCESS_CACHING_LOCAL_PARTICIPANT_STATE()
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		iLocalPart = PARTICIPANT_ID_TO_INT()
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF IS_FAKE_MULTIPLAYER_MODE_SET()
		iLocalPart = 0
	ENDIF
	#ENDIF
	
ENDPROC

PROC PROCESS_CACHING_LOCAL_PLAYER_STATE()
	
	PLAYER_INDEX ThisLocalPlayer = PLAYER_ID()
	PED_INDEX ThisLocalPlayerPed = PLAYER_PED_ID()
	
	#IF IS_DEBUG_BUILD
		IF (ThisLocalPlayer != LocalPlayer)
			PRINTLN("PROCESS_CACHING_LOCAL_PLAYER_STATE - LocalPlayer has changed from ", NATIVE_TO_INT(LocalPlayer), " to ", NATIVE_TO_INT(ThisLocalPlayer))
		ENDIF
		IF (ThisLocalPlayerPed != LocalPlayerPed)
			PRINTLN("PROCESS_CACHING_LOCAL_PLAYER_STATE - LocalPlayerPed has changed from ", NATIVE_TO_INT(LocalPlayerPed), " to ", NATIVE_TO_INT(ThisLocalPlayerPed))
		ENDIF
	#ENDIF
	
	LocalPlayer = ThisLocalPlayer
	LocalPlayerPed = ThisLocalPlayerPed
	playerPedToUse = LocalPlayerPed // Assigned properly in PROCESS_SPECTATOR_MODES_AND_LOCAL_PLAYER_VARIABLES, but only after game state running.
	
	IF IS_NET_PLAYER_OK(LocalPlayer)
		bLocalPlayerOK = TRUE
		bLocalPlayerPedOK = NOT IS_PED_INJURED(LocalPlayerPed)
	ELSE
		bLocalPlayerOK = FALSE
		bLocalPlayerPedOK = FALSE
	ENDIF

	IF IS_PLAYER_SCTV(LocalPlayer)
		bIsSCTV = TRUE
		bIsAnySpectator = TRUE
	ELSE
		bIsSCTV = FALSE
		bIsAnySpectator = IS_LOCAL_PLAYER_ANY_SPECTATOR(TRUE)
	ENDIF
	
	IF !bIsAnySpectator
		vLocalPlayerPosition = GET_ENTITY_COORDS(LocalPlayerPed, FALSE)
	ELSE
		vLocalPlayerPosition = <<0,0,0>>
	ENDIF
	
ENDPROC

PROC PROCESS_CACHING_LOCAL_PLAYER_INTERIOR_STATE()
	
	LocalPlayerCurrentInterior = GET_INTERIOR_FROM_ENTITY(LocalPlayerPed)
	IF IS_VALID_INTERIOR(LocalPlayerCurrentInterior)
		IF bLocalPlayerPedOk
			GET_INTERIOR_LOCATION_AND_NAMEHASH(LocalPlayerCurrentInterior, vLocalPlayerCurrentInteriorPos, iLocalPlayerCurrentInteriorHash)
			iLocalPlayerCurrentRoomKey = GET_ROOM_KEY_FROM_ENTITY(LocalPlayerPed)
		ENDIF
	ELSE
		LocalPlayerCurrentInterior = NULL
		vLocalPlayerCurrentInteriorPos = <<0,0,0>>
		iLocalPlayerCurrentInteriorHash = -1
		iLocalPlayerCurrentRoomKey = -1
	ENDIF
		
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Player State Alteration Wrappers
// ##### Description: Wrappers for functions that alter a cached player state which require re-caching of natives
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC FMMC_NETWORK_RESURRECT_LOCAL_PLAYER(VECTOR vPos, FLOAT fHeading, INT iInvicibilityTime = 0, BOOL bLeaveDeadPed = TRUE, BOOL bUnpauseRenderPhase = TRUE)		
	
	NETWORK_RESURRECT_LOCAL_PLAYER(vPos, fHeading, iInvicibilityTime, bLeaveDeadPed, bUnpauseRenderPhase)
	
	PRINTLN("[NATIVE_RECACHE] FMMC_NETWORK_RESURRECT_LOCAL_PLAYER - Recaching Local Player State")
	
	PROCESS_CACHING_LOCAL_PLAYER_STATE()
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Teams & Rules
// ##### Description: Various helper functions & wrappers to do with teams & rules
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC INT GET_NUM_TEAMS_FROM_PARTICIPANTS_FOR_JIP_PLAYER()
	
	INT iLoop, iReturn, iGbdSlot
	PARTICIPANT_INDEX partID
	PLAYER_INDEX playerID
	
	PRINTLN("[PLAYER_LOOP] - GET_NUM_TEAMS_FROM_PARTICIPANTS_FOR_JIP_PLAYER")
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iLoop
		
		partID = INT_TO_PARTICIPANTINDEX(iLoop)
		
		//If they are active
		IF NETWORK_IS_PARTICIPANT_ACTIVE(partID)
			//Get player ID
			playerID = NETWORK_GET_PLAYER_INDEX(partID)
			
			//Not SCTV
			IF NOT IS_PLAYER_SCTV(playerID)
			AND NOT IS_THIS_PLAYER_NOT_GOING_TO_TAKE_PART_IN_THIS_CORONA(playerID)
			AND NOT IS_THIS_PLAYER_ON_JIP_WARNING(playerID)
			AND NOT DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(playerID)
				iGbdSlot = NATIVE_TO_INT(playerID)
				IF GlobalplayerBD_FM[iGbdSlot].sClientCoronaData.iTeamChosen + 1 > iReturn 
					iReturn = GlobalplayerBD_FM[iGbdSlot].sClientCoronaData.iTeamChosen + 1
					PRINTLN("[RCC MISSION] GET_NUM_TEAMS_FROM_PARTICIPANTS_FOR_JIP_PLAYER CLIENT - iReturn = ", iReturn)
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF g_FMMC_STRUCT.iNumberOfTeams > iReturn
		iReturn = g_FMMC_STRUCT.iNumberOfTeams
		PRINTLN("[RCC MISSION] GET_NUM_TEAMS_FROM_PARTICIPANTS_FOR_JIP_PLAYER CLIENT - iReturn = g_FMMC_STRUCT.iNumberOfTeams = ", iReturn)
	#IF IS_DEBUG_BUILD
	ELIF iReturn > g_FMMC_STRUCT.iNumberOfTeams
		PRINTLN("[RCC MISSION] GET_NUM_TEAMS_FROM_PARTICIPANTS_FOR_JIP_PLAYER CLIENT - iReturn > g_FMMC_STRUCT.iNumberOfTeams!")
	#ENDIF
	ENDIF
	
	RETURN iReturn
	
ENDFUNC

FUNC INT GET_NUMBER_OF_TEAMS_FOR_PRE_GAME()

	INT iNumberOfTeams
	IF IS_FAKE_MULTIPLAYER_MODE_SET()
		iNumberOfTeams = g_FMMC_STRUCT.iTestNumberOfTeams
	ELIF DID_I_JOIN_MISSION_AS_SPECTATOR()
		iNumberOfTeams = GET_NUM_TEAMS_FROM_PARTICIPANTS_FOR_JIP_PLAYER()
	ELSE
		iNumberOfTeams = g_FMMC_STRUCT.iNumberOfTeams  
	ENDIF
	IF iNumberOfTeams < 1
		iNumberOfTeams = 1
	ENDIF
	IF iNumberOfTeams > FMMC_MAX_TEAMS
		iNumberOfTeams = FMMC_MAX_TEAMS
	ENDIF
	
	RETURN iNumberOfTeams
ENDFUNC

FUNC INT GET_LOCAL_PLAYER_TEAM(BOOL bUsePartToUse = FALSE)
	IF bUsePartToUse
		RETURN MC_playerBD[iPartToUse].iTeam
	ENDIF
	
	RETURN MC_playerBD[iLocalPart].iTeam
ENDFUNC

FUNC INT GET_STARTING_PLAYERS_FOR_TEAM(INT iTeam)
	INT i
	INT iCount
	PRINTLN("[PLAYER_LOOP] - GET_STARTING_PLAYERS_FOR_TEAM")
	FOR i = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION() - 1
		IF MC_playerBD[i].iTeam = iTeam
			IF (NOT IS_BIT_SET(MC_playerBD[i].iClientBitSet,PBBOOL_ANY_SPECTATOR) OR IS_BIT_SET(MC_playerBD[i].iClientBitSet,PBBOOL_HEIST_SPECTATOR))
			AND NOT IS_BIT_SET(MC_playerBD[i].iClientBitSet,PBBOOL_PLAYER_FAIL)
				PARTICIPANT_INDEX tempPart = INT_TO_PARTICIPANTINDEX(i)
				IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
					PLAYER_INDEX tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
					IF NOT IS_PLAYER_SCTV(tempPlayer)
						iCount++
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	RETURN iCount
ENDFUNC

FUNC BOOL IS_TEAM_COOP_MISSION()
	INT iteam
	INT iteamrepeat

	IF MC_serverBD.iNumberOfTeams < 2
		RETURN TRUE
	ENDIF
	
	FOR iteam = 0 TO (FMMC_MAX_TEAMS-1)
		FOR iteamrepeat = 0 TO (FMMC_MAX_TEAMS-1)
			IF NOT DOES_TEAM_LIKE_TEAM(iteam,iteamrepeat)
				RETURN FALSE
			ENDIF
		ENDFOR
	ENDFOR
	
	RETURN TRUE

ENDFUNC

FUNC BOOL WAS_TEAM_EVER_ACTIVE(INT iteam)
	
	IF iteam = 0
		IF IS_BIT_SET(MC_serverbd.iServerBitSet1, SBBOOL1_TEAM_0_WAS_ACTIVE)
			RETURN TRUE
		ENDIF
	ELIF iteam = 1
		IF IS_BIT_SET(MC_serverbd.iServerBitSet1, SBBOOL1_TEAM_1_WAS_ACTIVE)
			RETURN TRUE
		ENDIF
	ELIF iteam = 2
		IF IS_BIT_SET(MC_serverbd.iServerBitSet1, SBBOOL1_TEAM_2_WAS_ACTIVE)
			RETURN TRUE
		ENDIF
	ELIF iteam = 3
		IF IS_BIT_SET(MC_serverbd.iServerBitSet1, SBBOOL1_TEAM_3_WAS_ACTIVE)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL DID_TEAM_PASS_OBJECTIVE(INT iteam,INT ipriority)

	IF ipriority < FMMC_MAX_RULES
		IF IS_BIT_SET(MC_serverBD.iServerPassBitSet[iteam],ipriority)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL IS_RULE_FINAL_RULE(INT rule)

	IF rule >= MC_serverBD.iMaxObjectives[mc_playerBD[iPartToUse].iteam] 
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

FUNC BOOL DO_ALL_TEAMS_LIKE_EACH_OTHER()

	INT iteam,iteam2

	FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
		FOR iteam2 = 0 TO (MC_serverBD.iNumberOfTeams-1)
			IF iteam !=iteam2
				IF NOT DOES_TEAM_LIKE_TEAM(iteam,iteam2)
					RETURN FALSE
				ENDIF
			ENDIF
		ENDFOR
	ENDFOR
	
	RETURN TRUE

ENDFUNC

FUNC INT GET_TEAM_CURRENT_RULE(INT iTeam, BOOL bUseSafeguard = TRUE)
	IF NOT IS_TEAM_INDEX_VALID(iTeam)
		ASSERTLN("GET_TEAM_CURRENT_RULE | iTeam is ", iTeam, "!!!!!!!!")
		RETURN 0
	ENDIF
	
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	IF bUseSafeguard
		INT iTempRule = iRule
		iRule = CLAMP_INT(iRule, 0, FMMC_MAX_RULES - 1)
		
		IF iRule != iTempRule
			PRINTLN("GET_TEAM_CURRENT_RULE | Team ", iTeam, "'s Rule is ", iTempRule, ", clamping it to a safe value")
		ENDIF
	ENDIF
	
	RETURN iRule
ENDFUNC
FUNC INT GET_LOCAL_PLAYER_CURRENT_RULE(BOOL bUsePartToUse = FALSE, BOOL bUseSafeguard = TRUE)
	RETURN GET_TEAM_CURRENT_RULE(GET_LOCAL_PLAYER_TEAM(bUsePartToUse), bUseSafeguard)
ENDFUNC

FUNC BOOL HAVE_PLAYERS_PROGRESSED_TO_RULE(INT iRule, INT iTeam)
	IF iTeam = -1
		// Check if any team has progressed to the given rule
		INT iTempTeam
		FOR iTempTeam = 0 TO FMMC_MAX_TEAMS - 1
			IF NOT WAS_TEAM_EVER_ACTIVE(iTempTeam)
				PRINTLN("HAVE_PLAYERS_PROGRESSED_TO_RULE | Team ", iTempTeam, " was never active")
				RELOOP
			ENDIF
			
			IF HAVE_PLAYERS_PROGRESSED_TO_RULE(iRule, iTempTeam)
				RETURN TRUE
			ENDIF
		ENDFOR
	ELSE
		// Check if this particular team has progressed to the given rule
		IF GET_TEAM_CURRENT_RULE(iTeam) >= iRule
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_TEAM_HAVE_NEW_PRIORITY_THIS_FRAME(INT iTeam)
	IF NOT IS_TEAM_INDEX_VALID(iTeam)
		ASSERTLN("DOES_TEAM_HAVE_NEW_PRIORITY_THIS_FRAME | iTeam is ", iTeam, "!!!!!!!!")
		RETURN FALSE
	ENDIF
	
	RETURN IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam)
ENDFUNC
FUNC BOOL DOES_LOCAL_PLAYER_HAVE_NEW_PRIORITY_THIS_FRAME()
	RETURN DOES_TEAM_HAVE_NEW_PRIORITY_THIS_FRAME(MC_playerBD[iPartToUse].iTeam)
ENDFUNC

FUNC BOOL DOES_TEAM_HAVE_NEW_MIDPOINT_THIS_FRAME(INT iTeam)
	IF NOT IS_TEAM_INDEX_VALID(iTeam)
		ASSERTLN("DOES_TEAM_HAVE_NEW_MIDPOINT_THIS_FRAME | iTeam is ", iTeam, "!!!!!!!!")
		RETURN FALSE
	ENDIF
	
	RETURN IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_MIDPOINT_THIS_FRAME + iTeam)
ENDFUNC
FUNC BOOL DOES_LOCAL_PLAYER_HAVE_NEW_MIDPOINT_THIS_FRAME()	
	RETURN DOES_TEAM_HAVE_NEW_MIDPOINT_THIS_FRAME(MC_playerBD[iPartToUse].iTeam)
ENDFUNC

FUNC BOOL DOES_ANY_TEAM_HAVE_NEW_PRIORITY_THIS_FRAME(BOOL bAlsoCheckMidpoint = FALSE)
	
	IF IS_BIT_SET(iLocalBoolCheck7, LBOOL7_A_TEAM_HAS_A_NEW_PRIORITY_THIS_FRAME)
		RETURN TRUE
	ENDIF
	
	IF bAlsoCheckMidpoint
		IF IS_BIT_SET(iLocalBoolCheck7, LBOOL7_A_TEAM_HAS_NEW_MIDPOINT_THIS_FRAME)
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
	
ENDFUNC

PROC SET_TEAM_HAS_NEW_PRIORITY_THIS_FRAME(INT iTeam)
	SET_BIT(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam)
	SET_BIT(iLocalBoolCheck7, LBOOL7_A_TEAM_HAS_A_NEW_PRIORITY_THIS_FRAME)
ENDPROC

PROC SET_TEAM_HAS_NEW_MIDPOINT_THIS_FRAME(INT iTeam)
	SET_BIT(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_MIDPOINT_THIS_FRAME + iTeam)
	SET_BIT(iLocalBoolCheck7, LBOOL7_A_TEAM_HAS_NEW_MIDPOINT_THIS_FRAME)
ENDPROC

FUNC BOOL HAS_TEAM_FAILED(INT iTeam, BOOL bCheckRequests = FALSE)
	
	IF iTeam < 0
	OR iTeam >= FMMC_MAX_TEAMS
		ASSERTLN("HAS_TEAM_FAILED - Invalid Team: ", iTeam)
		RETURN FALSE
	ENDIF
	
	IF bCheckRequests
		IF MC_serverBD.iTeamFailRequests[iTeam] > 0
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN IS_BIT_SET(MC_serverBD.iServerBitSet, SSBOOL_TEAM0_FAILED + iTeam) 
	
ENDFUNC

FUNC BOOL HAS_TEAM_FINISHED(INT iteam)
	
	SWITCH iteam
		
		CASE 0
			IF IS_BIT_SET(MC_serverBD.iServerBitSet,SSBOOL_TEAM0_FINISHED)
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE 1
			IF IS_BIT_SET(MC_serverBD.iServerBitSet,SSBOOL_TEAM1_FINISHED)
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE 2
			IF IS_BIT_SET(MC_serverBD.iServerBitSet,SSBOOL_TEAM2_FINISHED)
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE 3
			IF IS_BIT_SET(MC_serverBD.iServerBitSet,SSBOOL_TEAM3_FINISHED)
				RETURN TRUE
			ENDIF
		BREAK
		
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL HAVE_ALL_TEAMS_FINISHED()
	
	IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet,SSBOOL_TEAM0_FINISHED)
		RETURN FALSE
	ENDIF
	
	IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet,SSBOOL_TEAM1_FINISHED)
		RETURN FALSE
	ENDIF
	
	IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet,SSBOOL_TEAM2_FINISHED)
		RETURN FALSE
	ENDIF
	
	IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet,SSBOOL_TEAM3_FINISHED)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL ALL_TEAMS_FAILED()

	INT iTeam
	
	FOR iTeam = 0 TO MC_ServerBD.iNumberOfTeams-1
		IF NOT HAS_TEAM_FAILED(iTeam)
			RETURN FALSE
		ENDIF
	ENDFOR

	RETURN TRUE

ENDFUNC

FUNC BOOL ALL_OTHER_HOSTILE_TEAMS_FAILED(INT iTeamToCheck)

	INT iteam
	INT iNumHostileTeams
	
	IF MC_serverBD.iNumberOfTeams = 1
		PRINTLN("[RCC MISSION] OTHERS_HOSTILE_TEAMS_FAILED: FALSE MC_serverBD.iNumberOfTeams = 1")
		RETURN FALSE
	ENDIF
	
	FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams -1)
		IF WAS_TEAM_EVER_ACTIVE(iTeam)
			IF NOT DOES_TEAM_LIKE_TEAM(iTeamToCheck, iTeam)
				iNumHostileTeams++
				IF NOT HAS_TEAM_FAILED(iTeam)
					PRINTLN("[RCC MISSION] OTHERS_HOSTILE_TEAMS_FAILED: FALSE IF NOT HAS_TEAM_FAILED(iTeam)",iTeam)
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	IF iNumHostileTeams = 0 
		PRINTLN("[RCC MISSION] OTHERS_HOSTILE_TEAMS_FAILED: FALSE NO HOSTILE TEAMS)")
		RETURN FALSE
	ENDIF
	PRINTLN("[RCC MISSION] OTHERS_HOSTILE_TEAMS_FAILED: TRUE")
	RETURN TRUE

ENDFUNC

FUNC BOOL IS_TEAM_SET_AS_CRITICAL(INT iteam)
	
	#IF IS_DEBUG_BUILD
	IF NOT IS_TRANSITION_DEBUG_LAUNCH_VAR_SET()
	#ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iteam].iTeamBitset, ciBS_TEAM_MISSION_CRITIAL)
		RETURN TRUE
	ENDIF
	#IF IS_DEBUG_BUILD
	ENDIF
	#ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_TEAM_ACTIVE(INT iTeam)

	#IF IS_DEBUG_BUILD
	IF GlobalServerBD_DM.bOnePlayerDeathmatch
		PRINTLN("IS_TEAM_ACTIVE - bOnePlayerDeathmatch is TRUE! TEAMS ARE ALWAYS COUNTED AS ACTIVE!")
		RETURN TRUE
	ENDIF
	#ENDIF
	
	IF IS_BIT_SET(MC_serverbd.iServerBitSet, SBBOOL_TEAM_0_ACTIVE + iTeam)
		IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SSBOOL_TEAM0_FINISHED + iTeam)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC INT GET_NUM_ACTIVE_TEAMS()
	INT i, iActiveTeams
	
	FOR i = 0 TO FMMC_MAX_TEAMS

		IF IS_BIT_SET(MC_serverbd.iServerBitSet, SBBOOL_TEAM_0_ACTIVE + i)
			IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet,SSBOOL_TEAM0_FINISHED + i)
				iActiveTeams++
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN iActiveTeams
	
ENDFUNC

//[KH][SSDS] check if only 1 team alive to pass mission
/// PURPOSE: Checks how many teams are still playing and 
///    
/// RETURNS: TRUE if only 1 team is remaining in the game
///    
FUNC BOOL IS_ONLY_ONE_TEAM_ALIVE(INT& iWinningTeam)
	
	INT iTeamBS, i, iTeamsStillPlaying, winningTeam
	
	PRINTLN("[PLAYER_LOOP] - IS_ONLY_ONE_TEAM_ALIVE")
	FOR i = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION() - 1
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
			PLAYER_INDEX tempPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
			IF IS_NET_PLAYER_OK(tempPlayer, TRUE)
			AND IS_ENTITY_ALIVE(GET_PLAYER_PED(tempPlayer))
			AND NOT IS_PARTICIPANT_A_SPECTATOR(i)
				IF NOT IS_BIT_SET(iTeamBS, MC_playerBD[i].iteam)
					PRINTLN("[KH] IS_ONLY_ONE_TEAM_ALIVE -  team ", MC_playerBD[i].iteam, " is still going")
					SET_BIT(iTeamBS, MC_playerBD[i].iteam)
					iTeamsStillPlaying++
					winningTeam = MC_playerBD[i].iteam
					IF iTeamsStillPlaying > 1
						PRINTLN("[KH] IS_ONLY_ONE_TEAM_ALIVE - Nope")
						BREAKLOOP
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	IF iTeamsStillPlaying <= 1
		iWinningTeam = winningTeam
		RETURN TRUE
	ELSE
		iWinningTeam = -1
		RETURN FALSE
	ENDIF
	
ENDFUNC

FUNC BOOL DOES_TEAM_HAVE_ANY_PLAYERS_DEAD(INT iTeam)
	INT i, iPlayersToCheck, iPlayersChecked
	iPlayersToCheck = MC_serverBD.iNumberOfPlayingPlayers[iTeam]
	PRINTLN("[PLAYER_LOOP] - DOES_TEAM_HAVE_ANY_PLAYERS_DEAD")
	FOR i = 0 TO (GET_MAXIMUM_PLAYERS_ON_MISSION() - 1)
		IF (NOT(IS_BIT_SET(MC_playerBD[i].iClientBitSet, PBBOOL_ANY_SPECTATOR)))
		AND NOT IS_BIT_SET(MC_playerBD[i].iClientBitSet,PBBOOL_PLAYER_FAIL)
		AND IS_BIT_SET(MC_playerBD[i].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
		AND NOT IS_BIT_SET(MC_playerBD[i].iClientBitSet, PBBOOL_FINISHED)
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
			AND MC_playerBD[i].iteam = iTeam
				iPlayersChecked++
				PLAYER_INDEX tempPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
				IF NOT IS_ENTITY_ALIVE(GET_PLAYER_PED(tempPlayer))
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
		IF iPlayersChecked >= iPlayersToCheck
			BREAKLOOP
		ENDIF
	ENDFOR
	RETURN FALSE
ENDFUNC

FUNC INT GET_PRE_ASSIGNMENT_PLAYER_TEAM()
	
	INT iTeam
	
	IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()		
		DEBUG_PRINTCALLSTACK()
		
		IF GET_PLAYER_TEAM(GET_PLAYER_INDEX()) > -1
			PRINTLN("[RCC MISSION] GET_PRE_ASSIGNMENT_PLAYER_TEAM - IS_FAKE_MULTIPLAYER_MODE_SET() = FALSE, Using GET_PLAYER_TEAM(): ", GET_PLAYER_TEAM(localPlayer), " as a team has been assigned now... (Implemented because of team rebalancing) url:bugstar:3583710")
			iTeam = GET_PLAYER_TEAM(GET_PLAYER_INDEX())
		ELSE
			iTeam = GlobalplayerBD_FM[NATIVE_TO_INT(GET_PLAYER_INDEX())].sClientCoronaData.iTeamChosen
			PRINTLN("[RCC MISSION] GET_PRE_ASSIGNMENT_PLAYER_TEAM - IS_FAKE_MULTIPLAYER_MODE_SET() = FALSE, setting iTeam = GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].sClientCoronaData.iTeamChosen : ",GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].sClientCoronaData.iTeamChosen) 
		ENDIF
	ELSE
		iTeam = g_FMMC_STRUCT.iTestMyTeam
		PRINTLN("[RCC MISSION] GET_PRE_ASSIGNMENT_PLAYER_TEAM - IS_FAKE_MULTIPLAYER_MODE_SET() = TRUE, setting MC_playerBD[iLocalPart].iteam = g_FMMC_STRUCT.iTestMyTeam")
	ENDIF
	
	PRINTLN("[RCC MISSION] GET_PRE_ASSIGNMENT_PLAYER_TEAM - PLAYER TEAM: ",iTeam)
	
	IF iTeam < 0
		PRINTLN("GET_PRE_ASSIGNMENT_PLAYER_TEAM - TEAM IS COMING THROUGH AS LESS THAN 0, ADD A BUG")
		SCRIPT_ASSERT("GET_PRE_ASSIGNMENT_PLAYER_TEAM - TEAM IS COMING THROUGH AS LESS THAN 0, ADD A BUG")
		iTeam = 0
	ENDIF
	
	IF iTeam >=FMMC_MAX_TEAMS
		IF NOT IS_PLAYER_SCTV(GET_PLAYER_INDEX(), TRUE)
			PRINTLN("GET_PRE_ASSIGNMENT_PLAYER_TEAM - TEAM IS COMING THROUGH MORE THAN FMMC_MAX_TEAMS, ADD A BUG")
			SCRIPT_ASSERT("GET_PRE_ASSIGNMENT_PLAYER_TEAM - TEAM IS COMING THROUGH MORE THAN FMMC_MAX_TEAMS, ADD A BUG")
		ENDIF
		iTeam = 3
	ENDIF
	
	RETURN iTeam
	
ENDFUNC

FUNC INT GET_PLAYER_COUNT_FOR_CRITICAL_CHECK(INT iTeam)
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitset4, ciBS4_TEAM_CRITICAL_COUNT_INCLUDE_SPECTATORS)
		RETURN MC_serverBD.iNumberOfPlayersAndSpectators[iTeam]
	ELSE
		RETURN MC_serverBD.iNumberOfPlayingPlayers[iTeam]
	ENDIF
ENDFUNC

FUNC INT GET_CRITICAL_MINIMUM_FOR_TEAM(INT iTeam)
	IF g_FMMC_STRUCT.iCriticalMinimumForTeam[iTeam] = ciPLAYER_NUM_VARIABLE_CRITICAL_MIN
		IF ((MC_ServerBD.iNumStartingPlayers[iTeam] < 2 AND MC_ServerBD.iNumberOfTeams = 1)
		OR (MC_ServerBD.iNumberOfTeams > 1 AND MC_ServerBD.iNumStartingPlayers[iTeam] < 1))
		AND NOT IS_FAKE_MULTIPLAYER_MODE_SET()
			RETURN 2
		ENDIF
		RETURN MC_ServerBD.iNumStartingPlayers[iTeam]
	ELSE
		RETURN g_FMMC_STRUCT.iCriticalMinimumForTeam[iTeam]
	ENDIF
ENDFUNC

FUNC BOOL DOES_OPPOSING_TEAMS_HAVE_ANY_LEFT_ALIVE()
	INT iPart
	PRINTLN("[PLAYER_LOOP] - DOES_OPPOSING_TEAMS_HAVE_ANY_LEFT_ALIVE")
	FOR iPart = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		IF NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPart)) != INVALID_PLAYER_INDEX()
			IF NOT IS_PLAYER_SPECTATOR_ONLY(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPart)))
				IF MC_PlayerBD[iPart].iteam != MC_playerBD[iLocalPart].iteam
				AND NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPart))
				AND IS_NET_PLAYER_OK(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPart)))
				AND NOT DOES_TEAM_LIKE_TEAM(MC_PlayerBD[iPart].iteam, MC_playerBD[iLocalPart].iteam)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	RETURN FALSE
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Local Player Helpers
// ##### Description: Various helper functions & wrappers to do the state of the local player.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC KILL_LOCAL_PLAYER_WITH_PLAYER_INDEX_AS_INSTIGATOR(PLAYER_INDEX piPlayer)
	PRINTLN("KILL_LOCAL_PLAYER_WITH_PLAYER_INDEX_AS_INSTIGATOR - local player is killing themself.")
	CLEAR_ENTITY_LAST_DAMAGE_ENTITY(localPlayerPed)
	SET_ENTITY_HEALTH(LocalPlayerPed, 0)
	BROADCAST_FMMC_EVENT_ENTITY_DAMAGED_EVENT_FAKE(iLocalPart, LocalPlayer, piPlayer, NULL, NULL, 15.0, TRUE, ENUM_TO_INT(WEAPONTYPE_FALL), FALSE, FALSE, FALSE)
	SET_KILL_STRIP_OVER_RIDE_PLAYER_ID(piPlayer)
ENDPROC

FUNC BOOL IS_LOCAL_PLAYER_PED_GETTING_IN_OR_OUT_OF_ANY_VEHICLE()
	IF IS_PED_GETTING_INTO_A_VEHICLE(localPlayerPed)
	OR GET_SCRIPT_TASK_STATUS(localPlayerPed, SCRIPT_TASK_ENTER_VEHICLE) = PERFORMING_TASK
	OR GET_SCRIPT_TASK_STATUS(localPlayerPed, SCRIPT_TASK_ENTER_VEHICLE) = WAITING_TO_START_TASK
	OR GET_SCRIPT_TASK_STATUS(localPlayerPed, SCRIPT_TASK_LEAVE_VEHICLE) = PERFORMING_TASK
	OR GET_SCRIPT_TASK_STATUS(localPlayerPed, SCRIPT_TASK_LEAVE_VEHICLE) = WAITING_TO_START_TASK
	OR GET_IS_TASK_ACTIVE(localPlayerPed, CODE_TASK_EXIT_VEHICLE)	
	OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_ENTER)
	OR IS_CONTROL_JUST_RELEASED(PLAYER_CONTROL, INPUT_ENTER)
	OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_EXIT)
	OR IS_CONTROL_JUST_RELEASED(PLAYER_CONTROL, INPUT_VEH_EXIT)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_LOCAL_PLAYER_FAILED()

	IF IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_PLAYER_FAIL)
		RETURN TRUE
	ENDIF

	RETURN FALSE
	
ENDFUNC

FUNC BOOL HAS_LOCAL_PLAYER_FINISHED_OR_REACHED_END()
	IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_FINISHED)
		RETURN TRUE
	ENDIF
	IF HAS_LOCAL_PLAYER_FAILED()
		RETURN TRUE
	ENDIF
	IF IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
	OR GET_MC_SERVER_GAME_STATE() = GAME_STATE_END
		RETURN TRUE
	ENDIF
	IF HAS_TEAM_FINISHED(MC_playerBD[iLocalPart].iteam)
	OR HAS_TEAM_FAILED(MC_playerBD[iLocalPart].iteam)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

// ##### ------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Content Specific Lives  ----------------------------------------------------------------------------------
// ##### Description: Functions related to content specific lives settings.  ----------------------------------------------------
// ##### ------------------------------------------------------------------------------------------------------------------------
// ##### ------------------------------------------------------------------------------------------------------------------------

FUNC INT GET_PLAYER_NUM_VARIABLE_LIVES_OVERRIDE_NORMAL(BOOL bLowPlayerCount)

	#IF FEATURE_HEIST_ISLAND	
	IF HEIST_ISLAND_FLOW_IS_CURRENT_MISSION_HEIST_ISLAND_FINALE()
		IF bLowPlayerCount
			PRINTLN("[VariableLives] GET_PLAYER_NUM_VARIABLE_LIVES_OVERRIDE_HARD - Using Island Override: ", g_sMPTunables.iIH_FINALE_PLAYER_NUM_VARIABLE_LIVES_NORMAL_LOW)
			RETURN g_sMPTunables.iIH_FINALE_PLAYER_NUM_VARIABLE_LIVES_NORMAL_LOW
		ELSE
			PRINTLN("[VariableLives] GET_PLAYER_NUM_VARIABLE_LIVES_OVERRIDE_HARD - Using Island Override: ", g_sMPTunables.iIH_FINALE_PLAYER_NUM_VARIABLE_LIVES_NORMAL)
			RETURN g_sMPTunables.iIH_FINALE_PLAYER_NUM_VARIABLE_LIVES_NORMAL
		ENDIF
	ENDIF
	#ENDIF
	
	IF bLowPlayerCount
		RETURN 1
	ELSE
		RETURN 2
	ENDIF
	
ENDFUNC

FUNC INT GET_PLAYER_NUM_VARIABLE_LIVES_OVERRIDE_HARD()
	#IF FEATURE_HEIST_ISLAND	
	IF HEIST_ISLAND_FLOW_IS_CURRENT_MISSION_HEIST_ISLAND_FINALE()
		PRINTLN("[VariableLives] GET_PLAYER_NUM_VARIABLE_LIVES_OVERRIDE_HARD - Using Island Override: ", g_sMPTunables.iIH_FINALE_PLAYER_NUM_VARIABLE_LIVES_HARD)
		RETURN g_sMPTunables.iIH_FINALE_PLAYER_NUM_VARIABLE_LIVES_HARD
	ENDIF
	#ENDIF
	
	RETURN 1
ENDFUNC

FUNC INT GET_DIFFICULTY_LIVES_OVERRIDE_EASY()
	#IF FEATURE_HEIST_ISLAND	
	IF HEIST_ISLAND_FLOW_IS_CURRENT_MISSION_HEIST_ISLAND_FINALE()
		PRINTLN("[VariableLives] GET_DIFFICULTY_LIVES_OVERRIDE_EASY - Using Island Override: ", g_sMPTunables.iIH_FINALE_DIFFICULTY_LIVES_EASY)
		RETURN g_sMPTunables.iIH_FINALE_DIFFICULTY_LIVES_EASY
	ENDIF
	#ENDIF
	
	RETURN 2
ENDFUNC

FUNC INT GET_DIFFICULTY_LIVES_OVERRIDE_NORMAL()
	#IF FEATURE_HEIST_ISLAND	
	IF HEIST_ISLAND_FLOW_IS_CURRENT_MISSION_HEIST_ISLAND_FINALE()
		PRINTLN("[VariableLives] GET_DIFFICULTY_LIVES_OVERRIDE_NORMAL - Using Island Override: ", g_sMPTunables.iIH_FINALE_DIFFICULTY_LIVES_NORMAL)
		RETURN g_sMPTunables.iIH_FINALE_DIFFICULTY_LIVES_NORMAL
	ENDIF
	#ENDIF
	
	RETURN 1
ENDFUNC

FUNC INT GET_DIFFICULTY_LIVES_OVERRIDE_HARD()
	#IF FEATURE_HEIST_ISLAND	
	IF HEIST_ISLAND_FLOW_IS_CURRENT_MISSION_HEIST_ISLAND_FINALE()
		PRINTLN("[VariableLives] GET_DIFFICULTY_LIVES_OVERRIDE_HARD - Using Island Override: ", g_sMPTunables.iIH_FINALE_DIFFICULTY_LIVES_HARD)
		RETURN g_sMPTunables.iIH_FINALE_DIFFICULTY_LIVES_HARD
	ENDIF
	#ENDIF
	
	RETURN 0
ENDFUNC

FUNC INT GET_VARIABLE_NUMBER_OF_TEAM_LIVES_TO_START_WITH(INT iTeam)

	INT iTeam2
	INT iNumPlayers
	INT iReturn
	
	IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iPlayerLives = ciNUMBER_OF_LIVES__PLAYER_NUM_VARIABLE
	OR g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iPlayerLives = ciNUMBER_OF_LIVES__HALF_PLAYER_NUM_VARIABLE

		IF MC_serverBD.iDifficulty = DIFF_EASY
			FOR iTeam2 = 0 TO (MC_serverBD.iNumberOfTeams - 1)
				IF DOES_TEAM_LIKE_TEAM(iTeam, iTeam2)
					iReturn = iReturn + MC_serverBD.iNumStartingPlayers[iTeam2]
				ENDIF
			ENDFOR
			PRINTLN("[VariableLives] GET_VARIABLE_NUMBER_OF_TEAM_LIVES_TO_START_WITH | player lives after easy mod = ", iReturn, " for team: ", iTeam)
			
		ELIF MC_serverBD.iDifficulty = DIFF_HARD
			iReturn = GET_PLAYER_NUM_VARIABLE_LIVES_OVERRIDE_HARD()
			PRINTLN("[VariableLives] GET_VARIABLE_NUMBER_OF_TEAM_LIVES_TO_START_WITH | player lives after hard mod = ",iReturn," for team: ",iTeam)
			
		ELSE
			FOR iTeam2 = 0 TO (MC_serverBD.iNumberOfTeams - 1)
				IF DOES_TEAM_LIKE_TEAM(iTeam, iTeam2)
					iNumPlayers = iNumPlayers + MC_serverBD.iNumStartingPlayers[iTeam2]
				ENDIF
			ENDFOR
			
			IF iNumPlayers <= 2
				iReturn = GET_PLAYER_NUM_VARIABLE_LIVES_OVERRIDE_NORMAL(TRUE)
			ELSE
				iReturn = GET_PLAYER_NUM_VARIABLE_LIVES_OVERRIDE_NORMAL(FALSE)
			ENDIF
			PRINTLN("[VariableLives] GET_VARIABLE_NUMBER_OF_TEAM_LIVES_TO_START_WITH | player lives after norm mod = ",iReturn," for team: ",iTeam)
			
		ENDIF
		
		IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iPlayerLives = ciNUMBER_OF_LIVES__HALF_PLAYER_NUM_VARIABLE
			iReturn /= 2
			PRINTLN("[VariableLives] GET_VARIABLE_NUMBER_OF_TEAM_LIVES_TO_START_WITH | Halving result now! iReturn: ", iReturn)
		ENDIF
	
	ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iPlayerLives = ciNUMBER_OF_LIVES__DIFFICULTY_VARIABLE
		SWITCH MC_serverBD.iDifficulty
			CASE DIFF_EASY
				iReturn = GET_DIFFICULTY_LIVES_OVERRIDE_EASY()
			BREAK
			CASE DIFF_NORMAL
				iReturn = GET_DIFFICULTY_LIVES_OVERRIDE_NORMAL()
			BREAK
			CASE DIFF_HARD
				iReturn = GET_DIFFICULTY_LIVES_OVERRIDE_HARD()
			BREAK
		ENDSWITCH
		
		PRINTLN("[VariableLives] GET_VARIABLE_NUMBER_OF_TEAM_LIVES_TO_START_WITH | Using ciNUMBER_OF_LIVES__DIFFICULTY_VARIABLE. Difficulty: ", MC_serverBD.iDifficulty)
	ENDIF
	
	PRINTLN("[VariableLives] GET_VARIABLE_NUMBER_OF_TEAM_LIVES_TO_START_WITH | iReturn: ",iReturn," for iTeam: ", iTeam)
	RETURN iReturn
	
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Client Score Requests
// ##### Description: Functions for requesting score to be added when rules are next processed
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC CLEAR_INCREMENT_SCORE_DATA_INDEX(INT i)
	INCREMENT_SCORE_DATA sBlank
	sIncrementScoreData[i] = sBlank
ENDPROC

FUNC BOOL IS_INCREMENT_SCORE_DATA_ARRAY_FREE(INT i)
	RETURN sIncrementScoreData[i].iBitset = 0
ENDFUNC

FUNC INT GET_FREE_INCREMENT_SCORE_DATA_ARRAY()
	INT i = 0
	FOR i = 0 TO ciMAX_INCREMENT_SCORE_REQUESTS-1
		IF IS_INCREMENT_SCORE_DATA_ARRAY_FREE(i)
			RETURN i
		ENDIF
	ENDFOR
	
	ASSERTLN("[SCORE][GET_FREE_INCREMENT_SCORE_DATA_ARRAY] - Request Failed. No free score increment request index. Likely something is spammning requests.")
	
	RETURN -1
ENDFUNC

PROC REQUEST_INCREMENT_LOCAL_PLAYER_SCORE_FOR_CURRENT_OBJECTIVE(INT iMultiplier = 1, BOOL bKillPoints = FALSE, INT iVictimPart = -1, INT iIdentifier = -1)
	
	DEBUG_PRINTCALLSTACK()
	PRINTLN("[SCORE] REQUEST_INCREMENT_LOCAL_PLAYER_SCORE_FOR_CURRENT_OBJECTIVE - Score: ", iMultiplier, ", Kill Points: ", BOOL_TO_STRING(bKillPoints), " Victim Part: ", iVictimPart, " Identifier: ", iIdentifier)
	
	INT iIndex = GET_FREE_INCREMENT_SCORE_DATA_ARRAY()
	IF iIndex = -1
		EXIT
	ENDIF
	
	SET_BIT(sIncrementScoreData[iIndex].iBitset, INCREMENT_SCORE_BITSET_TYPE_CURRENT_OBJECTIVE)
	
	IF bKillPoints
		SET_BIT(sIncrementScoreData[iIndex].iBitset, INCREMENT_SCORE_BITSET_GENERAL_KILL_POINTS)
	ENDIF
	
	IF iVictimPart > -1
		SET_BIT(sIncrementScoreData[iIndex].iBitset, INCREMENT_SCORE_BITSET_VICTIM_TYPE_PLAYER)
	ENDIF
	sIncrementScoreData[iIndex].iVictimID = iVictimPart
	
	sIncrementScoreData[iIndex].iScore = iMultiplier
	
	sIncrementScoreData[iIndex].iIdentifier = iIdentifier
	
ENDPROC

PROC REQUEST_INCREMENT_LOCAL_PLAYER_SCORE_BY(INT iChange, BOOL bKillPoints = FALSE, INT iIdentifier = -1)
	
	DEBUG_PRINTCALLSTACK()
	PRINTLN("[SCORE] REQUEST_INCREMENT_LOCAL_PLAYER_SCORE_BY - Score: ", iChange, ", Kill Points: ", BOOL_TO_STRING(bKillPoints), " Identifier: ", iIdentifier)
	
	INT iIndex = GET_FREE_INCREMENT_SCORE_DATA_ARRAY()
	IF iIndex = -1
		EXIT
	ENDIF
	
	SET_BIT(sIncrementScoreData[iIndex].iBitset, INCREMENT_SCORE_BITSET_TYPE_FORCED_AMOUNT)
	
	IF bKillPoints
		SET_BIT(sIncrementScoreData[iIndex].iBitset, INCREMENT_SCORE_BITSET_GENERAL_KILL_POINTS)
	ENDIF
	
	sIncrementScoreData[iIndex].iScore = iChange
	
	sIncrementScoreData[iIndex].iIdentifier = iIdentifier
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Server Score Requests
// ##### Description: Functions for requesting score to be added when rules are next processed on the server
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC REQUEST_INCREMENT_SERVER_REMOTE_PLAYER_SCORE(INT iParticipant, INT iScore, INT iTeam, INT iPriority)
	
	DEBUG_PRINTCALLSTACK()
	PRINTLN("[SCORE] REQUEST_SERVER_INCREMENT_REMOTE_PLAYER_SCORE - Participant: ", iParticipant, ", Score: ", iScore, ",  Team: ", iTeam, ", Priority: ", iPriority) 
	
	INT iAbsScore = ABSI(iScore)

	IF iAbsScore > ciSERVER_INCREMENT_SCORE_MAX
		ASSERTLN("[SCORE] REQUEST_SERVER_INCREMENT_REMOTE_PLAYER_SCORE - SCORE EXCEEDS THE LIMIT THAT CAN CURRENTLY BE PACKED")
		EXIT
	ENDIF
	
	INT iIndexToInsert = -1
	INT i
	FOR i = 0 TO ciMAX_SERVER_INCREMENT_SCORE_REQUESTS - 1
		IF MC_serverBD.iIncrementRemotePlayerScoreRequests[i] != 0
			RELOOP
		ENDIF
		
		iIndexToInsert = i
		BREAKLOOP
	ENDFOR
	
	IF iIndexToInsert = -1
		ASSERTLN("[SCORE] REQUEST_SERVER_INCREMENT_REMOTE_PLAYER_SCORE - REQUESTS ARE FULL, SCORING CANNOT BE PROCESSED")
	ENDIF
	
	INT iPackedIncrementRemotePlayerScoreRequest = 0
	
	SET_PACKED_BITFIELD_VALUE(iPackedIncrementRemotePlayerScoreRequest, ciSERVER_INCREMENT_SCORE_PARTICIPANT, ciSERVER_INCREMENT_SCORE_BIT_WIDTH, iParticipant)
	
	SET_PACKED_BITFIELD_VALUE(iPackedIncrementRemotePlayerScoreRequest, ciSERVER_INCREMENT_SCORE_SCORE, ciSERVER_INCREMENT_SCORE_BIT_WIDTH, iAbsScore)
	
	iPriority = PICK_INT(iPriority < 0 OR iPriority >= FMMC_MAX_RULES, ciSERVER_INCREMENT_SCORE_MAX, iPriority)
	SET_PACKED_BITFIELD_VALUE(iPackedIncrementRemotePlayerScoreRequest, ciSERVER_INCREMENT_SCORE_PRIORITY, ciSERVER_INCREMENT_SCORE_BIT_WIDTH, iPriority)
	
	//Team doesn't require more than 3 bits so it is last to allow space for setting if the score is positive or negative
	SET_PACKED_BITFIELD_VALUE(iPackedIncrementRemotePlayerScoreRequest, ciSERVER_INCREMENT_SCORE_TEAM, ciSERVER_INCREMENT_SCORE_BIT_WIDTH, iTeam)
	
	IF iScore < 0
		SET_BIT(iPackedIncrementRemotePlayerScoreRequest, ciSERVER_INCREMENT_SCORE_NEGATIVE)
	ENDIF
	
	MC_serverBD.iIncrementRemotePlayerScoreRequests[iIndexToInsert] = iPackedIncrementRemotePlayerScoreRequest
	
	PRINTLN("[SCORE] REQUEST_INCREMENT_SERVER_REMOTE_PLAYER_SCORE - Request(", iIndexToInsert, ") = ", MC_serverBD.iIncrementRemotePlayerScoreRequests[iIndexToInsert])
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Server Objective/Rule Logic Requests
// ##### Description: Functions for requesting server rule/objective logic to be run when rules are next processed
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
PROC REQUEST_SET_TEAM_FAILED(INT iTeam, eFailMissionEnum eFail = mFail_none, BOOL bForceReasonUpdate = TRUE, INT iPartCausingFail = -1, INT iEntityCausingFail = -1)
	
	DEBUG_PRINTCALLSTACK()
	PRINTLN("[MISSION_RESULTS] REQUEST_SET_TEAM_FAILED - Team: ", iTeam, " Fail Reason: ", ENUM_TO_INT(eFail), " Force Update: ", BOOL_TO_STRING(bForceReasonUpdate), " Part Causing Fail: ", iPartCausingFail, " Entity Causing Fail: ", iEntityCausingFail)
	
	IF MC_serverBD.iTeamFailRequests[iTeam] > 0
		PRINTLN("[MISSION_RESULTS] REQUEST_SET_TEAM_FAILED - Fail request already active for team: ", iTeam, ". Overwriting!")
		
		IF bForceReasonUpdate = FALSE
			INT iRequestData = SHIFT_RIGHT(MC_serverBD.iTeamFailRequests[iTeam], ciSET_TEAM_FAIL_REQUEST_SHIFT)
			eFail = INT_TO_ENUM(eFailMissionEnum, GET_PACKED_BITFILED_VALUE(iRequestData, ciSET_TEAM_FAIL_REQUEST_FAIL_REASON, ciSET_TEAM_FAIL_BIT_WIDTH))
			PRINTLN("[MISSION_RESULTS] REQUEST_SET_TEAM_FAILED - Force Update FALSE! Keeping previous fail reason: ", eFail)
		ENDIF
		
		MC_serverBD.iTeamFailRequests[iTeam] = 0
	ENDIF
	
	INT iPackedTeamFailRequest = 0
	
	SET_PACKED_BITFIELD_VALUE(iPackedTeamFailRequest, ciSET_TEAM_FAIL_REQUEST_FAIL_REASON, ciSET_TEAM_FAIL_BIT_WIDTH, ENUM_TO_INT(eFail))
	
	iPartCausingFail = PICK_INT(iPartCausingFail < 0, ciSET_TEAM_FAIL_REQUEST_INVALID, iPartCausingFail)
	SET_PACKED_BITFIELD_VALUE(iPackedTeamFailRequest, ciSET_TEAM_FAIL_REQUEST_FAIL_PART, ciSET_TEAM_FAIL_BIT_WIDTH, iPartCausingFail)
	
	iEntityCausingFail = PICK_INT(iEntityCausingFail < 0, ciSET_TEAM_FAIL_REQUEST_INVALID, iEntityCausingFail)
	SET_PACKED_BITFIELD_VALUE(iPackedTeamFailRequest, ciSET_TEAM_FAIL_REQUEST_FAIL_ENTITY, ciSET_TEAM_FAIL_BIT_WIDTH, iEntityCausingFail)
	
	iPackedTeamFailRequest = SHIFT_LEFT(iPackedTeamFailRequest, ciSET_TEAM_FAIL_REQUEST_SHIFT)
	
	SET_BIT(iPackedTeamFailRequest, ciSET_TEAM_FAIL_REQUEST_BS_ENABLED)
	
	IF bForceReasonUpdate
		SET_BIT(iPackedTeamFailRequest, ciSET_TEAM_FAIL_REQUEST_BS_FORCE_UPDATE_REASON)
	ENDIF
	
	MC_serverBD.iTeamFailRequests[iTeam] = iPackedTeamFailRequest
	
	PRINTLN("[MISSION_RESULTS] REQUEST_SET_TEAM_FAILED - Team: ", iTeam, " Fail Request = ", MC_serverBD.iTeamFailRequests[iTeam])
	
ENDPROC

/// PURPOSE:
///    Requests that the team's current rule is recalculated immediately - skipping the staggered loop
///    Do not call this unless you are certain it is required.
PROC REQUEST_RECALCULATE_OBJECTIVE_LOGIC_FOR_TEAM(INT iTeam)
	IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_RECALCULATE_OBJECTIVE_LOGIC_TEAM_0 + iTeam)
		DEBUG_PRINTCALLSTACK()
		PRINTLN("[OBJECTIVE_PROGRESSION] REQUEST_RECALCULATE_OBJECTIVE_LOGIC_FOR_TEAM - Setting for Team: ", iTeam)
		SET_BIT(MC_serverBD.iServerBitSet1, SBBOOL1_RECALCULATE_OBJECTIVE_LOGIC_TEAM_0 + iTeam)
	ENDIF
ENDPROC

FUNC BOOL SHOULD_RECALCULATE_OBJECTIVE_FOR_TEAM(INT iTeam)
	RETURN IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_RECALCULATE_OBJECTIVE_LOGIC_TEAM_0 + iTeam)
ENDFUNC

PROC CLEAR_RECALCULATE_OBJECTIVE_FOR_TEAM(INT iTeam)
	CLEAR_BIT(MC_serverBD.iServerBitSet1, SBBOOL1_RECALCULATE_OBJECTIVE_LOGIC_TEAM_0 + iTeam)
ENDPROC				

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Vector Helper Functions
// ##### Description: Various helper functions to do with Vectors or coords etc
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
FUNC VECTOR NORM_VEC( VECTOR vec )
	RETURN vec / VMAG( vec )
ENDFUNC

FUNC FLOAT GET_TRIANGLE_ALTITUDE_FROM_VECTORS( VECTOR a, VECTOR b, VECTOR c )
	FLOAT fArea = 0.5 * VMAG( CROSS_PRODUCT( b - a, b - c ) )
	FLOAT fMagAtoC = VMAG( a - c )
	RETURN fArea / ( 0.5 * fMagAtoC )
ENDFUNC

FUNC BOOL GET_VECTOR_PLANE_INTERSECTION_COORD( VECTOR vPlaneCenter, VECTOR vPlaneNormal, FLOAT fPlaneWidth, FLOAT fPlaneHeight, 
	VECTOR vLineOrigin, VECTOR vLineDirection, VECTOR &vResult )
	
	VECTOR vLeft = NORM_VEC( CROSS_PRODUCT( vPlaneNormal, <<0.0, 0.0, -1.0>> ) )
	VECTOR vUp	 = NORM_VEC( CROSS_PRODUCT( vPlaneNormal, vLeft ) )
	
	FLOAT fHalfPlaneWidth  = fPlaneWidth / 2.0
	FLOAT fHalfPlaneHeight = fPlaneHeight / 2.0
	
	VECTOR vTopLeft		= vPlaneCenter + -fHalfPlaneWidth * vLeft + fHalfPlaneHeight  * vUp
	VECTOR vTopRight	= vPlaneCenter + fHalfPlaneWidth  * vLeft + fHalfPlaneHeight  * vUp
	VECTOR vBottomLeft	= vPlaneCenter + -fHalfPlaneWidth * vLeft + -fHalfPlaneHeight * vUp
	VECTOR vBottomRight	= vPlaneCenter + fHalfPlaneWidth  * vLeft + -fHalfPlaneHeight * vUp
	
	FLOAT fExtent = DOT_PRODUCT( ( vPlaneCenter - vLineOrigin ), vPlaneNormal ) / DOT_PRODUCT( vLineDirection, vPlaneNormal )
	vResult = vLineOrigin + ( fExtent * vLineDirection )
	
	IF( GET_TRIANGLE_ALTITUDE_FROM_VECTORS( vTopLeft, vResult, vTopRight ) <= fPlaneHeight
	AND GET_TRIANGLE_ALTITUDE_FROM_VECTORS( vBottomLeft, vResult, vBottomRight ) <= fPlaneHeight
	AND GET_TRIANGLE_ALTITUDE_FROM_VECTORS( vTopLeft, vResult, vBottomLeft ) <= fPlaneWidth
	AND GET_TRIANGLE_ALTITUDE_FROM_VECTORS( vTopRight, vResult, vBottomRight ) <= fPlaneWidth )
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

FUNC BOOL IS_INSIDE_SPHERE(VECTOR playerCoord, VECTOR sphereCentre, FLOAT radius)
	IF GET_DISTANCE_BETWEEN_COORDS(playerCoord, sphereCentre) < radius
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC FLOAT CHECK_Z_FOR_OVERHANG(VECTOR vObjCoord)
	FLOAT fGroundZ
	VECTOR tempVector
	tempVector.z = vObjCoord.z - 2
	
	IF GET_GROUND_Z_FOR_3D_COORD(tempVector, fGroundZ)
		vObjCoord.z = fGroundZ
		PRINTLN("[JT] CHECK_Z_FOR_OVERHANG Valid ground z found: ", fGroundZ)
	ENDIF	
	
	RETURN vObjCoord.z
ENDFUNC

FUNC BOOL ARE_ROTATIONS_ALMOST_EQUAL(VECTOR vRotationA, VECTOR vRotationB, FLOAT fTolerance = 0.5)

	IF GET_ANGULAR_DIFFERENCE(vRotationA.x, vRotationB.x) > fTolerance
		RETURN FALSE
	ENDIF
	IF GET_ANGULAR_DIFFERENCE(vRotationA.y, vRotationB.y) > fTolerance
		RETURN FALSE
	ENDIF
	IF GET_ANGULAR_DIFFERENCE(vRotationA.z, vRotationB.z) > fTolerance
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC VECTOR GET_CENTER_OF_AREA(VECTOR vCoord1, VECTOR vCoord2)
	RETURN (vCoord1 + vCoord2) * <<0.5,0.5,0.5>>
ENDFUNC

FUNC VECTOR GET_CLOSEST_POINT_AT_EDGE_OF_BOUNDS(VECTOR vPoint, VECTOR vCoord1, VECTOR vCoord2, FLOAT fWidth)
	vCoord1.Z = FMAX(vCoord1.Z, vCoord2.Z)
	vCoord2.Z = vCoord1.Z
	
	VECTOR vDir = vCoord2 - vCoord1
	
	FLOAT fTemp = vDir.X
	vDir.X = vDir.Y
	vDir.Y = 0 - fTemp
	
	vDir = NORMALISE_VECTOR(vDir)
	
	fWidth = (fWidth / 2) - 2
	
	VECTOR vCorner[4]
	
	vCorner[0] = vCoord1 + (vDir * fWidth)
	vCorner[1] = vCoord1 + (vDir * -fWidth)
	vCorner[2] = vCoord2 + (vDir * fWidth)
	vCorner[3] = vCoord2 + (vDir * -fWidth)
	
	INT iClosestPoint = 0
	
	VECTOR vClosestPoint[4]
	
	vClosestPoint[0] = GET_CLOSEST_POINT_ON_LINE(vPoint, vCorner[0], vCorner[1])
	vClosestPoint[1] = GET_CLOSEST_POINT_ON_LINE(vPoint, vCorner[0], vCorner[2])
	vClosestPoint[2] = GET_CLOSEST_POINT_ON_LINE(vPoint, vCorner[2], vCorner[3])
	vClosestPoint[3] = GET_CLOSEST_POINT_ON_LINE(vPoint, vCorner[1], vCorner[3])
	
	INT i
	
	FOR i = 1 TO 3	//Skip 0, it's our starting point
		IF GET_DISTANCE_BETWEEN_COORDS(vPoint, vClosestPoint[i]) < GET_DISTANCE_BETWEEN_COORDS(vPoint, vClosestPoint[iClosestPoint])
			iClosestPoint = i
		ENDIF
	ENDFOR
	
	RETURN vClosestPoint[iClosestPoint]
ENDFUNC

FUNC FLOAT MINIMUM_DISTANCE_POINT_TO_LINE(VECTOR vLine1, VECTOR vLine2, VECTOR vPoint)
	// Return minimum distance between line segment vw and point vPoint
	FLOAT fLengthSquared = VDIST2(vLine1, vLine2) // Avoid a sqrt
	
	IF (fLengthSquared = 0.0) 
		RETURN GET_DISTANCE_BETWEEN_COORDS(vPoint, vLine1)   // vLine1 == vLine2 case
	ENDIF
	
	// Consider the line extending the segment, parameterized as vLine1 + t (vLine2 - vLine1).
	// We find projection of point vPoint onto the line. 
	// It falls where t = [(vPoint-vLine1) . (vLine2-vLine1)] / |vLine2-vLine1|^2
	FLOAT fDotProduct = DOT_PRODUCT(vPoint - vLine1, vLine2 - vLine1) / fLengthSquared
	
	IF (fDotProduct < 0.0)
		RETURN GET_DISTANCE_BETWEEN_COORDS(vPoint, vLine1)	// Beyond the 'vLine1' end of the segment
	ELIF (fDotProduct > 1.0)
		RETURN GET_DISTANCE_BETWEEN_COORDS(vPoint, vLine2)	// Beyond the 'vLine2' end of the segment
	ENDIF
	
	VECTOR vProjection = vLine1 + fDotProduct * (vLine2 - vLine1)  // Projection falls on the segment
	
	RETURN GET_DISTANCE_BETWEEN_COORDS(vPoint, vProjection)
ENDFUNC

FUNC ANGLED_AREA_CORNER_STRUCT GET_CORNERS_OF_ANGLED_AREA(VECTOR vLoc1, VECTOR vLoc2, FLOAT fWidth)
	ANGLED_AREA_CORNER_STRUCT vAngledAreaCorners
	
	vLoc1.Z = 0.0
	vLoc2.Z = 0.0
	
	VECTOR vNorm = NORMALISE_VECTOR(vLoc2 - vLoc1)
	
	FLOAT fTemp
	
	vAngledAreaCorners.vCorner[0] = vNorm
	fTemp = vAngledAreaCorners.vCorner[0].X
	vAngledAreaCorners.vCorner[0].X = vAngledAreaCorners.vCorner[0].Y
	vAngledAreaCorners.vCorner[0].Y = 0 - fTemp
	vAngledAreaCorners.vCorner[0] = vLoc1 + (vAngledAreaCorners.vCorner[0] * (fWidth / 2))
	
	vAngledAreaCorners.vCorner[1] = vNorm
	fTemp = vAngledAreaCorners.vCorner[1].X
	vAngledAreaCorners.vCorner[1].X = 0 - vAngledAreaCorners.vCorner[1].Y
	vAngledAreaCorners.vCorner[1].Y = fTemp
	vAngledAreaCorners.vCorner[1] = vLoc1 + (vAngledAreaCorners.vCorner[1] * (fWidth / 2))
	
	vAngledAreaCorners.vCorner[2] = vNorm
	fTemp = vAngledAreaCorners.vCorner[2].X
	vAngledAreaCorners.vCorner[2].X = vAngledAreaCorners.vCorner[2].Y
	vAngledAreaCorners.vCorner[2].Y = 0 - fTemp
	vAngledAreaCorners.vCorner[2] = vLoc2 + (vAngledAreaCorners.vCorner[2] * (fWidth / 2))
	
	vAngledAreaCorners.vCorner[3] = vNorm
	fTemp = vAngledAreaCorners.vCorner[3].X
	vAngledAreaCorners.vCorner[3].X = 0 - vAngledAreaCorners.vCorner[3].Y
	vAngledAreaCorners.vCorner[3].Y = fTemp
	vAngledAreaCorners.vCorner[3] = vLoc2 + (vAngledAreaCorners.vCorner[3] * (fWidth / 2))
	
	RETURN vAngledAreaCorners
ENDFUNC

PROC SERVER_CALCULATE_CENTRE_POINT_FOR_TEAMS(MC_ServerBroadcastData &ServerDataPassed, MC_PlayerBroadcastData &PlayerDataPassed[])

	INT i
	FLOAT iTeam0Num, iTeam1Num, iTeam2Num, iTeam3Num
	PLAYER_INDEX thePlayerID
	PED_INDEX pedLocal

	FOR i = 0 TO ServerDataPassed.iTotalNumStartingPlayers
	
		thePlayerID = INT_TO_PLAYERINDEX(i)
		pedLocal = GET_PLAYER_PED(thePlayerID)
		
		IF DOES_ENTITY_EXIST(pedLocal)
			IF NOT IS_ENTITY_DEAD(pedLocal)
				IF PlayerDataPassed[i].iTeam = 0
					ServerDataPassed.vTeamCentreStartPoint[0] += GET_ENTITY_COORDS(pedLocal)
					iTeam0Num++
					PRINTLN("[RCC MISSION] SERVER_CALCULATE_CENTRE_POINT_FOR_TEAMS, iTeam0Num = ", iTeam0Num)
				ELIF PlayerDataPassed[i].iTeam = 1
					ServerDataPassed.vTeamCentreStartPoint[1] += GET_ENTITY_COORDS(pedLocal)
					iTeam1Num++
					PRINTLN("[RCC MISSION] SERVER_CALCULATE_CENTRE_POINT_FOR_TEAMS, iTeam1Num = ", iTeam1Num)
				ELIF PlayerDataPassed[i].iTeam = 2
					ServerDataPassed.vTeamCentreStartPoint[2] += GET_ENTITY_COORDS(pedLocal)
					iTeam2Num++
					PRINTLN("[RCC MISSION] SERVER_CALCULATE_CENTRE_POINT_FOR_TEAMS, iTeam2Num = ", iTeam2Num)
				ELIF PlayerDataPassed[i].iTeam = 3
					ServerDataPassed.vTeamCentreStartPoint[3] += GET_ENTITY_COORDS(pedLocal)
					iTeam3Num++
					PRINTLN("[RCC MISSION] SERVER_CALCULATE_CENTRE_POINT_FOR_TEAMS, iTeam3Num =", iTeam3Num)
				ENDIF
			ENDIF
		ENDIF
	
	ENDFOR
	
	ServerDataPassed.vTeamCentreStartPoint[0] = (ServerDataPassed.vTeamCentreStartPoint[0]/iTeam0Num)
	ServerDataPassed.vTeamCentreStartPoint[1] = (ServerDataPassed.vTeamCentreStartPoint[1]/iTeam1Num)
	ServerDataPassed.vTeamCentreStartPoint[2] = (ServerDataPassed.vTeamCentreStartPoint[2]/iTeam2Num)
	ServerDataPassed.vTeamCentreStartPoint[3] = (ServerDataPassed.vTeamCentreStartPoint[3]/iTeam3Num)
	PRINTLN("[RCC MISSION] SERVER_CALCULATE_CENTRE_POINT_FOR_TEAMS, serverBDpassed.vTeamCentreStartPoint[0] = ", ServerDataPassed.vTeamCentreStartPoint[0])
	PRINTLN("[RCC MISSION] SERVER_CALCULATE_CENTRE_POINT_FOR_TEAMS, serverBDpassed.vTeamCentreStartPoint[1] = ", ServerDataPassed.vTeamCentreStartPoint[1])
	PRINTLN("[RCC MISSION] SERVER_CALCULATE_CENTRE_POINT_FOR_TEAMS, serverBDpassed.vTeamCentreStartPoint[2] = ", ServerDataPassed.vTeamCentreStartPoint[2])
	PRINTLN("[RCC MISSION] SERVER_CALCULATE_CENTRE_POINT_FOR_TEAMS, serverBDpassed.vTeamCentreStartPoint[3] = ", ServerDataPassed.vTeamCentreStartPoint[3])
	
ENDPROC

PROC PUT_VECTOR_ON_GROUND(VECTOR& vVector, FLOAT fDistanceAboveGround = 0.0)
	FLOAT fGroundZ = vVector.z
	IF GET_GROUND_Z_EXCLUDING_OBJECTS_FOR_3D_COORD(vVector, fGroundZ, FALSE)
		vVector.z = fGroundZ
	ENDIF
	
	vVector.z += fDistanceAboveGround
ENDPROC

FUNC FLOAT VECTOR_LENGTH_SQ(VECTOR vVector)
	RETURN (vVector.x * vVector.x) + (vVector.y * vVector.y) + (vVector.z * vVector.z)
ENDFUNC

FUNC VECTOR SMOOTH_DAMP_VECTOR(VECTOR vVector, VECTOR vTarget, FLOAT fInterpSpeed)
	
	VECTOR vDeltaVector = vTarget - vVector
	
	IF VECTOR_LENGTH_SQ(vDeltaVector) < 0.00001
		RETURN vTarget
	ENDIF

	vDeltaVector *= CLAMP(fInterpSpeed * TIMESTEP(), 0.0, 1.0)
	
	RETURN vVector + vDeltaVector
	
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Heading Helper Functions
// ##### Description: Various helper functions to do with Heading
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC GET_NORMALISED_DELTA_HEADINGS( FLOAT &fHeadingOne, FLOAT &fHeadingTwo )
	fHeadingOne = MOD_FLOAT( fHeadingOne, 360.0 )
	fHeadingTwo = MOD_FLOAT( fHeadingTwo, 360.0 )
	
	#IF IS_DEBUG_BUILD
	IF( fHeadingOne < 0.0 )
		ASSERTLN( "GET_NORMALISED_DELTA_HEADINGS fHeadingOne < 0.0! fHeadingOne:", fHeadingOne )
	ENDIF
	IF( fHeadingTwo < 0.0 )
		ASSERTLN( "GET_NORMALISED_DELTA_HEADINGS fHeadingTwo < 0.0! fHeadingTwo:", fHeadingTwo )
	ENDIF
	#ENDIF
	
	IF( ABSF( fHeadingTwo - fHeadingOne ) > 180.0 )
		IF( fHeadingTwo - fHeadingOne <= 0.0 )
			fHeadingTwo += 360.0
		ELSE
			fHeadingOne += 360.0
		ENDIF
	ENDIF
ENDPROC

FUNC FLOAT GET_ABSOLUTE_DELTA_HEADING( FLOAT fHeadingOne, FLOAT fHeadingTwo )
	GET_NORMALISED_DELTA_HEADINGS( fHeadingOne, fHeadingTwo )

	IF( fHeadingOne > fHeadingTwo )
		RETURN ABSF( fHeadingOne - fHeadingTwo )
	ELSE	
		RETURN ABSF( fHeadingTwo - fHeadingOne )
	ENDIF
ENDFUNC

FUNC FLOAT CONVERT_FLOAT_TO_ROTATION_VALUE(FLOAT fInput)
	IF fInput > 360
		fInput -= 360
	ENDIF
	IF fInput < 0
		fInput = (360 + fInput)
	ENDIF
	
	RETURN fInput
ENDFUNC

FUNC BOOL IS_HEADING_CLOSE_TO_HEADING(FLOAT fHeading1, FLOAT fHeading2, FLOAT fTolerance = 30.0)
	RETURN GET_ABSOLUTE_DELTA_HEADING(fHeading1, fHeading2) <= fTolerance
ENDFUNC	

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Audio Helper Functions
// ##### Description: Various helper functions & wrappers to do with sounds & sound IDs
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC ASSIGN_SOUND_ID(INT &iSound)
	iSound = GET_SOUND_ID()
ENDPROC

PROC MC_SCRIPT_TRIGGER_MUSIC_EVENT_STOP()
		
	SCRIPT_TRIGGER_MUSIC_EVENT_STOP()
	
ENDPROC

FUNC BOOL IS_SOUND_ID_VALID(INT tempSoundID)
	IF tempSoundID >= 0
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC SCRIPT_RELEASE_SOUND_ID(INT& tempSoundID)
	IF IS_SOUND_ID_VALID(tempSoundID)
	
		#IF IS_DEBUG_BUILD
			DEBUG_PRINTCALLSTACK()
			PRINTLN("[Audio] SCRIPT_RELEASE_SOUND_ID - Releasing sound ID: ", tempSoundID)
		#ENDIF
		
		RELEASE_SOUND_ID(tempSoundID)
		tempSoundID = -1
	ENDIF
ENDPROC

PROC CLEAR_10_SECOND_COUNTDOWN_BEEPS()

	IF IS_SOUND_ID_VALID(iTenSecondBeeps)
	AND NOT HAS_SOUND_FINISHED(iTenSecondBeeps)
		STOP_SOUND(iTenSecondBeeps)
	ENDIF
	
	SCRIPT_RELEASE_SOUND_ID(iTenSecondBeeps)
	
ENDPROC

PROC PLAY_10_SECOND_COUNTDOWN_BEEPS(BOOL bResetIfPlaying = FALSE)
	
	IF bResetIfPlaying
		CLEAR_10_SECOND_COUNTDOWN_BEEPS()
	ENDIF
	
	IF IS_SOUND_ID_VALID(iTenSecondBeeps)
		//Already playing
		EXIT
	ENDIF
	
	iTenSecondBeeps = GET_SOUND_ID()
	PRINTLN("[Audio] PLAY_10_SECOND_COUNTDOWN_BEEPS - Playing beeps, sound ID: ", iTenSecondBeeps)
	PLAY_SOUND_FRONTEND(iTenSecondBeeps, "10S", "MP_MISSION_COUNTDOWN_SOUNDSET")
	
ENDPROC

FUNC BOOL IS_HACK_START_SOUND_BLOCKED()
	IF iSpectatorTarget > -1
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_HACK_STOP_SOUND_BLOCKED()
	IF iHackPercentage = 100 OR iHackPercentage = 0
		RETURN TRUE
	ENDIF
	
	IF iSpectatorTarget > -1
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC enumCharacterList GET_INCOMING_CONTACT_CHARACTER_FROM_CREATOR(INT iCharacter)
	SWITCH iCharacter
		CASE DT_CHAR_AGATHA 		RETURN GET_CASINO_CONTACT()
		CASE DT_CHAR_CHENG			RETURN CHAR_CHENG
		CASE DT_CHAR_TRANSLATOR 	RETURN CHAR_CASINO_TAO_TRANSLATOR
		CASE DT_CHAR_YUNG_ANCESTOR 	RETURN CHAR_ARCADE_CELEB
		CASE DT_CHAR_YACHT_CAPTAIN 	RETURN CHAR_YACHT_CAPTAIN
		CASE DT_CHAR_UNKNOWN 		RETURN CHAR_BLOCKED
		#IF FEATURE_HEIST_ISLAND
		CASE DT_CHAR_MIGUEL 		RETURN CHAR_MIGUEL_MADRAZO
		CASE DT_CHAR_PAVEL 			RETURN CHAR_PAVEL
		CASE DT_CHAR_MOODY_MANN		RETURN CHAR_MOODYMANN
		CASE DT_CHAR_SESSANTA		RETURN CHAR_SESSANTA
		#ENDIF
		#IF FEATURE_TUNER
		CASE DT_CHAR_KDJ			RETURN CHAR_KDJ
		#ENDIF
		#IF FEATURE_FIXER
		CASE DT_CHAR_FRANKLIN_AND_IMANI	RETURN CHAR_FIXER_FRANKLIN_IMANI_CONF
		#ENDIF
		#IF FEATURE_DLC_1_2022
		CASE DT_CHAR_ULP			RETURN CHAR_ULP
		#ENDIF
		#IF FEATURE_DLC_2_2022
		CASE DT_CHAR_DAX			RETURN CHAR_DAX
		#ENDIF
	ENDSWITCH
	
	RETURN MAX_NRM_CHARACTERS_PLUS_DUMMY
ENDFUNC

FUNC BOOL IS_CREATOR_VOICE_CHAT_RESTRICTED()

	INT iTeam 
	INT iPriority 
	
	IF iPartToUse > -1
	AND iPartToUse < NUM_NETWORK_PLAYERS
		iTeam = MC_playerBD[iPartToUse].iteam
		iPriority = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]
	ENDIF

	IF iTeam > -1
	AND iPriority > -1 AND iPriority < FMMC_MAX_RULES
	AND NOT g_bMissionEnding
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwo[iPriority], ciBS_RULE2_RESTRICT_VOICE_CHAT)
			PRINTLN("IS_CREATOR_VOICE_CHAT_RESTRICTED - Returning True")
			RETURN TRUE
		ENDIF
	ENDIF

	PRINTLN("IS_CREATOR_VOICE_CHAT_RESTRICTED - Returning False")
	RETURN FALSE
ENDFUNC

FUNC FLOAT GET_MISSION_CHAT_PROXIMITY()
	
	IF IS_CREATOR_VOICE_CHAT_RESTRICTED()		
		RETURN 100.00
	ENDIF

	RETURN 0.0
ENDFUNC

FUNC BOOL IS_PROP_A_SPEAKER(INT iProp)
	
	MODEL_NAMES mnProp = g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].mn
	
	IF mnProp = PROP_BOOMBOX_01
	OR mnProp = PROP_GHETTOBLAST_02
	OR mnProp = PROP_TAPEPLAYER_01
	OR mnProp = PROP_RADIO_01
	OR mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("Sf_prop_sf_speaker_l_01a"))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC TEXT_LABEL_63 GET_EMITTER_FOR_MODEL(MODEL_NAMES mnModel, INT iIndex)
	
	TEXT_LABEL_63 tlEmitter = "SE_Script_Placed_Prop_Emitter_Boombox"
	
	IF mnModel = INT_TO_ENUM(MODEL_NAMES, HASH("Sf_prop_sf_speaker_l_01a"))
	OR mnModel = INT_TO_ENUM(MODEL_NAMES, HASH("sf_prop_sf_speaker_stand_01a"))
		tlEmitter = "SE_DLC_Fixer_Data_Leak_Mansion_Speaker_0"
		tlEmitter += iIndex + 1
		IF iIndex >= 9
			tlEmitter = "SE_DLC_Fixer_Data_Leak_Mansion_Speaker_10"
		ENDIF
	ENDIF
	
	RETURN tlEmitter
	
ENDFUNC

PROC ENABLE_AMBIENT_ZONE_FOR_EMITTER_FROM_MODEL(MODEL_NAMES mnModel)
	IF mnModel = INT_TO_ENUM(MODEL_NAMES, HASH("Sf_prop_sf_speaker_l_01a"))
	OR mnModel = INT_TO_ENUM(MODEL_NAMES, HASH("sf_prop_sf_speaker_stand_01a"))
		SET_AMBIENT_ZONE_STATE("AZ_DLC_Fixer_Data_Leak_Mansion_Party_Area_01", TRUE)
	ENDIF
ENDPROC

FUNC INT FIND_A_FREE_EMITTER_FOR_OBJECT(INT iIndex, INT iType)
	INT i
	FOR i = 0 TO FMMC_MAX_NUM_RADIO_EMITTERS-1
		IF MC_serverBD_3.sRadioEmitters[i].iRadioEmitterObjectType = iType
		AND MC_serverBD_3.sRadioEmitters[i].iRadioEmitterObjectIndex = iIndex
			PRINTLN("[Emitters][Emitter ",i,"] FIND_A_FREE_EMITTER_FOR_OBJECT - This object is already in the array!")
			RETURN -1
		ENDIF
		IF MC_serverBD_3.sRadioEmitters[i].iRadioEmitterObjectType = -1
		AND MC_serverBD_3.sRadioEmitters[i].iRadioEmitterObjectIndex = -1
			MC_serverBD_3.sRadioEmitters[i].iRadioEmitterObjectType = iType
			MC_serverBD_3.sRadioEmitters[i].iRadioEmitterObjectIndex = iIndex
			PRINTLN("[Emitters][Emitter ",i,"] FIND_A_FREE_EMITTER_FOR_OBJECT - New emitter set!")
			RETURN i
		ENDIF
	ENDFOR
	
	PRINTLN("[Emitters][Emitter ",i,"] FIND_A_FREE_EMITTER_FOR_OBJECT - The array is full!")
	
	RETURN -1
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Sync Scenes
// ##### Description: Various helper functions & wrappers to do with sync scenes
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//PURPOSE: Returns TRUE if the Sync Scene is active not finished like the poorly named command IS_SYNC_SCENE_ACTIVE!!
FUNC BOOL IS_SYNC_SCENE_RUNNING(INT ThisSynchScene)
	INT iThisLocalSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(ThisSynchScene)
	IF iThisLocalSceneID != -1
		IF IS_SYNCHRONIZED_SCENE_RUNNING(iThisLocalSceneID)		
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_SYNC_SCENE_ACTIVE(INT ThisSynchScene)
	INT iThisLocalSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(ThisSynchScene)
	IF iThisLocalSceneID != -1
		IF IS_SYNCHRONIZED_SCENE_RUNNING(iThisLocalSceneID)	
			IF GET_SYNCHRONIZED_SCENE_PHASE(iThisLocalSceneID) >= 1.0
				RETURN TRUE
			ENDIF
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Stops the sync scene
PROC STOP_SYNC_SCENE(INT ThisSynchScene, BOOL bUseNetworkSyncSceneIndex = FALSE)
	INT iThisLocalSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(ThisSynchScene)
	IF iThisLocalSceneID != -1
		IF IS_SYNCHRONIZED_SCENE_RUNNING(iThisLocalSceneID)	
			IF bUseNetworkSyncSceneIndex
				NETWORK_STOP_SYNCHRONISED_SCENE(ThisSynchScene)
				NET_PRINT_TIME() NET_PRINT("     ---------->     HOLD UP - STOP_SYNC_SCENE (NETWORKED)      <----------     ") NET_NL()
			ELSE
				NETWORK_STOP_SYNCHRONISED_SCENE(iThisLocalSceneID)
				NET_PRINT_TIME() NET_PRINT("     ---------->     HOLD UP - STOP_SYNC_SCENE      <----------     ") NET_NL()
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC FLOAT GET_SYNC_SCENE_PHASE(INT ThisSynchScene)
	INT iThisLocalSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(ThisSynchScene)
	 PRINTLN("    ---------->     GET_SYNC_SCENE_PHASE     iThisLocalSceneID ",iThisLocalSceneID)
	IF iThisLocalSceneID != -1
		IF IS_SYNCHRONIZED_SCENE_RUNNING(iThisLocalSceneID)
			RETURN GET_SYNCHRONIZED_SCENE_PHASE(iThisLocalSceneID)		
		ENDIF
	ENDIF
	return 0.0
ENDFUNC

//PURPOSE: Returns TRUE if the sync scene is past the specified phase
FUNC BOOL IS_PAST_SYNC_SCENE_PHASE(INT ThisSynchScene,FLOAT fPhase)
	INT iThisLocalSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(ThisSynchScene)
	 PRINTLN("    ---------->     IS_PAST_SYNC_SCENE_PHASE     iThisLocalSceneID ",iThisLocalSceneID)
	IF iThisLocalSceneID != -1
		IF IS_SYNCHRONIZED_SCENE_RUNNING(iThisLocalSceneID)
			PRINTLN("    ---------->     GET_SYNCHRONIZED_SCENE_PHASE      ",GET_SYNCHRONIZED_SCENE_PHASE(iThisLocalSceneID))
			IF GET_SYNCHRONIZED_SCENE_PHASE(iThisLocalSceneID) >= fPhase
				PRINTLN("    ---------->     GET_SYNCHRONIZED_SCENE_PHASE    true  ")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE 
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Player Controls and State  -----------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: This section covers the processing of player controls and their current state, e.g. forced cover, etc.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC APPLY_INTRO_DRIVE()
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iTeamBitset4, ciBS4_TEAM_START_MISSION_WALKING)
	OR IS_BIT_SET(iLocalBoolCheck32, LBOOL32_FORCED_MISSION_START_MOVING_FORWARD)
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(iLocalBoolCheck31, LBOOL31_APPLIED_INTRO_GAIT)
		VEHICLE_INDEX vehPedIsIn = GET_VEHICLE_PED_IS_IN(localPlayerPed)
		
		IF NOT IS_VEHICLE_DRIVEABLE(vehPedIsIn)
			PRINTLN("[RollingStart] - APPLY_INTRO_DRIVE - Vehicle not driveable.")
			EXIT
		ENDIF		
		
		IF GET_PED_IN_VEHICLE_SEAT(vehPedIsIn, VS_DRIVER) != LocalPlayerPed
			SET_BIT(iLocalBoolCheck31, LBOOL31_APPLIED_INTRO_GAIT)
			PRINTLN("[RollingStart] - APPLY_INTRO_DRIVE - Not Driver.")
			EXIT
		ENDIF
		
		IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(vehPedIsIn)
			PRINTLN("[RollingStart] - APPLY_INTRO_DRIVE - Waiting for Network Control")
			EXIT
		ENDIF
		
		PRINTLN("[RollingStart] - APPLY_INTRO_DRIVE - Go!")
		DEBUG_PRINTCALLSTACK()
		SET_VEHICLE_ENGINE_ON(vehPedIsIn, TRUE, TRUE, FALSE)
		SET_VEHICLE_LIMIT_SPEED_WHEN_PLAYER_INACTIVE(vehPedIsIn, FALSE)
		SET_VEHICLE_STOP_INSTANTLY_WHEN_PLAYER_INACTIVE(vehPedIsIn, FALSE)
		SET_VEHICLE_HANDBRAKE(vehPedIsIn, FALSE)		
		FREEZE_ENTITY_POSITION(vehPedIsIn, FALSE)
		SET_VEHICLE_FORWARD_SPEED(vehPedIsIn, 20.0)
		SET_BIT(iLocalBoolCheck31, LBOOL31_APPLIED_INTRO_GAIT)		
	ENDIF
ENDPROC

PROC APPLY_INTRO_GAIT()
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iTeamBitset4, ciBS4_TEAM_START_MISSION_WALKING)
	OR IS_BIT_SET(iLocalBoolCheck32, LBOOL32_FORCED_MISSION_START_MOVING_FORWARD)
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(iLocalBoolCheck31, LBOOL31_APPLIED_INTRO_GAIT)
		PRINTLN("[RollingStart] - APPLY_INTRO_GAIT - Go!")
		DEBUG_PRINTCALLSTACK()
		SIMULATE_PLAYER_INPUT_GAIT(LocalPlayer, 0.5, 3500)
		SET_BIT(iLocalBoolCheck31, LBOOL31_APPLIED_INTRO_GAIT)		
		
	ENDIF
ENDPROC

PROC APPLY_INTRO_SWIM()
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iTeamBitset4, ciBS4_TEAM_START_MISSION_WALKING)
	OR IS_BIT_SET(iLocalBoolCheck32, LBOOL32_FORCED_MISSION_START_MOVING_FORWARD)
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(iLocalBoolCheck31, LBOOL31_APPLIED_INTRO_GAIT)
		PRINTLN("[RollingStart] - APPLY_INTRO_SWIM - Go!")
		DEBUG_PRINTCALLSTACK()
		VECTOR vCoord = GET_ENTITY_COORDS(localPlayerPed, FALSE)
		vCoord = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCoord, GET_ENTITY_HEADING(LocalPlayerPed)+90, <<10.0, 0.0, 0.0>>)
		TASK_GO_STRAIGHT_TO_COORD(LocalPlayerPed, vCoord, 1.0, -1, GET_ENTITY_HEADING(LocalPlayerPed), 1.5)
		SET_BIT(iLocalBoolCheck31, LBOOL31_APPLIED_INTRO_GAIT)				
	ENDIF
ENDPROC

PROC PROCESS_APPLY_INTRO_MOVE_FORWARD()
	IF IS_PED_SWIMMING_UNDER_WATER(localPlayerPed)
		APPLY_INTRO_SWIM()
	ELIF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		APPLY_INTRO_DRIVE()
	ELSE
		APPLY_INTRO_GAIT()
	ENDIF
ENDPROC

PROC PROCESS_LOCAL_PLAYER_INTRO_GAIT()
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iTeamBitset4, ciBS4_TEAM_START_MISSION_WALKING)
	AND NOT IS_BIT_SET(iLocalBoolCheck32, LBOOL32_FORCED_MISSION_START_MOVING_FORWARD)
		EXIT
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck31, LBOOL31_APPLIED_INTRO_GAIT)
		EXIT
	ENDIF
	
	IF (IS_THIS_A_QUICK_RESTART_JOB() OR IS_CORONA_INITIALISING_A_QUICK_RESTART())
	AND FMMC_CHECKPOINTS_IS_ANY_RETRY_START_POINT_SET()
		PRINTLN("[RollingStart] - PROCESS_LOCAL_PLAYER_INTRO_GAIT - Not Applying intro walk. We are restarting at a checkpoint!")
		SET_BIT(iLocalBoolCheck31, LBOOL31_APPLIED_INTRO_GAIT)
	ENDIF
		
	IF (MC_playerBD[iLocalPart].iGameState >= GAME_STATE_INTRO_CUTSCENE AND (NOT IS_BIT_SET(iLocalBoolCheck33, LBOOL33_STARTED_MISSION_INTRO_SCENE) OR IS_BIT_SET(iLocalBoolCheck33, LBOOL33_FINISHED_MISSION_INTRO_SCENE)))
	OR (IS_THIS_A_QUICK_RESTART_JOB() OR IS_CORONA_INITIALISING_A_QUICK_RESTART())			
		IF (IS_SKYSWOOP_AT_GROUND() OR IS_SKYCAM_ON_LAST_CUT())
		AND NOT IS_SCREEN_FADED_OUT()
		AND GET_TOGGLE_PAUSED_RENDERPHASES_STATUS()
		AND NOT IS_CUTSCENE_ACTIVE()
			PROCESS_APPLY_INTRO_MOVE_FORWARD()
		ENDIF
	ENDIF
ENDPROC 

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Player State Setup 																						
// ##### Description: Functions which modify the attributes of the player or the way the world behaves around them. Player Proofs, Config Flags, Etc..
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC REFRESH_LOCAL_PLAYER_PROOFS()
	IF NOT IS_BIT_SET(iLocalBoolCheck25, LBOOL25_LOCAL_PLAYER_PROOFS_NEEDS_REFRESH)
		SET_BIT(iLocalBoolCheck25, LBOOL25_LOCAL_PLAYER_PROOFS_NEEDS_REFRESH)
	ENDIF
ENDPROC

PROC RUN_LOCAL_PLAYER_PROOF_SETTING(BOOL bForce = FALSE, BOOL bOverrideExplosionProof = FALSE)
	
	BOOL bBulletProof 	 = IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].iTeamBitset, ciBS_Player_BulletProofFlag)
	BOOL bFlameProof	 = IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].iTeamBitset, ciBS_Player_FlameProofFlag) OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iRuleBitsetFifteen[GET_TEAM_CURRENT_RULE(MC_playerBD[iLocalPart].iTeam)], ciBS_RULE15_SET_TEAM_AS_FIREPROOF)
	BOOL bExplosionProof = IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].iTeamBitset, ciBS_Player_ExplosionProofFlag) OR bOverrideExplosionProof
	BOOL bCollisionProof = IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].iTeamBitset, ciBS_Player_CollisionProofFlag)
	BOOL bMeleeProof 	 = IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].iTeamBitset, ciBS_Player_MeleeProofFlag)
	BOOL bSteamProof 	 = IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].iTeamBitset, ciBS_Player_SteamProofFlag)
	BOOL bSmokeProof 	 = IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].iTeamBitset, ciBS_Player_SmokeProofFlag)
	BOOL bForceEntry
	
	IF NOT bFlameProof
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			IF IS_ENTITY_ALIVE(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
				IF GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(LocalPlayerPed)) = SCARAB
				AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].iTeamBitset4, ciBS4_TEAM_VEHICLE_DISABLE_SCARAB_FIREPROOF)
					bForceEntry = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_FAKE_MULTIPLAYER_MODE_SET(TRUE) OR bForce
		IF bBulletProof OR bFlameProof OR bExplosionProof OR bCollisionProof OR bMeleeProof OR bSteamProof OR bSmokeProof OR bForce OR bForceEntry
			PRINTLN("[RCC MISSION] RUN_LOCAL_PLAYER_PROOF_SETTING - bullet ",bBulletProof,", flame ",bFlameProof,", explosion ",bExplosionProof,", collision ",bCollisionProof,", melee ",bMeleeProof,", steam ",bSteamProof,", smoke ",bSmokeProof)
			IF bLocalPlayerPedOK
				SET_ENTITY_PROOFS(LocalPlayerPed,bBulletProof,bFlameProof,bExplosionProof,bCollisionProof,bMeleeProof,bSteamProof,DEFAULT,bSmokeProof)
			#IF IS_DEBUG_BUILD
			ELSE
				PRINTLN("[RCC MISSION] RUN_LOCAL_PLAYER_PROOF_SETTING - player ped is injured!")
			#ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_FAKE_MULTIPLAYER_MODE_SET(TRUE) 
		SET_ENTITY_PROOFS(LocalPlayerPed,TRUE,TRUE,TRUE,TRUE,TRUE,TRUE,TRUE,TRUE) 
	ENDIF
		
ENDPROC

PROC SET_PLAYER_PROOFS_FOR_TEAM(INT iTeam)
	
	BOOL bBulletProof = IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitset, ciBS_Player_BulletProofFlag)
	BOOL bFlameProof = IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitset, ciBS_Player_FlameProofFlag)
	BOOL bExplosionProof = IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitset, ciBS_Player_ExplosionProofFlag)
	BOOL bCollisionProof = IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitset, ciBS_Player_CollisionProofFlag)
	BOOL bMeleeProof = IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitset, ciBS_Player_MeleeProofFlag)
	BOOL bSteamProof = IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitset, ciBS_Player_SteamProofFlag)
	BOOL bSmokeProof = IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitset, ciBS_Player_SmokeProofFlag)
	
	IF NOT IS_FAKE_MULTIPLAYER_MODE_SET(TRUE)
		PRINTLN("[RCC MISSION] SET_PLAYER_PROOFS_FOR_TEAM - bullet ",bBulletProof,", flame ",bFlameProof,", explosion ",bExplosionProof,", collision ",bCollisionProof,", melee ",bMeleeProof,", steam ",bSteamProof,", smoke ",bSmokeProof)
		IF bLocalPlayerPedOK
			SET_ENTITY_PROOFS(LocalPlayerPed,bBulletProof,bFlameProof,bExplosionProof,bCollisionProof,bMeleeProof,bSteamProof,DEFAULT,bSmokeProof)
		#IF IS_DEBUG_BUILD
		ELSE
			PRINTLN("[RCC MISSION] SET_PLAYER_PROOFS_FOR_TEAM - player ped is injured!")
		#ENDIF
		ENDIF
	ENDIF
	
ENDPROC


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Objective Blocker
// ##### Description: Helper functions related to the Objective Blocker system.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL IS_OBJECTIVE_BLOCKED_FOR_PARTICIPANT(INT iPart)
	RETURN IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_OBJECTIVE_BLOCKER)
ENDFUNC

FUNC BOOL IS_OBJECTIVE_BLOCKED()
	RETURN IS_OBJECTIVE_BLOCKED_FOR_PARTICIPANT(iPartToUse)
ENDFUNC

PROC BLOCK_OBJECTIVE_THIS_FRAME()
	IF NOT IS_OBJECTIVE_BLOCKED()
		SET_BIT(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_OBJECTIVE_BLOCKER)
		PRINTLN("[ObjectiveBlocker] BLOCK_OBJECTIVE_THIS_FRAME | PBBOOL_OBJECTIVE_BLOCKER is now set! Callstack below:")
		DEBUG_PRINTCALLSTACK()
	ELSE
		#IF IS_DEBUG_BUILD
		// It'd be too expensive to have a whole callstack in here every frame
		IF GET_FRAME_COUNT() % 60 = 0
			PRINTLN("[ObjectiveBlocker_SPAM] BLOCK_OBJECTIVE_THIS_FRAME | Objective is still blocked! Callstack below:")
			DEBUG_PRINTCALLSTACK()
		ELSE
			PRINTLN("[ObjectiveBlocker_SPAM] BLOCK_OBJECTIVE_THIS_FRAME | Objective is still blocked!")
		ENDIF
		#ENDIF
	ENDIF
	
	SET_BIT(iLocalBoolCheck30, LBOOL30_OBJECTIVE_BLOCKER_REQUESTED_THIS_FRAME)
ENDPROC

PROC CLEAR_OBJECTIVE_BLOCKER()
	CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_OBJECTIVE_BLOCKER)
	PRINTLN("[ObjectiveBlocker] CLEAR_OBJECTIVE_BLOCKER | Clearing PBBOOL_OBJECTIVE_BLOCKER!")
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Participants/Players
// ##### Description: Various helper functions & wrappers to do with participants or players
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC INT GET_TOTAL_STARTING_PLAYERS()
	
	IF IS_FAKE_MULTIPLAYER_MODE_SET()
		RETURN 1
	ENDIF

	IF MC_serverBD.iTotalNumStartingPlayers > 0
		RETURN MC_serverBD.iTotalNumStartingPlayers
	ENDIF
	
	INT iResult = MC_serverBD.iNumStartingPlayers[0] + MC_serverBD.iNumStartingPlayers[1] + MC_serverBD.iNumStartingPlayers[2] + MC_serverBD.iNumStartingPlayers[3]
	
	RETURN iResult
	
ENDFUNC

FUNC INT GET_TOTAL_PLAYING_PLAYERS()
	INT iResult = MC_serverBD.iNumberOfPlayingPlayers[0] + MC_serverBD.iNumberOfPlayingPlayers[1] + MC_serverBD.iNumberOfPlayingPlayers[2] + MC_serverBD.iNumberOfPlayingPlayers[3]
	RETURN iResult
ENDFUNC

FUNC INT GET_COOP_TEAMS_TOTAL_ALIVE_PLAYERS(INT iteam)

	INT iteamrepeat
	INT itotalAliveplayers

	FOR iteamrepeat = 0 TO (FMMC_MAX_TEAMS-1)
		IF DOES_TEAM_LIKE_TEAM(iteam,iteamrepeat)
			itotalAliveplayers = itotalAliveplayers + MC_serverBD.iNumberOfPlayingPlayers[iteamrepeat] 
		ENDIF
	ENDFOR
	
	RETURN itotalAliveplayers

ENDFUNC

PROC CACHE__LOBBY_HOST_PLAYER_INDEX()
	
	IF IS_BIT_SET(iLocalBoolCheck28, LBOOL28_FOUND_LOBBY_LEADER)
		EXIT
	ENDIF
	
	IF NOT bIsAnySpectator
	AND NOT DID_I_JOIN_MISSION_AS_SPECTATOR()
	AND NOT PLAYER_JOINED_IN_PROGRESS_MATCH(LocalPlayer)
		IF WAS_I_LOBBY_LEADER()
			IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_WAS_THE_LOBBY_LEADER)
				SET_BIT(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_WAS_THE_LOBBY_LEADER)
				PRINTLN("[RCC MISSION][LOBBYLEADER] CACHE_LOBBY_HOST_PLAYER_INDEX | Setting PBBOOL_WAS_THE_LOBBY_LEADER!")
			ENDIF
		ENDIF
	ELSE
		PRINTLN("[RCC MISSION][LOBBYLEADER] CACHE_LOBBY_HOST_PLAYER_INDEX | I'm a spectator or joined late, so I can't be the lobby leader!")
	ENDIF
	   
	INT iPart
	PRINTLN("[PLAYER_LOOP] - CACHE__LOBBY_HOST_PLAYER_INDEX")
	FOR iPart = 0 TO NUM_NETWORK_PLAYERS - 1
			
		PARTICIPANT_INDEX piThisParticipant = INT_TO_PARTICIPANTINDEX(iPart)
		
		IF NETWORK_IS_PARTICIPANT_ACTIVE(piThisParticipant)
			
			PLAYER_INDEX piThisPlayer = NETWORK_GET_PLAYER_INDEX(piThisParticipant)
			
			IF IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_WAS_THE_LOBBY_LEADER)
				g_piFMMCLobbyHostPlayer = piThisPlayer
				PRINTLN("[RCC MISSION][LOBBYLEADER] CACHE_LOBBY_HOST_PLAYER_INDEX | Lobby host is player ", iPart, " / ", GET_PLAYER_NAME(piThisPlayer))
				SET_BIT(iLocalBoolCheck28, LBOOL28_FOUND_LOBBY_LEADER)
				EXIT
			ELSE
				PRINTLN("[RCC MISSION][LOBBYLEADER] CACHE_LOBBY_HOST_PLAYER_INDEX | Player ", iPart, " / ", GET_PLAYER_NAME(piThisPlayer), " is not the lobby host")
			ENDIF
		ENDIF
	ENDFOR  
	
	PRINTLN("[RCC MISSION][LOBBYLEADER] CACHE_LOBBY_HOST_PLAYER_INDEX | Still looking for a lobby leader!")
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySeven, ciOptionsBS27_CriticalLobbyHost)
	AND GET_MC_SERVER_GAME_STATE() < GAME_STATE_RUNNING
		
		IF NOT HAS_NET_TIMER_STARTED(tdLobbyHostBailTimer)
			REINIT_NET_TIMER(tdLobbyHostBailTimer)
		ENDIF
		
		IF HAS_NET_TIMER_EXPIRED(tdLobbyHostBailTimer, ciLOBBY_HOST_BAIL_TIMER)
			IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
			AND IS_BIT_SET(g_FMMC_STRUCT.sMissionYachtInfo[ciYACHT_LOBBY_HOST_YACHT_INDEX].iMYacht_Bitset, ciYACHT_BITSET__MATCH_LOBBY_HOST_YACHT) 
				PRINTLN("[LOBBYLEADER] CACHE_LOBBY_HOST_PLAYER_INDEX | Clearing player owned Yacht - no lobby leader!")
				CLEAR_BIT(g_FMMC_STRUCT.sMissionYachtInfo[ciYACHT_LOBBY_HOST_YACHT_INDEX].iMYacht_Bitset, ciYACHT_BITSET__MATCH_LOBBY_HOST_YACHT) 
				sMissionYachtVars[ciYACHT_LOBBY_HOST_YACHT_INDEX].eMissionYachtSpawnState = FMMC_YACHT_SPAWN_STATE__INIT
				SET_BIT(iLocalBoolCheck12, LBOOL12_LOBBY_LEADER_QUIT_YACHT_CHANGED)
			ENDIF
		ELSE
			PRINTLN("[LOBBYLEADER] CACHE_LOBBY_HOST_PLAYER_INDEX | Time until bail: ", ciLOBBY_HOST_BAIL_TIMER - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdLobbyHostBailTimer))
		ENDIF
	
	ENDIF
	
ENDPROC

FUNC BOOL SHOULD_USE_CUSTOM_SPAWN_POINTS()
	RETURN IS_BIT_SET(iLocalBoolResetCheck1, LBOOL_RESET1_USE_CUSTOM_SPAWN_POINTS)
ENDFUNC

FUNC BOOL ARE_ANY_PLAYERS_ON_TEAM_DEAD_OR_BLOCKING_OBJECTIVE(INT iTeamToCheck)
	
	INT iPart = -1
	DO_PARTICIPANT_LOOP_FLAGS eFlags = GET_PARTICIPANT_LOOP_FLAGS_TO_CHECK_SINGLE_TEAM(iTeamToCheck)
	WHILE DO_PARTICIPANT_LOOP(iPart, eFlags | DPLF_FILL_PLAYER_IDS)
				
		PLAYER_INDEX piTemp = piParticipantLoop_PlayerIndex
		PED_INDEX pedTemp = piParticipantLoop_PedIndex				
		
		IF IS_OBJECTIVE_BLOCKED_FOR_PARTICIPANT(iPart)
		OR IS_PLAYER_DEAD(piTemp)
		OR IS_PED_DEAD_OR_DYING(pedTemp)
		OR IS_PED_INJURED(pedTemp)
		OR (IS_PLAYER_RESPAWNING(piTemp) AND NOT IS_FAKE_MULTIPLAYER_MODE_SET(TRUE))
			PRINTLN("ARE_ANY_PLAYERS_ON_TEAM_DEAD_OR_BLOCKING_OBJECTIVE - Participant ", iPart, " is dead or blocking objective!")
			RETURN TRUE
		ENDIF
		
	ENDWHILE
	
	RETURN FALSE
	
ENDFUNC

PROC DISABLE_PLAYER_MISSION_VEHICLE_CONTROLS_THIS_FRAME(BOOL bDisableExit = TRUE, BOOL bEnableShooting = FALSE, BOOL bDisableHydraulics = FALSE )
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_BRAKE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HANDBRAKE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_LR)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_UD)
	IF bDisableExit
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
	ENDIF
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
	IF NOT bEnableShooting
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_AIM)
	ENDIF
	IF bDisableHydraulics
		DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_VEH_HYDRAULICS_CONTROL_TOGGLE )
	ENDIF
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HEADLIGHT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HORN)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_NEXT_RADIO)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PREV_RADIO)
	
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_ROLL_LR)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_PITCH_UD)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_YAW_LEFT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_YAW_LEFT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_THROTTLE_UP)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_THROTTLE_DOWN)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_UNDERCARRIAGE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_SELECT_NEXT_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_SELECT_PREV_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_SELECT_TARGET_LEFT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_SELECT_TARGET_RIGHT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_VERTICAL_FLIGHT_MODE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ROCKET_BOOST)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_BOOST)
ENDPROC

FUNC BOOL IS_LOCAL_PED_DRIVING_VEHICLE_UNDERWATER(BOOL bSubMode = FALSE)	
	IF bLocalPlayerPedOK
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			VEHICLE_INDEX tempVeh = GET_VEHICLE_PED_IS_USING(LocalPlayerPed)			
			IF DOES_ENTITY_EXIST(tempVeh)	
				IF IS_ENTITY_IN_WATER(tempVeh)
					IF IS_PED_SITTING_IN_VEHICLE_SEAT(LocalPlayerPed,tempVeh,VS_DRIVER)
						IF bSubMode
							RETURN IS_VEHICLE_IN_SUBMARINE_MODE(tempVeh)
						ELSE
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_DROWNING()
	IF IS_PED_SWIMMING_UNDER_WATER(LocalPlayerPed)
		IF GET_PLAYER_UNDERWATER_TIME_REMAINING(LocalPlayer) <= 0
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

PROC MC_SET_LOCAL_PLAYER_AS_GHOST(BOOL bActivate)
	SET_LOCAL_PLAYER_AS_GHOST(bActivate, TRUE)	
	IF bActivate = TRUE
		SET_GHOST_ALPHA(50)
	ELSE
		RESET_GHOST_ALPHA()
	ENDIF
ENDPROC

PROC SET_LOCAL_PLAYER_FAILED(INT iCall)

	PRINTLN("[LM] - SET_LOCAL_PLAYER_FAILED - Setting TRUE - From iCall: ", iCall)
	
	SET_BIT(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_PLAYER_FAIL)
	
	SET_BIT(iLocalBoolCheck35, LBOOL35_APPLY_LIGHTS_OFF_TC_MODIFIER_FOR_SPECTATOR)
	
ENDPROC

FUNC BOOL IS_PARTICIPANT_A_JUGGERNAUT(INT iParticipant)
	RETURN IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iParticipant].iTeam].iTeamBitset2, ciBS2_JUGGERNAUTS)
		OR IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet3, PBBOOL3_TRANSFORMED_INTO_JUGGERNAUT)
ENDFUNC

FUNC BOOL IS_PARTICIPANT_A_BEAST(INT iParticipant)
	RETURN IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet3, PBBOOL3_TRANSFORMED_INTO_BEAST)
ENDFUNC

FUNC INT GET_NUMBER_OF_PARTICIPANTS_FROM_TEAM_IN_THIS_AREA(INT iLoc, INT iTeam)
	INT iPart, iNumberInArea
	PRINTLN("[PLAYER_LOOP] - GET_NUMBER_OF_PARTICIPANTS_FROM_TEAM_IN_THIS_AREA")
	FOR iPart = 0 TO (GET_MAXIMUM_PLAYERS_ON_MISSION() - 1)
		IF MC_PlayerBD[iPart].iteam = iTeam
			IF MC_playerBD[iPart].iCurrentLoc = iLoc
			AND NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPart))
				iNumberInArea++
			ENDIF
		ENDIF
	ENDFOR
	RETURN iNumberInArea
ENDFUNC

FUNC INT GET_NUMBER_OF_PLAYING_PLAYERS_ON_TEAMS(INT iTeamBS)
	INT i, iNumberOfPlayers
	FOR i = 0 TO FMMC_MAX_TEAMS - 1
		IF IS_BIT_SET(iTeamBS, i)
			iNumberOfPlayers += MC_serverBD.iNumberOfPlayingPlayers[i]
		ENDIF
	ENDFOR
	RETURN iNumberOfPlayers
ENDFUNC

FUNC INT GET_NUMBER_OF_PLAYING_PLAYERS()
	INT i, iNumberOfPlayers
	FOR i = 0 TO FMMC_MAX_TEAMS - 1
		iNumberOfPlayers = iNumberOfPlayers + MC_serverBD.iNumberOfPlayingPlayers[i]
	ENDFOR
	
	RETURN iNumberOfPlayers
ENDFUNC

FUNC INT GET_NUMBER_OF_PLAYERS_AND_SPECTATORS()
	INT i, iNumberOfPlayers
	FOR i = 0 TO FMMC_MAX_TEAMS - 1
		iNumberOfPlayers = iNumberOfPlayers + MC_serverBD.iNumberOfPlayersAndSpectators[i]
	ENDFOR
	
	RETURN iNumberOfPlayers
ENDFUNC

PROC FILL_ACTIVE_PARTICIPANT_INDEX_FROM_NUMBER_IN_TEAM(INT &iParts[], INT iTeam, BOOL bHostFirst)
	INT i, iCount
	INT iPartHost = GET_FMMC_LOBBY_LEADER_PARTICIPANT_INDEX()
	
	IF bHostFirst
	AND iPartHost != -1
		IF iTeam = MC_PlayerBD[iPartHost].iTeam		
			iParts[iCount] = iPartHost
			iCount++
		ENDIF
	ENDIF
	
	FOR i = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
		AND NOT IS_PLAYER_SPECTATOR_ONLY(INT_TO_PLAYERINDEX(i))
		AND IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(i))
		AND (NOT bHostFirst OR iPartHost != i)
			IF iTeam = MC_PlayerBD[i].iTeam								
				iParts[iCount] = i
				iCount++
			ENDIF
		ENDIF
	ENDFOR
	
ENDPROC

// Pass in that you want the "3rd" participant for example, and it will return their true participant number.
FUNC INT GET_ACTIVE_PARTICIPANT_INDEX_FROM_NUMBER(INT iParticipantNumber)
	INT i
	INT iParticipants = -1
	FOR i = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION() -1
		
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
		AND NOT IS_PLAYER_SPECTATOR_ONLY(INT_TO_PLAYERINDEX(i))
			PRINTLN("[RCC MISSION] GET_ACTIVE_PARTICIPANT_INDEX_FROM_NUMBER - Incrementing iParticipants")
			iParticipants++
		ENDIF
		
		PRINTLN("[RCC MISSION] GET_ACTIVE_PARTICIPANT_INDEX_FROM_NUMBER - Ordered Participant Number (1st 2nd etc) iParticipantNumber: ", iParticipantNumber, " i: ", i, " iParticipants: ", iParticipants)
		
		IF iParticipants = iParticipantNumber
			PRINTLN("[RCC MISSION] GET_ACTIVE_PARTICIPANT_INDEX_FROM_NUMBER - Ordered Participant Number Returning: ", i)
			RETURN i
		ENDIF
				
	ENDFOR
	
	RETURN -1
			
ENDFUNC

// Pass in that you want Participant 3, and it will return that they are the "2nd" participant (if participants 1 and 2 left for example)
FUNC INT GET_ORDERED_PARTICIPANT_NUMBER(INT iParticipantNumber)
	INT i
	INT iReturn = -1
	PRINTLN("[PLAYER_LOOP] - GET_ORDERED_PARTICIPANT_NUMBER")
	FOR i = 0 TO NUM_NETWORK_PLAYERS-1
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
		AND NOT IS_PLAYER_SPECTATOR_ONLY(INT_TO_PLAYERINDEX(i))
			iReturn++
			IF i = iParticipantNumber
				PRINTLN("[RCC MISSION] GET_ORDERED_PARTICIPANT_NUMBER - Participant ", iParticipantNumber, " Ordered Number: ", iReturn)
				RETURN iReturn
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN -1
			
ENDFUNC

FUNC INT GET_PARTICIPANT_NUMBER_IN_TEAM() 

	INT iplayer,iplayerGBD
	INT iMyteam
	PLAYER_INDEX tempPlayer
	INT ireturn

	IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
		iMyteam = GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].sClientCoronaData.iTeamChosen  
		PRINTLN("[RCC MISSION] GET_PARTICIPANT_NUMBER_IN_TEAM - local player team = ",iMyteam)
	ELSE
		RETURN 0
	ENDIF

	iteamstartPlayerJoinBitset = Get_Bitfield_Of_Players_On_Or_Joining_Mission(Get_UniqueID_For_This_Players_Mission(LocalPlayer))
	PRINTLN("[RCC MISSION] GET_PARTICIPANT_NUMBER_IN_TEAM - iteamstartPlayerJoinBitset = ", iteamstartPlayerJoinBitset)

	PRINTLN("[PLAYER_LOOP] - GET_PARTICIPANT_NUMBER_IN_TEAM")
	FOR iplayer = 0 TO (NUM_NETWORK_PLAYERS-1)
		PRINTLN("[RCC MISSION] GET_PARTICIPANT_NUMBER_IN_TEAM - iplayer = ",iplayer)
		tempPlayer = INT_TO_PLAYERINDEX(iplayer)
		
		IF IS_NET_PLAYER_OK(tempPlayer,FALSE)
			PRINTLN("[RCC MISSION] GET_PARTICIPANT_NUMBER_IN_TEAM - player ok = ",iplayer)
	       	IF tempPlayer!= LocalPlayer
				PRINTLN("[RCC MISSION] GET_PARTICIPANT_NUMBER_IN_TEAM - not local player ",iplayer)
				IF NOT IS_PLAYER_SCTV(tempPlayer)
					iplayerGBD = NATIVE_TO_INT(tempPlayer)
					PRINTLN("[RCC MISSION] GET_PARTICIPANT_NUMBER_IN_TEAM - native to int on player id: ",iplayerGBD)
					IF (IS_BIT_SET(iteamstartPlayerJoinBitset, iplayerGBD) OR DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(tempPlayer))
					AND NOT DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(tempPlayer)
					AND NOT IS_THIS_PLAYER_NOT_GOING_TO_TAKE_PART_IN_THIS_CORONA(tempPlayer)
						PRINTLN("[RCC MISSION] GET_PARTICIPANT_NUMBER_IN_TEAM - past iteamstartPlayerJoinBitset: ",iplayerGBD)						
						PRINTLN("[RCC MISSION] GET_PARTICIPANT_NUMBER_IN_TEAM - other player team = ",GlobalplayerBD_FM[iplayerGBD].sClientCoronaData.iTeamChosen)
						IF GlobalplayerBD_FM[iplayerGBD].sClientCoronaData.iTeamChosen = iMyteam
						AND (iMyTeam > -1 AND iMyTeam != 8)
							ireturn++
							PRINTLN("[RCC MISSION] GET_PARTICIPANT_NUMBER_IN_TEAM - past team check for player: ",iplayerGBD," return value is now: ",ireturn)
						ENDIF					
					ELSE
						PRINTLN("[RCC MISSION] GET_PARTICIPANT_NUMBER_IN_TEAM - ", iplayerGBD, " was not set in bitset iteamstartPlayerJoinBitset, which is currently: ", iteamstartPlayerJoinBitset," JIP: ", DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(tempPlayer))
					ENDIF
				ENDIF
			ELSE
				PRINTLN("[RCC MISSION] GET_PARTICIPANT_NUMBER_IN_TEAM - Player is local player ",iplayer)
				iplayer = NUM_NETWORK_PLAYERS
			ENDIF
		ENDIF
	ENDFOR

	PRINTLN("[RCC MISSION] GET_PARTICIPANT_NUMBER_IN_TEAM return value ", ireturn)
	RETURN ireturn
	
ENDFUNC

FUNC INT GET_PLAYER_TEAM_FOR_INTRO(INT iTeamOverride = -1)
	INT iTeam = MC_playerBD[iLocalPart].iTeam
	INT i
	
	IF iTeamOverride != -1
		iTeam = iTeamOverride
	ENDIF
	
	PRINTLN("[MMacK][TeamIntro] Player team is ", iTeam)
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciMOVE_TEAM_SPAWNS_EACH_ROUND)
	AND g_sTransitionSessionData.sMissionRoundData.iNumberOfRoundsPlayed > 0
		FOR i = 0 TO g_sTransitionSessionData.sMissionRoundData.iNumberOfRoundsPlayed-1
			iTeam++
			IF iTeam >= g_FMMC_STRUCT.iNumberOfTeams  
				iTeam = 0
			ENDIF
			PRINTLN("[MMacK][TeamIntro] Increased team for round ", i)
		ENDFOR
	ENDIF
	
	PRINTLN("[MMacK][TeamIntro] Set player team updated to ", iTeam)
	
	IF iTeam < 0 OR iTeam >= FMMC_MAX_TEAMS
		iTeam = 0
		PRINTLN("[MMacK][TeamIntro] something went very wrong, team set to default ", iTeam)
	ENDIF
	
	RETURN iTeam
ENDFUNC

FUNC INT GET_NUMBER_OF_ACTIVE_PARTICIPANTS()
	INT i, iActivePartCount
	PRINTLN("[PLAYER_LOOP] - GET_NUMBER_OF_ACTIVE_PARTICIPANTS")
	FOR i = 0 TO NETWORK_GET_MAX_NUM_PARTICIPANTS() - 1
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
		AND NOT IS_PARTICIPANT_A_SPECTATOR(i)
			iActivePartCount++
		ENDIF
	ENDFOR
	RETURN iActivePartCount
ENDFUNC

FUNC INT GET_CLOSEST_PARTICIPANT_NUMBER_ASCENDING(INT iStartFrom)
	INT iReturn = -1
	INT i
	INT iPart
	PRINTLN("[PLAYER_LOOP] - GET_CLOSEST_PARTICIPANT_NUMBER_ASCENDING")
	FOR i = 0 TO NUM_NETWORK_PLAYERS-1 
		iPart = i+iStartFrom
		
		IF iPart >= NUM_NETWORK_PLAYERS
			iPart -= NUM_NETWORK_PLAYERS
		ENDIF
		
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPart))
			IF NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_ANY_SPECTATOR)
			AND IS_BIT_SET(MC_playerBD[iPart].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
				iReturn = iPart
				BREAKLOOP
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN iReturn
ENDFUNC

FUNC BOOL SET_VEHICLE_HANDBRAKE_STATE(BOOL bOn)
	VEHICLE_INDEX vehPlayer

	IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		vehPlayer = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)

		IF NETWORK_HAS_CONTROL_OF_ENTITY(vehPlayer)
			IF bOn 
				PRINTLN("LIMIT RACE CONROLS - [SET_VEHICLE_HANDBRAKE_ON] VEHICLE_HANDBRAKE, ON")
			ELSE
				PRINTLN("LIMIT RACE CONROLS - [SET_VEHICLE_HANDBRAKE_ON] VEHICLE_HANDBRAKE, OFF")
			ENDIF

			SET_VEHICLE_HANDBRAKE(vehPlayer, bOn)
			RETURN TRUE
		ELSE
			PRINTLN("LIMIT RACE CONROLS - [SET_VEHICLE_HANDBRAKE_ON] VEHICLE_HANDBRAKE, NETWORK_HAS_CONTROL_OF_ENTITY")
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL OK_TO_DO_VEHICLE_BOOST()

	IF IS_BIT_SET(g_FMMC_STRUCT.bsRaceMission, ciDidSpeedBoost)
		RETURN FALSE
	ENDIF

	IF IS_RACE_PASSENGER()
		RETURN FALSE
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(g_FMMC_STRUCT.stBoostTimer)
	AND HAS_NET_TIMER_EXPIRED(g_FMMC_STRUCT.stBoostTimer, 1000)
		RESET_NET_TIMER(g_FMMC_STRUCT.stBoostTimer)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC DO_VEH_BOOST()
	IF HAS_NET_TIMER_STARTED(g_FMMC_STRUCT.stBoostTimer)
		IF OK_TO_DO_VEHICLE_BOOST()
			PRINTLN("[JS] DO_VEH_BOOST - Waiting for input")
			IF IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_ACCELERATE) 
				PRINTLN(">>>>>>>>>>>> OK_TO_DO_VEHICLE_BOOST, DO_VEH_BOOST, START_VEHICLE_BOOST")
				START_VEHICLE_BOOST(0.5)
				IF NOT IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitsetSix[0],  ciBS_RULE6_FILTERS_FORCE_FILTER)
					ANIMPOSTFX_PLAY("RaceTurbo", 0, FALSE)
				ENDIF
				SET_BIT(g_FMMC_STRUCT.bsRaceMission, ciDidSpeedBoost)
				INCREMENT_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_TURBO_STARTS_IN_RACE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC DISABLE_CONTROL_THIS_FRAME_ENABLE_HORN_AND_HOOD(BOOL bBlockCam = TRUE, BOOL bAllowRev = FALSE, BOOL bAllowRadio = FALSE , BOOL bHandBrakeOn = TRUE, BOOL bAllowHydro = FALSE)
	DISABLE_ALL_CONTROL_ACTIONS(PLAYER_CONTROL)
	IF bBlockCam
		DISABLE_ALL_CONTROL_ACTIONS(CAMERA_CONTROL)
	    PRINTLN("LIMIT RACE CONROLS - DISABLE_CONTROL_THIS_FRAME_ENABLE_HORN_AND_HOOD - DISABLE_CONTROL_THIS_FRAME_ENABLE_HORN_AND_HOOD - DISABLE_ALL_CONTROL_ACTIONS(CAMERA_CONTROL)")
	ELSE
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LR)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_UD)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_BEHIND)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_LOOK_BEHIND)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_LOOK_LEFT)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_LOOK_RIGHT)
	ENDIF
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HORN)
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SPECIAL)
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ROOF)	
	IF bAllowRadio
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_NEXT_RADIO)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PREV_RADIO)
		PRINTLN("LIMIT RACE CONROLS - bAllowRadio")
	ENDIF
	IF bAllowRev
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)	
		PRINTLN("LIMIT RACE CONROLS - bAllowRev")
	ENDIF
	IF bAllowHydro
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HYDRAULICS_CONTROL_TOGGLE)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HYDRAULICS_CONTROL_DOWN)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HYDRAULICS_CONTROL_LEFT)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HYDRAULICS_CONTROL_RIGHT)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HYDRAULICS_CONTROL_UP)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HYDRAULICS_CONTROL_UD)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HYDRAULICS_CONTROL_LR)
		PRINTLN("LIMIT RACE CONROLS - bAllowHydro")
	ENDIF
	
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)

	SET_VEHICLE_HANDBRAKE_STATE(bHandBrakeOn)

ENDPROC

PROC SET_PLAYER_ROLLING_START(PED_MOTION_STATE eOnFootSpeed = MS_ON_FOOT_WALK, FLOAT fVehSpeed = 6.0)
	
	IF NOT bLocalPlayerOK
		EXIT
	ENDIF
	
	// VEHICLE
	IF IS_PED_IN_ANY_VEHICLE(localPlayerPed, TRUE)
		
		VEHICLE_INDEX vehIndex = GET_VEHICLE_PED_IS_IN(localPlayerPed, TRUE)
		
		IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(vehIndex)
			PRINTLN("[RCC MISSION] SET_PLAYER_ROLLING_START - IN VEHICLE - No net control of entity")
			EXIT
		ENDIF
		
		SET_VEHICLE_ENGINE_ON(vehIndex, TRUE, TRUE, FALSE)
		
		MODEL_NAMES eModel = GET_ENTITY_MODEL(vehIndex)
		IF IS_THIS_MODEL_A_HELI(eModel)
		OR IS_THIS_MODEL_A_PLANE(eModel)
			PRINTLN("[RCC MISSION] SET_PLAYER_ROLLING_START - IN VEHICLE - Is heli, set blades full")
			SET_HELI_BLADES_FULL_SPEED(vehIndex)
		ENDIF
				
		SET_VEHICLE_LIMIT_SPEED_WHEN_PLAYER_INACTIVE(vehIndex, FALSE)
		SET_VEHICLE_STOP_INSTANTLY_WHEN_PLAYER_INACTIVE(vehIndex, FALSE)
		SET_VEHICLE_HANDBRAKE(vehIndex, FALSE)		
		FREEZE_ENTITY_POSITION(vehIndex, FALSE)
		
		SET_VEHICLE_FORWARD_SPEED(vehIndex, fVehSpeed)
		PRINTLN("[RCC MISSION] SET_PLAYER_ROLLING_START - IN VEHICLE - Complete with fVehSpeed = ", fVehSpeed)
	
	// ON FOOT
	ELSE
		INT iWalkTime = 2500
		FLOAT fMoveBlendRatio
		SWITCH eOnFootSpeed
			CASE MS_ON_FOOT_WALK	fMoveBlendRatio = PEDMOVEBLENDRATIO_WALK		BREAK
			CASE MS_ON_FOOT_RUN		fMoveBlendRatio = PEDMOVEBLENDRATIO_RUN			BREAK
			CASE MS_ON_FOOT_SPRINT	fMoveBlendRatio = PEDMOVEBLENDRATIO_SPRINT		BREAK
		ENDSWITCH
		
		FORCE_PED_MOTION_STATE(LocalPlayerPed, eOnFootSpeed) 
		SIMULATE_PLAYER_INPUT_GAIT(LocalPlayer, fMoveBlendRatio, iWalkTime, GET_ENTITY_HEADING(LocalPlayerPed), FALSE)
		PRINTLN("[RCC MISSION] SET_PLAYER_ROLLING_START - ON FOOT - Complete with fMoveBlendRatio = ", fMoveBlendRatio)
	ENDIF
	
ENDPROC

PROC PROCESS_RESTORE_PLAYER_WEAPON_AFTER_INTERACT_WITH()

	IF NOT IS_BIT_SET(iLocalBoolCheck27, LBOOL27_WEAPON_HAS_BEEN_HIDDEN_BY_INTERACTION)
		EXIT
	ENDIF
	
	IF HAS_PED_GOT_WEAPON(LocalPlayerPed, sInteractWithVars.wtInteractWith_StoredWeapon)   
		PRINTLN("[InteractWith] Setting weapon to be visible again because of sIWInfo.eInteractWith_CurrentState")
		SET_CURRENT_PED_WEAPON(LocalPlayerPed, sInteractWithVars.wtInteractWith_StoredWeapon, TRUE)
		CLEAR_BIT(iLocalBoolCheck27, LBOOL27_WEAPON_HAS_BEEN_HIDDEN_BY_INTERACTION)
	ENDIF
	
ENDPROC

PROC START_MINIGAME_DAMAGE_PROTECTION()
	PRINTLN("[LocalPlayer] START_MINIGAME_DAMAGE_PROTECTION - Starting!")
	DEBUG_PRINTCALLSTACK()
	REINIT_NET_TIMER(tdMinigameDamageReductionTimer)
	SET_PLAYER_WEAPON_DEFENSE_MODIFIER(LocalPlayer, 0.10005)
ENDPROC

//doesnt need a wrapper...
PROC STOP_LOCAL_PLAYER_FROM_GETTING_SHOT_THIS_FRAME()
	SET_PED_RESET_FLAG( LocalPlayerPed, PRF_CannotBeTargetedByAI, TRUE )
ENDPROC

FUNC BOOL IS_PLAYER_IN_A_FLYING_VEHICLE()

	IF IS_PED_IN_ANY_HELI( LocalPlayerPed )
	OR IS_PED_IN_ANY_PLANE( LocalPlayerPed )
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//rename GET_PART_FROM_ENTITY
FUNC INT GET_DAMAGER_PARTICIPANT_INDEX(ENTITY_INDEX eiDamager)
	PED_INDEX DamagerPed
	PLAYER_INDEX DamagerPlayer
	PARTICIPANT_INDEX DamagerParticipant
	INT iDamagerPartNumber
	
	IF IS_ENTITY_A_PED(eiDamager)
		DamagerPed = GET_PED_INDEX_FROM_ENTITY_INDEX(eiDamager)
		IF IS_PED_A_PLAYER(DamagerPed)
			DamagerPlayer = NETWORK_GET_PLAYER_INDEX_FROM_PED(DamagerPed)
			IF NETWORK_IS_PLAYER_A_PARTICIPANT(DamagerPlayer)
				DamagerParticipant = NETWORK_GET_PARTICIPANT_INDEX(DamagerPlayer)
				iDamagerPartNumber = NATIVE_TO_INT(DamagerParticipant)
			ENDIF
		ENDIF
	ELSE
		iDamagerPartNumber = -1
	ENDIF
	PRINTLN("[JT FLIP] Damager part = ", iDamagerPartNumber)
	RETURN iDamagerPartNumber
ENDFUNC

FUNC BOOL SHOULD_COUNT_PLAYER_DEATHS()

	#IF IS_DEBUG_BUILD
		IF bDisableDeathsCount
			RETURN FALSE
		ENDIF
	#ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck,LBOOL_COUNTED_FINAL_DEATH)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE

ENDFUNC

//remove, should never need this. It should be counted in the player loop
FUNC INT GET_REMAINING_PLAYERS_ON_TEAM(INT iTeam)
	INT i, iCount
	
	PRINTLN("[PLAYER_LOOP] - GET_REMAINING_PLAYERS_ON_TEAM")
	FOR i = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION() - 1
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
			IF NOT IS_PARTICIPANT_A_SPECTATOR(i)
			AND NOT IS_BIT_SET(MC_playerBD[i].iClientBitSet, PBBOOL_PLAYER_FAIL)
				IF iTeam = MC_PlayerBD[i].iteam
					iCount++
					
					IF iCount >= MC_serverBD.iNumStartingPlayers[iTeam]
						RETURN iCount
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	RETURN iCount
ENDFUNC

//remove, should never need this. It should be counted in the player loop
FUNC INT GET_PLAYERS_ON_TEAM(INT iTeam)
	INT i, iCount
	PRINTLN("[PLAYER_LOOP] - GET_PLAYERS_ON_TEAM")
	FOR i = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION() - 1
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
			PLAYER_INDEX piPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
			IF NOT DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(piPlayer)
				IF iTeam = MC_PlayerBD[i].iteam
					iCount++
					
					IF iCount >= MC_serverBD.iNumStartingPlayers[iTeam]
						RETURN iCount
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	RETURN iCount
ENDFUNC

FUNC PLAYER_INDEX GET_PARTICIPANT_PLAYER(INT iParticipant)
		
	IF iParticipant = iLocalPart
		RETURN LocalPlayer
	ENDIF
	
	PARTICIPANT_INDEX tempPart = INT_TO_PARTICIPANTINDEX(iParticipant)
	
	IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
		RETURN NETWORK_GET_PLAYER_INDEX(tempPart)
	ENDIF
	
	RETURN INVALID_PLAYER_INDEX()
ENDFUNC

FUNC PED_INDEX GET_PARTICIPANT_PED(INT iPart)
	
	IF iPart = iLocalPart
		RETURN localPlayerPed
	ENDIF
	
	IF iPart < 0
		RETURN NULL
	ENDIF
	
	PARTICIPANT_INDEX piPart = INT_TO_PARTICIPANTINDEX(iPart)
	IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(piPart)
		RETURN NULL
	ENDIF

	PLAYER_INDEX piPlayer = NETWORK_GET_PLAYER_INDEX(piPart)
	RETURN GET_PLAYER_PED(piPlayer)
ENDFUNC

FUNC INT GET_PARTICIPANT_FROM_PED_INDEX(PED_INDEX pedPlayer)
	
	IF pedPlayer = localPlayerPed
		RETURN iLocalPart
	ENDIF
	
	IF IS_PED_INJURED(pedPlayer)
		RETURN -1
	ENDIF
		
	IF NOT IS_PED_A_PLAYER(pedPlayer)
		RETURN -1
	ENDIF
	
	PLAYER_INDEX piPlayer = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedPlayer)
	PARTICIPANT_INDEX piPart = NETWORK_GET_PARTICIPANT_INDEX(piPlayer)

	RETURN NATIVE_TO_INT(piPart)
ENDFUNC

//Maybe refactor this to grab outfits from assets and check against a const
//e.g IS_LOCAL_PLAYER_IN_OUTFIT(CONST/ENUM BUGSTAR)
// Calls GET_OUTFIT_CATEGORY(local player outfit) which returns an enum to match
//Same for below
FUNC BOOL IS_LOCAL_PLAYER_WEARING_A_BUGSTAR_OUTFIT()
	IF MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_BUGSTARS_0)
	OR MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_BUGSTARS_1)
	OR MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_BUGSTARS_2)
	OR MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_BUGSTARS_3)
	OR MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_BUGSTARS_4)
	OR MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_BUGSTARS_5)
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_WEARING_A_MAINTENANCE_OUTFIT()
	IF MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_MAINTENANCE_0)
	OR MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_MAINTENANCE_1)
	OR MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_MAINTENANCE_2)
	OR MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_MAINTENANCE_3)
	OR MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_MAINTENANCE_4)
	OR MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_MAINTENANCE_5)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_WEARING_A_GRUPPE_SECHS_OUTFIT()
	IF MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_GRUPPE_SECHS_0)
	OR MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_GRUPPE_SECHS_1)
	OR MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_GRUPPE_SECHS_2)
	OR MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_GRUPPE_SECHS_3)
	OR MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_GRUPPE_SECHS_4)
	OR MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_GRUPPE_SECHS_5)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_WEARING_A_CELEB_OUTFIT()
	IF MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_CELEB_0)
	OR MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_CELEB_1)
	OR MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_CELEB_2)
	OR MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_CELEB_3)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_WEARING_A_FIREMAN_OUTFIT()
	IF MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_FIREFIGHTER_0)
	OR MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_FIREFIGHTER_1)
	OR MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_FIREFIGHTER_2)
	OR MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_FIREFIGHTER_3)
	OR MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_FIREFIGHTER_4)
	OR MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_FIREFIGHTER_5)
	OR MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_FIREFIGHTER_6)
	OR MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_FIREFIGHTER_7)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_WEARING_A_NOOSE_OUTFIT()
	IF MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_NOOSE_0)
	OR MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_NOOSE_1)
	OR MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_NOOSE_2)
	OR MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_NOOSE_3)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_WEARING_A_HIGH_ROLLER_OUTFIT()
	IF MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_HIGH_ROLLER_0)
	OR MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_HIGH_ROLLER_1)
	OR MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_HIGH_ROLLER_2)
	OR MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_HIGH_ROLLER_3)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_THIS_PLAYER_WEARING_AN_ISLAND_GUARD_OUTFIT(INT iPart)
	IF iPart != -1
		IF MC_playerBD[iPart].iOutfit = ENUM_TO_INT(OUTFIT_ISLAND_HEIST_GUARD_0)
		OR MC_playerBD[iPart].iOutfit = ENUM_TO_INT(OUTFIT_ISLAND_HEIST_GUARD_1)
		OR MC_playerBD[iPart].iOutfit = ENUM_TO_INT(OUTFIT_ISLAND_HEIST_GUARD_2)
		OR MC_playerBD[iPart].iOutfit = ENUM_TO_INT(OUTFIT_ISLAND_HEIST_GUARD_3)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_THIS_PLAYER_WEARING_AN_ISLAND_SMUGGLER_OUTFIT(INT iPart)
	IF iPart != -1
		IF MC_playerBD[iPart].iOutfit = ENUM_TO_INT(OUTFIT_ISLAND_HEIST_SMUGGLER_0)
		OR MC_playerBD[iPart].iOutfit = ENUM_TO_INT(OUTFIT_ISLAND_HEIST_SMUGGLER_1)
		OR MC_playerBD[iPart].iOutfit = ENUM_TO_INT(OUTFIT_ISLAND_HEIST_SMUGGLER_2)
		OR MC_playerBD[iPart].iOutfit = ENUM_TO_INT(OUTFIT_ISLAND_HEIST_SMUGGLER_3)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_THIS_PLAYER_WEARING_A_TUNER_SECURITY_OUTFIT(INT iPart)
	IF iPart != -1
		IF MC_playerBD[iPart].iOutfit = ENUM_TO_INT(OUTFIT_TUNER_SECURITY_0)
		OR MC_playerBD[iPart].iOutfit = ENUM_TO_INT(OUTFIT_TUNER_SECURITY_1)
		OR MC_playerBD[iPart].iOutfit = ENUM_TO_INT(OUTFIT_TUNER_SECURITY_2)
		OR MC_playerBD[iPart].iOutfit = ENUM_TO_INT(OUTFIT_TUNER_SECURITY_3)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_THIS_PLAYER_WEARING_AN_ULP_MAINTENANCE_OUTFIT(INT iPart)
	IF iPart != -1
		IF MC_playerBD[iPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_MAINTENANCE_0)
		OR MC_playerBD[iPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_MAINTENANCE_1)
		OR MC_playerBD[iPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_MAINTENANCE_4)
		OR MC_playerBD[iPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_MAINTENANCE_5)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_THIS_PLAYER_INSIDE_A_VALID_DISGUISE_VEHICLE_MODEL_TYPE(INT iPart, MODEL_NAMES mn)
	PED_INDEX pedPlayer = GET_PARTICIPANT_PED(iPart)
	
	IF IS_PED_INJURED(pedPlayer)
		RETURN FALSE
	ENDIF
		
	IF NOT IS_PED_IN_ANY_VEHICLE(pedPlayer)
		RETURN FALSE
	ENDIF
	
	VEHICLE_INDEX viVeh = GET_VEHICLE_PED_IS_IN(pedPlayer)
	
	IF NOT IS_VEHICLE_DRIVEABLE(viVeh)
		RETURN FALSE
	ENDIF
		
	RETURN GET_ENTITY_MODEL(viVeh) = mn
ENDFUNC

FUNC BOOL IS_THIS_PLAYER_INSIDE_A_VALID_DISGUISE_VEHICLE_INDEX(INT iPart, INT iVehicleBS)
	PED_INDEX pedPlayer = GET_PARTICIPANT_PED(iPart)
	
	IF IS_PED_INJURED(pedPlayer)
		RETURN FALSE
	ENDIF
	
	IF NOT IS_PED_IN_ANY_VEHICLE(pedPlayer, TRUE)
		RETURN FALSE
	ENDIF
	
	VEHICLE_INDEX viVeh = GET_VEHICLE_PED_IS_IN(pedPlayer, TRUE)
	
	IF NOT IS_VEHICLE_DRIVEABLE(viVeh)
		RETURN FALSE
	ENDIF
	
	INT iVeh = IS_ENTITY_A_MISSION_CREATOR_ENTITY(viVeh)
	
	IF iVeh = -1
		RETURN FALSE
	ENDIF
	
	RETURN IS_BIT_SET(iVehicleBS, iVeh)
ENDFUNC

FUNC BOOL IS_THIS_PLAYER_INSIDE_A_VALID_DISGUISE_PERSONAL_VEHICLE(INT iPart)
	PED_INDEX pedPlayer = GET_PARTICIPANT_PED(iPart)
	
	IF IS_PED_INJURED(pedPlayer)
		RETURN FALSE
	ENDIF
		
	IF NOT IS_PED_IN_ANY_VEHICLE(pedPlayer, TRUE)	
		RETURN FALSE
	ENDIF
	
	VEHICLE_INDEX viVeh = GET_VEHICLE_PED_IS_IN(pedPlayer, TRUE)
	
	IF NOT IS_VEHICLE_DRIVEABLE(viVeh)
		RETURN FALSE
	ENDIF
		
	// If we're in any personal vehicle then return true.
	INT iPartLoop = -1
	WHILE DO_PARTICIPANT_LOOP(iPartLoop)		
		IF NOT DOES_ENTITY_EXIST(MPGlobals.RemotePV[iPartLoop])	
			RELOOP
		ENDIF
		
		IF viVeh = MPGlobals.RemotePV[iPartLoop]
			RETURN TRUE
		ENDIF
	ENDWHILE
		
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_WEARING_COP_OUTFIT(INT iPart)
	
	BOOL bCop = FALSE
	
	SWITCH INT_TO_ENUM(MP_OUTFIT_ENUM, MC_playerBD[iPart].iOutfit)
		
		CASE OUTFIT_HEIST_POLICE_0
		CASE OUTFIT_HEIST_PRISON_OFFICER_0
		CASE OUTFIT_PRISON_POLICE_OFFICER_VS
			bCop = TRUE
		BREAK
		
	ENDSWITCH
	
	RETURN bCop
	
ENDFUNC

FUNC BOOL IS_PLAYER_WEARING_SCUBA_GEAR(INT iPart)
	RETURN INT_TO_ENUM(MP_OUTFIT_ENUM, MC_playerBD[iPart].iOutfit) = OUTFIT_GANGOPS_HEIST_SCUBA
ENDFUNC

PROC APPLY_NIGHTVISION_FROM_TEAM_GEAR_IF_AVAILABLE(INT iTeam)
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitset4, ciBS4_TEAM_GIVE_NIGHTVISION_IF_AVAILABLE)
		IF IS_LONG_BIT_SET(g_FMMC_STRUCT.biGearAvailableBitset[iTeam], ENUM_TO_INT(HEIST_GEAR_NIGHTVISION)) //Used Externally IS_LONG_BIT_SET
			IF NOT IS_MP_HEIST_GEAR_EQUIPPED(LocalPlayerPed, HEIST_GEAR_NIGHTVISION)
				SET_MP_HEIST_GEAR(LocalPlayerPed, HEIST_GEAR_NIGHTVISION)
				PRINTLN("APPLY_NIGHTVISION_FROM_TEAM_GEAR_IF_AVAILABLE - Applying: HEIST_GEAR_NIGHTVISION")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC CACHE_HEIST_GEAR(INT iGear, MP_HEIST_GEAR_ENUM eNewGear = HEIST_GEAR_NONE)
	
	IF iGear != -1
	AND eNewGear = HEIST_GEAR_NONE
		eNewGear = INT_TO_ENUM(MP_HEIST_GEAR_ENUM, iGear)
	ENDIF
	
	IF sApplyOutfitData.eGear != eNewGear
		PRINTLN("CACHE_HEIST_GEAR - Updating heist gear from ", sApplyOutfitData.eGear, " to ", eNewGear)
		sApplyOutfitData.eGear = eNewGear
	ENDIF
ENDPROC

FUNC BOOL SHOULD_APPLY_DUFFEL_BAG_HEIST_GEAR()
	
	IF IS_PED_WEARING_SCUBA_TANK(LocalPlayerPed)
		PRINTLN("SHOULD_APPLY_DUFFEL_BAG_HEIST_GEAR - Returning FALSE - Scuba Tank")
		SET_BIT(iLocalBoolCheck27, LBOOL27_BAG_REMOVED_FOR_BACK_GEAR)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC APPLY_DUFFEL_BAG_HEIST_GEAR(PED_INDEX piPed, INT iPart)
	
	IF NOT SHOULD_APPLY_DUFFEL_BAG_HEIST_GEAR()
		PRINTLN("[HeistBag] APPLY_DUFFEL_BAG_HEIST_GEAR Exiting")
		EXIT
	ENDIF
	
	MP_HEIST_GEAR_ENUM eBag = HEIST_GEAR_SPORTS_BAG
	
	IF iPart > -1
		eBag = MC_playerBD_1[iPart].eHeistGearBagType
	ENDIF
	PRINTLN("[HeistBag] APPLY_DUFFEL_BAG_HEIST_GEAR called")
	SET_MP_HEIST_GEAR(piPed, eBag) 
ENDPROC
PROC REMOVE_DUFFEL_BAG_HEIST_GEAR(PED_INDEX piPed, INT iPart)
	MP_HEIST_GEAR_ENUM eBag = HEIST_GEAR_SPORTS_BAG
	
	IF iPart > -1
		eBag = MC_playerBD_1[iPart].eHeistGearBagType
	ENDIF
	PRINTLN("[HeistBag] REMOVE_DUFFEL_BAG_HEIST_GEAR called")
	REMOVE_MP_HEIST_GEAR(piPed, eBag) 
ENDPROC
PROC PRELOAD_DUFFEL_BAG_HEIST_GEAR(PED_INDEX piPed, INT iPart)
	MP_HEIST_GEAR_ENUM eBag = HEIST_GEAR_SPORTS_BAG
	
	IF iPart > -1
		eBag = MC_playerBD_1[iPart].eHeistGearBagType
	ENDIF
	PRINTLN("[HeistBag] PRELOAD_DUFFEL_BAG_HEIST_GEAR called")
	PRELOAD_MP_HEIST_GEAR(piPed, eBag) 
ENDPROC

FUNC BOOL IS_OUTFIT_CORRECT_TO_USE_ARMOURED_BAG()
	
	SWITCH GET_STYLE_FROM_OUTFIT(MC_playerBD[iLocalPart].iOutfit)
		CASE OUTFIT_STYLE_CASINO_HEIST_DIRECT_LIGHT_I
		CASE OUTFIT_STYLE_CASINO_HEIST_DIRECT_LIGHT_II
		CASE OUTFIT_STYLE_CASINO_HEIST_DIRECT_LIGHT_III
			RETURN FALSE
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC BOOL FORCE_OUTFIT_TO_USE_ARMOURED_BAG()

	SWITCH GET_STYLE_FROM_OUTFIT(MC_playerBD[iLocalPart].iOutfit)
		CASE OUTFIT_STYLE_ISLAND_HEIST_LIGHT_II
		CASE OUTFIT_STYLE_ISLAND_HEIST_HEAVY_I
		CASE OUTFIT_STYLE_ISLAND_HEIST_HEAVY_II
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC SET_BAG_STYLE_FOR_CURRENT_OUTFIT(INT iTeam)
	
	MP_OUTFIT_ENUM eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, MC_playerBD[iLocalPart].iOutfit)
	
	IF MC_playerBD[iLocalPart].iOutfit = -1
	AND sApplyOutfitData.eOutfit != OUTFIT_MP_DEFAULT
		eOutfit = sApplyOutfitData.eOutfit
	ENDIF
	
	MP_HEIST_GEAR_ENUM eOutfitBag = HEIST_GEAR_NONE

	SWITCH eOutfit
		CASE OUTFIT_ISLAND_HEIST_SMUGGLER_0
		CASE OUTFIT_ISLAND_HEIST_SMUGGLER_1
		CASE OUTFIT_ISLAND_HEIST_SMUGGLER_2
		CASE OUTFIT_ISLAND_HEIST_SMUGGLER_3
			eOutfitBag = HEIST_GEAR_DUFFEL_BLACK_FULL
		BREAK
		CASE OUTFIT_ISLAND_HEIST_HEAVY_1_0
		CASE OUTFIT_ISLAND_HEIST_HEAVY_1_1
		CASE OUTFIT_ISLAND_HEIST_HEAVY_1_2
		CASE OUTFIT_ISLAND_HEIST_HEAVY_1_3
			eOutfitBag = HEIST_GEAR_DUFFEL_CELEB_FULL
		BREAK
		CASE OUTFIT_ISLAND_HEIST_HEAVY_2_0
		CASE OUTFIT_ISLAND_HEIST_HEAVY_2_1
		CASE OUTFIT_ISLAND_HEIST_HEAVY_2_2
		CASE OUTFIT_ISLAND_HEIST_HEAVY_2_3
			eOutfitBag = HEIST_GEAR_DUFFEL_CAMO_2_FULL
		BREAK
		CASE OUTFIT_ISLAND_HEIST_LIGHT_1_0
		CASE OUTFIT_ISLAND_HEIST_LIGHT_1_1
		CASE OUTFIT_ISLAND_HEIST_LIGHT_1_2
		CASE OUTFIT_ISLAND_HEIST_LIGHT_1_3
			eOutfitBag = HEIST_GEAR_DUFFEL_RED_FULL
		BREAK
		CASE OUTFIT_ISLAND_HEIST_LIGHT_2_0
		CASE OUTFIT_ISLAND_HEIST_LIGHT_2_1
		CASE OUTFIT_ISLAND_HEIST_LIGHT_2_2
		CASE OUTFIT_ISLAND_HEIST_LIGHT_2_3
			eOutfitBag = HEIST_GEAR_DUFFEL_CAMO_4_FULL
		BREAK
		CASE OUTFIT_ISLAND_HEIST_STEALTH_1_0
		CASE OUTFIT_ISLAND_HEIST_STEALTH_1_1
		CASE OUTFIT_ISLAND_HEIST_STEALTH_1_2
		CASE OUTFIT_ISLAND_HEIST_STEALTH_1_3
			eOutfitBag = HEIST_GEAR_DUFFEL_BLACK_FULL
		BREAK
		CASE OUTFIT_ISLAND_HEIST_STEALTH_2_0
		CASE OUTFIT_ISLAND_HEIST_STEALTH_2_1
		CASE OUTFIT_ISLAND_HEIST_STEALTH_2_2
		CASE OUTFIT_ISLAND_HEIST_STEALTH_2_3
			eOutfitBag = HEIST_GEAR_DUFFEL_CAMO_6_FULL
		BREAK
	ENDSWITCH
	
	IF eOutfitBag != HEIST_GEAR_NONE
		CACHE_HEIST_GEAR(-1, eOutfitBag)
		g_FMMC_STRUCT.iGearDefault[iTeam] = ENUM_TO_INT(sApplyOutfitData.eGear)
		PRINTLN("[HeistBag] SET_BAG_STYLE_FOR_CURRENT_OUTFIT - Updating bag to ", eOutfitBag)
	ENDIF
ENDPROC

PROC SET_BAG_FOR_HEIST(INT iTeam, BOOL bUpdate = FALSE, INT iCustomBag = -1)
	
	IF NOT IS_TEAM_INDEX_VALID(iTeam)
		EXIT
	ENDIF
	INT iBag = g_FMMC_STRUCT.iGearDefault[iTeam]
	IF iCustomBag != -1
		iBag = iCustomBag
	ENDIF
	
	CACHE_HEIST_GEAR(iBag)
	PRINTLN("[HeistBag] SET_BAG_FOR_HEIST - sApplyOutfitData.eGear is now: ", sApplyOutfitData.eGear)
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitset4, ciBS4_TEAM_USE_BAG_VARIATION)		
		
		IF sApplyOutfitData.eGear = HEIST_GEAR_SPORTS_BAG
			sApplyOutfitData.eGear = HEIST_GEAR_DUFFEL_BLACK_FULL
			CACHE_HEIST_GEAR(-1, sApplyOutfitData.eGear)
			g_FMMC_STRUCT.iGearDefault[iTeam] = ENUM_TO_INT(sApplyOutfitData.eGear)
			PRINTLN("[HeistBag] SET_BAG_FOR_HEIST - Basic Duffel Bag Update - sApplyOutfitData.eGear is now: ", sApplyOutfitData.eGear)
		ENDIF
		
		IF bUpdate
			CACHE_HEIST_GEAR(g_FMMC_STRUCT.iGearDefault[iTeam])
			PRINTLN("[HeistBag] SET_BAG_FOR_HEIST - bUpdate - sApplyOutfitData.eGear is now: ", sApplyOutfitData.eGear)
		ELIF sMissionLocalContinuityVars.iHeistBag != -1
			CACHE_HEIST_GEAR(sMissionLocalContinuityVars.iHeistBag)
			g_FMMC_STRUCT.iGearDefault[iTeam] = ENUM_TO_INT(sApplyOutfitData.eGear)
			PRINTLN("[HeistBag] SET_BAG_FOR_HEIST - Continuity - sApplyOutfitData.eGear is now: ", sApplyOutfitData.eGear)
		ENDIF
		
		SET_BAG_STYLE_FOR_CURRENT_OUTFIT(iTeam)
		
		IF (IS_PED_WEARING_ARMOUR(LocalPlayerPed) OR FORCE_OUTFIT_TO_USE_ARMOURED_BAG())
		AND IS_OUTFIT_CORRECT_TO_USE_ARMOURED_BAG()
			IF GET_HEIST_BAG_ARMOURED_VERSION(sApplyOutfitData.eGear) != HEIST_GEAR_NONE
				CACHE_HEIST_GEAR(-1, GET_HEIST_BAG_ARMOURED_VERSION(sApplyOutfitData.eGear))
				g_FMMC_STRUCT.iGearDefault[iTeam] = ENUM_TO_INT(sApplyOutfitData.eGear)
				PRINTLN("[HeistBag] SET_BAG_FOR_HEIST - Armoured Ped - sApplyOutfitData.eGear is now: ", sApplyOutfitData.eGear)
			ELSE
				PRINTLN("[HeistBag] SET_BAG_FOR_HEIST - No armoured bag variant, leaving bag as is.")
			ENDIF
		ELSE
			IF IS_HEIST_BAG_ARMOURED_VERSION(sApplyOutfitData.eGear)
				INT iGear = ENUM_TO_INT(sApplyOutfitData.eGear)
				iGear -= 2
				CACHE_HEIST_GEAR(iGear)
				g_FMMC_STRUCT.iGearDefault[iTeam] = iGear
				PRINTLN("[HeistBag] SET_BAG_FOR_HEIST - Non Armoured Ped was trying to use armoured bag - sApplyOutfitData.eGear is now: ", sApplyOutfitData.eGear)
			ELSE
				PRINTLN("[HeistBag] SET_BAG_FOR_HEIST - Not wearing armour, leaving bag as is.")
			ENDIF
		ENDIF
		
		IF NOT IS_LONG_BIT_SET(g_FMMC_STRUCT.biGearAvailableBitset[iTeam], g_FMMC_STRUCT.iGearDefault[iTeam]) //Used Externally IS_LONG_BIT_SET
			SET_LONG_BIT(g_FMMC_STRUCT.biGearAvailableBitset[iTeam], g_FMMC_STRUCT.iGearDefault[iTeam]) //Used Externally SET_LONG_BIT
			PRINTLN("[HeistBag] SET_BAG_FOR_HEIST - Setting available long bitset for new bag as it wasn't set.")
		ENDIF
		
	ENDIF
ENDPROC

PROC SET_CACHED_HEIST_GEAR_BAG()
	
	IF IS_HEIST_GEAR_A_BAG(sApplyOutfitData.eGear)
		DEBUG_PRINTCALLSTACK()
		MC_playerBD_1[iLocalPart].eHeistGearBagType = sApplyOutfitData.eGear
		MC_playerBD_1[iLocalPart].mnHeistGearBag = GET_HEIST_BAG_AS_MODEL(MC_playerBD_1[iLocalPart].eHeistGearBagType)
		PRINTLN("SET_CACHED_HEIST_GEAR_BAG - MC_playerBD_1[iLocalPart].mnHeistGearBag: ", FMMC_GET_MODEL_NAME_FOR_DEBUG(MC_playerBD_1[iLocalPart].mnHeistGearBag))
		IF sMissionLocalContinuityVars.iHeistBag != ENUM_TO_INT(MC_playerBD_1[iLocalPart].eHeistGearBagType)
		AND ENUM_TO_INT(MC_playerBD_1[iLocalPart].eHeistGearBagType) != -1
			sMissionLocalContinuityVars.iHeistBag = ENUM_TO_INT(MC_playerBD_1[iLocalPart].eHeistGearBagType)
			PRINTLN("[HeistBag] SET_CACHED_HEIST_GEAR_BAG - sMissionLocalContinuityVars.iHeistBag: ", sMissionLocalContinuityVars.iHeistBag)
		ENDIF
		
		PRINTLN("[HeistBag] SET_CACHED_HEIST_GEAR_BAG - eHeistGearBagType now ", MC_playerBD_1[iLocalPart].eHeistGearBagType)
	ENDIF

ENDPROC

FUNC BOOL GIVE_AND_CACHE_LOCAL_PLAYER_HEIST_BAG(INT iTeam)
	
	IF iTeam = -1
		RETURN FALSE
	ENDIF
	
	IF g_FMMC_STRUCT.iGearDefault[iTeam] = -1
		RETURN FALSE
	ENDIF
	
	IF NOT IS_HEIST_GEAR_A_BAG(sApplyOutfitData.eGear)
		RETURN FALSE
	ENDIF
	
	SET_BAG_FOR_HEIST(iTeam)
	SET_CACHED_HEIST_GEAR_BAG()
	
	IF NOT IS_MP_HEIST_GEAR_EQUIPPED(LocalPlayerPed, MC_playerBD_1[iLocalPart].eHeistGearBagType)
		APPLY_DUFFEL_BAG_HEIST_GEAR(LocalPlayerPed, iLocalPart)
		PRINTLN("[HeistBag] GIVE_AND_CACHE_LOCAL_PLAYER_HEIST_BAG - Applying heist bag!")
		RETURN TRUE
	#IF IS_DEBUG_BUILD
	ELSE
		PRINTLN("[HeistBag] GIVE_AND_CACHE_LOCAL_PLAYER_HEIST_BAG - Already has the bag on!")
	#ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_CURRENT_OUTFIT_MASK_MONOCULAR_NIGHTVISION()

	SWITCH sApplyOutfitData.eMask
		CASE MASK_ISLAND_HEIST_MONO_NIGHT_VISION_0
		CASE MASK_ISLAND_HEIST_MONO_NIGHT_VISION_1
		CASE MASK_ISLAND_HEIST_MONO_NIGHT_VISION_2
		CASE MASK_ISLAND_HEIST_MONO_NIGHT_VISION_3
		CASE MASK_ISLAND_HEIST_MONO_NIGHT_VISION_4
		CASE MASK_ISLAND_HEIST_MONO_NIGHT_VISION_5
		CASE MASK_ISLAND_HEIST_MONO_NIGHT_VISION_6
		CASE MASK_ISLAND_HEIST_MONO_NIGHT_VISION_7
		
		CASE MASK_ISLAND_HEIST_NIGHT_VISION_0
		CASE MASK_ISLAND_HEIST_NIGHT_VISION_3
			RETURN TRUE
			
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_CURRENT_OUTFIT_MASK_NIGHTVISION()

	IF IS_CURRENT_OUTFIT_MASK_MONOCULAR_NIGHTVISION()
		RETURN TRUE
	ENDIF
	
	SWITCH sApplyOutfitData.eMask
		CASE MASK_ISLAND_HEIST_NIGHT_VISION_0
		CASE MASK_ISLAND_HEIST_NIGHT_VISION_1
		CASE MASK_ISLAND_HEIST_NIGHT_VISION_2
		CASE MASK_ISLAND_HEIST_NIGHT_VISION_3
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC DELAY_QUICK_EQUIP_HELPTEXT()
	REINIT_NET_TIMER(stQuickEquipHelptextDelayTimer)
	PRINTLN("[Mask] DELAY_QUICK_EQUIP_HELPTEXT - Starting stQuickEquipHelptextDelayTimer")
ENDPROC

FUNC BOOL IS_PLAYER_WEARING_NIGHT_VISION_HELMET_WITH_MOVABLE_VISOR()

	RETURN IS_CURRENT_OUTFIT_MASK_NIGHTVISION() AND NOT IS_CURRENT_OUTFIT_MASK_MONOCULAR_NIGHTVISION()

	// Generic Proper Check to be Implemented Later
//	INT iItemHash = GET_HASH_NAME_FOR_PROP(NATIVE_TO_INT(PLAYER_PED_ID()), ENUM_TO_INT(ANCHOR_HEAD), GET_PED_PROP_INDEX(PLAYER_PED_ID(), ANCHOR_HEAD), GET_PED_PROP_TEXTURE_INDEX(PLAYER_PED_ID(), ANCHOR_HEAD))
//	
//	IF DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(iItemHash, DLC_RESTRICTION_TAG_HELMET, ENUM_TO_INT(SHOP_PED_PROP))
//	AND GET_SHOP_PED_APPAREL_VARIANT_PROP_COUNT(iItemHash) > 0
//		PRINTLN("[PLAYER VISUAL AIDS] IS_PLAYER_WEARING_NIGHT_VISION_HELMET_WITH_MOVABLE_VISOR | Returning TRUE due to iItemHash being ", iItemHash)
//		RETURN TRUE
//	ELSE
//		PRINTLN("[PLAYER VISUAL AIDS] IS_PLAYER_WEARING_NIGHT_VISION_HELMET_WITH_MOVABLE_VISOR | Returning FALSE due to iItemHash being 0")
//		RETURN FALSE
//	ENDIF
ENDFUNC


FUNC BOOL IS_PLAYER_IN_LOCATION_SIMPLE(INT iLoc, PED_INDEX piPlayerPed)
	IF NOT IS_PED_INJURED(piPlayerPed)
		IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].vloc1)
		AND NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].vloc2)
			IF IS_ENTITY_IN_ANGLED_AREA(piPlayerPed,g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].vloc1,g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].vloc2,g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].fWidth)
				PRINTLN("[RCC MISSION] IS_PLAYER_IN_LOCATION_SIMPLE - RETURNING TRUE 1")
				RETURN TRUE
			ENDIF
		ELSE
			VECTOR vLoc = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].vloc[0]
			
			FLOAT fDist = POW(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].fRadius[0], 2)
			
			IF VDIST2(GET_ENTITY_COORDS(piPlayerPed),vLoc) < fDist
				PRINTLN("[RCC MISSION] IS_PLAYER_IN_LOCATION_SIMPLE - RETURNING TRUE 2")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//WHy ped? Should be player and checked in object loop
FUNC BOOL IS_PED_CARRYING_ANY_OBJECTS(PED_INDEX pedToCheck)
	INT iObj
	FOR iObj = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfObjects-1
		IF NETWORK_DOES_NETWORK_ID_EXIST(GET_OBJECT_NET_ID(iObj))
			IF IS_ENTITY_ATTACHED_TO_ENTITY(NET_TO_OBJ(GET_OBJECT_NET_ID(iObj)), pedToCheck)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDFOR
	RETURN FALSE
ENDFUNC

//Should be done in player loop
FUNC PLAYER_INDEX GET_PLAYER_IN_CAPTURE_AREA(INT iLoc, BOOL bGetLone = TRUE)
	INT iTeam, iPlayer
	FOR iTeam = 0 TO (FMMC_MAX_TEAMS-1)
		IF (MC_serverBD_1.inumberOfTeamInArea[iLoc][iteam] = 1 OR NOT bGetLone)
		AND MC_serverBD.iLocOwner[iLoc] != iTeam
			PRINTLN("[PLAYER_LOOP] - GET_PLAYER_IN_CAPTURE_AREA")
			FOR iPlayer = 0 TO (GET_MAXIMUM_PLAYERS_ON_MISSION()-1)
				IF IS_PLAYER_IN_LOCATION_SIMPLE(iLoc, GET_PLAYER_PED(INT_TO_PLAYERINDEX(iPlayer)))
				AND NOT IS_PED_INJURED(GET_PLAYER_PED(INT_TO_PLAYERINDEX(iPlayer)))
					PRINTLN("[RCC MISSION] GET_PLAYER_IN_CAPTURE_AREA returning player: ", iplayer)
					RETURN INT_TO_PLAYERINDEX(iPlayer)
				ENDIF
			ENDFOR
		ENDIF
	ENDFOR
	
	RETURN INVALID_PLAYER_INDEX()
ENDFUNC

FUNC VISUAL_AID_MODES GET_PLAYER_VISUAL_AID_MODE()
	IF GET_USINGNIGHTVISION()
		RETURN VISUALAID_NIGHT
	ELIF GET_USINGSEETHROUGH()
		RETURN VISUALAID_THERMAL
	ELSE
		RETURN VISUALAID_OFF
	ENDIF
ENDFUNC

FUNC BOOL IS_PERSONAL_VEHICLE_FUNCTIONALITY_DISABLED_IN_MISSIONS(BOOL bEntireMission = FALSE)
	
	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	IF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(3) AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_DisablePersonalVehOnCheckpoint_3)
		PRINTLN("IS_PERSONAL_VEHICLE_FUNCTIONALITY_DISABLED_IN_MISSIONS - Checkpoint 3")
		RETURN TRUE
	ENDIF
	
	IF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(2) AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciOptionsBS17_DisablePersonalVehOnCheckpoint_2)
		PRINTLN("IS_PERSONAL_VEHICLE_FUNCTIONALITY_DISABLED_IN_MISSIONS - Checkpoint 2")
		RETURN TRUE
	ENDIF
	
	IF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(1) AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciDISABLE_PERSONAL_VEH_CHECKPOINT_1)
		PRINTLN("IS_PERSONAL_VEHICLE_FUNCTIONALITY_DISABLED_IN_MISSIONS - Checkpoint 1")
		RETURN TRUE
	ENDIF
	
	IF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(0) AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciDISABLE_PERSONAL_VEH_CHECKPOINT_0)
		PRINTLN("IS_PERSONAL_VEHICLE_FUNCTIONALITY_DISABLED_IN_MISSIONS - Checkpoint 0")
		RETURN TRUE
	ENDIF
	
	IF IS_PERSONAL_VEHICLE_DISABLED_IN_MISSION()
		PRINTLN("IS_PERSONAL_VEHICLE_FUNCTIONALITY_DISABLED_IN_MISSIONS - Disable Personal Veh")
		RETURN TRUE
	ENDIF
	
	IF iRule < FMMC_MAX_RULES 
	AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThree[iRule], ciBS_RULE3_DISABLE_PV_SPAWNING)
	AND NOT bEntireMission
		PRINTLN("IS_PERSONAL_VEHICLE_FUNCTIONALITY_DISABLED_IN_MISSIONS - Rule Setting - iRule: ", iRule)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL SHOULD_EXPLODE_ON_BOUNDS_FAIL(INT iTeam, INT iRule)
	RETURN IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFour[iRule], ciBS_RULE4_EXPLODE_ON_BOUNDS_FAIL)
ENDFUNC

FUNC INT GET_BOUNDS_TIME_LIMIT(RULE_BOUNDS_STRUCT& sBounds)
	
	INT iTimeLimit = sBounds.iRuleBoundsTime
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciDISABLE_BOUNDS_TIMER_OVERRIDE_FOR_SUDDEN_DEATH)
		IF ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
			IF IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_SUDDEN_DEATH_KILLALLENEMIES) 
				iTimeLimit = 30000
			ELIF IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL3_SUDDEN_DEATH_BE_MY_VALENTINES) 
				iTimeLimit = 60000
			ENDIF
		ENDIF
	ENDIF

	RETURN iTimeLimit
ENDFUNC

FUNC BOOL DO_THESE_BOUNDS_HAVE_A_TIME_LIMIT(RULE_BOUNDS_STRUCT& sBounds, BOOL bLongEnoughToBePublicFacing = FALSE)
	IF bLongEnoughToBePublicFacing
		RETURN GET_BOUNDS_TIME_LIMIT(sBounds) > 1000
	ELSE
		RETURN GET_BOUNDS_TIME_LIMIT(sBounds) > -1
	ENDIF
ENDFUNC

FUNC INT GET_BOUNDS_TIME_REMAINING(INT iBoundsIndex, RULE_BOUNDS_STRUCT& sBounds)
	IF NOT HAS_NET_TIMER_STARTED(sRuleBoundsRuntimeVars[iBoundsIndex].stOutOfBoundsTimer)
		RETURN 9999999
	ENDIF

	RETURN GET_BOUNDS_TIME_LIMIT(sBounds) - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(sRuleBoundsRuntimeVars[iBoundsIndex].stOutOfBoundsTimer) 
ENDFUNC

PROC SUMO_SUDDEN_DEATH_SET_PLAYER_FAILED_OUTSIDE_SPHERE()
	INT iTeam = GET_LOCAL_PLAYER_TEAM()
	INT iRule = GET_LOCAL_PLAYER_CURRENT_RULE()
	
	IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet2, PBBOOL2_PLAYER_OUT_BOUNDS_FAIL)	
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			//[KH] explode on out of bounds fail 
			IF iRule < FMMC_MAX_RULES
				IF SHOULD_EXPLODE_ON_BOUNDS_FAIL(iTeam, iRule)
					
					//[KH] if vehicle is explosion proof
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciVEHICLE_EXPLOSION_PROOF)
						SET_ENTITY_PROOFS(GET_VEHICLE_PED_IS_IN(LocalPlayerPed), FALSE, FALSE, FALSE, FALSE, FALSE)
					ENDIF
					
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_VEHICLE_WEAPON_BULLETPROOF_PLAYER)
						SET_ENTITY_PROOFS(LocalPlayerPed, FALSE, FALSE, FALSE, FALSE, FALSE)
					ENDIF
					
					//B*2957034 - Fix for exploding player and vehicle in pron mode
					SET_ENTITY_INVINCIBLE(LocalPlayerPed, FALSE)
					SET_ENTITY_INVINCIBLE(GET_VEHICLE_PED_IS_IN(LocalPlayerPed), FALSE)
					
					NETWORK_EXPLODE_VEHICLE(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
				ENDIF
			ENDIF
		ENDIF
		
		//Stop elimination sound
		IF IS_BIT_SET(MC_playerBD[iPartToUse].iSuddenDeathBS, ciPBDBOOL_SSDS_COUNTDOWN_SOUND_PLAYING)
			STOP_SOUND(iOutAreaSound)
			SCRIPT_RELEASE_SOUND_ID(iOutAreaSound)
		ENDIF
		SET_BIT(MC_playerBD[iPartToUse].iClientBitSet2, PBBOOL2_PLAYER_OUT_BOUNDS_FAIL)
	ENDIF
	IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_PLAYER_FAIL)
		SET_LOCAL_PLAYER_FAILED(10)
	ENDIF		
	IF MC_playerBD[iPartToUse].iReasonForPlayerFail != PLAYER_FAIL_REASON_BOUNDS
		MC_playerBD[iPartToUse].iReasonForPlayerFail = PLAYER_FAIL_REASON_BOUNDS
	ENDIF
	//[KH] - Displays out of bounds shard if option selected in creator
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciMISSION_CUSTOM_SHARD_ON_OUT_OF_BOUNDS)
		IF NOT IS_BIT_SET(iLocalBoolCheck15, LBOOL15_IS_OUT_OF_BOUNDS_SHARD_DISPLAYED)
			PRINTLN("[KH] | CUSTOM SHARD ON OUT OF BOUNDS | BOUNDS 1 | SHOW OUT OF BOUNDS SHARD")
			BROADCAST_FMMC_OBJECTIVE_PLAYER_KNOCKED_OUT(MC_playerBD[iLocalPart].iteam)
			SET_BIT(iLocalBoolCheck15, LBOOL15_IS_OUT_OF_BOUNDS_SHARD_DISPLAYED)
		ENDIF
	ENDIF
ENDPROC

FUNC INT GET_BASE_LIVES_FOR_TEAM(INT iTeam)
	
	IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iPlayerLives = ciNUMBER_OF_LIVES__PLAYER_NUM_VARIABLE
	OR g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iPlayerLives = ciNUMBER_OF_LIVES__HALF_PLAYER_NUM_VARIABLE
	OR g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iPlayerLives = ciNUMBER_OF_LIVES__DIFFICULTY_VARIABLE
		RETURN GET_VARIABLE_TEAM_LIVES(iTeam)
	ENDIF
	
	RETURN g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iPlayerLives
ENDFUNC

FUNC INT GET_ADDITIONAL_LIVES_FOR_TEAM(INT iTeam, BOOL bLocalPlayerLives = FALSE)
	IF bLocalPlayerLives
		RETURN MC_playerBD[iLocalPart].iAdditionalPlayerLives
	ENDIF
	
	RETURN MC_serverBD_3.iAdditionalTeamLives[iteam]
ENDFUNC

FUNC INT GET_FMMC_MISSION_LIVES_FOR_TEAM(INT iTeam, BOOL bLocalPlayerLives = FALSE)

	INT iBaseLives = GET_BASE_LIVES_FOR_TEAM(iTeam)
	INT iAdditionalLives = GET_ADDITIONAL_LIVES_FOR_TEAM(iTeam, bLocalPlayerLives)
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSet, ciALL_LIVES_POOL_OPTION)
		iBaseLives = g_FMMC_STRUCT.sFMMCEndConditions[0].iPlayerLives
		PRINTLN("[RCC MISSION] GET_FMMC_MISSION_LIVES_FOR_TEAM - ciALL_LIVES_POOL_OPTION: ", iBaseLives)
	ENDIF
	
	IF MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
	AND g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iPlayerLivesPerRule[MC_serverBD_4.iCurrentHighestPriority[iTeam]] != -1
	
		// Infinite is option 0 in the creator, so offset by -1 to make iBaseLives = ciFMMC_UNLIMITED_LIVES
		IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iPlayerLivesPerRule[MC_serverBD_4.iCurrentHighestPriority[iTeam]] = 0
			iBaseLives = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iPlayerLivesPerRule[MC_serverBD_4.iCurrentHighestPriority[iTeam]] -1
		ELSE
			iBaseLives = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iPlayerLivesPerRule[MC_serverBD_4.iCurrentHighestPriority[iTeam]]
		ENDIF
		
		PRINTLN("[RCC MISSION] GET_FMMC_MISSION_LIVES_FOR_TEAM - iPlayerLivesPerRule: ", iBaseLives)
	ENDIF
	
	IF NOT (bLocalPlayerLives AND IS_BIT_SET(iLocalBoolCheck23, LBOOL23_DIED_BEFORE_SUDDEN_DEATH))	//Player died before sudden death, so allow them to respawn
		IF ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
			PRINTLN("[RCC MISSION] GET_FMMC_MISSION_LIVES_FOR_TEAM - In sudden death, returning 0 lives")
			RETURN 0
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciCORONA_TEAM_LIVES_SETTING)
		
		iBaseLives = MC_serverBD_1.iCoronaTeamLives[iTeam]
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciEND_ROUND_ON_ZERO_LIVES)
			iBaseLives = iBaseLives - 1
		ENDIF
		
		PRINTLN("[RCC MISSION] GET_FMMC_MISSION_LIVES_FOR_TEAM - !OVERRIDING LIVES! - iCoronaTeamLives: ", iBaseLives)
		RETURN iBaseLives
	ENDIF
	
	IF MC_serverBD_3.iTeamLivesOverride[iTeam] != -1
		iBaseLives = MC_serverBD_3.iTeamLivesOverride[iTeam]
		PRINTLN("[RCC MISSION] GET_FMMC_MISSION_LIVES_FOR_TEAM - MC_serverBD_3.iTeamLivesOverride: ", iBaseLives)
	ENDIF
	
	IF iBaseLives = ciFMMC_UNLIMITED_LIVES
		PRINTLN("[RCC MISSION] GET_FMMC_MISSION_LIVES_FOR_TEAM - Returning ciFMMC_UNLIMITED_LIVES")
		RETURN ciFMMC_UNLIMITED_LIVES
	ENDIF
	
	PRINTLN("[RCC MISSION] GET_FMMC_MISSION_LIVES_FOR_TEAM - iBaseLives: ", iBaseLives, " iAdditionalLives: ", iAdditionalLives, " iTeam: ", iTeam)
	RETURN iBaseLives + iAdditionalLives
	
ENDFUNC

FUNC INT GET_FMMC_MISSION_LIVES_FOR_COOP_TEAMS()
	RETURN GET_FMMC_MISSION_LIVES_FOR_TEAM(0)
ENDFUNC

FUNC INT GET_FMMC_MISSION_LIVES_FOR_LOCAL_PLAYER()
	
	IF NOT IS_BIT_SET(iLocalBoolCheck23, LBOOL23_DIED_BEFORE_SUDDEN_DEATH)	//Player died before sudden death, so allow them to respawn
		IF ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
		AND (IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_SUDDEN_DEATH_KILLALLENEMIES)
		OR IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_KILLALLENEMIES_PENNED_IN)
		OR IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_SUMO_PENNED_IN)
		OR IS_BIT_SET(MC_serverBD.iServerBitSet4, SBBOOL4_SUDDEN_DEATH_JUGGERNAUT)
		OR IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_POWER_PLAY) AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciSUDDEN_DEATH_POWER_PLAY_CAPTURE))
			IF iSuddenDeath_PlayerLives = -1
				iSuddenDeath_PlayerLives = MC_playerBD[iPartToUse].iNumPlayerDeaths + 1
				PRINTLN("[RCC MISSION] GET_FMMC_MISSION_LIVES_FOR_LOCAL_PLAYER - iSuddenDeath_PlayerLives initialised to ",iSuddenDeath_PlayerLives," (should have one more life)")
			ENDIF
			RETURN iSuddenDeath_PlayerLives
		ENDIF
	ENDIF
	
	RETURN GET_FMMC_MISSION_LIVES_FOR_TEAM(MC_playerBD[iPartToUse].iteam, TRUE)
	
ENDFUNC

FUNC BOOL HAS_PLAYER_QUIT_MISSION(INT iPlayer)
	RETURN IS_BIT_SET(GlobalplayerBD_FM[iPlayer].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bQuitJob)//GlobalplayerBD_FM[iPlayer].bQuitJob
ENDFUNC

FUNC BOOL IS_THIS_PLAYER_WEARING_A_DISGUISE_OUTFIT(INT iPart)

	IF IS_THIS_PLAYER_WEARING_AN_ISLAND_GUARD_OUTFIT(iPart)
		RETURN TRUE
	ENDIF
	
	IF IS_THIS_PLAYER_WEARING_AN_ISLAND_SMUGGLER_OUTFIT(iPart)
		RETURN TRUE
	ENDIF
	
	IF IS_THIS_PLAYER_WEARING_A_TUNER_SECURITY_OUTFIT(iPart)
		RETURN TRUE
	ENDIF
	
	IF IS_THIS_PLAYER_WEARING_AN_ULP_MAINTENANCE_OUTFIT(iPart)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_THIS_PLAYER_INSIDE_A_VALID_DISGUISE_VEHICLE(INT iPart, FMMC_PED_STATE &sPedState)
		
	IF sPedState.sStealthAndAggroSystemPed.mnVehiclePartialDiguise != DUMMY_MODEL_FOR_SCRIPT	
	AND IS_THIS_PLAYER_INSIDE_A_VALID_DISGUISE_VEHICLE_MODEL_TYPE(iPart, sPedState.sStealthAndAggroSystemPed.mnVehiclePartialDiguise)
		RETURN TRUE
	ENDIF
	IF sPedState.sStealthAndAggroSystemPed.mnVehicleFullDiguise != DUMMY_MODEL_FOR_SCRIPT	
	AND IS_THIS_PLAYER_INSIDE_A_VALID_DISGUISE_VEHICLE_MODEL_TYPE(iPart, sPedState.sStealthAndAggroSystemPed.mnVehicleFullDiguise)
		RETURN TRUE
	ENDIF
	
	IF sPedState.sStealthAndAggroSystemPed.iVehiclePartialDisguiseBS != 0
	AND IS_THIS_PLAYER_INSIDE_A_VALID_DISGUISE_VEHICLE_INDEX(iPart, sPedState.sStealthAndAggroSystemPed.iVehiclePartialDisguiseBS)
		RETURN TRUE
	ENDIF	
	IF sPedState.sStealthAndAggroSystemPed.iVehicleFullDisguiseBS != 0
	AND IS_THIS_PLAYER_INSIDE_A_VALID_DISGUISE_VEHICLE_INDEX(iPart, sPedState.sStealthAndAggroSystemPed.iVehicleFullDisguiseBS)
		RETURN TRUE
	ENDIF
	
	IF (IS_BIT_SET(sPedState.sStealthAndAggroSystemPed.iBS, ciPED_SAS_AggroImmunityDisguise_PersonalVehicle_Partial) OR IS_BIT_SET(sPedState.sStealthAndAggroSystemPed.iBS, ciPED_SAS_AggroImmunityDisguise_PersonalVehicle_Full))
	AND IS_THIS_PLAYER_INSIDE_A_VALID_DISGUISE_PERSONAL_VEHICLE(iPart)
		RETURN TRUE
	ENDIF
		
	RETURN FALSE

ENDFUNC

FUNC BOOL ARE_ALL_PARTICIPANTS_ON_TEAM_IN_DISGUISE_OUTFIT(INT iTeamToCheck)
	
	IF IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_ALL_PLAYERS_IN_DISGUISE)
		RETURN TRUE
	ENDIF
	
	IF NOT IS_TEAM_ACTIVE(iTeamToCheck)
		RETURN FALSE
	ENDIF
	
	INT iPart = -1
	INT iPartsInDisguise = 0
	DO_PARTICIPANT_LOOP_FLAGS eFlags = GET_PARTICIPANT_LOOP_FLAGS_TO_CHECK_SINGLE_TEAM(iTeamToCheck)
	WHILE DO_PARTICIPANT_LOOP(iPart, eFlags | DPLF_CHECK_PED_ALIVE)
		IF IS_THIS_PLAYER_WEARING_A_DISGUISE_OUTFIT(iPart)
			iPartsInDisguise++
			IF iPartsInDisguise >= MC_serverBD.iNumberOfPlayingPlayers[iTeamToCheck]
				IF bIsLocalPlayerHost
					SET_BIT(MC_serverBD.iServerBitSet1, SBBOOL1_ALL_PLAYERS_IN_DISGUISE)
				ENDIF
				RETURN TRUE
			ENDIF
		ELSE
			RETURN FALSE
		ENDIF
	ENDWHILE
	
	RETURN FALSE
	
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Objective Entities
// ##### Description: Helpers and wrappers for Entity rule functionality 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
FUNC INT GET_NUMBER_OF_PLAYERS_REQUIRED_TO_LEAVE_ENTITY(INT iTeam)
	
	IF MC_serverBD_4.iCurrentHighestPriority[iTeam] >= FMMC_MAX_RULES
		RETURN 0
	ENDIF
	
	// Default
	INT iNumberOfPlayersRequiredToLeave = 1	
	
	// Entire Team
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFifteen[MC_serverBD_4.iCurrentHighestPriority[iTeam]], ciBS_RULE15_LEAVE_ENTITY_OBJECTIVE_ENTIRE_TEAM)
		IF MC_serverBD.iNumberOfPlayingPlayers[iTeam] > 0
			iNumberOfPlayersRequiredToLeave = MC_serverBD.iNumberOfPlayingPlayers[iTeam]	
		ELSE
			MC_serverBD.iNumberOfPlayingPlayers[iTeam] = 99 // Don't let it pass because 0 is invalid.
		ENDIF
	ENDIF
	
	RETURN iNumberOfPlayersRequiredToLeave
ENDFUNC

FUNC INT GET_PLAYERS_LEFT_LEAVE_OBJECTIVE_ENTITY(INT iTeam, INT iEntity)

	// If we pass in -1 it's because we're not near anything. Return the number of required "leaves". This will be the case for Objective Text.
	IF iEntity = -1
		RETURN GET_NUMBER_OF_PLAYERS_REQUIRED_TO_LEAVE_ENTITY(iTeam)
	ENDIF
	
	RETURN MC_serverBD_3.iNumberOfPlayersLeftLeaveEntity[iTeam][iEntity]
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Time Limits
// ##### Description: Helpers and wrappers to do with time limits
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

// Objective Time Limits
FUNC INT GET_OBJECTIVE_TIME_LIMIT_FOR_RULE(INT iTeam, INT iRule)
	IF NOT IS_RULE_INDEX_VALID(iRule)
		RETURN 0
	ENDIF
	RETURN GET_FMMC_OBJECTIVE_TIME_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjectiveTimeLimitRule[iRule], MC_serverBD.iTotalNumStartingPlayers)
ENDFUNC
FUNC INT GET_OBJECTIVE_TIME_LIMIT_FOR_CURRENT_RULE(INT iTeam)
	RETURN GET_OBJECTIVE_TIME_LIMIT_FOR_RULE(iTeam, GET_TEAM_CURRENT_RULE(iTeam))
ENDFUNC
FUNC INT GET_OBJECTIVE_TIME_LIMIT_FOR_LOCAL_PLAYER_CURRENT_RULE()
	RETURN GET_OBJECTIVE_TIME_LIMIT_FOR_CURRENT_RULE(GET_LOCAL_PLAYER_TEAM())
ENDFUNC

FUNC BOOL IS_OBJECTIVE_TIMER_RUNNING(INT iTeam)
	RETURN HAS_NET_TIMER_STARTED(MC_serverBD_3.tdObjectiveLimitTimer[iTeam])
ENDFUNC
FUNC BOOL IS_LOCAL_PLAYER_OBJECTIVE_TIMER_RUNNING()
	RETURN IS_OBJECTIVE_TIMER_RUNNING(GET_LOCAL_PLAYER_TEAM())
ENDFUNC

FUNC INT GET_OBJECTIVE_TIMER_TIME_REMAINING(INT iTeam, INT iRule)
	INT iTimeRemaining = GET_OBJECTIVE_TIME_LIMIT_FOR_RULE(iTeam, iRule)
	
	IF IS_OBJECTIVE_TIMER_RUNNING(iTeam)
		iTimeRemaining -= GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdObjectiveLimitTimer[iTeam])
	ENDIF
	
	iTimeRemaining -= MC_serverBD_3.iTimerPenalty[iTeam]
	
	RETURN iTimeRemaining
ENDFUNC
FUNC INT GET_CURRENT_OBJECTIVE_TIMER_TIME_REMAINING(INT iTeam)
	RETURN GET_OBJECTIVE_TIMER_TIME_REMAINING(iTeam, GET_TEAM_CURRENT_RULE(iTeam))
ENDFUNC
FUNC INT GET_LOCAL_PLAYER_OBJECTIVE_TIMER_TIME_REMAINING()
	RETURN GET_CURRENT_OBJECTIVE_TIMER_TIME_REMAINING(GET_LOCAL_PLAYER_TEAM())
ENDFUNC

FUNC BOOL HAS_OBJECTIVE_TIMER_EXPIRED(INT iTeam, INT iRule)
	RETURN GET_OBJECTIVE_TIMER_TIME_REMAINING(iTeam, iRule) <= 0
ENDFUNC
FUNC BOOL HAS_CURRENT_OBJECTIVE_TIMER_EXPIRED(INT iTeam)
	RETURN GET_OBJECTIVE_TIMER_TIME_REMAINING(iTeam, GET_TEAM_CURRENT_RULE(iTeam)) <= 0
ENDFUNC
FUNC BOOL HAS_LOCAL_PLAYER_OBJECTIVE_TIMER_EXPIRED()
	RETURN HAS_CURRENT_OBJECTIVE_TIMER_EXPIRED(GET_LOCAL_PLAYER_TEAM())
ENDFUNC
// Multi-Rule Timer Time Limits
FUNC INT GET_MULTIRULE_TIMER_TIME_LIMIT_FOR_RULE(INT iTeam, INT iRule)
	IF NOT IS_RULE_INDEX_VALID(iRule)
		RETURN 0
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciSELECTABLE_ROUND_LENGTH)
	AND NOT IS_FAKE_MULTIPLAYER_MODE_SET()
		RETURN g_FMMC_STRUCT.iMissionCustomMultiruleTime * 60000
	ELSE
		RETURN GET_FMMC_OBJECTIVE_TIME_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTimeLimit[iRule], MC_serverBD.iTotalNumStartingPlayers)
	ENDIF
ENDFUNC
FUNC INT GET_MULTIRULE_TIMER_TIME_LIMIT_FOR_CURRENT_RULE(INT iTeam)
	RETURN GET_MULTIRULE_TIMER_TIME_LIMIT_FOR_RULE(iTeam, GET_TEAM_CURRENT_RULE(iTeam))
ENDFUNC
FUNC INT GET_MULTIRULE_TIMER_TIME_LIMIT_FOR_LOCAL_PLAYER_CURRENT_RULE(BOOL bUsePartToUse = FALSE)
	RETURN GET_MULTIRULE_TIMER_TIME_LIMIT_FOR_CURRENT_RULE(GET_LOCAL_PLAYER_TEAM(bUsePartToUse))
ENDFUNC

FUNC INT GET_CURRENT_MULTIRULE_TIMER_TIME_LIMIT(INT iTeam)
	RETURN MC_serverBD_3.iMultiObjectiveTimeLimit[iTeam]
ENDFUNC
FUNC INT GET_LOCAL_PLAYER_CURRENT_MULTIRULE_TIMER_TIME_LIMIT(BOOL bUsePartToUse = FALSE)
	RETURN GET_CURRENT_MULTIRULE_TIMER_TIME_LIMIT(GET_LOCAL_PLAYER_TEAM(bUsePartToUse))
ENDFUNC

FUNC BOOL IS_MULTIRULE_TIMER_RUNNING(INT iTeam)
	IF MC_serverBD_3.iMultiObjectiveTimeLimit[iTeam] = 0
		RETURN FALSE
	ENDIF
	
	RETURN HAS_NET_TIMER_STARTED(MC_serverBD_3.tdMultiObjectiveLimitTimer[iTeam])
ENDFUNC
FUNC BOOL IS_LOCAL_PLAYER_MULTIRULE_TIMER_RUNNING(BOOL bUsePartToUse = FALSE)
	RETURN IS_MULTIRULE_TIMER_RUNNING(GET_LOCAL_PLAYER_TEAM(bUsePartToUse))
ENDFUNC

FUNC INT GET_CURRENT_MULTIRULE_TIMER_TIME_REMAINING(INT iTeam)
	INT iTimeRemaining = GET_MULTIRULE_TIMER_TIME_LIMIT_FOR_RULE(iTeam, GET_TEAM_CURRENT_RULE(iTeam))
	
	IF IS_MULTIRULE_TIMER_RUNNING(iTeam)
		iTimeRemaining = GET_CURRENT_MULTIRULE_TIMER_TIME_LIMIT(iTeam)
		iTimeRemaining -= GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdMultiObjectiveLimitTimer[iTeam])
	ENDIF
	
	RETURN iTimeRemaining
ENDFUNC
FUNC INT GET_LOCAL_PLAYER_MULTIRULE_TIMER_TIME_REMAINING(BOOL bUsePartToUse = FALSE)
	RETURN GET_CURRENT_MULTIRULE_TIMER_TIME_REMAINING(GET_LOCAL_PLAYER_TEAM(bUsePartToUse))
ENDFUNC

FUNC BOOL HAS_MULTIRULE_TIMER_EXPIRED(INT iTeam)
	RETURN GET_CURRENT_MULTIRULE_TIMER_TIME_REMAINING(iTeam) <= 0
ENDFUNC
FUNC BOOL HAS_LOCAL_PLAYER_MULTIRULE_TIMER_EXPIRED(BOOL bUsePartToUse = FALSE)
	RETURN HAS_MULTIRULE_TIMER_EXPIRED(GET_LOCAL_PLAYER_TEAM(bUsePartToUse))
ENDFUNC

// Sudden Death Time Limits
FUNC INT GET_SUDDEN_DEATH_TIME_LIMIT_FOR_RULE(INT iTeam, INT iRule)
	IF NOT IS_RULE_INDEX_VALID(iRule)
		RETURN 0
	ENDIF
	
	RETURN GET_FMMC_OBJECTIVE_TIME_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjectiveSuddenDeathTimeLimit[iRule], MC_serverBD.iTotalNumStartingPlayers)
ENDFUNC
FUNC INT GET_SUDDEN_DEATH_TIME_LIMIT_FOR_CURRENT_RULE(INT iTeam)
	RETURN GET_SUDDEN_DEATH_TIME_LIMIT_FOR_RULE(iTeam, GET_TEAM_CURRENT_RULE(iTeam))
ENDFUNC
FUNC INT GET_SUDDEN_DEATH_TIME_LIMIT_FOR_LOCAL_PLAYER_CURRENT_RULE()
	RETURN GET_SUDDEN_DEATH_TIME_LIMIT_FOR_CURRENT_RULE(GET_LOCAL_PLAYER_TEAM())
ENDFUNC

FUNC INT GET_TOTAL_SUDDEN_DEATH_TIME_LIMIT_FOR_RULE(INT iTeam, INT iRule)
	IF NOT IS_RULE_INDEX_VALID(iRule)
		RETURN 0
	ENDIF
	
	INT iTimeLimit = GET_FMMC_OBJECTIVE_TIME_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjectiveSuddenDeathTimeLimit[iRule], MC_serverBD.iTotalNumStartingPlayers)
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciMULTIRULE_TIMER_SUDDEN_DEATH)
		iTimeLimit += GET_CURRENT_MULTIRULE_TIMER_TIME_LIMIT(iTeam)
	ELSE
		iTimeLimit += GET_OBJECTIVE_TIME_LIMIT_FOR_RULE(iTeam, iRule)
	ENDIF
	
	RETURN iTimeLimit
ENDFUNC

FUNC INT GET_REMAINING_TIME_ON_SUDDEN_DEATH_SHRINKING_BOUNDS(INT iTeam)

	IF NOT IS_TEAM_INDEX_VALID(iTeam)
		RETURN -1
	ENDIF
	
	INT iTimeRemaining = g_FMMC_STRUCT.iSphereShrinkTime + g_FMMC_STRUCT.iTimeToGetInsideSphere
	
	IF HAS_NET_TIMER_STARTED(MC_serverBD.tdSuddenDeath_ShrinkingBoundsHUDTimer[iTeam])
		iTimeRemaining -= GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdSuddenDeath_ShrinkingBoundsHUDTimer[iTeam])
	ENDIF
	
	iTimeRemaining -= MC_serverBD_3.iTimerPenalty[iTeam]
	RETURN iTimeRemaining
ENDFUNC

FUNC INT GET_SUDDEN_DEATH_TIME_REMAINING(INT iTeam, INT iRule)

	IF IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_SUMO_PENNED_IN)
		RETURN GET_REMAINING_TIME_ON_SUDDEN_DEATH_SHRINKING_BOUNDS(iTeam)
	ENDIF
	
	INT iTimeRemaining = GET_TOTAL_SUDDEN_DEATH_TIME_LIMIT_FOR_RULE(iTeam, iRule)
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciMULTIRULE_TIMER_SUDDEN_DEATH)
		iTimeRemaining -= GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdMultiObjectiveLimitTimer[iTeam])
	ELSE
		iTimeRemaining -= GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdObjectiveLimitTimer[iTeam])
	ENDIF
	
	iTimeRemaining -= MC_serverBD_3.iTimerPenalty[iTeam]
	RETURN iTimeRemaining
ENDFUNC

FUNC BOOL HAS_SUDDEN_DEATH_TIMER_EXPIRED(INT iTeam, INT iRule)
	IF NOT ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
		RETURN FALSE
	ENDIF	
	RETURN GET_SUDDEN_DEATH_TIME_REMAINING(iTeam, iRule) <= 0
ENDFUNC

// Midpoint Fail Time Limits
FUNC INT GET_MIDPOINT_FAIL_TIME_LIMIT_FOR_RULE(INT iTeam, INT iRule)
	IF NOT IS_RULE_INDEX_VALID(iRule)
		RETURN 0
	ENDIF
	
	RETURN GET_FMMC_OBJECTIVE_TIME_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMidpointFailTime[iRule], MC_serverBD.iTotalNumStartingPlayers)
ENDFUNC
FUNC INT GET_MIDPOINT_FAIL_TIME_LIMIT_FOR_CURRENT_RULE(INT iTeam)
	RETURN GET_MIDPOINT_FAIL_TIME_LIMIT_FOR_RULE(iTeam, GET_TEAM_CURRENT_RULE(iTeam))
ENDFUNC
FUNC INT GET_MIDPOINT_FAIL_TIME_LIMIT_FOR_LOCAL_PLAYER_CURRENT_RULE()
	RETURN GET_MIDPOINT_FAIL_TIME_LIMIT_FOR_CURRENT_RULE(GET_LOCAL_PLAYER_TEAM())
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Timers
// ##### Description: Helpers and wrappers to do with timers
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL IS_FRAME_COUNT_STAGGER_READY(INT iFrames)	
	#IF NOT IS_DEBUG_BUILD
		RETURN FALSE
	#ENDIF
	
	#IF IS_DEBUG_BUILD
	IF bDontFrameStaggeredDebug
		RETURN TRUE
	ENDIF
	#ENDIF
	
	RETURN GET_FRAME_COUNT() % iFrames = 0	
ENDFUNC

FUNC BOOL CHECK_IF_TIME_PASSED_IN_HAS_EXPIRED(INT iStartTime, INT iDelay)
	
	IF GET_GAME_TIMER() - iStartTime > iDelay
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL HAS_SERVER_TIME_PASSED_ROUND(INT iTime = 1000)
	IF IS_THIS_A_ROUNDS_MISSION()
	OR IS_THIS_A_QUICK_RESTART_JOB()
		PRINTLN("[RCC MISSION]  HAS_SERVER_TIME_PASSED_ROUND = ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.iBetweenRoundsTimer), " " ) 					
		RETURN HAS_NET_TIMER_EXPIRED_READ_ONLY(MC_serverBD.iBetweenRoundsTimer, iTime)
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL DOES_MISSION_HAVE_SUDDEN_DEATH_ENDING()
	
	IF IS_BIT_SET(MC_serverBD.iOptionsMenuBitSet,ciUseCTFSuddenDeath) 
	OR IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_SUDDEN_DEATH_KILLALLENEMIES) 
	OR IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_SUDDEN_DEATH_MOREINLOC) 
	OR IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_SUMO_PENNED_IN) 
	OR IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_BE_MY_VALENTINES) 
	OR IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_IN_AND_OUT)
	OR IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_POWER_PLAY)
	OR IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_TARGET)
	OR IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_PACKAGE_HELD)
	OR IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_KILLALLENEMIES_PENNED_IN)
	OR IS_BIT_SET(MC_serverBD.iServerBitSet4, SBBOOL4_SUDDEN_DEATH_JUGGERNAUT)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL SHOULD_TEAM_FAIL_FOR_SUDDEN_DEATH_TIMEOUT(INT iTeam)
	
	BOOL bFail = TRUE
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitset2, ciBS2_SuddenDeathTimeout_DoesntFail)
		bFail = FALSE
	ENDIF
	
	RETURN bFail
	
ENDFUNC

FUNC BOOL SHOULD_MULTIRULE_TIMER_WAIT_FOR_OBJECTIVE_TIMER(INT iTeam)
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_Delay_Multirule_End_Wait_Until_Rule_Timer)
		IF NOT HAS_CURRENT_OBJECTIVE_TIMER_EXPIRED(iTeam)
			PRINTLN("[RCC MISSION][MultiRuleTimer] NEED_TO_WAIT_FOR_OBJECTIVE_TIMER returning TRUE due to HAS_OBJECTIVE_TIMER_EXPIRED!")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC START_MISSION_TIMERS()
	
	PRINTLN("[RCC MISSION] START_MISSION_TIMERS called")
	
	REINIT_NET_TIMER(MC_serverBD.tdMissionLengthTimer)
	
	INT iTeam
	
	FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
		REINIT_NET_TIMER(MC_serverBD_3.tdTeamMissionTime[iTeam])
	ENDFOR
	
ENDPROC

PROC END_TEAM_MISSION_TIMER(INT iTeam)
	
	PRINTLN("[RCC MISSION] END_TEAM_MISSION_TIMER called for team ",iTeam)
	DEBUG_PRINTCALLSTACK()
	
	IF MC_serverBD_3.iTeamMissionTime[iTeam] = 0
		MC_serverBD_3.iTeamMissionTime[iTeam] = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdTeamMissionTime[iTeam])
		PRINTLN("[RCC MISSION] END_TEAM_MISSION_TIMER - Setting MC_serverBD_3.iTeamMissionTime[",iTeam,"] = ",MC_serverBD_3.iTeamMissionTime[iTeam])
	ENDIF
	
ENDPROC

PROC END_MISSION_MRT_CHECKPOINT_TIMERS(INT iCheckpoint)
	
	PRINTLN("[MMacK][Checkpoint][RetryMRT] Calling END_MISSION_MRT_CHECKPOINT_TIMERS")
	
	INT iTeam
	INT iMRTThreshold, iBit
	
	FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
		
		PRINTLN("[MMacK][Checkpoint][RetryMRT] END_MISSION_MRT_CHECKPOINT_TIMERS - iTeam: ", iTeam, " iCheckpoint = ", iCheckpoint)
		
		SWITCH iCheckpoint
			CASE 1
				iMRTThreshold = g_FMMC_STRUCT.iCheckpointMRTThreshold[0][iTeam]
				iBit = SB_CP1_MRT_1 + iTeam
			BREAK
			CASE 2
				iMRTThreshold = g_FMMC_STRUCT.iCheckpointMRTThreshold[1][iTeam]
				iBit = SB_CP2_MRT_1 + iTeam
			BREAK
			CASE 3
				iMRTThreshold = g_FMMC_STRUCT.iCheckpointMRTThreshold[2][iTeam]
				iBit = SB_CP3_MRT_1 + iTeam
			BREAK
			CASE 4
				iMRTThreshold = g_FMMC_STRUCT.iCheckpointMRTThreshold[3][iTeam]
				iBit = SB_CP4_MRT_1 + iTeam
			BREAK
		ENDSWITCH
		
		PRINTLN("[MMacK][Checkpoint][RetryMRT] END_MISSION_MRT_CHECKPOINT_TIMERS - iTeam: ", iTeam, " iBit = ", iBit)
		
		IF IS_BIT_SET(MC_ServerBD.iCheckpointBitset, iBit)
			IF MC_ServerBD_4.iTransitionSessionMultirule_Checkpoint[iTeam] != iCheckpoint
			OR MC_ServerBD_4.iTransitionSessionMultirule_TimeStamp[iTeam] = 0				
				INT iMultiRuleTimer_Remaining = GET_CURRENT_MULTIRULE_TIMER_TIME_REMAINING(iTeam)
				
				IF iMultiRuleTimer_Remaining >= iMRTThreshold
					MC_ServerBD_4.iTransitionSessionMultirule_TimeStamp[iTeam] = iMultiRuleTimer_Remaining
					PRINTLN("[MMacK][Checkpoint][RetryMRT] END_MISSION_MRT_CHECKPOINT_TIMERS - CP ",iCheckpoint," Team ",iTeam," time left ",iMultiRuleTimer_Remaining," is above/equal to iMRTThreshold ",iMRTThreshold,", set iRetryMultiRuleTimeStamp as that")
				ELSE
					MC_ServerBD_4.iTransitionSessionMultirule_TimeStamp[iTeam] = iMRTThreshold
					PRINTLN("[MMacK][Checkpoint][RetryMRT] END_MISSION_MRT_CHECKPOINT_TIMERS - CP ",iCheckpoint," Team ",iTeam," time left ",iMultiRuleTimer_Remaining," is less than iMRTThreshold ",iMRTThreshold,", set iRetryMultiRuleTimeStamp as iMRTThreshold")
				ENDIF
				
				MC_ServerBD_4.iTransitionSessionMultirule_Checkpoint[iTeam] = iCheckpoint
				PRINTLN("[MMacK][Checkpoint][RetryMRT] END_MISSION_MRT_CHECKPOINT_TIMERS - CP ",iCheckpoint," Team ",iTeam,", set iRetryMultiRuleTimeStamp_Checkpoint as ",MC_ServerBD_4.iTransitionSessionMultirule_Checkpoint[iTeam])
			#IF IS_DEBUG_BUILD
			ELSE
				PRINTLN("[MMacK][Checkpoint][RetryMRT] END_MISSION_MRT_CHECKPOINT_TIMERS - CP ",iCheckpoint," Team ",iTeam," already has iRetryMultiRuleTimeStamp set as ",MC_ServerBD_4.iTransitionSessionMultirule_TimeStamp[iTeam]," for this checkpoint")
			#ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
ENDPROC

PROC END_MISSION_TIMERS()
	
	IF bIsLocalPlayerHost
		
		PRINTLN("[RCC MISSION] END_MISSION_TIMERS called")
		DEBUG_PRINTCALLSTACK()
		
		IF MC_serverBD.iTotalMissionEndTime = 0
			MC_serverBD.iTotalMissionEndTime = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdMissionLengthTimer)
			PRINTLN("[RCC MISSION] END_MISSION_TIMERS - Setting MC_serverBD.iTotalMissionEndTime = ",MC_serverBD.iTotalMissionEndTime)
		ENDIF
		
		INT iTeam
		
		FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
			END_TEAM_MISSION_TIMER(iTeam)
		ENDFOR
						
		IF IS_BIT_SET(MC_ServerBD.iCheckpointBitset, SB_CHECKPOINT_4)
			END_MISSION_MRT_CHECKPOINT_TIMERS(4)
		ELIF IS_BIT_SET(MC_ServerBD.iCheckpointBitset, SB_CHECKPOINT_3)
			END_MISSION_MRT_CHECKPOINT_TIMERS(3)
		ELIF IS_BIT_SET(MC_ServerBD.iCheckpointBitset, SB_CHECKPOINT_2)
			END_MISSION_MRT_CHECKPOINT_TIMERS(2)
		ELIF IS_BIT_SET(MC_ServerBD.iCheckpointBitset, SB_CHECKPOINT_1)
			END_MISSION_MRT_CHECKPOINT_TIMERS(1)
		ELSE
			PRINTLN("[MMacK][Checkpoint][RetryMRT] END_MISSION_TIMERS - No checkpoints, clear iRetryMultiRuleTimeStamp & iRetryMultiRuleTimeStamp_Checkpoint for all teams")
			FOR iTeam = 0 TO FMMC_MAX_TEAMS-1
				g_TransitionSessionNonResetVars.iRetryMultiRuleTimeStamp_Checkpoint[iTeam] = 0
				g_TransitionSessionNonResetVars.iRetryMultiRuleTimeStamp[iTeam] = 0
			ENDFOR
		ENDIF
		
	ENDIF
	
ENDPROC

FUNC BOOL RUN_TIMER(SCRIPT_TIMER& stTimer, INT iExpiryTime)
	IF NOT HAS_NET_TIMER_STARTED(stTimer)
		PRINTLN("[Timers] Starting timer with an expiry time of: ", iExpiryTime)
		REINIT_NET_TIMER(stTimer)
	ENDIF
	
	RETURN HAS_NET_TIMER_EXPIRED(stTimer, iExpiryTime)
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Scoring
// ##### Description: Wrappers and headers for getting and setting score data
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

// Wrapper added to handle a fringe case bad creator setup url:bugstar:7631826
FUNC INT GET_TEAM_SCORE_ON_CURRENT_RULE(INT iTeam)
	
	INT iRule = GET_TEAM_CURRENT_RULE(iTeam)
	IF iRule < FMMC_MAX_RULES
	AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFifteen[iRule], ciBS_RULE15_USE_TEAM_SCORE_FOR_TARGET_SCORE_CHECKS_THIS_RULE)
		RETURN MC_serverBD.iTeamScore[iTeam]
	ENDIF
	
	RETURN MC_serverBD.iScoreOnThisRule[iTeam]
ENDFUNC

PROC INCREMENT_SERVER_TEAM_SCORE(INT iTeam, INT iPriority, INT iScore)
	
	DEBUG_PRINTCALLSTACK()
	
	IF iTeam < FMMC_MAX_TEAMS
		
		MC_serverBD.iTeamScore[iTeam] += iScore
		PRINTLN("[RCC MISSION] INCREMENT_SERVER_TEAM_SCORE team: ",iteam," priority ",iPriority,", increased by ",iScore,",  total score ", MC_serverBD.iTeamScore[iTeam])

		
		IF iPriority < FMMC_MAX_RULES
		AND iPriority = MC_serverBD_4.iCurrentHighestPriority[iTeam]
			MC_serverBD.iScoreOnThisRule[iTeam] += iScore
			PRINTLN("[RCC MISSION] INCREMENT_SERVER_TEAM_SCORE team: ",iteam," priority ",iPriority,", increased by ",iScore,",  total score on this rule ", MC_serverBD.iScoreOnThisRule[iTeam])
		ENDIF
	ENDIF

ENDPROC

FUNC INT GET_POINTS_REQUIRED_FOR_TEAM_TO_PASS(INT iTeam)
	
	IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMissionTargetScore = -1
		IF MC_serverBD.iNumberOfPlayingPlayers[iTeam] > 0
			RETURN MC_serverBD.iNumberOfPlayingPlayers[iTeam]
		ENDIF
	ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMissionTargetScore != 0
		RETURN g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMissionTargetScore
	ENDIF
	
	RETURN 1
	
ENDFUNC

PROC GIVE_TEAM_ENOUGH_POINTS_TO_PASS(INT iTeam)

	INT iPassScore = GET_POINTS_REQUIRED_FOR_TEAM_TO_PASS(iTeam)
	INT iPointsToGive = iPassScore - MC_serverBD.iTeamScore[iTeam]
	
	IF iPointsToGive <= 0
		PRINTLN("[OBJECTIVE_LIMITS] GIVE_TEAM_ENOUGH_POINTS_TO_PASS - Team ", iTeam, " has already reached a score of at least ", iPassScore, " (", MC_serverBD.iTeamScore[iTeam], ")")
		EXIT
	ENDIF
	
	PRINTLN("[OBJECTIVE_LIMITS] GIVE_TEAM_ENOUGH_POINTS_TO_PASS - Giving team ", iTeam, " ", iPointsToGive, " points. Target Score: ", iPassScore)
	MC_ServerBD.iPointsGivenToPass[iTeam] += iPointsToGive
	INCREMENT_SERVER_TEAM_SCORE(iTeam, MC_serverBD_4.iCurrentHighestPriority[iTeam], iPointsToGive)
	
ENDPROC

FUNC BOOL ARE_MULTIPLE_TEAMS_ON_SAME_SCORE()
	
	INT iTeam
	BOOL bMultipleTeamsOnScore = FALSE
	INT iTeamsOnSameScore = 0
	INT iHighestScore = 0

	FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
		IF IS_TEAM_ACTIVE(iTeam)
			IF MC_serverBD.iTeamScore[iTeam] > iHighestScore
				iHighestScore = MC_serverBD.iTeamScore[iTeam]
			ENDIF
		ENDIF
	ENDFOR
	
	PRINTLN("[JS] ARE_MULTIPLE_TEAMS_ON_SAME_SCORE - HIGHEST SCORE: ", iHighestScore)

	FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
		IF IS_TEAM_ACTIVE(iTeam)
			IF MC_serverBD.iTeamScore[iTeam] = iHighestScore
				PRINTLN("[JS] ARE_MULTIPLE_TEAMS_ON_SAME_SCORE - TEAM: ", iTeam," has the highest score")
				iTeamsOnSameScore++
			ENDIF
		ENDIF
	ENDFOR
	
	IF iTeamsOnSameScore >= 2
		bMultipleTeamsOnScore = TRUE
	ENDIF
	
	RETURN bMultipleTeamsOnScore
	
ENDFUNC

FUNC BOOL IS_USING_SCALED_SCORE_VALUES()
	RETURN IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitsetTen, ciMODE_RULE_BASED_SCORE_SCALING) 
ENDFUNC
	
FUNC FLOAT GET_AMOUNT_OF_TEAMS_SCORE_MULTIPLIER()
	FLOAT fMultToAdd = 0.0
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTen, ciENABLE_TEAM_AMOUNT_SCORE_SCALING)
		IF MC_serverBD.iNumberOfTeams = 3
			fMultToAdd = g_FMMC_STRUCT.iTeamSizeScoreScalingForThree
		ELIF MC_serverBD.iNumberOfTeams = 4
			fMultToAdd = g_FMMC_STRUCT.iTeamSizeScoreScalingForFour
		ENDIF
	ENDIF
	
	RETURN fMultToAdd
ENDFUNC

FUNC FLOAT GET_MULTIPLAYER_BASED_ON_PLAYERS_AND_CORONA_SETTING(INT iTeam, INT iRule)
	IF NOT IS_USING_SCALED_SCORE_VALUES()
		RETURN 1.0
	ENDIF
	FLOAT fMult = (0.1 + (MC_ServerBD.iNumStartingPlayers[iTeam]*0.1)) + (0.5*MC_ServerBD_4.iTargetScoreMultiplierSetting) + GET_AMOUNT_OF_TEAMS_SCORE_MULTIPLIER()
	INT iScore = GET_FMMC_OBJECTIVE_SCORE_LIMIT_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTargetScore[iRule], MC_ServerBD.iNumberOfPlayingPlayers[iTeam], MC_serverBD.iMaxNumPriorityEntitiesThisRule[iTeam])
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[LM][GET_MULTIPLAYER_BASED_ON_PLAYERS_AND_CORONA_SETTING] - (Multiplier Corona Index) MC_ServerBD_4.iTargetScoreMultiplierSetting: ",	 	MC_ServerBD_4.iTargetScoreMultiplierSetting)
		PRINTLN("[LM][GET_MULTIPLAYER_BASED_ON_PLAYERS_AND_CORONA_SETTING] - (Multiplier setting) MC_ServerBD_4.iTargetScoreMultiplierSetting: ", 			(0.5*MC_ServerBD_4.iTargetScoreMultiplierSetting))
		PRINTLN("[LM][GET_MULTIPLAYER_BASED_ON_PLAYERS_AND_CORONA_SETTING] - (Amount of players) MC_ServerBD.iNumStartingPlayers[iTeam]: ", 				MC_ServerBD.iNumStartingPlayers[iTeam])
		PRINTLN("[LM][GET_MULTIPLAYER_BASED_ON_PLAYERS_AND_CORONA_SETTING] - (Players Multiplier): ", 														(0.1 + (MC_ServerBD.iNumStartingPlayers[iTeam]*0.1)))
		PRINTLN("[LM][GET_MULTIPLAYER_BASED_ON_PLAYERS_AND_CORONA_SETTING] - iTeamSizeScoreScalingForThree: ", 												g_FMMC_STRUCT.iTeamSizeScoreScalingForThree)
		PRINTLN("[LM][GET_MULTIPLAYER_BASED_ON_PLAYERS_AND_CORONA_SETTING] - iTeamSizeScoreScalingForFour: ",												g_FMMC_STRUCT.iTeamSizeScoreScalingForFour)		
		PRINTLN("[LM][GET_MULTIPLAYER_BASED_ON_PLAYERS_AND_CORONA_SETTING] - fMult: ", 																		fMult)
		PRINTLN("[LM][GET_MULTIPLAYER_BASED_ON_PLAYERS_AND_CORONA_SETTING] - Target Score is: ", 															iScore)
		PRINTLN("[LM][GET_MULTIPLAYER_BASED_ON_PLAYERS_AND_CORONA_SETTING] - Target Score should be set to: ", 												ROUND(iScore * fMult))
	#ENDIF
	
	// Contingency to make sure new target score is never set to 0
	IF ROUND(iScore * fMult) = 0
		PRINTLN("[LM][GET_MULTIPLAYER_BASED_ON_PLAYERS_AND_CORONA_SETTING] - We would have a target score of ZERO, starting Contingency plan")
		fMult = 0
		INT i = 0
		FOR i = 0 TO 11
			fMult += 0.1
			PRINTLN("[LM][GET_MULTIPLAYER_BASED_ON_PLAYERS_AND_CORONA_SETTING] - iScore: ", iScore, " * fMult: ", fMult, " EQUALS: " , 						ROUND(iScore * fMult))
			IF ROUND(iScore * fMult) = 1				
				PRINTLN("[LM][GET_MULTIPLAYER_BASED_ON_PLAYERS_AND_CORONA_SETTING] - Returning fMult (Contingency): ", 										fMult)		
				RETURN fMult
			ENDIF
		ENDFOR
	ENDIF
	
	PRINTLN("[LM][GET_MULTIPLAYER_BASED_ON_PLAYERS_AND_CORONA_SETTING] - Returning fMult: ", 																fMult)		
	RETURN fMult
ENDFUNC

FUNC INT GET_FMMC_POINTS_FOR_TEAM(INT iTeam, INT iRule = -1)
	
	INT iPoints
	
	IF iTeam != -1
		
		IF iRule = -1
			iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
		ENDIF
		
		IF iRule < FMMC_MAX_RULES
			INT iSelection = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjectiveScore[iRule]
			
			iPoints = GET_FMMC_OBJECTIVE_SCORE_LIMIT_FROM_SELECTION(iSelection, MC_ServerBD.iNumberOfPlayingPlayers[iTeam], MC_serverBD.iMaxNumPriorityEntitiesThisRule[iTeam])
			
			PRINTLN("[RCC MISSION] GET_FMMC_POINTS_FOR_TEAM - Team ",iTeam," should receive ",iPoints," points (creator selection ",iSelection,") for completing objective ",iRule)
		ENDIF
	ENDIF
	
	RETURN iPoints
	
ENDFUNC

FUNC INT GET_FMMC_GRANULAR_CAPTURE_TARGET(INT iPriority, INT iTeam)
	RETURN MC_serverBD.iObjTakeoverTime[iTeam][iPriority] / 1000
ENDFUNC

FUNC BOOL CHECK_SUICIDE_DECREMENT_DOES_NOT_EXCEED_PREV_RULE(INT iRule, INT iTeam, INT iMultiplier = 0, INT iChange = 0)
	BOOL bNewScoreLessThanPrevRule = FALSE
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciNEGATIVE_SUICIDE_SCORE_RULE_CAPPED)
		PRINTLN("[RCC MISSION] Bitset: ciNEGATIVE_SUICIDE_SCORE_RULE_CAPPED is set")
		INT iNewScoreToAdd 
		
		IF iMultiplier <> 0
			iNewScoreToAdd = (GET_FMMC_POINTS_FOR_TEAM(iTeam, iRule)*imultiplier)
		ENDIF
		IF iChange <> 0
			iNewScoreToAdd = iChange
		ENDIF
		
		INT iNewScore = MC_ServerBD.iTeamScore[MC_PlayerBD[iLocalPart].iTeam] + iNewScoreToAdd
		
		PRINTLN("[RCC MISSION] iNewScoreToAdd: ",												iNewScoreToAdd)
		PRINTLN("[RCC MISSION] Team iOldScore: ",												iNewScore-iNewScoreToAdd)
		PRINTLN("[RCC MISSION] Team iNewScore: ",												iNewScore)		
		
		PRINTLN("[RCC MISSION] iRule: ",														iRule)
		INT iPreviousRuleRequiredScore = 0
		INT iRuleCount = 0
		FOR iRuleCount = 0 TO iRule-1 			
			iPreviousRuleRequiredScore += ROUND(GET_FMMC_OBJECTIVE_SCORE_LIMIT_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTargetScore[iRuleCount], MC_ServerBD.iNumberOfPlayingPlayers[iTeam], MC_serverBD.iMaxNumPriorityEntitiesThisRule[iTeam])*GET_MULTIPLAYER_BASED_ON_PLAYERS_AND_CORONA_SETTING(iTeam, iRuleCount))
		ENDFOR
		
		PRINTLN("[RCC MISSION] iPreviousRuleRequiredScore: ",								iPreviousRuleRequiredScore)
		PRINTLN("[RCC MISSION] MC_playerBD[iPartToUse].iPlayerScore: ",						MC_playerBD[iPartToUse].iPlayerScore)
		PRINTLN("[RCC MISSION] iTeamScore: ",												MC_ServerBD.iTeamScore[MC_PlayerBD[iLocalPart].iTeam])
		PRINTLN("[RCC MISSION] iNewScoreToAdd: ",											iNewScoreToAdd) 
		PRINTLN("[RCC MISSION] iNewScore: ",												iNewScore) 
		
		IF iNewScoreToAdd > 0
			PRINTLN("[RCC MISSION] ", iNewScoreToAdd, " > ", 0) 
			PRINTLN("[RCC MISSION] iNewScoreToAdd: Positive Num, Allow the Increment") 	
			RETURN FALSE
		ENDIF
		
		IF iRule = 0
		AND iNewScore > -1
			PRINTLN("[RCC MISSION] iRule = 0   &  ", iNewScore, " > ", -1)
			PRINTLN("[RCC MISSION] We are on the First Rule, so 0 is the minimum score. NewScore is above that amount. Allow the decrement") 
				
			RETURN FALSE
		ENDIF		
						
		IF iNewScore < iPreviousRuleRequiredScore
			PRINTLN("[RCC MISSION] ", iNewScore, " < ", iPreviousRuleRequiredScore) 	
			PRINTLN("[RCC MISSION] The decrement would set us below the Rule Score we are current on. Don't allow the decrement, exit the function.") 	
			bNewScoreLessThanPrevRule = TRUE
		ENDIF
		
		IF (MC_serverBD.iScoreOnThisRule[iTeam] + iNewScoreToAdd) < 0
			PRINTLN("[RCC MISSION] MC_serverBD.iScoreOnThisRule[", iTeam, "]: ", MC_serverBD.iScoreOnThisRule[iTeam])
			PRINTLN("[RCC MISSION] MC_serverBD.iScoreOnThisRule[", iTeam, "] would be less than zero if we did this decrement, so we're exiting the function.") 	
			bNewScoreLessThanPrevRule = TRUE
		ENDIF
	ENDIF
	
	PRINTLN("[RCC MISSION] Returning bNewScoreLessThanPrevRule ",								bNewScoreLessThanPrevRule)
	RETURN bNewScoreLessThanPrevRule
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section: Restart Checkpoints ----------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: 
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC SET_IGNORE_CHECKPOINT_PROCESSING_FOR_RULE_SKIP(INT iTeam)
	PRINTLN("[Checkpoint] SET_IGNORE_CHECKPOINT_PROCESSING_FOR_RULE_SKIP - Team: ", iTeam, " Current Priority: ", MC_serverBD_4.iCurrentHighestPriority[iTeam])
	SET_BIT(iLocalBoolCheck11, LBOOL11_IGNORE_CHECKPOINTS_DUE_TO_SKIP_TEAM_0 + iTeam)
ENDPROC

PROC CLEAR_IGNORE_CHECKPOINT_PROCESSING_FOR_RULE_SKIP(INT iTeam)
	PRINTLN("[Checkpoint] CLEAR_IGNORE_CHECKPOINT_PROCESSING_FOR_RULE_SKIP - Team: ", iTeam, " Current Priority: ", MC_serverBD_4.iCurrentHighestPriority[iTeam])
	CLEAR_BIT(iLocalBoolCheck11, LBOOL11_IGNORE_CHECKPOINTS_DUE_TO_SKIP_TEAM_0 + iTeam)
ENDPROC

FUNC BOOL SHOULD_IGNORE_CHECKPOINT_PROCESSING_DUE_TO_RULE_SKIP(INT iTeam)
	RETURN IS_BIT_SET(iLocalBoolCheck11, LBOOL11_IGNORE_CHECKPOINTS_DUE_TO_SKIP_TEAM_0 + iTeam)
ENDFUNC

FUNC INT GET_CURRENT_CHECKPOINT_INDEX()
	IF IS_BIT_SET(MC_ServerBD.iCheckpointBitset, SB_CHECKPOINT_4)
		RETURN SB_CHECKPOINT_4
	ELIF IS_BIT_SET(MC_ServerBD.iCheckpointBitset, SB_CHECKPOINT_3)
		RETURN SB_CHECKPOINT_3
	ELIF IS_BIT_SET(MC_ServerBD.iCheckpointBitset, SB_CHECKPOINT_2)
		RETURN SB_CHECKPOINT_2
	ELIF IS_BIT_SET(MC_ServerBD.iCheckpointBitset, SB_CHECKPOINT_1)
		RETURN SB_CHECKPOINT_1
	ENDIF
	
	RETURN 0
ENDFUNC

FUNC INT GET_CURRENT_CHECKPOINT_MRT_INDEX(INT iTeam)
	IF IS_BIT_SET(MC_ServerBD.iCheckpointBitset, SB_CP4_MRT_1 + iTeam)
		RETURN SB_CHECKPOINT_4
	ELIF IS_BIT_SET(MC_ServerBD.iCheckpointBitset, SB_CP3_MRT_1 + iTeam)
		RETURN SB_CHECKPOINT_3
	ELIF IS_BIT_SET(MC_ServerBD.iCheckpointBitset, SB_CP2_MRT_1 + iTeam)
		RETURN SB_CHECKPOINT_2
	ELIF IS_BIT_SET(MC_ServerBD.iCheckpointBitset, SB_CP1_MRT_1 + iTeam)
		RETURN SB_CHECKPOINT_1
	ENDIF
	
	RETURN 0
ENDFUNC

FUNC INT GET_CHECKPOINT_MRT_BIT(INT iCheckpoint, INT iTeam)
	
	SWITCH iCheckpoint
		CASE 0
			RETURN SB_CP1_MRT_1 + iTeam
		BREAK
		CASE 1
			RETURN SB_CP2_MRT_1 + iTeam
		BREAK
		CASE 2
			RETURN SB_CP3_MRT_1 + iTeam
		BREAK
		CASE 3
			RETURN SB_CP4_MRT_1 + iTeam
		BREAK
	ENDSWITCH
	
	ASSERTLN("GET_CHECKPOINT_MRT_BIT INVALID TEAM OR CHECKPOINT")
	RETURN 0
	
ENDFUNC
	
FUNC BOOL WAS_RULE_FAILED_FOR_CHECKPOINT(INT iTeam)

	//We don't track individual entity fails. Can find a way if needed.
	IF MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_TIME_EXPIRED
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC PROCESS_TEAM_RESTART_CHECKPOINTS(INT iTeam, INT iPreviousPriority, INT iPreviousMidpoint)

	IF !bIsLocalPlayerHost
		EXIT
	ENDIF
	
	IF NOT IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_NEW_CHECKPOINT_LOGIC)
		EXIT
	ENDIF
	
	IF iPreviousPriority = -1
	OR iPreviousPriority >= FMMC_MAX_RULES
		EXIT
	ENDIF
	
	IF MC_serverBD_4.iCurrentHighestPriority[iTeam] >= FMMC_MAX_RULES
		EXIT
	ENDIF
	
	IF IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
		EXIT
	ENDIF
	
	INT iCurrentCheckpoint = GET_CURRENT_CHECKPOINT_INDEX() //No point processing checkpoints we have passed.
	
	IF iCurrentCheckpoint = FMMC_MAX_RESTART_CHECKPOINTS
		//Last checkpoint already set
		EXIT
	ENDIF
	
	INT iNewCheckpoint = iCurrentCheckpoint
	
	INT iCheckpoint
	FOR iCheckpoint = iCurrentCheckpoint TO FMMC_MAX_RESTART_CHECKPOINTS - 1
	
		//Apply On Midpoint
		IF iPreviousMidpoint != MC_serverBD.iObjectiveMidPointBitset[iTeam]
		AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRestartCheckpointBitset[iPreviousPriority][iCheckpoint], ciRESTART_CHECKPOINT_BITSET_UPDATE_ON_MIDPOINT)
			PRINTLN("[Checkpoint] - PROCESS_TEAM_RESTART_CHECKPOINTS - Team: ", iTeam, " Priority: ", iPreviousPriority, " Checkpoint ", iCheckpoint + 1, " Set. Reason: Midpoint Reached")
			SET_BIT(MC_ServerBD.iCheckpointBitset, SB_CHECKPOINT_1 + iCheckpoint)
			iNewCheckpoint = SB_CHECKPOINT_1 + iCheckpoint
			RELOOP
		ENDIF
		
		//Apply On Rule End
		IF iPreviousPriority != MC_ServerBD_4.iCurrentHighestPriority[iTeam]
			IF WAS_RULE_FAILED_FOR_CHECKPOINT(iTeam)
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRestartCheckpointBitset[iPreviousPriority][iCheckpoint], ciRESTART_CHECKPOINT_BITSET_UPDATE_ON_FAIL)
					PRINTLN("[Checkpoint] - PROCESS_TEAM_RESTART_CHECKPOINTS - Team: ", iTeam, " Priority: ", iPreviousPriority, " Checkpoint ", iCheckpoint + 1, " Set. Reason: Rule Fail")
					SET_BIT(MC_ServerBD.iCheckpointBitset, SB_CHECKPOINT_1 + iCheckpoint)
					iNewCheckpoint = SB_CHECKPOINT_1 + iCheckpoint
				ENDIF
				RELOOP
			ENDIF
		
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRestartCheckpointBitset[iPreviousPriority][iCheckpoint], ciRESTART_CHECKPOINT_BITSET_UPDATE_ON_PASS)
				PRINTLN("[Checkpoint] - PROCESS_TEAM_RESTART_CHECKPOINTS - Team: ", iTeam, " Priority: ", iPreviousPriority, " Checkpoint ", iCheckpoint + 1, " Set. Reason: Rule Pass")
				SET_BIT(MC_ServerBD.iCheckpointBitset, SB_CHECKPOINT_1 + iCheckpoint)
				iNewCheckpoint = SB_CHECKPOINT_1 + iCheckpoint
				RELOOP
			ENDIF
		ENDIF
	
	ENDFOR
	
	//New Checkpoint Reached
	IF iNewCheckpoint != iCurrentCheckpoint
		IF HAS_FIRST_STRAND_MISSION_BEEN_PASSED()
		AND NOT IS_BIT_SET(MC_ServerBD.iCheckpointBitset, SB_CHECKPOINT_STRAND_1)
			SET_BIT(MC_ServerBD.iCheckpointBitset, SB_CHECKPOINT_STRAND_1)
			PRINTLN("[Checkpoint] - PROCESS_TEAM_RESTART_CHECKPOINTS - Setting Checkpoint Strand Bit")
		ENDIF
	ENDIF

ENDPROC

PROC PROCESS_TEAM_RESTART_MULTI_RULE_TIMER_RESETS(INT iTeam, INT iPreviousPriority, INT iPreviousMidpoint)

	IF !bIsLocalPlayerHost
		EXIT
	ENDIF
	
	IF NOT IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_NEW_CHECKPOINT_LOGIC)
		EXIT
	ENDIF
	
	IF iPreviousPriority = -1
	OR iPreviousPriority >= FMMC_MAX_RULES
		EXIT
	ENDIF
	
	IF MC_serverBD_4.iCurrentHighestPriority[iTeam] >= FMMC_MAX_RULES
		EXIT
	ENDIF
	
	IF IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
		EXIT
	ENDIF
	
	INT iCurrentCheckpointMRT = GET_CURRENT_CHECKPOINT_MRT_INDEX(iTeam) //No point processing checkpoint MRT Indexes we have passed.
	
	IF iCurrentCheckpointMRT = FMMC_MAX_RESTART_CHECKPOINTS
		//Last checkpoint MRT already set
		EXIT
	ENDIF
	
	INT iCheckpoint
	FOR iCheckpoint = iCurrentCheckpointMRT TO FMMC_MAX_RESTART_CHECKPOINTS - 1
	
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRestartCheckpointBitset[iPreviousPriority][iCheckpoint], ciRESTART_CHECKPOINT_BITSET_UPDATE_MRT)
			RELOOP
		ENDIF
	
		//Apply On Midpoint
		IF iPreviousMidpoint != MC_serverBD.iObjectiveMidPointBitset[iTeam]
		AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRestartCheckpointBitset[iPreviousPriority][iCheckpoint], ciRESTART_CHECKPOINT_BITSET_UPDATE_ON_MIDPOINT)
			PRINTLN("[Checkpoint] - PROCESS_TEAM_RESTART_MULTI_RULE_TIMER_RESETS - Team: ", iTeam, " Priority: ", iPreviousPriority, " Checkpoint MRT ", iCheckpoint + 1, " Set. Reason: Midpoint Reached")
			SET_BIT(MC_ServerBD.iCheckpointBitset, GET_CHECKPOINT_MRT_BIT(iCheckpoint, iTeam))
			RELOOP
		ENDIF
		
		//Apply On Rule End
		IF iPreviousPriority != MC_ServerBD_4.iCurrentHighestPriority[iTeam]
			IF WAS_RULE_FAILED_FOR_CHECKPOINT(iTeam)
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRestartCheckpointBitset[iPreviousPriority][iCheckpoint], ciRESTART_CHECKPOINT_BITSET_UPDATE_ON_FAIL)
					PRINTLN("[Checkpoint] - PROCESS_TEAM_RESTART_MULTI_RULE_TIMER_RESETS - Team: ", iTeam, " Priority: ", iPreviousPriority, " Checkpoint MRT ", iCheckpoint + 1, " Set. Reason: Rule Fail")
					SET_BIT(MC_ServerBD.iCheckpointBitset, GET_CHECKPOINT_MRT_BIT(iCheckpoint, iTeam))
				ENDIF
				RELOOP
			ENDIF
		
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRestartCheckpointBitset[iPreviousPriority][iCheckpoint], ciRESTART_CHECKPOINT_BITSET_UPDATE_ON_PASS)
				PRINTLN("[Checkpoint] - PROCESS_TEAM_RESTART_MULTI_RULE_TIMER_RESETS - Team: ", iTeam, " Priority: ", iPreviousPriority, " Checkpoint MRT ", iCheckpoint + 1, " Set. Reason: Rule Pass")
				SET_BIT(MC_ServerBD.iCheckpointBitset, GET_CHECKPOINT_MRT_BIT(iCheckpoint, iTeam))
				RELOOP
			ENDIF
		ENDIF
	
	ENDFOR

ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Legacy Restart Checkpoint Processing
// ##### Description: Checkpoint Processing from before the refactor (FIXER - Pack 2 2021). Kept in to avoid retesting past content
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC LEGACY_INIT_CHECKPOINT_SERVER_DATA(INT iTeam, INT iRule)
	
	IF IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_NEW_CHECKPOINT_LOGIC)
		EXIT
	ENDIF

	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitset[iRule], ciBS_RULE_UPDATE_CHECKPOINT_0)
		MC_serverBD_4.iFirstCheckpointRule[iTeam] = iRule
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThree[iRule], ciBS_RULE3_UPDATE_CHECKPOINT_1)
		MC_serverBD_4.iSecondCheckpointRule[iTeam] = iRule
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTen[iRule], ciBS_RULE10_UPDATE_CHECKPOINT_3)
		MC_serverBD_4.iThirdCheckpointRule[iTeam] = iRule
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTen[iRule], ciBS_RULE10_UPDATE_CHECKPOINT_4)
		MC_serverBD_4.iFourthCheckpointRule[iTeam] = iRule
	ENDIF
	
ENDPROC

FUNC BOOL LEGACY_HAS_MIDPOINT_CHECKPOINT_BEEN_PASSED(INT iTeam, INT iCheckpointRule)
	IF IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThree[iCheckpointRule], ciBS_RULE3_UPDATE_CHECKPOINT_ON_MIDPOINT)  //this rule has been set in creator as a midpoint rule update for checkpoint
		IF IS_BIT_SET( MC_serverBD.iObjectiveMidPointBitset[iTeam], iCheckpointRule ) 
			RETURN TRUE //midpoint passed, checkpoint should update
		ELIF MC_serverBD_4.iCurrentHighestPriority[iTeam] > iCheckpointRule
			RETURN TRUE //passed the rule of the checkpoint
		ELSE
			RETURN FALSE //is a midpoint check, but not passed it yet
		ENDIF
	ENDIF
	
	RETURN TRUE //not a midpoint check, so all good
ENDFUNC

FUNC BOOL LEGACY_HAVE_WE_PASSED_THE_CHECKPOINT(INT iTeam, INT iPriority)
	
	IF iPriority < FMMC_MAX_RULES
	AND MC_serverBD_4.iFirstCheckpointRule[iTeam] > -1
		IF iPriority >= MC_serverBD_4.iFirstCheckpointRule[iTeam]
			IF LEGACY_HAS_MIDPOINT_CHECKPOINT_BEEN_PASSED(iTeam, MC_serverBD_4.iFirstCheckpointRule[iTeam])
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL LEGACY_HAVE_WE_PASSED_THE_SECOND_CHECKPOINT(INT iTeam, INT iPriority)
	
	IF iPriority < FMMC_MAX_RULES
	AND MC_serverBD_4.iSecondCheckpointRule[iTeam] > -1
		IF iPriority >= MC_serverBD_4.iSecondCheckpointRule[iTeam]
			IF LEGACY_HAS_MIDPOINT_CHECKPOINT_BEEN_PASSED(iTeam, MC_serverBD_4.iSecondCheckpointRule[iTeam])
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL LEGACY_HAVE_WE_PASSED_THE_THIRD_CHECKPOINT(INT iTeam, INT iPriority)
	
	IF iPriority < FMMC_MAX_RULES
	AND MC_serverBD_4.iThirdCheckpointRule[iTeam] > -1
		IF iPriority >= MC_serverBD_4.iThirdCheckpointRule[iTeam]
			IF LEGACY_HAS_MIDPOINT_CHECKPOINT_BEEN_PASSED(iTeam, MC_serverBD_4.iThirdCheckpointRule[iTeam])
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL LEGACY_HAVE_WE_PASSED_THE_FOURTH_CHECKPOINT(INT iTeam, INT iPriority)
	
	IF iPriority < FMMC_MAX_RULES
	AND MC_serverBD_4.iFourthCheckpointRule[iTeam] > -1
		IF iPriority >= MC_serverBD_4.iFourthCheckpointRule[iTeam]
			IF LEGACY_HAS_MIDPOINT_CHECKPOINT_BEEN_PASSED(iTeam, MC_serverBD_4.iFourthCheckpointRule[iTeam])
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

//Copied from PROCESS_PLAYERS_POST_STAGGERED
PROC LEGACY_PROCESS_TEAM_RESTART_CHECKPOINTS(INT iTeam)
	
	IF IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_NEW_CHECKPOINT_LOGIC)
		EXIT
	ENDIF
	
	INT iTeam2
	IF (NOT HAS_TEAM_FAILED(iTeam))
	AND (NOT HAS_TEAM_FINISHED(iTeam))
		IF MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
			IF (NOT IS_BIT_SET(MC_ServerBD.iCheckpointBitset, SB_CHECKPOINT_1))
			OR (HAS_FIRST_STRAND_MISSION_BEEN_PASSED() AND (NOT IS_BIT_SET(MC_ServerBD.iCheckpointBitset, SB_CHECKPOINT_STRAND_1)))
				IF LEGACY_HAVE_WE_PASSED_THE_CHECKPOINT(iTeam, MC_serverBD_4.iCurrentHighestPriority[iTeam]) 
					IF NOT IS_BIT_SET(MC_ServerBD.iCheckpointBitset, SB_CHECKPOINT_1)
						
						PRINTLN("[Players_Staggered][Checkpoint] - LEGACY_PROCESS_TEAM_RESTART_CHECKPOINTS - Setting SB_CHECKPOINT_1 = TRUE")
						SET_BIT(MC_ServerBD.iCheckpointBitset, SB_CHECKPOINT_1)
						
						FOR iTeam2 = 0 TO (MC_serverBD.iNumberOfTeams-1)
							IF MC_serverBD_4.iFirstCheckpointRule[iTeam2] < FMMC_MAX_RULES
							AND MC_serverBD_4.iCurrentHighestPriority[iTeam2] < FMMC_MAX_RULES
							AND MC_serverBD_4.iFirstCheckpointRule[iTeam2] > -1
								IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam2].iRuleBitsetEight[MC_serverBD_4.iFirstCheckpointRule[iTeam2]], ciBS_RULE8_UPDATE_CHECKPOINT_0_MRT)
								AND LEGACY_HAVE_WE_PASSED_THE_CHECKPOINT(iTeam2, MC_serverBD_4.iCurrentHighestPriority[iTeam2])
									PRINTLN("[Players_Staggered][Checkpoint][RetryMRT] - LEGACY_PROCESS_TEAM_RESTART_CHECKPOINTS - Setting CP1 MRT = TRUE for team ", iTeam2)
									SET_BIT(MC_ServerBD.iCheckpointBitset, SB_CP1_MRT_1 + iTeam2)
								ENDIF
							ENDIF
						ENDFOR
					ENDIF
					
					IF HAS_FIRST_STRAND_MISSION_BEEN_PASSED()
					AND NOT IS_BIT_SET(MC_ServerBD.iCheckpointBitset, SB_CHECKPOINT_STRAND_1)
						SET_BIT(MC_ServerBD.iCheckpointBitset, SB_CHECKPOINT_STRAND_1)
						PRINTLN("[Players_Staggered][Checkpoint][RetryMRT] - LEGACY_PROCESS_TEAM_RESTART_CHECKPOINTS - Setting SB_CHECKPOINT_STRAND_1 = TRUE")
					ENDIF
				ENDIF
			ENDIF
				
			IF NOT IS_BIT_SET(MC_ServerBD.iCheckpointBitset, SB_CHECKPOINT_2)
			AND LEGACY_HAVE_WE_PASSED_THE_SECOND_CHECKPOINT(iTeam, MC_serverBD_4.iCurrentHighestPriority[iTeam])
				
				PRINTLN("[Players_Staggered][Checkpoint] - LEGACY_PROCESS_TEAM_RESTART_CHECKPOINTS - Setting SB_CHECKPOINT_2 = TRUE")
				SET_BIT(MC_ServerBD.iCheckpointBitset, SB_CHECKPOINT_2)
				
				FOR iTeam2 = 0 TO (MC_serverBD.iNumberOfTeams-1)
					IF MC_serverBD_4.iSecondCheckpointRule[iTeam2] < FMMC_MAX_RULES
					AND MC_serverBD_4.iCurrentHighestPriority[iTeam2] < FMMC_MAX_RULES
					AND MC_serverBD_4.iSecondCheckpointRule[iTeam2] > -1
						IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam2].iRuleBitsetEight[MC_serverBD_4.iSecondCheckpointRule[iTeam2]], ciBS_RULE8_UPDATE_CHECKPOINT_1_MRT)
						AND LEGACY_HAVE_WE_PASSED_THE_SECOND_CHECKPOINT(iTeam2, MC_serverBD_4.iCurrentHighestPriority[iTeam2])
							PRINTLN("[Players_Staggered][Checkpoint][RetryMRT] - LEGACY_PROCESS_TEAM_RESTART_CHECKPOINTS - Setting CP2 MRT = TRUE for team ", iTeam2)
							SET_BIT(MC_ServerBD.iCheckpointBitset, SB_CP2_MRT_1 + iTeam2)
						ENDIF
					ENDIF
				ENDFOR
			ENDIF
			
			IF NOT IS_BIT_SET(MC_ServerBD.iCheckpointBitset, SB_CHECKPOINT_3)
			AND LEGACY_HAVE_WE_PASSED_THE_THIRD_CHECKPOINT(iTeam, MC_serverBD_4.iCurrentHighestPriority[iTeam]) 
				
				PRINTLN("[Players_Staggered][Checkpoint] - LEGACY_PROCESS_TEAM_RESTART_CHECKPOINTS - Setting SB_CHECKPOINT_3 = TRUE")
				SET_BIT(MC_ServerBD.iCheckpointBitset, SB_CHECKPOINT_3)
				
				FOR iTeam2 = 0 TO (MC_serverBD.iNumberOfTeams-1)
					IF MC_serverBD_4.iThirdCheckpointRule[iTeam2] < FMMC_MAX_RULES
					AND MC_serverBD_4.iCurrentHighestPriority[iTeam2] < FMMC_MAX_RULES
					AND MC_serverBD_4.iThirdCheckpointRule[iTeam2] > -1
						IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam2].iRuleBitsetTen[MC_serverBD_4.iThirdCheckpointRule[iTeam2]], ciBS_RULE10_UPDATE_CHECKPOINT_2_MRT)
						AND LEGACY_HAVE_WE_PASSED_THE_THIRD_CHECKPOINT(iTeam2, MC_serverBD_4.iCurrentHighestPriority[iTeam2])
							PRINTLN("[Players_Staggered][Checkpoint][RetryMRT] - LEGACY_PROCESS_TEAM_RESTART_CHECKPOINTS - Setting CP3 MRT = TRUE for team ", iTeam2)
							SET_BIT(MC_ServerBD.iCheckpointBitset, SB_CP3_MRT_1 + iTeam2)
						ENDIF
					ENDIF
				ENDFOR
			ENDIF
			
			IF NOT IS_BIT_SET(MC_ServerBD.iCheckpointBitset, SB_CHECKPOINT_4)
			AND LEGACY_HAVE_WE_PASSED_THE_FOURTH_CHECKPOINT(iTeam, MC_serverBD_4.iCurrentHighestPriority[iTeam]) 
				
				PRINTLN("[Players_Staggered][Checkpoint] - LEGACY_PROCESS_TEAM_RESTART_CHECKPOINTS - Setting SB_CHECKPOINT_4 = TRUE")
				SET_BIT(MC_ServerBD.iCheckpointBitset, SB_CHECKPOINT_4)
				
				FOR iTeam2 = 0 TO (MC_serverBD.iNumberOfTeams-1)
					IF MC_serverBD_4.iFourthCheckpointRule[iTeam2] < FMMC_MAX_RULES
					AND MC_serverBD_4.iCurrentHighestPriority[iTeam2] < FMMC_MAX_RULES
					AND MC_serverBD_4.iFourthCheckpointRule[iTeam2] > -1
						IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam2].iRuleBitsetTen[MC_serverBD_4.iFourthCheckpointRule[iTeam2]], ciBS_RULE10_UPDATE_CHECKPOINT_3_MRT)
						AND LEGACY_HAVE_WE_PASSED_THE_FOURTH_CHECKPOINT(iTeam2, MC_serverBD_4.iCurrentHighestPriority[iTeam2])
							PRINTLN("[Players_Staggered][Checkpoint][RetryMRT] - LEGACY_PROCESS_TEAM_RESTART_CHECKPOINTS - Setting CP4 MRT = TRUE for team ", iTeam2)
							SET_BIT(MC_ServerBD.iCheckpointBitset, SB_CP4_MRT_1 + iTeam2)
						ENDIF
					ENDIF
				ENDFOR
			ENDIF
		ENDIF
	ENDIF
ENDPROC


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Shared Rules and Objectives
// ##### Description: Various helper functions & wrappers to do with teams
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC VECTOR GET_DROP_OFF_CENTER_FOR_TEAM(BOOL bGround = FALSE, INT iTeam = 0, INT iPartDroppingOff = -1)
	
	UNUSED_PARAMETER(iPartDroppingOff)
	
	VECTOR vcenter
	FLOAT fwater

	IF MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
    	IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDropOffType[MC_serverBD_4.iCurrentHighestPriority[iTeam]] = ciFMMC_DROP_OFF_TYPE_CYLINDER
        OR 	g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDropOffType[MC_serverBD_4.iCurrentHighestPriority[iTeam]] = ciFMMC_DROP_OFF_TYPE_SPHERE
			vcenter = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vDropOff[MC_serverBD_4.iCurrentHighestPriority[iTeam]]
			
			IF MC_serverBD.iNumVehHighestPriority[iTeam] > 0
				
				INT iVeh = -1
				IF getVehicleBeingDroppedOff != NULL
					iVeh = CALL getVehicleBeingDroppedOff(iTeam, iPartDroppingOff)
				ENDIF
				
				IF iVeh != -1
					IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].vDropOffOverride)
						vcenter = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].vDropOffOverride
					ELSE
						IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vDropOff2[MC_serverBD_4.iCurrentHighestPriority[iTeam]])
							IF iVeh != iFirstHighPriorityVehicleThisRule[iTeam]
								PRINTLN("[RCC MISSION] GET_DROP_OFF_CENTER_FOR_TEAM - Radius drop off, using vDropOff2 as this isn't the first priority vehicle on this rule - vehicle dropping off = ",iVeh,", iFirstHighPriorityVehicleThisRule = ",iFirstHighPriorityVehicleThisRule[iTeam])
								PRINTLN("[RCC MISSION] GET_DROP_OFF_CENTER_FOR_TEAM - vDropOff2 = ",g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vDropOff2[MC_serverBD_4.iCurrentHighestPriority[iTeam]])
								vcenter = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vDropOff2[MC_serverBD_4.iCurrentHighestPriority[iTeam]]
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			
			ELIF MC_serverBD.iNumObjHighestPriority[iTeam] > 0
				IF iObjDeliveryOverridesObjToUse > -1
					vCenter = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjDeliveryOverridesObjToUse].vObjDropOffOverride
					
				ENDIF
				
			ENDIF
			
			IF bGround
				FLOAT fTempZ
				IF GET_GROUND_Z_FOR_3D_COORD(vcenter, fTempZ)
					vcenter.z = fTempZ
					PRINTLN("[RCC MISSION] GET_DROP_OFF_CENTER_FOR_TEAM - Grounding drop off center for cylinder/sphere")
				ELSE
					PRINTLN("[RCC MISSION] GET_DROP_OFF_CENTER_FOR_TEAM - Tried to ground drop off center for cylinder/sphere, but GET_GROUND_Z_FOR_3D_COORD failed. Sticking with ", vcenter)
				ENDIF
			ENDIF
			
		ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDropOffType[MC_serverBD_4.iCurrentHighestPriority[iTeam]] = ciFMMC_DROP_OFF_TYPE_PLACED_ZONE
			IF iCurrentDropOffZone[iTeam] > -1
				PRINTLN("[RCC MISSION] GET_DROP_OFF_CENTER_FOR_TEAM - vCurrentDropOffZoneCachedCentre[", iTeam, "]: ", vCurrentDropOffZoneCachedCentre[iTeam])
				RETURN vCurrentDropOffZoneCachedCentre[iTeam]
			ENDIF

   		ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDropOffType[MC_serverBD_4.iCurrentHighestPriority[iTeam]] = ciFMMC_DROP_OFF_TYPE_AREA
        	vcenter = (g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vDropOff[MC_serverBD_4.iCurrentHighestPriority[iTeam]] + g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vDropOff2[MC_serverBD_4.iCurrentHighestPriority[iTeam]])
        	vcenter *= << 0.5, 0.5, 0.5 >>
			PRINTLN("[GET_DROP_OFF_CENTER_FOR_TEAM] vcenter = <<", vcenter.x, ", ", vcenter.y, ", ", vcenter.z, ">>")
    		IF bGround
           		PRINTLN("[GET_DROP_OFF_CENTER_FOR_TEAM] bGround = ", bGround)
    			IF GET_WATER_HEIGHT_NO_WAVES(vcenter, fwater)
    				vcenter.z = fwater
                	PRINTLN("[GET_DROP_OFF_CENTER_FOR_TEAM] GET_WATER_HEIGHT_NO_WAVES ... fwater = ", fwater)
             	ELSE
                	GET_GROUND_Z_FOR_3D_COORD(vcenter, vcenter.z)
					PRINTLN("[GET_DROP_OFF_CENTER_FOR_TEAM] vcenter.z = ", vcenter.z)
					IF vCenter.z = 0.0
						PRINTLN("[GET_DROP_OFF_CENTER_FOR_TEAM] vcenter.z was 0. Replacing with bottom of angled area.")
						vCenter.z = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vDropOff2[MC_serverBD_4.iCurrentHighestPriority[iTeam]].z
					ENDIF
            	ENDIF
        	ENDIF
			
		ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDropOffType[MC_serverBD_4.iCurrentHighestPriority[iTeam]] = ciFMMC_DROP_OFF_TYPE_VEHICLE

			IF (g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDropOff_Vehicle[MC_serverBD_4.iCurrentHighestPriority[iTeam]]) > -1
				NETWORK_INDEX niVeh = MC_serverBD_1.sFMMC_SBD.niVehicle[ g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDropOff_Vehicle[MC_serverBD_4.iCurrentHighestPriority[iTeam]] ]
				IF NETWORK_DOES_NETWORK_ID_EXIST(niVeh)
				AND IS_VEHICLE_DRIVEABLE(NET_TO_VEH(niVeh))
					vcenter = GET_ENTITY_COORDS(NET_TO_VEH(niVeh))
				ENDIF
			ENDIF
			
		
		ENDIF
	ENDIF
     
	RETURN vcenter
	
ENDFUNC

FUNC VECTOR GET_DROP_OFF_CENTER( BOOL bground = FALSE )
	
	RETURN GET_DROP_OFF_CENTER_FOR_TEAM( bground, MC_playerBD[iPartToUse].iteam, iPartToUse )
	
ENDFUNC

FUNC FLOAT GET_DROP_OFF_HEIGHT(INT iTeam, INT iRule)

	IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].fDropOffHeight[iRule] != IGNORE_DROP_OFF_HEIGHT
		RETURN g_FMMC_STRUCT.sFMMCEndConditions[iTeam].fDropOffHeight[iRule]
	ENDIF
	
	FLOAT fcoronaheight = LOCATE_SIZE_HEIGHT
	
	IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		VEHICLE_INDEX tempVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
		IF IS_VEHICLE_DRIVEABLE(tempVeh)
			IF IS_PED_IN_ANY_HELI(LocalPlayerPed)
			OR IS_PED_IN_ANY_PLANE(LocalPlayerPed)
			OR IS_BIG_VEHICLE(tempVeh)
			OR GET_ENTITY_MODEL(tempVeh) = RIOT
			OR GET_ENTITY_MODEL(tempVeh) = RIOT2
			OR IS_PED_IN_ANY_BOAT(LocalPlayerPed)
			OR GET_ENTITY_MODEL(tempVeh) = SUBMERSIBLE
			OR GET_ENTITY_MODEL(tempVeh) = STROMBERG
			OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEight[iRule], ciBS_RULE8_OVERRIDE_CHECK_VERICALLY)
			OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iDropOffBitSet,  iRule )
			OR IS_ENTITY_IN_WATER(LocalPlayerPed)
				fcoronaheight = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].fDropOffRadius[iRule]
			ENDIF
		ENDIF
	ENDIF
	
	RETURN fCoronaHeight
	
ENDFUNC

FUNC BOOL IS_TEAM_ON_GET_AND_DELIVER_RULE( INT iTeam )
	BOOL bReturn = FALSE
	
	IF MC_serverBD.iNumVehCreated > 0
		IF MC_serverBD.iNumVehHighestPriority[ iTeam ] > 0 
			IF MC_serverBD_4.iVehMissionLogic[ iTeam ] = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER
				CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Team ", iTeam, " is on a vehicle get & deliver" )
				bReturn = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF MC_serverBD.iNumPedCreated > 0
		IF MC_serverBD.iNumPedHighestPriority[ iTeam ] > 0 
			IF MC_serverBD_4.iPedMissionLogic[ iTeam ] = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER
				CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Team ", iTeam, " is on a ped get & deliver" )
				bReturn = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF MC_serverBD.iNumObjCreated > 0
		IF MC_serverBD.iNumObjHighestPriority[ iTeam ] > 0 
			IF MC_serverBD_4.iObjMissionLogic[ iTeam ] = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER
				CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Team ", iTeam, " is on a object get & deliver" )
				bReturn = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT bReturn
		CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Team ", iTeam, " is not on a get & deliver" )
	ENDIF
	
	RETURN bReturn
ENDFUNC

FUNC BOOL IS_NEXT_RULE_A_CUTSCENE()
	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iPlayerRuleLoop
	
	FOR iPlayerRuleLoop = 0 TO MC_serverBD.iNumPlayerRuleCreated-1 
		IF MC_serverBD_4.iPlayerRulePriority[iPlayerRuleLoop][iteam] = MC_serverBD_4.iCurrentHighestPriority[iteam]+1
			IF MC_serverBD_4.iPlayerRule[ iPlayerRuleLoop ][ iteam ] = FMMC_OBJECTIVE_LOGIC_SCRIPTED_CUTSCENE
				RETURN TRUE
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_THIS_RULE_A_CUTSCENE()
	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iPlayerRuleLoop
	
	FOR iPlayerRuleLoop = 0 TO MC_serverBD.iNumPlayerRuleCreated-1 
		IF MC_serverBD_4.iPlayerRulePriority[iPlayerRuleLoop][iteam] = MC_serverBD_4.iCurrentHighestPriority[iteam]
			IF MC_serverBD_4.iPlayerRule[ iPlayerRuleLoop ][ iteam ] = FMMC_OBJECTIVE_LOGIC_SCRIPTED_CUTSCENE
				RETURN TRUE
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_TEAM_FINISH_POSITION(INT iteam)
	
	IF MC_serverBD.iNumActiveTeams = 4
		IF MC_serverBD.iWinningTeam = iteam
			RETURN 1
		ELIF MC_serverBD.iSecondTeam = iteam
			RETURN 2
		ELIF MC_serverBD.iThirdTeam = iteam
			RETURN 3
		ELSE
			RETURN 4
		ENDIF
	ELIF MC_serverBD.iNumActiveTeams = 3
		IF MC_serverBD.iWinningTeam = iteam
			RETURN 1
		ELIF MC_serverBD.iSecondTeam = iteam
			RETURN 2
		ELSE
			RETURN 3
		ENDIF
	ELIF MC_serverBD.iNumActiveTeams = 2
		IF MC_serverBD.iWinningTeam = iteam
			RETURN 1
		ELSE
			RETURN 2
		ENDIF
	ELSE
		RETURN 1
	ENDIF

	RETURN 1
	
ENDFUNC

FUNC BOOL HAS_TEAM_PASSED_MISSION(INT iteam)

INT iteamrepeat
INT iPassScore[FMMC_MAX_TEAMS]

	#IF IS_DEBUG_BUILD
		IF iPlayerPressedF !=-1
			PRINTLN("[RCC MISSION] FAIL - DEBUG")
			RETURN FALSE
		ENDIF
		IF iPlayerPressedS !=-1
			PRINTLN("[RCC MISSION] PASS - DEBUG")
			RETURN TRUE
		ENDIF
	#ENDIF
	
	INT iLocalTeamScore[ FMMC_MAX_TEAMS ]
	
	INT iTeamLoop = 0
	
	FOR iTeamLoop = 0 TO FMMC_MAX_TEAMS -1
		iLocalTeamScore[ iTeamLoop ] = MC_serverBD.iTeamScore[ iTeamLoop ] + iEarlyCelebrationTeamPoints[ iTeamLoop ]
	ENDFOR

	IF NOT HAS_TEAM_FAILED(iteam)
		
		FOR iteamrepeat = 0 TO (FMMC_MAX_TEAMS-1)
			IF g_FMMC_STRUCT.sFMMCEndConditions[iteamrepeat].iMissionTargetScore = -1
				IF MC_serverBD.iNumberOfPlayingPlayers[iteamrepeat] > 0
					iPassScore[iteamrepeat] = MC_serverBD.iNumberOfPlayingPlayers[iteamrepeat]
				ELSE
					iPassScore[iteamrepeat]  = 1
				ENDIF
			ELIF g_FMMC_STRUCT.sFMMCEndConditions[iteamrepeat].iMissionTargetScore != 0
			
				iPassScore[iteamrepeat] = g_FMMC_STRUCT.sFMMCEndConditions[iteamrepeat].iMissionTargetScore
			ELSE
				iPassScore[iteamrepeat]  = 1
			ENDIF
		ENDFOR
		
		IF g_FMMC_STRUCT.iMissionEndType = ciMISSION_SCORING_TYPE_TIME	
			IF MC_serverBD.iNumberOfTeams > 1
				IF GET_TEAM_FINISH_POSITION(iteam) = 1
					PRINTLN("[RCC MISSION] PASS - TIMETRAIL IN 1ST")
					RETURN TRUE
				ELSE
					PRINTLN("[RCC MISSION] FAIL - TIMETRAIL NOT IN 1ST")
					RETURN FALSE
				ENDIF
			ELSE
				PRINTLN("[RCC MISSION] PASS - TIMETRAIL only team")
				RETURN TRUE
			ENDIF
		ELIF g_FMMC_STRUCT.iMissionEndType = ciMISSION_SCORING_TYPE_ALL_VS_1
			
			IF MC_serverBD.iWinningTeam = 0
				IF iteam = 0
					PRINTLN("[RCC MISSION] PASS - ALL_VS_1, TEAM 0 WIN - THIS IS TEAM")
					RETURN TRUE
				ELSE
					PRINTLN("[RCC MISSION] FAIL - ALL_VS_1, TEAM 0 WIN - THIS ISN'T TEAM 0")
					RETURN FALSE
				ENDIF
			ELSE
				IF iteam = 0
					PRINTLN("[RCC MISSION] FAIL - ALL_VS_1, TEAM 0 LOSE - THIS IS TEAM 0")
					RETURN FALSE
				ELSE
					PRINTLN("[RCC MISSION] PASS - ALL_VS_1, TEAM 0 LOSE - THIS ISN'T TEAM 0, THIS IS TEAM ",iTeam)
					RETURN TRUE
				ENDIF
			ENDIF
			
		ELSE
			IF g_FMMC_STRUCT.iCoopMissionTargetScore != 0
				IF ( iLocalTeamScore[ 0 ] + iLocalTeamScore[ 1 ] + iLocalTeamScore[ 2 ] + iLocalTeamScore[ 3 ] ) >= g_FMMC_STRUCT.iCoopMissionTargetScore
					PRINTLN("[RCC MISSION] PASS - COOP SCORE MET")
					RETURN TRUE
				ENDIF
			ENDIF
			IF ((GET_TEAM_FINISH_POSITION(iteam) = 1 AND iLocalTeamScore[ iteam ] >= iPassScore[iteam]) OR (ALL_OTHER_HOSTILE_TEAMS_FAILED(iteam) AND iLocalTeamScore[ iteam ] >= iPassScore[iteam]))
				PRINTLN("[RCC MISSION] PASS - NORMAL -YOU FINISHED 1st ")
				RETURN TRUE
			ENDIF
			FOR iteamrepeat = 0 TO (FMMC_MAX_TEAMS-1)
				IF NOT WAS_TEAM_EVER_ACTIVE(iteamrepeat)
					RELOOP
				ENDIF
				IF (DOES_TEAM_LIKE_TEAM(iteam,iteamrepeat) AND (iLocalTeamScore[ iteamrepeat ] >= iPassScore[iteamrepeat] OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitset2, ciBS2_TARGET_SCORE_CRITICAL)))
				AND (iteamrepeat != iteam OR DO_ALL_TEAMS_LIKE_EACH_OTHER())
				AND NOT HAS_TEAM_FAILED(iteamrepeat)
					PRINTLN("[RCC MISSION] PASS - NORMAL -YOU LIKE A TEAM THAT PASSED OTHER TEAM: ",iteamrepeat)
					RETURN TRUE
				ENDIF
			ENDFOR
			
			//This bit is set when the target score is NOT critical
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitset2, ciBS2_TARGET_SCORE_CRITICAL)
				IF ((GET_TEAM_FINISH_POSITION(iteam) = 1 AND iLocalTeamScore[ iteam ] <= iPassScore[iteam]))
					PRINTLN("[RCC MISSION] PASS - NORMAL - TARGET SCORE NOT CRITICAL AND FINISHED FIRST")
					RETURN TRUE
				ENDIF
			ENDIF
			
			PRINTLN("[RCC MISSION] FAIL - NORMAL -YOU DID NOT FINISH 1st OR LIKE A TEAM THAT DID")
			RETURN FALSE
		ENDIF
	ELSE // you have failed
		FOR iteamrepeat = 0 TO (FMMC_MAX_TEAMS-1)
			IF g_FMMC_STRUCT.sFMMCEndConditions[iteamrepeat].iMissionTargetScore = -1
				IF MC_serverBD.iNumberOfPlayingPlayers[iteamrepeat] > 0
					iPassScore[iteamrepeat] = MC_serverBD.iNumberOfPlayingPlayers[iteamrepeat]
				ELSE
					iPassScore[iteamrepeat]  = 1
				ENDIF
			ELIF g_FMMC_STRUCT.sFMMCEndConditions[iteamrepeat].iMissionTargetScore != 0
				iPassScore[iteamrepeat] = g_FMMC_STRUCT.sFMMCEndConditions[iteamrepeat].iMissionTargetScore
			ELSE
				iPassScore[iteamrepeat]  = 1
			ENDIF
		ENDFOR
		IF g_FMMC_STRUCT.iCoopMissionTargetScore != 0
			IF (iLocalTeamScore[ 0 ] + iLocalTeamScore[ 1 ] + iLocalTeamScore[ 2 ] + iLocalTeamScore[ 3 ] ) >= g_FMMC_STRUCT.iCoopMissionTargetScore
				PRINTLN("[RCC MISSION] PASS - YOUR TEAM FAILED BEFORE END COOP SCORE MET")
				RETURN TRUE
			ENDIF
		ENDIF
		
		FOR iteamrepeat = 0 TO (FMMC_MAX_TEAMS-1)
			IF (DOES_TEAM_LIKE_TEAM(iteam,iteamrepeat)
			AND iLocalTeamScore[ iteamrepeat ] >= iPassScore[iteamrepeat])
			AND iteamrepeat != iteam
			AND NOT HAS_TEAM_FAILED(iteamrepeat)
				PRINTLN("[RCC MISSION] PASS - YOUR TEAM FAILED BEFORE END BUT FRIEND WON")
				RETURN TRUE
			ENDIF
		ENDFOR
		
		PRINTLN("[RCC MISSION] FAIL - EARLY - YOUR TEAM FAILED BEFORE END")
		DEBUG_PRINTCALLSTACK()
		RETURN FALSE
	
	ENDIF
	
	PRINTLN("[RCC MISSION] FAIL - DEFAULT")
	RETURN FALSE

ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Rewards
// ##### Description: 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC ADD_TO_LOCAL_CASH_GRABBED(INT iCashValue)
	MC_playerBD[iLocalPart].sLootBag.iCashGrabbed += iCashValue
	MC_playerBD[iLocalPart].sLootBag.iTotalCashGrabbed += iCashValue
	PRINTLN("[CashGrab] ADD_TO_LOCAL_CASH_GRABBED - Current Cash Grabbed: ", MC_playerBD[iLocalPart].sLootBag.iCashGrabbed, " Total Cash Grabbed: ", MC_playerBD[iLocalPart].sLootBag.iTotalCashGrabbed)
	sMissionLocalContinuityVars.sLootBag.iCashGrabbed = MC_playerBD[iLocalPart].sLootBag.iCashGrabbed
	PRINTLN("[CONTINUITY][CashGrab] ADD_TO_LOCAL_CASH_GRABBED - sMissionLocalContinuityVars.sLootBag.iCashGrabbed = ", sMissionLocalContinuityVars.sLootBag.iCashGrabbed)
	sMissionLocalContinuityVars.sLootBag.iTotalCashGrabbed = MC_playerBD[iLocalPart].sLootBag.iTotalCashGrabbed
	PRINTLN("[CONTINUITY][CashGrab] ADD_TO_LOCAL_CASH_GRABBED - sMissionLocalContinuityVars.sLootBag.iTotalCashGrabbed = ", sMissionLocalContinuityVars.sLootBag.iTotalCashGrabbed)
	
	g_TransitionSessionNonResetVars.bInvolvedInCashGrab = TRUE
	
ENDPROC

/// PURPOSE:
///    Returns the minimum heist take the players can steal on a heist that involves take being dropped.
///    Used in ornate bank finale only but could be extended to work in future heists as well.
///    See Lukasz for details.
/// PARAMS:
///    icash - Cash setting set in the content creator, use FMMC_CASH_ORNATE_BANK for ornate bank.
///    Any other value will return GET_CASH_VALUE_FROM_CREATOR(icash).
/// RETURNS:
///    Minimum take cash amount the players can get on a heist where money dropping is involved.
FUNC INT GET_HEIST_MINIMUM_TAKE_FOR_CASH_DROP(INT icash, BOOL bIndividualTake = FALSE)

	INT 	iBaseTake, iModifiedTake, iMinimumTake
	FLOAT	fHeistDifficulty, fMinimumTakePercentage
	
	SWITCH g_FMMC_STRUCT.iDifficulity
		CASE DIFF_EASY 		fHeistDifficulty = g_sMPTunables.fheist_difficulty_easy 	BREAK
		CASE DIFF_NORMAL 	fHeistDifficulty = g_sMPTunables.fheist_difficulty_normal 	BREAK
		CASE DIFF_HARD 		fHeistDifficulty = g_sMPTunables.fheist_difficulty_hard 	BREAK
		DEFAULT				fHeistDifficulty = g_sMPTunables.fheist_difficulty_normal	BREAK
	ENDSWITCH
	
	
	IF bIndividualTake
		iBaseTake = MC_playerBD[iLocalPart].sLootBag.iTotalCashGrabbed
	ELSE
		iBaseTake = GET_CASH_VALUE_FROM_CREATOR(icash)
	ENDIF
	
	iModifiedTake = FLOOR(TO_FLOAT(iBaseTake) * fHeistDifficulty )
	
	SWITCH icash

		CASE FMMC_CASH_ORNATE_BANK
			
			fMinimumTakePercentage	= TO_FLOAT(100 - CLAMP_INT(g_sMPTunables.imax_heist_cash_loss_percentage, 0, 100)) / 100.0 
			iMinimumTake			= FLOOR(TO_FLOAT(iModifiedTake) * fMinimumTakePercentage)

		BREAK
		
		CASE FMMC_CASH_ISLAND_HEIST
			
			fMinimumTakePercentage	= TO_FLOAT(100 - CLAMP_INT(g_sMPTunables.iIsland_Heist_Max_Cash_Loss_By_Drop_Percentage, 0, 100)) / 100.0 
			iMinimumTake			= FLOOR(TO_FLOAT(iModifiedTake) * fMinimumTakePercentage)

		BREAK
		
		DEFAULT
			iMinimumTake = GET_CASH_VALUE_FROM_CREATOR(icash)
		BREAK
		
	ENDSWITCH
	
	PRINTLN("GET_HEIST_MINIMUM_TAKE_FOR_CASH_DROP - icash: ", icash)
	PRINTLN("GET_HEIST_MINIMUM_TAKE_FOR_CASH_DROP - iBaseTake: ", iBaseTake)
	PRINTLN("GET_HEIST_MINIMUM_TAKE_FOR_CASH_DROP - fHeistDifficulty: ", fHeistDifficulty)
	PRINTLN("GET_HEIST_MINIMUM_TAKE_FOR_CASH_DROP - iModifiedTake: ", iModifiedTake)
	PRINTLN("GET_HEIST_MINIMUM_TAKE_FOR_CASH_DROP - fMinimumTakePercentage: ", fMinimumTakePercentage)
	PRINTLN("GET_HEIST_MINIMUM_TAKE_FOR_CASH_DROP - iMinimumTake: ", iMinimumTake)
	PRINTLN("GET_HEIST_MINIMUM_TAKE_FOR_CASH_DROP - fEarnings_Heists_Finale_replay_cash_reward: ", 		g_sMPTunables.fEarnings_Heists_Finale_replay_cash_reward)
	PRINTLN("GET_HEIST_MINIMUM_TAKE_FOR_CASH_DROP - fEarnings_Heists_Finale_first_play_cash_reward: ", 	g_sMPTunables.fEarnings_Heists_Finale_first_play_cash_reward)
	
	RETURN iMinimumTake

ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Cutscene Helper Functions
// ##### Description: Various helper functions to do with cutscenes
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL IS_MOCAP_CUTSCENE_A_PLACEHOLDER(FMMC_CUTSCENE_TYPE eCutType #IF IS_DEBUG_BUILD , STRING sMocapName #ENDIF )
	IF (eCutType = FMMCCUT_ENDMOCAP AND MC_ServerBD.iEndCutscene = ciMISSION_CUTSCENE_PLACE_HOLDER)
	#IF IS_DEBUG_BUILD OR (eCutType = FMMCCUT_MOCAP AND DOES_STRING_CONTAIN_STRING(sMocapName, "Placeholder")) #ENDIF
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_MOCAP_PLACEHOLDER_CUTSCENE_FINISHED()
	IF iScriptedCutsceneTeam != -1
	AND GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_ServerBD.tdCutsceneStartTime[iScriptedCutsceneTeam], FALSE, TRUE) < 3500
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_INTRO_CUTSCENE_RULE_ACTIVE()
	
	IF IS_BIT_SET(MC_ServerBD.iServerBitSet8, SBBOOL8_FINISHED_INTRO_CUTSCENE)
		RETURN FALSE
	ENDIF
	
	IF MC_serverBD_4.iCurrentHighestPriority[0] != 0
		//If team 0 isn't on rule 0 we can't be on an intro cutscene rule.
		RETURN FALSE
	ENDIF
	
	RETURN IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciENABLE_START_IN_CUTSCENE)
	
ENDFUNC

FUNC NETWORK_INDEX GET_CUTSCNE_VISIBLE_NET_ID(INT itype, INT ientity)

	NETWORK_INDEX TargetNetID= NULL
	PRINTLN("[KW_CUTSCNES]GET_CUTSCNE_VISIBLE_NET_ID itype , =",itype )
	PRINTLN("[KW_CUTSCNES]GET_CUTSCNE_VISIBLE_NET_ID ientity =",ientity )
	
	SWITCH itype

		CASE CREATION_TYPE_PEDS		
			IF NOT IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[ientity])
				PRINTLN("[KW_CUTSCNES]GET_CUTSCNE_VISIBLE_NET_ID CREATION_TYPE_PEDS ientity =",ientity, " Exists." )
				TargetNetID = MC_serverBD_1.sFMMC_SBD.niPed[ientity]
			ELSE
				PRINTLN("[KW_CUTSCNES]GET_CUTSCNE_VISIBLE_NET_ID CREATION_TYPE_PEDS ientity =",ientity, " is dead or does not exist.")
			ENDIF			
		BREAK
		
		CASE CREATION_TYPE_VEHICLES
			IF IS_NET_VEHICLE_DRIVEABLE(MC_serverBD_1.sFMMC_SBD.niVehicle[ientity])
				PRINTLN("[KW_CUTSCNES]GET_CUTSCNE_VISIBLE_NET_ID CREATION_TYPE_VEHICLES ientity =",ientity, " Exists.")
				TargetNetID = MC_serverBD_1.sFMMC_SBD.niVehicle[ientity]
			ELSE
				PRINTLN("[KW_CUTSCNES]GET_CUTSCNE_VISIBLE_NET_ID CREATION_TYPE_VEHICLES ientity =",ientity, " is dead or does not exist.")
			ENDIF
		BREAK
		
		CASE CREATION_TYPE_OBJECTS
			IF NETWORK_DOES_NETWORK_ID_EXIST(GET_OBJECT_NET_ID(ientity))
			AND NOT IS_ENTITY_DEAD(NET_TO_ENT(GET_OBJECT_NET_ID(ientity)))
				PRINTLN("[KW_CUTSCNES]GET_CUTSCNE_VISIBLE_NET_ID CREATION_TYPE_OBJECTS ientity =",ientity, "Exists" )
				TargetNetID = GET_OBJECT_NET_ID(ientity)
			ELSE
				PRINTLN("[KW_CUTSCNES]GET_CUTSCNE_VISIBLE_NET_ID CREATION_TYPE_OBJECTS ientity =",ientity, " is dead or does not exist.")
			ENDIF			
		BREAK
		
		CASE CREATION_TYPE_DYNOPROPS			
			IF NOT IS_ENTITY_DEAD(NET_TO_ENT(GET_DYNOPROP_NET_ID(iEntity)))
				PRINTLN("[KW_CUTSCNES]GET_CUTSCNE_VISIBLE_NET_ID CREATION_TYPE_DYNOPROPS ientity =",ientity, " Exists" )
				TargetNetID = GET_DYNOPROP_NET_ID(iEntity)
			ELSE
				PRINTLN("[KW_CUTSCNES]GET_CUTSCNE_VISIBLE_NET_ID CREATION_TYPE_DYNOPROPS ientity =",ientity, " is dead or does not exist.")
			ENDIF
		BREAK
		
	ENDSWITCH

	RETURN TargetNetID

ENDFUNC

FUNC ENTITY_INDEX GET_CUTSCNE_VISIBLE_PROP(INT ientity)

	ENTITY_INDEX TargetEntityID= NULL
	IF NOT IS_ENTITY_DEAD(oiProps[ ientity ])
		TargetEntityID = oiProps[ ientity ]
	ENDIF
			
	RETURN TargetEntityID		
	
ENDFUNC

FUNC BOOL SHOULD_CUTSCENE_DELAY_CUTSCENE_START_BECAUSE_OF_ENTITY(INT iCutscene, FMMC_CUTSCENE_TYPE eCutType)

	IF eCutType = FMMCCUT_SCRIPTED		
		IF iCutscene = -1
		OR iCutscene >= FMMC_GET_MAX_SCRIPTED_CUTSCENES()
			RETURN FALSE
		ENDIF
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].iCutsceneBitSet4, ci_CSBS4_DelayCutsceneForCutsceneEntities)
			RETURN FALSE
		ENDIF
	ELIF eCutsceneTypePlaying = FMMCCUT_MOCAP
		IF iCutscene = -1
		OR iCutscene >= MAX_MOCAP_CUTSCENES
			RETURN FALSE
		ENDIF		
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.sMocapCutsceneData[iCutscene].iCutsceneBitSet4, ci_CSBS4_DelayCutsceneForCutsceneEntities)
			RETURN FALSE
		ENDIF
	ELIF eCutsceneTypePlaying = FMMCCUT_ENDMOCAP
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.sEndMocapSceneData.iCutsceneBitSet4, ci_CSBS4_DelayCutsceneForCutsceneEntities)
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF MC_RUN_NET_TIMER_WITH_TIMESTAMP(iCutsceneWaitForEntitiesSafetyTimeStamp, CUTSCENE_WAIT_FOR_ENTITIES_SAFETY_TIMER)
		PRINTLN("[Cutscenes] - SHOULD_CUTSCENE_DELAY_CUTSCENE_START_BECAUSE_OF_ENTITY - CUTSCENE_WAIT_FOR_ENTITIES_SAFETY_TIMER has been hit, giving up on waiting for entities.")
		RETURN FALSE
	ENDIF
	
	PRINTLN("[Cutscenes] - SHOULD_CUTSCENE_DELAY_CUTSCENE_START_BECAUSE_OF_ENTITY - iCutscene: ", iCutscene)
	
	IF iCutscene > -1			
		NETWORK_INDEX niIndex
		INT i = 0
		FOR i = 0 TO FMMC_MAX_CUTSCENE_ENTITIES-1
			IF eCutType = FMMCCUT_SCRIPTED

				IF g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].iIndex = -1
					RELOOP
				ENDIF
							
				IF g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].iType = -1
					RELOOP
				ENDIF
				
				niIndex = GET_CUTSCNE_VISIBLE_NET_ID(g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].iType, g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].iIndex)
				
				IF niIndex = NULL
				OR NOT NETWORK_DOES_NETWORK_ID_EXIST(niIndex)
					PRINTLN("[Cutscenes] - SHOULD_CUTSCENE_DELAY_CUTSCENE_START_BECAUSE_OF_ENTITY - iCutscene: ", iCutscene, " FMMCCUT_SCRIPTED, Waiting for Type: ", g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].iType, " Index: ", g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].iIndex, " to spawn in ")
					RETURN TRUE
				ENDIF
				
			ELIF eCutsceneTypePlaying = FMMCCUT_MOCAP
				
				IF g_FMMC_STRUCT.sMocapCutsceneData[iCutscene].sCutsceneEntities[i].iIndex = -1
					RELOOP
				ENDIF
							
				IF g_FMMC_STRUCT.sMocapCutsceneData[iCutscene].sCutsceneEntities[i].iType = -1
					RELOOP
				ENDIF
				
				niIndex = GET_CUTSCNE_VISIBLE_NET_ID(g_FMMC_STRUCT.sMocapCutsceneData[iCutscene].sCutsceneEntities[i].iType, g_FMMC_STRUCT.sMocapCutsceneData[iCutscene].sCutsceneEntities[i].iIndex)
				
				IF niIndex = NULL
				OR NOT NETWORK_DOES_NETWORK_ID_EXIST(niIndex)
					PRINTLN("[Cutscenes] - SHOULD_CUTSCENE_DELAY_CUTSCENE_START_BECAUSE_OF_ENTITY - iCutscene: ", iCutscene, " FMMCCUT_MOCAP, Waiting for Type: ", g_FMMC_STRUCT.sMocapCutsceneData[iCutscene].sCutsceneEntities[i].iType, " Index: ", g_FMMC_STRUCT.sMocapCutsceneData[iCutscene].sCutsceneEntities[i].iIndex, " to spawn in ")
					RETURN TRUE
				ENDIF
				
			ELIF eCutsceneTypePlaying = FMMCCUT_ENDMOCAP
				
				IF g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[i].iIndex = -1
					RELOOP
				ENDIF
							
				IF g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[i].iType = -1
					RELOOP
				ENDIF
				
				niIndex = GET_CUTSCNE_VISIBLE_NET_ID(g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[i].iType, g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[i].iIndex)
				
				IF niIndex = NULL
				OR NOT NETWORK_DOES_NETWORK_ID_EXIST(niIndex)
					PRINTLN("[Cutscenes] - SHOULD_CUTSCENE_DELAY_CUTSCENE_START_BECAUSE_OF_ENTITY - iCutscene: ", iCutscene, " FMMCCUT_ENDMOCAP, Waiting for Type: ", g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[i].iType, " Index: ", g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[i].iIndex, " to spawn in ")
					RETURN TRUE
				ENDIF
				
			ENDIF
		ENDFOR
	ENDIF		
		
	PRINTLN("[Cutscenes] - SHOULD_CUTSCENE_DELAY_CUTSCENE_START_BECAUSE_OF_ENTITY - No entities to wait for or all entities have spawned!")
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_CUTSCENE_STREAMING_AND_WARP_RANGE(INT iCutsceneIndex, FMMC_CUTSCENE_TYPE cutType)
	
	INT iReturn = 0

	IF cutType != FMMCCUT_ENDMOCAP
		IF iCutsceneIndex < 0 
			RETURN 0
		ENDIF
	ENDIF
	
	SWITCH cutType
		
		CASE FMMCCUT_ENDMOCAP			
			IF g_FMMC_STRUCT.sEndMocapSceneData.iCutsceneWarpRange > 0
				iReturn = g_FMMC_STRUCT.sEndMocapSceneData.iCutsceneWarpRange
				PRINTLN("[Cutscenes] GET_CUTSCENE_STREAMING_AND_WARP_RANGE test 0 iReturn = ", iReturn)
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_PlayEndCutsceneForAllPlayers) 
				iReturn = 99999
				PRINTLN("[Cutscenes] GET_CUTSCENE_STREAMING_AND_WARP_RANGE ciOptionsBS24_PlayEndCutsceneForAllPlayers iReturn = ", iReturn)
			ENDIF			
		BREAK
		
		CASE FMMCCUT_MOCAP
			IF iCutsceneIndex >= MAX_MOCAP_CUTSCENES
				RETURN 0
			ENDIF
			
			IF g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneIndex].iCutsceneWarpRange > 0
				iReturn = g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneIndex].iCutsceneWarpRange
				PRINTLN("[Cutscenes] GET_CUTSCENE_STREAMING_AND_WARP_RANGE test 0 iReturn = ", iReturn)
			ENDIF
		BREAK
		
		CASE FMMCCUT_SCRIPTED
			IF iCutsceneIndex >= FMMC_GET_MAX_SCRIPTED_CUTSCENES()
				RETURN 0
			ENDIF
			
			IF g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneIndex].iCutsceneWarpRange > 0
				iReturn = g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneIndex].iCutsceneWarpRange
				PRINTLN("[Cutscenes] GET_CUTSCENE_STREAMING_AND_WARP_RANGE test 1 iReturn = ", iReturn)
			ENDIF
		BREAK
		
	ENDSWITCH

	PRINTLN("[Cutscenes] GET_CUTSCENE_STREAMING_AND_WARP_RANGE custom value ", iReturn)	
	RETURN iReturn
	
ENDFUNC

FUNC INT GET_CUTSCENE_INCLUSION_RANGE(INT iCutsceneIndex, FMMC_CUTSCENE_TYPE cutType)
	
	INT iReturn = 0

	IF cutType != FMMCCUT_ENDMOCAP
		IF iCutsceneIndex < 0 
			RETURN 0
		ENDIF
	ENDIF
	
	SWITCH cutType
		
		CASE FMMCCUT_ENDMOCAP			
			IF g_FMMC_STRUCT.sEndMocapSceneData.iCutscenePlayerInclusionRange > 0
				IF g_FMMC_STRUCT.sEndMocapSceneData.iCutscenePlayerInclusionRange = 5000
					iReturn = 99999
					PRINTLN("[Cutscenes] GET_CUTSCENE_INCLUSION_RANGE (EndMocap) iReturn = ", iReturn)
				ELSE
					iReturn = g_FMMC_STRUCT.sEndMocapSceneData.iCutscenePlayerInclusionRange					
					PRINTLN("[Cutscenes] GET_CUTSCENE_INCLUSION_RANGE (EndMocap) iReturn = ", iReturn)
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_PlayEndCutsceneForAllPlayers) 
				iReturn = 99999
				PRINTLN("[Cutscenes] GET_CUTSCENE_INCLUSION_RANGE (EndMocap) ciOptionsBS24_PlayEndCutsceneForAllPlayers iReturn = ", iReturn)
			ENDIF			
		BREAK
		
		CASE FMMCCUT_MOCAP
			IF iCutsceneIndex >= MAX_MOCAP_CUTSCENES
				RETURN 0
			ENDIF
			
			IF g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneIndex].iCutscenePlayerInclusionRange > 0				
				IF g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneIndex].iCutscenePlayerInclusionRange = 5000
					iReturn = 99999
					PRINTLN("[Cutscenes] GET_CUTSCENE_INCLUSION_RANGE (Mocap) iReturn = ", iReturn)
				ELSE
					iReturn = g_FMMC_STRUCT.sMocapCutsceneData[iCutsceneIndex].iCutscenePlayerInclusionRange
					PRINTLN("[Cutscenes] GET_CUTSCENE_INCLUSION_RANGE (Mocap) iReturn = ", iReturn)
				ENDIF
			ENDIF
		BREAK
		
		CASE FMMCCUT_SCRIPTED
			IF iCutsceneIndex >= FMMC_GET_MAX_SCRIPTED_CUTSCENES()
				RETURN 0
			ENDIF
			
			IF g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneIndex].iCutscenePlayerInclusionRange > 0
				IF g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneIndex].iCutscenePlayerInclusionRange = 5000
					iReturn = 99999
					PRINTLN("[Cutscenes] GET_CUTSCENE_INCLUSION_RANGE (Scripted) iReturn = ", iReturn)
				ELSE
					iReturn = g_FMMC_STRUCT.sScriptedCutsceneData[iCutsceneIndex].iCutscenePlayerInclusionRange
					PRINTLN("[Cutscenes] GET_CUTSCENE_INCLUSION_RANGE (Scripted) iReturn = ", iReturn)
				ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH

	PRINTLN("[Cutscenes] GET_CUTSCENE_INCLUSION_RANGE custom value ", iReturn)	
	RETURN iReturn
	
ENDFUNC

FUNC FLOAT GET_CUTSCENE_PLAYER_INCLUSION_RANGE(INT iCutsceneIndex, FMMC_CUTSCENE_TYPE cutType)
	
	// Default value
	FLOAT fReturn = 200.0
	
	INT iStreamingRange = GET_CUTSCENE_INCLUSION_RANGE(iCutsceneIndex, cutType)
	IF iStreamingRange > 0
		fReturn = TO_FLOAT(iStreamingRange)
	ENDIF
	
	PRINTLN("[Cutscenes] GET_CUTSCENE_PLAYER_INCLUSION_RANGE fReturn = ", fReturn)
	RETURN fReturn
	
ENDFUNC

FUNC FLOAT GET_CUTSCENE_PLAYER_WARP_RANGE(INT iCutsceneIndex, FMMC_CUTSCENE_TYPE cutType)
	
	// Default value
	FLOAT fReturn = 101.0
	
	INT iStreamingRange = GET_CUTSCENE_STREAMING_AND_WARP_RANGE(iCutsceneIndex, cutType)
	IF iStreamingRange > 0
		fReturn = TO_FLOAT(iStreamingRange)
	ENDIF
	
	PRINTLN("[Cutscenes] GET_CUTSCENE_PLAYER_WARP_RANGE fReturn = ", fReturn)
	RETURN fReturn
	
ENDFUNC

PROC PROCESS_PASS_OVER_MOCAP_PLAYER_PED_ORIGINAL_CLONES()
	INT iCutsceneClonePed = 0
	INT i = 0
	FOR i = 0 TO FMMC_MAX_TEAMS-1
		FOR iCutsceneClonePed = 0 TO FMMC_MAX_CUTSCENE_PLAYERS-1
			IF DOES_ENTITY_EXIST(MocapCachedIntroCutscenePlayerPedClone[i][iCutsceneClonePed])
				IF IS_A_STRAND_MISSION_BEING_INITIALISED()
				OR (IS_CORONA_INITIALISING_A_QUICK_RESTART() OR IS_THIS_A_ROUNDS_MISSION_FOR_CORONA())
				OR (MC_serverBD.sServerFMMC_EOM.bVotePassed AND	MC_serverBD.sServerFMMC_EOM.iVoteStatus = ciFMMC_EOM_VOTE_STATUS_QUICK_RESTART)
					PRINTLN("[Player Model Swap] - PROCESS_PASS_OVER_MOCAP_PLAYER_PED_ORIGINAL_CLONES - Passing over MocapCachedIntroCutscenePlayerPedClone for Team: ", i, " Ped Clone: ", iCutsceneClonePed)
					PASS_OVER_MISSION_MOCAP_ORIGINAL_PED_CLONE(MocapCachedIntroCutscenePlayerPedClone[i][iCutsceneClonePed], MocapCachedIntroCutscenePlayer[i][iCutsceneClonePed], iCutsceneClonePed, i)				
				ENDIF
			ENDIF
		ENDFOR
	ENDFOR
ENDPROC

FUNC BOOL IS_LOCAL_PLAYER_PLAYING_CUTSCENE()
	IF IS_CUTSCENE_PLAYING()
	OR g_bInMissionControllerCutscene
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC FLOAT GET_CUTSCENE_HEADING_FROM_LAST_LOCATION(INT iLocation)
	IF iLocation > -1
		RETURN g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLocation].sWarpLocationSettings.fHeading[0]	
	ENDIF
	RETURN 0.0
ENDFUNC

FUNC VECTOR GET_CUTSCENE_COORDINATE_FROM_LAST_LOCATION(INT iLocation)
	IF iLocation > -1
		RETURN g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLocation].sWarpLocationSettings.vPosition[0]	
	ENDIF
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC VECTOR GET_END_CUTSCENE_ROTATION()
	VECTOR vReturn = <<0.0, 0.0, 0.0>>
		
	SWITCH MC_ServerBD.iEndCutscene
		CASE ciMISSION_CUTSCENE_CASINO_HEIST_DRP1
		CASE ciMISSION_CUTSCENE_CASINO_HEIST_DRP2
		CASE ciMISSION_CUTSCENE_CASINO_HEIST_DRP3
		CASE ciMISSION_CUTSCENE_ISLAND_HEIST_HS4F_MAIN_ENT // Use Default or set up in the Creator.
		CASE ciMISSION_CUTSCENE_ISLAND_HEIST_HS4F_NRP_ENT
		CASE ciMISSION_CUTSCENE_ISLAND_HEIST_HS4F_DRP_OFF
		CASE ciMISSION_CUTSCENE_ISLAND_HEIST_HS4F_TUN_ENT
		CASE ciMISSION_CUTSCENE_ISLAND_HEIST_HS4F_MAIN_EXT
		CASE ciMISSION_CUTSCENE_ISLAND_HEIST_HS4F_TRO_ENT
		CASE ciMISSION_CUTSCENE_ISLAND_HEIST_HS4F_SSG_ENT_V1
		CASE ciMISSION_CUTSCENE_ISLAND_HEIST_HS4F_SSG_ENT_V2
		CASE ciMISSION_CUTSCENE_ISLAND_HEIST_HS4F_EXT
		CASE ciMISSION_CUTSCENE_ISLAND_HEIST_HS4F_SSG_EXT_V1
		CASE ciMISSION_CUTSCENE_ISLAND_HEIST_HS4F_SSG_EXT_V2
		CASE ciMISSION_CUTSCENE_TUNER_TUNF_DRP_HAND
		CASE ciMISSION_CUTSCENE_TUNER_TUNF_DRP_BAG
		CASE ciMISSION_CUTSCENE_TUNER_TUNF_DRP_AVI
		CASE ciMISSION_CUTSCENE_FIXER_FIX_GOLF_EXT
		CASE ciMISSION_CUTSCENE_FIXER_FIX_TRIP1_EXT
		CASE ciMISSION_CUTSCENE_FIXER_FIX_STU_EXT
		CASE ciMISSION_CUTSCENE_FIXER_FIX_TRIP2_EXT
		CASE ciMISSION_CUTSCENE_FIXER_FIX_FIN_MCS3
		CASE ciMISSION_CUTSCENE_FIXER_FIXF_TRIP3_EXT					
			vReturn = <<0.0, 0.0, 0.0>>
		BREAK
	ENDSWITCH
	
	IF NOT IS_VECTOR_ZERO(MC_ServerBD.vEndCutsceneOverridePosition)		
		vReturn = <<0.0, 0.0, MC_ServerBD.fEndCutsceneOverrideHeading>>
		PRINTLN("GET_END_CUTSCENE_ROTATION - Returning MC_ServerBD.fEndCutsceneOverrideHeading: ", vReturn)
		RETURN vReturn
	ENDIF
	
	IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sEndMocapSceneData.vCutsceneRotation)
		PRINTLN("GET_END_CUTSCENE_ROTATION - Returning sEndMocapSceneData.vCutsceneRotation: ", g_FMMC_STRUCT.sEndMocapSceneData.vCutsceneRotation)
		RETURN g_FMMC_STRUCT.sEndMocapSceneData.vCutsceneRotation
	ENDIF
	
	IF MC_ServerBD.iLastCompletedGoToLocation > -1
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[MC_ServerBD.iLastCompletedGoToLocation].iLocBS3, ciLoc_BS3_UseFirstAdditionalSpawnPosAsEndCutscenePos)
			vReturn.z = GET_CUTSCENE_HEADING_FROM_LAST_LOCATION(MC_ServerBD.iLastCompletedGoToLocation)
			PRINTLN("GET_END_CUTSCENE_ROTATION - iLastCompletedGoToLocation: ", MC_ServerBD.iLastCompletedGoToLocation, " vReturn: ", vReturn, " USING ciLoc_BS3_UseFirstAdditionalSpawnPosAsEndCutscenePos")
		ENDIF
	ENDIF
	
	PRINTLN("GET_END_CUTSCENE_ROTATION - MC_ServerBD.iEndCutscene: ", MC_ServerBD.iEndCutscene, " vReturn: ", vReturn)
	
	IF IS_VECTOR_ZERO(vReturn)
		CERRORLN(DEBUG_CONTROLLER, "GET_END_CUTSCENE_ROTATION - Returning a ZERO vector!")
	ENDIF
	
	RETURN vReturn
ENDFUNC

FUNC VECTOR GET_END_CUTSCENE_COORDS()
	VECTOR vReturn = <<0,0,0>>
	FLOAT fGroundZ
	
	SWITCH MC_ServerBD.iEndCutscene
		//Casino
		CASE ciMISSION_CUTSCENE_CASINO_HEIST_DRP1
			vReturn = GET_CUTSCENE_COORDINATE_FROM_LAST_LOCATION(MC_ServerBD.iLastCompletedGoToLocation)					
			vReturn.z += 1.5
			GET_GROUND_Z_FOR_3D_COORD(vReturn, fGroundZ, FALSE)
			vReturn.z = fGroundZ
			PRINTLN("GET_CUTSCENE_COORDS - 		iLastCompletedGoToLocation: ", MC_ServerBD.iLastCompletedGoToLocation, " vReturn: ", vReturn)
		BREAK
		CASE ciMISSION_CUTSCENE_CASINO_HEIST_DRP2
			vReturn = GET_CUTSCENE_COORDINATE_FROM_LAST_LOCATION(MC_ServerBD.iLastCompletedGoToLocation)
			vReturn.z += 1.5
			GET_GROUND_Z_FOR_3D_COORD(vReturn, fGroundZ, FALSE)
			vReturn.z = fGroundZ
			PRINTLN("GET_CUTSCENE_COORDS - 		iLastCompletedGoToLocation: ", MC_ServerBD.iLastCompletedGoToLocation, " vReturn: ", vReturn)
		BREAK
		CASE ciMISSION_CUTSCENE_CASINO_HEIST_DRP3
			vReturn = GET_CUTSCENE_COORDINATE_FROM_LAST_LOCATION(MC_ServerBD.iLastCompletedGoToLocation)
			vReturn.z += 1.5
			GET_GROUND_Z_FOR_3D_COORD(vReturn, fGroundZ, FALSE)
			vReturn.z = fGroundZ
			PRINTLN("GET_CUTSCENE_COORDS - 		iLastCompletedGoToLocation: ", MC_ServerBD.iLastCompletedGoToLocation, " vReturn: ", vReturn)
		BREAK
		
		//Island
		CASE ciMISSION_CUTSCENE_ISLAND_HEIST_HS4F_MAIN_ENT
			vReturn = <<4983.14, -5711.86, 19.70>>
		BREAK		
		CASE ciMISSION_CUTSCENE_ISLAND_HEIST_HS4F_NRP_ENT
			vReturn = <<4983.14, -5711.86, 19.70>>
		BREAK
		CASE ciMISSION_CUTSCENE_ISLAND_HEIST_HS4F_DRP_OFF
			vReturn = <<-1806.87, 458.00, 129.65>>
		BREAK
		CASE ciMISSION_CUTSCENE_ISLAND_HEIST_HS4F_TUN_ENT
			vReturn = <<5045.03, -5817.05, -12.60>>
		BREAK
		CASE ciMISSION_CUTSCENE_ISLAND_HEIST_HS4F_MAIN_EXT
			vReturn = <<4983.14, -5711.86, 19.69>>
		BREAK
		CASE ciMISSION_CUTSCENE_ISLAND_HEIST_HS4F_TRO_ENT
			vReturn = <<4983.144043, -5711.86084, 19.69935>>
		BREAK		
		CASE ciMISSION_CUTSCENE_ISLAND_HEIST_HS4F_SSG_ENT_V1
			vReturn = <<4961.799805, -5797.19043, 26.348686>>
		BREAK		
		CASE ciMISSION_CUTSCENE_ISLAND_HEIST_HS4F_SSG_ENT_V2
			vReturn = <<4961.799805, -5797.19043, 26.348686>>
		BREAK		
		CASE ciMISSION_CUTSCENE_ISLAND_HEIST_HS4F_EXT
			vReturn = <<-1806.10, 427.80, 131.00>>
		BREAK	
		CASE ciMISSION_CUTSCENE_ISLAND_HEIST_HS4F_SSG_EXT_V1
			vReturn = <<4961.799805, -5797.19043, 26.348686>>
		BREAK		
		CASE ciMISSION_CUTSCENE_ISLAND_HEIST_HS4F_SSG_EXT_V2
			vReturn = <<4961.799805, -5797.19043, 26.348686>>
		BREAK
		
		// Tuner
		CASE ciMISSION_CUTSCENE_TUNER_TUNF_DRP_HAND
			vReturn = <<4961.799805, -5797.19043, 26.348686>>
		BREAK
		
		CASE ciMISSION_CUTSCENE_TUNER_TUNF_DRP_BAG
			vReturn = <<4961.799805, -5797.19043, 26.348686>>
		BREAK
		
		CASE ciMISSION_CUTSCENE_TUNER_TUNF_DRP_AVI
			vReturn = <<57.466, -2520.512, 7.710>>
		BREAK
		
		CASE ciMISSION_CUTSCENE_FIXER_FIX_GOLF_EXT
			vReturn = <<-1717.55, -1118.07, 12.1537>>
		BREAK
		
		CASE ciMISSION_CUTSCENE_FIXER_FIX_TRIP1_EXT
			vReturn = <<138.940, 159.614, 104.769>>
		BREAK
		
		CASE ciMISSION_CUTSCENE_FIXER_FIX_STU_EXT
			vReturn = <<-996.520,-68.014,-99.000>>
		BREAK
		
		CASE ciMISSION_CUTSCENE_FIXER_FIX_TRIP2_EXT
			vReturn = <<25.595,546.232,174.974>>		
		BREAK
		
		CASE ciMISSION_CUTSCENE_FIXER_FIX_FIN_MCS3
			vReturn = <<-3036.1533, 103.9163, 10.6028>>		
		BREAK

		CASE ciMISSION_CUTSCENE_FIXER_FIXF_TRIP3_EXT
			vReturn = << -553.827,302.335,82.929 >>
		BREAK
		
	ENDSWITCH
	
	IF NOT IS_VECTOR_ZERO(MC_ServerBD.vEndCutsceneOverridePosition)
		PRINTLN("GET_END_CUTSCENE_COORDS - Returning MC_ServerBD.vEndCutsceneOverridePosition: ", MC_ServerBD.vEndCutsceneOverridePosition)
		RETURN MC_ServerBD.vEndCutsceneOverridePosition
	ENDIF
	
	IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sEndMocapSceneData.vCutscenePosition)
		PRINTLN("GET_END_CUTSCENE_COORDS - Returning sEndMocapSceneData.vCutscenePosition: ", g_FMMC_STRUCT.sEndMocapSceneData.vCutscenePosition)
		RETURN g_FMMC_STRUCT.sEndMocapSceneData.vCutscenePosition
	ENDIF
	
	IF MC_ServerBD.iLastCompletedGoToLocation > -1
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[MC_ServerBD.iLastCompletedGoToLocation].iLocBS3, ciLoc_BS3_UseFirstAdditionalSpawnPosAsEndCutscenePos)
			vReturn = GET_CUTSCENE_COORDINATE_FROM_LAST_LOCATION(MC_ServerBD.iLastCompletedGoToLocation)
			PRINTLN("GET_END_CUTSCENE_COORDS - iLastCompletedGoToLocation: ", MC_ServerBD.iLastCompletedGoToLocation, " vReturn: ", vReturn, " USING ciLoc_BS3_UseFirstAdditionalSpawnPosAsEndCutscenePos")
		ENDIF
	ENDIF
	
	PRINTLN("GET_END_CUTSCENE_COORDS - MC_ServerBD.iEndCutscene: ", MC_ServerBD.iEndCutscene)
	
	IF IS_VECTOR_ZERO(vReturn)
		CERRORLN(DEBUG_CONTROLLER, "GET_END_CUTSCENE_COORDS - Returning a ZERO vector!")
	ENDIF
	
	RETURN vReturn
ENDFUNC

FUNC BOOL HAS_VALID_PLAYERS_MOVED_TO_PROCESS_SHOTS()	
	INT iPart
	PRINTLN("[PLAYER_LOOP] - HAS_VALID_PLAYERS_MOVED_TO_PROCESS_SHOTS")
	FOR iPart = 0 TO NUM_NETWORK_PLAYERS-1
		PARTICIPANT_INDEX participantIndex = INT_TO_PARTICIPANTINDEX(iPart)
		
		IF NETWORK_IS_PARTICIPANT_ACTIVE(participantIndex)
			IF NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_ANY_SPECTATOR)
			AND NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_HEIST_SPECTATOR)
			
				IF NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet4, PBBOOL4_REACHED_CUT_PROCESS_SHOTS)
					PRINTLN("[LM][HAS_VALID_PLAYERS_MOVED_TO_PROCESS_SHOTS] Waiting for ", iPart, " so returning FALSE") 
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	PRINTLN("[LM][HAS_VALID_PLAYERS_MOVED_TO_PROCESS_SHOTS] All players reached process shots, returning TRUE")
	RETURN TRUE
ENDFUNC

FUNC eFMMC_SCRIPTED_CUTSCENE_PROGRESS GET_SCRIPTED_CUTSCENE_STATE_PROGRESS()
	RETURN eScriptedCutsceneProgress
ENDFUNC

FUNC eFMMC_MOCAP_PROGRESS GET_MOCAP_MANAGE_CUTSCENE_STATE_PROGRESS()
	RETURN eMocapManageCutsceneProgress
ENDFUNC

FUNC eFMMC_MOCAP_RUNNING_PROGRESS GET_MOCAP_RUNNING_CUTSCENE_STATE_PROGRESS()
	RETURN eMocapRunningCutsceneProgress
ENDFUNC

FUNC BOOL IS_CAM_CURRENTLY_FIRST_PERSON()

	IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		IF IS_PED_ON_ANY_BIKE(LocalPlayerPed)
			IF GET_CAM_VIEW_MODE_FOR_CONTEXT( CAM_VIEW_MODE_CONTEXT_ON_BIKE ) = CAM_VIEW_MODE_FIRST_PERSON
				CPRINTLN(DEBUG_MISSION, "IS_CAM_CURRENTLY_FIRST_PERSON - TRUE, On bike with bike view mode set to first person.")
				RETURN TRUE
			ENDIF
		ELIF IS_PED_IN_ANY_BOAT(LocalPlayerPed)
			IF GET_CAM_VIEW_MODE_FOR_CONTEXT( CAM_VIEW_MODE_CONTEXT_IN_BOAT ) = CAM_VIEW_MODE_FIRST_PERSON
				CPRINTLN(DEBUG_MISSION, "IS_CAM_CURRENTLY_FIRST_PERSON - TRUE, In boat with boat view mode set to first person.")
				RETURN TRUE
			ENDIF
		ELIF IS_PED_IN_ANY_PLANE(LocalPlayerPed)
			IF GET_CAM_VIEW_MODE_FOR_CONTEXT( CAM_VIEW_MODE_CONTEXT_IN_AIRCRAFT ) = CAM_VIEW_MODE_FIRST_PERSON
				CPRINTLN(DEBUG_MISSION, "IS_CAM_CURRENTLY_FIRST_PERSON - TRUE, In plane with aircraft view mode set to first person.")
				RETURN TRUE
			ENDIF
		ELIF IS_PED_IN_ANY_SUB(LocalPlayerPed)
			IF GET_CAM_VIEW_MODE_FOR_CONTEXT( CAM_VIEW_MODE_CONTEXT_IN_SUBMARINE ) = CAM_VIEW_MODE_FIRST_PERSON
				CPRINTLN(DEBUG_MISSION, "IS_CAM_CURRENTLY_FIRST_PERSON - TRUE, In sub with sub view mode set to first person.")
				RETURN TRUE
			ENDIF
		ELIF IS_PED_IN_ANY_HELI(LocalPlayerPed)
			IF GET_CAM_VIEW_MODE_FOR_CONTEXT( CAM_VIEW_MODE_CONTEXT_IN_HELI ) = CAM_VIEW_MODE_FIRST_PERSON
				CPRINTLN(DEBUG_MISSION, "IS_CAM_CURRENTLY_FIRST_PERSON - TRUE, In heli with heli view mode set to first person.")
				RETURN TRUE
			ENDIF
		ELSE
			IF GET_CAM_VIEW_MODE_FOR_CONTEXT( CAM_VIEW_MODE_CONTEXT_IN_VEHICLE ) = CAM_VIEW_MODE_FIRST_PERSON
				CPRINTLN(DEBUG_MISSION, "IS_CAM_CURRENTLY_FIRST_PERSON - TRUE, In vehicle with vehicle view mode set to first person.")
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE
		IF GET_CAM_VIEW_MODE_FOR_CONTEXT( CAM_VIEW_MODE_CONTEXT_ON_FOOT ) = CAM_VIEW_MODE_FIRST_PERSON
			CPRINTLN(DEBUG_MISSION, "IS_CAM_CURRENTLY_FIRST_PERSON - TRUE, Not in vehicle with on foot view mode set to first person.")
			RETURN TRUE
		ENDIF
	ENDIF 

	RETURN FALSE

ENDFUNC

proc clone_gameplay_camera()
	
	IF NOT IS_CAM_CURRENTLY_FIRST_PERSON()
	AND NOT IS_CINEMATIC_CAM_RENDERING()
		camera_a = create_cam_with_params("default_scripted_camera", GET_FINAL_RENDERED_CAM_COORD(), GET_FINAL_RENDERED_CAM_ROT(), GET_FINAL_RENDERED_CAM_FOV()) 
		set_cam_near_clip(camera_a, GET_FINAL_RENDERED_CAM_NEAR_CLIP())
		set_cam_far_clip(camera_a, GET_FINAL_RENDERED_CAM_FAR_CLIP())
		set_cam_motion_blur_strength(camera_a, GET_FINAL_RENDERED_CAM_MOTION_BLUR_STRENGTH())

		set_cam_active(camera_a, true)
		
		render_script_cams(true, false)
	
	ENDIF

endproc 

FUNC BOOL MISSION_HAS_VALID_MOCAP()

	IF MC_ServerBD.iEndCutscene != ciMISSION_CUTSCENE_DEFAULT
		RETURN TRUE
	ENDIF

	RETURN FALSE
	
ENDFUNC 

PROC PROCESS_HIDE_PERSONAL_VEHICLES_LOCALLY_FOR_PLAYER(VEHICLE_INDEX VehToExclude = NULL)
		
	INT iPart = -1
	WHILE DO_PARTICIPANT_LOOP(iPart)
				
		IF NOT DOES_ENTITY_EXIST(MPGlobals.RemotePV[iPart])	
			RELOOP
		ENDIF
		
		IF VehToExclude = MPGlobals.RemotePV[iPart]
			RELOOP
		ENDIF
		
		PRINTLN("PROCESS_HIDE_PERSONAL_VEHICLES_LOCALLY_FOR_PLAYER = Vehicle ", iPart, " setting as invisible during cutscene.")	
		
		SET_ENTITY_LOCALLY_INVISIBLE(MPGlobals.RemotePV[iPart])
		
	ENDWHILE
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Player Abilities
// ##### Description: Shared functions for checking ability state and properties
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

// Returns true if supplied ability is active (i.e. in use)
FUNC BOOL IS_PLAYER_ABILITY_ACTIVE(FMMC_PLAYER_ABILITY_TYPES eAbilityType)
	RETURN sLocalPlayerAbilities.sAbilities[ENUM_TO_INT(eAbilityType)].eState = PAS_ACTIVE
ENDFUNC

FUNC BOOL IS_PLAYER_ABILITY_ACTIVE_FOR_ANY_PLAYER_ON_MY_TEAM(FMMC_PLAYER_ABILITY_TYPES eAbilityType)
	
	// early return if local player has the ability active
	IF IS_PLAYER_ABILITY_ACTIVE(eAbilityType)		
		RETURN TRUE
	ENDIF

	// otherwise return true if any player on this player's team has the ability active	
	INT i
	FOR i = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION() - 1
		IF MC_playerBD[i].iTeam = MC_playerBD[iLocalPart].iTeam
			IF MC_playerBD_1[i].bPlayerAbilityIsActive[ENUM_TO_INT(eAbilityType)]
				RETURN TRUE
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN FALSE

ENDFUNC

// Returns true if an invalid target has been marked for the sniper
FUNC BOOL HAS_SUPPORT_SNIPER_MARKED_INVALID_TARGET()
	RETURN IS_BIT_SET(sLocalPlayerAbilities.sSupportSniperData.iBS_SupportSniperBitset, ciSupportSniperBS_MarkedInvalidTarget)
ENDFUNC

// Returns true if the sniper's marked target has been killed
FUNC BOOL HAS_SUPPORT_SNIPER_KILLED_TARGET()
	RETURN IS_BIT_SET(sLocalPlayerAbilities.sSupportSniperData.iBS_SupportSniperBitset, ciSupportSniperBS_KilledTarget)	
ENDFUNC

// Returns true if the sniper ability is in the right state to select a target
FUNC BOOL IS_SUPPORT_SNIPER_READY_TO_TAG_TARGET()
	IF IS_PLAYER_ABILITY_ACTIVE(PA_SUPPORT_SNIPER)
	AND sLocalPlayerAbilities.sSupportSniperData.eSSState = SS_READY
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_SUPPORT_SNIPER_AIMING_AT_TAGGED_TARGET()
	IF IS_PLAYER_ABILITY_ACTIVE(PA_SUPPORT_SNIPER)
	AND sLocalPlayerAbilities.sSupportSniperData.eSSState = SS_AIM
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_SUPPORT_SNIPER_SPOTTED_PENALTY_APPLY()

	#IF IS_DEBUG_BUILD
		IF sLocalPlayerAbilities.sSupportSniperData.bPlayerSpottedDebug
			RETURN TRUE
		ENDIF
	#ENDIF

	RETURN FALSE

ENDFUNC

FUNC INT GET_SNIPER_TIME_TILL_EXTRACTION()
	FLOAT fTimeTillExtraction = TO_FLOAT(g_FMMC_STRUCT.sPlayerAbilities.sSupportSniper.iExtractionDuration) * 1000
	
	IF SHOULD_SUPPORT_SNIPER_SPOTTED_PENALTY_APPLY()
		// get the penalty as a percentage of the extraction time
		FLOAT fPenalty = g_FMMC_STRUCT.sPlayerAbilities.sSupportSniper.iPlayerSpottedPenalty * 0.01
		fPenalty *= fTimeTillExtraction
		// subtract this from the extraction time
		fTimeTillExtraction -= fPenalty
	ENDIF
		
	RETURN CEIL(fTimeTillExtraction)
ENDFUNC

FUNC BOOL HAS_SUPPORT_SNIPER_ENDED_WITHOUT_MAKING_KILL()
	RETURN IS_BIT_SET(sLocalPlayerAbilities.sSupportSniperData.iBS_SupportSniperBitset, ciSupportSniperBS_SniperEndedWithNoTarget)
ENDFUNC

//Returns the time in seconds since the local player fired their weapon
FUNC INT GET_TIME_IN_SECONDS_SINCE_LOCAL_PLAYER_FIRED_THEIR_WEAPON()	
	IF iLastTimePlayerFiredTheirWeaponInSec > -1
	AND wtLastUsedWeapon != WEAPONTYPE_INVALID
		INT iCurrentTimeInSec = GET_CLOUD_TIME_AS_INT()
		//PRINTLN("GET_TIME_IN_SECONDS_SINCE_LOCAL_PLAYER_FIRED_THEIR_WEAPON - iLastTimePlayerFiredTheirWeaponInSec = ", iLastTimePlayerFiredTheirWeaponInSec, ", iCurrentTimeInSec = ", iCurrentTimeInSec, ", Difference = ", iCurrentTimeInSec - iLastTimePlayerFiredTheirWeaponInSec, "s")
		RETURN iCurrentTimeInSec - iLastTimePlayerFiredTheirWeaponInSec
	ENDIF
	
	RETURN 0
ENDFUNC

FUNC WEAPON_TYPE GET_LAST_USED_WEAPON()	
	RETURN wtLastUsedWeapon
ENDFUNC

FUNC INT GET_DISTRACTION_TIME_TO_USE(INT iPed)
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedCopDecoyActiveTime > 0
		RETURN g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedCopDecoyActiveTime
	ENDIF
	
	IF g_FMMC_STRUCT.sPlayerAbilities.sAbilities[PA_DISTRACTION].iActiveDuration > 0
		RETURN g_FMMC_STRUCT.sPlayerAbilities.sAbilities[PA_DISTRACTION].iActiveDuration
	ENDIF
	
	RETURN 60
ENDFUNC

PROC CLEAN_UP_AMBUSH_PLAYER_ABILITY()
	INT iAmbushVehicle
	FOR iAmbushVehicle = 0 TO ciAMBUSH_ABILITY_VEHICLES - 1
		ENTITY_INDEX eiAmbushVehicle = GET_FMMC_ENTITY(ciENTITY_TYPE_VEHICLE, sLocalPlayerAbilities.sAmbushData.iAmbushVehicleIndexes[iAmbushVehicle])
		
		IF NETWORK_HAS_CONTROL_OF_ENTITY(eiAmbushVehicle)
			SET_ENTITY_AS_NO_LONGER_NEEDED(eiAmbushVehicle)
			PRINTLN("[PlayerAbilities][AmbushAbility][Vehicle ", sLocalPlayerAbilities.sAmbushData.iAmbushVehicleIndexes[iAmbushVehicle], "] CLEAN_UP_AMBUSH_PLAYER_ABILITY | Setting iAmbushVehicleIndexes[", iAmbushVehicle, "] as no longer needed.")
		ENDIF
	ENDFOR
	
	INT iAmbushPed
	FOR iAmbushPed = 0 TO ciAMBUSH_ABILITY_PEDS - 1
		ENTITY_INDEX eiAmbushPed = GET_FMMC_ENTITY(ciENTITY_TYPE_PED, sLocalPlayerAbilities.sAmbushData.iAmbushPedIndexes[iAmbushPed])
		
		IF NETWORK_HAS_CONTROL_OF_ENTITY(eiAmbushPed)
			SET_ENTITY_AS_NO_LONGER_NEEDED(eiAmbushPed)
			PRINTLN("[PlayerAbilities][AmbushAbility][Ped ", sLocalPlayerAbilities.sAmbushData.iAmbushPedIndexes[iAmbushPed], "] CLEAN_UP_AMBUSH_PLAYER_ABILITY | Setting iAmbushPedIndexes[", iAmbushPed, "] as no longer needed.")
		ENDIF
	ENDFOR
ENDPROC

// Returns true if the use limit set in the creator is being used
FUNC BOOL SHOULD_ENFORCE_ABILITY_USE_LIMIT(FMMC_PLAYER_ABILITY_TYPES eType)
	RETURN g_FMMC_STRUCT.sPlayerAbilities.sAbilities[ENUM_TO_INT(eType)].iNumUses > 0
ENDFUNC

// Returns the number of remaining uses on the supplied ability
FUNC INT GET_NUM_REMAINING_ABILITY_USES(FMMC_PLAYER_ABILITY_TYPES eType)
	RETURN g_FMMC_STRUCT.sPlayerAbilities.sAbilities[ENUM_TO_INT(eType)].iNumUses - sLocalPlayerAbilities.sAbilities[ENUM_TO_INT(eType)].iUseCount
ENDFUNC

FUNC INT GET_USE_COUNT_FOR_PLAYER_ABILITY(FMMC_PLAYER_ABILITY_TYPES eType)
	RETURN sLocalPlayerAbilities.sAbilities[ENUM_TO_INT(eType)].iUseCount
ENDFUNC

// returns true if an active duration has been set
// will always return false for abilities that do not have their active duration exposed in the creator
FUNC BOOL ABILITY_HAS_ACTIVE_DURATION(FMMC_PLAYER_ABILITY_TYPES eType)
	SWITCH eType
		CASE PA_RECON_DRONE
			RETURN g_FMMC_STRUCT.sPlayerAbilities.sAbilities[ENUM_TO_INT(eType)].iActiveDuration > 0
		
		CASE PA_RIDE_ALONG
			RETURN g_FMMC_STRUCT.sPlayerAbilities.sAbilities[ENUM_TO_INT(eType)].iActiveDuration > 0
			
		DEFAULT 
			RETURN FALSE			
	ENDSWITCH	
ENDFUNC

// returns true if an ability type starts its timer at some point after activation, rather than straight away
FUNC BOOL ABILITY_USES_DELAYED_TIMER(FMMC_PLAYER_ABILITY_TYPES eType)
	SWITCH eType
		CASE PA_RIDE_ALONG
			RETURN TRUE
			
		DEFAULT 
			RETURN FALSE			
	ENDSWITCH	
ENDFUNC

// returns true if a cooldown duration has been set
FUNC BOOL ABILITY_HAS_COOLDOWN_DURATION(FMMC_PLAYER_ABILITY_TYPES eType)
	// add debug opt out here
#IF IS_DEBUG_BUILD
	IF sLocalPlayerAbilities.bDebugSkipCooldowns
		RETURN FALSE
	ENDIF
#ENDIF

	RETURN g_FMMC_STRUCT.sPlayerAbilities.sAbilities[ENUM_TO_INT(eType)].iCooldownDuration > 0
ENDFUNC

// returns true if the supplied ability has been used
FUNC BOOL ABILITY_HAS_BEEN_USED(FMMC_PLAYER_ABILITY_TYPES eType)
	RETURN sLocalPlayerAbilities.sAbilities[ENUM_TO_INT(eType)].iUseCount > 0
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Rounds, Restarts & Strands
// ##### Description: Various helper functions to do with rounds, restarts or strands
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL SHOULD_TURN_OFF_CONTROL_AT_END_OF_MISSION( STRING strMocap = NULL )

	IF MC_serverBD.iNextMission < FMMC_MAX_STRAND_MISSIONS
	AND MC_serverBD.iNextMission >= 0
	AND NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.tl23NextContentID[MC_serverBD.iNextMission])
		IF g_FMMC_STRUCT.eBranchingTransitionType[MC_serverBD.iNextMission] = FMMC_BRANCHING_MISSION_TRANSITION_TYPE_AIRLOCK
			RETURN FALSE
		ENDIF
	ENDIF

	IF HAS_TEAM_PASSED_MISSION( MC_playerBD[iLocalPart].iTeam )
		IF NOT IS_STRING_NULL_OR_EMPTY(strMocap)
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

///Determines whether the player the winning or losing postfx:
FUNC BOOL SHOULD_POSTFX_BE_WINNER_VERSION(BOOL bCurrentlyOnEarlyDeathScreen = FALSE)
	IF NOT bCurrentlyOnEarlyDeathScreen //Always do a fail transition if the player died early and we're transitioning.
		RETURN HAS_TEAM_PASSED_MISSION(MC_playerBD[iLocalPart].iteam)
	ENDIF
	RETURN FALSE
ENDFUNC

PROC PROCESS_SERVER_END_OF_MISSION_CONTINUITY()
	
	IF !bIsLocalPlayerHost
		EXIT
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_TimeofDayTracking)
		MC_ServerBD_1.sMissionContinuityVars.iTimeHour = GET_CLOCK_HOURS()
		MC_ServerBD_1.sMissionContinuityVars.iTimeMinute = GET_CLOCK_MINUTES()
		PRINTLN("[CONTINUITY] - PROCESS_SERVER_END_OF_MISSION_CONTINUITY - MC_ServerBD_1.sMissionContinuityVars.iTimeHour: ",MC_ServerBD_1.sMissionContinuityVars.iTimeHour,", MC_ServerBD_1.sMissionContinuityVars.iTimeMinute: ",MC_ServerBD_1.sMissionContinuityVars.iTimeMinute)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_TrackLeaderHeistCompletionStat)
		MC_ServerBD_1.sMissionContinuityVars.iCurrentHeistCompletionStat = GET_CURRENT_HEIST_COMPLETION_STAT_FOR_LEADER(#IF IS_DEBUG_BUILD TRUE #ENDIF)
		PRINTLN("[CONTINUITY] - PROCESS_SERVER_END_OF_MISSION_CONTINUITY - MC_ServerBD_1.sMissionContinuityVars.iCurrentHeistCompletionStat: ", MC_ServerBD_1.sMissionContinuityVars.iCurrentHeistCompletionStat)
	ENDIF
	
	MC_ServerBD_1.sMissionContinuityVars.iTotalMissionTime += MC_serverBD.iTotalMissionEndTime
	PRINTLN("[CONTINUITY] - PROCESS_SERVER_END_OF_MISSION_CONTINUITY - iTotalMissionTime: ", MC_ServerBD_1.sMissionContinuityVars.iTotalMissionTime)
	
	//Will only get saved if we are moving onto another mission so can be incremented every time
	MC_ServerBD_1.sMissionContinuityVars.iStrandMissionsComplete++
	
ENDPROC

PROC PROCESS_END_OF_MISSION_CONTINUITY_SAVING()

	//Assuming we have checked this isnt a quick restart and we are moving onto the next strand mission before this is called.
	
	INT i
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_PrerequisiteTracking)
		FOR i = 0 TO FMMC_MAX_INTERACTABLE_CONTINUITY_BITSET - 1
			sMissionLocalContinuityVars.iPrerequisiteBS[i] = MC_playerBD[iLocalPart].iPrerequisiteBS[i]
		ENDFOR
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_ObjectCarrierTracking)
		sMissionLocalContinuityVars.iObjectHeld = iLastDeliveredContinuityObjectThisRule
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_OutfitTracking)
		sMissionLocalContinuityVars.iOutfit = MC_playerBD[iLocalPart].iOutfit
		IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_CHANGED_INTO_DISGUISE)
			SET_BIT(sMissionLocalContinuityVars.iGeneralLocalTrackingBitset, ciContinuityGenericTrackingBS_DisguiseUsed)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_PlayerAbilityTracking)
		FOR i = 0 TO ciNUM_PLAYER_ABILITIES - 1
			sMissionLocalContinuityVars.sPlayerAbilities.sPAContinuity[i].bIsActive = IS_PLAYER_ABILITY_ACTIVE(INT_TO_ENUM(FMMC_PLAYER_ABILITY_TYPES, i))
			sMissionLocalContinuityVars.sPlayerAbilities.sPAContinuity[i].iUseCount = GET_USE_COUNT_FOR_PLAYER_ABILITY(INT_TO_ENUM(FMMC_PLAYER_ABILITY_TYPES, i))
		ENDFOR
	ENDIF

	sMissionLocalContinuityVars.iTripSkipCost = MC_playerBD[iLocalPart].TSPlayerData.iCost
	sMissionLocalContinuityVars.iPlayerThermalCharges = MC_playerBD_1[iLocalPart].iThermitePerPlayerRemaining
	
	sMissionLocalContinuityVars.iNumPedKills         = MC_Playerbd[iLocalPart].iNumPedKills		
	sMissionLocalContinuityVars.iAmbientCopsKilled   = MC_Playerbd[iLocalPart].iAmbientCopsKilled	
	sMissionLocalContinuityVars.iNumHeadshots        = MC_Playerbd[iLocalPart].iNumHeadshots		
	sMissionLocalContinuityVars.iStartHits           = MC_Playerbd[iLocalPart].iStartHits			
	sMissionLocalContinuityVars.iStartShots          = MC_Playerbd[iLocalPart].iStartShots			
	sMissionLocalContinuityVars.iCivilianKills       = MC_Playerbd[iLocalPart].iCivilianKills	
	sMissionLocalContinuityVars.iNumPlayerDeaths     = MC_Playerbd[iLocalPart].iNumPlayerDeaths	
	sMissionLocalContinuityVars.iHackTime            = MC_Playerbd[iLocalPart].iHackTime			
	sMissionLocalContinuityVars.iNumHacks            = MC_Playerbd[iLocalPart].iNumHacks			
	sMissionLocalContinuityVars.iWantedTime          = MC_Playerbd[iLocalPart].iWantedTime			
	sMissionLocalContinuityVars.iNumWantedLose       = MC_Playerbd[iLocalPart].iNumWantedLose		
	sMissionLocalContinuityVars.iMedalObjectives     = MC_Playerbd[iLocalPart].iMedalObjectives	
	sMissionLocalContinuityVars.iMedalInteractables  = MC_Playerbd[iLocalPart].iMedalInteractables	
	sMissionLocalContinuityVars.iMedalEquipment      = MC_Playerbd[iLocalPart].iMedalEquipment	
	sMissionLocalContinuityVars.iStartStealthKills   = MC_Playerbd[iLocalPart].iStartStealthKills	
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_VehiclePositionTracking)
		FOR i = 0 TO FMMC_MAX_VEHICLES - 1
			
			IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iContinuityID < 0
				RELOOP
			ENDIF
		
			IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[i])
				RELOOP
			ENDIF
			
			sMissionLocalContinuityVars.vVehicleLocations[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iContinuityID] = GET_ENTITY_COORDS(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i]), FALSE)
			PRINTLN("[JS][CONTINUITY] - PROCESS_END_OF_MISSION_CONTINUITY_SAVING - Storing Vehicle ", i, " (Continuity ID: ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iContinuityID, "'s  Position: ", sMissionLocalContinuityVars.vVehicleLocations[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iContinuityID])
			
		ENDFOR		
	ENDIF
	
ENDPROC
		
PROC CACHE_MISSION_CONTINUITY_VARS()
	
	IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
		EXIT
	ENDIF
	
	IF IS_CORONA_INITIALISING_A_QUICK_RESTART()
		EXIT
	ENDIF
	
	IF bIsSCTV
		EXIT
	ENDIF
	
	PROCESS_END_OF_MISSION_CONTINUITY_SAVING()
	
	PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - On a strand mission, caching continuity vars")
	g_TransitionSessionNonResetVars.sMissionContinuityVars = MC_serverBD_1.sMissionContinuityVars
	g_TransitionSessionNonResetVars.sMissionLocalContinuityVars = sMissionLocalContinuityVars
	
#IF IS_DEBUG_BUILD	
	INT i
	//Server Continuity
	PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - iGenericTrackingBitset = ", g_TransitionSessionNonResetVars.sMissionContinuityVars.iGenericTrackingBitset)
	PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - iLocationTrackingBitset = ", g_TransitionSessionNonResetVars.sMissionContinuityVars.iLocationTrackingBitset)
	PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - iVehicleDestroyedTrackingBitset = ", g_TransitionSessionNonResetVars.sMissionContinuityVars.iVehicleDestroyedTrackingBitset)
	PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - iDynoPropDestroyedTrackingBitset = ", g_TransitionSessionNonResetVars.sMissionContinuityVars.iDynoPropDestroyedTrackingBitset)
	FOR i = 0 TO FMMC_MAX_PEDS_BITSET - 1
		PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - iPedDeathBitset[",i,"] = ", g_TransitionSessionNonResetVars.sMissionContinuityVars.iPedDeathBitset[i])
		PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - iPedSpecialBitset[",i,"] = ", g_TransitionSessionNonResetVars.sMissionContinuityVars.iPedSpecialBitset[i])
	ENDFOR
	FOR i = 0 TO FMMC_MAX_OBJ_CONTINUITY_BITSET - 1
		PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - iObjectTrackingBitset[",i,"] = ", g_TransitionSessionNonResetVars.sMissionContinuityVars.iObjectTrackingBitset[i])
		PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - iObjectDestroyedBitset[",i,"] = ", g_TransitionSessionNonResetVars.sMissionContinuityVars.iObjectDestroyedBitset[i])
	ENDFOR
	FOR i = 0 TO FMMC_MAX_INTERACTABLE_CONTINUITY_BITSET - 1
		PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - iInteractablesCompleteBitset[",i,"] = ", g_TransitionSessionNonResetVars.sMissionContinuityVars.iInteractablesCompleteBitset[i])
	ENDFOR
	FOR i = 0 TO FMMC_MAX_DOOR_CONTINUITY_BITSET - 1
		PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - iDoorsUseAltConfigBitset[",i,"] = ", g_TransitionSessionNonResetVars.sMissionContinuityVars.iDoorsUseAltConfigBitset[i])
	ENDFOR
	PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - iTimeHour = ", g_TransitionSessionNonResetVars.sMissionContinuityVars.iTimeHour)
	PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - iTimeMinute = ", g_TransitionSessionNonResetVars.sMissionContinuityVars.iTimeMinute)
	PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - iCurrentHeistCompletionStat = ", g_TransitionSessionNonResetVars.sMissionContinuityVars.iCurrentHeistCompletionStat)	
	PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - iTotalMissionTime = ", g_TransitionSessionNonResetVars.sMissionContinuityVars.iTotalMissionTime)
	PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - iStrandMissionsComplete = ", g_TransitionSessionNonResetVars.sMissionContinuityVars.iStrandMissionsComplete)
	PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - iHackFails = ", g_TransitionSessionNonResetVars.sMissionContinuityVars.iHackFails)
	PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - iGrabbedCashTotalDropped = ", g_TransitionSessionNonResetVars.sMissionContinuityVars.iGrabbedCashTotalDropped)
	PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - iGrabbedCashTotalTake = ", g_TransitionSessionNonResetVars.sMissionContinuityVars.iGrabbedCashTotalTake)
	FOR i = 0 TO FMMC_MAX_TEAMS - 1
		PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - iTeamKills[",i,"] = ", g_TransitionSessionNonResetVars.sMissionContinuityVars.iTeamKills[i])
		PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - iTeamDeaths[",i,"] = ", g_TransitionSessionNonResetVars.sMissionContinuityVars.iTeamDeaths[i])
		PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - iVariableTeamLives[",i,"] = ", g_TransitionSessionNonResetVars.sMissionContinuityVars.iVariableTeamLives[i])
		PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - iTeamHeadshots[",i,"] = ", g_TransitionSessionNonResetVars.sMissionContinuityVars.iTeamHeadshots[i])
		PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - iTeamHeadshots[",i,"] = ", g_TransitionSessionNonResetVars.sMissionContinuityVars.iTeamHeadshots[i])
	ENDFOR
	PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - iContinuitySeed = ", g_TransitionSessionNonResetVars.sMissionContinuityVars.iContinuitySeed)
	PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - iContentSpecificContinuityBitset = ", g_TransitionSessionNonResetVars.sMissionContinuityVars.iContentSpecificContinuityBitset)
	FOR i = 0 TO ciContinuityTelemetryInts_Max - 1
		PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - iTelemetryInts[", i, "] = ", g_TransitionSessionNonResetVars.sMissionContinuityVars.iTelemetryInts[i])
	ENDFOR
	
	//Local Continuity
	PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - iEndVehicleContinuityId = ", g_TransitionSessionNonResetVars.sMissionLocalContinuityVars.iEndVehicleContinuityId)
	PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - eEndVehicleSeat = ", ENUM_TO_INT(g_TransitionSessionNonResetVars.sMissionLocalContinuityVars.eEndVehicleSeat))
	PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - iGeneralLocalTrackingBitset = ", g_TransitionSessionNonResetVars.sMissionLocalContinuityVars.iGeneralLocalTrackingBitset)
	FOR i = 0 TO FMMC_MAX_PICKUP_CONTINUITY_BITSET - 1
		PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - iPickupCollectedTrackingBitset[",i,"] = ", g_TransitionSessionNonResetVars.sMissionLocalContinuityVars.iPickupCollectedTrackingBitset[i])
	ENDFOR
	FOR i = 0 TO ciMAX_DIALOGUE_BIT_SETS - 1
		PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - iDialogueTrackingBitset[",i,"] = ", g_TransitionSessionNonResetVars.sMissionLocalContinuityVars.iDialogueTrackingBitset[i])
	ENDFOR
	PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - iHeistBag = ", g_TransitionSessionNonResetVars.sMissionLocalContinuityVars.iHeistBag)
	PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - iOutfit = ", g_TransitionSessionNonResetVars.sMissionLocalContinuityVars.iOutfit)
	PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - iCashGrabbed = ", g_TransitionSessionNonResetVars.sMissionLocalContinuityVars.sLootBag.iCashGrabbed)
	PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - iTotalCashGrabbed = ", g_TransitionSessionNonResetVars.sMissionLocalContinuityVars.sLootBag.iTotalCashGrabbed)
	PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - iStolenGoods = ", g_TransitionSessionNonResetVars.sMissionLocalContinuityVars.sLootBag.iStolenGoods)
	PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - iLootBitSet = ", g_TransitionSessionNonResetVars.sMissionLocalContinuityVars.sLootBag.iLootBitSet)
	PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - fCurrentBagCapacity = ", g_TransitionSessionNonResetVars.sMissionLocalContinuityVars.sLootBag.fCurrentBagCapacity)
	FOR i = 0 TO ciLOOT_TYPE_MAX - 1
		PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - sLootBag.fCurrentCapacity[",i,"] = ", g_TransitionSessionNonResetVars.sMissionLocalContinuityVars.sLootBag.fCurrentCapacity[i])
	ENDFOR
	
	FOR i = 0 TO ciNUM_PLAYER_ABILITIES - 1
		PRINTLN("[RM][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - sPAContinuity[",i,"] Is Active = ", g_TransitionSessionNonResetVars.sMissionLocalContinuityVars.sPlayerAbilities.sPAContinuity[i].bIsActive)
		PRINTLN("[RM][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - sPAContinuity[",i,"] Use Count = ", g_TransitionSessionNonResetVars.sMissionLocalContinuityVars.sPlayerAbilities.sPAContinuity[i].iUseCount)
	ENDFOR
		
	PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - iObjectHeld = ", g_TransitionSessionNonResetVars.sMissionLocalContinuityVars.iObjectHeld)
	PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - iTripSkipCost = ", g_TransitionSessionNonResetVars.sMissionLocalContinuityVars.iTripSkipCost)
	PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - iNumPedKills = ", g_TransitionSessionNonResetVars.sMissionLocalContinuityVars.iNumPedKills)
	PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - iAmbientCopsKilled = ", g_TransitionSessionNonResetVars.sMissionLocalContinuityVars.iAmbientCopsKilled)
	PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - iNumHeadshots = ", g_TransitionSessionNonResetVars.sMissionLocalContinuityVars.iNumHeadshots)
	PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - iStartHits = ", g_TransitionSessionNonResetVars.sMissionLocalContinuityVars.iStartHits)
	PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - iStartShots = ", g_TransitionSessionNonResetVars.sMissionLocalContinuityVars.iStartShots)
	PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - iCivilianKills = ", g_TransitionSessionNonResetVars.sMissionLocalContinuityVars.iCivilianKills)
	PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - iNumPlayerDeaths = ", g_TransitionSessionNonResetVars.sMissionLocalContinuityVars.iNumPlayerDeaths)
	PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - iHackTime = ", g_TransitionSessionNonResetVars.sMissionLocalContinuityVars.iHackTime)
	PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - iNumHacks = ", g_TransitionSessionNonResetVars.sMissionLocalContinuityVars.iNumHacks)
	PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - iWantedTime = ", g_TransitionSessionNonResetVars.sMissionLocalContinuityVars.iWantedTime)
	PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - iNumWantedLose = ", g_TransitionSessionNonResetVars.sMissionLocalContinuityVars.iNumWantedLose)
	PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - iMedalObjectives = ", g_TransitionSessionNonResetVars.sMissionLocalContinuityVars.iMedalObjectives)
	PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - iMedalInteractables = ", g_TransitionSessionNonResetVars.sMissionLocalContinuityVars.iMedalInteractables)
	PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - iMedalEquipment = ", g_TransitionSessionNonResetVars.sMissionLocalContinuityVars.iMedalEquipment)
	PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - iStartStealthKills = ", g_TransitionSessionNonResetVars.sMissionLocalContinuityVars.iStartStealthKills)
	PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - iPlayerThermalCharges = ", g_TransitionSessionNonResetVars.sMissionLocalContinuityVars.iPlayerThermalCharges)
	PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - iPlayerThermalCharges = ", g_TransitionSessionNonResetVars.sMissionLocalContinuityVars.iNumRebreathersFromFreemode)
	FOR i = 0 TO FMMC_MAX_PREREQ_CONTINUITY_BITSET - 1
		PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - iPrerequisiteBS[",i,"] = ", g_TransitionSessionNonResetVars.sMissionLocalContinuityVars.iPrerequisiteBS[i])
	ENDFOR
	
	FOR i = 0 TO ciLocalContinuityTelemetryInts_Max - 1
		PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - iLocalTelemetryInts[", i, "] = ", g_TransitionSessionNonResetVars.sMissionLocalContinuityVars.iLocalTelemetryInts[i])
	ENDFOR
	
	FOR i = 0 TO FMMC_MAX_VEHICLES - 1
		PRINTLN("[JS][CONTINUITY] - CACHE_MISSION_CONTINUITY_VARS - vVehicleLocations[", i, "] = ", g_TransitionSessionNonResetVars.sMissionLocalContinuityVars.vVehicleLocations[i])
	ENDFOR
	
#ENDIF	

ENDPROC

PROC PROCESS_INITIALISING_STRAND_MISSION()
	
	//Call the function to set up the strand details
	SET_UP_STRAND_DETAILS_AT_END_OF_MISSION(MC_serverBD.iEndCutscene, MC_serverBD.iNextMission)
	SET_BIT(iLocalBoolCheck5, LBOOL5_SET_UP_STRAND_DETAILS_AT_END_OF_MISSION)
	PRINTLN("[RCC MISSION][MSRAND] PROCESS_INITIALISING_STRAND_MISSION - called SET_UP_STRAND_DETAILS_AT_END_OF_MISSION for next mission: ", MC_serverBD.iNextMission)
	
	g_TransitionSessionNonResetVars.iStrandTotalStartingPlayers = GET_TOTAL_STARTING_PLAYERS()
	PRINTLN("[RCC MISSION][MSRAND] PROCESS_INITIALISING_STRAND_MISSION - Caching starting players for next part  = ", GET_TOTAL_STARTING_PLAYERS()) 
	
	INT iTeam
	FOR iteam = 0 TO (FMMC_MAX_TEAMS-1)
		g_TransitionSessionNonResetVars.iStrandStartingPlayers[iteam] = MC_serverBD.iNumStartingPlayers[iteam]
		PRINTLN("[RCC MISSION][MSRAND] PROCESS_INITIALISING_STRAND_MISSION - team = ",iteam," caching from previous strand part = ", MC_serverBD.iNumStartingPlayers[iteam])
	ENDFOR
	
	CACHE_MISSION_CONTINUITY_VARS()
	
ENDPROC

/// PURPOSE: returns TRUE when the start checkpoint has been set or passed
///    		e.g. if checkpoint 2 has been set then checkpoints 1 and 2 will return true if queried as both globals will be set.
FUNC BOOL HAS_MISSION_START_CHECKPOINT_BEEN_SET_OR_PASSED(INT iCheckpoint)

	SWITCH iCheckpoint
	
		CASE 0
			PRINTLN("[RCC MISSION] has_start_checkpoint_been_set_or_passed - 0")
			RETURN TRUE
		BREAK
		
		CASE 1
			IF IS_BIT_SET(iLocalBoolCheck12, LBOOL12_RETRY_CHECKPOINT_1)
			OR IS_BIT_SET(iLocalBoolCheck12, LBOOL12_RETRY_CHECKPOINT_2)
			OR IS_BIT_SET(iLocalBoolCheck11, LBOOL11_RETRY_CHECKPOINT_3)
			OR IS_BIT_SET(iLocalBoolCheck11, LBOOL11_RETRY_CHECKPOINT_4)
				PRINTLN("[RCC MISSION] has_start_checkpoint_been_set_or_passed - 1")
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE 2
			IF IS_BIT_SET(iLocalBoolCheck12, LBOOL12_RETRY_CHECKPOINT_2)
			OR IS_BIT_SET(iLocalBoolCheck11, LBOOL11_RETRY_CHECKPOINT_3)
			OR IS_BIT_SET(iLocalBoolCheck11, LBOOL11_RETRY_CHECKPOINT_4)
				PRINTLN("[RCC MISSION] has_start_checkpoint_been_set_or_passed - 2")
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE 3
			IF IS_BIT_SET(iLocalBoolCheck11, LBOOL11_RETRY_CHECKPOINT_3)
			OR IS_BIT_SET(iLocalBoolCheck11, LBOOL11_RETRY_CHECKPOINT_4)
				PRINTLN("[RCC MISSION] has_start_checkpoint_been_set_or_passed - 3")
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE 4
			IF IS_BIT_SET(iLocalBoolCheck11, LBOOL11_RETRY_CHECKPOINT_4)
				PRINTLN("[RCC MISSION] has_start_checkpoint_been_set_or_passed - 4")
				RETURN TRUE
			ENDIF
		BREAK
		
	ENDSWITCH
	
	RETURN FALSE

ENDFUNC

///PURPOSE: returns true when the checkpoint has been set in a PREVIOUS TRY OF THE MISSION. The data is obtained
///    		once at the beginning of the mission via STORE_MISSION_CHECKPOINT_STATE() within PROCESS_PRE_GAME().
///    		
///    During the previous play of the mission if checkpoint 2 was the last checkpoint reached it will return
///    		true for ONLY checkpoint_stage 2 and not any stages below it.
///    		
///    INT iCheckpoint - the checkpoint you want to check against.
FUNC BOOL IS_MISSION_START_CHECKPOINT_SET(INT iCheckpoint)

	SWITCH iCheckpoint
		
		CASE 1
			IF IS_BIT_SET(iLocalBoolCheck12, LBOOL12_RETRY_CHECKPOINT_1)
				PRINTLN("[RCC MISSION] IS_MISSION_START_CHECKPOINT_SET - LBOOL12_RETRY_CHECKPOINT_1")
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE 2
			IF IS_BIT_SET(iLocalBoolCheck12, LBOOL12_RETRY_CHECKPOINT_2)
				PRINTLN("[RCC MISSION] IS_MISSION_START_CHECKPOINT_SET - LBOOL12_RETRY_CHECKPOINT_2")
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE 3
			IF IS_BIT_SET(iLocalBoolCheck11, LBOOL11_RETRY_CHECKPOINT_3)
				PRINTLN("[RCC MISSION] IS_MISSION_START_CHECKPOINT_SET - LBOOL11_RETRY_CHECKPOINT_3")
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE 4
			IF IS_BIT_SET(iLocalBoolCheck11, LBOOL11_RETRY_CHECKPOINT_4)
				PRINTLN("[RCC MISSION] IS_MISSION_START_CHECKPOINT_SET - LBOOL11_RETRY_CHECKPOINT_4")
				RETURN TRUE
			ENDIF
		BREAK
		
	ENDSWITCH 
	
	RETURN FALSE 

ENDFUNC

FUNC BOOL IS_THIS_A_JOB_THAT_HAS_QUICK_RESTARTS()
	IF IS_THIS_A_CONTACT_MISSION()
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_THIS_THE_FIRST_ROUND_OR_NOT_A_ROUNDS_MISSION()
	IF ( IS_THIS_A_ROUNDS_MISSION() AND g_sTransitionSessionData.sMissionRoundData.iNumberOfRoundsPlayed = 0 )
	OR NOT IS_THIS_A_ROUNDS_MISSION()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_AN_AIRLOCK_TRANSITION_ENDING_OR_STARTING()
	
	IF HAS_TEAM_FAILED(0) //Update if used outside casino heist
	OR IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_PLAYER_FAIL)
	OR IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_PLAYER_OUT_OF_LIVES)
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_TriggerEndCelebration)
	OR IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_bDoDelayedCelebrationFreeze)
	OR sCelebrationData.eCurrentStage > CELEBRATION_STAGE_SETUP
	OR g_bCelebrationScreenIsActive
		RETURN FALSE
	ENDIF
	
	IF g_bStartedInAirlockTransition
		RETURN TRUE
	ENDIF
	
	IF IS_AN_AIRLOCK_STRAND_TRANSITION_RUNNING()
		RETURN TRUE
	ENDIF
	
	IF (g_bEndOfMissionCleanUpHUD
    AND (GET_STRAND_MISSION_TRANSITION_TYPE() = FMMC_BRANCHING_MISSION_TRANSITION_TYPE_AIRLOCK
	OR ((MC_serverBD.iNextMission > -1 AND MC_serverBD.iNextMission < FMMC_MAX_STRAND_MISSIONS)
	AND g_FMMC_STRUCT.eBranchingTransitionType[MC_serverBD.iNextMission] = FMMC_BRANCHING_MISSION_TRANSITION_TYPE_AIRLOCK))
    AND IS_STRAND_MISSION_READY_TO_START_DOWNLOAD())
		//Mission ending and airlock about to be started
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC INT GET_MAX_XP_GAIN_FOR_CELEBRATION_SCREEN(INT iXPGain)
	INT iMaXPGain = PRIVATE_GET_CAP_PLAYER_XP_MAX()
	IF iXPGain > iMaXPGain
		RETURN iMaXPGain
	ENDIF
	RETURN iXPGain
ENDFUNC

FUNC BOOL SHOULD_CELEBRATION_SCREEN_SKIP_JOB_POINTS()

	IF g_FMMC_STRUCT.iCelebrationType = FMMC_CELEBRATION_TYPE_TUNER_ROBBERY
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_HIDE_JOB_POINTS_CELEB_SCREEN)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_CELEBRATION_SCREEN_SKIP_NJVS_AND_LB()

	IF g_FMMC_STRUCT.iCelebrationType = FMMC_CELEBRATION_TYPE_TUNER_ROBBERY
	OR g_FMMC_STRUCT.iCelebrationType = FMMC_CELEBRATION_TYPE_FIXER_CONTRACT
	OR g_FMMC_STRUCT.iCelebrationType = FMMC_CELEBRATION_TYPE_SHORT_TRIP
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: PLACED MARKERS
// ##### Description: Placed Markers Process and helper functions.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_DOWNWARD_PLACED_MARKER(FMMC_MARKER_STRUCT &sPlacedMarker, VECTOR vMarkerCoord, FLOAT fScale, INT ir, INT ig, INT ib, INT ia)
	
	VECTOR vMarkerScaleArrow 		= (<<-0.275, -0.4, -0.275>>) * fScale
	FLOAT fAnimSpeed = (6 * sPlacedMarker.fSpeed) / 2
	vMarkerCoord.z += 0.9 + GET_ANIMATED_SINE_VALUE(fAnimSpeed, sPlacedMarker.fMaxAnimTravelDistance)
	FLOAT fPercentage = 1.0
	FLOAT fPulse = (GET_ANIMATED_SINE_VALUE(6) / 7)
	vMarkerScaleArrow *= 1.0+(fPercentage*0.5) + fPulse
	
	fPercentage *= 100
	
	VECTOR vTargetRot = GET_GAMEPLAY_CAM_ROT()
	vTargetRot.x = 0.0
	vTargetRot.y = 0.0
	
	DRAW_MARKER(MARKER_ARROW, vMarkerCoord, (<<0.0, 0.0, 0.0>>), vTargetRot, vMarkerScaleArrow,  ir, ig, ib, ia, DEFAULT, DEFAULT)
	
ENDPROC

FUNC BOOL SHOULD_PLACED_MARKER_BE_HIDDEN(FMMC_MARKER_STRUCT &sPlacedMarker)
	
	IF ARE_PREREQUISITES_SET_UP_IN_LONG_BITSET(sPlacedMarker.iHideOnPrereqSetBS)
		IF ARE_ALL_PREREQUSITES_IN_LONG_BITSET_COMPLETED(sPlacedMarker.iHideOnPrereqSetBS)			
			RETURN TRUE
		ELSE
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF ARE_PREREQUISITES_SET_UP_IN_LONG_BITSET(sPlacedMarker.iShowOnPrereqSetBS)
		IF ARE_ALL_PREREQUSITES_IN_LONG_BITSET_COMPLETED(sPlacedMarker.iShowOnPrereqSetBS)
			RETURN FALSE
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(sPlacedMarker.iHideOnRulesBS, GET_LOCAL_PLAYER_CURRENT_RULE(TRUE, TRUE))
	AND sPlacedMarker.iHideOnRulesTeam = GET_LOCAL_PLAYER_TEAM(TRUE)
		// Set to be hidden on this Rule for my Team
		RETURN TRUE
	ENDIF
		
	RETURN FALSE
ENDFUNC

PROC PROCESS_PLACED_MARKER(FMMC_MARKER_STRUCT &sPlacedMarker, VECTOR vNewPos, BOOL bForceShow)
	
	IF NOT bForceShow
	AND SHOULD_PLACED_MARKER_BE_HIDDEN(sPlacedMarker)
		EXIT
	ENDIF
	
	VECTOR vMarkerCoord = vNewPos
	IF NOT IS_VECTOR_ZERO(sPlacedMarker.vPosition)
		vMarkerCoord = sPlacedMarker.vPosition
	ENDIF
	
	vMarkerCoord += sPlacedMarker.vOffset
	
	FLOAT fScaleMod	= 0.75 * sPlacedMarker.fScale
	
	HUD_COLOURS hdColour
	
	IF sPlacedMarker.iColour = 0
		hdColour = HUD_COLOUR_PURE_WHITE
	ELSE
		hdColour = GET_HUD_COLOUR_FROM_CREATOR_BLIP_COLOUR(sPlacedMarker.iColour)
	ENDIF
	
	INT ir, ig, ib, ia
	GET_HUD_COLOUR(hdColour, ir, ig, ib, ia)
	
	ia = ROUND(sPlacedMarker.fAlpha)
	
	SWITCH sPlacedMarker.eType
		CASE PLACED_MARKER_TYPE_DOWN_ARROW
			PROCESS_DOWNWARD_PLACED_MARKER(sPlacedMarker, vMarkerCoord, fScaleMod, ir, ig, ib, ia)
		BREAK
	ENDSWITCH
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Meters / Atitude and Speed Bars
// ##### Description: Any kind of metered rule base consequence helper functions.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC CLEAN_UP_ALTITUDE_SYSTEMS()
	PRINTLN("[LM][AltitudeSystem] - CLEAN_UP_ALTITUDE_SYSTEMS - Cleaning up Altitude Systems.")
	IF sAltitudeBarLocal.iCountdownSound != -1
		STOP_SOUND(sAltitudeBarLocal.iCountdownSound)
		RELEASE_SOUND_ID(sAltitudeBarLocal.iCountdownSound)
		sAltitudeBarLocal.iCountdownSound = -1
	ENDIF	
	SET_FAKE_MINIMAP_MAX_ALTIMETER_HEIGHT(0)
	sAltitudeBarLocal.iBS = 0
	sAltitudeBarLocal.fAltitude = 0
	sAltitudeBarLocal.iProxyParticipant = - 1
	sAltitudeBarLocal.fFailTimer = 0
	RESET_NET_TIMER(sAltitudeBarLocal.tdAltitudeUpdateTimer)
	sAltitudeBarLocal.vehToUse = NULL
ENDPROC

FUNC FLOAT GET_PLAYER_ALTITUDE_FROM_TEAM(INT iTeam)
	IF iTeam != MC_PlayerBD[iPartToUse].iTeam
		// fAltitude = Player BD stored var.  MC_PlayerBD[iLocalPart].fCurrentAltitude
	ENDIF
	
	RETURN sAltitudeBarLocal.fAltitude
ENDFUNC
	
FUNC FLOAT GET_PLAYER_ALTITUDE_FAIL_TIMER_FROM_TEAM(INT iTeam)
	IF iTeam != MC_PlayerBD[iPartToUse].iTeam
		// fFailTimer = Player BD stored var.  MC_PlayerBD[iLocalPart].fCurrentAltitudeFailTime
	ENDIF
	
	RETURN sAltitudeBarLocal.fFailTimer
ENDFUNC

FUNC BOOL HAS_EXCEEDED_RULE_ALTITUDE_FAIL_TIMER(INT iRule, INT iTeam, FLOAT fFailTimer)
	IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sAltitudeBar[iRule].iFailTime > 0
		IF fFailTimer >= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sAltitudeBar[iRule].iFailTime
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_EXCEEDED_RULE_ALTITUDE_THRESHOLD(INT iRule, INT iTeam, FLOAT fAltitude)
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sAltitudeBar[iRule].iBS, ciRuleMeteredBehaviour_Enabled)
		IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sAltitudeBar[iRule].iMinimum < g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sAltitudeBar[iRule].iThreshold
			IF fAltitude > g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sAltitudeBar[iRule].iThreshold
				RETURN TRUE
			ENDIF
		ELSE
			IF fAltitude < g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sAltitudeBar[iRule].iThreshold
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Wanted
// ##### Description: Various helper functions to do with wanted stars and police response
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC SET_DISPATCH_SERVICES(BOOL bEnableDispatch, BOOL bDisableHelis, BOOL bForce = FALSE)
	
	IF IS_BIT_SET(iLocalBoolCheck31, LBOOL30_DISPATCH_BLOCKED_BY_ZONE)
	AND !bForce
		IF bEnableDispatch
			PRINTLN("[WANTED] SET_DISPATCH_SERVICES - Services blocked by zone, setting to re-apply after leaving zone")
			SET_BIT(iLocalBoolCheck31, LBOOL30_REAPPLY_DISPATCH_SERVICES_ON_ZONE_EXIT)
		ELSE
			PRINTLN("[WANTED] SET_DISPATCH_SERVICES - Services blocked by zone, setting to not re-apply after leaving zone")
			CLEAR_BIT(iLocalBoolCheck31, LBOOL30_REAPPLY_DISPATCH_SERVICES_ON_ZONE_EXIT)
		ENDIF
		EXIT
	ENDIF
	
	IF NOT bEnableDispatch
		SET_BIT(iLocalBoolCheck5, LBOOL5_DISPATCHOFF)
		PRINTLN("[WANTED] SET_DISPATCH_SERVICES - Turning off police dispatch services...")
	ELSE
		PRINTLN("[WANTED] SET_DISPATCH_SERVICES - Turning on police dispatch services...")
		CLEAR_BIT(iLocalBoolCheck5, LBOOL5_DISPATCHOFF)
	ENDIF
	
	FMMC_SET_DISPATCH_SERVICES(bEnableDispatch, bDisableHelis)
	
ENDPROC

PROC DO_CORONA_POLICE_SETTINGS()
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].iSurpressWantedBitset, 0)
		CLIENT_SET_POLICE(POLICE_OFF)
		g_bDeactivateRestrictedAreas = TRUE
	ELSE
		IF MC_serverBD.iPolice > POLICE_OFF
			IF GET_POLICE_MAX_WANTED_FROM_MENU_SETTING(MC_serverBD.iPolice) > 0
				SET_MAX_WANTED_LEVEL(GET_POLICE_MAX_WANTED_FROM_MENU_SETTING(MC_serverBD.iPolice))
				PRINTLN("[WANTED] DO_CORONA_POLICE_SETTINGS - setting max wanted on client to : ",GET_POLICE_MAX_WANTED_FROM_MENU_SETTING(MC_serverBD.iPolice))
				g_bDeactivateRestrictedAreas = FALSE
			ENDIF
		ELSE
			IF MC_serverBD.iPolice = POLICE_OFF
				g_bDeactivateRestrictedAreas = TRUE
				PRINTLN("[WANTED] DO_CORONA_POLICE_SETTINGS - corona police setting cops and military base off")
			ELSE
				g_bDeactivateRestrictedAreas = FALSE
				PRINTLN("[WANTED] DO_CORONA_POLICE_SETTINGS - corona police setting cops and military base on")
			ENDIF
			
			CLIENT_SET_POLICE(MC_serverBD.iPolice)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciBLOCK_ON_FOOT_DISPATCH)
		SET_CREATE_RANDOM_COPS( FALSE )
		SET_PED_MODEL_IS_SUPPRESSED(S_M_M_Security_01, TRUE)
		PRINTLN("[WANTED] DO_CORONA_POLICE_SETTINGS - Disabling cops and security guards" )
	ENDIF
ENDPROC

FUNC INT GET_GAIN_WANTED_LEVEL()
	
	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]
	IF iRule < FMMC_MAX_RULES
		PRINTLN("[WANTED] GET_GAIN_WANTED_LEVEL - Returning ", g_FMMC_STRUCT.sFMMCEndConditions[iteam].iWantedLevelReq[iRule], " star wanted requirement")
		RETURN (g_FMMC_STRUCT.sFMMCEndConditions[iteam].iWantedLevelReq[iRule]) + 1
	ENDIF
	
	RETURN 0
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: IPLs
// ##### Description: Various helper functions to do with IPLs
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
PROC CACHE__MISSION_CONTAINS_A_PLACED_YACHT()
	IF USING_FMMC_YACHT()
		PRINTLN("[MYACHT] Calling CACHE__MISSION_CONTAINS_A_PLACED_YACHT for Mission Yacht Setting LBOOL3_MISSION_CONTAINS_A_PLACED_YACHT")
		SET_BIT(iLocalBoolCheck3, LBOOL3_MISSION_CONTAINS_A_PLACED_YACHT)
	ENDIF
ENDPROC

FUNC BOOL MISSION_CONTAINS_A_PLACED_YACHT()
	RETURN IS_BIT_SET(iLocalBoolCheck3, LBOOL3_MISSION_CONTAINS_A_PLACED_YACHT)
ENDFUNC

PROC SET_FMMC_YACHT_STATE(INT iYachtIndex, FMMC_YACHT_SPAWN_STATE eNewState)
	sMissionYachtVars[iYachtIndex].eMissionYachtSpawnState = eNewState
	PRINTLN("[MYACHT][Yacht ", iYachtIndex, "] SET_FMMC_YACHT_STATE | Setting mission yacht state to ", eNewState)
ENDPROC

PROC SET_YACHT_NAME_CONTROL(FMMC_YACHT_VARS& sYachtVars, INT iNameControl)
	IF sYachtVars.iYachtNameControl != iNameControl
		sYachtVars.iYachtNameControl = iNameControl
		PRINTLN("[MYACHT] SET_YACHT_NAME_CONTROL | Setting sYachtVars.iYachtNameControl to ", sYachtVars.iYachtNameControl)
	ENDIF
ENDPROC

PROC CLEANUP_FMMC_YACHT_NAME(FMMC_YACHT_VARS& sYachtVars)
	PRINTLN("[MYACHT] Calling CLEANUP_FMMC_YACHT_NAME")
	
	// Clean up
	IF HAS_SCALEFORM_MOVIE_LOADED(sYachtVars.sYachtNameOverlay)
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sYachtVars.sYachtNameOverlay)
	ENDIF
	IF HAS_SCALEFORM_MOVIE_LOADED(sYachtVars.sYachtNameSternOverlay)
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sYachtVars.sYachtNameSternOverlay)
	ENDIF
	IF IS_NAMED_RENDERTARGET_REGISTERED("port_text")   RELEASE_NAMED_RENDERTARGET("port_text")   ENDIF
	IF IS_NAMED_RENDERTARGET_REGISTERED("starb_text")  RELEASE_NAMED_RENDERTARGET("starb_text")  ENDIF
	IF IS_NAMED_RENDERTARGET_REGISTERED("stern_text")  RELEASE_NAMED_RENDERTARGET("stern_text")  ENDIF
	IF DOES_ENTITY_EXIST(sYachtVars.oYachtNamePort)   SAFE_DELETE_OBJECT(sYachtVars.oYachtNamePort)   ENDIF
	IF DOES_ENTITY_EXIST(sYachtVars.oYachtNameStarb)  SAFE_DELETE_OBJECT(sYachtVars.oYachtNameStarb)  ENDIF
	IF DOES_ENTITY_EXIST(sYachtVars.oYachtNameStern)  SAFE_DELETE_OBJECT(sYachtVars.oYachtNameStern)  ENDIF
	
	sYachtVars.iPortRenderID = -1
	sYachtVars.iStarbRenderID = -1
	sYachtVars.iSternRenderID = -1
	SET_YACHT_NAME_CONTROL(sYachtVars, 0)
ENDPROC

PROC CLEANUP_FMMC_YACHT(INT iYachtIndex)
	PRINTLN("[MYACHT] Calling CLEANUP_FMMC_YACHT")
	INT iExtraVeh = 0
	FOR iExtraVeh = 0 TO NUMBER_OF_PRIVATE_YACHT_VEHICLES - 1
		IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.niYachtVehicles[iYachtIndex][iExtraVeh])
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.niYachtVehicles[iYachtIndex][iExtraVeh])
				DELETE_NET_ID(MC_serverBD_1.niYachtVehicles[iYachtIndex][iExtraVeh])
				PRINTLN("[MYACHT] Deleting extra vehicle ", iExtraVeh)
			ENDIF
		ENDIF
	ENDFOR
	
	CLEANUP_FMMC_YACHT_NAME(sMissionYachtVars[iYachtIndex])
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Interiors
// ##### Description: Various helper functions to do with interiors, including the simple interior script and vehicle interiors
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


PROC SET_ALLOW_INTERIOR_SCRIPT_FOR_MISSION(BOOL bActive)
	g_SimpleInteriorData.bAllowInteriorScriptForActivitySession = bActive
	PRINTLN("[RCC MISSION] SET_ALLOW_INTERIOR_SCRIPT_FOR_MISSION - Setting g_SimpleInteriorData.bAllowInteriorScriptForActivitySession to ", bActive)
ENDPROC

FUNC BOOL IS_ANY_PLAYER_IN_AVENGER_PILOT_SEAT(INT &iPilotPart)
	INT i, iPlayersToCheck, iPlayersChecked

	iPlayersToCheck = GET_NUMBER_OF_PLAYING_PLAYERS()
	PRINTLN("[PLAYER_LOOP] - IS_ANY_PLAYER_IN_AVENGER_PILOT_SEAT")
	FOR i = 0 TO (GET_MAXIMUM_PLAYERS_ON_MISSION() - 1)
		IF (NOT(IS_BIT_SET(MC_playerBD[i].iClientBitSet, PBBOOL_ANY_SPECTATOR)))
		AND NOT IS_BIT_SET(MC_playerBD[i].iClientBitSet,PBBOOL_PLAYER_FAIL)
		AND IS_BIT_SET(MC_playerBD[i].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
		AND NOT IS_BIT_SET(MC_playerBD[i].iClientBitSet, PBBOOL_FINISHED)
			PARTICIPANT_INDEX tempPart = INT_TO_PARTICIPANTINDEX(i)
			IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
				iPlayersChecked++
				IF MC_playerBD[i].iVehNear != -1
					PLAYER_INDEX tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
					PED_INDEX tempPed = GET_PLAYER_PED(tempPlayer)
					IF IS_PED_IN_ANY_VEHICLE(tempPed)
						VEHICLE_INDEX tempVeh = GET_VEHICLE_PED_IS_IN(tempPed)
						IF DOES_ENTITY_EXIST(tempVeh)
							IF GET_ENTITY_MODEL(tempVeh) = AVENGER
								iPilotPart = i
								RETURN TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		IF iPlayersChecked >= iPlayersToCheck
			BREAKLOOP
		ENDIF
	ENDFOR
	iPilotPart = -1
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ANY_PLAYER_IN_VEHICLE_INTERIOR()
	INT i
	
	PRINTLN("[PLAYER_LOOP] - IS_ANY_PLAYER_IN_VEHICLE_INTERIOR")
	FOR i = 0 TO (GET_MAXIMUM_PLAYERS_ON_MISSION() - 1)
		IF (NOT(IS_BIT_SET(MC_playerBD[i].iClientBitSet, PBBOOL_ANY_SPECTATOR)))
		AND NOT IS_BIT_SET(MC_playerBD[i].iClientBitSet,PBBOOL_PLAYER_FAIL)
		AND IS_BIT_SET(MC_playerBD[i].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
		AND NOT IS_BIT_SET(MC_playerBD[i].iClientBitSet, PBBOOL_FINISHED)
			PARTICIPANT_INDEX tempPart = INT_TO_PARTICIPANTINDEX(i)
			IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
				PLAYER_INDEX tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
				IF IS_PLAYER_IN_CREATOR_TRAILER(tempPlayer)
				OR IS_PLAYER_IN_CREATOR_AIRCRAFT(tempPlayer)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN FALSE

ENDFUNC

FUNC BOOL IS_ENTITY_IN_SAME_INTERIOR_AS_LOCAL_PLAYER(ENTITY_INDEX EntityIndex)
	
	IF DOES_ENTITY_EXIST(EntityIndex) 
		IF LocalPlayerCurrentInterior = GET_INTERIOR_FROM_ENTITY(EntityIndex)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Model Swaps
// ##### Description: Utility functions for the model swap system.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC INT GET_ACTIVE_MODEL_SWAP_INDEX(INT iModelSwapHash)
	INT iActiveModelSwap
	FOR iActiveModelSwap = 0 TO iCurrentActiveModelSwaps - 1
		IF sActiveModelSwaps[iActiveModelSwap].sModelSwapConfig.iModelSwapHash = iModelSwapHash
			RETURN iActiveModelSwap
		ELSE
			PRINTLN("[ModelSwaps_SPAM] GET_ACTIVE_MODEL_SWAP_INDEX | Not slot ", iActiveModelSwap, " | ", sActiveModelSwaps[iActiveModelSwap].sModelSwapConfig.iModelSwapHash)
		ENDIF
	ENDFOR
	
	PRINTLN("[ModelSwaps_SPAM] GET_ACTIVE_MODEL_SWAP_INDEX | We don't have a slot with this hash!")
	RETURN -1
ENDFUNC

FUNC INT GET_FIRST_FREE_ACTIVE_MODEL_SWAP_INDEX()
	INT iActiveModelSwap
	FOR iActiveModelSwap = 0 TO ciMAX_MODEL_SWAPS - 1
		IF sActiveModelSwaps[iActiveModelSwap].eModelSwapState = MODEL_SWAP_STATE__INVALID
			RETURN iActiveModelSwap
		ENDIF
	ENDFOR
	
	ASSERTLN("[ModelSwaps] GET_FIRST_FREE_ACTIVE_MODEL_SWAP_INDEX | All slots are full!")
	PRINTLN("[ModelSwaps] GET_FIRST_FREE_ACTIVE_MODEL_SWAP_INDEX | All slots are full!")
	RETURN -1
ENDFUNC

FUNC OBJECT_INDEX GET_MODEL_SWAP_ENTITY(INT iActiveModelSwap, FMMC_MODEL_SWAP_ENTITY_DATA& sEntityData, BOOL bGetEntityToSwapIn, BOOL bGetOriginalEntity = FALSE)
	SWITCH sEntityData.eModelSwapType
		CASE MODEL_SWAP_ENTITY_TYPE__PROP
			IF sEntityData.iEntityIndex > -1
			AND sEntityData.iEntityIndex < GET_FMMC_MAX_NUM_PROPS()
				RETURN oiProps[sEntityData.iEntityIndex]
			ENDIF
		BREAK
		
		CASE MODEL_SWAP_ENTITY_TYPE__NETWORKED_OBJECT
			IF NETWORK_DOES_NETWORK_ID_EXIST(sEntityData.niEntityNetID)
				RETURN NET_TO_OBJ(sEntityData.niEntityNetID)
			ENDIF
			
			IF sEntityData.iEntityIndex > -1
			AND sEntityData.iEntityIndex < FMMC_MAX_NUM_NETWORKED_OBJ_POOL
				IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niNetworkedObject[sEntityData.iEntityIndex])
					RETURN NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niNetworkedObject[sEntityData.iEntityIndex])
				ENDIF
			ENDIF
		BREAK
		
		CASE MODEL_SWAP_ENTITY_TYPE__MODEL_SWAP_PROP
			IF bGetEntityToSwapIn
				RETURN sActiveModelSwaps[iActiveModelSwap].oiModelSwapProp_SwapIn
			ELSE
				IF bGetOriginalEntity
					IF NETWORK_DOES_NETWORK_ID_EXIST(sEntityData.niEntityNetID)
						// Return the original networked object rather than the local clone
						RETURN NET_TO_OBJ(sEntityData.niEntityNetID)
					ENDIF
				ELSE
					RETURN sActiveModelSwaps[iActiveModelSwap].oiModelSwapProp_SwapOut
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN NULL
ENDFUNC

FUNC OBJECT_INDEX GET_MODEL_SWAP_ENTITY_TO_SWAP_OUT(INT iActiveModelSwap, BOOL bGetOriginalEntity = FALSE)
	RETURN GET_MODEL_SWAP_ENTITY(iActiveModelSwap, (sActiveModelSwaps[iActiveModelSwap].sModelSwapConfig.sEntityToSwapOut), FALSE, bGetOriginalEntity)
ENDFUNC

FUNC OBJECT_INDEX GET_MODEL_SWAP_ENTITY_TO_SWAP_IN(INT iActiveModelSwap)
	RETURN GET_MODEL_SWAP_ENTITY(iActiveModelSwap, (sActiveModelSwaps[iActiveModelSwap].sModelSwapConfig.sEntityToSwapIn), TRUE)
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Entity Self Destruct System
// ##### Description: Some entity types can process a self destruct sequence.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC CLEAN_UP_SELF_DESTRUCT_ENTITY(ENTITY_SELF_DESTRUCT_STRUCT &sSelfDeStruct)
	sSelfDeStruct.iBS = 0
	sSelfDeStruct.vPos = <<0.0, 0.0, 0.0>>
	RESET_NET_TIMER(sSelfDeStruct.tdTimer)
ENDPROC

FUNC BOOL SHOULD_ENTITY_PROCESS_SELF_DESTRUCT_SEQUENCE(SELF_DESTRUCT_STRUCT &sEntityGlobalSelfDestruct, ENTITY_SELF_DESTRUCT_STRUCT &sEntityLocalSelfDestruct, FLOAT fHealthPercentage)
	INT iTeam = MC_PlayerBD[iPartToUse].iTeam
	INT iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]
	
	IF iRule >= FMMC_MAX_RULES
		RETURN FALSE		
	ENDIF
	
	IF sEntityGlobalSelfDestruct.iRuleStart = -1
		RETURN FALSE
	ENDIF
	
	IF iRule < sEntityGlobalSelfDestruct.iRuleStart
		RETURN FALSE		
	ENDIF
		
	IF sEntityGlobalSelfDestruct.iRuleEnd != -1
	AND iRule >= sEntityGlobalSelfDestruct.iRuleEnd
		RETURN FALSE	
	ENDIF
	
	IF IS_BIT_SET(sEntityLocalSelfDestruct.iBS, ciEntitySelfDestruct_Finished)
		RETURN FALSE
	ENDIF
		
	IF IS_BIT_SET(sEntityLocalSelfDestruct.iBS, ciEntitySelfDestruct_ProcessFizzle)
		RETURN FALSE
	ENDIF
	
	IF sEntityGlobalSelfDestruct.iMustBeBelowHealthPercentage > 0
		IF fHealthPercentage >= sEntityGlobalSelfDestruct.iMustBeBelowHealthPercentage
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL SHOULD_ENTITY_PROCESS_FIZZLE_SELF_DESTRUCT_SEQUENCE(SELF_DESTRUCT_STRUCT &sEntityGlobalSelfDestruct, ENTITY_SELF_DESTRUCT_STRUCT &sEntityLocalSelfDestruct)
	
	IF IS_BIT_SET(sEntityLocalSelfDestruct.iBS, ciEntitySelfDestruct_Finished)
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(sEntityLocalSelfDestruct.iBS, ciEntitySelfDestruct_ProcessFizzle)
		RETURN TRUE
	ENDIF
	
	INT iTeam = MC_PlayerBD[iPartToUse].iTeam
	INT iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]
	
	IF iRule >= FMMC_MAX_RULES
		RETURN FALSE		
	ENDIF
	
	IF iRule <= sEntityGlobalSelfDestruct.iRuleStart
		RETURN FALSE
	ENDIF
	
	IF sEntityGlobalSelfDestruct.iRuleEnd != -1
	AND iRule <= sEntityGlobalSelfDestruct.iRuleEnd
		RETURN FALSE
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(sEntityLocalSelfDestruct.tdTimer)
	AND HAS_NET_TIMER_EXPIRED_READ_ONLY(sEntityLocalSelfDestruct.tdTimer, sEntityGlobalSelfDestruct.iDelayMiliseconds)
		RETURN FALSE
	ENDIF
	
	SET_BIT(sEntityLocalSelfDestruct.iBS, ciEntitySelfDestruct_ProcessFizzle)
	PRINTLN("[LM][SELF_DESTRUCT] - We should process a Fizzle")
	
	RETURN TRUE
ENDFUNC

// A Fizzle in this context will process what happens after the explosion, or if get past the rule and the explosion should no longer happen.
PROC PROCESS_ENTITY_FIZZLE_SELF_DESTRUCT_SEQUENCE(SELF_DESTRUCT_STRUCT &sEntityGlobalSelfDestruct, ENTITY_SELF_DESTRUCT_STRUCT &sEntityLocalSelfDestruct)
	UNUSED_PARAMETER(sEntityGlobalSelfDestruct)
	
	RESET_NET_TIMER(sEntityLocalSelfDestruct.tdTimer)
	CLEAR_BIT(sEntityLocalSelfDestruct.iBS, ciEntitySelfDestruct_ProcessFizzle)
	SET_BIT(sEntityLocalSelfDestruct.iBS, ciEntitySelfDestruct_Finished)
	
	PRINTLN("[LM][SELF_DESTRUCT] - We have now finished processing a fizzle.")
	
ENDPROC

FUNC VEHICLE_INDEX GET_ATTACHED_VEHICLE_INDEX_FOR_SELF_DESTRUCT(VEHICLE_INDEX &viVehToCheck)
	
	VEHICLE_INDEX viVehToReturn
	VEHICLE_INDEX viTrailer
	
	INT i
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles - 1

		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[i])
			PRINTLN("[LM][SELF_DESTRUCT][", i, "] - GET_ATTACHED_VEHICLE_INDEX_FOR_SELF_DESTRUCT - Net ID doesn't exist")
			RELOOP
		ENDIF
		
		VEHICLE_INDEX viVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i])
		
		IF GET_VEHICLE_TRAILER_VEHICLE(viVeh, viTrailer)
			PRINTLN("[LM][SELF_DESTRUCT][", i, "] - GET_ATTACHED_VEHICLE_INDEX_FOR_SELF_DESTRUCT - viVehToReturn: ", NATIVE_TO_INT(viVehToReturn), " - viTrailer: ", NATIVE_TO_INT(viVehToCheck))
			IF viTrailer = viVehToCheck
				//If the trailer is the self destructor then we need to return the truck
				viVehToReturn = viVeh
				BREAKLOOP
			ELIF viVeh = viVehToCheck
				//If the truck is the self destructor then we need to return the trailer
				viVehToReturn = viTrailer
				BREAKLOOP
			ENDIF
		ENDIF
	
	ENDFOR
	
	RETURN viVehToReturn

ENDFUNC

PROC PROCESS_ENTITY_SELF_DESTRUCT_SEQUENCE(ENTITY_INDEX eiEntity, SELF_DESTRUCT_STRUCT &sEntityGlobalSelfDestruct, ENTITY_SELF_DESTRUCT_STRUCT &sEntityLocalSelfDestruct)
				
	IF NOT HAS_NET_TIMER_STARTED(sEntityLocalSelfDestruct.tdTimer)
		START_NET_TIMER(sEntityLocalSelfDestruct.tdTimer)
		PRINTLN("[LM][SELF_DESTRUCT] - Starting the timer. Will explode after: ", sEntityGlobalSelfDestruct.iDelayMiliseconds, " miliseconds from now.)")
	ENDIF
	
	IF DOES_ENTITY_EXIST(eiEntity)
		sEntityLocalSelfDestruct.vPos = GET_ENTITY_COORDS(eiEntity, FALSE)
	ELIF IS_BIT_SET(sEntityGlobalSelfDestruct.iDestructBS, ciBS_SELF_DESTRUCT_EXPLODE_IF_DESTROYED)
		IF NOT IS_BIT_SET(sEntityLocalSelfDestruct.iBS, ciEntitySelfDestruct_ProcessExplosion)
			SET_BIT(sEntityLocalSelfDestruct.iBS, ciEntitySelfDestruct_ProcessExplosion)
			PRINTLN("[LM][SELF_DESTRUCT] - Entity is Dead. Option set to Explode Entity if it Dies.")
		ENDIF
	ELSE
		IF NOT IS_BIT_SET(sEntityLocalSelfDestruct.iBS, ciEntitySelfDestruct_Finished)
			SET_BIT(sEntityLocalSelfDestruct.iBS, ciEntitySelfDestruct_Finished)
			PRINTLN("[LM][SELF_DESTRUCT] - Entity is Dead. Marking as Finished..")
		ENDIF
	ENDIF
	
	IF HAS_NET_TIMER_EXPIRED_READ_ONLY(sEntityLocalSelfDestruct.tdTimer, sEntityGlobalSelfDestruct.iDelayMiliseconds)
		IF IS_BIT_SET(sEntityGlobalSelfDestruct.iDestructBS, ciBS_SELF_DESTRUCT_HAS_EXPLOSION)
			IF NOT IS_BIT_SET(sEntityLocalSelfDestruct.iBS, ciEntitySelfDestruct_ProcessExplosion)
				PRINTLN("[LM][SELF_DESTRUCT] - Timer is up. Option set to Mark for Explosion and Kill Entity.")
				SET_BIT(sEntityLocalSelfDestruct.iBS, ciEntitySelfDestruct_KillEntity)
				SET_BIT(sEntityLocalSelfDestruct.iBS, ciEntitySelfDestruct_ProcessExplosion)				
			ENDIF
		ELSE
			SET_BIT(sEntityLocalSelfDestruct.iBS, ciEntitySelfDestruct_KillEntity)
			PRINTLN("[LM][SELF_DESTRUCT] - Timer is up. Killing Entity.")
		ENDIF
	ENDIF
	
	/// NOTE: Ownership Issues.
	/// If this causes problems we will broadcast an event to ensure everyone has their local completion BS's set and the owner can make the explosion.
	
	IF IS_BIT_SET(sEntityLocalSelfDestruct.iBS, ciEntitySelfDestruct_KillEntity)
	AND NOT IS_ENTITY_DEAD(eiEntity)
	AND NETWORK_HAS_CONTROL_OF_ENTITY(eiEntity)
		PRINTLN("[LM][SELF_DESTRUCT] - Setting Health to 0.")
			
		IF NOT IS_ENTITY_DEAD(eiEntity)
		AND IS_ENTITY_A_PED(eiEntity)
		AND NETWORK_HAS_CONTROL_OF_ENTITY(eiEntity)
			PED_INDEX pedIndex = GET_PED_INDEX_FROM_ENTITY_INDEX(eiEntity)
			SET_PED_TO_RAGDOLL(pedIndex, 5000, 10000, TASK_RELAX)
		ENDIF
		
		SET_ENTITY_HEALTH(eiEntity, 0)
		
		IF IS_ENTITY_A_VEHICLE(eiEntity)
		AND NOT IS_BIT_SET(sEntityLocalSelfDestruct.iBS, ciEntitySelfDestruct_ProcessExplosion)
			PRINTLN("[LM][SELF_DESTRUCT] - Is a vehicle, Setting Engine and Body Health to 0.")
			VEHICLE_INDEX vehIndex = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(eiEntity)
			SET_VEHICLE_ENGINE_HEALTH(vehIndex, 0.0)
			SET_VEHICLE_BODY_HEALTH(vehIndex, 0.0)	
		ENDIF
		
		SET_BIT(sEntityLocalSelfDestruct.iBS, ciEntitySelfDestruct_Finished)
	ENDIF
	
	/// NOTE: If this gets much bigger then we want to functionlise explosion procedures for each entity type. It works well ennough like this but thinking about readability.	
	IF IS_BIT_SET(sEntityLocalSelfDestruct.iBS, ciEntitySelfDestruct_ProcessExplosion)
	
		IF DOES_ENTITY_EXIST(eiEntity)
		AND IS_ENTITY_A_VEHICLE(eiEntity)
			IF NETWORK_HAS_CONTROL_OF_ENTITY(eiEntity)
				PRINTLN("[LM][SELF_DESTRUCT] - I am the Owner and it's a vehicle. I will add the Explosion.")
				VEHICLE_INDEX vehIndex = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(eiEntity)
				
				IF IS_BIT_SET(sEntityGlobalSelfDestruct.iDestructBS, ciBS_SELF_DESTRUCT_EXPLODE_ATTACHED_VEHICLE)
					VEHICLE_INDEX viVehAttached = GET_ATTACHED_VEHICLE_INDEX_FOR_SELF_DESTRUCT(vehIndex)
					IF DOES_ENTITY_EXIST(viVehAttached)
					AND NETWORK_HAS_CONTROL_OF_ENTITY(viVehAttached)
						PRINTLN("[LM][SELF_DESTRUCT] - Vehicle is attached to another vehicle (probably a trailer etc.)")
						NETWORK_EXPLODE_VEHICLE(viVehAttached, TRUE)
					ENDIF
				ENDIF
				
				//Need to explode the vehicle after we've found if there's anything attached to it
				NETWORK_EXPLODE_VEHICLE(vehIndex, TRUE)
			ENDIF
		ELSE			
			IF bIsLocalPlayerHost
				PRINTLN("[LM][SELF_DESTRUCT] - I am the host. I will add the Explosion Setting.")
				ADD_EXPLOSION(sEntityLocalSelfDestruct.vPos, EXP_TAG_GRENADELAUNCHER, 1.0, TRUE, FALSE, 1.0, FALSE)
			ENDIF			
		ENDIF
		
		SET_BIT(sEntityLocalSelfDestruct.iBS, ciEntitySelfDestruct_Finished)
	ENDIF
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Fragging
// ##### Description: Various functions to support scripted fragging of entities
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC INT GET_MODEL_COMPONENTS_TO_DAMAGE_WHEN_FRAGGING(MODEL_NAMES mnModel)

	IF mnModel = INT_TO_ENUM(MODEL_NAMES, HASH("tr_Prop_TR_Break_Dev_01a"))
		RETURN 3
	ELIF mnModel = INT_TO_ENUM(MODEL_NAMES, HASH("prop_box_wood02a_pu"))
		RETURN 14
	ELIF mnModel = INT_TO_ENUM(MODEL_NAMES, HASH("sf_prop_sf_weed_lrg_01a"))
	OR mnModel = INT_TO_ENUM(MODEL_NAMES, HASH("sf_prop_sf_weed_med_01a"))
	OR mnModel = INT_TO_ENUM(MODEL_NAMES, HASH("sf_prop_sf_weed_01_small_01a"))
		RETURN 3
	ELIF mnModel = INT_TO_ENUM(MODEL_NAMES, HASH("sf_prop_sf_weed_bigbag_01a"))	
		RETURN 1
	ELIF mnModel = INT_TO_ENUM(MODEL_NAMES, HASH("sum_prop_ac_aircon_02a"))
		RETURN 19
	ENDIF
	
	RETURN 0
ENDFUNC

FUNC INT GET_MODEL_COMPONENTS_TO_BREAK_WHEN_FRAGGING(MODEL_NAMES mnModel)

	IF mnModel = INT_TO_ENUM(MODEL_NAMES, HASH("tr_Prop_TR_Break_Dev_01a"))
		RETURN 0
	ELIF mnModel = INT_TO_ENUM(MODEL_NAMES, HASH("prop_box_wood02a_pu"))
		RETURN 14
	ELIF mnModel = INT_TO_ENUM(MODEL_NAMES, HASH("sf_prop_sf_weed_lrg_01a"))
	OR mnModel = INT_TO_ENUM(MODEL_NAMES, HASH("sf_prop_sf_weed_med_01a"))
	OR mnModel = INT_TO_ENUM(MODEL_NAMES, HASH("sf_prop_sf_weed_01_small_01a"))
		RETURN 3
	ELIF mnModel = INT_TO_ENUM(MODEL_NAMES, HASH("sf_prop_sf_weed_bigbag_01a"))
		RETURN 1
	ELIF mnModel = INT_TO_ENUM(MODEL_NAMES, HASH("sum_prop_ac_aircon_02a"))
		RETURN 19
	ENDIF
	
	RETURN 0
ENDFUNC

PROC FRAG_ENTITY_COMPLETELY(OBJECT_INDEX oiObjectToFrag, BOOL bKillEntity = TRUE)
	IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(oiObjectToFrag)
		EXIT
	ENDIF
	
	IF NOT GET_IS_ENTITY_A_FRAG(oiObjectToFrag)
		EXIT
	ENDIF
	
	INT iLoopMax = 0
	MODEL_NAMES mnEntityModel = GET_ENTITY_MODEL(oiObjectToFrag)
	INT iComponentsToDamage = GET_MODEL_COMPONENTS_TO_DAMAGE_WHEN_FRAGGING(mnEntityModel)
	INT iComponentsToBreak = GET_MODEL_COMPONENTS_TO_BREAK_WHEN_FRAGGING(mnEntityModel)
	
	IF iComponentsToDamage = 0
	AND iComponentsToBreak = 0
		PRINTLN("[SCRIPT_OBJECT_", NETWORK_ENTITY_GET_OBJECT_ID(oiObjectToFrag), "] FRAG_ENTITY_COMPLETELY | Entity ", FMMC_GET_MODEL_NAME_FOR_DEBUG(mnEntityModel), " has no components specified to frag or break!")
		EXIT
	ENDIF
	
	PRINTLN("[SCRIPT_OBJECT_", NETWORK_ENTITY_GET_OBJECT_ID(oiObjectToFrag), "] FRAG_ENTITY_COMPLETELY | Fully fragging entity: ", FMMC_GET_MODEL_NAME_FOR_DEBUG(mnEntityModel))
	SET_DISABLE_FRAG_DAMAGE(oiObjectToFrag, FALSE)
	
	IF iComponentsToDamage > iComponentsToBreak
		iLoopMax = iComponentsToDamage
	ELSE
		iLoopMax = iComponentsToBreak
	ENDIF
	
	INT iComponent
	FOR iComponent = 0 TO iLoopMax - 1
		IF iComponentsToDamage > iComponent
			DAMAGE_OBJECT_FRAGMENT_CHILD(oiObjectToFrag, iComponent, -100)
		ENDIF
		
		IF iComponentsToBreak > iComponent
			BREAK_OBJECT_FRAGMENT_CHILD(oiObjectToFrag, iComponent, FALSE)
		ENDIF
	ENDFOR
	
	IF bKillEntity
	AND NOT IS_ENTITY_DEAD(oiObjectToFrag)
		PRINTLN("[SCRIPT_OBJECT_", NETWORK_ENTITY_GET_OBJECT_ID(oiObjectToFrag), "] FRAG_ENTITY_COMPLETELY | Killing entity: ", FMMC_GET_MODEL_NAME_FOR_DEBUG(mnEntityModel))
		SET_ENTITY_HEALTH(oiObjectToFrag, 0)
	ENDIF
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Entity Looping Audio
// ##### Description: Various functions to support having looping audio play from entities without having to reserve arrays for every placed entity
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC INT GET_ENTITY_LOOPING_AUDIO_INDEX_TO_USE(ENTITY_INDEX eiEntity)
	INT iLoopingAudioSlotToCheck
	INT iEmptySlot = -1
	
	FOR iLoopingAudioSlotToCheck = 0 TO ciEntityLoopingAudioIDPoolSize - 1
		IF sEntityLoopingAudioVars[iLoopingAudioSlotToCheck].iEntityLoopingAudioID = -1
		AND iEmptySlot = -1
			iEmptySlot = iLoopingAudioSlotToCheck
		ENDIF
		
		IF sEntityLoopingAudioVars[iLoopingAudioSlotToCheck].niEntityNetworkIndex = NETWORK_GET_NETWORK_ID_FROM_ENTITY(eiEntity)
			RETURN iLoopingAudioSlotToCheck
		ENDIF
	ENDFOR
	
	RETURN iEmptySlot
ENDFUNC

FUNC INT GET_ENTITY_LOOPING_AUDIO_INDEX_FOR_ENTITY(ENTITY_INDEX eiEntity)
	INT iLoopingAudioSlotToCheck
	FOR iLoopingAudioSlotToCheck = 0 TO ciEntityLoopingAudioIDPoolSize - 1
		IF sEntityLoopingAudioVars[iLoopingAudioSlotToCheck].niEntityNetworkIndex = NETWORK_GET_NETWORK_ID_FROM_ENTITY(eiEntity)
			RETURN iLoopingAudioSlotToCheck
		ENDIF
	ENDFOR
	
	RETURN -1
ENDFUNC

FUNC INT START_ENTITY_LOOPING_AUDIO(ENTITY_INDEX eiEntity, VECTOR vOffset, STRING sSoundName, STRING sSoundSet, BOOL bNetworked, BOOL bBailIfAlreadyPlaying = TRUE, BOOL bErrorIfNoSlotAvailable = TRUE)
	INT iLoopingAudioSlot = GET_ENTITY_LOOPING_AUDIO_INDEX_TO_USE(eiEntity)
	IF iLoopingAudioSlot = -1
		IF bErrorIfNoSlotAvailable
			ASSERTLN("[LoopingEntityAudio] START_ENTITY_LOOPING_AUDIO | No slots are free right now!")
		ELSE
			PRINTLN("[LoopingEntityAudio] START_ENTITY_LOOPING_AUDIO | No slots are free right now!")
		ENDIF
		
		RETURN -1
	ENDIF
	
	IF bBailIfAlreadyPlaying
		IF IS_SOUND_ID_VALID(sEntityLoopingAudioVars[iLoopingAudioSlot].iEntityLoopingAudioID)
		AND NOT HAS_SOUND_FINISHED(sEntityLoopingAudioVars[iLoopingAudioSlot].iEntityLoopingAudioID)
			RETURN iLoopingAudioSlot
		ENDIF
	ENDIF
	
	sEntityLoopingAudioVars[iLoopingAudioSlot].niEntityNetworkIndex = NETWORK_GET_NETWORK_ID_FROM_ENTITY(eiEntity)
	sEntityLoopingAudioVars[iLoopingAudioSlot].iEntityLoopingAudioID = GET_SOUND_ID()
	
	IF IS_VECTOR_ZERO(vOffset)
		PRINTLN("[LoopingEntityAudio][LoopingEntityAudioSlot ", iLoopingAudioSlot, "] START_ENTITY_LOOPING_AUDIO | Playing sound on entity! ", sSoundName, " / ", sSoundSet)
		PLAY_SOUND_FROM_ENTITY(sEntityLoopingAudioVars[iLoopingAudioSlot].iEntityLoopingAudioID, sSoundName, eiEntity, sSoundSet, bNetworked)
	ELSE
		VECTOR vOffsetPosition = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(eiEntity, vOffset)
		PRINTLN("[LoopingEntityAudio][LoopingEntityAudioSlot ", iLoopingAudioSlot, "] START_ENTITY_LOOPING_AUDIO | Playing sound on entity with an offset! ", sSoundName, " / ", sSoundSet, " || ", vOffsetPosition)
		PLAY_SOUND_FROM_COORD(sEntityLoopingAudioVars[iLoopingAudioSlot].iEntityLoopingAudioID, sSoundName, vOffsetPosition, sSoundSet, bNetworked)
	ENDIF
	
	RETURN iLoopingAudioSlot
ENDFUNC

PROC CLEANUP_ENTITY_LOOPING_AUDIO(INT iLoopingAudioSlot)
	PRINTLN("[LoopingEntityAudio][LoopingEntityAudioSlot ", iLoopingAudioSlot, "] CLEANUP_ENTITY_LOOPING_AUDIO | Stopping looping audio!")
	STOP_SOUND(sEntityLoopingAudioVars[iLoopingAudioSlot].iEntityLoopingAudioID)
	RELEASE_SOUND_ID(sEntityLoopingAudioVars[iLoopingAudioSlot].iEntityLoopingAudioID)
	sEntityLoopingAudioVars[iLoopingAudioSlot].iEntityLoopingAudioID = -1
ENDPROC

PROC CLEANUP_ALL_ENTITY_LOOPING_AUDIOS()
	INT iLoopingAudioSlotToCheck
	FOR iLoopingAudioSlotToCheck = 0 TO ciEntityLoopingAudioIDPoolSize - 1
		IF sEntityLoopingAudioVars[iLoopingAudioSlotToCheck].iEntityLoopingAudioID = -1
			RELOOP
		ENDIF
		
		CLEANUP_ENTITY_LOOPING_AUDIO(iLoopingAudioSlotToCheck)
	ENDFOR
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Scripted Flammable Entity System
// ##### Description: Various functions to support the scripted entity ignition functionality
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC STRING GET_SCRIPTED_FLAMMABLE_ENTITY_PTFX_ASSET(MODEL_NAMES mnEntityModel)
	
	IF mnEntityModel = INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_Weed_Lrg_01a"))
	OR mnEntityModel = INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_Weed_Med_01a"))
	OR mnEntityModel = INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_Weed_01_Small_01a"))
	OR mnEntityModel = INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_Weed_BigBag_01a"))
		RETURN "scr_sec"
	ENDIF
	
	IF mnEntityModel = INT_TO_ENUM(MODEL_NAMES, HASH("sf_prop_SF_tanker_crash_01a"))
		RETURN "scr_xs_pits"
	ENDIF
	
	RETURN ""
ENDFUNC

FUNC STRING GET_SCRIPTED_FLAMMABLE_ENTITY_PTFX_NAME(MODEL_NAMES mnEntityModel)
	
	IF mnEntityModel = INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_Weed_Lrg_01a"))
		RETURN "scr_sec_weed_burn_smoke_large"
	ELIF mnEntityModel = INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_Weed_Med_01a"))
		RETURN "scr_sec_weed_burn_smoke_medium"
	ELIF mnEntityModel = INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_Weed_01_Small_01a"))
		RETURN "scr_sec_weed_burn_smoke_small"
	ELIF mnEntityModel = INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_Weed_BigBag_01a"))
		RETURN "scr_sec_weed_burn_smoke_bag"
		
	ELIF mnEntityModel = INT_TO_ENUM(MODEL_NAMES, HASH("sf_prop_SF_tanker_crash_01a"))
		RETURN "scr_xs_fire_pit"
	ENDIF
	
	RETURN "INVALID"
ENDFUNC

FUNC STRING GET_SCRIPTED_FLAMMABLE_ENTITY_PTFX_EVOLUTION_NAME(MODEL_NAMES mnEntityModel, INT iEvolutionIndex = 0)
	
	IF mnEntityModel = INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_Weed_Lrg_01a"))
	OR mnEntityModel = INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_Weed_Med_01a"))
	OR mnEntityModel = INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_Weed_01_Small_01a"))
	OR mnEntityModel = INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_Weed_BigBag_01a"))
		SWITCH iEvolutionIndex
			CASE 0 RETURN "smoke"
			CASE 1 RETURN "flame"
		ENDSWITCH
		
	ELIF mnEntityModel = INT_TO_ENUM(MODEL_NAMES, HASH("sf_prop_SF_tanker_crash_01a"))
		RETURN "fire"
	ENDIF
	
	RETURN ""
ENDFUNC

FUNC BOOL IS_SCRIPTED_FLAMMABLE_ENTITY_DAMAGE_SCRIPT_CONTROLLED(MODEL_NAMES mnEntityModel)
	IF mnEntityModel = INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_Weed_Lrg_01a"))
	OR mnEntityModel = INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_Weed_Med_01a"))
	OR mnEntityModel = INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_Weed_01_Small_01a"))
	OR mnEntityModel = INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_Weed_BigBag_01a"))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC VECTOR GET_SCRIPTED_FLAMMABLE_ENTITY_PTFX_OFFSET(MODEL_NAMES mnEntityModel)
	IF mnEntityModel = INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_Weed_Lrg_01a"))
		RETURN <<0.0, 0.0, 0.5>>
		
	ELIF mnEntityModel = INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_Weed_Med_01a"))
		RETURN <<0.0, 0.0, 0.45>>
		
	ELIF mnEntityModel = INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_Weed_01_Small_01a"))
		RETURN <<0.0, 0.0, 0.55>>
		
	ELIF mnEntityModel = INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_Weed_BigBag_01a"))
		RETURN <<0.0, 0.0, 0.05>>
		
	ELIF mnEntityModel = INT_TO_ENUM(MODEL_NAMES, HASH("sf_prop_SF_tanker_crash_01a"))
		RETURN <<1.360, -4.730, 0.0>>
		
	ENDIF
	
	RETURN <<0,0,0>>
ENDFUNC

FUNC STRING GET_SCRIPTED_FLAMMABLE_ENTITY_PTFX_SOUND(MODEL_NAMES mnEntityModel)
	IF mnEntityModel = INT_TO_ENUM(MODEL_NAMES, HASH("sf_prop_SF_tanker_crash_01a"))
		RETURN "Fire_Loop"
		
	ENDIF
	
	RETURN ""
ENDFUNC

FUNC STRING GET_SCRIPTED_FLAMMABLE_ENTITY_PTFX_SOUNDSET(MODEL_NAMES mnEntityModel)
	IF mnEntityModel = INT_TO_ENUM(MODEL_NAMES, HASH("sf_prop_SF_tanker_crash_01a"))
		RETURN "DLC_Security_Data_Leak_5_Sounds"
		
	ENDIF
	
	RETURN ""
ENDFUNC

FUNC BOOL SHOULD_SCRIPTED_FLAMMABLE_ENTITY_EXPLODE_ON_IGNITION(MODEL_NAMES mnEntityModel)
	IF mnEntityModel = INT_TO_ENUM(MODEL_NAMES, HASH("sf_prop_SF_tanker_crash_01a"))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_SCRIPTED_FLAMMABLE_ENTITY_FIRE_END_ON_DEATH(MODEL_NAMES mnEntityModel)
	IF mnEntityModel = INT_TO_ENUM(MODEL_NAMES, HASH("sf_prop_SF_tanker_crash_01a"))
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_SCRIPTED_FLAMMABLE_ENTITY_PTFX_CONTINUE_ON_DEATH(MODEL_NAMES mnEntityModel)
	IF mnEntityModel = INT_TO_ENUM(MODEL_NAMES, HASH("sf_prop_SF_tanker_crash_01a"))
	OR mnEntityModel = INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_Weed_Lrg_01a"))
	OR mnEntityModel = INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_Weed_Med_01a"))
	OR mnEntityModel = INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_Weed_01_Small_01a"))
	OR mnEntityModel = INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_Weed_BigBag_01a"))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC VECTOR GET_FLAMMABLE_OBJECT_PTFX_POSITION(ENTITY_INDEX eiEntity, MODEL_NAMES mnEntityModel)
	VECTOR vLocalOffset = GET_SCRIPTED_FLAMMABLE_ENTITY_PTFX_OFFSET(mnEntityModel)
	RETURN GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(eiEntity, vLocalOffset)
ENDFUNC

FUNC FLOAT GET_SCRIPTED_FLAMMABLE_ENTITY_PTFX_SCALE(MODEL_NAMES mnEntityModel)
	IF mnEntityModel = INT_TO_ENUM(MODEL_NAMES, HASH("sf_prop_SF_tanker_crash_01a"))
		RETURN 0.65
	ENDIF
	
	IF mnEntityModel = INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_Weed_Lrg_01a"))
	OR mnEntityModel = INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_Weed_Med_01a"))
	OR mnEntityModel = INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_Weed_01_Small_01a"))
	OR mnEntityModel = INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_Weed_BigBag_01a"))
		RETURN 1.25
	ENDIF
	
	RETURN 1.0
ENDFUNC

FUNC FLOAT GET_SCRIPTED_FLAMMABLE_ENTITY_DAMAGE_DISTANCE(MODEL_NAMES mnEntityModel)
	IF mnEntityModel = INT_TO_ENUM(MODEL_NAMES, HASH("sf_prop_SF_tanker_crash_01a"))
		RETURN 2.0
	ENDIF
	
	RETURN 2.0
ENDFUNC

FUNC FLOAT GET_SCRIPTED_FLAMMABLE_ENTITY_RAGDOLL_DISTANCE(MODEL_NAMES mnEntityModel)
	IF mnEntityModel = INT_TO_ENUM(MODEL_NAMES, HASH("sf_prop_SF_tanker_crash_01a"))
		RETURN 1.75
	ENDIF
	
	RETURN 0.0
ENDFUNC

FUNC FLOAT GET_SCRIPTED_FLAMMABLE_ENTITY_BURN_DAMAGE_MULTIPLIER(MODEL_NAMES mnEntityModel)
	IF mnEntityModel = INT_TO_ENUM(MODEL_NAMES, HASH("sf_prop_SF_tanker_crash_01a"))
		RETURN 0.0
	ENDIF
	
	IF mnEntityModel = INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_Weed_Lrg_01a"))
		RETURN 9750.0
	ELIF mnEntityModel = INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_Weed_Med_01a"))
		RETURN 7350.0
	ELIF mnEntityModel = INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_Weed_01_Small_01a"))
		RETURN 6150.0
	ELIF mnEntityModel = INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_Weed_BigBag_01a"))
		RETURN 8100.0
	ENDIF
	
	RETURN 9500.0
ENDFUNC

FUNC FLOAT GET_SCRIPTED_FLAMMABLE_ENTITY_CREATE_PTFX_FIRE_SCALAR(MODEL_NAMES mnEntityModel)
	IF mnEntityModel = INT_TO_ENUM(MODEL_NAMES, HASH("sf_prop_SF_tanker_crash_01a"))
		RETURN 0.05
	ENDIF
	
	RETURN 0.025
ENDFUNC

FUNC FLOAT GET_SCRIPTED_FLAMMABLE_ENTITY_SHOW_FIRE_PTFX_FIRE_SCALAR(MODEL_NAMES mnEntityModel)
	IF mnEntityModel = INT_TO_ENUM(MODEL_NAMES, HASH("sf_prop_SF_tanker_crash_01a"))
		RETURN 0.01
	ENDIF
	
	RETURN 0.275
ENDFUNC

FUNC FLOAT GET_SCRIPTED_FLAMMABLE_ENTITY_KEEP_FIRE_PTFX_TINY_FIRE_SCALAR(MODEL_NAMES mnEntityModel)
	IF mnEntityModel = INT_TO_ENUM(MODEL_NAMES, HASH("sf_prop_SF_tanker_crash_01a"))
		RETURN 0.00
	ENDIF
	
	RETURN 0.0
ENDFUNC

FUNC INT GET_SCRIPTED_FLAMMABLE_ENTITY_PLAYER_PROXIMITY_DAMAGE(MODEL_NAMES mnEntityModel)
	IF mnEntityModel = INT_TO_ENUM(MODEL_NAMES, HASH("sf_prop_SF_tanker_crash_01a"))
		RETURN 2
	ENDIF
	
	RETURN 25
ENDFUNC

FUNC INT GET_SCRIPTED_FLAMMABLE_ENTITY_PLAYER_RAGDOLL_DAMAGE(MODEL_NAMES mnEntityModel)
	IF mnEntityModel = INT_TO_ENUM(MODEL_NAMES, HASH("sf_prop_SF_tanker_crash_01a"))
		RETURN 18
	ENDIF
	
	RETURN 5
ENDFUNC

FUNC FLOAT GET_FLAMMABLE_OBJECT_FLAME_SCALAR(ENTITY_INDEX eiEntity, MODEL_NAMES mnEntityModel)
	IF mnEntityModel = INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_Weed_Lrg_01a"))
	OR mnEntityModel = INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_Weed_Med_01a"))
	OR mnEntityModel = INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_Weed_01_Small_01a"))
	OR mnEntityModel = INT_TO_ENUM(MODEL_NAMES, HASH("sf_Prop_SF_Weed_BigBag_01a"))
		// Gradually scale the flames with damage done
		FLOAT fBaseFlameScalar = 1.0 - (TO_FLOAT(GET_ENTITY_HEALTH(eiEntity)) / TO_FLOAT(GET_ENTITY_MAX_HEALTH(eiEntity)))
		fBaseFlameScalar *= 1.15 // Multiply this value so we get a big flame sooner rather than just before death
		RETURN fBaseFlameScalar
	ENDIF
	
	// Ignite immediately from one or two hits
	IF HAS_ENTITY_BEEN_DAMAGED_BY_ANY_PED(eiEntity)
		RETURN 1.0
	ELSE
		RETURN 0.0
	ENDIF
ENDFUNC

FUNC BOOL SHOULD_SCRIPTED_FLAMMABLE_ENTITY_DAMAGE_PLAYER_NOW(ENTITY_INDEX eiEntity, MODEL_NAMES mnEntityModel, FLOAT fFlameScalar, FLOAT& fPlayerDistance)
	
	IF fFlameScalar < 0.4
		// Fire isn't large enough yet
		RETURN FALSE
	ENDIF
	
	IF SHOULD_SCRIPTED_FLAMMABLE_ENTITY_FIRE_END_ON_DEATH(mnEntityModel)
		IF NOT IS_ENTITY_ALIVE(eiEntity)
			// The entity is no longer visibly on fire, so shouldn't set the player on fire
			RETURN FALSE
		ENDIF
	ENDIF
	
	VECTOR vCheckCoords = GET_ENTITY_COORDS(LocalPlayerPed)
	VECTOR vFireCoords = GET_FLAMMABLE_OBJECT_PTFX_POSITION(eiEntity, mnEntityModel)
	
	IF ABSF(vCheckCoords.z - vFireCoords.z) < 8.0
		vCheckCoords.z = vFireCoords.z
	ENDIF
	
	fPlayerDistance = VDIST2(vCheckCoords, vFireCoords)
	IF fPlayerDistance <= POW(GET_SCRIPTED_FLAMMABLE_ENTITY_DAMAGE_DISTANCE(mnEntityModel), 2.0)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_SCRIPTED_FLAMMABLE_ENTITY(ENTITY_INDEX eiEntity, MODEL_NAMES mnEntityModel, PTFX_ID& ptPTFX)
	
	IF NOT IS_THIS_MODEL_A_SCRIPTED_FLAMMABLE_ENTITY_MODEL(mnEntityModel)
		EXIT
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(eiEntity)
	OR (NOT SHOULD_SCRIPTED_FLAMMABLE_ENTITY_PTFX_CONTINUE_ON_DEATH(mnEntityModel) AND NOT IS_ENTITY_ALIVE(eiEntity))
		IF DOES_PARTICLE_FX_LOOPED_EXIST(ptPTFX)
			STOP_PARTICLE_FX_LOOPED(ptPTFX, TRUE)
			PRINTLN("[FlammableObject][SCRIPT_OBJECT_", NETWORK_ENTITY_GET_OBJECT_ID(eiEntity), "] PROCESS_SCRIPTED_FLAMMABLE_ENTITY | Stopping PTFX for Object that no longer exists/died/is broken!")
		ENDIF
		
		EXIT
	ENDIF
	
	FLOAT fFlameScalar = CLAMP(GET_FLAMMABLE_OBJECT_FLAME_SCALAR(eiEntity, mnEntityModel), 0.0, 1.0)
	
	IF fFlameScalar >= GET_SCRIPTED_FLAMMABLE_ENTITY_CREATE_PTFX_FIRE_SCALAR(mnEntityModel)
		IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(ptPTFX)
			PRINTLN("[FlammableObject][SCRIPT_OBJECT_", NETWORK_ENTITY_GET_OBJECT_ID(eiEntity), "] PROCESS_SCRIPTED_FLAMMABLE_ENTITY | Creating PTFX! ", GET_SCRIPTED_FLAMMABLE_ENTITY_PTFX_ASSET(mnEntityModel), " / ", GET_SCRIPTED_FLAMMABLE_ENTITY_PTFX_NAME(mnEntityModel), " / Scale: ", GET_SCRIPTED_FLAMMABLE_ENTITY_PTFX_SCALE(mnEntityModel))
			USE_PARTICLE_FX_ASSET(GET_SCRIPTED_FLAMMABLE_ENTITY_PTFX_ASSET(mnEntityModel))
			ptPTFX = START_PARTICLE_FX_LOOPED_ON_ENTITY(GET_SCRIPTED_FLAMMABLE_ENTITY_PTFX_NAME(mnEntityModel), eiEntity, GET_SCRIPTED_FLAMMABLE_ENTITY_PTFX_OFFSET(mnEntityModel), (<<0,0,0>>), GET_SCRIPTED_FLAMMABLE_ENTITY_PTFX_SCALE(mnEntityModel))
			
			IF NOT IS_STRING_NULL_OR_EMPTY(GET_SCRIPTED_FLAMMABLE_ENTITY_PTFX_SOUND(mnEntityModel))
				PRINTLN("[FlammableObject][SCRIPT_OBJECT_", NETWORK_ENTITY_GET_OBJECT_ID(eiEntity), "] PROCESS_SCRIPTED_FLAMMABLE_ENTITY | Starting burn audio!")
				START_ENTITY_LOOPING_AUDIO(eiEntity, GET_SCRIPTED_FLAMMABLE_ENTITY_PTFX_OFFSET(mnEntityModel), GET_SCRIPTED_FLAMMABLE_ENTITY_PTFX_SOUND(mnEntityModel), GET_SCRIPTED_FLAMMABLE_ENTITY_PTFX_SOUNDSET(mnEntityModel), FALSE, TRUE, FALSE)
			ENDIF
			
			IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(ptPTFX)
				PRINTLN("[FlammableObject][SCRIPT_OBJECT_", NETWORK_ENTITY_GET_OBJECT_ID(eiEntity), "] PROCESS_SCRIPTED_FLAMMABLE_ENTITY | PTFX creation failed!")
				EXIT
			ENDIF
			
			IF SHOULD_SCRIPTED_FLAMMABLE_ENTITY_EXPLODE_ON_IGNITION(mnEntityModel)
				ADD_EXPLOSION(GET_FLAMMABLE_OBJECT_PTFX_POSITION(eiEntity, mnEntityModel), EXP_TAG_MOLOTOV, 10.0)
			ENDIF
		ENDIF
		
		// Smoke
		IF NOT IS_STRING_NULL_OR_EMPTY(GET_SCRIPTED_FLAMMABLE_ENTITY_PTFX_EVOLUTION_NAME(mnEntityModel))
			SET_PARTICLE_FX_LOOPED_EVOLUTION(ptPTFX, GET_SCRIPTED_FLAMMABLE_ENTITY_PTFX_EVOLUTION_NAME(mnEntityModel), fFlameScalar, TRUE)
			PRINTLN("[FlammableObject_SPAM][SCRIPT_OBJECT_", NETWORK_ENTITY_GET_OBJECT_ID(eiEntity), "] PROCESS_SCRIPTED_FLAMMABLE_ENTITY | Setting evolution 0 to ", fFlameScalar)
		ENDIF
		
		// Fire
		IF NOT IS_STRING_NULL_OR_EMPTY(GET_SCRIPTED_FLAMMABLE_ENTITY_PTFX_EVOLUTION_NAME(mnEntityModel, 1))
			FLOAT fEvolutionValueToUse = CLAMP(fFlameScalar * 1.25, 0.0, 1.0)
			
			IF NOT IS_ENTITY_ALIVE(eiEntity)
			AND SHOULD_SCRIPTED_FLAMMABLE_ENTITY_FIRE_END_ON_DEATH(mnEntityModel)
				fEvolutionValueToUse = 0.0
			ENDIF
			
			IF fFlameScalar < GET_SCRIPTED_FLAMMABLE_ENTITY_SHOW_FIRE_PTFX_FIRE_SCALAR(mnEntityModel)
				fEvolutionValueToUse = 0.0 // Have no flame
			ELIF fFlameScalar < GET_SCRIPTED_FLAMMABLE_ENTITY_KEEP_FIRE_PTFX_TINY_FIRE_SCALAR(mnEntityModel)
				fEvolutionValueToUse = 0.05 // Have a tiny flame
			ENDIF
			
			SET_PARTICLE_FX_LOOPED_EVOLUTION(ptPTFX, GET_SCRIPTED_FLAMMABLE_ENTITY_PTFX_EVOLUTION_NAME(mnEntityModel, 1), fEvolutionValueToUse, TRUE)
			PRINTLN("[FlammableObject_SPAM][SCRIPT_OBJECT_", NETWORK_ENTITY_GET_OBJECT_ID(eiEntity), "] PROCESS_SCRIPTED_FLAMMABLE_ENTITY | Setting evolution 1 to ", fEvolutionValueToUse)
		ENDIF
		
		IF NETWORK_HAS_CONTROL_OF_ENTITY(eiEntity)
			// Damaging the Burning Entity
			IF GET_SCRIPTED_FLAMMABLE_ENTITY_BURN_DAMAGE_MULTIPLIER(mnEntityModel) > 0.05
			AND IS_ENTITY_ALIVE(eiEntity)
				INT iDamageToDeal = CEIL(CLAMP(fFlameScalar, 0.5, 1.0) * GET_SCRIPTED_FLAMMABLE_ENTITY_BURN_DAMAGE_MULTIPLIER(mnEntityModel) * GET_FRAME_TIME())
				
				INT iNewHealth = GET_ENTITY_HEALTH(eiEntity) - iDamageToDeal
				IF iNewHealth <= 0
					IF GET_MODEL_COMPONENTS_TO_DAMAGE_WHEN_FRAGGING(mnEntityModel) > 0
					OR GET_MODEL_COMPONENTS_TO_BREAK_WHEN_FRAGGING(mnEntityModel) > 0
						IF IS_ENTITY_AN_OBJECT(eiEntity)
							OBJECT_INDEX oiObject = GET_OBJECT_INDEX_FROM_ENTITY_INDEX(eiEntity)
							PRINTLN("[FlammableObject][SCRIPT_OBJECT_", NETWORK_ENTITY_GET_OBJECT_ID(eiEntity), "] PROCESS_SCRIPTED_FLAMMABLE_ENTITY | Fragging burning object now!")
							FRAG_ENTITY_COMPLETELY(oiObject, FALSE)
						ENDIF
					ENDIF
					
					iNewHealth = 0
				ENDIF
				
				IF iNewHealth < 50
					SET_ENTITY_HEALTH(eiEntity, 50, NULL)
					SET_ENTITY_PROOFS(eiEntity, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE)
					ADD_OWNED_EXPLOSION(LocalPlayerPed, GET_ENTITY_COORDS(eiEntity, FALSE), EXP_TAG_GRENADE, 0.05, FALSE, TRUE, 0.0)
					PRINTLN("[FlammableObject_SPAM][SCRIPT_OBJECT_", NETWORK_ENTITY_GET_OBJECT_ID(eiEntity), "] PROCESS_SCRIPTED_FLAMMABLE_ENTITY | Killing entity with explosion to trigger a proper damage event")
				ELSE
					SET_ENTITY_HEALTH(eiEntity, iNewHealth, NULL)
					PRINTLN("[FlammableObject_SPAM][SCRIPT_OBJECT_", NETWORK_ENTITY_GET_OBJECT_ID(eiEntity), "] PROCESS_SCRIPTED_FLAMMABLE_ENTITY | Setting health to ", iNewHealth, " after doing ", iDamageToDeal, " damage")
				ENDIF
			ENDIF
		ENDIF
		
		FLOAT fPlayerDistance = 999.9
		IF SHOULD_SCRIPTED_FLAMMABLE_ENTITY_DAMAGE_PLAYER_NOW(eiEntity, mnEntityModel, fFlameScalar, fPlayerDistance)
			IF NOT IS_FAKE_MULTIPLAYER_MODE_SET(TRUE)
				IF MC_RUN_NET_TIMER_WITH_TIMESTAMP(iBurningEntityPlayerDamageTimestamp, iBurningEntityPlayerDamageCooldownLength)
					INT iNewPlayerHealth = GET_ENTITY_HEALTH(LocalPlayerPed) - GET_SCRIPTED_FLAMMABLE_ENTITY_PLAYER_PROXIMITY_DAMAGE(mnEntityModel)
					iNewPlayerHealth = CLAMP_INT(iNewPlayerHealth, 0, 9999)
					SET_ENTITY_HEALTH(LocalPlayerPed, iNewPlayerHealth, NULL)
					MC_STOP_NET_TIMER_WITH_TIMESTAMP(iBurningEntityPlayerDamageTimestamp)
					PRINTLN("[FlammableObject][SCRIPT_OBJECT_", NETWORK_ENTITY_GET_OBJECT_ID(eiEntity), "] PROCESS_SCRIPTED_FLAMMABLE_ENTITY | Damaging the player now due to being too close to the fire! iNewPlayerHealth: ", iNewPlayerHealth)
				ENDIF
			ENDIF
			
			IF fFlameScalar > 0.8
			AND fPlayerDistance <= GET_SCRIPTED_FLAMMABLE_ENTITY_RAGDOLL_DISTANCE(mnEntityModel)
				IF NOT IS_PED_INJURED(LocalPlayerPed)
				AND NOT IS_PED_RAGDOLL(LocalPlayerPed)
					SET_PED_TO_RAGDOLL(LocalPlayerPed, 3000, 3000, TASK_RELAX)
					SET_PED_RAGDOLL_FORCE_FALL(LocalPlayerPed)
					
					INT iNewPlayerHealth = GET_ENTITY_HEALTH(LocalPlayerPed) - GET_SCRIPTED_FLAMMABLE_ENTITY_PLAYER_RAGDOLL_DAMAGE(mnEntityModel)
					iNewPlayerHealth = CLAMP_INT(iNewPlayerHealth, 0, 9999)
					SET_ENTITY_HEALTH(LocalPlayerPed, iNewPlayerHealth, NULL)
					PRINTLN("[FlammableObject][SCRIPT_OBJECT_", NETWORK_ENTITY_GET_OBJECT_ID(eiEntity), "] PROCESS_SCRIPTED_FLAMMABLE_ENTITY | Ragdolling player & doing some damage now!! iNewPlayerHealth: ", iNewPlayerHealth)
				ENDIF
			ENDIF
		ENDIF
		
	ELSE
		IF DOES_PARTICLE_FX_LOOPED_EXIST(ptPTFX)
			STOP_PARTICLE_FX_LOOPED(ptPTFX, TRUE)
			PRINTLN("[FlammableObject][SCRIPT_OBJECT_", NETWORK_ENTITY_GET_OBJECT_ID(eiEntity), "] PROCESS_SCRIPTED_FLAMMABLE_ENTITY | Stopping PTFX for Object due to fFlameScalar")
		ENDIF
		
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF bScriptedFlammableEntityDebug
		TEXT_LABEL_63 tlVisualDebug = "fFlameScalar: "
		tlVisualDebug += FLOAT_TO_STRING(fFlameScalar)
		DRAW_DEBUG_TEXT(tlVisualDebug, GET_ENTITY_COORDS(eiEntity), 255, 255, 255, 255)
	ENDIF
	#ENDIF
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Entities - General
// ##### Description: Various helper functions & wrappers to do with mission entities
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    performs a VDIST2 check before calling IS_ENTITY_TOUCHING_ENTITY due to the latter being very expensive for every frame use.
/// PARAMS:
///    fProcessBelowDist - Distance at which we will start calling IS_ENTITY_TOUCHING_ENTITY. Testing Ped / Ped, Obj / Obj, or Ped / Obj can use a low fProcessBelowDist like 3.0 or 5.0. Vehicles require a higher one, like 25.0, as we are never use how large the vehicle can be. 
///    0.0 can be used to ignore the distance check for situations when we know we will always be close enough, such as "ped is entering vehicle".
FUNC BOOL MC_IS_ENTITY_TOUCHING_ENTITY(ENTITY_INDEX eiOne, ENTITY_INDEX eiTwo, FLOAT fProcessBelowDist = 0.0)

	IF fProcessBelowDist > 0.0
		IF VDIST2(GET_ENTITY_COORDS(eiOne), GET_ENTITY_COORDS(eiTwo)) > POW(fProcessBelowDist, 2.0)
			RETURN FALSE
		ENDIF
	ENDIF	
	
	RETURN IS_ENTITY_TOUCHING_ENTITY(eiOne, eiTwo)
	
ENDFUNC

FUNC FLOAT GET_PED_HEALTH_PERCENTAGE(PED_INDEX pedIndex)
	FLOAT fCurrentHealth = TO_FLOAT(GET_ENTITY_HEALTH(pedIndex)) - 100
	FLOAT fMaxHealth = TO_FLOAT(GET_ENTITY_MAX_HEALTH(pedIndex)) - 100
	
	RETURN (fCurrentHealth / fMaxHealth) * 100
ENDFUNC

FUNC FLOAT GET_ENTITY_HEALTH_PERCENTAGE(ENTITY_INDEX eiEntity)
	RETURN (TO_FLOAT(GET_ENTITY_HEALTH(eiEntity)) / TO_FLOAT(GET_ENTITY_MAX_HEALTH(eiEntity))) * 100
ENDFUNC

FUNC BOOL ARE_ANY_ENTITY_SPAWN_POS_SEARCHES_RUNNING(INT iExcludeType = -1)
		
	IF iSearchingForGangChase != -1
	AND iExcludeType != ciEntitySpawnSearchType_GangChase
		RETURN TRUE
	ENDIF
	
	IF MC_serverBD.isearchingforPed != -1
	AND iExcludeType != ciEntitySpawnSearchType_Peds
		RETURN TRUE
	ENDIF
	
	IF MC_serverBD.isearchingforObj != -1
	AND iExcludeType != ciEntitySpawnSearchType_Objs
		RETURN TRUE
	ENDIF
	
	IF MC_serverBD.isearchingforVeh != -1
	AND iExcludeType != ciEntitySpawnSearchType_Vehicles
		RETURN TRUE
	ENDIF
	
	IF sLocalPlayerAbilities.sVehicleRepairData.iLastFrameSearchingForVehicleRepair  > (GET_FRAME_COUNT() - 10)
	AND iExcludeType != ciEntitySpawnSearchType_VehicleRepair
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL CAN_RUN_ENTITY_SPAWN_POS_SEARCH(INT iEntitySearchType, INT iEntityIndex)
	
	IF ARE_ANY_ENTITY_SPAWN_POS_SEARCHES_RUNNING(iEntitySearchType)
		RETURN FALSE
	ENDIF
	
	SWITCH iEntitySearchType
		CASE ciEntitySpawnSearchType_Peds
			IF MC_serverBD.isearchingforPed != iEntityIndex
			AND MC_serverBD.isearchingforPed != -1
				RETURN FALSE
			ENDIF
		BREAK
		CASE ciEntitySpawnSearchType_Vehicles
			IF MC_serverBD.isearchingforVeh != iEntityIndex
			AND MC_serverBD.isearchingforVeh != -1
				RETURN FALSE
			ENDIF
		BREAK
		CASE ciEntitySpawnSearchType_Objs
			IF MC_serverBD.isearchingforObj != iEntityIndex
			AND MC_serverBD.isearchingforObj != -1
				RETURN FALSE
			ENDIF
		BREAK
		CASE ciEntitySpawnSearchType_GangChase
			IF iSearchingForGangChase != iEntityIndex
			AND iSearchingForGangChase != -1
				RETURN FALSE
			ENDIF
		BREAK
		CASE ciEntitySpawnSearchType_VehicleRepair
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN TRUE
	
ENDFUNC

/// PURPOSE:
///    Returns true if the prop needs to be processed each frame due to options set on it.
/// PARAMS:
///    iProp - The prop to check
FUNC BOOL DOES_PROP_NEED_PROCESSED_EVERY_FRAME(INT iProp)

	IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iRespawnOnRuleChangeBS != 0
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropBitset, ciFMMC_PROP_Requires_Alpha_Flash)
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropBitSet2, ciFMMC_PROP2_HidePropInCutscene)
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropBitSet2,  ciFMMC_PROP2_BlipAsSafeProp)
		RETURN TRUE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iForceCleanupPropAfterTime > 0
		RETURN TRUE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].mn =  IND_PROP_FIREWORK_01
	AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropBitSet2, ciFMMC_PROP2_UsePlacedFireworkZone)
		RETURN TRUE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iColourChange > -1
		RETURN TRUE
	ENDIF
	
	IF SHOULD_PROP_REQUIRE_PTFX(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].mn)
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropBitset2, ciFMMC_PROP2_CAN_BE_TAGGED)
		RETURN TRUE
	ENDIF

	IF IS_THIS_MODEL_A_TRAP_PRESSURE_PAD(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].mn)
		RETURN TRUE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].mn = INT_TO_ENUM(MODEL_NAMES, HASH("h4_Prop_h4_Loch_Monster"))
		RETURN TRUE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropSlideRule > -1
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_AVENGER_HOLD_TRANSITION_BLOCKED()
	RETURN IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_BLOCKING_AVENGER_HOLD_TRANSITION)
ENDFUNC

FUNC BOOL IS_AVENGER_HOLD_TRANSITION_BLOCKED_AND_INACTIVE(INT iRule, INT iTeam)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTen[iRule], ciBS_RULE10_PROGRESS_RULE_ALL_DRIVER_ACCEL)
		RETURN TRUE
	ENDIF
	
	INT iTeamLoop
	FOR iTeamLoop = 0 TO FMMC_MAX_TEAMS - 1
		IF MC_serverBD_4.iAvengerHoldTransitionBlockedCount[iTeamLoop] < MC_serverBD.iNumberOfPlayingPlayers[iTeamLoop]
			RETURN FALSE
		ENDIF
	ENDFOR
	
	RETURN TRUE
ENDFUNC

PROC REQUEST_AVENGER_HOLD_TRANSITION_BLOCKED(BOOL bValue)

	IF iSpectatorTarget != -1
	OR bIsAnySpectator
		PRINTLN("REQUEST_AVENGER_HOLD_TRANSITION_BLOCKED - bValue: ", BOOL_TO_STRING(bValue), " - am spectating, bail")
		EXIT
	ENDIF

	IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_REQUEST_AVENGER_HOLD_TRANSITION_BLOCK) = bValue
		PRINTLN("REQUEST_AVENGER_HOLD_TRANSITION_BLOCKED - bValue: ", BOOL_TO_STRING(bValue), " - PBBOOL4_REQUEST_AVENGER_HOLD_TRANSITION_BLOCK = bValue, bail")
		EXIT
	ENDIF

	IF bValue
		SET_BIT(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_REQUEST_AVENGER_HOLD_TRANSITION_BLOCK)
	ELSE
		CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_REQUEST_AVENGER_HOLD_TRANSITION_BLOCK)
	ENDIF
	
	PRINTLN("REQUEST_AVENGER_HOLD_TRANSITION_BLOCKED - bValue: ", BOOL_TO_STRING(bValue), " set/cleared PBBOOL4_REQUEST_AVENGER_HOLD_TRANSITION_BLOCK according to bValue")
	
ENDPROC

PROC SET_AVENGER_HOLD_TRANSITION_BLOCKED(BOOL bValue)
	
	// value the same, bail
	IF IS_AVENGER_HOLD_TRANSITION_BLOCKED() = bValue
		PRINTLN("SET_AVENGER_HOLD_TRANSITION_BLOCKED - bValue: ", BOOL_TO_STRING(bValue), " IS_AVENGER_HOLD_TRANSITION_BLOCKED = bValue, bail")
		EXIT
	ENDIF
	
	// cant block, warp in progress
	IF bValue
	AND IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(LocalPlayer)
		PRINTLN("SET_AVENGER_HOLD_TRANSITION_BLOCKED - bValue: ", BOOL_TO_STRING(bValue), " IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR, bail")
		EXIT
	ENDIF
	
	// Transition: Outside/Cockpick -> Hold
	IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
		IF IS_CREATOR_AIRCRAFT_ENTRY_BLOCKED(LocalPlayer) != bValue
			BLOCK_CREATOR_AIRCRAFT_INTERIOR_ENTRY(bValue)
			PRINTLN("SET_AVENGER_HOLD_TRANSITION_BLOCKED - bValue: ", BOOL_TO_STRING(bValue), " called BLOCK_CREATOR_AIRCRAFT_INTERIOR_ENTRY with bValue")
		ENDIF
	ENDIF
	
	// Transition: Hold -> Outside/Cockpick
	IF IS_SIMPLE_INTERIOR_EXIT_BLOCKED() != bValue
		BLOCK_SIMPLE_INTERIOR_EXIT(bValue)
		PRINTLN("SET_AVENGER_HOLD_TRANSITION_BLOCKED - bValue: ", BOOL_TO_STRING(bValue), " called BLOCK_SIMPLE_INTERIOR_EXIT with bValue")
	ENDIF
	
	// Lock the doors too
	IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		VEHICLE_INDEX veh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
		IF GET_ENTITY_MODEL(veh) = AVENGER
			
			IF bValue
				SET_VEHICLE_DOORS_LOCKED(veh, VEHICLELOCK_LOCKED_PLAYER_INSIDE)
			ELSE
				SET_VEHICLE_DOORS_LOCKED(veh, VEHICLELOCK_UNLOCKED)
			ENDIF
			
			PRINTLN("SET_AVENGER_HOLD_TRANSITION_BLOCKED - bValue: ", BOOL_TO_STRING(bValue), " Setting avenger doors lock state")
		ENDIF
	ENDIF

	IF bValue
		SET_BIT(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_BLOCKING_AVENGER_HOLD_TRANSITION)
	ELSE
		CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_BLOCKING_AVENGER_HOLD_TRANSITION)
	ENDIF
	
	PRINTLN("SET_AVENGER_HOLD_TRANSITION_BLOCKED - bValue: ", BOOL_TO_STRING(bValue), " set/cleared PBBOOL4_BLOCKING_AVENGER_HOLD_TRANSITION according to bValue")

ENDPROC

FUNC BOOL IS_VEHICLE_USING_RESPAWN_POOL(INT iVeh)
	RETURN g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehicleRespawnLives = NUMBER_OF_RESPAWNS_VEHICLE_POOL
ENDFUNC

FUNC INT GET_VEHICLE_RESPAWNS(INT iVeh)
	IF IS_VEHICLE_USING_RESPAWN_POOL(iVeh)
		RETURN MC_serverBD.iVehicleRespawnPool
	ELSE
		RETURN MC_serverBD_2.iCurrentVehRespawnLives[iVeh]
	ENDIF
ENDFUNC

PROC DECREMENT_VEHICLE_RESPAWNS(INT iVeh)
	IF IS_VEHICLE_USING_RESPAWN_POOL(iVeh)
		MC_serverBD.iVehicleRespawnPool--
		PRINTLN("[JS][VEHPOOL] - DECREMENT_VEHICLE_RESPAWNS - Decrementing vehicle pool respawns to ", MC_serverBD.iVehicleRespawnPool, " due to vehicle: ", iVeh)
	ELSE
		MC_serverBD_2.iCurrentVehRespawnLives[iVeh]--
		PRINTLN("[JS][VEHPOOL] - DECREMENT_VEHICLE_RESPAWNS - Decrementing vehicle ", iVeh, "'s respawns to ", MC_serverBD_2.iCurrentVehRespawnLives[iVeh])
	ENDIF
ENDPROC

PROC SET_VEHICLE_RESPAWNS(INT iVeh, INT iValue)
	IF IS_VEHICLE_USING_RESPAWN_POOL(iVeh)
		MC_serverBD.iVehicleRespawnPool = iValue
		PRINTLN("[JS][VEHPOOL] - SET_VEHICLE_RESPAWNS - Setting vehicle pool respawns to ", MC_serverBD.iVehicleRespawnPool, " due to vehicle: ", iVeh)
	ELSE
		MC_serverBD_2.iCurrentVehRespawnLives[iVeh] = iValue
		PRINTLN("[JS][VEHPOOL] - SET_VEHICLE_RESPAWNS - Setting vehicle ", iVeh, "'s respawns to ", MC_serverBD_2.iCurrentVehRespawnLives[iVeh])
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks if the object is visible and broken
///    Fixes issues caused by HAS_OBJECT_BEEN_BROKEN returning true if the object is invisible (e.g. during cutscenes)
/// PARAMS:
///    oiObj - The object to check
///    bNetworked - passed directly into HAS_OBJECT_BEEN_BROKEN's networked param
FUNC BOOL IS_OBJECT_BROKEN_AND_VISIBLE(OBJECT_INDEX oiObj, BOOL bNetworked = FALSE)
	IF NOT IS_ENTITY_VISIBLE(oiObj)
		RETURN FALSE
	ENDIF
	
	RETURN HAS_OBJECT_BEEN_BROKEN(oiObj, bNetworked)
ENDFUNC

FUNC BOOL IS_OBJECT_A_FRAGGABLE_CRATE(INT iObject)
	
	SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObject].mn
		CASE Prop_Box_Wood02A_MWS
		CASE PROP_BOX_WOOD02A
		CASE Prop_Container_LD_PU
			RETURN TRUE
	ENDSWITCH
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObject].mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_prop_sr_boxwood_01"))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC	

FUNC INT GET_HACK_SUBLOGIC_FROM_OBJECT(INT iObject)

	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObject].iObjectMinigame = ciFMMC_OBJECT_MINIGAME_TYPE__FULL_HACK
		RETURN ci_HACK_SUBLOGIC_HACK
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObject].iObjectMinigame = ciFMMC_OBJECT_MINIGAME_TYPE__CONNECT_HACK
		RETURN ci_HACK_SUBLOGIC_HACK_2
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObject].iObjectMinigame = ciFMMC_OBJECT_MINIGAME_TYPE__DRILLING
		RETURN ci_HACK_SUBLOGIC_DRILLING
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObject].iObjectMinigame = ciFMMC_OBJECT_MINIGAME_TYPE__VAULT_DRILLING
		RETURN ci_HACK_SUBLOGIC_VAULT_DRILLING
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObject].iObjectMinigame = ciFMMC_OBJECT_MINIGAME_TYPE__LESTER_SNAKE
		RETURN ci_HACK_SUBLOGIC_LESTER_SNAKE
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObject].iObjectMinigame = ciFMMC_OBJECT_MINIGAME_TYPE__BEAM_HACK
		RETURN ci_HACK_SUBLOGIC_BEAM_HACK
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObject].iObjectMinigame = ciFMMC_OBJECT_MINIGAME_TYPE__HOTWIRE
		RETURN ci_HACK_SUBLOGIC_HOTWIRE
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObject].iObjectMinigame = ciFMMC_OBJECT_MINIGAME_TYPE__INTERACT_WITH
		RETURN ci_HACK_SUBLOGIC_INTERACT_WITH
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObject].iObjectMinigame = ciFMMC_OBJECT_MINIGAME_TYPE__ORDER_UNLOCK
		RETURN ci_HACK_SUBLOGIC_ORDER_UNLOCK
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObject].iObjectMinigame = ciFMMC_OBJECT_MINIGAME_TYPE__FINGERPRINT_CLONE
		RETURN ci_HACK_SUBLOGIC_FINGERPRINT_CLONE
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObject].iObjectMinigame = ciFMMC_OBJECT_MINIGAME_TYPE__GLASS_CUTTING
		RETURN ci_HACK_SUBLOGIC_GLASS_CUTTING
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObject].iObjectMinigame = ciFMMC_OBJECT_MINIGAME_TYPE__WELD_UNDERWATER_TUNNEL
		RETURN ci_HACK_SUBLOGIC_UNDERWATER_WELDING
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObject].iObjectMinigame = ciFMMC_OBJECT_MINIGAME_TYPE__ENTER_SAFE_COMBINATION
		RETURN ci_HACK_SUBLOGIC_ENTER_SAFE_COMBINATION
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObject].iObjectMinigame = ciFMMC_OBJECT_MINIGAME_TYPE__INTERROGATION
		RETURN ci_HACK_SUBLOGIC_INTERROGATION
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObject].iObjectMinigame = ciFMMC_OBJECT_MINIGAME_TYPE__BRUTE_FORCE_ULP
		RETURN ci_HACK_SUBLOGIC_BRUTE_FORCE_ULP
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObject].iObjectMinigame = ciFMMC_OBJECT_MINIGAME_TYPE__BEAMHACK_VEHICLE
		RETURN ci_HACK_SUBLOGIC_BEAMHACK_VEH
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObject].iObjectMinigame = ciFMMC_OBJECT_MINIGAME_TYPE__CHECK_FUSEBOX
		RETURN ci_HACK_SUBLOGIC_CHECK_FUSEBOX
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObject].iObjectMinigame = ciFMMC_OBJECT_MINIGAME_TYPE__INSERT_FUSE
		RETURN ci_HACK_SUBLOGIC_INSERT_FUSE
	ENDIF	
	
	RETURN ci_HACK_SUBLOGIC_NONE
	
ENDFUNC

FUNC BOOL IS_PLAYER_IN_STEALTH_AND_AGGRO_DISGUISE(FMMC_CCTV_DATA& sCCTVData)
	IF IS_BIT_SET(sCCTVData.iCCTVBS, ciCCTV_BS_IgnoreDisguise_IslandGuard)
		PRINTLN("[CCTV] DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_CCTV_CAMERA - CCTV ignores Island Guard disguise")		
		IF IS_THIS_PLAYER_WEARING_AN_ISLAND_GUARD_OUTFIT(iLocalPart)
			PRINTLN("[CCTV] DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_CCTV_CAMERA - Player wearing Island Guard disguise")		
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(sCCTVData.iCCTVBS, ciCCTV_BS_IgnoreDisguise_IslandSmuggler)
		PRINTLN("[CCTV] DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_CCTV_CAMERA - CCTV ignores Island Smuggler disguise")		
		IF IS_THIS_PLAYER_WEARING_AN_ISLAND_SMUGGLER_OUTFIT(iLocalPart)
			PRINTLN("[CCTV] DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_CCTV_CAMERA - Player wearing Island Smuggler disguise")		
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(sCCTVData.iCCTVBS, ciCCTV_BS_IgnoreDisguise_TunerSecurity)
		PRINTLN("[CCTV] DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_CCTV_CAMERA - CCTV ignores Tuner Security disguise")		
		IF IS_THIS_PLAYER_WEARING_A_TUNER_SECURITY_OUTFIT(iLocalPart)
			PRINTLN("[CCTV] DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_CCTV_CAMERA - Player wearing Tuner Security disguise")		
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(sCCTVData.iCCTVBS, ciCCTV_BS_IgnoreDisguise_ULP_Maintenance)
		PRINTLN("[CCTV] DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_CCTV_CAMERA - CCTV ignores ULP Maintenance disguise")		
		IF IS_THIS_PLAYER_WEARING_AN_ULP_MAINTENANCE_OUTFIT(iLocalPart)
			PRINTLN("[CCTV] DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_CCTV_CAMERA - Player wearing ULP Maintenance disguise")		
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_USE_LONG_CCTV_SPOTTED_DELAY(FMMC_CCTV_DATA& sCCTVData)
	IF IS_BIT_SET(sCCTVData.iCCTVBS, ciCCTV_BS_UseLongDelayDisguise_IslandGuard)
		PRINTLN("[CCTV] DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_CCTV_CAMERA - CCTV ignores Island Guard disguise")		
		IF IS_THIS_PLAYER_WEARING_AN_ISLAND_GUARD_OUTFIT(iLocalPart)
			PRINTLN("[CCTV] DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_CCTV_CAMERA - Player wearing Island Guard disguise")		
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(sCCTVData.iCCTVBS, ciCCTV_BS_UseLongDelayDisguise_IslandSmuggler)
		PRINTLN("[CCTV] DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_CCTV_CAMERA - CCTV ignores Island Smuggler disguise")		
		IF IS_THIS_PLAYER_WEARING_AN_ISLAND_SMUGGLER_OUTFIT(iLocalPart)
			PRINTLN("[CCTV] DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_CCTV_CAMERA - Player wearing Island Smuggler disguise")		
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(sCCTVData.iCCTVBS, ciCCTV_BS_UseLongDelayDisguise_TunerSecurity)
		PRINTLN("[CCTV] DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_CCTV_CAMERA - CCTV ignores Tuner Security disguise")		
		IF IS_THIS_PLAYER_WEARING_A_TUNER_SECURITY_OUTFIT(iLocalPart)
			PRINTLN("[CCTV] DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_CCTV_CAMERA - Player wearing Tuner Security disguise")		
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(sCCTVData.iCCTVBS, ciCCTV_BS_UseLongDelayDisguise_ULPMaintenance)
		PRINTLN("[CCTV] DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_CCTV_CAMERA - CCTV ignores ULP Maintenance disguise")		
		IF IS_THIS_PLAYER_WEARING_AN_ULP_MAINTENANCE_OUTFIT(iLocalPart)
			PRINTLN("[CCTV] DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_CCTV_CAMERA - Player wearing ULP Maintenance  disguise")		
			RETURN TRUE
		ENDIF
	ENDIF	
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_CCTV_CAMERA(FMMC_CCTV_DATA& sCCTVData)
	//  CASINO_HEIST_OUTFIT_IN__BUGSTAR
	IF  IS_BIT_SET(sCCTVData.iCCTVBS, ciCCTV_BS_IgnoreDisguise_Bugstar)
		PRINTLN("[CCTV] DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_CCTV_CAMERA - CCTV ignores bugstar disguise")			
		IF  IS_LOCAL_PLAYER_WEARING_A_BUGSTAR_OUTFIT()
			PRINTLN("[CCTV] DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_CCTV_CAMERA - Player in Bugstar disguise")			
			RETURN TRUE
		ENDIF
	ENDIF
	
	// CASINO_HEIST_OUTFIT_IN__MECHANIC  
	
	IF IS_BIT_SET(sCCTVData.iCCTVBS, ciCCTV_BS_IgnoreDisguise_Mechanic)
		PRINTLN("[CCTV] DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_CCTV_CAMERA - CCTV ignores mechanic disguise")			
		IF IS_LOCAL_PLAYER_WEARING_A_MAINTENANCE_OUTFIT()
			PRINTLN("[CCTV] DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_CCTV_CAMERA - Player in mechanic disguise")			
			RETURN TRUE
		ENDIF
	ENDIF
	
	//  CASINO_HEIST_OUTFIT_IN__GRUPPE_SECHS
	
	IF IS_BIT_SET(sCCTVData.iCCTVBS, ciCCTV_BS_IgnoreDisguise_GruppeSechs)
		PRINTLN("[CCTV] DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_CCTV_CAMERA - CCTV ignores Gruppe Sechs disguise")			
		IF IS_LOCAL_PLAYER_WEARING_A_GRUPPE_SECHS_OUTFIT()
			PRINTLN("[CCTV] DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_CCTV_CAMERA - Player in Gruppe Sechs disguise")			
			RETURN TRUE
		ENDIF
	ENDIF
	
	//CASINO_HEIST_OUTFIT_IN__BRUCIE
	IF IS_BIT_SET(sCCTVData.iCCTVBS, ciCCTV_BS_IgnoreDisguise_Brucie)
		PRINTLN("[CCTV] DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_CCTV_CAMERA - CCTV ignores Brucie disguise")		
		IF IS_LOCAL_PLAYER_WEARING_A_CELEB_OUTFIT()
			PRINTLN("[CCTV] DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_CCTV_CAMERA - Player wearing Brucie disguise")		
			RETURN TRUE
		ENDIF
	ENDIF
	
	// CASINO_HEIST_OUTFIT_OUT__FIREMAN	
	IF IS_BIT_SET(sCCTVData.iCCTVBS, ciCCTV_BS_IgnoreDisguise_Fireman)
		PRINTLN("[CCTV] DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_CCTV_CAMERA - CCTV ignores Fireman disguise")		
		IF IS_LOCAL_PLAYER_WEARING_A_FIREMAN_OUTFIT()
			PRINTLN("[CCTV] DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_CCTV_CAMERA - Player wearing Fireman disguise")		
			RETURN TRUE
		ENDIF
	ENDIF
	
	// CASINO_HEIST_OUTFIT_OUT__HIGH_ROLLER
	IF  IS_BIT_SET(sCCTVData.iCCTVBS, ciCCTV_BS_IgnoreDisguise_HighRoller)
		PRINTLN("[CCTV] DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_CCTV_CAMERA - CCTV ignores High Roller disguise")		
		IF IS_LOCAL_PLAYER_WEARING_A_HIGH_ROLLER_OUTFIT()
			PRINTLN("[CCTV] DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_CCTV_CAMERA - Player wearing High Roller disguise")		
			RETURN TRUE
		ENDIF
	ENDIF
	
	//CASINO_HEIST_OUTFIT_OUT__SWAT
	IF IS_BIT_SET(sCCTVData.iCCTVBS, ciCCTV_BS_IgnoreDisguise_Noose)
		PRINTLN("[CCTV] DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_CCTV_CAMERA - CCTV ignores Noose disguise")		
		IF IS_LOCAL_PLAYER_WEARING_A_NOOSE_OUTFIT()
			PRINTLN("[CCTV] DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_CCTV_CAMERA - Player wearing Noose disguise")		
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_PLAYER_IN_STEALTH_AND_AGGRO_DISGUISE(sCCTVData)
		PRINTLN("[CCTV] DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_CCTV_CAMERA - Player wearing Stealth and Aggro disguise")		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PED_INVISIBLE(INT iPed)

	INT iTeamToCheck = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iCloakTeamIndex
	
	IF iTeamToCheck = -1
		RETURN FALSE
	ENDIF
	
	INT iRuleToCheck = gBG_MC_serverBD_VARS.iCurrentHighestPriority[iTeamToCheck]
	
	//Exit if the feature isn't used or if the current rule index is invalid
	IF (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iBecomeCloakedOnRule < 0)
	OR (iRuleToCheck >= FMMC_MAX_RULES)
	OR NOT DOES_ENTITY_EXIST(NET_TO_PED(mc_serverBD_1.sFMMC_SBD.niPed[iPed]))
		RETURN FALSE
	ENDIF
	
	//If the current rule number is higher than the "become cloaked on this rule", you might want to be cloaked right now
	BOOL bCurrentlyInvisible = (iRuleToCheck >= g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iBecomeCloakedOnRule)
	
	//Unless the rule number has gone past the "remove cloak" rule number (if it's larger than -1)
	IF (iRuleToCheck >= g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iLoseCloakOnRule)
	AND(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iLoseCloakOnRule > -1)
		bCurrentlyInvisible = FALSE
	ENDIF
	
	RETURN bCurrentlyInvisible
ENDFUNC

FUNC BOOL IS_PLAYER_PERFORMING_INTERACT_WITH_CUTSCENE_LEAD_IN()
	RETURN IS_BIT_SET(iLocalBoolCheck30, LBOOL30_PERFORMING_INTERACT_WITH_CUTSCENE_LEAD_IN)
ENDFUNC

FUNC NETWORK_INDEX GET_INTERACT_WITH_SPAWNED_PROP_NET_ID(INT iSpawnedProp, INT iPart = -1)
	IF iPart = -1
		iPart = iLocalPart
	ENDIF
	
	IF iSpawnedProp = -1
		RETURN NULL
	ENDIF
	
	RETURN MC_playerBD_1[iPart].niInteractWith_CurrentSpawnedProps[iSpawnedProp]
ENDFUNC

FUNC OBJECT_INDEX GET_INTERACT_WITH_SPAWNED_PROP(INT iSpawnedProp, INT iPart = -1)
	IF NETWORK_DOES_NETWORK_ID_EXIST(GET_INTERACT_WITH_SPAWNED_PROP_NET_ID(iSpawnedProp, iPart))
		RETURN NET_TO_OBJ(GET_INTERACT_WITH_SPAWNED_PROP_NET_ID(iSpawnedProp, iPart))
	ENDIF
	
	RETURN NULL
ENDFUNC

FUNC OBJECT_INDEX GET_INTERACT_WITH_PERSISTENT_PROP(INT iPersistentProp, INT iPart = -1)

	IF iPart = -1
		iPart = iLocalPart
	ENDIF
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(MC_PlayerBD_1[iPart].niInteractWith_PersistentProps[iPersistentProp])
		RETURN NET_TO_OBJ(MC_PlayerBD_1[iPart].niInteractWith_PersistentProps[iPersistentProp])
	ENDIF
	
	RETURN NULL
ENDFUNC

FUNC BOOL IS_OBJECT_AN_INTERACT_WITH_PERSISTENT_PROP(OBJECT_INDEX oiObj)

	INT iPersistentProp
	FOR iPersistentProp = 0 TO MC_PlayerBD_1[iLocalPart].iInteractWith_NumPersistentProps - 1
		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_PlayerBD_1[iLocalPart].niInteractWith_PersistentProps[iPersistentProp])
			// Persistent prop slot is empty!
			RELOOP
		ENDIF
		
		IF NET_TO_OBJ(MC_PlayerBD_1[iLocalPart].niInteractWith_PersistentProps[iPersistentProp]) = oiObj
			RETURN TRUE
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PED_CURRENTLY_INVOLVED_IN_INTERACT_WITH(INT iPed)
	RETURN FMMC_IS_LONG_BIT_SET(iPedInvolvedInInteractWithBS, iPed)
ENDFUNC

FUNC BOOL MARK_PLACED_PED_INVOLVED_IN_INTERACT_WITH(INT iPed, BOOL bInvolved)
	IF bInvolved
		IF NOT IS_PED_CURRENTLY_INVOLVED_IN_INTERACT_WITH(iPed)
			PRINTLN("[InteractWith_SPAM][Ped ", iPed, "][InteractWith_LinkedPeds][InteractWith_NearbyPeds] MARK_PLACED_PED_INVOLVED_IN_INTERACT_WITH | Broadcasting event to mark ped ", iPed, " as involved in an Interact-With!")
			FMMC_SET_LONG_BIT(iPedInvolvedInInteractWithBS, iPed)
			BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_MarkPedAsInvolvedInInteractWith, DEFAULT, iPed, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
			RETURN FALSE
		ELSE
			RETURN TRUE
		ENDIF
	ELSE
		IF IS_PED_CURRENTLY_INVOLVED_IN_INTERACT_WITH(iPed)
			PRINTLN("[InteractWith_SPAM][Ped ", iPed, "][InteractWith_LinkedPeds][InteractWith_NearbyPeds] MARK_PLACED_PED_INVOLVED_IN_INTERACT_WITH | Broadcasting event to mark ped ", iPed, " as no longer involved in an Interact-With!")
			FMMC_CLEAR_LONG_BIT(iPedInvolvedInInteractWithBS, iPed)
			BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_MarkPedAsInvolvedInInteractWith, DEFAULT, iPed, DEFAULT, DEFAULT, DEFAULT, DEFAULT, FALSE)
			RETURN FALSE
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
ENDFUNC

FUNC BOOL MARK_PED_INVOLVED_IN_INTERACT_WITH(PED_INDEX piPed, BOOL bInvolved)
	INT iLinkedPedIndex = IS_ENTITY_A_MISSION_CREATOR_ENTITY(piPed)
	
	IF iLinkedPedIndex = -1
		// This ped isn't a placed ped - just pass this check
		RETURN TRUE
	ENDIF
	
	RETURN MARK_PLACED_PED_INVOLVED_IN_INTERACT_WITH(iLinkedPedIndex, bInvolved)
ENDFUNC

FUNC MODEL_NAMES GET_INTERACT_WITH_BAG_MODEL_TO_USE()

	IF MC_playerBD_1[iLocalPart].mnHeistGearBag = hei_P_M_bag_var22_Arm_S
		// New and improved model for this, with a better rig to match the outfit version of the bag
		RETURN INT_TO_ENUM(MODEL_NAMES, HASH("h4_P_h4_M_bag_var22_Arm_S"))
	ENDIF
	
	RETURN MC_playerBD_1[iLocalPart].mnHeistGearBag
ENDFUNC

FUNC VEHICLE_INDEX GET_INTERACT_WITH_LINKED_VEHICLE_FROM_PARAMS(INTERACT_WITH_PARAMS& sInteractWithParams)
	IF sInteractWithParams.iAttachedVehicleIndex > -1
		RETURN GET_FMMC_ENTITY_VEHICLE(sInteractWithParams.iAttachedVehicleIndex)
	ENDIF
	
	RETURN NULL
ENDFUNC

FUNC PED_INDEX GET_INTERACT_WITH_LINKED_PED_FROM_PARAMS(INTERACT_WITH_PARAMS& sInteractWithParams)
	IF sInteractWithParams.iAttachedPedIndex > -1
		RETURN GET_FMMC_ENTITY_PED(sInteractWithParams.iAttachedPedIndex)
	ENDIF
	
	RETURN NULL
ENDFUNC

FUNC OBJECT_INDEX GET_INTERACT_WITH_LINKED_DYNOPROP_FROM_PARAMS(INTERACT_WITH_PARAMS& sInteractWithParams)
	IF sInteractWithParams.iAttachedDynopropIndex > -1
		RETURN GET_FMMC_ENTITY_DYNOPROP(sInteractWithParams.iAttachedDynopropIndex)
	ENDIF
	
	RETURN NULL
ENDFUNC

FUNC OBJECT_INDEX GET_INTERACT_WITH_LINKED_OBJECT_FROM_PARAMS(INTERACT_WITH_PARAMS& sInteractWithParams, INT iLinkedObject)
	IF sInteractWithParams.iLinkedObjectIDs[iLinkedObject] > -1
		RETURN GET_FMMC_ENTITY_OBJECT(sInteractWithParams.iLinkedObjectIDs[iLinkedObject])
	ENDIF
	
	RETURN NULL
ENDFUNC

FUNC OBJECT_INDEX GET_INTERACT_WITH_ATTACHED_OBJECT_FROM_PARAMS(INTERACT_WITH_PARAMS& sInteractWithParams)
	IF sInteractWithParams.iAttachedObjectIndex > -1
		RETURN GET_FMMC_ENTITY_OBJECT(sInteractWithParams.iAttachedObjectIndex)
	ENDIF
	
	RETURN NULL
ENDFUNC

FUNC VECTOR GET_FMMC_LAST_USED_VEHICLE_LOCATION()
	VECTOR vReturnLoc
	IF iLastUsedPlacedVeh > -1
		IF iLastUsedPlacedVeh < FMMC_MAX_VEHICLES
			IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iLastUsedPlacedVeh])
				PRINTLN("[RCC MISSION] GET_FMMC_LAST_USED_VEHICLE_LOCATION - Veh ", iLastUsedPlacedVeh)
				VEHICLE_INDEX viTemp = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iLastUsedPlacedVeh])
				vReturnLoc = GET_ENTITY_COORDS(viTemp, FALSE)
				IF NOT IS_ENTITY_DEAD(viTemp)
					IF GET_ENTITY_MODEL(viTemp) = TAMPA3 //This can probably be applied to all vehicles when there is more QA time
						VECTOR vMin, vMax
						GET_MODEL_DIMENSIONS(TAMPA3, vMin, vMax)
						vMin = vMin * 1.5
						vReturnLoc = vReturnLoc + (vMin.y * GET_ENTITY_FORWARD_VECTOR(viTemp))
						#IF IS_DEBUG_BUILD
						DRAW_DEBUG_SPHERE(vReturnLoc, 2,0,255,0,200)
						#ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN vReturnLoc
ENDFUNC

PROC SET_HELI_HOVERING(VEHICLE_INDEX vehHeli, BOOL bRetractLandingGear = TRUE)

	IF NOT IS_VEHICLE_DRIVEABLE(vehHeli)
		PRINTLN("[RCC MISSION] SET_HELI_HOVERING - vehicle is not drivable")
		EXIT
	ENDIF

	MODEL_NAMES eModel = GET_ENTITY_MODEL(vehHeli)
	IF NOT IS_THIS_MODEL_A_HELI(eModel)
	AND eModel != HYDRA
	AND eModel != TULA
	AND eModel != AVENGER
		PRINTLN("[RCC MISSION] SET_HELI_HOVERING - model: ", FMMC_GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(vehHeli))," is not a heli")
		EXIT
	ENDIF

	IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(vehHeli)
		PRINTLN("[RCC MISSION] SET_HELI_HOVERING - do not have net control of entity")
		EXIT
	ENDIF
	
	IF (eModel = HYDRA
	OR eModel = TULA
	OR eModel = AVENGER)
	AND GET_VEHICLE_FLIGHT_NOZZLE_POSITION(vehHeli) != 1.0
		SET_VEHICLE_FLIGHT_NOZZLE_POSITION_IMMEDIATE(vehHeli, 1.0)
		PRINTLN("[RCC MISSION] SET_HELI_HOVERING - Setting flight nozzle position to 1.0")
	ENDIF
	
	//vEndPos.z += 10.0
	SET_VEHICLE_ENGINE_ON(vehHeli, TRUE, TRUE, FALSE)
	
	IF bRetractLandingGear
	AND GET_VEHICLE_HAS_LANDING_GEAR(vehHeli)
		CONTROL_LANDING_GEAR(vehHeli, LGC_RETRACT_INSTANT)
		PRINTLN("[RCC MISSION] SET_HELI_HOVERING - Retracting landing gear")
	ENDIF

	SET_VEHICLE_FORWARD_SPEED(vehHeli, 1.0)
	SET_HELI_BLADES_FULL_SPEED(vehHeli)
	
	PRINTLN("[RCC MISSION] SET_HELI_HOVERING - Completed")
ENDPROC

// investigate this..
FUNC VECTOR GET_LOCATION_VECTOR(INT iLoc)
	
	RETURN vGoToLocateVectors[iLoc]
	
ENDFUNC


PROC  PROCESS_OFFSET_FOR_AREA_POS_0(VECTOR vPos1, VECTOR vPos2, VECTOR vNewCentre, VECTOR &vAssignPosition)
	VECTOR vCentre = (vPos1 + vPos2)
	vCentre.x /= 2
	vCentre.y /= 2
	vCentre.z /= 2
	
	VECTOR vOffset0 = vCentre - vPos2
	vOffset0.x = ABSF(vOffset0.x)
	vOffset0.y = ABSF(vOffset0.y)
	vOffset0.z = ABSF(vOffset0.z)
	
	vNewCentre.x -= (vOffset0.x/2)
	vNewCentre.y -= (vOffset0.y/2)
	vNewCentre.z -= (vOffset0.z/2)
	vAssignPosition = vNewCentre
ENDPROC

PROC PROCESS_OFFSET_FOR_AREA_POS_1(VECTOR vPos1, VECTOR vPos2, VECTOR vNewCentre, VECTOR &vAssignPosition)
	VECTOR vCentre = (vPos1 + vPos2)
	vCentre.x /= 2
	vCentre.y /= 2
	vCentre.z /= 2
	
	VECTOR vOffset1 = vCentre - vPos2
	
	vOffset1.x = ABSF(vOffset1.x)
	vOffset1.y = ABSF(vOffset1.y)
	vOffset1.z = ABSF(vOffset1.z)
	
	vNewCentre.x += (vOffset1.x/2)
	vNewCentre.y += (vOffset1.y/2)
	vNewCentre.z += (vOffset1.z/2)
	vAssignPosition = vNewCentre
ENDPROC

PROC MC_PROCESS_OFFSET_FOR_AREA(VECTOR &vPos1, VECTOR &vPos2, INT iType, VECTOR vNewCentre)
	VECTOR vNewPos0, vNewPos1
	IF iType = 0 // radius
		vPos1 = vNewCentre
		PRINTLN("[MC_PROCESS_OFFSET_FOR_AREA] - DONE vNewCentre: ", vNewCentre)
	ELSE
	
		#IF IS_DEBUG_BUILD
			VECTOR vOldCentre = (vPos1 + vPos2)
			vOldCentre.x /= 2
			vOldCentre.y /= 2
			vOldCentre.z /= 2
			PRINTLN("[MC_PROCESS_OFFSET_FOR_AREA] - vNewCentre: ", vNewCentre, " vOldCentre: ", vOldCentre)
		#ENDIF
		
		PRINTLN("[MC_PROCESS_OFFSET_FOR_AREA] - DONE vOldPos0: ", vPos1, " vOldPos1: ", vPos2)
		
		PROCESS_OFFSET_FOR_AREA_POS_0(vPos1, vPos2, vNewCentre, vNewPos0)
		PROCESS_OFFSET_FOR_AREA_POS_1(vPos1, vPos2, vNewCentre, vNewPos1)
		
		PRINTLN("[MC_PROCESS_OFFSET_FOR_AREA] - DONE vNewPos0: ", vNewPos0, " vNewPos1: ", vNewPos1)
				
		vPos1 = vNewPos0
		vPos2 = vNewPos1
	ENDIF
ENDPROC

FUNC VECTOR GET_INIT_LOCATION_VECTOR(INT iLoc)
	
	VECTOR vLoc

	VECTOR vPlacedGotoPosition = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].vloc[0]
	
	IF IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].vLoc1)
	OR IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].vLoc2)
		vLoc = vPlacedGotoPosition
	ELSE
	    vLoc = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].vLoc1 
				+ g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].vLoc2
        vLoc *= << 0.5, 0.5, 0.5 >>
	ENDIF
	
	RETURN vLoc
	
ENDFUNC

PROC GET_INIT_LOCATION_SIZE_AND_HEIGHT(INT iLoc, FLOAT &fSize, FLOAT &fHeight)

	IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iCoronaSize = LOCATION_CORONA_SIZE_SMALL
		fHeight = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].fLocateHeight
		fSize = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].fRadius[0] / 10
		IF fSize < 0.3
			fSize = 0.3
		ENDIF
		IF fSize > 1.0
			fSize = 1.0
		ENDIF
	ELIF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iCoronaSize = LOCATION_CORONA_SIZE_ACTUAL
		fHeight = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].fLocateHeight
		fSize = 2 * g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].fRadius[0]
		IF fSize < 0.3
			fSize = 0.3
		ENDIF
	ELIF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iCoronaSize = LOCATION_CORONA_SIZE_THREE_QUARTER
		fHeight = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].fLocateHeight
		fSize = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].fRadius[0]
		IF fSize < 0.3
			fSize = 0.3
		ENDIF
	ELIF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iCoronaSize = LOCATION_CORONA_SIZE_CUSTOM
		fHeight = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].fLocateHeight
		fSize = ((1 + g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].fLocateCoronaScaler) * g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].fRadius[0])
		IF fSize < 0.5
			fSize = 0.5
		ENDIF
	ELIF IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ iLoc ].iLocBS, ciLoc_BS_UseStandardSizeLocate)
		fHeight = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].fLocateHeight
		fSize = 0.6 // Default SP locate size
	ELIF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iSmallCorona[0] = 1
		fHeight = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].fLocateHeight
		fSize = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].fRadius[0] / 10
		IF fSize < 0.3
			fSize = 0.3
		ENDIF
		IF fSize > 1.0
			fSize = 1.0
		ENDIF
	ELSE //Large corona
		fHeight = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].fLocateHeight
		fSize = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].fRadius[0] / 5
		IF fSize < 0.5
			fSize = 0.5
		ENDIF
		IF fSize > 5.0
			fSize = 5.0
		ENDIF
	ENDIF
ENDPROC

FUNC VECTOR GET_FMMC_ENTITY_LOCATION(VECTOR vPlayerNearestThisLoc, INT iSpawnNearType = ciRULE_TYPE_NONE, INT iSpawnNearEntityID = -1, ENTITY_INDEX eiPlayerNearestThisEntity = NULL, INT iVeh = -1)
	
	VECTOR vEntityLoc
	BOOL bTargetCheck
	
	NETWORK_INDEX niTarget = NULL
	
	SWITCH iSpawnNearType
		CASE ciSPAWN_NEAR_ENTITY_TYPE_PED
			IF iSpawnNearEntityID < FMMC_MAX_PEDS
				PRINTLN("[JS] [SPAWNNEAR] - GET_FMMC_ENTITY_LOCATION - 1 PED: ", iSpawnNearEntityID)
				niTarget = MC_serverBD_1.sFMMC_SBD.niPed[iSpawnNearEntityID]
				bTargetCheck = TRUE
			ENDIF
		BREAK
		
		CASE ciSPAWN_NEAR_ENTITY_TYPE_VEHICLE
			IF iSpawnNearEntityID < FMMC_MAX_VEHICLES
				PRINTLN("[JS] [SPAWNNEAR] - GET_FMMC_ENTITY_LOCATION - 1 VEHICLE: ", iSpawnNearEntityID)
				niTarget = MC_serverBD_1.sFMMC_SBD.niVehicle[iSpawnNearEntityID]
				bTargetCheck = TRUE
			ENDIF
		BREAK
		
		CASE ciSPAWN_NEAR_ENTITY_TYPE_OBJECT
			IF iSpawnNearEntityID < FMMC_MAX_NUM_OBJECTS
				PRINTLN("[JS] [SPAWNNEAR] - GET_FMMC_ENTITY_LOCATION - 1 OBJECT: ", iSpawnNearEntityID)
				niTarget = GET_OBJECT_NET_ID(iSpawnNearEntityID)
				bTargetCheck = TRUE
			ENDIF
		BREAK
		
		CASE ciSPAWN_NEAR_ENTITY_TYPE_LOCATION
			IF iSpawnNearEntityID < FMMC_MAX_RULES
				PRINTLN("[JS] [SPAWNNEAR] - GET_FMMC_ENTITY_LOCATION - 1 GOTO: ", iSpawnNearEntityID)
				vEntityLoc = GET_LOCATION_VECTOR(iSpawnNearEntityID)
			ENDIF
		BREAK
		
		CASE ciSPAWN_NEAR_ENTITY_TYPE_TEAM
			//Get the player closest to the original spawn position:
			IF iSpawnNearEntityID < MC_serverBD.iNumberOfTeams
			AND (MC_serverBD.iNumberOfPlayingPlayers[iSpawnNearEntityID] > 0)
			
				PRINTLN("[JS] [SPAWNNEAR] - GET_FMMC_ENTITY_LOCATION - 1 PLAYER: ", iSpawnNearEntityID)
				
				INT iClosestPlayer
				PLAYER_INDEX tempPlayer				
				
				iClosestPlayer = GET_NEAREST_PLAYER_TO_ENTITY_ON_TEAM(eiPlayerNearestThisEntity, iSpawnNearEntityID)
				tempPlayer = INT_TO_PLAYERINDEX(iClosestPlayer)
				
				PRINTLN("GET_FMMC_ENTITY_LOCATION - Checking player: ", iClosestPlayer)
				
				IF (iClosestPlayer != -1 AND iClosestPlayer != iLocalPart)
				AND IS_NET_PLAYER_OK(tempPlayer)
					vEntityLoc = GET_PLAYER_COORDS(tempPlayer)
				#IF IS_DEBUG_BUILD
				ELSE
					IF NOT IS_NET_PLAYER_OK(tempPlayer)
						PRINTLN("GET_FMMC_ENTITY_LOCATION - Player is not OK, going to manual method of finding closest player")
					ENDIF
				#ENDIF
				ENDIF
				
				IF eiPlayerNearestThisEntity = NULL
				OR IS_VECTOR_ZERO(vEntityLoc)
					
					INT iPartLoop
					INT iClosestPart
					iClosestPart = -1
					INT iTeamChecked
					FLOAT fClosestDist2
					fClosestDist2 = 999999999 //Furthest distance apart on map is O(10,000)
					FLOAT fDistance2
					
					PRINTLN("[PLAYER_LOOP] - GET_FMMC_ENTITY_LOCATION 2")
					FOR iPartLoop = 0 TO (GET_MAXIMUM_PLAYERS_ON_MISSION() - 1)
						IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPartLoop))
						AND (MC_playerBD[iPartLoop].iteam = iSpawnNearEntityID)
							iTeamChecked++
							PRINTLN("NEW DEBUG - Checking part ", iPartLoop)
							tempPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPartLoop))
							
							PED_INDEX piEnt
							piEnt = GET_PLAYER_PED(tempPlayer)
							
							IF IS_NET_PLAYER_OK(tempPlayer)
							AND NOT IS_ENTITY_IN_WATER(piEnt)
							AND NOT IS_ENTITY_IN_AIR(piEnt)
								fDistance2 = VDIST2(GET_PLAYER_COORDS(tempPlayer),vPlayerNearestThisLoc)
								PRINTLN("NEW DEBUG - this participant is fine, fDistance2: ", fDistance2)
								IF fDistance2 < fClosestDist2
									fClosestDist2 = fDistance2
									iClosestPart = iPartLoop
									PRINTLN("NEW DEBUG - Updating: ", fClosestDist2, " iClosestPart: ", iClosestPart)
								ENDIF
							ENDIF
						ENDIF
						
						IF iTeamChecked >= MC_serverBD.iNumberOfPlayingPlayers[iSpawnNearEntityID]
							BREAKLOOP
						ENDIF
					ENDFOR
					
					IF iClosestPart != -1
						vEntityLoc = GET_PLAYER_COORDS(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iClosestPart)))
					ENDIF
					
				ENDIF
			ELSE
				PRINTLN("[JS] [SPAWNNEAR] - GET_FMMC_ENTITY_LOCATION - Number of players not assigned yet...")
			ENDIF
		BREAK
		
		CASE ciSPAWN_NEAR_ENTITY_TYPE_LAST_PLAYER
			PRINTLN("[JS] [SPAWNNEAR] - GET_FMMC_ENTITY_LOCATION - 1 ciSPAWN_NEAR_ENTITY_TYPE_LAST_PLAYER")
			IF iVeh > -1
				INT i, iPlayersToCheck, iPlayersChecked
				iPlayersToCheck = GET_NUMBER_OF_PLAYING_PLAYERS()
				IF iPlayersToCheck > 1
					PRINTLN("[PLAYER_LOOP] - GET_FMMC_ENTITY_LOCATION")
					FOR i = 0 TO (GET_MAXIMUM_PLAYERS_ON_MISSION() - 1)
						IF (NOT(IS_BIT_SET(MC_playerBD[i].iClientBitSet, PBBOOL_ANY_SPECTATOR)))
						AND NOT IS_BIT_SET(MC_playerBD[i].iClientBitSet,PBBOOL_PLAYER_FAIL)
						AND IS_BIT_SET(MC_playerBD[i].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
						AND NOT IS_BIT_SET(MC_playerBD[i].iClientBitSet, PBBOOL_FINISHED)
							PARTICIPANT_INDEX tempPart
							tempPart = INT_TO_PARTICIPANTINDEX(i)
							IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
								iPlayersChecked++
								IF MC_PlayerBD[i].iLastVeh = iVeh
									vEntityLoc = GET_PLAYER_COORDS(NETWORK_GET_PLAYER_INDEX(tempPart))
									PRINTLN("[JS] GET_FMMC_ENTITY_LOCATION - Spawning near part ", i, " vector: ", vEntityLoc)
									BREAKLOOP
								ENDIF
								IF iPlayersChecked >= iPlayersToCheck
									BREAKLOOP
								ENDIF
							ENDIF
						ENDIF
					ENDFOR
				ENDIF
			ENDIF
		BREAK
		
		CASE ciSPAWN_NEAR_ENTITY_TYPE_TRAIN
			IF iSpawnNearEntityID < FMMC_MAX_TRAINS
				PRINTLN("[JS] [SPAWNNEAR] - GET_FMMC_ENTITY_LOCATION - 1 TRAIN: ", iSpawnNearEntityID)
				niTarget = MC_serverBD_1.sFMMC_SBD.niTrain[iSpawnNearEntityID]
				bTargetCheck = TRUE
			ENDIF
		BREAK
		
		CASE ciSPAWN_NEAR_ENTITY_TYPE_LOBBY_LEADER
			PRINTLN("[JS] [SPAWNNEAR] - GET_FMMC_ENTITY_LOCATION - ciSPAWN_NEAR_ENTITY_TYPE_LOBBY_LEADER")
			
			IF iVeh > -1
				INT iLobbyLeaderPart
				iLobbyLeaderPart = GET_FMMC_LOBBY_LEADER_PARTICIPANT_INDEX()
				
				IF iLobbyLeaderPart > -1
					IF (NOT(IS_BIT_SET(MC_playerBD[iLobbyLeaderPart].iClientBitSet, PBBOOL_ANY_SPECTATOR)))
					AND NOT IS_BIT_SET(MC_playerBD[iLobbyLeaderPart].iClientBitSet,PBBOOL_PLAYER_FAIL)
					AND IS_BIT_SET(MC_playerBD[iLobbyLeaderPart].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
					AND NOT IS_BIT_SET(MC_playerBD[iLobbyLeaderPart].iClientBitSet, PBBOOL_FINISHED)
					
						PARTICIPANT_INDEX tempPart
						tempPart = INT_TO_PARTICIPANTINDEX(iLobbyLeaderPart)
						
						IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
							vEntityLoc = GET_PLAYER_COORDS(NETWORK_GET_PLAYER_INDEX(tempPart))
							PRINTLN("[SPAWNNEAR] GET_FMMC_ENTITY_LOCATION - Spawning near Lobby Leader (part ", iLobbyLeaderPart, ") vector: ", vEntityLoc)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	IF bTargetCheck
		PRINTLN("[JS] [SPAWNNEAR] - GET_FMMC_ENTITY_LOCATION - 2")
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(niTarget)
			vEntityLoc = GET_ENTITY_COORDS(NET_TO_ENT(niTarget), FALSE)
			PRINTLN("[JS] [SPAWNNEAR] - GET_FMMC_ENTITY_LOCATION - 3 vEntityLoc = ", vEntityLoc)
		ENDIF
	ENDIF
	
	PRINTLN("[JS] [SPAWNNEAR] - GET_FMMC_ENTITY_LOCATION - Returning vEntityLoc = ", vEntityLoc)
	
	RETURN vEntityLoc
	
ENDFUNC

//Gets what rule the entity is on for a certain team - first rule = 0, from any extra objectives = 1 - 5
FUNC INT GET_ENTITY_MULTIRULE_NUMBER(INT iEntity, INT iTeam, INT iTargetType = ci_TARGET_NONE)
	
	INT iMRule = -1
	
	IF iTargetType != ci_TARGET_NONE
	
		INT iCurrentPriority = FMMC_PRIORITY_IGNORE
		INT iPrimaryPriority = FMMC_PRIORITY_IGNORE
		INT iPrimaryRule = FMMC_OBJECTIVE_LOGIC_NONE
		INT iEntityType = ciRULE_TYPE_NONE
		
		SWITCH iTargetType
			CASE ci_TARGET_PED
				iCurrentPriority = MC_serverBD_4.iPedPriority[iEntity][iTeam]
				iEntityType = ciRULE_TYPE_PED
				iPrimaryPriority = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iEntity].iPriority[iTeam]
				iPrimaryRule = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iEntity].iRule[iTeam]
			BREAK
			
			CASE ci_TARGET_OBJECT
				iCurrentPriority = MC_serverBD_4.iObjPriority[iEntity][iTeam]
				iEntityType = ciRULE_TYPE_OBJECT
				iPrimaryPriority = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iEntity].iPriority[iTeam]
				iPrimaryRule = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iEntity].iRule[iTeam]
			BREAK
			
			CASE ci_TARGET_VEHICLE
				iCurrentPriority = MC_serverBD_4.iVehPriority[iEntity][iTeam]
				iEntityType = ciRULE_TYPE_VEHICLE
				iPrimaryPriority = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iEntity].iPriority[iTeam]
				iPrimaryRule = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iEntity].iRule[iTeam]
			BREAK
		ENDSWITCH
		
		IF iCurrentPriority < FMMC_MAX_RULES
			IF iPrimaryPriority < FMMC_MAX_RULES
			AND iPrimaryRule != FMMC_OBJECTIVE_LOGIC_NONE
				IF (iPrimaryPriority >= iCurrentPriority)
					iMRule = 0
				ELSE
					
					INT iextraObjectiveNum
					// Loop through all potential entites that have multiple rules
					FOR iextraObjectiveNum = 0 TO ( MAX_NUM_EXTRA_OBJECTIVE_ENTITIES-1 )
						// If the type matches the one passed
						IF g_FMMC_STRUCT.iExtraObjectiveEntityType[iextraObjectiveNum] = iEntityType
						AND g_FMMC_STRUCT.iExtraObjectiveEntityId[iextraObjectiveNum] = iEntity
							INT iextraObjectiveLoop
							
							FOR iextraObjectiveLoop = 0 TO (MAX_NUM_EXTRA_OBJECTIVE_PER_ENTITY - 1)
								IF g_FMMC_STRUCT.iExtraObjectivePriority[iextraObjectiveNum][iextraObjectiveLoop][iTeam] < FMMC_MAX_RULES
								AND (GET_FMMC_RULE_FROM_CREATOR_RULE(g_FMMC_STRUCT.iExtraObjectiveRule[iextraObjectiveNum][iextraObjectiveLoop][iTeam]) != FMMC_OBJECTIVE_LOGIC_NONE)
									
									IF (g_FMMC_STRUCT.iExtraObjectivePriority[iextraObjectiveNum][iextraObjectiveLoop][iTeam] >= iCurrentPriority)
										iMRule = iextraObjectiveLoop + 1
										iextraObjectiveLoop = MAX_NUM_EXTRA_OBJECTIVE_PER_ENTITY //Break out
									ELSE
										//Keep looking through the loop
									ENDIF
									
								ENDIF
							ENDFOR
							//We found the multi-rule number for this entity, there won't be any more so break out of the loop:
							iextraObjectiveNum = MAX_NUM_EXTRA_OBJECTIVE_ENTITIES
						ENDIF
					ENDFOR
					
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN iMRule
	
ENDFUNC

FUNC INT GET_NEXT_PRIORITY_FOR_ENTITY(INT iObjectiveType, INT iObjectiveID, INT iteam, INT iMinRuleToCheck )
	
	INT iextraObjectiveNum

	// Loop through all potential entites that have multiple rules
	FOR iextraObjectiveNum = 0 TO ( MAX_NUM_EXTRA_OBJECTIVE_ENTITIES-1 )
		// If the type matches the one passed
		IF g_FMMC_STRUCT.iExtraObjectiveEntityType[iextraObjectiveNum]= iObjectiveType
		AND g_FMMC_STRUCT.iExtraObjectiveEntityId[iextraObjectiveNum] = iObjectiveID
	
			// If the current objective passed is less than that of the max per entity
			IF MC_serverBD.iCurrentExtraObjective[iextraObjectiveNum][iteam] < MAX_NUM_EXTRA_OBJECTIVE_PER_ENTITY
				
				IF g_FMMC_STRUCT.iExtraObjectivePriority[iextraObjectiveNum][MC_serverBD.iCurrentExtraObjective[iextraObjectiveNum][iteam]][iteam] < FMMC_MAX_RULES
				AND g_FMMC_STRUCT.iExtraObjectivePriority[iextraObjectiveNum][MC_serverBD.iCurrentExtraObjective[iextraObjectiveNum][iteam]][iteam] >= 0 // and it's not -1
					
					INT iNextObjectiveValue = g_FMMC_STRUCT.iExtraObjectivePriority[ iextraObjectiveNum ][ MC_serverBD.iCurrentExtraObjective[ iextraObjectiveNum ][ iteam ] ][ iteam ]
					
					// So if iNextObjectiveValue is less than the total there can be, we've found a valid thang
					IF iNextObjectiveValue > iMinRuleToCheck 
						RETURN iNextObjectiveValue
					ENDIF
					
				ENDIF
			ENDIF
		ENDIF
	ENDFOR

	RETURN FMMC_PRIORITY_IGNORE

ENDFUNC

FUNC BOOL IS_ENTITY_INVOLVED_IN_THIS_TEAMS_RULE(INT iRuleType = ciRULE_TYPE_NONE, INT iNumber = -1, INT iTeam = -1, INT iRule = -1)
	
	BOOL bOKData = FALSE
	BOOL bInvolved = FALSE
		
	PRINTLN("[IS_ENTITY_INVOLVED_IN_THIS_TEAMS_RULE] - iRuleType: ", iRuleType, "  iNumber: ", iNumber, " iTeam: ", iTeam, " iRule: ", iRule)
	
	SWITCH iRuleType
		CASE ciRULE_TYPE_PED
			IF iNumber >= 0
			AND iNumber < FMMC_MAX_PEDS
			AND iTeam >= 0
			AND iTeam < FMMC_MAX_TEAMS
			AND iRule >= 0
			AND iRule < FMMC_MAX_RULES				
				PRINTLN("[IS_ENTITY_INVOLVED_IN_THIS_TEAMS_RULE] - pedPriority: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iNumber].iPriority[iTeam])
				bOKData = TRUE
				IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iNumber].iPriority[iTeam] = iRule
					bInvolved = TRUE
				ENDIF
			ELSE
				//Data is not valid
			ENDIF
		BREAK
		CASE ciRULE_TYPE_VEHICLE
			IF iNumber >= 0
			AND iNumber < FMMC_MAX_VEHICLES
			AND iTeam >= 0
			AND iTeam < FMMC_MAX_TEAMS
			AND iRule >= 0
			AND iRule < FMMC_MAX_RULES
				PRINTLN("[IS_ENTITY_INVOLVED_IN_THIS_TEAMS_RULE] - vehPriority: ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iNumber].iPriority[iTeam])
				bOKData = TRUE
				IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iNumber].iPriority[iTeam] = iRule
					bInvolved = TRUE
				ENDIF
			ELSE
				//Data is not valid
			ENDIF
		BREAK
		CASE ciRULE_TYPE_OBJECT
			IF iNumber >= 0
			AND iNumber < FMMC_MAX_NUM_OBJECTS
			AND iTeam >= 0
			AND iTeam < FMMC_MAX_TEAMS
			AND iRule >= 0
			AND iRule < FMMC_MAX_RULES
				PRINTLN("[IS_ENTITY_INVOLVED_IN_THIS_TEAMS_RULE] - ObjPriority: ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[iNumber].iPriority[iTeam])
				bOKData = TRUE
				IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iNumber].iPriority[iTeam] = iRule
					bInvolved = TRUE
				ENDIF
			ELSE
				//Data is not valid
			ENDIF
		BREAK
		CASE ciRULE_TYPE_GOTO			
			IF iNumber >= 0
			AND iNumber < FMMC_MAX_GO_TO_LOCATIONS
			AND iTeam >= 0
			AND iTeam < FMMC_MAX_TEAMS
			AND iRule >= 0
			AND iRule < FMMC_MAX_RULES
				PRINTLN("[IS_ENTITY_INVOLVED_IN_THIS_TEAMS_RULE] (Goto) ObjPriority: ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[iNumber].iPriority[iTeam])
				bOKData = TRUE
				IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iNumber].iPriority[iTeam] = iRule
					bInvolved = TRUE
				ENDIF
			ELSE
				//Data is not valid
			ENDIF
		BREAK
	ENDSWITCH
	
	IF bOKData
	AND NOT bInvolved
		
		INT iextraObjectiveNum
		
		// Loop through all potential entites that have multiple rules
		FOR iextraObjectiveNum = 0 TO ( MAX_NUM_EXTRA_OBJECTIVE_ENTITIES-1 )
			
			// If the type matches the one passed
			IF g_FMMC_STRUCT.iExtraObjectiveEntityType[iextraObjectiveNum] = iRuleType
			AND g_FMMC_STRUCT.iExtraObjectiveEntityId[iextraObjectiveNum] = iNumber
				
				INT iextraObjectiveLoop
				
				FOR iextraObjectiveLoop = 0 TO (MAX_NUM_EXTRA_OBJECTIVE_PER_ENTITY - 1)
					
					IF g_FMMC_STRUCT.iExtraObjectivePriority[iextraObjectiveNum][iextraObjectiveLoop][iTeam] = iRule
						bInvolved = TRUE
						//Break out of the loop:
						iextraObjectiveLoop = MAX_NUM_EXTRA_OBJECTIVE_PER_ENTITY
					ENDIF
					
				ENDFOR
				
				//We found the multi-rule number for this entity, there won't be any more so break out of the loop:
				iextraObjectiveNum = MAX_NUM_EXTRA_OBJECTIVE_ENTITIES
				
			ENDIF
		ENDFOR
		
	ENDIF
	
	PRINTLN("[IS_ENTITY_INVOLVED_IN_THIS_TEAMS_RULE] - bInvolved: ", bInvolved)
	
	RETURN bInvolved
	
ENDFUNC

FUNC BOOL SHOULD_FORCE_IMMEDIATE_ENTITY_SPAWN(INT iEntity, INT iType)
	
	IF g_bInMissionControllerCutscene
	OR IS_THIS_RULE_A_CUTSCENE()
		
		INT i = 0
		INT iCutscene = iCutsceneIndexPlaying
		
		PRINTLN("[RCC MISSION] RESPAWN_MISSION_VEHICLE - SHOULD_FORCE_IMMEDIATE_ENTITY_SPAWN - iEntity: ", iEntity, " iType: ", iType, " iCutscene: ", iCutscene, " eCutsceneTypePlaying: ", eCutsceneTypePlaying)
		
		IF iCutscene > -1			
			FOR i = 0 TO FMMC_MAX_CUTSCENE_ENTITIES-1
				IF eCutsceneTypePlaying = FMMCCUT_SCRIPTED
					IF g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].iType = iType
						IF g_FMMC_STRUCT.sScriptedCutsceneData[iCutscene].sCutsceneEntities[i].iIndex = iEntity
							PRINTLN("[RCC MISSION] RESPAWN_MISSION_VEHICLE - SHOULD_FORCE_IMMEDIATE_ENTITY_SPAWN - iEntity: ", iEntity, " returning TRUE (FMMCCUT_SCRIPTED)")
							RETURN TRUE
						ENDIF
					ENDIF
				ELIF eCutsceneTypePlaying = FMMCCUT_MOCAP
					IF g_FMMC_STRUCT.sMocapCutsceneData[iCutscene].sCutsceneEntities[i].iType = iType
						IF g_FMMC_STRUCT.sMocapCutsceneData[iCutscene].sCutsceneEntities[i].iIndex = iEntity
							PRINTLN("[RCC MISSION] RESPAWN_MISSION_VEHICLE - SHOULD_FORCE_IMMEDIATE_ENTITY_SPAWN - iEntity: ", iEntity, " returning TRUE (FMMCCUT_MOCAP)")
							RETURN TRUE
						ENDIF
					ENDIF
				ELIF eCutsceneTypePlaying = FMMCCUT_ENDMOCAP
					IF g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[i].iType = iType
						IF g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[i].iIndex = iEntity
							PRINTLN("[RCC MISSION] RESPAWN_MISSION_VEHICLE - SHOULD_FORCE_IMMEDIATE_ENTITY_SPAWN - iEntity: ", iEntity, " returning TRUE (FMMCCUT_ENDMOCAP)")
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
		ENDIF		
		
		PRINTLN("[RCC MISSION] RESPAWN_MISSION_VEHICLE - SHOULD_FORCE_IMMEDIATE_ENTITY_SPAWN - iEntity: ", iEntity, " trying to respawn during a cutscene but it's not a registered entity so going through normal procedures.")
	ENDIF
		
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PED_PERFORMING_TASK(PED_INDEX tempPed, SCRIPT_TASK_NAME eScriptTask #IF IS_DEBUG_BUILD , BOOL bPrintTaskInfo = FALSE #ENDIF)
	
	SCRIPTTASKSTATUS eTaskStatus = GET_SCRIPT_TASK_STATUS(tempPed, eScriptTask)
	
	#IF IS_DEBUG_BUILD
	IF bPrintTaskInfo
		PRINTLN("[LM][IS_PED_PERFORMING_TASK] - bPrintTaskInfo ------------------")
		IF eTaskStatus = PERFORMING_TASK		PRINTLN("[LM][IS_PED_PERFORMING_TASK][SHOULD_PED_BE_GIVEN_TASK] - PERFORMING_TASK")				ENDIF
		IF eTaskStatus = WAITING_TO_START_TASK	PRINTLN("[LM][IS_PED_PERFORMING_TASK][SHOULD_PED_BE_GIVEN_TASK] - WAITING_TO_START_TASK")		ENDIF
		IF eTaskStatus = FINISHED_TASK			PRINTLN("[LM][IS_PED_PERFORMING_TASK][SHOULD_PED_BE_GIVEN_TASK] - FINISHED_TASK")				ENDIF
		IF eTaskStatus = DORMANT_TASK			PRINTLN("[LM][IS_PED_PERFORMING_TASK][SHOULD_PED_BE_GIVEN_TASK] - DORMANT_TASK")				ENDIF
	ENDIF
	#ENDIF
	
	IF eTaskStatus = PERFORMING_TASK
	OR eTaskStatus = WAITING_TO_START_TASK
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL HAVE_ALL_PLAYERS_LEFT_PLACED_PEDS_VEHICLE(VEHICLE_INDEX vehIndex)
	RETURN NOT IS_ANY_PLAYER_IN_VEHICLE(vehIndex, FALSE)
ENDFUNC

FUNC INT GET_CUSTOM_SCENARIO_INDEX_FOR_PED(INT iPed, BOOL bDoNotAssign = FALSE)
	
	INT i, iLowestFree = -1
	FOR i = 0 TO ciFMMC_MAX_CUSTOM_SCENARIOS_ACTIVE - 1
		IF iAssociatedGOTOCustomScenarioIndex[i] = iPed
			RETURN i
		ENDIF
		
		IF iAssociatedGOTOCustomScenarioIndex[i] = -1
		AND iLowestFree = -1
			iLowestFree = i
		ENDIF
	ENDFOR
	
	IF bDoNotAssign
		RETURN -1
	ENDIF
	
	IF iLowestFree != -1
		iAssociatedGOTOCustomScenarioIndex[iLowestFree] = iPed
		PRINTLN("[CustomScenario][Peds][Ped ", iPed, "] GET_CUSTOM_SCENARIO_INDEX_FOR_PED - Assigning Custom Scenario Index: ", iLowestFree)
		RETURN iLowestFree
	ENDIF
	
	ASSERTLN("[CustomScenario][Peds][Ped ", iPed, "] GET_CUSTOM_SCENARIO_INDEX_FOR_PED - No free scenario indexes!")
	PRINTLN("[CustomScenario][Peds][Ped ", iPed, "] GET_CUSTOM_SCENARIO_INDEX_FOR_PED - No free scenario indexes!")
	#IF IS_DEBUG_BUILD
	ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_PED_GO_TO_TOO_MANY_CUSTOM_SCENARIOS, ENTITY_RUNTIME_ERROR_TYPE_CRITICAL, "Too many concurrent Custom Scenarios")	
	#ENDIF
	
	RETURN -1
	
ENDFUNC

PROC CLEAR_CUSTOM_SCENARIO_DATA_FOR_PED(INT iPed, INT iScenarioIndex = -1)
	
	IF iScenarioIndex = -1
		iScenarioIndex = GET_CUSTOM_SCENARIO_INDEX_FOR_PED(iPed, TRUE)
		IF iScenarioIndex = -1
			EXIT
		ENDIF
	ENDIF
	
	INT i 
	FOR i = 0 TO ciFMMC_MAX_CUSTOM_SCENARIO_TIMERS - 1
		RESET_NET_TIMER(tdAssociatedGOTOCustomScenarioTimer[iScenarioIndex][i])
	ENDFOR
	INITIALISE_INT_ARRAY(iAssociatedGOTOCustomScenarioInt[iScenarioIndex], -1)
	
ENDPROC

PROC RELEASE_PED_CUSTOM_SCENARIO_INDEX(INT iPed, INT iScenarioIndex = -1)
	
	IF iScenarioIndex = -1
		iScenarioIndex = GET_CUSTOM_SCENARIO_INDEX_FOR_PED(iPed, TRUE)
		IF iScenarioIndex = -1
			EXIT
		ENDIF
	ENDIF
	
	PRINTLN("[CustomScenario][Peds][Ped ", iPed, "] RELEASE_PED_CUSTOM_SCENARIO_INDEX - Releasing Custom Scenario Index: ", iScenarioIndex)
	CLEAR_CUSTOM_SCENARIO_DATA_FOR_PED(iPed, iScenarioIndex)
	iAssociatedGOTOCustomScenarioIndex[iScenarioIndex] = -1
	
ENDPROC

PROC CLEAR_PED_ASS_GOTO_DATA(INT iPed)
	PRINTLN("[Peds][Ped ", iPed, "][Task] - CLEAR_PED_ASS_GOTO_DATA - Clearing Data")
			
	INT iBitset = iPed / 32
	INT iBit = iPed % 32	
			
	CLEAR_BIT(iPedArrivedBitset[iBitset], iBit)
	CLEAR_BIT(iPedGotoWaitBitset[iBitset], iBit)				
	CLEAR_BIT(iBSTaskAchieveHeadingWait[iBitset], iBit)
	CLEAR_BIT(iPedGotoAssCompletedWaitBitset[iBitset], iBit)
	CLEAR_BIT(iPedGotoAssCompletedWaitPlayerVehExitBitset[iBitset], iBit)
	CLEAR_BIT(iPedGotoAssCompletedAchieveHeadingBitset[iBitset], iBit)
	CLEAR_BIT(iPedGotoAssCompletedWaitPlayIdleBitset[iBitset], iBit)
	CLEAR_BIT(iPedGotoAssCompletedLeaveVehicleBitset[iBitset], iBit)
	CLEAR_BIT(iPedGotoAssCompletedLandVehicleBitset[iBitset], iBit)
	CLEAR_BIT(iPedGotoAssCompletedRappelFromVehBitset[iBitset], iBit)
	CLEAR_BIT(iPedGotoAssCompletedHoverHeliBitset[iBitset], iBit)
	CLEAR_BIT(iPedGotoAssCompletedEnterVehicleBitset[iBitset], iBit)
	CLEAR_BIT(iPedGotoAssCompletedCombatGotoBitset[iBitset], iBit)	
	CLEAR_BIT(iPedGotoAssCompletedWaitCombatBitset[iBitset], iBit)	
	CLEAR_BIT(iPedGotoAssCompletedCustomScenarioBitset[iBitset], iBit)	
	CLEAR_BIT(iPedGotoAssCompletedDisableLockOnBitset[iBitset], iBit)
	RELEASE_PED_CUSTOM_SCENARIO_INDEX(iPed)
	
ENDPROC

FUNC BOOL SHOULD_PED_BE_GIVEN_TASK(PED_INDEX tempPed, INT iPed, SCRIPT_TASK_NAME eScriptTask = SCRIPT_TASK_INVALID, BOOL bDirtyFlagOverride = FALSE, VEHICLE_MISSION eVehicleMission = MISSION_NONE, VEHICLE_INDEX pedVehForMission = NULL #IF IS_DEBUG_BUILD , BOOL bPrintTaskInfo = FALSE #ENDIF)
	
	#IF IS_DEBUG_BUILD
		IF bMCScriptAIAdditionalPrints
			bPrintTaskInfo = TRUE
		ENDIF
	#ENDIF
	
	IF tempPed = NULL
		ASSERTLN("[RCC MISSION] - SHOULD_PED_BE_GIVEN_TASK - iPed: ", iPed, " tempPed = NULL")
		RETURN FALSE
	ENDIF
	
	IF tempPed = LocalPlayerPed
		ASSERTLN("[RCC MISSION] - SHOULD_PED_BE_GIVEN_TASK - iPed: ", iPed, " tempPed = LocalPlayerPed")
		RETURN FALSE
	ENDIF
	
	IF iPed < 0 OR iPed >= FMMC_MAX_PEDS
		ASSERTLN("[RCC MISSION] - SHOULD_PED_BE_GIVEN_TASK - iPed: ", iPed, " - out of range")
		RETURN FALSE
	ENDIF

	IF eScriptTask = SCRIPT_TASK_INVALID
		ASSERTLN("[RCC MISSION] - SHOULD_PED_BE_GIVEN_TASK - iPed: ", iPed, " eScriptTask = SCRIPT_TASK_INVALID")
		RETURN FALSE
	ENDIF

	IF NOT bDirtyFlagOverride
		IF FMMC_IS_LONG_BIT_SET(sRetaskDirtyFlagData.iDirtyFlagPedNeedsRetask, iPed)
			PRINTLN("[RCC MISSION] SHOULD_PED_BE_GIVEN_TASK - Called on ped ", iPed, " iDirtyFlagPedNeedsRetask so RETURN TRUE")
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF FMMC_IS_LONG_BIT_SET(iPedHitByStunGun, iPed)
		PRINTLN("[RCC MISSION] SHOULD_PED_BE_GIVEN_TASK - Called on ped ", iPed, " iPedHitByStunGun so RETURN FALSE")
		RETURN FALSE
	ENDIF	
	
	IF IS_ASSOCIATED_GOTO_TASK_DATA__WAIT_TIME_SET(MC_serverBD_2.iAssociatedGotoPoolIndex[iPed], g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[iPed])
	AND NOT IS_ASSOCIATED_GOTO_TASK_DATA__WAIT_TIME_FINISHED(MC_serverBD_2.iAssociatedGotoPoolIndex[iPed], g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[iPed], tdAssociatedGOTOWaitIdleTimer[iPed])
		PRINTLN("[RCC MISSION] SHOULD_PED_BE_GIVEN_TASK - Called on ped ", iPed, " Using tdAssociatedGOTOWaitIdleTimer and it has not expired so RETURN FALSE")	
		RETURN FALSE
	ENDIF
	
	IF IS_ASSOCIATED_GOTO_TASK_DATA__A_CUSTOM_SCENARIO(MC_serverBD_2.iAssociatedGotoPoolIndex[iPed], g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[iPed])
	AND GET_CUSTOM_SCENARIO_INDEX_FOR_PED(iPed, TRUE) != -1
		PRINTLN("[RCC MISSION] SHOULD_PED_BE_GIVEN_TASK - Called on ped ", iPed, " Using Custom Scenario and it has not finished so RETURN FALSE")		
		RETURN FALSE
	ENDIF
		
	IF (MC_serverBD_2.iPedGotoProgress[iped] < MAX_ASSOCIATED_GOTO_TASKS)
	AND (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].sAssociatedGotoTaskData.sPointData[MC_serverBD_2.iPedGotoProgress[iped]].iWaitTime > 0)		
		IF eScriptTask = SCRIPT_TASK_START_SCENARIO_IN_PLACE
			IF IS_PED_IN_ANY_VEHICLE(tempPed, TRUE)
				IF NETWORK_HAS_CONTROL_OF_ENTITY(tempPed)
					IF IS_PED_RUNNING_MOBILE_PHONE_TASK(tempPed)
						PRINTLN("[RCC MISSION] SHOULD_PED_BE_GIVEN_TASK - Called on ped ", iPed, " IS_PED_RUNNING_MOBILE_PHONE_TASK so RETURN FALSE")
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF	

	IF IS_PED_PERFORMING_TASK(tempPed, eScriptTask #IF IS_DEBUG_BUILD , bPrintTaskInfo #ENDIF)
		
		// There are actually quite a few different tasks that come under the 'SCRIPT_TASK_VEHICLE_MISSION' task name, but we can check for what type they are if we've passed in the VEHICLE_MISSION type and the ped's vehicle
		IF eScriptTask = SCRIPT_TASK_VEHICLE_MISSION
			IF eVehicleMission != MISSION_NONE
				IF DOES_ENTITY_EXIST(pedVehForMission)
					
					// If the ped is not doing the VEHICLE_MISSION type that we want them to here, then we should retask them with this new type
					IF GET_ACTIVE_VEHICLE_MISSION_TYPE(pedVehForMission) != eVehicleMission
						PRINTLN("[RCC MISSION] SHOULD_PED_BE_GIVEN_TASK - Returning TRUE for ",iped," w SCRIPT_TASK_VEHICLE_MISSION, not on eVehicleMission ",ENUM_TO_INT(eVehicleMission),", but currently on ",ENUM_TO_INT(GET_ACTIVE_VEHICLE_MISSION_TYPE(pedVehForMission)))
						RETURN TRUE
					ENDIF
					
				#IF IS_DEBUG_BUILD
				ELSE
					CASSERTLN(DEBUG_CONTROLLER, "[RCC MISSION] SHOULD_PED_BE_GIVEN_TASK - Called on ped ",iped," with SCRIPT_TASK_VEHICLE_MISSION but no vehicle")
					PRINTLN("[RCC MISSION] SHOULD_PED_BE_GIVEN_TASK - Called on ped ",iped," with SCRIPT_TASK_VEHICLE_MISSION but no vehicle, callstack:")
					DEBUG_PRINTCALLSTACK()
				#ENDIF
				ENDIF
			#IF IS_DEBUG_BUILD
			ELSE
				CASSERTLN(DEBUG_CONTROLLER, "[RCC MISSION] SHOULD_PED_BE_GIVEN_TASK - Called on ped ",iped," with SCRIPT_TASK_VEHICLE_MISSION but no eVehicleMission")
				PRINTLN("[RCC MISSION] SHOULD_PED_BE_GIVEN_TASK - Called on ped ",iped," with SCRIPT_TASK_VEHICLE_MISSION but no eVehicleMission, callstack:")
				DEBUG_PRINTCALLSTACK()
			#ENDIF
			ENDIF
		ENDIF
		
		RETURN FALSE
	ENDIF
	
	PRINTLN("[RCC MISSION] SHOULD_PED_BE_GIVEN_TASK - Called on ped ", iPed, " RETURN TRUE")
		
	RETURN TRUE
	
ENDFUNC

FUNC BOOL CAN_PED_TASKS_BE_CLEARED(PED_INDEX tempPed)

	ENTITY_INDEX tempEntity

	IF IS_PED_IN_WRITHE(tempPed)
	OR IS_PED_EVASIVE_DIVING(tempPed, tempEntity)
		RETURN FALSE
	ENDIF

	RETURN TRUE

ENDFUNC

FUNC BOOL ARE_WE_WAITING_FOR_OBJECTS_TO_CLEAN_UP()
	
	BOOL bWaiting
	
	IF DOES_ANY_TEAM_HAVE_NEW_PRIORITY_THIS_FRAME(TRUE)
	OR IS_BIT_SET(iLocalBoolCheck7, LBOOL7_HOST_APPLIED_NEW_PRIORITY_MIDFRAME)
	OR IS_BIT_SET(iLocalBoolCheck11, LBOOL11_HOST_APPLIED_NEW_MIDPOINT_MIDFRAME)
	OR MC_serverBD.iObjCleanup_NeedOwnershipBS != 0
		bWaiting = TRUE
	ENDIF
	
	RETURN bWaiting
	
ENDFUNC

FUNC BOOL ARE_WE_WAITING_FOR_PEDS_TO_CLEAN_UP()
	
	BOOL bWaiting
	
	IF DOES_ANY_TEAM_HAVE_NEW_PRIORITY_THIS_FRAME(TRUE)
	OR IS_BIT_SET(iLocalBoolCheck7, LBOOL7_HOST_APPLIED_NEW_PRIORITY_MIDFRAME)
	OR IS_BIT_SET(iLocalBoolCheck11, LBOOL11_HOST_APPLIED_NEW_MIDPOINT_MIDFRAME)
	OR MC_serverBD.iPedCleanup_NeedOwnershipBS[0] != 0
	OR MC_serverBD.iPedCleanup_NeedOwnershipBS[1] != 0
	OR MC_serverBD.iPedCleanup_NeedOwnershipBS[2] != 0
		bWaiting = TRUE
	ENDIF
	
	RETURN bWaiting
	
ENDFUNC

FUNC BOOL CAN_SPAWN_AND_PLAY_ANIM_CLOSE_TO_VEHICLE(int iAnimation)
	
	BOOL bSpawn = FALSE
	
	SWITCH iAnimation
		CASE ciPED_IDLE_ANIM__PRISONPLANE_AGENT_B_IDLE
		CASE ciPED_IDLE_ANIM__PRISONPLANE_AGENT_B_REACT
		CASE ciPED_IDLE_ANIM__BIKE_INSPECTION
		CASE ciPED_IDLE_ANIM__BROKEN_CAR
		CASE ciPED_IDLE_ANIM__WELDING_KNEELING
			bSpawn = TRUE
		BREAK
	ENDSWITCH
	
	RETURN bSpawn
ENDFUNC

PROC DISABLE_VEHICLE_ROOFS(VEHICLE_INDEX viVehToUse, MODEL_NAMES mn)
	IF mn = STALION
		INT i = 1
		FOR i = 1 TO ci_MaxVehicleExtras
			IF DOES_EXTRA_EXIST(viVehToUse, i)
				IF IS_VEHICLE_EXTRA_TURNED_ON(viVehToUse, i)
					SET_VEHICLE_EXTRA(viVehToUse,i,TRUE)
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
ENDPROC

PROC APPLY_TEAM_COLOUR_TO_VEHICLE(VEHICLE_INDEX vehToSpawnPlayerIn, INT iPlayerTeam)
	INT iColour = GET_VEHICLE_COLOR_FROM_SELECTION( g_FMMC_STRUCT.iVehicleColour[ iPlayerTeam ] )
	PRINTLN("[RCC MISSION] SPAWN_PLAYER_IN_TEAM_VEHICLE / APPLY_TEAM_COLOUR_TO_VEHICLE - VEHICLE RESPAWN COLOUR - iColour:", iColour )
	

	INT iColour2 = iColour
	IF g_FMMC_STRUCT.iVehicleSecondaryColour[iPlayerTeam] > -1
		PRINTLN("[RCC MISSION] SPAWN_PLAYER_IN_TEAM_VEHICLE / APPLY_TEAM_COLOUR_TO_VEHICLE - VEHICLE RESPAWN SECONDARY COLOUR - iColour:", g_FMMC_STRUCT.iVehicleSecondaryColour[iPlayerTeam])
		iColour2 = GET_VEHICLE_COLOR_FROM_SELECTION( g_FMMC_STRUCT.iVehicleSecondaryColour[iPlayerTeam])
	ENDIF
	SET_VEHICLE_COLOURS( vehToSpawnPlayerIn, iColour, iColour2 )
	SET_VEHICLE_EXTRA_COLOURS( vehToSpawnPlayerIn, iColour, iColour2 )
	
	CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] SPAWN_PLAYER_IN_TEAM_VEHICLE / APPLY_TEAM_COLOUR_TO_VEHICLE - Setting spawn vehicle primary and secondary colours to iColour: ", iColour , " iColour2: ", iColour2)
	
ENDPROC

//Should be cached
FUNC BOOL IS_HOTWIRE_BEING_USED()
	INT i
	FOR i = 0 TO FMMC_MAX_NUM_OBJECTS - 1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectMinigame = ciFMMC_OBJECT_MINIGAME_TYPE__HOTWIRE
			RETURN TRUE
		ENDIF
	ENDFOR
	
	FOR i = 0 TO FMMC_MAX_NUM_INTERACTABLES - 1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].iInteractable_InteractionType = ciInteractableInteraction_HackingMinigameA
			RETURN TRUE
		ENDIF
	ENDFOR
	
	RETURN FALSE		
ENDFUNC

//Should be cached
FUNC BOOL IS_BEAM_HACK_BEING_USED()
	INT i
	FOR i = 0 TO FMMC_MAX_NUM_OBJECTS - 1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectMinigame = ciFMMC_OBJECT_MINIGAME_TYPE__BEAM_HACK
		OR g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectMinigame = ciFMMC_OBJECT_MINIGAME_TYPE__BEAMHACK_VEHICLE
			RETURN TRUE
		ENDIF
	ENDFOR
	
	RETURN FALSE		
ENDFUNC

//Should be cached
FUNC BOOL IS_ORDER_UNLOCK_BEING_USED()
	INT i
	FOR i = 0 TO FMMC_MAX_NUM_OBJECTS - 1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectMinigame = ciFMMC_OBJECT_MINIGAME_TYPE__ORDER_UNLOCK
			RETURN TRUE
		ENDIF
	ENDFOR
	
	RETURN FALSE		
ENDFUNC

//Should be cached
FUNC BOOL IS_FINGERPRINT_CLONE_BEING_USED()
	INT i
	FOR i = 0 TO FMMC_MAX_NUM_OBJECTS - 1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectMinigame = ciFMMC_OBJECT_MINIGAME_TYPE__FINGERPRINT_CLONE
			RETURN TRUE
		ENDIF
	ENDFOR
	
	RETURN FALSE		
ENDFUNC

//Should be cached
FUNC BOOL IS_ORBITAL_CANNON_BEING_USED()
	INT i
	FOR i = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfObjects - 1)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iOrbitalCannonIndex > -1
			RETURN TRUE
		ENDIF
	ENDFOR
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_ANY_VEHICLE_SWAPS_BEEN_SET()
		
	IF IS_BIT_SET(iLocalBoolCheck10, LBOOL10_VEHICLE_SWAPS_CHECKED)
		RETURN IS_BIT_SET(iLocalBoolCheck10, LBOOL10_VEHICLE_SWAPS_USED)
	ENDIF
	
	SET_BIT(iLocalBoolCheck10, LBOOL10_VEHICLE_SWAPS_CHECKED)
	
	INT iTeam = 0
	INT iRule = 0
	
	FOR iTeam = 0 TO FMMC_MAX_TEAMS-1
		FOR iRule = 0 TO FMMC_MAX_RULES-1
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEight[iRule], ciBS_RULE8_VEHICLE_SWAP_ENABLED)
				SET_BIT(iLocalBoolCheck10, LBOOL10_VEHICLE_SWAPS_USED)
				PRINTLN("[LM][MISSION][PROCESS_SWAP_VEHICLE_ON_RULE] - Using Vehicle Swaps.")
				RETURN TRUE
			ENDIF
		ENDFOR
	ENDFOR
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_VOLTAGE_BEING_USED()
	
	IF IS_BIT_SET(iLocalBoolCheck10, LBOOL10_VOLTAGE_HACK_CHECKED)
		RETURN IS_BIT_SET(iLocalBoolCheck10, LBOOL10_VOLTAGE_HACK_USED)
	ENDIF
	
	SET_BIT(iLocalBoolCheck10, LBOOL10_VOLTAGE_HACK_CHECKED)
	
	INT i	
	FOR i = 0 TO FMMC_MAX_NUM_INTERACTABLES - 1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].iInteractable_InteractionType = ciInteractableInteraction_VoltageMinigame
			SET_BIT(iLocalBoolCheck10, LBOOL10_VOLTAGE_HACK_USED)
			RETURN TRUE
		ENDIF
	ENDFOR
	
	RETURN FALSE		
ENDFUNC

FUNC BOOL IS_FINGERPRINT_HACK_BEING_USED()

	IF IS_BIT_SET(iLocalBoolCheck10, LBOOL10_FINGERPRINT_HACK_CHECKED)
		RETURN IS_BIT_SET(iLocalBoolCheck10, LBOOL10_FINGERPRINT_HACK_USED)
	ENDIF
	
	SET_BIT(iLocalBoolCheck10, LBOOL10_FINGERPRINT_HACK_CHECKED)
	
	INT i	
	FOR i = 0 TO FMMC_MAX_NUM_INTERACTABLES - 1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].iInteractable_InteractionType = ciInteractableInteraction_FingerPrintMinigame
			SET_BIT(iLocalBoolCheck10, LBOOL10_FINGERPRINT_HACK_USED)
			RETURN TRUE
		ENDIF
	ENDFOR
	
	RETURN FALSE	
	
ENDFUNC

FUNC BOOL IS_GRATE_CUTTING_BEING_USED()
	
	IF IS_BIT_SET(iLocalBoolCheck10, LBOOL10_GRATE_CUTTING_CHECKED)
		RETURN IS_BIT_SET(iLocalBoolCheck10, LBOOL10_GRATE_CUTTING_USED)
	ENDIF
	
	SET_BIT(iLocalBoolCheck10, LBOOL10_GRATE_CUTTING_CHECKED)
	
	INT i	
	FOR i = 0 TO FMMC_MAX_NUM_OBJECTS - 1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectMinigame = ciFMMC_OBJECT_MINIGAME_TYPE__WELD_UNDERWATER_TUNNEL
			SET_BIT(iLocalBoolCheck10, LBOOL10_GRATE_CUTTING_USED)
			RETURN TRUE
		ENDIF
	ENDFOR
	
	RETURN FALSE	
		
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_ABLE_TO_DO_LEADER_RESTRICTED_INTERACTION()
	
	//Heist leader only - unless they're dead/invalid
	
	PLAYER_INDEX piLobbyLeader = GET_FMMC_LOBBY_LEADER()
	
	IF piLobbyLeader = INVALID_PLAYER_INDEX()
		RETURN TRUE
	ENDIF
	
	IF piLobbyLeader = LocalPlayer
		RETURN TRUE
	ENDIF
	
	IF NOT NETWORK_IS_PLAYER_ACTIVE(piLobbyLeader)
		RETURN TRUE
	ENDIF
	
	IF NOT NETWORK_IS_PLAYER_A_PARTICIPANT(piLobbyLeader)
		RETURN TRUE
	ENDIF
	
	PARTICIPANT_INDEX piPart = NETWORK_GET_PARTICIPANT_INDEX(piLobbyLeader)
	INT iLeader = NATIVE_TO_INT(piPart)
	
	IF iLeader != -1
	AND IS_BIT_SET(MC_playerBD[iLeader].iClientBitSet, PBBOOL_PLAYER_OUT_OF_LIVES)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC


PROC ASSIGN_GANG_BOSS_TYPE()
	IF iBossType != -1
		EXIT
	ENDIF
	
	IF NOT IS_THIS_A_GANG_MISSION()
		EXIT
	ENDIF
	
	PLAYER_INDEX bossIndex = GB_GET_LOCAL_PLAYER_GANG_BOSS()
	IF bossIndex = INVALID_PLAYER_INDEX()
		EXIT
	ENDIF
	
	IF NETWORK_IS_PLAYER_ACTIVE(bossIndex)
	AND NETWORK_IS_PLAYER_A_PARTICIPANT(bossIndex)
		IF GB_IS_PLAYER_BOSS_OF_A_GANG_TYPE(bossIndex, GT_BIKER)
			iBossType = ciBOSSTYPE_MC_PRESIDENT
		ELSE
			iBossType = ciBOSSTYPE_CEO
		ENDIF
		
		PRINTLN("[BOSSTYPE] iBossType = ", iBossType)
	ENDIF
ENDPROC 

PROC PREVENT_COLLISIONS_WITH_NEARBY_VEHICLES_FOR_THIS_VEHICLE(VEHICLE_INDEX &viVehicleToIgnore)
	    
	VEHICLE_INDEX VehicleID[16] 
	INT iNumVehicles = GET_PED_NEARBY_VEHICLES(LocalPlayerPed, VehicleID)
	PRINTLN("PREVENT_COLLISIONS_WITH_NEARBY_VEHICLES_FOR_THIS_VEHICLE = iNumVehicles = ", iNumVehicles)
	
	INT iNearbyVehicle = 0
	FOR iNearbyVehicle = 0 TO (iNumVehicles - 1)
		
		IF viVehicleToIgnore = VehicleID[iNearbyVehicle]
			RELOOP
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(viVehicleToIgnore)
			RELOOP
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(VehicleID[iNearbyVehicle])
			RELOOP
		ENDIF
		
		SET_ENTITY_NO_COLLISION_ENTITY(viVehicleToIgnore, VehicleID[iNearbyVehicle], FALSE)
		PRINTLN("PREVENT_COLLISIONS_WITH_NEARBY_VEHICLES_FOR_THIS_VEHICLE = Preventing Collision with iNearbyVehicle: ", iNearbyVehicle)
			
	ENDFOR
	
ENDPROC

PROC DELETE_ANY_EMPTY_NEARBY_VEHICLES(BOOL bOnlyDeleteIfAlive = FALSE, BOOL bOnlyDeleteBikesIfFd = FALSE, BOOL bDontDeleteBikes = FALSE)
	      
	// check any nearby vehicles
	VEHICLE_INDEX VehicleID[64] 
	INT iNumVehicles = GET_PED_NEARBY_VEHICLES(LocalPlayerPed, VehicleID)
	PRINTLN("DELETE_ANY_EMPTY_NEARBY_VEHICLES = iNumVehicles = ", iNumVehicles, "  || bOnlyDeleteIfAlive: ", bOnlyDeleteIfAlive, " | bOnlyDeleteBikesIfFd: ", bOnlyDeleteBikesIfFd, " | bDontDeleteBikes: ", bDontDeleteBikes)
	
	INT iNearbyVehicle = 0
	BOOL bSetAsMissionEntity
	
	FOR iNearbyVehicle = 0 TO (iNumVehicles - 1)
		IF NOT DOES_ENTITY_EXIST(VehicleID[iNearbyVehicle])
			RELOOP
		ENDIF
		
		IF NOT IS_VEHICLE_EMPTY(VehicleID[iNearbyVehicle], FALSE, DEFAULT, DEFAULT, TRUE)  
			RELOOP
		ENDIF
	
		IF bOnlyDeleteIfAlive
			IF NOT IS_ENTITY_ALIVE(VehicleID[iNearbyVehicle])
				PRINTLN("DELETE_ANY_EMPTY_NEARBY_VEHICLES - Relooping because the vehicle is dead and bOnlyDeleteIfAlive is set. Vehicle: ", iNearbyVehicle)
				RELOOP
			ENDIF
		ENDIF
		
		IF bOnlyDeleteBikesIfFd
			IF NOT IS_VEHICLE_FUCKED_MP(VehicleID[iNearbyVehicle])
			AND (IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(VehicleID[iNearbyVehicle])) OR IS_THIS_MODEL_A_BICYCLE(GET_ENTITY_MODEL(VehicleID[iNearbyVehicle])))
				PRINTLN("DELETE_ANY_EMPTY_NEARBY_VEHICLES - Relooping because the bike is not f'd and bOnlyDeleteBikesIfFd is set. Vehicle: ", iNearbyVehicle)
				RELOOP
			ENDIF
		ENDIF
		
		IF bDontDeleteBikes
			IF IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(VehicleID[iNearbyVehicle]))
			OR IS_THIS_MODEL_A_BICYCLE(GET_ENTITY_MODEL(VehicleID[iNearbyVehicle]))
				PRINTLN("DELETE_ANY_EMPTY_NEARBY_VEHICLES - Relooping because the vehicle is not a bike and bDontDeleteBikes is set. Vehicle: ", iNearbyVehicle)
				RELOOP
			ENDIF
		ENDIF
		
		IF CAN_EDIT_THIS_ENTITY(VehicleID[iNearbyVehicle], bSetAsMissionEntity)
			PRINTLN("DELETE_ANY_EMPTY_NEARBY_VEHICLES - Deleting vehicle now")
			DELETE_VEHICLE(VehicleID[iNearbyVehicle])
		ELSE
			PRINTLN("DELETE_ANY_EMPTY_NEARBY_VEHICLES - Cannot edit")
			
			IF bSetAsMissionEntity
			    SET_VEHICLE_AS_NO_LONGER_NEEDED(VehicleID[iNearbyVehicle])
			    PRINTLN("DELETE_ANY_EMPTY_NEARBY_VEHICLES - Marking as no longer needed")
			ENDIF                               
		ENDIF
	ENDFOR
ENDPROC

PROC PROCESS_DELETE_ANY_EMPTY_NEARBY_VEHICLES(INT iTeam)

	IF NOT bLocalPlayerPedOK
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciCLEAR_UP_VEHICLE_ON_GAME_LEAVE)
		EXIT
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitSet3, ciBS3_ENABLE_TEAM_VEHICLE_ALLOW_VEHICLE_SEAT_SWAPPING)
		EXIT
	ENDIF
	
	DELETE_ANY_EMPTY_NEARBY_VEHICLES(IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySix, ciOptionsBS26_OnlyDeleteAliveEmptyVehicles))
	
ENDPROC 

FUNC BOOL IS_WHOLE_GROUP_IN_VEHICLE(GROUP_INDEX giGroup, VEHICLE_INDEX tempVeh, INT& iFollowersInVehCount)
	
	BOOL bInVehicle = FALSE
	
	INT iHasLeader, iFollowers, iFollowersInVeh
	PED_INDEX leaderPed
	PED_INDEX tempPed
	
	IF DOES_GROUP_EXIST(giGroup)
		
		GET_GROUP_SIZE(giGroup, iHasLeader, iFollowers)
		
		IF iHasLeader >= 1
			
			leaderPed = GET_PED_AS_GROUP_LEADER(giGroup)
			
			IF NOT IS_PED_INJURED(leaderPed)
				
				IF IS_PED_IN_VEHICLE(leaderPed, tempVeh)
					
					IF iFollowers > 0
						
						INT i
						
						FOR i = 0 TO (iFollowers-1)
							tempPed = GET_PED_AS_GROUP_MEMBER(giPlayerGroup,i)
							
							IF NOT IS_PED_INJURED(tempPed)
								IF IS_PED_IN_VEHICLE(tempPed,tempVeh)
									iFollowersInVeh++
									iFollowersInVehCount++
								ENDIF
							ELSE
								iFollowersInVeh++ // Dead peds aren't going to get in the car, never mind about those guys
							ENDIF
						ENDFOR
						
						IF iFollowersInVeh >= iFollowers
							bInVehicle = TRUE
						ENDIF
					ELSE
						bInVehicle = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF
	
	RETURN bInVehicle
	
ENDFUNC

FUNC BOOL SHUT_VEHICLE_DOORS(VEHICLE_INDEX tempveh)
	
	IF IS_VEHICLE_DRIVEABLE(tempveh)
	
		PRINTLN("[RCC MISSION] SHUT_VEHICLE_DOORS called on veh: ",NATIVE_TO_INT(tempveh))
		
		IF NOT IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(tempveh))
			IF NOT HAS_NET_TIMER_STARTED(tddoorsafteytimer)
				START_NET_TIMER(tddoorsafteytimer)
			ELSE
				IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tddoorsafteytimer) < 2000
					IF GET_VEHICLE_DOOR_ANGLE_RATIO(tempveh, SC_DOOR_FRONT_LEFT) > 0
						SET_VEHICLE_DOOR_CONTROL(tempveh, SC_DOOR_FRONT_LEFT, DT_DOOR_INTACT, 0.0)
						RETURN FALSE
					ENDIF
					IF GET_VEHICLE_DOOR_ANGLE_RATIO(tempveh, SC_DOOR_FRONT_RIGHT) > 0
						SET_VEHICLE_DOOR_CONTROL(tempveh, SC_DOOR_FRONT_RIGHT, DT_DOOR_INTACT, 0.0)
						RETURN FALSE
					ENDIF
					IF GET_VEHICLE_DOOR_ANGLE_RATIO(tempveh, SC_DOOR_REAR_LEFT) > 0
						SET_VEHICLE_DOOR_CONTROL(tempveh, SC_DOOR_REAR_LEFT, DT_DOOR_INTACT, 0.0)
						RETURN FALSE
					ENDIF
					IF GET_VEHICLE_DOOR_ANGLE_RATIO(tempveh, SC_DOOR_REAR_RIGHT) > 0
						SET_VEHICLE_DOOR_CONTROL(tempveh, SC_DOOR_REAR_RIGHT, DT_DOOR_INTACT, 0.0)
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF
		RESET_NET_TIMER(tddoorsafteytimer)
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL DOES_FMMC_NET_ENTITY_EXIST(INT iEntityType, INT iEntityID)
	
	NETWORK_INDEX niEnt
	
	IF iEntityID >= 0
		SWITCH iEntityType
			CASE ci_TARGET_PED
				IF iEntityID < FMMC_MAX_PEDS
					niEnt = MC_serverBD_1.sFMMC_SBD.niPed[iEntityID]
				ENDIF
			BREAK
			CASE ci_TARGET_VEHICLE
				IF iEntityID < FMMC_MAX_VEHICLES
					niEnt = MC_serverBD_1.sFMMC_SBD.niVehicle[iEntityID]
				ENDIF
			BREAK
			CASE ci_TARGET_OBJECT
				IF iEntityID < FMMC_MAX_NUM_OBJECTS
					niEnt = GET_OBJECT_NET_ID(iEntityID)
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
	
	RETURN NETWORK_DOES_NETWORK_ID_EXIST(niEnt)
	
ENDFUNC

FUNC BOOL CLEAR_LAST_VEHICLE_INFO()
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySix, ciOptionsBS26_DontClearLastVehicleInfo)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC GET_LAST_VEHICLE_INFO(BOOL bRespawnIntoLastVehicleModel)
	IF NOT IS_PED_DRIVING_ANY_VEHICLE( LocalPlayerPed ) AND NOT bRespawnIntoLastVehicleModel
	AND NOT IS_PLAYER_RESPAWNING( LocalPlayer )
	AND CLEAR_LAST_VEHICLE_INFO()
		IF sLastVehicleInformation.mnModel != DUMMY_MODEL_FOR_SCRIPT	
			sLastVehicleInformation.mnModel = DUMMY_MODEL_FOR_SCRIPT
			sLastVehicleInformation.iVehicleLivery = -1
			sLastVehicleInformation.iVehicleColour1 = -1
			sLastVehicleInformation.iVehicleColour2 = -1
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] [MANUAL RESPAWN] Clearing sLastVehicleInformation as we're out of a vehicle" )
		ENDIF
	ENDIF
	
	IF IS_PED_DRIVING_ANY_VEHICLE(LocalPlayerPed)
	AND NOT IS_BIT_SET(iLocalBoolCheck4, LBOOL4_WAIT_FOR_RESPAWN_IN_VEH)
		
		BOOL bCanUpdateVehicle
		VEHICLE_INDEX vehPedIsIn = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
		
		
		bCanUpdateVehicle = sLastVehicleInformation.mnModel = DUMMY_MODEL_FOR_SCRIPT

		
		IF bCanUpdateVehicle
			
			sLastVehicleInformation.iVehicleLivery = GET_VEHICLE_LIVERY(vehPedIsIn)
			sLastVehicleInformation.mnModel = GET_ENTITY_MODEL(vehPedIsIn)
			
			GET_VEHICLE_COLOURS( vehPedIsIn, 
								sLastVehicleInformation.iVehicleColour1, 
								sLastVehicleInformation.iVehicleColour2 )
			
			STORE_VEHICLE_SPAWN_INFO_FROM_THIS_CAR(vehPedIsIn, GET_GAMER_HANDLE_PLAYER(LocalPlayer))
			
			CPRINTLN(DEBUG_CONTROLLER, "[RCC MISSION] [MANUAL RESPAWN] [LVEH] - Getting vehicle livery: ", sLastVehicleInformation.iVehicleLivery)
			CPRINTLN(DEBUG_CONTROLLER, "[RCC MISSION] [MANUAL RESPAWN] [LVEH] - Getting vehicle model: ", FMMC_GET_MODEL_NAME_FOR_DEBUG(sLastVehicleInformation.mnModel))
			CPRINTLN(DEBUG_CONTROLLER, "[RCC MISSION] [MANUAL RESPAWN] [LVEH] - Getting vehicle colour 1: ", sLastVehicleInformation.iVehicleColour1)
			CPRINTLN(DEBUG_CONTROLLER, "[RCC MISSION] [MANUAL RESPAWN] [LVEH] - Getting vehicle colour 2: ", sLastVehicleInformation.iVehicleColour2)
		ENDIF
	ENDIF
	
	IF NOT bRespawnIntoLastVehicleModel
	AND NOT IS_BIT_SET( iLocalBoolCheck4, LBOOL4_WAIT_FOR_RESPAWN_IN_VEH )
	AND IS_BIT_SET(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_SPAWN_IN_PLACED_VEHICLE )
		IF NETWORK_DOES_NETWORK_ID_EXIST(netRespawnVehicle)
			VEHICLE_INDEX vehPedShouldBeIn = NET_TO_VEH(netRespawnVehicle)
				
			IF IS_ENTITY_ALIVE(vehPedShouldBeIn)
				IF sLastVehicleInformation.mnModel = DUMMY_MODEL_FOR_SCRIPT
					sLastVehicleInformation.mnModel = g_FMMC_STRUCT.mnVehicleModel[MC_playerBD[iPartToUse].iteam]
					
					GET_VEHICLE_COLOURS( vehPedShouldBeIn, 
										sLastVehicleInformation.iVehicleColour1, 
										sLastVehicleInformation.iVehicleColour2 ) //get team colours
					
					STORE_VEHICLE_SPAWN_INFO_FROM_THIS_CAR(vehPedShouldBeIn, GET_GAMER_HANDLE_PLAYER(LocalPlayer))
					
					CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] [MANUAL RESPAWN] [LVEH] - Setting Team Vehicle")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_PED_IN_UNDRIVEABLE_VEHICLE_IN_WATER( PED_INDEX ped )
	BOOL bResult = FALSE
	IF( IS_PED_IN_ANY_VEHICLE( ped, FALSE ) )
		VEHICLE_INDEX veh = GET_VEHICLE_PED_IS_IN( ped, FALSE )
		IF( IS_PED_SITTING_IN_VEHICLE( ped, veh ) )
			IF( NOT IS_VEHICLE_DRIVEABLE( veh, FALSE ) )
				IF( IS_ENTITY_IN_WATER( veh ) )
					bResult = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN bResult
ENDFUNC

///PURPOSE: This function returns the current closest location to the player if they're alive
///    if they've just died, it uses the cached value stored in vLastPlayersPos, currently set up 
///    on player death in PROCESS_PLAYER_LIVES
FUNC VECTOR FIND_CLOSEST_CURRENT_LOCATION()

	VECTOR vClosestLocationPos = <<0,0,0>>
	INT iLoc
	FLOAT fMaxDistance = 999999.0
	VECTOR vPlayerPos

	FOR iLoc = 0 TO (MC_serverBD.iNumLocCreated-1)
		IF MC_serverBD_4.iGotoLocationDataRule[iLoc][MC_playerBD[iLocalPart].iteam] != FMMC_OBJECTIVE_LOGIC_NONE
		AND MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam] < FMMC_MAX_RULES
			IF MC_serverBD_4.iGotoLocationDataPriority[iLoc][MC_playerBD[iLocalPart].iteam] <= MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam]
				IF bLocalPlayerOK
					vPlayerPos = GET_PLAYER_COORDS(LocalPlayer)
				ELSE // Else we use a cached value - this is currently only set up on player death in PROCESS_PLAYER_LIVES
					vPlayerPos = vLastPlayersPos 
				ENDIF
				
				IF GET_DISTANCE_BETWEEN_COORDS(vPlayerPos, GET_LOCATION_VECTOR(iLoc)) < fMaxDistance
					vClosestLocationPos = GET_LOCATION_VECTOR(iLoc)
					fMaxDistance = GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(LocalPlayer), GET_LOCATION_VECTOR(iLoc))
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN vClosestLocationPos
ENDFUNC

FUNC VECTOR FIND_CLOSEST_CURRENT_PED()

	VECTOR vspawnheading
	PED_INDEX tempPed
	INT iPed
	FLOAT fMaxDistance = 999999.0

	FOR iPed = 0 TO (MC_serverBD.iNumPedCreated-1)
		IF MC_serverBD_4.iPedRule[iPed][MC_playerBD[iLocalPart].iteam] != FMMC_OBJECTIVE_LOGIC_NONE
		AND MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam] < FMMC_MAX_RULES
			IF MC_serverBD_4.iPedPriority[iPed][MC_playerBD[iLocalPart].iteam] <= MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam]
				IF NOT IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[iPed])
					IF NOT FMMC_IS_LONG_BIT_SET(MC_serverBD.iPedAtYourHolding[MC_playerBD[iLocalPart].iteam], iped)
						tempPed = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iPed])
						IF GET_DISTANCE_BETWEEN_ENTITIES(LocalPlayerPed,tempPed) < fMaxDistance
							vspawnheading = GET_ENTITY_COORDS(tempPed)
							fMaxDistance = GET_DISTANCE_BETWEEN_ENTITIES(LocalPlayerPed,tempPed)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
RETURN vspawnheading
	
ENDFUNC

FUNC VECTOR FIND_CLOSEST_CURRENT_VEH()

	VECTOR vspawnheading
	VEHICLE_INDEX tempVeh
	INT iVeh
	FLOAT fMaxDistance = 999999.0

	FOR iVeh = 0 TO (MC_serverBD.iNumVehCreated-1)
		IF MC_serverBD_4.ivehRule[iVeh][MC_playerBD[iLocalPart].iteam] != FMMC_OBJECTIVE_LOGIC_NONE
		AND MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam]  < FMMC_MAX_RULES
			IF MC_serverBD_4.iVehPriority[iVeh][MC_playerBD[iLocalPart].iteam] <= MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam] 
				IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
					IF IS_VEHICLE_DRIVEABLE(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh]))
						IF NOT IS_BIT_SET(MC_serverBD.iVehAtYourHolding[MC_playerBD[iPartToUse].iteam],iVeh)
							tempVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
							IF GET_DISTANCE_BETWEEN_ENTITIES(LocalPlayerPed,tempVeh) < fMaxDistance
								vspawnheading = GET_ENTITY_COORDS(tempVeh)
								fMaxDistance = GET_DISTANCE_BETWEEN_ENTITIES(LocalPlayerPed,tempVeh)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
RETURN vspawnheading
	
ENDFUNC

FUNC VECTOR FIND_CLOSEST_CURRENT_OBJ()

	VECTOR vspawnheading
	OBJECT_INDEX tempObj
	INT iObj
	FLOAT fMaxDistance = 999999.0

	FOR iObj = 0 TO (MC_serverBD.iNumObjCreated-1)
		IF MC_serverBD_4.iObjRule[iObj][MC_playerBD[iLocalPart].iteam] != FMMC_OBJECTIVE_LOGIC_NONE
		AND MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam]  < FMMC_MAX_RULES
			IF MC_serverBD_4.iObjPriority[iObj][MC_playerBD[iLocalPart].iteam] <= MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam] 
				IF NETWORK_DOES_NETWORK_ID_EXIST(GET_OBJECT_NET_ID(iObj))
					IF NOT IS_BIT_SET(MC_serverBD.iObjAtYourHolding[MC_playerBD[iPartToUse].iteam],iObj)
						tempObj = NET_TO_OBJ(GET_OBJECT_NET_ID(iObj))
						IF GET_DISTANCE_BETWEEN_ENTITIES(LocalPlayerPed,tempObj) < fMaxDistance
							vspawnheading = GET_ENTITY_COORDS(tempObj)
							fMaxDistance = GET_DISTANCE_BETWEEN_ENTITIES(LocalPlayerPed,tempObj)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN vspawnheading
	
ENDFUNC

//PURPOSE: Finds closest player on the given team. If -1 is passed in, it will find the closest player on any team except the local player's.
FUNC PED_INDEX GET_CLOSEST_PLAYER_IN_TEAM(INT iTeam = -1)
	
	PED_INDEX piClosestPlayer = NULL
	FLOAT fMaxDistance = 999999.0
	
	INT iParticipant = -1
	DO_PARTICIPANT_LOOP_FLAGS eFlags = (DPLF_CHECK_PLAYER_OK | DPLF_FILL_PLAYER_IDS)
	
	IF iTeam > -1
		eFlags = eFlags | GET_PARTICIPANT_LOOP_FLAGS_TO_CHECK_SINGLE_TEAM(iTeam)
	ENDIF
	
	WHILE DO_PARTICIPANT_LOOP(iParticipant, eFlags)
		PRINTLN("[RCC MISSION] GET_CLOSEST_PLAYER_IN_TEAM | Checking iParticipant = ", iParticipant)
		
		IF iTeam = -1
			// Find closest player of any team except the local player's team
			IF MC_playerBD[iParticipant].iTeam != MC_playerBD[iPartToUse].iTeam
			AND NOT DOES_TEAM_LIKE_TEAM(MC_playerBD[iParticipant].iTeam, MC_playerBD[iPartToUse].iTeam)
				FLOAT fDistance = GET_VDIST2_DISTANCE_BETWEEN_PLAYERS(PlayerToUse, piParticipantLoop_PlayerIndex)
				IF fDistance < POW(fMaxDistance, 2.0)
					PRINTLN("[RCC MISSION] GET_CLOSEST_PLAYER_IN_TEAM | Found match A iParticipant = ", iParticipant)
					piClosestPlayer = piParticipantLoop_PedIndex
					fMaxDistance = fDistance
				ENDIF
			ENDIF
		ELSE
			FLOAT fDistance = GET_VDIST2_DISTANCE_BETWEEN_PLAYERS(PlayerToUse, piParticipantLoop_PlayerIndex)
			IF fDistance < POW(fMaxDistance, 2.0)
				PRINTLN("[RCC MISSION] GET_CLOSEST_PLAYER_IN_TEAM | Found match B iParticipant = ", iParticipant)
				piClosestPlayer = piParticipantLoop_PedIndex
				fMaxDistance = fDistance
			ENDIF
		ENDIF
	ENDWHILE
	
	RETURN piClosestPlayer
	
ENDFUNC

FUNC VECTOR FIND_CLOSEST_OBJECTIVE_PLAYER_COORDS()

	PED_INDEX piClosestPlayerPed = NULL
	
	IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
		IF MC_serverBD_4.iPlayerRule[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]][MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_KILL_PLAYERS
			piClosestPlayerPed = GET_CLOSEST_PLAYER_IN_TEAM(-1)
		ELIF MC_serverBD_4.iPlayerRule[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]][MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_KILL_TEAM0
			piClosestPlayerPed = GET_CLOSEST_PLAYER_IN_TEAM(0)
		ELIF MC_serverBD_4.iPlayerRule[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]][MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_KILL_TEAM1
			piClosestPlayerPed = GET_CLOSEST_PLAYER_IN_TEAM(1)
		ELIF MC_serverBD_4.iPlayerRule[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]][MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_KILL_TEAM2
			piClosestPlayerPed = GET_CLOSEST_PLAYER_IN_TEAM(2)
		ELIF MC_serverBD_4.iPlayerRule[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]][MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_KILL_TEAM3
			piClosestPlayerPed = GET_CLOSEST_PLAYER_IN_TEAM(3)
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(piClosestPlayerPed)
		RETURN GET_ENTITY_COORDS(piClosestPlayerPed, FALSE)
	ELSE
		RETURN <<0,0,0>>
	ENDIF

ENDFUNC

FUNC VECTOR GET_COORDS_OF_CLOSEST_OBJECTIVE()

	VECTOR vClosestObjectiveCoords = <<0,0,0>>

	IF MC_serverBD.iNumPlayerRuleHighestPriority[MC_playerBD[iLocalPart].iteam] > 0
		PRINTLN("[RCC MISSION] GET_COORDS_OF_CLOSEST_OBJECTIVE | Using MC_serverBD.iNumPlayerRuleHighestPriority")
		vClosestObjectiveCoords = FIND_CLOSEST_OBJECTIVE_PLAYER_COORDS()
	ENDIF
	
	IF MC_serverBD.iNumLocHighestPriority[MC_playerBD[iLocalPart].iteam] > 0
		PRINTLN("[RCC MISSION] GET_COORDS_OF_CLOSEST_OBJECTIVE | Using MC_serverBD.iNumLocHighestPriority")
		vClosestObjectiveCoords = FIND_CLOSEST_CURRENT_LOCATION()
	ENDIF

	IF MC_serverBD.iNumPedHighestPriority[MC_playerBD[iLocalPart].iteam] > 0
		PRINTLN("[RCC MISSION] GET_COORDS_OF_CLOSEST_OBJECTIVE | Using MC_serverBD.iNumPedHighestPriority")
		vClosestObjectiveCoords = FIND_CLOSEST_CURRENT_PED()
	ENDIF

	IF MC_serverBD.iNumVehHighestPriority[MC_playerBD[iLocalPart].iteam] > 0
		PRINTLN("[RCC MISSION] GET_COORDS_OF_CLOSEST_OBJECTIVE | Using MC_serverBD.iNumVehHighestPriority")
		vClosestObjectiveCoords = FIND_CLOSEST_CURRENT_VEH()
	ENDIF

	IF MC_serverBD.iNumObjHighestPriority[MC_playerBD[iLocalPart].iteam] > 0
		PRINTLN("[RCC MISSION] GET_COORDS_OF_CLOSEST_OBJECTIVE | Using MC_serverBD.iNumObjHighestPriority")
		vClosestObjectiveCoords = FIND_CLOSEST_CURRENT_OBJ()
	ENDIF

	RETURN vClosestObjectiveCoords
ENDFUNC

FUNC VECTOR GET_COORDS_OF_CLOSEST_ENEMY_PED(VECTOR vCheckPos, FLOAT fMaxDistance = 999999.0, BOOL bMustBeAlive = TRUE, PED_INDEX piCustomPedToCheck = NULL)
	VECTOR vClosestPedCoords
	PED_INDEX piRelationshipPedToCheck = LocalPlayerPed
	
	IF piCustomPedToCheck != NULL
		piRelationshipPedToCheck = piCustomPedToCheck
	ENDIF
	
	INT iPed
	FOR iPed = 0 TO MC_serverBD.iNumPedCreated - 1
	
		PED_INDEX piPed = GET_FMMC_ENTITY_PED(iPed)
		
		IF NOT DOES_ENTITY_EXIST(piPed)
			RELOOP
		ENDIF
		
		IF bMustBeAlive
			IF IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[iPed])
				RELOOP
			ENDIF
		ENDIF
		
		VECTOR vPedCoords = GET_ENTITY_COORDS(piPed, FALSE)
		FLOAT fDistance = VDIST2(vCheckPos, vPedCoords)
		
		IF fDistance < POW(fMaxDistance, 2.0)
			RELATIONSHIP_TYPE eRelationship = GET_RELATIONSHIP_BETWEEN_PEDS(piPed, piRelationshipPedToCheck)
			
			IF eRelationship = ACQUAINTANCE_TYPE_PED_DISLIKE
			OR eRelationship = ACQUAINTANCE_TYPE_PED_WANTED
			OR eRelationship = ACQUAINTANCE_TYPE_PED_HATE
				vClosestPedCoords = vPedCoords
				fMaxDistance = fDistance
			ELSE
				// This ped is within range, but isn't an enemy
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN vClosestPedCoords
ENDFUNC

FUNC BOOL ATTACH_ENTITY_TO_ENTITY_KEEP_CURRENT_TRANSFORM(ENTITY_INDEX eiEntity, ENTITY_INDEX eiAttachToEntity, BOOL bDisableCollision = TRUE)
	
	IF IS_ENTITY_ATTACHED_TO_ENTITY(eiEntity, eiAttachToEntity)
		// Already attached!
		RETURN TRUE
	ENDIF
	
	VECTOR vOffsetFromEntity = GET_ENTITY_COORDS(eiEntity, FALSE) - GET_ENTITY_COORDS(eiAttachToEntity, FALSE)
	VECTOR vRotationOffsetFromEntity = GET_ENTITY_ROTATION(eiEntity) - GET_ENTITY_ROTATION(eiAttachToEntity)
	
	vOffsetFromEntity = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(eiAttachToEntity, GET_ENTITY_COORDS(eiEntity, FALSE))
	vRotationOffsetFromEntity = GET_DIFFERENCE_IN_ENTITY_ROTATION(eiAttachToEntity, eiEntity)
	
	ATTACH_ENTITY_TO_ENTITY(eiEntity, eiAttachToEntity, 0, vOffsetFromEntity, vRotationOffsetFromEntity, FALSE, FALSE, FALSE, FALSE, DEFAULT, TRUE)
	
	IF IS_ENTITY_ATTACHED_TO_ENTITY(eiEntity, eiAttachToEntity)
		IF bDisableCollision
			SET_ENTITY_COMPLETELY_DISABLE_COLLISION(eiEntity, FALSE, FALSE)
		ENDIF
		
		RETURN TRUE
	ENDIF
	
	PRINTLN("ATTACH_ENTITY_TO_ENTITY_KEEP_CURRENT_TRANSFORM | Failed to attach entities!")
	DEBUG_PRINTCALLSTACK()
	RETURN FALSE
ENDFUNC

PROC PROCESS_ENTITY_INTERIOR(INT iIndex, ENTITY_INDEX eiEntity, BOOL bEntityIsInInterior, VECTOR vCustomInteriorCoords, INT& iUpdatedInteriorBS, INT iCustomRoomKey = -1)
	
	#IF IS_DEBUG_BUILD
	BOOL bShowDebug = (bEntityInteriorDebug AND IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD1))
	#ENDIF
	
	IF NOT bEntityIsInInterior
		#IF IS_DEBUG_BUILD
		IF bShowDebug
			TEXT_LABEL_63 tlVisualDebug = "No Interior"
			DRAW_DEBUG_TEXT(tlVisualDebug, GET_ENTITY_COORDS(eiEntity, FALSE), 255, 255, 255, 255)
		ENDIF
		#ENDIF
		
		EXIT
	ENDIF
	
	IF IS_BIT_SET(iUpdatedInteriorBS, iIndex % 32)
		// Already sorted!
		#IF IS_DEBUG_BUILD
		IF bShowDebug
			TEXT_LABEL_63 tlVisualDebug = "Handled Interior"
			DRAW_DEBUG_TEXT(tlVisualDebug, GET_ENTITY_COORDS(eiEntity, FALSE), 255, 255, 255, 255)
		ENDIF
		#ENDIF
		
		EXIT
	ENDIF
	
	IF LocalPlayerCurrentInterior = NULL
		#IF IS_DEBUG_BUILD
		IF bShowDebug
			TEXT_LABEL_63 tlVisualDebug = "LocalPlayerCurrentInterior is NULL"
			DRAW_DEBUG_TEXT(tlVisualDebug, GET_ENTITY_COORDS(eiEntity, FALSE), 255, 255, 255, 255)
		ENDIF
		#ENDIF
		
		EXIT
	ENDIF
	
	INTERIOR_INSTANCE_INDEX iEntityInterior
	INT iRoomKey = 0
	
	IF IS_VECTOR_ZERO(vCustomInteriorCoords)
		iEntityInterior = GET_INTERIOR_FROM_ENTITY(eiEntity)
		iRoomKey = GET_ROOM_KEY_FROM_ENTITY(eiEntity)
	ELSE
		iEntityInterior = GET_INTERIOR_AT_COORDS(vCustomInteriorCoords)
		
		IF NOT IS_BIT_SET(gMC_LocalVariables_VARS.iLocalBGScriptBS, ciLocalBGScriptBS_BlockNewRoomKeyBackup)
			iRoomKey = GET_ROOM_KEY_FROM_ENTITY(eiEntity)
		ENDIF
	ENDIF
	
	IF iCustomRoomKey != -1
		iRoomKey = iCustomRoomKey
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF bShowDebug
		TEXT_LABEL_63 tlVisualDebug = "iEntityInterior: "
		tlVisualDebug += NATIVE_TO_INT(iEntityInterior)
		DRAW_DEBUG_TEXT(tlVisualDebug, GET_ENTITY_COORDS(eiEntity, FALSE), 255, 255, 255, 255)
		
		tlVisualDebug = "iRoomKey: "
		tlVisualDebug += iRoomKey
		DRAW_DEBUG_TEXT(tlVisualDebug, GET_ENTITY_COORDS(eiEntity, FALSE) + <<0, 0, -1.0>>, 255, 255, 255, 255)
		
		tlVisualDebug = "LocalPlayerCurrentInterior: "
		tlVisualDebug += NATIVE_TO_INT(LocalPlayerCurrentInterior)
		DRAW_DEBUG_TEXT(tlVisualDebug, GET_ENTITY_COORDS(eiEntity, FALSE) + <<0, 0, -2.50>>, 255, 255, 255, 255)
	ENDIF
	#ENDIF
		
	IF NOT IS_VALID_INTERIOR(iEntityInterior)
		#IF IS_DEBUG_BUILD
		IF bShowDebug
			TEXT_LABEL_63 tlVisualDebug = "iEntityInterior not VALID"
			DRAW_DEBUG_TEXT(tlVisualDebug, GET_ENTITY_COORDS(eiEntity, FALSE) + <<0, 0, -3.5>>, 255, 0, 0, 255)
		ENDIF
		#ENDIF
		
		EXIT
	ENDIF
	
	IF LocalPlayerCurrentInterior != iEntityInterior
		#IF IS_DEBUG_BUILD
		IF bShowDebug
			TEXT_LABEL_63 tlVisualDebug = "LocalPlayerCurrentInterior != iEntityInterior"
			DRAW_DEBUG_TEXT(tlVisualDebug, GET_ENTITY_COORDS(eiEntity, FALSE) + <<0, 0, -3.5>>, 255, 0, 0, 255)
		ENDIF
		#ENDIF
		
		EXIT
	ENDIF
	
	IF NOT IS_INTERIOR_READY(iEntityInterior)
		#IF IS_DEBUG_BUILD
		IF bShowDebug
			TEXT_LABEL_63 tlVisualDebug = "iEntityInterior not READY"
			DRAW_DEBUG_TEXT(tlVisualDebug, GET_ENTITY_COORDS(eiEntity, FALSE) + <<0, 0, -3.5>>, 255, 0, 0, 255)
		ENDIF
		#ENDIF
		
		EXIT
	ENDIF
	
	IF NETWORK_GET_ENTITY_IS_NETWORKED(eiEntity)
		IF NETWORK_HAS_CONTROL_OF_ENTITY(eiEntity)
			CLEAR_ROOM_FOR_ENTITY(eiEntity)
		ENDIF
	ELSE
		CLEAR_ROOM_FOR_ENTITY(eiEntity)
	ENDIF

	IF iRoomKey != 0
		FORCE_ROOM_FOR_ENTITY(eiEntity, iEntityInterior, iRoomKey)
	#IF IS_DEBUG_BUILD
	ELSE
		PRINTLN("PROCESS_ENTITY_INTERIOR - INVALID ROOM KEY: ", iRoomKey, " - not forcing entity into room")
	#ENDIF
	ENDIF

	SET_BIT(iUpdatedInteriorBS, iIndex % 32)
	UPDATE_LIGHTS_ON_ENTITY(eiEntity)
	PRINTLN("PROCESS_ENTITY_INTERIOR - Forcing entity ", FMMC_GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(eiEntity)), " (entity index ", iIndex, ") into interior ", NATIVE_TO_INT(iEntityInterior), " / Room Key: ", iRoomKey, " || Player interior: ", NATIVE_TO_INT(LocalPlayerCurrentInterior), " / iLocalPlayerCurrentRoomKey: ", iLocalPlayerCurrentRoomKey)
	
ENDPROC
				
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Entities - Objects
// ##### Description: Helper functions for objects
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC SET_OBJECT_NEEDS_TO_RE_CACHE_OUT_OF_BOUNDS(INT iObj)
	SET_BIT(iObjNeedsToCheckOutOfBoundsBS, iObj)
	PRINTLN("[Objects][Object ", iObj, "] SET_OBJECT_NEEDS_TO_RE_CACHE_OUT_OF_BOUNDS | Setting object to check if it is out of bounds during the staggered loop")
ENDPROC

//PURPOSE: Originally created to replace something that used to use legacy vars but could still be very useful re-purposed as a generic feature
FUNC BOOL SHOULD_OBJECT_MINIGAME_BE_PASSED_FOR_ALL_TEAMS_AT_ONCE(INT iObj)
	UNUSED_PARAMETER(iObj)
	RETURN FALSE
ENDFUNC

PROC GET_RANDOM_SAFE_CRACK_COMBINATION(INT &a, INT&b, INT &c)
	SET_RANDOM_SEED(MC_serverBD_1.sMissionContinuityVars.iContinuitySeed)
	a = GET_RANDOM_INT_IN_RANGE(1, 99)
	b = GET_RANDOM_INT_IN_RANGE(1, 99)
	c = GET_RANDOM_INT_IN_RANGE(1, 99)	
ENDPROC

FUNC INT GET_TEAM_HOLDING_PACKAGE(INT iObj)
	INT iTeam
	
	FOR iTeam = 0 TO FMMC_MAX_TEAMS - 1
		IF IS_BIT_SET(MC_serverBD.iObjAtYourHolding[iTeam], iObj)
			RETURN iTeam
		ENDIF
	ENDFOR
	
	RETURN -1
ENDFUNC				

PROC SET_OBJ_NEAR(INT iObjNear)
	IF MC_playerBD[iPartToUse].iObjNear != iObjNear
		MC_playerBD[iPartToUse].iObjNear = iObjNear
		PRINTLN("SET_OBJ_NEAR | Setting MC_playerBD[", iPartToUse, "].iObjNear to ", MC_playerBD[iPartToUse].iObjNear)
		DEBUG_PRINTCALLSTACK()
	ENDIF
ENDPROC

FUNC PED_INDEX GET_PED_CARRYING_OBJECT(OBJECT_INDEX oiObj)
	ENTITY_INDEX tempPlayerEnt = GET_ENTITY_ATTACHED_TO(oiObj)
	
	IF DOES_ENTITY_EXIST(tempPlayerEnt)
		RETURN GET_PED_INDEX_FROM_ENTITY_INDEX(tempPlayerEnt)
	ENDIF
	
	RETURN NULL
ENDFUNC

FUNC PLAYER_INDEX GET_PLAYER_CARRYING_OBJECT(OBJECT_INDEX oiObj)

	PED_INDEX piPed = GET_PED_CARRYING_OBJECT(oiObj)
	
	IF NOT DOES_ENTITY_EXIST(piPed)
		RETURN NULL
	ENDIF
	
	IF NOT IS_PED_A_PLAYER(piPed)
		RETURN NULL
	ENDIF
	
	IF IS_PED_INJURED(piPed)
		RETURN NULL
	ENDIF
		
	RETURN NETWORK_GET_PLAYER_INDEX_FROM_PED(piPed)
ENDFUNC

FUNC PARTICIPANT_INDEX GET_PARTICIPANT_CARRYING_OBJECT(OBJECT_INDEX oiObj)
	PLAYER_INDEX piPlayer = GET_PLAYER_CARRYING_OBJECT(oiObj)
	
	IF piPlayer = NULL
		RETURN NULL
	ENDIF
	
	RETURN NETWORK_GET_PARTICIPANT_INDEX(piPlayer)
ENDFUNC

FUNC BOOL SHOULD_OBJECT_BE_INVISIBLE(INT iObj)
	IF NOT IS_BIT_SET(iObjOverrideInvisibilitySettingBS, iObj)
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetTwo, cibsOBJ2_SetInvisible)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SET_OBJECT_OVERRIDE_INVISIBILITY_SETTING(INT iObj, BOOL bShouldOverride = TRUE)
	IF bShouldOverride
		SET_BIT(iObjOverrideInvisibilitySettingBS, iObj)
		PRINTLN("[Objects][Object ", iObj, "] SET_OBJECT_OVERRIDE_INVINCIBILITY_SETTING | Setting object ", iObj, " to ignore cibsOBJ2_SetInvisible")
	ELSE
		CLEAR_BIT(iObjOverrideInvisibilitySettingBS, iObj)
		PRINTLN("[Objects][Object ", iObj, "] SET_OBJECT_OVERRIDE_INVINCIBILITY_SETTING | Setting object ", iObj, " to use cibsOBJ2_SetInvisible")
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Aggro and Alerted Helper Functions.
// ##### Description: Various helper functions & wrappers to do with Aggroing, Alerting, both with CCTV and Aggro Systems
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

// Aggro Timer
FUNC INT GET_FIRST_AGGRO_INDEX_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME_FROM_BITSET(INT iTeam, INT iAggroBS)
	INT i
	IF iAggroBS = ciMISSION_AGGRO_INDEX_DEFAULT	
		IF MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(MC_serverBD.iAggroCountdownTimeStamp[iTeam][ciMISSION_AGGRO_INDEX_DEFAULT])
			RETURN MC_GET_NET_TIMER_DIFFERENCE_WITH_TIME_STAMP(MC_serverBD.iAggroCountdownTimeStamp[iTeam][ciMISSION_AGGRO_INDEX_DEFAULT])
		ENDIF
	ELIF iAggroBS = ciMISSION_AGGRO_INDEX_ALL
		FOR i = 0 TO ciMISSION_AGGRO_INDEX_MAX-1
			IF MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(MC_serverBD.iAggroCountdownTimeStamp[iTeam][i])
				RETURN MC_GET_NET_TIMER_DIFFERENCE_WITH_TIME_STAMP(MC_serverBD.iAggroCountdownTimeStamp[iTeam][i])
			ENDIF
		ENDFOR
	ELSE
		FOR i = 0 TO ciMISSION_AGGRO_INDEX_MAX-1
			IF IS_BIT_SET(iAggroBS, i)
			AND MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(MC_serverBD.iAggroCountdownTimeStamp[iTeam][i])
				RETURN MC_GET_NET_TIMER_DIFFERENCE_WITH_TIME_STAMP(MC_serverBD.iAggroCountdownTimeStamp[iTeam][i])
			ENDIF
		ENDFOR
	ENDIF
	
	RETURN 0
ENDFUNC

PROC START_AGGRO_INDEX_TIMER_FROM_BITSET(INT iTeam, INT iAggroBS)
	INT i
	IF iAggroBS = ciMISSION_AGGRO_INDEX_DEFAULT	
		MC_START_NET_TIMER_WITH_TIMESTAMP(MC_serverBD.iAggroCountdownTimeStamp[iTeam][ciMISSION_AGGRO_INDEX_DEFAULT])
	ELIF iAggroBS = ciMISSION_AGGRO_INDEX_ALL
		FOR i = 0 TO ciMISSION_AGGRO_INDEX_MAX-1
			MC_START_NET_TIMER_WITH_TIMESTAMP(MC_serverBD.iAggroCountdownTimeStamp[iTeam][i])
		ENDFOR
	ELSE
		FOR i = 0 TO ciMISSION_AGGRO_INDEX_MAX-1
			IF IS_BIT_SET(iAggroBS, i)
				MC_START_NET_TIMER_WITH_TIMESTAMP(MC_serverBD.iAggroCountdownTimeStamp[iTeam][i])
			ENDIF
		ENDFOR
	ENDIF	
ENDPROC

PROC REINIT_AGGRO_INDEX_TIMER_FROM_BITSET(INT iTeam, INT iAggroBS)
	INT i
	IF iAggroBS = ciMISSION_AGGRO_INDEX_DEFAULT		
		MC_REINIT_NET_TIMER_WITH_TIMESTAMP(MC_serverBD.iAggroCountdownTimeStamp[iTeam][ciMISSION_AGGRO_INDEX_DEFAULT])
	ELIF iAggroBS = ciMISSION_AGGRO_INDEX_ALL
		FOR i = 0 TO ciMISSION_AGGRO_INDEX_MAX-1
			MC_REINIT_NET_TIMER_WITH_TIMESTAMP(MC_serverBD.iAggroCountdownTimeStamp[iTeam][i])
		ENDFOR
	ELSE
		FOR i = 0 TO ciMISSION_AGGRO_INDEX_MAX-1
			IF IS_BIT_SET(iAggroBS, i)
				MC_REINIT_NET_TIMER_WITH_TIMESTAMP(MC_serverBD.iAggroCountdownTimeStamp[iTeam][i])
			ENDIF
		ENDFOR
	ENDIF
ENDPROC

PROC STOP_AGGRO_INDEX_TIMER_FROM_BITSET(INT iTeam, INT iAggroBS)
	INT i
	IF iAggroBS = ciMISSION_AGGRO_INDEX_DEFAULT		
		MC_STOP_NET_TIMER_WITH_TIMESTAMP(MC_serverBD.iAggroCountdownTimeStamp[iTeam][ciMISSION_AGGRO_INDEX_DEFAULT])
	ELIF iAggroBS = ciMISSION_AGGRO_INDEX_ALL
		FOR i = 0 TO ciMISSION_AGGRO_INDEX_MAX-1
			MC_STOP_NET_TIMER_WITH_TIMESTAMP(MC_serverBD.iAggroCountdownTimeStamp[iTeam][i])
		ENDFOR
	ELSE
		FOR i = 0 TO ciMISSION_AGGRO_INDEX_MAX-1
			IF IS_BIT_SET(iAggroBS, i)
				MC_STOP_NET_TIMER_WITH_TIMESTAMP(MC_serverBD.iAggroCountdownTimeStamp[iTeam][i])
			ENDIF
		ENDFOR
	ENDIF	
ENDPROC

FUNC BOOL HAS_ALL_AGGRO_INDEX_TIMER_STARTED_FROM_BITSET(INT iTeam, INT iAggroBS)	
	INT i
	IF iAggroBS = ciMISSION_AGGRO_INDEX_DEFAULT
		IF NOT MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(MC_serverBD.iAggroCountdownTimeStamp[iTeam][ciMISSION_AGGRO_INDEX_DEFAULT])
			RETURN FALSE
		ENDIF
	ELIF iAggroBS = ciMISSION_AGGRO_INDEX_ALL
		FOR i = 0 TO ciMISSION_AGGRO_INDEX_MAX-1
			IF NOT MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(MC_serverBD.iAggroCountdownTimeStamp[iTeam][i])
				RETURN FALSE
			ENDIF
		ENDFOR
	ELSE
		FOR i = 0 TO ciMISSION_AGGRO_INDEX_MAX-1
			IF IS_BIT_SET(iAggroBS, i)
			AND NOT MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(MC_serverBD.iAggroCountdownTimeStamp[iTeam][i])
				RETURN FALSE
			ENDIF
		ENDFOR
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL HAS_AGGRO_INDEX_TIMER_STARTED_FROM_BITSET(INT iTeam, INT iAggroBS)	
	INT i
	IF iAggroBS = ciMISSION_AGGRO_INDEX_DEFAULT
		RETURN MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(MC_serverBD.iAggroCountdownTimeStamp[iTeam][ciMISSION_AGGRO_INDEX_DEFAULT])
	ELIF iAggroBS = ciMISSION_AGGRO_INDEX_ALL
		FOR i = 0 TO ciMISSION_AGGRO_INDEX_MAX-1
			IF MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(MC_serverBD.iAggroCountdownTimeStamp[iTeam][i])
				RETURN TRUE
			ENDIF
		ENDFOR
	ELSE
		FOR i = 0 TO ciMISSION_AGGRO_INDEX_MAX-1
			IF IS_BIT_SET(iAggroBS, i)
			AND MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(MC_serverBD.iAggroCountdownTimeStamp[iTeam][i])
				RETURN TRUE	
			ENDIF
		ENDFOR
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_AGGRO_INDEX_TIMER_EXPIRED_FROM_BITSET(INT iTeam, INT iAggroBS, INT iTime)
	INT i
	IF iAggroBS = ciMISSION_AGGRO_INDEX_DEFAULT
		RETURN (MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(MC_serverBD.iAggroCountdownTimeStamp[iTeam][ciMISSION_AGGRO_INDEX_DEFAULT]) AND MC_HAS_NET_TIMER_EXPIRED_WITH_TIME_STAMP(MC_serverBD.iAggroCountdownTimeStamp[iTeam][ciMISSION_AGGRO_INDEX_DEFAULT], iTime))
	ELIF iAggroBS = ciMISSION_AGGRO_INDEX_ALL
		FOR i = 0 TO ciMISSION_AGGRO_INDEX_MAX-1
			IF MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(MC_serverBD.iAggroCountdownTimeStamp[iTeam][i])
			AND MC_HAS_NET_TIMER_EXPIRED_WITH_TIME_STAMP(MC_serverBD.iAggroCountdownTimeStamp[iTeam][i], iTime)		
				RETURN TRUE
			ENDIF
		ENDFOR
	ELSE
		FOR i = 0 TO ciMISSION_AGGRO_INDEX_MAX-1
			IF IS_BIT_SET(iAggroBS, i)
			AND MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(MC_serverBD.iAggroCountdownTimeStamp[iTeam][i])
			AND MC_HAS_NET_TIMER_EXPIRED_WITH_TIME_STAMP(MC_serverBD.iAggroCountdownTimeStamp[iTeam][i], iTime)
				RETURN TRUE	
			ENDIF
		ENDFOR
	ENDIF
	
	RETURN FALSE
ENDFUNC

// MAIN
FUNC BOOL HAS_AGGRO_BITSET_TRIGGERED(INT iAggroBS, INT iBitsetToCheck)

	IF iAggroBS = ciMISSION_AGGRO_INDEX_DEFAULT
		RETURN IS_BIT_SET(iBitsetToCheck, ciMISSION_AGGRO_INDEX_DEFAULT)
	ELIF iAggroBS = ciMISSION_AGGRO_INDEX_ALL
		INT i
		FOR i = 0 TO ciMISSION_AGGRO_INDEX_MAX-1
			IF IS_BIT_SET(iBitsetToCheck, i)
				RETURN TRUE
			ENDIF
		ENDFOR
	ELSE
		INT iSumAggroBS = iAggroBS & iBitsetToCheck
		IF iSumAggroBS != 0
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC CLEAR_AGGRO_BITSET_FROM_BITSET(INT iAggroBS, INT &iBitsetToClear)
	IF iAggroBS = ciMISSION_AGGRO_INDEX_ALL		
		iBitsetToClear = 0
	ELIF iAggroBS = ciMISSION_AGGRO_INDEX_DEFAULT
		CLEAR_BIT(iBitsetToClear, ciMISSION_AGGRO_INDEX_DEFAULT)
	ELSE		
		CLEAR_BITMASK(iBitsetToClear, iAggroBS)
	ENDIF	
ENDPROC

PROC SET_AGGRO_BITSET_FROM_BITSET(INT iAggroBS, INT &iBitsetToSet)
	IF iAggroBS = ciMISSION_AGGRO_INDEX_ALL
		iBitsetToSet = ciMISSION_AGGRO_INDEX_MAX_BITSET
	ELIF iAggroBS = ciMISSION_AGGRO_INDEX_DEFAULT
		SET_BIT(iBitsetToSet, ciMISSION_AGGRO_INDEX_DEFAULT)
	ELSE
		SET_BITMASK(iBitsetToSet, iAggroBS)
	ENDIF
ENDPROC

// CCTV
PROC SET_CCTV_CAMERA_REPORTED_BY_PED(INT iCam)
	SET_BIT(iCCTVCameraReportedByPedBS, iCam)
	PRINTLN("[CCTV][CAM ", iCam, "] Camera has has been triggered by a ped!")
ENDPROC

PROC SET_CCTV_CAMERA_SPOTTED_PLAYER(INT iCam)
	SET_BIT(iCCTVCameraSpottedPlayerBS, iCam)
	PRINTLN("[CCTV][CAM ", iCam, "] Camera has spotted a player!")
ENDPROC

PROC SET_CCTV_CAMERA_SPOTTED_BODY(INT iCam)
	SET_BIT(iCCTVCameraSpottedBodyBS, iCam)
	PRINTLN("[CCTV][CAM ", iCam, "] Camera has spotted a body!")
ENDPROC

FUNC BOOL HAS_CCTV_CAMERA_SPOTTED_SOMETHING(INT iCam, BOOL bIncludeSpottedDelay = FALSE)

	IF bIncludeSpottedDelay
		IF HAS_NET_TIMER_STARTED(CCTVSpottingPlayerTimer[iCam])
		OR HAS_NET_TIMER_STARTED(CCTVSpottingBodyTimer[iCam])
		OR HAS_NET_TIMER_STARTED(CCTVReportedByPedTimer[iCam])
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iCCTVCameraSpottedPlayerBS, iCam)
		// Player was spotted!
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(iCCTVCameraSpottedBodyBS, iCam)
		// Ped body was spotted!
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(iCCTVCameraReportedByPedBS, iCam)
		// Ped reported to CCTV!
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SET_CCTV_CAMERAS_WITH_AGGRO_BS_BEEN_ALERTED_BY_PLAYER(INT iTeam, INT iAggroBS)
	
	PRINTLN("[AggroIndex][SpookAggro] SET_CCTV_CAMERAS_WITH_AGGRO_BS_BEEN_ALERTED_BY_PLAYER - (Before) iAggroBS: ", iAggroBS, " MC_serverBD.iServerCCTVAlertedPlayerBS[", iTeam, "]: ", MC_serverBD.iServerCCTVAlertedPlayerBS[iTeam])
	
	SET_AGGRO_BITSET_FROM_BITSET(iAggroBS, MC_serverBD.iServerCCTVAlertedPlayerBS[iTeam])		
	
	PRINTLN("[AggroIndex][SpookAggro] SET_CCTV_CAMERAS_WITH_AGGRO_BS_BEEN_ALERTED_BY_PLAYER - (After) MC_serverBD.iServerCCTVAlertedPlayerBS[", iTeam, "]: ", MC_serverBD.iServerCCTVAlertedPlayerBS[iTeam])
	
ENDPROC

PROC SET_CCTV_CAMERAS_WITH_AGGRO_BS_SPOTTED_DEAD_BODY(INT iTeam, INT iAggroBS)
	
	PRINTLN("[AggroIndex][SpookAggro] SET_CCTV_CAMERAS_WITH_AGGRO_BS_SPOTTED_DEAD_BODY - (Before) iAggroBS: ", iAggroBS, " MC_serverBD.iServerCCTVAlertedPedBodyBS[", iTeam, "]: ", MC_serverBD.iServerCCTVAlertedPedBodyBS[iTeam])
	
	SET_AGGRO_BITSET_FROM_BITSET(iAggroBS, MC_serverBD.iServerCCTVAlertedPedBodyBS[iTeam])		
	
	PRINTLN("[AggroIndex][SpookAggro] SET_CCTV_CAMERAS_WITH_AGGRO_BS_SPOTTED_DEAD_BODY - (After) MC_serverBD.iServerCCTVAlertedPedBodyBS[", iTeam, "]: ", MC_serverBD.iServerCCTVAlertedPedBodyBS[iTeam])
	
ENDPROC

PROC SET_TEAM_AS_TRIGGERED_CCTV_FROM_PEDS_AGGRO_BITSET(INT iTeam, INT iAggroBS)

	PRINTLN("[AggroIndex][SpookAggro] SET_TEAM_AS_TRIGGERED_CCTV_FROM_PEDS_AGGRO_BITSET - (Before) iAggroBS: ", iAggroBS, " MC_serverBD.iServerCCTVTriggeredByPedsBS[", iTeam, "]: ", MC_serverBD.iServerCCTVTriggeredByPedsBS[iTeam])
	
	SET_AGGRO_BITSET_FROM_BITSET(iAggroBS, MC_serverBD.iServerCCTVTriggeredByPedsBS[iTeam])
	
	PRINTLN("[AggroIndex][SpookAggro] SET_TEAM_AS_TRIGGERED_CCTV_FROM_PEDS_AGGRO_BITSET - (After) MC_serverBD.iServerCCTVTriggeredByPedsBS[", iTeam, "]: ", MC_serverBD.iServerCCTVTriggeredByPedsBS[iTeam])
	
ENDPROC

FUNC BOOL HAVE_CCTV_CAMERAS_WITH_AGGRO_BS_BEEN_ALERTED_BY_PLAYER(INT iTeam, INT iAggroBS)
	RETURN HAS_AGGRO_BITSET_TRIGGERED(iAggroBS, MC_ServerBD.iServerCCTVAlertedPlayerBS[iTeam])	
ENDFUNC

FUNC BOOL HAVE_CCTV_CAMERAS_WITH_AGGRO_BS_SPOTTED_DEAD_BODY(INT iTeam, INT iAggroBS)
	RETURN HAS_AGGRO_BITSET_TRIGGERED(iAggroBS, MC_ServerBD.iServerCCTVAlertedPedBodyBS[iTeam])		
ENDFUNC

FUNC BOOL HAS_TEAM_TRIGGERED_CCTV_FROM_PEDS_AGGRO_BITSET(INT iTeam, INT iAggroBS)
	RETURN HAS_AGGRO_BITSET_TRIGGERED(iAggroBS, MC_serverBD.iServerCCTVTriggeredByPedsBS[iTeam])	
ENDFUNC

FUNC BOOL HAVE_CCTV_CAMERAS_WITH_AGGRO_BS_BEEN_ALERTED(INT iTeam, INT iAggroBS)

	IF HAS_TEAM_TRIGGERED_CCTV_FROM_PEDS_AGGRO_BITSET(iTeam, iAggroBS)
		RETURN TRUE
	ENDIF
	
	IF HAVE_CCTV_CAMERAS_WITH_AGGRO_BS_SPOTTED_DEAD_BODY(iTeam, iAggroBS)
		RETURN TRUE
	ENDIF
	
	RETURN HAVE_CCTV_CAMERAS_WITH_AGGRO_BS_BEEN_ALERTED_BY_PLAYER(iTeam, iAggroBS)
ENDFUNC


// AGGRO 
FUNC BOOL HAS_TEAM_TRIGGERED_AGGRO(INT iTeam, INT iAggroBS)
	RETURN HAS_AGGRO_BITSET_TRIGGERED(iAggroBS, MC_serverBD.iServerAggroBS[iTeam])
ENDFUNC

PROC CLEAR_TEAM_AS_TRIGGERED_AGGRO_FROM_BITSET(INT iTeam, INT iAggroBS)
	
	PRINTLN("[AggroIndex][SpookAggro] CLEAR_TEAM_AS_TRIGGERED_AGGRO_FROM_BITSET - (Before) iAggroBS: ", iAggroBS, " MC_serverBD.iServerAggroBS[", iTeam, "]:", MC_serverBD.iServerAggroBS[iTeam])
	
	CLEAR_AGGRO_BITSET_FROM_BITSET(iAggroBS, MC_serverBD.iServerAggroBS[iTeam])
	
	PRINTLN("[AggroIndex][SpookAggro] CLEAR_TEAM_AS_TRIGGERED_AGGRO_FROM_BITSET - (After) MC_serverBD.iServerAggroBS[", iTeam, "]:", MC_serverBD.iServerAggroBS[iTeam])
	
ENDPROC

PROC SET_TEAM_AS_TRIGGERED_AGGRO_FROM_BITSET(INT iTeam, INT iAggroBS)

	PRINTLN("[AggroIndex][SpookAggro] SET_TEAM_AS_TRIGGERED_AGGRO_FROM_BITSET - (Before) iAggroBS: ", iAggroBS, " MC_serverBD.iServerAggroBS[", iTeam, "]: ", MC_serverBD.iServerAggroBS[iTeam])
	
	SET_AGGRO_BITSET_FROM_BITSET(iAggroBS, MC_serverBD.iServerAggroBS[iTeam])
	
	PRINTLN("[AggroIndex][SpookAggro] SET_TEAM_AS_TRIGGERED_AGGRO_FROM_BITSET - (After) MC_serverBD.iServerAggroBS[", iTeam, "]: ", MC_serverBD.iServerAggroBS[iTeam])
	
ENDPROC

FUNC BOOL HAS_LOCAL_PLAYER_TEAM_TRIGGERED_AGGRO(BOOL bUsePartToUse, INT iAggroBS)
	RETURN HAS_TEAM_TRIGGERED_AGGRO(GET_LOCAL_PLAYER_TEAM(bUsePartToUse), iAggroBS)
ENDFUNC

FUNC BOOL HAS_ANY_TEAM_TRIGGERED_AGGRO(INT iAggroBS)
	INT iTeam = 0
	FOR iTeam = 0 TO MC_serverBD.iNumberOfTeams-1	
		IF HAS_TEAM_TRIGGERED_AGGRO(iTeam, iAggroBS)
			RETURN TRUE
		ENDIF
	ENDFOR
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_ANY_FRIENDLY_TEAM_TRIGGERED_AGGRO(INT iTeam, INT iAggroBS, BOOL bExcludeSameTeam = FALSE)
	
	INT iTeamLoop
	FOR iTeamLoop = 0 TO MC_serverBD.iNumberOfTeams - 1
		
		IF bExcludeSameTeam 
		AND iTeamLoop = iTeam
			RELOOP
		ENDIF
		
		IF DOES_TEAM_LIKE_TEAM(iTeam, iTeamLoop)
		AND HAS_TEAM_TRIGGERED_AGGRO(iTeam, iAggroBS)
			RETURN TRUE
		ENDIF
	ENDFOR
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL CHECK_AGGRO_INDEX_PREREQUISITES(INT &iAggroIndexBS_ApplyAggro_PrereqIndexBS[], BOOL bCheckAnyMatches)
		
	INT iPrereqs
	FOR iPrereqs = 0 TO ciPREREQ_Bitsets-1
		
		IF iAggroIndexBS_ApplyAggro_PrereqIndexBS[iPrereqs] = 0
			RELOOP
		ENDIF
		
		INT iTempBS = iAggroIndexBS_ApplyAggro_PrereqIndexBS[iPrereqs] & MC_playerBD[iLocalPart].iPrerequisiteBS[iPrereqs]
		
		PRINTLN("[AggroIndex][SpookAggro] - CHECK_AGGRO_INDEX_PREREQUISITES - iTempBS: ", iTempBS, " --- iAggroIndexBS_ApplyAggro_PrereqIndexBS[", iPrereqs, "]: ", iAggroIndexBS_ApplyAggro_PrereqIndexBS[iPrereqs])
		
		IF bCheckAnyMatches
			IF iTempBS > 0
				PRINTLN("[AggroIndex][SpookAggro] - CHECK_AGGRO_INDEX_PREREQUISITES - At least 1 prerequisite matches. Triggering Aggro Prerequisites. Returning True.")
				RETURN TRUE
			ENDIF
		ELSE
			IF iTempBS != iAggroIndexBS_ApplyAggro_PrereqIndexBS[iPrereqs]
				PRINTLN("[AggroIndex][SpookAggro] - CHECK_AGGRO_INDEX_PREREQUISITES - Expecting all prerequisites to match, and they do not. Returning False.")
				RETURN FALSE
			ENDIF
		ENDIF
		
	ENDFOR
			
	IF bCheckAnyMatches
		PRINTLN("[AggroIndex][SpookAggro] - CHECK_AGGRO_INDEX_PREREQUISITES - No Prerequisites match. Returning False.")
		RETURN FALSE
	ELSE
		PRINTLN("[AggroIndex][SpookAggro] - CHECK_AGGRO_INDEX_PREREQUISITES - All Prerequisites match. Returning True.")
		RETURN TRUE
	ENDIF
	
ENDFUNC

FUNC BOOL CAN_AGGRO_INDEX_PREREQUISITE_BE_CHECKED(INT iPrereqIndex)
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iAggroIndexBS_ApplyAggro_ModBS[iPrereqIndex], ciMISSION_AGGRO_INDEX_PREREQ_BS_CHECK_ON_PREREQ_CHANGE)
		IF IS_BIT_SET(MC_ServerBD.iServerBitSet8, SBBOOL8_PREREQ_CHANGE_TRIGGERED_AGGRO_INDEX_PREREQS_CHECK)
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iAggroIndexBS_ApplyAggro_ModBS[iPrereqIndex], ciMISSION_AGGRO_INDEX_PREREQ_BS_CHECK_ON_ZONE)
		IF IS_BIT_SET(MC_ServerBD.iServerBitSet8, SBBOOL8_ZONE_TRIGGERED_AGGRO_INDEX_PREREQS_CHECK)
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iAggroIndexBS_ApplyAggro_ModBS[iPrereqIndex], ciMISSION_AGGRO_INDEX_PREREQ_BS_CHECK_ON_RULE_CHANGE)
		IF IS_BIT_SET(MC_ServerBD.iServerBitSet8, SBBOOL8_RULE_CHANGE_TRIGGERED_AGGRO_INDEX_PREREQS_CHECK)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC CHECK_AGGRO_INDEX_PREREQUISITE(INT iTeam, INT iPrereqIndex)
	
	PRINTLN("[AggroIndex][SpookAggro] - CHECK_AGGRO_INDEX_PREREQUISITE - Checking now -------------")
	
	IF NOT CAN_AGGRO_INDEX_PREREQUISITE_BE_CHECKED(iPrereqIndex)
		EXIT
	ENDIF
		
	IF NOT CHECK_AGGRO_INDEX_PREREQUISITES(g_FMMC_STRUCT.iAggroIndexBS_ApplyAggro_PrereqIndexBS[iPrereqIndex], IS_BIT_SET(g_FMMC_STRUCT.iAggroIndexBS_ApplyAggro_ModBS[iPrereqIndex], ciMISSION_AGGRO_INDEX_PREREQ_BS_ANY_PREREQ))
		EXIT
	ENDIF
	
	INT iAggroIndexBS = g_FMMC_STRUCT.iAggroIndexBS_ApplyAggro_PrereqAggroBS[iPrereqIndex]
			
	IF IS_BIT_SET(g_FMMC_STRUCT.iAggroIndexBS_ApplyAggro_ModBS[iPrereqIndex], ciMISSION_AGGRO_INDEX_PREREQ_BS_CLEAR_INSTEAD_OF_SET_AGGRO)
		PRINTLN("[AggroIndex][SpookAggro] - CHECK_AGGRO_INDEX_PREREQUISITE - Conditions have been met, Clearing aggro with iAggroIndexBS: ", iAggroIndexBS)
		CLEAR_TEAM_AS_TRIGGERED_AGGRO_FROM_BITSET(iTeam, iAggroIndexBS)
	ELSE
		PRINTLN("[AggroIndex][SpookAggro] - CHECK_AGGRO_INDEX_PREREQUISITE - Conditions have been met, Setting aggro with iAggroIndexBS: ", iAggroIndexBS)
		SET_TEAM_AS_TRIGGERED_AGGRO_FROM_BITSET(iTeam, iAggroIndexBS)
	ENDIF
	
ENDPROC

PROC CHECK_ALL_AGGRO_INDEX_PREREQUISITES()
	
	PRINTLN("[AggroIndex][SpookAggro] - CHECK_ALL_AGGRO_INDEX_PREREQUISITES - Checking all aggro index prerequisites.")
	
	INT iTeam
	INT iPrereqIndex = 0
	FOR iPrereqIndex = 0 TO ciMISSION_AGGRO_INDEX_PREREQ_TRIGGERS_MAX-1
		FOR iTeam = 0 TO MC_serverBD.iNumberOfTeams-1
			CHECK_AGGRO_INDEX_PREREQUISITE(iTeam, iPrereqIndex)			
		ENDFOR
	ENDFOR
	
	#IF IS_DEBUG_BUILD
	IF IS_BIT_SET(MC_ServerBD.iServerBitSet8, SBBOOL8_ZONE_TRIGGERED_AGGRO_INDEX_PREREQS_CHECK)
		PRINTLN("[AggroIndex][SpookAggro] - CHECK_ALL_AGGRO_INDEX_PREREQUISITES - SBBOOL8_ZONE_TRIGGERED_AGGRO_INDEX_PREREQS_CHECK is no longer valid.")
	#ENDIF
		CLEAR_BIT(MC_ServerBD.iServerBitSet8, SBBOOL8_ZONE_TRIGGERED_AGGRO_INDEX_PREREQS_CHECK)
	#IF IS_DEBUG_BUILD
	ENDIF
	#ENDIF
	
	#IF IS_DEBUG_BUILD
	IF IS_BIT_SET(MC_ServerBD.iServerBitSet8, SBBOOL8_RULE_CHANGE_TRIGGERED_AGGRO_INDEX_PREREQS_CHECK)
		PRINTLN("[AggroIndex][SpookAggro] - CHECK_ALL_AGGRO_INDEX_PREREQUISITES - SBBOOL8_RULE_CHANGE_TRIGGERED_AGGRO_INDEX_PREREQS_CHECK is no longer valid.")
	#ENDIF
		CLEAR_BIT(MC_ServerBD.iServerBitSet8, SBBOOL8_RULE_CHANGE_TRIGGERED_AGGRO_INDEX_PREREQS_CHECK)
	#IF IS_DEBUG_BUILD
	ENDIF
	#ENDIF
	
	#IF IS_DEBUG_BUILD
	IF IS_BIT_SET(MC_ServerBD.iServerBitSet8, SBBOOL8_PREREQ_CHANGE_TRIGGERED_AGGRO_INDEX_PREREQS_CHECK)
		PRINTLN("[AggroIndex][SpookAggro] - CHECK_ALL_AGGRO_INDEX_PREREQUISITES - SBBOOL8_PREREQ_CHANGE_TRIGGERED_AGGRO_INDEX_PREREQS_CHECK is no longer valid.")
	#ENDIF
		CLEAR_BIT(MC_ServerBD.iServerBitSet8, SBBOOL8_PREREQ_CHANGE_TRIGGERED_AGGRO_INDEX_PREREQS_CHECK)
	#IF IS_DEBUG_BUILD
	ENDIF
	#ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Entities - Spawning
// ##### Description: Functions and start of larger entity spawning section
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL SHOULD_ENTITY_RESPAWN_THIS_RULE(INT iRespawnOnRuleChangeBS, INT &iRespawnRules[], INT iTeamToCheck, BOOL bSkipSpecificRuleCheck = TRUE)
	
	IF iRespawnOnRuleChangeBS = 0
		RETURN FALSE
	ENDIF
	
	IF !bSkipSpecificRuleCheck
		IF iTeamToCheck != -1
			
			IF NOT IS_BIT_SET(iRespawnOnRuleChangeBS, iTeamToCheck)
				RETURN FALSE
			ENDIF
			
			IF iRespawnRules[iTeamToCheck] != 0
				IF NOT IS_BIT_SET(iRespawnRules[iTeamToCheck], GET_TEAM_CURRENT_RULE(iTeamToCheck))
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

// ##### -------------------------------------------------------------------------
// ##### Section Name: Entities - Spawn Flags
// ##### Description: Various helper functions & wrappers to do with Spawn Groups
// ##### -------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------

FUNC BOOL IS_CONTENT_SPECIFIC_SPAWN_CONDITION_FLAG_SET(SPAWN_CONDITION_FLAG eSpawnCondition)
	SWITCH eSpawnCondition
		CASE SPAWN_CONDITION_FLAG_COMPOUND_GATE_EXPLOSIVES
		CASE SPAWN_CONDITION_FLAG_COMPOUND_KEYCARD_SOUTH
		CASE SPAWN_CONDITION_FLAG_COMPOUND_KEYCARD_NORTH
		CASE SPAWN_CONDITION_FLAG_COMPOUND_RAPPEL_SOUTH
		CASE SPAWN_CONDITION_FLAG_COMPOUND_RAPPEL_NORTH
		CASE SPAWN_CONDITION_FLAG_COMPOUND_VEHICLE_FRONT_GATE
		CASE SPAWN_CONDITION_FLAG_COMPOUND_SEWER_GRATE
		CASE SPAWN_CONDITION_FLAG_ESCAPE_ROUTE_AIRSTRIP		
		CASE SPAWN_CONDITION_FLAG_ESCAPE_ROUTE_MAINDOCKS		
		CASE SPAWN_CONDITION_FLAG_ESCAPE_ROUTE_SMALLDOCKS	
		CASE SPAWN_CONDITION_FLAG_ESCAPE_ROUTE_SUBMARINE		
		CASE SPAWN_CONDITION_FLAG_PLAYER_HAS_ACETYLENE_TORCH
		CASE SPAWN_CONDITION_FLAG_BOLT_CUTTERS_SCOPED			
		CASE SPAWN_CONDITION_FLAG_BOLT_CUTTERS_UNSCOPED			
		CASE SPAWN_CONDITION_FLAG_RAPPEL_SCOPED					
		CASE SPAWN_CONDITION_FLAG_RAPPEL_UNSCOPED				
		CASE SPAWN_CONDITION_FLAG_UNIFORM_SCOPED					
		CASE SPAWN_CONDITION_FLAG_UNIFORM_UNSCOPED		
		CASE SPAWN_CONDITION_FLAG_JAMMEDFLEECABANK_A
		CASE SPAWN_CONDITION_FLAG_JAMMEDFLEECABANK_B
		CASE SPAWN_CONDITION_FLAG_JAMMEDFLEECABANK_C
		CASE SPAWN_CONDITION_FLAG_JAMMEDFLEECABANK_D
		CASE SPAWN_CONDITION_FLAG_JAMMEDFLEECABANK_E
		CASE SPAWN_CONDITION_FLAG_JAMMEDFLEECABANK_F	
		CASE SPAWN_CONDITION_FLAG_NOTJAMMEDFLEECABANK_A
		CASE SPAWN_CONDITION_FLAG_NOTJAMMEDFLEECABANK_B
		CASE SPAWN_CONDITION_FLAG_NOTJAMMEDFLEECABANK_C
		CASE SPAWN_CONDITION_FLAG_NOTJAMMEDFLEECABANK_D
		CASE SPAWN_CONDITION_FLAG_NOTJAMMEDFLEECABANK_E
		CASE SPAWN_CONDITION_FLAG_NOTJAMMEDFLEECABANK_F
		CASE SPAWN_CONDITION_FLAG_BLOCK_SPAWN_IF_JAMMEDFLEECABANK_ALL	
		CASE SPAWN_CONDITION_FLAG_BLOCK_SPAWN_IF_NOTJAMMEDFLEECABANK_ALL
		CASE SPAWN_CONDITION_FLAG_TRUCK_WEAPONS
		CASE SPAWN_CONDITION_FLAG_TRUCK_INSURGENT
		CASE SPAWN_CONDITION_FLAG_TRUCK_AA_TRAILER
		CASE SPAWN_CONDITION_FLAG_NOT_TRUCK_WEAPONS
		CASE SPAWN_CONDITION_FLAG_NOT_TRUCK_INSURGENT
		CASE SPAWN_CONDITION_FLAG_NOT_TRUCK_AA_TRAILER
		CASE SPAWN_CONDITION_FLAG_METH_TANKER_PHANTOM
		CASE SPAWN_CONDITION_FLAG_METH_TANKER_HAULER
		CASE SPAWN_CONDITION_FLAG_METH_TANKER_PHANTOM_WEDGE
		CASE SPAWN_CONDITION_FLAG_AUTO_SHOP_LA_MESA 	
		CASE SPAWN_CONDITION_FLAG_AUTO_SHOP_STRAWBERRY 
		CASE SPAWN_CONDITION_FLAG_AUTO_SHOP_BURTON 	
		CASE SPAWN_CONDITION_FLAG_AUTO_SHOP_RANCHO 	
		CASE SPAWN_CONDITION_FLAG_AUTO_SHOP_MISSION_ROW
		CASE SPAWN_CONDITION_FLAG_JAMMEDFLEECABANK_ANY		
			PRINTLN("[SpawnConditionFlag] IS_CONTENT_SPECIFIC_SPAWN_CONDITION_FLAG_SET | eSpawnCondition: ", GET_SPAWN_CONDITION_FLAG_NAME_FOR_PRINT(eSpawnCondition), " BLOCKED!")
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL MC_SHOULD_BLOCK_SPAWN_DUE_TO_PLAYER_YACHT(SPAWN_CONDITION_FLAG eSpawnCondition)
	
	IF NOT FMMC_CURRENTLY_USING_PLAYER_OWNED_YACHT()
		PRINTLN("[MYACHT] MC_SHOULD_BLOCK_SPAWN_DUE_TO_PLAYER_YACHT | We're not using a player yacht")
		RETURN FALSE
	ENDIF
	
	PRINTLN("[MYACHT] MC_SHOULD_BLOCK_SPAWN_DUE_TO_PLAYER_YACHT | Appearance.iOption: ", sMissionYachtData[ciYACHT_LOBBY_HOST_YACHT_INDEX].Appearance.iOption)
	
	SWITCH eSpawnCondition
		CASE SPAWN_CONDITION_FLAG_ORION_YACHT
			RETURN sMissionYachtData[ciYACHT_LOBBY_HOST_YACHT_INDEX].Appearance.iOption != ciYACHT_MODEL__ORION
		BREAK
		CASE SPAWN_CONDITION_FLAG_PISCES_YACHT
			RETURN sMissionYachtData[ciYACHT_LOBBY_HOST_YACHT_INDEX].Appearance.iOption != ciYACHT_MODEL__PISCES
		BREAK
		CASE SPAWN_CONDITION_FLAG_AQUARIUS_YACHT
			RETURN sMissionYachtData[ciYACHT_LOBBY_HOST_YACHT_INDEX].Appearance.iOption != ciYACHT_MODEL__AQUARIUS
		BREAK
		CASE SPAWN_CONDITION_FLAG_ORION_OR_PISCES_YACHT
			RETURN sMissionYachtData[ciYACHT_LOBBY_HOST_YACHT_INDEX].Appearance.iOption != ciYACHT_MODEL__ORION AND sMissionYachtData[ciYACHT_LOBBY_HOST_YACHT_INDEX].Appearance.iOption != ciYACHT_MODEL__PISCES
		BREAK
		CASE SPAWN_CONDITION_FLAG_ORION_OR_AQUARIUS_YACHT
			RETURN sMissionYachtData[ciYACHT_LOBBY_HOST_YACHT_INDEX].Appearance.iOption != ciYACHT_MODEL__ORION AND sMissionYachtData[ciYACHT_LOBBY_HOST_YACHT_INDEX].Appearance.iOption != ciYACHT_MODEL__AQUARIUS
		BREAK
		CASE SPAWN_CONDITION_FLAG_PISCES_OR_AQUARIUS_YACHT
			RETURN sMissionYachtData[ciYACHT_LOBBY_HOST_YACHT_INDEX].Appearance.iOption != ciYACHT_MODEL__PISCES AND sMissionYachtData[ciYACHT_LOBBY_HOST_YACHT_INDEX].Appearance.iOption != ciYACHT_MODEL__AQUARIUS
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL MC_IS_SPAWN_CONDITION_FLAG_MET_DURING_GAMEPLAY(SPAWN_CONDITION_FLAG eSpawnCondition)
	
	SWITCH eSpawnCondition
		CASE SPAWN_CONDITION_FLAG_ONLY_SPAWN_AFTER_INITIAL_CREATION
		CASE SPAWN_CONDITION_FLAG_MORNING_TIME
		CASE SPAWN_CONDITION_FLAG_NOON_TIME
		CASE SPAWN_CONDITION_FLAG_NIGHT_TIME
		CASE SPAWN_CONDITION_FLAG_MIDNIGHT_TIME
		CASE SPAWN_CONDITION_FLAG_MORNING_OR_NOON_TIME
		CASE SPAWN_CONDITION_FLAG_NIGHT_OR_MIDNIGHT_TIME		
		CASE SPAWN_CONDITION_FLAG_NOT_AGGROED_ANY
		CASE SPAWN_CONDITION_FLAG_AGGROED_ANY
		CASE SPAWN_CONDITION_FLAG_NOT_AGGROED_INDEXES
		CASE SPAWN_CONDITION_FLAG_AGGROED_INDEXES		
		CASE SPAWN_CONDITION_FLAG_WANTED_LEVEL_GREATER_THAN_EQUAL_TO_1
		CASE SPAWN_CONDITION_FLAG_WANTED_LEVEL_GREATER_THAN_EQUAL_TO_2
		CASE SPAWN_CONDITION_FLAG_WANTED_LEVEL_GREATER_THAN_EQUAL_TO_3
		CASE SPAWN_CONDITION_FLAG_WANTED_LEVEL_GREATER_THAN_EQUAL_TO_4
		CASE SPAWN_CONDITION_FLAG_WANTED_LEVEL_GREATER_THAN_EQUAL_TO_5
		CASE SPAWN_CONDITION_FLAG_RIDE_ALONG_ABILITY_ACTIVATED
		CASE SPAWN_CONDITION_FLAG_AMBUSH_ABILITY_ACTIVATED
		CASE SPAWN_CONDITION_FLAG_DISTRACTION_ABILITY_ACTIVATED
			PRINTLN("[SpawnConditionFlag] MC_IS_SPAWN_CONDITION_FLAG_MET_DURING_GAMEPLAY | eSpawnCondition: ", GET_SPAWN_CONDITION_FLAG_NAME_FOR_PRINT(eSpawnCondition), " true")
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL MC_ARE_SPAWN_CONDITION_FLAGS_MET_DURING_GAMEPLAY(SPAWN_CONDITION_FLAG  &eSpawnCondition[])
	
	INT iFlag = 0
	FOR iFlag = 0 TO ciSPAWN_CONDITION_FLAGS_MAX-1
		IF MC_IS_SPAWN_CONDITION_FLAG_MET_DURING_GAMEPLAY(eSpawnCondition[iFlag])
			RETURN TRUE
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC

FUNC SIMPLE_INTERIORS GET_OWNED_SIMPLE_INTERIOR_FROM_SPAWN_FLAG(SPAWN_CONDITION_FLAG eSpawnCondition)
	
	SWITCH eSpawnCondition	
		
		CASE SPAWN_CONDITION_FLAG_USING_PROPERTY_HQ_HAWICK
			RETURN SIMPLE_INTERIOR_FIXER_HQ_HAWICK
		BREAK		
		CASE SPAWN_CONDITION_FLAG_USING_PROPERTY_HQ_ROCKFORD
			RETURN SIMPLE_INTERIOR_FIXER_HQ_ROCKFORD
		BREAK		
		CASE SPAWN_CONDITION_FLAG_USING_PROPERTY_HQ_SEOUL
			RETURN SIMPLE_INTERIOR_FIXER_HQ_SEOUL
		BREAK		
		CASE SPAWN_CONDITION_FLAG_USING_PROPERTY_HQ_VESPUCCI
			RETURN SIMPLE_INTERIOR_FIXER_HQ_VESPUCCI
		BREAK
		
		CASE SPAWN_CONDITION_FLAG_AUTO_SHOP_LA_MESA 	
			RETURN SIMPLE_INTERIOR_AUTO_SHOP_LA_MESA
		BREAK
		CASE SPAWN_CONDITION_FLAG_AUTO_SHOP_STRAWBERRY 
			RETURN SIMPLE_INTERIOR_AUTO_SHOP_STRAWBERRY
		BREAK
		CASE SPAWN_CONDITION_FLAG_AUTO_SHOP_BURTON
			RETURN SIMPLE_INTERIOR_AUTO_SHOP_BURTON
		BREAK
		CASE SPAWN_CONDITION_FLAG_AUTO_SHOP_RANCHO
			RETURN SIMPLE_INTERIOR_AUTO_SHOP_RANCHO
		BREAK
		CASE SPAWN_CONDITION_FLAG_AUTO_SHOP_MISSION_ROW
			RETURN SIMPLE_INTERIOR_AUTO_SHOP_MISSION_ROW
		BREAK	
		
	ENDSWITCH
	
	RETURN SIMPLE_INTERIOR_INVALID
ENDFUNC

FUNC BOOL MC_SHOULD_BLOCK_SPAWN_BASED_ON_SPAWN_CONDITION_FLAGS(SPAWN_CONDITION_FLAG &eSpawnCondition[], BOOL bInitialSpawnCheck, BOOL bSpawnLaterCheck, INT iAggrosBS = 0, INT iTeam = -1)
	
	INT iPart, iPartTarget
	INT iTime	
	INT iTeamLoop
	INT iFlag = 0
	FOR iFlag = 0 TO ciSPAWN_CONDITION_FLAGS_MAX-1
			
		IF MC_IS_SPAWN_CONDITION_FLAG_MET_DURING_GAMEPLAY(eSpawnCondition[iFlag])
			// Don't block if it's a Spawn Later check. We want that function to return true.
			IF bSpawnLaterCheck
				RELOOP
			ENDIF
			
			// Block from spawning at the start as conditions will be a false positive.
			IF bInitialSpawnCheck
			AND NOT IS_BIT_SET(MC_serverbd.iServerBitSet, SBBOOL_FIRST_UPDATE_DONE)
				RETURN TRUE
			ENDIF
		ENDIF
		
		SWITCH eSpawnCondition[iFlag]
			CASE SPAWN_CONDITION_FLAG_OBJECT_ART
				IF g_sCasinoHeistMissionConfigData.eTarget = CASINO_HEIST_TARGET_TYPE__ART
				OR GET_PLAYER_CASINO_HEIST_TARGET(GB_GET_LOCAL_PLAYER_GANG_BOSS()) = CASINO_HEIST_TARGET_TYPE__ART
					PRINTLN("[SpawnConditionFlag] | eSpawnCondition: ", GET_SPAWN_CONDITION_FLAG_NAME_FOR_PRINT(eSpawnCondition[iFlag]), " Blocking.")
					RETURN TRUE
				ENDIF
			BREAK
			
			CASE SPAWN_CONDITION_FLAG_OBJECT_NOT_ART
				IF g_sCasinoHeistMissionConfigData.eTarget != CASINO_HEIST_TARGET_TYPE__ART
					PRINTLN("[SpawnConditionFlag] | eSpawnCondition: ", GET_SPAWN_CONDITION_FLAG_NAME_FOR_PRINT(eSpawnCondition[iFlag]), " Blocking.")
					RETURN TRUE
				ENDIF
			BREAK
			
			CASE SPAWN_CONDITION_FLAG_NEVER_SPAWN
				PRINTLN("[SpawnConditionFlag] | eSpawnCondition: ", GET_SPAWN_CONDITION_FLAG_NAME_FOR_PRINT(eSpawnCondition[iFlag]), " Blocking.")
				RETURN TRUE
			BREAK
				
			CASE SPAWN_CONDITION_FLAG_ORION_YACHT
			CASE SPAWN_CONDITION_FLAG_PISCES_YACHT
			CASE SPAWN_CONDITION_FLAG_AQUARIUS_YACHT
			CASE SPAWN_CONDITION_FLAG_ORION_OR_PISCES_YACHT
			CASE SPAWN_CONDITION_FLAG_ORION_OR_AQUARIUS_YACHT
			CASE SPAWN_CONDITION_FLAG_PISCES_OR_AQUARIUS_YACHT
				RETURN MC_SHOULD_BLOCK_SPAWN_DUE_TO_PLAYER_YACHT(eSpawnCondition[iFlag])
			BREAK
			
			CASE SPAWN_CONDITION_FLAG_MORNING_TIME				
				iTime = GET_CLOCK_HOURS() // No Consts here.
				PRINTLN("[SpawnConditionFlag] | eSpawnCondition: ", GET_SPAWN_CONDITION_FLAG_NAME_FOR_PRINT(eSpawnCondition[iFlag]), " iTime in Hours: ", iTime)
				IF iTime < 6
				OR iTime > 12
					PRINTLN("[SpawnConditionFlag] | eSpawnCondition: ", GET_SPAWN_CONDITION_FLAG_NAME_FOR_PRINT(eSpawnCondition[iFlag]), " BLOCKING!")
					RETURN TRUE
				ENDIF
			BREAK	
			CASE SPAWN_CONDITION_FLAG_NOON_TIME		
				iTime = GET_CLOCK_HOURS() // No Consts here.
				PRINTLN("[SpawnConditionFlag] | eSpawnCondition: ", GET_SPAWN_CONDITION_FLAG_NAME_FOR_PRINT(eSpawnCondition[iFlag]), " iTime in Hours: ", iTime)
				IF iTime < 12
				OR iTime > 21
					PRINTLN("[SpawnConditionFlag] | eSpawnCondition: ", GET_SPAWN_CONDITION_FLAG_NAME_FOR_PRINT(eSpawnCondition[iFlag]), " BLOCKING!")
					RETURN TRUE
				ENDIF
			BREAK
			CASE SPAWN_CONDITION_FLAG_NIGHT_TIME
				iTime = GET_CLOCK_HOURS() // No Consts here.
				PRINTLN("[SpawnConditionFlag] | eSpawnCondition: ", GET_SPAWN_CONDITION_FLAG_NAME_FOR_PRINT(eSpawnCondition[iFlag]), " iTime in Hours: ", iTime)
				IF iTime < 21
					PRINTLN("[SpawnConditionFlag] | eSpawnCondition: ", GET_SPAWN_CONDITION_FLAG_NAME_FOR_PRINT(eSpawnCondition[iFlag]), " BLOCKING!")
					RETURN TRUE
				ENDIF
			BREAK
			CASE SPAWN_CONDITION_FLAG_MIDNIGHT_TIME
				iTime = GET_CLOCK_HOURS() // No Consts here.
				PRINTLN("[SpawnConditionFlag] | eSpawnCondition: ", GET_SPAWN_CONDITION_FLAG_NAME_FOR_PRINT(eSpawnCondition[iFlag]), " iTime in Hours: ", iTime)
				IF iTime < 24
				AND iTime > 6
					PRINTLN("[SpawnConditionFlag] | eSpawnCondition: ", GET_SPAWN_CONDITION_FLAG_NAME_FOR_PRINT(eSpawnCondition[iFlag]), " BLOCKING!")
					RETURN TRUE
				ENDIF
			BREAK
			CASE SPAWN_CONDITION_FLAG_MORNING_OR_NOON_TIME
				iTime = GET_CLOCK_HOURS() // No Consts here.
				PRINTLN("[SpawnConditionFlag] | eSpawnCondition: ", GET_SPAWN_CONDITION_FLAG_NAME_FOR_PRINT(eSpawnCondition[iFlag]), " iTime in Hours: ", iTime)
				IF iTime < 6
				OR iTime > 21
					PRINTLN("[SpawnConditionFlag] | eSpawnCondition: ", GET_SPAWN_CONDITION_FLAG_NAME_FOR_PRINT(eSpawnCondition[iFlag]), " BLOCKING!")
					RETURN TRUE
				ENDIF
			BREAK
			CASE SPAWN_CONDITION_FLAG_NIGHT_OR_MIDNIGHT_TIME
				iTime = GET_CLOCK_HOURS() // No Consts here.
				PRINTLN("[SpawnConditionFlag] | eSpawnCondition: ", GET_SPAWN_CONDITION_FLAG_NAME_FOR_PRINT(eSpawnCondition[iFlag]), " iTime in Hours: ", iTime)
				IF iTime < 21
				AND iTime > 6
					PRINTLN("[SpawnConditionFlag] | eSpawnCondition: ", GET_SPAWN_CONDITION_FLAG_NAME_FOR_PRINT(eSpawnCondition[iFlag]), " Time is wrong and not NIGHT_OR_MIDNIGHT - BLOCKING!")
					RETURN TRUE
				ENDIF
			BREAK
			CASE SPAWN_CONDITION_FLAG_PIRATE_RADIO_NOT_COLLECTED
				#IF FEATURE_HEIST_ISLAND
				IF IS_COLLECTABLE_COLLECTED(COLLECTABLE_USB_PIRATE_RADIO,ENUM_TO_INT( USB_COLLECTABLE_LOCATIONS_ISLAND_COMPOUND_FINALE))
					PRINTLN("[SpawnConditionFlag] | eSpawnCondition: ", eSpawnCondition[iFlag], " BLOCKING!")
					RETURN TRUE
				ENDIF
				#ENDIF
			BREAK
			CASE SPAWN_CONDITION_FLAG_ONLY_SPAWN_AFTER_INITIAL_CREATION
				IF GET_MC_SERVER_GAME_STATE() != GAME_STATE_RUNNING
				AND bInitialSpawnCheck
					PRINTLN("[SpawnConditionFlag] | eSpawnCondition: ", GET_SPAWN_CONDITION_FLAG_NAME_FOR_PRINT(eSpawnCondition[iFlag]), " Not in Game State Running - BLOCKING!")
					RETURN TRUE
				ENDIF
			BREAK
			CASE SPAWN_CONDITION_FLAG_NOT_AGGROED_ANY				
				IF iTeam = -1
					IF HAS_ANY_TEAM_TRIGGERED_AGGRO(ciMISSION_AGGRO_INDEX_ALL)
						PRINTLN("[SpawnConditionFlag] | eSpawnCondition: ", GET_SPAWN_CONDITION_FLAG_NAME_FOR_PRINT(eSpawnCondition[iFlag]), " Any Team Has Any Server Aggro Index Active.")
						RETURN TRUE
					ENDIF
				ELSE
					IF HAS_TEAM_TRIGGERED_AGGRO(iTeam, ciMISSION_AGGRO_INDEX_ALL)
						PRINTLN("[SpawnConditionFlag] | eSpawnCondition: ", GET_SPAWN_CONDITION_FLAG_NAME_FOR_PRINT(eSpawnCondition[iFlag]), " Team: ", iTeam, " Has Any Server Aggro Index Active.")
						RETURN TRUE
					ENDIF
				ENDIF
			BREAK
			CASE SPAWN_CONDITION_FLAG_AGGROED_ANY
				IF iTeam = -1
					IF NOT HAS_ANY_TEAM_TRIGGERED_AGGRO(ciMISSION_AGGRO_INDEX_ALL)
						PRINTLN("[SpawnConditionFlag] | eSpawnCondition: ", GET_SPAWN_CONDITION_FLAG_NAME_FOR_PRINT(eSpawnCondition[iFlag]), " Any Team does not have Any Server Aggro Index Active.")
						RETURN TRUE
					ENDIF
				ELSE
					IF NOT HAS_TEAM_TRIGGERED_AGGRO(iTeam, ciMISSION_AGGRO_INDEX_ALL)
						PRINTLN("[SpawnConditionFlag] | eSpawnCondition: ", GET_SPAWN_CONDITION_FLAG_NAME_FOR_PRINT(eSpawnCondition[iFlag]), " Team: ", iTeam, " does not have Any Server Aggro Index Active.")
						RETURN TRUE
					ENDIF
				ENDIF
			BREAK
			CASE SPAWN_CONDITION_FLAG_NOT_AGGROED_INDEXES
				IF iTeam = -1
					IF HAS_ANY_TEAM_TRIGGERED_AGGRO(iAggrosBS)
						PRINTLN("[SpawnConditionFlag] | eSpawnCondition: ", GET_SPAWN_CONDITION_FLAG_NAME_FOR_PRINT(eSpawnCondition[iFlag]), " Any Team has the Entity Aggro Index  active: ", iAggrosBS)
						RETURN TRUE
					ENDIF
				ELSE
					IF HAS_TEAM_TRIGGERED_AGGRO(iTeam, iAggrosBS)
						PRINTLN("[SpawnConditionFlag] | eSpawnCondition: ", GET_SPAWN_CONDITION_FLAG_NAME_FOR_PRINT(eSpawnCondition[iFlag]), " Team: ", iTeam, " has the Entity Aggro Index active: ", iAggrosBS)
						RETURN TRUE
					ENDIF
				ENDIF
			BREAK
			CASE SPAWN_CONDITION_FLAG_AGGROED_INDEXES
				IF iTeam = -1
					IF NOT HAS_ANY_TEAM_TRIGGERED_AGGRO(iAggrosBS)
						PRINTLN("[SpawnConditionFlag] | eSpawnCondition: ", GET_SPAWN_CONDITION_FLAG_NAME_FOR_PRINT(eSpawnCondition[iFlag]), " Any Team does not have the Entity Aggro Index active: ", iAggrosBS)
						RETURN TRUE
					ENDIF
				ELSE
					IF NOT HAS_TEAM_TRIGGERED_AGGRO(iTeam, iAggrosBS)	
						PRINTLN("[SpawnConditionFlag] | eSpawnCondition: ", GET_SPAWN_CONDITION_FLAG_NAME_FOR_PRINT(eSpawnCondition[iFlag]), " Team: ", iTeam, " does not have the Entity Aggro Index active: ", iAggrosBS)
						RETURN TRUE
					ENDIF
				ENDIF
			BREAK
			CASE SPAWN_CONDITION_FLAG_WANTED_LEVEL_GREATER_THAN_EQUAL_TO_1
			CASE SPAWN_CONDITION_FLAG_WANTED_LEVEL_GREATER_THAN_EQUAL_TO_2
			CASE SPAWN_CONDITION_FLAG_WANTED_LEVEL_GREATER_THAN_EQUAL_TO_3
			CASE SPAWN_CONDITION_FLAG_WANTED_LEVEL_GREATER_THAN_EQUAL_TO_4
			CASE SPAWN_CONDITION_FLAG_WANTED_LEVEL_GREATER_THAN_EQUAL_TO_5
				INT iWantedLevel
				iWantedLevel = 5 + (ENUM_TO_INT(eSpawnCondition[iFlag]) - ENUM_TO_INT(SPAWN_CONDITION_FLAG_WANTED_LEVEL_GREATER_THAN_EQUAL_TO_5))
				
				//PRINTLN("MC_SHOULD_BLOCK_SPAWN_BASED_ON_SPAWN_CONDITION_FLAGS | eSpawnCondition: ", GET_SPAWN_CONDITION_FLAG_NAME_FOR_PRINT(eSpawnCondition[iFlag]), " Team: ", iTeam, " iWantedLevel: ", iWantedLevel, " MC_serverBD.iNumberOfTeams: ", MC_serverBD.iNumberOfTeams, " g_FMMC_STRUCT.iNumberOfTeams: ", g_FMMC_STRUCT.iNumberOfTeams)
				
				IF iTeam = -1
					INT iNumberOfTeams
					iNumberOfTeams = MC_serverBD.iNumberOfTeams
					IF iNumberOfTeams < 1
						iNumberOfTeams = GET_NUMBER_OF_TEAMS_FOR_PRE_GAME()
					ENDIF
					
					FOR iTeamLoop = 0 TO iNumberOfTeams - 1						
						//PRINTLN("MC_SHOULD_BLOCK_SPAWN_BASED_ON_SPAWN_CONDITION_FLAGS | eSpawnCondition: ", GET_SPAWN_CONDITION_FLAG_NAME_FOR_PRINT(eSpawnCondition[iFlag]), " MC_serverBD.iNumOfHighestWanted[iTeam]: ", MC_serverBD.iNumOfHighestWanted[iTeam], " MC_serverBD.iNumOfHighestFakeWanted[iTeam]: ", MC_serverBD.iNumOfHighestFakeWanted[iTeam])
						
						IF MC_serverBD.iNumOfHighestWanted[iTeamLoop] < iWantedLevel
						AND MC_serverBD.iNumOfHighestFakeWanted[iTeamLoop] < iWantedLevel
							PRINTLN("[SpawnConditionFlag] | eSpawnCondition: ", GET_SPAWN_CONDITION_FLAG_NAME_FOR_PRINT(eSpawnCondition[iFlag]), " Team: ", iTeamLoop, " does not have a wanted level of at least: ", iWantedLevel)
							RETURN TRUE
						ENDIF
					ENDFOR
				ELSE
					IF MC_serverBD.iNumOfHighestWanted[iTeam] < iWantedLevel
					AND MC_serverBD.iNumOfHighestFakeWanted[iTeam] < iWantedLevel
						PRINTLN("[SpawnConditionFlag] | eSpawnCondition: ", GET_SPAWN_CONDITION_FLAG_NAME_FOR_PRINT(eSpawnCondition[iFlag]), " Team: ", iTeam, " does not have a wanted level of at least: ", iWantedLevel)
						RETURN TRUE
					ENDIF
				ENDIF
			BREAK
			
			CASE SPAWN_CONDITION_FLAG_RIDE_ALONG_ABILITY_ACTIVATED
				IF NOT IS_PLAYER_ABILITY_ACTIVE_FOR_ANY_PLAYER_ON_MY_TEAM(PA_RIDE_ALONG)
					PRINTLN("[PlayerAbilities_SPAM] [SpawnConditionFlag] | eSpawnCondition: ", GET_SPAWN_CONDITION_FLAG_NAME_FOR_PRINT(eSpawnCondition[iFlag]), " Team: ", iTeam, " 'Ride Along' hasn't been used | sLocalPlayerAbilities.sAbilities[PA_RIDE_ALONG].eState: ", sLocalPlayerAbilities.sAbilities[PA_RIDE_ALONG].eState)
					RETURN TRUE
				ENDIF
			BREAK
			
			CASE SPAWN_CONDITION_FLAG_AMBUSH_ABILITY_ACTIVATED
				IF NOT IS_PLAYER_ABILITY_ACTIVE_FOR_ANY_PLAYER_ON_MY_TEAM(PA_AMBUSH)
					PRINTLN("[PlayerAbilities_SPAM] [SpawnConditionFlag] | eSpawnCondition: ", GET_SPAWN_CONDITION_FLAG_NAME_FOR_PRINT(eSpawnCondition[iFlag]), " Team: ", iTeam, " 'Ambush' hasn't been used | sLocalPlayerAbilities.sAbilities[PA_AMBUSH].eState: ", sLocalPlayerAbilities.sAbilities[PA_AMBUSH].eState)
					RETURN TRUE
				ENDIF
				
				IF sLocalPlayerAbilities.sAmbushData.iWarpPositionToUse = -1
					PRINTLN("[PlayerAbilities_SPAM] [SpawnConditionFlag] | eSpawnCondition: ", GET_SPAWN_CONDITION_FLAG_NAME_FOR_PRINT(eSpawnCondition[iFlag]), " Team: ", iTeam, " 'Ambush' hasn't been used | iWarpPositionToUse is -1")
					RETURN TRUE
				ENDIF
			BREAK
			
			CASE SPAWN_CONDITION_FLAG_DISTRACTION_ABILITY_ACTIVATED
				IF NOT IS_PLAYER_ABILITY_ACTIVE_FOR_ANY_PLAYER_ON_MY_TEAM(PA_DISTRACTION)
					PRINTLN("[PlayerAbilities_SPAM] [SpawnConditionFlag] | eSpawnCondition: ", GET_SPAWN_CONDITION_FLAG_NAME_FOR_PRINT(eSpawnCondition[iFlag]), " Team: ", iTeam, " 'Distraction' hasn't been used | sLocalPlayerAbilities.sAbilities[PA_DISTRACTION].eState: ", sLocalPlayerAbilities.sAbilities[PA_DISTRACTION].eState)
					RETURN TRUE
				ENDIF
			BREAK
			
			CASE SPAWN_CONDITION_FLAG_I_AM_PLAYER_0
			CASE SPAWN_CONDITION_FLAG_I_AM_PLAYER_1
			CASE SPAWN_CONDITION_FLAG_I_AM_PLAYER_2
			CASE SPAWN_CONDITION_FLAG_I_AM_PLAYER_3
				PRINTLN("[SpawnConditionFlag] | eSpawnCondition: ", GET_SPAWN_CONDITION_FLAG_NAME_FOR_PRINT(eSpawnCondition[iFlag]), " CHECKING!! ")
				iPart = 3 + (ENUM_TO_INT(eSpawnCondition[iFlag]) - ENUM_TO_INT(SPAWN_CONDITION_FLAG_I_AM_PLAYER_3))
				iPart = GET_ACTIVE_PARTICIPANT_INDEX_FROM_NUMBER(iPart)
				iPartTarget = iLocalPart
				
				IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPart))
					PRINTLN("[SpawnConditionFlag] | eSpawnCondition: ", GET_SPAWN_CONDITION_FLAG_NAME_FOR_PRINT(eSpawnCondition[iFlag]), " BLOCKED! (1)")
					RETURN TRUE
				ENDIF
				
				IF iPart = -1 
					PRINTLN("[SpawnConditionFlag] | eSpawnCondition: ", GET_SPAWN_CONDITION_FLAG_NAME_FOR_PRINT(eSpawnCondition[iFlag]), " BLOCKED! (2)")
					RETURN TRUE
				ELSE
					IF iPart != iPartTarget
						PRINTLN("[SpawnConditionFlag] | eSpawnCondition: ", GET_SPAWN_CONDITION_FLAG_NAME_FOR_PRINT(eSpawnCondition[iFlag]), " BLOCKED! (3)")
						RETURN TRUE
					ENDIF
				ENDIF
			BREAK
			
			CASE SPAWN_CONDITION_FLAG_PERSONAL_VEHICLE_SELECTED_IN_LOBBY_PLAYER_0
			CASE SPAWN_CONDITION_FLAG_PERSONAL_VEHICLE_SELECTED_IN_LOBBY_PLAYER_1
			CASE SPAWN_CONDITION_FLAG_PERSONAL_VEHICLE_SELECTED_IN_LOBBY_PLAYER_2
			CASE SPAWN_CONDITION_FLAG_PERSONAL_VEHICLE_SELECTED_IN_LOBBY_PLAYER_3				
				iPart = 3 + (ENUM_TO_INT(eSpawnCondition[iFlag]) - ENUM_TO_INT(SPAWN_CONDITION_FLAG_PERSONAL_VEHICLE_SELECTED_IN_LOBBY_PLAYER_3))				
				iPart = GET_ACTIVE_PARTICIPANT_INDEX_FROM_NUMBER(iPart) // Handles gaps.				
				
				IF iPart = -1
					PRINTLN("[SpawnConditionFlag] | eSpawnCondition: ", GET_SPAWN_CONDITION_FLAG_NAME_FOR_PRINT(eSpawnCondition[iFlag]), " BLOCKED! (1)")
					RETURN TRUE
				ENDIF
							
				IF NOT IS_PLAYER_SELECTING_CUSTOM_VEHICLE(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPart)))
					PRINTLN("[SpawnConditionFlag] | eSpawnCondition: ", GET_SPAWN_CONDITION_FLAG_NAME_FOR_PRINT(eSpawnCondition[iFlag]), " BLOCKED! (2), GET_PLAYER_NAME(", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPart))), ")")
					RETURN TRUE
				ENDIF
			BREAK
			
			CASE SPAWN_CONDITION_FLAG_PLACED_VEHICLE_SELECTED_IN_LOBBY_PLAYER_0
			CASE SPAWN_CONDITION_FLAG_PLACED_VEHICLE_SELECTED_IN_LOBBY_PLAYER_1
			CASE SPAWN_CONDITION_FLAG_PLACED_VEHICLE_SELECTED_IN_LOBBY_PLAYER_2
			CASE SPAWN_CONDITION_FLAG_PLACED_VEHICLE_SELECTED_IN_LOBBY_PLAYER_3
				iPart = 3 + (ENUM_TO_INT(eSpawnCondition[iFlag]) - ENUM_TO_INT(SPAWN_CONDITION_FLAG_PLACED_VEHICLE_SELECTED_IN_LOBBY_PLAYER_3))				
				iPart = GET_ACTIVE_PARTICIPANT_INDEX_FROM_NUMBER(iPart) // Handles gaps.				
				
				IF iPart = -1
					PRINTLN("[SpawnConditionFlag] | eSpawnCondition: ", GET_SPAWN_CONDITION_FLAG_NAME_FOR_PRINT(eSpawnCondition[iFlag]), " BLOCKED! (1)")
					RETURN TRUE
				ENDIF
				
				IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPart))					
					PRINTLN("[SpawnConditionFlag] | eSpawnCondition: ", GET_SPAWN_CONDITION_FLAG_NAME_FOR_PRINT(eSpawnCondition[iFlag]), " BLOCKED! (2)")
					RETURN TRUE
				ENDIF
				
				IF IS_PLAYER_SELECTING_CUSTOM_VEHICLE(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPart)))
					PRINTLN("[SpawnConditionFlag] | eSpawnCondition: ", GET_SPAWN_CONDITION_FLAG_NAME_FOR_PRINT(eSpawnCondition[iFlag]), " BLOCKED! (3), GET_PLAYER_NAME(", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPart))), ")")
					RETURN TRUE
				ENDIF
			BREAK
			
			CASE SPAWN_CONDITION_FLAG_ONE_PLAYER_PLAYING
				IF GET_TOTAL_STARTING_PLAYERS() != 1
					PRINTLN("[SpawnConditionFlag] | eSpawnCondition: ", GET_SPAWN_CONDITION_FLAG_NAME_FOR_PRINT(eSpawnCondition[iFlag]), " BLOCKED! (3)")
					RETURN TRUE
				ENDIF
			BREAK
			
			CASE SPAWN_CONDITION_FLAG_MORE_THAN_ONE_PLAYER_PLAYING
				IF GET_TOTAL_STARTING_PLAYERS() <= 1
					PRINTLN("[SpawnConditionFlag] | eSpawnCondition: ", GET_SPAWN_CONDITION_FLAG_NAME_FOR_PRINT(eSpawnCondition[iFlag]), " BLOCKED! (3)")
					RETURN TRUE
				ENDIF
			BREAK
							
			CASE SPAWN_CONDITION_FLAG_USING_PROPERTY_HQ_HAWICK		
			CASE SPAWN_CONDITION_FLAG_USING_PROPERTY_HQ_ROCKFORD
			CASE SPAWN_CONDITION_FLAG_USING_PROPERTY_HQ_SEOUL
			CASE SPAWN_CONDITION_FLAG_USING_PROPERTY_HQ_VESPUCCI
			CASE SPAWN_CONDITION_FLAG_AUTO_SHOP_LA_MESA
			CASE SPAWN_CONDITION_FLAG_AUTO_SHOP_STRAWBERRY
			CASE SPAWN_CONDITION_FLAG_AUTO_SHOP_BURTON
			CASE SPAWN_CONDITION_FLAG_AUTO_SHOP_RANCHO
			CASE SPAWN_CONDITION_FLAG_AUTO_SHOP_MISSION_ROW
				IF GB_GET_LOCAL_PLAYER_GANG_BOSS_AS_INT() = -1
					PRINTLN("[SpawnConditionFlag] | eSpawnCondition: ", GET_SPAWN_CONDITION_FLAG_NAME_FOR_PRINT(eSpawnCondition[iFlag]), " BLOCKED!")
					RETURN TRUE
				ENDIF
				#IF FEATURE_TUNER
				SIMPLE_INTERIORS siAutoshop
				siAutoshop = GET_OWNED_SIMPLE_INTERIOR_FROM_SPAWN_FLAG(eSpawnCondition[iFlag])
				IF GET_OWNED_AUTO_SHOP_SIMPLE_INTERIOR(GB_GET_LOCAL_PLAYER_GANG_BOSS()) != siAutoshop		
					PRINTLN("[SpawnConditionFlag] | eSpawnCondition: ", GET_SPAWN_CONDITION_FLAG_NAME_FOR_PRINT(eSpawnCondition[iFlag]), " BLOCKED!")
					RETURN TRUE
				ENDIF
				#ENDIF	
			BREAK
			
			CASE SPAWN_CONDITION_FLAG_NDONE_CASNIO_P1_AND_NDONE_DIAMOND_HEIST
				IF HAS_PLAYER_COMPLETED_THIS_VCM(GET_FMMC_LOBBY_LEADER(), eVCM_FINALE)
				OR HAS_PLAYER_COMPLETED_CASINO_HEIST(GET_FMMC_LOBBY_LEADER(), CASINO_HEIST_APPROACH_TYPE__NOT_CHOSEN)
					PRINTLN("[SpawnConditionFlag] | eSpawnCondition: ", GET_SPAWN_CONDITION_FLAG_NAME_FOR_PRINT(eSpawnCondition[iFlag]), " BLOCKED!")
					RETURN TRUE
				ENDIF
			BREAK
			CASE SPAWN_CONDITION_FLAG_DONE_CASNIO_P1_AND_NDONE_DIAMOND_HEIST
				IF NOT HAS_PLAYER_COMPLETED_THIS_VCM(GET_FMMC_LOBBY_LEADER(), eVCM_FINALE)
				OR HAS_PLAYER_COMPLETED_CASINO_HEIST(GET_FMMC_LOBBY_LEADER(), CASINO_HEIST_APPROACH_TYPE__NOT_CHOSEN)
					PRINTLN("[SpawnConditionFlag] | eSpawnCondition: ", GET_SPAWN_CONDITION_FLAG_NAME_FOR_PRINT(eSpawnCondition[iFlag]), " BLOCKED!")
					RETURN TRUE
				ENDIF
			BREAK
			CASE SPAWN_CONDITION_FLAG_NDONE_CASNIO_P1_AND_DONE_DIAMOND_HEIST
				IF HAS_PLAYER_COMPLETED_THIS_VCM(GET_FMMC_LOBBY_LEADER(), eVCM_FINALE)
				OR NOT HAS_PLAYER_COMPLETED_CASINO_HEIST(GET_FMMC_LOBBY_LEADER(), CASINO_HEIST_APPROACH_TYPE__NOT_CHOSEN)
					PRINTLN("[SpawnConditionFlag] | eSpawnCondition: ", GET_SPAWN_CONDITION_FLAG_NAME_FOR_PRINT(eSpawnCondition[iFlag]), " BLOCKED!")
					RETURN TRUE
				ENDIF
			BREAK
			CASE SPAWN_CONDITION_FLAG_DONE_CASNIO_P1_AND_DONE_DIAMOND_HEIST
				IF NOT HAS_PLAYER_COMPLETED_THIS_VCM(GET_FMMC_LOBBY_LEADER(), eVCM_FINALE)
				OR NOT HAS_PLAYER_COMPLETED_CASINO_HEIST(GET_FMMC_LOBBY_LEADER(), CASINO_HEIST_APPROACH_TYPE__NOT_CHOSEN)
					PRINTLN("[SpawnConditionFlag] | eSpawnCondition: ", GET_SPAWN_CONDITION_FLAG_NAME_FOR_PRINT(eSpawnCondition[iFlag]), " BLOCKED!")
					RETURN TRUE
				ENDIF
			BREAK
			CASE SPAWN_CONDITION_FLAG_DONE_CASNIO_P1_OR_DONE_DIAMOND_HEIST
				IF NOT HAS_PLAYER_COMPLETED_THIS_VCM(GET_FMMC_LOBBY_LEADER(), eVCM_FINALE)
				AND NOT HAS_PLAYER_COMPLETED_CASINO_HEIST(GET_FMMC_LOBBY_LEADER(), CASINO_HEIST_APPROACH_TYPE__NOT_CHOSEN)
					PRINTLN("[SpawnConditionFlag] | eSpawnCondition: ", GET_SPAWN_CONDITION_FLAG_NAME_FOR_PRINT(eSpawnCondition[iFlag]), " BLOCKED!")
					RETURN TRUE
				ENDIF
			BREAK
			CASE SPAWN_CONDITION_FLAG_DONE_DOOMSDAY_HEIST
				IF NOT HAS_PLAYER_COMPLETED_GANG_OPS_HEIST_STRAND(GET_FMMC_LOBBY_LEADER(), GANG_OPS_MISSION_STRAND_MISSILE_SILO)				
					PRINTLN("[SpawnConditionFlag] | eSpawnCondition: ", GET_SPAWN_CONDITION_FLAG_NAME_FOR_PRINT(eSpawnCondition[iFlag]), " BLOCKED!")
					RETURN TRUE
				ENDIF
			BREAK
			CASE SPAWN_CONDITION_FLAG_NDONE_DOOMSDAY_HEIST
				IF HAS_PLAYER_COMPLETED_GANG_OPS_HEIST_STRAND(GET_FMMC_LOBBY_LEADER(), GANG_OPS_MISSION_STRAND_MISSILE_SILO)
					PRINTLN("[SpawnConditionFlag] | eSpawnCondition: ", GET_SPAWN_CONDITION_FLAG_NAME_FOR_PRINT(eSpawnCondition[iFlag]), " BLOCKED!")
					RETURN TRUE
				ENDIF
			BREAK

			CASE SPAWN_CONDITION_FLAG_OWNS_ANY_SPECIAL_CARGO_WAREHOUSE_LEADER
				IF GET_FMMC_LOBBY_LEADER() != INVALID_PLAYER_INDEX()
					IF NOT DOES_PLAYER_OWN_A_WAREHOUSE(GET_FMMC_LOBBY_LEADER())
						PRINTLN("[SpawnConditionFlag] | eSpawnCondition: ", GET_SPAWN_CONDITION_FLAG_NAME_FOR_PRINT(eSpawnCondition[iFlag]), " BLOCKED!")
						RETURN TRUE
					ENDIF
				ENDIF
			BREAK
			CASE SPAWN_CONDITION_FLAG_OWNS_ANY_SPECIAL_CARGO_WAREHOUSE_LOCAL
				IF NOT DOES_PLAYER_OWN_A_WAREHOUSE(LocalPlayer)
					PRINTLN("[SpawnConditionFlag] | eSpawnCondition: ", GET_SPAWN_CONDITION_FLAG_NAME_FOR_PRINT(eSpawnCondition[iFlag]), " BLOCKED!")
					RETURN TRUE
				ENDIF
			BREAK
			CASE SPAWN_CONDITION_FLAG_DIFFICULTY_LEVEL_EASY 
				IF g_FMMC_STRUCT.iDifficulity != DIFF_EASY
					RETURN TRUE
				ENDIF
			BREAK
			CASE SPAWN_CONDITION_FLAG_DIFFICULTY_LEVEL_EASY_OR_MEDIUM 
				IF g_FMMC_STRUCT.iDifficulity != DIFF_EASY
				AND g_FMMC_STRUCT.iDifficulity != DIFF_NORMAL
					RETURN TRUE
				ENDIF
			BREAK
			CASE SPAWN_CONDITION_FLAG_DIFFICULTY_LEVEL_MEDIUM 
				IF g_FMMC_STRUCT.iDifficulity != DIFF_NORMAL			
					RETURN TRUE
				ENDIF
			BREAK
			CASE SPAWN_CONDITION_FLAG_DIFFICULTY_LEVEL_MEDIUM_OR_HARD 
				IF g_FMMC_STRUCT.iDifficulity != DIFF_NORMAL
				AND g_FMMC_STRUCT.iDifficulity != DIFF_HARD
					RETURN TRUE
				ENDIF
			BREAK
			CASE SPAWN_CONDITION_FLAG_DIFFICULTY_LEVEL_HARD 
				IF g_FMMC_STRUCT.iDifficulity != DIFF_HARD			
					RETURN TRUE
				ENDIF
			BREAK
			CASE SPAWN_CONDITION_FLAG_DIFFICULTY_LEVEL_HARD_OR_EASY 
				IF g_FMMC_STRUCT.iDifficulity != DIFF_HARD
				AND g_FMMC_STRUCT.iDifficulity != DIFF_EASY
					RETURN TRUE
				ENDIF
			BREAK
		ENDSWITCH
		
		IF IS_CONTENT_SPECIFIC_SPAWN_CONDITION_FLAG_SET(eSpawnCondition[iFlag])			
			RETURN TRUE
		ENDIF
	ENDFOR
	
	#IF IS_DEBUG_BUILD	
	FOR iFlag = 0 TO ciSPAWN_CONDITION_FLAGS_MAX-1
		IF eSpawnCondition[iFlag] != SPAWN_CONDITION_FLAG_NOT_SET
			PRINTLN("[SpawnConditionFlag] | eSpawnCondition: ", GET_SPAWN_CONDITION_FLAG_NAME_FOR_PRINT(eSpawnCondition[iFlag]), " NOT BLOCKED!")
			BREAKLOOP
		ENDIF
	ENDFOR
	#ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ENTITY_USING_THIS_SPAWN_CONDITION_FLAGS(SPAWN_CONDITION_FLAG &eFlagList[], SPAWN_CONDITION_FLAG eFlagToLookFor)
	INT iFlag = 0
	FOR iFlag = 0 TO ciSPAWN_CONDITION_FLAGS_MAX-1
		IF eFlagList[iFlag] = eFlagToLookFor
			RETURN TRUE
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC

// ##### -------------------------------------------------------------------------
// ##### Section Name: Entities - Spawn Groups (sub section start)
// ##### Description: Various helper functions & wrappers to do with Spawn Groups
// ##### -------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------

FUNC INT GET_FIRST_MATCH_SPAWN_GROUP_FROM_SERVER(INT iSpawnGroupBS)
	INT iMatchedSpawnBS = iSpawnGroupBS & MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeedBS
	INT i		
	
	FOR i = 0 TO ciMAX_NUM_FMMC_ENTITY_SUB_SPAWN_GROUPS-1
		IF IS_BIT_SET(iMatchedSpawnBS, i)
			RETURN i
		ENDIF
	ENDFOR
	
	RETURN -1
ENDFUNC

FUNC INT GET_FIRST_MATCH_SUB_SPAWN_GROUP_FROM_SERVER(INT iSpawnGroupIndex, INT iSubSpawnGroupBS)
	INT iMatchedSpawnBS = iSubSpawnGroupBS & MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeed_SubGroupBS[iSpawnGroupIndex]
	
	INT i
	FOR i = 0 TO ciMAX_NUM_FMMC_ENTITY_SUB_SPAWN_GROUPS-1
		IF IS_BIT_SET(iMatchedSpawnBS, i)
			RETURN i
		ENDIF
	ENDFOR
	
	RETURN -1
ENDFUNC

#IF IS_DEBUG_BUILD
FUNC STRING MC_GET_SPAWN_GROUP_ENTITY_TYPE_STRING(eSpawnGroupEntityType eType)
	SWITCH eType
		CASE eSGET_None				RETURN "eSGET_None"
		CASE eSGET_Ped				RETURN "eSGET_Ped"
		CASE eSGET_Vehicle			RETURN "eSGET_Vehicle"
		CASE eSGET_Object			RETURN "eSGET_Object"
		CASE eSGET_GotoLoc			RETURN "eSGET_GotoLoc"
		CASE eSGET_Bounds			RETURN "eSGET_Bounds"
		CASE eSGET_Weapon			RETURN "eSGET_Weapon"
		CASE eSGET_Dummyblip 		RETURN "eSGET_Dummyblip"
		CASE eSGET_Prop				RETURN "eSGET_Prop"
		CASE eSGET_DynoProp 		RETURN "eSGET_DynoProp"
		CASE eSGET_Interactable		RETURN "eSGET_Interactable"
		CASE eSGET_TeamSpawnPoint	RETURN "eSGET_TeamSpawnPoint"
		CASE eSGET_DialogueTrigger	RETURN "eSGET_DialogueTrigger"
		CASE eSGET_Zone				RETURN "eSGET_Zone"
		CASE eSGET_Train			RETURN "eSGET_Train"
		CASE eSGET_QRCPoints		RETURN "eSGET_QRCPoints"
	ENDSWITCH
	RETURN "Unknown"
ENDFUNC
#ENDIF

PROC CLEAR_SPAWN_GROUP_STRUCT(sFMMC_RandomSpawnGroupStruct &rsgSpawnStruct)
	
	PRINTLN("[SpawnGroups] CLEAR_SPAWN_GROUP_STRUCT called")
	DEBUG_PRINTCALLSTACK()
	
	sFMMC_RandomSpawnGroupStruct sEmpty
	rsgSpawnStruct = sEmpty
	
ENDPROC

PROC MC_SET_CACHED_DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP(BOOL bSuitable, INT iEntityID, eSpawnGroupEntityType eEntityType = eSGET_None, INT iTeam = -1)
	SWITCH eEntityType
		CASE eSGET_Ped
			FMMC_SET_LONG_BIT(iSpawnGroupBS_CheckedSpawnPed, iEntityID)
			IF bSuitable
				FMMC_SET_LONG_BIT(iSpawnGroupBS_CanSpawnPed, iEntityID)
			ENDIF
		BREAK
		CASE eSGET_Vehicle
			SET_BIT(iSpawnGroupBS_CheckedSpawnVeh, iEntityID)
			IF bSuitable
				SET_BIT(iSpawnGroupBS_CanSpawnVeh, iEntityID)
			ENDIF
		BREAK
		CASE eSGET_Object
			SET_BIT(iSpawnGroupBS_CheckedSpawnObj, iEntityID)
			IF bSuitable
				SET_BIT(iSpawnGroupBS_CanSpawnObj, iEntityID)
			ENDIF
		BREAK
		CASE eSGET_GotoLoc
			SET_BIT(iSpawnGroupBS_CheckedSpawnLoc, iEntityID)
			IF bSuitable
				SET_BIT(iSpawnGroupBS_CanSpawnLoc, iEntityID)
			ENDIF
		BREAK
		CASE eSGET_Bounds
		BREAK
		CASE eSGET_Weapon
			FMMC_SET_LONG_BIT(iSpawnGroupBS_CheckedSpawnWep, iEntityID)
			IF bSuitable
				FMMC_SET_LONG_BIT(iSpawnGroupBS_CanSpawnWep, iEntityID)
			ENDIF
		BREAK
		CASE eSGET_Dummyblip
			SET_BIT(iSpawnGroupBS_CheckedSpawnDummyBlip, iEntityID)
			IF bSuitable
				SET_BIT(iSpawnGroupBS_CanSpawnDummyBlip, iEntityID)	
			ENDIF
		BREAK
		CASE eSGET_Prop
			FMMC_SET_LONG_BIT(iSpawnGroupBS_CheckedSpawnProp, iEntityID)
			IF bSuitable
				FMMC_SET_LONG_BIT(iSpawnGroupBS_CanSpawnProp, iEntityID)
			ENDIF
		BREAK
		CASE eSGET_DynoProp
			SET_BIT(iSpawnGroupBS_CheckedSpawnDynoProp, iEntityID)
			IF bSuitable
				SET_BIT(iSpawnGroupBS_CanSpawnDynoProp, iEntityID)
			ENDIF
		BREAK
		CASE eSGET_Interactable
			FMMC_SET_LONG_BIT(iSpawnGroupBS_CheckedSpawnInteractable, iEntityID)
			IF bSuitable
				FMMC_SET_LONG_BIT(iSpawnGroupBS_CanSpawnInteractable, iEntityID)
			ENDIF
		BREAK
		CASE eSGET_TeamSpawnPoint
			IF iTeam > -1
				FMMC_SET_LONG_BIT(iSpawnGroupBS_CheckedSpawnTeamSpawnPoint[iTeam], iEntityID)
				IF bSuitable
					FMMC_SET_LONG_BIT(iSpawnGroupBS_CanSpawnTeamSpawnPoint[iTeam], iEntityID)
				ENDIF
			ENDIF
		BREAK
		CASE eSGET_DialogueTrigger
			IF iTeam > -1
				FMMC_SET_LONG_BIT(iSpawnGroupBS_CheckedSpawnDialogueTrigger[iTeam], iEntityID)
				IF bSuitable
					FMMC_SET_LONG_BIT(iSpawnGroupBS_CanSpawnDialogueTrigger[iTeam], iEntityID)
				ENDIF
			ENDIF
		BREAK
		CASE eSGET_Zone
			FMMC_SET_LONG_BIT(iSpawnGroupBS_CheckedSpawnZone, iEntityID)
			IF bSuitable
				FMMC_SET_LONG_BIT(iSpawnGroupBS_CanSpawnZone, iEntityID)
			ENDIF
		BREAK
		CASE eSGET_Train
			SET_BIT(iSpawnGroupBS_CheckedSpawnTrain, iEntityID)
			IF bSuitable
				SET_BIT(iSpawnGroupBS_CanSpawnTrain, iEntityID)
			ENDIF
		BREAK		
		CASE eSGET_QRCPoints
			SET_BIT(iSpawnGroupBS_CheckedSpawnQRCPoint, iEntityID)
			IF bSuitable
				SET_BIT(iSpawnGroupBS_CanSpawnQRCPoint, iEntityID)
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC 

FUNC INT MC_GET_ENTITY_SUB_SPAWN_GROUP_BS(INT iSpawnGroup, INT iEntityID, eSpawnGroupEntityType eEntityType = eSGET_None, INT iTeam = -1)
	SWITCH eEntityType
		CASE eSGET_Ped
			RETURN g_FMMC_STRUCT_ENTITIES.sPlacedPed[iEntityID].iSpawnSubGroupBS[iSpawnGroup]
		BREAK
		CASE eSGET_Vehicle
			RETURN g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iEntityID].iSpawnSubGroupBS[iSpawnGroup]
		BREAK
		CASE eSGET_Object
			RETURN g_FMMC_STRUCT_ENTITIES.sPlacedObject[iEntityID].iSpawnSubGroupBS[iSpawnGroup]
		BREAK
		CASE eSGET_GotoLoc
			RETURN g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iEntityID].iSpawnSubGroupBS[iSpawnGroup]
		BREAK
		CASE eSGET_Bounds
		BREAK
		CASE eSGET_Weapon
			RETURN g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iEntityID].iSpawnSubGroupBS[iSpawnGroup]
		BREAK
		CASE eSGET_Dummyblip
			RETURN g_FMMC_STRUCT_ENTITIES.sDummyBlips[iEntityID].iSpawnSubGroupBS[iSpawnGroup]
		BREAK
		CASE eSGET_Prop
			RETURN g_FMMC_STRUCT_ENTITIES.sPlacedProp[iEntityID].iSpawnSubGroupBS[iSpawnGroup]
		BREAK
		CASE eSGET_DynoProp
			RETURN g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iEntityID].iSpawnSubGroupBS[iSpawnGroup]
		BREAK
		CASE eSGET_Interactable
			RETURN g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iEntityID].iInteractable_SpawnSubGroupBS[iSpawnGroup]
		BREAK
		CASE eSGET_TeamSpawnPoint
			RETURN g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iEntityID].iSpawnSubGroupBS[iSpawnGroup]
		BREAK	
		CASE eSGET_DialogueTrigger
			RETURN g_FMMC_STRUCT.sDialogueTriggers[iEntityID].iSpawnSubGroupBS[iSpawnGroup]
		BREAK		
		CASE eSGET_Zone
			RETURN g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEntityID].iSpawnSubGroupBS[iSpawnGroup]
		BREAK
		CASE eSGET_Train
			RETURN g_FMMC_STRUCT_ENTITIES.sPlacedTrains[iEntityID].iTrain_SpawnSubGroupBS[iSpawnGroup]
		BREAK
		CASE eSGET_QRCPoints
			RETURN g_FMMC_STRUCT.sQRCSpawnPoint[iEntityID].iSpawnSubGroupBS[iSpawnGroup]
		BREAK
	ENDSWITCH
	
	RETURN 0
ENDFUNC

FUNC INT MC_GET_ENTITY_SPAWN_GROUP_BS(INT iEntityID, eSpawnGroupEntityType eEntityType = eSGET_None, INT iTeam = -1)
	SWITCH eEntityType
		CASE eSGET_Ped
			RETURN g_FMMC_STRUCT_ENTITIES.sPlacedPed[iEntityID].iSpawnGroup
		BREAK
		CASE eSGET_Vehicle
			RETURN g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iEntityID].iSpawnGroup
		BREAK
		CASE eSGET_Object
			RETURN g_FMMC_STRUCT_ENTITIES.sPlacedObject[iEntityID].iSpawnGroup
		BREAK
		CASE eSGET_GotoLoc
			RETURN g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iEntityID].iSpawnGroup
		BREAK
		CASE eSGET_Bounds
		BREAK
		CASE eSGET_Weapon
			RETURN g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iEntityID].iSpawnGroup
		BREAK
		CASE eSGET_Dummyblip
			RETURN g_FMMC_STRUCT_ENTITIES.sDummyBlips[iEntityID].iSpawnGroup
		BREAK
		CASE eSGET_Prop
			RETURN g_FMMC_STRUCT_ENTITIES.sPlacedProp[iEntityID].iSpawnGroup
		BREAK
		CASE eSGET_DynoProp
			RETURN g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iEntityID].iSpawnGroup
		BREAK
		CASE eSGET_Interactable
			RETURN g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iEntityID].iInteractable_SpawnGroup	
		BREAK
		CASE eSGET_TeamSpawnPoint
			RETURN g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iEntityID].iSpawnGroup
		BREAK
		CASE eSGET_DialogueTrigger
			RETURN g_FMMC_STRUCT.sDialogueTriggers[iEntityID].iSpawnGroup
		BREAK
		CASE eSGET_Zone
			RETURN g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEntityID].iSpawnGroup
		BREAK		
		CASE eSGET_Train
			RETURN g_FMMC_STRUCT_ENTITIES.sPlacedTrains[iEntityID].iTrain_SpawnGroup
		BREAK
		CASE eSGET_QRCPoints
			RETURN g_FMMC_STRUCT.sQRCSpawnPoint[iEntityID].iSpawnGroupBS
		BREAK
	ENDSWITCH
	
	RETURN 0
ENDFUNC

FUNC BOOL MC_HAS_ENTITY_SPAWN_GROUP_BS_BEEN_CHECKED(INT iEntityID, eSpawnGroupEntityType eEntityType = eSGET_None, INT iTeam = -1)
	SWITCH eEntityType
		CASE eSGET_Ped
			RETURN FMMC_IS_LONG_BIT_SET(iSpawnGroupBS_CheckedSpawnPed, iEntityID)
		BREAK
		CASE eSGET_Vehicle
			RETURN IS_BIT_SET(iSpawnGroupBS_CheckedSpawnVeh, iEntityID)
		BREAK
		CASE eSGET_Object
			RETURN IS_BIT_SET(iSpawnGroupBS_CheckedSpawnObj, iEntityID)
		BREAK
		CASE eSGET_GotoLoc
			RETURN IS_BIT_SET(iSpawnGroupBS_CheckedSpawnLoc, iEntityID)
		BREAK
		CASE eSGET_Bounds
		BREAK
		CASE eSGET_Weapon
			RETURN FMMC_IS_LONG_BIT_SET(iSpawnGroupBS_CheckedSpawnWep, iEntityID)
		BREAK
		CASE eSGET_Dummyblip
			RETURN IS_BIT_SET(iSpawnGroupBS_CheckedSpawnDummyBlip, iEntityID)
		BREAK
		CASE eSGET_Prop
			RETURN FMMC_IS_LONG_BIT_SET(iSpawnGroupBS_CheckedSpawnProp, iEntityID)
		BREAK
		CASE eSGET_DynoProp
			RETURN IS_BIT_SET(iSpawnGroupBS_CheckedSpawnDynoProp, iEntityID)
		BREAK
		CASE eSGET_Interactable
			RETURN FMMC_IS_LONG_BIT_SET(iSpawnGroupBS_CheckedSpawnInteractable, iEntityID)			
		BREAK
		CASE eSGET_TeamSpawnPoint
			IF iTeam > -1
				RETURN FMMC_IS_LONG_BIT_SET(iSpawnGroupBS_CheckedSpawnTeamSpawnPoint[iTeam], iEntityID)			
			ENDIF
		BREAK
		CASE eSGET_DialogueTrigger
			IF iTeam > -1
				RETURN FMMC_IS_LONG_BIT_SET(iSpawnGroupBS_CheckedSpawnDialogueTrigger[iTeam], iEntityID)			
			ENDIF
		BREAK
		CASE eSGET_Zone
			RETURN FMMC_IS_LONG_BIT_SET(iSpawnGroupBS_CheckedSpawnZone, iEntityID)			
		BREAK
		CASE eSGET_Train
			RETURN IS_BIT_SET(iSpawnGroupBS_CheckedSpawnTrain, iEntityID)
		BREAK
		CASE eSGET_QRCPoints
			RETURN IS_BIT_SET(iSpawnGroupBS_CheckedSpawnQRCPoint, iEntityID)
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL MC_GET_ENTITY_SPAWN_GROUP_BS_RESULT(INT iEntityID, eSpawnGroupEntityType eEntityType = eSGET_None, INT iTeam = -1)
	SWITCH eEntityType
		CASE eSGET_Ped
			RETURN FMMC_IS_LONG_BIT_SET(iSpawnGroupBS_CanSpawnPed, iEntityID)
		BREAK
		CASE eSGET_Vehicle
			RETURN IS_BIT_SET(iSpawnGroupBS_CanSpawnVeh, iEntityID)
		BREAK
		CASE eSGET_Object
			RETURN IS_BIT_SET(iSpawnGroupBS_CanSpawnObj, iEntityID)
		BREAK
		CASE eSGET_GotoLoc
			RETURN IS_BIT_SET(iSpawnGroupBS_CanSpawnLoc, iEntityID)
		BREAK
		CASE eSGET_Bounds
		BREAK
		CASE eSGET_Weapon
			RETURN FMMC_IS_LONG_BIT_SET(iSpawnGroupBS_CanSpawnWep, iEntityID)
		BREAK
		CASE eSGET_Dummyblip
			RETURN IS_BIT_SET(iSpawnGroupBS_CanSpawnDummyBlip, iEntityID)
		BREAK
		CASE eSGET_Prop
			RETURN FMMC_IS_LONG_BIT_SET(iSpawnGroupBS_CanSpawnProp, iEntityID)
		BREAK
		CASE eSGET_DynoProp
			RETURN IS_BIT_SET(iSpawnGroupBS_CanSpawnDynoProp, iEntityID)
		BREAK
		CASE eSGET_Interactable
			RETURN FMMC_IS_LONG_BIT_SET(iSpawnGroupBS_CanSpawnInteractable, iEntityID)			
		BREAK
		CASE eSGET_TeamSpawnPoint
			IF iTeam > -1
				RETURN FMMC_IS_LONG_BIT_SET(iSpawnGroupBS_CanSpawnTeamSpawnPoint[iTeam], iEntityID)	
			ENDIF
		BREAK
		CASE eSGET_DialogueTrigger
			IF iTeam > -1
				RETURN FMMC_IS_LONG_BIT_SET(iSpawnGroupBS_CanSpawnDialogueTrigger[iTeam], iEntityID)
			ENDIF			
		BREAK	
		CASE eSGET_Zone
			RETURN FMMC_IS_LONG_BIT_SET(iSpawnGroupBS_CanSpawnZone, iEntityID)
		BREAK
		CASE eSGET_Train
			RETURN IS_BIT_SET(iSpawnGroupBS_CanSpawnTrain, iEntityID)
		BREAK
		CASE eSGET_QRCPoints
			RETURN IS_BIT_SET(iSpawnGroupBS_CanSpawnQRCPoint, iEntityID)
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL MC_DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP(INT iEntityID, eSpawnGroupEntityType eEntityType = eSGET_None, INT iTeam = -1, INT iRule = -1, BOOL bCheckCouldSpawnLater = FALSE)
	
	// FMMC2020 - Bounds didn't ever use spawn sub groups and only looked at the main spawn group. We can either convert it over to the usual way of doing things or keep it legacy. Problem is, new way is very data HEAVY, especially for rule/team based entities.
	IF eEntityType = eSGET_Bounds
		IF iTeam != -1
		AND iRule != -1			
			IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sRuleBounds[iRule][0].iLinkedSpawnGroup != 0
				#IF IS_DEBUG_BUILD
				ASSERTLN("[SpawnGroups] MC_DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP - eSGET_Bounds - Sub spawn groups aren't set up for bounds yet.")
				ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_BOUNDS_SPAWN_GROUPS_DOES_NOT_EXIST_YET, ENTITY_RUNTIME_ERROR_TYPE_WARNING, "Trying to use Bounds Spawn Groups but they don't work.")
				#ENDIF
			ENDIF
		ENDIF
		RETURN TRUE
	ENDIF
	
	IF NOT bCheckCouldSpawnLater
	AND MC_HAS_ENTITY_SPAWN_GROUP_BS_BEEN_CHECKED(iEntityID, eEntityType, iTeam)
		IF MC_GET_ENTITY_SPAWN_GROUP_BS_RESULT(iEntityID, eEntityType, iTeam)	
		//	PRINTLN("[RCC MISSION] MC_DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP (CACHED) - Entity Type: ", GET_SPAWN_GROUP_ENTITY_TYPE_STRING(eEntityType), " Entity Index: ", iEntityID, " Flagged that it has a suitable spawn group.")
			RETURN TRUE
		ELSE
		//	PRINTLN("[RCC MISSION] MC_DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP (CACHED) - Entity Type: ", GET_SPAWN_GROUP_ENTITY_TYPE_STRING(eEntityType), " Entity Index: ", iEntityID, " Flagged that it cannot spawn based on the current spawn groups.")
			RETURN FALSE
		ENDIF
	ENDIF
	
	PRINTLN("[SpawnGroups] MC_DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP - Checking if Entity Type: ", GET_SPAWN_GROUP_ENTITY_TYPE_STRING(eEntityType), " Entity Index: ", iEntityID, " has a suitable spawn group.")
	
	BOOL bShouldSpawn = TRUE	
	INT iSpawnGroupBS = MC_GET_ENTITY_SPAWN_GROUP_BS(iEntityID, eEntityType, iTeam)
	INT iSpawnSubGroupBS	
	INT iSpawnBS, iSubSpawnBS
	INT iServerSpawnGroupBS, iServerSubSpawnGroupBS
	
	IF iSpawnGroupBS != 0
		bShouldSpawn = FALSE
		
		IF ARE_SPAWN_GROUP_SETTINGS_VALID()
						
			PRINTLN("[SpawnGroups] MC_DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP - MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeedBS: ", MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeedBS, " iSpawnGroupBS: ", iSpawnGroupBS, " iSpawnGroupMightSpawnLaterBS: ", iSpawnGroupMightSpawnLaterBS, " bCheckCouldSpawnLater: ", bCheckCouldSpawnLater)
			
			IF bCheckCouldSpawnLater
				PRINTLN("[SpawnGroups] MC_DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP - iServerSpawnGroupBS: ", iServerSpawnGroupBS, "  including spawn groups that might spawn later.")
				iServerSpawnGroupBS = MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeedBS | iSpawnGroupMightSpawnLaterBS
			ELSE
				iServerSpawnGroupBS = MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeedBS
			ENDIF
					
			iSpawnBS = iSpawnGroupBS & iServerSpawnGroupBS
			
			// AND all groups are active
			/* IF iSpawnBS = iSpawnGroupBS
			
			ENDIF */
			
			// OR (any) groups are active
			// IF iSpawnBS != 0
			
			IF iSpawnBS != 0
				
				PRINTLN("[SpawnGroups] MC_DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP - Spawn Group Returning True... MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeedBS: ", iServerSpawnGroupBS, "  iSpawnGroupBS: ", iSpawnGroupBS, " Matches: ", iSpawnBS)
				
				bShouldSpawn = TRUE
				
				INT i
				FOR i = 0 TO ciMAX_NUM_FMMC_ENTITY_SUB_SPAWN_GROUPS-1	
					
					IF IS_BIT_SET(iSpawnBS, i)				
						iSpawnSubGroupBS = MC_GET_ENTITY_SUB_SPAWN_GROUP_BS(i, iEntityID, eEntityType, iTeam)	
					
						IF iSpawnSubGroupBS != 0
						
							bShouldSpawn = FALSE
								
							IF bCheckCouldSpawnLater
								iServerSubSpawnGroupBS = MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeed_SubGroupBS[i] | iSpawnSubGroupMightSpawnLaterBS[i]
								PRINTLN("[SpawnGroups] MC_DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP - iServerSpawnGroupBS: ", iServerSpawnGroupBS, "  including sub spawn groups that might spawn later.")
							ELSE
								iServerSubSpawnGroupBS = MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeed_SubGroupBS[i]
							ENDIF
							
							iSubSpawnBS = iSpawnSubGroupBS & iServerSubSpawnGroupBS
							
							IF iSubSpawnBS != 0
								bShouldSpawn = TRUE
								PRINTLN("[SpawnGroups] MC_DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP - Spawn Sub Group Returning True... MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeed_SubGroupBS[i]: ", iServerSubSpawnGroupBS, "  iSpawnSubGroupBS: ", iSpawnSubGroupBS, " Matches: ", iSubSpawnBS)
								BREAKLOOP
							ELSE
								PRINTLN("[SpawnGroups] MC_DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP - Spawn Sub Group Returning False... MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeed_SubGroupBS[i]: ", iServerSubSpawnGroupBS, "  iSpawnSubGroupBS: ", iSpawnSubGroupBS, " Matches: ", iSubSpawnBS)
							ENDIF
							
						ELSE
							PRINTLN("[SpawnGroups] MC_DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP - Spawn Group Has no sub Spawn Groups and is valid so breaking loop... MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeed_SubGroupBS[i]: ", iServerSubSpawnGroupBS, "  iSpawnSubGroupBS: ", iSpawnSubGroupBS, " Matches: ", iSubSpawnBS)
							BREAKLOOP
						ENDIF
					
					ENDIF
					
				ENDFOR
			ELSE
				PRINTLN("[SpawnGroups] MC_DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP - Spawn Group Returning False... MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeedBS: ", iServerSpawnGroupBS, "  iSpawnGroupBS: ", iSpawnGroupBS, " Matches: ", iSpawnBS)
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT bCheckCouldSpawnLater
	AND IS_BIT_SET(MC_serverBD_4.rsgSpawnSeed.iMiscBS, ciRandomSpawnGroupBS_HasBeenSet)
		MC_SET_CACHED_DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP(bShouldSpawn, iEntityID, eEntityType, iTeam)		
		PRINTLN("[SpawnGroups] MC_DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP - Entity Type: ", GET_SPAWN_GROUP_ENTITY_TYPE_STRING(eEntityType), " Entity Index: ", iEntityID, " Caching Check.")
	ENDIF
	
	IF bShouldSpawn
		PRINTLN("[SpawnGroups] MC_DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP - Entity Type: ", GET_SPAWN_GROUP_ENTITY_TYPE_STRING(eEntityType), " Entity Index: ", iEntityID, " Passed all checks!")
	ELSE
		PRINTLN("[SpawnGroups] MC_DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP - Entity Type: ", GET_SPAWN_GROUP_ENTITY_TYPE_STRING(eEntityType), " Entity Index: ", iEntityID, " Fails to spawn!")
	ENDIF
	
	RETURN bShouldSpawn
	
ENDFUNC
// ##### ----------------------------------------------------------------------------
// ##### Section Name: Entities - Spawn Groups (sub section end)
// ##### Description: Various helper functions & wrappers to do with Spawn Groups
// ##### ----------------------------------------------------------------------------

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Spawning, Respawning and Checkpoint shared functions.
// ##### Description: The main processing function.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#IF IS_DEBUG_BUILD
FUNC BOOL WAS_MISSION_LAUNCHED_THROUGH_Z_MENU(BOOL bUseCachedValue = FALSE)
	
	IF bUseCachedValue
		IF IS_BIT_SET(iLocalboolCheck34, LBOOL34_CHECKED_WAS_LAUNCHED_THROUGH_Z_MENU)
			RETURN IS_BIT_SET(iLocalboolCheck34, LBOOL34_WAS_MISSION_LAUNCHED_THROUGH_Z_MENU)
		ENDIF	
	ENDIF

	PARTICIPANT_INDEX temppart
	PLAYER_INDEX playerId 
	INT iparticipant
	
	PRINTLN("[PLAYER_LOOP] - WAS_MISSION_LAUNCHED_THROUGH_Z_MENU")
	
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iparticipant
		temppart = INT_TO_PARTICIPANTINDEX(iparticipant)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(temppart)
			playerId = NETWORK_GET_PLAYER_INDEX(tempPart)
			IF NOT IS_PLAYER_SCTV(playerId)
				IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerId)].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_DONT_USE_END_VOTE_SCREEN)
					PRINTLN("WAS_MISSION_LAUNCHED_THROUGH_Z_MENU() - Player ", iparticipant, " has ciFMMC_MISSION_DATA_BitSet_DONT_USE_END_VOTE_SCREEN set. Returning True.")
					IF bUseCachedValue
						SET_BIT(iLocalboolCheck34, LBOOL34_WAS_MISSION_LAUNCHED_THROUGH_Z_MENU)
						SET_BIT(iLocalboolCheck34, LBOOL34_CHECKED_WAS_LAUNCHED_THROUGH_Z_MENU)
					ENDIF
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	PRINTLN("WAS_MISSION_LAUNCHED_THROUGH_Z_MENU() - No players have ciFMMC_MISSION_DATA_BitSet_DONT_USE_END_VOTE_SCREEN set. Returning False.")
	
	IF bUseCachedValue
		SET_BIT(iLocalboolCheck34, LBOOL34_CHECKED_WAS_LAUNCHED_THROUGH_Z_MENU)
	ENDIF
	
	RETURN FALSE
ENDFUNC
#ENDIF

FUNC BOOL DOES_START_POSITION_MEET_CONTINUITY_REQUIREMENTS(INT iSpawnPoint #IF IS_DEBUG_BUILD , BOOL bPrint = TRUE #ENDIF)
	
	#IF IS_DEBUG_BUILD
		IF !bXMLOverride_Continuity
			IF IS_FAKE_MULTIPLAYER_MODE_SET()				
				IF bPrint
					PRINTLN("[CONTINUITY] - DOES_START_POSITION_MEET_CONTINUITY_REQUIREMENTS - adding point [",sSetUpStartPosition.iPlayerTeam,"][",iSpawnPoint,"] due to test mode!")
				ENDIF				
				
				RETURN TRUE
			ENDIF
			
			IF WAS_MISSION_LAUNCHED_THROUGH_Z_MENU(TRUE)
			AND NOT HAS_FIRST_STRAND_MISSION_BEEN_PASSED()				
				IF bPrint
				PRINTLN("[CONTINUITY] - DOES_START_POSITION_MEET_CONTINUITY_REQUIREMENTS - adding point [",sSetUpStartPosition.iPlayerTeam,"][",iSpawnPoint,"] due to Z Menu Launch!")
				ENDIF				
				
				RETURN TRUE
			ENDIF
		ENDIF
	#ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][iSpawnPoint].iLinkedContinuityEntityType = -1
	AND g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][iSpawnPoint].iLinkedContinuityID = -1
		//No Continuity!
		RETURN TRUE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][iSpawnPoint].iLinkedContinuityEntityType = ciCONTINUITY_ENTITY_TYPE_LOCATION
	AND IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_LocationTracking)
	AND NOT IS_BIT_SET(MC_serverBD_1.sMissionContinuityVars.iLocationTrackingBitset, g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][iSpawnPoint].iLinkedContinuityID)
		#IF IS_DEBUG_BUILD
		IF bPrint		
			PRINTLN("[CONTINUITY] - DOES_START_POSITION_MEET_CONTINUITY_REQUIREMENTS - not adding point [",sSetUpStartPosition.iPlayerTeam,"][",iSpawnPoint,"] due to location continuity")
		ENDIF
		#ENDIF
		
		RETURN FALSE
	ENDIF
		
	IF g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][iSpawnPoint].iLinkedContinuityEntityType = ciCONTINUITY_ENTITY_TYPE_OBJECT
	AND IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_ObjectTracking)
	AND NOT FMMC_IS_LONG_BIT_SET(MC_serverBD_1.sMissionContinuityVars.iObjectTrackingBitset, g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sSetUpStartPosition.iPlayerTeam][iSpawnPoint].iLinkedContinuityID)
		#IF IS_DEBUG_BUILD
		IF bPrint
		PRINTLN("[CONTINUITY] - DOES_START_POSITION_MEET_CONTINUITY_REQUIREMENTS - not adding point [",sSetUpStartPosition.iPlayerTeam,"][",iSpawnPoint,"] due to object continuity")
		ENDIF
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL IS_SPAWN_POINT_A_VALID_CHECKPOINT(INT iSpawnpoint, INT iTeam = -1, BOOL bCheckContinuity = TRUE, INT iCheckpointForced = -1)
	
	IF iTeam = -1
		iTeam = MC_playerBD[iLocalPart].iteam
	ENDIF

	IF bCheckContinuity
	AND NOT DOES_START_POSITION_MEET_CONTINUITY_REQUIREMENTS(iSpawnpoint)
		PRINTLN("[MC_CustomSpawning_Spam][iSpawnpoint ", iSpawnpoint, " ] - IS_SPAWN_POINT_A_VALID_CHECKPOINT - Returning FALSE due to DOES_START_POSITION_MEET_CONTINUITY_REQUIREMENTS")
		RETURN FALSE
	ENDIF
	
	IF MC_SHOULD_BLOCK_SPAWN_BASED_ON_SPAWN_CONDITION_FLAGS(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iSpawnpoint].eSpawnConditionFlag, TRUE, FALSE, MC_PlayerBD[iPartToUse].iTeam)
		PRINTLN("[MC_CustomSpawning_Spam][iSpawnpoint ", iSpawnpoint, " ] - IS_SPAWN_POINT_A_VALID_CHECKPOINT - Returning FALSE due to MC_SHOULD_BLOCK_SPAWN_BASED_ON_SPAWN_CONDITION_FLAGS")
		RETURN FALSE
	ENDIF
	
	IF NOT MC_DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP(iSpawnPoint, eSGET_TeamSpawnPoint, iTeam)
		PRINTLN("[MC_CustomSpawning_Spam][iSpawnpoint ", iSpawnpoint, " ] - IS_SPAWN_POINT_A_VALID_CHECKPOINT - Returning FALSE due to MC_DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP")
		RETURN FALSE
	ENDIF
	
	IF iCheckpointForced = -1
		IF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(3)
			PRINTLN("[MC_CustomSpawning_Spam][iSpawnpoint ", iSpawnpoint, " ] - IS_SPAWN_POINT_A_VALID_CHECKPOINT - Returning: ", BOOL_TO_STRING(IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iSpawnpoint].iSpawnBitset, ci_SpawnBS_Checkpoint_Spawn_Point_3)), " for ci_SpawnBS_Checkpoint_Spawn_Point_3")
			RETURN IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iSpawnpoint].iSpawnBitset, ci_SpawnBS_Checkpoint_Spawn_Point_3)
		ELIF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(2)
			PRINTLN("[MC_CustomSpawning_Spam][iSpawnpoint ", iSpawnpoint, " ] - IS_SPAWN_POINT_A_VALID_CHECKPOINT - Returning: ", BOOL_TO_STRING(IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iSpawnpoint].iSpawnBitset, ci_SpawnBS_Checkpoint_Spawn_Point_2)), " for ci_SpawnBS_Checkpoint_Spawn_Point_2")
			RETURN IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iSpawnpoint].iSpawnBitset, ci_SpawnBS_Checkpoint_Spawn_Point_2)
		ELIF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(1)
			PRINTLN("[MC_CustomSpawning_Spam][iSpawnpoint ", iSpawnpoint, " ] - IS_SPAWN_POINT_A_VALID_CHECKPOINT - Returning: ", BOOL_TO_STRING(IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iSpawnpoint].iSpawnBitset, ci_SpawnBS_Checkpoint_Spawn_Point_1)), " for ci_SpawnBS_Checkpoint_Spawn_Point_1")
			RETURN IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iSpawnpoint].iSpawnBitset, ci_SpawnBS_Checkpoint_Spawn_Point_1)
		ELIF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(0)
			PRINTLN("[MC_CustomSpawning_Spam][iSpawnpoint ", iSpawnpoint, " ] - IS_SPAWN_POINT_A_VALID_CHECKPOINT - Returning: ", BOOL_TO_STRING(IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iSpawnpoint].iSpawnBitset, ci_SpawnBS_Checkpoint_Spawn_Point_0)), " for ci_SpawnBS_Checkpoint_Spawn_Point_0")
			RETURN IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iSpawnpoint].iSpawnBitset, ci_SpawnBS_Checkpoint_Spawn_Point_0)
		ENDIF
	ELSE
		PRINTLN("[MC_CustomSpawning_Spam][iSpawnpoint ", iSpawnpoint, " ] - IS_SPAWN_POINT_A_VALID_CHECKPOINT - Returning Returning: ", BOOL_TO_STRING(IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iSpawnpoint].iSpawnBitset, ci_SpawnBS_Checkpoint_Spawn_Point_0)), " for ci_SpawnBS_Checkpoint_Spawn_Point_", iCheckpointForced)
		RETURN IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iSpawnpoint].iSpawnBitset, ci_SpawnBS_Checkpoint_Spawn_Point_0 + iCheckpointForced)
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_USE_CUSTOM_START_POINT_CHECKPOINTS()
	
	IF IS_BIT_SET(iCheckpointCacheBS, ciSHOULD_USE_CUSTOM_START_POINT_CHECKPOINTS_CHECKED)
		PRINTLN("[MC_CustomSpawning][SHOULD_USE_CUSTOM_START_POINT_CHECKPOINTS] - Returning Cached Result: ", BOOL_TO_STRING(IS_BIT_SET(iCheckpointCacheBS, ciSHOULD_USE_CUSTOM_START_POINT_CHECKPOINTS_RESULT)))
		RETURN IS_BIT_SET(iCheckpointCacheBS, ciSHOULD_USE_CUSTOM_START_POINT_CHECKPOINTS_RESULT)
	ENDIF
	
	IF (HAS_FIRST_STRAND_MISSION_BEEN_PASSED_NO_RESET() OR HAS_FIRST_STRAND_MISSION_BEEN_PASSED())
	AND NOT IS_BIT_SET(g_TransitionSessionNonResetVars.iRestartBitset, FMMC_RESTART_BITSET_RETRY_STRAND_PASS_START_POINT)
		PRINTLN("[MC_CustomSpawning][SHOULD_USE_CUSTOM_START_POINT_CHECKPOINTS] - Passed First Strand, But Not Checkpoint")
		SET_BIT(iCheckpointCacheBS, ciSHOULD_USE_CUSTOM_START_POINT_CHECKPOINTS_CHECKED)
		RETURN FALSE
	ENDIF
	
	INT iTeamSpawnPointCounter
	FOR iTeamSpawnPointCounter = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[MC_playerBD[iLocalPart].iteam] -1	
		IF MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam] < FMMC_MAX_RULES
			IF IS_SPAWN_POINT_A_VALID_CHECKPOINT(iTeamSpawnPointCounter, MC_playerBD[iLocalPart].iteam)
				PRINTLN("[MC_CustomSpawning][SHOULD_USE_CUSTOM_START_POINT_CHECKPOINTS] - Using Custom CHECKPOINT Points")
				SET_BIT(iCheckpointCacheBS, ciSHOULD_USE_CUSTOM_START_POINT_CHECKPOINTS_CHECKED)
				SET_BIT(iCheckpointCacheBS, ciSHOULD_USE_CUSTOM_START_POINT_CHECKPOINTS_RESULT)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDFOR
	
	PRINTLN("[MC_CustomSpawning][SHOULD_USE_CUSTOM_START_POINT_CHECKPOINTS] RETURNING FALSE")	
	SET_BIT(iCheckpointCacheBS, ciSHOULD_USE_CUSTOM_START_POINT_CHECKPOINTS_CHECKED)
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(INT iEntityType, INT iEntityIndex, INT iAssociatedSpawn, INT iAssociatedTeam, INT iAssociatedObjective, BOOL bPropAction = FALSE, INT iScoreRequired = 0, INT iAlwaysForceSpawnByRule = -1, INT iAggroBS = ciMISSION_AGGRO_INDEX_DEFAULT, INT iAssociatedObjectiveEnd = -1)
	
	BOOL bSpawn
	
	iScoreRequired = RETURN_INT_CUSTOM_VARIABLE_VALUE(iScoreRequired, iEntityType, iEntityIndex)

	SWITCH iAssociatedSpawn
		CASE ciOBJECTIVE_SPAWN_LIMIT_OFF
			PRINTLN("HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS - ciOBJECTIVE_SPAWN_LIMIT_OFF - Objective ", iAssociatedObjective, " - Setting bSpawn to TRUE due to ciOBJECTIVE_SPAWN_LIMIT_OFF")
			bSpawn = TRUE
		BREAK
		CASE ciOBJECTIVE_SPAWN_LIMIT_ON
			IF iAssociatedObjective <= MC_serverBD_4.iCurrentHighestPriority[iAssociatedTeam]
				bSpawn = TRUE
			ENDIF
		BREAK
		CASE ciOBJECTIVE_SPAWN_LIMIT_ON_OBJECTIVE_ONLY
			IF NOT bPropAction // This option doesn't exist for prop action triggering?
			AND iAssociatedObjective = MC_serverBD_4.iCurrentHighestPriority[iAssociatedTeam]
				PRINTLN("HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS - ciOBJECTIVE_SPAWN_LIMIT_ON_OBJECTIVE_ONLY - Objective ", iAssociatedObjective, " - Setting bSpawn to TRUE due to iAssociatedObjective being = current rule")
				bSpawn = TRUE
			ENDIF
		BREAK
		CASE ciOBJECTIVE_SPAWN_LIMIT_ON_COLLECTION
			IF iAssociatedObjective = MC_serverBD_4.iCurrentHighestPriority[iAssociatedTeam]
				IF IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iAssociatedTeam], iAssociatedObjective)
					PRINTLN("HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS - ciOBJECTIVE_SPAWN_LIMIT_ON_COLLECTION - Objective ", iAssociatedObjective, " - Setting bSpawn to TRUE due to iObjectiveMidPointBitset")
					bSpawn = TRUE
				ENDIF
			ELIF iAssociatedObjective < MC_serverBD_4.iCurrentHighestPriority[iAssociatedTeam]
				PRINTLN("HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS - ciOBJECTIVE_SPAWN_LIMIT_ON_COLLECTION - Objective ", iAssociatedObjective, " - Setting bSpawn to TRUE due to iAssociatedObjective being < current rule")
				bSpawn = TRUE
			ENDIF
		BREAK
		CASE ciOBJECTIVE_SPAWN_LIMIT_OnImminentAggroFail
			IF iAssociatedObjective <= MC_serverBD_4.iCurrentHighestPriority[iAssociatedTeam]
				IF HAS_TEAM_TRIGGERED_AGGRO(iAssociatedTeam, iAggroBS)
					PRINTLN("HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS - ciOBJECTIVE_SPAWN_LIMIT_OnImminentAggroFail - Objective ", iAssociatedObjective, " - 2")
					bSpawn = TRUE
				ENDIF
			ENDIF			
		BREAK
		
		CASE ciOBJECTIVE_SPAWN_LIMIT_ON_SCORE_REACHED
			IF iAssociatedObjectiveEnd < iAssociatedObjective
				iAssociatedObjectiveEnd = iAssociatedObjective
			ENDIF

			IF MC_ServerBD.iScoreOnThisRule[iAssociatedTeam] >= iScoreRequired
			AND iAssociatedObjective    <= MC_serverBD_4.iCurrentHighestPriority[iAssociatedTeam]
			AND iAssociatedObjectiveEnd >= MC_serverBD_4.iCurrentHighestPriority[iAssociatedTeam] 
				PRINTLN("HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS - ciOBJECTIVE_SPAWN_LIMIT_ON_SCORE_REACHED - Objective ", iAssociatedObjective, ", iAssociatedObjectiveEnd = ", iAssociatedObjectiveEnd, " - Score is high enough and iAssociatedObjective <= current rule")
				bSpawn = TRUE
			ENDIF
			
			//If we force the spawn by certain rule, ignore checking the score
			IF iAssociatedTeam > -1
				IF  iAlwaysForceSpawnByRule > -1
				AND MC_serverBD_4.iCurrentHighestPriority[iAssociatedTeam] > -1
				AND iAlwaysForceSpawnByRule <= MC_serverBD_4.iCurrentHighestPriority[iAssociatedTeam]				
					PRINTLN("[RCC MISSION][HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS] ciOBJECTIVE_SPAWN_LIMIT_ON_SCORE_REACHED - Overriding Spawn Conditions for Associated Rule. Spawning now. iAlwaysForceSpawnByRule = ", iAlwaysForceSpawnByRule)
					bSpawn = TRUE
				ENDIF
			ENDIF
		BREAK	
		
		CASE ciOBJECTIVE_SPAWN_LIMIT_ON_NUMBER_OF_KILLS
			IF iAssociatedObjectiveEnd < iAssociatedObjective
				iAssociatedObjectiveEnd = iAssociatedObjective
			ENDIF

			IF MC_ServerBD.iTeamEnemyKills[iAssociatedTeam] >= iScoreRequired
			AND iAssociatedObjective    <= MC_serverBD_4.iCurrentHighestPriority[iAssociatedTeam]
			AND iAssociatedObjectiveEnd >= MC_serverBD_4.iCurrentHighestPriority[iAssociatedTeam] 
				PRINTLN("HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS - ciOBJECTIVE_SPAWN_LIMIT_ON_NUMBER_OF_KILLS - Objective ", iAssociatedObjective, ", iAssociatedObjectiveEnd = ", iAssociatedObjectiveEnd, " - Num of kills is high enough and iAssociatedObjective <= current rule")
				bSpawn = TRUE
			ENDIF
			
			//If we force the spawn by certain rule, ignore checking the kills
			IF iAssociatedTeam > -1
				IF  iAlwaysForceSpawnByRule > -1
				AND MC_serverBD_4.iCurrentHighestPriority[iAssociatedTeam] > -1
				AND iAlwaysForceSpawnByRule <= MC_serverBD_4.iCurrentHighestPriority[iAssociatedTeam]				
					PRINTLN("[RCC MISSION][HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS] ciOBJECTIVE_SPAWN_LIMIT_ON_NUMBER_OF_KILLS - Overriding Spawn Conditions for Associated Rule. Spawning now. iAlwaysForceSpawnByRule = ", iAlwaysForceSpawnByRule)
					bSpawn = TRUE
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN bSpawn
	
ENDFUNC

//Should be generic entitity not ped exclusive
FUNC BOOL IS_PED_CLOSE_TO_HEADING( PED_INDEX ped, FLOAT fHeading, FLOAT fTolerance = 30.0 )
	RETURN GET_ABSOLUTE_DELTA_HEADING( GET_ENTITY_HEADING( ped ), fHeading ) <= fTolerance
ENDFUNC	

FUNC INT IS_ENTITY_A_CHASE_PED(ENTITY_INDEX eiPassed)
	
	INT iEntity = -1

	IF DECOR_IS_REGISTERED_AS_TYPE("MC_ChasePedID", DECOR_TYPE_INT)
		IF DECOR_EXIST_ON(eiPassed,"MC_ChasePedID")
			iEntity = DECOR_GET_INT(eiPassed,"MC_ChasePedID")
			PRINTLN("[RCC MISSION] IS_ENTITY_A_CHASE_PED ENTITY: ", iEntity)
		ENDIF
	ENDIF

	RETURN iEntity
	
ENDFUNC

FUNC WEAPON_TYPE GET_CURRENT_PLAYER_WEAPON_TYPE()
	WEAPON_TYPE wtCurrent = WEAPONTYPE_INVALID
	GET_CURRENT_PED_WEAPON(LocalPlayerPed, wtCurrent)
	
	RETURN wtCurrent
ENDFUNC

FUNC BOOL IS_PHONE_EMP_AVAILABLE(BOOL bCheckHeistLeader = TRUE)
	
	#IF IS_DEBUG_BUILD
	IF IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD1)
		RETURN TRUE
	ENDIF
	#ENDIF
	
	// Only available for Heist Leader
	IF bCheckHeistLeader
	AND NOT IS_FAKE_MULTIPLAYER_MODE_SET()			//If we're in the Creator, ignore this check
	AND GET_TOTAL_PLAYING_PLAYERS() > 1				//If we're testing in freemode but alone, ignore this check
		IF NOT GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
			RETURN FALSE
		ENDIF
	ENDIF
	
	// Casino Setup Check
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_PhoneEMPAvailable_CasinoPrep)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PHONE_EMP_CURRENTLY_ACTIVE()
	RETURN IS_BIT_SET(iLocalBoolCheck31, LBOOL31_PHONE_EMP_IS_ACTIVE)
ENDFUNC

FUNC BOOL HAS_PHONE_EMP_BEEN_USED()
	RETURN IS_BIT_SET(MC_serverBD_1.sMissionContinuityVars.iGenericTrackingBitset, ciContinuityGenericTrackingBS_EMPUsed)
ENDFUNC

FUNC BOOL IS_PHONE_EMP_TIMER_RUNNING()
	RETURN HAS_NET_TIMER_STARTED(stPhoneEMP_DeactivationTimer) AND NOT HAS_NET_TIMER_EXPIRED(stPhoneEMP_DeactivationTimer, g_FMMC_STRUCT.sPhoneEMPSettings.iPhoneEMP_Duration)
ENDFUNC

//Should be counted up in the player loop
FUNC BOOL ARE_ANY_PORTABLE_PICKUPS_CURRENTLY_HELD()
	INT iObj
	FOR iObj = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfObjects - 1)
		IF MC_serverBD.iObjCarrier[iObj] != -1
			RETURN TRUE
		ENDIF
	ENDFOR
	RETURN FALSE
ENDFUNC

//Should be counted up in the player loop
FUNC INT GET_NUMBER_OF_OBJECTS_AT_TEAM_HOLDING(INT iTeam)
	INT iObj, iNumberOfObjects
	FOR iObj = 0 TO FMMC_MAX_NUM_OBJECTS - 1
		IF IS_BIT_SET(MC_serverBD.iObjAtYourHolding[iTeam], iObj)
			iNumberOfObjects++
		ENDIF
	ENDFOR
	
	PRINTLN("[BMBFB] GET_NUMBER_OF_OBJECTS_AT_TEAM_HOLDING returning ", iNumberOfObjects, " for team ", iTeam)
	RETURN iNumberOfObjects
ENDFUNC

FUNC BOOL WAS_PICKUP_AT_ANOTHER_TEAM_HOLDING(INT iObjectIndex, INT iTeamToCheckAgainst)
	INT iTeam
	FOR iTeam = 0 TO (FMMC_MAX_TEAMS - 1)
		IF iTeam != iTeamToCheckAgainst
			IF IS_BIT_SET(MC_serverBD.iObjCaptured, iObjectIndex)
			AND IS_BIT_SET(MC_serverBD.iObjAtYourHolding[iTeam], iObjectIndex)
				PRINTLN("[RCC MISSION] WAS_PICKUP_AT_ANOTHER_TEAM_HOLDING - Returning True, was at team ", iTeam,"'s holding")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDFOR
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PICKUP_AT_ANY_HOLDING(INT iObjectIndex)
	INT iTeam
	FOR iTeam = 0 TO (FMMC_MAX_TEAMS - 1)
		IF IS_BIT_SET(MC_serverBD.iObjAtYourHolding[iTeam], iObjectIndex)
			RETURN TRUE
		ENDIF
	ENDFOR
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PICKUP_AT_SPECIFIC_HOLDING(INT iObjectIndex, INT iTeam)
	RETURN IS_BIT_SET(MC_serverBD.iObjAtYourHolding[iTeam], iObjectIndex)
ENDFUNC

FUNC INT GET_TEAM_WITH_MOST_OBJECTS_AT_HOLDING()
	INT iWinningTeam, iTeam, iHighestHoldingScore, iNumTeamObjects, iNumTeamsOnSameScore
	iWinningTeam = -1
	FOR iTeam = 0 TO (FMMC_MAX_TEAMS - 1)
		IF IS_TEAM_ACTIVE(iTeam)
			iNumTeamObjects = GET_NUMBER_OF_OBJECTS_AT_TEAM_HOLDING(iTeam)
			IF iNumTeamObjects > iHighestHoldingScore
				iWinningTeam = iTeam
				iHighestHoldingScore = iNumTeamObjects
				PRINTLN("[MC] GET_TEAM_WITH_MOST_OBJECTS_AT_HOLDING - Updating iWinningTeam: ", iWinningTeam, " & iHighestHoldingScore: ", iHighestHoldingScore)
			ENDIF
		ENDIF
	ENDFOR
	
	FOR iTeam = 0 TO (FMMC_MAX_TEAMS - 1)
		IF IS_TEAM_ACTIVE(iTeam)
			IF GET_NUMBER_OF_OBJECTS_AT_TEAM_HOLDING(iTeam) = iHighestHoldingScore
				iNumTeamsOnSameScore++
				PRINTLN("[MC] GET_TEAM_WITH_MOST_OBJECTS_AT_HOLDING highest score is: ", iHighestHoldingScore,"iNumTeamsOnSameScore: ", iNumTeamsOnSameScore)
			ENDIF
		ENDIF
	ENDFOR
	
	IF iNumTeamsOnSameScore >= 2
		RETURN -1 //Returning -1 as it is a draw
	ENDIF
	
	RETURN iWinningTeam
ENDFUNC

FUNC BOOL IS_THIS_CAM_THIS_ENTITY(INT iCam, INT iEntityIndex, FMMC_ELECTRONIC_ENTITY_TYPE eType)
	IF iEntityIndex = MC_serverBD.iCameraNumber[iCam]
	AND eType = MC_serverBD.eCameraType[iCam]
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//Should be cached
FUNC INT GET_CCTV_INDEX_FOR_ENTITY(INT iIndex, FMMC_ELECTRONIC_ENTITY_TYPE eType)
	
	INT iCam
	FOR iCam = 0 TO (MAX_NUM_CCTV_CAM - 1)
		IF IS_THIS_CAM_THIS_ENTITY(iCam, iIndex, eType)
			RETURN iCam
		ENDIF
	ENDFOR
	
	RETURN -1
ENDFUNC

FUNC BOOL IS_ENTITY_OUT_OF_BOUNDS(ENTITY_INDEX entToUse, INT iTeam)
	UNUSED_PARAMETER(entToUse)
	UNUSED_PARAMETER(iTeam)
	
	ASSERTLN("IS_ENTITY_OUT_OF_BOUNDS | FEATURE HAS NOT BEEN REWRITTEN FOR 2020 CONTROLLER YET")
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_VEHICLE_STOPPED(VEHICLE_INDEX tempVeh)
	
	IF GET_ENTITY_SPEED(tempVeh) < 2.0
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL MC_IS_THIS_PICKUP_VISIBLE_TO_THIS_TEAM( INT iPickupIndex, INT iTeamIndex )
	PRINTLN("[JS] WEAPON TYPE ", GET_WEAPON_NAME_FROM_PT(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[ iPickupIndex ].pt), " = ", g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[ iPickupIndex ].bRest[ iTeamIndex ], " for team ", iTeamIndex)
	IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[ iPickupIndex ].bRest[ iTeamIndex ]
		RETURN FALSE
	ELSE
		RETURN TRUE
	ENDIF
ENDFUNC

FUNC BOOL SHOULD_USE_LOCAL_MINIMUM_VEH_SPEED_CHECK()
		
	RETURN IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_MIN_SPEED_ONLY_CHECK_LOCAL_VEH)
ENDFUNC

PROC MC_RESERVE_NETWORK_MISSION_OBJECTS(INT iNumToReserve, BOOL bIgnoreNumReserved = FALSE, BOOL bAllScripts = FALSE)
	INT iInitialObjects = GET_NUM_RESERVED_MISSION_OBJECTS(bAllScripts)
	IF bIgnoreNumReserved
		iInitialObjects = 0
	ENDIF
	RESERVE_NETWORK_MISSION_OBJECTS(iInitialObjects + iNumToReserve)
	DEBUG_PRINTCALLSTACK()
	PRINTLN("[Reserve Obj] MC_RESERVE_NETWORK_MISSION_OBJECTS - Changing number of reserved Objects by ", iNumToReserve, " Reserved Objects is now: ", GET_NUM_RESERVED_MISSION_OBJECTS(bAllScripts))
ENDPROC

FUNC INT GET_ENTITY_PRIORITY(INT iTeam, INT iEntityType,INT iEntityNum)

	INT ipriority = FMMC_PRIORITY_IGNORE
	BOOL bCheckExtraObjectives
	INT iRuleType = ciRULE_TYPE_NONE
	
	INT iCurrentRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	iCurrentRule = CLAMP_INT(iCurrentRule, 0, FMMC_MAX_RULES) // We don't want to be getting rules with FMMC_PRIORITY_IGNORE
	
	SWITCH iEntityType
		CASE ci_TARGET_LOCATION
			IF iEntityNum >= 0
			AND iEntityNum < MC_serverBD.iNumLocCreated
				
				ipriority = MC_serverBD_4.iGotoLocationDataPriority[iEntityNum][iTeam]
				
				IF ipriority >= FMMC_MAX_RULES
					
					ipriority = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iEntityNum].iPriority[iTeam]
					
					IF ipriority < iCurrentRule
						bCheckExtraObjectives = TRUE
						iRuleType = ciRULE_TYPE_GOTO
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE ci_TARGET_PED
			IF iEntityNum >= 0
			AND iEntityNum < MC_serverBD.iNumPedCreated
				
				ipriority = MC_serverBD_4.iPedPriority[iEntityNum][iTeam]
				
				IF ipriority >= FMMC_MAX_RULES
					
					ipriority = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iEntityNum].iPriority[iTeam]
					
					IF ipriority < iCurrentRule
						bCheckExtraObjectives = TRUE
						iRuleType = ciRULE_TYPE_PED
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE ci_TARGET_VEHICLE
			IF iEntityNum >= 0
			AND iEntityNum < MC_serverBD.iNumVehCreated
				
				ipriority = MC_serverBD_4.iVehPriority[iEntityNum][iTeam]
				
				IF ipriority >= FMMC_MAX_RULES
					
					ipriority = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iEntityNum].iPriority[iTeam]
					
					IF ipriority < iCurrentRule
						bCheckExtraObjectives = TRUE
						iRuleType = ciRULE_TYPE_VEHICLE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE ci_TARGET_OBJECT
			IF iEntityNum >= 0
			AND iEntityNum < MC_serverBD.iNumObjCreated
				
				ipriority = MC_serverBD_4.iObjPriority[iEntityNum][iTeam]
				
				IF ipriority >= FMMC_MAX_RULES
					
					ipriority = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iEntityNum].iPriority[iTeam]
					
					IF ipriority < iCurrentRule
						bCheckExtraObjectives = TRUE
						iRuleType = ciRULE_TYPE_OBJECT
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH
	
	//Get the last objective we had:
	IF bCheckExtraObjectives
		INT iextraObjectiveNum
		// Loop through all potential entites that have multiple rules
		FOR iextraObjectiveNum = 0 TO ( MAX_NUM_EXTRA_OBJECTIVE_ENTITIES-1 )
			// If the type matches the one passed
			IF g_FMMC_STRUCT.iExtraObjectiveEntityType[iextraObjectiveNum] = iRuleType
			AND g_FMMC_STRUCT.iExtraObjectiveEntityId[iextraObjectiveNum] = iEntityNum
				INT iextraObjectiveLoop
				
				FOR iextraObjectiveLoop = 0 TO (MAX_NUM_EXTRA_OBJECTIVE_PER_ENTITY - 1)
					IF g_FMMC_STRUCT.iExtraObjectivePriority[iextraObjectiveNum][iextraObjectiveLoop][iTeam] < iCurrentRule
					AND (GET_FMMC_RULE_FROM_CREATOR_RULE(g_FMMC_STRUCT.iExtraObjectiveRule[iextraObjectiveNum][iextraObjectiveLoop][iTeam]) != FMMC_OBJECTIVE_LOGIC_NONE)
						ipriority = g_FMMC_STRUCT.iExtraObjectivePriority[iextraObjectiveNum][iextraObjectiveLoop][iTeam]
					ENDIF
				ENDFOR
				//We found the multi-rule number for this entity, there won't be any more so break out of the loop:
				iextraObjectiveNum = MAX_NUM_EXTRA_OBJECTIVE_ENTITIES
			ENDIF
		ENDFOR
	ENDIF
	
	RETURN ipriority

ENDFUNC

FUNC BOOL IS_ENTITY_A_CURRENT_PRIORITY_FOR_TEAM(INT iTeam, INT iEntityType, INT iEntityNum)
	IF GET_ENTITY_PRIORITY(iTeam, iEntityType, iEntityNum) = GET_TEAM_CURRENT_RULE(iTeam, FALSE)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_ENTITY_PRIORITY_FOR_ANY_TEAM(INT iEntityType, INT iEntityNum, INT& iReturnTeam)

	INT iPriority = FMMC_PRIORITY_IGNORE
	INT iTempPriority
	INT iTeam
	
	iReturnTeam = -1
	
	FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
		iTempPriority = GET_ENTITY_PRIORITY(iTeam, iEntityType, iEntityNum)
		
		IF iTempPriority < FMMC_MAX_RULES
			iPriority = iTempPriority
			iReturnTeam = iTeam
			iTeam = MC_serverBD.iNumberOfTeams // Break out!
		ENDIF
	ENDFOR
	
	RETURN ipriority

ENDFUNC

FUNC INT IS_OBJ_A_MISSION_CREATOR_INTERACTABLE(OBJECT_INDEX obj)
	INT i = IS_ENTITY_A_MISSION_CREATOR_ENTITY(obj) - (g_FMMC_STRUCT_ENTITIES.iNumberOfObjects + g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps)
	IF i > -1 
		//Creator entity but could be a obj or dynoprop
		IF i >= g_FMMC_STRUCT_ENTITIES.iNumberOfInteractables
			i = -1
		ENDIF
	ENDIF
	RETURN i
ENDFUNC
FUNC INT IS_OBJ_A_MISSION_CREATOR_OBJ(OBJECT_INDEX obj)
	INT i = IS_ENTITY_A_MISSION_CREATOR_ENTITY(obj)
	IF i > -1 
		//Creator entity but could be a dynoprop or interactable
		IF i >= g_FMMC_STRUCT_ENTITIES.iNumberOfObjects
			i = -1
		ENDIF
	ENDIF
	RETURN i
ENDFUNC
FUNC INT IS_OBJ_A_MISSION_CREATOR_DYNOPROP(OBJECT_INDEX obj)
	INT i = IS_ENTITY_A_MISSION_CREATOR_ENTITY(obj) - g_FMMC_STRUCT_ENTITIES.iNumberOfObjects
	IF i > -1 
		//Creator entity but could be a obj or interactable
		IF i >= g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps
			i = -1
		ENDIF
	ENDIF
	RETURN i
ENDFUNC

FUNC INT IS_VEH_A_MISSION_CREATOR_VEH(VEHICLE_INDEX veh)
	RETURN IS_ENTITY_A_MISSION_CREATOR_ENTITY(veh)
ENDFUNC

FUNC BOOL IS_ANY_VEHICLE_DOOR_DAMAGED(VEHICLE_INDEX viVeh)
	IF IS_VEHICLE_DOOR_DAMAGED(viVeh, SC_DOOR_BONNET)
	OR IS_VEHICLE_DOOR_DAMAGED(viVeh, SC_DOOR_BOOT)
	OR IS_VEHICLE_DOOR_DAMAGED(viVeh, SC_DOOR_FRONT_LEFT)
	OR IS_VEHICLE_DOOR_DAMAGED(viVeh, SC_DOOR_FRONT_RIGHT)
	OR IS_VEHICLE_DOOR_DAMAGED(viVeh, SC_DOOR_REAR_LEFT)
	OR IS_VEHICLE_DOOR_DAMAGED(viVeh, SC_DOOR_REAR_RIGHT)
		PRINTLN("[IS_ANY_VEHICLE_DOOR_DAMAGED][ML] A vehicle door is damaged")
		RETURN TRUE
	ELSE
		PRINTLN("[IS_ANY_VEHICLE_DOOR_DAMAGED][ML] No vehicle doors are damaged")
		RETURN FALSE
	ENDIF
ENDFUNC

FUNC PED_INDEX GET_PED_INDEX_FROM_NET_ID(NETWORK_INDEX netID)
	PED_INDEX pedTemp
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(netId)
		pedTemp = NET_TO_PED(netID)
	ENDIF	
	RETURN pedTemp	
ENDFUNC

FUNC FLOAT GET_VEHICLE_ALTITUDE(VEHICLE_INDEX vi)
	VECTOR vVehCoords = GET_ENTITY_COORDS(vi, FALSE)
	RETURN vVehCoords.z
ENDFUNC

FUNC FLOAT GET_PLANE_ALTITUDE(VEHICLE_INDEX vi)
	FLOAT fGroundHeight
	VECTOR vVehCoords = GET_ENTITY_COORDS(vi)
	IF GET_GROUND_Z_FOR_3D_COORD(vVehCoords, fGroundHeight, TRUE, TRUE)
		RETURN vVehCoords.z - fGroundHeight
	ELSE
		RETURN vVehCoords.z
	ENDIF
ENDFUNC

FUNC BOOL AM_I_IN_A_BOUNCING_PLANE_OR_HELI()
	IF IS_PED_IN_ANY_HELI(LocalPlayerPed)
		VEHICLE_INDEX tempVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
		
		IF IS_ENTITY_IN_AIR(tempVeh)
			CLEAR_BIT(MC_playerBD[ iPartToUse ].iClientBitSet2, PBBOOL2_DROPPING_OFF)
			CLEAR_BIT(iLocalBoolCheck2, LBOOL2_BRING_VEHICLE_TO_HALT_CALLED)
			MC_playerBD[iPartToUse].iVehDeliveryId = -1
			RETURN TRUE
		ENDIF
	ENDIF	
	
	RETURN FALSE
ENDFUNC

PROC DISABLE_RUINER_WEAPONS_TO_SHOW_COVER_PANEL()
	VEHICLE_INDEX vehPlayer = GET_VEHICLE_PED_IS_IN(localPlayerPed)
	
	SET_VEHICLE_WEAPON_RESTRICTED_AMMO(vehPlayer, 0, 0)
	SET_VEHICLE_WEAPON_RESTRICTED_AMMO(vehPlayer, 1, 0)
	
	SET_CURRENT_PED_VEHICLE_WEAPON(LocalPlayerPed, WEAPONTYPE_UNARMED)
	SET_CURRENT_PED_WEAPON(LocalPlayerPed, WEAPONTYPE_UNARMED, TRUE)
	
	DISABLE_VEHICLE_WEAPON(TRUE,WEAPONTYPE_DLC_VEHICLE_RUINER_ROCKET, vehPlayer, LocalPlayerPed)
	DISABLE_VEHICLE_WEAPON(TRUE,WEAPONTYPE_DLC_VEHICLE_RUINER_BULLET, vehPlayer, LocalPlayerPed)			
	
	SET_PED_CAN_SWITCH_WEAPON(LocalPlayerPed, FALSE)
	
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)	
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_NEXT_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_PREV_WEAPON)
	
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
	
	PRINTLN("[LM][DISABLE_RUINER_WEAPONS_TO_SHOW_COVER_PANEL] - This is called to disable the Ruiner Weapons and Expose the Panel to cover them. Mainly required to be called before Game State Running so that this happens during the intro cam.")
ENDPROC


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Entity Rule Warps
// ##### Description: Moving the Ped to a creator placed position on a rule change.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

// General Helpers
PROC GET_ENTITY_WARP_LOCATION_AND_HEADING_WITH_ENTITY_TYPE_AND_INDEX(INT iWarpPos, INT iEntityType, INT iEntityIndex, VECTOR &vRuleWarpPos, FLOAT &fRuleWarpHead)
	SWITCH iEntityType
		CASE CREATION_TYPE_PEDS
			vRuleWarpPos = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iEntityIndex].sWarpLocationSettings.vPosition[iWarpPos]
			fRuleWarpHead = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iEntityIndex].sWarpLocationSettings.fHeading[iWarpPos]
		BREAK
		CASE CREATION_TYPE_VEHICLES
			vRuleWarpPos = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iEntityIndex].sWarpLocationSettings.vPosition[iWarpPos]
			fRuleWarpHead = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iEntityIndex].sWarpLocationSettings.fHeading[iWarpPos]
		BREAK
		CASE CREATION_TYPE_OBJECTS
			vRuleWarpPos = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iEntityIndex].sWarpLocationSettings.vPosition[iWarpPos]
			fRuleWarpHead = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iEntityIndex].sWarpLocationSettings.fHeading[iWarpPos]
		BREAK
		CASE CREATION_TYPE_GOTO_LOC
			vRuleWarpPos = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iEntityIndex].sWarpLocationSettings.vPosition[iWarpPos]
			fRuleWarpHead = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iEntityIndex].sWarpLocationSettings.fHeading[iWarpPos]
		BREAK
		CASE CREATION_TYPE_INTERACTABLE
			vRuleWarpPos = g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iEntityIndex].sWarpLocationSettings.vPosition[iWarpPos]
			fRuleWarpHead = g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iEntityIndex].sWarpLocationSettings.fHeading[iWarpPos]
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL IS_WARP_LOCATION_VISIBLE_TO_ANY_PLAYERS(WARP_LOCATION_SETTINGS &sWarpLocationSettings, VECTOR vPosStart, VECTOR vPosEnd)
	IF NOT IS_BIT_SET(sWarpLocationSettings.iWarpLocationBS, ciWARP_LOCATION_BITSET_DontWarpWhileInVisualRangeOfPlayers)
		RETURN FALSE
	ENDIF
	IF IS_COORD_VISIBLE_TO_ANY_PLAYER(vPosStart)
		PRINTLN("[LM][Warp_location] - ciWARP_LOCATION_BITSET_DontWarpWhileInVisualRangeOfPlayers is set, and start position are visible.")
		RETURN TRUE
	ENDIF
	IF IS_COORD_VISIBLE_TO_ANY_PLAYER(vPosEnd)
		PRINTLN("[LM][Warp_location] - ciWARP_LOCATION_BITSET_DontWarpWhileInVisualRangeOfPlayers is set, and end position are visible.")
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL READY_TO_PROCESS_ENTITY_WARP()
	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule =	MC_serverBD_4.iCurrentHighestPriority[iTeam] 
	
	IF iRule >= FMMC_MAX_RULES
		RETURN FALSE
	ENDIF
			
	IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam)		
		PRINTLN("[LM][Warp_location] - READY_TO_PROCESS_ENTITY_WARP - Returning False - On frame new priority.")
		RETURN FALSE
	ENDIF
	
	IF IS_THIS_RULE_A_CUTSCENE()
	OR g_bInMissionControllerCutscene
		IF NOT HAS_VALID_PLAYERS_MOVED_TO_PROCESS_SHOTS()
			PRINTLN("[LM][Warp_location] - READY_TO_PROCESS_ENTITY_WARP - Returning False - Cutscene rule. Bailing until we are processing the shots.")
			RETURN FALSE
		ELSE
			PRINTLN("[LM][Warp_location] - READY_TO_PROCESS_ENTITY_WARP - All Players are processing the shots...")
		ENDIF		
	ENDIF
	
	PRINTLN("[LM][Warp_location] - READY_TO_PROCESS_ENTITY_WARP - Returning True.")
	
	RETURN TRUE
ENDFUNC

// Rule Start Warping Starts Here. ##########
FUNC INT GET_ENTITY_WARP_LOCATION_INDEX_ON_THIS_RULE_START(INT iRule, INT iTeam, INT iEntityType, INT iEntityIndex)
	
	SWITCH iEntityType
		CASE CREATION_TYPE_PEDS
			RETURN g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRulePedWarp[iRule][iEntityIndex]
		BREAK
		CASE CREATION_TYPE_VEHICLES
			RETURN g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleVehicleWarp[iRule][iEntityIndex]
		BREAK
		CASE CREATION_TYPE_OBJECTS
			RETURN g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleObjWarp[iRule][iEntityIndex]
		BREAK
		CASE CREATION_TYPE_GOTO_LOC
			RETURN g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleLocWarp[iRule][iEntityIndex]
		BREAK
		CASE CREATION_TYPE_INTERACTABLE
			RETURN g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleInteractableWarp[iRule][iEntityIndex]
		BREAK
	ENDSWITCH
	
	RETURN -1
ENDFUNC

PROC GET_ENTITY_WARP_LOCATION_ON_THIS_RULE_START(INT iRule, INT iTeam, INT iEntityType, INT iEntityIndex, VECTOR &vRuleWarpPos, FLOAT &fRuleWarpHead)
	
	INT iWarpPos = GET_ENTITY_WARP_LOCATION_INDEX_ON_THIS_RULE_START(iRule, iTeam, iEntityType, iEntityIndex)
	
	IF iWarpPos = -1
		PRINTLN("[LM][Warp_location] - iEntityIndex: ", iEntityIndex, " - GET_ENTITY_WARP_LOCATION_ON_THIS_RULE_START - iWarpPos = -1... Something is set up wrong.")
		EXIT
	ENDIF
	
	PRINTLN("[LM][Warp_location] - iEntityIndex: ", iEntityIndex, " - GET_ENTITY_WARP_LOCATION_ON_THIS_RULE_START - iWarpPos: ", iWarpPos)	
	
	GET_ENTITY_WARP_LOCATION_AND_HEADING_WITH_ENTITY_TYPE_AND_INDEX(iWarpPos, iEntityType, iEntityIndex, vRuleWarpPos, fRuleWarpHead)
	
ENDPROC

PROC PROCESS_WARP_ON_RULE_DATA_FOR_ENTITY(INT iEntityType, INT iEntityIndex, INT &iBS_ShoudWarp[], INT &iBS_HasWarped[])
	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule =	MC_serverBD_4.iCurrentHighestPriority[iTeam] 
	IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam)
		#IF IS_DEBUG_BUILD
		IF FMMC_IS_LONG_BIT_SET(iBS_ShoudWarp, iEntityIndex)
			PRINTLN("[LM][Warp_location] - iEntityIndex: ", iEntityIndex, " NEW RULE - Clearing Should Warp.")
		ENDIF			
		IF FMMC_IS_LONG_BIT_SET(iBS_HasWarped, iEntityIndex)
			PRINTLN("[LM][Warp_location] - iEntityIndex: ", iEntityIndex, " NEW RULE - Clearing Has Warped.")
		ENDIF
		#ENDIF
		FMMC_CLEAR_LONG_BIT(iBS_ShoudWarp, iEntityIndex)
		FMMC_CLEAR_LONG_BIT(iBS_HasWarped, iEntityIndex)
	ENDIF	
	IF iRule < FMMC_MAX_RULES
		IF NOT FMMC_IS_LONG_BIT_SET(iBS_HasWarped, iEntityIndex)
		AND NOT FMMC_IS_LONG_BIT_SET(iBS_ShoudWarp, iEntityIndex)			
			IF GET_ENTITY_WARP_LOCATION_INDEX_ON_THIS_RULE_START(iRule, iTeam, iEntityType, iEntityIndex) > -1
				PRINTLN("[LM][Warp_location] - iEntityIndex: ", iEntityIndex, " NEW RULE - Setting Should Warp.")
				FMMC_SET_LONG_BIT(iBS_ShoudWarp, iEntityIndex)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_ENTITY_PERFORM_A_RULE_WARP_LOCATION_WARP(INT iEntityType, INT iEntityIndex)
	
	SWITCH iEntityType
		CASE CREATION_TYPE_PEDS
			IF FMMC_IS_LONG_BIT_SET(iPedShouldWarpThisRuleStart, iEntityIndex)
			AND NOT FMMC_IS_LONG_BIT_SET(iPedWarpedOnThisRule, iEntityIndex)
				RETURN TRUE
			ENDIF
		BREAK
		CASE CREATION_TYPE_VEHICLES
			IF FMMC_IS_LONG_BIT_SET(iVehicleShouldWarpThisRuleStart, iEntityIndex)
			AND NOT FMMC_IS_LONG_BIT_SET(iVehicleWarpedOnThisRule, iEntityIndex)			
				RETURN TRUE
			ENDIF
		BREAK
		CASE CREATION_TYPE_OBJECTS
			IF FMMC_IS_LONG_BIT_SET(iObjectShouldWarpThisRuleStart, iEntityIndex)
			AND NOT FMMC_IS_LONG_BIT_SET(iObjectWarpedOnThisRule, iEntityIndex)				
				RETURN TRUE
			ENDIF
		BREAK
		CASE CREATION_TYPE_GOTO_LOC
			IF FMMC_IS_LONG_BIT_SET(iGoToLocationShouldWarpThisRuleStart, iEntityIndex)
			AND NOT FMMC_IS_LONG_BIT_SET(iGoToLocationWarpedOnThisRule, iEntityIndex)				
				RETURN TRUE
			ENDIF
		BREAK
		CASE CREATION_TYPE_INTERACTABLE
			IF FMMC_IS_LONG_BIT_SET(iInteractableShouldWarpThisRuleStart, iEntityIndex)
			AND NOT FMMC_IS_LONG_BIT_SET(iInteractableWarpedOnThisRule, iEntityIndex)				
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

// On Damage Warping Starts Here. ##########
PROC SET_SHOULD_WARP_ON_DAMAGE_FOR_ENTITY(INT iEntityType, INT iEntityIndex)
	SWITCH iEntityType		
		CASE CREATION_TYPE_PEDS			
			FMMC_CLEAR_LONG_BIT(iPedWarpedOnDamage, iEntityIndex)
			FMMC_SET_LONG_BIT(iPedShouldWarpOnDamage, iEntityIndex)			
		BREAK
		CASE CREATION_TYPE_VEHICLES
			FMMC_CLEAR_LONG_BIT(iVehicleWarpedOnDamage, iEntityIndex)
			FMMC_SET_LONG_BIT(iVehicleShouldWarpOnDamage, iEntityIndex)			
		BREAK
		CASE CREATION_TYPE_OBJECTS
			FMMC_CLEAR_LONG_BIT(iObjectWarpedOnDamage, iEntityIndex)
			FMMC_SET_LONG_BIT(iObjectShouldWarpOnDamage, iEntityIndex)			
		BREAK
		CASE CREATION_TYPE_GOTO_LOC
			FMMC_CLEAR_LONG_BIT(iGotoLocationWarpedOnDamage, iEntityIndex)
			FMMC_SET_LONG_BIT(iGotoLocationShouldWarpOnDamage, iEntityIndex)		
		BREAK
		CASE CREATION_TYPE_INTERACTABLE
			FMMC_CLEAR_LONG_BIT(iInteractableWarpedOnDamage, iEntityIndex)
			FMMC_SET_LONG_BIT(iInteractableShouldWarpOnDamage, iEntityIndex)			
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL SHOULD_USE_ENTITY_WARP_LOCATION_INCREMENTAL_INDEX_ON_DAMAGED(INT iEntityType, INT iEntityIndex)
		
	SWITCH iEntityType
		CASE CREATION_TYPE_PEDS
			RETURN IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iEntityIndex].sWarpLocationSettings.iWarpLocationBS, ciWARP_LOCATION_BITSET_IncrementalWarpEntityWhenDamaged)
		BREAK
		CASE CREATION_TYPE_VEHICLES
			RETURN IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iEntityIndex].sWarpLocationSettings.iWarpLocationBS, ciWARP_LOCATION_BITSET_IncrementalWarpEntityWhenDamaged)
		BREAK
		CASE CREATION_TYPE_OBJECTS
			RETURN IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iEntityIndex].sWarpLocationSettings.iWarpLocationBS, ciWARP_LOCATION_BITSET_IncrementalWarpEntityWhenDamaged)
		BREAK
		CASE CREATION_TYPE_GOTO_LOC
			RETURN IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iEntityIndex].sWarpLocationSettings.iWarpLocationBS, ciWARP_LOCATION_BITSET_IncrementalWarpEntityWhenDamaged)
		BREAK
		CASE CREATION_TYPE_INTERACTABLE
			RETURN IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iEntityIndex].sWarpLocationSettings.iWarpLocationBS, ciWARP_LOCATION_BITSET_IncrementalWarpEntityWhenDamaged)
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_WARP_ON_DAMAGE_INDEX_WITH_ENTITY_TYPE_AND_INDEX(INT iEntityType, INT iEntityIndex, BOOL bReturnFreeIndex)
	INT iTempFree = -1
	INT i = 0
	FOR i = 0 TO ciWARP_FROM_DAMAGE_TRACKER_MAX-1
		IF MC_ServerBD.sWarpOnDamageTracker.iWarpFromDamageTracker_EntityType[i] = CREATION_TYPE_NONE
		AND iTempFree = -1
			iTempFree = i
		ENDIF
		IF MC_ServerBD.sWarpOnDamageTracker.iWarpFromDamageTracker_EntityType[i] = iEntityType
		AND MC_ServerBD.sWarpOnDamageTracker.iWarpFromDamageTracker_EntityIndex[i] = iEntityIndex
			RETURN i
		ENDIF
	ENDFOR
	IF bReturnFreeIndex
		RETURN iTempFree
	ELSE
		RETURN -1
	ENDIF
ENDFUNC

PROC GET_ENTITY_WARP_LOCATION_ON_DAMAGED(INT iEntityType, INT iEntityIndex, VECTOR &vRuleWarpPos, FLOAT &fRuleWarpHead)
	
	INT iWarpPos = -1
	
	// Incremental Warp On Damage - Maybe another option using this option which specifies health.
		IF SHOULD_USE_ENTITY_WARP_LOCATION_INCREMENTAL_INDEX_ON_DAMAGED(iEntityType, iEntityIndex)
			INT iWarpTrackerIndex = GET_WARP_ON_DAMAGE_INDEX_WITH_ENTITY_TYPE_AND_INDEX(iEntityType, iEntityIndex, FALSE)	
			IF iWarpTrackerIndex = -1
			OR iWarpTrackerIndex >= ciWARP_FROM_DAMAGE_TRACKER_MAX
				PRINTLN("[LM][Warp_location] - GET_ENTITY_WARP_LOCATION_ON_DAMAGED - iEntityIndex: ", iEntityIndex, " - iWarpTrackerIndex: ", iWarpTrackerIndex, " Is out of range, EXIT!")
				EXIT
			ENDIF
			
			iWarpPos = MC_ServerBD.sWarpOnDamageTracker.iWarpFromDamageTracker_WarpIndex[iWarpTrackerIndex]
		ENDIF
	// Incremental Warp On Damage - End
	
	IF iWarpPos = -1
	OR iWarpPos >= FMMC_MAX_RULE_VECTOR_WARPS
		PRINTLN("[LM][Warp_location] - GET_ENTITY_WARP_LOCATION_ON_DAMAGED - iEntityIndex: ", iEntityIndex, " - GET_ENTITY_WARP_LOCATION_ON_THIS_RULE_START - iWarpPos = -1... Something is set up wrong.")
		EXIT
	ENDIF
	
	PRINTLN("[LM][Warp_location] - GET_ENTITY_WARP_LOCATION_ON_DAMAGED - iEntityIndex: ", iEntityIndex, " - GET_ENTITY_WARP_LOCATION_ON_THIS_RULE_START - iWarpPos: ", iWarpPos)	
	
	GET_ENTITY_WARP_LOCATION_AND_HEADING_WITH_ENTITY_TYPE_AND_INDEX(iWarpPos, iEntityType, iEntityIndex, vRuleWarpPos, fRuleWarpHead)
	
ENDPROC

PROC SET_WARP_ON_DAMAGE_DATA_FOR_ENTITY(INT iEntityType, INT iEntityIndex)

	// Incremental Warp On Damage - Maybe another option using this option which specifies health.
		IF NOT SHOULD_USE_ENTITY_WARP_LOCATION_INCREMENTAL_INDEX_ON_DAMAGED(iEntityType, iEntityIndex)	
			EXIT
		ENDIF
			
		INT iWarpTrackerIndex = GET_WARP_ON_DAMAGE_INDEX_WITH_ENTITY_TYPE_AND_INDEX(iEntityType, iEntityIndex, TRUE)	
		IF iWarpTrackerIndex = -1
		OR iWarpTrackerIndex >= ciWARP_FROM_DAMAGE_TRACKER_MAX
			PRINTLN("[LM][Warp_location] - SET_WARP_ON_DAMAGE_DATA_FOR_ENTITY - iEntityIndex: ", iEntityIndex, " - iWarpTrackerIndex: ", iWarpTrackerIndex, " Is out of range, EXIT!")
			EXIT
		ENDIF
		
		// Assign incase the iTempFree was returned here and not an assigned index.
		MC_ServerBD.sWarpOnDamageTracker.iWarpFromDamageTracker_EntityType[iWarpTrackerIndex] = iEntityType
		MC_ServerBD.sWarpOnDamageTracker.iWarpFromDamageTracker_EntityIndex[iWarpTrackerIndex] = iEntityIndex
		MC_ServerBD.sWarpOnDamageTracker.iWarpFromDamageTracker_WarpIndex[iWarpTrackerIndex]++
		
		PRINTLN("[LM][Warp_location] - SET_WARP_ON_DAMAGE_DATA_FOR_ENTITY - iEntityType: ", iEntityType, " iEntityIndex: ", iEntityIndex, " - iWarpTrackerIndex: ", iWarpTrackerIndex, " Warp Index is now: ", MC_ServerBD.sWarpOnDamageTracker.iWarpFromDamageTracker_WarpIndex[iWarpTrackerIndex])
		
		IF MC_ServerBD.sWarpOnDamageTracker.iWarpFromDamageTracker_WarpIndex[iWarpTrackerIndex] >= FMMC_MAX_RULE_VECTOR_WARPS
			MC_ServerBD.sWarpOnDamageTracker.iWarpFromDamageTracker_WarpIndex[iWarpTrackerIndex] = FMMC_MAX_RULE_VECTOR_WARPS-1
		ENDIF
	// Incremental Warp On Damage - End
	
	SET_SHOULD_WARP_ON_DAMAGE_FOR_ENTITY(iEntityType, iEntityIndex)	
	BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_ShouldWarpEntityOnDamageStart, iEntityType, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, iEntityIndex)
	
ENDPROC

FUNC BOOL SHOULD_ENTITY_PERFORM_AN_ON_DAMAGE_LOCATION_WARP(INT iEntityType, INT iEntityIndex)
	
	SWITCH iEntityType
		CASE CREATION_TYPE_PEDS
			IF FMMC_IS_LONG_BIT_SET(iPedShouldWarpOnDamage, iEntityIndex)
			AND NOT FMMC_IS_LONG_BIT_SET(iPedWarpedOnDamage, iEntityIndex)
				RETURN TRUE
			ENDIF
		BREAK
		CASE CREATION_TYPE_VEHICLES
			IF FMMC_IS_LONG_BIT_SET(iVehicleShouldWarpOnDamage, iEntityIndex)
			AND NOT FMMC_IS_LONG_BIT_SET(iVehicleWarpedOnDamage, iEntityIndex)			
				RETURN TRUE
			ENDIF
		BREAK
		CASE CREATION_TYPE_OBJECTS
			IF FMMC_IS_LONG_BIT_SET(iObjectShouldWarpOnDamage, iEntityIndex)
			AND NOT FMMC_IS_LONG_BIT_SET(iObjectWarpedOnDamage, iEntityIndex)				
				RETURN TRUE
			ENDIF
		BREAK
		CASE CREATION_TYPE_GOTO_LOC
			IF FMMC_IS_LONG_BIT_SET(iGoToLocationShouldWarpOnDamage, iEntityIndex)
			AND NOT FMMC_IS_LONG_BIT_SET(iGoToLocationWarpedOnDamage, iEntityIndex)				
				RETURN TRUE
			ENDIF
		BREAK
		CASE CREATION_TYPE_INTERACTABLE
			IF FMMC_IS_LONG_BIT_SET(iInteractableShouldWarpOnDamage, iEntityIndex)
			AND NOT FMMC_IS_LONG_BIT_SET(iInteractableWarpedOnDamage, iEntityIndex)				
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Peds
// ##### Description: Logic, and helper functions for the Peds
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC INT GET_NTH_CLOSEST_PED_TO_COORD(VECTOR vCoord, INT i)

	IF i >= FMMC_MAX_PEDS
	OR i <= -1
		RETURN -1
	ENDIF
	
	FLOAT fDistance[FMMC_MAX_PEDS]
	INT iPedIndexSorted[FMMC_MAX_PEDS]
	
	INT iPed
	FOR iPed = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfPeds - 1	
		fDistance[iPed] = VDIST2(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].vPos, vCoord)
		iPedIndexSorted[iPed] = iPed
	ENDFOR
	
	QUICK_SORT_FLOAT_WITH_INDEXES(fDistance, iPedIndexSorted, 0, g_FMMC_STRUCT_ENTITIES.iNumberOfPeds-1)		
	
	RETURN iPedIndexSorted[i] 
ENDFUNC

PROC GET_NTH_ARRAY_CLOSEST_PED_TO_COORD(VECTOR vCoord, INT &i[])
	
	INT iPed		
	FOR iPed = 0 TO COUNT_OF(i)-1
		IF i[iPed] >= FMMC_MAX_PEDS
		OR i[iPed] <= -1
			PRINTLN("GET_NTH_ARRAY_CLOSEST_PED_TO_COORD FAILED - Invalid ped ID (", i[iPed], ")")
			EXIT
		ENDIF
	ENDFOR
	
	FLOAT fDistance[FMMC_MAX_PEDS]
	INT iPedIndexSorted[FMMC_MAX_PEDS]
		
	FOR iPed = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfPeds - 1	
		fDistance[iPed] = VDIST2(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].vPos, vCoord)
		iPedIndexSorted[iPed] = iPed
	ENDFOR
	
	QUICK_SORT_FLOAT_WITH_INDEXES(fDistance, iPedIndexSorted, 0, g_FMMC_STRUCT_ENTITIES.iNumberOfPeds-1)		
	
	//Assign
	INT iLoop
	FOR iLoop = 0 TO COUNT_OF(i)-1
		i[iLoop] = iPedIndexSorted[iLoop]
	ENDFOR
	
ENDPROC

FUNC STRING GET_COMPANION_TASK_STATE_NAME(PED_COMPANION_TASK_STATE &sPedCompanionState)
	STRING sName
	
	SWITCH sPedCompanionState
		CASE PED_COMPANION_TASK_STATE_INIT						sName = "PED_COMPANION_TASK_STATE_INIT"						BREAK
		CASE PED_COMPANION_TASK_STATE_WAIT						sName = "PED_COMPANION_TASK_STATE_WAIT"						BREAK
		CASE PED_COMPANION_TASK_STATE_GOTO_POINT				sName = "PED_COMPANION_TASK_STATE_GOTO_POINT"				BREAK
		CASE PED_COMPANION_TASK_STATE_FOLLOW_INTO_VEHICLE		sName = "PED_COMPANION_TASK_STATE_FOLLOW_INTO_VEHICLE"		BREAK
		CASE PED_COMPANION_TASK_STATE_FOLLOW_AS_GROUP_MEMBER	sName = "PED_COMPANION_TASK_STATE_FOLLOW_AS_GROUP_MEMBER"	BREAK
		CASE PED_COMPANION_TASK_STATE_COMBAT					sName = "PED_COMPANION_TASK_STATE_COMBAT"					BREAK
	ENDSWITCH
	
	RETURN sName
ENDFUNC

PROC SET_COMPANION_TASK_STATE(INT iPed, PED_COMPANION_TASK_STATE sPedStateNew, PED_COMPANION_TASK_STATE &sPedStateOld)	
	PRINTLN("[Peds][Ped ", iPed, "][Task][CLIENT][Companion] - SET_COMPANION_TASK_STATE - Setting To: ", GET_COMPANION_TASK_STATE_NAME(sPedStateNew), "   Old: ", GET_COMPANION_TASK_STATE_NAME(sPedStateOld))
	UNUSED_PARAMETER(iPed)	
	sPedStateOld = sPedStateNew
ENDPROC

PROC SET_COMPANION_WARP_LOCATION(INT iCompanionIndex, VECTOR vWarpLocation, FLOAT fWarpHeading)

	MC_playerBD_1[iLocalPart].sPedCompanionTaskData[iCompanionIndex].vWarpLocation = vWarpLocation
	MC_playerBD_1[iLocalPart].sPedCompanionTaskData[iCompanionIndex].fWarpHeading = fWarpHeading
	
	SET_COMPANION_TASK_STATE(MC_playerBD_1[iLocalPart].sPedCompanionTaskData[iCompanionIndex].iPed, PED_COMPANION_TASK_STATE_WARP, MC_playerBD_1[iLocalPart].sPedCompanionTaskData[iCompanionIndex].ePedCompanionTaskState)
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Entities - Peds
// ##### Description: Various helper functions & wrappers to do with mission entities
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL DISABLE_PED_AMBIENT_SPEECH(FMMC_PED_STATE &sPedState)
	IF NOT sPedState.bHasControl
	OR NOT sPedState.bExists
	OR sPedState.bInjured
		RETURN FALSE
	ENDIF
	
	STOP_PED_SPEAKING_SYNCED(sPedState.pedIndex, TRUE)
	PRINTLN("[Peds][Ped ", sPedState.iIndex, "] DISABLE_PED_AMBIENT_SPEECH | Disabling ped's ambient speech")
	RETURN TRUE
ENDFUNC

FUNC BOOL REENABLE_PED_AMBIENT_SPEECH(FMMC_PED_STATE &sPedState)
	IF NOT sPedState.bHasControl
	OR NOT sPedState.bExists
	OR sPedState.bInjured
		RETURN FALSE
	ENDIF
	
	STOP_PED_SPEAKING_SYNCED(sPedState.pedIndex, FALSE)
	PRINTLN("[Peds][Ped ", sPedState.iIndex, "] REENABLE_PED_AMBIENT_SPEECH | Re-enabling ped's ambient speech")
	RETURN TRUE
ENDFUNC

// Peds don't take fire damage while performing animations normally, so we can use this function to handle dealing damage to peds who performing animations we don't want them to break out from.
PROC PROCESS_TAKING_FIRE_DAMAGE_DURING_ANIMATION(PED_INDEX piPed, INT& sDamageTimeStampToUse)
	
	IF NOT IS_ENTITY_ALIVE(piPed)
		EXIT
	ENDIF
	
	IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(piPed)
		EXIT
	ENDIF
	
	IF NOT IS_ENTITY_ON_FIRE(piPed)
		EXIT
	ENDIF
	
	IF MC_RUN_NET_TIMER_WITH_TIMESTAMP(sDamageTimeStampToUse, ciScriptedFireDamageInterval)
		INT iNewHealth = GET_ENTITY_HEALTH(piPed)
		iNewHealth -= ciScriptedFireDamageAmount
		SET_ENTITY_HEALTH(piPed, iNewHealth)
		PRINTLN("PROCESS_TAKING_FIRE_DAMAGE_DURING_ANIMATION | Ped has taken scripted fire damage. iNewHealth: ", iNewHealth)
		MC_STOP_NET_TIMER_WITH_TIMESTAMP(sDamageTimeStampToUse)
		
	ELSE
		// Waiting to take more damage
		
	ENDIF
ENDPROC

FUNC BOOL SHOULD_DISGUISE_BLOCK_AGGRO_FOR_TEAM(INT iped,INT iTeam)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iDisguiseRule[iTeam] > -1
		IF 	NOT  IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFourteen, ciPED_BSFourteen_DisguiseBeforeAndInclRuleT0 + iTeam * 2) 
		AND NOT	IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFourteen,ciPED_BSFourteen_DisguiseAfterAndInclRuleT0 + iTeam * 2) 
				
			//Only work on this rule, not on any others:
			IF MC_serverBD_4.iCurrentHighestPriority[iTeam] = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iDisguiseRule[iTeam]
				RETURN TRUE
			ELSE
				RETURN FALSE
			ENDIF
		ELSE
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFourteen,ciPED_BSFourteen_DisguiseAfterAndInclRuleT0 + iTeam * 2)
				IF MC_serverBD_4.iCurrentHighestPriority[iTeam] >= g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iDisguiseRule[iTeam]
					RETURN TRUE
				ELSE
					RETURN FALSE
				ENDIF
			ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFourteen,ciPED_BSFourteen_DisguiseBeforeAndInclRuleT0 + iTeam * 2)
				IF MC_serverBD_4.iCurrentHighestPriority[iTeam] <= g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iDisguiseRule[iTeam]
					RETURN TRUE
				ELSE
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_AGRO_FAIL_FOR_TEAM(INT iPed, INT iTeam)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAgroFailPriority[iTeam] > -1
		IF NOT ( IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_AggroAfterAndInclRuleT0 + iTeam * 2) 
				 OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_AggroBeforeAndInclRuleT0 + iTeam * 2) )
				
			//Only aggro on this rule, not on any others:
			IF MC_serverBD_4.iCurrentHighestPriority[iTeam] = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAgroFailPriority[iTeam]
				IF (NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEight, ciPED_BSEight_Aggro_SwitchesOnMidpoint_T0 + iTeam))
				OR (NOT IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iTeam], g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAgroFailPriority[iTeam])) // If we haven't reached the midpoint yet
					#IF IS_DEBUG_BUILD
					IF bMCScriptAISpammyPrints
					PRINTLN("[Peds][Ped ", iPed, "][SpookAggro] - SHOULD_AGRO_FAIL_FOR_TEAM - iTeam: ", iTeam, " Aggroing on THIS rule. 1")
					ENDIF
					#ENDIF
					RETURN TRUE
				ENDIF
			ENDIF
			
		ELSE
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_AggroAfterAndInclRuleT0 + iTeam * 2)
				
				IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEight, ciPED_BSEight_Aggro_SwitchesOnMidpoint_T0 + iTeam)
					IF MC_serverBD_4.iCurrentHighestPriority[iTeam] >= g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAgroFailPriority[iTeam]
						#IF IS_DEBUG_BUILD
						IF bMCScriptAISpammyPrints
						PRINTLN("[Peds][Ped ", iPed, "][SpookAggro] - SHOULD_AGRO_FAIL_FOR_TEAM - iTeam: ", iTeam, " Aggroing after or including THIS rule. 1")
						ENDIF
						#ENDIF
						RETURN TRUE
					ENDIF
				ELSE
					IF MC_serverBD_4.iCurrentHighestPriority[iTeam] > g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAgroFailPriority[iTeam]
						#IF IS_DEBUG_BUILD
						IF bMCScriptAISpammyPrints
						PRINTLN("[Peds][Ped ", iPed, "][SpookAggro] - SHOULD_AGRO_FAIL_FOR_TEAM - iTeam: ", iTeam, " Aggroing after or including THIS rule. 1")
						ENDIF
						#ENDIF
						RETURN TRUE
					ELIF MC_serverBD_4.iCurrentHighestPriority[iTeam] = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAgroFailPriority[iTeam]
						IF IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iTeam], g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAgroFailPriority[iTeam])
							#IF IS_DEBUG_BUILD
							IF bMCScriptAISpammyPrints
							PRINTLN("[Peds][Ped ", iPed, "][SpookAggro] - SHOULD_AGRO_FAIL_FOR_TEAM - iTeam: ", iTeam, " Aggroing after or including THIS rule. 2")
							ENDIF
							#ENDIF
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
				
			ELSE
				
				IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEight, ciPED_BSEight_Aggro_SwitchesOnMidpoint_T0 + iTeam)
					IF MC_serverBD_4.iCurrentHighestPriority[iTeam] <= g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAgroFailPriority[iTeam]
						#IF IS_DEBUG_BUILD
						IF bMCScriptAISpammyPrints
							PRINTLN("[Peds][Ped ", iPed, "][SpookAggro] - SHOULD_AGRO_FAIL_FOR_TEAM - iTeam: ", iTeam, " Aggroing before THIS rule. 1")
						ENDIF
						#ENDIF
						RETURN TRUE
					ENDIF
				ELSE
					IF MC_serverBD_4.iCurrentHighestPriority[iTeam] < g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAgroFailPriority[iTeam]
						#IF IS_DEBUG_BUILD
						IF bMCScriptAISpammyPrints
						PRINTLN("[Peds][Ped ", iPed, "][SpookAggro] - SHOULD_AGRO_FAIL_FOR_TEAM - iTeam: ", iTeam, " Aggroing before THIS rule. 2")
						ENDIF
						#ENDIF
						RETURN TRUE
					ELIF MC_serverBD_4.iCurrentHighestPriority[iTeam] = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAgroFailPriority[iTeam]
						IF NOT IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iTeam], g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAgroFailPriority[iTeam])
							#IF IS_DEBUG_BUILD
							IF bMCScriptAISpammyPrints
							PRINTLN("[Peds][Ped ", iPed, "][SpookAggro] - SHOULD_AGRO_FAIL_FOR_TEAM - iTeam: ", iTeam, " Aggroing before THIS rule. 3")
							ENDIF
							#ENDIF
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
				
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PED_A_COP_MC(INT iped)

	IF FMMC_IS_LONG_BIT_SET(MC_serverBD.iCopPed, iped)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

PROC SET_PROPERTIES_ON_ALL_CONTROLLED_PEDS_IN_VEHICLE(VEHICLE_INDEX viVehicle, BOOL bInvincible = FALSE, BOOL bDynamic = TRUE, BOOL bBlockRagdoll = FALSE)

	IF NOT DOES_ENTITY_EXIST(viVehicle)
		EXIT
	ENDIF
	
	PRINTLN("SET_PROPERTIES_ON_ALL_CONTROLLED_PEDS_IN_VEHICLE || ", FMMC_GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(viVehicle)), " | bInvincible: ", bInvincible, " | bBlockRagdoll: ", bBlockRagdoll)
	
	INT iSeatMax = GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(viVehicle) + 1 
	INT iSeat = -1
	
	FOR iSeat = -1 TO iSeatMax-1
		PED_INDEX pedInVeh = GET_PED_IN_VEHICLE_SEAT(viVehicle, INT_TO_ENUM(VEHICLE_SEAT, iSeat))
		
		IF DOES_ENTITY_EXIST(pedInVeh)
		AND IS_ENTITY_ALIVE(pedInVeh)
			IF NETWORK_HAS_CONTROL_OF_ENTITY(pedInVeh)
				SET_ENTITY_INVINCIBLE(pedInVeh, bInvincible)
				SET_ENTITY_DYNAMIC(pedInVeh, bDynamic)
				
				SET_PED_CONFIG_FLAG(pedInVeh, PCF_DontActivateRagdollFromVehicleImpact, bBlockRagdoll)
				SET_PED_CONFIG_FLAG(pedInVeh, PCF_DontActivateRagdollFromBulletImpact, bBlockRagdoll)
				SET_PED_CONFIG_FLAG(pedInVeh, PCF_DontActivateRagdollFromExplosions, bBlockRagdoll)
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Entities - Vehicles
// ##### Description: Various helper functions & wrappers to do with mission entities
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL PERFORMING_VEHICLE_DRIVEBY_SEQUENCE()
	RETURN iVehicleDrivebySequenceState != ciVEHICLE_DRIVEBY_SEQUENCE_STATE__INACTIVE
ENDFUNC

FUNC BOOL DID_A_PLAYER_JUST_CRASH_INTO_AN_ENEMY_VEHICLE()
	RETURN iPlayerCrashedIntoEnemyVehFrameCount = GET_FRAME_COUNT()
ENDFUNC

FUNC BOOL DID_A_PLAYER_JUST_CRASH_INTO_THIS_VEHICLE(INT iVehicle)
	IF NOT DID_A_PLAYER_JUST_CRASH_INTO_AN_ENEMY_VEHICLE()
		RETURN FALSE
	ENDIF
	
	RETURN (iPlayerCrashedIntoEnemyVehIndex = iVehicle)
ENDFUNC

FUNC BOOL IS_VEHICLE_SUPPOSED_TO_BE_IN_WATER(VEHICLE_INDEX tempVeh)
	MODEL_NAMES mnVeh = GET_ENTITY_MODEL(tempVeh)
	
	// Not using VEHICLE_IS_A_WATER_BASED_VEHICLE because it checks some vehicles we would not want to have return true in here.
	
	IF IS_THIS_MODEL_A_BOAT(mnVeh)
		RETURN TRUE
	ENDIF
	
	IF IS_THIS_MODEL_A_JETSKI(mnVeh)
		RETURN TRUE
	ENDIF
	
	IF IS_THIS_MODEL_A_SUB(mnVeh)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC LOAD_COLLISION_ON_VEH_IF_POSSIBLE(VEHICLE_INDEX tempVeh, INT iVeh, BOOL bForce = FALSE)

	INT iCreatorVehID = iVeh
	IF iCollisionStreamingLimit <= ciCOLLISION_STREAMING_MAX
		IF IS_VEHICLE_DRIVEABLE(tempVeh)
			IF bForce
			OR IS_ENTITY_WAITING_FOR_WORLD_COLLISION(tempVeh)
				IF iCreatorVehID = -1
					iCreatorVehID = IS_VEH_A_MISSION_CREATOR_VEH(tempVeh)
				ENDIF
				IF iCreatorVehID > -1
					IF NOT IS_BIT_SET(iVehCollisionBitset,iCreatorVehID)
						IF NETWORK_HAS_CONTROL_OF_ENTITY(tempVeh)
							SET_ENTITY_LOAD_COLLISION_FLAG(tempVeh, TRUE)
							iCollisionStreamingLimit++
							SET_BIT(iVehCollisionBitset,iCreatorVehID)
							PRINTLN("[LOAD_COLLISION_FLAG] Loading for veh: ",iCreatorVehID, " current requests: ",iCollisionStreamingLimit)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		PRINTLN("[LOAD_COLLISION_FLAG] iCollisionStreamingLimit reached! Shelved CL 13920263 has a possible solution. veh: ",iVeh)
		SCRIPT_ASSERT("[LOAD_COLLISION_FLAG] iCollisionStreamingLimit reached!")
	ENDIF

ENDPROC

FUNC BOOL IS_VEHICLE_MODEL_A_CARGOBOB(MODEL_NAMES mnVeh)
	RETURN mnVeh = CARGOBOB
		OR mnVeh = CARGOBOB2
		OR mnVeh = CARGOBOB3
		OR mnVeh = CARGOBOB4
ENDFUNC

PROC DISABLE_SPECIAL_VEHICLE_WEAPONS(VEHICLE_INDEX vehToUse)
	IF GET_ENTITY_MODEL(vehToUse) = BLAZER5
		UPDATE_BLAZER_GTA_RACE(eLastVehiclePickup)
	ELIF GET_ENTITY_MODEL(vehToUse) = RUINER2
		UPDATE_RUINER_GTA_RACE(eLastVehiclePickup)
	ELSE
		eLastVehiclePickup = VEHICLE_PICKUP_INIT
	ENDIF
ENDPROC

//Ideally this would be elsewhere, but it's needed in the function below to get the corona size and height
FUNC BOOL IS_CURRENT_VEHICLE_RULE_OF_TYPE_CARGOBOB_FOR_TEAM(INT iTeam, INT iRule)
	
	IF(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDropOff_Vehicle[iRule] > -1)
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iVehDropOff_HookUpBS, iRule)
		AND IS_VEHICLE_MODEL_A_CARGOBOB(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDropOff_Vehicle[iRule]].mn)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC INT EXTRA_STUCK_TIME(VEHICLE_INDEX vehIndex, INT iVehFMMCIndex = -1)
	MODEL_NAMES model = GET_ENTITY_MODEL(vehIndex)
	
	IF model = RUINER2
	OR model = DUNE3
		RETURN 3000
	ENDIF
	
	IF IS_THIS_MODEL_A_BIKE(model)
		RETURN 13000
	ENDIF
	
	IF iVehFMMCIndex > -1
	AND iVehFMMCIndex < FMMC_MAX_VEHICLES
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVehFMMCIndex].iVehBitsetSeven, ciFMMC_VEHICLE7_EXTENDED_STUCK_TIMER)
			RETURN 25000
		ENDIF
	ENDIF
	
	RETURN 0
ENDFUNC

PROC CHECK_VEHICLE_STUCK_TIMERS( VEHICLE_INDEX veh, INT iVeh, BOOL bHaveControl )
		
	IF iVeh > -1 AND iVeh < FMMC_MAX_VEHICLES
	AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetEight, ciFMMC_VEHICLE8_IGNORE_STUCK_TIMERS)
		EXIT
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyThree, ciOptionsBS23_DisableAllStuckTimers)
		EXIT
	ENDIF
	IF DOES_ENTITY_EXIST(veh)
		MODEL_NAMES model = GET_ENTITY_MODEL( veh )
		IF IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_EVERYONE_RUNNING)		
			INT iStuckTimer = ciSTUCK_TIMEOUT + EXTRA_STUCK_TIME(veh, iVeh) 
			INT iRoofTimer = ROOF_TIME + EXTRA_STUCK_TIME(veh, iVeh) 
			
			IF g_FMMC_STRUCT.iStuckTimeOut > 0
				iStuckTimer = g_FMMC_STRUCT.iStuckTimeOut
				iRoofTimer = g_FMMC_STRUCT.iStuckTimeOut
				PRINTLN("[RCC MISSION] CHECK_VEHICLE_STUCK_TIMERS - Using override Stuck TimeOut ",g_FMMC_STRUCT.iStuckTimeOut )
			ENDIF		
			
			IF (IS_VEHICLE_STUCK_TIMER_UP( veh, VEH_STUCK_ON_ROOF, iRoofTimer)
			OR IS_VEHICLE_STUCK_TIMER_UP( veh, VEH_STUCK_ON_SIDE, iStuckTimer)
			OR IS_VEHICLE_STUCK_TIMER_UP( veh, VEH_STUCK_HUNG_UP, iStuckTimer)
			OR IS_VEHICLE_STUCK_TIMER_UP( veh, VEH_STUCK_JAMMED, iStuckTimer))
			AND ((NOT IS_ENTITY_ATTACHED_TO_ANY_OBJECT(veh)
			AND NOT IS_ENTITY_ATTACHED_TO_ANY_VEHICLE(veh)) 
			OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetNine, ciFMMC_VEHICLE9_StuckTimersDontCareAboutAttachments))
				IF NOT IS_THIS_MODEL_A_BOAT( model ) 
				OR ( HAS_COLLISION_LOADED_AROUND_ENTITY(veh) )
				AND NOT IS_ENTITY_IN_WATER( veh ) 
					IF bHaveControl					
						#IF IS_DEBUG_BUILD
							IF IS_VEHICLE_STUCK_TIMER_UP( veh, VEH_STUCK_ON_ROOF, iRoofTimer)
								PRINTLN("[RCC MISSION] CHECK_VEHICLE_STUCK_TIMERS - VEH_STUCK_ON_ROOF ")
							ENDIF
							IF IS_VEHICLE_STUCK_TIMER_UP( veh, VEH_STUCK_ON_SIDE, iStuckTimer)
								PRINTLN("[RCC MISSION] CHECK_VEHICLE_STUCK_TIMERS - VEH_STUCK_ON_SIDE ")
							ENDIF
							IF IS_VEHICLE_STUCK_TIMER_UP( veh, VEH_STUCK_HUNG_UP, iStuckTimer)
								PRINTLN("[RCC MISSION] CHECK_VEHICLE_STUCK_TIMERS - VEH_STUCK_HUNG_UP ")
							ENDIF
							IF IS_VEHICLE_STUCK_TIMER_UP( veh, VEH_STUCK_JAMMED, iStuckTimer)
								PRINTLN("[RCC MISSION] CHECK_VEHICLE_STUCK_TIMERS - VEH_STUCK_JAMMED ")
							ENDIF
						#ENDIF
						
						// So We're not Spamming..
						IF IS_VEHICLE_DRIVEABLE(veh)
							PRINTLN("[RCC MISSION] CHECK_VEHICLE_STUCK_TIMERS Vehicle is still driveable")						
							
							IF model = TRFLAT
							OR model = TRAILERS
								SET_ENTITY_HEALTH( veh, 0 )
								SET_VEHICLE_ENGINE_HEALTH( veh, -1000 )
								SET_VEHICLE_PETROL_TANK_HEALTH( veh, -1000 )
								PRINTLN("[RCC MISSION] CHECK_VEHICLE_STUCK_TIMERS - TRAILER IS STUCK SETTING PETROL TANK HEALTH -1000: ", iveh )
							ENDIF									
								
							IF GET_VEHICLE_ENGINE_HEALTH( veh ) > -1000
								SET_VEHICLE_ENGINE_HEALTH( veh, -1000 )
								IF GET_ENTITY_HEALTH( veh ) > 100
									SET_ENTITY_HEALTH( veh, 100 )
								ENDIF
								SET_VEHICLE_PETROL_TANK_HEALTH( veh, -1000 )
								PRINTLN("[LM][RCC MISSION] CHECK_VEHICLE_STUCK_TIMERS - VEHICLE IS STUCK SETTING HEALTH -1000: ", iveh )
								
								IF CONTENT_IS_USING_ARENA()
								AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_Disable_Arena_Mode)
									PRINTLN("[RCC MISSION] CHECK_VEHICLE_STUCK_TIMERS - ARENA MODE IS SET - VEHICLE IS STUCK SETTING BODY HEALTH -1000: ", iveh )
									SET_VEHICLE_BODY_HEALTH(veh, -1000)
								ENDIF
							ENDIF
							SET_VEHICLE_UNDRIVEABLE(veh, TRUE)
							SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(veh, FALSE)								
						ENDIF
						
						NETWORK_INDEX vehIdToDelete
						vehIdToDelete = VEH_TO_NET(veh)
						
						IF NOT CONTENT_IS_USING_ARENA()
							
							IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciDELETE_VEHICLE_ON_STUCK)
								PRINTLN("[LM][RCC MISSION] CHECK_VEHICLE_STUCK_TIMERS - FORCE DELETE THE STUCK VEHICLE due to ciDELETE_VEHICLE_ON_STUCK")	
								IF NETWORK_DOES_NETWORK_ID_EXIST(vehIdToDelete)
									CLEANUP_NET_ID(vehIdToDelete)
								ENDIF
							ELSE
								PRINTLN("[LM][RCC MISSION] CHECK_VEHICLE_STUCK_TIMERS - cleaning up the old Veh (Via; CLEANUP_NET_ID)")
								IF NETWORK_DOES_NETWORK_ID_EXIST(vehIdToDelete)
									CLEANUP_NET_ID(vehIdToDelete)
								ENDIF
							ENDIF						
													
							IF iVeh = 666
								IF NETWORK_DOES_NETWORK_ID_EXIST(netRespawnVehicle)
									PRINTLN("[RCC MISSION] CHECK_VEHICLE_STUCK_TIMERS - Cleaning up old respawn vehicle Other via (IF iVeh = 666)")
									CLEANUP_NET_ID(netRespawnVehicle)
								ENDIF
							ENDIF
						ENDIF
					ELSE
						NETWORK_REQUEST_CONTROL_OF_ENTITY(veh)
					ENDIF
				ENDIF
			ENDIF
			
			IF iVeh < FMMC_MAX_VEHICLES
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetEight, ciFMMC_VEHICLE8_RIGHT_SELF_WHEN_OUT_OF_SIGHT)
					
					INT iTimeToRightSelf = ROUND(ROOF_TIME * 0.25)
					
					IF (IS_VEHICLE_STUCK_TIMER_UP(veh, VEH_STUCK_ON_ROOF, iTimeToRightSelf)
					OR IS_VEHICLE_STUCK_TIMER_UP(veh, VEH_STUCK_ON_SIDE, iTimeToRightSelf))
					AND NOT CAN_ANY_PLAYER_SEE_POINT(GET_ENTITY_COORDS(veh), 3.25)
					
						PRINTLN("CHECK_VEHICLE_STUCK_TIMERS - Flipping vehicle ", iVeh, " now!")
						
						FLOAT fZRot = GET_ENTITY_HEADING(veh)
						VECTOR vTargetRot = <<0, 0, fZRot>>

						INT iAttemptsPerFrame = 10
						INT iAttempt
						
						FOR iAttempt = 0 TO iAttemptsPerFrame
							
							IF IS_VECTOR_ZERO(vBarragePlacementVector)
								vBarragePlacementVector = GET_ENTITY_COORDS(veh)
								vBarragePlacementVector.z += 0.5
							ELSE
								DRAW_DEBUG_LINE(vBarragePlacementVector, GET_ENTITY_COORDS(veh), 255, 0, 100, 255)
							ENDIF
						
							IF GET_CAN_VEHICLE_BE_PLACED_HERE(veh, vBarragePlacementVector, vTargetRot)
								
								PRINTLN("CHECK_VEHICLE_STUCK_TIMERS - Space IS free! ", vBarragePlacementVector, " After attempts: ", iAttempt)
								
								SET_ENTITY_COORDS(veh, vBarragePlacementVector)
								SET_ENTITY_ROTATION(veh, vTargetRot)
								SET_VEHICLE_ON_GROUND_PROPERLY(veh)
								
								vBarragePlacementVector = <<0,0,0>>
								BREAKLOOP
							ELSE
								PRINTLN("CHECK_VEHICLE_STUCK_TIMERS - Space is not free! ", vBarragePlacementVector, " Attempt: ", iAttempt)
								
								FLOAT fRandomMoveAmount = 2.5
								vBarragePlacementVector += <<GET_RANDOM_FLOAT_IN_RANGE(-fRandomMoveAmount, fRandomMoveAmount), GET_RANDOM_FLOAT_IN_RANGE(-fRandomMoveAmount, fRandomMoveAmount), 0.05>>	
							ENDIF
						ENDFOR
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC FLOAT GET_MULTIPLIER_FOR_VEHICLE_HEALTH(INT iVeh, INT iNumPlayers)

	SWITCH iNumPlayers
		CASE 2
			IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].fCustomHealthScales[ciFMMC_VEHICLE_CUSTOM_HEALTH_SCALE_TWO_PLAYERS] != 2.0
				RETURN g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].fCustomHealthScales[ciFMMC_VEHICLE_CUSTOM_HEALTH_SCALE_TWO_PLAYERS]
			ENDIF
		BREAK
		CASE 3
			IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].fCustomHealthScales[ciFMMC_VEHICLE_CUSTOM_HEALTH_SCALE_THREE_PLAYERS] != 3.0
				RETURN g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].fCustomHealthScales[ciFMMC_VEHICLE_CUSTOM_HEALTH_SCALE_THREE_PLAYERS]
			ENDIF
		BREAK
		CASE 4
			IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].fCustomHealthScales[ciFMMC_VEHICLE_CUSTOM_HEALTH_SCALE_FOUR_PLAYERS] != 4.0
				RETURN g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].fCustomHealthScales[ciFMMC_VEHICLE_CUSTOM_HEALTH_SCALE_FOUR_PLAYERS]
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN TO_FLOAT(iNumPlayers)
	
ENDFUNC

FUNC FLOAT MC_GET_VEHICLE_MAX_BODY_HEALTH(INT iVeh, INT numPlayers = 0)	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetFive, ciFMMC_VEHICLE5_VEHICLE_SCALE_COMPONENTS_HEALTH)
	AND numPlayers > 0
		RETURN g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].fBodyHealth * GET_MULTIPLIER_FOR_VEHICLE_HEALTH(iVeh, numPlayers)
	ELSE
		RETURN g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].fBodyHealth
	ENDIF
ENDFUNC

FUNC FLOAT MC_GET_FMMC_VEHICLE_MAX_ENGINE_HEALTH(INT iVeh, INT numPlayers = 0)
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetFive, ciFMMC_VEHICLE5_VEHICLE_SCALE_COMPONENTS_HEALTH)
	AND numPlayers > 0
		RETURN g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].fEngineHealth * GET_MULTIPLIER_FOR_VEHICLE_HEALTH(iVeh, numPlayers)
	ELSE
		RETURN g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].fEngineHealth
	ENDIF	
ENDFUNC

FUNC FLOAT MC_GET_FMMC_VEHICLE_MAX_PETROL_TANK_HEALTH(INT iVeh, INT numPlayers = 0)
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetFive, ciFMMC_VEHICLE5_VEHICLE_SCALE_COMPONENTS_HEALTH)
	AND numPlayers > 0
		RETURN g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].fPetrolHealth * GET_MULTIPLIER_FOR_VEHICLE_HEALTH(iVeh, numPlayers)
	ELSE
		RETURN g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].fPetrolHealth
	ENDIF	
ENDFUNC


FUNC FLOAT MC_GET_FMMC_HELI_MAX_MAIN_ROTOR_HEALTH(INT iVeh, INT numPlayers = 0)
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetFive, ciFMMC_VEHICLE5_VEHICLE_SCALE_COMPONENTS_HEALTH)
	AND numPlayers > 0
		RETURN g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].fHeliMainRotorHealth * GET_MULTIPLIER_FOR_VEHICLE_HEALTH(iVeh, numPlayers)
	ELSE
		RETURN g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].fHeliMainRotorHealth
	ENDIF	
ENDFUNC


FUNC FLOAT MC_GET_FMMC_HELI_MAX_TAIL_ROTOR_HEALTH(INT iVeh, INT numPlayers = 0)
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetFive, ciFMMC_VEHICLE5_VEHICLE_SCALE_COMPONENTS_HEALTH)
	AND numPlayers > 0
		RETURN g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].fHeliTailRotorHealth * numPlayers
	ELSE
		RETURN g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].fHeliTailRotorHealth
	ENDIF	
ENDFUNC

FUNC FLOAT MC_GET_VEHICLE_HEALTH_PRIVATE(VEHICLE_INDEX tempVeh, INT iVeh, INT numPlayers)
	RETURN GET_VEHICLE_HEALTH_PERCENTAGE(tempVeh,
		MC_GET_FMMC_VEHICLE_MAX_ENGINE_HEALTH(iVeh, numPlayers),
		MC_GET_FMMC_VEHICLE_MAX_PETROL_TANK_HEALTH(iVeh, numPlayers),
		MC_GET_VEHICLE_MAX_BODY_HEALTH(iVeh, numPlayers), 
		MC_GET_FMMC_HELI_MAX_MAIN_ROTOR_HEALTH(iVeh, numPlayers),
		MC_GET_FMMC_HELI_MAX_TAIL_ROTOR_HEALTH(iVeh, numPlayers))
ENDFUNC

FUNC FLOAT MC_GET_RESPAWN_VEHICLE_TOTAL_HEALTH_PERCENTAGE(VEHICLE_INDEX tempVeh,INT iTeam)
	IF NOT IS_ENTITY_ALIVE(tempVeh)
		RETURN 0.0
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitSet3, ciBS3_TEAM_VEHICLE_USE_ONLY_HEALTH_BODY)
		RETURN (100.0 * GET_VEHICLE_BODY_HEALTH(tempVeh) / GET_FMMC_PLAYER_VEHICLE_BODY_HEALTH_FOR_TEAM(iTeam))
	ENDIF
	FLOAT iHealth = TO_FLOAT(GET_FMMC_PLAYER_VEHICLE_HEALTH_FOR_TEAM(iTeam))
	RETURN GET_VEHICLE_HEALTH_PERCENTAGE(tempVeh,iHealth,iHealth, GET_FMMC_PLAYER_VEHICLE_BODY_HEALTH_FOR_TEAM(iTeam))
ENDFUNC

FUNC FLOAT MC_GET_VEHICLE_TOTAL_HEALTH_PERCENTAGE(VEHICLE_INDEX tempVeh, INT iVeh, INT numPlayers = 0, BOOL bTrailer = FALSE)
	IF NOT IS_ENTITY_ALIVE(tempVeh)
		RETURN 0.0
	ENDIF
	
	IF iveh = -1
		SCRIPT_ASSERT("[LM][MC_GET_VEHICLE_TOTAL_HEALTH_PERCENTAGE] - Passing in -1. BAD. Sort it out. (likely GoTo Assigned)")
		RETURN 0.0
	ENDIF
	
	IF IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetSix, ciFMMC_VEHICLE6_USE_UNIFIED_ARCADE_HEALTH_BAR )		
		FLOAT fReturn = (100.0 * GET_VEHICLE_BODY_HEALTH(tempVeh) / MC_GET_VEHICLE_MAX_BODY_HEALTH(iVeh, numPlayers))
		PRINTLN("[LM][MC_GET_VEHICLE_TOTAL_HEALTH_PERCENTAGE] - Returning ciFMMC_VEHICLE6_USE_UNIFIED_ARCADE_HEALTH_BAR special: ", fReturn)
		RETURN fReturn
	ENDIF
	
	IF IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetFive, ciFMMC_VEHICLE5_CUSTOM_HEALTH_BAR_CALCULATION )		
		FLOAT health1 = MC_GET_VEHICLE_HEALTH_PRIVATE(tempVeh, iVeh, numPlayers)
		FLOAT health2 = 100.0 * GET_VEHICLE_ENGINE_HEALTH(tempVeh) / MC_GET_FMMC_VEHICLE_MAX_ENGINE_HEALTH(iVeh, numPlayers)
		FLOAT health3 = 100.0 * GET_VEHICLE_PETROL_TANK_HEALTH(tempVeh) / MC_GET_FMMC_VEHICLE_MAX_PETROL_TANK_HEALTH(iVeh, numPlayers)
		FLOAT health4
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetSix, ciFMMC_VEHICLE6_VEHICLE_EXPLODE_AT_ZERO_BODY)
			health4 = 100.0 * GET_VEHICLE_BODY_HEALTH(tempVeh) / MC_GET_VEHICLE_MAX_BODY_HEALTH(iVeh, numPlayers)
			IF health4 < health3
				health3 = health4
			ENDIF
		ENDIF
		IF health3 < health2
			health2 = health3
		ENDIF
		IF health1 < health2
			RETURN health1
		ENDIF
		RETURN health2
	ENDIF
	IF bTrailer
		FLOAT fMaxHealth = MC_GET_VEHICLE_MAX_BODY_HEALTH(iVeh, numPlayers)
		FLOAT fBodyHealth = GET_VEHICLE_BODY_HEALTH(tempVeh)
		RETURN (fBodyHealth / fMaxHealth) * 100
	ELSE
		RETURN MC_GET_VEHICLE_HEALTH_PRIVATE(tempVeh, iVeh, numPlayers)
	ENDIF
ENDFUNC

PROC SET_CURRENT_HEALTH_PERCENTAGES_OF_VEHICLE_AND_LINKED_VEHICLES(INT iVeh)

	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])			
		fCurrentHealthPercentageCache[iVeh] = -1.0
		SET_BIT(iCurrentVehHealthPercentageCachedBS, iVeh)	
		EXIT
	ENDIF	
			
	VEHICLE_INDEX VehIndex = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
	
	IF NOT IS_VEHICLE_DRIVEABLE(VehIndex)
		fCurrentHealthPercentageCache[iVeh] = -1.0
		SET_BIT(iCurrentVehHealthPercentageCachedBS, iVeh)	
		EXIT
	ENDIF
		
	INT iAdditionalVeh = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iHealthDamageCombinedEntityIndex	
	VEHICLE_INDEX VehIndexAdditional
		
	IF iAdditionalVeh > -1
		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iAdditionalVeh])			
			fCurrentHealthPercentageCache[iVeh] = -1.0	// iVeh deliberate
			SET_BIT(iCurrentVehHealthPercentageCachedBS, iVeh)	
			EXIT
		ENDIF	
		
		VehIndexAdditional = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iAdditionalVeh])
		
		IF NOT IS_VEHICLE_DRIVEABLE(VehIndexAdditional)
			fCurrentHealthPercentageCache[iVeh] = -1.0	// iVeh deliberate
			SET_BIT(iCurrentVehHealthPercentageCachedBS, iVeh)	
			EXIT
		ENDIF
	ENDIF
	
	FLOAT fCurrentHealth
	FLOAT fCurrentLowestMaxHealth
	FLOAT fCurrentMaxHealth
	FLOAT fDamageDone
	
	IF IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetSix, ciFMMC_VEHICLE6_USE_UNIFIED_ARCADE_HEALTH_BAR )
		fCurrentHealth = GET_VEHICLE_BODY_HEALTH(VehIndex)
		fCurrentLowestMaxHealth = MC_GET_VEHICLE_MAX_BODY_HEALTH(iVeh, GET_TOTAL_STARTING_PLAYERS())
		fCurrentMaxHealth = MC_GET_VEHICLE_MAX_BODY_HEALTH(iVeh, GET_TOTAL_STARTING_PLAYERS())
		fDamageDone = fCurrentMaxHealth - fCurrentHealth
	ELSE
		fCurrentHealth = TO_FLOAT(GET_ENTITY_HEALTH(VehIndex))
		fCurrentLowestMaxHealth = TO_FLOAT(GET_ENTITY_MAX_HEALTH(VehIndex))
		fCurrentMaxHealth = TO_FLOAT(GET_ENTITY_MAX_HEALTH(VehIndex))
		fDamageDone = fCurrentMaxHealth - fCurrentHealth
	ENDIF
	
	IF iAdditionalVeh > -1	
	AND iAdditionalVeh != iVeh				
		IF IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iAdditionalVeh].iVehBitsetSix, ciFMMC_VEHICLE6_USE_UNIFIED_ARCADE_HEALTH_BAR )
			fCurrentHealth += GET_VEHICLE_BODY_HEALTH(VehIndexAdditional)
			fCurrentMaxHealth += MC_GET_VEHICLE_MAX_BODY_HEALTH(iAdditionalVeh, GET_TOTAL_STARTING_PLAYERS())
			IF MC_GET_VEHICLE_MAX_BODY_HEALTH(iAdditionalVeh, GET_TOTAL_STARTING_PLAYERS()) < fCurrentLowestMaxHealth
				fCurrentLowestMaxHealth = MC_GET_VEHICLE_MAX_BODY_HEALTH(iAdditionalVeh, GET_TOTAL_STARTING_PLAYERS()) // Find Lowest "Max" health value.
			ENDIF
			fDamageDone += (MC_GET_VEHICLE_MAX_BODY_HEALTH(iAdditionalVeh, GET_TOTAL_STARTING_PLAYERS())) - GET_VEHICLE_BODY_HEALTH(VehIndexAdditional)
		ELSE	
			fCurrentHealth += TO_FLOAT(GET_ENTITY_HEALTH(VehIndexAdditional))
			fCurrentMaxHealth += TO_FLOAT(GET_ENTITY_MAX_HEALTH(VehIndexAdditional))
			IF TO_FLOAT(GET_ENTITY_MAX_HEALTH(VehIndexAdditional)) < fCurrentLowestMaxHealth
				fCurrentLowestMaxHealth = TO_FLOAT(GET_ENTITY_MAX_HEALTH(VehIndexAdditional)) // Find Lowest "Max" health value.
			ENDIF
			fDamageDone += (TO_FLOAT(GET_ENTITY_MAX_HEALTH(VehIndexAdditional)) - TO_FLOAT(GET_ENTITY_HEALTH(VehIndexAdditional)))
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetNine, ciFMMC_VEHICLE9_TotalCombinedVehicleMaxHealth)
		
		fCurrentHealthPercentageCache[iVeh] = (fCurrentHealth / fCurrentMaxHealth) * 100
		
	ELSE
		
		fCurrentHealthPercentageCache[iVeh] = ((fCurrentLowestMaxHealth-fDamageDone) / fCurrentLowestMaxHealth ) * 100

	ENDIF
	
	PRINTLN("[Vehicle][", iVeh, "] - fCurrentLowestMaxHealth: ", fCurrentLowestMaxHealth, " fCurrentMaxHealth: ", fCurrentMaxHealth, " fDamageDone: ", fDamageDone, " fCurrentHealthPercentageCache: ", fCurrentHealthPercentageCache[iVeh])
	
	SET_BIT(iCurrentVehHealthPercentageCachedBS, iVeh)		
	
ENDPROC

FUNC FLOAT GET_CURRENT_HEALTH_PERCENTAGES_OF_VEHICLE_AND_LINKED_VEHICLES(INT iVeh)

	IF IS_BIT_SET(iCurrentVehHealthPercentageCachedBS, iVeh)
		RETURN fCurrentHealthPercentageCache[iVeh]
	ENDIF
	
	SET_CURRENT_HEALTH_PERCENTAGES_OF_VEHICLE_AND_LINKED_VEHICLES(iVeh)
		
	RETURN fCurrentHealthPercentageCache[iVeh] 
	
ENDFUNC

FUNC BOOL IS_CURRENT_OBJECTIVE_REPAIR_VEHICLE(INT iParticipant = -1)
	IF iParticipant = -1
		iParticipant = iPartToUse
	ENDIF
	RETURN IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet4, PBBOOL4_IS_CURRENT_OBJECTIVE_REPAIR_CAR)
ENDFUNC

PROC GET_CURRENT_WINCHED_VEHICLE(VEHICLE_INDEX currentVeh, VEHICLE_INDEX &viWinch, BOOL bCargobobOnly = FALSE)
	MODEL_NAMES mn = GET_ENTITY_MODEL(currentVeh)
	ENTITY_INDEX eiWinch
	IF IS_VEHICLE_MODEL_A_CARGOBOB(mn)
		eiWinch = GET_VEHICLE_ATTACHED_TO_CARGOBOB(currentVeh)
		IF DOES_ENTITY_EXIST(eiWinch)
			IF IS_ENTITY_A_VEHICLE(eiWinch)
				viWinch = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(eiWinch)
			ENDIF
		ENDIF
	ELSE
		IF !bCargobobOnly
			eiWinch = GET_ENTITY_ATTACHED_TO(currentVeh)
			IF DOES_ENTITY_EXIST(eiWinch)
				IF IS_ENTITY_A_VEHICLE(eiWinch)
					viWinch = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(eiWinch)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Entities - Interactables
// ##### Description: Various helper functions & wrappers to do with mission entities.  ------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC SET_INTERACTABLE_INVINCIBILITY(OBJECT_INDEX oiInteractable, BOOL bInvincible)
	IF bInvincible
		SET_ENTITY_INVINCIBLE(oiInteractable, TRUE)
		
		IF GET_IS_ENTITY_A_FRAG(oiInteractable)
			SET_DISABLE_FRAG_DAMAGE(oiInteractable, TRUE)
		ENDIF
	ELSE
		SET_ENTITY_INVINCIBLE(oiInteractable, FALSE)
		
		IF GET_IS_ENTITY_A_FRAG(oiInteractable)
			SET_DISABLE_FRAG_DAMAGE(oiInteractable, FALSE)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL DOES_INTERACTABLE_USE_VECTOR_WARPS(INT iInteractable)
	INT iWarp
	
	FOR iWarp = 0 TO FMMC_MAX_RULE_VECTOR_WARPS -1
		IF IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].sWarpLocationSettings.vPosition[iWarp])
			// This one is empty!
		ELSE
			RETURN TRUE
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_INTERACTABLE_COMPLETION_MODEL_SWAP_HASH(INT iInteractable)
	INT iModelSwapHash = HASH("InteractableCompletionModelSwap") + iInteractable
	RETURN iModelSwapHash
ENDFUNC

FUNC BOOL IS_INTERACTABLE_USABLE_FROM_ANY_DISTANCE(INT iInteractable)
	IF g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iInteractable_InteractionType = ciInteractableInteraction_InteractWith
	AND g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iInteractable_InteractWithAnimPreset = ciINTERACT_WITH_PRESET__CUTSCENE_LEAD_OUT
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

// Returns TRUE for Interactables that can be completed multiple times without having to become incomplete again beforehand.
FUNC BOOL IS_INTERACTABLE_MULTI_USE(INT iInteractable)
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iInteractableBS, ciInteractableBS_ForceAllowMultiUse)
		RETURN TRUE
	ENDIF
	
	SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iInteractable_InteractionType
		CASE ciInteractableInteraction_SwitchOutfit
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_INTERACTABLE_COMPLETE(INT iInteractable, BOOL bIncludeCompletedLocally = FALSE, BOOL bReturnFalseForMultiUseInteractables = TRUE)

	IF bReturnFalseForMultiUseInteractables
		IF IS_INTERACTABLE_MULTI_USE(iInteractable)
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF bIncludeCompletedLocally
		IF FMMC_IS_LONG_BIT_SET(MC_playerBD[iLocalPart].iInteractablesLocallyCompletedBS, iInteractable)
			// Returning TRUE because this participant has completed the Interaction, even if it's not considered to be truly completed by the Server just yet
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FMMC_IS_LONG_BIT_SET(MC_serverBD_4.sIntServerData.iInteractable_CompletedBS, iInteractable)
ENDFUNC

FUNC BOOL IS_INTERACTION_TYPE_COMPLETE(INT iInteractionType)
	RETURN IS_BIT_SET(iInteractable_CompletedInteractionTypeBS, iInteractionType)
ENDFUNC

FUNC FLOAT GET_INTERACTABLE_INTERACTION_COORDS_MAX_DISTANCE(FMMC_INTERACTABLE_STATE& sInteractableState)
	RETURN g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].fInteractable_InteractionDistance
ENDFUNC

FUNC INT GET_INTERACTABLE_CURRENT_USER(INT iInteractable)
	RETURN MC_serverBD_4.sIntServerData.iInteractable_InteractingParticipant[iInteractable]
ENDFUNC

FUNC INT GET_ONGOING_INTERACTION_INDEX(INT iInteractable)
	
	INT iInteraction
	FOR iInteraction = 0 TO MC_playerBD[iPartToUse].iOngoingInteractionCount - 1
		IF sOngoingInteractionVars[iInteraction].iInteractable = iInteractable
			// This is the interaction in our ongoing interactions list which is with the given Interactable
			RETURN iInteraction
		ENDIF
	ENDFOR
	
	RETURN -1
ENDFUNC

FUNC BOOL IS_ONGOING_INTERACTION_FOREGROUND(INT iInteraction)
	IF iInteraction >= MC_playerBD[iPartToUse].iOngoingInteractionCount
		RETURN FALSE
	ENDIF
	
	RETURN NOT IS_BIT_SET(sOngoingInteractionVars[iInteraction].iOngoingInteractionBS, ciONGOING_INTERACTION_BS__IS_BACKGROUND_INTERACTION)
ENDFUNC

FUNC INT GET_FOREGROUND_INTERACTABLE_INDEX()
	INT iLoop
	FOR iLoop = 0 TO MC_playerBD[iLocalPart].iOngoingInteractionCount - 1
		IF IS_ONGOING_INTERACTION_FOREGROUND(iLoop)
			// This ongoing interaction is actively taking the player ped's attention - return this one's interactable index!
			RETURN sOngoingInteractionVars[iLoop].iInteractable
		ENDIF
	ENDFOR
	
	RETURN -1
ENDFUNC

FUNC BOOL CURRENTLY_PERFORMING_FOREGROUND_INTERACTION()
	RETURN GET_FOREGROUND_INTERACTABLE_INDEX() > -1
ENDFUNC

FUNC BOOL IS_THIS_PARTICIPANT_INTERACTING(INT iPart, INT iInteractable = ciANY_INTERACTABLE)
	
	IF iInteractable = ciANY_INTERACTABLE
		RETURN MC_playerBD[iPart].iOngoingInteractionCount > 0
	ENDIF
	
	INT iLoop
	FOR iLoop = 0 TO MC_playerBD[iPart].iOngoingInteractionCount - 1
		IF MC_playerBD[iPart].iCurrentInteractables[iLoop] = iInteractable
			RETURN TRUE
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_INTERACTING(INT iInteractable = ciANY_INTERACTABLE, BOOL bForegroundInteractionOnly = TRUE)
	IF bForegroundInteractionOnly
		IF iInteractable = ciANY_INTERACTABLE
			RETURN GET_FOREGROUND_INTERACTABLE_INDEX() > -1
		ELSE
			RETURN GET_FOREGROUND_INTERACTABLE_INDEX() = iInteractable
		ENDIF
		
	ELSE
		RETURN IS_THIS_PARTICIPANT_INTERACTING(iLocalPart, iInteractable)
		
	ENDIF
ENDFUNC

FUNC OBJECT_INDEX GET_FOREGROUND_INTERACTION_INTERACTABLE_OBJECT()

	INT iForegroundInteractable = GET_FOREGROUND_INTERACTABLE_INDEX()
	
	IF iForegroundInteractable > -1
		NETWORK_INDEX niObj = GET_INTERACTABLE_NET_ID(iForegroundInteractable)
		
		IF NETWORK_DOES_NETWORK_ID_EXIST(niObj)
			RETURN NET_TO_OBJ(niObj)
		ENDIF
	ENDIF
	
	RETURN NULL
ENDFUNC

PROC BAIL_FOREGROUND_INTERACTION_ASAP()

	IF GET_FOREGROUND_INTERACTABLE_INDEX() = -1
		EXIT
	ENDIF
	
	PRINTLN("[Interactables][Interactable ", GET_FOREGROUND_INTERACTABLE_INDEX(), "] BAIL_FOREGROUND_INTERACTION_ASAP")
	SET_BIT(iSharedInteractableBS, ciSHARED_INTERACTABLE_BS__BAIL_FOREGROUND_INTERACTION_ASAP)
	
ENDPROC

FUNC BOOL HAS_INTERACTABLE_BAIL_TIMER_EXPIRED()
	IF iInteractableBailTime > -1
	OR HAS_NET_TIMER_STARTED(stInteractableBailTimer)
		IF HAS_NET_TIMER_EXPIRED(stInteractableBailTimer, iInteractableBailTime)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC RUN_INTERACTABLE_BAIL_TIMER(INT iBailTime)
	IF NOT HAS_NET_TIMER_STARTED(stInteractableBailTimer)
	OR iBailTime != iInteractableBailTime
		REINIT_NET_TIMER(stInteractableBailTimer)
		iInteractableBailTime = iBailTime
		PRINTLN("[Interactables][InteractableBailTimer] RUN_INTERACTABLE_BAIL_TIMER | Starting bail timer with a time of ", iBailTime)
	ENDIF
ENDPROC

PROC RESET_INTERACTABLE_BAIL_TIMER()
	IF iInteractableBailTime > -1
	OR HAS_NET_TIMER_STARTED(stInteractableBailTimer)
		iInteractableBailTime = -1
		RESET_NET_TIMER(stInteractableBailTimer)
		PRINTLN("[Interactables][InteractableBailTimer] RESET_INTERACTABLE_BAIL_TIMER | Resetting bail timer")
	ENDIF
ENDPROC

PROC SET_SYNCLOCK_INTERACTION_STATE(SYNCLOCK_INTERACTION_STATE eNewState)
	MC_playerBD[iLocalPart].eSyncLockInteractionState = eNewState
	PRINTLN("[Interactables][SyncLock] SET_SYNCLOCK_INTERACTION_STATE | Sync lock state is now ", MC_playerBD[iLocalPart].eSyncLockInteractionState)
ENDPROC

FUNC INT GET_LINKED_INTERACTABLE_INDEX(INT iInteractable)
	RETURN iInteractable_CachedLinkedInteractable[iInteractable]
ENDFUNC

FUNC BOOL DOES_INTERACTABLE_HAVE_LINKED_INTERACTABLE(INT iInteractable)
	RETURN iInteractable_CachedLinkedInteractable[iInteractable] != -1
ENDFUNC

FUNC BOOL IS_INTERACTABLE_OCCUPIED(INT iInteractable)
	IF FMMC_IS_LONG_BIT_SET(iInteractable_OccupiedBS, iInteractable)
		RETURN TRUE
	ENDIF
	
	RETURN GET_INTERACTABLE_CURRENT_USER(iInteractable) > -1
ENDFUNC

FUNC BOOL IS_LINKED_INTERACTABLE_OCCUPIED(INT iInteractable)
	IF DOES_INTERACTABLE_HAVE_LINKED_INTERACTABLE(iInteractable)
		RETURN IS_INTERACTABLE_OCCUPIED(GET_LINKED_INTERACTABLE_INDEX(iInteractable))
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC CACHE_LINKED_INTERACTABLE(INT iInteractable, INT iLinkedInteractable)
	IF GET_LINKED_INTERACTABLE_INDEX(iInteractable) != iLinkedInteractable
		iInteractable_CachedLinkedInteractable[iInteractable] = iLinkedInteractable
		PRINTLN("[Interactables][iInteractable ", iInteractable, "] CACHE_LINKED_INTERACTABLE | Caching that Interactable ", iInteractable, " is linked to ", iLinkedInteractable)
	ENDIF
ENDPROC

PROC STOP_INTERACTABLE_LOOPING_SOUND(INTERACTION_VARS& sInteractionVars)
	IF sInteractionVars.iInteractable_LoopingSoundID != -1
		PRINTLN("[Interactables][Interactable ", sInteractionVars.iInteractable, "] STOP_INTERACTABLE_LOOPING_SOUND | Stopping looping sound!")
		STOP_SOUND(sInteractionVars.iInteractable_LoopingSoundID)
		sInteractionVars.iInteractable_LoopingSoundID = -1
	ENDIF
ENDPROC

FUNC INT GET_SYNCLOCK_REQUIRED_ENGAGEMENTS(INT iInteractable)
	UNUSED_PARAMETER(iInteractable)
	RETURN 2 // To be replaced with an option later potentially
ENDFUNC

FUNC BOOL IS_SYNC_LOCK_PANEL_RELEVANT_TO_LOCAL_PLAYER(INT iInteractable)
	IF NOT IS_LOCAL_PLAYER_INTERACTING()
		// Can't be relevant, we're not interacting with anything
		RETURN FALSE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[GET_FOREGROUND_INTERACTABLE_INDEX()].iInteractable_InteractionType != ciInteractableInteraction_SyncLock
		// Can't be relevant, we're not interacting with a sync-lock panel
		RETURN FALSE
	ENDIF
	
	IF IS_LOCAL_PLAYER_INTERACTING(iInteractable)
		// It's definitely relevant - we're using it right now
		RETURN TRUE
	ENDIF
	
	IF FMMC_IS_LONG_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[GET_FOREGROUND_INTERACTABLE_INDEX()].sConsequences.iInteractable_LinkedInteractablesBS, iInteractable)
		// It's relevant because it's marked as being connected to the panel I'm currently using
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_INTERACTABLE_MULTI_SOLUTION_LOCK_CURRENT_BEST_SOLUTION(INT iInteractable)

	#IF IS_DEBUG_BUILD
	IF bInteractableDebug
		IF IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD1)
			DRAW_DEBUG_TEXT("Solution_A", g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].vInteractable_Position + <<0, 0, 2.0>>, 255, 255, 255, 255)
			RETURN ciInteractable_MultiSolutionLockSolution_A
		ELIF IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD2)
			DRAW_DEBUG_TEXT("Solution_B", g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].vInteractable_Position + <<0, 0, 2.0>>, 255, 255, 255, 255)
			RETURN ciInteractable_MultiSolutionLockSolution_B
		ELIF IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD3)
			DRAW_DEBUG_TEXT("Solution_LastResort", g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].vInteractable_Position + <<0, 0, 2.0>>, 255, 255, 255, 255)
			RETURN ciInteractable_MultiSolutionLockSolution_LastResort
		ENDIF
	ENDIF
	#ENDIF
	
	IF IS_PREREQUISITE_COMPLETED(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iInteractable_SolutionAPreReq)
		RETURN ciInteractable_MultiSolutionLockSolution_A
	ENDIF
	
	IF IS_PREREQUISITE_COMPLETED(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iInteractable_SolutionBPreReq)
		RETURN ciInteractable_MultiSolutionLockSolution_B
	ENDIF
	
	RETURN ciInteractable_MultiSolutionLockSolution_LastResort
ENDFUNC

FUNC BOOL SHOULD_INTERACTABLE_BE_COMPLETELY_FROZEN(INT iInteractable)
	
	IF IS_INTERACTABLE_OCCUPIED(iInteractable)
		RETURN FALSE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].mnInteractable_Model = INT_TO_ENUM(MODEL_NAMES, HASH("hei_prop_hei_keypad_01"))
	OR g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].mnInteractable_Model = INT_TO_ENUM(MODEL_NAMES, HASH("h4_Prop_h4_FingerKeyPad_01a"))
	OR g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].mnInteractable_Model = INT_TO_ENUM(MODEL_NAMES, HASH("h4_prop_h4_casino_button_01a"))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_INTERACTABLE_GIVE_CASH_WHEN_COMPLETED(INT iInteractable)

	IF g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iInteractable_InteractionType = ciInteractableInteraction_LootTray
		// Cash is given throughout this Interaction, not at the end
		RETURN FALSE
	ENDIF
	
	RETURN g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].sConsequences.iInteractable_CashConsequence > 0
ENDFUNC

FUNC BOOL HAVE_ALL_PLAYERS_COMPLETED_INTERACTABLE(INT iInteractable, INT iSkipThisParticipant = -1)
	INT iPart = -1
	WHILE DO_PARTICIPANT_LOOP(iPart)
		IF iPart = iSkipThisParticipant
			PRINTLN("[Interactables][Interactable ", iInteractable, "] HAVE_ALL_PLAYERS_COMPLETED_INTERACTABLE | Skipping participant ", iPart)
			RELOOP
		ENDIF
		
		IF NOT FMMC_IS_LONG_BIT_SET(MC_playerBD[iPart].iInteractablesLocallyCompletedBS, iInteractable)
			PRINTLN("[Interactables][Interactable ", iInteractable, "] HAVE_ALL_PLAYERS_COMPLETED_INTERACTABLE | Interactable hasn't been completed by participant ", iPart)
			RETURN FALSE
		ENDIF
	ENDWHILE
	
	PRINTLN("[Interactables][Interactable ", iInteractable, "] HAVE_ALL_PLAYERS_COMPLETED_INTERACTABLE | Interactable has been completed by all participants")
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_INTERACTABLE_READY_TO_BE_COMPLETED(INT iInteractable, INT iCompleterParticipant)
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iInteractableBS, ciInteractableBS_OnlyCompleteAfterAllPlayersUsed)
		IF NOT HAVE_ALL_PLAYERS_COMPLETED_INTERACTABLE(iInteractable, iCompleterParticipant)
			PRINTLN("[Interactables][Interactable ", iInteractable, "] IS_INTERACTABLE_READY_TO_BE_COMPLETED | Interactable hasn't yet been completed by all players!")
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Bag Capacity ----------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Functions relating to Bag Capacity.  			 -------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL BAG_CAPACITY__IS_ENABLED()
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySeven, ciOptionsBS27_EnableBagCapacityForGrab)
		RETURN TRUE
	ENDIF
	
	IF g_FMMC_STRUCT.iMaximumBagCapacity != 0
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL BAG_CAPACITY__SHOULD_WAIT_TO_SHOW_HUD()

	IF IS_BIT_SET(sMissionLocalContinuityVars.iGeneralLocalTrackingBitset, ciContinuityGenericTrackingBS_ShowBagCapacityHUD)
		RETURN FALSE
	ENDIF
	
	//Check if iCurrentPriority is valid and if we are using ciBS_RULE15_FAKE_FULL_CAPACITY
	BOOL bIsFakeFullCapacityActive = FALSE	
	IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam] < FMMC_MAX_RULES
		bIsFakeFullCapacityActive = IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iRuleBitsetFifteen[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]], ciBS_RULE15_FAKE_FULL_CAPACITY)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyNine, ciOptionsBS29_BagCapacityHUDWaitForLootRule) 
	AND NOT bIsFakeFullCapacityActive
		IF GET_MC_CLIENT_MISSION_STAGE(iPartToUse) != CLIENT_MISSION_STAGE_LOOT_THRESHOLD
			RETURN TRUE
		ENDIF
	ENDIF
	
	SET_BIT(sMissionLocalContinuityVars.iGeneralLocalTrackingBitset, ciContinuityGenericTrackingBS_ShowBagCapacityHUD)
	
	RETURN FALSE
	
ENDFUNC

FUNC INT BAG_CAPACITY__GET_MAX_CAPACITY()
	
	//Done like this to preserve content that has come out with the old setup.
	//The override should be returning first to override the tunable.
	IF IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_DLC_2_2022)
		RETURN g_FMMC_STRUCT.iMaximumBagCapacity
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySeven, ciOptionsBS27_EnableBagCapacityForGrab)
		RETURN 	g_sMPTunables.iHeistBagCapacity_MaxCapacity
	ENDIF
	
	IF g_FMMC_STRUCT.iMaximumBagCapacity != 0
		RETURN g_FMMC_STRUCT.iMaximumBagCapacity
	ENDIF
	
	RETURN 0
ENDFUNC

FUNC BOOL BAG_CAPACITY__IS_BAG_FULL()
	RETURN MC_playerBD[iLocalPart].sLootBag.fCurrentBagCapacity >= BAG_CAPACITY__GET_MAX_CAPACITY()
ENDFUNC

PROC BAG_CAPACITY__SET_MOST_GRABBED_LOOT_TYPE(INT iLootType)
	IF iBagCapacityMostGrabbedLootType = -1
		iBagCapacityMostGrabbedLootType = iLootType
		PRINTLN("BAG_CAPACITY__SET_MOST_GRABBED_LOOT_TYPE - Updating most grabbed loot type to: ", iBagCapacityMostGrabbedLootType)
	ELSE
		IF iBagCapacityMostGrabbedLootType != iLootType
		AND MC_playerBD[iLocalPart].sLootBag.fCurrentCapacity[iLootType] > MC_playerBD[iLocalPart].sLootBag.fCurrentCapacity[iBagCapacityMostGrabbedLootType]
			iBagCapacityMostGrabbedLootType = iLootType
			PRINTLN("BAG_CAPACITY__SET_MOST_GRABBED_LOOT_TYPE - Changing most grabbed loot type to: ", iBagCapacityMostGrabbedLootType)
		ENDIF
	ENDIF
ENDPROC

FUNC INT BAG_CAPACITY__GET_MOST_GRABBED_LOOT_TYPE()
	INT iLootType
	INT iHighestLootType
	
	IF iBagCapacityMostGrabbedLootType != -1
		RETURN iBagCapacityMostGrabbedLootType
	ENDIF
	
	FOR iLootType = 0 TO ciLOOT_TYPE_MAX-1
		IF MC_playerBD[iLocalPart].sLootBag.fCurrentCapacity[iLootType] > MC_playerBD[iLocalPart].sLootBag.fCurrentCapacity[iHighestLootType]
			iHighestLootType = iLootType
		ENDIF
	ENDFOR
	
	BAG_CAPACITY__SET_MOST_GRABBED_LOOT_TYPE(iHighestLootType)
	
	RETURN iBagCapacityMostGrabbedLootType
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section: Goto Rule Processing -----------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: 
// ##### 
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC INT GET_RULE_GOTO_TEAMS(INT iTeam, INT iRule, BOOL& bCheckOnRule)
	
	INT iTeamLoop
	INT iTeamsToCheck = 0
	bCheckOnRule = TRUE
	
	//Get the setting from the rule of the vehicle
	IF IS_BIT_SET(g_FMMC_STRUCT.iVehRuleWaitForTeamsBS[iRule][iTeam], ciFMMC_VehRuleTeamBS_ALL_TEAM_GOTO)
		SET_BIT(iTeamsToCheck,iTeam)
	ELIF IS_BIT_SET(g_FMMC_STRUCT.iVehRuleWaitForTeamsBS[iRule][iTeam],ciFMMC_VehRuleTeamBS_ALL_FRIENDLY_GOTO)
		FOR iTeamLoop = 0 TO FMMC_MAX_TEAMS-1
			IF DOES_TEAM_LIKE_TEAM(iTeam,iTeamLoop)
				SET_BIT(iTeamsToCheck,iTeamLoop)
			ENDIF
		ENDFOR
	ELIF IS_BIT_SET(g_FMMC_STRUCT.iVehRuleWaitForTeamsBS[iRule][iTeam],ciFMMC_VehRuleTeamBS_EVERYONE_GOTO)
		FOR iTeamLoop = 0 TO FMMC_MAX_TEAMS-1
			SET_BIT(iTeamsToCheck,iTeamLoop)
		ENDFOR
	ELIF IS_BIT_SET(g_FMMC_STRUCT.iVehRuleWaitForTeamsBS[iRule][iTeam],ciFMMC_VehRuleTeamBS_CUSTOMTEAMS_GOTO)
		FOR iTeamLoop = 0 TO FMMC_MAX_TEAMS-1
			IF IS_BIT_SET(g_FMMC_STRUCT.iVehRuleWaitForTeamsBS[iRule][iTeam],ciFMMC_VehRuleTeamBS_CUSTOMTEAMS_GOTO_T0 + iTeamLoop)
				SET_BIT(iTeamsToCheck,iTeamLoop)
			ENDIF
		ENDFOR
	ELSE
		//Only need one player in vehicle
		iTeamsToCheck = 0
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iVehRuleWaitForTeamsBS[iRule][iTeam],ciFMMC_VehRuleTeamBS_IGNORE_CHECK_FOR_ON_VEH_RULE)
		bCheckOnRule = FALSE
	ENDIF
	
	RETURN iTeamsToCheck
	
ENDFUNC

//Get a bitset of teams who need to get in their vehicle for this vehicle's GoTo to complete for this team
FUNC INT GET_VEHICLE_GOTO_TEAMS(INT iTeam, INT iVeh, BOOL& bCheckOnRule)
	
	INT iTeamsToCheck = 0
	INT iTeamLoop
	
	bCheckOnRule = TRUE
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitSet,ciFMMC_VEHICLE_OVERRIDEWRULESETTING_GOTO)
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitSet,ciFMMC_VEHICLE_ALL_TEAM_GOTO)
			SET_BIT(iTeamsToCheck,iTeam)
		ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitSet,ciFMMC_VEHICLE_ALL_FRIENDLY_GOTO)
			FOR iTeamLoop = 0 TO FMMC_MAX_TEAMS-1
				IF DOES_TEAM_LIKE_TEAM(iTeam,iTeamLoop)
					SET_BIT(iTeamsToCheck,iTeamLoop)
				ENDIF
			ENDFOR
		ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitSet,ciFMMC_VEHICLE_EVERYONE_GOTO)
			FOR iTeamLoop = 0 TO FMMC_MAX_TEAMS-1
				SET_BIT(iTeamsToCheck,iTeamLoop)
			ENDFOR
		ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitSet,ciFMMC_VEHICLE_CUSTOMTEAMS_GOTO)
			FOR iTeamLoop = 0 TO FMMC_MAX_TEAMS-1
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitSet,ciFMMC_VEHICLE_CUSTOMTEAMS_GOTO_T0 + iTeamLoop)
					SET_BIT(iTeamsToCheck,iTeamLoop)
				ENDIF
			ENDFOR
		ELSE
			//Only need one player in vehicle
			iTeamsToCheck = 0
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetTwo, ciFMMC_VEHICLE2_IGNORE_CHECK_FOR_ON_VEH_RULE)
			bCheckOnRule = FALSE
		ENDIF
		
	ELSE
		//Get the setting from the rule of the vehicle:
		IF MC_serverBD_4.iVehPriority[iVeh][iTeam] >= FMMC_MAX_RULES
			RETURN 0
		ENDIF
		iTeamsToCheck = GET_RULE_GOTO_TEAMS(iTeam, MC_serverBD_4.iVehPriority[iVeh][iTeam], bCheckOnRule)
	ENDIF
	
	RETURN iTeamsToCheck
	
ENDFUNC

FUNC BOOL IS_VEHICLE_WAITING_FOR_PLAYERS(INT iVeh, INT iTeam)
	BOOL bCheckOnRule = TRUE
	INT iTeamsToCheck = GET_VEHICLE_GOTO_TEAMS(iTeam, iVeh, bCheckOnRule)
	
	IF iTeamsToCheck = 0 //Only a single player goto
		RETURN FALSE
	ENDIF
	
	INT iTeamLoop, iFreeSeats, iPlayersInVeh, iTotalPlayers
	
	//is everyone in a vehicle
	//or are all the seats taken
	
	FOR iTeamLoop = 0 TO (FMMC_MAX_TEAMS - 1)
		IF IS_BIT_SET(iTeamsToCheck,iTeamLoop)
			IF (NOT bCheckOnRule)
			OR (MC_serverBD.iNumVehHighestPriority[iTeamLoop] > 0
			AND ((MC_serverBD_4.iVehMissionLogic[iTeamLoop] = FMMC_OBJECTIVE_LOGIC_GO_TO) OR (MC_serverBD_4.iVehMissionLogic[iTeamLoop] = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER)))
			
				iPlayersInVeh += MC_serverBD.iTeamVehNumPlayers[iTeamLoop]
				iTotalPlayers += MC_serverBD.iNumberOfPlayingPlayers[iTeamLoop]
				iFreeSeats += MC_serverBD.iNumVehSeatsHighestPriority[iTeamLoop] - MC_serverBD.iTeamVehNumPlayers[iTeamLoop]
			ENDIF			
		ENDIF
	ENDFOR
	
	IF iFreeSeats = 0
		RETURN FALSE
	ELIF (iPlayersInVeh >= iTotalPlayers)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Entities - Trains
// ##### Description: Various helper functions & wrappers to do with Trains
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL MC_IS_ENTITY_ON_AN_ASSIGNED_TRAIN(ENTITY_INDEX eiEntity, TRAIN_ATTACHMENT_STRUCT& sTrainAttachmentData)
		
	IF sTrainAttachmentData.iTrainIndex = -1
		RETURN FALSE
	ENDIF
	
	ENTITY_INDEX eiAttachmentTrain = GET_FMMC_ENTITY(ciENTITY_TYPE_TRAIN, sTrainAttachmentData.iTrainIndex)
	
	IF NOT DOES_ENTITY_EXIST(eiAttachmentTrain)
		RETURN FALSE
	ENDIF
	
	VEHICLE_INDEX viAttachmentTrain = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(eiAttachmentTrain)
	
	IF NOT IS_ENTITY_ALIVE(viAttachmentTrain)
		RETURN FALSE
	ENDIF
	
	VEHICLE_INDEX viAttachmentCarriage = GET_TRAIN_CARRIAGE(viAttachmentTrain, sTrainAttachmentData.iCarriageIndex)
	
	IF NOT IS_ENTITY_ALIVE(viAttachmentCarriage)
		RETURN FALSE
	ENDIF
	
	IF NOT MC_IS_ENTITY_TOUCHING_ENTITY(eiEntity, viAttachmentCarriage, 30.0)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC VEHICLE_INDEX MC_GET_ATTACHMENT_TRAIN(TRAIN_ATTACHMENT_STRUCT& sTrainAttachmentData)
	ENTITY_INDEX eiTrain = GET_FMMC_ENTITY(ciENTITY_TYPE_TRAIN, sTrainAttachmentData.iTrainIndex, -1) // Pass in -1 for the carriage - We want a reference to the train itself
	
	IF DOES_ENTITY_EXIST(eiTrain)
		RETURN GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(eiTrain)
	ENDIF
	
	RETURN NULL
ENDFUNC

PROC MC_GET_TRAIN_ATTACHMENT_POINT_IN_WORLD_COORDS(TRAIN_ATTACHMENT_STRUCT& sTrainAttachmentData, VECTOR &vPosition, VECTOR &vRotation)
	GET_TRAIN_ATTACHMENT_POINT_IN_WORLD_COORDS(MC_GET_ATTACHMENT_TRAIN(sTrainAttachmentData), sTrainAttachmentData, vPosition, vRotation)
ENDPROC

FUNC BOOL IS_TRAIN_ATTACHMENT_READY(TRAIN_ATTACHMENT_STRUCT& sTrainAttachmentData)
	ENTITY_INDEX eiAttachmentTrain = GET_FMMC_ENTITY(ciENTITY_TYPE_TRAIN, sTrainAttachmentData.iTrainIndex)
	
	IF NOT DOES_ENTITY_EXIST(eiAttachmentTrain)
		PRINTLN("[Trains][Train ", sTrainAttachmentData.iTrainIndex, "] IS_TRAIN_ATTACHMENT_READY | Train ", sTrainAttachmentData.iTrainIndex, " doesn't exist!")
		RETURN FALSE
	ENDIF
	
	IF NOT IS_ENTITY_ALIVE(eiAttachmentTrain)
		PRINTLN("[Trains][Train ", sTrainAttachmentData.iTrainIndex, "] IS_TRAIN_ATTACHMENT_READY | Train ", sTrainAttachmentData.iTrainIndex, " is dead!")
		RETURN FALSE
	ENDIF
	
	ENTITY_INDEX eiAttachmentCarriage = GET_FMMC_ENTITY(ciENTITY_TYPE_TRAIN, sTrainAttachmentData.iTrainIndex, sTrainAttachmentData.iCarriageIndex)
	
	IF NOT DOES_ENTITY_EXIST(eiAttachmentCarriage)
		PRINTLN("[Trains][Train ", sTrainAttachmentData.iTrainIndex, "] IS_TRAIN_ATTACHMENT_READY | Carriage ", sTrainAttachmentData.iCarriageIndex, " doesn't exist!")
		RETURN FALSE
	ENDIF
	
	IF NOT IS_ENTITY_ALIVE(eiAttachmentCarriage)
		PRINTLN("[Trains][Train ", sTrainAttachmentData.iTrainIndex, "] IS_TRAIN_ATTACHMENT_READY | Carriage ", sTrainAttachmentData.iCarriageIndex, " is dead!")
		RETURN FALSE
	ENDIF
	
	PRINTLN("[Trains_SPAM][Train_SPAM ", sTrainAttachmentData.iTrainIndex, "] IS_TRAIN_ATTACHMENT_READY | Train is ready!")
	RETURN TRUE
	
ENDFUNC

FUNC BOOL PROCESS_TRAIN_ATTACHMENT(ENTITY_INDEX eiEntity, TRAIN_ATTACHMENT_STRUCT& sTrainAttachmentData, BOOL bShouldBeAttached = TRUE)
	
	IF sTrainAttachmentData.iTrainIndex = -1
		RETURN TRUE
	ENDIF
	
	ENTITY_INDEX eiAttachmentTrain = GET_FMMC_ENTITY(ciENTITY_TYPE_TRAIN, sTrainAttachmentData.iTrainIndex)
	
	IF NOT bShouldBeAttached
		IF IS_ENTITY_ATTACHED_TO_ENTITY(eiEntity, eiAttachmentTrain)
			PRINTLN("[Trains][Train ", sTrainAttachmentData.iTrainIndex, "] PROCESS_TRAIN_ATTACHMENT | Detaching entity ", FMMC_GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(eiEntity)), " from its train due to bShouldBeAttached being FALSE")
			DETACH_ENTITY(eiEntity, TRUE)
		ENDIF
		
		RETURN FALSE
	ENDIF
	
	IF IS_ENTITY_ATTACHED(eiEntity)
		// Already attached!
		RETURN TRUE
	ENDIF
	
	IF DOES_ENTITY_EXIST(eiAttachmentTrain)
		VEHICLE_INDEX viAttachmentTrain = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(eiAttachmentTrain)
		ATTACH_ENTITY_TO_SPECIFIED_TRAIN(eiEntity, viAttachmentTrain, sTrainAttachmentData)
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_TRAIN_PERFECTLY_STILL(INT iTrain)
	IF HAS_NET_TIMER_STARTED(tdTrainFullyStoppedTimer[iTrain])
		IF HAS_NET_TIMER_EXPIRED(tdTrainFullyStoppedTimer[iTrain], ciTrainFullyStoppedTimerDelayLength)
			RETURN TRUE
		ELSE
			PRINTLN("[Trains_SPAM][Train_SPAM ", iTrain, "] IS_TRAIN_PERFECTLY_STILL | Train is slowing down! Waiting for tdTrainFullyStoppedTimer. ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdTrainFullyStoppedTimer[iTrain]), " / ", ciTrainFullyStoppedTimerDelayLength)
		ENDIF
	ELSE
		PRINTLN("[Trains_SPAM][Train_SPAM ", iTrain, "] IS_TRAIN_PERFECTLY_STILL | Train is in motion!")
		
	ENDIF
	
	RETURN FALSE
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Entity State Helpers
// ##### Description: Fillers and wrappers for the entity state structs
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC VECTOR GET_DYNO_PROP_SPAWN_LOCATION(INT iDynoPropIndex)
	VECTOR vSpawnLocation = g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoPropIndex].vPos
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoPropIndex].sDynopropTrainAttachmentData.iTrainIndex != -1
	AND NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoPropIndex].sDynopropTrainAttachmentData.vCarriageOffsetPosition)		
		VECTOR vTemp = <<0.0, 0.0, 0.0>>
		MC_GET_TRAIN_ATTACHMENT_POINT_IN_WORLD_COORDS(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoPropIndex].sDynopropTrainAttachmentData, vSpawnLocation, vTemp)		
		RETURN vSpawnLocation
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoPropIndex].iDynopropBitset, ciFMMC_DYNOPROP_DroppedByEvent)
	AND NOT IS_VECTOR_ZERO(vDroppedDynoPropLocation[iDynoPropIndex])
		vSpawnLocation = vDroppedDynoPropLocation[iDynoPropIndex]	
	ENDIF
	
	RETURN vSpawnLocation
	
ENDFUNC

FUNC VECTOR GET_FMMC_PED_COORDS(FMMC_PED_STATE &sPedState)
	
	IF sPedState.iIndex = -1
		PRINTLN("[Peds] GET_FMMC_PED_COORDS - Ped State Not Filled!")
		ASSERTLN("GET_FMMC_PED_COORDS -  Ped State Not Filled!")
		RETURN <<0,0,0>>
	ENDIF
	
	IF NOT sPedState.bExists
		PRINTLN("[Peds] GET_FMMC_PED_COORDS - Ped Does Not Exist! ", sPedState.iIndex)
		ASSERTLN("GET_FMMC_PED_COORDS -  Ped Does Not Exist! ", sPedState.iIndex)
		RETURN <<0,0,0>>
	ENDIF
	
	IF IS_VECTOR_ZERO(sPedState.___vCoords)
		sPedState.___vCoords = GET_ENTITY_COORDS(sPedState.pedIndex, FALSE)
	ENDIF
	
	RETURN sPedState.___vCoords
	
ENDFUNC

FUNC INTERIOR_INSTANCE_INDEX GET_FMMC_PED_INTERIOR(FMMC_PED_STATE &sPedState)
	
	IF sPedState.iIndex = -1
		PRINTLN("[Peds] GET_FMMC_PED_INTERIOR - Ped State Not Filled!")
		ASSERTLN("GET_FMMC_PED_INTERIOR -  Ped State Not Filled!")
		RETURN NULL
	ENDIF
	
	IF NOT sPedState.bExists
		PRINTLN("[Peds] GET_FMMC_PED_INTERIOR - Ped Does Not Exist! ", sPedState.iIndex)
		ASSERTLN("GET_FMMC_PED_INTERIOR -  Ped Does Not Exist! ", sPedState.iIndex)
		RETURN NULL
	ENDIF
	
	IF sPedState.___iiiCurrentInterior != NULL
		RETURN sPedState.___iiiCurrentInterior
	ENDIF
	
	sPedState.___iiiCurrentInterior = GET_INTERIOR_FROM_ENTITY(sPedState.pedIndex)
	RETURN sPedState.___iiiCurrentInterior
	
ENDFUNC

FUNC FLOAT GET_FMMC_DIST2_FROM_LOCAL_PLAYER_PED(FMMC_PED_STATE &sPedState)
	
	IF sPedState.iIndex = -1
		PRINTLN("[Peds] GET_FMMC_DIST2_FROM_LOCAL_PLAYER_PED - Ped State Not Filled!")
		ASSERTLN("GET_FMMC_DIST2_FROM_LOCAL_PLAYER_PED -  Ped State Not Filled!")
		RETURN 99999999.0
	ENDIF
	
	IF NOT sPedState.bExists
		PRINTLN("[Peds] GET_FMMC_DIST2_FROM_LOCAL_PLAYER_PED - Ped Does Not Exist! ", sPedState.iIndex)
		ASSERTLN("GET_FMMC_DIST2_FROM_LOCAL_PLAYER_PED -  Ped Does Not Exist! ", sPedState.iIndex)
		RETURN 99999999.0
	ENDIF
	
	IF sPedState.fDistFromLocalPlayerPed = 0.0
		sPedState.fDistFromLocalPlayerPed = vDIST2(GET_FMMC_PED_COORDS(sPedState), GET_ENTITY_COORDS(LocalPlayerPed))
	ENDIF
	
	RETURN sPedState.fDistFromLocalPlayerPed
	
ENDFUNC

PROC FILL_FMMC_PED_STEALTH_AND_AGGRO_STRUCT(FMMC_PED_STATE &sPedState)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].sStealthAndAggroSystem.iBS, ciPED_SAS_ForceUseOfPreset)
		INT iPresetToUse = RETURN_INT_CUSTOM_VARIABLE_VALUE(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].sStealthAndAggroSystem.iPresetIndex, CREATION_TYPE_PEDS, sPedState.iIndex)
		sPedState.sStealthAndAggroSystemPed = g_FMMC_STRUCT_ENTITIES.sStealthAndAggroSystemPresets[iPresetToUse]
	ELSE
		sPedState.sStealthAndAggroSystemPed = g_FMMC_STRUCT_ENTITIES.sPlacedPed[sPedState.iIndex].sStealthAndAggroSystem
	ENDIF
	
ENDPROC

PROC FILL_FMMC_PED_STATE_STRUCT(FMMC_PED_STATE &sPedState, INT iPed, BOOL bRefill = FALSE)
	
	IF bRefill
		FMMC_PED_STATE sPedStateBlank
		sPedState = sPedStateBlank
	ENDIF
	
	IF sPedState.iIndex != -1	
		PRINTLN("[Peds] FILL_FMMC_PED_STATE_STRUCT - Already Filled! Index: ", sPedState.iIndex)
		ASSERTLN("FILL_FMMC_PED_STATE_STRUCT - Already Filled! Index: ", sPedState.iIndex)			
		EXIT
	ENDIF
	
	sPedState.iIndex = iPed
	sPedState.niIndex = MC_serverBD_1.sFMMC_SBD.niPed[iPed]
	sPedState.bIsPlacedPed = TRUE
	
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(sPedState.niIndex)
		EXIT
	ENDIF
	
	sPedState.bExists = TRUE
		
	sPedState.pedIndex = NET_TO_PED(sPedState.niIndex)
	
	sPedState.mn = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].mn
	
	FILL_FMMC_PED_STEALTH_AND_AGGRO_STRUCT(sPedState)
	
	IF NETWORK_HAS_CONTROL_OF_ENTITY(sPedState.pedIndex)
		sPedState.bHasControl = TRUE
	ENDIF
	
	IF IS_PED_INJURED(sPedState.pedIndex)
		sPedState.bInjured = TRUE
		EXIT
	ENDIF
	
	IF IS_PED_IN_COMBAT(sPedState.pedIndex)
		SET_BIT(sPedState.iBitset, FMMC_PED_STATE_IN_COMBAT)
	ENDIF
	
	sPedState.bIsOnPlacedTrain = MC_IS_ENTITY_ON_AN_ASSIGNED_TRAIN(sPedState.PedIndex, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sTrainAttachmentData)
		
	IF IS_PED_IN_ANY_VEHICLE(sPedState.pedIndex, FALSE)
		sPedState.bIsInAnyVehicle = TRUE
		sPedState.vehIndexPedIsIn = GET_VEHICLE_PED_IS_IN(sPedState.pedIndex, FALSE)
	ELIF IS_PED_IN_ANY_VEHICLE(sPedState.pedIndex, TRUE)
		sPedState.bEnteringAnyVehicle = TRUE
		sPedState.vehIndexPedIsIn = GET_VEHICLE_PED_IS_IN(sPedState.pedIndex, TRUE)
	ENDIF
	
ENDPROC

PROC FILL_FMMC_GANG_CHASE_PED_STATE_STRUCT(FMMC_PED_STATE &sPedState, INT iGangChaseUnit, INT iPed, BOOL bRefill = FALSE)
	
	IF bRefill
		FMMC_PED_STATE sPedStateBlank
		sPedState = sPedStateBlank
	ENDIF
	
	IF sPedState.iIndex != -1	
		PRINTLN("[Gang Chase][Peds] FILL_FMMC_GANG_CHASE_PED_STATE_STRUCT - Already Filled! Index: ", sPedState.iIndex)
		ASSERTLN("FILL_FMMC_GANG_CHASE_PED_STATE_STRUCT - Already Filled! Index: ", sPedState.iIndex)			
		EXIT
	ENDIF
	
	IF iGangChaseUnit < 0
	OR iGangChaseUnit >= ciMAX_GANG_CHASE_VEHICLES
		PRINTLN("[Gang Chase][Peds] FILL_FMMC_GANG_CHASE_PED_STATE_STRUCT - exiting, iGangChaseUnit is out of bounds: ", iGangChaseUnit)
		EXIT
	ENDIF
	
	IF iPed < 0
	OR iPed >= ciMAX_GANG_CHASE_PEDS_PER_VEHICLE
		PRINTLN("[Gang Chase][Peds] FILL_FMMC_GANG_CHASE_PED_STATE_STRUCT - exiting, iPed is out of bounds: ", iPed)
		EXIT
	ENDIF
	
	sPedState.iIndex = iPed
	sPedState.iGangChaseUnit = iGangChaseUnit
	sPedState.niIndex = MC_serverBD_1.sGangChase[iGangChaseUnit].niPeds[iPed]
	sPedState.bIsPlacedPed = FALSE
	
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(sPedState.niIndex)
		EXIT
	ENDIF
	
	sPedState.bExists = TRUE
	
	sPedState.pedIndex = NET_TO_PED(sPedState.niIndex)
	sPedState.mn = MC_serverBD_1.sGangChase[iGangChaseUnit].mnPedModelNames[iPed]
	
	IF NETWORK_HAS_CONTROL_OF_ENTITY(sPedState.pedIndex)
		sPedState.bHasControl = TRUE
	ENDIF
	
	IF IS_PED_INJURED(sPedState.pedIndex)
		sPedState.bInjured = TRUE
		EXIT
	ENDIF
	
	IF IS_PED_IN_COMBAT(sPedState.pedIndex)
		SET_BIT(sPedState.iBitset, FMMC_PED_STATE_IN_COMBAT)
	ENDIF
		
	IF IS_PED_IN_ANY_VEHICLE(sPedState.pedIndex, FALSE)
		sPedState.bIsInAnyVehicle = TRUE
		sPedState.vehIndexPedIsIn = GET_VEHICLE_PED_IS_IN(sPedState.pedIndex, FALSE)
	ELIF IS_PED_IN_ANY_VEHICLE(sPedState.pedIndex, TRUE)
		sPedState.bEnteringAnyVehicle = TRUE
		sPedState.vehIndexPedIsIn = GET_VEHICLE_PED_IS_IN(sPedState.pedIndex, TRUE)
	ENDIF
	
ENDPROC


PROC FILL_FMMC_VEHICLE_STATE_STRUCT(FMMC_VEHICLE_STATE &sVehState, INT iVeh, BOOL bRefill = FALSE)
	
	IF bRefill
		FMMC_VEHICLE_STATE sVehStateBlank
		sVehState = sVehStateBlank
	ENDIF
	
	IF sVehState.iIndex != -1
		PRINTLN("[Vehicles] FILL_FMMC_VEHICLE_STATE_STRUCT - Already Filled! Index: ", sVehState.iIndex)
		ASSERTLN("FILL_FMMC_VEHICLE_STATE_STRUCT - Already Filled! Index: ", sVehState.iIndex)
		EXIT
	ENDIF
	
	sVehState.iIndex = iVeh
	sVehState.niIndex = MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh]
	
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(sVehState.niIndex)
		EXIT
	ENDIF
	
	sVehState.bExists = TRUE
	
	sVehState.vehIndex = NET_TO_VEH(sVehState.niIndex)
	
	sVehState.mn = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn
	
	sVehState.bIsOnPlacedTrain = MC_IS_ENTITY_ON_AN_ASSIGNED_TRAIN(sVehState.vehIndex, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].sTrainAttachmentData)
		
	IF NETWORK_HAS_CONTROL_OF_ENTITY(sVehState.vehIndex)
		sVehState.bHasControl = TRUE
	ENDIF
	
	IF IS_ENTITY_ALIVE(sVehState.vehIndex)
		sVehState.bAlive = TRUE
	ENDIF
	
	IF NOT IS_VEHICLE_DRIVEABLE(sVehState.vehIndex)
		EXIT
	ENDIF
	
	sVehState.bDrivable = TRUE
	
ENDPROC

FUNC INTERIOR_INSTANCE_INDEX GET_FMMC_VEHICLE_INTERIOR(FMMC_VEHICLE_STATE &sVehState)
	
	IF sVehState.iIndex = -1
		PRINTLN("[Vehicles] GET_FMMC_VEHICLE_INTERIOR - Vehicle State Not Filled!")
		ASSERTLN("GET_FMMC_VEHICLE_INTERIOR -  Vehicle State Not Filled!")
		RETURN NULL
	ENDIF
	
	IF NOT sVehState.bExists
		PRINTLN("[Vehicles] GET_FMMC_VEHICLE_INTERIOR - Vehicle Does Not Exist! ", sVehState.iIndex)
		ASSERTLN("GET_FMMC_VEHICLE_INTERIOR -  Vehicle Does Not Exist! ", sVehState.iIndex)
		RETURN NULL
	ENDIF
	
	IF sVehState.___iiiCurrentInterior != NULL
		RETURN sVehState.___iiiCurrentInterior
	ENDIF
	
	sVehState.___iiiCurrentInterior = GET_INTERIOR_FROM_ENTITY(sVehState.vehIndex)
	RETURN sVehState.___iiiCurrentInterior
	
ENDFUNC

FUNC VECTOR GET_FMMC_VEHICLE_COORDS(FMMC_VEHICLE_STATE &sVehState)
	
	IF sVehState.iIndex = -1
		PRINTLN("[Vehicles] GET_FMMC_VEHICLE_COORDS - Vehicle State Not Filled!")
		ASSERTLN("GET_FMMC_VEHICLE_COORDS -  Vehicle State Not Filled!")
		RETURN <<0,0,0>>
	ENDIF
	
	IF NOT sVehState.bExists
		PRINTLN("[Vehicles] GET_FMMC_VEHICLE_COORDS - Vehicle Does Not Exist! ", sVehState.iIndex)
		ASSERTLN("GET_FMMC_VEHICLE_COORDS -  Vehicle Does Not Exist! ", sVehState.iIndex)
		RETURN <<0,0,0>>
	ENDIF
	
	IF IS_VECTOR_ZERO(sVehState.___vCoords)
		sVehState.___vCoords = GET_ENTITY_COORDS(sVehState.vehIndex, FALSE)
	ENDIF
	
	RETURN sVehState.___vCoords
	
ENDFUNC

FUNC FLOAT GET_FMMC_VEHICLE_CURRENT_BODY_HEALTH(FMMC_VEHICLE_STATE &sVehState)

	IF sVehState.iIndex = -1
		PRINTLN("[Vehicles] GET_FMMC_VEHICLE_CURRENT_BODY_HEALTH - Vehicle State Not Filled!")
		ASSERTLN("GET_FMMC_VEHICLE_CURRENT_BODY_HEALTH -  Vehicle State Not Filled!")
		RETURN 0.0
	ENDIF
	
	IF NOT sVehState.bExists
		PRINTLN("[Vehicles] GET_FMMC_VEHICLE_CURRENT_BODY_HEALTH - Vehicle Does Not Exist! ", sVehState.iIndex)
		ASSERTLN("GET_FMMC_VEHICLE_CURRENT_BODY_HEALTH -  Vehicle Does Not Exist! ", sVehState.iIndex)
		RETURN 0.0
	ENDIF
	
	IF sVehState.___fBodyHealth = 0.0
		sVehState.___fBodyHealth = GET_VEHICLE_BODY_HEALTH(sVehState.vehIndex)
	ENDIF
	
	RETURN sVehState.___fBodyHealth
	
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_IN_FMMC_VEHICLE(FMMC_VEHICLE_STATE &sVehState, BOOL bCheckEntering = FALSE)
	
	IF sVehState.iIndex = -1
		PRINTLN("[Vehicles] IS_LOCAL_PLAYER_IN_FMMC_VEHICLE - Vehicle State Not Filled!")
		ASSERTLN("IS_LOCAL_PLAYER_IN_FMMC_VEHICLE -  Vehicle State Not Filled!")
		RETURN FALSE
	ENDIF
	
	IF NOT sVehState.bExists
		PRINTLN("[Vehicles] IS_LOCAL_PLAYER_IN_FMMC_VEHICLE - Vehicle Does Not Exist! ", sVehState.iIndex)
		RETURN FALSE
	ENDIF
	
	IF sVehState.___iTB_LocalPlayerInVehicle != FMMC_STRUCT_TERTIARY_BOOL_NULL
	AND sVehState.___iTB_LocalPlayerInVehicleEntering != FMMC_STRUCT_TERTIARY_BOOL_NULL
		IF bCheckEntering
			RETURN sVehState.___iTB_LocalPlayerInVehicleEntering = FMMC_STRUCT_TERTIARY_BOOL_TRUE
		ELSE
			RETURN sVehState.___iTB_LocalPlayerInVehicle = FMMC_STRUCT_TERTIARY_BOOL_TRUE
		ENDIF
	ENDIF
	
	IF IS_PED_IN_VEHICLE(LocalPlayerPed, sVehState.vehIndex, TRUE)
		sVehState.___iTB_LocalPlayerInVehicle = FMMC_STRUCT_TERTIARY_BOOL_TRUE
		sVehState.___iTB_LocalPlayerInVehicleEntering = FMMC_STRUCT_TERTIARY_BOOL_TRUE
	ELSE
		IF IS_PED_IN_VEHICLE(LocalPlayerPed, sVehState.vehIndex, FALSE)
			sVehState.___iTB_LocalPlayerInVehicle = FMMC_STRUCT_TERTIARY_BOOL_TRUE
			sVehState.___iTB_LocalPlayerInVehicleEntering = FMMC_STRUCT_TERTIARY_BOOL_FALSE
		ENDIF
	ENDIF
	
	IF bCheckEntering
		RETURN sVehState.___iTB_LocalPlayerInVehicleEntering = FMMC_STRUCT_TERTIARY_BOOL_TRUE
	ELSE
		RETURN sVehState.___iTB_LocalPlayerInVehicle = FMMC_STRUCT_TERTIARY_BOOL_TRUE
	ENDIF
	
ENDFUNC

FUNC BOOL IS_FMMC_VEHICLE_EMPTY(FMMC_VEHICLE_STATE &sVehState)
	
	IF sVehState.iIndex = -1
		PRINTLN("[Vehicles] IS_FMMC_VEHICLE_EMPTY - Vehicle State Not Filled!")
		ASSERTLN("IS_FMMC_VEHICLE_EMPTY -  Vehicle State Not Filled!")
		RETURN FALSE
	ENDIF
	
	IF NOT sVehState.bExists
		PRINTLN("[Vehicles] IS_FMMC_VEHICLE_EMPTY - Vehicle Does Not Exist! ", sVehState.iIndex)
		ASSERTLN("IS_FMMC_VEHICLE_EMPTY -  Vehicle Does Not Exist! ", sVehState.iIndex)
		RETURN FALSE
	ENDIF
	
	IF sVehState.___iTB_VehicleEmpty != FMMC_STRUCT_TERTIARY_BOOL_NULL
		RETURN sVehState.___iTB_VehicleEmpty = FMMC_STRUCT_TERTIARY_BOOL_TRUE
	ENDIF
	
	IF IS_VEHICLE_EMPTY(sVehState.vehIndex) AND IS_VEHICLE_SEAT_FREE(sVehState.vehIndex, VS_DRIVER) AND NOT IS_ANY_PLAYER_IN_VEHICLE(sVehState.vehIndex, TRUE)
		sVehState.___iTB_VehicleEmpty = FMMC_STRUCT_TERTIARY_BOOL_TRUE
	ELSE
		sVehState.___iTB_VehicleEmpty = FMMC_STRUCT_TERTIARY_BOOL_FALSE
	ENDIF
	
	RETURN sVehState.___iTB_VehicleEmpty = FMMC_STRUCT_TERTIARY_BOOL_TRUE
	
ENDFUNC

FUNC BOOL IS_FMMC_VEHICLE_DRIVERS_SEAT_FREE(FMMC_VEHICLE_STATE &sVehState)
	IF sVehState.iIndex = -1
		PRINTLN("[Vehicles] IS_FMMC_VEHICLE_DRIVERS_SEAT_FREE - Vehicle State Not Filled!")
		ASSERTLN("IS_FMMC_VEHICLE_DRIVERS_SEAT_FREE -  Vehicle State Not Filled!")
		RETURN FALSE
	ENDIF
	
	IF NOT sVehState.bExists
		PRINTLN("[Vehicles] IS_FMMC_VEHICLE_DRIVERS_SEAT_FREE - Vehicle Does Not Exist! ", sVehState.iIndex)
		RETURN FALSE
	ENDIF
	
	IF sVehState.___iTB_DriverSeatEmpty != FMMC_STRUCT_TERTIARY_BOOL_NULL
		RETURN sVehState.___iTB_DriverSeatEmpty = FMMC_STRUCT_TERTIARY_BOOL_TRUE
	ENDIF
	
	IF IS_VEHICLE_SEAT_FREE(sVehState.vehIndex)
		sVehState.___iTB_DriverSeatEmpty = FMMC_STRUCT_TERTIARY_BOOL_TRUE
	ELSE
		sVehState.___iTB_DriverSeatEmpty = FMMC_STRUCT_TERTIARY_BOOL_FALSE
	ENDIF
	
	RETURN sVehState.___iTB_DriverSeatEmpty = FMMC_STRUCT_TERTIARY_BOOL_TRUE
	
ENDFUNC

FUNC FLOAT GET_FMMC_VEHICLE_HEADING(FMMC_VEHICLE_STATE &sVehState)
	
	IF sVehState.iIndex = -1
		PRINTLN("[Vehicles] GET_FMMC_VEHICLE_HEADING - Vehicle State Not Filled!")
		ASSERTLN("GET_FMMC_VEHICLE_HEADING -  Vehicle State Not Filled!")
		RETURN -1.0
	ENDIF
	
	IF NOT sVehState.bExists
		PRINTLN("[Vehicles] GET_FMMC_VEHICLE_HEADING - Vehicle Does Not Exist! ", sVehState.iIndex)
		ASSERTLN("GET_FMMC_VEHICLE_HEADING -  Vehicle Does Not Exist! ", sVehState.iIndex)
		RETURN -1.0
	ENDIF
	
	IF sVehState.___fHeading = -1.0
		sVehState.___fHeading = GET_ENTITY_HEADING(sVehState.vehIndex)
	ENDIF
	
	RETURN sVehState.___fHeading
ENDFUNC

FUNC BOOL IS_TRAILER_STUCK(VEHICLE_INDEX tempVeh)

	IF IS_ENTITY_DEAD(tempVeh)
		RETURN FALSE
	ENDIF

	SWITCH GET_ENTITY_MODEL(tempVeh)
		CASE TRFLAT
		CASE TRAILERS
			IF GET_VEHICLE_PETROL_TANK_HEALTH(tempVeh) <= -1000
				RETURN TRUE
			ENDIF
		BREAK
		CASE TANKER
		CASE TANKER2
			IF GET_ENTITY_SUBMERGED_LEVEL(tempVeh) >= 0.5
			AND GET_ENTITY_SPEED(tempVeh) < 1.0
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH

	RETURN FALSE

ENDFUNC

FUNC BOOL IS_FMMC_VEHICLE_TRAILER_STUCK(FMMC_VEHICLE_STATE &sVehState)
	
	IF sVehState.iIndex = -1
		PRINTLN("[Vehicles] IS_FMMC_VEHICLE_TRAILER_STUCK - Vehicle State Not Filled!")
		ASSERTLN("IS_FMMC_VEHICLE_TRAILER_STUCK -  Vehicle State Not Filled!")
		RETURN FALSE
	ENDIF
	
	IF NOT sVehState.bExists
		PRINTLN("[Vehicles] IS_FMMC_VEHICLE_TRAILER_STUCK - Vehicle Does Not Exist! ", sVehState.iIndex)
		ASSERTLN("IS_FMMC_VEHICLE_TRAILER_STUCK -  Vehicle Does Not Exist! ", sVehState.iIndex)
		RETURN FALSE
	ENDIF
	
	IF sVehState.___iTB_TrailerStuck != FMMC_STRUCT_TERTIARY_BOOL_NULL
		RETURN sVehState.___iTB_TrailerStuck = FMMC_STRUCT_TERTIARY_BOOL_TRUE
	ENDIF
	
	IF IS_TRAILER_STUCK(sVehState.vehIndex)
		sVehState.___iTB_TrailerStuck = FMMC_STRUCT_TERTIARY_BOOL_TRUE
	ELSE
		sVehState.___iTB_TrailerStuck = FMMC_STRUCT_TERTIARY_BOOL_FALSE
	ENDIF
	
	RETURN sVehState.___iTB_TrailerStuck = FMMC_STRUCT_TERTIARY_BOOL_TRUE
	
ENDFUNC

FUNC VECTOR GET_FMMC_INTERACTABLE_COORDS(FMMC_INTERACTABLE_STATE &sInteractableState)
	
	IF sInteractableState.iIndex = -1
		PRINTLN("[Interactables] GET_FMMC_INTERACTABLE_COORDS - INTERACTABLE State Not Filled!")
		ASSERTLN("GET_FMMC_INTERACTABLE_COORDS -  INTERACTABLE State Not Filled!")
		RETURN <<0,0,0>>
	ENDIF
	
	IF NOT sInteractableState.bInteractableExists
		PRINTLN("[Interactables] GET_FMMC_INTERACTABLE_COORDS - INTERACTABLE Does Not Exist! ", sInteractableState.iIndex)
		ASSERTLN("GET_FMMC_INTERACTABLE_COORDS -  INTERACTABLE Does Not Exist! ", sInteractableState.iIndex)
		RETURN <<0,0,0>>
	ENDIF
	
	IF IS_VECTOR_ZERO(sInteractableState.___vCoords)
		sInteractableState.___vCoords = GET_ENTITY_COORDS(sInteractableState.objIndex, FALSE)
	ENDIF
	
	RETURN sInteractableState.___vCoords
	
ENDFUNC

FUNC FLOAT GET_DISTANCE_TO_INTERACTABLE(FMMC_INTERACTABLE_STATE &sInteractableState)
	RETURN VDIST2(GET_ENTITY_COORDS(LocalPlayerPed), sInteractableState.vInteractionCoords)
ENDFUNC

PROC SET_INTERACTABLE_INTERACTION_COORDS(FMMC_INTERACTABLE_STATE &sInteractableState, VECTOR vInteractionCoords)
	sInteractableState.vInteractionCoords = vInteractionCoords
	sInteractableState.fDistanceToInteractable = GET_DISTANCE_TO_INTERACTABLE(sInteractableState)
ENDPROC

PROC FILL_FMMC_INTERACTABLE_STATE_STRUCT(FMMC_INTERACTABLE_STATE &sInteractableState, INT iInt, INT iInteractionIndex = -1)
	
	IF sInteractableState.iIndex != -1
		PRINTLN("[Interactables] FILL_FMMC_INTERACTABLE_STATE_STRUCT - Already Filled! Index: ", sInteractableState.iIndex)
		ASSERTLN("FILL_FMMC_INTERACTABLE_STATE_STRUCT - Already Filled! Index: ", sInteractableState.iIndex)
		EXIT
	ENDIF
	
	sInteractableState.iIndex = iInt
	sInteractableState.niIndex = GET_INTERACTABLE_NET_ID(iInt)
	
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(sInteractableState.niIndex)
		EXIT
	ENDIF
	
	sInteractableState.bInteractableExists = TRUE
	
	sInteractableState.ObjIndex = NET_TO_OBJ(sInteractableState.niIndex)
	GET_FMMC_INTERACTABLE_COORDS(sInteractableState) // We will always need the Interactable's coords
	
	sInteractableState.mn = g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInt].mnInteractable_Model
	
	sInteractableState.bHaveControlOfInteractable = NETWORK_HAS_CONTROL_OF_ENTITY(sInteractableState.ObjIndex)
	sInteractableState.bInteractableAlive = IS_ENTITY_ALIVE(sInteractableState.ObjIndex)
	
	sInteractableState.iInteractableHackingStructIndex = GET_HACKING_STRUCT_INDEX_TO_USE(sInteractableState.iIndex, ciENTITY_TYPE_INTERACTABLES)
	
	IF iInteractionIndex > -1
		sInteractableState.iOngoingInteractionIndex = iInteractionIndex
		sInteractableState.bIsBackgroundInteraction = IS_BIT_SET(sOngoingInteractionVars[iInteractionIndex].iOngoingInteractionBS, ciONGOING_INTERACTION_BS__IS_BACKGROUND_INTERACTION)
	ENDIF
	
	SET_INTERACTABLE_INTERACTION_COORDS(sInteractableState, GET_ENTITY_COORDS(sInteractableState.objIndex))
	
	IF IS_INTERACTABLE_OCCUPIED(sInteractableState.iIndex)
		SET_BIT(sInteractableState.iBitset, FMMC_INTERACTABLE_STATE_OCCUPIED)
	ENDIF
ENDPROC


PROC FILL_FMMC_OBJECT_STATE_STRUCT(FMMC_OBJECT_STATE &sObjState, INT iObj, BOOL bResetIfDifferent = FALSE)
	
	IF sObjState.iIndex != -1
		IF bResetIfDifferent
			IF sObjState.iIndex = iObj
				EXIT
			ENDIF
			FMMC_OBJECT_STATE sEmpty
			COPY_SCRIPT_STRUCT(sObjState, sEmpty, SIZE_OF(sEmpty))
		ELSE
			PRINTLN("[Objects] FILL_FMMC_OBJECT_STATE_STRUCT - Already Filled! Index: ", sObjState.iIndex)
			ASSERTLN("FILL_FMMC_OBJECT_STATE_STRUCT - Already Filled! Index: ", sObjState.iIndex)
			EXIT
		ENDIF
	ENDIF
	
	sObjState.iIndex = iObj
	sObjState.niIndex = GET_OBJECT_NET_ID(iObj)
	
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(sObjState.niIndex)
		EXIT
	ENDIF
	
	sObjState.bObjExists = TRUE
	
	sObjState.ObjIndex = NET_TO_OBJ(sObjState.niIndex)
	
	sObjState.mn = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].mn
	
	sObjState.iObjHackingStructIndex = GET_HACKING_STRUCT_INDEX_TO_USE(sObjState.iIndex, ciENTITY_TYPE_OBJECT)
	
	sObjState.bHaveControlOfObj = NETWORK_HAS_CONTROL_OF_ENTITY(sObjState.ObjIndex)
	sObjState.bObjAlive = IS_ENTITY_ALIVE(sObjState.ObjIndex)
	sObjState.bObjBroken = IS_OBJECT_BROKEN_AND_VISIBLE(sObjState.objIndex, TRUE)
	
	IF IS_ENTITY_ATTACHED(sObjState.ObjIndex)
	
		SET_BIT(sObjState.iBitset, FMMC_OBJECT_STATE_ATTACHED_TO_SOMETHING)
	
		IF IS_ENTITY_ATTACHED_TO_ANY_PED(sObjState.ObjIndex)
			SET_BIT(sObjState.iBitset, FMMC_OBJECT_STATE_CARRIED_BY_SOMEONE)
			
			IF IS_ENTITY_ATTACHED_TO_ENTITY(sObjState.ObjIndex, LocalPlayerPed)
				SET_BIT(sObjState.iBitset, FMMC_OBJECT_STATE_CARRIED_BY_LOCAL_PLAYER)
			ELSE
				SET_BIT(sObjState.iBitset, FMMC_OBJECT_STATE_CARRIED_BY_SOMEONE_ELSE)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

FUNC INTERIOR_INSTANCE_INDEX GET_FMMC_OBJECT_INTERIOR(FMMC_OBJECT_STATE &sObjState)
	
	IF sObjState.iIndex = -1
		PRINTLN("[Objects] GET_FMMC_OBJECT_INTERIOR - OBJECT State Not Filled!")
		ASSERTLN("GET_FMMC_OBJECT_INTERIOR -  OBJECT State Not Filled!")
		RETURN NULL
	ENDIF
	
	IF NOT sObjState.bObjExists
		PRINTLN("[Objects] GET_FMMC_OBJECT_INTERIOR - OBJECT Does Not Exist! ", sObjState.iIndex)
		ASSERTLN("GET_FMMC_OBJECT_INTERIOR -  OBJECT Does Not Exist! ", sObjState.iIndex)
		RETURN NULL
	ENDIF
	
	IF sObjState.___iiiCurrentInterior != NULL
		RETURN sObjState.___iiiCurrentInterior
	ENDIF
	
	sObjState.___iiiCurrentInterior = GET_INTERIOR_FROM_ENTITY(sObjState.objIndex)
	RETURN sObjState.___iiiCurrentInterior
	
ENDFUNC

FUNC VECTOR GET_FMMC_OBJECT_COORDS(FMMC_OBJECT_STATE &sObjState)
	
	IF sObjState.iIndex = -1
		PRINTLN("[Objects] GET_FMMC_OBJECT_COORDS - OBJECT State Not Filled!")
		ASSERTLN("GET_FMMC_OBJECT_COORDS -  OBJECT State Not Filled!")
		RETURN <<0,0,0>>
	ENDIF
	
	IF NOT sObjState.bObjExists
		PRINTLN("[Objects] GET_FMMC_OBJECT_COORDS - OBJECT Does Not Exist! ", sObjState.iIndex)
		ASSERTLN("GET_FMMC_OBJECT_COORDS -  OBJECT Does Not Exist! ", sObjState.iIndex)
		RETURN <<0,0,0>>
	ENDIF
	
	IF IS_VECTOR_ZERO(sObjState.___vCoords)
		sObjState.___vCoords = GET_ENTITY_COORDS(sObjState.objIndex, FALSE)
	ENDIF
	
	RETURN sObjState.___vCoords
	
ENDFUNC


PROC FILL_FMMC_DYNOPROP_STATE_STRUCT(FMMC_DYNOPROP_STATE &sDynopropState, INT iDynoprop)
	
	IF sDynopropState.iIndex != -1
		PRINTLN("[Dynoprops] FILL_FMMC_DYNOPROP_STATE_STRUCT - Already Filled! Index: ", sDynopropState.iIndex)
		ASSERTLN("FILL_FMMC_DYNOPROP_STATE_STRUCT - Already Filled! Index: ", sDynopropState.iIndex)
		EXIT
	ENDIF
	
	sDynopropState.iIndex = iDynoprop
	sDynopropState.niIndex = GET_DYNOPROP_NET_ID(iDynoprop)
	
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(sDynopropState.niIndex)
		EXIT
	ENDIF
	
	sDynopropState.bDynopropExists = TRUE
	
	sDynopropState.ObjIndex = NET_TO_OBJ(sDynopropState.niIndex)
	
	sDynopropState.mn = g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoprop].mn
	
	sDynopropState.bHaveControlOfDynoprop = NETWORK_HAS_CONTROL_OF_ENTITY(sDynopropState.ObjIndex)
	sDynopropState.bDynopropAlive = NOT IS_ENTITY_DEAD(sDynopropState.objIndex)

ENDPROC

FUNC VECTOR GET_FMMC_DYNOPROP_COORDS(FMMC_DYNOPROP_STATE &sDynopropState)
	
	IF sDynopropState.iIndex = -1
		PRINTLN("[Dynoprops] GET_FMMC_DYNOPROP_COORDS - DYNOPROP State Not Filled!")
		ASSERTLN("GET_FMMC_DYNOPROP_COORDS -  DYNOPROP State Not Filled!")
		RETURN <<0,0,0>>
	ENDIF
	
	IF NOT sDynopropState.bDynopropExists
		PRINTLN("[Dynoprops] GET_FMMC_DYNOPROP_COORDS - DYNOPROP Does Not Exist! ", sDynopropState.iIndex)
		ASSERTLN("GET_FMMC_DYNOPROP_COORDS -  DYNOPROP Does Not Exist! ", sDynopropState.iIndex)
		RETURN <<0,0,0>>
	ENDIF
	
	IF IS_VECTOR_ZERO(sDynopropState.___vCoords)
		sDynopropState.___vCoords = GET_ENTITY_COORDS(sDynopropState.objIndex, FALSE)
	ENDIF
	
	RETURN sDynopropState.___vCoords
	
ENDFUNC

PROC FILL_FMMC_PLAYER_STATE_STRUCT(FMMC_PLAYER_STATE &sPlayerState, INT iParticipant)
	
	IF sPlayerState.iIndex != -1
		PRINTLN("[Players] FILL_FMMC_PLAYER_STATE_STRUCT - Already Filled! Index: ", sPlayerState.iIndex)
		ASSERTLN("FILL_FMMC_PLAYER_STATE_STRUCT - Already Filled! Index: ", sPlayerState.iIndex)
		EXIT
	ENDIF
	
	sPlayerState.iIndex = iParticipant
	sPlayerState.piPartIndex = INT_TO_PARTICIPANTINDEX(sPlayerState.iIndex)
	
	IF sPlayerState.iIndex = iLocalPart
		//It's us...
		sPlayerState.bParticipantActive = TRUE
		sPlayerState.piPlayerIndex = LocalPlayer
		sPlayerState.bPlayerActive = TRUE
		sPlayerState.bPlayerOK = bLocalPlayerOK
		sPlayerState.piPedIndex = LocalPlayerPed
		sPlayerState.bPedAlive = bLocalPlayerPedOk
		sPlayerState.bSCTV = bIsSCTV
		EXIT
	ENDIF
	
	IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(sPlayerState.piPartIndex)
		sPlayerState.piPlayerIndex = INVALID_PLAYER_INDEX()
		EXIT
	ENDIF
	
	sPlayerState.bParticipantActive = TRUE
	
	sPlayerState.piPlayerIndex = NETWORK_GET_PLAYER_INDEX(sPlayerState.piPartIndex)
	
	sPlayerState.bPlayerActive = NETWORK_IS_PLAYER_ACTIVE(sPlayerState.piPlayerIndex)
	
	IF sPlayerState.bPlayerActive
		sPlayerState.bPlayerOK = IS_PLAYER_PLAYING(sPlayerState.piPlayerIndex)
	ENDIF
	
	sPlayerState.piPedIndex = GET_PLAYER_PED(sPlayerState.piPlayerIndex)
	
	sPlayerState.bPedAlive = !IS_PED_INJURED(sPlayerState.piPedIndex)
	
	sPlayerState.bSCTV = IS_PLAYER_SCTV(sPlayerState.piPlayerIndex)

ENDPROC

FUNC VECTOR GET_FMMC_PLAYER_COORDS(FMMC_PLAYER_STATE &sPlayerState)
	
	IF sPlayerState.iIndex = -1
		PRINTLN("[Players] GET_FMMC_PLAYER_COORDS - Player State Not Filled!")
		ASSERTLN("GET_FMMC_PLAYER_COORDS -  Player State Not Filled!")
		RETURN <<0,0,0>>
	ENDIF
	
	IF !sPlayerState.bParticipantActive
		PRINTLN("[Players] GET_FMMC_PLAYER_COORDS - Player is not active! ", sPlayerState.iIndex)
		ASSERTLN("GET_FMMC_PLAYER_COORDS -  Player is not active! ", sPlayerState.iIndex)
		RETURN <<0,0,0>>
	ENDIF
	
	IF IS_VECTOR_ZERO(sPlayerState.___vCoords)
		sPlayerState.___vCoords = GET_ENTITY_COORDS(sPlayerState.piPedIndex, FALSE)
	ENDIF
	
	RETURN sPlayerState.___vCoords
	
ENDFUNC

PROC FILL_FMMC_PICKUP_STATE_STRUCT(FMMC_PICKUP_STATE &sPickupState, INT iPickup)
	
	IF sPickupState.iIndex != -1
		PRINTLN("[Pickups][Pickup ",iPickup,"] FILL_FMMC_PICKUP_STATE_STRUCT - Already Filled! Index: ", sPickupState.iIndex)
		ASSERTLN("FILL_FMMC_PICKUP_STATE_STRUCT - Already Filled! Index: ", sPickupState.iIndex)
		EXIT
	ENDIF
	
	sPickupState.iIndex = iPickup
	
	sPickupState.puiIndex = pipickup[sPickupState.iIndex]
	
	IF NOT DOES_PICKUP_EXIST(sPickupState.puiIndex)
		EXIT
	ENDIF
	
	sPickupState.bPickupExists = TRUE
	
ENDPROC

FUNC BOOL HAS_FMMC_PICKUP_BEEN_COLLECTED(FMMC_PICKUP_STATE &sPickupState)
	IF NOT sPickupState.bPickupExists
		RETURN FALSE
	ENDIF
	
	IF sPickupState.___bPickupCollected
		RETURN TRUE
	ENDIF
	
	sPickupState.___bPickupCollected = HAS_PICKUP_BEEN_COLLECTED(sPickupState.puiIndex)
	
	IF FMMC_IS_LONG_BIT_SET(iPickupCollectedCleanupLocally, sPickupState.iIndex)
		RETURN TRUE
	ENDIF
	
	RETURN sPickupState.___bPickupCollected
ENDFUNC

FUNC OBJECT_INDEX GET_FMMC_PICKUP_OBJECT(FMMC_PICKUP_STATE &sPickupState)
	
	IF NOT sPickupState.bPickupExists
		RETURN NULL
	ENDIF
	
	IF sPickupState.___oiPickupObject != NULL
		RETURN sPickupState.___oiPickupObject
	ENDIF
	
	sPickupState.___oiPickupObject = GET_PICKUP_OBJECT(sPickupState.puiIndex)
	
	RETURN sPickupState.___oiPickupObject
ENDFUNC

FUNC BOOL DOES_FMMC_PICKUP_OBJECT_EXIST(FMMC_PICKUP_STATE &sPickupState)
	
	IF NOT sPickupState.bPickupExists
		RETURN FALSE
	ENDIF
	
	IF GET_FMMC_PICKUP_OBJECT(sPickupState) = NULL
		RETURN FALSE
	ENDIF
	
	IF sPickupState.___bPickupObjectExist
		RETURN TRUE
	ENDIF
	
	sPickupState.___bPickupObjectExist = DOES_PICKUP_OBJECT_EXIST(sPickupState.puiIndex)
	
	RETURN sPickupState.___bPickupObjectExist
ENDFUNC

PROC PROCESS_COLLECTED_PICKUP_PREREQS(INT iPickup, PLAYER_INDEX piPlayerWhoCollectedPickup)

	IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickup].iPickupPrerequisiteToCompleteOnPickup = ciPREREQ_None
		// There's no PreReq assigned to this pickup!
		EXIT
	ENDIF

	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickup].iPlacedBitset, ciFMMC_WEP_CompletePreReqLocalPlayerOnly)
		IF piPlayerWhoCollectedPickup = LocalPlayer
			SET_PREREQUISITE_COMPLETED(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickup].iPickupPrerequisiteToCompleteOnPickup)
			PRINTLN("[Pickups][Pickup ", iPickup, "][MissionEquipment] PROCESS_COLLECTED_PICKUP_PREREQS | Setting PreReq ", g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickup].iPickupPrerequisiteToCompleteOnPickup, " as complete due to us being the one to pick up pickup ", iPickup)
		ELSE
			PRINTLN("[Pickups][Pickup ", iPickup, "][MissionEquipment] PROCESS_COLLECTED_PICKUP_PREREQS | Pickup ", iPickup, " is set to only complete prereq ", g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickup].iPickupPrerequisiteToCompleteOnPickup, " for the player who picks it up!")
		ENDIF
	ELSE
		SET_PREREQUISITE_COMPLETED(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickup].iPickupPrerequisiteToCompleteOnPickup)
		PRINTLN("[Pickups][Pickup ", iPickup, "][MissionEquipment] PROCESS_COLLECTED_PICKUP_PREREQS | Setting PreReq ", g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickup].iPickupPrerequisiteToCompleteOnPickup, " as complete due to someone picking up pickup ", iPickup)
	ENDIF
ENDPROC

FUNC BOOL DOES_FMMC_PICKUP_OBJECT_ENTITY_EXIST(FMMC_PICKUP_STATE &sPickupState)
	
	IF NOT sPickupState.bPickupExists
		RETURN FALSE
	ENDIF
	
	IF GET_FMMC_PICKUP_OBJECT(sPickupState) = NULL
		RETURN FALSE
	ENDIF
	
	IF sPickupState.___bPickupObjectEntityExist
		RETURN TRUE
	ENDIF
	
	sPickupState.___bPickupObjectEntityExist = DOES_ENTITY_EXIST(GET_FMMC_PICKUP_OBJECT(sPickupState))
	
	RETURN sPickupState.___bPickupObjectEntityExist
ENDFUNC

PROC FILL_FMMC_TRAIN_STATE_STRUCT(FMMC_TRAIN_STATE &sTrainState, INT iTrain)
	
	IF sTrainState.iIndex != -1
		PRINTLN("[Trains] FILL_FMMC_TRAIN_STATE_STRUCT - Already Filled! Index: ", sTrainState.iIndex)
		ASSERTLN("FILL_FMMC_TRAIN_STATE_STRUCT - Already Filled! Index: ", sTrainState.iIndex)
		EXIT
	ENDIF
	
	sTrainState.iIndex = iTrain
	sTrainState.niIndex = MC_serverBD_1.sFMMC_SBD.niTrain[iTrain]
	
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(sTrainState.niIndex)
		EXIT
	ENDIF
	
	sTrainState.bTrainExists = TRUE
	
	sTrainState.viTrainIndex = NET_TO_VEH(sTrainState.niIndex)
	
	sTrainState.bHaveControlOfTrain = NETWORK_HAS_CONTROL_OF_ENTITY(sTrainState.viTrainIndex)
	sTrainState.bTrainAlive = NOT IS_ENTITY_DEAD(sTrainState.viTrainIndex)

ENDPROC

FUNC VECTOR GET_FMMC_TRAIN_COORDS(FMMC_TRAIN_STATE &sTrainState)
	
	IF sTrainState.iIndex = -1
		PRINTLN("[Trains] GET_FMMC_TRAIN_COORDS - TRAIN State Not Filled!")
		ASSERTLN("GET_FMMC_TRAIN_COORDS -  TRAIN State Not Filled!")
		RETURN <<0,0,0>>
	ENDIF
	
	IF NOT sTrainState.bTrainExists
		PRINTLN("[Trains] GET_FMMC_TRAIN_COORDS - TRAIN Does Not Exist! ", sTrainState.iIndex)
		ASSERTLN("GET_FMMC_TRAIN_COORDS -  TRAIN Does Not Exist! ", sTrainState.iIndex)
		RETURN <<0,0,0>>
	ENDIF
	
	IF IS_VECTOR_ZERO(sTrainState.___vCoords)
		sTrainState.___vCoords = GET_ENTITY_COORDS(sTrainState.viTrainIndex, FALSE)
	ENDIF
	
	RETURN sTrainState.___vCoords
	
ENDFUNC

FUNC BOOL IS_FMMC_TRAIN_EMPTY(FMMC_TRAIN_STATE &sTrainState)
	
	IF sTrainState.iIndex = -1
		PRINTLN("[Trains] IS_FMMC_TRAIN_EMPTY - Train State Not Filled!")
		ASSERTLN("IS_FMMC_TRAIN_EMPTY -  Train State Not Filled!")
		RETURN FALSE
	ENDIF
	
	IF NOT sTrainState.bTrainExists
		PRINTLN("[Trains] IS_FMMC_TRAIN_EMPTY - Train Does Not Exist! ", sTrainState.iIndex)
		ASSERTLN("IS_FMMC_TRAIN_EMPTY -  Train Does Not Exist! ", sTrainState.iIndex)
		RETURN FALSE
	ENDIF
	
	IF sTrainState.___bTrainEmpty
		RETURN TRUE
	ENDIF
	
	sTrainState.___bTrainEmpty = (IS_VEHICLE_EMPTY(sTrainState.viTrainIndex) AND IS_VEHICLE_SEAT_FREE(sTrainState.viTrainIndex, VS_DRIVER) AND NOT IS_ANY_PLAYER_IN_VEHICLE(sTrainState.viTrainIndex, TRUE))
	
	RETURN sTrainState.___bTrainEmpty
	
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: World Props
// ##### Description: Various helper functions to do with world props
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
FUNC MODEL_NAMES GET_WORLD_PROP_MODEL_SWAP_MODEL_NAME(INT iWorldProp)
	
	IF NOT IS_CUSTOM_STRING_LIST_STRING_VALID(g_FMMC_STRUCT_ENTITIES.sWorldProps[iWorldProp].iModelSwapStringIndex)
		RETURN DUMMY_MODEL_FOR_SCRIPT
	ENDIF
	
	INT iModelSwapHash = GET_HASH_KEY(GET_CUSTOM_STRING_LIST_STRING(g_FMMC_STRUCT_ENTITIES.sWorldProps[iWorldProp].iModelSwapStringIndex))
	MODEL_NAMES mnWorldPropModelToSwapTo = INT_TO_ENUM(MODEL_NAMES, iModelSwapHash)
	
	RETURN mnWorldPropModelToSwapTo
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: World
// ##### Description: Various helper functions to do with world state
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

/// PURPOSE: This function will find and return the first unused array position in the iWaterCalmingQuad array of water calming quads.
///    If a value of -1 is returned, there are no free entries in the array and we have reached the maximum number of water calming quads.
///    This value is currently set at 8 which is a script limit, but the code limit on the number of quads is also 8.
FUNC INT GET_FREE_WATER_CALMING_QUAD()
	
	INT iQuadNum = -1
	
	INT iQuad
	
	FOR iQuad = 0 TO (FMMC_MAX_WATER_CALMING_QUADS - 1)
		IF iWaterCalmingQuad[iQuad] = -1
			iQuadNum = iQuad
			iQuad = FMMC_MAX_WATER_CALMING_QUADS
		ENDIF
	ENDFOR
	
	RETURN iQuadNum
	
ENDFUNC

PROC MC_SET_PED_AND_TRAFFIC_DENSITIES(INT& iHandle, BOOL& bVehGen, FLOAT fPedDensity, FLOAT fTrafficDensity)
	BOOL bLocalOnly = TRUE
	VECTOR vMinWorldLimitCoords = <<-16000, -16000, -1700>>
	VECTOR vMaxWorldLimitCoords = <<16000, 16000, 2700>>
	
	IF iHandle = -1 
		//Can't use DOES_POP_MULTIPLIER_AREA_EXIST(iHandle) here as -1 is an invalid index for it.
		iHandle = ADD_POP_MULTIPLIER_AREA(vMinWorldLimitCoords, vMaxWorldLimitCoords, fPedDensity, fTrafficDensity, bLocalOnly)
	ENDIF
	
	IF fPedDensity = 0 AND fTrafficDensity = 0
		IF NOT bVehGen
			SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(vMinWorldLimitCoords, vMaxWorldLimitCoords, FALSE, FALSE)
			PRINTLN("[Traffic][Zones] [MC_SET_PED_AND_TRAFFIC_DENSITIES] SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA = FALSE")
			bVehGen = TRUE
			
			CLEAR_AREA_OF_PROJECTILES((<<0.0, 0.0, 0.0>>), 10000, FALSE)
			CLEAR_AREA_OF_VEHICLES((<<0.0, 0.0, 0.0>>), 10000, TRUE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
			CLEAR_AREA_OF_PEDS((<<0.0, 0.0, 0.0>>), 10000, TRUE)
		ENDIF
	ENDIF
		
	PRINTLN("[Traffic][Zones] [MC_SET_PED_AND_TRAFFIC_DENSITIES] fPedDensity = ", fPedDensity, "  fTrafficDensity = ", fTrafficDensity, " iHandle = ", iHandle)
	
ENDPROC

PROC MC_CLEAR_PED_AND_TRAFFIC_DENSITIES(INT& iHandle, BOOL& bVehGen)
	BOOL bLocalOnly = TRUE
	VECTOR vMinWorldLimitCoords = <<-16000, -16000, -1700>>
	VECTOR vMaxWorldLimitCoords = <<16000, 16000, 2700>>
	
	IF iHandle > -1
		//Added to stop assert - 2116964 - No area registered for index 0 - either not registered or already freed up?
		IF DOES_POP_MULTIPLIER_AREA_EXIST(iHandle)
			REMOVE_POP_MULTIPLIER_AREA(iHandle, bLocalOnly)
		ENDIF
		iHandle = -1
	ENDIF
	
	IF bVehGen
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(vMinWorldLimitCoords,vMaxWorldLimitCoords,TRUE,FALSE)
		SET_ROADS_BACK_TO_ORIGINAL(vMinWorldLimitCoords,vMaxWorldLimitCoords, (NOT bLocalOnly))
		bVehGen = FALSE
	ENDIF
	
	PRINTLN("[Traffic][Zones][MC_CLEAR_PED_AND_TRAFFIC_DENSITIES] iHandle = ", iHandle)
	
ENDPROC

FUNC BOOL IS_CURRENTLY_NIGHT()
	IF GET_CLOCK_HOURS() > 17
	OR GET_CLOCK_HOURS() < 5
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_CURRENTLY_DAY()
	RETURN !IS_CURRENTLY_NIGHT()
ENDFUNC

PROC FREEZE_TOD_FOR_CUTSCENE()

	IF (iTODh = 0 and iTODm = 0 and  iTODs = 0)
		iTODh = GET_CLOCK_HOURS()
		iTODm = GET_CLOCK_MINUTES()
		iTODs = GET_CLOCK_SECONDS()
		PRINTLN("[RCC MISSION] 2 getting network TOD hours: ",iTODh," mins: ",iTODm," secs: ",iTODs)
		NETWORK_OVERRIDE_CLOCK_TIME(iTODh,iTODm,iTODs)
		SET_BIT(iLocalBoolCheck7, LBOOL7_BLOCK_TOD_EVERY_FRAME)
	ELSE
		NETWORK_OVERRIDE_CLOCK_TIME(iTODh,iTODm,iTODs)
	ENDIF

ENDPROC

FUNC BOOL IS_ENTITY_IN_OPEN_WATER(ENTITY_INDEX eiEntityToCheck)

	FLOAT fWaterHeight
	VECTOR vFwd = GET_ENTITY_FORWARD_VECTOR(eiEntityToCheck)
	VECTOR vEntPos = GET_ENTITY_COORDS(eiEntityToCheck)
	INT iDistance = g_FMMC_STRUCT.iOpenWaterRange
	INT iHeightIncrease = 50
	
	//Forwards
	VECTOR vPoint = <<vFwd.x * iDistance, vFwd.y * iDistance, vFwd.z>>
	vPoint = vEntPos + vPoint
	vPoint.z = vEntPos.z + iHeightIncrease
	
	IF !GET_WATER_HEIGHT(vPoint, fWaterHeight)
		PRINTLN("[RCC MISSION] IS_ENTITY_IN_OPEN_WATER - land found infront of entity, range: ", iDistance)			
		RETURN FALSE
	ENDIF
	
	//Backwards
	vPoint = <<-vFwd.x * iDistance, -vFwd.y * iDistance, vFwd.z>>
	vPoint = vEntPos + vPoint
	vPoint.z = vEntPos.z + iHeightIncrease
	IF !GET_WATER_HEIGHT(vPoint, fWaterHeight)
		PRINTLN("[RCC MISSION] IS_ENTITY_IN_OPEN_WATER - land found behind entity, range: ", iDistance)			
		RETURN FALSE
	ENDIF
	
	//Right
	vPoint = <<vFwd.y * iDistance, -vFwd.x * iDistance, vFwd.z>>
	vPoint = vEntPos + vPoint
	vPoint.z = vEntPos.z + iHeightIncrease
	IF !GET_WATER_HEIGHT(vPoint, fWaterHeight)
		PRINTLN("[RCC MISSION] IS_ENTITY_IN_OPEN_WATER - land found to the right of entity, range: ", iDistance)			
		RETURN FALSE
	ENDIF
	
	//Left
	vPoint = <<-vFwd.y * iDistance, vFwd.x * iDistance, vFwd.z>>
	vPoint = vEntPos + vPoint
	vPoint.z = vEntPos.z + iHeightIncrease
	IF !GET_WATER_HEIGHT(vPoint, fWaterHeight)
		PRINTLN("[RCC MISSION] IS_ENTITY_IN_OPEN_WATER - land found to the left of entity, range: ", iDistance)			
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC INT GET_INTERIOR_DESTRUCTION_TIME_REMAINING(INT iDestructionIndex)

	IF NOT HAS_NET_TIMER_STARTED(sInteriorDestructionVars[iDestructionIndex].stInteriorDestructionTimer)
		RETURN -1
	ENDIF
	
	INT iTimePassed = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(sInteriorDestructionVars[iDestructionIndex].stInteriorDestructionTimer)
	INT iTimeLeft = g_FMMC_STRUCT.sInteriorDestructionData[iDestructionIndex].iInteriorDestructionTimeLimit - iTimePassed
	
	RETURN iTimeLeft
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Artificial Lights
// ##### Description: Various helper functions to do with artificial lights/EMP
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL SHOULD_EMP_BE_LIMITED_TO_SPECIFIED_INTERIOR()

	IF IS_VALID_INTERIOR(intEMPSpecifiedInterior)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_EMP_BE_IGNORED_DUE_TO_CURRENT_INTERIOR()
	IF SHOULD_EMP_BE_LIMITED_TO_SPECIFIED_INTERIOR()
		IF LocalPlayerCurrentInterior != intEMPSpecifiedInterior
			// The local player isn't in the relevant interior!
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_SPECTATOR_REAPPLY_TURNED_OFF_LIGHTS()
	IF NOT bIsAnySpectator	
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck35, LBOOL35_APPLY_LIGHTS_OFF_TC_MODIFIER_FOR_SPECTATOR)
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(MC_PlayerBD[iPartToUse].iClientBitset3, PBBOOL3_TURNED_ON_LIGHTS_OFF_TC_MODIFIER)
	AND NOT IS_BIT_SET(iLocalBoolCheck31, LBOOL31_TURNED_ON_LIGHTS_OFF_TC_MODIFIER)				
		RETURN TRUE
	ENDIF	
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_ARTIFICIAL_LIGHTS_BE_TURNED_OFF_AGAIN()
	
	IF SHOULD_SPECTATOR_REAPPLY_TURNED_OFF_LIGHTS()
		// The spectator needs to reapply the TC Mod.
		PRINTLN("[ArtificialLights] SHOULD_ARTIFICIAL_LIGHTS_BE_TURNED_OFF_AGAIN | Lights may be broken due to Spectator INIT procedures. Returning TRUE.")
		RETURN TRUE
	ENDIF
	
	IF iArtificialLightsOffTCIndex != -1
	AND iArtificialLightsOffTCIndex != GET_TIMECYCLE_MODIFIER_INDEX()
		// Somehow our lights off TC modifier has been taken off!
		PRINTLN("[ArtificialLights] SHOULD_ARTIFICIAL_LIGHTS_BE_TURNED_OFF_AGAIN | Lights are in the wrong state according to iArtificialLightsOffTCIndex! (", iArtificialLightsOffTCIndex, ")")
		RETURN TRUE
	ENDIF
	
	// Lights are in the correct state
	RETURN FALSE
ENDFUNC

FUNC BOOL ARE_LIGHTS_TURNED_OFF()
	RETURN g_bFMMCLightsTurnedOff
ENDFUNC

FUNC BOOL ARE_LIGHTS_TURNED_ON()
	RETURN NOT ARE_LIGHTS_TURNED_OFF()
ENDFUNC

FUNC BOOL SHOULD_EMP_IMPACT_PED_VISION(PED_INDEX piPed)
	// Can still see if they're outside in daytime
	IF IS_CURRENTLY_DAY()
		IF piPed != NULL
			IF GET_INTERIOR_FROM_ENTITY(piPed) = NULL
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC RESET_ARTIFICIAL_LIGHTS_LOCALLY(BOOL bPlaySound = TRUE)
	
	eLocalArtificialLightsState = eARTIFICIAL_LIGHTS_STATE__DEFAULT
	intEMPSpecifiedInterior = NULL
	iMaxEMPDuration = -1
	PRINTLN("[ArtificialLights] RESET_ARTIFICIAL_LIGHTS_LOCALLY | Resetting artificial lights locally!")
	
	IF bPlaySound
		CLEAR_BIT(iLocalBoolCheck5, LBOOL5_SKIP_ARTIFICIAL_LIGHT_SOUNDS)
	ELSE
		SET_BIT(iLocalBoolCheck5, LBOOL5_SKIP_ARTIFICIAL_LIGHT_SOUNDS)
	ENDIF
ENDPROC

PROC TURN_OFF_ARTIFICIAL_LIGHTS_LOCALLY(BOOL bPlaySound = TRUE)
	
	eLocalArtificialLightsState = eARTIFICIAL_LIGHTS_STATE__OFF
	PRINTLN("[ArtificialLights] TURN_OFF_ARTIFICIAL_LIGHTS_LOCALLY | Turning off artificial lights locally!")
	
	IF bPlaySound
		CLEAR_BIT(iLocalBoolCheck5, LBOOL5_SKIP_ARTIFICIAL_LIGHT_SOUNDS)
	ELSE
		SET_BIT(iLocalBoolCheck5, LBOOL5_SKIP_ARTIFICIAL_LIGHT_SOUNDS)
	ENDIF
	
	REENABLE_AMBIENT_NIGHT_VISION()
	CLEAR_BIT(iLocalBoolCheck34, LBOOL34_BLOCK_EMP_SOUNDS_DUE_TO_SPECIFIED_INTERIOR)
		
	INT iPed
	FOR iPed = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfPeds - 1
		
		IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_ServerBD_1.sFMMC_SBD.niPed[iPed])
			RELOOP
		ENDIF
	
		PED_INDEX piPed = NET_TO_PED(MC_ServerBD_1.sFMMC_SBD.niPed[iPed])
		IF NOT IS_ENTITY_ALIVE(piPed)
			RELOOP
		ENDIF
		
		eCachedPedCombatRanges[iPed] = GET_PED_COMBAT_RANGE(piPed)
		
		IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(piPed)
			RELOOP
		ENDIF
		
		IF SHOULD_EMP_IMPACT_PED_VISION(piPed)
			FLOAT fNewRange = GET_FMMC_PED_PERCEPTION_RANGE(iPed, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedPerceptionSettings, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].fPedPerceptionCentreRange, iPedSightModifierTOD, iPedSightModifierWeather)
			fNewRange *= 0.01
			fNewRange *= g_FMMC_STRUCT.sPhoneEMPSettings.iPhoneEMP_PerceptionPercentage
			
			PRINTLN("[ArtificialLights] Setting ped ", iPed, " seeing range to ", fNewRange)
			SET_PED_SEEING_RANGE(piPed, fNewRange)
			SET_PED_COMBAT_RANGE(piPed, CR_NEAR)
		ENDIF
		
	ENDFOR
ENDPROC

PROC TURN_ON_ARTIFICIAL_LIGHTS_LOCALLY(BOOL bPlaySound = TRUE)

	eLocalArtificialLightsState = eARTIFICIAL_LIGHTS_STATE__ON
	PRINTLN("[ArtificialLights] TURN_ON_ARTIFICIAL_LIGHTS_LOCALLY | Turning on artificial lights locally!")
	
	IF bPlaySound
		CLEAR_BIT(iLocalBoolCheck5, LBOOL5_SKIP_ARTIFICIAL_LIGHT_SOUNDS)
	ELSE
		SET_BIT(iLocalBoolCheck5, LBOOL5_SKIP_ARTIFICIAL_LIGHT_SOUNDS)
	ENDIF
	
	DISABLE_AMBIENT_NIGHT_VISION()
	SET_BIT(iLocalBoolCheck32, LBOOL32_TURN_OFF_VISUAL_AIDS_ASAP)
	CLEAR_BIT(iLocalBoolCheck34, LBOOL34_BLOCK_EMP_SOUNDS_DUE_TO_SPECIFIED_INTERIOR)
		
	INT iPed
	FOR iPed = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfPeds - 1
		
		IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_ServerBD_1.sFMMC_SBD.niPed[iPed])
			RELOOP
		ENDIF
	
		PED_INDEX piPed = NET_TO_PED(MC_ServerBD_1.sFMMC_SBD.niPed[iPed])
		IF NOT IS_ENTITY_ALIVE(piPed)
			RELOOP
		ENDIF
		
		IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(piPed)
			RELOOP
		ENDIF
		
		IF SHOULD_EMP_IMPACT_PED_VISION(piPed)
			SET_PED_SEEING_RANGE(piPed, GET_FMMC_PED_PERCEPTION_RANGE(iPed, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedPerceptionSettings, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].fPedPerceptionCentreRange, iPedSightModifierTOD, iPedSightModifierWeather))
			SET_PED_COMBAT_RANGE(piPed, eCachedPedCombatRanges[iPed])
		ENDIF
		
		WEAPON_TYPE wtCurrentWep = WEAPONTYPE_INVALID
		IF GET_CURRENT_PED_WEAPON(piPed, wtCurrentWep)
			IF wtCurrentWep = WEAPONTYPE_DLC_FLASHLIGHT
				WEAPON_TYPE wtBestWep = GET_BEST_PED_WEAPON(piPed)
				SET_CURRENT_PED_WEAPON(piPed, wtBestWep, FALSE)
			ENDIF
		ENDIF
		PRINTLN("[ArtificialLights] Setting ped ", iPed, " setting seeing range to ", GET_FMMC_PED_PERCEPTION_RANGE(iPed, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedPerceptionSettings, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].fPedPerceptionCentreRange, iPedSightModifierTOD, iPedSightModifierWeather))
	ENDFOR
ENDPROC

FUNC BOOL ARE_LIGHTS_OFF_DUE_TO_CURRENT_RULE()
	
	IF ARE_LIGHTS_TURNED_OFF()
	AND g_FMMC_STRUCT.sFMMCEndConditions[g_FMMC_STRUCT.iArtificialLightControlTeam].iArtificalLightTimer[GET_TEAM_CURRENT_RULE(g_FMMC_STRUCT.iArtificialLightControlTeam)] >= FMMC_ARTIFICIAL_LIGHTS_INSTANTLY_OFF
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL IS_PLAYER_TRIGGERED_EMP_CURRENTLY_ACTIVE()
	IF IS_PHONE_EMP_CURRENTLY_ACTIVE()
		RETURN TRUE
	ENDIF
	
	IF ARE_LIGHTS_OFF_DUE_TO_CURRENT_RULE()
		// The lights have been turned off due to mission progression (currently assumed to be caused by NPCs/some other character in the story) and not a player-triggered EMP
		RETURN FALSE
	ENDIF
	
	IF ARE_LIGHTS_TURNED_OFF()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_NPC_TRIGGERED_EMP_CURRENTLY_ACTIVE()

	IF ARE_LIGHTS_OFF_DUE_TO_CURRENT_RULE()
		// The lights have been turned off due to mission progression (currently assumed to be caused by NPCs/some other character in the story) and not a player-triggered EMP
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ANY_EMP_CURRENTLY_ACTIVE()
	
	IF IS_PLAYER_TRIGGERED_EMP_CURRENTLY_ACTIVE()
		RETURN TRUE
	ENDIF
	IF IS_NPC_TRIGGERED_EMP_CURRENTLY_ACTIVE()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Doors
// ##### Description: Various helper functions to do with doors
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC INT GET_NTH_CLOSEST_DOOR_TO_COORD(VECTOR vCoord, INT i)

	IF i >= FMMC_MAX_NUM_DOORS
	OR i < -1
		RETURN -1
	ENDIF
	
	FLOAT fDistance[FMMC_MAX_NUM_DOORS]
	INT iDoorIndexSorted[FMMC_MAX_NUM_DOORS]
	
	INT iDoor
	FOR iDoor = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfDoors - 1	
		fDistance[iDoor] = VDIST2(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].vPos, vCoord)
		iDoorIndexSorted[iDoor] = iDoor
	ENDFOR
	
	QUICK_SORT_FLOAT_WITH_INDEXES(fDistance, iDoorIndexSorted, 0, g_FMMC_STRUCT_ENTITIES.iNumberOfDoors-1)		
	
	RETURN iDoorIndexSorted[i] 
ENDFUNC

FUNC INT GET_DOOR_CONFIGURATION_INDEX(INT iDoor)
	RETURN iDoorCurrentConfig[iDoor]
ENDFUNC

/// PURPOSE:
///    Get the door's intended MODEL_NAME for the given config
/// PARAMS:
///    iDoor - 
///    iConfig - Pass in -1 to have it check the door's current config
/// RETURNS:
///    
FUNC MODEL_NAMES GET_DOOR_MODEL(INT iDoor, INT iConfig = -1)
	
	IF iConfig = -1
		iConfig = GET_DOOR_CONFIGURATION_INDEX(iDoor)
	ENDIF
	
	MODEL_NAMES mnDoorModel = g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].mnDoorModel // Set it to the base model by default
	
	INT iModelSwapStringIndex = g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].sDoorConfigs[iConfig].iDoorModelSwapStringIndex
	IF iModelSwapStringIndex > -1
		mnDoorModel = GET_CUSTOM_STRING_LIST_MODEL_NAME(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].sDoorConfigs[iConfig].iDoorModelSwapStringIndex)
	ENDIF
	
	RETURN mnDoorModel
ENDFUNC

FUNC BOOL HAS_DOOR_BEEN_INITIALISED(INT iDoor)
	RETURN IS_BIT_SET(iDoorHasBeenInitialisedBS, iDoor)
ENDFUNC

PROC SET_DOOR_NEEDS_TO_REFRESH(INT iDoor)
	SET_BIT(iDoorNeedsRefreshBS, iDoor)
	PRINTLN("[Doors][Door ", iDoor, "] SET_DOOR_NEEDS_TO_REFRESH | Setting door to be processed")
ENDPROC

FUNC INT GET_DOOR_HASH(INT iDoor)
	IF iCachedDoorHashes[iDoor] = 0
		iCachedDoorHashes[iDoor] = GET_PLACED_DOOR_RUNTIME_HASH(iDoor)
	ENDIF
	
	//PRINTLN("[Doors_SPAM][Door_SPAM ", iDoor, "] GET_DOOR_HASH | iCachedDoorHashes[", iDoor, "]: ", iCachedDoorHashes[iDoor])
	RETURN iCachedDoorHashes[iDoor]
ENDFUNC

FUNC BOOL IS_DOOR_NETWORKED(INT iDoor)
	RETURN NETWORK_IS_DOOR_NETWORKED(GET_DOOR_HASH(iDoor))
ENDFUNC

FUNC BOOL SHOULD_DOOR_BE_NETWORKED(INT iDoor)
	UNUSED_PARAMETER(iDoor)
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_DOOR_EXTERNALLY_UNLOCKED(INT iDoor)
	RETURN IS_BIT_SET(iDoorExternallyUnlockedBS, iDoor)
ENDFUNC
FUNC BOOL IS_DOOR_EXTERNALLY_LOCKED(INT iDoor)
	RETURN IS_BIT_SET(iDoorExternallyLockedBS, iDoor)
ENDFUNC

PROC SET_DOOR_AS_UNLOCKED_LOCAL(INT iDoor)
	IF NOT IS_BIT_SET(iDoorExternallyUnlockedBS, iDoor)
		SET_BIT(iDoorExternallyUnlockedBS, iDoor)
		PRINTLN("[Doors][Door ", iDoor, "] SET_DOOR_AS_UNLOCKED_LOCAL | Unlocking door due to external factors! Callstack below:")
		DEBUG_PRINTCALLSTACK()
		
		SET_DOOR_NEEDS_TO_REFRESH(iDoor)
	ENDIF
ENDPROC
PROC UNSET_DOOR_AS_UNLOCKED_LOCAL(INT iDoor)
	IF IS_BIT_SET(iDoorExternallyUnlockedBS, iDoor)
		CLEAR_BIT(iDoorExternallyUnlockedBS, iDoor)
		PRINTLN("[Doors][Door ", iDoor, "] UNSET_DOOR_AS_UNLOCKED_LOCAL | Re-locking door due to external factors! Callstack below:")
		DEBUG_PRINTCALLSTACK()
		
		SET_DOOR_NEEDS_TO_REFRESH(iDoor)
	ENDIF
ENDPROC

PROC SET_DOOR_AS_LOCKED_LOCAL(INT iDoor)
	IF NOT IS_BIT_SET(iDoorExternallyLockedBS, iDoor)
		SET_BIT(iDoorExternallyLockedBS, iDoor)
		PRINTLN("[Doors][Door ", iDoor, "] SET_DOOR_AS_UNLOCKED_LOCAL | Locking door due to external factors! Callstack below:")
		DEBUG_PRINTCALLSTACK()
		
		SET_DOOR_NEEDS_TO_REFRESH(iDoor)
	ENDIF
ENDPROC
PROC UNSET_DOOR_AS_LOCKED_LOCAL(INT iDoor)
	IF IS_BIT_SET(iDoorExternallyLockedBS, iDoor)
		CLEAR_BIT(iDoorExternallyLockedBS, iDoor)
		PRINTLN("[Doors][Door ", iDoor, "] UNSET_DOOR_AS_LOCKED_LOCAL | Re-unlocking door due to external factors! Callstack below:")
		DEBUG_PRINTCALLSTACK()
		
		SET_DOOR_NEEDS_TO_REFRESH(iDoor)
	ENDIF
ENDPROC

PROC SET_DOOR_TO_USE_ALT_CONFIG_LOCAL(INT iDoor)
	IF NOT IS_BIT_SET(iDoorExternallySetToUseAltConfigBS, iDoor)
		SET_BIT(iDoorExternallySetToUseAltConfigBS, iDoor)
		PRINTLN("[Doors][Door ", iDoor, "] SET_DOOR_TO_USE_ALT_CONFIG_LOCAL | Setting door to use Alt Config due to external factors! Callstack below:")
		DEBUG_PRINTCALLSTACK()
		
		SET_DOOR_NEEDS_TO_REFRESH(iDoor)
	ENDIF
ENDPROC
PROC UNSET_DOOR_TO_USE_ALT_CONFIG_LOCAL(INT iDoor)
	IF IS_BIT_SET(iDoorExternallySetToUseAltConfigBS, iDoor)
		CLEAR_BIT(iDoorExternallySetToUseAltConfigBS, iDoor)
		PRINTLN("[Doors][Door ", iDoor, "] SET_DOOR_TO_USE_ALT_CONFIG_LOCAL | Setting door to stop using Alt Config due to external factors! Callstack below:")
		DEBUG_PRINTCALLSTACK()
		
		SET_DOOR_NEEDS_TO_REFRESH(iDoor)
	ENDIF
ENDPROC

FUNC BOOL IS_DOOR_REGISTERED(INT iDoor)
	RETURN IS_DOOR_REGISTERED_WITH_SYSTEM(GET_DOOR_HASH(iDoor))
ENDFUNC

FUNC BOOL DOES_DOOR_HAVE_ANIMATION_DYNOPROP(INT iDoor)
	RETURN g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iDoorAnimDynopropIndex > -1
ENDFUNC

FUNC BOOL HAS_CONTROL_OF_DOOR(INT iDoor)
	IF NOT IS_DOOR_NETWORKED(iDoor)
		RETURN TRUE
	ENDIF
	
	RETURN NETWORK_HAS_CONTROL_OF_DOOR(GET_DOOR_HASH(iDoor))
ENDFUNC

FUNC BOOL IS_DOOR_READY_FOR_DYNOPROP_SWAP(INT iDoor, BOOL bReadyForAllPlayers = TRUE)
	IF bReadyForAllPlayers
		RETURN IS_BIT_SET(iDoorAnimDynopropInitialisedForAllPlayersBS, iDoor)
	ELSE
		RETURN IS_BIT_SET(MC_playerBD_1[iLocalPart].iDoorAnimDynopropInitialisedBS, iDoor)
	ENDIF
ENDFUNC

FUNC OBJECT_INDEX GET_DOOR_ANIMATION_DYNOPROP(INT iDoor, BOOL bNeedsToBeInitialised = FALSE, BOOL bNeedControl = FALSE, BOOL bLockControl = FALSE)
	IF NOT DOES_DOOR_HAVE_ANIMATION_DYNOPROP(iDoor)
		RETURN NULL
	ENDIF
	
	IF bNeedsToBeInitialised
		IF NOT IS_DOOR_READY_FOR_DYNOPROP_SWAP(iDoor)
			PRINTLN("[Doors][Door ", iDoor, "][DoorDynoprops] GET_DOOR_ANIMATION_DYNOPROP | Waiting for door ", iDoor, "'s dynoprop to be initialised by all players")
			RETURN NULL
		ENDIF
	ENDIF
	
	OBJECT_INDEX oiDynoprop = GET_FMMC_ENTITY_DYNOPROP(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iDoorAnimDynopropIndex)
	
	IF bNeedControl
		IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(oiDynoprop)
			PRINTLN("[Doors][Door ", iDoor, "][DoorDynoprops] GET_DOOR_ANIMATION_DYNOPROP | Grabbing control of dynoprop for door ", iDoor)
			NETWORK_REQUEST_CONTROL_OF_ENTITY(oiDynoprop)
			RETURN NULL
		ELSE
			PRINTLN("[Doors_SPAM][Door_SPAM ", iDoor, "][DoorDynoprops_SPAM] GET_DOOR_ANIMATION_DYNOPROP | We have control of dynoprop for door ", iDoor)
			
			IF bLockControl
				PRINTLN("[Doors][Door ", iDoor, "][DoorDynoprops] GET_DOOR_ANIMATION_DYNOPROP | Locking migration of dynoprop for door ", iDoor)
				SET_NETWORK_ID_CAN_MIGRATE(OBJ_TO_NET(oiDynoprop), FALSE)
				SET_NETWORK_ID_CAN_BE_REASSIGNED(OBJ_TO_NET(oiDynoprop), TRUE)
			ENDIF
		ENDIF
	ENDIF
	
	RETURN oiDynoprop
ENDFUNC

FUNC VECTOR GET_COORDS_FOR_DOOR_ANIMATION_REAL_DOOR(INT iDoor, BOOL bDynopropActive = TRUE)

	IF IS_VECTOR_ZERO(vCachedDoorPositions[iDoor])
		RETURN g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].vPos
	ENDIF
	
	IF bDynopropActive
		RETURN vCachedDoorPositions[iDoor] + <<0, 0, fDoorDynopropRealDoorHeightOffset_Inactive>>
	ELSE
		RETURN vCachedDoorPositions[iDoor] + <<0, 0, fDoorDynopropRealDoorHeightOffset_Active>>
	ENDIF
ENDFUNC

FUNC VECTOR GET_COORDS_FOR_DOOR_ANIMATION_DYNOPROP(INT iDoor, BOOL bDynopropActive = TRUE)
	IF bDynopropActive
		RETURN vCachedDoorPositions[iDoor] + <<0, 0, fDoorDynopropHeightOffset_Active>>
	ELSE
		RETURN vCachedDoorPositions[iDoor] + <<0, 0, fDoorDynopropHeightOffset_Inactive>>
	ENDIF
ENDFUNC

FUNC BOOL HAS_DOOR_BEEN_SWAPPED_WITH_DYNOPROP(INT iDoor)
	RETURN IS_BIT_SET(iDoorAnimDynopropSwappedInBS, iDoor)
ENDFUNC

FUNC OBJECT_INDEX GET_DOOR_OBJECT(INT iDoor)
	IF DOES_ENTITY_EXIST(oiCachedDoorObjects[iDoor])
		RETURN oiCachedDoorObjects[iDoor]
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF bDoorDebug
		DRAW_DEBUG_SPHERE(GET_COORDS_FOR_DOOR_ANIMATION_REAL_DOOR(iDoor, HAS_DOOR_BEEN_SWAPPED_WITH_DYNOPROP(iDoor)), 0.1, 255, 255, 255)
	ENDIF
	#ENDIF
	
	OBJECT_INDEX oiFoundDoor = GET_CLOSEST_OBJECT_OF_TYPE(GET_COORDS_FOR_DOOR_ANIMATION_REAL_DOOR(iDoor, HAS_DOOR_BEEN_SWAPPED_WITH_DYNOPROP(iDoor)), 1.0, GET_DOOR_MODEL(iDoor), FALSE, FALSE, FALSE)
	
	IF DOES_ENTITY_EXIST(oiFoundDoor)
		IF oiFoundDoor != GET_DOOR_ANIMATION_DYNOPROP(iDoor)
			oiCachedDoorObjects[iDoor] = oiFoundDoor
			vCachedDoorPositions[iDoor] = GET_ENTITY_COORDS(oiCachedDoorObjects[iDoor], FALSE)
			PRINTLN("[Doors][Door ", iDoor, "] GET_DOOR_OBJECT | Caching door object! vCachedDoorPositions: ", vCachedDoorPositions[iDoor])
			RETURN oiCachedDoorObjects[iDoor]
		ELSE
			PRINTLN("[Doors][Door ", iDoor, "] GET_DOOR_OBJECT | Only found the dynoprop!")
		ENDIF
	ENDIF
	
	PRINTLN("[Doors][Door ", iDoor, "] GET_DOOR_OBJECT | Failed to find door object!")
	RETURN NULL
ENDFUNC

// PURPOSE: Swaps out the real door and dynoprop based on bMakeDynopropActive. Returns a reference to the Dynoprop to be added to sync scenes etc where required.
FUNC OBJECT_INDEX PERFORM_LOCAL_DOOR_ANIMATION_DYNOPROP_SWAP(INT iDoor, BOOL bMakeDynopropActive)
	
	IF NOT IS_DOOR_READY_FOR_DYNOPROP_SWAP(iDoor)
		PRINTLN("[RCC MISSION][Doors][Door ", iDoor, "][DoorDynoprops] PERFORM_LOCAL_DOOR_ANIMATION_DYNOPROP_SWAP | Door Dynoprop isn't initialised yet!!!")
		ASSERTLN("[RCC MISSION][Doors][Door ", iDoor, "][DoorDynoprops] PERFORM_LOCAL_DOOR_ANIMATION_DYNOPROP_SWAP | Door Dynoprop isn't initialised yet!!!")
		RETURN NULL
	ENDIF
	
	OBJECT_INDEX oiDoorDynoprop = GET_DOOR_ANIMATION_DYNOPROP(iDoor)
	OBJECT_INDEX oiDoorObject = GET_DOOR_OBJECT(iDoor)
		
	IF NOT DOES_ENTITY_EXIST(oiDoorDynoprop)
		PRINTLN("[RCC MISSION][Doors][Door ", iDoor, "][DoorDynoprops][Dynoprop ", g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iDoorAnimDynopropIndex, "] PERFORM_LOCAL_DOOR_ANIMATION_DYNOPROP_SWAP | oiDoorDynoprop doesn't exist!!!")
		ASSERTLN("[RCC MISSION][Doors][Door ", iDoor, "][DoorDynoprops][Dynoprop ", g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iDoorAnimDynopropIndex, "] PERFORM_LOCAL_DOOR_ANIMATION_DYNOPROP_SWAP | oiDoorDynoprop doesn't exist!!!")
		RETURN NULL
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(oiDoorObject)
		PRINTLN("[RCC MISSION][Doors][Door ", iDoor, "][DoorDynoprops][Dynoprop ", g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iDoorAnimDynopropIndex, "] PERFORM_LOCAL_DOOR_ANIMATION_DYNOPROP_SWAP | oiDoorObject doesn't exist!!!")
		ASSERTLN("[RCC MISSION][Doors][Door ", iDoor, "][DoorDynoprops][Dynoprop ", g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iDoorAnimDynopropIndex, "] PERFORM_LOCAL_DOOR_ANIMATION_DYNOPROP_SWAP | oiDoorObject doesn't exist!!!")
		RETURN NULL
	ENDIF
	
	IF oiDoorObject = oiDoorDynoprop
		PRINTLN("[RCC MISSION][Doors][Door ", iDoor, "][DoorDynoprops][Dynoprop ", g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iDoorAnimDynopropIndex, "] PERFORM_LOCAL_DOOR_ANIMATION_DYNOPROP_SWAP | oiDoorObject and oiDoorDynoprop are the same!!")
		ASSERTLN("[RCC MISSION][Doors][Door ", iDoor, "][DoorDynoprops][Dynoprop ", g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iDoorAnimDynopropIndex, "] PERFORM_LOCAL_DOOR_ANIMATION_DYNOPROP_SWAP | oiDoorObject and oiDoorDynoprop are the same!!")
		RETURN NULL
	ENDIF
	
	// Handle the Dynoprop
	IF bMakeDynopropActive
		PRINTLN("[RCC MISSION][Doors][Door ", iDoor, "][DoorDynoprops][Dynoprop ", g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iDoorAnimDynopropIndex, "] PERFORM_LOCAL_DOOR_ANIMATION_DYNOPROP_SWAP | Swapping in the dynoprop now!")
		SET_ENTITY_ALPHA(oiDoorDynoprop, 255, FALSE)
	ELSE
		PRINTLN("[RCC MISSION][Doors][Door ", iDoor, "][DoorDynoprops][Dynoprop ", g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iDoorAnimDynopropIndex, "] PERFORM_LOCAL_DOOR_ANIMATION_DYNOPROP_SWAP | Swapping out the dynoprop now!")
		SET_ENTITY_ALPHA(oiDoorDynoprop, 0, FALSE)
	ENDIF
		
	IF NETWORK_HAS_CONTROL_OF_ENTITY(oiDoorDynoprop)
		SET_ENTITY_COMPLETELY_DISABLE_COLLISION(oiDoorDynoprop, bMakeDynopropActive)
		
		VECTOR vDoorDynopropCoords = GET_COORDS_FOR_DOOR_ANIMATION_DYNOPROP(iDoor, bMakeDynopropActive)
		PRINTLN("[RCC MISSION][Doors][Door ", iDoor, "][DoorDynoprops] PERFORM_LOCAL_DOOR_ANIMATION_DYNOPROP_SWAP | Moving dynoprop door to ", vDoorDynopropCoords)
		SET_ENTITY_COORDS(oiDoorDynoprop, vDoorDynopropCoords, FALSE)
		
		INTERIOR_INSTANCE_INDEX iiDoorInterior = GET_INTERIOR_FROM_ENTITY(oiDoorObject)
		INT iDoorRoomKey = GET_ROOM_KEY_FROM_ENTITY(oiDoorObject)
		FORCE_ROOM_FOR_ENTITY(oiDoorDynoprop, iiDoorInterior, iDoorRoomKey)
		PRINTLN("[RCC MISSION][Doors][Door ", iDoor, "][DoorDynoprops] PERFORM_LOCAL_DOOR_ANIMATION_DYNOPROP_SWAP | Putting Dynoprop ", g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iDoorAnimDynopropIndex, " into interior ", NATIVE_TO_INT(iiDoorInterior), " / Room: ", iDoorRoomKey)
		
		IF NOT bMakeDynopropActive
			PRINTLN("[RCC MISSION][Doors][Door ", iDoor, "][DoorDynoprops] PERFORM_LOCAL_DOOR_ANIMATION_DYNOPROP_SWAP | Releasing network migration for dynoprop!")
			SET_NETWORK_ID_CAN_MIGRATE(OBJ_TO_NET(oiDoorDynoprop), TRUE)
		ENDIF
	ENDIF
	
	// Handle the real Door
	IF bMakeDynopropActive
		PRINTLN("[RCC MISSION][Doors][Door ", iDoor, "][DoorDynoprops][Dynoprop ", g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iDoorAnimDynopropIndex, "] PERFORM_LOCAL_DOOR_ANIMATION_DYNOPROP_SWAP | Swapping out the real door now!")
		SET_ENTITY_ALPHA(oiDoorObject, 0, FALSE)
	ELSE
		PRINTLN("[RCC MISSION][Doors][Door ", iDoor, "][DoorDynoprops][Dynoprop ", g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iDoorAnimDynopropIndex, "] PERFORM_LOCAL_DOOR_ANIMATION_DYNOPROP_SWAP | Swapping in the real door now!")
		SET_ENTITY_ALPHA(oiDoorObject, 255, FALSE)
	ENDIF
		
	IF HAS_CONTROL_OF_DOOR(iDoor)
		SET_ENTITY_COMPLETELY_DISABLE_COLLISION(oiDoorObject, NOT bMakeDynopropActive)
		DOOR_SYSTEM_SET_OPEN_RATIO(GET_DOOR_HASH(iDoor), 0.0, IS_DOOR_NETWORKED(iDoor), TRUE)
		PRINTLN("[RCC MISSION][Doors][Door ", iDoor, "][DoorDynoprops][Dynoprop ", g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iDoorAnimDynopropIndex, "] PERFORM_LOCAL_DOOR_ANIMATION_DYNOPROP_SWAP | Moving real door to ", vCachedDoorPositions[iDoor])
	ENDIF
	
	IF bMakeDynopropActive
		SET_BIT(iDoorAnimDynopropSwappedInBS, iDoor)
	ELSE
		CLEAR_BIT(iDoorAnimDynopropSwappedInBS, iDoor)
	ENDIF
	
	RETURN oiDoorDynoprop
ENDFUNC

FUNC OBJECT_INDEX PERFORM_NETWORKED_DOOR_ANIMATION_DYNOPROP_SWAP(INT iDoor, BOOL bMakeDynopropActive)
	IF bMakeDynopropActive
		IF NOT HAS_DOOR_BEEN_SWAPPED_WITH_DYNOPROP(iDoor)
			BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_SwapDoorWithDynoprop, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, bMakeDynopropActive, iDoor)
			SET_BIT(iDoorAnimDynopropSwappedInBS, iDoor)
		ELSE
			PRINTLN("[RCC MISSION][Doors][Door ", iDoor, "][DoorDynoprops] PERFORM_NETWORKED_DOOR_ANIMATION_DYNOPROP_SWAP | Door has already become a dynoprop!")
		ENDIF
	ELSE
		IF HAS_DOOR_BEEN_SWAPPED_WITH_DYNOPROP(iDoor)
			BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_SwapDoorWithDynoprop, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, bMakeDynopropActive, iDoor)
			CLEAR_BIT(iDoorAnimDynopropSwappedInBS, iDoor)
		ELSE
			PRINTLN("[RCC MISSION][Doors][Door ", iDoor, "][DoorDynoprops] PERFORM_NETWORKED_DOOR_ANIMATION_DYNOPROP_SWAP | Door has already turned back from being a dynoprop!")
		ENDIF
	ENDIF
	
	RETURN GET_DOOR_ANIMATION_DYNOPROP(iDoor)
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Entities - Props
// ##### Description: Various helper functions & wrappers to do with mission entities
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL IS_PROP_GLASS_CUT_MINIGAME_LOOT(MODEL_NAMES mnPropModel)
	IF mnPropModel = INT_TO_ENUM(MODEL_NAMES, HASH("h4_Prop_h4_T_Bottle_02a"))
	OR mnPropModel = INT_TO_ENUM(MODEL_NAMES, HASH("h4_Prop_h4_Necklace_01a"))
	OR mnPropModel = INT_TO_ENUM(MODEL_NAMES, HASH("h4_Prop_h4_Art_Pant_01a"))
	OR mnPropModel = INT_TO_ENUM(MODEL_NAMES, HASH("h4_Prop_h4_Diamond_01a"))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC HIDE_PROP(INT iProp, BOOL bNetworked = TRUE)
	IF bNetworked
		PRINTLN("[Props][Prop ", iProp, "] HIDE_PROP | Broadcasting the hide prop event for prop ", iProp)
		BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_HideProp, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, FALSE, iProp, DEFAULT)
	ENDIF
	
	IF DOES_ENTITY_EXIST(oiProps[iProp])
		PRINTLN("[Props][Prop ", iProp, "] HIDE_PROP | Hiding prop ", iProp, " now!")
		SET_ENTITY_VISIBLE(oiProps[iProp], FALSE)
		SET_ENTITY_COMPLETELY_DISABLE_COLLISION(oiProps[iProp], FALSE)
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Entities - Dyno Props
// ##### Description: Various helper functions & wrappers to do with mission entities
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC INT GET_DYNOPROP_SPAWN_GROUP_MODEL_SWAP_STRING_BITSET(INT iDynoprop)
	INT iModelSwapExistsBitset = 0
	INT iSpawnGroupIterator
	
	FOR iSpawnGroupIterator = 0 TO FMMC_MAX_RANDOM_ENTITY_SPAWN_GROUPS - 1
		IF IS_CUSTOM_STRING_LIST_STRING_VALID(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoprop].iDynopropSpawnGroupModelSwapStringIndexes[iSpawnGroupIterator])
			//PRINTLN("[Dynoprops][Dynoprop ", iDynoprop, "] GET_DYNOPROP_SPAWN_GROUP_MODEL_SWAP_STRING_BITSET | Spawngroup ", iSpawnGroupIterator, " has a model-swap available")
			SET_BIT(iModelSwapExistsBitset, iSpawnGroupIterator)
		ELSE
			//PRINTLN("[Dynoprops][Dynoprop ", iDynoprop, "] GET_DYNOPROP_SPAWN_GROUP_MODEL_SWAP_STRING_BITSET | Spawngroup ", iSpawnGroupIterator, " isn't relevant here")
		ENDIF
	ENDFOR

	RETURN iModelSwapExistsBitset
ENDFUNC

FUNC MODEL_NAMES GET_DYNOPROP_MODEL_TO_USE(INT iDynoprop)
	
	IF IS_MODEL_VALID(mnCachedDynopropModelNames[iDynoprop])
		RETURN mnCachedDynopropModelNames[iDynoprop]
	ENDIF
	
	MODEL_NAMES mnModelToUse = g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoprop].mn
	
	// Spawn Group Model-Swap
	INT iSpawnGroupModelSwapBS = GET_DYNOPROP_SPAWN_GROUP_MODEL_SWAP_STRING_BITSET(iDynoprop)
	INT iSpawnGroupToUse = GET_FIRST_MATCH_SPAWN_GROUP_FROM_SERVER(iSpawnGroupModelSwapBS)
	PRINTLN("[Dynoprops][Dynoprop ", iDynoprop, "] GET_DYNOPROP_MODEL_TO_USE | iSpawnGroupModelSwapBS: ", iSpawnGroupModelSwapBS, " // iSpawnGroupToUse: ", iSpawnGroupToUse, " // MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeedBS: ", MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeedBS)
	
	IF iSpawnGroupToUse > -1
		INT iSpawnGroupModelSwapStringIndex = g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoprop].iDynopropSpawnGroupModelSwapStringIndexes[iSpawnGroupToUse]
		
		IF IS_CUSTOM_STRING_LIST_STRING_VALID(iSpawnGroupModelSwapStringIndex)
			MODEL_NAMES mnSpawnGroupModelSwapModel = GET_CUSTOM_STRING_LIST_MODEL_NAME(iSpawnGroupModelSwapStringIndex)
			
			IF IS_MODEL_VALID(mnSpawnGroupModelSwapModel)
				mnModelToUse = mnSpawnGroupModelSwapModel
				PRINTLN("[Dynoprops][Dynoprop ", iDynoprop, "] GET_DYNOPROP_MODEL_TO_USE | Swapping model from ", FMMC_GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoprop].mn), " to ", FMMC_GET_MODEL_NAME_FOR_DEBUG(mnModelToUse))
			ENDIF
		ENDIF
	ENDIF
	
	mnCachedDynopropModelNames[iDynoprop] = mnModelToUse
	RETURN mnModelToUse
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Entities - Weapons / Pickups
// ##### Description: Various helper functions & wrappers to do with mission entities
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC INT GET_MONEY_VALUE_OF_CASH_PICKUP_MODEL(MODEL_NAMES mnCashModel)
	IF mnCashModel = INT_TO_ENUM(MODEL_NAMES, HASH("h4_Prop_h4_Cash_Stack_02a"))
		#IF FEATURE_HEIST_ISLAND
		// Temporary method - potentially replace with an Alt Vars method
		RETURN GET_RANDOM_INT_IN_RANGE(g_sMPTunables.iH4LOOT_MIN_SAFE_CASH_VALUE, g_sMPTunables.iH4LOOT_MAX_SAFE_CASH_VALUE+1)
		#ENDIF
	ENDIF
	
	RETURN 5000
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Entities - Zones
// ##### Description: Various helper functions & wrappers to do with mission entities
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC INT GET_NUM_PLAYERS_TRIGGERING_ZONE(INT iZone)
	RETURN iZoneTriggeringPlayerCount[iZone]
ENDFUNC
FUNC BOOL ARE_ALL_PLAYERS_TRIGGERING_ZONE(INT iZone)
	IF GET_TOTAL_PLAYING_PLAYERS() > 0
		RETURN GET_NUM_PLAYERS_TRIGGERING_ZONE(iZone) >= GET_TOTAL_PLAYING_PLAYERS()
	ENDIF
	
	RETURN FALSE
ENDFUNC
FUNC INT GET_NUM_PLAYERS_IN_TEAM_TRIGGERING_ZONE(INT iZone, INT iTeam)
	RETURN iZoneTriggeringPlayerCountByTeam[iZone][iTeam]
ENDFUNC

FUNC INT GET_NUM_ACTIVE_ZONES_OF_THIS_TYPE(INT iZoneType = ciFMMC_ZONE_TYPE__NO_PLAYER_SPAWNING)
	RETURN iCurrentNumActiveZones[iZoneType]
ENDFUNC
FUNC BOOL DOES_A_ZONE_OF_THIS_TYPE_EXIST(INT iZoneType = ciFMMC_ZONE_TYPE__NO_PLAYER_SPAWNING)
	RETURN GET_NUM_ACTIVE_ZONES_OF_THIS_TYPE(iZoneType) > 0
ENDFUNC

FUNC INT GET_TRIGGERING_ZONE_INDEX(INT iZoneType = ciFMMC_ZONE_TYPE__NO_PLAYER_SPAWNING)
	RETURN iCurrentTriggeringZone[iZoneType]
ENDFUNC
FUNC BOOL IS_ZONE_TYPE_TRIGGERING(INT iZoneType = ciFMMC_ZONE_TYPE__NO_PLAYER_SPAWNING)
	RETURN GET_TRIGGERING_ZONE_INDEX(iZoneType) > -1
ENDFUNC

FUNC BOOL DOES_ZONE_USE_A_CREATION_DELAY_TIMER(INT iZone)
	RETURN g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneDelay != 0
ENDFUNC

FUNC BOOL DOES_ZONE_USE_A_ZONE_TIMER(INT iZone)
	RETURN g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneTimer_EnableTime > 0
ENDFUNC

FUNC TEXT_LABEL_63 GET_ZONE_STRING_LIST_STRING(INT iZone)
	IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneCustomStringListSelection = -1
		TEXT_LABEL_63 tlEmpty = ""
		RETURN tlEmpty
	ENDIF
	
	RETURN g_FMMC_STRUCT.tlCustomStringList[g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneCustomStringListSelection]
ENDFUNC

FUNC BOOL DOES_ZONE_EXIST(INT iZone)
	IF IS_BIT_SET(iLocalZoneRemovedBitset, iZone)
		RETURN FALSE
	ENDIF
	
	RETURN IS_BIT_SET(iZoneCreatedBS, iZone)
ENDFUNC

FUNC BOOL IS_ZONE_TRIGGER_BLOCKED(INT iZone)

	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneBS2, ciFMMC_ZONEBS2_REQUIRE_ALL_PLAYERS)
		IF NOT ARE_ALL_PLAYERS_TRIGGERING_ZONE(iZone)
			PRINTLN("[Zones_SPAM][Zone_SPAM ", iZone, "] IS_ZONE_TRIGGER_BLOCKED | Blocked due to ARE_ALL_PLAYERS_TRIGGERING_ZONE")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ZONE_TRIGGERING(INT iZone, BOOL bIncludeRemotePlayers = FALSE)
	IF IS_BIT_SET(iZoneIsTriggeredBS, iZone)
	OR (bIncludeRemotePlayers AND IS_BIT_SET(iCachedZoneIsTriggeredNetworkedBS, iZone))
		IF IS_ZONE_TRIGGER_BLOCKED(iZone)
			PRINTLN("[Zones][Zone ", iZone, "] IS_ZONE_TRIGGERING | Returning FALSE due to IS_ZONE_TRIGGER_BLOCKED! (See Above)")
			RETURN FALSE
		ELSE
			// We've got it set as triggered and that trigger isn't being blocked by anything - return TRUE!
			RETURN TRUE
		ENDIF
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

FUNC INT GET_ZONE_TIMER_ELAPSED_TIME(INT iZone)

	IF NOT HAS_NET_TIMER_STARTED(stZoneTimers[iZone])
		RETURN 0
	ENDIF
	
	RETURN GET_ABS_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stZoneTimers[iZone])
ENDFUNC

FUNC BOOL IS_ZONE_SAFE_TO_BLIP(INT iZone)
	RETURN IS_BIT_SET(iZoneBS_SafeToBlip, iZone)
ENDFUNC

PROC SET_ZONE_AS_SAFE_TO_BLIP(INT iZone)
	IF NOT IS_BIT_SET(iZoneBS_SafeToBlip, iZone)
		SET_BIT(iZoneBS_SafeToBlip, iZone)
		PRINTLN("[Zones][Zone ", iZone, "] SET_ZONE_AS_SAFE_TO_BLIP | Setting Zone as safe to blip!")
	ENDIF
ENDPROC

PROC SET_ZONE_AS_NOT_SAFE_TO_BLIP(INT iZone)
	IF IS_BIT_SET(iZoneBS_SafeToBlip, iZone)
		CLEAR_BIT(iZoneBS_SafeToBlip, iZone)
		PRINTLN("[Zones][Zone ", iZone, "] SET_ZONE_AS_SAFE_TO_BLIP | Setting Zone as not safe to blip!")
	ENDIF
ENDPROC

FUNC INT GET_ZONE_TIMER_TIME_REMAINING(INT iZone, BOOL bInSeconds = FALSE)

	INT iTimeRemaining = (g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneTimer_EnableTime - GET_ZONE_TIMER_ELAPSED_TIME(iZone))
	
	IF bInSeconds
		iTimeRemaining /= 1000
	ENDIF
	
	RETURN iTimeRemaining
ENDFUNC

FUNC BOOL HAS_ZONE_TIMER_COMPLETED(INT iZone)
	
	IF IS_BIT_SET(iZoneTimersCompletedBS, iZone)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_ZONE_TIMER_EXPIRED(INT iZone, INT iExpiryTime = -1)
	
	IF iExpiryTime = -1
		iExpiryTime = g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneTimer_EnableTime
	ENDIF
	
	RETURN GET_ZONE_TIMER_ELAPSED_TIME(iZone) >= iExpiryTime
ENDFUNC

FUNC BOOL IS_ZONE_TIMER_RUNNING(INT iZone)

	IF HAS_ZONE_TIMER_COMPLETED(iZone)
		RETURN FALSE
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(stZoneTimers[iZone])
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_ENTITY_SPAWN_ZONE_REQUIREMENT_BEEN_MET(INT iZone, INT iPlayerReq)
	IF iZone < 0
		RETURN FALSE
	ENDIF
	
	IF iPlayerReq = 0 // 0 means all players must be in the Zone
		IF ARE_ALL_PLAYERS_TRIGGERING_ZONE(iZone)
			RETURN TRUE
		ENDIF
	ELSE
		IF GET_NUM_PLAYERS_TRIGGERING_ZONE(iZone) >= iPlayerReq
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_ZONE_SHAPE(INT iZone)
	RETURN g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iAreaType
ENDFUNC

PROC REFRESH_ZONES_FOR_TEAM(INT iTeam)
	iRefreshZonesForThisTeam = iTeam
	PRINTLN("[RCC MISSION] REFRESH_ZONES - Setting iRefreshZonesForThisTeam to ", iRefreshZonesForThisTeam)
ENDPROC

FUNC ENTITY_INDEX GET_ZONE_LINKED_ENTITY(INT iZone)
	RETURN GET_FMMC_ENTITY(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iEntityLinkType, g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iEntityLinkIndex, g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iEntityLink_TrainCarriage)
ENDFUNC

// PURPOSE: Gets the given Zone's vPos coords in local space (the coords that vPos[iPositionIndex] would be at if the centre of the Zone was <<0,0,0>> rather than wherever it actually is)
FUNC VECTOR GET_ZONE_LOCAL_POSITION(INT iZone, INT iPositionIndex)
	UNUSED_PARAMETER(iPositionIndex)
	SWITCH GET_ZONE_SHAPE(iZone)
		CASE ciFMMC_ZONE_SHAPE__SPHERE
		CASE ciFMMC_ZONE_SHAPE__CYLINDER
			RETURN <<0,0,0>>
		BREAK
		
		CASE ciFMMC_ZONE_SHAPE__ANGLED_AREA
		CASE ciFMMC_ZONE_SHAPE__AXIS_ALIGNED_BOX
			RETURN g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[iPositionIndex] - GET_FMMC_ZONE_PLACED_CENTRE(iZone)
		BREAK
	ENDSWITCH
	
	RETURN <<0,0,0>>
ENDFUNC

FUNC FLOAT GET_PLACED_ZONE_RADIUS(INT iZone)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iRangePercentPerPoint > 0
		
		INT iTeam = MC_playerBD[iPartToUse].iTeam
		//Get the range to reduce the radius by from the given percentage reduction
		FLOAT fRangePerPointToReduce = ((g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].fRadius * 0.01) * g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iRangePercentPerPoint)
		
		//Calculate how much to reduce the range by based on team points on the rule
		FLOAT fRangeToReduce = MC_serverBD.iScoreOnThisRule[iTeam] * fRangePerPointToReduce
		
		RETURN CLAMP((g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].fRadius - fRangeToReduce), 5.0, 10000.0)
		
	ENDIF

	RETURN g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].fRadius

ENDFUNC

FUNC FLOAT GET_ZONE_RUNTIME_RADIUS(INT iZone)
	RETURN GET_PLACED_ZONE_RADIUS(iZone) * fZoneSizeMultiplier[iZone]
ENDFUNC

FUNC FLOAT GET_ZONE_RUNTIME_WIDTH(INT iZone)
	RETURN GET_ZONE_RUNTIME_RADIUS(iZone)
ENDFUNC

FUNC VECTOR GET_LINKED_ENTITY_ZONE_VEHICLE_REPAIR_RUNTIME_POSITION(INT iZone, INT iPositionIndex, ENTITY_INDEX eiLinkedEntity, FLOAT& fHeight, BOOL& bGroundZone)
	// The Vehicle Repair Zone is specifically overridden to go alongside the attached dynoprop
	FLOAT fExtraDistanceFromDynoprop = -1.0
	fHeight = cfRacePitZoneAttachedHeight
	VECTOR vCentreOfPlacedZone = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(eiLinkedEntity, <<GET_ZONE_RUNTIME_WIDTH(iZone) + fExtraDistanceFromDynoprop, 0.0, 0.0>>)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iAreaType = ciFMMC_ZONE_SHAPE__ANGLED_AREA
		FLOAT fForwardVectorSize = 4.0
		IF iPositionIndex = 0
			bGroundZone = TRUE
			RETURN vCentreOfPlacedZone + GET_ENTITY_FORWARD_VECTOR(eiLinkedEntity) * fForwardVectorSize
			
		ELIF iPositionIndex = 1
			bGroundZone = TRUE
			RETURN vCentreOfPlacedZone - GET_ENTITY_FORWARD_VECTOR(eiLinkedEntity) * fForwardVectorSize
			
		ENDIF
	ENDIF
	
	RETURN <<0,0,0>>
ENDFUNC

PROC SET_NEW_ZONE_OFFSET_REQUIRED(INT iZone)
	vZoneRandomOffset[iZone] = <<0,0,0>>
ENDPROC

FUNC VECTOR GET_LINKED_ENTITY_ZONE_CENTRE(INT iZone, INT iPositionIndex, VECTOR vBaseCentre, VECTOR& vDirectlySetPosition, FLOAT& fHeight, BOOL& bGroundZone)
	
	ENTITY_INDEX eiLinkedEntity = GET_ZONE_LINKED_ENTITY(iZone)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iSmoothZoneAttachmentOffsetSlot > -1
		// If this Zone uses smooth attachment, return its interpolated position rather than positioning directly on the entity
		RETURN vSmoothAttachedZonePos[g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iSmoothZoneAttachmentOffsetSlot]
	ENDIF
	
	IF DOES_ENTITY_EXIST(eiLinkedEntity)
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneBS2, ciFMMC_ZONEBS2_ATTACH_TO_LINKED_ENTITY_WITH_OFFSET)
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneBS2, ciFMMC_ZONEBS2_USE_RANDOM_OFFSET)
				IF IS_VECTOR_ZERO(vZoneRandomOffset[iZone])
					FLOAT fRandomXRange = GET_RANDOM_FLOAT_IN_RANGE(-g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].fRandomOffsetRange, g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].fRandomOffsetRange)
					FLOAT fRandomYRange = GET_RANDOM_FLOAT_IN_RANGE(-g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].fRandomOffsetRange, g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].fRandomOffsetRange)
					VECTOR vEntityPos = GET_ENTITY_COORDS(eiLinkedEntity, FALSE)
					vZoneRandomOffset[iZone] = << fRandomXRange, fRandomYRange, vEntityPos.z >>
				ENDIF
				vBaseCentre = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(eiLinkedEntity, vZoneRandomOffset[iZone])
			ELSE
				vBaseCentre = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(eiLinkedEntity, g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vZoneAttachmentOffset)
			ENDIF
		ELSE
			vBaseCentre = GET_ENTITY_COORDS(eiLinkedEntity, FALSE)
		ENDIF
		
		IF g_FMMC_STRUCT.sPlayerAbilities.sVehicleRepair.iRepairZoneIndex = iZone
			vDirectlySetPosition = GET_LINKED_ENTITY_ZONE_VEHICLE_REPAIR_RUNTIME_POSITION(iZone, iPositionIndex, eiLinkedEntity, fHeight, bGroundZone)
		ENDIF
	ENDIF
	
	RETURN vBaseCentre
ENDFUNC

FUNC VECTOR GET_ZONE_RUNTIME_POSITION(INT iZone, INT iPositionIndex)

	IF NOT IS_VECTOR_ZERO(vCachedZonePosition[iZone][iPositionIndex])
		RETURN vCachedZonePosition[iZone][iPositionIndex]
	ENDIF
	
	VECTOR vResult = <<0,0,0>>
	VECTOR vDirectlySetPosition = <<0,0,0>>
	VECTOR vCentreOfPlacedZone = g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[iPositionIndex] - GET_ZONE_LOCAL_POSITION(iZone, iPositionIndex)
	BOOL bGroundZone = FALSE
	FLOAT fHeight = ABSF(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[1].z - g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[0].z)

	// If this Zone is attached to an entity, the centre should be that entity's coords instead
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneBS, ciFMMC_ZONEBS_ATTACH_ZONE_TO_LINKED_ENTITY)
		vCentreOfPlacedZone = GET_LINKED_ENTITY_ZONE_CENTRE(iZone, iPositionIndex, vCentreOfPlacedZone, vDirectlySetPosition, fHeight, bGroundZone)
	ENDIF
	
	IF IS_VECTOR_ZERO(vDirectlySetPosition)
		// If it isn't overridden, the final coords for this position should be the centre + the vector going from the centre to where vPos[iPositionIndex] was, multiplied by the size multiplier.
		// Most of the time (when fZoneSizeMultiplier is untouched) this will end up returning the same vPos[iPositionIndex] that was placed in the Creator in the first place
		vResult = vCentreOfPlacedZone + (GET_ZONE_LOCAL_POSITION(iZone, iPositionIndex) * fZoneSizeMultiplier[iZone])
	ELSE
		// We've calculated the position we want already and stored it in vDirectlySetPosition, no need to use any Creator positions
		vResult = vDirectlySetPosition
	ENDIF
	
	IF bGroundZone
		FLOAT fGroundZ = 0.0
		IF GET_GROUND_Z_EXCLUDING_OBJECTS_FOR_3D_COORD(vResult + <<0,0,5>>, fGroundZ, TRUE)
			// Successfully grounded the Zone. Adding a offset downwards here to ensure the bottom of the Zone is consistently underground to prevent slight clips
			FLOAT fZOffset = -cfGroundedZoneUndergroundOffset
			vResult.z = fGroundZ + fZOffset
			
			IF iPositionIndex = 1
				vResult.z += fHeight
			ENDIF
		ELSE
			PRINTLN("[Zones][Zone ", iZone, "] GET_ZONE_RUNTIME_POSITION | Failed to ground Zone's position ", iPositionIndex)
		ENDIF
	ENDIF
	
	vCachedZonePosition[iZone][iPositionIndex] = vResult
	
	RETURN vResult
ENDFUNC

FUNC VECTOR GET_ZONE_RUNTIME_CENTRE(INT iZone)
	VECTOR vRuntimeCoords[ciFMMC_ZONE_MAX_POSITIONS]
	vRuntimeCoords[0] = GET_ZONE_RUNTIME_POSITION(iZone, 0)
	vRuntimeCoords[1] = GET_ZONE_RUNTIME_POSITION(iZone, 1)
	
	RETURN GET_FMMC_ZONE_COORDS_CENTRE(vRuntimeCoords, DOES_ZONE_USE_TWO_POSITIONS(iZone))
ENDFUNC

FUNC BOOL SHOULD_ZONE_USE_Z_CHECK(INT iZone)
	RETURN IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneBS, ciFMMC_ZONEBS_IncludeZAxisInAreaCheck)
ENDFUNC

FUNC BOOL ARE_COORDS_IN_FMMC_ZONE(VECTOR vLoc, INT iZone)
	
	VECTOR vPos1 = GET_ZONE_RUNTIME_POSITION(iZone, 0)
	VECTOR vPos2 = GET_ZONE_RUNTIME_POSITION(iZone, 1)
	VECTOR vCentre = GET_ZONE_RUNTIME_CENTRE(iZone)
	FLOAT fRadius = GET_ZONE_RUNTIME_RADIUS(iZone)
	
	IF NOT SHOULD_ZONE_USE_Z_CHECK(iZone)
		vLoc.z = vCentre.z
	ENDIF
	
	SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iAreaType
		CASE ciFMMC_ZONE_SHAPE__AXIS_ALIGNED_BOX
			IF IS_POSITION_IN_AREA(vLoc, vPos1, vPos2)
			AND IS_POINT_WITHIN_Z_RANGE(vLoc, vPos1.z, vPos2.z)
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE ciFMMC_ZONE_SHAPE__SPHERE
			IF VDIST2(vLoc, vPos1) < POW(fRadius, 2)
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE ciFMMC_ZONE_SHAPE__CYLINDER
			IF (vLoc.z > vPos1.z AND vLoc.z < vPos2.z)
			OR (vLoc.z < vPos1.z AND vLoc.z > vPos2.z)
			
				vLoc.z = vPos1.z
				
				IF VDIST2(vLoc, vPos1) < POW(fRadius, 2)
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
		
		CASE ciFMMC_ZONE_SHAPE__ANGLED_AREA
			IF IS_POINT_IN_ANGLED_AREA(vLoc, vPos1, vPos2, fRadius, FALSE, TRUE)
			AND IS_POINT_WITHIN_Z_RANGE(vLoc, vPos1.z, vPos2.z)
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_THIS_ZONE_TYPE_ALWAYS_TRIGGERED_WHEN_TRIGGERABLE(INT iType)
	SWITCH iType
		CASE ciFMMC_ZONE_TYPE__RETURN_OBJECT
		CASE ciFMMC_ZONE_TYPE__HIDE_OBJECTS_SEARCH_AREA
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_THIS_ZONE_TRIGGERED_WHEN_LOCAL_PLAYER_IS_INSIDE(INT iZone)
	
	IF IS_THIS_ZONE_TYPE_ALWAYS_TRIGGERED_WHEN_TRIGGERABLE(GET_FMMC_ZONE_TYPE(iZone))
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneBS2, ciFMMC_ZONEBS2_TRIGGER_ZONE_WHEN_OUTSIDE_OF_IT)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_THIS_ZONE_TRIGGERED_WHEN_LOCAL_PLAYER_IS_OUTSIDE(INT iZone)
	
	IF IS_THIS_ZONE_TYPE_ALWAYS_TRIGGERED_WHEN_TRIGGERABLE(GET_FMMC_ZONE_TYPE(iZone))
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneBS2, ciFMMC_ZONEBS2_TRIGGER_ZONE_WHEN_OUTSIDE_OF_IT)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_THIS_ZONE_REQUIRE_PLAYER_TO_BE_ALIVE_TO_TRIGGER(INT iZone)
	IF IS_THIS_ZONE_TRIGGERED_WHEN_LOCAL_PLAYER_IS_INSIDE(iZone)
	OR IS_THIS_ZONE_TRIGGERED_WHEN_LOCAL_PLAYER_IS_OUTSIDE(iZone)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ENTITY_IN_FMMC_ZONE(ENTITY_INDEX eiEntity, INT iZone, BOOL bOrActivatedRemotely = FALSE, BOOL bEntityNeedsToBeAlive = FALSE)

	IF bOrActivatedRemotely
	AND IS_BIT_SET(iZoneBS_TriggeredByLinkedZone, iZone)
		RETURN TRUE
	ENDIF
	
	IF bEntityNeedsToBeAlive
		IF NOT IS_ENTITY_ALIVE(eiEntity)
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN ARE_COORDS_IN_FMMC_ZONE(GET_ENTITY_COORDS(eiEntity, FALSE), iZone)
ENDFUNC

PROC RENDER_SPAWN_PROTECTION_SPHERES_EARLY()
	
	IF MC_playerBD[iLocalPart].iGameState = GAME_STATE_MISSION_OVER
	OR MC_playerBD[iLocalPart].iGameState = GAME_STATE_LEAVE
	OR MC_playerBD[iLocalPart].iGameState = GAME_STATE_END
	OR GET_SKYSWOOP_STAGE() != SKYSWOOP_NONE
	OR IS_BIT_SET(MC_PlayerBD[iLocalPart].iClientBitset, PBBOOL_ON_LEADERBOARD)
	OR IS_BIT_SET(MC_PlayerBD[iLocalPart].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
	OR IS_PLAYER_IN_CORONA()
		EXIT
	ENDIF
	
	INT iTeam = MC_PlayerBD[iLocalPart].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	IF iRule < FMMC_MAX_RULES
		INT iZoneTeam
		INT iZone
		FOR iZone = 0 TO FMMC_MAX_NUM_ZONES - 1
			IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iType = ciFMMC_ZONE_TYPE__SPAWN_PROTECTION		
			AND g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneDelay <= 0
				
				ASSERTLN("FMMC2020 - RENDER_SPAWN_PROTECTION_SPHERES_EARLY | NEEDS REFACTOR, PREFERABLY SHOULDN'T EXIST")
				iZoneTeam = ROUND(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].fZoneValue)
				
				PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] RENDER_SPAWN_PROTECTION_SPHERES_EARLY - ciFMMC_ZONE_TYPE__SPAWN_PROTECTION: ",iZone)
				PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] RENDER_SPAWN_PROTECTION_SPHERES_EARLY - Checking iTeam: ", iZoneTeam)
				
				IF iZoneTeam > -1
				AND iZoneTeam < FMMC_MAX_TEAMS				
					// If always on or on for the first rule
					IF (g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneStartPriority[iZoneTeam] = -1 OR g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneStartPriority[iZoneTeam] = 0)
						iZoneTeam = ROUND(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].fZoneValue)
						
						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneBS, ciFMMC_ZONEBS_ONLY_SHOW_WITH_PICKUP)
							IF MC_playerBD[iPartToUse].iObjCarryCount = 0
								PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] RENDER_SPAWN_PROTECTION_SPHERES_EARLY - Exiting because of no pickups")
								EXIT
							ENDIF
						ENDIF
						
						IF iZoneTeam > -1
						AND iZoneTeam < FMMC_MAX_TEAMS
							IF MC_serverBD.iNumStartingPlayers[iZoneTeam] > 0
								IF ARE_COORDS_IN_FMMC_ZONE(GET_ENTITY_COORDS(LocalPlayerPed), iZone)
								OR IS_BIT_SET(iZoneBS_TriggeredByLinkedZone, iZone)	
									DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)	
									DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_UD)
									DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_LR)
									DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_NEXT)
									DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_PREV)
									DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_NEXT_WEAPON)
									DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_PREV_WEAPON)
									
									DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK, TRUE)
									DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM, TRUE)
									
									DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MELEE_LEFT)
									
									IF bLocalPlayerPedOK
										SET_PED_RESET_FLAG(LocalPlayerPed, PRF_BlockWeaponFire, TRUE)
									ENDIF
									
									SET_PED_CONFIG_FLAG(localPlayerPed, PCF_DontActivateRagdollFromExplosions, TRUE)
									SET_PED_CONFIG_FLAG(localPlayerPed, PCF_DontActivateRagdollFromFire, TRUE)
									SET_PED_CONFIG_FLAG(localPlayerPed, PCF_DontActivateRagdollFromElectrocution, TRUE)
									
									WEAPON_TYPE wt = WEAPONTYPE_INVALID
									GET_CURRENT_PED_WEAPON(LocalPlayerPed, wt)
								
									IF wt != WEAPONTYPE_UNARMED
										DISABLE_PLAYER_THROW_GRENADE_WHILE_USING_GUN()
										DISABLE_PLAYER_FIRING(localPlayer, TRUE)										
									ENDIF
									
									IF wt = WEAPONTYPE_GRENADE
										SET_CURRENT_PED_WEAPON(LocalPlayerPed, WEAPONTYPE_UNARMED, TRUE)
									ENDIF
									
									DISABLE_VEHICLE_MINES(TRUE)		
								ENDIF		
								
								PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] RENDER_SPAWN_PROTECTION_SPHERES_EARLY - There is a playing on this team.")
								INT iR = 0, iG = 0, iB = 0, iA = 0
								HUD_COLOURS tempColour
								tempColour = GET_HUD_COLOUR_FOR_FMMC_TEAM(ROUND(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].fZoneValue), LocalPlayer)
								IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneBS, ciFMMC_ZONEBS_APPLY_TO_ALL_TEAMS)
									tempColour = HUD_COLOUR_RED
								ENDIF
								GET_RGB_FROM_INT(GET_INT_FROM_HUD_COLOUR(tempColour), iR, iG, iB, iA)
								iA = 100
								IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iAreaType = ciFMMC_ZONE_SHAPE__SPHERE
									DRAW_MARKER(MARKER_SPHERE, g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[0], <<0,0,0>>, <<0,0,0>>, <<GET_PLACED_ZONE_RADIUS(iZone),GET_PLACED_ZONE_RADIUS(iZone),GET_PLACED_ZONE_RADIUS(iZone)>>, iR, iG, iB, iA)									
									PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] RENDER_SPAWN_PROTECTION_SPHERES_EARLY - Drawing Sphere")
								ELSE
									VECTOR vMidPoint = (g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[0] + g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[1])
									vMidPoint = <<vMidPoint.x / 2, vMidPoint.y / 2, vMidPoint.z / 2>>
									FLOAT fScale = VDIST2(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[0], g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[1])
									fScale = PICK_FLOAT(fScale > POW(GET_PLACED_ZONE_RADIUS(iZone), 2), SQRT(fScale), GET_PLACED_ZONE_RADIUS(iZone))
									
									DRAW_ANGLED_AREA_FROM_FACES(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[0], g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[1], GET_PLACED_ZONE_RADIUS(iZone), iR, iG, iB, iA)
									PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] RENDER_SPAWN_PROTECTION_SPHERES_EARLY - Drawing Angled Area")
								ENDIF
							ELSE
								PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] RENDER_SPAWN_PROTECTION_SPHERES_EARLY - MC_serverBD.iNumStartingPlayers[iZoneTeam]: ", MC_serverBD.iNumStartingPlayers[iZoneTeam])
							ENDIF
						ELSE
							PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] RENDER_SPAWN_PROTECTION_SPHERES_EARLY - There is a playing on this team.")
						ENDIF
					ENDIF
				ELSE
					PRINTLN("[RCC MISSION][Zones][Zone ", iZone, "] RENDER_SPAWN_PROTECTION_SPHERES_EARLY- iTeam: ", iZoneTeam, " is not active, therefore we will not create the Zone.")
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	
ENDPROC

FUNC BOOL USING_SPAWN_AREA_ZONE()
	RETURN iCurrentSpawnAreaZone > -1
ENDFUNC

FUNC BOOL USING_SPAWN_AREA_ZONE_RESPAWN_HEADING()
	RETURN IS_BIT_SET(iLocalBoolCheck31, LBOOL31_USING_SPAWN_AREA_ZONE_FOR_RESPAWN_HEADING)
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Entities - Bounds
// ##### Description: Various helper functions & wrappers to do with Bounds
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL IS_ZONE_A_BOUNDS_ZONE(INT iZone)
	RETURN GET_FMMC_ZONE_TYPE(iZone) = ciFMMC_ZONE_TYPE__BOUNDS
ENDFUNC

FUNC BOOL ARE_BOUNDS_ACTIVE(INT iBoundsIndex = ciRULE_BOUNDS_INDEX__ANY, BOOL bOnlyIncludeCurrentlyProcessedBounds = TRUE)
	
	IF iBoundsIndex = ciRULE_BOUNDS_INDEX__NONE
		RETURN FALSE
		
	ELIF iBoundsIndex = ciRULE_BOUNDS_INDEX__ANY
		FOR iBoundsIndex = 0 TO ciMAX_RULE_BOUNDS_PER_RULE - 1
			IF ARE_BOUNDS_ACTIVE(iBoundsIndex)
				RETURN TRUE
			ENDIF
		ENDFOR
		
		RETURN FALSE
		
	ELIF iBoundsIndex = ciRULE_BOUNDS_INDEX__ALL
		FOR iBoundsIndex = 0 TO ciMAX_RULE_BOUNDS_PER_RULE - 1
			IF NOT ARE_BOUNDS_ACTIVE(iBoundsIndex)
				RETURN FALSE
			ENDIF
		ENDFOR
		
		RETURN TRUE
	ENDIF
	
	IF bOnlyIncludeCurrentlyProcessedBounds
		IF IS_BIT_SET(sRuleBoundsRuntimeVars[iBoundsIndex].iRuleBoundsBS, ciRULE_BOUNDS_BS__DONT_PROCESS_BOUNDS_THIS_FRAME)
			PRINTLN("[Bounds ", iBoundsIndex, "] ARE_BOUNDS_ACTIVE | returning FALSE due to ciRULE_BOUNDS_BS__DONT_PROCESS_BOUNDS_THIS_FRAME")
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF NOT MC_DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP(iBoundsIndex, eSGET_Bounds, GET_LOCAL_PLAYER_TEAM(), GET_LOCAL_PLAYER_CURRENT_RULE())
		PRINTLN("[Bounds ", iBoundsIndex, "] ARE_BOUNDS_ACTIVE | returning FALSE due to MC_DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP")
		RETURN FALSE
	ENDIF
	
	RETURN sRuleBoundsRuntimeVars[iBoundsIndex].iRuleBoundsActiveZoneCount > 0
ENDFUNC

FUNC BOOL ARE_BOUNDS_VALID(INT iBoundsIndex, INT iTeam = -1)
	IF iTeam = -1
		iTeam = GET_LOCAL_PLAYER_TEAM(TRUE)
	ENDIF
	RETURN NOT IS_LONG_BITSET_EMPTY(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sRuleBounds[GET_TEAM_CURRENT_RULE(iTeam)][iBoundsIndex].iZonesToUseBS)
ENDFUNC

FUNC BOOL IS_THIS_ZONE_SET_AS_BOUNDS_ON_CURRENT_RULE(INT iZone, INT iBoundsIndex = 0, INT iTeam = -1)
	IF iTeam = -1
		iTeam = GET_LOCAL_PLAYER_TEAM(TRUE)
	ENDIF
	RETURN IS_LONG_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sRuleBounds[GET_TEAM_CURRENT_RULE(iTeam)][iBoundsIndex].iZonesToUseBS, iZone) //Used Externally
ENDFUNC

FUNC INT GET_BOUNDS_FIRST_ACTIVE_ZONE(INT iBoundsIndex)
	IF NOT ARE_BOUNDS_VALID(iBoundsIndex)
		RETURN -1
	ENDIF
	
	IF sRuleBoundsRuntimeVars[iBoundsIndex].iRuleBoundsFirstActiveZone > -1
		IF IS_THIS_ZONE_SET_AS_BOUNDS_ON_CURRENT_RULE(sRuleBoundsRuntimeVars[iBoundsIndex].iRuleBoundsFirstActiveZone, iBoundsIndex)
			RETURN sRuleBoundsRuntimeVars[iBoundsIndex].iRuleBoundsFirstActiveZone
		ENDIF
	ENDIF
	
	RETURN -1
ENDFUNC

FUNC PED_INDEX GET_BOUNDS_PED_TO_USE()
	RETURN PlayerPedToUse
ENDFUNC
FUNC PLAYER_INDEX GET_BOUNDS_PLAYER_TO_USE()
	RETURN PlayerToUse
ENDFUNC
FUNC INT GET_BOUNDS_PART_TO_USE()
	RETURN iPartToUse
ENDFUNC

FUNC BOOL ARE_BOUNDS_CRITICAL(INT iBoundsIndex, INT iTeam, INT iRule)
	IF iTeam = -1
		iTeam = GET_LOCAL_PLAYER_TEAM(TRUE)
	ENDIF
	
	IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sRuleBounds[iRule][iBoundsIndex].iRuleBoundsType = ciRULE_BOUNDS_TYPE__PLAY_AREA
	OR g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sRuleBounds[iRule][iBoundsIndex].iRuleBoundsType = ciRULE_BOUNDS_TYPE__LEAVE_AREA
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PARTICIPANT_OUT_OF_BOUNDS(INT iPart, INT iBoundsIndex = ciRULE_BOUNDS_INDEX__ANY, BOOL bCriticalBoundsOnly = FALSE)
	
	IF NOT ARE_BOUNDS_ACTIVE(iBoundsIndex)
		RETURN FALSE
	ENDIF
	
	IF iBoundsIndex = ciRULE_BOUNDS_INDEX__NONE
		RETURN FALSE
		
	ELIF iBoundsIndex = ciRULE_BOUNDS_INDEX__ANY
		FOR iBoundsIndex = 0 TO ciMAX_RULE_BOUNDS_PER_RULE - 1
			IF IS_PARTICIPANT_OUT_OF_BOUNDS(iPart, iBoundsIndex, bCriticalBoundsOnly)
				RETURN TRUE
			ENDIF
		ENDFOR
	
	ELIF iBoundsIndex = ciRULE_BOUNDS_INDEX__ALL
		FOR iBoundsIndex = 0 TO ciMAX_RULE_BOUNDS_PER_RULE - 1
			IF NOT IS_PARTICIPANT_OUT_OF_BOUNDS(iPart, iBoundsIndex, bCriticalBoundsOnly)
				RETURN FALSE
			ENDIF
		ENDFOR
		
		RETURN TRUE
	
	ELSE
		IF bCriticalBoundsOnly
			INT iParticipantTeam = MC_playerBD[iPart].iTeam
			IF NOT ARE_BOUNDS_CRITICAL(iBoundsIndex, iParticipantTeam, GET_TEAM_CURRENT_RULE(iParticipantTeam))
				// Even if the player is technically out of these bounds, we aren't interested in checking them here
				PRINTLN("[Bounds_SPAM] IS_PARTICIPANT_OUT_OF_BOUNDS | Ignoring bounds ", iBoundsIndex, " because they're not critical!")
				RETURN FALSE
			ENDIF
		ENDIF
	
		RETURN IS_BIT_SET(MC_playerBD[iPart].iClientBitSet2, PBBOOL2_OUT_OF_BOUNDS_0 + iBoundsIndex)
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_OUT_OF_BOUNDS(INT iBoundsIndex = ciRULE_BOUNDS_INDEX__ANY)
	RETURN IS_PARTICIPANT_OUT_OF_BOUNDS(iPartToUse, iBoundsIndex)
ENDFUNC

FUNC BOOL IS_PARTICIPANT_INSIDE_BOUNDS(INT iPart, INT iBoundsIndex = ciRULE_BOUNDS_INDEX__ANY)
	RETURN NOT IS_PARTICIPANT_OUT_OF_BOUNDS(iPart, iBoundsIndex)
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_INSIDE_BOUNDS(INT iBoundsIndex = ciRULE_BOUNDS_INDEX__ANY)
	RETURN NOT IS_LOCAL_PLAYER_OUT_OF_BOUNDS(iBoundsIndex)
ENDFUNC

FUNC BOOL IS_OBJ_OUT_OF_BOUNDS(INT iObjIndex)
	RETURN IS_BIT_SET(iObjOnGroundOutOfBoundsBS, iObjIndex)
ENDFUNC

PROC CLEAR_BOUNDS_OBJECTIVE_TEXT(INT iBoundsIndex)
	IF iBoundsToShowObjectiveText = iBoundsIndex
		tlOutOfBoundsObjectiveText = ""
		iBoundsToShowObjectiveText = -1
		PRINTLN("[Bounds][RB ", iBoundsIndex, "] CLEAR_BOUNDS_OBJECTIVE_TEXT | Clearing iBoundsToShowObjectiveText")
	ENDIF
ENDPROC

FUNC INT GET_BOUNDS_TO_SHOW_OBJECTIVE_TEXT()
	RETURN iBoundsToShowObjectiveText
ENDFUNC

PROC RESET_RULE_BOUNDS_OUT_OF_BOUNDS_VARS(INT iBoundsIndex)
	
	RESET_NET_TIMER(sRuleBoundsRuntimeVars[iBoundsIndex].stOutOfBoundsTimer)
	
	CLEAR_BOUNDS_OBJECTIVE_TEXT(iBoundsIndex)
	
	IF IS_SOUND_ID_VALID(iBoundsTimerSound)
		IF NOT HAS_SOUND_FINISHED(iBoundsTimerSound)
			STOP_SOUND(iBoundsTimerSound)
		ENDIF
		
		RELEASE_SOUND_ID(iBoundsTimerSound)
		iBoundsTimerSound = -1		
		PRINTLN("[Bounds][RB ", iBoundsIndex, "] RESET_RULE_BOUNDS_OUT_OF_BOUNDS_VARS | Stopping and clearing up iBoundsTimerSound")
	ENDIF
	
	IF IS_BIT_SET(sRuleBoundsRuntimeVars[iBoundsIndex].iRuleBoundsBS, ciRULE_BOUNDS_BS__DISPLAYING_BIG_MESSAGE)
	
		PRINTLN("[Bounds][RB ", iBoundsIndex, "] RESET_RULE_BOUNDS_OUT_OF_BOUNDS_VARS | Clearing shards!")
		
		IF IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_OO_BOUNDS)
			CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_OO_BOUNDS)
		ENDIF
		IF IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_CUSTOM)
			CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_CUSTOM)
		ENDIF
		IF IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_LEAVE_AREA)
			CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_LEAVE_AREA)
		ENDIF
		
		CLEAR_BIT(iSharedRuleBoundsBS, ciSHARED_RULE_BOUNDS_BS__DISPLAYING_BIG_MESSAGE)
		CLEAR_BIT(sRuleBoundsRuntimeVars[iBoundsIndex].iRuleBoundsBS, ciRULE_BOUNDS_BS__DISPLAYING_BIG_MESSAGE)
	ENDIF
ENDPROC

FUNC BOOL ARE_ANY_RULE_BOUNDS_ON_HOLD()
	RETURN iRuleBoundsCurrentlyOnHoldBS != 0
ENDFUNC

PROC SET_RULE_BOUNDS_TO_WAIT_FOR_FULL_STAGGERED_LOOP(INT iBoundsIndex)
	IF NOT IS_BIT_SET(sRuleBoundsRuntimeVars[iBoundsIndex].iRuleBoundsBS, ciRULE_BOUNDS_BS__WAIT_FOR_NEXT_COMPLETED_ZONE_STAGGERED_LOOP)
		SET_BIT(sRuleBoundsRuntimeVars[iBoundsIndex].iRuleBoundsBS, ciRULE_BOUNDS_BS__WAIT_FOR_NEXT_COMPLETED_ZONE_STAGGERED_LOOP)
		SET_BIT(sRuleBoundsRuntimeVars[iBoundsIndex].iRuleBoundsBS, ciRULE_BOUNDS_BS__BLOCK_BOUNDS_EVERY_FRAME_PROCESSING)
		
		IF sRuleBoundsRuntimeVars[iBoundsIndex].iRuleBoundsLatestBoundsZoneProcessed > -1
			SET_BIT(sRuleBoundsRuntimeVars[iBoundsIndex].iRuleBoundsBS, ciRULE_BOUNDS_BS__WAITING_FOR_ZONE_STAGGERED_LOOP_LOOP_POINT)
			sRuleBoundsRuntimeVars[iBoundsIndex].iRuleBoundsFullZoneStaggeredLoop_LoopPoint = sRuleBoundsRuntimeVars[iBoundsIndex].iRuleBoundsLatestBoundsZoneProcessed
			PRINTLN("[Bounds][RB ", iBoundsIndex, "] SET_RULE_BOUNDS_TO_WAIT_FOR_FULL_STAGGERED_LOOP | Now set to wait for the next Zone staggered loop to reach Zone ", sRuleBoundsRuntimeVars[iBoundsIndex].iRuleBoundsFullZoneStaggeredLoop_LoopPoint, " again")
		ELSE
			CLEAR_BIT(sRuleBoundsRuntimeVars[iBoundsIndex].iRuleBoundsBS, ciRULE_BOUNDS_BS__WAITING_FOR_ZONE_STAGGERED_LOOP_LOOP_POINT)
			sRuleBoundsRuntimeVars[iBoundsIndex].iRuleBoundsFullZoneStaggeredLoop_LoopPoint = -1
			PRINTLN("[Bounds][RB ", iBoundsIndex, "] SET_RULE_BOUNDS_TO_WAIT_FOR_FULL_STAGGERED_LOOP | No Bounds Zone is currently valid to be used as a loop point! We'll just wait for the end of the next loop")
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_RULE_BOUNDS_WAIT_FOR_FULL_STAGGERED_LOOP(INT iBoundsIndex)
	
	UNUSED_PARAMETER(iBoundsIndex) // For Release
	
	IF NOT bLocalPlayerPedOk
	AND NOT bIsAnySpectator
		PRINTLN("[Bounds][RB ", iBoundsIndex, "] SHOULD_RULE_BOUNDS_WAIT_FOR_FULL_STAGGERED_LOOP | Local player ped is not okay!")
		RETURN TRUE
	ENDIF
	
	IF GlobalplayerBD[NATIVE_TO_INT(LocalPlayer)].iRespawnState != RESPAWN_STATE_PLAYING
	AND NOT IS_FAKE_MULTIPLAYER_MODE_SET(TRUE)
		PRINTLN("[Bounds][RB ", iBoundsIndex, "] SHOULD_RULE_BOUNDS_WAIT_FOR_FULL_STAGGERED_LOOP | Local player ped is respawning!")
		RETURN TRUE
	ENDIF
	
	IF GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iWarpToLocationState != 0
		PRINTLN("[Bounds][RB ", iBoundsIndex, "] SHOULD_RULE_BOUNDS_WAIT_FOR_FULL_STAGGERED_LOOP | Local player ped is warping!")
		RETURN TRUE
	ENDIF
	
	IF DOES_LOCAL_PLAYER_HAVE_NEW_PRIORITY_THIS_FRAME()
		PRINTLN("[Bounds][RB ", iBoundsIndex, "] SHOULD_RULE_BOUNDS_WAIT_FOR_FULL_STAGGERED_LOOP | Local player has just reached rule ", GET_LOCAL_PLAYER_CURRENT_RULE())
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Leaderboard Helpers
// ##### Description: Various helper functions & wrappers for accessign leaderboard data
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC INT GET_LEADERBOARD_INDEX_OF_TOP_WINNER()
	INT i = 0
	REPEAT COUNT_OF(g_MissionControllerserverBD_LB.sleaderboard) i
		INT iParticipant = g_MissionControllerserverBD_LB.sleaderboard[i].iParticipant
		IF iParticipant > -1
			IF MC_Playerbd[iParticipant].iteam = MC_serverBD.iWinningTeam
			AND HAS_TEAM_PASSED_MISSION(MC_Playerbd[iParticipant].iteam)
				RETURN i
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN 0
ENDFUNC

FUNC INT GET_LEADERBOARD_INDEX_OF_PLAYER(PLAYER_INDEX playerId)
	INT i = 0
	REPEAT COUNT_OF(g_MissionControllerserverBD_LB.sleaderboard) i
		IF g_MissionControllerserverBD_LB.sleaderboard[i].playerID = playerId
			RETURN i
		ENDIF
	ENDREPEAT
	RETURN 0
ENDFUNC

FUNC INT GET_PLAYERS_LEADERBOARD_POSITION(PLAYER_INDEX tempPlayer)

INT iplayer
INT iReturnPlayer = -1

	PRINTLN("[PLAYER_LOOP] - GET_PLAYERS_LEADERBOARD_POSITION")
	FOR iplayer = 0 TO (NUM_NETWORK_PLAYERS-1)
		IF g_MissionControllerserverBD_LB.sleaderboard[iplayer].playerID = tempPlayer
			iReturnPlayer = iplayer
			iplayer = NUM_NETWORK_PLAYERS // Break out!
		ENDIF
	ENDFOR
	
	RETURN iReturnPlayer

ENDFUNC

FUNC INT GET_PLAYERS_TEAM_LEADERBOARD_POSITION(PLAYER_INDEX tempPlayer, INT iTeam)

	INT iplayer
	INT iReturnPos = -1
	INT iOffset

	PRINTLN("[PLAYER_LOOP] - GET_PLAYERS_TEAM_LEADERBOARD_POSITION")
	FOR iplayer = 0 TO (NUM_NETWORK_PLAYERS-1)
		IF g_MissionControllerserverBD_LB.sleaderboard[iplayer].iTeam = iTeam
			IF g_MissionControllerserverBD_LB.sleaderboard[iplayer].playerID = tempPlayer
				iReturnPos = iplayer
				iplayer = NUM_NETWORK_PLAYERS // Break out!
			ENDIF
		ELSE
			iOffset++
		ENDIF
	ENDFOR
	
	iReturnPos = iReturnPos - iOffset
	PRINTLN("[JS] GET_PLAYERS_TEAM_LEADERBOARD_POSITION - ", iReturnPos)
	RETURN iReturnPos

ENDFUNC


				
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Weapons
// ##### Description: Functions for weapons and inventories
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

#IF IS_DEBUG_BUILD
FUNC STRING GET_INVENTORY_TYPE_NAME(FMMC_INVENTORY_TYPES eInventoryType)
	SWITCH eInventoryType
		CASE FMMC_INVENTORY_STARTING
			RETURN "Starting"
		CASE FMMC_INVENTORY_MID_MISSION
			RETURN "Mid-Mission"
		CASE FMMC_INVENTORY_MID_MISSION_2
			RETURN "Mid-Mission 2"
		CASE FMMC_INVENTORY_CUSTOM_MID_MISSION
			RETURN "Custom Mid-Mission"
	ENDSWITCH
	
	RETURN "INVALID"
ENDFUNC

PROC GIVE_DEV_TEST_MODE_WEAPONS(FMMC_INVENTORY_TYPES eInventoryType)
	IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
	OR NOT IS_ROCKSTAR_DEV()
	OR eInventoryType != FMMC_INVENTORY_STARTING
		EXIT
	ENDIF
	
	EQUIP_PLAYER_WITH_STARTING_MISSION_WEAPONS_AND_AMMO(WEAPONTYPE_RPG, 10, TRUE, TRUE, FALSE)
	EQUIP_PLAYER_WITH_STARTING_MISSION_WEAPONS_AND_AMMO(WEAPONTYPE_STICKYBOMB, 10, TRUE, TRUE, FALSE)
	EQUIP_PLAYER_WITH_STARTING_MISSION_WEAPONS_AND_AMMO(WEAPONTYPE_PISTOL, 500, TRUE, TRUE, FALSE)
	EQUIP_PLAYER_WITH_STARTING_MISSION_WEAPONS_AND_AMMO(WEAPONTYPE_ASSAULTRIFLE, 1000, TRUE, TRUE, FALSE)
ENDPROC
#ENDIF

FUNC INT GET_INVENTORY_INDEX_FOR_LOCAL_TEAM(FMMC_INVENTORY_TYPES eInventoryType)
	SWITCH eInventoryType
		CASE FMMC_INVENTORY_STARTING
			RETURN iInventory_Starting[GET_LOCAL_PLAYER_TEAM()]
		CASE FMMC_INVENTORY_MID_MISSION
			RETURN g_FMMC_STRUCT.sTeamData[GET_LOCAL_PLAYER_TEAM()].iMidMissionInventoryIndex
		CASE FMMC_INVENTORY_MID_MISSION_2
			RETURN g_FMMC_STRUCT.sTeamData[GET_LOCAL_PLAYER_TEAM()].iMidMissionInventory2Index
		CASE FMMC_INVENTORY_CUSTOM_MID_MISSION
			RETURN iInventory_CustomMidMission
	ENDSWITCH
	
	RETURN -1
ENDFUNC

FUNC BOOL GIVE_LOCAL_PLAYER_WEAPON_INVENTORY(FMMC_INVENTORY_TYPES eInventoryType, WEAPONINHAND WhichWeaponInHand, BOOL bWeaponsWereRemoved = FALSE)
	
	INT iInventoryIndex = GET_INVENTORY_INDEX_FOR_LOCAL_TEAM(eInventoryType)
	
	IF iInventoryIndex = -1
		#IF IS_DEBUG_BUILD
		GIVE_DEV_TEST_MODE_WEAPONS(eInventoryType)
		PRINTLN("GIVE_LOCAL_PLAYER_WEAPON_INVENTORY - Not giving any weapons. Invalid inventory index for ", GET_INVENTORY_TYPE_NAME(eInventoryType))
		#ENDIF
		RETURN FALSE
	ENDIF
	
	PRINTLN("GIVE_LOCAL_PLAYER_WEAPON_INVENTORY - Giving ", GET_INVENTORY_TYPE_NAME(eInventoryType), " inventory ", iInventoryIndex," weapons.")
	
	GIVE_PLAYER_FMMC_INVENTORY(g_FMMC_STRUCT.sPlayerWeaponInventories[iInventoryIndex], WhichWeaponInHand, bWeaponsWereRemoved)
	
	IF g_FMMC_STRUCT.sPlayerWeaponInventories[iInventoryIndex].sParachute.iCount = ciFMMC_PARACHUTE_COUNT_INFINITE
		SET_BIT(iLocalBoolCheck33, LBOOL33_INFINITE_PARACHUTES)
	ELSE
		CLEAR_BIT(iLocalBoolCheck33, LBOOL33_INFINITE_PARACHUTES)
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC UPDATE_CUSTOM_MID_MISSION_INVENTORY(INT iNewCustomInventoryIndex)
	IF iInventory_CustomMidMission != iNewCustomInventoryIndex
		iInventory_CustomMidMission = iNewCustomInventoryIndex
		PRINTLN("UPDATE_CUSTOM_MID_MISSION_INVENTORY - Updated to ", iInventory_CustomMidMission)
		
		IF IS_BIT_SET(g_FMMC_STRUCT.sPlayerWeaponInventories[iInventory_CustomMidMission].iInventoryBitset, ciFMMC_INVENTORY_BS_UPDATE_STARTING_INVENTORY_WHEN_GIVEN)
			iInventory_Starting[GET_LOCAL_PLAYER_TEAM()] = iInventory_CustomMidMission
			PRINTLN("UPDATE_CUSTOM_MID_MISSION_INVENTORY - iInventory_Starting is now set to: ", iInventory_Starting[GET_LOCAL_PLAYER_TEAM()])
		ENDIF
	ENDIF
ENDPROC

PROC GIVE_FORCED_WEAPON_AND_AMMO(BOOL bInitialSpawn, INT& iWeaponBitSet, WEAPON_TYPE& wtWeaponRemove, BOOL bStartingInventory = FALSE)

	PRINTLN("[RCC MISSION][FORCE_WEAPON] - GIVE_FORCED_WEAPON_AND_AMMO - START_______________________________ ")
	
	WEAPON_TYPE wtWeaponToGive

	BOOL bForcedWeapon
	
	IF NOT IS_JOB_OWNED_WEAPONS_PLUS_PICKUPS()
		bForcedWeapon = TRUE
	ENDIF
	
	PRINTLN("[RCC MISSION][FORCE_WEAPON] - GIVE_FORCED_WEAPON_AND_AMMO bInitialSpawn = ", bInitialSpawn, " bStartingInventory = ", bStartingInventory, " bForcedWeapon = ", bForcedWeapon)
	
	// Check for new DLC weapons as we need to remove any that were dished out
	IF bInitialSpawn
		IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
			IF NOT IS_PLAYER_ON_IMPROMPTU_DM()
			AND NOT IS_PLAYER_ON_BOSSVBOSS_DM() 
				DEAL_WITH_DLC_WEAPONS(iWeaponBitSet, TRUE)
			ENDIF
		ENDIF
	ENDIF
	
	// Special case for Missions
	IF bStartingInventory
		
		IF NOT bInitialSpawn OR NOT bForcedWeapon
			PRINTLN("[RCC MISSION][FORCE_WEAPON] - GIVE_FORCED_WEAPON_AND_AMMO - iStartingInventory != 0, giving starting mission weapons...")
			
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciMISSION_OVERRIDE_AMMUNATION_FORCE_CLOSE)
				g_bMissionInventory = TRUE
			ENDIF
			
			IF IS_CHECKPOINT_ANIM_GLOBAL_SET()
				GIVE_LOCAL_PLAYER_WEAPON_INVENTORY(FMMC_INVENTORY_STARTING, WEAPONINHAND_FIRSTSPAWN_HOLSTERED, bForcedWeapon)
				PRINTLN("[RCC MISSION][FORCE_WEAPON] - GIVE_FORCED_WEAPON_AND_AMMO - NOT GIVE_PLAYER_STARTING_MISSION_WEAPONS, SHOULD_USE_CHECKPOINT_ANIMS ")
			ELSE
										
				IF bInitialSpawn
					GIVE_LOCAL_PLAYER_WEAPON_INVENTORY(FMMC_INVENTORY_STARTING, WEAPONINHAND_LASTWEAPON_BOTH, bForcedWeapon)
					PRINTLN("[RCC MISSION][FORCE_WEAPON] - GIVE_FORCED_WEAPON_AND_AMMO - bInitialSpawn = TRUE - WEAPONINHAND_LASTWEAPON_BOTH")
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciRESPAWN_WITH_LAST_WEAPON)
					PRINTLN("[RCC MISSION][FORCE_WEAPON] - GIVE_FORCED_WEAPON_AND_AMMO - ciRESPAWN_WITH_LAST_WEAPON")
					IF IS_BIT_SET(g_FMMC_STRUCT.iRuleOptionsBitSet, ciGIVE_WEAPONS_ON_RESPAWN) 
						GIVE_LOCAL_PLAYER_WEAPON_INVENTORY(FMMC_INVENTORY_STARTING, WEAPONINHAND_LASTWEAPON_BOTH, bForcedWeapon)
					ENDIF
					
					IF DOES_PLAYER_HAVE_WEAPON(GET_PLAYER_CURRENT_HELD_WEAPON())
						WEAPON_TYPE wtWeapon
						wtWeapon = GET_PLAYER_CURRENT_HELD_WEAPON()
						IF wtWeapon != WEAPONTYPE_INVALID
							PRINTLN("[RCC MISSION][FORCE_WEAPON] - GIVE_FORCED_WEAPON_AND_AMMO - wtWeapon = VALID | Calling SET_CURRENT_PED_WEAPON for ", ENUM_TO_INT(wtWeapon))
							SET_CURRENT_PED_WEAPON(LocalPlayerPed, wtWeapon, TRUE)
						ELIF wtWeapon = WEAPONTYPE_UNARMED
							PRINTLN("[RCC MISSION][FORCE_WEAPON] - GIVE_FORCED_WEAPON_AND_AMMO - wtWeapon = WT_UNARMED")
							wtWeapon = GET_MISSION_STARTING_WEAPON(g_FMMC_STRUCT.sFMMCEndConditions[g_i_Mission_team].iStartingInventoryStartWep)
							
							IF wtWeapon = WEAPONTYPE_UNARMED
								PRINTLN("[RCC MISSION][FORCE_WEAPON] - GIVE_FORCED_WEAPON_AND_AMMO - wtWeapon = WT_UNARMED - no start weapon set")
								wtWeapon = GET_BEST_PED_WEAPON(LocalPlayerPed, FALSE)

								PRINTLN("[RCC MISSION][FORCE_WEAPON] - GIVE_FORCED_WEAPON_AND_AMMO - wtWeapon = WT_UNARMED - ped has a good weapon")
								SET_CURRENT_PED_WEAPON(LocalPlayerPed, wtWeapon, TRUE)
								SET_PLAYER_CURRENT_HELD_WEAPON(wtWeapon)
								
							ENDIF
						ELSE
							PRINTLN("[RCC MISSION][FORCE_WEAPON] - GIVE_FORCED_WEAPON_AND_AMMO - wtWeapon = INVALID")
						ENDIF
					ELSE
						PRINTLN("[RCC MISSION][FORCE_WEAPON] - GIVE_FORCED_WEAPON_AND_AMMO - NOT DOES_PLAYER_HAVE_WEAPON")
					ENDIF
					
				ELSE
					IF IS_BIT_SET(g_FMMC_STRUCT.iRuleOptionsBitSet, ciGIVE_WEAPONS_ON_RESPAWN) 
						GIVE_LOCAL_PLAYER_WEAPON_INVENTORY(FMMC_INVENTORY_STARTING, WEAPONINHAND_LASTWEAPON_BOTH, bForcedWeapon)
						PRINTLN("[RCC MISSION][FORCE_WEAPON] - GIVE_FORCED_WEAPON_AND_AMMO - (g_FMMC_STRUCT.iRuleOptionsBitSet, ciGIVE_WEAPONS_ON_RESPAWN) = TRUE - WEAPONINHAND_LASTWEAPON_BOTH")
					ENDIF	
				ENDIF		
				
				PUT_WEAPON_IN_HAND(WEAPONINHAND_LASTWEAPON_BOTH)
			ENDIF
			
			EXIT
		ENDIF
	ENDIF
	
	// Remove all weapons for initial spawn
	IF bInitialSpawn
		IF NOT IS_JOB_OWNED_WEAPONS_PLUS_PICKUPS()
			REMOVE_ALL_PED_WEAPONS(LocalPlayerPed)
			PRINTLN("[RCC MISSION][FORCE_WEAPON] - GIVE_FORCED_WEAPON_AND_AMMO - IS_JOB_FORCED_WEAPON_ONLY, REMOVE_ALL_PED_WEAPONS ")
			SET_BLOCK_AMMO_GLOBAL()
		ENDIF
	ENDIF
		
	// Backup pistol
	IF wtWeaponToGive = WEAPONTYPE_UNARMED
	AND NOT IS_JOB_OWNED_WEAPONS_PLUS_PICKUPS()
	AND NOT bInitialSpawn
		wtWeaponToGive = WEAPONTYPE_PISTOL
		PRINTLN("[RCC MISSION][FORCE_WEAPON]  GIVE_FORCED_WEAPON_AND_AMMO - GIVE_FORCED_WEAPON_AND_AMMO - Giving player backup pistol: ",  GET_WEAPON_NAME(INT_TO_ENUM(WEAPON_TYPE, wtWeaponToGive)))
	ENDIF
	
	INT iAmmoToGive = 0
	
	// Give weapon
	IF IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, FALSE)
	AND IS_WEAPON_VALID(wtWeaponToGive)
		IF NOT IS_MP_WEAPON_EQUIPPED(wtWeaponToGive)
		OR NOT IS_JOB_OWNED_WEAPONS_PLUS_PICKUPS() 
		
			BOOL bInhand = FALSE
			IF bForcedWeapon
			AND wtWeaponToGive != WEAPONTYPE_PETROLCAN
				bInhand = TRUE
			ENDIF
			
			IF IS_WEAPON_VALID(wtWeaponToGive)
				IF bInitialSpawn
					IF HAS_CORONA_WEAPON_GOT_GUNRUNNING_AMMO_TYPE(wtWeaponToGive)
						SET_CURRENT_PED_WEAPON(LocalPlayerPed, wtWeaponToGive, bInhand)
						PRINTLN("[RCC MISSION][FORCE_WEAPON] - GIVE_FORCED_WEAPON_AND_AMMO - bInitialSpawn, SET_CURRENT_PED_WEAPON  ", GET_WEAPON_NAME(wtWeaponToGive))
					ELSE
						iAmmoToGive = GET_AMMO_FOR_WEAPON(wtWeaponToGive)
						GIVE_WEAPON_TO_PED(LocalPlayerPed, wtWeaponToGive, iAmmoToGive, bInhand)
					ENDIF
				ENDIF
			ENDIF
			
			IF bInitialSpawn
				IF NOT IS_MP_WEAPON_EQUIPPED(wtWeaponToGive)
				AND wtWeaponToGive != WEAPONTYPE_UNARMED
					wtWeaponRemove = wtWeaponToGive
					PRINTLN("[RCC MISSION][FORCE_WEAPON] GIVE_FORCED_WEAPON_AND_AMMO - PLAYER_DIDNT_HAVE_WEAPON_GIVING ",  GET_WEAPON_NAME(INT_TO_ENUM(WEAPON_TYPE, wtWeaponToGive)))
				ENDIF
			ELSE
				IF bForcedWeapon = TRUE
				AND NOT g_bMissionEnding
				
					wtWeaponToGive = GET_DEATHMATCH_RESPAWN_WEAPON()
					PRINTLN("[RCC MISSION][FORCE_WEAPON] GIVE_FORCED_WEAPON_AND_AMMO - bForcedWeapon weapon locked to  ", GET_WEAPON_NAME(wtWeaponToGive))
					
					IF IS_WEAPON_VALID(wtWeaponToGive)
						IF NOT DOES_PLAYER_HAVE_WEAPON(wtWeaponToGive)
							PRINTLN("[RCC MISSION][FORCE_WEAPON] GIVE_FORCED_WEAPON_AND_AMMO - Does not have a  ", GET_WEAPON_NAME(wtWeaponToGive))
							
							IF IS_WEAPON_VALID(wtWeaponToGive)
								IF HAS_CORONA_WEAPON_GOT_GUNRUNNING_AMMO_TYPE(wtWeaponToGive)
									SET_CURRENT_PED_WEAPON(LocalPlayerPed, wtWeaponToGive)
									PRINTLN("[RCC MISSION][FORCE_WEAPON] GIVE_FORCED_WEAPON_AND_AMMO - SET_CURRENT_PED_WEAPON  ", GET_WEAPON_NAME(wtWeaponToGive))
								ELSE
									iAmmoToGive = GET_AMMO_FOR_WEAPON(wtWeaponToGive)
									GIVE_WEAPON_TO_PED(LocalPlayerPed, wtWeaponToGive, iAmmoToGive)
									PRINTLN("[RCC MISSION][FORCE_WEAPON] GIVE_FORCED_WEAPON_AND_AMMO - GIVE_WEAPON_TO_PED  ", GET_WEAPON_NAME(wtWeaponToGive))
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF g_bMissionEnding
						PRINTLN("[RCC MISSION][FORCE_WEAPON] GIVE_FORCED_WEAPON_AND_AMMO - Not doing GIVE_WEAPON_TO_PED due to g_bMissionEnding")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	// For non forced weapon modes
	IF NOT bInitialSpawn
	AND IS_JOB_OWNED_WEAPONS_PLUS_PICKUPS()
		wtWeaponToGive = GET_PLAYER_CURRENT_HELD_WEAPON()
		PRINTLN("[RCC MISSION][FORCE_WEAPON] GIVE_FORCED_WEAPON_AND_AMMO - Player's last weapon was a  ", GET_WEAPON_NAME(wtWeaponToGive))
		IF IS_THROWN_WEAPON(wtWeaponToGive)
		OR wtWeaponToGive = WEAPONTYPE_INVALID
			wtWeaponToGive = WEAPONTYPE_PISTOL
		ENDIF
		IF NOT DOES_PLAYER_HAVE_WEAPON(wtWeaponToGive)
			PRINTLN("[RCC MISSION][FORCE_WEAPON] GIVE_FORCED_WEAPON_AND_AMMO - Player does not have a ", GET_WEAPON_NAME(wtWeaponToGive))
			// Pistol if all else fails
			wtWeaponToGive = WEAPONTYPE_PISTOL
			iAmmoToGive = GET_AMMO_FOR_WEAPON(wtWeaponToGive)
			
			IF IS_WEAPON_VALID(wtWeaponToGive)
				GIVE_WEAPON_TO_PED(LocalPlayerPed, wtWeaponToGive, iAmmoToGive)
			ENDIF
			PRINTLN("[RCC MISSION][FORCE_WEAPON] GIVE_FORCED_WEAPON_AND_AMMO - GIVE_PLAYER_PISTOL ")
		ENDIF
	ENDIF
	
	IF wtWeaponToGive != WEAPONTYPE_INVALID
		// Finish up and put weapon in hand
		AMMO_FAILSAFE(wtWeaponToGive, bForcedWeapon)
		REFILL_AMMO_INSTANTLY(LocalPlayerPed)
		GIVE_WEAPON_EQUIPPED_TINT(wtWeaponToGive)
	ENDIF

	IF NOT bForcedWeapon
		IF g_SpawnData.MyLastWeapon != WEAPONTYPE_INVALID
			GIVE_PLAYER_ALL_EQUIPPED_WEAPON_ADDONS_FOR_WEAPON(g_SpawnData.MyLastWeapon)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciDISABLE_ARMING) 
		SET_CURRENT_PED_WEAPON(LocalPlayerPed, WEAPONTYPE_UNARMED, TRUE)
	ELSE
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciRESPAWN_WITH_LAST_WEAPON)
			IF GET_PLAYER_CURRENT_HELD_WEAPON() = WEAPONTYPE_UNARMED
			AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciRESPAWN_WITH_BEST_WEAPON)
				IF GET_BEST_PED_WEAPON(LocalPlayerPed, FALSE) != WEAPONTYPE_INVALID
					SET_CURRENT_PED_WEAPON(LocalPlayerPed, GET_BEST_PED_WEAPON(LocalPlayerPed, FALSE), TRUE)
					PRINTLN("[RCC MISSION][FORCE_WEAPON] GIVE_FORCED_WEAPON_AND_AMMO - SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), GET_BEST_PED_WEAPON(PLAYER_PED_ID(), FALSE), TRUE)")
				ENDIF
			ELSE
				PRINTLN("[RCC MISSION][FORCE_WEAPON] GIVE_FORCED_WEAPON_AND_AMMO - SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), GET_PLAYER_CURRENT_HELD_WEAPON(), TRUE)")
				IF DOES_PLAYER_HAVE_WEAPON(GET_PLAYER_CURRENT_HELD_WEAPON())
					IF IS_WEAPON_VALID(GET_PLAYER_CURRENT_HELD_WEAPON())
						SET_CURRENT_PED_WEAPON(LocalPlayerPed, GET_PLAYER_CURRENT_HELD_WEAPON(), TRUE)
					ENDIF
				ELSE
					PRINTLN("[RCC MISSION][FORCE_WEAPON] GIVE_FORCED_WEAPON_AND_AMMO - Error: Player does not have this weapon! ", GET_PLAYER_CURRENT_HELD_WEAPON())
				ENDIF
			ENDIF
		ELSE
			IF IS_WEAPON_VALID(wtWeaponToGive)
				IF DOES_PLAYER_HAVE_WEAPON(wtWeaponToGive)
					SET_CURRENT_PED_WEAPON(LocalPlayerPed, wtWeaponToGive, TRUE)
				ELSE
					PRINTLN("[RCC MISSION][FORCE_WEAPON] GIVE_FORCED_WEAPON_AND_AMMO - DOES_PLAYER_HAVE_WEAPON(wtWeaponToGive) FALSE ")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF SHOULD_PLAYER_RETAIN_EXACT_AMMO_ON_RESPAWN()
		WEAPON_TYPE wtPlayerWeapon
		GET_CURRENT_PED_WEAPON(LocalPlayerPed, wtPlayerWeapon, FALSE)
		
		IF IS_WEAPON_VALID(wtPlayerWeapon)
		AND wtPlayerWeapon != WEAPONTYPE_INVALID
		AND NOT IS_WEAPON_A_MELEE_WEAPON(wtPlayerWeapon)
			INT iCurrentAmmo = GET_AMMO_IN_PED_WEAPON(LocalPlayerPed, wtPlayerWeapon)
			INT iClipAmmoToGive = GET_WEAPON_CLIP_SIZE(wtPlayerWeapon)
			
			IF iClipAmmoToGive > 0
				PRINTLN("[RCC MISSION][FORCE_WEAPON] GIVE_FORCED_WEAPON_AND_AMMO - Giving player extra ammo (", iClipAmmoToGive, ") on top of their current ammo (", iCurrentAmmo, ") to account for the clip of ammo they're about to lose")
				SET_PED_AMMO(LocalPlayerPed, wtPlayerWeapon, iCurrentAmmo + iClipAmmoToGive, TRUE)
			ENDIF
		ENDIF
	ENDIF
	
	PRINTLN("[RCC MISSION][FORCE_WEAPON] GIVE_FORCED_WEAPON_AND_AMMO - END_______________________________ ")
ENDPROC

PROC RESTORE_CONTINUITY_COLLECTED_WEAPONS()
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_WeaponPickupTracking)
	AND IS_BIT_SET(sMissionLocalContinuityVars.iGeneralLocalTrackingBitset, ciContinuityGenericTrackingBS_MidMissionInventoryCollected)
		PRINTLN("RESTORE_CONTINUITY_COLLECTED_WEAPONS - Restoring collected mid mission inventory")
		GIVE_LOCAL_PLAYER_WEAPON_INVENTORY(FMMC_INVENTORY_MID_MISSION, WEAPONINHAND_LASTWEAPON_BOTH, !IS_JOB_OWNED_WEAPONS_PLUS_PICKUPS())
	ENDIF
	
	IF IS_BIT_SET(sMissionLocalContinuityVars.iLocalTelemetryInts[ciLocalContinuityTelemetryInts_IslandHeist_MiscActionsBS], ciLocalContinuityTelemetry_miscActions_GoldenGunCollected)
		PRINTLN("RESTORE_CONTINUITY_COLLECTED_WEAPONS - Restoring collected Golden Gun")
		GIVE_WEAPON_TO_PED(LocalPlayerPed, WEAPONTYPE_DLC_GADGETPISTOL, 50, TRUE, TRUE)
	ENDIF
	
	IF IS_BIT_SET(sMissionLocalContinuityVars.iLocalTelemetryInts[ciLocalContinuityTelemetryInts_IslandHeist_MiscActionsBS], ciLocalContinuityTelemetry_miscActions_SPAS12Collected)
		PRINTLN("RESTORE_CONTINUITY_COLLECTED_WEAPONS - Restoring collected combat shotgun")
		GIVE_WEAPON_TO_PED(LocalPlayerPed, WEAPONTYPE_DLC_COMBATSHOTGUN, 50, TRUE, TRUE)
	ENDIF
	
ENDPROC

PROC REMOVE_MISSION_RESTRICTED_WEAPONS()
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyNine, ciOptionsBS29_RemoveMissionRestrictedWeapons)
		EXIT
	ENDIF

	IF HAS_PED_GOT_WEAPON(LocalPlayerPed, WEAPONTYPE_DLC_RAYPISTOL)
		REMOVE_WEAPON_FROM_PED(LocalPlayerPed, WEAPONTYPE_DLC_RAYPISTOL)
		PRINTLN("REMOVE_MISSION_RESTRICTED_WEAPONS - Removing Ray Pistol")
	ENDIF
	
ENDPROC


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Proximity Spawning
// ##### Description: Shared system for spawning and cleaning up entities based on player proximity
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


PROC PROCESS_SERVER_PROXIMITY_SPAWNING_PRE_PLAYER_LOOP()
	
	INT i
	FOR i = 0 TO FMMC_MAX_PEDS_BITSET - 1
		iTempProximitySpawning_PedSpawnAllBS[i] = -1 //All 32 bits set
		iTempProximitySpawning_PedSpawnAnyBS[i] = 0
		iTempProximitySpawning_PedSpawnOneBS[i] = 0
		iTempProximitySpawning_PedCleanupBS[i] = 0
	ENDFOR
	
	iTempProximitySpawning_DynoPropSpawnAllBS = -1 //All 32 bits set
	iTempProximitySpawning_DynoPropSpawnAnyBS = 0
	iTempProximitySpawning_DynoPropSpawnOneBS = 0
	iTempProximitySpawning_DynoPropCleanupBS = 0
	
	iTempProximitySpawning_VehSpawnAllBS = -1 //All 32 bits set
	iTempProximitySpawning_VehSpawnAnyBS = 0
	iTempProximitySpawning_VehSpawnOneBS = 0
	iTempProximitySpawning_VehCleanupBS = 0

ENDPROC

PROC PROCESS_SERVER_PROXIMITY_SPAWNING_FOR_PLAYER(INT iPlayer)
	
	#IF IS_DEBUG_BUILD
	IF bProxSpawnDebug_EnablePlayerPositionSpoofing
		EXIT
	ENDIF
	#ENDIF
	
	INT i
	FOR i = 0 TO FMMC_MAX_PEDS_BITSET - 1
		iTempProximitySpawning_PedSpawnAllBS[i] = iTempProximitySpawning_PedSpawnAllBS[i] & MC_playerBD_1[iPlayer].iProximitySpawning_PedSpawnRangeBS[i]
		iTempProximitySpawning_PedSpawnAnyBS[i] = iTempProximitySpawning_PedSpawnAnyBS[i] | MC_playerBD_1[iPlayer].iProximitySpawning_PedSpawnRangeBS[i]
		iTempProximitySpawning_PedSpawnOneBS[i] = iTempProximitySpawning_PedSpawnOneBS[i] ^ MC_playerBD_1[iPlayer].iProximitySpawning_PedSpawnRangeBS[i]
		iTempProximitySpawning_PedCleanupBS[i] = iTempProximitySpawning_PedCleanupBS[i] | MC_playerBD_1[iPlayer].iProximitySpawning_PedCleanupRangeBS[i]
	ENDFOR
	
	iTempProximitySpawning_DynoPropSpawnAllBS = iTempProximitySpawning_DynoPropSpawnAllBS & MC_playerBD_1[iPlayer].iProximitySpawning_DynoPropSpawnRangeBS
	iTempProximitySpawning_DynoPropSpawnAnyBS = iTempProximitySpawning_DynoPropSpawnAnyBS | MC_playerBD_1[iPlayer].iProximitySpawning_DynoPropSpawnRangeBS
	iTempProximitySpawning_DynoPropSpawnOneBS = iTempProximitySpawning_DynoPropSpawnOneBS ^ MC_playerBD_1[iPlayer].iProximitySpawning_DynoPropSpawnRangeBS
	iTempProximitySpawning_DynoPropCleanupBS = iTempProximitySpawning_DynoPropCleanupBS | MC_playerBD_1[iPlayer].iProximitySpawning_DynoPropCleanupRangeBS
	
	iTempProximitySpawning_VehSpawnAllBS = iTempProximitySpawning_VehSpawnAllBS & MC_playerBD_1[iPlayer].iProximitySpawning_VehSpawnRangeBS
	iTempProximitySpawning_VehSpawnAnyBS = iTempProximitySpawning_VehSpawnAnyBS | MC_playerBD_1[iPlayer].iProximitySpawning_VehSpawnRangeBS
	iTempProximitySpawning_VehSpawnOneBS = iTempProximitySpawning_VehSpawnOneBS ^ MC_playerBD_1[iPlayer].iProximitySpawning_VehSpawnRangeBS
	iTempProximitySpawning_VehCleanupBS = iTempProximitySpawning_VehCleanupBS | MC_playerBD_1[iPlayer].iProximitySpawning_VehCleanupRangeBS

ENDPROC

#IF IS_DEBUG_BUILD
PROC PROCESS_SERVER_PROXIMITY_SPAWNING_SPOOFING()
	
	INT i
	FOR i = 0 TO ciProxSpawnDebug_MAX_PLAYERS - 1
		
		INT j = 0
		FOR j = 0 TO FMMC_MAX_PEDS_BITSET - 1
			iTempProximitySpawning_PedSpawnAllBS[j] = iTempProximitySpawning_PedSpawnAllBS[j] & sProxSpawnDebug_PlayerOverrideData[i].iProximitySpawning_PedSpawnRangeBS[j]
			iTempProximitySpawning_PedSpawnAnyBS[j] = iTempProximitySpawning_PedSpawnAnyBS[j] | sProxSpawnDebug_PlayerOverrideData[i].iProximitySpawning_PedSpawnRangeBS[j]
			iTempProximitySpawning_PedSpawnOneBS[j] = iTempProximitySpawning_PedSpawnOneBS[j] ^ sProxSpawnDebug_PlayerOverrideData[i].iProximitySpawning_PedSpawnRangeBS[j]
			iTempProximitySpawning_PedCleanupBS[j] = iTempProximitySpawning_PedCleanupBS[j] | sProxSpawnDebug_PlayerOverrideData[i].iProximitySpawning_PedCleanupRangeBS[j]
		ENDFOR
		
		iTempProximitySpawning_DynoPropSpawnAllBS = iTempProximitySpawning_DynoPropSpawnAllBS & sProxSpawnDebug_PlayerOverrideData[i].iProximitySpawning_DynoPropSpawnRangeBS
		iTempProximitySpawning_DynoPropSpawnAnyBS = iTempProximitySpawning_DynoPropSpawnAnyBS | sProxSpawnDebug_PlayerOverrideData[i].iProximitySpawning_DynoPropSpawnRangeBS
		iTempProximitySpawning_DynoPropSpawnOneBS = iTempProximitySpawning_DynoPropSpawnOneBS ^ sProxSpawnDebug_PlayerOverrideData[i].iProximitySpawning_DynoPropSpawnRangeBS
		iTempProximitySpawning_DynoPropCleanupBS = iTempProximitySpawning_DynoPropCleanupBS | sProxSpawnDebug_PlayerOverrideData[i].iProximitySpawning_DynoPropCleanupRangeBS
		
		iTempProximitySpawning_VehSpawnAllBS = iTempProximitySpawning_VehSpawnAllBS & sProxSpawnDebug_PlayerOverrideData[i].iProximitySpawning_VehSpawnRangeBS
		iTempProximitySpawning_VehSpawnAnyBS = iTempProximitySpawning_VehSpawnAnyBS | sProxSpawnDebug_PlayerOverrideData[i].iProximitySpawning_VehSpawnRangeBS
		iTempProximitySpawning_VehSpawnOneBS = iTempProximitySpawning_VehSpawnOneBS ^ sProxSpawnDebug_PlayerOverrideData[i].iProximitySpawning_VehSpawnRangeBS
		iTempProximitySpawning_VehCleanupBS = iTempProximitySpawning_VehCleanupBS | sProxSpawnDebug_PlayerOverrideData[i].iProximitySpawning_VehCleanupRangeBS
	
	ENDFOR

ENDPROC
#ENDIF

PROC PROCESS_SERVER_PROXIMITY_SPAWNING_POST_PLAYER_LOOP()
	
	#IF IS_DEBUG_BUILD
	IF bProxSpawnDebug_EnablePlayerPositionSpoofing
		PROCESS_SERVER_PROXIMITY_SPAWNING_SPOOFING()
	ENDIF
	#ENDIF
	
	INT i
	FOR i = 0 TO FMMC_MAX_PEDS_BITSET - 1
		MC_serverBD_3.iProximitySpawning_PedSpawnAllBS[i] = iTempProximitySpawning_PedSpawnAllBS[i]
		MC_serverBD_3.iProximitySpawning_PedSpawnAnyBS[i] = iTempProximitySpawning_PedSpawnAnyBS[i]
		MC_serverBD_3.iProximitySpawning_PedSpawnOneBS[i] = iTempProximitySpawning_PedSpawnOneBS[i]
		MC_serverBD_3.iProximitySpawning_PedCleanupBS[i] = iTempProximitySpawning_PedCleanupBS[i]
	ENDFOR
	
	MC_serverBD_3.iProximitySpawning_DynoPropSpawnAllBS = iTempProximitySpawning_DynoPropSpawnAllBS
	MC_serverBD_3.iProximitySpawning_DynoPropSpawnAnyBS = iTempProximitySpawning_DynoPropSpawnAnyBS
	MC_serverBD_3.iProximitySpawning_DynoPropSpawnOneBS = iTempProximitySpawning_DynoPropSpawnOneBS
	MC_serverBD_3.iProximitySpawning_DynoPropCleanupBS = iTempProximitySpawning_DynoPropCleanupBS
	
	MC_serverBD_3.iProximitySpawning_VehSpawnAllBS = iTempProximitySpawning_VehSpawnAllBS
	MC_serverBD_3.iProximitySpawning_VehSpawnAnyBS = iTempProximitySpawning_VehSpawnAnyBS
	MC_serverBD_3.iProximitySpawning_VehSpawnOneBS = iTempProximitySpawning_VehSpawnOneBS
	MC_serverBD_3.iProximitySpawning_VehCleanupBS = iTempProximitySpawning_VehCleanupBS

ENDPROC

FUNC BOOL SHOULD_CLEANUP_DUE_TO_PLAYER_PROXIMITY(PROXIMITY_SPAWNING_STRUCT &sProximitySpawningData, INT iEntity, INT &iCleanupRangeBS)
	
	IF sProximitySpawningData.iSpawnRange = 0
		RETURN FALSE
	ENDIF
	
	IF NOT IS_BIT_SET(iCleanupRangeBS, iEntity)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL SHOULD_CLEANUP_DUE_TO_PLAYER_PROXIMITY_ARRAYED(PROXIMITY_SPAWNING_STRUCT &sProximitySpawningData, INT iEntity, INT &iCleanupRangeBS[])
	
	INT iAdjustedEntity = iEntity % 32
	INT iBitsetIndex = iEntity / 32
	RETURN SHOULD_CLEANUP_DUE_TO_PLAYER_PROXIMITY(sProximitySpawningData, iAdjustedEntity, iCleanupRangeBS[iBitsetIndex])
	
ENDFUNC

FUNC BOOL IS_PLAYER_PROXIMITY_BLOCKING_SPAWN(PROXIMITY_SPAWNING_STRUCT &sProximitySpawningData, INT iEntity, INT iSpawnRangeAllBS, INT iSpawnRangeAnyBS, INT iSpawnRangeOneBS, INT iCleanupRangeBS)
	
	IF sProximitySpawningData.iSpawnRange = 0
		RETURN FALSE
	ENDIF
	
	IF SHOULD_CLEANUP_DUE_TO_PLAYER_PROXIMITY(sProximitySpawningData, iEntity, iCleanupRangeBS)
		RETURN TRUE
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF bProxSpawnDebug_ForcePlayersReqAny
		RETURN NOT IS_BIT_SET(iSpawnRangeAnyBS, iEntity)
	ENDIF
	#ENDIF

	SWITCH sProximitySpawningData.iRequiredPlayers
		
		CASE ciPROXIMITY_SPAWNING_REQUIRED_PLAYERS_ALL 			RETURN NOT IS_BIT_SET(iSpawnRangeAllBS, iEntity)
		
		CASE ciPROXIMITY_SPAWNING_REQUIRED_PLAYERS_ANY 			RETURN NOT IS_BIT_SET(iSpawnRangeAnyBS, iEntity)
		
		CASE ciPROXIMITY_SPAWNING_REQUIRED_PLAYERS_MULTIPLE 	
			
			IF IS_BIT_SET(iSpawnRangeAllBS, iEntity)
				RETURN FALSE
			ENDIF
			
			IF IS_BIT_SET(iSpawnRangeOneBS, iEntity)
				RETURN TRUE
			ENDIF
			
			IF IS_BIT_SET(iSpawnRangeAnyBS, iEntity)
				RETURN FALSE
			ENDIF
			
			RETURN TRUE
		BREAK
		
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_PLAYER_PROXIMITY_BLOCKING_SPAWN_ARRAYED(PROXIMITY_SPAWNING_STRUCT &sProximitySpawningData, INT iEntity, INT &iSpawnRangeAllBS[], INT &iSpawnRangeAnyBS[], INT &iSpawnRangeOneBS[], INT &iCleanupRangeBS[])
	
	INT iAdjustedEntity = iEntity % 32
	INT iBitsetIndex = iEntity / 32
	RETURN IS_PLAYER_PROXIMITY_BLOCKING_SPAWN(sProximitySpawningData, iAdjustedEntity, iSpawnRangeAllBS[iBitsetIndex], iSpawnRangeAnyBS[iBitsetIndex], iSpawnRangeOneBS[iBitsetIndex], iCleanupRangeBS[iBitsetIndex])
	
ENDFUNC

PROC PROCESS_CLIENT_PROXIMITY_SPAWNING(PROXIMITY_SPAWNING_STRUCT &sProximitySpawningData, VECTOR vSpawnLocation, INT iEntity, INT &iSpawnRangeBS, INT &iCleanupRangeBS, VECTOR vPlayerPosition #IF IS_DEBUG_BUILD, STRING stPrintTag #ENDIF )			
	
	IF sProximitySpawningData.iSpawnRange = 0
		EXIT
	ENDIF
	
	IF bIsAnySpectator
		EXIT
	ENDIF

	FLOAT fDist2 = VDIST2(vSpawnLocation, vPlayerPosition)
	
	//Check Spawn Range
	IF fDist2 < POW(TO_FLOAT(sProximitySpawningData.iSpawnRange), 2)
		#IF IS_DEBUG_BUILD
		IF NOT IS_BIT_SET(iSpawnRangeBS, iEntity)
			PRINTLN("[PROXSPAWN_CLIENT][", stPrintTag, ": ", iEntity, "] PROCESS_CLIENT_PROXIMITY_SPAWNING - In Spawn Range")
		ENDIF
		#ENDIF
	
		SET_BIT(iSpawnRangeBS, iEntity) //Should spawn
	ELSE
		#IF IS_DEBUG_BUILD
		IF IS_BIT_SET(iSpawnRangeBS, iEntity)
			PRINTLN("[PROXSPAWN_CLIENT][", stPrintTag, ": ", iEntity, "] PROCESS_CLIENT_PROXIMITY_SPAWNING - Out of Spawn Range")
		ENDIF
		#ENDIF
		
		CLEAR_BIT(iSpawnRangeBS, iEntity)
	ENDIF
	
	//Check Cleanup Range
	IF fDist2 < POW(TO_FLOAT(sProximitySpawningData.iCleanupRange), 2)
		#IF IS_DEBUG_BUILD
		IF NOT IS_BIT_SET(iCleanupRangeBS, iEntity)
			PRINTLN("[PROXSPAWN_CLIENT][", stPrintTag, ": ", iEntity, "] PROCESS_CLIENT_PROXIMITY_SPAWNING - In Cleanup Range")
		ENDIF
		#ENDIF
		
		SET_BIT(iCleanupRangeBS, iEntity)
	ELSE
		#IF IS_DEBUG_BUILD
		IF IS_BIT_SET(iCleanupRangeBS, iEntity)
			PRINTLN("[PROXSPAWN_CLIENT][", stPrintTag, ": ", iEntity, "] PROCESS_CLIENT_PROXIMITY_SPAWNING - Out of Cleanup Range")
		ENDIF
		#ENDIF
		
		CLEAR_BIT(iCleanupRangeBS, iEntity) //Should clean up
	ENDIF
	
ENDPROC

PROC PROCESS_CLIENT_PROXIMITY_SPAWNING_ARRAYED(PROXIMITY_SPAWNING_STRUCT &sProximitySpawningData, VECTOR vSpawnLocation, INT iEntity, INT &iSpawnRangeBS[], INT &iCleanupRangeBS[], VECTOR vPlayerPosition #IF IS_DEBUG_BUILD, STRING stPrintTag #ENDIF )			
	
	INT iAdjustedEntity = iEntity % 32
	INT iBitsetIndex = iEntity / 32
	PROCESS_CLIENT_PROXIMITY_SPAWNING(sProximitySpawningData, vSpawnLocation, iAdjustedEntity, iSpawnRangeBS[iBitsetIndex], iCleanupRangeBS[iBitsetIndex], vPlayerPosition #IF IS_DEBUG_BUILD, stPrintTag #ENDIF )
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Target Tagging
// ##### Description: Shared functions for object tagging
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC CONTROL_ACTION GET_CONTROL_FOR_TARGET_TAGGING()
	
	IF IS_SUPPORT_SNIPER_READY_TO_TAG_TARGET()
		RETURN INPUT_ACCURATE_AIM
	ENDIF
	
	RETURN INPUT_VEH_HEADLIGHT
ENDFUNC

FUNC STRING GET_HELP_TEXT_FOR_TARGET_TAGGING()
	IF IS_PLAYER_ABILITY_ACTIVE(PA_SUPPORT_SNIPER)
		RETURN "SNIPER_TAG_HELP_2"
	ENDIF
	
	RETURN "FMMC_TAG_HELP"
ENDFUNC

FUNC BOOL SHOULD_SHOW_TAGGED_TARGET()
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciENABLE_TAGGING_TARGETS)
	OR IS_SUPPORT_SNIPER_AIMING_AT_TAGGED_TARGET()
		RETURN TRUE
	ENDIF
	RETURN FALSE
  ENDFUNC

FUNC BOOL IS_TARGET_TAGGING_ENABLED()
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciENABLE_TAGGING_TARGETS)
	OR IS_SUPPORT_SNIPER_READY_TO_TAG_TARGET()
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_TARGET_TAGGING_ENABLED_THIS_RULE(INT iTeam, INT iRule)
	IF iTeam > -1 AND iTeam < FMMC_MAX_TEAMS
	AND iRule > -1 AND iRule < FMMC_MAX_RULES
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTen[iRule], ciBS_RULE10_ENABLE_TAGGING_THIS_RULE)
		OR IS_SUPPORT_SNIPER_READY_TO_TAG_TARGET()
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Generic Render Targets
// ##### Description: Functions related to the shared render target system which can be used by all entity types.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
FUNC INT GET_SHARED_RENDERTARGET_INDEX_FOR_ENTITY(INT iEntityType = CREATION_TYPE_NONE, INT iEntityIndex = -1)
	INT iSharedRT
	FOR iSharedRT = 0 TO iCurrentSharedRTs - 1
		IF sSharedRTs[iSharedRT].iEntityType = iEntityType
		AND sSharedRTs[iSharedRT].iEntityIndex = iEntityIndex
			RETURN iSharedRT
		ENDIF
	ENDFOR
	
	RETURN -1
ENDFUNC

FUNC INT GET_SHARED_RENDERTARGET_INDEX_FOR_MODEL(MODEL_NAMES mnModelName)
	INT iSharedRT
	FOR iSharedRT = 0 TO iCurrentSharedRTs - 1
		IF sSharedRTs[iSharedRT].mnRenderTargetModel = mnModelName
			RETURN iSharedRT
		ENDIF
	ENDFOR
	
	RETURN -1
ENDFUNC

FUNC BOOL ADD_SHARED_RENDERTARGET(MODEL_NAMES mnEntityModel, INT iEntityType = CREATION_TYPE_NONE, INT iEntityIndex = -1)
	IF iCurrentSharedRTs >= ciSHARED_RT_MAX
		#IF IS_DEBUG_BUILD
		ASSERTLN("[SharedRTs][RT ", iCurrentSharedRTs, "] ADD_SHARED_RENDERTARGET | Too many render targets running!")
		ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_TOO_MANY_RENDER_TARGET_ENTITIES, ENTITY_RUNTIME_ERROR_TYPE_WARNING, "TOO MANY Render Targets set up! Entity Type #, Entity Index # ", iEntityType, iEntityIndex)
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF GET_SHARED_RENDERTARGET_INDEX_FOR_ENTITY(iEntityType, iEntityIndex) > -1
		PRINTLN("[SharedRTs][RT ", iCurrentSharedRTs, "] ADD_SHARED_RENDERTARGET | Blocked adding the same rendertarget for the same entity twice! iEntityType: ", iEntityType, " / iEntityIndex: ", iEntityIndex, "/ mnEntityModel: ", FMMC_GET_MODEL_NAME_FOR_DEBUG(mnEntityModel))
		RETURN FALSE
	ENDIF
	
	#IF IS_DEBUG_BUILD
	PRINTLN("[SharedRTs][RT ", iCurrentSharedRTs, "] ADD_SHARED_RENDERTARGET | Adding new shared rendertarget to the list! Model: ", FMMC_GET_MODEL_NAME_FOR_DEBUG(mnEntityModel), " || iEntityType: ", iEntityType, " / iEntityIndex: ", iEntityIndex)
	#ENDIF
	
	sSharedRTs[iCurrentSharedRTs].iEntityType = iEntityType
	sSharedRTs[iCurrentSharedRTs].iEntityIndex = iEntityIndex
	sSharedRTs[iCurrentSharedRTs].mnRenderTargetModel = mnEntityModel
	
	// Entity-Type Specific Config
	SWITCH iEntityType
		CASE ciENTITY_TYPE_OBJECT
			sSharedRTs[iCurrentSharedRTs].iMatchInteriorDestructionTimerIndex = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iEntityIndex].iInteriorDestructionIndex
		BREAK
	ENDSWITCH
	
	iCurrentSharedRTs++
	RETURN TRUE
ENDFUNC

PROC CLEAN_UP_SHARED_RENDERTARGET(INT iSharedRT)
	
	IF IS_STRING_NULL_OR_EMPTY(sSharedRTs[iSharedRT].sRenderTargetName)
		EXIT
	ENDIF
	
	IF IS_NAMED_RENDERTARGET_REGISTERED(sSharedRTs[iSharedRT].sRenderTargetName)
		PRINTLN("[SharedRTs][RT ", iSharedRT, "] CLEAN_UP_SHARED_RENDERTARGET | Cleaning up rendertarget ", sSharedRTs[iSharedRT].sRenderTargetName)
		RELEASE_NAMED_RENDERTARGET(sSharedRTs[iSharedRT].sRenderTargetName)
	ENDIF
	
	IF NOT IS_STRING_NULL_OR_EMPTY(sSharedRTs[iSharedRT].sTextureDict)
		PRINTLN("[SharedRTs][RT ", iSharedRT, "] CLEAN_UP_SHARED_RENDERTARGET | Releasing texture dict ", sSharedRTs[iSharedRT].sTextureDict)
		SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(sSharedRTs[iSharedRT].sTextureDict)
	ENDIF
	
	IF NOT IS_STRING_NULL_OR_EMPTY(sSharedRTs[iSharedRT].sScaleformName)
		PRINTLN("[SharedRTs][RT ", iSharedRT, "] CLEAN_UP_SHARED_RENDERTARGET | Releasing scaleform ", sSharedRTs[iSharedRT].sScaleformName)
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sSharedRTs[iSharedRT].siScaleformIndex)
	ENDIF
ENDPROC

PROC CLEAN_UP_ALL_SHARED_RENDERTARGETS()
	INT iSharedRT
	FOR iSharedRT = 0 TO iCurrentSharedRTs - 1
		CLEAN_UP_SHARED_RENDERTARGET(iSharedRT)
	ENDFOR
ENDPROC

FUNC FMMC_ENTITY_RENDER_TARGET_OPTIONS GET_ENTITY_RENDER_TARGET_OPTIONS(INT iEntityType, INT iEntityIndex)
	SWITCH iEntityType
		CASE ciENTITY_TYPE_PROPS
			RETURN g_FMMC_STRUCT_ENTITIES.sPlacedProp[iEntityIndex].sPropRenderTargetOptions
		BREAK
		CASE ciENTITY_TYPE_DYNO_PROPS
			RETURN g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iEntityIndex].sDynoPropRenderTargetOptions
		BREAK
		CASE ciENTITY_TYPE_OBJECT
			RETURN g_FMMC_STRUCT_ENTITIES.sPlacedObject[iEntityIndex].sObjectRenderTargetOptions
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
	ASSERTLN("[SharedRTs] GET_ENTITY_RENDER_TARGET_OPTIONS | Creation type ", iEntityType, " doesn't have a struct set up to return!")
	PRINTLN("[SharedRTs] GET_ENTITY_RENDER_TARGET_OPTIONS | Creation type ", iEntityType, " doesn't have a struct set up to return!")
	ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_INCORRECT_INVALID_RENDER_TARGET_OPTIONS, ENTITY_RUNTIME_ERROR_TYPE_WARNING, "Render Targets set up Incorrectly! Entity Type #, Entity Index # ", iEntityType, iEntityIndex)
	#ENDIF
	
	FMMC_ENTITY_RENDER_TARGET_OPTIONS sInvalid
	RETURN sInvalid
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Entity Checkpoint Continuity
// ##### Description: Functions related to to the Entity Checkpoint Continuity system, shared between all entity types.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC INT GET_ENTITY_CHECKPOINT_CONTINUITY_VARS_INDEX_FOR_ENTITY(INT iEntityType, INT iEntityIndex)
	INT iContinuityVarsIndex
	FOR iContinuityVarsIndex = 0 TO g_FMMC_STRUCT_ENTITIES.iRegisteredCheckpointContinuityEntities - 1
	
		IF g_FMMC_STRUCT_ENTITIES.sEntityCheckpointContinuityData[iContinuityVarsIndex].iContinuityEntity_Type != iEntityType
		OR g_FMMC_STRUCT_ENTITIES.sEntityCheckpointContinuityData[iContinuityVarsIndex].iContinuityEntity_Index != iEntityIndex
			// This entity doesn't match these vars
			RELOOP
		ENDIF
		
		RETURN iContinuityVarsIndex
	ENDFOR
	
	RETURN -1
ENDFUNC

FUNC INT GET_VALID_ENTITY_CHECKPOINT_CONTINUITY_VARS_INDEX_FOR_ENTITY(INT iEntityType, INT iEntityIndex)
	INT iContinuityVarsIndex = GET_ENTITY_CHECKPOINT_CONTINUITY_VARS_INDEX_FOR_ENTITY(iEntityType, iEntityIndex)
	
	IF iContinuityVarsIndex > -1
		IF IS_VECTOR_ZERO(g_TransitionSessionNonResetVars.sEntityCheckpointContinuityVars[iContinuityVarsIndex].vContinuityEntity_Position)
			// This slot is invalid! Nothing has been saved yet, don't use it for now
			RETURN -1
		ENDIF
	ENDIF
	
	RETURN iContinuityVarsIndex
ENDFUNC

FUNC BOOL CAN_REGISTER_ENTITY_CHECKPOINT_CONTINUITY_VARS_FOR_THIS_CHECKPOINT(INT iEntityCheckpointContinuityVarsIndex, INT iCheckpointIndex)
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sEntityCheckpointContinuityData[iEntityCheckpointContinuityVarsIndex].iContinuityEntity_IgnoreOnCheckpointBS, iCheckpointIndex)
		PRINTLN("[EntityCheckpointContinuity][ECC ", iEntityCheckpointContinuityVarsIndex, "] CAN_REGISTER_ENTITY_CHECKPOINT_CONTINUITY_VARS_FOR_THIS_CHECKPOINT | Blocking registration for checkpoint ", iCheckpointIndex, " due to iContinuityEntity_IgnoreOnCheckpointBS")
		RETURN FALSE
	ENDIF

	RETURN TRUE
ENDFUNC

PROC CLEAR_ENTITY_CHECKPOINT_CONTINUITY_VARS(INT iEntityCheckpointContinuityVarsIndex)
	g_TransitionSessionNonResetVars.sEntityCheckpointContinuityVars[iEntityCheckpointContinuityVarsIndex].vContinuityEntity_Position = <<0,0,0>>
	g_TransitionSessionNonResetVars.sEntityCheckpointContinuityVars[iEntityCheckpointContinuityVarsIndex].fContinuityEntity_Heading = 0.0
	g_TransitionSessionNonResetVars.sEntityCheckpointContinuityVars[iEntityCheckpointContinuityVarsIndex].iContinuityEntity_Bitset = 0
	PRINTLN("[EntityCheckpointContinuity][ECC ", iEntityCheckpointContinuityVarsIndex, "] CLEAR_ENTITY_CHECKPOINT_CONTINUITY_VARS | Clearing g_TransitionSessionNonResetVars.sEntityCheckpointContinuityVars[", iEntityCheckpointContinuityVarsIndex, "]")
ENDPROC

PROC CLEAR_ALL_ENTITY_CHECKPOINT_CONTINUITY_VARS()
	INT iEntityCheckpointContinuityVarsIndex
	FOR iEntityCheckpointContinuityVarsIndex = 0 TO g_FMMC_STRUCT_ENTITIES.iRegisteredCheckpointContinuityEntities - 1
		CLEAR_ENTITY_CHECKPOINT_CONTINUITY_VARS(iEntityCheckpointContinuityVarsIndex)
	ENDFOR
ENDPROC

PROC REGISTER_ENTITY_CHECKPOINT_CONTINUITY_VARS(INT iIndexToUse, ENTITY_INDEX eiEntity)

	IF NOT DOES_ENTITY_EXIST(eiEntity)
		PRINTLN("[EntityCheckpointContinuity][ECC ", iIndexToUse, "] REGISTER_ENTITY_CHECKPOINT_CONTINUITY_VARS | Entity in slot ", iIndexToUse, " || Doesn't exist!")
		EXIT
	ENDIF
	
	g_TransitionSessionNonResetVars.sEntityCheckpointContinuityVars[iIndexToUse].vContinuityEntity_Position = GET_ENTITY_COORDS(eiEntity, FALSE)
	g_TransitionSessionNonResetVars.sEntityCheckpointContinuityVars[iIndexToUse].fContinuityEntity_Heading = GET_ENTITY_HEADING(eiEntity)
	
	IF IS_ENTITY_AN_OBJECT(eiEntity)
		OBJECT_INDEX oiEntity = GET_OBJECT_INDEX_FROM_ENTITY_INDEX(eiEntity)
		
		IF IS_OBJECT_BROKEN_AND_VISIBLE(oiEntity, TRUE)
		OR IS_ENTITY_DEAD(eiEntity)
			SET_BIT(g_TransitionSessionNonResetVars.sEntityCheckpointContinuityVars[iIndexToUse].iContinuityEntity_Bitset, ciFMMCContinuityEntityBitset_FraggedObject)
			PRINTLN("[EntityCheckpointContinuity][ECC ", iIndexToUse, "] REGISTER_ENTITY_CHECKPOINT_CONTINUITY_VARS | Entity in slot ", iIndexToUse, " is fragged!")
		ELSE
			PRINTLN("[EntityCheckpointContinuity][ECC ", iIndexToUse, "] REGISTER_ENTITY_CHECKPOINT_CONTINUITY_VARS | Entity in slot ", iIndexToUse, " is NOT broken!")
		ENDIF
	ENDIF
	
	PRINTLN("[EntityCheckpointContinuity][ECC ", iIndexToUse, "] REGISTER_ENTITY_CHECKPOINT_CONTINUITY_VARS | Updating registered vars for entity in slot ", iIndexToUse, " || vContinuityEntity_Position: ", g_TransitionSessionNonResetVars.sEntityCheckpointContinuityVars[iIndexToUse].vContinuityEntity_Position, " | fContinuityEntity_Heading: ", g_TransitionSessionNonResetVars.sEntityCheckpointContinuityVars[iIndexToUse].fContinuityEntity_Heading, " | iContinuityEntity_Bitset: ", g_TransitionSessionNonResetVars.sEntityCheckpointContinuityVars[iIndexToUse].iContinuityEntity_Bitset)
ENDPROC

FUNC VECTOR GET_COORDS_FOR_ENTITY_CHECKPOINT_CONTINUITY(INT iEntityType, INT iEntityIndex)
	INT iIndex = GET_ENTITY_CHECKPOINT_CONTINUITY_VARS_INDEX_FOR_ENTITY(iEntityType, iEntityIndex)
	
	IF iIndex > -1
	AND NOT IS_VECTOR_ZERO(g_TransitionSessionNonResetVars.sEntityCheckpointContinuityVars[iIndex].vContinuityEntity_Position)
		PRINTLN("[EntityCheckpointContinuity_SPAM][ECC_SPAM ", iIndex, "] Using checkpoint continuity coords: ", g_TransitionSessionNonResetVars.sEntityCheckpointContinuityVars[iIndex].vContinuityEntity_Position)
		RETURN g_TransitionSessionNonResetVars.sEntityCheckpointContinuityVars[iIndex].vContinuityEntity_Position
	ENDIF
	
	RETURN <<0,0,0>>
ENDFUNC

FUNC FLOAT GET_HEADING_FOR_ENTITY_CHECKPOINT_CONTINUITY(INT iEntityType, INT iEntityIndex)
	INT iIndex = GET_ENTITY_CHECKPOINT_CONTINUITY_VARS_INDEX_FOR_ENTITY(iEntityType, iEntityIndex)
	
	IF iIndex > -1
		PRINTLN("[EntityCheckpointContinuity_SPAM][ECC_SPAM ", iIndex, "] Using checkpoint continuity heading: ", g_TransitionSessionNonResetVars.sEntityCheckpointContinuityVars[iIndex].fContinuityEntity_Heading)
		RETURN g_TransitionSessionNonResetVars.sEntityCheckpointContinuityVars[iIndex].fContinuityEntity_Heading
	ENDIF
	
	RETURN -999.9
ENDFUNC

FUNC BOOL IS_BIT_SET_FOR_ENTITY_CHECKPOINT_CONTINUITY(INT iEntityType, INT iEntityIndex, INT iBit)
	INT iIndex = GET_ENTITY_CHECKPOINT_CONTINUITY_VARS_INDEX_FOR_ENTITY(iEntityType, iEntityIndex)
	
	IF iIndex > -1
		PRINTLN("[EntityCheckpointContinuity_SPAM][ECC_SPAM ", iIndex, "] Checking checkpoint continuity bitset: ", g_TransitionSessionNonResetVars.sEntityCheckpointContinuityVars[iIndex].iContinuityEntity_Bitset)
		RETURN IS_BIT_SET(g_TransitionSessionNonResetVars.sEntityCheckpointContinuityVars[iIndex].iContinuityEntity_Bitset, iBit)
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_ENTITY_USE_CHECKPOINT_CONTINUITY_VARS(INT iEntityType, INT iEntityIndex)
	IF IS_VECTOR_ZERO(GET_COORDS_FOR_ENTITY_CHECKPOINT_CONTINUITY(iEntityType, iEntityIndex))
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC CHECK_SHOULD_SAVE_ENTITY_CHECKPOINT_CONTINUITY_THIS_FRAME(INT iCheckpoint)
	IF IS_BIT_SET(MC_ServerBD.iCheckpointBitset, iCheckpoint)
	AND NOT IS_BIT_SET(iEntityCheckpointContinuityProcessedCheckpointBS, iCheckpoint)
		SET_BIT(iLocalBoolCheck34, LBOOL34_REGISTER_ENTITY_CHECKPOINT_CONTINUITY_THIS_FRAME)
		SET_BIT(iEntityCheckpointContinuityProcessedCheckpointBS, iCheckpoint)
		PRINTLN("[EntityCheckpointContinuity] Registering for checkpoint ", iCheckpoint)
	ENDIF
ENDPROC

PROC PROCESS_ENTITY_CHECKPOINT_CONTINUITY_PRE_EVERY_FRAME_CLIENT()
	
	// Checkpoint 
	CHECK_SHOULD_SAVE_ENTITY_CHECKPOINT_CONTINUITY_THIS_FRAME(SB_CHECKPOINT_1)
	CHECK_SHOULD_SAVE_ENTITY_CHECKPOINT_CONTINUITY_THIS_FRAME(SB_CHECKPOINT_2)
	CHECK_SHOULD_SAVE_ENTITY_CHECKPOINT_CONTINUITY_THIS_FRAME(SB_CHECKPOINT_3)
	CHECK_SHOULD_SAVE_ENTITY_CHECKPOINT_CONTINUITY_THIS_FRAME(SB_CHECKPOINT_4)
	
	#IF IS_DEBUG_BUILD
	IF bEntityCheckpointContinuityDebug
		INT iContinuityVarsIndex
		VECTOR vDebugPos = <<0.25, 0.6, 0.0>>
		FOR iContinuityVarsIndex = 0 TO g_FMMC_STRUCT_ENTITIES.iRegisteredCheckpointContinuityEntities - 1
		
			TEXT_LABEL_63 tlVisualDebug = "Cont Ent "
			tlVisualDebug += iContinuityVarsIndex
			
			tlVisualDebug += " | Type: "
			tlVisualDebug += g_FMMC_STRUCT_ENTITIES.sEntityCheckpointContinuityData[iContinuityVarsIndex].iContinuityEntity_Type
			
			tlVisualDebug += " | Index: "
			tlVisualDebug += g_FMMC_STRUCT_ENTITIES.sEntityCheckpointContinuityData[iContinuityVarsIndex].iContinuityEntity_Index
			
			DRAW_DEBUG_TEXT_2D(tlVisualDebug, (vDebugPos), 255, 255, 255, 255)
			
			IF NOT IS_VECTOR_ZERO(g_TransitionSessionNonResetVars.sEntityCheckpointContinuityVars[iContinuityVarsIndex].vContinuityEntity_Position)
				TEXT_LABEL_63 tlSavedPos = " || Saved Pos: "
				tlSavedPos += FLOAT_TO_STRING(g_TransitionSessionNonResetVars.sEntityCheckpointContinuityVars[iContinuityVarsIndex].vContinuityEntity_Position.x)
				tlSavedPos += ", "
				tlSavedPos += FLOAT_TO_STRING(g_TransitionSessionNonResetVars.sEntityCheckpointContinuityVars[iContinuityVarsIndex].vContinuityEntity_Position.y)
				tlSavedPos += ", "
				tlSavedPos += FLOAT_TO_STRING(g_TransitionSessionNonResetVars.sEntityCheckpointContinuityVars[iContinuityVarsIndex].vContinuityEntity_Position.z)
				DRAW_DEBUG_TEXT_2D(tlSavedPos, vDebugPos + (<<0.35, 0.0, 0.0>>), 255, 255, 255, 255)
			ENDIF

			vDebugPos.y += 0.04
		ENDFOR
	ENDIF
	#ENDIF
	
ENDPROC

PROC PROCESS_ENTITY_CHECKPOINT_CONTINUITY_EVERY_FRAME_CLIENT()
	BOOL bShouldRegisterNow = IS_BIT_SET(iLocalBoolCheck34, LBOOL34_REGISTER_ENTITY_CHECKPOINT_CONTINUITY_THIS_FRAME)
	
	IF bShouldRegisterNow
		INT iContinuityVarsIndex
		FOR iContinuityVarsIndex = 0 TO g_FMMC_STRUCT_ENTITIES.iRegisteredCheckpointContinuityEntities - 1
			INT iEntityType = g_FMMC_STRUCT_ENTITIES.sEntityCheckpointContinuityData[iContinuityVarsIndex].iContinuityEntity_Type
			INT iEntityIndex = g_FMMC_STRUCT_ENTITIES.sEntityCheckpointContinuityData[iContinuityVarsIndex].iContinuityEntity_Index
			ENTITY_INDEX eiEntity = GET_FMMC_ENTITY(iEntityType, iEntityIndex)
			
			IF CAN_REGISTER_ENTITY_CHECKPOINT_CONTINUITY_VARS_FOR_THIS_CHECKPOINT(iContinuityVarsIndex, GET_CURRENT_CHECKPOINT_INDEX())
				REGISTER_ENTITY_CHECKPOINT_CONTINUITY_VARS(iContinuityVarsIndex, eiEntity)
			ENDIF
		ENDFOR
		
		CLEAR_BIT(iLocalBoolCheck34, LBOOL34_REGISTER_ENTITY_CHECKPOINT_CONTINUITY_THIS_FRAME)
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: External Cutscenes
// ##### Description: Anim/Cutscene related helper functions which are used in custom sequences
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC STRING GET_EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_STRING(EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE eState)
	
	IF eState = EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_IDLE			RETURN "EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_IDLE"			ENDIF
	IF eState = EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_LOAD			RETURN "EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_LOAD"			ENDIF
	IF eState = EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_SETUP			RETURN "EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_SETUP"		ENDIF
	IF eState = EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_MAINTAIN		RETURN "EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_MAINTAIN"		ENDIF
	IF eState = EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_CLEANUP		RETURN "EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_CLEANUP"		ENDIF
	
	RETURN ""
ENDFUNC

PROC SET_EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE(EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE eNewState, EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE &eState)
	PRINTLN("[SET_EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE] - Current State is: ", GET_EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_STRING(eState), " Moving to >>>>> ", GET_EXTERNAL_SCRIPTED_CUTSCENE_SEQUENCE_STATE_STRING(eNewState))
	eState = eNewState
ENDPROC 

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Awards
// ##### Description: Functions for settign and giving awards
// ##### Anything content specific should be in the relevant _Rewards header
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL PROCESS_GIVING_FMMC_CHARACTER_AWARD(MP_BOOL_AWARD eAward)
	
	IF GET_MP_BOOL_CHARACTER_AWARD(eAward)
		PRINTLN("[Rewards] - PROCESS_GIVING_FMMC_CHARACTER_AWARD - Already Achieved Award: ", eAward)
		RETURN FALSE
	ENDIF
	
	PRINTLN("[Rewards] - PROCESS_GIVING_FMMC_CHARACTER_AWARD - Setting Award Achieved: ", eAward)
	SET_MP_BOOL_CHARACTER_AWARD(eAward, TRUE)
	
	RETURN TRUE
	
ENDFUNC



// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Player Ped Model Swapping
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL SHOULD_PLAYER_PED_MODEL_BE_SWAPPED(INT iTeam = -1, INT iModelSelected = -1)
	
	IF iTeam = -1
		iTeam = GET_LOCAL_PLAYER_TEAM()
	ENDIF
	
	IF iModelSelected = FMMC_PLAYER_PED_MODELS_CLEAR
		PRINTLN("[Player Model Swap] SHOULD_PLAYER_PED_MODEL_BE_SWAPPED - Returning FALSE but clearing so actually going to transform")
		RETURN FALSE
	ENDIF
	
	IF iModelSelected > FMMC_PLAYER_PED_MODELS_OFF
	AND iModelSelected < FMMC_PLAYER_PED_MODELS_MAX
		RETURN g_FMMC_STRUCT.sTeamData[iTeam].iPlayerPedModels[iModelSelected] != FMMC_PLAYER_PED_MODELS_OFF
	ENDIF
	
	INT iPartOnTeam = GET_PARTICIPANT_NUMBER_IN_TEAM()
	IF iPartOnTeam >= FMMC_PLAYER_PED_MODELS_MAX
		PRINTLN("[Player Model Swap] SHOULD_PLAYER_PED_MODEL_BE_SWAPPED - iPartOnTeam out of range")
		RETURN FALSE
	ENDIF

	RETURN IS_BIT_SET(g_FMMC_STRUCT.sTeamData[iTeam].iTeamBitSet, ciTEAM_BS_SwapPlayerPed)
	AND g_FMMC_STRUCT.sTeamData[iTeam].iPlayerPedModels[iPartOnTeam] != FMMC_PLAYER_PED_MODELS_OFF
ENDFUNC

FUNC MODEL_NAMES GET_FMMC_PLAYER_PED_MODEL(INT iTeam = -1, BOOL bSetPlayerBD = FALSE, INT iModelSelected = FMMC_PLAYER_PED_MODELS_OFF)
	
	IF iTeam = -1
		iTeam = GET_LOCAL_PLAYER_TEAM()
	ENDIF
	
	IF SHOULD_PLAYER_PED_MODEL_BE_SWAPPED(iTeam, iModelSelected)
		
		INT iModelIndex
		IF iModelSelected != FMMC_PLAYER_PED_MODELS_OFF
			iModelIndex = iModelSelected
			PRINTLN("[Player Model Swap] GET_FMMC_PLAYER_PED_MODEL - Using iModelSelected = ", iModelIndex)
		ELSE
			iModelIndex = GET_LOCAL_PLAYER_MODEL_SWAP_INDEX()
			PRINTLN("[Player Model Swap] GET_FMMC_PLAYER_PED_MODEL - GET_LOCAL_PLAYER_MODEL_SWAP_INDEX = ", iModelIndex)
			IF iModelIndex = FMMC_PLAYER_PED_MODELS_OFF
			OR NOT IS_THIS_THE_FIRST_ROUND_OR_NOT_A_ROUNDS_MISSION()
				iModelIndex = GET_PARTICIPANT_NUMBER_IN_TEAM()
				PRINTLN("[Player Model Swap] GET_FMMC_PLAYER_PED_MODEL - GET_PARTICIPANT_NUMBER_IN_TEAM = ", iModelIndex)
			ENDIF
		ENDIF
		
		IF bSetPlayerBD
			MC_playerBD[iLocalPart].iCustomPedModelIndex = iModelIndex
			PRINTLN("[Player Model Swap] GET_FMMC_PLAYER_PED_MODEL - iCustomPedModelIndex set to ", MC_playerBD[iLocalPart].iCustomPedModelIndex)
		ENDIF
		
		RETURN FMMC_GET_PLAYER_PED_MODEL_TO_CHANGE_TO(g_FMMC_STRUCT.sTeamData[iTeam].iPlayerPedModels[iModelIndex])
	#IF IS_DEBUG_BUILD
	ELSE
		PRINTLN("[Player Model Swap] GET_FMMC_PLAYER_PED_MODEL - SHOULD_PLAYER_PED_MODEL_BE_SWAPPED returned FALSE")
	#ENDIF
	ENDIF
		
	RETURN GET_PLAYER_MODEL()
ENDFUNC

FUNC INT GET_FMMC_PLAYER_PED_MODEL_HUD_COLOUR(MODEL_NAMES mnPedModel)
	IF mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("P_Franklin_02"))
	OR mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("IG_LamarDavis_02"))
		RETURN ENUM_TO_INT(HUD_COLOUR_FRANKLIN)
	ENDIF
	
	RETURN -1
ENDFUNC

PROC SET_UP_PED_MODEL_SWAP_EXTRAS(MODEL_NAMES mnPedModel)
	
	IF NOT IS_USING_CUSTOM_PLAYER_MODEL()
		EXIT
	ENDIF
	
	IF mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("P_Franklin_02"))
		g_bUseFranklinThemeInMP = TRUE
		PRINTLN("[Player Model Swap] SET_UP_PED_MODEL_SWAP_EXTRAS - Setting g_bUseFranklinThemeInMP to TRUE")
	ELIF mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("IG_LamarDavis_02"))
		g_bUseLamarThemeInMP = TRUE
		PRINTLN("[Player Model Swap] SET_UP_PED_MODEL_SWAP_EXTRAS - Setting g_bUseLamarThemeInMP to TRUE")
	ENDIF
	
ENDPROC

PROC SET_PLAYER_MODEL_SWAP_DATA(MODEL_NAMES mnSwapTo)
	
	IF GET_PLAYER_MODEL() != mnSwapTo
		IF mnPlayerModelSwapFrom = DUMMY_MODEL_FOR_SCRIPT
			mnPlayerModelSwapFrom = GET_PLAYER_MODEL()
		ELIF mnPlayerModelSwapFrom = mnSwapTo
			mnPlayerModelSwapFrom = DUMMY_MODEL_FOR_SCRIPT
		ENDIF
		
		mnPlayerModelToSwapTo = mnSwapTo
		PRINTLN("[Player Model Swap] SET_PLAYER_MODEL_SWAP_DATA - mnSwapTo: ", FMMC_GET_MODEL_NAME_FOR_DEBUG(mnPlayerModelToSwapTo), " mnPlayerModelSwapFrom: ", FMMC_GET_MODEL_NAME_FOR_DEBUG(mnPlayerModelSwapFrom))
	ENDIF
	
ENDPROC

PROC START_PLAYER_MODEL_SWAP(INT iTeam, INT iModelToSwapTo)
	
	IF NOT IS_TEAM_VALID(iTeam)
		EXIT
	ENDIF
	
	IF iModelToSwapTo = FMMC_PLAYER_PED_MODELS_OFF
		EXIT
	ENDIF
	
	MODEL_NAMES mnSwapModel = GET_FMMC_PLAYER_PED_MODEL(iTeam, TRUE, iModelToSwapTo)
	
	IF iModelToSwapTo = FMMC_PLAYER_PED_MODELS_CLEAR
		mnSwapModel = mnPlayerModelSwapFrom
		PRINTLN("[Player Model Swap] START_PLAYER_MODEL_SWAP - Setting mnSwapModel to mnPlayerModelSwapFrom")
	ENDIF
	SET_PLAYER_MODEL_SWAP_DATA(mnSwapModel)
	PRINTLN("[Player Model Swap] START_PLAYER_MODEL_SWAP - mnPlayerModelToSwapTo set")
	DEBUG_PRINTCALLSTACK()
	
ENDPROC

PROC PROCESS_PLAYER_MODEL_SWAP_ON_RULE_CHANGE(INT iTeam, INT iRule)
	
	IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sOnRuleStart[iRule].iPlayerModelToSwapTo = FMMC_PLAYER_PED_MODELS_OFF
	AND mnPlayerModelSwapFrom = DUMMY_MODEL_FOR_SCRIPT
		EXIT
	ENDIF
	
	PRINTLN("[Player Model Swap] PROCESS_PLAYER_MODEL_SWAP_ON_RULE_CHANGE - g_FMMC_STRUCT.sFMMCEndConditions[",iTeam,"].sOnRuleStart[",iRule,"].iPlayerModelToSwapTo: ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sOnRuleStart[iRule].iPlayerModelToSwapTo)
	START_PLAYER_MODEL_SWAP(iTeam, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sOnRuleStart[iRule].iPlayerModelToSwapTo)
	
ENDPROC

FUNC BOOL IS_PLAYER_MODEL_FREEMODE_PED(MODEL_NAMES mnPlayerModel)
	IF mnPlayerModel = MP_F_FREEMODE_01
	OR mnPlayerModel = MP_M_FREEMODE_01
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SWAP_PLAYER_MODEL_BACK_TO_NORMAL()
	IF NOT (GET_PLAYER_MODEL() = g_TransitionSpawnData.StoredModelName)
		REQUEST_MODEL(g_TransitionSpawnData.StoredModelName)
		IF HAS_MODEL_LOADED(g_TransitionSpawnData.StoredModelName)					
			PRINTLN("[Player Model Swap] SWAP_PLAYER_BACK_TO_NORMAL - setting model.")					
			RestorePlayerModelFromCustom()
			SET_PLAYER_CLOTHES_FOR_RETURN_TO_FREEMODE()
			LocalPlayerPed = PLAYER_PED_ID()
			LocalPlayer = PLAYER_ID()
		ELSE
			PRINTLN("[Player Model Swap] SWAP_PLAYER_MODEL_BACK_TO_NORMAL - waiting on model to load")
		ENDIF
	ELSE
	
		IF NOT HAS_PED_HEAD_BLEND_FINISHED(PLAYER_PED_ID())
		OR NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(PLAYER_PED_ID())
			PRINTLN("[Player Model Swap] SWAP_PLAYER_MODEL_BACK_TO_NORMAL - waiting on HAS_PED_HEAD_BLEND_FINISHED ")
		ELSE				
			PRINTLN("[Player Model Swap] SWAP_PLAYER_MODEL_BACK_TO_NORMAL - calling NET_SPAWN_PLAYER")
			SETUP_PLAYER_FROM_STATS()
			g_TransitionSpawnData.StoredModelName = DUMMY_MODEL_FOR_SCRIPT
			g_TransitionSpawnData.CustomModelName = DUMMY_MODEL_FOR_SCRIPT
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SET_MODEL_SWAP_STATE(INT iNewState)
	IF iModelSwapState != iNewState
		iModelSwapState = iNewState
		PRINTLN("[Player Model Swap] SET_MODEL_SWAP_STATE - Moved to state ", iModelSwapState)
	ENDIF
ENDPROC

PROC PROCESS_PLAYER_MODEL_SWAP_RETURN_TO_PLAYER_SETUP()
	//Outfit
	iLastOutfit = FMMC_RULE_OUTFIT_DEFAULT
	MC_playerBD[iLocalPart].iOutfit = FMMC_RULE_OUTFIT_DEFAULT
	SET_BIT(iLocalBoolCheck29, LBOOL29_RULE_OUTFIT_CHANGE_REQUEST)
	PRINTLN("[Outfits][Player Model Swap] PROCESS_PLAYER_MODEL_SWAP_RETURN_TO_PLAYER_SETUP - Clearing LBOOL29_RULE_OUTFIT_CHANGE_REQUEST and iRuleOutfit")
	
	//Weapons
	INT iTeam = GET_LOCAL_PLAYER_TEAM()
	IF g_FMMC_STRUCT.sTeamData[iTeam].iMidMissionInventoryIndex != -1
		PRINTLN("[Player Model Swap] PROCESS_PLAYER_MODEL_SWAP_RETURN_TO_PLAYER_SETUP - Giving mid mission inventory")
		GIVE_LOCAL_PLAYER_WEAPON_INVENTORY(FMMC_INVENTORY_MID_MISSION, WEAPONINHAND_LASTWEAPON_BOTH)
	ELIF g_FMMC_STRUCT.sTeamData[iTeam].iMidMissionInventory2Index != -1
		PRINTLN("[Player Model Swap] PROCESS_PLAYER_MODEL_SWAP_RETURN_TO_PLAYER_SETUP - Giving mid mission 2 inventory")
		GIVE_LOCAL_PLAYER_WEAPON_INVENTORY(FMMC_INVENTORY_MID_MISSION_2, WEAPONINHAND_LASTWEAPON_BOTH)
	ELIF g_FMMC_STRUCT.sTeamData[iTeam].iStartingInventoryIndex != -1
		PRINTLN("[Player Model Swap] PROCESS_PLAYER_MODEL_SWAP_RETURN_TO_PLAYER_SETUP - Giving starting inventory")
		GIVE_LOCAL_PLAYER_WEAPON_INVENTORY(FMMC_INVENTORY_STARTING, WEAPONINHAND_LASTWEAPON_BOTH)
	ELSE
		PRINTLN("[Player Model Swap] PROCESS_PLAYER_MODEL_SWAP_RETURN_TO_PLAYER_SETUP - Giving freemode inventory")
		GIVE_MP_REWARD_WEAPON_IN_FREEMODE(TRUE, FALSE, WEAPONINHAND_FIRSTSPAWN_HOLSTERED, FALSE, FALSE, FALSE, TRUE)
		g_GIVE_PLAYER_ALL_UNLOCKED_AND_EQUIPED_WEAPONS_FM_CHECK = TRUE
	ENDIF
	
	EQUIP_TORSO_DECALS_MP(LocalPlayerPed, COMP_TYPE_JBIB, GET_PED_COMPONENT_ITEM_CURRENT_FROM_LOOKUP(LocalPlayerPed, COMP_TYPE_JBIB), FALSE)
	UPDATE_TATOOS_MP(LocalPlayerPed)
		
	SET_PLAYER_IS_IN_ANIMAL_FORM(FALSE)
ENDPROC

PROC PROCESS_MID_GAME_PLAYER_MODEL_SWAPS()	
	
	SWITCH iModelSwapState
		CASE ciModelSwap_WAIT
			IF mnPlayerModelToSwapTo != DUMMY_MODEL_FOR_SCRIPT
				SET_MODEL_SWAP_STATE(ciModelSwap_LOAD)
			ENDIF
		BREAK
		CASE ciModelSwap_LOAD
			REQUEST_MODEL(mnPlayerModelToSwapTo)
			IF HAS_MODEL_LOADED(mnPlayerModelToSwapTo)
				SET_MODEL_SWAP_STATE(ciModelSwap_PTFX)
			ENDIF
		BREAK
		CASE ciModelSwap_PTFX
		
			REQUEST_NAMED_PTFX_ASSET("scr_rcbarry2")
			IF HAS_NAMED_PTFX_ASSET_LOADED("scr_rcbarry2")
				USE_PARTICLE_FX_ASSET("scr_rcbarry2")
				IF START_NETWORKED_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_clown_death", GET_ENTITY_COORDS(LocalPlayerPed, FALSE), GET_ENTITY_ROTATION(LocalPlayerPed), 1.0)
					IF NOT IS_PLAYER_MODEL_FREEMODE_PED(mnPlayerModelToSwapTo)
						SET_MODEL_SWAP_STATE(ciModelSwap_NEW_MODEL)
					ELSE
						SET_MODEL_SWAP_STATE(ciModelSwap_FREEMODE_MODEL)
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
		CASE ciModelSwap_NEW_MODEL
		
			SET_CUSTOM_PLAYER_MODEL(mnPlayerModelToSwapTo, TRUE)
			LocalPlayerPed = PLAYER_PED_ID()
			LocalPlayer = PLAYER_ID()
		
			INT iVariation 
			iVariation = GET_FMMC_PLAYER_PED_MODEL_VARIATION(mnPlayerModelToSwapTo)
			IF iVariation != -1
				FMMC_SET_PED_VARIATION(localPlayerPed, mnPlayerModelToSwapTo, iVariation)
			ENDIF
			
			IF IS_MODEL_AN_ANIMAL(mnPlayerModelToSwapTo)
				SET_PLAYER_IS_IN_ANIMAL_FORM(TRUE)
			ENDIF
			
			IF IS_RULE_INDEX_VALID(GET_LOCAL_PLAYER_CURRENT_RULE())
				SET_PREREQUISITE_COMPLETED(g_FMMC_STRUCT.sFMMCEndConditions[GET_LOCAL_PLAYER_TEAM()].sRulePlayerModelSwap[GET_LOCAL_PLAYER_CURRENT_RULE()].iPlayerModelSwapPrereqToComplete)
			ENDIF
			
			IF GET_PLAYER_MODEL() = mnPlayerModelToSwapTo
				SET_MODEL_SWAP_STATE(ciModelSwap_CLEANUP)
			ENDIF
			
		BREAK
		CASE ciModelSwap_FREEMODE_MODEL
		
			IF SWAP_PLAYER_MODEL_BACK_TO_NORMAL()
				PRINTLN("[Player Model Swap] PROCESS_MID_GAME_PLAYER_MODEL_SWAPS - Returned to local player")
				
				IF IS_RULE_INDEX_VALID(GET_LOCAL_PLAYER_CURRENT_RULE())
					RESET_PREREQUISITE_COMPLETED(g_FMMC_STRUCT.sFMMCEndConditions[GET_LOCAL_PLAYER_TEAM()].sRulePlayerModelSwap[GET_LOCAL_PLAYER_CURRENT_RULE()].iPlayerModelSwapPrereqToComplete)
				ENDIF
				
				SET_MODEL_SWAP_STATE(ciModelSwap_FREEMODE_RESTORE)
			ELSE
				PRINTLN("[Player Model Swap] PROCESS_MID_GAME_PLAYER_MODEL_SWAPS - bClearModelSwap = FALSE")
			ENDIF
			
		BREAK
		CASE ciModelSwap_FREEMODE_RESTORE
			
			PROCESS_PLAYER_MODEL_SWAP_RETURN_TO_PLAYER_SETUP()
			
			SET_MODEL_SWAP_STATE(ciModelSwap_CLEANUP)
		BREAK
		CASE ciModelSwap_CLEANUP
			mnPlayerModelToSwapTo = DUMMY_MODEL_FOR_SCRIPT
			SET_MODEL_SWAP_STATE(ciModelSwap_WAIT)
		BREAK
	ENDSWITCH
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Detonate Bomb Contact
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC REMOVE_DETONATE_BOMB_CONTACT()
	REMOVE_CONTACT_FROM_INDIVIDUAL_PHONEBOOK(CHAR_MP_DETONATEPHONE, MULTIPLAYER_BOOK)
	HANG_UP_AND_PUT_AWAY_PHONE(FALSE)
	eDetonateOrigin = DETONATE_BOMB_INVALID
ENDPROC

PROC ADD_DETONATE_BOMB_CONTACT(DETONATE_BOMB_ORIGIN_ENUM eOrigin)
	ADD_CONTACT_TO_PHONEBOOK(CHAR_MP_DETONATEPHONE, MULTIPLAYER_BOOK, TRUE)
	MAKE_CONTACT_ENTRY_PRIORITY(CHAR_MP_DETONATEPHONE)
	eDetonateOrigin = eOrigin
	PRINTLN("ADD_DETONATE_BOMB_CONTACT - eDetonateOrigin = ", eDetonateOrigin)
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Scripted Anim Conversations
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC TEXT_LABEL_23 GET_CURRENT_PLAYING_CONVERSATION()
	RETURN g_ConversationData.ConversationSegmentToGrab
ENDFUNC

FUNC STRING GET_CURRENT_PLAYING_CONVERSATION_STRING()
	STRING sResult = TEXT_LABEL_TO_STRING(g_ConversationData.ConversationSegmentToGrab)
	RETURN sResult
ENDFUNC

/// PURPOSE:
///    Returns TRUE if the given root is the one active in the conversation script system & a scripted conversation is playing
/// PARAMS:
///    tlRoot - 
/// RETURNS:
///    
FUNC BOOL IS_THIS_CONVERSATION_ROOT_PLAYING(STRING sRoot)

	IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
		RETURN FALSE
	ENDIF
	
	RETURN ARE_STRINGS_EQUAL(g_ConversationData.ConversationSegmentToGrab, sRoot)
ENDFUNC

FUNC BOOL IS_SCRIPTED_ANIM_CONVERSATION_IN_USE()
	RETURN iScriptedAnimConversationState != ciScriptedAnimConversationState_Off
ENDFUNC

FUNC BOOL IS_SCRIPTED_ANIM_CONVERSATION_WAITING_TO_START()
	RETURN iScriptedAnimConversationState = ciScriptedAnimConversationState_WaitingToStart
ENDFUNC

FUNC BOOL IS_THIS_SCRIPTED_ANIM_CONVERSATION_IN_USE(STRING sRoot)
	RETURN ARE_STRINGS_EQUAL(sCurrentScriptedAnimConversationData.tl23_Root, sRoot)
ENDFUNC

FUNC BOOL IS_THIS_SCRIPTED_ANIM_CONVERSATION_WAITING_TO_START(STRING sRoot)
	RETURN IS_THIS_SCRIPTED_ANIM_CONVERSATION_IN_USE(sRoot)
ENDFUNC


FUNC BOOL IS_SCRIPTED_ANIM_CONVERSATION_PLAYING()
	RETURN iScriptedAnimConversationState = ciScriptedAnimConversationState_ConversationPlaying
ENDFUNC

FUNC BOOL IS_SCRIPTED_ANIM_CONVERSATION_PAUSED()
	RETURN iScriptedAnimConversationState = ciScriptedAnimConversationState_PaussedConversation
ENDFUNC

PROC SET_SCRIPTED_ANIM_CONVERSATION_STATE(INT iNewState)
	iScriptedAnimConversationState = iNewState
	PRINTLN("[ScriptedAnimConversation] SET_SCRIPTED_ANIM_CONVERSATION_STATE - State is now ", iScriptedAnimConversationState)		
ENDPROC

PROC CLEAR_SCRIPTED_ANIM_CONVERSATION_PED_DATA(FMMC_SCRIPTED_ANIM_CONVERSATION_PED_DATA &sScriptedAnimConversationPedDataToClear)
	sScriptedAnimConversationPedDataToClear.iConversationPedIndex = -1
	sScriptedAnimConversationPedDataToClear.iConversationParticipantIndex = -1
	sScriptedAnimConversationPedDataToClear.iSpeakerID = -1
	sScriptedAnimConversationPedDataToClear.tl23_VoiceName = ""
	sScriptedAnimConversationPedDataToClear.bPedPlayAmbientAnims = FALSE
	sScriptedAnimConversationPedDataToClear.bPedCanUseAutoLookAt = TRUE
ENDPROC

PROC CLEAR_SCRIPTED_ANIM_CONVERSATION(FMMC_SCRIPTED_ANIM_CONVERSATION_DATA &sScriptedAnimConversationToClear)
	
	PRINTLN("[ScriptedAnimConversation] CLEAR_SCRIPTED_ANIM_CONVERSATION - Clearing scripted anim conversation now!")
	DEBUG_PRINTCALLSTACK()
	
	INT iConversationPed
	FOR iConversationPed = 0 TO ciScriptedAnimConversation_MaxPeds - 1
		CLEAR_SCRIPTED_ANIM_CONVERSATION_PED_DATA(sScriptedAnimConversationToClear.sConversationPedData[iConversationPed])
	ENDFOR
	
	sScriptedAnimConversationToClear.tl23_Block = ""
	sScriptedAnimConversationToClear.tl23_Root = ""
	sScriptedAnimConversationToClear.ePriority = CONV_PRIORITY_NONE
	sScriptedAnimConversationToClear.iBS = 0
	
	SET_SCRIPTED_ANIM_CONVERSATION_STATE(ciScriptedAnimConversationState_Off)
ENDPROC

PROC FMMC_SET_UP_SCRIPTED_ANIM_CONVERSATION(STRING sConversationTextBlock, STRING sConversationRoot, enumConversationPriority ePriority = CONV_PRIORITY_AMBIENT_MEDIUM, INT iBS = 0)

	IF IS_THIS_SCRIPTED_ANIM_CONVERSATION_IN_USE(sConversationRoot)
		// Already set up!
		EXIT
	ENDIF
	
	IF IS_SCRIPTED_ANIM_CONVERSATION_IN_USE()
		CLEAR_SCRIPTED_ANIM_CONVERSATION(sCurrentScriptedAnimConversationData)
	ENDIF
	
	sCurrentScriptedAnimConversationData.tl23_Block = sConversationTextBlock
	sCurrentScriptedAnimConversationData.tl23_Root = sConversationRoot
	sCurrentScriptedAnimConversationData.ePriority = ePriority
	sCurrentScriptedAnimConversationData.iBS = iBS
ENDPROC

PROC FMMC_ADD_PED_TO_SCRIPTED_ANIM_CONVERSATION(INT iConversationPedSlotToUse, INT iPedOrParticipant, STRING sConversationRoot, STRING sVoiceOverride, INT iSpeakerIDOverride = -1, BOOL bPedPlayAmbientAnims = FALSE, BOOL bPedCanUseAutoLookAt = TRUE, BOOL bPedIsAParticipant = FALSE)
	
	IF sCurrentScriptedAnimConversationData.sConversationPedData[iConversationPedSlotToUse].iConversationPedIndex = iPedOrParticipant
	OR sCurrentScriptedAnimConversationData.sConversationPedData[iConversationPedSlotToUse].iConversationParticipantIndex = iPedOrParticipant
		//Already added!
		EXIT
	ENDIF
	
	TEXT_LABEL_23 tl23_ConversationRoot = sConversationRoot
	INT iSpeakerIDToUse = 0
	
	IF iSpeakerIDOverride > -1
		iSpeakerIDToUse = iSpeakerIDOverride
	ELSE
		iSpeakerIDToUse = GET_SPEAKER_INT_FROM_ROOT(tl23_ConversationRoot, FMMC_MAX_SPEAKERS)
		IF iSpeakerIDToUse >= constMaxNum_Conversers
			PRINTLN("[Dialogue][ScriptedAnimConversation] FMMC_ADD_PED_TO_SCRIPTED_ANIM_CONVERSATION | iSpeakerIDToUse >= constMaxNum_Conversers setting to 0 from ", iSpeakerIDToUse)
			iSpeakerIDToUse = 0
		ENDIF
	ENDIF
	
	TEXT_LABEL_23 tl23_VoiceName = g_FMMC_STRUCT.sSpeakers[iSpeakerIDToUse]
	IF NOT IS_STRING_NULL_OR_EMPTY(sVoiceOverride)
		tl23_VoiceName = sVoiceOverride
	ENDIF
	
	IF bPedIsAParticipant
		sCurrentScriptedAnimConversationData.sConversationPedData[iConversationPedSlotToUse].iConversationParticipantIndex = iPedOrParticipant
	ELSE
		sCurrentScriptedAnimConversationData.sConversationPedData[iConversationPedSlotToUse].iConversationPedIndex = iPedOrParticipant
	ENDIF
	
	sCurrentScriptedAnimConversationData.sConversationPedData[iConversationPedSlotToUse].iSpeakerID = iSpeakerIDToUse
	sCurrentScriptedAnimConversationData.sConversationPedData[iConversationPedSlotToUse].tl23_VoiceName = tl23_VoiceName
	sCurrentScriptedAnimConversationData.sConversationPedData[iConversationPedSlotToUse].bPedPlayAmbientAnims = bPedPlayAmbientAnims
	sCurrentScriptedAnimConversationData.sConversationPedData[iConversationPedSlotToUse].bPedCanUseAutoLookAt = bPedCanUseAutoLookAt
	
	IF bPedIsAParticipant
		PRINTLN("[Dialogue][ScriptedAnimConversation_Adding] FMMC_ADD_PED_TO_SCRIPTED_ANIM_CONVERSATION | Added participant ", iPedOrParticipant, " to conversation data! || iSpeakerID: ", sCurrentScriptedAnimConversationData.sConversationPedData[iConversationPedSlotToUse].iSpeakerID, " | tl23_VoiceName: ", sCurrentScriptedAnimConversationData.sConversationPedData[iConversationPedSlotToUse].tl23_VoiceName)
	ELSE
		PRINTLN("[Dialogue][ScriptedAnimConversation_Adding] FMMC_ADD_PED_TO_SCRIPTED_ANIM_CONVERSATION | Added ped ", iPedOrParticipant, " to conversation data! || iSpeakerID: ", sCurrentScriptedAnimConversationData.sConversationPedData[iConversationPedSlotToUse].iSpeakerID, " | tl23_VoiceName: ", sCurrentScriptedAnimConversationData.sConversationPedData[iConversationPedSlotToUse].tl23_VoiceName)
	ENDIF
ENDPROC

// A helper function that will try to kick off whatever conversation is currently set up in sCurrentScriptedAnimConversationData, and will return TRUE when the conversation successfully starts
FUNC BOOL FMMC_PLAY_CURRENT_SCRIPTED_ANIM_CONVERSATION(BOOL bNetworked = TRUE)	
	IF NOT IS_SCRIPTED_ANIM_CONVERSATION_IN_USE()
		IF bNetworked
			// Send the data to everyone
			BROADCAST_FMMC_EVENT_START_SCRIPTED_ANIM_CONVERSATION(sCurrentScriptedAnimConversationData)
		ENDIF
		
		// Let the conversation play locally whenever it's ready
		SET_SCRIPTED_ANIM_CONVERSATION_STATE(ciScriptedAnimConversationState_WaitingToStart)
	ENDIF
	
	IF IS_SCRIPTED_ANIM_CONVERSATION_PLAYING()
		PRINTLN("[Dialogue][ScriptedAnimConversation] FMMC_PLAY_CURRENT_SCRIPTED_ANIM_CONVERSATION | Root is now playing: ", sCurrentScriptedAnimConversationData.tl23_Root)
		RETURN TRUE
	ELSE
		PRINTLN("[Dialogue][ScriptedAnimConversation] FMMC_PLAY_CURRENT_SCRIPTED_ANIM_CONVERSATION | Waiting for ", sCurrentScriptedAnimConversationData.tl23_Root, " to start!")
		RETURN FALSE
	ENDIF
ENDFUNC

// A helper function for easily playing a conversation that only involves one ped
FUNC BOOL FMMC_CREATE_SINGLE_PED_SCRIPTED_ANIM_CONVERSATION(INT iPed, TEXT_LABEL_23 tl23_ConversationTextBlock, TEXT_LABEL_23 tl23_ConversationRoot, TEXT_LABEL_23 tl23_VoiceOverride, INT iSpeakerIDOverride = -1, enumConversationPriority ePriority = CONV_PRIORITY_AMBIENT_MEDIUM, BOOL bPedPlayAmbientAnims = FALSE, BOOL bPedCanUseAutoLookAt = TRUE, BOOL bNetworked = FALSE)
	
	IF NOT IS_THIS_SCRIPTED_ANIM_CONVERSATION_IN_USE(tl23_ConversationRoot)
		PRINTLN("[Dialogue][ScriptedAnimConversation_Adding] FMMC_CREATE_SINGLE_PED_SCRIPTED_ANIM_CONVERSATION | Setting up a conversation for ped ", iPed, ": ", tl23_ConversationRoot)
		
		FMMC_SET_UP_SCRIPTED_ANIM_CONVERSATION(tl23_ConversationTextBlock, tl23_ConversationRoot, ePriority)
		FMMC_ADD_PED_TO_SCRIPTED_ANIM_CONVERSATION(0, iPed, tl23_ConversationRoot, tl23_VoiceOverride, iSpeakerIDOverride, bPedPlayAmbientAnims, bPedCanUseAutoLookAt)
	ELSE
		PRINTLN("[Dialogue][ScriptedAnimConversation_Adding] FMMC_CREATE_SINGLE_PED_SCRIPTED_ANIM_CONVERSATION | Conversation is about to start for ped ", iPed, ": ", tl23_ConversationRoot)
		
	ENDIF
	
	RETURN FMMC_PLAY_CURRENT_SCRIPTED_ANIM_CONVERSATION(bNetworked)
ENDFUNC


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Filters
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC INT GET_FILTER_SCORE_FROM_TEAMS()
	INT iTotalScore
	INT iTeamCount
	
	INT iTeam = GET_LOCAL_PLAYER_TEAM(TRUE)
	INT iRule = GET_LOCAL_PLAYER_CURRENT_RULE(TRUE)
	
	FOR iTeamCount = 0 TO g_FMMC_STRUCT.iNumberOfTeams-1
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sFilterControls[iRule].iFilterControlBitSet, FMMC_FILTER_BS_COUNT_TEAM_0+iTeamCount)
			iTotalScore += MC_serverBD.iTeamScore[iTeamCount]
		ENDIF
	ENDFOR
	
	RETURN iTotalScore
ENDFUNC

FUNC BOOL SHOULD_FILTERS_BE_PROCESSED(INT iTeam, INT iRule)
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSix[iRule],  ciBS_RULE6_FILTERS_PLAYER_SELECTION)
	OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSix[iRule],  ciBS_RULE6_FILTERS_FORCE_FILTER)
	OR g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iLowHealthFilter[iRule] > 0
	OR HAS_NET_TIMER_STARTED(tdTeamSwapPassiveModeTimer)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Linked Entities
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


FUNC ENTITY_INDEX GET_ENTITY_FROM_LINKED_ENTITY_TYPE(INT iType, INT iIndex)
	
	SWITCH iType
		CASE ciFMMC_ENTITY_LINK_TYPE_PED
			IF iIndex < g_FMMC_STRUCT_ENTITIES.iNumberOfPeds
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niPed[iIndex])
					RETURN NET_TO_ENT(MC_serverBD_1.sFMMC_SBD.niPed[iIndex])
				ENDIF
			ENDIF
		BREAK
		CASE ciFMMC_ENTITY_LINK_TYPE_VEHICLE
			IF iIndex < g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iIndex])
					RETURN NET_TO_ENT(MC_serverBD_1.sFMMC_SBD.niVehicle[iIndex])
				ENDIF
			ENDIF
		BREAK
		CASE ciFMMC_ENTITY_LINK_TYPE_OBJECT
			IF iIndex < g_FMMC_STRUCT_ENTITIES.iNumberOfObjects
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GET_OBJECT_NET_ID(iIndex))
					RETURN NET_TO_ENT(GET_OBJECT_NET_ID(iIndex))
				ENDIF
			ENDIF
		BREAK
		CASE ciFMMC_ENTITY_LINK_TYPE_PROP
			IF iIndex < g_FMMC_STRUCT_ENTITIES.iNumberOfProps
				IF DOES_ENTITY_EXIST(oiProps[iIndex])
					RETURN GET_ENTITY_FROM_PED_OR_VEHICLE(oiProps[iIndex])
				ENDIF
			ENDIF
		BREAK
		CASE ciFMMC_ENTITY_LINK_TYPE_DYNOPROP
			IF iIndex < g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GET_DYNOPROP_NET_ID(iIndex))
					RETURN NET_TO_ENT(GET_DYNOPROP_NET_ID(iIndex))
				ENDIF
			ENDIF
		BREAK
		CASE ciFMMC_ENTITY_LINK_TYPE_INTERACTABLE
			IF iIndex < g_FMMC_STRUCT_ENTITIES.iNumberOfInteractables
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GET_INTERACTABLE_NET_ID(iIndex))
					RETURN NET_TO_ENT(GET_INTERACTABLE_NET_ID(iIndex))
				ENDIF
			ENDIF
		BREAK
		CASE ciFMMC_ENTITY_LINK_TYPE_PICKUP
			IF iIndex < g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons
				IF DOES_PICKUP_EXIST(pipickup[iIndex])
	 				RETURN GET_ENTITY_FROM_PED_OR_VEHICLE(GET_PICKUP_OBJECT(pipickup[iIndex]))
	 			ENDIF
	 		ENDIF
		BREAK
		CASE ciFMMC_ENTITY_LINK_TYPE_TRAIN	
			IF iIndex < g_FMMC_STRUCT_ENTITIES.iNumberOfTrains
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niTrain[iIndex])
					RETURN NET_TO_ENT(MC_serverBD_1.sFMMC_SBD.niTrain[iIndex])
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN NULL
	
ENDFUNC

FUNC VECTOR GET_COORDS_FROM_LINKED_ENTITY_TYPE(INT iType, INT iIndex)
	
	ENTITY_INDEX eiEntity = GET_ENTITY_FROM_LINKED_ENTITY_TYPE(iType, iIndex)
	IF eiEntity != NULL
	AND DOES_ENTITY_EXIST(eiEntity)
		RETURN GET_ENTITY_COORDS(eiEntity, FALSE)
	ENDIF
	
	SWITCH iType
		CASE ciFMMC_ENTITY_LINK_TYPE_LOCATION
			IF iIndex < g_FMMC_STRUCT_ENTITIES.iNumberOfGoToLocations[0]
				RETURN GET_LOCATION_VECTOR(iIndex)
			ENDIF
		BREAK
		CASE ciFMMC_ENTITY_LINK_TYPE_DIALOGUE_TRIGGER
			IF iIndex < g_FMMC_STRUCT.iNumberOfDialogueTriggers
				RETURN g_FMMC_STRUCT.sDialogueTriggers[iIndex].vPosition
			ENDIF
		BREAK
		CASE ciFMMC_ENTITY_LINK_TYPE_ZONE
			IF iIndex < g_FMMC_STRUCT_ENTITIES.iNumberOfZones
				RETURN GET_ZONE_RUNTIME_CENTRE(iIndex)
			ENDIF
		BREAK
		CASE ciFMMC_ENTITY_LINK_TYPE_DOOR
			IF iIndex < g_FMMC_STRUCT_ENTITIES.iNumberOfDoors
				RETURN g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iIndex].vPos
			ENDIF
		BREAK
	ENDSWITCH

	RETURN <<0,0,0>>
	
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Forced Sniper
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC FLOAT GET_SNIPER_RIFLE_AIM_HEADING_FOR_PED(PED_INDEX piPed) 	
	
	IF NOT IS_ENTITY_ALIVE(piPed)
	OR NOT bLocalPlayerPedOk
		RETURN 0.0
	ENDIF
	
    VECTOR vVector = GET_ENTITY_COORDS(piPed) - GET_ENTITY_COORDS(LocalPlayerPed)      
   	FLOAT fHeading = (GET_HEADING_FROM_VECTOR_2D(vVector.x, vVector.y) - GET_ENTITY_HEADING(LocalPlayerPed))   
	
  	IF fHeading > 180
        fHeading -= 360
 	ENDIF
	
  	IF fHeading < -180
        fHeading += 360
  	ENDIF
	
    RETURN fHeading    
	
endfunc 

FUNC FLOAT GET_SNIPER_RIFLE_AIM_PITCH_FOR_PED(PED_INDEX piPed)
	
	IF NOT IS_ENTITY_ALIVE(piPed)
	OR NOT bLocalPlayerPedOk
		RETURN 0.0
	ENDIF
	
	VECTOR vVector = GET_ENTITY_COORDS(piPed) - GET_ENTITY_COORDS(LocalPlayerPed)
    vVector = NORMALISE_VECTOR(vVector)

    RETURN ATAN2(vVector.z, VMAG(<<vVector.x, vVector.y, 0.0>>))

ENDFUNC

PROC CLEAN_UP_FORCE_SNIPER()
	PRINTLN("[ForcedSniper] - CLEAN_UP_FORCE_SNIPER - Cleaning up")
	iForceSniperScopeTime = 0
	iForceSniperScopeTargetPed = -1
	RESET_NET_TIMER(tdForceSniperScopeTimer)
	CLEAR_BIT(iLocalBoolCheck11, LBOOL11_FORCE_SNIPER_SCOPE)
	IF IS_BIT_SET(iLocalBoolCheck11, LBOOL11_FORCE_SNIPER_SCOPE_ACTIVE)
		SET_PLAYER_FORCE_SKIP_AIM_INTRO(LocalPlayer, FALSE)
		SET_PLAYER_FORCED_AIM(LocalPlayer, FALSE)
		CLEAR_BIT(iLocalBoolCheck11, LBOOL11_FORCE_SNIPER_SCOPE_ACTIVE)
	ENDIF
	CLEAR_BIT(iLocalBoolCheck11, LBOOL11_FORCE_SNIPER_SCOPE_ACTIVE_AIMING)
	CLEAR_BIT(iLocalBoolCheck11, LBOOL11_FORCE_SNIPER_SCOPE_ACTIVE_ZOOMING)
ENDPROC

PROC PROCESS_FORCE_SNIPER_SCOPE_AIM()
	
	IF NOT IS_BIT_SET(iLocalBoolCheck11, LBOOL11_FORCE_SNIPER_SCOPE)
		EXIT
	ENDIF
	
	IF iForceSniperScopeTime <= 0
		CLEAN_UP_FORCE_SNIPER()
		EXIT
	ENDIF
	
	IF iForceSniperScopeTargetPed = -1
		CLEAN_UP_FORCE_SNIPER()
		EXIT
	ENDIF
	
	IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niPed[iForceSniperScopeTargetPed])
		CLEAN_UP_FORCE_SNIPER()
		EXIT
	ENDIF
	
	PED_INDEX piTarget = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iForceSniperScopeTargetPed])
	IF IS_ENTITY_DEAD(piTarget)
		CLEAN_UP_FORCE_SNIPER()
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(iLocalBoolCheck11, LBOOL11_FORCE_SNIPER_SCOPE_ACTIVE)
		PRINTLN("[ForcedSniper] - CLEAN_UP_FORCE_SNIPER - First Frame Activation. Target: ", iForceSniperScopeTargetPed, " Time: ", iForceSniperScopeTime)
		SET_PLAYER_FORCE_SKIP_AIM_INTRO(LocalPlayer, TRUE)
		SET_PLAYER_FORCED_AIM(LocalPlayer, TRUE)
		FORCE_PED_AI_AND_ANIMATION_UPDATE(LocalPlayerPed, TRUE)
		
		REINIT_NET_TIMER(tdForceSniperScopeTimer)
		SET_BIT(iLocalBoolCheck11, LBOOL11_FORCE_SNIPER_SCOPE_ACTIVE)
	ENDIF
	
	IF HAS_NET_TIMER_STARTED_AND_EXPIRED(tdForceSniperScopeTimer, iForceSniperScopeTime)
		PRINTLN("[ForcedSniper] - CLEAN_UP_FORCE_SNIPER - Timed Out")
		CLEAN_UP_FORCE_SNIPER()
		EXIT
	ENDIF
	
	SET_GAMEPLAY_CAM_RELATIVE_HEADING(GET_SNIPER_RIFLE_AIM_HEADING_FOR_PED(piTarget))
	SET_GAMEPLAY_CAM_RELATIVE_PITCH(GET_SNIPER_RIFLE_AIM_PITCH_FOR_PED(piTarget))
		
	SET_FIRST_PERSON_AIM_CAM_ZOOM_FACTOR(7)

	SET_PED_NO_TIME_DELAY_BEFORE_SHOT(LocalPlayerPed)
	DISPLAY_SNIPER_SCOPE_THIS_FRAME()
	FORCE_PED_MOTION_STATE(LocalPlayerPed, MS_AIMING)
	
	IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_AIM)
		PRINTLN("[ForcedSniper] - CLEAN_UP_FORCE_SNIPER - Player Took over control")
		CLEAN_UP_FORCE_SNIPER()
		EXIT
	ENDIF
	
ENDPROC

FUNC BOOL IS_LOCAL_PLAYER_IN_FIRST_PERSON_MODE()
	
	IF GET_CAM_VIEW_MODE_FOR_CONTEXT(GET_CAM_ACTIVE_VIEW_MODE_CONTEXT()) = CAM_VIEW_MODE_FIRST_PERSON
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC PROCESS_DISABLE_REALTIME_MULTIPLAYER()
	
	IF g_bBlock_PROCESS_DISABLE_REALTIME_MULTIPLAYER
		PRINTLN("PROCESS_DISABLE_REALTIME_MULTIPLAYER - GLOBAL BLOCK SET - EXITING")
		EXIT
	ENDIF
	
	IF g_sMPTunables.bEnableRealtimeMPCheck_MC2020
	AND g_bMissionEnding
		NETWORK_DISABLE_REALTIME_MULTIPLAYER()
		PRINTLN("PROCESS_DISABLE_REALTIME_MULTIPLAYER - Calling NETWORK_DISABLE_REALTIME_MULTIPLAYER for Mission Controller 2020")
	ENDIF
ENDPROC

PROC PROCESS_RAGDOLL_ON_RULE(INT iTeam)

	INT iCurrentTeamRule = GET_TEAM_CURRENT_RULE(iTeam)
	
	IF NOT IS_RULE_INDEX_VALID(iCurrentTeamRule)
		EXIT
	ENDIF
	
	IF IS_PED_INJURED(LocalPlayerPed)
		EXIT
	ENDIF

	INT iRagdollDuration = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sOnRuleStart[iCurrentTeamRule].iRagdollDuration * 1000
	
	IF iRagdollDuration > -1
		SET_PED_TO_RAGDOLL(LocalPlayerPed, iRagdollDuration, iRagdollDuration, TASK_RELAX, FALSE, FALSE)
		SET_PLAYER_CONTROL(LocalPlayer, FALSE)
	ENDIF
	
ENDPROC

PROC PROCESS_FADE_OUT_ON_RULE(INT iTeam)

	INT iCurrentTeamRule = GET_TEAM_CURRENT_RULE(iTeam)
	
	IF NOT IS_RULE_INDEX_VALID(iCurrentTeamRule)
		EXIT
	ENDIF
	
	INT iFadeOutDuration = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sOnRuleStart[iCurrentTeamRule].iFadeOutDuration * 1000
	
	IF iFadeOutDuration > -1
		FMMC_SAFE_FADE_SCREEN_OUT_TO_BLACK(iFadeOutDuration)
	ENDIF
	
ENDPROC

PROC PROCESS_FADE_IN_ON_RULE(INT iTeam)

	INT iCurrentTeamRule = GET_TEAM_CURRENT_RULE(iTeam)
	
	IF NOT IS_RULE_INDEX_VALID(iCurrentTeamRule)
		EXIT
	ENDIF
	
	INT iFadeInDuration = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sOnRuleStart[iCurrentTeamRule].iFadeInDuration * 1000
	
	IF iFadeInDuration > -1
		FMMC_SAFE_FADE_SCREEN_IN_FROM_BLACK(iFadeInDuration)
	ENDIF
	
ENDPROC

FUNC BOOL IS_PART_WAITING_IN_SPECTATE(INT iPart)
	RETURN IS_BIT_SET(MC_playerBD[iPart].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN)
ENDFUNC

PROC FORCE_BLIP_ALL_PLAYERS(BOOL bBlipPlayers = TRUE, BOOL bIsLongRange = TRUE, BOOL bFocusOnlyOnEnemyPlayers = FALSE)
	INT i
	FOR i = 0 TO (GET_MAXIMUM_PLAYERS_ON_MISSION() - 1)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
			IF bFocusOnlyOnEnemyPlayers 
			AND DOES_TEAM_LIKE_TEAM(MC_playerBD[iLocalPart].iTeam, MC_playerBD[i].iTeam)
				RELOOP
			ENDIF
			PLAYER_INDEX tempPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
			IF IS_ENTITY_ALIVE(GET_PLAYER_PED(tempPlayer))									
				SET_PLAYER_BLIP_AS_HIDDEN(tempPlayer, !bBlipPlayers, TRUE)
				FORCE_BLIP_PLAYER(tempPlayer, bBlipPlayers, bIsLongRange, TRUE)
				#IF IS_DEBUG_BUILD
					IF bBlipPlayers
						PRINTLN("FORCE_BLIP_ALL_PLAYERS - Blipping player ", GET_PLAYER_NAME(tempPlayer), ". bFocusOnlyOnEnemyPlayers = ", BOOL_TO_STRING(bFocusOnlyOnEnemyPlayers))
					ELSE
						PRINTLN("FORCE_BLIP_ALL_PLAYERS - Removing blip for player ", GET_PLAYER_NAME(tempPlayer), ". bFocusOnlyOnEnemyPlayers = ", BOOL_TO_STRING(bFocusOnlyOnEnemyPlayers))
					ENDIF
				#ENDIF
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

PROC SKIP_BLIP_CREATION_FOR_THIS_VEHICLE(INT iVehIndex, BOOL bSkipBlipCreation)
	#IF IS_DEBUG_BUILD
		IF iVehIndex > FMMC_MAX_VEHICLES
			ASSERTLN("SKIP_BLIP_CREATION_FOR_THIS_VEHICLE - iVehIndex can't be greater than num of FMMC_MAX_VEHICLES!")
			EXIT
		ENDIF
		IF iVehIndex < 0
			ASSERTLN("SKIP_BLIP_CREATION_FOR_THIS_VEHICLE - iVehIndex can't be lower than 0!")
			EXIT
		ENDIF
		INT iZero = 0 //To prevent "dead code" warning
		IF FMMC_MAX_VEHICLES > ci_VEHICLE_IGNORE_BLIP_CREATION_BS_MAX * 31 + iZero
			INT iNewArraySize = FMMC_MAX_VEHICLES / 31
			IF FMMC_MAX_VEHICLES % 31 != iZero
				iNewArraySize++
			ENDIF
			
			ASSERTLN("SKIP_BLIP_CREATION_FOR_THIS_VEHICLE - FMMC_MAX_VEHICLES is greater than ", ci_VEHICLE_IGNORE_BLIP_CREATION_BS_MAX * 31, ". Please update ci_VEHICLE_IGNORE_BLIP_CREATION_BS_MAX with the new max value (", iNewArraySize, ")")
			EXIT
		ENDIF
	#ENDIF
	
	IF bSkipBlipCreation
		FMMC_SET_LONG_BIT(iVehIgnoreBlipCreationBS, iVehIndex)
		PRINTLN("SKIP_BLIP_CREATION_FOR_THIS_VEHICLE - Vehicle ", iVehIndex, " set to skip FMMC blip creation!")
	ELSE 
		FMMC_CLEAR_LONG_BIT(iVehIgnoreBlipCreationBS, iVehIndex)
		PRINTLN("SKIP_BLIP_CREATION_FOR_THIS_VEHICLE - Clearing vehicle ", iVehIndex, " from iVehIgnoreBlipCreationBS!")
	ENDIF
ENDPROC

FUNC PLAYER_INDEX GET_PLAYER_INDEX_FROM_ENTITY(ENTITY_INDEX EntityIndex)
	RETURN NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_PED_INDEX_FROM_ENTITY_INDEX(EntityIndex))
ENDFUNC

FUNC STRING RGBA_TO_STRING(RGBA_STRUCT sRGBA)
	TEXT_LABEL_23 tlColour = "R("
	tlColour += FLOAT_TO_STRING(sRGBA.R, 1)
	
	tlColour += ") G("
	tlColour += FLOAT_TO_STRING(sRGBA.G, 1)
	
	tlColour += ") B("
	tlColour += FLOAT_TO_STRING(sRGBA.B, 1)
	
	tlColour += ") A("
	tlColour += FLOAT_TO_STRING(sRGBA.A, 1)
	
	tlColour += ")"
	
	RETURN TEXT_LABEL_TO_STRING(tlColour)
ENDFUNC
