// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### System: GTA V Online Mission Controller - Interactables -----------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Script: FM_Mission_Controller_2020.sc -----------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: This header contains functions related to Interactable processing during the mission.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### This script was forked from FM_Mission_Controller.sc at the beginning of 2020		 																				  
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

USING "FM_Mission_Controller_ObjectsMinigames_2020.sch"

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Interaction-Specific Utility Functions --------------------------------------------------------------------------------------------------------------------------
// ##### Description: Basic utility/helper functions that are specific to a particular Interaction (Interact-With, Sync Lock, Looting etc).  -------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC SET_SYNCLOCK_STATE(SYNCLOCK_INTERACTION_STATE eSyncLockState)
	MC_playerBD[iLocalPart].eSyncLockInteractionState = eSyncLockState
	RESET_INTERACTABLE_BAIL_TIMER()
	PRINTLN("[Interactables][SyncLock] eSyncLockInteractionState is now ", MC_playerBD[iLocalPart].eSyncLockInteractionState)
ENDPROC

FUNC BOOL IS_CURRENT_SYNC_LOCK_INTERACTION_SUCCESSFUL()
	
	// If we've got a cached linked Interactable, just check that
	IF DOES_INTERACTABLE_HAVE_LINKED_INTERACTABLE(GET_FOREGROUND_INTERACTABLE_INDEX())
		RETURN FMMC_IS_LONG_BIT_SET(iInteractable_SyncLockEngagedBS, GET_LINKED_INTERACTABLE_INDEX(GET_FOREGROUND_INTERACTABLE_INDEX()))
	ENDIF
	
	INT iRemoteInteractable = 0
	FOR iRemoteInteractable = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfInteractables - 1
		IF IS_SYNC_LOCK_PANEL_RELEVANT_TO_LOCAL_PLAYER(iRemoteInteractable)
			IF FMMC_IS_LONG_BIT_SET(iInteractable_SyncLockEngagedBS, iRemoteInteractable)
				// This one is relevant and also currently engaged!
			ELSE
				// This one is required, but hasn't been engaged
				RETURN FALSE
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN TRUE
ENDFUNC

PROC ENGAGE_SYNC_LOCK(INT iInteractable, OBJECT_INDEX oiInteractable)
	IF iInteractable > -1
		PRINTLN("[Interactables][Interactable ", iInteractable, "][SyncLock] ENGAGE_SYNC_LOCK")
		BROADCAST_FMMC_INTERACTABLE_EVENT(FMMC_INTERACTABLE_EVENT_TYPE__SYNCLOCK_ENGAGED, iInteractable)
		PLAY_SOUND_FROM_ENTITY(-1, "Insert_Keycard", oiInteractable, "Twin_Card_Entry_Sounds")
	ENDIF
ENDPROC

PROC DISENGAGE_SYNC_LOCK(INT iInteractable, BOOL bResetSyncLockTimer)
	IF iInteractable > -1
		IF bResetSyncLockTimer
		OR FMMC_IS_LONG_BIT_SET(iInteractable_SyncLockEngagedBS, iInteractable)
			PRINTLN("[Interactables][Interactable ", iInteractable, "][SyncLock] DISENGAGE_SYNC_LOCK | bResetSyncLockTimer: ", bResetSyncLockTimer)
			INT iEventBS = 0
			
			IF bResetSyncLockTimer
				SET_BIT(iEventBS, ciINTERACTABLE_EVENT_BS__RESET_SYNCLOCK_TIMER)
			ENDIF
			
			BROADCAST_FMMC_INTERACTABLE_EVENT(FMMC_INTERACTABLE_EVENT_TYPE__SYNCLOCK_DISENGAGED, iInteractable, iEventBS)
			
			IF FMMC_IS_LONG_BIT_SET(iInteractable_SyncLockEngagedBS, iInteractable)
			AND DOES_ENTITY_EXIST(GET_FOREGROUND_INTERACTION_INTERACTABLE_OBJECT())
				PLAY_SOUND_FROM_ENTITY(-1, "Remove_Keycard", GET_FOREGROUND_INTERACTION_INTERACTABLE_OBJECT(), "Twin_Card_Entry_Sounds")
			ENDIF
			
			INT iMySyncLockInteraction = GET_ONGOING_INTERACTION_INDEX(iInteractable)
			IF iMySyncLockInteraction > -1
				STOP_INTERACTABLE_LOOPING_SOUND(sOngoingInteractionVars[iMySyncLockInteraction])
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC RESET_SYNC_LOCK_VARS(BOOL bIncludeState = FALSE, BOOL bIncludeTimer = FALSE)
	IF bIncludeState
		SET_SYNCLOCK_STATE(SYNCLOCK_INTERACTION_STATE__OFF)
	ENDIF
	
	DISENGAGE_SYNC_LOCK(GET_FOREGROUND_INTERACTABLE_INDEX(), bIncludeTimer)
	PRINTLN("[Interactables][SyncLock] RESET_SYNC_LOCK_VARS | bIncludeState: ", bIncludeState, "/ bIncludeTimer: ", bIncludeTimer)
ENDPROC

PROC SET_CUTPAINTING_STATE(CUTPAINTING_INTERACTION_STATE eCutPaintingState)
	eCutPaintingInteractionState = eCutPaintingState
	RESET_INTERACTABLE_BAIL_TIMER()
	PRINTLN("[Interactables][CutPainting] eCutPaintingInteractionState is now ", eCutPaintingInteractionState)
ENDPROC

PROC RESET_CUT_PAINTING_VARS()
	PRINTLN("[Interactables][CutPainting] RESET_CUT_PAINTING_VARS")
	SET_CUTPAINTING_STATE(CUTPAINTING_INTERACTION_STATE__OFF)
ENDPROC

FUNC BOOL IS_INTERACTABLE_ATTACHED_VEHICLE_BOOT_OPEN(INT iInteractable)

	VEHICLE_INDEX viAttachedVeh = GET_FMMC_ENTITY_VEHICLE(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iInteractable_AttachedEntityIndex)
	
	IF DOES_ENTITY_EXIST(viAttachedVeh)
		IF GET_VEHICLE_DOOR_ANGLE_RATIO(viAttachedVeh, SC_DOOR_BOOT) >= 0.55
			PRINTLN("[Interactables][Interactable ", iInteractable, "] PROCESS_LOADING_INTERACTION__LOOT_VEHICLE_TRUNK | Trunk is open!")
			RETURN TRUE
		ELSE
			PRINTLN("[Interactables][Interactable ", iInteractable, "] PROCESS_LOADING_INTERACTION__LOOT_VEHICLE_TRUNK | Need to open trunk!")
			RETURN FALSE
		ENDIF
	ENDIF
	
	// Return TRUE here so we don't block the interaction if the vehicle doesn't happen to exist for whatever reason
	RETURN TRUE
ENDFUNC

PROC UNLOAD_INTERACTABLE_PREPARATION(INT iInteractable)
	
	INT iInteractionType = g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iInteractable_InteractionType
	
	SWITCH iInteractionType
		CASE ciInteractableInteraction_MultiSolutionLock
			PRINTLN("[Interactables][Interactable ", iInteractable, "] UNLOAD_INTERACTABLE_PREPARATION | Unloading extra preparation from ciInteractableInteraction_MultiSolutionLock")
			REMOVE_NAMED_PTFX_ASSET("scr_ih_fin")
		BREAK
		
		CASE ciInteractableInteraction_PlantThermalCharge
			PRINTLN("[Interactables][Interactable ", iInteractable, "] UNLOAD_INTERACTABLE_PREPARATION | Unloading extra preparation from ciInteractableInteraction_PlantThermalCharge")
			REMOVE_NAMED_PTFX_ASSET("scr_ch_finale")
			REMOVE_NAMED_PTFX_ASSET("scr_ornate_heist")
			REMOVE_NAMED_PTFX_ASSET("pat_heist")
			
			REMOVE_ANIM_DICT("anim@heists@ornate_bank@thermal_charge")
		BREAK
	ENDSWITCH
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Interactable Rule Warps
// ##### Description: Moving the Interactable to a creator placed position on a rule change.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
PROC WARP_INTERACTABLE_TO_WARP_LOCATION(FMMC_INTERACTABLE_STATE &sInteractableState, VECTOR vObjWarpPos, FLOAT fObjWarpHead, INT &iInteractableWarpedBS[], INT iEventInteractableWarpedBS)	
	// Should be checked last so that we can return true in this function on the first frame the checks come back positive.
	IF IS_WARP_LOCATION_VISIBLE_TO_ANY_PLAYERS(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].sWarpLocationSettings, GET_FMMC_INTERACTABLE_COORDS(sInteractableState), vObjWarpPos)
		PRINTLN("[Warp_location][Interactables][Interactable ", sInteractableState.iIndex, "] - PROCESS_INTERACTABLE_ON_RULE_WARP - IS_WARP_LOCATION_VISIBLE_TO_ANY_PLAYERS = TRUE")
		EXIT
	ENDIF	
	IF NOT IS_VECTOR_ZERO(vObjWarpPos)		
		PRINTLN("[Warp_location][Interactables][Interactable ", sInteractableState.iIndex, "] - WARP_INTERACTABLE_TO_WARP_LOCATION - Warping this interactable with COORD: ", vObjWarpPos, " and Heading: ", fObjWarpHead, " Setting iInteractableWarpedBS.")		
		PLAY_WARP_ENTITY_TO_WARP_LOCATION_PTFX(GET_FMMC_INTERACTABLE_COORDS(sInteractableState), g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].sWarpLocationSettings.sEntityWarpPTFX, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].sWarpLocationSettings.iWarpLocationBS, ciWARP_LOCATION_BITSET_PlayWarpPTFX_Start))
		PLAY_WARP_ENTITY_TO_WARP_LOCATION_PTFX(vObjWarpPos, g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].sWarpLocationSettings.sEntityWarpPTFX, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].sWarpLocationSettings.iWarpLocationBS, ciWARP_LOCATION_BITSET_PlayWarpPTFX_End))
		SET_ENTITY_COORDS(sInteractableState.objIndex, vObjWarpPos, FALSE, TRUE)
		SET_ENTITY_HEADING(sInteractableState.objIndex, fObjWarpHead)
		FMMC_SET_LONG_BIT(iInteractableWarpedBS, sInteractableState.iIndex)
		BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, iEventInteractableWarpedBS, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE, sInteractableState.iIndex)
	ELSE
		ASSERTLN("[Warp_location][Interactables][Interactable ", sInteractableState.iIndex, "] - WARP_INTERACTABLE_TO_WARP_LOCATION NULL VECTOR POSITION GIVEN. Check the Creator Data. ")
		#IF IS_DEBUG_BUILD
		ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_IGNORE, ENTITY_RUNTIME_ERROR_TYPE_CRITICAL_INTERACTABLE, "Set up to warp on rule to a NULL/ZERO vector!", sInteractableState.iIndex)
		#ENDIF
		FMMC_SET_LONG_BIT(iInteractableWarpedBS, sInteractableState.iIndex)
		BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, iEventInteractableWarpedBS, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULt, DEFAULT, TRUE, sInteractableState.iIndex)
	ENDIF
ENDPROC

FUNC BOOL CAN_INTERACTABLE_PROCESS_WARP_LOCATION(FMMC_INTERACTABLE_STATE &sInteractableState)
	IF NOT sInteractableState.bInteractableAlive
		PRINTLN("[Warp_location][Interactables][Interactable ", sInteractableState.iIndex, "] - CAN_INTERACTABLE_PROCESS_WARP_LOCATION - Has been set to warp on this rule but the Object does not exist or is destroyed...")
		RETURN FALSE
	ENDIF
	
	IF NOT sInteractableState.bHaveControlOfInteractable
		PRINTLN("[Warp_location][Interactables][Interactable ", sInteractableState.iIndex, "] - CAN_INTERACTABLE_PROCESS_WARP_LOCATION - Has been set to warp on this rule but we don't have control over it.")
		RETURN FALSE
	ENDIF
	
	IF NOT READY_TO_PROCESS_ENTITY_WARP()
		PRINTLN("[Warp_location][Interactables][Interactable ", sInteractableState.iIndex, "] - CAN_INTERACTABLE_PROCESS_WARP_LOCATION - We should perform a warp, but we're not ready to process it yet.")
		RETURN FALSE
	ENDIF	
	RETURN TRUE
ENDFUNC

PROC PROCESS_INTERACTABLE_ON_RULE_WARP(FMMC_INTERACTABLE_STATE &sInteractableState)
	
	IF NOT SHOULD_ENTITY_PERFORM_A_RULE_WARP_LOCATION_WARP(CREATION_TYPE_INTERACTABLE, sInteractableState.iIndex)
		EXIT
	ENDIF
	
	IF NOT CAN_INTERACTABLE_PROCESS_WARP_LOCATION(sInteractableState)
		EXIT
	ENDIF
	
	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule =	MC_serverBD_4.iCurrentHighestPriority[iTeam] 
	
	FLOAT fObjWarpHead
	VECTOR vObjWarpPos
	
	GET_ENTITY_WARP_LOCATION_ON_THIS_RULE_START(iRule, iTeam, CREATION_TYPE_INTERACTABLE, sInteractableState.iIndex, vObjWarpPos, fObjWarpHead)
	
	WARP_INTERACTABLE_TO_WARP_LOCATION(sInteractableState, vObjWarpPos, fObjWarpHead, iInteractableWarpedOnThisRule, g_ciInstancedcontentEventType_WarpedInteractableOnRuleStart)	
	
ENDPROC

PROC PROCESS_INTERACTABLE_ON_DAMAGE_WARP(FMMC_INTERACTABLE_STATE &sInteractableState)
	
	IF NOT SHOULD_ENTITY_PERFORM_AN_ON_DAMAGE_LOCATION_WARP(CREATION_TYPE_INTERACTABLE, sInteractableState.iIndex)
		EXIT
	ENDIF
		
	IF NOT CAN_INTERACTABLE_PROCESS_WARP_LOCATION(sInteractableState)
		EXIT
	ENDIF
	
	FLOAT fObjWarpHead
	VECTOR vObjWarpPos
	
	GET_ENTITY_WARP_LOCATION_ON_DAMAGED(CREATION_TYPE_INTERACTABLE, sInteractableState.iIndex, vObjWarpPos, fObjWarpHead)	
	
	WARP_INTERACTABLE_TO_WARP_LOCATION(sInteractableState, vObjWarpPos, fObjWarpHead, iInteractableWarpedOnDamage, g_ciInstancedcontentEventType_WarpedInteractableDamageStart)	
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Interactable Utility Functions ----------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Basic utility/helper functions that can be used throughout the Interactable system.  -----------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL SHOULD_INTERACTABLE_INTERACTION_REQUIRE_PLAYER_INPUT_TO_START(FMMC_INTERACTABLE_STATE& sInteractableState)
	SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_InteractionType
		CASE ciInteractableInteraction_InteractWith
			SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_InteractWithAnimPreset
				CASE ciINTERACT_WITH_PRESET__PASS_GUARD
				CASE ciINTERACT_WITH_PRESET__CUTSCENE_LEAD_OUT
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

PROC SET_INTERACTION_SECONDARY_ANIM_STATE(INTERACTION_VARS& sInteractionVars, INT iState)
	sInteractionVars.iInteractableSecondaryAnimState = iState
	PRINTLN("[Interactables][Interactable ", sInteractionVars.iInteractable, "] SET_INTERACTION_SECONDARY_ANIM_STATE | Setting state to ", iState)
ENDPROC

PROC CLEAN_UP_INTERACTION_SECONDARY_ANIM(BOOL bPlayExitAnim = TRUE)
	IF iInteractable_CachedSecondaryAnimInteractable > -1
		PRINTLN("[Interactables][Interactable ", iInteractable_CachedSecondaryAnimInteractable, "] CLEAN_UP_INTERACTION_SECONDARY_ANIM | Cleaning up secondary anim")
		DEBUG_PRINTCALLSTACK()
		
		IF bPlayExitAnim
			PLAY_SECONDARY_ANIM_ON_PLAYER(sInteractable_CachedSecondaryAnimDict, sInteractable_CachedSecondaryExitAnimName, sInteractable_CachedSecondaryAnimFilter, FALSE)
		ENDIF
		
		iInteractable_CachedSecondaryAnimInteractable = -1
		sInteractable_CachedSecondaryAnimDict = ""
		sInteractable_CachedSecondaryExitAnimName = ""
		sInteractable_CachedSecondaryAnimFilter = ""
	ENDIF
ENDPROC

PROC PROCESS_INTERACTION_SECONDARY_ANIM(INTERACTION_VARS& sInteractionVars, BOOL bShouldPlay, STRING sDict, STRING sIntroAnim, STRING sLoopAnim, STRING sExitAnim, STRING sFilter)
	
	IF sInteractionVars.iInteractableSecondaryAnimState != ciINTERACTION_SECONDARY_ANIM_STATE__LOADING
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_ALTERNATE)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_RELOAD)
		
		iInteractable_CachedSecondaryAnimInteractable = sInteractionVars.iInteractable
		sInteractable_CachedSecondaryAnimDict = sDict
		sInteractable_CachedSecondaryExitAnimName = sExitAnim
		sInteractable_CachedSecondaryAnimFilter = sFilter
	ENDIF
	
	SWITCH sInteractionVars.iInteractableSecondaryAnimState
		CASE ciINTERACTION_SECONDARY_ANIM_STATE__LOADING
			REQUEST_ANIM_DICT(sDict)
			
			IF HAS_ANIM_DICT_LOADED(sDict)
			AND bShouldPlay
				IF PLAY_SECONDARY_ANIM_ON_PLAYER(sDict, sIntroAnim, sFilter, FALSE)
					SET_INTERACTION_SECONDARY_ANIM_STATE(sInteractionVars, ciINTERACTION_SECONDARY_ANIM_STATE__PLAYING_INTRO)
				ENDIF
			ENDIF
		BREAK
		
		CASE ciINTERACTION_SECONDARY_ANIM_STATE__PLAYING_INTRO
			IF IS_PLAYER_SECONDARY_ANIM_COMPLETE(sDict, sIntroAnim)
				IF PLAY_SECONDARY_ANIM_ON_PLAYER(sDict, sLoopAnim, sFilter, TRUE)
					SET_INTERACTION_SECONDARY_ANIM_STATE(sInteractionVars, ciINTERACTION_SECONDARY_ANIM_STATE__PLAYING_LOOP)
				ENDIF
			ENDIF
		BREAK
		
		CASE ciINTERACTION_SECONDARY_ANIM_STATE__PLAYING_LOOP
			IF bShouldPlay
				IF NOT IS_ENTITY_PLAYING_ANIM(LocalPlayerPed, sDict, sLoopAnim)
					SET_INTERACTION_SECONDARY_ANIM_STATE(sInteractionVars, ciINTERACTION_SECONDARY_ANIM_STATE__LOADING)
				ENDIF
			ELSE
				PLAY_SECONDARY_ANIM_ON_PLAYER(sDict, sExitAnim, sFilter, FALSE)
				SET_INTERACTION_SECONDARY_ANIM_STATE(sInteractionVars, ciINTERACTION_SECONDARY_ANIM_STATE__PLAYING_EXIT)
			ENDIF
		BREAK
		
		CASE ciINTERACTION_SECONDARY_ANIM_STATE__PLAYING_EXIT
			IF IS_PLAYER_SECONDARY_ANIM_COMPLETE(sDict, sExitAnim)
				CLEAN_UP_INTERACTION_SECONDARY_ANIM(FALSE)
				SET_INTERACTION_SECONDARY_ANIM_STATE(sInteractionVars, ciINTERACTION_SECONDARY_ANIM_STATE__LOADING)
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

FUNC STRING GET_DEFAULT_PROMPT_HELPTEXT_FOR_INTERACTABLE(FMMC_INTERACTABLE_STATE& sInteractableState, INTERACTION_VARS& sInteractionVars)

	TEXT_LABEL_15 tl15 = "FMMC_INT_P"
	tl15 += g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_InteractionType
	
	SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_InteractionType
		CASE ciInteractableInteraction_InteractWith
			tl15 = GET_INTERACT_WITH_PROMPT(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_InteractWithAnimPreset, g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_InteractWithAltAnimSet, -1)
		BREAK
		
		CASE ciInteractableInteraction_MultiSolutionLock
			tl15 = "MC_INT_MSL_"
			tl15 += GET_INTERACTABLE_MULTI_SOLUTION_LOCK_TYPE(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex])
			tl15 += "_"
			tl15 += GET_INTERACTABLE_MULTI_SOLUTION_LOCK_CURRENT_BEST_SOLUTION(sInteractableState.iIndex)
		BREAK
		
		CASE ciInteractableInteraction_LootVehicleTrunk
			IF IS_INTERACTABLE_ATTACHED_VEHICLE_BOOT_OPEN(sInteractableState.iIndex)
			AND NOT IS_BIT_SET(sInteractionVars.iOngoingInteractionBS, ciONGOING_INTERACTION_BS__CURRENT_LOOT_VEHICLE_INTERACTABLE_OPENING_TRUNK)
				tl15 = GET_INTERACT_WITH_PROMPT(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_InteractWithAnimPreset, g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_InteractWithAltAnimSet, -1)
			ELSE
				tl15 = GET_INTERACT_WITH_PROMPT(ciINTERACT_WITH_PRESET__OPEN_VEHICLE_TRUNK, g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_InteractWithAltAnimSet, -1)
			ENDIF
		BREAK
	ENDSWITCH
	
	STRING sString = TEXT_LABEL_TO_STRING(tl15)
	RETURN sString
ENDFUNC

FUNC STRING GET_INTERACTABLE_PROMPT_HELPTEXT(FMMC_INTERACTABLE_STATE& sInteractableState, INTERACTION_VARS& sInteractionVars)
	IF g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_PromptStringIndex > -1
		RETURN GET_CUSTOM_STRING_LIST_STRING(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_PromptStringIndex)
	ELSE
		RETURN GET_DEFAULT_PROMPT_HELPTEXT_FOR_INTERACTABLE(sInteractableState, sInteractionVars)
	ENDIF
	
	RETURN ""
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_WEARING_OUTFIT_FROM_THIS_INTERACTABLE(FMMC_INTERACTABLE_STATE& sInteractableState)
	IF g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_InteractionType = ciInteractableInteraction_SwitchOutfit
		IF MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(GET_CHANGE_CLOTHES_OUTFIT_FROM_SELECTION(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_Outfit))
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_InteractionType = ciInteractableInteraction_InteractWith
		IF g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_InteractWithAnimPreset = ciINTERACT_WITH_PRESET__CLOTHES
			IF MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(GET_CHANGE_CLOTHES_OUTFIT_FROM_SELECTION(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_Outfit))
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC STRING GET_DEFAULT_NOT_MET_PREREQ_HELPTEXT_FOR_INTERACTION(FMMC_INTERACTABLE_STATE& sInteractableState)
	SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_InteractionType
		CASE ciInteractableInteraction_InteractWith
			SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_InteractWithAnimPreset
				CASE ciINTERACT_WITH_PRESET__USE_KEY_ON_DOOR 	RETURN "IHF_NOEQ_1"
			ENDSWITCH
		BREAK
		
		CASE ciInteractableInteraction_SyncLock		RETURN "IHF_NOEQ_2"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

PROC DISPLAY_INTERACTABLE_NOT_MET_PREREQ_HELPTEXT_THIS_FRAME(FMMC_INTERACTABLE_STATE& sInteractableState)
	IF NOT IS_STRING_NULL_OR_EMPTY(GET_DEFAULT_NOT_MET_PREREQ_HELPTEXT_FOR_INTERACTION(sInteractableState))
		DISPLAY_HELP_TEXT_THIS_FRAME(GET_DEFAULT_NOT_MET_PREREQ_HELPTEXT_FOR_INTERACTION(sInteractableState), TRUE)
	ELSE
		// This interaction isn't set up to have any helptext for not meeting the required prereqs!
	ENDIF
ENDPROC

FUNC BOOL IS_INTERACTABLE_INTERACTION_BLOCKED_BY_ATTACHED_ENTITY(FMMC_INTERACTABLE_STATE& sInteractableState, BOOL bDisplayHelptext)
	
	UNUSED_PARAMETER(bDisplayHelptext) // To be used if any states require special "you can't do that" helptext
	STRING sBlockedInteractionHelpText = ""
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_AttachedEntityIndex = -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].sInteractableTrainAttachmentData.iTrainIndex = -1
		RETURN FALSE
	ENDIF
	
	INT iRequiredState = g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_AttachedEntityRequiredState
	ENTITY_INDEX eiAttachedEntity = NULL
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_AttachedEntityIndex > -1
		eiAttachedEntity = GET_FMMC_ENTITY(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_AttachedEntityType, g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_AttachedEntityIndex)
	
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].sInteractableTrainAttachmentData.iTrainIndex > -1
		// If it's on a train, we want to grab the particular carriage it's attached to and we want to ensure that interaction is blocked if the carriage is moving
		iRequiredState = ciInteractable_AttachedEntityState_NotMoving
		eiAttachedEntity = GET_ENTITY_ATTACHED_TO(sInteractableState.objIndex)
		sBlockedInteractionHelpText = "MC_INTOBJ_TRAIN"
	ENDIF
	
	IF DOES_ENTITY_EXIST(eiAttachedEntity)
		SWITCH iRequiredState
			CASE ciInteractable_AttachedEntityState_Dead
				IF IS_ENTITY_ALIVE(eiAttachedEntity)
					IF bDisplayHelptext
					AND NOT IS_STRING_NULL_OR_EMPTY(sBlockedInteractionHelpText)
						DISPLAY_HELP_TEXT_THIS_FRAME(sBlockedInteractionHelpText, TRUE)
					ENDIF
					
					RETURN TRUE
				ENDIF
			BREAK
			
			CASE ciInteractable_AttachedEntityState_Stunned
			
				IF IS_ENTITY_DEAD(eiAttachedEntity)
					// Entity needs to be stunned, not dead
					IF bDisplayHelptext
					AND NOT IS_STRING_NULL_OR_EMPTY(sBlockedInteractionHelpText)
						DISPLAY_HELP_TEXT_THIS_FRAME(sBlockedInteractionHelpText, TRUE)
					ENDIF
					
					RETURN TRUE
				ENDIF
				
				IF g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_AttachedEntityType = ciENTITY_TYPE_PED
					PED_INDEX eiAttachedPed
					eiAttachedPed = GET_FMMC_ENTITY_PED(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_AttachedEntityIndex)
					
					IF IS_PED_RAGDOLL(eiAttachedPed)
					OR IS_PED_PERFORMING_TASK(eiAttachedPed, SCRIPT_TASK_NM_ELECTROCUTE)
					OR IS_PED_BEING_STUNNED(eiAttachedPed, WEAPONTYPE_STUNGUN)
						// Ped is stunned!
					ELSE
						// Ped isn't currently stunned
						IF bDisplayHelptext
						AND NOT IS_STRING_NULL_OR_EMPTY(sBlockedInteractionHelpText)
							DISPLAY_HELP_TEXT_THIS_FRAME(sBlockedInteractionHelpText, TRUE)
						ENDIF
						
						RETURN TRUE
					ENDIF
				ENDIF
			BREAK
			
			CASE ciInteractable_AttachedEntityState_NotMoving
				IF IS_ENTITY_ALIVE(eiAttachedEntity)
					IF VMAG(GET_ENTITY_VELOCITY(eiAttachedEntity)) > 0.05
					OR VMAG(GET_ENTITY_ROTATION_VELOCITY(eiAttachedEntity)) > 0.05
						IF bDisplayHelptext
						AND NOT IS_STRING_NULL_OR_EMPTY(sBlockedInteractionHelpText)
							DISPLAY_HELP_TEXT_THIS_FRAME(sBlockedInteractionHelpText, TRUE)
						ENDIF
						
						RETURN TRUE
					ENDIF
				ELSE
					// Attached entity is dead!
				ENDIF
			BREAK
		ENDSWITCH
	
	ELSE
		IF iRequiredState != ciInteractable_AttachedEntityState_Any
		AND iRequiredState != ciInteractable_AttachedEntityState_Dead
		AND iRequiredState != ciInteractable_AttachedEntityState_NotMoving
			// Blocked by entity not existing/our reference has cleaned up (Peds not set to stick around after death, etc)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_FLIP_DIRECTION_FOR_INTERACTABLE_FRONT_CHECK(FMMC_INTERACTABLE_STATE& sInteractableState)
	IF g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_InteractionType = ciInteractableInteraction_LootTray
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_INTERACTION_PROMPT_BE_BLOCKED_IF_POSITION_IS_OCCUPIED(FMMC_INTERACTABLE_STATE& sInteractableState)

	IF g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_InteractionType = ciInteractableInteraction_InteractWith
		IF SHOULD_INTERACT_WITH_PRESET_PROMPT_BE_BLOCKED_IF_POSITION_IS_OCCUPIED(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_InteractWithAnimPreset)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_PERFORM_INTERACTION_GENERIC_CHECKS(FMMC_INTERACTABLE_STATE& sInteractableState)

	IF g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_InteractionType = ciInteractableInteraction_InteractWith
		IF NOT DOES_CURRENT_INTERACT_WITH_FEATURE_A_PLAYER_ANIM()
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_INTERACTION_BE_ALLOWED_IN_VEHICLE(FMMC_INTERACTABLE_STATE& sInteractableState)

	IF g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_InteractionType = ciInteractableInteraction_InteractWith
		IF g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_InteractWithAnimPreset = ciINTERACT_WITH_PRESET__PLAYER_IN_CAR_ANIM
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_RequirePlayerInVehicleIndex > -1
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_INTERACTION_CHECK_FOR_LINE_OF_SIGHT(FMMC_INTERACTABLE_STATE& sInteractableState)

	IF g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_InteractionType = ciInteractableInteraction_InteractWith
		IF g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_InteractWithAnimPreset = ciINTERACT_WITH_PRESET__CLOTHES
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL CAN_INTERACT_WITH_INTERACTABLE(FMMC_INTERACTABLE_STATE& sInteractableState, BOOL bCheckPositioning = TRUE, BOOL bCheckInUse = TRUE, BOOL bCheckInputBlockers = FALSE, BOOL bIsBlipCheck = FALSE)
	
	BOOL bDisplayHelpText = (iClosestInteractable = sInteractableState.iIndex AND bCheckPositioning)
	
	IF NOT SHOULD_INTERACTABLE_INTERACTION_REQUIRE_PLAYER_INPUT_TO_START(sInteractableState)
		// Automatically triggered interactions shouldn't display "you can't use this" helptext.
		bDisplayHelpText = FALSE
	ENDIF
	
	IF NOT DOES_INTERACTABLE_HAVE_AN_INTERACTION_SET(sInteractableState.iIndex)
		// This is set up to not have any interaction!
		PRINTLN("[Interactables_SPAMCanInteract][Interactable_SPAMCanInteract ", sInteractableState.iIndex, "] CAN_INTERACT_WITH_INTERACTABLE | This Interactable has no interaction!")
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(gMC_LocalVariables_VARS.iLocalBGScriptBS, ciLocalBGScriptBS_BlockInteractionWithCurrentNearestInteractable)
		PRINTLN("[Interactables_SPAMCanInteract][Interactable_SPAMCanInteract ", sInteractableState.iIndex, "] CAN_INTERACT_WITH_INTERACTABLE | Blocking interaction due to ciLocalBGScriptBS_BlockInteractionWithCurrentNearestInteractable!")
		RETURN FALSE
	ENDIF
	
	IF NOT bIsBlipCheck
		IF bIsAnySpectator
			PRINTLN("[Interactables_SPAMCanInteract][Interactable_SPAMCanInteract ", sInteractableState.iIndex, "] CAN_INTERACT_WITH_INTERACTABLE | Blocking interaction due to bIsAnySpectator!")
			RETURN FALSE
		ENDIF
		
		IF MC_playerBD[iLocalPart].iOngoingInteractionCount >= ciINTERACTABLE_MAX_ONGOING_INTERACTIONS
			ASSERTLN("[Interactables_SPAMCanInteract][Interactable_SPAMCanInteract ", sInteractableState.iIndex, "] CAN_INTERACT_WITH_INTERACTABLE | Blocking interaction due to having too many background interactions going on right now!")
			PRINTLN("[Interactables_SPAMCanInteract][Interactable_SPAMCanInteract ", sInteractableState.iIndex, "] CAN_INTERACT_WITH_INTERACTABLE | Blocking interaction due to having too many background interactions going on right now!")
			#IF IS_DEBUG_BUILD
			ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_TOO_MANY_INTERACTIONS_GOING_ON_BLOCK, ENTITY_RUNTIME_ERROR_TYPE_WARNING_INTERACTABLE, "Blocking interaction due to having too many background interactions going on right now!", sInteractableState.iIndex)
			#ENDIF
			RETURN FALSE
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF bInteractableDebug
		IF IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD5)
			PRINTLN("[Interactables_CanInteract][Interactable_CanInteract ", sInteractableState.iIndex, "] CAN_INTERACT_WITH_INTERACTABLE | Returning TRUE due to Numpad5 debug!")
			RETURN TRUE
		ENDIF
	ENDIF
	#ENDIF
	
	IF bCheckInUse
	AND NOT bIsBlipCheck
		IF IS_BIT_SET(sInteractableState.iBitset, FMMC_INTERACTABLE_STATE_OCCUPIED)
		AND GET_INTERACTABLE_CURRENT_USER(sInteractableState.iIndex) != iLocalPart
			// Someone else is using this!
			#IF IS_DEBUG_BUILD
			IF bDisplayHelpText
				PRINTLN("[Interactables_CanInteract][Interactable_CanInteract ", sInteractableState.iIndex, "] CAN_INTERACT_WITH_INTERACTABLE | Interaction being used by participant ", MC_serverBD_4.sIntServerData.iInteractable_InteractingParticipant[sInteractableState.iIndex], "!")
			ELSE
				PRINTLN("[Interactables_SPAMCanInteract][Interactable_SPAMCanInteract ", sInteractableState.iIndex, "] CAN_INTERACT_WITH_INTERACTABLE | Interaction being used by participant ", MC_serverBD_4.sIntServerData.iInteractable_InteractingParticipant[sInteractableState.iIndex], "!")
			ENDIF
			#ENDIF
			RETURN FALSE
		ENDIF
		
		IF HAS_NET_TIMER_STARTED(stInteractableUsageCooldownTimer[sInteractableState.iIndex])
			IF NOT HAS_NET_TIMER_EXPIRED(stInteractableUsageCooldownTimer[sInteractableState.iIndex], ciInteractableUsageCooldownLength)
				// Someone else/I used this very recently - we enforce a cooldown here to mitigate slow serverBD issues
				#IF IS_DEBUG_BUILD
				IF bDisplayHelpText
					PRINTLN("[Interactables_CanInteract][Interactable_CanInteract ", sInteractableState.iIndex, "] CAN_INTERACT_WITH_INTERACTABLE | stInteractableUsageCooldownTimer is running!")
				ELSE
					PRINTLN("[Interactables_SPAMCanInteract][Interactable_SPAMCanInteract ", sInteractableState.iIndex, "] CAN_INTERACT_WITH_INTERACTABLE | stInteractableUsageCooldownTimer is running!")
				ENDIF
				#ENDIF
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF bCheckInputBlockers

		IF SHOULD_PERFORM_INTERACTION_GENERIC_CHECKS(sInteractableState)
			IF NOT bIsBlipCheck
				IF IS_INTERACTION_BLOCKED_DUE_TO_GENERIC_REASON(bDisplayHelpText, NOT SHOULD_INTERACTION_BE_ALLOWED_IN_VEHICLE(sInteractableState))
					#IF IS_DEBUG_BUILD
					IF bDisplayHelpText
						PRINTLN("[Interactables_CanInteract][Interactable_CanInteract ", sInteractableState.iIndex, "] CAN_INTERACT_WITH_INTERACTABLE | Blocked due to IS_INTERACTION_BLOCKED_DUE_TO_GENERIC_REASON (specific reason printed above)!")
					ELSE
						PRINTLN("[Interactables_SPAMCanInteract][Interactable_SPAMCanInteract ", sInteractableState.iIndex, "] CAN_INTERACT_WITH_INTERACTABLE | Blocked due to IS_INTERACTION_BLOCKED_DUE_TO_GENERIC_REASON (specific reason printed above)!")
					ENDIF
					#ENDIF
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_LOCAL_PLAYER_WEARING_OUTFIT_FROM_THIS_INTERACTABLE(sInteractableState)
			#IF IS_DEBUG_BUILD
			IF bDisplayHelpText
				PRINTLN("[Interactables_CanInteract][Interactable_CanInteract ", sInteractableState.iIndex, "] CAN_INTERACT_WITH_INTERACTABLE | Player is already wearing this outfit!")
			ELSE
				PRINTLN("[Interactables_SPAMCanInteract][Interactable_SPAMCanInteract ", sInteractableState.iIndex, "] CAN_INTERACT_WITH_INTERACTABLE | Player is already wearing this outfit!")
			ENDIF
			#ENDIF
			RETURN FALSE
		ENDIF
		
		IF IS_CUTSCENE_PLAYING()
			#IF IS_DEBUG_BUILD
			IF bDisplayHelpText
				PRINTLN("[Interactables_CanInteract][Interactable_CanInteract ", sInteractableState.iIndex, "] CAN_INTERACT_WITH_INTERACTABLE | Player is watching a cutscene!")
			ELSE
				PRINTLN("[Interactables_SPAMCanInteract][Interactable_SPAMCanInteract ", sInteractableState.iIndex, "] CAN_INTERACT_WITH_INTERACTABLE | Player is watching a cutscene!")
			ENDIF
			#ENDIF
			RETURN FALSE
		ENDIF
		
		IF IS_SCREEN_FADING_OUT()
			#IF IS_DEBUG_BUILD
			IF bDisplayHelpText
				PRINTLN("[Interactables_CanInteract][Interactable_CanInteract ", sInteractableState.iIndex, "] CAN_INTERACT_WITH_INTERACTABLE | Screen is fading out!")
			ELSE
				PRINTLN("[Interactables_SPAMCanInteract][Interactable_SPAMCanInteract ", sInteractableState.iIndex, "] CAN_INTERACT_WITH_INTERACTABLE | Screen is fading out!")
			ENDIF
			#ENDIF
			RETURN FALSE
		ENDIF
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_InteractionType = ciInteractableInteraction_InteractWith
		AND g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_InteractWithAnimPreset = ciINTERACT_WITH_PRESET__CUTSCENE_LEAD_OUT
			IF NOT IS_SCREEN_FADED_OUT()
				#IF IS_DEBUG_BUILD
				IF bDisplayHelpText
					PRINTLN("[Interactables_CanInteract][Interactable_CanInteract ", sInteractableState.iIndex, "] CAN_INTERACT_WITH_INTERACTABLE | Waiting for screen to be fully faded out!")
				ELSE
					PRINTLN("[Interactables_SPAMCanInteract][Interactable_SPAMCanInteract ", sInteractableState.iIndex, "] CAN_INTERACT_WITH_INTERACTABLE | Waiting for screen to be fully faded out!")
				ENDIF
				#ENDIF
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_BlockInteractionRuleBS[GET_LOCAL_PLAYER_TEAM()], GET_LOCAL_PLAYER_CURRENT_RULE())
		#IF IS_DEBUG_BUILD
		// Blocked on this rule!
		IF bDisplayHelpText
			PRINTLN("[Interactables_CanInteract][Interactable_CanInteract ", sInteractableState.iIndex, "] CAN_INTERACT_WITH_INTERACTABLE | Interaction is blocked on this rule for my team!")
		ELSE
			PRINTLN("[Interactables_SPAMCanInteract][Interactable_SPAMCanInteract ", sInteractableState.iIndex, "] CAN_INTERACT_WITH_INTERACTABLE | Interaction is blocked on this rule for my team!")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_INTERACTABLE_COMPLETE(sInteractableState.iIndex, TRUE)
		// Interaction already complete!
		#IF IS_DEBUG_BUILD
		IF bDisplayHelpText
			PRINTLN("[Interactables_CanInteract][Interactable_CanInteract ", sInteractableState.iIndex, "] CAN_INTERACT_WITH_INTERACTABLE | This Interactable is completed!")
		ELSE
			PRINTLN("[Interactables_SPAMCanInteract][Interactable_SPAMCanInteract ", sInteractableState.iIndex, "] CAN_INTERACT_WITH_INTERACTABLE | This Interactable is completed!")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractableBS, ciInteractableBS_ForceSingleUseOnly)
	AND FMMC_IS_LONG_BIT_SET(iInteractable_WasCompletedBS, sInteractableState.iIndex)
		// Interaction already complete!
		#IF IS_DEBUG_BUILD
		IF bDisplayHelpText
			PRINTLN("[Interactables_CanInteract][Interactable_CanInteract ", sInteractableState.iIndex, "] CAN_INTERACT_WITH_INTERACTABLE | This Interactable is single-use, and has been completed before!")
		ELSE
			PRINTLN("[Interactables_SPAMCanInteract][Interactable_SPAMCanInteract ", sInteractableState.iIndex, "] CAN_INTERACT_WITH_INTERACTABLE | This Interactable is single-use, and has been completed before!")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF NOT bIsBlipCheck
		IF MC_playerBD[iLocalPart].iObjHacking > -1
			#IF IS_DEBUG_BUILD
			IF bDisplayHelpText
				PRINTLN("[Interactables_CanInteract][Interactable_CanInteract ", sInteractableState.iIndex, "] CAN_INTERACT_WITH_INTERACTABLE | Player is currently hacking obj " , MC_playerBD[iLocalPart].iObjHacking)
			ELSE
				PRINTLN("[Interactables_SPAMCanInteract][Interactable_SPAMCanInteract ", sInteractableState.iIndex, "] CAN_INTERACT_WITH_INTERACTABLE | Player is currently hacking obj " , MC_playerBD[iLocalPart].iObjHacking)
			ENDIF
			#ENDIF
			
			RETURN FALSE
		ENDIF
	ENDIF
	
	BOOL bPlayerInRequiredVehicle = FALSE
	VEHICLE_INDEX viRequiredVehicle = NULL
	IF g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_RequirePlayerInVehicleIndex > -1
		viRequiredVehicle = GET_FMMC_ENTITY_VEHICLE(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_RequirePlayerInVehicleIndex)
		
		IF IS_ENTITY_ALIVE(viRequiredVehicle)
		AND IS_VEHICLE_DRIVEABLE(viRequiredVehicle)
			bPlayerInRequiredVehicle = IS_PED_IN_VEHICLE(LocalPlayerPed, viRequiredVehicle, FALSE)
		ENDIF
		
		IF NOT bPlayerInRequiredVehicle
			#IF IS_DEBUG_BUILD
			IF bDisplayHelpText
				PRINTLN("[Interactables_CanInteract][Interactable_CanInteract ", sInteractableState.iIndex, "] CAN_INTERACT_WITH_INTERACTABLE | Player isn't in the required vehicle! Vehicle: ", g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_RequirePlayerInVehicleIndex)
			ELSE
				PRINTLN("[Interactables_SPAMCanInteract][Interactable_SPAMCanInteract ", sInteractableState.iIndex, "] CAN_INTERACT_WITH_INTERACTABLE | Player isn't in the required vehicle! Vehicle: ", g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_RequirePlayerInVehicleIndex)
			ENDIF
			#ENDIF
			
			RETURN FALSE
		ELSE
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractableBS, ciInteractableBS_OnlyVehicleDriverCanInteract)
				// Return FALSE early here if there isn't anyone in the driver's seat, to prevent any objective text confusion when sitting in a passenger seat or shuffling seats
				IF IS_VEHICLE_SEAT_FREE(viRequiredVehicle, VS_DRIVER)
					#IF IS_DEBUG_BUILD
					IF bDisplayHelpText
						PRINTLN("[Interactables_CanInteract][Interactable_CanInteract ", sInteractableState.iIndex, "] CAN_INTERACT_WITH_INTERACTABLE | Blocking because there's no driver in Vehicle: ", g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_RequirePlayerInVehicleIndex)
					ELSE
						PRINTLN("[Interactables_SPAMCanInteract][Interactable_SPAMCanInteract ", sInteractableState.iIndex, "] CAN_INTERACT_WITH_INTERACTABLE | Blocking because there's no driver in Vehicle: ", g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_RequirePlayerInVehicleIndex)
					ENDIF
					#ENDIF
					
					RETURN FALSE
				ENDIF
			ENDIF
			
			//The normal "Driver Only" check is within the Input Blockers section
		ENDIF
	ENDIF
	
	IF bCheckPositioning
		
		IF IS_BIT_SET(iSharedInteractableBS, ciSHARED_INTERACTABLE_BS__INPUT_BLOCKED_BY_INTERACT_WITH_POSITION)
			// Wrong position!
			#IF IS_DEBUG_BUILD
			IF bDisplayHelpText
				PRINTLN("[Interactables_CanInteract][Interactable_CanInteract ", sInteractableState.iIndex, "] CAN_INTERACT_WITH_INTERACTABLE | Position is wrong! ciSHARED_INTERACTABLE_BS__INPUT_BLOCKED_BY_INTERACT_WITH_POSITION is set")
			ELSE
				PRINTLN("[Interactables_SPAMCanInteract][Interactable_SPAMCanInteract ", sInteractableState.iIndex, "] CAN_INTERACT_WITH_INTERACTABLE | Position is wrong! ciSHARED_INTERACTABLE_BS__INPUT_BLOCKED_BY_INTERACT_WITH_POSITION is set")
			ENDIF
			#ENDIF
			
			RETURN FALSE
		ENDIF
		
		IF sInteractableState.fDistanceToInteractable > POW(GET_INTERACTABLE_INTERACTION_COORDS_MAX_DISTANCE(sInteractableState), 2.0)
		AND NOT IS_INTERACTABLE_USABLE_FROM_ANY_DISTANCE(sInteractableState.iIndex)
			// Too far away!
			#IF IS_DEBUG_BUILD
			IF bDisplayHelpText
				PRINTLN("[Interactables_CanInteract][Interactable_CanInteract ", sInteractableState.iIndex, "] CAN_INTERACT_WITH_INTERACTABLE | Too far away! sInteractableState.fDistanceToInteractable: ", sInteractableState.fDistanceToInteractable)
			ELSE
				PRINTLN("[Interactables_SPAMCanInteract][Interactable_SPAMCanInteract ", sInteractableState.iIndex, "] CAN_INTERACT_WITH_INTERACTABLE | Too far away! sInteractableState.fDistanceToInteractable: ", sInteractableState.fDistanceToInteractable)
			ENDIF
			#ENDIF
			
			RETURN FALSE
		ENDIF
		
		IF sInteractableState.fInteractionHeading != 999.9
			IF NOT IS_PED_CLOSE_TO_HEADING(LocalPlayerPed, sInteractableState.fInteractionHeading, cfInteractWith__InteractionAllowedMaxHeadingDifference)
				// Facing the wrong way!
				
				#IF IS_DEBUG_BUILD
				IF bInteractableDebug
				AND bInteractWithDebug
					TEXT_LABEL_63 tlVisualDebug = "Req Heading:  "
					tlVisualDebug += FLOAT_TO_STRING(sInteractableState.fInteractionHeading)
					tlVisualDebug +=  " Cur Heading:  "
					tlVisualDebug +=  FLOAT_TO_STRING(GET_ENTITY_HEADING( LocalPlayerPed ))
					tlVisualDebug +=  " Delta Heading:  "
					tlVisualDebug += FLOAT_TO_STRING(GET_ABSOLUTE_DELTA_HEADING( GET_ENTITY_HEADING( LocalPlayerPed ), sInteractableState.fInteractionHeading ))
					DRAW_DEBUG_TEXT_2D(tlVisualDebug, (<<0.1, 0.25, 0.5>>), 255, 0, 0, 255)
				ENDIF
				#ENDIF
				
				#IF IS_DEBUG_BUILD
				IF bDisplayHelpText
					PRINTLN("[Interactables_CanInteract][Interactable_CanInteract ", sInteractableState.iIndex, "] CAN_INTERACT_WITH_INTERACTABLE | Facing the wrong way!")
				ELSE
					PRINTLN("[Interactables_SPAMCanInteract][Interactable_SPAMCanInteract ", sInteractableState.iIndex, "] CAN_INTERACT_WITH_INTERACTABLE | Facing the wrong way!")
				ENDIF
				#ENDIF
				RETURN FALSE
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractableBS, ciInteractableBS_OneSidedPrompt)
			// Check the player is in front of the Interactable
			VECTOR vPlayerPosInObjectSpace = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(sInteractableState.objIndex, GET_ENTITY_COORDS(LocalPlayerPed))
			
			IF (vPlayerPosInObjectSpace.y <= 0.1 AND NOT SHOULD_FLIP_DIRECTION_FOR_INTERACTABLE_FRONT_CHECK(sInteractableState))
			OR (vPlayerPosInObjectSpace.y >= -0.1 AND SHOULD_FLIP_DIRECTION_FOR_INTERACTABLE_FRONT_CHECK(sInteractableState))
				#IF IS_DEBUG_BUILD
				IF bInteractableDebug
				AND bInteractWithDebug
					TEXT_LABEL_63 tlVisualDebug = "On the wrong side! vPlayerPosInObjectSpace.y = "
					tlVisualDebug += FLOAT_TO_STRING(vPlayerPosInObjectSpace.y)
					DRAW_DEBUG_TEXT_2D(tlVisualDebug, (<<0.1, 0.25, 0.5>>), 255, 0, 0, 255)
				ENDIF
				#ENDIF
				
				PRINTLN("[Interactables_SPAMCanInteract][Interactable_SPAMCanInteract ", sInteractableState.iIndex, "] CAN_INTERACT_WITH_INTERACTABLE | Need to be on the other side! vPlayerPosInObjectSpace: ", vPlayerPosInObjectSpace)
				RETURN FALSE
			ENDIF
		ENDIF
		
		IF SHOULD_INTERACTION_PROMPT_BE_BLOCKED_IF_POSITION_IS_OCCUPIED(sInteractableState)
			IF NOT IS_INTERACTION_START_POSITION_CLEAR(sInteractableState.vInteractionCoords)
				PRINTLN("[Interactables_SPAMCanInteract][Interactable_SPAMCanInteract ", sInteractableState.iIndex, "] CAN_INTERACT_WITH_INTERACTABLE | Not allowing interaction due to IS_INTERACTION_START_POSITION_CLEAR.")
				RETURN FALSE
			ENDIF
		ENDIF
		
		IF SHOULD_INTERACTION_CHECK_FOR_LINE_OF_SIGHT(sInteractableState)
			IF NOT HAS_ENTITY_CLEAR_LOS_TO_ENTITY(LocalPlayerPed, sInteractableState.objIndex, LOS_FLAGS_BOUNDING_BOX)
				#IF IS_DEBUG_BUILD
				IF bDisplayHelpText
					PRINTLN("[Interactab_CanInteract][Interactable_CanInteract ", sInteractableState.iIndex, "] CAN_INTERACT_WITH_INTERACTABLE | Blocked due to HAS_ENTITY_CLEAR_LOS_TO_ENTITY")
				ELSE
					PRINTLN("[Interactables_SPAMCanInteract][Interactable_SPAMCanInteract ", sInteractableState.iIndex, "] CAN_INTERACT_WITH_INTERACTABLE | Blocked due to HAS_ENTITY_CLEAR_LOS_TO_ENTITY")
				ENDIF
				#ENDIF
				RETURN FALSE
			ENDIF
		ENDIF
		
	ENDIF
	
	IF IS_BIT_SET(gMC_LocalVariables_VARS.iLocalBGScriptBS, ciLocalBGScriptBS_ForceAllowInteractionWithCurrentNearestInteractable)
		PRINTLN("[Interactables_SPAMCanInteract][Interactable_SPAMCanInteract ", sInteractableState.iIndex, "] CAN_INTERACT_WITH_INTERACTABLE | Force-allowing interaction due to ciLocalBGScriptBS_ForceAllowInteractionWithCurrentNearestInteractable!")
		RETURN TRUE
	ENDIF
	
	// Interaction blockers that require help text to explain why.
	IF bCheckInputBlockers
		IF g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_InteractionType = ciInteractableInteraction_LootTray
		OR g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_InteractionType = ciInteractableInteraction_CutPainting
			IF NOT IS_THERE_ENOUGH_SPACE_IN_BAG_FOR_LOOT(sInteractableState.mn)
				IF bDisplayHelpText
					PRINTLN("[Interactables_CanInteract][Interactable_CanInteract ", sInteractableState.iIndex, "] CAN_INTERACT_WITH_INTERACTABLE | Not enough space in bag!")
					DISPLAY_HELP_TEXT_THIS_FRAME("MC_BAG_FULL", TRUE)
				ELSE
					PRINTLN("[Interactables_SPAMCanInteract][Interactable_SPAMCanInteract ", sInteractableState.iIndex, "] CAN_INTERACT_WITH_INTERACTABLE | Not enough space in bag!")
				ENDIF
				RETURN FALSE
			ENDIF
		ENDIF
		
		INT iPreReq = 0
		FOR iPreReq = 0 TO g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_PreReqsRequiredCount - 1
			IF NOT IS_PREREQUISITE_COMPLETED(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_PreReqsRequired[iPreReq])
				// Haven't completed required this required prereq yet!
				IF bDisplayHelpText
					DISPLAY_INTERACTABLE_NOT_MET_PREREQ_HELPTEXT_THIS_FRAME(sInteractableState)
					PRINTLN("[Interactables_CanInteract][Interactable_CanInteract ", sInteractableState.iIndex, "] CAN_INTERACT_WITH_INTERACTABLE | Need to complete assigned prereq ", iPreReq, "  (PreReq ", g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_PreReqsRequired[iPreReq], ")")
				ELSE
					PRINTLN("[Interactables_SPAMCanInteract][Interactable_SPAMCanInteract ", sInteractableState.iIndex, "] CAN_INTERACT_WITH_INTERACTABLE | Need to complete assigned prereq ", iPreReq, "  (PreReq ", g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_PreReqsRequired[iPreReq], ")")
				ENDIF
				
				RETURN FALSE
			ENDIF
		ENDFOR
		
		IF IS_INTERACTABLE_INTERACTION_BLOCKED_BY_ATTACHED_ENTITY(sInteractableState, bDisplayHelpText)
			PRINTLN("[Interactables_CanInteract][Interactable_CanInteract ", sInteractableState.iIndex, "] CAN_INTERACT_WITH_INTERACTABLE | Blocked by attached entity!")
			RETURN FALSE
		ENDIF
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].sInteractableTrainAttachmentData.iTrainIndex > -1
			IF NOT IS_TRAIN_PERFECTLY_STILL(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].sInteractableTrainAttachmentData.iTrainIndex)
				PRINTLN("[Interactables_CanInteract][Interactable_CanInteract ", sInteractableState.iIndex, "] CAN_INTERACT_WITH_INTERACTABLE | Blocked by being attached to a moving train!")
				IF bDisplayHelpText
					DISPLAY_HELP_TEXT_THIS_FRAME("MC_INTOBJ_TRAIN", TRUE)
				ENDIF
				
				RETURN FALSE
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractableBS, ciInteractableBS_BlockInteractionOnAggro)
			IF HAS_LOCAL_PLAYER_TEAM_TRIGGERED_AGGRO(TRUE, g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iAggroIndexBS_Entity_Interactable)
			
				IF bDisplayHelpText
					DISPLAY_HELP_TEXT_THIS_FRAME("MC_INTOBJ_AGGR", TRUE)
				ENDIF
				
				PRINTLN("[Interactables_CanInteract][Interactable_CanInteract ", sInteractableState.iIndex, "] CAN_INTERACT_WITH_INTERACTABLE | Blocked by aggro!")
				RETURN FALSE
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractableBS, ciInteractableBS_OnlyLobbyLeaderCanInteract)
			IF IS_LOCAL_PLAYER_ABLE_TO_DO_LEADER_RESTRICTED_INTERACTION()
				// All good
			ELSE
				IF bDisplayHelpText
					PRINTLN("[Interactables_CanInteract][Interactable_CanInteract ", sInteractableState.iIndex, "] CAN_INTERACT_WITH_INTERACTABLE | This Interactable is only available for the lobby leader!")
					
					IF SHOULD_INTERACTABLE_INTERACTION_REQUIRE_PLAYER_INPUT_TO_START(sInteractableState)	
						DISPLAY_HELP_TEXT_THIS_FRAME("MC_INTOBJ_LOBB", TRUE)
					ENDIF
				ELSE
					PRINTLN("[Interactables_SPAMCanInteract][Interactable_SPAMCanInteract ", sInteractableState.iIndex, "] CAN_INTERACT_WITH_INTERACTABLE | This Interactable is only available for the lobby leader!")
				ENDIF
				
				RETURN FALSE
			ENDIF
		ENDIF
		
		IF bPlayerInRequiredVehicle
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractableBS, ciInteractableBS_OnlyVehicleDriverCanInteract)
			AND GET_PED_IN_VEHICLE_SEAT(viRequiredVehicle, VS_DRIVER) != PlayerPedToUse
				#IF IS_DEBUG_BUILD
				IF bDisplayHelpText
					PRINTLN("[Interactables_CanInteract][Interactable_CanInteract ", sInteractableState.iIndex, "] CAN_INTERACT_WITH_INTERACTABLE | Player isn't the driver of the required vehicle! Vehicle: ", g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_RequirePlayerInVehicleIndex)
				ELSE
					PRINTLN("[Interactables_SPAMCanInteract][Interactable_SPAMCanInteract ", sInteractableState.iIndex, "] CAN_INTERACT_WITH_INTERACTABLE | Player isn't the driver of the required vehicle! Vehicle: ", g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_RequirePlayerInVehicleIndex)
				ENDIF
				#ENDIF
				
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_LOCAL_PLAYER_INTERACTING()
		#IF IS_DEBUG_BUILD
		IF bDisplayHelpText
			PRINTLN("[Interactables_CanInteract][Interactable_CanInteract ", sInteractableState.iIndex, "] CAN_INTERACT_WITH_INTERACTABLE | Can Interact!")
		ELSE
			PRINTLN("[Interactables_SPAMCanInteract][Interactable_SPAMCanInteract ", sInteractableState.iIndex, "] CAN_INTERACT_WITH_INTERACTABLE | Can Interact!")
		ENDIF
		#ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_BAIL_INTERACTION(FMMC_INTERACTABLE_STATE& sInteractableState)

	UNUSED_PARAMETER(sInteractableState)
	
	IF HAS_INTERACTABLE_BAIL_TIMER_EXPIRED()
		PRINTLN("[Interactables][Interactable ", sInteractableState.iIndex, "] SHOULD_BAIL_INTERACTION | Bailing due to HAS_INTERACTABLE_BAIL_TIMER_EXPIRED")
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(iSharedInteractableBS, ciSHARED_INTERACTABLE_BS__BAIL_FOREGROUND_INTERACTION_ASAP)
		PRINTLN("[Interactables][Interactable ", sInteractableState.iIndex, "] SHOULD_BAIL_INTERACTION | Bailing due to ciSHARED_INTERACTABLE_BS__BAIL_FOREGROUND_INTERACTION_ASAP")
		RETURN TRUE
		
	ELIF IS_BIT_SET(iSharedInteractableBS, ciSHARED_INTERACTABLE_BS__BAIL_FOREGROUND_INTERACTION_DUE_TO_MULTI_INTERACT)
		PRINTLN("[Interactables][Interactable ", sInteractableState.iIndex, "] SHOULD_BAIL_INTERACTION | Bailing due to ciSHARED_INTERACTABLE_BS__BAIL_FOREGROUND_INTERACTION_DUE_TO_MULTI_INTERACT")
		RETURN TRUE
	ENDIF
	
	IF NOT bLocalPlayerOK
		PRINTLN("[Interactables][Interactable ", sInteractableState.iIndex, "] SHOULD_BAIL_INTERACTION | Bailing due to bLocalPlayerOK being FALSE")
		RETURN TRUE
	ENDIF
	
	IF NOT bLocalPlayerPedOk
		PRINTLN("[Interactables][Interactable ", sInteractableState.iIndex, "] SHOULD_BAIL_INTERACTION | Bailing due to bLocalPlayerPedOk being FALSE")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SHOW_INTERACTABLE_CONTROLS_HELPTEXT(STRING sHelptextForController, STRING sHelptextForMouseAndKeyboard)
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		DISPLAY_HELP_TEXT_THIS_FRAME(sHelptextForMouseAndKeyboard, TRUE)
	ELSE
		DISPLAY_HELP_TEXT_THIS_FRAME(sHelptextForController, TRUE)
	ENDIF
ENDPROC

FUNC BOOL SHOULD_INTERACTION_BE_UNDONE_IF_PLAYER_STOPS_INTERACTING(INT iInteractable)
	IF g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iInteractable_InteractionType = ciInteractableInteraction_InteractWith
	AND IS_INTERACT_WITH_ANIM_PRESET_A_HOLD_INPUT(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iInteractable_InteractWithAnimPreset, g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iInteractable_InteractWithAltAnimSet)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_INTERACTABLE_COMPLETION_CONTINUITY(INT iInteractable)

	IF iInteractable = -1
		EXIT
	ENDIF

	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_ContentSpecificTracking)
		EXIT
	ENDIF
	
	INT iContentContinuityType = g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iContentContinuityType
	IF iContentContinuityType > 0
		PROCESS_SPECIFIC_CONTENT_CONTINUITY_TYPE_TELEMETRY(iContentContinuityType)
		SET_BIT(MC_serverBD_1.sMissionContinuityVars.iContentSpecificContinuityBitset, iContentContinuityType)
		PRINTLN("[Interactables][Interactable ", iInteractable, "] PROCESS_INTERACTABLE_COMPLETION_CONTINUITY | Content Continuity Type Set: ", iContentContinuityType)
	ENDIF
	
ENDPROC

PROC SET_INTERACTABLE_COMPLETE(INT iInteractable, BOOL bCompletedByContinuity = FALSE)

	IF iInteractable = -1
		EXIT
	ENDIF
	
	IF IS_INTERACTABLE_COMPLETE(iInteractable, TRUE)
		// Already set as complete
		PRINTLN("[Interactables][Interactable ", iInteractable, "] SET_INTERACTABLE_COMPLETE | Interactable is already marked as complete")
		EXIT
	ENDIF
	
	IF bCompletedByContinuity
		PRINTLN("[Interactables][Interactable ", iInteractable, "] SET_INTERACTABLE_COMPLETE | Interaction is complete due to being completed on a previous mission! Broadcasting the event now...")
		BROADCAST_FMMC_INTERACTABLE_EVENT(FMMC_INTERACTABLE_EVENT_TYPE__INTERACTION_COMPLETED_ON_PREVIOUS_MISSION, iInteractable)
	ELSE
		PRINTLN("[Interactables][Interactable ", iInteractable, "] SET_INTERACTABLE_COMPLETE | Interaction is complete!! Broadcasting the event now...")
		BROADCAST_FMMC_INTERACTABLE_EVENT(FMMC_INTERACTABLE_EVENT_TYPE__INTERACTION_COMPLETE, iInteractable)
		
		INCREMENT_MEDAL_INTERACTABLES_COMPLETED()
		PROCESS_INTERACTABLE_COMPLETION_CONTINUITY(iInteractable)
	ENDIF
ENDPROC

PROC SET_INTERACTABLE_INCOMPLETE(INT iInteractable, BOOL bBlockIfSetToStayCompleteForever = TRUE)
	
	IF iInteractable = -1
		EXIT
	ENDIF
	
	IF NOT IS_INTERACTABLE_COMPLETE(iInteractable, TRUE, FALSE)
		// Isn't set as complete in the first place
		EXIT
	ENDIF
	
	IF bBlockIfSetToStayCompleteForever
		IF FMMC_IS_LONG_BIT_SET(iInteractable_StayCompleteForeverBS, iInteractable)
			PRINTLN("[Interactables][Interactable ", iInteractable, "] SET_INTERACTABLE_INCOMPLETE | Blocking setting the Interactable as incomplete due to iInteractable_StayCompleteForeverBS - This Interactable has been overridden by a linked Interactable to stay complete forever.")
			EXIT
		ENDIF
	ENDIF
	
	PRINTLN("[Interactables][Interactable ", iInteractable, "] SET_INTERACTABLE_INCOMPLETE | Interaction is no longer complete!! Broadcasting the event now...")
	BROADCAST_FMMC_INTERACTABLE_EVENT(FMMC_INTERACTABLE_EVENT_TYPE__INTERACTION_NO_LONGER_COMPLETE, iInteractable)
ENDPROC

PROC START_INTERACTABLE_INTERACTION(INT iInteractable)
	
	IF IS_LOCAL_PLAYER_INTERACTING(iInteractable, FALSE)
		// We're already interacting with this one (background or foreground)
		EXIT
	ENDIF
	
	BROADCAST_FMMC_INTERACTABLE_EVENT(FMMC_INTERACTABLE_EVENT_TYPE__PLAYER_STARTED_INTERACTING, iInteractable)
	
	IF NOT IS_BIT_SET(gMC_LocalVariables_VARS.iLocalBGScriptBS, ciLocalBGScriptBS_DoNotDisablePlayerControl)
		NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE, (NSPC_REENABLE_CONTROL_ON_DEATH | NSPC_LEAVE_CAMERA_CONTROL_ON | NSPC_ALLOW_PLAYER_DAMAGE | NSPC_ALLOW_PAD_SHAKE | NSPC_DONT_CLEAR_TASKS_ON_RESUME_CONTROL))
	ENDIF
	
	// Initialise the Interaction vars with a clear struct - Put this interaction on the end of the list
	INT iInteractionSlotToFill = MC_playerBD[iLocalPart].iOngoingInteractionCount
	
	INTERACTION_VARS sNewInteractionVars
	sOngoingInteractionVars[iInteractionSlotToFill] = sNewInteractionVars
	sOngoingInteractionVars[iInteractionSlotToFill].iInteractable = iInteractable
	
	MC_playerBD[iLocalPart].iCurrentInteractables[iInteractionSlotToFill] = iInteractable
	MC_playerBD[iLocalPart].iOngoingInteractionCount++
	
	PRINTLN("[Interactables][Interactable ", iInteractable, "] START_INTERACTABLE_INTERACTION | Setting MC_playerBD[", iLocalPart, "].iCurrentInteractables[", iInteractionSlotToFill, "] to ", MC_playerBD[iLocalPart].iCurrentInteractables[iInteractionSlotToFill])
ENDPROC

PROC CLEAR_INTERACTION_VARS(INT iOngoingInteractionIndex)

	INT iPTFX
	FOR iPTFX = 0 TO ciINTERACTION_MAX_PTFX - 1
		IF DOES_PARTICLE_FX_LOOPED_EXIST(sOngoingInteractionVars[iOngoingInteractionIndex].ptInteractionPTFX[iPTFX])
			STOP_PARTICLE_FX_LOOPED(sOngoingInteractionVars[iOngoingInteractionIndex].ptInteractionPTFX[iPTFX])
		ENDIF
	ENDFOR
	
	INTERACTION_VARS sClearStruct
	sOngoingInteractionVars[iOngoingInteractionIndex] = sClearStruct
	
ENDPROC

PROC END_INTERACTABLE_INTERACTION(INT iInteractable)
	
	IF NOT IS_LOCAL_PLAYER_INTERACTING(iInteractable, FALSE)
		// The player isn't interacting with this Interactable at all
		EXIT
	ENDIF
	
	INT iOngoingInteractionIndex = GET_ONGOING_INTERACTION_INDEX(iInteractable)
	BOOL bIsForegroundInteraction = IS_ONGOING_INTERACTION_FOREGROUND(iOngoingInteractionIndex)
	
	IF SHOULD_INTERACTION_BE_UNDONE_IF_PLAYER_STOPS_INTERACTING(iInteractable)
		SET_INTERACTABLE_INCOMPLETE(iInteractable)
	ENDIF
	
	PRINTLN("[Interactables][Interactable ", iInteractable, "] END_INTERACTABLE_INTERACTION | Ending interaction ", iOngoingInteractionIndex, " which was with Interactable ", iInteractable)
	DEBUG_PRINTCALLSTACK()
	
	BROADCAST_FMMC_INTERACTABLE_EVENT(FMMC_INTERACTABLE_EVENT_TYPE__PLAYER_STOPPED_INTERACTING, iInteractable)
	
	// Clear the interaction vars
	CLEAR_INTERACTION_VARS(iOngoingInteractionIndex)
	MC_playerBD[iLocalPart].iCurrentInteractables[iOngoingInteractionIndex] = -1
	
	INT iShuffleDownIndex
	FOR iShuffleDownIndex = iOngoingInteractionIndex TO MC_playerBD[iLocalPart].iOngoingInteractionCount - 2
		MC_playerBD[iLocalPart].iCurrentInteractables[iShuffleDownIndex] = MC_playerBD[iLocalPart].iCurrentInteractables[iShuffleDownIndex + 1]
		sOngoingInteractionVars[iShuffleDownIndex] = sOngoingInteractionVars[iShuffleDownIndex + 1]
	ENDFOR
	
	IF iInteractable = iInteractable_CachedSecondaryAnimInteractable
		CLEAN_UP_INTERACTION_SECONDARY_ANIM(TRUE)
	ENDIF
	
	MC_playerBD[iLocalPart].iOngoingInteractionCount--
	
	// Interaction-Specific Cleanup below //
	IF bIsForegroundInteraction
		IF NOT IS_BIT_SET(gMC_LocalVariables_VARS.iLocalBGScriptBS, ciLocalBGScriptBS_DoNotDisablePlayerControl)
			NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
		ENDIF
		
		CLEAR_BIT(iSharedInteractableBS, ciSHARED_INTERACTABLE_BS__BAIL_FOREGROUND_INTERACTION_DUE_TO_MULTI_INTERACT)
		CLEAR_BIT(iSharedInteractableBS, ciSHARED_INTERACTABLE_BS__BAIL_FOREGROUND_INTERACTION_ASAP)
		RESET_INTERACTABLE_BAIL_TIMER()
		
		RESET_SYNC_LOCK_VARS(TRUE, TRUE)
		RESET_CUT_PAINTING_VARS()
	ENDIF
	
ENDPROC

PROC END_ALL_INTERACTIONS()
	INT iLoop
	FOR iLoop = 0 TO MC_playerBD[iLocalPart].iOngoingInteractionCount - 1
		END_INTERACTABLE_INTERACTION(sOngoingInteractionVars[iLoop].iInteractable)
	ENDFOR
ENDPROC

PROC SET_CLOSEST_INTERACTABLE(INT iInteractable)
	IF iInteractable = iClosestInteractable
		// This is already our closest Interactable!
		EXIT
	ENDIF
	
	IF iClosestInteractable != -1
		// Unload anything loaded for the previous closest interactable before we lose our reference to it
		UNLOAD_INTERACTABLE_PREPARATION(iClosestInteractable)
	ENDIF
	
	iClosestInteractable = iInteractable
	PRINTLN("[Interactables][Interactable ", iInteractable, "] SET_CLOSEST_INTERACTABLE | Setting iClosestInteractable to ", iClosestInteractable)
ENDPROC

PROC SET_INTERACTABLE_OBJECTIVE_TEXT_TEXTLABEL(STRING sObjTxtLabel)
	tlInteractableObjectiveText = GET_FILENAME_FOR_AUDIO_CONVERSATION(sObjTxtLabel)
	SET_BIT(iSharedInteractableBS, ciSHARED_INTERACTABLE_BS__DISPLAYED_OBJECTIVE_TEXT_THIS_FRAME)
	PRINTLN("[Interactables_SPAM][Interactable_SPAM ", iClosestInteractable, "] SET_INTERACTABLE_OBJECTIVE_TEXT_TEXTLABEL | Setting tlInteractableObjectiveText to ", tlInteractableObjectiveText, " (", sObjTxtLabel, ")")
ENDPROC

PROC SET_INTERACTABLE_OBJECTIVE_TEXT_LITERAL(TEXT_LABEL_63 tlObjTxtLiteral)
	tlInteractableObjectiveText = tlObjTxtLiteral
	SET_BIT(iSharedInteractableBS, ciSHARED_INTERACTABLE_BS__DISPLAYED_OBJECTIVE_TEXT_THIS_FRAME)
	PRINTLN("[Interactables_SPAM][Interactable_SPAM ", iClosestInteractable, "] SET_INTERACTABLE_OBJECTIVE_TEXT_LITERAL | Setting tlInteractableObjectiveText to ", tlInteractableObjectiveText)
ENDPROC

PROC CLEAR_INTERACTABLE_OBJECTIVE_TEXT()
	tlInteractableObjectiveText = ""
	CLEAR_BIT(iSharedInteractableBS, ciSHARED_INTERACTABLE_BS__DISPLAYED_OBJECTIVE_TEXT_THIS_FRAME)
ENDPROC

PROC RESET_INTERACTABLE(INT iInteractable)
	
	PRINTLN("[Interactables][Interactable ", iInteractable, "] RESET_INTERACTABLE | Resetting Interactable due to an interaction going wrong!")
	DEBUG_PRINTCALLSTACK()
	
	NETWORK_INDEX niInteractable = GET_INTERACTABLE_NET_ID(iInteractable)
	
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niInteractable)
		EXIT
	ENDIF
	
	OBJECT_INDEX oiInteractable = NET_TO_OBJ(niInteractable)
	PRINTLN("[Interactables][Interactable ", iInteractable, "] RESET_INTERACTABLE | model: ", FMMC_GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(oiInteractable)), " || ", NATIVE_TO_INT(niInteractable))
	
	IF NETWORK_HAS_CONTROL_OF_ENTITY(oiInteractable)
		VECTOR vResetCoords = MC_GET_INTERACTABLE_POSITION(iInteractable, FALSE)
		PRINTLN("[Interactables][Interactable ", iInteractable, "] RESET_INTERACTABLE | Resetting Interactable to coords ", vResetCoords)
		SET_ENTITY_COORDS(oiInteractable, vResetCoords)
		MC_SET_UP_INTERACTABLE(iInteractable, FALSE)
		SET_INTERACTABLE_INCOMPLETE(iInteractable)
	ENDIF
	
	SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iInteractable_InteractionType
		CASE ciInteractableInteraction_SyncLock
			IF NETWORK_HAS_CONTROL_OF_ENTITY(oiInteractable) // Control check so that only one player calls this
				DISENGAGE_SYNC_LOCK(iInteractable, FALSE)
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL CONVERT_INTERACTION_TO_BACKGROUND_INTERACTION(INT iInteractionIndex)

	IF iInteractionIndex >= MC_playerBD[iLocalPart].iOngoingInteractionCount
		// Invalid interaction index!
		PRINTLN("[Interactables][Interactable ", sOngoingInteractionVars[iInteractionIndex].iInteractable, "] CONVERT_INTERACTION_TO_BACKGROUND_INTERACTION | Invalid interaction index ", iInteractionIndex)
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(sOngoingInteractionVars[iInteractionIndex].iOngoingInteractionBS, ciONGOING_INTERACTION_BS__IS_BACKGROUND_INTERACTION)
		// Already a background interaction!
		PRINTLN("[Interactables][Interactable ", sOngoingInteractionVars[iInteractionIndex].iInteractable, "] CONVERT_INTERACTION_TO_BACKGROUND_INTERACTION | Interaction ", iInteractionIndex, " is already a BG interaction!")
		RETURN TRUE
	ENDIF
	
	PRINTLN("[Interactables][Interactable ", sOngoingInteractionVars[iInteractionIndex].iInteractable, "] CONVERT_INTERACTION_TO_BACKGROUND_INTERACTION | Converting interaction ", iInteractionIndex, " to a BG interaction")
	SET_BIT(sOngoingInteractionVars[iInteractionIndex].iOngoingInteractionBS, ciONGOING_INTERACTION_BS__IS_BACKGROUND_INTERACTION)
	NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
	
	RETURN TRUE
ENDFUNC

FUNC BOOL CONVERT_INTERACTION_TO_FOREGROUND_INTERACTION(INT iInteractionIndex)

	IF iInteractionIndex >= MC_playerBD[iLocalPart].iOngoingInteractionCount
		// Invalid interaction index!
		RETURN FALSE
	ENDIF
	
	IF NOT IS_BIT_SET(sOngoingInteractionVars[iInteractionIndex].iOngoingInteractionBS, ciONGOING_INTERACTION_BS__IS_BACKGROUND_INTERACTION)
		// Already a foreground interaction!
		RETURN TRUE
	ENDIF
	
	PRINTLN("[Interactables][Interactable ", sOngoingInteractionVars[iInteractionIndex].iInteractable, "] CONVERT_INTERACTION_TO_FOREGROUND_INTERACTION | Converting interaction ", iInteractionIndex, " back to a foreground interaction")
	CLEAR_BIT(sOngoingInteractionVars[iInteractionIndex].iOngoingInteractionBS, ciONGOING_INTERACTION_BS__IS_BACKGROUND_INTERACTION)
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_PROCESS_ONE_FRAME_INTERACTABLE_LOOP_THIS_FRAME()
	IF NOT IS_LONG_BITSET_EMPTY(MC_serverBD.iInteractableCleanup_NeedOwnershipBS)
		RETURN TRUE
	ENDIF
	
	IF DOES_ANY_TEAM_HAVE_NEW_PRIORITY_THIS_FRAME()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Interactable Staggered Processing -------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Functions that are called during the Interactable staggered loop.  -----------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_INTERACTABLE_PRE_STAGGERED_CLIENT()
	
	IF IS_LOCAL_PLAYER_INTERACTING()
		// The player is currently interacting with something already!
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
	iTempProfilerActiveInteractables = 0
	#ENDIF	
	
	iClosestInteractable_Pending = -1
	fCurrentPendingClosestInteractableDistance = 100.0 // Anything further than this almost certainly isn't worth considering
ENDPROC

FUNC BOOL IS_INTERACTABLE_VALID_TO_BE_CLOSEST_INTERACTABLE(FMMC_INTERACTABLE_STATE& sInteractableState)
	IF DOES_INTERACTABLE_HAVE_AN_INTERACTION_SET(sInteractableState.iIndex)
		IF NOT CAN_INTERACT_WITH_INTERACTABLE(sInteractableState, FALSE)
			// Can't interact with this one right now, skip it to avoid any confusion
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC PROCESS_FINDING_CLOSEST_INTERACTABLE_STAGGERED(FMMC_INTERACTABLE_STATE& sInteractableState)
	
	IF sInteractableState.fDistanceToInteractable < fCurrentPendingClosestInteractableDistance
	OR IS_INTERACTABLE_USABLE_FROM_ANY_DISTANCE(sInteractableState.iIndex)
		IF IS_INTERACTABLE_VALID_TO_BE_CLOSEST_INTERACTABLE(sInteractableState)
			// This is the closest one so far in the staggered loop!
			iClosestInteractable_Pending = sInteractableState.iIndex
			fCurrentPendingClosestInteractableDistance = sInteractableState.fDistanceToInteractable
			
			//PRINTLN("[Interactables][Interactable ", sInteractableState.iIndex, "] PROCESS_FINDING_CLOSEST_INTERACTABLE_STAGGERED | Closest pending Interactable is now ", iClosestInteractable_Pending)
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL SHOULD_INTERACTABLE_BE_BLIPPED(FMMC_INTERACTABLE_STATE& sInteractableState)
	
	IF NOT sInteractableState.bInteractableExists
		// Doesn't exist
		PRINTLN("[Interactables_SPAM][Interactable_SPAM ", sInteractableState.iIndex, "] SHOULD_INTERACTABLE_BE_BLIPPED | Interactable doesn't exist")
		RETURN FALSE
	ENDIF
	
	IF NOT sInteractableState.bInteractableAlive
		// Dead
		PRINTLN("[Interactables_SPAM][Interactable_SPAM ", sInteractableState.iIndex, "] SHOULD_INTERACTABLE_BE_BLIPPED | Interactable is dead!")
		RETURN FALSE
	ENDIF
	
	IF DOES_INTERACTABLE_HAVE_AN_INTERACTION_SET(sInteractableState.iIndex)
		IF NOT CAN_INTERACT_WITH_INTERACTABLE(sInteractableState, FALSE, FALSE, TRUE, TRUE)
			// Can't Interact-With it right now (for reasons other than distance)
			PRINTLN("[Interactables_SPAM][Interactable_SPAM ", sInteractableState.iIndex, "] SHOULD_INTERACTABLE_BE_BLIPPED | Blip blocked by CAN_INTERACT_WITH_INTERACTABLE")
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF SHOULD_ENTITY_BLIP_BE_FORCED_OFF(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].sInteractableBlipStruct)
		RETURN FALSE
	ENDIF
	
	IF SHOULD_ENTITY_BLIP_BE_FORCED_ON(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].sInteractableBlipStruct, sInteractableState.objIndex)
		PRINTLN("[Interactables_SPAM][Interactable_SPAM ", sInteractableState.iIndex, "] SHOULD_INTERACTABLE_BE_BLIPPED | Interactable blip struct force blip check returning true")
		RETURN TRUE
	ENDIF
	
	IF NOT SHOULD_ENTITY_BE_BLIPPED(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].sInteractableBlipStruct, sMissionInteractablesLocalVars[sInteractableState.iIndex].sBlipRuntimeVars, sInteractableState.objIndex)
		PRINTLN("[Interactables_SPAM][Interactable_SPAM ", sInteractableState.iIndex, "] SHOULD_INTERACTABLE_BE_BLIPPED | Interactable blip struct check returning false")
		RETURN FALSE
	ENDIF
	
	IF IS_BLIP_SET_AS_HIDDEN(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].sInteractableBlipStruct)
		PRINTLN("[Interactables_SPAM][Interactable_SPAM ", sInteractableState.iIndex, "] SHOULD_INTERACTABLE_BE_BLIPPED | Interactable blip set as hidden")
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(sInteractableState.iBitset, FMMC_INTERACTABLE_STATE_OCCUPIED)
		// Already being used
		PRINTLN("[Interactables_SPAM][Interactable_SPAM ", sInteractableState.iIndex, "] SHOULD_INTERACTABLE_BE_BLIPPED | Interactable is busy")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC PROCESS_INTERACTABLE_BLIP_STAGGERED(FMMC_INTERACTABLE_STATE& sInteractableState)
	
	IF NOT SHOULD_INTERACTABLE_BE_BLIPPED(sInteractableState)
		REMOVE_INTERACTABLE_BLIP(sInteractableState.iIndex)
		EXIT
	ENDIF
	
	IF NOT DOES_BLIP_EXIST(biInteractableBlip[sInteractableState.iIndex])
		CREATE_INTERACTABLE_BLIP(sInteractableState.objIndex, sInteractableState.iIndex)
	ENDIF
	
ENDPROC

PROC PROCESS_INTERACTABLE_AUDIO_STAGGERED(FMMC_INTERACTABLE_STATE& sInteractableState)
	
	PROCESS_ENTITY_AUDIO(sInteractableState.iIndex, sInteractableState.objIndex, sInteractableState.bInteractableAlive, CREATION_TYPE_INTERACTABLE)
	
ENDPROC

PROC PROCESS_INTERACTABLE_COMPLETED_TIMER_STAGGERED(FMMC_INTERACTABLE_STATE& sInteractableState)
	
	IF NOT HAS_NET_TIMER_STARTED(stInteractableCompletedTimers[sInteractableState.iIndex])
		EXIT
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].sConsequences.iInteractable_StayCompleteDuration > 0
		IF FMMC_IS_LONG_BIT_SET(iInteractable_StayCompleteForeverBS, sInteractableState.iIndex)
			#IF IS_DEBUG_BUILD
			IF bInteractableDebug
				TEXT_LABEL_63 tlVisualDebug = "Complete Duration Disabled"
				DRAW_DEBUG_TEXT(tlVisualDebug, GET_FMMC_INTERACTABLE_COORDS(sInteractableState) + (<<0.5, 0.5, 0.5>>), 0, 255, 0, 255)
			ENDIF
			
			PRINTLN("[Interactables_SPAM][Interactable_SPAM ", sInteractableState.iIndex, "] PROCESS_INTERACTABLE_COMPLETED_TIMER_STAGGERED | Ignoring this Interactable's stay complete duration due to iInteractable_StayCompleteForeverBS")
			#ENDIF
			
			EXIT
		ENDIF
		
		IF HAS_NET_TIMER_EXPIRED(stInteractableCompletedTimers[sInteractableState.iIndex], g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].sConsequences.iInteractable_StayCompleteDuration)
			IF NOT FMMC_IS_LONG_BIT_SET(iInteractable_SentCompletionTimerExpiryEventBS, sInteractableState.iIndex)
				IF sInteractableState.bHaveControlOfInteractable // The control check is just here to ensure that only one player broadcasts this event, since all players will be running this timer, not just the one who completed the Interaction (otherwise, they could quit or die etc and the Interactable would never reset)
					PRINTLN("[Interactables][Interactable ", sInteractableState.iIndex, "] PROCESS_INTERACTABLE_COMPLETED_TIMER_STAGGERED | Completed Timer has expired! Sending FMMC_INTERACTABLE_EVENT_TYPE__INTERACTION_NO_LONGER_COMPLETE event...")
					SET_INTERACTABLE_INCOMPLETE(sInteractableState.iIndex)
					FMMC_SET_LONG_BIT(iInteractable_SentCompletionTimerExpiryEventBS, sInteractableState.iIndex)
				ELSE
					PRINTLN("[Interactables][Interactable ", sInteractableState.iIndex, "] PROCESS_INTERACTABLE_COMPLETED_TIMER_STAGGERED | Completed Timer has expired, but I don't have control of the Interactable")
				ENDIF
			ELSE
				PRINTLN("[Interactables][Interactable ", sInteractableState.iIndex, "] PROCESS_INTERACTABLE_COMPLETED_TIMER_STAGGERED | Completed Timer has expired, but I've already sent the Event")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_INTERACTABLE_ATTACHMENT(FMMC_INTERACTABLE_STATE& sInteractableState)

	IF NOT sInteractableState.bInteractableExists
	OR NOT sInteractableState.bHaveControlOfInteractable
		EXIT
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_AttachedEntityType = ciENTITY_TYPE_NONE
	OR g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_AttachedEntityIndex = -1
		// This interactable isn't set to be attached to anything
		EXIT
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractableBS, ciInteractableBS_DontPhysicallyAttachToLinkedEntity)
	OR g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].sInteractableTrainAttachmentData.iTrainIndex > -1
		// This interactable is set to be linked to another entity, but not physically attached
		EXIT
	ENDIF
	
	IF IS_ENTITY_ATTACHED(sInteractableState.objIndex)
		// Already attached!
		EXIT
	ENDIF
	
	ENTITY_INDEX eiAttachmentEntity = GET_FMMC_ENTITY(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_AttachedEntityType, g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_AttachedEntityIndex)
	
	IF NOT DOES_ENTITY_EXIST(eiAttachmentEntity)
		PRINTLN("[Interactables][Interactable ", sInteractableState.iIndex, "] PROCESS_INTERACTABLE_ATTACHMENT | Attachment object doesn't exist!")
		EXIT
	ENDIF
	
	IF ATTACH_ENTITY_TO_ENTITY_KEEP_CURRENT_TRANSFORM(sInteractableState.objIndex, eiAttachmentEntity, FALSE)
		PRINTLN("[Interactables][Interactable ", sInteractableState.iIndex, "] PROCESS_INTERACTABLE_ATTACHMENT | Interactable has been attached!")
	ENDIF
	
ENDPROC

PROC PROCESS_INTERACTABLE_MARKERS(FMMC_INTERACTABLE_STATE& sInteractableState)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iPlacedMarkerIndex = -1
		EXIT
	ENDIF
		
	INT iIndex = g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iPlacedMarkerIndex
		
	IF IS_INTERACTABLE_COMPLETE(sInteractableState.iIndex, TRUE)
	OR NOT sInteractableState.bInteractableExists
	OR NOT sInteractableState.bInteractableAlive
		IF FMMC_IS_LONG_BIT_SET(iPlacedWorldMarkerForInteractableOverriden, sInteractableState.iIndex)
			vPlacedWorldMarkerForInteractable[iIndex] = <<0.0, 0.0, 0.0>>
			FMMC_CLEAR_LONG_BIT(iPlacedWorldMarkerForInteractableOverriden, sInteractableState.iIndex)
		ENDIF
		
		EXIT
	ENDIF
	
	vPlacedWorldMarkerForInteractable[iIndex] = GET_FMMC_INTERACTABLE_COORDS(sInteractableState)
	FMMC_SET_LONG_BIT(iPlacedWorldMarkerForInteractableOverriden, sInteractableState.iIndex)
	
ENDPROC

PROC PROCESS_INTERACTABLE_AVAILABLE_INTERACTION_COUNT_STAGGERED(FMMC_INTERACTABLE_STATE& sInteractableState)
	
	INT iInteractionCounter
	FOR iInteractionCounter = 0 TO ciInteractable_InteractionCounters - 1
	
		IF g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_InteractionType != g_FMMC_STRUCT_ENTITIES.sInteractionCounters[iInteractionCounter].iInteractionCounter_InteractionType
			RELOOP
		ENDIF
		
		IF IS_INTERACTABLE_COMPLETE(sInteractableState.iIndex, TRUE, FALSE)
			iInteractable_Temp_CompletedInteractionCount[iInteractionCounter]++
		ELSE
			iInteractable_Temp_AvailableInteractionCount[iInteractionCounter]++
		ENDIF
	ENDFOR
	
ENDPROC

PROC PROCESS_INTERACTABLE_STAGGERED_CLIENT(INT iInt)
	
	FMMC_INTERACTABLE_STATE sInteractableState
	FILL_FMMC_INTERACTABLE_STATE_STRUCT(sInteractableState, iInt)
	
	PROCESS_INTERACTABLE_AUDIO_STAGGERED(sInteractableState)
	
	PROCESS_INTERACTABLE_BLIP_STAGGERED(sInteractableState)
	
	PROCESS_INTERACTABLE_MARKERS(sInteractableState)
	
	IF sInteractableState.bInteractableExists
	AND CAN_INTERACT_WITH_INTERACTABLE(sInteractableState, FALSE, FALSE, FALSE, FALSE)
		IF NOT FMMC_IS_LONG_BIT_SET(iInteractable_CachedAvailableBS, sInteractableState.iIndex)
			FMMC_SET_LONG_BIT(iInteractable_CachedAvailableBS, sInteractableState.iIndex)
			PRINTLN("[Interactables][Interactable ", sInteractableState.iIndex, "] PROCESS_INTERACTABLE_STAGGERED_CLIENT | Setting iInteractable_CachedAvailableBS for this Interactable!")
			SET_BIT(iSharedInteractableBS, ciSHARED_INTERACTABLE_BS__FREEZE_INTERACTION_COUNTERS_THIS_LOOP)
		ENDIF
	ELSE
		IF FMMC_IS_LONG_BIT_SET(iInteractable_CachedAvailableBS, sInteractableState.iIndex)
			FMMC_CLEAR_LONG_BIT(iInteractable_CachedAvailableBS, sInteractableState.iIndex)
			PRINTLN("[Interactables][Interactable ", sInteractableState.iIndex, "] PROCESS_INTERACTABLE_STAGGERED_CLIENT | Clearing iInteractable_CachedAvailableBS for this Interactable!")
			SET_BIT(iSharedInteractableBS, ciSHARED_INTERACTABLE_BS__FREEZE_INTERACTION_COUNTERS_THIS_LOOP)
		ENDIF
	ENDIF
	
	IF NOT sInteractableState.bInteractableExists
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
	iTempProfilerActiveInteractables++
	#ENDIF	
	
	// Find the closest Interactable over the course of the staggered loop, for the Every-Frame loop to look at in more detail
	IF NOT IS_LOCAL_PLAYER_INTERACTING()
		PROCESS_FINDING_CLOSEST_INTERACTABLE_STAGGERED(sInteractableState)
	ENDIF
	
	PROCESS_INTERACTABLE_COMPLETED_TIMER_STAGGERED(sInteractableState)
	
	PROCESS_ENTITY_INTERIOR(sInteractableState.iIndex, sInteractableState.objIndex, g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_InteriorIndex != -1, <<0,0,0>>, iInteractableHasBeenPlacedIntoInteriorBS[sInteractableState.iIndex / 32])
	
	PROCESS_INTERACTABLE_COMPLETION_MODEL_SWAP(sInteractableState.iIndex, sInteractableState.objIndex)
	
	PROCESS_INTERACTABLE_AVAILABLE_INTERACTION_COUNT_STAGGERED(sInteractableState)
	
	IF NOT IS_LOCAL_PLAYER_INTERACTING(sInteractableState.iIndex)
		PROCESS_INTERACTABLE_ATTACHMENT(sInteractableState)
		
		IF sInteractableState.bHaveControlOfInteractable
			PROCESS_TRAIN_ATTACHMENT(sInteractableState.objIndex, g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].sInteractableTrainAttachmentData, NOT IS_INTERACTABLE_OCCUPIED(sInteractableState.iIndex))
		ENDIF
	ENDIF

	// Review Play Fix for Interactables becoming unfrozen
	IF SHOULD_INTERACTABLE_BE_COMPLETELY_FROZEN(sInteractableState.iIndex)
		IF sInteractableState.bHaveControlOfInteractable
			FREEZE_ENTITY_POSITION(sInteractableState.objIndex, TRUE)
			SET_ENTITY_DYNAMIC(sInteractableState.objIndex, FALSE)
				
			IF NOT ARE_VECTORS_ALMOST_EQUAL(GET_ENTITY_COORDS(sInteractableState.objIndex, FALSE), g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].vInteractable_Position, 0.1)
				PRINTLN("[Interactables][Interactable ", sInteractableState.iIndex, "] PROCESS_INTERACTABLE_STAGGERED_CLIENT | Putting interactable back in place!")
				SET_ENTITY_COORDS_NO_OFFSET(sInteractableState.objIndex, g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].vInteractable_Position)
				MC_SET_UP_INTERACTABLE(sInteractableState.iIndex, FALSE)
			ENDIF
		ENDIF
	ENDIF
	
	PROCESS_INTERACTABLE_ON_RULE_WARP(sInteractableState)
	
	PROCESS_INTERACTABLE_ON_DAMAGE_WARP(sInteractableState)
		
ENDPROC

PROC PROCESS_INTERACTABLE_POST_STAGGERED_CLIENT()

	#IF IS_DEBUG_BUILD
	iProfilerActiveInteractables = iTempProfilerActiveInteractables
	#ENDIF	
	
	// Transfer the temp vars counted up over the staggered loop to the proper vars
	INT iInteractionCounter
	FOR iInteractionCounter = 0 TO ciInteractable_InteractionCounters - 1
	
		IF NOT IS_BIT_SET(iSharedInteractableBS, ciSHARED_INTERACTABLE_BS__FREEZE_INTERACTION_COUNTERS_THIS_LOOP)
			iInteractable_AvailableInteractionCount[iInteractionCounter] = iInteractable_Temp_AvailableInteractionCount[iInteractionCounter]
			iInteractable_CompletedInteractionCount[iInteractionCounter] = iInteractable_Temp_CompletedInteractionCount[iInteractionCounter]
		ELSE
			PRINTLN("[Interactables] PROCESS_INTERACTABLE_POST_STAGGERED_CLIENT | Leaving Interaction Counter vars this time due to ciSHARED_INTERACTABLE_BS__FREEZE_INTERACTION_COUNTERS_THIS_LOOP!")
		ENDIF
		
		iInteractable_Temp_AvailableInteractionCount[iInteractionCounter] = 0
		iInteractable_Temp_CompletedInteractionCount[iInteractionCounter] = 0
	ENDFOR
	
	CLEAR_BIT(iSharedInteractableBS, ciSHARED_INTERACTABLE_BS__FREEZE_INTERACTION_COUNTERS_THIS_LOOP)
	
	IF NOT IS_LOCAL_PLAYER_INTERACTING()
		// Set the cached closest Interactable to be the one we decided is closest over the course of the staggered loop (iClosestInteractable_Pending)
		SET_CLOSEST_INTERACTABLE(iClosestInteractable_Pending)
	ENDIF
	
ENDPROC

FUNC BOOL DOES_REMOTE_INTERACTION_NEED_TO_BAIL(FMMC_INTERACTABLE_STATE& sInteractableState)

	INT iInteractableUserPart = GET_INTERACTABLE_CURRENT_USER(sInteractableState.iIndex)
	PARTICIPANT_INDEX piUserPart = INT_TO_PARTICIPANTINDEX(iInteractableUserPart)
	
	IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(piUserPart)
		PRINTLN("[Interactables][Interactable ", sInteractableState.iIndex, "] DOES_REMOTE_INTERACTION_NEED_TO_BAIL | Bailing because piUserPart (", iInteractableUserPart, ") isn't an active participant!")
		RETURN TRUE
	ENDIF
	
	PLAYER_INDEX piUserPlayer = NETWORK_GET_PLAYER_INDEX(piUserPart)
	IF NOT IS_NET_PLAYER_OK(piUserPlayer, FALSE, TRUE)
		PRINTLN("[Interactables][Interactable ", sInteractableState.iIndex, "] DOES_REMOTE_INTERACTION_NEED_TO_BAIL | Bailing because piUserPlayer (participant ", iInteractableUserPart, ") isn't ok!")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_BAILING_REMOTE_INTERACTIONS(FMMC_INTERACTABLE_STATE& sInteractableState)
	
	IF GET_INTERACTABLE_CURRENT_USER(sInteractableState.iIndex) = -1
		EXIT
	ENDIF
	
	IF DOES_REMOTE_INTERACTION_NEED_TO_BAIL(sInteractableState)
		BROADCAST_FMMC_INTERACTABLE_EVENT(FMMC_INTERACTABLE_EVENT_TYPE__RESET_INTERACTABLE, sInteractableState.iIndex)
		MC_serverBD_4.sIntServerData.iInteractable_InteractingParticipant[sInteractableState.iIndex] = -1
		PRINTLN("[Interactables][Interactable ", sInteractableState.iIndex, "] PROCESS_BAILING_REMOTE_INTERACTIONS | Bailing remote interaction now!")
	ENDIF
ENDPROC

PROC PROCESS_INTERACTABLE_STAGGERED_SERVER(INT iInteractable)
	
	IF NOT bIsLocalPlayerHost
		EXIT
	ENDIF

	FMMC_INTERACTABLE_STATE sInteractableState
	FILL_FMMC_INTERACTABLE_STATE_STRUCT(sInteractableState, iInteractable)
	
	IF sInteractableState.bInteractableExists
		IF SHOULD_CLEANUP_INTERACTABLE(iInteractable)
			IF NOT FMMC_IS_LONG_BIT_SET(MC_serverBD.iInteractableCleanup_NeedOwnershipBS, iInteractable)
				FMMC_SET_LONG_BIT(MC_serverBD.iInteractableCleanup_NeedOwnershipBS, iInteractable)
				PRINTLN("[Interactables][Interactable ", iInteractable, "] PROCESS_INTERACTABLE_STAGGERED_SERVER | Setting iInteractableCleanup_NeedOwnershipBS for Interactable ", iInteractable)
			ENDIF
			
			EXIT
		ELSE
			PROCESS_BAILING_REMOTE_INTERACTIONS(sInteractableState)
			
			IF FMMC_IS_LONG_BIT_SET(MC_serverBD.iInteractableCleanup_NeedOwnershipBS, iInteractable)
				FMMC_CLEAR_LONG_BIT(MC_serverBD.iInteractableCleanup_NeedOwnershipBS, iInteractable)
				PRINTLN("[Interactables][Interactable ", iInteractable, "] PROCESS_INTERACTABLE_STAGGERED_SERVER | Clearing iInteractableCleanup_NeedOwnershipBS for Interactable ", iInteractable)
			ENDIF
		ENDIF
	ELSE		
		IF IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_FIRST_UPDATE_DONE)
			IF SHOULD_INTERACTABLE_SPAWN_NOW(iInteractable, TRUE)
				MC_CREATE_INTERACTABLE(iInteractable, FALSE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Interactable Interaction Processing -----------------------------------------------------------------------------------------------------------------------------
// ##### Description: Functions that are called during the Interactable every-frame loop -----------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Minisection: Interact-With ----------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
FUNC BOOL PROCESS_LOADING_INTERACTION__INTERACT_WITH_ANIM(FMMC_INTERACTABLE_STATE& sInteractableState)
	INTERACT_WITH_PARAMS sInteractWithParams
	FILL_INTERACT_WITH_PARAMS_FROM_INTERACTABLE(sInteractWithParams, sInteractableState.iIndex, sInteractableState.objIndex) 
	RETURN GET_INTERACT_WITH_INFO_AND_LOAD_ANIMS(sInteractWithParams)
ENDFUNC

FUNC BOOL PROCESS_GENERIC_INTERACTABLE_INTERACT_WITH_FUNCTIONALITY(FMMC_INTERACTABLE_STATE& sInteractableState, INTERACT_WITH_PARAMS& sInteractWithParams, BOOL bEndInteractionAfter = TRUE)
	
	BLOCK_INTERACT_WITH_CONFLICTING_INPUTS()
	
	IF PROCESS_INTERACT_WITH_ANIMATION(sInteractWithParams)
		IF WAS_INTERACT_WITH_SUCCESSFUL(sInteractWithParams)
			IF bEndInteractionAfter
				SET_INTERACTABLE_COMPLETE(sInteractableState.iIndex)
				
				IF NOT IS_INTERACT_WITH_WAITING_TO_CLEAN_UP(sInteractWithParams)
					END_INTERACTABLE_INTERACTION(sInteractableState.iIndex)
				ENDIF
			ENDIF
			
			IF NOT IS_INTERACT_WITH_WAITING_TO_CLEAN_UP(sInteractWithParams)
				RETURN TRUE
			ENDIF
		ELSE
			IF bEndInteractionAfter
				SET_INTERACTABLE_INCOMPLETE(sInteractableState.iIndex)
				
				IF NOT IS_INTERACT_WITH_WAITING_TO_CLEAN_UP(sInteractWithParams)
					IF SHOULD_BAIL_INTERACTION(sInteractableState)
						RESET_INTERACTABLE(sInteractableState.iIndex)
						
						IF NOT sInteractableState.bIsBackgroundInteraction
							BAIL_FOREGROUND_INTERACTION_ASAP()
						ENDIF
					ELSE
						END_INTERACTABLE_INTERACTION(sInteractableState.iIndex)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_INTERACT_WITH_ANIM_PRESET_A_HOLD_INPUT(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_InteractWithAnimPreset, sInteractWithParams.iInteractWith_AltAnimSet)
		IF sInteractWithVars.eInteractWith_CurrentState = IW_STATE_PERFORMING_LOOPING_ANIMATION
			IF bEndInteractionAfter
			AND NOT IS_INTERACTABLE_COMPLETE(sInteractableState.iIndex, TRUE)
				SET_INTERACTABLE_COMPLETE(sInteractableState.iIndex)
			ENDIF
			
			RETURN TRUE
		ELSE
			IF bEndInteractionAfter
			AND IS_INTERACTABLE_COMPLETE(sInteractableState.iIndex, TRUE)
				SET_INTERACTABLE_INCOMPLETE(sInteractableState.iIndex)
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_INTERACTION__INTERACT_WITH_ANIM(FMMC_INTERACTABLE_STATE& sInteractableState)
	INTERACT_WITH_PARAMS sInteractWithParams
	FILL_INTERACT_WITH_PARAMS_FROM_INTERACTABLE(sInteractWithParams, sInteractableState.iIndex, sInteractableState.objIndex) 
	
	PROCESS_GENERIC_INTERACTABLE_INTERACT_WITH_FUNCTIONALITY(sInteractableState, sInteractWithParams)
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Minisection: Sync-Lock --------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
PROC FILL_IW_PARAMS_FOR_SYNC_LOCK(FMMC_INTERACTABLE_STATE& sInteractableState, INTERACT_WITH_PARAMS& sInteractWithParams)
	FILL_INTERACT_WITH_PARAMS_FROM_INTERACTABLE(sInteractWithParams, sInteractableState.iIndex, sInteractableState.objIndex)
	sInteractWithParams.iInteractWith_AnimPreset = ciINTERACT_WITH_PRESET__SYNC_LOCK_PANEL
ENDPROC

FUNC BOOL PROCESS_LOADING_INTERACTION__SYNC_LOCK(FMMC_INTERACTABLE_STATE& sInteractableState)
	INTERACT_WITH_PARAMS sInteractWithParams
	FILL_IW_PARAMS_FOR_SYNC_LOCK(sInteractableState, sInteractWithParams)
	
	RETURN GET_INTERACT_WITH_INFO_AND_LOAD_ANIMS(sInteractWithParams)
ENDFUNC

PROC SUCCEED_AT_SYNC_LOCK_INTERACTION(FMMC_INTERACTABLE_STATE& sInteractableState, INTERACT_WITH_PARAMS& sInteractWithParams)
	UNUSED_PARAMETER(sInteractableState)
	PLAY_INTERACT_WITH_PRESET_SUB_ANIM(sInteractWithParams, ciIW_SUBANIM__SYNCLOCK_SUCCESS)
	RESET_NET_TIMER(stSyncLockEngageTimer)
	SET_SYNCLOCK_STATE(SYNCLOCK_INTERACTION_STATE__SUCCESS)
	
	#IF FEATURE_HEIST_ISLAND
	SET_ISLAND_HEIST_INTEREST_ITEM_USED(ciContinuityTelemetry_InterestItemBS_DoubleKeycardUsed)
	#ENDIF
	
	PLAY_SOUND_FROM_ENTITY(-1, "Keycard_Success", sInteractableState.objIndex, "Twin_Card_Entry_Sounds")
ENDPROC

PROC FAIL_AT_SYNC_LOCK_INTERACTION(FMMC_INTERACTABLE_STATE& sInteractableState, INTERACT_WITH_PARAMS& sInteractWithParams)
	UNUSED_PARAMETER(sInteractableState)
	PLAY_INTERACT_WITH_PRESET_SUB_ANIM(sInteractWithParams, ciIW_SUBANIM__SYNCLOCK_FAIL)
	SET_SYNCLOCK_STATE(SYNCLOCK_INTERACTION_STATE__FAIL)
	DISENGAGE_SYNC_LOCK(sInteractableState.iIndex, TRUE)
	
	PLAY_SOUND_FROM_ENTITY(-1, "Keycard_Fail", sInteractableState.objIndex, "Twin_Card_Entry_Sounds")
ENDPROC

PROC PROCESS_INTERACTION__SYNC_LOCK(FMMC_INTERACTABLE_STATE& sInteractableState)
	
	INTERACT_WITH_PARAMS sInteractWithParams
	FILL_IW_PARAMS_FOR_SYNC_LOCK(sInteractableState, sInteractWithParams)
	
	BOOL bDebugComplete = FALSE
	
	#IF IS_DEBUG_BUILD
	IF bInteractableDebug
		TEXT_LABEL_63 tlDebugText3D = "SyncLock State: "
		tlDebugText3D += ENUM_TO_INT(MC_playerBD[iLocalPart].eSyncLockInteractionState)
		DRAW_DEBUG_TEXT(tlDebugText3D, GET_FMMC_INTERACTABLE_COORDS(sInteractableState))
		
		tlDebugText3D = "iInteractable_SyncLockEngagedBS[0]: "
		tlDebugText3D += iInteractable_SyncLockEngagedBS[0]
		DRAW_DEBUG_TEXT(tlDebugText3D, GET_FMMC_INTERACTABLE_COORDS(sInteractableState) + <<0, 0, -0.5>>)
		tlDebugText3D = "iInteractable_SyncLockEngagedBS[1]: "
		tlDebugText3D += iInteractable_SyncLockEngagedBS[1]
		DRAW_DEBUG_TEXT(tlDebugText3D, GET_FMMC_INTERACTABLE_COORDS(sInteractableState) + <<0, 0, -0.35>>)
		
		IF FMMC_IS_LONG_BIT_SET(iInteractable_SyncLockEngagedBS, sInteractableState.iIndex)
			tlDebugText3D = "ENGAGED"
			DRAW_DEBUG_TEXT(tlDebugText3D, GET_FMMC_INTERACTABLE_COORDS(sInteractableState) + <<0, 0, 0.15>>, 255, 0, 0)
		ENDIF
		
		IF IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD5)
			bDebugComplete = TRUE
			tlDebugText3D = "Will pass!"
			DRAW_DEBUG_TEXT(tlDebugText3D, GET_FMMC_INTERACTABLE_COORDS(sInteractableState) + <<0, 0, -0.85>>)
		ELSE
			bDebugComplete = FALSE
			tlDebugText3D = "Hold Numpad5 to pass!"
			DRAW_DEBUG_TEXT(tlDebugText3D, GET_FMMC_INTERACTABLE_COORDS(sInteractableState) + <<0, 0, -0.85>>, 255, 0, 0)
		ENDIF
	ENDIF
	#ENDIF
	
	IF NOT DOES_INTERACTABLE_HAVE_LINKED_INTERACTABLE(sInteractableState.iIndex)
		PRINTLN("[Interactables][Interactable ", sInteractableState.iIndex, "][SyncLock] PROCESS_INTERACTION__SYNC_LOCK | This sync lock panel isn't linked to any other sync lock panel!")
		ASSERTLN("[Interactables][Interactable ", sInteractableState.iIndex, "][SyncLock] PROCESS_INTERACTION__SYNC_LOCK | This sync lock panel isn't linked to any other sync lock panel!")
		#IF IS_DEBUG_BUILD
		ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_INTERACTABLE_HAS_NO_LINKED_SYNC_LOCK_PANEL, ENTITY_RUNTIME_ERROR_TYPE_CRITICAL_INTERACTABLE, "This sync lock panel isn't linked to any other sync lock panel!", sInteractableState.iIndex)
		#ENDIF
		EXIT
	ENDIF
	
	IF IS_LINKED_INTERACTABLE_OCCUPIED(sInteractableState.iIndex)
		SET_INTERACTABLE_OBJECTIVE_TEXT_TEXTLABEL("MC_TSYNCLOCK_PL")
	ELSE
		SET_INTERACTABLE_OBJECTIVE_TEXT_TEXTLABEL("MC_WSYNCLOCK_PL")
	ENDIF
	
	SWITCH MC_playerBD[iLocalPart].eSyncLockInteractionState
		CASE SYNCLOCK_INTERACTION_STATE__OFF
			RESET_NET_TIMER(stSyncLockEngageTimer)
			PRINTLN("[Interactables][Interactable ", sInteractableState.iIndex, "][SyncLock] PROCESS_INTERACTION__SYNC_LOCK | Initialising! Resetting stSyncLockEngageTimer locally just in case")
				
			SET_SYNCLOCK_STATE(SYNCLOCK_INTERACTION_STATE__INIT)
		BREAK
		
		CASE SYNCLOCK_INTERACTION_STATE__INIT
			IF sInteractWithVars.eInteractWith_CurrentState = IW_STATE_PERFORMING_LOOPING_ANIMATION
				RESET_SYNC_LOCK_VARS(FALSE, FALSE)
				
				
				SET_SYNCLOCK_STATE(SYNCLOCK_INTERACTION_STATE__IN_POSITION)
			ELSE
				// We're still waiting for the Interact-With to get to the looping section
			ENDIF
		BREAK
		
		CASE SYNCLOCK_INTERACTION_STATE__IN_POSITION
			IF HAS_PLAYER_PRESSED_ACCEPT_BUTTON()
			AND IS_LINKED_INTERACTABLE_OCCUPIED(sInteractableState.iIndex)
				SET_SYNCLOCK_STATE(SYNCLOCK_INTERACTION_STATE__ENGAGE_INIT)
				
			ELIF HAS_PLAYER_PRESSED_BACK_OUT_BUTTON()
				SET_BIT(sInteractWithParams.iInteractWithParamsRuntimeBS, ciINTERACT_WITH_PARAMS_RUNTIME_BS__StopLoopingASAP)
				SET_SYNCLOCK_STATE(SYNCLOCK_INTERACTION_STATE__BACKING_OUT)
				
			ELSE
				IF IS_LINKED_INTERACTABLE_OCCUPIED(sInteractableState.iIndex)
					SHOW_INTERACTABLE_CONTROLS_HELPTEXT("H2_HSL", "H2_HSL_MK")
				ELSE
					SHOW_INTERACTABLE_CONTROLS_HELPTEXT("H2_HWSL", "H2_HWSL_MK")
				ENDIF
				
			ENDIF
		BREAK
		
		CASE SYNCLOCK_INTERACTION_STATE__BACKING_OUT
			// Wait for animation to end
		BREAK
		
		CASE SYNCLOCK_INTERACTION_STATE__ENGAGE_INIT
			PLAY_INTERACT_WITH_PRESET_SUB_ANIM(sInteractWithParams, ciIW_SUBANIM__SYNCLOCK_ENGAGE)
			SET_SYNCLOCK_STATE(SYNCLOCK_INTERACTION_STATE__ENGAGE_ENTER)
		BREAK
		
		CASE SYNCLOCK_INTERACTION_STATE__ENGAGE_ENTER
			IF sInteractWithVars.eInteractWith_CurrentState = IW_STATE_PERFORMING_LOOPING_ANIMATION
				ENGAGE_SYNC_LOCK(sInteractableState.iIndex, sInteractableState.objIndex)
				SET_SYNCLOCK_STATE(SYNCLOCK_INTERACTION_STATE__ENGAGED_WAIT_FOR_RESULT)
			ELSE
				// We're still waiting for the Interact-With to get to the looping section
			ENDIF
		BREAK
		
		CASE SYNCLOCK_INTERACTION_STATE__ENGAGED_WAIT_FOR_RESULT
			IF NOT HAS_NET_TIMER_STARTED(stSyncLockEngageTimer)
				IF IS_LONG_BIT_SET(iInteractable_SyncLockEngagedBS, sInteractableState.iIndex)
				OR NOT IS_LINKED_INTERACTABLE_OCCUPIED(sInteractableState.iIndex)
					// Failure because the timer is in a weird state - this will be a very rare occurance
					FAIL_AT_SYNC_LOCK_INTERACTION(sInteractableState, sInteractWithParams)
				ELSE
					PRINTLN("[Interactables][Interactable ", sInteractableState.iIndex, "] PROCESS_INTERACTION__SYNC_LOCK | Waiting to receive my engage event")
				ENDIF
				
			ELSE
				IF HAS_NET_TIMER_EXPIRED(stSyncLockEngageTimer, ciSyncLockEngageTimeWindowDuration)
				OR NOT IS_LINKED_INTERACTABLE_OCCUPIED(sInteractableState.iIndex)
					IF IS_CURRENT_SYNC_LOCK_INTERACTION_SUCCESSFUL()
					OR bDebugComplete
						// Success!
						SUCCEED_AT_SYNC_LOCK_INTERACTION(sInteractableState, sInteractWithParams)
					ELSE
						// Failure!
						FAIL_AT_SYNC_LOCK_INTERACTION(sInteractableState, sInteractWithParams)
					ENDIF
				ELSE
					// Waiting for the timer to run out...
				ENDIF
			ENDIF
			
			RUN_INTERACTABLE_BAIL_TIMER(12000)
		BREAK
		
		CASE SYNCLOCK_INTERACTION_STATE__SUCCESS
			// Wait for the Interact-With to finish
			RUN_INTERACTABLE_BAIL_TIMER(8000)
		BREAK
		
		CASE SYNCLOCK_INTERACTION_STATE__FAIL
			RUN_INTERACTABLE_BAIL_TIMER(8000)
			
			IF sInteractWithVars.eInteractWith_CurrentState = IW_STATE_PERFORMING_LOOPING_ANIMATION
				SET_SYNCLOCK_STATE(SYNCLOCK_INTERACTION_STATE__INIT)
			ELSE
				// We're still waiting for the Interact-With to get back to the original looping section
			ENDIF
		BREAK
	ENDSWITCH
	
	IF HAS_NET_TIMER_STARTED(stSyncLockEngageTimer)
		IF NOT HAS_NET_TIMER_EXPIRED(stSyncLockEngageTimer, ciSyncLockEngageTimeWindowDuration)
			// Draw the time meter in the bottom-right
			DRAW_GENERIC_METER(ciSyncLockEngageTimeWindowDuration - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stSyncLockEngageTimer), ciSyncLockEngageTimeWindowDuration, "LOCK_TIME")
		ELSE
			RESET_SYNC_LOCK_VARS(FALSE, TRUE)
		ENDIF
	ENDIF
	
	// Safeguard to keep it in sync with the connected panel (if the connected panel is completed first, it'll also complete this panel, which is when this will kick in)
	IF IS_INTERACTABLE_COMPLETE(sInteractableState.iIndex)
	AND MC_playerBD[iLocalPart].eSyncLockInteractionState != SYNCLOCK_INTERACTION_STATE__SUCCESS
		PRINTLN("[Interactables][Interactable ", sInteractableState.iIndex, "][SyncLock] PROCESS_INTERACTION__SYNC_LOCK | This Interactable has been completed by its linked panel - succeeding now in order to keep it all in sync")
		SUCCEED_AT_SYNC_LOCK_INTERACTION(sInteractableState, sInteractWithParams)
	ENDIF
	
	IF PROCESS_INTERACT_WITH_ANIMATION(sInteractWithParams)
		IF MC_playerBD[iLocalPart].eSyncLockInteractionState = SYNCLOCK_INTERACTION_STATE__SUCCESS
			SET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_ACTIVATE_2_PERSON_KEY, TRUE)
			SET_INTERACTABLE_COMPLETE(sInteractableState.iIndex)
		ENDIF
		
		// Interaction ended!
		END_INTERACTABLE_INTERACTION(sInteractableState.iIndex)
	ENDIF
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Minisection: Hacking Minigame A -----------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
FUNC INTERACT_WITH_PARAMS FILL_IW_PARAMS_FOR_HACKING_MINIGAME_A(FMMC_INTERACTABLE_STATE& sInteractableState)
	INTERACT_WITH_PARAMS sInteractWithParams
	FILL_INTERACT_WITH_PARAMS_FROM_INTERACTABLE(sInteractWithParams, sInteractableState.iIndex, sInteractableState.objIndex)
	
	sInteractWithParams.iInteractWith_AnimPreset = ciINTERACT_WITH_PRESET__NONE
	sInteractWithParams.sCustomIWAnims.sIW_AnimDict = "anim@GangOps@Facility@Servers@"
	sInteractWithParams.sCustomIWAnims.sPlayerAnims.sIW_Enter = "HOTWIRE_INTRO"
	sInteractWithParams.sCustomIWAnims.sPlayerAnims.sIW_Looping = "HOTWIRE"
	sInteractWithParams.sCustomIWAnims.sPlayerAnims.sIW_Exit = "HOTWIRE_OUTRO"
	
	RETURN sInteractWithParams
ENDFUNC

FUNC BOOL PROCESS_LOADING_INTERACTION__HACKING_MINIGAME_A(FMMC_INTERACTABLE_STATE& sInteractableState)
	INTERACT_WITH_PARAMS sInteractWithParams
	FILL_IW_PARAMS_FOR_HACKING_MINIGAME_A(sInteractableState)
	RETURN GET_INTERACT_WITH_INFO_AND_LOAD_ANIMS(sInteractWithParams)
ENDFUNC

PROC PROCESS_INTERACTION__HACKING_MINIGAME_A(FMMC_INTERACTABLE_STATE& sInteractableState)
	
	INTERACT_WITH_PARAMS sInteractWithParams
	FILL_IW_PARAMS_FOR_HACKING_MINIGAME_A(sInteractableState)
	
	IF sInteractWithVars.eInteractWith_CurrentState = IW_STATE_PERFORMING_LOOPING_ANIMATION
		PROCESS_HOTWIRE_MINIGAME(sHotwire[sInteractableState.iInteractableHackingStructIndex])
		
		IF IS_BIT_SET(sHotwire[sInteractableState.iInteractableHackingStructIndex].iBS, ciGOHACKBS_PASSED)
			SET_INTERACTABLE_COMPLETE(sInteractableState.iIndex)
			
			SET_BIT(sInteractWithParams.iInteractWithParamsRuntimeBS, ciINTERACT_WITH_PARAMS_RUNTIME_BS__StopLoopingASAP)
			CLEAN_UP_HOTWIRE_MINIGAME(sHotwire[sInteractableState.iInteractableHackingStructIndex])
			START_MINIGAME_DAMAGE_PROTECTION()
			
		ELIF IS_BIT_SET(sHotwire[sInteractableState.iInteractableHackingStructIndex].iBS, ciGOHACKBS_FAILED)
			SET_BIT(sInteractWithParams.iInteractWithParamsRuntimeBS, ciINTERACT_WITH_PARAMS_RUNTIME_BS__StopLoopingASAP)
			CLEAN_UP_HOTWIRE_MINIGAME(sHotwire[sInteractableState.iInteractableHackingStructIndex])
			START_MINIGAME_DAMAGE_PROTECTION()
			
		ELIF IS_BIT_SET(sHotwire[sInteractableState.iInteractableHackingStructIndex].iBS, ciGOHACKBS_QUIT)
			SET_BIT(sInteractWithParams.iInteractWithParamsRuntimeBS, ciINTERACT_WITH_PARAMS_RUNTIME_BS__StopLoopingASAP)
			CLEAN_UP_HOTWIRE_MINIGAME(sHotwire[sInteractableState.iInteractableHackingStructIndex])
			START_MINIGAME_DAMAGE_PROTECTION()
		ENDIF
	ENDIF
	
	IF PROCESS_INTERACT_WITH_ANIMATION(sInteractWithParams)
		// Interaction ended!
		END_INTERACTABLE_INTERACTION(sInteractableState.iIndex)
	ENDIF
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Minisection: Voltage Minigame -----------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
PROC FILL_IW_PARAMS_FOR_VOLTAGE_MINIGAME(FMMC_INTERACTABLE_STATE& sInteractableState, INTERACT_WITH_PARAMS& sInteractWithParams)	
	FILL_INTERACT_WITH_PARAMS_FROM_INTERACTABLE(sInteractWithParams, sInteractableState.iIndex, sInteractableState.objIndex)	
	sInteractWithParams.iInteractWith_AnimPreset = ciINTERACT_WITH_PRESET__VOLTAGE_MINIGAME	
ENDPROC

FUNC BOOL PROCESS_LOADING_INTERACTION__VOLTAGE_MINIGAME(FMMC_INTERACTABLE_STATE& sInteractableState)
	INTERACT_WITH_PARAMS sInteractWithParams
	FILL_IW_PARAMS_FOR_VOLTAGE_MINIGAME(sInteractableState, sInteractWithParams)
	RETURN GET_INTERACT_WITH_INFO_AND_LOAD_ANIMS(sInteractWithParams)
ENDFUNC

PROC PROCESS_INTERACTION__VOLTAGE_MINIGAME(FMMC_INTERACTABLE_STATE& sInteractableState)
	
	INTERACT_WITH_PARAMS sInteractWithParams 
	FILL_IW_PARAMS_FOR_VOLTAGE_MINIGAME(sInteractableState, sInteractWithParams)
	
	IF sInteractWithVars.eInteractWith_CurrentState = IW_STATE_INIT
		SET_BIT(iLocalBoolCheck34, LBOOL34_VOLTAGE_MINIGAME__STARTED)
	ENDIF
	
	IF sInteractWithVars.eInteractWith_CurrentState = IW_STATE_PERFORMING_LOOPING_ANIMATION
		
		IF NOT IS_BIT_SET(iLocalBoolCheck34, LBOOL34_VOLTAGE_MINIGAME__STARTED)
			SET_BIT(iLocalBoolCheck34, LBOOL34_VOLTAGE_MINIGAME__STARTED)
			PRINTLN("[Interactables] - PROCESS_INTERACTION__VOLTAGE_MINIGAME - Setting LBOOL34_VOLTAGE_MINIGAME__STARTED")
		ENDIF
		
		PROCESS_VOLTAGE_GAME(sVoltageHack[sInteractableState.iInteractableHackingStructIndex], sVoltageGameplay, TRUE)
		
		IF IS_BIT_SET(sVoltageHack[sInteractableState.iInteractableHackingStructIndex].iBS, ciGOHACKBS_PASSED)
			SET_INTERACTABLE_COMPLETE(sInteractableState.iIndex)	
			SET_BIT(iLocalBoolCheck34, LBOOL34_VOLTAGE_MINIGAME__PASSED)
			MC_PlayerBD[iLocalPart].iHackTime =  MC_PlayerBD[iLocalPart].iHackTime + GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(sVoltageGameplay.tdTimer)
			MC_playerBD[iPartToUse].iNumHacks++			
			SET_BIT(sInteractWithParams.iInteractWithParamsRuntimeBS, ciINTERACT_WITH_PARAMS_RUNTIME_BS__StopLoopingASAP)
			
		ELIF IS_BIT_SET(sVoltageHack[sInteractableState.iInteractableHackingStructIndex].iBS, ciHGBS_TIMED_OUT)
		OR IS_BIT_SET(sVoltageHack[sInteractableState.iInteractableHackingStructIndex].iBS, ciHGBS_FAILED)
			SET_BIT(iLocalBoolCheck34, LBOOL34_VOLTAGE_MINIGAME__FAILED)
			iNumberOfTimesFailedVoltage++	
			MC_playerBD[iLocalPart].iHackingFails++
			PRINTLN("[PROCESS_INTERACTION__VOLTAGE_MINIGAME]Number of Failures increased to: ", iNumberOfTimesFailedVoltage)
			SET_BIT(sInteractWithParams.iInteractWithParamsRuntimeBS, ciINTERACT_WITH_PARAMS_RUNTIME_BS__StopLoopingASAP)
			
		ELIF IS_BIT_SET(sVoltageHack[sInteractableState.iInteractableHackingStructIndex].iBS, ciHGBS_QUIT)
			SET_BIT(sInteractWithParams.iInteractWithParamsRuntimeBS, ciINTERACT_WITH_PARAMS_RUNTIME_BS__StopLoopingASAP)
			iNumberOfTimesExitedVoltage++
			MC_PlayerBD[iLocalPart].iHackTime =  MC_PlayerBD[iLocalPart].iHackTime + GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(sVoltageGameplay.tdTimer)
			MC_playerBD[iLocalPart].iHackingFails++
			PRINTLN("[PROCESS_INTERACTION__VOLTAGE_MINIGAME]Number of Exits increased to: ", iNumberOfTimesExitedVoltage)
		ENDIF
	ENDIF

	IF PROCESS_INTERACT_WITH_ANIMATION(sInteractWithParams)
		// Interaction ended!
		END_INTERACTABLE_INTERACTION(sInteractableState.iIndex)
		NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
		VOLTAGE_CLEANUP(sVoltageHack[sInteractableState.iInteractableHackingStructIndex], sVoltageGameplay)
		START_MINIGAME_DAMAGE_PROTECTION()
	ENDIF	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Minisection: Fingerprint Minigame -----------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
PROC FILL_IW_PARAMS_FOR_FINGERPRINT_MINIGAME(FMMC_INTERACTABLE_STATE& sInteractableState, INTERACT_WITH_PARAMS& sInteractWithParams)	
	FILL_INTERACT_WITH_PARAMS_FROM_INTERACTABLE(sInteractWithParams, sInteractableState.iIndex, sInteractableState.objIndex)	
	sInteractWithParams.iInteractWith_AnimPreset = ciINTERACT_WITH_PRESET__FINGERPRINT_MINIGAME
ENDPROC

FUNC BOOL PROCESS_LOADING_INTERACTION__FINGERPRINT_MINIGAME(FMMC_INTERACTABLE_STATE& sInteractableState)
	INTERACT_WITH_PARAMS sInteractWithParams
	FILL_IW_PARAMS_FOR_FINGERPRINT_MINIGAME(sInteractableState, sInteractWithParams)
	RETURN GET_INTERACT_WITH_INFO_AND_LOAD_ANIMS(sInteractWithParams)
ENDFUNC

PROC PROCESS_INTERACTION__FINGERPRINT_MINIGAME(FMMC_INTERACTABLE_STATE& sInteractableState)
	
	INTERACT_WITH_PARAMS sInteractWithParams 
	FILL_IW_PARAMS_FOR_FINGERPRINT_MINIGAME(sInteractableState, sInteractWithParams)
		
	IF sInteractWithVars.eInteractWith_CurrentState = IW_STATE_PERFORMING_LOOPING_ANIMATION
		
		IF NOT IS_BIT_SET(iLocalBoolCheck32, LBOOL32_STARTED_FINGERPRINT_CLONE)
			SET_BIT(iLocalBoolCheck32, LBOOL32_STARTED_FINGERPRINT_CLONE)
			PRINTLN("[Interactables] - PROCESS_INTERACTION__FINGERPRINT_MINIGAME - Setting LBOOL32_STARTED_FINGERPRINT_CLONE")
		ENDIF
		
		PROCESS_FINGERPRINT_MINIGAME(sFingerprintCloneGameplay, sFingerprintClone[sInteractableState.iInteractableHackingStructIndex], GET_NUMBER_OF_PATTERNS_REQUIRED_FOR_KEYPAD(sInteractableState.iIndex, TRUE, iFingerprintClone_LessPatternsDueToFailure[sInteractableState.iInteractableHackingStructIndex]), -1, FALSE, TRUE)	
		
		IF IS_BIT_SET(sFingerprintClone[sInteractableState.iInteractableHackingStructIndex].iBS, ciHGBS_PASSED_SECTION)
			iFingerprintClone_PatternDone++
			PRINTLN("[Interactables] - PROCESS_INTERACTION__FINGERPRINT_MINIGAME - Number of Patterns passed increased to: ", iFingerprintClone_PatternDone)
		ENDIF
		
		IF IS_BIT_SET(sFingerprintClone[sInteractableState.iInteractableHackingStructIndex].iBS, ciHGBS_LOST_LIFE) 
			iFingerprintClone_LivesLost++
			PRINTLN("[Interactables] - PROCESS_INTERACTION__FINGERPRINT_MINIGAME - Number of lives lost increased to: ", iFingerprintClone_LivesLost)
		ENDIF
		
		IF IS_BIT_SET(sFingerprintClone[sInteractableState.iInteractableHackingStructIndex].iBS, ciHGBS_PASSED)
			SET_INTERACTABLE_COMPLETE(sInteractableState.iIndex)			
			SET_BIT(sInteractWithParams.iInteractWithParamsRuntimeBS, ciINTERACT_WITH_PARAMS_RUNTIME_BS__StopLoopingASAP)
			MC_PlayerBD[iLocalPart].iHackTime =  MC_PlayerBD[iLocalPart].iHackTime + GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(sFingerprintCloneGameplay.sBaseStruct.tdTimer)
			MC_playerBD[iPartToUse].iNumHacks++
			iFingerprintClone_Wins++
			PRINTLN("[Interactables] - PROCESS_INTERACTION__FINGERPRINT_MINIGAME - Number of Wins increased to: ", iFingerprintClone_Wins)
			
		ELIF IS_BIT_SET(sFingerprintClone[sInteractableState.iInteractableHackingStructIndex].iBS, ciHGBS_QUIT)
			SET_BIT(sInteractWithParams.iInteractWithParamsRuntimeBS, ciINTERACT_WITH_PARAMS_RUNTIME_BS__StopLoopingASAP)
			iFingerprintClone_Exits++
			PRINTLN("[Interactables] - PROCESS_INTERACTION__FINGERPRINT_MINIGAME - Number of Exits increased to: ", iFingerprintClone_Exits)
			
		ELIF IS_BIT_SET(sFingerprintClone[sInteractableState.iInteractableHackingStructIndex].iBS, ciHGBS_FAILED)
		OR IS_BIT_SET(sFingerprintClone[sInteractableState.iInteractableHackingStructIndex].iBS, ciHGBS_TIMED_OUT)
			SET_BIT(sInteractWithParams.iInteractWithParamsRuntimeBS, ciINTERACT_WITH_PARAMS_RUNTIME_BS__StopLoopingASAP)
			iFingerprintClone_Fails++ 
			iFingerprintClone_LocalFails[sInteractableState.iInteractableHackingStructIndex]++
			MC_PlayerBD[iLocalPart].iHackTime =  MC_PlayerBD[iLocalPart].iHackTime + GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(sFingerprintCloneGameplay.sBaseStruct.tdTimer)
			MC_playerBD[iLocalPart].iHackingFails++
			IF iFingerprintClone_LocalFails[sInteractableState.iInteractableHackingStructIndex] >= g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_FingerprintNumberOfFailuresRequiredToDecreaseDifficulty		
				iFingerprintClone_LessPatternsDueToFailure[sInteractableState.iInteractableHackingStructIndex] = FLOOR(TO_FLOAT(iFingerprintClone_LocalFails[sInteractableState.iInteractableHackingStructIndex]) / TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_FingerprintNumberOfFailuresRequiredToDecreaseDifficulty))	
				PRINTLN("[Interactables] - PROCESS_FINGERPRINT_MINIGAME_FAILED - FAILED ENOUGH TIMES TO DECREASE DIFFICULTY - ", iFingerprintClone_LessPatternsDueToFailure[sInteractableState.iInteractableHackingStructIndex], " less patterns required.")		
			ENDIF
			PRINTLN("[Interactables] - PROCESS_INTERACTION__FINGERPRINT_MINIGAME - Number of Fails increased to: ", iFingerprintClone_Fails)
		ENDIF
	ENDIF

	IF PROCESS_INTERACT_WITH_ANIMATION(sInteractWithParams)
		// Interaction ended!
		END_INTERACTABLE_INTERACTION(sInteractableState.iIndex)
		NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
		CLEAN_UP_FINGERPRINT_RETRO(sFingerprintCloneGameplay, sFingerprintClone[sInteractableState.iInteractableHackingStructIndex], DEFAULT, DEFAULT, GET_MP_INT_CHARACTER_STAT(MP_STAT_HEIST4_SESSION_ID_POSTIME))
		START_MINIGAME_DAMAGE_PROTECTION()
		iFingerprintClone_PatternDone = 0
		iFingerprintClone_LivesLost = 0
	ENDIF	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Minisection: Cut Painting -----------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
FUNC BOOL PROCESS_LOADING_INTERACTION__CUT_PAINTING(FMMC_INTERACTABLE_STATE& sInteractableState)
	INTERACT_WITH_PARAMS sInteractWithParams
	FILL_INTERACT_WITH_PARAMS_FROM_INTERACTABLE(sInteractWithParams, sInteractableState.iIndex, sInteractableState.objIndex)
	sInteractWithParams.iInteractWith_AnimPreset = ciINTERACT_WITH_PRESET__CUT_OUT_PAINTING
	
	RETURN GET_INTERACT_WITH_INFO_AND_LOAD_ANIMS(sInteractWithParams)
ENDFUNC

FUNC BOOL HAS_CUT_PAINTING_REQUIRED_INPUT_BEEN_RECEIVED(INT iLocalPaintingState)

	SWITCH iLocalPaintingState
		CASE ciIW_SUBANIM__CUTPAINTING_TOPLEFT   
			IF GET_DISABLED_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_SCRIPT_LEFT_AXIS_X) > 0.7
				RETURN TRUE
			ENDIF
		BREAK
		CASE ciIW_SUBANIM__CUTPAINTING_TOPRIGHT
			IF GET_DISABLED_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y) > 0.7
				RETURN TRUE
			ENDIF
		BREAK
		CASE ciIW_SUBANIM__CUTPAINTING_BOTTOMRIGHT
			IF GET_DISABLED_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_SCRIPT_LEFT_AXIS_X) < -0.7
				RETURN TRUE
			ENDIF
		BREAK
		CASE ciIW_SUBANIM__CUTPAINTING_BOTTOMLEFT
			IF GET_DISABLED_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y) > 0.7
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC STRING GET_CUT_PAINTING_HELPTEXT(INTERACT_WITH_PARAMS& sInteractWithParams, INT iCutPaintingSubAnim)

	UNUSED_PARAMETER(sInteractWithParams)
	
	SWITCH iCutPaintingSubAnim
		CASE ciIW_SUBANIM__CUTPAINTING_TOPLEFT   
			RETURN "MC_INTOBJ_CP_R"
		BREAK
		CASE ciIW_SUBANIM__CUTPAINTING_TOPRIGHT
			RETURN "MC_INTOBJ_CP_D"
		BREAK
		CASE ciIW_SUBANIM__CUTPAINTING_BOTTOMRIGHT
			RETURN "MC_INTOBJ_CP_L"
		BREAK
		CASE ciIW_SUBANIM__CUTPAINTING_BOTTOMLEFT
			RETURN "MC_INTOBJ_CP_D"
		BREAK
	ENDSWITCH
	
	RETURN ""
ENDFUNC

PROC PROCESS_INTERACTION__CUT_PAINTING(FMMC_INTERACTABLE_STATE& sInteractableState)
	INTERACT_WITH_PARAMS sInteractWithParams
	FILL_INTERACT_WITH_PARAMS_FROM_INTERACTABLE(sInteractWithParams, sInteractableState.iIndex, sInteractableState.objIndex)
	sInteractWithParams.iInteractWith_AnimPreset = ciINTERACT_WITH_PRESET__CUT_OUT_PAINTING
	
	IF sInteractableState.iIndex % 2 = 0
		sInteractWithParams.iInteractWith_AltAnimSet = ciIW_ALTANIMSET__CUTPAINTING_RIGHTHAND
	ELSE
		sInteractWithParams.iInteractWith_AltAnimSet = ciIW_ALTANIMSET__CUTPAINTING_LEFTHAND
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF bInteractableDebug
		TEXT_LABEL_63 tlDebugText3D = "Cut Painting State: "
		tlDebugText3D += ENUM_TO_INT(eCutPaintingInteractionState)
		DRAW_DEBUG_TEXT(tlDebugText3D, GET_FMMC_INTERACTABLE_COORDS(sInteractableState))
	ENDIF
	#ENDIF
	
	SET_INTERACTABLE_OBJECTIVE_TEXT_TEXTLABEL("MC_INT_CPNT")
	
	SWITCH eCutPaintingInteractionState
		CASE CUTPAINTING_INTERACTION_STATE__OFF
			SET_CUTPAINTING_STATE(CUTPAINTING_INTERACTION_STATE__INIT)
		BREAK
		
		CASE CUTPAINTING_INTERACTION_STATE__INIT
			IF sInteractWithVars.eInteractWith_CurrentState = IW_STATE_PERFORMING_LOOPING_ANIMATION
				SET_CUTPAINTING_STATE(CUTPAINTING_INTERACTION_STATE__IDLE)
			ELSE
				// We're still waiting for the Interact-With to get to the looping section
				RUN_INTERACTABLE_BAIL_TIMER(15000)
			ENDIF
		BREAK
		
		CASE CUTPAINTING_INTERACTION_STATE__IDLE
			IF HAS_CUT_PAINTING_REQUIRED_INPUT_BEEN_RECEIVED(sInteractWithVars.iInteractWith_SubAnimPreset)
				// Player has pressed the correct direction on the left control stick
				PLAY_INTERACT_WITH_PRESET_SUB_ANIM(sInteractWithParams, sInteractWithVars.iInteractWith_SubAnimPreset + 1)
				SET_CUTPAINTING_STATE(CUTPAINTING_INTERACTION_STATE__CUTTING)
			
			ELIF HAS_PLAYER_PRESSED_BACK_OUT_BUTTON()
				// Player has backed out
				SET_BIT(sInteractWithParams.iInteractWithParamsRuntimeBS, ciINTERACT_WITH_PARAMS_RUNTIME_BS__StopLoopingASAP)
				SET_CUTPAINTING_STATE(CUTPAINTING_INTERACTION_STATE__BACKING_OUT)
				
			ELSE
				// Player is idle
				DISPLAY_HELP_TEXT_THIS_FRAME(GET_CUT_PAINTING_HELPTEXT(sInteractWithParams, sInteractWithVars.iInteractWith_SubAnimPreset), TRUE)
				
			ENDIF
		BREAK
		
		CASE CUTPAINTING_INTERACTION_STATE__CUTTING
			IF sInteractWithVars.eInteractWith_CurrentState = IW_STATE_PERFORMING_LOOPING_ANIMATION
				// The player is back in an idle state now
				BROADCAST_CASINO_UPDATED_STEAL_PAINTING_STATE(sInteractWithVars.iInteractWith_SubAnimPreset, sInteractWithParams.iPaintingIndex, -1)
				SET_CUTPAINTING_STATE(CUTPAINTING_INTERACTION_STATE__IDLE)
			
			ELIF sInteractWithVars.eInteractWith_CurrentState = IW_STATE_PERFORMING_EXIT_ANIMATION
			AND sInteractWithVars.iInteractWith_SubAnimPreset = ciIW_SUBANIM__CUTPAINTING_FINALCUTANDROLLUP
				// The player is doing the Exit anim of the final state - this means they're now rolling up the painting and have successfully completed the minigame
				BROADCAST_CASINO_UPDATED_STEAL_PAINTING_STATE(ciIW_SUBANIM__CUTPAINTING_FINALCUTANDROLLUP, sInteractWithParams.iPaintingIndex, -1)
				SET_CUTPAINTING_STATE(CUTPAINTING_INTERACTION_STATE__ROLLINGUP)

			ELSE
				// We're still waiting for the Interact-With to get to the looping section or the exit section of the "final cut and roll up" subanim
				RUN_INTERACTABLE_BAIL_TIMER(15000)
			ENDIF
		BREAK
		
		CASE CUTPAINTING_INTERACTION_STATE__BACKING_OUT
			// Wait for Interact-With to finish - the player is backing out of the minigame
			RUN_INTERACTABLE_BAIL_TIMER(10000)
		BREAK
		
		CASE CUTPAINTING_INTERACTION_STATE__ROLLINGUP
			// Wait for Interact-With to finish - the player has completed the minigame
			RUN_INTERACTABLE_BAIL_TIMER(15000)
		BREAK
	ENDSWITCH
	
	IF PROCESS_GENERIC_INTERACTABLE_INTERACT_WITH_FUNCTIONALITY(sInteractableState, sInteractWithParams, FALSE)
		
		IF eCutPaintingInteractionState = CUTPAINTING_INTERACTION_STATE__ROLLINGUP
			
			#IF FEATURE_HEIST_ISLAND
			COMPLETE_ISLAND_HEIST_ITEM_REWARD(STEAL_FROM_SECONDARY_TARGET)
			SET_ISLAND_HEIST_SECONDARY_LOOT_GRABBED(sInteractableState.iIndex)
			#ENDIF
			
			SET_INTERACTABLE_COMPLETE(sInteractableState.iIndex)
		ELSE
			SET_INTERACTABLE_INCOMPLETE(sInteractableState.iIndex)
		ENDIF
		
		END_INTERACTABLE_INTERACTION(sInteractableState.iIndex)
	ENDIF
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Minisection: Multi-Solution Lock ----------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC FILL_INTERACT_WITH_PARAMS_FOR_MULTI_SOLUTION_LOCK(FMMC_INTERACTABLE_STATE& sInteractableState, INTERACT_WITH_PARAMS& sInteractWithParams)
	FILL_INTERACT_WITH_PARAMS_FROM_INTERACTABLE(sInteractWithParams, sInteractableState.iIndex, sInteractableState.objIndex)
	
	SWITCH GET_INTERACTABLE_MULTI_SOLUTION_LOCK_TYPE(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex])
		CASE ciInteractable_MultiSolutionLockType__ChainLock
			sInteractWithParams.iInteractWith_AnimPreset = ciINTERACT_WITH_PRESET__DEAL_WITH_CHAIN_LOCK
			
			SWITCH GET_INTERACTABLE_MULTI_SOLUTION_LOCK_CURRENT_BEST_SOLUTION(sInteractableState.iIndex)
				CASE ciInteractable_MultiSolutionLockSolution_A
					sInteractWithParams.iInteractWith_AltAnimSet = ciIW_ALTANIMSET__DEALWITHCHAIN_BOLT_CUTTERS
				BREAK
				CASE ciInteractable_MultiSolutionLockSolution_B
					sInteractWithParams.iInteractWith_AltAnimSet = ciIW_ALTANIMSET__DEALWITHCHAIN_TORCH
				BREAK
				CASE ciInteractable_MultiSolutionLockSolution_LastResort
					sInteractWithParams.iInteractWith_AltAnimSet = ciIW_ALTANIMSET__DEALWITHCHAIN_EXPLOSIVE
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL PROCESS_LOADING_INTERACTION__MULTI_SOLUTION_LOCK(FMMC_INTERACTABLE_STATE& sInteractableState)
	INTERACT_WITH_PARAMS sInteractWithParams
	FILL_INTERACT_WITH_PARAMS_FOR_MULTI_SOLUTION_LOCK(sInteractableState, sInteractWithParams)
	IF NOT GET_INTERACT_WITH_INFO_AND_LOAD_ANIMS(sInteractWithParams)
		// Waiting for Interact-With to load
		RETURN FALSE
	ENDIF
	
	REQUEST_NAMED_PTFX_ASSET("scr_ih_fin")
	IF NOT HAS_NAMED_PTFX_ASSET_LOADED("scr_ih_fin")
		// Waiting for PTFX to load
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC PROCESS_INTERACTION__MULTI_SOLUTION_LOCK__CHAIN_LOCK(FMMC_INTERACTABLE_STATE& sInteractableState, INTERACTION_VARS& sInteractionVars, INTERACT_WITH_PARAMS& sInteractWithParams)
	
	SWITCH GET_INTERACTABLE_MULTI_SOLUTION_LOCK_CURRENT_BEST_SOLUTION(sInteractableState.iIndex)
		CASE ciInteractable_MultiSolutionLockSolution_A
			IF PROCESS_GENERIC_INTERACTABLE_INTERACT_WITH_FUNCTIONALITY(sInteractableState, sInteractWithParams, TRUE)
				#IF FEATURE_HEIST_ISLAND
				SET_ISLAND_HEIST_INTEREST_ITEM_USED(ciContinuityTelemetry_InterestItemBS_BoltCuttersUsed)
				#ENDIF
			ENDIF
		BREAK
		
		CASE ciInteractable_MultiSolutionLockSolution_B
			IF PROCESS_GENERIC_INTERACTABLE_INTERACT_WITH_FUNCTIONALITY(sInteractableState, sInteractWithParams, TRUE)
				// Spook peds using the noise of the torch
				IF spookPedsAtCoordInRadius != NULL
					CALL spookPedsAtCoordInRadius(GET_FMMC_INTERACTABLE_COORDS(sInteractableState), g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].fInteractable_MultiSolutionLock_GenericFloatOption[ciInteractable_MultiSolutionLockSolution_B], ciSpookCoordID_OBJECT_MULTI_SOLUTION_LOCK, LocalPlayerPed, FALSE)
				ENDIF
			ENDIF
		BREAK
		
		CASE ciInteractable_MultiSolutionLockSolution_LastResort
			IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(sInteractionVars.niInteractionSpawnedEntity)
			OR sInteractWithVars.eInteractWith_CurrentState != IW_STATE_IDLE
				IF PROCESS_GENERIC_INTERACTABLE_INTERACT_WITH_FUNCTIONALITY(sInteractableState, sInteractWithParams, FALSE)
					#IF FEATURE_HEIST_ISLAND
					SET_ISLAND_HEIST_INTEREST_ITEM_USED(ciContinuityTelemetry_InterestItemBS_ExplosivesUsed)
					#ENDIF
				
					// Explosive is now planted!
					IF NOT IS_BIT_SET(gMC_LocalVariables_VARS.iLocalBGScriptBS, ciLocalBGScriptBS_DoNotDisablePlayerControl)
						NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE) // Turn control back on so the player can walk away & trigger the explosive
					ENDIF
					
					PRINTLN("[Interactables][Interactable ", sInteractableState.iIndex, "][MultiSolutionLock] PROCESS_INTERACTION__MULTI_SOLUTION_LOCK | Explosive planted!")
				ENDIF
			ELSE
			
				IF NOT NETWORK_HAS_CONTROL_OF_NETWORK_ID(sInteractionVars.niInteractionSpawnedEntity)
					NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(sInteractionVars.niInteractionSpawnedEntity)
					PRINTLN("[Interactables][Interactable ", sInteractableState.iIndex, "][MultiSolutionLock] PROCESS_INTERACTION__MULTI_SOLUTION_LOCK | Requesting control of the explosive obj!")
					EXIT
				ENDIF
				
				IF NOT NETWORK_HAS_CONTROL_OF_NETWORK_ID(sInteractableState.niIndex)
					NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(sInteractableState.niIndex)
					PRINTLN("[Interactables][Interactable ", sInteractableState.iIndex, "][MultiSolutionLock] PROCESS_INTERACTION__MULTI_SOLUTION_LOCK | Requesting control of the interactable!")
					EXIT
				ENDIF
				
				OBJECT_INDEX oiExplosiveObject
				oiExplosiveObject = NET_TO_OBJ(sInteractionVars.niInteractionSpawnedEntity)
				
				IF IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_CONTEXT)
				OR HAS_ENTITY_BEEN_DAMAGED_BY_ANY_PED(oiExplosiveObject)
					// Detonate the planted explosive
					PRINTLN("[Interactables][Interactable ", sInteractableState.iIndex, "][MultiSolutionLock] PROCESS_INTERACTION__MULTI_SOLUTION_LOCK | Explosive detonated!")
					
					USE_PARTICLE_FX_ASSET("scr_ih_fin")
					START_NETWORKED_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_ih_fin_explosive_charge", GET_ENTITY_COORDS(oiExplosiveObject), <<0,0,0>>)
					
					FLOAT fBaseShakeSize
					fBaseShakeSize = 0.9
					FLOAT fShakeSize
					fShakeSize = (POW(fBaseShakeSize, 2.0) / sInteractableState.fDistanceToInteractable)
					
					SHAKE_GAMEPLAY_CAM("SMALL_EXPLOSION_SHAKE", CLAMP(fShakeSize, 0.05, 0.25))
					PLAY_SOUND_FROM_ENTITY(-1, "magnetic_explosion", sInteractableState.objIndex, "dlc_h4_heist_finale_sounds_soundset", TRUE, 100)
					
					SET_ENTITY_VISIBLE(sInteractableState.objIndex, FALSE)
					
					// Spook peds using the noise of the explosion
					IF spookPedsAtCoordInRadius != NULL
						CALL spookPedsAtCoordInRadius(GET_FMMC_INTERACTABLE_COORDS(sInteractableState),  g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].fInteractable_MultiSolutionLock_GenericFloatOption[ciInteractable_MultiSolutionLockSolution_LastResort], ciSpookCoordID_OBJECT_MULTI_SOLUTION_LOCK, LocalPlayerPed, FALSE)
					ENDIF
					
					DELETE_NET_ID(sInteractionVars.niInteractionSpawnedEntity)
					sInteractionVars.niInteractionSpawnedEntity = NULL
					
					SET_INTERACTABLE_COMPLETE(sInteractableState.iIndex)
					END_INTERACTABLE_INTERACTION(sInteractableState.iIndex)
					
				ELSE
					DISPLAY_HELP_TEXT_THIS_FRAME("MC_INT_EXP1", TRUE)
					SET_INTERACTABLE_OBJECTIVE_TEXT_TEXTLABEL("MC_INT_EXP2")
					
					FLOAT fMaxAllowedDistance
					fMaxAllowedDistance = 15.0 // The distance you're allowed to walk away from the explosive before you're forced back
					
					IF VDIST2(GET_ENTITY_COORDS(LocalPlayerPed), GET_FMMC_INTERACTABLE_COORDS(sInteractableState)) > POW(fMaxAllowedDistance, 2.0)
					AND NOT IS_PED_PERFORMING_TASK(LocalPlayerPed, SCRIPT_TASK_GO_STRAIGHT_TO_COORD)
						VECTOR vGotoCoords
						vGotoCoords = GET_FMMC_INTERACTABLE_COORDS(sInteractableState)
						vGotoCoords += NORMALISE_VECTOR((GET_ENTITY_COORDS(LocalPlayerPed) - GET_FMMC_INTERACTABLE_COORDS(sInteractableState))) * (fMaxAllowedDistance * 0.75)
						TASK_GO_STRAIGHT_TO_COORD(LocalPlayerPed, vGotoCoords, 0.5, DEFAULT, DEFAULT)
						PRINTLN("[Interactables][Interactable ", sInteractableState.iIndex, "][MultiSolutionLock] PROCESS_INTERACTION__MULTI_SOLUTION_LOCK | Tasking player back towards the explosive!")
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
	ENDSWITCH
ENDPROC

PROC PROCESS_INTERACTION__MULTI_SOLUTION_LOCK(FMMC_INTERACTABLE_STATE& sInteractableState, INTERACTION_VARS& sInteractionVars)
	
	INTERACT_WITH_PARAMS sInteractWithParams
	FILL_INTERACT_WITH_PARAMS_FOR_MULTI_SOLUTION_LOCK(sInteractableState, sInteractWithParams)
	
	SWITCH GET_INTERACTABLE_MULTI_SOLUTION_LOCK_TYPE(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex])
		CASE ciInteractable_MultiSolutionLockType__ChainLock
			PROCESS_INTERACTION__MULTI_SOLUTION_LOCK__CHAIN_LOCK(sInteractableState, sInteractionVars, sInteractWithParams)
		BREAK
	ENDSWITCH
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Minisection: Switch Outfit ----------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
PROC FILL_IW_PARAMS_FOR_SWITCH_OUTFIT(FMMC_INTERACTABLE_STATE& sInteractableState, INTERACT_WITH_PARAMS& sInteractWithParams)
	FILL_INTERACT_WITH_PARAMS_FROM_INTERACTABLE(sInteractWithParams, sInteractableState.iIndex, sInteractableState.objIndex)
	sInteractWithParams.iInteractWith_AnimPreset = ciINTERACT_WITH_PRESET__CLOTHES
	sInteractWithParams.iInteractWith_AltAnimSet = g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_InteractWithAltAnimSet
ENDPROC

FUNC BOOL PROCESS_LOADING_INTERACTION__SWITCH_OUTFIT(FMMC_INTERACTABLE_STATE& sInteractableState)
	INTERACT_WITH_PARAMS sInteractWithParams
	FILL_IW_PARAMS_FOR_SWITCH_OUTFIT(sInteractableState, sInteractWithParams)
	RETURN GET_INTERACT_WITH_INFO_AND_LOAD_ANIMS(sInteractWithParams)
ENDFUNC

PROC PROCESS_INTERACTION__SWITCH_CLOTHES(FMMC_INTERACTABLE_STATE& sInteractableState)
	INTERACT_WITH_PARAMS sInteractWithParams
	FILL_IW_PARAMS_FOR_SWITCH_OUTFIT(sInteractableState, sInteractWithParams)
	
	PROCESS_GENERIC_INTERACTABLE_INTERACT_WITH_FUNCTIONALITY(sInteractableState, sInteractWithParams, TRUE)
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Minisection: Loot Vehicle Boot ------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
PROC FILL_IW_PARAMS_FOR_LOOT_VEHICLE_TRUNK(FMMC_INTERACTABLE_STATE& sInteractableState, INTERACTION_VARS& sInteractionVars, INTERACT_WITH_PARAMS& sInteractWithParams)
	FILL_INTERACT_WITH_PARAMS_FROM_INTERACTABLE(sInteractWithParams, sInteractableState.iIndex, sInteractableState.objIndex)
	sInteractWithParams.iInteractWith_AnimPreset = ciINTERACT_WITH_PRESET__OPEN_VEHICLE_TRUNK
	
	IF NOT IS_INTERACTABLE_ATTACHED_VEHICLE_BOOT_OPEN(sInteractableState.iIndex)
	OR IS_BIT_SET(sInteractionVars.iOngoingInteractionBS, ciONGOING_INTERACTION_BS__CURRENT_LOOT_VEHICLE_INTERACTABLE_OPENING_TRUNK)
		sInteractWithParams.iInteractWith_AnimPreset = ciINTERACT_WITH_PRESET__OPEN_VEHICLE_TRUNK
	ELSE
		// Leave the preset as the one selected in the creator and clear the vehicle vars so the anim plays as normal, attached to the Interactable itself
		sInteractWithParams.iAttachedVehicleIndex = -1
		iInteractObject_AttachedVehicle[sInteractWithParams.iNetworkedObjectIndex] = -1
	ENDIF
ENDPROC

FUNC BOOL PROCESS_LOADING_INTERACTION__LOOT_VEHICLE_TRUNK(FMMC_INTERACTABLE_STATE& sInteractableState, INTERACTION_VARS& sInteractionVars)
	INTERACT_WITH_PARAMS sInteractWithParams
	FILL_IW_PARAMS_FOR_LOOT_VEHICLE_TRUNK(sInteractableState, sInteractionVars, sInteractWithParams)
	
	RETURN GET_INTERACT_WITH_INFO_AND_LOAD_ANIMS(sInteractWithParams)
ENDFUNC

PROC PROCESS_INTERACTION__LOOT_VEHICLE_TRUNK(FMMC_INTERACTABLE_STATE& sInteractableState, INTERACTION_VARS& sInteractionVars)
	INTERACT_WITH_PARAMS sInteractWithParams
	FILL_IW_PARAMS_FOR_LOOT_VEHICLE_TRUNK(sInteractableState, sInteractionVars, sInteractWithParams)
	
	IF NOT IS_INTERACTABLE_ATTACHED_VEHICLE_BOOT_OPEN(sInteractableState.iIndex)
	OR IS_BIT_SET(sInteractionVars.iOngoingInteractionBS, ciONGOING_INTERACTION_BS__CURRENT_LOOT_VEHICLE_INTERACTABLE_OPENING_TRUNK)
		SET_BIT(sInteractionVars.iOngoingInteractionBS, ciONGOING_INTERACTION_BS__CURRENT_LOOT_VEHICLE_INTERACTABLE_OPENING_TRUNK)
		
		// First, do the Interact-With to get the player to open the boot
		IF PROCESS_GENERIC_INTERACTABLE_INTERACT_WITH_FUNCTIONALITY(sInteractableState, sInteractWithParams, FALSE)
			END_INTERACTABLE_INTERACTION(sInteractableState.iIndex)
		ENDIF
	ELSE
		PROCESS_GENERIC_INTERACTABLE_INTERACT_WITH_FUNCTIONALITY(sInteractableState, sInteractWithParams, TRUE)
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Minisection: Loot Tray ----------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL PROCESS_LOADING_INTERACTION__LOOT_TRAY(FMMC_INTERACTABLE_STATE& sInteractableState)
	
	REQUEST_HEIST_BAG_MINIGAME_ASSETS(sInteractableState.objIndex, IS_PED_WEARING_HIGH_HEELS(LocalPlayerPed), MC_playerBD_1[iLocalPart].mnHeistGearBag)
	RETURN HAVE_HEIST_BAG_MINIGAME_ASSETS_LOADED(sInteractableState.objIndex, IS_PED_WEARING_HIGH_HEELS(LocalPlayerPed), MC_playerBD_1[iLocalPart].mnHeistGearBag)
	
ENDFUNC

PROC PROCESS_INTERACTION__LOOT_TRAY(FMMC_INTERACTABLE_STATE& sInteractableState, INTERACTION_VARS& sInteractionVars)
	
	IF PROCESS_CASH_GRAB(sInteractableState.objIndex, sInteractableState.iIndex)
		IF IS_BIT_SET(sInteractionVars.iOngoingInteractionBS, ciONGOING_INTERACTION_BS__CASH_GRAB_COMPLETE)
			PRINTLN("[CashGrab][Interactables][Interactable ", sInteractableState.iIndex, "] PROCESS_INTERACTION__LOOT_TRAY | All cash was grabbed!")
			SET_INTERACTABLE_COMPLETE(sInteractableState.iIndex)
			
		ELSE
			PRINTLN("[CashGrab][Interactables][Interactable ", sInteractableState.iIndex, "] PROCESS_INTERACTION__LOOT_TRAY | Ending interaction without having grabbed all of the cash!")
			
		ENDIF
		
		END_INTERACTABLE_INTERACTION(sInteractableState.iIndex)
	ENDIF
	
ENDPROC
		

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Minisection: Plant Thermal Charge ---------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
PROC FILL_IW_PARAMS_FOR_PLANT_THERMAL_CHARGE(FMMC_INTERACTABLE_STATE& sInteractableState, INTERACT_WITH_PARAMS& sInteractWithParams)
	FILL_INTERACT_WITH_PARAMS_FROM_INTERACTABLE(sInteractWithParams, sInteractableState.iIndex, sInteractableState.objIndex)
	sInteractWithParams.iInteractWith_AnimPreset = ciINTERACT_WITH_PRESET__PLANT_THERMAL_CHARGE
ENDPROC

FUNC BOOL PROCESS_LOADING_INTERACTION__PLANT_THERMAL_CHARGE(FMMC_INTERACTABLE_STATE& sInteractableState)
	INTERACT_WITH_PARAMS sInteractWithParams
	FILL_IW_PARAMS_FOR_PLANT_THERMAL_CHARGE(sInteractableState, sInteractWithParams)
	
	REQUEST_ANIM_DICT("anim@heists@ornate_bank@thermal_charge")
	
	REQUEST_NAMED_PTFX_ASSET("scr_ch_finale")
	IF NOT HAS_NAMED_PTFX_ASSET_LOADED("scr_ch_finale")
		PRINTLN("[Interactables][Interactable ", sInteractableState.iIndex, "] PROCESS_LOADING_INTERACTION__PLANT_THERMAL_CHARGE | Loading scr_ch_finale...")
		RETURN FALSE
	ENDIF
	
	REQUEST_NAMED_PTFX_ASSET("pat_heist")
	IF NOT HAS_NAMED_PTFX_ASSET_LOADED("pat_heist")
		PRINTLN("[Interactables][Interactable ", sInteractableState.iIndex, "] PROCESS_LOADING_INTERACTION__PLANT_THERMAL_CHARGE | Loading pat_heist...")
		RETURN FALSE
	ENDIF
	
	REQUEST_NAMED_PTFX_ASSET("scr_ornate_heist")
	IF NOT HAS_NAMED_PTFX_ASSET_LOADED("scr_ornate_heist")
		PRINTLN("[Interactables][Interactable ", sInteractableState.iIndex, "] PROCESS_LOADING_INTERACTION__PLANT_THERMAL_CHARGE | Loading scr_ornate_heist...")
		RETURN FALSE
	ENDIF
	
	RETURN GET_INTERACT_WITH_INFO_AND_LOAD_ANIMS(sInteractWithParams)
ENDFUNC

PROC PROCESS_INTERACTION__PLANT_THERMAL_CHARGE(FMMC_INTERACTABLE_STATE& sInteractableState, INTERACTION_VARS& sInteractionVars)
	INTERACT_WITH_PARAMS sInteractWithParams
	FILL_IW_PARAMS_FOR_PLANT_THERMAL_CHARGE(sInteractableState, sInteractWithParams)
	
	IF NOT sInteractableState.bIsBackgroundInteraction
		
		SET_PED_CURRENT_WEAPON_VISIBLE(LocalPlayerPed, FALSE)
		
		// Plant the charge
		IF PROCESS_GENERIC_INTERACTABLE_INTERACT_WITH_FUNCTIONALITY(sInteractableState, sInteractWithParams, FALSE)
			// Once the charge is planted, move this interaction into being a background interaction
			CONVERT_INTERACTION_TO_BACKGROUND_INTERACTION(sInteractableState.iOngoingInteractionIndex)
		ENDIF
		
		IF sInteractWithVars.eInteractWith_CurrentState = IW_STATE_CLEANUP_WHEN_READY
		OR sInteractWithVars.eInteractWith_CurrentState = IW_STATE_COMPLETED
			
			PROCESS_INTERACTION_SECONDARY_ANIM(sInteractionVars, TRUE, "anim@heists@ornate_bank@thermal_charge", "cover_eyes_intro", "cover_eyes_loop", "cover_eyes_exit", "BONEMASK_HEAD_NECK_AND_L_ARM")
			
			IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(sInteractionVars.ptInteractionPTFX[ciINTERACTION_PTFX__THERMAL_CHARGE_BURN])
				USE_PARTICLE_FX_ASSET("scr_ch_finale")
				sInteractionVars.ptInteractionPTFX[ciINTERACTION_PTFX__THERMAL_CHARGE_BURN] = START_NETWORKED_PARTICLE_FX_LOOPED_ON_ENTITY("scr_ch_finale_thermal_burn", sInteractableState.objIndex, (<<0,0,0>>), (<<0, 0, 0>>))
				
				USE_PARTICLE_FX_ASSET("scr_ornate_heist")
				sInteractionVars.ptInteractionPTFX[ciINTERACTION_PTFX__THERMAL_CHARGE_DRIP] = START_NETWORKED_PARTICLE_FX_LOOPED_ON_ENTITY("scr_heist_ornate_metal_drip", sInteractableState.objIndex, (<<0, 1, 0>>), (<<0, 0, 0>>))
				
				USE_PARTICLE_FX_ASSET("pat_heist")
				sInteractionVars.ptInteractionPTFX[ciINTERACTION_PTFX__THERMAL_CHARGE_SPARKS] = START_NETWORKED_PARTICLE_FX_LOOPED_ON_ENTITY("scr_heist_ornate_thermal_burn_patch", sInteractableState.objIndex, (<<0, 1, 0>>), (<<0.0, 0.0, 0.0>>))
			ENDIF
			
		ENDIF
		
	ELSE
		// Process the ongoing thermal charge in the background
		IF RUN_TIMER(sInteractionVars.stInteractionTimer, ciTHERMAL_CHARGE_BURN_TIME)
			
			PLAY_SOUND_FROM_COORD(-1, "Gate_Lock_Break", GET_FMMC_INTERACTABLE_COORDS(sInteractableState), "DLC_HEISTS_ORNATE_BANK_FINALE_SOUNDS", TRUE, 30)
			
			IF NETWORK_DOES_NETWORK_ID_EXIST(sInteractionVars.niInteractionSpawnedEntity)
				PRINTLN("[Interactables][Interactable ", sInteractableState.iIndex, "] PROCESS_INTERACTION__PLANT_THERMAL_CHARGE | Deleting thermal charge prop")
				DELETE_NET_ID(sInteractionVars.niInteractionSpawnedEntity)
			ENDIF
			
			SET_INTERACTABLE_COMPLETE(sInteractableState.iIndex)
			END_INTERACTABLE_INTERACTION(sInteractableState.iIndex)

		ELSE
			
			INT iBurnTime = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(sInteractionVars.stInteractionTimer) - ciTHERMAL_CHARGE_DIE_OFF_START_TIME
			
			// Handle getting the player to cover their eyes when relevant
			BOOL bShouldCoverEyes = FALSE
			FLOAT fCoverEyesHeading = GET_HEADING_FROM_COORDS_LA(GET_ENTITY_COORDS(LocalPlayerPed), GET_FMMC_INTERACTABLE_COORDS(sInteractableState))
			
			IF sInteractableState.fDistanceToInteractable < POW(cfTHERMAL_CHARGE_COVER_EYES_DISTANCE, 2.0)
			AND IS_PED_CLOSE_TO_HEADING(LocalPlayerPed, fCoverEyesHeading, cfTHERMAL_CHARGE_COVER_EYES_HEADING_RANGE)
				bShouldCoverEyes = TRUE
			ENDIF
			
			PROCESS_INTERACTION_SECONDARY_ANIM(sInteractionVars, bShouldCoverEyes, "anim@heists@ornate_bank@thermal_charge", "cover_eyes_intro", "cover_eyes_loop", "cover_eyes_exit", "BONEMASK_HEAD_NECK_AND_L_ARM")
			
			// Keep the player's weapon hidden for an extra bit of time to hide any weird blending
			IF iBurnTime >= ciTHERMAL_CHARGE_WEAPON_REAPPEAR_DELAY
				SET_PED_CURRENT_WEAPON_VISIBLE(LocalPlayerPed, TRUE)
			ELSE
				SET_PED_CURRENT_WEAPON_VISIBLE(LocalPlayerPed, FALSE)
			ENDIF
			
			// Handle getting some of the PTFX to die off gradually
			IF DOES_PARTICLE_FX_LOOPED_EXIST(sInteractionVars.ptInteractionPTFX[ciINTERACTION_PTFX__THERMAL_CHARGE_DRIP])
				FLOAT fEvo = CLAMP(TO_FLOAT(iBurnTime) / TO_FLOAT(ciTHERMAL_CHARGE_DIE_OFF_DRIP_TIME), 0.0, 1.0)
				IF fEvo < 1.0
					SET_PARTICLE_FX_LOOPED_EVOLUTION(sInteractionVars.ptInteractionPTFX[ciINTERACTION_PTFX__THERMAL_CHARGE_DRIP], "DieOff", fEvo)
				ELSE
					STOP_PARTICLE_FX_LOOPED(sInteractionVars.ptInteractionPTFX[ciINTERACTION_PTFX__THERMAL_CHARGE_DRIP])
				ENDIF
			ENDIF
			
			IF DOES_PARTICLE_FX_LOOPED_EXIST(sInteractionVars.ptInteractionPTFX[ciINTERACTION_PTFX__THERMAL_CHARGE_SPARKS])
				FLOAT fEvo = CLAMP(TO_FLOAT(iBurnTime) / TO_FLOAT(ciTHERMAL_CHARGE_DIE_OFF_SPARKS_TIME), 0.0, 1.0)
				IF fEvo < 1.0
					SET_PARTICLE_FX_LOOPED_EVOLUTION(sInteractionVars.ptInteractionPTFX[ciINTERACTION_PTFX__THERMAL_CHARGE_SPARKS], "DIE_OFF", fEvo)
				ELSE
					STOP_PARTICLE_FX_LOOPED(sInteractionVars.ptInteractionPTFX[ciINTERACTION_PTFX__THERMAL_CHARGE_SPARKS])
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Continuity
// ##### Description: Functions for processing Interactable continuity
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_INITIAL_INTERACTABLE_CONTINUITY()
			
	IF IS_BIT_SET(iLocalBoolCheck8, LBOOL8_INTERACTABLE_CONTINUITY_INIT_DONE)
		EXIT
	ENDIF
		
	SET_BIT(iLocalBoolCheck8, LBOOL8_INTERACTABLE_CONTINUITY_INIT_DONE)		
			
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_InteractableTracking)								
		EXIT
	ENDIF
	
	INT iInteractable
	
	FOR iInteractable = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfInteractables - 1
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iContinuityId = -1
			RELOOP
		ENDIF
		
		IF FMMC_IS_LONG_BIT_SET(MC_serverBD_1.sMissionContinuityVars.iInteractablesCompleteBitset, g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iContinuityId)
			PRINTLN("[CONTINUITY][Interactables][Interactable ", iInteractable, "] PROCESS_INITIAL_INTERACTABLE_CONTINUITY | Interactable was complete on a previous strand.")
			SET_INTERACTABLE_COMPLETE(iInteractable, TRUE)
		ENDIF
		
	ENDFOR
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Interactable Every-Frame Processing -----------------------------------------------------------------------------------------------------------------------------
// ##### Description: Functions that are called during the Interactable every-frame loop.  ---------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

#IF IS_DEBUG_BUILD
PROC PROCESS_INTERACTABLE_INTERACTION_DEBUG(FMMC_INTERACTABLE_STATE sInteractableState)
	
	IF NOT bInteractableDebug
		EXIT
	ENDIF
	
	VECTOR vDrawPos = <<0.15, 0.25, 0.0>>
	vDrawPos.y += 0.04 * sInteractableState.iOngoingInteractionIndex
	
	TEXT_LABEL_63 tlListDebug = "iCurrentInteractables["
	tlListDebug += sInteractableState.iOngoingInteractionIndex
	tlListDebug += "] = "
	tlListDebug += MC_playerBD[iLocalPart].iCurrentInteractables[sInteractableState.iOngoingInteractionIndex]
	DRAW_DEBUG_TEXT_2D(tlListDebug, (vDrawPos), 255, 255, 255, 255)

ENDPROC

PROC PROCESS_INTERACTABLE_APPROACH_DEBUG(FMMC_INTERACTABLE_STATE sInteractableState)
	IF NOT bInteractableDebug
		EXIT
	ENDIF
	
	DRAW_DEBUG_LINE(sInteractableState.vInteractionCoords, GET_FMMC_INTERACTABLE_COORDS(sInteractableState), 200, 0, 200)
	
	IF SHOULD_INTERACTABLE_BE_INVISIBLE(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex], FALSE)
		DRAW_DEBUG_MODEL_FOR_INTERACTABLE(sInteractableState.objIndex)
	ENDIF
	
	TEXT_LABEL_63 tlVisualDebug = "Interactable "
	tlVisualDebug += sInteractableState.iIndex
	DRAW_DEBUG_TEXT(tlVisualDebug, GET_FMMC_INTERACTABLE_COORDS(sInteractableState), 255, 255, 255, 255)
	
	IF sInteractableState.fDistanceToInteractable > POW(GET_INTERACTABLE_INTERACTION_COORDS_MAX_DISTANCE(sInteractableState), 2.0)
		DRAW_DEBUG_SPHERE(sInteractableState.vInteractionCoords, GET_INTERACTABLE_INTERACTION_COORDS_MAX_DISTANCE(sInteractableState), 200, 0, 200, 50)
	ELSE
		DRAW_DEBUG_SPHERE(sInteractableState.vInteractionCoords, GET_INTERACTABLE_INTERACTION_COORDS_MAX_DISTANCE(sInteractableState), 0, 255, 0, 50)
	ENDIF
	
	IF DOES_INTERACTABLE_HAVE_LINKED_INTERACTABLE(sInteractableState.iIndex)
		DRAW_DEBUG_LINE(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].vInteractable_Position, g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[GET_LINKED_INTERACTABLE_INDEX(sInteractableState.iIndex)].vInteractable_Position, 0, 255, 255)
	ENDIF
	
	IF IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD5)
		TEXT_LABEL_63 tlDebugText
		tlDebugText = "Int Heading: "
		tlDebugText += ROUND(sInteractableState.fInteractionHeading)
		tlDebugText += "/ Cur Heading: "
		tlDebugText += ROUND(GET_ENTITY_HEADING(LocalPlayerPed))
		DRAW_DEBUG_TEXT(tlDebugText, sInteractableState.vInteractionCoords)
		
		IF IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD1)
		AND IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD2)
			PRINTLN("[Interactables] PROCESS_INTERACTABLE_DEBUG | Resetting all Interactables to being incomplete!")
			ASSERTLN("[Interactables] PROCESS_INTERACTABLE_DEBUG | Resetting all Interactables to being incomplete!")
			
			INT iInteractableReset
			FOR iInteractableReset = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfInteractables - 1
				SET_INTERACTABLE_INCOMPLETE(iInteractableReset, FALSE)
			ENDFOR
		ENDIF
		
		INT iIntLoop
		VECTOR vIntDrawPos = <<0.3, 0.21, 0.0>>
		FOR iIntLoop = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfInteractables - 1
			TEXT_LABEL_63 tlUserDebug = "iInteractable_InteractingParticipant["
			tlUserDebug += iIntLoop
			tlUserDebug += "]: "
			tlUserDebug += MC_serverBD_4.sIntServerData.iInteractable_InteractingParticipant[iIntLoop]
			DRAW_DEBUG_TEXT_2D(tlUserDebug, (vIntDrawPos), 255, 255, 255, 255)
			
			vIntDrawPos.y += 0.04
		ENDFOR
	ENDIF
	
ENDPROC
#ENDIF

PROC BAIL_CURRENT_INTERACTION_NOW(FMMC_INTERACTABLE_STATE& sInteractableState)
	
	PRINTLN("[Interactables][Interactable ", sInteractableState.iIndex, "] BAIL_CURRENT_INTERACTION_NOW | Bailing Interaction with Interactable ", sInteractableState.iIndex)
	
	SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_InteractionType
		CASE ciInteractableInteraction_LootTray
			PROCESS_CASH_GRAB_FAIL_CLEANUP(sInteractableState.iIndex, sInteractableState.objIndex, CASH_GRAB_GET_CASH_ANIM_NAME(sInteractableState.mn), TRUE)
			CASH_GRAB_SET_NEW_STATE(ciCASH_GRAB_STATE_IDLE)
		BREAK
	ENDSWITCH

	IF bLocalPlayerPedOk
		CLEAR_PED_TASKS(LocalPlayerPed)
	ENDIF
	
	END_INTERACTABLE_INTERACTION(sInteractableState.iIndex)
	BROADCAST_FMMC_INTERACTABLE_EVENT(FMMC_INTERACTABLE_EVENT_TYPE__RESET_INTERACTABLE, sInteractableState.iIndex)

ENDPROC

PROC PROCESS_INTERACTION(FMMC_INTERACTABLE_STATE& sInteractableState, INTERACTION_VARS& sInteractionVars)
	
	IF bIsAnySpectator
		// We're just spectating this Interaction, not performing it ourselves
		EXIT
	ENDIF
	
	IF SHOULD_BAIL_INTERACTION(sInteractableState)
		IF sInteractWithVars.eInteractWith_CurrentState = IW_STATE_IDLE
			BAIL_CURRENT_INTERACTION_NOW(sInteractableState)
			EXIT
		ELSE
			// Let Interact-With bail itself first
			MAKE_INTERACT_WITH_BAIL_ASAP()
		ENDIF
	ENDIF
	
	SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_InteractionType
	
		CASE ciInteractableInteraction_InteractWith
			PROCESS_INTERACTION__INTERACT_WITH_ANIM(sInteractableState)
		BREAK
		
		CASE ciInteractableInteraction_SyncLock
			PROCESS_INTERACTION__SYNC_LOCK(sInteractableState)
		BREAK
		
		CASE ciInteractableInteraction_HackingMinigameA
			PROCESS_INTERACTION__HACKING_MINIGAME_A(sInteractableState)
		BREAK
		
		CASE ciInteractableInteraction_CutPainting
			PROCESS_INTERACTION__CUT_PAINTING(sInteractableState)
		BREAK
		
		CASE ciInteractableInteraction_MultiSolutionLock
			PROCESS_INTERACTION__MULTI_SOLUTION_LOCK(sInteractableState, sInteractionVars)
		BREAK
		
		CASE ciInteractableInteraction_SwitchOutfit
			PROCESS_INTERACTION__SWITCH_CLOTHES(sInteractableState)
		BREAK
		
		CASE ciInteractableInteraction_LootTray
			PROCESS_INTERACTION__LOOT_TRAY(sInteractableState, sInteractionVars)
		BREAK
		
		CASE ciInteractableInteraction_VoltageMinigame
			PROCESS_INTERACTION__VOLTAGE_MINIGAME(sInteractableState)
		BREAK
		
		CASE ciInteractableInteraction_FingerPrintMinigame
			PROCESS_INTERACTION__FINGERPRINT_MINIGAME(sInteractableState)
		BREAK
		
		CASE ciInteractableInteraction_LootVehicleTrunk
			PROCESS_INTERACTION__LOOT_VEHICLE_TRUNK(sInteractableState, sInteractionVars)
		BREAK
		
		CASE ciInteractableInteraction_PlantThermalCharge
			PROCESS_INTERACTION__PLANT_THERMAL_CHARGE(sInteractableState, sInteractionVars)
		BREAK
		
		DEFAULT
			// PLACEHOLDER - Just make the player and the Interactable spin around
			SET_ENTITY_HEADING(sInteractableState.objIndex, TO_FLOAT(GET_FRAME_COUNT()))
			SET_ENTITY_HEADING(LocalPlayerPed, TO_FLOAT(GET_FRAME_COUNT()))
		BREAK
	ENDSWITCH
	
ENDPROC

FUNC BOOL SHOULD_INTERACTION_START_NOW(FMMC_INTERACTABLE_STATE& sInteractableState)

	IF SHOULD_INTERACTABLE_INTERACTION_REQUIRE_PLAYER_INPUT_TO_START(sInteractableState)
		
		BLOCK_INTERACT_WITH_CONFLICTING_INPUTS()
		
		IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_CONTEXT)
		OR IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_CONTEXT)
			RETURN TRUE
		ENDIF
	ELSE
		// Start as soon as the player is in the correct place and able to interact
		RETURN TRUE
		
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_INTERACTABLE_WAITING_FOR_INPUT(FMMC_INTERACTABLE_STATE& sInteractableState, INTERACTION_VARS& sInteractionVars)
	
	// Wait for input to start the interaction
	IF SHOULD_INTERACTION_START_NOW(sInteractableState)
		START_INTERACTABLE_INTERACTION(sInteractableState.iIndex)
	ELSE
		STRING sHelpText = GET_INTERACTABLE_PROMPT_HELPTEXT(sInteractableState, sInteractionVars)
		DISPLAY_HELP_TEXT_THIS_FRAME(sHelpText, TRUE)
	ENDIF
ENDPROC

FUNC FLOAT GET_OFFSET_MULITPLIER_TO_USE_FOR_INTERACT_WITH_INTERACTION_COORDS(FMMC_INTERACTABLE_STATE& sInteractableState)

	IF g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_AttachedEntityType != ciENTITY_TYPE_NONE
	AND g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_AttachedEntityIndex > -1
		RETURN 0.0
	ENDIF
	
	SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_InteractionType
		CASE ciInteractableInteraction_CutPainting
			RETURN -0.1
		BREAK
		CASE ciInteractableInteraction_SwitchOutfit
			RETURN 0.0
		BREAK
	ENDSWITCH	
	
	RETURN 0.25
ENDFUNC

FUNC BOOL SHOULD_INTERACTABLE_INTERACT_WITH_CHECK_PLAYER_SIDE(FMMC_INTERACTABLE_STATE& sInteractableState)
	
	SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_InteractionType
		CASE ciInteractableInteraction_InteractWith
			IF g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_InteractWithAnimPreset = ciINTERACT_WITH_PRESET__PASS_GUARD
			OR g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_InteractWithAnimPreset = ciINTERACT_WITH_PRESET__CUTSCENE_LEAD_OUT
				RETURN FALSE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

PROC SET_INTERACTABLE_INTERACTION_COORDS_AND_HEADING_FROM_INTERACT_WITH_VARS(FMMC_INTERACTABLE_STATE& sInteractableState)
	
	IF IS_VECTOR_ZERO(sInteractWithVars.vInteractWith_PlayerStartPos)
		SET_INTERACTABLE_INTERACTION_COORDS(sInteractableState, GET_FMMC_INTERACTABLE_COORDS(sInteractableState))
		sInteractableState.fInteractionHeading = 999.9
		EXIT
	ENDIF
	
	// Offset the Interaction Coords:
	VECTOR vCoordsToUse = sInteractWithVars.vInteractWith_PlayerStartPos
	VECTOR vPlayerStartPosOffset = (sInteractWithVars.vInteractWith_PlayerStartPos - GET_FMMC_INTERACTABLE_COORDS(sInteractableState))
	vPlayerStartPosOffset.z *= 0.5
	
	FLOAT fPlayerOffsetMultiplier = g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].fInteractable_InteractionDistance
	fPlayerOffsetMultiplier *= GET_OFFSET_MULITPLIER_TO_USE_FOR_INTERACT_WITH_INTERACTION_COORDS(sInteractableState)
	
	vCoordsToUse += NORMALISE_VECTOR(vPlayerStartPosOffset) * fPlayerOffsetMultiplier
	
	#IF IS_DEBUG_BUILD
	IF bInteractableDebug
	AND bInteractWithDebug
		DRAW_DEBUG_LINE(sInteractWithVars.vInteractWith_PlayerStartPos, vCoordsToUse, 255, 255, 255, 100 + ROUND(250 * GET_ANIMATED_SINE_VALUE(10.0)))
	ENDIF
	#ENDIF
	
	SET_INTERACTABLE_INTERACTION_COORDS(sInteractableState, vCoordsToUse)
	
	IF IS_INTERACT_WITH_PRESET_HEADING_DEPENDANT(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_InteractWithAnimPreset)
		sInteractableState.fInteractionHeading = sInteractWithVars.fInteractWith_RequiredHeading
	ELSE
		sInteractableState.fInteractionHeading = 999.9
	ENDIF
	
	IF SHOULD_INTERACTABLE_INTERACT_WITH_CHECK_PLAYER_SIDE(sInteractableState)
		// Check the player is on the correct side:
		VECTOR vPlayerCurrentOffset = (GET_ENTITY_COORDS(LocalPlayerPed) - GET_FMMC_INTERACTABLE_COORDS(sInteractableState))
		vPlayerCurrentOffset.z = vPlayerStartPosOffset.z
		
		FLOAT fDotProduct = DOT_PRODUCT(vPlayerCurrentOffset, vPlayerStartPosOffset)
		IF fDotProduct < 0.0
			#IF IS_DEBUG_BUILD
			IF bInteractableDebug
			AND bInteractWithDebug
				TEXT_LABEL_63 tlVisualDebug = "Wrong Side! fDotProduct: "
				tlVisualDebug += FLOAT_TO_STRING(fDotProduct)
				DRAW_DEBUG_TEXT_2D(tlVisualDebug, (<<0.1, 0.25, 0.5>>), 255, 0, 0, 255)
			ENDIF
			#ENDIF
		
			SET_BIT(iSharedInteractableBS, ciSHARED_INTERACTABLE_BS__INPUT_BLOCKED_BY_INTERACT_WITH_POSITION)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL PREPARE_INTERACTABLE_INTERACTION(FMMC_INTERACTABLE_STATE& sInteractableState, INTERACTION_VARS& sInteractionVars)

	BOOL bLoaded = FALSE
	SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_InteractionType
		CASE ciInteractableInteraction_InteractWith
			bLoaded = PROCESS_LOADING_INTERACTION__INTERACT_WITH_ANIM(sInteractableState)
			SET_INTERACTABLE_INTERACTION_COORDS_AND_HEADING_FROM_INTERACT_WITH_VARS(sInteractableState)
		BREAK
		CASE ciInteractableInteraction_SyncLock
			bLoaded = PROCESS_LOADING_INTERACTION__SYNC_LOCK(sInteractableState)
			SET_INTERACTABLE_INTERACTION_COORDS_AND_HEADING_FROM_INTERACT_WITH_VARS(sInteractableState)
		BREAK
		CASE ciInteractableInteraction_HackingMinigameA
			bLoaded = PROCESS_LOADING_INTERACTION__HACKING_MINIGAME_A(sInteractableState)
			SET_INTERACTABLE_INTERACTION_COORDS_AND_HEADING_FROM_INTERACT_WITH_VARS(sInteractableState)
		BREAK
		CASE ciInteractableInteraction_CutPainting
			bLoaded = PROCESS_LOADING_INTERACTION__CUT_PAINTING(sInteractableState)
			SET_INTERACTABLE_INTERACTION_COORDS_AND_HEADING_FROM_INTERACT_WITH_VARS(sInteractableState)
		BREAK
		CASE ciInteractableInteraction_MultiSolutionLock
			bLoaded = PROCESS_LOADING_INTERACTION__MULTI_SOLUTION_LOCK(sInteractableState)
			SET_INTERACTABLE_INTERACTION_COORDS_AND_HEADING_FROM_INTERACT_WITH_VARS(sInteractableState)
		BREAK
		CASE ciInteractableInteraction_SwitchOutfit
			bLoaded = PROCESS_LOADING_INTERACTION__SWITCH_OUTFIT(sInteractableState)
			SET_INTERACTABLE_INTERACTION_COORDS_AND_HEADING_FROM_INTERACT_WITH_VARS(sInteractableState)
		BREAK
		CASE ciInteractableInteraction_LootTray
			bLoaded = PROCESS_LOADING_INTERACTION__LOOT_TRAY(sInteractableState)
			SET_INTERACTABLE_INTERACTION_COORDS(sInteractableState, sInteractableState.vInteractionCoords)
		BREAK
		CASE ciInteractableInteraction_VoltageMinigame
			bLoaded = PROCESS_LOADING_INTERACTION__VOLTAGE_MINIGAME(sInteractableState)
			SET_INTERACTABLE_INTERACTION_COORDS_AND_HEADING_FROM_INTERACT_WITH_VARS(sInteractableState)
		BREAK
		CASE ciInteractableInteraction_FingerPrintMinigame
			bLoaded = PROCESS_LOADING_INTERACTION__FINGERPRINT_MINIGAME(sInteractableState)
			SET_INTERACTABLE_INTERACTION_COORDS_AND_HEADING_FROM_INTERACT_WITH_VARS(sInteractableState)
		BREAK
		
		CASE ciInteractableInteraction_LootVehicleTrunk
			bLoaded = PROCESS_LOADING_INTERACTION__LOOT_VEHICLE_TRUNK(sInteractableState, sInteractionVars)
			SET_INTERACTABLE_INTERACTION_COORDS_AND_HEADING_FROM_INTERACT_WITH_VARS(sInteractableState)
		BREAK
		
		CASE ciInteractableInteraction_PlantThermalCharge
			bLoaded = PROCESS_LOADING_INTERACTION__PLANT_THERMAL_CHARGE(sInteractableState)
			SET_INTERACTABLE_INTERACTION_COORDS_AND_HEADING_FROM_INTERACT_WITH_VARS(sInteractableState)
		BREAK
		
		CASE ciInteractableInteraction_None
			// No loading needed
			RETURN TRUE
		BREAK
		
		DEFAULT
			ASSERTLN("[Interactables][Interactable ", sInteractableState.iIndex, "] PREPARE_INTERACTABLE_INTERACTION | Interaction type ", g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_InteractionType, " is missing any form of preparation/loading!")
			#IF IS_DEBUG_BUILD
			ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_INTERACTABLE_HAS_NO_INTERACTION_TYPE_PREP, ENTITY_RUNTIME_ERROR_TYPE_CRITICAL_INTERACTABLE, "No Preparation set up for interaction type!", sInteractableState.iIndex)
			#ENDIF
		BREAK
	ENDSWITCH
	
	RETURN bLoaded
ENDFUNC

FUNC BOOL SHOULD_INTERACTABLE_USE_WAIT_FOR_TEAM_PROXIMITY_OBJECTIVE_TEXT(FMMC_INTERACTABLE_STATE& sInteractableState)
	
	IF IS_BIT_SET(sInteractableState.iBitset, FMMC_INTERACTABLE_STATE_OCCUPIED)
	AND GET_INTERACTABLE_CURRENT_USER(sInteractableState.iIndex) != iPartToUse
		// The Interactable is already being used
		RETURN TRUE
	ENDIF
		
	IF g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_RequirePlayerInVehicleIndex > -1
		VEHICLE_INDEX viRequiredVehicle = GET_FMMC_ENTITY_VEHICLE(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_RequirePlayerInVehicleIndex)
		
		IF IS_ENTITY_ALIVE(viRequiredVehicle)
		AND IS_VEHICLE_DRIVEABLE(viRequiredVehicle)
			IF IS_PED_IN_VEHICLE(LocalPlayerPed, viRequiredVehicle, FALSE)
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractableBS, ciInteractableBS_OnlyVehicleDriverCanInteract)
				AND GET_PED_IN_VEHICLE_SEAT(viRequiredVehicle, VS_DRIVER) != PlayerPedToUse
					// Player is waiting for the driver to do it!
					RETURN TRUE
				ENDIF
			ELSE
				// Player isn't in the required vehicle!
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_INTERACTABLE_PROXIMITY_OBJECTIVE_TEXT(FMMC_INTERACTABLE_STATE& sInteractableState)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_ProximityObjectiveTextIndex_Standard = -1
		// No objective text is set for this Interactable
		EXIT
	ENDIF
	
	// If this is an Interactable with an Interaction assigned to it, only display its objective text if that Interaction is available
	IF DOES_INTERACTABLE_HAVE_AN_INTERACTION_SET(sInteractableState.iIndex)
		IF NOT CAN_INTERACT_WITH_INTERACTABLE(sInteractableState, FALSE, FALSE)
			EXIT
		ENDIF
	ENDIF
	
	// If the player is close enough to the Interactable, set some data for the Objective Text system to use later to display a custom string
	IF sInteractableState.fDistanceToInteractable < POW(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].fInteractable_ProximityObjectiveTextDistance, 2.0)
		TEXT_LABEL_63 tlTxt = GET_CUSTOM_STRING_LIST_STRING(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_ProximityObjectiveTextIndex_Standard)
		
		IF SHOULD_INTERACTABLE_USE_WAIT_FOR_TEAM_PROXIMITY_OBJECTIVE_TEXT(sInteractableState)
			tlTxt = GET_CUSTOM_STRING_LIST_STRING(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_ProximityObjectiveTextIndex_WaitForTeam)
		ENDIF
		
		SET_INTERACTABLE_OBJECTIVE_TEXT_LITERAL(tlTxt)
	ENDIF
ENDPROC

PROC PROCESS_INTERACTABLE_EVERY_FRAME_BG_SCRIPT_VARS_CLIENT()

	IF IS_BIT_SET(gMC_LocalVariables_VARS.iLocalBGScriptBS, ciLocalBGScriptBS_BailCurrentInteractable)
		PRINTLN("[Interactable][Interactable ", GET_FOREGROUND_INTERACTABLE_INDEX(), "] PROCESS_INTERACTABLE_EVERY_FRAME_BG_SCRIPT_VARS_CLIENT | Bailing current interaction!")
		BAIL_FOREGROUND_INTERACTION_ASAP()
		CLEAR_BIT(gMC_LocalVariables_VARS.iLocalBGScriptBS, ciLocalBGScriptBS_BailCurrentInteractable)
	ENDIF
	
	IF IS_BIT_SET(gMC_LocalVariables_VARS.iLocalBGScriptBS, ciLocalBGScriptBS_CompleteCurrentInteractable)
		PRINTLN("[Interactable][Interactable ", GET_FOREGROUND_INTERACTABLE_INDEX(), "] PROCESS_INTERACTABLE_EVERY_FRAME_BG_SCRIPT_VARS_CLIENT | Completing current interaction and setting us up to bail (to stop any anims etc that may be currently happening)!")
		SET_INTERACTABLE_COMPLETE(GET_FOREGROUND_INTERACTABLE_INDEX())
		BAIL_FOREGROUND_INTERACTION_ASAP()
		CLEAR_BIT(gMC_LocalVariables_VARS.iLocalBGScriptBS, ciLocalBGScriptBS_CompleteCurrentInteractable)
	ENDIF
	
	IF gMC_LocalVariables_VARS.iResetThisInteractableAsap > -1
		PRINTLN("[Interactable][Interactable ", gMC_LocalVariables_VARS.iResetThisInteractableAsap, "] PROCESS_INTERACTABLE_EVERY_FRAME_BG_SCRIPT_VARS_CLIENT | Resetting Interactable ", gMC_LocalVariables_VARS.iResetThisInteractableAsap, "!")
		BROADCAST_FMMC_INTERACTABLE_EVENT(FMMC_INTERACTABLE_EVENT_TYPE__RESET_INTERACTABLE, gMC_LocalVariables_VARS.iResetThisInteractableAsap)
		gMC_LocalVariables_VARS.iResetThisInteractableAsap = -1
	ENDIF
ENDPROC

/// PURPOSE:
///    Processes the interaction itself after the player has initiated it by pressing d-pad right. This may be called multiple times in one frame if the player has multiple interactions happening at once.
/// PARAMS:
///    iInteractable - The index of the current interactable. Matches the g_FMMC_STRUCT_ENTITIES Interactable list
///    iInteractionIndex - The index of the ongoing interaction that we're processing here. Most of the time this'll just be 0, but if multiple interactions are ongoing in the background it could be something else.
PROC PROCESS_INTERACTABLE_INTERACTION_EVERY_FRAME_CLIENT(INT iInteractable, INT iInteractionIndex)
	
	FMMC_INTERACTABLE_STATE sInteractableState
	FILL_FMMC_INTERACTABLE_STATE_STRUCT(sInteractableState, iInteractable, iInteractionIndex)
		
	IF NOT sInteractableState.bInteractableExists
		PRINTLN("[Interactables][Interactable ", sInteractableState.iIndex, "] PROCESS_INTERACTABLE_INTERACTION_EVERY_FRAME_CLIENT | Interactable doesn't exist!")
		EXIT
	ENDIF
	
	IF NOT PREPARE_INTERACTABLE_INTERACTION(sInteractableState, sOngoingInteractionVars[iInteractionIndex])
		// Interaction still loading!
		PRINTLN("[Interactables][Interactable ", sInteractableState.iIndex, "] PROCESS_INTERACTABLE_INTERACTION_EVERY_FRAME_CLIENT | Interaction not ready yet!")
		EXIT
	ENDIF
	
	PROCESS_INTERACTABLE_PROXIMITY_OBJECTIVE_TEXT(sInteractableState)
	
	// The player is currently interacting with this!
	PROCESS_INTERACTION(sInteractableState, sOngoingInteractionVars[iInteractionIndex])
	
	#IF IS_DEBUG_BUILD
	PROCESS_INTERACTABLE_INTERACTION_DEBUG(sInteractableState)
	#ENDIF
	
ENDPROC

/// PURPOSE:
///    Handles the approach - checking the player's distance and current conditions to see if we should display the d-pad right prompt. If so, starts the interaction when the player presses d-pad right
/// PARAMS:
///    iInteractable - The index of the interactable we want to check. Matches the g_FMMC_STRUCT_ENTITIES Interactable list
PROC PROCESS_INTERACTABLE_APPROACH_EVERY_FRAME_CLIENT(INT iInteractable)
	
	IF iInteractable = -1
		EXIT
	ENDIF
	
	INT iInteractionIndex = MC_playerBD[iPartToUse].iOngoingInteractionCount
	IF iInteractionIndex >= ciINTERACTABLE_MAX_ONGOING_INTERACTIONS
		PRINTLN("[Interactables][Interactable ", iInteractable, "] PROCESS_INTERACTABLE_ONE_FRAME_LOOP_CLIENT | Not processing PROCESS_INTERACTABLE_ONE_FRAME_LOOP_CLIENT for interaction ", iInteractionIndex, " - too many interactions going on at once")
		EXIT
	ENDIF
	
	PROCESS_INTERACTABLE_EVERY_FRAME_BG_SCRIPT_VARS_CLIENT()

	FMMC_INTERACTABLE_STATE sInteractableState
	FILL_FMMC_INTERACTABLE_STATE_STRUCT(sInteractableState, iInteractable, iInteractionIndex)
	
	IF NOT sInteractableState.bInteractableExists
		PRINTLN("[Interactables][Interactable ", sInteractableState.iIndex, "] PROCESS_INTERACTABLE_ONE_FRAME_LOOP_CLIENT | Interactable doesn't exist!")
		EXIT
	ENDIF
	
	IF NOT PREPARE_INTERACTABLE_INTERACTION(sInteractableState, sOngoingInteractionVars[iInteractionIndex])
		// Interaction still loading!
		PRINTLN("[Interactables][Interactable ", sInteractableState.iIndex, "] PROCESS_INTERACTABLE_ONE_FRAME_LOOP_CLIENT | Interaction not ready yet!")
		EXIT
	ENDIF
	
	PROCESS_INTERACTABLE_PROXIMITY_OBJECTIVE_TEXT(sInteractableState)
	
	IF DOES_INTERACTABLE_HAVE_AN_INTERACTION_SET(sInteractableState.iIndex)	
		IF NOT IS_LOCAL_PLAYER_INTERACTING(iInteractable, FALSE)
			IF CAN_INTERACT_WITH_INTERACTABLE(sInteractableState, TRUE, TRUE, TRUE)
				// Wait for the d-pad right press
				PROCESS_INTERACTABLE_WAITING_FOR_INPUT(sInteractableState, sOngoingInteractionVars[iInteractionIndex])
			ENDIF
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	PROCESS_INTERACTABLE_APPROACH_DEBUG(sInteractableState)
	#ENDIF
ENDPROC

PROC PROCESS_INTERACTABLE_ONE_FRAME_LOOP_CLIENT(INT iInteractable)
	
	FMMC_INTERACTABLE_STATE sInteractableState
	FILL_FMMC_INTERACTABLE_STATE_STRUCT(sInteractableState, iInteractable)
		
	IF NOT sInteractableState.bInteractableExists
		PRINTLN("[Interactables][Interactable ", sInteractableState.iIndex, "] PROCESS_INTERACTABLE_ONE_FRAME_LOOP_CLIENT | Interactable doesn't exist!")
		EXIT
	ENDIF
	
	IF sInteractableState.bHaveControlOfInteractable
		// Invincibility on Rules
		IF g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_InvincibleRulesTeam > -1
		AND DOES_TEAM_HAVE_NEW_PRIORITY_THIS_FRAME(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_InvincibleRulesTeam)
			INT iRuleToCheck = GET_TEAM_CURRENT_RULE(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_InvincibleRulesTeam)
			
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[sInteractableState.iIndex].iInteractable_InvincibleRulesBS, iRuleToCheck)
				SET_INTERACTABLE_INVINCIBILITY(sInteractableState.objIndex, TRUE)
				PRINTLN("[Interactables][Interactable ", sInteractableState.iIndex, "] PROCESS_INTERACTABLE_ONE_FRAME_LOOP_CLIENT | Making Interactable invincible due to iInteractable_InvincibleRulesBS!")
			ELSE
				SET_INTERACTABLE_INVINCIBILITY(sInteractableState.objIndex, FALSE)
				PRINTLN("[Interactables][Interactable ", sInteractableState.iIndex, "] PROCESS_INTERACTABLE_ONE_FRAME_LOOP_CLIENT | Clearing Interactable invincibility due to iInteractable_InvincibleRulesBS!")
			ENDIF
		ENDIF
	ENDIF
		
	PROCESS_WARP_ON_RULE_DATA_FOR_ENTITY(CREATION_TYPE_INTERACTABLE, sInteractableState.iIndex, iInteractableShouldWarpThisRuleStart, iInteractableWarpedOnThisRule)
ENDPROC

PROC PROCESS_INTERACTABLE_ONE_FRAME_LOOP_SERVER(INT iInteractable)
	
	FMMC_INTERACTABLE_STATE sInteractableState
	FILL_FMMC_INTERACTABLE_STATE_STRUCT(sInteractableState, iInteractable)
	
	IF NOT sInteractableState.bInteractableExists
		IF FMMC_IS_LONG_BIT_SET(MC_serverBD.iInteractableCleanup_NeedOwnershipBS, iInteractable)
			FMMC_CLEAR_LONG_BIT(MC_serverBD.iInteractableCleanup_NeedOwnershipBS, iInteractable)
		ENDIF
	
		EXIT
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iInteractable_ResetCompletionOnRuleBS_Team > -1
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iInteractable_ResetCompletionOnRuleBS, GET_TEAM_CURRENT_RULE(g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[iInteractable].iInteractable_ResetCompletionOnRuleBS_Team))
			IF IS_INTERACTABLE_COMPLETE(iInteractable)
				PRINTLN("[Interactables][Interactable ", iInteractable, "] PROCESS_INTERACTABLE_INTERACTION_EVERY_FRAME_SERVER | Set Interactable incomplete again due to iInteractable_ResetCompletionOnRuleBS!")
				SET_INTERACTABLE_INCOMPLETE(iInteractable, FALSE)
			ENDIF
		ENDIF
	ENDIF
	
	IF FMMC_IS_LONG_BIT_SET(MC_serverBD.iInteractableCleanup_NeedOwnershipBS, iInteractable)
		IF sInteractableState.bHaveControlOfInteractable
			PRINTLN("[Interactables][Interactable ", iInteractable, "] PROCESS_INTERACTABLE_INTERACTION_EVERY_FRAME_SERVER | Cleaning up Interactable now!")
			DELETE_NET_ID(sInteractableState.niIndex)
			
		ELSE
			PRINTLN("[Interactables][Interactable ", iInteractable, "] PROCESS_INTERACTABLE_INTERACTION_EVERY_FRAME_SERVER | Requesting control of Interactable to clean it up!")
			NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(sInteractableState.niIndex)
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF bInteractableDebug
		TEXT_LABEL_63 tlVisualDebug = "Calling PROCESS_INTERACTABLE_INTERACTION_EVERY_FRAME_SERVER"
		DRAW_DEBUG_TEXT_2D(tlVisualDebug, (<<0.15, 0.25, 0.5>>), 255, 255, 255, 255)
	ENDIF
	#ENDIF
	
ENDPROC

PROC PROCESS_ONGOING_INTERACTABLE_INTERACTIONS_EVERY_FRAME_CLIENT()
	INT iOngoingInteraction
	FOR iOngoingInteraction = 0 TO MC_playerBD[iPartToUse].iOngoingInteractionCount - 1
		INT iInteractable = MC_playerBD[iPartToUse].iCurrentInteractables[iOngoingInteraction]
		IF iInteractable > -1
			PRINTLN("[Interactables_SPAM][Interactable_SPAM ", iInteractable, "] PROCESS_ONGOING_INTERACTABLE_INTERACTIONS | Processing Interaction ", iOngoingInteraction)
			PROCESS_INTERACTABLE_INTERACTION_EVERY_FRAME_CLIENT(iInteractable, iOngoingInteraction)
		ENDIF
	ENDFOR
ENDPROC

PROC PROCESS_INTERACTABLE_PRE_EVERY_FRAME_CLIENT()
	CLEAR_BIT(iSharedInteractableBS, ciSHARED_INTERACTABLE_BS__INPUT_BLOCKED_BY_INTERACT_WITH_POSITION)
	CLEAR_INTERACTABLE_OBJECTIVE_TEXT()
ENDPROC

PROC PROCESS_INTERACTABLE_AVAILABLE_INTERACTION_COUNT_POST_EVERY_FRAME()
	INT iInteractionCounter
	FOR iInteractionCounter = 0 TO ciInteractable_InteractionCounters - 1
		IF NOT IS_CUSTOM_STRING_LIST_STRING_VALID(g_FMMC_STRUCT_ENTITIES.sInteractionCounters[iInteractionCounter].iInteractionCounter_HUDStringIndex)
			RELOOP
		ENDIF
		
		TEXT_LABEL_63 tlCounterTitle = GET_CUSTOM_STRING_LIST_TEXT_LABEL(g_FMMC_STRUCT_ENTITIES.sInteractionCounters[iInteractionCounter].iInteractionCounter_HUDStringIndex)
		
		INT iAvailableInteractions = iInteractable_AvailableInteractionCount[iInteractionCounter]
		INT iTotalInteractions = iAvailableInteractions + iInteractable_CompletedInteractionCount[iInteractionCounter]
		
		IF iAvailableInteractions > 0
			DRAW_GENERIC_BIG_DOUBLE_NUMBER(iAvailableInteractions, iTotalInteractions, tlCounterTitle, DEFAULT, DEFAULT, HUDORDER_TOP)
		ENDIF
	ENDFOR
ENDPROC

PROC PROCESS_INTERACTABLE_POST_EVERY_FRAME_CLIENT()

	PROCESS_INTERACTABLE_AVAILABLE_INTERACTION_COUNT_POST_EVERY_FRAME()
	
	IF IS_BIT_SET(iSharedInteractableBS, ciSHARED_INTERACTABLE_BS__DISPLAYED_OBJECTIVE_TEXT_THIS_FRAME)
		// We're displaying Interactable objective text this frame
		SET_BIT(iSharedInteractableBS, ciSHARED_INTERACTABLE_BS__DISPLAYED_OBJECTIVE_TEXT_LAST_FRAME)
	ELSE
		// We're not displaying Interactable objective text this frame
		IF IS_BIT_SET(iSharedInteractableBS, ciSHARED_INTERACTABLE_BS__DISPLAYED_OBJECTIVE_TEXT_LAST_FRAME)
			// Clear the unneeded Interactable objective text here just in case
			PRINTLN("[Interactables] PROCESS_INTERACTABLE_POST_EVERY_FRAME_CLIENT | Calling Delete_MP_Objective_Text due to ciSHARED_INTERACTABLE_BS__DISPLAYED_OBJECTIVE_TEXT_LAST_FRAME")
			Delete_MP_Objective_Text()
			
			CLEAR_BIT(iSharedInteractableBS, ciSHARED_INTERACTABLE_BS__DISPLAYED_OBJECTIVE_TEXT_LAST_FRAME)
		ENDIF
	ENDIF
ENDPROC

