// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### System: GTA V Online Mission Controller - Vehicles ----------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Script: FM_Mission_Controller_2020.sc -----------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Contains both server & client functions used by placed vehicles. Handles vehicle objectives, blips, spawning and more.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### This script was forked from FM_Mission_Controller.sc at the beginning of 2020                                                                                       
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

USING "FM_Mission_Controller_EntityObjectives_2020.sch"

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Vehicle Objective Failure
// ##### Description: Functions for processing the failure of an objective/mission due to vehicles
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL SHOULD_VEHICLE_CAUSE_MISSION_FAILURE(INT iVeh, INT iTeam, INT iPriority)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetSix, ciFMMC_VEHICLE6_FAIL_AFTER_RESPAWNS)
	AND GET_VEHICLE_RESPAWNS(iVeh) > 0
		RETURN FALSE
	ENDIF
	
	IF iPriority < FMMC_MAX_RULES
	AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamFailBitset, iPriority)
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(MC_serverBD.iMissionCriticalVeh[iTeam], iVeh)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL CHECK_VEHICLE_FAILURE_FOR_TEAM(INT iVeh, INT iTeam, INT iPriority)
	
	IF NOT IS_BIT_SET(MC_serverBD.iVehTeamFailBitset[iVeh], iTeam)
		RETURN FALSE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iMissionCriticalRemovalRule[iTeam] > 0
	AND MC_serverBD_4.iCurrentHighestPriority[iTeam] >= g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iMissionCriticalRemovalRule[iTeam]
		PRINTLN("[END_CONDITIONS][VEHICLES] CHECK_VEHICLE_FAILURE_FOR_TEAM - Team: ", iTeam, ". Vehicle ", iVeh, " is no longer critical as they have reached rule ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iMissionCriticalRemovalRule[iTeam])
		CLEAR_BIT(MC_serverBD.iVehTeamFailBitset[iveh], iTeam)
		CLEAR_BIT(MC_serverBD.iMissionCriticalVeh[iTeam], iveh)
		RETURN FALSE
	ENDIF
		
	MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_VEH_DEAD
		
	INT iTeamLoop
	FOR iTeamLoop = 0 TO (MC_serverBD.iNumberOfTeams - 1)
		IF IS_BIT_SET(MC_serverBD.iVehTeamFailBitset[iVeh], SBBOOL1_TEAM0_NEEDS_KILL + iTeamLoop)
			IF DOES_TEAM_LIKE_TEAM(iTeam, iTeamLoop)
				PRINTLN("[END_CONDITIONS][VEHICLES] CHECK_VEHICLE_FAILURE_FOR_TEAM - Team: ", iTeamLoop, " needs to destroy veh ", iVeh, " clearing critical for team: ", iTeam)
				CLEAR_BIT(MC_serverBD.iVehTeamFailBitset[iVeh], iTeam)
				CLEAR_BIT(MC_serverBD.iMissionCriticalVeh[iTeam], iVeh)
				BREAKLOOP
			ENDIF
		ENDIF
	ENDFOR
	
	BOOL bFailMission = FALSE
	
	IF IS_BIT_SET(MC_serverBD.iVehTeamFailBitset[iVeh], SBBOOL1_TEAM0_NEEDS_KILL + iTeam)
		PRINTLN("[END_CONDITIONS][VEHICLES] CHECK_VEHICLE_FAILURE_FOR_TEAM - Team: ", iTeam, ". Vehicle ", iVeh, ". Destroying the vehicle was the team's objective.")
		SET_TEAM_OBJECTIVE_OUTCOME(iTeam, iPriority, TRUE)
		PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_VEHICLE, iVeh, iTeam, TRUE)
	ELSE
		IF SHOULD_VEHICLE_CAUSE_MISSION_FAILURE(iVeh, iTeam, iPriority)
			bFailMission = TRUE
			PRINTLN("[END_CONDITIONS][VEHICLES] CHECK_VEHICLE_FAILURE_FOR_TEAM - Team: ", iTeam, ". Vehicle ", iVeh, ". Mission Failure as this vehicle has been destroyed.")
			MC_serverBD.iEntityCausingFail[iTeam] = iVeh
			REQUEST_SET_TEAM_FAILED(iTeam, mFail_VEH_DEAD, TRUE, DEFAULT, iVeh)
		ELSE
			PRINTLN("[END_CONDITIONS][VEHICLES] CHECK_VEHICLE_FAILURE_FOR_TEAM - Team: ", iTeam, ". Vehicle ", iVeh, ". Objective Failure as this vehicle has been destroyed.")
			SET_TEAM_OBJECTIVE_OUTCOME(iTeam, iPriority, FALSE)
			PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_VEHICLE, iVeh, iTeam, FALSE)
		ENDIF
	ENDIF
	
	SET_LOCAL_OBJECTIVE_END_MESSAGE(iTeam, iPriority, MC_serverBD.iReasonForObjEnd[iTeam], bFailMission)
	BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[iTeam], iTeam, iPriority, DID_TEAM_PASS_OBJECTIVE(iTeam, iPriority), bFailMission)
	
	CLEAR_BIT(MC_serverBD.iVehTeamFailBitset[iVeh], iTeam)
	
	RETURN bFailMission
	
ENDFUNC

/// PURPOSE:
///    Called on the frame HAVE_MC_END_CONDITIONS_BEEN_MET returns true
///    to decide if the mission should fail due some vehicle condition/state
PROC PROCESS_FINAL_VEHICLE_FAILURE_CHECKS()
		
	INT iTeam
	BOOL bHaveAllTeamsFailed = TRUE
	
	FOR iTeam = 0 TO MC_serverBD.iNumberOfTeams - 1
		IF NOT HAS_TEAM_FAILED(iTeam)
			bHaveAllTeamsFailed = FALSE
			BREAKLOOP
		ENDIF
	ENDFOR
	
	IF bHaveAllTeamsFailed
		//All teams have failed already - no need to check vehicle fail conditions.
		EXIT
	ENDIF
	
	INT iVeh
	FOR iVeh = 0 TO MC_serverBD.iNumVehCreated - 1
		
		IF MC_serverBD.iVehTeamFailBitset[iVeh] = 0
			EXIT
		ENDIF
		
		IF IS_BIT_SET(MC_serverBD_1.sCreatedCount.iSkipVehBitset, iVeh)
			EXIT
		ENDIF
		
		IF GET_VEHICLE_RESPAWNS(iVeh) > 0
			EXIT
		ENDIF
			
		IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
			IF IS_VEHICLE_DRIVEABLE(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh]))
			OR NOT IS_TRAILER_STUCK(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh]))
				RELOOP
			ENDIF
		ENDIF
						
		PRINTLN("[END_CONDITIONS][VEHICLES] PROCESS_FINAL_VEHICLE_FAILURE_CHECKS - Checking Vehicle: ", iVeh)
						
		bHaveAllTeamsFailed = TRUE
		
		FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
			IF HAS_TEAM_FAILED(iTeam)
				RELOOP
			ENDIF
			
			IF NOT CHECK_VEHICLE_FAILURE_FOR_TEAM(iVeh, iTeam, MC_serverBD_4.iCurrentHighestPriority[iTeam])
				bHaveAllTeamsFailed = FALSE
			ENDIF
		ENDFOR
		
		IF bHaveAllTeamsFailed
			BREAKLOOP
		ENDIF
	
	ENDFOR
		
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Vehicle-in-Vehicle System
// ##### Description: Processing of vehicles that come out of other vehicles
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC MODEL_NAMES GET_CONTAINER_VEHICLE_MODEL_NAME(FMMC_VEHICLE_STATE &sVehState)
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehicleInVehicle_ContainerVehicleIndex > -1
		RETURN g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehicleInVehicle_ContainerVehicleIndex].mn
	ENDIF
	
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

FUNC VEHICLE_INDEX GET_CONTAINER_VEHICLE_TO_ATTACH_VEHICLE_TO(FMMC_VEHICLE_STATE &sVehState)
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehicleInVehicle_ContainerVehicleIndex = -1
		RETURN NULL
	ENDIF
	
	RETURN GET_FMMC_ENTITY_VEHICLE(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehicleInVehicle_ContainerVehicleIndex)
ENDFUNC

FUNC BOOL IS_VEHICLE_ATTACHED_TO_CREATOR_SPECIFIED_VEHICLE(FMMC_VEHICLE_STATE &sVehState)

	IF DOES_ENTITY_EXIST(GET_CONTAINER_VEHICLE_TO_ATTACH_VEHICLE_TO(sVehState))
		RETURN IS_ENTITY_ATTACHED_TO_ENTITY(sVehState.vehIndex, GET_CONTAINER_VEHICLE_TO_ATTACH_VEHICLE_TO(sVehState))
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_VEHICLE_EMERGE_FROM_CONTAINER_VEHICLE_NOW(FMMC_VEHICLE_STATE &sVehState)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehicleInVehicle_EmergeRule > -1
		IF HAVE_PLAYERS_PROGRESSED_TO_RULE(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehicleInVehicle_EmergeRule, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehicleInVehicle_EmergeTeam)
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehicleInVehicle_EmergePreReq > -1
		IF IS_PREREQUISITE_COMPLETED(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehicleInVehicle_EmergePreReq)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC DETACH_VEHICLE_INSIDE_ATTACHED_CONTAINER_VEHICLE(FMMC_VEHICLE_STATE &sVehState)
	IF sVehState.bHasControl
		IF IS_VEHICLE_ATTACHED_TO_CREATOR_SPECIFIED_VEHICLE(sVehState)
			PRINTLN("[VehicleInVehicle][Vehicle ", sVehState.iIndex, "] DETACH_VEHICLE_INSIDE_ATTACHED_CONTAINER_VEHICLE | Detaching the vehicle now!")
			DETACH_ENTITY(sVehState.vehIndex, FALSE, TRUE)
			
			SET_ENTITY_INVINCIBLE(sVehState.vehIndex, FALSE)
			SET_ENTITY_DYNAMIC(sVehState.vehIndex, TRUE)
			SET_ENTITY_HAS_GRAVITY(sVehState.vehIndex, TRUE)
			
			SET_ENTITY_VELOCITY(sVehState.vehIndex, <<0,0,0>>)
			SET_ENTITY_ANGULAR_VELOCITY(sVehState.vehIndex, <<0,0,0>>)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL PREPARE_VEHICLE_FOR_CONTAINER_VEHICLE(FMMC_VEHICLE_STATE &sVehState, VEHICLE_INDEX viContainerVehicle)
	IF sVehState.bHasControl
		
		PRINTLN("[VehicleInVehicle][Vehicle ", sVehState.iIndex, "] PREPARE_VEHICLE_FOR_CONTAINER_VEHICLE | Preparing vehicle for going into the container vehicle!")
		
		// Ensure that the contained vehicle won't damage the Container Vehicle when landing on it/emerging from it and also the other way around
		SET_ENTITY_CANT_CAUSE_COLLISION_DAMAGED_ENTITY(viContainerVehicle, sVehState.vehIndex)
		SET_ENTITY_CANT_CAUSE_COLLISION_DAMAGED_ENTITY(sVehState.vehIndex, viContainerVehicle)
		
		IF IS_ENTITY_ATTACHED(viContainerVehicle)
			// The container is already connected to something - ensure that the contained vehicle won't damage this other attached entity as well
			SET_ENTITY_CANT_CAUSE_COLLISION_DAMAGED_ENTITY(GET_ENTITY_ATTACHED_TO(viContainerVehicle), sVehState.vehIndex)
			SET_ENTITY_CANT_CAUSE_COLLISION_DAMAGED_ENTITY(sVehState.vehIndex, GET_ENTITY_ATTACHED_TO(viContainerVehicle))
		ENDIF
		
		// Make the contained vehicle invincible until it emerges later
		SET_ENTITY_INVINCIBLE(sVehState.vehIndex, TRUE)
		SET_ENTITY_DYNAMIC(sVehState.vehIndex, FALSE)
		SET_ENTITY_HAS_GRAVITY(sVehState.vehIndex, FALSE)
		
		RETURN TRUE
	ENDIF	
	
	RETURN FALSE
ENDFUNC

FUNC VECTOR GET_VEHICLE_IN_VEHICLE_ATTACH_OFFSET(FMMC_VEHICLE_STATE &sVehState)
	SWITCH GET_CONTAINER_VEHICLE_MODEL_NAME(sVehState)
		CASE TRAILERLARGE
			RETURN <<0.0, -3.0, -0.370>>
		BREAK
		CASE TITAN
		CASE BOMBUSHKA
			RETURN <<0.0, -4.0, -0.0>>
		BREAK
	ENDSWITCH
	
	RETURN <<0,0,0>>
ENDFUNC

FUNC VECTOR GET_VEHICLE_IN_VEHICLE_ATTACH_ROTATION(FMMC_VEHICLE_STATE &sVehState)
	SWITCH GET_CONTAINER_VEHICLE_MODEL_NAME(sVehState)
		CASE TRAILERLARGE
			RETURN <<0.0, 0.0, 0.0>>
		BREAK
		CASE TITAN
		CASE BOMBUSHKA
			RETURN <<0.0, 0.0, 180.0>>
		BREAK
	ENDSWITCH
	
	RETURN <<0,0,0>>
ENDFUNC

FUNC VECTOR GET_VEHICLE_IN_VEHICLE_BASE_PUSH_AWAY_FORCE(FMMC_VEHICLE_STATE &sVehState)
	SWITCH GET_CONTAINER_VEHICLE_MODEL_NAME(sVehState)
		CASE TRAILERLARGE
			RETURN <<0.0, -2.0, 0.15>>
		BREAK
		CASE TITAN
		CASE BOMBUSHKA
			RETURN  <<0.0, 1.25, 0.0>>
		BREAK
	ENDSWITCH
	
	RETURN <<0,0,0>>
ENDFUNC

FUNC FLOAT GET_VEHICLE_IN_VEHICLE_EMERGE_DOOR_RATIO(FMMC_VEHICLE_STATE &sVehState)
	SWITCH GET_CONTAINER_VEHICLE_MODEL_NAME(sVehState)
		CASE TRAILERLARGE
			RETURN 0.5
		BREAK
		CASE TITAN
		CASE BOMBUSHKA
			RETURN 0.1
		BREAK
	ENDSWITCH
	
	RETURN 0.45
ENDFUNC

FUNC BOOL SHOULD_VEHICLE_IN_VEHICLE_DETACH_IMMEDIATELY(FMMC_VEHICLE_STATE &sVehState)
	SWITCH GET_CONTAINER_VEHICLE_MODEL_NAME(sVehState)
		CASE TRAILERLARGE
			RETURN FALSE
		BREAK
		CASE TITAN
		CASE BOMBUSHKA
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_VEHICLE_IN_VEHICLE_CONTAINER_DOOR_CLOSE_AFTER_EMERGE(FMMC_VEHICLE_STATE &sVehState)
	SWITCH GET_CONTAINER_VEHICLE_MODEL_NAME(sVehState)
		CASE TRAILERLARGE
			RETURN TRUE
		BREAK
		CASE TITAN
		CASE BOMBUSHKA
			RETURN FALSE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL PLACE_VEHICLE_INTO_SPECIFIED_VEHICLE(FMMC_VEHICLE_STATE &sVehState, VEHICLE_INDEX viContainerVehicle)
	IF sVehState.bHasControl
		IF NOT IS_VEHICLE_ATTACHED_TO_CREATOR_SPECIFIED_VEHICLE(sVehState)

			PRINTLN("[VehicleInVehicle][Vehicle ", sVehState.iIndex, "] PLACE_VEHICLE_INTO_SPECIFIED_VEHICLE | Attaching vehicle inside the Container Vehicle now!")
			
			VECTOR vContainerVehicleAttachOffset = GET_VEHICLE_IN_VEHICLE_ATTACH_OFFSET(sVehState)
			ATTACH_ENTITY_TO_ENTITY(sVehState.vehIndex, viContainerVehicle, 0, vContainerVehicleAttachOffset, GET_VEHICLE_IN_VEHICLE_ATTACH_ROTATION(sVehState), FALSE, FALSE)
			
			RETURN TRUE
		ENDIF
	ENDIF	
	
	RETURN FALSE
ENDFUNC

FUNC BOOL NEED_TO_PUSH_VEHICLE_OUT_OF_CONTAINER_VEHICLE(FMMC_VEHICLE_STATE &sVehState, VEHICLE_INDEX viContainerVehicle)
	RETURN MC_IS_ENTITY_TOUCHING_ENTITY(sVehState.vehIndex, viContainerVehicle, 0.0)
ENDFUNC

PROC PUSH_VEHICLE_OUT_OF_CONTAINER_VEHICLE(FMMC_VEHICLE_STATE &sVehState, VECTOR vForceToUse)
	// Contained vehicle is still in contact with the Container Vehicle! Apply more force to get it out
	APPLY_FORCE_TO_ENTITY(sVehState.vehIndex, APPLY_TYPE_IMPULSE, vForceToUse, (<<0,0,0>>), 0, TRUE, FALSE, TRUE)
ENDPROC

#IF IS_DEBUG_BUILD
FUNC STRING GET_VEHICLE_IN_VEHICLE_EMERGE_STATE_NAME(INT iVehicleInVehicleStateToCheck)
	SWITCH iVehicleInVehicleStateToCheck
		CASE ciVEHICLE_IN_VEHICLE_STATE__INACTIVE												RETURN "INACTIVE"			
		CASE ciVEHICLE_IN_VEHICLE_STATE__PREPARING												RETURN "PREPARING"				
		CASE ciVEHICLE_IN_VEHICLE_STATE__ATTACHING												RETURN "ATTACHING"			
		CASE ciVEHICLE_IN_VEHICLE_STATE__WAITING_FOR_PEDS										RETURN "WAITING_FOR_PEDS"			
		CASE ciVEHICLE_IN_VEHICLE_STATE__WAITING_TO_EMERGE										RETURN "WAITING_TO_EMERGE"	
		CASE ciVEHICLE_IN_VEHICLE_STATE__GET_CONTAINER_VEHICLE_READY_FOR_VEHICLE_DETACH			RETURN "GET_CONTAINER_VEHICLE_READY_FOR_VEHICLE_DETACH"	
		CASE ciVEHICLE_IN_VEHICLE_STATE__DETACH_AND_OPEN_CONTAINER_VEHICLE_WHEN_READY			RETURN "DETACH_AND_OPEN_CONTAINER_VEHICLE_WHEN_READY"	
		CASE ciVEHICLE_IN_VEHICLE_STATE__EMERGE_WHEN_READY										RETURN "EMERGE_WHEN_READY"	
		CASE ciVEHICLE_IN_VEHICLE_STATE__EMERGING												RETURN "EMERGING"	
		CASE ciVEHICLE_IN_VEHICLE_STATE__WAITING_TO_CLOSE_DOOR									RETURN "WAITING_TO_CLOSE_DOOR"	
		CASE ciVEHICLE_IN_VEHICLE_STATE__EMERGE_ENDING											RETURN "EMERGE_ENDING"
		CASE ciVEHICLE_IN_VEHICLE_STATE__EMERGE_COMPLETE										RETURN "EMERGE_COMPLETE"		
	ENDSWITCH
	
	RETURN "INVALID"
ENDFUNC
#ENDIF

PROC PROCESS_VEHICLE_IN_VEHICLE_FUNCTIONALITY(FMMC_VEHICLE_STATE &sVehState)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehicleInVehicle_ContainerVehicleIndex = -1
		EXIT
	ENDIF
	
	VEHICLE_INDEX viContainerVehicle = GET_CONTAINER_VEHICLE_TO_ATTACH_VEHICLE_TO(sVehState)
	
	IF NOT sVehState.bExists
	OR NOT DOES_ENTITY_EXIST(viContainerVehicle)
	
		IF NOT sVehState.bExists
			PRINTLN("[VehicleInVehicle] PROCESS_VEHICLE_IN_VEHICLE_FUNCTIONALITY | Vehicle-in-Vehicle doesn't exist!")
		ENDIF
		IF NOT DOES_ENTITY_EXIST(viContainerVehicle)
			PRINTLN("[VehicleInVehicle] PROCESS_VEHICLE_IN_VEHICLE_FUNCTIONALITY | Container Vehicle doesn't exist!")
		ENDIF
		
		IF IS_BIT_SET(iLocalBoolCheck32, LBOOL32_LOCKED_MIGRATION_FOR_VEHICLE_IN_VEHICLE)
			PRINTLN("[VehicleInVehicle] PROCESS_VEHICLE_IN_VEHICLE_FUNCTIONALITY | Removing migration locks (failsafe)!")
			
			IF sVehState.bExists
				SET_NETWORK_ID_CAN_MIGRATE(NETWORK_GET_NETWORK_ID_FROM_ENTITY(sVehState.vehIndex), TRUE)
			ENDIF
			IF DOES_ENTITY_EXIST(viContainerVehicle)
				SET_NETWORK_ID_CAN_MIGRATE(NETWORK_GET_NETWORK_ID_FROM_ENTITY(viContainerVehicle), TRUE)
			ENDIF
			
			CLEAR_BIT(iLocalBoolCheck32, LBOOL32_LOCKED_MIGRATION_FOR_VEHICLE_IN_VEHICLE)
		ENDIF
		
		EXIT
	ENDIF
	
	BOOL bControlsContainerVehicle = NETWORK_HAS_CONTROL_OF_ENTITY(viContainerVehicle)
	INT iPart = -1
	
	// Scaling the force
	VECTOR vPushPlayerAwayForce = <<0.0, -5.5, 0.15>>
	VECTOR vPushEmergingVehicleAwayForce = GET_VEHICLE_IN_VEHICLE_BASE_PUSH_AWAY_FORCE(sVehState)
	vPushEmergingVehicleAwayForce.y *= g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].fVehicleInVehicle_EmergeForceMultiplier
	
	SWITCH iVehicleInVehicleState
		CASE ciVEHICLE_IN_VEHICLE_STATE__INACTIVE
			
		BREAK
		
		CASE ciVEHICLE_IN_VEHICLE_STATE__PREPARING
			IF bControlsContainerVehicle
				IF sVehState.bHasControl
					IF PREPARE_VEHICLE_FOR_CONTAINER_VEHICLE(sVehState, viContainerVehicle)
						BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_VehicleInVehicle_StateChange, sVehState.iIndex, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, ciVEHICLE_IN_VEHICLE_STATE__ATTACHING)
					ENDIF
				ELSE
					NETWORK_REQUEST_CONTROL_OF_ENTITY(sVehState.vehIndex)
					PRINTLN("[VehicleInVehicle] PROCESS_VEHICLE_IN_VEHICLE_FUNCTIONALITY | Getting control of Emerging Vehicle before attaching!")
				ENDIF
			ENDIF
		BREAK
		
		CASE ciVEHICLE_IN_VEHICLE_STATE__WAITING_FOR_PEDS
			IF sVehState.bHasControl
				IF iVehicleInVehicleEmergePeds = 0
					PRINTLN("[VehicleInVehicle][Vehicle ", sVehState.iIndex, "] PROCESS_VEHICLE_IN_VEHICLE_FUNCTIONALITY | No peds to wait for!")
					BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_VehicleInVehicle_StateChange, sVehState.iIndex, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, ciVEHICLE_IN_VEHICLE_STATE__WAITING_TO_EMERGE)
					
				ELIF NOT IS_VEHICLE_SEAT_FREE(sVehState.vehIndex, VS_DRIVER)
				AND GET_VEHICLE_NUMBER_OF_PASSENGERS(sVehState.vehIndex) >= (iVehicleInVehicleEmergePeds - 1)
					PRINTLN("[VehicleInVehicle][Vehicle ", sVehState.iIndex, "] PROCESS_VEHICLE_IN_VEHICLE_FUNCTIONALITY | Emerging Vehicle peds ready to go!")
					BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_VehicleInVehicle_StateChange, sVehState.iIndex, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, ciVEHICLE_IN_VEHICLE_STATE__WAITING_TO_EMERGE)
					
				ELSE
					PRINTLN("[VehicleInVehicle][Vehicle ", sVehState.iIndex, "] PROCESS_VEHICLE_IN_VEHICLE_FUNCTIONALITY | Waiting for peds to spawn! ", GET_VEHICLE_NUMBER_OF_PASSENGERS(sVehState.vehIndex), " / ", iVehicleInVehicleEmergePeds)
					
				ENDIF
			ENDIF
		BREAK
		
		CASE ciVEHICLE_IN_VEHICLE_STATE__ATTACHING
			IF bControlsContainerVehicle
				IF sVehState.bHasControl
					IF NOT IS_VEHICLE_ATTACHED_TO_CREATOR_SPECIFIED_VEHICLE(sVehState)
						IF PLACE_VEHICLE_INTO_SPECIFIED_VEHICLE(sVehState, viContainerVehicle)
							BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_VehicleInVehicle_StateChange, sVehState.iIndex, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, ciVEHICLE_IN_VEHICLE_STATE__WAITING_FOR_PEDS)
						ENDIF
					ENDIF
				ELSE
					NETWORK_REQUEST_CONTROL_OF_ENTITY(sVehState.vehIndex)
					PRINTLN("[VehicleInVehicle] PROCESS_VEHICLE_IN_VEHICLE_FUNCTIONALITY | Getting control of Emerging Vehicle before attaching!")
				ENDIF
			ENDIF
		BREAK
		
		CASE ciVEHICLE_IN_VEHICLE_STATE__WAITING_TO_EMERGE
			IF sVehState.bHasControl
				IF SHOULD_VEHICLE_EMERGE_FROM_CONTAINER_VEHICLE_NOW(sVehState)
					IF RUN_TIMER(tdVehicleExitingVehicleStateTimer, ciVehicleExitingVehicleStateTimer_UnlockDoorDelayLength)
						RESET_NET_TIMER(tdVehicleExitingVehicleStateTimer)
						
						IF SHOULD_VEHICLE_IN_VEHICLE_DETACH_IMMEDIATELY(sVehState)
							DETACH_VEHICLE_INSIDE_ATTACHED_CONTAINER_VEHICLE(sVehState)
						ENDIF
						
						BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_VehicleInVehicle_StateChange, sVehState.iIndex, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, ciVEHICLE_IN_VEHICLE_STATE__GET_CONTAINER_VEHICLE_READY_FOR_VEHICLE_DETACH)
					ELSE
						PRINTLN("[VehicleInVehicle] PROCESS_VEHICLE_IN_VEHICLE_FUNCTIONALITY | Waiting for the emerge delay to expire!")
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE ciVEHICLE_IN_VEHICLE_STATE__GET_CONTAINER_VEHICLE_READY_FOR_VEHICLE_DETACH
			IF bControlsContainerVehicle
				PRINTLN("[VehicleInVehicle] PROCESS_VEHICLE_IN_VEHICLE_FUNCTIONALITY | Setting Container Vehicle temporarily only damageable by players!")
				SET_ENTITY_CAN_ONLY_BE_DAMAGED_BY_SCRIPT_PARTICIPANTS(viContainerVehicle, TRUE)
				
				BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_VehicleInVehicle_StateChange, sVehState.iIndex, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, ciVEHICLE_IN_VEHICLE_STATE__DETACH_AND_OPEN_CONTAINER_VEHICLE_WHEN_READY)
			ENDIF
		BREAK
		
		CASE ciVEHICLE_IN_VEHICLE_STATE__DETACH_AND_OPEN_CONTAINER_VEHICLE_WHEN_READY
			IF bControlsContainerVehicle
				IF sVehState.bHasControl
					// The player who has control of the Container Vehicle sets the back door to swing open
					SET_VEHICLE_DOOR_OPEN(viContainerVehicle, SC_DOOR_BOOT, TRUE, FALSE)
					PRINTLN("[VehicleInVehicle] PROCESS_VEHICLE_IN_VEHICLE_FUNCTIONALITY | Opening Container Vehicle boot!")
					
					// Wait for the doors to be about half-open
					IF GET_VEHICLE_DOOR_ANGLE_RATIO(viContainerVehicle, SC_DOOR_BOOT) >= GET_VEHICLE_IN_VEHICLE_EMERGE_DOOR_RATIO(sVehState)
						IF RUN_TIMER(tdVehicleExitingVehicleStateTimer, ciVehicleExitingVehicleStateTimer_EmergeDelayLength)
							RESET_NET_TIMER(tdVehicleExitingVehicleStateTimer)
							
							SET_NETWORK_ID_CAN_MIGRATE(NETWORK_GET_NETWORK_ID_FROM_ENTITY(sVehState.vehIndex), FALSE)
							SET_NETWORK_ID_CAN_BE_REASSIGNED(NETWORK_GET_NETWORK_ID_FROM_ENTITY(sVehState.vehIndex), TRUE) // Allows control to change if this player quits, just in case
							
							SET_NETWORK_ID_CAN_MIGRATE(NETWORK_GET_NETWORK_ID_FROM_ENTITY(viContainerVehicle), FALSE)
							SET_NETWORK_ID_CAN_BE_REASSIGNED(NETWORK_GET_NETWORK_ID_FROM_ENTITY(viContainerVehicle), TRUE) // Allows control to change if this player quits, just in case
							
							SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(sVehState.vehIndex, FALSE)
							SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(sVehState.vehIndex, TRUE)
							SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(sVehState.vehIndex, TRUE)
							SET_VEHICLE_RESPECTS_LOCKS_WHEN_HAS_DRIVER(sVehState.vehIndex, TRUE)
							
							SET_BIT(iLocalBoolCheck32, LBOOL32_LOCKED_MIGRATION_FOR_VEHICLE_IN_VEHICLE)
								
							PRINTLN("[VehicleInVehicle] PROCESS_VEHICLE_IN_VEHICLE_FUNCTIONALITY | Locking down network control of these vehicles & broadcasting state change to ciVEHICLE_IN_VEHICLE_STATE__EMERGE_WHEN_READY!")
							BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_VehicleInVehicle_StateChange, sVehState.iIndex, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, ciVEHICLE_IN_VEHICLE_STATE__EMERGE_WHEN_READY)
						ELSE
							PRINTLN("[VehicleInVehicle] PROCESS_VEHICLE_IN_VEHICLE_FUNCTIONALITY | Door is open, just waiting for delay timer to allow time for the door to sync before detaching!")
							
						ENDIF
					ELSE
						PRINTLN("[VehicleInVehicle] PROCESS_VEHICLE_IN_VEHICLE_FUNCTIONALITY | Waiting for container vehicle door to open a bit more before detaching!")
						
					ENDIF
					
				ELSE
					NETWORK_REQUEST_CONTROL_OF_ENTITY(sVehState.vehIndex)
					PRINTLN("[VehicleInVehicle] PROCESS_VEHICLE_IN_VEHICLE_FUNCTIONALITY | Requesting control of contained vehicle!")
				ENDIF
			ELSE
				PRINTLN("[VehicleInVehicle] PROCESS_VEHICLE_IN_VEHICLE_FUNCTIONALITY | Local player doesn't control container vehicle")
				
			ENDIF			
		BREAK
		
		CASE ciVEHICLE_IN_VEHICLE_STATE__EMERGE_WHEN_READY
			IF sVehState.bHasControl
				// Wait for the doors to be quite far open
				IF GET_VEHICLE_DOOR_ANGLE_RATIO(viContainerVehicle, SC_DOOR_BOOT) >= GET_VEHICLE_IN_VEHICLE_EMERGE_DOOR_RATIO(sVehState) * 1.25
				OR RUN_TIMER(tdVehicleExitingVehicleStateTimer, ciVehicleExitingVehicleStateTimer_IgnoreDoorOpenCheckLength)
					IF IS_VEHICLE_ON_ALL_WHEELS(viContainerVehicle)
					OR RUN_TIMER(tdVehicleExitingVehicleStateTimer, ciVehicleExitingVehicleStateTimer_IgnoreAllWheelsCheckLength)
						IF IS_ENTITY_ATTACHED(sVehState.vehIndex)
							DETACH_VEHICLE_INSIDE_ATTACHED_CONTAINER_VEHICLE(sVehState)
						ENDIF
						
						RESET_NET_TIMER(tdVehicleExitingVehicleStateTimer)
						RESET_NET_TIMER(tdVehicleExitingVehicleClearVelocityTimer)
						REINIT_NET_TIMER(tdVehicleExitingVehicleReleaseControlFailsafeTimer)
						
						BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_VehicleInVehicle_StateChange, sVehState.iIndex, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, ciVEHICLE_IN_VEHICLE_STATE__EMERGING)
					ELSE
						PRINTLN("[VehicleInVehicle] PROCESS_VEHICLE_IN_VEHICLE_FUNCTIONALITY | Waiting for Container vehicle to be on all wheels before emerging!")
						
					ENDIF
				ELSE
					PRINTLN("[VehicleInVehicle] PROCESS_VEHICLE_IN_VEHICLE_FUNCTIONALITY | Waiting for Container vehicle door to open a bit more before emerging!")
					
				ENDIF
				
				SET_ENTITY_VELOCITY(sVehState.vehIndex, <<0,0,0>>)
				SET_ENTITY_ANGULAR_VELOCITY(sVehState.vehIndex, <<0,0,0>>)
			ENDIF
		BREAK
		
		CASE ciVEHICLE_IN_VEHICLE_STATE__EMERGING
			IF sVehState.bHasControl
				IF NOT RUN_TIMER(tdVehicleExitingVehicleClearVelocityTimer, ciVehicleExitingVehicleClearVelocityTimer_Length)
					// Keep control of the Emerging Vehicle's angular velocity for a brief period to prevent somersaulting
					IF IS_ENTITY_IN_AIR(sVehState.vehIndex)
						SET_ENTITY_ANGULAR_VELOCITY(sVehState.vehIndex, <<0.5, 0, 0>>)
						PRINTLN("[VehicleInVehicle] PROCESS_VEHICLE_IN_VEHICLE_FUNCTIONALITY | Controlling Emerging Vehicle's angular velocity this frame!")
					ENDIF
				ENDIF
					
				IF NEED_TO_PUSH_VEHICLE_OUT_OF_CONTAINER_VEHICLE(sVehState, viContainerVehicle)
					PUSH_VEHICLE_OUT_OF_CONTAINER_VEHICLE(sVehState, vPushEmergingVehicleAwayForce)
					PRINTLN("[VehicleInVehicle] PROCESS_VEHICLE_IN_VEHICLE_FUNCTIONALITY | Pushing out Emerging Vehicle this frame! vPushEmergingVehicleAwayForce: ", vPushEmergingVehicleAwayForce)
					
				ELSE
					IF SHOULD_VEHICLE_IN_VEHICLE_CONTAINER_DOOR_CLOSE_AFTER_EMERGE(sVehState)
						BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_VehicleInVehicle_StateChange, sVehState.iIndex, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, ciVEHICLE_IN_VEHICLE_STATE__WAITING_TO_CLOSE_DOOR)
					ELSE
						BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_VehicleInVehicle_StateChange, sVehState.iIndex, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, ciVEHICLE_IN_VEHICLE_STATE__EMERGE_ENDING)
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE ciVEHICLE_IN_VEHICLE_STATE__WAITING_TO_CLOSE_DOOR
			
			BOOL bCanCloseDoorNow
			bCanCloseDoorNow = TRUE
			
			WHILE DO_PARTICIPANT_LOOP(iPart, DPLF_CHECK_PED_ALIVE | DPLF_FILL_PLAYER_IDS)
				IF NOT DOES_ENTITY_EXIST(piParticipantLoop_PedIndex)
					RELOOP
				ENDIF
				
				VECTOR vObjectSpaceOffsetFromContainerVehicle
				vObjectSpaceOffsetFromContainerVehicle = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(viContainerVehicle, GET_ENTITY_COORDS(piParticipantLoop_PedIndex))
				
				IF vObjectSpaceOffsetFromContainerVehicle.z > -1.0
				OR ABSF(vObjectSpaceOffsetFromContainerVehicle.x) > 2.0
					// This player couldn't be touching a restricted part of the Container Vehicle!
					RELOOP
				ENDIF

				IF IS_PED_IN_ANY_VEHICLE(piParticipantLoop_PedIndex)
					VEHICLE_INDEX viPlayerVeh
					viPlayerVeh = GET_VEHICLE_PED_IS_USING(piParticipantLoop_PedIndex)
					
					IF MC_IS_ENTITY_TOUCHING_ENTITY(viPlayerVeh, viContainerVehicle)
						// This participant's vehicle is in contact with the Container Vehicle!
						IF GET_VEHICLE_DOOR_ANGLE_RATIO(viContainerVehicle, SC_DOOR_BOOT) > 0.25 // Only bail on closing the door if it had only just started closing, not if it's nearly shut
							bCanCloseDoorNow = FALSE
						ENDIF
						
						IF piParticipantLoop_PedIndex = LocalPlayerPed
							APPLY_FORCE_TO_ENTITY(viPlayerVeh, APPLY_TYPE_IMPULSE, vPushPlayerAwayForce, (<<0,0,0>>), 0, TRUE, FALSE, TRUE)
						ENDIF
						
						PRINTLN("[VehicleInVehicle] PROCESS_VEHICLE_IN_VEHICLE_FUNCTIONALITY | Participant ", iPart, "'s vehicle is getting pushed away! Can't close door until they stop colliding")
					ENDIF
				ELSE
					IF MC_IS_ENTITY_TOUCHING_ENTITY(piParticipantLoop_PedIndex, viContainerVehicle)
						// This player is in contact with the Container Vehicle!
						IF GET_VEHICLE_DOOR_ANGLE_RATIO(viContainerVehicle, SC_DOOR_BOOT) > 0.25 // Only bail on closing the door if it had only just started closing, not if it's nearly shut
							bCanCloseDoorNow = FALSE
						ENDIF
						
						IF piParticipantLoop_PedIndex = LocalPlayerPed
							APPLY_FORCE_TO_ENTITY(piParticipantLoop_PedIndex, APPLY_TYPE_IMPULSE, vPushPlayerAwayForce, (<<0,0,0>>), 0, TRUE, FALSE, TRUE)
						ENDIF
						
						PRINTLN("[VehicleInVehicle] PROCESS_VEHICLE_IN_VEHICLE_FUNCTIONALITY | Participant ", iPart, " is getting pushed away! Can't close door until they stop colliding")
					ENDIF
				ENDIF
			ENDWHILE
			
			IF bControlsContainerVehicle
				IF sVehState.bHasControl
					
					IF NEED_TO_PUSH_VEHICLE_OUT_OF_CONTAINER_VEHICLE(sVehState, viContainerVehicle)
						PUSH_VEHICLE_OUT_OF_CONTAINER_VEHICLE(sVehState, vPushEmergingVehicleAwayForce)
						bCanCloseDoorNow = FALSE
						PRINTLN("[VehicleInVehicle] PROCESS_VEHICLE_IN_VEHICLE_FUNCTIONALITY | Can't close door until contained vehicle stops colliding!")
					ENDIF
					
					IF bCanCloseDoorNow 
						IF RUN_TIMER(tdVehicleExitingVehicleStateTimer, ciVehicleExitingVehicleStateTimer_CloseDoorLength)
							
							FLOAT fNewOpenRatio
							fNewOpenRatio = GET_VEHICLE_DOOR_ANGLE_RATIO(viContainerVehicle, SC_DOOR_BOOT) - (cfVehicleExitingVehicleDoorCloseSpeed * GET_FRAME_TIME())
							
							PRINTLN("[VehicleInVehicle] PROCESS_VEHICLE_IN_VEHICLE_FUNCTIONALITY | fNewOpenRatio is ", fNewOpenRatio)
							SET_VEHICLE_DOOR_CONTROL(viContainerVehicle, SC_DOOR_BOOT, DT_DOOR_INTACT, fNewOpenRatio)
							
							// Moving on when the door is shut
							IF fNewOpenRatio < 0.025
								SET_VEHICLE_DOOR_SHUT(viContainerVehicle, SC_DOOR_BOOT, TRUE)
								SET_VEHICLE_INDIVIDUAL_DOORS_LOCKED(viContainerVehicle, ENUM_TO_INT(SC_DOOR_BOOT), VEHICLELOCK_LOCKED)
								BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_VehicleInVehicle_StateChange, sVehState.iIndex, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, ciVEHICLE_IN_VEHICLE_STATE__EMERGE_ENDING)
							ENDIF
						ELSE
							PRINTLN("[VehicleInVehicle] PROCESS_VEHICLE_IN_VEHICLE_FUNCTIONALITY | Timer is running!")
						ENDIF
					ELSE
						PRINTLN("[VehicleInVehicle] PROCESS_VEHICLE_IN_VEHICLE_FUNCTIONALITY | Container Vehicle and Emerging Vehicle are still colliding!")
						RESET_NET_TIMER(tdVehicleExitingVehicleStateTimer)
						
						IF HAS_NET_TIMER_STARTED(tdVehicleExitingVehicleReleaseControlFailsafeTimer)
							IF RUN_TIMER(tdVehicleExitingVehicleReleaseControlFailsafeTimer, ciVehicleExitingVehicleReleaseControlFailsafeTimer_Length)
								PRINTLN("[VehicleInVehicle] PROCESS_VEHICLE_IN_VEHICLE_FUNCTIONALITY | Releasing network control lock due to failsafe timer!")
								SET_NETWORK_ID_CAN_MIGRATE(NETWORK_GET_NETWORK_ID_FROM_ENTITY(sVehState.vehIndex), TRUE)
								SET_NETWORK_ID_CAN_MIGRATE(NETWORK_GET_NETWORK_ID_FROM_ENTITY(viContainerVehicle), TRUE)
								
								// Reset net timer here so we don't get back in here again
								RESET_NET_TIMER(tdVehicleExitingVehicleReleaseControlFailsafeTimer)
							ENDIF
						ENDIF
					ENDIF
				ELSE
					NETWORK_REQUEST_CONTROL_OF_ENTITY(sVehState.vehIndex)
					PRINTLN("[VehicleInVehicle] PROCESS_VEHICLE_IN_VEHICLE_FUNCTIONALITY | Requesting control of Emerging Vehicle!")
						
				ENDIF
			ENDIF
		BREAK
		
		CASE ciVEHICLE_IN_VEHICLE_STATE__EMERGE_ENDING
			IF bControlsContainerVehicle
				SET_NETWORK_ID_CAN_MIGRATE(NETWORK_GET_NETWORK_ID_FROM_ENTITY(viContainerVehicle), TRUE)
				SET_ENTITY_CAN_ONLY_BE_DAMAGED_BY_SCRIPT_PARTICIPANTS(viContainerVehicle, FALSE)
				
				CLEAR_BIT(iLocalBoolCheck32, LBOOL32_LOCKED_MIGRATION_FOR_VEHICLE_IN_VEHICLE)
				PRINTLN("[VehicleInVehicle] PROCESS_VEHICLE_IN_VEHICLE_FUNCTIONALITY | Releasing network control of the Container vehicle!")
			ENDIF
			
			IF sVehState.bHasControl
				SET_NETWORK_ID_CAN_MIGRATE(NETWORK_GET_NETWORK_ID_FROM_ENTITY(sVehState.vehIndex), TRUE)
				SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(sVehState.vehIndex, TRUE)
				SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(sVehState.vehIndex, FALSE)
				SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(sVehState.vehIndex, FALSE)
				SET_VEHICLE_RESPECTS_LOCKS_WHEN_HAS_DRIVER(sVehState.vehIndex, FALSE)
				
				CLEAR_BIT(iLocalBoolCheck32, LBOOL32_LOCKED_MIGRATION_FOR_VEHICLE_IN_VEHICLE)
				PRINTLN("[VehicleInVehicle] PROCESS_VEHICLE_IN_VEHICLE_FUNCTIONALITY | Releasing network control of the Contained vehicle!")
			ENDIF
			
			BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_VehicleInVehicle_StateChange, sVehState.iIndex, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, ciVEHICLE_IN_VEHICLE_STATE__EMERGE_COMPLETE)
		BREAK
		
		CASE ciVEHICLE_IN_VEHICLE_STATE__EMERGE_COMPLETE
			// All done - vehicle is released and Container Vehicle is closed again
		BREAK
		
	ENDSWITCH
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Burning Vehicles
// ##### Description: Processing of vehicles that are set to be on fire.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Called every frame by the player who has control of the burning vehicle.
PROC PROCESS_CLIENT_BURNING_VEHICLE_WITH_CONTROL(FMMC_VEHICLE_STATE &sVehState)

	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetSeven, ciFMMC_VEHICLE7_SPAWN_BURNING)
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(MC_serverBD_4.sBurningVehicle.iBurningVehiclePTFXBitset, sVehState.iIndex)
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(iBurningVehicleScorchedBitset, sVehState.iIndex)
	AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iBurningVehicleBS, ciFMMC_BURNING_VEH_NO_SCORCH)
		SET_ENTITY_RENDER_SCORCHED(sVehState.vehIndex, TRUE)
		SET_BIT(iBurningVehicleScorchedBitset, sVehState.iIndex)
		PRINTLN("[Vehicle ",sVehState.iIndex,"][BURNING_VEHICLE] PROCESS_CLIENT_BURNING_VEHICLE_WITH_CONTROL - Vehicle: ", sVehState.iIndex, " Setting Scorched")
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iBurningLifetime <= 0
		//Infinite burn - no damage.
		EXIT
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iBurningVehicleBS, ciFMMC_BURNING_VEH_DEAL_DAMAGE_TO_VEH)
	AND NOT IS_VEHICLE_FUCKED_MP(sVehState.vehIndex)
		IF GET_FMMC_VEHICLE_CURRENT_BODY_HEALTH(sVehState) - MC_serverBD_4.sBurningVehicle.sVehicle[sVehState.iIndex].fBurningVehicleHealth >= 50
			SET_VEHICLE_BODY_HEALTH(sVehState.vehIndex, MC_serverBD_4.sBurningVehicle.sVehicle[sVehState.iIndex].fBurningVehicleHealth)
			PRINTLN("[Vehicle ",sVehState.iIndex,"][BURNING_VEHICLE] PROCESS_CLIENT_BURNING_VEHICLE_WITH_CONTROL - Reducing vehicle body health to match burning health!")
		ENDIF
	ENDIF
	
	IF MC_serverBD_4.sBurningVehicle.sVehicle[sVehState.iIndex].fBurningVehicleHealth <= 0
	AND NOT IS_BIT_SET(MC_serverBD_4.sBurningVehicle.iBurningVehicleExtinguishedBitset, sVehState.iIndex)
		PRINTLN("[Vehicle ",sVehState.iIndex,"][BURNING_VEHICLE] PROCESS_CLIENT_BURNING_VEHICLE_WITH_CONTROL - Vehicle: ", sVehState.iIndex, " Destroying!")
		SET_VEHICLE_BODY_HEALTH(sVehState.vehIndex, 0.0)
		SET_ENTITY_HEALTH(sVehState.vehIndex, 0)
	ENDIF
	
ENDPROC

FUNC FLOAT GET_BURNING_VEHICLE_HEALTH_START(INT iVeh)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iBurningVehicleBS, ciFMMC_BURNING_VEH_DEAL_DAMAGE_TO_VEH)
		RETURN MC_serverBD_4.sBurningVehicle.sVehicle[iVeh].fVehicleStartHealth
	ENDIF
	
	RETURN TO_FLOAT(ciMAX_BURNING_VEHICLE_HEALTH)
ENDFUNC

FUNC INT GET_BURNING_LIFE_TIME(FMMC_VEHICLE_STATE &sVehState)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iBurningVehicleBS, ciFMMC_BURNING_VEH_DEAL_DAMAGE_TO_VEH)
		RETURN ROUND(TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iBurningLifetime) * (GET_ENTITY_HEALTH(sVehState.vehIndex) / g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].fBodyHealth))
	ENDIF
	
	RETURN g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iBurningLifetime
ENDFUNC

PROC PROCESS_YOUGA4_SMOKE_PTFX_EVOLUTION(FMMC_VEHICLE_STATE &sVehState)
	FLOAT fHealthTop = GET_BURNING_VEHICLE_HEALTH_START(sVehState.iIndex)
	//Scale the evo at 2x the rate of burning
	FLOAT fEvo = CLAMP(1.0 - (MC_serverBD_4.sBurningVehicle.sVehicle[sVehState.iIndex].fBurningVehicleHealth / fHealthTop), 0.0, 1.0)
	fEvo= CLAMP(fEvo*2.0, 0.0, 1.0)
	FLOAT fBackEvo = CLAMP(fEvo * 10.0, 0.0, 1.0)
	
	SET_PARTICLE_FX_LOOPED_EVOLUTION(MC_serverBD_4.sBurningVehicle.sVehicle[sVehState.iIndex].ptfxVehicleFireFX_Front, "spread_back", fBackEvo, FALSE)
	SET_PARTICLE_FX_LOOPED_EVOLUTION(MC_serverBD_4.sBurningVehicle.sVehicle[sVehState.iIndex].ptfxVehicleFireFX_Front, "spread_length", fEvo, FALSE)
ENDPROC

PROC PROCESS_BURNING_VEHICLE_SMOKE_PTFX_EVOLUTION(FMMC_VEHICLE_STATE &sVehState)
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iBurningVehicleBS, ciFMMC_BURNING_VEH_SCALE_EVOLUTION)
		EXIT
	ENDIF
	
	IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(MC_serverBD_4.sBurningVehicle.sVehicle[sVehState.iIndex].ptfxVehicleFireFX_Front)
		EXIT
	ENDIF
	
	SWITCH sVehState.mn
		CASE YOUGA4
			PROCESS_YOUGA4_SMOKE_PTFX_EVOLUTION(sVehState)
		BREAK
		DEFAULT
			FLOAT fHealthTop, fEvo
			fHealthTop = GET_BURNING_VEHICLE_HEALTH_START(sVehState.iIndex)
			//Scale the evo at 2x the rate of burning
			fEvo = CLAMP(1.0 - (MC_serverBD_4.sBurningVehicle.sVehicle[sVehState.iIndex].fBurningVehicleHealth / fHealthTop), 0.0, 1.0)
			fEvo= CLAMP(fEvo*2.0, 0.0, 1.0)
			SET_PARTICLE_FX_LOOPED_EVOLUTION(MC_serverBD_4.sBurningVehicle.sVehicle[sVehState.iIndex].ptfxVehicleFireFX_Front, "strength", fEvo, FALSE)
		BREAK
	ENDSWITCH
	
ENDPROC

PROC PROCESS_SERVER_BURNING_VEHICLE(FMMC_VEHICLE_STATE &sVehState)

	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetSeven, ciFMMC_VEHICLE7_SPAWN_BURNING)
		EXIT
	ENDIF
	
	INT iTeam = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iBurnTeam
	IF MC_serverBD_4.iCurrentHighestPriority[iTeam] < g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iBurnRule
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(MC_serverBD_4.sBurningVehicle.iBurningVehiclePTFXBitset, sVehState.iIndex)
	AND NOT IS_BIT_SET(MC_serverBD_4.sBurningVehicle.iBurningVehicleExtinguishedBitset, sVehState.iIndex)
		
		MODEL_NAMES mnVeh = GET_ENTITY_MODEL(sVehState.vehIndex)
		
		PRINTLN("[Vehicle ",sVehState.iIndex,"][BURNING_VEHICLE] PROCESS_SERVER_BURNING_VEHICLE - Vehicle: ", sVehState.iIndex, " Starting PTFX.")
		
		USE_PARTICLE_FX_ASSET(GET_BURNING_VEHICLE_PTFX_ASSET(mnVeh))
		MC_serverBD_4.sBurningVehicle.sVehicle[sVehState.iIndex].ptfxVehicleFireFX_Front = START_NETWORKED_PARTICLE_FX_LOOPED_ON_ENTITY(GET_BURNING_VEHICLE_PTFX_FRONT(mnVeh), sVehState.vehIndex, <<0.0,0.0,0.0>>, <<0.0,0.0,0.0>>)
		
		IF NOT IS_STRING_NULL_OR_EMPTY(GET_BURNING_VEHICLE_PTFX_BACK(mnVeh, sVehState.iIndex))
			USE_PARTICLE_FX_ASSET(GET_BURNING_VEHICLE_PTFX_ASSET(mnVeh))
			MC_serverBD_4.sBurningVehicle.sVehicle[sVehState.iIndex].ptfxVehicleFireFX_Back = START_NETWORKED_PARTICLE_FX_LOOPED_ON_ENTITY(GET_BURNING_VEHICLE_PTFX_BACK(mnVeh, sVehState.iIndex), sVehState.vehIndex, <<0.0,0.0,0.0>>, <<0.0,0.0,0.0>>)
		ENDIF
		
		MC_serverBD_4.sBurningVehicle.sVehicle[sVehState.iIndex].fVehicleStartHealth = GET_FMMC_VEHICLE_CURRENT_BODY_HEALTH(sVehState)
		MC_serverBD_4.sBurningVehicle.sVehicle[sVehState.iIndex].iLifeTime = GET_BURNING_LIFE_TIME(sVehState)
		
		MC_serverBD_4.sBurningVehicle.sVehicle[sVehState.iIndex].fBurningVehicleHealth = GET_BURNING_VEHICLE_HEALTH_START(sVehState.iIndex)
		
		IF NOT DOES_VEHICLE_MODEL_REQUIRE_ALTERNATIVE_SMOKE_VFX(mnVeh)
			IF DOES_PARTICLE_FX_LOOPED_EXIST(MC_serverBD_4.sBurningVehicle.sVehicle[sVehState.iIndex].ptfxVehicleFireFX_Front)
				SET_PARTICLE_FX_LOOPED_EVOLUTION(MC_serverBD_4.sBurningVehicle.sVehicle[sVehState.iIndex].ptfxVehicleFireFX_Front, "strength", 1.0, FALSE)
			ENDIF
			IF DOES_PARTICLE_FX_LOOPED_EXIST(MC_serverBD_4.sBurningVehicle.sVehicle[sVehState.iIndex].ptfxVehicleFireFX_Back)
				SET_PARTICLE_FX_LOOPED_EVOLUTION(MC_serverBD_4.sBurningVehicle.sVehicle[sVehState.iIndex].ptfxVehicleFireFX_Back, "strength", 1.0, FALSE)
			ENDIF
		ENDIF
		
		IF DOES_PARTICLE_FX_LOOPED_EXIST(MC_serverBD_4.sBurningVehicle.sVehicle[sVehState.iIndex].ptfxVehicleFireFX_Front)
		
			PRINTLN("[Vehicle ",sVehState.iIndex,"][BURNING_VEHICLE] PROCESS_SERVER_BURNING_VEHICLE - Vehicle: ", sVehState.iIndex, " PTFX Started")
			SET_BIT(MC_serverBD_4.sBurningVehicle.iBurningVehiclePTFXBitset, sVehState.iIndex)
			
			IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iBurningVehicleBS, ciFMMC_BURNING_VEH_NO_SCORCH)
				VECTOR vVanOffset = -GET_ENTITY_FORWARD_VECTOR(sVehState.vehIndex) * 2.5
				VECTOR vDecalPos = GET_ENTITY_COORDS(sVehState.vehIndex) + vVanOffset
				VECTOR vDecalRot = GET_ENTITY_FORWARD_VECTOR(sVehState.vehIndex)
				di_RiotVanDecals[sVehState.iIndex] = ADD_DECAL(DECAL_RSID_GENERIC_SCORCH, vDecalPos, vDecalRot, vDecalRot, 2.5, 2.5, 1.0, 1.0, 1.0, 1.0, 99000)
			ENDIF
		ENDIF
		
		EXIT
		
	ENDIF	
		
	IF MC_serverBD_4.sBurningVehicle.sVehicle[sVehState.iIndex].iLifeTime > 0
	AND NOT IS_BIT_SET(MC_serverBD_4.sBurningVehicle.iBurningVehicleExtinguishedBitset, sVehState.iIndex)
	AND MC_serverBD_4.sBurningVehicle.sVehicle[sVehState.iIndex].fBurningVehicleHealth >= 0.0
		
		IF MC_serverBD_4.sBurningVehicle.sVehicle[sVehState.iIndex].fBurningVehicleHealth > GET_BURNING_VEHICLE_HEALTH_START(sVehState.iIndex)
			MC_serverBD_4.sBurningVehicle.sVehicle[sVehState.iIndex].fBurningVehicleHealth = GET_BURNING_VEHICLE_HEALTH_START(sVehState.iIndex)
		ENDIF
		
		FLOAT fDamageToVehicleThisFrame = GET_BURNING_VEHICLE_HEALTH_START(sVehState.iIndex) / MC_serverBD_4.sBurningVehicle.sVehicle[sVehState.iIndex].iLifeTime * GET_FRAME_TIME() * TO_FLOAT(MC_serverBD.iNumVehCreated)
		MC_serverBD_4.sBurningVehicle.sVehicle[sVehState.iIndex].fBurningVehicleHealth -= fDamageToVehicleThisFrame
		
		PROCESS_BURNING_VEHICLE_SMOKE_PTFX_EVOLUTION(sVehState)
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iBurningVehicleBS, ciFMMC_BURNING_VEH_DEAL_DAMAGE_TO_VEH)
		AND MC_serverBD_4.sBurningVehicle.sVehicle[sVehState.iIndex].fBurningVehicleHealth - GET_FMMC_VEHICLE_CURRENT_BODY_HEALTH(sVehState) >= 50
			MC_serverBD_4.sBurningVehicle.sVehicle[sVehState.iIndex].fBurningVehicleHealth = GET_FMMC_VEHICLE_CURRENT_BODY_HEALTH(sVehState)
			PRINTLN("[Vehicle ",sVehState.iIndex,"][BURNING_VEHICLE] PROCESS_SERVER_BURNING_VEHICLE - Adjusting fBurningVehicleHealth to match vehicle health. Likely taken damage from another source!")
		ENDIF
		
	ENDIF

	IF IS_BIT_SET(MC_serverBD_4.sBurningVehicle.iBurningVehicleExtinguishedBitset, sVehState.iIndex)
	AND IS_BIT_SET(MC_serverBD_4.sBurningVehicle.iBurningVehiclePTFXBitset, sVehState.iIndex)
		IF DOES_PARTICLE_FX_LOOPED_EXIST(MC_serverBD_4.sBurningVehicle.sVehicle[sVehState.iIndex].ptfxVehicleFireFX_Front)
			
			PRINTLN("[Vehicle ",sVehState.iIndex,"][BURNING_VEHICLE] PROCESS_SERVER_BURNING_VEHICLE - Vehicle: ", sVehState.iIndex, " Cleanup up PTFX")
		
			//Extinguish effect was here - not in Mule ptfx asset. Re-add if needed
			
			STOP_PARTICLE_FX_LOOPED(MC_serverBD_4.sBurningVehicle.sVehicle[sVehState.iIndex].ptfxVehicleFireFX_Front)
			MC_serverBD_4.iNumberOfFiresExtinguished[iTeam]++
			
		ENDIF
		
		IF DOES_PARTICLE_FX_LOOPED_EXIST(MC_serverBD_4.sBurningVehicle.sVehicle[sVehState.iIndex].ptfxVehicleFireFX_Back)
		
			//Extinguish effect was here - not in Mule ptfx asset. Re-add if needed
			STOP_PARTICLE_FX_LOOPED(MC_serverBD_4.sBurningVehicle.sVehicle[sVehState.iIndex].ptfxVehicleFireFX_Back)
			
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetSeven, ciFMMC_VEHICLE7_PROGRESS_RULE_IF_FIRE_PUT_OUT) 
			
			PRINTLN("[Vehicle ",sVehState.iIndex,"][BURNING_VEHICLE] PROCESS_SERVER_BURNING_VEHICLE - Vehicle: ", sVehState.iIndex, " Progressing Objective")
			PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_VEHICLE, sVehState.iIndex, iTeam, TRUE)
			
		ENDIF	
		
		CLEAR_BIT(MC_serverBD_4.sBurningVehicle.iBurningVehiclePTFXBitset, sVehState.iIndex)
		PRINTLN("[Vehicle ",sVehState.iIndex,"][BURNING_VEHICLE] PROCESS_SERVER_BURNING_VEHICLE - Vehicle: ", sVehState.iIndex, " No longer burning")
		
	ENDIF
			
ENDPROC

PROC PROCESS_EXTINGUISH_VEHICLE_FIRE(FMMC_VEHICLE_STATE &sVehState)
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetFive, ciFMMC_VEHICLE5_EXTINGUISH_FIRE_ON_VEH_HIGH_HEALTH)
		IF GET_ENTITY_HEALTH(sVehState.vehIndex) > GET_ENTITY_MAX_HEALTH(sVehState.vehIndex) * 0.2
			IF IS_ENTITY_ON_FIRE(sVehState.vehIndex)
				PRINTLN("[RCC MISSION] PROCESS_VEH_BODY PROCESS_EXTINGUISH_VEHICLE_FIRE - Veh ", sVehState.iIndex, " was on fire. Extinguishing. ", GET_ENTITY_HEALTH(sVehState.vehIndex), " / ", GET_ENTITY_MAX_HEALTH(sVehState.vehIndex))
				STOP_ENTITY_FIRE(sVehState.vehIndex)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Shared Vehicle Processing ---------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Functions that can be used by both the staggered and every frame vehicle loops.  ---------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

//[FMMC2020] Suitable for local player?
FUNC BOOL IS_PLAYER_IN_PLANE_IN_WATER(PED_INDEX tempped)
	IF NOT IS_PED_INJURED(tempped)
		IF IS_PED_A_PLAYER(tempped)
			IF IS_PED_IN_ANY_PLANE(tempped)
				VEHICLE_INDEX vehTemp = GET_VEHICLE_PED_IS_IN(tempped)
				IF IS_VEHICLE_IN_WATER(vehTemp)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_PLACED_PED_IN_VEHICLE(VEHICLE_INDEX viVehicleToCheck)

	IF DOES_ENTITY_EXIST(viVehicleToCheck)
		IF NOT IS_VEHICLE_EMPTY(viVehicleToCheck)
			INT iPlacedPed
			FOR iPlacedPed = 0 TO MC_serverBD.iNumPedCreated - 1
				PED_INDEX piVehiclePed 
				IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[iPlacedPed])
					piVehiclePed = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iPlacedPed])
				ENDIF
				
				IF DOES_ENTITY_EXIST(piVehiclePed)
					IF IS_PED_IN_THIS_VEHICLE(piVehiclePed, viVehicleToCheck)
						RETURN iPlacedPed
					ENDIF
				ENDIF
			ENDFOR
		ENDIF
	ENDIF
	
	RETURN -1
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Back of Vehicle Driveby Sequence
// ##### Description: Processing the back-of-vehicle driveby functionality (e.g sitting in the back of a Mule, shooting out of the back)
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC SET_BACK_OF_VEHICLE_DRIVEBY_SEQUENCE_STATE(INT iNewState)
	iVehicleDrivebySequenceState = iNewState
	PRINTLN("[Driveby] SET_BACK_OF_VEHICLE_DRIVEBY_SEQUENCE_STATE | iVehicleDrivebySequenceState is now ", iVehicleDrivebySequenceState)
ENDPROC

PROC PLAY_BACK_OF_VEHICLE_DRIVEBY_SEQUENCE_EXIT_ANIM()
	// Play custom player exit anims
	TASK_LEAVE_ANY_VEHICLE(LocalPlayerPed, 0, ECF_WARP_PED | ECF_WARP_IF_DOOR_IS_BLOCKED | ECF_DONT_WAIT_FOR_VEHICLE_TO_STOP)
ENDPROC

FUNC VEHICLE_SEAT GET_TARGET_SEAT_FOR_DRIVEBY_SEQUENCE(BOOL bGetFirstSeat = FALSE)
	IF bGetFirstSeat
		// Get the "back seat" rather than the final driveby seat
		SWITCH GET_LOCAL_PLAYER_TEAM(TRUE)
			CASE 0			RETURN VS_BACK_RIGHT
			CASE 1			RETURN VS_BACK_LEFT
		ENDSWITCH
	ELSE
		SWITCH GET_LOCAL_PLAYER_TEAM(TRUE)
			CASE 0			RETURN VS_EXTRA_RIGHT_1
			CASE 1			RETURN VS_EXTRA_LEFT_1
		ENDSWITCH
	ENDIF
	
	RETURN VS_DRIVER
ENDFUNC

PROC CLEAN_UP_BACK_OF_VEHICLE_DRIVEBY_SEQUENCE()

	#IF IS_DEBUG_BUILD
	IF bDrivebyDebug
		TEXT_LABEL_63 tlVisualDebug = "CLEAN_UP_BACK_OF_VEHICLE_DRIVEBY_SEQUENCE"
		DRAW_DEBUG_TEXT_2D(tlVisualDebug, (<<0.5, 0.5, 0.5>>), 255, 0, 0, 255)
	ENDIF
	#ENDIF
		
	IF NOT PERFORMING_VEHICLE_DRIVEBY_SEQUENCE()
		EXIT
	ENDIF
	
	PRINTLN("[Driveby] PROCESS_BACK_OF_VEHICLE_DRIVEBY_SEQUENCE | Cleaning up driveby sequence now!")
	SET_BACK_OF_VEHICLE_DRIVEBY_SEQUENCE_STATE(ciVEHICLE_DRIVEBY_SEQUENCE_STATE__INACTIVE)
	
	PLAY_BACK_OF_VEHICLE_DRIVEBY_SEQUENCE_EXIT_ANIM()
	SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_ForcedAim, FALSE)
ENDPROC

FUNC BOOL SHOULD_PROCESS_VEHICLE_DRIVEBY_SEQUENCE_NOW()
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[GET_LOCAL_PLAYER_TEAM()].iRuleBitsetFifteen[GET_LOCAL_PLAYER_CURRENT_RULE()], ciBS_RULE15_PERFORM_VEHICLE_DRIVEBY_SHOOTING_SEQUENCE)
		// We're currently on a rule set as a "driveby" rule
		RETURN TRUE
	ENDIF
	
	// Check the next rule - if we're watching a cutscene before a driveby rule, get the driveby set up in the background
	INT iPrevRule = CLAMP_INT(GET_LOCAL_PLAYER_CURRENT_RULE() - 1, 0, FMMC_MAX_RULES - 1)
	INT iNextRule = CLAMP_INT(GET_LOCAL_PLAYER_CURRENT_RULE() + 1, 0, FMMC_MAX_RULES - 1)
	
	IF NOT PERFORMING_VEHICLE_DRIVEBY_SEQUENCE()
		// Wait until we're watching the cutscene and then work in the background
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[GET_LOCAL_PLAYER_TEAM()].iRuleBitsetFifteen[iNextRule], ciBS_RULE15_PERFORM_VEHICLE_DRIVEBY_SHOOTING_SEQUENCE)
		AND IS_CUTSCENE_PLAYING()
		AND GET_CUTSCENE_TIME() > 17100
			PRINTLN("[Driveby_SPAM] SHOULD_PROCESS_VEHICLE_DRIVEBY_SEQUENCE_NOW | Processing in the background of the driveby start cutscene!")
			RETURN TRUE
		ELSE
			PRINTLN("[Driveby_SPAM] SHOULD_PROCESS_VEHICLE_DRIVEBY_SEQUENCE_NOW | Waiting for driveby start cutscene!")
		ENDIF
		
	ELSE
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[GET_LOCAL_PLAYER_TEAM()].iRuleBitsetFifteen[iNextRule], ciBS_RULE15_PERFORM_VEHICLE_DRIVEBY_SHOOTING_SEQUENCE)
			// Keep processing while in the brief period between the cutscene and the rule fully moving on
			PRINTLN("[Driveby_SPAM] SHOULD_PROCESS_VEHICLE_DRIVEBY_SEQUENCE_NOW | Continuing to process the driveby sequence while we wait to be moved into the driveby rule")
			RETURN TRUE
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[GET_LOCAL_PLAYER_TEAM()].iRuleBitsetFifteen[iPrevRule], ciBS_RULE15_PERFORM_VEHICLE_DRIVEBY_SHOOTING_SEQUENCE)
			IF NOT IS_CUTSCENE_PLAYING()
			OR GET_CUTSCENE_TIME() < 2000
				// Keep processing while in the brief period between moving to the post-driveby cutscene rule before the cutscene starts. Clean up the driveby stuff in the background of the cutscene
				PRINTLN("[Driveby_SPAM] SHOULD_PROCESS_VEHICLE_DRIVEBY_SEQUENCE_NOW | Continuing to process the driveby sequence while we wait for the driveby end cutscene to start")
				RETURN TRUE
			ELSE
				PRINTLN("[Driveby_SPAM] SHOULD_PROCESS_VEHICLE_DRIVEBY_SEQUENCE_NOW | Driveby end cutscene is playing!")
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_BACK_OF_VEHICLE_DRIVEBY_SEQUENCE(FMMC_VEHICLE_STATE &sVehState)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetNine, ciFMMC_VEHICLE9_IsDrivebySequenceVehicle)
		EXIT
	ENDIF
	
	IF NOT SHOULD_PROCESS_VEHICLE_DRIVEBY_SEQUENCE_NOW()
		CLEAN_UP_BACK_OF_VEHICLE_DRIVEBY_SEQUENCE()
		EXIT
	ENDIF
		
	SWITCH iVehicleDrivebySequenceState
		CASE ciVEHICLE_DRIVEBY_SEQUENCE_STATE__INACTIVE
			IF GET_PED_IN_VEHICLE_SEAT(sVehState.vehIndex, GET_TARGET_SEAT_FOR_DRIVEBY_SEQUENCE(FALSE), TRUE) = LocalPlayerPed
				// We're already good to go!
				PRINTLN("[Driveby] PROCESS_BACK_OF_VEHICLE_DRIVEBY_SEQUENCE | Player is already in seat ", GET_TARGET_SEAT_FOR_DRIVEBY_SEQUENCE(FALSE))
				SET_BACK_OF_VEHICLE_DRIVEBY_SEQUENCE_STATE(ciVEHICLE_DRIVEBY_SEQUENCE_STATE__MAINTAIN_DRIVEBY)
			ELSE
				// Go through the state machine to arrive in the seat safely
				SET_BACK_OF_VEHICLE_DRIVEBY_SEQUENCE_STATE(ciVEHICLE_DRIVEBY_SEQUENCE_STATE__START_ENTERING_BACK_SEATS)
			ENDIF
		BREAK
		
		CASE ciVEHICLE_DRIVEBY_SEQUENCE_STATE__START_ENTERING_BACK_SEATS
			SET_PED_ALLOW_VEHICLES_OVERRIDE(LocalPlayerPed, TRUE)
			
			PRINTLN("[Driveby] PROCESS_BACK_OF_VEHICLE_DRIVEBY_SEQUENCE | Entering seat ", GET_TARGET_SEAT_FOR_DRIVEBY_SEQUENCE(TRUE))
			FREEZE_ENTITY_POSITION(LocalPlayerPed, FALSE)
			SET_PED_INTO_VEHICLE(LocalPlayerPed, sVehState.vehIndex, GET_TARGET_SEAT_FOR_DRIVEBY_SEQUENCE(TRUE))
			
			MC_STOP_NET_TIMER_WITH_TIMESTAMP(iVehicleDrivebySeatHopTimestamp)
			SET_BACK_OF_VEHICLE_DRIVEBY_SEQUENCE_STATE(ciVEHICLE_DRIVEBY_SEQUENCE_STATE__ENTERING_BACK_SEATS)
		BREAK
		
		CASE ciVEHICLE_DRIVEBY_SEQUENCE_STATE__ENTERING_BACK_SEATS
			IF NOT IS_PED_IN_VEHICLE(LocalPlayerPed, sVehState.vehIndex, FALSE)
				PRINTLN("[Driveby] PROCESS_BACK_OF_VEHICLE_DRIVEBY_SEQUENCE | Waiting for ped to enter vehicle!")
			ELSE
				IF MC_RUN_NET_TIMER_WITH_TIMESTAMP(iVehicleDrivebySeatHopTimestamp, ciVehicleDrivebySeatHopDelayLength)
					SET_BACK_OF_VEHICLE_DRIVEBY_SEQUENCE_STATE(ciVEHICLE_DRIVEBY_SEQUENCE_STATE__ENTER_DRIVEBY_SEATS)
				ELSE
					PRINTLN("[Driveby] PROCESS_BACK_OF_VEHICLE_DRIVEBY_SEQUENCE | Waiting for iVehicleDrivebySeatHopTimestamp!")
				ENDIF
			ENDIF
		BREAK
		
		CASE ciVEHICLE_DRIVEBY_SEQUENCE_STATE__ENTER_DRIVEBY_SEATS
			PRINTLN("[Driveby] PROCESS_BACK_OF_VEHICLE_DRIVEBY_SEQUENCE | Entering seat ", GET_TARGET_SEAT_FOR_DRIVEBY_SEQUENCE(FALSE))
			FREEZE_ENTITY_POSITION(LocalPlayerPed, FALSE)
			SET_PED_INTO_VEHICLE(LocalPlayerPed, sVehState.vehIndex, GET_TARGET_SEAT_FOR_DRIVEBY_SEQUENCE(FALSE))
			
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0) 
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
			
			SET_BACK_OF_VEHICLE_DRIVEBY_SEQUENCE_STATE(ciVEHICLE_DRIVEBY_SEQUENCE_STATE__MAINTAIN_DRIVEBY)
		BREAK
		
		CASE ciVEHICLE_DRIVEBY_SEQUENCE_STATE__MAINTAIN_DRIVEBY
			PRINTLN("[Driveby_SPAM] PROCESS_BACK_OF_VEHICLE_DRIVEBY_SEQUENCE | Maintaining driveby")
			SET_ALLOW_CUSTOM_VEHICLE_DRIVE_BY_CAM_THIS_UPDATE(TRUE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
			
			IF NOT GET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_ForcedAim, FALSE)
				PRINTLN("[Driveby] PROCESS_BACK_OF_VEHICLE_DRIVEBY_SEQUENCE | Setting PCF_ForcedAim")
				SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_ForcedAim, TRUE)
			ENDIF
			
			IF IS_CUTSCENE_PLAYING()
			OR IS_SCREEN_FADED_OUT()
			OR IS_SCREEN_FADING_IN()
				// Centre the player's aim so it's the correct angle when they take control rather than aiming off to the side
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0) 
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
			ENDIF
			
			IF NOT IS_CUTSCENE_PLAYING()
				IF NOT IS_PED_IN_VEHICLE(LocalPlayerPed, sVehState.vehIndex, TRUE)
					PRINTLN("[Driveby] PROCESS_BACK_OF_VEHICLE_DRIVEBY_SEQUENCE | Something went wrong! Returning the ped to the vehicle by going through the state machine again")
					ASSERTLN("[Driveby] PROCESS_BACK_OF_VEHICLE_DRIVEBY_SEQUENCE | Something went wrong! Returning the ped to the vehicle by going through the state machine again")
					CLEAN_UP_BACK_OF_VEHICLE_DRIVEBY_SEQUENCE()
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
	IF bDrivebyDebug
		ENTER_EXIT_VEHICLE_FLAGS eEnterVehicleFlags = (ECF_WARP_PED | ECF_RESUME_IF_INTERRUPTED | ECF_JACK_ANYONE | ECF_DONT_WAIT_FOR_VEHICLE_TO_STOP | ECF_BLOCK_SEAT_SHUFFLING | ECF_DONT_CLOSE_DOOR)
		
		TEXT_LABEL_63 tlVisualDebug2 = "PROCESSING DRIVEBY SEQUENCE NOW!"
		DRAW_DEBUG_TEXT_2D(tlVisualDebug2, (<<0.25, 0.1, 0.5>>), 255, 255, 255, 255)
		
		tlVisualDebug2 = "iVehicleDrivebySequenceState: "
		tlVisualDebug2 += iVehicleDrivebySequenceState
		DRAW_DEBUG_TEXT_2D(tlVisualDebug2, (<<0.25, 0.15, 0.5>>), 255, 255, 255, 255)
	
		IF IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD1)
			TASK_ENTER_VEHICLE(LocalPlayerPed, sVehState.vehIndex, DEFAULT, INT_TO_ENUM(VEHICLE_SEAT, ENUM_TO_INT(VS_BACK_LEFT) + 0), DEFAULt, eEnterVehicleFlags)
		ENDIF
		IF IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD3)
			TASK_ENTER_VEHICLE(LocalPlayerPed, sVehState.vehIndex, DEFAULT, INT_TO_ENUM(VEHICLE_SEAT, ENUM_TO_INT(VS_BACK_RIGHT) + 0), DEFAULt, eEnterVehicleFlags)
		ENDIF
		
		IF IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD4)
			TASK_ENTER_VEHICLE(LocalPlayerPed, sVehState.vehIndex, DEFAULT, VS_EXTRA_LEFT_1, DEFAULt, eEnterVehicleFlags)
		ENDIF
		IF IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD6)
			TASK_ENTER_VEHICLE(LocalPlayerPed, sVehState.vehIndex, DEFAULT, VS_EXTRA_RIGHT_1, DEFAULt, eEnterVehicleFlags)
		ENDIF
		
		PED_INDEX pedLeft0 = GET_PED_IN_VEHICLE_SEAT(sVehState.vehIndex, VS_BACK_LEFT)
		PED_INDEX pedRight0 = GET_PED_IN_VEHICLE_SEAT(sVehState.vehIndex, VS_BACK_RIGHT)
		
		IF DOES_ENTITY_EXIST(pedLeft0)
			TEXT_LABEL_63 tlVisualDebug = "VS_BACK_LEFT: "
			tlVisualDebug += FMMC_GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(pedLeft0))
			DRAW_DEBUG_TEXT_2D(tlVisualDebug, (<<0.1, 0.5, 0.5>>), 255, 255, 255, 255)
		ENDIF
		
		IF DOES_ENTITY_EXIST(pedRight0)
			TEXT_LABEL_63 tlVisualDebug = "VS_BACK_RIGHT: "
			tlVisualDebug += FMMC_GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(pedRight0))
			DRAW_DEBUG_TEXT_2D(tlVisualDebug, (<<0.6, 0.5, 0.5>>), 255, 255, 255, 255)
		ENDIF
		
		PED_INDEX pedLeft = GET_PED_IN_VEHICLE_SEAT(sVehState.vehIndex, VS_EXTRA_LEFT_1)
		PED_INDEX pedRight = GET_PED_IN_VEHICLE_SEAT(sVehState.vehIndex, VS_EXTRA_RIGHT_1)
		
		IF DOES_ENTITY_EXIST(pedLeft)
			TEXT_LABEL_63 tlVisualDebug = "VS_EXTRA_LEFT_1: "
			tlVisualDebug += FMMC_GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(pedLeft))
			DRAW_DEBUG_TEXT_2D(tlVisualDebug, (<<0.1, 0.6, 0.5>>), 255, 255, 255, 255)
		ENDIF
		
		IF DOES_ENTITY_EXIST(pedRight)
			TEXT_LABEL_63 tlVisualDebug = "VS_EXTRA_RIGHT_1: "
			tlVisualDebug += FMMC_GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(pedRight))
			DRAW_DEBUG_TEXT_2D(tlVisualDebug, (<<0.6, 0.6, 0.5>>), 255, 255, 255, 255)
		ENDIF
		
		PED_INDEX pedLeft2 = GET_PED_IN_VEHICLE_SEAT(sVehState.vehIndex, VS_EXTRA_LEFT_2)
		PED_INDEX pedRight2 = GET_PED_IN_VEHICLE_SEAT(sVehState.vehIndex, VS_EXTRA_RIGHT_2)
		
		IF DOES_ENTITY_EXIST(pedLeft2)
			TEXT_LABEL_63 tlVisualDebug = "VS_EXTRA_LEFT_2: "
			tlVisualDebug += FMMC_GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(pedLeft2))
			DRAW_DEBUG_TEXT_2D(tlVisualDebug, (<<0.1, 0.7, 0.5>>), 255, 255, 255, 255)
		ENDIF
		
		IF DOES_ENTITY_EXIST(pedRight2)
			TEXT_LABEL_63 tlVisualDebug = "VS_EXTRA_RIGHT_2: "
			tlVisualDebug += FMMC_GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(pedRight2))
			DRAW_DEBUG_TEXT_2D(tlVisualDebug, (<<0.6, 0.7, 0.5>>), 255, 255, 255, 255)
		ENDIF
	ENDIF
	#ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Vehicle Rule Warps
// ##### Description: Moving the Vehicle to a creator placed position on a rule change.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC WARP_VEHICLE_TO_WARP_LOCATION(FMMC_VEHICLE_STATE &sVehState, VECTOR vVehWarpPos, FLOAT fVehWarpHead, INT &iVehWarpedBS[], INT iEventVehWarpedBS)	
	// Should be checked last so that we can return true in this function on the first frame the checks come back positive.
	IF IS_WARP_LOCATION_VISIBLE_TO_ANY_PLAYERS(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].sWarpLocationSettings, GET_FMMC_VEHICLE_COORDS(sVehState), vVehWarpPos)
		PRINTLN("[Warp_location][Vehicles][Vehicle ", sVehState.iIndex, "] - WARP_VEHICLE_TO_WARP_LOCATION - IS_WARP_LOCATION_VISIBLE_TO_ANY_PLAYERS = TRUE. Returning False.")
		EXIT
	ENDIF
	
	IF NOT IS_VECTOR_ZERO(vVehWarpPos)		
		PRINTLN("[Warp_location][Vehicles][Vehicle ", sVehState.iIndex, "] - WARP_VEHICLE_TO_WARP_LOCATION - Warping this vehicle with COORD: ", vVehWarpPos, " and Heading: ", fVehWarpHead, " Setting iVehicleWarpedOnThisRule.")		
		PLAY_WARP_ENTITY_TO_WARP_LOCATION_PTFX(GET_FMMC_VEHICLE_COORDS(sVehState), g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].sWarpLocationSettings.sEntityWarpPTFX, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].sWarpLocationSettings.iWarpLocationBS, ciWARP_LOCATION_BITSET_PlayWarpPTFX_Start))
		PLAY_WARP_ENTITY_TO_WARP_LOCATION_PTFX(vVehWarpPos, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].sWarpLocationSettings.sEntityWarpPTFX, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].sWarpLocationSettings.iWarpLocationBS, ciWARP_LOCATION_BITSET_PlayWarpPTFX_End))
		SET_ENTITY_COORDS(sVehState.vehIndex, vVehWarpPos, FALSE, TRUE)
		SET_ENTITY_HEADING(sVehState.vehIndex, fVehWarpHead)
		FMMC_SET_LONG_BIT(iVehWarpedBS, sVehState.iIndex)
		BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, iEventVehWarpedBS, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULt, DEFAULT, TRUE, sVehState.iIndex)
	ELSE
		#IF IS_DEBUG_BUILD
		ASSERTLN("[LM][Warp_location] - WARP_VEHICLE_TO_WARP_LOCATION - NULL VECTOR POSITION GIVEN. Check the Creator Data. ")
		ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_IGNORE, ENTITY_RUNTIME_ERROR_TYPE_CRITICAL_VEH, "Set up to warp on rule to a NULL/ZERO vector!", sVehState.iIndex)
		#ENDIF
		FMMC_SET_LONG_BIT(iVehWarpedBS, sVehState.iIndex)
		BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, iEventVehWarpedBS, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULt, DEFAULT, TRUE, sVehState.iIndex)
	ENDIF
ENDPROC

FUNC BOOL CAN_VEHICLE_PROCESS_WARP_LOCATION(FMMC_VEHICLE_STATE &sVehState)
	IF NOT sVehState.bExists
	OR NOT sVehState.bDrivable
		PRINTLN("[Warp_location][Vehicles][Vehicle ", sVehState.iIndex, "] - CAN_VEHICLE_PROCESS_WARP_LOCATION - Has been set to warp on this rule but the vehicle does not exist or is destroyed...")
		RETURN FALSE
	ENDIF
	
	IF NOT sVehState.bHasControl
		PRINTLN("[Warp_location][Vehicles][Vehicle ", sVehState.iIndex, "] - CAN_VEHICLE_PROCESS_WARP_LOCATION - Has been set to warp on this rule but we don't have control over it.")
		RETURN FALSE
	ENDIF
		
	IF NOT READY_TO_PROCESS_ENTITY_WARP()
		PRINTLN("[Warp_location][Vehicles][Vehicle ", sVehState.iIndex, "] - CAN_VEHICLE_PROCESS_WARP_LOCATION - We should perform a warp, but we're not ready to process it yet.")
		RETURN FALSE
	ENDIF	
	RETURN TRUE
ENDFUNC

PROC PROCESS_VEHICLE_ON_RULE_WARP(FMMC_VEHICLE_STATE &sVehState)
		
	IF NOT SHOULD_ENTITY_PERFORM_A_RULE_WARP_LOCATION_WARP(CREATION_TYPE_VEHICLES, sVehState.iIndex)
		EXIT
	ENDIF
	
	IF NOT CAN_VEHICLE_PROCESS_WARP_LOCATION(sVehState)
		EXIT
	ENDIF
		
	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule =	MC_serverBD_4.iCurrentHighestPriority[iTeam] 
	
	FLOAT fVehWarpHead
	VECTOR vVehWarpPos
	
	GET_ENTITY_WARP_LOCATION_ON_THIS_RULE_START(iRule, iTeam, CREATION_TYPE_VEHICLES, sVehState.iIndex, vVehWarpPos, fVehWarpHead)
		
	WARP_VEHICLE_TO_WARP_LOCATION(sVehState, vVehWarpPos, fVehWarpHead, iVehicleWarpedOnThisRule, g_ciInstancedcontentEventType_WarpedVehicleOnRuleStart)
	
ENDPROC

PROC PROCESS_VEHICLE_ON_DAMAGE_WARP(FMMC_VEHICLE_STATE &sVehState)
	
	IF NOT SHOULD_ENTITY_PERFORM_AN_ON_DAMAGE_LOCATION_WARP(CREATION_TYPE_VEHICLES, sVehState.iIndex)
		EXIT
	ENDIF
	
	IF NOT CAN_VEHICLE_PROCESS_WARP_LOCATION(sVehState)
		EXIT
	ENDIF

	FLOAT fVehWarpHead
	VECTOR vVehWarpPos
	
	GET_ENTITY_WARP_LOCATION_ON_DAMAGED(CREATION_TYPE_VEHICLES, sVehState.iIndex, vVehWarpPos, fVehWarpHead)
		
	WARP_VEHICLE_TO_WARP_LOCATION(sVehState, vVehWarpPos, fVehWarpHead, iVehicleWarpedOnDamage, g_ciInstancedcontentEventType_WarpedVehicleOnDamageStart)
	
ENDPROC

FUNC ENTITY_INDEX GET_WHATEVER_ATTACHED_TO_CARGOBOB(VEHICLE_INDEX thisVeh, INT entityType)
	IF entityType = ciRULE_TYPE_VEHICLE
		RETURN GET_VEHICLE_ATTACHED_TO_CARGOBOB(thisVeh) 
	ELSE
		RETURN GET_ENTITY_ATTACHED_TO_CARGOBOB(thisVeh)
	ENDIF
ENDFUNC

PROC CONVERT_AMBIENT_VEHICLE_TO_MISSION_ENTITY(INT iVeh)

	IF IS_BIT_SET(MC_serverBD.iAmbientOverrideVehicle,iVeh)
		IF NOT IS_BIT_SET(MC_serverBD.iAmbientOverrideVehicleSetup,iVeh)
			IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])

					VEHICLE_INDEX tempVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
					
					CLEANUP_NET_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
					
					SET_ENTITY_AS_MISSION_ENTITY(tempVeh,TRUE,TRUE)
					
					NETWORK_INDEX tempNet = VEH_TO_NET(tempVeh)
					
					MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh] = tempNet
					
					SET_NETWORK_ID_EXISTS_ON_ALL_MACHINES(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh] , TRUE)	
					SET_BIT(MC_serverBD.iAmbientOverrideVehicleSetup,iVeh)
					
					SET_VEHICLE_HANDBRAKE(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh]), FALSE)
					PRINTLN("[RCC MISSION] CONVERT_AMBIENT_VEHICLE_TO_MISSION_ENTITY - setting up ambient vehicle: ", iveh, " turned off handbrake, time ", GET_CLOUD_TIME_AS_INT())
				ELSE
					NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

FUNC BOOL HAS_VEH_RESPAWN_DELAY_EXPIRED(INT iveh)
	
	BOOL bExpired

	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iRespawnDelay != 0
	AND (IS_BIT_SET(MC_serverBD_2.iVehSpawnedOnce, iveh) OR MC_ARE_SPAWN_CONDITION_FLAGS_MET_DURING_GAMEPLAY(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].eSpawnConditionFlag) OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetTen, ciFMMC_VEHICLE10_AlwaysUseRespawnDelay))
		IF NOT MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(MC_serverBD_3.iVehSpawnDelay[iveh])
			MC_REINIT_NET_TIMER_WITH_TIMESTAMP(MC_serverBD_3.iVehSpawnDelay[iveh])
		ELSE
			INT iTimeScale = 1000
			IF MC_GET_NET_TIMER_DIFFERENCE_WITH_TIME_STAMP(MC_serverBD_3.iVehSpawnDelay[iveh]) >= g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iRespawnDelay*iTimeScale
				bExpired = TRUE
			ENDIF
		ENDIF
	ELSE
		bExpired = TRUE
	ENDIF
	
	RETURN bExpired
	
ENDFUNC

FUNC BOOL IS_VEHICLE_OUT_OF_BOUNDS(VEHICLE_INDEX thisVeh, INT iVeh)
	
	UNUSED_PARAMETER(thisVeh)
	UNUSED_PARAMETER(iVeh)
	
	RETURN FALSE

ENDFUNC

FUNC BOOL IS_VEHICLE_TOUCHING_THIS_VEHICLE(VEHICLE_INDEX veh1, VEHICLE_INDEX veh2)
	VECTOR vMin
	VECTOR vMax
	GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(veh1), vMin, vMax)
	VECTOR vMinOffset = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(veh1, <<0.0, vMin.y - 2.8, vMin.z - 1.2>>)
	VECTOR vMaxOffset = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(veh1, <<0.0, vMax.y + 2.8, vMax.z + 1.2>>)
	
	IF IS_ENTITY_IN_ANGLED_AREA(veh2, vMinOffset, vMaxOffset, ((vMax.x  + 2.1) - (vMin.x - 2.1)))
		PRINTLN("[LM][IS_VEHICLE_TOUCHING_THIS_VEHICLE] - Vehicles Touching.")
		RETURN TRUE
	ENDIF
	PRINTLN("[LM][IS_VEHICLE_TOUCHING_THIS_VEHICLE] - Vehicles Not Touching.")
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_VEHICLE_RESIST_DUNE_FLIP(VEHICLE_INDEX VictimVeh)
	INT i
	FOR i = 0 TO MC_serverBD.iNumVehCreated - 1
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[i])
			IF VictimVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i])
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehBitsetFour, ciFMMC_VEHICLE4_VEHICLE_RESIST_EASY_EXPLODE)
					PRINTLN("[JT EXPLODE] RESISTING")
					RETURN TRUE
				ELSE
					PRINTLN("[JT EXPLODE] FALSE NATIVE_TO_INT(VictimVeh): ", NATIVE_TO_INT(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i])), " BIT SET?: ", IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehBitsetFour, ciFMMC_VEHICLE4_VEHICLE_RESIST_EASY_EXPLODE))
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	RETURN FALSE
ENDFUNC

FUNC INT GET_TEAM_HOLDING_VEHICLE(INT iVeh)

	INT iteam
	INT ireturnteam = -1
	
	FOR iteam = 0 to (FMMC_MAX_TEAMS-1)
		IF IS_BIT_SET(MC_serverBD.iVehAtYourHolding[iteam],iVeh)
			ireturnteam = iteam
		ENDIF
	ENDFOR
	
	RETURN ireturnteam

ENDFUNC 

FUNC BOOL ARE_ANY_TEAM_PARICIPANTS_IN_VEHICLE(INT iVeh, INT iTeamsBitset)
	INT iLoopIndex
	INT iPlayerCount = 0
	
	//Sum the number of players on all included teams.
	REPEAT FMMC_MAX_TEAMS iLoopIndex
		IF IS_BIT_SET(iTeamsBitset, iLoopIndex)
			iPlayerCount += MC_serverBD.iNumberOfPlayingPlayers[iLoopIndex]
		ENDIF
	ENDREPEAT
	
	INT iPlayersChecked = 0
	PRINTLN("[PLAYER_LOOP] - ARE_ANY_TEAM_PARICIPANTS_IN_VEHICLE")
	REPEAT GET_MAXIMUM_PLAYERS_ON_MISSION() iLoopIndex
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iLoopIndex))
			IF IS_BIT_SET(iTeamsBitset, MC_playerBD[iLoopIndex].iteam)
				//We've found a player in the vehicle. We're done.
				IF MC_playerBD[iLoopIndex].iVehNear = iVeh
					RETURN TRUE
				ENDIF
				iPlayersChecked++
				
				//We've checked as many players as there are on the teams. Early out.
				IF iPlayersChecked >= iPlayerCount
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

FUNC BOOL ARE_ALL_PARTICIPANTS_IN_OTHER_OBJECTIVE_VEHICLE(INT iVeh, VEHICLE_INDEX tempVeh)
	INT iLoopIndex
	IF GET_DIST2_BETWEEN_ENTITIES(tempVeh, LocalPlayerPed) < cfUnblipOtherEntityDistance * cfUnblipOtherEntityDistance
		CPRINTLN(DEBUG_MC_BLIP, "[RCC MISSION][VEH] Vehicle ", iveh, " ARE_ALL_PARTICIPANTS_IN_OTHER_OBJECTIVE_VEHICLE - Player is too close. Returning False")
		RETURN FALSE
	ENDIF
	
	INT iVehNear = -1
	PRINTLN("[PLAYER_LOOP] - ARE_ALL_PARTICIPANTS_IN_OTHER_OBJECTIVE_VEHICLE")
	REPEAT GET_MAXIMUM_PLAYERS_ON_MISSION() iLoopIndex


		IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iLoopIndex))
			RELOOP
		ENDIF
		IF MC_playerBD[iLoopIndex].iVehNear != -1
			IF MC_playerBD[iLoopIndex].iVehNear = iVeh
				CPRINTLN(DEBUG_MC_BLIP, "[RCC MISSION][VEH] Vehicle ", iveh, " ARE_ALL_PARTICIPANTS_IN_OTHER_OBJECTIVE_VEHICLE - Player is in this vehicle! Returning False")
				RETURN FALSE
			ENDIF
			IF iVehNear != -1
				IF iVehNear != MC_playerBD[iLoopIndex].iVehNear
					CPRINTLN(DEBUG_MC_BLIP, "[RCC MISSION][VEH] Vehicle ", iveh, " ARE_ALL_PARTICIPANTS_IN_OTHER_OBJECTIVE_VEHICLE - All players are not in the same vehicle. Returning False")
					RETURN FALSE
				ENDIF
			ELSE
				iVehNear = MC_playerBD[iLoopIndex].iVehNear
			ENDIF
		ELSE
			CPRINTLN(DEBUG_MC_BLIP, "[RCC MISSION][VEH] Vehicle ", iveh, " ARE_ALL_PARTICIPANTS_IN_OTHER_OBJECTIVE_VEHICLE - One player is not in a vehicle. Returning False")
			RETURN FALSE
		ENDIF
	ENDREPEAT
	CPRINTLN(DEBUG_MC_BLIP, "[RCC MISSION][VEH] Vehicle ", iveh, " ARE_ALL_PARTICIPANTS_IN_OTHER_OBJECTIVE_VEHICLE - Returning True")
	RETURN TRUE
ENDFUNC

FUNC BOOL ARE_ANY_PARTICIPANTS_IN_VEHICLE(INT iVeh, INT iTeamsBitset, INT &iTeamInVeh)
	
	iTeamInVeh = -1
	
	IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
		RETURN FALSE
	ENDIF
	
	VEHICLE_INDEX viVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
	
	INT i = -1
	DO_PARTICIPANT_LOOP_FLAGS eFlags = GET_PARTICIPANT_LOOP_FLAGS_FROM_VALID_TEAM_BITSET(iTeamsBitset)
	WHILE DO_PARTICIPANT_LOOP(i, eFlags | DPLF_CHECK_PED_ALIVE | DPLF_FILL_PLAYER_IDS)
		
		IF IS_PED_IN_THIS_VEHICLE(piParticipantLoop_PedIndex, viVeh)
		OR (DOES_ENTITY_EXIST(viVeh)
		AND GET_ENTITY_MODEL(viVeh) = AVENGER
		AND IS_PLAYER_IN_CREATOR_AIRCRAFT(piParticipantLoop_PlayerIndex))
			PRINTLN("[RCC MISSION] ARE_ANY_PARTICIPANTS_IN_VEHICLE - RETURNING TRUE for participant: ", i, " vehicle: ", iveh)
			iTeamInVeh = MC_playerBD[i].iTeam
			RETURN TRUE
		ENDIF
		
	ENDWHILE
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_VEHICLE_DRIVER_DEAD(VEHICLE_INDEX viVeh)

	IF NOT IS_VEHICLE_EMPTY(viVeh)
		PED_INDEX piDriver = GET_PED_IN_VEHICLE_SEAT(viVeh)
		IF DOES_ENTITY_EXIST(piDriver)
			IF IS_PED_INJURED(piDriver)
			OR IS_ENTITY_DEAD(piDriver)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC CHECK_SHOULD_VEHICLE_HAVE_CUSTOM_BLIP(FMMC_VEHICLE_STATE &sVehState)

	BOOL bBlip
	
	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	INT iVeh = sVehState.iIndex
	
	IF DOES_ENTITY_HAVE_BLIP_OVERRIDES(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sVehBlipStruct)
		bBlip = SHOULD_ENTITY_BE_BLIPPED(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sVehBlipStruct, sMissionVehiclesLocalVars[iVeh].sBlipRuntimeVars, sVehState.vehIndex)
	ENDIF
	
	IF iRule < FMMC_MAX_RULES
		// If it is flagged to keep it's blip until the rule ends keep its blip alive.
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThree[iRule], ciBS_RULE3_BLIP_AFTER_DELIVERY)
			STRING strDecorName
			SWITCH iTeam
				CASE 0	strDecorName = "MC_Team0_VehDeliveredRules"		BREAK
				CASE 1	strDecorName = "MC_Team1_VehDeliveredRules"		BREAK
				CASE 2	strDecorName = "MC_Team2_VehDeliveredRules"		BREAK
				CASE 3	strDecorName = "MC_Team3_VehDeliveredRules"		BREAK
			ENDSWITCH
			IF DECOR_IS_REGISTERED_AS_TYPE(strDecorName, DECOR_TYPE_INT)
				IF sVehState.bExists
					IF DECOR_EXIST_ON(sVehState.vehIndex, strDecorName)
						IF IS_BIT_SET(DECOR_GET_INT(sVehState.vehIndex, strDecorName), iRule)
							#IF IS_DEBUG_BUILD
								IF IS_FRAME_COUNT_STAGGER_READY(ciFRAME_STAGGERED_DEBUG_VEH)
									PRINTLN("[Blips][Veh] CHECK_SHOULD_VEHICLE_HAVE_CUSTOM_BLIP - Veh ", iVeh, " blip being kept alive by ciBS_RULE3_BLIP_AFTER_DELIVERY setting.")
								ENDIF
							#ENDIF
							bBlip = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		INT iBlipVehSetting = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehicleBlipVisibiltyRange[iRule]
		
		IF sVehState.bExists
			
			IF iBlipVehSetting = ci_VEHICLE_BLIP_RULE_ALWAYS_OFF
				bBlip = FALSE
			ELIF iBlipVehSetting = ci_VEHICLE_BLIP_RULE_ALWAYS_ON
				bBlip = TRUE
			ELIF iBlipVehSetting > ci_VEHICLE_BLIP_RULE_ALWAYS_ON
				VECTOR vVehCoord = GET_FMMC_VEHICLE_COORDS(sVehState)
				VECTOR vPlayerCoord = GET_ENTITY_COORDS(LocalPlayerPed)
			
				FLOAT fDist = ABSF(GET_DISTANCE_BETWEEN_COORDS(vVehCoord, vPlayerCoord, FALSE))
				
				IF fDist < iBlipVehSetting
					bBlip = TRUE
				ELSE 
					PRINTLN("[Blips][Veh] CHECK_SHOULD_VEHICLE_HAVE_CUSTOM_BLIP - Vehicle ", iVeh, " blip is outside of blip range ", iBlipVehSetting, ", cleaing up blip. (2)")
					bBlip = FALSE
				ENDIF
			ENDIF
		ELSE
			bBlip = FALSE
		ENDIF
	ENDIF
	
	IF SHOULD_ENTITY_BLIP_BE_FORCED_ON(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sVehBlipStruct, sVehState.vehIndex)
		bBlip = TRUE
	ENDIF
	
	IF SHOULD_ENTITY_BLIP_BE_FORCED_OFF(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sVehBlipStruct)
		bBlip = FALSE
	ENDIF

	IF bBlip
		SET_BIT(iShouldBlipVehBitSet, iVeh)
	ELSE
		CLEAR_BIT(iShouldBlipVehBitSet, iVeh)
	ENDIF
	
ENDPROC

FUNC BOOL SHOULD_VEHICLE_BLIP_BE_REMOVED(VEHICLE_INDEX tempVeh)
		
	INT iPlacedPedInVeh = GET_PLACED_PED_IN_VEHICLE(tempVeh)
	IF iPlacedPedInVeh != -1
		RETURN IS_BLIP_SET_AS_HIDDEN(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPlacedPedInVeh].sPedBlipStruct)
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

/// PURPOSE:
///    Checks if we need to clean up the vehicles blip
FUNC BOOL SHOULD_VEHICLE_BLIP_CLEANUP(INT iVeh, BLIP_RUNTIME_VARS_STRUCT &sBlipRuntimeVars, VEHICLE_INDEX tempVeh)
	
	IF SHOULD_ENTITY_BLIP_BE_FORCED_ON(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sVehBlipStruct, tempVeh)
		RETURN FALSE
	ENDIF
	
	IF SHOULD_ENTITY_BLIP_BE_FORCED_OFF(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sVehBlipStruct)
		RETURN TRUE
	ENDIF
	
	IF NOT SHOULD_ENTITY_BE_BLIPPED(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sVehBlipStruct, sBlipRuntimeVars, tempVeh, FALSE)
		IF MC_serverBD_4.iVehPriority[iVeh][MC_playerBD[iPartToUse].iTeam] != MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]
			PRINTLN("[Vehicles][Vehicle ", iVeh, "] SHOULD_VEHICLE_BLIP_CLEANUP - SHOULD_ENTITY_BE_BLIPPED returning false")
			RETURN TRUE
		ENDIF
		
		IF IS_VEHICLE_BLIP_A_CUSTOM_BLIP(iVeh)
			PRINTLN("[Vehicles][Vehicle ", iVeh, "] SHOULD_VEHICLE_BLIP_CLEANUP - SHOULD_ENTITY_BE_BLIPPED returning false")
			CLEAR_BIT(iVehicleCustomBlip, iVeh)
			RETURN TRUE
		ENDIF
	ENDIF
	
	BOOL bShouldCleanup
	
	INT iAllTeamsBS = ciALL_TEAMS_BITSET_VALUE
	INT iTeamInVehicle
	BOOL bAnyoneInVehicle = ARE_ANY_PARTICIPANTS_IN_VEHICLE(iveh, iAllTeamsBS, iTeamInVehicle)
	
	IF NOT (bAnyoneInVehicle AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetSix,ciFMMC_VEHICLE6_IGNORE_RANGE_IF_OCCUPIED))
	AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetSix,ciFMMC_VEHICLE6_BLIP_ON_MAP)
	AND NOT WAS_PLAYER_PREVIOUSLY_IN_RANGE_OF_BLIP(sBlipRuntimeVars)
			
		IF NOT IS_PLAYER_WITHIN_ENTITY_BLIP_RANGE(tempVeh, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sVehBlipStruct)
			PRINTLN("[Vehicles][Vehicle ", iVeh, "] SHOULD_VEHICLE_BLIP_CLEANUP - is out of blipping range")
			bShouldCleanup = TRUE
		ENDIF
		IF NOT IS_ENTITY_WITHIN_HEIGHT_DIFFERENCE_RANGE_FOR_BLIP(tempVeh, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sVehBlipStruct)
			PRINTLN("[Vehicles][Vehicle ", iVeh, "] SHOULD_VEHICLE_BLIP_CLEANUP - is out of height range")
			bShouldCleanup = TRUE
		ENDIF
		
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFour, ciFMMC_VEHICLE4_BLIP_OVERRIDE_HIDE_WHEN_IN_VEH_T0 + MC_playerBD[iPartToUse].iteam)
	AND SHOULD_VEHICLE_HAVE_CUSTOM_BLIP(iVeh)
		IF IS_PED_IN_VEHICLE(PlayerPedToUse, tempVeh)
		OR IS_PLAYER_IN_CREATOR_AIRCRAFT(PlayerToUse)
			bShouldCleanup = TRUE
		ELIF GET_ENTITY_MODEL(tempVeh) = AVENGER
			IF IS_PLAYER_IN_CREATOR_AIRCRAFT(PlayerToUse)
				bShouldCleanup = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_THIS_MODEL_A_HELI(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn)
		IF IS_VEHICLE_EMPTY_AND_STATIONARY(tempVeh)
		AND (GET_BLIP_SPRITE(biVehBlip[iveh]) = RADAR_TRACE_ENEMY_HELI_SPIN OR GET_BLIP_SPRITE(biVehBlip[iveh]) = RADAR_TRACE_POLICE_HELI_SPIN)
			bShouldCleanup = TRUE
		ENDIF
	ENDIF
	
	IF (GET_ENTITY_MODEL(tempVeh) = TRAILERLARGE
	OR GET_ENTITY_MODEL(tempVeh) = HAULER2
	OR GET_ENTITY_MODEL(tempVeh) = AVENGER)
	AND IS_BIT_SET(iLocalBoolCheck24, LBOOL24_IN_CREATOR_TRAILER)
		bShouldCleanup = TRUE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetSix, ciFMMC_VEHICLE6_DONT_BLIP_WITH_TRAILER)
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			VEHICLE_INDEX viTempVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
			IF DOES_ENTITY_EXIST(viTempVeh)
				VEHICLE_INDEX viTrailer
				IF GET_VEHICLE_TRAILER_VEHICLE(viTempVeh, viTrailer)
				OR IS_VEHICLE_A_TRAILER(viTempVeh)
					bShouldCleanup = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	INT iRule = MC_ServerBD_4.iCurrentHighestPriority[MC_PlayerBD[iPartToUse].iTeam]
	IF iRule < FMMC_MAX_RULES
		INT iBlipVehSetting = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehicleBlipVisibiltyRange[MC_ServerBD_4.iCurrentHighestPriority[MC_PlayerBD[iPartToUse].iTeam]]
		
		IF iBlipVehSetting = ci_VEHICLE_BLIP_RULE_ALWAYS_OFF
			bShouldCleanup = TRUE
		ELIF iBlipVehSetting = ci_VEHICLE_BLIP_RULE_ALWAYS_ON
			bShouldCleanup = FALSE
		ELIF iBlipVehSetting > ci_VEHICLE_BLIP_RULE_ALWAYS_ON
			VECTOR vVehCoord = GET_ENTITY_COORDS(tempVeh)
			VECTOR vPlayerCoord = GET_ENTITY_COORDS(LocalPlayerPed)
		
			FLOAT fDist = ABSF(GET_DISTANCE_BETWEEN_COORDS(vVehCoord, vPlayerCoord, FALSE))
			
			IF fDist < iBlipVehSetting
				bShouldCleanup = TRUE
			ELSE 
				PRINTLN("[Vehicles][Vehicle ", iVeh, "] SHOULD_VEHICLE_BLIP_CLEANUP - blip is outside of blip range ", iBlipVehSetting, ", cleaing up blip.")
				bShouldCleanup = FALSE
			ENDIF
		ENDIF

		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFive, ciFMMC_VEHICLE5_HACKING_USE_DISTANCE_FOR_CAPTURE)	
		AND MC_serverBD_4.ivehRule[iVeh][MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_CAPTURE
		AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitsetEleven[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE11_SECUROHACK_REQUIRES_KILL)
			IF MC_serverBD_4.iHackingTargetsRemaining > 0
				IF MC_PlayerBD[iPartToUse].iVehDestroyBitset != 0
					IF NOT IS_BIT_SET(MC_PlayerBD[iPartToUse].iVehDestroyBitset, iVeh)
						bShouldCleanup = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF (NOT bShouldCleanup)
		AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitsetEleven[iRule], ciBS_RULE11_UNBLIP_OTHER_ENTITY)
		AND ARE_ALL_PARTICIPANTS_IN_OTHER_OBJECTIVE_VEHICLE(iveh, tempVeh)
			bShouldCleanup = TRUE
			PRINTLN("[Vehicles][Vehicle ", iVeh, "] SHOULD_VEHICLE_BLIP_CLEANUP - ciBS_RULE11_UNBLIP_OTHER_ENTITY unblipping")
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetTen, ciFMMC_VEHICLE10_RemoveBlipDeadDriver)
		AND IS_VEHICLE_DRIVER_DEAD(tempVeh)
			bShouldCleanup = TRUE
			PRINTLN("[Vehicles][Vehicle ", iVeh, "] SHOULD_VEHICLE_BLIP_CLEANUP - Driver is dead")
		ENDIF
	ENDIF
	
	RETURN bShouldCleanup
	
ENDFUNC

FUNC BOOL SHOULD_DEAD_VEHICLE_CHECK_TEAM_FAILS(INT iVeh)
	
	PRINTLN("[Vehicles][Vehicle ", iVeh, "] PROCESS_VEH_BRAIN - SHOULD_DEAD_VEHICLE_CHECK_TEAM_FAILS - Vehicle GET_VEHICLE_RESPAWNS: ", GET_VEHICLE_RESPAWNS(iVeh), " iRespawnOnRuleChangeBS: ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iRespawnOnRuleChangeBS)
	
	IF GET_VEHICLE_RESPAWNS(iVeh) <= 0 
		IF NOT IS_BIT_SET(MC_serverBD_1.sCreatedCount.iSkipVehBitset,iveh)
			RETURN TRUE
		ELSE
			PRINTLN("[Vehicles][Vehicle ", iVeh, "] PROCESS_VEH_BRAIN - SHOULD_DEAD_VEHICLE_CHECK_TEAM_FAILS - Vehicle iSkipVehBitset skip set...")
		ENDIF
	ENDIF
	
	IF SHOULD_ENTITY_RESPAWN_THIS_RULE(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iRespawnOnRuleChangeBS, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iRespawnOnRule, -1)
		IF NOT IS_BIT_SET(MC_ServerBD.iServerBS_PassRuleVehAfterRespawn, iVeh)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL SHOULD_VEHICLE_COUNT_AS_HELD_FOR_TEAM(INT iveh,INT iteam)
	IF MC_serverBD_4.ivehRule[iveh][iteam] = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER
	OR MC_serverBD_4.ivehRule[iveh][iteam] = FMMC_OBJECTIVE_LOGIC_CAPTURE
	OR MC_serverBD_4.ivehRule[iveh][iteam] = FMMC_OBJECTIVE_LOGIC_GET_AND_HOLD
		IF MC_serverBD_4.iVehPriority[iveh][iteam] <= MC_serverBD_4.iCurrentHighestPriority[iteam]
		AND ((MC_serverBD_4.ivehRule[iveh][iteam] != FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER) OR (NOT IS_VEHICLE_WAITING_FOR_PLAYERS(iVeh,iTeam)))
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//[FMMC2020] Something to delete?
PROC PROCESS_LOCK_PEGASUS_VEHICLE()
	IF g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iPegasusLockRule > -1
	AND g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iPegasusLockRule <= MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iTeam]
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(LocalPlayer)].netID_PegV)
			VEHICLE_INDEX viPegasus = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(LocalPlayer)].netID_PegV)
			IF DOES_ENTITY_EXIST(viPegasus)
				IF IS_VEHICLE_EMPTY(viPegasus, TRUE, TRUE)
					IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(LocalPlayer)].netID_PegV)
						SET_VEHICLE_DOORS_SHUT(viPegasus, TRUE)
						SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(viPegasus, TRUE)
						SET_VEHICLE_UNDRIVEABLE(viPegasus, TRUE)
					ELSE
						NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(LocalPlayer)].netID_PegV)
					ENDIF
				ENDIF
			ENDIF
		ENDIF 
	ENDIF
ENDPROC

PROC CLEANUP_COLLISION_ON_VEH(VEHICLE_INDEX tempVeh, INT iVeh)

	IF DOES_ENTITY_EXIST(tempVeh)
		IF NOT IS_ENTITY_DEAD(tempVeh)
			IF NETWORK_HAS_CONTROL_OF_ENTITY(tempVeh)
				SET_ENTITY_LOAD_COLLISION_FLAG(tempVeh, FALSE)
			ENDIF
		ENDIF
	ENDIF
	IF IS_BIT_SET(iVehCollisionBitset,iVeh)
		IF iCollisionStreamingLimit > 0
			iCollisionStreamingLimit--
		ENDIF
		CLEAR_BIT(iVehCollisionBitset,iVeh)
		PRINTLN("[LOAD_COLLISION_FLAG] Cleanup for veh: ",iVeh, " current requests: ",iCollisionStreamingLimit)
	ENDIF

ENDPROC

//PURPOSE: Removes the load collision settings for planes/helis that are in the air and no longer need it for Take Off.
PROC MAINTAIN_VEH_COLLISION_CLEANUP(FMMC_VEHICLE_STATE &sVehState)
	
	IF IS_BIT_SET(iVehCollisionBitset, sVehState.iIndex)
		IF IS_ENTITY_IN_AIR(sVehState.vehIndex)
			IF sVehState.bHasControl
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(sVehState.vehIndex, FALSE)
			ENDIF
			
			CLEANUP_COLLISION_ON_VEH(sVehState.vehIndex, sVehState.iIndex)
			PRINTLN("VEHICLE IN AIR - CLEANUP COLLISION AND SET CLEANUP_COLLISION_ON_VEH FALSE -  veh: ", sVehState.iIndex)
		ENDIF
	ENDIF
	
ENDPROC


				
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Zones
// ##### Description: Vehicle logic based on zone interaction
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL IS_ANYONE_IN_ZONE_FOR_VEHICLE(INT iZoneBitset)
	
	IF iZoneBitset = 0
		PRINTLN("IS_ANYONE_IN_ZONE_FOR_VEHICLE F1")
		RETURN FALSE
	ENDIF
	
	INT iZone
	FOR iZone = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfZones-1
		IF IS_BIT_SET(iZoneBitset, iZone)
		AND GET_NUM_PLAYERS_TRIGGERING_ZONE(iZone) > 0
			PRINTLN("IS_ANYONE_IN_ZONE_FOR_VEHICLE T")
			RETURN TRUE
		ELSE
			PRINTLN("IS_ANYONE_IN_ZONE_FOR_VEHICLE - ",IS_BIT_SET(iZoneBitset, iZone), " ", GET_NUM_PLAYERS_TRIGGERING_ZONE(iZone))
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC
				
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Immovable
// ##### Description: Functions for controlling immovable state of the vehicle. invincible and frozen.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL SHOULD_VEHICLE_IMMOVABLE_RUN(FMMC_VEHICLE_STATE &sVehState)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iImmovableViaZone_BS != 0
		RETURN TRUE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iImmovableTeam != -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iImmovableRule != -1
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL SHOULD_VEHICLE_IMMOVABLE_STATE_CHANGE(FMMC_VEHICLE_STATE &sVehState)
	
	IF IS_ENTITY_IN_AIR(sVehState.vehIndex)
		RETURN FALSE
	ENDIF
	
	IF IS_ANYONE_IN_ZONE_FOR_VEHICLE(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iImmovableViaZone_BS)
		RETURN TRUE
	ENDIF
	
	IF IS_TEAM_VALID(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iImmovableTeam)
	AND GET_TEAM_CURRENT_RULE(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iImmovableTeam) >= g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iImmovableRule
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SET_VEHICLE_IMMOVABLE(FMMC_VEHICLE_STATE &sVehState, BOOL bHasChanged)
	IF NOT IS_BIT_SET(MC_serverBD_4.iVehicleImmovableBitset, sVehState.iIndex)
		SET_ENTITY_INVINCIBLE(sVehState.vehIndex, TRUE)
		FREEZE_ENTITY_POSITION(sVehState.vehIndex, TRUE)
		BROADCAST_FMMC_SET_VEHICLE_IMMOVABLE(sVehState.iIndex, TRUE, bHasChanged)
		PRINTLN("PROCESS_VEHICLE_IMMOVABLE - Making Vehicle ", sVehState.iIndex, " immovable")
	ENDIF
ENDPROC

PROC CLEAR_VEHICLE_IMMOVABLE(FMMC_VEHICLE_STATE &sVehState, BOOL bHasChanged)
	IF IS_BIT_SET(MC_serverBD_4.iVehicleImmovableBitset, sVehState.iIndex)
		SET_ENTITY_INVINCIBLE(sVehState.vehIndex, FALSE)
		FREEZE_ENTITY_POSITION(sVehState.vehIndex, FALSE)
		BROADCAST_FMMC_SET_VEHICLE_IMMOVABLE(sVehState.iIndex, FALSE, bHasChanged)
		PRINTLN("PROCESS_VEHICLE_IMMOVABLE - Removing Vehicle ", sVehState.iIndex, " immovable")
	ENDIF
ENDPROC

PROC PROCESS_VEHICLE_IMMOVABLE(FMMC_VEHICLE_STATE &sVehState)

	IF NOT SHOULD_VEHICLE_IMMOVABLE_RUN(sVehState)
		EXIT
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetNine, ciFMMC_VEHICLE9_ChangeImmovableOnlyOnce)
	AND IS_BIT_SET(MC_serverBD_4.iVehicleHadImmovableSetBitset, sVehState.iIndex)
		PRINTLN("[Vehicle ", sVehState.iIndex,"] - PROCESS_VEHICLE_IMMOVABLE - ciFMMC_VEHICLE9_ChangeImmovableOnlyOnce is set and immovable state has been changed.")
		EXIT
	ENDIF
	
	IF NOT SHOULD_VEHICLE_IMMOVABLE_STATE_CHANGE(sVehState)
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetNine, ciFMMC_VEHICLE9_StartAsImmovable)
			SET_VEHICLE_IMMOVABLE(sVehState, FALSE)
		ELSE
			CLEAR_VEHICLE_IMMOVABLE(sVehState, FALSE)
		ENDIF
		
		EXIT
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetNine, ciFMMC_VEHICLE9_StartAsImmovable)
		CLEAR_VEHICLE_IMMOVABLE(sVehState, TRUE)
	ELSE
		SET_VEHICLE_IMMOVABLE(sVehState, TRUE)
	ENDIF
	
ENDPROC



// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Health Threshold
// ##### Description: Functions for vehicle behaviour on a health threshold
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL DOES_VEHICLE_BODY_HEALTH_MEET_HEALTH_THRESHOLD(FMMC_VEHICLE_STATE &sVehState)
	RETURN GET_FMMC_VEHICLE_CURRENT_BODY_HEALTH(sVehState) <= g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].sHealthThreshold.iBodyHealth
ENDFUNC

FUNC BOOL DOES_VEHICLE_HAVE_A_BODY_HEALTH_THRESHOLD(FMMC_VEHICLE_STATE &sVehState)
	RETURN g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].sHealthThreshold.iBodyHealth > 0
ENDFUNC

FUNC BOOL SHOULD_VEHICLE_HAVE_PROOFS_SET_ON_HEALTH_THRESHOLD(FMMC_VEHICLE_STATE &sVehState)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].sHealthThreshold.iThresholdBitset, ciFMMC_VHT_BULLET_PROOF)
	OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].sHealthThreshold.iThresholdBitset, ciFMMC_VHT_FLAME_PROOF)
	OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].sHealthThreshold.iThresholdBitset, ciFMMC_VHT_EXPLOSION_PROOF)
	OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].sHealthThreshold.iThresholdBitset, ciFMMC_VHT_COLLISION_PROOF)
	OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].sHealthThreshold.iThresholdBitset, ciFMMC_VHT_MELEE_PROOF)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL SHOULD_VEHICLE_HEALTH_THRESHOLD_END(FMMC_VEHICLE_STATE &sVehState)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].sHealthThreshold.iEndRule != -1
		IF GET_TEAM_CURRENT_RULE(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].sHealthThreshold.iTeamToCheck) >= g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].sHealthThreshold.iEndRule
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC PROCESS_VEHICLE_HEALTH_THRESHOLD_CLEANUP(FMMC_VEHICLE_STATE &sVehState)
	
	IF SHOULD_VEHICLE_HAVE_PROOFS_SET_ON_HEALTH_THRESHOLD(sVehState)
		BOOL bBulletProof = IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetSix, ciFMMC_VEHICLE6_SET_VEHICLE_AS_BULLET_PROOF)
		BOOL bFireProof = SHOULD_VEHICLE_BE_FIRE_PROOF(sVehState.vehIndex, sVehState.iIndex)
		SET_ENTITY_PROOFS(sVehState.vehIndex, bBulletProof, bFireProof, FALSE, FALSE, FALSE)
	ENDIF
	
ENDPROC

PROC PROCESS_VEHICLE_HEALTH_THRESHOLD(FMMC_VEHICLE_STATE &sVehState)

	IF NOT sVehState.bAlive
		EXIT
	ENDIF
	
	IF NOT DOES_VEHICLE_HAVE_A_BODY_HEALTH_THRESHOLD(sVehState)
		EXIT
	ENDIF
	
	IF IS_BIT_SET(iVehBS_HealthThresholdEnded, sVehState.iIndex)
		EXIT
	ENDIF
	
	IF SHOULD_VEHICLE_HEALTH_THRESHOLD_END(sVehState)
		PROCESS_VEHICLE_HEALTH_THRESHOLD_CLEANUP(sVehState)
		SET_BIT(iVehBS_HealthThresholdEnded, sVehState.iIndex)
		PRINTLN("[Vehicle ",sVehState.iIndex,"] PROCESS_VEHICLE_HEALTH_THRESHOLD - Health threshold processing end met")
		EXIT
	ENDIF
	
	IF IS_BIT_SET(iVehBS_HealthThresholdMet, sVehState.iIndex)
		EXIT
	ENDIF
	
	IF NOT DOES_VEHICLE_BODY_HEALTH_MEET_HEALTH_THRESHOLD(sVehState)
		EXIT
	ENDIF
	
	SET_BIT(iVehBS_HealthThresholdMet, sVehState.iIndex)
	PRINTLN("[Vehicle ",sVehState.iIndex,"] PROCESS_VEHICLE_HEALTH_THRESHOLD - Health threshold met")
	
	IF NOT sVehState.bHasControl
		EXIT
	ENDIF
	
	IF SHOULD_VEHICLE_HAVE_PROOFS_SET_ON_HEALTH_THRESHOLD(sVehState)
		BOOL bBulletProof		= IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].sHealthThreshold.iThresholdBitset, ciFMMC_VHT_BULLET_PROOF)
		BOOL bFlameProof		= IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].sHealthThreshold.iThresholdBitset, ciFMMC_VHT_FLAME_PROOF)
		BOOL bExplosionProof	= IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].sHealthThreshold.iThresholdBitset, ciFMMC_VHT_EXPLOSION_PROOF)
		BOOL bCollisionProof	= IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].sHealthThreshold.iThresholdBitset, ciFMMC_VHT_COLLISION_PROOF)
		BOOL bMeleeProof		= IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].sHealthThreshold.iThresholdBitset, ciFMMC_VHT_MELEE_PROOF)
		SET_ENTITY_PROOFS(sVehState.vehIndex, bBulletProof, bFlameProof, bExplosionProof, bCollisionProof, bMeleeProof)
		PRINTLN("[Vehicle ",sVehState.iIndex,"] PROCESS_VEHICLE_HEALTH_THRESHOLD - Entity proofs set!")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].sHealthThreshold.iThresholdBitset, ciFMMC_VHT_MATCH_HEALTH)
		SET_VEHICLE_BODY_HEALTH(sVehState.vehIndex, TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].sHealthThreshold.iBodyHealth))
		PRINTLN("[Vehicle ",sVehState.iIndex,"] PROCESS_VEHICLE_HEALTH_THRESHOLD - Restoring health to threshold amount!")
	ENDIF
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Blips  ----------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Functions related to vehicle blips.  -----------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC SET_SECONDARY_COLOUR_FOR_VEHICLE_BLIP_BASED_ON_TEAM(INT& iSColourTeam, INT iSetToValue, INT iCallNumber)
	PRINTLN("[RCC MISSION] SET_SECONDARY_COLOUR_FOR_VEHICLE_BLIP_BASED_ON_TEAM - Call number: ",iCallNumber,". Setting secondary blip colour to: ", iSetToValue)
	iSColourTeam = iSetToValue
ENDPROC

FUNC BOOL SHOULD_VEHICLE_HAVE_BLIP(FMMC_VEHICLE_STATE &sVehState, BLIP_RUNTIME_VARS_STRUCT &sBlipRuntimeVars, INT& iPColour, INT& iSColourTeam) //, BOOL& bFlashing)
	
	SET_SECONDARY_COLOUR_FOR_VEHICLE_BLIP_BASED_ON_TEAM(iSColourTeam, -1, 1)
	
	IF IS_LOCAL_PLAYER_IN_FMMC_VEHICLE(sVehState)
		RETURN FALSE
	ENDIF
	
	BOOL bBlip = FALSE
	
	IF NOT SHOULD_ENTITY_BLIP_BE_FORCED_ON(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].sVehBlipStruct, sVehState.vehIndex)
		IF NOT SHOULD_ENTITY_BE_BLIPPED(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].sVehBlipStruct, sBlipRuntimeVars, sVehState.vehIndex, FALSE)
			bBlip = FALSE
		ENDIF
	ELSE
		bBlip = TRUE
	ENDIF
	
	IF SHOULD_ENTITY_BLIP_BE_FORCED_OFF(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].sVehBlipStruct)
		RETURN FALSE
	ENDIF
	
	iPColour = BLIP_COLOUR_BLUEDARK
	
	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	INT iAllTeamsBS = ciALL_TEAMS_BITSET_VALUE
	INT iTeamInVehicle
	BOOL bAnyoneInVehicle = ARE_ANY_PARTICIPANTS_IN_VEHICLE(sVehState.iIndex, iAllTeamsBS, iTeamInVehicle)
	BOOL bCustomBlip = SHOULD_VEHICLE_HAVE_CUSTOM_BLIP(sVehState.iIndex)
	
	PRINTLN("[Blips][Vehicles][Vehicle ", sVehState.iIndex, "] SHOULD_VEHICLE_HAVE_BLIP - bAnyoneInVehicle: ", BOOL_TO_STRING(bAnyoneInVehicle), " bCustomBlip: ", BOOL_TO_STRING(bCustomBlip))
	
	IF iRule < FMMC_MAX_RULES
		IF MC_serverBD_4.iVehPriority[sVehState.iIndex][iTeam] <= iRule
		AND MC_serverBD_4.iVehPriority[sVehState.iIndex][iTeam] < FMMC_MAX_RULES
		
			IF NOT IS_OBJECTIVE_BLOCKED()
			AND NOT IS_BLIP_SET_AS_HIDDEN(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].sVehBlipStruct)
			AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitset[iRule], ciBS_RULE_ENABLETRACKIFY)
			
				BOOL bBlip2 = TRUE
				
				IF MC_serverBD_4.ivehRule[sVehState.iIndex][iTeam] = FMMC_OBJECTIVE_LOGIC_GET_AND_HOLD
				AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFour[iRule] ,ciBS_RULE4_ALWAYS_BLIP_DELIVERY_POINT)
					
					PRINTLN("[Blips][Vehicles][Vehicle ", sVehState.iIndex, "] SHOULD_VEHICLE_HAVE_BLIP - ciBS_RULE4_ALWAYS_BLIP_DELIVERY_POINT is set using custom blip logic")
					IF GET_TEAM_HOLDING_VEHICLE(sVehState.iIndex) > -1
						SET_SECONDARY_COLOUR_FOR_VEHICLE_BLIP_BASED_ON_TEAM(iSColourTeam, GET_TEAM_HOLDING_VEHICLE(sVehState.iIndex), 2)
					ELSE
						IF MC_serverBD.iDriverPart[sVehState.iIndex] != -1
							SET_SECONDARY_COLOUR_FOR_VEHICLE_BLIP_BASED_ON_TEAM(iSColourTeam, MC_playerBD[MC_serverBD.iDriverPart[sVehState.iIndex]].iteam, 3)
						ENDIF
					ENDIF
					
					bBlip = TRUE
					iPColour = BLIP_COLOUR_BLUEDARK
					
				ELSE
					IF ((MC_serverBD_4.ivehRule[sVehState.iIndex][iTeam] = FMMC_OBJECTIVE_LOGIC_GO_TO
					OR MC_serverBD_4.ivehRule[sVehState.iIndex][iTeam] = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER)
					AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThree[iRule], ciBS_RULE3_ALL_IN_SEPARATE_VEHICLES))
					OR bCustomBlip
						
						IF bCustomBlip
							IF NOT bAnyoneInVehicle
								PRINTLN("[Blips][Vehicles][Vehicle ", sVehState.iIndex, "] SHOULD_VEHICLE_HAVE_BLIP - There are no players in vehicle ", sVehState.iIndex, ". Cancelling blip.")
								bBlip2 = FALSE
							ENDIF
						ELSE
							BOOL bCheckOnRule
							INT iTeamsBS = GET_VEHICLE_GOTO_TEAMS(iTeam, sVehState.iIndex, bCheckOnRule)
							// If any participant on a team involved in the rule is in this vehicle we don't want to blip it.
							IF ARE_ANY_TEAM_PARICIPANTS_IN_VEHICLE(sVehState.iIndex, iTeamsBS)
								PRINTLN("[Blips][Vehicles][Vehicle ", sVehState.iIndex, "] SHOULD_VEHICLE_HAVE_BLIP - A participant on the rule is in vehicle ", sVehState.iIndex, ". Cancelling blip.")
								bBlip2 = FALSE
							ENDIF
						ENDIF
					ENDIF
					
					
					IF bBlip2
					AND NOT IS_LOCAL_PLAYER_IN_FMMC_VEHICLE(sVehState)
						IF MC_serverBD_4.ivehRule[sVehState.iIndex][iTeam] = FMMC_OBJECTIVE_LOGIC_LEAVE_ENTITY
						
							PRINTLN("[Blips][Vehicles][Vehicle ", sVehState.iIndex, "] SHOULD_VEHICLE_HAVE_BLIP - Vehicle ", sVehState.iIndex, " adding blip for Leave Entity rule.")
							bBlip = TRUE
							iPColour = BLIP_COLOUR_BLUEDARK
							
						ELIF MC_serverBD_4.ivehRule[sVehState.iIndex][iTeam] = FMMC_OBJECTIVE_LOGIC_KILL	
						OR MC_serverBD_4.ivehRule[sVehState.iIndex][iTeam] = FMMC_OBJECTIVE_LOGIC_DAMAGE
						
							PRINTLN("[Blips][Vehicles][Vehicle ", sVehState.iIndex, "] SHOULD_VEHICLE_HAVE_BLIP - Vehicle ", sVehState.iIndex, " adding blip for KILL rule.")
							bBlip = TRUE
							iPColour = BLIP_COLOUR_RED
							
						ELSE
							IF (MC_serverBD_4.ivehRule[sVehState.iIndex][iTeam] != FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER
							OR iDeliveryVehForcingOut != sVehState.iIndex // If we're doing a get & deliver (COLLECT) rule, then don't re-blip this vehicle if we're just getting forced out of it before dropping off
							OR bCustomBlip)
							AND NOT IS_PLAYER_IN_CREATOR_AIRCRAFT(PlayerToUse)
								PRINTLN("[Blips][Vehicles][Vehicle ", sVehState.iIndex, "] SHOULD_VEHICLE_HAVE_BLIP - Vehicle ", sVehState.iIndex, " adding blip for COLLECT&HOLD rule. MC_serverBD.iDriverPart: ", MC_serverBD.iDriverPart[sVehState.iIndex])
								
								bBlip = TRUE
								iPColour = BLIP_COLOUR_BLUEDARK

								IF GET_TEAM_HOLDING_VEHICLE(sVehState.iIndex) > -1
									SET_SECONDARY_COLOUR_FOR_VEHICLE_BLIP_BASED_ON_TEAM(iSColourTeam, GET_TEAM_HOLDING_VEHICLE(sVehState.iIndex), 4)
									PRINTLN("[Blips][Vehicles][Vehicle ", sVehState.iIndex, "] SHOULD_VEHICLE_HAVE_BLIP - Setting iSColourTeam to ", iSColourTeam)
								ELSE
								
									BOOL bDriverSeatFree = IS_FMMC_VEHICLE_DRIVERS_SEAT_FREE(sVehState)
								
									IF MC_serverBD.iDriverPart[sVehState.iIndex] != -1
									AND NOT bDriverSeatFree
									AND bAnyoneInVehicle
										SET_SECONDARY_COLOUR_FOR_VEHICLE_BLIP_BASED_ON_TEAM(iSColourTeam, MC_playerBD[MC_serverBD.iDriverPart[sVehState.iIndex]].iteam, 5)
										PRINTLN("[Blips][Vehicles][Vehicle ", sVehState.iIndex, "] SHOULD_VEHICLE_HAVE_BLIP - Set flashing and 2nd colour of team (", iSColourTeam, ") MC_serverBD.iDriverPart[sVehState.iIndex] : ", MC_serverBD.iDriverPart[sVehState.iIndex])
									ELIF (bCustomBlip OR IS_BLIP_SPRITE_OVERRIDE_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].sVehBlipStruct))
									AND bAnyoneInVehicle
									AND NOT bDriverSeatFree
										PLAYER_INDEX piInVeh = GET_DRIVER_OF_CAR_AS_PLAYER(sVehState.vehIndex)
										IF NETWORK_IS_PLAYER_A_PARTICIPANT(piInVeh)
											SET_SECONDARY_COLOUR_FOR_VEHICLE_BLIP_BASED_ON_TEAM(iSColourTeam, MC_playerBD[NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(piInVeh))].iteam, 6)
										ENDIF
									ELIF IS_VEHICLE_A_TRAILER(sVehState.vehIndex)
									AND NOT IS_ENTITY_DEAD(GET_ENTITY_ATTACHED_TO(sVehState.vehIndex))
										VEHICLE_INDEX truckVeh = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_ATTACHED_TO(sVehState.vehIndex))
										PLAYER_INDEX piInVeh = GET_DRIVER_OF_CAR_AS_PLAYER(truckVeh)
										IF NETWORK_IS_PLAYER_A_PARTICIPANT(piInVeh)
											SET_SECONDARY_COLOUR_FOR_VEHICLE_BLIP_BASED_ON_TEAM(iSColourTeam, MC_playerBD[NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(piInVeh))].iteam, 7)
										ENDIF
									#IF IS_DEBUG_BUILD
									ELSE
										PRINTLN("[Blips][Vehicles][Vehicle ", sVehState.iIndex, "] SHOULD_VEHICLE_HAVE_BLIP - Vehicle not in a state to have a secondary blip")
										PRINTLN("[Blips][Vehicles][Vehicle ", sVehState.iIndex, "] SHOULD_VEHICLE_HAVE_BLIP - Is drivers seat free? ", BOOL_TO_STRING(bDriverSeatFree))
									#ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF bBlip
				
					IF NOT SHOULD_ENTITY_BLIP_BE_FORCED_ON(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].sVehBlipStruct, sVehState.vehIndex)
					AND NOT WAS_PLAYER_PREVIOUSLY_IN_RANGE_OF_BLIP(sBlipRuntimeVars)
						IF NOT IS_PLAYER_WITHIN_ENTITY_BLIP_RANGE(sVehState.vehIndex, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].sVehBlipStruct)
							bBlip = FALSE
							PRINTLN("[Blips][Vehicles][Vehicle ", sVehState.iIndex, "] SHOULD_VEHICLE_HAVE_BLIP - Vehicle ", sVehState.iIndex, " blip is outside of blip range ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].sVehBlipStruct.fBlipRange, ", not blipping.")
						ENDIF
						IF NOT IS_ENTITY_WITHIN_HEIGHT_DIFFERENCE_RANGE_FOR_BLIP(sVehState.vehIndex, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].sVehBlipStruct)
							bBlip = FALSE
							PRINTLN("[Blips][Vehicles][Vehicle ", sVehState.iIndex, "] SHOULD_VEHICLE_HAVE_BLIP - Vehicle ", sVehState.iIndex, " blip is outside of height range ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].sVehBlipStruct.fBlipHeightDifference, ", not blipping.")
						ENDIF
					ENDIF
				
				ENDIF
			ENDIF
		ENDIF	
	ENDIF
	
	IF NOT bBlip
		IF bCustomBlip
		AND sVehState.bExists
		AND sVehState.bDrivable
			BOOL bBlip2 = TRUE
			
			IF NOT bAnyoneInVehicle
				PRINTLN("[Blips][Vehicles][Vehicle ", sVehState.iIndex, "] SHOULD_VEHICLE_HAVE_BLIP - Don't add secondary blip colour - no one is in vehicle ", sVehState.iIndex)
				bBlip2 = FALSE
				IF DOES_BLIP_EXIST(biVehBlip[sVehState.iIndex])
					CLEAR_SECONDARY_COLOUR_FROM_BLIP(biVehBlip[sVehState.iIndex])
				ENDIF
			ENDIF
				
			IF bBlip2
			AND NOT IS_LOCAL_PLAYER_IN_FMMC_VEHICLE(sVehState)
				IF iDeliveryVehForcingOut != sVehState.iIndex 
					IF NOT IS_FMMC_VEHICLE_DRIVERS_SEAT_FREE(sVehState)
						PLAYER_INDEX piInVeh = GET_DRIVER_OF_CAR_AS_PLAYER(sVehState.vehIndex)
						IF NETWORK_IS_PLAYER_A_PARTICIPANT(piInVeh)
							SET_SECONDARY_COLOUR_FOR_VEHICLE_BLIP_BASED_ON_TEAM(iSColourTeam, MC_playerBD[NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(piInVeh))].iteam, 8)
						ENDIF
					ENDIF
				ENDIF
			ENDIF

			IF (NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetFour, ciFMMC_VEHICLE4_BLIP_OVERRIDE_HIDE_WHEN_IN_VEH_T0 + iTeam))
			OR (NOT IS_PED_IN_VEHICLE(PlayerPedToUse, sVehState.vehIndex))
				IF NOT (IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetFour, ciFMMC_VEHICLE4_BLIP_OVERRIDE_HIDE_WHEN_IN_VEH_T0 + iTeam)
				AND IS_PLAYER_IN_CREATOR_AIRCRAFT(PlayerToUse))
					bBlip = TRUE
					iPColour = BLIP_COLOUR_BLUEDARK
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetSix, ciFMMC_VEHICLE6_TEAM_COLOURED_BLIP)
						IF iTeamInVehicle != -1
							iPColour = GET_BLIP_COLOUR_FROM_HUD_COLOUR(GET_HUD_COLOUR_FOR_FMMC_TEAM(iTeamInVehicle, PlayerToUse))
						ENDIF
					ENDIF
					PRINTLN("[Blips][Vehicles][Vehicle ", sVehState.iIndex, "] SHOULD_VEHICLE_HAVE_BLIP - Veh ", sVehState.iIndex, " being blipped by custom blip settings.")
				ENDIF
			ENDIF			
		ENDIF
	ENDIF
	
	IF bBlip
		
		IF NOT (bAnyoneInVehicle AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetSix, ciFMMC_VEHICLE6_IGNORE_RANGE_IF_OCCUPIED))
		AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetSix, ciFMMC_VEHICLE6_BLIP_ON_MAP)
		AND NOT SHOULD_ENTITY_BLIP_BE_FORCED_ON(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].sVehBlipStruct, sVehState.vehIndex)
		AND NOT WAS_PLAYER_PREVIOUSLY_IN_RANGE_OF_BLIP(sBlipRuntimeVars)
		
			IF NOT IS_PLAYER_WITHIN_ENTITY_BLIP_RANGE(sVehState.vehIndex, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].sVehBlipStruct)
				bBlip = FALSE
				PRINTLN("[Blips][Vehicles][Vehicle ", sVehState.iIndex, "] SHOULD_VEHICLE_HAVE_BLIP - Vehicle ", sVehState.iIndex, " blip is outside of blip range ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].sVehBlipStruct.fBlipRange, ", not blipping.")
			ENDIF
			IF NOT IS_ENTITY_WITHIN_HEIGHT_DIFFERENCE_RANGE_FOR_BLIP(sVehState.vehIndex, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].sVehBlipStruct)
				bBlip = FALSE
				PRINTLN("[Blips][Vehicles][Vehicle ", sVehState.iIndex, "] SHOULD_VEHICLE_HAVE_BLIP - Vehicle ", sVehState.iIndex, " blip is outside of height range ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].sVehBlipStruct.fBlipHeightDifference, ", not blipping.")
			ENDIF
			
		ENDIF
	ENDIF
	
	IF bBlip
	AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetSix, ciFMMC_VEHICLE6_DONT_BLIP_WITH_TRAILER)
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			VEHICLE_INDEX viTempVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
			IF DOES_ENTITY_EXIST(viTempVeh)
				VEHICLE_INDEX viTrailer
				IF GET_VEHICLE_TRAILER_VEHICLE(viTempVeh, viTrailer)
				OR IS_VEHICLE_A_TRAILER(viTempVeh)
					bBlip = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF bBlip
		IF (sVehState.mn = TRAILERLARGE
		OR sVehState.mn = HAULER2)
		AND IS_BIT_SET(iLocalBoolCheck24, LBOOL24_IN_CREATOR_TRAILER)
			bBlip = FALSE
		ENDIF
	ENDIF
	
	IF bBlip
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetTen, ciFMMC_VEHICLE10_RemoveBlipDeadDriver)
		AND IS_VEHICLE_DRIVER_DEAD(sVehState.vehIndex)
			bBlip = FALSE
			PRINTLN("[Blips][Vehicles][Vehicle ", sVehState.iIndex, "] SHOULD_VEHICLE_HAVE_BLIP - Driver is dead")
		ENDIF
	ENDIF
	
	IF iRule < FMMC_MAX_RULES
		INT iBlipVehSetting = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehicleBlipVisibiltyRange[iRule]
		
		IF iBlipVehSetting = ci_VEHICLE_BLIP_RULE_ALWAYS_OFF
			bBlip = FALSE
		ELIF iBlipVehSetting = ci_VEHICLE_BLIP_RULE_ALWAYS_ON
			bBlip = TRUE
		ELIF iBlipVehSetting > ci_VEHICLE_BLIP_RULE_ALWAYS_ON
			VECTOR vVehCoord = GET_FMMC_VEHICLE_COORDS(sVehState)
			VECTOR vPlayerCoord = GET_ENTITY_COORDS(LocalPlayerPed)
		
			FLOAT fDist = ABSF(GET_DISTANCE_BETWEEN_COORDS(vVehCoord, vPlayerCoord, FALSE))
			
			IF fDist < iBlipVehSetting
			OR SHOULD_ENTITY_BLIP_BE_FORCED_ON(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].sVehBlipStruct, sVehState.vehIndex)
				bBlip = TRUE
			ELSE 
				PRINTLN("[Blips][Vehicles][Vehicle ", sVehState.iIndex, "] SHOULD_VEHICLE_HAVE_BLIP - Vehicle ", sVehState.iIndex, " blip is outside of blip range ", iBlipVehSetting, ", not blipping.")
				bBlip = FALSE
			ENDIF
		ENDIF
	
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetFive, ciFMMC_VEHICLE5_HACKING_USE_DISTANCE_FOR_CAPTURE)	
		AND MC_serverBD_4.ivehRule[sVehState.iIndex][MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_CAPTURE
		AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitsetEleven[iRule], ciBS_RULE11_SECUROHACK_REQUIRES_KILL)
			IF MC_serverBD_4.iHackingTargetsRemaining > 0
				IF MC_PlayerBD[iPartToUse].iVehDestroyBitset != 0
					IF NOT IS_BIT_SET(MC_PlayerBD[iPartToUse].iVehDestroyBitset, sVehState.iIndex)
						bBlip = FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF bBlip
		AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitsetEleven[iRule], ciBS_RULE11_UNBLIP_OTHER_ENTITY)
			PRINTLN("[RCC MISSION][Vehicle ",sVehState.iIndex,"] SHOULD_VEHICLE_HAVE_BLIP - Vehicle ", sVehState.iIndex, " checking for ciBS_RULE11_UNBLIP_OTHER_ENTITY.")
			IF ARE_ALL_PARTICIPANTS_IN_OTHER_OBJECTIVE_VEHICLE(sVehState.iIndex, sVehState.vehIndex)
				PRINTLN("[Blips][Vehicles][Vehicle ", sVehState.iIndex, "] SHOULD_VEHICLE_HAVE_BLIP - Vehicle ", sVehState.iIndex, " unblipping because of ciBS_RULE11_UNBLIP_OTHER_ENTITY.")
				bBlip = FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF bBlip
	AND bCustomBlip
		SET_BIT(iVehicleCustomBlip, sVehState.iIndex)
		PRINTLN("[Blips][Vehicles][Vehicle ", sVehState.iIndex, "] SHOULD_VEHICLE_HAVE_BLIP - Setting iVehicleCustomBlip for vehicle.")
	ENDIF
		
	RETURN bBlip
ENDFUNC

PROC SET_VEHICLE_BLIP_NAME(FMMC_VEHICLE_STATE &sVehState)
	IF NOT IS_BLIP_SPRITE_OVERRIDE_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].sVehBlipStruct)
		SET_CUSTOM_BLIP_NAME_FROM_MODEL(biVehBlip[sVehState.iIndex], sVehState.mn)
	ENDIF
	
	TEXT_LABEL_23 tlBlipNameFromRule = GET_BLIP_LITERAL_STRING_NAME(GET_LOCAL_PLAYER_TEAM(), MC_serverBD_4.iVehPriority[sVehState.iIndex][GET_LOCAL_PLAYER_TEAM()], iVehBlipCachedNamePriority[sVehState.iIndex])
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].sVehBlipStruct.iBlipBitset, ciBLIP_INFO_Use_Default_Blip_Name)
	AND NOT IS_CUSTOM_STRING_LIST_STRING_VALID(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].sVehBlipStruct.iBlipNameIndex)
	AND IS_STRING_NULL_OR_EMPTY(tlBlipNameFromRule)
		SET_BLIP_OVERRIDE_SPRITE_USE_CUSTOM_NAME(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].sVehBlipStruct.iBlipSpriteOverride, biVehBlip[sVehState.iIndex], IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetNine, ciFMMC_VEHICLE9_UsingNewBlipSpriteData))
	ENDIF
	
	SET_NAME_FOR_BLIP(biVehBlip[sVehState.iIndex], g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].sVehBlipStruct, FMMC_BLIP_VEHICLE, sVehState.iIndex, GET_LOCAL_PLAYER_TEAM(TRUE))
ENDPROC

PROC SET_VEHICLE_BLIP_SETTINGS_EVERY_FRAME(FMMC_VEHICLE_STATE &sVehState)
	IF DOES_BLIP_EXIST(biVehBlip[sVehState.iIndex])
		IF SHOULD_BLIP_BE_MANUALLY_ROTATED(biVehBlip[sVehState.iIndex])
			INT iVehHeading = ROUND(GET_FMMC_VEHICLE_HEADING(sVehState))
			IF GET_BLIP_ROTATION(biVehBlip[sVehState.iIndex]) != iVehHeading
				SET_BLIP_ROTATION(biVehBlip[sVehState.iIndex], iVehHeading)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SET_VEHICLE_BLIP_SETTINGS(FMMC_VEHICLE_STATE &sVehState, BLIP_RUNTIME_VARS_STRUCT &sBlipRuntimeVars, INT iPrimaryBlipColour)
	
	INT iTeam = MC_PlayerBD[iLocalPart].iTeam
	INT iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]

	IF GET_CORRECT_BLIP_COLOUR(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].sVehBlipStruct, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iAggroIndexBS_Entity_Veh) != BLIP_COLOUR_DEFAULT
		iPrimaryBlipColour =  GET_BLIP_COLOUR_FROM_CREATOR(GET_CORRECT_BLIP_COLOUR(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].sVehBlipStruct, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iAggroIndexBS_Entity_Veh))
	ENDIF
	
	INT iTeamsBS = ciALL_TEAMS_BITSET_VALUE
	INT iTeamInVeh
	BOOL bOccupied = ARE_ANY_PARTICIPANTS_IN_VEHICLE(sVehState.iIndex, iTeamsBS, iTeamInVeh)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetSix, ciFMMC_VEHICLE6_TEAM_COLOURED_BLIP)
		IF bOccupied
			IF iTeamInVeh != -1
				INT iTempBlipColour = GET_BLIP_COLOUR_FROM_HUD_COLOUR(GET_HUD_COLOUR_FOR_FMMC_TEAM(iTeamInVeh, PlayerToUse))
				IF iPrimaryBlipColour != iTempBlipColour
					PRINTLN("[Blips][Vehicle ",sVehState.iIndex,"] SET_VEHICLE_BLIP_SETTINGS - Vehicle ", sVehState.iIndex, " blip setup: Using team colour", iTeamInVeh)
					iPrimaryBlipColour = iTempBlipColour
				ENDIF
			ENDIF
		ELSE
			IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].sVehBlipStruct.iBlipColour = BLIP_COLOUR_DEFAULT
			OR (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].sVehBlipStruct.iBlipColourAggro = BLIP_COLOUR_DEFAULT AND HAS_TEAM_TRIGGERED_AGGRO(iTeam, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iAggroIndexBS_Entity_Veh))
				iPrimaryBlipColour = BLIP_COLOUR_BLUEDARK
			ENDIF
		ENDIF
	ENDIF
	
	PRINTLN("[Blips][Vehicle ",sVehState.iIndex,"] SET_VEHICLE_BLIP_SETTINGS - Vehicle ", sVehState.iIndex, " blip setup: Primary colour is ", iPrimaryBlipColour, ".")
	
	SET_BLIP_HEIGHT_INDICATOR(biVehBlip[sVehState.iIndex], g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].sVehBlipStruct)
	
	IF SHOULD_BLIP_BE_MANUALLY_ROTATED(biVehBlip[sVehState.iIndex])
		SET_BLIP_ROTATION(biVehBlip[sVehState.iIndex], ROUND(GET_ENTITY_HEADING(sVehState.vehIndex)))
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetThree, ciFMMC_VEHICLE3_AutoGPSVehicle)
		PRINTLN("[Blips][Vehicle ",sVehState.iIndex,"] SET_VEHICLE_BLIP_SETTINGS - Vehicle ", sVehState.iIndex, " blip setup: Enabling GPS route for vehicle blip.")
		SET_BLIP_ROUTE(biVehBlip[sVehState.iIndex], TRUE)
		SET_BLIP_ROUTE_COLOUR(biVehBlip[sVehState.iIndex], iPrimaryBlipColour)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetThree, ciFMMC_VEHICLE3_IgnoreNoGPSFlag)
		PRINTLN("[Blips][Vehicle ",sVehState.iIndex,"] SET_VEHICLE_BLIP_SETTINGS - Vehicle ", sVehState.iIndex, " blip setup: Ignoring \"No GPS\" flag.")
		SET_IGNORE_NO_GPS_FLAG(TRUE)
	ENDIF
	
	SET_BLIP_COLOUR(biVehBlip[sVehState.iIndex], iPrimaryBlipColour)
	
	SET_VEHICLE_BLIP_NAME(sVehState)
	
	IF iRule < FMMC_MAX_RULES
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetSix,ciFMMC_VEHICLE6_BLIP_ON_MAP)
			BLIP_DISPLAY eBlipDisplay = DISPLAY_BOTH
			IF sVehState.bAlive
			AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehicleBlipVisibiltyRange[iRule] != ci_VEHICLE_BLIP_RULE_ALWAYS_ON
				IF NOT (bOccupied AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetSix,ciFMMC_VEHICLE6_IGNORE_RANGE_IF_OCCUPIED))
					IF NOT IS_PLAYER_WITHIN_ENTITY_BLIP_RANGE(sVehState.vehIndex, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].sVehBlipStruct, IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitsetSeventeen, ciIGNORE_Z_FOR_VEH_BLIP_RANGE))
						eBlipDisplay = DISPLAY_MAP
					ENDIF
					
					IF NOT IS_ENTITY_WITHIN_HEIGHT_DIFFERENCE_RANGE_FOR_BLIP(sVehState.vehIndex, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].sVehBlipStruct)
						eBlipDisplay = DISPLAY_MAP
					ENDIF
				ENDIF
			ENDIF
			SET_BLIP_DISPLAY(biVehBlip[sVehState.iIndex], eBlipDisplay)
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetSix, ciFMMC_VEHICLE6_TEAM_COLOURED_BLIP)
		INT iPColour, iSColourTeam, iTeamForSecondaryColour
		BOOL bShouldUseSecondaryColour
		SHOULD_VEHICLE_HAVE_BLIP(sVehState, sBlipRuntimeVars, iPColour, iSColourTeam)
		bShouldUseSecondaryColour = (iSColourTeam != -1)
		iTeamForSecondaryColour = iSColourTeam
		IF bShouldUseSecondaryColour
			PRINTLN("[Blips][Vehicle ",sVehState.iIndex,"] SET_VEHICLE_BLIP_SETTINGS - Vehicle ", sVehState.iIndex, " blip setup: Overriding colour to secondary colour ", iTeamForSecondaryColour, ".")
			IF (iTeamForSecondaryColour = -1)
				CASSERTLN(DEBUG_MC_BLIP, "[RCC MISSION][VEH] Vehicle ", sVehState.iIndex, " blip setup: Invalid secondary colour of ", iTeamForSecondaryColour, " passed.") 
			ENDIF
			SET_BLIP_TEAM_SECONDARY_COLOUR(biVehBlip[sVehState.iIndex], TRUE, iTeamForSecondaryColour)
		ENDIF
	ENDIF
	
	PRINTLN("[Blips][Vehicle ",sVehState.iIndex,"] SET_VEHICLE_BLIP_SETTINGS - Vehicle ", sVehState.iIndex, " blip setup: Done.")
ENDPROC

PROC RUN_UPDATE_ON_VEHICLE_BLIP(FMMC_VEHICLE_STATE &sVehState, BLIP_RUNTIME_VARS_STRUCT &sBlipRuntimeVars)
	
	INT iPrimaryBlipColour
	
	IF DOES_BLIP_EXIST(biVehBlip[sVehState.iIndex])

		BOOL bSetBlipSettings = FALSE
		BOOL bCheckOccupation = FALSE
				
		iPrimaryBlipColour = GET_BLIP_COLOUR(biVehBlip[sVehState.iIndex])
		IF SHOULD_BLIP_ATLER_DEPENDING_ON_OCCUPANCY(biVehBlip[sVehState.iIndex])
			IF UPDATE_VEHILCE_BLIP_BASED_ON_OCCUPANCY(sVehState.vehIndex, biVehBlip[sVehState.iIndex])
				PRINTLN("[Blips][Vehicle ",sVehState.iIndex,"] RUN_UPDATE_ON_VEHICLE_BLIP - Blip sprite was changed based on occupancy.")
				bSetBlipSettings = TRUE
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetSix, ciFMMC_VEHICLE6_TEAM_COLOURED_BLIP)
			PRINTLN("[RCC MISSION][Vehicle ",sVehState.iIndex,"] setting bCheckOccupation as iVehBitsetSix, ciFMMC_VEHICLE6_TEAM_COLOURED_BLIP is set.")
			bCheckOccupation = TRUE
		ENDIF
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetEight, ciFMMC_VEHICLE8_RunBlipUpdates)
			PRINTLN("[RCC MISSION][Vehicle ",sVehState.iIndex,"] setting bCheckOccupation as iVehBitsetEight, ciFMMC_VEHICLE8_RunBlipUpdates is set.")
			bCheckOccupation = TRUE
		ENDIF
		
		IF bCheckOccupation
			IF sVehState.bAlive
			AND NOT IS_FMMC_VEHICLE_EMPTY(sVehState)
				IF NOT IS_BIT_SET(iVehOccupiedBS, sVehState.iIndex)
					SET_BIT(iVehOccupiedBS, sVehState.iIndex)
					bSetBlipSettings = TRUE
					PRINTLN("[Blips][Vehicle ",sVehState.iIndex,"] RUN_UPDATE_ON_VEHICLE_BLIP - Updating blip as vehicle is now occupied")
				ENDIF
			ELSE
				IF IS_BIT_SET(iVehOccupiedBS, sVehState.iIndex)
					CLEAR_BIT(iVehOccupiedBS, sVehState.iIndex)
					bSetBlipSettings = TRUE
					PRINTLN("[Blips][Vehicle ",sVehState.iIndex,"] RUN_UPDATE_ON_VEHICLE_BLIP - Updating blip as vehicle is no longer occupied")
				ENDIF
			ENDIF
		ENDIF
		
		IF bSetBlipSettings
			SET_VEHICLE_BLIP_SETTINGS(sVehState, sBlipRuntimeVars, iPrimaryBlipColour)
		ENDIF
		
	ENDIF
ENDPROC

PROC CREATE_BLIP_FOR_MP_VEHICLE(FMMC_VEHICLE_STATE &sVehState, BLIP_RUNTIME_VARS_STRUCT &sBlipRuntimeVars, INT iPrimaryBlipColour)
	
	CREATE_BLIP_WITH_CREATOR_SETTINGS(
		FMMC_BLIP_VEHICLE, 
		biVehBlip[sVehState.iIndex], 
		g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].sVehBlipStruct, 
		sMissionVehiclesLocalVars[sVehState.iIndex].sBlipRuntimeVars, 
		sVehState.vehIndex, 
		sVehState.iIndex, 
		EMPTY_VEC(), 
		g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iAggroIndexBS_Entity_Veh
	)
	
	IF DOES_BLIP_EXIST(biVehBlip[sVehState.iIndex])
		//Blocks another kind of vehicle blip attempting to be made on this vehicle this frame.
		PRINTLN("[Blips][Veh] CREATE_BLIP_FOR_MP_VEHICLE - Flagging to block any more new blips being created on veh ", sVehState.iIndex, " this frame (", GET_FRAME_COUNT(), ").")
		SET_BIT(iVehBlipCreatedThisFrameBitset, sVehState.iIndex)
		
		IF NOT IS_BLIP_SPRITE_OVERRIDE_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].sVehBlipStruct)
			SET_BLIP_SPRITE(biVehBlip[sVehState.iIndex], GET_CORRECT_PED_BLIP_SPRITE_FOR_VEHICLE_MODEL(sVehState.mn, FALSE, IS_VEHICLE_EMPTY_AND_STATIONARY(sVehState.vehIndex)))
		ENDIF
		
		SET_VEHICLE_BLIP_NAME(sVehState)
		
		SET_VEHICLE_BLIP_SETTINGS(sVehState, sBlipRuntimeVars, iPrimaryBlipColour)
		
	ENDIF
ENDPROC

FUNC BOOL SHOULD_SKIP_BLIP_CREATION_FOR_THIS_VEHICLE(INT iVehIndex)
	#IF IS_DEBUG_BUILD
		IF iVehIndex > FMMC_MAX_VEHICLES
			ASSERTLN("SHOULD_SKIP_BLIP_CREATION_FOR_THIS_VEHICLE - iVehIndex can't be greater than num of FMMC_MAX_VEHICLES!")
			RETURN FALSE
		ENDIF
		IF iVehIndex < 0
			ASSERTLN("SHOULD_SKIP_BLIP_CREATION_FOR_THIS_VEHICLE - iVehIndex can't be lower than 0!")
			RETURN FALSE
		ENDIF
		INT iZero = 0 //To prevent "dead code" warning
		IF FMMC_MAX_VEHICLES > ci_VEHICLE_IGNORE_BLIP_CREATION_BS_MAX * 31 + iZero
			INT iNewArraySize = FMMC_MAX_VEHICLES / 31
			IF FMMC_MAX_VEHICLES % 31 != iZero 
				iNewArraySize++
			ENDIF
			
			ASSERTLN("SHOULD_SKIP_BLIP_CREATION_FOR_THIS_VEHICLE - FMMC_MAX_VEHICLES is greater than ", ci_VEHICLE_IGNORE_BLIP_CREATION_BS_MAX * 31, ". Please update ci_VEHICLE_IGNORE_BLIP_CREATION_BS_MAX with the new max value (", iNewArraySize, ")")
			RETURN FALSE
		ENDIF
	#ENDIF
	
	RETURN FMMC_IS_LONG_BIT_SET(iVehIgnoreBlipCreationBS, iVehIndex)
ENDFUNC

PROC HANDLE_VEHICLE_BLIP_CREATION(FMMC_VEHICLE_STATE &sVehState, BLIP_RUNTIME_VARS_STRUCT &sBlipRuntimeVars, INT& iPColour, INT& iSColourTeam)
	IF SHOULD_SKIP_BLIP_CREATION_FOR_THIS_VEHICLE(sVehState.iIndex)
		EXIT
	ENDIF	
	
	IF NOT DOES_BLIP_EXIST(biVehBlip[sVehState.iIndex])
		IF SHOULD_VEHICLE_HAVE_BLIP(sVehState, sBlipRuntimeVars, iPColour, iSColourTeam)
			IF NOT IS_BIT_SET(iVehBlipCreatedThisFrameBitset, sVehState.iIndex)
				PRINTLN("[RCC MISSION][VEH] HANDLE_VEHICLE_BLIP_CREATION - Creating blip for vehicle ", sVehState.iIndex, ". SHOULD_VEHICLE_HAVE_BLIP returned TRUE.")
				CREATE_BLIP_FOR_MP_VEHICLE(sVehState, sBlipRuntimeVars, iPColour)
			ELSE
				PRINTLN("[RCC MISSION][VEH] HANDLE_VEHICLE_BLIP_CREATION - Block vehicle blip creation this frame as another blip has just been made on the vehicle.")
			ENDIF
		ELSE
			IF SHOULD_VEHICLE_BLIP_BE_REMOVED(sVehState.vehIndex)
				REMOVE_VEHICLE_BLIP(sVehState.iIndex)
				REMOVE_ANY_EXISTING_BLIP_FROM_ENTITY(sVehState.vehIndex)
			ENDIF
		ENDIF
	ELSE
		// We know a blip exists, monitor any conditions that require it to be cleaned up
		IF SHOULD_VEHICLE_BLIP_CLEANUP(sVehState.iIndex, sBlipRuntimeVars, sVehState.vehIndex)
			PRINTLN("[RCC MISSION][VEH] HANDLE_VEHICLE_BLIP_CREATION - Removing blip for vehicle ", sVehState.iIndex, ". SHOULD_VEHICLE_BLIP_CLEANUP returned TRUE.")
			REMOVE_VEHICLE_BLIP(sVehState.iIndex)
		ENDIF
	ENDIF
ENDPROC

PROC RESYNC_FLASHING_VEHICLE_BLIPS()
	IF IS_BIT_SET(iVehBlipFlashingState, BIT_FLASH_VEH_BLIP_RESYNC_REQUESTED)
		PRINTLN("[RCC MISSION][VEH] Handling flashing veh blip resync request this frame.")
		INT i
		
		IF NOT IS_BIT_SET(iVehBlipFlashingState, BIT_FLASH_VEH_BLIP_FLASHING_STOPPED)
			REPEAT FMMC_MAX_VEHICLES i
				IF DOES_BLIP_EXIST(biVehBlip[i])
					IF IS_BIT_SET(iVehBlipFlashingBitset, i)
						SET_BLIP_FLASHES(biVehBlip[i], FALSE)
					ENDIF
				ENDIF
			ENDREPEAT
			SET_BIT(iVehBlipFlashingState, BIT_FLASH_VEH_BLIP_FLASHING_STOPPED)
			PRINTLN("[RCC MISSION][VEH] Stopped all flashing blips this frame.")
			
		ELSE
			REPEAT FMMC_MAX_VEHICLES i
				IF DOES_BLIP_EXIST(biVehBlip[i])
					IF IS_BIT_SET(iVehBlipFlashingBitset, i)
						SET_BLIP_FLASHES(biVehBlip[i], TRUE)
					ENDIF
				ENDIF
			ENDREPEAT
			PRINTLN("[RCC MISSION][VEH] Restarted all flashing blips this frame.")
			CLEAR_BIT(iVehBlipFlashingState, BIT_FLASH_VEH_BLIP_FLASHING_STOPPED)
			CLEAR_BIT(iVehBlipFlashingState, BIT_FLASH_VEH_BLIP_RESYNC_REQUESTED)
			PRINTLN("[RCC MISSION][VEH] Finished flashing veh blip resync.")
		ENDIF
		
	ENDIF
ENDPROC

PROC PROCESS_VEHICLE_BLIPS_STAGGERED(FMMC_VEHICLE_STATE &sVehState, BLIP_RUNTIME_VARS_STRUCT &sBlipRuntimeVars)
	
	INT iPColour
	INT iSColourTeam = -1
	
	//Request from last update to resync all flashing veh blips.
	RESYNC_FLASHING_VEHICLE_BLIPS()
	
	BOOL bDoesBlipExist = DOES_BLIP_EXIST(biVehBlip[sVehState.iIndex])
		
	IF iLocalDriverPart[sVehState.iIndex] != MC_serverBD.iDriverPart[sVehState.iIndex]
		IF bDoesBlipExist
			IF MC_serverBD.iDriverPart[sVehState.iIndex] !=-1
				SET_BLIP_TEAM_SECONDARY_COLOUR(biVehBlip[sVehState.iIndex],TRUE,MC_playerBD[MC_serverBD.iDriverPart[sVehState.iIndex]].iteam)
			ELSE
				SET_BLIP_TEAM_SECONDARY_COLOUR(biVehBlip[sVehState.iIndex],FALSE,-1)
			ENDIF
		
			PRINTLN("[RCC MISSION] PROCESS_VEHICLE_BLIPS_STAGGERED - Creating blip for vehicle ", sVehState.iIndex, ". iLocalDriverPart[sVehState.iIndex]: ", iLocalDriverPart[sVehState.iIndex], " != MC_serverBD.iDriverPart[sVehState.iIndex]: ", MC_serverBD.iDriverPart[sVehState.iIndex])
			REMOVE_VEHICLE_BLIP(sVehState.iIndex)
		ENDIF
		
		iLocalDriverPart[sVehState.iIndex] = MC_serverBD.iDriverPart[sVehState.iIndex]
	ENDIF
	
	HANDLE_VEHICLE_BLIP_CREATION(sVehState, sBlipRuntimeVars, iPColour, iSColourTeam)
	
	IF !bDoesBlipExist
		EXIT
	ENDIF
	
	IF sVehState.bExists
		IF sVehState.bDrivable
			IF NOT DOES_BLIP_EXIST(GET_BLIP_FROM_ENTITY(sVehState.vehIndex))
				PRINTLN("[RCC MISSION] PROCESS_VEHICLE_BLIPS_STAGGERED - Removing blip for vehicle ", sVehState.iIndex, ". The blip was on an old vehicle.")
				REMOVE_VEHICLE_BLIP(sVehState.iIndex)
			ENDIF
		ENDIF
	ENDIF

	RUN_UPDATE_ON_VEHICLE_BLIP(sVehState, sBlipRuntimeVars)
	
	PROCESS_ENTITY_BLIP(biVehBlip[sVehState.iIndex], g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].sVehBlipStruct, GET_FMMC_VEHICLE_COORDS(sVehState), g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iAggroIndexBS_Entity_Veh)
	
	IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam] < FMMC_MAX_RULES
	
		BOOL bCustomBlip = SHOULD_VEHICLE_HAVE_CUSTOM_BLIP(sVehState.iIndex)
		
		IF MC_serverBD_4.iVehPriority[sVehState.iIndex][MC_playerBD[iPartToUse].iTeam] > MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]
		OR (MC_serverBD_4.ivehRule[sVehState.iIndex][MC_playerBD[iPartToUse].iTeam] = FMMC_OBJECTIVE_LOGIC_GET_AND_HOLD AND IS_BIT_SET(MC_serverBD.iVehAtYourHolding[MC_playerBD[iPartToUse].iteam],sVehState.iIndex))
		OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iRuleBitset[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]], ciBS_RULE_ENABLETRACKIFY)
			IF NOT bCustomBlip
				PRINTLN("[RCC MISSION] PROCESS_VEHICLE_BLIPS_STAGGERED - Removing blip for vehicle ", sVehState.iIndex, ". The vehicle is in a Collect and Hold drop-off.")
				REMOVE_VEHICLE_BLIP(sVehState.iIndex)
			ENDIF
		ELSE
			IF sVehState.bDrivable
				IF (MC_serverBD_4.ivehRule[sVehState.iIndex][MC_playerBD[iPartToUse].iTeam] = FMMC_OBJECTIVE_LOGIC_GO_TO
				OR MC_serverBD_4.ivehRule[sVehState.iIndex][MC_playerBD[iPartToUse].iTeam] = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER)
				AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iRuleBitsetThree[MC_serverBD_4.iVehPriority[sVehState.iIndex][MC_playerBD[iPartToUse].iTeam]], ciBS_RULE3_ALL_IN_SEPARATE_VEHICLES)
					
					BOOL bCheckOnRule
					INT iTeamsBS = GET_VEHICLE_GOTO_TEAMS(MC_playerBD[iPartToUse].iTeam, sVehState.iIndex, bCheckOnRule)
										
					IF NOT bCustomBlip
						IF ARE_ANY_TEAM_PARICIPANTS_IN_VEHICLE(sVehState.iIndex, iTeamsBS)
							PRINTLN("[RCC MISSION] PROCESS_VEHICLE_BLIPS_STAGGERED -Removing blip for vehicle ", sVehState.iIndex, ". A participant on the vehicle's rule has entered the vehicle.")
							REMOVE_VEHICLE_BLIP(sVehState.iIndex)
						ENDIF
					ELSE
						iTeamsBS = ciALL_TEAMS_BITSET_VALUE
						INT iTeamInVeh
						IF ARE_ANY_PARTICIPANTS_IN_VEHICLE(sVehState.iIndex, iTeamsBS, iTeamInVeh)
							RUN_UPDATE_ON_VEHICLE_BLIP(sVehState, sBlipRuntimeVars)
						ENDIF
					ENDIF
				ENDIF
				
				IF MC_serverBD_4.ivehRule[sVehState.iIndex][MC_playerBD[iPartToUse].iTeam] = FMMC_OBJECTIVE_LOGIC_CAPTURE
				AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitsetEleven[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]], ciBS_RULE11_SECUROHACK_REQUIRES_KILL)
				AND IS_BIT_SET(MC_serverBD_4.iVehHackedReadyForDeathBS, sVehState.iIndex)
				AND GET_BLIP_COLOUR(biVehBlip[sVehState.iIndex]) != BLIP_COLOUR_RED
					SET_BLIP_COLOUR(biVehBlip[sVehState.iIndex], BLIP_COLOUR_RED)
				ENDIF
				
				IF IS_LOCAL_PLAYER_IN_FMMC_VEHICLE(sVehState)
					PRINTLN("[RCC MISSION] PROCESS_VEHICLE_BLIPS_STAGGERED - Removing blip for vehicle ", sVehState.iIndex, ". The local player has entered the vehicle.")
					REMOVE_VEHICLE_BLIP(sVehState.iIndex)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Server/Brain Processing  ----------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Vehicle functions that only the host will process.  --------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_VEHICLE_DAMAGE_ENTITY_OBJECTIVE_STAGGERED(INT iVeh)
	
	INT iTeam = 0
	FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
		
		IF HAS_TEAM_FAILED(iTeam)
		OR MC_serverBD.iNumVehHighestPriority[iTeam] <= 0
		OR MC_serverBD_4.iVehPriority[iVeh][iTeam] > MC_serverBD_4.iCurrentHighestPriority[iTeam]
		OR MC_serverBD_4.iVehRule[iVeh][iTeam] != FMMC_OBJECTIVE_LOGIC_DAMAGE
			RELOOP
		ENDIF
			
		SET_CURRENT_HEALTH_PERCENTAGES_OF_VEHICLE_AND_LINKED_VEHICLES(iVeh)
		
		SET_BIT(iDamageVehicleObjectiveRequiresCheckBS[iTeam], iVeh)
		
	ENDFOR
	
ENDPROC

PROC PROCESS_VEHICLE_DAMAGE_ENTITY_OBJECTIVE_POST_STAGGERED(INT iVeh)
	
	INT iTeam = 0
	FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
		
		IF MC_serverBD_4.iCurrentHighestPriority[iTeam] >= FMMC_MAX_RULES
			RELOOP
		ENDIF
		
		IF NOT IS_BIT_SET(iDamageVehicleObjectiveRequiresCheckBS[iTeam], iVeh)
			BREAKLOOP
		ENDIF
		
		FLOAT fCurrentPercentage = GET_CURRENT_HEALTH_PERCENTAGES_OF_VEHICLE_AND_LINKED_VEHICLES(iVeh)
		
		INT iHealthRequiredForRule = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iHealthDamageRequiredForRule
		IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iPriorityEntityHealthThresholdOverride[MC_serverBD_4.iCurrentHighestPriority[iTeam]] != 0
			iHealthRequiredForRule = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iPriorityEntityHealthThresholdOverride[MC_serverBD_4.iCurrentHighestPriority[iTeam]]
		ENDIF
		
		PRINTLN("[Vehicle][Server][", iVeh, "] - PROCESS_VEHICLE_DAMAGE_ENTITY_OBJECTIVE_POST_STAGGERED - fCurrentPercentage: ", fCurrentPercentage, " thresh-hold to pass objective: ", iHealthRequiredForRule)
		
		IF fCurrentPercentage > iHealthRequiredForRule
			RELOOP	
		ENDIF
	
		PRINTLN("[Vehicle][Server][", iVeh, "] - PROCESS_VEHICLE_DAMAGE_ENTITY_OBJECTIVE_POST_STAGGERED - Below the thresh-hold to pass objective")
				
		SET_TEAM_OBJECTIVE_OUTCOME(iTeam, MC_serverBD_4.iVehPriority[iVeh][iTeam], fCurrentPercentage >= 0)
				
		PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_VEHICLE, iVeh, iTeam, fCurrentPercentage >= 0)
		
		MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_VEH_DAMAGED
		
		SET_LOCAL_OBJECTIVE_END_MESSAGE(iTeam, MC_serverBD_4.iVehPriority[iVeh][iTeam], MC_serverBD.iReasonForObjEnd[iTeam], fCurrentPercentage >= 0)
		
		BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[iTeam], iTeam, MC_serverBD_4.iVehPriority[iVeh][iTeam], DID_TEAM_PASS_OBJECTIVE(iTeam, MC_serverBD_4.iVehPriority[iVeh][iTeam]))
	
	ENDFOR
ENDPROC

PROC MANAGE_SERVER_VEH_CAPTURE_BY_DISTANCE_EVERY_FRAME(FMMC_VEHICLE_STATE &sVehState)
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetFive, ciFMMC_VEHICLE5_HACKING_USE_DISTANCE_FOR_CAPTURE)	
		PARTICIPANT_INDEX tempPart
		PLAYER_INDEX tempPlayer
		INT iCaptureTeam
		INT iTeam
		INT ipriority[FMMC_MAX_TEAMS]
		BOOL bwasobjective[FMMC_MAX_TEAMS]
		INT iPartThatHasControl = -1
		INT iPartsChecked = 0
		INT iTotalPartsToCheck = MC_serverBD.iNumberOfPlayingPlayers[0] + MC_serverBD.iNumberOfPlayingPlayers[1] + MC_serverBD.iNumberOfPlayingPlayers[2] + MC_serverBD.iNumberOfPlayingPlayers[3]

		IF sVehState.bExists
			IF NOT IS_BIT_SET(MC_serverBD_4.iVehHackedReadyForDeathBS, sVehState.iIndex)
			AND NOT IS_BIT_SET(MC_serverBD_4.iVehHackeDeathCountedBS, sVehState.iIndex)
				FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams-1)
					IF MC_serverBD_4.iVehPriority[sVehState.iIndex][iTeam] = MC_ServerbD_4.iCurrentHighestPriority[iTeam]
						iTempHackingTargetsRemaining++
						BREAKLOOP
					ENDIF
				ENDFOR
			ENDIF
			IF sVehState.bAlive
				IF NOT IS_BIT_SET(MC_serverBD_4.iVehHackedReadyForDeathBS, sVehState.iIndex)
					IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetFive, ciFMMC_VEHICLE5_HACKING_RESET_CAPTURE_OUT_OF_RANGE)
					AND MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(MC_serverBD.iControlVehTimerOffset[sVehState.iIndex])
						PRINTLN("MANAGE_SERVER_VEH_CAPTURE_BY_DISTANCE_EVERY_FRAME Timer: ", MC_GET_NET_TIMER_DIFFERENCE_WITH_TIME_STAMP(MC_serverBD_1.iControlVehTimer[sVehState.iIndex]))
						PRINTLN("MANAGE_SERVER_VEH_CAPTURE_BY_DISTANCE_EVERY_FRAME Offset Timer: ", MC_GET_NET_TIMER_DIFFERENCE_WITH_TIME_STAMP(MC_serverBD.iControlVehTimerOffset[sVehState.iIndex]))
						MC_serverBD_1.iControlVehTimer[sVehState.iIndex] += MC_GET_NET_TIMER_DIFFERENCE_WITH_TIME_STAMP(MC_serverBD.iControlVehTimerOffset[sVehState.iIndex])
						PRINTLN("MANAGE_SERVER_VEH_CAPTURE_BY_DISTANCE_EVERY_FRAME New Timer: ", MC_GET_NET_TIMER_DIFFERENCE_WITH_TIME_STAMP(MC_serverBD_1.iControlVehTimer[sVehState.iIndex]))
						MC_STOP_NET_TIMER_WITH_TIMESTAMP(MC_serverBD.iControlVehTimerOffset[sVehState.iIndex])
					ENDIF
				
					INT iPartToCheck = -1
					IF NOT IS_BIT_SET(MC_serverBD_4.iCaptureVehRequestPartBS, sVehState.iIndex)
						//Check who asked to hack
						PRINTLN("[PLAYER_LOOP] - MANAGE_SERVER_VEH_CAPTURE_BY_DISTANCE_EVERY_FRAME 2")
						FOR iPartToCheck = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION() - 1
							IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPartToCheck))
							AND NOT (IS_BIT_SET(MC_playerBD[iPartToCheck].iClientBitSet, PBBOOL_ANY_SPECTATOR)
								OR IS_BIT_SET(MC_playerBD[iPartToCheck].iClientBitSet, PBBOOL_HEIST_SPECTATOR))
							AND NOT IS_BIT_SET(MC_playerBD[iPartToCheck].iClientBitSet,PBBOOL_PLAYER_FAIL)
							AND IS_BIT_SET(MC_playerBD[iPartToCheck].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
							AND NOT IS_BIT_SET(MC_playerBD[iPartToCheck].iClientBitSet, PBBOOL_FINISHED)
								iPartsChecked++
								IF IS_BIT_SET(MC_playerBD[iPartToCheck].iRequestCaptureVehBS, sVehState.iIndex)
									SET_BIT(MC_serverBD_4.iCaptureVehRequestPartBS, sVehState.iIndex)
									iCaptureTeam = MC_playerBD[iPartToCheck].iTeam
									PRINTLN("MANAGE_SERVER_VEH_CAPTURE_BY_DISTANCE_EVERY_FRAME - Participant: ", iPartToCheck," has requested to capture the veh: ", sVehState.iIndex)
									BREAKLOOP
								ENDIF
								IF iPartsChecked >= iTotalPartsToCheck
									BREAKLOOP
								ENDIF
							ENDIF
						ENDFOR
					ELSE
						BOOL bPartHasControl = FALSE
						PRINTLN("[PLAYER_LOOP] - MANAGE_SERVER_VEH_CAPTURE_BY_DISTANCE_EVERY_FRAME")
						FOR iPartToCheck = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION() - 1
							IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPartToCheck))
							AND NOT (IS_BIT_SET(MC_playerBD[iPartToCheck].iClientBitSet, PBBOOL_ANY_SPECTATOR)
								OR IS_BIT_SET(MC_playerBD[iPartToCheck].iClientBitSet, PBBOOL_HEIST_SPECTATOR))
							AND NOT IS_BIT_SET(MC_playerBD[iPartToCheck].iClientBitSet,PBBOOL_PLAYER_FAIL)
							AND IS_BIT_SET(MC_playerBD[iPartToCheck].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
							AND NOT IS_BIT_SET(MC_playerBD[iPartToCheck].iClientBitSet, PBBOOL_FINISHED)
								iPartsChecked++
								
								IF IS_BIT_SET(MC_playerBD[iPartToCheck].iRequestCaptureVehBS, sVehState.iIndex)
									iPartThatHasControl = iPartToCheck
									bPartHasControl = TRUE
									PRINTLN("MANAGE_SERVER_VEH_CAPTURE_BY_DISTANCE_EVERY_FRAME - Participant: ", iPartToCheck," is still in control - veh: ", sVehState.iIndex)
									BREAKLOOP
								ENDIF
								
								IF iPartsChecked >= iTotalPartsToCheck
									BREAKLOOP
								ENDIF
							ENDIF
						ENDFOR
						IF !bPartHasControl
							PRINTLN("MANAGE_SERVER_VEH_CAPTURE_BY_DISTANCE_EVERY_FRAME - Participant: ", iPartToCheck, " has gone out of range, clearing bits - veh: ", sVehState.iIndex)
							CLEAR_BIT(MC_serverBD_4.iCaptureVehRequestPartBS, sVehState.iIndex)
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF IS_BIT_SET(MC_serverBD_4.iVehHackedReadyForDeathBS, sVehState.iIndex)
					PRINTLN("MANAGE_SERVER_VEH_CAPTURE_BY_DISTANCE_EVERY_FRAME - DESTROYED veh: ", sVehState.iIndex)
					FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams-1)
						iPriority[iTeam] = MC_serverBD_4.iVehPriority[sVehState.iIndex][iTeam]
						IF MC_serverBD_4.ivehRule[sVehState.iIndex][iTeam] = FMMC_OBJECTIVE_LOGIC_CAPTURE	
							SET_TEAM_OBJECTIVE_OUTCOME(iTeam, iPriority[iTeam], TRUE)
							PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_VEHICLE, sVehState.iIndex, iTeam, TRUE)
							MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_VEH_CAPTURED
							IF IS_THIS_OBJECTIVE_OVER(ci_TARGET_VEHICLE, iTeam, iPriority[iTeam])
								SET_LOCAL_OBJECTIVE_END_MESSAGE(iTeam, iPriority[iTeam], MC_serverBD.iReasonForObjEnd[iTeam])
								BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[iTeam], iTeam, iPriority[iTeam], DID_TEAM_PASS_OBJECTIVE(iTeam,iPriority[iTeam]))
							ENDIF
						ENDIF
					ENDFOR
					CLEAR_BIT(MC_serverBD_4.iVehHackedReadyForDeathBS, sVehState.iIndex)
					SET_BIT(MC_serverBD_4.iVehHackeDeathCountedBS, sVehState.iIndex)
				ENDIF
				
				EXIT
			ENDIF
		ENDIF

		IF NOT IS_BIT_SET(MC_serverBD_4.iVehExplodeKillOnCaptureBS, sVehState.iIndex)
			IF iPartThatHasControl != -1
				tempPart = INT_TO_PARTICIPANTINDEX(iPartThatHasControl)
			
				IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
				AND NOT IS_BIT_SET(MC_playerBD[iPartThatHasControl].iClientBitSet, PBBOOL_FINISHED)
					tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
					
					IF IS_NET_PLAYER_OK(tempPlayer)
						iCaptureTeam = MC_playerBD[iPartThatHasControl].iteam
						
						IF MC_serverBD_4.ivehRule[sVehState.iIndex][iCaptureTeam] = FMMC_OBJECTIVE_LOGIC_CAPTURE
							IF MC_serverBD_4.iVehPriority[sVehState.iIndex][iCaptureTeam] < FMMC_MAX_RULES
								IF NOT MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(MC_serverBD_1.iControlVehTimer[sVehState.iIndex])
									MC_START_NET_TIMER_WITH_TIMESTAMP(MC_serverBD_1.iControlVehTimer[sVehState.iIndex])
									NET_PRINT("MANAGE_SERVER_VEH_CAPTURE_BY_DISTANCE_EVERY_FRAME starting control timer for veh: ")  NET_PRINT_INT(sVehState.iIndex) NET_NL()
								ELSE
									IF (MC_GET_NET_TIMER_DIFFERENCE_WITH_TIME_STAMP(MC_serverBD_1.iControlVehTimer[sVehState.iIndex]) > MC_serverBD.iVehTakeoverTime[iCaptureTeam][MC_serverBD_4.iVehPriority[sVehState.iIndex][iCaptureTeam]])
										BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_TEAM_CON_VEH, 0, iCaptureTeam, -1, tempPlayer, ci_TARGET_VEHICLE, sVehState.iIndex)
										
										REQUEST_INCREMENT_SERVER_REMOTE_PLAYER_SCORE(iPartThatHasControl, GET_FMMC_POINTS_FOR_TEAM(iCaptureTeam, MC_serverBD_4.iVehPriority[sVehState.iIndex][iCaptureTeam]), iCaptureTeam, MC_serverBD_4.iVehPriority[sVehState.iIndex][iCaptureTeam])										
										
										IF MC_serverBD_4.iCurrentHighestPriority[iCaptureTeam] < FMMC_MAX_RULES
										AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iCaptureTeam].iRuleBitsetEleven[MC_serverBD_4.iCurrentHighestPriority[iCaptureTeam]], ciBS_RULE11_SECUROHACK_REQUIRES_KILL)
											PRINTLN("MANAGE_SERVER_VEH_CAPTURE_BY_DISTANCE_EVERY_FRAME - Time to destroy veh: ", sVehState.iIndex)
											SET_BIT(MC_serverBD_4.iVehHackedReadyForDeathBS, sVehState.iIndex)
										ENDIF
										
										IF GET_VEHICLE_RESPAWNS(sVehState.iIndex) <= 0	
										AND MC_serverBD.iRuleReCapture[iCaptureTeam][MC_serverBD_4.iVehPriority[sVehState.iIndex][iCaptureTeam]] <= 0
											IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetFive, ciFMMC_VEHICLE5_HACKING_EXPLODE_ON_CAPTURE)
											OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetSeven, ciFMMC_VEHICLE7_REMOVE_CRITICAL_ON_HACK)
											OR (MC_serverBD_4.iCurrentHighestPriority[iCaptureTeam] < FMMC_MAX_RULES
											AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iCaptureTeam].iRuleBitsetEleven[MC_serverBD_4.iCurrentHighestPriority[iCaptureTeam]], ciBS_RULE11_SECUROHACK_REQUIRES_KILL))
												FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
													iPriority[iTeam] = MC_serverBD_4.iVehPriority[sVehState.iIndex][iTeam]
													
													IF iTeam = iCaptureTeam
														IF IS_BIT_SET(MC_serverBD.iVehTeamFailBitset[sVehState.iIndex], iTeam)
															CLEAR_BIT(MC_serverBD.iVehTeamFailBitset[sVehState.iIndex], iTeam)
															CLEAR_BIT(MC_serverBD.iMissionCriticalVeh[iTeam], sVehState.iIndex)
															NET_PRINT("MANAGE_SERVER_VEH_CAPTURE_BY_DISTANCE_EVERY_FRAME OBJECTIVE NOT FAILED FOR TEAM: ")NET_PRINT_INT(iCaptureTeam) NET_PRINT(" BECAUSE THEY CAPTURED VEH: ") NET_PRINT_INT(sVehState.iIndex) NET_NL()
															bWasObjective[iTeam] = TRUE
														ENDIF
													ELSE
														IF DOES_TEAM_LIKE_TEAM(iTeam, iCaptureTeam)
														AND MC_serverBD_4.ivehRule[sVehState.iIndex][iTeam] = FMMC_OBJECTIVE_LOGIC_CAPTURE	
															IF IS_BIT_SET(MC_serverBD.iVehTeamFailBitset[sVehState.iIndex], iTeam)
																CLEAR_BIT(MC_serverBD.iVehTeamFailBitset[sVehState.iIndex], iTeam)
																CLEAR_BIT(MC_serverBD.iMissionCriticalVeh[iTeam], sVehState.iIndex)
																bWasObjective[iTeam] = TRUE
															ENDIF
														ENDIF
													ENDIF
												ENDFOR
											ENDIF
											IF MC_serverBD_4.iCurrentHighestPriority[iCaptureTeam] < FMMC_MAX_RULES
												IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iCaptureTeam].iRuleBitsetEleven[MC_serverBD_4.iCurrentHighestPriority[iCaptureTeam]], ciBS_RULE11_SECUROHACK_REQUIRES_KILL)
													FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams-1)
														IF iTeam != iCaptureTeam
															IF MC_serverBD_4.ivehRule[sVehState.iIndex][iTeam] = FMMC_OBJECTIVE_LOGIC_CAPTURE	
																IF DOES_TEAM_LIKE_TEAM(iTeam, iCaptureTeam)
																	SET_TEAM_OBJECTIVE_OUTCOME(iTeam, iPriority[iTeam], TRUE)
																	PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_VEHICLE, sVehState.iIndex, iTeam, TRUE)
																ELSE
																	SET_TEAM_OBJECTIVE_OUTCOME(iTeam, iPriority[iTeam], FALSE)
																	PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_VEHICLE, sVehState.iIndex, iTeam, FALSE)
																ENDIF
															ENDIF
														ELSE
															SET_TEAM_OBJECTIVE_OUTCOME(iTeam, iPriority[iTeam], TRUE)
															PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_VEHICLE, sVehState.iIndex, iTeam, TRUE)
														ENDIF
														
														MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_VEH_CAPTURED
													ENDFOR
												ENDIF
											ENDIF
										ENDIF
										
										IF MC_serverBD_4.iVehPriority[sVehState.iIndex][iCaptureTeam] < FMMC_MAX_RULES
											IF MC_serverBD.iRuleReCapture[iCaptureTeam][MC_serverBD_4.iVehPriority[sVehState.iIndex][iCaptureTeam]] != UNLIMITED_CAPTURES_KILLS
												MC_serverBD.iRuleReCapture[iCaptureTeam][MC_serverBD_4.iVehPriority[sVehState.iIndex][iCaptureTeam]]--
											ENDIF
										ENDIF
										
										IF MC_serverBD_4.iCurrentHighestPriority[iCaptureTeam] < FMMC_MAX_RULES
											IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iCaptureTeam].iRuleBitsetEleven[MC_serverBD_4.iCurrentHighestPriority[iCaptureTeam]], ciBS_RULE11_SECUROHACK_REQUIRES_KILL)
												FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
													IF bWasObjective[iTeam] 
														IF IS_THIS_OBJECTIVE_OVER(ci_TARGET_VEHICLE, iTeam, iPriority[iTeam])
															SET_LOCAL_OBJECTIVE_END_MESSAGE(iTeam, iPriority[iTeam], MC_serverBD.iReasonForObjEnd[iTeam])
															BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[iTeam], iTeam, iPriority[iTeam], DID_TEAM_PASS_OBJECTIVE(iTeam,iPriority[iTeam]))
														ENDIF
													ENDIF
												ENDFOR
											ENDIF
										ENDIF
										
										IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetFive, ciFMMC_VEHICLE5_HACKING_EXPLODE_ON_CAPTURE)
											SET_BIT(MC_serverBD_4.iVehExplodeKillOnCaptureBS, sVehState.iIndex)
										ENDIF
										
										SET_BIT(MC_serverBD_4.iVehUnlockOnCapture, sVehState.iIndex)
										
										MC_serverBD_4.iCaptureTimestamp = 0

										NET_PRINT("MANAGE_SERVER_VEH_CAPTURE_BY_DISTANCE_EVERY_FRAME Control complete for veh: ")  NET_PRINT_INT(sVehState.iIndex) NET_NL()
										MC_STOP_NET_TIMER_WITH_TIMESTAMP(MC_serverBD_1.iControlVehTimer[sVehState.iIndex])
										MC_STOP_NET_TIMER_WITH_TIMESTAMP(MC_serverBD.iControlVehTimerOffset[sVehState.iIndex])
									ENDIF
								ENDIF
							ELSE
								IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetFive,  ciFMMC_VEHICLE5_HACKING_RESET_CAPTURE_OUT_OF_RANGE)
									MC_STOP_NET_TIMER_WITH_TIMESTAMP(MC_serverBD_1.iControlVehTimer[sVehState.iIndex])
								ELSE
									IF NOT MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(MC_serverBD.iControlVehTimerOffset[sVehState.iIndex])
									AND MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(MC_serverBD_1.iControlVehTimer[sVehState.iIndex])
										PRINTLN("MANAGE_SERVER_VEH_CAPTURE_BY_DISTANCE_EVERY_FRAME - veh ", sVehState.iIndex," starting offset timer 5")
										PRINTLN("MANAGE_SERVER_VEH_CAPTURE_BY_DISTANCE_EVERY_FRAME Timer: ", MC_GET_NET_TIMER_DIFFERENCE_WITH_TIME_STAMP(MC_serverBD_1.iControlVehTimer[sVehState.iIndex]))
										MC_REINIT_NET_TIMER_WITH_TIMESTAMP(MC_serverBD.iControlVehTimerOffset[sVehState.iIndex])
									ENDIF
								ENDIF
							ENDIF
						ELSE
							IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetFive,  ciFMMC_VEHICLE5_HACKING_RESET_CAPTURE_OUT_OF_RANGE)
								MC_STOP_NET_TIMER_WITH_TIMESTAMP(MC_serverBD_1.iControlVehTimer[sVehState.iIndex])
							ELSE
								IF NOT MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(MC_serverBD.iControlVehTimerOffset[sVehState.iIndex])
								AND MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(MC_serverBD_1.iControlVehTimer[sVehState.iIndex])
									PRINTLN("MANAGE_SERVER_VEH_CAPTURE_BY_DISTANCE_EVERY_FRAME - veh ", sVehState.iIndex," starting offset timer 4")
									PRINTLN("MANAGE_SERVER_VEH_CAPTURE_BY_DISTANCE_EVERY_FRAME Timer: ", MC_GET_NET_TIMER_DIFFERENCE_WITH_TIME_STAMP(MC_serverBD_1.iControlVehTimer[sVehState.iIndex]))
									MC_REINIT_NET_TIMER_WITH_TIMESTAMP(MC_serverBD.iControlVehTimerOffset[sVehState.iIndex])
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetFive,  ciFMMC_VEHICLE5_HACKING_RESET_CAPTURE_OUT_OF_RANGE)
							MC_STOP_NET_TIMER_WITH_TIMESTAMP(MC_serverBD_1.iControlVehTimer[sVehState.iIndex])
						ELSE
							IF NOT MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(MC_serverBD.iControlVehTimerOffset[sVehState.iIndex])
							AND MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(MC_serverBD_1.iControlVehTimer[sVehState.iIndex])
								PRINTLN("MANAGE_SERVER_VEH_CAPTURE_BY_DISTANCE_EVERY_FRAME - veh ", sVehState.iIndex," starting offset timer 3")
								PRINTLN("MANAGE_SERVER_VEH_CAPTURE_BY_DISTANCE_EVERY_FRAME Timer: ", MC_GET_NET_TIMER_DIFFERENCE_WITH_TIME_STAMP(MC_serverBD_1.iControlVehTimer[sVehState.iIndex]))
								MC_REINIT_NET_TIMER_WITH_TIMESTAMP(MC_serverBD.iControlVehTimerOffset[sVehState.iIndex])
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetFive,  ciFMMC_VEHICLE5_HACKING_RESET_CAPTURE_OUT_OF_RANGE)
						MC_STOP_NET_TIMER_WITH_TIMESTAMP(MC_serverBD_1.iControlVehTimer[sVehState.iIndex])
					ELSE
						IF NOT MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(MC_serverBD.iControlVehTimerOffset[sVehState.iIndex])
						AND MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(MC_serverBD_1.iControlVehTimer[sVehState.iIndex])
							PRINTLN("MANAGE_SERVER_VEH_CAPTURE_BY_DISTANCE_EVERY_FRAME - veh ", sVehState.iIndex," starting offset timer 2")
							PRINTLN("MANAGE_SERVER_VEH_CAPTURE_BY_DISTANCE_EVERY_FRAME Timer: ", MC_GET_NET_TIMER_DIFFERENCE_WITH_TIME_STAMP(MC_serverBD_1.iControlVehTimer[sVehState.iIndex]))
							MC_REINIT_NET_TIMER_WITH_TIMESTAMP(MC_serverBD.iControlVehTimerOffset[sVehState.iIndex])
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetFive,  ciFMMC_VEHICLE5_HACKING_RESET_CAPTURE_OUT_OF_RANGE)
					MC_STOP_NET_TIMER_WITH_TIMESTAMP(MC_serverBD_1.iControlVehTimer[sVehState.iIndex])
				ELSE
					IF NOT MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(MC_serverBD.iControlVehTimerOffset[sVehState.iIndex])
					AND MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(MC_serverBD_1.iControlVehTimer[sVehState.iIndex])
						PRINTLN("MANAGE_SERVER_VEH_CAPTURE_BY_DISTANCE_EVERY_FRAME - veh ", sVehState.iIndex," starting offset timer 1")
						PRINTLN("MANAGE_SERVER_VEH_CAPTURE_BY_DISTANCE_EVERY_FRAME Timer: ", MC_GET_NET_TIMER_DIFFERENCE_WITH_TIME_STAMP(MC_serverBD_1.iControlVehTimer[sVehState.iIndex]))
						MC_REINIT_NET_TIMER_WITH_TIMESTAMP(MC_serverBD.iControlVehTimerOffset[sVehState.iIndex])
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL HAVE_VEHICLE_MODELS_LOADED(INT iVeh)
	
	BOOL bModelLoaded = TRUE
	
	REQUEST_MODEL(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn)
	
	IF NOT HAS_MODEL_LOADED(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn)
		bModelLoaded = FALSE
	ENDIF
	
	MODEL_NAMES mnCargo = GET_VEHICLE_CARGO_MODEL_FROM_INT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCargo, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn)
	IF mnCargo != DUMMY_MODEL_FOR_SCRIPT
		PRINTLN("[RCC MISSION] HAVE_VEHICLE_MODELS_LOADED - VEHICLE HAS VALID CRATE:", iVeh)
		REQUEST_MODEL(mnCargo)
		IF NOT HAS_MODEL_LOADED(mnCargo)
			bModelLoaded = FALSE
		ENDIF
	ENDIF
	
	RETURN bModelLoaded

ENDFUNC

FUNC FLOAT GET_AIR_VEHICLE_VISIBLE_RANGE_CHECK(INT iveh)
	
	SWITCH iVehSpawnFailCount[iveh]
		CASE 0
			RETURN 300.0
		BREAK
		
		CASE 1
			RETURN 250.0
		BREAK
		
		CASE 2
			RETURN 200.0
		BREAK
		
		CASE 3
			RETURN 150.0
		BREAK
		
		CASE 4
			RETURN 100.0
		BREAK
		
		CASE 5
			RETURN 75.0
		BREAK
		
		CASE 6
			RETURN 50.0
		BREAK
		
		CASE 7
			RETURN 25.0
		BREAK
		
		CASE 8
			RETURN 10.0
		BREAK
	ENDSWITCH
	
	RETURN 1.0
ENDFUNC

FUNC BOOL RUN_VEH_VISIBLE_CHECK(INT iveh)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitSet,ciFMMC_VEHICLE_IgnoreVisCheck)
		PRINTLN("[RCC MISSION] RUN_VEH_VISIBLE_CHECK - Ignoring vis check on veh: ",iveh, " because of ciFMMC_VEHICLE_IgnoreVisCheck")
		RETURN TRUE
	ELSE
		FLOAT fVisDist = 120.0
		
		IF (IS_THIS_MODEL_A_HELI(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn) OR IS_THIS_MODEL_A_PLANE(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn))
		AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetThree, ciFMMC_VEHICLE3_VehicleStartsAirborne)
			fVisDist = GET_AIR_VEHICLE_VISIBLE_RANGE_CHECK(iveh)
		ENDIF
		
		IF IS_POINT_OK_FOR_NET_ENTITY_CREATION(vEntityRespawnLoc, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, fVisDist)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC VECTOR GET_VEHICLE_FMMC_SPAWN_LOCATION(INT iVeh, BOOL& bIgnoreAreaCheck, INT &iSpawnInterior)
	
	VECTOR vSpawnLoc = MC_GET_VEH_SPAWN_LOCATION(iVeh, iSpawnRandomHeadingToSelect, iSpawnInterior)
	bIgnoreAreaCheck = FALSE
	
	IF IS_BIT_SET(MC_serverBD_2.iVehSpawnedOnce, iveh)
	AND NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].vSecondSpawnPos)
		IF GET_RANDOM_INT_IN_RANGE(0, 100) < 50
			SET_BIT(iLocalBoolCheck10, LBOOL10_VEH_SPAWN_AT_SECOND_POSITION)
			vSpawnLoc = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].vSecondSpawnPos
			iSpawnInterior = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iSecondInterior
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iSpawnNearType != ciRULE_TYPE_NONE
	AND (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iSpawnNearEntityID > -1 OR g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iSpawnNearType = ciSPAWN_NEAR_ENTITY_TYPE_LAST_PLAYER OR g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iSpawnNearType = ciSPAWN_NEAR_ENTITY_TYPE_LOBBY_LEADER)		
		IF (NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetTwo, ciFMMC_VEHICLE2_FirstSpawnNotNearEntity))
		OR IS_BIT_SET(MC_serverBD_2.iVehSpawnedOnce, iveh)			
			VECTOR vNewSpawn = GET_FMMC_ENTITY_LOCATION(vSpawnLoc, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iSpawnNearType, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iSpawnNearEntityID, DEFAULT, iVeh)
			
			IF NOT IS_VECTOR_ZERO(vNewSpawn)
				vSpawnLoc = vNewSpawn
				bIgnoreAreaCheck = FALSE
			ENDIF		
		ENDIF		
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetSix, ciFMMC_VEHICLE6_ALWAYS_FORCE_ORIGINAL_RESPAWN_POS)
		vSpawnLoc = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].vPos
		iSpawnInterior = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehInterior
		bIgnoreAreaCheck = TRUE
	ENDIF
	
	RETURN vSpawnLoc
	
ENDFUNC

FUNC BOOL IS_DESTROYED_VEHICLE_READY_FOR_CLEANUP(INT iVeh)
	
	BOOL bReady = TRUE
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn = GBURRITO
	OR g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn = GBURRITO2
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetTwo, ciFMMC_VEHICLE2_EXTRA_MINI_SERVER) //Check if the broken server prop has already been created
		AND NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicleCrate[iveh])
			
			MODEL_NAMES mnBrokenCrate = HEI_PROP_MINI_SEVER_BROKEN
			
			MODEL_NAMES mnCurrentCrate = GET_ENTITY_MODEL(NET_TO_ENT(MC_serverBD_1.sFMMC_SBD.niVehicleCrate[iveh]))
			
			IF mnCurrentCrate != mnBrokenCrate
				bReady = FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iVehUsesCleanupDelayForDialogue, iVeh)
		SET_BIT(ciDialogueInstantLoopConditionBS, ciDialogueInstantLoopCondition_VehicleHealthThreshold)
		IF NOT HAS_NET_TIMER_STARTED(tdVehDialogueDelayTimer)
			START_NET_TIMER(tdVehDialogueDelayTimer)
		ENDIF
		IF HAS_NET_TIMER_STARTED(tdVehDialogueDelayTimer)
		AND HAS_NET_TIMER_EXPIRED_READ_ONLY(tdVehDialogueDelayTimer, 3000)
			RESET_NET_TIMER(tdVehDialogueDelayTimer)
		ELSE
			PRINTLN("[RCC MISSION][PROCESS_VEH_BRAIN] IS_DESTROYED_VEHICLE_READY_FOR_CLEANUP - Vehicle is undriveable but we are waiting on cleanup due to dialogue needing to check it.")
			bReady = FALSE
		ENDIF
	ENDIF
	
	IF IS_TEAM_INDEX_VALID(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCleanupDelayTeam)
	AND IS_RULE_INDEX_VALID(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCleanupDelayRule)
		IF GET_TEAM_CURRENT_RULE(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCleanupDelayTeam) < g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCleanupDelayRule
			PRINTLN("[RCC MISSION][PROCESS_VEH_BRAIN] IS_DESTROYED_VEHICLE_READY_FOR_CLEANUP - Waiting for rule to clean up.")
			bReady = FALSE
		ENDIF
	ENDIF
	
	RETURN bReady
	
ENDFUNC

FUNC BOOL ARE_WE_WAITING_FOR_VEHICLES_TO_CLEAN_UP()
	
	BOOL bWaiting
	
	IF DOES_ANY_TEAM_HAVE_NEW_PRIORITY_THIS_FRAME(TRUE)
	OR IS_BIT_SET(iLocalBoolCheck7, LBOOL7_HOST_APPLIED_NEW_PRIORITY_MIDFRAME)
	OR IS_BIT_SET(iLocalBoolCheck11, LBOOL11_HOST_APPLIED_NEW_MIDPOINT_MIDFRAME)
	OR MC_serverBD.iVehCleanup_NeedOwnershipBS != 0
		bWaiting = TRUE
	ENDIF
	
	RETURN bWaiting
	
ENDFUNC

PROC CLEANUP_VEHICLE_RESPAWNING_DATA(INT iVeh, BOOL bResetFails = TRUE)
	
	CLEANUP_GENERAL_RESPAWNING_DATA(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn)
	
	MC_serverBD.isearchingforVeh = -1
	
	iSpawnPoolVehIndex = -1
	iSpawnRandomHeadingToSelect = -1
	
	IF bResetFails
		PRINTLN("[MCSpawning][Vehicles][Vehicle ", iVeh, "] CLEANUP_VEHICLE_RESPAWNING_DATA - bResetFails = true. Resetting Veh Spawn Fail Count and Fail Delay.")
		iVehSpawnFailCount[iveh] = 0
		iVehSpawnFailDelay[iveh] = -1
	ENDIF
	
ENDPROC

PROC PROCESS_VEHICLE_TRAIN_ATTACHMENT_EVERY_FRAME(FMMC_VEHICLE_STATE &sVehState)
	
	IF NOT sVehState.bExists
	OR NOT sVehState.bHasControl
		EXIT
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].sTrainAttachmentData.iTrainIndex = -1
		// This object isn't set up to be attached to a train in any way
		EXIT
	ENDIF
	
	PROCESS_TRAIN_ATTACHMENT(sVehState.vehIndex, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].sTrainAttachmentData, TRUE)
	
ENDPROC

FUNC BOOL RESPAWN_MISSION_VEHICLE(INT iveh)

	IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh])
		SET_BIT(MC_serverBD.iVehSpawnBitset, iveh)
		RETURN TRUE
	ENDIF
	
	FLOAT fSpawnHeading
	BOOL bWaterNode
	BOOL bRoadNode
	BOOL bForFlyingVehicle
	BOOL bIninterior
	FLOAT fMaxZUnderRaw
	VEHICLE_INDEX tempveh
	BOOL bIgnoreAreaCheck
	BOOL bShouldForceSpawn = (SHOULD_FORCE_IMMEDIATE_ENTITY_SPAWN(iVeh, CREATION_TYPE_VEHICLES) OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetEight, ciFMMC_VEHICLE8_AlwaysSpawnAtAuthoredPosition))
		
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sTrainAttachmentData.iTrainIndex != -1
	AND NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sTrainAttachmentData.vCarriageOffsetPosition)		
		bShouldForceSpawn = TRUE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn = DUMMY_MODEL_FOR_SCRIPT
		RETURN FALSE
	ENDIF
	
	// If we want to instantly respawn this veh
	IF IS_BIT_SET(MC_serverBD.iVehDelayRespawnBitset, iveh)
	OR iVehSpawnFailDelay[iveh] != -1		
		IF (iVehSpawnFailDelay[iveh] >= 1000)
			iVehSpawnFailDelay[iveh] = -1
			PRINTLN("[MCSpawning][Vehicles][Vehicle ", iVeh, "] RESPAWN_MISSION_VEHICLE - Reached iVehSpawnFailDelay of 1 second, resetting spawn delay timer")
		ELIF iVehSpawnFailDelay[iveh] != -1
			iVehSpawnFailDelay[iveh] += ROUND(fLastFrameTime * 1000)
		ENDIF
		RETURN FALSE
	ENDIF
	
	PRINTLN("[MCSpawning][Vehicles][Vehicle ", iVeh, "] RESPAWN_MISSION_VEHICLE - requesting model for veh to respawn. Model = ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn))
	IF NOT HAVE_VEHICLE_MODELS_LOADED(iveh)	
		PRINTLN("[MCSpawning][Vehicles][Vehicle ", iVeh, "] RESPAWN_MISSION_VEHICLE - waiting for model to load. model = ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn))	
		RETURN FALSE
	ENDIF
	
	IF NOT CAN_REGISTER_MISSION_VEHICLES(1)
	OR ARE_WE_WAITING_FOR_VEHICLES_TO_CLEAN_UP()
	#IF IS_DEBUG_BUILD	
		IF NOT CAN_REGISTER_MISSION_VEHICLES(1)
			PRINTLN("[MCSpawning][Vehicles][Vehicle ", iVeh, "] RESPAWN_MISSION_VEHICLE - failing to spawn veh as CAN_REGISTER_MISSION_VEHICLES is failing")
		ENDIF
		IF ARE_WE_WAITING_FOR_VEHICLES_TO_CLEAN_UP()
			IF DOES_ANY_TEAM_HAVE_NEW_PRIORITY_THIS_FRAME(TRUE)
				PRINTLN("[MCSpawning][Vehicles][Vehicle ", iVeh, "] RESPAWN_MISSION_VEHICLE - failing to spawn veh because of ARE_WE_WAITING_FOR_VEHICLES_TO_CLEAN_UP: new priority / midpoint this frame")
			ENDIF
			IF IS_BIT_SET(iLocalBoolCheck7, LBOOL7_HOST_APPLIED_NEW_PRIORITY_MIDFRAME)
			OR IS_BIT_SET(iLocalBoolCheck11, LBOOL11_HOST_APPLIED_NEW_MIDPOINT_MIDFRAME)
				PRINTLN("[MCSpawning][Vehicles][Vehicle ", iVeh, "] RESPAWN_MISSION_VEHICLE - failing to spawn veh because of ARE_WE_WAITING_FOR_VEHICLES_TO_CLEAN_UP: new priority / midpoint set by host")
			ENDIF
			IF MC_serverBD.iVehCleanup_NeedOwnershipBS != 0
				PRINTLN("[MCSpawning][Vehicles][Vehicle ", iVeh, "] RESPAWN_MISSION_VEHICLE - failing to spawn veh because of ARE_WE_WAITING_FOR_VEHICLES_TO_CLEAN_UP: iVehCleanup_NeedOwnershipBS = ",MC_serverBD.iVehCleanup_NeedOwnershipBS)
			ENDIF
		ENDIF
	#ENDIF
		RETURN FALSE
	ENDIF

	IF NOT CAN_RUN_ENTITY_SPAWN_POS_SEARCH(ciEntitySpawnSearchType_Vehicles, iveh)
		PRINTLN("[MCSpawning][Vehicles][Vehicle ", iVeh, "] RESPAWN_MISSION_VEHICLE - failing to spawn because attempting to spawn one of the following: ped: ", MC_serverBD.isearchingforPed," / veh ", MC_serverBD.isearchingforVeh ," / obj ", MC_serverBD.isearchingforObj)
		RETURN FALSE
	ENDIF

	IF MC_serverBD.isearchingforVeh != iVeh
		MC_serverBD.isearchingforVeh = iVeh
		PRINTLN("[MCSpawning][Vehicles][Vehicle ", iVeh, "] Assigning isearchingforVeh: ", MC_serverBD.isearchingforVeh)
	ENDIF

	// Initialise
	IF iEntityRespawnType != ci_TARGET_VEHICLE
	OR iEntityRespawnID != iveh
		vEntityRespawnLoc = <<0, 0, 0>>
		iEntityRespawnInterior = -1
		SpawnInterior = NULL
		iEntityRespawnType = ci_TARGET_VEHICLE
		iEntityRespawnID = iveh
	ENDIF
	
	IF IS_VECTOR_ZERO(vEntityRespawnLoc)
		CLEAR_BIT(iLocalBoolCheck10, LBOOL10_VEH_SPAWN_AT_SECOND_POSITION)
		vEntityRespawnLoc = GET_VEHICLE_FMMC_SPAWN_LOCATION(iveh, bIgnoreAreaCheck, iEntityRespawnInterior)
		PRINTLN("[MCSpawning][Vehicles][Vehicle ", iVeh, "] RESPAWN_MISSION_VEHICLE - (Initial Assignment) Using vEntityRespawnLoc = ", vEntityRespawnLoc)
		IF bIgnoreAreaCheck
			SET_BIT(iLocalBoolCheck4, LBOOL4_RESPAWN_SHOULD_FORCE_AREA_CHECK)
		ELSE
			CLEAR_BIT(iLocalBoolCheck4, LBOOL4_RESPAWN_SHOULD_FORCE_AREA_CHECK)
		ENDIF
	ELSE
		bIgnoreAreaCheck = IS_BIT_SET(iLocalBoolCheck4, LBOOL4_RESPAWN_SHOULD_FORCE_AREA_CHECK)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetNine, ciFMMC_VEHICLE9_RespawningForceSpawnSearch)
	AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetSix, ciFMMC_VEHICLE6_ALWAYS_FORCE_ORIGINAL_RESPAWN_POS)
		PRINTLN("[MCSpawning][Vehicles][Vehicle ", iVeh, "] RESPAWN_MISSION_VEHICLE - Respawn Search Area is being forced.")
		bIgnoreAreaCheck = FALSE
	ENDIF
	
	fSpawnHeading = MC_GET_VEH_SPAWN_HEADING(iveh, IS_BIT_SET(iLocalBoolCheck10, LBOOL10_VEH_SPAWN_AT_SECOND_POSITION), iSpawnRandomHeadingToSelect, FALSE)
			
	IF NOT bIgnoreAreaCheck
	AND NOT NETWORK_ENTITY_AREA_DOES_EXIST(MC_serverBD.iSpawnArea)
	AND NOT bShouldForceSpawn
		PRINTLN("[MCSpawning][Vehicles][Vehicle ", iVeh, "] RESPAWN_MISSION_VEHICLE - is creating a network area check at coords: ", vEntityRespawnLoc)
		VECTOR vMin, vMax
		
		GET_MODEL_DIMENSIONS(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn,vMin,vMax)
		
		FLOAT fWidth = vMax.x - vMin.x
		
		vMax = <<0,vMax.y,vMax.z>>
		vMin = <<0,vMin.y,vMin.z>>
		
		vMax = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vEntityRespawnLoc,fSpawnHeading,vMax)
		vMin = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vEntityRespawnLoc,fSpawnHeading,vMin)
		
		MC_serverBD.iSpawnArea = NETWORK_ADD_ENTITY_ANGLED_AREA(vMax,vMin,fWidth)
		REINIT_NET_TIMER(MC_serverBD.tdSpawnAreaTimeout)
		RETURN FALSE
	ENDIF

	IF NOT (bIgnoreAreaCheck
	OR NETWORK_ENTITY_AREA_HAVE_ALL_REPLIED(MC_serverBD.iSpawnArea)
	OR bShouldForceSpawn)
		
		IF (NOT HAS_NET_TIMER_STARTED(MC_serverBD.tdSpawnAreaTimeout))
		OR GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdSpawnAreaTimeout) >= ciFMMCSpawnAreaTimeout
			
			PRINTLN("[MCSpawning][Vehicles][Vehicle ", iVeh, "] RESPAWN_MISSION_VEHICLE - waited for everyone to reply back on entity area for over 10s, delete and try again")
			NETWORK_REMOVE_ENTITY_AREA(MC_serverBD.iSpawnArea)
			MC_serverBD.iSpawnArea = -1
			RESET_NET_TIMER(MC_serverBD.tdSpawnAreaTimeout)
			
		#IF IS_DEBUG_BUILD
		ELSE
			PRINTLN("[MCSpawning][Vehicles][Vehicle ", iVeh, "] RESPAWN_MISSION_VEHICLE - waiting for everyone to reply back on entity area, waited for ",GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdSpawnAreaTimeout),"ms")
		#ENDIF
		ENDIF
		
		RETURN FALSE
	ENDIF

	PRINTLN("[MCSpawning][Vehicles][Vehicle ", iVeh, "] RESPAWN_MISSION_VEHICLE - bShouldForceSpawn: ", bShouldForceSpawn, " bIgnoreAreaCheck: ", bIgnoreAreaCheck, " AreaCheckReplied: ", NETWORK_ENTITY_AREA_HAVE_ALL_REPLIED(MC_serverBD.iSpawnArea))
	
	IF NOT bIgnoreAreaCheck
	AND NOT bShouldForceSpawn
	AND (NETWORK_ENTITY_AREA_IS_OCCUPIED(MC_serverBD.iSpawnArea)
	OR NOT RUN_VEH_VISIBLE_CHECK(iveh)
	OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetNine, ciFMMC_VEHICLE9_RespawningForceSpawnSearch))
	
		PRINTLN("[MCSpawning][Vehicles][Vehicle ", iVeh, "] RESPAWN_MISSION_VEHICLE - Attempting to find an appropriate new location for the vehicle.")
		
		IF IS_THIS_MODEL_A_BOAT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn)
		OR g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn = SUBMERSIBLE
			bWaterNode = TRUE
			PRINTLN("[MCSpawning][Vehicles][Vehicle ", iVeh, "] RESPAWN_MISSION_VEHICLE - Water Nodes set up by Default.")
		ELIF (IS_THIS_MODEL_A_HELI(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn) OR IS_THIS_MODEL_A_PLANE(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn))
		AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetThree, ciFMMC_VEHICLE3_VehicleStartsAirborne)					
			PRINTLN("[MCSpawning][Vehicles][Vehicle ", iVeh, "] RESPAWN_MISSION_VEHICLE - bForFlyingVehicle = TRUE ")
			bForFlyingVehicle = TRUE 
		ELSE
			PRINTLN("[MCSpawning][Vehicles][Vehicle ", iVeh, "] RESPAWN_MISSION_VEHICLE - Road Nodes set up by default.")
			bRoadNode = TRUE
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetNine, ciFMMC_VEHICLE9_RespawningForceRoadNodes)
			PRINTLN("[MCSpawning][Vehicles][Vehicle ", iVeh, "] RESPAWN_MISSION_VEHICLE - Forcing Road Node in Search.")
			bRoadNode = TRUE
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetNine, ciFMMC_VEHICLE9_RespawningForcewWaterNodes)
			PRINTLN("[MCSpawning][Vehicles][Vehicle ", iVeh, "] RESPAWN_MISSION_VEHICLE - Forcing Water Node in Search.")
			bWaterNode = TRUE
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetNine, ciFMMC_VEHICLE9_RespawningForcewNavmeshSearch)
			PRINTLN("[MCSpawning][Vehicles][Vehicle ", iVeh, "] RESPAWN_MISSION_VEHICLE - Forcing use of Navmesh in Search.")
			bRoadNode = FALSE
			bWaterNode = FALSE
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetFive, ciFMMC_VEHICLE5_CONSTRAINT_Z_SPAWN)
			PRINTLN("[MCSpawning][Vehicles][Vehicle ", iVeh, "] RESPAWN_MISSION_VEHICLE - Checking Z Spawn Constraints.")
			FLOAT fDiff =  ABSF(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].vPos.z - vEntityRespawnLoc.z)					
			IF fDiff > 10.0
				fMaxZUnderRaw = 5.0
			ENDIF				
		ENDIF
		
		IF SpawnInterior = NULL					
			SpawnInterior = GET_INTERIOR_AT_COORDS(vEntityRespawnLoc)
		ENDIF
		
		IF SpawnInterior = NULL
			bIninterior = FALSE
		ELSE
			bIninterior = TRUE
		ENDIF

		PRINTLN("[MCSpawning][Vehicles][Vehicle ", iVeh, "] RESPAWN_MISSION_VEHICLE - bIninterior: ", bIninterior)
		
		SPAWN_SEARCH_PARAMS SpawnSearchParams
		SpawnSearchParams.bConsiderInteriors = bIninterior
		SpawnSearchParams.fMinDistFromPlayer = 20
		SpawnSearchParams.bCloseToOriginAsPossible = TRUE
		SpawnSearchParams.bConsiderOriginAsValidPoint = TRUE
		SpawnSearchParams.bSearchVehicleNodesOnly = bRoadNode
		SpawnSearchParams.bUseOnlyBoatNodes = bWaterNode
		SpawnSearchParams.fHeadingForConsideredOrigin = MC_GET_VEH_SPAWN_HEADING(iveh, DEFAULT, DEFAULT, FALSE)
		SpawnSearchParams.fMaxZBelowRaw = fMaxZUnderRaw
		SpawnSearchParams.bIsForFlyingVehicle = bForFlyingVehicle
		
		FLOAT fSearchRadius
		fSearchRadius = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].fRespawnRange
		
		IF NOT bIgnoreAreaCheck
		OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetNine, ciFMMC_VEHICLE9_RespawningForceSpawnSearch)
			SpawnSearchParams.vFacingCoords = vEntityRespawnLoc
			//Want to spawn outside of a range from vEntityRespawnLoc:
			SpawnSearchParams.vAvoidCoords[0] = vEntityRespawnLoc
			SpawnSearchParams.fAvoidRadius[0] = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].fRespawnMinRange
			SpawnSearchParams.bConsiderOriginAsValidPoint = FALSE
			SpawnSearchParams.bCloseToOriginAsPossible = FALSE																				
			IF (fSearchRadius < SpawnSearchParams.fAvoidRadius[0])
				fSearchRadius += 100.0
				PRINTLN("[MCSpawning][Vehicles][Vehicle ", iVeh, "] RESPAWN_MISSION_VEHICLE - fSearchRadius was smaller than avoid radius, increasing.")
			ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
		DEBUG_PRINT_SPAWN_SEARCH_PARAMS(SpawnSearchParams)
		#ENDIF
		
		//Generating a facing towards co-ords based on fSpawnHeading
		SpawnSearchParams.vFacingCoords = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vEntityRespawnLoc,fSpawnHeading,<<0,1,0>>)
		PRINTLN("[MCSpawning][Vehicles][Vehicle ", iVeh, "] RESPAWN_MISSION_VEHICLE - SpawnSearchParams.vFacingCoords is set to: ", SpawnSearchParams.vFacingCoords)
		
		PRINTLN("[MCSpawning][Vehicles][Vehicle ", iVeh, "] RESPAWN_MISSION_VEHICLE - Calling GET_SAFE_COORDS_IN_AREA_FOR_CREATING_ENTITY with iVeh: ", iVeh, " vEntityRespawnLoc: ", vEntityRespawnLoc, " fSearchRadius: ", fSearchRadius, " fAvoidRadius: ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].fRespawnMinRange)
		
		VECTOR vSpawnLoc
		IF GET_SAFE_COORDS_IN_AREA_FOR_CREATING_ENTITY(vEntityRespawnLoc,fSearchRadius,vSpawnLoc,fSpawnHeading,SpawnSearchParams)
								
			// if we're creating the vehicle in the air then add 100m to z
			IF (bForFlyingVehicle)
				vSpawnLoc.z += 100.0
				PRINTLN("[MCSpawning][Vehicles][Vehicle ", iVeh, "] RESPAWN_MISSION_VEHICLE - is set up to spawn airborne. Adding 100 to z.")
				
				IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitSet,ciFMMC_VEHICLE_IgnoreVisCheck)
				AND CAN_ANY_PLAYER_SEE_POINT(vSpawnLoc, 5.0, TRUE, TRUE, GET_AIR_VEHICLE_VISIBLE_RANGE_CHECK(iveh))
					
					PRINTLN("[MCSpawning][Vehicles][Vehicle ", iVeh, "] RESPAWN_MISSION_VEHICLE - a player can see new point in air at ",vSpawnLoc,", range check was ",GET_AIR_VEHICLE_VISIBLE_RANGE_CHECK(iveh))
					
					CLEANUP_VEHICLE_RESPAWNING_DATA(iVeh, FALSE)
					
					INT iteam
					BOOL crit
					
					//Check if this ped is marked as critical for any teams
					FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
						IF IS_BIT_SET(MC_serverBD.iMissionCriticalVeh[iteam], iveh)
							crit = TRUE
							BREAKLOOP
						ENDIF
					ENDFOR
					
					IF NOT crit
						IF iVehSpawnFailCount[iveh] < 2
							iVehSpawnFailCount[iveh]++
						ENDIF
						iVehSpawnFailDelay[iveh] = 0 //Start the delay timer
						PRINTLN("[MCSpawning][Vehicles][Vehicle ", iVeh, "] RESPAWN_MISSION_VEHICLE - coords visible, setting iVehSpawnFailDelay timer running ",iveh)
					ELSE
						iVehSpawnFailCount[iveh]++
					ENDIF
					
					SET_BIT(MC_serverBD.iVehDelayRespawnBitset, iveh)
					
					RETURN FALSE
				ENDIF
			ENDIF
			
			vEntityRespawnLoc = vSpawnLoc
			
			PRINTLN("[MCSpawning][Vehicles][Vehicle ", iVeh, "] RESPAWN_MISSION_VEHICLE - repawn has gone through - GET_SAFE_COORDS_IN_AREA_FOR_CREATING_ENTITY ")
																			
			IF NOT IS_VECTOR_ZERO(SpawnSearchParams.vFacingCoords)
				fSpawnHeading = GET_HEADING_BETWEEN_VECTORS(vSpawnLoc, SpawnSearchParams.vFacingCoords)
			ELSE
				PRINTLN("[MCSpawning][Vehicles][Vehicle ", iVeh, "] RESPAWN_MISSION_VEHICLE - SpawnSearchParams.vFacingCoords is <<0,0,0>> keeping fSpawnheading with fSpawnHeading : ", fSpawnHeading)
			ENDIF
			
			PRINTLN("[MCSpawning][Vehicles][Vehicle ", iVeh, "] RESPAWN_MISSION_VEHICLE - set up with vEntityRespawnLoc: ", vEntityRespawnLoc, " fSpawnHeading: ", fSpawnHeading)					
		ELSE
			RETURN FALSE
		ENDIF
	ENDIF
				
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetSix, ciFMMC_VEHICLE6_ALWAYS_FORCE_ORIGINAL_RESPAWN_POS)
		PRINTLN("[MCSpawning][Vehicles][Vehicle ", iVeh, "] RESPAWN_MISSION_VEHICLE - ciFMMC_VEHICLE6_ALWAYS_FORCE_ORIGINAL_RESPAWN_POS - Setting Coord: ", vEntityRespawnLoc, " and Heading: ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].fHead)
		vEntityRespawnLoc = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].vPos
		fSpawnHeading = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].fHead
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetThree, ciFMMC_VEHICLE3_DONT_CLEAR_AREA)
		FLOAT fClearRadius = 10.0
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetSeven, ciFMMC_VEHICLE7_LARGER_CLEAR_AREA)
			PRINTLN("[MCSpawning][Vehicles][Vehicle ", iVeh, "] RESPAWN_MISSION_VEHICLE - Using larger clear area on respawn.")
			fClearRadius = 30.0
		ENDIF
		CLEAR_AREA_LEAVE_VEHICLE_HEALTH(vEntityRespawnLoc, fClearRadius, FALSE, FALSE, FALSE, FALSE)
	ENDIF
	
	IF FMMC_CREATE_NET_VEHICLE(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh], g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn, vEntityRespawnLoc, fSpawnHeading, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, SHOULD_VEHICLE_CREATION_IGNORE_GROUND_CHECK(iveh, vEntityRespawnLoc, iEntityRespawnInterior))
		
		tempveh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh])
		IF IS_VEHICLE_MODEL(tempVeh, MULE)
			FMMC_SET_THIS_VEHICLE_EXTRAS(tempveh, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitSet, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iLivery)
		ELSE
			FMMC_SET_THIS_VEHICLE_EXTRAS(tempveh, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitSet)
		ENDIF
		
		FMMC_SET_THIS_VEHICLE_COLOURS(tempveh, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iColour,g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iLivery, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iColourCombination, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iColour2, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iColour3, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].fEnvEff, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCustomModSettingsIndex)
		MC_SET_UP_VEH(tempveh,iveh, MC_ServerBD_1.sFMMC_SBD.niVehicle, vEntityRespawnLoc, (iSpawnRandomHeadingToSelect > 0), iEntityRespawnInterior, DEFAULT, GET_TOTAL_STARTING_PLAYERS())
		SET_ENTITY_VISIBLE(tempveh,TRUE)		
		
		IF GET_VEHICLE_CARGO_MODEL_FROM_INT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCargo, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn) != DUMMY_MODEL_FOR_SCRIPT
			IF CREATE_THIS_VEHICLE_CARGO(MC_serverBD_1.sFMMC_SBD.niVehicleCrate[iveh], tempveh, iVeh)
				SET_ENTITY_VISIBLE(NET_TO_ENT(MC_serverBD_1.sFMMC_SBD.niVehicleCrate[iveh]), TRUE)
			ENDIF
		ENDIF
		
		SET_BIT(MC_serverBD.iVehSpawnBitset, iveh)
		
		CLEANUP_VEHICLE_RESPAWNING_DATA(iVeh)
		
		IF getNextSpawningPedAfterVehicleSpawn != NULL
			CALL getNextSpawningPedAfterVehicleSpawn(iVeh)	
		ENDIF
				
		MC_PROCESS_REPOSITION_ENTITY_BASED_ON_SUB_SPAWN_GROUP_SETTINGS(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sWarpLocationSettings, CREATION_TYPE_VEHICLES, iVeh, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iSpawnGroup, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iSpawnSubGroupBS, tempveh)	
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetSeven, ciFMMC_VEHICLE7_SPAWN_WITH_LINKED_VEH)
			IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iLinkedDestroyVeh  != -1
				IF IS_VEHICLE_MODEL(tempVeh, TRAILERSMALL2)
					INT iAttachVeh
					iAttachVeh = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iLinkedDestroyVeh
					IF IS_NET_VEHICLE_DRIVEABLE(MC_serverBD_1.sFMMC_SBD.niVehicle[iAttachVeh])
						VEHICLE_INDEX trailer = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh])
						FLOAT fOffset = 4.0
						IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iAttachVeh].mn = HAULER
						OR g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iAttachVeh].mn = HAULER2
							fOffset = 8.55 // Length of the hauler
						ENDIF
						SET_ENTITY_COORDS(trailer, GET_ENTITY_COORDS(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iAttachVeh])) - (GET_ENTITY_FORWARD_VECTOR(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iAttachVeh]))*fOffset))
						SET_ENTITY_HEADING(trailer, GET_ENTITY_HEADING(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iAttachVeh])))
						SET_VEHICLE_ON_GROUND_PROPERLY(trailer, 8.0)
						ATTACH_VEHICLE_TO_TRAILER(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iAttachVeh]), trailer)
						PRINTLN("[MCSpawning][Vehicles][Vehicle ", iVeh, "] RESPAWN_MISSION_VEHICLE - RESPAWN_WITH_TRAILER - Trailer attached!")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iAttachParent > -1
		AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iAttachParentType = CREATION_TYPE_VEHICLES

			IF IS_NET_VEHICLE_DRIVEABLE(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iAttachParent])
				IF GET_ENTITY_MODEL(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])) = CARGOBOB2
					FMMC_ATTACH_CARGOBOB_TO_VEHICLE(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh]),NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iAttachParent]),TRUE)
				ELSE
					FMMC_ATTACH_VEHICLE_TO_VEHICLE(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh]),NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iAttachParent]),TRUE)
				ENDIF
				PRINTLN("[MCSpawning][Vehicles][Vehicle ", iVeh, "] RESPAWN_MISSION_VEHICLE - attaching vehicle: ",iVeh, "to trailer: ",g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iAttachParent)
			ENDIF
		
		ENDIF
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].sTrainAttachmentData.iTrainIndex != -1
		AND NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].sTrainAttachmentData.vCarriageOffsetPosition)
			VECTOR vVehCoord
			VECTOR vVehRot
			
			MC_GET_TRAIN_ATTACHMENT_POINT_IN_WORLD_COORDS(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].sTrainAttachmentData, vVehCoord, vVehRot)
			
			FLOAT fTempZ
			IF GET_GROUND_Z_FOR_3D_COORD(vVehCoord, fTempZ)
				vVehCoord.z = fTempZ
			ENDIF
			
			SET_ENTITY_HEADING(tempveh, vVehRot.z)
			SET_ENTITY_COORDS(tempveh, vVehCoord)
			
			PRINTLN("[MCSpawning][Vehicles][Vehicle ", iVeh, "] RESPAWN_MISSION_VEHICLE - placed on a Train - Setting vVehCoord: ", vVehCoord, "  Heading: ", vVehRot.z)
		ENDIF
					
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetSeven, ciFMMC_VEHICLE7_DYNAMIC_FACILITY_SPAWN_OVERRIDE)
		AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn = STROMBERG
			PRINTLN("[MCSpawning][Vehicles][Vehicle ", iVeh, "] RESPAWN_MISSION_VEHICLE - Incrementing the iFacilityPosVehSpawned to: ", (MC_ServerBD_4.iFacilityPosVehSpawned+1))
			MC_ServerBD_4.iFacilityPosVehSpawned++
		ENDIF
		
		START_ENTITY_PTFX_AT_COORDS(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sEntitySpawnPTFX, GET_ENTITY_COORDS(NET_TO_ENT(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh])), GET_ENTITY_ROTATION(NET_TO_ENT(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh])))
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

//[FMMC2020] This needs merged into PROCESS_VEH_BRAIN when it is refactored
PROC PROCESS_SERVER_VEH_OBJECTIVES_OLD(INT iVeh)
	INT iTeam
	BOOL bcheckteamfail
	VEHICLE_INDEX tempVeh
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh])
		
		IF NOT IS_VEHICLE_DRIVEABLE(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh]))
		OR IS_TRAILER_STUCK(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh]))
			
			IF SHOULD_DEAD_VEHICLE_CHECK_TEAM_FAILS(iVeh)
				bcheckteamfail = TRUE
			ENDIF

		ELSE
			tempVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh])

		ENDIF
	ELSE
		
		IF SHOULD_DEAD_VEHICLE_CHECK_TEAM_FAILS(iVeh)
			bcheckteamfail = TRUE
		ENDIF
	ENDIF
	
	FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams-1)
	
		IF MC_serverBD_4.iVehPriority[iveh][iteam] < FMMC_MAX_RULES
			IF NOT bcheckteamfail
			OR (SHOULD_ENTITY_RESPAWN_THIS_RULE(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iRespawnOnRuleChangeBS, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iRespawnOnRule, iTeam) AND NOT IS_BIT_SET(MC_ServerBD.iServerBS_PassRuleVehAfterRespawn, iVeh))
				IF MC_serverBD_4.ivehRule[iveh][iteam] != FMMC_OBJECTIVE_LOGIC_NONE
					IF MC_serverBD_4.iVehPriority[iveh][iteam] <= iCurrentHighPriority[iteam]
						IF MC_serverBD_4.ivehRule[iveh][iteam] = FMMC_OBJECTIVE_LOGIC_GET_AND_HOLD
							IF MC_serverBD_4.iVehPriority[iveh][iteam] <= MC_serverBD_4.iCurrentHighestPriority[iteam]
								
								IF IS_ENTITY_IN_DROP_OFF_AREA(tempVeh, iteam, MC_serverBD_4.iVehPriority[iveh][iteam], ciRULE_TYPE_VEHICLE, iveh)
									IF NOT IS_BIT_SET(MC_serverBD.iVehAtYourHolding[iteam],iveh)
										IF MC_serverBD.iDriverPart[iveh] > -1
											IF MC_PlayerBD[MC_serverBD.iDriverPart[iveh]].iteam = iteam
												SET_BIT(MC_serverBD.ivehHoldPartPoints,MC_serverBD.iDriverPart[iveh])
												REQUEST_INCREMENT_SERVER_REMOTE_PLAYER_SCORE(MC_serverBD.iDriverPart[iveh], GET_FMMC_POINTS_FOR_TEAM(iteam, MC_serverBD_4.iVehPriority[iveh][iteam]), iteam, MC_serverBD_4.iVehPriority[iveh][iteam])													
												SET_BIT(MC_serverBD.iVehAtYourHolding[iteam],iveh)
											ENDIF
										ELSE
											SET_BIT(MC_serverBD.iVehAtYourHolding[iteam],iveh)
											INCREMENT_SERVER_TEAM_SCORE(iteam,MC_serverBD_4.iVehPriority[iveh][iteam],GET_FMMC_POINTS_FOR_TEAM(iteam,MC_serverBD_4.iVehPriority[iveh][iteam]))
										ENDIF
									ENDIF
								ELSE
									IF IS_BIT_SET(MC_serverBD.iVehAtYourHolding[iteam],iveh)
										CLEAR_BIT(MC_serverBD.iVehAtYourHolding[iteam],iveh)
										BOOL bRemovedPlayerScore
										INT iPart
										PRINTLN("[PLAYER_LOOP] - PROCESS_SERVER_VEH_OBJECTIVES_OLD")
										FOR ipart = 0 TO (NUM_NETWORK_PLAYERS-1)
											IF IS_BIT_SET(MC_serverBD.iVehHoldPartPoints,ipart)
													REQUEST_INCREMENT_SERVER_REMOTE_PLAYER_SCORE(ipart, (-1 * GET_FMMC_POINTS_FOR_TEAM(iteam, MC_serverBD_4.iVehPriority[iveh][iteam])), iteam, MC_serverBD_4.iVehPriority[iveh][iteam])													
													bRemovedPlayerScore = TRUE
												CLEAR_BIT(MC_serverBD.iVehHoldPartPoints,ipart)
											ENDIF
										ENDFOR
										IF NOT bRemovedPlayerScore
											INCREMENT_SERVER_TEAM_SCORE(iteam,MC_serverBD_4.iVehPriority[iveh][iteam], -GET_FMMC_POINTS_FOR_TEAM(iteam,MC_serverBD_4.iVehPriority[iveh][iteam]))
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	IF SHOULD_ENTITY_RESPAWN_THIS_RULE(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iRespawnOnRuleChangeBS, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iRespawnOnRule, iTeam)
	AND bcheckteamfail
	AND NOT IS_BIT_SET(MC_ServerBD.iServerBS_PassRuleVehAfterRespawn, iVeh)
		PRINTLN("[RCC MISSION][PROCESS_VEH_BRAIN] - Setting iServerBS_PassRuleVehAfterRespawn on iVeh: ",iveh)
		SET_BIT(MC_ServerBD.iServerBS_PassRuleVehAfterRespawn, iVeh)
	ENDIF
	
ENDPROC

FUNC BOOL HAS_VEH_PASSED_LEAVE_CONDITIONS_FOR_RULE(INT iVeh, INT iTeam)
	IF MC_serverBD_4.iVehRule[iVeh][iTeam] = FMMC_OBJECTIVE_LOGIC_LEAVE_ENTITY
	AND GET_MC_CLIENT_MISSION_STAGE(iLocalPart) = CLIENT_MISSION_STAGE_LEAVE_VEH
		INT iNumberOfPlayersLeft = GET_PLAYERS_LEFT_LEAVE_OBJECTIVE_ENTITY(iTeam, iVeh)
		INT iNumberOfPlayersRequiredToLeave = GET_NUMBER_OF_PLAYERS_REQUIRED_TO_LEAVE_ENTITY(iTeam)
	
	 	PRINTLN("[RCC MISSION][PROCESS_VEH_BRAIN] iVeh: ", iVeh, " - iNumberOfPlayersLeft: ", iNumberOfPlayersLeft, " iNumberOfPlayersRequiredToLeave: ", iNumberOfPlayersRequiredToLeave)
	
		IF iNumberOfPlayersLeft >= iNumberOfPlayersRequiredToLeave
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC PROCESS_VEH_BRAIN(FMMC_VEHICLE_STATE &sVehState)
	
	IF NOT bIsLocalPlayerHost
		EXIT
	ENDIF
	
	INT iVeh = sVehState.iIndex

	PED_INDEX tempPed
	PLAYER_INDEX tempPlayer
	PARTICIPANT_INDEX tempPart
	VEHICLE_INDEX tempTruck
	INT iVehTeam
	INT iTeam	
	INT iPart
	BOOL bCheckTeamFail
	INT iPriority[FMMC_MAX_TEAMS]
	BOOL bWasObjective[FMMC_MAX_TEAMS]
	
	PROCESS_SERVER_VEH_OBJECTIVES_OLD(iVeh) //[FMMC2020]
	
	IF MC_serverBD.iVehRequestPart[iVeh] != -1
	AND MC_playerBD[MC_serverBD.iVehRequestPart[iVeh]].iVehRequest = -1
		PRINTLN("[Vehicles][Vehicle ", sVehState.iIndex, "] PROCESS_VEH_BRAIN - Vehicle no longer being requested by part ", MC_serverBD.iVehRequestPart[iVeh])
		MC_serverBD.iVehRequestPart[iVeh] = -1
	ENDIF
	
	IF sVehState.bExists
		
		IF NOT sVehState.bDrivable
		OR IS_FMMC_VEHICLE_TRAILER_STUCK(sVehState)
		
			MC_STOP_NET_TIMER_WITH_TIMESTAMP(MC_serverBD_1.iControlVehTimer[iVeh])
			
			#IF IS_DEBUG_BUILD
			IF NOT bStopVehDeadCleanup
			#ENDIF			
			
			IF IS_DESTROYED_VEHICLE_READY_FOR_CLEANUP(iVeh)
				PRINTLN("[Vehicles][Vehicle ", sVehState.iIndex, "] PROCESS_VEH_BRAIN - Vehicle is undriveable (or it's a trailer and it's stuck) - cleaning up.")
				
				CLEANUP_NET_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
				CLEANUP_VEHICLE_CARGO(iVeh)
				
				SET_BIT(MC_serverBD.iPedRespawnVehBitset, iVeh)
				
			#IF IS_DEBUG_BUILD
			ELSE
				PRINTLN("[Vehicles][Vehicle ", sVehState.iIndex, "] PROCESS_VEH_BRAIN - Vehicle is undriveable (or it's a trailer and it's stuck) - waiting for IS_DESTROYED_VEHICLE_READY_FOR_CLEANUP.")
			#ENDIF
			ENDIF
			
			IF SHOULD_DEAD_VEHICLE_CHECK_TEAM_FAILS(iVeh)
				bCheckTeamFail = TRUE
			ENDIF
			
			#IF IS_DEBUG_BUILD
			ENDIF
			#ENDIF

		ELSE
			IF SHOULD_CLEANUP_VEH(sVehState)
			AND CLEANUP_VEH_EARLY(sVehState)
				EXIT
			ENDIF
			
			IF SHOULD_CLEANUP_DUE_TO_PLAYER_PROXIMITY(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sProximitySpawningData, iVeh, MC_serverBD_3.iProximitySpawning_VehCleanupBS)
				IF CLEANUP_VEH_EARLY(sVehState, FALSE)
					PRINTLN("[PROXSPAWN_SERVER][Vehicles][Vehicle ", sVehState.iIndex, "] PROCESS_VEH_BRAIN - Cleaning up vehicle due to proximity!")
					SET_VEHICLE_RESPAWNS(iVeh, GET_VEHICLE_RESPAWNS(iVeh) + 1)
					EXIT
				ENDIF
			ENDIF
		ENDIF
			
		PROCESS_SERVER_BURNING_VEHICLE(sVehState)
		
	ELSE
		IF IS_BIT_SET(MC_ServerBD.iVehAboutToCleanup_BS, iVeh)
			PRINTLN("[Vehicles][Vehicle ", sVehState.iIndex, "] PROCESS_VEH_BRAIN - Clearing iVehAboutToCleanup_BS.")
			CLEAR_BIT(MC_ServerBD.iVehAboutToCleanup_BS, iVeh)
		ENDIF
		
		IF SHOULD_DEAD_VEHICLE_CHECK_TEAM_FAILS(iVeh)
			bCheckTeamFail = TRUE
		ENDIF
	ENDIF
	
	FOR iTeam = 0 TO MC_serverBD.iNumberOfTeams - 1
		
		// Remove as an objective entity if we are meant to leave it and we are no longer near it.
		IF sVehState.bExists	
		AND HAS_VEH_PASSED_LEAVE_CONDITIONS_FOR_RULE(iVeh, iTeam)				
			PRINTLN("[Vehicles][Vehicle ", sVehState.iIndex, "] PROCESS_VEH_BRAIN - No player is near Veh.")
			bCheckTeamFail = TRUE
		ENDIF
		
		IF MC_serverBD_4.iVehPriority[iVeh][iTeam] < FMMC_MAX_RULES
		OR IS_BIT_SET(MC_serverBD.iMissionCriticalVeh[iTeam], iVeh)
			IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
				IF bCheckTeamFail
					CHECK_VEHICLE_FAILURE_FOR_TEAM(iVeh, iTeam, MC_serverBD_4.iVehPriority[iVeh][iTeam])
				ENDIF
			ENDIF
		ENDIF
		
		PRINTLN("[Vehicles][Vehicle ", sVehState.iIndex, "] PROCESS_VEH_BRAIN - iTeam: ", iTeam, " Respawns: ", MC_serverBD_2.iCurrentVehRespawnLives[iVeh], " iRespawnOnRuleChangeBS: ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iRespawnOnRuleChangeBS, " iServerBS_PassRuleVehAfterRespawn: ", IS_BIT_SET(MC_ServerBD.iServerBS_PassRuleVehAfterRespawn, iVeh)
			, " iVehPriority: ", MC_serverBD_4.iVehPriority[iVeh][iTeam], " iCurrentHighestPriority: ", MC_serverBD_4.iCurrentHighestPriority[iTeam], " iCurrentHigh: ", iCurrentHighPriority[iTeam])
		
		IF sVehState.bExists
		AND sVehState.bDrivable
			IF MC_serverBD_4.iVehPriority[iVeh][iTeam] < FMMC_MAX_RULES
			AND MC_serverBD_4.iVehPriority[iVeh][iTeam] > -1
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTen[MC_serverBD_4.iVehPriority[iVeh][iTeam]], ciBS_RULE10_PASS_RULE_THROUGH_TAGGING)
					IF IS_ENTITY_ALREADY_TAGGED(ENUM_TO_INT(ET_VEHICLE),iVeh)
						PRINTLN("[Vehicles][Vehicle ", sVehState.iIndex, "] PROCESS_VEH_BRAIN - VEHICLE HAS BEEN TAGGED. Calling PROGRESS_SPECIFIC_OBJECTIVE")
						INVALIDATE_SINGLE_OBJECTIVE(iTeam, MC_ServerBD_4.iVehPriority[iVeh][iTeam], TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
	ENDFOR
	
	IF NOT sVehState.bExists
	OR NOT sVehState.bDrivable
		MC_serverBD.iDriverPart[iVeh] = -1
		MC_serverBD.iVehRequestPart[iVeh] = -1
		EXIT
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFive, ciFMMC_VEHICLE5_HACKING_USE_DISTANCE_FOR_CAPTURE)
		EXIT
	ENDIF
	
	tempPed = GET_PED_IN_VEHICLE_SEAT(sVehState.vehIndex)
	IF IS_ENTITY_ATTACHED_TO_ANY_VEHICLE(sVehState.vehIndex)
		tempTruck = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_ATTACHED_TO(sVehState.vehIndex))
	ENDIF
	IF IS_VEHICLE_DRIVEABLE(tempTruck)
		tempPed = GET_PED_IN_VEHICLE_SEAT(tempTruck)
	ENDIF
	
	IF NOT IS_PED_INJURED(tempPed)
		IF IS_PED_A_PLAYER(tempPed)
			tempPlayer = NETWORK_GET_PLAYER_INDEX_FROM_PED(tempPed)
			IF IS_NET_PLAYER_OK(tempPlayer)
				IF NETWORK_IS_PLAYER_A_PARTICIPANT(tempPlayer)
					tempPart = NETWORK_GET_PARTICIPANT_INDEX(tempPlayer)
					IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
						iPart = NATIVE_TO_INT(tempPart)
						iVehTeam = MC_playerBD[iPart].iTeam
						IF iVehTeam >= 0

							IF SHOULD_VEHICLE_COUNT_AS_HELD_FOR_TEAM(iVeh,iVehTeam)							
								MC_serverBD.iDriverPart[iVeh] = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(tempPlayer))
								PRINTLN("[Vehicles][Vehicle ", sVehState.iIndex, "] PROCESS_VEH_BRAIN - Driver Part: ", MC_serverBD.iDriverPart[iVeh])
							ELSE
								MC_serverBD.iDriverPart[iVeh] = -1
							ENDIF
							
							IF IS_OBJECTIVE_BLOCKED_FOR_PARTICIPANT(NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(tempPlayer)))
								MC_STOP_NET_TIMER_WITH_TIMESTAMP(MC_serverBD_1.iControlVehTimer[iVeh])
								MC_serverBD.iDriverPart[iVeh] = -1
								EXIT
							ENDIF
							
							IF MC_serverBD_4.iVehRule[iVeh][iVehTeam] = FMMC_OBJECTIVE_LOGIC_CAPTURE //[FMMC_2020] - Needs cleaned up if used again
								IF MC_serverBD_4.iVehPriority[iVeh][iVehTeam] < FMMC_MAX_RULES
									IF NOT MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(MC_serverBD_1.iControlVehTimer[iVeh])
										MC_START_NET_TIMER_WITH_TIMESTAMP(MC_serverBD_1.iControlVehTimer[iVeh])
										MC_serverBD_1.iControlVehTimer[iVeh] += (-1*iStoredVehCaptureMS[iVehTeam][iVeh])
										PRINTLN("[Vehicles][Vehicle ", sVehState.iIndex, "] PROCESS_VEH_BRAIN - starting control timer")
									ELSE
										IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFour, ciFMMC_VEHICLE4_VEHICLE_RETAIN_CAPTURE_PROGRESS)
											IF iStoredVehCaptureMS[iVehTeam][iVeh] > MC_serverBD.iVehTakeoverTime[iVehTeam][MC_serverBD_4.iVehPriority[iVeh][iVehTeam]]
																							
												BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_TEAM_CON_VEH,0,iVehTeam,-1,tempplayer,ci_TARGET_VEHICLE,iVeh)
												
												PRINTLN("[Vehicles][Vehicle ", sVehState.iIndex, "] PROCESS_VEH_BRAIN - cleaning up vehicle for control objective")
												
												PRINTLN("[Vehicles][Vehicle ", sVehState.iIndex, "] PROCESS_VEH_BRAIN - Number of iRuleReCapture: ", MC_serverBD.iRuleReCapture[iVehTeam][MC_serverBD_4.iVehPriority[iVeh][iVehTeam]]) 
												PRINTLN("[Vehicles][Vehicle ", sVehState.iIndex, "] PROCESS_VEH_BRAIN - GET_VEHICLE_RESPAWNS: ", GET_VEHICLE_RESPAWNS(iVeh))
												
												IF MC_serverBD_4.iVehPriority[iVeh][iVehTeam] < FMMC_MAX_RULES
													REQUEST_INCREMENT_SERVER_REMOTE_PLAYER_SCORE(iPart, GET_FMMC_POINTS_FOR_TEAM(iVehTeam,MC_serverBD_4.iVehPriority[iVeh][iVehTeam]), iVehTeam, MC_serverBD_4.iVehPriority[iVeh][iVehTeam])
													
													IF MC_serverBD.iRuleReCapture[iVehTeam][MC_serverBD_4.iVehPriority[iVeh][iVehTeam]] <= 0
														IF SHOULD_VEH_CLEAN_UP_ON_DELIVERY(iVeh, iVehTeam)
															PRINTLN("[Vehicles][Vehicle ", sVehState.iIndex, "] PROCESS_VEH_BRAIN - cleaning up vehicle for control objective")
															CLEANUP_NET_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
															CLEANUP_VEHICLE_CARGO(iVeh)
														ENDIF
													ENDIF
													IF MC_serverBD.iRuleReCapture[iVehTeam][MC_serverBD_4.iVehPriority[iVeh][iVehTeam]] <=0	
														
														IF SHOULD_VEH_CLEAN_UP_ON_DELIVERY(iVeh, iVehTeam)
															FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams-1)
																iPriority[iTeam] =MC_serverBD_4.iVehPriority[iVeh][iTeam]
																IF iTeam = iVehTeam
																	IF IS_BIT_SET(MC_serverBD.iVehTeamFailBitset[iVeh],iTeam)
																		CLEAR_BIT(MC_serverBD.iVehTeamFailBitset[iVeh],iTeam)
																		CLEAR_BIT(MC_serverBD.iMissionCriticalVeh[iTeam], iVeh)
																		PRINTLN("[Vehicles][Vehicle ", sVehState.iIndex, "] PROCESS_VEH_BRAIN - OBJECTIVE NOT FAILED FOR TEAM: ", iVehTeam, " BECAUSE THEY CAPTURED VEH")
																		bWasObjective[iTeam] = TRUE
																	ENDIF
																ELSE
																	IF DOES_TEAM_LIKE_TEAM(iTeam,iVehTeam)
																		IF IS_BIT_SET(MC_serverBD.iVehTeamFailBitset[iVeh],iTeam)
																			CLEAR_BIT(MC_serverBD.iVehTeamFailBitset[iVeh],iTeam)
																			CLEAR_BIT(MC_serverBD.iMissionCriticalVeh[iTeam], iVeh)
																			PRINTLN("[Vehicles][Vehicle ", sVehState.iIndex, "] PROCESS_VEH_BRAIN - OBJECTIVE NOT FAILED FOR TEAM: ", iVehTeam, " BECAUSE FRIENDLY TEAM CAPTURED VEH")
																			bWasObjective[iTeam] = TRUE
																		ENDIF
																	ENDIF
																ENDIF
															ENDFOR
														ENDIF
														FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams-1)
															IF iTeam != iVehTeam
																IF SHOULD_VEH_CLEAN_UP_ON_DELIVERY(iVeh, iVehTeam)
																	IF IS_BIT_SET(MC_serverBD.iVehTeamFailBitset[iVeh],iTeam)
																		IF iPriority[iTeam] < FMMC_MAX_RULES
																		OR IS_BIT_SET(MC_serverBD.iMissionCriticalVeh[iTeam], iVeh)
																			IF SHOULD_VEHICLE_CAUSE_MISSION_FAILURE(iVeh, iTeam, iPriority[iTeam])
																				MC_serverBD.iEntityCausingFail[iTeam] = iVeh																				
																				REQUEST_SET_TEAM_FAILED(iTeam, mFail_VEH_CAPTURED)																				
																				PRINTLN("[Vehicles][Vehicle ", sVehState.iIndex, "] PROCESS_VEH_BRAIN - MISSION FAILED FOR TEAM: ", iTeam, "  BECAUSE VEH CAPTURED")
																			ENDIF
																		ENDIF
																		CLEAR_BIT(MC_serverBD.iVehTeamFailBitset[iVeh],iTeam)
																		bWasObjective[iTeam] = TRUE
																	ENDIF
																	IF DOES_TEAM_LIKE_TEAM(iTeam,iVehTeam)
																		SET_TEAM_OBJECTIVE_OUTCOME(iTeam,iPriority[iTeam],TRUE)
																		PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_VEHICLE,iVeh,iTeam,TRUE)
																	ELSE
																		SET_TEAM_OBJECTIVE_OUTCOME(iTeam,iPriority[iTeam],FALSE)
																		PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_VEHICLE,iVeh,iTeam,FALSE)
																	ENDIF
																	MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_VEH_CAPTURED
																ENDIF
															ELSE
																PRINTLN("[Vehicles][Vehicle ", sVehState.iIndex, "] PROCESS_VEH_BRAIN - SET_TEAM_OBJECTIVE_OUTCOME")
																SET_TEAM_OBJECTIVE_OUTCOME(iTeam,iPriority[iTeam],TRUE)
																PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_VEHICLE,iVeh,iTeam,TRUE)
																MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_VEH_CAPTURED
															ENDIF

														ENDFOR
													ENDIF
												ENDIF
												IF MC_serverBD_4.iVehPriority[iVeh][iVehTeam] < FMMC_MAX_RULES
													IF MC_serverBD.iRuleReCapture[iVehTeam][MC_serverBD_4.iVehPriority[iVeh][iVehTeam]] != UNLIMITED_CAPTURES_KILLS
														MC_serverBD.iRuleReCapture[iVehTeam][MC_serverBD_4.iVehPriority[iVeh][iVehTeam]]--
													ENDIF
												ENDIF
												FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams-1)
													IF bWasObjective[iTeam]
														IF IS_THIS_OBJECTIVE_OVER(ci_TARGET_VEHICLE,iTeam,iPriority[iTeam])
															SET_LOCAL_OBJECTIVE_END_MESSAGE(iTeam,iPriority[iTeam],MC_serverBD.iReasonForObjEnd[iTeam])
															BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[iTeam],iTeam,iPriority[iTeam],DID_TEAM_PASS_OBJECTIVE(iTeam,iPriority[iTeam]))
														ENDIF
													ENDIF
												ENDFOR

												PRINTLN("[Vehicles][Vehicle ", sVehState.iIndex, "] PROCESS_VEH_BRAIN - Control Complete")
												MC_STOP_NET_TIMER_WITH_TIMESTAMP(MC_serverBD_1.iControlVehTimer[iVeh])
												iStoredVehCaptureMS[iVehTeam][iVeh] = 0
											ENDIF
										ELSE
											IF MC_GET_NET_TIMER_DIFFERENCE_WITH_TIME_STAMP(MC_serverBD_1.iControlVehTimer[iVeh]) > MC_serverBD.iVehTakeoverTime[iVehTeam][MC_serverBD_4.iVehPriority[iVeh][iVehTeam]]   
											
												BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_TEAM_CON_VEH,0,iVehTeam,-1,tempplayer,ci_TARGET_VEHICLE,iVeh)
												
												IF MC_serverBD_4.iVehPriority[iVeh][iVehTeam] < FMMC_MAX_RULES
													REQUEST_INCREMENT_SERVER_REMOTE_PLAYER_SCORE(iPart, GET_FMMC_POINTS_FOR_TEAM(iVehTeam,MC_serverBD_4.iVehPriority[iVeh][iVehTeam]), iVehTeam, MC_serverBD_4.iVehPriority[iVeh][iVehTeam])
																										
													IF MC_serverBD.iRuleReCapture[iVehTeam][MC_serverBD_4.iVehPriority[iVeh][iVehTeam]] <= 0
														IF SHOULD_VEH_CLEAN_UP_ON_DELIVERY(iVeh, iVehTeam)
															PRINTLN("[Vehicles][Vehicle ", sVehState.iIndex, "] PROCESS_VEH_BRAIN - Cleaning up vehicle for control objective")
															CLEANUP_NET_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
															CLEANUP_VEHICLE_CARGO(iVeh)
														ENDIF
													ENDIF
													IF GET_VEHICLE_RESPAWNS(iVeh) <= 0 	
													AND MC_serverBD.iRuleReCapture[iVehTeam][MC_serverBD_4.iVehPriority[iVeh][iVehTeam]] <=0
														
														IF SHOULD_VEH_CLEAN_UP_ON_DELIVERY(iVeh, iVehTeam)
															FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams-1)
																iPriority[iTeam] =MC_serverBD_4.iVehPriority[iVeh][iTeam]
																IF iTeam = iVehTeam
																	IF IS_BIT_SET(MC_serverBD.iVehTeamFailBitset[iVeh],iTeam)
																		CLEAR_BIT(MC_serverBD.iVehTeamFailBitset[iVeh],iTeam)
																		CLEAR_BIT(MC_serverBD.iMissionCriticalVeh[iTeam], iVeh)
																		PRINTLN("[Vehicles][Vehicle ", sVehState.iIndex, "] PROCESS_VEH_BRAIN - OBJECTIVE NOT FAILED FOR TEAM: ", iVehTeam, " BECAUSE THEY CAPTURED VEH")
																		bWasObjective[iTeam] = TRUE
																	ENDIF
																ELSE
																	IF DOES_TEAM_LIKE_TEAM(iTeam,iVehTeam)
																		IF IS_BIT_SET(MC_serverBD.iVehTeamFailBitset[iVeh],iTeam)
																			CLEAR_BIT(MC_serverBD.iVehTeamFailBitset[iVeh],iTeam)
																			CLEAR_BIT(MC_serverBD.iMissionCriticalVeh[iTeam], iVeh)
																			PRINTLN("[Vehicles][Vehicle ", sVehState.iIndex, "] PROCESS_VEH_BRAIN - OBJECTIVE NOT FAILED FOR TEAM: ", iVehTeam, " BECAUSE FRIENDLY TEAM CAPTURED VEH")
																			bWasObjective[iTeam] = TRUE
																		ENDIF
																	ENDIF
																ENDIF
															ENDFOR
														ENDIF
														FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams-1)
															IF iTeam!=iVehTeam
																IF SHOULD_VEH_CLEAN_UP_ON_DELIVERY(iVeh, iVehTeam)
																	IF IS_BIT_SET(MC_serverBD.iVehTeamFailBitset[iVeh],iTeam)
																		IF iPriority[iTeam] < FMMC_MAX_RULES
																		OR IS_BIT_SET(MC_serverBD.iMissionCriticalVeh[iTeam], iVeh)
																			IF SHOULD_VEHICLE_CAUSE_MISSION_FAILURE(iVeh, iTeam, iPriority[iTeam])
																				MC_serverBD.iEntityCausingFail[iTeam] = iVeh
																				REQUEST_SET_TEAM_FAILED(iTeam, mFail_VEH_CAPTURED)
																				PRINTLN("[Vehicles][Vehicle ", sVehState.iIndex, "] PROCESS_VEH_BRAIN - MISSION FAILED FOR TEAM: ", iTeam, "  BECAUSE VEH CAPTURED")
																			ENDIF
																		ENDIF
																		CLEAR_BIT(MC_serverBD.iVehTeamFailBitset[iVeh],iTeam)
																		bWasObjective[iTeam] = TRUE
																	ENDIF
																	IF DOES_TEAM_LIKE_TEAM(iTeam,iVehTeam)
																		SET_TEAM_OBJECTIVE_OUTCOME(iTeam,iPriority[iTeam],TRUE)
																		PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_VEHICLE,iVeh,iTeam,TRUE)
																	ELSE
																		SET_TEAM_OBJECTIVE_OUTCOME(iTeam,iPriority[iTeam],FALSE)
																		PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_VEHICLE,iVeh,iTeam,FALSE)
																	ENDIF
																	MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_VEH_CAPTURED
																ENDIF
															ELSE
																SET_TEAM_OBJECTIVE_OUTCOME(iTeam,iPriority[iTeam],TRUE)
																PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_VEHICLE,iVeh,iTeam,TRUE)
																MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_VEH_CAPTURED
															ENDIF

														ENDFOR
													ENDIF
												ENDIF
												IF MC_serverBD_4.iVehPriority[iVeh][iVehTeam] < FMMC_MAX_RULES
													IF MC_serverBD.iRuleReCapture[iVehTeam][MC_serverBD_4.iVehPriority[iVeh][iVehTeam]] !=UNLIMITED_CAPTURES_KILLS
														MC_serverBD.iRuleReCapture[iVehTeam][MC_serverBD_4.iVehPriority[iVeh][iVehTeam]]--
													ENDIF
												ENDIF
												FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams-1)
													IF bWasObjective[iTeam]
														IF IS_THIS_OBJECTIVE_OVER(ci_TARGET_VEHICLE,iTeam,iPriority[iTeam])
															SET_LOCAL_OBJECTIVE_END_MESSAGE(iTeam,iPriority[iTeam],MC_serverBD.iReasonForObjEnd[iTeam])
															BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[iTeam],iTeam,iPriority[iTeam],DID_TEAM_PASS_OBJECTIVE(iTeam,iPriority[iTeam]))
														ENDIF
													ENDIF
												ENDFOR

												PRINTLN("[Vehicles][Vehicle ", sVehState.iIndex, "] PROCESS_VEH_BRAIN - Control complete for veh")
												MC_STOP_NET_TIMER_WITH_TIMESTAMP(MC_serverBD_1.iControlVehTimer[iVeh])
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ELSE
								MC_STOP_NET_TIMER_WITH_TIMESTAMP(MC_serverBD_1.iControlVehTimer[iVeh])
							ENDIF
						ELSE
							MC_serverBD.iDriverPart[iVeh] = -1
							MC_STOP_NET_TIMER_WITH_TIMESTAMP(MC_serverBD_1.iControlVehTimer[iVeh])
						ENDIF
					ELSE //from here on add setting checks and cache capture prog
						MC_serverBD.iDriverPart[iVeh] = -1
						MC_STOP_NET_TIMER_WITH_TIMESTAMP(MC_serverBD_1.iControlVehTimer[iVeh])
					ENDIF
				ELSE
					MC_serverBD.iDriverPart[iVeh] = -1
					MC_STOP_NET_TIMER_WITH_TIMESTAMP(MC_serverBD_1.iControlVehTimer[iVeh])
				ENDIF
			ELSE
				MC_serverBD.iDriverPart[iVeh] = -1
				MC_STOP_NET_TIMER_WITH_TIMESTAMP(MC_serverBD_1.iControlVehTimer[iVeh])
			ENDIF
		ELSE
			MC_serverBD.iDriverPart[iVeh] = -1
			MC_STOP_NET_TIMER_WITH_TIMESTAMP(MC_serverBD_1.iControlVehTimer[iVeh])
		ENDIF
	ELSE
		MC_serverBD.iDriverPart[iVeh] = -1
		MC_STOP_NET_TIMER_WITH_TIMESTAMP(MC_serverBD_1.iControlVehTimer[iVeh])
	ENDIF
	
ENDPROC

FUNC INT GET_VEH_KILLERS_TEAM(PED_INDEX killerPed,INT ivehID)

	PLAYER_INDEX tempPlayer
	PARTICIPANT_INDEX tempPart
	INT ipart

	IF DOES_ENTITY_EXIST(killerPed)
		tempPlayer = NETWORK_GET_PLAYER_INDEX_FROM_PED(killerPed)
		IF NETWORK_IS_PLAYER_A_PARTICIPANT(tempPlayer)
			tempPart = NETWORK_GET_PARTICIPANT_INDEX(tempPlayer)
			ipart = NATIVE_TO_INT(tempPart)
			IF MC_serverBD_4.iVehPriority[ivehID][MC_playerBD[ipart].iteam] = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[ipart].iteam]
				IF MC_serverBD_4.iVehPriority[ivehID][MC_playerBD[ipart].iteam] < FMMC_MAX_RULES
					IF (MC_serverBD_4.ivehRule[ivehID][MC_playerBD[ipart].iteam] = FMMC_OBJECTIVE_LOGIC_KILL OR MC_serverBD_4.ivehRule[ivehID][MC_playerBD[ipart].iteam] = FMMC_OBJECTIVE_LOGIC_DAMAGE)
						RETURN MC_playerBD[ipart].iteam
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN -1

ENDFUNC

PROC GIVE_TEAM_POINTS_FOR_VEH_DAMAGE(INT& iVehID, VEHICLE_INDEX Victimveh,PED_INDEX killerPed = NULL)

	INT iteam
	INT iKillerTeam = -1

	IF bIsLocalPlayerHost
		IF iVehID = -2
			PRINTLN("[RCC MISSION]  - GIVE_TEAM_POINTS_FOR_VEH_KILL - iVehID = -2")
			ivehid = IS_VEH_A_MISSION_CREATOR_VEH(Victimveh)
		ENDIF
		IF ivehid >= 0
			IF killerPed != NULL
				iKillerTeam = GET_VEH_KILLERS_TEAM(killerPed,ivehid)
			ENDIF
			IF NOT IS_BIT_SET(MC_serverBD_2.iVehIsDestroyed, iVehID)
				FOR iteam = 0 TO (MC_serverBD.iNumActiveTeams-1)
					IF iKillerTeam != iteam
						IF MC_serverBD_4.iVehPriority[ivehid][iteam] < FMMC_MAX_RULES
							IF MC_serverBD_4.iVehPriority[ivehid][iteam] <= MC_serverBD_4.iCurrentHighestPriority[iteam]
								IF (MC_serverBD_4.ivehRule[ivehid][iteam] = FMMC_OBJECTIVE_LOGIC_KILL OR MC_serverBD_4.ivehRule[ivehid][iteam] = FMMC_OBJECTIVE_LOGIC_DAMAGE)
									INCREMENT_SERVER_TEAM_SCORE(iteam,MC_serverBD_4.iVehPriority[ivehid][iteam],GET_FMMC_POINTS_FOR_TEAM(iteam,MC_serverBD_4.iVehPriority[ivehid][iteam]))
									PRINTLN("ADDING TO TEAM SCORE for team: ", iteam," VEH was killed : ",ivehid)
									BROADCAST_OBJECTIVE_MID_POINT(MC_serverBD_4.iVehPriority[ivehid][iteam],iteam)								
								ELSE
									PRINTLN("rule not kill veh for team: ", iteam," veh was killed : ",ivehid)
								ENDIF
							ELSE
								PRINTLN("priority not current veh for team: ", iteam," veh was killed : ",ivehid)
								PRINTLN("priority: ", MC_serverBD_4.iCurrentHighestPriority[iteam]," veh priority : ",MC_serverBD_4.iVehPriority[ivehid][iteam])
								
							ENDIF
						ELSE
							PRINTLN("priority ignore veh for team: ", iteam," veh was killed : ",ivehid)
						ENDIF
					ELSE
						PRINTLN("iKillerTeam = iteam: ", iteam," veh was killed : ",ivehid)
					ENDIF
				ENDFOR
			ENDIF
		ELSE
			PRINTLN("Invalid vehicle id - ivehid =-1 for veh for team: ", iteam," veh was killed : ",ivehid)
		ENDIF
	ENDIF


ENDPROC

PROC GIVE_TEAM_POINTS_FOR_VEH_KILL(INT& iVehID, VEHICLE_INDEX Victimveh,PED_INDEX killerPed = NULL)

	INT iteam
	INT iKillerTeam = -1

	IF bIsLocalPlayerHost
		IF iVehID = -2
			PRINTLN("[RCC MISSION]  - GIVE_TEAM_POINTS_FOR_VEH_KILL - iVehID = -2")
			ivehid = IS_VEH_A_MISSION_CREATOR_VEH(Victimveh)
		ENDIF
		IF ivehid >= 0
			IF killerPed != NULL
				iKillerTeam = GET_VEH_KILLERS_TEAM(killerPed,ivehid)
			ENDIF
			IF NOT IS_BIT_SET(MC_serverBD_2.iVehIsDestroyed, iVehID)
				FOR iteam = 0 TO (MC_serverBD.iNumActiveTeams-1)
					IF iKillerTeam != iteam
						IF MC_serverBD_4.iVehPriority[ivehid][iteam] < FMMC_MAX_RULES
							IF MC_serverBD_4.iVehPriority[ivehid][iteam] <= MC_serverBD_4.iCurrentHighestPriority[iteam]
								IF MC_serverBD_4.ivehRule[ivehid][iteam] = FMMC_OBJECTIVE_LOGIC_KILL
									INCREMENT_SERVER_TEAM_SCORE(iteam,MC_serverBD_4.iVehPriority[ivehid][iteam],GET_FMMC_POINTS_FOR_TEAM(iteam,MC_serverBD_4.iVehPriority[ivehid][iteam]))
									PRINTLN("ADDING TO TEAM SCORE for team: ", iteam," VEH was killed : ",ivehid)
									BROADCAST_OBJECTIVE_MID_POINT(MC_serverBD_4.iVehPriority[ivehid][iteam],iteam)
									IF NOT IS_BIT_SET(MC_serverBD_2.iVehIsDestroyed, iVehID)
										SET_BIT(MC_serverBD_2.iVehIsDestroyed, iVehID)
									ENDIF
								ELSE
									PRINTLN("rule not kill veh for team: ", iteam," veh was killed : ",ivehid)
								ENDIF
							ELSE
								PRINTLN("priority not current veh for team: ", iteam," veh was killed : ",ivehid)
								PRINTLN("priority: ", MC_serverBD_4.iCurrentHighestPriority[iteam]," veh priority : ",MC_serverBD_4.iVehPriority[ivehid][iteam])
								
							ENDIF
						ELSE
							PRINTLN("priority ignore veh for team: ", iteam," veh was killed : ",ivehid)
						ENDIF
						
						//Custom points
						INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
						INT iCustomPoints = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVehID].iVehPointsToGive
						INT iRuleStart = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVehID].iVehGivePointsOnRuleStart
						INt iRuleEnd = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVehID].iVehGivePointsOnRuleEnd
						
						IF iCustomPoints > 0
							PRINTLN("GIVE_TEAM_POINTS_FOR_VEH_KILL - iVeh: ", iVehID, " iTeam: ", iTeam, " iRule: ", iRule, " iCustomPoints: ", iCustomPoints, " iRuleStart: ", iRuleStart, " iRuleEnd: ", iRuleEnd)
							
							IF iRule >= iRuleStart
							AND (iRule < iRuleEnd OR iRuleEnd = -1)
								INCREMENT_SERVER_TEAM_SCORE(iTeam, iRule, iCustomPoints)
								PRINTLN("ADDING TO TEAM SCORE for team: ", iTeam," veh was killed : ", iVehID)
							ENDIF
						ENDIF
						//////
					ELSE
						PRINTLN("iKillerTeam = iteam: ", iteam," veh was killed : ",ivehid)
					ENDIF
				ENDFOR
			ENDIF
		ELSE
			PRINTLN("Invalid vehicle id - ivehid =-1 for veh for team: ", iteam," veh was killed : ",ivehid)
		ENDIF
	ENDIF


ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Staggered Objective Processing ----------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Functions that relate to vehicle objectives and are called from the staggered loop.  -----------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL SHOULD_DELIVER_VEH_BE_PROGRESSED_BY_LIKED_TEAM(INT iveh, INT iTeam, INT ipriority, INT iTeam2, INT ipriority2)

	PRINTLN("[RCC MISSION][MJM] - SHOULD_DELIVER_VEH_BE_PROGRESSED_BY_LIKED_TEAM - ipriority = ",ipriority, " ipriority2 = ",ipriority2)
	
	IF ipriority < FMMC_MAX_RULES
	AND ipriority2 < FMMC_MAX_RULES
		PRINTLN("[RCC MISSION][MJM] - SHOULD_DELIVER_VEH_BE_PROGRESSED_BY_LIKED_TEAM - Priority ok ")
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iteam2].iRuleBitsetThree[ipriority2], ciBS_RULE3_PROGRESS_IN_SAME_PLACE)
			
			IF g_FMMC_STRUCT.sFMMCEndConditions[iteam].iDropOffType[ipriority] = g_FMMC_STRUCT.sFMMCEndConditions[iteam2].iDropOffType[ipriority2]
				
				SWITCH g_FMMC_STRUCT.sFMMCEndConditions[iteam].iDropOffType[ipriority]
					
					CASE ciFMMC_DROP_OFF_TYPE_CYLINDER
					CASE ciFMMC_DROP_OFF_TYPE_SPHERE
						
						VECTOR v1
						v1 = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vDropOff[ipriority]
						VECTOR v2
						v2 = g_FMMC_STRUCT.sFMMCEndConditions[iTeam2].vDropOff[ipriority2]
						FLOAT r1
						r1 = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].fDropOffRadius[ipriority]
						FLOAT r2
						r2 = g_FMMC_STRUCT.sFMMCEndConditions[iTeam2].fDropOffRadius[ipriority2]
						
						IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].vDropOffOverride)
							v1 = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].vDropOffOverride
							v2 = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].vDropOffOverride
							IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].fVehDropOffRadius > 0
								r1 = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].fVehDropOffRadius
								r2 = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].fVehDropOffRadius
							ENDIF
						ELSE
							IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sFMMCEndConditions[iteam].vDropOff2[ipriority])
								IF iveh != iFirstHighPriorityVehicleThisRule[iteam]
									v1 = g_FMMC_STRUCT.sFMMCEndConditions[iteam].vDropOff2[ipriority]
								ENDIF
							ENDIF
							IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sFMMCEndConditions[iteam2].vDropOff2[ipriority2])
								IF iveh != iFirstHighPriorityVehicleThisRule[iteam2]
									v2 = g_FMMC_STRUCT.sFMMCEndConditions[iteam2].vDropOff2[ipriority2]
								ENDIF
							ENDIF
						ENDIF
						
						PRINTLN("[RCC MISSION][MJM] - SHOULD_DELIVER_VEH_BE_PROGRESSED_BY_LIKED_TEAM (1) - iTeam: ",iTeam, " ipriority: ",ipriority, " v1: ",v1," r1: ",r1)
						PRINTLN("[RCC MISSION][MJM] - SHOULD_DELIVER_VEH_BE_PROGRESSED_BY_LIKED_TEAM (2) - iTeam2: ",iTeam2, " ipriority2: ",ipriority2, " v2: ",v2," r2: ",r2)
						
						IF NOT ARE_VECTORS_EQUAL(v1, v2)
						OR (r1 != r2)
							RETURN FALSE
						ENDIF
						
					BREAK
					
					CASE ciFMMC_DROP_OFF_TYPE_AREA
						VECTOR v1_1
						v1_1 = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vDropOff[ipriority]
						VECTOR v1_2
						v1_2 = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vDropOff2[ipriority]
						FLOAT fWidth1
						fWidth1 = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].fDropOffRadius[ipriority]
						
						VECTOR v2_1
						v2_1 = g_FMMC_STRUCT.sFMMCEndConditions[iTeam2].vDropOff[ipriority2]
						VECTOR v2_2
						v2_2 = g_FMMC_STRUCT.sFMMCEndConditions[iTeam2].vDropOff2[ipriority2]
						FLOAT fWidth2
						fWidth2 = g_FMMC_STRUCT.sFMMCEndConditions[iTeam2].fDropOffRadius[ipriority2]
						
						IF (fWidth1 = fWidth2)
						AND ((ARE_VECTORS_EQUAL(v1_1, v2_1) AND ARE_VECTORS_EQUAL(v1_2, v2_2)) OR (ARE_VECTORS_EQUAL(v1_1, v2_2) AND ARE_VECTORS_EQUAL(v1_2, v2_1)))
							RETURN TRUE
						ELSE
							RETURN FALSE
						ENDIF
					BREAK
					
					CASE ciFMMC_DROP_OFF_TYPE_VEHICLE
						IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDropOff_Vehicle[ipriority] != g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDropOff_Vehicle[ipriority2]
							RETURN FALSE
						ELSE
							
							BOOL bGetIn1
							bGetIn1 = IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iVehDropOff_GetInBS, ipriority)
							BOOL bHookUp1
							bHookUp1 = IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iVehDropOff_HookUpBS, ipriority)
							FLOAT fRadius1
							fRadius1 = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].fDropOffRadius[ipriority]
							
							BOOL bGetIn2
							bGetIn2 = IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam2].iVehDropOff_GetInBS, ipriority2)
							BOOL bHookUp2
							bHookUp2 = IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam2].iVehDropOff_HookUpBS, ipriority2)
							FLOAT fRadius2
							fRadius2 = g_FMMC_STRUCT.sFMMCEndConditions[iTeam2].fDropOffRadius[ipriority2]
							
							IF (bGetIn1 = bGetIn2)
							AND (bGetIn1 OR (bHookUp1 = bHookUp2))
							AND (bGetIn1 OR bHookUp1 OR (fRadius1 = fRadius2))
								RETURN TRUE
							ELSE
								RETURN FALSE
							ENDIF
							
						ENDIF
					BREAK
					
					CASE ciFMMC_DROP_OFF_TYPE_LEADER_GARAGE
					CASE ciFMMC_DROP_OFF_TYPE_PROPERTY
					CASE ciFMMC_DROP_OFF_TYPE_PLACED_ZONE
						RETURN TRUE
					BREAK
					
				ENDSWITCH
			ELSE
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

//FMMC2020 - Calculate here and set data all other headers can read
FUNC INT GET_VEHICLE_BEING_DROPPED_OFF(INT iTeam, INT iPartDroppingOff = -1)
	
	INT iveh = -1
	
	IF iPartDroppingOff != -1
		IF MC_playerBD[iPartDroppingOff].iteam = iteam
		AND MC_playerBD[iPartDroppingOff].iVehCarryCount > 0
		AND MC_playerBD[iPartDroppingOff].iVehNear != -1
			iveh = MC_playerBD[iPartDroppingOff].iVehNear
		ENDIF
	ENDIF
	
	IF iveh = -1
		IF MC_serverBD.iNumVehHighestPriority[iteam] = 1
			IF iAHighPriorityVehicle != -1
				iveh = iAHighPriorityVehicle
			ENDIF
		ELIF MC_serverBD.iNumVehHighestPriorityHeld[iteam] = 1
			INT iPart, iPlayersChecked
			INT iPlayersToCheck = MC_serverBD.iNumberOfPlayingPlayers[iteam]
			PARTICIPANT_INDEX tempPart
			
			PRINTLN("[PLAYER_LOOP] - GET_VEHICLE_BEING_DROPPED_OFF")
			FOR iPart = 0 TO (GET_MAXIMUM_PLAYERS_ON_MISSION() - 1)
				IF MC_playerBD[iPart].iteam = iteam
					tempPart = INT_TO_PARTICIPANTINDEX(iPart)
					
					IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
						iPlayersChecked++
						
						IF MC_playerBD[iPart].iVehCarryCount > 0
						AND MC_playerBD[iPart].iVehNear != -1
							iveh = MC_playerBD[iPart].iVehNear
							BREAKLOOP
						ENDIF
					ENDIF
				ENDIF
				
				IF iPlayersChecked >= iPlayersToCheck
					BREAKLOOP
				ENDIF
			ENDFOR
		ELSE
			//Just try the first one we can find:
			iveh = iAHighPriorityVehicle
		ENDIF
	ENDIF
	
	RETURN iveh
	
ENDFUNC

PROC PROCESS_UNLOCK_VEHICLE_ON_RULE(FMMC_VEHICLE_STATE &sVehState)
	
	INT iUnstuckRule = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBecomesUnstuckOnRule
	
	IF iUnstuckRule = -1
	OR IS_BIT_SET(iVehicleHasBeenUnlockedBS, sVehState.iIndex)
		EXIT
	ENDIF
	
	INT iTeam = MC_playerBD[iPartToUse].iTeam
	INT iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]
	
	IF iRule < FMMC_MAX_RULES
 		IF iRule = iUnstuckRule
			PRINTLN("[PROCESS_UNLOCK_VEHICLE_ON_RULE] sVehState.iIndex: ", sVehState.iIndex, " unlocking as we are now on rule: ", iRule, " Vehicle should now also be considered by local player - iUnstuckRule: ", iUnstuckRule)
			BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedContentEventType_AllPlayersToggleConsiderVehicle, sVehState.iIndex, DEFAULT, DEFAULT, DEFAULT, DEFAULt, DEFAULT, TRUE)
			IF IS_ENTITY_ALIVE(sVehState.vehIndex)
				SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(sVehState.vehIndex, FALSE)
				SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(sVehState.vehIndex, TRUE)
			ENDIF
			SET_BIT(iVehicleHasBeenUnlockedBS, sVehState.iIndex)
		ENDIF
	ENDIF

ENDPROC

FUNC INT GET_NUMBER_OF_FREE_SEATS_IN_HIGH_PRIORITY_VEHICLES_FOR_TEAM(INT iTeam)
	INT iFreeSeats = 0
	
	IF MC_serverBD.iNumVehHighestPriority[iTeam] > 0
	AND ((MC_serverBD_4.iVehMissionLogic[iTeam] = FMMC_OBJECTIVE_LOGIC_GO_TO)
		 OR (MC_serverBD_4.iVehMissionLogic[iTeam] = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER))
		
		INT iVeh = -1
		
		IF MC_playerBD[iPartToUse].iVehNear != -1
			iVeh = MC_playerBD[iPartToUse].iVehNear
		ELIF iAHighPriorityVehicle != -1
			iVeh = iAHighPriorityVehicle //First vehicle to be checked in DISPLAY_VEHICLE_HUD which is a priority for my team
		ENDIF
		
		IF iVeh != -1
			BOOL bCheckOnRule = TRUE
			INT iTeamsToCheck = GET_VEHICLE_GOTO_TEAMS(iTeam, iVeh, bCheckOnRule)
			
			IF iTeamsToCheck = 0
				iFreeSeats = MC_serverBD.iNumVehSeatsHighestPriority[iTeam] - MC_serverBD.iTeamVehNumPlayers[iTeam]
			ELSE
				
				INT iTeamLoop
				
				FOR iTeamLoop = 0 TO (FMMC_MAX_TEAMS - 1)
					IF IS_BIT_SET(iTeamsToCheck,iTeamLoop)
						IF (NOT bCheckOnRule)
						OR (MC_serverBD.iNumVehHighestPriority[iTeamLoop] > 0
							AND ((MC_serverBD_4.iVehMissionLogic[iTeamLoop] = FMMC_OBJECTIVE_LOGIC_GO_TO)
								 OR (MC_serverBD_4.iVehMissionLogic[iTeamLoop] = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER)))
							iFreeSeats += MC_serverBD.iNumVehSeatsHighestPriority[iTeamLoop] - MC_serverBD.iTeamVehNumPlayers[iTeamLoop]
						ENDIF
					ENDIF
				ENDFOR
				
			ENDIF
			
		ELSE
			//Backup:
			iFreeSeats = MC_serverBD.iNumVehSeatsHighestPriority[iTeam] - MC_serverBD.iTeamVehNumPlayers[iTeam]
		ENDIF
		
	ENDIF
	
	RETURN iFreeSeats
	
ENDFUNC

FUNC BOOL IS_OBJECTIVE_VEHICLE_ON_PLAYERS_CARGOBOB(FMMC_VEHICLE_STATE &sVehState)
	
	BOOL bPrintDebug = FALSE
	
	#IF IS_DEBUG_BUILD
	bPrintDebug = bobjectiveVehicleOnCargobobDebug
	#ENDIF
	
	IF NOT sVehState.bExists
		IF bPrintDebug
			PRINTLN("IS_OBJECTIVE_VEHICLE_ON_PLAYERS_CARGOBOB - Veh ", sVehState.iIndex, " || tempVeh doesn't exist")
		ENDIF
		RETURN FALSE
	ENDIF
		
	VEHICLE_INDEX viPlayerVeh
	
	IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
				
		viPlayerVeh = GET_VEHICLE_PED_IS_USING(LocalPlayerPed)
		MODEL_NAMES mnplayerVeh = GET_ENTITY_MODEL(viPlayerVeh)
		
		IF !IS_VEHICLE_MODEL_A_CARGOBOB(mnplayerVeh)
			IF bPrintDebug
				PRINTLN("IS_OBJECTIVE_VEHICLE_ON_PLAYERS_CARGOBOB - Veh ", sVehState.iIndex, " || Player isn't in cargobob")
			ENDIF
			RETURN FALSE
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(viPlayerVeh)
			IF bPrintDebug
				PRINTLN("IS_OBJECTIVE_VEHICLE_ON_PLAYERS_CARGOBOB - Veh ", sVehState.iIndex, " || viPlayerVeh doesn't exist")
			ENDIF
			RETURN FALSE
		ENDIF
	ELSE
		IF bPrintDebug
		PRINTLN("IS_OBJECTIVE_VEHICLE_ON_PLAYERS_CARGOBOB - Veh ", sVehState.iIndex, " || Player isn't in any vehicle")
		ENDIF
		RETURN FALSE
	ENDIF
	
	IF MC_serverBD_4.iVehPriority[sVehState.iIndex][MC_playerBD[iPartToUse].iteam] > MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam]
	OR MC_serverBD_4.iVehPriority[sVehState.iIndex][MC_playerBD[iPartToUse].iteam] = -1
		IF bPrintDebug
			PRINTLN("IS_OBJECTIVE_VEHICLE_ON_PLAYERS_CARGOBOB - Veh ", sVehState.iIndex, " || Not an objective veh")
		ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_VEHICLE_ATTACHED_TO_CARGOBOB(viPlayerVeh, sVehState.vehIndex)
		IF bPrintDebug
			PRINTLN("IS_OBJECTIVE_VEHICLE_ON_PLAYERS_CARGOBOB - Veh ", sVehState.iIndex, " || Is attached to viPlayerVeh!!!")
		ENDIF
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Staggered Vehicle Processing ------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Vehicle processing called in the staggered loop.  ----------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL DOES_VEHICLE_HAVE_DELIVERED_DECOR_FOR_TEAM(INT iTeam, INT iVeh, INT iRule)
	IF iTeam = -1
	OR iTeam >= FMMC_MAX_TEAMS
		RETURN FALSE
	ENDIF
	STRING strDecorName
	SWITCH iTeam
		CASE 0	strDecorName = "MC_Team0_VehDeliveredRules"		BREAK
		CASE 1	strDecorName = "MC_Team1_VehDeliveredRules"		BREAK
		CASE 2	strDecorName = "MC_Team2_VehDeliveredRules"		BREAK
		CASE 3	strDecorName = "MC_Team3_VehDeliveredRules"		BREAK
	ENDSWITCH
	IF DECOR_IS_REGISTERED_AS_TYPE(strDecorName, DECOR_TYPE_INT)
		VEHICLE_INDEX vehID = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
		IF DECOR_EXIST_ON(vehID, strDecorName)
			IF IS_BIT_SET(DECOR_GET_INT(vehID, strDecorName), iRule)
				PRINTLN("[RCC MISSION]DOES_VEHICLE_HAVE_DELIVERED_DECOR_FOR_TEAM Veh ", iVeh, " has been delivered.")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC EXT2_CREATE_CLOUD_PTFX(FMMC_VEHICLE_STATE &sVehState)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetSeven, ciFMMC_VEHICLE7_ENGINE_KNOCKBACK)
		EXIT
	ENDIF
	
	IF sVehState.mn != CARGOPLANE
		EXIT
	ENDIF
	
	IF sVehState.bAlive
	
		REQUEST_NAMED_PTFX_ASSET("scr_rcextreme2")
		IF HAS_NAMED_PTFX_ASSET_LOADED("scr_rcextreme2")
			IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(ptfx_cargoplane_moving_clouds)
				FLOAT fCloudsCurrentAlpha = 0.4
				USE_PARTICLE_FX_ASSET("scr_rcextreme2")
				ptfx_cargoplane_moving_clouds = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_extrm2_moving_cloud", sVehState.vehIndex, <<0,-20,-13>>, <<0,0,0>>)
				SET_PARTICLE_FX_LOOPED_ALPHA(ptfx_cargoplane_moving_clouds, fCloudsCurrentAlpha)
				PRINTLN("EXT2_CREATE_CLOUD_PTFX make clouds ptfx")
			ENDIF
			IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(ptfx_cargoplane_wind_and_smoke)
				USE_PARTICLE_FX_ASSET("scr_rcextreme2")
				ptfx_cargoplane_wind_and_smoke = START_PARTICLE_FX_LOOPED_AT_COORD("scr_rcext2_cargo_smoke", <<417.0, 3920.0, 1449.0>>, <<0,0,0>>)
				PRINTLN("EXT2_CREATE_CLOUD_PTFX make winds and smoke")
			ENDIF
		ENDIF
	ELSE
		IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfx_cargoplane_moving_clouds)
		OR DOES_PARTICLE_FX_LOOPED_EXIST(ptfx_cargoplane_wind_and_smoke)
			REMOVE_PARTICLE_FX(ptfx_cargoplane_moving_clouds)
			REMOVE_PARTICLE_FX(ptfx_cargoplane_wind_and_smoke)
			REMOVE_NAMED_PTFX_ASSET("scr_rcextreme2")
		ENDIF
	ENDIF
ENDPROC

//[FMMC2020] This belongs in Players really
FUNC BOOL IS_ANY_PLAYER_TRYING_TO_OPEN_LOCKED_VEHICLE()
	INT i
	PRINTLN("[PLAYER_LOOP] - IS_ANY_PLAYER_TRYING_TO_OPEN_LOCKED_VEHICLE")
	FOR i = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION() - 1
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
			PLAYER_INDEX piPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
			
			PED_INDEX playerPed = GET_PLAYER_PED(piPlayer)
			
			IF DOES_ENTITY_EXIST(playerPed)
				IF IS_ENTITY_ALIVE(playerPed)
					IF IS_PED_TRYING_TO_ENTER_A_LOCKED_VEHICLE(playerPed)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	RETURN FALSE
ENDFUNC

//[FMMC2020] - Needs meerged into PROCESS_VEH_BODY when it is refactored
PROC PROCESS_CLIENT_VEHICLE_OBJECTIVES_OLD(INT iVeh)

	INT iMyTeam = MC_playerBD[iPartToUse].iteam
	
	IF MC_serverBD_4.iVehPriority[iveh][iMyTeam] <= MC_serverBD_4.iCurrentHighestPriority[iMyTeam]
	AND MC_serverBD_4.iVehPriority[iveh][iMyTeam] < FMMC_MAX_RULES
	AND MC_serverBD_4.ivehRule[iveh][iMyTeam] != FMMC_OBJECTIVE_LOGIC_NONE

		IF iAHighPriorityVehicle = -1
			iAHighPriorityVehicle = iveh
		ENDIF
		
		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh])
			EXIT
		ENDIF
		
		IF MC_serverBD_4.ivehRule[iveh][iMyTeam]= FMMC_OBJECTIVE_LOGIC_CAPTURE
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_SECUROSERV_HACKING)
				
				IF MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(MC_serverBD_1.iControlVehTimer[iVeh])
					
					INT iHackingPart = -1
					
					IF MC_playerBD[iPartToUse].iVehCapturing = iVeh
						//We are the hacker
						iHackingPart = iPartToUse
						
						IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciSECUROSERV_DONT_FORCE_ON_SCREEN)
							DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_PHONE)
							DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_CELLPHONE_DOWN)
							DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_CELLPHONE_CANCEL)
						ENDIF
					ELSE
						IF IS_BIT_SET(iHackingVehInRangeBitSet, iVeh)
							//In range of the veh
							INT iPartsChecked
							INT iPlayersToCheck = MC_serverBD.iNumberOfPlayingPlayers[0] + MC_serverBD.iNumberOfPlayingPlayers[1] + MC_serverBD.iNumberOfPlayingPlayers[2] + MC_serverBD.iNumberOfPlayingPlayers[3]
							INT i
							PRINTLN("[PLAYER_LOOP] - PROCESS_VEH_OBJECTIVES_CLIENT")
							FOR i = 0 TO (GET_MAXIMUM_PLAYERS_ON_MISSION() - 1)
								IF ((NOT IS_BIT_SET(MC_playerBD[i].iClientBitSet, PBBOOL_ANY_SPECTATOR)) OR IS_BIT_SET(MC_playerBD[i].iClientBitSet, PBBOOL_HEIST_SPECTATOR))
								AND NOT IS_BIT_SET(MC_playerBD[i].iClientBitSet,PBBOOL_PLAYER_FAIL)
								AND IS_BIT_SET(MC_playerBD[i].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
								AND NOT IS_BIT_SET(MC_playerBD[i].iClientBitSet, PBBOOL_FINISHED)
									IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
										iPartsChecked++
										IF i != iPartToUse
										AND DOES_TEAM_LIKE_TEAM(MC_playerBD[i].iTeam, MC_playerBD[iPartToUse].iTeam)
											IF MC_playerBD[i].iVehCapturing = iVeh
												iHackingPart = i
												PRINTLN("[SECUROHACK] PROCESS_VEH_OBJECTIVES_CLIENT Hacker is, ", iHackingPart)
												BREAKLOOP
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								IF iPartsChecked >= iPlayersToCheck
									BREAKLOOP
								ENDIF
							ENDFOR
						ENDIF
					ENDIF
					
					IF iHackingPart != -1
						IF iSpectatorTarget = -1
							IF NOT HAS_NET_TIMER_STARTED(tdTimeHackingTimer)
								PRINTLN("[JS][SECUROHACK] - PROCESS_VEH_OBJECTIVES_CLIENT - Starting hacking tracking timer")
								REINIT_NET_TIMER(tdTimeHackingTimer)
							ENDIF
						ENDIF
						
						SET_BIT(iLocalBoolCheck23, LBOOL23_SOMEONE_IS_HACKING_A_VEHICLE)
						
						IF MC_serverBD_4.iVehPriority[iVeh][MC_playerBD[iHackingPart].iTeam] > -1
						AND MC_serverBD_4.iVehPriority[iVeh][MC_playerBD[iHackingPart].iTeam] < FMMC_MAX_RULES
							IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("appSecuroHack")) > 0
							AND IS_PHONE_ONSCREEN(TRUE)
								IF NOT IS_BIT_SET(iLocalBoolCheck20, LBOOL20_HACKING_LOOP_STARTED)
									IF iHackingLoopSoundID = -1
										iHackingLoopSoundID = GET_SOUND_ID()
									ENDIF
									
									PLAY_SOUND_FRONTEND(iHackingLoopSoundID, "Hack_Loop", "DLC_IE_SVM_Voltic2_Hacking_Sounds")
									SET_BIT(iLocalBoolCheck20, LBOOL20_HACKING_LOOP_STARTED)
								ENDIF
								
								SET_VARIABLE_ON_SOUND(iHackingLoopSoundID, "percentageComplete", TO_FLOAT(iHackPercentage) / 100)
							ENDIF
							
							INT iTempHackPercentage = ROUND(TO_FLOAT(MC_GET_NET_TIMER_DIFFERENCE_WITH_TIME_STAMP(MC_serverBD_1.iControlVehTimer[iVeh])) / MC_serverBD.iVehTakeoverTime[MC_playerBD[iHackingPart].iTeam][MC_serverBD_4.iVehPriority[iVeh][MC_playerBD[iHackingPart].iTeam]] * 100)
							
							IF iTempHackPercentage >= iHackPercentage
							OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFive, ciFMMC_VEHICLE5_HACKING_RESET_CAPTURE_OUT_OF_RANGE)
							OR iPreviousVehHack != iVeh
								iHackPercentage = iTempHackPercentage
							ELSE
								PRINTLN("[SECUROHACK] PROCESS_VEH_OBJECTIVES_CLIENT iTempHackPercentage is less that we think it should be ", iTempHackPercentage, " vs ", iHackPercentage, " keeping higher value until we catch up")
							ENDIF
							
							IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciDISPLAY_HACK_PROGRESS_BAR)
								IF MC_playerBD[iLocalPart].iVehCapturing = iVeh
									DRAW_GENERIC_METER(MC_GET_NET_TIMER_DIFFERENCE_WITH_TIME_STAMP(MC_serverBD_1.iControlVehTimer[iVeh]), MC_serverBD.iVehTakeoverTime[MC_playerBD[iHackingPart].iTeam][MC_serverBD_4.iVehPriority[iVeh][MC_playerBD[iHackingPart].iTeam]], "HACK_PROG", DEFAULT, DEFAULT, HUDORDER_TOP, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT)
								ENDIF
							ENDIF
															
							IF iHackPercentage < 100
								IF iHackStage != ciSECUROSERV_HACK_STAGE_HACKING
								AND iHackStage != ciSECUROSERV_HACK_STAGE_LOSING_SIGNAL
									IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciDISPLAY_HACK_PROGRESS_BAR)
										SET_BIT(iHackingVehInRangeBitSet, iVeh)	//Fix for bug 3515537 (forcing the player to be in range because you can always see the progress bar)
									ENDIF
									
									IF NOT IS_HACK_START_SOUND_BLOCKED()
										PLAY_SOUND_FRONTEND(-1, "Hack_Start", "DLC_IE_SVM_Voltic2_Hacking_Sounds")
									ENDIF
									
									iHackStage = ciSECUROSERV_HACK_STAGE_HACKING
								ENDIF
								
								IF IS_BIT_SET(iLocalBoolCheck26, LBOOL26_SERVUROSERV_LOSING_SIGNAL)
									IF iHackStage = ciSECUROSERV_HACK_STAGE_HACKING
										iHackStage = ciSECUROSERV_HACK_STAGE_LOSING_SIGNAL
										PRINTLN("[SECUROHACK] PROCESS_VEH_OBJECTIVES_CLIENT Losing signal now!")
									ENDIF
								ELSE
									IF iHackStage = ciSECUROSERV_HACK_STAGE_LOSING_SIGNAL
										iHackStage = ciSECUROSERV_HACK_STAGE_HACKING
										PRINTLN("[SECUROHACK] PROCESS_VEH_OBJECTIVES_CLIENT Signal back to normal now!")
									ENDIF
								ENDIF
							ENDIF
							iPreviousVehHack = iVeh
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_VEH_BODY_CLEANUP(FMMC_VEHICLE_STATE &sVehState)
	IF DOES_BLIP_EXIST(biVehBlip[sVehState.iIndex]) 
		CPRINTLN(DEBUG_MC_BLIP, "[RCC MISSION] PROCESS_VEH_BODY_CLEANUP - Removing blip for vehicle ", sVehState.iIndex, ". The vehicle netID doesn't exist.")
		REMOVE_VEHICLE_BLIP(sVehState.iIndex)
	ENDIF
	IF IS_BIT_SET(iTitanRotorParticlesBitset, sVehState.iIndex)
		CLEAR_BIT(iTitanRotorParticlesBitset, sVehState.iIndex)
		STOP_PARTICLE_FX_LOOPED(ptfxTitanRotorParticles[sVehState.iIndex][0])
		STOP_PARTICLE_FX_LOOPED(ptfxTitanRotorParticles[sVehState.iIndex][1])
		STOP_PARTICLE_FX_LOOPED(ptfxTitanRotorParticles[sVehState.iIndex][2])
		STOP_PARTICLE_FX_LOOPED(ptfxTitanRotorParticles[sVehState.iIndex][3])
		IF iTitanRotorParticlesBitset = 0
			REMOVE_NAMED_PTFX_ASSET("scr_gr_def")
		ENDIF
	ENDIF
	
	CLEAR_BIT(iLocalBoolCheck26, LBOOL26_OVERRIDING_SEAT_PREF_SPECIAL_VEH)

	IF IS_BIT_SET(iDisableVehiclColdmgBitset, sVehState.iIndex)	
		PRINTLN("[LM][PROCESS_VEH_BODY] PROCESS_VEH_BODY_CLEANUP - (1) Clearing iDisableVehiclColdmgBitset for sVehState.iIndex: ", sVehState.iIndex)
		CLEAR_BIT(iDisableVehiclColdmgBitset, sVehState.iIndex)	
	ENDIF
	
	CLEAR_BIT(iDisableVehiclePedNavigation, sVehState.iIndex)
	
	CLEANUP_COLLISION_ON_VEH(NULL,sVehState.iIndex)
ENDPROC

PROC PROCESS_VEH_BODY_DRIVABLE_CLEANUP(FMMC_VEHICLE_STATE &sVehState)
	IF DOES_BLIP_EXIST(biVehBlip[sVehState.iIndex])
		PRINTLN("[Vehicles] PROCESS_VEH_BODY_DRIVABLE_CLEANUP - Removing blip for vehicle ", sVehState.iIndex, ". The vehicle is no longer driveable.")
		REMOVE_VEHICLE_BLIP(sVehState.iIndex)
	ENDIF		
	
	IF IS_TARGET_TAGGING_ENABLED()
		REMOVE_TAGGED_BLIP(2, sVehState.iIndex)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetEight, ciFMMC_VEHICLE8_OffRuleQuickGPS)
		MPGlobalsAmbience.vQuickGPS2 = <<0,0,0>>
	ENDIF
					
	CLEANUP_COLLISION_ON_VEH(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[sVehState.iIndex]),sVehState.iIndex)
ENDPROC

PROC PROCESS_VEHICLE_MINES(FMMC_VEHICLE_STATE &sVehState)
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetFive, ciFMMC_VEHICLE5_ENABLE_REAR_MINES_DUNE3)
		IF g_bDisableVehicleMines
			PRINTLN("[LM][PROCESS_RESET_DUNE_REAR_MINES][MINES] - g_bDisableVehicleMines is not set Setting to false so we can use the mines.")
			DISABLE_VEHICLE_MINES(FALSE)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_BLOCK_MANUAL_RESPAWN_IN_VEHICLE(FMMC_VEHICLE_STATE &sVehState)
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetSeven, ciFMMC_VEHICLE7_VEHICLE_BLOCK_MANUAL_RESPAWN)
		IF NOT IS_BIT_SET(iLocalBoolCheck25, LBOOL25_BLOCK_MANUAL_RESPAWN_DUE_TO_VEHICLE)
			PRINTLN("[LM][MANUAL RESPAWN] - We are inside sVehState.iIndex: ", sVehState.iIndex, " Which has LBOOL25_BLOCK_MANUAL_RESPAWN_DUE_TO_VEHICLE set.")
			SET_BIT(iLocalBoolCheck25, LBOOL25_BLOCK_MANUAL_RESPAWN_DUE_TO_VEHICLE)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_PLAYER_INVINCIBLE_IN_VEHICLE(FMMC_VEHICLE_STATE &sVehState)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetSeven, ciFMMC_VEHICLE7_PLAYER_INVINCIBLE_IN_VEHICLE)
		EXIT
	ENDIF
	
	BOOL bCanPlayerBeDamaged = GET_ENTITY_CAN_BE_DAMAGED(LocalPlayerPed)
	
	IF IS_LOCAL_PLAYER_IN_FMMC_VEHICLE(sVehState)
		IF NOT (IS_ENTITY_IN_WATER(localPlayerPed) AND IS_ENTITY_IN_DEEP_WATER(sVehState.vehIndex))
			IF bCanPlayerBeDamaged
				SET_ENTITY_CAN_BE_DAMAGED(LocalPlayerPed, FALSE)
				PRINTLN("[LM][PROCESS_VEH_BODY] - Player is in vehicle with ciFMMC_VEHICLE7_PLAYER_INVINCIBLE_IN_VEHICLE. Setting SET_ENTITY_CAN_BE_DAMAGED to false - sVehState.iIndex: ", sVehState.iIndex)				
				SET_BIT(iLocalboolCheck25, LBOOL25_CANNOT_BE_DAMAGED_DUE_TO_VEHICLE)
				iVehLastInsideGrantedInvulnability = sVehState.iIndex
			ENDIF
		ELSE
			IF NOT bCanPlayerBeDamaged
			AND IS_BIT_SET(iLocalBoolCheck25, LBOOL25_CANNOT_BE_DAMAGED_DUE_TO_VEHICLE)
				SET_ENTITY_CAN_BE_DAMAGED(LocalPlayerPed, TRUE)
				PRINTLN("[LM][PROCESS_VEH_BODY] - Player in water with ciFMMC_VEHICLE7_PLAYER_INVINCIBLE_IN_VEHICLE. Setting SET_ENTITY_CAN_BE_DAMAGED to true - sVehState.iIndex: ", sVehState.iIndex)		
				CLEAR_BIT(iLocalBoolCheck25, LBOOL25_CANNOT_BE_DAMAGED_DUE_TO_VEHICLE)
				iVehLastInsideGrantedInvulnability = -1
			ENDIF
		ENDIF
	ELSE
		IF NOT bCanPlayerBeDamaged
		AND IS_BIT_SET(iLocalBoolCheck25, LBOOL25_CANNOT_BE_DAMAGED_DUE_TO_VEHICLE)
			SET_ENTITY_CAN_BE_DAMAGED(LocalPlayerPed, TRUE)
			PRINTLN("[LM][PROCESS_VEH_BODY] - Player left a vehicle with ciFMMC_VEHICLE7_PLAYER_INVINCIBLE_IN_VEHICLE. Setting SET_ENTITY_CAN_BE_DAMAGED to true - sVehState.iIndex: ", sVehState.iIndex)		
			CLEAR_BIT(iLocalBoolCheck25, LBOOL25_CANNOT_BE_DAMAGED_DUE_TO_VEHICLE)
			iVehLastInsideGrantedInvulnability = -1
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_OFF_RULE_QUICK_GPS_FOR_VEHICLE(FMMC_VEHICLE_STATE &sVehState)
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetEight, ciFMMC_VEHICLE8_OffRuleQuickGPS)
		MPGlobalsAmbience.vQuickGPS2 = GET_FMMC_VEHICLE_COORDS(sVehState)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iCustomVehName > -1
			MPGlobalsAmbience.tlQuickGPS_Offrule = g_FMMC_STRUCT_ENTITIES.tlCustomVehName[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iCustomVehName]
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_COLLISION_DAMAGE_IMMUNITY_WITH_OTHER_PLACED_VEHICLES(FMMC_VEHICLE_STATE &sVehState)

	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehicleIndexForColImmunity > -1
		IF NOT IS_BIT_SET(iShouldProcessVehiclColdmgBitset, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehicleIndexForColImmunity)
			SET_BIT(iShouldProcessVehiclColdmgBitset, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehicleIndexForColImmunity)
			PRINTLN("[LM][PROCESS_VEH_BODY] PROCESS_COLLISION_DAMAGE_IMMUNITY_WITH_OTHER_PLACED_VEHICLES - Setting iShouldProcessVehiclColdmgBitset from ", sVehState.iIndex, " for sVehState.iIndex: ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehicleIndexForColImmunity)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iShouldProcessVehiclColdmgBitset, sVehState.iIndex)
		INT iVeh = 0
		FOR iVeh = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles-1
			IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehicleIndexForColImmunity > -1
				IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
					VEHICLE_INDEX vehTarget = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
					BOOL bTargetAlive = IS_ENTITY_ALIVE(vehTarget)
					
					IF NOT IS_BIT_SET(iDisableVehiclColdmgBitset, iVeh)
						IF bTargetAlive
						AND sVehState.bAlive
							SET_ENTITY_CANT_CAUSE_COLLISION_DAMAGED_ENTITY(sVehState.vehIndex, vehTarget)
							SET_BIT(iDisableVehiclColdmgBitset, iVeh)
							
							PRINTLN("[LM][PROCESS_VEH_BODY] PROCESS_COLLISION_DAMAGE_IMMUNITY_WITH_OTHER_PLACED_VEHICLES - Setting iDisableVehiclColdmgBitset for iVeh: ", iVeh)
						ENDIF						
					ELSE
						IF bTargetAlive
						AND sVehState.bAlive
							CLEAR_BIT(iDisableVehiclColdmgBitset, iVeh)
							PRINTLN("[LM][PROCESS_VEH_BODY] PROCESS_COLLISION_DAMAGE_IMMUNITY_WITH_OTHER_PLACED_VEHICLES - (1) Clearing iDisableVehiclColdmgBitset for iVeh: ", iVeh)
						ENDIF
					ENDIF						
				ELSE
					IF IS_BIT_SET(iDisableVehiclColdmgBitset, iVeh)
						PRINTLN("[LM][PROCESS_VEH_BODY] PROCESS_COLLISION_DAMAGE_IMMUNITY_WITH_OTHER_PLACED_VEHICLES - (2) Clearing iDisableVehiclColdmgBitset for iVeh: ", iVeh)
						CLEAR_BIT(iDisableVehiclColdmgBitset, iVeh)
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
ENDPROC

PROC PROCESS_VEHICLE_DISABLE_VEHICLE_ACTIVE_FOR_PED_NAVIGATION(FMMC_VEHICLE_STATE &sVehState)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetTen, ciFMMC_VEHICLE10_DisableForPedNavigation)
		EXIT
	ENDIF
	
	IF IS_BIT_SET(iDisableVehiclePedNavigation, sVehState.iIndex)
		EXIT
	ENDIF
	
	PRINTLN("[Vehicles][Vehicle ", sVehState.iIndex, "] - PROCESS_VEHICLE_DISABLE_VEHICLE_ACTIVE_FOR_PED_NAVIGATION - Vehicle disabled for ped navigation!")
	SET_VEHICLE_ACTIVE_FOR_PED_NAVIGATION(sVehState.vehIndex, FALSE)		
	SET_BIT(iDisableVehiclePedNavigation, sVehState.iIndex)
	
ENDPROC

PROC PROCESS_SEAT_PREFERENCE_OVERRIDE(FMMC_VEHICLE_STATE &sVehState)
	// Very specific for Heists or Special Missions with 4 players.
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetSeven, ciFMMC_VEHICLE7_SPECIAL_VOLATOL_SEAT_PREF_OVERRIDE)
		IF iVehSeatPrefOverrideVeh != sVehState.iIndex
			PRINTLN("[LM][PROCESS_VEH_BODY] PROCESS_SEAT_PREFERENCE_OVERRIDE - Setting iVehSeatPrefOverrideVeh to sVehState.iIndex: ", sVehState.iIndex)
			iVehSeatPrefOverrideVeh = sVehState.iIndex
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iSeatBitset != 0
		PRINTLN("[LM][PROCESS_VEH_BODY] PROCESS_SEAT_PREFERENCE_OVERRIDE - Using forced Seating bitset. iSeatBitset: ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iSeatBitset)
		
		VEHICLE_SEAT vs_ForcedSeating
		PED_INDEX pedNone
		INT iSeatMax = ENUM_TO_INT(VS_EXTRA_RIGHT_3)
		INT iSeat = -1	
		FOR iSeat = -1 TO iSeatMax-1
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iSeatBitset, iSeat+1)
				
				PED_INDEX pedInVeh = GET_PED_IN_VEHICLE_SEAT(sVehState.vehIndex, INT_TO_ENUM(VEHICLE_SEAT, iSeat))
			
				IF pedInVeh = pedNone
				OR NOT IS_PED_A_PLAYER(pedInVeh)
					vs_ForcedSeating = INT_TO_ENUM(VEHICLE_SEAT, iSeat)
					BREAKLOOP
				ENDIF
				
			ENDIF
		ENDFOR
		
		PRINTLN("[LM][PROCESS_VEH_BODY] PROCESS_SEAT_PREFERENCE_OVERRIDE - vs_ForcedSeating: ", ENUM_TO_INT(vs_ForcedSeating))
		
		SET_PED_VEHICLE_FORCED_SEAT_USAGE(LocalPlayerPed, sVehState.vehIndex, iVehicleSeatingEnforcementSlot, 0, vs_ForcedSeating)
		iVehicleSeatingEnforcementSlot++
	ENDIF
ENDPROC

PROC PROCESS_DOOR_STAGGERED_SETTINGS(FMMC_VEHICLE_STATE &sVehState)
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehicleDoorSettingsOnRule = -1
	AND NOT IS_BIT_SET(iLocalBoolCheck26, LBOOL26_FIRST_STAGGERED_LOOP_OF_VEH_BODY)
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitSet, ENUM_TO_INT(SC_DOOR_FRONT_LEFT))
			SET_VEHICLE_DOOR_OPEN(sVehState.vehIndex,SC_DOOR_FRONT_LEFT, FALSE, TRUE)
			PRINTLN("[RCC MISSION] PROCESS_VEH_BODY - PROCESS_DOOR_STAGGERED_SETTINGS - opening FL DOOR for vehicle: ", sVehState.iIndex) 
		ENDIF
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitSet, ENUM_TO_INT(SC_DOOR_FRONT_RIGHT))
			SET_VEHICLE_DOOR_OPEN(sVehState.vehIndex,SC_DOOR_FRONT_RIGHT, FALSE, TRUE)
			PRINTLN("[RCC MISSION] PROCESS_VEH_BODY - PROCESS_DOOR_STAGGERED_SETTINGS - opening FR DOOR for vehicle: ", sVehState.iIndex) 
		ENDIF
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitSet, ENUM_TO_INT(SC_DOOR_REAR_LEFT))
			SET_VEHICLE_DOOR_OPEN(sVehState.vehIndex,SC_DOOR_REAR_LEFT, FALSE, TRUE)
			PRINTLN("[RCC MISSION] PROCESS_VEH_BODY - PROCESS_DOOR_STAGGERED_SETTINGS - opening RL DOOR for vehicle: ", sVehState.iIndex) 
		ENDIF
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitSet, ENUM_TO_INT(SC_DOOR_REAR_RIGHT))
			SET_VEHICLE_DOOR_OPEN(sVehState.vehIndex,SC_DOOR_REAR_RIGHT, FALSE, TRUE)
			PRINTLN("[RCC MISSION] PROCESS_VEH_BODY - PROCESS_DOOR_STAGGERED_SETTINGS - opening RR DOOR for vehicle: ", sVehState.iIndex) 
		ENDIF
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitSet, ENUM_TO_INT(SC_DOOR_BONNET))
			SET_VEHICLE_DOOR_OPEN(sVehState.vehIndex,SC_DOOR_BONNET, FALSE, TRUE)
			PRINTLN("[RCC MISSION] PROCESS_VEH_BODY - PROCESS_DOOR_STAGGERED_SETTINGS - opening BONNET for vehicle: ", sVehState.iIndex) 
		ENDIF
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitSet, ENUM_TO_INT(SC_DOOR_BOOT))
			SET_VEHICLE_DOOR_OPEN(sVehState.vehIndex,SC_DOOR_BOOT, FALSE, TRUE)
			PRINTLN("[RCC MISSION] PROCESS_VEH_BODY - PROCESS_DOOR_STAGGERED_SETTINGS - opening BOOT for vehicle: ", sVehState.iIndex) 
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitSet, ENUM_TO_INT(SC_DOOR_BOOT))
	AND NOT IS_BIT_SET(ibootbitset,sVehState.iIndex)
		IF DOES_ENTITY_EXIST(GET_PED_IN_VEHICLE_SEAT(sVehState.vehIndex, VS_DRIVER))
			IF NOT IS_VEHICLE_STOPPED(sVehState.vehIndex)
				SET_VEHICLE_DOOR_OPEN(sVehState.vehIndex, SC_DOOR_BOOT, TRUE,FALSE)
				PRINTLN("[RCC MISSION] PROCESS_DOOR_STAGGERED_SETTINGS - Calling SET_VEHICLE_DOOR_OPEN(TRUE, FALSE) for SC_DOOR_BOOT for vehicle: ") NET_PRINT_INT(sVehState.iIndex) NET_NL()
			ENDIF
		ENDIF
		SET_BIT(ibootbitset, sVehState.iIndex)
	ENDIF
	
	IF NOT IS_BIT_SET(iVehicleDoorUnlockBitset, sVehState.iIndex)
	AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iDoorUnlockRule != -1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iAssociatedTeam !=-1
	 	OR g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iSecondAssociatedTeam !=-1
		OR g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iThirdAssociatedTeam !=-1
		OR g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iFourthAssociatedTeam !=-1
			IF NOT IS_ANY_PLAYER_TRYING_TO_OPEN_LOCKED_VEHICLE()
				
				BOOL bUnlockVeh = FALSE
				
				IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iAssociatedTeam !=-1
				AND MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iAssociatedTeam] < FMMC_MAX_RULES
				AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iDoorUnlockRule <= MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iAssociatedTeam]
				AND NOT (IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetTwo, ciFMMC_VEHICLE2_LOCK_ON_RULE) AND DOES_VEHICLE_HAVE_DELIVERED_DECOR_FOR_TEAM(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iAssociatedTeam,sVehState.iIndex,MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iAssociatedTeam]))
					
					bUnlockVeh = TRUE
					
				ELIF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iSecondAssociatedTeam !=-1 
				AND MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iSecondAssociatedTeam] < FMMC_MAX_RULES
				AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iDoorUnlockRule <= MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iSecondAssociatedTeam]
					
					bUnlockVeh = TRUE
					
				ELIF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iThirdAssociatedTeam != -1 
				AND MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iThirdAssociatedTeam] < FMMC_MAX_RULES
				AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iDoorUnlockRule <= MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iThirdAssociatedTeam]
					
					bUnlockVeh = TRUE
					
				ELIF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iFourthAssociatedTeam !=-1
				AND MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iFourthAssociatedTeam] < FMMC_MAX_RULES
				AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iDoorUnlockRule <= MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iFourthAssociatedTeam]
					
					bUnlockVeh = TRUE
					
				ENDIF
				
				IF bUnlockVeh
					INT iTeam
					FOR iTeam = 0 TO (FMMC_MAX_TEAMS-1)
						SET_VEHICLE_DOORS_LOCKED_FOR_TEAM(sVehState.vehIndex, iTeam, FALSE)
					ENDFOR
					
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetEight, ciFMMC_VEHICLE8_REQUIRES_HOTWIRE) // [ML] url:bugstar:5833072 - Locking then unlocking on associated rule was overriding this bitset.
						SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(sVehState.vehIndex, FALSE)
						SET_VEHICLE_DOORS_LOCKED(sVehState.vehIndex,VEHICLELOCK_LOCKED_BUT_CAN_BE_DAMAGED)
						SET_VEHICLE_NEEDS_TO_BE_HOTWIRED(sVehState.vehIndex,TRUE)		
						PRINTLN("[RCC MISSION] PROCESS_DOOR_STAGGERED_SETTINGS - vehicle: ",sVehState.iIndex," Unlocking on this rule, but set as requiring Hotwire")
					ELSE
						SET_VEHICLE_DOORS_LOCKED(sVehState.vehIndex, VEHICLELOCK_UNLOCKED)
						SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(sVehState.vehIndex, FALSE)
						SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(sVehState.vehIndex,FALSE)
						PRINTLN("[RCC MISSION] PROCESS_DOOR_STAGGERED_SETTINGS - Unlocking the doors for vehicle: ",sVehState.iIndex," g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iDoorUnlockRule : ",g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iDoorUnlockRule, " iAssociatedTeam = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iAssociatedTeam, " Model = ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(sVehState.mn))			
					ENDIF
					//-- Don't want any rear doors being unlocked if there's crates in the back.
					LOCK_VEHICLE_DOORS_FOR_CRATES(sVehState.vehIndex, sVehState.iIndex)
					SET_BIT(iVehicleDoorUnlockBitset,sVehState.iIndex)
				
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF sVehState.mn = AVENGER
	AND GB_GET_LOCAL_PLAYER_GANG_BOSS() = LocalPlayer
		
		LOCK_STATE vehLockState = GET_VEHICLE_DOOR_LOCK_STATUS(sVehState.vehIndex)
		IF vehLockState = VEHICLELOCK_LOCKED
		OR vehLockState = VEHICLELOCK_LOCKED_BUT_CAN_BE_DAMAGED
		OR vehLockState = VEHICLELOCK_LOCKED_BUT_BOOT_UNLOCKED
		OR vehLockState = VEHICLELOCK_LOCKED_NO_PASSENGERS
			
			IF NOT IS_CREATOR_AIRCRAFT_ENTRY_BLOCKED(LocalPlayer)
				PRINTLN("[RCC MISSION] PROCESS_DOOR_STAGGERED_SETTINGS - vehLockState: ", vehLockState, " - BLOCK_CREATOR_AIRCRAFT_INTERIOR_ENTRY = TRUE")
				BLOCK_CREATOR_AIRCRAFT_INTERIOR_ENTRY(TRUE)
			ENDIF
			
		ELSE
		
			IF IS_CREATOR_AIRCRAFT_ENTRY_BLOCKED(LocalPlayer)
			AND NOT IS_AVENGER_HOLD_TRANSITION_BLOCKED()
				PRINTLN("[RCC MISSION] PROCESS_DOOR_STAGGERED_SETTINGS - vehLockState: ", vehLockState, " - BLOCK_CREATOR_AIRCRAFT_INTERIOR_ENTRY = FALSE")
				BLOCK_CREATOR_AIRCRAFT_INTERIOR_ENTRY(FALSE)
			ENDIF

		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_DELUXO_FLIGHT_MODE(FMMC_VEHICLE_STATE &sVehState)

	IF sVehState.mn != DELUXO
		EXIT
	ENDIF
	
	INT iTeamVeh = MC_PlayerBD[iLocalPart].iteam
	INT iRuleVeh = MC_ServerBD_4.iCurrentHighestPriority[iTeamVeh]
			
	IF iRuleVeh < FMMC_MAX_RULES
		
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeamVeh].iRuleBitsetEleven[iRuleVeh], ciBS_RULE11_DISABLE_DELUXO_DRIVE)
		AND IS_LOCAL_PLAYER_IN_FMMC_VEHICLE(sVehState)
			SET_SPECIAL_FLIGHT_MODE_TARGET_RATIO(sVehState.vehIndex, 1.0)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_TRANSFORM)
		ENDIF
		
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeamVeh].iRuleBitsetTen[iRuleVeh], ciBS_RULE10_DISABLE_DELUXO_FLIGHT_MODE)
			IF NOT IS_BIT_SET(iFlightModeEnabledForVeh, sVehState.iIndex)
				SET_BIT(iFlightModeEnabledForVeh, sVehState.iIndex)
				PRINTLN("[RCC MISSION][PROCESS_VEH_BODY] PROCESS_DELUXO_FLIGHT_MODE - Vehicle ", sVehState.iIndex," Special flight mode activated this rule. iRule: ", iRuleVeh)
				
				SET_SPECIAL_FLIGHT_MODE_ALLOWED(sVehState.vehIndex, TRUE)
				SET_DISABLE_HOVER_MODE_FLIGHT(sVehState.vehIndex, FALSE)

			ENDIF
		ELSE
			
			IF IS_BIT_SET(iLocalBoolCheck27, LBOOL27_DELUXO_SOUND_UNLOCK_FLIGHT_PLAYED)
				CLEAR_BIT(iLocalboolCheck27, LBOOL27_DELUXO_SOUND_UNLOCK_FLIGHT_PLAYED)
				PRINTLN("[RCC MISSION][PROCESS_VEH_BODY] PROCESS_DELUXO_FLIGHT_MODE - Vehicle ", sVehState.iIndex," Special flight mode Deactivated Clearing Sound Bit")
			ENDIF
			
			//Allow hover if it's not disabled
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeamVeh].iRuleBitsetTen[iRuleVeh], ciBS_RULE10_DISABLE_DELUXO_HOVER_MODE)
				PRINTLN("[RCC MISSION][PROCESS_VEH_BODY] PROCESS_DELUXO_FLIGHT_MODE - Vehicle ", sVehState.iIndex," Re-enabling hover flight mode iRule: ", iRuleVeh)
				
				SET_SPECIAL_FLIGHT_MODE_ALLOWED(sVehState.vehIndex, TRUE)
				SET_DISABLE_HOVER_MODE_FLIGHT(sVehState.vehIndex, TRUE)
				
			ELSE
				PRINTLN("[RCC MISSION][PROCESS_VEH_BODY] PROCESS_DELUXO_FLIGHT_MODE - Vehicle ", sVehState.iIndex," Re-disabling hover flight mode iRule: ", iRuleVeh)
				SET_SPECIAL_FLIGHT_MODE_ALLOWED(sVehState.vehIndex, FALSE)
				SET_DISABLE_HOVER_MODE_FLIGHT(sVehState.vehIndex, FALSE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_VEHICLE_INVINCIBLE_WHEN_EMPTY(FMMC_VEHICLE_STATE &sVehState)
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetSix,  ciFMMC_VEHICLE6_VEHICLE_IS_INVINCIBLE_WHEN_EMPTY)
		BOOL bCanVehBeDamaged = GET_ENTITY_CAN_BE_DAMAGED(sVehState.vehIndex)
		IF IS_VEHICLE_EMPTY(sVehState.vehIndex)
			IF bCanVehBeDamaged
				SET_ENTITY_CAN_BE_DAMAGED(sVehState.vehIndex, FALSE)
				PRINTLN("[LM][PROCESS_VEH_BODY] PROCESS_VEHICLE_INVINCIBLE_WHEN_EMPTY - Vehicle Is Empty. Setting SET_ENTITY_CAN_BE_DAMAGED to false for sVehState.iIndex: ", sVehState.iIndex)
			ENDIF
		ELSE
			IF NOT bCanVehBeDamaged
				SET_ENTITY_CAN_BE_DAMAGED(sVehState.vehIndex, TRUE)
				PRINTLN("[LM][PROCESS_VEH_BODY] PROCESS_VEHICLE_INVINCIBLE_WHEN_EMPTY - Vehicle no longer Empty. Setting SET_ENTITY_CAN_BE_DAMAGED to true for sVehState.iIndex: ", sVehState.iIndex)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_DISABLING_VEHICLE_HOMING_ROCKETS(FMMC_VEHICLE_STATE &sVehState)
	IF NOT bLocalPlayerPedOK
		EXIT
	ENDIF
	
	IF IS_LOCAL_PLAYER_IN_FMMC_VEHICLE(sVehState)
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetSix,  ciFMMC_VEHICLE6_DISABLE_HOMING_MISSILES)
			IF sVehState.bDrivable
				IF DOES_VEHICLE_HAVE_WEAPONS(sVehState.vehIndex)
					IF NOT IS_BIT_SET(iLocalBoolCheck25, LBOOL25_VEHICLE_WEAPONS_HOMING_DISABLED)
						SET_PLAYER_HOMING_DISABLED_FOR_ALL_VEHICLE_WEAPONS(localPlayer, TRUE)
						SET_BIT(iLocalBoolCheck25, LBOOL25_VEHICLE_WEAPONS_HOMING_DISABLED)
						
						IF sVehState.bExists
							IF sVehState.mn = HUNTER
								DISABLE_VEHICLE_WEAPON(TRUE, WEAPONTYPE_DLC_VEHICLE_HUNTER_MISSILE, sVehState.vehIndex, LocalPlayerPed)
							ENDIF
						ENDIF
						
						PRINTLN("[RCC MISSION] PROCESS_VEH_BODY PROCESS_DISABLING_VEHICLE_HOMING_ROCKETS - sVehState.iIndex ", sVehState.iIndex, " Blocking Homing Missiles.")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELIF NOT IS_PED_IN_ANY_VEHICLE(localPlayerPed)
		IF IS_BIT_SET(iLocalBoolCheck25, LBOOL25_VEHICLE_WEAPONS_HOMING_DISABLED)
			SET_PLAYER_HOMING_DISABLED_FOR_ALL_VEHICLE_WEAPONS(localPlayer, FALSE)
			CLEAR_BIT(iLocalBoolCheck25, LBOOL25_VEHICLE_WEAPONS_HOMING_DISABLED)
			
			IF sVehState.bExists
				IF sVehState.mn = HUNTER
					DISABLE_VEHICLE_WEAPON(FALSE, WEAPONTYPE_DLC_VEHICLE_HUNTER_MISSILE, sVehState.vehIndex, LocalPlayerPed)
				ENDIF
			ENDIF
			
			PRINTLN("[RCC MISSION] PROCESS_VEH_BODY PROCESS_DISABLING_VEHICLE_HOMING_ROCKETS - sVehState.iIndex ", sVehState.iIndex, " Clearing Blocked Homing Missiles.")
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_HELI_ROTOR_DAMAGE(FMMC_VEHICLE_STATE &sVehState)
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetFive, ciFMMC_VEHICLE5_PREVENT_HELI_ROTOR_DAMAGE)
		IF IS_THIS_MODEL_A_HELI(sVehState.mn)
			IF NOT IS_BIT_SET(iHeliRotorHealthCacheBitset, sVehState.iIndex)
				fHeliMainRotorHealthCache[sVehState.iIndex] = GET_HELI_MAIN_ROTOR_HEALTH(sVehState.vehIndex)
				fHeliTailRotorHealthCache[sVehState.iIndex] = GET_HELI_TAIL_ROTOR_HEALTH(sVehState.vehIndex)
				
				SET_HELI_TAIL_BOOM_CAN_BREAK_OFF(sVehState.vehIndex, FALSE)
				
				SET_BIT(iHeliRotorHealthCacheBitset, sVehState.iIndex)
			ELSE
				IF GET_HELI_MAIN_ROTOR_HEALTH(sVehState.vehIndex) != fHeliMainRotorHealthCache[sVehState.iIndex]
					SET_HELI_MAIN_ROTOR_HEALTH(sVehState.vehIndex, fHeliMainRotorHealthCache[sVehState.iIndex])
				ENDIF
				
				IF GET_HELI_TAIL_ROTOR_HEALTH(sVehState.vehIndex) != fHeliTailRotorHealthCache[sVehState.iIndex]
					SET_HELI_TAIL_ROTOR_HEALTH(sVehState.vehIndex, fHeliTailRotorHealthCache[sVehState.iIndex])
				ENDIF
			ENDIF
		ELIF IS_THIS_MODEL_A_PLANE(sVehState.mn)
			SET_PLANE_PROPELLER_HEALTH(sVehState.vehIndex, 1000.0)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_KEEP_VEHICLE_IN_BOUNDS(FMMC_VEHICLE_STATE &sVehState)
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetSeven, ciFMMC_VEHICLE7_KEEP_IN_BOUNDS)
				
		IF IS_ENTITY_OUT_OF_BOUNDS(sVehState.vehIndex, 0)
		AND IS_VEHICLE_EMPTY(sVehState.vehIndex, TRUE) 
		AND (NOT IS_VEHICLE_A_TRAILER(sVehState.vehIndex) OR (IS_VEHICLE_A_TRAILER(sVehState.vehIndex) AND NOT IS_ENTITY_ATTACHED_TO_ANY_VEHICLE(sVehState.vehIndex)))
		AND NOT IS_LOCAL_PLAYER_IN_FMMC_VEHICLE(sVehState, TRUE)
			IF NOT HAS_NET_TIMER_STARTED(tdKeepVehicleInBounds[sVehState.iIndex])
				REINIT_NET_TIMER(tdKeepVehicleInBounds[sVehState.iIndex])
			ELSE
				IF HAS_NET_TIMER_EXPIRED(tdKeepVehicleInBounds[sVehState.iIndex], ciKeepVehicleInBoundsTimer)
					NETWORK_EXPLODE_VEHICLE(sVehState.vehIndex, TRUE)
					RESET_NET_TIMER(tdKeepVehicleInBounds[sVehState.iIndex])
				ENDIF
			ENDIF
		ELSE
			IF HAS_NET_TIMER_STARTED(tdKeepVehicleInBounds[sVehState.iIndex])
				RESET_NET_TIMER(tdKeepVehicleInBounds[sVehState.iIndex])	
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_AVENGER_INTERIOR_ENTRY(FMMC_VEHICLE_STATE &sVehState)
	IF sVehState.mn = AVENGER
	AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciENABLE_AVENGER_INTERIOR_ENTRY)
		IF sVehState.bAlive
		AND IS_ENTITY_IN_AIR(sVehState.vehIndex)
			IF IS_FMMC_VEHICLE_EMPTY(sVehState)
			AND NOT IS_ANY_PLAYER_IN_VEHICLE_INTERIOR()
				IF NOT HAS_NET_TIMER_STARTED(tdEmptyAvengerTimer)
					REINIT_NET_TIMER(tdEmptyAvengerTimer)
					PRINTLN("[RCC MISSION] PROCESS_AVENGER_INTERIOR_ENTRY - Starting crash timer for Avenger. Vehicle: ", sVehState.iIndex)
				ELSE
					IF HAS_NET_TIMER_EXPIRED(tdEmptyAvengerTimer, ciForceAvengerCrash)
						CLEAR_PRIMARY_VEHICLE_TASK(sVehState.vehIndex)
						BROADCAST_EVENT_AVENGER_AUTOPILOT_ACTIVATED(GET_OWNER_OF_CREATOR_AIRCRAFT(sVehState.vehIndex), FALSE)
						PRINTLN("[RCC MISSION] PROCESS_AVENGER_INTERIOR_ENTRY - Crashing Avenger as it has been empty and in the air for 5 seconds")
					ENDIF
				ENDIF
			ELSE
				IF HAS_NET_TIMER_STARTED(tdEmptyAvengerTimer)
					PRINTLN("[RCC MISSION] PROCESS_AVENGER_INTERIOR_ENTRY - Resetting Avenger Timer 1")
					RESET_NET_TIMER(tdEmptyAvengerTimer)
				ENDIF
			ENDIF
		ELSE
			IF HAS_NET_TIMER_STARTED(tdEmptyAvengerTimer)
				PRINTLN("[RCC MISSION] PROCESS_AVENGER_INTERIOR_ENTRY - Resetting Avenger Timer 2")
				RESET_NET_TIMER(tdEmptyAvengerTimer)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_BOMB_BAY_DOORS_HELP_TEXT(FMMC_VEHICLE_STATE &sVehState)
	IF IS_PLAYER_SPECTATING(localPlayer)
		EXIT
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetSeven, ciFMMC_VEHICLE7_OPEN_BOMB_BAY_DOORS_HELP_TEXT)
	AND IS_ENTITY_IN_AIR(sVehState.vehIndex)
	AND IS_PED_IN_ANY_VEHICLE(localPlayerPed, FALSE)
	AND IS_LOCAL_PLAYER_IN_FMMC_VEHICLE(sVehState)
		
		BOOL bHasControl = FALSE					
		IF GET_SEAT_PED_IS_IN(localPlayerPed) = VS_FRONT_RIGHT
		AND MPGlobalsAmbience.bEnablePassengerBombing
			bHasControl = TRUE
		ELIF GET_SEAT_PED_IS_IN(localPlayerPed) = VS_DRIVER
		AND NOT MPGlobalsAmbience.bEnablePassengerBombing
			bHasControl = TRUE
		ENDIF
		
		IF bHasControl
			IF NOT GET_ARE_BOMB_BAY_DOORS_OPEN(sVehState.vehIndex)
				IF IS_BIT_SET(iLocalBoolCheck27, LBOOL27_BOMB_BAY_HELP_TEXT_PLAYED_OPEN)
					CLEAR_BIT(iLocalBoolCheck27, LBOOL27_BOMB_BAY_HELP_TEXT_PLAYED_OPEN)
					PRINTLN("[LM][PROCESS_VEH_BODY] - bomb bay doors are closed, clearing bit to re-allow help text (Use Controls).")
				ENDIF
				IF NOT IS_BIT_SET(iLocalBoolCheck27, LBOOL27_BOMB_BAY_HELP_TEXT_PLAYED)
					SET_BIT(iLocalBoolCheck27, LBOOL27_BOMB_BAY_HELP_TEXT_PLAYED)
					IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(PICK_STRING(IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetNine, ciFMMC_VEHICLE9_WaterBombs), "WB_CAM_HELP", "BOMB_CAM_HELP"))
						PRINT_HELP(PICK_STRING(IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetNine, ciFMMC_VEHICLE9_WaterBombs), "WB_CAM_HELP", "BOMB_CAM_HELP"), 10000)
						PRINTLN("[LM][PROCESS_VEH_BODY] - bomb bay doors are closed, Playing help text. (Open Doors)")
					ENDIF
				ENDIF
			ELIF GET_ARE_BOMB_BAY_DOORS_OPEN(sVehState.vehIndex)
				IF IS_BIT_SET(iLocalBoolCheck27, LBOOL27_BOMB_BAY_HELP_TEXT_PLAYED) 
					CLEAR_BIT(iLocalBoolCheck27, LBOOL27_BOMB_BAY_HELP_TEXT_PLAYED)
					PRINTLN("[LM][PROCESS_VEH_BODY] - bomb bay doors are open, clearing bit to re-allow help text (Open Doors).")
				ENDIF
				IF NOT IS_BIT_SET(iLocalBoolCheck27, LBOOL27_BOMB_BAY_HELP_TEXT_PLAYED_OPEN)
					SET_BIT(iLocalBoolCheck27, LBOOL27_BOMB_BAY_HELP_TEXT_PLAYED_OPEN)
					IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(PICK_STRING(IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetNine, ciFMMC_VEHICLE9_WaterBombs), "WB_HELP", "BOMB_HELP"))
						PRINT_HELP(PICK_STRING(IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetNine, ciFMMC_VEHICLE9_WaterBombs), "WB_HELP", "BOMB_HELP"), 10000)
						PRINTLN("[LM][PROCESS_VEH_BODY] - bomb bay doors are open, Playing help text. (Use Controls)")
					ENDIF
				ENDIF
			ENDIF					
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_OBJECTIVE_VEHICLE_ATTACHED_TO_CARGOBOB(FMMC_VEHICLE_STATE &sVehState)
	//Detecting Objective Vehicles Connected to Cargobob
	IF IS_OBJECTIVE_VEHICLE_ON_PLAYERS_CARGOBOB(sVehState)
		SET_BIT(iObjectiveVehicleAttachedToCargobobBS, sVehState.iIndex)
		PRINTLN("[RCC MISSION] PROCESS_VEH_BODY - Setting iObjectiveVehicleAttachedToCargobobBS for veh ", sVehState.iIndex)
	ELSE
		IF IS_BIT_SET(iObjectiveVehicleAttachedToCargobobBS, sVehState.iIndex)
			CLEAR_BIT(iObjectiveVehicleAttachedToCargobobBS, sVehState.iIndex)
			PRINTLN("[RCC MISSION] PROCESS_VEH_BODY - Clearing iObjectiveVehicleAttachedToCargobobBS for veh ", sVehState.iIndex)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_DUNE_RAMP_IMPULSE(FMMC_VEHICLE_STATE &sVehState)
	IF IS_LOCAL_PLAYER_IN_FMMC_VEHICLE(sVehState, TRUE)
		IF sVehState.mn = DUNE4
			IF NOT IS_BIT_SET(iLocalBoolCheck21, LBOOL21_DUNE4_RAMP_BUGGY_SIDE_IMPULSE_ACTIVE)
				PRINTLN("[RCC MISSION] PROCESS_DUNE_RAMP_IMPULSE - SETTING VEHICLE_SET_ENABLE_RAMP_CAR_SIDE_IMPULSE for vehicle: ", sVehState.iIndex)
				VEHICLE_SET_ENABLE_RAMP_CAR_SIDE_IMPULSE(sVehState.vehIndex, TRUE)
				SET_BIT(iLocalBoolCheck21, LBOOL21_DUNE4_RAMP_BUGGY_SIDE_IMPULSE_ACTIVE)
			ENDIF
		ENDIF
	ELIF NOT IS_PED_IN_ANY_VEHICLE(localPlayerPed)
		IF IS_BIT_SET(iLocalBoolCheck21, LBOOL21_DUNE4_RAMP_BUGGY_SIDE_IMPULSE_ACTIVE)
			CLEAR_BIT(iLocalBoolCheck21, LBOOL21_DUNE4_RAMP_BUGGY_SIDE_IMPULSE_ACTIVE)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_INSURGENT_HELP_TEXT(FMMC_VEHICLE_STATE &sVehState)
	IF NOT IS_BIT_SET(iLocalBoolCheck14, LBOOL14_INSURGENT_GUN_SEAT_HELP)
	AND NOT IS_BIT_SET(g_iFmNmhBitSet8, BI_FM_NMH8_TURRET_HELP)
		IF sVehState.mn = INSURGENT
		AND IS_LOCAL_PLAYER_IN_FMMC_VEHICLE(sVehState)
		AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
		AND IS_VEHICLE_SEAT_FREE(sVehState.vehIndex, VS_EXTRA_LEFT_3)
			VEHICLE_SEAT vehSeat = GET_SEAT_PED_IS_IN(LocalPlayerPed)
			
			IF vehSeat = VS_BACK_LEFT OR vehSeat = VS_BACK_RIGHT
				PRINT_HELP("FMMC_GUN_SEAT_HELP")
				PRINTLN("[RCC MISSION] - PRINT_HELP FMMC_GUN_SEAT_HELP")
				SET_BIT(iLocalBoolCheck14, LBOOL14_INSURGENT_GUN_SEAT_HELP)
				SET_BIT(g_iFmNmhBitSet8, BI_FM_NMH8_TURRET_HELP)
			ENDIF
		ENDIF
	ELSE
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FMMC_GUN_SEAT_HELP")
		AND sVehState.mn = INSURGENT
		AND IS_LOCAL_PLAYER_IN_FMMC_VEHICLE(sVehState)
			VEHICLE_SEAT vehSeat = GET_SEAT_PED_IS_IN(LocalPlayerPed)
			
			IF vehSeat = VS_EXTRA_LEFT_3
				PRINTLN("[RCC MISSION] PROCESS_INSURGENT_HELP_TEXT - CLEAR_HELP FMMC_GUN_SEAT_HELP")
				CLEAR_HELP(TRUE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_VEHICLE_HORN_OVERRIDE(FMMC_VEHICLE_STATE &sVehState)
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehTeamHorn > -1
	AND g_FMMC_STRUCT.iTeamHorn[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehTeamHorn] > -1
	AND g_FMMC_STRUCT.iTeamHorn[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehTeamHorn] != GET_VEHICLE_MOD(sVehState.vehIndex, MOD_HORN)
		PRINTLN("[JS] PROCESS_VEHICLE_HORN_OVERRIDE - Applying horn: ", GET_HORN_LABEL_FROM_MOD_INDEX(g_FMMC_STRUCT.iTeamHorn[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehTeamHorn]))
		SET_VEHICLE_MOD(sVehState.vehIndex, MOD_HORN, g_FMMC_STRUCT.iTeamHorn[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehTeamHorn], FALSE)
	ENDIF
ENDPROC

PROC PROCESS_TITAN_PTFX(FMMC_VEHICLE_STATE &sVehState)
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetSix, ciFMMC_VEHICLE6_TITAN_DAMAGED_PARTICLES)
	AND sVehState.mn = TITAN
		
		REQUEST_NAMED_PTFX_ASSET("scr_gr_def")
		
		IF HAS_NAMED_PTFX_ASSET_LOADED("scr_gr_def")
			IF NOT IS_BIT_SET(iTitanRotorParticlesBitset, sVehState.iIndex)
			AND GET_VEHICLE_ENGINE_HEALTH(sVehState.vehIndex) / MC_GET_FMMC_VEHICLE_MAX_ENGINE_HEALTH(sVehState.iIndex) < 0.70 
				SET_BIT(iTitanRotorParticlesBitset, sVehState.iIndex)
				USE_PARTICLE_FX_ASSET("scr_gr_def")
				ptfxTitanRotorParticles[sVehState.iIndex][0] = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_gr_sw_engine_smoke", sVehState.vehIndex, <<-10.5,-2.1,2.9>>, <<0,0,0>>)
				USE_PARTICLE_FX_ASSET("scr_gr_def")
				ptfxTitanRotorParticles[sVehState.iIndex][1] = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_gr_sw_engine_smoke", sVehState.vehIndex, <<-4.5,-2.1,2.9>>, <<0,0,0>>)
				USE_PARTICLE_FX_ASSET("scr_gr_def")
				ptfxTitanRotorParticles[sVehState.iIndex][2] = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_gr_sw_engine_smoke", sVehState.vehIndex, <<4.5,-2.1,2.9>>, <<0,0,0>>)
				USE_PARTICLE_FX_ASSET("scr_gr_def")
				ptfxTitanRotorParticles[sVehState.iIndex][3] = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_gr_sw_engine_smoke", sVehState.vehIndex, <<10.5,-2.1,2.9>>, <<0,0,0>>)
			ENDIF	
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_REMOTE_VEHICLE_EXPLOSIVE(FMMC_VEHICLE_STATE &sVehState)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetNine, ciFMMC_VEHICLE9_RemoteExplosive)
		EXIT
	ENDIF
	
	IF iRemoteVehicleExplosionInitiator = -1
		EXIT
	ENDIF
	
	PRINTLN("[Vehicle ", sVehState.iIndex, "][RemoteExplosive] PROCESS_REMOTE_VEHICLE_EXPLOSIVE - Exploded by remote vehicle explosive")
	NETWORK_EXPLODE_VEHICLE(sVehState.vehIndex, DEFAULT, TRUE, iRemoteVehicleExplosionInitiator)
	
ENDPROC

PROC PROCESS_VEHICLE_EXPLOSION_LOGIC_STAGGERED(FMMC_VEHICLE_STATE &sVehState)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetFour, ciFMMC_VEHICLE4_VEHICLE_OCCUPANT_EXPLOSION_DMG)
	AND NOT IS_BIT_SET(iLocalVehicleExplosiveBitset, sVehState.iIndex)	
		SET_BIT(iLocalVehicleExplosiveBitset, sVehState.iIndex)
		PRINTLN("[RCC MISSION] PROCESS_VEHICLE_EXPLOSION_LOGIC_STAGGERED - Allowing explosive damage to affect occupants of this vehicle: ", sVehState.iIndex)
		SET_VEHICLE_OCCUPANTS_TAKE_EXPLOSIVE_DAMAGE(sVehState.vehIndex, TRUE)
	ENDIF
	
	IF sVehState.mn = TANKER
	OR sVehState.mn = ARMYTANKER
	OR sVehState.mn = TANKER2
		FLOAT fPercentage = MC_GET_VEHICLE_TOTAL_HEALTH_PERCENTAGE(sVehState.vehIndex, sVehState.iIndex, GET_TOTAL_STARTING_PLAYERS())
		
		IF fPercentage <= 1
			NETWORK_EXPLODE_VEHICLE(sVehState.vehIndex, DEFAULT, TRUE)
			PRINTLN("[RCC MISSION] PROCESS_VEHICLE_EXPLOSION_LOGIC_STAGGERED - tanker health <= 100, exploding w NETWORK_EXPLODE_VEHICLE: ",sVehState.iIndex)
		ENDIF
	ENDIF	
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitSet, ciFMMC_VEHICLE_EASYEXPLODER)
		IF GET_VEHICLE_ENGINE_HEALTH(sVehState.vehIndex) <= -3650
			PRINTLN("[RCC MISSION] PROCESS_VEHICLE_EXPLOSION_LOGIC_STAGGERED - Calling NETWORK_EXPLODE_VEHICLE on veh ",sVehState.iIndex," as it's an easy exploder and engine health <= -3650")
			NETWORK_EXPLODE_VEHICLE(sVehState.vehIndex, DEFAULT, TRUE)
		ELSE
			FLOAT fEngineHealth = GET_VEHICLE_ENGINE_HEALTH(sVehState.vehIndex)
			IF fEngineHealth > -3500
				IF (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].fEngineHealth = 1001.0) //Default values
					IF fEngineHealth <= 100
						SET_VEHICLE_ENGINE_HEALTH(sVehState.vehIndex,-3500) //Set it on fire
						PRINTLN("[RCC MISSION] PROCESS_VEHICLE_EXPLOSION_LOGIC_STAGGERED - Vehicle ",sVehState.iIndex," is an easy exploder and taken enough engine damage 1, set engine health to -3500 (set on fire)")
					ENDIF
				ELSE
					//Need to do a fifth of the engine health in damage (engine health is from -1000 to +1000)
					IF fEngineHealth <= (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].fEngineHealth - ((g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].fEngineHealth + 1000) / 5))
						SET_VEHICLE_ENGINE_HEALTH(sVehState.vehIndex, -3500) //Set it on fire
						PRINTLN("[RCC MISSION] PROCESS_VEHICLE_EXPLOSION_LOGIC_STAGGERED - Vehicle ",sVehState.iIndex," is an easy exploder and taken enough engine damage 2, set engine health to -3500 (set on fire)")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck3, LBOOL3_EXPLOSIVE_ZONE_ACTIVE)
	AND NOT IS_BIT_SET(iVehDamageTrackerBS, sVehState.iIndex)
				
		NETWORK_INDEX niVeh = VEH_TO_NET(sVehState.vehIndex)
		
		IF NOT IS_DAMAGE_TRACKER_ACTIVE_ON_NETWORK_ID(niVeh)
			ACTIVATE_DAMAGE_TRACKER_ON_NETWORK_ID(niVeh, TRUE)
		ENDIF
		
		PRINTLN("[RCC MISSION] PROCESS_VEHICLE_EXPLOSION_LOGIC_STAGGERED - ACTIVATE_DAMAGE_TRACKER_ON_NETWORK_ID enabled on sVehState.iIndex: ", sVehState.iIndex)
		SET_BIT(iVehDamageTrackerBS, sVehState.iIndex)
	ENDIF
	
	PROCESS_REMOTE_VEHICLE_EXPLOSIVE(sVehState)
	
ENDPROC

PROC PROCESS_VEHICLE_RAMP_AND_RAMMING_DAMAGE_IMMUNITY(FMMC_VEHICLE_STATE &sVehState)
	IF sVehState.mn = DUNE4
	OR sVehState.mn = DUNE5
	OR sVehState.mn = PHANTOM2
	OR sVehState.mn = BOXVILLE5
		PRINTLN("[PROCESS_VEH_BODY] PROCESS_VEHICLE_RAMP_AND_RAMMING_DAMAGE_IMMUNITY - Setting VEHICLE_SET_RAMP_AND_RAMMING_CARS_TAKE_DAMAGE FALSE ... sVehState.iIndex = ", sVehState.iIndex, " ... Model = ", FMMC_GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].mn))
		VEHICLE_SET_RAMP_AND_RAMMING_CARS_TAKE_DAMAGE(sVehState.vehIndex, FALSE)
	ENDIF
ENDPROC

PROC PROCESS_VEHICLE_PHYSICS(FMMC_VEHICLE_STATE &sVehState)
	IF NETWORK_HAS_ENTITY_BEEN_REGISTERED_WITH_THIS_THREAD(sVehState.vehIndex)
		
		PED_INDEX vehiclePed
		
		IF IS_THIS_MODEL_A_HELI(sVehState.mn)
		OR IS_THIS_MODEL_A_PLANE(sVehState.mn)
			IF NOT IS_ANY_PLAYER_IN_VEHICLE(sVehState.vehIndex)
				IF IS_ENTITY_IN_AIR(sVehState.vehIndex) AND IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_EVERYONE_RUNNING)
					PRINTLN("[MMacK][HeliFall] Entity is airborne and we are running according to server")
					IF NOT IS_FMMC_VEHICLE_DRIVERS_SEAT_FREE(sVehState)
						vehiclePed = GET_PED_IN_VEHICLE_SEAT(sVehState.vehIndex)
						IF IS_ENTITY_DEAD(vehiclePed)
							SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(sVehState.vehIndex, FALSE)
							PRINTLN("[MMacK][HeliFall] FALSE A")
						ENDIF
					ELSE
						SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(sVehState.vehIndex, FALSE)
						PRINTLN("[MMacK][HeliFall] FALSE B")
					ENDIF
				ELSE
					IF NOT IS_FMMC_VEHICLE_DRIVERS_SEAT_FREE(sVehState)
						vehiclePed = GET_PED_IN_VEHICLE_SEAT(sVehState.vehIndex)
						IF IS_ENTITY_DEAD(vehiclePed)
							SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(sVehState.vehIndex, TRUE)
							PRINTLN("[MMacK][HeliFall] TRUE A")
						ENDIF
					ELSE
						SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(sVehState.vehIndex, TRUE)
						PRINTLN("[MMacK][HeliFall] TRUE B")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_THIS_MODEL_A_BOAT(sVehState.mn)
	OR sVehState.mn = SUBMERSIBLE
		IF NOT IS_ENTITY_WAITING_FOR_WORLD_COLLISION(sVehState.vehIndex)
			IF IS_VEHICLE_EMPTY(sVehState.vehIndex, TRUE)
				ACTIVATE_PHYSICS(sVehState.vehIndex)
			ENDIF
		ENDIF
	ENDIF
	
	//Use kinematic physics to push against any obstacles - url:bugstar:7305594 
	IF MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iKinematicPhysicsTeam] < FMMC_MAX_RULES
	AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iKinematicPhysicsBS, MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iKinematicPhysicsTeam])
	AND sVehState.bAlive AND sVehState.bDrivable
	AND IS_VEHICLE_ALMOST_STOPPED(sVehState.vehIndex)
	AND IS_VEHICLE_ON_ALL_WHEELS(sVehState.vehIndex)
	AND NOT IS_VEHICLE_STOPPED_AT_TRAFFIC_LIGHTS(sVehState.vehIndex)		
		SET_USE_KINEMATIC_PHYSICS(sVehState.vehIndex, TRUE)
		PRINTLN("PROCESS_VEHICLE_PHYSICS - VEH_", sVehState.iIndex," will use kinematic physics this frame")
	ENDIF
ENDPROC

PROC PROCESS_VEHICLE_LOCKON(FMMC_VEHICLE_STATE &sVehState)
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetFour, ciFMMC_VEHICLE4_VEHICLE_IMMUNE_HOMING_LAUNCHER)
	AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iRemoveHomingLockRule != -1
	AND MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iRemoveHomingLockRule
		PRINTLN("[RCC MISSION] PROCESS_VEH_BODY - Allowing homing missile lock from vehicle: ", sVehState.iIndex)
		SET_VEHICLE_ALLOW_HOMING_MISSLE_LOCKON_SYNCED(sVehState.vehIndex, TRUE)
	ENDIF
ENDPROC

PROC PROCESS_RESTART_WITH_TRAILER_ATTACHED(FMMC_VEHICLE_STATE &sVehState)
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetFive, ciFMMC_VEHICLE5_RESTART_WITH_TRAILER)
		
		INT iPrevTrailer = g_TransitionSessionNonResetVars.iAttachedVehicleTrailers[sVehState.iIndex]
		VEHICLE_INDEX viTrailer
		
		IF GET_VEHICLE_TRAILER_VEHICLE(sVehState.vehIndex, viTrailer)
			NETWORK_INDEX niTrailerID
			IF iPrevTrailer != -1
				niTrailerID = MC_serverBD_1.sFMMC_SBD.niVehicle[iPrevTrailer]
			ENDIF
			
			IF iPrevTrailer = -1
			OR (NETWORK_DOES_NETWORK_ID_EXIST(niTrailerID)
			AND IS_VEHICLE_DRIVEABLE(NET_TO_VEH(niTrailerID))
			AND NET_TO_VEH(niTrailerID) != viTrailer)
				PRINTLN("[RCC MISSION] PROCESS_VEH_BODY - RESPAWN_WITH_TRAILER - (not trailer) Vehicle number: ", sVehState.iIndex)
				PRINTLN("[RCC MISSION] PROCESS_VEH_BODY - RESPAWN_WITH_TRAILER - New trailer was detected, ent id: ", NATIVE_TO_INT(viTrailer), ", old value: ", iPrevTrailer)
				g_TransitionSessionNonResetVars.iAttachedVehicleTrailers[sVehState.iIndex] = -1
				INT i
				FOR i = 0 TO (FMMC_MAX_VEHICLES-1)
					IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[i])
					AND NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i]) = viTrailer
						PRINTLN("[RCC MISSION] PROCESS_VEH_BODY - RESPAWN_WITH_TRAILER - New trailer saved, vehicle number ", i)
						g_TransitionSessionNonResetVars.iAttachedVehicleTrailers[sVehState.iIndex] = i
						BREAKLOOP
					ENDIF
				ENDFOR
			ENDIF
		ELSE
			IF iPrevTrailer != 0
				PRINTLN("[RCC MISSION] PROCESS_VEH_BODY - RESPAWN_WITH_TRAILER - Trailer was detached, saving, old value: ", iPrevTrailer)
				g_TransitionSessionNonResetVars.iAttachedVehicleTrailers[sVehState.iIndex] = -1
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_VEHICLE_CRATE_STAGGERED(FMMC_VEHICLE_STATE &sVehState)
	IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicleCrate[sVehState.iIndex])
		IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicleCrate[sVehState.iIndex])
			OBJECT_INDEX oiCrate = NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niVehicleCrate[sVehState.iIndex])
			IF NOT IS_ENTITY_VISIBLE(oiCrate)
				SET_ENTITY_VISIBLE(oiCrate, TRUE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SET_LAST_VEHICLE_DATA_FOR_LOCAL_PLAYER(FMMC_VEHICLE_STATE &sVehState)
	IF iLastUsedPlacedVeh != sVehState.iIndex
		PRINTLN("[RCC MISSION][JS] SET_LAST_VEHICLE_DATA_FOR_LOCAL_PLAYER - Setting last used placed vehicle as ", sVehState.iIndex)
		iLastUsedPlacedVeh = sVehState.iIndex
	ENDIF
	
	IF MC_playerBD[iLocalPart].iTeam > -1
	AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iRespawnInLastMissionVehicleFromBS, sVehState.iIndex)
	AND iLastOccupiedRespawnListVehicle != sVehState.iIndex
		PRINTLN("[RCC MISSION] SET_LAST_VEHICLE_DATA_FOR_LOCAL_PLAYER - Setting iLastOccupiedRespawnListVehicle as vehicle ",sVehState.iIndex)
		iLastOccupiedRespawnListVehicle = sVehState.iIndex
	ENDIF
	
	IF MC_playerBD[iLocalPart].iTeam > -1
	AND MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iTeam] < FMMC_MAX_RULES
	AND g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iRespawnInMissionVehicle[MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iTeam]] = sVehState.iIndex
		vsLastOccupiedVehicleSeat = GET_SEAT_PED_IS_IN(LocalPlayerPed)
		PRINTLN("[RCC MISSION] SET_LAST_VEHICLE_DATA_FOR_LOCAL_PLAYER - Setting vsLastOccupiedVehicleSeat as ",ENUM_TO_INT(vsLastOccupiedVehicleSeat))
	ENDIF
ENDPROC

PROC PROCESS_LECTRO_HELP_TEXT(FMMC_VEHICLE_STATE &sVehState)
	IF NOT g_EnableKersHelp
	AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].mn = LECTRO
		IF HAS_NET_TIMER_STARTED(MC_serverBD.tdMissionLengthTimer) 
		AND GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdMissionLengthTimer) > 30000 //url:bugstar:4867662
			PRINTLN("[RCC MISSION] PROCESS_LECTRO_HELP_TEXT - Vehicle is LECTRO (KERS). Enabling Kers help text")
			g_EnableKersHelp = TRUE
		ELSE
			g_EnableKersHelp = FALSE
			PRINTLN("[RCC MISSION] PROCESS_LECTRO_HELP_TEXT - Vehicle is LECTRO (KERS). Disabling Kers help text")
		ENDIF
	ENDIF
ENDPROC
PROC PROCESS_OCCUPANT_EXPLOSION_PROOFING(FMMC_VEHICLE_STATE &sVehState)
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetFive, ciFMMC_VEHICLE5_OCCUPANTS_EXPLOSION_PROOF)
	AND NOT IS_BIT_SET(iLocalBoolCheck23, LBOOL23_VEHICLE_SET_EXPLOSION_PROOF)
		PRINTLN("[JS] PROCESS_OCCUPANT_EXPLOSION_PROOFING - Making player explosion proof as in vehicle ", sVehState.iIndex)
		RUN_LOCAL_PLAYER_PROOF_SETTING(TRUE, TRUE)
		SET_BIT(iLocalBoolCheck23, LBOOL23_VEHICLE_SET_EXPLOSION_PROOF)
	ENDIF
ENDPROC

PROC PROCESS_REMOVING_VEHICLE_MADE_INVINCIBLE(FMMC_VEHICLE_STATE &sVehState)
	IF NOT IS_BIT_SET(MC_serverBD_4.iVehMadeInvincibleBS, sVehState.iIndex)
		EXIT
	ENDIF
	
	IF sVehState.bHasControl
		SET_ENTITY_INVINCIBLE(sVehState.vehIndex, FALSE)
		SET_VEHICLE_ALLOW_HOMING_MISSLE_LOCKON(sVehState.vehIndex, TRUE)
		BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_ToggleVehicleInvincibleByFlee, sVehState.iIndex, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, FALSE)
		PRINTLN("PROCESS_REMOVING_VEHICLE_MADE_INVINCIBLE - Removing invincibility from vehicle ", sVehState.iIndex)
	ENDIF
	
ENDPROC

PROC PROCESS_VEHICLE_PREREQS(FMMC_VEHICLE_STATE &sVehState)

	IF IS_BIT_SET(iEnterVehiclePreReqsAppliedBS, sVehState.iIndex)
		EXIT
	ENDIF
	
	IF IS_LONG_BITSET_EMPTY(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iSetPrereqsOnEnterBS)
		EXIT
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetTen, ciFMMC_VEHICLE10_WaitForEngineBeforePreReqs)
	AND NOT GET_IS_VEHICLE_ENGINE_RUNNING(sVehState.vehIndex)
		EXIT
	ENDIF
	
	PRINTLN("[Vehicles][Vehicle ", sVehState.iIndex, "] PROCESS_VEHICLE_PREREQS - Applying PreReqs on entry")
	SET_PREREQUISITES_COMPLETED_FROM_LONG_BITSET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iSetPrereqsOnEnterBS)
	SET_BIT(iEnterVehiclePreReqsAppliedBS, sVehState.iIndex)
	
ENDPROC 

#IF IS_DEBUG_BUILD
PROC PROCESS_VEH_PROXIMITY_SPOOFING(INT iVeh)
	
	INT i
	FOR i = 0 TO ciProxSpawnDebug_MAX_PLAYERS - 1
		
		IF bProxSpawnDebug_PlayerOverrideUpdate[i]
			vProxSpawnDebug_PlayerOverride[i] = vLocalPlayerPosition
		ENDIF
		
		PROCESS_CLIENT_PROXIMITY_SPAWNING(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sProximitySpawningData, MC_GET_VEH_SPAWN_LOCATION_FOR_PROXIMITY_SPAWNING(iVeh), iVeh, sProxSpawnDebug_PlayerOverrideData[i].iProximitySpawning_VehSpawnRangeBS, sProxSpawnDebug_PlayerOverrideData[i].iProximitySpawning_VehCleanupRangeBS, vProxSpawnDebug_PlayerOverride[i] #IF IS_DEBUG_BUILD , "Veh" #ENDIF )
	
	ENDFOR
	
ENDPROC
#ENDIF

PROC PROCESS_VEHICLE_SEARCHLIGHT_ACTIVATION(FMMC_VEHICLE_STATE &sVehicleState)

	IF NOT sVehicleState.bAlive
		EXIT
	ENDIF
	
	IF NOT IS_THIS_MODEL_A_HELI(sVehicleState.mn)	
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehicleState.iIndex].iVehBitsetThree, ciFMMC_VEHICLE3_UseSearchLightInDark)
		EXIT
	ENDIF

	IF NOT DOES_VEHICLE_HAVE_WEAPONS(sVehicleState.vehIndex)
		PRINTLN("[Vehicles][Vehicle ", sVehicleState.iIndex, "] - PROCESS_VEHICLE_SEARCHLIGHT_ACTIVATION - [ST_DEBUG] Vehicle does not have a searchlight (NOT DOES_VEHICLE_HAVE_WEAPONS).")
		EXIT
	ENDIF
		
	IF IS_VEHICLE_SEARCHLIGHT_ON(sVehicleState.vehIndex)
		EXIT
	ENDIF
	
	IF NOT SHOULD_VEHICLE_HAVE_SEARCHLIGHT_ON()
		PRINTLN("[Vehicles][Vehicle ", sVehicleState.iIndex, "] - PROCESS_VEHICLE_SEARCHLIGHT_ACTIVATION - [ST_DEBUG] MC_SET_UP_VEH - SEARCHLIGHT Should not be on yet. (NOT SHOULD_VEHICLE_HAVE_SEARCHLIGHT_ON)")
		EXIT
	ENDIF
	
	PRINTLN("[Vehicles][Vehicle ", sVehicleState.iIndex, "] - PROCESS_VEHICLE_SEARCHLIGHT_ACTIVATION - [ST_DEBUG] MC_SET_UP_VEH - SET_VEHICLE_SEARCHLIGHT(TRUE)")
	SET_VEHICLE_SEARCHLIGHT(sVehicleState.vehIndex, TRUE, TRUE)
	
ENDPROC

PROC PROCESS_VEH_BODY(FMMC_VEHICLE_STATE &sVehState)
	
	#IF IS_DEBUG_BUILD
	IF bProxSpawnDebug_EnablePlayerPositionSpoofing
		PROCESS_VEH_PROXIMITY_SPOOFING(sVehState.iIndex)
	ELSE
	#ENDIF
		PROCESS_CLIENT_PROXIMITY_SPAWNING(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].sProximitySpawningData, MC_GET_VEH_SPAWN_LOCATION_FOR_PROXIMITY_SPAWNING(sVehState.iIndex), sVehState.iIndex, MC_playerBD_1[iLocalPart].iProximitySpawning_VehSpawnRangeBS, MC_playerBD_1[iLocalPart].iProximitySpawning_VehCleanupRangeBS, vLocalPlayerPosition #IF IS_DEBUG_BUILD , "Veh" #ENDIF )
	#IF IS_DEBUG_BUILD
	ENDIF
	#ENDIF
	
	PROCESS_CLIENT_VEHICLE_OBJECTIVES_OLD(sVehState.iIndex)
	
	IF sVehState.bExists
		
		IF NOT sVehState.bDrivable
			PROCESS_VEH_BODY_DRIVABLE_CLEANUP(sVehState)			
			EXIT
		ELSE
			
			IF IS_LOCAL_PLAYER_IN_FMMC_VEHICLE(sVehState)
				iPlacedVehicleIAmIn = sVehState.iIndex
				
				PROCESS_VEHICLE_MINES(sVehState)
				
				PROCESS_BLOCK_MANUAL_RESPAWN_IN_VEHICLE(sVehState)
				
				SET_LAST_VEHICLE_DATA_FOR_LOCAL_PLAYER(sVehState)
				
				PROCESS_OCCUPANT_EXPLOSION_PROOFING(sVehState)
				
				PROCESS_LECTRO_HELP_TEXT(sVehState)
				
				PROCESS_REMOVING_VEHICLE_MADE_INVINCIBLE(sVehState)
				
				PROCESS_VEHICLE_PREREQS(sVehState)

			ENDIF
			
			PROCESS_PLAYER_INVINCIBLE_IN_VEHICLE(sVehState)
			
			PROCESS_OFF_RULE_QUICK_GPS_FOR_VEHICLE(sVehState)
			
			PROCESS_COLLISION_DAMAGE_IMMUNITY_WITH_OTHER_PLACED_VEHICLES(sVehState)
			
			PROCESS_SEAT_PREFERENCE_OVERRIDE(sVehState)
			
			PROCESS_VEHICLE_ON_RULE_WARP(sVehState)
			
			PROCESS_VEHICLE_ON_DAMAGE_WARP(sVehState)
			
			IF sVehState.bHasControl
				
				PROCESS_DOOR_STAGGERED_SETTINGS(sVehState)
				
				PROCESS_DELUXO_FLIGHT_MODE(sVehState)
				
				PROCESS_CLIENT_BURNING_VEHICLE_WITH_CONTROL(sVehState)
				
				PROCESS_EXTINGUISH_VEHICLE_FIRE(sVehState)
								
				PROCESS_VEHICLE_INVINCIBLE_WHEN_EMPTY(sVehState)
				
				PROCESS_DISABLING_VEHICLE_HOMING_ROCKETS(sVehState)
				
				PROCESS_HELI_ROTOR_DAMAGE(sVehState)
				
				PROCESS_KEEP_VEHICLE_IN_BOUNDS(sVehState)
				
				PROCESS_AVENGER_INTERIOR_ENTRY(sVehState)
				
				PROCESS_VEHICLE_EXPLOSION_LOGIC_STAGGERED(sVehState)

				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetSix, ciFMMC_VEHICLE6_FORCE_COLLISION_LOADING)
					LOAD_COLLISION_ON_VEH_IF_POSSIBLE(sVehState.vehIndex, sVehState.iIndex, TRUE)
				ENDIF
				
				PROCESS_VEHICLE_RAMP_AND_RAMMING_DAMAGE_IMMUNITY(sVehState)
				
				CHECK_VEHICLE_STUCK_TIMERS(sVehState.vehIndex, sVehState.iIndex, TRUE)
				
				PROCESS_VEHICLE_PHYSICS(sVehState)
				
				PROCESS_VEHICLE_LOCKON(sVehState)
				
				PROCESS_VEHICLE_IMMOVABLE(sVehState)
				
				PROCESS_VEHICLE_DISABLE_VEHICLE_ACTIVE_FOR_PED_NAVIGATION(sVehState)
				
				PROCESS_VEHICLE_SEARCHLIGHT_ACTIVATION(sVehState)
			ENDIF
						
			PROCESS_BOMB_BAY_DOORS_HELP_TEXT(sVehState)
			
			PROCESS_OBJECTIVE_VEHICLE_ATTACHED_TO_CARGOBOB(sVehState)
			
			PROCESS_DUNE_RAMP_IMPULSE(sVehState)
			
			PROCESS_INSURGENT_HELP_TEXT(sVehState)
			
			PROCESS_VEHICLE_HORN_OVERRIDE(sVehState)
			
			PROCESS_RESTART_WITH_TRAILER_ATTACHED(sVehState)
			
			PROCESS_ENTITY_INTERIOR(sVehState.iIndex, sVehState.vehIndex, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehInterior != -1 OR g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].sWarpLocationSettings.iWarpLocationBS != 0, <<0,0,0>>, iVehicleHasBeenPlacedIntoInteriorBS, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehInteriorRoomKey)
			
			PROCESS_VEHICLE_HEALTH_THRESHOLD(sVehState)
			
		ENDIF
	ELSE
	
		PROCESS_VEH_BODY_CLEANUP(sVehState)
		
		EXIT
	ENDIF
	
	EXT2_CREATE_CLOUD_PTFX(sVehState)

	MAINTAIN_VEH_COLLISION_CLEANUP(sVehState)
	
	PROCESS_TITAN_PTFX(sVehState)

	// We need to do some creation functionality when the vehicle has loaded and has collision / has migrated.
	IF iStaggeredVehLoopCounterPostCreationCalls >= FMMC_MAX_VEHICLES-1
	OR iStaggeredVehLoopCounterPostCreationCalls >= MC_serverBD.iNumVehCreated-1
		IF NOT IS_BIT_SET(iLocalBoolCheck26, LBOOL26_FIRST_STAGGERED_LOOP_OF_VEH_BODY)
			PRINTLN("[RCC MISSION] PROCESS_VEH_BODY - Setting LBOOL26_FIRST_STAGGERED_LOOP_OF_VEH_BODY")
			SET_BIT(iLocalBoolCheck26, LBOOL26_FIRST_STAGGERED_LOOP_OF_VEH_BODY)			
		ENDIF	
	ELSE
		iStaggeredVehLoopCounterPostCreationCalls++
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Every-Frame Vehicle Functions ----------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Vehicle processing called in the every-frame loop.  --------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_VEHICLE_HEALTHBAR(FMMC_VEHICLE_STATE& sVehState)
	
	IF NOT sVehState.bExists
	OR NOT sVehState.bAlive
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetTwo, ciFMMC_VEHICLE2_ENABLE_HEALTH_BAR)
		EXIT
	ENDIF
	
	ENTITY_HEALTHBAR_VARS sEntityHealthbarVars
	sEntityHealthbarVars.iEntityIndex = sVehState.iIndex
	sEntityHealthbarVars.iEntityType = ciENTITY_TYPE_VEHICLE
	sEntityHealthbarVars.eiEntity = sVehState.vehIndex
	
	sEntityHealthbarVars.iBlipName = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].sVehBlipStruct.iBlipNameIndex
	
	sEntityHealthbarVars.iCustomTitleCustomStringIndex = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iHealthBarCustomTitleStringIndex
	
	sEntityHealthbarVars.vEntityCoords = GET_FMMC_VEHICLE_COORDS(sVehState)
	sEntityHealthbarVars.iDisplayInRange = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iHealthBarInRange
	
	sEntityHealthbarVars.iOnlyDisplayOnRule = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iHealthBarRule
	
	sEntityHealthbarVars.ePercentageMeterLine = INT_TO_ENUM(PERCENTAGE_METER_LINE, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iHealthBarPercentageLine)
	sEntityHealthbarVars.iPretendZeroIs = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iHealthBarPretendZeroIs
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetNine, ciFMMC_VEHICLE9_HideHealthbarBelowSetPercentageBar)
		SET_BIT(sEntityHealthbarVars.iEntityHealthbarConfigBS, ciEntityHealthbarConfigBS_HideUnderPercentageLine)
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetSeven, ciFMMC_VEHICLE7_HIDE_PLAYER_NAME_ON_HEALTHBAR)
		sEntityHealthbarVars.piDriver = GET_PED_IN_VEHICLE_SEAT(sVehState.vehIndex)
	ENDIF
	
	INT iTeamToUse = GET_TEAM_TO_USE_FOR_ENTITY_HEALTHBAR(sEntityHealthbarVars, GET_LOCAL_PLAYER_TEAM(TRUE))
	sEntityHealthbarVars.tlHealthbarTitle = GET_ENTITY_HEALTHBAR_TITLE(sEntityHealthbarVars, iTeamToUse, sEntityHealthbarVars.bIsPlayerName, sEntityHealthbarVars.bIsLiteralString)
	sEntityHealthbarVars.iPercentageToDisplay = GET_ENTITY_HEALTHBAR_PERCENTAGE_TO_DISPLAY(sEntityHealthbarVars)
	
	PROCESS_ENTITY_HEALTHBAR_TURN_RED_AT_PERCENT(sEntityHealthbarVars, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sEntityHealthbarVars.iEntityIndex].iHealthBarTurnRedAtPercent)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetNine, ciFMMC_VEHICLE9_InvertHealthbar)
		SET_BIT(sEntityHealthbarVars.iEntityHealthbarConfigBS, ciEntityHealthbarConfigBS_InvertPercentage)
	ENDIF

	STORE_ENTITY_HEALTHBAR_STRUCT(sEntityHealthbarVars, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetNine, ciFMMC_VEHICLE9_ExtraHealthbarIncreaseChecks))
ENDPROC

PROC PROCESS_FORCE_HELI_BLADES_AT_FULL_SPEED(FMMC_VEHICLE_STATE &sVehState)
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetFour, ciFMMC_VEHICLE4_VEHICLE_FREEZE_POS)
	AND IS_THIS_MODEL_A_PLANE(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].mn)
	AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetThree, ciFMMC_VEHICLE3_VehicleStartsAirborne)
	AND sVehState.bHasControl
		SET_HELI_BLADES_FULL_SPEED(sVehState.vehIndex)
	ENDIF
ENDPROC

//[FMMC2020] Definitely belongs somewhere else
PROC SET_UP_HIGHER_JUMP_CAR()
	INT i
	IF NOT IS_BIT_SET(iLocalBoolCheck21, LBOOL21_SET_CAR_HIGHER_JUMP)
		IF MC_serverBD.iNumVehCreated > 0
		AND MC_serverBD.iNumVehCreated <= FMMC_MAX_VEHICLES
			FOR i = 0 TO (MC_serverBD.iNumVehCreated-1)
				IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[i])
					VEHICLE_INDEX ThisVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i])
					IF IS_VEHICLE_DRIVEABLE(ThisVeh)
						PRINTLN("[RCC MISSION] DISPLAY_VEHICLE_HUD - vehicle: ", i," ciFMMC_VEHICLE5_INCREASE_JUMP_HEIGHT - check: ", IS_BIT_SET(iLocalBoolCheck21, LBOOL21_SET_CAR_HIGHER_JUMP))
						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehBitsetFive, ciFMMC_VEHICLE5_INCREASE_JUMP_HEIGHT)
							PRINTLN("[RCC MISSION] DISPLAY_VEHICLE_HUD - vehicle: ", i," ciFMMC_VEHICLE5_INCREASE_JUMP_HEIGHT - SET")
							
							IF GET_CAR_HAS_JUMP(ThisVeh)		
								PRINTLN("[RCC MISSION] DISPLAY_VEHICLE_HUD - vehicle: ", i," Is a jump car, calling: SET_USE_HIGHER_CAR_JUMP")
								SET_USE_HIGHER_CAR_JUMP(ThisVeh, TRUE)
								
							ELSE
								PRINTLN("[RCC MISSION] DISPLAY_VEHICLE_HUD - vehicle: ", i," Is not a jump car, this is weird to set this option for this vehicle.")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
			SET_BIT(iLocalBoolCheck21, LBOOL21_SET_CAR_HIGHER_JUMP)
		ENDIF		
	ENDIF
ENDPROC

PROC PROCESS_VEHICLE_WEAPON_EXPLODE_ON_ZERO_HEALTH()
	
	VEHICLE_INDEX viPlayerVeh
	
	IF NOT bIsAnySpectator
		IF IS_PED_IN_ANY_VEHICLE(PlayerPedToUse)
			viPlayerVeh = GET_VEHICLE_PED_IS_IN(PlayerPedToUse)
			
			IF IS_ENTITY_ALIVE(viPlayerVeh)
				//Lets stop the engine from causing the player to blow up
				IF GET_VEHICLE_ENGINE_HEALTH(viPlayerVeh) < 1000
				AND GET_VEHICLE_ENGINE_HEALTH(viPlayerVeh) > -1000.0
					IF NETWORK_HAS_CONTROL_OF_ENTITY(viPlayerVeh)
						SET_VEHICLE_ENGINE_HEALTH(viPlayerVeh, 1000)			
					ELSE
						NETWORK_REQUEST_CONTROL_OF_ENTITY(viPlayerVeh)
					ENDIF
				ENDIF
				
				IF IS_ENTITY_A_MISSION_ENTITY(viPlayerVeh)
				AND NETWORK_HAS_CONTROL_OF_ENTITY(viPlayerVeh)
					SET_DISABLE_VEHICLE_PETROL_TANK_DAMAGE(viPlayerVeh, TRUE)
				ENDIF
				
				IF NETWORK_HAS_CONTROL_OF_ENTITY(viPlayerVeh)
					FLOAT fHealth = TO_FLOAT(GET_ENTITY_HEALTH(viPlayerVeh))
					
					IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iTeamBitSet3, ciBS3_TEAM_VEHICLE_USE_ONLY_HEALTH_BODY)
						fHealth = GET_VEHICLE_BODY_HEALTH(viPlayerVeh)
						PRINTLN("[KH][SMOKE] PROCESS_VEHICLE_WEAPON_EXPLODE_ON_ZERO_HEALTH - Using Body Health ",fHealth)
					ENDIF
					
					IF fHealth <= 0.0
					OR NOT IS_VEHICLE_DRIVEABLE(viPlayerVeh)
						IF iPartToShootPlayerLast != -1
							PRINTLN("PROCESS_VEHICLE_WEAPON_EXPLODE_ON_ZERO_HEALTH - Using Body iPartToShootPlayerLast ", iPartToShootPlayerLast)
							
							SET_KILL_STRIP_OVER_RIDE_PLAYER_ID(NETWORK_GET_PLAYER_INDEX(INT_TO_NATIVE(PARTICIPANT_INDEX, iPartToShootPlayerLast)))
							NETWORK_EXPLODE_VEHICLE(viPlayerVeh, DEFAULT, FALSE, iPartToShootPlayerLast)
							iPartToShootPlayerLast = -1
						ELSE
							PRINTLN("PROCESS_VEHICLE_WEAPON_EXPLODE_ON_ZERO_HEALTH - Not Using Body iPartToShootPlayerLast")
							NETWORK_EXPLODE_VEHICLE(viPlayerVeh, DEFAULT, TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL CAN_PLACED_VEHICLE_BE_TARGETTED_BY_ORBITAL_CANNON(VEHICLE_INDEX vehIndex, INT iVeh)
	
	IF IS_ENTITY_A_GHOST(vehIndex)	
	OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetSeven, ciFMMC_VEHICLE7_HIDE_MARKER_FROM_ORBITAL_CANNON)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
//    Handles drawing boxes around players in session, and arrows for players offscreen
PROC DRAW_PLACED_VEHICLE_TRACKERS()

	ASSERTLN("[FMMC2020] DRAW_PLACED_VEHICLE_TRACKERS | Needs refactoring to make it so this is just called within the every frame vehicle loop")
	VEHICLE_INDEX vehIndex
	
	INT i
	REPEAT FMMC_MAX_VEHICLES i
		IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[i])
			vehIndex = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i])
			
			IF DOES_ENTITY_EXIST(vehIndex)
			AND IS_VEHICLE_DRIVEABLE(vehIndex)
				IF CAN_PLACED_VEHICLE_BE_TARGETTED_BY_ORBITAL_CANNON(vehIndex, i)
					IF HAS_STREAMED_TEXTURE_DICT_LOADED("helicopterhud")
						DRAW_BOX_AROUND_TARGET_VEHICLE(vehIndex, IS_BLIP_SPRITE_OVERRIDE_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].sVehBlipStruct))
						DRAW_OFFSCREEN_ARROW_FOR_COORD_VEHICLE(vehIndex)
						IF IS_BLIP_SPRITE_OVERRIDE_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].sVehBlipStruct)
							DRAW_TARGET_LETTER_FOR_COORD_VEHICLE(vehIndex, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].sVehBlipStruct.iBlipSpriteOverride)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC PROCESS_LOCK_VEHICLE_ON_RULE(FMMC_VEHICLE_STATE &sVehState)
	
	INT iLockRule = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBecomesLockedOnRule
	
	IF iLockRule = -1
	OR IS_BIT_SET(iVehicleHasBeenLockedBS, sVehState.iIndex)
		EXIT
	ENDIF
	
	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]
	
	IF iRule < FMMC_MAX_RULES
 		IF iRule = iLockRule
			PRINTLN("[PROCESS_LOCK_VEHICLE_ON_RULE] sVehState.iIndex: ", sVehState.iIndex, " Locking as we are now on rule: ", iRule, " - LockRule: ", iLockRule)
			BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedContentEventType_AllPlayersToggleConsiderVehicle, sVehState.iIndex, DEFAULT, DEFAULT, DEFAULT, DEFAULt, DEFAULT, FALSE)
			IF sVehState.bAlive
				SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(sVehState.vehIndex, FALSE)
				SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(sVehState.vehIndex, TRUE)
			ENDIF
			SET_BIT(iVehicleHasBeenLockedBS, sVehState.iIndex)
		ENDIF
	ENDIF

ENDPROC

PROC PROCESS_RESET_VEHICLE_WHEN_OOB(FMMC_VEHICLE_STATE &sVehState)
		
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetFour, ciFMMC_VEHICLE4_VEHICLE_RESET_IF_OOB)
		EXIT
	ENDIF
	
	IF IS_VEHICLE_OUT_OF_BOUNDS(sVehState.vehIndex, sVehState.iIndex)
	AND NOT IS_ANY_PLAYER_IN_VEHICLE(sVehState.vehIndex)	
		IF sVehState.bExists
			BOOL bIgnoreAreaCheck = FALSE
			VECTOR vValidSpawnPoint
			FLOAT fValidHeading
			VEHICLE_SPAWN_LOCATION_PARAMS spawnParams
			spawnParams.fMaxDistance = 10.0
			INT iSpawnInterior
			
			IF HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS(GET_VEHICLE_FMMC_SPAWN_LOCATION(sVehState.iIndex, bIgnoreAreaCheck, iSpawnInterior), <<0,0,0>>, sVehState.mn, FALSE, vValidSpawnPoint, fValidHeading, spawnParams)
				SET_ENTITY_COORDS(sVehState.vehIndex, vValidSpawnPoint)
				INT iNumPlayers = MC_serverBD.iNumberOfPlayingPlayers[0] + MC_serverBD.iNumberOfPlayingPlayers[1] + MC_serverBD.iNumberOfPlayingPlayers[2] + MC_serverBD.iNumberOfPlayingPlayers[3]
				MC_SET_UP_VEH(sVehState.vehIndex, sVehState.iIndex,MC_ServerBD_1.sFMMC_SBD.niVehicle, vValidSpawnPoint, FALSE, iSpawnInterior, DEFAULT, iNumPlayers)				
				iSpawnPoolVehIndex = -1
				PRINTLN("[JS] DISPLAY_VEHICLE_HUD IS_VEHICLE_OUT_OF_BOUNDS - VALID SPAWN POINT FOUND - VEHICLE RESPAWNED")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_VEHICLE_ANCHOR_WHILE_STATIONARY(FMMC_VEHICLE_STATE &sVehState)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetFour, ciFMMC_VEHICLE4_VEHICLE_ANCHORS_WHEN_EMPTY)
		EXIT
	ENDIF
	
	MODEL_NAMES vehModel = sVehState.mn
	
	IF IS_THIS_MODEL_A_BOAT(vehModel)
	OR IS_THIS_MODEL_A_JETSKI(vehModel)
	OR IS_THIS_MODEL_AN_AMPHIBIOUS_CAR(vehModel)
	OR IS_THIS_MODEL_AN_AMPHIBIOUS_QUADBIKE(vehModel)
	OR vehModel = SUBMERSIBLE 
	OR vehModel = SUBMERSIBLE2
	OR vehModel = DODO
	OR vehModel = TULA
		IF IS_ENTITY_IN_WATER(sVehState.vehIndex)
			IF IS_FMMC_VEHICLE_DRIVERS_SEAT_FREE(sVehState)
				IF CAN_ANCHOR_BOAT_HERE(sVehState.vehIndex) // stops an assert..
					IF NOT IS_BOAT_ANCHORED(sVehState.vehIndex)
						PRINTLN("[LM][DISPLAY_VEHICLE_HUD] - ciFMMC_VEHICLE4_VEHICLE_ANCHORS_WHEN_EMPTY: Anchoring Vehicle: ", sVehState.iIndex)
						SET_BOAT_ANCHOR(sVehState.vehIndex, TRUE)
						IF vehModel != TULA
							SET_BOAT_REMAINS_ANCHORED_WHILE_PLAYER_IS_DRIVER(sVehState.vehIndex, TRUE)
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_VEH_ACCELERATE)
				OR IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_VEH_BRAKE)
					IF GET_SEAT_PED_IS_IN(localPlayerPed) = VS_DRIVER
						IF IS_BOAT_ANCHORED(sVehState.vehIndex)
							PRINTLN("[LM][DISPLAY_VEHICLE_HUD] - ciFMMC_VEHICLE4_VEHICLE_ANCHORS_WHEN_EMPTY: Releasing Anchor from Vehicle: ", sVehState.iIndex)
							SET_BOAT_ANCHOR(sVehState.vehIndex, FALSE)
							IF vehModel != TULA
								SET_BOAT_REMAINS_ANCHORED_WHILE_PLAYER_IS_DRIVER(sVehState.vehIndex, FALSE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_ENTITY_IN_AIR(sVehState.vehIndex)
		AND NOT IS_VEHICLE_IN_WATER(sVehState.vehIndex)
		OR (IS_VEHICLE_ON_ALL_WHEELS(sVehState.vehIndex)
		AND NOT IS_VEHICLE_IN_WATER(sVehState.vehIndex))
			IF IS_BOAT_ANCHORED(sVehState.vehIndex)
				PRINTLN("[LM][DISPLAY_VEHICLE_HUD] - ciFMMC_VEHICLE4_VEHICLE_ANCHORS_WHEN_EMPTY: Releasing Anchor from Vehicle (failsafe): ", sVehState.iIndex)
				SET_BOAT_ANCHOR(sVehState.vehIndex, FALSE)
				IF vehModel != TULA
					SET_BOAT_REMAINS_ANCHORED_WHILE_PLAYER_IS_DRIVER(sVehState.vehIndex, FALSE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_DISABLE_VEHICLE_ON_FRAME(FMMC_VEHICLE_STATE &sVehState)
	INT iVehDisableOnRule = g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iLocalPart].iteam].iVehDisableOnRule[MC_ServerBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam]]
	IF iVehDisableOnRule > -1
		PRINTLN("[LM][DISPLAY_VEHICLE_HUD] - iVehDisableOnRule: ", iVehDisableOnRule, " Vehicle: ", sVehState.iIndex, " Rule: ", MC_ServerBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam])
		IF iVehDisableOnRule = sVehState.iIndex
			IF bLocalPlayerPedOk
				IF IS_LOCAL_PLAYER_IN_FMMC_VEHICLE(sVehState)
					PRINTLN("[LM][DISPLAY_VEHICLE_HUD] - Local Player Ped is in the vehicle. Disable Movement Controls.")
					DISABLE_VEHICLE_MOVEMENT_CONTROLS_THIS_FRAME()
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF sVehState.mn = OPPRESSOR
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iLocalPart].iteam].iRuleBitsetNine[MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam]], ciBS_RULE9_DISABLE_OPPRESSOR_BOOST)
		OR IS_BIT_SET(iLocalBoolCheck24, LBOOL24_BLOCK_VEHICLE_KERS_ON_RULE_CHECK)
			SET_VEHICLE_KERS_ALLOWED(sVehState.vehIndex, FALSE)
		ELSE							
			SET_VEHICLE_KERS_ALLOWED(sVehState.vehIndex, TRUE)							
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_VEHICLE_COLLISION_SHOULD_EXPLODE(FMMC_VEHICLE_STATE &sVehState)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].fVelocityReqForExplosion > -1.0
	AND HAS_ENTITY_COLLIDED_WITH_ANYTHING(sVehState.vehIndex)
	AND sVehState.iIndex != g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iEntityIndexToCheck

		PRINTLN("[LM][PROCESS_VEHICLE_COLLISION_SHOULD_EXPLODE] - Collision Detected for iVehicle: ", sVehState.iIndex)	
		PRINTLN("[LM][PROCESS_VEHICLE_COLLISION_SHOULD_EXPLODE] - We will explode if we are touched by: ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iEntityIndexToCheck)
		PRINTLN("[LM][PROCESS_VEHICLE_COLLISION_SHOULD_EXPLODE] - At this set speed: ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].fVelocityReqForExplosion)
		
		VEHICLE_INDEX vehTarget		
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iEntityIndexToCheck > -1
			
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iEntityIndexToCheck])
				PRINTLN("[LM][PROCESS_VEHICLE_COLLISION_SHOULD_EXPLODE] - Assigning from iEntityIndexToCheck")
				vehTarget = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iEntityIndexToCheck])
			ENDIF
			
		ELSE
			
			IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(LocalPlayer)
			AND NOT IS_PLAYER_SPECTATOR_ONLY(LocalPlayer)
				PRINTLN("[LM][PROCESS_VEHICLE_COLLISION_SHOULD_EXPLODE] - Player Exists and is not a spectator")
			
				IF bLocalPlayerPedOK
					PRINTLN("[LM][PROCESS_VEHICLE_COLLISION_SHOULD_EXPLODE] - Player is not injured")
					
					IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed, FALSE)
						PRINTLN("[LM][PROCESS_VEHICLE_COLLISION_SHOULD_EXPLODE] - Player is in a Vehicle.")			
						vehTarget = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(vehTarget)	
			// This is needed for debugging only....
//			#IF IS_DEBUG_BUILD
//				PRINTLN("[LM][PROCESS_VEHICLE_COLLISION_SHOULD_EXPLODE] - Vehicle i: ", GET_VEHICLE_INT_FROM_INDEX(vehTarget))
//			#ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(vehTarget)
				IF IS_VEHICLE_TOUCHING_THIS_VEHICLE(vehTarget, sVehState.vehIndex)
					PRINTLN("[LM][PROCESS_VEHICLE_COLLISION_SHOULD_EXPLODE] - Player Vehicle and this vehicle is touching.")
					
					FLOAT fVelReq = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].fVelocityReqForExplosion
					FLOAT fVelIncoming = GET_ENTITY_SPEED(vehTarget)
					
					PRINTLN("[LM][PROCESS_VEHICLE_COLLISION_SHOULD_EXPLODE] - fVelReq: ", fVelReq)
					PRINTLN("[LM][PROCESS_VEHICLE_COLLISION_SHOULD_EXPLODE] - fVelIncoming: ", fVelIncoming)
					
					IF fVelIncoming > fVelReq
						PRINTLN("[LM][PROCESS_VEHICLE_COLLISION_SHOULD_EXPLODE] - Explode Vehicle")
						NETWORK_EXPLODE_VEHICLE(sVehState.vehIndex, TRUE, TRUE)
					ENDIF
				ENDIF
			ENDIF	
		ENDIF
	ENDIF
ENDPROC

PROC DISABLE_VEHICLE_WEAPONS(VEHICLE_INDEX vehIndex)
	MODEL_NAMES mnToCheck = GET_ENTITY_MODEL(vehIndex)
	
	INT iWeaponSlotBitset = 0
	
	//Copied from weapon_enums.sch - DLC Vehicle Weapons - Gunrunning
	SWITCH mnToCheck
		CASE APC
			iWeaponSlotBitset = BIT0 | BIT1 | BIT2 | BIT3
		
			DISABLE_VEHICLE_WEAPON(TRUE, WEAPONTYPE_DLC_VEHICLE_APC_CANNON, vehIndex, LocalPlayerPed)
			PRINTLN("[DISABLE_VEHICLE_WEAPONS] DISABLE_VEHICLE_WEAPON - WEAPONTYPE_DLC_VEHICLE_APC_CANNON")
			DISABLE_VEHICLE_WEAPON(TRUE, WEAPONTYPE_DLC_VEHICLE_APC_MISSILE, vehIndex, LocalPlayerPed)
			PRINTLN("[DISABLE_VEHICLE_WEAPONS] DISABLE_VEHICLE_WEAPON - WEAPONTYPE_DLC_VEHICLE_APC_MISSILE")
			DISABLE_VEHICLE_WEAPON(TRUE, WEAPONTYPE_DLC_VEHICLE_APC_MG, vehIndex, LocalPlayerPed)
			PRINTLN("[DISABLE_VEHICLE_WEAPONS] DISABLE_VEHICLE_WEAPON - WEAPONTYPE_DLC_VEHICLE_APC_MG")
		BREAK
		CASE ARDENT
			iWeaponSlotBitset = BIT0
			
			DISABLE_VEHICLE_WEAPON(TRUE, WEAPONTYPE_DLC_VEHICLE_ARDENT_MG, vehIndex, LocalPlayerPed)
			PRINTLN("[DISABLE_VEHICLE_WEAPONS] DISABLE_VEHICLE_WEAPON - WEAPONTYPE_DLC_VEHICLE_ARDENT_MG")
		BREAK
		CASE DUNE3
			iWeaponSlotBitset = BIT0 | BIT1 | BIT2
			
			DISABLE_VEHICLE_WEAPON(TRUE, WEAPONTYPE_DLC_VEHICLE_DUNE_MG, vehIndex, LocalPlayerPed)
			PRINTLN("[DISABLE_VEHICLE_WEAPONS] DISABLE_VEHICLE_WEAPON - WEAPONTYPE_DLC_VEHICLE_DUNE_MG")
			DISABLE_VEHICLE_WEAPON(TRUE, WEAPONTYPE_DLC_VEHICLE_DUNE_GRENADELAUNCHER, vehIndex, LocalPlayerPed)
			PRINTLN("[DISABLE_VEHICLE_WEAPONS] DISABLE_VEHICLE_WEAPON - WEAPONTYPE_DLC_VEHICLE_DUNE_GRENADELAUNCHER")
			DISABLE_VEHICLE_WEAPON(TRUE, WEAPONTYPE_DLC_VEHICLE_DUNE_MINIGUN, vehIndex, LocalPlayerPed)
			PRINTLN("[DISABLE_VEHICLE_WEAPONS] DISABLE_VEHICLE_WEAPON - WEAPONTYPE_DLC_VEHICLE_DUNE_MINIGUN")
		BREAK
		CASE HALFTRACK
			iWeaponSlotBitset = BIT0 | BIT1
			
			DISABLE_VEHICLE_WEAPON(TRUE, WEAPONTYPE_DLC_VEHICLE_HALFTRACK_DUALMG, vehIndex, LocalPlayerPed)
			PRINTLN("[DISABLE_VEHICLE_WEAPONS] DISABLE_VEHICLE_WEAPON - WEAPONTYPE_DLC_VEHICLE_HALFTRACK_DUALMG")
			DISABLE_VEHICLE_WEAPON(TRUE, WEAPONTYPE_DLC_VEHICLE_HALFTRACK_QUADMG, vehIndex, LocalPlayerPed)
			PRINTLN("[DISABLE_VEHICLE_WEAPONS] DISABLE_VEHICLE_WEAPON - WEAPONTYPE_DLC_VEHICLE_HALFTRACK_QUADMG")
		BREAK
		CASE INSURGENT3
			iWeaponSlotBitset = BIT1
			
			DISABLE_VEHICLE_WEAPON(TRUE, WEAPONTYPE_DLC_VEHICLE_INSURGENT_MINIGUN, vehIndex, LocalPlayerPed)
			PRINTLN("[DISABLE_VEHICLE_WEAPONS] DISABLE_VEHICLE_WEAPON - WEAPONTYPE_DLC_VEHICLE_INSURGENT_MINIGUN")
		BREAK
		CASE NIGHTSHARK
			iWeaponSlotBitset = BIT0
			
			DISABLE_VEHICLE_WEAPON(TRUE, WEAPONTYPE_DLC_VEHICLE_NIGHTSHARK_MG, vehIndex, LocalPlayerPed)
			PRINTLN("[DISABLE_VEHICLE_WEAPONS] DISABLE_VEHICLE_WEAPON - WEAPONTYPE_DLC_VEHICLE_NIGHTSHARK_MG")
		BREAK
		CASE OPPRESSOR
			iWeaponSlotBitset = BIT0 | BIT1
			
			DISABLE_VEHICLE_WEAPON(TRUE, WEAPONTYPE_DLC_VEHICLE_OPPRESSOR_MG, vehIndex, LocalPlayerPed)
			PRINTLN("[DISABLE_VEHICLE_WEAPONS] DISABLE_VEHICLE_WEAPON - WEAPONTYPE_DLC_VEHICLE_OPPRESSOR_MG")
			DISABLE_VEHICLE_WEAPON(TRUE, WEAPONTYPE_DLC_VEHICLE_OPPRESSOR_MISSILE, vehIndex, LocalPlayerPed)
			PRINTLN("[DISABLE_VEHICLE_WEAPONS] DISABLE_VEHICLE_WEAPON - WEAPONTYPE_DLC_VEHICLE_OPPRESSOR_MISSILE")
		BREAK
		CASE TAMPA3
			iWeaponSlotBitset = BIT0 | BIT1 | BIT2 | BIT3
		
			DISABLE_VEHICLE_WEAPON(TRUE, WEAPONTYPE_DLC_VEHICLE_TAMPA_MISSILE, vehIndex, LocalPlayerPed)
			PRINTLN("[DISABLE_VEHICLE_WEAPONS] DISABLE_VEHICLE_WEAPON - WEAPONTYPE_DLC_VEHICLE_TAMPA_MISSILE")
			DISABLE_VEHICLE_WEAPON(TRUE, WEAPONTYPE_DLC_VEHICLE_TAMPA_MORTAR, vehIndex, LocalPlayerPed)
			PRINTLN("[DISABLE_VEHICLE_WEAPONS] DISABLE_VEHICLE_WEAPON - WEAPONTYPE_DLC_VEHICLE_TAMPA_MORTAR")
			DISABLE_VEHICLE_WEAPON(TRUE, WEAPONTYPE_DLC_VEHICLE_TAMPA_FIXEDMINIGUN, vehIndex, LocalPlayerPed)
			PRINTLN("[DISABLE_VEHICLE_WEAPONS] DISABLE_VEHICLE_WEAPON - WEAPONTYPE_DLC_VEHICLE_TAMPA_FIXEDMINIGUN")
			DISABLE_VEHICLE_WEAPON(TRUE, WEAPONTYPE_DLC_VEHICLE_TAMPA_DUALMINIGUN, vehIndex, LocalPlayerPed)
			PRINTLN("[DISABLE_VEHICLE_WEAPONS] DISABLE_VEHICLE_WEAPON - WEAPONTYPE_DLC_VEHICLE_TAMPA_DUALMINIGUN")
		BREAK
		CASE TECHNICAL3
			iWeaponSlotBitset = BIT1
			
			DISABLE_VEHICLE_WEAPON(TRUE, WEAPONTYPE_DLC_VEHICLE_TECHNICAL_MINIGUN, vehIndex, LocalPlayerPed)
			PRINTLN("[DISABLE_VEHICLE_WEAPONS] DISABLE_VEHICLE_WEAPON - WEAPONTYPE_DLC_VEHICLE_TECHNICAL_MINIGUN")
		BREAK
		CASE TRAILERSMALL2
			iWeaponSlotBitset = BIT0 | BIT1 | BIT2
			
			DISABLE_VEHICLE_WEAPON(TRUE, WEAPONTYPE_DLC_VEHICLE_TRAILER_QUADMG, vehIndex, LocalPlayerPed)
			PRINTLN("[DISABLE_VEHICLE_WEAPONS] DISABLE_VEHICLE_WEAPON - WEAPONTYPE_DLC_VEHICLE_TRAILER_QUADMG")
			DISABLE_VEHICLE_WEAPON(TRUE, WEAPONTYPE_DLC_VEHICLE_TRAILER_DUALAA, vehIndex, LocalPlayerPed)
			PRINTLN("[DISABLE_VEHICLE_WEAPONS] DISABLE_VEHICLE_WEAPON - WEAPONTYPE_DLC_VEHICLE_TRAILER_DUALAA")
			DISABLE_VEHICLE_WEAPON(TRUE, WEAPONTYPE_DLC_VEHICLE_TRAILER_MISSILE, vehIndex, LocalPlayerPed)
			PRINTLN("[DISABLE_VEHICLE_WEAPONS] DISABLE_VEHICLE_WEAPON - WEAPONTYPE_DLC_VEHICLE_TRAILER_MISSILE")
		BREAK
		DEFAULT
			PRINTLN("[DISABLE_VEHICLE_WEAPON] Not a configured vehicle model!")
		BREAK
	ENDSWITCH
	
	INT iWeaponSlot
	
	REPEAT 4 iWeaponSlot	//Max Vehicle Weapon Slots
		IF IS_BIT_SET(iWeaponSlotBitset, iWeaponSlot)
			SET_VEHICLE_WEAPON_RESTRICTED_AMMO(vehIndex, iWeaponSlot, 0)
			PRINTLN("[DISABLE_VEHICLE_WEAPONS] SET_VEHICLE_WEAPON_RESTRICTED_AMMO - iWeaponSlot = ", iWeaponSlot)
		ENDIF
	ENDREPEAT
ENDPROC

PROC EXPLODE_VEHICLE_AT_ZERO_HEALTH(INT iVeh, VEHICLE_INDEX vehToCheck)
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFour, ciFMMC_VEHICLE4_VEHICLE_EXPLODE_AT_ZERO)
		
		FLOAT fPercentage = MC_GET_VEHICLE_TOTAL_HEALTH_PERCENTAGE(vehToCheck, iVeh, GET_TOTAL_STARTING_PLAYERS(), IS_VEHICLE_A_TRAILER(vehToCheck))
		
		IF fPercentage <= 0
		OR NOT IS_VEHICLE_DRIVEABLE(vehToCheck)
			IF NETWORK_HAS_CONTROL_OF_ENTITY(vehToCheck)
				PRINTLN("[RCC MISSION] EXPLODE_VEHICLE_AT_ZERO_HEALTH health: ", GET_ENTITY_HEALTH(vehToCheck))
				PRINTLN("[RCC MISSION] EXPLODE_VEHICLE_AT_ZERO_HEALTH health percentage: ", fPercentage)
				IF IS_ENTITY_ALIVE(vehToCheck) 
					NETWORK_EXPLODE_VEHICLE(vehToCheck, TRUE,TRUE)
				ENDIF
				PRINTLN("[RCC MISSION] EXPLODE_VEHICLE_AT_ZERO_HEALTH - Vehicle: ", iVeh, " ID: ", NATIVE_TO_INT(vehToCheck))
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC EXPLODE_VEHICLE_AT_ZERO_BODY_HEALTH(INT iVeh, VEHICLE_INDEX vehToCheck)
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetSix, ciFMMC_VEHICLE6_VEHICLE_EXPLODE_AT_ZERO_BODY)
		
		IF IS_ENTITY_ALIVE(vehToCheck) 
		AND GET_VEHICLE_BODY_HEALTH(vehToCheck) <= 0.0						
			IF NETWORK_HAS_CONTROL_OF_ENTITY(vehToCheck)
				PRINTLN("[RCC MISSION] EXPLODE_VEHICLE_AT_ZERO_BODY_HEALTH health: ", GET_ENTITY_HEALTH(vehToCheck))
				PRINTLN("[RCC MISSION] EXPLODE_VEHICLE_AT_ZERO_BODY_HEALTH body health: ", GET_VEHICLE_BODY_HEALTH(vehToCheck))
				NETWORK_EXPLODE_VEHICLE(vehToCheck, TRUE, TRUE)
				PRINTLN("[RCC MISSION] EXPLODE_VEHICLE_AT_ZERO_BODY_HEALTH - Vehicle: ", iVeh, " ID: ", NATIVE_TO_INT(vehToCheck))
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_VEHICLE_PHOTO_LOGIC(INT iveh, VEHICLE_INDEX ThisVeh)

	FLOAT fPhotoRange
	VECTOR vVehCoords, vPlateCoords1, vPlateCoords2
								
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitset[MC_serverBD_4.iVehPriority[iveh][MC_playerBD[iPartToUse].iteam]], ciBS_RULE_VEHPHOTO_NUMPLATES)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].ivehPhotoRange > 0
			fPhotoRange = TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].ivehPhotoRange)
		ELSE
			fPhotoRange = ci_PHOTO_RANGE_VEH
		ENDIF
	ELSE
		fPhotoRange = ci_PHOTO_RANGE_VEH_NUMPLATE
	ENDIF
	
	IF GET_DISTANCE_BETWEEN_ENTITIES(LocalPlayerPed,ThisVeh) < fPhotoRange
	
		PRINTLN("[RCC MISSION] [AW PHOTO]GET_DISTANCE_BETWEEN_ENTITIES < fPhotoRange : ", fPhotoRange) 
		
		IF MC_playerBD[iPartToUse].iVehNear = -1
			MC_playerBD[iPartToUse].iVehNear = iveh
			
			#IF IS_DEBUG_BUILD
				IF bInVehicleDebug
					PRINTLN("[RCC MISSION] AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH (1) Setting MC_playerBD[iPartToUse].iVehNear =  ", MC_playerBD[iPartToUse].iVehNear, " for part ",iPartToUse, " who is player ",  GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPartToUse))))
				ENDIF
			#ENDIF
		ENDIF
		
		IF HAS_CELLPHONE_CAM_JUST_TAKEN_PIC()
			
			SET_BIT(iVehPhotoChecksBS, iveh)
			
			BOOL bPlates = FALSE
			VECTOR vPlayer
			
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitset[MC_serverBD_4.iVehPriority[iveh][MC_playerBD[iPartToUse].iteam]], ciBS_RULE_VEHPHOTO_NUMPLATES)
				vVehCoords = GET_ENTITY_COORDS(ThisVeh)
				
				VECTOR vMax, vMin
				GET_MODEL_DIMENSIONS(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn, vMin, vMax)
				FLOAT fAddZ = FMAX(vMin.z, vMax.z)
				
				PRINTLN("[RCC MISSION] [AW PHOTO]ciBS_RULE_VEHPHOTO_NUMPLATES is not set. entity coords ", vVehCoords,", add on ",fAddZ," in z to get final vVehCoords ",vVehCoords + <<0, 0, fAddZ>>)
				vVehCoords.Z = vVehCoords.Z + fAddZ
			ELSE
				
				VEHICLE_PLATE_TYPE vpt = GET_VEHICLE_PLATE_TYPE(ThisVeh)
				
				IF vpt = VPT_BACK_PLATES
					INT iNumPlateBone = GET_ENTITY_BONE_INDEX_BY_NAME(ThisVeh,"platelight")
					
					vPlayer = GET_PLAYER_COORDS(LocalPlayer)
					
					IF iNumPlateBone != -1
						vVehCoords = GET_WORLD_POSITION_OF_ENTITY_BONE(ThisVeh,iNumPlateBone)
						vVehCoords.z = (vVehCoords.z - 0.075)
					ELSE
						VECTOR vMin,vMax
						GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(ThisVeh),vMin,vMax)
						vVehCoords = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ThisVeh,<<0,vMin.y,0>>)
						vVehCoords.z = (vVehCoords.z - 0.15)
					ENDIF
					
					bPlates = TRUE
					
				ELIF vpt = VPT_FRONT_PLATES
					
					vPlayer = GET_PLAYER_COORDS(LocalPlayer)
					
					VECTOR vMin,vMax
					GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(ThisVeh),vMin,vMax)
					
					vVehCoords = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ThisVeh,<<0,vMax.y,0>>)
					vVehCoords.z = (vVehCoords.z - 0.15)
					
					bPlates = TRUE
					
				ELIF vpt = VPT_FRONT_AND_BACK_PLATES
					
					INT iNumPlateBone = GET_ENTITY_BONE_INDEX_BY_NAME(ThisVeh,"platelight")
					
					vPlayer = GET_PLAYER_COORDS(LocalPlayer)
					
					VECTOR vMin,vMax
					GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(ThisVeh),vMin,vMax)
					
					VECTOR vMinCoords,vMaxCoords
					
					vMinCoords = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ThisVeh,<<0,vMin.y,0>>)
					vMaxCoords = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ThisVeh,<<0,vMax.y,0>>)
					
					FLOAT fPlayerFromMin, fPlayerFromMax
					
					fPlayerFromMin = GET_DISTANCE_BETWEEN_COORDS(vPlayer,vMinCoords)
					fPlayerFromMax = GET_DISTANCE_BETWEEN_COORDS(vPlayer,vMaxCoords)
					
					IF fPlayerFromMin < fPlayerFromMax
						IF iNumPlateBone != -1
							vVehCoords = GET_WORLD_POSITION_OF_ENTITY_BONE(ThisVeh,iNumPlateBone)
						ELSE
							vVehCoords = vMinCoords
							vVehCoords.z = (vVehCoords.z - 0.15)
						ENDIF
					ELSE
						vVehCoords = vMaxCoords
						vVehCoords.z = (vVehCoords.z - 0.15)
					ENDIF
					
					bPlates = TRUE
					
				ELSE //no numplates:
					vVehCoords = GET_ENTITY_COORDS(ThisVeh)
				ENDIF
				
			ENDIF
			
			IF bPlates
				
				VECTOR v1 = vVehCoords - GET_ENTITY_COORDS(ThisVeh)
				VECTOR v2 = vPlayer - vVehCoords
							
				FLOAT fAngle = GET_ANGLE_BETWEEN_2D_VECTORS(v1.x,v1.y,v2.x,v2.y)

				IF fAngle <= cf_PHOTO_NUMPLATE_MAXANGLE
				
					//for license plates use 2 tracked points that encompass the plates
					//this should allow for more precise check when photographing plates
					//and also stop plates being photographed when they are obscured, see B*2090614
					vPlateCoords1 = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vVehCoords, GET_ENTITY_HEADING(ThisVeh), << -0.08, 0.0, 0.0 >>)
					vPlateCoords2 = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vVehCoords, GET_ENTITY_HEADING(ThisVeh), << 0.08, 0.0, 0.0 >>)
					
					//DRAW_DEBUG_SPHERE(vPlateCoords1, 0.10, 255, 0, 0, 64)
					//DRAW_DEBUG_SPHERE(vPlateCoords2, 0.10, 0, 255, 0, 64)
					
					IF NOT DOES_PHOTO_TRACKED_POINT_EXIST(iVehPhotoTrackedPoint[iveh])
						//create new tracked point for current target coords
						CREATE_PHOTO_TRACKED_POINT_FOR_COORD(iVehPhotoTrackedPoint[iveh], vPlateCoords1, 0.10)
					ELSE
						SET_TRACKED_POINT_INFO(iVehPhotoTrackedPoint[iveh], vPlateCoords1, 0.10)
					ENDIF
					
					IF NOT DOES_PHOTO_TRACKED_POINT_EXIST(iVehPhotoTrackedPoint2[iveh])
						//create new tracked point for current target coords
						CREATE_PHOTO_TRACKED_POINT_FOR_COORD(iVehPhotoTrackedPoint2[iveh], vPlateCoords2, 0.10)
					ELSE
						SET_TRACKED_POINT_INFO(iVehPhotoTrackedPoint2[iveh], vPlateCoords2, 0.10)
					ENDIF
					
					IF DOES_PHOTO_TRACKED_POINT_EXIST(iVehPhotoTrackedPoint[iveh]) AND IS_PHOTO_TRACKED_POINT_VISIBLE(iVehPhotoTrackedPoint[iveh])
					AND DOES_PHOTO_TRACKED_POINT_EXIST(iVehPhotoTrackedPoint2[iveh]) AND IS_PHOTO_TRACKED_POINT_VISIBLE(iVehPhotoTrackedPoint2[iveh])
						IF IS_POINT_IN_CELLPHONE_CAMERA_VIEW(vVehCoords, 0.4)
							IF HAS_CELLPHONE_CAM_JUST_TAKEN_PIC()
								RESET_PHOTO_DATA(iveh)
								IF MC_playerBD[iPartToUse].iVehNear = iveh
									MC_playerBD[iPartToUse].iVehNear = -1
								ENDIF
								MC_playerBD[iPartToUse].ivehPhoto = iveh
								CLEAR_BIT(iVehPhotoChecksBS, iveh)
								
								IF SHOULD_PUT_AWAY_PHONE_AFTER_PHOTO(MC_serverBD_4.iVehPriority[iveh][MC_playerBD[iPartToUse].iTeam], ci_TARGET_VEHICLE)
									REINIT_NET_TIMER(tdPhotoTakenTimer)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
				ENDIF
				
			ELSE
				
				PRINTLN("[RCC MISSION] [AW PHOTO] bPlates: ", bPlates) 
				
				IF NOT DOES_PHOTO_TRACKED_POINT_EXIST(iVehPhotoTrackedPoint[iveh])
					//create new tracked point for current target coords
					CREATE_PHOTO_TRACKED_POINT_FOR_COORD(iVehPhotoTrackedPoint[iveh], vVehCoords, 2.5)
					vVehPhotoCoords[iveh] = vVehCoords
					PRINTLN("[RCC MISSION] [AW PHOTO] Creating a tracked point for the vehicle")
					PRINTLN("[RCC MISSION] [AW PHOTO] vVehPhotoCoords[iveh]: ", vVehPhotoCoords[iveh]) 
				ELSE
					IF NOT ARE_VECTORS_ALMOST_EQUAL(vVehCoords, vVehPhotoCoords[iveh], 0.05) //If the car's moved
						vVehPhotoCoords[iveh] = vVehCoords
						SET_TRACKED_POINT_INFO(iVehPhotoTrackedPoint[iveh],vVehPhotoCoords[iveh],2.5)
						PRINTLN("[RCC MISSION] [AW PHOTO] Tracked point has moved")
						PRINTLN("[RCC MISSION] [AW PHOTO] vVehPhotoCoords[iveh]: ", vVehPhotoCoords[iveh]) 
					ENDIF
				ENDIF
				
				IF DOES_PHOTO_TRACKED_POINT_EXIST(iVehPhotoTrackedPoint[iveh]) AND IS_PHOTO_TRACKED_POINT_VISIBLE(iVehPhotoTrackedPoint[iveh])
					PRINTLN("[RCC MISSION] [AW PHOTO] Tracked point exists and is visible")
					IF IS_POINT_IN_CELLPHONE_CAMERA_VIEW(vVehCoords, 0.5)
						PRINTLN("[RCC MISSION] [AW PHOTO] Point is in cellphone view")

						IF HAS_ENTITY_CLEAR_LOS_TO_ENTITY(LocalPlayerPed,ThisVeh,SCRIPT_INCLUDE_FOLIAGE)
							PRINTLN("[RCC MISSION] [AW PHOTO] Entity has clear LOS")
							RESET_PHOTO_DATA(iveh)
							IF MC_playerBD[iPartToUse].iVehNear = iveh
								MC_playerBD[iPartToUse].iVehNear = -1
							ENDIF
							MC_playerBD[iPartToUse].ivehPhoto = iveh
							CLEAR_BIT(iVehPhotoChecksBS, iveh)
							
							IF SHOULD_PUT_AWAY_PHONE_AFTER_PHOTO(MC_serverBD_4.iVehPriority[iveh][MC_playerBD[iPartToUse].iTeam], ci_TARGET_VEHICLE)
								PRINTLN("[RCC MISSION] [AW PHOTO] LBOOL3_PHOTOTAKEN : TRUE")
								REINIT_NET_TIMER(tdPhotoTakenTimer)
							ENDIF
						ENDIF							
					ENDIF
				#IF IS_DEBUG_BUILD
				ELSE
					PRINTLN("[RCC MISSION] [AW PHOTO] DOES_PHOTO_TRACKED_POINT_EXIST ",DOES_PHOTO_TRACKED_POINT_EXIST(iVehPhotoTrackedPoint[iveh]),", IS_PHOTO_TRACKED_POINT_VISIBLE ",IS_PHOTO_TRACKED_POINT_VISIBLE(iVehPhotoTrackedPoint[iveh]))
				#ENDIF
				ENDIF
				
			ENDIF
		ELSE
			PRINTLN("[RCC MISSION] [AW PHOTO] We haven't taken a photo any time recently, don't bother processing position things")
			IF IS_BIT_SET(iVehPhotoChecksBS, iveh)
				RESET_PHOTO_DATA(iveh)
				CLEAR_BIT(iVehPhotoChecksBS, iveh)
			ENDIF
		ENDIF
	ELSE
		IF IS_BIT_SET(iVehPhotoChecksBS, iveh)
			RESET_PHOTO_DATA(iveh)
			CLEAR_BIT(iVehPhotoChecksBS, iveh)
			
			IF MC_playerBD[iPartToUse].iVehNear = iveh
				MC_playerBD[iPartToUse].iVehNear = -1
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC CHECK_VEHICLE_TARGETING(FMMC_VEHICLE_STATE &sVehState)
	BOOL bNoLockOn = FALSE
	INT iteamrepeat
	
	IF sVehState.bExists
	AND sVehState.bAlive
	AND IS_ENTITY_A_MISSION_ENTITY(sVehState.vehIndex)
		IF NOT IS_BIT_SET(iVehTargetingCheckedThisRuleBS, sVehState.iIndex)
			FOR iteamrepeat = 0 TO (MC_serverBD.iNumberOfTeams-1)
				IF NOT bNoLockOn
					IF iteamrepeat = MC_playerBD[iPartToUse].iteam
					OR DOES_TEAM_LIKE_TEAM(MC_playerBD[iPartToUse].iteam, iteamrepeat)
						IF( (MC_serverBD_4.ivehRule[sVehState.iIndex][iteamrepeat] = FMMC_OBJECTIVE_LOGIC_PROTECT)
						OR (MC_serverBD_4.ivehRule[sVehState.iIndex][iteamrepeat] = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER)
						OR (MC_serverBD_4.ivehRule[sVehState.iIndex][iteamrepeat] = FMMC_OBJECTIVE_LOGIC_GO_TO)
						OR (MC_serverBD_4.ivehRule[sVehState.iIndex][iteamrepeat] = FMMC_OBJECTIVE_LOGIC_PHOTO)
						OR (MC_serverBD_4.ivehRule[sVehState.iIndex][iteamrepeat] = FMMC_OBJECTIVE_LOGIC_CAPTURE)
						OR (MC_serverBD_4.ivehRule[sVehState.iIndex][iteamrepeat] = FMMC_OBJECTIVE_LOGIC_GET_AND_HOLD) 
						OR (MC_serverBD_4.ivehRule[sVehState.iIndex][iteamrepeat] = FMMC_OBJECTIVE_LOGIC_LEAVE_ENTITY) )
						AND (MC_serverBD_4.iVehPriority[sVehState.iIndex][iteamrepeat] < FMMC_MAX_RULES)
						AND NOT (MC_serverBD_4.iVehMissionLogic[MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_KILL OR MC_serverBD_4.iVehMissionLogic[MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_DAMAGE) AND IS_FAKE_MULTIPLAYER_MODE_SET(TRUE)
							PRINTLN("[RCC MISSION] CHECK_VEHICLE_TARGETING, veh ",sVehState.iIndex, " Shouldn't lock on,  My team  = ", MC_playerBD[iPartToUse].iteam , " iteamrepeat = ", iteamrepeat, " MC_serverBD_4.ivehRule[sVehState.iIndex][iteamrepeat] = ", MC_serverBD_4.ivehRule[sVehState.iIndex][iteamrepeat] )
							bNoLockOn = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
			
			IF bNoLockOn			
				SET_VEHICLE_ALLOW_HOMING_MISSLE_LOCKON(sVehState.vehIndex, FALSE)
				PRINTLN("[RCC MISSION] CHECK_VEHICLE_TARGETING, veh ",sVehState.iIndex," - Vehicle cannot be locked on, on a protect rule")
			ELSE	
				SET_VEHICLE_ALLOW_HOMING_MISSLE_LOCKON(sVehState.vehIndex, TRUE)
				PRINTLN("[RCC MISSION] CHECK_VEHICLE_TARGETING, veh ",sVehState.iIndex," - Vehicle can be locked on")			
			ENDIF
			
			SET_BIT(iVehTargetingCheckedThisRuleBS, sVehState.iIndex)
		ENDIF
	ENDIF
ENDPROC

FUNC INT GET_PARTICIPANT_VEHICLE_EXCLUSIVE_DRIVER_NUMBER(INT iPart, INT iVeh)
	
	INT iPartLoop
	INT iDriverNum = 0
	
	PRINTLN("[PLAYER_LOOP] - GET_PARTICIPANT_VEHICLE_EXCLUSIVE_DRIVER_NUMBER")
	FOR iPartLoop = 0 TO (GET_MAXIMUM_PLAYERS_ON_MISSION() - 1)
		IF iPartLoop != iPart
			IF (MC_playerBD[iPartLoop].iTeam != -1)
			AND (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iTeamSeatPreference[MC_playerBD[iPartLoop].iTeam] = ciFMMC_SeatPreference_Driver)
			AND NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPartLoop))
				PRINTLN("[RCC MISSION] GET_PARTICIPANT_VEHICLE_EXCLUSIVE_DRIVER_NUMBER iPartLoop = ",iPartLoop," iTeam = ",MC_playerBD[iPartLoop].iTeam," iDriverNum = ",iDriverNum)
				iDriverNum++
			ENDIF
		ELSE
			BREAKLOOP
		ENDIF
	ENDFOR
	
	RETURN iDriverNum
	
ENDFUNC

PROC CHECK_SEATING_PREFERENCES(INT iVeh)
	
	#IF IS_DEBUG_BUILD
		MODEL_NAMES mSeat
		IF bWdSeatPreferenceDebug
			mSeat = GET_ENTITY_MODEL(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh]))
			PRINTLN("[RCC MISSION] CHECK_SEATING_PREFERENCES - Called for veh ", iVeh, " model = ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(mSeat), " team set pref = ",g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iTeamSeatPreference[MC_playerBD[iLocalPart].iTeam])
		ENDIF
	#ENDIF
	
	IF NOT IS_BIT_SET(iVehSeatPreferenceCheckBS, iVeh)
		
		INT iTeam = GET_PRE_ASSIGNMENT_PLAYER_TEAM()
		
		IF (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iTeamSeatPreference[iTeam] != ciFMMC_SeatPreference_None)
			IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
			AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
			AND IS_VEHICLE_DRIVEABLE(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh]))
				IF iSeatPreferenceSlot < 3
					IF (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iTeamSeatPreference[iTeam] = ciFMMC_SeatPreference_Driver)
						IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iLocalPart))
						AND bLocalPlayerPedOk
							
							INT iDriver = GET_PARTICIPANT_VEHICLE_EXCLUSIVE_DRIVER_NUMBER(iLocalPart, iVeh)
							
							SET_VEHICLE_EXCLUSIVE_DRIVER(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh]), LocalPlayerPed, iDriver)
							SET_PED_VEHICLE_FORCED_SEAT_USAGE(LocalPlayerPed,NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh]),iSeatPreferenceSlot,ENUM_TO_INT(VC_FORCE_USE_FRONT_SEATS))
							iSeatPreferenceSlot++
							
							#IF IS_DEBUG_BUILD
								IF bWdSeatPreferenceDebug
									PRINTLN("[RCC MISSION] CHECK_SEATING_PREFERENCES - Setting as exclusive driver ",iDriver," on vehicle ", iVeh, " Model = ",  GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(mSeat))
								ENDIF
							#ENDIF
							
							PRINTLN("[RCC MISSION] CHECK_SEATING_PREFERENCES - Setting as exclusive driver ",iDriver," on vehicle ", iVeh)
						
						ENDIF
					ELIF (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iTeamSeatPreference[iTeam] = ciFMMC_SeatPreference_Rear_left)
						IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iLocalPart))
						AND bLocalPlayerPedOk
						
							SET_PED_VEHICLE_FORCED_SEAT_USAGE(LocalPlayerPed,NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh]),iSeatPreferenceSlot,ENUM_TO_INT(VC_FORCE_USE_REAR_SEATS))
							iSeatPreferenceSlot++
							
							#IF IS_DEBUG_BUILD
								IF bWdSeatPreferenceDebug
									PRINTLN("[RCC MISSION] CHECK_SEATING_PREFERENCES - Setting as prefer rear left seat on vehicle ", iVeh, " Model = ",  GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(mSeat))
								ENDIF
							#ENDIF
							
							PRINTLN("[RCC MISSION] CHECK_SEATING_PREFERENCES - Setting as prefer rear left seat on vehicle ", iVeh)
							
						ENDIF
					ELIF (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iTeamSeatPreference[iTeam] = ciFMMC_SeatPreference_Rear_right)
						IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iLocalPart))
						AND bLocalPlayerPedOk
							SET_PED_VEHICLE_FORCED_SEAT_USAGE(LocalPlayerPed,NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh]),iSeatPreferenceSlot,ENUM_TO_INT(VC_FORCE_USE_REAR_SEATS))
							iSeatPreferenceSlot++							
							
							#IF IS_DEBUG_BUILD
								IF bWdSeatPreferenceDebug
									PRINTLN("[RCC MISSION] CHECK_SEATING_PREFERENCES - Setting as prefer rear right seat on vehicle ", iVeh, " Model = ",  GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(mSeat))
								ENDIF
							#ENDIF
							
							PRINTLN("[RCC MISSION] CHECK_SEATING_PREFERENCES - Setting as prefer rear right seat on vehicle ", iVeh)
						ENDIF
					ELIF (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iTeamSeatPreference[iTeam] = ciFMMC_SeatPreference_Passenger)
						IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iLocalPart))
						AND bLocalPlayerPedOk
						
							SET_PED_VEHICLE_FORCED_SEAT_USAGE(LocalPlayerPed,NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh]),iSeatPreferenceSlot,ENUM_TO_INT(VC_FORCE_USE_FRONT_SEATS))
							iSeatPreferenceSlot++
							#IF IS_DEBUG_BUILD
								IF bWdSeatPreferenceDebug
									PRINTLN("[RCC MISSION] CHECK_SEATING_PREFERENCES - Setting as prefer passenger seats on vehicle ", iVeh, " Model = ",  GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(mSeat))
								ENDIF
							#ENDIF
							
							PRINTLN("[RCC MISSION] CHECK_SEATING_PREFERENCES - Setting as prefer passenger seats on vehicle ", iVeh)
						ENDIF
					ELIF (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iTeamSeatPreference[iTeam] = ciFMMC_SeatPreference_Front_Seats)
						IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iLocalPart))
						AND bLocalPlayerPedOk
						
							SET_PED_VEHICLE_FORCED_SEAT_USAGE(LocalPlayerPed,NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh]),iSeatPreferenceSlot,ENUM_TO_INT(VC_FORCE_USE_FRONT_SEATS))
							iSeatPreferenceSlot++
							#IF IS_DEBUG_BUILD
								IF bWdSeatPreferenceDebug
									PRINTLN("[RCC MISSION] CHECK_SEATING_PREFERENCES - Setting as prefer front seats on vehicle ", iVeh, " Model = ",  GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(mSeat))
								ENDIF
							#ENDIF
							
							PRINTLN("[RCC MISSION] CHECK_SEATING_PREFERENCES - Setting as prefer front seats on vehicle ", iVeh)
						ENDIF
					ELIF (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iTeamSeatPreference[iTeam] = ciFMMC_SeatPreference_Extra_left_1)
						IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iLocalPart))
						AND bLocalPlayerPedOk
						
							SET_PED_VEHICLE_FORCED_SEAT_USAGE(LocalPlayerPed,NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh]), iSeatPreferenceSlot, ENUM_TO_INT(VC_NONE), VS_EXTRA_LEFT_1)
							iSeatPreferenceSlot++
							#IF IS_DEBUG_BUILD
								IF bWdSeatPreferenceDebug
									PRINTLN("[RCC MISSION] CHECK_SEATING_PREFERENCES - Setting as prefer VS_EXTRA_LEFT_1 on vehicle ", iVeh, " Model = ",  GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(mSeat))
								ENDIF
							#ENDIF
							
							PRINTLN("[RCC MISSION] CHECK_SEATING_PREFERENCES - Setting as prefer VS_EXTRA_LEFT_1 on vehicle ", iVeh)
						ENDIF
					ELIF (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iTeamSeatPreference[iTeam] = ciFMMC_SeatPreference_Extra_right_1)
						IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iLocalPart))
						AND bLocalPlayerPedOk
						
							SET_PED_VEHICLE_FORCED_SEAT_USAGE(LocalPlayerPed,NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh]), iSeatPreferenceSlot, ENUM_TO_INT(VC_NONE), VS_EXTRA_RIGHT_1)
							iSeatPreferenceSlot++
							#IF IS_DEBUG_BUILD
								IF bWdSeatPreferenceDebug
									PRINTLN("[RCC MISSION] CHECK_SEATING_PREFERENCES - Setting as prefer VS_EXTRA_RIGHT_1 on vehicle ", iVeh, " Model = ",  GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(mSeat))
								ENDIF
							#ENDIF
							
							PRINTLN("[RCC MISSION] CHECK_SEATING_PREFERENCES - Setting as prefer VS_EXTRA_RIGHT_1 on vehicle ", iVeh)
						ENDIF
					ELSE
						PRINTLN("[RCC MISSION] CHECK_SEATING_PREFERENCES - No seat preference on vehicle ", iVeh)
					ENDIF
					SET_BIT(iVehSeatPreferenceCheckBS, iVeh)
					PRINTLN("[RCC MISSION] CHECK_SEATING_PREFERENCES - iSeatPreferenceSlot = ", iSeatPreferenceSlot)
				ELSE
					PRINTLN("[RCC MISSION] CHECK_SEATING_PREFERENCES - Player trying to set more than the max allowed seating prefs iSeatPreferenceSlot: ", iSeatPreferenceSlot)
				ENDIF
			ENDIF
		ELSE
			SET_BIT(iVehSeatPreferenceCheckBS, iVeh)
		ENDIF
	ENDIF
	
ENDPROC

//PURPOSE: When the mission is ending this checks for cases where we should be setting the heli blades to full speed
PROC HANDLE_PROPELLORS_WHEN_MISSION_ENDS()

	IF IS_PED_IN_ANY_HELI(LocalPlayerPed)
	
		VEHICLE_INDEX viPlayerVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
		
		IF (IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
		OR MC_serverBD.iServerGameState = GAME_STATE_END)
			SET_HELI_BLADES_FULL_SPEED(viPlayerVeh)
		ENDIF
	ENDIF
ENDPROC

PROC GET_CARGOPLANE_ENGINE_KNOCKBACK(VEHICLE_INDEX veh, VEHICLE_INDEX playerVeh)
	
	VECTOR vMin, vMax
	GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(veh), vMin, vMax)
	VECTOR playerPos = GET_ENTITY_COORDS(playerVeh)
	VECTOR localOffset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(veh, playerPos)
	FLOAT height = ABSF(localOffset.z)
	IF height < 8.0
	AND localOffset.x > vMin.x AND localOffset.x < vMax.x
	AND localOffset.y > vMin.y AND localOffset.y < vMax.y
		VECTOR direction = GET_ENTITY_FORWARD_VECTOR(veh)
		APPLY_FORCE_TO_ENTITY_CENTER_OF_MASS(playerVeh, APPLY_TYPE_FORCE, direction* -10.0, 0, FALSE, TRUE)
	ENDIF
ENDPROC

PROC PROCESS_VEHICLE_ENGINE_KNOCKBACK(FMMC_VEHICLE_STATE &sVehState)
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetSeven, ciFMMC_VEHICLE7_ENGINE_KNOCKBACK)
	AND sVehState.bExists
	AND IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		VEHICLE_INDEX playerVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
		IF sVehState.mn = CARGOPLANE
			IF GET_DIST2_BETWEEN_ENTITIES(sVehState.vehIndex, playerVeh) < POW(75.0, 2)
				GET_CARGOPLANE_ENGINE_KNOCKBACK(sVehState.vehIndex, playerVeh)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC CLEAN_UP_VEH_CAPTURE_PROGRESS(INT iVeh)
	
	IF IS_BIT_SET(MC_PlayerBD[iLocalPart].iVehDestroyBitset, iVeh)
		CLEAR_BIT(MC_PlayerBD[iLocalPart].iVehDestroyBitset, iVeh)
	ENDIF
	
	IF IS_BIT_SET(iHackingVehInRangeBitSet, iVeh)
		CLEAR_BIT(iHackingVehInRangeBitSet, iVeh)
	ENDIF
	
	IF MC_playerBD[iLocalPart].iVehCapturing = iVeh
		PRINTLN("[JS] iVehCapturing cleared (2). Was: ", iVeh)
		MC_playerBD[iLocalPart].iVehCapturing = -1
		
		IF iHackStage != ciSECUROSERV_HACK_STAGE_HACK_COMPLETE
			iHackStage = ciSECUROSERV_HACK_STAGE_NO_SIGNAL
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(MC_playerBD[iLocalPart].iRequestCaptureVehBS, iVeh)
		CLEAR_BIT(MC_playerBD[iLocalPart].iRequestCaptureVehBS, iVeh)
	ENDIF
	
	IF DOES_BLIP_EXIST(biVehCaptureRangeBlip[iVeh])
		REMOVE_BLIP(biVehCaptureRangeBlip[iVeh])
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck20, LBOOL20_HACKING_LOOP_STARTED)
		STOP_SOUND(iHackingLoopSoundID)
		RELEASE_SOUND_ID(iHackingLoopSoundID)
		iHackingLoopSoundID = -1
		CLEAR_BIT(iLocalBoolCheck20, LBOOL20_HACKING_LOOP_STARTED)
	ENDIF
ENDPROC

PROC PROCESS_VEH_CAPTURE_BLIP(INT iVeh, VEHICLE_INDEX viTemp, BOOL bInRange)
	
	IF DOES_BLIP_EXIST(biVehBlip[iVeh])
		IF NOT DOES_BLIP_EXIST(biVehCaptureRangeBlip[iVeh])
			biVehCaptureRangeBlip[iVeh] = ADD_BLIP_FOR_RADIUS(GET_ENTITY_COORDS(viTemp, FALSE), TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCaptureRange))
			SET_BLIP_COLOUR(biVehCaptureRangeBlip[iVeh], BLIP_COLOUR_YELLOW)
		ENDIF
	ELSE
		IF DOES_BLIP_EXIST(biVehCaptureRangeBlip[iVeh])
			REMOVE_BLIP(biVehCaptureRangeBlip[iVeh])
		ENDIF
	ENDIF
	
	IF NOT DOES_BLIP_EXIST(biVehCaptureRangeBlip[iVeh])
		EXIT
	ENDIF
	
	SET_BLIP_COORDS(biVehCaptureRangeBlip[iVeh], GET_ENTITY_COORDS(viTemp, FALSE))
	
	IF bInRange
		SET_BLIP_ALPHA(biVehCaptureRangeBlip[iVeh], 50)
	ELSE
		SET_BLIP_ALPHA(biVehCaptureRangeBlip[iVeh], 255)
	ENDIF
ENDPROC

PROC PROCESS_VEH_CAPTURE_RANGE(INT iVeh)
	
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
		CLEAN_UP_VEH_CAPTURE_PROGRESS(iVeh)
		EXIT
	ENDIF
	
	VEHICLE_INDEX viTemp = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
	
	IF IS_ENTITY_DEAD(viTemp)
		CLEAN_UP_VEH_CAPTURE_PROGRESS(iVeh)
		EXIT
	ENDIF
	
	IF IS_BIT_SET(MC_serverBD_4.iVehHackedReadyForDeathBS, iVeh)
		IF (MC_playerBD[iPartToUse].iVehCapturing = iVeh OR IS_BIT_SET(iHackingVehInRangeBitSet, iVeh))
		AND NOT IS_BIT_SET(MC_PlayerBD[iLocalPart].iVehDestroyBitset, iVeh)	
			PRINTLN("PROCESS_VEH_CAPTURE_RANGE - We need to destroy veh: ", iVeh)
			SET_BIT(MC_PlayerBD[iLocalPart].iVehDestroyBitset, iVeh)
			MC_playerBD[iLocalPart].iVehCapturing = -1
			CLEAR_BIT(MC_playerBD[iLocalPart].iRequestCaptureVehBS, iVeh)
		ENDIF
		
		SET_VEHICLE_ALLOW_HOMING_MISSLE_LOCKON(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh]), TRUE)
	ENDIF
	
	IF IS_BIT_SET(MC_PlayerBD[iPartToUse].iVehDestroyBitset, iVeh)
		CLEAN_UP_VEH_CAPTURE_PROGRESS(iVeh)
		EXIT
	ENDIF
	
	IF IS_BIT_SET(MC_serverBD_4.iVehUnlockOnCapture, iVeh)
		bReadyToCapture = FALSE
		CLEAN_UP_VEH_CAPTURE_PROGRESS(iVeh)
		
		IF NOT IS_BIT_SET(MC_serverBD_4.iVehHackedReadyForDeathBS, iVeh)
			IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFive, ciFMMC_VEHICLE5_HACKING_EXPLODE_ON_CAPTURE)
				IF bIsLocalPlayerHost
					IF NETWORK_HAS_CONTROL_OF_ENTITY(viTemp)
						SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(viTemp, FALSE)
					ELSE
						NETWORK_REQUEST_CONTROL_OF_ENTITY(viTemp)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(MC_serverBD_4.iVehExplodeKillOnCaptureBS, iVeh)
		IF MC_serverBD.iVehTeamFailBitset[iVeh] > 0
			IF bIsLocalPlayerHost
				INT iTeam
				
				FOR iTeam = 0 TO FMMC_MAX_TEAMS - 1
					CLEAR_BIT(MC_serverBD.iMissionCriticalVeh[iTeam], iVeh)
					CLEAR_BIT(MC_serverBD.iVehTeamFailBitset[iVeh], iTeam)
				ENDFOR
			ENDIF
		ELSE
			IF NETWORK_HAS_CONTROL_OF_ENTITY(viTemp)
				NETWORK_EXPLODE_VEHICLE(viTemp)
			ELSE
				NETWORK_REQUEST_CONTROL_OF_ENTITY(viTemp)
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(MC_serverBD_4.iVehExplodeKillOnCaptureBS, iVeh)
	OR IS_BIT_SET(MC_serverBD_4.iVehUnlockOnCapture, iVeh)
		EXIT
	ENDIF
		
	FLOAT fDistanceToHackVeh = VDIST2(GET_ENTITY_COORDS(LocalPlayerPed), GET_ENTITY_COORDS(viTemp))
	BOOL bInRangeToCapture = fDistanceToHackVeh < POW(TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCaptureRange), 2)
	
	IF bInRangeToCapture
		
		IF NOT IS_BIT_SET(iHackingVehInRangeBitSet, iVeh)
			SET_BIT(iHackingVehInRangeBitSet, iVeh)
		ENDIF
		
		IF NOT IS_BIT_SET(MC_serverBD_4.iCaptureVehRequestPartBS, iVeh)
			IF NOT bReadyToCapture
				IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iAlreadyBeenCapturingVehBS, iVeh)
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_SECUROSERV_HACKING)
					AND GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("appSecuroHack")) = 0
						IF iHackStage != ciSECUROSERV_HACK_STAGE_HACK_COMPLETE
							IF IS_PHONE_ONSCREEN()
								IF IS_THIS_PRINT_BEING_DISPLAYED("HACK_HELP2") //Should be IS_THIS_HELP_MESSAGE_BEING_DISPLAYED - Change if refactored
									CLEAR_HELP()
								ENDIF
								
								IF NOT IS_THIS_PRINT_BEING_DISPLAYED("HACK_HELP3") //Should be IS_THIS_HELP_MESSAGE_BEING_DISPLAYED - Change if refactored
									PRINT_HELP_FOREVER("HACK_HELP3")
								ENDIF
							ELSE
								IF IS_THIS_PRINT_BEING_DISPLAYED("HACK_HELP3") //Should be IS_THIS_HELP_MESSAGE_BEING_DISPLAYED - Change if refactored
									CLEAR_HELP()
								ENDIF
								
								IF NOT IS_THIS_PRINT_BEING_DISPLAYED("HACK_HELP2") //Should be IS_THIS_HELP_MESSAGE_BEING_DISPLAYED - Change if refactored
								AND bLocalPlayerPedOK
									PRINT_HELP_FOREVER("HACK_HELP2")
								ENDIF
							ENDIF
						ELSE
							IF NOT IS_THIS_PRINT_BEING_DISPLAYED("HACK_HELP") //Should be IS_THIS_HELP_MESSAGE_BEING_DISPLAYED - Change if refactored
								PRINT_HELP_FOREVER("HACK_HELP")
							ENDIF
						ENDIF
					ENDIF
					
					SET_BIT(iHackingVehHelpBitSet, iVeh)
				ENDIF
			ENDIF

			IF bReadyToCapture
			OR IS_BIT_SET(MC_playerBD[iPartToUse].iAlreadyBeenCapturingVehBS, iVeh)
			
				BOOL bCaptureBlocked
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetTen, ciFMMC_VEHICLE10_UseCapturePrompt)
					IF IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_CONTEXT)
						PRINTLN("PROCESS_VEH_CAPTURE_RANGE - Player pressed input context - Initiating capture")
						bCaptureBlocked = FALSE
					ELSE
						PRINTLN("PROCESS_VEH_CAPTURE_RANGE - Blocking capture and displaying prompt text to user")
						STRING stHelpText = GET_VEHICLE_CAPTURE_HELP_TEXT(iVeh)
						DISPLAY_HELP_TEXT_THIS_FRAME(stHelpText, FALSE)
						bCaptureBlocked = TRUE
					ENDIF
				ENDIF
				
				IF NOT bCaptureBlocked
			
					PRINTLN("PROCESS_VEH_CAPTURE_RANGE - Setting request capture bit, asking server to let this player capture the veh")
					iHackPercentage = 0
					
					IF MC_playerBD[iLocalPart].iVehCapturing != iVeh
						MC_playerBD[iLocalPart].iVehCapturing = iVeh
					ENDIF
					
					SET_BIT(MC_playerBD[iPartToUse].iRequestCaptureVehBS, iVeh)
					SET_BIT(MC_playerBD[iPartToUse].iAlreadyBeenCapturingVehBS, iVeh)
					bReadyToCapture = FALSE
					
					IF IS_BIT_SET(iHackingVehHelpBitSet, iVeh)
						CLEAR_HELP()
						CLEAR_BIT(iHackingVehHelpBitSet, iVeh)
					ENDIF
					
				ENDIF
				
			ENDIF
		ELSE
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_SECUROSERV_HACKING)
				IF IS_BIT_SET(MC_playerBD[iPartToUse].iRequestCaptureVehBS, iVeh)
				AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciSECUROSERV_DONT_FORCE_ON_SCREEN)	
					IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("appSecuroHack")) = 0
					AND NOT IS_FAKE_MULTIPLAYER_MODE_SET(TRUE)
					AND NOT	IS_PLAYER_RESPAWNING(LocalPlayer)
					AND NOT IS_PED_DEAD_OR_DYING(LocalPlayerPed)
						PRINTLN("[JS][SECUROHACK] relaunching app")
						LAUNCH_CELLPHONE_APPLICATION(AppDummyApp0, TRUE, TRUE, FALSE)
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(MC_playerBD[iPartToUse].iRequestCaptureVehBS, iVeh)
					FLOAT fLosingSignalAreaSize = 0.15
					FLOAT fRange = TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCaptureRange)
					
					fRange = fRange - (fRange * fLosingSignalAreaSize)
					
					//DIST CHECK
					IF fDistanceToHackVeh > POW(fRange, 2)
						SET_BIT(iLocalBoolCheck26, LBOOL26_SERVUROSERV_LOSING_SIGNAL)
						PRINTLN("[SECUROHACK] Setting losing signal bit")
					ELSE
						CLEAR_BIT(iLocalBoolCheck26, LBOOL26_SERVUROSERV_LOSING_SIGNAL)
						PRINTLN("[SECUROHACK] Clearing losing signal bit")
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(iHackingVehHelpBitSet, iVeh)
				CLEAR_HELP()
				CLEAR_BIT(iHackingVehHelpBitSet, iVeh)
			ENDIF
			
			bReadyToCapture = FALSE
		ENDIF
	ELSE
		IF IS_BIT_SET(iHackingVehInRangeBitSet, iVeh)
			CLEAR_BIT(iHackingVehInRangeBitSet, iVeh)
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_SECUROSERV_HACKING)
				PRINTLN("[SECUROHACK] Reseting hack stage")
				
				IF NOT IS_HACK_STOP_SOUND_BLOCKED()
					PLAY_SOUND_FRONTEND(-1, "Hack_Stop", "DLC_IE_SVM_Voltic2_Hacking_Sounds")
				ENDIF
				
				iHackStage = ciSECUROSERV_HACK_STAGE_NO_SIGNAL
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_SECUROSERV_HACKING)
			IF IS_BIT_SET(iLocalBoolCheck20, LBOOL20_HACKING_LOOP_STARTED)
			AND iHackStage = ciSECUROSERV_HACK_STAGE_NO_SIGNAL
				STOP_SOUND(iHackingLoopSoundID)
				RELEASE_SOUND_ID(iHackingLoopSoundID)
				iHackingLoopSoundID = -1
				CLEAR_BIT(iLocalBoolCheck20, LBOOL20_HACKING_LOOP_STARTED)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(iHackingVehHelpBitSet, iVeh)
			CLEAR_HELP()
			CLEAR_BIT(iHackingVehHelpBitSet, iVeh)
		ENDIF
		
		IF IS_BIT_SET(MC_playerBD[iPartToUse].iRequestCaptureVehBS, iVeh)
			PRINTLN("PROCESS_VEH_CAPTURE_RANGE - Clearing the capture request bit for local player as they have gone out of range - range is: ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iCaptureRange)
			CLEAR_BIT(MC_playerBD[iPartToUse].iRequestCaptureVehBS, iVeh)
			IF MC_playerBD[iLocalPart].iVehCapturing = iVeh
				MC_playerBD[iLocalPart].iVehCapturing = -1
			ENDIF
			bReadyToCapture = FALSE
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Applies a wanted level to the local player on stealing this specific vehicle. Value set on placing the vehicle in the creator.
PROC STEALING_SETS_LOCAL_PLAYER_WANTED(FMMC_VEHICLE_STATE &sVehState)
	
	INT iWantedLevel = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iWantedLevelOnTheft
	
	IF iWantedLevel = 0
		EXIT
	ENDIF
	
	IF GET_PLAYER_WANTED_LEVEL(LocalPlayer) < iWantedLevel	// [ML] Less than to avoid the player gaming this by stealing a car to reduce their wanted level
	AND NOT IS_BIT_SET(MC_serverBD_1.iVehicleTheftWantedTriggered, sVehState.iIndex)
		IF sVehState.bExists

			IF GET_VEHICLE_PED_IS_ENTERING(LocalPlayerPed) = sVehState.vehIndex
			AND MC_IS_ENTITY_TOUCHING_ENTITY(LocalPlayerPed, sVehState.vehIndex)
				
				IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehicleSpecificTheftWantedDelay = 0
					PRINTLN("[STEALING_SETS_LOCAL_PLAYER_WANTED] - Current Player wanted level = ", GET_PLAYER_WANTED_LEVEL(LocalPlayer))
					SET_PLAYER_WANTED_LEVEL(LocalPlayer, iWantedLevel)
					SET_PLAYER_WANTED_LEVEL_NOW(LocalPlayer)	
					BROADCAST_NOTIFY_WANTED_SET_FOR_VEHICLE(sVehState.iIndex)		
					PRINTLN("[STEALING_SETS_LOCAL_PLAYER_WANTED] - Set Player wanted level = ", iWantedLevel)
				ELSE
					IF NOT HAS_NET_TIMER_STARTED(tdVehicleWantedDelayTimer)
						REINIT_NET_TIMER(tdVehicleWantedDelayTimer)
					ENDIF
				ENDIF
			ENDIF
			
			IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehicleSpecificTheftWantedDelay > 0
				IF HAS_NET_TIMER_STARTED(tdVehicleWantedDelayTimer)
					IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdVehicleWantedDelayTimer) < (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehicleSpecificTheftWantedDelay * 1000)
						EXIT					
					ELSE
						PRINTLN("[STEALING_SETS_LOCAL_PLAYER_WANTED] - Current Player wanted level = ", GET_PLAYER_WANTED_LEVEL(LocalPlayer))
						SET_PLAYER_WANTED_LEVEL(LocalPlayer, iWantedLevel)
						SET_PLAYER_WANTED_LEVEL_NOW(LocalPlayer)	
						BROADCAST_NOTIFY_WANTED_SET_FOR_VEHICLE(sVehState.iIndex)		
						PRINTLN("[STEALING_SETS_LOCAL_PLAYER_WANTED] - Set Player wanted level = ", iWantedLevel)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_VEHICLE_LOCK_SHOCK(FMMC_VEHICLE_STATE &sVehState)
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetFive, ciFMMC_VEHICLE5_SHOCKED_IF_LOCKED)
		IF sVehState.bExists
			SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(sVehState.vehIndex, FALSE)
			SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(sVehState.vehIndex, TRUE)
			IF NOT IS_BIT_SET(MC_serverBD_4.iVehUnlockOnCapture, sVehState.iIndex)
				IF IS_PED_TRYING_TO_ENTER_A_LOCKED_VEHICLE(LocalPlayerPed)
					IF bLocalPlayerOK
					AND IS_PED_GETTING_INTO_A_VEHICLE(LocalPlayerPed)
						VEHICLE_INDEX vehEntering = GET_VEHICLE_PED_IS_USING(LocalPlayerPed)
						IF sVehState.vehIndex = vehEntering
							IF sVehState.bDrivable
								IF iOpenAnimStarted = -1
									iOpenAnimStarted = GET_GAME_TIMER()
								ELSE
									IF HAS_ANIM_EVENT_FIRED(LocalPlayerPed, HASH("HandContactWithDoor"))
									OR (GET_GAME_TIMER() - iOpenAnimStarted) > waittimezap
										PRINTLN("PROCESS_VEHICLE_LOCK_SHOCK - Zapping player for attempting to enter vehicle with a shock lock turned on.")
										VECTOR vOffset
										SHOOT_SINGLE_BULLET_BETWEEN_COORDS(GET_PED_BONE_COORDS(LocalPlayerPed, BONETAG_PELVIS, vOffset), GET_PED_BONE_COORDS(LocalPlayerPed, BONETAG_NECK, vOffset), 1, TRUE, WEAPONTYPE_STUNGUN, NULL, TRUE, TRUE, -1)
										iOpenAnimStarted = -1
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_HELICOPTER_SMOKE_FX_ON_LOW_BODY_HEALTH(FMMC_VEHICLE_STATE &sVehState)
	
	FLOAT fPTFXEvo = 1.0
		
	STRING sBone1 = "exhaust"
	STRING sBone2 = "exhaust_2"
	
	INT iBone1 = -1
	INT iBone2 = -1
	
	iBone1 = GET_ENTITY_BONE_INDEX_BY_NAME(sVehState.vehIndex, sBone1)
	iBone2 = GET_ENTITY_BONE_INDEX_BY_NAME(sVehState.vehIndex, sBone2)
	
	IF iBone1 != -1
		IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(ptfxHeliDamageSmoke1)
			PRINTLN("[SMOKE] PROCESS_HELICOPTER_WEAPON_SMOKE_EFFECT - Starting helicopter damage smoke fx 1")
			USE_PARTICLE_FX_ASSET(GET_HELI_SMOKE_PTFX_ASSET(sVehState.iIndex))
			ptfxHeliDamageSmoke1 = START_PARTICLE_FX_LOOPED_ON_ENTITY_BONE(GET_HELI_SMOKE_PTFX_NAME(sVehState.iIndex), sVehState.vehIndex, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, iBone1)
		ELSE
			SET_PARTICLE_FX_LOOPED_EVOLUTION(ptfxHeliDamageSmoke1, "damage", fPTFXEvo)
		ENDIF
	ENDIF
	
	IF iBone2 != -1
		IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(ptfxHeliDamageSmoke2)
			PRINTLN("[SMOKE] PROCESS_HELICOPTER_WEAPON_SMOKE_EFFECT - Starting helicopter damage smoke fx 2")
			USE_PARTICLE_FX_ASSET(GET_HELI_SMOKE_PTFX_ASSET(sVehState.iIndex))
			ptfxHeliDamageSmoke2 = START_PARTICLE_FX_LOOPED_ON_ENTITY_BONE(GET_HELI_SMOKE_PTFX_NAME(sVehState.iIndex), sVehState.vehIndex, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, iBone2)
		ELSE
			SET_PARTICLE_FX_LOOPED_EVOLUTION(ptfxHeliDamageSmoke2, "damage", fPTFXEvo)
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_HELICOPTER_SOUND_FX_ON_LOW_BODY_HEALTH(FMMC_VEHICLE_STATE &sVehState)

	IF NOT IS_BIT_SET(iLocalBoolCheck35, LBOOL35_PLAYED_LOW_BODY_HEALTH_SOUND_FX)
		
		STRING stSoundName = GET_HELI_LOW_HEALTH_SOUND_NAME(sVehState.iIndex)
		STRING stSoundSet = GET_HELI_LOW_HEALTH_SOUND_SET(sVehState.iIndex)
		
		IF NOT IS_STRING_NULL_OR_EMPTY(stSoundName)
		AND NOT IS_STRING_NULL_OR_EMPTY(stSoundSet)
			
			IF sVehState.bExists
			AND sVehState.bHasControl
				PRINTLN("[VEH_SFX] PROCESS_HELICOPTER_SOUND_FX_ON_LOW_BODY_HEALTH - Playing sound effect on heli with low body health. Sound name: ", stSoundName, " Sound set: ", stSoundSet)
				PLAY_SOUND_FROM_ENTITY(-1, stSoundName, sVehState.vehIndex, stSoundSet, TRUE)
			ENDIF
			
			SET_BIT(iLocalBoolCheck35, LBOOL35_PLAYED_LOW_BODY_HEALTH_SOUND_FX)
			
		ENDIF
		
	ENDIF
		
ENDPROC

PROC PROCESS_HELICOPTER_ON_LOW_BODY_HEALTH(FMMC_VEHICLE_STATE &sVehState)

	FLOAT fCurrentHealthPercentage = MC_GET_VEHICLE_TOTAL_HEALTH_PERCENTAGE(sVehState.vehIndex, sVehState.iIndex, GET_TOTAL_STARTING_PLAYERS())
	
	FLOAT fPercentageThreshold = 20.0
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].sEngineSmoke.iEngineSmokeHealthPercentage != 0
		fPercentageThreshold = TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].sEngineSmoke.iEngineSmokeHealthPercentage)
	ENDIF
	
	IF fCurrentHealthPercentage < fPercentageThreshold
				
		PROCESS_HELICOPTER_SMOKE_FX_ON_LOW_BODY_HEALTH(sVehState)
	
		PROCESS_HELICOPTER_SOUND_FX_ON_LOW_BODY_HEALTH(sVehState)
		
	ENDIF
ENDPROC

PROC PROCESS_HACKING_DISTANCE_CAPTURE(FMMC_VEHICLE_STATE &sVehState)
		
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetFive, ciFMMC_VEHICLE5_HACKING_USE_DISTANCE_FOR_CAPTURE)
	OR MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] >= FMMC_MAX_RULES
		EXIT
	ENDIF
	
	IF MC_serverBD_4.ivehRule[sVehState.iIndex][MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_CAPTURE
	AND MC_serverBD_4.iVehPriority[sVehState.iIndex][MC_playerBD[iPartToUse].iteam] = MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]
		PROCESS_VEH_CAPTURE_RANGE(sVehState.iIndex)
	ELSE
		CLEAN_UP_VEH_CAPTURE_PROGRESS(sVehState.iIndex)
	ENDIF
ENDPROC

FUNC BOOL HAS_VEHICLE_RADIO_STATION_SEQUENCE_DELAY_FINISHED(FMMC_VEHICLE_STATE &sVehState)
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehRadioSequenceDelay = 0
		RETURN TRUE
	ENDIF
	
	IF NOT HAS_NET_TIMER_STARTED(tdVehicleRadioSequenceDelayTimer[sVehState.iIndex])
		PRINTLN("[LM][RadioSequence] PROCESS_VEHICLE_RADIO_STATION_SEQUENCE - Start Delay")	
		START_NET_TIMER(tdVehicleRadioSequenceDelayTimer[sVehState.iIndex])
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(tdVehicleRadioSequenceDelayTimer[sVehState.iIndex])
	AND HAS_NET_TIMER_EXPIRED_READ_ONLY(tdVehicleRadioSequenceDelayTimer[sVehState.iIndex], g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehRadioSequenceDelay*1000)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_VEHICLE_RADIO_STATION_SEQUENCE(FMMC_VEHICLE_STATE &sVehState)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].eVehRadioSequenceType = VEHICLE_RADIO_SEQUENCE_TYPE_NONE
		EXIT
	ENDIF
	
	IF IS_BIT_SET(iVehicleRadioSequenceBitset, sVehState.iIndex)
		EXIT
	ENDIF
	
	IF NOT IS_LOCAL_PLAYER_IN_FMMC_VEHICLE(sVehState)
		EXIT
	ENDIF
	
	IF IS_PED_GETTING_INTO_A_VEHICLE(localPlayerped)
		EXIT
	ENDIF
	
	IF NOT HAS_VEHICLE_RADIO_STATION_SEQUENCE_DELAY_FINISHED(sVehState)
		PRINTLN("[LM][RadioSequence] PROCESS_VEHICLE_RADIO_STATION_SEQUENCE - Delay has not finished.")
		EXIT
	ENDIF
	
	SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].eVehRadioSequenceType
		CASE VEHICLE_RADIO_SEQUENCE_TYPE_CASINO_CELEBRITY
			PRINTLN("[LM][RadioSequence] PROCESS_VEHICLE_RADIO_STATION_SEQUENCE - Trying to play special radio sequence... VEHICLE_RADIO_SEQUENCE_TYPE_CASINO_CELEBRITY")	
			
			FREEZE_RADIO_STATION("RADIO_23_DLC_XM19_RADIO")
			SET_RADIO_TRACK_WITH_START_OFFSET("RADIO_23_DLC_XM19_RADIO", "DLC_HEIST3_CRIME_PAYS", 120000)
			UNFREEZE_RADIO_STATION("RADIO_23_DLC_XM19_RADIO")
			SET_NEXT_RADIO_TRACK("RADIO_23_DLC_XM19_RADIO", "RADIO_TRACK_CAT_DJSOLO", "", "dlc_ch_finale_radio_djsolo")
			
			SET_RADIO_TO_STATION_NAME("RADIO_23_DLC_XM19_RADIO")
			
			IF sVehState.bHasControl
				SET_VEH_RADIO_STATION(sVehState.vehIndex, "RADIO_23_DLC_XM19_RADIO")
			ENDIF
			
			SET_BIT(iVehicleRadioSequenceBitset, sVehState.iIndex)
			
			PRINTLN("[LM][RadioSequence] PROCESS_VEHICLE_RADIO_STATION_SEQUENCE - (SET_RADIO_TRACK_WITH_START_OFFSET) - RadioStationName: RADIO_23_DLC_XM19_RADIO, TrackName: RADIO_TRACK_CAT_DJSOLO, Time: 120000")
			PRINTLN("[LM][RadioSequence] PROCESS_VEHICLE_RADIO_STATION_SEQUENCE - (SET_NEXT_RADIO_TRACK) - RadioStationName: RADIO_23_DLC_XM19_RADIO, category: RADIO_TRACK_CAT_DJSOLO, TrackIndex: dlc_heist_finale_custom_radio_djsolo ")
		BREAK		
	ENDSWITCH	
			
ENDPROC

PROC SET_DECORATOR_ON_MOC_TRAILER(FMMC_VEHICLE_STATE &sVehState)
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].mn = TRAILERLARGE
		IF (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciALLOW_MOC_ENTRY_THIS_MISSION)
			AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetSix, ciFMMC_VEHICLE6_ALLOW_MOC_ENTRY))
		AND NOT DECOR_EXIST_ON(sVehState.vehIndex, "Creator_Trailer")
			IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
				IF sVehState.bHasControl
					IF DECOR_IS_REGISTERED_AS_TYPE("Creator_Trailer", DECOR_TYPE_INT)
						IF NOT DECOR_EXIST_ON(sVehState.vehIndex, "Creator_Trailer")
							IF GB_GET_LOCAL_PLAYER_GANG_BOSS() != INVALID_PLAYER_INDEX()
								DECOR_SET_INT(sVehState.vehIndex, "Creator_Trailer", NETWORK_HASH_FROM_PLAYER_HANDLE(GB_GET_LOCAL_PLAYER_GANG_BOSS()))
								PRINTLN("[RCC MISSION] SET_DECORATOR_ON_MOC_TRAILER - setting ID decorator on created trailer to Creator_Trailer")
							ENDIF
						ENDIF
					ENDIF
				ELSE
					NETWORK_REQUEST_CONTROL_OF_ENTITY(sVehState.vehIndex)
					PRINTLN("[RCC MISSION] SET_DECORATOR_ON_MOC_TRAILER - Requesting control of MOC Trailer")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SET_DECORATOR_ON_AVENGER(FMMC_VEHICLE_STATE &sVehState)
	IF sVehState.mn = AVENGER
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciENABLE_AVENGER_INTERIOR_ENTRY)		
		AND NOT DECOR_EXIST_ON(sVehState.vehIndex, "Creator_Trailer")
			IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
				IF sVehState.bHasControl
					IF DECOR_IS_REGISTERED_AS_TYPE("Creator_Trailer", DECOR_TYPE_INT)
						IF NOT DECOR_EXIST_ON(sVehState.vehIndex, "Creator_Trailer")
							IF GB_GET_LOCAL_PLAYER_GANG_BOSS() != INVALID_PLAYER_INDEX()
								DECOR_SET_INT(sVehState.vehIndex, "Creator_Trailer", NETWORK_HASH_FROM_PLAYER_HANDLE(GB_GET_LOCAL_PLAYER_GANG_BOSS()))
								PRINTLN("[RCC MISSION] SET_DECORATOR_ON_MOC_TRAILER - setting ID decorator on created Avenger to Creator_Trailer")
							ENDIF
						ENDIF
					ENDIF
				ELSE
					NETWORK_REQUEST_CONTROL_OF_ENTITY(sVehState.vehIndex)
					PRINTLN("[RCC MISSION] SET_DECORATOR_ON_MOC_TRAILER - Requesting control of Avenger")
				ENDIF
			ELSE
				PRINTLN("[RCC MISSION] - SET_DECORATOR_ON_AVENGER Not a gang boss.")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC CLEAR_VEHICLE_DAMAGE_RECORD(FMMC_VEHICLE_STATE &sVehState)
	IF FMMC_IS_LONG_BIT_SET(iCleanVehicleDamageRecordBS, sVehState.iIndex)
		FMMC_CLEAR_LONG_BIT(iCleanVehicleDamageRecordBS, sVehState.iIndex)
		CLEAR_ENTITY_LAST_DAMAGE_ENTITY(sVehState.vehIndex)
		PRINTLN("[PROCESS_VEH_EVERY_FRAME_CLIENT] CLEAR_VEHICLE_DAMAGE_RECORD- Vehicle ", sVehState.iIndex, " - Clearing vehicle's damage record.")
	ENDIF
ENDPROC

PROC KEEP_FORCE_UNDER_RECOMMENDED_LIMIT(VECTOR& vForceToCheck)
	IF vForceToCheck.x > 150
		PRINTLN("[RCC MISSION] KEEP_FORCE_UNDER_RECOMMENDED_LIMIT - Capping X force to 145")
		vForceToCheck.x = 145
	ENDIF
	IF vForceToCheck.y > 150
		PRINTLN("[RCC MISSION] KEEP_FORCE_UNDER_RECOMMENDED_LIMIT - Capping Y force to 145")
		vForceToCheck.y = 145
	ENDIF
	IF vForceToCheck.z > 150
		PRINTLN("[RCC MISSION] KEEP_FORCE_UNDER_RECOMMENDED_LIMIT - Capping Z force to 145")
		vForceToCheck.z = 145
	ENDIF
ENDPROC

PROC CARGOBOB_EXPLODE_FORCE(VEHICLE_INDEX thisVeh, VEHICLE_INDEX viCarriedVeh)
	VECTOR vFwrd, vSide, vUp, v4, vOffset
	GET_ENTITY_MATRIX(thisVeh, vFwrd, vSide, vUp, v4)
	vOffset = vSide
	vOffset = vOffset * <<10,10,10>>	
	vSide = vSide * <<50,50,50>>
	vSide.z = 25
	KEEP_FORCE_UNDER_RECOMMENDED_LIMIT(vSide)
	vUp = vUp * <<-10, -10, -10>>
	PRINTLN("[RCC MISSION] CARGOBOB_EXPLODE_FORCE - Adding force to cargobob: ", vSide)
	APPLY_FORCE_TO_ENTITY(thisVeh, APPLY_TYPE_FORCE, vSide, vOffset, 0, TRUE, TRUE, TRUE)
	PRINTLN("[RCC MISSION] CARGOBOB_EXPLODE_FORCE - Adding force to viCarriedVeh: ", vUp)
	APPLY_FORCE_TO_ENTITY(viCarriedVeh, APPLY_TYPE_FORCE, vUp, v4, 0, TRUE, TRUE, TRUE)
ENDPROC

PROC PROCESS_ATTACH_ASSIGNED_CARGOBOB_VEHICLE_ENTITY(FMMC_VEHICLE_STATE &sVehState)
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iCargobobEntitySelectID > -1
		
		PRINTLN("[RCC MISSION] PROCESS_ATTACH_ASSIGNED_CARGOBOB_VEHICLE_ENTITY - Processing veh: ", sVehState.iIndex)

		IF IS_VEHICLE_MODEL_A_CARGOBOB(sVehState.mn)
			IF sVehState.bAlive
			AND sVehState.bDrivable
				IF NOT IS_BIT_SET(MC_serverBD.iCargobobDettachedBitset, sVehState.iIndex)
					BOOL bInControl = sVehState.bHasControl
					BOOL bHost = bIsLocalPlayerHost
					IF IS_BIT_SET(MC_serverBD.iCargobobShouldDetachBitset, sVehState.iIndex)
						//Needs called on all clones else they constantly reattach
						SET_CARGOBOB_FORCE_DONT_DETACH_VEHICLE(sVehState.vehIndex, FALSE)
					ELIF IS_BIT_SET(MC_serverBD.iCargobobAttachedBitset, sVehState.iIndex)
						SET_CARGOBOB_FORCE_DONT_DETACH_VEHICLE(sVehState.vehIndex, TRUE)
					ENDIF
					PRINTLN("[RCC MISSION] PROCESS_ATTACH_ASSIGNED_CARGOBOB_VEHICLE_ENTITY - bInControl: ", bInControl, ", bHost: ", bHost)
					IF bInControl
					OR bHost
						NETWORK_INDEX netEntityToAttach
						INT iIndexToAttach = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iCargobobEntitySelectID
						INT iEntityType = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iCargobobEntitySelectType
						PRINTLN("[RCC MISSION] PROCESS_ATTACH_ASSIGNED_CARGOBOB_VEHICLE_ENTITY - iEntityType: ", iEntityType, ", iIndexToAttach: ", iIndexToAttach)
						SWITCH iEntityType
							CASE ciRULE_TYPE_PED
								netEntityToAttach = MC_serverBD_1.sFMMC_SBD.niPed[iIndexToAttach]
							BREAK
							CASE ciRULE_TYPE_VEHICLE
								netEntityToAttach = MC_serverBD_1.sFMMC_SBD.niVehicle[iIndexToAttach]
							BREAK
							CASE ciRULE_TYPE_OBJECT
								netEntityToAttach = GET_OBJECT_NET_ID(iIndexToAttach)
							BREAK
							DEFAULT
								EXIT
						ENDSWITCH
						BOOL useMagnet = IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetFive, ciFMMC_VEHICLE5_CARGOBOB_PICKUP_USE_MAGNET)
						PICKUP_GADGET_TYPE pickupType = PICKUP_HOOK
						FLOAT pickupRopeLength = 4.0
						FLOAT pickupPositionOffset = -0.5
						IF useMagnet
							pickupType = PICKUP_MAGNET
							pickupRopeLength = 10.0
						ENDIF
						IF NETWORK_DOES_NETWORK_ID_EXIST(netEntityToAttach)
						
							ENTITY_INDEX entToAttach = NET_TO_ENT(netEntityToAttach)
							VEHICLE_INDEX vehToAttach
							IF iEntityType = ciRULE_TYPE_VEHICLE
								vehToAttach = NET_TO_VEH(netEntityToAttach)
							ENDIF
							IF DOES_ENTITY_EXIST(entToAttach)
								//Player in control stuff - detaching and attaching objects
								IF bInControl
									IF (useMagnet AND DOES_CARGOBOB_HAVE_PICKUP_MAGNET(sVehState.vehIndex))
									OR (NOT useMagnet AND DOES_CARGOBOB_HAVE_PICK_UP_ROPE(sVehState.vehIndex))
										IF NETWORK_HAS_CONTROL_OF_ENTITY(entToAttach)
											IF IS_ENTITY_ALIVE(entToAttach)
												OBJECT_INDEX oiMagnet
												IF useMagnet
													VECTOR tempLoc = GET_FMMC_VEHICLE_COORDS(sVehState)
													oiMagnet = GET_CLOSEST_OBJECT_OF_TYPE(tempLoc, 10.0, HEI_PROP_HEIST_MAGNET, false, false, false)
													SET_CARGOBOB_PICKUP_MAGNET_ACTIVE(sVehState.vehIndex, FALSE)
													SET_CARGOBOB_PICKUP_MAGNET_REDUCED_STRENGTH(sVehState.vehIndex, 0.0)
													SET_CARGOBOB_PICKUP_MAGNET_PULL_STRENGTH(sVehState.vehIndex, 0.0)
												ELSE
													VECTOR tempLoc = GET_ATTACHED_PICK_UP_HOOK_POSITION(sVehState.vehIndex)
													oiMagnet = GET_CLOSEST_OBJECT_OF_TYPE(tempLoc, 10.0, PROP_V_HOOK_S, false, false, false)
												ENDIF

												IF NOT IS_BIT_SET(MC_serverBD.iCargobobShouldDetachBitset, sVehState.iIndex)
													IF NOT IS_BIT_SET(MC_serverBD.iCargobobAttachedBitset, sVehState.iIndex)
														PRINTLN("[JS] PROCESS_ATTACH_ASSIGNED_CARGOBOB_VEHICLE_ENTITY - Attaching...")
														IF DOES_ENTITY_EXIST(oiMagnet)
															IF GET_WHATEVER_ATTACHED_TO_CARGOBOB(sVehState.vehIndex, iEntityType) != entToAttach
																IF GET_ENTITY_HEIGHT_ABOVE_GROUND(sVehState.vehIndex) > 8.0
																	PRINTLN("[JS] PROCESS_ATTACH_ASSIGNED_CARGOBOB_VEHICLE_ENTITY - Attaching attachment")
																	SET_CARGOBOB_PICKUP_MAGNET_ENSURE_PICKUP_ENTITY_UPRIGHT(sVehState.vehIndex, TRUE)
																	SET_ENTITY_NO_COLLISION_ENTITY(oiMagnet, entToAttach, FALSE)
																	SET_ENTITY_NO_COLLISION_ENTITY(sVehState.vehIndex, entToAttach, FALSE)
																	SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(entToAttach, FALSE)
																	SET_CARGOBOB_FORCE_DONT_DETACH_VEHICLE(sVehState.vehIndex, TRUE)
																	IF iEntityType = ciRULE_TYPE_VEHICLE
																		ATTACH_VEHICLE_TO_CARGOBOB(sVehState.vehIndex, vehToAttach, -1, <<0,0,pickupPositionOffset>>)
																	ELSE
																		ATTACH_ENTITY_TO_CARGOBOB(sVehState.vehIndex, entToAttach, -1, <<0,0,pickupPositionOffset>>)
																	ENDIF
																	FREEZE_ENTITY_POSITION(entToAttach, FALSE)
																ELSE
																	PRINTLN("[JS] PROCESS_ATTACH_ASSIGNED_CARGOBOB_VEHICLE_ENTITY - Else 1")
																ENDIF
															ELSE
																PRINTLN("[JS] PROCESS_ATTACH_ASSIGNED_CARGOBOB_VEHICLE_ENTITY - Else 2")
															ENDIF
														ELSE
															PRINTLN("[JS] PROCESS_ATTACH_ASSIGNED_CARGOBOB_VEHICLE_ENTITY - Else 3")
														ENDIF
													ELSE
														PRINTLN("[JS] PROCESS_ATTACH_ASSIGNED_CARGOBOB_VEHICLE_ENTITY - Else 4")
													ENDIF
												ELSE
													IF GET_WHATEVER_ATTACHED_TO_CARGOBOB(sVehState.vehIndex, iEntityType) = entToAttach
														PRINTLN("[JS] PROCESS_ATTACH_ASSIGNED_CARGOBOB_VEHICLE_ENTITY - Detaching attachment")
														IF iEntityType = ciRULE_TYPE_VEHICLE
															DETACH_VEHICLE_FROM_CARGOBOB(sVehState.vehIndex, vehToAttach)
														ELSE
															DETACH_ENTITY_FROM_CARGOBOB(sVehState.vehIndex, entToAttach)
														ENDIF
													ELSE
														PRINTLN("[JS] PROCESS_ATTACH_ASSIGNED_CARGOBOB_VEHICLE_ENTITY - Attached entity isn't the same")
													ENDIF
												ENDIF
											ELSE
												PRINTLN("[JS] PROCESS_ATTACH_ASSIGNED_CARGOBOB_VEHICLE_ENTITY - Attachment entity isn't alive")
											ENDIF
										ELSE
											PRINTLN("[JS] PROCESS_ATTACH_ASSIGNED_CARGOBOB_VEHICLE_ENTITY - Requesting control of attachment")
											NETWORK_REQUEST_CONTROL_OF_ENTITY(entToAttach)
										ENDIF
									ELSE
										IF IS_VEHICLE_MODEL_A_CARGOBOB(sVehState.mn)
											PRINTLN("[JS] PROCESS_ATTACH_ASSIGNED_CARGOBOB_VEHICLE_ENTITY - Creating rope")
											CREATE_PICK_UP_ROPE_FOR_CARGOBOB(sVehState.vehIndex, pickupType)
											SET_CARGOBOB_PICKUP_ROPE_TYPE(sVehState.vehIndex, pickupType)
											SET_PICKUP_ROPE_LENGTH_FOR_CARGOBOB(sVehState.vehIndex, pickupRopeLength, pickupRopeLength)
											SET_CARGOBOB_PICKUP_ROPE_DAMPING_MULTIPLIER(sVehState.vehIndex, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].fCargobobDamp)
										ENDIF
									ENDIF
								ENDIF
								//Host stuff - checking and updating ServerBD
								IF bHost
									IF IS_BIT_SET(MC_serverBD.iCargobobShouldDetachBitset, sVehState.iIndex)
										IF NOT IS_BIT_SET(MC_serverBD.iCargobobDettachedBitset, sVehState.iIndex)
											IF GET_WHATEVER_ATTACHED_TO_CARGOBOB(sVehState.vehIndex, iEntityType) != entToAttach
												PRINTLN("[JS] PROCESS_ATTACH_ASSIGNED_CARGOBOB_VEHICLE_ENTITY (SERVER) - Entity has been detached")
												SET_BIT(MC_serverBD.iCargobobDettachedBitset, sVehState.iIndex)
											ENDIF
										ENDIF
									ELSE
										IF NOT IS_BIT_SET(MC_serverBD.iCargobobAttachedBitset, sVehState.iIndex)
											IF GET_WHATEVER_ATTACHED_TO_CARGOBOB(sVehState.vehIndex, iEntityType) = entToAttach
												PRINTLN("[JS] PROCESS_ATTACH_ASSIGNED_CARGOBOB_VEHICLE_ENTITY (SERVER) - Entity has been attached")
												SET_BIT(MC_serverBD.iCargobobAttachedBitset, sVehState.iIndex)
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ELSE
								PRINTLN("[JS] PROCESS_ATTACH_ASSIGNED_CARGOBOB_VEHICLE_ENTITY - Attachment entity doesn't exist")
							ENDIF
						ELSE
							PRINTLN("[JS] PROCESS_ATTACH_ASSIGNED_CARGOBOB_VEHICLE_ENTITY - Attachment netId doesn't exist")
						ENDIF
					ENDIF
				ELSE
					PRINTLN("[RCC MISSION] PROCESS_ATTACH_ASSIGNED_CARGOBOB_VEHICLE_ENTITY - Dettached")
					SET_CARGOBOB_FORCE_DONT_DETACH_VEHICLE(sVehState.vehIndex, FALSE)
				ENDIF
			ELSE
				PRINTLN("[RCC MISSION] PROCESS_ATTACH_ASSIGNED_CARGOBOB_VEHICLE_ENTITY - Dead")
				ENTITY_INDEX eiTemp = GET_WHATEVER_ATTACHED_TO_CARGOBOB(sVehState.vehIndex, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iCargobobEntitySelectType)
				IF DOES_ENTITY_EXIST(eiTemp)
					SET_CARGOBOB_FORCE_DONT_DETACH_VEHICLE(sVehState.vehIndex, FALSE)
					IF NETWORK_HAS_CONTROL_OF_ENTITY(sVehState.vehIndex)
						IF NETWORK_HAS_CONTROL_OF_ENTITY(eiTemp)
							DETACH_ENTITY_FROM_CARGOBOB(sVehState.vehIndex, eiTemp)
							SET_ENTITY_NO_COLLISION_ENTITY(sVehState.vehIndex, eiTemp, TRUE)
							SET_ENTITY_LOAD_COLLISION_FLAG(eiTemp, TRUE)
							IF IS_ENTITY_DEAD(sVehState.vehIndex)
							AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iCargobobEntitySelectType = ciRULE_TYPE_VEHICLE
								VEHICLE_INDEX tempCarriedVeh = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(eiTemp)
								CARGOBOB_EXPLODE_FORCE(sVehState.vehIndex, tempCarriedVeh)
							ENDIF
							PRINTLN("[JS] PROCESS_ATTACH_ASSIGNED_CARGOBOB_VEHICLE_ENTITY - Force detach as cargobob is dead")
						ELSE
							NETWORK_REQUEST_CONTROL_OF_ENTITY(eiTemp)
							PRINTLN("[JS] PROCESS_ATTACH_ASSIGNED_CARGOBOB_VEHICLE_ENTITY - Requesting control of attachment (cargobob dead)")
						ENDIF
					ENDIF
				ELSE
					NETWORK_INDEX netEntityToAttach
					INT iIndexToAttach = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iCargobobEntitySelectID
					INT iEntityType = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iCargobobEntitySelectType
					SWITCH iEntityType
						CASE ciRULE_TYPE_PED
							netEntityToAttach = MC_serverBD_1.sFMMC_SBD.niPed[iIndexToAttach]
						BREAK
						CASE ciRULE_TYPE_VEHICLE
							netEntityToAttach = MC_serverBD_1.sFMMC_SBD.niVehicle[iIndexToAttach]
						BREAK
						CASE ciRULE_TYPE_OBJECT
							netEntityToAttach = GET_OBJECT_NET_ID(iIndexToAttach)
						BREAK
					ENDSWITCH
					IF NETWORK_DOES_NETWORK_ID_EXIST(netEntityToAttach)
						eiTemp = NET_TO_ENT(netEntityToAttach)
					ENDIF
				ENDIF
				IF DOES_ENTITY_EXIST(eiTemp)
					IF IS_BIT_SET(MC_serverBD.iCargobobAttachedBitset, sVehState.iIndex)
						IF NETWORK_HAS_CONTROL_OF_ENTITY(eiTemp)
							IF NOT IS_BIT_SET(iVehCargoCollisionDoneBS, sVehState.iIndex)
								IF IS_ENTITY_STATIC(eiTemp)
									IF NOT IS_ENTITY_WAITING_FOR_WORLD_COLLISION(eiTemp)
									AND NOT HAS_COLLISION_LOADED_AROUND_ENTITY(eiTemp)
										PRINTLN("[JS] PROCESS_ATTACH_ASSIGNED_CARGOBOB_VEHICLE_ENTITY - Force loading collision around cargo")
										SET_ENTITY_LOAD_COLLISION_FLAG(eiTemp, TRUE)
										SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(eiTemp, TRUE)
									ELSE
										ACTIVATE_PHYSICS(eiTemp)
										SET_ENTITY_DYNAMIC(eiTemp, TRUE)
										PRINTLN("[JS] PROCESS_ATTACH_ASSIGNED_CARGOBOB_VEHICLE_ENTITY - Activating physics")
									ENDIF
								ELSE
									IF HAS_COLLISION_LOADED_AROUND_ENTITY(eiTemp)
										IF NOT IS_ENTITY_IN_AIR(eiTemp)
											PRINTLN("[JS] PROCESS_ATTACH_ASSIGNED_CARGOBOB_VEHICLE_ENTITY - clear load collision around cargo")
											SET_ENTITY_LOAD_COLLISION_FLAG(eiTemp, FALSE)
											SET_BIT(iVehCargoCollisionDoneBS, sVehState.iIndex)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL HAS_PLAYER_VEHICLE_COLLIDED_WITH_COP_VEHICLE(VEHICLE_INDEX viPlayerVehicle)
	
	VECTOR vMin
	VECTOR vMax
	GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(viPlayerVehicle), vMin, vMax)
	VECTOR vMinOffset = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(viPlayerVehicle, <<0.0, vMin.y - 2.8, vMin.z - 1.2>>)
	VECTOR vMaxOffset = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(viPlayerVehicle, <<0.0, vMax.y + 2.8, vMax.z + 1.2>>)
	
	IF IS_PED_IN_VEHICLE(LocalPlayerPed, viPlayerVehicle, TRUE)
	AND GET_ENTITY_SPEED(viPlayerVehicle) > 1.0
	AND HAS_ENTITY_COLLIDED_WITH_ANYTHING(viPlayerVehicle)
	AND IS_COP_PED_IN_AREA_3D(vMinOffset, vMaxOffset)
		PRINTLN("HAS_PLAYER_VEHICLE_COLLIDED_WITH_COP_VEHICLE - Return TRUE")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_PLAYER_ENTERED_WANTED_BOUNDS_IN_VEHICLE(VEHICLE_INDEX viPlayerVehicle)
	
	IF IS_LOCAL_PLAYER_OUT_OF_BOUNDS()
	AND NOT IS_BIT_SET(iLocalBoolCheck3, LBOOL3_SAFE_FROM_WANTED_BOUNDS1)
	AND NOT IS_BIT_SET(iLocalBoolCheck3, LBOOL3_SAFE_FROM_WANTED_BOUNDS2)
		IF IS_PED_IN_VEHICLE(LocalPlayerPed, viPlayerVehicle, TRUE)
			PRINTLN("HAS_PLAYER_ENTERED_WANTED_BOUNDS_IN_VEHICLE - Return TRUE")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC 

FUNC BOOL HAS_PLAYER_DONE_SOMETHING_ILLEGAL_NEAR_COP_AND_VEHICLE(VEHICLE_INDEX ThisVeh)
	
	FLOAT fCopAreaRadius = 70.0
	FLOAT fVehicleAreaRadius = 80.0
	
	IF bLocalPlayerPedOk
		FLOAT fDistFromVehicle = GET_DISTANCE_BETWEEN_ENTITIES(LocalPlayerPed, ThisVeh)
		
		IF fDistFromVehicle <= fVehicleAreaRadius	
		AND IS_COP_PED_IN_AREA_3D(GET_ENTITY_COORDS(LocalPlayerPed) - << fCopAreaRadius, fCopAreaRadius, fCopAreaRadius >>, GET_ENTITY_COORDS(LocalPlayerPed) + << fCopAreaRadius, fCopAreaRadius, fCopAreaRadius >>)
			IF ((fDistFromVehicle <= 10.0) AND IS_WANTED_AND_HAS_BEEN_SEEN_BY_COPS(LocalPlayer)) //First check if the player is already wanted, seen, and near/in the vehicle.
			OR IS_PED_SHOOTING(LocalPlayerPed)
			OR HAS_PLAYER_VEHICLE_COLLIDED_WITH_COP_VEHICLE(ThisVeh)
			OR IS_BIT_SET(iCopDecoyBS, ci_COP_DECOY_ENTERED_AREA)
				PRINTLN("HAS_PLAYER_DONE_SOMETHING_ILLEGAL_NEAR_COP_AND_VEHICLE - Return TRUE")
				RETURN TRUE
			ENDIF
			
			//If the player remains stationary near cops then clear the vision block after a time so they can see them. url:bugstar:6158680
			IF NOT HAS_NET_TIMER_STARTED(tdBlockWantedConeResponseStationaryTimer)
				IF IS_PED_IN_VEHICLE(LocalPlayerPed, ThisVeh, FALSE)
				AND GET_ENTITY_SPEED(ThisVeh) < 22.0
					REINIT_NET_TIMER(tdBlockWantedConeResponseStationaryTimer)
					PRINTLN("HAS_PLAYER_DONE_SOMETHING_ILLEGAL_NEAR_COP_AND_VEHICLE - Vehicle is stationary near cops, starting timer!") 
				ENDIF
			ELIF HAS_NET_TIMER_EXPIRED(tdBlockWantedConeResponseStationaryTimer, 25000)
				PRINTLN("HAS_PLAYER_DONE_SOMETHING_ILLEGAL_NEAR_COP_AND_VEHICLE - Vehicle was stationary near cops for too long! Return TRUE")
				RETURN TRUE
			ELSE 
				IF NOT IS_PED_IN_VEHICLE(LocalPlayerPed, ThisVeh, FALSE)
				OR GET_ENTITY_SPEED(ThisVeh) >= 22.0
					PRINTLN("HAS_PLAYER_DONE_SOMETHING_ILLEGAL_NEAR_COP_AND_VEHICLE - Vehicle is no longer stationary near cops, clearing timer!")
					RESET_NET_TIMER(tdBlockWantedConeResponseStationaryTimer)
				ENDIF
			ENDIF
		ELSE
			IF HAS_NET_TIMER_STARTED(tdBlockWantedConeResponseStationaryTimer)
				PRINTLN("HAS_PLAYER_DONE_SOMETHING_ILLEGAL_NEAR_COP_AND_VEHICLE - Vehicle is no longer stationary near cops, clearing timer!")
				RESET_NET_TIMER(tdBlockWantedConeResponseStationaryTimer)
			ENDIF
		ENDIF
	ENDIF
	
	IF HAS_PLAYER_ENTERED_WANTED_BOUNDS_IN_VEHICLE(ThisVeh)
		PRINTLN("HAS_PLAYER_DONE_SOMETHING_ILLEGAL_NEAR_COP_AND_VEHICLE - Return TRUE")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_PLAYER_BLIP_IN_VEHICLE_WITH_BLOCK_WANTED_CONE_RESPONSE(FMMC_VEHICLE_STATE &sVehState)
	
	BLIP_INDEX bPlayerBlip = GET_MAIN_PLAYER_BLIP_ID()
	
	IF NOT IS_BIT_SET(MC_serverBD_1.iVehicleBlockedWantedConeResponseSeenIllegalBS, sVehState.iIndex)
		IF IS_LOCAL_PLAYER_IN_FMMC_VEHICLE(sVehState)
			IF NOT IS_BIT_SET(sMissionLocalContinuityVars.iGeneralLocalTrackingBitset, ciContinuityGenericTrackingBS_SwapVehicleUsed)
				PRINTLN("[JS][CONTINUITY] - SWAP VEHICLE - Setting swap vehicle as used")
				SET_BIT(sMissionLocalContinuityVars.iGeneralLocalTrackingBitset, ciContinuityGenericTrackingBS_SwapVehicleUsed)
			ENDIF
			IF NOT IS_BIT_SET(iLocalBoolCheck29, LBOOL29_SET_BLIP_IN_BLOCK_WANTED_CONE_RESPONSE_VEHICLE)
				IF GET_PLAYER_WANTED_LEVEL(LocalPlayer) > 0
					FORCE_START_HIDDEN_EVASION(LocalPlayer)
				ENDIF
				SET_CUSTOM_BLIP_COLOUR_FOR_PLAYER(LocalPlayer, BLIP_COLOUR_STEALTH_GREY, TRUE)
				SET_BIT(iLocalBoolCheck29, LBOOL29_SET_BLIP_IN_BLOCK_WANTED_CONE_RESPONSE_VEHICLE)
			ENDIF
			SET_PLAYER_BLIP_COLOUR(LocalPlayer, bPlayerBlip)
		ELSE
			IF IS_BIT_SET(iLocalBoolCheck29, LBOOL29_SET_BLIP_IN_BLOCK_WANTED_CONE_RESPONSE_VEHICLE)
				SET_CUSTOM_BLIP_COLOUR_FOR_PLAYER(LocalPlayer, BLIP_COLOUR_STEALTH_GREY, FALSE)
				SET_PLAYER_BLIP_COLOUR(LocalPlayer, bPlayerBlip)
				CLEAR_BIT(iLocalBoolCheck29, LBOOL29_SET_BLIP_IN_BLOCK_WANTED_CONE_RESPONSE_VEHICLE)
			ENDIF
		ENDIF
	ELSE
		IF IS_BIT_SET(iLocalBoolCheck29, LBOOL29_SET_BLIP_IN_BLOCK_WANTED_CONE_RESPONSE_VEHICLE)
			SET_CUSTOM_BLIP_COLOUR_FOR_PLAYER(LocalPlayer, BLIP_COLOUR_STEALTH_GREY, FALSE)
			SET_PLAYER_BLIP_COLOUR(LocalPlayer, bPlayerBlip)
			CLEAR_BIT(iLocalBoolCheck29, LBOOL29_SET_BLIP_IN_BLOCK_WANTED_CONE_RESPONSE_VEHICLE)
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_PLAYER_IN_VEHICLE_WANTED_CONE_RESPONSE(FMMC_VEHICLE_STATE &sVehState)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBlockWantedConeResponse = ci_VEHICLE_BLOCK_WANTED_CONE_RESPONSE_On
	OR g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBlockWantedConeResponse = ci_VEHICLE_BLOCK_WANTED_CONE_RESPONSE_UntilIllegal
		
		MAINTAIN_PLAYER_BLIP_IN_VEHICLE_WITH_BLOCK_WANTED_CONE_RESPONSE(sVehState)
		
		IF NOT IS_BIT_SET(MC_serverBD_1.iVehicleBlockedWantedConeResponseSeenIllegalBS, sVehState.iIndex)
			IF NOT IS_BIT_SET(MC_serverBD_1.iVehicleBlockedWantedConeResponseBS, sVehState.iIndex)
				IF sVehState.bHasControl
				AND sVehState.bAlive
					SET_DISABLE_WANTED_CONES_RESPONSE(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[sVehState.iIndex]), TRUE)
					BROADCAST_FMMC_BLOCK_WANTED_CONE_RESPONSE_EVENT(TRUE, FALSE, sVehState.iIndex, FALSE)
					PRINTLN("PROCESS_PLAYER_IN_VEHICLE_WANTED_CONE_RESPONSE - Setting vehicle: ", sVehState.iIndex, " to Block wanted cone response.")
				ENDIF
			ELIF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBlockWantedConeResponse = ci_VEHICLE_BLOCK_WANTED_CONE_RESPONSE_UntilIllegal
				IF HAS_PLAYER_DONE_SOMETHING_ILLEGAL_NEAR_COP_AND_VEHICLE(sVehState.vehIndex)
					//Switch off the blocking so the owner can unblock it below. 
					BROADCAST_FMMC_BLOCK_WANTED_CONE_RESPONSE_EVENT(FALSE, FALSE, sVehState.iIndex, TRUE)
				ENDIF
			ENDIF
		ELSE //Set Off for Seen Illegal
			IF IS_BIT_SET(MC_serverBD_1.iVehicleBlockedWantedConeResponseBS, sVehState.iIndex)
				IF sVehState.bHasControl
				AND sVehState.bAlive
					SET_DISABLE_WANTED_CONES_RESPONSE(sVehState.vehIndex, FALSE)
					BROADCAST_FMMC_BLOCK_WANTED_CONE_RESPONSE_EVENT(FALSE, TRUE, sVehState.iIndex, FALSE)
					PRINTLN("PROCESS_PLAYER_IN_VEHICLE_WANTED_CONE_RESPONSE - Setting vehicle: ", sVehState.iIndex, " to CLEAR Block wanted cone response.")
				ENDIF	
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_VEHICLE_BLIPS(FMMC_VEHICLE_STATE &sVehState)

	CHECK_SHOULD_VEHICLE_HAVE_CUSTOM_BLIP(sVehState)

	IF sVehState.bExists
		
		//Setting to flash the player's vehicle if they are not currently in it
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetSix, ciFMMC_VEHICLE6_FLASH_WHEN_NO_PLAYER_IN)
			INT mAlpha = GET_ENTITY_ALPHA(sVehState.vehIndex)
			IF NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed, TRUE)
			AND NOT IS_ANY_PLAYER_IN_VEHICLE(sVehState.vehIndex)
				IF mAlpha <= 70
					mAlpha = 255
				ELSE
					mAlpha = mAlpha-10
				ENDIF
				SET_ENTITY_ALPHA(sVehState.vehIndex, mAlpha, TRUE)
				SET_BLIP_FLASHES(biVehBlip[sVehState.iIndex], TRUE)
			ELSE
				IF mAlpha != 255
					SET_ENTITY_ALPHA(sVehState.vehIndex, 255, TRUE)
					SET_BLIP_FLASHES(biVehBlip[sVehState.iIndex], FALSE)
				ENDIF
			ENDIF
		ENDIF
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehicleFlashBlipRule != -1
		AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehicleFlashBlipRule < FMMC_MAX_RULES
		AND NOT IS_BIT_SET(iVehBlipFlashExpiredBS, sVehState.iIndex)
			BOOL bBlipExists = DOES_BLIP_EXIST(biVehBlip[sVehState.iIndex])
			IF NOT IS_BIT_SET(iVehBlipFlashActiveBS, sVehState.iIndex)
				INT iTeam
				FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
					IF MC_serverBD_4.iCurrentHighestPriority[iTeam] = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehicleFlashBlipRule
						IF bBlipExists
							SET_BLIP_FLASHES(biVehBlip[sVehState.iIndex], TRUE)
							SET_BIT(iVehBlipFlashActiveBS, sVehState.iIndex)
							PRINTLN("[Blips][Veh] PROCESS_VEHICLE_BLIPS - Starting flashing blip. Rule: ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehicleFlashBlipRule, " Duration: ", (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehicleFlashBlipDuration * 1000))
						ENDIF
					ENDIF
				ENDFOR
			ELSE
				IF HAS_NET_TIMER_STARTED(tdVehicleBlipFlash[sVehState.iIndex])
					IF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdVehicleBlipFlash[sVehState.iIndex], (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehicleFlashBlipDuration * 1000))
						IF bBlipExists
							SET_BLIP_FLASHES(biVehBlip[sVehState.iIndex], FALSE)
						ENDIF
						RESET_NET_TIMER(tdVehicleBlipFlash[sVehState.iIndex])
						SET_BIT(iVehBlipFlashExpiredBS, sVehState.iIndex)
						PRINTLN("[Blips][Veh] PROCESS_VEHICLE_BLIPS - Ending flashing blip")
					ENDIF
				ELSE
					REINIT_NET_TIMER(tdVehicleBlipFlash[sVehState.iIndex])
				ENDIF
			ENDIF
		ENDIF
		
		SET_VEHICLE_BLIP_SETTINGS_EVERY_FRAME(sVehState)

	ENDIF
ENDPROC

PROC PROCESS_VEHICLE_HUD(FMMC_VEHICLE_STATE &sVehState)

	INT iMyTeam = MC_playerBD[iPartToUse].iteam
	INT iTeam
	HUD_COLOURS eTeamColour
	
	IF sVehState.bExists
		
		//Setting to place a marker above the vehicle when the player is not in the vehicle we're currently looking at
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetFour, ciFMMC_VEHICLE4_VEHICLE_PLACE_CHEVRON)
			IF sVehState.bAlive
			AND NOT IS_LOCAL_PLAYER_IN_FMMC_VEHICLE(sVehState, TRUE)
				FM_EVENTDRAW_MARKER_ABOVE_VEHICLE(sVehState.vehIndex,
				g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iChevronColours[0],
				g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iChevronColours[1],
				g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iChevronColours[2])
			ENDIF			
		ENDIF		
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_BlockModShopOptions)
			IF sVehState.bAlive
				IF NOT IS_BIT_SET(iVehicleModShopOptionsBlockedBS, sVehState.iIndex)
					INT iModsLoop = 0
					FOR iModsLoop = 0 TO (NUMBER_OF_CAR_MODS - 1)
						IF iModsLoop != ENUM_TO_INT(CMM_MAIN)
						OR iModsLoop != ENUM_TO_INT(CMM_BUY)
						OR iModsLoop != ENUM_TO_INT(CMM_MOD)
						OR iModsLoop != ENUM_TO_INT(CMM_MAINTAIN)
							PRINTLN("PROCESS_VEHICLE_HUD Vehicle: ", sVehState.iIndex, "    Blocking car mod ", iModsLoop)
							BLOCK_CARMOD_MENU_OPTION_FOR_CREATOR_MISSION(sVehState.vehIndex, INT_TO_ENUM(CARMOD_MENU_ENUM, iModsLoop) , TRUE)
						ENDIF
					ENDFOR
					SET_BIT(iVehicleModShopOptionsBlockedBS, sVehState.iIndex)
				ENDIF
			ENDIF
		ENDIF		
		
		IF SHOULD_SHOW_TAGGED_TARGET()
			IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam] < FMMC_MAX_RULES
				IF IS_IT_SAFE_TO_ADD_TAG_BLIP(MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam])	
					INT iTagged
					FOR iTagged = 0 TO FMMC_MAX_TAGGED_ENTITIES-1
						IF MC_serverBD_3.iTaggedEntityType[iTagged] = ENUM_TO_INT(GET_ENTITY_TYPE(sVehState.vehIndex))
							IF MC_serverBD_3.iTaggedEntityIndex[iTagged] = sVehState.iIndex
								IF IS_ENTITY_ALIVE(sVehState.vehIndex)
									VECTOR vEntityCoords = GET_FMMC_VEHICLE_COORDS(sVehState)
									vEntityCoords.z = (vEntityCoords.z + 1.0)
									DRAW_TAGGED_MARKER(vEntityCoords, sVehState.vehIndex, ENUM_TO_INT(GET_ENTITY_TYPE(sVehState.vehIndex)), sVehState.iIndex)
									
									IF NOT DOES_BLIP_EXIST(biTaggedEntity[iTagged])
									AND NOT DOES_BLIP_EXIST(GET_BLIP_FROM_ENTITY(sVehState.vehIndex))
										ADD_TAGGED_BLIP(sVehState.vehIndex, ENUM_TO_INT(GET_ENTITY_TYPE(sVehState.vehIndex)), sVehState.iIndex, iTagged)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDFOR
				ENDIF
			ENDIF
		ENDIF
		
		
		IF MC_serverBD_4.iVehPriority[sVehState.iIndex][iMyTeam] <= MC_serverBD_4.iCurrentHighestPriority[iMyTeam]
		AND MC_serverBD_4.iVehPriority[sVehState.iIndex][iMyTeam] < FMMC_MAX_RULES
		AND MC_serverBD_4.ivehRule[sVehState.iIndex][iMyTeam] != FMMC_OBJECTIVE_LOGIC_NONE
			
			BOOL bSingular
			BOOL bIsLiteralString
			IF MC_serverBD.iNumStartingPlayers[iMyTeam] > 1
				bSingular = FALSE
			ELSE
				bSingular = TRUE
			ENDIF
			
			TEXT_LABEL_63 tl63CaptureBarText = "CON_TIME"
			IF IS_CUSTOM_STRING_LIST_STRING_VALID(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iCaptureBarCustomString)
				tl63CaptureBarText = GET_CUSTOM_STRING_LIST_TEXT_LABEL(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iCaptureBarCustomString)
				bIsLiteralString = TRUE
			ENDIF
			
			IF MC_serverBD_4.iVehRule[sVehState.iIndex][iMyTeam]= FMMC_OBJECTIVE_LOGIC_CAPTURE
			
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetFour, ciFMMC_VEHICLE4_VEHICLE_SHOW_CAPTURE_FOR_ALL_TEAMS)
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetFour, ciFMMC_VEHICLE4_VEHICLE_ALWAYS_SHOW_CAPTURE_BAR)
						IF bPlayerToUseOK
							IF NOT IS_SPECTATOR_HUD_HIDDEN()
							AND NOT IS_SPECTATOR_SHOWING_NEWS_HUD()
							AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iMyTeam].iRuleBitset[MC_serverBD_4.iVehPriority[sVehState.iIndex][iMyTeam]], ciBS_RULE_HIDE_HUD)
								// Show all teams vehicle capture bars
								FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
									eTeamColour = GET_HUD_COLOUR_FOR_FMMC_TEAM(iTeam,LocalPlayer)
									
									HUDORDER eHudOrder = INT_TO_ENUM(HUDORDER, ENUM_TO_INT(HUDORDER_SIXTHBOTTOM) - iTeam)
									
									IF iTeam = MC_playerBD[iPartToUse].iTeam
										eHudOrder = HUDORDER_SEVENTHBOTTOM
									ENDIF
									
									IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetFour, ciFMMC_VEHICLE4_VEHICLE_RETAIN_CAPTURE_PROGRESS)
										
										IF g_FMMC_STRUCT.iTeamNames[iTeam] != 0
											g_sMission_TeamName[iTeam] = GET_CREATOR_NAME_STRING_FOR_TEAM(iTeam , bSingular) 
											bIsLiteralString = TRUE
											PRINTLN("[RCC MISSION] [VEHICLE CAPTURE] - g_sMission_TeamName[iTeam] assigned") 
										ELSE
											SCRIPT_ASSERT("[RCC MISSION] - [VEHICLE CAPTURE] on heist or heist planning mission but team does not have creator name. Give an A class bug to *default online content creation*!, team: ")
										ENDIF
										
										IF MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(MC_serverBD_1.iControlVehTimer[sVehState.iIndex])
											IF MC_serverBD.iDriverPart[sVehState.iIndex] != -1
											AND iTeam = MC_playerBD[MC_serverBD.iDriverPart[sVehState.iIndex]].iTeam
												iStoredVehCaptureMS[iTeam][sVehState.iIndex] = (MC_GET_NET_TIMER_DIFFERENCE_WITH_TIME_STAMP(MC_serverBD_1.iControlVehTimer[sVehState.iIndex]))
											ENDIF
										ENDIF
										IF g_FMMC_STRUCT.iTeamNames[iTeam] != 0
											DRAW_GENERIC_METER(iStoredVehCaptureMS[iTeam][sVehState.iIndex], MC_serverBD.iVehTakeoverTime[iTeam][MC_serverBD_4.iVehPriority[sVehState.iIndex][iTeam]], g_sMission_TeamName[iTeam], eTeamColour, -1, eHudOrder, -1, -1, FALSE, TRUE, HUDFLASHING_NONE, 0, FALSE, 0, bIsLiteralString)
										ELSE
											DRAW_GENERIC_METER(iStoredVehCaptureMS[iTeam][sVehState.iIndex], MC_serverBD.iVehTakeoverTime[iTeam][MC_serverBD_4.iVehPriority[sVehState.iIndex][iTeam]], tl63CaptureBarText, eTeamColour, -1, eHudOrder, -1, -1, FALSE, TRUE, HUDFLASHING_NONE, 0, FALSE, 0, bIsLiteralString)
										ENDIF
									ELSE
										DRAW_GENERIC_METER(MC_GET_NET_TIMER_DIFFERENCE_WITH_TIME_STAMP(MC_serverBD_1.iControlVehTimer[sVehState.iIndex]),MC_serverBD.iVehTakeoverTime[iTeam][MC_serverBD_4.iVehPriority[sVehState.iIndex][iTeam]], tl63CaptureBarText, eTeamColour, -1, eHudOrder, -1, -1, FALSE, TRUE, HUDFLASHING_NONE, 0, FALSE, 0, bIsLiteralString)
									ENDIF
								ENDFOR
							ENDIF
						ENDIF
					ENDIF
				ELSE
				
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetFour, ciFMMC_VEHICLE4_VEHICLE_ALWAYS_SHOW_CAPTURE_BAR)
						IF NOT IS_SPECTATOR_HUD_HIDDEN()
						AND NOT IS_SPECTATOR_SHOWING_NEWS_HUD()
						AND NOT IS_OBJECTIVE_BLOCKED()
						AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iMyTeam].iRuleBitset[MC_serverBD_4.iVehPriority[sVehState.iIndex][iMyTeam]], ciBS_RULE_HIDE_HUD)
						AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetFive, ciFMMC_VEHICLE5_HACKING_DISABLE_CAPTURE_BAR)
							
							//Just show for your own team
							IF MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(MC_serverBD_1.iControlVehTimer[sVehState.iIndex])	
								DRAW_GENERIC_METER(MC_GET_NET_TIMER_DIFFERENCE_WITH_TIME_STAMP(MC_serverBD_1.iControlVehTimer[sVehState.iIndex]), MC_serverBD.iVehTakeoverTime[iMyTeam][MC_serverBD_4.iVehPriority[sVehState.iIndex][iMyTeam]], tl63CaptureBarText, HUD_COLOUR_GREEN, -1, HUDORDER_DONTCARE, -1, -1, FALSE, TRUE, HUDFLASHING_NONE, 0, FALSE, 0, bIsLiteralString)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF
	
	IF MC_ServerBD_4.iCurrentHighestPriority[iMyTeam] < FMMC_MAX_RULES
		
		PROCESS_VEHICLE_HEALTHBAR(sVehState)
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_SECUROSERV_HACKING)
			IF MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(MC_serverBD_1.iControlVehTimer[sVehState.iIndex])
				ASSERTLN("[FMMC_2020] - Securohack for vehicles should be refactored")
				PRINTLN("[FMMC_2020] - Securohack for vehicles should be refactored")
				IF MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam] < FMMC_MAX_RULES	
					IF g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iShowTeamHackingProgress[MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]] > -2
						INT iHackingPart = -1
						
						IF MC_playerBD[iPartToUse].iVehCapturing != sVehState.iIndex
						
							INT iPart = -1
							WHILE DO_PARTICIPANT_LOOP(iPart, DPLF_EXCLUDE_LOCAL_PART)
								IF DOES_TEAM_LIKE_TEAM(MC_playerBD[iPart].iTeam, MC_playerBD[iPartToUse].iTeam)
									IF MC_playerBD[iPart].iVehCapturing = sVehState.iIndex
										iHackingPart = iPart
										BREAKLOOP
									ENDIF
								ENDIF
							ENDWHILE
						ENDIF
						
						IF iHackingPart > -1
							INT iTeamLoop = 0
							FOR iTeamLoop = 0 TO MC_serverBD.iNumberOfTeams - 1
								IF MC_serverBD_4.iVehPriority[sVehState.iIndex][iTeamLoop] <= MC_serverBD_4.iCurrentHighestPriority[iMyTeam]
								AND MC_serverBD_4.iVehPriority[sVehState.iIndex][iTeamLoop] < FMMC_MAX_RULES
								AND MC_serverBD_4.ivehRule[sVehState.iIndex][iTeamLoop] = FMMC_OBJECTIVE_LOGIC_CAPTURE
								AND sVehState.iIndex != MC_playerBD[iLocalPart].iVehCapturing
									IF iTeamLoop = g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iShowTeamHackingProgress[MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]]
									OR g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iShowTeamHackingProgress[MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]] = -1	
										PRINTLN("[SECUROHACK] We are iPart: ", iPartToUse, " on Team: ", MC_PlayerBD[iParttoUse].iTeam, " and we want to see the bar for team: ", g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iShowTeamHackingProgress[MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]])
										TEXT_LABEL_15 tl15 = "HACK_PROG"
										INT iProgressMax = MC_serverBD.iVehTakeoverTime[iTeamLoop][MC_serverBD_4.iVehPriority[sVehState.iIndex][iTeamLoop]]
										INT iProgress = MC_GET_NET_TIMER_DIFFERENCE_WITH_TIME_STAMP(MC_serverBD_1.iControlVehTimer[sVehState.iIndex])
										DRAW_GENERIC_METER(iProgress, iProgressMax, tl15, HUD_COLOUR_WHITE, -1, INT_TO_ENUM(HUDORDER, ENUM_TO_INT(HUDORDER_SEVENTHBOTTOM)+iTeamLoop))
										
										//Don't keep looping
										BREAKLOOP
									ENDIF
								ENDIF
							ENDFOR
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF // If the current rule is valid
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciDISPLAY_HEALTH_ON_TEAM_VEHICLE)
		DISPLAY_TEAM_VEHICLE_HUD(1)
	ENDIF
	
ENDPROC

PROC PROCESS_VEH_TEAM_EVERY_FRAME_LOGIC_CLIENT(FMMC_VEHICLE_STATE &sVehState)

	INT iTeam, iDoor

	FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
		INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
		
		IF SHOULD_ENTITY_RESPAWN_THIS_RULE(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iRespawnOnRuleChangeBS, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iRespawnOnRule, iTeam)
			IF (IS_BIT_SET(iLocalBoolCheck16, LBOOL16_TEAM_0_IS_PAST_THEIR_FIRST_RULE + iTeam)
			AND IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam))
			OR IS_BIT_SET(MC_serverBD.iServerBitSet5, SBBOOL5_RETRY_CURRENT_RULE_TEAM_0 + iTeam)
				IF SHOULD_ENTITY_RESPAWN_THIS_RULE(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iRespawnOnRuleChangeBS, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iRespawnOnRule, iTeam, FALSE)
					CLEAR_BIT(iCountThisVehAsDestroyedBS, sVehState.iIndex)
					PRINTLN("[JS] Cleaning up iCountThisVehAsDestroyedBS  ", sVehState.iIndex)
					BREAKLOOP
				ENDIF
			ENDIF
		ENDIF
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehicleDoorSettingsOnRule != -1
		AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehicleDoorSettingsOnRule = iRule

			IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetEight, ciFMMC_VEHICLE8_VehicleDoorSettingsMidpoint)
			OR (IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetEight, ciFMMC_VEHICLE8_VehicleDoorSettingsMidpoint) AND IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iTeam], iRule))

				IF sVehState.bExists				
					IF NOT IS_BIT_SET(iVehicleDoorSetupSetToCreatorSettings, sVehState.iIndex)
						IF sVehState.bHasControl
							FOR iDoor = VEHICLE_DOORS_FRONT_LEFT TO VEHICLE_DOORS_TRUNC
								
								IF NOT DOES_VEHICLE_HAVE_DOOR(sVehState.vehIndex, INT_TO_ENUM(SC_DOOR_LIST, iDoor))
									RELOOP
								ENDIF
								
								IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetFour, ciFMMC_VEHICLE4_VEHICLE_DOOR_FRONT_LEFT_LOCKED + iDoor)
									PRINTLN("[PROCESS_VEHICLES_EVERY_FRAME] On the specified rule. Locking door ", iDoor, " on vehicle - ", sVehState.iIndex) 
									SET_VEHICLE_INDIVIDUAL_DOORS_LOCKED(sVehState.vehIndex, iDoor, VEHICLELOCK_LOCKED)
									
								ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetNine, ciFMMC_VEHICLE9_VEHICLE_DOOR_FRONT_LEFT_BREAKOFF + iDoor)
									PRINTLN("[PROCESS_VEHICLES_EVERY_FRAME] On the specified rule. Breaking door ", iDoor, " on vehicle - ", sVehState.iIndex) 
									SET_VEHICLE_DOOR_BROKEN(sVehState.vehIndex, INT_TO_ENUM(SC_DOOR_LIST, iDoor), FALSE)	
									
								ELSE
									PRINTLN("[PROCESS_VEHICLES_EVERY_FRAME] On the specified rule. Unlocking door ", iDoor, " on vehicle - ", sVehState.iIndex) 
									SET_VEHICLE_INDIVIDUAL_DOORS_LOCKED(sVehState.vehIndex, iDoor, VEHICLELOCK_UNLOCKED)
									
									IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitset, ciFMMC_VEHICLE_DOORS_FRONT_LEFT_OPEN + iDoor)
										SET_VEHICLE_DOOR_OPEN(sVehState.vehIndex, INT_TO_ENUM(SC_DOOR_LIST, iDoor), TRUE)
									ENDIF
									
								ENDIF
							ENDFOR
						ENDIF
						
						SET_BIT(iVehicleDoorSetupSetToCreatorSettings, sVehState.iIndex)
					ENDIF
				ENDIF
			ENDIF				
		ENDIF
		
		IF iFirstHighPriorityVehicleThisRule[iTeam] = -1
			IF MC_serverBD_4.iVehPriority[sVehState.iIndex][iTeam] <= MC_serverBD_4.iCurrentHighestPriority[iTeam]
				iFirstHighPriorityVehicleThisRule[iTeam] = sVehState.iIndex
			ENDIF
		ENDIF
		
	ENDFOR
	
ENDPROC

PROC PROCESS_VEH_TEAM_EVERY_FRAME_LOGIC_SERVER(FMMC_VEHICLE_STATE &sVehState)
	INT iTeam
	FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
	
		IF SHOULD_ENTITY_RESPAWN_THIS_RULE(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iRespawnOnRuleChangeBS, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iRespawnOnRule, iTeam)
			IF (IS_BIT_SET(iLocalBoolCheck16, LBOOL16_TEAM_0_IS_PAST_THEIR_FIRST_RULE + iTeam)
			AND IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam))
			OR IS_BIT_SET(MC_serverBD.iServerBitSet5, SBBOOL5_RETRY_CURRENT_RULE_TEAM_0 + iTeam)
				IF SHOULD_ENTITY_RESPAWN_THIS_RULE(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iRespawnOnRuleChangeBS, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iRespawnOnRule, iTeam, FALSE)
					
					IF sVehState.bExists
						SET_BIT(MC_serverBD.iVehCleanup_NeedOwnershipBS, sVehState.iIndex)
					ENDIF
					
					CLEAR_BIT(iCountThisVehAsDestroyedBS, sVehState.iIndex)
					IF NOT IS_VEHICLE_USING_RESPAWN_POOL(sVehState.iIndex)											
						PRINTLN("[JS] GET_VEHICLE_RESPAWNS: ", GET_VEHICLE_RESPAWNS(sVehState.iIndex), " sVehState.iIndex:  ", sVehState.iIndex , " iTeam = ", iTeam)											
						IF GET_VEHICLE_RESPAWNS(sVehState.iIndex) < 1
							SET_VEHICLE_RESPAWNS(sVehState.iIndex, 1)
						ENDIF
					ELSE										
						PRINTLN("[JS] IS_VEHICLE_USING_RESPAWN_POOL true for sVehState.iIndex:  ", sVehState.iIndex , " iTeam = ", iTeam)
					ENDIF
					
					PRINTLN("[JS] Setting to Clean up veh to respawn on rule change ", sVehState.iIndex , " iTeam = ", iTeam)
					BREAKLOOP
				ENDIF
			ENDIF
		ENDIF
		
		IF MC_serverBD_4.iVehPriority[sVehState.iIndex][iTeam] <= MC_serverBD_4.iCurrentHighestPriority[iTeam]
		AND IS_MODEL_VALID(sVehState.mn)
			MC_serverBD.iNumVehSeatsHighestPriority[iTeam] += GET_VEHICLE_MODEL_NUMBER_OF_SEATS(sVehState.mn)
		ENDIF
		
	ENDFOR
ENDPROC

PROC PROCESS_VEH_SELF_DESTRUCT_SEQUENCE(FMMC_VEHICLE_STATE &sVehState)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].sSelfDestructSettings.iRuleStart = -1
		EXIT
	ENDIF
	
	IF NOT sVehState.bExists
		IF HAS_NET_TIMER_STARTED(sVehSelfDestruct[sVehState.iIndex].tdTimer)
			CLEAN_UP_SELF_DESTRUCT_ENTITY(sVehSelfDestruct[sVehState.iIndex])
		ENDIF
		
		EXIT
	ENDIF
	
	BOOL bProcess			= SHOULD_ENTITY_PROCESS_SELF_DESTRUCT_SEQUENCE(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].sSelfDestructSettings, sVehSelfDestruct[sVehState.iIndex], MC_GET_VEHICLE_TOTAL_HEALTH_PERCENTAGE(sVehState.vehIndex, sVehState.iIndex, GET_TOTAL_STARTING_PLAYERS()))
	BOOL bProcessFizzle		= SHOULD_ENTITY_PROCESS_FIZZLE_SELF_DESTRUCT_SEQUENCE(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].sSelfDestructSettings, sVehSelfDestruct[sVehState.iIndex])
	
	#IF IS_DEBUG_BUILD
		IF bProcess
		OR bProcessFizzle
			IF sVehState.bExists
				PRINTLN("[LM][SELF_DESTRUCT] - Processing the Self Destruction of sVehState.iIndex: ", sVehState.iIndex, " SCRIPT_AUTOMOBILE_", NETWORK_ENTITY_GET_OBJECT_ID(sVehState.vehIndex), " --------------------")
			ELSE
				PRINTLN("[LM][SELF_DESTRUCT] - Processing the Self Destruction of sVehState.iIndex: ", sVehState.iIndex, " Entity Index Does Not Exist")
			ENDIF
		ENDIF
	#ENDIF
	
	IF bProcessFizzle
		PROCESS_ENTITY_FIZZLE_SELF_DESTRUCT_SEQUENCE(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].sSelfDestructSettings, sVehSelfDestruct[sVehState.iIndex])
	ELIF bProcess
		PROCESS_ENTITY_SELF_DESTRUCT_SEQUENCE(sVehState.vehIndex, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].sSelfDestructSettings, sVehSelfDestruct[sVehState.iIndex])
	ENDIF
ENDPROC

PROC PROCESS_VEHICLE_BEING_TOUCHED(FMMC_VEHICLE_STATE &sVehState)
	
	IF NOT IS_BIT_SET(iVehTrackTouchedByPlayerBS, sVehState.iIndex)
		EXIT
	ENDIF
	
	IF IS_BIT_SET(iVehHasBeenTouchedByPlayerBS, sVehState.iIndex)
		EXIT
	ENDIF
	
	INT iClosestPlayer = GET_NEAREST_PLAYER_TO_ENTITY(sVehState.vehIndex)
	PLAYER_INDEX ClosestPlayer = INT_TO_PLAYERINDEX(iClosestPlayer)
	IF IS_NET_PLAYER_OK(ClosestPlayer)
		PED_INDEX ClosestPlayerPed = GET_PLAYER_PED(ClosestPlayer)
		
		IF IS_PED_INJURED(ClosestPlayerPed)
			EXIT			
		ENDIF
		
		IF MC_IS_ENTITY_TOUCHING_ENTITY(sVehState.vehIndex, ClosestPlayerPed, 25.0)
			SET_BIT(iVehHasBeenTouchedByPlayerBS, sVehState.iIndex)
			PRINTLN("[Vehicles][Vehicle ", sVehState.iIndex, "] - PROCESS_VEHICLE_BEING_TOUCHED - Vehicle has been touched by the local players ped.")
			EXIT
		ENDIF
		
		IF NOT IS_PED_IN_ANY_VEHICLE(ClosestPlayerPed)
			EXIT
		ENDIF
		
		VEHICLE_INDEX viVeh = GET_VEHICLE_PED_IS_IN(ClosestPlayerPed)
		
		IF MC_IS_ENTITY_TOUCHING_ENTITY(sVehState.vehIndex, viVeh, 25.0)
			SET_BIT(iVehHasBeenTouchedByPlayerBS, sVehState.iIndex)			
			PRINTLN("[Vehicles][Vehicle ", sVehState.iIndex, "] - PROCESS_VEHICLE_BEING_TOUCHED - Vehicle has been touched by the local players vehicle.")
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_RESET_VEHICLES_ON_RULE_START(FMMC_VEHICLE_STATE &sVehState)
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetFour, ciFMMC_VEHICLE4_VEHICLE_RESPAWNS_AT_END_OF_RULE)
		EXIT
	ENDIF
	
	IF NOT sVehState.bDrivable
		EXIT
	ENDIF
	
	IF NOT DOES_ANY_TEAM_HAVE_NEW_PRIORITY_THIS_FRAME()
		EXIT
	ENDIF
	
	INT iTeam
	FOR iTeam = 0 TO (g_FMMC_STRUCT.iNumberOfTeams - 1)
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSix[GET_TEAM_CURRENT_RULE(iTeam)], ciBS_RULE6_RESPAWN_VEHICLES_EACH_RULE)
			PRINTLN("[Vehicles][Vehicle ", sVehState.iIndex, "] PROCESS_RESET_VEHICLES_ON_RULE_START | Resetting vehicle ", sVehState.iIndex, " now")
			SET_ENTITY_COORDS(sVehState.vehIndex, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].vPos)
			SET_ENTITY_HEADING(sVehState.vehIndex, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].fHead)
			SET_VEHICLE_FIXED(sVehState.vehIndex)
		ENDIF
	ENDFOR
ENDPROC

PROC PROCESS_VEHICLE_DOES_NOT_EXIST_CARGOBOB_ATTACHED_ENTITY(FMMC_VEHICLE_STATE &sVehState)
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iCargobobEntitySelectID > -1
		IF IS_BIT_SET(MC_serverBD.iCargobobAttachedBitset, sVehState.iIndex)
			NETWORK_INDEX netEntityToAttach
			INT iIndexToAttach = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iCargobobEntitySelectID
			INT iEntityType = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iCargobobEntitySelectType
			SWITCH iEntityType
				CASE ciRULE_TYPE_PED
					netEntityToAttach = MC_serverBD_1.sFMMC_SBD.niPed[iIndexToAttach]
				BREAK
				CASE ciRULE_TYPE_VEHICLE
					netEntityToAttach = MC_serverBD_1.sFMMC_SBD.niVehicle[iIndexToAttach]
				BREAK
				CASE ciRULE_TYPE_OBJECT
					netEntityToAttach = GET_OBJECT_NET_ID(iIndexToAttach)
				BREAK
			ENDSWITCH
			IF NETWORK_DOES_NETWORK_ID_EXIST(netEntityToAttach)
				ENTITY_INDEX eiTemp = NET_TO_ENT(netEntityToAttach)
			
				IF IS_ENTITY_ALIVE(eiTemp)
				AND NETWORK_HAS_CONTROL_OF_ENTITY(eiTemp)
				AND NOT IS_BIT_SET(iVehCargoCollisionDoneBS, sVehState.iIndex)
					IF IS_ENTITY_STATIC(eiTemp)
						IF NOT IS_ENTITY_WAITING_FOR_WORLD_COLLISION(eiTemp)
						AND NOT HAS_COLLISION_LOADED_AROUND_ENTITY(eiTemp)
							PRINTLN("[JS] PROCESS_VEH_EVERY_FRAME_CLIENT PROCESS_ATTACH_ASSIGNED_CARGOBOB_VEHICLE_ENTITY - Force loading collision around cargo 2")
							SET_ENTITY_LOAD_COLLISION_FLAG(eiTemp, TRUE)
							SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(eiTemp, TRUE)
						ELSE
							ACTIVATE_PHYSICS(eiTemp)
							SET_ENTITY_DYNAMIC(eiTemp, TRUE)
							PRINTLN("[JS] PROCESS_VEH_EVERY_FRAME_CLIENT PROCESS_ATTACH_ASSIGNED_CARGOBOB_VEHICLE_ENTITY - Activating physics 2")
						ENDIF
					ELSE
						IF HAS_COLLISION_LOADED_AROUND_ENTITY(eiTemp)
							IF NOT IS_ENTITY_IN_AIR(eiTemp)
								PRINTLN("[JS] PROCESS_VEH_EVERY_FRAME_CLIENT PROCESS_ATTACH_ASSIGNED_CARGOBOB_VEHICLE_ENTITY - clear load collision around cargo 2")
								SET_ENTITY_LOAD_COLLISION_FLAG(eiTemp, FALSE)
								SET_BIT(iVehCargoCollisionDoneBS, sVehState.iIndex)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_BLOCK_EXIT_VEHICLE_WHEN_MOVING(FMMC_VEHICLE_STATE &sVehState)
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetSix, ciFMMC_VEHICLE6_BLOCK_MOVING_EXIT)
		IF IS_LOCAL_PLAYER_IN_FMMC_VEHICLE(sVehState)
			IF IS_ENTITY_IN_AIR(sVehState.vehIndex)
			OR VMAG2(GET_ENTITY_VELOCITY(sVehState.vehIndex)) > POW(cfMIN_MOVING_VEH_SPEED, 2.0)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Setting to completely ignore a vehicle - this will mean the player cannot enter or will even recognise there's a vehicle there
PROC PROCESS_VEHICLE_NOT_RECOGNISED_BY_PLAYERS(FMMC_VEHICLE_STATE &sVehState)
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetSix, ciFMMC_VEHICLE6_VEHICLE_NOT_CONSIDERED_BY_PLAYERS)
		IF NOT IS_BIT_SET(iVehConsideredBS, sVehState.iIndex)
			SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(sVehState.vehIndex, FALSE)
			SET_BIT(iVehConsideredBS, sVehState.iIndex)
			PRINTLN("[JS] PROCESS_VEHICLE_NOT_RECOGNISED_BY_PLAYERS Setting not considered by player for veh: ", sVehState.iIndex)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL DOES_VEHICLE_HAVE_ENGINE_SMOKE_RULE_SET(FMMC_VEHICLE_STATE &sVehState)
	RETURN IS_RULE_INDEX_VALID(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].sEngineSmoke.iRuleStart)
ENDFUNC

FUNC BOOL IS_VEHICLE_WITHIN_RULE_RANGE_FOR_ENGINE_SMOKE(FMMC_VEHICLE_STATE &sVehState)
	IF NOT IS_RULE_INDEX_VALID(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].sEngineSmoke.iRuleStart)
		RETURN TRUE
	ENDIF
	
	IF NOT IS_TEAM_INDEX_VALID(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].sEngineSmoke.iTeam)
		RETURN TRUE
	ENDIF
	
	INT iRule = GET_TEAM_CURRENT_RULE(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].sEngineSmoke.iTeam)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].sEngineSmoke.iRuleStart > iRule
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC FLOAT GET_ENGINE_HEALTH_FOR_SMOKE_AMOUNT(FMMC_VEHICLE_STATE &sVehState)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].sEngineSmoke.iSmokeThickness != 0
		RETURN TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].sEngineSmoke.iSmokeThickness) * 50.0
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].sEngineSmoke.iEngineSmokeHealthPercentage = 0
		RETURN GET_FMMC_VEHICLE_CURRENT_BODY_HEALTH(sVehState)
	ENDIF
	
	RETURN 50.0
ENDFUNC

PROC PROCESS_SMOKE_ON_LOW_VEHICLE_BODY_HEALTH(FMMC_VEHICLE_STATE &sVehState)
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetEight, ciFMMC_VEHICLE8_ENGINE_SMOKE_ON_LOW_BODY_HEALTH)
		EXIT
	ENDIF
	
	IF DOES_VEHICLE_HAVE_ENGINE_SMOKE_RULE_SET(sVehState)
		IF IS_VEHICLE_WITHIN_RULE_RANGE_FOR_ENGINE_SMOKE(sVehState)
			IF sVehState.bExists
			AND sVehState.bHasControl
				SET_VEHICLE_ENGINE_HEALTH(sVehState.vehIndex, GET_ENGINE_HEALTH_FOR_SMOKE_AMOUNT(sVehState))
				PRINTLN("PROCESS_SMOKE_ON_LOW_VEHICLE_BODY_HEALTH - Engine health set to ", GET_ENGINE_HEALTH_FOR_SMOKE_AMOUNT(sVehState), " for vehicle ", sVehState.iIndex, " through rule")
			ENDIF
		ENDIF
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].sEngineSmoke.iEngineSmokeHealthPercentage = 0	
		IF sVehState.bExists
		AND sVehState.bHasControl
			FLOAT fEngineHealthForSmoke = GET_FMMC_VEHICLE_CURRENT_BODY_HEALTH(sVehState)
			
			IF NOT IS_THIS_MODEL_A_HELI(sVehState.mn)
			AND fEngineHealthForSmoke != GET_VEHICLE_ENGINE_HEALTH(sVehState.vehIndex)
				SET_VEHICLE_ENGINE_HEALTH(sVehState.vehIndex, fEngineHealthForSmoke)
				PRINTLN("PROCESS_SMOKE_ON_LOW_VEHICLE_BODY_HEALTH - Engine health set to ", fEngineHealthForSmoke, " for vehicle ", sVehState.iIndex)
			ENDIF
		ENDIF
	ELSE
	
		IF IS_THIS_MODEL_A_HELI(sVehState.mn)
			
			PROCESS_HELICOPTER_ON_LOW_BODY_HEALTH(sVehState)
			
		ELSE
		
			IF sVehState.bHasControl
				FLOAT fVehicleBodyHealthPercentage
				IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iHealthDamageCombinedEntityIndex > -1
					fVehicleBodyHealthPercentage = GET_CURRENT_HEALTH_PERCENTAGES_OF_VEHICLE_AND_LINKED_VEHICLES(sVehState.iIndex)
				ELSE
					fVehicleBodyHealthPercentage = MC_GET_VEHICLE_TOTAL_HEALTH_PERCENTAGE(sVehState.vehIndex, sVehState.iIndex, GET_TOTAL_STARTING_PLAYERS(), IS_VEHICLE_A_TRAILER(sVehState.vehIndex))
				ENDIF
				
				FLOAT fEngineSmokePercentage = TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].sEngineSmoke.iEngineSmokeHealthPercentage)
				
				IF fVehicleBodyHealthPercentage <= fEngineSmokePercentage
					SET_VEHICLE_ENGINE_HEALTH(sVehState.vehIndex, GET_ENGINE_HEALTH_FOR_SMOKE_AMOUNT(sVehState))
					PRINTLN("PROCESS_SMOKE_ON_LOW_VEHICLE_BODY_HEALTH - Engine health set to 50 to force it into smoking. We are below the Smoking threshold of: ", fEngineSmokePercentage, " Percent")
				ELSE
					PRINTLN("PROCESS_SMOKE_ON_LOW_VEHICLE_BODY_HEALTH - Not smoking yet. fEngineSmokePercentage: ", fEngineSmokePercentage, " fVehicleBodyHealthPercentage: ", fVehicleBodyHealthPercentage)
				ENDIF			
			ENDIF
			
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_DELAYED_VEHICLE_DOOR_OPENING(FMMC_VEHICLE_STATE &sVehState)
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetFive, ciFMMC_VEHICLE5_DELAYED_DOOR_OPEN)
	AND IS_BIT_SET(MC_serverBD.iOpenVehicleDoorsBitset, sVehState.iIndex)
		
		IF IS_ENTITY_CARGOBOB(sVehState.vehIndex) // Might need for some planes?
			PRINTLN("[LM] PROCESS_VEH_EVERY_FRAME_CLIENT - Preventing NetID from migrating as it is a cargobob and door opening ratios are not net synced.")
			SET_NETWORK_ID_CAN_MIGRATE(MC_serverBD_1.sFMMC_SBD.niVehicle[sVehState.iIndex], FALSE)
		ENDIF
		
		SET_VEHICLE_DOOR_OPEN(sVehState.vehIndex, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].eVehicleDelayedDoorWhichDoor)
		SET_OVERRIDE_VEHICLE_DOOR_TORQUE(sVehState.vehIndex, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].eVehicleDelayedDoorWhichDoor, TRUE)
	ENDIF
ENDPROC

PROC DESTROY_LINKED_VEHICLE(FMMC_VEHICLE_STATE &sVehState)
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iLinkedDestroyVeh != -1
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iLinkedDestroyVeh])
			IF IS_ENTITY_DEAD(NET_TO_ENT(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iLinkedDestroyVeh]))
				NETWORK_EXPLODE_VEHICLE(sVehState.vehIndex, TRUE, FALSE)
				PRINTLN("[JS] PROCESS_VEH_EVERY_FRAME_CLIENT Linked Vehicle: ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iLinkedDestroyVeh, " is dead exploding vehicle: ", sVehState.iIndex)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SET_SPAWN_VEHICLE_NEAR_LAST_PLAYER(FMMC_VEHICLE_STATE &sVehState)
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iSpawnNearType = ciSPAWN_NEAR_ENTITY_TYPE_LAST_PLAYER
	AND MC_PlayerBD[iLocalPart].iLastVeh != sVehState.iIndex
		IF IS_LOCAL_PLAYER_IN_FMMC_VEHICLE(sVehState, TRUE)
			MC_PlayerBD[iLocalPart].iLastVeh = sVehState.iIndex
			PRINTLN("[JS] PROCESS_VEH_EVERY_FRAME_CLIENT Setting iLastVeh to veh: ", sVehState.iIndex)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_VEHICLE_BRAKES_PERMANENTLY_ON(FMMC_VEHICLE_STATE &sVehState)
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetFive,  ciFMMC_VEHICLE5_PERMANENT_BRAKES_ON)
		SET_VEHICLE_BRAKE(sVehState.vehIndex, 5.0)
		SET_VEHICLE_HANDBRAKE(sVehState.vehIndex, TRUE)
		SET_VEHICLE_BRAKE_LIGHTS(sVehState.vehIndex, FALSE)
		
		#IF IS_DEBUG_BUILD
			IF (GET_FRAME_COUNT() % 15) = 0
				PRINTLN("[ciFMMC_VEHICLE5_PERMANENT_BRAKES_ON] PROCESS_VEH_EVERY_FRAME_CLIENT Maintaining Permanent Hand and Regular Brakes for sVehState.iIndex: ", sVehState.iIndex)
			ENDIF
		#ENDIF
	ENDIF
ENDPROC

PROC PROCESS_DISABLE_VEHICLE_SPACE_ROCKETS(FMMC_VEHICLE_STATE &sVehState)
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetSix, ciFMMC_VEHICLE6_DISABLE_SPACE_ROCKETS)
		IF bLocalPlayerPedOK
			IF IS_LOCAL_PLAYER_IN_FMMC_VEHICLE(sVehState, TRUE)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_NEXT_WEAPON)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_PREV_WEAPON)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_NEXT_WEAPON)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_PREV_WEAPON)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_SELECT_NEXT_WEAPON)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_SELECT_PREV_WEAPON)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_UD)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_LR)

				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM_IHELP_BUZ")
					HIDE_HELP_TEXT_THIS_FRAME()
				ENDIF
				
				WEAPON_TYPE Wep		
				GET_CURRENT_PED_VEHICLE_WEAPON(LocalPlayerPed, Wep)
				
				IF Wep = WEAPONTYPE_VEHICLE_SPACE_ROCKET
				OR Wep != WEAPONTYPE_VEHICLE_PLAYER_BUZZARD
				OR NOT IS_BIT_SET(iDisableVehicleWeaponBitset, sVehState.iIndex)
					DISABLE_VEHICLE_WEAPON(TRUE, WEAPONTYPE_VEHICLE_SPACE_ROCKET, sVehState.vehIndex, LocalPlayerPed)
					SET_CURRENT_PED_VEHICLE_WEAPON(LocalPlayerPed, WEAPONTYPE_VEHICLE_PLAYER_BUZZARD)
					SET_BIT(iDisableVehicleWeaponBitset, sVehState.iIndex)
				ENDIF
			ELSE
				IF IS_BIT_SET(iDisableVehicleWeaponBitset, sVehState.iIndex)
					CLEAR_BIT(iDisableVehicleWeaponBitset, sVehState.iIndex)
				ENDIF
			ENDIF
		ELSE
			IF IS_BIT_SET(iDisableVehicleWeaponBitset, sVehState.iIndex)
				CLEAR_BIT(iDisableVehicleWeaponBitset, sVehState.iIndex)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_DISABLE_VEHICLE_WEAPONS(FMMC_VEHICLE_STATE &sVehState)
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetSix, ciFMMC_VEHICLE6_DISABLE_VEHICLE_WEAPONS)
		IF IS_LOCAL_PLAYER_IN_FMMC_VEHICLE(sVehState)
			IF NOT IS_BIT_SET(iDisableVehicleWeaponBitset, sVehState.iIndex)
				DISABLE_VEHICLE_WEAPONS(sVehState.vehIndex)
				
				SET_CURRENT_PED_VEHICLE_WEAPON(LocalPlayerPed, WEAPONTYPE_UNARMED)
				SET_CURRENT_PED_WEAPON(LocalPlayerPed, WEAPONTYPE_UNARMED, TRUE)
				
				SET_BIT(iDisableVehicleWeaponBitset, sVehState.iIndex)
			ENDIF
		ELSE
			CLEAR_BIT(iDisableVehicleWeaponBitset, sVehState.iIndex)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetFive,  ciFMMC_VEHICLE5_DISABLE_VEHICLE_WEAPONS)
	OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciDISABLE_SPECIAL_VEHICLE_WEAPONS)
		DISABLE_SPECIAL_VEHICLE_WEAPONS(sVehState.vehIndex)
	ENDIF
	
	PROCESS_DISABLE_VEHICLE_SPACE_ROCKETS(sVehState)
ENDPROC

PROC PROCESS_VEHICLE_DOORS(FMMC_VEHICLE_STATE &sVehState)
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetSeven, ciFMMC_VEHICLE7_KEEP_DOORS_OPEN)
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitSet, enum_to_int(SC_DOOR_FRONT_LEFT))
			SET_VEHICLE_DOOR_CONTROL(sVehState.vehIndex, SC_DOOR_FRONT_LEFT, DT_DOOR_NO_RESET, 1.0)
		ENDIF
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitSet, enum_to_int(SC_DOOR_FRONT_RIGHT))
			SET_VEHICLE_DOOR_CONTROL(sVehState.vehIndex, SC_DOOR_FRONT_RIGHT, DT_DOOR_NO_RESET, 1.0)
		ENDIF
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitSet, enum_to_int(SC_DOOR_REAR_LEFT))
			SET_VEHICLE_DOOR_CONTROL(sVehState.vehIndex, SC_DOOR_REAR_LEFT, DT_DOOR_NO_RESET, 1.0)
		ENDIF
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitSet, enum_to_int(SC_DOOR_REAR_RIGHT))
			SET_VEHICLE_DOOR_CONTROL(sVehState.vehIndex, SC_DOOR_REAR_RIGHT, DT_DOOR_NO_RESET, 1.0)
		ENDIF
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitSet, enum_to_int(SC_DOOR_BONNET))
			SET_VEHICLE_DOOR_CONTROL(sVehState.vehIndex, SC_DOOR_BONNET, DT_DOOR_NO_RESET, 1.0)
		ENDIF
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitSet, enum_to_int(SC_DOOR_BOOT))
			IF GET_VEHICLE_DOOR_ANGLE_RATIO(sVehState.vehIndex, SC_DOOR_BOOT) < 0.95
				SET_VEHICLE_DOOR_OPEN(sVehState.vehIndex, SC_DOOR_BOOT, FALSE, TRUE)
			ENDIF
			
			SET_VEHICLE_DOOR_CONTROL(sVehState.vehIndex, SC_DOOR_BOOT, DT_DOOR_NO_RESET, 1.0)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitSet, ciFMMC_VEHICLE_ChangeBootWhenDropOff)
		
		BOOL bVehBootOpen = FALSE
		INT iTeamLoop
		
		FOR iTeamLoop = 0 TO (MC_serverBD.iNumberOfTeams - 1)
			IF MC_serverBD_4.iCurrentHighestPriority[iTeamLoop] < FMMC_MAX_RULES
			AND (g_FMMC_STRUCT.sFMMCEndConditions[iTeamLoop].iDropOffType[MC_serverBD_4.iCurrentHighestPriority[iTeamLoop]] = ciFMMC_DROP_OFF_TYPE_VEHICLE)
			AND (g_FMMC_STRUCT.sFMMCEndConditions[iTeamLoop].iDropOff_Vehicle[MC_serverBD_4.iCurrentHighestPriority[iTeamLoop]] = sVehState.iIndex)
			AND IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iTeamLoop],MC_serverBD_4.iCurrentHighestPriority[iTeamLoop]) //At the objective midpoint (ie the package/whatever has been collected and is ready for delivery)
				bVehBootOpen = TRUE
				BREAKLOOP
			ENDIF
		ENDFOR
		
		IF bVehBootOpen
			IF GET_VEHICLE_DOOR_ANGLE_RATIO(sVehState.vehIndex, SC_DOOR_BOOT) < 0.95
				SET_VEHICLE_DOOR_OPEN(sVehState.vehIndex, SC_DOOR_BOOT)
			ENDIF
		ELSE
			IF GET_VEHICLE_DOOR_ANGLE_RATIO(sVehState.vehIndex, SC_DOOR_BOOT) > 0.05
				SET_VEHICLE_DOOR_SHUT(sVehState.vehIndex, SC_DOOR_BOOT,FALSE)
			ENDIF
		ENDIF
		
	ENDIF
ENDPROC

PROC PROCESS_EXPLODING_VEHICLE(FMMC_VEHICLE_STATE &sVehState)
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetEight, ciFMMC_VEHICLE8_EXPLODE_IN_WATER)
		IF IS_ENTITY_SUBMERGED_IN_WATER(sVehState.vehIndex, DEFAULT, 0.3)
			NETWORK_EXPLODE_VEHICLE(sVehState.vehIndex, TRUE, FALSE)
			PRINTLN("PROCESS_EXPLODING_VEHICLE - Exploding vehicle as it is submerged in water with option set.")
		ENDIF	
	ENDIF
	
	EXPLODE_VEHICLE_AT_ZERO_HEALTH(sVehState.iIndex,sVehState.vehIndex)
		
	EXPLODE_VEHICLE_AT_ZERO_BODY_HEALTH(sVehState.iIndex,sVehState.vehIndex)
ENDPROC

PROC PROCESS_VEHICLE_INVINCIBILITY(FMMC_VEHICLE_STATE &sVehState)
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iInvincibleOnRules >= 0
		INT currentRule = 1 + MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iInvincibleOnRulesTeam]
		IF currentRule < FMMC_MAX_RULES
			BOOL bInvincible = IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iInvincibleOnRules, currentRule)			
			SET_ENTITY_INVINCIBLE(sVehState.vehIndex, bInvincible)
			SET_ENTITY_PROOFS(sVehState.vehIndex, bInvincible, bInvincible, bInvincible, bInvincible, bInvincible, bInvincible)
			
			PRINTLN("[Vehicle ", sVehState.iIndex, "] - PROCESS_VEHICLE_INVINCIBILITY - Setting vehicle to be invincible.")
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SERVER_VEHICLE_CARGO_DAMAGE(FMMC_VEHICLE_STATE &sVehState)
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciCARGO_SUFFERS_COLLISION_DAMAGE)
		FLOAT fVehHealth, fParentHealth 
		VEHICLE_INDEX parentVeh
			
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iAttachParent >= 0
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iAttachParent])
				parentVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iAttachParent])
			ENDIF
			fVehHealth = GET_FMMC_VEHICLE_CURRENT_BODY_HEALTH(sVehState)
			fParentHealth = GET_VEHICLE_BODY_HEALTH(parentVeh)
			
			IF fVehHealth != fParentHealth
				IF sVehState.bHasControl
					PRINTLN("[RCC MISSION] [PROCESS_VEH_EVERY_FRAME_CLIENT][RUIN] setting Vehicle health to ", fParentHealth, " from ", fVehHealth)
					SET_VEHICLE_BODY_HEALTH(sVehState.vehIndex, fParentHealth)
					SET_ENTITY_HEALTH(sVehState.vehIndex, CEIL(fParentHealth))
					
					PRINTLN("[RCC MISSION] [PROCESS_VEH_EVERY_FRAME_CLIENT][RUIN] Cargo vehicle health: ", GET_FMMC_VEHICLE_CURRENT_BODY_HEALTH(sVehState))
				ELSE
					PRINTLN("[RCC MISSION] [PROCESS_VEH_EVERY_FRAME_CLIENT][RUIN] Requesting control")
					NETWORK_REQUEST_CONTROL_OF_ENTITY(sVehState.vehIndex)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_EXPLODING_CARGO(FMMC_VEHICLE_STATE &sVehState)

	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iCargo != ciVEHICLE_CARGO_SERVER_BOMB
		//This type of cargo can't explode
		EXIT
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehCargoExplodeTeam = -1
	OR g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehCargoExplodeRule = -1
		EXIT
	ENDIF
	
	IF MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehCargoExplodeTeam] >= FMMC_MAX_RULES
		EXIT
	ENDIF
	
	IF IS_BIT_SET(iVehicleCargoExplodedBS, sVehState.iIndex)
		EXIT
	ENDIF
	
	IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicleCrate[sVehState.iIndex])
		EXIT
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(NET_TO_ENT(MC_serverBD_1.sFMMC_SBD.niVehicleCrate[sVehState.iIndex]))

		IF MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehCargoExplodeTeam] = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehCargoExplodeRule
			IF NOT IS_BIT_SET(iVehicleCargoBeepPlayedBS, sVehState.iIndex)
				PLAY_SOUND_FROM_ENTITY(-1, "Bomb_Arm", NET_TO_ENT(MC_serverBD_1.sFMMC_SBD.niVehicleCrate[sVehState.iIndex]), "DLC_MPSUM2_ULP2_Rogue_Drones", TRUE, 20)
				SET_BIT(iVehicleCargoBeepPlayedBS, sVehState.iIndex)
			ENDIF
			
			INT iObjTimeRemaining = GET_OBJECTIVE_TIMER_TIME_REMAINING(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehCargoExplodeTeam, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehCargoExplodeRule)
			
			IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehCargoBeepTime = -1
			OR iObjTimeRemaining < (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehCargoBeepTime * 1000)
				IF iObjTimeRemaining % 1000 <= CEIL(GET_FRAME_TIME() * 1000)
					PLAY_SOUND_FROM_ENTITY(-1, "Bomb_Countdown_Beep", NET_TO_ENT(MC_serverBD_1.sFMMC_SBD.niVehicleCrate[sVehState.iIndex]), "DLC_MPSUM2_ULP2_Rogue_Drones", TRUE, 20)
				ENDIF
			ENDIF
		ENDIF
		
		IF MC_serverBD.iReasonForObjEnd[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehCargoExplodeTeam] != OBJ_END_REASON_TIME_EXPIRED
			IF MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehCargoExplodeTeam] > g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehCargoExplodeRule
			AND NOT IS_BIT_SET(iVehicleCargoDefusePlayedBS, sVehState.iIndex)
				SET_BIT(iVehicleCargoDefusePlayedBS, sVehState.iIndex)
				PLAY_SOUND_FROM_ENTITY(-1, "Bomb_Defuse", NET_TO_ENT(MC_serverBD_1.sFMMC_SBD.niVehicleCrate[sVehState.iIndex]), "DLC_MPSUM2_ULP2_Rogue_Drones", TRUE, 20)
			ENDIF
		
			EXIT
		ENDIF
		
		IF MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehCargoExplodeTeam] < g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehCargoExplodeRule
			EXIT
		ENDIF
	
	ENDIF
	
	IF NOT NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicleCrate[sVehState.iIndex])
		PRINTLN("[Vehicles][Vehicle ",sVehState.iIndex,"] PROCESS_EXPLODING_CARGO - Requesting control of cargo.")
		NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicleCrate[sVehState.iIndex])
		EXIT
	ENDIF
	
	PRINTLN("[Vehicles][Vehicle ",sVehState.iIndex,"] PROCESS_EXPLODING_CARGO - Exploding cargo.")
	PLAY_SOUND_FROM_ENTITY(-1, "Bomb_Detonate", NET_TO_ENT(MC_serverBD_1.sFMMC_SBD.niVehicleCrate[sVehState.iIndex]), "DLC_MPSUM2_ULP2_Rogue_Drones", TRUE, 20)
	ADD_EXPLOSION(GET_ENTITY_COORDS(NET_TO_ENT(MC_serverBD_1.sFMMC_SBD.niVehicleCrate[sVehState.iIndex])), EXP_TAG_PETROL_PUMP)
	SET_ENTITY_HEALTH(NET_TO_ENT(MC_serverBD_1.sFMMC_SBD.niVehicleCrate[sVehState.iIndex]), 0)
	SET_BIT(iVehicleCargoExplodedBS, sVehState.iIndex)
	
ENDPROC

PROC PROCESS_VEHICLE_EXTRA_EMP_EFFECTS(FMMC_VEHICLE_STATE &sVehState)
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetTwo, ciFMMC_VEHICLE2_EXTRA_EMP)
		UPDATE_LIGHTS_ON_ENTITY(NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niVehicleCrate[sVehState.iIndex]))
		IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetTwo, ciFMMC_VEHICLE2_TriggerAudioOnVehicle) 
			IF sVehState.bAlive
				PLAY_SOUND_FROM_ENTITY(-1, "EMP_Vehicle_Hum", sVehState.vehIndex, "DLC_HEIST_BIOLAB_DELIVER_EMP_SOUNDS")
				SET_BIT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetTwo, ciFMMC_VEHICLE2_TriggerAudioOnVehicle) 
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_DISABLE_DRIVING_VEHICLE_ON_REMOTE_RESERVE(FMMC_VEHICLE_STATE &sVehState)
	// disable being able to move vehicle when another player has reserved the vehicle
	IF MC_serverBD.iVehRequestPart[sVehState.iIndex] != -1 
	AND MC_serverBD.iVehRequestPart[sVehState.iIndex] != iPartToUse
		IF IS_LOCAL_PLAYER_IN_FMMC_VEHICLE(sVehState, TRUE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_BRAKE)
			PRINTLN("PROCESS_VEH_EVERY_FRAME_CLIENT - Disabling ped ability to move veh because of server data")
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_DISABLE_RADIO_CONTROLS()
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_NEXT_RADIO)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PREV_RADIO)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_NEXT_RADIO_TRACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PREV_RADIO_TRACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_RADIO_WHEEL)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_RADIO_WHEEL_LR)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_RADIO_WHEEL_UD)
ENDPROC

PROC PROCESS_DISABLE_RADIO_CONTROL_WHEN_IN_VEHICLE(FMMC_VEHICLE_STATE &sVehState)

	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetNine, ciFMMC_VEHICLE9_DisableRadioControl)
		EXIT
	ENDIF
	
	IF NOT IS_PED_IN_VEHICLE(PlayerPedToUse, sVehState.vehIndex)
		EXIT
	ENDIF
	
	PROCESS_DISABLE_RADIO_CONTROLS()
ENDPROC

PROC PROCESS_VEHICLE_LOUD_RADIO(FMMC_VEHICLE_STATE &sVehState)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetTen, ciFMMC_VEHICLE10_EnableLoudRadio)
		EXIT
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("MP_POSITIONED_RADIO_MUTE_SCENE")
		SET_AUDIO_SCENE_VARIABLE("MP_POSITIONED_RADIO_MUTE_SCENE", "apply", 0)
	ENDIF
	ADD_ENTITY_TO_AUDIO_MIX_GROUP(sVehState.vehIndex, "MP_Scripted_Mixgroup_Focus_Entity")
	
	IF NOT sVehState.bHasControl
		EXIT
	ENDIF
		
	IF NOT GET_IS_VEHICLE_ENGINE_RUNNING(sVehState.vehIndex)
		SET_VEHICLE_ENGINE_ON(sVehState.vehIndex, TRUE, TRUE)
	ENDIF
	
	SET_VEHICLE_RADIO_ENABLED(sVehState.vehIndex, TRUE)
	SET_VEH_FORCED_RADIO_THIS_FRAME(sVehState.vehIndex)
	SET_VEHICLE_RADIO_LOUD(sVehState.vehIndex, TRUE)
	
ENDPROC

PROC PROCESS_FORCE_VEHICLE_RADIO_STATION(FMMC_VEHICLE_STATE &sVehState)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehRadio = -1
		EXIT
	ENDIF
	
	IF NOT sVehState.bHasControl
		EXIT
	ENDIF
	
	SET_VEHICLE_RADIO_ENABLED(sVehState.vehIndex, TRUE)
	SET_VEH_FORCED_RADIO_THIS_FRAME(sVehState.vehIndex)
	SET_VEH_RADIO_STATION(sVehState.vehIndex, GET_RADIO_STATION_NAME_FOR_EMITTER(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehRadio))
	IF IS_PED_IN_VEHICLE(localPlayerPed, sVehState.vehIndex)
		SET_RADIO_TO_STATION_NAME(GET_RADIO_STATION_NAME_FOR_EMITTER(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehRadio))
	ENDIF

ENDPROC

PROC PROCESS_VEHICLE_RADIO(FMMC_VEHICLE_STATE &sVehState)
	
	PROCESS_FORCE_VEHICLE_RADIO_STATION(sVehState)
	
	PROCESS_DISABLE_RADIO_CONTROL_WHEN_IN_VEHICLE(sVehState)
	
	PROCESS_VEHICLE_LOUD_RADIO(sVehState)
	
ENDPROC

PROC SET_VEHICLE_AUDIO_STREAM_STATE(VEHICLE_AUDIO_STREAM_STATE eNewState)
	PRINTLN("SET_VEHICLE_AUDIO_STREAM_STATE - Setting state from ", eVehicleAudioStreamState," to ", eNewState) 
	eVehicleAudioStreamState = eNewState
ENDPROC

FUNC BOOL LOAD_AUDIO_STREAM_FOR_VEHICLE(FMMC_VEHICLE_STATE &sVehState)
	
	SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehAudioStream
		CASE ciFMMC_VEH_AUDIO_STREAM_DATA_LEAK_LOWRIDER RETURN LOAD_STREAM("Music_Steam_NoFade", "DLC_Security_Data_Leak_3_Hood_Pass_Sounds")
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

PROC PROCESS_VEHICLE_AUDIO_STREAM_CLEANUP(FMMC_VEHICLE_STATE &sVehState)
	SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehAudioStream
		CASE ciFMMC_VEH_AUDIO_STREAM_DATA_LEAK_LOWRIDER
			PRINTLN("PROCESS_VEHICLE_AUDIO_STREAM_CLEANUP - Cleaning up ciFMMC_VEH_AUDIO_STREAM_DATA_LEAK_LOWRIDER")
			IF IS_AUDIO_SCENE_ACTIVE("DLC_Fixer_DL3_Hood_Pass_Scene")
				STOP_AUDIO_SCENE("DLC_Fixer_DL3_Hood_Pass_Scene")
			ENDIF
		BREAK
	ENDSWITCH	
ENDPROC

PROC PROCESS_SPECIFIC_STREAM_FUNCTIONALITY(FMMC_VEHICLE_STATE &sVehState)
	
	SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehAudioStream
		
		CASE ciFMMC_VEH_AUDIO_STREAM_DATA_LEAK_LOWRIDER
			PRINTLN("PROCESS_SPECIFIC_STREAM_FUNCTIONALITY - processing ciFMMC_VEH_AUDIO_STREAM_DATA_LEAK_LOWRIDER")
			PED_INDEX piVehDriver 			
			piVehDriver = GET_PED_IN_VEHICLE_SEAT(sVehState.vehIndex)
			
			IF DOES_ENTITY_EXIST(piVehDriver)
			AND IS_PED_A_PLAYER(piVehDriver)
				STOP_STREAM()
				IF g_bSCL_RadioNativeSwapBack
					SET_CUSTOM_RADIO_TRACK_LIST("RADIO_36_AUDIOPLAYER", "FIXER_AP_LOWRIDERS_MIX", TRUE)
				ELSE
					FORCE_MUSIC_TRACK_LIST("RADIO_36_AUDIOPLAYER", "FIXER_AP_LOWRIDERS_MIX", 0)
				ENDIF
	  			SET_RADIO_TO_STATION_NAME("RADIO_36_AUDIOPLAYER")
				IF sVehState.bHasControl
					SET_VEHICLE_RADIO_ENABLED(sVehState.vehIndex, TRUE)
					PLAY_SOUND_FROM_ENTITY(-1, "Music_Stream_Stop", sVehState.vehIndex, "DLC_Security_Data_Leak_3_Hood_Pass_Sounds", TRUE, 20)
					SET_VEH_RADIO_STATION(sVehState.vehIndex, "RADIO_36_AUDIOPLAYER")
					SET_VEH_FORCED_RADIO_THIS_FRAME(sVehState.vehIndex)
				ENDIF
				PROCESS_VEHICLE_AUDIO_STREAM_CLEANUP(sVehState)
				SET_VEHICLE_AUDIO_STREAM_STATE(VASS_END)
			ENDIF	
			
		BREAK
		
		DEFAULT 
			IF IS_LOCAL_PLAYER_IN_FMMC_VEHICLE(sVehState)
				PROCESS_DISABLE_RADIO_CONTROLS()
				IF sVehState.bHasControl
				AND IS_VEHICLE_RADIO_ON(sVehState.vehIndex)
					SET_VEH_RADIO_STATION(sVehState.vehIndex, "OFF")
					SET_VEH_FORCED_RADIO_THIS_FRAME(sVehState.vehIndex)
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL IS_VEHICLE_READY_TO_PROCESS_AUDIO_STREAM(FMMC_VEHICLE_STATE &sVehState)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iAudioStartTeam != -1
	AND GET_TEAM_CURRENT_RULE(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iAudioStartTeam) < g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iAudioStartRule
		PRINTLN("IS_VEHICLE_READY_TO_PROCESS_AUDIO_STREAM - Not at starting rule yet!")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC PROCESS_VEHICLE_AUDIO_STREAM(FMMC_VEHICLE_STATE &sVehState)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehAudioStream = ciFMMC_VEH_AUDIO_STREAM_OFF
		EXIT
	ENDIF
	
	SWITCH eVehicleAudioStreamState
		CASE VASS_WAIT
			IF IS_VEHICLE_READY_TO_PROCESS_AUDIO_STREAM(sVehState)
				SET_VEHICLE_AUDIO_STREAM_STATE(VASS_LOAD)
			ENDIF
		BREAK
		CASE VASS_LOAD
			IF LOAD_AUDIO_STREAM_FOR_VEHICLE(sVehState)
				ADD_ENTITY_TO_AUDIO_MIX_GROUP(sVehState.vehIndex, "MP_Scripted_Mixgroup_Focus_Entity")
				IF IS_AUDIO_SCENE_ACTIVE("MP_POSITIONED_RADIO_MUTE_SCENE")
					SET_AUDIO_SCENE_VARIABLE("MP_POSITIONED_RADIO_MUTE_SCENE", "apply", 0)
				ENDIF
				SET_VEHICLE_AUDIO_STREAM_STATE(VASS_START)
			ENDIF
		BREAK
		CASE VASS_START
			IF NOT IS_STREAM_PLAYING()
				PLAY_STREAM_FROM_VEHICLE(sVehState.vehIndex)
				
				IF sVehState.bHasControl
					SET_VEHICLE_RADIO_ENABLED(sVehState.vehIndex, FALSE)
				ENDIF
			ELSE
				SET_VEHICLE_AUDIO_STREAM_STATE(VASS_PLAYING)
			ENDIF
		BREAK
		CASE VASS_PLAYING
			
			IF IS_STREAM_PLAYING()
				
				PROCESS_SPECIFIC_STREAM_FUNCTIONALITY(sVehState)
				
				EXIT
				
			ENDIF
			
			IF NOT HAS_NET_TIMER_STARTED(tdVehicleAudioStreamTimer)
				REINIT_NET_TIMER(tdVehicleAudioStreamTimer)
			ELIF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdVehicleAudioStreamTimer, ciVehicleAudioStreamDelay)
				RESET_NET_TIMER(tdVehicleAudioStreamTimer)
				IF IS_LOCAL_PLAYER_IN_FMMC_VEHICLE(sVehState)
					PROCESS_VEHICLE_AUDIO_STREAM_CLEANUP(sVehState)
					SET_VEHICLE_AUDIO_STREAM_STATE(VASS_END)
				ELSE
					SET_VEHICLE_AUDIO_STREAM_STATE(VASS_LOAD)
				ENDIF
			ENDIF
		BREAK
		
		CASE VASS_END
			
		BREAK
		
	ENDSWITCH
	
ENDPROC

PROC PROCESS_VEHICLE_NOT_ALIVE_CLEANUP(FMMC_VEHICLE_STATE &sVehState)
	IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxHeliDamageSmoke1)
		STOP_PARTICLE_FX_LOOPED(ptfxHeliDamageSmoke1)
	ENDIF
	IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxHeliDamageSmoke2)
		STOP_PARTICLE_FX_LOOPED(ptfxHeliDamageSmoke2)
	ENDIF
	CLEAR_BIT(iDisableVehicleWeaponBitset, sVehState.iIndex)
ENDPROC

PROC PROCESS_VEHICLE_NOT_DRIVABLE(FMMC_VEHICLE_STATE &sVehState)
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetSix, ciFMMC_VEHICLE6_DISABLE_BREAKING)
		SET_VEHICLE_CAN_BREAK(sVehState.vehIndex, TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetEight, ciFMMC_VEHICLE8_EXPLODE_IN_WATER)
		IF IS_ENTITY_IN_WATER(sVehState.vehIndex)
			NETWORK_EXPLODE_VEHICLE(sVehState.vehIndex, TRUE, TRUE)
			PRINTLN("PROCESS_VEH_EVERY_FRAME_CLIENT - Exploding vehicle as it is in water with option set.")
		ENDIF	
	ENDIF
	CLEAR_BIT(iDisableVehicleWeaponBitset, sVehState.iIndex)
ENDPROC

#IF IS_DEBUG_BUILD
PROC PROCESS_VEHICLE_DEBUG(FMMC_VEHICLE_STATE &sVehState)
	IF bEnableDamageTrackerOnNetworkID
	AND NOT IS_DAMAGE_TRACKER_ACTIVE_ON_NETWORK_ID(sVehState.niIndex)
		ACTIVATE_DAMAGE_TRACKER_ON_NETWORK_ID(sVehState.niIndex, TRUE)
		PRINTLN("[RCC MISSION] PROCESS_VEH_EVERY_FRAME_CLIENT ACTIVATE_DAMAGE_TRACKER_ON_NETWORK_ID on vehicle: ", sVehState.iIndex)
	ENDIF
ENDPROC
#ENDIF

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Vehicle Objective Logic Client
// ##### Description: Functions for setting objective data for vehicles
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL IS_LOCAL_PLAYER_IN_THIS_OBJECTIVE_VEHICLE_INTERIOR(FMMC_VEHICLE_STATE &sVehState)
	
	IF sVehState.mn = AVENGER
	AND IS_BIT_SET(iLocalBoolCheck30, LBOOL30_PRIORITY_VEHICLE_IS_AVENGER)	
	AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(LocalPlayer)
		IF IS_PLAYER_IN_CREATOR_AIRCRAFT(LocalPlayer)
			MC_playerBD[iLocalPart].iVehNear = sVehState.iIndex
			PRINTLN("[Vehicles][Vehicle ", sVehState.iIndex, "] - IS_LOCAL_PLAYER_IN_THIS_OBJECTIVE_VEHICLE_INTERIOR - Vehicle is an Avenger, and it is a priority vehicle. We are inside Aircraft")
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetSix, ciFMMC_VEHICLE6_ALLOW_MOC_ENTRY)
	AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciALLOW_MOC_ENTRY_THIS_MISSION)	
	AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(LocalPlayer)		
		IF IS_PLAYER_IN_CREATOR_TRAILER(LocalPlayer)
			PRINTLN("[Vehicles][Vehicle ", sVehState.iIndex, "] - IS_LOCAL_PLAYER_IN_THIS_OBJECTIVE_VEHICLE_INTERIOR - Vehicle is marked as an MOC, and we are in a creator trailer")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

//FMMC2020 - Vehicle objective logic that needs to be moved, but some of the data (iVehNear) is cleared every frame
PROC PROCESS_VEHICLE_OBJECTIVE_LOGIC_CLIENT(FMMC_VEHICLE_STATE &sVehState)
	
	INT iMyTeam = GET_LOCAL_PLAYER_TEAM()
	
	IF MC_serverBD_4.iVehPriority[sVehState.iIndex][iMyTeam] > MC_serverBD_4.iCurrentHighestPriority[iMyTeam]
		EXIT
	ENDIF
	
	IF MC_serverBD_4.iVehPriority[sVehState.iIndex][iMyTeam] >= FMMC_MAX_RULES
		EXIT
	ENDIF
	
	IF MC_serverBD_4.ivehRule[sVehState.iIndex][iMyTeam] = FMMC_OBJECTIVE_LOGIC_NONE
		EXIT
	ENDIF
	
	IF bPlayerToUseOK
		INT iMultiRuleNo
		IF bPedToUseOk
			IF ((MC_serverBD_4.ivehRule[sVehState.iIndex][iMyTeam] = FMMC_OBJECTIVE_LOGIC_PHOTO) OR (MC_playerBD[iPartToUse].iVehNear = sVehState.iIndex OR MC_playerBD[iPartToUse].iVehNear = -1))
			AND iSpectatorTarget = -1
				IF MC_serverBD_4.ivehRule[sVehState.iIndex][iMyTeam] != FMMC_OBJECTIVE_LOGIC_PHOTO
					IF MC_playerBD[iPartToUse].iVehNear = sVehState.iIndex
						CLEAR_BIT(iVehPhotoChecksBS, sVehState.iIndex)
						RESET_PHOTO_DATA(sVehState.iIndex)
						MC_playerBD[iLocalPart].iVehNear = -1
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciENABLE_AVENGER_INTERIOR_ENTRY)
				AND sVehState.mn = AVENGER
				AND (MC_serverBD_4.ivehRule[sVehState.iIndex][iMyTeam] = FMMC_OBJECTIVE_LOGIC_GO_TO OR MC_serverBD_4.ivehRule[sVehState.iIndex][iMyTeam] = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER)
					SET_BIT(iLocalBoolCheck30, LBOOL30_PRIORITY_VEHICLE_IS_AVENGER)
					PRINTLN("[Vehicles][Vehicle ", sVehState.iIndex, "] PROCESS_VEHICLE_OBJECTIVE_LOGIC_CLIENT - Setting LBOOL30_PRIORITY_VEHICLE_IS_AVENGER")
				ENDIF
				
				SWITCH MC_serverBD_4.ivehRule[sVehState.iIndex][iMyTeam]
				
					CASE FMMC_OBJECTIVE_LOGIC_PHOTO
					
						IF MC_playerBD[iLocalPart].iVehPhoto = -1
							PROCESS_VEHICLE_PHOTO_LOGIC(sVehState.iIndex, sVehState.vehIndex)
						#IF IS_DEBUG_BUILD
						ELSE
							PRINTLN("[RCC MISSION] [AW PHOTO] PROCESS_VEH_EVERY_FRAME_CLIENT Unable to process photo logic, MC_playerBD[iLocalPart].iVehPhoto = ",MC_playerBD[iLocalPart].iVehPhoto)
						#ENDIF
						ENDIF
					
					BREAK
					
					CASE FMMC_OBJECTIVE_LOGIC_GO_TO
						IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iGotoRange != 0
							iMultiRuleNo = GET_ENTITY_MULTIRULE_NUMBER(sVehState.iIndex, MC_playerBD[iLocalPart].iteam,ci_TARGET_VEHICLE)
							
							IF (iMultiRuleNo != -1)
							AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetTwo, ciFMMC_VEHICLE2_IgnoreProximityPrimaryRule + iMultiRuleNo)
								IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iMyTeam].iRuleBitsetFive[MC_ServerBD_4.iCurrentHighestPriority[iMyTeam]], ciBS_RULE5_IGNORE_GOTO_RADIUS)
									PRINTLN("[RCC MISSION] PROCESS_VEH_EVERY_FRAME_CLIENT - ciBS_RULE5_IGNORE_GOTO_RADIUS is set - 3")
									IF sVehState.bDrivable
										IF IS_LOCAL_PLAYER_IN_FMMC_VEHICLE(sVehState)
											PRINTLN("[RCC MISSION] PROCESS_VEH_EVERY_FRAME_CLIENT - ciBS_RULE5_IGNORE_GOTO_RADIUS - IN VEHICLE")
											MC_playerBD[iLocalPart].iVehNear = sVehState.iIndex
										ENDIF
									ENDIF
								ELSE
									IF GET_DISTANCE_BETWEEN_ENTITIES(LocalPlayerPed,sVehState.vehIndex) <= g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iGotoRange
										MC_playerBD[iLocalPart].iVehNear = sVehState.iIndex
										
										#IF IS_DEBUG_BUILD
											IF bInVehicleDebug
												PRINTLN("[RCC MISSION] PROCESS_VEH_EVERY_FRAME_CLIENT AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH (2) Setting MC_playerBD[iPartToUse].iVehNear =  ", MC_playerBD[iPartToUse].iVehNear, " for part ",iPartToUse, " who is player ",  GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPartToUse))))
											ENDIF
										#ENDIF
									ENDIF
								ENDIF
							ELSE
								#IF IS_DEBUG_BUILD
									IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iMyTeam].iRuleBitsetFive[MC_ServerBD_4.iCurrentHighestPriority[iMyTeam]], ciBS_RULE5_IGNORE_GOTO_RADIUS)
										PRINTLN("[RCC MISSION] PROCESS_VEH_EVERY_FRAME_CLIENT - ciBS_RULE5_IGNORE_GOTO_RADIUS is set - 2")
									ENDIF
								#ENDIF
							ENDIF
						ENDIF
					
					BREAK
					
					CASE FMMC_OBJECTIVE_LOGIC_LEAVE_ENTITY
						PRINTLN("[RCC MISSION] PROCESS_VEH_EVERY_FRAME_CLIENT - FMMC_OBJECTIVE_LOGIC_LEAVE_ENTITY checking")
					
						IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iGotoRange != 0
							iMultiRuleNo = GET_ENTITY_MULTIRULE_NUMBER(sVehState.iIndex, MC_playerBD[iLocalPart].iteam, ci_TARGET_VEHICLE)
							
							IF (iMultiRuleNo != -1)
							AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetTwo, ciFMMC_VEHICLE2_IgnoreProximityPrimaryRule + iMultiRuleNo)
								IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iMyTeam].iRuleBitsetFive[MC_ServerBD_4.iCurrentHighestPriority[iMyTeam]], ciBS_RULE5_IGNORE_GOTO_RADIUS)
									PRINTLN("[RCC MISSION] PROCESS_VEH_EVERY_FRAME_CLIENT - ciBS_RULE5_IGNORE_GOTO_RADIUS is set - 3")
									IF sVehState.bDrivable
										IF IS_LOCAL_PLAYER_IN_FMMC_VEHICLE(sVehState)
											PRINTLN("[RCC MISSION] PROCESS_VEH_EVERY_FRAME_CLIENT - ciBS_RULE5_IGNORE_GOTO_RADIUS - IN VEHICLE")
											CLEAR_BIT(MC_playerBD[iLocalPart].iVehLeftBS, sVehState.iIndex)
										ELSE
											PRINTLN("[RCC MISSION] PROCESS_VEH_EVERY_FRAME_CLIENT - ciBS_RULE5_IGNORE_GOTO_RADIUS - NOT IN VEHICLE")
											SET_BIT(MC_playerBD[iLocalPart].iVehLeftBS, sVehState.iIndex)
										ENDIF
									ENDIF
								ELSE
									IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iGotoRange != 0
									AND iSpectatorTarget = -1									
										FLOAT fDist
										FLOAT fRange
										fDist = VDIST2(GET_ENTITY_COORDS(sVehState.vehIndex, FALSE), GET_ENTITY_COORDS(LocalPlayerPed, FALSE))
										fRange = POW(TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iGotoRange), 2.0)
										
										IF fDist <= fRange
											PRINTLN("[RCC MISSION] PROCESS_VEH_EVERY_FRAME_CLIENT - Range CLOSE TO VEHICLE")
											CLEAR_BIT(MC_playerBD[iLocalPart].iVehLeftBS, sVehState.iIndex)
										ELSE
											PRINTLN("[RCC MISSION] PROCESS_VEH_EVERY_FRAME_CLIENT - Range NOT CLOSE TO VEHICLE")
											SET_BIT(MC_playerBD[iLocalPart].iVehLeftBS, sVehState.iIndex)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ELSE
							SET_BIT(MC_playerBD[iLocalPart].iVehLeftBS, sVehState.iIndex)
						ENDIF
					BREAK
				
				ENDSWITCH

			ENDIF
			
			BOOL bIsPedInVehicleInterior
			bIsPedInVehicleInterior = IS_LOCAL_PLAYER_IN_THIS_OBJECTIVE_VEHICLE_INTERIOR(sVehState)
			
			IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			OR bIsPedInVehicleInterior
				#IF IS_DEBUG_BUILD
					IF bInVehicleDebug
						PRINTLN("[RCC MISSION] PROCESS_VEH_EVERY_FRAME_CLIENT - AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH local player is in a vehicle... ")
					ENDIF
				#ENDIF
				
				VEHICLE_INDEX tempVeh, viTrailer, viWinch
				tempVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
				GET_VEHICLE_TRAILER_VEHICLE(tempVeh, viTrailer)
				GET_CURRENT_WINCHED_VEHICLE(tempVeh, viWinch)
				
				IF bIsPedInVehicleInterior
					tempVeh = sVehState.vehIndex
				ENDIF
				
				IF tempVeh = sVehState.vehIndex
				OR viTrailer = sVehState.vehIndex
				OR viWinch = sVehState.vehIndex
				
					#IF IS_DEBUG_BUILD
						IF bInVehicleDebug
							PRINTLN("[RCC MISSION] PROCESS_VEH_EVERY_FRAME_CLIENT - AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH vehicle matches ")
						ENDIF
					#ENDIF
					
					IF MC_serverBD_4.iVehPriority[sVehState.iIndex][iMyTeam] < FMMC_MAX_RULES
						IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iMyTeam].iWantedAtMidBitset,MC_serverBD_4.iVehPriority[sVehState.iIndex][iMyTeam])
							IF IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iMyTeam],MC_serverBD_4.iVehPriority[sVehState.iIndex][iMyTeam])
								IF NOT IS_BIT_SET(iWantedVehBitset,sVehState.iIndex)
									iLocalWantedHighestPriority = -1
									SET_BIT(iWantedVehBitset,sVehState.iIndex)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF MC_serverBD_4.ivehRule[sVehState.iIndex][iMyTeam] = FMMC_OBJECTIVE_LOGIC_GO_TO
					OR MC_serverBD_4.ivehRule[sVehState.iIndex][iMyTeam] = FMMC_OBJECTIVE_LOGIC_LEAVE_ENTITY
						#IF IS_DEBUG_BUILD
							IF bInVehicleDebug
								PRINTLN("[RCC MISSION] PROCESS_VEH_EVERY_FRAME_CLIENT - AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH I'm on a goto rule ")
							ENDIF
						#ENDIF
						
						IF iSpectatorTarget = -1
						OR IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet,PBBOOL_HEIST_SPECTATOR)
							#IF IS_DEBUG_BUILD
								IF bInVehicleDebug
									PRINTLN("[RCC MISSION] PROCESS_VEH_EVERY_FRAME_CLIENT - AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH Not a spectator ")
								ENDIF
							#ENDIF
						
							IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iMyTeam].iRuleBitsetThree[MC_serverBD_4.iVehPriority[sVehState.iIndex][iMyTeam]], ciBS_RULE3_ALL_IN_SEPARATE_VEHICLES)
								IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iMyTeam].iRuleBitsetFive[MC_ServerBD_4.iCurrentHighestPriority[iMyTeam]], ciBS_RULE5_IGNORE_GOTO_RADIUS)
									MC_playerBD[iLocalPart].iVehNear = sVehState.iIndex
								ELSE
									PRINTLN("[RCC MISSION] PROCESS_VEH_EVERY_FRAME_CLIENT - ciBS_RULE5_IGNORE_GOTO_RADIUS is set - 1")
								ENDIF
								
								#IF IS_DEBUG_BUILD
									IF bInVehicleDebug
										PRINTLN("[RCC MISSION] PROCESS_VEH_EVERY_FRAME_CLIENT - AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH (3) Setting MC_playerBD[iPartToUse].iVehNear =  ", MC_playerBD[iPartToUse].iVehNear, " for part ",iPartToUse, " who is player ",  GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPartToUse))))
									ENDIF
								#ENDIF
							ELSE
								#IF IS_DEBUG_BUILD
									IF bInVehicleDebug
										PRINTLN("[RCC MISSION] PROCESS_VEH_EVERY_FRAME_CLIENT - AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH ciBS_RULE3_ALL_IN_SEPARATE_VEHICLES is set! ")
									ENDIF
								#ENDIF
							
								BOOL bCheckOnRule // To get it compiling
								INT iTeamsBS = GET_VEHICLE_GOTO_TEAMS(iMyTeam, sVehState.iIndex, bCheckOnRule)
								
								INT iCombinedBS = 0
								
								INT iTeamLoop
								FOR iTeamLoop = 0 TO (MC_serverBD.iNumberOfTeams - 1)
									IF IS_BIT_SET(iTeamsBS, iTeamLoop)
										iCombinedBS |= iVehHeld_NotMe_BS[iTeamLoop] 
									ENDIF
								ENDFOR
								
								IF NOT IS_BIT_SET(iCombinedBS, sVehState.iIndex)
								AND IS_PED_SITTING_IN_VEHICLE_SEAT(LocalPlayerPed, sVehState.vehIndex, VS_DRIVER)
									MC_playerBD[iLocalPart].iVehNear = sVehState.iIndex
								ENDIF
							ENDIF
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
							IF bInVehicleDebug
								PRINTLN("[RCC MISSION] PROCESS_VEH_EVERY_FRAME_CLIENT - AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH I'm not on a goto rule! MC_serverBD_4.ivehRule[sVehState.iIndex][iMyTeam] = ", MC_serverBD_4.ivehRule[sVehState.iIndex][iMyTeam])
							ENDIF
						#ENDIF
								
						IF MC_serverBD_4.iVehRule[sVehState.iIndex][iMyTeam]= FMMC_OBJECTIVE_LOGIC_CAPTURE
							IF MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(MC_serverBD_1.iControlVehTimer[sVehState.iIndex])
								IF bPlayerToUseOK
									IF NOT IS_SPECTATOR_HUD_HIDDEN()
									AND NOT IS_SPECTATOR_SHOWING_NEWS_HUD()
									AND NOT IS_OBJECTIVE_BLOCKED()
									AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iMyTeam].iRuleBitset[MC_serverBD_4.iVehPriority[sVehState.iIndex][iMyTeam]], ciBS_RULE_HIDE_HUD)
									
										// Don't draw this one if 'always show is on as we draw this elsewhere all the time.
										IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetFour, ciFMMC_VEHICLE4_VEHICLE_ALWAYS_SHOW_CAPTURE_BAR)
											
											BOOL bIsLiteralString = FALSE
											TEXT_LABEL_63 tl63CaptureBarText = "CON_TIME"
											IF IS_CUSTOM_STRING_LIST_STRING_VALID(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iCaptureBarCustomString)
												tl63CaptureBarText = GET_CUSTOM_STRING_LIST_TEXT_LABEL(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iCaptureBarCustomString)
												bIsLiteralString = TRUE
											ENDIF
											
											DRAW_GENERIC_METER(MC_GET_NET_TIMER_DIFFERENCE_WITH_TIME_STAMP(MC_serverBD_1.iControlVehTimer[sVehState.iIndex]), MC_serverBD.iVehTakeoverTime[iMyTeam][MC_serverBD_4.iVehPriority[sVehState.iIndex][iMyTeam]], tl63CaptureBarText, HUD_COLOUR_GREEN, -1, HUDORDER_DONTCARE, -1, -1, FALSE, TRUE, HUDFLASHING_NONE, 0, FALSE, 0, bIsLiteralString)
											
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						IF MC_serverBD_4.ivehRule[sVehState.iIndex][iMyTeam] = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER
						OR MC_serverBD_4.ivehRule[sVehState.iIndex][iMyTeam] = FMMC_OBJECTIVE_LOGIC_GET_AND_HOLD
						OR MC_serverBD_4.ivehRule[sVehState.iIndex][iMyTeam] = FMMC_OBJECTIVE_LOGIC_CAPTURE
						OR MC_serverBD_4.ivehRule[sVehState.iIndex][iMyTeam] = FMMC_OBJECTIVE_LOGIC_PROTECT
							IF iSpectatorTarget = -1
							OR IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet,PBBOOL_HEIST_SPECTATOR)
								IF MC_serverBD_4.ivehRule[sVehState.iIndex][MC_playerBD[iLocalPart].iteam] != FMMC_OBJECTIVE_LOGIC_PROTECT
									IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iMyTeam].iRuleBitsetThree[MC_serverBD_4.iVehPriority[sVehState.iIndex][iMyTeam]], ciBS_RULE3_ALL_IN_SEPARATE_VEHICLES)
										MC_playerBD[iLocalPart].iVehNear = sVehState.iIndex
										
										#IF IS_DEBUG_BUILD
											IF bInVehicleDebug
												PRINTLN("[RCC MISSION] PROCESS_VEH_EVERY_FRAME_CLIENT - AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH (4) Setting MC_playerBD[iPartToUse].iVehNear =  ", MC_playerBD[iPartToUse].iVehNear, " for part ",iPartToUse, " who is player ",  GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPartToUse))))
											ENDIF
										#ENDIF
										
									ELSE
										BOOL bCheckOnRule // To get it compiling
										INT iTeamsBS = GET_VEHICLE_GOTO_TEAMS(iMyTeam, sVehState.iIndex, bCheckOnRule)
										
										INT iCombinedBS = 0
										
										INT iTeamLoop
										FOR iTeamLoop = 0 TO (MC_serverBD.iNumberOfTeams - 1)
											IF IS_BIT_SET(iTeamsBS, iTeamLoop)
												iCombinedBS |= iVehHeld_NotMe_BS[iTeamLoop]
											ENDIF
										ENDFOR
										
										IF NOT IS_BIT_SET(iCombinedBS, sVehState.iIndex)
										AND IS_PED_SITTING_IN_VEHICLE_SEAT(LocalPlayerPed, sVehState.vehIndex, VS_DRIVER)
											MC_playerBD[iLocalPart].iVehNear = sVehState.iIndex
											
											#IF IS_DEBUG_BUILD
												IF bInVehicleDebug
													PRINTLN("[RCC MISSION] PROCESS_VEH_EVERY_FRAME_CLIENT - AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH (5) Setting MC_playerBD[iPartToUse].iVehNear =  ", MC_playerBD[iPartToUse].iVehNear, " for part ",iPartToUse, " who is player ",  GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPartToUse))))
												ENDIF
											#ENDIF
										ENDIF
									ENDIF
								ENDIF
								
								IF MC_serverBD_4.ivehRule[sVehState.iIndex][MC_playerBD[iLocalPart].iteam] = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER
									IF (amIWaitingForPlayersToEnterVeh != NULL AND NOT CALL amIWaitingForPlayersToEnterVeh())
										IF NOT bIsPedInVehicleInterior
										AND GET_PED_IN_VEHICLE_SEAT(tempVeh) = LocalPlayerPed										
											IF NOT IS_BIT_SET(ivehcolBoolCheck,sVehState.iIndex)
												BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_COLLECT_VEH,0,MC_playerBD[iLocalPart].iteam,-1,INVALID_PLAYER_INDEX(),ci_TARGET_VEHICLE,sVehState.iIndex)
												SET_BIT(ivehcolBoolCheck,sVehState.iIndex)
											ENDIF
										ENDIF
										imyVehicleCarryCount++
										PRINTLN("[RCC MISSION] PROCESS_VEH_EVERY_FRAME_CLIENT - imyVehicleCarryCount = ", imyVehicleCarryCount)
									ENDIF
								ELSE
									IF NOT bIsPedInVehicleInterior
									AND GET_PED_IN_VEHICLE_SEAT(tempVeh) = LocalPlayerPed
										IF NOT IS_BIT_SET(ivehcolBoolCheck,sVehState.iIndex)
											BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_COLLECT_VEH,0,MC_playerBD[iLocalPart].iteam, -1, INVALID_PLAYER_INDEX(), ci_TARGET_VEHICLE, sVehState.iIndex)
											SET_BIT(ivehcolBoolCheck,sVehState.iIndex)
										ENDIF
									ENDIF
									imyVehicleCarryCount++
									PRINTLN("[RCC MISSION] PROCESS_VEH_EVERY_FRAME_CLIENT - imyVehicleCarryCount = ", imyVehicleCarryCount)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF MC_playerBD[iPartToUse].iVehCarryCount < 1
					IF IS_BIT_SET(ivehcolBoolCheck,sVehState.iIndex)
						CLEAR_BIT(ivehcolBoolCheck,sVehState.iIndex)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Vehicle Every Frame Processing
// ##### Description: Client and Server Vehicle Every Frame Processing
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_VEH_PRE_EVERY_FRAME()
	
	#IF IS_DEBUG_BUILD
	iTempProfilerActiveVehicles = 0
	#ENDIF	
	
ENDPROC

PROC PROCESS_VEH_POST_EVERY_FRAME()
	
	#IF IS_DEBUG_BUILD
	iProfilerActiveVehicles = iTempProfilerActiveVehicles
	#ENDIF	
	
ENDPROC		

PROC PROCESS_VEH_EVERY_FRAME_CLIENT(INT iVeh)
	
	IF IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
		EXIT
	ENDIF
	
	FMMC_VEHICLE_STATE sVehState
	FILL_FMMC_VEHICLE_STATE_STRUCT(sVehState, iVeh)
	
	PROCESS_VEHICLE_TRAIN_ATTACHMENT_EVERY_FRAME(sVehState)
	
	PROCESS_HACKING_DISTANCE_CAPTURE(sVehState)
	
	PROCESS_VEHICLE_HUD(sVehState)
	
	PROCESS_VEH_TEAM_EVERY_FRAME_LOGIC_CLIENT(sVehState)
	
	PROCESS_VEH_SELF_DESTRUCT_SEQUENCE(sVehState)
	
	PROCESS_VEHICLE_BEING_TOUCHED(sVehState)
	
	PROCESS_VEHICLE_IN_VEHICLE_FUNCTIONALITY(sVehState)
	
	IF NOT sVehState.bExists
		
		PROCESS_VEHICLE_DOES_NOT_EXIST_CARGOBOB_ATTACHED_ENTITY(sVehState)
		
		PROCESS_VEHICLE_CLEANUP_CONTINUITY(sVehState)
		
		EXIT
		
	ELSE
	
		#IF IS_DEBUG_BUILD
		iTempProfilerActiveVehicles++
		#ENDIF	
	
		// At the start so we get the print before script modifications.	 
		#IF IS_DEBUG_BUILD
			DEBUG_PRINT_PLACED_VEHICLE_HEALTH_INFO(iVeh)
		#ENDIF
					
		PROCESS_VEHICLE_EVERY_FRAME_CONTINUITY(sVehState)
		
		PROCESS_WARP_ON_RULE_DATA_FOR_ENTITY(CREATION_TYPE_VEHICLES, sVehState.iIndex, iVehicleShouldWarpThisRuleStart, iVehicleWarpedOnThisRule)
		
		PROCESS_VEHICLE_BLIPS(sVehState)
	
		PROCESS_VEHICLE_LOCK_SHOCK(sVehState)
		
		STEALING_SETS_LOCAL_PLAYER_WANTED(sVehState)
		
		PROCESS_VEHICLE_ENGINE_KNOCKBACK(sVehState)
				
		PROCESS_VEHICLE_RADIO_STATION_SEQUENCE(sVehState)
		
		PROCESS_FORCE_HELI_BLADES_AT_FULL_SPEED(sVehState)
		
		PROCESS_PLAYER_IN_VEHICLE_WANTED_CONE_RESPONSE(sVehState)
		
		PROCESS_LOCK_VEHICLE_ON_RULE(sVehState)
		
		PROCESS_UNLOCK_VEHICLE_ON_RULE(sVehState)
		
		PROCESS_RESET_VEHICLE_WHEN_OOB(sVehState)
		
		PROCESS_RESET_VEHICLES_ON_RULE_START(sVehState)
		
		IF sVehState.bAlive
			
			PROCESS_BLOCK_EXIT_VEHICLE_WHEN_MOVING(sVehState)
			
			PROCESS_VEHICLE_NOT_RECOGNISED_BY_PLAYERS(sVehState)
			
			PROCESS_SMOKE_ON_LOW_VEHICLE_BODY_HEALTH(sVehState)
			
			PROCESS_BACK_OF_VEHICLE_DRIVEBY_SEQUENCE(sVehState)
			
			IF sVehState.bHasControl
				IF sVehState.bDrivable
					
					PROCESS_DELAYED_VEHICLE_DOOR_OPENING(sVehState)
					
					DESTROY_LINKED_VEHICLE(sVehState)
					
					PROCESS_VEHICLE_COLLISION_SHOULD_EXPLODE(sVehState)
					
					PROCESS_VEHICLE_ANCHOR_WHILE_STATIONARY(sVehState)					

					SET_SPAWN_VEHICLE_NEAR_LAST_PLAYER(sVehState)
					
					PROCESS_VEHICLE_BRAKES_PERMANENTLY_ON(sVehState)
					
					IF MC_ServerBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam] < FMMC_MAX_RULES
						
						PROCESS_DISABLE_VEHICLE_ON_FRAME(sVehState)	
						
					ENDIF
					
					PROCESS_DISABLE_VEHICLE_WEAPONS(sVehState)
					
					PROCESS_VEHICLE_DOORS(sVehState)
					
					PROCESS_VEHICLE_INVINCIBILITY(sVehState)
					
					SET_DECORATOR_ON_MOC_TRAILER(sVehState)
					
					SET_DECORATOR_ON_AVENGER(sVehState)
					
					CLEAR_VEHICLE_DAMAGE_RECORD(sVehState)
					
				ELSE
					
					PROCESS_VEHICLE_NOT_DRIVABLE(sVehState)
					
				ENDIF
				PROCESS_EXPLODING_CARGO(sVehState)
				PROCESS_EXPLODING_VEHICLE(sVehState)
			ENDIF
			
			PROCESS_ATTACH_ASSIGNED_CARGOBOB_VEHICLE_ENTITY(sVehState)
			
			PROCESS_DISABLE_DRIVING_VEHICLE_ON_REMOTE_RESERVE(sVehState)
						
			PROCESS_VEHICLE_RADIO(sVehState)
			
			PROCESS_VEHICLE_AUDIO_STREAM(sVehState)
				
		ELSE
			
			PROCESS_VEHICLE_NOT_ALIVE_CLEANUP(sVehState)
			
		ENDIF
		
		PROCESS_VEHICLE_EXTRA_EMP_EFFECTS(sVehState)
		
		IF !bIsAnySpectator
			CHECK_SEATING_PREFERENCES(sVehState.iIndex)
			CHECK_VEHICLE_TARGETING(sVehState)
		ENDIF
		
		//FMMC2020 - Vehicle objective logic that needs to be moved, but some of the data (iVehNear) is cleared every frame
		PROCESS_VEHICLE_OBJECTIVE_LOGIC_CLIENT(sVehState)
		
		#IF IS_DEBUG_BUILD
		PROCESS_VEHICLE_DEBUG(sVehState)
		#ENDIF
	ENDIF
	
ENDPROC	

PROC PROCESS_FMMC_VAN_CARGO_SERVER(FMMC_VEHICLE_STATE &sVehState)
	IF sVehState.mn = GBURRITO
	OR sVehState.mn = GBURRITO2
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetTwo, ciFMMC_VEHICLE2_EXTRA_MINI_SERVER) //Check if the broken server prop has already been created
		AND sVehState.bExists
			
			MODEL_NAMES mnBrokenCrate = HEI_PROP_MINI_SEVER_BROKEN
			
			MODEL_NAMES mnCurrentCrate = GET_ENTITY_MODEL(NET_TO_ENT(MC_serverBD_1.sFMMC_SBD.niVehicleCrate[sVehState.iIndex]))
			
			IF mnCurrentCrate != mnBrokenCrate
				
				IF CAN_REGISTER_MISSION_OBJECTS(1)
					
					REQUEST_MODEL(mnBrokenCrate)
					
					//Need control of the vehicle to attach the new crate to it:
					IF sVehState.bHasControl
						
						//Need control of the old crate to delete it:
						IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicleCrate[sVehState.iIndex])
							
							IF HAS_MODEL_LOADED(mnBrokenCrate)
								
								MC_RESERVE_NETWORK_MISSION_OBJECTS(1)
								
								NETWORK_INDEX niBrokenCrate
								STRING sBone = GET_VEHICLE_CRATE_BONE(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitSet, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetTwo, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetSeven, sVehState.vehIndex)
								
								PRINTLN("[RCC MISSION] PROCESS_FMMC_VAN_CARGO_SERVER - Creating new object to replace ",sVehState.iIndex,"'s crate in back with broken server - bone index ",GET_ENTITY_BONE_INDEX_BY_NAME(sVehState.vehIndex, sBone))
								
								INT iBone = GET_ENTITY_BONE_INDEX_BY_NAME(sVehState.vehIndex, sBone)
								VECTOR vBone = GET_WORLD_POSITION_OF_ENTITY_BONE(sVehState.vehIndex, iBone)
								
								PRINTLN("[RCC MISSION] PROCESS_FMMC_VAN_CARGO_SERVER - Creating new object to replace ",sVehState.iIndex,"'s crate in back with broken server - bone index ",iBone,", bone loc ",vBone)
								
								IF FMMC_CREATE_NET_OBJ(niBrokenCrate, mnBrokenCrate, vBone)
									
									PRINTLN("[RCC MISSION] PROCESS_FMMC_VAN_CARGO_SERVER - Removing ",sVehState.iIndex,"'s old crate, as new broken server has been created!")
									
									//Get rid of the old crate:
									DELETE_NET_ID(MC_serverBD_1.sFMMC_SBD.niVehicleCrate[sVehState.iIndex])
									
									OBJECT_INDEX oiBrokenCrate = NET_TO_OBJ(niBrokenCrate)
									
									ATTACH_ENTITY_TO_ENTITY(oiBrokenCrate, sVehState.vehIndex, iBone, GET_CRATE_OFFSET_FOR_VEHICLE(sVehState.vehIndex, oiBrokenCrate), GET_CRATE_ROTATION_FOR_VEHICLE(sVehState.vehIndex, oiBrokenCrate), TRUE, FALSE, SHOULD_CRATE_COLLISION_BE_ENABLED(sVehState.vehIndex))
									SET_ENTITY_LOD_DIST(oiBrokenCrate, 100)
									SET_ENTITY_VISIBLE(oiBrokenCrate, TRUE)
									SET_MODEL_AS_NO_LONGER_NEEDED(mnBrokenCrate)
									
									//Update server variable so we can clean it up normally:
									MC_serverBD_1.sFMMC_SBD.niVehicleCrate[sVehState.iIndex] = niBrokenCrate
									
								ENDIF
							#IF IS_DEBUG_BUILD
							ELSE
								PRINTLN("[RCC MISSION] PROCESS_FMMC_VAN_CARGO_SERVER - Waiting for model of ",sVehState.iIndex,"'s crate to replace crate in back with broken server")
							#ENDIF
							ENDIF
						ELSE
							PRINTLN("[RCC MISSION] PROCESS_FMMC_VAN_CARGO_SERVER - Requesting control of vehicle ",sVehState.iIndex,"'s crate to replace crate in back with broken server")
							NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicleCrate[sVehState.iIndex])
						ENDIF
					ELSE
						PRINTLN("[RCC MISSION] PROCESS_FMMC_VAN_CARGO_SERVER - Requesting control of vehicle ",sVehState.iIndex," to replace crate in back with broken server")
						NETWORK_REQUEST_CONTROL_OF_ENTITY(sVehState.vehIndex)
					ENDIF
				#IF IS_DEBUG_BUILD
				ELSE
					PRINTLN("[RCC MISSION] PROCESS_FMMC_VAN_CARGO_SERVER - Unable to register 1 more mission object to replace crate in back of vehicle ",sVehState.iIndex," with broken server")
				#ENDIF
				ENDIF
				
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_CRITICAL_LINKED_VEHICLE_SERVER(FMMC_VEHICLE_STATE &sVehState)
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iMissionCriticalLinkedVeh > -1
		
		VEHICLE_INDEX vehLinkedCritical = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iMissionCriticalLinkedVeh])
		BOOL bExists = DOES_ENTITY_EXIST(vehLinkedCritical)
		BOOL bDead = IS_ENTITY_DEAD(vehLinkedCritical)
		INT iTeam
		
		FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)	
			IF IS_BIT_SET(MC_serverBD.iVehTeamFailBitset[sVehState.iIndex], iTeam)
				IF bExists
				AND NOT bDead
					PRINTLN("[RCC MISSION] PROCESS_CRITICAL_LINKED_VEHICLE_SERVER - Veh ",sVehState.iIndex," set as no longer critical for team ",iteam," as linked veh is ok: ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iMissionCriticalLinkedVeh)
					CLEAR_BIT(MC_serverBD.iMissionCriticalVeh[iTeam], sVehState.iIndex)
					CLEAR_BIT(MC_serverBD.iVehTeamFailBitset[sVehState.iIndex], iTeam)
				ENDIF
			ELSE
				IF NOT bExists
				OR bDead
					PRINTLN("[RCC MISSION] PROCESS_CRITICAL_LINKED_VEHICLE_SERVER - Veh ",sVehState.iIndex," set as  critical for team ",iteam," as linked veh is NOT ok: ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iMissionCriticalLinkedVeh)
					SET_BIT(MC_serverBD.iMissionCriticalVeh[iTeam], sVehState.iIndex)
					SET_BIT(MC_serverBD.iVehTeamFailBitset[sVehState.iIndex], iTeam)
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
ENDPROC

PROC PROCESS_FMMC_VEHICLE_DELAYED_DOOR_OPENING_SERVER(FMMC_VEHICLE_STATE &sVehState)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetFive, ciFMMC_VEHICLE5_DELAYED_DOOR_OPEN)
	AND NOT IS_BIT_SET(MC_serverBD.iOpenVehicleDoorsBitset, sVehState.iIndex)
		IF HAS_NET_TIMER_STARTED(MC_serverBD.stVehicleDoorsOpenTimer)

			IF HAS_NET_TIMER_EXPIRED(MC_serverBD.stVehicleDoorsOpenTimer, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehicleDelayedDoorTimer * 1000)
				IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetFive, ciFMMC_VEHICLE5_DELAYED_DOOR_OPEN_SECOND_CHECK)
					PRINTLN("[RCC MISSION] PROCESS_FMMC_VEHICLE_DELAYED_DOOR_OPENING_SERVER - Timer expired, setting doors open")
					SET_BIT(MC_serverBD.iOpenVehicleDoorsBitset, sVehState.iIndex)
					RESET_NET_TIMER(MC_serverBD.stVehicleDoorsOpenTimer)
				ELSE
					INT iClosestPlayer = GET_NEAREST_PLAYER_TO_ENTITY(sVehState.vehIndex)
					PLAYER_INDEX ClosestPlayer = INT_TO_PLAYERINDEX(iClosestPlayer)
					IF IS_NET_PLAYER_OK(ClosestPlayer)
						PED_INDEX ClosestPlayerPed = GET_PLAYER_PED(ClosestPlayer)
						IF GET_DISTANCE_BETWEEN_ENTITIES(sVehState.vehIndex,ClosestPlayerPed, FALSE) < g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].fVehicleDelayedDoorRange
							PRINTLN("[RCC MISSION] PROCESS_FMMC_VEHICLE_DELAYED_DOOR_OPENING_SERVER - Timer expired and second range check satisfied, setting doors open")
							SET_BIT(MC_serverBD.iOpenVehicleDoorsBitset, sVehState.iIndex)
							RESET_NET_TIMER(MC_serverBD.stVehicleDoorsOpenTimer)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
		
			INT iTeamToCheck = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehicleDelayedDoorRule_Team
			IF iTeamToCheck = -1
				iTeamToCheck = GET_LOCAL_PLAYER_TEAM()
			ENDIF
			
			IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehicleDelayedDoorRule = -1
			OR g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehicleDelayedDoorRule = GET_TEAM_CURRENT_RULE(iTeamToCheck)

				IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].fVehicleDelayedDoorRange < 0.0
					START_NET_TIMER(MC_serverBD.stVehicleDoorsOpenTimer)
					PRINTLN("[RCC MISSION] PROCESS_FMMC_VEHICLE_DELAYED_DOOR_OPENING_SERVER - No range check, starting timer")
				ELSE
					INT iClosestPlayer = GET_NEAREST_PLAYER_TO_ENTITY(sVehState.vehIndex)
					PLAYER_INDEX ClosestPlayer = INT_TO_PLAYERINDEX(iClosestPlayer)
					IF IS_NET_PLAYER_OK(ClosestPlayer)
						PED_INDEX ClosestPlayerPed = GET_PLAYER_PED(ClosestPlayer)
						IF GET_DISTANCE_BETWEEN_ENTITIES(sVehState.vehIndex,ClosestPlayerPed, FALSE) < g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].fVehicleDelayedDoorRange
							START_NET_TIMER(MC_serverBD.stVehicleDoorsOpenTimer)
							PRINTLN("[RCC MISSION] PROCESS_FMMC_VEHICLE_DELAYED_DOOR_OPENING_SERVER - Range check succeeded, starting timer ")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL HAS_FMMC_VEHICLE_CLEANED_UP_WITH_OWNERSHIP(FMMC_VEHICLE_STATE &sVehState)
	IF IS_BIT_SET(MC_serverBD.iVehCleanup_NeedOwnershipBS, sVehState.iIndex)
	
		PRINTLN("[RCC MISSION] HAS_FMMC_VEHICLE_CLEANED_UP_WITH_OWNERSHIP - iVehCleanup_NeedOwnershipBS set for veh ",sVehState.iIndex)
		
		IF SHOULD_ENTITY_RESPAWN_THIS_RULE(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iRespawnOnRuleChangeBS, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iRespawnOnRule, -1)
			
			IF NOT sVehState.bHasControl
				IF IS_FMMC_VEHICLE_EMPTY(sVehState)
					PRINTLN("[RCC MISSION] HAS_FMMC_VEHICLE_CLEANED_UP_WITH_OWNERSHIP - Requesting control of veh ",sVehState.iIndex," for cleanup - for delete, objective: ",g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iCleanupObjective," team: ",g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iCleanupTeam," vehicle: ",sVehState.iIndex)
					NETWORK_REQUEST_CONTROL_OF_ENTITY(sVehState.vehIndex)
				ENDIF
			ELSE
				IF IS_FMMC_VEHICLE_EMPTY(sVehState)
					PRINTLN("[RCC MISSION] HAS_FMMC_VEHICLE_CLEANED_UP_WITH_OWNERSHIP - Deleting vehicle ",sVehState.iIndex)
					DELETE_FMMC_VEHICLE(sVehState.iIndex)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL PROCESS_VEHICLE_RESPAWN(FMMC_VEHICLE_STATE &sVehState)
	
	IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_FIRST_UPDATE_DONE)
		RETURN FALSE
	ENDIF
		
	IF GET_VEHICLE_RESPAWNS(sVehState.iIndex) > 0
		INT iTeam
		IF SHOULD_VEH_RESPAWN_NOW(sVehState)
		AND HAS_VEH_RESPAWN_DELAY_EXPIRED(sVehState.iIndex)
			IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iAttachParent > -1
			AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iAttachParentType = CREATION_TYPE_VEHICLES
				FMMC_VEHICLE_STATE sAttachVehState
				FILL_FMMC_VEHICLE_STATE_STRUCT(sAttachVehState, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iAttachParent)
				
				IF !sAttachVehState.bDrivable
				AND SHOULD_VEH_RESPAWN_NOW(sAttachVehState)
					PRINTLN("[MCSpawning][Vehicles][Vehicle ", sVehState.iIndex, "] PROCESS_VEHICLE_RESPAWN - Waiting for veh ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iAttachParent," to spawn")
					RETURN TRUE
				ENDIF
			
			ENDIF	
			
			IF RESPAWN_MISSION_VEHICLE(sVehState.iIndex)
				
				CLEAR_BIT(iVehShouldRespawnNowBS, sVehState.iIndex)
				
				SET_BIT(MC_serverBD_2.iVehSpawnedOnce, sVehState.iIndex)
				IF IS_BIT_SET(MC_serverBD_2.iVehIsDestroyed, sVehState.iIndex)
					CLEAR_BIT(MC_serverBD_2.iVehIsDestroyed, sVehState.iIndex)
				ENDIF
				
				IF IS_BIT_SET(MC_ServerBD.iServerBS_PassRuleVehAfterRespawn, sVehState.iIndex)
					PRINTLN("[MCSpawning][Vehicles][Vehicle ", sVehState.iIndex, "] PROCESS_VEHICLE_RESPAWN - Clearing iServerBS_PassRuleVehAfterRespawn on sVehState.iIndex, ", sVehState.iIndex)
					CLEAR_BIT(MC_ServerBD.iServerBS_PassRuleVehAfterRespawn, sVehState.iIndex)
				ENDIF
				
				IF SHOULD_ENTITY_RESPAWN_THIS_RULE(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iRespawnOnRuleChangeBS, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iRespawnOnRule, -1)
					
					FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
						IF MC_serverBD_4.iVehPriority[sVehState.iIndex][iTeam] < FMMC_MAX_RULES
							SET_BIT(MC_serverBD.iVehTeamFailBitset[sVehState.iIndex],iteam)
						ENDIF
					ENDFOR
				ENDIF
				
				MC_STOP_NET_TIMER_WITH_TIMESTAMP(MC_serverBD_3.iVehSpawnDelay[sVehState.iIndex])
										
				IF DOES_VEH_HAVE_PLAYER_VARIABLE_SETTING(sVehState.iIndex)
					IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iAssociatedObjective > -1
					AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iAssociatedTeam > -1
						IF MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iAssociatedTeam] < FMMC_MAX_RULES
							MC_serverBD_1.sCreatedCount.iNumVehCreated[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iAssociatedObjective]++
						ENDIF
					ENDIF
				ENDIF
				
				// We still need to decrement so that Peds know when a new vehicle has spawned and that they are allowed to respawn inside of it.
				DECREMENT_VEHICLE_RESPAWNS(sVehState.iIndex)
				IF GET_ENTITY_STARTING_RESPAWN_LIVES(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehicleRespawnLives) = UNLIMITED_RESPAWNS
				AND GET_VEHICLE_RESPAWNS(sVehState.iIndex) < 1
					MC_serverBD_2.iCurrentVehRespawnLives[sVehState.iIndex] = UNLIMITED_RESPAWNS
				ENDIF
			ENDIF
		ELSE
			IF MC_serverBD.isearchingforVeh = sVehState.iIndex
				IF NETWORK_ENTITY_AREA_DOES_EXIST(MC_serverBD.iSpawnArea)
					NETWORK_REMOVE_ENTITY_AREA(MC_serverBD.iSpawnArea)
					PRINTLN("[MCSpawning][Vehicles][Vehicle ", sVehState.iIndex, "] PROCESS_VEHICLE_RESPAWN - MC_serverBD.iSpawnArea Reset veh: ", sVehState.iIndex)
				ENDIF
				RESET_NET_TIMER(MC_serverBD.tdSpawnAreaTimeout)
				MC_serverBD.iSpawnArea = -1
				IF MC_serverBD.isearchingforVeh != -1	
					PRINTLN("[MCSpawning][Vehicles][Vehicle ", sVehState.iIndex, "] Clearing isearchingforVeh: ", MC_serverBD.isearchingforVeh)
					MC_serverBD.isearchingforVeh = -1	
				ENDIF
			ENDIF
			
			
			IF DOES_ANY_TEAM_HAVE_NEW_PRIORITY_THIS_FRAME()
			AND (NOT CAN_VEH_STILL_SPAWN_IN_MISSION(sVehState.iIndex))
				
				MC_serverBD.iVehTeamFailBitset[sVehState.iIndex] = 0
				
				FOR iTeam = 0 TO (FMMC_MAX_TEAMS - 1)
					MC_serverBD_4.iVehPriority[sVehState.iIndex][iTeam] = FMMC_PRIORITY_IGNORE
					MC_serverBD_4.ivehRule[sVehState.iIndex][iTeam] = FMMC_OBJECTIVE_LOGIC_NONE
					CLEAR_BIT(MC_serverBD.iMissionCriticalVeh[iTeam], sVehState.iIndex)
				ENDFOR
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_VEH_EVERY_FRAME_SERVER(INT iVeh)
	
	IF !bIsLocalPlayerHost
		EXIT
	ENDIF

	IF IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
		EXIT
	ENDIF
	
	FMMC_VEHICLE_STATE sVehState
	FILL_FMMC_VEHICLE_STATE_STRUCT(sVehState, iVeh)
	
	PROCESS_HACKING_DISTANCE_CAPTURE(sVehState)
	
	iTempHackingTargetsRemaining = 0

	MANAGE_SERVER_VEH_CAPTURE_BY_DISTANCE_EVERY_FRAME(sVehState)
	
	PROCESS_VEH_TEAM_EVERY_FRAME_LOGIC_SERVER(sVehState)
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_SECUROSERV_HACKING)
		IF MC_serverBD_4.iHackingTargetsRemaining != iTempHackingTargetsRemaining
			MC_serverBD_4.iHackingTargetsRemaining = iTempHackingTargetsRemaining 
			PRINTLN("[Vehicles][Vehicle ", sVehState.iIndex, "][SECUROHACK] - PROCESS_VEH_EVERY_FRAME_SERVER - iHackingTargetsRemaining updated to  ", MC_serverBD_4.iHackingTargetsRemaining)
		ENDIF
	ENDIF
	
	IF NOT sVehState.bExists
		
		IF IS_BIT_SET(MC_serverBD.iVehCleanup_NeedOwnershipBS, sVehState.iIndex)
			PRINTLN("[Vehicles][Vehicle ", sVehState.iIndex, "] - PROCESS_VEH_EVERY_FRAME_SERVER - we were trying to request ownership of for cleanup no longer exists!")
			CLEAR_BIT(MC_serverBD.iVehCleanup_NeedOwnershipBS, sVehState.iIndex)
		ENDIF

		IF IS_BIT_SET(MC_serverBD.iVehSpawnBitset, sVehState.iIndex)
			PRINTLN("[Vehicles][Vehicle ", sVehState.iIndex, "] - PROCESS_VEH_EVERY_FRAME_SERVER - Clear iVehSpawnBitset for veh net ID doesn't exist")
			CLEAR_BIT(MC_serverBD.iVehSpawnBitset, sVehState.iIndex)
		ENDIF
		CLEAR_BIT(iDisableVehicleWeaponBitset, sVehState.iIndex)
	
		IF PROCESS_VEHICLE_RESPAWN(sVehState)
			EXIT
		ENDIF
	ELSE
		
		IF HAS_FMMC_VEHICLE_CLEANED_UP_WITH_OWNERSHIP(sVehState)
			EXIT
		ENDIF
	
		CONVERT_AMBIENT_VEHICLE_TO_MISSION_ENTITY(sVehState.iIndex)
		
		IF sVehState.bDrivable
			
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iVehBitsetSix, ciFMMC_VEHICLE6_GHOST_PLAYER_VEHICLES)
				MC_serverBD_3.iGhostingPlayerVehiclesVeh = sVehState.iIndex
				PRINTLN("[Vehicles][Vehicle ", sVehState.iIndex, "] - PROCESS_VEH_EVERY_FRAME_SERVER - Setting vehicle GHOST PLAYER VEHICLES")
			ENDIF
			
			PROCESS_FMMC_VEHICLE_DELAYED_DOOR_OPENING_SERVER(sVehState)
			
			PROCESS_FMMC_VAN_CARGO_SERVER(sVehState)
			
			PROCESS_CRITICAL_LINKED_VEHICLE_SERVER(sVehState)
			
		ELSE
			CLEAR_BIT(MC_serverBD.iOpenVehicleDoorsBitset, sVehState.iIndex)
			RESET_NET_TIMER(MC_serverBD.stVehicleDoorsOpenTimer)
			
			IF IS_BIT_SET(MC_serverBD.iVehSpawnBitset, sVehState.iIndex)
				PRINTLN("[Vehicles][Vehicle ", sVehState.iIndex, "] - PROCESS_VEH_EVERY_FRAME_SERVER - Clear iVehSpawnBitset for veh is undriveable")
				CLEAR_BIT(MC_serverBD.iVehSpawnBitset, sVehState.iIndex)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(MC_serverBD.iVehCleanup_NeedOwnershipBS, sVehState.iIndex)
			IF NOT sVehState.bHasControl
				IF IS_FMMC_VEHICLE_EMPTY(sVehState)
					PRINTLN("[Vehicles][Vehicle ", sVehState.iIndex, "] - PROCESS_VEH_EVERY_FRAME_SERVER - Requesting control of veh for cleanup - for delete, objective: ",g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iCleanupObjective," team: ",g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iCleanupTeam," vehicle: ",sVehState.iIndex)
					NETWORK_REQUEST_CONTROL_OF_ENTITY(sVehState.vehIndex)
				ENDIF
			ELSE
				IF IS_FMMC_VEHICLE_EMPTY(sVehState)
					PRINTLN("[Vehicles][Vehicle ", sVehState.iIndex, "] - PROCESS_VEH_EVERY_FRAME_SERVER - (2) Deleting")
					DELETE_FMMC_VEHICLE(sVehState.iIndex)
				ENDIF
				
				EXIT
			ENDIF
			
		ELIF DOES_ANY_TEAM_HAVE_NEW_PRIORITY_THIS_FRAME(TRUE)
			IF MC_serverBD.iVehTeamFailBitset[sVehState.iIndex] > 0
				INT iTeam
				FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
					IF IS_BIT_SET(MC_serverBD.iVehTeamFailBitset[sVehState.iIndex], iTeam)
					AND (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iMissionCriticalRemovalRule[iTeam] > 0)
					AND MC_serverBD_4.iCurrentHighestPriority[iTeam] >= g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iMissionCriticalRemovalRule[iTeam]
						CLEAR_BIT(MC_serverBD.iMissionCriticalVeh[iTeam], sVehState.iIndex)
						CLEAR_BIT(MC_serverBD.iVehTeamFailBitset[sVehState.iIndex], iTeam)
						PRINTLN("[Vehicles][Vehicle ", sVehState.iIndex, "] - PROCESS_VEH_EVERY_FRAME_SERVER - Veh set as no longer critical for team ",iteam," as they have reached iMissionCriticalRemovalRule ",g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sVehState.iIndex].iMissionCriticalRemovalRule[iTeam])
					ENDIF
				ENDFOR
			ENDIF
			
			IF SHOULD_CLEANUP_VEH(sVehState)
			AND CLEANUP_VEH_EARLY(sVehState)
				PRINTLN("[Vehicles][Vehicle ", sVehState.iIndex, "] - PROCESS_VEH_EVERY_FRAME_SERVER - has cleaned up early, new priority/midpoint check")
				EXIT
			ENDIF
			
		ENDIF
			
	ENDIF
	
	PROCESS_SERVER_VEHICLE_CARGO_DAMAGE(sVehState)
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Vehicle Staggered Processing
// ##### Description: Client and Server Vehicle Staggered Processing
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_VEH_STAGGERED_CLIENT(INT iVeh)
	FMMC_VEHICLE_STATE sVehState
	FILL_FMMC_VEHICLE_STATE_STRUCT(sVehState, iVeh)
	
	PROCESS_VEH_BODY(sVehState)
	PROCESS_VEHICLE_BLIPS_STAGGERED(sVehState, sMissionVehiclesLocalVars[iVeh].sBlipRuntimeVars)
ENDPROC	

PROC PROCESS_VEH_PRE_STAGGERED_SERVER()
		
	INT iTeam = 0
	FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
		iDamageVehicleObjectiveRequiresCheckBS[iTeam] = 0		
	ENDFOR
	
	iCurrentVehHealthPercentageCachedBS = 0
	
ENDPROC

PROC PROCESS_VEH_STAGGERED_SERVER(INT iVeh)
	
	IF NOT bIsLocalPlayerHost
		EXIT
	ENDIF
	
	FMMC_VEHICLE_STATE sVehState
	FILL_FMMC_VEHICLE_STATE_STRUCT(sVehState, iVeh)
	
	PROCESS_VEH_BRAIN(sVehState)
	
	PROCESS_VEHICLE_DAMAGE_ENTITY_OBJECTIVE_STAGGERED(iVeh)
	
ENDPROC

PROC PROCESS_VEH_POST_STAGGERED_SERVER()

	INT iVeh
	FOR iVeh = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles-1
		PROCESS_VEHICLE_DAMAGE_ENTITY_OBJECTIVE_POST_STAGGERED(iVeh)
	ENDFOR
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Vehicle Objective Processing
// ##### Description: Client and Server Vehicle Objective Processing
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Runs checks to see if this placed vehicle is an objective that should be added to the Quick GPS menu.
PROC PROCESS_VEH_OBJECTIVES_CLIENT(INT iVeh)

	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])	
		EXIT
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetSix, ciFMMC_VEHICLE6_BLOCK_VEHICLE_AS_HINT_TARGET)
		EXIT
	ENDIF
	
	INT iTeam = MC_playerBD[iPartToUse].iTeam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	IF iRule >= FMMC_MAX_RULES
		EXIT
	ENDIF
	
	IF MC_serverBD_4.iVehPriority[iVeh][iTeam] != iRule
		EXIT
	ENDIF
	
	VEHICLE_INDEX viVehicle = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
	
	IF NOT IS_VEHICLE_DRIVEABLE(viVehicle)
		EXIT
	ENDIF
	
	IF IS_PED_IN_VEHICLE(LocalPlayerPed, viVehicle)
		EXIT
	ENDIF
	
	IF SHOULD_VEHICLE_HAVE_CUSTOM_BLIP(iVeh)
		
		IF IS_BIT_SET(iLocalBoolCheck26, LBOOL26_DUMMY_BLIP_OVERRIDE)
			EXIT
		ENDIF
		
	ELSE
		
		IF IS_BLIP_SET_AS_HIDDEN(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sVehBlipStruct)
		AND NOT SHOULD_HINT_CAM_WORK_FOR_OBJECTIVE_ENTITY_WITHOUT_BLIP(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sVehBlipStruct)
			EXIT
		ENDIF
		
		IF MC_serverBD_4.iVehRule[iVeh][iTeam] = FMMC_OBJECTIVE_LOGIC_GET_AND_HOLD 
		AND IS_BIT_SET(MC_serverBD.iVehAtYourHolding[iTeam], iVeh)
			EXIT
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitset[iRule], ciBS_RULE_ENABLETRACKIFY)
			EXIT
		ENDIF
		
		IF MC_serverBD_4.iVehRule[iVeh][iTeam] = FMMC_OBJECTIVE_LOGIC_NONE
			EXIT
		ENDIF
		
		IF IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_WITH_CARRIER)	
		AND MC_playerBD[iPartToUse].iVehNear != iVeh
			EXIT
		ENDIF
	
	ENDIF
	
	IF GET_NUM_PEDS_IN_VEHICLE(viVehicle) > 0
		//Don't allow Quick GPS to player driven vehicles
		IF NOT IS_VEHICLE_SEAT_FREE(viVehicle, VS_DRIVER)
		AND IS_PED_A_PLAYER(GET_PED_IN_VEHICLE_SEAT(viVehicle, VS_DRIVER))
			EXIT
		ENDIF
	ENDIF
		
	FLOAT fVehDist = GET_DIST2_BETWEEN_ENTITIES(LocalPlayerPed, viVehicle)
	IF fVehDist > fNearestTargetDist2Temp
		EXIT
	ENDIF
	
	IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		VEHICLE_INDEX viPlayerVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
		IF IS_ENTITY_ATTACHED_TO_ANY_VEHICLE(viPlayerVeh)
			IF viVehicle = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_ATTACHED_TO(viPlayerVeh))
				//This vehicle is a trailer attached to the player's vehicle
				EXIT
			ENDIF
		ENDIF
	ENDIF
				
	iNearestTargetTemp = iVeh
	iNearestTargetTypeTemp = ci_TARGET_VEHICLE
	fNearestTargetDist2Temp = fVehDist
	                
ENDPROC	

PROC PROCESS_VEHICLE_IN_PREREQ_ZONES(INT iVeh)

	IF iRuleEntityPrereqZoneCount <= 0
		EXIT
	ENDIF
	
	FMMC_VEHICLE_STATE sVehState //Should be passed through same as objects but its a bit change
	FILL_FMMC_VEHICLE_STATE_STRUCT(sVehState, iVeh)
	
	IF NOT sVehState.bExists
		EXIT
	ENDIF
	
	IF NOT sVehState.bAlive
		EXIT
	ENDIF
	
	INT i 
	FOR i = 0 TO iRuleEntityPrereqZoneCount - 1
		IF iZoneIndicesOfTypeRuleEntityPrereq[i] = -1
			RELOOP
		ENDIF
		IF NOT DOES_ZONE_EXIST(iZoneIndicesOfTypeRuleEntityPrereq[i])
			RELOOP
		ENDIF
		
		IF ARE_COORDS_IN_FMMC_ZONE(GET_FMMC_VEHICLE_COORDS(sVehState), iZoneIndicesOfTypeRuleEntityPrereq[i])
#IF IS_DEBUG_BUILD
			IF NOT IS_BIT_SET(iTempEntitiesInZoneBS, iZoneIndicesOfTypeRuleEntityPrereq[i])
				PRINTLN("[Vehicles][Vehicle ", sVehState.iIndex, "] PROCESS_VEHICLE_IN_PREREQ_ZONES - Vehicle is within zone: ", iZoneIndicesOfTypeRuleEntityPrereq[i])
			ENDIF
#ENDIF
			SET_BIT(iTempEntitiesInZoneBS, iZoneIndicesOfTypeRuleEntityPrereq[i])
		ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZoneIndicesOfTypeRuleEntityPrereq[i]].iZoneBS2, ciFMMC_ZONEBS2_CHECK_SAME_INTERIOR)
			IF GET_FMMC_VEHICLE_INTERIOR(sVehState) != NULL
			AND GET_INTERIOR_AT_COORDS(GET_ZONE_RUNTIME_CENTRE(iZoneIndicesOfTypeRuleEntityPrereq[i])) = GET_FMMC_VEHICLE_INTERIOR(sVehState)
#IF IS_DEBUG_BUILD
				IF NOT IS_BIT_SET(iTempEntitiesInZoneBS, iZoneIndicesOfTypeRuleEntityPrereq[i])
					PRINTLN("[Vehicles][Vehicle ", sVehState.iIndex, "] PROCESS_VEHICLE_IN_PREREQ_ZONES - Vehicle is in the same interior as zone: ", iZoneIndicesOfTypeRuleEntityPrereq[i])
				ENDIF
#ENDIF
				SET_BIT(iTempEntitiesInZoneBS, iZoneIndicesOfTypeRuleEntityPrereq[i])
			ENDIF
		ENDIF
	ENDFOR

ENDPROC

/// PURPOSE:
///    Processes whether the passed in placed vehicle should be the passed in team's current objective.
PROC PROCESS_VEH_OBJECTIVES_SERVER(INT iVeh, INT iTeam)

	IF !bIsLocalPlayerHost
		EXIT
	ENDIF
	
	IF GET_VEHICLE_RESPAWNS(iVeh) <= 0
		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
		OR NOT IS_VEHICLE_DRIVEABLE(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh]))
			IF MC_serverBD_4.iVehPriority[iVeh][iTeam] < FMMC_MAX_RULES
			OR IS_BIT_SET(MC_serverBD.iMissionCriticalVeh[iTeam], iVeh)
				IF NOT HAS_TEAM_FAILED(iTeam)
					CHECK_VEHICLE_FAILURE_FOR_TEAM(iVeh, iTeam, MC_serverBD_4.iVehPriority[iVeh][iTeam])
				ENDIF
			ENDIF
			
			EXIT
		ENDIF
	ENDIF
	
	IF MC_serverBD_4.iVehRule[iVeh][iTeam] = FMMC_OBJECTIVE_LOGIC_NONE
		EXIT
	ENDIF
	
	IF MC_serverBD_4.iVehPriority[iVeh][iTeam] >= FMMC_MAX_RULES
		EXIT
	ENDIF
	
	IF MC_serverBD_4.iVehPriority[iVeh][iTeam] > iCurrentHighPriority[iTeam]
		//Has an objective but the team has not reached it yet
		EXIT
	ENDIF
	
	PRINTLN("[OBJECTIVE_ENTITIES] PROCESS_VEH_OBJECTIVES_SERVER - Vehicle: ", iVeh, " Team: ", iTeam, " Objective is valid - Assigning. Priority: ",
				MC_serverBD_4.iVehPriority[iVeh][iTeam], " Rule: ", MC_serverBD_4.iVehRule[iVeh][iTeam])
		
	IF MC_serverBD_4.iVehPriority[iVeh][iTeam] <= MC_serverBD_4.iCurrentHighestPriority[iTeam]
		iTempNumPriorityRespawnVeh[iTeam] = iTempNumPriorityRespawnVeh[iTeam] + GET_VEHICLE_RESPAWNS(iVeh)
		PROCESS_VEHICLE_IN_PREREQ_ZONES(iVeh)
	ENDIF
			
	iCurrentHighPriority[iTeam] = MC_serverBD_4.iVehPriority[iVeh][iTeam]
	iTempVehMissionLogic[iTeam] = MC_serverBD_4.iVehRule[iVeh][iTeam]
	
	IF iOldHighPriority[iTeam] > iCurrentHighPriority[iTeam]
		iNumHighPriorityVeh[iTeam] = 0
		iOldHighPriority[iTeam] = iCurrentHighPriority[iTeam]
	ENDIF
	
	iNumHighPriorityVeh[iTeam]++
	
	RESET_TEMP_OBJECTIVE_VARS(iTeam, ciRULE_TYPE_VEHICLE)
	
	
ENDPROC
