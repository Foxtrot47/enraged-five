// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### System: GTA V Online Mission Controller - Local Player ------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Script Name: FM_Mission_Controller_2020.sc ------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Runs all instanced content created with the Mission Creator.
// ##### Runs pre-game initialisation and calls the main state machine update.
// ##### No additional functionality should be added here.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### This script was forked from FM_Mission_Controller.sc at the beginning of 2020		 																				  
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

USING "FM_Mission_Controller_Spawning_2020.sch"
USING "FM_Mission_Controller_Wanted_2020.sch"

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Utility and Helper Functions ------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Helper functions and processing of the local player Blips.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL SHOULD_TEAM_BE_PASSIVE(INT iTeam)
	IF MC_playerBD[iLocalPart].iTeam != iTeam
	AND NOT bIsAnySpectator
		SWITCH iTeam
			CASE 0	RETURN IS_BIT_SET(g_FMMC_STRUCT.iBS_MissionTeamPassive, FMMC_TEAM_PASSIVE_0_0 + MC_playerBD[iLocalPart].iTeam)
			CASE 1	RETURN IS_BIT_SET(g_FMMC_STRUCT.iBS_MissionTeamPassive, FMMC_TEAM_PASSIVE_1_0 + MC_playerBD[iLocalPart].iTeam)
			CASE 2	RETURN IS_BIT_SET(g_FMMC_STRUCT.iBS_MissionTeamPassive, FMMC_TEAM_PASSIVE_2_0 + MC_playerBD[iLocalPart].iTeam)
			CASE 3	RETURN IS_BIT_SET(g_FMMC_STRUCT.iBS_MissionTeamPassive, FMMC_TEAM_PASSIVE_3_0 + MC_playerBD[iLocalPart].iTeam)
		ENDSWITCH
	ENDIF	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_SMOKE_SHOW()
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyThree, ciOptionsBS23_SmokeTrailRequiresMovement)
		RETURN GET_ENTITY_SPEED(LocalPlayerPed) > 1.0
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC BLOCK_ALL_PICKUPS_FOR_LOCAL_PLAYER(BOOL bBlock)
	INT i
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons - 1
		IF i < FMMC_MAX_WEAPONS
			IF DOES_PICKUP_EXIST(pipickup[i])
				SET_PICKUP_UNCOLLECTABLE(pipickup[i], bBlock)
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

PROC GET_SNACKS_STATUS(INT iTeam)
	IF MC_serverBD_4.iCurrentHighestPriority[ MC_playerBD[iPartToUse].iteam ] < FMMC_MAX_RULES
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThree[MC_serverBD_4.iCurrentHighestPriority[iTeam]], ciBS_RULE3_NO_SNACKING)
			IF NOT ARE_SNACKS_BLOCKED()
				BLOCK_SNACKS(TRUE)
			ENDIF
		ELSE
			IF ARE_SNACKS_BLOCKED()
				BLOCK_SNACKS(FALSE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC INT GET_TIME_SINCE_THE_LOCAL_PLAYER_GOT_SHOT()
	RETURN iLastTimePlayerGotShotInSec
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Spectator -------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Processes the Spectator Systems for the local player.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

///Updates the celebration screen while preloading the spectator cam: assumes that a celebration screen was already created and started.
PROC UPDATE_CELEBRATION_SCREEN_WHILE_SPECTATING()
	IF IS_A_STRAND_MISSION_BEING_INITIALISED()
		IF sCelebrationData.eCurrentStage = CELEBRATION_STAGE_PLAYING
			IF GET_SPEC_CAM_STATE(g_BossSpecData.specCamData) = eSPECCAMSTATE_WATCHING
				sCelebrationData.eCurrentStage = CELEBRATION_STAGE_WAITING_FOR_SPECTATOR_CAM
				EXIT
			ENDIF
		ENDIf
	ENDIF
	IF sCelebrationData.eCurrentStage = CELEBRATION_STAGE_PLAYING
		IF NOT IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_bCelebrationSummaryComplete) //This is flagged once the job is ready to go to the winner screen (or skycam).
			HIDE_UI_FOR_CELEBRATION_SCREEN_THIS_FRAME()
			DRAW_CELEBRATION_SCREEN_THIS_FRAME(sCelebrationData, FALSE)
		ENDIF
		
		IF GET_SPEC_CAM_STATE(g_BossSpecData.specCamData) = eSPECCAMSTATE_WATCHING
		AND HAS_NET_TIMER_EXPIRED(sCelebrationData.sCelebrationTimer, sCelebrationData.iEstimatedScreenDuration)
			STOP_CELEBRATION_SCREEN_ANIM(sCelebrationData, "EARLYDEATH")
			STOP_CELEBRATION_SCREEN_ANIM(sCelebrationData, "SUMMARY")
			RESET_CELEBRATION_SCREEN_STATE(sCelebrationData)
			SET_CELEBRATION_SCREEN_AS_ACTIVE(FALSE)
			
			RESET_NET_TIMER(sCelebrationData.sCelebrationTransitionTimer)
			START_NET_TIMER(sCelebrationData.sCelebrationTransitionTimer)
			
			CLEAR_SPEC_CAM_ACTIVATED_BY_CELEBRATION_SCREEN(g_BossSpecData)
			SET_SKYFREEZE_CLEAR(TRUE)
			SET_SKYBLUR_CLEAR()
			
			IF SHOULD_POSTFX_BE_WINNER_VERSION(TRUE)
				PLAY_CELEB_WIN_OUT_POST_FX()
			ELSE
				PLAY_CELEB_LOSE_OUT_POST_FX()
			ENDIF
			
			IF IS_AUDIO_SCENE_ACTIVE("MP_CELEB_SCREEN_SCENE")
				STOP_AUDIO_SCENE("MP_CELEB_SCREEN_SCENE")
			ENDIF
			
			IF IS_AUDIO_SCENE_ACTIVE("MP_LEADERBOARD_SCENE")
				STOP_AUDIO_SCENE("MP_LEADERBOARD_SCENE")
			ENDIF
			IF IS_AUDIO_SCENE_ACTIVE("MP_LEADERBOARD_PP_SCENE")
				STOP_AUDIO_SCENE("MP_LEADERBOARD_PP_SCENE")
			ENDIF
			
			PRINTLN("[NETCELEBRATION] UPDATE_CELEBRATION_SCREEN_WHILE_SPECTATING - Spectator cam is ready, unfreezing scren.")
			
			sCelebrationData.eCurrentStage = CELEBRATION_STAGE_WAITING_FOR_SPECTATOR_CAM
		ENDIF
	ENDIF
ENDPROC

PROC BAIL_FROM_FORCED_HEIST_SPECTATE()
	IF IS_BIT_SET(iLocalBoolCheck25, LBOOL25_FORCED_INTO_HEIST_SPECTATE)
		DISABLE_HEIST_SPECTATE(TRUE)
		CLEANUP_HEIST_SPECTATE_FOR_NON_HEISTS()
		SET_ON_LBD_GLOBAL(FALSE)
		MPGlobalsAmbience.piHeistSpectateTarget = INVALID_PLAYER_INDEX()
		PRINTLN("[JS] - PROCESS_FORCED_HEIST_SPECTATOR - Clearing forced spectate")
		CLEAR_BIT(iLocalBoolCheck25, LBOOL25_FORCED_INTO_HEIST_SPECTATE)
		MPGlobalsAmbience.bIgnoreInvalidToSpectateBail = FALSE
	ENDIF
ENDPROC

PROC PROCESS_FORCED_HEIST_SPECTATOR()
	INT iTeam = MC_PlayerBD[iLocalPart].iTeam
	INT iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]
	IF iRule < FMMC_MAX_RULES
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTen[iRule], ciBS_RULE10_FORCE_HEIST_SPECTATE)
		AND NOT IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam)
		AND NOT (g_bMissionEnding OR g_bMissionOver)
			PLAYER_INDEX toSpectate = INVALID_PLAYER_INDEX()
			IF MPGlobalsAmbience.piHeistSpectateTarget = INVALID_PLAYER_INDEX()
			OR MPGlobalsAmbience.piHeistSpectateTarget = LocalPlayer
			OR NOT IS_NET_PLAYER_OK(MPGlobalsAmbience.piHeistSpectateTarget, FALSE)
			OR NOT USING_HEIST_SPECTATE()
				INT iPart
				PARTICIPANT_INDEX tempPart
				PLAYER_INDEX tempPlayer
				PRINTLN("[PLAYER_LOOP] - PROCESS_FORCED_HEIST_SPECTATOR")
				FOR iPart = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION() - 1
					IF iPart != iLocalPart
						tempPart = INT_TO_PARTICIPANTINDEX(iPart)
						IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
						AND NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_ANY_SPECTATOR)
						AND NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet,PBBOOL_PLAYER_FAIL)
						AND IS_BIT_SET(MC_playerBD[iPart].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
						AND NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_FINISHED)
							tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
							IF IS_NET_PLAYER_OK(tempPlayer, FALSE)
							AND NOT IS_PARTICIPANT_A_SPECTATOR(iPart)	
							AND NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(tempPlayer)].iSpecInfoBitset, SPEC_INFO_BS_USING_HEIST_SPECTATE)
							AND NOT IS_MP_DECORATOR_BIT_SET(tempPlayer, MP_DECORATOR_BS_INVALID_TO_SPECTATE)
								toSpectate = tempPlayer
								PRINTLN("[JS] - PROCESS_FORCED_HEIST_SPECTATOR - new spectator target: ", iPart)
								BREAKLOOP
							ENDIF
						ENDIF
					ENDIF
				ENDFOR
				
				IF toSpectate != INVALID_PLAYER_INDEX()
				AND IS_NET_PLAYER_OK(toSpectate, FALSE)
					MPGlobalsAmbience.piHeistSpectateTarget = toSpectate
				ELSE
					PRINTLN("[JS] - PROCESS_FORCED_HEIST_SPECTATOR - No valid player to spectate")
					BAIL_FROM_FORCED_HEIST_SPECTATE()
				ENDIF
			ENDIF
				
			IF NOT USING_HEIST_SPECTATE()
				IF toSpectate != INVALID_PLAYER_INDEX()
				AND IS_NET_PLAYER_OK(toSpectate)
					CLEAR_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_STOP_HEIST_SPECTATE_NOW)
					DISABLE_HEIST_SPECTATE(FALSE)
					DO_SCREEN_FADE_OUT(500)
					BEGIN_TEXT_COMMAND_BUSYSPINNER_ON("") 
					END_TEXT_COMMAND_BUSYSPINNER_ON(ENUM_TO_INT(LOADING_ICON_LOADING)) 
					SET_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_DO_HEIST_SPECTATE)
					SET_BIT(GlobalplayerBD[NATIVE_TO_INT(LocalPlayer)].iSpecInfoBitset, SPEC_INFO_BS_USING_HEIST_SPECTATE)
					NETWORK_SET_OVERRIDE_SPECTATOR_MODE(TRUE)
					SET_BIT(iLocalBoolCheck25, LBOOL25_FORCED_INTO_HEIST_SPECTATE)
					MPGlobalsAmbience.bIgnoreInvalidToSpectateBail = TRUE
					PRINTLN("[JS] - PROCESS_FORCED_HEIST_SPECTATOR - Going to heist spectate, chosen player ",GET_PLAYER_NAME(toSpectate))
				ENDIF
			ELSE
				SET_SPECTATOR_CAN_QUIT(FALSE)
				SET_SPECTATOR_HUD_BLOCK_CIRCLE_INSTRUCTION(g_BossSpecData.specHUDData, TRUE)
			ENDIF
		ELSE
			BAIL_FROM_FORCED_HEIST_SPECTATE()
			IF NOT (g_bMissionEnding OR g_bMissionOver)
				IF IS_MP_DECORATOR_BIT_SET(LocalPlayer, MP_DECORATOR_BS_INVALID_TO_SPECTATE)
					INT iTeamToCheck
					FOR iTeamToCheck = 0 TO MC_serverBD.iNumberOfTeams - 1
						IF MC_ServerBD_4.iCurrentHighestPriority[iTeamToCheck] < FMMC_MAX_RULES
							IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeamToCheck].iRuleBitsetTen[MC_ServerBD_4.iCurrentHighestPriority[iTeamToCheck]], ciBS_RULE10_FORCE_HEIST_SPECTATE)
								CLEAR_MP_DECORATOR_BIT(LocalPlayer, MP_DECORATOR_BS_INVALID_TO_SPECTATE)
								PRINTLN("[JS] - PROCESS_FORCED_HEIST_SPECTATOR - Clearing MP_DECORATOR_BS_INVALID_TO_SPECTATE")
								BREAKLOOP
							ENDIF
						ENDIF
					ENDFOR
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Special Case Processing - Airstrike  -------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Handles airstrike functionality.  --------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC SET_GLOBAL_AIRSTRIKE_COORDS(VECTOR vTargetCoords)
	PRINTLN("[RCC MISSION][AIRSTRIKE] - SET_GLOBAL_AIRSTRIKE_COORDS Setting g_vAirstrikeCoords to ", vTargetCoords)
	g_vAirstrikeCoords = vTargetCoords
ENDPROC

FUNC ENTITY_INDEX GET_AIRSTRIKE_TARGET_ENTITY(INT iEntityType, INT iEntityID)
	IF iEntityType = ciAIRSTRIKE_TARGET_ENTITY_TYPE_PED
		IF iEntityID > -1
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niPed[iEntityID])
				RETURN CONVERT_TO_ENTITY_INDEX(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iEntityID]))
			ENDIF
		ENDIF
	ELIF iEntityType = ciAIRSTRIKE_TARGET_ENTITY_TYPE_VEHICLE
		IF iEntityID > -1
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iEntityID])
				RETURN CONVERT_TO_ENTITY_INDEX(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iEntityID]))
			ENDIF
		ENDIF
	ELIF iEntityType = ciAIRSTRIKE_TARGET_ENTITY_TYPE_OBJECT
		IF iEntityID > -1
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GET_OBJECT_NET_ID(iEntityID))
				RETURN CONVERT_TO_ENTITY_INDEX(NET_TO_OBJ(GET_OBJECT_NET_ID(iEntityID)))
			ENDIF
		ENDIF
	ENDIF
	RETURN NULL
ENDFUNC

PROC GET_AIRSTRIKE_TARGET_COORDS(VECTOR &vTargetCoords, INT iTeam, INT iRule)

	INT iEntityType = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sAirstrikeStruct[iRule].iEntityType
	INT iEntityID = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sAirstrikeStruct[iRule].iEntityID

	SWITCH iEntityType
		CASE ciAIRSTRIKE_TARGET_ENTITY_TYPE_PED
			IF iEntityID > -1
			AND DOES_ENTITY_EXIST(GET_AIRSTRIKE_TARGET_ENTITY(iEntityType, iEntityID))
				vTargetCoords = GET_ENTITY_COORDS(GET_AIRSTRIKE_TARGET_ENTITY(iEntityType, iEntityID), FALSE)
				PRINTLN("[RCC MISSION][AIRSTRIKE] GET_AIRSTRIKE_TARGET_COORDS - PED - setting vTargetCoords to: ", vTargetCoords)
			ELSE
				PRINTLN("[RCC MISSION][AIRSTRIKE] No Entity ID for Ped")
			ENDIF
		BREAK
		CASE ciAIRSTRIKE_TARGET_ENTITY_TYPE_VEHICLE
			IF iEntityID > -1
			AND DOES_ENTITY_EXIST(GET_AIRSTRIKE_TARGET_ENTITY(iEntityType, iEntityID))
				vTargetCoords = GET_ENTITY_COORDS(GET_AIRSTRIKE_TARGET_ENTITY(iEntityType, iEntityID), FALSE)
				PRINTLN("[RCC MISSION][AIRSTRIKE] GET_AIRSTRIKE_TARGET_COORDS  - VEHICLE - setting vTargetCoords to: ", vTargetCoords)
			ELSE
				PRINTLN("[RCC MISSION][AIRSTRIKE] No Entity ID for Vehicle")
			ENDIF
		BREAK
		CASE ciAIRSTRIKE_TARGET_ENTITY_TYPE_OBJECT
			IF iEntityID > -1
			AND DOES_ENTITY_EXIST(GET_AIRSTRIKE_TARGET_ENTITY(iEntityType, iEntityID))
				vTargetCoords = GET_ENTITY_COORDS(GET_AIRSTRIKE_TARGET_ENTITY(iEntityType, iEntityID), FALSE)
				PRINTLN("[RCC MISSION][AIRSTRIKE] GET_AIRSTRIKE_TARGET_COORDS - OBJECT - setting vTargetCoords to: ", vTargetCoords)
			ELSE
				PRINTLN("[RCC MISSION][AIRSTRIKE] No Entity ID for Object")
			ENDIF
		BREAK
		CASE ciAIRSTRIKE_TARGET_ENTITY_TYPE_LOCATION
			IF iEntityID > -1
			AND DOES_ENTITY_EXIST(GET_AIRSTRIKE_TARGET_ENTITY(iEntityType, iEntityID))
				vTargetCoords = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iEntityID].vLoc[0]
				PRINTLN("[RCC MISSION][AIRSTRIKE] GET_AIRSTRIKE_TARGET_COORDS - LOCATION - setting vTargetCoords to: ", vTargetCoords)
			ELSE
				PRINTLN("[RCC MISSION][AIRSTRIKE] No Entity ID for Location")
			ENDIF
		BREAK
		DEFAULT
			vTargetCoords = <<0,0,0>>
			PRINTLN("[RCC MISSION][AIRSTRIKE] GET_AIRSTRIKE_TARGET_COORDS - DEFAULT - setting vTargetCoords to: ", vTargetCoords, " Mission Rule ", iRule, " probably doesn't have entity type set")
		BREAK
	ENDSWITCH
ENDPROC

FUNC VECTOR GET_AIRSTRIKE_OFFSET_COORDS(FLOAT fMinRadius, FLOAT fMaxRadius, VECTOR vDirection)
	IF fMaxRadius < fMinRadius
		fMaxRadius = fMinRadius
	ENDIF
	
	VECTOR vDir = GET_VECTOR_OF_LENGTH(vDirection, GET_RANDOM_FLOAT_IN_RANGE(fMinRadius, fMaxRadius))
	
	RETURN vDir
ENDFUNC

FUNC FLOAT GET_RANDOM_ROTATION_FOR_AIRSTRIKE(FLOAT fLow, FLOAT fHigh)
	IF GET_RANDOM_BOOL() //Positive
		RETURN GET_RANDOM_FLOAT_IN_RANGE(fLow, fHigh)
	ELSE //Negative
		RETURN GET_RANDOM_FLOAT_IN_RANGE(-fHigh, -fLow)
	ENDIF
ENDFUNC

PROC UPDATE_AIRSTRIKE_OFFSET_VECTOR(INT iEntityType, INT iEntityID, INT iTeam, INT iRule)
	IF DOES_ENTITY_EXIST(GET_AIRSTRIKE_TARGET_ENTITY(iEntityType, iEntityID))
		VECTOR vTempForward = GET_ENTITY_FORWARD_VECTOR(GET_AIRSTRIKE_TARGET_ENTITY(iEntityType, iEntityID))
		
		ROTATE_VECTOR_FMMC(vTempForward, <<0,0,GET_RANDOM_ROTATION_FOR_AIRSTRIKE(TO_FLOAT(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sAirstrikeStruct[iRule].iOffsetLowAngle),
																					TO_FLOAT(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sAirstrikeStruct[iRule].iOffsetHighAngle))>>)
		vAirstrikeOffset = GET_AIRSTRIKE_OFFSET_COORDS(TO_FLOAT(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sAirstrikeStruct[iRule].iOffsetDistanceMin),
														TO_FLOAT(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sAirstrikeStruct[iRule].iOffsetDistanceMax), vTempForward)
	ELSE
		PRINTLN("[RCC MISSION][UPDATE_AIRSTRIKE_OFFSET_VECTOR] iEntityID does not exist!")
	ENDIF
ENDPROC

FUNC VECTOR GET_AIRSTIKE_OFFSET(INT iOffsetNum)
	SWITCH iOffsetNum
		CASE 0		RETURN <<0,0,0>>
		CASE 1		RETURN <<5,5,0>>
		CASE 2		RETURN <<-5,-5,0>>
		CASE 3		RETURN <<-5,5,0>>
		CASE 4		RETURN <<5,-5,0>>
		CASE 5		RETURN <<10,5,0>>
		CASE 6		RETURN <<10,10,0>>
		CASE 7		RETURN <<-10, -10,0>>
		CASE 8		RETURN <<-10, 10, 0>>
		CASE 9		RETURN <<10, -10, 0>>
	ENDSWITCH
	RETURN <<0,0,0>>
ENDFUNC

PROC DO_SCRIPTED_AIRSTRIKE(VECTOR vTargetCoords, INT iNumRockets)

	VECTOR vStartPos = vTargetCoords
	VECTOR vEndPos = vTargetCoords
	
	INT iLongTimer
	INT iShortTimer
	
	IF IS_BIT_SET(iLocalBoolCheck23, LBOOL23_AIRSTRIKE_SPEED_BAR_FIRED)
		iLongTimer = 250
		iShortTimer = 150
	ELSE
		iLongTimer = 750
		iShortTimer = 500
	ENDIF
	
	vStartPos.z += 40
	
	IF iAirstrikeExplosionCount >= iNumRockets
		PRINTLN("[RCC MISSION] DO_SCRIPTED_AIRSTRIKE - Exiting due to rocket count")
		SET_BIT(iLocalBoolCheck24, LBOOL24_AIRSTRIKE_FINAL_SHOT_FIRED)
		EXIT
	ENDIF
		
	SWITCH iAirstrikeExplosionCount
		CASE 0
			IF NOT HAS_NET_TIMER_STARTED(tdAirstrikeRocketTimer)
				IF NOT IS_BIT_SET(iAirstrikeRocketFiredBitSet, iAirstrikeExplosionCount)
					CLEAR_BIT(iLocalBoolCheck24, LBOOL24_AIRSTRIKE_FINAL_SHOT_FIRED)
					GET_GROUND_Z_FOR_3D_COORD(vEndPos+<<0,0,10>>, vEndPos.z)
					vEndPos = vEndPos + <<0.1, 0.1, 0.0>>
					START_NET_TIMER(tdAirstrikeRocketTimer)
					SHOOT_SINGLE_BULLET_BETWEEN_COORDS_IGNORE_ENTITY_NEW(vStartPos,vEndPos, 200, TRUE, WEAPONTYPE_AIRSTRIKE_ROCKET, NULL, TRUE, FALSE)
					PRINTLN("[RCC MISSION] DO_SCRIPTED_AIRSTRIKE - Rocket ", iAirstrikeExplosionCount, " launched.")
					PRINTLN("[RCC MISSION] DO_SCRIPTED_AIRSTRIKE - Rocket ", iAirstrikeExplosionCount, " Starting Coord: ", vStartPos," Ending Coord: ", vEndPos)
					SET_BIT(iAirstrikeRocketFiredBitSet, iAirstrikeExplosionCount)
					SET_BIT(iLocalBoolCheck24, LBOOL24_AIRSTRIKE_IN_PROGRESS)
				ENDIF
			ELSE
				IF HAS_NET_TIMER_EXPIRED(tdAirstrikeRocketTimer, iLongTimer)
					RESET_NET_TIMER(tdAirstrikeRocketTimer)
					iAirstrikeExplosionCount++
					PRINTLN("[RCC MISSION] DO_SCRIPTED_AIRSTRIKE - Incrementing iAirstrikeExplosionCount to: ", iAirstrikeExplosionCount)
				ENDIF
			ENDIF
		BREAK
		CASE 1
			IF NOT HAS_NET_TIMER_STARTED(tdAirstrikeRocketTimer)
				IF NOT IS_BIT_SET(iAirstrikeRocketFiredBitSet, iAirstrikeExplosionCount)
					vStartPos = vTargetCoords + GET_AIRSTIKE_OFFSET(iAirstrikeExplosionCount)
					vEndPos = vTargetCoords + GET_AIRSTIKE_OFFSET(iAirstrikeExplosionCount)
					vEndPos = vEndPos + <<0.1, 0.1, 0.0>>
					GET_GROUND_Z_FOR_3D_COORD(vEndPos+<<0,0,10>>, vEndPos.z)
					ADD_EXPLOSION(vEndPos,EXP_TAG_PLANE_ROCKET,1,FALSE) //Using add explosion with no sound as sfx was dropping out
//					SHOOT_SINGLE_BULLET_BETWEEN_COORDS_IGNORE_ENTITY_NEW(vStartPos,vEndPos, 200, TRUE, WEAPONTYPE_AIRSTRIKE_ROCKET, NULL, TRUE, FALSE)
					PRINTLN("[RCC MISSION] DO_SCRIPTED_AIRSTRIKE - Rocket ", iAirstrikeExplosionCount, " launched.")
					PRINTLN("[RCC MISSION] DO_SCRIPTED_AIRSTRIKE - Rocket ", iAirstrikeExplosionCount, " Starting Coord: ", vStartPos," Ending Coord: ", vEndPos)
					START_NET_TIMER(tdAirstrikeRocketTimer)
					SET_BIT(iAirstrikeRocketFiredBitSet, iAirstrikeExplosionCount)
				ENDIF
			ELSE
				IF HAS_NET_TIMER_EXPIRED(tdAirstrikeRocketTimer, iShortTimer)
					RESET_NET_TIMER(tdAirstrikeRocketTimer)
					iAirstrikeExplosionCount++
					PRINTLN("[RCC MISSION] DO_SCRIPTED_AIRSTRIKE - Incrementing iAirstrikeExplosionCount to: ", iAirstrikeExplosionCount)
				ENDIF
			ENDIF
		BREAK
		CASE 2
			IF NOT HAS_NET_TIMER_STARTED(tdAirstrikeRocketTimer)
				IF NOT IS_BIT_SET(iAirstrikeRocketFiredBitSet, iAirstrikeExplosionCount)
					vStartPos = vTargetCoords + GET_AIRSTIKE_OFFSET(iAirstrikeExplosionCount)
					vStartPos.z += 40
					vEndPos = vTargetCoords + GET_AIRSTIKE_OFFSET(iAirstrikeExplosionCount)
					vEndPos = vEndPos + <<0.1, 0.1, 0.0>>
					GET_GROUND_Z_FOR_3D_COORD(vEndPos+<<0,0,10>>, vEndPos.z)
					SHOOT_SINGLE_BULLET_BETWEEN_COORDS_IGNORE_ENTITY_NEW(vStartPos,vEndPos, 200, TRUE, WEAPONTYPE_AIRSTRIKE_ROCKET, NULL, TRUE, FALSE)
					PRINTLN("[RCC MISSION] DO_SCRIPTED_AIRSTRIKE - Rocket ", iAirstrikeExplosionCount, " launched.")
					PRINTLN("[RCC MISSION] DO_SCRIPTED_AIRSTRIKE - Rocket ", iAirstrikeExplosionCount, " Starting Coord: ", vStartPos," Ending Coord: ", vEndPos)
					START_NET_TIMER(tdAirstrikeRocketTimer)
					SET_BIT(iAirstrikeRocketFiredBitSet, iAirstrikeExplosionCount)
				ENDIF
			ELSE
				IF HAS_NET_TIMER_EXPIRED(tdAirstrikeRocketTimer, iShortTimer)
					RESET_NET_TIMER(tdAirstrikeRocketTimer)
					iAirstrikeExplosionCount++
					PRINTLN("[RCC MISSION] DO_SCRIPTED_AIRSTRIKE - Incrementing iAirstrikeExplosionCount to: ", iAirstrikeExplosionCount)
				ENDIF
			ENDIF
		BREAK
		CASE 3
			IF NOT HAS_NET_TIMER_STARTED(tdAirstrikeRocketTimer)
				IF NOT IS_BIT_SET(iAirstrikeRocketFiredBitSet, iAirstrikeExplosionCount)
					vStartPos = vTargetCoords + GET_AIRSTIKE_OFFSET(iAirstrikeExplosionCount)
					vStartPos.z += 40
					vEndPos = vTargetCoords + GET_AIRSTIKE_OFFSET(iAirstrikeExplosionCount)
					vEndPos = vEndPos + <<0.1, 0.1, 0.0>>
					GET_GROUND_Z_FOR_3D_COORD(vEndPos+<<0,0,10>>, vEndPos.z)
					ADD_EXPLOSION(vEndPos,EXP_TAG_PLANE_ROCKET,1,FALSE) //Using add explosion with no sound as sfx was dropping out
//					SHOOT_SINGLE_BULLET_BETWEEN_COORDS_IGNORE_ENTITY_NEW(vStartPos,vEndPos, 200, TRUE, WEAPONTYPE_AIRSTRIKE_ROCKET, NULL, TRUE, FALSE)
					PRINTLN("[RCC MISSION] DO_SCRIPTED_AIRSTRIKE - Rocket ", iAirstrikeExplosionCount, " launched.")
					PRINTLN("[RCC MISSION] DO_SCRIPTED_AIRSTRIKE - Rocket ", iAirstrikeExplosionCount, " Starting Coord: ", vStartPos," Ending Coord: ", vEndPos)
					START_NET_TIMER(tdAirstrikeRocketTimer)
					SET_BIT(iAirstrikeRocketFiredBitSet, iAirstrikeExplosionCount)
				ENDIF
			ELSE
				IF HAS_NET_TIMER_EXPIRED(tdAirstrikeRocketTimer, iLongTimer)
					RESET_NET_TIMER(tdAirstrikeRocketTimer)
					iAirstrikeExplosionCount++
					PRINTLN("[RCC MISSION] DO_SCRIPTED_AIRSTRIKE - Incrementing iAirstrikeExplosionCount to: ", iAirstrikeExplosionCount)
				ENDIF
			ENDIF
		BREAK
		CASE 4
			IF NOT HAS_NET_TIMER_STARTED(tdAirstrikeRocketTimer)
				IF NOT IS_BIT_SET(iAirstrikeRocketFiredBitSet, iAirstrikeExplosionCount)
					vStartPos = vTargetCoords + GET_AIRSTIKE_OFFSET(iAirstrikeExplosionCount)
					vStartPos.z += 40
					vEndPos = vTargetCoords + GET_AIRSTIKE_OFFSET(iAirstrikeExplosionCount)
					vEndPos = vEndPos + <<0.1, 0.1, 0.0>>
					GET_GROUND_Z_FOR_3D_COORD(vEndPos+<<0,0,10>>, vEndPos.z)
					SHOOT_SINGLE_BULLET_BETWEEN_COORDS_IGNORE_ENTITY_NEW(vStartPos,vEndPos, 200, TRUE, WEAPONTYPE_AIRSTRIKE_ROCKET, NULL, TRUE, FALSE)
					PRINTLN("[RCC MISSION] DO_SCRIPTED_AIRSTRIKE - Rocket ", iAirstrikeExplosionCount, " launched.")
					PRINTLN("[RCC MISSION] DO_SCRIPTED_AIRSTRIKE - Rocket ", iAirstrikeExplosionCount, " Starting Coord: ", vStartPos," Ending Coord: ", vEndPos)
					START_NET_TIMER(tdAirstrikeRocketTimer)
					SET_BIT(iAirstrikeRocketFiredBitSet, iAirstrikeExplosionCount)
				ENDIF
			ELSE
				IF HAS_NET_TIMER_EXPIRED(tdAirstrikeRocketTimer, iShortTimer)
					RESET_NET_TIMER(tdAirstrikeRocketTimer)
					iAirstrikeExplosionCount++
					PRINTLN("[RCC MISSION] DO_SCRIPTED_AIRSTRIKE - Incrementing iAirstrikeExplosionCount to: ", iAirstrikeExplosionCount)
				ENDIF
			ENDIF
		BREAK
		CASE 5
			IF NOT HAS_NET_TIMER_STARTED(tdAirstrikeRocketTimer)
				IF NOT IS_BIT_SET(iAirstrikeRocketFiredBitSet, iAirstrikeExplosionCount)
					vStartPos = vTargetCoords + GET_AIRSTIKE_OFFSET(iAirstrikeExplosionCount)
					vStartPos.z += 40
					vEndPos = vTargetCoords + GET_AIRSTIKE_OFFSET(iAirstrikeExplosionCount)
					vEndPos = vEndPos + <<0.1, 0.1, 0.0>>
					GET_GROUND_Z_FOR_3D_COORD(vEndPos+<<0,0,10>>, vEndPos.z)
					ADD_EXPLOSION(vEndPos,EXP_TAG_PLANE_ROCKET,1,FALSE) //Using add explosion with no sound as sfx was dropping out
//					SHOOT_SINGLE_BULLET_BETWEEN_COORDS_IGNORE_ENTITY_NEW(vStartPos,vEndPos, 200, TRUE, WEAPONTYPE_AIRSTRIKE_ROCKET, NULL, TRUE, FALSE)
					PRINTLN("[RCC MISSION] DO_SCRIPTED_AIRSTRIKE - Rocket ", iAirstrikeExplosionCount, " launched.")
					PRINTLN("[RCC MISSION] DO_SCRIPTED_AIRSTRIKE - Rocket ", iAirstrikeExplosionCount, " Starting Coord: ", vStartPos," Ending Coord: ", vEndPos)
					START_NET_TIMER(tdAirstrikeRocketTimer)
					SET_BIT(iAirstrikeRocketFiredBitSet, iAirstrikeExplosionCount)
				ENDIF
			ELSE
				IF HAS_NET_TIMER_EXPIRED(tdAirstrikeRocketTimer, iLongTimer)
					RESET_NET_TIMER(tdAirstrikeRocketTimer)
					iAirstrikeExplosionCount++
					PRINTLN("[RCC MISSION] DO_SCRIPTED_AIRSTRIKE - Incrementing iAirstrikeExplosionCount to: ", iAirstrikeExplosionCount)
				ENDIF
			ENDIF
		BREAK
		CASE 6
			IF NOT HAS_NET_TIMER_STARTED(tdAirstrikeRocketTimer)
				IF NOT IS_BIT_SET(iAirstrikeRocketFiredBitSet, iAirstrikeExplosionCount)
					vStartPos = vTargetCoords + GET_AIRSTIKE_OFFSET(iAirstrikeExplosionCount)
					vStartPos.z += 40
					vEndPos = vTargetCoords + GET_AIRSTIKE_OFFSET(iAirstrikeExplosionCount)
					vEndPos = vEndPos + <<0.1, 0.1, 0.0>>
					GET_GROUND_Z_FOR_3D_COORD(vEndPos+<<0,0,10>>, vEndPos.z)
					SHOOT_SINGLE_BULLET_BETWEEN_COORDS_IGNORE_ENTITY_NEW(vStartPos,vEndPos, 200, TRUE, WEAPONTYPE_AIRSTRIKE_ROCKET, NULL, TRUE, FALSE)
					PRINTLN("[RCC MISSION] DO_SCRIPTED_AIRSTRIKE - Rocket ", iAirstrikeExplosionCount, " launched.")
					PRINTLN("[RCC MISSION] DO_SCRIPTED_AIRSTRIKE - Rocket ", iAirstrikeExplosionCount, " Starting Coord: ", vStartPos," Ending Coord: ", vEndPos)
					START_NET_TIMER(tdAirstrikeRocketTimer)
					SET_BIT(iAirstrikeRocketFiredBitSet, iAirstrikeExplosionCount)
				ENDIF
			ELSE
				IF HAS_NET_TIMER_EXPIRED(tdAirstrikeRocketTimer, iShortTimer)
					RESET_NET_TIMER(tdAirstrikeRocketTimer)
					iAirstrikeExplosionCount++
					PRINTLN("[RCC MISSION] DO_SCRIPTED_AIRSTRIKE - Incrementing iAirstrikeExplosionCount to: ", iAirstrikeExplosionCount)
				ENDIF
			ENDIF
		BREAK
		CASE 7
			IF NOT HAS_NET_TIMER_STARTED(tdAirstrikeRocketTimer)
				IF NOT IS_BIT_SET(iAirstrikeRocketFiredBitSet, iAirstrikeExplosionCount)
					vStartPos = vTargetCoords + GET_AIRSTIKE_OFFSET(iAirstrikeExplosionCount)
					vStartPos.z += 40
					vEndPos = vTargetCoords + GET_AIRSTIKE_OFFSET(iAirstrikeExplosionCount)
					vEndPos = vEndPos + <<0.1, 0.1, 0.0>>
					GET_GROUND_Z_FOR_3D_COORD(vEndPos+<<0,0,10>>, vEndPos.z)
					ADD_EXPLOSION(vEndPos,EXP_TAG_PLANE_ROCKET,1,FALSE) //Using add explosion with no sound as sfx was dropping out
//					SHOOT_SINGLE_BULLET_BETWEEN_COORDS_IGNORE_ENTITY_NEW(vStartPos,vEndPos, 200, TRUE, WEAPONTYPE_AIRSTRIKE_ROCKET, NULL, TRUE, FALSE)
					PRINTLN("[RCC MISSION] DO_SCRIPTED_AIRSTRIKE - Rocket ", iAirstrikeExplosionCount, " launched.")
					PRINTLN("[RCC MISSION] DO_SCRIPTED_AIRSTRIKE - Rocket ", iAirstrikeExplosionCount, " Starting Coord: ", vStartPos," Ending Coord: ", vEndPos)
					START_NET_TIMER(tdAirstrikeRocketTimer)
					SET_BIT(iAirstrikeRocketFiredBitSet, iAirstrikeExplosionCount)
				ENDIF
			ELSE
				IF HAS_NET_TIMER_EXPIRED(tdAirstrikeRocketTimer, iShortTimer)
					RESET_NET_TIMER(tdAirstrikeRocketTimer)
					iAirstrikeExplosionCount++
					PRINTLN("[RCC MISSION] DO_SCRIPTED_AIRSTRIKE - Incrementing iAirstrikeExplosionCount to: ", iAirstrikeExplosionCount)
				ENDIF
			ENDIF
		BREAK
		CASE 8
			IF NOT HAS_NET_TIMER_STARTED(tdAirstrikeRocketTimer)
				IF NOT IS_BIT_SET(iAirstrikeRocketFiredBitSet, iAirstrikeExplosionCount)
					vStartPos = vTargetCoords + GET_AIRSTIKE_OFFSET(iAirstrikeExplosionCount)
					vStartPos.z += 40
					vEndPos = vTargetCoords + GET_AIRSTIKE_OFFSET(iAirstrikeExplosionCount)
					vEndPos = vEndPos + <<0.1, 0.1, 0.0>>
					GET_GROUND_Z_FOR_3D_COORD(vEndPos+<<0,0,10>>, vEndPos.z)
					SHOOT_SINGLE_BULLET_BETWEEN_COORDS_IGNORE_ENTITY_NEW(vStartPos,vEndPos, 200, TRUE, WEAPONTYPE_AIRSTRIKE_ROCKET, NULL, TRUE, FALSE)
					PRINTLN("[RCC MISSION] DO_SCRIPTED_AIRSTRIKE - Rocket ", iAirstrikeExplosionCount, " launched.")
					PRINTLN("[RCC MISSION] DO_SCRIPTED_AIRSTRIKE - Rocket ", iAirstrikeExplosionCount, " Starting Coord: ", vStartPos," Ending Coord: ", vEndPos)
					START_NET_TIMER(tdAirstrikeRocketTimer)
					SET_BIT(iAirstrikeRocketFiredBitSet, iAirstrikeExplosionCount)
				ENDIF
			ELSE
				IF HAS_NET_TIMER_EXPIRED(tdAirstrikeRocketTimer, iShortTimer)
					RESET_NET_TIMER(tdAirstrikeRocketTimer)
					iAirstrikeExplosionCount++
					PRINTLN("[RCC MISSION] DO_SCRIPTED_AIRSTRIKE - Incrementing iAirstrikeExplosionCount to: ", iAirstrikeExplosionCount)
				ENDIF
			ENDIF
		BREAK
		CASE 9
			IF NOT HAS_NET_TIMER_STARTED(tdAirstrikeRocketTimer)
				IF NOT IS_BIT_SET(iAirstrikeRocketFiredBitSet, iAirstrikeExplosionCount)
					vStartPos = vTargetCoords + GET_AIRSTIKE_OFFSET(iAirstrikeExplosionCount)
					vStartPos.z += 40
					vEndPos = vTargetCoords + GET_AIRSTIKE_OFFSET(iAirstrikeExplosionCount)
					vEndPos = vEndPos + <<0.1, 0.1, 0.0>>
					GET_GROUND_Z_FOR_3D_COORD(vEndPos+<<0,0,10>>, vEndPos.z)
					ADD_EXPLOSION(vEndPos,EXP_TAG_PLANE_ROCKET,1,FALSE) //Using add explosion with no sound as sfx was dropping out
//					SHOOT_SINGLE_BULLET_BETWEEN_COORDS_IGNORE_ENTITY_NEW(vStartPos,vEndPos, 200, TRUE, WEAPONTYPE_AIRSTRIKE_ROCKET, NULL, TRUE, FALSE)
					PRINTLN("[RCC MISSION] DO_SCRIPTED_AIRSTRIKE - Rocket ", iAirstrikeExplosionCount, " launched.")
					PRINTLN("[RCC MISSION] DO_SCRIPTED_AIRSTRIKE - Rocket ", iAirstrikeExplosionCount, " Starting Coord: ", vStartPos," Ending Coord: ", vEndPos)
					SET_BIT(iAirstrikeRocketFiredBitSet, iAirstrikeExplosionCount)
					iAirstrikeExplosionCount++
					SET_BIT(iLocalBoolCheck24, LBOOL24_AIRSTRIKE_FINAL_SHOT_FIRED)
					PRINTLN("[RCC MISSION] DO_SCRIPTED_AIRSTRIKE - Incrementing iAirstrikeExplosionCount to: ", iAirstrikeExplosionCount)
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
ENDPROC

PROC ACTIVATE_AIRSTRIKE_AND_SET_COORDS(FLOAT fXOffset = 0.0, FLOAT fYOffset = 0.0, BOOL bSpecificCoords = FALSE)

	PRINTLN("[RCC MISSION][AIRSTRIKE] Calling ACTIVATE_AIRSTRIKE_AND_SET_COORDS")
		
	VECTOR tempTargetCoords
	
	IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam] < FMMC_MAX_RULES
		IF bSpecificCoords
			tempTargetCoords = g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].sAirstrikeStruct[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]].vTargetVector
		ELSE	
			GET_AIRSTRIKE_TARGET_COORDS(tempTargetCoords, MC_playerBD[iPartToUse].iTeam, MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam])
		ENDIF
	ELSE
		tempTargetCoords = <<0,0,0>>
	ENDIF
	PRINTLN("[RCC MISSION][AIRSTRIKE][OFFSET] ACTIVATE_AIRSTRIKE_AND_SET_COORDS - tempTargetCoords Pre offset: ", tempTargetCoords)
	PRINTLN("[RCC MISSION][AIRSTRIKE][OFFSET] ACTIVATE_AIRSTRIKE_AND_SET_COORDS - fXOffset: ", fXOffset, " Y Offset: ", fYOffset)
	tempTargetCoords.x += fXOffset
	tempTargetCoords.y += fYOffset
	PRINTLN("[RCC MISSION][AIRSTRIKE][OFFSET] ACTIVATE_AIRSTRIKE_AND_SET_COORDS - tempTargetCoords post offset: ", tempTargetCoords)
	SET_GLOBAL_AIRSTRIKE_COORDS(tempTargetCoords)
	PRINTLN("[RCC MISSION][AIRSTRIKE][CALL] Calling DO_SCRIPTED_AIRSTRIKE from: Activate airstrike")
	
	IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam] < FMMC_MAX_RULES
		IF g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].sAirstrikeStruct[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]].iAirstrikeType = ciAIRSTRIKE_TYPE_MISSILES
			PRINTLN("[RCC MISSION][AIRSTRIKE][OFFSET] ACTIVATE_AIRSTRIKE_AND_SET_COORDS CALLING DO_SCRIPTED_AIRSTRIKE - tempTargetCoords: ", tempTargetCoords)
			DO_SCRIPTED_AIRSTRIKE(tempTargetCoords, 10)
		ELIF g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].sAirstrikeStruct[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]].iAirstrikeType = ciAIRSTRIKE_TYPE_ORBITAL_CANNON
			PRINTLN("[RCC MISSION][AIRSTRIKE][OFFSET] ACTIVATE_AIRSTRIKE_AND_SET_COORDS CALLING FIRE_ORBITAL_CANNON - tempTargetCoords: ", tempTargetCoords)
			FIRE_ORBITAL_CANNON(tempTargetCoords)
		ENDIF
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Special Case Processing - General -----------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: This section covers the processing of non-creator vehicles associated with the local player.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_ALTITUDE_FAIL_EXPLOSION()
	
	IF bIsAnySpectator
		EXIT
	ENDIF
	
	VECTOR vPlayer = GET_ENTITY_COORDS(LocalPlayerPed, FALSE)
	
	IF DOES_ENTITY_EXIST(sAltitudeBarLocal.vehToUse)
		PRINTLN("[LM][AltitudeSystem] - PROCESS_ALTITUDE_FAIL_EXPLOSION - Cached Vehicle to use exists.")
		
		IF NETWORK_HAS_CONTROL_OF_ENTITY(sAltitudeBarLocal.vehToUse)
			NETWORK_EXPLODE_VEHICLE(sAltitudeBarLocal.vehToUse, TRUE, FALSE)
			PRINTLN("[LM][AltitudeSystem] - PROCESS_ALTITUDE_FAIL_EXPLOSION - Exploding vehicle now")
		ELSE
			PRINTLN("[LM][AltitudeSystem] - PROCESS_ALTITUDE_FAIL_EXPLOSION - We are not the owner.")
		ENDIF
		
		IF IS_ENTITY_SUBMERGED_IN_WATER(sAltitudeBarLocal.vehToUse, TRUE, WATER_SUBMERGED_LEVEL)
			PRINTLN("[LM][AltitudeSystem] - PROCESS_ALTITUDE_FAIL_EXPLOSION - Adding underwater Explosion.")
			ADD_EXPLOSION(vPlayer, EXP_TAG_MINE_UNDERWATER, 2.0)
		ELSE
			PRINTLN("[LM][AltitudeSystem] - PROCESS_ALTITUDE_FAIL_EXPLOSION - Adding regular Explosion.")
			ADD_EXPLOSION(vPlayer, EXP_TAG_GRENADELAUNCHER, 2.0)
		ENDIF
		
		IF NOT IS_FAKE_MULTIPLAYER_MODE_SET(TRUE)
			SET_ENTITY_HEALTH(LocalPlayerPed, 0)
		ENDIF
	ELSE		
		IF NOT IS_FAKE_MULTIPLAYER_MODE_SET(TRUE)
			SET_ENTITY_INVINCIBLE(LocalPlayerPed, FALSE)
		ENDIF		
		SET_ENTITY_PROOFS(LocalPlayerPed, FALSE, FALSE, FALSE, FALSE, FALSE)
		
		IF NOT IS_PED_SWIMMING(LocalPlayerPed)
			PRINTLN("[LM][AltitudeSystem] - PROCESS_ALTITUDE_FAIL_EXPLOSION - Killing Ourself.")
						
			IF IS_ENTITY_SUBMERGED_IN_WATER(LocalPlayerPed, TRUE, WATER_SUBMERGED_LEVEL)
				PRINTLN("[LM][AltitudeSystem] - PROCESS_ALTITUDE_FAIL_EXPLOSION - Adding underwater Explosion.")
				ADD_EXPLOSION(vPlayer, EXP_TAG_MINE_UNDERWATER, 1.0)				
			ELSE
				PRINTLN("[LM][AltitudeSystem] - PROCESS_ALTITUDE_FAIL_EXPLOSION - Adding regular Explosion.")
				ADD_EXPLOSION(vPlayer, EXP_TAG_GRENADELAUNCHER, 1.0)				
			ENDIF
			
			SET_PED_TO_RAGDOLL(LocalPlayerPed, 5000, 10000, TASK_RELAX)
		
			VECTOR vVelocity = GET_ENTITY_VELOCITY(LocalPlayerPed)
			SET_ENTITY_VELOCITY(LocalPlayerPed, vVelocity + <<0, 0, 15>>)
			SET_ENTITY_HEALTH(LocalPlayerPed, 0)
		ELSE
			PRINTLN("[LM][AltitudeSystem] - PROCESS_ALTITUDE_FAIL_EXPLOSION - Killing Ourself but in water.")
			IF NOT IS_FAKE_MULTIPLAYER_MODE_SET(TRUE)
				SET_ENTITY_HEALTH(LocalPlayerPed, 0)
			ENDIF
		ENDIF
	ENDIF	
ENDPROC

PROC PROCESS_ALTITUDE_FAIL_AIRSTRIKE()

ENDPROC

PROC PROCESS_ALTITUDE_FAIL_GANG_CHASE()

ENDPROC

PROC PROCESS_ALTITUDE_FAIL_WANTED()

ENDPROC
			
// Altitude
PROC PROCESS_UPDATE_ALTITUDE_BAR_CACHING(FLOAT fAltitude, FLOAT fFailTimer)
		
	IF HAS_NET_TIMER_STARTED(sAltitudeBarLocal.tdAltitudeUpdateTimer)
		IF HAS_NET_TIMER_EXPIRED_READ_ONLY(sAltitudeBarLocal.tdAltitudeUpdateTimer, 500)			
			RESET_NET_TIMER(sAltitudeBarLocal.tdAltitudeUpdateTimer)
			START_NET_TIMER(sAltitudeBarLocal.tdAltitudeUpdateTimer)
			SET_BIT(sAltitudeBarLocal.iBS, ciALTITUDE_BAR_LOCAL_UPDATE_NOW)
		ENDIF
	ELSE
		START_NET_TIMER(sAltitudeBarLocal.tdAltitudeUpdateTimer)
		SET_BIT(sAltitudeBarLocal.iBS, ciALTITUDE_BAR_LOCAL_UPDATE_NOW)
	ENDIF
		
	IF IS_BIT_SET(sAltitudeBarLocal.iBS, ciALTITUDE_BAR_LOCAL_UPDATE_NOW)
		CLEAR_BIT(sAltitudeBarLocal.iBS, ciALTITUDE_BAR_LOCAL_UPDATE_NOW)
		MC_playerBD[iLocalPart].fCurrentAltitude = fAltitude
		MC_playerBD[iLocalPart].fCurrentAltitudeFailTime = fFailTimer
	ENDIF
	
ENDPROC

PROC PROCESS_LOCAL_ALTITUDE()
	
	INT iTeam = MC_PlayerBD[iPartToUse].iTeam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
		
	IF iRule >= FMMC_MAX_RULES
	OR g_bMissionEnding
	OR NOT IS_SKYSWOOP_AT_GROUND()
	OR NOT IS_TEAM_ACTIVE(MC_playerBD[iPartToUse].iteam)
	OR NOT IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
	OR !bPedToUseOk
	OR NOT IS_BIT_SET(iEntityFirstStaggeredLoopComplete, ci_ZONES_FIRST_STAGGERED_LOOP_COMPLETE)
		IF IS_BIT_SET(sAltitudeBarLocal.iBS, ciALTITUDE_BAR_PREV_RULE_USED_ALTITUDE_SYSTEMS)
			CLEAN_UP_ALTITUDE_SYSTEMS()
		ENDIF
		
		EXIT
	ENDIF
	
	STRING sSoundSet = GET_ALTITUDE_FAIL_SOUNDSET()
	STRING sSoundName_Armed = GET_ALTITUTDE_FAIL_ARMRED_SOUND_NAME()
	STRING sSoundName_Countdown = GET_ALTITUTDE_FAIL_COUNTDOWN_SOUND_NAME()
	STRING sSoundName_CountdownStop = GET_ALTITUTDE_FAIL_COUNTDOWN_STOP_SOUND_NAME()	
	ENTITY_INDEX entSound
	INT iFailTime = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sAltitudeBar[iRule].iFailTime
		
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sAltitudeBar[iRule].iBS, ciRuleMeteredBehaviour_Enabled)
	AND IS_ZONE_TYPE_TRIGGERING(ciFMMC_ZONE_TYPE__ALTIMETER_SYSTEM_ZONE)
		SET_BIT(sAltitudeBarLocal.iBS, ciALTITUDE_BAR_PREV_RULE_USED_ALTITUDE_SYSTEMS)
		
		IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sAltitudeBar[iRule].iSpecificVehicle > -1
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sAltitudeBar[iRule].iSpecificVehicle])
				VEHICLE_INDEX vehTemp = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sAltitudeBar[iRule].iSpecificVehicle])
				sAltitudeBarLocal.vehToUse = vehTemp
				
				IF IS_PED_IN_VEHICLE(playerPedToUse, vehTemp)
					CLEAR_BIT(sAltitudeBarLocal.iBS, ciALTITUDE_BAR_LOCAL_NOT_IN_SPECIFIC_VEHICLE)
				ELSE
					SET_BIT(sAltitudeBarLocal.iBS, ciALTITUDE_BAR_LOCAL_NOT_IN_SPECIFIC_VEHICLE)
				ENDIF
				
			ENDIF
		ELIF IS_PED_IN_ANY_VEHICLE(playerPedToUse)
			sAltitudeBarLocal.vehToUse = GET_VEHICLE_PED_IS_IN(playerPedToUse)
		ELSE
			sAltitudeBarLocal.vehToUse = NULL		
		ENDIF
						
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sAltitudeBar[iRule].iBS, ciRuleMeteredBehaviour_RequiresVehicle)
		AND DOES_ENTITY_EXIST(sAltitudeBarLocal.vehToUse)
			sAltitudeBarLocal.fAltitude = GET_VEHICLE_ALTITUDE(sAltitudeBarLocal.vehToUse)
			entSound = sAltitudeBarLocal.vehToUse			
		ELSE
			VECTOR vCoord = GET_ENTITY_COORDS(playerPedToUse, FALSE)
			sAltitudeBarLocal.fAltitude = vCoord.z
			entSound = playerPedToUse
		ENDIF
		
		IF bIsAnySpectator			
			sAltitudeBarLocal.fFailTimer = MC_playerBD[iPartToUse].fCurrentAltitudeFailTime
		ENDIF
		
		IF NOT IS_BIT_SET(sAltitudeBarLocal.iBS, ciALTITUDE_BAR_LOCAL_NOT_IN_SPECIFIC_VEHICLE)
		AND g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sAltitudeBar[iRule].iSpecificVehicle > -1
			// Assign this within this function ; sAltitudeBarLocal.iProxyParticipant
			// Should be similar to how iExplodeDriverPart is done.
			
			IF sAltitudeBarLocal.iProxyParticipant != -1
				sAltitudeBarLocal.fAltitude = MC_playerBD[sAltitudeBarLocal.iProxyParticipant].fCurrentAltitude
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sAltitudeBar[iRule].iBS, ciRuleMeteredBehaviour_RequiresVehicle)
		AND NOT DOES_ENTITY_EXIST(sAltitudeBarLocal.vehToUse)
			PRINTLN("[LM][AltitudeSystem] - PROCESS_LOCAL_ALTITUDE - We Require a Vehicle but there is none.")
		ELSE
			// Exceeded Threshhold Bar
			IF HAS_EXCEEDED_RULE_ALTITUDE_THRESHOLD(iRule, iTeam, sAltitudeBarLocal.fAltitude)
				sAltitudeBarLocal.fFailTimer += GET_FRAME_TIME()
				IF sAltitudeBarLocal.fFailTimer > iFailTime 
					sAltitudeBarLocal.fFailTimer = TO_FLOAT(iFailTime)
				ENDIF
				
				IF NOT IS_BIT_SET(sAltitudeBarLocal.iBS, ciALTITUDE_BAR_PLAYED_ARMED_SFX)
				AND NOT IS_STRING_NULL_OR_EMPTY(sSoundName_Armed)					
					PLAY_SOUND_FROM_ENTITY(-1, sSoundName_Armed, entSound, sSoundSet, TRUE)
				ENDIF
				
				IF NOT IS_STRING_NULL_OR_EMPTY(sSoundName_Countdown)
					IF sAltitudeBarLocal.iCountdownSound = -1			
						sAltitudeBarLocal.iCountdownSound = GET_SOUND_ID()
						PLAY_SOUND_FROM_ENTITY(sAltitudeBarLocal.iCountdownSound, sSoundName_Countdown, entSound, sSoundSet, TRUE)
					ENDIF				
					SET_VARIABLE_ON_SOUND(sAltitudeBarLocal.iCountdownSound, "Ctrl", (sAltitudeBarLocal.fFailTimer / TO_FLOAT(iFailTime)))			
				ENDIF
				
				SET_BIT(sAltitudeBarLocal.iBS, ciALTITUDE_BAR_PLAYED_ARMED_SFX)
				
				PRINTLN("[LM][AltitudeSystem] - PROCESS_LOCAL_ALTITUDE - Exceeding Threshold. fFailTimer: ", sAltitudeBarLocal.fFailTimer, " iFailTime: ", iFailTime)
			ELSE			
				sAltitudeBarLocal.fFailTimer -= GET_FRAME_TIME()
				IF sAltitudeBarLocal.fFailTimer <= 0
					sAltitudeBarLocal.fFailTimer = 0
				ENDIF
				
				IF IS_BIT_SET(sAltitudeBarLocal.iBS, ciALTITUDE_BAR_PLAYED_ARMED_SFX)			
					CLEAR_BIT(sAltitudeBarLocal.iBS, ciALTITUDE_BAR_PLAYED_ARMED_SFX)
					IF NOT IS_STRING_NULL_OR_EMPTY(sSoundName_CountdownStop)
						PLAY_SOUND_FROM_ENTITY(-1, sSoundName_CountdownStop, entSound, sSoundSet, TRUE)
					ENDIF
					
					IF sAltitudeBarLocal.iCountdownSound != -1
						PRINTLN("[LM][AltitudeSystem] - PROCESS_LOCAL_ALTITUDE - Stopping Sound sAltitudeBarLocal.iCountdownSound: ", sAltitudeBarLocal.iCountdownSound)
						STOP_SOUND(sAltitudeBarLocal.iCountdownSound)
						RELEASE_SOUND_ID(sAltitudeBarLocal.iCountdownSound)
						sAltitudeBarLocal.iCountdownSound = -1
					ENDIF				
				ENDIF
				IF sAltitudeBarLocal.fFailTimer >= 0
					PRINTLN("[LM][AltitudeSystem] - PROCESS_LOCAL_ALTITUDE - No longer Exceeding Threshold. fFailTimer: ", sAltitudeBarLocal.fFailTimer, " iFailTime: ", iFailTime)
				ENDIF
			ENDIF
			
			// Exceeded Consequence Bar
			IF HAS_EXCEEDED_RULE_ALTITUDE_FAIL_TIMER(iRule, iTeam, sAltitudeBarLocal.fFailTimer)
				PRINTLN("[LM][AltitudeSystem] - PROCESS_LOCAL_ALTITUDE - Fail timer has been met Triggering: -------")
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sAltitudeBar[iRule].iBS, ciRuleMeteredBehaviour_ExplodeWhenFull)
					PRINTLN("[LM][AltitudeSystem] - PROCESS_LOCAL_ALTITUDE - Explode when full is Set.")
					PROCESS_ALTITUDE_FAIL_EXPLOSION()
				ENDIF
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sAltitudeBar[iRule].iBS, ciRuleMeteredBehaviour_AirstrikeWhenFull)
					PRINTLN("[LM][AltitudeSystem] - PROCESS_LOCAL_ALTITUDE - Airstrike is Set.")
					PROCESS_ALTITUDE_FAIL_AIRSTRIKE()
				ENDIF
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sAltitudeBar[iRule].iBS, ciRuleMeteredBehaviour_GangchaseWhenFull)
					PRINTLN("[LM][AltitudeSystem] - PROCESS_LOCAL_ALTITUDE - Gang Chase is Set.")
					PROCESS_ALTITUDE_FAIL_GANG_CHASE()
				ENDIF
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sAltitudeBar[iRule].iBS, ciRuleMeteredBehaviour_WantedWhenFull)
					PRINTLN("[LM][AltitudeSystem] - PROCESS_LOCAL_ALTITUDE - Wanted is Set.")
					PROCESS_ALTITUDE_FAIL_WANTED()
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF IS_BIT_SET(sAltitudeBarLocal.iBS, ciALTITUDE_BAR_PREV_RULE_USED_ALTITUDE_SYSTEMS)
			CLEAN_UP_ALTITUDE_SYSTEMS()
		ENDIF
	ENDIF
	
	PROCESS_UPDATE_ALTITUDE_BAR_CACHING(sAltitudeBarLocal.fAltitude, sAltitudeBarLocal.fFailTimer)

ENDPROC

PROC PROCESS_VEHICLE_EXPLOSION_UNDER_MIN_SPEED()

	BOOL bUseAltitude 
	
	bExplodeSpeedUpdateThisFrame = FALSE
	IF HAS_NET_TIMER_STARTED(tdExplodeSpeedUpdateTimer)
		IF HAS_NET_TIMER_EXPIRED(tdExplodeSpeedUpdateTimer, 500)
			bExplodeSpeedUpdateThisFrame = TRUE
			REINIT_NET_TIMER(tdExplodeSpeedUpdateTimer)
		ENDIF
		
		IF fRealExplodeProgress >= 100
			bExplodeSpeedUpdateThisFrame = TRUE
		ENDIF 
		
	ELSE
		START_NET_TIMER(tdExplodeSpeedUpdateTimer)
		bExplodeSpeedUpdateThisFrame = TRUE
	ENDIF
	
	IF bDisableExplosionBarKill
		EXIT
	ENDIF
	
	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	INT iMinSpeed
	FLOAT fVehSpeed
	VEHICLE_INDEX tempVeh, tempTrailer, tempSpecificVeh
	BOOL bInCar, bProcessSpecificVehicle, bHasSpecificVeh, bProcessAsPassenger
	
	iMinSpeed = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMinSpeed[iRule]
	
	IF iRule < FMMC_MAX_RULES
		bUseAltitude = IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEleven[iRule], ciBS_RULE11_USE_ALTITUDE_SPEED_BAR)
		
		IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMinSpeed_SpecificVehicle[iRule] > -1
			bProcessSpecificVehicle = TRUE
			
			IF iSpectatorTarget = -1
				IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
					tempVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
					GET_VEHICLE_TRAILER_VEHICLE(tempVeh, tempTrailer)
				ENDIF
				
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMinSpeed_SpecificVehicle[iRule]])
					tempSpecificVeh = (NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMinSpeed_SpecificVehicle[iRule]]))
					IF tempTrailer = NULL
						IF IS_VEHICLE_A_TRAILER(tempSpecificVeh)
							tempTrailer = tempSpecificVeh
						ENDIF
					ENDIF
					bHasSpecificVeh = TRUE					
				ELSE
					PRINTLN("[AltitudeSpeedBar] - PROCESS_VEHICLE_EXPLOSION_UNDER_MIN_SPEED - Specific vehicle does not exist")
				ENDIF
				
			ENDIF
		ELSE
			IF bUseAltitude
				IF NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
					SET_FAKE_MINIMAP_MAX_ALTIMETER_HEIGHT(0)
				ENDIF
			ENDIF
		ENDIF 
	ENDIF
	
	IF iSpectatorTarget = -1
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		OR bProcessSpecificVehicle
			
			IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
				tempVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
			ENDIF
			
			IF bHasSpecificVeh
			AND tempSpecificVeh != NULL
				tempVeh = tempSpecificVeh
			ELSE
				IF bProcessSpecificVehicle
					PRINTLN("[AltitudeSpeedBar] PROCESS_VEHICLE_EXPLOSION_UNDER_MIN_SPEED - bProcessSpecificVehicle and tempSpecificVeh = NULL")
					IF IS_BIT_SET(MC_PlayerBD[iLocalPart].iClientBitSet4, PBBOOL4_SPEED_FAIL_TRIGGERED)
						PRINTLN("[RCC MISSION] PROCESS_VEHICLE_EXPLOSION_UNDER_MIN_SPEED - clearing speed fail triggered 3")
						CLEAR_BIT(MC_PlayerBD[iLocalPart].iClientBitSet4, PBBOOL4_SPEED_FAIL_TRIGGERED)
					ENDIF
					EXIT
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(tempVeh)
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwo[iRule], ciBS_RULE11_SPEED_BAR_NOT_STEALTHED)
					IF DOES_ENTITY_EXIST(tempVeh)
					AND GET_ENTITY_MODEL(tempVeh) = AKULA
					AND NOT ARE_FOLDING_WINGS_DEPLOYED(tempVeh)
						PRINTLN("[AltitudeSpeedBar] - PROCESS_VEHICLE_EXPLOSION_UNDER_MIN_SPEED - Akula not in stealth mode")
						EXIT
					ENDIF
				ENDIF
			
				IF IS_VEHICLE_DRIVEABLE(tempVeh)
					
					IF IS_LOCAL_PLAYER_IN_ANY_SIMPLE_INTERIOR()
					OR VDIST2(GET_ENTITY_COORDS(LocalPlayerPed, FALSE),GET_ENTITY_COORDS(tempVeh, FALSE)) > POW(250, 2)
						IF iExplodeDriverPart != -1
							fVehSpeed = MC_playerBD[iExplodeDriverPart].fBombVehicleSpeed
						ENDIF
						MC_playerBD[iLocalPart].fBombVehicleSpeed = 0.0
					ELSE
						IF bUseAltitude
							IF IS_VEHICLE_SUPPOSED_TO_BE_IN_WATER(tempVeh)
								fVehSpeed = GET_VEHICLE_ALTITUDE(tempVeh)
							ELSE
								fVehSpeed = GET_PLANE_ALTITUDE(tempVeh)
							ENDIF
						ELSE
							fVehSpeed = ABSF(GET_ENTITY_SPEED(tempVeh))
						ENDIF
						IF bExplodeSpeedUpdateThisFrame
							MC_playerBD[iLocalPart].fBombVehicleSpeed = fVehSpeed
						ENDIF
						fVehSpeedForAudioTrigger = fVehSpeed
					ENDIF
					
					IF bProcessSpecificVehicle
						IF bExplodeSpeedUpdateThisFrame
						AND bIsLocalPlayerHost
							MC_serverBD_4.fVehSpeed = fVehSpeed
						ELSE
							fVehSpeed = MC_serverBD_4.fVehSpeed
						ENDIF
					ENDIF
					
					IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFour[iRule], ciBS_RULE4_DontExplodeWhenUnderMinSpeed)
					AND ((IS_PLAYER_IN_VEH_SEAT(LocalPlayer,VS_DRIVER)
					AND NETWORK_HAS_CONTROL_OF_ENTITY(GET_VEHICLE_PED_IS_IN(LocalPlayerPed)))
					OR bProcessSpecificVehicle
					OR bProcessAsPassenger)

						bInCar = TRUE
						
						BOOL bExplode
						
						IF g_FMMC_STRUCT.iSpeedRaceCheckpointTimer != -1
							IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam)
							OR (NOT HAS_NET_TIMER_STARTED(tdSpeedRaceCheckpointTimer))
								PRINTLN("[AltitudeSpeedBar] - PROCESS_VEHICLE_EXPLOSION_UNDER_MIN_SPEED - - New rule ",iRule," for my team ",iTeam,", reinit checkpoint timer")
								REINIT_NET_TIMER(tdSpeedRaceCheckpointTimer)
							ENDIF
							
							IF HAS_NET_TIMER_STARTED(tdSpeedRaceCheckpointTimer)
							AND (GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdSpeedRaceCheckpointTimer) > (g_FMMC_STRUCT.iSpeedRaceCheckpointTimer * 1000))
								PRINTLN("[AltitudeSpeedBar] - PROCESS_VEHICLE_EXPLOSION_UNDER_MIN_SPEED - - Out of checkpoint time (",g_FMMC_STRUCT.iSpeedRaceCheckpointTimer," seconds), explode!")
								bExplode = TRUE
							ENDIF
						ENDIF
						
						IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetNine[iRule], ciBS_RULE9_AIRSTRIKE_ON_SPEED_FAIL)
						AND IS_BIT_SET(iLocalBoolCheck23, LBOOL23_SPEED_BAR_AIRSTRIKE_CALLED)
						AND NOT bDisableExplosionBarKill
							IF HAS_NET_TIMER_STARTED(tdAirstrikeExplodeTimer)

								IF IS_SOUND_ID_VALID(iSoundIDVehBeacon)
									IF NOT HAS_SOUND_FINISHED(iSoundIDVehBeacon)
										IF (GET_FRAME_COUNT() % 5) = 0
											VECTOR vSfxAirstrikeTargetCoords = GET_ENTITY_COORDS(tempVeh)
											VECTOR vSfxForwardVec = GET_ENTITY_FORWARD_VECTOR(tempVeh)
											vSfxForwardVec.x = vSfxForwardVec.x * GET_ENTITY_SPEED(tempVeh)
											vSfxForwardVec.y = vSfxForwardVec.y * GET_ENTITY_SPEED(tempVeh)

											vSfxAirstrikeTargetCoords.x += vSfxForwardVec.x
											vSfxAirstrikeTargetCoords.y += vSfxForwardVec.y
											
											UPDATE_SOUND_COORD(iSoundIDVehBeacon, vSfxAirstrikeTargetCoords)
										ENDIF
									ENDIF
								ENDIF
								
								IF HAS_NET_TIMER_EXPIRED(tdAirstrikeExplodeTimer, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sAirstrikeStruct[iRule].iSpeedFailTimerLength)
									
									STOP_SOUND(iSoundIDVehBeacon)
									
									IF NETWORK_HAS_CONTROL_OF_ENTITY(tempVeh)
									AND IS_ENTITY_ALIVE(tempVeh)
										IF NOT IS_BIT_SET(iLocalBoolCheck23, LBOOL23_AIRSTRIKE_SPEED_BAR_FIRED)
											vAirstrikeTargetCoords = GET_ENTITY_COORDS(tempVeh)
											VECTOR vForwardVec = GET_ENTITY_FORWARD_VECTOR(tempVeh)
											vForwardVec.x = vForwardVec.x * GET_ENTITY_SPEED(tempVeh)
											vForwardVec.y = vForwardVec.y * GET_ENTITY_SPEED(tempVeh)

											vAirstrikeTargetCoords.x += vForwardVec.x
											vAirstrikeTargetCoords.y += vForwardVec.y
											
											SET_BIT(iLocalBoolCheck23, LBOOL23_AIRSTRIKE_SPEED_BAR_FIRED)
											PRINTLN("[AltitudeSpeedBar] - PROCESS_VEHICLE_EXPLOSION_UNDER_MIN_SPEED - [AIRSTRIKE] Setting LBOOL23_AIRSTRIKE_SPEED_BAR_FIRED")
										ELSE
											PRINTLN("[AltitudeSpeedBar] - PROCESS_VEHICLE_EXPLOSION_UNDER_MIN_SPEED - [AIRSTRIKE][CALL] Calling DO_SCRIPTED_AIRSTRIKE from: AIRSTRIKE BAR FULL")
											DO_SCRIPTED_AIRSTRIKE(vAirstrikeTargetCoords, 5)
										ENDIF
										
										IF HAS_NET_TIMER_EXPIRED(tdAirstrikeExplodeTimer, (g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sAirstrikeStruct[iRule].iSpeedFailTimerLength + 1500))
											ADD_EXPLOSION(vAirstrikeTargetCoords, EXP_TAG_HI_OCTANE, 1.0)
											NETWORK_EXPLODE_VEHICLE(tempVeh, TRUE, TRUE)
										ENDIF
									ENDIF
								ENDIF
							ELSE
								REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("w_lr_rpg_rocket")))	
								START_NET_TIMER(tdAirstrikeExplodeTimer)
								PRINTLN("[AltitudeSpeedBar] - PROCESS_VEHICLE_EXPLOSION_UNDER_MIN_SPEED - [AIRSTRIKE] Starting Explode timer!")
							ENDIF
						ENDIF
						
						BOOL bProcessExplosionProgress
						
						IF NOT bUseAltitude
							IF fVehSpeed <= iMinSpeed
								bProcessExplosionProgress = TRUE
							ENDIF
						ENDIF
						
						IF (bUseAltitude AND fVehSpeed >= iMinSpeed)
							bProcessExplosionProgress = TRUE
						ENDIF
						
						IF bExplode
							bProcessExplosionProgress = TRUE
						ENDIF
					
						IF bProcessExplosionProgress
							PRINTLN("[AltitudeSpeedBar] - PROCESS_VEHICLE_EXPLOSION_UNDER_MIN_SPEED - bProcessExplosionProgress = TRUE - fVehSpeed: ", fVehSpeed, " iMinSpeed: ", iMinSpeed)
							
							IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iFailSpeedTimer[iRule] > 0
								
								// Cache prev frame progress
								fRealExplodeProgress = CLAMP(fRealExplodeProgress + ((fLastFrameTime / TO_FLOAT(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iFailSpeedTimer[iRule])) * 100), 0, 100)
								PRINTLN("[AltitudeSpeedBar] - PROCESS_VEHICLE_EXPLOSION_UNDER_MIN_SPEED - - Increasing fRealExplodeProgress to ", fRealExplodeProgress)
								
								IF bExplodeSpeedUpdateThisFrame
									PRINTLN("[AltitudeSpeedBar] - PROCESS_VEHICLE_EXPLOSION_UNDER_MIN_SPEED - - Increasing update")
									IF bIsLocalPlayerHost
									AND NOT SHOULD_USE_LOCAL_MINIMUM_VEH_SPEED_CHECK()
										MC_serverBD_4.fExplodeProgress = fRealExplodeProgress
										MC_serverBD_4.iExplodeUpdateIter++
									ENDIF
									MC_playerBD[iLocalPart].fBombVehicleSpeed = fVehSpeed
									MC_playerBD[iLocalPart].iMinSpeed_ExplodeProgress = ROUND(fRealExplodeProgress)
								ENDIF
								
								IF NOT bIsLocalPlayerHost
								AND iExplodeProgressUpdate != MC_serverBD_4.iExplodeUpdateIter
								AND NOT SHOULD_USE_LOCAL_MINIMUM_VEH_SPEED_CHECK()
									iExplodeProgressUpdate = MC_serverBD_4.iExplodeUpdateIter
									fRealExplodeProgress = MC_serverBD_4.fExplodeProgress
									PRINTLN("[AltitudeSpeedBar] - PROCESS_VEHICLE_EXPLOSION_UNDER_MIN_SPEED - Setting fRealExplodeProgress to MC_serverBD_4.fExplodeProgress which is: ", fRealExplodeProgress)
								ENDIF
								
								IF fRealExplodeProgress >= 100.0
								AND (IS_PED_IN_VEHICLE(LocalPlayerPed, tempVeh)
									OR (IS_VEHICLE_EMPTY(tempVeh) AND bHasSpecificVeh))
									PRINTLN("[AltitudeSpeedBar] - PROCESS_VEHICLE_EXPLOSION_UNDER_MIN_SPEED - PROCESS_VEHICLE_EXPLOSION_UNDER_MIN_SPEED - Out of safety time, explode!")
									bExplode = TRUE
								ENDIF
								
							ELSE
								PRINTLN("[AltitudeSpeedBar] - PROCESS_VEHICLE_EXPLOSION_UNDER_MIN_SPEED - PROCESS_VEHICLE_EXPLOSION_UNDER_MIN_SPEED - No safety timer allowed, explode!")
								bExplode = TRUE
							ENDIF
													
							IF NOT bExplode
								IF NOT IS_BIT_SET(iLocalBoolCheck13, LBOOL13_MIN_SPEED_EXPLODE_BAR_INCREASING)
									IF IS_SOUND_ID_VALID(iSoundIDVehBomb)
										STOP_SOUND(iSoundIDVehBomb)
									ENDIF
									IF IS_SOUND_ID_VALID(iSoundIDVehBomb2)
										STOP_SOUND(iSoundIDVehBomb2)
									ENDIF
									
									PLAY_MIN_SPEED_SOUND(iSoundIDVehBomb, "Armed", tempVeh, 2500)
									
									SET_BIT(iLocalBoolCheck13, LBOOL13_MIN_SPEED_EXPLODE_BAR_INCREASING)
								ELSE
									IF IS_SOUND_ID_VALID(iSoundIDVehBomb)
									AND HAS_SOUND_FINISHED(iSoundIDVehBomb)
										IF IS_SOUND_ID_VALID(iSoundIDVehBomb2)
										AND HAS_SOUND_FINISHED(iSoundIDVehBomb2)
											PLAY_MIN_SPEED_SOUND(iSoundIDVehBomb2, "Countdown", tempVeh)
										ENDIF
										SET_VARIABLE_ON_SOUND(iSoundIDVehBomb2, "Ctrl", SQRT(fRealExplodeProgress / 100.0))
									ENDIF
								ENDIF
								IF IS_BIT_SET(MC_PlayerBD[iLocalPart].iClientBitSet4, PBBOOL4_SPEED_FAIL_TRIGGERED)
									PRINTLN("[AltitudeSpeedBar] - PROCESS_VEHICLE_EXPLOSION_UNDER_MIN_SPEED - PROCESS_VEHICLE_EXPLOSION_UNDER_MIN_SPEED - clearing speed fail triggered")
									CLEAR_BIT(MC_PlayerBD[iLocalPart].iClientBitSet4, PBBOOL4_SPEED_FAIL_TRIGGERED)
								ENDIF
							ENDIF
							
							SET_CONTROL_SHAKE(PLAYER_CONTROL, 130, ROUND(fRealExplodeProgress) + 80)
							
							IF IS_PLAYSTATION_PLATFORM()
								SET_CONTROL_LIGHT_EFFECT_COLOR(PLAYER_CONTROL, 255, 0, 0)
								SET_BIT(iLocalBoolCheck13, LBOOL13_CONTROL_PAD_COLOUR_SHOULD_BE_RESET) // Resets the pad colour in PROCESS_PRE_MAIN_CHECKS (it's already done in MC_SCRIPT_CLEANUP)
							ENDIF
							
							IF bExplode
							OR (IS_VEHICLE_IN_WATER( tempVeh ) AND NOT IS_VEHICLE_SUPPOSED_TO_BE_IN_WATER(tempVeh))
								IF IS_SOUND_ID_VALID(iSoundIDVehBomb)
									STOP_SOUND(iSoundIDVehBomb)
								ENDIF
								IF IS_SOUND_ID_VALID(iSoundIDVehBomb2)
									STOP_SOUND(iSoundIDVehBomb2)
								ENDIF
								
								IF IS_SOUND_ID_VALID(iSoundIDVehBombCountdown)
									STOP_SOUND(iSoundIDVehBombCountdown)
								ENDIF
								
								CLEAR_BIT(iLocalBoolCheck13, LBOOL13_MIN_SPEED_EXPLODE_BAR_INCREASING)
								
								IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMinSpeed_SpecificVehicle[iRule] = -1
								AND NOT bUseAltitude // gang chase instead.
								
									IF NOT IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitsetFour[ iRule ], ciBS_RULE4_HIDE_DEATH_TICKERS )
									OR IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitsetFive[ iRule ], ciBS_RULE5_OVERRIDE_DEATH_TICKERS_TO_SHOW_SPEED_BLOW_UP )
										
										SCRIPT_EVENT_DATA_TICKER_CUSTOM_MESSAGE TickerMessage
										
										IF g_FMMC_STRUCT.iSpeedRaceCheckpointTimer != -1
										AND HAS_NET_TIMER_STARTED(tdSpeedRaceCheckpointTimer)
										AND (GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdSpeedRaceCheckpointTimer) > (g_FMMC_STRUCT.iSpeedRaceCheckpointTimer * 1000))
											TickerMessage.TickerEvent = TICKER_EVENT_CHECKPOINT_FAIL_TIMER_EXPLODE
										ELSE
											TickerMessage.TickerEvent = TICKER_EVENT_BELOW_SPEED_EXPLODE
										ENDIF
										
										TickerMessage.TeamInt = iTeam
										TickerMessage.iTeamToUse = iTeam
										TickerMessage.iRule = iRule
										TickerMessage.playerID = LocalPlayer
										TickerMessage.iSubType = -1
										
										BROADCAST_CUSTOM_TICKER_EVENT(TickerMessage, ALL_PLAYERS_ON_SCRIPT(FALSE))
									ENDIF
								ENDIF
								
								#IF IS_DEBUG_BUILD
								IF NOT GET_PLAYER_INVINCIBLE(LocalPlayer)
								#ENDIF
									//They're about to die from being blown up, so set the following bit to stop the suicide ticker from showing up:
									SET_BIT(g_i_IgnoreSuicideTicker_PlayerBS, NATIVE_TO_INT(LocalPlayer))
								#IF IS_DEBUG_BUILD
								ELSE
									PRINTLN("[AltitudeSpeedBar] - PROCESS_VEHICLE_EXPLOSION_UNDER_MIN_SPEED - PROCESS_VEHICLE_EXPLOSION_UNDER_MIN_SPEED - We're exploding, but WE'RE INVINCIBLE!")
								ENDIF
								#ENDIF
								
								#IF IS_DEBUG_BUILD
								IF g_FMMC_STRUCT.iSpeedRaceCheckpointTimer != -1
								AND HAS_NET_TIMER_STARTED(tdSpeedRaceCheckpointTimer)
								AND (GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdSpeedRaceCheckpointTimer) > (g_FMMC_STRUCT.iSpeedRaceCheckpointTimer * 1000))
									PRINTLN("[AltitudeSpeedBar] - PROCESS_VEHICLE_EXPLOSION_UNDER_MIN_SPEED - PROCESS_VEHICLE_EXPLOSION_UNDER_MIN_SPEED - Checkpoint timer took too long, call NETWORK_EXPLODE_VEHICLE")
								ELSE
									PRINTLN("[AltitudeSpeedBar] - PROCESS_VEHICLE_EXPLOSION_UNDER_MIN_SPEED - PROCESS_VEHICLE_EXPLOSION_UNDER_MIN_SPEED - Speed dropped below threshold of ",iMinSpeed," and safety timer expired, call NETWORK_EXPLODE_VEHICLE")
								ENDIF
								#ENDIF
								
								IF NOT IS_BIT_SET(MC_PlayerBD[iLocalPart].iClientBitSet4, PBBOOL4_SPEED_FAIL_TRIGGERED)
									PRINTLN("[AltitudeSpeedBar] - PROCESS_VEHICLE_EXPLOSION_UNDER_MIN_SPEED - PROCESS_VEHICLE_EXPLOSION_UNDER_MIN_SPEED - Setting speed fail triggered")
									SET_BIT(MC_PlayerBD[iLocalPart].iClientBitSet4, PBBOOL4_SPEED_FAIL_TRIGGERED)
								ENDIF
								
								IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetNine[iRule], ciBS_RULE9_AIRSTRIKE_ON_SPEED_FAIL)
									IF NOT IS_BIT_SET(iLocalBoolCheck23, LBOOL23_SPEED_BAR_AIRSTRIKE_CALLED)
										SET_BIT(iLocalBoolCheck23, LBOOL23_SPEED_BAR_AIRSTRIKE_CALLED)
										IF bIsLocalPlayerHost
											MC_serverBD_4.fExplodeProgress = 100.0
											MC_serverBD_4.iExplodeUpdateIter++
										ENDIF
									ENDIF
									
								ELSE
									IF NETWORK_HAS_CONTROL_OF_ENTITY(tempVeh)
										NETWORK_EXPLODE_VEHICLE(tempVeh, TRUE, TRUE)
										PRINTLN("[AltitudeSpeedBar] - PROCESS_VEHICLE_EXPLOSION_UNDER_MIN_SPEED - PROCESS_VEHICLE_EXPLOSION_UNDER_MIN_SPEED - Exploding vehicle now")
									ENDIF
								ENDIF
							ENDIF
						ELSE
							IF IS_BIT_SET(MC_PlayerBD[iLocalPart].iClientBitSet4, PBBOOL4_SPEED_FAIL_TRIGGERED)
								PRINTLN("[AltitudeSpeedBar] - PROCESS_VEHICLE_EXPLOSION_UNDER_MIN_SPEED - PROCESS_VEHICLE_EXPLOSION_UNDER_MIN_SPEED - clearing speed fail triggered 2")
								CLEAR_BIT(MC_PlayerBD[iLocalPart].iClientBitSet4, PBBOOL4_SPEED_FAIL_TRIGGERED)
							ENDIF
							IF fRealExplodeProgress > 0
							OR MC_playerBD[iLocalPart].iMinSpeed_ExplodeProgress > 0

								// Cache prev frame progress
								
								fRealExplodeProgress = CLAMP(fRealExplodeProgress - ((fLastFrameTime / TO_FLOAT(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iFailSpeedTimer[iRule])) * 100), 0, 100)
								PRINTLN("[AltitudeSpeedBar] - PROCESS_VEHICLE_EXPLOSION_UNDER_MIN_SPEED - Decreasing fRealExplodeProgress to ", fRealExplodeProgress)
								
								IF bExplodeSpeedUpdateThisFrame
									PRINTLN("[AltitudeSpeedBar] - PROCESS_VEHICLE_EXPLOSION_UNDER_MIN_SPEED - Decreasing update")
									IF bIsLocalPlayerHost
										MC_serverBD_4.fExplodeProgress = fRealExplodeProgress
										MC_serverBD_4.iExplodeUpdateIter++
									ENDIF
									MC_playerBD[iLocalPart].fBombVehicleSpeed = fVehSpeed
									MC_playerBD[iLocalPart].iMinSpeed_ExplodeProgress = ROUND(fRealExplodeProgress)
								ENDIF
								
								IF NOT bIsLocalPlayerHost
								AND NOT SHOULD_USE_LOCAL_MINIMUM_VEH_SPEED_CHECK()
								AND iExplodeProgressUpdate != MC_serverBD_4.iExplodeUpdateIter
									iExplodeProgressUpdate = MC_serverBD_4.iExplodeUpdateIter
									fRealExplodeProgress = MC_serverBD_4.fExplodeProgress
								ENDIF
								
								IF IS_BIT_SET(iLocalBoolCheck13, LBOOL13_MIN_SPEED_EXPLODE_BAR_INCREASING)
									IF IS_SOUND_ID_VALID(iSoundIDVehBomb)
										STOP_SOUND(iSoundIDVehBomb)
									ENDIF
									IF IS_SOUND_ID_VALID(iSoundIDVehBomb2)
										STOP_SOUND(iSoundIDVehBomb2)
									ENDIF
									
									PLAY_MIN_SPEED_SOUND(iSoundIDVehBomb, "Count_Stop", tempVeh, 1000)
									
									CLEAR_BIT(iLocalBoolCheck13, LBOOL13_MIN_SPEED_EXPLODE_BAR_INCREASING)
								ENDIF
								
								IF NETWORK_HAS_CONTROL_OF_ENTITY(tempVeh)
									IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciALLOW_MISSION_AIRSTRIKES)
										IF ROUND(fRealExplodeProgress) > g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sAirstrikeStruct[iRule].iStartPercentage
										AND NOT IS_BIT_SET(iLocalBoolCheck24, LBOOL24_AIRSTRIKE_IN_PROGRESS)
											IF NOT IS_BIT_SET(iLocalBoolCheck23, LBOOL23_AIRSTRIKE_FIFTY_PERCENT_STRIKE)
												PRINTLN("[AltitudeSpeedBar] - PROCESS_VEHICLE_EXPLOSION_UNDER_MIN_SPEED - [AIRSTRIKE][OFFSET] Setting LBOOL23_AIRSTRIKE_FIFTY_PERCENT_STRIKE")
												SET_BIT(iLocalBoolCheck23, LBOOL23_AIRSTRIKE_FIFTY_PERCENT_STRIKE)
												IF NOT HAS_NET_TIMER_STARTED(tdAirstrikeStartTimer)
													START_NET_TIMER(tdAirstrikeStartTimer)
												ENDIF
											ENDIF
										ELSE
											IF IS_BIT_SET(iLocalBoolCheck23, LBOOL23_AIRSTRIKE_FIFTY_PERCENT_STRIKE)
												PRINTLN("[AltitudeSpeedBar] - PROCESS_VEHICLE_EXPLOSION_UNDER_MIN_SPEED - [AIRSTRIKE][OFFSET] Clearing LBOOL23_AIRSTRIKE_FIFTY_PERCENT_STRIKE")
												CLEAR_BIT(iLocalBoolCheck23, LBOOL23_AIRSTRIKE_FIFTY_PERCENT_STRIKE)
											ENDIF
										ENDIF								
									ENDIF
								ENDIF
								
								SET_CONTROL_SHAKE(PLAYER_CONTROL, 130, ROUND(fRealExplodeProgress) + 80)
								
								IF IS_PLAYSTATION_PLATFORM()
									SET_CONTROL_LIGHT_EFFECT_COLOR(PLAYER_CONTROL, 255, 0, 0)
									SET_BIT(iLocalBoolCheck13, LBOOL13_CONTROL_PAD_COLOUR_SHOULD_BE_RESET) // Resets the pad colour in PROCESS_PRE_MAIN_CHECKS (it's already done in MC_SCRIPT_CLEANUP)
								ENDIF
								
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
		
	IF NOT bInCar
		fRealExplodeProgress = 0
	ENDIF
	
ENDPROC

PROC PROCESS_PLAYER_PASSENGER_BOMBING()
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySix, ciOptionsBS26_AllowPassengerBombing)
		IF MC_ServerBD.iNumberOfPlayingPlayers[0]+MC_ServerBD.iNumberOfPlayingPlayers[1]+MC_ServerBD.iNumberOfPlayingPlayers[2]+MC_ServerBD.iNumberOfPlayingPlayers[3] > 3
			IF NOT MPGlobalsAmbience.bEnablePassengerBombing
				MPGlobalsAmbience.bEnablePassengerBombing = TRUE
				PRINTLN("[LM][RCC MISSION] - bEnablePassengerBombing Setting this to TRUE.")
			ENDIF
		ELSE
			IF MPGlobalsAmbience.bEnablePassengerBombing
				MPGlobalsAmbience.bEnablePassengerBombing = FALSE
				PRINTLN("[LM][RCC MISSION] - bEnablePassengerBombing Setting this to FALSE.")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC DROP_MONEY_SET_PTFX(BOOL &bUseArtworkColor)
	
	IF bShouldProcessCokeGrabHud
		sDropMoney.sPTFXAsset = "scr_ie_svm_phantom2"
		sDropMoney.sPTFXName = "scr_ie_bul_coc_bag"
	ELSE
		sDropMoney.sPTFXAsset = "scr_ornate_heist"
		sDropMoney.sPTFXName = "scr_heist_ornate_banknotes"
	ENDIF
	
	IF BAG_CAPACITY__IS_ENABLED()
		SWITCH BAG_CAPACITY__GET_MOST_GRABBED_LOOT_TYPE()
			CASE ciLOOT_TYPE_CASH
			CASE ciLOOT_TYPE_WEED
				sDropMoney.sPTFXAsset = "scr_ornate_heist"
				sDropMoney.sPTFXName = "scr_heist_ornate_banknotes"
			BREAK
			CASE ciLOOT_TYPE_COKE
				sDropMoney.sPTFXAsset = "scr_ie_svm_phantom2"
				sDropMoney.sPTFXName = "scr_ie_bul_coc_bag"
			BREAK
			CASE ciLOOT_TYPE_GOLD
				sDropMoney.sPTFXAsset = "scr_ch_finale"
				sDropMoney.sPTFXName  = "scr_ch_finale_hit_gold"
			BREAK
			CASE ciLOOT_TYPE_PAINTING
				sDropMoney.sPTFXAsset = "scr_ch_finale"
				sDropMoney.sPTFXName  = "scr_ch_finale_hit_art"
				bUseArtworkColor = TRUE
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

PROC DROP_MONEY_SET_DROP_OFFSETS()
	IF bLocalPlayerPedOk
		IF NOT IS_PED_ON_ANY_BIKE(LocalPlayerPed)
			//set the offsets based on player model and high heels
			SWITCH GET_ENTITY_MODEL(LocalPlayerPed)
				CASE MP_M_FREEMODE_01
					sDropMoney.vCashGrabDropOffset1 		= << -0.180, -0.190, -0.150 >>
					sDropMoney.vCashGrabDropOffset2 		= << -0.130, -0.190, 0.0000 >>
					sDropMoney.vCashGrabDropOffset3 		= << -0.090, -0.170, 0.1600 >>
				BREAK
				CASE MP_F_FREEMODE_01
					IF IS_PED_WEARING_HIGH_HEELS(LocalPlayerPed)
						sDropMoney.vCashGrabDropOffset1 	= << -0.270, -0.130, -0.190 >>
						sDropMoney.vCashGrabDropOffset2 	= << -0.220, -0.160, -0.010 >>
						sDropMoney.vCashGrabDropOffset3 	= << -0.190, -0.160, 0.1100 >>
					ELSE
						sDropMoney.vCashGrabDropOffset1 	= << -0.190, -0.130, -0.190 >>
						sDropMoney.vCashGrabDropOffset2 	= << -0.130, -0.160, -0.010 >>
						sDropMoney.vCashGrabDropOffset3 	= << -0.100, -0.160, 0.1100 >>
					ENDIF
				BREAK
			ENDSWITCH
		ELSE
			//set the offset if player is on a motorbike
			sDropMoney.vCashGrabDropOffset1 				= << -0.270, -0.130, -0.190 >>
			sDropMoney.vCashGrabDropOffset2 				= << -0.220, -0.160, -0.010 >>
			sDropMoney.vCashGrabDropOffset3 				= << -0.190, -0.160, 0.1100 >>
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL DROP_MONEY_CASH_OVER_MINIMUM_TAKE(INT iCash)
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySeven, ciOptionsBS27_UseIndividualBagValues)
		RETURN MC_playerBD[iLocalPart].sLootBag.iCashGrabbed > GET_HEIST_MINIMUM_TAKE_FOR_CASH_DROP(iCash, TRUE)
	ENDIF
	
	RETURN (GET_TOTAL_CASH_GRAB_TAKE() > GET_HEIST_MINIMUM_TAKE_FOR_CASH_DROP(iCash))
		OR GET_TOTAL_CASH_GRAB_TAKE() > 250000
ENDFUNC

PROC DROP_MONEY_SET_PARTICLE_COLOUR()
	INT iTint = GET_RANDOM_INT_IN_RANGE(0, 9) //0-3 will just use the default tint.
	IF iTint > 7
		SET_PARTICLE_FX_NON_LOOPED_COLOUR(0.41, 0.25, 0.13)   //Brown
	ELIF iTint > 6
		SET_PARTICLE_FX_NON_LOOPED_COLOUR(0.18, 0.31, 0.17)   //Green
	ELIF iTint > 5
		SET_PARTICLE_FX_NON_LOOPED_COLOUR(0.45, 0.39, 0.18)   //Yellow
	ELIF iTint > 4
		SET_PARTICLE_FX_NON_LOOPED_COLOUR(0.33, 0.53, 0.6) 	  //Blue
	ELIF iTint > 3
		SET_PARTICLE_FX_NON_LOOPED_COLOUR(0.48, 0.7, 0.0)     //Red
	ENDIF
ENDPROC

FUNC BOOL DROP_MONEY_HIT_IN_THE_BAG()
	
	VECTOR vPlayerPos = GET_ENTITY_COORDS(LocalPlayerPed)
	
	RETURN IS_BULLET_IN_AREA(GET_PED_BONE_COORDS(LocalPlayerPed, BONETAG_PELVIS, sDropMoney.vCashGrabDropOffset1 ), fCashGrabDropRadius, FALSE)
		OR IS_BULLET_IN_AREA(GET_PED_BONE_COORDS(LocalPlayerPed, BONETAG_PELVIS, sDropMoney.vCashGrabDropOffset2 ), fCashGrabDropRadius, FALSE)
		OR IS_BULLET_IN_AREA(GET_PED_BONE_COORDS(LocalPlayerPed, BONETAG_PELVIS, sDropMoney.vCashGrabDropOffset3 ), fCashGrabDropRadius, FALSE)
		OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_GRENADE, vPlayerPos, 5.0)
		OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_GRENADELAUNCHER, vPlayerPos, 5.0)
		OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_STICKYBOMB, vPlayerPos, 5.0)
		OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_MOLOTOV, vPlayerPos, 5.0)
		OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_ROCKET, vPlayerPos, 5.0)
		OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_TANKSHELL, vPlayerPos, 5.0)
		OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_CAR, vPlayerPos, 5.0)
		OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_BIKE, vPlayerPos, 5.0)
		OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_TRUCK, vPlayerPos, 5.0)
		OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_PLANE, vPlayerPos, 5.0)
		OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_PLANE_ROCKET, vPlayerPos, 5.0)
		OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_VEHICLE_BULLET, vPlayerPos, 5.0)
		OR GET_NUMBER_OF_FIRES_IN_RANGE(vPlayerPos, 0.75 ) > 0
ENDFUNC

PROC DROP_MONEY_DROP_CASH(INT iCash, FLOAT fDropScale)
	
	INT iCashDropped
	
	iCashDropped = GET_RANDOM_INT_IN_RANGE(g_FMMC_STRUCT.iCashGrabMinimumHitLoss, g_FMMC_STRUCT.iCashGrabMaximumHitLoss)
	iCashDropped = FLOOR(TO_FLOAT(iCashDropped) * fDropScale)
	
	BOOL bIndividualTake = IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySeven, ciOptionsBS27_UseIndividualBagValues)
	
	MC_playerBD[iLocalPart].sLootBag.iCashGrabbed = CLAMP_INT(MC_playerBD[iLocalPart].sLootBag.iCashGrabbed - iCashDropped,
												   GET_HEIST_MINIMUM_TAKE_FOR_CASH_DROP(iCash, bIndividualTake),
												   MC_playerBD[iLocalPart].sLootBag.iCashGrabbed)
												   
	sMissionLocalContinuityVars.sLootBag.iCashGrabbed = MC_playerBD[iLocalPart].sLootBag.iCashGrabbed
	PRINTLN("[CONTINUITY][CashGrab] DROP_MONEY_DROP_CASH - sMissionLocalContinuityVars.sLootBag.iCashGrabbed = ", sMissionLocalContinuityVars.sLootBag.iCashGrabbed)
	
	PRINTLN("[RCC MISSION] - DROP_MONEY_DROP_CASH - Dropping cash ", iCashDropped)
	PRINTLN("[RCC MISSION] - DROP_MONEY_DROP_CASH - Personal cash grabbed total take after this drop ", MC_playerBD[iLocalPart].sLootBag.iCashGrabbed)
	PRINTLN("[RCC MISSION] - DROP_MONEY_DROP_CASH - Cash grab minimum take for this heist ", GET_HEIST_MINIMUM_TAKE_FOR_CASH_DROP(iCash, bIndividualTake))
	
	BROADCAST_FMMC_REMOVE_GRABBED_CASH(iCashDropped)
				
ENDPROC

PROC DROP_MONEY_PROCESS_LOSING_CASH(INT iCash, FLOAT fDropScale, BOOL bUseArtworkColor)
	CLEAR_ENTITY_LAST_DAMAGE_ENTITY(LocalPlayerPed)
					
	IF NOT IS_PED_PLAYING_ANY_CASH_GRAB_ANIM(LocalPlayerPed)
		IF HAS_NAMED_PTFX_ASSET_LOADED(sDropMoney.sPTFXAsset)
			USE_PARTICLE_FX_ASSET(sDropMoney.sPTFXAsset)
			
			IF bUseArtworkColor
				DROP_MONEY_SET_PARTICLE_COLOUR()
			ENDIF
			
			START_NETWORKED_PARTICLE_FX_NON_LOOPED_ON_ENTITY(sDropMoney.sPTFXName, LocalPlayerPed,
															 PICK_VECTOR(IS_PED_ON_ANY_BIKE(LocalPlayerPed),
															 << 0.00, -0.10, 0.20>>, << -0.10, -0.30, 0.0>>),
															 << 0.0, 0.0, 0.0 >>)
			
			DROP_MONEY_DROP_CASH(iCash, fDropScale)
			
			PRINTLN( "[CashGrab][DropMoney] Dropping money, scale of ", fDropScale)
			sDropMoney.iNoteTimer = GET_GAME_TIMER()
		ENDIF
	ENDIF
ENDPROC

///PURPOSE: This function is what checks to see if the player has been hit by a bullet/explosion
///    and plays the correct vfx
PROC DROP_MONEY_IF_HIT_EVERY()
	
	INT iCash = FMMC_CASH_ORNATE_BANK
	IF g_FMMC_STRUCT.iCashReward != 0
		iCash = g_FMMC_STRUCT.iCashReward
	ENDIF
	
	IF g_TransitionSessionNonResetVars.bInvolvedInCashGrab
		IF DROP_MONEY_CASH_OVER_MINIMUM_TAKE(iCash)
		
			BOOL bUseArtworkColor
			DROP_MONEY_SET_PTFX(bUseArtworkColor)

			REQUEST_NAMED_PTFX_ASSET(sDropMoney.sPTFXAsset)
			
			DROP_MONEY_SET_DROP_OFFSETS()

			IF GET_GAME_TIMER() - sDropMoney.iNoteTimer > ciDROP_MONEY_FREQUENCY
								
				//check for player damage
				IF IS_ENTITY_ON_FIRE(LocalPlayerPed)
				OR HAS_ENTITY_BEEN_DAMAGED_BY_ANY_PED(LocalPlayerPed)
				OR HAS_ENTITY_BEEN_DAMAGED_BY_ANY_OBJECT(LocalPlayerPed)
				OR HAS_ENTITY_BEEN_DAMAGED_BY_ANY_VEHICLE(LocalPlayerPed)
				
					DROP_MONEY_PROCESS_LOSING_CASH(iCash, 0.5, bUseArtworkColor)
					
					EXIT
				ENDIF
				
				//check bullets and explosions around player bag
				
				IF DROP_MONEY_HIT_IN_THE_BAG()
					DROP_MONEY_PROCESS_LOSING_CASH(iCash, 1.0, bUseArtworkColor)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_LIMITED_STUN_GUN()
	
	IF g_FMMC_STRUCT.iStunGunCharges = -1
	AND NOT IS_BIT_SET(g_TransitionSessionNonResetVars.iFMMCMissionBS, iFMMCMissionBS__LIMITED_STUN_GUN_INITIALISED)
		EXIT
	ENDIF
	
	IF NOT HAS_PED_GOT_WEAPON(LocalPlayerPed, WEAPONTYPE_STUNGUN)
	OR g_bMissionEnding
		EXIT
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck32, LBOOL32_LIMITED_STUN_GUN_FULLY_INITIALISED)
		WEAPON_TYPE wtCurrentWeapon
		IF GET_CURRENT_PED_WEAPON(LocalPlayerPed, wtCurrentWeapon)
			IF NOT IS_WEAPONTYPE_A_STUN_GUN(wtCurrentWeapon)
				EXIT
			ENDIF
			
			// Holding stun gun
			INT iAmmo
			IF wtCurrentWeapon = WEAPONTYPE_STUNGUN
				iAmmo = GET_PED_AMMO_BY_TYPE(LocalPlayerPed, AMMOTYPE_STUNGUN)
			ENDIF
			
			DRAW_GENERIC_SCORE(iAmmo, "FMMC_STUN_AMMO", DEFAULT, DEFAULT, HUDORDER_TOP)
		ENDIF
	
	ELIF IS_BIT_SET(iLocalBoolCheck32, LBOOL32_SET_STUN_GUN_AS_LIMITED)
		IF NOT HAS_FIRST_STRAND_MISSION_BEEN_PASSED()
			PRINTLN("[StunGun] Setting stun gun ammo to ", g_FMMC_STRUCT.iStunGunCharges)
			SET_PED_AMMO(LocalPlayerPed, WEAPONTYPE_STUNGUN, g_FMMC_STRUCT.iStunGunCharges)
		ENDIF
		
		SET_BIT(iLocalBoolCheck32, LBOOL32_LIMITED_STUN_GUN_FULLY_INITIALISED)
		
	ELSE
		PRINTLN("[StunGun] Initialising limited stun gun and setting iFMMCMissionBS__LIMITED_STUN_GUN_INITIALISED!")
		SET_PED_STUN_GUN_FINITE_AMMO(LocalPlayerPed, TRUE)
		
		SET_BIT(iLocalBoolCheck32, LBOOL32_SET_STUN_GUN_AS_LIMITED)
		SET_BIT(g_TransitionSessionNonResetVars.iFMMCMissionBS, iFMMCMissionBS__LIMITED_STUN_GUN_INITIALISED)
	ENDIF
ENDPROC

PROC PROCESS_REENABLE_SCUBA_GEAR()
	IF IS_CUTSCENE_PLAYING()
		EXIT
	ENDIF
	
	g_bBlockScubaGear = FALSE
ENDPROC

PROC HANDLE_LADDER_DISABLED_ON_RULE()
	INT iTeam = MC_playerBD[iLocalPart].iteam
	
	IF iTeam >= 0
	AND iTeam < FMMC_MAX_TEAMS
		INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
		
		IF iRule >= 0
		AND iRule < FMMC_MAX_RULES
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwelve[iRule], ciBS_RULE12_RESTRICT_LADDERS)
				IF NOT IS_BIT_SET(iLocalBoolCheck28, LBBOOL28_LADDERS_DISABLED_BY_RULE_OPTION)
					PRINTLN("HANDLE_LADDER_DISABLED_ON_RULE - Setting PCF_DisableLadderClimbing")
					SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_DisableLadderClimbing, TRUE)
					SET_BIT(iLocalBoolCheck28, LBBOOL28_LADDERS_DISABLED_BY_RULE_OPTION)
				ENDIF
			ELSE
				IF IS_BIT_SET(iLocalBoolCheck28, LBBOOL28_LADDERS_DISABLED_BY_RULE_OPTION)
					PRINTLN("HANDLE_LADDER_DISABLED_ON_RULE - Clearing PCF_DisableLadderClimbing")
					SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_DisableLadderClimbing, FALSE)
					CLEAR_BIT(iLocalBoolCheck28, LBBOOL28_LADDERS_DISABLED_BY_RULE_OPTION)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_GHOSTING_THROUGH_PLAYER_VEHICLES()
	IF MC_serverBD_3.iGhostingPlayerVehiclesVeh = -1
		EXIT
	ENDIF
	
	NETWORK_INDEX netGhostVeh = MC_serverBD_1.sFMMC_SBD.niVehicle[MC_serverBD_3.iGhostingPlayerVehiclesVeh]
	IF NETWORK_DOES_NETWORK_ID_EXIST(netGhostVeh)
	AND IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		VEHICLE_INDEX playerVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
		VEHICLE_INDEX ghostVeh = NET_TO_VEH(netGhostVeh)
		IF NOT IS_PED_IN_VEHICLE(LocalPlayerPed, ghostVeh)
			SET_ENTITY_NO_COLLISION_ENTITY(playerVeh, ghostVeh, FALSE)
			IF IS_BIT_SET(g_FMMC_Struct.iOptionsMenuBitSetSixteen, ciVEHICLE_PROXIMITY_DESTROY_PLAYER_VEHICLES)
			AND IS_ENTITY_ALIVE(playerVeh) 
			AND GET_DIST2_BETWEEN_ENTITIES(playerVeh, ghostVeh) < 10.0 * 10.0
			AND NETWORK_HAS_CONTROL_OF_ENTITY(playerVeh)
				NETWORK_EXPLODE_VEHICLE(playerVeh, FALSE,FALSE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_DELIVERY_WAIT_HAND_BRAKE()
	
	IF IS_BIT_SET(iLocalBoolCheck4, LBOOL4_DELIVERY_WAIT)
		
		VEHICLE_INDEX tempVeh
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			tempVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)		
		ENDIF
		
		IF DOES_ENTITY_EXIST(tempVeh)
		AND NETWORK_HAS_CONTROL_OF_ENTITY(tempVeh)
		AND GET_PED_IN_VEHICLE_SEAT(tempVeh, VS_DRIVER) = localPlayerPed
			IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdDeliverTimer) > 1800
				IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
					IF IS_VEHICLE_DRIVEABLE(tempVeh)
						SET_VEHICLE_HANDBRAKE(tempVeh, FALSE)
					ENDIF
					NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
					CLEAR_BIT(iLocalBoolCheck4, LBOOL4_DELIVERY_WAIT)
				ELSE
					PRINTLN("[RCC MISSION] LBOOL4_DELIVERY_WAIT Want to turn on player controle but IS_NEW_LOAD_SCENE_ACTIVE (1) ")
				ENDIF
			ELSE
				IF IS_VEHICLE_DRIVEABLE(tempVeh)
					IF GET_ENTITY_SPEED(tempVeh) > 0.2
						SET_ENTITY_VELOCITY(tempVeh,<<0,0,0>>)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
				NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
				CLEAR_BIT(iLocalBoolCheck4, LBOOL4_DELIVERY_WAIT)
			ELSE
				PRINTLN("[RCC MISSION] LBOOL4_DELIVERY_WAIT Want to turn on player controle but IS_NEW_LOAD_SCENE_ACTIVE (2) ")
			ENDIF
		ENDIF
		
	ELIF HAS_NET_TIMER_STARTED(tdDeliverBackupTimer)
	
		VEHICLE_INDEX tempVeh
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			tempVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)		
		ENDIF
		
		IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdDeliverBackupTimer) > 10000
						
			//Safety thing
			IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
				NET_SET_PLAYER_CONTROL(LocalPlayer,TRUE)
			ENDIF
			
			IF NOT IS_VEHICLE_FUCKED_MP(DeliveryVeh)
			AND NETWORK_HAS_CONTROL_OF_ENTITY(DeliveryVeh)
				SET_VEHICLE_HANDBRAKE(DeliveryVeh, FALSE)
				
			ELIF IS_PLAYER_IN_VEH_SEAT(LocalPlayer, VS_DRIVER)
				IF NOT IS_VEHICLE_FUCKED_MP(tempVeh)
				AND NETWORK_HAS_CONTROL_OF_ENTITY(tempVeh)
					SET_VEHICLE_HANDBRAKE(tempVeh, FALSE)
				ENDIF
			ENDIF
			
			IF IS_PED_IN_ANY_BOAT(LocalPlayerPed)
			OR IS_PED_IN_ANY_AMPHIBIOUS_VEHICLE(LocalPlayerPed)			
				IF NOT IS_VEHICLE_FUCKED_MP(tempVeh)
					IF GET_PED_IN_VEHICLE_SEAT(tempVeh) = LocalPlayerPed
						IF NETWORK_HAS_CONTROL_OF_ENTITY(tempVeh)
							SET_BOAT_ANCHOR(tempVeh, FALSE)
							SET_BOAT_REMAINS_ANCHORED_WHILE_PLAYER_IS_DRIVER(tempVeh, FALSE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			MC_playerBD[iPartToUse].iVehDeliveryId =-1
			DeliveryVeh = NULL
			
			RESET_NET_TIMER(tdDeliverBackupTimer)
			CLEAR_BIT(iLocalBoolCheck2,LBOOL2_BRING_VEHICLE_TO_HALT_CALLED)
			CLEAR_BIT(iLocalBoolCheck2,LBOOL2_PED_DROPPED_OFF)
			CLEAR_BIT(iLocalBoolCheck,LBOOL_CARGOBOB_DROP)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck5, LBOOL5_KEEP_FORCE_EVERYONE_FROM_MY_CAR)
		IF FORCE_EVERYONE_FROM_MY_CAR(TRUE, TRUE)
			CLEAR_BIT(iLocalBoolCheck5, LBOOL5_KEEP_FORCE_EVERYONE_FROM_MY_CAR)
		ENDIF
	ENDIF
	
	IF iDeliveryVehForcingOut != -1
		IF IS_BIT_SET(MC_serverBD.iProcessJobCompBitset, iPartSending_DeliveryVehForcingOut)
			//Delivery has finished, and we've got the server update saying it's all good!
			iDeliveryVehForcingOut = -1
			iPartSending_DeliveryVehForcingOut = -1
			RESET_NET_TIMER(tdForceEveryoneOutBackupTimer)
		ELSE
			IF NOT HAS_NET_TIMER_STARTED(tdForceEveryoneOutBackupTimer)
				REINIT_NET_TIMER(tdForceEveryoneOutBackupTimer)
			ELSE
				IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdForceEveryoneOutBackupTimer) > 10000
					PRINTLN("[RCC MISSION] tdForceEveryoneOutBackupTimer has been running for 10 seconds with no job completion event, clear it and iDeliveryVehForcingOut!")
					iDeliveryVehForcingOut = -1
					iPartSending_DeliveryVehForcingOut = -1
					RESET_NET_TIMER(tdForceEveryoneOutBackupTimer)
				ENDIF
			ENDIF
		ENDIF
	ELIF HAS_NET_TIMER_STARTED(tdForceEveryoneOutBackupTimer)
		RESET_NET_TIMER(tdForceEveryoneOutBackupTimer)
	ENDIF
ENDPROC

PROC PROCESS_ON_RULE_EMAIL(INT iTeam, INT iRule)

	INT iEmailIndex = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iSendEmail[iRule]
	
	IF DID_RULE_CHANGE_DURING_STAGGERED_LOCAL_PLAYER_PROCESSING()
	AND iEmailIndex > -1
		enumCharacterList eclEmailSender = CHAR_LESTER
		enumMPemailLockedStatus eLockedStatus = EMAIL_LOCKED
		
		SWITCH iEmailIndex
			CASE 0	eclEmailSender = CHAR_LESTER BREAK
		ENDSWITCH
		
		TEXT_LABEL_15 sEmailTextLabel = "MC_EMAIL_"
		sEmailTextLabel += iEmailIndex
		
		PRINTLN("PROCESS_ON_RULE_EMAIL - Sending now for rule: ", iRule)
		SEND_EMAIL_MESSAGE_TO_CURRENT_PLAYER(eclEmailSender, sEmailTextLabel, eLockedStatus, EMAIL_NOT_CRITICAL, EMAIL_DO_NOT_AUTO_UNLOCK, MPE_NO_REPLY_REQUIRED)
	ENDIF
ENDPROC

PROC PROCESS_CUSTOM_SHARD_ON_TEAM_POINT()
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciMISSION_CUSTOM_SHARD_ON_TEAM_POINT)
		EXIT	
	ENDIF
	
	IF IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
	OR g_bCelebrationScreenIsActive		
	OR g_bEndOfMissionCleanUpHUD
		EXIT
	ENDIF
	
	IF MC_serverBD.iTeamScore[iStaggeredTeamIterator] > 0
	AND MC_serverBD.iTeamScore[iStaggeredTeamIterator] != iTeamScoreLast[iStaggeredTeamIterator]
		PRINTLN("PROCESS_CUSTOM_SHARD_ON_TEAM_POINT | Attempting to print shard for team ", iStaggeredTeamIterator, " previous score: ", iTeamScoreLast[iStaggeredTeamIterator], " new score: ", MC_serverBD.iTeamScore[iStaggeredTeamIterator])
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciCUSTOM_SHARD_HAS_LITERAL_STRING)
		AND NOT IS_STRING_NULL_OR_EMPTY(g_sMission_TeamName[iStaggeredTeamIterator])
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciCUSTOM_SHARD_COLOUR_REPLACEMENT_FOR_LITERAL)
				INT R = 0, G = 0, B = 0, A = 0
				HUD_COLOURS TempTeamColour = GET_HUD_COLOUR_FOR_FMMC_TEAM(iStaggeredTeamIterator, PlayerToUse)
				GET_HUD_COLOUR(TempTeamColour,R,G,B,A)
				SET_SCRIPT_VARIABLE_HUD_COLOUR(R,G,B,A)
			ENDIF
			SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_AND_INT(BIG_MESSAGE_GENERIC_MIDSIZED, iStaggeredTeamIterator, g_FMMC_STRUCT.tlCustomShardStrapline, g_sMission_TeamName[iStaggeredTeamIterator], g_FMMC_STRUCT.tlCustomShardTitle)
		ELSE
			SETUP_NEW_BIG_MESSAGE_WITH_INT_AND_STRING(BIG_MESSAGE_GENERIC_MIDSIZED, iStaggeredTeamIterator, g_FMMC_STRUCT.tlCustomShardStrapline, g_FMMC_STRUCT.tlCustomShardTitle)
		ENDIF
		
		RESET_NET_TIMER(tdWaitForScoreShardTimeOut)
		iTeamScoreLast[iStaggeredTeamIterator] = MC_serverBD.iTeamScore[iStaggeredTeamIterator]
	ENDIF
	
ENDPROC

PROC PROCESS_CAMERA_SHAKE_ON_RULE()
		
	CLEAR_BIT(g_iMissionMiscBitset1, ciMissionMiscBitset1_OverridingYachtCamShake)
	
	INT iTeam = MC_playerBD[iPartToUse].iTeam
	IF iTeam < FMMC_MAX_TEAMS
	AND iTeam > -1
	
		INT iRule = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]
		IF iRule < FMMC_MAX_RULES
		
			IF IS_BIT_SET(iLocalBoolCheck6, (LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam))
				IF IS_BIT_SET(iLocalBoolCheck25, LBOOL25_FINISHED_CAMERA_SHAKING_ON_RULE)
					PRINTLN("[LM][PROCESS_CAMERA_SHAKE_ON_RULE] - New Rule. Clearing 'Finished' bitset.")
					CLEAR_BIT(iLocalBoolCheck25, LBOOL25_FINISHED_CAMERA_SHAKING_ON_RULE)
					CLEAR_BIT(iLocalBoolCheck25, LBOOL25_USING_CAMERA_SHAKING_ON_RULE)
					RESET_NET_TIMER(td_CameraShakingOnRuleTimer)
				ENDIF
			ENDIF
						
			IF NOT SHOULD_BLOCK_SHAKE_WHILE_AIMING(iTeam, iRule)
			AND NOT IS_BIT_SET(iLocalBoolCheck25, LBOOL25_FINISHED_CAMERA_SHAKING_ON_RULE)
				
				// Secondary Shake
				IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iSecondaryCameraShakeInterval[iRule] != 0
					
					IF NOT HAS_NET_TIMER_STARTED(td_SecondaryCameraShakeIntervalTimer)
						START_NET_TIMER(td_SecondaryCameraShakeIntervalTimer)
					ENDIF
					
					// Start secondary shake every INTERVAL seconds
					IF HAS_NET_TIMER_STARTED(td_SecondaryCameraShakeIntervalTimer)
					AND HAS_NET_TIMER_EXPIRED_READ_ONLY(td_SecondaryCameraShakeIntervalTimer, GET_SECONDARY_CAMERA_SHAKE_INTERVAL(iTeam, iRule))
						
						PRINTLN("[LM][PROCESS_CAMERA_SHAKE_ON_RULE] - Applying secondary camera shake")
						SHAKE_GAMEPLAY_CAM(GET_CAMERA_SHAKING_TYPE_FROM_CREATOR_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iSecondaryCameraShakeType[iRule]), g_FMMC_STRUCT.sFMMCEndConditions[iTeam].fSecondaryCameraShakeIntensity[iRule])
						
						IF NOT HAS_NET_TIMER_STARTED(td_SecondaryCameraShakeDurationTimer)
							START_NET_TIMER(td_SecondaryCameraShakeDurationTimer)
						ENDIF
					ENDIF
					
					// Stop secondary shake after DURATION seconds
					IF HAS_NET_TIMER_STARTED(td_SecondaryCameraShakeDurationTimer)
					AND HAS_NET_TIMER_EXPIRED_READ_ONLY(td_SecondaryCameraShakeDurationTimer, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iSecondaryCameraShakeDuration[iRule])
						
						PRINTLN("[LM][PROCESS_CAMERA_SHAKE_ON_RULE] - Removing secondary camera shake.")
						SET_BIT(iLocalBoolCheck36, LBOOL36_SECONDARY_CAMERA_SHAKE_FINISHED)
						RESET_NET_TIMER(td_SecondaryCameraShakeIntervalTimer)
						RESET_NET_TIMER(td_SecondaryCameraShakeDurationTimer)
					ENDIF
				ENDIF
					
				// Primary Shake
				IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iCameraShakeDuration[iRule] != 0
					
					SET_BIT(g_iMissionMiscBitset1, ciMissionMiscBitset1_OverridingYachtCamShake)
					
					IF NOT IS_BIT_SET(iLocalBoolCheck25, LBOOL25_USING_CAMERA_SHAKING_ON_RULE)
						PRINTLN("[LM][PROCESS_CAMERA_SHAKE_ON_RULE] - Using Camera Shaking on Rule.")
						SET_BIT(iLocalBoolCheck25, LBOOL25_USING_CAMERA_SHAKING_ON_RULE)
						
						IF IS_GAMEPLAY_CAM_SHAKING()
							PRINTLN("[LM][PROCESS_CAMERA_SHAKE_ON_RULE] - Camera was already shaking. Stopping it.")
							STOP_GAMEPLAY_CAM_SHAKING()
						ENDIF
					ENDIF
					
					// Do Shake
					IF NOT IS_GAMEPLAY_CAM_SHAKING()
					OR IS_BIT_SET(iLocalBoolCheck36, LBOOL36_SECONDARY_CAMERA_SHAKE_FINISHED)
						PRINTLN("[LM][PROCESS_CAMERA_SHAKE_ON_RULE] - Applying camera shake.")
						SHAKE_GAMEPLAY_CAM(GET_CAMERA_SHAKING_TYPE_FROM_CREATOR_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iCameraShakeType[iRule]), GET_CAMERA_SHAKE_INTENSITY(iTeam, iRule))
						CLEAR_BIT(iLocalBoolCheck36, LBOOL36_SECONDARY_CAMERA_SHAKE_FINISHED)
					ENDIF
					
					// Timers for Reset
					IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iCameraShakeDuration[iRule] != -1
						IF NOT HAS_NET_TIMER_STARTED(td_CameraShakingOnRuleTimer)
							START_NET_TIMER(td_CameraShakingOnRuleTimer)
						ENDIF
						
						IF HAS_NET_TIMER_STARTED(td_CameraShakingOnRuleTimer)
						AND HAS_NET_TIMER_EXPIRED_READ_ONLY(td_CameraShakingOnRuleTimer, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iCameraShakeDuration[iRule]*1000)
							PRINTLN("[LM][PROCESS_CAMERA_SHAKE_ON_RULE] - Camera shaking has finished now on this Rule. Duration is up.")
							SET_BIT(iLocalBoolCheck25, LBOOL25_FINISHED_CAMERA_SHAKING_ON_RULE)
							RESET_NET_TIMER(td_CameraShakingOnRuleTimer)
						ENDIF
					ENDIF
				ENDIF
				
			ELIF IS_BIT_SET(iLocalBoolCheck25, LBOOL25_USING_CAMERA_SHAKING_ON_RULE)
			
				// Cleanup
				PRINTLN("[LM][PROCESS_CAMERA_SHAKE_ON_RULE] - Cleaning up Camera Shaking on Rule. Aiming: ", GET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_IsAimingGun, FALSE))
				CLEAR_BIT(iLocalBoolCheck25, LBOOL25_USING_CAMERA_SHAKING_ON_RULE)
				STOP_GAMEPLAY_CAM_SHAKING()
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC CACHE_IS_PLAYER_IN_CREATOR_TRAILER()
	IF IS_PLAYER_IN_CREATOR_TRAILER(LocalPlayer)
	OR IS_PLAYER_IN_CREATOR_AIRCRAFT(LocalPlayer)
		IF NOT IS_BIT_SET(iLocalBoolCheck24, LBOOL24_IN_CREATOR_TRAILER)
			SET_BIT(iLocalBoolCheck24, LBOOL24_IN_CREATOR_TRAILER)
		ENDIF
	ELSE
		IF IS_BIT_SET(iLocalBoolCheck24, LBOOL24_IN_CREATOR_TRAILER)
			CLEAR_BIT(iLocalBoolCheck24, LBOOL24_IN_CREATOR_TRAILER)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_ALLOWING_NON_HEIST_REBREATHERS(INT& iTeam, INT& iRule)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciALLOW_NON_HEIST_REBREATHERS)
		EXIT
	ENDIF
	
	IF DID_RULE_CHANGE_DURING_STAGGERED_LOCAL_PLAYER_PROCESSING()
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThree[iRule], ciBS_RULE3_QUICK_EQUIP_REBREATHER)
			IF NOT IS_BIT_SET(iLocalBoolCheck23, LBOOL23_HAS_PLAYER_BEEN_GIVEN_A_REBREATHER)
				INT iBreatherCount = GET_MP_INT_CHARACTER_STAT(MP_STAT_BREATHING_APPAR_BOUGHT, DEFAULT, TRUE)
				IF iBreatherCount < g_FMMC_STRUCT.iWVMAmountOfRebreatherCanisters
					INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_BREATHING_APPAR_BOUGHT, g_FMMC_STRUCT.iWVMAmountOfRebreatherCanisters - iBreatherCount)
					PRINTLN("[RCC MISSION] PROCESS_ALLOWING_NON_HEIST_REBREATHERS | Giving ", g_FMMC_STRUCT.iWVMAmountOfRebreatherCanisters, " rebreathers")
				ENDIF
				
				SET_BIT(iLocalBoolCheck23, LBOOL23_HAS_PLAYER_BEEN_GIVEN_A_REBREATHER)
				PREVENT_GANG_OUTFIT_CHANGING(TRUE)
			ENDIF
			
		ELSE
			PREVENT_GANG_OUTFIT_CHANGING(FALSE)
			
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_ALLOWING_SPRINTING_IN_INTERIORS()
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciALLOW_SPRINTING_IN_INTERIORS)
		EXIT
	ENDIF
	
	IF NOT bLocalPlayerPedOk
		bIgnoreInteriorCheckForSprinting = FALSE
		
	ELSE
		IF NOT bIgnoreInteriorCheckForSprinting
		OR NOT GET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_IgnoreInteriorCheckForSprinting)
			SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_IgnoreInteriorCheckForSprinting, TRUE)
			PRINTLN("[RCC MISSION] PROCESS_ALLOWING_SPRINTING_IN_INTERIORS | SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_IgnoreInteriorCheckForSprinting, TRUE)")
			bIgnoreInteriorCheckForSprinting = TRUE
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_PLAYER_RESET_SPAWN_PROTECTION()
	IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_ANY_SPECTATOR)
		EXIT
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(tdDefenseSphereInvulnerabilityTimer)		
		IF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdDefenseSphereInvulnerabilityTimer, (g_FMMC_STRUCT.iSpawnProtectionDuration*100))
			PRINTLN("[LM][PROCESS_ZONES](PROCESS_PLAYER_RESET_SPAWN_PROTECTION) - Resetting Ghost and Invincibility.")
			IF bLocalPlayerPedOK
				SET_ENTITY_INVINCIBLE(localPlayerPed, FALSE)
				MC_SET_LOCAL_PLAYER_AS_GHOST(FALSE)
				
				BROADCAST_FMMC_SPAWN_PROTECTION_ALPHA(FALSE, iLocalPart, GET_FRAME_COUNT())
			ENDIF
			RESET_NET_TIMER(tdDefenseSphereInvulnerabilityTimer)
		ENDIF
	ENDIF	
ENDPROC

PROC PROCESS_HELI_STABILIZATION(VEHICLE_INDEX vehHeli)
	IF NOT HAS_NET_TIMER_STARTED(tdRappelStabilizeTimer)
		REINIT_NET_TIMER(tdRappelStabilizeTimer)
	ELSE
		IF HAS_NET_TIMER_EXPIRED(tdRappelStabilizeTimer, RAPPEL_STABILIZE_TIME)
			RESET_NET_TIMER(tdRappelStabilizeTimer)
			EXIT
		ENDIF
		
		BRING_VEHICLE_TO_HALT(vehHeli, 5.0, 1, TRUE)
		
		VECTOR vCurrentHeliRot = GET_ENTITY_ROTATION(vehHeli)
		
		FLOAT fStablizeFrac = TO_FLOAT(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdRappelStabilizeTimer)) / RAPPEL_STABILIZE_TIME
		fStablizeFrac = CLAMP(fStablizeFrac, 0, 1)
		
		PRINTLN("STABILIZE_HELI_FOR_RAPPEL - fStablizeFrac = ", fStablizeFrac, ", at time = ", TO_FLOAT(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdRappelStabilizeTimer)))
		
		SET_ENTITY_ROTATION(vehHeli, <<LERP_FLOAT(vCurrentHeliRot.X, 0, fStablizeFrac), LERP_FLOAT(vCurrentHeliRot.Y, 0, fStablizeFrac), vCurrentHeliRot.Z>>)
	ENDIF
ENDPROC

PROC PROCESS_RAPPELLING_FROM_HELI()
	VEHICLE_INDEX viCurrentVehicle
	BOOL bPerformingTask

	IF NOT IS_RAPPELLING_ALLOWED()
	AND NOT IS_RAPPELLING_FORCED()
		EXIT
	ENDIF
	
	IF GET_SCRIPT_TASK_STATUS(LocalPlayerPed, SCRIPT_TASK_RAPPEL_FROM_HELI) = PERFORMING_TASK
	OR GET_SCRIPT_TASK_STATUS(LocalPlayerPed, SCRIPT_TASK_RAPPEL_FROM_HELI) = WAITING_TO_START_TASK
		PRINTLN("PROCESS_RAPPELLING_FROM_HELI - Performing Rappel!")
		bPerformingTask = TRUE
	ENDIF
	
	IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		viCurrentVehicle = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
		IF NOT IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(viCurrentVehicle))
			EXIT
		ENDIF
	ELSE
		EXIT
	ENDIF
	
	IF GET_ENTITY_SPEED(viCurrentVehicle) > 1.5
		PRINTLN("PROCESS_RAPPELLING_FROM_HELI - Cannot leave vehicle. Heli moving too fast.")
		EXIT
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck31, LBOOL31_SOMEONE_IS_RAPPELLING)
	AND NETWORK_HAS_CONTROL_OF_ENTITY(viCurrentVehicle)
		PROCESS_HELI_STABILIZATION(viCurrentVehicle)
	ENDIF
	
	IF NOT bPerformingTask
		
		IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_RAPELLING_FROM_HELI)
			CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_RAPELLING_FROM_HELI)
		ENDIF
		
		DISPLAY_HELP_TEXT_THIS_FRAME("RAPP_HELP_PAS1", FALSE)
		
		sRappelButtonHold.action = INPUT_FRONTEND_X
		sRappelButtonHold.control = FRONTEND_CONTROL

		IF IS_CONTROL_HELD(sRappelButtonHold, DEFAULT, RAPPEL_BUTTON_TIME)
		OR IS_RAPPELLING_FORCED()
			FLOAT fHeight = GET_ENTITY_HEIGHT_ABOVE_GROUND(viCurrentVehicle)
			IF NOT IS_VEHICLE_FUCKED_MP(viCurrentVehicle)
			AND GET_PED_IN_VEHICLE_SEAT(viCurrentVehicle, VS_FRONT_RIGHT) != LocalPlayerPed
			AND !bPerformingTask
			AND SAFE_TO_DO_INPUT_CHECK()
			AND fHeight < RAPPELLING_MAX_HEIGHT
			AND fHeight > 10.0
				CLEAR_PED_TASKS(LocalPlayerPed)
				TASK_RAPPEL_FROM_HELI(LocalPlayerPed)
				SET_BIT(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_RAPELLING_FROM_HELI)
				PRINTLN("PROCESS_RAPPELLING_FROM_HELI - Rappelling from heli!!")
			ENDIF
		ENDIF
	ENDIF

ENDPROC

//Store the last time the local player fired their weapon
PROC PROCESS_LAST_USED_WEAPON()
	WEAPON_TYPE wtTempWeapon
	INT iTempAmmoInClip
	
	IF GET_CURRENT_PED_WEAPON(LocalPlayerPed, wtTempWeapon)
		IF wtTempWeapon = wtLastUsedWeapon			
			IF GET_AMMO_IN_CLIP(LocalPlayerPed, wtTempWeapon, iTempAmmoInClip)
				//Did the local player fire their weapon?
				IF iTempAmmoInClip < iLastAmountOfAmmoInClip
					iLastTimePlayerFiredTheirWeaponInSec = GET_CLOUD_TIME_AS_INT()
					PRINTLN("PROCESS_LAST_USED_WEAPON - The local player fired their weapon this frame. iLastTimePlayerFiredTheirWeaponInSec = ", iLastTimePlayerFiredTheirWeaponInSec)					
				ENDIF
				
				iLastAmountOfAmmoInClip = iTempAmmoInClip
			ENDIF
		ELSE
			//Weapon was changed
			wtLastUsedWeapon = wtTempWeapon
			GET_AMMO_IN_CLIP(LocalPlayerPed, wtTempWeapon, iLastAmountOfAmmoInClip)				
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_VEHICLE_LEAVE_TEXT()
		
	IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		VEHICLE_INDEX viCurrentVehicle = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
				
		INT	ivehID = IS_VEH_A_MISSION_CREATOR_VEH(viCurrentVehicle)
		
		IF (iVehID = -1 OR iVehID >= FMMC_MAX_VEHICLES)
			EXIT
		ENDIF
		
		IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[ivehID].iVehBitsetNine, ciFMMC_VEHICLE9_ShowJumpVehicleText)
			EXIT
		ENDIF
	ELSE
		EXIT
	ENDIF
	
	IF IS_ZONE_TYPE_TRIGGERING(ciFMMC_ZONE_TYPE__PED_PLAYER_PREVENT_LEAVE_VEHICLE)
		EXIT
	ENDIF
	
	IF NOT IS_PHONE_ONSCREEN()
		DISPLAY_HELP_TEXT_THIS_FRAME("VEH_JUMP_H_1", FALSE)
	ENDIF
	
ENDPROC

PROC PROCESS_PLAYER_STOLEN_GOODS_SLOW_DOWN_MC()
	
	IF MC_playerBD[iLocalPart].sLootBag.iStolenGoods >= g_FMMC_STRUCT.iCashGrabSlowTrigger
		
		FLOAT fBlendRatioPercent = TO_FLOAT(ciSTOLEN_GOODS_SLOW_SCALE_END - MC_playerBD[iLocalPart].sLootBag.iStolenGoods)
		fBlendRatioPercent = fBlendRatioPercent /ciSTOLEN_GOODS_SLOW_SCALE_END
		fBlendRatioPercent = 1.0 - fBlendRatioPercent
		fBlendRatioPercent *= 2
		
		FLOAT fNewSpeed = PEDMOVEBLENDRATIO_SPRINT-fBlendRatioPercent
		IF IS_BIT_SET(MC_playerBD[iLocalPart].sLootBag.iLootBitSet, ciFMMC_StolenGoods_Heavy)
			IF fNewSpeed < g_FMMC_STRUCT.fCashGrabSlowCap[ciCASH_GRAB_SLOW_VALUES_HEAVY]
				fNewSpeed = g_FMMC_STRUCT.fCashGrabSlowCap[ciCASH_GRAB_SLOW_VALUES_HEAVY]
			ENDIF
		ELIF IS_BIT_SET(MC_playerBD[iLocalPart].sLootBag.iLootBitSet, ciFMMC_StolenGoods_Medium)
			IF fNewSpeed < g_FMMC_STRUCT.fCashGrabSlowCap[ciCASH_GRAB_SLOW_VALUES_MEDIUM]
				fNewSpeed = g_FMMC_STRUCT.fCashGrabSlowCap[ciCASH_GRAB_SLOW_VALUES_MEDIUM]
			ENDIF
		ELSE
			IF fNewSpeed < g_FMMC_STRUCT.fCashGrabSlowCap[ciCASH_GRAB_SLOW_VALUES_LIGHT]
				fNewSpeed = g_FMMC_STRUCT.fCashGrabSlowCap[ciCASH_GRAB_SLOW_VALUES_LIGHT]
			ENDIF
		ENDIF
		
		g_TransitionSessionNonResetVars.fStolenGoodsSpeed = fNewSpeed
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyEight, ciOptionsBS28_DisableJumpOnLootSlow)
			g_TransitionSessionNonResetVars.bStolenGoodsDisableJumping = TRUE
		ELSE
			g_TransitionSessionNonResetVars.bStolenGoodsDisableJumping = FALSE
		ENDIF
		
	ELSE
		g_TransitionSessionNonResetVars.bStolenGoodsDisableJumping = FALSE
		g_TransitionSessionNonResetVars.fStolenGoodsSpeed = -1.0
	ENDIF

ENDPROC

FUNC BOOL CAN_PLAYER_KILL_THEMSELVES(INT& iTeam)

	IF MC_serverBD.iNumStartingPlayers[iTeam] <= 1
		RETURN FALSE
	ENDIF
	
	IF MC_serverBD.iNumberOfPlayingPlayers[iTeam] != 1
		RETURN FALSE
	ENDIF
	
	IF IS_MISSION_TEAM_LIVES()
		INT iTeamLives = GET_FMMC_MISSION_LIVES_FOR_TEAM(iTeam)
		IF iTeamLives = ciFMMC_UNLIMITED_LIVES
			RETURN FALSE
		ENDIF
		IF GET_TEAM_DEATHS(iTeam) != iTeamLives
			RETURN FALSE
		ENDIF
		
	ELIF IS_MISSION_COOP_TEAM_LIVES()
		INT iTeamLives = GET_FMMC_MISSION_LIVES_FOR_COOP_TEAMS()
		IF iTeamLives = ciFMMC_UNLIMITED_LIVES
			RETURN FALSE
		ENDIF
		IF GET_COOP_TEAMS_TOTAL_DEATHS(iTeam) != iTeamLives
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE

ENDFUNC

PROC PROCESS_ALLOW_KILL_YOURSELF_ON_MISSION(INT& iTeam)

	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySeven, ciOptionsBS27_AllowKillYourselfOnMission)
		EXIT
	ENDIF
	
	IF IS_FAKE_MULTIPLAYER_MODE_SET(TRUE)
		EXIT
	ENDIF
	
	IF CAN_PLAYER_KILL_THEMSELVES(iTeam)
		IF !g_bAllowKillYourselfOnMission
			g_bAllowKillYourselfOnMission = TRUE
			PRINTLN("PROCESS_ALLOW_KILL_YOURSELF_ON_MISSION - Setting g_bAllowKillYourselfOnMission to TRUE")
		ENDIF
	ELSE
		IF g_bAllowKillYourselfOnMission
			g_bAllowKillYourselfOnMission = FALSE
			PRINTLN("PROCESS_ALLOW_KILL_YOURSELF_ON_MISSION - Setting g_bAllowKillYourselfOnMission to FALSE")
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_PLAYER_USING_PLACED_PED_TAXI()
	
	IF !bLocalPlayerPedOK
		EXIT
	ENDIF
	
	IF NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed, TRUE)
		EXIT
	ENDIF
	
	VEHICLE_INDEX vehPlayerIsIn
	IF GET_VEHICLE_PED_IS_ENTERING(LocalPlayerPed) != NULL
		vehPlayerIsIn = GET_VEHICLE_PED_IS_ENTERING(LocalPlayerPed)
	ELSE
		vehPlayerIsIn = GET_VEHICLE_PED_IS_IN(LocalPlayerPed, TRUE)
	ENDIF
	
	IF NOT IS_VEHICLE_DRIVEABLE(vehPlayerIsIn)
		EXIT
	ENDIF
	
	PED_INDEX pedDriver = GET_PED_IN_VEHICLE_SEAT(vehPlayerIsIn, VS_DRIVER, TRUE)	
	
	IF IS_PED_INJURED(pedDriver)
		EXIT
	ENDIF
	
	INT iTempPed = IS_ENTITY_A_MISSION_CREATOR_ENTITY(pedDriver)
	IF iTempPed > -1
	AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iTempPed].iPedBitsetThirteen, ciPED_BSThirteen_BlockGoToTaskWhenVehicleNotFull)	
		
		VECTOR vPos = GET_ENTITY_COORDS(vehPlayerIsIn)
		FLOAT fGround
		FLOAT fDistanceFromGround
		GET_GROUND_Z_FOR_3D_COORD(vPos, fGround, TRUE)
		fDistanceFromGround = vPos.z - fGround
		
		IF fDistanceFromGround > 1.0
		OR GET_VEHICLE_PED_IS_ENTERING(LocalPlayerPed) != NULL
			PRINTLN("[LM][PedAiTaxi] - PROCESS_PLAYER_USING_PLACED_PED_TAXI - iTempPed: ", iTempPed," is our taxi - Disabling Vehicle Entry/Exit")
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER, TRUE)
		ELSE
			PRINTLN("[LM][PedAiTaxi] - PROCESS_PLAYER_USING_PLACED_PED_TAXI - iTempPed: ", iTempPed," is our taxi - Not disabling vehicle Entry/Exit fDistanceFromGround: ", fDistanceFromGround)
		ENDIF
	ELSE
		PRINTLN("[LM][PedAiTaxi] - PROCESS_PLAYER_USING_PLACED_PED_TAXI - iTempPed: ", iTempPed," is our taxi - Not processing because they might not have ciPED_BSThirteen_BlockGoToTaskWhenVehicleNotFull set")
	ENDIF
	
ENDPROC

PROC CLEANUP_JET_SMOKE_TRAIL()
	IF NOT IS_MULTI_TEAM_VEHICLE_SEAT_SWAP_PAST_INITIATED() //Not stopping particle effect whilst switching seats
		IF DOES_PARTICLE_FX_LOOPED_EXIST(StockpileSmokeTrail)
			PRINTLN("[RCC MISSION] PROCESS_JET_SMOKE_TRAIL - Stopping StockpileSmokeTrail")
			STOP_PARTICLE_FX_LOOPED(StockpileSmokeTrail)
		ENDIF
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(SmokeTrailTimer)
		RESET_NET_TIMER(SmokeTrailTimer)
	ENDIF
ENDPROC

PROC PROCESS_JET_SMOKE_TRAIL(INT& iTeam)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciENABLE_SMOKE_TRAILS)
		EXIT
	ENDIF
	
	IF NOT bLocalPlayerPedOK
	OR g_bMissionEnding
	OR IS_MULTI_TEAM_VEHICLE_SEAT_SWAP_PAST_INITIATED()
	OR manualRespawnState != eManualRespawnState_OKAY_TO_SPAWN
	OR MC_playerBD[iLocalPart].iObjCarryCount = 0
	OR NOT IS_PED_CARRYING_ANY_OBJECTS(LocalPlayerPed)
		CLEANUP_JET_SMOKE_TRAIL()
		EXIT
	ENDIF
	
	IF NOT SHOULD_SMOKE_SHOW()
		CLEANUP_JET_SMOKE_TRAIL()
		EXIT
	ENDIF
	
	IF NOT HAS_NET_TIMER_STARTED(SmokeTrailTimer)
		REINIT_NET_TIMER(SmokeTrailTimer)
		PRINTLN("[RCC MISSION] PROCESS_JET_SMOKE_TRAIL - Starting time delay")
	ENDIF
	
	IF HAS_NET_TIMER_EXPIRED(SmokeTrailTimer, ci_SMOKE_TRAIL_TIMER)
		IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(StockpileSmokeTrail)
			USE_PARTICLE_FX_ASSET("scr_ar_planes")
			VECTOR vPFXOffset = <<0,0,0>>
			VECTOR vPFXRot = <<0,0,0>>
			STRING sPFXName = "scr_ar_trail_smoke"
			
			IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
				VECTOR vMin, vMax
				GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(LocalPlayerPed)),vMin, vMax)
				vPFXOffset = <<0,vMin.y,0>>
				IF GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(LocalPlayerPed)) = HYDRA
				OR GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(LocalPlayerPed)) = LAZER
					vPFXOffset.y -= 4
				ENDIF
			ENDIF
			
			INT iR, iG, iB, iA
			GET_HUD_COLOUR(GET_HUD_COLOUR_FOR_FMMC_TEAM(iTeam, LocalPlayer), iR, iG, iB, iA)
			FLOAT fR = TO_FLOAT(iR)
			FLOAT fG = TO_FLOAT(iG)
			FLOAT fB = TO_FLOAT(iB)
			
			fR = fR / 255
			fG = fG / 255
			fB = fB / 255
			
			ENTITY_INDEX entityToUse
			
			IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
				entityToUse = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
				IF GET_ENTITY_MODEL(entityToUse) = HUNTER
				OR GET_ENTITY_MODEL(entityToUse) = NOKOTA
					sPFXName = "scr_ar_trail_smoke_slow"
				ENDIF
			ELSE
				entityToUse = LocalPlayerPed
			ENDIF
			
			PRINTLN("[RCC MISSION] PROCESS_JET_SMOKE_TRAIL - Colour: RGB: ", fR,", ", fG,", ", fB)
			
			StockpileSmokeTrail = START_NETWORKED_PARTICLE_FX_LOOPED_ON_ENTITY_BONE(sPFXName,entityToUse,vPFXOffset,vPFXRot ,DEFAULT, g_FMMC_STRUCT.fSmokeTrailScale, DEFAULT, DEFAULT, DEFAULT, fR, fG, fB)
		ENDIF
	ENDIF
		
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Player Phone ----------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: The player phone and interactions with it is processed here.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC SET_PHONE_EMP_STATE(PHONE_EMP_STATE eNewState)
	IF ePhoneEMPState != eNewState
		ePhoneEMPState = eNewState
		PRINTLN("[PhoneEMP] Setting ePhoneEMPState to ", ePhoneEMPState)
	ENDIF
ENDPROC

PROC ACTIVATE_PHONE_EMP()
	
	IF NOT IS_PHONE_EMP_CURRENTLY_ACTIVE()
		PRINTLN("[PhoneEMP] Activating Phone EMP!")

		PLAY_SOUND_FRONTEND(-1, "emp_activate", "dlc_ch_heist_finale_sounds")
		TURN_OFF_ARTIFICIAL_LIGHTS_LOCALLY(FALSE)
		
		REINIT_NET_TIMER(stPhoneEMP_DeactivationTimer)
		PRINTLN("[PhoneEMP] Starting stPhoneEMP_DeactivationTimer")
		
		SET_BIT(iLocalBoolCheck31, LBOOL31_PHONE_EMP_IS_ACTIVE)
	ENDIF
ENDPROC

PROC DEACTIVATE_PHONE_EMP()

	IF IS_PHONE_EMP_CURRENTLY_ACTIVE()
		PRINTLN("[PhoneEMP] Deactivating Phone EMP!")
		
		PLAY_SOUND_FRONTEND(-1, "power_on", "dlc_ch_heist_finale_sounds", FALSE)
		TURN_ON_ARTIFICIAL_LIGHTS_LOCALLY(FALSE)
		
		IF HAS_NET_TIMER_STARTED(stPhoneEMP_DeactivationTimer)
			RESET_NET_TIMER(stPhoneEMP_DeactivationTimer)
			PRINTLN("[PhoneEMP] DEACTIVATE_PHONE_EMP - Starting stPhoneEMP_DeactivationTimer")
		ENDIF
		
		CLEAR_BIT(iLocalBoolCheck31, LBOOL31_PHONE_EMP_IS_ACTIVE)
	ENDIF
ENDPROC

PROC PROCESS_PHONE_EMP()

	IF NOT IS_PHONE_EMP_AVAILABLE()
		EXIT
	ENDIF
	
	IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
		IF g_bMissionEnding
			
			iHackStage = ciSECUROSERV_HACK_STAGE_NO_SIGNAL
			SET_PHONE_EMP_STATE(PHONE_EMP_STATE__WAITING)
			
			IF IS_PHONE_ONSCREEN()
				HANG_UP_AND_PUT_AWAY_PHONE()
			ENDIF
			
			IF IS_SOUND_ID_VALID(iHackingLoopSoundID)
				STOP_SOUND(iHackingLoopSoundID)
				RELEASE_SOUND_ID(iHackingLoopSoundID)
			ENDIF
			
			IF IS_PHONE_EMP_CURRENTLY_ACTIVE()
				DEACTIVATE_PHONE_EMP()
			ENDIF
			
			PRINTLN("[PhoneEMP] PROCESS_PHONE_EMP - Exiting because Mission is ending")
			EXIT
		ENDIF
	ENDIF
	
	IF HAS_PHONE_EMP_BEEN_USED()
		IF ePhoneEMPState = PHONE_EMP_STATE__WAITING
			//For the mission(s) after using it
			SET_PHONE_EMP_STATE(PHONE_EMP_STATE__FINISHED)
			PRINTLN("[PhoneEMP] PROCESS_PHONE_EMP - EMP has been used")
			
		ELIF ePhoneEMPState = PHONE_EMP_STATE__FINISHED
			iHackStage = ciSECUROSERV_HACK_STAGE_NO_SIGNAL
			
		ENDIF
	ENDIF
	
	SWITCH ePhoneEMPState
		CASE PHONE_EMP_STATE__INIT
		
			IF NOT IS_BIT_SET(BitSet_CellphoneTU, g_BSTU_ENABLE_MP_SECUROSERV_HACK_APP)
				PRINTLN("[PhoneEMP] PROCESS_PHONE_EMP - Turning on")
				SET_BIT(BitSet_CellphoneTU, g_BSTU_ENABLE_MP_SECUROSERV_HACK_APP)
			ELSE
				PRINTLN("[PhoneEMP] PROCESS_PHONE_EMP - App is already turned on")
			ENDIF
			
			iHackStage = ciSECUROSERV_HACK_STAGE_NO_SIGNAL
			SET_PHONE_EMP_STATE(PHONE_EMP_STATE__LOADING_ASSETS)
		BREAK
		
		CASE PHONE_EMP_STATE__LOADING_ASSETS
			SET_PHONE_EMP_STATE(PHONE_EMP_STATE__WAITING)
		BREAK
		
		CASE PHONE_EMP_STATE__WAITING
		
			BOOL bShouldMoveOn
			bShouldMoveOn = GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("appSecuroHack")) > 0
			
			#IF IS_DEBUG_BUILD
			IF bPhoneEMPDebug
				IF IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD1)
				AND IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD2)
					bShouldMoveOn = TRUE
				ENDIF
			ENDIF
			#ENDIF
			
			IF bShouldMoveOn
				iHackStage = ciSECUROSERV_HACK_STAGE_HACKING
				iHackingLoopSoundID = GET_SOUND_ID()
				
				PLAY_SOUND_FRONTEND(-1, "Hack_Start", "DLC_IE_SVM_Voltic2_Hacking_Sounds")
				PLAY_SOUND_FRONTEND(iHackingLoopSoundID, "Hack_Loop", "DLC_IE_SVM_Voltic2_Hacking_Sounds")
				SET_PHONE_EMP_STATE(PHONE_EMP_STATE__HACKING)
			ENDIF
		BREAK
		
		CASE PHONE_EMP_STATE__HACKING
			fPhoneEMPCharge += GET_FRAME_TIME() * (100 / cfPhoneEMP_ChargeTimeInSeconds)
			SET_VARIABLE_ON_SOUND(iHackingLoopSoundID, "percentageComplete", fPhoneEMPCharge)
			
			IF FLOOR(fPhoneEMPCharge) > iHackPercentage
				iHackPercentage = FLOOR(fPhoneEMPCharge)
			ENDIF
			
			IF iHackPercentage >= 100
			
				STOP_SOUND(iHackingLoopSoundID)
				RELEASE_SOUND_ID(iHackingLoopSoundID)
				
				PLAY_SOUND_FRONTEND(-1, "Hack_Complete", "DLC_IE_SVM_Voltic2_Hacking_Sounds")
			
				tl15HackComplete = "FMMC_PEMP_SUC"
				iHackStage = ciSECUROSERV_HACK_STAGE_HACK_COMPLETE
				SET_PHONE_EMP_STATE(PHONE_EMP_STATE__TRIGGERING)
			ENDIF
		BREAK
		
		CASE PHONE_EMP_STATE__TRIGGERING
			IF HAS_NET_TIMER_STARTED(stPhoneEMP_ActivationDelayTimer)
				IF HAS_NET_TIMER_EXPIRED(stPhoneEMP_ActivationDelayTimer, ciPhoneEMP_EMPActivateDelay)
					BROADCAST_FMMC_PHONE_EMP_EVENT(TRUE, FALSE)
					RESET_NET_TIMER(stPhoneEMP_ActivationDelayTimer)
					SET_PHONE_EMP_STATE(PHONE_EMP_STATE__WAITING_TO_HANG_UP)
				ENDIF
			ELSE
				REINIT_NET_TIMER(stPhoneEMP_ActivationDelayTimer)
			ENDIF
		BREAK
		
		CASE PHONE_EMP_STATE__WAITING_TO_HANG_UP
			IF HAS_NET_TIMER_EXPIRED(stPhoneEMP_DeactivationTimer, ciPhoneEMP_HangupTime)
				IF IS_PHONE_ONSCREEN()
					PRINTLN("[PhoneEMP] Putting away phone now!")
					HANG_UP_AND_PUT_AWAY_PHONE()
				ENDIF
				
				SET_PHONE_EMP_STATE(PHONE_EMP_STATE__WAITING_TO_DEACTIVATE)
			ENDIF
		BREAK
		
		CASE PHONE_EMP_STATE__WAITING_TO_DEACTIVATE
			
			iHackStage = ciSECUROSERV_HACK_STAGE_NO_SIGNAL
			BOOL bShouldDeactivate
			bShouldDeactivate = HAS_NET_TIMER_EXPIRED(stPhoneEMP_DeactivationTimer, g_FMMC_STRUCT.sPhoneEMPSettings.iPhoneEMP_Duration)
			
			#IF IS_DEBUG_BUILD
			IF bPhoneEMPDebug
				IF IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD1)
				AND IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD4)
					bShouldDeactivate = TRUE
				ENDIF
			ENDIF
			#ENDIF
			
			IF bShouldDeactivate
				RESET_NET_TIMER(stPhoneEMP_DeactivationTimer)
				SET_PHONE_EMP_STATE(PHONE_EMP_STATE__DEACTIVATING)
			ENDIF
		BREAK
		
		CASE PHONE_EMP_STATE__DEACTIVATING
			BROADCAST_FMMC_PHONE_EMP_EVENT(FALSE, TRUE)
			SET_PHONE_EMP_STATE(PHONE_EMP_STATE__FINISHED)
		BREAK
		
		CASE PHONE_EMP_STATE__FINISHED
			//Do nothing
		BREAK
	ENDSWITCH
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Player Drone ----------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Local Player Mission Controller Drone functionality is handled in this section.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL CREATE_DRONE_PICKUP()
	INT iModelHash =  HASH("ch_Prop_Casino_Drone_01a") 
		
	REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, iModelHash) )
	
	IF HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, iModelHash))
		IF CAN_REGISTER_MISSION_OBJECTS(1)
			IF NOT DOES_ENTITY_EXIST(oiDronePickup)
				VECTOR tempDronePos =  GlobalplayerBD[NATIVE_TO_INT(LocalPlayer)].vDroneCoords 
				GET_GROUND_Z_FOR_3D_COORD(GlobalplayerBD[NATIVE_TO_INT(LocalPlayer)].vDroneCoords, tempDronePos.z)
				
				vDronePosCache = tempDronePos
				tempDronePos.z  = tempDronePos.z + 1.0
				
				
				oiDronePickup = CREATE_OBJECT(INT_TO_ENUM(MODEL_NAMES, iModelHash), tempDronePos, FALSE, FALSE, TRUE)
				
				PRINTLN("PROCESS_DRONE_USAGE - CREATE DRONE PICKUP: CREATING")
			ENDIF
			
			IF DOES_ENTITY_EXIST(oiDronePickup)
				SET_ENTITY_COLLISION(oiDronePickup, FALSE)
				SET_USE_KINEMATIC_PHYSICS(oiDronePickup, TRUE)
				
				SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, iModelHash))
				
				
				IF NOT DOES_BLIP_EXIST(biDronePickup)
					biDronePickup = ADD_BLIP_FOR_ENTITY(oiDronePickup)
					SET_BLIP_SPRITE(biDronePickup, RADAR_TRACE_BAT_DRONE)
				ENDIF
					
				PRINTLN("PROCESS_DRONE_USAGE - CREATE DRONE PICKUP: SUCCESS")
				RETURN TRUE
			ENDIF
		ELSE
			PRINTLN("PROCESS_DRONE_USAGE - CAN'T REGISTER PROP")
		ENDIF
	ELSE
		PRINTLN("PROCESS_DRONE_USAGE - CREATE DRONE PICKUP: LOADING ASSET")
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_PLAYER_COLLECTED_DRONE_PICKUP()
	IF IS_BIT_SET(iLocalBoolCheck33, LBOOL33_DRONE_PICKUP_SPAWNED)
		IF DOES_ENTITY_EXIST(oiDronePickup)
		
			IF IS_ENTITY_AT_COORD(LocalPlayerPed, vDronePosCache, <<2.0,2.0,2.0>>,FALSE, TRUE,TM_ON_FOOT )
				PRINTLN("PROCESS_DRONE_USAGE - HAS_PLAYER_COLLECTED_DRONE_PICKUP: SUCCESS")				
				CLEANUP_BLIP(biDronePickup)
				
				DELETE_OBJECT(oiDronePickup)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC REENABLE_DRONE_USAGE()
	CLEAR_BIT(sMissionLocalContinuityVars.iGeneralLocalTrackingBitset, ciContinuityGenericTrackingBS_DroneUsed)
	CLEAR_BIT(iLocalBoolCheck32, LBOOL32_USING_DRONE)
	CLEAR_BIT(iLocalBoolCheck33, LBOOL33_DRONE_PICKUP_SPAWNED)
	CLEAR_BIT(iLocalBoolCheck33, LBOOL33_DRONE_PICKUP_COLLECTED)
	OVERRIDE_DRONE_COOL_DOWN_TIMER(0)
	SET_ENABLE_DRONE_USE_FROM_PI_MENU_IN_MISSION(TRUE)
	PRINTLN("[NM] PROCESS_DRONE_USAGE - REENABLE_DRONE_USAGE()")
ENDPROC

PROC PROCESS_DRONE_USAGE()

	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_EnableDroneDeployment)
		//Drone has been used.
		IF IS_BIT_SET(iLocalBoolCheck33, LBOOL33_SHOULD_CREATE_PICKUP)
			PRINTLN("PROCESS_DRONE_USAGE - SHOULD CREATE PICKUP")
			IF NOT IS_BIT_SET(iLocalBoolCheck33, LBOOL33_DRONE_PICKUP_SPAWNED)
				PRINTLN("PROCESS_DRONE_USAGE - SHOULD CREATE PICKUP - NOT LBOOL33_DRONE_PICKUP_SPAWNED ")
				IF CREATE_DRONE_PICKUP()
					SET_BIT(iLocalBoolCheck33, LBOOL33_DRONE_PICKUP_SPAWNED)
					CLEAR_BIT(iLocalBoolCheck33, LBOOL33_SHOULD_CREATE_PICKUP)
				ENDIF
			ENDIF
		ENDIF
		
		
		// Collection logic here.
		IF HAS_PLAYER_COLLECTED_DRONE_PICKUP()
			SET_BIT(iLocalBoolCheck33, LBOOL33_DRONE_PICKUP_COLLECTED)
			PRINTLN("[NM] - PROCESS_DRONE_USAGE - PICKUP COLLECTED")
		ENDIF
		
		IF NOT IS_BIT_SET(iLocalBoolCheck31, LBOOL33_EMP_BLOCKING_DRONE_ACTIVE)
			IF IS_ANY_EMP_CURRENTLY_ACTIVE()
				IF IS_LOCAL_PLAYER_USING_DRONE()
					SET_BIT(iLocalBoolCheck33, LBOOL33_SHOULD_CREATE_PICKUP)
					PRINTLN("[NM] - PROCESS_DRONE_USAGE - EMP DISABLED DRONES")
					SET_DRONE_FORCE_EXTERNAL_CLEANING_UP(TRUE)
				ENDIF
				
				SET_BIT(sMissionLocalContinuityVars.iGeneralLocalTrackingBitset, ciContinuityGenericTrackingBS_DroneUsed)
				CLEAR_BIT(iLocalBoolCheck32, LBOOL32_USING_DRONE)	
				SET_ENABLE_DRONE_USE_FROM_PI_MENU_IN_MISSION(FALSE)
				SET_BIT(iLocalBoolCheck31, LBOOL33_EMP_BLOCKING_DRONE_ACTIVE)
				PRINTLN("[NM] - PROCESS_DRONE_USAGE - SETTING LBOOL33_EMP_BLOCKING_DRONE_ACTIVE")
				EXIT
			ENDIF
		ELSE
			IF IS_ANY_EMP_CURRENTLY_ACTIVE()
				EXIT
			ELSE
				//Re-enable the drone
				
				//re enable the option if it's not got a pickup already available.
				IF NOT IS_BIT_SET(iLocalBoolCheck33, LBOOL33_DRONE_TRANQUILISER_AMMO_ASSIGNED)
					REENABLE_DRONE_USAGE()
				ENDIF
				
				PRINTLN("[NM] - PROCESS_DRONE_USAGE - EMP EXPIRED -  CLEARING LBOOL33_EMP_BLOCKING_DRONE_ACTIVE")
				CLEAR_BIT(iLocalBoolCheck31, LBOOL33_EMP_BLOCKING_DRONE_ACTIVE)
			ENDIF
		ENDIF
		
		
		IF IS_BIT_SET(iLocalBoolCheck33, LBOOL33_DRONE_PICKUP_COLLECTED)
		AND NOT IS_BIT_SET(iLocalBoolCheck31, LBOOL33_EMP_BLOCKING_DRONE_ACTIVE)
			REENABLE_DRONE_USAGE()
		ENDIF
		
		IF HAS_DRONE_BEEN_USED()
		OR IS_BIT_SET(iLocalBoolCheck31, LBOOL33_EMP_BLOCKING_DRONE_ACTIVE)
			EXIT
		ENDIF
		
		IF IS_BIT_SET(iLocalBoolCheck32, LBOOL32_USING_DRONE)
			IF NOT IS_LOCAL_PLAYER_USING_DRONE()
				SET_BIT(iLocalBoolCheck33, LBOOL33_SHOULD_CREATE_PICKUP)
				SET_BIT(sMissionLocalContinuityVars.iGeneralLocalTrackingBitset, ciContinuityGenericTrackingBS_DroneUsed)
				CLEAR_BIT(iLocalBoolCheck32, LBOOL32_USING_DRONE)
				PRINTLN("[JS][CONTINUITY] - PROCESS_DRONE_USAGE - Finished using drone - setting as used")
				CLEAR_BIT(iLocalBoolCheck33, LBOOL33_DRONE_PICKUP_SPAWNED)
				SET_ENABLE_DRONE_USE_FROM_PI_MENU_IN_MISSION(FALSE)
			ENDIF
		ELSE
			IF IS_LOCAL_PLAYER_USING_DRONE()
				IF NOT IS_BIT_SET(iLocalBoolCheck33, LBOOL33_DRONE_TRANQUILISER_AMMO_ASSIGNED)
					SET_DRONE_TRANQUILIZER_AMMO(3)
					SET_BIT(iLocalBoolCheck33, LBOOL33_DRONE_TRANQUILISER_AMMO_ASSIGNED)
					PRINTLN("[NM] - PROCESS_DRONE_USAGE - Started using drone - setting as being used")
				ENDIF
				
				PRINTLN("[JS][CONTINUITY] - PROCESS_DRONE_USAGE - Started using drone - setting as being used")
				SET_BIT(iLocalBoolCheck32, LBOOL32_USING_DRONE)
			 	
			ENDIF
		ENDIF
		
		
	ELSE
		//Drones not enabled
		//PRINTLN("[CONTINUITY] - PROCESS_DRONE_USAGE - Drones not set as enabled on this mission")
		EXIT
	ENDIF
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Player Controls and State  -----------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: This section covers the processing of player controls and their current state, e.g. forced cover, etc.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_CANCEL_DIALOGUE_CALL_ANIMATION()
	IF NOT IS_BIT_SET(iLocalBoolCheck34, LBOOL34_LOCAL_PLAYER_CANCEL_CALL_ANIMATION_AFTER_DELAY)
		EXIT
	ENDIF
	
	DISABLE_HANGUP_FOR_THIS_CALL(TRUE)
	
	IF IS_PHONE_ONSCREEN_AND_RINGING(FALSE)
	OR IS_PHONE_ONSCREEN_AND_RINGING(TRUE)		
		PRINTLN("PROCESS_CANCEL_DIALOGUE_CALL_ANIMATION - Blocking Pause Menu input (INPUT_FRONTEND_PAUSE) 1")
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE, TRUE)
	ENDIF
	
	IF g_Cellphone.PhoneDS != PDS_ONGOING_CALL		
		EXIT
	ELSE
		PRINTLN("PROCESS_CANCEL_DIALOGUE_CALL_ANIMATION - g_Cellphone.PhoneDS = PDS_ONGOING_CALL")
	ENDIF
	
	IF NOT HAS_NET_TIMER_STARTED(tdDialogue_CancelCallAnimTimer)
		PRINTLN("PROCESS_CANCEL_DIALOGUE_CALL_ANIMATION - Starting Delay.")
		START_NET_TIMER(tdDialogue_CancelCallAnimTimer)
	ENDIF
	
	IF NOT HAS_NET_TIMER_EXPIRED_READ_ONLY(tdDialogue_CancelCallAnimTimer, DIALOGUE_CANCEL_CALL_ANIM_TIME_DELAY)		
		PRINTLN("PROCESS_CANCEL_DIALOGUE_CALL_ANIMATION - Blocking Pause Menu input (INPUT_FRONTEND_PAUSE) 2")
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE, TRUE)
		EXIT
	ENDIF
	
	PRINTLN("PROCESS_CANCEL_DIALOGUE_CALL_ANIMATION - Delay Finished, Cancelled Phone Task, putting it away but continuing the call.")
 	
	PUT_AWAY_ACTIVE_PHONE()
	
	SET_BIT(BitSet_CellphoneDisplay, g_BS_HAS_CELLPHONE_FULLY_MOVED_UP)
	
	RESET_NET_TIMER(tdDialogue_CancelCallAnimTimer)
	
	CLEAR_BIT(iLocalBoolCheck34, LBOOL34_LOCAL_PLAYER_CANCEL_CALL_ANIMATION_AFTER_DELAY)
	
	g_bMissionDoNotHangUpFromDialogueHandler = TRUE
ENDPROC

FUNC BOOL IS_DISABLED_CONTROL_HELD(CONTROL_ACTION caAction, INT iHoldDuration)
	
	IF IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, caAction)
	AND HAS_NET_TIMER_STARTED(tdButtonHeldTimer)
	AND HAS_NET_TIMER_EXPIRED(tdButtonHeldTimer, iHoldDuration)
	AND IS_BIT_SET(iLocalBoolCheck25, LBOOL25_HOLDING_CONTROL)
		RETURN TRUE
	ELIF IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, caAction)
		IF NOT IS_BIT_SET(iLocalBoolCheck25, LBOOL25_HOLDING_CONTROL)
			SET_BIT(iLocalBoolCheck25, LBOOL25_HOLDING_CONTROL)
			START_NET_TIMER(tdButtonHeldTimer)
		ENDIF
	ELSE
		IF IS_BIT_SET(iLocalBoolCheck25, LBOOL25_HOLDING_CONTROL)		
			CLEAR_BIT(iLocalBoolCheck25, LBOOL25_HOLDING_CONTROL)
		ENDIF
		IF HAS_NET_TIMER_STARTED(tdButtonHeldTimer)
			RESET_NET_TIMER(tdButtonHeldTimer)
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC DISABLE_WEAPON(WEAPON_TYPE wtWeapon)
	
	WEAPON_TYPE wtEquipped
	
	IF GET_CURRENT_PED_WEAPON(LocalPlayerPed, wtEquipped)
		IF wtEquipped = wtWeapon
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_SPECIAL)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_SPECIAL_TWO)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PASSENGER_ATTACK)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_ALTERNATE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_BLOCK)
			PRINTLN("[RCC MISSION] DISABLE_WEAPON - Disabling attacks as weapon ",GET_WEAPON_NAME_FROM_WEAPON_TYPE(wtWeapon)," is equipped")
		ENDIF
	ENDIF
	
	IF wtWeapon = WEAPONTYPE_STICKYBOMB
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DETONATE)
	ENDIF
	
ENDPROC

PROC DISABLE_VEHICLE_ENTRY_FOR_ALL_BUT_THIS_VEHICLE()
	INT iVehicleNear = MC_playerBD[iPartToUse].iVehNear
	INT iTeam = MC_playerBD[ iPartToUse ].iteam
	INT iRule = MC_ServerBD_4.iCurrentHighestPriority[ iTeam ]
	VEHICLE_INDEX lastVehicle = NULL
	
	IF iVehicleNear > -1
	AND IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitset[ iRule + 1 ], ciBS_RULE_CANNOT_EXIT_VEHICLE )
		IF NETWORK_DOES_NETWORK_ID_EXIST( MC_serverBD_1.sFMMC_SBD.niVehicle[ iVehicleNear ] )
			VEHICLE_INDEX vehToGoTo = NET_TO_VEH( MC_serverBD_1.sFMMC_SBD.niVehicle[ iVehicleNear ] )
			IF IS_VEHICLE_DRIVEABLE(vehToGoTo)
				SET_PLAYER_MAY_ONLY_ENTER_THIS_VEHICLE(LocalPlayer, vehToGoTo)
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		lastVehicle = GET_LAST_DRIVEN_VEHICLE()
		IF DOES_ENTITY_EXIST(lastVehicle)
			IF lastVehicle != NULL
			AND IS_VEHICLE_DRIVEABLE(lastVehicle)
				SET_PLAYER_MAY_ONLY_ENTER_THIS_VEHICLE(LocalPlayer, lastVehicle)
			ELSE
				SET_PLAYER_MAY_ONLY_ENTER_THIS_VEHICLE(LocalPlayer, NULL) //#2148781
				SET_PLAYER_MAY_NOT_ENTER_ANY_VEHICLE(LocalPlayer)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC BLOCK_VEHICLE_REWARD_WEAPONS_DURING_LOCKED_CAPTURE()
	IF IS_JOB_FORCED_WEAPON_ONLY()
	OR IS_JOB_FORCED_WEAPON_PLUS_PICKUPS()
		DISABLE_PLAYER_VEHICLE_REWARDS(LocalPlayer)
	ENDIF 
ENDPROC

PROC PROCESS_LOCAL_PLAYER_PROOFS()
	
	IF DOES_LOCAL_PLAYER_HAVE_NEW_PRIORITY_THIS_FRAME()
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iRuleBitsetFifteen[GET_TEAM_CURRENT_RULE(MC_playerBD[iLocalPart].iTeam)], ciBS_RULE15_SET_TEAM_AS_FIREPROOF)
			SET_BIT(iLocalBoolCheck25, LBOOL25_LOCAL_PLAYER_PROOFS_NEEDS_REFRESH)
			SET_BIT(iLocalBoolCheck28, LBOOL28_DISABLE_LOCAL_PLAYER_FIRE_PROOF)
			PRINTLN("PROCESS_LOCAL_PLAYER_PROOFS - Setting bit LBOOL25_LOCAL_PLAYER_PROOFS_NEEDS_REFRESH due to ciBS_RULE15_SET_TEAM_AS_FIREPROOF being set on one of the rules (", GET_TEAM_CURRENT_RULE(MC_playerBD[iLocalPart].iTeam), ") for the team ", MC_playerBD[iLocalPart].iTeam)
		ELIF IS_BIT_SET(iLocalBoolCheck28, LBOOL28_DISABLE_LOCAL_PLAYER_FIRE_PROOF)
			SET_BIT(iLocalBoolCheck25, LBOOL25_LOCAL_PLAYER_PROOFS_NEEDS_REFRESH)
			CLEAR_BIT(iLocalBoolCheck28, LBOOL28_DISABLE_LOCAL_PLAYER_FIRE_PROOF)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck25, LBOOL25_LOCAL_PLAYER_PROOFS_NEEDS_REFRESH)
		PRINTLN("PROCESS_PROOFS - Refreshing local player proofs due to LBOOL25_LOCAL_PLAYER_PROOFS_NEEDS_REFRESH")
		RUN_LOCAL_PLAYER_PROOF_SETTING(TRUE)
		CLEAR_BIT(iLocalBoolCheck25, LBOOL25_LOCAL_PLAYER_PROOFS_NEEDS_REFRESH)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyThree, ciOptionsBS23_ProofsInvalidatedOnFoot)
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			IF NOT g_bProofsSet 
				RUN_LOCAL_PLAYER_PROOF_SETTING(TRUE)
				g_bProofsSet = TRUE
				PRINTLN("PROCESS_PROOFS - MissionController - Proofs are SET")
			ENDIF
		ELSE
			IF g_bProofsSet
				SET_ENTITY_PROOFS(LocalPlayerPed, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, DEFAULT, FALSE)
				g_bProofsSet = FALSE
				PRINTLN("PROCESS_PROOFS - MissionController - Proofs are all set to FALSE")	
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_PLAYER_FORCED_SEAT_PREF_SPECIAL()
	
	IF iVehSeatPrefOverrideVeh > -1
		IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iVehSeatPrefOverrideVeh])
			VEHICLE_INDEX tempVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVehSeatPrefOverrideVeh])
		
			IF DOES_ENTITY_EXIST(tempVeh)
				INT iDriver = 0
				IF NOT IS_VEHICLE_SEAT_FREE(tempVeh, VS_DRIVER, TRUE)
				OR GET_PED_IN_VEHICLE_SEAT(tempVeh, VS_DRIVER, TRUE) != null				
					iDriver = 1
				ENDIF
				
				IF (NOT IS_BIT_SET(iLocalBoolCheck26, LBOOL26_OVERRIDING_SEAT_PREF_SPECIAL_VEH) AND NOT IS_PED_IN_VEHICLE(localPlayerPed, tempveh))
				OR iPlayersInsideSeatPrefOverrideVeh != (GET_VEHICLE_NUMBER_OF_PASSENGERS(tempVeh)+iDriver)
					INT iFlags
					INT iSlotPref
					
					iPlayersInsideSeatPrefOverrideVeh = (GET_VEHICLE_NUMBER_OF_PASSENGERS(tempVeh)+iDriver)
					
					IF iPlayersInsideSeatPrefOverrideVeh = 0
						iPlayersInsideSeatPref = ENUM_TO_INT(VS_DRIVER)
						iSlotPref = 0
						iFlags = ENUM_TO_INT(VC_FORCE_USE_FRONT_SEATS)
						SET_PED_VEHICLE_FORCED_SEAT_USAGE(LocalPlayerPed, tempVeh, iSlotPref, iFlags)
					ELIF iPlayersInsideSeatPrefOverrideVeh = 1
						iPlayersInsideSeatPref = ENUM_TO_INT(VS_BACK_LEFT)
						iSlotPref = 0
						iFlags = ENUM_TO_INT(VC_FORCE_USE_REAR_SEATS)
						SET_PED_VEHICLE_FORCED_SEAT_USAGE(LocalPlayerPed, tempVeh, iSlotPref, iFlags)
					ELIF iPlayersInsideSeatPrefOverrideVeh = 2
						iPlayersInsideSeatPref = ENUM_TO_INT(VS_BACK_RIGHT)
						iSlotPref = 1
						iFlags = ENUM_TO_INT(VC_FORCE_USE_REAR_SEATS)
						SET_PED_VEHICLE_FORCED_SEAT_USAGE(LocalPlayerPed, tempVeh, iSlotPref, iFlags)						
					ELIF iPlayersInsideSeatPrefOverrideVeh = 3
						iPlayersInsideSeatPref = ENUM_TO_INT(VS_FRONT_RIGHT)
						IF NOT IS_PED_IN_VEHICLE(localPlayerPed, tempveh)
							CLEAR_ALL_PED_VEHICLE_FORCED_SEAT_USAGE(LocalPlayerPed)
						ENDIF
					ENDIF
					
					IF IS_PED_IN_VEHICLE(localPlayerPed, tempVeh, FALSE)
						iPlayersInsideSeatPref = -3
					ENDIf
										
					SET_BIT(iLocalBoolCheck26, LBOOL26_OVERRIDING_SEAT_PREF_SPECIAL_VEH)					
					PRINTLN("[LM][PROCESS_VEH_BODY][LBOOL26_OVERRIDING_SEAT_PREF_SPECIAL_VEH] - Calling Vehicle Seat Prefs on iVeh: ", iVehSeatPrefOverrideVeh, " --- iSlotPref: ", iSlotPref, " iFlags:", iFlags, " iPlayersInsideSeatPrefOverrideVeh: ", iPlayersInsideSeatPrefOverrideVeh)
				ENDIF
				
				IF IS_PED_IN_VEHICLE(localPlayerPed, tempveh)
					CLEAR_BIT(iLocalBoolCheck26, LBOOL26_OVERRIDING_SEAT_PREF_SPECIAL_VEH)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_DISABLE_HEADLIGHT_CONTROL()
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_DISABLE_HEADLIGHT_CONTROL)
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed, TRUE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HEADLIGHT)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_RULE_WEAPON_DISABLING()
	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	IF MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
	
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciMISSION_BLOCK_DROP_WEAPON_ON_WHEEL)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DROP_AMMO)	
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitsetFourteen[ iRule ], ciBS_RULE14_DISABLE_MELEE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK1)	
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK2)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_BLOCK)	
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_ALTERNATE)	
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)	
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)	
			
			WEAPON_TYPE weapCurrent
			IF GET_CURRENT_PED_WEAPON(LocalPlayerPed, weapCurrent)
				IF (weapCurrent = WEAPONTYPE_UNARMED)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
				ENDIF
			ENDIF
			IF NOT IS_BIT_SET(iLocalBoolCheck33, LBOOL33_MELEE_PCF_IS_RULE_BLOCKED)
				PRINTLN("[JS] - PROCESS_RULE_WEAPON_DISABLING - Setting PCF_DisableMelee")
				SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_DisableMelee, TRUE)			
				SET_BIT(iLocalBoolCheck33, LBOOL33_MELEE_PCF_IS_RULE_BLOCKED)
			ENDIF
		ELSE
			IF IS_BIT_SET(iLocalBoolCheck33, LBOOL33_MELEE_PCF_IS_RULE_BLOCKED)
				PRINTLN("[JS] - PROCESS_RULE_WEAPON_DISABLING - Clearing PCF_DisableMelee")
				SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_DisableMelee, FALSE)
				CLEAR_BIT(iLocalBoolCheck33, LBOOL33_MELEE_PCF_IS_RULE_BLOCKED)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitsetFour[ iRule ], ciBS_RULE4_REMOVE_WEAPONS )
		OR (IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitsetFour[ iRule ], ciBS_RULE4_START_UNARMED ) AND IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam))
		OR IS_PARTICIPANT_A_BEAST(iLocalPart)
			IF NOT IS_BIT_SET(iLocalBoolCheck33, LBOOL33_KEEP_WEAPON_HOLSTERED_UNTIL_SWITCH_OR_FIRE)	
				PRINTLN("[JS] - PROCESS_RULE_WEAPON_DISABLING - Setting PCF_KeepWeaponHolsteredUnlessFired")
				SET_CURRENT_PED_WEAPON(LocalPlayerPed, WEAPONTYPE_UNARMED, TRUE)
				SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_KeepWeaponHolsteredUnlessFired, TRUE)
				SET_BIT(iLocalBoolCheck33, LBOOL33_KEEP_WEAPON_HOLSTERED_UNTIL_SWITCH_OR_FIRE)
			ENDIF
			
			WEAPON_TYPE currentWeaponType
			IF GET_CURRENT_PED_WEAPON(LocalPlayerPed, currentWeaponType)
				IF (currentWeaponType != WEAPONTYPE_UNARMED)
					SET_CURRENT_PED_WEAPON(LocalPlayerPed, WEAPONTYPE_UNARMED, TRUE)
					PRINTLN("[JS] - PROCESS_RULE_WEAPON_DISABLING - Setting current weapon to unarmed")
				ENDIF
			ENDIF
			
			IF GET_CURRENT_PED_VEHICLE_WEAPON(LocalPlayerPed, currentWeaponType)
				IF (currentWeaponType != WEAPONTYPE_UNARMED)
					SET_CURRENT_PED_VEHICLE_WEAPON(LocalPlayerPed, WEAPONTYPE_UNARMED)
					SET_CURRENT_PED_WEAPON(LocalPlayerPed, WEAPONTYPE_UNARMED, TRUE)					
					PRINTLN("[JS] - PROCESS_RULE_WEAPON_DISABLING - Setting current vehicle weapon to unarmed")
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFour[iRule], ciBS_RULE4_REMOVE_WEAPONS)
			OR IS_PARTICIPANT_A_BEAST(iLocalPart)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_NEXT_WEAPON)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_PREV_WEAPON)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM)
			ENDIF
		ENDIF
	
		IF NOT IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitsetFour[ iRule ], ciBS_RULE4_REMOVE_WEAPONS )
		AND NOT IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitsetFour[ iRule ], ciBS_RULE4_START_UNARMED )
		AND NOT IS_PARTICIPANT_A_BEAST(iLocalPart)
		AND IS_BIT_SET(iLocalBoolCheck33, LBOOL33_KEEP_WEAPON_HOLSTERED_UNTIL_SWITCH_OR_FIRE)
			SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_KeepWeaponHolsteredUnlessFired, FALSE)
			CLEAR_BIT(iLocalBoolCheck33, LBOOL33_KEEP_WEAPON_HOLSTERED_UNTIL_SWITCH_OR_FIRE)
			PRINTLN("[JS] - PROCESS_RULE_WEAPON_DISABLING - Resetting: PCF_KeepWeaponHolsteredUnlessFired.")
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_PLAYER_NEAR_DROP_OFF()
	
	INT iTeam = MC_playerBD[iPartToUse].iTeam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	IF iRule >= FMMC_MAX_RULES
		RETURN FALSE
	ENDIF
	
	//Extra added on to make it "near"
	FLOAT fRadius = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].fDropOffRadius[iRule] + 10.0 
	FLOAT fCoronaHeight = GET_DROP_OFF_HEIGHT(iTeam, iRule) + 2.0
	
	// Cylinder and sphere drop off
	IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDropOffType[iRule] = ciFMMC_DROP_OFF_TYPE_CYLINDER
	OR g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDropOffType[iRule] = ciFMMC_DROP_OFF_TYPE_SPHERE
		
		VECTOR vDropOff = GET_DROP_OFF_CENTER()
		IF GET_DIST2_BETWEEN_ENTITY_AND_COORD(LocalPlayerPed, vDropOff, FALSE) <= POW(fRadius, 2)
		AND IS_ENTITY_AT_COORD(LocalPlayerPed, vDropOff, <<fRadius, fRadius, fCoronaHeight>>, FALSE)
			RETURN TRUE
		ENDIF
		
		RETURN FALSE
		
	ENDIF
	
	// Zone Drop off
	IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDropOffType[iRule] = ciFMMC_DROP_OFF_TYPE_PLACED_ZONE
		INT iZone = iCurrentDropOffZone[iTeam]
		
		IF iZone > -1
			VECTOR vDropOff = GET_DROP_OFF_CENTER()
			
			IF GET_DIST2_BETWEEN_ENTITY_AND_COORD(LocalPlayerPed, vDropOff, FALSE) <= POW(fRadius, 2)
			AND IS_ENTITY_AT_COORD(LocalPlayerPed, vDropOff, <<GET_ZONE_RUNTIME_RADIUS(iZone), GET_ZONE_RUNTIME_RADIUS(iZone), fCoronaHeight>>, FALSE)
				RETURN TRUE
			ENDIF
		ENDIF
		
		RETURN FALSE
		
	ENDIF
	
	// Area drop off
	IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDropOffType[iRule] = ciFMMC_DROP_OFF_TYPE_AREA
		
		IF IS_ENTITY_IN_ANGLED_AREA(LocalPlayerPed, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vDropOff[iRule],
					g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vDropOff2[iRule], g_FMMC_STRUCT.sFMMCEndConditions[iTeam].fDropOffRadius[iRule], FALSE)
			RETURN TRUE
		ENDIF
		
		RETURN FALSE
		
	ENDIF
	
	//Vehicle drop off
	IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDropOff_Vehicle[iRule] > -1
		
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iVehDropOff_HookUpBS, iRule)
			RETURN FALSE
		ENDIF
		
		INT iVeh = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDropOff_Vehicle[iRule]
		
		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
			RETURN FALSE
		ENDIF
		
		VEHICLE_INDEX tempVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
		
		IF NOT IS_VEHICLE_DRIVEABLE(tempVeh)
			RETURN FALSE
		ENDIF
			
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iVehDropOff_GetInBS, iRule)
			fRadius = 15
		ENDIF
						
		IF GET_DIST2_BETWEEN_ENTITIES(LocalPlayerPed, tempVeh) <= POW(fRadius, 2)
		AND IS_ENTITY_AT_COORD(LocalPlayerPed, GET_ENTITY_COORDS(tempVeh), <<fRadius, fRadius, fCoronaHeight>>, FALSE)	
			RETURN TRUE
		ENDIF
		
		RETURN FALSE
		
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

///PURPOSE: This function runs every frame and checks which controls ought be disabled
///    given some situations
PROC PROCESS_DISABLED_CONTROLS()
	
	INT iTeam = MC_playerBD[ iPartToUse ].iteam
	INT iRule = MC_ServerBD_4.iCurrentHighestPriority[ iTeam ]
	
	IF iSpectatorTarget = -1
		IF iRule < FMMC_MAX_RULES
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitset[ iRule ],ciBS_RULE_DISABLE_CONTROL)
				
				PRINTLN("PROCESS_DISABLED_CONTROLS - ciBS_RULE_DISABLE_CONTROL is enabled on this rule.")
				
				BOOL bDisable
				
				IF (g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iDisableControlTimer[ iRule ] = 0)
					//No timer, just remove control for the whole rule:
					bDisable = TRUE
				ELSE
					IF NOT IS_BIT_SET(iLocalBoolCheck6, LBOOL6_DISABLECONTROLTIMER_COMPLETE)
						IF NOT HAS_NET_TIMER_STARTED(tdDisableControlTimer)
							REINIT_NET_TIMER(tdDisableControlTimer)
							bDisable = TRUE
						ELSE
							IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdDisableControlTimer) < ( g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iDisableControlTimer[ iRule ] * 1000)
								bDisable = TRUE
							ELSE
								SET_BIT(iLocalBoolCheck6, LBOOL6_DISABLECONTROLTIMER_COMPLETE)
								RESET_NET_TIMER(tdDisableControlTimer)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF bDisable
					DISABLE_PLAYER_CONTROLS_THIS_FRAME()
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitsetFour[ iRule ], ciBS_RULE4_DISABLE_VEHICLE_MOVEMENT_CONTROLS )
				PRINTLN("PROCESS_DISABLED_CONTROLS - ciBS_RULE4_DISABLE_VEHICLE_MOVEMENT_CONTROLS is enabled on this rule.")
				DISABLE_VEHICLE_MOVEMENT_CONTROLS_THIS_FRAME()
			ENDIF			
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitsetFour[ iRule ],ciBS_RULE4_DISABLE_HANDBRAKE_CONTROL)
				PRINTLN("PROCESS_DISABLED_CONTROLS - ciBS_RULE4_DISABLE_HANDBRAKE_CONTROL is enabled on this rule.")
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HANDBRAKE)
			ENDIF
			
			BOOL bDisableVehExit = FALSE
			
			// If we're going to a vehicle and the next rule disables you getting out,
			// disable you getting out once you actually get into the vehicle
			IF (iRule + 1) < FMMC_MAX_RULES
			AND IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitset[ iRule + 1 ], ciBS_RULE_CANNOT_EXIT_VEHICLE )
				PRINTLN("PROCESS_DISABLED_CONTROLS - ciBS_RULE_CANNOT_EXIT_VEHICLE is enabled on this rule.")
				
				IF GET_MC_CLIENT_MISSION_STAGE( iPartToUse ) = CLIENT_MISSION_STAGE_GOTO_VEH
					
					INT iVehicleNear = MC_playerBD[iPartToUse].iVehNear
					
					IF iVehicleNear > -1
					AND IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitset[ iRule + 1 ], ciBS_RULE_CANNOT_EXIT_VEHICLE )
						IF NETWORK_DOES_NETWORK_ID_EXIST( MC_serverBD_1.sFMMC_SBD.niVehicle[ iVehicleNear ] )
							VEHICLE_INDEX vehToGoTo = NET_TO_VEH( MC_serverBD_1.sFMMC_SBD.niVehicle[ iVehicleNear ] )
							
							IF IS_VEHICLE_DRIVEABLE( vehToGoTo )
								IF IS_PED_IN_VEHICLE( LocalPlayerPed, vehToGoTo )
									bDisableVehExit = TRUE
									PRINTLN("[RCC MISSION] PROCESS_DISABLED_CONTROLS - Disabling control as on goto and in vehicle, next rule is cannot exit")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
				ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDropOffType[iRule] = ciFMMC_DROP_OFF_TYPE_VEHICLE
				AND g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDropOff_Vehicle[iRule] != -1
				AND IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iTeam], iRule) // The AI is in our possession
					
					NETWORK_INDEX niVeh = MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDropOff_Vehicle[iRule]]
					
					IF NETWORK_DOES_NETWORK_ID_EXIST(niVeh)
						VEHICLE_INDEX vehToGoTo = NET_TO_VEH(niVeh)
						
						IF IS_VEHICLE_DRIVEABLE(vehToGoTo)
						AND IS_PED_IN_VEHICLE(LocalPlayerPed, vehToGoTo)
							bDisableVehExit = TRUE
							PRINTLN("[RCC MISSION] PROCESS_DISABLED_CONTROLS - Disabling control as vehicle dropoff and in vehicle, next rule is cannot exit")
						ENDIF
					ENDIF
					
				ENDIF
			ENDIF
			
			IF IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitset[ iRule ], ciBS_RULE_CANNOT_EXIT_VEHICLE )
				PRINTLN("PROCESS_DISABLED_CONTROLS - ciBS_RULE_CANNOT_EXIT_VEHICLE is enabled on this rule.")
				
				BOOL bExitCheck = FALSE
				
				IF IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitset[ iRule ], ciBS_RULE_CANNOT_EXIT_VEHICLE_MIDPOINT ) 
					IF IS_BIT_SET( MC_serverBD.iObjectiveMidPointBitset[ iTeam ], iRule )
						PRINTLN("bExitCheck = true" )
						bExitCheck = TRUE
					ENDIF
				ELSE
					PRINTLN("bExitCheck = true" )
					bExitCheck = TRUE
				ENDIF
				
				IF bExitCheck
					//Should we only be locked into specific vehicles?
					IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iLockedInVehBS[iRule] != 0
						IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
							VEHICLE_INDEX tempVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
							INT iveh = IS_ENTITY_A_MISSION_CREATOR_ENTITY(tempVeh)
							
							IF iveh > -1
							AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iLockedInVehBS[iRule], iveh)
								PRINTLN("bDisableVehExit = true, we're in a vehicle that should lock players in - iveh = " , iveh )
								bDisableVehExit = TRUE
							ENDIF
						ENDIF
					ELSE // Lock into all vehicles!
						PRINTLN("bDisableVehExit = true, all vehicles should lock players in" )
						bDisableVehExit = TRUE
					ENDIF
				ENDIF
				
			ENDIF
			
			IF IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitsetThree[ iRule ], ciBS_RULE3_CANT_GET_OUT_NEAR_DELIVERY )
			AND MC_playerBD[iPartToUse].iVehNear != -1
			AND IS_PLAYER_NEAR_DROP_OFF()
				PRINTLN("PROCESS_DISABLED_CONTROLS - ciBS_RULE3_CANT_GET_OUT_NEAR_DELIVERY is enabled on this rule.")
				PRINTLN("[RCC MISSION] bDisableVehExit = TRUE as Player near drop off")
				bDisableVehExit = TRUE
			ENDIF
			
			VEHICLE_INDEX vehTempPlayer
			IF IS_CURRENT_VEHICLE_RULE_OF_TYPE_CARGOBOB_FOR_TEAM(iTeam, iRule)
				IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
					vehTempPlayer = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
					INT iTempDeliveryVeh = g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iDropOff_Vehicle[ iRule ]
					IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iTempDeliveryVeh])
						VEHICLE_INDEX vehTempCargo =  NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iTempDeliveryVeh])
						IF IS_VEHICLE_DRIVEABLE(vehTempCargo)
							IF IS_ENTITY_ATTACHED_TO_ENTITY(vehTempPlayer,vehTempCargo)
								PRINTLN("[RCC MISSION] bDisableVehExit = TRUE as Player attached to cargobob (1)")
								IF NOT IS_BIT_SET(iLocalBoolCheck11, LBOOL11_WAS_ATTACHED_TO_CARGOBOB)
									SET_BIT(iLocalBoolCheck11, LBOOL11_WAS_ATTACHED_TO_CARGOBOB)
									PRINTLN("[RCC MISSION] bDisableVehExit = TRUE as Player attached to cargobob - Set LBOOL11_WAS_ATTACHED_TO_CARGOBOB")
								ENDIF
								bDisableVehExit = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF IS_BIT_SET(iLocalBoolCheck11, LBOOL11_WAS_ATTACHED_TO_CARGOBOB)
					IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed) 
						vehTempPlayer = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
						IF IS_ENTITY_ATTACHED(vehTempPlayer)
							bDisableVehExit = TRUE
							PRINTLN("[RCC MISSION] bDisableVehExit = TRUE as Player attached to cargobob (2)")
						ELSE
							PRINTLN("[RCC MISSION] LBOOL11_WAS_ATTACHED_TO_CARGOBOB set but player veh not attached!")
						ENDIF
					ELSE
						PRINTLN("[RCC MISSION] LBOOL11_WAS_ATTACHED_TO_CARGOBOB set but player not in a vehicle!")
					ENDIF
				ENDIF
			ENDIF
					
			IF bDisableVehExit
				IF NOT IS_PLAYER_BLOCKED_FROM_EXITING_VEHICLE_IN_MISSION()
					SET_PLAYER_BLOCKED_FROM_EXITING_VEHICLE_IN_MISSION_FLAG(TRUE)
					SET_PED_CONFIG_FLAG(LocalPlayerPed,PCF_PlayersDontDragMeOutOfCar, TRUE)
					PRINTLN("[RCC MISSION] setting PCF_PlayersDontDragMeOutOfCar")
				ENDIF
				DISABLE_VEHICLE_EXIT_THIS_FRAME()
				
				IF g_bVSMission
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciFORCE_START_VEHICLE)
						DISABLE_VEHICLE_ENTRY_FOR_ALL_BUT_THIS_VEHICLE()
					ENDIF
				ENDIF
			ELSE
				IF IS_PLAYER_BLOCKED_FROM_EXITING_VEHICLE_IN_MISSION()
					SET_PLAYER_BLOCKED_FROM_EXITING_VEHICLE_IN_MISSION_FLAG(FALSE)
					SET_PED_CONFIG_FLAG(LocalPlayerPed,PCF_PlayersDontDragMeOutOfCar, FALSE)
					PRINTLN("[RCC MISSION] clearing PCF_PlayersDontDragMeOutOfCar")
				ENDIF
			ENDIF
	
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitset[ iRule ],ciBS_RULE_CANNOT_JUMP)
				PRINTLN("PROCESS_DISABLED_CONTROLS - ciBS_RULE_CANNOT_JUMP is enabled on this rule.")
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_JUMP)
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSeven[iRule], ciBS_RULE7_DISABLE_JUMPING_ONLY)
				PRINTLN("PROCESS_DISABLED_CONTROLS - ciBS_RULE7_DISABLE_JUMPING_ONLY is enabled on this rule.")
				SET_PED_RESET_FLAG(LocalPlayerPed, PRF_DisablePlayerJumping, TRUE)
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEleven[iRule], ciBS_RULE11_DISABLE_VAULTING_ONLY)
				PRINTLN("PROCESS_DISABLED_CONTROLS - ciBS_RULE11_DISABLE_VAULTING_ONLY is enabled on this rule.")
				SET_PED_RESET_FLAG(LocalPlayerPed, PRF_DisablePlayerVaulting, TRUE)
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEleven[iRule], ciBS_RULE11_DISABLE_COMBAT_ROLL_ONLY)
				PRINTLN("PROCESS_DISABLED_CONTROLS - ciBS_RULE11_DISABLE_COMBAT_ROLL_ONLY is enabled on this rule.")
				SET_PED_RESET_FLAG(LocalPlayerPed, PRF_DisablePlayerCombatRoll, TRUE)
			ENDIF
			
			IF (g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iDisabledWeapon[ iRule ] != 0)
				PRINTLN("PROCESS_DISABLED_CONTROLS - iDisabledWeapon is enabled on this rule.")
				WEAPON_TYPE wtDisabled = INT_TO_ENUM(WEAPON_TYPE, g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iDisabledWeapon[ iRule ])
				DISABLE_WEAPON(wtDisabled)
			ENDIF
			
				// CMcM - Fix for 2076901
			IF bLocalPlayerPedOk
				IF IS_PED_ON_ANY_BIKE(LocalPlayerPed)
					IF IS_ENTITY_IN_WATER(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
						IF IS_PLAYER_BLOCKED_FROM_EXITING_VEHICLE_IN_MISSION()
							ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
							SET_PLAYER_BLOCKED_FROM_EXITING_VEHICLE_IN_MISSION_FLAG(FALSE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwelve[iRule], ciBS_RULE12_DISABLE_VEHICLE_ENTRY)
				PRINTLN("PROCESS_DISABLED_CONTROLS - ciBS_RULE12_DISABLE_VEHICLE_ENTRY is enabled on this rule.")
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
			ENDIF
			
		ENDIF
		
		IF IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iTeamBitset, ciBS_FORCE_RESPAWN_WHEN_NOT_IN_RESPAWN_VEHICLE )
		AND (g_FMMC_STRUCT.mnVehicleModel[ iTeam ] != DUMMY_MODEL_FOR_SCRIPT)
		AND NOT IS_FAKE_MULTIPLAYER_MODE_SET()		
			DISABLE_VEHICLE_EXIT_THIS_FRAME()
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitset3, ciBS3_TEAM_PLAYER_PREVENT_FALLS_OUT_WHEN_KILLED)			
			SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_FallsOutOfVehicleWhenKilled, FALSE)
			SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_GetOutUndriveableVehicle, FALSE)
			SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_GetOutBurningVehicle, FALSE)
			SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_RunFromFiresAndExplosions, FALSE)
			SET_PED_RESET_FLAG(LocalPlayerPed, PRF_DisableAutoForceOutWhenBlowingUpCar, TRUE)
		ENDIF
	ENDIF
	
	SET_USER_RADIO_CONTROL_ENABLED(TRUE)
	
	//Disable the radio when a countdown to the mission end is playing:
	IF iRule < FMMC_MAX_RULES
		IF IS_BIT_SET( g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciUSE_COUNTDOWN )
			IF HAS_NET_TIMER_STARTED( MC_serverBD_3.tdObjectiveLimitTimer[ MC_playerBD[ iPartToUse ].iteam ] )
				INT ms = GET_OBJECTIVE_TIMER_TIME_REMAINING(iTeam, iRule)
				IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
					IF ms < 30000
						DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_VEH_RADIO_WHEEL ) 
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTen, ciDISABLE_VISOR_ANIMATION)
		IF IS_PED_ON_ANY_BIKE(LocalPlayerPed)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SWITCH_VISOR)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitset3, ciBS3_TEAM_DISABLE_SEAT_SWAPPING)
		IF iSpectatorTarget = -1
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HEADLIGHT)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SHUFFLE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_SPECIAL_TWO)
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HELP_TUR")
			OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HELP_APC_TUR")
				CLEAR_HELP(TRUE)
				PRINTLN("[RCC MISSION][PROCESS_DISABLED_CONTROLS] CLEAR_HELP For ciBS3_TEAM_DISABLE_SEAT_SWAPPING")
			ENDIF		
			PRINTLN("[RCC MISSION][PROCESS_DISABLED_CONTROLS] DISABLE_CONTROL_ACTION For ciBS3_TEAM_DISABLE_SEAT_SWAPPING")
		ENDIF
	ENDIF
	
	IF g_bMissionEnding
	OR iRule >= FMMC_MAX_RULES
		DISABLE_VEHICLE_EXIT_THIS_FRAME()
	ENDIF
ENDPROC

FUNC BOOL CAN_FORCE_PLAYER_ACTION_MODE_FOR_MP(BOOL bForceOn)
	UNUSED_PARAMETER(bForceOn)
	
	// Used to contain heist specific logic, default returned true.
	
	RETURN TRUE
ENDFUNC

PROC PROCESS_EVERY_FRAME_PLAYER_ACTION_MODE( BOOL bForceInstantUpdate )

	IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
		
		IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + MC_playerBD[iPartToUse].iteam)
			CLEAR_BIT(iLocalBoolCheck33, LBOOL33_WEAPON_FORCED_EQUIPPED_THIS_RULE)
		ENDIF
		
		// 2079480 - Added ability to set combat mode on a per rule basis.
		// updated for bugs 2227329 & 2235826
		
		IF IS_PED_IN_COVER(localPlayerPed)
		OR IS_PED_GOING_INTO_COVER(localPlayerPed)
		OR (IS_BIT_SET(iLocalBoolCheck30, LBOOL30_HAS_BEEN_SET_INTO_COVER_START_POSITION)
		AND NOT IS_BIT_SET(iLocalBoolCheck30, LBOOL30_HAS_CORRECTED_COVER_START_CAMERA_HEADING))
			PRINTLN("[RCC MISSION] - SET_PED_USING_ACTION_MODE - In or going in cover, not setting action mode yet... ")
			EXIT
		ENDIF
		
		IF IS_PED_IN_ANY_VEHICLE(localPlayerPed, TRUE)
		OR IS_PED_GETTING_INTO_A_VEHICLE(LocalPlayerPed)
			PRINTLN("[RCC MISSION] - SET_PED_USING_ACTION_MODE - In or getting in a vehicle, not setting action mode yet... ")
			EXIT
		ENDIF
		
		// FORCE ON
		IF NOT IS_CELLPHONE_CAMERA_IN_USE()
			IF IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitsetTwo[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE2_FORCE_COMBAT_MODE)
				
				IF NOT IS_PED_USING_ACTION_MODE( LocalPlayerPed )
				AND NOT GET_PED_STEALTH_MOVEMENT( LocalPlayerPed )
					IF CAN_FORCE_PLAYER_ACTION_MODE_FOR_MP( TRUE )
						SET_PED_USING_ACTION_MODE( LocalPlayerPed, TRUE )
						IF bForceInstantUpdate
							SET_PED_RESET_FLAG( LocalPlayerPed, PRF_SkipOnFootIdleIntro, TRUE )
							FORCE_PED_MOTION_STATE( LocalPlayerPed, MS_ON_FOOT_IDLE )
						ENDIF
						PRINTLN("[RCC MISSION] - SET_PED_USING_ACTION_MODE - Forcing Action mode ON - bForceInstantUpdate = ", bForceInstantUpdate)
						CLEAR_BIT(iLocalBoolCheck10, LBOOL10_PLAYER_ACTION_MODE_RESET)
						SET_BIT(iLocalBoolCheck33, LBOOL33_USING_ACTION_MODE)
					ENDIF
				ENDIF

			// FORCE OFF
			ELIF IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitsetThree[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE3_FORCE_COMBAT_MODE_OFF)
			
				PRINTLN("[RCC MISSION] - SET_PED_RESET_FLAG( PRF_DisableActionMode ) - Forcing Action mode OFF ")
				SET_PED_RESET_FLAG( LocalPlayerPed, PRF_DisableActionMode, TRUE )
			
				IF IS_PED_USING_ACTION_MODE( LocalPlayerPed )
					IF CAN_FORCE_PLAYER_ACTION_MODE_FOR_MP( FALSE )
						SET_PED_USING_ACTION_MODE( LocalPlayerPed, FALSE )
						IF bForceInstantUpdate
							SET_PED_RESET_FLAG( LocalPlayerPed, PRF_SkipOnFootIdleIntro, TRUE )
							FORCE_PED_MOTION_STATE( LocalPlayerPed, MS_ON_FOOT_IDLE )
						ENDIF
						PRINTLN("[RCC MISSION] - SET_PED_USING_ACTION_MODE - Forcing Action mode OFF - bForceInstantUpdate = ", bForceInstantUpdate)
						CLEAR_BIT(iLocalBoolCheck10, LBOOL10_PLAYER_ACTION_MODE_RESET)
						SET_BIT(iLocalBoolCheck33, LBOOL33_USING_ACTION_MODE_FORCE_OFF)
					ENDIF
				ENDIF
				
			// DO NOT FORCE EITHER
			ELSE
				IF NOT IS_BIT_SET( iLocalBoolCheck10, LBOOL10_PLAYER_ACTION_MODE_RESET )
				AND (IS_BIT_SET(iLocalBoolCheck33, LBOOL33_USING_ACTION_MODE) OR IS_BIT_SET(iLocalBoolCheck33, LBOOL33_USING_ACTION_MODE_FORCE_OFF))
					// Reset the action mode with a short duration (to overwrite it previously having been set to -1)
					// by calling SET_PED_USING_ACTION_MODE with the same state it's already in.
					SET_PED_USING_ACTION_MODE( LocalPlayerPed, IS_PED_USING_ACTION_MODE( LocalPlayerPed ), 1000 ) 
					PRINTLN("[RCC MISSION] - SET_PED_USING_ACTION_MODE - Reset Action mode with ", BOOL_TO_STRING(IS_PED_USING_ACTION_MODE( LocalPlayerPed )), " to naturally end")
					
					SET_BIT(iLocalBoolCheck10, LBOOL10_PLAYER_ACTION_MODE_RESET)
					CLEAR_BIT(iLocalBoolCheck33, LBOOL33_USING_ACTION_MODE)
					CLEAR_BIT(iLocalBoolCheck33, LBOOL33_USING_ACTION_MODE_FORCE_OFF)
				ENDIF
			ENDIF
			
			IF IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitsetFourteen[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE14_FORCE_WEAPON_IN_HAND_ON_RULE)									
				IF NOT IS_BIT_SET(iLocalBoolCheck33, LBOOL33_WEAPON_FORCED_EQUIPPED_THIS_RULE)
					WEAPON_TYPE weapType
					
					IF NOT IS_PED_CLIMBING(LocalPlayerPed)
					AND NOT IS_PED_JUMPING(LocalPlayerPed)
					AND NOT IS_PED_SWIMMING(LocalPlayerPed)
					AND NOT IS_PED_FALLING(LocalPlayerPed)	
						weapType = GET_BEST_PED_WEAPON(LocalPlayerPed)
						
						IF IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitsetFifteen[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE15_FORCE_STARTING_WEAPON_IN_HAND_ON_RULE)
							INT iStartingInventory = iInventory_Starting[MC_playerBD[iPartToUse].iteam]
							IF iStartingInventory != -1
							AND g_FMMC_STRUCT.sPlayerWeaponInventories[iStartingInventory].iStartWeapon != FMMC_STARTING_WEAPON_CURRENT
								WEAPON_TYPE wtStartingWeapon = g_FMMC_STRUCT.sPlayerWeaponInventories[iStartingInventory].sWeaponStruct[g_FMMC_STRUCT.sPlayerWeaponInventories[iStartingInventory].iStartWeapon].wtWeapon
								IF DOES_PLAYER_HAVE_WEAPON(wtStartingWeapon)
									weapType = wtStartingWeapon
									PRINTLN("[RCC MISSION] - SET_PED_USING_ACTION_MODE - ciBS_RULE14_FORCE_WEAPON_IN_HAND_ON_RULE - Using starting inventory weapon")
								ENDIF
							ENDIF
						ENDIF
						
						IF weapType != WEAPONTYPE_INVALID
						AND weapType != WEAPONTYPE_UNARMED
							PRINTLN("[RCC MISSION] - SET_PED_USING_ACTION_MODE - ciBS_RULE14_FORCE_WEAPON_IN_HAND_ON_RULE - (1) - Setting Ped Weapon as: ", GET_WEAPON_NAME_FROM_WEAPON_TYPE(weapType), " Int: ", ENUM_TO_INT(weapType))
							SET_CURRENT_PED_WEAPON(LocalPlayerPed, weapType, TRUE)
						ELSE
							PRINTLN("[RCC MISSION] - SET_PED_USING_ACTION_MODE - ciBS_RULE14_FORCE_WEAPON_IN_HAND_ON_RULE - (1) - No weapon to equip. Weapon is: ", GET_WEAPON_NAME_FROM_WEAPON_TYPE(weapType), " Int: ", ENUM_TO_INT(weapType))
						ENDIF
					ENDIF
					
					IF GET_CURRENT_PED_WEAPON(LocalPlayerPed, weapType, FALSE)
						IF weapType != WEAPONTYPE_INVALID
						AND weapType != WEAPONTYPE_UNARMED
							PRINTLN("[RCC MISSION] - SET_PED_USING_ACTION_MODE - ciBS_RULE14_FORCE_WEAPON_IN_HAND_ON_RULE - (2) - Weapon is: ", GET_WEAPON_NAME_FROM_WEAPON_TYPE(weapType), " Int: ", ENUM_TO_INT(weapType), " Success")
							SET_BIT(iLocalBoolCheck33, LBOOL33_WEAPON_FORCED_EQUIPPED_THIS_RULE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF				
		ENDIF
		
	ENDIF

ENDPROC

PROC PROCESS_FORCED_STEALTH()
	INT iTeam = MC_PlayerBD[iLocalPart].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	IF iRule < FMMC_MAX_RULES
		IF bLocalPlayerPedOk
			IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam)
				IF IS_BIT_SET(iLocalBoolCheck25, LBOOL25_PLAYER_SET_FORCED_STEALTH_THIS_RULE)
					PRINTLN("PROCESS_FORCED_STEALTH - Resetting stealth")
					SET_PED_STEALTH_MOVEMENT(LocalPlayerPed, FALSE)
					CLEAR_BIT(iLocalBoolCheck25, LBOOL25_PLAYER_SET_FORCED_STEALTH_THIS_RULE)
				ENDIF
			ENDIF
			
			IF NOT IS_BIT_SET(iLocalBoolCheck25, LBOOL25_PLAYER_SET_FORCED_STEALTH_THIS_RULE) OR g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iForceStealthSetting[iRule] = ciFORCED_STEALTH_WHOLE_RULE
				IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iForceStealthSetting[iRule] != ciFORCED_STEALTH_NO
					SET_PED_STEALTH_MOVEMENT(LocalPlayerPed, TRUE)
					SET_BIT(iLocalBoolCheck25, LBOOL25_PLAYER_SET_FORCED_STEALTH_THIS_RULE)
				ENDIF
				
				IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iForceStealthSetting[iRule] = ciFORCED_STEALTH_WHOLE_RULE
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPRINT)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_PLAYER_DRUNK_ON_RULE(INT& iTeam, INT& iRule)

	IF NOT DID_RULE_CHANGE_DURING_STAGGERED_LOCAL_PLAYER_PROCESSING()
		EXIT
	ENDIF
	
	IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDrunkLevelOnRule[iRule] > 0
		PRINTLN("PROCESS_SET_PLAYER_DRUNK_LEVEL | player should currently be drunk because the drunk level of this rule is ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDrunkLevelOnRule[iRule])
		MAKE_PED_DRUNK(LocalPlayerPed, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDrunkLevelOnRule[iRule] * 1000)

		ACTIVATE_DRUNK_CAMERA_CONSTANT(99999, 1)
		PRINTLN("PROCESS_SET_PLAYER_DRUNK_LEVEL | drunk cam activated")
	ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDrunkLevelOnRule[iRule] = 0 AND IS_CONTENT_AFTER_DLC(ciDLC_DLC_1_2022)
		PRINTLN("PROCESS_SET_PLAYER_DRUNK_LEVEL | player should be constantly drunk because the drunk level of this rule is ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDrunkLevelOnRule[iRule])
		MAKE_PED_DRUNK_CONSTANT(LocalPlayerPed)

		ACTIVATE_DRUNK_CAMERA_CONSTANT(99999, 1)
		PRINTLN("PROCESS_SET_PLAYER_DRUNK_LEVEL | drunk cam activated")
	ELSE
		PRINTLN("PROCESS_SET_PLAYER_DRUNK_LEVEL | Turning off the drunk effects now because drunk level on this rule is 0 or less (RULE: ", iRule, ")")
		MAKE_PED_SOBER(LocalPlayerPed)
		
		//Gradually turns off the drunk camera effect
		QUIT_DRUNK_CAMERA_GRADUALLY()
	ENDIF
ENDPROC

PROC PROCESS_PLAYER_FORCE_LOCK_ON(INT& iTeam, INT& iRule)

	IF IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFive[iRule], ciBS_RULE5_FORCE_OFF_LOCK_ON_AIM )
		IF NOT IS_BIT_SET( iLocalBoolCheck14, LBOOL14_HAS_FORCED_OFF_LOCK_ON )
			SET_BIT( iLocalBoolCheck14, LBOOL14_HAS_FORCED_OFF_LOCK_ON )
			PRINTLN("[JJT] Entering Forced Off Lock On rule.")
			
			INT i
			FOR i = 0 TO (MC_serverBD.iNumberOfTeams - 1)
				IF NETWORK_IS_GAME_IN_PROGRESS()
					PRINTLN("[JJT] SET_PED_CAN_BE_TARGETTED_BY_TEAM(FALSE): ", i)
					SET_PED_CAN_BE_TARGETTED_BY_TEAM(LocalPlayerPed, i, FALSE)
				ENDIF
			ENDFOR
		ENDIF
	ELIF IS_BIT_SET( iLocalBoolCheck14, LBOOL14_HAS_FORCED_OFF_LOCK_ON )
		CLEAR_BIT( iLocalBoolCheck14, LBOOL14_HAS_FORCED_OFF_LOCK_ON )
		PRINTLN("[JJT] Come out of a Force Off Lock On rule, resetting targetting logic.")
		
		INT i
		FOR i = 0 TO (MC_serverBD.iNumberOfTeams - 1)
			IF DOES_TEAM_LIKE_TEAM(iTeam, i)
				IF NETWORK_IS_GAME_IN_PROGRESS()
					PRINTLN("[JJT] Resetting SET_PED_CAN_BE_TARGETTED_BY_TEAM(FALSE): ", i)
					SET_PED_CAN_BE_TARGETTED_BY_TEAM(LocalPlayerPed, i, FALSE)
				ENDIF
			ELSE
				IF NETWORK_IS_GAME_IN_PROGRESS()
					PRINTLN("[JJT] Resetting SET_PED_CAN_BE_TARGETTED_BY_TEAM(TRUE): ", i)
					SET_PED_CAN_BE_TARGETTED_BY_TEAM(LocalPlayerPed, i, TRUE)
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
ENDPROC

PROC PROCESS_MINIGAME_DAMAGE_PROTECTION()
	IF HAS_NET_TIMER_STARTED(tdMinigameDamageReductionTimer)
	AND HAS_NET_TIMER_EXPIRED(tdMinigameDamageReductionTimer, ciMINIGAME_DAMAGE_REDUCTION_TIME)
		PRINTLN("[LocalPlayer] PROCESS_MINIGAME_DAMAGE_PROTECTION - End reduced damage!")
		SET_PLAYER_WEAPON_DEFENSE_MODIFIER(LocalPlayer, FREEMODE_PLAYER_WEAPON_DEFENSE)
		RESET_NET_TIMER(tdMinigameDamageReductionTimer)
	ENDIF
ENDPROC

PROC PROCESS_POST_CUTSCENE_COVER()

	INT iTeam = MC_PlayerBD[iLocalPart].iTeam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	IF iRule >= FMMC_MAX_RULES
		EXIT
	ENDIF
		
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTen[iRule], ciBS_RULE10_POST_CUTSCENE_COVER)
		EXIT
	ENDIF	
	
	IF NOT MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(iCoverAfterCutsceneTimeStamp)
	OR (MC_HAS_NET_TIMER_STARTED_WITH_TIMESTAMP(iCoverAfterCutsceneTimeStamp) AND MC_HAS_NET_TIMER_EXPIRED_WITH_TIME_STAMP(iCoverAfterCutsceneTimeStamp, 1000))
		
		IF NOT IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam) //If we've just entered a new rule..
			EXIT
		ENDIF				
		
		MC_REINIT_NET_TIMER_WITH_TIMESTAMP(iCoverAfterCutsceneTimeStamp)
		
		PRINTLN("PROCESS_POST_CUTSCENE_COVER - Starting timer for cover on Rule ", iRule)
	ENDIF
	
	PRINTLN("PROCESS_POST_CUTSCENE_COVER - Checking if we should be put into cover on Rule ", iRule)
	
	IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		EXIT
	ENDIF
		
	IF NOT IS_PED_INJURED(LocalPlayerPed)
	AND NOT IS_PED_IN_COVER(LocalPlayerPed, TRUE)	
	AND NOT IS_PED_GOING_INTO_COVER(LocalPlayerPed)
		CLEAR_PED_TASKS_IMMEDIATELY(LocalPlayerPed)
		VECTOR vCoverPos = GET_ENTITY_COORDS(LocalPlayerPed)
		COVERPOINT_INDEX cpIndex
		cpIndex = GET_CLOSEST_COVER_POINT_TO_LOCATION(GET_ENTITY_COORDS(LocalPlayerPed), vCoverPos)	
		SET_PED_TO_LOAD_COVER(LocalPlayerPed, TRUE)
		IF cpIndex != NULL
			TASK_WARP_PED_DIRECTLY_INTO_COVER(localPlayerPed, -1, TRUE, DEFAULT, DEFAULT, cpIndex)
			PRINTLN("PROCESS_POST_CUTSCENE_COVER - using TASK_WARP_PED_DIRECTLY_INTO_COVER, with cpIndex at vCoverPos: ", vCoverPos)
		ELSE
			IF IS_VECTOR_ZERO(vCoverPos)
				vCoverPos = GET_ENTITY_COORDS(LocalPlayerPed)
			ENDIF
			TASK_PUT_PED_DIRECTLY_INTO_COVER(LocalPlayerPed, vCoverPos, -1, TRUE, DEFAULT, DEFAULT, DEFAULT, cpIndex, TRUE)	
			PRINTLN("PROCESS_POST_CUTSCENE_COVER - SET_PLAYER_INTO_COVER_AFTER_CUTSCENE - using TASK_PUT_PED_DIRECTLY_INTO_COVER, with vCoverPos: ", vCoverPos)		
		ENDIF
		FORCE_PED_AI_AND_ANIMATION_UPDATE(LocalPlayerPed, TRUE)
		PRINTLN("PROCESS_POST_CUTSCENE_COVER - Putting player into nearest available cover while this rule as starting because it is set to do so on Rule ", iRule)
	ELSE
		PRINTLN("PROCESS_POST_CUTSCENE_COVER - Not putting ped in cover as they are deemed already in cover by IS_PED_IN_COVER. Rule ", iRule)
	ENDIF
ENDPROC

PROC PROCESS_SET_PLAYER_MOVESPEED_OVERRIDE_FOR_THIS_RULE()
	
	INT iTeam = MC_PlayerBD[iLocalPart].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	IF iRule < FMMC_MAX_RULES
		IF bLocalPlayerPedOk
			IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMoveOverrideSpeed[iRule] > -1
				PRINTLN("[MOVE SPEED OVERRIDE] PROCESS_SET_PLAYER_MOVESPEED_OVERRIDE_FOR_THIS_RULE ", TO_FLOAT(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMoveOverrideSpeed[iRule]/ 10))
				RESET_PLAYER_STAMINA(LocalPlayer)
				SET_PED_MAX_MOVE_BLEND_RATIO(LocalPlayerPed, TO_FLOAT(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMoveOverrideSpeed[iRule])/ 10.0)
			ENDIF
		ENDIF
	ENDIF
		
ENDPROC

PROC SET_MINIGUN_PLAYER_DAMAGE(FLOAT fDamage = 1.0)
	PRINTLN("[JS] SET_MINIGUN_PLAYER_DAMAGE - Set to ", fDamage)
	SET_PLAYER_WEAPON_MINIGUN_DEFENSE_MODIFIER(LocalPlayer, fDamage)
ENDPROC

PROC PROCESS_MELEE_DAMAGE_OVERRIDE_FOR_THIS_RULE()
	INT iTeam
	
	FOR iTeam = 0 TO (FMMC_MAX_TEAMS - 1)
		INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
		
		IF iRule < FMMC_MAX_RULES
			IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam)
				FLOAT fMeleeDamageModifier = TO_FLOAT(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMeleeDamageModifier[iRule] / 10)
				
				IF fMeleeDamageModifier <> 1.0
					INT iDamageModLoop
					
					FOR iDamageModLoop = 0 TO NETWORK_GET_MAX_NUM_PARTICIPANTS() -1
						PARTICIPANT_INDEX tempPart = INT_TO_PARTICIPANTINDEX(iDamageModLoop)
						
						IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
							PLAYER_INDEX tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
							
							IF MC_PlayerBD[iDamageModLoop].iTeam = iTeam
								IF fMeleeDamageModifier <= 0.1
									PRINTLN("[MELEE_DAMAGE_MODIFIER] fMeleeDamageModifier = ", fMeleeDamageModifier)
									SET_PLAYER_WEAPON_TAKEDOWN_DEFENSE_MODIFIER(tempPlayer, 0.11) //Minimum Value
								ELSE
									PRINTLN("[MELEE_DAMAGE_MODIFIER] fMeleeDamageModifier = ", fMeleeDamageModifier)
									SET_PLAYER_WEAPON_TAKEDOWN_DEFENSE_MODIFIER(tempPlayer, fMeleeDamageModifier)
								ENDIF							
							ENDIF
						ENDIF
					ENDFOR	
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

PROC PROCESS_MELEE_WEAPON_DAMAGE_OVERRIDE_FOR_THIS_RULE(INT iTeam, INT iRule)
	IF iRule >= FMMC_MAX_RULES
		EXIT
	ENDIF

	IF !bLocalPlayerPedOK
		SET_BIT(iLocalBoolCheck18, LBOOL18_MELEE_DAMAGE_MODIFIER_RESPAWN)
	ELSE
		IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam) OR IS_BIT_SET(iLocalBoolCheck18, LBOOL18_MELEE_DAMAGE_MODIFIER_RESPAWN)
			FLOAT fMeleeDamageModifier = TO_FLOAT(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMeleeWeaponDamageModifier[iRule] / 10)
					
			IF fMeleeDamageModifier <= 0.1
				PRINTLN("[PROCESS_MELEE_WEAPON_DAMAGE_OVERRIDE_FOR_THIS_RULE] Apply fMeleeDamageModifier = ", fMeleeDamageModifier)
				SET_PLAYER_MELEE_WEAPON_DAMAGE_MODIFIER(LocalPlayer, 0.11, TRUE) //Minimum Value
				CLEAR_BIT(iLocalBoolCheck18, LBOOL18_MELEE_DAMAGE_MODIFIER_RESPAWN)
			ELSE
				PRINTLN("[PROCESS_MELEE_WEAPON_DAMAGE_OVERRIDE_FOR_THIS_RULE] Apply fMeleeDamageModifier = ", fMeleeDamageModifier)
				SET_PLAYER_MELEE_WEAPON_DAMAGE_MODIFIER(LocalPlayer, fMeleeDamageModifier, TRUE)
				CLEAR_BIT(iLocalBoolCheck18, LBOOL18_MELEE_DAMAGE_MODIFIER_RESPAWN)
			ENDIF
		ENDIF
	ENDIF
ENDPROC


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: HUD and Blips Processing  -----------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: This section covers the processing of Blips associated with the local player.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

// This function will make the player respawn back at the start of the mission using their restart pos.
PROC CLEANUP_PLAYER_FOR_WINNER_LOSER_SHARD_APPEARANCE(BOOL bKillPlayer = FALSE, BOOL inNotVehicle = FALSE)
	
	PRINTLN("[LM][CLEANUP_PLAYER_FOR_WINNER_LOSER_SHARD_APPEARANCE] - Settting the player in a state so that they don't look ridiculous on the WINNER/LOSER ")
	
	IF bLocalPlayerPedOK
		CLEAR_PED_TASKS_IMMEDIATELY(localPlayerPed)
		SET_PED_TO_RAGDOLL(LocalPlayerPed, 3500, 5000, TASK_RELAX, FALSE, FALSE)
	ENDIF
	
	INT iPlayerTeam = GET_PLAYER_TEAM_FOR_INTRO()
	INT iTeamSlot = GET_PARTICIPANT_NUMBER_IN_TEAM()
	VECTOR vStartPos = g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iPlayerTeam][iTeamSlot].vPos + <<0,0,1>>
	FLOAT fHeading = g_FMMC_STRUCT.fTeamRestartHeading[iPlayerTeam][0][iTeamSlot]
	
	SETUP_SPECIFIC_SPAWN_LOCATION(vStartPos, fHeading, 5.0, FALSE, 0, DEFAULT, DEFAULT, 1)
	SET_PLAYER_NEXT_RESPAWN_LOCATION(SPAWN_LOCATION_AT_SPECIFIC_COORDS, TRUE) 
	
	IF inNotVehicle
		SET_PLAYER_RESPAWN_IN_VEHICLE(FALSE)
		SET_BIT(iLocalBoolCheck25, LBOOL25_CLEANUP_RESPAWN_SPAWN_AT_START_NO_VEH)
		PRINTLN("[LM][CLEANUP_PLAYER_FOR_WINNER_LOSER_SHARD_APPEARANCE] - Making sure they don't spawn in a vehicle.")
	ENDIF
	
	IF bKillPlayer
		IF bLocalPlayerPedOK
			SET_ENTITY_INVINCIBLE(localPlayerPed, FALSE)
			SET_ENTITY_CAN_BE_DAMAGED(localPlayerPed, TRUE)
			SET_ENTITY_HEALTH(localPlayerPed, 0)
		ENDIF
		PRINTLN("[LM][CLEANUP_PLAYER_FOR_WINNER_LOSER_SHARD_APPEARANCE] - Killing the player so that they respawn at the start, and that the shard wait for us.")
	ENDIF
		
ENDPROC

PROC PROCESS_MY_PLAYER_BLIP_VISIBILITY(INT iTeam, INT iRule)
	
	IF iSpectatorTarget = -1
		
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeam1BlipVisibility, iRule)
		AND NOT IS_BIT_SET(iLocalBoolCheck14, LBOOL14_BLIP_ON_LOCATE)
		AND NOT IS_BIT_SET(iLocalBoolCheck16, LBOOL16_BLIP_ON_COLLECT)
		AND NOT IS_BIT_SET(iBlippingPlayerBS, iLocalPart)
		AND NOT (IS_BIT_SET( MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH )
			AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciBLIP_PLAYERS_AT_SUDDEN_DEATH))
		AND NOT IS_BIT_SET(iLocalBoolCheck23, LBOOL23_BLIP_FORCED_ON_BY_ZONE)
			PRINTLN("[RCC MISSION] PROCESS_MY_PLAYER_BLIP_VISIBILITY - Hiding my blip (team ",iTeam,") from team 0")
			HIDE_MY_PLAYER_BLIP_FROM_TEAM(TRUE,0)
		ELSE
			PRINTLN("[RCC MISSION] PROCESS_MY_PLAYER_BLIP_VISIBILITY - Showing my blip (team ",iTeam,") to team 0")
			HIDE_MY_PLAYER_BLIP_FROM_TEAM(FALSE,0)
		ENDIF
		
		IF MC_serverBD.iNumberOfTeams >= 2
					
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeam2BlipVisibility, iRule)
			AND NOT IS_BIT_SET(iLocalBoolCheck14, LBOOL14_BLIP_ON_LOCATE)
			AND NOT IS_BIT_SET(iLocalBoolCheck16, LBOOL16_BLIP_ON_COLLECT)
			AND NOT IS_BIT_SET(iBlippingPlayerBS, iLocalPart) 
			AND NOT (IS_BIT_SET( MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH )
				AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciBLIP_PLAYERS_AT_SUDDEN_DEATH))
			AND NOT IS_BIT_SET(iLocalBoolCheck23, LBOOL23_BLIP_FORCED_ON_BY_ZONE)
				PRINTLN("[RCC MISSION] PROCESS_MY_PLAYER_BLIP_VISIBILITY - Hiding my blip (team ",iTeam,") from team 1")
				HIDE_MY_PLAYER_BLIP_FROM_TEAM(TRUE,1)
			ELSE
				PRINTLN("[RCC MISSION] PROCESS_MY_PLAYER_BLIP_VISIBILITY - Showing my blip (team ",iTeam,") to team 1")
				HIDE_MY_PLAYER_BLIP_FROM_TEAM(FALSE,1)
			ENDIF
			
			IF MC_serverBD.iNumberOfTeams >= 3
				
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeam3BlipVisibility, iRule)
				AND NOT IS_BIT_SET(iLocalBoolCheck14, LBOOL14_BLIP_ON_LOCATE)
				AND NOT IS_BIT_SET(iLocalBoolCheck16, LBOOL16_BLIP_ON_COLLECT)
				AND NOT IS_BIT_SET(iBlippingPlayerBS, iLocalPart) 
				AND NOT (IS_BIT_SET( MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH )
					AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciBLIP_PLAYERS_AT_SUDDEN_DEATH))
				AND NOT IS_BIT_SET(iLocalBoolCheck23, LBOOL23_BLIP_FORCED_ON_BY_ZONE)
					PRINTLN("[RCC MISSION] PROCESS_MY_PLAYER_BLIP_VISIBILITY - Hiding my blip (team ",iTeam,") from team 2")
					HIDE_MY_PLAYER_BLIP_FROM_TEAM(TRUE,2)
				ELSE
					PRINTLN("[RCC MISSION] PROCESS_MY_PLAYER_BLIP_VISIBILITY - Showing my blip (team ",iTeam,") to team 2")
					HIDE_MY_PLAYER_BLIP_FROM_TEAM(FALSE,2)
				ENDIF
				
				IF MC_serverBD.iNumberOfTeams >= 4
					
					IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeam4BlipVisibility, iRule)
					AND NOT IS_BIT_SET(iLocalBoolCheck14, LBOOL14_BLIP_ON_LOCATE)
					AND NOT IS_BIT_SET(iLocalBoolCheck16, LBOOL16_BLIP_ON_COLLECT)
					AND NOT IS_BIT_SET(iBlippingPlayerBS, iLocalPart) 
					AND NOT (IS_BIT_SET( MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH )
						AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciBLIP_PLAYERS_AT_SUDDEN_DEATH))
					AND NOT IS_BIT_SET(iLocalBoolCheck23, LBOOL23_BLIP_FORCED_ON_BY_ZONE)
						PRINTLN("[RCC MISSION] PROCESS_MY_PLAYER_BLIP_VISIBILITY - Hiding my blip (team ",iTeam,") from team 3")
						HIDE_MY_PLAYER_BLIP_FROM_TEAM(TRUE,3)
					ELSE
						PRINTLN("[RCC MISSION] PROCESS_MY_PLAYER_BLIP_VISIBILITY - Showing my blip (team ",iTeam,") to team 3")
						HIDE_MY_PLAYER_BLIP_FROM_TEAM(FALSE,3)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_PLAYER_CURRENT_SPEED()
	IF iSpectatorTarget = -1
		INT iScratchTeam = MC_playerBD[iLocalPart].iteam
		IF iScratchTeam < FMMC_MAX_TEAMS
		AND iScratchTeam != -1
		
			INT iScratchRule = MC_serverBD_4.iCurrentHighestPriority[iScratchTeam]
			
			IF iScratchRule < FMMC_MAX_RULES
			
				INT iThresholdSpeed = g_FMMC_STRUCT.sFMMCEndConditions[iScratchTeam].iBlipOnSpeedThreshold[MC_serverBD_4.iCurrentHighestPriority[iScratchTeam]]
	
				//check that the player is in a car before trying to blip them
				IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
					IF GET_ENTITY_SPEED(LocalPlayerPed) > iThresholdSpeed
						MC_playerBD[iLocalPart].bIsOverSpeedThreshold = TRUE
					ELSE
						MC_playerBD[iLocalPart].bIsOverSpeedThreshold = FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_PLAYER_USING_TRACKIFY_HUD()

	//creator setting on
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciSHOW_PLAYERS_USING_TRACKIFY)
		IF IS_CELLPHONE_TRACKIFY_IN_USE()
			UPDATE_INVENTORY_BOX_CONTENT(ci_Obj_inventory, MP_TAG_TRACKIFY, FALSE,1, DEFAULT, DEFAULT, TRUE)
		ELSE
			CLEAR_A_INVENTORY_BOX_CONTENT(ci_Obj_inventory)
		ENDIF		
	ENDIF
	
ENDPROC

PROC PROCESS_PLAYER_SHOW_BLIP_IF_IDLE()

	IF NOT HAS_NET_TIMER_STARTED( tIdleTimer )
		vLastCachedPosition = GET_ENTITY_COORDS( LocalPlayerPed )
		iIdleBlipExitRetry = 0
		
		SET_MY_PLAYER_BLIP_AS_IDLE(FALSE)
		START_NET_TIMER( tIdleTimer )
	ELSE
		IF iSpectatorTarget = -1
		AND (MC_playerBD[iLocalPart].iBeastAlpha != 0)
			IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME( tIdleTimer ) > ( 500 * iIdleBlipExitRetry )
				
				INT idleTimer = g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iIdleBlipTimer[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]]
				
				//0 = DEFAULT which is 4.5 seconds
				//values are 1,2,3,4,5... but calculated as 0.5,1,1.5,2,2.5...
				idleTimer = PICK_INT( (idleTimer = 0), 4500, idleTimer * 500 )
				
				//Check every 500ms if the player is out of camping range, if so reset and cache a new position
				IF VDIST( GET_ENTITY_COORDS( LocalPlayerPed ), vLastCachedPosition ) > ciIDLE_BLIP_CAMP_RADIUS
					RESET_NET_TIMER( tIdleTimer )
				
				//Else if they're still in the radius, and over the threshold timer, blip them
				ELIF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME( tIdleTimer ) > idleTimer
					SET_MY_PLAYER_BLIP_AS_IDLE(TRUE)
				ENDIF
				
				iIdleBlipExitRetry++
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC KEEP_PLAYER_BLIPS_LONG_RANGE_STAGGERED()
	IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPlayerBlipRangeStaggeredIndex))
		SET_PLAYER_BLIP_AS_LONG_RANGE(INT_TO_PLAYERINDEX(iPlayerBlipRangeStaggeredIndex), TRUE)
	ENDIF
	
	iPlayerBlipRangeStaggeredIndex++
	IF iPlayerBlipRangeStaggeredIndex >= GET_MAXIMUM_PLAYERS_ON_MISSION()
		iPlayerBlipRangeStaggeredIndex = 0
	ENDIF
ENDPROC

PROC PROCESS_PLAYER_BLIPS()
	
	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	IF iRule < FMMC_MAX_RULES
		
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFour[iRule], ciBS_RULE4_SHOW_IDLE_BLIP)
			IF bLocalPlayerPedOK
				IF iSpectatorTarget = -1
					PROCESS_PLAYER_SHOW_BLIP_IF_IDLE()
				ENDIF
			ELSE
				IF IS_MY_PLAYER_BLIP_SET_AS_IDLE()
					SET_MY_PLAYER_BLIP_AS_IDLE(FALSE)
				ENDIF
			ENDIF
		ELSE
			IF IS_MY_PLAYER_BLIP_SET_AS_IDLE()
				SET_MY_PLAYER_BLIP_AS_IDLE(FALSE)
			ENDIF
		ENDIF
		
		KEEP_PLAYER_BLIPS_LONG_RANGE_STAGGERED()
		
		IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam)
		OR IS_BIT_SET(iLocalBoolCheck7, LBOOL7_REFRESH_PLAYER_BLIPS)
			
			IF IS_BIT_SET(iLocalBoolCheck7, LBOOL7_REFRESH_PLAYER_BLIPS)
				PRINTLN("[RCC MISSION] PROCESS_PLAYER_BLIPS - Refreshing player blips due to LBOOL7_REFRESH_PLAYER_BLIPS")
				CLEAR_BIT(iLocalBoolCheck7, LBOOL7_REFRESH_PLAYER_BLIPS)
			ENDIF
			
			//My blip's visibility to other teams:
			PROCESS_MY_PLAYER_BLIP_VISIBILITY(iTeam, iRule)
			
			//All players blip visibility:
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFour[iRule], ciBS_RULE4_BLIP_PLAYERS)
				
				IF NOT IS_BIT_SET(iLocalBoolCheck,LBOOL_SHOW_ALL_PLAYER_BLIPS)
					//If the player blips timer is running, then we're already showing all player blips
					IF NOT HAS_NET_TIMER_STARTED(tdPlayerBlipsTimer)
						SHOW_ALL_PLAYER_BLIPS(TRUE)
						PRINTLN("[RCC MISSION] PROCESS_PLAYER_BLIPS - Show all player blips")
						IF IS_BIT_SET(iLocalBoolCheck16, LBOOL16_FORCE_BLIP_ALL_PLAYERS_TRIGGERED)
							FORCE_BLIP_ALL_PLAYERS(TRUE, TRUE, TRUE)
						ENDIF
					#IF IS_DEBUG_BUILD
					ELSE
						PRINTLN("[RCC MISSION] PROCESS_PLAYER_BLIPS - Show all player blips (already set by the tdPlayerBlipsTimer)")
					#ENDIF
					ENDIF
					
					SET_BIT(iLocalBoolCheck,LBOOL_SHOW_ALL_PLAYER_BLIPS)
				ENDIF
			ELSE
				IF IS_BIT_SET(iLocalBoolCheck,LBOOL_SHOW_ALL_PLAYER_BLIPS)
					IF NOT IS_BIT_SET( MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH )
					AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciBLIP_PLAYERS_AT_SUDDEN_DEATH)
						SHOW_ALL_PLAYER_BLIPS(FALSE)
						IF IS_BIT_SET(iLocalBoolCheck16, LBOOL16_FORCE_BLIP_ALL_PLAYERS_TRIGGERED)
							FORCE_BLIP_ALL_PLAYERS(FALSE, FALSE, TRUE)
						ENDIF
						PRINTLN("[RCC MISSION] PROCESS_PLAYER_BLIPS - Stop showing all player blips")
						
						CLEAR_BIT(iLocalBoolCheck,LBOOL_SHOW_ALL_PLAYER_BLIPS)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_BIT_SET( MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH )
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciBLIP_PLAYERS_AT_SUDDEN_DEATH)
				IF NOT IS_BIT_SET(iLocalBoolCheck,LBOOL_SHOW_ALL_PLAYER_BLIPS)
					SHOW_ALL_PLAYER_BLIPS(TRUE)
					SET_ALL_PLAYER_BLIPS_LONG_RANGE(TRUE)
					PRINTLN("[RCC MISSION] PROCESS_PLAYER_BLIPS - Show all player blips")
					SET_BIT(iLocalBoolCheck,LBOOL_SHOW_ALL_PLAYER_BLIPS)
					PROCESS_MY_PLAYER_BLIP_VISIBILITY(iTeam, iRule)
				ENDIF
			ENDIF
		ENDIF
		
		//All players blip visibility (VS missions have a 5 second window at the start of a mission where you can see other players' blips):
		IF HAS_NET_TIMER_STARTED(tdPlayerBlipsTimer)
			IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdPlayerBlipsTimer) > 5000
				
				IF NOT IS_BIT_SET(iLocalBoolCheck,LBOOL_SHOW_ALL_PLAYER_BLIPS)
					SHOW_ALL_PLAYER_BLIPS(FALSE)
					PRINTLN("[RCC MISSION] PROCESS_PLAYER_BLIPS - Stop showing all player blips, tdPlayerBlipsTimer has passed 5s")
				#IF IS_DEBUG_BUILD
				ELSE
					PRINTLN("[RCC MISSION] PROCESS_PLAYER_BLIPS - tdPlayerBlipsTimer has passed 5s, but we shouldn't stop showing all player blips as the rule setting dictates otherwise")
				#ENDIF
				ENDIF
				
				RESET_NET_TIMER(tdPlayerBlipsTimer)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_BLIP_PLAYER_ARRIVED_AT_LOCATION()
	INT iTeam = MC_PlayerBD[iLocalPart].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	IF iRule < FMMC_MAX_RULES
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitSetFive[ iRule ], ciBS_RULE5_BLIP_PLAYER_ARRIVED_AT_LOCATION)
			IF MC_playerBD[iLocalPart].iObjectiveTypeCompleted = JOB_COMP_ARRIVE_LOC
				IF NOT HAS_NET_TIMER_STARTED(tdLocationBlipTimer)
					PRINTLN("[JS] [BEASTMODE] - Player arrived at a checkpoint blipping starting blip timer")
					START_NET_TIMER(tdLocationBlipTimer)
					SET_BIT(iLocalBoolCheck14, LBOOL14_BLIP_ON_LOCATE)
					GlobalplayerBD[NATIVE_TO_INT(LocalPlayer)].bForceBeastBlip = TRUE
					PROCESS_MY_PLAYER_BLIP_VISIBILITY(iTeam, iRule)
				ELSE
					PRINTLN("[JS] [BEASTMODE] - Player arrived at a checkpoint before the last timer ended, restarting timer")
					RESET_NET_TIMER(tdLocationBlipTimer)
					START_NET_TIMER(tdLocationBlipTimer)
				ENDIF
			ENDIF
			
			IF HAS_NET_TIMER_STARTED(tdLocationBlipTimer)
				IF HAS_NET_TIMER_EXPIRED(tdLocationBlipTimer, (g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iBlipOnLocateTime[iRule] * 1000))
					PRINTLN("[JS] [BEASTMODE] - Blip timer expired, unblipping player")
					CLEAR_BIT(iLocalBoolCheck14, LBOOL14_BLIP_ON_LOCATE)
					RESET_NET_TIMER(tdLocationBlipTimer)
					GlobalplayerBD[NATIVE_TO_INT(LocalPlayer)].bForceBeastBlip = FALSE
					PROCESS_MY_PLAYER_BLIP_VISIBILITY(iTeam, iRule)
				ENDIF
			ENDIF
		ELIF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitSetSix[ iRule ], ciBS_RULE6_BLIP_PLAYER_ON_COLLECT)
			IF MC_playerBD[iLocalPart].iObjCarryCount > iNumObjsHeldLastFrame
			OR MC_playerBD[iLocalPart].iObjectiveTypeCompleted = JOB_COMP_ARRIVE_OBJ
				IF NOT HAS_NET_TIMER_STARTED(tdCollectBlipTimer)
					PRINTLN("[KH] - Player collected a checkpoint blipping starting blip timer - objs held: ", MC_playerBD[iLocalPart].iObjCarryCount)
					START_NET_TIMER(tdCollectBlipTimer)
					SET_BIT(iLocalBoolCheck16, LBOOL16_BLIP_ON_COLLECT)
					GlobalplayerBD[NATIVE_TO_INT(LocalPlayer)].bForceBeastBlip = TRUE
					FLASH_MY_PLAYER_ARROW_RED(TRUE, -1, HIGHEST_INT, 0)
					PROCESS_MY_PLAYER_BLIP_VISIBILITY(iTeam, iRule)
				ELSE
					PRINTLN("[KH] - Player collect a package before the last timer ended, restarting timer")
					RESET_NET_TIMER(tdCollectBlipTimer)
					START_NET_TIMER(tdCollectBlipTimer)
				ENDIF
			ENDIF
			
			IF HAS_NET_TIMER_STARTED(tdCollectBlipTimer)
				IF HAS_NET_TIMER_EXPIRED(tdCollectBlipTimer, (g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iBlipOnLocateTime[iRule] * 1000))
					PRINTLN("[KH] - Blip timer expired, unblipping player")
					CLEAR_BIT(iLocalBoolCheck16, LBOOL16_BLIP_ON_COLLECT)
					RESET_NET_TIMER(tdCollectBlipTimer)
					GlobalplayerBD[NATIVE_TO_INT(LocalPlayer)].bForceBeastBlip = FALSE
					FLASH_MY_PLAYER_ARROW_RED(FALSE)
					PROCESS_MY_PLAYER_BLIP_VISIBILITY(iTeam, iRule)
				ENDIF
			ENDIF
			
			iNumObjsHeldLastFrame = MC_playerBD[iLocalPart].iObjCarryCount
			
			PROCESS_MY_PLAYER_BLIP_VISIBILITY(iTeam, iRule)
		ENDIF
	ENDIF

ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Health  --------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: This section covers the players health, draining, setting, modifying.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC INCREASE_PLAYER_HEALTH(INT iNewMaxHealth)
	
	IF NOT bFMMCHealthBoosted
		IF bLocalPlayerOK
			iMaxHealthBeforebigfoot = GET_PED_MAX_HEALTH(LocalPlayerPed)
			PRINTLN("[JS] [BEASTMODE] Increasing health from ", iMaxHealthBeforebigfoot," to ", iNewMaxHealth)
			SET_PED_MAX_HEALTH(LocalPlayerPed, iNewMaxHealth + 100)
			SET_ENTITY_HEALTH(LocalPlayerPed, iNewMaxHealth + 100)
			SET_PED_SUFFERS_CRITICAL_HITS(LocalPlayerPed, FALSE)
			SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_IgnoreMeleeFistWeaponDamageMult, TRUE)
			FLOAT fMaxDamage = TO_FLOAT(iNewMaxHealth / 5)
			IF fMaxDamage >= 1000
				fMaxDamage = 999
			ENDIF
			SET_PLAYER_MAX_EXPLOSIVE_DAMAGE(LocalPlayer, fMaxDamage)
			bFMMCHealthBoosted = TRUE
		ENDIF
	ENDIF

ENDPROC

PROC BLOCK_PLAYER_HEALTH_REGEN(BOOL bSet)
	IF bSet
		PRINTLN("[JS] BLOCK_PLAYER_HEALTH_REGEN - SET")
		SET_PLAYER_HEALTH_RECHARGE_MULTIPLIER(localPlayer, 0.0)
	ELSE
		PRINTLN("[JS] BLOCK_PLAYER_HEALTH_REGEN - NOT SET")
		SET_PLAYER_HEALTH_RECHARGE_MULTIPLIER(localPlayer, 1.0)
	ENDIF
ENDPROC

PROC PROCESS_KILL_SELF_ON_TEAM_FAIL()
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciCRIT_TEAM_KILL_SELF)
		INT iTeam = MC_playerBD[iLocalPart].iteam
		INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
		IF NOT bIsAnySpectator
		AND bLocalPlayerPedOk
		AND iRule < FMMC_MAX_RULES
		AND NOT (GET_MC_SERVER_GAME_STATE() = GAME_STATE_END)
		AND NOT g_bMissionEnding
			IF IS_BIT_SET(MC_serverbd.iServerBitSet, SBBOOL_FIRST_UPDATE_DONE)
			AND MC_serverBD.iNumberOfPlayingPlayers[iTeam] = 1
				
				IF IS_BROWSER_OPEN()
					MP_FORCE_TERMINATE_INTERNET() 
					PRINTLN("[JS] PROCESS_KILL_SELF_ON_TEAM_FAIL - closing browser")
					EXIT
				ELSE
					MP_FORCE_TERMINATE_INTERNET_CLEAR()
				ENDIF
				
				#IF NOT IS_JAPANESE_BUILD 
				IF IS_PED_SWIMMING(LocalPlayerPed) 
				OR IS_CELLPHONE_CAMERA_IN_USE()
				OR IS_PED_CLIMBING(LocalPlayerPed)
				OR IS_PED_RAGDOLL(LocalPlayerPed) 
				#ENDIF
					PRINTLN("[JS] PROCESS_KILL_SELF_ON_TEAM_FAIL - Player isn't able to do suicide anim (swimming, using phone camera, climbing)")
					SET_PED_TO_RAGDOLL(LocalPlayerPed, 0, 250, TASK_RELAX, FALSE, FALSE, FALSE)
					CLEAR_ENTITY_LAST_DAMAGE_ENTITY(LocalPlayerPed)
					IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iteam].iTeamBitset2, ciBS2_CUSTOM_WASTED_SHARD)
						INT iParticipant = iLocalPart
						REQUEST_PLAY_CUSTOM_SHARD(PLAY_SHARD_TYPE_CUSTOM_WASTED, iteam, iParticipant, iPartTeammateWasKilledBy)						
					ENDIF
					
					SET_ENTITY_HEALTH(LocalPlayerPed, 0)
					EXIT
				#IF NOT IS_JAPANESE_BUILD 
				ENDIF
				#ENDIF
				
				IF IS_PED_RELOADING(LocalPlayerPed)
					NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE)
					EXIT
				ENDIF
					
				IF NOT IS_BIT_SET(MPGlobals.KillYourselfData.iBitSet, biKY_AnimStarted)
					CLEAR_PED_TASKS(LocalPlayerPed)
					NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE, NSPC_ALLOW_PLAYER_DAMAGE | NSPC_CLEAR_TASKS | NSPC_LEAVE_CAMERA_CONTROL_ON | NSPC_REENABLE_CONTROL_ON_DEATH)
					IF HAS_PED_GOT_WEAPON(LocalPlayerPed, WEAPONTYPE_PISTOL)
					AND GET_AMMO_IN_PED_WEAPON(LocalPlayerPed, WEAPONTYPE_PISTOL) > 0
					AND NOT HAS_PED_GOT_WEAPON_COMPONENT(LocalPlayerPed, WEAPONTYPE_PISTOL, WEAPONCOMPONENT_AT_PI_SUPP_02)
						SET_CURRENT_PED_WEAPON(LocalPlayerPed, WEAPONTYPE_PISTOL, TRUE)
						MPGlobals.KillYourselfData.sAnim = "PISTOL"
						SET_BIT(MPGlobals.KillYourselfData.iBitSet, biKY_PistolUsed)
						MPGlobals.KillYourselfData.fAnimPhaseToRagdoll = 0.365
						CLEAR_PED_TASKS(LocalPlayerPed)
						TASK_PLAY_ANIM(LocalPlayerPed, "MP_SUICIDE", MPGlobals.KillYourselfData.sAnim, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_ENDS_IN_DEAD_POSE|AF_EXIT_AFTER_INTERRUPTED|AF_USE_ALTERNATIVE_FP_ANIM)
						SET_BIT(MPGlobals.KillYourselfData.iBitSet, biKY_AnimStarted)
						PRINTLN("[JS] PROCESS_KILL_SELF_ON_TEAM_FAIL - Playing anim - PISTOL")
					ELSE
						SET_CURRENT_PED_WEAPON(LocalPlayerPed, WEAPONTYPE_UNARMED, TRUE)
						MPGlobals.KillYourselfData.sAnim = "PILL"
						MPGlobals.KillYourselfData.fAnimPhaseToRagdoll = 0.536	//67
						CLEAR_PED_TASKS(LocalPlayerPed)
						TASK_PLAY_ANIM(LocalPlayerPed, "MP_SUICIDE", MPGlobals.KillYourselfData.sAnim, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_ENDS_IN_DEAD_POSE|AF_EXIT_AFTER_INTERRUPTED|AF_USE_ALTERNATIVE_FP_ANIM)
						SET_BIT(MPGlobals.KillYourselfData.iBitSet, biKY_AnimStarted)
						PRINTLN("[JS] PROCESS_KILL_SELF_ON_TEAM_FAIL - Playing anim - PILL ")
					ENDIF
					iKillSafetyTimer = GET_GAME_TIMER()
				//Anim Playing Checks
				ELSE
					IF IS_ENTITY_PLAYING_ANIM(LocalPlayerPed, "MP_SUICIDE", MPGlobals.KillYourselfData.sAnim)
					OR CHECK_IF_TIME_PASSED_IN_HAS_EXPIRED(iKillSafetyTimer,6000)
						//Pistol Shot
						IF NOT IS_BIT_SET(MPGlobals.KillYourselfData.iBitSet, biKY_PistolFired)
							IF HAS_ANIM_EVENT_FIRED(LocalPlayerPed, GET_HASH_KEY("Fire"))
								CLEAR_ENTITY_LAST_DAMAGE_ENTITY(LocalPlayerPed)
								WEAPON_TYPE PlayerWeapon = GET_PEDS_CURRENT_WEAPON(LocalPlayerPed)
								IF PlayerWeapon = WEAPONTYPE_PISTOL
									SET_PED_SHOOTS_AT_COORD(LocalPlayerPed, <<0, 0, 0>>)
								ENDIF
								SET_BIT(MPGlobals.KillYourselfData.iBitSet, biKY_PistolFired)
								PRINTLN("[JS] PROCESS_KILL_SELF_ON_TEAM_FAIL - Fired Pistol")
							ENDIF
						ENDIF			
						//Anim Phase Checks
						IF( IS_ENTITY_PLAYING_ANIM(LocalPlayerPed, "MP_SUICIDE", MPGlobals.KillYourselfData.sAnim)
							AND GET_ENTITY_ANIM_CURRENT_TIME(LocalPlayerPed, "MP_SUICIDE", MPGlobals.KillYourselfData.sAnim) > MPGlobals.KillYourselfData.fAnimPhaseToRagdoll)
						OR CHECK_IF_TIME_PASSED_IN_HAS_EXPIRED(iKillSafetyTimer,6000)
							SET_PED_TO_RAGDOLL(LocalPlayerPed, 0, 250, TASK_RELAX, FALSE, FALSE, FALSE)
							CLEAR_ENTITY_LAST_DAMAGE_ENTITY(LocalPlayerPed)
							IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iteam].iTeamBitset2, ciBS2_CUSTOM_WASTED_SHARD)
								INT iParticipant = iLocalPart
								REQUEST_PLAY_CUSTOM_SHARD(PLAY_SHARD_TYPE_CUSTOM_WASTED, iteam, iParticipant, iPartTeammateWasKilledBy)
							ENDIF
							NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
							SET_ENTITY_HEALTH(LocalPlayerPed, 0)
							PRINTLN("[JS] PROCESS_KILL_SELF_ON_TEAM_FAIL - Finished")
							KILL_YOURSELF_CLEANUP()
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_TEAM_REGEN__LOCAL()
	INT iTeam = MC_PlayerBD[iPartToUse].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	BOOL bIncreasedRegen = FALSE
	BOOL bDecreasedDamage = FALSE
	BOOL bShowingHUD = FALSE
	
	IF iRule < FMMC_MAX_RULES
		IF (g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sRegenStruct[ iRule ].fDistance > 0.0
			OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSeven[iRule], ciBS_RULE7_TEAM_REGEN_FRIENDLY))
		AND NOT (ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
			AND IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_BE_MY_VALENTINES))	//Don't process team regen in sudden death
		AND bPedToUseOk
			PRINTLN("[JS][TEAMREGEN] - PROCESS_TEAM_REGEN - Distance > 0.0 (", g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sRegenStruct[ iRule ].fDistance,") AND player is OK")
			INT iRegenLoop
			PRINTLN("[PLAYER_LOOP] - PROCESS_TEAM_REGEN__LOCAL")
			FOR iRegenLoop = 0 TO NETWORK_GET_MAX_NUM_PARTICIPANTS() -1
				IF iRegenLoop != iPartToUse
					PARTICIPANT_INDEX tempPart = INT_TO_PARTICIPANTINDEX(iRegenLoop)
					IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
						BOOL bFriendlyTeamRegen = FALSE
						IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSeven[iRule], ciBS_RULE7_TEAM_REGEN_FRIENDLY)
							IF DOES_TEAM_LIKE_TEAM(iTeam, MC_PlayerBD[iRegenloop].iteam)
								PRINTLN("[JS][TEAMREGEN] Friendly team and distance = ", g_FMMC_STRUCT.sFMMCEndConditions[ MC_PlayerBD[iRegenloop].iteam ].sRegenStruct[ iRule ].fDistance)
								bFriendlyTeamRegen = TRUE
							ENDIF
						ENDIF
						IF iTeam = MC_PlayerBD[iRegenloop].iteam
						OR (bFriendlyTeamRegen 
							AND g_FMMC_STRUCT.sFMMCEndConditions[ MC_PlayerBD[iRegenloop].iteam ].sRegenStruct[ iRule ].fDistance > 0.0)
							PRINTLN("[JS][TEAMREGEN] - PROCESS_TEAM_REGEN - Player ", iRegenLoop ," is on my team/friendly (",MC_PlayerBD[iRegenloop].iteam ," , ", iTeam,")")
							PLAYER_INDEX tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
							IF NOT IS_PLAYER_SPECTATING(tempPlayer)
								IF IS_ENTITY_DEAD(GET_PLAYER_PED(tempPlayer))
								AND g_FMMC_STRUCT.sFMMCEndConditions[ MC_PlayerBD[iRegenloop].iteam ].sRegenStruct[ iRule ].fDistance > 0.0
									IF DOES_BLIP_EXIST(biRegenBlip)
										REMOVE_BLIP(biRegenBlip)
										PRINTLN("[JS][TEAMREGEN] - PROCESS_TEAM_REGEN - Player dead, removing blip")
									ENDIF
								ELIF VDIST2(GET_ENTITY_COORDS(PlayerPedToUse), GET_ENTITY_COORDS(GET_PLAYER_PED(tempPlayer))) < POW(g_FMMC_STRUCT.sFMMCEndConditions[ MC_PlayerBD[iRegenloop].iteam ].sRegenStruct[ iRule ].fDistance, 2)
									SET_PLAYER_HEALTH_RECHARGE_MULTIPLIER(PlayerToUse, TO_FLOAT(g_FMMC_STRUCT.sFMMCEndConditions[ MC_PlayerBD[iRegenloop].iteam ].sRegenStruct[ iRule ].iRegenRate))
									IF GET_PLAYER_HEALTH_RECHARGE_MAX_PERCENT(PlayerToUse) != (TO_FLOAT(g_FMMC_STRUCT.sFMMCEndConditions[ MC_PlayerBD[iRegenloop].iteam ].sRegenStruct[ iRule ].iMaxRegen) / 100)
										SET_PLAYER_HEALTH_RECHARGE_MAX_PERCENT(PlayerToUse, (TO_FLOAT(g_FMMC_STRUCT.sFMMCEndConditions[ MC_PlayerBD[iRegenloop].iteam ].sRegenStruct[ iRule ].iMaxRegen) / 100))
									ENDIF
									IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iRegenloop].iteam].iRuleBitsetSeven[iRule], ciBS_RULE7_TEAM_REGEN_DAMAGE)
										SET_PLAYER_WEAPON_DEFENSE_MODIFIER(LocalPlayer, FREEMODE_PLAYER_WEAPON_DEFENSE/2)
										SET_PLAYER_MELEE_WEAPON_DEFENSE_MODIFIER(LocalPlayer, FREEMODE_PLAYER_MELEE_WEAPON_DEFENSE/2)
										PRINTLN("[JS][TEAMREGEN] - PROCESS_TEAM_REGEN - Denfence modifier applied")
										bDecreasedDamage = TRUE
									ENDIF
									IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ MC_PlayerBD[iRegenloop].iteam ].iRuleBitSetSix[ iRule ], ciBS_RULE6_TEAM_REGEN_HUD)
										PROCESS_REGEN_HUD(TRUE, tempPlayer, MC_PlayerBD[iRegenloop].iteam, iRule)
										bShowingHUD = TRUE
									ELSE
										IF DOES_BLIP_EXIST(biRegenBlip)
											REMOVE_BLIP(biRegenBlip)
										ENDIF
									ENDIF
									bIncreasedRegen = TRUE
									PRINTLN("[JS][TEAMREGEN] - PROCESS_TEAM_REGEN - Player in range")
								ELSE
									PRINTLN("[JS][TEAMREGEN] - PROCESS_TEAM_REGEN - Player not in range")
									IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ MC_PlayerBD[iRegenloop].iteam ].iRuleBitSetSix[ iRule ], ciBS_RULE6_TEAM_REGEN_HUD)
										PROCESS_REGEN_HUD(FALSE, tempPlayer, MC_PlayerBD[iRegenloop].iteam, iRule)
										bShowingHUD = TRUE
									ELSE
										IF DOES_BLIP_EXIST(biRegenBlip)
											REMOVE_BLIP(biRegenBlip)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
			IF bIncreasedRegen = FALSE
				RESET_TEAM_HEALTH_REGEN()
			ENDIF
			IF bDecreasedDamage = FALSE
				RESET_TEAM_DAMAGE_MOD()
			ENDIF
			IF bShowingHUD = FALSE
				IF DOES_BLIP_EXIST(biRegenBlip)
					REMOVE_BLIP(biRegenBlip)
				ENDIF
			ENDIF
		ELSE 
			IF DOES_BLIP_EXIST(biRegenBlip)
				REMOVE_BLIP(biRegenBlip)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC FLOAT GET_DRAIN_PERCENTAGE(INT iTeam, INT iRule)

	//Returns the percentage health to be drained this frame
	IF HAS_NET_TIMER_STARTED(tdDamageDrainRateTimer)
	AND NOT HAS_NET_TIMER_EXPIRED(tdDamageDrainRateTimer, 3000)
		RETURN g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sDrainStruct[ iRule ].fReducedDrain[3]
	ELSE
		IF GET_IS_TASK_ACTIVE(LocalPlayerPed, CODE_TASK_COMBAT_ROLL)
			RETURN g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sDrainStruct[ iRule ].fNormalDrain
		ELIF IS_PED_SPRINTING(LocalPlayerPed)
		OR IS_PED_CLIMBING(LocalPlayerPed)
		OR IS_PED_JUMPING(LocalPlayerPed)
		OR IS_PED_VAULTING(LocalPlayerPed)
		OR IS_PED_LANDING(LocalPlayerPed)
			RETURN g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sDrainStruct[ iRule ].fReducedDrain[2]
		ELIF IS_PED_RUNNING(LocalPlayerPed)
		OR NOT HAS_NET_TIMER_EXPIRED(tdIsShooting, 1000)
			RETURN g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sDrainStruct[ iRule ].fReducedDrain[1]
		ELIF IS_PED_WALKING(LocalPlayerPed)
			RETURN g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sDrainStruct[ iRule ].fReducedDrain[0]
		ELSE	
			RETURN g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sDrainStruct[ iRule ].fNormalDrain
		ENDIF
	ENDIF
	
ENDFUNC

PROC DRAIN_HEALTH(INT iTeam, INT iRule)

	IF HAS_LOCAL_PLAYER_FINISHED_OR_REACHED_END()
		EXIT
	ENDIF
	
	//Drain health each second
	INT iDrain
	INT iMaxHealth
	INT iTime
	
	IF IS_PED_SHOOTING(LocalPlayerPed)
		RESET_NET_TIMER(tdIsShooting)
		START_NET_TIMER(tdIsShooting)
	ENDIF
	
	IF NOT HAS_NET_TIMER_STARTED(tdPerSecondDrainTimer)		//Timer to damage the player every second
		START_NET_TIMER(tdPerSecondDrainTimer)
	ELSE
		iTime = 1000
		IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdPerSecondDrainTimer) > iTime

			iMaxHealth = GET_ENTITY_MAX_HEALTH(LocalPlayerPed) - 100
			
			iDrain = ROUND(iMaxHealth / (100 / GET_DRAIN_PERCENTAGE(iTeam, iRule)))
						
			CLEAR_BIT(iKilledByHealthDrainBS, iLocalPart)		
			INT iHealth = (GET_ENTITY_HEALTH(LocalPlayerPed) - iDrain)			
			IF iHealth >= 0
				SET_ENTITY_HEALTH(LocalPlayerPed,iHealth)
				SET_BIT(iKilledByHealthDrainBS, iLocalPart)	
			ELSE
				SET_ENTITY_HEALTH(LocalPlayerPed,0)
			ENDIF
			
			RESET_NET_TIMER(tdPerSecondDrainTimer)
		ENDIF
	ENDIF
ENDPROC

PROC INCREASE_HEALTH(FLOAT fPercentage)
	PRINTLN("[JS] [HEALTHDRAIN] - INCREASE_HEALTH - Increasing player health by ", fPercentage,"% from ", GET_ENTITY_HEALTH(LocalPlayerPed)," to ", (GET_ENTITY_HEALTH(LocalPlayerPed) + (GET_ENTITY_MAX_HEALTH(LocalPlayerPed) / (100 / fPercentage))))
	INT iHeal
	iHeal = ROUND((GET_ENTITY_MAX_HEALTH(LocalPlayerPed) - 100) / (100 / fPercentage))
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciUSE_RATC_SWAP_TYPE)
		iHeal = (iHeal * -1)
	ENDIF

	SET_ENTITY_HEALTH(LocalPlayerPed, (GET_ENTITY_HEALTH(LocalPlayerPed) + iHeal))
ENDPROC

FUNC BOOL SHOULD_DRAIN_WITH_BEAST_PICKUP(INT iTeam, INT iRule)
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitSetNine[iRule], CIBS_RULE9_HEALTH_DRAIN_WITH_BEAST_ONLY)
		RETURN TRUE
	ELSE
		IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_TRANSFORMED_INTO_BEAST)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC PROCESS_PLAYER_HEALTH_DRAIN()

	INT iTeam = MC_PlayerBD[iPartToUse].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	IF iRule < FMMC_MAX_RULES
	
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitSetSix[ iRule ], ciBS_RULE6_ENABLE_HEALTH_DRAIN)
		AND NOT IS_PLAYER_SPECTATING(localPlayer)
		AND bLocalPlayerOK	
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitSetNine[iRule], ciBS_RULE9_HEALTH_DRAIN_OPTIONS_OOB_ONLY)
			AND IS_LOCAL_PLAYER_OUT_OF_BOUNDS()
				IF IS_BIT_SET(iLocalBoolCheck15, LBOOL15_DRAIN_HEALTH_INITIALISED)
					SET_PLAYER_HEALTH_RECHARGE_MULTIPLIER(localPlayer, 1.0)
					SET_PLAYER_HEALTH_RECHARGE_MAX_PERCENT(localPlayer, fCachedHealthRechargeMaxPercent)
					CLEAR_BIT(iLocalBoolCheck15, LBOOL15_DRAIN_HEALTH_INITIALISED)
					
					#IF IS_DEBUG_BUILD
						PRINTLN("[JS] [HEALTHDRAIN] [ciBS_RULE9_HEALTH_DRAIN_OPTIONS_OOB_ONLY] !--RESETTING HEALTH DRAIN--!")					
						PRINTLN("[JS] [HEALTHDRAIN] [ciBS_RULE9_HEALTH_DRAIN_OPTIONS_OOB_ONLY] SETTING MULTIPLIER to 1.0 (default) ")
						PRINTLN("[JS] [HEALTHDRAIN] [ciBS_RULE9_HEALTH_DRAIN_OPTIONS_OOB_ONLY] Setting MAX_PERCENT  -- ", fCachedHealthRechargeMaxPercent,"%")
						PRINTLN("[JS] [HEALTHDRAIN] [ciBS_RULE9_HEALTH_DRAIN_OPTIONS_OOB_ONLY] Clearing: LBOOL15_DRAIN_HEALTH_INITIALISED")
					#ENDIF
				ENDIF			
				
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("VWAR_HELP_OOB")
					CLEAR_THIS_PRINT("VWAR_HELP_OOB")
				ENDIF
				
				EXIT
			ELSE
				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("VWAR_HELP_OOB")
					PRINT_HELP("VWAR_HELP_OOB")
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitSetSix[ iRule ], ciBS_RULE6_ENABLE_HEALTH_DRAIN)
		AND NOT IS_PLAYER_SPECTATING(localPlayer)
		AND bLocalPlayerOK
		AND IS_PLAYER_CONTROL_ON(LocalPlayer)
		AND SHOULD_DRAIN_WITH_BEAST_PICKUP(iTeam, iRule)
		
			IF NOT IS_BIT_SET(iLocalBoolCheck15, LBOOL15_DRAIN_HEALTH_INITIALISED)
				#IF IS_DEBUG_BUILD
					PRINTLN("[JS] [HEALTHDRAIN] !--INITIALISING HEALTH DRAIN--!")
					PRINTLN("[JS] [HEALTHDRAIN] NORMAL DRAIN ------- ", g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sDrainStruct[ iRule ].fNormalDrain,"%")
					PRINTLN("[JS] [HEALTHDRAIN] REDUCED DRAIN 1----- ", g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sDrainStruct[ iRule ].fReducedDrain[0],"%")
					PRINTLN("[JS] [HEALTHDRAIN] REDUCED DRAIN 2----- ", g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sDrainStruct[ iRule ].fReducedDrain[1],"%")
					PRINTLN("[JS] [HEALTHDRAIN] REDUCED DRAIN 3----- ", g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sDrainStruct[ iRule ].fReducedDrain[2],"%")
					PRINTLN("[JS] [HEALTHDRAIN] PLAYER KILL HEAL --- ", g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sDrainStruct[ iRule ].fPlayerHeal,"%")
					PRINTLN("[JS] [HEALTHDRAIN] PED KILL HEAL ------ ", g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sDrainStruct[ iRule ].fPedHeal,"%")
					PRINTLN("[JS] [HEALTHDRAIN] VEHICLE KILL HEAL -- ", g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sDrainStruct[ iRule ].fVehicleHeal,"%")
					PRINTLN("[JS] [HEALTHDRAIN] OBJECT KILL TIMER -- ", g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sDrainStruct[ iRule ].fObjectTimer," seconds")
					PRINTLN("[JS] [HEALTHDRAIN] HEALTH DAMAGE RESTORE PERCENTAGE -- ", g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sDrainStruct[ iRule ].fHealthDmgRestore,"%")
				#ENDIF
				//Stop player from regenerating normally
				PRINTLN("[JS] [HEALTHDRAIN] - PROCESS_PLAYER_HEALTH_DRAIN - setting player's health regen to 0")
				fCachedHealthRechargeMaxPercent = GET_PLAYER_HEALTH_RECHARGE_MAX_PERCENT(LocalPlayer)
				SET_PLAYER_HEALTH_RECHARGE_MULTIPLIER(localPlayer, 0.0)
				SET_PLAYER_HEALTH_RECHARGE_MAX_PERCENT(localPlayer, 0.0001)
				SET_BIT(iLocalBoolCheck15, LBOOL15_DRAIN_HEALTH_INITIALISED)
			ENDIF

			IF fDrainHealPercentage > 0
				INCREASE_HEALTH(fDrainHealPercentage)
			ENDIF

			//Drain health unless the player is currently immune.
			IF NOT HAS_NET_TIMER_STARTED(tdStopDrainTimer)
				IF iStopDrainTime > 0
					START_NET_TIMER(tdStopDrainTimer)
					PRINTLN("[JS] [HEALTHDRAIN] - PROCESS_PLAYER_HEALTH_DRAIN - starting timer, player is immune from drain for ", (iStopDrainTime / 1000) ," seconds")
				ELSE
					DRAIN_HEALTH(iTeam, iRule)
				ENDIF
			ELSE
				IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdStopDrainTimer) > iStopDrainTime
					PRINTLN("[JS] [HEALTHDRAIN] - PROCESS_PLAYER_HEALTH_DRAIN - timer expired at ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdStopDrainTimer), " iStopDrain time = ", iStopDrainTime)
					PRINTLN("[JS] [HEALTHDRAIN] - PROCESS_PLAYER_HEALTH_DRAIN - player is no longer immune from health drain")
					RESET_NET_TIMER(tdStopDrainTimer)
					iStopDrainTime = 0
				ENDIF
			ENDIF
			DRAIN_HEALTH_WASTED_SHARD(GET_ENTITY_HEALTH(LocalPlayerPed))
		ELSE
			IF HAS_NET_TIMER_STARTED(tdPerSecondDrainTimer)
				RESET_NET_TIMER(tdPerSecondDrainTimer)
			ENDIF
			IF IS_BIT_SET(iLocalBoolCheck15, LBOOL15_DRAIN_HEALTH_INITIALISED)
				SET_PLAYER_HEALTH_RECHARGE_MULTIPLIER(localPlayer, 1.0)
			ENDIF
			IF iStopDrainTime > 0
				iStopDrainTime = 0
			ENDIF
		ENDIF
	ENDIF
	fDrainHealPercentage = 0.0
	
ENDPROC


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Team / Personal Vehicle Processing  -----------------------------------------------------------------------------------------------------------------------------
// ##### Description: This section covers the processing of non-creator vehicles associated with the local player.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_REACTIVATING_PERSONAL_VEHICLE_ASSOCIATED_START_VEHICLE_DESTROYED()
	
	IF IS_BIT_SET(g_iBS1_Mission, ciBS1_Mission_EnablePersonalVehicles)
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_RENABLE_PERSONAL_VEHICLES_IF_START_VEHICLE_IS_GONE)
		EXIT
	ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(sSetUpStartPosition.vehStartingVehicle)
		EXIT
	ENDIF
		
	SET_BIT(g_iBS1_Mission, ciBS1_Mission_EnablePersonalVehicles)
	
	DISABLE_PERSONAL_VEHICLE_CREATION_FOR_MISSION_STARTUP(FALSE)
	DISABLE_PERSONAL_VEHICLE_CREATION_FOR_MISSION(FALSE)
	
	PRINTLN("[Vehicles][PersonalVehicle] - PROCESS_REACTIVATING_PERSONAL_VEHICLE_ASSOCIATED_START_VEHICLE_DESTROYED - Renabling Personal Vehicles as our Starting Vehicle has been destroyed or cleaned up.")
	
ENDPROC

PROC PROCESS_REACTIVATING_PERSONAL_VEHICLES()
	
	PROCESS_REACTIVATING_PERSONAL_VEHICLE_ASSOCIATED_START_VEHICLE_DESTROYED()
	
ENDPROC

PROC PROCESS_RESPAWN_DELUXO_VEHICLE()
	INT iPlayerTeam = MC_playerBD[iLocalPart].iteam
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iPlayerTeam].iTeamBitset, ciBS_TEAM_USES_CORONA_VEHICLE_LIST)
		PRINTLN("[PROCESS_RESPAWN_DELUXO_VEHICLE] NOT ciBS_TEAM_USES_CORONA_VEHICLE_LIST EXIT")
		EXIT
	ENDIF
	
	VEHICLE_INDEX vehPlayer
	IF NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		PRINTLN("[PROCESS_RESPAWN_DELUXO_VEHICLE] NOT IS_PED_IN_ANY_VEHICLE EXIT")
		EXIT
	ENDIF
	
	vehPlayer = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
	
	IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(vehPlayer)
		PRINTLN("[PROCESS_RESPAWN_DELUXO_VEHICLE] NOT NETWORK_HAS_CONTROL_OF_ENTITY EXIT")
		EXIT
	ENDIF
	
	IF GET_ENTITY_MODEL(vehPlayer) != DELUXO
		PRINTLN("[PROCESS_RESPAWN_DELUXO_VEHICLE] GET_ENTITY_MODEL NOT DELUXO EXIT")
		EXIT
	ENDIF
	
	INT iTeamVeh = MC_PlayerBD[iLocalPart].iteam
	INT iRuleVeh = MC_ServerBD_4.iCurrentHighestPriority[iTeamVeh]	
	
	BOOL bDriveMode = NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeamVeh].iRuleBitsetEleven[iRuleVeh], ciBS_RULE11_DISABLE_DELUXO_DRIVE)
	BOOL bHoverMode = NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeamVeh].iRuleBitsetTen[iRuleVeh], ciBS_RULE10_DISABLE_DELUXO_HOVER_MODE) AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeamVeh].iTeamBitSet3, ciBS3_TEAM_VEH_DELUXO_HOVER_MODE)
	BOOL bFlightMode = NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeamVeh].iRuleBitsetTen[iRuleVeh], ciBS_RULE10_DISABLE_DELUXO_FLIGHT_MODE)
	
	PRINTLN("[PROCESS_RESPAWN_DELUXO_VEHICLE] bFlightMode: ", bFlightMode," bHoverMode: ",bHoverMode, " bDriveMode: ",bDriveMode)
	
	IF bFlightMode
		SET_DISABLE_HOVER_MODE_FLIGHT(vehPlayer, FALSE)
	ELSE
		SET_DISABLE_HOVER_MODE_FLIGHT(vehPlayer, TRUE)
	ENDIF

	IF bHoverMode
		SET_SPECIAL_FLIGHT_MODE_ALLOWED(vehPlayer, TRUE)
	ELSE
		SET_SPECIAL_FLIGHT_MODE_ALLOWED(vehPlayer, FALSE)
	ENDIF
	
	IF bDriveMode
	ELSE
		SET_SPECIAL_FLIGHT_MODE_TARGET_RATIO(vehPlayer, 1)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_TRANSFORM)
	ENDIF
ENDPROC

PROC PROCESS_SPECIAL_VEHICLE_WEAPON_GLOBAL_OPTIONS()
	IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciDISABLE_SPECIAL_VEHICLE_WEAPONS)
		AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciDISABLE_SPECIAL_VEHICLE_WEAPONS_EXCEPT_PICKUPS)
			DISABLE_SPECIAL_VEHICLE_WEAPONS(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
			DRAW_GTA_RACE_WEAPON_INFO()
		ENDIF
	ELSE
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciDISABLE_SPECIAL_VEHICLE_WEAPONS)
		AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciDISABLE_SPECIAL_VEHICLE_WEAPONS_EXCEPT_PICKUPS)
			eLastVehiclePickup = VEHICLE_PICKUP_INIT
		ENDIF
		
		IF IS_BIT_SET(iLocalBoolCheck23, LBOOL23_VEHICLE_SET_EXPLOSION_PROOF)
			PRINTLN("[JS] - Resetting player proofs, no longer in vehicle")
			CLEAR_BIT(iLocalBoolCheck23, LBOOL23_VEHICLE_SET_EXPLOSION_PROOF)
			RUN_LOCAL_PLAYER_PROOF_SETTING(TRUE)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_VEHICLE_WEAPON_DAMAGE_OUTPUT_OVERRIDE_FOR_THIS_RULE(INT iTeam, INT iRule)
	IF iRule >= FMMC_MAX_RULES
		EXIT
	ENDIF
	
	IF !bLocalPlayerPedOK
		SET_BIT(iLocalBoolCheck21, LBOOL21_VEHICLE_DAMAGE_MODIFIER_OUTPUT_RESPAWN)
	ELSE
		IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam) OR IS_BIT_SET(iLocalBoolCheck21, LBOOL21_VEHICLE_DAMAGE_MODIFIER_OUTPUT_RESPAWN)
			FLOAT fVehicleDamageModifierOutput = TO_FLOAT(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iVehicleDamageModOutput[iRule]) / 10.0
					
			IF fVehicleDamageModifierOutput <= 0.1
				PRINTLN("[PROCESS_VEHICLE_WEAPON_DAMAGE_OUTPUT_OVERRIDE_FOR_THIS_RULE] Apply fVehicleDamageModifierOutput = ", fVehicleDamageModifierOutput)
				SET_PLAYER_VEHICLE_DAMAGE_MODIFIER(LocalPlayer, 0.11) //Minimum Value
				CLEAR_BIT(iLocalBoolCheck21, LBOOL21_VEHICLE_DAMAGE_MODIFIER_OUTPUT_RESPAWN)
			ELSE
				PRINTLN("[PROCESS_VEHICLE_WEAPON_DAMAGE_OUTPUT_OVERRIDE_FOR_THIS_RULE] Apply fVehicleDamageModifierOutput = ", fVehicleDamageModifierOutput)
				SET_PLAYER_VEHICLE_DAMAGE_MODIFIER(LocalPlayer, fVehicleDamageModifierOutput)
				CLEAR_BIT(iLocalBoolCheck21, LBOOL21_VEHICLE_DAMAGE_MODIFIER_OUTPUT_RESPAWN)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_VEHICLE_WEAPON_DAMAGE_INTAKE_OVERRIDE_FOR_THIS_RULE(INT iTeam, INT iRule)
	IF iRule >= FMMC_MAX_RULES
		EXIT
	ENDIF
	
	IF !bLocalPlayerPedOK
		SET_BIT(iLocalBoolCheck21, LBOOL21_VEHICLE_DAMAGE_MODIFIER_INTAKE_RESPAWN)
	ELSE
		IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam) OR IS_BIT_SET(iLocalBoolCheck21, LBOOL21_VEHICLE_DAMAGE_MODIFIER_INTAKE_RESPAWN)
			FLOAT fVehicleDamageModifierIntake = TO_FLOAT(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iVehicleDamageModIntake[iRule]) / 10
					
			IF fVehicleDamageModifierIntake <= 0.1
				PRINTLN("[PROCESS_VEHICLE_WEAPON_DAMAGE_INTAKE_OVERRIDE_FOR_THIS_RULE] Apply fVehicleDamageModifierIntake = ", fVehicleDamageModifierIntake)
				SET_PLAYER_VEHICLE_DEFENSE_MODIFIER(LocalPlayer, 0.11) //Minimum Value
				CLEAR_BIT(iLocalBoolCheck21, LBOOL21_VEHICLE_DAMAGE_MODIFIER_INTAKE_RESPAWN)
			ELSE
				PRINTLN("[PROCESS_VEHICLE_WEAPON_DAMAGE_INTAKE_OVERRIDE_FOR_THIS_RULE] Apply fVehicleDamageModifierIntake = ", fVehicleDamageModifierIntake)
				SET_PLAYER_VEHICLE_DEFENSE_MODIFIER(LocalPlayer, fVehicleDamageModifierIntake)
				CLEAR_BIT(iLocalBoolCheck21, LBOOL21_VEHICLE_DAMAGE_MODIFIER_INTAKE_RESPAWN)
			ENDIF							
		ENDIF
	ENDIF
ENDPROC

PROC PLAY_VEHICLE_UPGRADE_EFFECTS(VEHICLE_INDEX veh, BOOL bTransformBack = FALSE)
	IF bTransformBack
		SHAKE_GAMEPLAY_CAM("SMALL_EXPLOSION_SHAKE")
		SET_PARTICLE_FX_NON_LOOPED_COLOUR(GET_BEAST_PARTICLE_COLOUR(0), GET_BEAST_PARTICLE_COLOUR(1), GET_BEAST_PARTICLE_COLOUR(2))
		USE_PARTICLE_FX_ASSET("scr_powerplay")
		START_NETWORKED_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_powerplay_beast_vanish", veh, <<0,0,0>>, <<0,0,0>>, 4.0)
		
		STRING sSoundSet = GET_BEAST_MODE_SOUNDSET()
		
		PLAY_SOUND_FROM_ENTITY(-1, "Beast_Attack",LocalPlayerPed, sSoundSet, TRUE, 250)
	ELSE
		SHAKE_GAMEPLAY_CAM("SMALL_EXPLOSION_SHAKE")
		SET_PARTICLE_FX_NON_LOOPED_COLOUR(GET_BEAST_PARTICLE_COLOUR(0), GET_BEAST_PARTICLE_COLOUR(1), GET_BEAST_PARTICLE_COLOUR(2))
		USE_PARTICLE_FX_ASSET("scr_powerplay")
		START_NETWORKED_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_powerplay_beast_appear", veh, <<0,0,0>>, <<0,0,0>>, 2.5)
		
		STRING sSoundSet = GET_BEAST_MODE_SOUNDSET()
		
		PLAY_SOUND_FROM_ENTITY(-1, "Beast_Attack",LocalPlayerPed, sSoundSet, TRUE, 250)
	ENDIF
ENDPROC

PROC APPLY_VEHICLE_UPGRADE_MODS(VEHICLE_INDEX veh, BOOL bTransformBack = FALSE)
	IF bTransformBack
		FMMC_CLEAR_ALL_VEHICLE_MODS(veh)
	ELSE
		IF DOES_ENTITY_EXIST(veh) AND NOT IS_ENTITY_DEAD(veh) AND GET_NUM_MOD_KITS(veh) > 0
			//Apply Mod Presets
			SET_VEHICLE_MOD_KIT(veh, 0)
			FMMC_CLEAR_ALL_VEHICLE_MODS(veh)
			MODEL_NAMES model = GET_ENTITY_MODEL(veh)
			IF model = TAMPA3
				FMMC_SET_VEHICLE_MOD_CHECK(veh, MOD_ROOF, 0)
				FMMC_SET_VEHICLE_MOD_CHECK(veh, MOD_BUMPER_F, 0)
				FMMC_SET_VEHICLE_MOD_CHECK(veh, MOD_BUMPER_R, 1)
				FMMC_SET_VEHICLE_MOD_CHECK(veh, MOD_CHASSIS, 2)
				FMMC_SET_VEHICLE_MOD_CHECK(veh, MOD_BONNET, 2)
				FMMC_SET_VEHICLE_MOD_CHECK(veh, MOD_WHEELS, 15)
				RELEASE_PRELOAD_MODS(veh)
			ELIF DOES_VEHICLE_HAVE_ANY_WEAPON_MODS(model)
				//this is just the same as the tampa to give them something to play with.
				//other presets can be included above.
				FMMC_SET_VEHICLE_MOD_CHECK(veh, MOD_ROOF, 0)
				FMMC_SET_VEHICLE_MOD_CHECK(veh, MOD_BUMPER_F, 0)
				FMMC_SET_VEHICLE_MOD_CHECK(veh, MOD_BUMPER_R, 1)
				FMMC_SET_VEHICLE_MOD_CHECK(veh, MOD_CHASSIS, 2)
				FMMC_SET_VEHICLE_MOD_CHECK(veh, MOD_BONNET, 2)
				FMMC_SET_VEHICLE_MOD_CHECK(veh, MOD_WHEELS, 15)
				RELEASE_PRELOAD_MODS(veh)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC UPGRADE_WEAPONISED_VEHICLE(BOOL bTransformBack = FALSE)
	
	VEHICLE_INDEX veh
	
	IF bTransformBack
		IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_UPGRADED_WEP_VEHICLE)
			PRINTLN("UPGRADE_WEAPONISED_VEHICLE - PBBOOL3_UPGRADED_WEP_VEHICLE is not set")
			EXIT
		ENDIF
		
		CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_UPGRADED_WEP_VEHICLE)
		PRINTLN("UPGRADE_WEAPONISED_VEHICLE - NO LONGER IN AN UPGRADED VEHICLE")
		
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			veh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
			
			//VFX
			PLAY_VEHICLE_UPGRADE_EFFECTS(veh, bTransformBack)
			
			//Apply Mods
			APPLY_VEHICLE_UPGRADE_MODS(veh, bTransformBack)
		ENDIF
	ELSE
		IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_UPGRADED_WEP_VEHICLE)
		OR NOT bLocalPlayerPedOk
			PRINTLN("UPGRADE_WEAPONISED_VEHICLE - PBBOOL3_UPGRADED_WEP_VEHICLE is already set so we're already a super vehicle or dead")
			EXIT
		ENDIF
		
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			veh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
			
			SET_BIT(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_UPGRADED_WEP_VEHICLE)
			PRINTLN("UPGRADE_WEAPONISED_VEHICLE - IM A SUPER VEHICLE")
			
			//VFX
			PLAY_VEHICLE_UPGRADE_EFFECTS(veh, bTransformBack)
			
			//Apply Mods
			APPLY_VEHICLE_UPGRADE_MODS(veh, bTransformBack)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_TEAM_VEHICLE_CLEAN_UP()
	IF IS_BIT_SET(iLocalBoolCheck16, LBOOL16_VEHICLE_MARKED_FOR_CLEAN_UP)
		PRINTLN("[KH] PROCESS_TEAM_VEHICLE_CLEAN_UP - Vehicle Marked for cleanup!")	
		IF HAS_NET_TIMER_STARTED(vehicleCleanUpTimer)	
			INT iTime = ciCLEAN_UP_VEHICLE_TIME			
			
			IF HAS_NET_TIMER_EXPIRED(vehicleCleanUpTimer, iTime)
			AND (NOT IS_PLAYER_RESPAWNING(LocalPlayer) OR bIsAnySpectator)
				IF NETWORK_DOES_NETWORK_ID_EXIST(netVehicleToCleanUp)
					IF NOT NETWORK_HAS_CONTROL_OF_NETWORK_ID(netVehicleToCleanUp)
						IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iTeamBitSet3, ciBS3_ENABLE_MULTIPLE_PLAYER_SAME_VEHICLE_RESPAWN)
						OR (IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iTeamBitSet3, ciBS3_ENABLE_MULTIPLE_PLAYER_SAME_VEHICLE_RESPAWN) 
						AND GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(GET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER()) = iLocalPart)
							NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(netVehicleToCleanUp)
							IF IS_VEHICLE_EMPTY(NET_TO_VEH(netVehicleToCleanUp))
								PRINTLN("[KH] PROCESS_TEAM_VEHICLE_CLEAN_UP - Requesting control of Net ID netVehicleToCleanUp")
								NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(netVehicleToCleanUp)
							ELSE
								NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(netVehicleToCleanUp)
								PRINTLN("[KH] PROCESS_TEAM_VEHICLE_CLEAN_UP - Could not request control of the vehicle as it is occupied (because another player has taken it)")
								PRINTLN("[KH] PROCESS_TEAM_VEHICLE_CLEAN_UP - Marking netVehicleToCleanUp as no longer needed")		
								IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciCLEAR_UP_VEHICLE_ON_DEATH)
									IF IS_VEHICLE_EMPTY(NET_TO_VEH(netVehicleToCleanUp))
										SET_ENTITY_COLLISION( NET_TO_VEH( netVehicleToCleanUp ), FALSE ) 
										SET_ENTITY_VISIBLE( NET_TO_VEH( netVehicleToCleanUp ), FALSE )
										DELETE_NET_ID(netVehicleToCleanUp)
									ELSE
										PRINTLN("[KH] PROCESS_TEAM_VEHICLE_CLEAN_UP - NETWORK_FADE_OUT_ENTITY Vehicle is not empty (1)")
									ENDIF
								ELSE
									CLEANUP_NET_ID(netVehicleToCleanUp)
								ENDIF
								CLEAR_BIT(iLocalBoolCheck16, LBOOL16_VEHICLE_MARKED_FOR_CLEAN_UP)
								RESET_NET_TIMER(vehicleCleanUpTimer)
							ENDIF
						ENDIF
					ELSE
						IF IS_VEHICLE_EMPTY(NET_TO_VEH(netVehicleToCleanUp), DEFAULT, DEFAULT, DEFAULT, TRUE)
							IF NOT IS_ENTITY_VISIBLE_TO_SCRIPT(NET_TO_VEH(netVehicleToCleanUp))		
								PRINTLN("[KH] PROCESS_TEAM_VEHICLE_CLEAN_UP - Deleting netVehicleToCleanUp")	
								SET_ENTITY_COLLISION( NET_TO_VEH( netVehicleToCleanUp ), FALSE ) 
								SET_ENTITY_VISIBLE( NET_TO_VEH( netVehicleToCleanUp ), FALSE )
								DELETE_NET_ID(netVehicleToCleanUp)
								CLEAR_BIT(iLocalBoolCheck16, LBOOL16_VEHICLE_MARKED_FOR_CLEAN_UP)
								RESET_NET_TIMER(vehicleCleanUpTimer)
							ELSE
								PRINTLN("[KH] PROCESS_TEAM_VEHICLE_CLEAN_UP - NETWORK_FADE_OUT_ENTITY netVehicleToCleanUp")	
								NETWORK_FADE_OUT_ENTITY(NET_TO_VEH(netVehicleToCleanUp), TRUE, TRUE)
							ENDIF
						ELSE
							PRINTLN("[KH] PROCESS_TEAM_VEHICLE_CLEAN_UP - NETWORK_FADE_OUT_ENTITY Vehicle is not empty (2)")
						ENDIF
					ENDIF
				ELSE
					PRINTLN("[KH] PROCESS_TEAM_VEHICLE_CLEAN_UP - netVehicleToCleanUp does not exist")
					CLEAR_BIT(iLocalBoolCheck16, LBOOL16_VEHICLE_MARKED_FOR_CLEAN_UP)
					RESET_NET_TIMER(vehicleCleanUpTimer)
				ENDIF
			ELSE
				PRINTLN("[KH] PROCESS_TEAM_VEHICLE_CLEAN_UP - Waiting for Timer, or we are respawning.")
			ENDIF
		ELSE
			PRINTLN("[KH] PROCESS_TEAM_VEHICLE_CLEAN_UP - Timer not started.")
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_TEAM_VEHICLE_FLAG_AND_CLEAN_UP()	
	IF HAS_TEAM_FAILED(MC_PlayerBD[iLocalPart].iTeam)
		IF NOT IS_BIT_SET(iLocalBoolCheck16, LBOOL16_VEHICLE_MARKED_FOR_CLEAN_UP)
			SET_BIT(iLocalBoolCheck16, LBOOL16_VEHICLE_MARKED_FOR_CLEAN_UP)
			START_NET_TIMER(vehicleCleanUpTimer)
			netVehicleToCleanUp = netRespawnVehicle
			PRINTLN("[RCC MISSION][SD] SETTING VEHICLE TO CLEAN UP AS THIS TEAM FAILED iTeam: ", MC_PlayerBD[iLocalPart].iTeam)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciCLEAR_UP_VEHICLE_ON_DEATH)
		PROCESS_TEAM_VEHICLE_CLEAN_UP()
	ENDIF
ENDPROC

PROC PROCESS_RACE_BOOST_START()
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciRACE_BOOST_START) 
		IF HAS_NET_TIMER_STARTED(g_FMMC_STRUCT.stBoostTimer)
			DO_VEH_BOOST()
		ENDIF
		UPDATE_VEHICLE_BOOSTING()
	ENDIF
ENDPROC

PROC PROCESS_UPDATE_VEHICLE_HEALTH_ON_RULE()
	INT iTeam = MC_PlayerBD[iLocalPart].iteam
	
	IF iTeam < FMMC_MAX_TEAMS
		INT iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]
		IF iRule < FMMC_MAX_RULES
			IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iVehicleHealthSwap[iRule] > 0 // just incase...
				IF sVehicleSwap.eVehicleSwapState = eVehicleSwapState_IDLE
					IF IS_BIT_SET(iLocalBoolCheck6, (LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + MC_playerBD[iPartToUse].iteam))
						IF NOT IS_BIT_SET(iLocalBoolCheck21, LBOOL21_VEHICLE_HEALTH_UDPATED)
							SET_BIT(iLocalBoolCheck21, LBOOL21_VEHICLE_HEALTH_UDPATED)
						ENDIF
					ENDIF
					IF IS_BIT_SET(iLocalBoolCheck21, LBOOL21_VEHICLE_HEALTH_UDPATED)
						IF bLocalPlayerPedOK
						AND IS_PED_IN_ANY_VEHICLE(localPlayerPed)
						
							VEHICLE_INDEX vehPlayer = GET_VEHICLE_PED_IS_IN(localPlayerPed)
						
							IF IS_ENTITY_ALIVE(vehPlayer)
								PRINTLN("[LM][MISSION][PROCESS_UPDATE_VEHICLE_HEALTH_ON_RULE] - iVehicleHealthSwap is set. Applying Team Respawn Health Settings to new Vehicle. Setting: ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iVehicleHealthSwap[iRule])
								SET_VEHICLE_HEALTH_FROM_RULE_AND_TEAM(iTeam, iRule, vehPlayer)
								CLEAR_BIT(iLocalBoolCheck21, LBOOL21_VEHICLE_HEALTH_UDPATED)
							ELSE
								CLEAR_BIT(iLocalBoolCheck21, LBOOL21_VEHICLE_HEALTH_UDPATED)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SWAP_VEHICLE_ON_RULE()
	
	IF IS_BIT_SET(sWarpPortal.iWarpPortalBitset, ciWarpPortalBS_TriggeredVehicleTransform)
		EXIT
	ENDIF
	
	INT iTeam = MC_PlayerBD[iLocalPart].iteam
	INT iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]
	
	BOOL bDisableTransforming = FALSE
	
	IF iRule < FMMC_MAX_RULES
	
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEight[iRule], ciBS_RULE8_VEHICLE_SWAP_ENABLED)
		AND sVehicleSwap.eVehicleSwapState = eVehicleSwapState_IDLE
			IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + MC_playerBD[iLocalPart].iteam)
			AND IS_BIT_SET(iLocalBoolCheck16, LBOOL16_TEAM_0_IS_PAST_THEIR_FIRST_RULE + MC_playerBD[iLocalPart].iteam)
			AND bDisableTransforming = FALSE
				PRINTLN("[JS][VehSwap] - PROCESS_SWAP_VEHICLE_ON_RULE - Initialising Vehicle Swap. Rule: ", iRule)
				
				IF DOES_PARTICLE_FX_LOOPED_EXIST(sVehicleSwap.ptfxID)
					REMOVE_PARTICLE_FX(sVehicleSwap.ptfxID)
					PRINTLN("[VehSwap] - PROCESS_SWAP_VEHICLE_ON_RULE - Clearing up the smokey effect because we are swapping now!")
				ENDIF
				
				IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
					VEHICLE_INDEX viPlayerVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
					REMOVE_PARTICLE_FX_FROM_ENTITY(viPlayerVeh) // 4003739
					REMOVE_PARTICLE_FX_FROM_ENTITY(LocalPlayerPed) // 3979316
					PRINTLN("[VehSwap] - PROCESS_SWAP_VEHICLE_ON_RULE - Removing particle FX from the vehicle the player is currently in")
				ENDIF
				
				REMOVE_PARTICLE_FX_FROM_ENTITY(LocalPlayerPed) // 3979316
				PRINTLN("[VehSwap] - PROCESS_SWAP_VEHICLE_ON_RULE - Removing particle FX from the local player's ped")
				
				SET_BIT(sWarpPortal.iWarpPortalBitset, ciWarpPortalBS_VehicleTransformBlocked)
				SET_VEHICLE_SWAP_STATE(sVehicleSwap.eVehicleSwapState, eVehicleSwapState_INITIALISE)
			ELSE
				IF iRule < FMMC_MAX_RULES
					IF MC_serverBD_4.sVehicleSwaps[iTeam][iRule + 1].mnVehicleModelSwap != DUMMY_MODEL_FOR_SCRIPT
						MODEL_NAMES mnNextVeh = MC_serverBD_4.sVehicleSwaps[iTeam][iRule + 1].mnVehicleModelSwap
						
						PRINTLN("[VehSwapOnRule] Next vehicle is ", mnNextVeh, " pre-loading now")
						
						REQUEST_MODEL(mnNextVeh)
					ENDIF
				ENDIF
				
				IF bDisableTransforming
					PRINTLN("[VehSwap] Not starting new transform due to bDisableTransforming!")
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_PLAYER_RESPAWNING(LocalPlayer)
			bReadyToSwap = FALSE
		ENDIF
		
		IF bLocalPlayerPedOK
		AND NOT IS_PLAYER_RESPAWNING(LocalPlayer)
			
			IF NOT bReadyToSwap // 4016888
				bReadyToSwap = TRUE // Delays the vehicle swapping by a frame
				PRINTLN("[VehSwap] Waiting for a frame before to swapping, setting bReadyToSwap to TRUE")
				EXIT
			ENDIF
		
			VEHICLE_SWAP_STATE ePreState = sVehicleSwap.eVehicleSwapState
			PROCESS_VEHICLE_SWAP(sVehicleSwap, MC_serverBD_4.sVehicleSwaps[iTeam][iRule].mnVehicleModelSwap)
			IF MC_serverBD_4.sVehicleSwaps[iTeam][iRule].mnVehicleModelSwap != DUMMY_MODEL_FOR_SCRIPT
				IF (ePreState = eVehicleSwapState_CREATE
				OR ePreState = eVehicleSwapState_INITIALISE)
				AND sVehicleSwap.eVehicleSwapState != eVehicleSwapState_CREATE
				AND sVehicleSwap.eVehicleSwapState != eVehicleSwapState_INITIALISE
					PRINTLN("[JS][VehSwap] - PROCESS_SWAP_VEHICLE_ON_RULE - Create state over, doing mission controller specific setup")
					INT iColour1, iColour2
					IF DOES_ENTITY_EXIST(sVehicleSwap.viPrevVeh)
						IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
							GET_VEHICLE_COLOURS(sVehicleSwap.viPrevVeh, iColour1, iColour2)
						ENDIF
						FMMC_SET_THIS_VEHICLE_COLOURS(sVehicleSwap.viNewVeh, iColour1, -1, -1, iColour2, iColour1)
					ENDIF
					INT iHealth = GET_FMMC_PLAYER_VEHICLE_HEALTH_FOR_TEAM(MC_playerBD[iLocalPart].iteam)
					IF iHealth >= 3000 
						SET_VEHICLE_EXPLODES_ON_HIGH_EXPLOSION_DAMAGE(sVehicleSwap.viNewVeh, FALSE)
					ENDIF
					SET_ENTITY_HEALTH(sVehicleSwap.viNewVeh, iHealth)
					SET_VEHICLE_WEAPON_DAMAGE_SCALE(sVehicleSwap.viNewVeh, GET_FMMC_PLAYER_VEHICLE_WEAPON_DAMAGE_SCALE_FOR_TEAM(MC_playerBD[iLocalPart].iTeam))
					SET_VEHICLE_ENGINE_HEALTH(sVehicleSwap.viNewVeh, TO_FLOAT(iHealth))
					SET_VEHICLE_PETROL_TANK_HEALTH(sVehicleSwap.viNewVeh, TO_FLOAT(iHealth))
					SET_VEHICLE_BODY_HEALTH(sVehicleSwap.viNewVeh, TO_FLOAT(iHealth))
					IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iVehicleHealthSwap[iRule] > -1							
						SET_VEHICLE_HEALTH_FROM_RULE_AND_TEAM(iTeam, iRule, sVehicleSwap.viNewVeh)
					ENDIF
					SET_VEHICLE_WEAPON_MODS_FROM_RULE_AND_TEAM(iTeam, iRule, sVehicleSwap.viNewVeh)
					
					IF sVehicleSwap.wtPrevWep != WEAPONTYPE_INVALID
						IF SET_CURRENT_PED_VEHICLE_WEAPON(LocalPlayerPed, sVehicleSwap.wtPrevWep)
							PRINTLN("[VehSwap] Setting new vehicle's weapon type to ", sVehicleSwap.wtPrevWep)
						ELSE
							PRINTLN("[VehSwap] Failed to set vehicle's weapon type to ", sVehicleSwap.wtPrevWep)
							RETAIN_APPROXIMATE_WEAPON(sVehicleSwap, MC_serverBD_4.sVehicleSwaps[iTeam][iRule].mnVehicleModelSwap)
						ENDIF
						
						IF sVehicleSwap.bMissilesHoming
						AND sVehicleSwap.wtPrevWep != WEAPONTYPE_DLC_VEHICLE_HUNTER_BARRAGE
							PRINTLN("{TMS][VehSwap] Leaving new vehicle's weapon on 'homing'")
						ELSE
							IF MC_serverBD_4.sVehicleSwaps[iTeam][iRule].mnVehicleModelSwap = HUNTER
								SET_CURRENT_PED_VEHICLE_WEAPON(LocalPlayerPed, WEAPONTYPE_DLC_VEHICLE_HUNTER_BARRAGE)
								PRINTLN("[VehSwap] Setting Hunter's weapon to Barrage rather than the homing missile")
							ELSE
								SET_PLAYER_VEHICLE_WEAPON_TO_NON_HOMING(LocalPlayer)
								PRINTLN("[VehSwap] Setting the vehicle to not use homing missiles (calling SET_PLAYER_VEHICLE_WEAPON_TO_NON_HOMING)")
							ENDIF
						ENDIF
					ENDIF
					
					IF MC_serverBD_4.sVehicleSwaps[iTeam][iRule].iVehicleAirCounterCooldown > -1
						SET_OVERRIDE_FLARE_COOLDOWN(TRUE, MC_serverBD_4.sVehicleSwaps[iTeam][iRule].iVehicleAirCounterCooldown)
						SET_OVERRIDE_CHAFF_COOLDOWN(TRUE, MC_serverBD_4.sVehicleSwaps[iTeam][iRule].iVehicleAirCounterCooldown)
						PRINTLN("[VehSwap] Setting the new vehicle's countermeasure cooldown to ", MC_serverBD_4.sVehicleSwaps[iTeam][iRule].iVehicleAirCounterCooldown)
					ENDIF

					IF sVehicleSwap.eVehicleSwapState = eVehicleSwapState_APPLY_FORCES
						VS_PROCESS_APPLY_FORCES(sVehicleSwap, MC_serverBD_4.sVehicleSwaps[iTeam][iRule].mnVehicleModelSwap)
					ENDIF
					
					g_SpawnData.MissionSpawnDetails.SpawnModel = MC_serverBD_4.sVehicleSwaps[iTeam][iRule].mnVehicleModelSwap
					PRINTLN("[VehSwap] Setting g_SpawnData.MissionSpawnDetails.SpawnModel to ", MC_serverBD_4.sVehicleSwaps[iTeam][iRule].mnVehicleModelSwap)
				ENDIF
			ELSE
				IF sVehicleSwap.eVehicleSwapState != eVehicleSwapState_IDLE
					IF sVehicleSwap.eVehicleSwapState = eVehicleSwapState_CREATE
					OR sVehicleSwap.eVehicleSwapState = eVehicleSwapState_INITIALISE
					OR sVehicleSwap.eVehicleSwapState = eVehicleSwapState_APPLY_FORCES
						SET_VEHICLE_SWAP_STATE(sVehicleSwap.eVehicleSwapState, eVehicleSwapState_CLEANUP)							
					ENDIF
					PROCESS_VEHICLE_SWAP(sVehicleSwap, DUMMY_MODEL_FOR_SCRIPT)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SWAP_VEHICLE_ON_WARP()

	IF IS_BIT_SET(sWarpPortal.iWarpPortalBitset, ciWarpPortalBS_VehicleTransformBlocked)
		EXIT
	ENDIF
	
	INT iTeam = MC_PlayerBD[iLocalPart].iteam
	INT iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]
	
	IF iRule < FMMC_MAX_RULES
		
		IF sVehicleSwap.eVehicleSwapState = eVehicleSwapState_CLEANUP
			CLEAR_BIT(sWarpPortal.iWarpPortalBitset, ciWarpPortalBS_TriggeredVehicleTransform)
		ENDIF
		
		IF sVehicleSwap.eVehicleSwapState = eVehicleSwapState_IDLE			
		AND IS_BIT_SET(sWarpPortal.iWarpPortalBitset, ciWarpPortalBS_TriggeredVehicleTransform)
			PRINTLN("[JS][VehSwap] - PROCESS_SWAP_VEHICLE_ON_WARP - Initialising Vehicle Swap. Rule: ", iRule)
			
			IF DOES_PARTICLE_FX_LOOPED_EXIST(sVehicleSwap.ptfxID)
				REMOVE_PARTICLE_FX(sVehicleSwap.ptfxID)
				PRINTLN("[VehSwap] - PROCESS_SWAP_VEHICLE_ON_WARP - Clearing up the smokey effect because we are swapping now!")
			ENDIF
			
			IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
				VEHICLE_INDEX viPlayerVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
				REMOVE_PARTICLE_FX_FROM_ENTITY(viPlayerVeh) // 4003739
				REMOVE_PARTICLE_FX_FROM_ENTITY(LocalPlayerPed) // 3979316
				PRINTLN("[VehSwap] - PROCESS_SWAP_VEHICLE_ON_WARP - Removing particle FX from the vehicle the player is currently in")
			ENDIF
			
			REMOVE_PARTICLE_FX_FROM_ENTITY(LocalPlayerPed) // 3979316
			PRINTLN("[VehSwap] - PROCESS_SWAP_VEHICLE_ON_WARP - Removing particle FX from the local player's ped")
			
			SET_VEHICLE_SWAP_STATE(sVehicleSwap.eVehicleSwapState, eVehicleSwapState_INITIALISE)
		ENDIF
				
		IF IS_PLAYER_RESPAWNING(LocalPlayer)
		AND NOT IS_FAKE_MULTIPLAYER_MODE_SET()
			bReadyToSwap = FALSE
		ENDIF
		
		IF bLocalPlayerPedOK
		AND (NOT IS_PLAYER_RESPAWNING(LocalPlayer) OR IS_FAKE_MULTIPLAYER_MODE_SET())
			
			IF NOT bReadyToSwap // 4016888
				bReadyToSwap = TRUE // Delays the vehicle swapping by a frame
				PRINTLN("[VehSwap] Waiting for a frame before to swapping, setting bReadyToSwap to TRUE")
				EXIT
			ENDIF
				
			VEHICLE_SWAP_STATE ePreState = sVehicleSwap.eVehicleSwapState
			PROCESS_VEHICLE_SWAP(sVehicleSwap, sWarpPortal.sVehicleSwap.mnVehicleModelSwap)
			IF sWarpPortal.sVehicleSwap.mnVehicleModelSwap != DUMMY_MODEL_FOR_SCRIPT
				IF (ePreState = eVehicleSwapState_CREATE
				OR ePreState = eVehicleSwapState_INITIALISE)
				AND sVehicleSwap.eVehicleSwapState != eVehicleSwapState_CREATE
				AND sVehicleSwap.eVehicleSwapState != eVehicleSwapState_INITIALISE
					PRINTLN("[JS][VehSwap] - PROCESS_SWAP_VEHICLE_ON_WARP - Create state over, doing mission controller specific setup")
					INT iColour1, iColour2
					IF DOES_ENTITY_EXIST(sVehicleSwap.viPrevVeh)
						IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
							GET_VEHICLE_COLOURS(sVehicleSwap.viPrevVeh, iColour1, iColour2)
						ENDIF
						FMMC_SET_THIS_VEHICLE_COLOURS(sVehicleSwap.viNewVeh, iColour1, -1, -1, iColour2, iColour1)
					ENDIF
					INT iHealth = GET_FMMC_PLAYER_VEHICLE_HEALTH_FOR_TEAM(MC_playerBD[iLocalPart].iteam)
					IF iHealth >= 3000 
						SET_VEHICLE_EXPLODES_ON_HIGH_EXPLOSION_DAMAGE(sVehicleSwap.viNewVeh, FALSE)
					ENDIF
					SET_ENTITY_HEALTH(sVehicleSwap.viNewVeh, iHealth)
					SET_VEHICLE_WEAPON_DAMAGE_SCALE(sVehicleSwap.viNewVeh, GET_FMMC_PLAYER_VEHICLE_WEAPON_DAMAGE_SCALE_FOR_TEAM(MC_playerBD[iLocalPart].iTeam))
					SET_VEHICLE_ENGINE_HEALTH(sVehicleSwap.viNewVeh, TO_FLOAT(iHealth))
					SET_VEHICLE_PETROL_TANK_HEALTH(sVehicleSwap.viNewVeh, TO_FLOAT(iHealth))
					SET_VEHICLE_BODY_HEALTH(sVehicleSwap.viNewVeh, TO_FLOAT(iHealth))
								
					// =====
					// Revisit if we need this. Implemented a mod preset index instead.
					// =====	
					// IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iVehicleHealthSwap[iRule] > -1							
					// 	SET_VEHICLE_HEALTH_FROM_RULE_AND_TEAM(iTeam, iRule, sVehicleSwap.viNewVeh)
					// ENDIF
					// SET_VEHICLE_WEAPON_MODS_FROM_RULE_AND_TEAM(iTeam, iRule, sVehicleSwap.viNewVeh)
					
					SET_VEHICLE_ALL_CUSTOM_MODS(sVehicleSwap.viNewVeh, sWarpPortal.sVehicleSwap.iModPresetIndex)
					
					IF sVehicleSwap.wtPrevWep != WEAPONTYPE_INVALID
						IF SET_CURRENT_PED_VEHICLE_WEAPON(LocalPlayerPed, sVehicleSwap.wtPrevWep)
							PRINTLN("[VehSwap] Setting new vehicle's weapon type to ", sVehicleSwap.wtPrevWep)
						ELSE
							PRINTLN("[VehSwap] Failed to set vehicle's weapon type to ", sVehicleSwap.wtPrevWep)									
							RETAIN_APPROXIMATE_WEAPON(sVehicleSwap, sWarpPortal.sVehicleSwap.mnVehicleModelSwap)
						ENDIF
						
						IF sVehicleSwap.bMissilesHoming
						AND sVehicleSwap.wtPrevWep != WEAPONTYPE_DLC_VEHICLE_HUNTER_BARRAGE
							PRINTLN("{TMS][VehSwap] Leaving new vehicle's weapon on 'homing'")
						ELSE
							IF sWarpPortal.sVehicleSwap.mnVehicleModelSwap = HUNTER
								SET_CURRENT_PED_VEHICLE_WEAPON(LocalPlayerPed, WEAPONTYPE_DLC_VEHICLE_HUNTER_BARRAGE)
								PRINTLN("[VehSwap] Setting Hunter's weapon to Barrage rather than the homing missile")
							ELSE
								SET_PLAYER_VEHICLE_WEAPON_TO_NON_HOMING(LocalPlayer)
								PRINTLN("[VehSwap] Setting the vehicle to not use homing missiles (calling SET_PLAYER_VEHICLE_WEAPON_TO_NON_HOMING)")
							ENDIF
						ENDIF
					ENDIF
					
					IF sWarpPortal.sVehicleSwap.iVehicleAirCounterCooldown > -1
						SET_OVERRIDE_FLARE_COOLDOWN(TRUE, sWarpPortal.sVehicleSwap.iVehicleAirCounterCooldown)
						SET_OVERRIDE_CHAFF_COOLDOWN(TRUE, sWarpPortal.sVehicleSwap.iVehicleAirCounterCooldown)
						PRINTLN("[VehSwap] Setting the new vehicle's countermeasure cooldown to ", sWarpPortal.sVehicleSwap.iVehicleAirCounterCooldown)
					ENDIF

					IF sVehicleSwap.eVehicleSwapState = eVehicleSwapState_APPLY_FORCES
						VS_PROCESS_APPLY_FORCES(sVehicleSwap, sWarpPortal.sVehicleSwap.mnVehicleModelSwap)
					ENDIF
					
					g_SpawnData.MissionSpawnDetails.SpawnModel = sWarpPortal.sVehicleSwap.mnVehicleModelSwap
					PRINTLN("[VehSwap] Setting g_SpawnData.MissionSpawnDetails.SpawnModel to ", sWarpPortal.sVehicleSwap.mnVehicleModelSwap)
				ENDIF
			ELSE
				IF sVehicleSwap.eVehicleSwapState != eVehicleSwapState_IDLE
					IF sVehicleSwap.eVehicleSwapState = eVehicleSwapState_CREATE
					OR sVehicleSwap.eVehicleSwapState = eVehicleSwapState_INITIALISE
					OR sVehicleSwap.eVehicleSwapState = eVehicleSwapState_APPLY_FORCES
						PRINTLN("[JS][VehSwap] - PROCESS_SWAP_VEHICLE_ON_WARP - DUMMY_MODEL_FOR_SCRIPT, CLEANING UP")
						SET_VEHICLE_SWAP_STATE(sVehicleSwap.eVehicleSwapState, eVehicleSwapState_CLEANUP)							
					ENDIF
					PROCESS_VEHICLE_SWAP(sVehicleSwap, DUMMY_MODEL_FOR_SCRIPT)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SWAP_VEHICLE_RETRACTABLE_WHEEL_BUTTON()
	IF NOT IS_BIT_SET(iLocalBoolCheck21, LBOOL21_VEHICLE_SWAP_RETRACTABLE_WHEEL_BUTTON)
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			VEHICLE_INDEX vehIndex = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
			
			IF GET_HAS_RETRACTABLE_WHEELS(vehIndex)
				IF(GET_ENTITY_MODEL(vehIndex) = BLAZER5)
					SET_VEHICLE_USE_BOOST_BUTTON_FOR_WHEEL_RETRACT(TRUE)
					SET_BIT(iLocalBoolCheck21, LBOOL21_VEHICLE_SWAP_RETRACTABLE_WHEEL_BUTTON)
					PRINTLN("[MISSION] LBOOL21_VEHICLE_SWAP_RETRACTABLE_WHEEL_BUTTON, YES ")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_LOCAL_PLAYER_MELEE_DAMAGE_MODIFIER()
	IF g_FMMC_STRUCT.fMeleeDamageModifier <> 1
		INT iDamageModLoop
		PRINTLN("[PLAYER_LOOP] - PROCESS_EVERY_FRAME_PLAYER_CHECKS")
		FOR iDamageModLoop = 0 TO NETWORK_GET_MAX_NUM_PARTICIPANTS() -1
			PARTICIPANT_INDEX tempPart = INT_TO_PARTICIPANTINDEX(iDamageModLoop)
			IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
				PLAYER_INDEX tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
				IF g_FMMC_STRUCT.fMeleeDamageModifier <= 0.1
					PRINTLN("[LH][MELEE_DAMAGE] g_FMMC_STRUCT.fMeleeDamageModifier = ",g_FMMC_STRUCT.fMeleeDamageModifier)
					SET_PLAYER_WEAPON_TAKEDOWN_DEFENSE_MODIFIER(tempPlayer, 0.11) //minimum value
				ELSE
					PRINTLN("[LH][MELEE_DAMAGE] g_FMMC_STRUCT.fMeleeDamageModifier = ",g_FMMC_STRUCT.fMeleeDamageModifier)
					SET_PLAYER_WEAPON_TAKEDOWN_DEFENSE_MODIFIER(tempPlayer, g_FMMC_STRUCT.fMeleeDamageModifier)
				ENDIF
			ENDIF
		ENDFOR
		
		bTakedownDamageModifier = TRUE
	ENDIF
ENDPROC

// FMMC2020 - Refactor for missions ?
PROC PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC()	
	INT iTeam = MC_PlayerBD[iLocalPart].iTeam
	BOOL bDoWeNeedToPromote
	
	#IF IS_DEBUG_BUILD
		IF bMultiPersonVehiclePrints
			PRINTLN("[SEAT_SWAP_DEBUG] ***************** MULTI VEHICLE PRINT SPAM *****************")
			PRINTLN("[SEAT_SWAP_DEBUG] Local participant ", iLocalPart)
			PRINTLN("[SEAT_SWAP_DEBUG] GET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER(): ",GET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER())
			PRINTLN("[SEAT_SWAP_DEBUG] Driver part: ", GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(GET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER()))
			
			PRINTLN("[SEAT_SWAP_DEBUG] My preference: ", GET_PLAYER_RESPAWN_VEHICLE_SEAT_PREFERENCE())
			IF GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(GET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER()) > -1
				PRINTLN("[SEAT_SWAP_DEBUG] Driver seat prefrence: ", GlobalplayerBD[GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(GET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER())].iRespawnSeatPreference)
				PRINTLN("[SEAT_SWAP_DEBUG] Driver participant number from partners array", GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(GET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER()))
			ELSE
				PRINTLN("[SEAT_SWAP_DEBUG] Driver seat prefrence: CANNOT PRINT AS DRIVER INDEX IS -1")
			ENDIF
			
			PRINTLN("[SEAT_SWAP_DEBUG] Has cooldown timer started? ", HAS_NET_TIMER_STARTED(tdVehicleSeatSwapCooldownTimer))
			PRINTLN("[SEAT_SWAP_DEBUG] Is seat swap in progress? ", IS_MULTI_TEAM_VEHICLE_SEAT_SWAP_IN_PROGRESS())
		ENDIF
	#ENDIF
	
	IF GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(GET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER()) > -1
	AND (NOT NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(GET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER())))
	OR IS_BIT_SET(MC_playerBD[GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(GET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER())].iClientBitSet, PBBOOL_ANY_SPECTATOR))
		PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC] - Setting bDoWeNeedToPromote due to player leaving")
		bDoWeNeedToPromote = TRUE
	ENDIF
	
	IF GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(GET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER()) > -1
	AND GlobalplayerBD[GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(GET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER())].iRespawnSeatPreference != -1
	AND NOT IS_MULTI_TEAM_VEHICLE_SEAT_SWAP_IN_PROGRESS()
	AND NOT HAS_NET_TIMER_STARTED(tdVehicleSeatSwapCooldownTimer)
		PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC] - Setting bDoWeNeedToPromote due to driver having incorrect preference")
		bDoWeNeedToPromote = TRUE
	ENDIF
	// If the Driver Participant leaves or is inactive, then promote the next person in the array.

	IF bDoWeNeedToPromote
 
		INT iOldDriverPointer = GET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER()
		INT iNewDriverPointer = (iOldDriverPointer+1)
		
		IF iNewDriverPointer >= MAX_VEHICLE_PARTNERS
			iNewDriverPointer = 0
		ENDIF
		
		PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC] - DriverPointer is old and is pointing to an invalid participant. Reassigning iOldDriverPointer: ", iOldDriverPointer)			
		PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC] - iNewDriverPointer: ", iNewDriverPointer, " PartNum: ", GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(iNewDriverPointer))
		
		SET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER(iNewDriverPointer)			
		
		// If we are the new Driver then we need to call some stuff...
		IF GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(GET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER()) = iLocalPart
			SET_RACE_PASSENGER_GLOBAL(FALSE)
			SET_RACE_DRIVER_GLOBAL(TRUE)
			SET_PLAYER_RESPAWN_VEHICLE_SEAT_PREFERENCE(ENUM_TO_INT(VS_DRIVER))
			PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC] - I am now promoted as the new Driver")
		ENDIF
		
		// Cleanup the inactive participant from our list of partners. This should fix respawn issues and seat swapping issues.
		IF GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(iOldDriverPointer) > -1
		AND (NOT NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(iOldDriverPointer)))
		OR IS_BIT_SET(MC_playerBD[GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(iOldDriverPointer)].iClientBitSet, PBBOOL_ANY_SPECTATOR))
			PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC] - Invalididating the old driver from our list...")
			SET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(iOldDriverPointer, -1)
		ENDIF
		IF IS_MULTI_TEAM_VEHICLE_SEAT_SWAP_IN_PROGRESS()
			DO_SCREEN_FADE_IN(250)
			CLEANUP_MULTI_VEHICLE_SEAT_SWAP()
		ENDIF
	ENDIF
	
	// Cleanup the inactive participant from our list of partners. This should fix respawn issues and seat swapping issues.
	INT iPart = 0 
	FOR iPart = 0 TO MAX_VEHICLE_PARTNERS-1
		IF GlobalplayerBD[iPart].iRespawnSeatPreference != -1
			IF GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(iPart) > -1
			AND (NOT NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(iPart)))
			OR IS_BIT_SET(MC_playerBD[GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(iPart)].iClientBitSet, PBBOOL_ANY_SPECTATOR))
				PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC] - Invalididating and old passenger from our list...")
				SET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(iPart, -1)
				
				IF IS_MULTI_TEAM_VEHICLE_SEAT_SWAP_IN_PROGRESS()
					DO_SCREEN_FADE_IN(250)
					CLEANUP_MULTI_VEHICLE_SEAT_SWAP()
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	IF NOT IS_MULTI_TEAM_VEHICLE_SEAT_SWAP_IN_PROGRESS()
		IF bLocalPlayerPedOK
		AND INT_TO_ENUM(VEHICLE_SEAT, GET_PLAYER_RESPAWN_VEHICLE_SEAT_PREFERENCE()) = VS_DRIVER
		AND NOT IS_PLAYER_RESPAWNING(LocalPlayer)
			IF IS_PED_IN_ANY_VEHICLE(localPlayerPed)
				VEHICLE_INDEX vehPlayer = GET_VEHICLE_PED_IS_IN(localPlayerPed)
				
				// Sometimes the Mission Controller did not claim ownership of the Respawn System created vehicle. This is to fix that. This needs to happen FAST because passengers who try to respawn in the Drivers vehicle when it's not a MC owned veh will spawn outside of it!
				
				IF DOES_ENTITY_EXIST(vehPlayer)
					IF NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(vehPlayer)
					OR NOT IS_ENTITY_A_MISSION_ENTITY(vehPlayer)
					OR IS_BIT_SET(iLocalBoolCheck25, LBOOL25_DRIVER_DIED_OR_RESPAWNED_INTO_NEW_VEH)
						PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC] - Calling to grab ownership for FM_MISSION_CONTROLLER.")
						SET_ENTITY_AS_MISSION_ENTITY(vehPlayer, FALSE, TRUE)
						IF NETWORK_HAS_CONTROL_OF_ENTITY(vehPlayer)
							SET_NETWORK_ID_EXISTS_ON_ALL_MACHINES(VEH_TO_NET(vehPlayer), TRUE)
						ENDIF
						CLEAR_BIT(iLocalboolCheck25, LBOOL25_DRIVER_DIED_OR_RESPAWNED_INTO_NEW_VEH)
					ELSE
						PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC] - FM_MISSION_CONTROLLER already owns this.")
					ENDIF
				ENDIF
				
				// Clean Up Old vehicle.
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitSet3, ciBS3_ENABLE_OLD_TEAM_VEHICLE_CLEANUP)
				OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitSet3, ciBS3_ENABLE_OLD_TEAM_VEHICLE_DELETION)
					
					IF DOES_ENTITY_EXIST(vehOldTeamMultiRespawnVehicle)
						PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC] - vehOldTeamMultiRespawnVehicle exists ", NETWORK_ENTITY_GET_OBJECT_ID(vehOldTeamMultiRespawnVehicle))
						IF IS_ENTITY_A_MISSION_ENTITY(vehOldTeamMultiRespawnVehicle)
							PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC] - Is a mission Entity")
							IF GET_VEHICLE_PED_IS_IN(localPlayerPed) != vehOldTeamMultiRespawnVehicle
								PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC] - Not the vehicle player is in")
								IF IS_ENTITY_ALIVE(vehOldTeamMultiRespawnVehicle)
									PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC] - Vehicle is alive")
									IF GET_VEHICLE_NUMBER_OF_PASSENGERS(vehOldTeamMultiRespawnVehicle) <= 0
										PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC] - No passengers")
										IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(vehOldTeamMultiRespawnVehicle)
											PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC] - Belongs to Mission Controller")
											IF NETWORK_HAS_CONTROL_OF_ENTITY(vehOldTeamMultiRespawnVehicle)
												PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC] - We have control of the entity")
												IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitSet3, ciBS3_ENABLE_OLD_TEAM_VEHICLE_DELETION)
													PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC] - Deleting vehicle (vehOldTeamMultiRespawnVehicle): ", NATIVE_TO_INT(vehOldTeamMultiRespawnVehicle))
													
													SET_ENTITY_AS_MISSION_ENTITY(vehOldTeamMultiRespawnVehicle, FALSE, TRUE)
													
													IF IS_ENTITY_A_MISSION_ENTITY(vehOldTeamMultiRespawnVehicle)
														IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(vehOldTeamMultiRespawnVehicle, FALSE)
															PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC] - CALLING DELETION! (vehOldTeamMultiRespawnVehicle): ", NATIVE_TO_INT(vehOldTeamMultiRespawnVehicle))
															DELETE_VEHICLE(vehOldTeamMultiRespawnVehicle)
														ELSE
															PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC] - Deletion failed (1) (vehOldTeamMultiRespawnVehicle): ", NATIVE_TO_INT(vehOldTeamMultiRespawnVehicle))
														ENDIF
													ELSE
														PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC] - Deletion failed (2) (vehOldTeamMultiRespawnVehicle): ", NATIVE_TO_INT(vehOldTeamMultiRespawnVehicle))
													ENDIF
												ELIF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitSet3, ciBS3_ENABLE_OLD_TEAM_VEHICLE_CLEANUP)
													PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC] - cleaning up vehicle (vehOldTeamMultiRespawnVehicle): ", NATIVE_TO_INT(vehOldTeamMultiRespawnVehicle))
													SET_VEHICLE_AS_NO_LONGER_NEEDED(vehOldTeamMultiRespawnVehicle)
												ENDIF
											ELSE
												NETWORK_REQUEST_CONTROL_OF_ENTITY(vehOldTeamMultiRespawnVehicle)
												PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC] - requesting control (vehOldTeamMultiRespawnVehicle): ", NATIVE_TO_INT(vehOldTeamMultiRespawnVehicle))
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF IS_PED_IN_ANY_VEHICLE(localPlayerPed)
							vehOldTeamMultiRespawnVehicle = GET_VEHICLE_PED_IS_IN(localPlayerPed)
							PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC] - Assigned new vehicle (vehOldTeamMultiRespawnVehicle): ", NATIVE_TO_INT(vehOldTeamMultiRespawnVehicle))
						ENDIF
					ENDIF
				ENDIF	
			ENDIF 
			IF IS_BIT_SET(MC_PlayerBD[iLocalPart].iManualRespawnBitset, ciBS_MAN_RESPAWN_FORCED)
				BOOl bShouldClear = TRUE
				INT i = 0 
				FOR i = 0 TO MAX_VEHICLE_PARTNERS-1
					IF GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(i) > -1
					AND NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(i)))
						IF NOT IS_PART_IN_SAME_VEHICLE_AS_PART(iLocalPart, GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(i))				
							PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC] - Everyone has returned to the vehicle. Cleaing: ciBS_MAN_RESPAWN_FORCED")
							bShouldClear = FALSE
						ENDIF
					ENDIF
				ENDFOR
				
				If bShouldClear
					CLEAR_BIT(MC_PlayerBD[iLocalPart].iManualRespawnBitset, ciBS_MAN_RESPAWN_FORCED)
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_PLAYER_RESPAWNING(LocalPlayer)
			IF bLocalPlayerPedOK
				SET_PED_RESET_FLAG(LocalPlayerPed, PRF_SuppressInAirEvent, TRUE)
				IF CAN_PED_RAGDOLL(localPlayerPed)
					SET_PED_CAN_RAGDOLL(localPlayerPed, FALSE)
				ENDIF
			ENDIF
		ELSE
			IF bLocalPlayerPedOK
				IF NOT CAN_PED_RAGDOLL(localPlayerPed)
					SET_PED_CAN_RAGDOLL(localPlayerPed, TRUE)
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitSet3, ciBS3_ENABLE_TEAM_VEHICLE_PASS_PRIORITIZE_WEAPON_SEATS)
		AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[ iLocalPart ].iteam].iTeamBitSet3, ciBS3_ENABLE_CHOOSE_SEAT_PREFERENCE_IN_CORONA)
			IF GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(GET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER()) > -1
				IF bLocalPlayerPedOK
				AND INT_TO_ENUM(VEHICLE_SEAT, GET_PLAYER_RESPAWN_VEHICLE_SEAT_PREFERENCE()) != VS_DRIVER
				AND NOT IS_PLAYER_RESPAWNING(LocalPlayer)
					IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
						VEHICLE_SEAT vehTurretSeat = GET_SEAT_PED_IS_IN(localPlayerPed)
						VEHICLE_INDEX vehPlayer = GET_VEHICLE_PED_IS_IN(localPlayerPed)
						
						IF NOT IS_TURRET_SEAT(vehPlayer, vehTurretSeat)
							IF IS_PART_IN_SAME_VEHICLE_AS_PART(iLocalPart, GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(GET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER()))
								IF GET_FREE_TURRET_VEHICLE_SEAT(vehPlayer, vehTurretSeat)
									FORCE_PED_AI_AND_ANIMATION_UPDATE(localPlayerPed)
									SET_ENTITY_COLLISION(localPlayerPed, TRUE)	
									FREEZE_ENTITY_POSITION(localPlayerPed, FALSE)	
									SET_PED_CAN_RAGDOLL(localPlayerPed, FALSE)
									SET_PED_RESET_FLAG(localPlayerPed, PRF_SuppressInAirEvent, TRUE)
									TASK_ENTER_VEHICLE(LocalPlayerPed, vehPlayer, 1, vehTurretSeat, DEFAULT, ECF_WARP_PED)
									SET_PED_RESET_FLAG(localPlayerPed, PRF_SuppressInAirEvent, TRUE)
									PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC] - There is a free turret seat. Repositioning player to that one.")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
		
	IF NOT IS_PLAYER_RESPAWNING(localPlayer)
	AND bLocalPlayerPedOK
	AND IS_PED_IN_ANY_VEHICLE(localPlayerPEd)
		IF IS_BIT_SET(iLocalBoolCheck25, LBOOL25_INITIALIZED_SEATING_DATA_FOR_MULTI_VEH)
			IF NOT IS_BIT_SET(iLocalBoolCheck25, LBOOL25_SAVED_ACTUAL_VEHICLE_SEAT_TO_MULTI_VEH_DATA)
				CACHE_VEHICLE_SEATING_PREFERENCES_FOR_MULTI_VEH()
				SET_BIT(iLocalBoolCheck25, LBOOL25_SAVED_ACTUAL_VEHICLE_SEAT_TO_MULTI_VEH_DATA)
			ENDIF
		ENDIF
	ENDIF
	
	BOOL bCooldown = FALSE
	
	IF HAS_NET_TIMER_STARTED(tdVehicleSeatSwapCooldownTimer)
		IF NOT HAS_NET_TIMER_EXPIRED_READ_ONLY(tdVehicleSeatSwapCooldownTimer, ci_VEH_SEAT_SWAP_COOLDOWN_TIME)
			bCooldown = TRUE
		ELSE
			RESET_NET_TIMER(tdVehicleSeatSwapCooldownTimer)
		ENDIF
	ENDIF
		
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iTeamBitSet3, ciBS3_ENABLE_TEAM_VEHICLE_ALLOW_VEHICLE_SEAT_SWAPPING)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HEADLIGHT, TRUE)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SHUFFLE, TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iTeamBitSet3, ciBS3_ENABLE_TEAM_VEHICLE_ALLOW_VEHICLE_SEAT_SWAPPING)
	AND NOT bCooldown		
		IF NOT IS_PLAYER_RESPAWNING(localPlayer)
		AND bLocalPlayerPedOK
			IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
				IF GET_NUM_PEDS_IN_VEHICLE(GET_VEHICLE_PED_IS_IN(LocalPlayerPed)) < 2 
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HELP_TUR")
					OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("STOC_HELP_1")
						HIDE_HELP_TEXT_THIS_FRAME()
					ENDIF
				ENDIF
			ENDIF
			IF NOT IS_MULTI_TEAM_VEHICLE_SEAT_SWAP_IN_PROGRESS()
				IF IS_DISABLED_CONTROL_HELD(INPUT_VEH_HEADLIGHT, 1000) // Right d-pad.
				AND IS_OK_TO_MULTI_TEAM_VEHICLE_SEAT_SWAP()
					PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC][SWAP_SEAT] - We want to make a vehicle seat switch!!")
						
					// If button pressed, send the event:
					SET_BIT(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_INITIATED_VEHICLE_SEAT_SWAP)
					BROADCAST_FMMC_INITIATE_SEAT_SWAP_TEAM_VEHICLE(GlobalplayerBD[iLocalPart].iVehiclePartners)
				ENDIF
			ENDIF
			
			// Swap Seat on Right-dpad press starts here....
		
			IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_INITIATED_VEHICLE_SEAT_SWAP)
				
				BOOL bGaveConsent = TRUE
				
				// Option, ask for consent.
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iTeamBitSet3, ciBS3_ENABLE_TEAM_VEHICLE_SWAP_REQUIRES_CONSENT)
										
					// If Participants are still on the first stage, then we are not ready!
					INT i = 0 
					FOR i = 0 TO MAX_VEHICLE_PARTNERS-1
						IF GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(i) > -1
						AND NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(i)))
							IF NOT IS_BIT_SET(MC_playerBD[GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(i)].iClientBitSet3, PBBOOL3_DATA_CHANGING_SEAT_GAVE_CONSENT)
								bGaveConsent = FALSE
								
								PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC][SWAP_SEAT] - Waiting for Vehicle Partner: ", i, " PartNumber: ", GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(i), " To give consent in vehicle seat swap.")
							ELSE
								PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC][SWAP_SEAT] - Vehicle Partner: ", i, " PartNumber: ", GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(i), " has given consent")
							ENDIF
						ENDIF
					ENDFOR
					
					IF NOT HAS_NET_TIMER_STARTED(tdVehicleSeatSwapConsentTimer)
						START_NET_TIMER(tdVehicleSeatSwapConsentTimer)
					ELSE
						IF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdVehicleSeatSwapConsentTimer, ci_VEH_SEAT_SWAP_CONSENT_TIME)
							IF NOT HAS_NET_TIMER_STARTED(tdVehicleSeatSwapSafetyTimer)
							OR (HAS_NET_TIMER_STARTED(tdVehicleSeatSwapSafetyTimer) AND HAS_NET_TIMER_EXPIRED_READ_ONLY(tdVehicleSeatSwapSafetyTimer, ci_VEH_SEAT_SWAP_SAFETY_TIME))							
								IF NOT bGaveConsent
									PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC][SWAP_SEAT] - Consent Timer Expired. Resetting.")
									DO_SCREEN_FADE_IN(250)
									CLEANUP_MULTI_VEHICLE_SEAT_SWAP()
									EXIT
								ENDIf
							ENDIF						
						ENDIF
					ENDIF
					
					IF HAS_NET_TIMER_STARTED(tdVehicleSeatSwapConsentTimer)
						IF NOT HAS_NET_TIMER_EXPIRED_READ_ONLY(tdVehicleSeatSwapConsentTimer, ci_VEH_SEAT_SWAP_CONSENT_TIME)
							IF IS_DISABLED_CONTROL_HELD(INPUT_VEH_HEADLIGHT, 1000) // Right d-pad.
							AND IS_OK_TO_MULTI_TEAM_VEHICLE_SEAT_SWAP()
								IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_DATA_CHANGING_SEAT_GAVE_CONSENT)
									SET_BIT(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_DATA_CHANGING_SEAT_GAVE_CONSENT)
									g_bMissionSeatSwapping = TRUE
									PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC][SWAP_SEAT] - I have just given consent.")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_DATA_CHANGING_SEAT_GAVE_CONSENT)
					AND bGaveConsent
						IF NOT HAS_NET_TIMER_STARTED(tdVehicleSeatSwapSafetyTimer)
							START_NET_TIMER(tdVehicleSeatSwapSafetyTimer)
							PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC][SWAP_SEAT] - Starting safety sync timer for consent.")	
						ENDIF
					ENDIF
				ENDIF
				
				IF (bGaveConsent
				AND IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_DATA_CHANGING_SEAT_GAVE_CONSENT))
				OR NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iTeamBitSet3, ciBS3_ENABLE_TEAM_VEHICLE_SWAP_REQUIRES_CONSENT)
					
					IF IS_SCREEN_FADED_OUT()
						PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC][SWAP_SEAT] - Screen Has faded...")
						
						// This check makes sure that the Driver leaves the vehicle first.
						BOOL bCanProceed						
						IF IS_PED_IN_ANY_VEHICLE(localPlayerPed)
							VEHICLE_SEAT vehSeat = INT_TO_ENUM(VEHICLE_SEAT, GET_SEAT_PED_IS_IN(localPlayerPed, TRUE))
							IF IS_VEHICLE_SEAT_FREE(GET_VEHICLE_PED_IS_IN(localPlayerPed), VS_DRIVER, TRUE)
							OR vehSeat = VS_DRIVER
								bCanProceed = TRUE
							ENDIF
						ELSE 
							bCanProceed = TRUE
						ENDIF
						
						IF bCanProceed
							IF IS_PED_IN_ANY_VEHICLE(localPlayerPed)							
								vehOurMultiSwitchVehicle = GET_VEHICLE_PED_IS_IN(localPlayerPed)
								SET_ENTITY_INVINCIBLE(localPlayerPed, TRUE)
								SET_ENTITY_VISIBLE(localPlayerPed, FALSE)
								CLEAR_PED_TASKS_IMMEDIATELY(localPlayerPed)
								PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC][SWAP_SEAT] - Clearing tasks and caching vehicle we need to get back in.")
							ELSE
								IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_DATA_CHANGE_READY_TO_MOVE_VEHICLE)
									SET_BIT(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_DATA_CHANGE_READY_TO_MOVE_VEHICLE)
									CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_INITIATED_VEHICLE_SEAT_SWAP)
									SET_ENTITY_COLLISION(localPlayerPed, FALSE)
									PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC][SWAP_SEAT] - Data has finished being set, Left old vehicle. Clearing bit and getting ready to make a move! changing to next stage. (PBBOOL3_DATA_CHANGE_READY_TO_MOVE_VEHICLE)")
								ENDIF
							ENDIF
						ENDIF
					ELIF NOT IS_SCREEN_FADING_OUT()
						PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC][SWAP_SEAT] - Fading Screen Out to hide the process...")
						SET_ENTITY_INVINCIBLE(localPlayerPed, TRUE)
						SET_ENTITY_VISIBLE(localPlayerPed, FALSE)
						DO_SCREEN_FADE_OUT(250)
					ENDIF
				ELSE
					IF HAS_NET_TIMER_STARTED(tdVehicleSeatSwapSafetyTimer) 
						IF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdVehicleSeatSwapSafetyTimer, ci_VEH_SEAT_SWAP_SAFETY_TIME)
						AND HAS_NET_TIMER_EXPIRED_READ_ONLY(tdVehicleSeatSwapConsentTimer, ci_VEH_SEAT_SWAP_CONSENT_TIME)
							PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC][SWAP_SEAT] - Cleaning Up. Someone went out of Sync.")						
							DO_SCREEN_FADE_IN(250)
							CLEANUP_MULTI_VEHICLE_SEAT_SWAP()
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_DATA_CHANGE_READY_TO_MOVE_VEHICLE)
				BOOL bReady = TRUE
				
				// If Participants are still on the first stage, then we are not ready!
				INT i = 0 
				FOR i = 0 TO MAX_VEHICLE_PARTNERS-1
					IF GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(i) > -1
					AND NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(i)))
						IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_DATA_CHANGE_READY_TO_MOVE_VEHICLE)
							bReady = FALSE
							PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC][SWAP_SEAT] - Not quite ready to make the seat swap. We are still waiting for a data change on Vehicle Partner: ", i, " PartNumber: ", GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(i))
						ENDIF
					ENDIF
				ENDFOR
							
				IF bReady
					IF SHUFFLE_MULTIPLE_PLAYER_SAME_VEHICLE_DATA_CYCLE_SEATS()
						CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_DATA_CHANGE_READY_TO_MOVE_VEHICLE)
						SET_BIT(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_DATA_CHANGING_SEAT_FINAL)
						PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC][SWAP_SEAT] - changing to next stage. (PBBOOL3_DATA_CHANGING_SEAT_FINAL)")
					ENDIF
				ENDIF				
			ENDIF
			
			IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_DATA_CHANGING_SEAT_DONE)
				BOOL bDoneMultiVehSeatSwap = TRUE
					
				INT i = 0 
				FOR i = 0 TO MAX_VEHICLE_PARTNERS-1
					IF GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(i) > -1
					AND NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(i)))
						IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_DATA_CHANGING_SEAT_DONE)
							bDoneMultiVehSeatSwap = FALSE
								
							PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC][SWAP_SEAT] - We are in our new seat and just waiting for Vehicle Partner: ", i, " PartNumber: ", GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(i)," to get to also enter theirs before cleaning this up.")
						ENDIF
					ENDIF
				ENDFOR
				
				IF bDoneMultiVehSeatSwap
					CACHE_VEHICLE_SEATING_PREFERENCES_FOR_MULTI_VEH()
					PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC][SWAP_SEAT] - Success!")
					DO_SCREEN_FADE_IN(250)
					CLEANUP_MULTI_VEHICLE_SEAT_SWAP()
				ENDIF
			ELIF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_DATA_CHANGING_SEAT_FINAL)
				BOOL bReady = TRUE
				
				INT i = 0 
				FOR i = 0 TO MAX_VEHICLE_PARTNERS-1
					IF GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(i) > -1
					AND NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(i)))
						IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_DATA_CHANGING_SEAT_FINAL)
							bReady = FALSE
							
							PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC][SWAP_SEAT] - Not quite ready to make the seat swap. We are still waiting for Vehicle Partner: ", i, " PartNumber: ", GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(i), " to get to the final stage.")
						ENDIF
					ENDIF
				ENDFOR
				
				IF bReady
					IF iCachedNewVehSeatPref > -3
					AND IS_ENTITY_ALIVE(vehOurMultiSwitchVehicle)
						VEHICLE_SEAT vehSeat = INT_TO_ENUM(VEHICLE_SEAT, iCachedNewVehSeatPref)
						
						IF NOT IS_VEHICLE_SEAT_FREE(vehOurMultiSwitchVehicle, VS_DRIVER, TRUE)
						OR vehSeat = VS_DRIVER							
							IF NOT IS_PED_IN_ANY_VEHICLE(localPlayerPed, TRUE)
								PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC][SWAP_SEAT] - Setting to enter vehicle in Vehicle Seat: ", iCachedNewVehSeatPref)
								CLEAR_PED_TASKS_IMMEDIATELY(localPlayerPed)
								SET_ENTITY_COLLISION(localPlayerPed, TRUE)
								FREEZE_ENTITY_POSITION(localPlayerPed, FALSE)
								SET_PED_CAN_RAGDOLL(localPlayerPed, FALSE)
								SET_PED_RESET_FLAG(localPlayerPed, PRF_SuppressInAirEvent, TRUE)
								TASK_ENTER_VEHICLE(LocalPlayerPed, vehOurMultiSwitchVehicle, 1, vehSeat, DEFAULT, ECF_WARP_PED)
								FORCE_PED_AI_AND_ANIMATION_UPDATE(localPlayerPed)
								SET_PED_RESET_FLAG(localPlayerPed, PRF_SuppressInAirEvent, TRUE)						
							ENDIF
							
							IF IS_PED_IN_ANY_VEHICLE(localPlayerPed, FALSE)
							AND GET_SEAT_PED_IS_IN(LocalPlayerPed, FALSE) = vehSeat
								PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC][SWAP_SEAT] - Ped is now back in the vehicle and in their new seat, changing to next stage. (PBBOOL3_DATA_CHANGING_SEAT_DONE). In Seat: ", ENUM_TO_INT(GET_SEAT_PED_IS_IN(LocalPlayerPed, FALSE)))
								SET_BIT(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_DATA_CHANGING_SEAT_DONE)
							ELSE
								IF IS_PED_IN_ANY_VEHICLE(localPlayerPed)
									IF GET_SEAT_PED_IS_IN(LocalPlayerPed, FALSE) != vehSeat
										CLEAR_PED_TASKS_IMMEDIATELY(localPlayerPed)
										PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC][SWAP_SEAT] - SOMEHOW in the wrong seat. Exiting... ")
									ENDIF
								ENDIF
							ENDIF
						ELSE
							PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC][SWAP_SEAT] - Waiting for Driver to get in first...")
						ENDIF
					ELSE
						PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC][SWAP_SEAT] - We got here with iCachedNewVehSeatPref: ", iCachedNewVehSeatPref, " Or the vehicle died... Something went terribly wrong.")
						DO_SCREEN_FADE_IN(250)
						CLEANUP_MULTI_VEHICLE_SEAT_SWAP()
					ENDIF
				ENDIF
			ENDIF
		ELSE
			// cleanup Player Dead.		
			IF IS_MULTI_TEAM_VEHICLE_SEAT_SWAP_IN_PROGRESS()
				IF !bLocalPlayerPedOK
				OR IS_PLAYER_RESPAWNING(localPlayer)
					PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC][SWAP_SEAT] - Emergancy cleanup. Something went terribly wrong.. (player Dead)")
					DO_SCREEN_FADE_IN(250)
					CLEANUP_MULTI_VEHICLE_SEAT_SWAP()
				ENDIF
			ENDIF
			
			// cleanup Veh Destroyed.
			IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_DATA_CHANGING_SEAT_DONE)
				IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_DATA_CHANGING_SEAT_FINAL)
				OR IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_DATA_CHANGE_READY_TO_MOVE_VEHICLE)
				OR IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_DATA_CHANGING_SEAT_FINAL)
					IF NOT DOES_ENTITY_EXIST(vehOurMultiSwitchVehicle)
					OR NOT IS_VEHICLE_DRIVEABLE(vehOurMultiSwitchVehicle)
						PRINTLN("[LM][MULTI_VEH_RESPAWN][PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC][SWAP_SEAT] - Emergancy cleanup. Something went terribly wrong.. (Vehicle Destroyed)")
						DO_SCREEN_FADE_IN(250)
						CLEANUP_MULTI_VEHICLE_SEAT_SWAP()
					ENDIF
				ENDIF
			ENDIF
			
			// Cleanup Loop....
			// If someone has set this BD, then we seriously need to clean this up and reset the array pointers.
			
		ENDIF
		//Timeout??
		// Swap Seat Ends here.
	ENDIF
ENDPROC

PROC DISABLE_RUINER_ROCKETS()

	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciDISABLE_RUINER_ROCKETS)
		EXIT
	ENDIF
	
	VEHICLE_INDEX vehPlayer
	IF bLocalPlayerPedOK
		IF IS_PED_IN_ANY_VEHICLE( LocalPlayerPed )
			vehPlayer = GET_VEHICLE_PED_IS_IN( LocalPlayerPed )
		ELSE
			IF NETWORK_DOES_NETWORK_ID_EXIST(netRespawnVehicle)
				vehPlayer = NET_TO_VEH(netRespawnVehicle)
			ENDIF
		ENDIF
		
		IF IS_ENTITY_ALIVE(vehPlayer)
			IF NETWORK_HAS_CONTROL_OF_ENTITY(vehPlayer)
			AND IS_VEHICLE_MODEL(vehPlayer, RUINER2)
				CPRINTLN( DEBUG_CONTROLLER,"[RCC MISSION][DISABLE_RUINER_ROCKETS] - DISABLED")
				SET_PED_CAN_SWITCH_WEAPON(LocalPlayerPed, FALSE) 
				DISABLE_VEHICLE_WEAPON(TRUE,WEAPONTYPE_DLC_VEHICLE_RUINER_ROCKET, vehPlayer, LocalPlayerPed)
				SET_CURRENT_PED_VEHICLE_WEAPON(LocalPlayerPed, WEAPONTYPE_DLC_VEHICLE_RUINER_BULLET)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

// Rockets and Homing Rockets by default share ammo... So we have to fudge it.
PROC PROCESS_CURRENT_RUINER_ROCKET_AMMO()
	IF IS_BIT_SET(iLocalBoolCheck22, LBOOL22_USING_CUSTOM_VEHICLE_AMMO)
		IF bLocalPlayerPedOK
		AND IS_PED_IN_ANY_VEHICLE(localPlayerPed)
			
			VEHICLE_INDEX playerVeh = GET_VEHICLE_PED_IS_IN(localPlayerPed)
			WEAPON_TYPE wtCurrentRuinerWeapon
			
			IF GET_CURRENT_PED_VEHICLE_WEAPON(localPlayerPed, wtCurrentRuinerWeapon)
				
				IF wtCurrentRuinerWeapon = WEAPONTYPE_DLC_VEHICLE_RUINER_ROCKET
				AND IS_PLAYER_VEHICLE_WEAPON_TOGGLED_TO_NON_HOMING(LocalPlayer)
					IF NOT IS_BIT_SET(iLocalBoolCheck22, LBOOL22_SWITCHED_ROCKET_TYPE)
						SET_BIT(iLocalBoolCheck22, LBOOL22_SWITCHED_ROCKET_TYPE)
						
						SET_BIT(iLocalBoolCheck22, LBOOL22_SWITCHED_ROCKET_AMMO)
						
						PRINTLN("[LM][MISSION][PROCESS_CURRENT_RUINER_ROCKET_AMMO] - (Bitset) Rockets Switched To Regular.")
					ENDIF
				ELIF wtCurrentRuinerWeapon = WEAPONTYPE_DLC_VEHICLE_RUINER_ROCKET
				AND NOT IS_PLAYER_VEHICLE_WEAPON_TOGGLED_TO_NON_HOMING(LocalPlayer)
					IF IS_BIT_SET(iLocalBoolCheck22, LBOOL22_SWITCHED_ROCKET_TYPE)
						CLEAR_BIT(iLocalBoolCheck22, LBOOL22_SWITCHED_ROCKET_TYPE)
						
						SET_BIT(iLocalBoolCheck22, LBOOL22_SWITCHED_ROCKET_AMMO)
						
						PRINTLN("[LM][MISSION][PROCESS_CURRENT_RUINER_ROCKET_AMMO] - (Bitset) Rockets Switched To Homing.")
					ENDIF			
				ENDIF
				
				IF wtCurrentRuinerWeaponCache != wtCurrentRuinerWeapon
				OR IS_BIT_SET(iLocalBoolCheck22, LBOOL22_SWITCHED_ROCKET_AMMO)
				
					wtCurrentRuinerWeaponCache = wtCurrentRuinerWeapon
					
					IF wtCurrentRuinerWeapon = WEAPONTYPE_DLC_VEHICLE_RUINER_BULLET
						IF iRuinerMGAmmo > -1
							iRuinerRocketsAmmo = GET_VEHICLE_WEAPON_RESTRICTED_AMMO(playerVeh, 1)
							
							SET_VEHICLE_WEAPON_RESTRICTED_AMMO(playerVeh, 0, iRuinerMGAmmo)							
						ENDIF
						PRINTLN("[LM][MISSION][PROCESS_CURRENT_RUINER_ROCKET_AMMO] - Weapon Switched to MG")
						
					ELIF wtCurrentRuinerWeapon = WEAPONTYPE_DLC_VEHICLE_RUINER_ROCKET
					AND IS_PLAYER_VEHICLE_WEAPON_TOGGLED_TO_NON_HOMING(LocalPlayer)
						IF iRuinerRocketsAmmo > -1
							iRuinerRocketsHomingAmmo = GET_VEHICLE_WEAPON_RESTRICTED_AMMO(playerVeh, 1)
							
							SET_VEHICLE_WEAPON_RESTRICTED_AMMO(playerVeh, 1, iRuinerRocketsAmmo)
						ENDIF
						PRINTLN("[LM][MISSION][PROCESS_CURRENT_RUINER_ROCKET_AMMO] - Weapon Switched to Rockets")
						
					ELIF wtCurrentRuinerWeapon = WEAPONTYPE_DLC_VEHICLE_RUINER_ROCKET
					AND NOT IS_PLAYER_VEHICLE_WEAPON_TOGGLED_TO_NON_HOMING(LocalPlayer)
						IF iRuinerRocketsHomingAmmo > -1
							iRuinerMGAmmo = GET_VEHICLE_WEAPON_RESTRICTED_AMMO(playerVeh, 0)
							
							SET_VEHICLE_WEAPON_RESTRICTED_AMMO(playerVeh, 1, iRuinerRocketsHomingAmmo)
						ENDIF
						PRINTLN("[LM][MISSION][PROCESS_CURRENT_RUINER_ROCKET_AMMO] - Weapon Switched to Homing")
						
					ENDIF
					
					CLEAR_BIT(iLocalBoolCheck22, LBOOL22_SWITCHED_ROCKET_AMMO)
					
					PRINTLN("[LM][MISSION][PROCESS_CURRENT_RUINER_ROCKET_AMMO] - Weapon Switch. Ammos - MG: ", iRuinerMGAmmo, " Rockets: ", iRuinerRocketsAmmo, " Homing Rockets: ", iRuinerRocketsHomingAmmo)					
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_UPDATE_SPECIAL_VEHICLE_WEAPONS()
	INT iTeam = MC_PlayerBD[iLocalPart].iteam
	INT iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]
		
	IF iTeam < FMMC_MAX_TEAMS
	AND iRule < FMMC_MAX_RULES
		
		IF IS_BIT_SET(iLocalBoolCheck6, (LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + MC_playerBD[iLocalPart].iteam))
		OR !bLocalPlayerPedOK
			IF NOT IS_BIT_SET(iLocalBoolCheck22, LBOOL22_VEHICLE_AMMO_RELOAD_ENGAGED)
				PRINTLN("[LM][MISSION][PROCESS_UPDATE_SPECIAL_VEHICLE_WEAPONS] - Rule Changed. Setting vehicle Ammo Reload engaged. LBOOL22_VEHICLE_AMMO_RELOAD_ENGAGED")
				SET_BIT(iLocalBoolCheck22, LBOOL22_VEHICLE_AMMO_RELOAD_ENGAGED)				
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(iLocalBoolCheck22, LBOOL22_VEHICLE_AMMO_RELOAD_ENGAGED)
			IF bLocalPlayerPedOK
			AND IS_PED_IN_ANY_VEHICLE(localPlayerPed)
			
				VEHICLE_INDEX playerVeh = GET_VEHICLE_PED_IS_IN(localPlayerPed)
				WEAPON_TYPE wtCurrentRuinerWeapon
				
				IF GET_CURRENT_PED_VEHICLE_WEAPON(localPlayerPed, wtCurrentRuinerWeapon)
					IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iSVWAmmoMG[iRule] > -1
						IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEight[iRule], ciBS_RULE8_SV_WEAPON_RELOAD_ON_RULE_MG)
							IF iRuinerMGAmmo = -1
								iRuinerMGAmmo++
							ENDIF
							iRuinerMGAmmo += g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iSVWAmmoMG[iRule]
						ELSE
							iRuinerMGAmmo = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iSVWAmmoMG[iRule]						
						ENDIF
						iRuinerMGAmmoMax = iRuinerMGAmmo
					ENDIF
					
					IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iSVWAmmoRockets[iRule] > -1
						IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEight[iRule], ciBS_RULE8_SV_WEAPON_RELOAD_ON_RULE_ROCKETS)
							IF iRuinerRocketsAmmo = -1
								iRuinerRocketsAmmo++
							ENDIF
							iRuinerRocketsAmmo += g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iSVWAmmoRockets[iRule]
						ELSE
							iRuinerRocketsAmmo = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iSVWAmmoRockets[iRule]
						ENDIF
						iRuinerRocketsAmmoMax = iRuinerRocketsAmmo
					ENDIF
					
					IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iSVWAmmoHRockets[iRule] > -1
						IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEight[iRule], ciBS_RULE8_SV_WEAPON_RELOAD_ON_RULE_HOMING_ROCKETS)
							IF iRuinerRocketsHomingAmmo = -1
								iRuinerRocketsHomingAmmo++
							ENDIF
							iRuinerRocketsHomingAmmo += g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iSVWAmmoHRockets[iRule]
						ELSE
							iRuinerRocketsHomingAmmo = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iSVWAmmoHRockets[iRule]
						ENDIF
						iRuinerRocketsHomingAmmoMax = iRuinerRocketsHomingAmmo
					ENDIF					
					
					IF iRuinerMGAmmo > -1
						SET_VEHICLE_WEAPON_RESTRICTED_AMMO(playerVeh, 0, iRuinerMGAmmo)							
					ENDIF					
					IF iRuinerRocketsHomingAmmo > -1
						SET_VEHICLE_WEAPON_RESTRICTED_AMMO(playerVeh, 1, iRuinerRocketsHomingAmmo)
					ENDIF
					
					SET_BIT(iLocalBoolCheck22, LBOOL22_USING_CUSTOM_VEHICLE_AMMO)
					CLEAR_BIT(iLocalBoolCheck22, LBOOL22_VEHICLE_AMMO_RELOAD_ENGAGED)
					PRINTLN("[LM][MISSION][PROCESS_UPDATE_SPECIAL_VEHICLE_WEAPONS] - (Cached) Weapon Switch. Ammos - MG: ", iRuinerMGAmmo, " Rockets: ", iRuinerRocketsAmmo, " Homing Rockets: ", iRuinerRocketsHomingAmmo)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_VEHICLES_FLIPPED_BY_DUNE()

	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_DUNE_EXPLOSIONS)
		EXIT
	ENDIF
	
	INT i
	FLOAT fTime, fVelocity
	FOR i = 0 TO DUNE_MAX_FLIPS -1
		IF viFlippedVeh[i] != NULL
			IF HAS_NET_TIMER_STARTED(stFlipTimer[i])
				IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stFlipTimer[i]) > 5000
					viFlippedVeh[i] = NULL
					RESET_NET_TIMER(stFlipTimer[i])
					CLEAR_BIT(iLocalVehicleExplosionBitSet, (ciVeh_up_0+i))
					PRINTLN("[JT FLIP] timer over 5 seconds, clearing vehicle and timer arrays")
					BREAKLOOP
				ENDIF
			ENDIF
			IF IS_BIT_SET(iLocalVehicleExplosionBitSet, (ciVeh_up_0+i))
				IF IS_ENTITY_ALIVE(viFlippedVeh[i])
					IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stFlipTimer[i]) > 750
						IF NOT IS_ENTITY_IN_AIR(viFlippedVeh[i])
							
							IF GET_CREATOR_VEHICLE_LIBRARY_FROM_MODEL(GET_ENTITY_MODEL(viFlippedVeh[i])) != VEH_LIBRARY_MOTORCYCLES
								fTime = g_FMMC_STRUCT.fEasyExplosionTime
								fVelocity = g_FMMC_STRUCT.fEasyExplosionVelocity
								PRINTLN("CAR EET")
							ELSE
								fTime = g_FMMC_STRUCT.fEasyExplosionTimeBike
								fVelocity = g_FMMC_STRUCT.fEasyExplosionVelocityBike
								PRINTLN("BIKE EET")
							ENDIF
							
							IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stFlipTimer[i]) > (fTime * 1000)
							OR VMAG(GET_ENTITY_VELOCITY(viFlippedVeh[i])) > fVelocity
								IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(viFlippedVeh[i])
									PRINTLN("[JT FLIP] BOOM - REQUESTING CONTROL of Vehicle: ", i, " ID: ", NATIVE_TO_INT(viFlippedVeh[i]))
									NETWORK_REQUEST_CONTROL_OF_ENTITY(viFlippedVeh[i])
								ELSE
									PRINTLN("[JT FLIP] BOOM")
									PRINTLN("[JT FLIP] BOOM - Timer: ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stFlipTimer[i]), " Velocity: ", (VMAG(GET_ENTITY_VELOCITY(viFlippedVeh[i]))))
									PRINTLN("[JT FLIP] BOOM - Timer set: ", g_FMMC_STRUCT.fEasyExplosionTime * 1000, "Velocity: ", g_FMMC_STRUCT.fEasyExplosionVelocity)
									RESET_NET_TIMER(stFlipTimer[i])
									
									PRINTLN("[JT FLIP] BOOM - Timer ",i ," reset: ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stFlipTimer[i]))
									
									IF DOES_ENTITY_EXIST(viFlippedVeh[i])
										NETWORK_EXPLODE_VEHICLE(viFlippedVeh[i] , TRUE, TRUE)
									ENDIF
									viFlippedVeh[i] = NULL
									CLEAR_BIT(iLocalVehicleExplosionBitSet, (ciVeh_up_0+i))
									BREAKLOOP
								ENDIF
							ELSE
								PRINTLN("[JT FLIP] DUD - Resetting")
								PRINTLN("[JT FLIP] DUD - Timer: ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stFlipTimer[i]), " Velocity: ", (VMAG(GET_ENTITY_VELOCITY(viFlippedVeh[i]))))
								viFlippedVeh[i] = NULL
								RESET_NET_TIMER(stFlipTimer[i])
								CLEAR_BIT(iLocalVehicleExplosionBitSet, (ciVeh_up_0+i))
								PRINTLN("[JT FLIP] BOOM - Timer ",i ," reset: ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stFlipTimer[i]))
								BREAKLOOP
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF IS_ENTITY_ALIVE(viFlippedVeh[i])
					IF IS_ENTITY_IN_AIR(viFlippedVeh[i])
						REINIT_NET_TIMER(stFlipTimer[i])
						SET_BIT(iLocalVehicleExplosionBitSet, (ciVeh_up_0+i))
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

//[FMMC2020] All ruiner stuff should probably go somewhere else
PROC PROCESS_RUINER_PARACHUTE()
	IF bLocalPlayerPedOK
		IF IS_PED_IN_ANY_VEHICLE(localPlayerPed)
			VEHICLE_INDEX vehPlayer = GET_VEHICLE_PED_IS_IN(localPlayerPed)
			
			IF GET_ENTITY_MODEL(vehPlayer) = RUINER2
			AND NETWORK_HAS_CONTROL_OF_ENTITY(vehPlayer)			
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciENABLE_TEAM_COLOURED_PARACHUTES)
					SWITCH MC_playerBD[iLocalPart].iteam
						CASE 0
							iMyParachuteMC = 0
						BREAK
						CASE 1
							iMyParachuteMC = 1
						BREAK
						CASE 2
							iMyParachuteMC = 2
						BREAK
						CASE 3
							iMyParachuteMC = 3
						BREAK
					ENDSWITCH
					VEHICLE_SET_PARACHUTE_MODEL_OVERRIDE(vehPlayer, HASH("gr_Prop_GR_Para_S_01"))
					VEHICLE_SET_PARACHUTE_MODEL_TINT_INDEX(vehPlayer, iMyParachuteMC)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC STUNT_RACE_VEHICLE_DAMAGE_SCALE(VEHICLE_INDEX viVeh)
	IF DOES_ENTITY_EXIST(viVeh)
	AND NOT IS_ENTITY_DEAD(viVeh)
	AND NETWORK_HAS_CONTROL_OF_ENTITY(viVeh)
		IF GET_ENTITY_MODEL(viVeh) = SANCTUS 
		OR GET_ENTITY_MODEL(viVeh) = AVARUS
			SET_VEHICLE_DAMAGE_SCALE(viVeh, g_sMPTunables.fStuntBikerVehDamageScale)
			PRINTLN("[PROPS] STUNT_RACE_PREP, SET_VEHICLE_DAMAGE_SCALE - g_sMPTunables.fStuntBikerVehDamageScale = ", g_sMPTunables.fStuntBikerVehDamageScale)
		ELSE
			SET_VEHICLE_DAMAGE_SCALE(viVeh, g_sMPTunables.fStuntVehDamageScale)
			PRINTLN("[PROPS] STUNT_RACE_PREP, SET_VEHICLE_DAMAGE_SCALE - g_sMPTunables.fStuntVehDamageScale = ", g_sMPTunables.fStuntVehDamageScale)
		ENDIF
	ELSE
		PRINTLN("[PROPS] STUNT_RACE_PREP, SET_VEHICLE_DAMAGE_SCALE - DOES_ENTITY_EXIST")
	ENDIF
ENDPROC

PROC STORE_PV_PLAYER_IS_USING()
	
	IF IS_PLAYER_CONTROL_ON(LocalPlayer)
	AND GET_MC_SERVER_GAME_STATE() = GAME_STATE_RUNNING
	
		g_TransitionSessionNonResetVars.iHashOfLastUsedPVInMission = -1
		
		VEHICLE_INDEX VehicleID
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			VehicleID = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
		ELSE
			VehicleID = GET_VEHICLE_PED_IS_USING(LocalPlayerPed)
		ENDIF	
		IF NOT DOES_ENTITY_EXIST(VehicleID)
			VehicleID = GET_PLAYERS_LAST_VEHICLE()
		ENDIF
		
		IF DOES_ENTITY_EXIST(VehicleID)
		AND NOT IS_ENTITY_DEAD(VehicleID)
		AND IS_VEHICLE_A_PERSONAL_VEHICLE(VehicleID)
			
			// are we within 50m of the vehicle?
			IF (VDIST2(GET_PLAYER_COORDS(LocalPlayer), GET_ENTITY_COORDS(VehicleID)) < 2500.0)
			
				g_TransitionSessionNonResetVars.iHashOfLastUsedPVInMission = GET_HASH_OF_PERSONAL_VEHICLE(VehicleID)
				
				PRINTLN("STORE_PV_PLAYER_IS_USING - setting g_TransitionSessionNonResetVars.iHashOfLastUsedPVInMission = ", g_TransitionSessionNonResetVars.iHashOfLastUsedPVInMission )
			
			ELSE
				PRINTLN("STORE_PV_PLAYER_IS_USING - too far from pv " )
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
				IF NOT DOES_ENTITY_EXIST(VehicleID)
					PRINTLN("STORE_PV_PLAYER_IS_USING - entity not exist " )
				ELIF IS_ENTITY_DEAD(VehicleID)
					PRINTLN("STORE_PV_PLAYER_IS_USING - entity dead " )
				ELIF NOT IS_VEHICLE_A_PERSONAL_VEHICLE(VehicleID)
					PRINTLN("STORE_PV_PLAYER_IS_USING - bot a pv " )	
				ENDIF
			#ENDIF
		ENDIF

	ENDIF

ENDPROC

PROC PROCESS_AKULA_STEALTH_MODE_IN_MISSION()
	BOOL bInStealthMode = IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].iBSVehStealthMode, GLOBAL_BS_VEH_STEALTH_MODE_ON)
	PRINTLN("PROCESS_AKULA_STEALTH_MODE_IN_MISSION - bInStealthMode: ", bInStealthMode)
	
	VEHICLE_INDEX viAkula
	
	IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
	
		viAkula = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
		
		IF GET_ENTITY_MODEL(viAkula) = AKULA
			IF IS_FAKE_MULTIPLAYER_MODE_SET()
				bInStealthMode =  NOT ARE_FOLDING_WINGS_DEPLOYED(viAkula)
				PRINTLN("PROCESS_AKULA_STEALTH_MODE_IN_MISSION - IS_FAKE_MULTIPLAYER_MODE_SET - bInStealthMode: ", bInStealthMode)
			ENDIF
			
			IF GET_PLAYER_WANTED_LEVEL(LocalPlayer) > 0
				INT	ivehID = IS_VEH_A_MISSION_CREATOR_VEH(viAkula)
				IF iVehID > -1 
				AND iVehID < FMMC_MAX_VEHICLES
				AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[ivehID].iAkulaTimeToResetWantedLvl > 0
					IF ARE_PLAYER_STARS_GREYED_OUT(LocalPlayer)
					OR  ARE_PLAYER_FLASHING_STARS_ABOUT_TO_DROP(LocalPlayer)
						IF HAS_NET_TIMER_STARTED(timerResetWantedLevelInStealth)
							IF HAS_NET_TIMER_EXPIRED(timerResetWantedLevelInStealth, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[ivehID].iAkulaTimeToResetWantedLvl)
								RESET_NET_TIMER(timerResetWantedLevelInStealth)
								IF bInStealthMode
									//Reset Wanted Level
									CLEAR_PLAYER_WANTED_LEVEL(LocalPlayer)
									PRINTLN("PROCESS_AKULA_STEALTH_MODE_IN_MISSION - Reset Wanted Level")
								ENDIF
							ELSE 
								SUPPRESS_LOSING_WANTED_LEVEL_IF_HIDDEN_THIS_FRAME(LocalPlayer)
								IF bInStealthMode
									PRINTLN("PROCESS_AKULA_STEALTH_MODE_IN_MISSION - ALLOW_EVASION_HUD_IF_DISABLING_HIDDEN_EVASION_THIS_FRAME")
									ALLOW_EVASION_HUD_IF_DISABLING_HIDDEN_EVASION_THIS_FRAME(LocalPlayer, 1)
								ENDIF
							ENDIF
						ENDIF
					ELSE
						//Reset and Start Timer
						IF HAS_NET_TIMER_STARTED(timerResetWantedLevelInStealth)
							RESET_NET_TIMER(timerResetWantedLevelInStealth)
						ENDIF
						START_NET_TIMER(timerResetWantedLevelInStealth)
						IF bInStealthMode
							PRINTLN("PROCESS_AKULA_STEALTH_MODE_IN_MISSION - Start Timer to Reset the Wanted Level: ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[ivehID].iAkulaTimeToResetWantedLvl)
							FORCE_START_HIDDEN_EVASION(LocalPlayer)
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF HAS_NET_TIMER_STARTED(timerResetWantedLevelInStealth)
					RESET_NET_TIMER(timerResetWantedLevelInStealth)
				ENDIF
			ENDIF
			
			IF bInStealthMode
			AND bLocalPlayerPedOK
				SET_VEHICLE_ALLOW_HOMING_MISSLE_LOCKON_SYNCED(viAkula, FALSE, TRUE) // Disables lock-on
				
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_KEEP_STEALTH_VEHICLE_BLIPS_VISIBLE)
					SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].iBSVehStealthMode, GLOBAL_BS_VEH_STEALTH_DISABLE_HIDDEN_BLIP_THIS_FRAME) // Keeps blip visible
				ENDIF
				
				PRINTLN("PROCESS_AKULA_STEALTH_MODE_IN_MISSION - Disabling lock on for local player's Akula!")
				SET_BIT(iVehStealthMode_MC_BS, ci_VehStealthMode_HasDisabledLockOn)		
			ELSE
				IF IS_BIT_SET(iVehStealthMode_MC_BS, ci_VehStealthMode_HasDisabledLockOn)
					SET_VEHICLE_ALLOW_HOMING_MISSLE_LOCKON_SYNCED(viAkula, TRUE, TRUE)
					CLEAR_BIT(iVehStealthMode_MC_BS, ci_VehStealthMode_HasDisabledLockOn)
					PRINTLN("PROCESS_AKULA_STEALTH_MODE_IN_MISSION - Re-enabling lock on for local player's Akula!")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Masks
// ##### Description: This section covers mask functionality for the local player
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC SET_MASK_STATE(QUICK_EQUIP_MASK_TYPE eNewMaskState)
	IF eMaskState != eNewMaskState
		PRINTLN("[Mask] QUICK_EQUIP_MASK_TYPE - Updating eMaskState from ", eMaskState ," to ", eNewMaskState)
		eMaskState = eNewMaskState
	ENDIF
ENDPROC

FUNC BOOL IS_MASK_STATE(QUICK_EQUIP_MASK_TYPE eMaskSateToCheck)
	RETURN eMaskState = eMaskSateToCheck
ENDFUNC

FUNC BOOL IS_QUICK_EQUIP_MASK_REBREATHER()
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitsetThree[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE3_QUICK_EQUIP_REBREATHER)
		RETURN FALSE
	ENDIF
	
	IF IS_PED_WEARING_SCUBA_TANK(LocalPlayerPed)
		RETURN FALSE
	ENDIF
	
	IF GET_MP_INT_CHARACTER_STAT(MP_STAT_BREATHING_APPAR_BOUGHT) <= 0
		RETURN FALSE
	ENDIF
	
	IF NOT (IS_ENTITY_SUBMERGED_IN_WATER(LocalPlayerPed) OR NOT IS_BIT_SET(iLocalQuickMaskBitSet, LBS_MASK_QUICK_EQUIP_AVAILABLE))
		RETURN FALSE
	ENDIF
	
	IF IS_MASK_STATE(QE_MASK_AUTO_EQUIP)
		RETURN FALSE
	ENDIF
	
	IF (IS_CURRENT_OUTFIT_MASK_NIGHTVISION() AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[GET_LOCAL_PLAYER_TEAM()].iTeamBitset4, ciBS4_TEAM_4_AutoEquipNightVision))
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC SET_MASK_PROCESSING_AVAILABILITY()
	
	//Rebreather
	IF IS_QUICK_EQUIP_MASK_REBREATHER()
		SET_MASK_STATE(QE_MASK_REBREATHER)
		#IF IS_DEBUG_BUILD IF GET_FRAME_COUNT() % 60 = 0 PRINTLN("[Mask] - SERVER - ciBS_RULE3_QUICK_EQUIP_REBREATHER = TRUE") ENDIF #ENDIF
		IF IS_BIT_SET(iLocalQuickMaskBitSet, LBS_MASK_QUICK_EQUIPPED)
			IF NOT IS_MP_HEIST_GEAR_EQUIPPED(LocalPlayerPed, HEIST_GEAR_REBREATHER)
				CLEAR_BIT(iLocalQuickMaskBitSet, LBS_MASK_QUICK_EQUIPPED)
				PRINTLN("[Mask] PROCESS_QUICK_EQUIP_MASK - Clearing LBS_MASK_QUICK_EQUIPPED")
			ENDIF
		ENDIF
	
	//Remove mask
	ELIF IS_BIT_SET(iLocalQuickMaskBitSet, LBS_MASK_QUICK_UNEQUIP_AVAILABLE)
		SET_MASK_STATE(QE_MASK_UNEQUIP)
		#IF IS_DEBUG_BUILD IF GET_FRAME_COUNT() % 60 = 0 PRINTLN("[Mask] - SERVER - LBS_MASK_QUICK_UNEQUIP_AVAILABLE = TRUE - REMOVE MASK") ENDIF #ENDIF
	
	//Equip mask
	ELIF IS_BIT_SET(iLocalQuickMaskBitSet, LBS_MASK_QUICK_EQUIP_AVAILABLE)
		SET_MASK_STATE(QE_MASK_EQUIP)
		#IF IS_DEBUG_BUILD IF GET_FRAME_COUNT() % 60 = 0 PRINTLN("[Mask] - SERVER - ciBS_RULE2_QUICK_EQUIP_MASK = TRUE") ENDIF #ENDIF
	
	//Auto equip mask
	ELIF g_bAutoMaskEquip
		SET_MASK_STATE(QE_MASK_AUTO_EQUIP)
		#IF IS_DEBUG_BUILD IF GET_FRAME_COUNT() % 60 = 0 PRINTLN("[Mask] - SERVER - g_bAutoMaskEquip = TRUE") ENDIF #ENDIF
	ELSE
		SET_MASK_STATE(QE_MASK_UNAVAILABLE)
	ENDIF
	
ENDPROC

PROC BLOCK_INPUTS_FOR_QUICK_EQUIP_MASK()
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SCRIPT_PAD_LEFT)	
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DETONATE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_RADIO_WHEEL)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_NEXT_RADIO)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PREV_RADIO)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_THROW_GRENADE)
ENDPROC

FUNC BOOL IS_ANY_MASK_HELP_TEXT_BEING_DISPLAYED(BOOL bCheckRemoveText = FALSE)
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HQE_MASK")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HQE_MASK1")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HQE_MASK2")
	OR (bCheckRemoveText AND IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HQE_MASK3"))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_QUICK_EQUIP_MASK_VEHICLE_LOGIC()
	IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		PRINTLN("[Mask] FMMC_PROCESS_QUICK_EQUIP_MASK - Player in car so SET_PED_CAN_PLAY_AMBIENT_IDLES(LocalPlayerPed, TRUE, TRUE).")
		SET_PED_CAN_PLAY_AMBIENT_IDLES(LocalPlayerPed, TRUE, TRUE)
	ENDIF
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
ENDPROC

FUNC BOOL IS_GAME_IN_STATE_TO_PROCESS_QUICK_EQUIP_MASK()
	
	IF IS_CUSTOM_MENU_ON_SCREEN()
		RETURN FALSE
	ENDIF
	
	IF IS_PAUSE_MENU_ACTIVE()
		RETURN FALSE
	ENDIF
	
	IF IS_BROWSER_OPEN()
		RETURN FALSE
	ENDIF
	
	IF g_bAtmShowing
		RETURN FALSE
	ENDIF
	
	IF GET_PED_RESET_FLAG(LocalPlayerPed,PRF_PuttingOnHelmet)
		RETURN FALSE
	ENDIF
	
	IF IS_PED_TAKING_OFF_HELMET(LocalPlayerPed)
		RETURN FALSE
	ENDIF
	
	IF IS_PHONE_ONSCREEN()
		RETURN FALSE
	ENDIF
	
	IF IS_HUD_COMPONENT_ACTIVE(NEW_HUD_WEAPON_WHEEL)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC PROCESS_QUICK_EQUIP_MASK_HELP_TEXT()
	
	IF HAS_NET_TIMER_STARTED(stQuickEquipHelptextDelayTimer)
	AND NOT HAS_NET_TIMER_EXPIRED(stQuickEquipHelptextDelayTimer, ciQuickEquipHelptextDelayLength)
		PRINTLN("[Mask] PROCESS_QUICK_EQUIP_MASK_HELP_TEXT - Waiting for stQuickEquipHelptextDelayTimer")
		EXIT
	ENDIF
	
	SWITCH eMaskState
		CASE QE_MASK_UNAVAILABLE
			
		BREAK
		
		CASE QE_MASK_EQUIP
			
			INT iStatInt
			iStatInt = GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_NON_MS_HELP_TEXT7)
	
			IF NOT IS_BIT_SET(iStatInt, biNmh7_QuickEquipMask)
			AND NOT IS_BIT_SET(iLocalQuickMaskBitSet, LBS_MASK_QUICK_EQUIP_HELP_1)
			AND IS_THIS_A_HEIST_MISSION()
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					PRINT_HELP("HQE_MASK1", 15000)
					
					SET_BIT(iStatInt, biNmh7_QuickEquipMask)
					SET_MP_INT_CHARACTER_STAT(MP_STAT_FM_NON_MS_HELP_TEXT7, iStatInt)
					PRINTLN("[Mask] PROCESS_QUICK_EQUIP_MASK_HELP_TEXT - TUTORIAL HELP TEXT DONE - biNmh7_QuickEquipMask - SET")
					
					SET_BIT(iLocalQuickMaskBitSet, LBS_MASK_QUICK_EQUIP_HELP_1)
					PRINTLN("[Mask] PROCESS_QUICK_EQUIP_MASK_HELP_TEXT - HELP TEXT DONE - LBS_MASK_QUICK_EQUIP_HELP_1 - SET")
				ENDIF
				
			ELIF NOT IS_BIT_SET(iLocalQuickMaskBitSet, LBS_MASK_QUICK_EQUIP_HELP_2)
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HQE_MASK1")
					PRINT_HELP("HQE_MASK", 15000)
					SET_BIT(iLocalQuickMaskBitSet, LBS_MASK_QUICK_EQUIP_HELP_2)
					PRINTLN("[Mask] PROCESS_QUICK_EQUIP_MASK_HELP_TEXT - EQUIP MASK HELP TEXT DONE - LBS_MASK_QUICK_EQUIP_HELP_2 - SET")
				ENDIF
			ENDIF
		BREAK
		
		CASE QE_MASK_UNEQUIP
			IF NOT IS_BIT_SET(iLocalQuickMaskBitSet, LBS_MASK_QUICK_REMOVE_HELP)
				PRINT_HELP("HQE_MASK3", 15000)
				SET_BIT(iLocalQuickMaskBitSet, LBS_MASK_QUICK_REMOVE_HELP)
				PRINTLN("[Mask] PROCESS_QUICK_EQUIP_MASK_HELP_TEXT - REMOVE MASK HELP TEXT DONE - LBS_MASK_QUICK_REMOVE_HELP - SET")
			ENDIF
		BREAK
		
		CASE QE_MASK_REBREATHER
			IF IS_LOCAL_PLAYER_DROWNING()
				IF NOT IS_BIT_SET(iLocalBoolCheck33, LBOOL33_PLAYER_SEEN_REBREATHER_CONTROLS_HELPTEXT)
					PRINTLN("[Mask] PROCESS_QUICK_EQUIP_MASK_HELP_TEXT - Displaying rebreather helptext now!")
					PRINT_HELP("HQE_MASK2", 15000)
					SET_BIT(iLocalBoolCheck33, LBOOL33_PLAYER_SEEN_REBREATHER_CONTROLS_HELPTEXT)
				ENDIF
			ELSE
				CLEAR_BIT(iLocalBoolCheck33, LBOOL33_PLAYER_SEEN_REBREATHER_CONTROLS_HELPTEXT)
			ENDIF
		BREAK
		
	ENDSWITCH
ENDPROC

FUNC BOOL IS_PLAYER_ALREADY_WEARING_MASK_OR_HELMET()
	IF CORRECT_MASK_STATE_FOR_QUICK_EQUIP_MASK(IS_MASK_STATE(QE_MASK_REBREATHER), IS_MASK_STATE(QE_MASK_UNEQUIP))
		IF CORRECT_MASK_STATE_FOR_QUICK_EQUIP_HELMET(IS_MASK_STATE(QE_MASK_REBREATHER), IS_MASK_STATE(QE_MASK_UNEQUIP))
			RETURN FALSE
		ELSE
			IF IS_ANY_MASK_HELP_TEXT_BEING_DISPLAYED()
				CLEAR_HELP()
				PRINTLN("[Mask] FMMC_PROCESS_QUICK_EQUIP_MASK - IS_PLAYER_ALREADY_WEARING_MASK_OR_HELMET - ALREADY WEARING A HELMET")
			ENDIF
			IF NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed, TRUE)
			AND NOT GET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_HasHelmet)
			AND NOT GET_PED_RESET_FLAG(LocalPlayerPed, PRF_PuttingOnHelmet)
			AND NOT IS_PED_TAKING_OFF_HELMET(LocalPlayerPed)
				SET_BIT(iLocalQuickMaskBitSet, LBS_MASK_QUICK_EQUIPPED)
				PRINTLN("[Mask] FMMC_PROCESS_QUICK_EQUIP_MASK - IS_PLAYER_ALREADY_WEARING_MASK_OR_HELMET - ALREADY WEARING A HELMET - NOT FROM A VEHICLE - LBS_MASK_QUICK_EQUIPPED - SET")
			ENDIF
		ENDIF
	ELSE
		IF IS_MASK_STATE(QE_MASK_UNEQUIP)
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HQE_MASK3")
				CLEAR_HELP()
			ENDIF
			SET_BIT(iLocalQuickMaskBitSet, LBS_MASK_QUICK_REMOVED)
			PRINTLN("[Mask] FMMC_PROCESS_QUICK_EQUIP_MASK - IS_PLAYER_ALREADY_WEARING_MASK_OR_HELMET - ALREADY WEARING A MASK - LBS_MASK_QUICK_REMOVED - SET")
		ELSE
			IF IS_ANY_MASK_HELP_TEXT_BEING_DISPLAYED()
				CLEAR_HELP()
			ENDIF
			SET_BIT(iLocalQuickMaskBitSet, LBS_MASK_QUICK_EQUIPPED)
			PRINTLN("[Mask] FMMC_PROCESS_QUICK_EQUIP_MASK - IS_PLAYER_ALREADY_WEARING_MASK_OR_HELMET - ALREADY WEARING A MASK - LBS_MASK_QUICK_EQUIPPED - SET")
		ENDIF
	ENDIF
	
	RETURN TRUE

ENDFUNC

PROC PROCESS_MASK_ANIM_CLEANUP()
	IF bAnimationPlaying
		IF IS_ENTITY_PLAYING_ANIM(LocalPlayerPed, GET_ANIM_DICT_MASK(maskAnimData), GET_ANIM_CLIP_NAME_MASK())
			PROCESS_QUICK_EQUIP_MASK_VEHICLE_LOGIC()
		ELSE
			CLEANUP_MASK_ANIMS(maskAnimData)
			bAnimationPlaying = FALSE
		ENDIF
	ELSE
		CLEANUP_MASK_ANIMS(maskAnimData)
	ENDIF
ENDPROC

PROC APPLY_QUICK_EQUIP_MASK(MP_OUTFIT_MASK_ENUM eMask)
	MP_OUTFITS_APPLY_DATA   		sMPOutfitApplyData
	sMPOutfitApplyData.eApplyStage 	= AOS_SET         
    sMPOutfitApplyData.pedID        = LocalPlayerPed
    sMPOutfitApplyData.eMask        = eMask
	CLEAR_PED_STORED_HAT_PROP(LocalPlayerPed)
	SET_PED_MP_OUTFIT(sMPOutfitApplyData, TRUE, TRUE, FALSE)
ENDPROC

PROC FMMC_PROCESS_QUICK_EQUIP_MASK(INT iTeam)

	BOOL bHasFailsafeTimerExpired
	
	SET_MASK_PROCESSING_AVAILABILITY()
	
	IF IS_MASK_STATE(QE_MASK_UNAVAILABLE)
		PROCESS_MASK_ANIM_CLEANUP()
		EXIT
	ENDIF
		
	// Load in mask anims
	REQUEST_MASK_ANIMS(maskAnimData)

	IF (!IS_MASK_STATE(QE_MASK_UNEQUIP) AND IS_BIT_SET(iLocalQuickMaskBitSet, LBS_MASK_QUICK_EQUIPPED))
	OR (IS_MASK_STATE(QE_MASK_UNEQUIP) AND IS_BIT_SET(iLocalQuickMaskBitSet, LBS_MASK_QUICK_REMOVED))
		PROCESS_MASK_ANIM_CLEANUP()
		EXIT
	ENDIF
	
	IF NOT READY_TO_PUT_MASK_ON(maskAnimData)
	OR IS_MASK_STATE(QE_MASK_UNEQUIP)
		IF NOT IS_PLAYER_ALREADY_WEARING_MASK_OR_HELMET()
			IF IS_GAME_IN_STATE_TO_PROCESS_QUICK_EQUIP_MASK()
				
				PROCESS_QUICK_EQUIP_MASK_HELP_TEXT()
			
				BLOCK_INPUTS_FOR_QUICK_EQUIP_MASK()
				
				IF NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
				OR (IS_PED_IN_ANY_VEHICLE(LocalPlayerPed) AND IS_PED_SITTING_IN_ANY_VEHICLE(LocalPlayerPed))
				
					IF IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_DETONATE)
					OR IS_MASK_STATE(QE_MASK_AUTO_EQUIP)
						IF IS_MASK_STATE(QE_MASK_UNEQUIP)
							//Remove Masks and Helmets
							SET_PED_COMP_ITEM_CURRENT_MP(LocalPlayerPed, COMP_TYPE_BERD, INT_TO_ENUM(PED_COMP_NAME_ENUM, 0))
							SET_PED_COMP_ITEM_CURRENT_MP(LocalPlayerPed, COMP_TYPE_PROPS, PROPS_HEAD_NONE)
							
							FINALIZE_HEAD_BLEND(LocalPlayerPed)
							BROADCAST_GENERAL_EVENT(GENERAL_EVENT_TYPE_PLAYER_CHANGED_APPEARANCE, ALL_PLAYERS())
																	
							IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HQE_MASK3")
								CLEAR_HELP()
							ENDIF
							
							g_iEquippedMask = -1
							PRINTLN("[Mask] FMMC_PROCESS_QUICK_EQUIP_MASK - Is now g_iEquippedMask: ", g_iEquippedMask)
							
							SET_BIT(iLocalQuickMaskBitSet, LBS_MASK_QUICK_REMOVED)
							PRINTLN("[Mask] FMMC_PROCESS_QUICK_EQUIP_MASK - Input pressed to remove mask!")
						ELSE
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
							
							SET_BIT(maskAnimData.iMaskBitSet, ciMASK_BITSET_PUT_ON_MASK)
							PRINTLN("[Mask] FMMC_PROCESS_QUICK_EQUIP_MASK - Input pressed to put on mask!")
						ENDIF
						
						SET_BIT(iLocalQuickMaskBitSet, LBS_MASK_EXTEND_CONTROL_BLOCK)
			            PRINTLN("[Mask] FMMC_PROCESS_QUICK_EQUIP_MASK - LBS_MASK_EXTEND_CONTROL_BLOCK - SET")
						
					ENDIF
				ENDIF
			ELSE
				IF IS_ANY_MASK_HELP_TEXT_BEING_DISPLAYED(TRUE)
					CLEAR_HELP()
					CLEAR_BIT(iLocalQuickMaskBitSet, LBS_MASK_QUICK_EQUIP_HELP_2)
					CLEAR_BIT(iLocalQuickMaskBitSet, LBS_MASK_QUICK_REMOVE_HELP)
				ENDIF
			ENDIF
		ENDIF
	ELSE					
	
		IF NOT HAS_NET_TIMER_STARTED(maskAnimData.stMaskFailsafe)
			START_NET_TIMER(maskAnimData.stMaskFailsafe)
		ELSE
		
			bHasFailsafeTimerExpired = HAS_NET_TIMER_EXPIRED(maskAnimData.stMaskFailsafe, ciMASK_FAILSAFE_TIMEOUT)
			
			IF HAS_MASK_ANIM_STARTED(maskAnimData)
			OR DONT_DO_MASK_ANIM(maskAnimData)
			OR bHasFailsafeTimerExpired
			
				IF NOT DONT_DO_MASK_ANIM(maskAnimData)
					PROCESS_QUICK_EQUIP_MASK_VEHICLE_LOGIC()
				ENDIF
			
				IF HAS_MASK_EVENT_FIRED()
				OR DONT_DO_MASK_ANIM(maskAnimData)
				OR bHasFailsafeTimerExpired
				
					#IF IS_DEBUG_BUILD
					IF bHasFailsafeTimerExpired
						PRINTLN("[Mask] FMMC_PROCESS_QUICK_EQUIP_MASK - bHasFailsafeTimerExpired")
						SCRIPT_ASSERT("[Mask] FMMC_PROCESS_QUICK_EQUIP_MASK - bHasFailsafeTimerExpired ")
					ENDIF
					#ENDIF
					
					IF IS_MASK_STATE(QE_MASK_REBREATHER)
						PRINTLN("[Mask] FMMC_PROCESS_QUICK_EQUIP_MASK - SET_MP_HEIST_GEAR - HEIST_GEAR_REBREATHER")
						SET_MP_HEIST_GEAR(LocalPlayerPed, HEIST_GEAR_REBREATHER)
					ELSE
						PRINTLN("[Mask] FMMC_PROCESS_QUICK_EQUIP_MASK - SET_PED_MP_OUTFIT.")
						APPLY_QUICK_EQUIP_MASK(INT_TO_ENUM(MP_OUTFIT_MASK_ENUM, FMMC_GET_LOCAL_DEFAULT_MASK(iTeam)))
						
						IF IS_BIT_SET(MC_ServerBD.iCheckpointBitset, SB_CHECKPOINT_1)
						OR IS_BIT_SET(MC_ServerBD.iCheckpointBitset, SB_CHECKPOINT_2)
					    OR (HAS_FIRST_STRAND_MISSION_BEEN_PASSED_NO_RESET() OR HAS_FIRST_STRAND_MISSION_BEEN_PASSED())
						OR IS_MASK_STATE(QE_MASK_AUTO_EQUIP)
							g_iEquippedMask = FMMC_GET_LOCAL_DEFAULT_MASK(iTeam)
							PRINTLN("[Mask] FMMC_PROCESS_QUICK_EQUIP_MASK - Is now g_iEquippedMask: ", g_iEquippedMask)
						ENDIF
					ENDIF
					
					FINALIZE_HEAD_BLEND(LocalPlayerPed)
					BROADCAST_GENERAL_EVENT(GENERAL_EVENT_TYPE_PLAYER_CHANGED_APPEARANCE, ALL_PLAYERS())
					
					IF IS_ANY_MASK_HELP_TEXT_BEING_DISPLAYED()
						CLEAR_HELP()
					ENDIF
					
					IF NOT DONT_DO_MASK_ANIM(maskAnimData)
						bAnimationPlaying = TRUE
					ENDIF
					
					SET_BIT(iLocalQuickMaskBitSet, LBS_MASK_QUICK_EQUIPPED)
					PRINTLN("[Mask] FMMC_PROCESS_QUICK_EQUIP_MASK - LBS_MASK_QUICK_EQUIPPED.")
		            PRINTLN("[Mask] FMMC_PROCESS_QUICK_EQUIP_MASK - MASK HAS BEEN EQUIPPED - LBS_MASK_QUICK_EQUIPPED - SET")
					PRINTLN("[Mask][MASK_ANIM] FMMC_PROCESS_QUICK_EQUIP_MASK - DONE")
					
					IF IS_MASK_STATE(QE_MASK_AUTO_EQUIP)
						g_bAutoMaskEquip = FALSE
						SET_MASK_STATE(QE_MASK_UNAVAILABLE)
						PRINTLN("[Mask] PROCESS_PLAYER_MASK_STATUS - Clearing g_bAutoMaskEquip as mask is now on.")
					ENDIF
					
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//BLOCK CONTROLS
	IF IS_BIT_SET(iLocalQuickMaskBitSet, LBS_MASK_EXTEND_CONTROL_BLOCK)
		IF IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_DETONATE)
			BLOCK_INPUTS_FOR_QUICK_EQUIP_MASK()
			PRINTLN("[Mask] FMMC_PROCESS_QUICK_EQUIP_MASK - LBS_MASK_EXTEND_CONTROL_BLOCK - BLOCKING DPAD LEFT INPUTS")
		ELSE
			CLEAR_BIT(iLocalQuickMaskBitSet, LBS_MASK_EXTEND_CONTROL_BLOCK)
		    PRINTLN("[Mask] FMMC_PROCESS_QUICK_EQUIP_MASK - LBS_MASK_EXTEND_CONTROL_BLOCK - CLEARED")
		ENDIF
	ENDIF
	
ENDPROC

FUNC INT GET_NUMBER_OF_FREEMODE_DLC_MASKS()

	INT iCurrentPed
    scrShopPedProp propItem
    scrShopPedComponent componentItem
    INIT_SHOP_PED_PROP(propItem)
    INIT_SHOP_PED_COMPONENT(componentItem)
  
    SWITCH GET_ENTITY_MODEL(LocalPlayerPed)
        CASE MP_M_FREEMODE_01
            iCurrentPed = 3   //Male
        BREAK
        CASE MP_F_FREEMODE_01
            iCurrentPed = 4   //Female
        BREAK
    ENDSWITCH

	INT iDLCItem = 0
	INT iMaskTotalCount = 0
	INT iDLCItemCount = SETUP_SHOP_PED_APPAREL_QUERY_TU(iCurrentPed, ENUM_TO_INT(CLO_SHOP_NONE), -1, ENUM_TO_INT(SHOP_PED_COMPONENT), -1, ENUM_TO_INT(PED_COMP_BERD))

	REPEAT iDLCItemCount iDLCItem
	    GET_SHOP_PED_QUERY_COMPONENT(iDLCItem, componentItem)
	    IF NOT IS_CONTENT_ITEM_LOCKED(componentItem.m_lockHash)
            IF NOT IS_CONTENT_ITEM_LOCKED_BY_SCRIPT(componentItem.m_lockHash, componentItem.m_nameHash, SHOP_PED_COMPONENT)
				IF IS_BIT_SET_SHOP_PED_APPAREL_SCRIPT(componentItem.m_nameHash, PED_COMPONENT_ACQUIRED_SLOT)
                   iMaskTotalCount++
               ENDIF
            ENDIF
	    ENDIF
	ENDREPEAT

	RETURN iMaskTotalCount

ENDFUNC

FUNC BOOL DO_I_HAVE_A_MASK()
	
    INT MAX_MASKS = GET_NUMBER_OF_FREEMODE_MASKS(IS_PLAYER_MALE(LocalPlayer)) + GET_NUMBER_OF_FREEMODE_DLC_MASKS()
	INT i = 0
	
	IF IS_LONG_BIT_SET(g_FMMC_STRUCT.biGearAvailableBitset[MC_playerBD[iPartToUse].iteam], ENUM_TO_INT(HEIST_GEAR_BALACLAVA)) //Used Externally IS_LONG_BIT_SET
	OR IS_LONG_BIT_SET(g_FMMC_STRUCT.biGearAvailableBitset[MC_playerBD[iPartToUse].iteam], ENUM_TO_INT(HEIST_GEAR_WHITE_HOCKEY_MASK)) //Used Externally IS_LONG_BIT_SET
	OR IS_LONG_BIT_SET(g_FMMC_STRUCT.biGearAvailableBitset[MC_playerBD[iPartToUse].iteam], ENUM_TO_INT(HEIST_GEAR_GAS_MASK)) //Used Externally IS_LONG_BIT_SET
	OR IS_LONG_BIT_SET(g_FMMC_STRUCT.biGearAvailableBitset[MC_playerBD[iPartToUse].iteam], ENUM_TO_INT(HEIST_GEAR_NIGHTVISION)) //Used Externally IS_LONG_BIT_SET
	OR IS_LONG_BIT_SET(g_FMMC_STRUCT.biGearAvailableBitset[MC_playerBD[iPartToUse].iteam], ENUM_TO_INT(HEIST_GEAR_CORONA_MASK)) //Used Externally IS_LONG_BIT_SET
		PRINTLN("[Mask] DO_I_HAVE_A_MASK: Heist Gear")
		RETURN TRUE
	ENDIF
		
	IF FMMC_GET_LOCAL_DEFAULT_MASK(MC_playerBD[iPartToUse].iteam) != -1
		PRINTLN("[Mask] DO_I_HAVE_A_MASK: GET_LOCAL_DEFAULT_MASK(MC_playerBD[iPartToUse].iteam):", FMMC_GET_LOCAL_DEFAULT_MASK(MC_playerBD[iPartToUse].iteam))
		RETURN TRUE
	ENDIF
	
	//only check the bought masks if not on a heist as acquired masks no longer get added to PI menu on heists only selected ones		
	IF NOT IS_THIS_A_HEIST_MISSION()
		REPEAT MAX_MASKS i
	        IF IS_PED_COMP_ITEM_ACQUIRED_MP(GET_PLAYER_MODEL(), COMP_TYPE_BERD, INT_TO_ENUM(PED_COMP_NAME_ENUM, i))
		    	IF i != 0
					PRINTLN("[Mask] DO_I_HAVE_A_MASK: Has mask: ", i)
		            RETURN TRUE
		        ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL CAN_LOCAL_PLAYER_DO_QUICK_EQUIP_MASK()
	
	IF !bLocalPlayerPedOK
		RETURN FALSE
	ENDIF
	
	IF IS_LOCAL_PLAYER_INTERACTING()
		RETURN FALSE
	ENDIF
	
	IF GET_PEDS_CURRENT_WEAPON(LocalPlayerPed) = WEAPONTYPE_STICKYBOMB
	AND IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_AIM)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

PROC SET_QUICK_EQUIP_MASK_AVAILABLE()
		
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFive, ciOptionsBS25_HeistOptionAllowMaskAfterTeamAggro)
	AND HAS_ANY_TEAM_TRIGGERED_AGGRO(ciMISSION_AGGRO_INDEX_ALL)
		IF NOT IS_BIT_SET(iLocalQuickMaskBitSet, LBS_MASK_QUICK_EQUIP_AVAILABLE)
			SET_BIT(iLocalQuickMaskBitSet, LBS_MASK_QUICK_EQUIP_AVAILABLE)
			DELAY_QUICK_EQUIP_HELPTEXT()
			PRINTLN("[Mask] SET_QUICK_EQUIP_MASK_AVAILABLE - LBS_MASK_QUICK_EQUIP_AVAILABLE set - Aggro")
		ENDIF
		EXIT
	ENDIF
	
	IF IS_RULE_INDEX_VALID(GET_LOCAL_PLAYER_CURRENT_RULE())
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[GET_LOCAL_PLAYER_TEAM()].iRuleBitsetTwo[GET_LOCAL_PLAYER_CURRENT_RULE()], ciBS_RULE2_QUICK_EQUIP_MASK)
			#IF IS_DEBUG_BUILD
			IF NOT IS_BIT_SET(iLocalQuickMaskBitSet, LBS_MASK_QUICK_EQUIP_AVAILABLE)
				PRINTLN("[Mask] SET_QUICK_EQUIP_MASK_AVAILABLE - LBS_MASK_QUICK_EQUIP_AVAILABLE Set - Rule")
			ENDIF
			#ENDIF
			
			SET_BIT(iLocalQuickMaskBitSet, LBS_MASK_QUICK_EQUIP_AVAILABLE)
			
		ELSE
			#IF IS_DEBUG_BUILD
			IF IS_BIT_SET(iLocalQuickMaskBitSet, LBS_MASK_QUICK_EQUIP_AVAILABLE)
				PRINTLN("[Mask] SET_QUICK_EQUIP_MASK_AVAILABLE - LBS_MASK_QUICK_EQUIP_AVAILABLE Cleared - Rule")
			ENDIF
			#ENDIF
			
			CLEAR_BIT(iLocalQuickMaskBitSet, LBS_MASK_QUICK_EQUIP_AVAILABLE)
			
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_PROCESS_QUICK_EQUIP_MASKS()
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciALLOW_NON_HEIST_REBREATHERS)
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(iLocalQuickMaskBitSet, LBS_MASK_QUICK_EQUIP_AVAILABLE)
		RETURN TRUE
	ENDIF
	
	IF g_bAutoMaskEquip
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_QUICK_EQUIP_MASK()

	SET_QUICK_EQUIP_MASK_AVAILABLE()

	IF NOT SHOULD_PROCESS_QUICK_EQUIP_MASKS()
		EXIT
	ENDIF
		
	IF NOT CAN_LOCAL_PLAYER_DO_QUICK_EQUIP_MASK()
		EXIT
	ENDIF
	
	//Quick Equip Mask (also Rebreather, and Quick Remove)
	IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam] < FMMC_MAX_RULES
	AND iSpectatorTarget = -1
		
		FMMC_PROCESS_QUICK_EQUIP_MASK(MC_playerBD[iLocalPart].iTeam)
		
		IF (IS_MASK_STATE(QE_MASK_EQUIP) OR IS_MASK_STATE(QE_MASK_AUTO_EQUIP))
		AND g_iEquippedMask != -1 //if we've done a quick equip restart, disable help
			IF IS_ANY_MASK_HELP_TEXT_BEING_DISPLAYED(TRUE)
				CLEAR_HELP()
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_PLAYER_MASK_INIT()
	
	IF NOT IS_BIT_SET(iLocalBoolCheck4, LBOOL4_OWN_MASK)
	AND NOT IS_BIT_SET(iLocalBoolCheck4, LBOOL4_IN_MASK_SHOP)
		IF DO_I_HAVE_A_MASK()
			SET_BIT(iLocalBoolCheck4,LBOOL4_OWN_MASK)
		ELSE
			//Set this bit so it checks if you have a mask as soon as a Get Mask rule begins
			SET_BIT(iLocalBoolCheck4,LBOOL4_IN_MASK_SHOP)
		ENDIF	
	ENDIF
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Outfits  --------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: This section covers the players current outfit and also applying outfits, swaps and transforms.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PLAY_RULE_OUTFIT_CHANGE_PTFX()
	IF NOT IS_BIT_SET(iLocalBoolCheck26, LBOOL26_PLAYED_OUTFIT_CHANGE_PTFX)
	AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciENABLE_SMOKE_ON_OUTFIT_CHANGE)
	AND bLocalPlayerPedOK
	AND NOT IS_PLAYER_SPECTATING(LocalPlayer)
	AND NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_ANY_SPECTATOR)
	AND NOT g_bMissionEnding
	AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
		IF HAS_NAMED_PTFX_ASSET_LOADED("scr_sm_trans")
			USE_PARTICLE_FX_ASSET("scr_sm_trans")
			START_NETWORKED_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_sm_con_trans", LocalPlayerPed, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
			IF GET_CAM_VIEW_MODE_FOR_CONTEXT(GET_CAM_ACTIVE_VIEW_MODE_CONTEXT()) = CAM_VIEW_MODE_FIRST_PERSON
				USE_PARTICLE_FX_ASSET("scr_sm_trans")
				START_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_sm_con_trans_fp", LocalPlayerPed, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
			ENDIF
			SET_BIT(iLocalBoolCheck26, LBOOL26_PLAYED_OUTFIT_CHANGE_PTFX)
		ENDIF					
	ELSE
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciENABLE_SMOKE_ON_OUTFIT_CHANGE)
			PRINTLN("[RCC MISSION] Outfits - Don't play PTFX. Spectating: ", IS_PLAYER_SPECTATING(LocalPlayer), " Spectating2: ", IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_ANY_SPECTATOR), " Injured: ", !bLocalPlayerPedOK)
		ENDIF
	ENDIF
ENDPROC

// FMMC2020 - This should be setting data to tell _SPAWNING that it needs to add spawn points!
PROC PROCESS_IMMEDIATE_TEAM_SWAP_SPAWN_POINTS()
	IF SHOULD_USE_CUSTOM_SPAWN_POINTS()	
		PRINTLN("[JT][TeamSwaps] - PROCESS_IMMEDIATE_TEAM_SWAP_SPAWN_POINTS - Switched teams need to update spawn points -  ADD_ALL_CUSTOM_SPAWN_POINTS")
		ADD_ALL_CUSTOM_SPAWN_POINTS()
	ELSE
		IF IS_BIT_SET(iLocalBoolCheck21, LBOOL21_USING_CUSTOM_RESPAWN_POINTS)
			PRINTLN("[JT][TeamSwaps] - PROCESS_IMMEDIATE_TEAM_SWAP_SPAWN_POINTS - We are using Custom Respawn Points, but they are not valid. Setting should clear")
			SET_BIT(iLocalBoolCheck21, LBOOL21_SHOULD_CLEAR_CUSTOM_RESPAWN_POINTS)
		ENDIF
	ENDIF
ENDPROC

PROC CLOBBER_OLD_TEAM_DATA_FOR_PLAYER(INT iNewTeam)
	Clear_Any_Objective_Text()
	
	IF iNewTeam = 1 //Winners
		SET_TEMP_PASSIVE_MODE(TRUE, TRUE)
		TURN_OFF_PASSIVE_MODE_OPTION(TRUE)
		REINIT_NET_TIMER(tdTeamSwapPassiveModeTimer)
		SET_ENTITY_ALPHA(LocalPlayerPed, 200, FALSE)
		PRINTLN("[MMacK][TeamSwaps] Set Passive")
	ENDIF
	
	MP_OUTFITS_APPLY_DATA   sApplyData
	sApplyData.pedID		= LocalPlayerPed
	sApplyData.eApplyStage 	= AOS_SET
	INT iOutfitToUse
	iOutfitToUse = ENUM_TO_INT(OUTFIT_VERSUS_EXEC1_TRADING_WINNER_0) 
	iOutfitToUse += GET_PARTICIPANT_NUMBER_IN_TEAM()
	IF iNewTeam = 0
		iOutfitToUse += 6 //number of outfits in winner block
	ENDIF
	MP_OUTFIT_ENUM eOutfitToSwapTo = INT_TO_ENUM(MP_OUTFIT_ENUM, iOutfitToUse)
	
	sApplyData.eOutfit		= eOutfitToSwapTo
	SET_PED_MP_OUTFIT(sApplyData, FALSE, FALSE, FALSE, FALSE)
	
	IF DOES_PLAYER_HAVE_WEAPON(wtCachedLastWeaponHeld)
		SET_CURRENT_PED_WEAPON(LocalPlayerPed, wtCachedLastWeaponHeld, TRUE)
	ELSE
		SET_CURRENT_PED_WEAPON(LocalPlayerPed, GET_BEST_PED_WEAPON(LocalPlayerPed), TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciCASH_EXPLOSION_ON_TEAM_SWAP)
	AND IS_ENTITY_VISIBLE(LocalPlayerPed)
		IF iNewTeam = 1 //Winning Team
			IF HAS_NAMED_PTFX_ASSET_LOADED("scr_tplaces")
				BROADCAST_TRADING_PLACES_PLAY_TRANSFORM_SFX()	// ML
				PRINTLN("[RCC MISSION] Playing Transform_Local_Player")
				PLAY_SOUND_FRONTEND(-1, "Transform_Local_Player", "DLC_Exec_TP_SoundSet", FALSE)
				
				USE_PARTICLE_FX_ASSET("scr_tplaces")
				SET_PARTICLE_FX_NON_LOOPED_COLOUR(1.0, 0.94, 0.57)
				START_NETWORKED_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_tplaces_team_swap", LocalPlayerPed, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
			ENDIF
		ENDIF
		
		IF iNewTeam = 0	//Losing Team
			IF MC_serverBD_1.piTimeBarWinners[0] != INVALID_PLAYER_INDEX()	//Final Team Swap
				BROADCAST_TRADING_PLACES_PLAY_TRANSFORM_SFX()	// ML
				PRINTLN("[RCC MISSION] Playing Transform_Local_Player")
				PLAY_SOUND_FRONTEND(-1, "Transform_Loser_Local_Player", "DLC_Exec_TP_SoundSet", FALSE)
				
				USE_PARTICLE_FX_ASSET("scr_tplaces")
				START_NETWORKED_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_tplaceS_team_swap_nocash", LocalPlayerPed, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
			ENDIF
		ENDIF
	ENDIF
	
	SET_BIT(iLocalBoolCheck17, LBOOL17_TEAM_SWAP_PICKUP_PROCESS)
	
	IF NOT IS_BIT_SET( g_FMMC_STRUCT.iOptionsMenuBitSet, ciHIDE_VERSUS_BLIPS )
		SHOW_ALL_PLAYER_BLIPS(TRUE)
		SET_ALL_PLAYER_BLIPS_LONG_RANGE(TRUE)
		PRINTLN("[MMacK][TeamSwaps] SHOW_ALL_PLAYER_BLIPS = TRUE")
	ELSE
		SHOW_ALL_PLAYER_BLIPS(FALSE)
		PRINTLN("[MMacK][TeamSwaps] SHOW_ALL_PLAYER_BLIPS = FALSE")
	ENDIF
	
	iCustomPlayerBlipSetBS = 0
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iNewTeam].iTeamBitset4, ciBS4_TEAM_1_UsingNewBlipSpriteData+iNewTeam)
		SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(LocalPlayer, INT_TO_ENUM(BLIP_SPRITE, g_FMMC_STRUCT.iTeamBlipType[iNewTeam]), TRUE)
	ELSE
		IF g_FMMC_STRUCT.iTeamBlipType[iNewTeam] = TEAM_BLIP_RED_SKULL
			SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(LocalPlayer, RADAR_TRACE_BOUNTY_HIT, TRUE)
			PRINTLN("[MMacK][TeamSwaps] Set Blip To Skull for LocalPlayer")
		ELSE
			SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(LocalPlayer, RADAR_TRACE_BOUNTY_HIT, FALSE)
			PRINTLN("[MMacK][TeamSwaps] Set Blip To Default for LocalPlayer")
		ENDIF
	ENDIF
	
	IF MC_serverBD_4.iCurrentHighestPriority[iNewTeam] < FMMC_MAX_RULES
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iNewTeam].iRuleBitsetEight[MC_serverBD_4.iCurrentHighestPriority[iNewTeam]], ciBS_RULE8_EXPLOSIONS_DONT_RAGDOLL)
		OR IS_PARTICIPANT_A_JUGGERNAUT(iLocalPart)
			SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_DontActivateRagdollFromExplosions, TRUE)
		ELSE
			SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_DontActivateRagdollFromExplosions, FALSE)
		ENDIF
		
		PROCESS_MY_PLAYER_BLIP_VISIBILITY(iNewTeam, MC_serverBD_4.iCurrentHighestPriority[iNewTeam])
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iNewTeam].iRuleBitsetFour[MC_serverBD_4.iCurrentHighestPriority[iNewTeam]], ciBS_RULE4_BLIP_PLAYERS)
			IF NOT IS_BIT_SET(iLocalBoolCheck,LBOOL_SHOW_ALL_PLAYER_BLIPS)
				SHOW_ALL_PLAYER_BLIPS(TRUE)
				SET_ALL_PLAYER_BLIPS_LONG_RANGE(TRUE)
				PRINTLN("[RCC MISSION][TeamSwaps] CLOBBER_OLD_TEAM_DATA_FOR_PLAYER - Show all player blips")
				SET_BIT(iLocalBoolCheck,LBOOL_SHOW_ALL_PLAYER_BLIPS)
			ENDIF
		ELSE
			IF IS_BIT_SET(iLocalBoolCheck,LBOOL_SHOW_ALL_PLAYER_BLIPS)
				IF NOT IS_BIT_SET( MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH )
				AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciBLIP_PLAYERS_AT_SUDDEN_DEATH)
					SHOW_ALL_PLAYER_BLIPS(FALSE)
					PRINTLN("[RCC MISSION] PROCESS_PLAYER_BLIPS - Stop showing all player blips")
					
					CLEAR_BIT(iLocalBoolCheck,LBOOL_SHOW_ALL_PLAYER_BLIPS)
				ENDIF
			ENDIF
		ENDIF
		
		BLOCK_PLAYER_HEALTH_REGEN(IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iNewTeam].iRuleBitsetEight[MC_serverBD_4.iCurrentHighestPriority[iNewTeam]], ciBS_RULE8_BLOCK_HEALTH_REGEN))
		
		IF g_FMMC_STRUCT.sFMMCEndConditions[iNewTeam].iMaxHealth[MC_serverBD_4.iCurrentHighestPriority[iNewTeam]] != 0
			bFMMCHealthBoosted = FALSE
			INT iNewMaxHealth = g_FMMC_STRUCT.sFMMCEndConditions[iNewTeam].iMaxHealth[MC_serverBD_4.iCurrentHighestPriority[iNewTeam]]
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iNewTeam].iRuleBitsetEight[MC_serverBD_4.iCurrentHighestPriority[iNewTeam]], ciBS_RULE8_SCALE_MAX_PLAYER_HEALTH)
				INT i
				INT iEnemyPlayers
				FOR i = 0 TO FMMC_MAX_TEAMS - 1
					PRINTLN("[MMacK][TeamSwaps] Health is being scaled by players: ", MC_serverBD.iNumberOfPlayingPlayers[i])
					iEnemyPlayers = iEnemyPlayers + MC_serverBD.iNumberOfPlayingPlayers[i]
				ENDFOR
				iNewMaxHealth = (iNewMaxHealth * iEnemyPlayers)
			ENDIF
			INCREASE_PLAYER_HEALTH(iNewMaxHealth)
			PRINTLN("[MMacK][TeamSwaps] Player Health Modded to ", iNewMaxHealth)
		ELSE
			IF iMaxHealthBeforebigfoot != 0
				SET_ENTITY_MAX_HEALTH(LocalPlayerPed, iMaxHealthBeforebigfoot)
				iMaxHealthBeforebigfoot = 0
			ENDIF
			IF bLocalPlayerPedOK
				SET_ENTITY_HEALTH(LocalPlayerPed, GET_ENTITY_MAX_HEALTH(LocalPlayerPed))
			ENDIF
			SET_PED_SUFFERS_CRITICAL_HITS(LocalPlayerPed, TRUE)
			bFMMCHealthBoosted = FALSE
			PRINTLN("[MMacK][TeamSwaps] SET_ENTITY_HEALTH = GET_ENTITY_MAX_HEALTH")
		ENDIF
	ENDIF
	
	SET_PLAYER_PROOFS_FOR_TEAM(iNewTeam)
	
	PRINTLN("[MMacK][TeamSwaps] CLOBBER_OLD_TEAM_DATA_FOR_PLAYER - SUCCESS")
ENDPROC

FUNC PLAYER_INDEX GET_PLAYER_FROM_LOSING_TEAM_FOR_SWAP()
	INT iPart, iPartForSwap, iTopKills
	
	iPartForSwap = iLocalPart //defaults to the local player
	
	IF iPartLastDamager != -1
	AND MC_playerBD[iPartLastDamager].iTeam = 0
	AND NOT MC_PlayerBD[iPartLastDamager].bSwapTeamStarted
	AND NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPartLastDamager))
		PRINTLN("[RCC MISSION][MMacK][ServerSwitch] CACHED DAMAGER - GET_PLAYER_FROM_LOSING_TEAM_FOR_SWAP = ", iPartLastDamager)
		RETURN NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPartLastDamager))
	ENDIF
	
	PRINTLN("[PLAYER_LOOP] - GET_PLAYER_FROM_LOSING_TEAM_FOR_SWAP 4")
	FOR iPart = 0 TO (GET_MAXIMUM_PLAYERS_ON_MISSION() - 1)
		IF (MC_playerBD[iPart].iTeam = 0) //losing team
		AND NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPart))
		AND NOT MC_PlayerBD[iPart].bSwapTeamStarted
		AND iPart != iLocalPart
			PLAYER_INDEX tempPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPart))
			IF IS_NET_PLAYER_OK(tempPlayer)
				IF MC_playerBD[iPart].iNumPlayerKills > iTopKills
					PRINTLN("[RCC MISSION][MMacK][ServerSwitch] TOP KILLER = GET_PLAYER_FROM_LOSING_TEAM_FOR_SWAP = ", iPartForSwap)
					iPartForSwap = iPart
					iTopKills = MC_playerBD[iPart].iNumPlayerKills 
				ENDIF
			ENDIF
		ENDIF
	ENDFOR	
	
	IF iTopKills = 0
		PRINTLN("[RCC MISSION][MMacK][ServerSwitch] NO CANDIDATE")
		//lets see if our part counterpart is good to swap?
		INT iMyTeamPart = 0
		INT iOtherTeamParts = 0
		PRINTLN("[PLAYER_LOOP] - GET_PLAYER_FROM_LOSING_TEAM_FOR_SWAP 3")
		FOR iPart = 0 TO (GET_MAXIMUM_PLAYERS_ON_MISSION() - 1)
			IF (MC_playerBD[iPart].iTeam = 1)
				IF iPart != iLocalPart
					iMyTeamPart++
				ELSE
					PRINTLN("[PLAYER_LOOP] - GET_PLAYER_FROM_LOSING_TEAM_FOR_SWAP 2")
					FOR iOtherTeamParts = 0 TO (GET_MAXIMUM_PLAYERS_ON_MISSION() - 1)
						IF (MC_playerBD[iOtherTeamParts].iTeam = 0)
						AND NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iOtherTeamParts))
							IF iMyTeamPart = 0
								IF NOT MC_PlayerBD[iOtherTeamParts].bSwapTeamStarted
								AND NOT MC_PlayerBD[iOtherTeamParts].bSwapTeamFinished
								AND NOT IS_BIT_SET(MC_serverBD_4.iTeamSwapLockFlag, iOtherTeamParts)
								AND NOT IS_BIT_SET(MC_playerBD[iOtherTeamParts].iClientBitSet, PBBOOL_ANY_SPECTATOR)
									PRINTLN("[RCC MISSION][MMacK][ServerSwitch] COUNTERPART = GET_PLAYER_FROM_LOSING_TEAM_FOR_SWAP = ", iOtherTeamParts)
									RETURN NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iOtherTeamParts))
								ELSE
									BREAKLOOP
								ENDIF	
							ELSE
								iMyTeamPart--
							ENDIF
						ENDIF
					ENDFOR
				ENDIF
			ENDIF
		ENDFOR
		
		PRINTLN("[PLAYER_LOOP] - GET_PLAYER_FROM_LOSING_TEAM_FOR_SWAP")
		FOR iPart = 0 TO (GET_MAXIMUM_PLAYERS_ON_MISSION() - 1)
			IF (MC_playerBD[iPart].iTeam = 0) //losing team
			AND NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPart))
			AND NOT MC_PlayerBD[iPart].bSwapTeamStarted
			AND iPart != iLocalPart
			AND NOT IS_BIT_SET(MC_serverBD_4.iTeamSwapLockFlag, iPart)
			AND NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_ANY_SPECTATOR)		
				PRINTLN("[RCC MISSION][MMacK][ServerSwitch] FIRST AVAILABLE = GET_PLAYER_FROM_LOSING_TEAM_FOR_SWAP = ", iPart)
				RETURN NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPart))	
			ENDIF
		ENDFOR
	ENDIF
	
	PRINTLN("[RCC MISSION][MMacK][ServerSwitch] BAILED")

	RETURN NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPartForSwap))
ENDFUNC

PROC PLAY_TEAM_SWAP_TRANSFORM_EFFECTS()
	SET_PARTICLE_FX_NON_LOOPED_COLOUR(GET_BEAST_PARTICLE_COLOUR(0), GET_BEAST_PARTICLE_COLOUR(1), GET_BEAST_PARTICLE_COLOUR(2))
	USE_PARTICLE_FX_ASSET("scr_powerplay")
	START_NETWORKED_PARTICLE_FX_NON_LOOPED_ON_PED_BONE("scr_powerplay_beast_appear", localPlayerPed, <<0, 0, -0.3>>, <<0, 0, 0>>, BONETAG_PELVIS, 0.75)
ENDPROC

PROC INITIALISE_NEW_TEAM_FOR_PLAYER(INT iNewTeam)
	
	Clear_Any_Objective_Text()
	
	PLAY_TEAM_SWAP_TRANSFORM_EFFECTS()
	APPLY_OUTFIT_TO_LOCAL_PLAYER(INT_TO_ENUM(MP_OUTFIT_ENUM, GET_LOCAL_DEFAULT_OUTFIT(iNewTeam)))
	
	REMOVE_ALL_PLAYERS_WEAPONS()
	IF GIVE_LOCAL_PLAYER_WEAPON_INVENTORY(FMMC_INVENTORY_STARTING, WEAPONINHAND_LASTWEAPON_BOTH, TRUE)
		PRINTLN("[JS][LOCSWAP] - INITIALISE_NEW_TEAM_FOR_PLAYER - Swapped weapons")
		WEAPON_TYPE wtCurrentWeap
		GET_CURRENT_PED_WEAPON(LocalPlayerPed, wtCurrentWeap)
		IF wtCurrentWeap != GET_BEST_PED_WEAPON(LocalPlayerPed)
			SET_CURRENT_PED_WEAPON(LocalPlayerPed, GET_BEST_PED_WEAPON(LocalPlayerPed), TRUE)
		ENDIF
		FORCE_PED_AI_AND_ANIMATION_UPDATE(LocalPlayerPed)
	ENDIF
	
	//VFX/Sounds go here
	
	SET_BIT(iLocalBoolCheck17, LBOOL17_TEAM_SWAP_PICKUP_PROCESS)
	
	iCustomPlayerBlipSetBS = 0
	
	IF SHOULD_USE_CUSTOM_SPAWN_POINTS()	
		PRINTLN("[JS][LOCSWAP] - INITIALISE_NEW_TEAM_FOR_PLAYER -  ADD_ALL_CUSTOM_SPAWN_POINTS")
		ADD_ALL_CUSTOM_SPAWN_POINTS()
	ELSE
		IF IS_BIT_SET(iLocalBoolCheck21, LBOOL21_USING_CUSTOM_RESPAWN_POINTS)
			PRINTLN("[JS][LOCSWAP] - INITIALISE_NEW_TEAM_FOR_PLAYER -  We are using Custom Respawn Points, but they are not valid. Setting should clear")
			SET_BIT(iLocalBoolCheck21, LBOOL21_SHOULD_CLEAR_CUSTOM_RESPAWN_POINTS)
		ENDIF
	ENDIF
		
	RESET_PLAYER_HEALTH()	
	IF MC_serverBD_4.iCurrentHighestPriority[iNewTeam] < FMMC_MAX_RULES
		IF g_FMMC_STRUCT.sFMMCEndConditions[iNewTeam].iMaxHealth[MC_serverBD_4.iCurrentHighestPriority[iNewTeam]] != 0
			bFMMCHealthBoosted = FALSE
			INT iNewMaxHealth = g_FMMC_STRUCT.sFMMCEndConditions[iNewTeam].iMaxHealth[MC_serverBD_4.iCurrentHighestPriority[iNewTeam]]
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iNewTeam].iRuleBitsetEight[MC_serverBD_4.iCurrentHighestPriority[iNewTeam]], ciBS_RULE8_SCALE_MAX_PLAYER_HEALTH)
				INT i
				INT iEnemyPlayers
				FOR i = 0 TO FMMC_MAX_TEAMS - 1
					PRINTLN("[JS][LOCSWAP] - INITIALISE_NEW_TEAM_FOR_PLAYER -  Health is being scaled by players: ", MC_serverBD.iNumberOfPlayingPlayers[i])
					iEnemyPlayers = iEnemyPlayers + MC_serverBD.iNumberOfPlayingPlayers[i]
				ENDFOR
				iNewMaxHealth = (iNewMaxHealth * iEnemyPlayers)
			ENDIF
			INCREASE_PLAYER_HEALTH(iNewMaxHealth)
			PRINTLN("[JS][LOCSWAP] - INITIALISE_NEW_TEAM_FOR_PLAYER - Player health modded to ", iNewMaxHealth)
		ELSE
			PRINTLN("[JS][LOCSWAP] - INITIALISE_NEW_TEAM_FOR_PLAYER - Player health reset")
			SET_ENTITY_HEALTH(LocalPlayerPed, GET_PED_MAX_HEALTH(LocalPlayerPed))
		ENDIF
	ENDIF
	
	SET_PLAYER_PROOFS_FOR_TEAM(iNewTeam)
	
	PRINTLN("[JS][LOCSWAP] - INITIALISE_NEW_TEAM_FOR_PLAYER - SUCCESS")
	CLEAR_BIT(iLocalBoolCheck23, LBOOL23_LOCATE_TEAM_SWAP_INITIALISED)
ENDPROC

PROC SET_TEAM_TARGETS_ON_SWAP(INT iNewTeam)
	SET_PED_RELATIONSHIP_GROUP_HASH(LocalPlayerPed, rgfm_PlayerTeam[iNewTeam])
	
	NETWORK_SET_FRIENDLY_FIRE_OPTION(FALSE)
		
	IF NETWORK_IS_GAME_IN_PROGRESS()
		INT iTeamLoop
		
		FOR iTeamLoop = 0 TO (MC_serverBD.iNumberOfTeams - 1)
			IF DOES_TEAM_LIKE_TEAM(iNewTeam, iTeamLoop)
				SET_PED_CAN_BE_TARGETTED_BY_TEAM(LocalPlayerPed, iTeamLoop, FALSE)
				PRINTLN("[RCC MISSION] CHANGE_PLAYER_TEAM - SET_PED_CAN_BE_TARGETTED_BY_TEAM called with FALSE for team ",iTeamLoop)
		   		NETWORK_OVERRIDE_TEAM_RESTRICTIONS(iTeamLoop, TRUE)
			ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSet, ciUseAllTeamChat)
				SET_PED_CAN_BE_TARGETTED_BY_TEAM(LocalPlayerPed, iTeamLoop, TRUE)
				NETWORK_OVERRIDE_TEAM_RESTRICTIONS(iTeamLoop, TRUE)
				PRINTLN("[RCC MISSION] CHANGE_PLAYER_TEAM - ciUseAllTeamChat called with TRUE for team ",iTeamLoop)
		    ELSE
				SET_PED_CAN_BE_TARGETTED_BY_TEAM(LocalPlayerPed, iTeamLoop, TRUE)
				NETWORK_OVERRIDE_TEAM_RESTRICTIONS(iTeamLoop, FALSE)
				PRINTLN("[RCC MISSION] CHANGE_PLAYER_TEAM - SET_PED_CAN_BE_TARGETTED_BY_TEAM called with TRUE for team ",iTeamLoop)
		    ENDIF
		ENDFOR
	ENDIF
ENDPROC

PROC SET_PLAYER_TEAM_SWAP_DATA(INT iNewTeam)
	IF NOT IS_PED_DEAD_OR_DYING(LocalPlayerPed)
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciCLOBBER_PLAYER_ON_TEAM_SWAP)
			CLOBBER_OLD_TEAM_DATA_FOR_PLAYER(MC_playerBD[iLocalPart].iteam)
			SET_TEAM_TARGETS_ON_SWAP(iNewTeam)
			CLEAR_BIT(iLocalBoolCheck16,LBOOL16_CHANGE_PLAYER_TEAM_ALIVE_VARS_SETTER)
			PRINTLN("[MMacK][TeamSwaps] CHANGE_PLAYER_TEAM - Completed with CLOBBERING TIME!")
		ELIF IS_BIT_SET(iLocalBoolCheck23, LBOOL23_LOCATE_TEAM_SWAP_INITIALISED)
			INITIALISE_NEW_TEAM_FOR_PLAYER(iNewTeam)
			SET_TEAM_TARGETS_ON_SWAP(iNewTeam)
			CLEAR_BIT(iLocalBoolCheck16,LBOOL16_CHANGE_PLAYER_TEAM_ALIVE_VARS_SETTER)
			PRINTLN("[JS][LOCSWAP] CHANGE_PLAYER_TEAM - Completed!")
		ELSE
			SET_TEAM_TARGETS_ON_SWAP(iNewTeam)
			CLEAR_BIT(iLocalBoolCheck16,LBOOL16_CHANGE_PLAYER_TEAM_ALIVE_VARS_SETTER)
			PRINTLN("[MMacK][TeamSwaps] CHANGE_PLAYER_TEAM - Completed!")
		ENDIF	
	#IF IS_DEBUG_BUILD
	ELSE
		PRINTLN("[RCC MISSION] CHANGE_PLAYER_TEAM - Unable to call SET_PED_RELATIONSHIP_GROUP_HASH as player ped is injured!")
	#ENDIF
	ENDIF
ENDPROC

PROC PROCESS_PLAYER_TEAM_SWITCH(INT iVictimPart, PLAYER_INDEX piKiller, BOOL bSuicide = FALSE, BOOL bNoVictim = FALSE)
	PRINTLN("[RCC MISSION][MMacK][ServerSwitch] PROCESS_PLAYER_TEAM_SWITCH INVOKED")
	IF IS_BIT_SET(iLocalBoolCheck20, LBOOL20_JUGGERNAUT_LEAVING_SWAP_DONE)
		CLEAR_BIT(iLocalBoolCheck20, LBOOL20_JUGGERNAUT_LEAVING_SWAP_DONE)
	ENDIF
	
	IF iVictimPart != -1 //save the bother if we have no victim
	OR bNoVictim
		INT iKillerPart = -1
		BOOL bValidLocks
		IF bIsLocalPlayerHost
			IF NETWORK_IS_PLAYER_A_PARTICIPANT(piKiller)
				PARTICIPANT_INDEX piKillerPart = NETWORK_GET_PARTICIPANT_INDEX(piKiller)
				IF NETWORK_IS_PARTICIPANT_ACTIVE(piKillerPart)
					iKillerPart = NATIVE_TO_INT(piKillerPart)
					
					PRINTLN("[RCC MISSION][MMacK][ServerSwitch] iKillerPart = ", iKillerPart)
					PRINTLN("[RCC MISSION][MMacK][ServerSwitch] iVictimPart = ", iVictimPart)
					
					IF iKillerPart != -1 
					AND iKillerPart != iVictimPart 
						IF IS_BIT_SET(MC_serverBD_4.iTeamSwapLockFlag, iKillerPart)
							PRINTLN("[RCC MISSION][MMacK][ServerSwitch] Killer Swap Locked")
						ELIF NOT bNoVictim
						AND IS_BIT_SET(MC_serverBD_4.iTeamSwapLockFlag, iVictimPart)
							PRINTLN("[RCC MISSION][MMacK][ServerSwitch] Victim Swap Locked")
						ELSE
							PRINTLN("[RCC MISSION][MMacK][ServerSwitch] Locks Validated")
							SET_BIT(MC_serverBD_4.iTeamSwapLockFlag, iKillerPart)
							IF NOT bNoVictim
								SET_BIT(MC_serverBD_4.iTeamSwapLockFlag, iVictimPart)
							ENDIF
							bValidLocks = TRUE
						ENDIF
					ELSE
						PRINTLN("[RCC MISSION][MMacK][ServerSwitch] iKillerPart is not determined to be valid")
					ENDIF
				ELSE
					PRINTLN("[RCC MISSION][MMacK][ServerSwitch] Killer is no longer active, purge this request!")
				ENDIF
			ELSE
				PRINTLN("[RCC MISSION][MMacK][ServerSwitch] Killer is no longer participating, purge this request!")
			ENDIF
			
			IF bValidLocks
				PRINTLN("[RCC MISSION][MMacK][ServerSwitch] OK to switch this player, they haven't moved recently")
				IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iKillerPart].iTeam] < FMMC_MAX_RULES
					IF NOT bNoVictim
						IF NOT (MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iVictimPart].iTeam] < FMMC_MAX_RULES)
							PRINTLN("[RCC MISSION][MMacK][ServerSwitch] Invalid Rule")
							EXIT
						ENDIF
					ENDIF
					
					INT iKillerTeam = g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iKillerPart].iTeam].iSwapToTeamOnKill[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iKillerPart].iTeam]]
						
					INT iVictimTeam
					IF NOT bNoVictim
						iVictimTeam = g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iVictimPart].iTeam].iSwapToTeamOnDeath[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iVictimPart].iTeam]]
					ENDIF
					
					IF iKillerTeam != -1 AND iKillerTeam < FMMC_MAX_TEAMS
						IF NOT bNoVictim
							IF NOT (iVictimTeam != -1 AND iVictimTeam < FMMC_MAX_TEAMS)
								PRINTLN("[RCC MISSION][MMacK][ServerSwitch] Invalid Team For Switching")
								EXIT
							ENDIF
						ENDIF
						BROADCAST_SERVER_AUTHORISED_TEAM_SWAP(iKillerPart, iKillerTeam, iVictimPart, iVictimTeam, bSuicide, MC_ServerBD.iSessionScriptEventKey)
						IF bNoVictim
							SET_BIT(iLocalBoolCheck20, LBOOL20_JUGGERNAUT_LEAVING_SWAP_DONE)
						ENDIF
					ELSE
						PRINTLN("[RCC MISSION][MMacK][ServerSwitch] Invalid Team For Switching")
					ENDIF
				ELSE
					PRINTLN("[RCC MISSION][MMacK][ServerSwitch] Invalid Rule")
				ENDIF
			ELSE
				PRINTLN("[RCC MISSION][MMacK][ServerSwitch] Server determined the request to be invalid, reason above")
			ENDIF
		ELSE
			PRINTLN("[RCC MISSION][MMacK][ServerSwitch] I'm not the server, so really should never be seeing this")	
		ENDIF
	ENDIF
ENDPROC

PROC PLAY_JUGGERNAUT_TRANSFORM_EFFECTS()
	IF NOT g_bMissionOver
		SET_PARTICLE_FX_NON_LOOPED_COLOUR(GET_BEAST_PARTICLE_COLOUR(0, TRUE), GET_BEAST_PARTICLE_COLOUR(1, TRUE), GET_BEAST_PARTICLE_COLOUR(2, TRUE))
		USE_PARTICLE_FX_ASSET("scr_powerplay")
		START_NETWORKED_PARTICLE_FX_NON_LOOPED_ON_PED_BONE("scr_powerplay_beast_appear", localPlayerPed, <<0, 0, -0.3>>, <<0, 0, 0>>, BONETAG_PELVIS, 0.75)
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciENABLE_POWER_MAD_SOUNDS)
			PLAY_SOUND_FROM_ENTITY(-1, "Transform_JN_VFX", LocalPlayerPed, "DLC_GR_PM_Juggernaut_Player_Sounds", TRUE)
		ELSE
			PLAY_SOUND_FROM_ENTITY(-1, "Transform_JN_VFX", LocalPlayerPed, "DLC_IE_JN_Player_Sounds", TRUE)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL TRANSFORM_LOCAL_PLAYER_INTO_JUGGERNAUT(BOOL bTransformBack = FALSE, INT iMaxHealth = 1100)
	IF bTransformBack
		IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_TRANSFORMED_INTO_JUGGERNAUT)
			PRINTLN("[JS] TRANSFORM_LOCAL_PLAYER_INTO_JUGGERNAUT - We are not a juggernaut")
			RETURN FALSE
		ENDIF
		
		REINIT_NET_TIMER(tdTransformDelayTimer)
		
		CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_TRANSFORMED_INTO_JUGGERNAUT)
		PRINTLN("[JS] - TRANSFORM_LOCAL_PLAYER_INTO_JUGGERNAUT - NO LONGER THE JUGGERNAUT")
		
		//Audio
		STOP_AUDIO_SCENE("DLC_IE_JN_Player_Is_JN_Scene")
		
		//Outfit
		APPLY_OUTFIT_TO_LOCAL_PLAYER(INT_TO_ENUM(MP_OUTFIT_ENUM, GET_LOCAL_DEFAULT_OUTFIT(MC_playerBD[iLocalPart].iTeam)))
		
		//Weapons
		IF HAS_PED_GOT_WEAPON(LocalPlayerPed, WEAPONTYPE_MINIGUN)
			SET_PED_AMMO(LocalPlayerPed, WEAPONTYPE_MINIGUN, 0, TRUE)
			REMOVE_WEAPON_FROM_PED(LocalPlayerPed, WEAPONTYPE_MINIGUN)
		ENDIF
		IF HAS_PED_GOT_WEAPON(LocalPlayerPed, WEAPONTYPE_DLC_RAILGUN)
			SET_PED_AMMO(LocalPlayerPed, WEAPONTYPE_DLC_RAILGUN, 0, TRUE)
			REMOVE_WEAPON_FROM_PED(LocalPlayerPed, WEAPONTYPE_DLC_RAILGUN)
		ENDIF
		IF HAS_PED_GOT_WEAPON(LocalPlayerPed, WEAPONTYPE_DLC_PROXMINE)
			SET_PED_AMMO(LocalPlayerPed, WEAPONTYPE_DLC_PROXMINE, 0, TRUE)
			REMOVE_WEAPON_FROM_PED(LocalPlayerPed, WEAPONTYPE_DLC_PROXMINE)
		ENDIF
		SET_CAN_PED_SELECT_ALL_WEAPONS(LocalPlayerPed, TRUE)
		WEAPON_TYPE wtCurrentWeap
		GET_CURRENT_PED_WEAPON(LocalPlayerPed, wtCurrentWeap)
		IF wtCurrentWeap != GET_BEST_PED_WEAPON(LocalPlayerPed)
			SET_CURRENT_PED_WEAPON(LocalPlayerPed, GET_BEST_PED_WEAPON(LocalPlayerPed), TRUE)
		ENDIF
		SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_BlockAutoSwapOnWeaponPickups, FALSE)
		BLOCK_ALL_PICKUPS_FOR_LOCAL_PLAYER(FALSE)
		//VFX
		IF bLocalPlayerPedOK
			PLAY_JUGGERNAUT_TRANSFORM_EFFECTS()
		ENDIF
		//Ped Settings
		RESET_PLAYER_HEALTH()
		BLOCK_PLAYER_HEALTH_REGEN(FALSE)
		SET_PLAYER_CAN_USE_COVER(LocalPlayer, TRUE)
		SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_DontActivateRagdollFromExplosions, FALSE)
		IF MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam] < FMMC_MAX_RULES
		AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].iRuleBitsetSix[MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam]], ciBS_RULE6_RESPAWN_ON_RULE_END)
			SET_PLAYER_PROOFS_FOR_TEAM(MC_playerBD[iLocalPart].iTeam)
		ELSE
			IF NOT bLocalPlayerPedOK
				SET_PLAYER_PROOFS_FOR_TEAM(MC_playerBD[iLocalPart].iTeam)
			ENDIF
		ENDIF
		
		IF NOT bLocalPlayerPedOK
			SET_BIT(iLocalBoolCheck23, LBOOL23_CLEANUP_OUTFIT_ON_RESPAWN)
		ENDIF
		
	ELSE
		IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_TRANSFORMED_INTO_JUGGERNAUT)
		OR NOT bLocalPlayerPedOk
		OR IS_PLAYER_RESPAWNING(LocalPlayer)
		OR g_bMissionOver
			PRINTLN("[JS] TRANSFORM_LOCAL_PLAYER_INTO_JUGGERNAUT - We are already a juggernaut or dead")
			RETURN FALSE
		ENDIF
		
		IF HAS_NET_TIMER_STARTED(tdTransformDelayTimer)
			IF NOT HAS_NET_TIMER_EXPIRED(tdTransformDelayTimer, ciTRANSFORM_DELAY_TIME)
				PRINTLN("[JS] TRANSFORM_LOCAL_PLAYER_INTO_JUGGERNAUT - Trying to transform too quickly after another transform")
				RETURN FALSE
			ENDIF
		ENDIF
		
		SET_BIT(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_TRANSFORMED_INTO_JUGGERNAUT)
		PRINTLN("[JS] - TRANSFORM_LOCAL_PLAYER_INTO_JUGGERNAUT - IM THE JUGGERNAUT")
		
		//Audio
		START_AUDIO_SCENE("DLC_IE_JN_Player_Is_JN_Scene")
		PLAY_SOUND_FROM_ENTITY(-1, "Become_JN", LocalPlayerPed, "DLC_GR_PM_Juggernaut_Player_Sounds", TRUE)
		
		//Ped Settings
		SET_PLAYER_CAN_USE_COVER(LocalPlayer, FALSE)
		SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_DontActivateRagdollFromExplosions, TRUE)
		BLOCK_PLAYER_HEALTH_REGEN(TRUE)
		bFMMCHealthBoosted = FALSE
		IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iTeam] < FMMC_MAX_RULES
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iRuleBitsetEight[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iTeam]], ciBS_RULE8_SCALE_MAX_PLAYER_HEALTH)
				INT i
				INT iEnemyPlayers
				FOR i = 0 TO MC_serverBD.iNumberOfTeams - 1
					PRINTLN("[JS] - TRANSFORM_LOCAL_PLAYER_INTO_JUGGERNAUT - Health is being scaled by players: ", MC_serverBD.iNumberOfPlayingPlayers[i])
					iEnemyPlayers = iEnemyPlayers + MC_serverBD.iNumberOfPlayingPlayers[i]
				ENDFOR
				iMaxHealth = (iMaxHealth * iEnemyPlayers)
			ENDIF
		ENDIF
		INCREASE_PLAYER_HEALTH(iMaxHealth)
		SET_ENTITY_PROOFS(LocalPlayerPed, FALSE, FALSE, FALSE, FALSE, TRUE)
		
		//Outfit
		MP_OUTFIT_ENUM eOutfit = OUTFIT_HIDDEN_IE_JN_TARGET_0
		SWITCH(GET_HUD_COLOUR_FOR_FMMC_TEAM(MC_playerBD[iLocalPart].iteam, LocalPlayer))
			CASE HUD_COLOUR_ORANGE
				eOutfit = OUTFIT_HIDDEN_IE_JN4_ORANGE_TARGET_0
			BREAK
			CASE HUD_COLOUR_PURPLE
				eOutfit = OUTFIT_HIDDEN_IE_JN4_PURPLE_TARGET_0
			BREAK
			CASE HUD_COLOUR_GREEN
				eOutfit = OUTFIT_HIDDEN_IE_JN4_GREEN_TARGET_0
			BREAK
			CASE HUD_COLOUR_PINK
				eOutfit = OUTFIT_HIDDEN_IE_JN4_PINK_TARGET_0
			BREAK
		ENDSWITCH
		APPLY_OUTFIT_TO_LOCAL_PLAYER(eOutfit)
		
		//Weapons
		CLEAR_PED_TASKS_IMMEDIATELY(LocalPlayerPed)
		SET_CAN_PED_SELECT_ALL_WEAPONS(LocalPlayerPed, FALSE)
		GIVE_WEAPON_TO_PED(LocalPlayerPed, WEAPONTYPE_DLC_RAILGUN, 2)
		SET_CAN_PED_SELECT_INVENTORY_WEAPON(LocalPlayerPed, WEAPONTYPE_DLC_RAILGUN, TRUE)
		HUD_SET_WEAPON_WHEEL_TOP_SLOT(WEAPONTYPE_DLC_RAILGUN)
		GIVE_WEAPON_TO_PED(LocalPlayerPed, WEAPONTYPE_DLC_PROXMINE, 5)
		SET_CAN_PED_SELECT_INVENTORY_WEAPON(LocalPlayerPed, WEAPONTYPE_DLC_PROXMINE, TRUE)
		HUD_SET_WEAPON_WHEEL_TOP_SLOT(WEAPONTYPE_DLC_PROXMINE)
		GIVE_WEAPON_TO_PED(LocalPlayerPed, WEAPONTYPE_MINIGUN, -1, TRUE)
		SET_CAN_PED_SELECT_INVENTORY_WEAPON(LocalPlayerPed, WEAPONTYPE_MINIGUN, TRUE)
		HUD_SET_WEAPON_WHEEL_TOP_SLOT(WEAPONTYPE_MINIGUN)
		IF HAS_PED_GOT_WEAPON(LocalPlayerPed, WEAPONTYPE_MINIGUN)
			WEAPON_TYPE wtCurrentWeapon
			GET_CURRENT_PED_WEAPON(LocalPlayerPed, wtCurrentWeapon)
			IF wtCurrentWeapon != WEAPONTYPE_MINIGUN
				SET_CURRENT_PED_WEAPON(LocalPlayerPed, WEAPONTYPE_MINIGUN, TRUE)
			ELSE
				PRINTLN("[JS] - TRANSFORM_LOCAL_PLAYER_INTO_JUGGERNAUT - already holding a minigun???")
			ENDIF
		ELSE
			PRINTLN("[JS] - TRANSFORM_LOCAL_PLAYER_INTO_JUGGERNAUT - We don't have a minigun???")
		ENDIF
		SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_BlockAutoSwapOnWeaponPickups, TRUE)
		BLOCK_ALL_PICKUPS_FOR_LOCAL_PLAYER(TRUE)
		
		//VFX
		PLAY_JUGGERNAUT_TRANSFORM_EFFECTS()
	ENDIF
	RETURN TRUE
ENDFUNC

PROC PLAY_BEAST_TRANSFORM_EFFECTS(BOOL bTransformBack = FALSE)
	IF NOT g_bMissionOver
		IF bTransformBack
			SHAKE_GAMEPLAY_CAM("SMALL_EXPLOSION_SHAKE")
			SET_PARTICLE_FX_NON_LOOPED_COLOUR(GET_BEAST_PARTICLE_COLOUR(0, TRUE), GET_BEAST_PARTICLE_COLOUR(1, TRUE), GET_BEAST_PARTICLE_COLOUR(2, TRUE))
			USE_PARTICLE_FX_ASSET("scr_powerplay")
			START_NETWORKED_PARTICLE_FX_NON_LOOPED_ON_PED_BONE("scr_powerplay_beast_vanish", localPlayerPed, <<0, 0, -0.3>>, <<0, 0, 0>>, BONETAG_PELVIS)
			STRING sSoundSet = GET_BEAST_MODE_SOUNDSET()
			PLAY_SOUND_FRONTEND(-1, "Beast_Attack", sSoundSet)
		ELSE
			SHAKE_GAMEPLAY_CAM("SMALL_EXPLOSION_SHAKE")
			SET_PARTICLE_FX_NON_LOOPED_COLOUR(GET_BEAST_PARTICLE_COLOUR(0, TRUE), GET_BEAST_PARTICLE_COLOUR(1, TRUE), GET_BEAST_PARTICLE_COLOUR(2, TRUE))
			USE_PARTICLE_FX_ASSET("scr_powerplay")
			START_NETWORKED_PARTICLE_FX_NON_LOOPED_ON_PED_BONE("scr_powerplay_beast_appear", localPlayerPed, <<0, 0, -0.3>>, <<0, 0, 0>>, BONETAG_PELVIS, 0.75)
			STRING sSoundSet = GET_BEAST_MODE_SOUNDSET()
			PLAY_SOUND_FROM_ENTITY(-1, "Beast_Attack",LocalPlayerPed, sSoundSet, TRUE, 250)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL TRANSFORM_LOCAL_PLAYER_INTO_BEAST(BOOL bTransformBack = FALSE)
	IF bTransformBack
		IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_TRANSFORMED_INTO_BEAST)
			PRINTLN("[JS] TRANSFORM_LOCAL_PLAYER_INTO_BEAST - We are not a beast")
			RETURN FALSE
		ENDIF
		
		REINIT_NET_TIMER(tdTransformDelayTimer)
		
		CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_TRANSFORMED_INTO_BEAST)
		PRINTLN("[JS] - TRANSFORM_LOCAL_PLAYER_INTO_BEAST - NO LONGER THE BEAST")

		//Weapons
		WEAPON_TYPE wtCurrentWeap
		GET_CURRENT_PED_WEAPON(LocalPlayerPed, wtCurrentWeap)
		IF wtCurrentWeap != GET_BEST_PED_WEAPON(LocalPlayerPed)
			SET_CURRENT_PED_WEAPON(LocalPlayerPed, GET_BEST_PED_WEAPON(LocalPlayerPed), TRUE)
		ENDIF
		BLOCK_ALL_PICKUPS_FOR_LOCAL_PLAYER(FALSE)

		//VFX
		IF bLocalPlayerPedOK
			PLAY_BEAST_TRANSFORM_EFFECTS(bTransformBack)
		ENDIF
		
		//Ped Settings
		MC_playerBD[iLocalPart].iBeastAlpha = 255
		IF bLocalPlayerPedOK
			CLEAN_UP_BEAST_MODE(DEFAULT, TRUE, TRUE)
		ELSE
			SET_BIT(iLocalBoolCheck23, LBOOL23_CLEANUP_OUTFIT_ON_RESPAWN)
			CLEAN_UP_BEAST_MODE(DEFAULT, FALSE, TRUE)
		ENDIF
		SET_PLAYER_WEAPON_TAKEDOWN_DEFENSE_MODIFIER(LocalPlayer, 1.0)
	ELSE
		IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_TRANSFORMED_INTO_BEAST)
		OR NOT bLocalPlayerPedOk
		OR IS_PLAYER_RESPAWNING(LocalPlayer)
		OR g_bMissionOver
			PRINTLN("[JS] TRANSFORM_LOCAL_PLAYER_INTO_BEAST - We are already a beast or dead")
			RETURN FALSE
		ENDIF
		
		IF HAS_NET_TIMER_STARTED(tdTransformDelayTimer)
			IF NOT HAS_NET_TIMER_EXPIRED(tdTransformDelayTimer, ciTRANSFORM_DELAY_TIME)
				PRINTLN("[JS] TRANSFORM_LOCAL_PLAYER_INTO_BEAST - Trying to transform too quickly after another transform")
				RETURN FALSE
			ENDIF
		ENDIF
		
		SET_BIT(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_TRANSFORMED_INTO_BEAST)
		PRINTLN("[JS] - TRANSFORM_LOCAL_PLAYER_INTO_BEAST - IM THE BEAST")
		
		//VFX
		PLAY_BEAST_TRANSFORM_EFFECTS(bTransformBack)
		
		//Ped Settings
		bFMMCHealthBoosted = FALSE
		SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_BlockAutoSwapOnWeaponPickups, TRUE)
		BLOCK_ALL_PICKUPS_FOR_LOCAL_PLAYER(TRUE)
		
		SET_PLAYER_WEAPON_TAKEDOWN_DEFENSE_MODIFIER(LocalPlayer, 0.2)
		
		INT iTeam = MC_PlayerBD[iLocalPart].iTeam
		INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]			
		IF iRule < FMMC_MAX_RULES
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFive[iRule], ciBS_RULE5_BEASTMODE_ENABLE_SUPER_JUMP)
			AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFive[iRule], ciBS_RULE5_BEASTMODE_ENABLE_STEALTH)
				PRINT_HELP("BEAST_HELP_06")
			ELIF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFive[iRule], ciBS_RULE5_BEASTMODE_ENABLE_SUPER_JUMP)
				PRINT_HELP("BEAST_HELP_06c")
			ELIF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFive[iRule], ciBS_RULE5_BEASTMODE_ENABLE_STEALTH)
				PRINT_HELP("BEAST_HELP_06b")
			ENDIF
		ENDIF
		
	ENDIF
	RETURN TRUE
ENDFUNC

PROC PROCESS_TRANSFORM_IN_SUDDEN_DEATH(INT iTeam)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciTRANSFORM_TO_JUGGERNAUT_ON_SUDDEN_DEATH)
	AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciTRANSFORM_TO_BEAST_ON_SUDDEN_DEATH)
		// Not using this functionality
		EXIT
	ENDIF
	
	IF NOT ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
	OR bIsAnySpectator
	OR HAS_TEAM_FINISHED(iTeam)
	OR HAS_TEAM_FAILED(iTeam)
		EXIT
	ENDIF
	
	IF DOES_LOCAL_PLAYER_HAVE_NEW_PRIORITY_THIS_FRAME()
		// This functionality is blocked if the rule just started
		EXIT
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciTRANSFORM_TO_JUGGERNAUT_ON_SUDDEN_DEATH)
		IF NOT IS_PARTICIPANT_A_JUGGERNAUT(iLocalPart)
			TRANSFORM_LOCAL_PLAYER_INTO_JUGGERNAUT(DEFAULT, 3000)
		ENDIF
		
	ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciTRANSFORM_TO_BEAST_ON_SUDDEN_DEATH)
		IF NOT IS_PARTICIPANT_A_BEAST(iLocalPart)
			TRANSFORM_LOCAL_PLAYER_INTO_BEAST()
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_CLEANUP_OBJECT_TRANSFORMATION_NOW()
	BOOL bReturn = TRUE
	
	//Don't transform bacj ever during sudden death
	IF (ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
	AND (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciTRANSFORM_TO_JUGGERNAUT_ON_SUDDEN_DEATH)
	OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciTRANSFORM_TO_BEAST_ON_SUDDEN_DEATH)))
		bReturn = FALSE
	ENDIF
	
	RETURN bReturn
ENDFUNC

PROC CLEANUP_OBJECT_TRANSFORMATIONS()
	IF NOT g_bMissionOver
		IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_TRANSFORMED_INTO_JUGGERNAUT)
			TRANSFORM_LOCAL_PLAYER_INTO_JUGGERNAUT(TRUE)
		ENDIF
		
		IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_TRANSFORMED_INTO_BEAST)
			TRANSFORM_LOCAL_PLAYER_INTO_BEAST(TRUE)
		ENDIF
		
		IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_UPGRADED_WEP_VEHICLE)
			UPGRADE_WEAPONISED_VEHICLE(TRUE)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_CURRENT_OUTFIT_A_SLOWING_OUTFIT()
	
	SWITCH GET_STYLE_FROM_OUTFIT(MC_playerBD[iLocalPart].iOutfit)
		CASE OUTFIT_STYLE_CASINO_HEIST_DIRECT_HEAVY_I
		CASE OUTFIT_STYLE_CASINO_HEIST_DIRECT_HEAVY_II
		CASE OUTFIT_STYLE_CASINO_HEIST_DIRECT_HEAVY_III
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC FMMC_PROCESS_HEIST_BAG_GEAR()
	
	IF IS_HEIST_GEAR_A_BAG(sApplyOutfitData.eGear)
		IF IS_PED_WEARING_SCUBA_TANK(LocalPlayerPed)
		OR IS_PLAYER_WEARING_PARACHUTE()
			IF NOT IS_BIT_SET(iLocalBoolCheck27, LBOOL27_BAG_REMOVED_FOR_BACK_GEAR)
				REMOVE_DUFFEL_BAG_HEIST_GEAR(LocalPlayerPed, iLocalPart)
				SET_BIT(iLocalBoolCheck27, LBOOL27_BAG_REMOVED_FOR_BACK_GEAR)
			ENDIF
		ELSE
			IF IS_BIT_SET(iLocalBoolCheck27, LBOOL27_BAG_REMOVED_FOR_BACK_GEAR)
				IF NOT HAS_NET_TIMER_STARTED(tdBackGearRemoveAnimTimer)
					REINIT_NET_TIMER(tdBackGearRemoveAnimTimer)
				ELIF HAS_NET_TIMER_EXPIRED(tdBackGearRemoveAnimTimer, ciBackGearAnimTime)
					APPLY_DUFFEL_BAG_HEIST_GEAR(LocalPlayerPed, iLocalPart)
					CLEAR_BIT(iLocalBoolCheck27, LBOOL27_BAG_REMOVED_FOR_BACK_GEAR)
					RESET_NET_TIMER(tdBackGearRemoveAnimTimer)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

///PURPOSE: This function is called every frame and checks to see if the player's outfit should affect anything
PROC PROCESS_HEIST_OUTFIT_BEHAVIOURS()
	
	IF (g_sMPTunables.bEnable_Heavy_Utility_Vest = TRUE AND NOT IS_PLAYER_ON_ANY_FM_JOB( LocalPlayer))
	OR IS_CURRENT_OUTFIT_A_SLOWING_OUTFIT()
		
		g_bSlowingArmour = IS_CURRENT_OUTFIT_A_SLOWING_OUTFIT()
		
		DO_HEIST_HEAVY_ARMOUR_SLOWDOWN_EFFECT()
		
		IF g_bSlowingArmour
			IF NOT IS_BIT_SET(iLocalBoolCheck33, LBOOL33_HEAVY_ARMOUR_CHECK)
			AND bLocalPlayerOK
				DO_HEIST_HEAVY_ARMOUR_CHECKS()
				SET_BIT(iLocalBoolCheck33, LBOOL33_HEAVY_ARMOUR_CHECK)
			ENDIF
			IF !bLocalPlayerOK
			AND IS_BIT_SET(iLocalBoolCheck33, LBOOL33_HEAVY_ARMOUR_CHECK)
				CLEAR_BIT(iLocalBoolCheck33, LBOOL33_HEAVY_ARMOUR_CHECK)
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySeven, ciOptionsBS27_PoliceOutfitSetsVehNotStolen)
		//Cop outfits allow ambient vehicles to no longer be set as stolen
		IF IS_PLAYER_WEARING_COP_OUTFIT(iLocalPart)
			IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed, TRUE)
				VEHICLE_INDEX tempVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed, TRUE)
				IF IS_VEHICLE_STOLEN(tempVeh)
				AND NETWORK_HAS_CONTROL_OF_ENTITY(tempVeh)
					IF IS_VEHICLE_DRIVEABLE(tempVeh)
						SET_VEHICLE_HAS_BEEN_OWNED_BY_PLAYER(tempVeh, TRUE)
						SET_VEHICLE_IS_STOLEN(tempVeh, FALSE)
						SET_VEHICLE_IS_WANTED(tempVeh, FALSE)
						SET_POLICE_FOCUS_WILL_TRACK_VEHICLE(tempVeh, FALSE)
						SET_VEHICLE_INFLUENCES_WANTED_LEVEL(tempVeh, FALSE)
						PRINTLN("[RCC MISSION] PROCESS_HEIST_OUTFIT_BEHAVIOURS - Player is dressed as a cop, setting current vehicle as not stolen")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	FMMC_PROCESS_HEIST_BAG_GEAR()
	
ENDPROC

PROC APPLY_FEMALE_BIGFOOT_CLOTHES_TEAM(INT iTeam = 0)
	PRINTLN("[MORPHMODEL] APPLY_FEMALE_BIGFOOT_CLOTHES_TEAM - called ")
	
	scrShopPedComponent componentItem
	IF iTeam = 0	//Orange
		GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_REPLAY_F_BERD_0_0), componentItem)
		SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_BERD, componentItem.m_drawableIndex, componentItem.m_textureIndex)
		GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_REPLAY_F_TORSO_0_0), componentItem)
		SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_TORSO, componentItem.m_drawableIndex, componentItem.m_textureIndex)
		SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_LEG, 4, 8)
		SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_FEET, 2, 0)
		SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_TEETH, 0, 0)
		SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_SPECIAL, 3, 0)
		SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_SPECIAL2, 0, 0)
		SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_DECL, 0, 0)
		GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_EXEC_F_JBIB_13_1), componentItem)
		SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_JBIB, componentItem.m_drawableIndex, componentItem.m_textureIndex)
	ELIF iTeam = 1	//Purple
		GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_XMAS3_F_BERD_5_1), componentItem)
		SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_BERD, componentItem.m_drawableIndex, componentItem.m_textureIndex)
		GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_XMAS3_F_TORSO_0_1), componentItem)
		SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_TORSO, componentItem.m_drawableIndex, componentItem.m_textureIndex)
		SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_LEG, 4, 8)
		SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_FEET, 2, 2)
		SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_TEETH, 0, 0)
		SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_SPECIAL, 3, 0)
		SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_SPECIAL2, 0, 0)
		SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_DECL, 0, 0)
		GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_EXEC_F_JBIB_13_2), componentItem)
		SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_JBIB, componentItem.m_drawableIndex, componentItem.m_textureIndex)
	ELIF iTeam = 2	//Pink
		GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_XMAS3_F_BERD_5_2), componentItem)
		SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_BERD, componentItem.m_drawableIndex, componentItem.m_textureIndex)
		GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_XMAS3_F_TORSO_0_2), componentItem)
		SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_TORSO, componentItem.m_drawableIndex, componentItem.m_textureIndex)
		SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_LEG, 4, 8)
		SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_FEET, 2, 4)
		SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_TEETH, 0, 0)
		SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_SPECIAL, 3, 0)
		SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_SPECIAL2, 0, 0)
		SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_DECL, 0, 0)
		GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_EXEC_F_JBIB_13_3), componentItem)
		SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_JBIB, componentItem.m_drawableIndex, componentItem.m_textureIndex)
	ELIF iTeam = 3	//Green
		GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_XMAS3_F_BERD_5_0), componentItem)
		SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_BERD, componentItem.m_drawableIndex, componentItem.m_textureIndex)
		GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_XMAS3_F_TORSO_0_0), componentItem)
		SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_TORSO, componentItem.m_drawableIndex, componentItem.m_textureIndex)
		SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_LEG, 4, 8)
		SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_FEET, 2, 5)
		SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_TEETH, 0, 0)
		SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_SPECIAL, 3, 0)
		SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_SPECIAL2, 0, 0)
		SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_DECL, 0, 0)
		GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_EXEC_F_JBIB_13_0), componentItem)
		SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_JBIB, componentItem.m_drawableIndex, componentItem.m_textureIndex)
	ENDIF
ENDPROC

PROC APPLY_MALE_BIGFOOT_CLOTHES_TEAM(INT iTeam = 0)
	PRINTLN("[MORPHMODEL] APPLY_MALE_BIGFOOT_CLOTHES_TEAM - called ")
	
	scrShopPedComponent componentItem
	
	IF iTeam = 0	//Orange
		GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_REPLAY_M_BERD_0_0), componentItem)
		SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_BERD, componentItem.m_drawableIndex, componentItem.m_textureIndex)
		GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_REPLAY_M_TORSO_0_0), componentItem)
		SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_TORSO, componentItem.m_drawableIndex, componentItem.m_textureIndex)
		SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_LEG, 1, 5)
		SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_FEET, 1, 4)
		SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_TEETH, 0, 0)
		SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_SPECIAL, 15, 0)
		SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_SPECIAL2, 0, 0)
		SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_DECL, 0, 0)
		GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_EXEC_M_JBIB_14_1), componentItem)
		SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_JBIB, componentItem.m_drawableIndex, componentItem.m_textureIndex)
	ELIF iTeam = 1	//Purple
		GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_XMAS3_M_BERD_5_1), componentItem)
		SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_BERD, componentItem.m_drawableIndex, componentItem.m_textureIndex)
		GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_XMAS3_M_TORSO_0_1), componentItem)
		SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_TORSO, componentItem.m_drawableIndex, componentItem.m_textureIndex)
		SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_LEG, 1, 5)
		SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_FEET, 1, 13)
		SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_TEETH, 0, 0)
		SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_SPECIAL, 15, 0)
		SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_SPECIAL2, 0, 0)
		SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_DECL, 0, 0)
		GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_EXEC_M_JBIB_14_2), componentItem)
		SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_JBIB, componentItem.m_drawableIndex, componentItem.m_textureIndex)
	ELIF iTeam = 2	//Pink
		GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_XMAS3_M_BERD_5_2), componentItem)
		SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_BERD, componentItem.m_drawableIndex, componentItem.m_textureIndex)
		GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_XMAS3_M_TORSO_0_2), componentItem)
		SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_TORSO, componentItem.m_drawableIndex, componentItem.m_textureIndex)
		SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_LEG, 1, 5)
		SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_FEET, 1, 12)
		SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_TEETH, 0, 0)
		SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_SPECIAL, 15, 0)
		SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_SPECIAL2, 0, 0)
		SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_DECL, 0, 0)
		GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_EXEC_M_JBIB_14_3), componentItem)
		SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_JBIB, componentItem.m_drawableIndex, componentItem.m_textureIndex)
	ELIF iTeam = 3	//Green
		GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_XMAS3_M_BERD_5_0), componentItem)
		SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_BERD, componentItem.m_drawableIndex, componentItem.m_textureIndex)
		GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_XMAS3_M_TORSO_0_0), componentItem)
		SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_TORSO, componentItem.m_drawableIndex, componentItem.m_textureIndex)
		SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_LEG, 1, 5)
		SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_FEET, 1, 11)
		SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_TEETH, 0, 0)
		SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_SPECIAL, 15, 0)
		SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_SPECIAL2, 0, 0)
		SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_DECL, 0, 0)
		GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_EXEC_M_JBIB_14_0), componentItem)
		SET_PED_COMPONENT_VARIATION(LocalPlayerPed, PED_COMP_JBIB, componentItem.m_drawableIndex, componentItem.m_textureIndex)
	ENDIF
ENDPROC

PROC APPLY_BEAST_COSTUME(INT iTeam = 0)

	IF NOT IS_BIT_SET(iLocalBoolCheck14, LBOOL14_IN_BEAST_COSTUME)
		CLEAR_PED_PROP(LocalPlayerPed, ANCHOR_HEAD)
		REMOVE_PLAYER_HELMET(LocalPlayer, TRUE)
		IF GET_PACKED_STAT_INT(PACKED_CHAR_PICTURE) = 0 //Male
			PRINTLN("[JS] [BEASTMODE] Applying male beast costume")
			APPLY_MALE_BIGFOOT_CLOTHES_TEAM(iTeam)
			SET_BIT(iLocalBoolCheck14, LBOOL14_IN_BEAST_COSTUME)
		ELSE
			APPLY_FEMALE_BIGFOOT_CLOTHES_TEAM(iTeam)
			PRINTLN("[JS] [BEASTMODE] Applying female beast costume")
			SET_BIT(iLocalBoolCheck14, LBOOL14_IN_BEAST_COSTUME)
		ENDIF
	ENDIF
	
ENDPROC

PROC INITIALISE_BEAST_MODE(INT iTeam, INT iRule)
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[JS] [BEASTMODE] !--INITIALISING BEAST MODE--!")
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitSetFive[ iRule ], ciBS_RULE5_BEASTMODE_INCREASE_HEALTH)
			PRINTLN("[JS] [BEASTMODE] INCREASED HEALTH ----- ON (", g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sBeastStruct[ iRule ].iMaxHealth,")")
		ENDIF
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitSetFive[ iRule ], ciBS_RULE5_BEASTMODE_INCREASE_DAMAGE)
			PRINTLN("[JS] [BEASTMODE] INCREASED DAMAGE ----- ON") 
		ENDIF
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitSetFive[ iRule ], ciBS_RULE5_BEASTMODE_ENABLE_STEALTH)
			PRINTLN("[JS] [BEASTMODE] STEALTH -------------- ON") 
			PRINTLN("[JS] [BEASTMODE] STEALTH DAMAGE ------- ", g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sBeastStruct[ iRule ].iStealthDamage,"%")
		ENDIF
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitSetFive[ iRule ], ciBS_RULE5_BEASTMODE_INCREASE_SPEED)
			PRINTLN("[JS] [BEASTMODE] INCREASED SPEED ------ ON (",g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sBeastStruct[ iRule ].fSpeedMod * 100,"%)")
		ENDIF
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitSetFive[ iRule ], ciBS_RULE5_BEASTMODE_ENABLE_SUPER_JUMP)
			PRINTLN("[JS] [BEASTMODE] SUPER JUMP ----------- ON") 
			PRINTLN("[JS] [BEASTMODE] SUPER JUMP DAMAGE ---- ", g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sBeastStruct[ iRule ].iSuperJumpDamage,"%")
		ENDIF
		IF g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sBeastStruct[ iRule ].iHealthOnKill > 0
			PRINTLN("[JS] [BEASTMODE] HEALTH ON KILL ------- ON (", g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sBeastStruct[ iRule ].iHealthOnKill,"%)") 
		ENDIF
		PRINTLN("[JS] [BEASTMODE] MIN POWER HEALTH --------- ", g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sBeastStruct[ iRule ].iMinPowerHealth) 
		PRINTLN("[JS] [BEASTMODE] HEALTH REGEN RATE -------- ", g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sBeastStruct[ iRule ].fHealthRegen * 100, "%") 
	#ENDIF
	
	//Increase health
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitSetFive[ iRule ], ciBS_RULE5_BEASTMODE_INCREASE_HEALTH)
		INT iNewMaxHealth = g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sBeastStruct[ iRule ].iMaxHealth
		INCREASE_PLAYER_HEALTH(iNewMaxHealth)
	ENDIF
				
	//Increase unarmed damage
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitSetFive[ iRule ], ciBS_RULE5_BEASTMODE_INCREASE_DAMAGE)
		SET_PLAYER_MELEE_WEAPON_DAMAGE_MODIFIER(LocalPlayer, 100.0)
		PRINTLN("[JS] [BEASTMODE] Increasing player damage")
	ENDIF
	
	IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBeastStruct[iRule].iMeleeDamageMod != 100
		SET_PLAYER_MELEE_WEAPON_DAMAGE_MODIFIER(LocalPlayer, TO_FLOAT(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBeastStruct[iRule].iMeleeDamageMod))
		PRINTLN("[JS] [BEASTMODE] Setting player damage to specific amount g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBeastStruct[iRule].iMeleeDamageMod: ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBeastStruct[iRule].iMeleeDamageMod)
	ENDIF
					
	//General settings
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSeven[iRule], ciBS_RULE7_TOGGLE_BEAST_FALL_DAMAGE)
		SET_DISABLE_HIGH_FALL_DEATH(LocalPlayerPed, TRUE)
		SET_PLAYER_FALL_DISTANCE_TO_TRIGGER_RAGDOLL_OVERRIDE(LocalPlayer, 85.0)
	ENDIF
	iHealthBeforeStealth = GET_ENTITY_HEALTH(LocalPlayerPed)				
	SET_PLAYER_HEALTH_RECHARGE_MAX_PERCENT(LocalPlayer, 1.0)
	SET_PED_ARMOUR(LocalPlayerPed, 0)
	
	SET_BIT(iLocalBoolCheck14, LBOOL14_IN_BEAST_MODE)
	
ENDPROC

//Should be remote player stuff only
PROC PROCESS_BEAST_MODE_PLAYER_STEALTH(INT iTeam, INT iRule, BOOL bDamaged, BOOL bCanUsePowers)

	//Check if stealth should be enabled
	IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_AIM)
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT)
			IF NOT IS_BIT_SET(iLocalBoolCheck14, LBOOL14_BEAST_STEALTH_TOGGLE)
				IF bCanUsePowers
					IF NOT HAS_NET_TIMER_STARTED(tdStealthToggleTimer)
					OR HAS_NET_TIMER_EXPIRED(tdStealthToggleTimer, 1000)
						SET_BIT(iLocalBoolCheck14, LBOOL14_BEAST_STEALTH_TOGGLE)
						//Play Cloaking sound
						IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSix[iRule], ciBS_RULE6_TOGGLE_BEAST_SOUNDS)
							STRING sSoundSet = GET_BEAST_MODE_SOUNDSET()
							IF iStealthSoundID = -1
								iStealthSoundID = GET_SOUND_ID()
							ENDIF
							PLAY_SOUND_FROM_ENTITY(iStealthSoundID, "Beast_Cloak", localPlayerPed, sSoundSet, TRUE, 60)
						ENDIF
						PRINTLN("[JS] [BEASTMODE] Stealth activated")
						IF HAS_NET_TIMER_STARTED(tdStealthToggleTimer)
							RESET_NET_TIMER(tdStealthToggleTimer)
						ENDIF
						IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwelve[iRule], ciBS_RULE12_HIDE_BEAST_BLIP_IN_STEALTH)
							INT iTeamLoop
							FOR iTeamLoop = 0 TO FMMC_MAX_TEAMS-1
								IF iTeamLoop != iTeam
									HIDE_MY_PLAYER_BLIP_FROM_TEAM(TRUE, iTeamLoop)
									PRINTLN("PROCESS_BEAST_MODE_PLAYER_STEALTH - Hiding blip from team ", iTeamLoop)
								ENDIF
							ENDFOR
						ENDIF
					ENDIF
				ENDIF
			ELSE
				CLEAR_BIT(iLocalBoolCheck14, LBOOL14_BEAST_STEALTH_TOGGLE)
				IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSix[iRule], ciBS_RULE6_TOGGLE_BEAST_SOUNDS)
					STRING sSoundSet = GET_BEAST_MODE_SOUNDSET()
					IF iStealthSoundID = -1
						iStealthSoundID = GET_SOUND_ID()
					ENDIF
					PLAY_SOUND_FROM_ENTITY(iStealthSoundID, "Beast_Uncloak", localPlayerPed, sSoundSet, TRUE, 60)
				ENDIF
				START_NET_TIMER(tdStealthToggleTimer)
				PRINTLN("[JS] [BEASTMODE] Stealth deactivated")
			ENDIF
		ENDIF
	ENDIF
	
	//Check if player should be decloaked due to damage
	IF bDamaged
		IF IS_BIT_SET(iLocalBoolCheck14, LBOOL14_BEAST_STEALTH_TOGGLE)
			START_NET_TIMER(tdStealthToggleTimer)
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSix[iRule], ciBS_RULE6_TOGGLE_BEAST_SOUNDS)
				STRING sSoundSet = GET_BEAST_MODE_SOUNDSET()
				IF iStealthSoundID = -1
					iStealthSoundID = GET_SOUND_ID()
				ENDIF
				PLAY_SOUND_FROM_ENTITY(iStealthSoundID, "Beast_Uncloak", localPlayerPed, sSoundSet, TRUE, 60)
			ENDIF
			CLEAR_BIT(iLocalBoolCheck14, LBOOL14_BEAST_STEALTH_TOGGLE)
			

		ENDIF
		iHealthBeforeStealth = GET_ENTITY_HEALTH(LocalPlayerPed)
		PRINTLN("[JS] [BEASTMODE] - Beast damaged while stealthed, new max regen health = ", iHealthBeforeStealth)
	ENDIF
	
	//Player has toggled stealth on
	IF IS_BIT_SET(iLocalBoolCheck14, LBOOL14_BEAST_STEALTH_TOGGLE)
		//Set alpha to 0 and stop health regenerating
		IF NOT IS_BIT_SET(iLocalBoolCheck14, LBOOL14_BEAST_IS_STEALTHED)
			SET_PLAYER_HEALTH_RECHARGE_MULTIPLIER(localPlayer, 0.0)
			iHealthBeforeStealth = GET_ENTITY_HEALTH(LocalPlayerPed)
			PRINTLN("[JS] [BEASTMODE] Health before stealth = ", iHealthBeforeStealth)
			MC_PlayerBD[iLocalPart].iBeastAlpha = 0
			SET_BIT(iLocalBoolCheck14, LBOOL14_BEAST_IS_STEALTHED)
		ENDIF
		
		//Damage player each second for using stealth
		//Decloak the player if their health is too low to use their powers
		IF bCanUsePowers
			IF NOT HAS_NET_TIMER_STARTED(tdStealthDamageTimer)
				START_NET_TIMER(tdStealthDamageTimer)
			ELSE
				IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdStealthDamageTimer) > 1000
					PRINTLN("[JS] [BEASTMODE] STEALTH DAMAGE: ", (g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sBeastStruct[ iRule ].iMaxHealth / (100 / g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sBeastStruct[ iRule ].iStealthDamage)))
					SET_ENTITY_HEALTH(LocalPlayerPed, (GET_ENTITY_HEALTH(LocalPlayerPed) - (g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sBeastStruct[ iRule ].iMaxHealth / (100 / g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sBeastStruct[ iRule ].iStealthDamage))))
					RESET_NET_TIMER(tdStealthDamageTimer)
				ENDIF
			ENDIF
		ELSE
			CLEAR_BIT(iLocalBoolCheck14, LBOOL14_BEAST_STEALTH_TOGGLE)
		ENDIF
		
	//Player has toggled stealth off or been decloaked due to damage
	ELSE
		//Reset player's alpha
		IF IS_BIT_SET(iLocalBoolCheck14, LBOOL14_BEAST_IS_STEALTHED)
			IF HAS_NET_TIMER_STARTED(tdStealthDamageTimer)
				RESET_NET_TIMER(tdStealthDamageTimer)
			ENDIF
			MC_PlayerBD[iLocalPart].iBeastAlpha = 255
			CLEAR_BIT(iLocalBoolCheck14, LBOOL14_BEAST_IS_STEALTHED)
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwelve[iRule], ciBS_RULE12_HIDE_BEAST_BLIP_IN_STEALTH)
				INT iTeamLoop
				FOR iTeamLoop = 0 TO FMMC_MAX_TEAMS-1
					HIDE_MY_PLAYER_BLIP_FROM_TEAM(FALSE, iTeamLoop)
					PRINTLN("PROCESS_BEAST_MODE_PLAYER_STEALTH - Un-Hiding blip from team ", iTeamLoop)
				ENDFOR
			ENDIF
			
		ELSE
			//Set health to regen up to the health the player had before activating stealth
			IF GET_ENTITY_HEALTH(LocalPlayerPed) < iHealthBeforeStealth
			OR GET_ENTITY_HEALTH(LocalPlayerPed) < (ROUND(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sBeastStruct[ iRule ].iMaxHealth * 0.75) + 100)
			AND bLocalPlayerPedOK
				SET_PLAYER_HEALTH_RECHARGE_MULTIPLIER(localPlayer, g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sBeastStruct[ iRule ].fHealthRegen)
			ELSE
				SET_PLAYER_HEALTH_RECHARGE_MULTIPLIER(localPlayer, 0.0)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_BEAST_MODE_EVERY_FRAME(INT iTeam, INT iRule)

	BOOL bCanUsePowers = FALSE
	
	//Check health for power usage
	IF GET_ENTITY_HEALTH(LocalPlayerPed) > (g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sBeastStruct[ iRule ].iMaxHealth / (100 / g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sBeastStruct[ iRule ].iMinPowerHealth )+100)
		bCanUsePowers = TRUE
	ENDIF
	
	//Stealth
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitSetFive[ iRule ], ciBS_RULE5_BEASTMODE_ENABLE_STEALTH)
		PROCESS_BEAST_MODE_PLAYER_STEALTH(iTeam, iRule, bBeastBeenDamaged, bCanUsePowers)
	ENDIF
	
	//Health on Kill
	IF bBeastKilledPlayer
	AND g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sBeastStruct[ iRule ].iHealthOnKill > 0
	AND bLocalPlayerPedOK
		PRINTLN("[JS] [BEASTMODE] Killed Player, giving ", GET_ENTITY_HEALTH(LocalPlayerPed) + g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sBeastStruct[ iRule ].iMaxHealth / (100 / g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sBeastStruct[ iRule ].iHealthOnKill )," health")
		SET_ENTITY_HEALTH(LocalPlayerPed, (GET_ENTITY_HEALTH(LocalPlayerPed) + (g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sBeastStruct[ iRule ].iMaxHealth / (100 / g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sBeastStruct[ iRule ].iHealthOnKill ))))
	ENDIF

	//Increase Speed
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitSetFive[ iRule ], ciBS_RULE5_BEASTMODE_INCREASE_SPEED)
		RESET_PLAYER_STAMINA(LocalPlayer)
		SET_PED_MOVE_RATE_OVERRIDE(LocalPlayerPed, g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sBeastStruct[ iRule ].fSpeedMod)
		//Increase swim speed (capped at 3.0)
		IF g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sBeastStruct[ iRule ].fSpeedMod <= 3.0
			SET_PED_MOVE_RATE_IN_WATER_OVERRIDE(LocalPlayerPed, g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sBeastStruct[ iRule ].fSpeedMod)
		ELSE
			SET_PED_MOVE_RATE_IN_WATER_OVERRIDE(LocalPlayerPed, 3.0)
		ENDIF
	ENDIF
	
	//Running sounds
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSix[iRule], ciBS_RULE6_TOGGLE_BEAST_SOUNDS)
		IF bLocalPlayerPedOK
			IF IS_PED_SPRINTING(LocalPlayerPed)
			AND NOT (IS_PED_JUMPING(LocalPlayerPed) OR IS_PED_LANDING(localPlayerPed) OR IS_ENTITY_IN_AIR(localPlayerPed))
				IF iBeastSprintSoundID = -1
					iBeastSprintSoundID = GET_SOUND_ID()
				ENDIF
				IF HAS_SOUND_FINISHED(iBeastSprintSoundID)
					STRING sSoundSet = GET_BEAST_MODE_SOUNDSET()					
					PLAY_SOUND_FROM_ENTITY(iBeastSprintSoundID, "Beast_Sprint_Loop", localPlayerPed, sSoundSet, FALSE)
					SET_VARIABLE_ON_SOUND(iBeastSprintSoundID, "Intensity", 1.0)
				ENDIF
			ELIF IS_PED_RUNNING(LocalPlayerPed)		
			AND NOT (IS_PED_JUMPING(LocalPlayerPed) OR IS_PED_LANDING(localPlayerPed) OR IS_ENTITY_IN_AIR(localPlayerPed))
				IF iBeastSprintSoundID = -1
					iBeastSprintSoundID = GET_SOUND_ID()
				ENDIF
				IF HAS_SOUND_FINISHED(iBeastSprintSoundID)
					STRING sSoundSet = GET_BEAST_MODE_SOUNDSET()
					PLAY_SOUND_FROM_ENTITY(iBeastSprintSoundID, "Beast_Sprint_Loop", localPlayerPed, sSoundSet, FALSE)
					SET_VARIABLE_ON_SOUND(iBeastSprintSoundID, "Intensity", 0.7)
				ENDIF
			ELSE
				IF iBeastSprintSoundID > -1
					STOP_SOUND(iBeastSprintSoundID)
				ENDIF
			ENDIF			
		ENDIF
	ENDIF
	
	//Attack sounds
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSix[iRule], ciBS_RULE6_TOGGLE_BEAST_SOUNDS)
		IF (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_ATTACK)
		OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_MELEE_ATTACK1))
		AND HAS_SOUND_FINISHED(iBeastAttackSoundID)
		AND IS_PED_PERFORMING_MELEE_ACTION(LocalPlayerPed)
		
			STRING sSoundSet = GET_BEAST_MODE_SOUNDSET()
			IF iBeastAttackSoundID = -1
				iBeastAttackSoundID = GET_SOUND_ID()
			ENDIF
			PLAY_SOUND_FROM_ENTITY(iBeastAttackSoundID, "Beast_Attack", localPlayerPed, sSoundSet, TRUE, 60)
		ENDIF
	ENDIF

	//Enable super jump
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitSetFive[ iRule ], ciBS_RULE5_BEASTMODE_ENABLE_SUPER_JUMP)
		IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_AIM)
		AND NOT IS_PED_DOING_A_BEAST_JUMP(LocalPlayerPed)
			IF bCanUsePowers
				DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_PHONE)
				SET_PED_RESET_FLAG(LocalPlayerPed, PRF_DisablePlayerVaulting, TRUE)
				SET_SUPER_JUMP_THIS_FRAME(LocalPlayer)
				SET_BEAST_JUMP_THIS_FRAME(LocalPlayer)
				SET_PED_RESET_FLAG(LocalPlayerPed, PRF_DisableParachuting, TRUE)   //Stops a bug where the player parachutes after a super jump.
				IF IS_BIT_SET( iLocalBoolCheck14, LBOOL14_CAN_SHOW_BEAST_POWERS_UNAVAILABLE_HELP_TEXT )
					CLEAR_BIT( iLocalBoolCheck14, LBOOL14_CAN_SHOW_BEAST_POWERS_UNAVAILABLE_HELP_TEXT)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	IF IS_PED_DOING_A_BEAST_JUMP(LocalPlayerPed)
		SET_PED_RESET_FLAG(LocalPlayerPed, PRF_DisableParachuting, TRUE)
		IF NOT IS_BIT_SET( iLocalBooLCheck14, LBOOL14_DAMAGED_BY_SUPER_JUMP )
		AND NOT (IS_PED_CLIMBING(localPlayerPed) OR IS_PED_LANDING(localPlayerPed))
			PRINTLN("[JS] [BEASTMODE] - Beast has triggered a super jump")
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSix[iRule], ciBS_RULE6_TOGGLE_BEAST_SOUNDS)
				STRING sSoundSet = GET_BEAST_MODE_SOUNDSET()
				PLAY_SOUND_FROM_ENTITY(-1, "Beast_Jump", localPlayerPed, sSoundSet, TRUE, 60)
			ENDIF
			SET_ENTITY_HEALTH(LocalPlayerPed, (GET_ENTITY_HEALTH(LocalPlayerPed) - (g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sBeastStruct[ iRule ].iMaxHealth / (100 / g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sBeastStruct[ iRule ].iSuperJumpDamage ))))
			SET_BIT( iLocalBooLCheck14, LBOOL14_DAMAGED_BY_SUPER_JUMP )
		ENDIF
		IF IS_PED_LANDING(localPlayerPed)
			IF NOT IS_ENTITY_IN_AIR(localPlayerPed)
			AND NOT IS_BIT_SET( iLocalBoolCheck14, LBOOL14_DONE_BEAST_LANDING_SOUND )
				IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSix[iRule], ciBS_RULE6_TOGGLE_BEAST_SOUNDS)
					STRING sSoundSet = GET_BEAST_MODE_SOUNDSET()
					PLAY_SOUND_FROM_ENTITY(-1, "Beast_Jump_Land", localPlayerPed, sSoundSet, TRUE, 60)
				ENDIF
				PRINTLN("[JS] [BEASTMODE] Playing jump landing sound")
				SET_BIT( iLocalBoolCheck14, LBOOL14_DONE_BEAST_LANDING_SOUND )
			ENDIF
		ENDIF
	ELSE
		CLEAR_BIT( iLocalBoolCheck14, LBOOL14_DAMAGED_BY_SUPER_JUMP )
		CLEAR_BIT( iLocalBoolCheck14, LBOOL14_DONE_BEAST_LANDING_SOUND )
	ENDIF
	
	//Display help text
	IF NOT bCanUsePowers
		IF iBeastPowersUnavailableDisplayCount < 3
			IF NOT IS_BIT_SET( iLocalBoolCheck14, LBOOL14_CAN_SHOW_BEAST_POWERS_UNAVAILABLE_HELP_TEXT )
				IF NOT IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iTeamBitset, ciBS_TEAM_BLOCK_HELP_TEXT )
				AND NOT IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFifteen[iRule], ciBS_RULE15_BLOCK_BEAST_HELP_TEXT) 
					IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciUSE_IN_AND_OUT_HELPTEXT)
						PRINT_HELP( "BEAST_HELP_01" )
					ENDIF
				ELSE
					PRINTLN( "[JS][BEASTMODE] Blocking beast powers unavailable help text." )
				ENDIF
				SET_BIT( iLocalBoolCheck14, LBOOL14_CAN_SHOW_BEAST_POWERS_UNAVAILABLE_HELP_TEXT)
				iBeastPowersUnavailableDisplayCount++
			ENDIF	
		ENDIF
	ENDIF
	
	IF NOT IS_MP_PASSIVE_MODE_ENABLED()
	AND bLocalPlayerPedOK
		IF GET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_DisableMelee)
			SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_DisableMelee, FALSE)
			PRINTLN("[RCC MISSION] PROCESS_BEAST_MODE_EVERY_FRAME - Setting PCF_DisableMelee to FALSE")
		ENDIF
	ENDIF

ENDPROC

PROC BEAST_MODE_STEALTH_LOOP()

	IF NETWORK_IS_LOCAL_PLAYER_INVINCIBLE()
		IF NOT IS_BIT_SET(mc_playerBD[iLocalPart].iClientBitSet3,PBBOOL3_SPAWN_PROTECTION_ON)
			SET_BIT(mc_playerBD[iLocalPart].iClientBitSet3,PBBOOL3_SPAWN_PROTECTION_ON)
		ENDIF
	ELSE
		IF IS_BIT_SET(mc_playerBD[iLocalPart].iClientBitSet3,PBBOOL3_SPAWN_PROTECTION_ON)
			CLEAR_BIT(mc_playerBD[iLocalPart].iClientBitSet3,PBBOOL3_SPAWN_PROTECTION_ON)
		ENDIF
	ENDIF

	INT iBeastLoop
	PRINTLN("[PLAYER_LOOP] - BEAST_MODE_STEALTH_LOOP")
	FOR iBeastLoop = 0 TO NETWORK_GET_MAX_NUM_PARTICIPANTS() -1
		PARTICIPANT_INDEX tempPart = INT_TO_PARTICIPANTINDEX(iBeastLoop)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
			IF MC_playerBD[iBeastLoop].iBeastAlpha != -1
				
				BOOL bIgnoreSpawnPotection
				IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iBeastLoop].iteam] < FMMC_MAX_RULES
					bIgnoreSpawnPotection = IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iBeastLoop].iteam].iRuleBitsetTwelve[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iBeastLoop].iteam]], ciBS_RULE12_BEAST_STEALTH_IGNORE_SPAWN_PROTECTION)
				ENDIF
				
				
				IF ((NOT IS_BIT_SET(mc_playerBD[iBeastLoop].iClientBitSet3,PBBOOL3_SPAWN_PROTECTION_ON)) OR bIgnoreSpawnPotection)
				AND NOT IS_PLAYER_IN_TEMP_PASSIVE_MODE(NETWORK_GET_PLAYER_INDEX(tempPart))
					IF iBeastLoop = iLocalPart
					OR DOES_TEAM_LIKE_TEAM(MC_playerBD[iBeastLoop].iteam, MC_playerBD[iLocalPart].iteam)
					OR GET_USINGSEETHROUGH()
					OR bIsAnySpectator
						IF (MC_playerBD[iBeastLoop].iBeastAlpha != 255)
							SET_ENTITY_ALPHA( GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(tempPart)), (MC_playerBD[iBeastLoop].iBeastAlpha + 50) , FALSE)
						ELSE
							SET_ENTITY_ALPHA( GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(tempPart)), MC_playerBD[iBeastLoop].iBeastAlpha , FALSE)
						ENDIF
					ELSE
						SET_ENTITY_ALPHA( GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(tempPart)), MC_playerBD[iBeastLoop].iBeastAlpha , FALSE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
ENDPROC

PROC PROCESS_BEAST_MODE_PLAYER()
	
	INT iTeam = MC_PlayerBD[iLocalPart].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	IF iRule < FMMC_MAX_RULES
		
		IF bLocalPlayerPedOk
		
			IF (IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitSetFive[ iRule ], ciBS_RULE5_ENABLE_BEAST_COSTUME)
			OR IS_PARTICIPANT_A_BEAST(iLocalPart))
			AND NOT bIsAnySpectator
				APPLY_BEAST_COSTUME(iTeam)
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitSetFive[ iRule ], ciBS_RULE5_ENABLE_BEAST_MODE)
			OR IS_PARTICIPANT_A_BEAST(iLocalPart)
				LOAD_BEAST_SFX()
				IF NOT IS_BIT_SET(iLocalBoolCheck14, LBOOL14_IN_BEAST_MODE)
					INITIALISE_BEAST_MODE(iTeam, iRule)
				ENDIF
				BEAST_MODE_STEALTH_LOOP()
				PROCESS_BEAST_MODE_EVERY_FRAME(iTeam, iRule)
				
			ELSE
				//Cleanup beast stats if player was in beast mode
				IF IS_BIT_SET(iLocalBoolCheck14, LBOOL14_IN_BEAST_MODE)
					CLEAN_UP_BEAST_MODE()
				ENDIF
				
				//Check if another team are in beast mode
				IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam)
					IF IS_BIT_SET(iLocalBoolCheck14, LBOOL14_OTHER_TEAM_IN_BEAST_MODE)
						CLEAR_BIT(iLocalBoolCheck14, LBOOL14_OTHER_TEAM_IN_BEAST_MODE)
					ENDIF
					INT iTeamLoop
					FOR iTeamLoop = 0 TO (FMMC_MAX_TEAMS - 1)
						IF iTeamLoop != iTeam
							IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeamLoop ].iRuleBitSetFive[ iRule ], ciBS_RULE5_ENABLE_BEAST_MODE)
								SET_BIT(iLocalBoolCheck14, LBOOL14_OTHER_TEAM_IN_BEAST_MODE)
							ENDIF
						ENDIF
					ENDFOR
				ENDIF
				
				INT iPartLoop
				PRINTLN("[PLAYER_LOOP] - PROCESS_BEAST_MODE_PLAYER")
				FOR iPartLoop = 0 TO NETWORK_GET_MAX_NUM_PARTICIPANTS() -1
					IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPartLoop))
						IF IS_PARTICIPANT_A_BEAST(iPartLoop)
							SET_BIT(iLocalBoolCheck14, LBOOL14_OTHER_TEAM_IN_BEAST_MODE)
						ENDIF
					ENDIF
				ENDFOR
				
				IF IS_BIT_SET(iLocalBoolCheck14, LBOOL14_OTHER_TEAM_IN_BEAST_MODE)
				OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_TRANSFORM_ASSETS)
					BEAST_MODE_STEALTH_LOOP()
					LOAD_BEAST_SFX()
				ENDIF
				
			ENDIF
		ELSE
			IF bIsAnySpectator
				BEAST_MODE_STEALTH_LOOP()
			ENDIF
		//If the player is dead then clean up so stats are reapplied
			CLEAN_UP_BEAST_MODE(DEFAULT, FALSE)
		ENDIF
	ENDIF
	
	bBeastBeenDamaged = FALSE
	bBeastKilledPlayer = FALSE
		
ENDPROC

PROC PROCESS_PARACHUTE_REEQUIP_LAST_WEAPON_AFTER_LANDING()
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciPARACHUTE_EQUIP_LAST_WEAPON)
		IF bLocalPlayerPedOK
			IF NOT bParachuteInProgress
				GET_CURRENT_PED_WEAPON(LocalPlayerPed, wtLastWeaponParachute)
				
				IF GET_PED_PARACHUTE_STATE(LocalPlayerPed) != PPS_INVALID // Parachuting...
					bParachuteInProgress = TRUE
				ENDIF
			ELSE
				IF GET_PED_PARACHUTE_STATE(LocalPlayerPed) = PPS_INVALID // Parachuting...
					WEAPON_TYPE wtCurrent
					
					GET_CURRENT_PED_WEAPON(LocalPlayerPed, wtCurrent)
					
					IF wtCurrent != wtLastWeaponParachute
						IF HAS_PED_GOT_WEAPON(LocalPlayerPed, wtLastWeaponParachute)
							SET_CURRENT_PED_WEAPON(LocalPlayerPed, wtLastWeaponParachute, TRUE)
						ENDIF
					ENDIF
					
					bParachuteInProgress = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_LOCAL_PLAYER_DEATH_USES_WHITEOUT_FADING()
	
	IF bIsAnySpectator
	OR DID_I_JOIN_MISSION_AS_SPECTATOR()
	OR g_bMissionOver
		EXIT
	ENDIF
	
	INT iTeam = GET_LOCAL_PLAYER_TEAM(FALSE)
	
	IF iTeam <= -1
	OR iTeam >= FMMC_MAX_TEAMS
		EXIT
	ENDIF
	
	INT iRule = GET_TEAM_CURRENT_RULE(iTeam, FALSE)
	
	IF iRule <= -1
	OR iRule >= FMMC_MAX_RULES
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSixteen[iRule], ciBS_RULE16_PLAYER_DEATH_USES_WHITEOUT_FADING)
		EXIT
	ENDIF
	
	SET_BIG_MESSAGE_SUPPRESS_WASTED(TRUE)
	g_b_SkipKillStrip = TRUE
	g_b_RespawnUseWhiteOutIn = TRUE
	
	PRINTLN("PROCESS_LOCAL_PLAYER_DEATH_USES_WHITEOUT_FADING - g_b_RespawnUseWhiteOutIn = TRUE and g_b_SkipKillStrip = TRUE")
	
ENDPROC
		
PROC PROCESS_CLIENT_LOCATE_TEAM_SWAP()
	INT iTeam = MC_PlayerBD[iLocalPart].iTeam
	INt iPriority = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	IF iPriority < FMMC_MAX_RULES
		BROADCAST_FMMC_OBJECTIVE_PLAYER_SWITCHED_TEAMS(iTeam, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iLocateSwapTeam[iPriority], TRUE)
		CHANGE_PLAYER_TEAM(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iLocateSwapTeam[iPriority], TRUE)
		SET_BIT(iLocalBoolCheck23, LBOOL23_LOCATE_TEAM_SWAP_INITIALISED)
		PRINTLN("[JS][LOCSWAP] - PROCESS_CLIENT_LOCATE_TEAM_SWAP - Swap Complete (client)")
		CLEAR_BIT(iLocalBoolCheck23, LBOOL23_LOCATE_TEAM_SWAP_REQUESTED)
	ENDIF
ENDPROC

PROC PROCESS_TEAM_SWAPPING_ADVERSARY_MODES()
	IF HAS_NET_TIMER_STARTED(tdTeamSwapPassiveModeTimer)
	AND g_MultiplayerSettings.g_bTempPassiveModeSetting
		IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdTeamSwapPassiveModeTimer) >= 5000
			CLEANUP_TEMP_PASSIVE_MODE()
			TURN_OFF_PASSIVE_MODE_OPTION(FALSE)
			
			SET_ENTITY_ALPHA(LocalPlayerPed, 255, FALSE)
						
			SET_PED_RESET_FLAG(LocalPlayerPed, PRF_BlockWeaponFire, FALSE)
			
			NETWORK_SET_FRIENDLY_FIRE_OPTION(FALSE)
			
			RESET_NET_TIMER(tdTeamSwapPassiveModeTimer)
			
			PRINTLN("[MMacK][TeamSwaps] Cleared Passive")
		ELSE
			SET_PED_RESET_FLAG(LocalPlayerPed, PRF_BlockWeaponFire, TRUE)
			SET_PED_RESET_FLAG(LocalPlayerPed, PRF_SuspendInitiatedMeleeActions, TRUE)
		ENDIF
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciCLOBBER_PLAYER_ON_TEAM_SWAP)
		IF NOT IS_BIT_SET(MC_serverBD_4.iTeamSwapLockFlag, iLocalPart)
			IF MC_PlayerBD[iLocalPart].bSwapTeamFinished
				MC_PlayerBD[iLocalPart].bSwapTeamStarted = FALSE
				MC_PlayerBD[iLocalPart].bSwapTeamFinished = FALSE
				
				PROCESS_IMMEDIATE_TEAM_SWAP_SPAWN_POINTS()
				
				PRINTLN("[MMacK][TeamSwaps] SWAP COMPLETE")
			ELSE
				IF MC_PlayerBD[iLocalPart].bSwapTeamStarted
					IF HAS_NET_TIMER_STARTED(tdTeamSwapFailSafe)
						IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdTeamSwapFailSafe) >= ciTIMESTAMP_FOR_TEAM_SWAP
							MC_PlayerBD[iLocalPart].bSwapTeamStarted = FALSE
							MC_PlayerBD[iLocalPart].bSwapTeamFinished = FALSE
							PRINTLN("[MMacK][TeamSwaps] SAFETY BAIL, normally because we killed whilst waiting for a server response for another kill which was rejected")
						ENDIF
					ELSE
						REINIT_NET_TIMER(tdTeamSwapFailSafe)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF HAS_NET_TIMER_STARTED(tdTeamSwapFailSafe)
				IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdTeamSwapFailSafe) >= ciTIMESTAMP_FOR_TEAM_SWAP
					MC_PlayerBD[iLocalPart].bSwapTeamStarted = TRUE
					MC_PlayerBD[iLocalPart].bSwapTeamFinished = TRUE
					PRINTLN("[MMacK][TeamSwaps] SAFETY CLEANUP, the server thinks we are swapping, but we dont. Lets pretend we are clear to cleanup server")
				ENDIF
			ELSE
				REINIT_NET_TIMER(tdTeamSwapFailSafe)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC POST_RULE_OUTFIT_APPLICATION(BOOL &bOutfitSet, INT iRuleOutfit)
	CLEAR_BIT(iLocalBoolCheck7, LBOOL7_OUTFIT_ON_RULE_PROCESSING)
	CLEAR_BIT(iLocalBoolCheck29, LBOOL29_RULE_OUTFIT_CHANGE_REQUEST)
	iRuleOutfitLoading = -1
	iLastOutfit = iRuleOutfit
	MC_playerBD[iLocalPart].iOutfit = iRuleOutfit
	PRINTLN("[RCC MISSION] [PROCESS_RULE_OUTFITS] POST_RULE_OUTFIT_APPLICATION Set Outfit iRuleOutfit > -1 Outfits - MC_playerBD[",iLocalPart,"].iOutfit set as ",iRuleOutfit)
	bOutfitSet = TRUE
	CLEAR_BIT(iLocalBoolCheck26, LBOOL26_PLAYED_OUTFIT_CHANGE_PTFX)
	BROADCAST_GENERAL_EVENT(GENERAL_EVENT_TYPE_PLAYER_CHANGED_APPEARANCE, ALL_PLAYERS())
	IF IS_BIT_SET(iLocalBoolCheck26, LBOOL26_START_OUTFIT_CHANGE_AT_END_OF_CUTSCENE)
		CLEAR_BIT(iLocalBoolCheck26, LBOOL26_START_OUTFIT_CHANGE_AT_END_OF_CUTSCENE)
		PRINTLN("[RCC MISSION] [PROCESS_RULE_OUTFITS] POST_RULE_OUTFIT_APPLICATION iRuleOutfit > -1 CLEAR LBOOL26_START_OUTFIT_CHANGE_AT_END_OF_CUTSCENE")
	ENDIF
	IF IS_BIT_SET(iLocalBoolCheck28, LBOOL28_PRELOAD_OUTFIT_CHANGE_AT_END_OF_CUTSCENE)
		CLEAR_BIT(iLocalBoolCheck28, LBOOL28_PRELOAD_OUTFIT_CHANGE_AT_END_OF_CUTSCENE)
	ENDIF
	START_NET_TIMER(tdChangeOutfitAppearance)
ENDPROC

PROC PROCESS_APPLY_RULE_OUTFIT()
	
	INT iTeam = GET_LOCAL_PLAYER_TEAM()
	INT iRule = GET_LOCAL_PLAYER_CURRENT_RULE()
	
	IF NOT IS_TEAM_VALID(iTeam)
	OR NOT IS_RULE_INDEX_VALID(iRule)
		EXIT
	ENDIF
	
	INT iRuleOutfit = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iOutfit[iRule]
	
	IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iOutfitVariations[iRule] > 0
		iRuleOutfit += NATIVE_TO_INT(LocalPlayer) % g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iOutfitVariations[iRule]
	ENDIF
	BOOL bOutfitSet = FALSE
	
	IF IS_BIT_SET(iLocalBoolCheck26, LBOOL26_START_OUTFIT_CHANGE_AT_END_OF_CUTSCENE)
		INT iRuleToUse = iRule + 1
		IF IS_RULE_INDEX_VALID(iRuleToUse)
			IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iOutfit[iRuleToUse] != FMMC_RULE_OUTFIT_DEFAULT
				iRuleOutfit =  g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iOutfit[iRuleToUse]
				
				IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iOutfitVariations[iRuleToUse] > 0
					iRuleOutfit += NATIVE_TO_INT(LocalPlayer) % g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iOutfitVariations[iRuleToUse]
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF GET_PED_STEALTH_MOVEMENT(LocalPlayerPed)
		PRINTLN("[RCC MISSION][PROCESS_RULE_OUTFITS] PROCESS_APPLY_RULE_OUTFIT was in stealth. setting LBOOL26_PUT_BACK_INTO_STEALTH_MODE")
		SET_BIT(iLocalBoolCheck26, LBOOL26_PUT_BACK_INTO_STEALTH_MODE_OUTFIT_CHANGE)
	ENDIF
	
	PRINTLN("[RCC MISSION] [PROCESS_RULE_OUTFITS] PROCESS_APPLY_RULE_OUTFIT - iRuleOutfit: ", iRuleOutfit, " Default Outfit: ", GET_LOCAL_DEFAULT_OUTFIT(iTeam))
	PRINTLN("[RCC MISSION] [PROCESS_RULE_OUTFITS] PROCESS_APPLY_RULE_OUTFIT - Outfit on Rule: ", IS_BIT_SET(iLocalBoolCheck7, LBOOL7_OUTFIT_ON_RULE_PROCESSING))
	PRINTLN("[RCC MISSION] [PROCESS_RULE_OUTFITS] PROCESS_APPLY_RULE_OUTFIT - Change at end of cutscene: ", IS_BIT_SET(iLocalBoolCheck26, LBOOL26_START_OUTFIT_CHANGE_AT_END_OF_CUTSCENE))
	PRINTLN("[RCC MISSION] [PROCESS_RULE_OUTFITS] PROCESS_APPLY_RULE_OUTFIT - Outfit change request: ", IS_BIT_SET(iLocalBoolCheck29, LBOOL29_RULE_OUTFIT_CHANGE_REQUEST))
	
	//in case a rule gets processed immediately whilst clothes are still loading and the new rule has no clothes  
	IF iRuleOutfit = FMMC_RULE_OUTFIT_DEFAULT
	AND iRuleOutfitLoading != FMMC_RULE_OUTFIT_DEFAULT
		iRuleOutfit = iRuleOutfitLoading
		PRINTLN("[RCC MISSION] [PROCESS_RULE_OUTFITS] PROCESS_APPLY_RULE_OUTFIT iRuleOutfit = iRuleOutfitLoading, iRuleOutfit: ", iRuleOutfit)
	ENDIF
	
	IF iRuleOutfit != iLastOutfit
	OR IS_BIT_SET(iLocalBoolCheck29, LBOOL29_RULE_OUTFIT_CHANGE_REQUEST)
		
		//Need to equip a new outfit this rule				
		IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam)
		AND (iRuleOutfit != iRuleOutfitLoading)
			SET_BIT(iLocalBoolCheck7, LBOOL7_OUTFIT_ON_RULE_PROCESSING)
			iRuleOutfitLoading = iRuleOutfit
			PRINTLN("[RCC MISSION] [PROCESS_RULE_OUTFITS] PROCESS_APPLY_RULE_OUTFIT iRuleOutfit = -2 Set LBOOL7_OUTFIT_ON_RULE_PROCESSING as LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME and  iRuleOutfit != iRuleOutfitLoading, iRuleOutfitLoading: ", iRuleOutfitLoading)
		ENDIF
		
		PLAY_RULE_OUTFIT_CHANGE_PTFX()
						
		//set local struct to use this outfit data to set   
		sApplyOutfitData.pedID 		= LocalPlayerPed
		IF iRuleOutfit = FMMC_RULE_OUTFIT_LOBBY
			sApplyOutfitData.eOutfit 	= INT_TO_ENUM(MP_OUTFIT_ENUM, GET_LOCAL_DEFAULT_OUTFIT(iTeam))
			iRuleOutfit = ENUM_TO_INT(sApplyOutfitData.eOutfit)
			sApplyOutfitData.eMask   	= INT_TO_ENUM(MP_OUTFIT_MASK_ENUM, FMMC_GET_LOCAL_DEFAULT_MASK(iTeam))
		ELSE
			sApplyOutfitData.eOutfit 	= INT_TO_ENUM(MP_OUTFIT_ENUM, iRuleOutfit)
		ENDIF
		
		IF SET_PED_MP_OUTFIT(sApplyOutfitData)
			PRINTLN("[RCC MISSION] [PROCESS_RULE_OUTFITS] PROCESS_APPLY_RULE_OUTFIT - Outfit change complete")
			POST_RULE_OUTFIT_APPLICATION(bOutfitSet, iRuleOutfit)
		ELSE
			PRINTLN("[RCC MISSION] [PROCESS_RULE_OUTFITS] PROCESS_APPLY_RULE_OUTFIT - Still trying to set outfit")
		ENDIF
		
	ELSE
		PRINTLN("[RCC MISSION] [PROCESS_RULE_OUTFITS] PROCESS_APPLY_RULE_OUTFIT - iRuleOutfit is the same as iLastOutfit")
		IF IS_BIT_SET(iLocalBoolCheck26, LBOOL26_START_OUTFIT_CHANGE_AT_END_OF_CUTSCENE)
			CLEAR_BIT(iLocalBoolCheck26, LBOOL26_START_OUTFIT_CHANGE_AT_END_OF_CUTSCENE)
			PRINTLN("[RCC MISSION] [PROCESS_RULE_OUTFITS] PROCESS_APPLY_RULE_OUTFIT iRuleOutfit: ", iRuleOutfit, " No outfit change required CLEAR LBOOL26_START_OUTFIT_CHANGE_AT_END_OF_CUTSCENE")
		ENDIF
		IF IS_BIT_SET(iLocalBoolCheck28, LBOOL28_PRELOAD_OUTFIT_CHANGE_AT_END_OF_CUTSCENE)
			CLEAR_BIT(iLocalBoolCheck28, LBOOL28_PRELOAD_OUTFIT_CHANGE_AT_END_OF_CUTSCENE)
		ENDIF
	ENDIF

	IF bOutfitSet
	AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciENABLE_SMOKE_ON_OUTFIT_CHANGE)
	AND bLocalPlayerPedOK
	AND NOT g_bMissionEnding
	AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
		IF IS_BIT_SET(iLocalBoolCheck26, LBOOL26_PUT_BACK_INTO_STEALTH_MODE_OUTFIT_CHANGE)
			SET_PED_STEALTH_MOVEMENT(LocalPlayerPed, TRUE)
			CLEAR_BIT(iLocalBoolCheck26, LBOOL26_PUT_BACK_INTO_STEALTH_MODE_OUTFIT_CHANGE)
			PRINTLN("[LM][LOADOUT] [PROCESS_RULE_OUTFITS] PROCESS_APPLY_RULE_OUTFIT Putting back into stealth.")
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_RULE_OUTFITS()
	
	IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
		IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + MC_playerBD[iPartToUse].iteam)
		OR IS_BIT_SET(iLocalBoolCheck7, LBOOL7_OUTFIT_ON_RULE_PROCESSING)
		OR IS_BIT_SET(iLocalBoolCheck26, LBOOL26_START_OUTFIT_CHANGE_AT_END_OF_CUTSCENE)
		OR IS_BIT_SET(iLocalBoolCheck29, LBOOL29_RULE_OUTFIT_CHANGE_REQUEST)
			IF NOT IS_PLAYER_SPECTATING(LocalPlayer)
			AND NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_ANY_SPECTATOR)
				PROCESS_APPLY_RULE_OUTFIT()
			ENDIF
		ENDIF
	ENDIF
ENDPROC


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Inventory and Loadout -------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: This section covers the processing the local player's weapons and inventory.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL DOES_PLAYER_HAVE_CONTEXT_TOGGLABLE_WEAPON_COMPONENT_EQUIPPED()

	WEAPON_TYPE wtPlayerWeapon = GET_CURRENT_PLAYER_WEAPON_TYPE()
	
	IF HAS_PED_GOT_WEAPON_COMPONENT(LocalPlayerPed, wtPlayerWeapon, WEAPONCOMPONENT_AT_AR_FLSH)
	OR HAS_PED_GOT_WEAPON_COMPONENT(LocalPlayerPed, wtPlayerWeapon, WEAPONCOMPONENT_AT_PI_FLSH)
	OR HAS_PED_GOT_WEAPON_COMPONENT(LocalPlayerPed, wtPlayerWeapon, WEAPONCOMPONENT_DLC_AT_PI_FLSH_02)
	OR HAS_PED_GOT_WEAPON_COMPONENT(LocalPlayerPed, wtPlayerWeapon, WEAPONCOMPONENT_DLC_AT_SCOPE_NV)
	OR HAS_PED_GOT_WEAPON_COMPONENT(LocalPlayerPed, wtPlayerWeapon, WEAPONCOMPONENT_DLC_AT_SCOPE_THERMAL)
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

PROC PROCESS_INFINITE_AMMO_CREATOR_SETTING()
	IF IS_BIT_SET(iLocalBoolCheck33, LBOOL33_INFINITE_PARACHUTES)
	AND NOT HAS_PED_GOT_WEAPON(LocalPlayerPed, GADGETTYPE_PARACHUTE)
		GIVE_WEAPON_TO_PED(LocalPlayerPed, GADGETTYPE_PARACHUTE, 1, FALSE, FALSE)
		INT iTint = -1
		GET_PED_PARACHUTE_TINT_INDEX(LocalPlayerPed, iTint)
		GIVE_RESERVE_PARACHUTE_TO_PLAYER(iTint)
		SET_PACKED_STAT_BOOL(PACKED_MP_STAT_HAS_RESERVE_PARACHUTE, TRUE)
	ENDIF
ENDPROC

FUNC BOOL DOES_PLAYER_HAVE_A_WEAPON(PED_INDEX piPlayerToCheck)
	IF GET_BEST_PED_WEAPON(piPlayerToCheck, TRUE) = WEAPONTYPE_UNARMED
	OR GET_BEST_PED_WEAPON(piPlayerToCheck, TRUE) = WEAPONTYPE_INVALID
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

PROC GIVE_WEAPON_WITH_ACTION_CHECK(WEAPON_TYPE wtWeaponToUse)
	DEBUG_PRINTCALLSTACK()
	IF NOT IS_PED_CLIMBING(LocalPlayerPed)
	AND NOT IS_PED_SWIMMING(LocalPlayerPed)
		SET_CURRENT_PED_WEAPON(LocalPlayerPed, wtWeaponToUse, TRUE)
	ELSE
		SET_CURRENT_PED_WEAPON(LocalPlayerPed, wtWeaponToUse)
	ENDIF
ENDPROC

PROC BLOCK_WEAPON_ACTIONS_WHILE_SWAPPING()
	IF (IS_PED_RELOADING(LocalPlayerPed) OR IS_PED_RELOADING(localPlayerPed) OR IS_PED_SHOOTING(localPlayerPed) 
	OR IS_PED_PERFORMING_MELEE_ACTION(LocalPlayerPed) OR IS_PED_USING_ACTION_MODE(LocalPlayerPed) OR IS_PED_IN_MELEE_COMBAT(LocalPlayerPed))
	AND NOT GET_PED_STEALTH_MOVEMENT(LocalPlayerPed)
	AND NOT IS_PED_JUMPING(LocalPlayerPed)
	AND NOT IS_PED_CLIMBING(LocalPlayerPed)
		PRINTLN("[JT][LOADOUT] Loadout block Clear Sec Tasks")
		CLEAR_PED_SECONDARY_TASK(LocalPlayerPed)
		SET_PED_USING_ACTION_MODE(LocalPlayerPed, FALSE)
		CLEAR_PED_TASKS(LocalPlayerPed)
	ENDIF
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_UD)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_LR)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_NEXT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_PREV)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_NEXT_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_PREV_WEAPON)
ENDPROC

PROC SET_WEAPON_LOADOUT_FOR_RULE(INT iTeam, INT iRule)

	// Sets a Local Player as ready to receive a new weapon.
	IF IS_BIT_SET(iLocalBoolCheck6, (LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam))
		//Store weapon at the start of weak rules
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSeven[iRule], ciBS_RULE7_WEAPON_LOADOUT1)
			IF DOES_PLAYER_HAVE_A_WEAPON(LocalPlayerPed)
				wtLastWeapon[iLocalPart] = GET_SELECTED_PED_WEAPON(LocalPlayerPed)
			ENDIF
		ENDIF
		SET_BIT(iLocalBoolCheck18, LBOOL18_WEAPON_LOADOUT_READY)
		PRINTLN("[JT][LOADOUT] SETTING BIT: LBOOL18_WEAPON_LOADOUT_READY")
	ENDIF
	
	BOOL bHasRemainingTime = FALSE
	
	IF GET_LOCAL_PLAYER_MULTIRULE_TIMER_TIME_REMAINING() > 1000
	OR GET_SUDDEN_DEATH_TIME_REMAINING(iTeam, iRule) > 1000
	OR GET_OBJECTIVE_TIMER_TIME_REMAINING(iTeam, iRule) > 1000
		bHasRemainingTime = TRUE
	ENDIF
	
	IF g_bMissionEnding
	OR IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
	OR HAS_TEAM_FINISHED(MC_playerBD[iLocalPart].iteam)
	OR HAS_TEAM_FAILED(MC_playerBD[iLocalPart].iteam)
	OR !bLocalPlayerPedOK
		EXIT
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSeven[iRule], ciBS_RULE7_WEAPON_LOADOUT1)
	AND NOT ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
		//Checking if ped is injured before giving weapons
		IF bLocalPlayerPedOK
		AND IS_BIT_SET(iLocalBoolCheck18, LBOOL18_WEAPON_LOADOUT_READY)
		AND bHasRemainingTime
			IF DOES_TEAM_HAVE_AN_FMMC_GIVEN_INVENTORY(iTeam, FALSE, TRUE )
			AND NOT IS_BIT_SET( iLocalBoolCheck4, LBOOL4_BEEN_GIVEN_MIDMISSION_INVENTORY )
				IF iTeam != -1
				AND iRule != -1
					IF MC_serverBD_4.iCurrentHighestPriority[iTeam] >= iRule
					AND MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
						//If option is set in creator, clear all current weapons and armour
						INT iParticipantNumber = iLocalPart
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciMID_MISSION_INVENTORY_REMOVE_WEAPONS)
							
							//Setting wtLastWeapon before clearing weapons
							IF IS_BIT_SET(iLocalBoolCheck13, LBOOL13_BEEN_GIVEN_MIDMISSION_INVENTORY_2)
								wtLastWeapon[iParticipantNumber] = GET_SELECTED_PED_WEAPON(LocalPlayerPed)
								CLEAR_BIT(iLocalBoolCheck13, LBOOL13_BEEN_GIVEN_MIDMISSION_INVENTORY_2)
								PRINTLN("[JT][LOADOUT] Weapon just stored: ", GET_WEAPON_NAME_FROM_WEAPON_TYPE(wtLastWeapon[iParticipantNumber]))
							ENDIF
							IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMidMissionInventoryStartWep != -1
								gweapon_type_CurrentlyHeldWeapon = GET_MISSION_STARTING_WEAPON(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMidMissionInventoryStartWep)
							ENDIF
							PRINTLN("[JT][LOADOUT] wtLastWeapon confirm", GET_WEAPON_NAME_FROM_WEAPON_TYPE(wtLastWeapon[iParticipantNumber]))
							
							IF GET_PED_STEALTH_MOVEMENT(LocalPlayerPed)
								PRINTLN("[JT][LOADOUT] was in stealth. setting LBOOL26_PUT_BACK_INTO_STEALTH_MODE")
								SET_BIT(iLocalBoolCheck26, LBOOL26_PUT_BACK_INTO_STEALTH_MODE_WEP_CHANGE)
							ENDIF
							
							IF (IS_PED_RELOADING(LocalPlayerPed) OR IS_PED_RELOADING(localPlayerPed) OR IS_PED_SHOOTING(localPlayerPed) 
							OR IS_PED_PERFORMING_MELEE_ACTION(LocalPlayerPed) OR IS_PED_USING_ACTION_MODE(LocalPlayerPed) OR IS_PED_IN_MELEE_COMBAT(LocalPlayerPed))
							AND NOT IS_PED_JUMPING(LocalPlayerPed)
							AND NOT IS_PED_CLIMBING(LocalPlayerPed)
							AND NOT GET_PED_STEALTH_MOVEMENT(LocalPlayerPed)
								PRINTLN("[JT][LOADOUT] Loadout 1 Clear Sec Tasks")								
								SET_PED_USING_ACTION_MODE(LocalPlayerPed, FALSE)
							
								CLEAR_PED_TASKS(LocalPlayerPed)
								CLEAR_PED_SECONDARY_TASK(LocalPlayerPed)
							ENDIF
							
							SET_PED_ARMOUR(LocalPlayerPed, 0)
						ENDIF
						
						BLOCK_WEAPON_ACTIONS_WHILE_SWAPPING()
						
						//Giving player the selected loadout
						PRINTLN("[JT][LOADOUT] Loadout 1 is active")
						PRINTLN("[LM][LOADOUT] Should weapon addons and tints be forced off:  ", IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciWEAPONS_FORCE_NO_ADDONS_MID_MISSION_INVENTORY))
						IF GIVE_LOCAL_PLAYER_WEAPON_INVENTORY(FMMC_INVENTORY_MID_MISSION, WEAPONINHAND_LASTWEAPON_BOTH, !IS_JOB_OWNED_WEAPONS_PLUS_PICKUPS())
							SET_PED_ARMOUR(LocalPlayerPed, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iArmourAmount[iRule])
							
							PRINTLN("[JT][LOADOUT] Creator set starting weapon ", GET_WEAPON_NAME_FROM_WEAPON_TYPE(GET_MISSION_STARTING_WEAPON(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMidMissionInventoryStartWep)))
							
							IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMidMissionInventoryStartWep = -1
								PRINTLN("[JT][LOADOUT] STARTING WEAPON Loadout 1. Team: ", iTeam, " Setting as strongest weapon")
								GIVE_WEAPON_WITH_ACTION_CHECK(GET_BEST_PED_WEAPON(LocalPlayerPed))
								SET_BIT(iLocalBoolCheck4, LBOOL4_BEEN_GIVEN_MIDMISSION_INVENTORY)
							ELSE
								IF DOES_PLAYER_HAVE_WEAPON(GET_MISSION_STARTING_WEAPON(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMidMissionInventoryStartWep))
									GIVE_WEAPON_WITH_ACTION_CHECK(GET_MISSION_STARTING_WEAPON(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMidMissionInventoryStartWep))
									SET_BIT(iLocalBoolCheck4, LBOOL4_BEEN_GIVEN_MIDMISSION_INVENTORY)
									PRINTLN("[JT][LOADOUT] STARTING WEAPON Loadout 1. Team: ", iTeam, "   ", GET_WEAPON_NAME_FROM_WEAPON_TYPE(GET_MISSION_STARTING_WEAPON(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMidMissionInventoryStartWep)))
								ENDIF
							ENDIF
							IF DOES_PLAYER_HAVE_A_WEAPON(LocalPlayerPed)
								IF IS_BIT_SET(iLocalBoolCheck4, LBOOL4_BEEN_GIVEN_MIDMISSION_INVENTORY)
									PRINTLN("[JT][LOADOUT] Player has a valid weapon. Clearing ready bit")
									CLEAR_BIT(iLocalBoolCheck18, LBOOL18_WEAPON_LOADOUT_READY)
									
									BLOCK_WEAPON_ACTIONS_WHILE_SWAPPING()
								ENDIF
							ELSE
								CLEAR_BIT(iLocalBoolCheck4, LBOOL4_BEEN_GIVEN_MIDMISSION_INVENTORY)
							ENDIF
							
							IF IS_BIT_SET(iLocalBoolCheck26, LBOOL26_PUT_BACK_INTO_STEALTH_MODE_WEP_CHANGE)
								SET_PED_STEALTH_MOVEMENT(LocalPlayerPed, TRUE)
								CLEAR_BIT(iLocalBoolCheck26, LBOOL26_PUT_BACK_INTO_STEALTH_MODE_WEP_CHANGE)
								PRINTLN("[JT][LOADOUT] Putting back into stealth.")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
			//Reload Ammo
			ELIF DOES_TEAM_HAVE_AN_FMMC_GIVEN_INVENTORY(iTeam, FALSE, TRUE )
			AND IS_BIT_SET( iLocalBoolCheck4, LBOOL4_BEEN_GIVEN_MIDMISSION_INVENTORY )
				PRINTLN("[JT][LOADOUT] Perform a reload. (1)")
				CLEAR_BIT( iLocalBoolCheck4, LBOOL4_BEEN_GIVEN_MIDMISSION_INVENTORY )
			ENDIF
		ENDIF
		
	ELIF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSeven[iRule], ciBS_RULE7_WEAPON_LOADOUT2)
	AND NOT ARE_WE_CURRENTLY_IN_SUDDEN_DEATH()
	
		//Checking if ped is injured before giving weapons
		IF bLocalPlayerPedOK
			IF IS_BIT_SET(iLocalBoolCheck18, LBOOL18_WEAPON_LOADOUT_READY)
			AND bHasRemainingTime
				IF DOES_TEAM_HAVE_AN_FMMC_GIVEN_INVENTORY(iTeam, FALSE, FALSE, TRUE )
				AND NOT IS_BIT_SET( iLocalBoolCheck13, LBOOL13_BEEN_GIVEN_MIDMISSION_INVENTORY_2 )
					IF iTeam != -1
					AND iRule != -1
						IF MC_serverBD_4.iCurrentHighestPriority[iTeam] >= iRule
						AND MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
							//If option is set in creator, clear all current weapons and armour
							IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciMID_MISSION_INVENTORY_2_REMOVE_WEAPONS)
								SET_PED_ARMOUR(LocalPlayerPed, 0)
								//Clearing bit for inventory 1
								IF IS_BIT_SET(iLocalBoolCheck4, LBOOL4_BEEN_GIVEN_MIDMISSION_INVENTORY)
									CLEAR_BIT(iLocalBoolCheck4, LBOOL4_BEEN_GIVEN_MIDMISSION_INVENTORY)
								ENDIF
								
								IF wtLastWeapon[iLocalPart] != WEAPONTYPE_INVALID
									gweapon_type_CurrentlyHeldWeapon = wtLastWeapon[iLocalPart]
									PRINTLN("[JT][LOADOUT] Setting CurrentlyHeldWeapon to: ", GET_WEAPON_NAME_FROM_WEAPON_TYPE(gweapon_type_CurrentlyHeldWeapon))
								ENDIF
							ENDIF
								
							IF GET_PED_STEALTH_MOVEMENT(LocalPlayerPed)
								PRINTLN("[JT][LOADOUT] was in stealth. setting LBOOL26_PUT_BACK_INTO_STEALTH_MODE")
								SET_BIT(iLocalBoolCheck26, LBOOL26_PUT_BACK_INTO_STEALTH_MODE_WEP_CHANGE)
							ENDIF
							
							BLOCK_WEAPON_ACTIONS_WHILE_SWAPPING()
							
							//Giving player the selected loadout
							PRINTLN("[JT][LOADOUT] Loadout 2 is active")
							PRINTLN("[LM][LOADOUT] Should weapon addons and tints be forced off:  ", IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciWEAPONS_FORCE_NO_ADDONS_MID_MISSION_INVENTORY_2))
							IF GIVE_LOCAL_PLAYER_WEAPON_INVENTORY(FMMC_INVENTORY_MID_MISSION_2, WEAPONINHAND_LASTWEAPON_BOTH, !IS_JOB_OWNED_WEAPONS_PLUS_PICKUPS())	
								INT iParticipantNumber = iLocalPart
								
								IF (IS_PED_RELOADING(LocalPlayerPed) OR IS_PED_RELOADING(localPlayerPed) OR IS_PED_SHOOTING(localPlayerPed) 
								OR IS_PED_PERFORMING_MELEE_ACTION(LocalPlayerPed) OR IS_PED_USING_ACTION_MODE(LocalPlayerPed) OR IS_PED_IN_MELEE_COMBAT(LocalPlayerPed))
								AND NOT IS_PED_JUMPING(LocalPlayerPed)
								AND NOT IS_PED_CLIMBING(LocalPlayerPed)
								AND NOT GET_PED_STEALTH_MOVEMENT(LocalPlayerPed)
									PRINTLN("[JT][LOADOUT] Loadout 2 Clear Sec Tasks")									
									SET_PED_USING_ACTION_MODE(LocalPlayerPed, FALSE)
									
									CLEAR_PED_TASKS(LocalPlayerPed)
									CLEAR_PED_SECONDARY_TASK(LocalPlayerPed)
								ENDIF
								
								PRINTLN("[JT][LOADOUT] No weapon yet, wtLastWeapon: ", GET_WEAPON_NAME_FROM_WEAPON_TYPE(wtLastWeapon[iParticipantNumber]))
								SET_BIT(iLocalBoolCheck13, LBOOL13_BEEN_GIVEN_MIDMISSION_INVENTORY_2)
								SET_PED_ARMOUR(LocalPlayerPed, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iArmourAmount[iRule])
								
								PRINTLN("[JT][LOADOUT] Creator set starting weapon 2: ", GET_WEAPON_NAME_FROM_WEAPON_TYPE(GET_MISSION_STARTING_WEAPON(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMidMissionInventory2StartWep)))
								IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMidMissionInventory2StartWep = -1 AND wtLastWeapon[iParticipantNumber] = WEAPONTYPE_INVALID
									PUT_WEAPON_IN_HAND(WEAPONINHAND_LASTWEAPON_BOTH)
									PRINTLN("[JT][LOADOUT] no iMidMissionInventory2StartWep or wtLastWeapon. Giving a weapon from inventory.")
									SET_BIT(iLocalBoolCheck13, LBOOL13_BEEN_GIVEN_MIDMISSION_INVENTORY_2)
								ELSE
									IF wtLastWeapon[iParticipantNumber] = WEAPONTYPE_INVALID OR wtLastWeapon[iParticipantNumber] = WEAPONTYPE_UNARMED
										//Give player the starting weapon if wtLastWeapon is invalid
										IF DOES_PLAYER_HAVE_WEAPON(GET_MISSION_STARTING_WEAPON(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMidMissionInventory2StartWep))
											GIVE_WEAPON_WITH_ACTION_CHECK(GET_MISSION_STARTING_WEAPON(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMidMissionInventory2StartWep))
											PRINTLN("[JT][LOADOUT] STARTING WEAPON Loadout 2. Team: ", iTeam, "   ", GET_WEAPON_NAME_FROM_WEAPON_TYPE(GET_MISSION_STARTING_WEAPON(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMidMissionInventory2StartWep)))
											wtLastWeapon[iParticipantNumber] = GET_MISSION_STARTING_WEAPON(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMidMissionInventory2StartWep)
											PRINTLN("[JT][LOADOUT] Weapon just stored: ", GET_WEAPON_NAME_FROM_WEAPON_TYPE(wtLastWeapon[iParticipantNumber]))
											gweapon_type_CurrentlyHeldWeapon = wtLastWeapon[iParticipantNumber]
											SET_BIT(iLocalBoolCheck13, LBOOL13_BEEN_GIVEN_MIDMISSION_INVENTORY_2)
											PRINTLN("[JT][LOADOUT] Giving player the set starting weapon")
										ENDIF
									ELSE 
										//Give player wtLastWeapon if they have it
										IF DOES_PLAYER_HAVE_WEAPON(wtLastWeapon[iParticipantNumber])
											GIVE_WEAPON_WITH_ACTION_CHECK(wtLastWeapon[iParticipantNumber])
											gweapon_type_CurrentlyHeldWeapon = wtLastWeapon[iParticipantNumber]
											SET_BIT(iLocalBoolCheck13, LBOOL13_BEEN_GIVEN_MIDMISSION_INVENTORY_2)
											PRINTLN("[JT][LOADOUT] Giving player the last weapon they used (wtLastWeapon)")
										ELSE
											//Fallback for if it gets through to here, will give starting weapon.
											IF DOES_PLAYER_HAVE_WEAPON(GET_MISSION_STARTING_WEAPON(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMidMissionInventory2StartWep))
												GIVE_WEAPON_WITH_ACTION_CHECK(GET_MISSION_STARTING_WEAPON(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMidMissionInventory2StartWep))
												wtLastWeapon[iParticipantNumber] = GET_MISSION_STARTING_WEAPON(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMidMissionInventory2StartWep)
												PRINTLN("[JT][LOADOUT] Weapon just stored: ", GET_WEAPON_NAME_FROM_WEAPON_TYPE(wtLastWeapon[iParticipantNumber]))
												gweapon_type_CurrentlyHeldWeapon = wtLastWeapon[iParticipantNumber]
												SET_BIT(iLocalBoolCheck13, LBOOL13_BEEN_GIVEN_MIDMISSION_INVENTORY_2)
												PRINTLN("[JT][LOADOUT] Giving player the starting weapon as a fallback")
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								
								IF DOES_PLAYER_HAVE_A_WEAPON(LocalPlayerPed)
									IF IS_BIT_SET(iLocalBoolCheck13, LBOOL13_BEEN_GIVEN_MIDMISSION_INVENTORY_2)
										CLEAR_BIT(iLocalBoolCheck18, LBOOL18_WEAPON_LOADOUT_READY)
										PRINTLN("[JT][LOADOUT] Player has weapon. Clearing Loadout ready Bit")
										
										BLOCK_WEAPON_ACTIONS_WHILE_SWAPPING()
									ENDIF
								ELSE
									CLEAR_BIT(iLocalBoolCheck13, LBOOL13_BEEN_GIVEN_MIDMISSION_INVENTORY_2)
								ENDIF
								
								IF IS_BIT_SET(iLocalBoolCheck26, LBOOL26_PUT_BACK_INTO_STEALTH_MODE_WEP_CHANGE)
									SET_PED_STEALTH_MOVEMENT(LocalPlayerPed, TRUE)
									CLEAR_BIT(iLocalBoolCheck26, LBOOL26_PUT_BACK_INTO_STEALTH_MODE_WEP_CHANGE)
									PRINTLN("[JT][LOADOUT] Putting back into stealth.")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				
				// Reload Ammo
				ELIF DOES_TEAM_HAVE_AN_FMMC_GIVEN_INVENTORY(iTeam, FALSE, FALSE, TRUE )
				AND IS_BIT_SET( iLocalBoolCheck13, LBOOL13_BEEN_GIVEN_MIDMISSION_INVENTORY_2 )
					PRINTLN("[JT][LOADOUT] perform a reload. (2)")	
					CLEAR_BIT( iLocalBoolCheck13, LBOOL13_BEEN_GIVEN_MIDMISSION_INVENTORY_2 )
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PLAYER_INVENTORY_PROCESSING()
	
	IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
		IF ioldCarryLimit != g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iMaxDeliveryEntitiesCanCarry[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]]
		OR MPGlobalsAmbience.g_bPropertyReset

			IF g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iMaxDeliveryEntitiesCanCarry[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]] > 0
				SET_MAX_NUM_PORTABLE_PICKUPS_CARRIED_BY_PLAYER(DUMMY_MODEL_FOR_SCRIPT, g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iMaxDeliveryEntitiesCanCarry[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]])
				NET_PRINT("[RCC MISSION] player team: ") NET_PRINT_INT(MC_playerBD[iPartToUse].iteam) NET_NL()
				NET_PRINT("[RCC MISSION] current priority: ") NET_PRINT_INT(MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]) NET_NL()
				NET_PRINT("[RCC MISSION] setting max num of pickups for player to: ") NET_PRINT_INT(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iMaxDeliveryEntitiesCanCarry[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]]) NET_NL()
			ELSE
				SET_MAX_NUM_PORTABLE_PICKUPS_CARRIED_BY_PLAYER(DUMMY_MODEL_FOR_SCRIPT, 8)
				NET_PRINT("[RCC MISSION] setting pickups limit back to max: ")NET_NL()
			ENDIF
			
			MPGlobalsAmbience.g_bPropertyReset = FALSE
			ioldCarryLimit = g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iMaxDeliveryEntitiesCanCarry[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]]
		ENDIF
	ENDIF
	
	IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitSetSix[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE6_GET_DELIVER_ICON)
		OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitSetSix[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE6_GET_DELIVER_ICON_LARGE)
			IF iMyOldObjCarryCount != MC_playerBD[iPartToUse].iObjCarryCount
				IF MC_playerBD[iPartToUse].iObjCarryCount > 0
					
					BOOL bLocalIcon
					
					eMP_TAG modelTag = MP_TAG_PACKAGES
					
					IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitSetSix[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE6_GET_DELIVER_ICON_LOCAL)
						PRINTLN("[LH][OBJ_ICON] UPDATE_INVENTORY_BOX_CONTENT LOCAL MODE")
						bLocalIcon = TRUE
					ENDIF
					
					IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitSetSix[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE6_GET_DELIVER_ICON_LARGE)
						PRINTLN("[LH][OBJ_ICON] UPDATE_INVENTORY_BOX_CONTENT LARGE PACKAGE")
						modelTag = MP_TAG_PACKAGE_LARGE
					ENDIF
					
					PRINTLN("[LH][OBJ_ICON] UPDATE_INVENTORY_BOX_CONTENT")
					UPDATE_INVENTORY_BOX_CONTENT(ci_Obj_inventory, modelTag, TRUE,MC_playerBD[iPartToUse].iObjCarryCount, DEFAULT, DEFAULT, bLocalIcon)
					
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciDELAYED_ICON_REMOVAL)
						IF HAS_NET_TIMER_STARTED(stIconRemovalDelayTimer)
							RESET_NET_TIMER(stIconRemovalDelayTimer)
						ENDIF
					ENDIF
					
				ELSE
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciDELAYED_ICON_REMOVAL)
						IF NOT HAS_NET_TIMER_STARTED(stIconRemovalDelayTimer)
							START_NET_TIMER(stIconRemovalDelayTimer)
						ENDIF
					ELSE
						PRINTLN("[LH][OBJ_ICON] CLEAR_A_INVENTORY_BOX_CONTENT")
						CLEAR_A_INVENTORY_BOX_CONTENT(ci_Obj_inventory)
					ENDIF
				ENDIF
				iMyOldObjCarryCount = MC_playerBD[iPartToUse].iObjCarryCount
				PRINTLN("[KH] iMyOldObjCarryCount UPDATED: ", iMyOldObjCarryCount)
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciDELAYED_ICON_REMOVAL)
		IF HAS_NET_TIMER_STARTED(stIconRemovalDelayTimer)
			INT iTimeElapsed = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stIconRemovalDelayTimer)						
			IF iTimeElapsed > iIconRemovalTime
				RESET_NET_TIMER(stIconRemovalDelayTimer)
				PRINTLN("[LH][KH][OBJ_ICON] CLEAR_A_INVENTORY_BOX_CONTENT")
				CLEAR_A_INVENTORY_BOX_CONTENT(ci_Obj_inventory)
			ENDIF
		ENDIF
	ENDIF
	
	IF GET_MC_CLIENT_MISSION_STAGE(iPartToUse) = CLIENT_MISSION_STAGE_GET_MASK
		
		IF IS_FAKE_MULTIPLAYER_MODE_SET()
			//We can't equip a mask, so skip past this rule
			SET_BIT(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_WEARING_MASK)
		ELSE
			
			IF (NOT IS_INTERACTION_MENU_OPEN())
				IF (NOT IS_PED_COMP_ITEM_CURRENT_MP(LocalPlayerPed, COMP_TYPE_BERD, BERD_FMM_0_0))
				OR IS_MP_HEIST_MASK_EQUIPPED(LocalPlayerPed,INT_TO_ENUM(MP_OUTFIT_MASK_ENUM, FMMC_GET_LOCAL_DEFAULT_MASK(MC_playerBD[iPartToUse].iteam)))
					IF bLocalPlayerOK
						SET_BIT(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_WEARING_MASK)
					ENDIF
				ELSE
					CLEAR_BIT(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_WEARING_MASK)
				ENDIF
			ENDIF
			
		ENDIF
		
		IF NOT IS_BIT_SET(iLocalBoolCheck4,LBOOL4_OWN_MASK)
			
			VECTOR vMaskShop = <<-1337.15, -1278.16, 3.87>>
			//If we're more than 8m away from the mask shop:
			IF VDIST2(GET_ENTITY_COORDS(LocalPlayerPed),vMaskShop) > 64
				
				IF IS_BIT_SET(iLocalBoolCheck4,LBOOL4_IN_MASK_SHOP)
					
					CLEAR_BIT(iLocalBoolCheck4,LBOOL4_IN_MASK_SHOP)
					
					IF DO_I_HAVE_A_MASK()
						SET_BIT(iLocalBoolCheck4,LBOOL4_OWN_MASK)
						IF DOES_BLIP_EXIST(MaskShopBlip)
							REMOVE_BLIP(MaskShopBlip)
						ENDIF
					ELSE
						IF DOES_BLIP_EXIST(MaskShopBlip)
							SET_BLIP_ROUTE(MaskShopBlip,TRUE)
						ENDIF
					ENDIF
					
				ELSE
					IF NOT DOES_BLIP_EXIST(MaskShopBlip)
						MaskShopBlip=ADD_BLIP_FOR_COORD(vMaskShop)
						SET_BLIP_SPRITE(MaskShopBlip,RADAR_TRACE_MASK)
						SET_BLIP_ROUTE(MaskShopBlip,TRUE)
						SET_BLIP_PRIORITY(MaskShopBlip,BLIPPRIORITY_HIGHEST)
						SET_BLIP_HIDDEN_ON_LEGEND(MaskShopBlip,TRUE)
					ENDIF
				ENDIF
			ELSE
			
				SET_BIT(iLocalBoolCheck4,LBOOL4_IN_MASK_SHOP)
				IF DOES_BLIP_EXIST(MaskShopBlip)
					SET_BLIP_ROUTE(MaskShopBlip,FALSE)
				ENDIF
				
				IF IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()
					SET_BIT(iLocalBoolCheck4,LBOOL4_BROWSING_SHOP)
				ELSE
					IF IS_BIT_SET(iLocalBoolCheck4,LBOOL4_BROWSING_SHOP)
						IF DO_I_HAVE_A_MASK()
							SET_BIT(iLocalBoolCheck4,LBOOL4_OWN_MASK)
							IF DOES_BLIP_EXIST(MaskShopBlip)
								REMOVE_BLIP(MaskShopBlip)
							ENDIF
						ENDIF
						CLEAR_BIT(iLocalBoolCheck4,LBOOL4_BROWSING_SHOP)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF DOES_BLIP_EXIST(MaskShopBlip)
				REMOVE_BLIP(MaskShopBlip)
			ENDIF
			
			IF NOT IS_THIS_A_HEIST_MISSION()
				IF (NOT IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_WEARING_MASK))
				AND (NOT IS_INTERACTION_MENU_OPEN())
					//If we aren't wearing a mask, remind the player how they can equip it
					//Should use input_interaction_menu for NG
					DISPLAY_HELP_TEXT_THIS_FRAME("MC_MASK_HELP",TRUE)
				ELSE
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MC_MASK_HELP")
						CLEAR_HELP()
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF DOES_BLIP_EXIST(MaskShopBlip)
			REMOVE_BLIP(MaskShopBlip)
		ENDIF
	ENDIF
	
	//Uses inventory system to draw HUD elements but doesn't relie upon picking up objects 
	PROCESS_PLAYER_USING_TRACKIFY_HUD()
	
ENDPROC

PROC PROCESS_PLAYER_MISSION_PARACHUTES()
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciRESPAWN_WITH_PARACHUTE_EQUIPPED)
		
		IF NOT IS_PLAYER_WEARING_PARACHUTE()
			MPGlobalsAmbience.bDisableTakeOffChute = TRUE
			EQUIP_STORED_MP_PARACHUTE(PLAYER_ID(), g_FMMC_STRUCT.iTeamParachuteTint[g_i_Mission_team], g_FMMC_STRUCT.iTeamParachutePackTint[g_i_Mission_team])			
		ENDIF
		
		MAINTAIN_CHUTE_ON()
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_ENABLE_TEAM_COLOUR_PARACHUTES)
		IF NOT IS_BIT_SET(iLocalBoolCheck26, LBOOL26_PARACHUTE_OVERRIDDEN_TO_TEAM_COLOUR)
			SET_PLAYER_PARACHUTE_CANOPY(LocalPlayer, g_i_Mission_team, PARACHUTE_CANOPY_TEAM_COLOUR)
			SET_BIT(iLocalBoolCheck26, LBOOL26_PARACHUTE_OVERRIDDEN_TO_TEAM_COLOUR)
			PRINTLN("[RCC MISSION] Parachute model and colour have been overridden to use team colour parachutes.")
		ELSE
			IF !bLocalPlayerPedOK
				CLEAR_BIT(iLocalBoolCheck26, LBOOL26_PARACHUTE_OVERRIDDEN_TO_TEAM_COLOUR)
			ENDIF
		ENDIF
	ENDIF
		
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciDONT_REMOVE_PARACHUTE)
		SET_PED_RESET_FLAG(LocalPlayerPed, PRF_DisableTakeOffParachutePack, TRUE)
	ENDIF
ENDPROC

PROC PROCESS_WEAPON_AUTO_FLASHLIGHT()
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySeven, ciOptionsBS27_EnableFlashlightAttachmentByDefault)
		EXIT
	ENDIF
	
	WEAPON_TYPE wtCurrentWeapon
	IF GET_CURRENT_PED_WEAPON(LocalPlayerPed, wtCurrentWeapon)
		IF GET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_IsAimingGun)
			IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_CONTEXT)
			AND GET_FLASHLIGHT_FOR_FMMC_STARTING_WEAPON(wtCurrentWeapon) != WEAPONCOMPONENT_INVALID
				TOGGLE_BIT(iLocalBoolCheck33, LBOOL33_PLAYER_DISABLED_THEIR_OWN_FLASHLIGHT)
				PRINTLN("PROCESS_WEAPON_AUTO_FLASHLIGHT | Toggling LBOOL33_PLAYER_DISABLED_THEIR_OWN_FLASHLIGHT")
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_FLASH_LIGHT_ON(LocalPlayerPed)	
		// If it's not on right now, and there's no reason for us to want it to be off, then set LBOOL33_FLASHLIGHT_NEEDS_TO_BE_TURNED_ON
		IF NOT IS_BIT_SET(iLocalBoolCheck33, LBOOL33_PLAYER_DISABLED_THEIR_OWN_FLASHLIGHT)
			SET_BIT(iLocalBoolCheck33, LBOOL33_FLASHLIGHT_NEEDS_TO_BE_TURNED_ON)
			PRINTLN("PROCESS_WEAPON_AUTO_FLASHLIGHT | Setting LBOOL33_FLASHLIGHT_NEEDS_TO_BE_TURNED_ON")
		ENDIF
	ENDIF
	
	// If we want it to turn on, try to turn it on
	IF IS_BIT_SET(iLocalBoolCheck33, LBOOL33_FLASHLIGHT_NEEDS_TO_BE_TURNED_ON)
		SET_FLASH_LIGHT_ACTIVE_HISTORY(LocalPlayerPed, TRUE)
		PRINTLN("PROCESS_WEAPON_AUTO_FLASHLIGHT | Calling SET_FLASH_LIGHT_ACTIVE_HISTORY")
		
		IF IS_FLASH_LIGHT_ON(LocalPlayerPed)
			// The flashlight is definitely on now, so we can clear LBOOL33_FLASHLIGHT_NEEDS_TO_BE_TURNED_ON
			CLEAR_BIT(iLocalBoolCheck33, LBOOL33_FLASHLIGHT_NEEDS_TO_BE_TURNED_ON)
			PRINTLN("PROCESS_WEAPON_AUTO_FLASHLIGHT | Clearing LBOOL33_FLASHLIGHT_NEEDS_TO_BE_TURNED_ON")
		ENDIF
	ENDIF
ENDPROC

// FMMC2020 - Refactor. Do spawn points elsewhere.
PROC PROCESS_WEAPON_AND_LOADOUTS()
	
	PROCESS_WEAPON_AUTO_FLASHLIGHT()
	
	IF NOT bIsAnySpectator
	OR USING_HEIST_SPECTATE()
	
		INT iTeam = MC_playerBD[iPartToUse].iteam
		INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
		
		// Nothing here checks that the player is alive ?
		IF DOES_TEAM_HAVE_AN_FMMC_GIVEN_INVENTORY( MC_playerBD[iPartToUse].iTeam, FALSE, TRUE )
		AND NOT IS_BIT_SET( iLocalBoolCheck4, LBOOL4_BEEN_GIVEN_MIDMISSION_INVENTORY ) 
			//Haven't been given my starting inventory yet:
			INT iTriggerTeam = g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iMidMissionInventoryGiveOnTeam
			INT iTriggerRule = g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iMidMissionInventoryGiveOnTeamRule
						
			IF iTriggerTeam != -1
			AND iTriggerRule != -1
			
				BOOL bOnlyTriggerOnQuickRestart = IS_BIT_SET(g_FMMC_STRUCT.sTeamData[iTriggerTeam].iTeamBitSet, ciTEAM_BS_MID_MISSION_INVENTORY_RESTART_ONLY)
				
				PRINTLN("[LM][LOADOUT][MID] - (1) iTriggerTeam: ", iTriggerTeam, " iTriggerRule: ", iTriggerRule, " bOnlyTriggerOnQuickRestart: ", bOnlyTriggerOnQuickRestart)
				IF IS_BIT_SET( iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTriggerTeam )				
					PRINTLN("[LM][LOADOUT][MID] - (1) New Frame")
					
					IF MC_serverBD_4.iCurrentHighestPriority[iTriggerTeam] >= iTriggerRule
					AND MC_serverBD_4.iCurrentHighestPriority[iTriggerTeam] < FMMC_MAX_RULES
						IF NOT bOnlyTriggerOnQuickRestart
						OR ( bOnlyTriggerOnQuickRestart AND IS_THIS_A_QUICK_RESTART_JOB() )
							GIVE_LOCAL_PLAYER_WEAPON_INVENTORY(FMMC_INVENTORY_MID_MISSION, WEAPONINHAND_LASTWEAPON_BOTH)
							SET_BIT(iLocalBoolCheck4, LBOOL4_BEEN_GIVEN_MIDMISSION_INVENTORY)
							PRINTLN("[LM][LOADOUT][MID] - (1) Setting LBOOL4_BEEN_GIVEN_MIDMISSION_INVENTORY")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		// Nothing here checks that the player is alive ?
		IF DOES_TEAM_HAVE_AN_FMMC_GIVEN_INVENTORY( MC_playerBD[iPartToUse].iTeam, FALSE, FALSE, TRUE )
		AND NOT IS_BIT_SET( iLocalBoolCheck13, LBOOL13_BEEN_GIVEN_MIDMISSION_INVENTORY_2 )
			//Haven't been given my starting inventory yet:
			INT iTriggerTeam = g_FMMC_STRUCT.sFMMCEndConditions[GET_LOCAL_PLAYER_TEAM()].iMidMissionInventory2GiveOnTeam
			INT iTriggerRule = g_FMMC_STRUCT.sFMMCEndConditions[GET_LOCAL_PLAYER_TEAM()].iMidMissionInventory2GiveOnTeamRule
			
			
			
			IF iTriggerTeam != -1
			AND iTriggerRule != -1
				
				BOOL bOnlyTriggerOnQuickRestart = IS_BIT_SET(g_FMMC_STRUCT.sTeamData[iTriggerTeam].iTeamBitSet, ciTEAM_BS_MID_MISSION_INVENTORY_2_RESTART_ONLY)
				
				PRINTLN("[LM][LOADOUT][MID] - (2) iTriggerTeam: ", iTriggerTeam, " iTriggerRule: ", iTriggerRule, " bOnlyTriggerOnQuickRestart: ", bOnlyTriggerOnQuickRestart)
				IF IS_BIT_SET( iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTriggerTeam )			
					PRINTLN("[LM][LOADOUT][MID] - (2) New Frame")
				
					IF MC_serverBD_4.iCurrentHighestPriority[iTriggerTeam] >= iTriggerRule
					AND MC_serverBD_4.iCurrentHighestPriority[iTriggerTeam] < FMMC_MAX_RULES
						IF NOT bOnlyTriggerOnQuickRestart
						OR ( bOnlyTriggerOnQuickRestart AND IS_THIS_A_QUICK_RESTART_JOB() )
							GIVE_LOCAL_PLAYER_WEAPON_INVENTORY(FMMC_INVENTORY_MID_MISSION_2, WEAPONINHAND_LASTWEAPON_BOTH)
							SET_BIT(iLocalBoolCheck13, LBOOL13_BEEN_GIVEN_MIDMISSION_INVENTORY_2)
							PRINTLN("[LM][LOADOUT][MID] - (2) Setting LBOOL13_BEEN_GIVEN_MIDMISSION_INVENTORY_2")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		IF iRule < FMMC_MAX_RULES		
			SET_WEAPON_LOADOUT_FOR_RULE(iTeam, iRule)
		ENDIF
	ENDIF
ENDPROC


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Visual Aids  ----------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: This section covers the players ability to utilize visual aids such as Thermal Vision and Night Vision.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL ENABLE_HUNTER_VISION(INT &iRule, INT &iTeam)
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFifteen[iRule], ciBS_RULE15_OVERRIDE_THERMAL_VISION_WITH_HUNTER_EFFECT)
	OR IS_BIT_SET(iLocalBoolCheck14, LBOOL14_HUNTER_VISION_COOLDOWN)
		RETURN FALSE
	ENDIF

	RETURN IS_BIT_SET(iLocalBoolCheck12, LBOOL12_USE_HUNTER_VISION)
ENDFUNC

PROC INIT_PLAYER_VISUAL_AID_THERMAL_HUNTER_MODE_OVERRIDE(INT &iTeam, INT &iRule)
	
	SEETHROUGH_RESET()
	
	SEETHROUGH_SET_HEATSCALE(TB_DEAD, 0.0)
	SEETHROUGH_SET_HEATSCALE(TB_COLD, 1.0)
	SEETHROUGH_SET_HEATSCALE(TB_WARM, 1.0) 
	SEETHROUGH_SET_HEATSCALE(TB_HOT, 1.0) 

	SEETHROUGH_SET_MAX_THICKNESS(250.0)
	
	SEETHROUGH_SET_NOISE_MIN(0.01)
	SEETHROUGH_SET_NOISE_MAX(0.2)
	SEETHROUGH_SET_HILIGHT_INTENSITY(0.35)
	SEETHROUGH_SET_HIGHLIGHT_NOISE(0.3)
	
	//Change the heatscale for the player and their teammates
	INT iPlayerIndex
	FOR iPlayerIndex = 0 TO MAX_NUM_MC_PLAYERS - 1
		IF NOT DOES_TEAM_LIKE_TEAM(iTeam, MC_playerBD[iPlayerIndex].iTeam)
			RELOOP
		ENDIF
		
		PARTICIPANT_INDEX piTemp = INT_TO_PARTICIPANTINDEX(iPlayerIndex)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(piTemp)
			PLAYER_INDEX piTempPlayer = NETWORK_GET_PLAYER_INDEX(piTemp)
			IF IS_ENTITY_ALIVE(GET_PLAYER_PED(piTempPlayer))
				SET_PED_HEATSCALE_OVERRIDE(GET_PLAYER_PED(piTempPlayer), 70)
			ENDIF
		ENDIF
	ENDFOR
	
	SEETHROUGH_SET_FADE_STARTDISTANCE(TO_FLOAT(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iThermalStartDistance[iRule]))
	SEETHROUGH_SET_FADE_ENDDISTANCE(TO_FLOAT(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iThermalEndDistance[iRule]))
	
	SET_BIT(iLocalBoolCheck14, LBOOL14_THERMALVISION_INITIALISED)
	
	PRINTLN("[PLAYER VISUAL AIDS][GHOST RIDER VISION] INIT_PLAYER_VISUAL_AID_THERMAL - Using ghost rider vision override")
ENDPROC

PROC INIT_PLAYER_VISUAL_AID_THERMAL(INT &iTeam, INT &iRule)
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFifteen[iRule], ciBS_RULE15_OVERRIDE_THERMAL_VISION_WITH_HUNTER_EFFECT)
		INIT_PLAYER_VISUAL_AID_THERMAL_HUNTER_MODE_OVERRIDE(iTeam, iRule)
		EXIT
	ENDIF
	
	SEETHROUGH_RESET()
	
	SEETHROUGH_SET_HEATSCALE(TB_DEAD, 0.0)
	SEETHROUGH_SET_HEATSCALE(TB_COLD, 1.0)
	SEETHROUGH_SET_HEATSCALE(TB_WARM, 1.0) 
	SEETHROUGH_SET_HEATSCALE(TB_HOT, 1.0) 
	
	SEETHROUGH_SET_COLOR_NEAR(0,0,100)
	SEETHROUGH_SET_MAX_THICKNESS(12.5)
	
	SEETHROUGH_SET_NOISE_MIN(0.01)
	SEETHROUGH_SET_NOISE_MAX(0.2)
	SEETHROUGH_SET_HILIGHT_INTENSITY(0.35)
	SEETHROUGH_SET_HIGHLIGHT_NOISE(0.3)
	
	SEETHROUGH_SET_FADE_STARTDISTANCE(TO_FLOAT(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iThermalStartDistance[iRule]))
	SEETHROUGH_SET_FADE_ENDDISTANCE(TO_FLOAT(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iThermalEndDistance[iRule]))
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFIve, ciBEAST_SFX)
		LOAD_BEAST_SFX()
	ENDIF
	
	SET_BIT(iLocalBoolCheck14, LBOOL14_THERMALVISION_INITIALISED)
	
ENDPROC

FUNC BOOL PROCESS_PLAYER_VISUAL_AID_THERMAL_HUNTER_MODE_OVERRIDE(INT &iTeam, INT &iRule)
	
	//Exit if the option is disabled
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFifteen[iRule], ciBS_RULE15_OVERRIDE_THERMAL_VISION_WITH_HUNTER_EFFECT)
		RETURN FALSE
	ENDIF
	
	//Disable player action when Hunter Vision option is enabled - url:bugstar:7785096 - Halloween Mode - Judgement Day I - Rider players use their player action when trying to use thermal vision while not on their bike.
	DISABLE_INTERACTIONS_THIS_FRAME()
	
	IF NOT IS_BIT_SET(iLocalBoolCheck14, LBOOL14_RESET_HUNTER_VISION)
		SET_BIT(iLocalBoolCheck14, LBOOL14_RESET_HUNTER_VISION)
	ENDIF
	
	//Override seethrough max thickness
	IF IS_BIT_SET(iLocalBoolCheck12, LBOOL12_USE_HUNTER_VISION)
	AND SEETHROUGH_GET_MAX_THICKNESS() != 250.0
		SEETHROUGH_SET_MAX_THICKNESS(250.0)
		PRINTLN("[PLAYER VISUAL AIDS] PROCESS_PLAYER_VISUAL_AIDS - Override SEETHROUGH_SET_MAX_THICKNESS")
	ENDIF
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_HALLOWEEN_ADVERSARY_2022(g_FMMC_STRUCT.iAdversaryModeType)
		IF IS_BIT_SET(iLocalBoolCheck12, LBOOL12_USE_HUNTER_VISION)
		AND NOT IS_BIT_SET(iLocalBoolCheck14, LBOOL14_HUNTER_VISION_COOLDOWN)
			IF iHunterThermalVisionSoundLoop = -1
				iHunterThermalVisionSoundLoop = GET_SOUND_ID()
				PLAY_SOUND_FRONTEND(iHunterThermalVisionSoundLoop, "Thermal_Vision_Loop", "Halloween_Adversary_Sounds")
				PRINTLN("[PLAYER VISUAL AIDS] PROCESS_PLAYER_VISUAL_AIDS - PLAY_SOUND_FRONTEND - Thermal_Vision_Loop with Sound ID = ", iHunterThermalVisionSoundLoop)
			ENDIF
		ELIF iHunterThermalVisionSoundLoop != -1
			STOP_SOUND(iHunterThermalVisionSoundLoop)
			RELEASE_SOUND_ID(iHunterThermalVisionSoundLoop)
			iHunterThermalVisionSoundLoop = -1
		ENDIF
	ENDIF
	
	RETURN TRUE
	
ENDFUNC


PROC PROCESS_PLAYER_VISUAL_AID_THERMAL(INT iTeam, INT iRule)
	
	IF PROCESS_PLAYER_VISUAL_AID_THERMAL_HUNTER_MODE_OVERRIDE(iTeam, iRule)	
		EXIT
	ENDIF
	
	//Setup lowered heatmaps for friendly players
	INT iParticipant
	PRINTLN("[PLAYER_LOOP] - PROCESS_PLAYER_VISUAL_AID_THERMAL")
	FOR iParticipant = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION() -1
		IF iParticipant < GET_MAXIMUM_PLAYERS_ON_MISSION()
		PARTICIPANT_INDEX participantIndex = INT_TO_PARTICIPANTINDEX(iParticipant)
		
			IF NETWORK_IS_PARTICIPANT_ACTIVE(participantIndex)

				PLAYER_INDEX 	playerIndex = NETWORK_GET_PLAYER_INDEX(participantIndex)
				PED_INDEX		playerPedIndex = GET_PLAYER_PED( playerIndex )
				
				IF NOT IS_PED_INJURED( playerPedIndex )
				AND NOT IS_BIT_SET(iPlayerThermalRefreshBitset, iParticipant)
					PRINTLN("[PLAYER VISUAL AIDS] PROCESS_PLAYER_VISUAL_AID_THERMAL - Ped ", iParticipant, " is not injured!")	
					
					IF MC_playerBD[iParticipant].iGameState >= GAME_STATE_CAMERA_BLEND
						PRINTLN("[PLAYER VISUAL AIDS] PROCESS_PLAYER_VISUAL_AID_THERMAL - MyTeam = ", iTeam, ", TheirTeam = ", MC_playerBD[iParticipant].iteam)
						
						IF MC_playerBD[iParticipant].iteam = iTeam
						OR DOES_TEAM_LIKE_TEAM(MC_playerBD[iParticipant].iteam, iTeam)
							PRINTLN("[PLAYER VISUAL AIDS] PROCESS_PLAYER_VISUAL_AID_THERMAL - Ped ", iParticipant, " is on the same team as me! Reducing heatscale.")
							SET_PED_HEATSCALE_OVERRIDE( playerPedIndex, 100 )
							SET_BIT(iPlayerThermalRefreshBitset, iParticipant)
							iVisualAidCachedTeam[iParticipant] = MC_playerBD[iParticipant].iteam
							PRINTLN("[PLAYER VISUAL AIDS] PROCESS_PLAYER_VISUAL_AID_THERMAL - Ped ", iParticipant," Setting cached team to: ", iVisualAidCachedTeam[iParticipant])
						ELSE
							iVisualAidCachedTeam[iParticipant] = MC_playerBD[iParticipant].iteam
							SET_BIT(iPlayerThermalRefreshBitset, iParticipant)
							IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwelve[iRule], ciBS_RULE12_CLEAR_THERMAL_OVERRIDE_ON_SWAP)
								DISABLE_PED_HEATSCALE_OVERRIDE(playerPedIndex)
								PRINTLN("[PLAYER VISUAL AIDS] PROCESS_PLAYER_VISUAL_AID_THERMAL - Ped ", iParticipant, " Resetting Heat scale override.")
							ENDIF
							PRINTLN("[PLAYER VISUAL AIDS] PROCESS_PLAYER_VISUAL_AID_THERMAL - Ped ", iParticipant, " is not on the same team as me.")
							PRINTLN("[PLAYER VISUAL AIDS] PROCESS_PLAYER_VISUAL_AID_THERMAL - Ped ", iParticipant," Setting cached team to: ", iVisualAidCachedTeam[iParticipant])
						ENDIF
					ELSE
						PRINTLN("[PLAYER VISUAL AIDS] PROCESS_PLAYER_VISUAL_AID_THERMAL - Remote player is not in game state GAME_STATE_CAMERA_BLEND or higher. Retrying next frame.")
					ENDIF
				ELIF IS_PED_INJURED( playerPedIndex )
					PRINTLN("[PLAYER VISUAL AIDS] PROCESS_PLAYER_VISUAL_AID_THERMAL - Ped ", iParticipant, " is injured. Resetting thermal vision flag.")
					CLEAR_BIT(iPlayerThermalRefreshBitset, iParticipant)
				ELIF iVisualAidCachedTeam[iParticipant] != MC_playerBD[iParticipant].iteam
					PRINTLN("[PLAYER VISUAL AIDS] PROCESS_PLAYER_VISUAL_AID_THERMAL - Ped ", iParticipant, " has changed team. Resetting thermal vision flag.")
					CLEAR_BIT(iPlayerThermalRefreshBitset, iParticipant)
					IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwelve[iRule], ciBS_RULE12_CLEAR_THERMAL_OVERRIDE_ON_SWAP)
						DISABLE_PED_HEATSCALE_OVERRIDE(playerPedIndex)
						PRINTLN("[PLAYER VISUAL AIDS] PROCESS_PLAYER_VISUAL_AID_THERMAL - Ped ", iParticipant, " Resetting Heat scale override.")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR

	IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam)
		FLOAT fadeStartDist = TO_FLOAT(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iThermalStartDistance[iRule])
		FLOAT fadeEndDist = TO_FLOAT(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iThermalEndDistance[iRule])
		SEETHROUGH_SET_FADE_STARTDISTANCE(fadeStartDist)
		SEETHROUGH_SET_FADE_ENDDISTANCE(fadeEndDist)
		PRINTLN("[PLAYER VISUAL AIDS] PROCESS_PLAYER_VISUAL_AID_THERMAL - iTeam: ", iTeam, " iRule: ", iRule, " - Setting thermal fade distances - Start: ", fadeStartDist, ", End: ", fadeEndDist)
	ENDIF
	
ENDPROC

FUNC BOOL IS_PLAYER_VISUAL_AID_TOGGLE_ENABLED(INT iTeam, INT iRule, VISUAL_AID_MODES eVisionMode)
	
	SWITCH eVisionMode
	
		CASE VISUALAID_NIGHT
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitset4, ciBS4_TEAM_4_EnableNightVisionQuickEquip)
			AND IS_CURRENT_OUTFIT_MASK_NIGHTVISION()
			AND NOT IS_PLAYER_WEARING_NIGHT_VISION_HELMET_WITH_MOVABLE_VISOR()
				PRINTLN("[PLAYER VISUAL AIDS] IS_PLAYER_VISUAL_AID_TOGGLE_ENABLED | Returning TRUE due to ciBS4_TEAM_4_EnableNightVisionQuickEquip and not having a movable visor")
				RETURN TRUE
			ENDIF
			
			RETURN NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThree[iRule], ciBS_RULE3_LOCK_ON_NIGHT_VISION) AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFive[iRule], ciBS_RULE5_ENABLE_NIGHTVISION_TOGGLING) 
		
		CASE VISUALAID_THERMAL
			RETURN IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFive[iRule], ciBS_RULE5_ENABLE_THERMALVISION_TOGGLING)

	ENDSWITCH
	
	RETURN FALSE

ENDFUNC

//PURPOSE: Used to check if there are any special reasons why D-Pad Right shouldn't toggle night vision
FUNC BOOL IS_PLAYER_VISUAL_AID_TOGGLE_BLOCKED(INT iTeam, INT iRule)

	IF NOT bPlayerToUseOK
		RETURN TRUE
	ENDIF

	IF g_bMissionEnding
		RETURN TRUE
	ENDIF
	
	IF IS_PED_RAGDOLL(LocalPlayerPed)
		RETURN TRUE
	ENDIF
		
	IF NOT IS_PLAYER_VISUAL_AID_TOGGLE_ENABLED(iTeam, iRule, VISUALAID_NIGHT)
	AND NOT IS_PLAYER_VISUAL_AID_TOGGLE_ENABLED(iTeam, iRule, VISUALAID_THERMAL)
		//PRINTLN("[PLAYER VISUAL AIDS] IS_PLAYER_VISUAL_AID_TOGGLE_BLOCKED - All visual aid modes IS_PLAYER_VISUAL_AID_TOGGLE_ENABLED = FALSE")
		RETURN TRUE
	ENDIF

	IF NOT IS_CONTROL_ENABLED(PLAYER_CONTROL, INPUT_CONTEXT)
		//PRINTLN("[PLAYER VISUAL AIDS] IS_PLAYER_VISUAL_AID_TOGGLE_BLOCKED - IS_CONTROL_ENABLED(CONTROL_ACTION, INPUT_CONTEXT) = FALSE")
		RETURN TRUE
	ENDIF
	
	IF NOT IS_STRING_NULL_OR_EMPTY (sInteractWithVars.tlInteractWith_Object)
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sInteractWithVars.tlInteractWith_Object)
			//PRINTLN("[PLAYER VISUAL AIDS] IS_PLAYER_VISUAL_AID_TOGGLE_BLOCKED - Interact-With Help Message is Being Displayed")
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_AIM)
	AND GET_CURRENT_PLAYER_WEAPON_TYPE() != WEAPONTYPE_UNARMED
		//Disables when aiming with flashlight attachment on weapon // 3983001
		IF DOES_PLAYER_HAVE_CONTEXT_TOGGLABLE_WEAPON_COMPONENT_EQUIPPED()
			//PRINTLN("[PLAYER VISUAL AIDS] IS_PLAYER_VISUAL_AID_TOGGLE_BLOCKED - Context togglable weapon")
			RETURN TRUE
		ENDIF
		
		//When using the d-pad left version, block the toggle while aiming in general to prevent button conflicts with a "throw explosive" shortcut
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitset4, ciBS4_TEAM_4_EnableNightVisionQuickEquip)
			//PRINTLN("[PLAYER VISUAL AIDS] IS_PLAYER_VISUAL_AID_TOGGLE_BLOCKED - Currently aiming, blocking toggle to prevent button conflict on d-pad left")
			RETURN TRUE
		ENDIF
	ENDIF
	
	//Disables when prompted to press D-pad right by an interact-with object 
	IF sInteractWithVars.eInteractWith_CurrentState != IW_STATE_IDLE
		//PRINTLN("[PLAYER VISUAL AIDS] IS_PLAYER_VISUAL_AID_TOGGLE_BLOCKED - Interact object")
		RETURN TRUE
	ENDIF
	
	IF IS_ENTITY_IN_AIR(LocalPlayerPed)
		IF NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		OR NOT (GET_PED_PARACHUTE_STATE(LocalPlayerPed) = PPS_PARACHUTING)
			PRINTLN("[PLAYER VISUAL AIDS] IS_PLAYER_VISUAL_AID_TOGGLE_BLOCKED - Falling")
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_PC_VERSION() AND ((GET_PED_PARACHUTE_STATE(LocalPlayerPed) = PPS_PARACHUTING) OR (GET_PED_PARACHUTE_STATE(LocalPlayerPed) = PPS_DEPLOYING))
		//PRINTLN("[PLAYER VISUAL AIDS] IS_PLAYER_VISUAL_AID_TOGGLE_BLOCKED - PC ONLY - Parachuting")
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(MPGlobalsAmbience.iAmbBitSet, iABI_BLOCK_TOGGLING_NIGHTVISION_FILTER)
		PRINTLN("[PLAYER VISUAL AIDS] IS_PLAYER_VISUAL_AID_TOGGLE_BLOCKED - iABI_BLOCK_TOGGLING_NIGHTVISION_FILTER")
		RETURN TRUE
	ENDIF
	
	IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_DisableNVToggleInVeh)
			RETURN TRUE
		ENDIF
		
		VEHICLE_INDEX viPlayerVehicle = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
		MODEL_NAMES mnPlayerVehicleModel = GET_ENTITY_MODEL(viPlayerVehicle)
		
		IF mnPlayerVehicleModel = VALKYRIE
		OR mnPlayerVehicleModel = AVENGER
		OR mnPlayerVehicleModel = AKULA
		
			RETURN TRUE
			
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck32, LBOOL32_MINIGAME_HELP_TEXT_SHOWING_THIS_FRAME)
		RETURN TRUE
	ENDIF
	
	//Block Visual Aids if another interaction promp is active
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("VENDHLP")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("VENDCSH")
		RETURN TRUE
	ENDIF
	
	//Ghost Rider Vision Cooldown
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFifteen[iRule], ciBS_RULE15_OVERRIDE_THERMAL_VISION_WITH_HUNTER_EFFECT)
	AND IS_BIT_SET(iLocalBoolCheck14, LBOOL14_HUNTER_VISION_COOLDOWN)
		PRINTLN("[PLAYER VISUAL AIDS][GHOST RIDER VISION] IS_PLAYER_VISUAL_AID_TOGGLE_BLOCKED - Disabled due to the cooldown timer!")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_PLAYER_VISUAL_AIDS_BE_DISABLED_DURING_CUTSCENES()
	IF IS_ANY_EMP_CURRENTLY_ACTIVE()
	AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFive, ciOptionsBS25_AutomaticallyToggleNightvisionWithEMP)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL PROCESS_PLAYER_VISUAL_AID_FORCED(INT iTeam, INT iRule)

	BOOL bAltSFX = IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSeven[iRule], ciBS_RULE7_THERMAL_NV_ALTERNATIVE_SFX)
	VISUAL_AID_SOUND  eAltSFX = VISUALAID_SOUND_ON
	IF bAltSFX
		eAltSFX = VISUALAID_SOUND_ALT
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFifteen[iRule], ciBS_RULE15_OVERRIDE_THERMAL_VISION_WITH_HUNTER_EFFECT)
		eAltSFX = VISUALAID_SOUND_OFF
	ENDIF
	
	//----------------------------------------------------------------------------------------------------------------------------------------------------
	// GENERAL STUFF
	//----------------------------------------------------------------------------------------------------------------------------------------------------
	IF IS_BIT_SET(MPGlobalsAmbience.iAmbBitSet, iABI_DEACTIVATE_NIGHTVISION)
		
		IF SET_PLAYER_VISUAL_AID(VISUALAID_OFF, TRUE, eAltSFX)
			PRINTLN("[PLAYER VISUAL AIDS] - PROCESS_PLAYER_VISUAL_AID_FORCED - iTeam: ", iTeam, " iRule: ", iRule, " - FORCING: VISUALAID_OFF - iABI_DEACTIVATE_NIGHTVISION")
		ENDIF
		RETURN TRUE
	ENDIF

	IF IS_TRANSITION_ACTIVE() OR GET_MC_SERVER_GAME_STATE() = GAME_STATE_END
		IF SET_PLAYER_VISUAL_AID(VISUALAID_OFF, TRUE, eAltSFX)
			PRINTLN("[PLAYER VISUAL AIDS] - PROCESS_PLAYER_VISUAL_AID_FORCED - iTeam: ", iTeam, " iRule: ", iRule, " - FORCING: VISUALAID_OFF - Transitioning")
		ENDIF
		RETURN TRUE
	ENDIF

	IF IS_CELLPHONE_CAMERA_IN_USE()
		IF SET_PLAYER_VISUAL_AID(VISUALAID_OFF, TRUE, eAltSFX)
			PRINTLN("[PLAYER VISUAL AIDS] - PROCESS_VISUAL_AID_OVERRIDE - iTeam: ", iTeam, " iRule: ", iRule, " - FORCING: VISUALAID_OFF - IS_CELLPHONE_CAMERA_IN_USE")
		ENDIF
		RETURN TRUE
	ENDIF
	
	//----------------------------------------------------------------------------------------------------------------------------------------------------
	// CUTSCENE STUFF
	//----------------------------------------------------------------------------------------------------------------------------------------------------
	IF SHOULD_PLAYER_VISUAL_AIDS_BE_DISABLED_DURING_CUTSCENES()
		IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_PLAYING_CUTSCENE)
		
			IF NOT IS_BIT_SET(iLocalBoolCheck27, LBOOL27_STORED_NIGHTVISION_STATE)			
				SET_BIT(iLocalBoolCheck27, LBOOL27_STORED_NIGHTVISION_STATE)
			ENDIF
			
			// Cutscene specific
			IF NOT IS_STRING_NULL_OR_EMPTY(sMocapCutscene) 
			AND ARE_STRINGS_EQUAL(sMocapCutscene, "MPH_HUM_FIN_MCS1") //Humane Labs interior mocap

				IF g_sFMMCVisualAid.eOverride != VISUALAID_NIGHT
					PRINTLN("[PLAYER VISUAL AIDS] - PROCESS_PLAYER_VISUAL_AID_FORCED - iTeam: ", iTeam, " iRule: ", iRule, " - FORCING: VISUALAID_NIGHT - Cutscene start - forced ON NV")
					SET_PLAYER_VISUAL_AID(VISUALAID_NIGHT, TRUE, eAltSFX)
				ENDIF
				RETURN TRUE
				
			// All other cutscenes
			ELSE

				IF SET_PLAYER_VISUAL_AID(VISUALAID_OFF, TRUE, eAltSFX)
					PRINTLN("[PLAYER VISUAL AIDS] - PROCESS_PLAYER_VISUAL_AID_FORCED - iTeam: ", iTeam, " iRule: ", iRule, " - FORCING: VISUALAID_OFF - Cutscene start - forced OFF NV")
				ENDIF
				RETURN TRUE
				
			ENDIF
		
		// Cutscene not playing/finished
		ELSE
			IF IS_BIT_SET(iLocalBoolCheck27, LBOOL27_STORED_NIGHTVISION_STATE)
				IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_USING_NIGHTVISION_WHEN_ENTERING_CUTSCENE)

					// Had NV on before cutscene
					IF SET_PLAYER_VISUAL_AID(VISUALAID_NIGHT, FALSE, eAltSFX)
						PRINTLN("[PLAYER VISUAL AIDS] - PROCESS_PLAYER_VISUAL_AID_FORCED - iTeam: ", iTeam, " iRule: ", iRule, " - FORCING: VISUALAID_NIGHT - Cutscene end - restoring NV")
					ENDIF
					
					CLEAR_BIT(iLocalBoolCheck6, LBOOL6_USING_NIGHTVISION_WHEN_ENTERING_CUTSCENE)
					RETURN TRUE
				ENDIF
				
				CLEAR_BIT(iLocalBoolCheck27, LBOOL27_STORED_NIGHTVISION_STATE)
			ENDIF

		ENDIF
	ENDIF
	
	//----------------------------------------------------------------------------------------------------------------------------------------------------
	// RULE STUFF
	//----------------------------------------------------------------------------------------------------------------------------------------------------
	
	IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		VEHICLE_INDEX viPlayerVehicle = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
		MODEL_NAMES mnPlayerVehicleModel = GET_ENTITY_MODEL(viPlayerVehicle)
		
		IF mnPlayerVehicleModel = VALKYRIE
		OR mnPlayerVehicleModel = AVENGER
		OR mnPlayerVehicleModel = AKULA
		
			IF g_sFMMCVisualAid.eOverride != VISUALAID_OFF
			AND g_sFMMCVisualAid.eOverride != VISUALAID_DEFAULT
				SET_PLAYER_VISUAL_AID(VISUALAID_OFF, FALSE, eAltSFX)
				PRINTLN("[PLAYER VISUAL AIDS] - PROCESS_PLAYER_VISUAL_AID_FORCED - iTeam: ", iTeam, " iRule: ", iRule, " - FORCING: VISUALAID_OFF - In gun cam vehicle")	
			ENDIF
			RETURN TRUE // return outside to block further checks
			
		ENDIF
	ENDIF

	// Rule force NV on
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitset[iRule], ciBS_RULE_NIGHT_VISION_ON)
	
		IF NOT HAS_NET_TIMER_STARTED(tdNightVisionDelayTimer)
		AND g_sFMMCVisualAid.eOverride != VISUALAID_NIGHT // Don't bother with all this delay stuff if nightvision is already on
		
			// MIDPOINT
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitset[iRule], ciBS_RULE_NIGHT_VISION_MIDPOINT)
				IF IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iTeam], iRule)
				AND IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_MIDPOINT_THIS_FRAME + iTeam)
					PRINTLN("[PLAYER VISUAL AIDS] - PROCESS_PLAYER_VISUAL_AID_FORCED - iTeam: ", iTeam, " iRule: ", iRule, " - Rule Midpoint timer start")
					REINIT_NET_TIMER(tdNightVisionDelayTimer) // start timer
				ENDIF
			// START
			ELSE
				IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam)
					PRINTLN("[PLAYER VISUAL AIDS] - PROCESS_PLAYER_VISUAL_AID_FORCED - iTeam: ", iTeam, " iRule: ", iRule, " - Rule Start timer start")
					REINIT_NET_TIMER(tdNightVisionDelayTimer) // start timer
				ENDIF
			ENDIF
		ELSE
			IF HAS_NET_TIMER_EXPIRED(tdNightVisionDelayTimer, 700)
			OR g_sFMMCVisualAid.eOverride = VISUALAID_NIGHT // Don't bother with all this delay stuff if nightvision is already on
			
				BOOL bOverride = IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThree[iRule], ciBS_RULE3_LOCK_ON_NIGHT_VISION)
				IF NOT bOverride
					RESET_NET_TIMER(tdNightVisionDelayTimer)
				ENDIF
				
				IF SET_PLAYER_VISUAL_AID(VISUALAID_NIGHT, bOverride, eAltSFX)
					PRINTLN("[PLAYER VISUAL AIDS] - PROCESS_PLAYER_VISUAL_AID_FORCED - iTeam: ", iTeam, " iRule: ", iRule, " - FORCING: VISUALAID_NIGHT - Rule forcing NV on - LOCKED = ", BOOL_TO_STRING(bOverride))
				ENDIF

				RETURN TRUE
				
			ENDIF
		
		ENDIF
	
	ELSE
		RESET_NET_TIMER(tdNightVisionDelayTimer)
	ENDIF
	
	// Rule force TV on
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFive[iRule], ciBS_RULE5_FORCE_THERMAL_VISION)
	OR ENABLE_HUNTER_VISION(iRule, iTeam)
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFifteen[iRule], ciBS_RULE15_OVERRIDE_THERMAL_VISION_WITH_HUNTER_EFFECT)
			eAltSFX = VISUALAID_SOUND_OFF
		ENDIF
	
		IF SET_PLAYER_VISUAL_AID(VISUALAID_THERMAL, TRUE, eAltSFX)
			PRINTLN("[PLAYER VISUAL AIDS] - PROCESS_PLAYER_VISUAL_AID_FORCED - iTeam: ", iTeam, " iRule: ", iRule, " - FORCING: VISUALAID_THERMAL - Rule forcing TV on")
		ENDIF
		RETURN TRUE
	ELSE
		// only force off if it was previously forced on
		IF g_sFMMCVisualAid.eOverride = VISUALAID_THERMAL
		AND g_sFMMCVisualAid.bMaintainOverride
			IF SET_PLAYER_VISUAL_AID(VISUALAID_OFF, FALSE, eAltSFX)
				PRINTLN("[PLAYER VISUAL AIDS] - PROCESS_PLAYER_VISUAL_AID_FORCED - iTeam: ", iTeam, " iRule: ", iRule, " - FORCING: VISUALAID_THERMAL - Rule forcing TV off")	
				RETURN TRUE
			ENDIF
		ENDIF
		
	ENDIF
	
	
	IF IS_ANY_EMP_CURRENTLY_ACTIVE()
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFive, ciOptionsBS25_AutomaticallyToggleNightvisionWithEMP)
			IF NOT IS_LOCAL_PLAYER_USING_DRONE()
				IF SET_PLAYER_VISUAL_AID(VISUALAID_NIGHT, TRUE, eAltSFX)
					PRINTLN("[PLAYER VISUAL AIDS] - PROCESS_PLAYER_VISUAL_AID_FORCED - SET_PLAYER_VISUAL_AID returned TRUE when called due to IS_ANY_EMP_CURRENTLY_ACTIVE()")	
				ENDIF
				
				PRINTLN("[PLAYER VISUAL AIDS] - PROCESS_PLAYER_VISUAL_AID_FORCED - iTeam: ", iTeam, " iRule: ", iRule, " - FORCING: VISUALAID_NIGHT - IS_ANY_EMP_CURRENTLY_ACTIVE()")	
				RETURN TRUE
			ELSE
				PRINTLN("[PLAYER VISUAL AIDS] - PROCESS_PLAYER_VISUAL_AID_FORCED - Not turning on IS_ANY_EMP_CURRENTLY_ACTIVE() nightvision because the player is currently using a drone")
			ENDIF
		ELSE
			PRINTLN("[PLAYER VISUAL AIDS] - PROCESS_PLAYER_VISUAL_AID_FORCED - ciOptionsBS25_AutomaticallyToggleNightvisionWithEMP is not set")
		ENDIF
	ENDIF

	PRINTLN("[PLAYER VISUAL AIDS] - PROCESS_PLAYER_VISUAL_AID_FORCED - Rturning false")
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_HUNTER_VISION_BUTTON_PRESSED()
	IF NOT IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
		IF (IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SPECIAL_ABILITY_SECONDARY) AND IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SPECIAL_ABILITY))
			RETURN TRUE
		ELIF (IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SPECIAL_ABILITY) AND IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SPECIAL_ABILITY_SECONDARY))
			RETURN TRUE
		ENDIF
	ELSE
		IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SPECIAL_ABILITY_PC)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL PROCESS_PLAYER_VISUAL_AID_TOGGLE(INT iTeam, INT iRule)
	
	VISUAL_AID_MODES eCurrentMode = GET_PLAYER_VISUAL_AID_OVERRIDE()
	
	BOOL bAltSFX = IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSeven[iRule], ciBS_RULE7_THERMAL_NV_ALTERNATIVE_SFX)
	VISUAL_AID_SOUND eAltSFX = VISUALAID_SOUND_ON
	IF bAltSFX
		eAltSFX = VISUALAID_SOUND_ALT
	ENDIF
	
	BOOL bToggleButtonPressed 
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFifteen[iRule], ciBS_RULE15_OVERRIDE_THERMAL_VISION_WITH_HUNTER_EFFECT)
		bToggleButtonPressed = IS_HUNTER_VISION_BUTTON_PRESSED()
	ELSE
		bToggleButtonPressed = IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_CONTEXT)
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitset4, ciBS4_TEAM_4_EnableNightVisionQuickEquip)
			bToggleButtonPressed = IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_DETONATE)
		ENDIF
	ENDIF
	
	// Deactivates a toggled on visual aid if the rule no longer has it enabled
	IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam)
	AND eCurrentMode != VISUALAID_DEFAULT
	AND eCurrentMode != VISUALAID_OFF
	AND NOT IS_PLAYER_VISUAL_AID_TOGGLE_ENABLED(iTeam, iRule, eCurrentMode)
		IF NOT (MC_serverBD_4.iPlayerRuleMissionLogic[MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_SCRIPTED_CUTSCENE)
			IF SET_PLAYER_VISUAL_AID(VISUALAID_OFF, FALSE, eAltSFX)
				PRINTLN("[PLAYER VISUAL AIDS] - PROCESS_PLAYER_VISUAL_AID_TOGGLE - iTeam: ", iTeam, " iRule: ", iRule, " - TOGGLE DISABLED FOR: ", GET_VISUAL_AID_MODE_NAME(eCurrentMode))	
				RETURN TRUE
			ENDIF
		ELSE
			// Store previous state
			IF GET_USINGNIGHTVISION()
				PRINTLN("[PLAYER VISUAL AIDS] - PROCESS_PLAYER_VISUAL_AID_FORCED - iTeam: ", iTeam, " iRule: ", iRule, " - Set Bit - LBOOL6_USING_NIGHTVISION_WHEN_ENTERING_CUTSCENE")
				SET_BIT(iLocalBoolCheck6, LBOOL6_USING_NIGHTVISION_WHEN_ENTERING_CUTSCENE)
			ELSE
				PRINTLN("[PLAYER VISUAL AIDS] - PROCESS_PLAYER_VISUAL_AID_FORCED - iTeam: ", iTeam, " iRule: ", iRule, " - Clear Bit - LBOOL6_USING_NIGHTVISION_WHEN_ENTERING_CUTSCENE")
				CLEAR_BIT(iLocalBoolCheck6, LBOOL6_USING_NIGHTVISION_WHEN_ENTERING_CUTSCENE)
			ENDIF
		ENDIF
	
	// Process regular toggle behaviour
	ELIF NOT IS_PLAYER_VISUAL_AID_TOGGLE_BLOCKED(iTeam, iRule)
	AND bToggleButtonPressed
		BOOL bNVTogglable = IS_PLAYER_VISUAL_AID_TOGGLE_ENABLED(iTeam, iRule, VISUALAID_NIGHT)
		BOOL bTVTogglable = IS_PLAYER_VISUAL_AID_TOGGLE_ENABLED(iTeam, iRule, VISUALAID_THERMAL)
		
		VISUAL_AID_MODES eNewState = VISUALAID_OFF
		
		SWITCH GET_PLAYER_VISUAL_AID_MODE()
		
			CASE VISUALAID_OFF
				
				IF bNVTogglable
					eNewState = VISUALAID_NIGHT
				ELIF bTVTogglable
					 eNewState = VISUALAID_THERMAL
				ENDIF
				
			BREAK
			
			CASE VISUALAID_NIGHT
				
				IF bTVTogglable
					eNewState = VISUALAID_THERMAL
				ELSE
					eNewState = VISUALAID_OFF
				ENDIF
				
			BREAK
			
			CASE VISUALAID_THERMAL
				eNewState = VISUALAID_OFF
			BREAK
			
		ENDSWITCH
		
		//Ghost Rider Vision
		IF bToggleButtonPressed
		AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFifteen[iRule], ciBS_RULE15_OVERRIDE_THERMAL_VISION_WITH_HUNTER_EFFECT)
			eAltSFX = VISUALAID_SOUND_OFF
			IF eNewState = VISUALAID_THERMAL
				SET_BIT(iLocalBoolCheck12, LBOOL12_USE_HUNTER_VISION)
				PRINTLN("[PLAYER VISUAL AIDS][GHOST RIDER VISION] Toggle On")	
			ELSE
				CLEAR_BIT(iLocalBoolCheck12, LBOOL12_USE_HUNTER_VISION)
				PRINTLN("[PLAYER VISUAL AIDS][GHOST RIDER VISION] Toggle Off")
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(iLocalBoolCheck12, LBOOL12_USE_HUNTER_VISION)
			RETURN TRUE
		ENDIF
		
		IF SET_PLAYER_VISUAL_AID(eNewState, FALSE, eAltSFX)
			PRINTLN("[PLAYER VISUAL AIDS] - PROCESS_PLAYER_VISUAL_AID_TOGGLE - iTeam: ", iTeam, " iRule: ", iRule, " - PLAYER TOGGLED: ", GET_VISUAL_AID_MODE_NAME(eNewState))			
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_VISUAL_AIDS_FORCE_CLEAN_UP(INT iTeam, INT iRule)
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwelve[iRule], ciBS_RULE12_CLEAR_VISUAL_AIDS_ON_DEATH)
		IF !bPedToUseOk
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_DisableNVToggleInVeh)
	AND IS_PED_IN_ANY_VEHICLE(PlayerPedToUse)
		IF GET_PLAYER_VISUAL_AID_MODE() != VISUALAID_OFF
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck32, LBOOL32_TURN_OFF_VISUAL_AIDS_ASAP)
		CLEAR_BIT(iLocalBoolCheck32, LBOOL32_TURN_OFF_VISUAL_AIDS_ASAP)
		PRINTLN("[PLAYER VISUAL AIDS] SHOULD_VISUAL_AIDS_FORCE_CLEAN_UP - Returning TRUE due to LBOOL32_TURN_OFF_VISUAL_AIDS_ASAP")
		RETURN TRUE
	ENDIF
	
	IF GET_PLAYER_VISUAL_AID_MODE() != VISUALAID_OFF
		IF IS_ANY_EMP_CURRENTLY_ACTIVE()
		AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFive, ciOptionsBS25_AutomaticallyToggleNightvisionWithEMP)
			IF IS_LOCAL_PLAYER_USING_DRONE()
			AND NOT IS_GAMEPLAY_CAM_RENDERING()
				PRINTLN("[PLAYER VISUAL AIDS] SHOULD_VISUAL_AIDS_FORCE_CLEAN_UP - Turning off nightvision because we're using the drone")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_HUNTER_VISION_TIMERS_AND_CLEANUP(INT iTeam, INT iRule)
	
	//Process Timers
	INT iGRVDuration = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iHunterVisionDuration[iRule]
	INT iGRVCooldown = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iHunterVisionCooldown[iRule]
	
	IF iGRVDuration = 0
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(iLocalBoolCheck14, LBOOL14_HUNTER_VISION_COOLDOWN)
		IF HAS_NET_TIMER_STARTED(tdHunterVisionTimer)
			INT iTemp = iGRVDuration - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdHunterVisionTimer)
			DRAW_GENERIC_METER(iTemp, iGRVDuration, "GR_VS", DEFAULT, DEFAULT, HUDORDER_TOP, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT)
		ENDIF
		
		IF IS_BIT_SET(iLocalBoolCheck12, LBOOL12_USE_HUNTER_VISION)
		AND NOT HAS_NET_TIMER_STARTED(tdHunterVisionTimer)
			//We are using the hunter vision now - Start the timer that decides how long we can use the hunter vision for
			START_NET_TIMER(tdHunterVisionTimer)
			iCachedHunterVisionUnusedTime = 0
			PRINTLN("[PLAYER VISUAL AIDS][GHOST RIDER VISION] PROCESS_HUNTER_VISION_TIMERS_AND_CLEANUP - Start tdHunterVisionTimer timer")
			
			IF NOT IS_AUDIO_SCENE_ACTIVE("Halloween_Adversary_Thermal_Vison_Mixscene")
				START_AUDIO_SCENE("Halloween_Adversary_Thermal_Vison_Mixscene")
				PRINTLN("[PLAYER VISUAL AIDS][GHOST RIDER VISION] PROCESS_HUNTER_VISION_TIMERS_AND_CLEANUP - START_AUDIO_SCENE(\"Halloween_Adversary_Thermal_Vison_Mixscene\")")
			ENDIF
			EXIT
		ENDIF
		
		//If the timer has expired, disable the hunter vision	
		IF HAS_NET_TIMER_STARTED(tdHunterVisionTimer)
		AND ( GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdHunterVisionTimer) > iGRVDuration OR NOT IS_BIT_SET(iLocalBoolCheck12, LBOOL12_USE_HUNTER_VISION) OR !bLocalPlayerOK OR GET_MC_SERVER_GAME_STATE() = GAME_STATE_END)
			SET_BIT(iLocalBoolCheck32, LBOOL32_TURN_OFF_VISUAL_AIDS_ASAP)
			SET_BIT(iLocalBoolCheck14, LBOOL14_HUNTER_VISION_COOLDOWN)
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFifteen[iRule], ciBS_RULE15_HUNTER_VISION_CACHE_REMAINING_DURATION_FOR_COOLDOWN)
			AND iGRVDuration <= iGRVCooldown
				PRINTLN("[PLAYER VISUAL AIDS][GHOST RIDER VISION] PROCESS_HUNTER_VISION_TIMERS_AND_CLEANUP - iUnusedTime = ", iCachedHunterVisionUnusedTime)
				iCachedHunterVisionUnusedTime = iGRVDuration - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdHunterVisionTimer)
				PRINTLN("[PLAYER VISUAL AIDS][GHOST RIDER VISION] PROCESS_HUNTER_VISION_TIMERS_AND_CLEANUP - iUnusedTime = ", iCachedHunterVisionUnusedTime)
			ENDIF
			
			IF IS_AUDIO_SCENE_ACTIVE("Halloween_Adversary_Thermal_Vison_Mixscene")
				STOP_AUDIO_SCENE("Halloween_Adversary_Thermal_Vison_Mixscene")
				PRINTLN("[PLAYER VISUAL AIDS][GHOST RIDER VISION] PROCESS_HUNTER_VISION_TIMERS_AND_CLEANUP - STOP_AUDIO_SCENE(\"Halloween_Adversary_Thermal_Vison_Mixscene\")")
			ENDIF
			
			RESET_NET_TIMER(tdHunterVisionTimer)
			PRINTLN("[PLAYER VISUAL AIDS][GHOST RIDER VISION] PROCESS_HUNTER_VISION_TIMERS_AND_CLEANUP - Disable hunter vision")
			EXIT
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck14, LBOOL14_HUNTER_VISION_COOLDOWN)
		IF NOT HAS_NET_TIMER_STARTED(tdHunterVisionTimer)
			START_NET_TIMER(tdHunterVisionTimer)
		ELIF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdHunterVisionTimer) > iGRVCooldown - iCachedHunterVisionUnusedTime
		OR NOT bLocalPlayerOK
			//Cooldown timer has expired - start the cleanup
			PRINTLN("[PLAYER VISUAL AIDS][GHOST RIDER VISION] PROCESS_HUNTER_VISION_TIMERS_AND_CLEANUP - Reset tdHunterVisionCooldownTimer timer and cleanup")
			RESET_NET_TIMER(tdHunterVisionTimer)
			CLEAR_BIT(iLocalBoolCheck12, LBOOL12_USE_HUNTER_VISION)
			CLEAR_BIT(iLocalBoolCheck14, LBOOL14_THERMALVISION_INITIALISED)
			CLEAR_BIT(iLocalBoolCheck14, LBOOL14_HUNTER_VISION_COOLDOWN)
			CLEAR_BIT(iLocalBoolCheck14, LBOOL14_RESET_HUNTER_VISION)
		ENDIF
		
		IF HAS_NET_TIMER_STARTED(tdHunterVisionTimer)
			INT iTemp = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdHunterVisionTimer)
			DRAW_GENERIC_METER(iTemp + iCachedHunterVisionUnusedTime, iGRVCooldown, "GR_VS_CD", HUD_COLOUR_ORANGE, DEFAULT, HUDORDER_TOP, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT)
		ENDIF
	ENDIF
		
ENDPROC
			
PROC PROCESS_PLAYER_VISUAL_AIDS()

	INT iTeam = MC_PlayerBD[iPartToUse].iteam
	IF iTeam < 0 OR iTeam >= FMMC_MAX_TEAMS
		EXIT
	ENDIF

	INT iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]
	IF iRule < 0 OR iRule >= FMMC_MAX_RULES	
		EXIT		
	ENDIF
	
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitset4, ciBS4_TEAM_4_EnableNightVisionQuickEquip)
	AND IS_CURRENT_OUTFIT_MASK_NIGHTVISION()
		IF IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_DETONATE)
		AND CAN_LOCAL_PLAYER_DO_QUICK_EQUIP_MASK()
		AND NOT GET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_IsSwitchingHelmetVisor)
			IF IS_PLAYER_WEARING_NIGHT_VISION_HELMET_WITH_MOVABLE_VISOR()
				PRINTLN("[VisualAids] PROCESS_PLAYER_VISUAL_AIDS | Setting PCF_ForceHelmetVisorSwitch now")
				SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_ForceHelmetVisorSwitch, TRUE)
			ELSE
				// Handled in PROCESS_PLAYER_VISUAL_AID_TOGGLE
				
			ENDIF
		ENDIF
	ENDIF
	
	IF SHOULD_VISUAL_AIDS_FORCE_CLEAN_UP(iTeam, iRule)
		ENABLE_NIGHTVISION(VISUALAID_OFF)
		CLEANUP_PLAYER_VISUAL_AIDS()
	ENDIF

	IF NOT PROCESS_PLAYER_VISUAL_AID_FORCED(iTeam, iRule)
	AND NOT PROCESS_PLAYER_VISUAL_AID_TOGGLE(iTeam, iRule)
		
		BOOL bAltSFX = IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSeven[iRule], ciBS_RULE7_THERMAL_NV_ALTERNATIVE_SFX)
		VISUAL_AID_SOUND eAltSFX = VISUALAID_SOUND_ON
		IF bAltSFX
			eAltSFX = VISUALAID_SOUND_ALT
		ENDIF
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFifteen[iRule], ciBS_RULE15_OVERRIDE_THERMAL_VISION_WITH_HUNTER_EFFECT)
			eAltSFX = VISUALAID_SOUND_OFF
		ENDIF
		
		IF SET_PLAYER_VISUAL_AID(VISUALAID_DEFAULT, DEFAULT, eAltSFX)
			PRINTLN("[PLAYER VISUAL AIDS] - PROCESS_PLAYER_VISUAL_AIDS - iTeam: ", iTeam, " iRule: ", iRule, " - No force or toggle, CLEARING to Default")
		ENDIF
	ENDIF
	
	//Init the thermal vision settings on load if any option is on for this rule
	IF IS_PLAYER_VISUAL_AID_TOGGLE_ENABLED(iTeam, iRule, VISUALAID_THERMAL)
	OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFive[iRule], ciBS_RULE5_FORCE_THERMAL_VISION)
	
		IF NOT IS_BIT_SET(iLocalBoolCheck14, LBOOL14_THERMALVISION_INITIALISED)
			INIT_PLAYER_VISUAL_AID_THERMAL(iTeam, iRule)
		ENDIF
		
		PROCESS_PLAYER_VISUAL_AID_THERMAL(iTeam, iRule)
		
		//Ghost Rider Vision Specific
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFifteen[iRule], ciBS_RULE15_OVERRIDE_THERMAL_VISION_WITH_HUNTER_EFFECT)
		AND NOT IS_BIT_SET(iLocalBoolCheck14, LBOOL14_HUNTER_VISION_COOLDOWN)
			PROCESS_PLAYER_VISUAL_AID_TOGGLE(iTeam, iRule)
		ENDIF
	ENDIF
	
	IF IS_PLAYER_VISUAL_AID_TOGGLE_ENABLED(iTeam, iRule, VISUALAID_NIGHT)
		IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam)
			FLOAT fNightVisionRange = TO_FLOAT(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iOverrideNightvisionRange[iRule])
			OVERRIDE_NIGHTVISION_LIGHT_RANGE(fNightVisionRange)
			PRINTLN("[PLAYER VISUAL AIDS] PROCESS_PLAYER_VISUAL_AIDS - iTeam: ", iTeam, " iRule: ", iRule, " - Setting night vision range - Range: ", fNightVisionRange)
		ENDIF
	ENDIF
	
	//Ghost Rider Vision Timers & Cleanup
	IF IS_BIT_SET(iLocalBoolCheck14, LBOOL14_RESET_HUNTER_VISION)
		PROCESS_HUNTER_VISION_TIMERS_AND_CLEANUP(iTeam, iRule)
	ENDIF 
	
	IF IS_FAKE_MULTIPLAYER_MODE_SET()
		MANAGE_VISUALAID_CONTENT_STRUCT s
		MANAGE_VISUALAID_CONTENT (s)
	ENDIF

ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Trip Skip  ------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: This section processes a Trip Skip, which is a sequence the players can trigger if the leader has completed the heist/mission before.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_TRIP_SKIP_EVERY_FRAME()
	IF IS_FAKE_MULTIPLAYER_MODE_SET()
	OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciALLOW_NON_HEIST_REBREATHERS)
	OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_EnableTripSkipForMission)
		VECTOR vTripSkipDestination
		
		IF MC_playerBD[iLocalPart].TSPlayerData.iStage > TSS_VOTE
		AND MC_playerBD[iLocalPart].TSPlayerData.iStage <= TSS_INTERP_CAM
			PRINTLN("TRIP SKIP - Disabling Player controls to avoid leaving the vehicle. (TSPlayerData) We are between TSS_VOTE/TSS_INTERP_CAM")
			PROCESS_DISABLING_CONTROLS_WHILE_ACTIVE_TRIP_SKIP()
		ENDIF
		
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciALLOW_NON_HEIST_REBREATHERS)
		OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_EnableTripSkipForMission)
			//Don't Do Trip Skip after Checkpoint or on Restart
			
			PRINTLN("TRIP SKIP - IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciTRIP_SKIP_ALL): ", IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciTRIP_SKIP_ALL))
			//Whole Crew Trip Skip
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciTRIP_SKIP_ALL)						
				IF NOT IS_BIT_SET(MC_serverBD_3.TSServerData[0].iBitSet, TSS_BI_BLOCKED)
					IF (IS_THIS_A_QUICK_RESTART_JOB() OR IS_CORONA_INITIALISING_A_QUICK_RESTART())
					AND (FMMC_CHECKPOINTS_IS_ANY_RETRY_START_POINT_SET())
						SET_BIT(MC_serverBD_3.TSServerData[0].iBitSet, TSS_BI_BLOCKED)
						PRINTLN("TRIP SKIP - BLOCKED - TSS_BI_BLOCKED")
					ENDIF
				ENDIF
				
				IF MC_serverBD_4.iCurrentHighestPriority[0] < FMMC_MAX_RULES
				AND NOT IS_BIT_SET(MC_serverBD_3.TSServerData[0].iBitSet, TSS_BI_BLOCKED)
					vTripSkipDestination = MPGlobalsAmbience.vQuickGPS
					PRINTLN("(1) TRIP SKIP [RCC MISSION] - vTripSkipDestination: ", vTripSkipDestination)
					PRINTLN("TRIP SKIP - TYPE - WHOLE CREW TRIP SKIP")
					IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
						MAINTAIN_TRIP_SKIP_CLIENT(MC_serverBD_3.TSServerData[0], MC_playerBD[iLocalPart].TSPlayerData, TSLocalData, vTripSkipDestination, IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[0].iRuleBitsetTwo[MC_serverBD_4.iCurrentHighestPriority[0]], ciBS_RULE2_USE_TRIP_SKIP), (GET_CURRENT_HEIST_COMPLETION_STAT_FOR_LEADER() > 0), bHasTripSkipJustFinished, TRUE)
					ENDIF
					IF bIsLocalPlayerHost
						MAINTAIN_TRIP_SKIP_SERVER(MC_serverBD_3.TSServerData[0], (MC_playerBD[iLocalPart].bPlayedFirstDialogue OR IS_THIS_A_QUICK_RESTART_JOB()), 0)
					ENDIF
				ENDIF
					
			//Separate Team Trip Skip
			ELSE
				PRINTLN("____________ (2) TRIP SKIP - CLIENT - (SEPARATE TEAM) for Team: ", MC_playerBD[iPartToUse].iteam, "_______________")
				BOOL bTeamOnGDRule
				bTeamOnGDRule = IS_TEAM_ON_GET_AND_DELIVER_RULE(MC_playerBD[iPartToUse].iteam)
				IF NOT IS_BIT_SET(MC_serverBD_3.TSServerData[MC_playerBD[iPartToUse].iteam].iBitSet, TSS_BI_BLOCKED)
					IF (IS_THIS_A_QUICK_RESTART_JOB() OR IS_CORONA_INITIALISING_A_QUICK_RESTART())
					AND (FMMC_CHECKPOINTS_IS_ANY_RETRY_START_POINT_SET())
						SET_BIT(MC_serverBD_3.TSServerData[MC_playerBD[iPartToUse].iteam].iBitSet, TSS_BI_BLOCKED)
						PRINTLN("TRIP SKIP - BLOCKED - TSS_BI_BLOCKED")
					ENDIF
				ENDIF
				
				IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
				AND NOT IS_BIT_SET(MC_serverBD_3.TSServerData[MC_playerBD[iPartToUse].iteam].iBitSet, TSS_BI_BLOCKED)
					vTripSkipDestination = MPGlobalsAmbience.vQuickGPS
					PRINTLN("(2) TRIP SKIP - CLIENT - vTripSkipDestination: ", vTripSkipDestination)
					PRINTLN("(2) TRIP SKIP - CLIENT - TSServerBD.vCarPos.x: ", MC_serverBD_3.TSServerData[MC_playerBD[iPartToUse].iteam].vCarPos.x, " TSServerBD.vCarPos.y: ", MC_serverBD_3.TSServerData[MC_playerBD[iPartToUse].iteam].vCarPos.y, " TSServerBD.vCarPos.z: ", MC_serverBD_3.TSServerData[MC_playerBD[iPartToUse].iteam].vCarPos.z)
					
					IF (bTeamOnGDRule
					AND GET_MC_CLIENT_MISSION_STAGE(iPartToUse) = CLIENT_MISSION_STAGE_DELIVER_VEH)
					OR NOT bTeamOnGDRule 
						IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
							MAINTAIN_TRIP_SKIP_CLIENT(MC_serverBD_3.TSServerData[MC_playerBD[iPartToUse].iteam], MC_playerBD[iLocalPart].TSPlayerData, TSLocalData, vTripSkipDestination, IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitsetTwo[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE2_USE_TRIP_SKIP), IS_FAKE_MULTIPLAYER_MODE_SET() OR (GET_CURRENT_HEIST_COMPLETION_STAT_FOR_LEADER() > 0), bHasTripSkipJustFinished)
						ENDIF
					ELSE
						IF MC_playerBD[iLocalPart].TSPlayerData.iStage > TSS_VOTE
						AND MC_playerBD[iLocalPart].TSPlayerData.iStage <= TSS_INTERP_CAM
							IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
								PRINTLN("TRIP SKIP - CLIENT - (SEPARATE TEAM)- NOT ON DELIVER STAGE but already started - CLEANUP")
								MC_playerBD[iLocalPart].TSPlayerData.iStage = TSS_CLEANUP
								MAINTAIN_TRIP_SKIP_CLIENT(MC_serverBD_3.TSServerData[MC_playerBD[iLocalPart].iteam], MC_playerBD[iLocalPart].TSPlayerData, TSLocalData, vTripSkipDestination, IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitsetTwo[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE2_USE_TRIP_SKIP), IS_FAKE_MULTIPLAYER_MODE_SET() OR (GET_CURRENT_HEIST_COMPLETION_STAT_FOR_LEADER() > 0), bHasTripSkipJustFinished)
							ENDIF
						#IF IS_DEBUG_BUILD
						ELSE
							PRINTLN("TRIP SKIP - CLIENT - (SEPARATE TEAM)- NOT ON DELIVER STAGE")
						#ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF bIsLocalPlayerHost
					INT iTripSkipTeam
					FOR iTripSkipTeam = 0 TO (FMMC_MAX_TEAMS-1)
						IF IS_TEAM_ACTIVE(iTripSkipTeam)
							PRINTLN("________________ TRIP SKIP - SERVER - (SEPARATE TEAM) - NOT ON DELIVER STAGE iTripSkipTeam: ", iTripSkipTeam, "_________________")
							MAINTAIN_TRIP_SKIP_SERVER(MC_serverBD_3.TSServerData[iTripSkipTeam], (MC_playerBD[iLocalPart].bPlayedFirstDialogue OR IS_THIS_A_QUICK_RESTART_JOB()), iTripSkipTeam)
						ENDIF
					ENDFOR
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_INIT_TRIP_SKIP_DATA_FOR_LOCAL_PLAYER()
	
	// -1 means unassigned by MC yet. Any other value is assigned.
	IF GET_CURRENT_HEIST_COMPLETION_STAT_FOR_LEADER() = -1
		EXIT
	ENDIF
	
	IF IS_FAKE_MULTIPLAYER_MODE_SET()
		INIT_TRIP_SKIP(MC_playerBD[iLocalPart].TSPlayerData, IS_FAKE_MULTIPLAYER_MODE_SET())			
	
	ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_EnableTripSkipForMission)		
		INIT_TRIP_SKIP(MC_playerBD[iLocalPart].TSPlayerData, GET_CURRENT_HEIST_COMPLETION_STAT_FOR_LEADER() > 0)
		PRINTLN("[RCC MISSION][PROCESS_CLIENT][GAME_STATE_INI] TRIP SKIP - INIT_TRIP_SKIP - bCompletedBefore = ", GET_CURRENT_HEIST_COMPLETION_STAT_FOR_LEADER() > 0)		
		
	ENDIF
ENDPROC


				
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Vehicles
// ##### Description: Logic for local player in relation to vehicles
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_SECUROSERV_HACKING_VEH()
	
	IF IS_BIT_SET(iLocalBoolCheck23, LBOOL23_SOMEONE_IS_HACKING_A_VEHICLE)
		CLEAR_BIT(iLocalBoolCheck23, LBOOL23_SOMEONE_IS_HACKING_A_VEHICLE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_SECUROSERV_HACKING)
		IF NOT IS_BIT_SET(iLocalBoolCheck23, LBOOL23_SOMEONE_IS_HACKING_A_VEHICLE)
		AND iHackStage != ciSECUROSERV_HACK_STAGE_HACK_COMPLETE
		AND bReadyToCapture
			iHackingVehInRangeBitSet = 0
			PRINTLN("[SECUROHACK] Reseting hack stage as not LBOOL23_SOMEONE_IS_HACKING_A_VEHICLE")
			iHackStage = ciSECUROSERV_HACK_STAGE_NO_SIGNAL
			CLEAR_BIT(iLocalBoolCheck26, LBOOL26_SERVUROSERV_LOSING_SIGNAL)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_LOCAL_PLAYER_IN_VEHICLE()

	IF NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciDISABLE_SPECIAL_VEHICLE_WEAPONS)
			eLastVehiclePickup = VEHICLE_PICKUP_INIT
		ENDIF
		
		IF IS_BIT_SET(iLocalBoolCheck25, LBOOL25_STROMBERG_FORCE_GPS_ACTIVE)
			CLEAR_BIT(iLocalBoolCheck25, LBOOL25_STROMBERG_FORCE_GPS_ACTIVE)
			SET_FORCE_SHOW_GPS(FALSE)
			PRINTLN("[RCC MISSION] ciENABLE_STROMBERG_SUB_GPS - Not in a vehicle clearing bit and force show gps")
		ENDIF
		
		EXIT
	ENDIF
	
	VEHICLE_INDEX viCurrentVehicle = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
		
	IF bLocalPlayerPedOk
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciDISABLE_SPECIAL_VEHICLE_WEAPONS)
			IF IS_ENTITY_ALIVE(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
				DISABLE_SPECIAL_VEHICLE_WEAPONS(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
			ELSE
				eLastVehiclePickup = VEHICLE_PICKUP_INIT
			ENDIF
		ENDIF
		
		IF GET_ENTITY_MODEL(viCurrentVehicle) = STROMBERG
			IF IS_ENTITY_IN_WATER(viCurrentVehicle)
				SET_PED_RESET_FLAG(LocalPlayerPed, PRF_DisableAutoForceOutWhenBlowingUpCar, TRUE)
			ENDIF
		ENDIF
		
		IF LocalPlayerCurrentInterior = GET_INTERIOR_AT_COORDS(vHangarInteriorCoords)
			SET_THIRD_PERSON_CAM_ORBIT_DISTANCE_LIMITS_THIS_UPDATE(10,15)
			SET_THIRD_PERSON_CAM_RELATIVE_PITCH_LIMITS_THIS_UPDATE(-13, 16)
			PRINTLN("[RCC MISSION] PROCESS_VEH_EVERY_FRAME_CLIENT - In an Avenger inside the hangar. Setting third person camera limits.")
		ENDIF
		
		IF NOT IS_BIT_SET(iLocalBoolCheck27, LBOOL27_STROMBERG_TRANSFORM_SWEETENED)
			IF GET_ENTITY_MODEL(viCurrentVehicle) = STROMBERG
				IF IS_VEHICLE_IN_SUBMARINE_MODE(viCurrentVehicle)
				AND IS_ENTITY_IN_WATER(viCurrentVehicle)
					PLAY_SOUND_FRONTEND(-1, "transform_oneshot", "dlc_xm_stromberg_sounds")
					SET_BIT(iLocalBoolCheck27, LBOOL27_STROMBERG_TRANSFORM_SWEETENED)
				ENDIF
			ENDIF
		ENDIF
		
		BOOL bIsPlayerInTrash = FALSE
		IF DOES_ENTITY_EXIST(viCurrentVehicle)
			IF GET_ENTITY_MODEL(viCurrentVehicle) = TRASH
			OR GET_ENTITY_MODEL(viCurrentVehicle) = TRASH2
				bIsPlayerInTrash = TRUE
			ENDIF
		ENDIF		
		IF( bIsPlayerInTrash )
			IF( NOT bWasKnockOffSettingChangedForTrash )
				SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE( LocalPlayerPed, KNOCKOFFVEHICLE_NEVER )
				bWasKnockOffSettingChangedForTrash = TRUE
			ENDIF
		ELSE
			IF( bWasKnockOffSettingChangedForTrash )
				SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE( LocalPlayerPed, KNOCKOFFVEHICLE_DEFAULT )
				bWasKnockOffSettingChangedForTrash = FALSE
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciENABLE_STROMBERG_SUB_GPS)		
			IF NOT IS_BIT_SET(iLocalBoolCheck25, LBOOL25_STROMBERG_FORCE_GPS_ACTIVE)
				IF GET_ENTITY_MODEL(viCurrentVehicle) = STROMBERG
				AND IS_VEHICLE_IN_SUBMARINE_MODE(viCurrentVehicle)
					SET_FORCE_SHOW_GPS(TRUE)
					SET_BIT(iLocalBoolCheck25, LBOOL25_STROMBERG_FORCE_GPS_ACTIVE)
					PRINTLN("[RCC MISSION] ciENABLE_STROMBERG_SUB_GPS - In a Stromberg, force showing gps and setting bit")
				ENDIF
			ELSE
				IF GET_ENTITY_MODEL(viCurrentVehicle) = STROMBERG
				AND NOT IS_VEHICLE_IN_SUBMARINE_MODE(viCurrentVehicle)
					CLEAR_BIT(iLocalBoolCheck25, LBOOL25_STROMBERG_FORCE_GPS_ACTIVE)
					SET_FORCE_SHOW_GPS(FALSE)
					PRINTLN("[RCC MISSION] ciENABLE_STROMBERG_SUB_GPS - Not in sub mode. clearing bit and force show gps")
				ENDIF
			ENDIF
		ENDIF
		
		MODEL_NAMES mnVeh = GET_ENTITY_MODEL(viCurrentVehicle)
		
		IF IS_THIS_MODEL_A_BIKE(mnVeh)
			
			IF MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam] < FMMC_MAX_RULES
				IF mnVeh = OPPRESSOR
					IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iLocalPart].iteam].iRuleBitsetNine[MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam]], ciBS_RULE9_DISABLE_OPPRESSOR_WINGS)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_BIKE_WINGS)
					ENDIF
					
					IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iLocalPart].iteam].iRuleBitsetNine[MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam]], ciBS_RULE9_DISABLE_OPPRESSOR_BOOST)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ROCKET_BOOST)
					ENDIF
				ENDIF
			ENDIF
			IF IS_ENTITY_IN_WATER(viCurrentVehicle)
				IF IS_PLAYER_BLOCKED_FROM_EXITING_VEHICLE_IN_MISSION()
					ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
					SET_PLAYER_BLOCKED_FROM_EXITING_VEHICLE_IN_MISSION_FLAG(FALSE)
				ENDIF
			ENDIF
			
		ELIF (mnVeh = BUZZARD
		OR mnVeh = BUZZARD2)
			IF IS_BIT_SET(g_FMMC_Struct.iTeamVehicleBitSet, ciFMMC_TeamVehBS_DISABLE_TEAM_RESPAWN_VEHICLE_SPACE_ROCKETS)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_NEXT_WEAPON)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_PREV_WEAPON)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_SELECT_NEXT_WEAPON)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_SELECT_PREV_WEAPON)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_UD)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_LR)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
			ENDIF
		ENDIF
		
		IF MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam] < FMMC_MAX_RULES
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iLocalPart].iteam].iRuleBitsetTen[MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam]], ciBS_RULE10_DISABLE_VEHICLE_JUMP)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CAR_JUMP)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_JUMP)
			ENDIF
		ENDIF
	ENDIF
		
ENDPROC

PROC PROCESS_LOCAL_PLAYER_VEHICLE_WHEN_INACTIVE(INT iTeam, INT iRule)
	// Allows the players vehicle to keep moving when they disconnect the controller for example.	
	
	IF iTeam <= -1
		EXIT
	ENDIF
	
	IF IS_PLAYER_SPECTATING(LocalPlayer)
		EXIT
	ENDIF

	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEleven[iRule], ciBS_RULE11_ALLOW_PLAYER_VEH_MOVEMENT_WITHOUT_CTRL)
		EXIT
	ENDIF
	
	IF bLocalPlayerPedOK
		IF IS_PED_IN_ANY_VEHICLE(localPlayerPed)
			IF NOT IS_BIT_SET(iLocalBoolCheck24, LBOOL24_VEHICLE_ALLOW_MOVEMENT_WHILE_INACTIVE)
				VEHICLE_INDEX vehPlayer = GET_VEHICLE_PED_IS_IN(localPlayerPed)
				IF NETWORK_HAS_ENTITY_BEEN_REGISTERED_WITH_THIS_THREAD(vehPlayer)
				AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(VEH_TO_NET(vehPlayer))
					IF IS_VEHICLE_DRIVEABLE(vehPlayer)
						SET_VEHICLE_LIMIT_SPEED_WHEN_PLAYER_INACTIVE(vehPlayer, FALSE)
						SET_VEHICLE_STOP_INSTANTLY_WHEN_PLAYER_INACTIVE(vehPlayer, FALSE)
						PRINTLN("[LM] SET_VEHICLE_STOP_INSTANTLY_WHEN_PLAYER_INACTIVE / SET_VEHICLE_LIMIT_SPEED_WHEN_PLAYER_INACTIVE - Setting Bit.")
						SET_BIT(iLocalBoolCheck24, LBOOL24_VEHICLE_ALLOW_MOVEMENT_WHILE_INACTIVE)
					ENDIF	
				ENDIF
			ENDIF
		ELSE
			PRINTLN("[LM] SET_VEHICLE_STOP_INSTANTLY_WHEN_PLAYER_INACTIVE / SET_VEHICLE_LIMIT_SPEED_WHEN_PLAYER_INACTIVE - Clearing Bit (1).")
			CLEAR_BIT(iLocalBoolCheck24, LBOOL24_VEHICLE_ALLOW_MOVEMENT_WHILE_INACTIVE)
		ENDIF
	ELSE
		PRINTLN("[LM] SET_VEHICLE_STOP_INSTANTLY_WHEN_PLAYER_INACTIVE / SET_VEHICLE_LIMIT_SPEED_WHEN_PLAYER_INACTIVE - Clearing Bit (2).")
		CLEAR_BIT(iLocalBoolCheck24, LBOOL24_VEHICLE_ALLOW_MOVEMENT_WHILE_INACTIVE)
	ENDIF
ENDPROC

PROC PROCESS_LOCAL_PLAYER_DEFENSE_TAKEDOWN_MODIFIER(INT iTeam, INT iRule)
	IF NOT IS_TEAM_INDEX_VALID(iTeam)
		EXIT
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck27, LBOOL27_PLAYER_TAKEDOWN_DEFENSE_MODIFIER_APPLIED)
		IF DOES_LOCAL_PLAYER_HAVE_NEW_PRIORITY_THIS_FRAME()
			//Reset modifier
			IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].fTakedownDefenseModifier[iRule] = 0
				SET_PLAYER_WEAPON_TAKEDOWN_DEFENSE_MODIFIER(LocalPlayer, 1.0)
			ENDIF
			
			//Set a new modifier as the rule changed and the value might be different now
			CLEAR_BIT(iLocalBoolCheck27, LBOOL27_PLAYER_TAKEDOWN_DEFENSE_MODIFIER_APPLIED)
		ENDIF
	ELSE
		//Apply the modifier
		IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].fTakedownDefenseModifier[iRule] > 0
			SET_PLAYER_WEAPON_TAKEDOWN_DEFENSE_MODIFIER(LocalPlayer, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].fTakedownDefenseModifier[iRule])
			SET_BIT(iLocalBoolCheck27, LBOOL27_PLAYER_TAKEDOWN_DEFENSE_MODIFIER_APPLIED)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_HAS_LOCAL_PLAYER_BEEN_IN_THE_WATER()
	
	IF IS_BIT_SET(iLocalBoolCheck33, LBOOL33_LOCAL_PLAYER_BEEN_PROPERLY_SUBMERGED_THIS_RULE)
	OR IS_PED_INJURED(localPlayerPed)
		EXIT
	ENDIF
	
	ENTITY_INDEX eIndex 
	IF IS_PED_IN_ANY_VEHICLE(localPlayerPed, TRUE)
		eIndex = GET_VEHICLE_PED_IS_IN(localPlayerPed, TRUE)
	ELSE
		eIndex = LocalPlayerPed
	ENDIF
	
	IF DOES_ENTITY_EXIST(eIndex)
	AND IS_ENTITY_IN_WATER(eIndex)
	AND GET_ENTITY_SUBMERGED_LEVEL(eIndex) >= 1.0
		IF NOT HAS_NET_TIMER_STARTED(tdSubmersionTimer)
			PRINTLN("[RCC MISSION] PROCESS_HAS_LOCAL_PLAYER_BEEN_IN_THE_WATER - Starting Submersion Timer")
			START_NET_TIMER(tdSubmersionTimer)
		ENDIF
		
		IF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdSubmersionTimer, ciTIME_SUBMERSION_THRESHOLD)
			PRINTLN("[RCC MISSION] PROCESS_HAS_LOCAL_PLAYER_BEEN_IN_THE_WATER - Setting that localPlayerPed has been properly submerged in the water on this rule.")
			SET_BIT(iLocalBoolCheck33, LBOOL33_LOCAL_PLAYER_BEEN_PROPERLY_SUBMERGED_THIS_RULE)
		ENDIF
	ELSE
		IF HAS_NET_TIMER_STARTED(tdSubmersionTimer)
			PRINTLN("[RCC MISSION] PROCESS_HAS_LOCAL_PLAYER_BEEN_IN_THE_WATER - Resettting Submersion Timer")
			RESET_NET_TIMER(tdSubmersionTimer)
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_REQUIRED_OBJECT_HELD(INT& iTeam, INT& iRule)

	IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjRequiredForRulePass[iRule] = -1
		EXIT
	ENDIF
	
	IF MC_serverBD.iObjCarrier[g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjRequiredForRulePass[iRule]] = iLocalPart
		IF iLastDeliveredContinuityObjectThisRule = -1
		AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjRequiredForRulePass[iRule]].iContinuityId > -1
			iLastDeliveredContinuityObjectThisRule = g_FMMC_STRUCT_ENTITIES.sPlacedObject[g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjRequiredForRulePass[iRule]].iContinuityId
			PRINTLN("[CONTINUITY] PROCESS_REQUIRED_OBJECT_HELD - Setting iLastDeliveredObjectThisRule to Continuity ID: ", iLastDeliveredContinuityObjectThisRule)
		ENDIF
	ELIF iLastDeliveredContinuityObjectThisRule != -1
	AND !bLocalPlayerPedOk
		PRINTLN("[CONTINUITY] PROCESS_REQUIRED_OBJECT_HELD - Setting iLastDeliveredObjectThisRule to -1")
		iLastDeliveredContinuityObjectThisRule = -1
	ENDIF

ENDPROC

PROC PROCES_REMOVE_LOCAL_PLAYER_HELMET_CREATOR_OPTION()
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciREMOVE_PLAYER_HELMETS)
		EXIT
	ENDIF
	
	REMOVE_PLAYER_HELMET(LocalPlayer, TRUE)
ENDPROC

PROC PROCESS_ENABLE_STUNT_DRIVING_FOR_LOCAL_PLAYER()
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTen, ciENABLE_STUNT_DRIVING)
		EXIT
	ENDIF

	IF IS_PED_INJURED(localPlayerPed)
		CLEAR_BIT(iLocalBoolCheck19, LBOOL19_SET_UP_VEHICLE_FOR_STUNT_RACE)
		EXIT
	ENDIF
	
	IF NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		CLEAR_BIT(iLocalBoolCheck19, LBOOL19_SET_UP_VEHICLE_FOR_STUNT_RACE)
		EXIT
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck19, LBOOL19_SET_UP_VEHICLE_FOR_STUNT_RACE)
		EXIT
	ENDIF
	
	VEHICLE_INDEX viPlayerVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)		
	STUNT_RACE_VEHICLE_DAMAGE_SCALE(viPlayerVeh)		
	SET_IN_STUNT_MODE(TRUE)
	SET_BIT(iLocalBoolCheck19, LBOOL19_SET_UP_VEHICLE_FOR_STUNT_RACE)
	
	PRINTLN("[LocalPlayer] - PROCESS_ENABLE_STUNT_DRIVING_FOR_LOCAL_PLAYER - Enabling Stunt Driving for this vehicle.")
ENDPROC

PROC PROCESS_DIVING_HELP_TEXT(INT iTeam, INT iRule)

	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFourteen[iRule], ciBS_RULE14_DO_DIVE_HELP)
		EXIT
	ENDIF
	
	IF !bLocalPlayerOK
		EXIT
	ENDIF

	IF IS_PED_SITTING_IN_ANY_VEHICLE(LocalPlayerPed)
		EXIT
	ENDIF
	
	IF NOT IS_MP_HEIST_GEAR_EQUIPPED(LocalPlayerPed, HEIST_GEAR_REBREATHER)
		EXIT
	ENDIF
	
	BOOL bInDeepWater = IS_ENTITY_IN_DEEP_WATER(LocalPlayerPed)
	BOOL bInFirstPerson = (GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON )
	
	IF bInDeepWater = IS_BIT_SET(iLocalBoolCheck2, LBOOL2_WAS_IN_DEEP_WATER)
	AND bInFirstPerson = IS_BIT_SET(iLocalBoolCheck2, LBOOL2_WAS_IN_FIRST_PERSON)
		//No Change
		EXIT
	ENDIF
	   
	PRINTLN("[DIVE_HELP] PROCESS_DIVING_HELP_TEXT - Player in deep water state: ", BOOL_TO_STRING(bInDeepWater), " first person: ", BOOL_TO_STRING(bInFirstPerson))
	
	IF bInFirstPerson
		SET_BIT(iLocalBoolCheck2, LBOOL2_WAS_IN_FIRST_PERSON)
	ELSE
		CLEAR_BIT(iLocalBoolCheck2, LBOOL2_WAS_IN_FIRST_PERSON)
	ENDIF
	
	IF bInDeepWater
		
		IF bInFirstPerson
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HELP_HDVE")
				CLEAR_HELP()
			ENDIF
		ELIF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HELP_HDVEFP")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HELP_HDVEFA")
			CLEAR_HELP()
		ENDIF
		
		IF IS_HELP_MESSAGE_BEING_DISPLAYED()
			//Bail until we can show
			EXIT
		ENDIF
		
		IF bInFirstPerson
			IF !IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
			AND IS_LOOK_INVERTED()
				PRINT_HELP("HELP_HDVEFP")
			ELSE
				PRINT_HELP("HELP_HDVEFA")
			ENDIF
		ELSE
			PRINT_HELP("HELP_HDVE")
		ENDIF
	
		SET_BIT(iLocalBoolCheck2, LBOOL2_WAS_IN_DEEP_WATER)
		
	ELSE
		
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HELP_HDVEFP")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HELP_HDVEFA")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HELP_HDVE")
			CLEAR_HELP()
		ENDIF
		
		CLEAR_BIT(iLocalBoolCheck2, LBOOL2_WAS_IN_DEEP_WATER)
		
	ENDIF
													
ENDPROC

PROC MAINTAIN_EMERGENCY_CALLS()
	
	IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitsetTwo[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]],ciBS_RULE2_ENABLE_EMERGENCY_CALLS)
			IF NOT ARE_EMERGENCY_CALLS_SUPPRESSED()
				SUPPRESS_EMERGENCY_CALLS()
			ENDIF
		ELSE
			IF ARE_EMERGENCY_CALLS_SUPPRESSED()
				RELEASE_SUPPRESSED_EMERGENCY_CALLS()
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_CONTACT_IN_PHONEBOOK(CHAR_CALL911, MULTIPLAYER_BOOK)
		INT iStatInt = GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_NON_MS_HELP_TEXT5)
		IF IS_BIT_SET(iStatInt, biNmh5_AddedEmergencyServices)
			ADD_CONTACT_TO_PHONEBOOK (CHAR_CALL911, MULTIPLAYER_BOOK, FALSE)
		ENDIF
	ENDIF
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Local Player Abilities  ---------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Ability processing for local player.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

// ##### Section Name: Support Sniper Ability  ----------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Support Sniper ability processing for local player.

PROC CLEAR_SNIPER_TARGET()
	PRINTLN("[PlayerAbilities][Sniper] Clear Sniper Target.")
	INT iTagged	
	FOR iTagged = 0 TO FMMC_MAX_TAGGED_ENTITIES-1
		IF MC_serverBD_3.iTaggedEntityType[iTagged] = ENUM_TO_INT(ET_PED)
			IF DOES_BLIP_EXIST(biTaggedEntity[iTagged])
				REMOVE_BLIP(biTaggedEntity[iTagged])
			ENDIF
			IF bIsLocalPlayerHost				
				CLEAR_BIT(MC_serverBD_3.iTaggedEntityBitset, iTagged)
				MC_serverBD_3.iTaggedEntityType[iTagged] = 0
				MC_serverBD_3.iTaggedEntityIndex[iTagged] = -1
				MC_serverBD_3.iTaggedEntityGangChaseUnit[iTagged] = -1
			ENDIF
		ENDIF
	ENDFOR
  ENDPROC

FUNC BOOL GET_SNIPER_TARGET_PED_STATE(FMMC_PED_STATE& sPedState)
	INT iTagged	
	FOR iTagged = 0 TO FMMC_MAX_TAGGED_ENTITIES-1
		
		IF MC_serverBD_3.iTaggedEntityType[iTagged] = ENUM_TO_INT(ET_PED)
			
			// check gang chase peds
			IF MC_serverBD_3.iTaggedEntityGangChaseUnit[iTagged] >= 0
				FILL_FMMC_GANG_CHASE_PED_STATE_STRUCT(sPedState, MC_serverBD_3.iTaggedEntityGangChaseUnit[iTagged], MC_serverBD_3.iTaggedEntityIndex[iTagged], TRUE)
				IF sPedState.bExists
					RETURN TRUE
				ENDIF			
			ENDIF
			
			// check placed peds
			FILL_FMMC_PED_STATE_STRUCT(sPedState, MC_serverBD_3.iTaggedEntityIndex[iTagged], TRUE)
			IF sPedState.bExists
				RETURN TRUE
			ENDIF
			
		ENDIF
		
	ENDFOR
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SUPPORT_SNIPER_HAS_TARGET()

	// do I have a target?
	INT iTagged
	FOR iTagged = 0 TO FMMC_MAX_TAGGED_ENTITIES-1
		IF MC_serverBD_3.iTaggedEntityType[iTagged] != ENUM_TO_INT(ET_NONE)
			RETURN TRUE
		ENDIF
	ENDFOR
	
#IF IS_DEBUG_BUILD
	RETURN sLocalPlayerAbilities.sSupportSniperData.bHasValidTargetDebug
#ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL IS_SUPPORT_SNIPER_TARGET_VALID()
	FMMC_PED_STATE sPedState
	IF GET_SNIPER_TARGET_PED_STATE(sPedState)
		IF NOT sPedState.bInjured
			RETURN TRUE
		ENDIF
	ENDIF
#IF IS_DEBUG_BUILD
	RETURN sLocalPlayerAbilities.sSupportSniperData.bHasValidTargetDebug
#ENDIF

	RETURN FALSE
ENDFUNC

PROC ON_SNIPER_DEPLOY()
	PRINTLN("[PlayerAbilities][Sniper] Sniper is deploying")
	RESET_NET_TIMER(sLocalPlayerAbilities.sSupportSniperData.tAimTimer)
	RESET_NET_TIMER(sLocalPlayerAbilities.sSupportSniperData.tDurationTimer)
	START_NET_TIMER(sLocalPlayerAbilities.sSupportSniperData.tDurationTimer)
	
	CLEAR_BIT(sLocalPlayerAbilities.sSupportSniperData.iBS_SupportSniperBitset, ciSupportSniperBS_SniperReadyTimerStarted)
ENDPROC

PROC ON_SNIPER_READY()
	PRINTLN("[PlayerAbilities][Sniper] Sniper is ready")
	RESET_NET_TIMER(sLocalPlayerAbilities.sSupportSniperData.tAimTimer)	
	CLEAR_SNIPER_TARGET()
	IF NOT IS_BIT_SET(sLocalPlayerAbilities.sSupportSniperData.iBS_SupportSniperBitset, ciSupportSniperBS_SniperReadyTimerStarted)
		SET_BIT(sLocalPlayerAbilities.sSupportSniperData.iBS_SupportSniperBitset, ciSupportSniperBS_SniperReadyTimerStarted)
		RESET_NET_TIMER(sLocalPlayerAbilities.sSupportSniperData.tDurationTimer)
		START_NET_TIMER(sLocalPlayerAbilities.sSupportSniperData.tDurationTimer)			
	ENDIF
ENDPROC

PROC ON_SNIPER_AIM()
	PRINTLN("[PlayerAbilities][Sniper] Sniper is aiming")
	RESET_NET_TIMER(sLocalPlayerAbilities.sSupportSniperData.tAimTimer)
	START_NET_TIMER(sLocalPlayerAbilities.sSupportSniperData.tAimTimer)
	
	SET_BIT(sLocalPlayerAbilities.sSupportSniperData.iBS_SupportSniperBitset, ciSupportSniperBS_SniperAiming)
	FMMC_SET_LONG_BIT(MPGlobalsAmbience.iPlayerAbilitiesBitset, ciPlayerAbilitiesBS_PlaySniperAimingDialogue)
ENDPROC
  
PROC ON_SNIPER_FIRE()
	PRINTLN("[PlayerAbilities][Sniper] Sniper is firing")
	RESET_NET_TIMER(sLocalPlayerAbilities.sSupportSniperData.tAimTimer)
	
	FMMC_PED_STATE sPedState
	IF IS_SUPPORT_SNIPER_TARGET_VALID()
	AND GET_SNIPER_TARGET_PED_STATE(sPedState)
		PRINTLN("[PlayerAbilities][Sniper] Sniper has valid target and is taking the shot")
		VECTOR vStart = GET_PED_BONE_COORDS(sPedState.pedIndex, BONETAG_HEAD, << 0.0, -5.0, 0.0 >>)
		VECTOR vEnd = GET_PED_BONE_COORDS(sPedState.pedIndex, BONETAG_HEAD, << 0.0, 0.0, 0.0 >>)
	
		SHOOT_SINGLE_BULLET_BETWEEN_COORDS(vStart, vEnd, 300, TRUE, WEAPONTYPE_HEAVYSNIPER, LocalPlayerPed, FALSE, FALSE, 80)
		
		SET_PED_CONFIG_FLAG(sPedState.pedIndex, PCF_ForceRagdollUponDeath, TRUE)
		SET_PED_CONFIG_FLAG(sPedState.pedIndex, PCF_FallsOutOfVehicleWhenKilled, FALSE)
		
		APPLY_RANDOM_BLOOD_DAMAGE_TO_PED(sPedState.pedIndex)
		
		CLEAR_SNIPER_TARGET()
	ELSE
		PRINTLN("[PlayerAbilities][Sniper] Sniper does not have a valid target")
	ENDIF	
ENDPROC
  
PROC ON_SNIPER_INACTIVE()
 	PRINTLN("[PlayerAbilities][Sniper] Sniper is inactive")
	RESET_NET_TIMER(sLocalPlayerAbilities.sSupportSniperData.tAimTimer)
	RESET_NET_TIMER(sLocalPlayerAbilities.sSupportSniperData.tDurationTimer)
ENDPROC

#IF IS_DEBUG_BUILD
FUNC STRING GET_SUPPORT_SNIPER_STATE_AS_STRING(SUPPORT_SNIPER_STATE eState)
	SWITCH eState
		CASE SS_INACTIVE
		RETURN "INACTIVE"
	 	CASE SS_DEPLOY
	 	RETURN "DEPLOY"
		CASE SS_READY
		RETURN "READY"
		CASE SS_AIM
		RETURN "AIM"
		CASE SS_FIRE
		RETURN "FIRE"
	ENDSWITCH
	RETURN ""
ENDFUNC
#ENDIF

PROC CHANGE_SUPPORT_SNIPER_STATE(SUPPORT_SNIPER_STATE eState)
	
	SWITCH eState
		CASE SS_INACTIVE
			ON_SNIPER_INACTIVE()
		BREAK
		CASE SS_DEPLOY
			ON_SNIPER_DEPLOY()
		BREAK
		CASE SS_READY
			ON_SNIPER_READY()
		BREAK
		CASE SS_AIM
			ON_SNIPER_AIM()
		BREAK
		CASE SS_FIRE
			ON_SNIPER_FIRE()
		BREAK
	ENDSWITCH
	
	PRINTLN("[PlayerAbilities][Sniper] Changing state from ", GET_SUPPORT_SNIPER_STATE_AS_STRING(sLocalPlayerAbilities.sSupportSniperData.eSSState), " to ", GET_SUPPORT_SNIPER_STATE_AS_STRING(eState))
	sLocalPlayerAbilities.sSupportSniperData.eSSState = eState
	
ENDPROC

PROC ON_SUPPORT_SNIPER_ABILITY_ACTIVATED()
	PRINTLN("[PlayerAbilities][Sniper] ON_SUPPORT_SNIPER_ABILITY_ACTIVATED")
	CLEAR_BIT(sLocalPlayerAbilities.sSupportSniperData.iBS_SupportSniperBitset, ciSupportSniperBS_MarkedInvalidTarget)
	CLEAR_BIT(sLocalPlayerAbilities.sSupportSniperData.iBS_SupportSniperBitset, ciSupportSniperBS_KilledTarget)
	CLEAR_BIT(sLocalPlayerAbilities.sSupportSniperData.iBS_SupportSniperBitset, ciSupportSniperBS_SniperHasTarget)
	CLEAR_BIT(sLocalPlayerAbilities.sSupportSniperData.iBS_SupportSniperBitset, ciSupportSniperBS_SniperAiming)
	CLEAR_BIT(sLocalPlayerAbilities.sSupportSniperData.iBS_SupportSniperBitset, ciSupportSniperBS_SniperEndedWithNoTarget)
		
	CHANGE_SUPPORT_SNIPER_STATE(SS_DEPLOY)
ENDPROC

PROC PROCESS_LOCAL_PLAYER_SUPPORT_SNIPER_ABILITY_ACTIVE_STATE(PLAYER_ABILITY_STATE& ePlayerAbilityState)

	IF ePlayerAbilityState != PAS_ACTIVE
		EXIT
	ENDIF
	
	SUPPORT_SNIPER_STATE eNewState = sLocalPlayerAbilities.sSupportSniperData.eSSState
	
	SWITCH sLocalPlayerAbilities.sSupportSniperData.eSSState
		CASE SS_INACTIVE													
		BREAK
		CASE SS_DEPLOY
			// Has deploy timer expired?
			IF HAS_NET_TIMER_EXPIRED(sLocalPlayerAbilities.sSupportSniperData.tDurationTimer, g_FMMC_STRUCT.sPlayerAbilities.sSupportSniper.iDeployDuration * 1000)
				// Go to ready
				eNewState = SS_READY							
			ENDIF
		BREAK
		CASE SS_READY
			CLEAR_BIT(sLocalPlayerAbilities.sSupportSniperData.iBS_SupportSniperBitset, ciSupportSniperBS_MarkedInvalidTarget)			
			
			IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				PRINT_HELP("SNIPER_TAG_HELP_1")
			ENDIF
			
			// Has extraction timer expired?
			IF HAS_NET_TIMER_EXPIRED(sLocalPlayerAbilities.sSupportSniperData.tDurationTimer, GET_SNIPER_TIME_TILL_EXTRACTION())
				// Go to inactive
				IF ABILITY_HAS_COOLDOWN_DURATION(PA_SUPPORT_SNIPER)
					ePlayerAbilityState = PAS_COOLDOWN
				ELSE
					ePlayerAbilityState = PAS_UNAVAILABLE
				ENDIF
				
				IF NOT IS_BIT_SET(sLocalPlayerAbilities.sSupportSniperData.iBS_SupportSniperBitset, ciSupportSniperBS_KilledTarget)
					SET_BIT(sLocalPlayerAbilities.sSupportSniperData.iBS_SupportSniperBitset, ciSupportSniperBS_SniperEndedWithNoTarget)
				ENDIF
				
			// Has a target ped been selected?
			ELIF SUPPORT_SNIPER_HAS_TARGET()
				// is the target valid?
				IF IS_SUPPORT_SNIPER_TARGET_VALID()
					// Go to Aim
					eNewState = SS_AIM
					SET_BIT(sLocalPlayerAbilities.sSupportSniperData.iBS_SupportSniperBitset, ciSupportSniperBS_SniperHasTarget)
				ELSE
					SET_BIT(sLocalPlayerAbilities.sSupportSniperData.iBS_SupportSniperBitset, ciSupportSniperBS_MarkedInvalidTarget)
				ENDIF
			ENDIF					
		BREAK
		CASE SS_AIM
			// Is target ped is invalid
			IF NOT IS_SUPPORT_SNIPER_TARGET_VALID()
				// return to ready
				eNewState = SS_READY			
			// Has the aiming timer expired?
			ELIF HAS_NET_TIMER_EXPIRED(sLocalPlayerAbilities.sSupportSniperData.tAimTimer, g_FMMC_STRUCT.sPlayerAbilities.sSupportSniper.iAimDuration * 1000)
				// Go to fire
				eNewState = SS_FIRE		
			ENDIF
		BREAK
		CASE SS_FIRE
			// Is the target ped valid?
			IF NOT IS_SUPPORT_SNIPER_TARGET_VALID()
				PRINTLN("[PlayerAbilities] Sniper: Killed target.")
				SET_BIT(sLocalPlayerAbilities.sSupportSniperData.iBS_SupportSniperBitset, ciSupportSniperBS_KilledTarget)											
				
				// return to ready				
				eNewState = SS_READY
			ENDIF
		BREAK
		
	ENDSWITCH
	
	IF eNewState != sLocalPlayerAbilities.sSupportSniperData.eSSState
		CHANGE_SUPPORT_SNIPER_STATE(eNewState)
	ENDIF
	
ENDPROC

// ##### Section Name: Heli Backup Ability  ----------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Common Heli Backup ability processing for local player.

#IF IS_DEBUG_BUILD
FUNC STRING GET_HELI_BACKUP_STATE_AS_STRING(HELI_BACKUP_STATE eState)
	SWITCH eState
		CASE HB_INACTIVE	RETURN "HB_INACTIVE"
		CASE HB_LAUNCHING	RETURN "HB_LAUNCHING"
		CASE HB_ACTIVE		RETURN "HB_ACTIVE"
	ENDSWITCH	
	RETURN ""
ENDFUNC
#ENDIF

PROC ON_HELI_BACKUP_ABILITY_DEACTIVATED()
	PRINTLN("[PlayerAbilities] ON_HELI_BACKUP_ABILITY_DEACTIVATED")
#IF FEATURE_HEIST_ISLAND
	HEIST_ISLAND_BACKUP_HELI__CANCEL_BACKUP()
#ENDIF
ENDPROC

PROC CHANGE_HELI_BACKUP_STATE(HELI_BACKUP_STATE eState)
	PRINTLN("[PlayerAbilities][Heli Backup] Changing state to ", GET_HELI_BACKUP_STATE_AS_STRING(eState))

	SWITCH eState
		CASE HB_INACTIVE
			ON_HELI_BACKUP_ABILITY_DEACTIVATED()
		BREAK
		CASE HB_LAUNCHING BREAK	
		CASE HB_ACTIVE BREAK
	ENDSWITCH	

	
	sLocalPlayerAbilities.sHeliBackupData.eHBState = eState
ENDPROC

PROC PROCESS_LOCAL_PLAYER_HELI_BACKUP_ABILITY_ACTIVE_STATE(PLAYER_ABILITY_STATE& eState)	
	
	IF eState != PAS_ACTIVE
		EXIT
	ENDIF
	
#IF FEATURE_HEIST_ISLAND
	
	SWITCH sLocalPlayerAbilities.sHeliBackupData.eHBState
		CASE HB_INACTIVE
		BREAK
		CASE HB_LAUNCHING
			IF HEIST_ISLAND_BACKUP_HELI__IS_ACTIVE()
				PRINTLN("[PlayerAbilities] Heli backup is switching internal state to active.")
				CHANGE_HELI_BACKUP_STATE(HB_ACTIVE)
			ENDIF
		BREAK
		CASE HB_ACTIVE
			IF NOT HEIST_ISLAND_BACKUP_HELI__IS_ACTIVE()
				// if a cooldown has been set, go to cooldown, otherwise go straight to ready
				IF ABILITY_HAS_COOLDOWN_DURATION(PA_HELI_BACKUP)				
					PRINTLN("[PlayerAbilities] Heli backup is no longer active, extiting to cooldown state.")
					eState = PAS_COOLDOWN
				ELSE
					PRINTLN("[PlayerAbilities] Heli backup is no longer active, extiting to ready state.")
					eState = PAS_READY
				ENDIF
				
				CHANGE_HELI_BACKUP_STATE(HB_INACTIVE)
			ENDIF
		BREAK
	ENDSWITCH
	
#ENDIF
	
ENDPROC

PROC ON_HELI_BACKUP_ABILITY_ACTIVATED()
	PRINTLN("[PlayerAbilities] ON_HELI_BACKUP_ABILITY_ACTIVATED")
	#IF FEATURE_HEIST_ISLAND
		HEIST_ISLAND_BACKUP_HELI__CALL_BACKUP()
		CHANGE_HELI_BACKUP_STATE(HB_LAUNCHING)
	#ENDIF
ENDPROC


// ##### Section Name: Heavy Loadout Ability  ----------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Heavy Loadout Ability processing for local player.

#IF IS_DEBUG_BUILD
FUNC STRING GET_HEAVY_LOADOUT_STATE_AS_STRING(HEAVY_LOADOUT_STATE eState)
	SWITCH eState
		CASE HL_INACTIVE
			RETURN "HL_INACTIVE"
		CASE HL_SET_UP_CONTAINER
			RETURN "HL_SET_UP_CONTAINER"
		CASE HL_SPAWN_CONTAINER
			RETURN "HL_SPAWN_CONTAINER"
		CASE HL_DROP_CONTAINER
			RETURN "HL_DROP_CONTAINER"
		CASE HL_SPAWN_CONTENTS
			RETURN "HL_SPAWN_CONTENTS"
		CASE HL_FINISHED
			RETURN "HL_FINISHED"
	ENDSWITCH
	  
	RETURN ""  
ENDFUNC
#ENDIF

FUNC ENTITY_INDEX GET_HEAVY_LOADOUT_CONTAINER_ENTITY_INDEX()
	NETWORK_INDEX niContainerNetId = GET_DYNOPROP_NET_ID(g_FMMC_STRUCT.sPlayerAbilities.sHeavyLoadout.iContainerIndex)
	RETURN NET_TO_ENT(niContainerNetId)
ENDFUNC

FUNC VECTOR GET_HEAVY_LOADOUT_CONTAINER_TARGET_SPAWN_POS(VECTOR vPlayerCoords, VECTOR vPlayerForward)
		
	VECTOR vTargetSpawnPos = vPlayerCoords + (vPlayerForward * cfHeavyLoadout_CharacterProximity)
	
	PRINTLN("[PlayerAbilities][Heavy Loadout] GET_HEAVY_LOADOUT_CONTAINER_TARGET_SPAWN_POS: Player Coords: ", vPlayerCoords, " Player Forward: ", vPlayerForward, ", Returning: ", vTargetSpawnPos)
	
	RETURN vTargetSpawnPos
ENDFUNC

FUNC BOOL GET_HEAVY_LOADOUT_CONTAINER_SPAWN_POS(VECTOR vPlayerCoords, VECTOR vPlayerForward)
	
	// do shapetest
  	VECTOR vPos, vNormal, vStart, vEnd
	
    SHAPETEST_STATUS stStatusHLContainerTest
  	stStatusHLContainerTest = GET_SHAPE_TEST_RESULT(stiHLContainerShapeTest, iHLContainerShapeHitSomething, vPos, vNormal, HLContainerTestEntity)
	IF stStatusHLContainerTest = SHAPETEST_STATUS_RESULTS_READY
					
		IF DOES_ENTITY_EXIST(HLContainerTestEntity)
			
			PRINTLN("[PlayerAbilities][Heavy Loadout] GET_HEAVY_LOADOUT_CONTAINER_SPAWN_POS: Found Result.")
			
			IF iHLContainerShapeHitSomething = 0
				// no collision, this is good, take result & bail
				// record the result 
				sLocalPlayerAbilities.sHeavyLoadoutData.sContainerProgress.vShapeTestResults[sLocalPlayerAbilities.sHeavyLoadoutData.sContainerProgress.iShapeTestCount] = sLocalPlayerAbilities.sHeavyLoadoutData.sContainerProgress.vCurrentShapeTestPoint
			
				PRINTLN("[PlayerAbilities][Heavy Loadout] GET_HEAVY_LOADOUT_CONTAINER_SPAWN_POS: Found clear spawn point: ", sLocalPlayerAbilities.sHeavyLoadoutData.sContainerProgress.vShapeTestResults[sLocalPlayerAbilities.sHeavyLoadoutData.sContainerProgress.iShapeTestCount], " on attempt: ", sLocalPlayerAbilities.sHeavyLoadoutData.sContainerProgress.iShapeTestCount)
				RETURN TRUE
			ELIF iHLContainerShapeHitSomething = 1
				// there was a collision, see if there's a better position
				
				// record the result 
				sLocalPlayerAbilities.sHeavyLoadoutData.sContainerProgress.vShapeTestResults[sLocalPlayerAbilities.sHeavyLoadoutData.sContainerProgress.iShapeTestCount] = vPos
			
				PRINTLN("[PlayerAbilities][Heavy Loadout] GET_HEAVY_LOADOUT_CONTAINER_SPAWN_POS: Found obstructed spawn point: ", sLocalPlayerAbilities.sHeavyLoadoutData.sContainerProgress.vShapeTestResults[sLocalPlayerAbilities.sHeavyLoadoutData.sContainerProgress.iShapeTestCount], " on attempt: ", sLocalPlayerAbilities.sHeavyLoadoutData.sContainerProgress.iShapeTestCount)
				
				// increase the current test count
				sLocalPlayerAbilities.sHeavyLoadoutData.sContainerProgress.iShapeTestCount++
				
			ENDIF
		ELSE
			PRINTLN("[PlayerAbilities][Heavy Loadout] GET_HEAVY_LOADOUT_CONTAINER_SPAWN_POS: Result is ready but test entity does not exist.")
		ENDIF
	ELIF stStatusHLContainerTest = SHAPETEST_STATUS_NONEXISTENT
		
		IF sLocalPlayerAbilities.sHeavyLoadoutData.sContainerProgress.iShapeTestCount < ciHeavyLoadout_MaxShapeTests
			PRINTLN("[PlayerAbilities][Heavy Loadout] GET_HEAVY_LOADOUT_CONTAINER_SPAWN_POS: Starting new shape test.")
			
			// start a new shape test			
			// find the angle to rotate the original point around
			FLOAT fAngleIncrement = 360.0 / ciHeavyLoadout_MaxShapeTests
			FLOAT fAngle = fAngleIncrement * sLocalPlayerAbilities.sHeavyLoadoutData.sContainerProgress.iShapeTestCount
					
			// rotate to find the new target point to check
			
			VECTOR_2D v2Player
			v2Player.x = vPlayerCoords.x
			v2Player.y = vPlayerCoords.y
			
			VECTOR vTarget = GET_HEAVY_LOADOUT_CONTAINER_TARGET_SPAWN_POS(vPlayerCoords, vPlayerForward)
			VECTOR_2D v2Target
			v2Target.x = vTarget.x
			v2Target.y = vTarget.y
			
			VECTOR_2D v2NewTarget = ROTATE_VECTOR_2D_AROUND_VECTOR_2D(v2Player, v2Target, fAngle)
			sLocalPlayerAbilities.sHeavyLoadoutData.sContainerProgress.vCurrentShapeTestPoint = <<v2NewTarget.x,v2NewTarget.y,vPlayerCoords.z - 1.0>>			
					
			vEnd = sLocalPlayerAbilities.sHeavyLoadoutData.sContainerProgress.vCurrentShapeTestPoint
			vStart = vEnd
			vStart.z += cfHeavyLoadout_DropHeight
					
			stiHLContainerShapeTest = START_SHAPE_TEST_SWEPT_SPHERE(vStart, vEnd, 1.0, SCRIPT_INCLUDE_ALL)
			PRINTLN("[PlayerAbilities][Heavy Loadout] GET_HEAVY_LOADOUT_CONTAINER_SPAWN_POS: Testing from: ", vStart, " to ", vEnd)
		ELSE
			PRINTLN("[PlayerAbilities][Heavy Loadout] GET_HEAVY_LOADOUT_CONTAINER_SPAWN_POS: Performed max shape tests, returning true.")
			CLAMP_INT(sLocalPlayerAbilities.sHeavyLoadoutData.sContainerProgress.iShapeTestCount, 0, ciHeavyLoadout_MaxShapeTests - 1)
			
			RETURN TRUE
		ENDIF
				
	ENDIF
	
	RETURN FALSE
  
ENDFUNC

FUNC BOOL SET_UP_HEAVY_LOADOUT_CONTAINER()
	
	IF GET_HEAVY_LOADOUT_CONTAINER_SPAWN_POS(sLocalPlayerAbilities.sHeavyLoadoutData.vPlayerCoords, sLocalPlayerAbilities.sHeavyLoadoutData.vPlayerForward)
		VECTOR vSpawnPos
		
		// if clear to drop
		IF iHLContainerShapeHitSomething = 0
			// best position is current test point
			vSpawnPos = sLocalPlayerAbilities.sHeavyLoadoutData.sContainerProgress.vCurrentShapeTestPoint
		ELSE
			// find best position (smallest z delta to the player's position)
			INT i
			FLOAT fDeltaZ, fSmallestDeltaZ = cfHeavyLoadout_DropHeight
			INT iSmallestDeltaZIndex
			FOR i = 0 TO ciHeavyLoadout_MaxShapeTests - 1
				fDeltaZ = ABSF(sLocalPlayerAbilities.sHeavyLoadoutData.sContainerProgress.vShapeTestResults[i].z - sLocalPlayerAbilities.sHeavyLoadoutData.vPlayerCoords.z)
				IF fDeltaZ < fSmallestDeltaZ
					fSmallestDeltaZ = fDeltaZ
					iSmallestDeltaZIndex = i
				ENDIF
			ENDFOR
			
			vSpawnPos = sLocalPlayerAbilities.sHeavyLoadoutData.sContainerProgress.vShapeTestResults[iSmallestDeltaZIndex]
			
		ENDIF
		
		vSpawnPos.z += cfHeavyLoadout_DropHeight
		vDroppedDynoPropLocation[g_FMMC_STRUCT.sPlayerAbilities.sHeavyLoadout.iContainerIndex] = vSpawnPos
		
		PRINTLN("[PlayerAbilities][Heavy Loadout] SET_UP_HEAVY_LOADOUT_CONTAINER: Spawn position found: ", vSpawnPos)
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC INIT_HEAVY_LOADOUT_CONTAINER_PROGRESS()
	PRINTLN("[PlayerAbilities][Heavy Loadout] INIT_HEAVY_LOADOUT_CONTAINER_PROGRESS")
	sLocalPlayerAbilities.sHeavyLoadoutData.sContainerProgress.vCurrentShapeTestPoint = <<0.0, 0.0, 0.0>>
	sLocalPlayerAbilities.sHeavyLoadoutData.sContainerProgress.iShapeTestCount = 0
	
	INT i 
	FOR i = 0 TO ciHeavyLoadout_MaxShapeTests - 1
		sLocalPlayerAbilities.sHeavyLoadoutData.sContainerProgress.vShapeTestResults[i] = <<0.0, 0.0, 0.0>>	
	ENDFOR
ENDPROC

PROC ON_HEAVY_LOADOUT_INACTIVE()
	PRINTLN("[PlayerAbilities][Heavy Loadout] ON_HEAVY_LOADOUT_INACTIVE")
ENDPROC

PROC ON_HEAVY_LOADOUT_SET_UP_CONTAINER()	
	
	IF bIsLocalPlayerHost
		// proceed straight to spawn set up as ability user is script host
		PRINTLN("[PlayerAbilities][Heavy Loadout] ON_HEAVY_LOADOUT_SET_UP_CONTAINER - Player is local host, processing container setup")
  		// use local player coords if not processing this ability for someone else
		IF NOT IS_BIT_SET(sLocalPlayerAbilities.sHeavyLoadoutData.iBS_HeavyLoadoutBitset, ciHeavyLoadoutBS_ProcessingContainerDeployment)
			sLocalPlayerAbilities.sHeavyLoadoutData.vPlayerCoords = GET_PLAYER_COORDS(PLAYER_ID())
			sLocalPlayerAbilities.sHeavyLoadoutData.vPlayerForward = GET_ENTITY_FORWARD_VECTOR(PLAYER_PED_ID())
			PRINTLN("[PlayerAbilities][Heavy Loadout] ON_HEAVY_LOADOUT_SET_UP_CONTAINER - player coords: ", sLocalPlayerAbilities.sHeavyLoadoutData.vPlayerCoords)
		ENDIF

		INIT_HEAVY_LOADOUT_CONTAINER_PROGRESS()

 	ELSE
		// broadcast for processing by script host
		PRINTLN("[PlayerAbilities][Heavy Loadout] ON_HEAVY_LOADOUT_SPAWN_CONTAINER - Player is not local host, broadcasting event for script host")
		SET_BIT(sLocalPlayerAbilities.sHeavyLoadoutData.iBS_HeavyLoadoutBitset, ciHeavyLoadoutBS_WaitingContainerDeployment)
  		BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_PlayerAbilities_StartDeploymentHeavyLoadoutContainer)
  	ENDIF
	
ENDPROC

PROC ON_HEAVY_LOADOUT_SPAWN_CONTAINER()
  	PRINTLN("[PlayerAbilities][Heavy Loadout] ON_HEAVY_LOADOUT_SPAWN_CONTAINER")
	
	IF bIsLocalPlayerHost
		PRINTLN("[PlayerAbilities][Heavy Loadout] ON_HEAVY_LOADOUT_SPAWN_CONTAINER - Player is local host, flagging container to spawn")
		FMMC_SET_LONG_BIT(iDroppedDynoPropBS, g_FMMC_STRUCT.sPlayerAbilities.sHeavyLoadout.iContainerIndex)
		SET_BIT(sLocalPlayerAbilities.sHeavyLoadoutData.iBS_HeavyLoadoutBitset, ciHeavyLoadoutBS_RequiresNetControlOfContainer)
 	ENDIF
	
ENDPROC

PROC ON_HEAVY_LOADOUT_DROP_CONTAINER()
	PRINTLN("[PlayerAbilities][Heavy Loadout] ON_HEAVY_LOADOUT_DROP_CONTAINER")
	
	IF NOT bIsLocalPlayerHost
		EXIT
	ENDIF

	vDroppedDynoPropLocation[g_FMMC_STRUCT.sPlayerAbilities.sHeavyLoadout.iContainerIndex] = <<0.0, 0.0, 0.0>>
		
	SET_ENTITY_HAS_GRAVITY(GET_HEAVY_LOADOUT_CONTAINER_ENTITY_INDEX(), TRUE)
	SET_ENTITY_VELOCITY(GET_HEAVY_LOADOUT_CONTAINER_ENTITY_INDEX(), <<0.0, 0.0, -10.0>>)
ENDPROC

PROC SETTLE_HEAVY_LOADOUT_CONTAINER()
	ENTITY_INDEX eiContainerIndex = GET_HEAVY_LOADOUT_CONTAINER_ENTITY_INDEX()
	
	VECTOR vContainerPos = GET_ENTITY_COORDS(eiContainerIndex)
	SET_ENTITY_COORDS(eiContainerIndex, vContainerPos, DEFAULT, DEFAULT, DEFAULT, FALSE)
	FREEZE_ENTITY_POSITION(eiContainerIndex, TRUE)
ENDPROC

PROC SET_UP_HEAVY_LOADOUT_SPAWN_CONTENTS()

	ENTITY_INDEX eiContainerIndex = GET_HEAVY_LOADOUT_CONTAINER_ENTITY_INDEX()	
	VECTOR vContainerPos = GET_ENTITY_COORDS(eiContainerIndex)
	FLOAT fHeightAboveGround = 0.5
	
	// flag the pickups for spawn
	FMMC_SET_LONG_BIT(iDroppedPickupBS, g_FMMC_STRUCT.sPlayerAbilities.sHeavyLoadout.iPickupIndex1)
	vDroppedPickupLocation[g_FMMC_STRUCT.sPlayerAbilities.sHeavyLoadout.iPickupIndex1] = vContainerPos + <<1.0, 0.0, 0.0>>
	PUT_VECTOR_ON_GROUND(vDroppedPickupLocation[g_FMMC_STRUCT.sPlayerAbilities.sHeavyLoadout.iPickupIndex1], fHeightAboveGround)
	BROADCAST_FMMC_SPAWN_PICKUP(g_FMMC_STRUCT.sPlayerAbilities.sHeavyLoadout.iPickupIndex1, vDroppedPickupLocation[g_FMMC_STRUCT.sPlayerAbilities.sHeavyLoadout.iPickupIndex1])
	
	FMMC_SET_LONG_BIT(iDroppedPickupBS, g_FMMC_STRUCT.sPlayerAbilities.sHeavyLoadout.iPickupIndex2)
	vDroppedPickupLocation[g_FMMC_STRUCT.sPlayerAbilities.sHeavyLoadout.iPickupIndex2] = vContainerPos + <<-1.0, 0.0, 0.0>>
	PUT_VECTOR_ON_GROUND(vDroppedPickupLocation[g_FMMC_STRUCT.sPlayerAbilities.sHeavyLoadout.iPickupIndex2], fHeightAboveGround)
	BROADCAST_FMMC_SPAWN_PICKUP(g_FMMC_STRUCT.sPlayerAbilities.sHeavyLoadout.iPickupIndex2, vDroppedPickupLocation[g_FMMC_STRUCT.sPlayerAbilities.sHeavyLoadout.iPickupIndex2])
	
	FMMC_SET_LONG_BIT(iDroppedPickupBS, g_FMMC_STRUCT.sPlayerAbilities.sHeavyLoadout.iPickupIndex3)	
	vDroppedPickupLocation[g_FMMC_STRUCT.sPlayerAbilities.sHeavyLoadout.iPickupIndex3] = vContainerPos + <<0.0, 1.0, 0.0>>
	PUT_VECTOR_ON_GROUND(vDroppedPickupLocation[g_FMMC_STRUCT.sPlayerAbilities.sHeavyLoadout.iPickupIndex3], fHeightAboveGround)
	BROADCAST_FMMC_SPAWN_PICKUP(g_FMMC_STRUCT.sPlayerAbilities.sHeavyLoadout.iPickupIndex3, vDroppedPickupLocation[g_FMMC_STRUCT.sPlayerAbilities.sHeavyLoadout.iPickupIndex3])

ENDPROC

PROC ON_HEAVY_LOADOUT_SPAWN_CONTENTS()
	PRINTLN("[PlayerAbilities][Heavy Loadout] ON_HEAVY_LOADOUT_SPAWN_CONTENTS")
	
	CLEAR_BIT(sLocalPlayerAbilities.sHeavyLoadoutData.iBS_HeavyLoadoutBitset, ciHeavyLoadoutBS_RequiresNetControlOfContainer)
	
	IF bIsLocalPlayerHost
		// If script host is processing this for someone else		
		IF sLocalPlayerAbilities.sAbilities[PA_HEAVY_LOADOUT].eState != PAS_ACTIVE
			PRINTLN("[PlayerAbilities][Heavy Loadout] ON_HEAVY_LOADOUT_SPAWN_CONTENTS - Player is local host but not ability user")
			// settle the container and broadcast for processing by ability owner
			SETTLE_HEAVY_LOADOUT_CONTAINER()  		
  			BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_PlayerAbilities_FinishedDeploymentHeavyLoadoutContainer)
		ELSE
			PRINTLN("[PlayerAbilities][Heavy Loadout] ON_HEAVY_LOADOUT_SPAWN_CONTENTS - Player is local host and ability user")
			
			SETTLE_HEAVY_LOADOUT_CONTAINER()  		
			SET_UP_HEAVY_LOADOUT_SPAWN_CONTENTS()
		ENDIF
 	ELSE
		PRINTLN("[PlayerAbilities][Heavy Loadout] ON_HEAVY_LOADOUT_SPAWN_CONTENTS - Player is not local but is ability user")
		SET_UP_HEAVY_LOADOUT_SPAWN_CONTENTS()
  	ENDIF
ENDPROC

PROC ON_HEAVY_LOADOUT_FINISHED()
	PRINTLN("[PlayerAbilities][Heavy Loadout] ON_HEAVY_LOADOUT_FINISHED")
ENDPROC

PROC CHANGE_HEAVY_LOADOUT_STATE(HEAVY_LOADOUT_STATE eState)
	PRINTLN("[PlayerAbilities][Heavy Loadout] Changing state to ", GET_HEAVY_LOADOUT_STATE_AS_STRING(eState))
	
	SWITCH eState
		CASE HL_INACTIVE
			ON_HEAVY_LOADOUT_INACTIVE()
		BREAK
		CASE HL_SET_UP_CONTAINER
			ON_HEAVY_LOADOUT_SET_UP_CONTAINER()
		BREAK
		CASE HL_SPAWN_CONTAINER
			ON_HEAVY_LOADOUT_SPAWN_CONTAINER()
		BREAK
		CASE HL_DROP_CONTAINER
			ON_HEAVY_LOADOUT_DROP_CONTAINER()
		BREAK
		CASE HL_SPAWN_CONTENTS
			ON_HEAVY_LOADOUT_SPAWN_CONTENTS()
		BREAK
		CASE HL_FINISHED
			ON_HEAVY_LOADOUT_FINISHED()
		BREAK
	ENDSWITCH
	
	sLocalPlayerAbilities.sHeavyLoadoutData.eHLState = eState
ENDPROC

PROC MAINTAIN_NETWORK_CONTROL_OF_HEAVY_LOADOUT_CONTAINER()
	NETWORK_INDEX niContainer = GET_DYNOPROP_NET_ID(g_FMMC_STRUCT.sPlayerAbilities.sHeavyLoadout.iContainerIndex)
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(niContainer)
		IF NOT NETWORK_HAS_CONTROL_OF_NETWORK_ID(niContainer)
			NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(niContainer)
			CDEBUG1LN(DEBUG_MISSION, "[VAULT DRILL] Driller is requesting control of creator object.")
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_LOCAL_PLAYER_HEAVY_LOADOUT_ABILITY_ACTIVE_STATE(PLAYER_ABILITY_STATE& eState)	
	IF eState != PAS_ACTIVE
		EXIT
	ENDIF
	
	IF IS_BIT_SET(sLocalPlayerAbilities.sHeavyLoadoutData.iBS_HeavyLoadoutBitset, ciHeavyLoadoutBS_RequiresNetControlOfContainer)
		MAINTAIN_NETWORK_CONTROL_OF_HEAVY_LOADOUT_CONTAINER()
	ENDIF
	
	HEAVY_LOADOUT_STATE eNewHLState = sLocalPlayerAbilities.sHeavyLoadoutData.eHLState
	
	SWITCH sLocalPlayerAbilities.sHeavyLoadoutData.eHLState
		CASE HL_INACTIVE
		BREAK
		CASE HL_SET_UP_CONTAINER
			IF bIsLocalPlayerHost
				// if container has been set up, spawn it
				IF SET_UP_HEAVY_LOADOUT_CONTAINER()
					eNewHLState = HL_SPAWN_CONTAINER
				ENDIF
			ELIF NOT IS_BIT_SET(sLocalPlayerAbilities.sHeavyLoadoutData.iBS_HeavyLoadoutBitset, ciHeavyLoadoutBS_WaitingContainerDeployment)
				// go straight to spawn contents, as local host has handled up till that point
				eNewHLState = HL_SPAWN_CONTENTS
			ENDIF
		BREAK
		CASE HL_SPAWN_CONTAINER
			IF bIsLocalPlayerHost
				// if container has spawned, drop it
				IF NOT IS_LONG_BIT_SET(iDroppedDynoPropBS, g_FMMC_STRUCT.sPlayerAbilities.sHeavyLoadout.iContainerIndex)
					eNewHLState = HL_DROP_CONTAINER
				ENDIF
			ELIF NOT IS_BIT_SET(sLocalPlayerAbilities.sHeavyLoadoutData.iBS_HeavyLoadoutBitset, ciHeavyLoadoutBS_WaitingContainerDeployment)
				// go straight to spawn contents, as local host has handled up till that point
				eNewHLState = HL_SPAWN_CONTENTS
			ENDIF
		BREAK
		CASE HL_DROP_CONTAINER
			IF bIsLocalPlayerHost
				// if container has landed, spawn contents
				IF NOT IS_ENTITY_IN_AIR(GET_HEAVY_LOADOUT_CONTAINER_ENTITY_INDEX())
					eNewHLState = HL_SPAWN_CONTENTS
				ENDIF
			ELSE
				PRINTLN("[PlayerAbilities][Heavy Loadout] PROCESS_LOCAL_PLAYER_HEAVY_LOADOUT_ABILITY_ACTIVE_STATE - non script host is in HL_DROP_CONTAINER, something has gone wrong.")					
			ENDIF
		BREAK
		CASE HL_SPAWN_CONTENTS
			// if contents have spawned, finish
			IF NOT IS_LONG_BIT_SET(iDroppedPickupBS, g_FMMC_STRUCT.sPlayerAbilities.sHeavyLoadout.iPickupIndex1)
			AND NOT IS_LONG_BIT_SET(iDroppedPickupBS, g_FMMC_STRUCT.sPlayerAbilities.sHeavyLoadout.iPickupIndex2)
			AND NOT IS_LONG_BIT_SET(iDroppedPickupBS, g_FMMC_STRUCT.sPlayerAbilities.sHeavyLoadout.iPickupIndex3)
				eNewHLState = HL_FINISHED
			ENDIF
		BREAK
		CASE HL_FINISHED
			// if a cooldown has been set, go to cooldown, otherwise go straight to ready
			IF ABILITY_HAS_COOLDOWN_DURATION(PA_HEAVY_LOADOUT)				
				PRINTLN("[PlayerAbilities][Heavy Loadout] no longer active, extiting to cooldown state.")
				eState = PAS_COOLDOWN
			ELSE
				PRINTLN("[PlayerAbilities][Heavy Loadout] no longer active, extiting to ready state.")
				eState = PAS_READY
			ENDIF				
		BREAK
	ENDSWITCH
	
	IF eNewHLState != sLocalPlayerAbilities.sHeavyLoadoutData.eHLState
		CHANGE_HEAVY_LOADOUT_STATE(eNewHLState)
	ENDIF
	
ENDPROC

PROC ON_HEAVY_LOADOUT_ABILITY_ACTIVATED()
	PRINTLN("[PlayerAbilities][Heavy Loadout] ON_HEAVY_LOADOUT_ABILITY_ACTIVATED")
	CHANGE_HEAVY_LOADOUT_STATE(HL_SET_UP_CONTAINER)
ENDPROC

// ##### Section Name: Air Strike Ability  ----------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Air Strike Ability processing for local player.

PROC PROCESS_LOCAL_PLAYER_AIR_STRIKE_ABILITY_ACTIVE_STATE(PLAYER_ABILITY_STATE& eState)
	
	BOOL bIsActive = TRUE
	IF NOT MPGlobalsAmbience.bLaunchAirstrike
		bIsActive = FALSE
	ELIF NOT MPGlobalsAmbience.bAirStrikeVehicleAlive
		FMMC_SET_LONG_BIT(MPGlobalsAmbience.iPlayerAbilitiesBitset, ciPlayerAbilitiesBS_PlayAirStrikeVechileDestroyedDialogue)
		bIsActive = FALSE		
	ENDIF
	
	IF NOT bIsActive
		IF ABILITY_HAS_COOLDOWN_DURATION(PA_AIR_STRIKE)				
			PRINTLN("[PlayerAbilities] Air Strike is no longer active, extiting to cooldown state.")
			eState = PAS_COOLDOWN
		ELSE
			PRINTLN("[PlayerAbilities] Air Strike is no longer active, extiting to ready state.")
			eState = PAS_READY
		ENDIF				
	ENDIF
ENDPROC

PROC ON_AIR_STRIKE_ACTIVATED()
	PRINTLN("[PlayerAbilities] ON_AIR_STRIKE_ACTIVATED")
	MPGlobalsAmbience.bLaunchAirstrike = TRUE
	MPGlobalsAmbience.bAirStrikeVehicleAlive = TRUE
ENDPROC

// ##### Section Name: Vehicle Repair Ability  ----------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Vehicle Repair Ability processing for local player.

PROC ON_VEHICLE_REPAIR_ACTIVATED()
	PRINTLN("[PlayerAbilities][RepairAbility] ON_VEHICLE_REPAIR_ACTIVATED")
ENDPROC

FUNC BOOL IS_POSITION_SAFE_FOR_VEHICLE_REPAIR_ABILITY(VECTOR& vCoords)
	IF VDIST2(vCoords, GET_ENTITY_COORDS(LocalPlayerPed)) < 100.0
		PRINTLN("[PlayerAbilities][RepairAbility] IS_POSITION_SAFE_FOR_VEHICLE_REPAIR_ABILITY | Too close to the player!")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_HEADING_SAFE_FOR_VEHICLE_REPAIR_ABILITY(FLOAT& fHeading)
	UNUSED_PARAMETER(fHeading)
	RETURN TRUE
ENDFUNC

FUNC BOOL GET_POSITION_FOR_VEHICLE_REPAIR_ABILITY(VECTOR& vCoords, FLOAT& fHeading)
	
	VECTOR vSearchOrigin = GET_ENTITY_COORDS(LocalPlayerPed)
	sLocalPlayerAbilities.sVehicleRepairData.iLastFrameSearchingForVehicleRepair = GET_FRAME_COUNT()
	
	INT iNodeDistance = 30
	NODE_FLAGS eNodeFlags = NF_INCLUDE_SWITCHED_OFF_NODES
	
	MODEL_NAMES mnSpaceCheckModel = INT_TO_ENUM(MODEL_NAMES, HASH("sum_prop_track_pit_garage_04a"))
	
	VEHICLE_SPAWN_LOCATION_PARAMS sParams
	sParams.fMinDistFromCoords = 70.0
	sParams.fMaxDistance = 2000.0
	sParams.bCheckEntityArea = TRUE
	
	INT iNumLanes = 0
	
	IF GET_NTH_CLOSEST_VEHICLE_NODE_WITH_HEADING(vSearchOrigin, iNodeDistance, vCoords, fHeading, iNumLanes, eNodeFlags)
		PRINTLN("[PlayerAbilities][RepairAbility] GET_POSITION_FOR_VEHICLE_REPAIR_ABILITY || GET_NTH_CLOSEST_VEHICLE_NODE_WITH_HEADING was successful. vCoords: ", vCoords, " / fHeading: ", fHeading)
		
		IF HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS(vCoords, <<0,0,0>>, mnSpaceCheckModel, TRUE, vCoords, fHeading, sParams)
			PRINTLN("[PlayerAbilities][RepairAbility] GET_POSITION_FOR_VEHICLE_REPAIR_ABILITY || HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS was successful. vCoords: ", vCoords, " / fHeading: ", fHeading)
			RETURN TRUE
		ENDIF
	ENDIF

	PRINTLN("[PlayerAbilities][RepairAbility] GET_POSITION_FOR_VEHICLE_REPAIR_ABILITY | Couldn't find a safe position!")
	RETURN FALSE
	
ENDFUNC

PROC PROCESS_LOCAL_PLAYER_VEHICLE_REPAIR_ACTIVE_STATE(PLAYER_ABILITY_STATE& eState)
	
	IF CAN_RUN_ENTITY_SPAWN_POS_SEARCH(ciEntitySpawnSearchType_VehicleRepair, 0)
		VECTOR vCoords
		FLOAT fHeading
		
		IF GET_POSITION_FOR_VEHICLE_REPAIR_ABILITY(vCoords, fHeading)
			BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_PlayerAbilities_ActivatedVehicleRepair, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, fHeading, vCoords.x, vCoords.y, vCoords.z)
			eState = PAS_COOLDOWN
		ELSE
			//PRINT_HELP("VEH_REP_HELP_1")
		ENDIF
	ELSE
		PRINTLN("[PlayerAbilities][RepairAbility] PROCESS_LOCAL_PLAYER_VEHICLE_REPAIR_ACTIVE_STATE | Waiting for CAN_RUN_ENTITY_SPAWN_POS_SEARCH")
	ENDIF
ENDPROC

// ##### Section Name: Ride Along Ability  ----------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Ride Along Ability processing for local player.

PROC ON_RIDE_ALONG_ACTIVATED()
	PRINTLN("[PlayerAbilities][RideAlongAbility] ON_RIDE_ALONG_ACTIVATED")
	CLEAR_BIT(sLocalPlayerAbilities.iLocalPlayerAbilityMiscBS, ciLocalPlayerAbilityMiscBS_RideAlongPedSpawned)
ENDPROC

PROC PROCESS_LOCAL_PLAYER_RIDE_ALONG_ACTIVE_STATE(PLAYER_ABILITY_STATE& eState)
	UNUSED_PARAMETER(eState)
	
	PED_INDEX piPed = GET_FMMC_ENTITY_PED(sLocalPlayerAbilities.sRideAlongData.iRideAlongPedIndexes[0])
	
	IF NOT HAS_NET_TIMER_STARTED(sLocalPlayerAbilities.sAbilities[PA_RIDE_ALONG].tDurationTimer)
		BOOL bStartDurationTimer = FALSE
		
		IF DOES_ENTITY_EXIST(piPed)
			
			IF NOT IS_BIT_SET(sLocalPlayerAbilities.iLocalPlayerAbilityMiscBS, ciLocalPlayerAbilityMiscBS_RideAlongPedSpawned)
				PRINTLN("[PlayerAbilities][RideAlongAbility] PROCESS_LOCAL_PLAYER_RIDE_ALONG_ACTIVE_STATE | Noting that ride-along ped has spawned!")
				SET_BIT(sLocalPlayerAbilities.iLocalPlayerAbilityMiscBS, ciLocalPlayerAbilityMiscBS_RideAlongPedSpawned)
			ENDIF
			
			IF IS_ENTITY_ALIVE(piPed)
				FLOAT fDistance = VDIST2(GET_ENTITY_COORDS(piPed), GET_ENTITY_COORDS(LocalPlayerPed))
				
				IF fDistance > POW(cfPLAYER_ABILITY_RIDE_ALONG_DURATION_TIMER_START_DISTANCE, 2)
					PRINTLN("[PlayerAbilities][RideAlongAbility] PROCESS_LOCAL_PLAYER_RIDE_ALONG_ACTIVE_STATE | Too far away to start duration timer! fDistance: ", fDistance)
				ELSE
					bStartDurationTimer = TRUE
				ENDIF
			ELSE
				PRINTLN("[PlayerAbilities][RideAlongAbility] PROCESS_LOCAL_PLAYER_RIDE_ALONG_ACTIVE_STATE | Setting duration timer to start - the Ride-Along ped has died")
				bStartDurationTimer = TRUE
			ENDIF
		ELSE
			IF NOT IS_BIT_SET(sLocalPlayerAbilities.iLocalPlayerAbilityMiscBS, ciLocalPlayerAbilityMiscBS_RideAlongPedSpawned)
				PRINTLN("[PlayerAbilities][RideAlongAbility] PROCESS_LOCAL_PLAYER_RIDE_ALONG_ACTIVE_STATE | Ride-Along ped doesn't exist yet")
			ELSE
				PRINTLN("[PlayerAbilities][RideAlongAbility] PROCESS_LOCAL_PLAYER_RIDE_ALONG_ACTIVE_STATE | Ride-Along ped no longer exists. Starting timer now")
				bStartDurationTimer = TRUE
			ENDIF
			
		ENDIF
		
		IF bStartDurationTimer
			PRINTLN("[PlayerAbilities][RideAlongAbility] PROCESS_LOCAL_PLAYER_RIDE_ALONG_ACTIVE_STATE | Starting duration timer now!")
			REINIT_NET_TIMER(sLocalPlayerAbilities.sAbilities[PA_RIDE_ALONG].tDurationTimer)
		ENDIF
	ENDIF
	
ENDPROC


// ##### Section Name: Ambush Ability  ----------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Ambush Ability processing for local player.

FUNC INT GET_AMBUSH_LOCATION_TO_USE()

	INT iAmbushLocationToUse = 0
	FLOAT fClosestAmbushLocationDistance = -1.0
	
	ENTITY_INDEX eiEntityToCheck = LocalPlayerPed
	INT iRuleToCheck = GET_TEAM_CURRENT_RULE(g_FMMC_STRUCT.sPlayerAbilities.sAmbush.iAmbushTeamToUse)
	
	// Find which entity we want to check
	INT iAmbushEntityType = g_FMMC_STRUCT.sPlayerAbilities.sAmbush.iAmbushEntityType[iRuleToCheck]
	INT iAmbushEntityIndex = g_FMMC_STRUCT.sPlayerAbilities.sAmbush.iAmbushEntityIndex[iRuleToCheck]
	
	IF iAmbushEntityType != ciENTITY_TYPE_NONE
		ENTITY_INDEX eiSpecifiedEntity = GET_FMMC_ENTITY(iAmbushEntityType, iAmbushEntityIndex)
		
		IF DOES_ENTITY_EXIST(eiSpecifiedEntity)
			PRINTLN("[PlayerAbilities][AmbushAbility] GET_AMBUSH_LOCATION_TO_USE | Using mission entity || Type: ", iAmbushEntityType, " / Index: ", iAmbushEntityIndex)
			eiEntityToCheck = eiSpecifiedEntity
		ELSE
			PRINTLN("[PlayerAbilities][AmbushAbility] GET_AMBUSH_LOCATION_TO_USE | Specified mission entity doesn't exist || Type: ", iAmbushEntityType, " / Index: ", iAmbushEntityIndex)
		ENDIF
	ENDIF
	
	// Find which location we want to use
	INT iAmbushLocationToCheck
	FOR iAmbushLocationToCheck = 0 TO FMMC_MAX_RULE_VECTOR_WARPS - 1
		VECTOR vAmbushLocationPos = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sLocalPlayerAbilities.sAmbushData.iAmbushVehicleIndexes[0]].sWarpLocationSettings.vPosition[iAmbushLocationToCheck]
		
		IF IS_VECTOR_ZERO(vAmbushLocationPos)
			PRINTLN("[PlayerAbilities][AmbushAbility] GET_AMBUSH_LOCATION_TO_USE | Location ", iAmbushLocationToCheck, " isn't set up")
			RELOOP
		ENDIF
		
		INT iPart = -1
		BOOL bBlockedByParticipant = FALSE
		WHILE DO_PARTICIPANT_LOOP(iPart, DPLF_FILL_PLAYER_IDS)
		
			IF NOT DOES_ENTITY_EXIST(piParticipantLoop_PedIndex)
				RELOOP
			ENDIF
			
			FLOAT fDistanceToParticipant = VDIST2(GET_ENTITY_COORDS(piParticipantLoop_PedIndex, FALSE), vAmbushLocationPos)
			
			IF fDistanceToParticipant < POW(cfAMBUSH_ABILITY_MINIMUM_DISTANCE, 2.0)
				PRINTLN("[PlayerAbilities][AmbushAbility] GET_AMBUSH_LOCATION_TO_USE | Location ", iAmbushLocationToCheck, " is too close to participant ", iPart)
				bBlockedByParticipant = TRUE
				BREAKLOOP
			ENDIF
		ENDWHILE
		
		IF bBlockedByParticipant
			RELOOP
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.sPlayerAbilities.sAmbush.iAmbush_SpawnInFrontBS, iRuleToCheck)
			VECTOR vAmbushLocationPosInLocalSpace = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(eiEntityToCheck, vAmbushLocationPos)
			
			IF vAmbushLocationPosInLocalSpace.y <= 0.0
				PRINTLN("[PlayerAbilities][AmbushAbility] GET_AMBUSH_LOCATION_TO_USE | Location ", iAmbushLocationToCheck, " isn't in front of the entity || vAmbushLocationPosInLocalSpace: ", vAmbushLocationPosInLocalSpace)
				RELOOP
			ENDIF
		ENDIF
		
		FLOAT fDistanceToAmbushLocation = VDIST2(GET_ENTITY_COORDS(eiEntityToCheck, FALSE), vAmbushLocationPos)
		
		IF fDistanceToAmbushLocation < fClosestAmbushLocationDistance
		OR fClosestAmbushLocationDistance < 0.0
			iAmbushLocationToUse = iAmbushLocationToCheck
			fClosestAmbushLocationDistance = fDistanceToAmbushLocation
			PRINTLN("[PlayerAbilities][AmbushAbility] GET_AMBUSH_LOCATION_TO_USE | Location ", iAmbushLocationToCheck, " is the current best selection. || fClosestAmbushLocationDistance: ", fClosestAmbushLocationDistance)
		ELSE
			// We already found a more suitable location than this one
			PRINTLN("[PlayerAbilities][AmbushAbility] GET_AMBUSH_LOCATION_TO_USE | Location ", iAmbushLocationToCheck, " getting ignored - We already found a more suitable location than this one || fClosestAmbushLocationDistance: ", fClosestAmbushLocationDistance)
			RELOOP
		ENDIF
	ENDFOR
	
	RETURN iAmbushLocationToUse
ENDFUNC

PROC ON_AMBUSH_ACTIVATED()

	INT iAmbushLocationToUse = GET_AMBUSH_LOCATION_TO_USE()
	PRINTLN("[PlayerAbilities][AmbushAbility] ON_AMBUSH_ACTIVATED | iAmbushLocationToUse: ", iAmbushLocationToUse)
	
	BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_PlayerAbilities_ActivatedAmbush, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, iAmbushLocationToUse)
ENDPROC

PROC PROCESS_LOCAL_PLAYER_AMBUSH_ACTIVE_STATE(PLAYER_ABILITY_STATE& eState)
	UNUSED_PARAMETER(eState)
	
	INT iAmbushPickup
	BOOL bAllAmbushPickupsCollected = TRUE
	
	FOR iAmbushPickup = 0 TO ciAMBUSH_ABILITY_PEDS - 1
		IF FMMC_IS_LONG_BIT_SET(iPickupCollected, sLocalPlayerAbilities.sAmbushData.iAmbushPickupIndexes[iAmbushPickup])
			
		ELSE
			// Haven't collected this one!
			bAllAmbushPickupsCollected = FALSE
		ENDIF
	ENDFOR
	
	IF bAllAmbushPickupsCollected
		
		INT iPart = -1
		BOOL bParticipantNearby = FALSE
		WHILE DO_PARTICIPANT_LOOP(iPart, DPLF_FILL_PLAYER_IDS)
		
			IF NOT DOES_ENTITY_EXIST(piParticipantLoop_PedIndex)
				RELOOP
			ENDIF
			
			ENTITY_INDEX eiAmbushVehicle = GET_FMMC_ENTITY(ciENTITY_TYPE_VEHICLE, sLocalPlayerAbilities.sAmbushData.iAmbushVehicleIndexes[0])
			FLOAT fDistanceToParticipant = VDIST2(GET_ENTITY_COORDS(piParticipantLoop_PedIndex, FALSE), GET_ENTITY_COORDS(eiAmbushVehicle, FALSE))
			
			IF fDistanceToParticipant < POW(cfAMBUSH_ABILITY_CLEANUP_DISTANCE, 2.0)
				PRINTLN("[PlayerAbilities][AmbushAbility] PROCESS_LOCAL_PLAYER_AMBUSH_ACTIVE_STATE | Can't clean up yet - too close to participant ", iPart)
				bParticipantNearby = TRUE
				BREAKLOOP
			ENDIF
		ENDWHILE
		
		IF NOT bParticipantNearby
			PRINTLN("[PlayerAbilities][AmbushAbility] PROCESS_LOCAL_PLAYER_AMBUSH_ACTIVE_STATE | All Ambush pickups have been collected. Sending out cleanup event now!")
			BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_PlayerAbilities_CleanUpAmbush)
			eState = PAS_COOLDOWN
		ENDIF
		
	ENDIF
ENDPROC

// ##### Section Name: Distraction Ability  ----------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Distraction Ability processing for local player.

PROC ON_DISTRACTION_ACTIVATED()
	PRINTLN("[PlayerAbilities][DistractionAbility] ON_DISTRACTION_ACTIVATED")
	BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_PlayerAbilities_ActivatedDistraction, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT)
ENDPROC

PROC PROCESS_LOCAL_PLAYER_DISTRACTION_ACTIVE_STATE(PLAYER_ABILITY_STATE& eState)
	UNUSED_PARAMETER(eState)
ENDPROC

// ##### Section Name: Common Abilities  ----------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Common ability processing for local player.

PROC CLEAR_LOCAL_PLAYER_ABILITY_ACTIVATION_PENDING(FMMC_PLAYER_ABILITY_TYPES eType)
	SWITCH eType
		CASE PA_AIR_STRIKE
			FMMC_CLEAR_LONG_BIT(MPGlobalsAmbience.iPlayerAbilitiesBitset, ciPlayerAbilitiesBS_AirStrikeActivationPending)
		BREAK
		CASE PA_HELI_BACKUP
			FMMC_CLEAR_LONG_BIT(MPGlobalsAmbience.iPlayerAbilitiesBitset, ciPlayerAbilitiesBS_HeliBackupActivationPending)		
		BREAK
		CASE PA_HEAVY_LOADOUT
			FMMC_CLEAR_LONG_BIT(MPGlobalsAmbience.iPlayerAbilitiesBitset, ciPlayerAbilitiesBS_HeavyLoadoutActivationPending)
		BREAK
		CASE PA_RECON_DRONE
			FMMC_CLEAR_LONG_BIT(MPGlobalsAmbience.iPlayerAbilitiesBitset, ciPlayerAbilitiesBS_ReconDroneActivationPending)
		BREAK
		CASE PA_SUPPORT_SNIPER
			FMMC_CLEAR_LONG_BIT(MPGlobalsAmbience.iPlayerAbilitiesBitset, ciPlayerAbilitiesBS_SupportSniperActivationPending)
		BREAK
		CASE PA_VEHICLE_REPAIR
			FMMC_CLEAR_LONG_BIT(MPGlobalsAmbience.iPlayerAbilitiesBitset, ciPlayerAbilitiesBS_VehicleRepairActivationPending)
		BREAK
		CASE PA_RIDE_ALONG
			FMMC_CLEAR_LONG_BIT(MPGlobalsAmbience.iPlayerAbilitiesBitset, ciPlayerAbilitiesBS_RideAlongActivationPending)
		BREAK
		CASE PA_AMBUSH
			FMMC_CLEAR_LONG_BIT(MPGlobalsAmbience.iPlayerAbilitiesBitset, ciPlayerAbilitiesBS_AmbushActivationPending)
		BREAK
		CASE PA_DISTRACTION
			FMMC_CLEAR_LONG_BIT(MPGlobalsAmbience.iPlayerAbilitiesBitset, ciPlayerAbilitiesBS_DistractionActivationPending)
		BREAK
	ENDSWITCH
ENDPROC

PROC SET_LOCAL_PLAYER_ABILITY_AVAILABILITY(FMMC_PLAYER_ABILITY_TYPES eType, BOOL bIsAvailable)

	SWITCH eType
		CASE PA_AIR_STRIKE
			IF bIsAvailable FMMC_SET_LONG_BIT(MPGlobalsAmbience.iPlayerAbilitiesBitset, ciPlayerAbilitiesBS_AirStrikeAvailable)
			ELSE FMMC_CLEAR_LONG_BIT(MPGlobalsAmbience.iPlayerAbilitiesBitset, ciPlayerAbilitiesBS_AirStrikeAvailable) ENDIF
		BREAK
		CASE PA_HEAVY_LOADOUT
			IF bIsAvailable FMMC_SET_LONG_BIT(MPGlobalsAmbience.iPlayerAbilitiesBitset, ciPlayerAbilitiesBS_HeavyLoadoutAvailable)
			ELSE FMMC_CLEAR_LONG_BIT(MPGlobalsAmbience.iPlayerAbilitiesBitset, ciPlayerAbilitiesBS_HeavyLoadoutAvailable) ENDIF
		BREAK	
		CASE PA_HELI_BACKUP
			IF bIsAvailable FMMC_SET_LONG_BIT(MPGlobalsAmbience.iPlayerAbilitiesBitset, ciPlayerAbilitiesBS_HeliBackupAvailable)
			ELSE FMMC_CLEAR_LONG_BIT(MPGlobalsAmbience.iPlayerAbilitiesBitset, ciPlayerAbilitiesBS_HeliBackupAvailable) ENDIF
		BREAK
		CASE PA_RECON_DRONE
			IF bIsAvailable FMMC_SET_LONG_BIT(MPGlobalsAmbience.iPlayerAbilitiesBitset, ciPlayerAbilitiesBS_ReconDroneAvailable)
			ELSE FMMC_CLEAR_LONG_BIT(MPGlobalsAmbience.iPlayerAbilitiesBitset, ciPlayerAbilitiesBS_ReconDroneAvailable) ENDIF
		BREAK
		CASE PA_SUPPORT_SNIPER
			IF bIsAvailable FMMC_SET_LONG_BIT(MPGlobalsAmbience.iPlayerAbilitiesBitset, ciPlayerAbilitiesBS_SupportSniperAvailable)
			ELSE FMMC_CLEAR_LONG_BIT(MPGlobalsAmbience.iPlayerAbilitiesBitset, ciPlayerAbilitiesBS_SupportSniperAvailable) ENDIF
		BREAK
		CASE PA_VEHICLE_REPAIR
			IF bIsAvailable FMMC_SET_LONG_BIT(MPGlobalsAmbience.iPlayerAbilitiesBitset, ciPlayerAbilitiesBS_VehicleRepairAvailable)
			ELSE FMMC_CLEAR_LONG_BIT(MPGlobalsAmbience.iPlayerAbilitiesBitset, ciPlayerAbilitiesBS_VehicleRepairAvailable) ENDIF
		BREAK
		CASE PA_RIDE_ALONG
			IF bIsAvailable FMMC_SET_LONG_BIT(MPGlobalsAmbience.iPlayerAbilitiesBitset, ciPlayerAbilitiesBS_RideAlongAvailable)
			ELSE FMMC_CLEAR_LONG_BIT(MPGlobalsAmbience.iPlayerAbilitiesBitset, ciPlayerAbilitiesBS_RideAlongAvailable) ENDIF
		BREAK
		CASE PA_AMBUSH
			IF bIsAvailable FMMC_SET_LONG_BIT(MPGlobalsAmbience.iPlayerAbilitiesBitset, ciPlayerAbilitiesBS_AmbushAvailable)
			ELSE FMMC_CLEAR_LONG_BIT(MPGlobalsAmbience.iPlayerAbilitiesBitset, ciPlayerAbilitiesBS_AmbushAvailable) ENDIF
		BREAK
		CASE PA_DISTRACTION
			IF bIsAvailable FMMC_SET_LONG_BIT(MPGlobalsAmbience.iPlayerAbilitiesBitset, ciPlayerAbilitiesBS_DistractionAvailable)
			ELSE FMMC_CLEAR_LONG_BIT(MPGlobalsAmbience.iPlayerAbilitiesBitset, ciPlayerAbilitiesBS_DistractionAvailable) ENDIF
		BREAK
	ENDSWITCH

ENDPROC

PROC SET_LOCAL_PLAYER_ABILITY_ON_COOLDOWN(FMMC_PLAYER_ABILITY_TYPES eType, BOOL bIsOnCooldown)
	SWITCH eType
		CASE PA_AIR_STRIKE
			IF bIsOnCooldown FMMC_SET_LONG_BIT(MPGlobalsAmbience.iPlayerAbilitiesBitset, ciPlayerAbilitiesBS_AirStrikeOnCooldown)
			ELSE FMMC_CLEAR_LONG_BIT(MPGlobalsAmbience.iPlayerAbilitiesBitset, ciPlayerAbilitiesBS_AirStrikeOnCooldown) ENDIF
		BREAK
		CASE PA_HEAVY_LOADOUT
			IF bIsOnCooldown FMMC_SET_LONG_BIT(MPGlobalsAmbience.iPlayerAbilitiesBitset, ciPlayerAbilitiesBS_HeavyLoadoutOnCooldown)
			ELSE FMMC_CLEAR_LONG_BIT(MPGlobalsAmbience.iPlayerAbilitiesBitset, ciPlayerAbilitiesBS_HeavyLoadoutOnCooldown) ENDIF
		BREAK	
		CASE PA_HELI_BACKUP
			IF bIsOnCooldown FMMC_SET_LONG_BIT(MPGlobalsAmbience.iPlayerAbilitiesBitset, ciPlayerAbilitiesBS_HeliBackupOnCooldown)
			ELSE FMMC_CLEAR_LONG_BIT(MPGlobalsAmbience.iPlayerAbilitiesBitset, ciPlayerAbilitiesBS_HeliBackupOnCooldown) ENDIF
		BREAK
		CASE PA_RECON_DRONE
			IF bIsOnCooldown FMMC_SET_LONG_BIT(MPGlobalsAmbience.iPlayerAbilitiesBitset, ciPlayerAbilitiesBS_ReconDroneOnCooldown)
			ELSE FMMC_CLEAR_LONG_BIT(MPGlobalsAmbience.iPlayerAbilitiesBitset, ciPlayerAbilitiesBS_ReconDroneOnCooldown) ENDIF
		BREAK
		CASE PA_SUPPORT_SNIPER
			IF bIsOnCooldown FMMC_SET_LONG_BIT(MPGlobalsAmbience.iPlayerAbilitiesBitset, ciPlayerAbilitiesBS_SupportSniperOnCooldown)
			ELSE FMMC_CLEAR_LONG_BIT(MPGlobalsAmbience.iPlayerAbilitiesBitset, ciPlayerAbilitiesBS_SupportSniperOnCooldown) ENDIF
		BREAK
		CASE PA_VEHICLE_REPAIR
			IF bIsOnCooldown FMMC_SET_LONG_BIT(MPGlobalsAmbience.iPlayerAbilitiesBitset, ciPlayerAbilitiesBS_VehicleRepairOnCooldown)
			ELSE FMMC_CLEAR_LONG_BIT(MPGlobalsAmbience.iPlayerAbilitiesBitset, ciPlayerAbilitiesBS_VehicleRepairOnCooldown) ENDIF
		BREAK
		CASE PA_RIDE_ALONG
			IF bIsOnCooldown FMMC_SET_LONG_BIT(MPGlobalsAmbience.iPlayerAbilitiesBitset, ciPlayerAbilitiesBS_RideAlongOnCooldown)
			ELSE FMMC_CLEAR_LONG_BIT(MPGlobalsAmbience.iPlayerAbilitiesBitset, ciPlayerAbilitiesBS_RideAlongOnCooldown) ENDIF
		BREAK
		CASE PA_AMBUSH
			IF bIsOnCooldown FMMC_SET_LONG_BIT(MPGlobalsAmbience.iPlayerAbilitiesBitset, ciPlayerAbilitiesBS_AmbushOnCooldown)
			ELSE FMMC_CLEAR_LONG_BIT(MPGlobalsAmbience.iPlayerAbilitiesBitset, ciPlayerAbilitiesBS_AmbushOnCooldown) ENDIF
		BREAK
		CASE PA_DISTRACTION
			IF bIsOnCooldown FMMC_SET_LONG_BIT(MPGlobalsAmbience.iPlayerAbilitiesBitset, ciPlayerAbilitiesBS_DistractionOnCooldown)
			ELSE FMMC_CLEAR_LONG_BIT(MPGlobalsAmbience.iPlayerAbilitiesBitset, ciPlayerAbilitiesBS_DistractionOnCooldown) ENDIF
		BREAK
	ENDSWITCH
ENDPROC

// Disqualifiers to ability activation should be checked here
// Returns true if ability can be activated
FUNC BOOL CAN_LOCAL_PLAYER_ABILITY_BE_ACTIVATED(FMMC_PLAYER_ABILITY_TYPES eType)
	
	// ignore the check for prereqsd below if debug flag is set
	#IF IS_DEBUG_BUILD
		IF sLocalPlayerAbilities.bDebugIgnorePrereqs
			RETURN TRUE
		ENDIF
	#ENDIF
	
	SWITCH eType
		CASE PA_AIR_STRIKE
			IF gMC_PlayerBD_VARS.iAbilityPrerequisite_AirStrike != ciPREREQ_None
			AND NOT FMMC_IS_LONG_BIT_SET(gMC_PlayerBD_VARS.iPrerequisiteBS, gMC_PlayerBD_VARS.iAbilityPrerequisite_AirStrike)
				RETURN FALSE
			ENDIF
		BREAK
		
		CASE PA_HEAVY_LOADOUT
			IF gMC_PlayerBD_VARS.iAbilityPrerequisite_HeavyLoadout != ciPREREQ_None
			AND NOT FMMC_IS_LONG_BIT_SET(gMC_PlayerBD_VARS.iPrerequisiteBS, gMC_PlayerBD_VARS.iAbilityPrerequisite_HeavyLoadout)
				RETURN FALSE
			ENDIF
		BREAK
		
		CASE PA_HELI_BACKUP
			IF gMC_PlayerBD_VARS.iAbilityPrerequisite_HeliBackup != ciPREREQ_None
			AND NOT FMMC_IS_LONG_BIT_SET(gMC_PlayerBD_VARS.iPrerequisiteBS, gMC_PlayerBD_VARS.iAbilityPrerequisite_HeliBackup)
				RETURN FALSE
			ENDIF
		BREAK
		
		CASE PA_RECON_DRONE
			IF gMC_PlayerBD_VARS.iAbilityPrerequisite_ReconDrone != ciPREREQ_None
			AND NOT FMMC_IS_LONG_BIT_SET(gMC_PlayerBD_VARS.iPrerequisiteBS, gMC_PlayerBD_VARS.iAbilityPrerequisite_ReconDrone)
				RETURN FALSE
			ENDIF
		BREAK
		
		CASE PA_SUPPORT_SNIPER
			IF gMC_PlayerBD_VARS.iAbilityPrerequisite_SupportSniper != ciPREREQ_None
			AND NOT FMMC_IS_LONG_BIT_SET(gMC_PlayerBD_VARS.iPrerequisiteBS, gMC_PlayerBD_VARS.iAbilityPrerequisite_SupportSniper)
				RETURN FALSE
			ENDIF
		BREAK
		
		CASE PA_VEHICLE_REPAIR
			
		BREAK
		
		CASE PA_RIDE_ALONG
			
		BREAK
		
		CASE PA_AMBUSH
			
		BREAK
		
		CASE PA_DISTRACTION
			
		BREAK
			
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC INT GET_INVALID_ACTIVATION_BIT_INDEX_FOR_ABILITY(FMMC_PLAYER_ABILITY_TYPES eAbilityType)
	
	SWITCH eAbilityType
		CASE PA_AIR_STRIKE
			RETURN ciPlayerAbilitiesBS_AirstrikeAttemptedInvalidActivation
		CASE PA_HELI_BACKUP
			RETURN ciPlayerAbilitiesBS_HeliBackupAttemptedInvalidActivation
		CASE PA_HEAVY_LOADOUT
			RETURN ciPlayerAbilitiesBS_HeavyLoadoutAttemptedInvalidActivation
		CASE PA_RECON_DRONE
			RETURN ciPlayerAbilitiesBS_ReconDroneAttemptedInvalidActivation
		CASE PA_SUPPORT_SNIPER		
			RETURN ciPlayerAbilitiesBS_SupportSniperAttemptedInvalidActivation
	ENDSWITCH
	
	RETURN -1
ENDFUNC

PROC SET_HELI_BACKUP_HAS_BEEN_ACTIVE()
	IF IS_BIT_SET(iLocalBoolCheck34, LBOOL34_PLAYER_ABILITY_HELI_BACKUP_HAS_BEEN_ACTIVE)
		EXIT
	ENDIF	
	IF IS_PLAYER_ABILITY_ACTIVE(PA_HELI_BACKUP)
	AND HEIST_ISLAND_BACKUP_HELI__GET_HELI_HEALTH() > 0
		SET_BIT(iLocalBoolCheck34, LBOOL34_PLAYER_ABILITY_HELI_BACKUP_HAS_BEEN_ACTIVE)
	ENDIF
ENDPROC

PROC PROCESS_ABILITY_SPEND(FMMC_PLAYER_ABILITY_TYPES eType)

	INT iCost = GET_PLAYER_ABILITY_COST(eType)
	INT iAbility = ENUM_TO_INT(eType)
	
	PRINTLN("[PlayerAbilities] PROCESS_ABILITY_SPEND | Spending ", iCost)
	
	IF USE_SERVER_TRANSACTIONS()
		INT iTransactionID
		TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_SPEND_INTERACTION_MENU_ABILITY, iCost, iTransactionID, FALSE, TRUE)
	 	g_cashTransactionData[iTransactionID].eEssentialData.iExtraItemID = iAbility
	ELSE
		NETWORK_SPEND_INTERACTION_MENU_ABILITY(iCost, FALSE, TRUE, iAbility)
	ENDIF
ENDPROC

// Attempts to activate the player ability, returns true if activation was successful
FUNC BOOL ATTEMPT_LOCAL_PLAYER_ABILITY_ACTIVATION(FMMC_PLAYER_ABILITY_TYPES eType)
	PRINTLN("[PlayerAbilities] Ability Activated: ", eType)
	
	// clear the activation request
	CLEAR_LOCAL_PLAYER_ABILITY_ACTIVATION_PENDING(eType)
	
	// Ensure the heli has spawned for some systems like Dialogue Triggers, in case it fails after the player has activated it.
	SET_HELI_BACKUP_HAS_BEEN_ACTIVE()
	
	// Early out if abiity cannot be activated
	IF NOT CAN_LOCAL_PLAYER_ABILITY_BE_ACTIVATED(eType)
		RETURN FALSE
	ENDIF
	
	#IF FEATURE_HEIST_ISLAND
	SET_ISLAND_HEIST_TELEMETRY_SUPPORT_CREW_USED(GET_ISLAND_HEIST_TELEMETRY_INDEX_FROM_ABILITY_TYPE(eType))
	#ENDIF
	
	SET_LOCAL_PLAYER_ABILITY_AVAILABILITY(eType, FALSE)
	SET_LOCAL_PLAYER_ABILITY_ON_COOLDOWN(eType, FALSE)
	PROCESS_ABILITY_SPEND(eType)
	
	BROADCAST_FMMC_PLAYER_ABILITY_ACTIVATED(localPlayer)
	
	sLocalPlayerAbilities.sAbilities[ENUM_TO_INT(eType)].iUseCount++
	IF ABILITY_HAS_ACTIVE_DURATION(eType)
		IF NOT ABILITY_USES_DELAYED_TIMER(eType)
			START_NET_TIMER(sLocalPlayerAbilities.sAbilities[ENUM_TO_INT(eType)].tDurationTimer)
		ELSE
			PRINTLN("[PlayerAbilities] Ability ", eType, " starts its duration timer later!")
		ENDIF
	ENDIF
	
	SWITCH eType
		CASE PA_SUPPORT_SNIPER
			ON_SUPPORT_SNIPER_ABILITY_ACTIVATED()
		BREAK
		CASE PA_HEAVY_LOADOUT
			ON_HEAVY_LOADOUT_ABILITY_ACTIVATED()
		BREAK
		CASE PA_HELI_BACKUP
			ON_HELI_BACKUP_ABILITY_ACTIVATED()
		BREAK
		CASE PA_AIR_STRIKE
			ON_AIR_STRIKE_ACTIVATED()			
		BREAK
		CASE PA_VEHICLE_REPAIR
			ON_VEHICLE_REPAIR_ACTIVATED()			
		BREAK
		CASE PA_RIDE_ALONG
			ON_RIDE_ALONG_ACTIVATED()			
		BREAK
		CASE PA_AMBUSH
			ON_AMBUSH_ACTIVATED()			
		BREAK
		CASE PA_DISTRACTION
			ON_DISTRACTION_ACTIVATED()			
		BREAK
		DEFAULT BREAK
	ENDSWITCH
	
	RETURN TRUE
		
ENDFUNC

PROC ON_LOCAL_PLAYER_ABILIITY_COOLDOWN(FMMC_PLAYER_ABILITY_TYPES eType)
	PRINTLN("[PlayerAbilities] Ability Cooldown: ", eType)
	IF ABILITY_HAS_COOLDOWN_DURATION(eType)
		SET_LOCAL_PLAYER_ABILITY_ON_COOLDOWN(eType, TRUE)
		START_NET_TIMER(sLocalPlayerAbilities.sAbilities[ENUM_TO_INT(eType)].tDurationTimer)
	ENDIF
ENDPROC

PROC ON_LOCAL_PLAYER_ABILITY_UNAVAILABLE(FMMC_PLAYER_ABILITY_TYPES eType)
	PRINTLN("[PlayerAbilities] Ability Unavailable: ", eType)	

	SET_LOCAL_PLAYER_ABILITY_AVAILABILITY(eType, FALSE)
	SET_LOCAL_PLAYER_ABILITY_ON_COOLDOWN(eType, FALSE)
	
	SWITCH eType
		CASE PA_HELI_BACKUP
			CHANGE_HELI_BACKUP_STATE(HB_INACTIVE)
		BREAK
		CASE PA_SUPPORT_SNIPER
			CHANGE_SUPPORT_SNIPER_STATE(SS_INACTIVE)
		BREAK
		DEFAULT BREAK
	ENDSWITCH
ENDPROC

PROC ON_LOCAL_PLAYER_ABILITY_READY(FMMC_PLAYER_ABILITY_TYPES eType)
	PRINTLN("[PlayerAbilities] Ability Ready: ", eType)
	SET_LOCAL_PLAYER_ABILITY_AVAILABILITY(eType, TRUE)
	SET_LOCAL_PLAYER_ABILITY_ON_COOLDOWN(eType, FALSE)
ENDPROC

PROC HANDLE_FAILED_ABILITY_ACTIVATION(FMMC_PLAYER_ABILITY_TYPES eType)
	PRINTLN("[PlayerAbilities] HANDLE_FAILED_ABILITY_ACTIVATION")
	INT iInvalidActivationIndex 
	iInvalidActivationIndex  = GET_INVALID_ACTIVATION_BIT_INDEX_FOR_ABILITY(eType)				
	FMMC_SET_LONG_BIT(MPGlobalsAmbience.iPlayerAbilitiesBitset, iInvalidActivationIndex)
	
	// dialogue trigger set up
	SWITCH eType
		CASE PA_AIR_STRIKE
			FMMC_SET_LONG_BIT(MPGlobalsAmbience.iPlayerAbilitiesBitset, ciPlayerAbilitiesBS_PlayAirStrikeAttemptedInvalidActivationDialogue)
			SET_BIT(ciDialogueInstantLoopConditionBS, ciDialogueInstantLoopCondition_AirStrikeUnavailable)
		BREAK
		CASE PA_HELI_BACKUP
			FMMC_SET_LONG_BIT(MPGlobalsAmbience.iPlayerAbilitiesBitset, ciPlayerAbilitiesBS_PlayHeliBackupAttemptedInvalidActivationDialogue)
			SET_BIT(ciDialogueInstantLoopConditionBS, ciDialogueInstantLoopCondition_HeliBackupUnavailable)
		BREAK
		CASE PA_SUPPORT_SNIPER
			FMMC_SET_LONG_BIT(MPGlobalsAmbience.iPlayerAbilitiesBitset, ciPlayerAbilitiesBS_PlaySupportSniperAttemptedInvalidActivationDialogue)
			SET_BIT(ciDialogueInstantLoopConditionBS, ciDialogueInstantLoopCondition_SupportSniperUnavailable)
		BREAK
		CASE PA_HEAVY_LOADOUT
		CASE PA_RECON_DRONE
			FMMC_SET_LONG_BIT(MPGlobalsAmbience.iPlayerAbilitiesBitset, ciPlayerAbilitiesBS_PlayAttemptedInvalidActivationDialogue)		
			SET_BIT(ciDialogueInstantLoopConditionBS, ciDialogueInstantLoopCondition_InvalidPlayerAbilityActivated)
		BREAK	
	ENDSWITCH
	
ENDPROC

PROC CHANGE_LOCAL_PLAYER_ABILITY_STATE(FMMC_PLAYER_ABILITY_TYPES eType, PLAYER_ABILITY_STATE eNewState)
	PRINTLN("[PlayerAbilities] CHANGE_LOCAL_PLAYER_ABILITY_STATE: Type: ", eType, ", State: ", eNewState)
	
	RESET_NET_TIMER(sLocalPlayerAbilities.sAbilities[ENUM_TO_INT(eType)].tDurationTimer)
	
	SWITCH eNewState
		CASE PAS_ACTIVE
			IF NOT ATTEMPT_LOCAL_PLAYER_ABILITY_ACTIVATION(eType)
				HANDLE_FAILED_ABILITY_ACTIVATION(eType)
				PRINTLN("[PlayerAbilities] CHANGE_LOCAL_PLAYER_ABILITY_STATE - state change to ACTIVE failed - exiting.")

				EXIT
			ENDIF
		BREAK
		CASE PAS_COOLDOWN
			ON_LOCAL_PLAYER_ABILIITY_COOLDOWN(eType)
		BREAK
		CASE PAS_READY
			ON_LOCAL_PLAYER_ABILITY_READY(eType)
		BREAK
		CASE PAS_UNAVAILABLE
			ON_LOCAL_PLAYER_ABILITY_UNAVAILABLE(eType)
		BREAK
	ENDSWITCH
	
	sLocalPlayerAbilities.sAbilities[ENUM_TO_INT(eType)].eState = eNewState
	MC_playerBD_1[iLocalPart].bPlayerAbilityIsActive[ENUM_TO_INT(eType)] = (sLocalPlayerAbilities.sAbilities[ENUM_TO_INT(eType)].eState = PAS_ACTIVE)	

ENDPROC

FUNC INT GET_RULE_BITSET_FOR_ABILITY_ENABLED_BY_TYPE(FMMC_PLAYER_ABILITY_TYPES eType)
	
	SWITCH eType
		CASE PA_AIR_STRIKE
		CASE PA_HEAVY_LOADOUT
		CASE PA_HELI_BACKUP
		CASE PA_RECON_DRONE
		CASE PA_SUPPORT_SNIPER
			RETURN g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iLocalPart].iteam].iRuleBitsetFourteen[GET_LOCAL_PLAYER_CURRENT_RULE()]
		BREAK
		
		CASE PA_VEHICLE_REPAIR
		CASE PA_RIDE_ALONG
		CASE PA_AMBUSH
		CASE PA_DISTRACTION
			RETURN g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iLocalPart].iteam].iRuleBitsetFifteen[GET_LOCAL_PLAYER_CURRENT_RULE()]
		BREAK
	ENDSWITCH
	
	RETURN 0
ENDFUNC

FUNC BOOL GET_RULE_BITINDEX_FOR_ABILITY_ENABLED_BY_TYPE(INT& iBitIndex, FMMC_PLAYER_ABILITY_TYPES eType)
	
	SWITCH eType
		CASE PA_AIR_STRIKE
			iBitIndex = ciBS_RULE14_PLAYER_ABILITY_ENABLE_AIRSTRIKE
			RETURN TRUE
		CASE PA_HEAVY_LOADOUT
			iBitIndex = ciBS_RULE14_PLAYER_ABILITY_ENABLE_HEAVY_LOADOUT
			RETURN TRUE
		CASE PA_HELI_BACKUP
			iBitIndex = ciBS_RULE14_PLAYER_ABILITY_ENABLE_HELI_BACKUP
				RETURN TRUE
		CASE PA_RECON_DRONE
			iBitIndex = ciBS_RULE14_PLAYER_ABILITY_ENABLE_RECON_DRONE
		RETURN TRUE
		CASE PA_SUPPORT_SNIPER
			iBitIndex = ciBS_RULE14_PLAYER_ABILITY_ENABLE_SUPPORT_SNIPER
			RETURN TRUE
		CASE PA_VEHICLE_REPAIR
			iBitIndex = ciBS_RULE15_PLAYER_ABILITY_VEHICLE_REPAIR
			RETURN TRUE
		CASE PA_RIDE_ALONG
			iBitIndex = ciBS_RULE15_PLAYER_ABILITY_RIDE_ALONG
			RETURN TRUE
		CASE PA_AMBUSH
			iBitIndex = ciBS_RULE15_PLAYER_ABILITY_AMBUSH
			RETURN TRUE
		CASE PA_DISTRACTION
			iBitIndex = ciBS_RULE15_PLAYER_ABILITY_DISTRACTION
			RETURN TRUE
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ABILITY_AVAILABLE_FOR_LOCAL_PLAYER(FMMC_PLAYER_ABILITY_TYPES eType)
	#IF IS_DEBUG_BUILD
		IF sLocalPlayerAbilities.sAbilities[ENUM_TO_INT(eType)].bDebugAvailable
			RETURN TRUE
		ENDIF
	#ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sPlayerAbilities.sAbilities[ENUM_TO_INT(eType)].iAbilityBitset, ciAbilityBitset_AbilityIsHeistLeaderOnly)
		RETURN GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()		
	ENDIF
	
	RETURN TRUE

ENDFUNC

FUNC BOOL IS_ABILITY_AVAILABLE_FOR_CURRENT_RULE(FMMC_PLAYER_ABILITY_TYPES eType)
	#IF IS_DEBUG_BUILD
		IF sLocalPlayerAbilities.sAbilities[ENUM_TO_INT(eType)].bDebugAvailable
			RETURN TRUE
		ENDIF
	#ENDIF
	
	INT iBitIndex = 0	
	IF GET_RULE_BITINDEX_FOR_ABILITY_ENABLED_BY_TYPE(iBitIndex, eType)
		RETURN IS_BIT_SET(GET_RULE_BITSET_FOR_ABILITY_ENABLED_BY_TYPE(eType), iBitIndex)
	ELSE 
		RETURN FALSE
	ENDIF
ENDFUNC

PROC PROCESS_LOCAL_PLAYER_ABILITY_ACTIVE_STATE(FMMC_PLAYER_ABILITY_TYPES eType, PLAYER_ABILITY_STATE& eState)	
	SWITCH eType
		CASE PA_SUPPORT_SNIPER
			PROCESS_LOCAL_PLAYER_SUPPORT_SNIPER_ABILITY_ACTIVE_STATE(eState)
		BREAK
		CASE PA_HEAVY_LOADOUT
			PROCESS_LOCAL_PLAYER_HEAVY_LOADOUT_ABILITY_ACTIVE_STATE(eState)
		BREAK
		CASE PA_HELI_BACKUP
			PROCESS_LOCAL_PLAYER_HELI_BACKUP_ABILITY_ACTIVE_STATE(eState)
		BREAK
		CASE PA_AIR_STRIKE
			PROCESS_LOCAL_PLAYER_AIR_STRIKE_ABILITY_ACTIVE_STATE(eState)
		BREAK
		CASE PA_VEHICLE_REPAIR
			PROCESS_LOCAL_PLAYER_VEHICLE_REPAIR_ACTIVE_STATE(eState)
		BREAK
		CASE PA_RIDE_ALONG
			PROCESS_LOCAL_PLAYER_RIDE_ALONG_ACTIVE_STATE(eState)
		BREAK
		CASE PA_AMBUSH
			PROCESS_LOCAL_PLAYER_AMBUSH_ACTIVE_STATE(eState)
		BREAK
		CASE PA_DISTRACTION
			PROCESS_LOCAL_PLAYER_DISTRACTION_ACTIVE_STATE(eState)
		BREAK
		DEFAULT
		BREAK
	 ENDSWITCH	 
ENDPROC

FUNC BOOL IS_LOCAL_PLAYER_ABILITY_PENDING_ACTIVATION(FMMC_PLAYER_ABILITY_TYPES eAbilityType)
	
	SWITCH eAbilityType
		CASE PA_AIR_STRIKE
			RETURN FMMC_IS_LONG_BIT_SET(MPGlobalsAmbience.iPlayerAbilitiesBitset, ciPlayerAbilitiesBS_AirStrikeActivationPending)
		CASE PA_HELI_BACKUP
			RETURN FMMC_IS_LONG_BIT_SET(MPGlobalsAmbience.iPlayerAbilitiesBitset, ciPlayerAbilitiesBS_HeliBackupActivationPending)
		CASE PA_HEAVY_LOADOUT
			RETURN FMMC_IS_LONG_BIT_SET(MPGlobalsAmbience.iPlayerAbilitiesBitset, ciPlayerAbilitiesBS_HeavyLoadoutActivationPending)
		CASE PA_RECON_DRONE
			RETURN FMMC_IS_LONG_BIT_SET(MPGlobalsAmbience.iPlayerAbilitiesBitset, ciPlayerAbilitiesBS_ReconDroneActivationPending)
		CASE PA_SUPPORT_SNIPER		
			RETURN FMMC_IS_LONG_BIT_SET(MPGlobalsAmbience.iPlayerAbilitiesBitset, ciPlayerAbilitiesBS_SupportSniperActivationPending)	
		CASE PA_VEHICLE_REPAIR	
			RETURN FMMC_IS_LONG_BIT_SET(MPGlobalsAmbience.iPlayerAbilitiesBitset, ciPlayerAbilitiesBS_VehicleRepairActivationPending)
		CASE PA_RIDE_ALONG
			RETURN FMMC_IS_LONG_BIT_SET(MPGlobalsAmbience.iPlayerAbilitiesBitset, ciPlayerAbilitiesBS_RideAlongActivationPending)	
		CASE PA_AMBUSH
			RETURN FMMC_IS_LONG_BIT_SET(MPGlobalsAmbience.iPlayerAbilitiesBitset, ciPlayerAbilitiesBS_AmbushActivationPending)	
		CASE PA_DISTRACTION
			RETURN FMMC_IS_LONG_BIT_SET(MPGlobalsAmbience.iPlayerAbilitiesBitset, ciPlayerAbilitiesBS_DistractionActivationPending)	
	ENDSWITCH
	
	RETURN FALSE

ENDFUNC

// returns false if finished
FUNC BOOL PROCESS_HEAVY_LOADOUT_CONTAINER_DEPLOYMENT__SCRIPT_HOST()
	PRINTLN("[PlayerAbilities][Heavy Loadout] PROCESS_HEAVY_LOADOUT_CONTAINER_DEPLOYMENT__SCRIPT_HOST")
	
	IF NOT bIsLocalPlayerHost
		PRINTLN("[PlayerAbilities][Heavy Loadout] PROCESS_HEAVY_LOADOUT_CONTAINER_DEPLOYMENT__SCRIPT_HOST - returning false as the local player is not the script host")
		RETURN FALSE
	ENDIF
	
	// force to spawn container as we are processing this for another player
	IF sLocalPlayerAbilities.sHeavyLoadoutData.eHLState = HL_INACTIVE
		PRINTLN("[PlayerAbilities][Heavy Loadout] PROCESS_HEAVY_LOADOUT_CONTAINER_DEPLOYMENT__SCRIPT_HOST - forcing state to set up container for another player")
		CHANGE_HEAVY_LOADOUT_STATE(HL_SET_UP_CONTAINER)		
	ENDIF
	
	// fake active state as we are processing this for another player
	PRINTLN("[PlayerAbilities][Heavy Loadout] PROCESS_HEAVY_LOADOUT_CONTAINER_DEPLOYMENT__SCRIPT_HOST - processing active state for another player")
	PLAYER_ABILITY_STATE eState = PAS_ACTIVE
	PROCESS_LOCAL_PLAYER_HEAVY_LOADOUT_ABILITY_ACTIVE_STATE(eState)
	
	// complete if the process ability check has not exited the fake active state
	IF eState != PAS_ACTIVE
		PRINTLN("[PlayerAbilities][Heavy Loadout] PROCESS_HEAVY_LOADOUT_CONTAINER_DEPLOYMENT__SCRIPT_HOST - returning false as the ability is no longer active")
		RETURN FALSE
	ENDIF
	
	// complete if the internal state has moved to spawn contents
	IF sLocalPlayerAbilities.sHeavyLoadoutData.eHLState = HL_SPAWN_CONTENTS
		PRINTLN("[PlayerAbilities][Heavy Loadout] PROCESS_HEAVY_LOADOUT_CONTAINER_DEPLOYMENT__SCRIPT_HOST - returning false as the script host has finished processing")
		RETURN FALSE
	ENDIF

	RETURN TRUE	
ENDFUNC

PROC PROCESS_LOCAL_PLAYER_ABILITY_STATE__SCRIPT_HOST()
	
	IF NOT bIsLocalPlayerHost
		EXIT
	ENDIF
	
	IF IS_BIT_SET(sLocalPlayerAbilities.sHeavyLoadoutData.iBS_HeavyLoadoutBitset, ciHeavyLoadoutBS_ProcessingContainerDeployment)
		PRINTLN("[PlayerAbilities][Heavy Loadout] PROCESS_LOCAL_PLAYER_ABILITY_STATE__SCRIPT_HOST - processing heavy loadout container deployment")
		IF NOT PROCESS_HEAVY_LOADOUT_CONTAINER_DEPLOYMENT__SCRIPT_HOST()
			PRINTLN("[PlayerAbilities][Heavy Loadout] PROCESS_LOCAL_PLAYER_ABILITY_STATE__SCRIPT_HOST - finished processing container deployment")
			// Broadcast finished event
			BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_PlayerAbilities_FinishedDeploymentHeavyLoadoutContainer)
			CLEAR_BIT(sLocalPlayerAbilities.sHeavyLoadoutData.iBS_HeavyLoadoutBitset, ciHeavyLoadoutBS_ProcessingContainerDeployment)
			
			// If I don't have the ability active
			IF sLocalPlayerAbilities.sAbilities[PA_HEAVY_LOADOUT].eState != PAS_ACTIVE
				// ensure the heavy loadout is marked inactive
				CHANGE_HEAVY_LOADOUT_STATE(HL_INACTIVE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

// Processing function called every frame to handle ability state
PROC PROCESS_LOCAL_PLAYER_ABILITY_STATE()
	PLAYER_ABILITY_STATE eNewState
	INT i = 0
	REPEAT PA_MAX i
		
		eNewState = sLocalPlayerAbilities.sAbilities[i].eState
				
		IF IS_RULE_INDEX_VALID(GET_LOCAL_PLAYER_CURRENT_RULE(DEFAULT, FALSE)) 
		AND DOES_LOCAL_PLAYER_HAVE_NEW_PRIORITY_THIS_FRAME()
			PRINTLN("[PlayerAbilities] New rule priority")
			// if ability is avilable for this rule
			IF IS_ABILITY_AVAILABLE_FOR_CURRENT_RULE(INT_TO_ENUM(FMMC_PLAYER_ABILITY_TYPES, i))
			AND IS_ABILITY_AVAILABLE_FOR_LOCAL_PLAYER(INT_TO_ENUM(FMMC_PLAYER_ABILITY_TYPES, i))
				PRINTLN("[PlayerAbilities] Ability ", i, " is available this rule")
				// if ability state was unavailable
				IF (sLocalPlayerAbilities.sAbilities[i].eState = PAS_UNAVAILABLE)
					// new state is ready
					eNewState = PAS_READY
				ENDIF
			ELSE // ability is not available for this rule
				PRINTLN("[PlayerAbilities] Ability ", i, " is not available this rule")
				eNewState = PAS_UNAVAILABLE
			ENDIF
		ENDIF		
		
		IF eNewState = sLocalPlayerAbilities.sAbilities[i].eState
			SWITCH sLocalPlayerAbilities.sAbilities[i].eState			
			CASE PAS_ACTIVE
				// if there is no active duration, should remain in this state indefinitely
				IF ABILITY_HAS_ACTIVE_DURATION(INT_TO_ENUM(FMMC_PLAYER_ABILITY_TYPES, i))
					// if active duration has expired
					IF HAS_NET_TIMER_STARTED(sLocalPlayerAbilities.sAbilities[i].tDurationTimer)
					AND HAS_NET_TIMER_EXPIRED(sLocalPlayerAbilities.sAbilities[i].tDurationTimer, g_FMMC_STRUCT.sPlayerAbilities.sAbilities[i].iActiveDuration * 1000)
						// if a cooldown has been set, go to cooldown, otherwise go straight to ready
						IF ABILITY_HAS_COOLDOWN_DURATION(INT_TO_ENUM(FMMC_PLAYER_ABILITY_TYPES, i))
							eNewState = PAS_COOLDOWN
						ELSE
							eNewState = PAS_READY
						ENDIF											
					ENDIF
				ENDIF
				
				// handle active state behaviour for this frame
				PROCESS_LOCAL_PLAYER_ABILITY_ACTIVE_STATE(INT_TO_ENUM(FMMC_PLAYER_ABILITY_TYPES, i), eNewState)
			BREAK
			CASE PAS_COOLDOWN
				IF NOT ABILITY_HAS_COOLDOWN_DURATION(INT_TO_ENUM(FMMC_PLAYER_ABILITY_TYPES, i))
				OR HAS_NET_TIMER_EXPIRED(sLocalPlayerAbilities.sAbilities[i].tDurationTimer, g_FMMC_STRUCT.sPlayerAbilities.sAbilities[i].iCooldownDuration * 1000)
					eNewState = PAS_READY
				ENDIF
			BREAK
			CASE PAS_READY
				#IF IS_DEBUG_BUILD
					IF sLocalPlayerAbilities.sAbilities[i].bDebugActivate
						eNewState = PAS_ACTIVE
					ENDIF
				#ENDIF
				IF IS_LOCAL_PLAYER_ABILITY_PENDING_ACTIVATION(INT_TO_ENUM(FMMC_PLAYER_ABILITY_TYPES, i))
					eNewState = PAS_ACTIVE
				ENDIF
			BREAK
			CASE PAS_UNAVAILABLE
				#IF IS_DEBUG_BUILD
					IF sLocalPlayerAbilities.sAbilities[i].bDebugAvailable
						eNewState = PAS_READY
					ENDIF
				#ENDIF
			BREAK			
			ENDSWITCH
		ENDIF
				
		// if state is about to change
		IF eNewState != sLocalPlayerAbilities.sAbilities[i].eState
			
			// enforce use limit if set
			// (transition to unavailable if no uses remaining, but allow cooldown to complete)
			IF SHOULD_ENFORCE_ABILITY_USE_LIMIT(INT_TO_ENUM(FMMC_PLAYER_ABILITY_TYPES, i))
			AND GET_NUM_REMAINING_ABILITY_USES(INT_TO_ENUM(FMMC_PLAYER_ABILITY_TYPES, i)) <= 0
			AND eNewState != PAS_COOLDOWN
				PRINTLN("[PlayerAbilities] Reached permitted number of uses, ability is no longer available.")
				eNewState = PAS_UNAVAILABLE
			ENDIF			
						
			// change state
			CHANGE_LOCAL_PLAYER_ABILITY_STATE(INT_TO_ENUM(FMMC_PLAYER_ABILITY_TYPES, i), eNewState)
		ENDIF
		
	ENDREPEAT
ENDPROC

PROC INIT_LOCAL_PLAYER_ABILITIES()
	
	// update from continuity if tracking abilities
	IF IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_PlayerAbilityTracking)
		
		// update number of uses for all abilities
		INT i
		FOR i = 0 TO ciNUM_PLAYER_ABILITIES - 1
			sLocalPlayerAbilities.sAbilities[i].iUseCount = sMissionLocalContinuityVars.sPlayerAbilities.sPAContinuity[i].iUseCount	
		ENDFOR
		
		// activate recon drone if it was active in the previous mission
		IF sMissionLocalContinuityVars.sPlayerAbilities.sPAContinuity[ENUM_TO_INT(PA_RECON_DRONE)].bIsActive
			CHANGE_LOCAL_PLAYER_ABILITY_STATE(PA_RECON_DRONE, PAS_ACTIVE)
		ENDIF
		
	ENDIF
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Player Flare Attachment
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL CAN_ATTACH_FLARE_TO_PLAYER()
	
	INT iTeam = MC_playerBD[iLocalPart].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	IF IS_PED_INJURED(localPlayerPed)
	OR (IS_PLAYER_RESPAWNING(localPlayer) AND (NOT IS_FAKE_MULTIPLAYER_MODE_SET() OR IS_FAKE_MULTIPLAYER_MODE_ALLOWING_RESPAWNS()))
		RETURN FALSE
	ENDIF
	
	IF iRule >= FMMC_MAX_RULES
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFourteen[iRule], ciBS_RULE14_ATTACH_FLARE_TO_BACK)
		RETURN TRUE
	ENDIF
	
	IF IS_CUTSCENE_PLAYING()
		PRINTLN("[AttachedFlare] - CAN_ATTACH_FLARE_TO_PLAYER - IS_CUTSCENE_PLAYING - RETURNING FALSE")
		RETURN FALSE
	ENDIF

	IF NOT IS_ENTITY_DEAD(LocalPlayerPed)
		IF (IS_ENTITY_IN_WATER(LocalPlayerPed) OR IS_PED_SWIMMING(LocalPlayerPed))
		AND IS_MP_HEIST_GEAR_EQUIPPED(LocalPlayerPed, HEIST_GEAR_REBREATHER)
			PRINTLN("[AttachedFlare] - CAN_ATTACH_FLARE_TO_PLAYER - IS_CUTSCENE_PLAYING - RETURNING TRUE")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL HAS_FLARE_ATTACHMENT_ASSETS_LOADED()
	REQUEST_NAMED_PTFX_ASSET(GET_FLARE_ATTACHMENT_PTFX_ASSET_NAME())
	IF NOT HAS_NAMED_PTFX_ASSET_LOADED(GET_FLARE_ATTACHMENT_PTFX_ASSET_NAME())
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC PROCESS_FLARE_ATTACHMENT_FOR_PLAYER()
	
	IF NOT CAN_ATTACH_FLARE_TO_PLAYER()
		UNLOAD_FLARE_ATTACHMENT_ASSETS()
		EXIT
	ENDIF
	
	IF NOT HAS_FLARE_ATTACHMENT_ASSETS_LOADED()
		EXIT
	ENDIF
			
	IF NOT DOES_ENTITY_EXIST(objFlare)
		IF NOT IS_BIT_SET(iLocalBoolCheck12, LBOOL12_OBJ_RES_UNDERWATER_FLARE)
			MC_RESERVE_NETWORK_MISSION_OBJECTS(1)
			SET_BIT(iLocalBoolCheck12, LBOOL12_OBJ_RES_UNDERWATER_FLARE)
		ENDIF
		
		IF IS_BIT_SET(iLocalBoolCheck12,LBOOL12_OBJ_RES_UNDERWATER_FLARE)
			IF CAN_REGISTER_MISSION_OBJECTS(1)
				objFlare = CREATE_OBJECT(PROP_FLARE_01B, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(LocalPlayerPed, <<0.0, 0.0, 1.0>> ), TRUE, bIsLocalPlayerHost )
				SET_NETWORK_ID_EXISTS_ON_ALL_MACHINES(OBJ_TO_NET(objFlare), TRUE)
				ATTACH_ENTITY_TO_ENTITY(objFlare, LocalPlayerPed, GET_PED_BONE_INDEX(LocalPlayerPed, BONETAG_SPINE3), GET_FLARE_ATTACHMENT_OFFSET_FOR_PLAYER(), <<0.0, 0.00, 345.00>>)
				SET_BIT(iLocalBoolCheck10, LBOOL10_HUMANE_FLARE_CREATED)
				CLEAR_BIT(iLocalBoolCheck12,LBOOL12_OBJ_RES_UNDERWATER_FLARE)
				
				IF DOES_PARTICLE_FX_LOOPED_EXIST(players_underwater_flare)
					PRINTLN("[AttachedFlare] - PROCESS_FLARE_ATTACHMENT_FOR_PLAYER | Removing existing invalid flare PTFX so that we can recreate it on the fresh flare object")
					REMOVE_PARTICLE_FX(players_underwater_flare, FALSE)
					RESET_NET_TIMER(stFlarePTFXDelayTimer)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
			
	IF NOT (IS_ENTITY_IN_WATER(LocalPlayerPed) OR IS_PED_SWIMMING(LocalPlayerPed))
		IF HAS_NET_TIMER_STARTED(stFlarePTFXDelayTimer)
			IF NOT HAS_NET_TIMER_EXPIRED(stFlarePTFXDelayTimer, ciFlarePTFXDelayTimerLength)
				PRINTLN("[AttachedFlare] - PROCESS_FLARE_ATTACHMENT_FOR_PLAYER | Waiting for stFlarePTFXDelayTimer")
				EXIT
			ENDIF
		ELSE
			PRINTLN("[AttachedFlare] - PROCESS_FLARE_ATTACHMENT_FOR_PLAYER | Starting stFlarePTFXDelayTimer")
			REINIT_NET_TIMER(stFlarePTFXDelayTimer)
			EXIT
		ENDIF
	ENDIF
	
	// Create PTFX for that
	IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(players_underwater_flare)
	AND DOES_ENTITY_EXIST(objFlare)
	AND DOES_ENTITY_HAVE_DRAWABLE(objFlare)
		PRINTLN("[AttachedFlare] - PROCESS_FLARE_ATTACHMENT_FOR_PLAYER USE_PARTICLE_FX_ASSET - 1 ")
		USE_PARTICLE_FX_ASSET(GET_FLARE_ATTACHMENT_PTFX_ASSET_NAME())
		
		PRINTLN("[AttachedFlare] - PROCESS_FLARE_ATTACHMENT_FOR_PLAYER - Starting Flare FX now!")
		players_underwater_flare =  START_NETWORKED_PARTICLE_FX_LOOPED_ON_ENTITY( "scr_heist_biolab_flare_underwater", objFlare, <<0.11, 0.00, 0.0>>, <<0.0, 0.0, 0.0>> ) 
	ENDIF

ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Remote Explosive Vehicle
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL IS_REMOTE_EXPLOSIVE_VEHICLE_CONTACT_AVAILABLE_THIS_RULE()
	
	IF NOT IS_RULE_INDEX_VALID(GET_LOCAL_PLAYER_CURRENT_RULE())
		RETURN FALSE
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[GET_LOCAL_PLAYER_TEAM()].iRuleBitsetFifteen[GET_LOCAL_PLAYER_CURRENT_RULE()], ciBS_RULE15_ALLOW_REMOTE_EXPLOSIVE_VEHICLES)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL ARE_ANY_VEHICLES_ALIVE_WITH_REMOTE_DETONATION()
	INT iVeh
	FOR iVeh = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles-1
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetNine, ciFMMC_VEHICLE9_RemoteExplosive)
			IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
				VEHICLE_INDEX viVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
				IF NOT IS_VEHICLE_FUCKED_MP(viVeh)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	PRINTLN("ARE_ANY_VEHICLES_ALIVE_WITH_REMOTE_DETONATION - No valid vehicles!!")
	RETURN FALSE
ENDFUNC

PROC REMOVE_DETONATE_BOMB_CONTACT_FOR_REMOTE_EXPLOSIVE(BOOL bSkipCallCheck = FALSE)
	IF IS_CONTACT_IN_PHONEBOOK(CHAR_MP_DETONATEPHONE, MULTIPLAYER_BOOK)
	AND (bSkipCallCheck OR NOT IS_CALLING_CONTACT(CHAR_MP_DETONATEPHONE))
	AND eDetonateOrigin = DETONATE_BOMB_VEHICLE_REMOTE
		REMOVE_DETONATE_BOMB_CONTACT()
		PRINTLN("[RemoteExplosive] REMOVE_DETONATE_BOMB_CONTACT_FOR_REMOTE_EXPLOSIVE - Removing CHAR_MP_DETONATEPHONE!")
	ENDIF
ENDPROC

PROC PROCESS_LOCAL_PLAYER_REMOTE_EXPLOSIVE_VEHICLE()
	
	IF NOT IS_BIT_SET(iLocalBoolCheck29, LBOOL29_REMOTE_DETONATE_VEHICLE_IN_THIS_MISSION)
		EXIT
	ENDIF
	
	IF NOT IS_LOCAL_PLAYER_ABLE_TO_DO_LEADER_RESTRICTED_INTERACTION()
		EXIT
	ENDIF
	
	IF iRemoteVehicleExplosionInitiator = -1
	AND IS_REMOTE_EXPLOSIVE_VEHICLE_CONTACT_AVAILABLE_THIS_RULE()
		IF NOT IS_BIT_SET(iLocalBoolCheck29, LBOOL29_REMOTE_DETONATE_VEHICLE_CHECKED_THIS_RULE)
			IF ARE_ANY_VEHICLES_ALIVE_WITH_REMOTE_DETONATION()
				IF NOT IS_CONTACT_IN_PHONEBOOK(CHAR_MP_DETONATEPHONE, MULTIPLAYER_BOOK)
					ADD_DETONATE_BOMB_CONTACT(DETONATE_BOMB_VEHICLE_REMOTE)
					PRINTLN("[RemoteExplosive] PROCESS_LOCAL_PLAYER_REMOTE_EXPLOSIVE_VEHICLE - Adding CHAR_MP_DETONATEPHONE!")
				ENDIF
			ELSE
				REMOVE_DETONATE_BOMB_CONTACT_FOR_REMOTE_EXPLOSIVE()
			ENDIF
			SET_BIT(iLocalBoolCheck29, LBOOL29_REMOTE_DETONATE_VEHICLE_CHECKED_THIS_RULE)
		ENDIF
	ELSE
		REMOVE_DETONATE_BOMB_CONTACT_FOR_REMOTE_EXPLOSIVE()
		CLEAR_BIT(iLocalBoolCheck29, LBOOL29_REMOTE_DETONATE_VEHICLE_CHECKED_THIS_RULE)
		
		EXIT
	ENDIF
	
	IF IS_CALLING_CONTACT(CHAR_MP_DETONATEPHONE)
	AND iRemoteVehicleExplosionInitiator = -1
		
		BROADCAST_FMMC_REMOTE_VEHICLE_EXPLOSION(iLocalPart)
		
		REMOVE_DETONATE_BOMB_CONTACT_FOR_REMOTE_EXPLOSIVE(TRUE)
		PRINTLN("[RemoteExplosive] PROCESS_LOCAL_PLAYER_REMOTE_EXPLOSIVE_VEHICLE - Setting LBOOL35_EXPLOSIVE_VEHICLE_TRIGGERED")
	ENDIF
	
	
ENDPROC

//iEndAlpha - Alpha we want to set for the entity at the end of the flash fade process
PROC START_ENTITY_FLASH_FADE(NETWORK_INDEX &niEntRef, INT iEndAlpha = 0)
	BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_FlashFadeEntityStart, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, iEndAlpha, DEFAULT, niEntRef)
ENDPROC

PROC END_ENTITY_FLASH_FADE(INT &iEntIndex)
	BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_FlashFadeEntityEnd, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, iEntIndex)
ENDPROC

//Returns -1 if flash fade wasn't initiated
FUNC INT GET_ENTITY_FLASH_FADE_INDEX()
	RETURN MC_serverBD.sFlashFade.iFlashFadePartEntIndex[iLocalPart]
ENDFUNC

FUNC BOOL IS_FLASH_FADE_ARRAY_FULL()
	RETURN MC_serverBD.sFlashFade.iNumOfEntsUsingFlashFade >= MAX_FLASH_FADE_ENTITIES
ENDFUNC

FUNC BOOL IS_IT_SAFE_TO_DELETE_A_FLASH_FADE_ENTITY(INT &iFlashFadeEntIndex)
	RETURN MC_serverBD.sFlashFade.iPlayersDoingFlashFadeBS[iFlashFadeEntIndex] = 0
ENDFUNC

PROC CLEAR_ENTITY_FLASH_FADE(INT &iEntIndex)
	BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_FlashFadeEntityClear, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, iEntIndex)
ENDPROC

FUNC BOOL IS_CACHED_VEHICLES_FOR_CLEANUP_ARRAY_FULL()
	RETURN MC_playerBD_1[iLocalPart].iNumOfCachedVehiclesForCleanup = MAX_NUM_OF_CACHED_VEHICLES_FOR_CLEANUP
ENDFUNC

FUNC BOOL CACHE_VEHICLE_FOR_CLEANUP(NETWORK_INDEX &niVeh)
	IF NOT IS_CACHED_VEHICLES_FOR_CLEANUP_ARRAY_FULL()
		IF NOT IS_ENTITY_ALIVE(NET_TO_ENT(niVeh))
			PRINTLN("[CACHE_VEHICLE_FOR_CLEANUP] - Can't cache the vehicle as it's not alive!")
			RETURN FALSE
		ENDIF
		
		INT iNextFreeIndex = MC_playerBD_1[iLocalPart].iNumOfCachedVehiclesForCleanup
		MC_playerBD_1[iLocalPart].niCachedVehiclesForCleanup[iNextFreeIndex] = niVeh
		MC_playerBD_1[iLocalPart].iNumOfCachedVehiclesForCleanup++
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DELETE_CACHED_VEHICLE(INT &iIndex)
	IF NOT DOES_ENTITY_EXIST(NET_TO_ENT(MC_playerBD_1[iLocalPart].niCachedVehiclesForCleanup[iIndex]))
		RETURN FALSE
	ENDIF
	IF TAKE_CONTROL_OF_ENTITY(NET_TO_ENT(MC_playerBD_1[iLocalPart].niCachedVehiclesForCleanup[iIndex]))
		DELETE_NET_ID(MC_playerBD_1[iLocalPart].niCachedVehiclesForCleanup[iIndex])
		MC_playerBD_1[iLocalPart].niCachedVehiclesForCleanup[iIndex] = NULL
		MC_playerBD_1[iLocalPart].iNumOfCachedVehiclesForCleanup--
		
		//Sort the array (shift left)
		IF iIndex < MAX_NUM_OF_CACHED_VEHICLES_FOR_CLEANUP - 1
			INT i
			FOR i = iIndex + 1 TO MAX_NUM_OF_CACHED_VEHICLES_FOR_CLEANUP - 1
				IF MC_playerBD_1[iLocalPart].niCachedVehiclesForCleanup[i] != NULL
					MC_playerBD_1[iLocalPart].niCachedVehiclesForCleanup[i-1] = MC_playerBD_1[iLocalPart].niCachedVehiclesForCleanup[i]
					MC_playerBD_1[iLocalPart].niCachedVehiclesForCleanup[i] = NULL
				ENDIF
			ENDFOR
		ENDIF
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_CLEAR_CACHED_VEHICLES(BOOL bForce = FALSE)
	IF MC_playerBD_1[iLocalPart].iNumOfCachedVehiclesForCleanup = 0
		EXIT
	ENDIF

	INT i
	FOR i = 0 TO MC_playerBD_1[iLocalPart].iNumOfCachedVehiclesForCleanup - 1
		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_playerBD_1[iLocalPart].niCachedVehiclesForCleanup[i])
			RELOOP
		ENDIF
	
		NETWORK_INDEX niVeh = MC_playerBD_1[iLocalPart].niCachedVehiclesForCleanup[i]
		ENTITY_INDEX  eiVeh = NET_TO_VEH(niVeh)
		IF bForce
			DELETE_CACHED_VEHICLE(i)
		ELIF IS_ENTITY_ALIVE(eiVeh)
		AND NOT CAN_ANY_PLAYER_SEE_POINT(GET_ENTITY_COORDS(eiVeh))
			DELETE_CACHED_VEHICLE(i)
		ENDIF
	ENDFOR
ENDPROC

PROC CLEAR_FLAME_EFFECT_ON_SANCTUS()
	IF IS_BIT_SET(iLocalBoolCheck30, LBOOL30_FLAME_EFFECT_SPAWNED)
		IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxFlameEffect[ciFLAMEEFFECT_SACTUS_WHEEL])
			STOP_PARTICLE_FX_LOOPED(ptfxFlameEffect[ciFLAMEEFFECT_SACTUS_WHEEL])
		ENDIF
	
		IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxFlameEffect[ciFLAMEEFFECT_SACTUS_HEADLIGHT])
			STOP_PARTICLE_FX_LOOPED(ptfxFlameEffect[ciFLAMEEFFECT_SACTUS_HEADLIGHT])
		ENDIF
		
		CLEAR_BIT(iLocalBoolCheck30, LBOOL30_FLAME_EFFECT_SPAWNED)
		PRINTLN("CLEAR_FLAME_EFFECT_ON_SANCTUS - Clearing the flame effects")
	ENDIF
ENDPROC

PROC SET_FLAME_EFFECT_ON_SANCTUS(INT iFlameColour)
	IF NOT IS_BIT_SET(iLocalBoolCheck30, LBOOL30_FLAME_EFFECT_SPAWNED)
		ENTITY_INDEX eiVeh = NET_TO_ENT(MC_playerBD_1[iLocalPart].sCachedVehicleData.niCachedVehicle)
		STRING strPTFXAsset = "scr_sum2_hal"
		STRING strFlameFX
		INT iBone
		REQUEST_NAMED_PTFX_ASSET(strPTFXAsset)
		
		IF NOT HAS_NAMED_PTFX_ASSET_LOADED(strPTFXAsset)
			PRINTLN("SET_FLAME_EFFECT_ON_SANCTUS - Waiting on the PTFX asset to load!")
			EXIT
		ENDIF
		
			
		//Wheel
		IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(ptfxFlameEffect[ciFLAMEEFFECT_SACTUS_WHEEL])
			
			SWITCH iFlameColour
				CASE 1  strFlameFX = "scr_sum2_hal_bike_wheel_flame_blue"       BREAK
				CASE 2  strFlameFX = "scr_sum2_hal_bike_wheel_flame_green"      BREAK
				CASE 3  strFlameFX = "scr_sum2_hal_bike_wheel_flame_greyblack"  BREAK
				CASE 4  strFlameFX = "scr_sum2_hal_bike_wheel_flame_orange"	    BREAK
				DEFAULT strFlameFX = "scr_sum2_hal_bike_wheel_flame_green"      BREAK
			ENDSWITCH
			
			iBone = GET_ENTITY_BONE_INDEX_BY_NAME(eiVeh, "bikedisc_f")
			USE_PARTICLE_FX_ASSET(strPTFXAsset)
			ptfxFlameEffect[ciFLAMEEFFECT_SACTUS_WHEEL] = START_NETWORKED_PARTICLE_FX_LOOPED_ON_ENTITY_BONE(strFlameFX, eiVeh, <<0.0,0.0,0.0>>, <<0.0,0.0,0.0>>, iBone)
			PRINTLN("SET_FLAME_EFFECT_ON_SANCTUS - Starting ", strFlameFX," effect")
		ENDIF
		
		//Headlight
		IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(ptfxFlameEffect[ciFLAMEEFFECT_SACTUS_HEADLIGHT])
			strFlameFX = "scr_sum2_hal_bike_flames_blue"
			
			SWITCH iFlameColour
				CASE 1  strFlameFX = "scr_sum2_hal_bike_flames_blue"       BREAK
				CASE 2  strFlameFX = "scr_sum2_hal_bike_flames_green"      BREAK
				CASE 3  strFlameFX = "scr_sum2_hal_bike_flames_greyblack"  BREAK
				CASE 4  strFlameFX = "scr_sum2_hal_bike_flames_orange"	   BREAK
				DEFAULT strFlameFX = "scr_sum2_hal_bike_flames_green"      BREAK
			ENDSWITCH
			
			iBone  = GET_ENTITY_BONE_INDEX_BY_NAME(eiVeh, "headlight_l")
			USE_PARTICLE_FX_ASSET(strPTFXAsset)
			ptfxFlameEffect[ciFLAMEEFFECT_SACTUS_HEADLIGHT] = START_NETWORKED_PARTICLE_FX_LOOPED_ON_ENTITY_BONE(strFlameFX, eiVeh, <<-0.05,-0.05,0.0>>, <<0.0,0.0,0.0>>, iBone)
			PRINTLN("SET_FLAME_EFFECT_ON_SANCTUS - Starting ", strFlameFX, " effect")
		ENDIF
		
		IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxFlameEffect[ciFLAMEEFFECT_SACTUS_WHEEL])
		AND DOES_PARTICLE_FX_LOOPED_EXIST(ptfxFlameEffect[ciFLAMEEFFECT_SACTUS_HEADLIGHT])
			SET_BIT(iLocalBoolCheck30, LBOOL30_FLAME_EFFECT_SPAWNED)
		ENDIF
	ENDIF
	
ENDPROC

PROC SET_FLAME_EFFECT_ON_PLAYER(INT iFlameColour)
	STRING strFlameFX
	ENTITY_INDEX eiPlayer = LocalPlayerPed
	INT iBone  = GET_ENTITY_BONE_INDEX_BY_NAME(eiPlayer, "MH_Hair_Crown")
	
	STRING strPTFXAsset = "scr_sum2_hal"
	REQUEST_NAMED_PTFX_ASSET(strPTFXAsset)
	
	IF NOT HAS_NAMED_PTFX_ASSET_LOADED(strPTFXAsset)
		PRINTLN("SET_FLAME_EFFECT_ON_SANCTUS - Waiting on the PTFX asset to load!")
		EXIT
	ENDIF
	
	//Dead
	IF NOT bLocalPlayerOK AND eHalloweenModeRiderStateForVFX != eHalloweenModeRiderStateForVFX_Dead
		IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxFlameEffect[ciFLAMEEFFECT_HUNTER])
			STOP_PARTICLE_FX_LOOPED(ptfxFlameEffect[ciFLAMEEFFECT_HUNTER])
			EXIT
		ENDIF
		
		//Weapon
		IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxFlameEffect[ciFLAMEEFFECT_HUNTER_WEAPON])
			STOP_PARTICLE_FX_LOOPED(ptfxFlameEffect[ciFLAMEEFFECT_HUNTER_WEAPON])
			EXIT
		ENDIF
		
		SWITCH iFlameColour
			CASE 1  strFlameFX = "scr_sum2_hal_rider_death_blue"       BREAK
			CASE 2  strFlameFX = "scr_sum2_hal_rider_death_green"      BREAK
			CASE 3  strFlameFX = "scr_sum2_hal_rider_death_greyblack"  BREAK
			CASE 4  strFlameFX = "scr_sum2_hal_rider_death_orange"	   BREAK
			DEFAULT strFlameFX = "scr_sum2_hal_rider_death_green"      BREAK
		ENDSWITCH
		
		USE_PARTICLE_FX_ASSET(strPTFXAsset)
		
		IF START_NETWORKED_PARTICLE_FX_NON_LOOPED_ON_PED_BONE(strFlameFX, LocalPlayerPed, <<0.0,0.0,0.0>>, <<0.0,0.0,0.0>>, BONETAG_HEAD)
			eHalloweenModeRiderStateForVFX = eHalloweenModeRiderStateForVFX_Dead
			PRINTLN("SET_FLAME_EFFECT_ON_PLAYER - Playing ", strFlameFX, " FX")
		ENDIF
	//Weak
	ELIF eHalloweenModeRiderStateForVFX != eHalloweenModeRiderStateForVFX_Weak AND bLocalPlayerOK AND GET_ENTITY_HEALTH_PERCENTAGE(eiPlayer) < 40 
		IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxFlameEffect[ciFLAMEEFFECT_HUNTER])
			STOP_PARTICLE_FX_LOOPED(ptfxFlameEffect[ciFLAMEEFFECT_HUNTER])
			EXIT
		ENDIF

		SWITCH iFlameColour
			CASE 1  strFlameFX = "scr_sum2_hal_rider_weak_blue"       BREAK
			CASE 2  strFlameFX = "scr_sum2_hal_rider_weak_green"      BREAK
			CASE 3  strFlameFX = "scr_sum2_hal_rider_weak_greyblack"  BREAK
			CASE 4  strFlameFX = "scr_sum2_hal_rider_weak_orange"	  BREAK
			DEFAULT strFlameFX = "scr_sum2_hal_rider_weak_green"      BREAK
		ENDSWITCH
		
		USE_PARTICLE_FX_ASSET(strPTFXAsset)
		
		IF START_NETWORKED_PARTICLE_FX_NON_LOOPED_ON_PED_BONE(strFlameFX, LocalPlayerPed, <<0.0,0.0,0.0>>, <<0.0,0.0,0.0>>, BONETAG_HEAD)
			eHalloweenModeRiderStateForVFX = eHalloweenModeRiderStateForVFX_Weak
			PRINTLN("SET_FLAME_EFFECT_ON_PLAYER - Playing ", strFlameFX, " FX")
		ENDIF
	//Default
	ELIF eHalloweenModeRiderStateForVFX != eHalloweenModeRiderStateForVFX_Default AND bLocalPlayerOK AND GET_ENTITY_HEALTH_PERCENTAGE(eiPlayer) >= 40
		IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxFlameEffect[ciFLAMEEFFECT_HUNTER])
			STOP_PARTICLE_FX_LOOPED(ptfxFlameEffect[ciFLAMEEFFECT_HUNTER])
			EXIT
		ENDIF
		
		SWITCH iFlameColour
			CASE 1  strFlameFX = "scr_sum2_hal_rider_head_blue"       BREAK
			CASE 2  strFlameFX = "scr_sum2_hal_rider_head_green"      BREAK
			CASE 3  strFlameFX = "scr_sum2_hal_rider_head_greyblack"  BREAK
			CASE 4  strFlameFX = "scr_sum2_hal_rider_head_orange"	  BREAK
			DEFAULT strFlameFX = "scr_sum2_hal_rider_head_green"      BREAK
		ENDSWITCH
		
		USE_PARTICLE_FX_ASSET(strPTFXAsset)
		
		ptfxFlameEffect[ciFLAMEEFFECT_HUNTER] = START_NETWORKED_PARTICLE_FX_LOOPED_ON_ENTITY_BONE(strFlameFX, eiPlayer, <<0.1,0.0,0.0>>, <<0.0,0.0,0.0>>, iBone, 0.6)
		eHalloweenModeRiderStateForVFX = eHalloweenModeRiderStateForVFX_Default
		PRINTLN("SET_FLAME_EFFECT_ON_PLAYER - Playing ", strFlameFX, " FX")
	ENDIF
	
	eiPlayer = GET_CURRENT_PED_WEAPON_ENTITY_INDEX(LocalPlayerPed)
	IF NOT DOES_ENTITY_EXIST(eiPlayer)
	OR (NOT IS_PLAYER_FREE_AIMING(LocalPlayer) AND IS_PED_IN_VEHICLE(LocalPlayerPed, NET_TO_VEH(MC_playerBD_1[iLocalPart].sCachedVehicleData.niCachedVehicle)))
		IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxFlameEffect[ciFLAMEEFFECT_HUNTER_WEAPON])	
			STOP_PARTICLE_FX_LOOPED(ptfxFlameEffect[ciFLAMEEFFECT_HUNTER_WEAPON])
			wtWeaponWithFireEffectApplied = WEAPONTYPE_INVALID
			PRINTLN("SET_FLAME_EFFECT_ON_PLAYER - Remove The Effect From Player's Weapon")
		ENDIF
		EXIT
	ENDIF

	IF GET_LAST_USED_WEAPON() != wtWeaponWithFireEffectApplied
		IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(ptfxFlameEffect[ciFLAMEEFFECT_HUNTER_WEAPON])
			SWITCH iFlameColour
				CASE 1  strFlameFX = "scr_sum2_hal_rider_weapon_blue"       BREAK
				CASE 2  strFlameFX = "scr_sum2_hal_rider_weapon_green"      BREAK
				CASE 3  strFlameFX = "scr_sum2_hal_rider_weapon_greyblack"  BREAK
				CASE 4  strFlameFX = "scr_sum2_hal_rider_weapon_orange"	  	BREAK
				DEFAULT strFlameFX = "scr_sum2_hal_rider_weapon_green"      BREAK
			ENDSWITCH
			
			USE_PARTICLE_FX_ASSET(strPTFXAsset)
			
			ptfxFlameEffect[ciFLAMEEFFECT_HUNTER_WEAPON] = START_NETWORKED_PARTICLE_FX_LOOPED_ON_ENTITY(strFlameFX, eiPlayer, <<0.0,0.0,0.0>>, <<0.0,0.0,0.0>>)
			wtWeaponWithFireEffectApplied = GET_LAST_USED_WEAPON()
			PRINTLN("SET_FLAME_EFFECT_ON_PLAYER - Weapon")
		ELSE
			STOP_PARTICLE_FX_LOOPED(ptfxFlameEffect[ciFLAMEEFFECT_HUNTER_WEAPON])
			PRINTLN("SET_FLAME_EFFECT_ON_PLAYER - Remove The Effect From Player's Weapon")
		ENDIF
	ENDIF
ENDPROC

PROC SET_HALLOWEEN_ADVERSARY_2022_HUNTER_VFX(INT iTeam, INT iRule, INT iTeamPart)
	IF iRule > 0
		CLEAR_FLAME_EFFECT_ON_SANCTUS()
		
		IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxFlameEffect[ciFLAMEEFFECT_HUNTER])
			STOP_PARTICLE_FX_LOOPED(ptfxFlameEffect[ciFLAMEEFFECT_HUNTER])
		ENDIF
		
		IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxFlameEffect[ciFLAMEEFFECT_HUNTER_WEAPON])
			STOP_PARTICLE_FX_LOOPED(ptfxFlameEffect[ciFLAMEEFFECT_HUNTER_WEAPON])
		ENDIF
		
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(iLocalBoolCheck29, LBOOL29_HalloweenAdversary2022ThermalHelpTextDisplayed)
	AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
	AND NOT IS_PHONE_ONSCREEN()
		IF NOT HAS_NET_TIMER_STARTED(tdThermalVisionHelpText)
			START_NET_TIMER(tdThermalVisionHelpText)
		ELIF HAS_NET_TIMER_EXPIRED(tdThermalVisionHelpText, 10000)
			IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
				PRINT_HELP_NO_SOUND("RIDR_HELP_1B", 5000)
			ELSE
				PRINT_HELP_NO_SOUND("RIDR_HELP_1", 5000)
			ENDIF
			
			SET_BIT(iLocalBoolCheck29, LBOOL29_HalloweenAdversary2022ThermalHelpTextDisplayed)
			RESET_NET_TIMER(tdThermalVisionHelpText)
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT.iRiderFlameEffect[iTeam][iTeamPart] = 0
	OR MC_playerBD_1[iLocalPart].sCachedVehicleData.mnModelName = DUMMY_MODEL_FOR_SCRIPT
	OR NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_playerBD_1[iLocalPart].sCachedVehicleData.niCachedVehicle)
		EXIT
	ENDIF	
		
	IF IS_PED_IN_VEHICLE(LocalPlayerPed, NET_TO_VEH(MC_playerBD_1[iLocalPart].sCachedVehicleData.niCachedVehicle))
		SET_FLAME_EFFECT_ON_SANCTUS(g_FMMC_STRUCT.iRiderFlameEffect[iTeam][iTeamPart])
	ELSE
		CLEAR_FLAME_EFFECT_ON_SANCTUS()
	ENDIF
	
	SET_FLAME_EFFECT_ON_PLAYER(g_FMMC_STRUCT.iRiderFlameEffect[iTeam][iTeamPart])
ENDPROC

PROC SET_HALLOWEEN_ADVERSARY_2022_HUNTED_VFX()
	//Respawn Effect
	IF IS_BIT_SET(iLocalBoolCheck14, LBOOL14_TRIGGER_HUNTED_RESPAWN_EFFECT) 
	AND bLocalPlayerOK
	AND IS_SCREEN_FADED_IN()
			IF NOT DOES_ENTITY_EXIST(LocalPlayerPed)
				EXIT
			ENDIF
			
			STRING strPTFXAsset = "scr_sum2_hal"
			REQUEST_NAMED_PTFX_ASSET(strPTFXAsset)
		
			IF NOT HAS_NAMED_PTFX_ASSET_LOADED(strPTFXAsset)
				PRINTLN("SET_HALLOWEEN_ADVERSARY_2022_HUNTED_VFX - Waiting on the PTFX asset to load!")
				EXIT
			ENDIF
			
			USE_PARTICLE_FX_ASSET(strPTFXAsset)
			
			IF START_NETWORKED_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_sum2_hal_hunted_respawn", LocalPlayerPed, <<0.0,0.0,0.0>>, <<0.0,0.0,0.0>>)
				CLEAR_BIT(iLocalBoolCheck14, LBOOL14_TRIGGER_HUNTED_RESPAWN_EFFECT)
				PRINTLN("SET_HALLOWEEN_ADVERSARY_2022_HUNTED_VFX - Start scr_sum2_hal_hunted_respawn FX")
			ENDIF
		ENDIF
	
	//Death Effect
	IF IS_BIT_SET(iLocalBoolCheck14, LBOOL14_TRIGGER_HUNTED_DEATH_EFFECT)	
		STRING strPTFXAsset = "scr_sum2_hal"
		REQUEST_NAMED_PTFX_ASSET(strPTFXAsset)
	
		IF NOT HAS_NAMED_PTFX_ASSET_LOADED(strPTFXAsset)
			PRINTLN("SET_HALLOWEEN_ADVERSARY_2022_HUNTED_VFX - Waiting on the PTFX asset to load!")
			EXIT
		ENDIF
		
		USE_PARTICLE_FX_ASSET(strPTFXAsset)
		
		IF START_NETWORKED_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_sum2_hal_hunted_death", GET_PLAYER_COORDS(localPlayer), <<0.0,0.0,0.0>>)
			CLEAR_BIT(iLocalBoolCheck14, LBOOL14_TRIGGER_HUNTED_DEATH_EFFECT)
			PRINTLN("SET_HALLOWEEN_ADVERSARY_2022_HUNTED_VFX - Start scr_sum2_hal_hunted_death FX")
		ENDIF
	ENDIF
ENDPROC

PROC CLONE_CACHED_VEHICLE_SETTINGS(VEHICLE_INDEX &viCachedVeh, BOOL bSwitchOffRadio = FALSE)
	IF NOT IS_ENTITY_ALIVE(viCachedVeh)
		PRINTLN("[CLONE_CACHED_VEHICLE_SETTINGS] - Error - Unable to clone the cached vehicle - entity is not alive!")
		EXIT
	ENDIF
	
	CACHED_VEHICLE_STRUCT sCachedVehData = MC_playerBD_1[iLocalPart].sCachedVehicleData

	IF GET_ENTITY_MODEL(viCachedVeh) != sCachedVehData.mnModelName
		PRINTLN("[CLONE_CACHED_VEHICLE_SETTINGS] - Error - Unable to clone the cached vehicle - model name changed!")
		EXIT
	ENDIF

	SET_VEHICLE_COLOURS(viCachedVeh, sCachedVehData.iVehColour1, sCachedVehData.iVehColour2)

	IF sCachedVehData.iCustomModSettingsIndex != -1
		PRINTLN("[CLONE_CACHED_VEHICLE_SETTINGS] - Applying custom mods. iCustomModSettingsIndex = ", sCachedVehData.iCustomModSettingsIndex)
		SET_VEHICLE_MOD_KIT(viCachedVeh, 0)
		SET_VEHICLE_ALL_CUSTOM_MODS(viCachedVeh, sCachedVehData.iCustomModSettingsIndex)
	ENDIF
	
	SET_VEHICLE_NUMBER_PLATE_TEXT(viCachedVeh, sCachedVehData.strNumberPlateText)
	
	IF bSwitchOffRadio
		SET_VEH_RADIO_STATION(viCachedVeh, "OFF")
	ENDIF
	
	PRINTLN("[CLONE_CACHED_VEHICLE_SETTINGS] - Cached vehicle was successfully cloned!")
ENDPROC

PROC SET_CACHED_VEHICLE_BLIP(BOOL bIgnoreInVehCheck = FALSE)
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_playerBD_1[iLocalPart].sCachedVehicleData.niCachedVehicle)
		EXIT
	ENDIF
	
	IF NOT IS_ENTITY_ALIVE(NET_TO_ENT(MC_playerBD_1[iLocalPart].sCachedVehicleData.niCachedVehicle))
		PRINTLN("[PROCESS_CALL_PLAYER_VEHICLE] - SET_CACHED_VEHICLE_BLIP - Entity is not alive!")
		EXIT
	ENDIF
	
	BOOL bInVehicle = IS_PED_IN_VEHICLE(LocalPlayerPed, NET_TO_VEH(MC_playerBD_1[iLocalPart].sCachedVehicleData.niCachedVehicle))
		
	IF NOT DOES_BLIP_EXIST(biCachedVehicleBlip)
		IF !bInVehicle OR bIgnoreInVehCheck
			REMOVE_ANY_EXISTING_BLIP_FROM_ENTITY(NET_TO_ENT(MC_playerBD_1[iLocalPart].sCachedVehicleData.niCachedVehicle))
			biCachedVehicleBlip = CREATE_BLIP_FOR_VEHICLE(NET_TO_VEH(MC_playerBD_1[iLocalPart].sCachedVehicleData.niCachedVehicle))
			PRINTLN("[PROCESS_CALL_PLAYER_VEHICLE] - SET_CACHED_VEHICLE_BLIP - Create the blip!")
			IF IS_THIS_MODEL_A_BIKE(MC_playerBD_1[iLocalPart].sCachedVehicleData.mnModelName)
				SET_BLIP_SPRITE(biCachedVehicleBlip, RADAR_TRACE_ARENA_DEATHBIKE)
			ELIF IS_THIS_MODEL_A_CAR(MC_playerBD_1[iLocalPart].sCachedVehicleData.mnModelName)
				SET_BLIP_SPRITE(biCachedVehicleBlip, RADAR_TRACE_GETAWAY_CAR)
			ENDIF
		ENDIF
	ELIF bInVehicle AND !bIgnoreInVehCheck
		REMOVE_BLIP(biCachedVehicleBlip)
		PRINTLN("[PROCESS_CALL_PLAYER_VEHICLE] - SET_CACHED_VEHICLE_BLIP - Remove the blip!")
	ENDIF
ENDPROC

FUNC BOOL IS_CALL_VEHICLE_BUTTON_PRESSED(INT iTeam, INT iRule)
	//Button Pressed
	IF (IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_ENTER) OR IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_ENTER))
		IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iCachedVehicleCallButtonMinHeldTime[iRule] = 0
			//Skip the timer
			RETURN TRUE
		ENDIF
		//Start The Timer
		START_NET_TIMER(tdCallVehicleCooldownTimer)
	
	//Button Released
	ELIF (IS_CONTROL_JUST_RELEASED(PLAYER_CONTROL, INPUT_ENTER) OR IS_DISABLED_CONTROL_JUST_RELEASED(PLAYER_CONTROL, INPUT_ENTER))
		//Stop The Timer
		RESET_NET_TIMER(tdCallVehicleCooldownTimer)	
	ENDIF

	RETURN HAS_NET_TIMER_STARTED(tdCallVehicleCooldownTimer)
ENDFUNC

FUNC BOOL IS_CALL_VEHICLE_DELAY_FINISHED(INT iTeam, INT iRule)
	IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iCachedVehicleCallButtonMinHeldTime[iRule] = 0
		RETURN TRUE
	ENDIF

	INT iCurrentTimeDifference = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdCallVehicleCooldownTimer)
	INT iCachedVehicleCallButtonMinHeldTime = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iCachedVehicleCallButtonMinHeldTime[iRule]
	
	//Check if the player held the button long enough
	IF HAS_NET_TIMER_STARTED(tdCallVehicleCooldownTimer)
	AND iCurrentTimeDifference > iCachedVehicleCallButtonMinHeldTime
		RESET_NET_TIMER(tdCallVehicleCooldownTimer)	
		RETURN TRUE
	ENDIF

	
	RETURN FALSE
ENDFUNC

PROC DRAW_CALL_VEHICLE_BUTTON_METER(INT iTeam, INT iRule)
	INT iCurrentTimeDifference = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdCallVehicleCooldownTimer)
	INT iCachedVehicleCallButtonMinHeldTime = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iCachedVehicleCallButtonMinHeldTime[iRule]
	
	STRING strTemp

	IF IS_THIS_MODEL_A_BIKE(MC_playerBD_1[iLocalPart].sCachedVehicleData.mnModelName)
		strTemp = "GR_RB"
	ELSE
		strTemp = "GR_RV"
	ENDIF
	
	DRAW_GENERIC_METER(iCurrentTimeDifference, iCachedVehicleCallButtonMinHeldTime, strTemp, DEFAULT, DEFAULT, HUDORDER_TOP, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT)
ENDPROC

PROC PROCESS_CALL_PLAYER_VEHICLE()	
	INT iTeam = MC_playerBD[iLocalPart].iTeam
	INT iRule = GET_TEAM_CURRENT_RULE(iTeam)

	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFifteen[iRule], ciBS_RULE15_CALL_CACHED_VEHICLE) 
		EXIT
	ENDIF

	IF NOT IS_BIT_SET(MC_PlayerBD[iLocalPart].iClientBitSet4, PBBOOL4_CACHE_VEHICLE_COMPLETED)
	OR MC_playerBD_1[iLocalPart].sCachedVehicleData.mnModelName = DUMMY_MODEL_FOR_SCRIPT
		//Vehicle not cached yet
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(iLocalBoolCheck35, LBOOL35_SKIPPING_BLIP_CREATION_FOR_CACHED_VEHICLE)
		//Block HANDLE_VEHICLE_BLIP_CREATION from removing our blip when using FMMC-created veh
		SKIP_BLIP_CREATION_FOR_THIS_VEHICLE(g_FMMC_STRUCT.iCachedVehicle[MC_PlayerBD[iLocalPart].iTeam][GET_PARTICIPANT_NUMBER_IN_TEAM()], TRUE)
		SET_BIT(iLocalBoolCheck35, LBOOL35_SKIPPING_BLIP_CREATION_FOR_CACHED_VEHICLE)
		SET_BIT(iLocalBoolCheck35, LBOOL35_USING_FMMC_CREATED_CACHED_VEHICLE)
	ENDIF
	
	SET_CACHED_VEHICLE_BLIP()
	
	INT iFlashFadeEntIndex = GET_ENTITY_FLASH_FADE_INDEX()
	
	//Cleanup
	IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_FINISH_FLASH_FADE_PROC)
		//Flash Fade Cleanup
		IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_CALL_CACHED_VEHICLE_AND_SKIP_FLASH_FADE)
		AND iFlashFadeEntIndex != -1
		AND IS_IT_SAFE_TO_DELETE_A_FLASH_FADE_ENTITY(iFlashFadeEntIndex)
		AND NOT IS_BIT_SET(MC_serverBD.sFlashFade.iEntityDoingFlashFadeBS, iFlashFadeEntIndex)
			//Take control of the old flash fade entity as we need to delete it
			IF MC_serverBD.sFlashFade.niEntitiesDoingFlashFade[iFlashFadeEntIndex] != NULL
			AND NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD.sFlashFade.niEntitiesDoingFlashFade[iFlashFadeEntIndex])
			AND DOES_ENTITY_EXIST(NET_TO_ENT(MC_serverBD.sFlashFade.niEntitiesDoingFlashFade[iFlashFadeEntIndex]))
				IF NOT TAKE_CONTROL_OF_ENTITY(NET_TO_ENT(MC_serverBD.sFlashFade.niEntitiesDoingFlashFade[iFlashFadeEntIndex]))
					EXIT
				ENDIF
				VEHICLE_INDEX viPlayerVehTemp = NET_TO_VEH(MC_serverBD.sFlashFade.niEntitiesDoingFlashFade[iFlashFadeEntIndex])
				DELETE_VEHICLE(viPlayerVehTemp)
			ENDIF
			
			CLEAR_ENTITY_FLASH_FADE(iFlashFadeEntIndex)
			CLEANUP_PTFX_SMOKE(MC_playerBD_1[iLocalPart].ptfxSmokeParticle)	
			CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_CALL_CACHED_VEHICLE_AND_SKIP_FLASH_FADE)
			CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_FINISH_FLASH_FADE_PROC)
			PRINTLN("[PROCESS_CALL_PLAYER_VEHICLE] - Finish the vehicle call and flash fade processes and delete the old flash fade entity. iLocalPart ", iLocalPart, ", Ent ", iFlashFadeEntIndex)
		ENDIF
		
		//General Cleanup
		IF  IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_CALL_CACHED_VEHICLE_AND_SKIP_FLASH_FADE)
			RESET_NET_TIMER(tdCallVehicleCooldownTimer)		
			IF DOES_PARTICLE_FX_LOOPED_EXIST(MC_playerBD_1[iLocalPart].ptfxSmokeParticle)
				STOP_PARTICLE_FX_LOOPED(MC_playerBD_1[iLocalPart].ptfxSmokeParticle)
			ENDIF
			CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_CALL_CACHED_VEHICLE_AND_SKIP_FLASH_FADE)
			CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_FINISH_FLASH_FADE_PROC)
			PRINTLN("[PROCESS_CALL_PLAYER_VEHICLE] - Finish the vehicle call process. iLocalPart ", iLocalPart, ", Ent ", iFlashFadeEntIndex)
		ENDIF
		
		//We no longer need to block the FMMC blip creation as the FMMC created veh was removed
		IF IS_BIT_SET(iLocalBoolCheck35, LBOOL35_USING_FMMC_CREATED_CACHED_VEHICLE)
			SKIP_BLIP_CREATION_FOR_THIS_VEHICLE(g_FMMC_STRUCT.iCachedVehicle[MC_PlayerBD[iLocalPart].iTeam][GET_PARTICIPANT_NUMBER_IN_TEAM()], FALSE)
			CLEAR_BIT(iLocalBoolCheck35, LBOOL35_USING_FMMC_CREATED_CACHED_VEHICLE)
		ENDIF
		
		EXIT
	ENDIF
	
	//Button Press
	IF iFlashFadeEntIndex = -1
	AND NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_CALL_CACHED_VEHICLE_AND_SKIP_FLASH_FADE)
	AND _IS_PLAYER_ON_FOOT(#IF IS_DEBUG_BUILD FALSE #ENDIF)
	AND NOT IS_FLASH_FADE_ARRAY_FULL()
	AND IS_CALL_VEHICLE_BUTTON_PRESSED(iTeam, iRule)
		//Draw a meter when the button is being held
		IF NOT IS_CALL_VEHICLE_DELAY_FINISHED(iTeam, iRule)
			DRAW_CALL_VEHICLE_BUTTON_METER(iTeam, iRule)
			EXIT
		ENDIF
		
		IF NETWORK_DOES_NETWORK_ID_EXIST(MC_playerBD_1[iLocalPart].sCachedVehicleData.niCachedVehicle)		
			NETWORK_INDEX niPlayerVeh  = MC_playerBD_1[iLocalPart].sCachedVehicleData.niCachedVehicle
			VEHICLE_INDEX viPlayerVeh  = NET_TO_VEH(niPlayerVeh)
			IF IS_ENTITY_ALIVE(viPlayerVeh)
			AND IS_VEHICLE_EMPTY(viPlayerVeh, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE, FALSE)
				START_ENTITY_FLASH_FADE(niPlayerVeh)
				PRINTLN("[PROCESS_CALL_PLAYER_VEHICLE] - Button Pressed. Vehicle is empty - use the flash fade effect")
			ELIF NOT IS_CACHED_VEHICLES_FOR_CLEANUP_ARRAY_FULL()
				//Call the vehicle but skip the flash fade process as the cached vehicle is being used or doesn't exist anymore
				SET_BIT(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_CALL_CACHED_VEHICLE_AND_SKIP_FLASH_FADE)
				PRINTLN("[PROCESS_CALL_PLAYER_VEHICLE] - Button Pressed. Vehicle is not empty or not alive anymore! Skip the flash fade effect!")
			ENDIF
		ELSE //We lost the reference to the cached vehicle - skip the flash fade and recreate the vehicle
			SET_BIT(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_CALL_CACHED_VEHICLE_AND_SKIP_FLASH_FADE)
			PRINTLN("[PROCESS_CALL_PLAYER_VEHICLE] - Button Pressed. Network ID doesn't exist anymore! Skip the flash fade effect!")
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_CALL_CACHED_VEHICLE_AND_SKIP_FLASH_FADE)
		//Is entity valid? (should become valid after g_ciInstancedcontentEventType_FlashFadeEntityStart broadcast is completed)
		IF iFlashFadeEntIndex < 0
			EXIT
		ENDIF
		
		//Did the owner of the flash fade entity completed the flash fade process?
		IF NOT FLASH_FADE_HAS_FINISHED(MC_playerBD_1[iLocalPart].sFlashFade[iFlashFadeEntIndex])
			PRINTLN("[PROCESS_CALL_PLAYER_VEHICLE] - Exit as the entity is still doing a flash fade. iPart = ", iLocalPart, " | iEntIndex = ", iFlashFadeEntIndex)
			EXIT
		ENDIF
	ENDIF

	//Warp the vehicle
	IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_FINISH_FLASH_FADE_PROC)	
		//Delay the warp if we are not using the flash fade effect
		IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_CALL_CACHED_VEHICLE_AND_SKIP_FLASH_FADE)
			IF NOT HAS_NET_TIMER_STARTED(tdCallVehicleCooldownTimer)
				START_NET_TIMER(tdCallVehicleCooldownTimer)
				EXIT
			ELIF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdCallVehicleCooldownTimer) < 500
				EXIT
			ENDIF
		ENDIF
		
		PRINTLN("[PROCESS_CALL_PLAYER_VEHICLE] - Fade Completed")
		VECTOR vPlayerPos = GET_PLAYER_COORDS(GET_PLAYER_INDEX())
		FLOAT fNodeHeading
		INT iNumLanesOut
		VECTOR vNewPos
		PRINTLN("[PROCESS_CALL_PLAYER_VEHICLE] - Attempting to find a safe vehicle node for the entity")
		IF GET_SAFE_VEHICLE_NODE(vPlayerPos, vNewPos, fNodeHeading, iNumLanesOut, 1, FALSE, FALSE)	
			PRINTLN("[PROCESS_CALL_PLAYER_VEHICLE] - Safe road node found at ", vNewPos, " >>, Player Pos ", vPlayerPos, " >>, Distance = ", GET_DISTANCE_BETWEEN_COORDS(vNewPos, vPlayerPos))

			//Use player's forward vector so the new vehicle faces the same way
			fNodeHeading = GET_ENTITY_HEADING(GET_PLAYER_PED(GET_PLAYER_INDEX()))
			
			NETWORK_INDEX niNewVeh
			IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_CALL_CACHED_VEHICLE_AND_SKIP_FLASH_FADE)
				//Re-use the old NI as we are deleting the old vehicle
				niNewVeh = MC_playerBD_1[iLocalPart].sCachedVehicleData.niCachedVehicle
			ENDIF
			
			//Try to create a new vehicle
			IF NOT FMMC_CREATE_NET_VEHICLE(niNewVeh, MC_playerBD_1[iLocalPart].sCachedVehicleData.mnModelName, vNewPos, fNodeHeading, FALSE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, FALSE, DEFAULT, DEFAULT, TRUE, DEFAULT, TRUE)
				EXIT
			ENDIF
			
			PRINTLN("[PROCESS_CALL_PLAYER_VEHICLE] - Successfully created a new vehicle")
			
			//Clone vehicle
			VEHICLE_INDEX viNewVeh = NET_TO_VEH(niNewVeh)
			IF IS_ENTITY_ALIVE(viNewVeh)
				CLONE_CACHED_VEHICLE_SETTINGS(viNewVeh)
				REMOVE_BLIP_IF_EXIST(biCachedVehicleBlip)
			ENDIF
			
			NETWORK_INDEX niPlayerVeh  = MC_playerBD_1[iLocalPart].sCachedVehicleData.niCachedVehicle
			
			//Cache the new vehicle
			MC_playerBD_1[iLocalPart].sCachedVehicleData.niCachedVehicle = niNewVeh
			
			IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_CALL_CACHED_VEHICLE_AND_SKIP_FLASH_FADE)
				IF NETWORK_DOES_NETWORK_ID_EXIST(niPlayerVeh)	
					VEHICLE_INDEX viPlayerVeh  = NET_TO_VEH(niPlayerVeh)
					IF IS_ENTITY_ALIVE(viPlayerVeh)
						//If the old vehicle is being used by somone else cache it so we can delete it when it's safe to do so
						CACHE_VEHICLE_FOR_CLEANUP(niPlayerVeh)
						PRINTLN("[PROCESS_CALL_PLAYER_VEHICLE] - Cache the old vehicle for a clean-up")
					ENDIF
				ENDIF
			ELSE
				//If the old vehicle is not being used we can end the flash fade process now
				END_ENTITY_FLASH_FADE(iFlashFadeEntIndex)
				PRINTLN("[PROCESS_CALL_PLAYER_VEHICLE] - End the flash fade process")
			ENDIF
			
			//Create a smoke effect
			CREATE_PLANE_SMOKE_FX(MC_playerBD_1[iLocalPart].ptfxSmokeParticle, NET_TO_ENT(MC_playerBD_1[iLocalPart].sCachedVehicleData.niCachedVehicle), ciSMOKE_COLOUR_WHITE)
			IF DOES_PARTICLE_FX_LOOPED_EXIST(MC_playerBD_1[iLocalPart].ptfxSmokeParticle)
				SET_PARTICLE_FX_LOOPED_SCALE(MC_playerBD_1[iLocalPart].ptfxSmokeParticle, 0.8)
			ENDIF
			
			//Ready for cleanup
			SET_BIT(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_FINISH_FLASH_FADE_PROC)	
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_CLIENT_OWNER_OF_A_FLASH_FADE_ENTITY(INT &iEntToCheck)
	RETURN MC_serverBD.sFlashFade.iFlashFadePartEntIndex[iLocalPart]  = iEntToCheck
ENDFUNC

//Check if the owner of the flash fade ent finished the flash fade process
FUNC BOOL SHOULD_ENT_FINISH_FLASH_FADE(INT &iEntIndex)
	RETURN IS_CLIENT_OWNER_OF_A_FLASH_FADE_ENTITY(iEntIndex) AND FLASH_FADE_HAS_FINISHED(MC_playerBD_1[iLocalPart].sFlashFade[iEntIndex])
ENDFUNC

PROC PROCESS_ENTITY_FLASH_FADE()
	INT iEntIndex
	FOR iEntIndex = 0 TO MAX_FLASH_FADE_ENTITIES - 1
		IF MC_serverBD.sFlashFade.niEntitiesDoingFlashFade[iEntIndex] = NULL
			RELOOP
		ENDIF
		
		IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD.sFlashFade.niEntitiesDoingFlashFade[iEntIndex])
			IF NOT DOES_ENTITY_EXIST(NET_TO_ENT(MC_serverBD.sFlashFade.niEntitiesDoingFlashFade[iEntIndex]))
				PRINTLN("[FLASH FADE] - PROCESS_ENTITY_FLASH_FADE - Entity doesn't exist!. iPart = ", iLocalPart, ", iEntIndex = ", iEntIndex)
				RELOOP
			ENDIF
		ELSE
			PRINTLN("[FLASH FADE] - PROCESS_ENTITY_FLASH_FADE - NET ID doesn't exist!. iPart = ", iLocalPart, ", iEntIndex = ", iEntIndex)
			RELOOP
		ENDIF
		
		IF IS_BIT_SET(MC_serverBD.sFlashFade.iEntityDoingFlashFadeBS, iEntIndex)
			IF NOT SHOULD_ENT_FINISH_FLASH_FADE(iEntIndex)		
				IF NOT IS_BIT_SET(MC_serverBD.sFlashFade.iPlayersDoingFlashFadeBS[iEntIndex], iLocalPart)
					BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_SetPlayerDoingFlashFade, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE, iEntIndex)
					PRINTLN("[FLASH FADE] - PROCESS_ENTITY_FLASH_FADE - Broadcast that the player is doing flash fade. iPart = ", iLocalPart, ", iEntIndex = ", iEntIndex)
				ENDIF

				ENTITY_INDEX eiTemp = NET_TO_ENT(MC_serverBD.sFlashFade.niEntitiesDoingFlashFade[iEntIndex])
				FLASH_FADE_ENTITY(MC_playerBD_1[iLocalPart].sFlashFade[iEntIndex], eiTemp, 2, 50, TRUE, TRUE)
			ENDIF
		ELIF IS_BIT_SET(MC_serverBD.sFlashFade.iPlayersDoingFlashFadeBS[iEntIndex], iLocalPart)
			BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_SetPlayerDoingFlashFade, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, FALSE, iEntIndex)
			FLASH_FADE_RESET(MC_playerBD_1[iLocalPart].sFlashFade[iEntIndex])
			SET_ENTITY_ALPHA(NET_TO_ENT(MC_serverBD.sFlashFade.niEntitiesDoingFlashFade[iEntIndex]), MC_serverBD.sFlashFade.iFlashFadeEndAlpha[iEntIndex], FALSE)
			PRINTLN("[FLASH FADE] - PROCESS_ENTITY_FLASH_FADE - FLASH_FADE_RESET. iPart = ", iLocalPart, ", iEntIndex = ", iEntIndex)
		ENDIF
	ENDFOR
ENDPROC

FUNC BOOL IS_VEHICLE_CACHING_REQUIRED_FOR_THIS_TEAM()
	INT iTeam = MC_PlayerBD[iLocalPart].iTeam
	INT iRule
	
	IF IS_BIT_SET(iLocalBoolCheck11, LBOOL11_VEHICLE_CACHING_REQUIRED_CHECKED_TEAM_0 + iTeam)
		RETURN IS_BIT_SET(iLocalBoolCheck11, LBOOL11_VEHICLE_CACHING_REQUIRED_TEAM_0 + iTeam)
	ENDIF
	
	SET_BIT(iLocalBoolCheck11, LBOOL11_VEHICLE_CACHING_REQUIRED_CHECKED_TEAM_0 + iTeam)
	
	FOR iRule = 0 TO FMMC_MAX_RULES - 1
		//Vehicle calling
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFifteen[iRule], ciBS_RULE15_CALL_CACHED_VEHICLE)
			SET_BIT(iLocalBoolCheck11, LBOOL11_VEHICLE_CACHING_REQUIRED_TEAM_0 + iTeam)
			RETURN TRUE
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_LOCAL_PLAYER_CACHED_VEHICLE_RANGE_CHECK(INT iTeam, INT iRule, ENTITY_INDEX &eiPlayer, ENTITY_INDEX &eiCachedVeh)
	IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iCachedVehicleRangeCheck[iRule] = 0
		IF IS_BIT_SET(iLocalBoolCheck27, LBOOL27_PLAYER_IN_RANGE_OF_THE_CACHED_VEHICLE)
			CLEAR_BIT(iLocalBoolCheck27, LBOOL27_PLAYER_IN_RANGE_OF_THE_CACHED_VEHICLE)
		ENDIF
		EXIT
	ENDIF
	
	INT iRange = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iCachedVehicleRangeCheck[iRule]
	
	//Blip
	IF DOES_BLIP_EXIST(biCachedVehicleRangeCheckBlip)
		SET_BLIP_COORDS(biCachedVehicleRangeCheckBlip, GET_ENTITY_COORDS(eiCachedVeh, FALSE))
	ELSE 
		biCachedVehicleRangeCheckBlip = ADD_BLIP_FOR_RADIUS(GET_ENTITY_COORDS(eiCachedVeh, FALSE), TO_FLOAT(iRange))
		SET_BLIP_ALPHA(biCachedVehicleRangeCheckBlip, 150)
	ENDIF
	
	//Is player in range?
	IF GET_DISTANCE_BETWEEN_ENTITIES(eiPlayer, eiCachedVeh) < iRange
		SET_BIT(iLocalBoolCheck27, LBOOL27_PLAYER_IN_RANGE_OF_THE_CACHED_VEHICLE)
		IF NOT IS_BIT_SET(iLocalBoolCheck27, LBOOL27_CACHED_VEHICLE_RANGE_CHECK_SKIP_BLIP_COLOUR)
			SET_BLIP_COLOUR(biCachedVehicleRangeCheckBlip, BLIP_COLOUR_GREEN)
		ENDIF
	ELSE
		CLEAR_BIT(iLocalBoolCheck27, LBOOL27_PLAYER_IN_RANGE_OF_THE_CACHED_VEHICLE)
		IF NOT IS_BIT_SET(iLocalBoolCheck27, LBOOL27_CACHED_VEHICLE_RANGE_CHECK_SKIP_BLIP_COLOUR)
			SET_BLIP_COLOUR(biCachedVehicleRangeCheckBlip, BLIP_COLOUR_GREY)
		ENDIF
	ENDIF	
	
ENDPROC

PROC PROCESS_LOCAL_PLAYER_CACHED_VEHICLE_DEAL_EXTRA_DAMAGE(INT iTeam, INT iRule)
	IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].fCachedVehicleDamageMult[iRule] < 1
	OR NOT IS_BIT_SET(iLocalBoolCheck27, LBOOL27_PLAYER_IN_RANGE_OF_THE_CACHED_VEHICLE)
		IF IS_BIT_SET(iLocalBoolCheck27, LBOOL27_PLAYER_WEAPON_DAMAGE_MODIFIER_APPLIED)
			SET_PLAYER_WEAPON_DAMAGE_MODIFIER(LocalPlayer, GET_PLAYER_WEAPON_DAMAGE_MODIFIER())
			CLEAR_BIT(iLocalBoolCheck27, LBOOL27_PLAYER_WEAPON_DAMAGE_MODIFIER_APPLIED)
			PRINTLN("[CACHE_VEHICLE_DATA][PROCESS_ADDITIONAL_CACHED_VEHICLE_SYSTEMS] - PROCESS_LOCAL_PLAYER_CACHED_VEHICLE_DEAL_EXTRA_DAMAGE - Clearing weapon damage modifier")
		ENDIF
		EXIT
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck27, LBOOL27_PLAYER_WEAPON_DAMAGE_MODIFIER_APPLIED)
		EXIT
	ENDIF
	
	SET_PLAYER_WEAPON_DAMAGE_MODIFIER(LocalPlayer, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].fCachedVehicleDamageMult[iRule])
	SET_BIT(iLocalBoolCheck27, LBOOL27_PLAYER_WEAPON_DAMAGE_MODIFIER_APPLIED)
	PRINTLN("[CACHE_VEHICLE_DATA][PROCESS_ADDITIONAL_CACHED_VEHICLE_SYSTEMS] - PROCESS_LOCAL_PLAYER_CACHED_VEHICLE_DEAL_EXTRA_DAMAGE - Applying weapon damage modifier to ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].fCachedVehicleDamageMult[iRule])
ENDPROC

PROC PROCESS_LOCAL_PLAYER_CACHED_VEHICLE_HEALING(INT iTeam, INT iRule)
	//Reset values on a new rule
	IF DOES_LOCAL_PLAYER_HAVE_NEW_PRIORITY_THIS_FRAME()
	AND IS_BIT_SET(iLocalBoolCheck27, LBOOL27_CACHED_LOCAL_PLAYER_HEALTH_VALUES)
		SET_PLAYER_HEALTH_RECHARGE_MAX_PERCENT(LocalPlayer, fCachedHealthRechargeMaxPercent)
		SET_PLAYER_HEALTH_RECHARGE_MULTIPLIER(LocalPlayer, 0.0)
		IF DOES_BLIP_EXIST(biCachedVehicleRangeCheckBlip)
			REMOVE_BLIP(biCachedVehicleRangeCheckBlip)
		ENDIF
		CLEAR_BIT(iLocalBoolCheck27, LBOOL27_CACHED_LOCAL_PLAYER_HEALTH_VALUES)
		CLEAR_BIT(iLocalBoolCheck27, LBOOL27_CAN_PROCESS_CACHED_VEH_HEALING_EVENTS)
		EXIT
	ENDIF
	
	//Gets set when we cached a vehicle and nothing else is blocking it
	SET_BIT(iLocalBoolCheck27, LBOOL27_CAN_PROCESS_CACHED_VEH_HEALING_EVENTS)	
	
	//Player in range check
	IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iCachedVehicleRangeCheck[iRule] = 0
	OR g_FMMC_STRUCT.sFMMCEndConditions[iTeam].fCachedVehicleHealingRate[iRule] = 0
		EXIT
	ENDIF
	
	//Is player in range?
	IF NOT IS_BIT_SET(iLocalBoolCheck27, LBOOL27_PLAYER_IN_RANGE_OF_THE_CACHED_VEHICLE)
		SET_PLAYER_HEALTH_RECHARGE_MULTIPLIER(LocalPlayer, 0.0)
		SET_BIT(iLocalBoolCheck27, LBOOL27_CAN_PROCESS_CACHED_VEH_HEALING_EVENTS)
		CLEAR_BIT(iLocalBoolCheck27, LBOOL27_CAN_PROCESS_CACHED_VEH_HEALING_EVENTS)
		EXIT
	ENDIF
	
	//Change the area blip to red for 6 seconds if the player was shot
	IF GET_CLOUD_TIME_AS_INT() - GET_TIME_SINCE_THE_LOCAL_PLAYER_GOT_SHOT() < 6
		SET_BIT(iLocalBoolCheck27, LBOOL27_CACHED_VEHICLE_RANGE_CHECK_SKIP_BLIP_COLOUR)
		SET_BLIP_COLOUR(biCachedVehicleRangeCheckBlip, BLIP_COLOUR_GREY)
	ELSE
		CLEAR_BIT(iLocalBoolCheck27, LBOOL27_CACHED_VEHICLE_RANGE_CHECK_SKIP_BLIP_COLOUR)
	ENDIF
	
	//Cache max health recharge percent
	IF NOT IS_BIT_SET(iLocalBoolCheck27, LBOOL27_CACHED_LOCAL_PLAYER_HEALTH_VALUES)
		fCachedHealthRechargeMaxPercent = GET_PLAYER_HEALTH_RECHARGE_MAX_PERCENT(LocalPlayer)
		SET_BIT(iLocalBoolCheck27, LBOOL27_CACHED_LOCAL_PLAYER_HEALTH_VALUES)
	ENDIF

	//Heal the player
	SET_PLAYER_HEALTH_RECHARGE_MAX_PERCENT(LocalPlayer, 1.0)
	SET_PLAYER_HEALTH_RECHARGE_MULTIPLIER(LocalPlayer, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].fCachedVehicleHealingRate[iRule])
	PRINTLN("[CACHE_VEHICLE_DATA][PROCESS_ADDITIONAL_CACHED_VEHICLE_SYSTEMS] - PROCESS_LOCAL_PLAYER_CACHED_VEHICLE_HEALING - Healing the player")
ENDPROC

//Process any additional systems that use the cached vehicle data
PROC PROCESS_ADDITIONAL_CACHED_VEHICLE_SYSTEMS()
	INT iTeam = MC_PlayerBD[iLocalPart].iTeam
	INT iRule = GET_TEAM_CURRENT_RULE(iTeam)
	
	IF NOT IS_TEAM_INDEX_VALID(iTeam) OR NOT IS_RULE_INDEX_VALID(iRule)
		EXIT
	ENDIF
	
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_playerBD_1[iLocalPart].sCachedVehicleData.niCachedVehicle)
		EXIT
	ENDIF
	
	ENTITY_INDEX eiPlayer = LocalPlayerPed
	ENTITY_INDEX eiCachedVeh = NET_TO_ENT(MC_playerBD_1[iLocalPart].sCachedVehicleData.niCachedVehicle)
	
	IF NOT IS_ENTITY_ALIVE(eiPlayer)
	OR NOT IS_ENTITY_ALIVE(eiCachedVeh)
		EXIT
	ENDIF
	
	PROCESS_LOCAL_PLAYER_CACHED_VEHICLE_RANGE_CHECK(iTeam, iRule, eiPlayer, eiCachedVeh)
	PROCESS_LOCAL_PLAYER_CACHED_VEHICLE_HEALING(iTeam, iRule)
	PROCESS_LOCAL_PLAYER_CACHED_VEHICLE_DEAL_EXTRA_DAMAGE(iTeam, iRule)

ENDPROC

PROC CACHE_RESPAWN_VEHICLE_OVERRIDE()
	IF NOT bLocalPlayerOK
		EXIT
	ENDIF
	
	//Cleanup the old cached vehicle
	IF NETWORK_DOES_NETWORK_ID_EXIST(MC_playerBD_1[iLocalPart].sCachedVehicleData.niCachedVehicle)
		PRINTLN("CACHE_VEHICLE_DATA - CACHE_RESPAWN_VEHICLE_OVERRIDE - Mark old cached vehicle for cleanup")
		CACHE_VEHICLE_FOR_CLEANUP(MC_playerBD_1[iLocalPart].sCachedVehicleData.niCachedVehicle)
		MC_playerBD_1[iLocalPart].sCachedVehicleData.niCachedVehicle = NULL
	ENDIF
	
	IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		VEHICLE_INDEX viPlayerVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
		NETWORK_INDEX niTemp
		
		//Grab from freemode
		IF NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(viPlayerVeh)
		OR NOT IS_ENTITY_A_MISSION_ENTITY(viPlayerVeh)
			SET_ENTITY_AS_MISSION_ENTITY(viPlayerVeh, FALSE, TRUE)
			IF NETWORK_HAS_CONTROL_OF_ENTITY(viPlayerVeh)
				niTemp = VEH_TO_NET(viPlayerVeh)
				SET_NETWORK_ID_EXISTS_ON_ALL_MACHINES(niTemp, TRUE)
				PRINTLN("CACHE_VEHICLE_DATA - CACHE_RESPAWN_VEHICLE_OVERRIDE - Vehicle grabbed from freemode")
			ENDIF
		ENDIF
		
		IF NOT TAKE_CONTROL_OF_ENTITY(CONVERT_TO_ENTITY(viPlayerVeh))
			EXIT
		ENDIF
		
		//Cache the new vehicle
		IF NETWORK_DOES_NETWORK_ID_EXIST(niTemp)
			RESET_ENTITY_ALPHA(viPlayerVeh)
			SET_ENTITY_VISIBLE(viPlayerVeh, TRUE)
			SET_NETWORK_VEHICLE_RESPOT_TIMER(niTemp, 0, FALSE, FALSE)
			SET_NETWORK_VEHICLE_AS_GHOST(viPlayerVeh, FALSE)
			SET_ENTITY_ALPHA(viPlayerVeh, 255, FALSE)
			FREEZE_ENTITY_POSITION(viPlayerVeh, FALSE)
			SET_ENTITY_COLLISION(viPlayerVeh, TRUE)
			MC_playerBD_1[iLocalPart].sCachedVehicleData.niCachedVehicle = niTemp
			CLONE_CACHED_VEHICLE_SETTINGS(viPlayerVeh, TRUE)
			CLEAR_BIT(MC_PlayerBD[iLocalPart].iClientBitSet2, PBBOOL2_CACHE_RESPAWN_VEHICLE)
			SET_BIT(MC_PlayerBD[iLocalPart].iClientBitSet4, PBBOOL4_CACHE_VEHICLE_COMPLETED)
			PRINTLN("CACHE_VEHICLE_DATA - CACHE_RESPAWN_VEHICLE_OVERRIDE - Successfully cached the respawn vehicle for Player ", GET_PARTICIPANT_NUMBER_IN_TEAM(), " from team ", MC_PlayerBD[iLocalPart].iTeam, " (iLocalPart = ", iLocalPart, ")")
		ENDIF
		
	ENDIF
ENDPROC

//This should run once at the start of the mission but if we need to cache a vehicle later
//for example on a specific rule, we can just clear the PBBOOL4_CACHE_VEHICLE_COMPLETED bit for a specific player.
PROC CACHE_VEHICLE_DATA()
	
	IF IS_BIT_SET(MC_PlayerBD[iLocalPart].iClientBitSet4, PBBOOL4_CACHE_VEHICLE_COMPLETED)
		PROCESS_ADDITIONAL_CACHED_VEHICLE_SYSTEMS()
		EXIT
	ENDIF
	
	//Only cache the linked vehicle if we are going to use it for anything
	IF NOT IS_VEHICLE_CACHING_REQUIRED_FOR_THIS_TEAM()
		CLEAR_BIT(MC_PlayerBD[iLocalPart].iClientBitSet2, PBBOOL2_CACHE_RESPAWN_VEHICLE)
		SET_BIT(MC_PlayerBD[iLocalPart].iClientBitSet4, PBBOOL4_CACHE_VEHICLE_COMPLETED)
		PRINTLN("CACHE_VEHICLE_DATA - Don't cache the linked vehicle as the team ", MC_PlayerBD[iLocalPart].iTeam, " is not using the cached data for anything!")
		EXIT
	ENDIF
	
	INT iTeam      = MC_PlayerBD[iLocalPart].iTeam
	INT iTeamPart  = GET_PARTICIPANT_NUMBER_IN_TEAM()
	
	IF iTeamPart >= MAX_PLAYERS_PER_TEAM
		ASSERTLN("CACHE_VEHICLE_DATA - We can only cache linked vehicle for 4 players per team!")
		EXIT
	ENDIF
	
	IF IS_BIT_SET(MC_PlayerBD[iLocalPart].iClientBitSet2, PBBOOL2_CACHE_RESPAWN_VEHICLE)
		//Cache the new respawn vehicle instead of the linked one
		PRINTLN("CACHE_VEHICLE_DATA - Using cache respawn vehicle override")
		CACHE_RESPAWN_VEHICLE_OVERRIDE()
		EXIT
	ENDIF
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(MC_playerBD_1[iLocalPart].sCachedVehicleData.niCachedVehicle)
		ASSERTLN("CACHE_VEHICLE_DATA - Overriding Net ID of the cached vehicle!")
	ENDIF
	
	INT iLinkedVehicleID = g_FMMC_STRUCT.iCachedVehicle[iTeam][iTeamPart]
	
	IF iLinkedVehicleID < 0
		EXIT
	ENDIF
	
	NETWORK_INDEX niVehToCache = MC_serverBD_1.sFMMC_SBD.niVehicle[iLinkedVehicleID]
	VEHICLE_INDEX viVehToCache = NET_TO_VEH(niVehToCache)
	
	IF NOT IS_ENTITY_ALIVE(viVehToCache)
		EXIT
	ENDIF
	
	MC_playerBD_1[iLocalPart].sCachedVehicleData.niCachedVehicle = niVehToCache
	MC_playerBD_1[iLocalPart].sCachedVehicleData.mnModelName = GET_ENTITY_MODEL(NET_TO_ENT(niVehToCache))
	GET_VEHICLE_COLOURS(viVehToCache, MC_playerBD_1[iLocalPart].sCachedVehicleData.iVehColour1, MC_playerBD_1[iLocalPart].sCachedVehicleData.iVehColour2)
	MC_playerBD_1[iLocalPart].sCachedVehicleData.strNumberPlateText = GET_VEHICLE_NUMBER_PLATE_TEXT(viVehToCache)
	MC_playerBD_1[iLocalPart].sCachedVehicleData.iCustomModSettingsIndex = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iLinkedVehicleID].iCustomModSettingsIndex
	
	SET_BIT(MC_PlayerBD[iLocalPart].iClientBitSet4, PBBOOL4_CACHE_VEHICLE_COMPLETED)
	PRINTLN("CACHE_VEHICLE_DATA - Successfully cached the vehicle ", iLinkedVehicleID, " for Player ", iTeamPart, " from team ", iTeam, " (iLocalPart = ", iLocalPart, ")")
ENDPROC

PROC DRAW_BLIP_OUTLINE_WHEN_THE_ENEMY_TEAM_IS_FORCE_BLIPPING_THE_LOCAL_PLAYER(INT iTeam)
	//Check if any of the enemy teams is force blipping the local player this rule so we can draw an outline when it happens
	IF DOES_LOCAL_PLAYER_HAVE_NEW_PRIORITY_THIS_FRAME()
		IF IS_BIT_SET(iLocalBoolCheck29, LBOOL29_LIGHTNING_STRIKE_BLIPPING_ENABLED_FOR_THE_ENEMY_TEAM)
			PROCESS_MY_PLAYER_BLIP_VISIBILITY(iTeam, GET_TEAM_CURRENT_RULE(iTeam)) //To re-enable HIDE_MY_PLAYER_BLIP_FROM_TEAM based on the previous options
			CLEAR_OUTLINE_BLIP_COLOUR_FOR_PLAYER(iLocalPart)
			CLEAR_BIT(iLocalBoolCheck29, LBOOL29_LIGHTNING_STRIKE_BLIPPING_ENABLED_FOR_THE_ENEMY_TEAM)
		ENDIF
		
		//Enable bit if the enemy team is force blipping the player on this rule
		INT iEnemyTeam
		FOR iEnemyTeam = 0 TO FMMC_MAX_TEAMS - 1
			//Only check the enemy teams
			IF iEnemyTeam = iTeam
				RELOOP
			ENDIF
			//Is the option enabled for any of the enemy teams?
			IF g_FMMC_STRUCT.sFMMCEndConditions[iEnemyTeam].iLightningStrikeMin[GET_TEAM_CURRENT_RULE(iEnemyTeam)] > 0	
				SET_BIT(iLocalBoolCheck29, LBOOL29_LIGHTNING_STRIKE_BLIPPING_ENABLED_FOR_THE_ENEMY_TEAM)
				
				#IF IS_DEBUG_BUILD
					IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBlipData.iFlags, (PBBD_HIDDEN_FROM_TEAM_0 + iTeam) )
						PRINTLN("HIDE_MY_PLAYER_BLIP_FROM_TEAM -  DRAW_BLIP_OUTLINE_WHEN_THE_ENEMY_TEAM_IS_FORCE_BLIPPING_THE_LOCAL_PLAYER - Ignoring \"Hidden from Team ", iEnemyTeam, "\" option due to LBOOL29_LIGHTNING_STRIKE_BLIPPING_ENABLED_FOR_THE_ENEMY_TEAM being set for the enemy team as we need to force blip this player!")
					ENDIF
				#ENDIF
				
				HIDE_MY_PLAYER_BLIP_FROM_TEAM(FALSE, iEnemyTeam)
			ENDIF			
		ENDFOR
	ENDIF
	
	//Enable/Disable blip outline
	IF IS_BIT_SET(iLocalBoolCheck29, LBOOL29_LIGHTNING_STRIKE_BLIPPING_ENABLED_FOR_THE_ENEMY_TEAM)
		IF IS_PLAYER_FORCE_BLIPPED(LocalPlayer)
			IF NOT IS_OUTLINE_BLIP_ACTIVE_FOR_PLAYER(iLocalPart)
				SET_OUTLINE_BLIP_COLOUR_FOR_PLAYER(iLocalPart, 255, 0, 0)
			ENDIF
		ELSE
			CLEAR_OUTLINE_BLIP_COLOUR_FOR_PLAYER(iLocalPart)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_LIGHTNING_STRIKE(INT iTeam, INT iRule)
	
	DRAW_BLIP_OUTLINE_WHEN_THE_ENEMY_TEAM_IS_FORCE_BLIPPING_THE_LOCAL_PLAYER(iTeam)
	
	//Option Disdabled
	IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iLightningStrikeMin[iRule] = 0
		EXIT
	ENDIF
	
	IF DOES_LOCAL_PLAYER_HAVE_NEW_PRIORITY_THIS_FRAME()
		SET_BIT(iLocalBoolCheck16, LBOOL16_FORCE_BLIP_ALL_PLAYERS_TRIGGERED)
	ENDIF
	
	//Remove the enemy blips
	IF (iRemoveLightningStrikeBlipTime != 0
	AND GET_CLOUD_TIME_AS_INT() >= iRemoveLightningStrikeBlipTime)
		FORCE_BLIP_ALL_PLAYERS(FALSE, FALSE, TRUE)
		iRemoveLightningStrikeBlipTime = 0
		//CLEAR_BIT(iLocalBoolCheck, LBOOL_SHOW_ALL_PLAYER_BLIPS)
		PRINTLN("PROCESS_LIGHTNING_STRIKE - Removing the enemy blips from the minimap")
		EXIT
	ENDIF
	
	//Force lightning and blip the enemies
	IF iRemoveLightningStrikeBlipTime = 0
	AND GET_CLOUD_TIME_AS_INT() >= iCachedNextLightningStrike
		FORCE_BLIP_ALL_PLAYERS(TRUE, TRUE, TRUE)
	
		iRemoveLightningStrikeBlipTime = GET_CLOUD_TIME_AS_INT() + g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iLightningStrikeBlipTime[iRule]
		
		IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iLightningStrikeMax[iRule] > 0
			iCachedNextLightningStrike = GET_CLOUD_TIME_AS_INT() + GET_RANDOM_INT_IN_RANGE(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iLightningStrikeMin[iRule], g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iLightningStrikeMax[iRule])
		ELSE
			iCachedNextLightningStrike = GET_CLOUD_TIME_AS_INT() + g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iLightningStrikeMin[iRule]
		ENDIF
				
		FORCE_LIGHTNING_FLASH()
		PRINTLN("PROCESS_LIGHTNING_STRIKE - Forcing lightning strike and blipping the enemy players on the minimap")
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: React To Smoke  ------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_REACT_TO_SMOKE(INT iTeam, INT iRule)

	// Cleanup
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFifteen[iRule], ciBS_RULE15_REACT_TO_SMOKE)
		CLEAR_BIT(iLocalBoolCheck35, LBOOL35_SETUP_REACT_TO_SMOKE)
		CLEAR_BIT(iLocalBoolCheck35, LBOOL35_LOADED_REACT_TO_SMOKE_ANIM)
		CLEAR_BIT(iLocalBoolCheck35, LBOOL35_STARTED_REACT_TO_SMOKE_ANIM)
		CLEAR_BIT(iLocalBoolCheck35, LBOOL35_REACT_TO_SMOKE_COUGH_FIRED_THIS_EVENT)
		EXIT
	ENDIF
	
	// Setup
	IF NOT IS_BIT_SET(iLocalBoolCheck35, LBOOL35_SETUP_REACT_TO_SMOKE)
		GENERATE_REACT_TO_SMOKE_REACT_VARIATION()
		ASSIGN_SOUND_ID(iRogueDronesPlayerDruggedLoopSoundID)
		PLAY_SOUND_FRONTEND(iRogueDronesPlayerDruggedLoopSoundID, "player_drugged_loop", "DLC_MPSUM2_ULP2_Rogue_Drones")
		SET_BIT(iLocalBoolCheck35, LBOOL35_SETUP_REACT_TO_SMOKE)
	ENDIF
	
	IF NOT LOAD_REACT_TO_SMOKE_ANIMATION()
	OR NOT REQUEST_SCRIPT_AUDIO_BANK("DLC_HEIST3/CASINO_HEIST_FINALE_GENERAL_01")
		EXIT
	ENDIF
	
	IF IS_ENTITY_ALIVE(LocalPlayerPed)
	
		// Start animation
		IF NOT IS_BIT_SET(iLocalBoolCheck35, LBOOL35_STARTED_REACT_TO_SMOKE_ANIM)
		
			FLOAT fAnimDuration = GET_ANIM_DURATION(sReactToSmokeAnimData.dictionary0, sReactToSmokeAnimData.anim0) * 1000
			
			IF GET_LOCAL_PLAYER_OBJECTIVE_TIMER_TIME_REMAINING() <= iReactToSmokeReactVariation + fAnimDuration
				
				APPLY_OR_REMOVE_POISON_GAS_SCREEN_EFFECT(TRUE, DEFAULT, DEFAULT, TRUE)
				
				SET_PLAYER_CONTROL(LocalPlayer, FALSE)
				TASK_PLAY_ANIM(LocalPlayerPed, sReactToSmokeAnimData.dictionary0, sReactToSmokeAnimData.anim0)
				SET_BIT(iLocalBoolCheck35, LBOOL35_STARTED_REACT_TO_SMOKE_ANIM)
				
			ENDIF
			
		ENDIF
		
		// Playing animation
		IF IS_ENTITY_PLAYING_ANIM(LocalPlayerPed, sReactToSmokeAnimData.dictionary0, sReactToSmokeAnimData.anim0)
			
			// Cough
			IF NOT HAS_ANIM_EVENT_FIRED(LocalPlayerPed, HASH("cough"))

				CLEAR_BIT_IF_SET(iLocalBoolCheck35, LBOOL35_REACT_TO_SMOKE_COUGH_FIRED_THIS_EVENT)
			
			ELIF NOT IS_BIT_SET(iLocalBoolCheck35, LBOOL35_REACT_TO_SMOKE_COUGH_FIRED_THIS_EVENT)
			
				PLAY_FACIAL_ANIM(LocalPlayerPed, sReactToSmokeAnimData.anim1, sReactToSmokeAnimData.dictionary0)
				PLAY_SOUND_FROM_ENTITY(-1, GET_COUGH_SOUND_EFFECT(), LocalPlayerPed, "dlc_ch_heist_finale_poison_gas_coughs_sounds", TRUE, 500)
				SET_BIT(iLocalBoolCheck35, LBOOL35_REACT_TO_SMOKE_COUGH_FIRED_THIS_EVENT)	
			
			ENDIF
		
			// Ragdoll
			IF HAS_ANIM_EVENT_FIRED(LocalPlayerPed, HASH("ragdoll"))
			
				SET_PED_TO_RAGDOLL(LocalPlayerPed, GET_LOCAL_PLAYER_OBJECTIVE_TIMER_TIME_REMAINING() + 1000, GET_LOCAL_PLAYER_OBJECTIVE_TIMER_TIME_REMAINING() + 1000, TASK_RELAX)
				
				IF IS_ENTITY_PLAYING_ANIM(LocalPlayerPed, sReactToSmokeAnimData.dictionary0, sReactToSmokeAnimData.anim1)
					STOP_ANIM_TASK(LocalPlayerPed, sReactToSmokeAnimData.dictionary0, sReactToSmokeAnimData.anim1)
				ENDIF
				
				STOP_ANIM_TASK(LocalPlayerPed, sReactToSmokeAnimData.dictionary0, sReactToSmokeAnimData.anim0)
				FMMC_SAFE_FADE_SCREEN_OUT_TO_BLACK(GET_LOCAL_PLAYER_OBJECTIVE_TIMER_TIME_REMAINING() - 1000)
		
			ENDIF
			
		ENDIF
		
		IF GET_LOCAL_PLAYER_OBJECTIVE_TIMER_TIME_REMAINING() <= cfPoisonGasFXTransitionTimeShort
			APPLY_OR_REMOVE_POISON_GAS_SCREEN_EFFECT(FALSE, DEFAULT, cfPoisonGasFXTransitionTimeShort, TRUE)
		ENDIF
		
	ENDIF
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Players Wake Up -------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_PLAYERS_WAKE_UP(INT iTeam, INT iRule)
	
	// Cleanup
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSixteen[iRule], ciBS_RULE16_PLAYERS_WAKE_UP)
		IF IS_BIT_SET(iLocalBoolCheck35, LBOOL35_PLAYING_PLAYERS_WAKE_UP_ANIM)
			PRINTLN("PROCESS_PLAYERS_WAKE_UP - rule over, giving control back")
			PWU_RESTORE_LOCAL_PLAYER_CAMERA_VIEW_MODE()
			NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
		ENDIF
		CLEAR_BIT(iLocalBoolCheck35, LBOOL35_LOADED_PLAYERS_WAKE_UP_ANIM)
		CLEAR_BIT(iLocalBoolCheck35, LBOOL35_STARTED_PLAYERS_WAKE_UP_ANIM)
		CLEAR_BIT(iLocalBoolCheck35, LBOOL35_PLAYING_PLAYERS_WAKE_UP_ANIM)
		CLEAR_BIT(iLocalBoolCheck35, LBOOL35_SETUP_PLAYERS_WAKE_UP)
		EXIT
	ENDIF
	
	// Setup
	IF NOT IS_BIT_SET(iLocalBoolCheck35, LBOOL35_SETUP_PLAYERS_WAKE_UP)
		
		STOP_AND_RELEASE_SOUND(iRogueDronesPlayerDruggedLoopSoundID)
		PWU_CACHE_LOCAL_PLAYER_CAMERA_VIEW_MODE()
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
		SET_CAM_VIEW_MODE_FOR_CONTEXT(GET_CAM_ACTIVE_VIEW_MODE_CONTEXT(), CAM_VIEW_MODE_FIRST_PERSON)
		
		APPLY_OR_REMOVE_POISON_GAS_SCREEN_EFFECT(TRUE, FALSE, cfPoisonGasFXTransitionTimeShort, TRUE)
		SET_BIT(iLocalBoolCheck35, LBOOL35_SETUP_PLAYERS_WAKE_UP)
		
	ENDIF
	
	IF NOT LOAD_PLAYERS_WAKE_UP_ANIMATION()
		EXIT
	ENDIF
	
	IF IS_ENTITY_ALIVE(LocalPlayerPed)
	
		// Start animation
		IF NOT IS_BIT_SET(iLocalBoolCheck35, LBOOL35_STARTED_PLAYERS_WAKE_UP_ANIM)
			   
			PRINTLN("PROCESS_PLAYERS_WAKE_UP - Tasking to do wake up anim and removing control")
			NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE)
			
			ANIMPOSTFX_PLAY("ULP_PlayerWakeUp", 0, FALSE)
			TASK_PLAY_ANIM(LocalPlayerPed, sPlayersWakeUpAnimData.dictionary0, sPlayersWakeUpAnimData.anim0, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 0.05)
			SET_BIT(iLocalBoolCheck35, LBOOL35_STARTED_PLAYERS_WAKE_UP_ANIM)
			
		ENDIF
		
		// Playing animation
		IF IS_ENTITY_PLAYING_ANIM(LocalPlayerPed, sPlayersWakeUpAnimData.dictionary0, sPlayersWakeUpAnimData.anim0)
		
			FMMC_SAFE_FADE_SCREEN_IN_FROM_BLACK(2500)
			APPLY_OR_REMOVE_POISON_GAS_SCREEN_EFFECT(FALSE, FALSE, cfPoisonGasFXTransitionTimeExtended, TRUE)
			SET_BIT(iLocalBoolCheck35, LBOOL35_PLAYING_PLAYERS_WAKE_UP_ANIM)
			
		ELSE
		   
			// Animation over
			IF IS_BIT_SET(iLocalBoolCheck35, LBOOL35_PLAYING_PLAYERS_WAKE_UP_ANIM)
				PRINTLN("PROCESS_PLAYERS_WAKE_UP - anim over, giving control back")
				PWU_RESTORE_LOCAL_PLAYER_CAMERA_VIEW_MODE()
				NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
			ENDIF
			
		ENDIF
		
	ENDIF
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Main Staggered Processing Function  ---------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Staggerd processing for the local player.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_STAGGERED_LOCAL_PLAYER_CHECKS_ONE(INT& iTeam, INT& iRule, BOOL& bValidRule)
	
	PROCESS_REENABLE_SCUBA_GEAR()
	
	HANDLE_LADDER_DISABLED_ON_RULE()
	
	PROCESS_HAS_LOCAL_PLAYER_BEEN_IN_THE_WATER()
	
	PROCESS_PLAYER_PASSENGER_BOMBING()
	
	IF bValidRule
		PROCESS_LOCAL_PLAYER_VEHICLE_WHEN_INACTIVE(iTeam, iRule)
		PROCESS_LOCAL_PLAYER_DEFENSE_TAKEDOWN_MODIFIER(iTeam, iRule)
	ENDIF
	
ENDPROC

PROC PROCESS_STAGGERED_LOCAL_PLAYER_CHECKS_TWO(INT& iTeam, INT& iRule, BOOL& bValidRule)

	PROCESS_LOCAL_PLAYER_INTRO_GAIT()
	
	CHECK_IF_NEW_RESPAWN_POINTS_SHOULD_BE_ADDED()
	
	PROCESS_MINIGAME_DAMAGE_PROTECTION()
	
	IF bValidRule
		PROCESS_PLAYER_FORCE_LOCK_ON(iTeam, iRule)
		
		PROCESS_DIVING_HELP_TEXT(iTeam, iRule)
	ENDIF
	
ENDPROC

PROC PROCESS_STAGGERED_LOCAL_PLAYER_CHECKS_THREE(INT& iTeam, INT& iRule, BOOL& bValidRule)
	
	ASSIGN_GANG_BOSS_TYPE()
	
	PROCESS_ENABLE_STUNT_DRIVING_FOR_LOCAL_PLAYER()
	
	PROCES_REMOVE_LOCAL_PLAYER_HELMET_CREATOR_OPTION()
		
	PROCESS_REACTIVATING_PERSONAL_VEHICLES()
	
	PROCESS_SWAP_VEHICLE_RETRACTABLE_WHEEL_BUTTON()
	
	PROCESS_LOCAL_PLAYER_MELEE_DAMAGE_MODIFIER()
	
	IF bValidRule
		PROCESS_REQUIRED_OBJECT_HELD(iTeam, iRule)
	ENDIF
	
ENDPROC

PROC PROCESS_STAGGERED_LOCAL_PLAYER_CHECKS_FOUR(INT& iTeam, INT& iRule, BOOL& bValidRule)
	
	UNUSED_PARAMETER(iRule)
	UNUSED_PARAMETER(bValidRule)
	
	STORE_PV_PLAYER_IS_USING()
	
	MAINTAIN_EMERGENCY_CALLS()
	
	PROCESS_INFINITE_AMMO_CREATOR_SETTING()
	
	PROCESS_TEAM_VEHICLE_FLAG_AND_CLEAN_UP()
	
	PROCESS_DELETE_ANY_EMPTY_NEARBY_VEHICLES(iTeam)
	
	PROCESS_LOCAL_PLAYER_REMOTE_EXPLOSIVE_VEHICLE()
	
	PROCESS_HALLOWEEN_MODE_LOGIC()
	
	PROCESS_PARACHUTE_REEQUIP_LAST_WEAPON_AFTER_LANDING()
	
	PROCESS_LOCAL_PLAYER_DEATH_USES_WHITEOUT_FADING()	
	
ENDPROC

PROC PROCESS_STAGGERED_LOCAL_PLAYER_CHECKS_FIVE(INT& iTeam, INT& iRule, BOOL& bValidRule)

	PROCESS_JET_SMOKE_TRAIL(iTeam)
	
	PROCESS_PLAYER_RESET_SPAWN_PROTECTION()
	
	PROCESS_ALLOWING_SPRINTING_IN_INTERIORS()
	
	PROCESS_PLAYER_STOLEN_GOODS_SLOW_DOWN_MC()
	
	PROCESS_ALLOW_KILL_YOURSELF_ON_MISSION(iTeam)
	
	PROCESS_CLEAR_CACHED_VEHICLES()
	
	PROCESS_FLARE_ATTACHMENT_FOR_PLAYER()
	
	IF bValidRule
		PROCESS_ON_RULE_EMAIL(iTeam, iRule)
		
		PROCESS_PLAYER_DRUNK_ON_RULE(iTeam, iRule)
		
		PROCESS_CONTROLLER_LIGHT(iTeam, iRule)
		
		CACHE_IS_PLAYER_IN_CREATOR_TRAILER()
		
		PROCESS_TRANSFORM_IN_SUDDEN_DEATH(iTeam)
		
		PROCESS_ALLOWING_NON_HEIST_REBREATHERS(iTeam, iRule)
	ENDIF
		
ENDPROC

// Do stuff at the Start of Staggered processing.
PROC PRE_PROCESS_STAGGERED_LOCAL_PLAYER_CHECKS()

ENDPROC

// Do stuff at the end of staggered processing 
PROC POST_PROCESS_STAGGERED_LOCAL_PLAYER_CHECKS()

ENDPROC

/// PURPOSE: Reduce every frame processing.
/// Functions that do a fair bit of procesing only to set some data should go in here, especially if they aren't necessary to be done every frame.
/// Split into multiple functions that are staggered every frame. This is to improve frame time.
/// Nothing that needs to check HAS_NEW_PRIORITY_THIS_FRAME or NEW_MIDPOINT should go in here!  
PROC PROCESS_STAGGERED_LOCAL_PLAYER_CHECKS() // Enter a bug before submission to move more things into this function.	
	
	INT iTeam = MC_PlayerBD[iLocalPart].iteam
	INT iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]
	
	IF iStaggeredLocalPlayerIterator = 0
		PRE_PROCESS_STAGGERED_LOCAL_PLAYER_CHECKS()
	ENDIF
	
	IF DOES_LOCAL_PLAYER_HAVE_NEW_PRIORITY_THIS_FRAME()
		iRuleChangedDuringStaggeredLocalPlayerIteratorTracker = iStaggeredLocalPlayerIterator
	ELIF iRuleChangedDuringStaggeredLocalPlayerIteratorTracker = iStaggeredLocalPlayerIterator
		iRuleChangedDuringStaggeredLocalPlayerIteratorTracker = -1
	ENDIF
	
	BOOL bValidRule = (iRule < FMMC_MAX_RULES)
	
	SWITCH iStaggeredLocalPlayerIterator
		CASE ciFMMC_MAX_LOCAL_PLAYER_STAGGERED_ITERATOR_ONE
			PROCESS_STAGGERED_LOCAL_PLAYER_CHECKS_ONE(iTeam, iRule, bValidRule)
		BREAK
		
		CASE ciFMMC_MAX_LOCAL_PLAYER_STAGGERED_ITERATOR_TWO
			PROCESS_STAGGERED_LOCAL_PLAYER_CHECKS_TWO(iTeam, iRule, bValidRule)
		BREAK
		
		CASE ciFMMC_MAX_LOCAL_PLAYER_STAGGERED_ITERATOR_THREE
			PROCESS_STAGGERED_LOCAL_PLAYER_CHECKS_THREE(iTeam, iRule, bValidRule)
		BREAK
		
		CASE ciFMMC_MAX_LOCAL_PLAYER_STAGGERED_ITERATOR_FOUR
			PROCESS_STAGGERED_LOCAL_PLAYER_CHECKS_FOUR(iTeam, iRule, bValidRule)
		BREAK
		
		CASE ciFMMC_MAX_LOCAL_PLAYER_STAGGERED_ITERATOR_FIVE
			PROCESS_STAGGERED_LOCAL_PLAYER_CHECKS_FIVE(iTeam, iRule, bValidRule)
		BREAK
	ENDSWITCH
	
	iStaggeredLocalPlayerIterator++
	IF iStaggeredLocalPlayerIterator > ciFMMC_MAX_LOCAL_PLAYER_STAGGERED_ITERATOR
		iStaggeredLocalPlayerIterator = 0
		POST_PROCESS_STAGGERED_LOCAL_PLAYER_CHECKS()
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Main Processing Function  ---------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: The main processing every frame loop 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_LOCAL_PLAYER_NEW_RULE(INT iTeam, INT iRule)
	
	IF iTeam <= -1
	OR iRule >= FMMC_MAX_RULES
		EXIT
	ENDIF

	MC_START_NET_TIMER_WITH_TIMESTAMP(iRuleStartedTimeStamp)	
	
	IF DOES_LOCAL_PLAYER_HAVE_NEW_PRIORITY_THIS_FRAME()
	
		PRINTLN("PROCESS_LOCAL_PLAYER_NEW_RULE - DOES_LOCAL_PLAYER_HAVE_NEW_PRIORITY_THIS_FRAME - TRUE this frame.")
		
		MC_REINIT_NET_TIMER_WITH_TIMESTAMP(iRuleStartedTimeStamp)
	
		IF NOT IS_PARTICIPANT_A_BEAST(iLocalPart)
		AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitSetFive[iRule], ciBS_RULE5_BEASTMODE_INCREASE_DAMAGE)
			PROCESS_MELEE_DAMAGE_OVERRIDE_FOR_THIS_RULE()
			PROCESS_MELEE_WEAPON_DAMAGE_OVERRIDE_FOR_THIS_RULE(iTeam, iRule)
			PROCESS_VEHICLE_WEAPON_DAMAGE_OUTPUT_OVERRIDE_FOR_THIS_RULE(iTeam, iRule)
			PROCESS_VEHICLE_WEAPON_DAMAGE_INTAKE_OVERRIDE_FOR_THIS_RULE(iTeam, iRule)	
		ENDIF
		
		BLOCK_PLAYER_HEALTH_REGEN(IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEight[iRule], ciBS_RULE8_BLOCK_HEALTH_REGEN))
		
		SET_MINIGUN_PLAYER_DAMAGE(TO_FLOAT(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMinigunDamage[iRule])/100)	
		
		IGNORE_NO_GPS_FLAG(IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEight[iRule], ciBS_RULE8_SET_IGNORE_NO_GPS))
		
		REMOVE_REBREATHER_ON_RULE(iTeam, iRule)
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciALLOW_MISSION_AIRSTRIKES)
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetNine[iRule], ciBS_RULE9_AIRSTRIKE_ON_RULE_PROGRESSION)
				IF bIsLocalPlayerHost
					IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetNine[iRule], ciBS_RULE9_AIRSTRIKE_SPECIFIC_COORD)
					AND NOT IS_BIT_SET(iLocalBoolCheck24, LBOOL24_AIRSTRIKE_SAFETY_RANGE_STOP)
						PRINTLN("[RCC MISSION][AIRSTRIKE][OFFSET] Launching Airstrike at specific coord")
						ACTIVATE_AIRSTRIKE_AND_SET_COORDS(DEFAULT, DEFAULT, TRUE)
					ENDIF
				ENDIF
			ENDIF
			
			IF bUseRAGValues
				fRAGAirstrikeMin = TO_FLOAT(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sAirstrikeStruct[iRule].iOffsetDistanceMin)
				fRAGAirstrikeMax = TO_FLOAT(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sAirstrikeStruct[iRule].iOffsetDistanceMax)
				PRINTLN("[RCC MISSION][AIRSTRIKE] Min range: ", fRAGAirstrikeMin)
				PRINTLN("[RCC MISSION][AIRSTRIKE] Max Range: ", fRAGAirstrikeMax)
			ENDIF
			
			PRINTLN("[RCC MISSION][AIRSTRIKE] Moved onto Rule ", iRule)
			PRINTLN("[RCC MISSION][AIRSTRIKE] Target Entity Type: ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sAirstrikeStruct[iRule].iEntityType)
			PRINTLN("[RCC MISSION][AIRSTRIKE] Target Entity ID: ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sAirstrikeStruct[iRule].iEntityID)
		ENDIF
		
		IF g_FMMC_STRUCT.iPerRuleVehicleRockets[iRule][iTeam] != 0
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSix[iRule], ciBS_RULE6_VEHICLE_MENU_WEAPON_ADD_WEAPON1)
				iVehicleRockets += g_FMMC_STRUCT.iPerRuleVehicleRockets[iRule][iTeam]
			ELSE
				iVehicleRockets = g_FMMC_STRUCT.iPerRuleVehicleRockets[iRule][iTeam]
			ENDIF
			SET_BIT(iLocalBoolCheck15, LBOOL15_VEHICLE_MENU_WEAPON_ACTIVE_ROCKETS)
		ENDIF
		
		IF g_FMMC_STRUCT.iPerRuleVehicleBoost[iRule][iTeam] != 0
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSix[iRule], ciBS_RULE6_VEHICLE_MENU_WEAPON_ADD_WEAPON2)
				iVehicleBoost += g_FMMC_STRUCT.iPerRuleVehicleBoost[iRule][iTeam]
			ELSE
				iVehicleBoost = g_FMMC_STRUCT.iPerRuleVehicleBoost[iRule][iTeam]
			ENDIF
			SET_BIT(iLocalBoolCheck15, LBOOL15_VEHICLE_MENU_WEAPON_ACTIVE_BOOST)
		ENDIF
		
		IF g_FMMC_STRUCT.iPerRuleVehicleRepair[iRule][iTeam] != 0
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSix[iRule], ciBS_RULE6_VEHICLE_MENU_WEAPON_ADD_WEAPON3)
				iVehicleRepair += g_FMMC_STRUCT.iPerRuleVehicleRepair[iRule][iTeam]
			ELSE
				iVehicleRepair = g_FMMC_STRUCT.iPerRuleVehicleRepair[iRule][iTeam]
			ENDIF
			SET_BIT(iLocalBoolCheck15, LBOOL15_VEHICLE_MENU_WEAPON_ACTIVE_REPAIR)
		ENDIF
		
		IF g_FMMC_STRUCT.iPerRuleVehicleSpikes[iRule][iTeam] != 0
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSix[iRule], ciBS_RULE6_VEHICLE_MENU_WEAPON_ADD_WEAPON4)
				iVehicleSpikes += g_FMMC_STRUCT.iPerRuleVehicleSpikes[iRule][iTeam]
			ELSE
				iVehicleSpikes = g_FMMC_STRUCT.iPerRuleVehicleSpikes[iRule][iTeam]
			ENDIF
			SET_BIT(iLocalBoolCheck15, LBOOL15_VEHICLE_MENU_WEAPON_ACTIVE_SPIKES)
		ENDIF
		
		IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iShowTickerOnRuleStart[iRule] > -1
			TEXT_LABEL_15 tlCustomStringListTicker = GET_CUSTOM_STRING_LIST_TEXT_LABEL(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iShowTickerOnRuleStart[iRule])
			PRINTLN("[RCC MISSION] PROCESS_LOCAL_PLAYER_NEW_RULE | Printing custom string list ticker on rule start: ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iShowTickerOnRuleStart[iRule], " / ", tlCustomStringListTicker)
			PRINT_TICKER(tlCustomStringListTicker)
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwo[iRule], ciBS_RULE2_SLIPSTREAM)
			SET_ENABLE_VEHICLE_SLIPSTREAMING(TRUE)
			PRINTLN("[RCC MISSION] PROCESS_LOCAL_PLAYER_NEW_RULE - Enabling Slipstreaming")
		ELSE
			SET_ENABLE_VEHICLE_SLIPSTREAMING(FALSE)
			PRINTLN("[RCC MISSION] PROCESS_LOCAL_PLAYER_NEW_RULE - Disabling Slipstreaming")
		ENDIF
		
		IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sFilterControls[iRule].iFilterScoreTarget != 0
			IF iCachedFilterScoreCount = -1
				iCachedFilterScoreCount = GET_FILTER_SCORE_FROM_TEAMS()
				PRINTLN("[RCC MISSION] PROCESS_LOCAL_PLAYER_NEW_RULE - iCachedFilterScoreCount: ", iCachedFilterScoreCount)
			ENDIF
		ELSE
			iCachedFilterScoreCount = -1
		ENDIF
		
		PROCESS_PLAYER_MODEL_SWAP_ON_RULE_CHANGE(iTeam, iRule)
		
		PRINTLN("[RCC MISSION] - iVehicleRockets = ", iVehicleRockets)
		PRINTLN("[RCC MISSION] - iVehicleBoost = ", iVehicleBoost)
		PRINTLN("[RCC MISSION] - iVehicleRepair = ", iVehicleRepair)
		PRINTLN("[RCC MISSION] - iVehicleSpikes = ", iVehicleSpikes)
	ENDIF
ENDPROC

PROC PROCESS_RENDERPHASE_UNPAUSE_WITH_DELAY()
	IF HAS_NET_TIMER_STARTED(tdRenderPhaseUnPauseDelay)
		PRINTLN("[LM][PROCESS_RENDERPHASE_PAUSE_UNPAUSE_WITH_DELAY] - Time Progressed for Pause: ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdRenderPhaseUnPauseDelay))
		
		IF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdRenderPhaseUnPauseDelay, iRenderPhasePauseDelay)
		AND iRenderPhasePauseDelay > -1
			PRINTLN("[LM][PROCESS_RENDERPHASE_PAUSE_UNPAUSE_WITH_DELAY] - Timer Expired. PAUSING THE RENDERPHASE")
			TOGGLE_PAUSED_RENDERPHASES(FALSE)
		ENDIF
		
		IF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdRenderPhaseUnPauseDelay, iRenderPhaseUnPauseDelay)
			PRINTLN("[LM][PROCESS_RENDERPHASE_PAUSE_UNPAUSE_WITH_DELAY] - Timer Expired. UNPAUSING / RESETTING THE RENDERPHASE")
			RESET_NET_TIMER(tdRenderPhaseUnPauseDelay)
			RESET_PAUSED_RENDERPHASES()
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_MIDPOINT_BLOCKING()
	INT iTeam = MC_PlayerBD[iLocalPart].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	BOOL bResetVehicleSpeed = FALSE
	
	IF iRule < FMMC_MAX_RULES
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSix[iRule], ciBS_RULE6_DROP_OFF_DISABLE_WEAPONS)
		OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSix[iRule], ciBS_RULE6_DROP_OFF_DISABLE_SPRINT)
		OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSeven[iRule], ciBS_RULE7_DROP_OFF_DISABLE_JUMPING)
		OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwelve[iRule], ciBS_RULE12_DROP_OFF_VEHICLE_SLOW_DOWN)
			IF MC_playerBD[iLocalPart].iObjCarryCount > 0
			OR MC_playerBD[iLocalPart].iVehCarryCount > 0
			OR MC_playerBD[iLocalPart].iPedCarryCount > 0
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwelve[iRule], ciBS_RULE12_DROP_OFF_VEHICLE_SLOW_DOWN)
					SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(LocalPlayer, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].fDropOffVehicleDrag[iRule])
					bAppliedVehicleDrag = TRUE
					PRINTLN("[PROCESS_MIDPOINT_BLOCKING] Setting AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE by ",g_FMMC_STRUCT.sFMMCEndConditions[iTeam].fDropOffVehicleDrag[iRule])
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSix[iRule], ciBS_RULE6_DROP_OFF_DISABLE_WEAPONS)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)	
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_NEXT_WEAPON)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_PREV_WEAPON)
					
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciENABLE_RUGBY_SOUNDS)
						IF NOT IS_BIT_SET(iLocalBoolCheck16, LBOOL16_HAS_SENT_RUGBY_PICKUP_SOUND_EVENT)
						AND NOT IS_POINT_IN_ANGLED_AREA(GET_ENTITY_COORDS(LocalPlayerPed), g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vDropOff[iRule], g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vDropOff2[iRule], g_FMMC_STRUCT.sFMMCEndConditions[iTeam].fDropOffRadius[iRule]) 
							SET_BIT(iLocalBoolCheck16, LBOOL16_HAS_SENT_RUGBY_PICKUP_SOUND_EVENT)
							BROADCAST_RUGBY_PLAY_SOUND(ciRUGBY_SOUND_COLLECTED_BALL)
						ENDIF
						IF NOT IS_BIT_SET(iLocalBoolCheck16, LBOOL16_HAS_STARTED_BALL_CARRIER_LOOP_SOUND)
							SET_BIT(iLocalBoolCheck16, LBOOL16_HAS_STARTED_BALL_CARRIER_LOOP_SOUND)
							
							IF iRugbyCarryingBallSoundID = -1
								iRugbyCarryingBallSoundID = GET_SOUND_ID()
							ENDIF
							
							PLAY_SOUND_FRONTEND(iRugbyCarryingBallSoundID, "Carrying", "DLC_Low2_Ibi_Sounds", FALSE)
						ENDIF
					ENDIF
					
					IF NOT IS_BIT_SET(iLocalBoolCheck16, LBOOL16_WEAPON_PICKUPS_BLOCKED)
						SET_PLAYER_CAN_DO_DRIVE_BY(LocalPlayer, FALSE) 
						SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_BlockAutoSwapOnWeaponPickups, TRUE)
						SET_BIT(iLocalBoolCheck16, LBOOL16_WEAPON_PICKUPS_BLOCKED)
						PRINTLN("[JS] - PROCESS_MIDPOINT_BLOCKING - PCF_BlockAutoSwapOnWeaponPickups - TRUE")
					ENDIF
					
					WEAPON_TYPE currentWeaponType
					IF GET_CURRENT_PED_WEAPON(LocalPlayerPed, currentWeaponType)
						IF (currentWeaponType != WEAPONTYPE_UNARMED)
						AND !bHasObjectJustBeenDropped
							wtRugbyRespawnWeapon = currentWeaponType
							SET_CURRENT_PED_WEAPON(LocalPlayerPed, WEAPONTYPE_UNARMED, TRUE)
							PRINTLN("[JS] - PROCESS_MIDPOINT_BLOCKING - Setting current weapon to unarmed")
						ENDIF
					ENDIF
					
					IF GET_CURRENT_PED_VEHICLE_WEAPON(LocalPlayerPed, currentWeaponType)
						IF (currentWeaponType != WEAPONTYPE_UNARMED)
							wtRugbyRespawnWeapon = currentWeaponType
							SET_CURRENT_PED_VEHICLE_WEAPON(LocalPlayerPed, WEAPONTYPE_UNARMED)
							SET_CURRENT_PED_WEAPON(LocalPlayerPed, WEAPONTYPE_UNARMED, TRUE)
							PRINTLN("[JS] - PROCESS_MIDPOINT_BLOCKING - Setting current vehicle weapon to unarmed")
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSix[iRule], ciBS_RULE6_DROP_OFF_DISABLE_SPRINT)
					SET_PED_MAX_MOVE_BLEND_RATIO(LocalPlayerPed, PEDMOVEBLENDRATIO_RUN)
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSeven[iRule], ciBS_RULE7_DROP_OFF_DISABLE_JUMPING)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
				ENDIF

			ELSE
				IF IS_BIT_SET(iLocalBoolCheck16, LBOOL16_WEAPON_PICKUPS_BLOCKED)
					SET_PLAYER_CAN_DO_DRIVE_BY(LocalPlayer, TRUE) 
					SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_BlockAutoSwapOnWeaponPickups, FALSE)
					CLEAR_BIT(iLocalBoolCheck16, LBOOL16_WEAPON_PICKUPS_BLOCKED)
					PRINTLN("[JS] - PROCESS_MIDPOINT_BLOCKING - PCF_BlockAutoSwapOnWeaponPickups - FALSE")
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciENABLE_RUGBY_SOUNDS)
					IF IS_BIT_SET(iLocalBoolCheck16, LBOOL16_HAS_SENT_RUGBY_PICKUP_SOUND_EVENT)
						CLEAR_BIT(iLocalBoolCheck16, LBOOL16_HAS_SENT_RUGBY_PICKUP_SOUND_EVENT)
					ENDIF
					
					IF IS_BIT_SET(iLocalBoolCheck16, LBOOL16_HAS_STARTED_BALL_CARRIER_LOOP_SOUND)
						CLEAR_BIT(iLocalBoolCheck16, LBOOL16_HAS_STARTED_BALL_CARRIER_LOOP_SOUND)
						STOP_SOUND(iRugbyCarryingBallSoundID)
					ENDIF
				ENDIF

				bHasObjectJustBeenDropped = FALSE
				
				bResetVehicleSpeed = TRUE
			ENDIF
		ELSE
			IF IS_BIT_SET(iLocalBoolCheck16, LBOOL16_WEAPON_PICKUPS_BLOCKED)
				SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_BlockAutoSwapOnWeaponPickups, FALSE)
				CLEAR_BIT(iLocalBoolCheck16, LBOOL16_WEAPON_PICKUPS_BLOCKED)
				PRINTLN("[JS] - PROCESS_MIDPOINT_BLOCKING - PCF_BlockAutoSwapOnWeaponPickups - FALSE")
			ENDIF
			bResetVehicleSpeed = TRUE
		ENDIF
		
		IF bResetVehicleSpeed
		AND bAppliedVehicleDrag
			SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(LocalPlayer,1.0)
			bAppliedVehicleDrag = FALSE
			PRINTLN("[PROCESS_MIDPOINT_BLOCKING] Reseting AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE")
		ENDIF
	ENDIF
	
ENDPROC

PROC SET_TEAMS_AS_PASSIVE()

	IF g_FMMC_STRUCT.iBS_MissionTeamPassive = 0
		EXIT
	ENDIF
	
	INT iPlayerLoop
	PED_INDEX tempPlayerPed
	PLAYER_INDEX tempPlayer
	PRINTLN("[PLAYER_LOOP] - SET_TEAMS_AS_PASSIVE")
	FOR iPlayerLoop = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		tempPlayer = INT_TO_NATIVE(PLAYER_INDEX, iPlayerLoop)
		IF NETWORK_IS_PLAYER_ACTIVE(tempPlayer)
		AND SHOULD_TEAM_BE_PASSIVE(MC_playerBD[iPlayerLoop].iteam)
			tempPlayerPed = GET_PLAYER_PED(tempPlayer)
			IF NOT IS_PED_INJURED(tempPlayerPed)
				IF NOT IS_BIT_SET(iLocalGhostRemotePlayerBitSet, iPlayerLoop)
					SET_REMOTE_PLAYER_AS_GHOST(tempPlayer, TRUE)
					PRINTLN("[RCC MISSION] SET_TEAMS_AS_PASSIVE - Setting player ", iPlayerLoop, " as ghosted")
					SET_BIT(iLocalGhostRemotePlayerBitSet, iPlayerLoop)
				ENDIF
			ELSE
				CLEAR_BIT(iLocalGhostRemotePlayerBitSet, iPlayerLoop)
				PRINTLN("[RCC MISSION] SET_TEAMS_AS_PASSIVE - Clearing bit ", iPlayerLoop, " as player died")
			ENDIF
		ENDIF
	ENDFOR
	
ENDPROC

PROC SET_SPAWN_VEHICLE_STARTING_VELOCITY_BACKUP()
	IF NOT IS_BIT_SET(iLocalBoolCheck5, LBOOL5_GIVEN_SPAWN_VEHICLE_STARTING_VELOCITY) // This is here as a backup, so players aren't left frozen in their spawn vehicles:
		IF IS_BIT_SET(sSetUpStartPosition.iLocalBoolInitBS, BS_INIT_START_SPAWN_IN_PLACED_VEHICLE)
		AND NOT (bIsSCTV OR DID_I_JOIN_MISSION_AS_SPECTATOR())
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] GAME_STATE_RUNNING call to UNFREEZE_AND_PUNT_PLAYER_SPAWN_VEHICLE_FORWARDS!")
			IF IS_PED_IN_ANY_HELI(localPlayerPed)
				UNFREEZE_AND_PUNT_PLAYER_SPAWN_VEHICLE_FORWARDS(1.0)
				PRINTLN("[RCC MISSION] GAME_STATE_RUNNING - Calling SET_VEHICLE_FORWARD_SPEED with 1.0")
			ELSE
				UNFREEZE_AND_PUNT_PLAYER_SPAWN_VEHICLE_FORWARDS(60.0)
			ENDIF
		ENDIF
		
		SET_BIT(iLocalBoolCheck5, LBOOL5_GIVEN_SPAWN_VEHICLE_STARTING_VELOCITY)
	ENDIF
ENDPROC

FUNC INT GET_FORCED_CAMERA_MODE_THIS_FRAME()

	IF sCashGrabData.eState != HBGS_NONE
		RETURN ciCAM_LOCK_THIRD
	ENDIF
	
	RETURN g_FMMC_STRUCT.iFixedCamera
ENDFUNC

PROC SET_MC_PED_AND_VEHILE_DENSITY_MULTIPLIIERS()	
	IF GET_PED_DENSITY_FROM_INT(g_FMMC_STRUCT.iPedDensity) = 0
		SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
	ENDIF
	IF GET_MISSION_VEHICLE_DENSITY_FROM_INT(g_FMMC_STRUCT.iTraffic) = 0		
		SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
	ENDIF
ENDPROC

PROC PROCESS_MC_CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(INT iTeam, INT iRule)
	
	IF NOT bIsAnySpectator
		IF iTeam < FMMC_MAX_TEAMS
		
			IF iRule < FMMC_MAX_RULES
		
				IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sDrainStruct[iRule].iMissedShotPerc > 0
					
					IF bLocalPlayerPedOK
						WEAPON_TYPE wtWeap
						IF GET_CURRENT_PED_WEAPON(localPlayerPed, wtWeap)
							
							IF HAS_PLAYER_DAMAGED_AT_LEAST_ONE_PED(LocalPlayer)
								RESET_NET_TIMER(td_MissedShotTakeDamageTimer)
								CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(LocalPlayer)
								PRINTLN("[LM][iMissedShotPerc] Hit someone, reset timer (2)")
							ENDIF
							
							IF HAS_NET_TIMER_STARTED(td_MissedShotTakeDamageTimer)
							AND HAS_NET_TIMER_EXPIRED_READ_ONLY(td_MissedShotTakeDamageTimer, (ROUND(GET_WEAPON_TIME_BETWEEN_SHOTS(wtWeap)*1000)))
								
								INT iHealth = GET_ENTITY_HEALTH(localPlayerPed)
								INT iNewHealth = ROUND((TO_FLOAT((GET_PED_MAX_HEALTH(localPlayerPed)) / 100 ) * g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sDrainStruct[iRule].iMissedShotPerc))
								
								PRINTLN("[LM][iMissedShotPerc] Timer Expired: iHealth = ", iHealth, " iNewHealth = ", iNewHealth, " EndHealth = ", (iHealth - iNewHealth))

								iHealth -= iNewHealth
								
								SET_ENTITY_HEALTH(localPlayerPed, iHealth)
								
								RESET_NET_TIMER(td_MissedShotTakeDamageTimer)
							ENDIF
							
							IF IS_PED_SHOOTING(localPlayerPed)
								IF NOT HAS_NET_TIMER_STARTED(td_MissedShotTakeDamageTimer)
									START_NET_TIMER(td_MissedShotTakeDamageTimer)
									CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(LocalPlayer)
									PRINTLN("[LM][iMissedShotPerc] Fired Weapon: GET_WEAPON_TIME_BETWEEN_SHOTS(wtWeap) = ", (GET_WEAPON_TIME_BETWEEN_SHOTS(wtWeap)*1000))								
								ENDIF
							ENDIF							
						ENDIF
					ENDIF
				ENDIF
			ENDIF	
		ENDIF
	ENDIF
ENDPROC

// Some logic ripped out of PROCESS_SET_FIRE_TO_THE_LOCAL_PLAYER to be processeed every frame instead of staggered as the fire can sometimes kill the local player before we process the section that calls KILL_LOCAL_PLAYER_WITH_PLAYER_INDEX_AS_INSTIGATOR.
// Note: This function is to fix "url:bugstar:7811336". Ideally we might want to convert PROCESS_SET_FIRE_TO_THE_LOCAL_PLAYER to be processed every frame but it's too dangerous of a fix for the last couple of days of the pack. Investigating in: "url:bugstar:7831856"
PROC PROCESS_KILL_LOCAL_PLAYER_FROM_FIRE()
	
	IF NOT IS_NET_PLAYER_OK(localPlayer)
	OR IS_PED_INJURED(localPlayerPed)
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_PLAYER_SET_ON_FIRE)
		EXIT
	ENDIF
			
	//Kill the player when they are low on health so the other player gets a kill
	IF GET_PED_HEALTH_PERCENTAGE(localPlayerPed) >= 10
		EXIT
	ENDIF
		
	PRINTLN("[SET FIRE] PROCESS_KILL_LOCAL_PLAYER_FROM_FIRE - Stop entity fire")
	STOP_ENTITY_FIRE(LocalPlayerPed)
	RESET_NET_TIMER(tdCatchFireDelay)
	RESET_NET_TIMER(tdStopFireDelay)
	CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_PLAYER_SET_ON_FIRE)
	
	INT iRule = GET_TEAM_CURRENT_RULE(MC_playerBD[iLocalPart].iTeam)	
	IF iRule >= FMMC_MAX_RULES
		EXIT
	ENDIF
	
	PLAYER_INDEX piPlayerDamager
		
	FLOAT fCatchFireDistance = g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].fCatchFireDistance[iRule]
	INT iTeamToCheck
	FOR iTeamToCheck = 0 TO FMMC_MAX_TEAMS - 1
		IF iTeamToCheck = MC_playerBD[iLocalPart].iTeam
			RELOOP
		ENDIF
		INT iPlayerIndex = GET_NEAREST_PLAYER_TO_ENTITY_ON_TEAM(localPlayerPed, iTeamToCheck)
		IF iPlayerIndex < 0
			RELOOP
		ENDIF
		
		PLAYER_INDEX piTempPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPlayerIndex))
		
		IF IS_NET_PLAYER_OK(piTempPlayer) AND GET_DISTANCE_BETWEEN_PLAYERS(LocalPlayer, piTempPlayer) < fCatchFireDistance
			piPlayerDamager = piTempPlayer	
		ENDIF
	ENDFOR
	
	IF IS_ENTITY_ALIVE(GET_PLAYER_PED(piPlayerDamager))		
		PRINTLN("[SET FIRE] PROCESS_KILL_LOCAL_PLAYER_FROM_FIRE - Apply damage to the local player with an instigator index")		
		KILL_LOCAL_PLAYER_WITH_PLAYER_INDEX_AS_INSTIGATOR(piPlayerDamager)
		
	ELSE //No Instigator Index		
		PRINTLN("[SET FIRE] PROCESS_KILL_LOCAL_PLAYER_FROM_FIRE - Apply damage to the local player without an instigator index")		
		SET_ENTITY_HEALTH(LocalPlayerPed, 0)
		
	ENDIF
	
ENDPROC

PROC PROCESS_MUZZLE_AND_BLOOD_VFX_OVERRIDES(INT &iRuleBitsetSixteen)
	//Trigger Once On Rule Change
	IF NOT DOES_LOCAL_PLAYER_HAVE_NEW_PRIORITY_THIS_FRAME()
		EXIT
	ENDIF
	
	//Load scr_rcbarry1 PTFX asset when needed
	IF (IS_BIT_SET(iRuleBitsetSixteen, ciBS_RULE16_USE_ALIEN_BLOOD_VFX) OR IS_BIT_SET(iRuleBitsetSixteen, ciBS_RULE16_PED_SPAWN_ALIEN_DEATH_PTFX))
		REQUEST_NAMED_PTFX_ASSET("scr_rcbarry1")
	ENDIF
	
	//Load scr_rcbarry2 PTFX asset when needed
	IF (IS_BIT_SET(iRuleBitsetSixteen, ciBS_RULE16_USE_CLOWN_BLOOD_VFX) OR IS_BIT_SET(iRuleBitsetSixteen, ciBS_RULE16_PED_SPAWN_CLOWN_DEATH_PTFX))
		REQUEST_NAMED_PTFX_ASSET("scr_rcbarry2")
	ENDIF
	
	//Enable Clown Blood VFX
	IF IS_BIT_SET(iRuleBitsetSixteen, ciBS_RULE16_USE_CLOWN_BLOOD_VFX)
		ENABLE_CLOWN_BLOOD_VFX(TRUE)		
		SET_BIT(iLocalBoolCheck35, LBOOL35_USING_CLOWN_BLOOD_VFX)
	//Disable Clown Blood VFX	
	ELIF IS_BIT_SET(iLocalBoolCheck35, LBOOL35_USING_CLOWN_BLOOD_VFX)
		ENABLE_CLOWN_BLOOD_VFX(FALSE)
		//Remove scr_rcbarry2 PTFX asset when it's not needed anymore
		IF NOT IS_BIT_SET(iRuleBitsetSixteen, ciBS_RULE16_USE_CLOWN_BLOOD_VFX) 
		AND NOT IS_BIT_SET(iRuleBitsetSixteen, ciBS_RULE16_PED_SPAWN_CLOWN_DEATH_PTFX)
			REMOVE_NAMED_PTFX_ASSET("scr_rcbarry2")
		ENDIF
		CLEAR_BIT(iLocalBoolCheck35, LBOOL35_USING_CLOWN_BLOOD_VFX)
	ENDIF
	
	//Enable Alien Blood VFX
	IF IS_BIT_SET(iRuleBitsetSixteen, ciBS_RULE16_USE_ALIEN_BLOOD_VFX)
		ENABLE_ALIEN_BLOOD_VFX(TRUE)
		SET_BIT(iLocalBoolCheck35, LBOOL35_USING_ALIEN_BLOOD_VFX)
	//Disable Alien Blood VFX	
	ELIF IS_BIT_SET(iLocalBoolCheck35, LBOOL35_USING_ALIEN_BLOOD_VFX)
		ENABLE_ALIEN_BLOOD_VFX(FALSE)
		//Remove scr_rcbarry1 PTFX asset when it's not needed anymore
		IF NOT IS_BIT_SET(iRuleBitsetSixteen, ciBS_RULE16_USE_ALIEN_BLOOD_VFX) 
		AND NOT IS_BIT_SET(iRuleBitsetSixteen, ciBS_RULE16_PED_SPAWN_ALIEN_DEATH_PTFX)
		AND NOT IS_ENTITY_PTFX_TYPE_IN_USE(FMMC_ENTITY_PTFX_SPARKS)
			REMOVE_NAMED_PTFX_ASSET("scr_rcbarry1")
		ENDIF
		CLEAR_BIT(iLocalBoolCheck35, LBOOL35_USING_ALIEN_BLOOD_VFX)
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_AUDIO_BANK_LOADING_FOR_TEAM_PED_DEATH_EFFECTS(INT &iRuleBitsetSixteen)
	//Trigger Once On Rule Change
	IF NOT DOES_LOCAL_PLAYER_HAVE_NEW_PRIORITY_THIS_FRAME()
		EXIT
	ENDIF
	
	IF IS_BIT_SET(iRuleBitsetSixteen, ciBS_RULE16_PED_PLAY_CLOWN_DEATH_AUDIO)
		REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\BARRY_02_CLOWN_A")
		REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\BARRY_02_CLOWN_B")
		REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\BARRY_02_CLOWN_C")
		SET_BIT(iLocalBoolCheck35, LBOOL35_USING_CLOWN_DEATH_AUDIO)
	ELIF IS_BIT_SET(iLocalBoolCheck35, LBOOL35_USING_CLOWN_DEATH_AUDIO)
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("SCRIPT\\BARRY_02_CLOWN_A")
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("SCRIPT\\BARRY_02_CLOWN_B")
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("SCRIPT\\BARRY_02_CLOWN_C")
		CLEAR_BIT(iLocalBoolCheck35, LBOOL35_USING_CLOWN_DEATH_AUDIO)
	ENDIF
ENDPROC

PROC PROCESS_APPLY_SERVER_CACHED_PLAYER_HEAD_BLEND_DATA_TO_PLAYER_PED_CLONES()
	IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet5, PBBOOL5_APPLY_SERVER_CACHED_CLONE_HEAD_BLEND)
	OR MC_serverBD_4.sPedCloningData.iNumOfEntsToClone = 0
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD BOOL bClonedAllEnts = TRUE #ENDIF

	
	INT i
	REPEAT MC_serverBD_4.sPedCloningData.iNumOfEntsToClone i
		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_4.sPedCloningData.niTarget[i])
			PRINTLN("[PlayerPedClone] PROCESS_APPLY_SERVER_CACHED_PLAYER_HEAD_BLEND_DATA_TO_PLAYER_PED_CLONES - Index ", i, " - Net ID for niTarget deosn't exist! Exiting the function!")
			EXIT
		ENDIF
		
		PED_INDEX piTarget = NET_TO_PED(MC_serverBD_4.sPedCloningData.niTarget[i])
		
		IF NOT DOES_ENTITY_EXIST(piTarget) OR NOT DOES_ENTITY_EXIST(GET_PLAYER_PED(MC_serverBD_4.sPedCloningData.playerSource[i]))
			PRINTLN("[PlayerPedClone] PROCESS_APPLY_SERVER_CACHED_PLAYER_HEAD_BLEND_DATA_TO_PLAYER_PED_CLONES - Index ", i, " - Ent deosn't exist! Relooping!")
			#IF IS_DEBUG_BUILD bClonedAllEnts = FALSE #ENDIF
			RELOOP
		ENDIF
		
		
		PRINTLN("[PlayerPedClone] PROCESS_APPLY_SERVER_CACHED_PLAYER_HEAD_BLEND_DATA_TO_PLAYER_PED_CLONES - Index ", i, " -  Applying the cached head blend. Target - SCRIPT_PED_", NETWORK_ENTITY_GET_OBJECT_ID(piTarget), ", Source - ", GET_PLAYER_NAME(MC_serverBD_4.sPedCloningData.playerSource[i]))
		NETWORK_APPLY_CACHED_PLAYER_HEAD_BLEND_DATA(piTarget, MC_serverBD_4.sPedCloningData.playerSource[i])
	ENDREPEAT
	
	CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet5, PBBOOL5_APPLY_SERVER_CACHED_CLONE_HEAD_BLEND)
	PRINTLN("[PlayerPedClone] PROCESS_APPLY_SERVER_CACHED_PLAYER_HEAD_BLEND_DATA_TO_PLAYER_PED_CLONES - Process completed! bClonedAllEnts = ", BOOL_TO_STRING(bClonedAllEnts), ". Clearing PBBOOL5_APPLY_SERVER_CACHED_CLONE_HEAD_BLEND flag!")
ENDPROC

PROC PROCESS_RUNNING_AROUND_ON_ALL_FOURS_ANIMAL_STYLE()
	
	IF NOT GET_IS_PLAYER_IN_ANIMAL_FORM()
		EXIT
	ENDIF
	
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_LR)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_NEXT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_PREV)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_UD)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_NEXT_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_PREV_WEAPON)
	
ENDPROC

PROC PROCESS_LOCAL_PlAYER_INFINITE_STAMINA(INT iTeam, INT iRule)	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSixteen[iRule], ciBS_RULE16_PLAYER_INFINITE_STAMINA)
		EXIT
	ENDIF
	
	RESET_PLAYER_STAMINA(LocalPlayer)
	
ENDPROC

PROC PROCESS_LOCAL_INVINCIBILITY_OPTIONS(INT iTeam, INT iRule)
	//Invincible while falling
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSixteen[iRule], ciBS_RULE16_PLAYER_INVINCIBLE_WHILE_FALLING)
		IF IS_PED_FALLING(LocalPlayerPed)
			SET_PLAYER_INVINCIBLE(localplayer, TRUE) //Resets after each frame
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_HANGING_UP_PHONE_AFTER_PHOTO()
	
	IF NOT HAS_NET_TIMER_STARTED_AND_EXPIRED(tdPhotoTakenTimer, ciFMMC_PUT_AWAY_PHONE_TIMER)
		EXIT
	ENDIF
	   
	PRINTLN("[PHOTO] PROCESS_HANGING_UP_PHONE_AFTER_PHOTO - Putting away phone")
	RESET_NET_TIMER(tdPhotoTakenTimer)
	HANG_UP_AND_PUT_AWAY_PHONE(FALSE)
	
ENDPROC

PROC PROCESS_LOCAL_PLAYER_RULE_SPECIFIC_OPTIONS(INT iTeam, INT iRule)
	IF NOT IS_TEAM_INDEX_VALID(iTeam)
	OR NOT IS_RULE_INDEX_VALID(iRule)
		EXIT
	ENDIF
	
	PROCESS_LIGHTNING_STRIKE(iTeam, iRule)
	PROCESS_MUZZLE_AND_BLOOD_VFX_OVERRIDES(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSixteen[iRule])
	PROCESS_SCRIPT_AUDIO_BANK_LOADING_FOR_TEAM_PED_DEATH_EFFECTS(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSixteen[iRule])
	PROCESS_REACT_TO_SMOKE(iTeam, iRule)
	PROCESS_LOCAL_PLAYER_INFINITE_STAMINA(iTeam, iRule)
	PROCESS_PLAYERS_WAKE_UP(iTeam, iRule)
	PROCESS_LOCAL_INVINCIBILITY_OPTIONS(iTeam, iRule)
ENDPROC

// FMMC2020 - Has stuff that can be taken out into another version that is in _Players. Can also take stuff out into a host only function.
// // [FIX_2020_CONTROLLER] - Loose Logic could be functionalised so that it's easier to read execution order of logic. (I started doing this  about 100 lines down.)
PROC PROCESS_EVERY_FRAME_LOCAL_PLAYER_CHECKS()
	INT iTeam = MC_PlayerBD[iLocalPart].iTeam
	INT iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]
	
	PROCESS_LOCAL_PLAYER_SPAWNING()
		
	PROCESS_RACE_BOOST_START()
	
	PROCESS_FORCE_SNIPER_SCOPE_AIM()
	
	PROCESS_LOCAL_PLAYER_WANTED()
	
	IF IS_BIT_SET(iLocalBoolCheck16, LBOOL16_CHANGE_PLAYER_TEAM_ALIVE_VARS_SETTER)
		SET_PLAYER_TEAM_SWAP_DATA(iTeam)
		IF iRule < FMMC_MAX_RULES
			PROCESS_MY_PLAYER_BLIP_VISIBILITY(iTeam, iRule)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(MC_serverbd.iServerBitSet, SBBOOL_FIRST_UPDATE_DONE)
	AND MC_playerBD_1[iLocalPart].iCachedPartNumInTeam = -1
	AND bLocalPlayerOK
		MC_playerBD_1[iLocalPart].iCachedPartNumInTeam = GET_PARTICIPANT_NUMBER_IN_TEAM()
	ENDIF
	
	SET_SPAWN_VEHICLE_STARTING_VELOCITY_BACKUP()			
				
	PROCESS_HEIST_SPECTATE(g_BossSpecData)
	
	BLOCK_VEHICLE_REWARD_WEAPONS_DURING_LOCKED_CAPTURE()
	
	SET_MC_PED_AND_VEHILE_DENSITY_MULTIPLIIERS()	
	
	IF IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)					
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitSet3, ciBS3_ENABLE_MULTIPLE_PLAYER_SAME_VEHICLE_RESPAWN)
			SET_ON_TEAM_RACE_RACE_GLOBAL(FALSE)
		ENDIF
	ENDIF
		
	CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_HEIST_I_AM_INSIDE_ANY_MP_PROPERTY)
	IF bLocalPlayerOK
		IF IS_PLAYER_IN_MP_PROPERTY(LocalPlayer, FALSE)
			SET_BIT(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_HEIST_I_AM_INSIDE_ANY_MP_PROPERTY)
		ENDIF
	ENDIF
	
	PROCESS_LOCAL_PLAYER_NEW_RULE(iTeam, iRule)
		
	IF iRule < FMMC_MAX_RULES
	AND iTeam > -1
		PROCESS_EXTRA_VEHICLE_COLLISION_DAMAGE(timerExtraVehDamageForceHitting, ciTimeExtraVehDamageToBeHittingAgain) // Might only need this temp until code solution. Bug: -- url:bugstar:5338680 - [SCRIPT WORKAROUND] Vamos2 - The weapons on this vehicle don't seem to do extra damage to other vehicles
	ENDIF
	
	BLOCK_INVALID_CLIMBABLE_AREAS_IN_OLD_INTERIORS()
			
	PROCESS_PLAYER_FORCED_SEAT_PREF_SPECIAL()
			
	PROCESS_DISABLE_HEADLIGHT_CONTROL()
	
	IF NOT SHOULD_HIDE_THE_HUD_THIS_FRAME()
		PROCESS_VEHICLE_WEAPON_FAKE_AMMO(fVehicleWeaponShootingTime, fVehicleWeaponTimeSinceLastfired, bRegenVehicleWeapons, iFakeAmmoRechargeSoundID)
	ENDIF

	IF NOT bLocalPlayerPedOK
	OR NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		IF IS_BIT_SET(iLocalBoolCheck25, LBOOL25_BLOCK_MANUAL_RESPAWN_DUE_TO_VEHICLE)
			PRINTLN("[LM][MANUAL RESPAWN] - Not in a vehicle anymore, clearing: LBOOL25_BLOCK_MANUAL_RESPAWN_DUE_TO_VEHICLE")
			CLEAR_BIT(iLocalBoolCheck25, LBOOL25_BLOCK_MANUAL_RESPAWN_DUE_TO_VEHICLE)
		ENDIF
	ENDIF
	
	IF MC_PlayerBD[iPartToUse].iAssists != g_iMyAssists
		MC_PlayerBD[iPartToUse].iAssists = g_iMyAssists
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciHIDE_AIM_RETICLE)
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_RETICLE)
	ENDIF
		
	PROCESS_VEHICLES_FLIPPED_BY_DUNE()
	
	SKIP_SKIPPABLE_HELP_TEXT()
	
	PROCESS_HEIST_OUTFIT_BEHAVIOURS()
	
	PROCESS_WEAPON_AND_LOADOUTS()
	
	PROCESS_PLAYER_MISSION_PARACHUTES()
			
	PROCESS_RULE_WEAPON_DISABLING()
	
	PROCESS_MID_GAME_PLAYER_MODEL_SWAPS()
	
	IF NOT g_bMissionEnding
	AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
	AND NOT HAS_TEAM_FINISHED(MC_playerBD[iPartToUse].iteam)
	AND NOT HAS_TEAM_FAILED(MC_playerBD[iPartToUse].iteam)
	AND bPedToUseOk
		PROCESS_RULE_OUTFITS()
	ENDIF
		
	// Update the Vehicle, then update the health, then update the weapons.
	IF NOT bIsAnySpectator
		PROCESS_SWAP_VEHICLE_ON_RULE()
		PROCESS_SWAP_VEHICLE_ON_WARP()
		PROCESS_UPDATE_VEHICLE_HEALTH_ON_RULE()
		PROCESS_UPDATE_SPECIAL_VEHICLE_WEAPONS()
		PROCESS_CURRENT_RUINER_ROCKET_AMMO()
	ENDIF
	
	IF IS_TEAM_COOP_MISSION()
		UPDATE_PC_TEXT_CHAT_COLOUR(g_FMMC_STRUCT.iTeamColourOverride[MC_playerBD[iPartToUse].iTeam],IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciDROP_ZONE_TEAM_COLOURS))
	ENDIF
		
	IF IS_BIT_SET(g_FMMC_Struct.iTeamVehicleBitSet, ciFMMC_TeamVehBS_DISABLE_TEAM_RESPAWN_VEHICLE_SPACE_ROCKETS)
		IF NOT IS_PED_DEAD_OR_DYING(LocalPlayerPed)
			IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
				VEHICLE_INDEX viPlayerVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
				IF GET_ENTITY_MODEL(viPlayerVeh) = BUZZARD
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_NEXT_WEAPON)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_PREV_WEAPON)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_SELECT_NEXT_WEAPON)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_SELECT_PREV_WEAPON)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_UD)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_LR)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	PROCESS_MC_CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(iTeam, iRule)
	
	IF iRule < FMMC_MAX_RULES
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEight[iRule], ciBS_RULE8_SHOW_YACHT_BLIP_THIS_RULE)
						
			// via SET_UP_IPL_SWAP_FOR_MISSION_CONTROLLER
			YACHT_DATA YachtData    
			YachtData.iLocation = 34            // Paleto Bay
			YachtData.Appearance.iOption = 2    // The Aquaris
			YachtData.Appearance.iTint = 12     // Vintage
			YachtData.Appearance.iLighting = 5 // Blue Vivacious
			YachtData.Appearance.iRailing = 1   // Gold Fittings
			YachtData.Appearance.iFlag = -1     // no flag 
			UPDATE_YACHT_FOR_MISSION_CREATOR(YachtData)			
		ENDIF
	ENDIF
		
	IF IS_BIT_SET( iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam)
		IF IS_BIT_SET(iLocalBoolCheck24, LBOOL24_BLOCK_VEHICLE_KERS_ON_RULE_CHECK)
			CLEAR_BIT(iLocalBoolCheck24, LBOOL24_BLOCK_VEHICLE_KERS_ON_RULE_CHECK)
			PRINTLN("[RCC MISSION] Clearing Bit: LBOOL24_BLOCK_VEHICLE_KERS_ON_RULE_CHECK")
		ENDIF
	ENDIF
	
	IF NOT bIsAnySpectator	
		PROCESS_RUINER_PARACHUTE()
	ENDIF
	
	// Cache the starting point.
	INT iTeamBS = MC_playerBD[iPartToUse].iteam
	INT iRuleBS = MC_serverBD_4.iCurrentHighestPriority[iTeamBS]
	
	IF iRuleBS < FMMC_MAX_RULES
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeamBS].iRuleBitsetEight[iRuleBS], ciBS_RULE8_GPS_TRACKING_DISTANCE_METER)
			IF IS_BIT_SET(iLocalBoolCheck6, (LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + MC_PlayerBD[iPartToUse].iteam))
			
			//end rule = current rule
				PRINTLN("[LM][MAINTAIN_GPS_DISTANCE_METER_BAR] - New Rule.")
				INT i = 0
				FOR i = 0 TO ci_TOTAL_DISTANCE_CHECK_BARS-1
					iGPSTrackingMeterPedIndex[i] = -1
					vGPSTrackingMeterStart[i] = <<0.0, 0.0, 0.0>>
					vGPSTrackingMeterEnd[i] = <<0.0, 0.0, 0.0>>
					iGPSTrackingMeterDistCache[i] = 0
					iGPSTrackingMeterRegisteredPedBitset[0] = 0
					iGPSTrackingMeterRegisteredPedBitset[1] = 0
				ENDFOR
				
				RESET_NET_TIMER(tdWaitForPedRuleSpawn)
				START_NET_TIMER(tdWaitForPedRuleSpawn)
			ENDIF
			IF HAS_NET_TIMER_STARTED(tdWaitForPedRuleSpawn)
				IF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdWaitForPedRuleSpawn, ci_WAIT_FOR_PED_RULE_SPAWN)
					RESET_NET_TIMER(tdWaitForPedRuleSpawn)
					SET_BIT(iLocalBoolCheck19, LBOOL19_CACHE_PLAYER_POS_FOR_GPS_TRACKING)
				ENDIF
			ENDIF
			IF IS_BIT_SET(iLocalBoolCheck19, LBOOL19_CACHE_PLAYER_POS_FOR_GPS_TRACKING)
				INT i = 0
				INT ii = 0
				FOR i = 0 TO (MC_serverBD.iNumPedCreated-1)
					
					INT iIndex = GET_FREE_INDEX_FOR_DISTANCE_TRACKING_BAR()
					
					IF iIndex > -1
						IF DOES_PED_REQUIRE_DISTANCE_TRACKING(i, iRuleBS)
							PRINTLN("[LM][MAINTAIN_GPS_DISTANCE_METER_BAR] - iPed: ", i, " Is involved with this teams Rule.")
							
							IF NOT IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[i])
								IF NOT IS_BIT_SET(iGPSTrackingMeterRegisteredPedBitset[i / 32], i % 32)
									PRINTLN("[LM][MAINTAIN_GPS_DISTANCE_METER_BAR] - iPed: ", i, " exists, let's check his go-tos")
									
									BOOL bAssignedCheck = FALSE
									// We want the last valid item.
									FOR ii = 0 TO MAX_ASSOCIATED_GOTO_TASKS-1
										vGPSTrackingMeterEnd[iIndex] = GET_ASSOCIATED_GOTO_TASK_DATA__POSITION(MC_serverBD_2.iAssociatedGotoPoolIndex[i], g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sAssociatedGotoTaskData, MAX_ASSOCIATED_GOTO_TASKS-(ii+1))
																				
										PRINTLN("[LM][MAINTAIN_GPS_DISTANCE_METER_BAR] - Goto Pos: ", MAX_ASSOCIATED_GOTO_TASKS-(ii+1), " vPos: ", vGPSTrackingMeterEnd[iIndex])
										
										IF NOT IS_VECTOR_ZERO(vGPSTrackingMeterEnd[iIndex])									
											iGPSTrackingMeterPedIndex[iIndex] = i
											iGPSTrackingMeterDistCache[iIndex] = ROUND(VDIST(GET_ENTITY_COORDS(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[i])), vGPSTrackingMeterEnd[iIndex]))
											vGPSTrackingMeterStart[iIndex] = g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].vPos
											PRINTLN("[LM][MAINTAIN_GPS_DISTANCE_METER_BAR] - Assigned final Loc - Breaking Loop.")
											bAssignedCheck = TRUE
											BREAKLOOP
										ENDIF
									ENDFOR
									
									IF NOT bAssignedCheck
										PRINTLN("[LM][MAINTAIN_GPS_DISTANCE_METER_BAR] - Nothing was assigned here...")
									ENDIF
									
									SET_BIT(iGPSTrackingMeterRegisteredPedBitset[i / 32], i % 32)
								ENDIF
							ELSE
								
								PRINTLN("[LM][MAINTAIN_GPS_DISTANCE_METER_BAR] - iPed: ", i, " Is dead or does not exist.")
							ENDIF
						ENDIF
					ENDIF
				ENDFOR	
			ENDIF
		ENDIF
	ENDIF
		
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iTeamBitSet3, ciBS3_ENABLE_MULTIPLE_PLAYER_SAME_VEHICLE_RESPAWN)
		IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)		
			IF IS_BIT_SET(iLocalBoolCheck25, LBOOL25_INITIALIZED_SEATING_DATA_FOR_MULTI_VEH)
				IF MC_playerBD[iLocalPart].iGameState = GAME_STATE_RUNNING
					PROCESS_MULTIPLE_PLAYER_SAME_VEHICLE_LOGIC()
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	PROCESS_CAMERA_SHAKE_ON_RULE()
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciHUD_HIDE_RP_BAR)
		HIDE_SCRIPTED_HUD_COMPONENT_THIS_FRAME(HUD_MP_RANK_BAR)
	ENDIF
		
	IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam] < FMMC_MAX_RULES
	
		MAINTAIN_FORCED_CAMERA(GET_FORCED_CAMERA_MODE_THIS_FRAME())
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciHWN_HEARTBEAT_VIBRATE_ENABLED)
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciHWN_ENABLE_HEARTBEAT_BOTH_TEAMS)
				private_FIND_OTHER_TEAM_FOR_HEARTBEAT()
			ENDIF
		ENDIF
				
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciBIKES_DONT_WIPE_OUT)
		OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTen, ciDISABLE_BIKE_COMBAT)
			IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed, TRUE)
				IF IS_PED_ON_ANY_BIKE(LocalPlayerPed)
					VEHICLE_INDEX vIndex = GET_VEHICLE_PED_IS_IN(LocalPlayerPed, TRUE)
					IF NETWORK_HAS_CONTROL_OF_ENTITY(vIndex)
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciBIKES_DONT_WIPE_OUT)
							SET_BIKE_EASY_TO_LAND(vIndex, TRUE)
							PRINTLN("[RCC MISSION] - SET_BIKE_EASY_TO_LAND VEHICLE FOR CURRENT BIKE")
						ENDIF
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTen, ciDISABLE_BIKE_COMBAT)
							SET_PEDS_CAN_FALL_OFF_THIS_VEHICLE_FROM_LARGE_FALL_DAMAGE(vIndex,TRUE,250)
							PRINTLN("[RCC MISSION] - SET_PEDS_CAN_FALL_OFF_THIS_VEHICLE_FROM_LARGE_FALL_DAMAGE VEHICLE FOR CURRENT BIKE")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		

		IF NOT bIsAnySpectator
			IF MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam] < FMMC_MAX_RULES

				IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_REQUEST_AVENGER_HOLD_TRANSITION_BLOCK)
					
					BOOL bUnblock
					INT iCurrentStage = GET_MC_CLIENT_MISSION_STAGE(iLocalPart)
					
					// can unblock for definite now
					IF iCurrentStage != CLIENT_MISSION_STAGE_SCRIPTED_CUTSCENE
					AND iCurrentStage != CLIENT_MISSION_STAGE_DELIVER_VEH
					
						PRINTLN("[RCC MISSION] - PROCESS_EVERY_FRAME_PLAYER_CHECKS - REQUEST_AVENGER_HOLD_TRANSITION_BLOCKED - iCurrentStage: ", GET_MC_CLIENT_MISSION_STAGE_STRING_FOR_DEBUG(iCurrentStage), " - UNBLOCK IMMEDIATELY")
						bUnblock = TRUE
					
					// On deliver vehicle, and not in drop off/dropping off, start a timer
					ELIF iCurrentStage = CLIENT_MISSION_STAGE_DELIVER_VEH
					AND NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_IN_DROP_OFF)
					AND NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_DROPPING_OFF)
					
						IF NOT HAS_NET_TIMER_STARTED(tdAvengerHoldUnlockTimer)
							
							PRINTLN("[RCC MISSION] - PROCESS_EVERY_FRAME_PLAYER_CHECKS - REQUEST_AVENGER_HOLD_TRANSITION_BLOCKED - iCurrentStage: ", GET_MC_CLIENT_MISSION_STAGE_STRING_FOR_DEBUG(iCurrentStage), " - START UNBLOCK TIMER")
							REINIT_NET_TIMER(tdAvengerHoldUnlockTimer)
							
						ELIF HAS_NET_TIMER_EXPIRED(tdAvengerHoldUnlockTimer, 2000)
							
							PRINTLN("[RCC MISSION] - PROCESS_EVERY_FRAME_PLAYER_CHECKS - REQUEST_AVENGER_HOLD_TRANSITION_BLOCKED - iCurrentStage: ", GET_MC_CLIENT_MISSION_STAGE_STRING_FOR_DEBUG(iCurrentStage), " - UNBLOCK TIMER EXPIRED")
							bUnblock = TRUE
							
						ENDIF
					
					// needs to remain blocked
					ELSE
						IF HAS_NET_TIMER_STARTED(tdAvengerHoldUnlockTimer)
							PRINTLN("[RCC MISSION] - PROCESS_EVERY_FRAME_PLAYER_CHECKS - REQUEST_AVENGER_HOLD_TRANSITION_BLOCKED - iCurrentStage: ", GET_MC_CLIENT_MISSION_STAGE_STRING_FOR_DEBUG(iCurrentStage), " - REMAIN BLOCKED")
							RESET_NET_TIMER(tdAvengerHoldUnlockTimer)
						ENDIF
					ENDIF

					IF bUnblock	
						PRINTLN("[RCC MISSION] - PROCESS_EVERY_FRAME_PLAYER_CHECKS - REQUEST_AVENGER_HOLD_TRANSITION_BLOCKED - UNBLOCKING")
						REQUEST_AVENGER_HOLD_TRANSITION_BLOCKED(FALSE)
						RESET_NET_TIMER(tdAvengerHoldUnlockTimer)
					ENDIF
					
				ENDIF

				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].iRuleBitsetTen[iRule], ciBS_RULE10_PROGRESS_RULE_ALL_DRIVER_ACCEL)
				AND GET_SEAT_PED_IS_IN(localPlayerPed) = VS_DRIVER
				
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ACCELERATE, TRUE)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_THROTTLE_UP, TRUE)
					
					TEXT_LABEL_63 tlTextToShow = "HLP_PRS_R2"
					
					IF g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].iCustomHelpTextPressAccel[iRule] != -1
						tlTextToShow = GET_CUSTOM_STRING_LIST_TEXT_LABEL(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].iCustomHelpTextPressAccel[iRule])
						PRINTLN("[RCC MISSION] - PROCESS_EVERY_FRAME_PLAYER_CHECKS - Using custom help text for ciBS_RULE10_PROGRESS_RULE_ALL_DRIVER_ACCEL")
					ENDIF
					
					// Print Help					
					IF (amIWaitingForPlayersToEnterVeh != NULL AND NOT CALL amIWaitingForPlayersToEnterVeh())
					AND IS_AVENGER_HOLD_TRANSITION_BLOCKED_AND_INACTIVE(iRule, MC_playerBD[iLocalPart].iteam)
					AND (NOT IS_BIT_SET(iLocalBoolCheck,LBOOL_ON_LAST_LIFE)
					OR NOT DOES_TEAM_HAVE_ANY_PLAYERS_DEAD(MC_playerBD[iLocalPart].iteam))
						IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(tlTextToShow)
							PRINT_HELP_FOREVER(tlTextToShow)
						ENDIF
					ELSE
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(tlTextToShow)
							CLEAR_HELP(TRUE)
						ENDIF
					ENDIF
				ELSE
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HLP_PRS_R2")
						CLEAR_HELP(TRUE)
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iRuleBitsetSixteen[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iTeam]], ciBS_RULE16_ALLOW_VEHICLE_REAR_ENTRY)
					IF bLocalPlayerPedOk
						SET_PED_RESET_FLAG(LocalPlayerPed, PRF_AllowPedRearEntry, TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF	
				
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciALLOW_MISSION_AIRSTRIKES)
			IF iRule < FMMC_MAX_RULES
				IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sAirstrikeStruct[iRule].iSafetyRange > 0
				AND NOT IS_BIT_SET(iLocalBoolCheck23, LBOOL23_AIRSTRIKE_SPEED_BAR_FIRED)
					IF IS_BIT_SET(iLocalBoolCheck24, LBOOL24_AIRSTRIKE_IN_PROGRESS)
						FLOAT fDist = VDIST2(GET_PLAYER_COORDS(LocalPlayer), g_vAirstrikeCoords)
						IF fDist < POW(TO_FLOAT(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sAirstrikeStruct[iRule].iSafetyRange), 2)
						AND NOT IS_BIT_SET(iLocalBoolCheck24, LBOOL24_AIRSTRIKE_SAFETY_RANGE_STOP)
							SET_BIT(iLocalBoolCheck24, LBOOL24_AIRSTRIKE_SAFETY_RANGE_STOP)
							SET_BIT(iLocalBoolCheck24, LBOOL24_AIRSTRIKE_FINAL_SHOT_FIRED)
							PRINTLN("[RCC MISSION][AIRSTRIKE] - SETTING LBOOL24_AIRSTRIKE_SAFETY_RANGE_STOP")
							REINIT_NET_TIMER(tdAirstrikeSafetyTimer)
							REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("w_lr_rpg_rocket")))	
						ENDIF
						
						IF IS_BIT_SET(iLocalBoolCheck24, LBOOL24_AIRSTRIKE_DIALOGUE_TRIGGERED)
							fDist = VDIST2(GET_PLAYER_COORDS(LocalPlayer), vDialogueAirstrikeCoords)
							IF fDist < POW(TO_FLOAT(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sAirstrikeStruct[iRule].iSafetyRange), 2)
							AND NOT IS_BIT_SET(iLocalBoolCheck24, LBOOL24_AIRSTRIKE_SAFETY_RANGE_STOP)
								SET_BIT(iLocalBoolCheck24, LBOOL24_AIRSTRIKE_SAFETY_RANGE_STOP)
								SET_BIT(iLocalBoolCheck24, LBOOL24_AIRSTRIKE_FINAL_SHOT_FIRED)
								PRINTLN("[RCC MISSION][AIRSTRIKE] - SETTING LBOOL24_AIRSTRIKE_SAFETY_RANGE_STOP")
								REINIT_NET_TIMER(tdAirstrikeSafetyTimer)
								REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("w_lr_rpg_rocket")))	
							ENDIF
						ENDIF
					ENDIF

				ENDIF
							
				IF HAS_NET_TIMER_STARTED(tdAirstrikeSafetyTimer)
					IF HAS_NET_TIMER_EXPIRED(tdAirstrikeSafetyTimer, ci_iAirstrikeStartDelay)
						PRINTLN("[RCC MISSION][AIRSTRIKE] - SETTING LBOOL24_AIRSTRIKE_FINAL_SHOT_FIRED - SAFETY RANGE TRIGGERED")
						CLEAR_BIT(iLocalBoolCheck24, LBOOL24_AIRSTRIKE_SAFETY_RANGE_STOP)
						RESET_NET_TIMER(tdAirstrikeSafetyTimer)
					ENDIF
				ENDIF
				
				//Dialogue trigger airstrikes
				IF IS_BIT_SET(iLocalBoolCheck24, LBOOL24_AIRSTRIKE_DIALOGUE_TRIGGERED)
				AND NOT IS_BIT_SET(iLocalBoolCheck24, LBOOL24_AIRSTRIKE_IN_PROGRESS)
					IF NOT IS_BIT_SET(iLocalBoolCheck23, LBOOL23_SPEED_BAR_AIRSTRIKE_CALLED)
					AND NOT IS_BIT_SET(iLocalBoolCheck24, LBOOL24_AIRSTRIKE_SAFETY_RANGE_STOP)					
						IF NOT IS_BIT_SET(iLocalBoolCheck25, LBOOL25_ORBITAL_CANNON_DIALOGUE_AIRSTRIKE)
							PRINTLN("[RCC MISSION][AIRSTRIKE][CALL] Calling DO_SCRIPTED_AIRSTRIKE from: STARTING DIALOGUE")
							DO_SCRIPTED_AIRSTRIKE(vDialogueAirstrikeCoords, 10)
						ELSE
							PRINTLN("[RCC MISSION][AIRSTRIKE][CALL] Calling FIRE_ORBITAL_CANNON from: STARTING DIALOGUE")
							FIRE_ORBITAL_CANNON(vDialogueAirstrikeCoords)
						ENDIF
					ENDIF
				ENDIF
				
				IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sAirstrikeStruct[iRule].iEntityType > -1				
					
					IF NOT IS_BIT_SET(iLocalBoolCheck24, LBOOL24_AIRSTRIKE_IN_PROGRESS)
										
						IF IS_BIT_SET(iLocalBoolCheck23, LBOOL23_AIRSTRIKE_FIFTY_PERCENT_STRIKE)
						AND NOT IS_BIT_SET(iLocalBoolCheck23, LBOOL23_SPEED_BAR_AIRSTRIKE_CALLED)
						AND NOT IS_BIT_SET(iLocalBoolCheck24, LBOOL24_AIRSTRIKE_SAFETY_RANGE_STOP)
							IF HAS_NET_TIMER_STARTED(tdAirstrikeStartTimer)
								IF HAS_NET_TIMER_EXPIRED(tdAirstrikeStartTimer, ci_iAirstrikeStartDelay)
									PRINTLN("[RCC MISSION][AIRSTRIKE] ACTIVATE_AIRSTRIKE_AND_SET_COORDS - PERCENTAGE LAUNCH")
									UPDATE_AIRSTRIKE_OFFSET_VECTOR(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sAirstrikeStruct[iRule].iEntityType, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sAirstrikeStruct[iRule].iEntityID, iTeam, iRule)
									
									PRINTLN("[RCC MISSION][AIRSTRIKE][OFFSET] Getting offset from GET_RANDOM_OFFSET_ON_RING - vAirstrikeOffset: ", vAirstrikeOffset)
									ACTIVATE_AIRSTRIKE_AND_SET_COORDS(vAirstrikeOffset.x, vAirstrikeOffset.y)								
								ENDIF
							ELSE
								REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("w_lr_rpg_rocket")))	
								START_NET_TIMER(tdAirstrikeStartTimer)
							ENDIF
						ENDIF				
						
					ENDIF				
					
					IF bAirstrikeSpheres
						VECTOR tempTargetCoords

						GET_AIRSTRIKE_TARGET_COORDS(tempTargetCoords, MC_playerBD[iPartToUse].iTeam, MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam])
						//tempTargetCoords = GET_ENTITY_COORDS(LocalPlayerPed)
						IF bAirstrikeSphereMax
							DRAW_DEBUG_SPHERE(tempTargetCoords, fRAGAirstrikeMax,255, 170, 0, 175)
						ELIF bAirstrikeSphereMin
							DRAW_DEBUG_SPHERE(tempTargetCoords, fRAGAirstrikeMin,255, 170, 0, 175)
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(iLocalBoolCheck24, LBOOL24_AIRSTRIKE_IN_PROGRESS)
					#IF IS_DEBUG_BUILD
						DRAW_DEBUG_SPHERE(g_vAirstrikeCoords, 10, 255, 0 ,0 ,150)
						DRAW_DEBUG_LINE(GET_ENTITY_COORDS(LocalPlayerPed), g_vAirstrikeCoords, 255, 0 ,0)
					#ENDIF
					IF IS_BIT_SET(iLocalBoolCheck24, LBOOL24_AIRSTRIKE_FINAL_SHOT_FIRED)
						PRINTLN("[RCC MISSION][AIRSTRIKE] - Clearing bits and vector")
						CLEAR_BIT(iLocalBoolCheck24, LBOOL24_AIRSTRIKE_IN_PROGRESS)
						CLEAR_BIT(iLocalBoolCheck24, LBOOL24_AIRSTRIKE_SAFETY_RANGE_STOP)
						CLEAR_BIT(iLocalBoolCheck24, LBOOL24_AIRSTRIKE_DIALOGUE_TRIGGERED)
						vAirstrikeOffset = <<0,0,0>>
						iAirstrikeExplosionCount = 0
						iAirstrikeRocketFiredBitSet = 0
						RESET_NET_TIMER(tdAirstrikeStartTimer)
						RESET_NET_TIMER(tdAirstrikeSafetyTimer)
						STOP_SOUND(iSoundIDVehBeacon)
					ELSE
						IF NOT IS_BIT_SET(iLocalBoolCheck24, LBOOL24_AIRSTRIKE_SAFETY_RANGE_STOP)
						AND NOT IS_BIT_SET(iLocalBoolCheck23, LBOOL23_AIRSTRIKE_SPEED_BAR_FIRED)
							IF NOT IS_BIT_SET(iLocalBoolCheck24, LBOOL24_AIRSTRIKE_DIALOGUE_TRIGGERED)							
								IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sAirstrikeStruct[iRule].iAirstrikeType = ciAIRSTRIKE_TYPE_MISSILES
									PRINTLN("[RCC MISSION][AIRSTRIKE][CALL] Calling DO_SCRIPTED_AIRSTRIKE from: Airstrike in progress, NOT DIALOGUE")
									DO_SCRIPTED_AIRSTRIKE(g_vAirstrikeCoords, 10)
								ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sAirstrikeStruct[iRule].iAirstrikeType = ciAIRSTRIKE_TYPE_ORBITAL_CANNON
									PRINTLN("[RCC MISSION][AIRSTRIKE][CALL] Calling FIRE_ORBITAL_CANNON from: Airstrike in progress, NOT DIALOGUE")
									FIRE_ORBITAL_CANNON(g_vAirstrikeCoords)
								ENDIF
							ELSE
								IF NOT IS_BIT_SET(iLocalBoolCheck25, LBOOL25_ORBITAL_CANNON_DIALOGUE_AIRSTRIKE)
									PRINTLN("[RCC MISSION][AIRSTRIKE][CALL] Calling DO_SCRIPTED_AIRSTRIKE from: Airstrike in progress, DIALOGUE")
									DO_SCRIPTED_AIRSTRIKE(vDialogueAirstrikeCoords, 10)
								ELSE
									PRINTLN("[RCC MISSION][AIRSTRIKE][CALL] Calling FIRE_ORBITAL_CANNON from: Airstrike in progress, NOT DIALOGUE")
									FIRE_ORBITAL_CANNON(vDialogueAirstrikeCoords)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
			
		//Vehicle Rockets
		IF IS_BIT_SET(iLocalBoolCheck15, LBOOL15_VEHICLE_MENU_WEAPON_ACTIVE_ROCKETS)
			REQUEST_WEAPON_ASSET(g_wtPickupRocketType)
			
			SET_BIT(iLocalBoolCheck14, LBOOL14_VEHICLE_ROCKET_ASSET_REQUEST)
			
			IF HAS_WEAPON_ASSET_LOADED(g_wtPickupRocketType)
				IF NOT g_VehicleRocketInfo.bIsCollected
					IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(VehicleDelayTimer) > g_FMMC_STRUCT.iPerRuleVehicleDelayTimer[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]][MC_playerBD[iPartToUse].iTeam]
						IF iVehicleRockets != 0
							g_VehicleRocketInfo.bIsCollected = TRUE
						ENDIF
					ENDIF
				ENDIF
				
				UPDATE_VEHICLE_ROCKETS()
				
				IF NOT g_VehicleRocketInfo.bIsCollected
					IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(VehicleDelayTimer) > g_FMMC_STRUCT.iPerRuleVehicleDelayTimer[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]][MC_playerBD[iPartToUse].iTeam]
						IF iVehicleRockets > 0
							iVehicleRockets--
						ENDIF
						
						REINIT_NET_TIMER(VehicleDelayTimer)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//Vehicle Boost
		IF IS_BIT_SET(iLocalBoolCheck15, LBOOL15_VEHICLE_MENU_WEAPON_ACTIVE_BOOST)
			IF NOT g_VehicleBoostInfo.bCollected
				IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(VehicleDelayTimer) > g_FMMC_STRUCT.iPerRuleVehicleDelayTimer[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]][MC_playerBD[iPartToUse].iTeam]
					IF iVehicleBoost != 0
						g_VehicleBoostInfo.bCollected = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			UPDATE_VEHICLE_BOOSTING(DEFAULT, 
									g_FMMC_STRUCT.iPerRuleVehicleBoostDuration[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]][MC_playerBD[iPartToUse].iTeam], 
									TO_FLOAT(g_FMMC_STRUCT.iPerRuleVehicleBoostSpeed[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]][MC_playerBD[iPartToUse].iTeam]))
			
			IF NOT g_VehicleBoostInfo.bCollected
				IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(VehicleDelayTimer) > g_FMMC_STRUCT.iPerRuleVehicleDelayTimer[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]][MC_playerBD[iPartToUse].iTeam]
					IF iVehicleBoost > 0
						iVehicleBoost--
					ENDIF
					
					REINIT_NET_TIMER(VehicleDelayTimer)
				ENDIF
			ENDIF
		ENDIF
		
		//Vehicle Repair
		IF IS_BIT_SET(iLocalBoolCheck15, LBOOL15_VEHICLE_MENU_WEAPON_ACTIVE_REPAIR)
			iVehicleRepair = iVehicleRepair
		ENDIF
		
		//Vehicle Spikes
		IF IS_BIT_SET(iLocalBoolCheck15, LBOOL15_VEHICLE_MENU_WEAPON_ACTIVE_SPIKES)
			IF NOT g_VehicleSpikeInfo.bIsCollected
				IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(VehicleDelayTimer) > g_FMMC_STRUCT.iPerRuleVehicleDelayTimer[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]][MC_playerBD[iPartToUse].iTeam]
					IF iVehicleSpikes != 0
						g_VehicleSpikeInfo.bIsCollected = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT g_VehicleSpikeInfo.bIsCollected
				IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(VehicleDelayTimer) > g_FMMC_STRUCT.iPerRuleVehicleDelayTimer[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]][MC_playerBD[iPartToUse].iTeam]
					IF iVehicleSpikes > 0
						iVehicleSpikes--
					ENDIF
					
					REINIT_NET_TIMER(VehicleDelayTimer)
				ENDIF
			ENDIF
		ENDIF
		
		//Vehicle Combat
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iRuleBitsetSeven[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]], ciBS_RULE7_TURN_ON_BIKE_COMBAT)
			IF IS_ENTITY_ALIVE(LocalPlayerPed)
				IF NOT GET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_DisableVehicleCombat)
					PRINTLN("[KH] PROCESS_EVERY_FRAME_PLAYER_CHECKS - Disabling Vehicle Combat for local player")
					SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_DisableVehicleCombat, TRUE)
				ENDIF
			ENDIF
		ELSE
			IF IS_ENTITY_ALIVE(LocalPlayerPed)
				IF GET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_DisableVehicleCombat)
					PRINTLN("[KH] PROCESS_EVERY_FRAME_PLAYER_CHECKS - Enabling Vehicle Combat for local player")
					SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_DisableVehicleCombat, FALSE)
				ENDIF
			ENDIF
		ENDIF
		
		PROCESS_EVERY_FRAME_PLAYER_ACTION_MODE(FALSE)	
				
		IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + MC_playerBD[iPartToUse].iteam)
			PRINTLN("[RCC MISSION] LBOOL10_DROP_OFF_LOC_DISPLAY is set")
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitsetFour[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE4_HIDE_DEATH_TICKERS)
				IF NOT g_bNoDeathTickers
					PRINTLN("[RCC MISSION] Setting g_bNoDeathTickers = TRUE")
					g_bNoDeathTickers = TRUE
				ENDIF
			ELSE
				IF g_bNoDeathTickers
					PRINTLN("[RCC MISSION] Setting g_bNoDeathTickers = FALSE")
					g_bNoDeathTickers = FALSE
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitsetFour[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE4_RESPAWN_WITH_LIMITED_Z)
				SET_CAR_NODE_SEARCH_LOWER_Z_LIMIT(5.0)
				PRINTLN("[RCC MISSION][RESPAWNING] 5 limit on respawning")
			ELSE
				SET_CAR_NODE_SEARCH_LOWER_Z_LIMIT(0.0)
				PRINTLN("[RCC MISSION][RESPAWNING] 0 limit on respawning")
			ENDIF
			
			IF g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iMinigunDamage[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]] != 100
				PRINTLN("[RCC MISSION] PROCESS_EVERY_FRAME_PLAYER_CHECKS - Setting up Minigun defence mod - ",TO_FLOAT(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iMinigunDamage[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]])/100,"%")
				SET_MINIGUN_PLAYER_DAMAGE(TO_FLOAT(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iMinigunDamage[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]])/100)
				SET_BIT(iLocalBoolCheck26, LBOOL26_MINIGUN_DEFENCE_MODIFIED)
			ELSE
				IF IS_BIT_SET(iLocalBoolCheck26, LBOOL26_MINIGUN_DEFENCE_MODIFIED)
					SET_MINIGUN_PLAYER_DAMAGE(TO_FLOAT(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iMinigunDamage[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam]])/100)
					CLEAR_BIT(iLocalBoolCheck26, LBOOL26_MINIGUN_DEFENCE_MODIFIED)
					PRINTLN("[RCC MISSION] PROCESS_EVERY_FRAME_PLAYER_CHECKS - Resetting minigun damage mod")
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF
	
	PROCESS_HANGING_UP_PHONE_AFTER_PHOTO()
	
	IF bLocalPlayerPedOk		
		IF IS_BIT_SET(g_FMMC_Struct.iTeamVehicleBitSet, ciFMMC_TeamVehBS_DISABLE_TEAM_RESPAWN_VEHICLE_SPACE_ROCKETS)
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM_IHELP_BUZ")
				HIDE_HELP_TEXT_THIS_FRAME()
			ENDIF
		ENDIF
		
		IF MC_PlayerBD[iLocalPart].iteam < FMMC_MAX_TEAMS
		AND MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam] < FMMC_MAX_RULES
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iLocalPart].iteam].iRuleBitsetEight[MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam]], ciBS_RULE8_EXPLOSIONS_DONT_RAGDOLL)
			OR IS_PARTICIPANT_A_JUGGERNAUT(iLocalPart)
				SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_DontActivateRagdollFromExplosions, TRUE)
			ELSE
				SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_DontActivateRagdollFromExplosions, FALSE)
			ENDIF
			
			IF g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iLocalPart].iteam].iMaxHealth[MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam]] != 0
			AND NOT IS_PARTICIPANT_A_BEAST(iLocalPart)
				INT iNewMaxHealth = g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iLocalPart].iteam].iMaxHealth[MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam]]
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iLocalPart].iteam].iRuleBitsetEight[MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam]], ciBS_RULE8_SCALE_MAX_PLAYER_HEALTH)
					INT i
					INT iEnemyPlayers = 0
					FOR i = 0 TO FMMC_MAX_TEAMS - 1
						iEnemyPlayers = iEnemyPlayers + MC_serverBD.iNumberOfPlayingPlayers[i]
					ENDFOR
					iNewMaxHealth = (iNewMaxHealth * iEnemyPlayers)
					INCREASE_PLAYER_HEALTH(iNewMaxHealth)
				ELSE
					INCREASE_PLAYER_HEALTH(iNewMaxHealth)
				ENDIF
			ENDIF
		
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ MC_PlayerBD[iLocalPart].iteam ].iRuleBitSetFive[ MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam] ], ciBS_RULE5_SLOW_PLAYER)
				DO_HEIST_HEAVY_ARMOUR_SLOWDOWN_EFFECT()
			ENDIF
		ENDIF
		
		PROCESS_KILL_SELF_ON_TEAM_FAIL()
		
		PROCESS_BLIP_PLAYER_ARRIVED_AT_LOCATION()
		
		PROCESS_SET_PLAYER_MOVESPEED_OVERRIDE_FOR_THIS_RULE()
	ENDIF
	
	IF g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iSpawnInvincibility > -1
		SET_NEXT_RESPAWN_INVINCIBLE_TIME(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iSpawnInvincibility * 1000, g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iTeam].iSpawnInvincibility * 1000)
	ENDIF
	
	PROCESS_TEAM_REGEN__LOCAL()
	
	IF IS_BIT_SET(iLocalBoolCheck2, LBOOL2_BEAST_MODE_ACTIVE_ON_MISSION)
		PROCESS_BEAST_MODE_PLAYER()
	ENDIF
	
	PROCESS_APPLY_SERVER_CACHED_PLAYER_HEAD_BLEND_DATA_TO_PLAYER_PED_CLONES()
	
	PROCESS_PLAYER_VISUAL_AIDS()
		
	PROCESS_PLAYER_HEALTH_DRAIN()

	PROCESS_MIDPOINT_BLOCKING()
		
	PROCESS_FORCED_STEALTH()
		
	PROCESS_POST_CUTSCENE_COVER()
	
	PROCESS_FORCED_HEIST_SPECTATOR()
	
	PROCESS_PHONE_EMP()
	
	PROCESS_LIMITED_STUN_GUN()
	
	IF IS_BIT_SET(iLocalBoolCheck23, LBOOL23_LOCATE_TEAM_SWAP_REQUESTED)
		PROCESS_CLIENT_LOCATE_TEAM_SWAP()
	ENDIF
	
	PROCESS_AKULA_STEALTH_MODE_IN_MISSION()
			
	IF GET_LOCAL_PLAYER_CURRENT_RULE(TRUE, FALSE) < FMMC_MAX_RULES
		IF g_FMMC_STRUCT.sFMMCEndConditions[GET_LOCAL_PLAYER_TEAM(TRUE)].iAmbientPoliceAccuracy[GET_LOCAL_PLAYER_CURRENT_RULE(TRUE)] > -1
			FLOAT fAccuracy = TO_FLOAT(g_FMMC_STRUCT.sFMMCEndConditions[GET_LOCAL_PLAYER_TEAM(TRUE)].iAmbientPoliceAccuracy[GET_LOCAL_PLAYER_CURRENT_RULE(TRUE)])/100
			SET_AMBIENT_LAW_PED_ACCURACY_MODIFIER(fAccuracy)
		ENDIF
	
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[GET_LOCAL_PLAYER_TEAM(TRUE)].iRuleBitset[GET_LOCAL_PLAYER_CURRENT_RULE(TRUE)], ciBS_RULE_ENABLETRACKIFY)
			IF NOT IS_CELLPHONE_TRACKIFY_IN_USE()
				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TRACKIFY_PHO")
				AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DR_CNIC")
					IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciHIDE_DRIVING_ARROW_HELP)
						DISPLAY_HELP_TEXT_THIS_FRAME("TRACKIFY_PHO",TRUE)
						PRINTLN("[RCC MISSION]  DISPLAYING TRACKIFY_PHO TEXT")
					ENDIF
				ENDIF
				
				IF IS_PED_RUNNING_MOBILE_PHONE_TASK(LocalPlayerPed)
				AND NOT IS_CELLPHONE_CAMERA_IN_USE()
					IF bLocalPlayerPedOK
						CLEAR_PED_TASKS(LocalPlayerPed)
					ENDIF
				ENDIF
			ELSE
				IF NETWORK_HAS_CONTROL_OF_ENTITY(LocalPlayerPed)
					IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciHIDE_DRIVING_ARROW_HELP)
						IF NOT IS_PED_RUNNING_MOBILE_PHONE_TASK(LocalPlayerPed)
							CLEAR_PED_TASKS(LocalPlayerPed)
							TASK_USE_MOBILE_PHONE(LocalPlayerPed,TRUE,Mode_ToText)
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TRACKIFY_PHO")
					CLEAR_HELP()
					PRINTLN("[RCC MISSION]  CLEARING HELP AS TRACKIFY IS IN USE")
				ENDIF
			ENDIF
		ELSE
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TRACKIFY_PHO")
				CLEAR_HELP()
				PRINTLN("[RCC MISSION]  CLEARING HELP AS ENABLETRACKIFY RULE IS OFF")
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(LocalPlayer)
	OR DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(LocalPlayer) AND (PlayerToUse != LocalPlayer)
	OR g_eSpecialSpectatorState != SSS_IDLE
			
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciUSE_CIRCLE_SCORE_HUD)
		OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciUSE_CIRCLE_HUD_TIMER)
		OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_CIRCLE_HUD_SCORES_WITH_TIMER)
			IF g_bMissionEnding
			AND NOT IS_BIT_SET(iLocalBoolCheck26, LBOOL26_ENDING_HUD_UPDATED)
				IF NOT HAS_NET_TIMER_STARTED(tdEndingTimer)
					REINIT_NET_TIMER(tdEndingTimer)
				ELSE
					IF HAS_NET_TIMER_EXPIRED(tdEndingTimer, ciEndingTimerLength)
						SET_BIT(iLocalBoolCheck26, LBOOL26_ENDING_HUD_UPDATED)
						RESET_NET_TIMER(tdEndingTimer)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	PROCESS_CUSTOM_SHARD_ON_TEAM_POINT()
	
	IF MC_serverBD.iCleanedUpUndeliveredVehiclesBS != g_TransitionSessionNonResetVars.iDoNotRespawnVehBitSet
		g_TransitionSessionNonResetVars.iDoNotRespawnVehBitSet = MC_serverBD.iCleanedUpUndeliveredVehiclesBS
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyTwo, ciOptionsBS22_ShowVehicleHealthinFP)
	AND NOT IS_ON_SCREEN_WHEEL_SPIN_MINIGAME_ACTIVE()
	AND NOT IS_BIT_SET(iLocalBoolResetCheck1, LBOOL_RESET1_SHOWING_VEH_HEALTH_BARS)
		IF PROCESS_FP_VEHICLE_HEALTHBAR(ROUND(GET_FMMC_PLAYER_VEHICLE_BODY_HEALTH_FOR_TEAM(MC_PlayerBD[iLocalPart].iteam)))
			SET_BIT(iLocalBoolResetCheck1, LBOOL_RESET1_SHOWING_VEH_HEALTH_BARS)
		ENDIF
	ENDIF
	
	IF NOT bIsAnySpectator
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciBLIP_AFTER_ENTERING_VEHICLE)
		AND NOT IS_BIT_SET(iLocalBoolCheck23, LBOOL23_BLIP_FORCED_ON_BY_ZONE)
			IF MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iTeam] < FMMC_MAX_RULES
				IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
					SET_BIT(iLocalBoolCheck23, LBOOL23_BLIP_FORCED_ON_BY_ZONE)
					PROCESS_MY_PLAYER_BLIP_VISIBILITY(MC_PlayerBD[iLocalPart].iTeam, MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iTeam])
				ENDIF
			ENDIF
		ENDIF
	ENDIF
		
	IF IS_BIT_SET(iLocalboolCheck25, LBOOL25_CANNOT_BE_DAMAGED_DUE_TO_VEHICLE)
	AND iVehLastInsideGrantedInvulnability != -1
	AND IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
		BOOL bResetInvulnerability
		
		IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iVehLastInsideGrantedInvulnability])
			VEHICLE_INDEX viPlayer = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVehLastInsideGrantedInvulnability])
			
			IF IS_VEHICLE_FUCKED(viPlayer)
				bResetInvulnerability = TRUE
			ENDIF
		ELSE
			bResetInvulnerability = TRUE
		ENDIF
		
		IF bResetInvulnerability
			PRINTLN("[LM][EVERY_FRAME_CHECKS] - ciFMMC_VEHICLE7_PLAYER_INVINCIBLE_IN_VEHICLE is set when mission is over. Setting SET_ENTITY_VISIBLE to false because of - iVeh: ", iVehLastInsideGrantedInvulnability)	
			
			CLEAR_BIT(iLocalBoolCheck25, LBOOL25_CANNOT_BE_DAMAGED_DUE_TO_VEHICLE)
			iVehLastInsideGrantedInvulnability = -1
			
			CLEANUP_PLAYER_FOR_WINNER_LOSER_SHARD_APPEARANCE(TRUE, TRUE)
		ENDIF
	ENDIF
	
	/*IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciEXPLODE_VEHICLE_ON_ZERO_HEALTH)
		PROCESS_VEHICLE_WEAPON_EXPLODE_ON_ZERO_HEALTH()
	ENDIF*/

	PROCESS_GHOSTING_THROUGH_PLAYER_VEHICLES()
		
	/*IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitsetFour[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE4_CAN_DROP_PACKAGE)
		AND MC_playerBD[iPartToUse].iObjCarryCount > 0
			PROCESS_PACKAGE_HANDLING()
		ENDIF
	ENDIF*/
	
	PROCESS_DELIVERY_WAIT_HAND_BRAKE()
	
	IF bStopDurationBlockSeatShuffle
		IF GET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_PreventAutoShuffleToDriversSeat)
			IF NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
				SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_PreventAutoShuffleToDriversSeat, FALSE)				
				bStopDurationBlockSeatShuffle = FALSE
			ENDIF
		ENDIF
	ENDIF
	
	PROCESS_LOCAL_ALTITUDE()
	
	IF bPlayerToUseOK		
		IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_FINISHED)
		AND IS_TEAM_ACTIVE(MC_playerBD[iPartToUse].iteam)
		AND MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES		
		AND IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
		AND NOT g_bMissionEnding
		AND IS_PLAYER_CONTROL_ON(LocalPlayer)
		AND IS_SKYSWOOP_AT_GROUND()
			
			//Only process if player hasn't actually finished the race
			IF (g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iMinSpeed[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]] != -1)
				PROCESS_VEHICLE_EXPLOSION_UNDER_MIN_SPEED()
			ENDIF
		ELSE
			IF NOT HAS_SOUND_FINISHED(iSoundIDVehBomb)
				STOP_SOUND(iSoundIDVehBomb)
			ENDIF
			IF NOT HAS_SOUND_FINISHED(iSoundIDVehBomb2)
				STOP_SOUND(iSoundIDVehBomb2)
			ENDIF
			IF NOT HAS_SOUND_FINISHED(iSoundIDVehBombCountdown)
				STOP_SOUND(iSoundIDVehBombCountdown)
			ENDIF
			fRealExplodeProgress = 0
			
			//Clear HUD thing
			IF bShowingAltitudeLimit
				SET_FAKE_MINIMAP_MAX_ALTIMETER_HEIGHT(0)
				bShowingAltitudeLimit = FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF bLocalPlayerOK
		IF GET_PLAYER_UNDERWATER_TIME_REMAINING( LocalPlayer ) > 0 // We're not drowning
			IF IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_I_AM_DROWNING)
				CLEAR_BIT(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_I_AM_DROWNING)
				PRINTLN("[RCC MISSION] PROCESS_EVERY_FRAME_PLAYER_CHECKS - I'm no longer drowning, clearing ClientBitset bit...")
			ENDIF
		ELSE
			IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_I_AM_DROWNING)
				SET_BIT(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_I_AM_DROWNING)
				PRINTLN("[RCC MISSION] PROCESS_EVERY_FRAME_PLAYER_CHECKS - I'm drowning! Set the ClientBitset bit")
			ENDIF
		ENDIF
	ELSE
		IF IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_I_AM_DROWNING)
			CLEAR_BIT(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_I_AM_DROWNING)
			PRINTLN("[RCC MISSION] PROCESS_EVERY_FRAME_PLAYER_CHECKS - I'm not okay, clearing ClientBitset drowning bit...")
		ENDIF
	ENDIF
					
	IF NOT bIsAnySpectator
		IF MC_playerBD[iPartToUse].bCelebrationScreenIsActive != g_bCelebrationScreenIsActive
			MC_playerBD[iPartToUse].bCelebrationScreenIsActive = g_bCelebrationScreenIsActive
			//dont want sctv coming in during celebration and seeing weirdness
		ENDIF
	ENDIF	
	
	WEAPON_TYPE wtWeaponType
	GET_CURRENT_PED_WEAPON(LocalPlayerPed, wtWeaponType)
	IF wtWeaponType != WEAPONTYPE_UNARMED
		GET_CURRENT_PED_WEAPON(LocalPlayerPed, wtCachedLastWeaponHeld)
	ENDIF

	IF NOT bIsAnySpectator
		IF NOT IS_BIT_SET(iLocalBoolCheck18, LBOOL18_WEAPONS_ENABLED_SHARD_PLAY)
			IF (g_FMMC_STRUCT.fTimeWeaponsDisabledAtStart*1000) > 0
				IF HAS_NET_TIMER_STARTED(tdMissionStartWeaponsDisabled)
					IF NOT HAS_NET_TIMER_EXPIRED_READ_ONLY(tdMissionStartWeaponsDisabled, ROUND(g_FMMC_STRUCT.fTimeWeaponsDisabledAtStart*1000))
						// Stop Attacking.
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PARACHUTE_DEPLOY)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_SPECIAL)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_SPECIAL_TWO)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_ALTERNATE)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_BLOCK)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
						IF NOT GET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_DisableVehicleCombat)
							SET_PED_CONFIG_FLAG(LocalPlayerPed,PCF_DisableVehicleCombat,TRUE)
						ENDIF
												
						SET_PED_CAN_SWITCH_WEAPON(LocalPlayerPed, FALSE)
						SET_CURRENT_PED_WEAPON(LocalPlayerPed, WEAPONTYPE_UNARMED, TRUE)
					ELSE
						// Play Shard
						IF GET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_DisableVehicleCombat)
							SET_PED_CONFIG_FLAG(LocalPlayerPed,PCF_DisableVehicleCombat,FALSE)
						ENDIF
						SET_PED_CAN_SWITCH_WEAPON(LocalPlayerPed, TRUE)
						SET_CURRENT_PED_WEAPON(LocalPlayerPed, wtCachedLastWeaponHeld, TRUE)
						SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GENERIC_TEXT, "SHD_TAG_WPN")
						SET_BIT(iLocalBoolCheck18, LBOOL18_WEAPONS_ENABLED_SHARD_PLAY)					
					ENDIF
				ELSE
					START_NET_TIMER(tdMissionStartWeaponsDisabled)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	// Exposing as an option BUT if we used custom respawn points then we definitely don't care about this.
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySix, ciOptionsBS26_UseFMSpawningInInteriors)
	AND (g_FMMC_STRUCT.iInteriorBS != 0 OR g_FMMC_STRUCT.iInteriorBS2 != 0)
		
		IF LocalPlayerCurrentInterior != NULL
			IF NOT g_SpawnData.MissionSpawnDetails.bConsiderInteriors	
				IF bLocalPlayerPedOK
					SET_PLAYER_RESPAWN_TO_CONSIDER_INTERIORS_ON_THIS_MISSION(TRUE)
					CPRINTLN(DEBUG_MISSION,"[RCC MISSION] PROCESS_EVERY_FRAME_PLAYER_CHECKS - setting dead spawn to consider interiors ")
				ENDIF
			ENDIF
		ELSE
			IF g_SpawnData.MissionSpawnDetails.bConsiderInteriors
				IF bLocalPlayerPedOK
				AND NOT IS_PLAYER_RESPAWNING(LocalPlayer)
					SET_PLAYER_RESPAWN_TO_CONSIDER_INTERIORS_ON_THIS_MISSION(FALSE)
					CPRINTLN(DEBUG_MISSION,"[RCC MISSION] PROCESS_EVERY_FRAME_PLAYER_CHECKS - Clearing dead spawn to consider interiors ")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SUB)
		IF GET_INTERIOR_AT_COORDS(GET_ENTITY_COORDS(LocalPlayerPed)) = GET_INTERIOR_AT_COORDS(vSubmarineInteriorCoords)
			SET_BLIP_COORDS(g_PlayerBlipsData.playerBlips[NATIVE_TO_INT(LocalPlayer)], <<-1682.577,5926.310,-62.092>>)
		ENDIF
	ENDIF
	
	IF MC_playerBD[iLocalPart].iCurrentLoc != -1
	AND GET_MC_SERVER_GAME_STATE() != GAME_STATE_END
		IF MC_serverBD_4.iGotoLocationDataRule[MC_playerBD[iLocalPart].iCurrentLoc][MC_playerBD[iPartToUse].iTeam] = FMMC_OBJECTIVE_LOGIC_CAPTURE
			fTempCaptureTime += fLastFrameTime
		ENDIF
	ELSE
		IF fTempCaptureTime > 0
			MC_playerBD[iLocalPart].fCaptureTime +=  fTempCaptureTime
			fTempCaptureTime = 0
			PRINTLN("[RCC MISSION] - Not in a locate, updating capture time to: ", MC_playerBD[iLocalPart].fCaptureTime)
		ENDIF
	ENDIF
		
	IF MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam] < FMMC_MAX_RULES
		
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].iRuleBitsetTen[MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam]], ciBS_RULE10_PROGRESS_RULE_ALL_DRIVER_ACCEL)
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].iRuleBitsetEleven[MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam]], ciBS_RULE11_DISABLE_AVENGER_TAKEOFF_UNTIL_FULL)	
				VEHICLE_INDEX viVeh
				IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
					viVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
				ENDIF
				IF DOES_ENTITY_EXIST(viVeh)
				AND IS_VEHICLE_DRIVEABLE(viVeh)				
					INT iVeh = IS_ENTITY_A_MISSION_CREATOR_ENTITY(viVeh)
					IF iVeh != -1
					AND IS_VEHICLE_WAITING_FOR_PLAYERS(iVeh, MC_playerBD[iLocalPart].iteam)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_THROTTLE_UP)
						PRINTLN("[RCC MISSION] Disabling PLAYER_CONTROL, INPUT_VEH_ACCELERATE. Need everyone to be in the vehicle/ vehicle interior.")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].iRuleBitsetEleven[MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam]], ciBS_RULE11_BLOCK_VEHICLE_EXIT)
			
			IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
				PRINTLN("[RCC MISSION] Disabling PLAYER_CONTROL, INPUT_VEH_EXIT. ciBS_RULE11_BLOCK_VEHICLE_EXIT")
			ENDIF
			
			IF IS_PLAYER_IN_CREATOR_AIRCRAFT(LocalPlayer)
				IF NOT IS_SIMPLE_INTERIOR_EXIT_BLOCKED()
					PRINTLN("[RCC MISSION] Set BLOCK_SIMPLE_INTERIOR_EXIT = TRUE, because IS_PLAYER_IN_CREATOR_AIRCRAFT = TRUE ")
					BLOCK_SIMPLE_INTERIOR_EXIT(TRUE)
				ENDIF
			ELSE
				IF IS_SIMPLE_INTERIOR_EXIT_BLOCKED()
				AND NOT IS_AVENGER_HOLD_TRANSITION_BLOCKED()
					PRINTLN("[RCC MISSION] Set BLOCK_SIMPLE_INTERIOR_EXIT = FALSE, because IS_PLAYER_IN_CREATOR_AIRCRAFT = FALSE ")
					BLOCK_SIMPLE_INTERIOR_EXIT(FALSE)
				ENDIF
			ENDIF
		
		ELSE
			
			IF IS_SIMPLE_INTERIOR_EXIT_BLOCKED()
			AND NOT IS_AVENGER_HOLD_TRANSITION_BLOCKED()
				PRINTLN("[RCC MISSION] Set BLOCK_SIMPLE_INTERIOR_EXIT = FALSE, because ciBS_RULE11_BLOCK_VEHICLE_EXIT = FALSE ")
				BLOCK_SIMPLE_INTERIOR_EXIT(FALSE)
			ENDIF

		ENDIF
		PROCESS_RESPAWN_DELUXO_VEHICLE()		
	ENDIF	

	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_CTF_SCREEN_FILTER_ON_HOLDING_FLAG)
			
		IF MC_playerBD[iLocalPart].iObjCarryCount != 0
		AND MC_serverBD.iServerGameState = GAME_STATE_RUNNING
		AND IS_PED_CARRYING_ANY_OBJECTS(LocalPlayerPed)
		AND NOT HAS_TEAM_FINISHED(MC_PlayerBD[iLocalPart].iteam)
			IF NOT ANIMPOSTFX_IS_RUNNING("CrossLine")
				PRINTLN("[RCC MISSION] ciOptionsBS21_CTF_SCREEN_FILTER_ON_HOLDING_FLAG - Playing CrossLine VFX")
				ANIMPOSTFX_PLAY("CrossLine", 0, TRUE)
				PLAY_SOUND_FRONTEND(-1,"Player_Enter_Line","GTAO_FM_Cross_The_Line_Soundset", FALSE)
			ENDIF
		ELSE 
			IF ANIMPOSTFX_IS_RUNNING("CrossLine")
				PRINTLN("[RCC MISSION] ciOptionsBS21_CTF_SCREEN_FILTER_ON_HOLDING_FLAG - Stopping CrossLine VFX")
				ANIMPOSTFX_STOP("CrossLine")
			ENDIF
			
			IF IS_BIT_SET(iLocalBoolCheck29, LBOOL29_DO_DELIVER_FX)
				CLEAR_BIT(iLocalBoolCheck29, LBOOL29_DO_DELIVER_FX)
				ANIMPOSTFX_PLAY("CrossLineOut", 0, FALSE)
				PRINTLN("[CTLSFX] Stopping Local")
				IF IS_BIT_SET(iLocalBoolCheck14, LBOOL14_VFX_CAPTURE_OWNER)
					STRING sSoundSet 
					sSoundSet = "DLC_Apartments_Drop_Zone_Sounds"
					PLAY_SOUND_FRONTEND(-1,"Exit_Capture_Zone",sSoundSet, FALSE)
				ELSE	
					PLAY_SOUND_FRONTEND(-1,"Player_Exit_Line","GTAO_FM_Cross_The_Line_Soundset", FALSE)
				ENDIF
				
			ENDIF
		ENDIF
	ENDIF
	
	SET_TEAMS_AS_PASSIVE()
	
	PROCESS_RAPPELLING_FROM_HELI()
	
	PROCESS_VEHICLE_LEAVE_TEXT()
	
	PROCESS_PLAYER_COP_DECOY()
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFive, ciOptionsBS25_DisablePIMOutfitSwap)
		IF NOT IS_BIT_SET(MPGlobalsAmbience.iAmbBitSet, iABI_BLOCK_MASKS_AND_GEAR)
			SET_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_BLOCK_MASKS_AND_GEAR)
			PRINTLN("[RCC MISSION][Mask] PROCESS_EVERY_FRAME_PLAYER_CHECKS - Setting MPGlobalsAmbience.iAmbBitSet, iABI_BLOCK_MASKS_AND_GEAR")
		ENDIF
	ENDIF
	
	PROCESS_LOCAL_PLAYER_IN_VEHICLE()
	
	PROCESS_SECUROSERV_HACKING_VEH()
		
	PROCESS_CANCEL_DIALOGUE_CALL_ANIMATION()	
	
	PROCESS_LAST_USED_WEAPON()
	
	PROCESS_CALL_PLAYER_VEHICLE()
	
	PROCESS_ENTITY_FLASH_FADE()
	
	CACHE_VEHICLE_DATA()
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_HALLOWEEN_ADVERSARY_2022(g_FMMC_STRUCT.iAdversaryModeType)
	AND IS_RULE_INDEX_VALID(iRule)
	AND MC_playerBD_1[iLocalPart].iCachedPartNumInTeam != -1
		IF iTeam = ciHALLOWEEN_ADVERSARY_HUNTER_TEAM
			SET_HALLOWEEN_ADVERSARY_2022_HUNTER_VFX(iTeam, iRule, MC_playerBD_1[iLocalPart].iCachedPartNumInTeam)
		ELSE
			SET_HALLOWEEN_ADVERSARY_2022_HUNTED_VFX()
		ENDIF
		
		PROCESS_HALLOWEEN_ADVERSARY_2022_SOUNDS(iRule, iTeam, MC_playerBD_1[iLocalPart].iCachedPartNumInTeam)
	ENDIF
	
	PROCESS_KILL_LOCAL_PLAYER_FROM_FIRE()
		
	PROCESS_RUNNING_AROUND_ON_ALL_FOURS_ANIMAL_STYLE()

	//For any rule-specific options 
	PROCESS_LOCAL_PLAYER_RULE_SPECIFIC_OPTIONS(iTeam, iRule)
ENDPROC

PROC PROCESS_LOCAL_PLAYER()
	#IF IS_DEBUG_BUILD
	START_MISSION_CONTROLLER_PROFILING(ciFMMC_PROFILER_LOCAL_PLAYER)
	#ENDIF
		
	PROCESS_STAGGERED_LOCAL_PLAYER_CHECKS()
	
	PROCESS_EVERY_FRAME_LOCAL_PLAYER_CHECKS()
	
	#IF IS_DEBUG_BUILD
	PAUSE_MISSION_CONTROLLER_PROFILING(ciFMMC_PROFILER_LOCAL_PLAYER)
	#ENDIF
ENDPROC
