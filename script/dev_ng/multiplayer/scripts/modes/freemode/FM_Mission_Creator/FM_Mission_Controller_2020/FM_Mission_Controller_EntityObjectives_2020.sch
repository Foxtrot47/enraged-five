// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### System: GTA V Online Mission Controller - Entity Objectives -------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Script: FM_Mission_Controller_2020.sc -----------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### This script was forked from FM_Mission_Controller.sc at the beginning of 2020
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description:
// ##### Shared functionality for rule types that apply to multiple entities:
// ##### Get & Deliver, Capture, Kill/Destroy, Go To etc.
// ##### 
// ##### Player rule processing for the all-objective-entity staggered loop
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

USING "FM_Mission_Controller_Include_2020.sch"

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section: Objective Progression -----------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: 
// ##### 
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC RESET_TEMP_OBJECTIVE_VARS(INT iTeam, INT iNewRuleType = ciRULE_TYPE_NONE) 

	IF iNewRuleType != ciRULE_TYPE_VEHICLE
		iNumHighPriorityVeh[iTeam] = 0
		iTempVehMissionLogic[iTeam] = FMMC_OBJECTIVE_LOGIC_NONE
	ENDIF
	
	IF iNewRuleType != ciRULE_TYPE_GOTO
		iNumHighPriorityLoc[iTeam] = 0
		iTempLocMissionLogic[iTeam] = FMMC_OBJECTIVE_LOGIC_NONE
	ENDIF
	
	IF iNewRuleType != ciRULE_TYPE_OBJECT
		iNumHighPriorityObj[iTeam] = 0
		iTempObjMissionLogic[iTeam] = FMMC_OBJECTIVE_LOGIC_NONE
		iTempObjMissionSubLogic[iTeam] = ci_HACK_SUBLOGIC_NONE
	ENDIF
	
	IF iNewRuleType != ciRULE_TYPE_PED
		iNumHighPriorityPed[iTeam] = 0
		iNumHighPriorityDeadPed[iTeam] = 0
		iTempPedMissionLogic[iTeam] = FMMC_OBJECTIVE_LOGIC_NONE
	ENDIF
	
	IF iNewRuleType != ciRULE_TYPE_PLAYER
		iNumHighPriorityPlayerRule[iTeam] = 0
		iTempPlayerRuleMissionLogic[iTeam] = FMMC_OBJECTIVE_LOGIC_NONE
	ENDIF
	
//	IF iNewRuleType != ciRULE_TYPE_TRAIN
//		iNumHighPriorityTrain[iTeam] = 0
//		iTempTrainMissionLogic[iTeam] = FMMC_OBJECTIVE_LOGIC_NONE
//	ENDIF

ENDPROC

FUNC INT GET_OBJECTIVE_TO_JUMP_TO_FROM_CREATOR_DATA(INT iCreatorData)
	
	IF iCreatorData = ciJumpToObjective_None
		RETURN -1
	ENDIF
	
	IF iCreatorData = ciJumpToObjective_MissionStart
		RETURN 0
	ENDIF

	RETURN iCreatorData
	
ENDFUNC

PROC SET_PROGRESS_OBJECTIVE_FOR_TEAM(INT iTeam, BOOL bSet, INT iNextObjective = -1)

	DEBUG_PRINTCALLSTACK()
	PRINTLN("[OBJECTIVE_PROGRESSION] SET_PROGRESS_OBJECTIVE_FOR_TEAM - Team: ", iTeam, " Set: ", bSet, " Next Objective: ", iNextObjective)
	
	IF bSet
		SET_BIT(MC_serverBD.iServerBitSet1, SBBOOL1_PROGRESS_OBJECTIVE_FOR_TEAM_0 + iTeam)
	ELSE
		CLEAR_BIT(MC_serverBD.iServerBitSet1, SBBOOL1_PROGRESS_OBJECTIVE_FOR_TEAM_0 + iTeam)
	ENDIF
		
	IF iNextObjective > MC_serverBD.iNextObjective[iTeam]
		MC_serverBD.iNextObjective[iTeam] = iNextObjective
		PRINTLN("[OBJECTIVE_PROGRESSION] SET_PROGRESS_OBJECTIVE_FOR_TEAM - Next objective updated to: ", MC_serverBD.iNextObjective[iTeam])
	ENDIF
										
ENDPROC

FUNC INT GET_OBJECTIVE_TYPE_FROM_OBJECTIVE_TARGET(INT iTargetType)
	SWITCH iTargetType
		CASE ci_TARGET_PLAYER			RETURN ciRULE_TYPE_PLAYER
		CASE ci_TARGET_LOCATION			RETURN ciRULE_TYPE_GOTO
		CASE ci_TARGET_PED     			RETURN ciRULE_TYPE_PED
		CASE ci_TARGET_VEHICLE 			RETURN ciRULE_TYPE_VEHICLE
		CASE ci_TARGET_OBJECT  			RETURN ciRULE_TYPE_OBJECT
		DEFAULT							RETURN ciRULE_TYPE_NONE
	ENDSWITCH
ENDFUNC

DEBUGONLY FUNC STRING GET_NAME_FROM_OBJECTIVE_TARGET(INT iTargetType)
	SWITCH iTargetType
		CASE ci_TARGET_PLAYER			RETURN "Player"
		CASE ci_TARGET_LOCATION			RETURN "Location"
		CASE ci_TARGET_PED     			RETURN "Ped"
		CASE ci_TARGET_VEHICLE 			RETURN "Vehicle"
		CASE ci_TARGET_OBJECT  			RETURN "Object"
		DEFAULT							RETURN "INVALID"
	ENDSWITCH
ENDFUNC

FUNC INT GET_PRIORITY_FROM_OBJECTIVE_TARGET(INT iTargetType, INT iObjectiveID, INT iTeam)
	SWITCH iTargetType
		CASE ci_TARGET_PLAYER			RETURN MC_serverBD_4.iPlayerRulePriority[iObjectiveID][iTeam]
		CASE ci_TARGET_LOCATION			RETURN MC_serverBD_4.iGotoLocationDataPriority[iObjectiveID][iTeam]
		CASE ci_TARGET_PED     			RETURN MC_serverBD_4.iPedPriority[iObjectiveID][iTeam]
		CASE ci_TARGET_VEHICLE 			RETURN MC_serverBD_4.iVehPriority[iObjectiveID][iTeam]
		CASE ci_TARGET_OBJECT  			RETURN MC_serverBD_4.iObjPriority[iObjectiveID][iTeam]
		DEFAULT							RETURN -1
	ENDSWITCH
ENDFUNC

PROC SET_PRIORITY_FROM_OBJECTIVE_TARGET(INT iTargetType, INT iObjectiveID, INT iTeam, INT iNewPriority)
	SWITCH iTargetType
	
		CASE ci_TARGET_PLAYER			
			MC_serverBD_4.iPlayerRulePriority[iObjectiveID][iTeam] = iNewPriority
		BREAK
		
		CASE ci_TARGET_LOCATION
			MC_serverBD_4.iGotoLocationDataPriority[iObjectiveID][iTeam] = iNewPriority
		BREAK
		
		CASE ci_TARGET_PED
			MC_serverBD_4.iPedPriority[iObjectiveID][iTeam] = iNewPriority
		BREAK
		
		CASE ci_TARGET_VEHICLE
			MC_serverBD_4.iVehPriority[iObjectiveID][iTeam] = iNewPriority
		BREAK
		
		CASE ci_TARGET_OBJECT  			
			MC_serverBD_4.iObjPriority[iObjectiveID][iTeam] = iNewPriority
		BREAK
		
	ENDSWITCH
ENDPROC

FUNC INT GET_RULE_FROM_OBJECTIVE_TARGET(INT iTargetType, INT iObjectiveID, INT iTeam)
	SWITCH iTargetType
		CASE ci_TARGET_PLAYER			RETURN MC_serverBD_4.iPlayerRule[iObjectiveID][iTeam]
		CASE ci_TARGET_LOCATION			RETURN MC_serverBD_4.iGotoLocationDataRule[iObjectiveID][iTeam]
		CASE ci_TARGET_PED     			RETURN MC_serverBD_4.iPedRule[iObjectiveID][iTeam]
		CASE ci_TARGET_VEHICLE 			RETURN MC_serverBD_4.iVehRule[iObjectiveID][iTeam]
		CASE ci_TARGET_OBJECT  			RETURN MC_serverBD_4.iObjRule[iObjectiveID][iTeam]
		DEFAULT							RETURN -1
	ENDSWITCH
ENDFUNC

PROC SET_RULE_FROM_OBJECTIVE_TARGET(INT iTargetType, INT iObjectiveID, INT iTeam, INT iNewRule)
	SWITCH iTargetType
	
		CASE ci_TARGET_PLAYER			
			MC_serverBD_4.iPlayerRule[iObjectiveID][iTeam] = iNewRule
		BREAK
		
		CASE ci_TARGET_LOCATION
			MC_serverBD_4.iGotoLocationDataRule[iObjectiveID][iTeam] = iNewRule
		BREAK
		
		CASE ci_TARGET_PED
			MC_serverBD_4.iPedRule[iObjectiveID][iTeam] = iNewRule
		BREAK
		
		CASE ci_TARGET_VEHICLE
			MC_serverBD_4.iVehRule[iObjectiveID][iTeam] = iNewRule
		BREAK
		
		CASE ci_TARGET_OBJECT  			
			MC_serverBD_4.iObjRule[iObjectiveID][iTeam] = iNewRule
		BREAK
		
	ENDSWITCH
ENDPROC

FUNC INT GET_PASS_OBJECTIVE_JUMP_TO_FROM_OBJECTIVE_TARGET(INT iTargetType, INT iObjectiveID, INT iTeam)
	SWITCH iTargetType
		CASE ci_TARGET_PLAYER			RETURN g_FMMC_STRUCT.sPlayerRuleData[iObjectiveID].iJumpToObjectivePass[iTeam]
		CASE ci_TARGET_LOCATION		
			MC_ServerBD.iLastCompletedGoToLocation = iObjectiveID
			RETURN g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iObjectiveID].iJumpToObjectivePass[iTeam]
		BREAK
		CASE ci_TARGET_PED     			RETURN g_FMMC_STRUCT_ENTITIES.sPlacedPed[iObjectiveID].iJumpToObjectivePass[iTeam]
		CASE ci_TARGET_VEHICLE 			RETURN g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iObjectiveID].iJumpToObjectivePass[iTeam]
		CASE ci_TARGET_OBJECT  			RETURN g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjectiveID].iJumpToObjectivePass[iTeam]
	ENDSWITCH
	
	RETURN -1
ENDFUNC

FUNC INT GET_FAIL_OBJECTIVE_JUMP_TO_FROM_OBJECTIVE_TARGET(INT iTargetType, INT iObjectiveID, INT iTeam)
	SWITCH iTargetType
		CASE ci_TARGET_PLAYER			RETURN g_FMMC_STRUCT.sPlayerRuleData[iObjectiveID].iJumpToObjectiveFail[iTeam]
		CASE ci_TARGET_LOCATION			RETURN g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iObjectiveID].iJumpToObjectiveFail[iTeam]
		CASE ci_TARGET_PED     			RETURN g_FMMC_STRUCT_ENTITIES.sPlacedPed[iObjectiveID].iJumpToObjectiveFail[iTeam]
		CASE ci_TARGET_VEHICLE 			RETURN g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iObjectiveID].iJumpToObjectiveFail[iTeam]
		CASE ci_TARGET_OBJECT  			RETURN g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjectiveID].iJumpToObjectiveFail[iTeam]
		DEFAULT							RETURN -1
	ENDSWITCH
ENDFUNC

FUNC INT GET_PASS_NEXT_MISSION_FROM_OBJECTIVE_TARGET(INT iTargetType, INT iObjectiveID)
	SWITCH iTargetType
		CASE ci_TARGET_PLAYER			RETURN g_FMMC_STRUCT.sPlayerRuleData[iObjectiveID].iNextMissionToPlayPass
		CASE ci_TARGET_LOCATION			RETURN g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iObjectiveID].sEndMissionData.iNextMissionToPlayPass
		CASE ci_TARGET_PED     			RETURN g_FMMC_STRUCT_ENTITIES.sPlacedPed[iObjectiveID].sEndMissionData.iNextMissionToPlayPass
		CASE ci_TARGET_VEHICLE 			RETURN g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iObjectiveID].sEndMissionData.iNextMissionToPlayPass
		CASE ci_TARGET_OBJECT  			RETURN g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjectiveID].sEndMissionData.iNextMissionToPlayPass
		DEFAULT							RETURN -1
	ENDSWITCH
ENDFUNC

FUNC INT GET_FAIL_NEXT_MISSION_FROM_OBJECTIVE_TARGET(INT iTargetType, INT iObjectiveID)
	SWITCH iTargetType
		CASE ci_TARGET_PLAYER			RETURN g_FMMC_STRUCT.sPlayerRuleData[iObjectiveID].iNextMissionToPlayFail
		CASE ci_TARGET_LOCATION			RETURN g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iObjectiveID].sEndMissionData.iNextMissionToPlayFail
		CASE ci_TARGET_PED     			RETURN g_FMMC_STRUCT_ENTITIES.sPlacedPed[iObjectiveID].sEndMissionData.iNextMissionToPlayFail
		CASE ci_TARGET_VEHICLE 			RETURN g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iObjectiveID].sEndMissionData.iNextMissionToPlayFail
		CASE ci_TARGET_OBJECT  			RETURN g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjectiveID].sEndMissionData.iNextMissionToPlayFail
		DEFAULT							RETURN -1
	ENDSWITCH
ENDFUNC

FUNC INT GET_AGGRO_PASS_NEXT_MISSION_FROM_OBJECTIVE_TARGET(INT iTargetType, INT iObjectiveID)
	
	#IF IS_DEBUG_BUILD
		IF bBlockBranchingOnAggro
			PRINTLN("[OBJECTIVE_PROGRESSION] GET_AGGRO_PASS_NEXT_MISSION_FROM_OBJECTIVE_TARGET - sc_BlockBranchingOnAggro on, no aggro branching!")
			RETURN -1
		ENDIF
	#ENDIF

	SWITCH iTargetType
		CASE ci_TARGET_PLAYER			RETURN -1
		CASE ci_TARGET_LOCATION			RETURN g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iObjectiveID].sEndMissionData.iNextMissionToPlayAggroPass
		CASE ci_TARGET_PED     			RETURN g_FMMC_STRUCT_ENTITIES.sPlacedPed[iObjectiveID].sEndMissionData.iNextMissionToPlayAggroPass
		CASE ci_TARGET_VEHICLE 			RETURN g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iObjectiveID].sEndMissionData.iNextMissionToPlayAggroPass
		CASE ci_TARGET_OBJECT  			RETURN g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjectiveID].sEndMissionData.iNextMissionToPlayAggroPass
		DEFAULT							RETURN -1
	ENDSWITCH
	
ENDFUNC

FUNC INT GET_AGGRO_PASS_BITSET_FOR_NEXT_MISSION_FROM_OBJECTIVE_TARGET(INT iTargetType, INT iObjectiveID)
	
	#IF IS_DEBUG_BUILD
		IF bBlockBranchingOnAggro
			PRINTLN("[OBJECTIVE_PROGRESSION] GET_AGGRO_PASS_BITSET_FOR_NEXT_MISSION_FROM_OBJECTIVE_TARGET - sc_BlockBranchingOnAggro on, no aggro branching!")
			RETURN -1
		ENDIF
	#ENDIF

	SWITCH iTargetType
		CASE ci_TARGET_PLAYER			RETURN -1
		CASE ci_TARGET_LOCATION			RETURN g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iObjectiveID].iAggroIndexBS_Entity_Loc
		CASE ci_TARGET_PED     			RETURN g_FMMC_STRUCT_ENTITIES.sPlacedPed[iObjectiveID].iAggroIndexBS_Entity_Ped
		CASE ci_TARGET_VEHICLE 			RETURN g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iObjectiveID].iAggroIndexBS_Entity_Veh
		CASE ci_TARGET_OBJECT  			RETURN g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjectiveID].iAggroIndexBS_Entity_Obj
		DEFAULT							RETURN -1
	ENDSWITCH
	
ENDFUNC

FUNC INT GET_PASS_END_MOCAP_FROM_OBJECTIVE_TARGET(INT iTargetType, INT iObjectiveID)
	SWITCH iTargetType
		CASE ci_TARGET_PLAYER			RETURN g_FMMC_STRUCT.sPlayerRuleData[iObjectiveID].iEndPassMocap
		CASE ci_TARGET_LOCATION			RETURN g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iObjectiveID].sEndMissionData.iEndPassMocap
		CASE ci_TARGET_PED     			RETURN g_FMMC_STRUCT_ENTITIES.sPlacedPed[iObjectiveID].sEndMissionData.iEndPassMocap
		CASE ci_TARGET_VEHICLE 			RETURN g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iObjectiveID].sEndMissionData.iEndPassMocap
		CASE ci_TARGET_OBJECT  			RETURN g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjectiveID].sEndMissionData.iEndPassMocap
		DEFAULT							RETURN -1
	ENDSWITCH
ENDFUNC

FUNC INT GET_FAIL_END_MOCAP_FROM_OBJECTIVE_TARGET(INT iTargetType, INT iObjectiveID)
	SWITCH iTargetType
		CASE ci_TARGET_PLAYER			RETURN g_FMMC_STRUCT.sPlayerRuleData[iObjectiveID].iEndFailMocap
		CASE ci_TARGET_LOCATION			RETURN g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iObjectiveID].sEndMissionData.iEndFailMocap
		CASE ci_TARGET_PED     			RETURN g_FMMC_STRUCT_ENTITIES.sPlacedPed[iObjectiveID].sEndMissionData.iEndFailMocap
		CASE ci_TARGET_VEHICLE 			RETURN g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iObjectiveID].sEndMissionData.iEndFailMocap
		CASE ci_TARGET_OBJECT  			RETURN g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjectiveID].sEndMissionData.iEndFailMocap
		DEFAULT							RETURN -1
	ENDSWITCH
ENDFUNC

FUNC INT GET_SCRIPTED_CUTSCENE_RULE_FROM_OBJECTIVE_TARGET(INT iTargetType, INT iObjectiveID)
	SWITCH iTargetType
		CASE ci_TARGET_PLAYER			RETURN -1
		CASE ci_TARGET_LOCATION			RETURN g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iObjectiveID].sEndMissionData.iBranchScriptCutRule
		CASE ci_TARGET_PED     			RETURN g_FMMC_STRUCT_ENTITIES.sPlacedPed[iObjectiveID].sEndMissionData.iBranchScriptCutRule
		CASE ci_TARGET_VEHICLE 			RETURN g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iObjectiveID].sEndMissionData.iBranchScriptCutRule
		CASE ci_TARGET_OBJECT  			RETURN g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjectiveID].sEndMissionData.iBranchScriptCutRule
		DEFAULT							RETURN -1
	ENDSWITCH
ENDFUNC

FUNC INT GET_SCRIPTED_CUTSCENE_INDEX_FROM_OBJECTIVE_TARGET(INT iTargetType, INT iObjectiveID)
	SWITCH iTargetType
		CASE ci_TARGET_PLAYER			RETURN -1
		CASE ci_TARGET_LOCATION			RETURN g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iObjectiveID].sEndMissionData.iBranchScriptCutScene
		CASE ci_TARGET_PED     			RETURN g_FMMC_STRUCT_ENTITIES.sPlacedPed[iObjectiveID].sEndMissionData.iBranchScriptCutScene
		CASE ci_TARGET_VEHICLE 			RETURN g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iObjectiveID].sEndMissionData.iBranchScriptCutScene
		CASE ci_TARGET_OBJECT  			RETURN g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjectiveID].sEndMissionData.iBranchScriptCutScene
		DEFAULT							RETURN -1
	ENDSWITCH
ENDFUNC

FUNC INT GET_SPECIFIC_CONTENT_CONTINUITY_TYPE_OBJECTIVE_TARGET(INT iTargetType, INT iObjectiveID)
	SWITCH iTargetType
		CASE ci_TARGET_LOCATION			RETURN g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iObjectiveID].iContentContinuityType
		CASE ci_TARGET_OBJECT  			RETURN g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjectiveID].iContentContinuityType
		DEFAULT							RETURN 0
	ENDSWITCH
ENDFUNC

PROC ADD_NEW_SPAWN_GROUP_TO_BUILD_IN_PLAY_FOR_OBJECTIVE_TARGET(INT iTargetType, INT iObjectiveID)
	SWITCH iTargetType
		CASE ci_TARGET_LOCATION	
			ADD_NEW_SPAWN_GROUP_TO_BUILD_IN_PLAY(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iObjectiveID].iSpawnGroup)
		BREAK
		CASE ci_TARGET_PED   
			ADD_NEW_SPAWN_GROUP_TO_BUILD_IN_PLAY(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iObjectiveID].iSpawnGroup)
		BREAK
		CASE ci_TARGET_VEHICLE 	
			ADD_NEW_SPAWN_GROUP_TO_BUILD_IN_PLAY(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iObjectiveID].iSpawnGroup)
		BREAK
		CASE ci_TARGET_OBJECT
			ADD_NEW_SPAWN_GROUP_TO_BUILD_IN_PLAY(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjectiveID].iSpawnGroup)
		BREAK
	ENDSWITCH
ENDPROC

PROC PROCESS_PASS_CONTINUITY_FOR_OBJECTIVE_TARGET(INT iTargetType, INT iObjectiveID)
	SWITCH iTargetType
		CASE ci_TARGET_LOCATION	
			IF IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_LocationTracking)
				IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iObjectiveID].iContinuityId > -1
				AND NOT IS_BIT_SET(MC_serverBD_1.sMissionContinuityVars.iLocationTrackingBitset, g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iObjectiveID].iContinuityId)
					SET_BIT(MC_serverBD_1.sMissionContinuityVars.iLocationTrackingBitset, g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iObjectiveID].iContinuityId)
					PRINTLN("[OBJECTIVE_PROGRESSION] PROCESS_PASS_CONTINUITY_FOR_OBJECTIVE_TARGET - location ", iObjectiveID, " with continuityID ", g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iObjectiveID].iContinuityId, " was completed")
				ENDIF
			ENDIF
		BREAK
		CASE ci_TARGET_OBJECT
			IF IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_ObjectTracking)
				IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjectiveID].iContinuityId > -1
				AND NOT FMMC_IS_LONG_BIT_SET(MC_serverBD_1.sMissionContinuityVars.iObjectTrackingBitset, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjectiveID].iContinuityId)
					FMMC_SET_LONG_BIT(MC_serverBD_1.sMissionContinuityVars.iObjectTrackingBitset, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjectiveID].iContinuityId)
					PRINTLN("[OBJECTIVE_PROGRESSION] PROCESS_PASS_CONTINUITY_FOR_OBJECTIVE_TARGET - object ", iObjectiveID, " with continuityID ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjectiveID].iContinuityId, " was completed")
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC CALCULATE_NEXT_VALID_OBJECTIVE_GIVEN_TYPE_AND_TEAM(INT iObjectiveType, INT iObjectiveID, INT iTeam, INT iObjectiveToJumpPast)
														
	INT iPriorityOld = GET_PRIORITY_FROM_OBJECTIVE_TARGET(iObjectiveType, iObjectiveID, iTeam)
	INT iExtraObjectiveEntity
	INT iIndexOfExtraObjective
	
	SET_PRIORITY_FROM_OBJECTIVE_TARGET(iObjectiveType, iObjectiveID, iTeam, FMMC_PRIORITY_IGNORE)
	SET_RULE_FROM_OBJECTIVE_TARGET(iObjectiveType, iObjectiveID, iTeam, FMMC_OBJECTIVE_LOGIC_NONE)
	
	FOR iExtraObjectiveEntity = 0 TO MAX_NUM_EXTRA_OBJECTIVE_ENTITIES - 1
		
		iIndexOfExtraObjective = MC_serverBD.iCurrentExtraObjective[iExtraObjectiveEntity][iTeam]
		PRINTLN("[OBJECTIVE_PROGRESSION] CALCULATE_NEXT_VALID_OBJECTIVE_GIVEN_TYPE_AND_TEAM - Extra Objective Entity: ", iExtraObjectiveEntity)
		
		IF g_FMMC_STRUCT.iExtraObjectiveEntityType[iExtraObjectiveEntity] != GET_OBJECTIVE_TYPE_FROM_OBJECTIVE_TARGET(iObjectiveType)
		OR g_FMMC_STRUCT.iExtraObjectiveEntityId[iExtraObjectiveEntity] != iObjectiveID
			RELOOP
		ENDIF
		
		PRINTLN("[OBJECTIVE_PROGRESSION] CALCULATE_NEXT_VALID_OBJECTIVE_GIVEN_TYPE_AND_TEAM - Objective Type: ", GET_OBJECTIVE_TYPE_FROM_OBJECTIVE_TARGET(iObjectiveType), ", Objective ID: ", iObjectiveID)
		
		IF MC_serverBD.iCurrentExtraObjective[iExtraObjectiveEntity][iTeam] >= MAX_NUM_EXTRA_OBJECTIVE_PER_ENTITY
			RELOOP
		ENDIF
			
		PRINTLN("[OBJECTIVE_PROGRESSION] CALCULATE_NEXT_VALID_OBJECTIVE_GIVEN_TYPE_AND_TEAM - Current Extra Objective: ", MC_serverBD.iCurrentExtraObjective[iExtraObjectiveEntity][iTeam])
		
		IF g_FMMC_STRUCT.iExtraObjectivePriority[iExtraObjectiveEntity][iIndexOfExtraObjective][iTeam] >= FMMC_MAX_RULES
		OR g_FMMC_STRUCT.iExtraObjectivePriority[iExtraObjectiveEntity][iIndexOfExtraObjective][iTeam] < 0
			RELOOP
		ENDIF
					
		INT iCreatorRuleType = g_FMMC_STRUCT.iExtraObjectiveRule[iExtraObjectiveEntity][iIndexOfExtraObjective][iTeam]
		INT iControllerRuleType = GET_FMMC_RULE_FROM_CREATOR_RULE(iCreatorRuleType)
		
		PRINTLN("[OBJECTIVE_PROGRESSION] CALCULATE_NEXT_VALID_OBJECTIVE_GIVEN_TYPE_AND_TEAM - iCreatorRuleType: ", iCreatorRuleType, " iControllerRuleType: ", iControllerRuleType)
		
		PRINTLN("[OBJECTIVE_PROGRESSION] CALCULATE_NEXT_VALID_OBJECTIVE_GIVEN_TYPE_AND_TEAM - Old Priority: ", iPriorityOld, " New Priority: ", g_FMMC_STRUCT.iExtraObjectivePriority[iExtraObjectiveEntity][iIndexOfExtraObjective][iTeam])
					
		IF g_FMMC_STRUCT.iExtraObjectivePriority[iExtraObjectiveEntity][iIndexOfExtraObjective][iTeam] = iPriorityOld
			
			PRINTLN("[OBJECTIVE_PROGRESSION] CALCULATE_NEXT_VALID_OBJECTIVE_GIVEN_TYPE_AND_TEAM - NOT ASSIGNING. PREVIOUS PRIORITY IS THE SAME AS EXTRA OBJECTIVE PRIORITY")
			
			MC_serverBD.iCurrentExtraObjective[iExtraObjectiveEntity][iTeam]++
			
			iExtraObjectiveEntity--
			
			RELOOP
			
		ENDIF
		
		//Previous Priority != Extra Objective Priority
		
		IF g_FMMC_STRUCT.iExtraObjectivePriority[iExtraObjectiveEntity][iIndexOfExtraObjective][iTeam] > iObjectiveToJumpPast 
			
			SET_PRIORITY_FROM_OBJECTIVE_TARGET(iObjectiveType, iObjectiveID, iTeam, g_FMMC_STRUCT.iExtraObjectivePriority[iExtraObjectiveEntity][iIndexOfExtraObjective][iTeam])
			SET_RULE_FROM_OBJECTIVE_TARGET(iObjectiveType, iObjectiveID, iTeam, GET_FMMC_RULE_FROM_CREATOR_RULE(g_FMMC_STRUCT.iExtraObjectiveRule[iExtraObjectiveEntity][iIndexOfExtraObjective][iTeam]))
			
			PRINTLN("[OBJECTIVE_PROGRESSION] CALCULATE_NEXT_VALID_OBJECTIVE_GIVEN_TYPE_AND_TEAM - ASSIGNED. Priority: ", GET_PRIORITY_FROM_OBJECTIVE_TARGET(iObjectiveType, iObjectiveID, iTeam), ", Rule: ", GET_RULE_FROM_OBJECTIVE_TARGET(iObjectiveType, iObjectiveID, iTeam))
			
			MC_serverBD.iCurrentExtraObjective[iExtraObjectiveEntity][iTeam]++
			
			//Break out of the loop after this one
			iExtraObjectiveEntity = MAX_NUM_EXTRA_OBJECTIVE_ENTITIES
			
		ELSE
		
			PRINTLN("[OBJECTIVE_PROGRESSION] CALCULATE_NEXT_VALID_OBJECTIVE_GIVEN_TYPE_AND_TEAM - Priority is less than Objective To Jump Past so moving to the next. Priority: ", GET_PRIORITY_FROM_OBJECTIVE_TARGET(iObjectiveType, iObjectiveID, iTeam), ", Current Extra Objective: ",MC_serverBD.iCurrentExtraObjective[iExtraObjectiveEntity][iTeam])
			
			MC_serverBD.iCurrentExtraObjective[iExtraObjectiveEntity][iTeam]++
			
			iExtraObjectiveEntity--
			
		ENDIF
		
		// If we're moving to a kill rule, clear the mission critical flag
		IF iControllerRuleType = FMMC_OBJECTIVE_LOGIC_KILL
			IF iCreatorRuleType = ciSELECTION_KILL_VEHICLE
				SET_VEHICLE_CAN_BE_KILLED_ON_THIS_RULE(iObjectiveID, iTeam)
			ELIF iCreatorRuleType = ciSELECTION_KILL_PED
				SET_PED_CAN_BE_KILLED_ON_THIS_RULE(iObjectiveID, iTeam)
			ELIF iCreatorRuleType = ciSELECTION_KILL_OBJECT
				SET_OBJECT_CAN_BE_KILLED_ON_THIS_RULE(iObjectiveID, iTeam)
			ENDIF
		ENDIF
			
	ENDFOR
		
ENDPROC

PROC MODIFY_SPAWN_GROUP_DURING_GAMEPLAY_FROM_BITSET_OBJECTIVE_COMPLETION(INT iObjectiveType, INT iObjectiveID)
	
	IF iObjectiveID = -1
		EXIT
	ENDIF
	
	SWITCH iObjectiveType
				
		CASE ci_TARGET_LOCATION
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iObjectiveID].iSpawnGroupAdditionalFunctionalityBS, ciSPAWN_GROUP_ADDITIONAL_ENTITY_FUNCTIONALITY_EVENT_IS_COMPLETION)			
				PRINTLN("[OBJECTIVE_PROGRESSION][SpawnGroups] MODIFY_SPAWN_GROUP_DURING_GAMEPLAY_FROM_BITSET_OBJECTIVE_COMPLETION - LOCATION: ", iObjectiveID, " flagged as complete with spawn groups to be modified on this event.")	
				MODIFY_SPAWN_GROUP_DURING_GAMEPLAY_FROM_BITSET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iObjectiveID].iSpawnGroupAdditionalFunctionalityBS, g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iObjectiveID].iModifySpawnGroupOnEvent, g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iObjectiveID].iModifySpawnSubGroupOnEventBS)
			ENDIF
		BREAK
		
		CASE ci_TARGET_PED     
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iObjectiveID].iSpawnGroupAdditionalFunctionalityBS, ciSPAWN_GROUP_ADDITIONAL_ENTITY_FUNCTIONALITY_EVENT_IS_COMPLETION)			
				PRINTLN("[OBJECTIVE_PROGRESSION][SpawnGroups] MODIFY_SPAWN_GROUP_DURING_GAMEPLAY_FROM_BITSET_OBJECTIVE_COMPLETION - PED: ", iObjectiveID, " flagged as complete with spawn groups to be modified on this event.")	
				MODIFY_SPAWN_GROUP_DURING_GAMEPLAY_FROM_BITSET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iObjectiveID].iSpawnGroupAdditionalFunctionalityBS, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iObjectiveID].iModifySpawnGroupOnEvent, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iObjectiveID].iModifySpawnSubGroupOnEventBS)
			ENDIF
		BREAK
		
		CASE ci_TARGET_VEHICLE 
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iObjectiveID].iSpawnGroupAdditionalFunctionalityBS, ciSPAWN_GROUP_ADDITIONAL_ENTITY_FUNCTIONALITY_EVENT_IS_COMPLETION)	
				PRINTLN("[OBJECTIVE_PROGRESSION][SpawnGroups] MODIFY_SPAWN_GROUP_DURING_GAMEPLAY_FROM_BITSET_OBJECTIVE_COMPLETION - VEHICLE: ", iObjectiveID, " flagged as complete with spawn groups to be modified on this event.")	
				MODIFY_SPAWN_GROUP_DURING_GAMEPLAY_FROM_BITSET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iObjectiveID].iSpawnGroupAdditionalFunctionalityBS, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iObjectiveID].iModifySpawnGroupOnEvent, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iObjectiveID].iModifySpawnSubGroupOnEventBS)
			ENDIF
		BREAK
		
		CASE ci_TARGET_OBJECT  
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjectiveID].iSpawnGroupAdditionalFunctionalityBS, ciSPAWN_GROUP_ADDITIONAL_ENTITY_FUNCTIONALITY_EVENT_IS_COMPLETION)			
				PRINTLN("[OBJECTIVE_PROGRESSION][SpawnGroups] MODIFY_SPAWN_GROUP_DURING_GAMEPLAY_FROM_BITSET_OBJECTIVE_COMPLETION - OBJECT: ", iObjectiveID, " flagged as complete with spawn groups to be modified on this event.")	
				MODIFY_SPAWN_GROUP_DURING_GAMEPLAY_FROM_BITSET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjectiveID].iSpawnGroupAdditionalFunctionalityBS, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjectiveID].iModifySpawnGroupOnEvent, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjectiveID].iModifySpawnSubGroupOnEventBS)
			ENDIF
		BREAK
	
	ENDSWITCH
	
	
ENDPROC

PROC PROGRESS_SPECIFIC_OBJECTIVE(INT iObjectiveType, INT iObjectiveID, INT iTeam, BOOL bPass, INT iObjectiveToJumpPast = -1)
	
	INT iObjectiveJumpTo = -1
	INT iContentContinuityType = 0
	
	DEBUG_PRINTCALLSTACK() 
	PRINTLN("[OBJECTIVE_PROGRESSION] PROGRESS_SPECIFIC_OBJECTIVE - Objective Type: ", GET_NAME_FROM_OBJECTIVE_TARGET(iObjectiveType), " Objective ID: ", iObjectiveID, " Team: ", iTeam, " Pass: ", bPass, " Objective to Jump Past: ", iObjectiveToJumpPast)
	
	CALCULATE_NEXT_VALID_OBJECTIVE_GIVEN_TYPE_AND_TEAM(iObjectiveType, iObjectiveID, iTeam, iObjectiveToJumpPast)
	
	PRINTLN("[OBJECTIVE_PROGRESSION] PROGRESS_SPECIFIC_OBJECTIVE - ", GET_NAME_FROM_OBJECTIVE_TARGET(iObjectiveType), " rule new priority: ", GET_PRIORITY_FROM_OBJECTIVE_TARGET(iObjectiveType, iObjectiveID, iTeam), " new rule type: ", GET_RULE_FROM_OBJECTIVE_TARGET(iObjectiveType, iObjectiveID, iTeam))
	
	IF bPass
		
		MODIFY_SPAWN_GROUP_DURING_GAMEPLAY_FROM_BITSET_OBJECTIVE_COMPLETION(iObjectiveType, iObjectiveID)
			
		iObjectiveJumpTo = GET_OBJECTIVE_TO_JUMP_TO_FROM_CREATOR_DATA(GET_PASS_OBJECTIVE_JUMP_TO_FROM_OBJECTIVE_TARGET(iObjectiveType, iObjectiveID, iTeam))
		PRINTLN("[OBJECTIVE_PROGRESSION] PROGRESS_SPECIFIC_OBJECTIVE - ", GET_NAME_FROM_OBJECTIVE_TARGET(iObjectiveType), " rule pass, Objective Jump To: ", iObjectiveJumpTo)
	
		IF iObjectiveJumpTo >= 0
		AND iObjectiveJumpTo != MC_serverBD_4.iCurrentHighestPriority[iTeam]
			SET_PROGRESS_OBJECTIVE_FOR_TEAM(iTeam, TRUE, iObjectiveJumpTo)
			PRINTLN("[OBJECTIVE_PROGRESSION] PROGRESS_SPECIFIC_OBJECTIVE - ", GET_NAME_FROM_OBJECTIVE_TARGET(iObjectiveType), " rule pass, next objective: ", MC_serverBD.iNextObjective[iTeam])
		ENDIF
		
		IF GET_PASS_NEXT_MISSION_FROM_OBJECTIVE_TARGET(iObjectiveType, iObjectiveID) > 0
			MC_serverBD.iNextMission = GET_PASS_NEXT_MISSION_FROM_OBJECTIVE_TARGET(iObjectiveType, iObjectiveID)
			PRINTLN("[OBJECTIVE_PROGRESSION] PROGRESS_SPECIFIC_OBJECTIVE - ", GET_NAME_FROM_OBJECTIVE_TARGET(iObjectiveType), " rule pass, next mission: ", MC_serverBD.iNextMission)
		ENDIF
		
		IF GET_AGGRO_PASS_NEXT_MISSION_FROM_OBJECTIVE_TARGET(iObjectiveType, iObjectiveID) > 0
		AND HAS_ANY_TEAM_TRIGGERED_AGGRO(GET_AGGRO_PASS_BITSET_FOR_NEXT_MISSION_FROM_OBJECTIVE_TARGET(iObjectiveType, iObjectiveID))
			MC_serverBD.iNextMission = GET_AGGRO_PASS_NEXT_MISSION_FROM_OBJECTIVE_TARGET(iObjectiveType, iObjectiveID)
			PRINTLN("[OBJECTIVE_PROGRESSION] PROGRESS_SPECIFIC_OBJECTIVE - ", GET_NAME_FROM_OBJECTIVE_TARGET(iObjectiveType), " Rule Aggro Triggered, next mission: ",MC_serverBD.iNextMission)
			IF NOT IS_BIT_SET(MC_serverBD_1.sMissionContinuityVars.iGenericTrackingBitset, ciContinuityGenericTrackingBS_DroppedToDirect)
				SET_BIT(MC_serverBD_1.sMissionContinuityVars.iGenericTrackingBitset, ciContinuityGenericTrackingBS_DroppedToDirect)
			ENDIF
		ENDIF
		
		IF GET_PASS_END_MOCAP_FROM_OBJECTIVE_TARGET(iObjectiveType, iObjectiveID) > 0
			MC_serverBD.iEndMocapVariation = GET_PASS_END_MOCAP_FROM_OBJECTIVE_TARGET(iObjectiveType, iObjectiveID)
			PRINTLN("[OBJECTIVE_PROGRESSION] PROGRESS_SPECIFIC_OBJECTIVE - ", GET_NAME_FROM_OBJECTIVE_TARGET(iObjectiveType), " rule pass, mocap variation: ", MC_serverBD.iEndMocapVariation)
		ENDIF
		
		IF GET_SCRIPTED_CUTSCENE_RULE_FROM_OBJECTIVE_TARGET(iObjectiveType, iObjectiveID) > -1
		AND GET_SCRIPTED_CUTSCENE_INDEX_FROM_OBJECTIVE_TARGET(iObjectiveType, iObjectiveID) > -1
			MC_serverBD.iOverriddenScriptedCutsceneRule[iTeam] = GET_SCRIPTED_CUTSCENE_RULE_FROM_OBJECTIVE_TARGET(iObjectiveType, iObjectiveID)
			MC_serverBD.iOverriddenScriptedCutsceneIndex[iTeam] = GET_SCRIPTED_CUTSCENE_INDEX_FROM_OBJECTIVE_TARGET(iObjectiveType, iObjectiveID)
			PRINTLN("[OBJECTIVE_PROGRESSION] PROGRESS_SPECIFIC_OBJECTIVE - ", GET_NAME_FROM_OBJECTIVE_TARGET(iObjectiveType), " rule pass, scripted cut override. rule: ",MC_serverBD.iOverriddenScriptedCutsceneRule[iTeam], " index: ", MC_serverBD.iOverriddenScriptedCutsceneIndex[iTeam])
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_ContentSpecificTracking)
			iContentContinuityType = GET_SPECIFIC_CONTENT_CONTINUITY_TYPE_OBJECTIVE_TARGET(iObjectiveType, iObjectiveID)
			IF iContentContinuityType > 0
				PROCESS_SPECIFIC_CONTENT_CONTINUITY_TYPE_TELEMETRY(iContentContinuityType)
			
				SET_BIT(MC_serverBD_1.sMissionContinuityVars.iContentSpecificContinuityBitset, iContentContinuityType)
				PRINTLN("[CONTINUITY][OBJECTIVE_PROGRESSION] PROGRESS_SPECIFIC_OBJECTIVE - ", GET_NAME_FROM_OBJECTIVE_TARGET(iObjectiveType), " rule pass, Content Continuity Type Set: ", iContentContinuityType)
			ENDIF
		ENDIF
		
		SWITCH iObjectiveType
			CASE ci_TARGET_OBJECT
				IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjectiveID].iInteriorDestructionIndex > -1
					BROADCAST_FMMC_INTERIOR_DESTRUCTION_EVENT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjectiveID].iInteriorDestructionIndex)
				ENDIF
										
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjectiveID].sObjectRenderTargetOptions.iSharedRT_Bitset, ciSharedRTBitset_GenericStartOnObjectiveComplete)
					PRINTLN("[RCC MISSION][SharedRTs] PROGRESS_SPECIFIC_OBJECTIVE - iObjectiveID: ", iObjectiveID, " sending ciSHARED_RT_EVENT_TYPE__GENERIC_START event due to ciSharedRTBitset_GenericStartOnObjectiveComplete!") 
					BROADCAST_FMMC_SHARED_RENDERTARGET_EVENT(ciSHARED_RT_EVENT_TYPE__GENERIC_START, CREATION_TYPE_OBJECTS, iObjectiveID)
				ENDIF
			BREAK
			
			CASE ci_TARGET_LOCATION
				IF bPass
				AND g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iObjectiveID].iLocPMCOverride > -1
					PRINTLN("[RCC MISSION] PROGRESS_SPECIFIC_OBJECTIVE - iObjectiveID: ", iObjectiveID, " sending g_ciInstancedcontentEventType_Override_PMC event due to g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iObjectiveID].iLocPMCOverride: ", g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iObjectiveID].iLocPMCOverride) 
					BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedcontentEventType_Override_PMC, DEFAULT, DEFAULT, DEFAULT, DEFAULt, DEFAULT, DEFAULt, DEFAULT, g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iObjectiveID].iLocPMCOverride)
				ENDIF
			BREAK
		ENDSWITCH
		
		ADD_NEW_SPAWN_GROUP_TO_BUILD_IN_PLAY_FOR_OBJECTIVE_TARGET(iObjectiveType, iObjectiveID)
		
		PROCESS_PASS_CONTINUITY_FOR_OBJECTIVE_TARGET(iObjectiveType, iObjectiveID)
		
	ELSE
	
		iObjectiveJumpTo = GET_OBJECTIVE_TO_JUMP_TO_FROM_CREATOR_DATA(GET_FAIL_OBJECTIVE_JUMP_TO_FROM_OBJECTIVE_TARGET(iObjectiveType, iObjectiveID, iTeam))
		PRINTLN("[OBJECTIVE_PROGRESSION] PROGRESS_SPECIFIC_OBJECTIVE - ", GET_NAME_FROM_OBJECTIVE_TARGET(iObjectiveType), " rule fail, Objective Jump To: ", iObjectiveJumpTo)
	
		IF iObjectiveJumpTo >= 0
		AND iObjectiveJumpTo != MC_serverBD_4.iCurrentHighestPriority[iTeam]
			SET_PROGRESS_OBJECTIVE_FOR_TEAM(iTeam, TRUE, iObjectiveJumpTo)
			PRINTLN("[OBJECTIVE_PROGRESSION] PROGRESS_SPECIFIC_OBJECTIVE - ", GET_NAME_FROM_OBJECTIVE_TARGET(iObjectiveType), " rule fail, next objective: ", MC_serverBD.iNextObjective[iTeam])
		ENDIF
		
		IF GET_FAIL_NEXT_MISSION_FROM_OBJECTIVE_TARGET(iObjectiveType, iObjectiveID) > 0
			MC_serverBD.iNextMission = GET_FAIL_NEXT_MISSION_FROM_OBJECTIVE_TARGET(iObjectiveType, iObjectiveID)
			PRINTLN("[OBJECTIVE_PROGRESSION] PROGRESS_SPECIFIC_OBJECTIVE - ", GET_NAME_FROM_OBJECTIVE_TARGET(iObjectiveType), " rule fail, next mission: ", MC_serverBD.iNextMission)
		ENDIF
		
		IF GET_FAIL_END_MOCAP_FROM_OBJECTIVE_TARGET(iObjectiveType, iObjectiveID) > 0
			MC_serverBD.iEndMocapVariation = GET_FAIL_END_MOCAP_FROM_OBJECTIVE_TARGET(iObjectiveType, iObjectiveID)
			PRINTLN("[OBJECTIVE_PROGRESSION] PROGRESS_SPECIFIC_OBJECTIVE - ", GET_NAME_FROM_OBJECTIVE_TARGET(iObjectiveType), " rule fail, mocap variation: ", MC_serverBD.iEndMocapVariation)
		ENDIF
	
	ENDIF

ENDPROC

PROC RESET_SERVER_TARGETTYPES_FOR_ENTITY(INT iNumber, INT iTeam, INT iNewPriority, INT& iNewExtraObjectiveID, INT& iNewExtraObjectiveArrayPos, INT iTargetType = ci_TARGET_NONE)
	
	BOOL bCheckExtraObjectives = FALSE
	INT iRuleType = ciRULE_TYPE_NONE
	INT iPriority = FMMC_PRIORITY_IGNORE
	
	iNewExtraObjectiveID = -1
	iNewExtraObjectiveArrayPos = -1
	
	SWITCH iTargetType
	
		CASE ci_TARGET_PLAYER
			IF g_FMMC_STRUCT.sPlayerRuleData[iNumber].iPriority[iTeam] < FMMC_MAX_RULES
			AND g_FMMC_STRUCT.sPlayerRuleData[iNumber].iRule[iTeam] != FMMC_OBJECTIVE_LOGIC_NONE
				iPriority = g_FMMC_STRUCT.sPlayerRuleData[iNumber].iPriority[iTeam]
				
				IF iNewPriority <= iPriority
					PRINTLN("[OBJECTIVE_PROGRESSION] RESET_SERVER_TARGETTYPES_FOR_ENTITY - PLAYER RULE ", iNumber, " is an objective for team ", iTeam, ", priority ", iPriority)
					MC_serverBD.iTargetType[iPriority][iTeam] = iTargetType
				ENDIF
			ENDIF
		BREAK
		
		CASE ci_TARGET_LOCATION
			IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iNumber].iPriority[iTeam] < FMMC_MAX_RULES
				iPriority = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iNumber].iPriority[iTeam]
				
				IF iNewPriority <= iPriority
					
					PRINTLN("[OBJECTIVE_PROGRESSION] RESET_SERVER_TARGETTYPES_FOR_ENTITY - LOCATION ", iNumber, " is an objective for team ", iTeam, ", priority ", iPriority)
					
					MC_serverBD.iRuleReCapture[iTeam][iPriority] = GET_FMMC_OBJECTIVE_CAPTURES_OR_KILLS_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iReCapture[iPriority])
					
					IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iNumber].iRule[iTeam] = FMMC_OBJECTIVE_LOGIC_CAPTURE
						SET_BIT(MC_serverBD.iLocteamFailBitset[iNumber],iTeam)
					ENDIF
					
					IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iNumber].iRule[iTeam] != FMMC_OBJECTIVE_LOGIC_NONE
						MC_serverBD.iTargetType[iPriority][iTeam] = iTargetType
						
						bCheckExtraObjectives = TRUE
						iRuleType = ciRULE_TYPE_GOTO
					ENDIF
				ELSE
					IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iNumber].iRule[iTeam] != FMMC_OBJECTIVE_LOGIC_NONE
						PRINTLN("[OBJECTIVE_PROGRESSION] RESET_SERVER_TARGETTYPES_FOR_ENTITY - LOCATION ", iNumber, " isn't an objective for team ", iTeam, ", priority ", iPriority, ", but it might have extra objectives")
						bCheckExtraObjectives = TRUE
						iRuleType = ciRULE_TYPE_GOTO
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE ci_TARGET_PED
			IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iNumber].iPriority[iTeam] < FMMC_MAX_RULES
				iPriority = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iNumber].iPriority[iTeam]
				
				IF iNewPriority <= iPriority
				
					PRINTLN("[OBJECTIVE_PROGRESSION] RESET_SERVER_TARGETTYPES_FOR_ENTITY - PED ", iNumber, " is an objective for team ", iTeam, ", priority ", iPriority)
					
					MC_serverBD.iRuleReCapture[iTeam][iPriority] = GET_FMMC_OBJECTIVE_CAPTURES_OR_KILLS_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iReCapture[iPriority])
					
					IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iNumber].iRule[iTeam] != FMMC_OBJECTIVE_LOGIC_NONE
						SET_BIT(MC_serverBD.iPedteamFailBitset[iNumber],iTeam)
						MC_serverBD.iTargetType[iPriority][iTeam] = iTargetType
						
						bCheckExtraObjectives = TRUE
						iRuleType = ciRULE_TYPE_PED
						
						IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iNumber].iRule[iTeam] = FMMC_OBJECTIVE_LOGIC_PHOTO
							IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iPhotoCanBeDead, iPriority)
								FMMC_SET_LONG_BIT(MC_serverBD.iDeadPedPhotoBitset[iTeam], iNumber)
							ENDIF	
						ENDIF
					ENDIF
				ELSE
					IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iNumber].iRule[iTeam] != FMMC_OBJECTIVE_LOGIC_NONE
						PRINTLN("[OBJECTIVE_PROGRESSION] RESET_SERVER_TARGETTYPES_FOR_ENTITY - PED ", iNumber, " isn't an objective for team ", iTeam, ", priority ", iPriority, ", but it might have extra objectives")
						bCheckExtraObjectives = TRUE
						iRuleType = ciRULE_TYPE_PED
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE ci_TARGET_VEHICLE
			IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iNumber].iPriority[iTeam] < FMMC_MAX_RULES
				iPriority = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iNumber].iPriority[iTeam]
				
				IF iNewPriority <= iPriority
					
					PRINTLN("[OBJECTIVE_PROGRESSION] RESET_SERVER_TARGETTYPES_FOR_ENTITY - VEHICLE ", iNumber, " is an objective for team ", iTeam, ", priority ", iPriority)
					
					MC_serverBD.iRuleReCapture[iTeam][iPriority] = GET_FMMC_OBJECTIVE_CAPTURES_OR_KILLS_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iReCapture[iPriority])
					
					IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iNumber].iRule[iTeam] != FMMC_OBJECTIVE_LOGIC_NONE
						SET_BIT(MC_serverBD.iVehteamFailBitset[iNumber],iTeam)
						MC_serverBD.iTargetType[iPriority][iTeam] = iTargetType
						
						bCheckExtraObjectives = TRUE
						iRuleType = ciRULE_TYPE_VEHICLE
					ENDIF
				ELSE
					IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iNumber].iRule[iTeam] != FMMC_OBJECTIVE_LOGIC_NONE
						PRINTLN("[OBJECTIVE_PROGRESSION] RESET_SERVER_TARGETTYPES_FOR_ENTITY - VEHICLE ", iNumber, " isn't an objective for team ", iTeam, ", priority ", iPriority, ", but it might have extra objectives")
						bCheckExtraObjectives = TRUE
						iRuleType = ciRULE_TYPE_VEHICLE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE ci_TARGET_OBJECT
			IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iNumber].iPriority[iTeam] < FMMC_MAX_RULES
				iPriority = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iNumber].iPriority[iTeam]
				
				IF iNewPriority <= iPriority
					
					PRINTLN("[OBJECTIVE_PROGRESSION] RESET_SERVER_TARGETTYPES_FOR_ENTITY - OBJECT ", iNumber, " is an objective for team ", iTeam, ", priority ", iPriority)
					
					MC_serverBD.iRuleReCapture[iTeam][iPriority] = GET_FMMC_OBJECTIVE_CAPTURES_OR_KILLS_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iReCapture[iPriority])
					
					IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iNumber].iRule[iTeam] != FMMC_OBJECTIVE_LOGIC_NONE
						SET_BIT(MC_serverBD.iObjteamFailBitset[iNumber], iTeam)
						MC_serverBD.iTargetType[iPriority][iTeam] = iTargetType
						
						bCheckExtraObjectives = TRUE
						iRuleType = ciRULE_TYPE_OBJECT

					ENDIF
					
				ELSE
					IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iNumber].iRule[iTeam] != FMMC_OBJECTIVE_LOGIC_NONE
						PRINTLN("[OBJECTIVE_PROGRESSION] RESET_SERVER_TARGETTYPES_FOR_ENTITY - OBJECT ", iNumber, " isn't an objective for team ", iTeam, ", priority ", iPriority, ", but it might have extra objectives")
						bCheckExtraObjectives = TRUE
						iRuleType = ciRULE_TYPE_OBJECT
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	IF !bCheckExtraObjectives
		EXIT
	ENDIF
		
	INT iExtraObjectiveNum
	
	FOR iExtraObjectiveNum = 0 TO MAX_NUM_EXTRA_OBJECTIVE_ENTITIES - 1
		
		// If the type matches the one passed
		IF g_FMMC_STRUCT.iExtraObjectiveEntityType[iExtraObjectiveNum] != iRuleType
		OR g_FMMC_STRUCT.iExtraObjectiveEntityId[iExtraObjectiveNum] != iNumber
			RELOOP
		ENDIF
			
		iNewExtraObjectiveID = iExtraObjectiveNum
		
		INT iExtraObjectiveLoop = 0
		
		FOR iExtraObjectiveLoop = 0 TO MAX_NUM_EXTRA_OBJECTIVE_PER_ENTITY - 1
			
			IF g_FMMC_STRUCT.iExtraObjectivePriority[iExtraObjectiveNum][iExtraObjectiveLoop][iTeam] >= FMMC_MAX_RULES
			OR g_FMMC_STRUCT.iExtraObjectivePriority[iExtraObjectiveNum][iExtraObjectiveLoop][iTeam] < 0
			OR GET_FMMC_RULE_FROM_CREATOR_RULE(g_FMMC_STRUCT.iExtraObjectiveRule[iExtraObjectiveNum][iExtraObjectiveLoop][iTeam]) = FMMC_OBJECTIVE_LOGIC_NONE
				RELOOP
			ENDIF
			
			iPriority = g_FMMC_STRUCT.iExtraObjectivePriority[iExtraObjectiveNum][iExtraObjectiveLoop][iTeam]
			INT iRule = GET_FMMC_RULE_FROM_CREATOR_RULE(g_FMMC_STRUCT.iExtraObjectiveRule[iExtraObjectiveNum][iExtraObjectiveLoop][iTeam])
			
			PRINTLN("[OBJECTIVE_PROGRESSION] RESET_SERVER_TARGETTYPES_FOR_ENTITY - (extra objective entity) target type ", iTargetType, " for team ", iTeam, ", priority ", iPriority)
			
			IF iNewPriority <= iPriority
				
				IF iNewExtraObjectiveArrayPos = -1
					iNewExtraObjectiveArrayPos = iExtraObjectiveLoop
				ENDIF
				
				MC_serverBD.iTargetType[iPriority][iTeam] = iTargetType
				
				SWITCH iTargetType
					CASE ci_TARGET_LOCATION
						MC_serverBD.iRuleReCapture[iTeam][iPriority] = GET_FMMC_OBJECTIVE_CAPTURES_OR_KILLS_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iReCapture[iPriority])
						
						IF iRule = FMMC_OBJECTIVE_LOGIC_CAPTURE
							SET_BIT(MC_serverBD.iLocteamFailBitset[iNumber],iTeam)
						ENDIF
					BREAK
					
					CASE ci_TARGET_PED
						MC_serverBD.iRuleReCapture[iTeam][iPriority] = GET_FMMC_OBJECTIVE_CAPTURES_OR_KILLS_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iReCapture[iPriority])
						
						IF iRule = FMMC_OBJECTIVE_LOGIC_PHOTO
							IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iPhotoCanBeDead,iPriority)
								FMMC_SET_LONG_BIT(MC_serverBD.iDeadPedPhotoBitset[iTeam], iNumber)
							ENDIF
						ENDIF
					BREAK
					
					CASE ci_TARGET_VEHICLE
						MC_serverBD.iRuleReCapture[iTeam][iPriority] = GET_FMMC_OBJECTIVE_CAPTURES_OR_KILLS_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iReCapture[iPriority])
					BREAK
					
					CASE ci_TARGET_OBJECT
						MC_serverBD.iRuleReCapture[iTeam][iPriority] = GET_FMMC_OBJECTIVE_CAPTURES_OR_KILLS_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iReCapture[iPriority])
						
						IF iRule = FMMC_OBJECTIVE_LOGIC_MINIGAME
							SET_BIT(MC_serverBD.iWasHackObj, iNumber)
						ENDIF
					BREAK
				ENDSWITCH
				
			ENDIF
			
		ENDFOR
		
		//We found the multi-rule number for this entity, there won't be any more so break out of the objective entity loop:
		BREAKLOOP
		
	ENDFOR
		
	
ENDPROC

PROC REVALIDATE_BACK_TO_AND_INCLUDING_OBJECTIVE(INT iTeam, INT iObjective)
	
	INT i, iPriority, iRule
	INT iNewPriority = FMMC_MAX_RULES
	INT iNewRule = FMMC_OBJECTIVE_LOGIC_NONE
	INT iNewExtraObjectiveID, iNewExtraObjectiveArrayPos
	
	DEBUG_PRINTCALLSTACK() 
	PRINTLN("[OBJECTIVE_PROGRESSION] REVALIDATE_BACK_TO_AND_INCLUDING_OBJECTIVE - called for team ", iTeam, " back to objective ", iObjective)
	
	IF MC_serverBD.iNumPlayerRuleCreated > 0
		FOR i = 0 TO (MC_serverBD.iNumPlayerRuleCreated - 1)
			iPriority = g_FMMC_STRUCT.sPlayerRuleData[i].iPriority[iTeam]
			iRule = g_FMMC_STRUCT.sPlayerRuleData[i].iRule[iTeam]
			iNewPriority = FMMC_MAX_RULES
			iNewRule = FMMC_OBJECTIVE_LOGIC_NONE
			
			IF iPriority < FMMC_MAX_RULES
			AND iRule != FMMC_OBJECTIVE_LOGIC_NONE
				
				//If we're jumping back to before the main priority of this player rule
				IF iObjective <= iPriority
					IF MC_serverBD_4.iPlayerRulePriority[i][iTeam] != iPriority
					OR MC_serverBD_4.iPlayerRule[i][iTeam] != iRule
						iNewPriority = iPriority
						iNewRule = iRule
						RESET_SERVER_TARGETTYPES_FOR_ENTITY(i, iTeam, iObjective, iNewExtraObjectiveID, iNewExtraObjectiveArrayPos, ci_TARGET_PLAYER)
					ENDIF
				ENDIF
				
				IF iNewPriority < FMMC_MAX_RULES
				AND iNewRule != FMMC_OBJECTIVE_LOGIC_NONE
					PRINTLN("[OBJECTIVE_PROGRESSION] REVALIDATE_BACK_TO_AND_INCLUDING_OBJECTIVE - team ", iTeam, " - Player Rule ", i, " gets new priority ", iNewPriority, " and rule ", iNewRule)
					
					MC_serverBD_4.iPlayerRulePriority[i][iTeam] = iNewPriority
					MC_serverBD_4.iPlayerRule[i][iTeam] = iNewRule
					
					IF iNewExtraObjectiveID != -1
					AND iNewExtraObjectiveArrayPos != -1
						MC_serverBD.iCurrentExtraObjective[iNewExtraObjectiveID][iTeam] = iNewExtraObjectiveArrayPos
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	
	IF MC_serverBD.iNumLocCreated > 0
		INT iLocResetBS
		FOR i = 0 TO (MC_serverBD.iNumLocCreated - 1)
			iPriority = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].iPriority[iTeam]
			iRule = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].iRule[iTeam]
			iNewPriority = FMMC_MAX_RULES
			iNewRule = FMMC_OBJECTIVE_LOGIC_NONE
			
			IF iPriority < FMMC_MAX_RULES
			AND iRule != FMMC_OBJECTIVE_LOGIC_NONE
			AND MC_DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP(i, eSGET_GotoLoc)

				//If we're jumping back to before the main priority of this location
				IF iObjective <= iPriority
					IF MC_serverBD_4.iGotoLocationDataPriority[i][iTeam] != iPriority
					OR MC_serverBD_4.iGotoLocationDataRule[i][iTeam] != iRule
						iNewPriority = iPriority
						iNewRule = iRule
						RESET_SERVER_TARGETTYPES_FOR_ENTITY(i, iTeam, iObjective, iNewExtraObjectiveID, iNewExtraObjectiveArrayPos, ci_TARGET_LOCATION)
					ENDIF
				ELSE
					RESET_SERVER_TARGETTYPES_FOR_ENTITY(i, iTeam, iObjective, iNewExtraObjectiveID, iNewExtraObjectiveArrayPos, ci_TARGET_LOCATION)
					
					IF iNewExtraObjectiveID != -1
					AND iNewExtraObjectiveArrayPos != -1
						iNewPriority = g_FMMC_STRUCT.iExtraObjectivePriority[iNewExtraObjectiveID][iNewExtraObjectiveArrayPos][iTeam]
						iNewRule = GET_FMMC_RULE_FROM_CREATOR_RULE(g_FMMC_STRUCT.iExtraObjectiveRule[iNewExtraObjectiveID][iNewExtraObjectiveArrayPos][iTeam])
					ENDIF
				ENDIF
				
				IF iNewPriority < FMMC_MAX_RULES
				AND iNewRule != FMMC_OBJECTIVE_LOGIC_NONE
					PRINTLN("[OBJECTIVE_PROGRESSION] REVALIDATE_BACK_TO_AND_INCLUDING_OBJECTIVE - team ", iTeam, " - Location ", i, " gets new priority ", iNewPriority, " and rule ", iNewRule)
					
					MC_serverBD_4.iGotoLocationDataPriority[i][iTeam] = iNewPriority
					MC_serverBD_4.iGotoLocationDataRule[i][iTeam] = iNewRule
					
					SET_BIT(iLocResetBS, i)
					
					IF iNewExtraObjectiveID != -1
					AND iNewExtraObjectiveArrayPos != -1
						MC_serverBD.iCurrentExtraObjective[iNewExtraObjectiveID][iTeam] = iNewExtraObjectiveArrayPos
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
		
		IF iLocResetBS != 0
			BROADCAST_FMMC_OBJECTIVE_REVALIDATE(iTeam, iLocResetBS)
		ENDIF
		
	ENDIF
	
	IF MC_serverBD.iNumPedCreated > 0
		FOR i = 0 TO (MC_serverBD.iNumPedCreated - 1)
			iPriority = g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPriority[iTeam]
			iRule = g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iRule[iTeam]
			iNewPriority = FMMC_MAX_RULES
			iNewRule = FMMC_OBJECTIVE_LOGIC_NONE
			
			IF iPriority < FMMC_MAX_RULES
			AND iRule != FMMC_OBJECTIVE_LOGIC_NONE
			AND MC_DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP(i, eSGET_Ped)
				
				//If we're jumping back to before the main priority of this ped
				IF iObjective <= iPriority
					IF MC_serverBD_4.iPedPriority[i][iTeam] != iPriority
					OR MC_serverBD_4.iPedRule[i][iTeam] != iRule
						iNewPriority = iPriority
						iNewRule = iRule
						RESET_SERVER_TARGETTYPES_FOR_ENTITY(i, iTeam, iObjective, iNewExtraObjectiveID, iNewExtraObjectiveArrayPos, ci_TARGET_PED)
					ENDIF
				ELSE
					RESET_SERVER_TARGETTYPES_FOR_ENTITY(i, iTeam, iObjective, iNewExtraObjectiveID, iNewExtraObjectiveArrayPos, ci_TARGET_PED)
					
					IF iNewExtraObjectiveID != -1
					AND iNewExtraObjectiveArrayPos != -1
						iNewPriority = g_FMMC_STRUCT.iExtraObjectivePriority[iNewExtraObjectiveID][iNewExtraObjectiveArrayPos][iTeam]
						iNewRule = GET_FMMC_RULE_FROM_CREATOR_RULE(g_FMMC_STRUCT.iExtraObjectiveRule[iNewExtraObjectiveID][iNewExtraObjectiveArrayPos][iTeam])
					ENDIF
				ENDIF
				
				IF iNewPriority < FMMC_MAX_RULES
				AND iNewRule != FMMC_OBJECTIVE_LOGIC_NONE
					IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedBitsetFifteen, ciPED_BSFifteen_DontRevalidateEntityForObjective)
						PRINTLN("[OBJECTIVE_PROGRESSION] REVALIDATE_BACK_TO_AND_INCLUDING_OBJECTIVE - team ", iTeam, " - Ped ", i, " gets new priority ", iNewPriority, " and rule ", iNewRule)
					
						MC_serverBD_4.iPedPriority[i][iTeam] = iNewPriority
						MC_serverBD_4.iPedRule[i][iTeam] = iNewRule
						
						IF iNewExtraObjectiveID != -1
						AND iNewExtraObjectiveArrayPos != -1
							MC_serverBD.iCurrentExtraObjective[iNewExtraObjectiveID][iTeam] = iNewExtraObjectiveArrayPos
						ENDIF
						
						MC_serverBD_2.iCurrentPedRespawnLives[i] = GET_ENTITY_STARTING_RESPAWN_LIVES(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedRespawnLives)
						
						IF MC_serverBD_2.iCurrentPedRespawnLives[i] != UNLIMITED_RESPAWNS
						AND NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[i])
							MC_serverBD_2.iCurrentPedRespawnLives[i]++
						ENDIF
						
						SET_PED_CAN_BE_KILLED_ON_THIS_RULE(i, iTeam)
						
						IF (NOT CAN_PED_STILL_SPAWN_IN_MISSION(i))
						AND CAN_PED_STILL_SPAWN_IN_MISSION(i, iTeam, iObjective) // If the ped will be able to spawn again:
							INT iTeamLoop
							FOR iTeamLoop = 0 TO (FMMC_MAX_TEAMS - 1)
								IF IS_LONG_BIT_SET(g_FMMC_STRUCT.iMissionCriticalPedDoubleBS[iTeamLoop], i) //Used Externally IS_LONG_BIT_SET
									FMMC_SET_LONG_BIT(MC_serverBD.iMissionCriticalPed[iTeamLoop], i)
								ENDIF
							ENDFOR
						ENDIF
					ELSE
						PRINTLN("[OBJECTIVE_PROGRESSION] REVALIDATE_BACK_TO_AND_INCLUDING_OBJECTIVE - team ", iTeam, " - Ped ", i, " NOT GETTING DUE TO BLOCK new priority ", iNewPriority, " and rule ", iNewRule)
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	
	IF MC_serverBD.iNumVehCreated > 0
		FOR i = 0 TO (MC_serverBD.iNumVehCreated - 1)
			iPriority = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iPriority[iTeam]
			iRule = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iRule[iTeam]
			iNewPriority = FMMC_MAX_RULES
			iNewRule = FMMC_OBJECTIVE_LOGIC_NONE
			
			IF iPriority < FMMC_MAX_RULES
			AND iRule != FMMC_OBJECTIVE_LOGIC_NONE
			AND MC_DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP(i, eSGET_Vehicle)
				
				//If we're jumping back to before the main priority of this vehicle
				IF iObjective <= iPriority
					IF MC_serverBD_4.iVehPriority[i][iTeam] != iPriority
					OR MC_serverBD_4.ivehRule[i][iTeam] != iRule
						iNewPriority = iPriority
						iNewRule = iRule
						RESET_SERVER_TARGETTYPES_FOR_ENTITY(i, iTeam, iObjective, iNewExtraObjectiveID, iNewExtraObjectiveArrayPos, ci_TARGET_VEHICLE)
					ENDIF
				ELSE
					RESET_SERVER_TARGETTYPES_FOR_ENTITY(i, iTeam, iObjective, iNewExtraObjectiveID, iNewExtraObjectiveArrayPos, ci_TARGET_VEHICLE)
					
					IF iNewExtraObjectiveID != -1
					AND iNewExtraObjectiveArrayPos != -1
						iNewPriority = g_FMMC_STRUCT.iExtraObjectivePriority[iNewExtraObjectiveID][iNewExtraObjectiveArrayPos][iTeam]
						iNewRule = GET_FMMC_RULE_FROM_CREATOR_RULE(g_FMMC_STRUCT.iExtraObjectiveRule[iNewExtraObjectiveID][iNewExtraObjectiveArrayPos][iTeam])
					ENDIF
				ENDIF
				
				IF iNewPriority < FMMC_MAX_RULES
				AND iNewRule != FMMC_OBJECTIVE_LOGIC_NONE
					IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehBitsetNine, ciFMMC_VEHICLE9_DontRevalidateEntityForObjective)
						PRINTLN("[OBJECTIVE_PROGRESSION] REVALIDATE_BACK_TO_AND_INCLUDING_OBJECTIVE - team ", iTeam, " - Vehicle ", i, " gets new priority ", iNewPriority, " and rule ", iNewRule)
						
						MC_serverBD_4.iVehPriority[i][iTeam] = iNewPriority
						MC_serverBD_4.ivehRule[i][iTeam] = iNewRule
						
						IF iNewExtraObjectiveID != -1
						AND iNewExtraObjectiveArrayPos != -1
							MC_serverBD.iCurrentExtraObjective[iNewExtraObjectiveID][iTeam] = iNewExtraObjectiveArrayPos
						ENDIF
						
						IF NOT IS_VEHICLE_USING_RESPAWN_POOL(i)
							MC_serverBD_2.iCurrentVehRespawnLives[i] = GET_ENTITY_STARTING_RESPAWN_LIVES(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehicleRespawnLives)
							
							PRINTLN("[OBJECTIVE_PROGRESSION] REVALIDATE_BACK_TO_AND_INCLUDING_OBJECTIVE - Adding respawns to vehicle: ", i, ". It is now: ", MC_serverBD_2.iCurrentVehRespawnLives[i])
							
							IF GET_VEHICLE_RESPAWNS(i) != UNLIMITED_RESPAWNS
							AND (NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[i]) OR IS_BIT_SET(MC_ServerBD.iVehAboutToCleanup_BS, i))
								SET_VEHICLE_RESPAWNS(i, GET_VEHICLE_RESPAWNS(i) + 1)
								PRINTLN("[OBJECTIVE_PROGRESSION] REVALIDATE_BACK_TO_AND_INCLUDING_OBJECTIVE - Overwriting respawns to vehicle: ", i, "  It is now: ", MC_serverBD_2.iCurrentVehRespawnLives[i])
							ENDIF
						ENDIF
						
						SET_VEHICLE_CAN_BE_KILLED_ON_THIS_RULE(i, iTeam)
						
						IF (NOT CAN_VEH_STILL_SPAWN_IN_MISSION(i))
						AND CAN_VEH_STILL_SPAWN_IN_MISSION(i, iTeam, iObjective) // If the vehicle will be able to spawn again:
							INT iTeamLoop
							FOR iTeamLoop = 0 TO (FMMC_MAX_TEAMS - 1)
								IF IS_BIT_SET(g_FMMC_STRUCT.iMissionCriticalvehBS[iTeamLoop], i)
									SET_BIT(MC_serverBD.iMissionCriticalVeh[iTeamLoop], i)
								ENDIF
							ENDFOR
						ENDIF
					ELSE
						PRINTLN("[OBJECTIVE_PROGRESSION] REVALIDATE_BACK_TO_AND_INCLUDING_OBJECTIVE - team ", iTeam, " - Vehicle ", i, " NOT GETTING DUE TO BLOCK new priority ", iNewPriority, " and rule ", iNewRule)
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	
	IF MC_serverBD.iNumObjCreated > 0
		FOR i = 0 TO (MC_serverBD.iNumObjCreated - 1)
			iPriority = g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iPriority[iTeam]
			iRule = g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iRule[iTeam]
			iNewPriority = FMMC_MAX_RULES
			iNewRule = FMMC_OBJECTIVE_LOGIC_NONE
			
			IF iPriority < FMMC_MAX_RULES
			AND iRule != FMMC_OBJECTIVE_LOGIC_NONE
			AND MC_DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP(i, eSGET_Object)
				
				//If we're jumping back to before the main priority of this object
				IF iObjective <= iPriority
					IF MC_serverBD_4.iObjPriority[i][iTeam] != iPriority
					OR MC_serverBD_4.iObjRule[i][iTeam] != iRule
						iNewPriority = iPriority
						iNewRule = iRule
						
						RESET_SERVER_TARGETTYPES_FOR_ENTITY(i, iTeam, iObjective, iNewExtraObjectiveID, iNewExtraObjectiveArrayPos, ci_TARGET_OBJECT)
					ENDIF
				ELSE
					RESET_SERVER_TARGETTYPES_FOR_ENTITY(i, iTeam, iObjective, iNewExtraObjectiveID, iNewExtraObjectiveArrayPos, ci_TARGET_OBJECT)
					
					IF iNewExtraObjectiveID != -1
					AND iNewExtraObjectiveArrayPos != -1
						iNewPriority = g_FMMC_STRUCT.iExtraObjectivePriority[iNewExtraObjectiveID][iNewExtraObjectiveArrayPos][iTeam]
						iNewRule = GET_FMMC_RULE_FROM_CREATOR_RULE(g_FMMC_STRUCT.iExtraObjectiveRule[iNewExtraObjectiveID][iNewExtraObjectiveArrayPos][iTeam])
					ENDIF
				ENDIF
				
				IF iNewPriority < FMMC_MAX_RULES
				AND iNewRule != FMMC_OBJECTIVE_LOGIC_NONE
					IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectBitSetFour, cibsOBJ4_DontRevalidateEntityForObjective)
						PRINTLN("[OBJECTIVE_PROGRESSION] REVALIDATE_BACK_TO_AND_INCLUDING_OBJECTIVE - team ", iTeam, " - Object ", i, " gets new priority ", iNewPriority, " and rule ", iNewRule)
						
						MC_serverBD_4.iObjPriority[i][iTeam] = iNewPriority
						MC_serverBD_4.iObjRule[i][iTeam] = iNewRule
						
						IF iNewExtraObjectiveID != -1
						AND iNewExtraObjectiveArrayPos != -1
							MC_serverBD.iCurrentExtraObjective[iNewExtraObjectiveID][iTeam] = iNewExtraObjectiveArrayPos
						ENDIF
						
						MC_serverBD_2.iCurrentObjRespawnLives[i] = GET_ENTITY_STARTING_RESPAWN_LIVES(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectRespawnLives)
						
						IF MC_serverBD_2.iCurrentObjRespawnLives[i] != UNLIMITED_RESPAWNS
						AND NOT NETWORK_DOES_NETWORK_ID_EXIST(GET_OBJECT_NET_ID(i))
							MC_serverBD_2.iCurrentObjRespawnLives[i]++
						ENDIF
						
						SET_OBJECT_CAN_BE_KILLED_ON_THIS_RULE(i, iTeam)
						
						IF (NOT CAN_OBJ_STILL_SPAWN_IN_MISSION(i))
						AND CAN_OBJ_STILL_SPAWN_IN_MISSION(i, iTeam, iObjective) // If the object will be able to spawn again:
							INT iTeamLoop
							FOR iTeamLoop = 0 TO (FMMC_MAX_TEAMS - 1)
								IF IS_BIT_SET(g_FMMC_STRUCT.iMissionCriticalobjBS[iTeamLoop], i)
									SET_BIT(MC_serverBD.iMissionCriticalObj[iTeamLoop], i)
								ENDIF
							ENDFOR
						ENDIF
					ELSE
						PRINTLN("[OBJECTIVE_PROGRESSION] REVALIDATE_BACK_TO_AND_INCLUDING_OBJECTIVE - team ", iTeam, " - Object ", i, " NOT GETTING DUE TO BLOCK new priority ", iNewPriority, " and rule ", iNewRule)
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	
	MC_serverBD_4.iTimesRevalidatedObjectives[iTeam]++
	PRINTLN("[OBJECTIVE_PROGRESSION] REVALIDATE_BACK_TO_AND_INCLUDING_OBJECTIVE - incrementing team ", iTeam, " Times Revalidated Objectives to = ", MC_serverBD_4.iTimesRevalidatedObjectives[iTeam])
	
ENDPROC

FUNC BOOL SHOULD_INVALIDATE_THIS_OBJECTIVE(INT iPriority, INT iObjective, BOOL bUpToAndIncluding = FALSE)
	
	IF bUpToAndIncluding
		RETURN iPriority <= iObjective
	ELSE
		RETURN iPriority = iObjective
	ENDIF
	
ENDFUNC

PROC INVALIDATE_PLAYER_OBJECTIVES(INT iTeam, BOOL bForcePass, INT iObjective, BOOL bLimitForceEnd, BOOL bUpToAndIncluding, BOOL &bProtectPass, INT &iHighestPriority)
	
	IF MC_serverBD.iNumPlayerRuleCreated <= 0
		EXIT
	ENDIF
	
	INT i
	FOR i = 0 TO MC_serverBD.iNumPlayerRuleCreated - 1
		
		IF NOT SHOULD_INVALIDATE_THIS_OBJECTIVE(MC_serverBD_4.iPlayerRulePriority[i][iTeam], iObjective, bUpToAndIncluding)
			RELOOP
		ENDIF
		
		IF MC_serverBD_4.iPlayerRule[i][iTeam] = FMMC_OBJECTIVE_LOGIC_HOLDING_RULE
		AND bLimitForceEnd	
			INCREMENT_SERVER_TEAM_SCORE(iTeam, MC_serverBD_4.iPlayerRulePriority[i][iTeam], GET_FMMC_POINTS_FOR_TEAM(iTeam, MC_serverBD_4.iPlayerRulePriority[i][iTeam]))
			SET_TEAM_OBJECTIVE_OUTCOME(iTeam, MC_serverBD_4.iPlayerRulePriority[i][iTeam], TRUE)
			MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_POINTS_RULE_HELD
			PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_PLAYER, i, iTeam, TRUE, iObjective)
			IF iHighestPriority < MC_serverBD_4.iPlayerRulePriority[i][iTeam]
				bProtectPass = TRUE
				iHighestPriority = MC_serverBD_4.iPlayerRulePriority[i][iTeam]
			ENDIF
		ELSE
			IF iHighestPriority < MC_serverBD_4.iPlayerRulePriority[i][iTeam]
				bProtectPass = FALSE
				iHighestPriority = MC_serverBD_4.iPlayerRulePriority[i][iTeam]
			ENDIF
			PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_PLAYER, i, iTeam, bForcePass, iObjective)
		ENDIF
		
		PRINTLN("[OBJECTIVE_PROGRESSION] INVALIDATE_PLAYER_OBJECTIVES - Up To And Including = ", bUpToAndIncluding, " - server setting as no longer valid: ", i)
	
	ENDFOR
	
ENDPROC

PROC INVALIDATE_LOCATION_OBJECTIVES(INT iTeam, BOOL bForcePass, INT iObjective, BOOL bUpToAndIncluding)
	
	IF MC_serverBD.iNumLocCreated <= 0
		EXIT
	ENDIF
	
	INT i
	FOR i = 0 TO MC_serverBD.iNumLocCreated - 1

		IF NOT SHOULD_INVALIDATE_THIS_OBJECTIVE(MC_serverBD_4.iGotoLocationDataPriority[i][iTeam], iObjective, bUpToAndIncluding)
			RELOOP
		ENDIF
		
		PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_LOCATION, i, iTeam, bForcePass, iObjective)
		PRINTLN("[OBJECTIVE_PROGRESSION] INVALIDATE_LOCATION_OBJECTIVES - Up To And Including = ", bUpToAndIncluding, " - server setting as no longer valid: ", i)
	
	ENDFOR
	
ENDPROC

PROC INVALIDATE_PED_OBJECTIVES(INT iTeam, BOOL bForcePass, INT iObjective, BOOL bLimitForceEnd, BOOL bUpToAndIncluding, BOOL &bProtectPass, INT &iHighestPriority)
	
	IF MC_serverBD.iNumPedCreated <= 0
		EXIT
	ENDIF
	
	INT i
	FOR i = 0 TO MC_serverBD.iNumPedCreated - 1
		
		IF NOT SHOULD_INVALIDATE_THIS_OBJECTIVE(MC_serverBD_4.iPedPriority[i][iTeam], iObjective, bUpToAndIncluding)
			RELOOP
		ENDIF
		
		IF(MC_serverBD_4.iPedRule[i][iTeam] = FMMC_OBJECTIVE_LOGIC_PROTECT
		OR MC_serverBD_4.iPedRule[i][iTeam] = FMMC_OBJECTIVE_LOGIC_CAPTURE)
		AND bLimitForceEnd	
			INCREMENT_SERVER_TEAM_SCORE(iTeam, MC_serverBD_4.iPedPriority[i][iTeam], GET_FMMC_POINTS_FOR_TEAM(iTeam, MC_serverBD_4.iPedPriority[i][iTeam]))
			
			BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_TEAM_PRO_PED, 0, iTeam, -1, INVALID_PLAYER_INDEX(), ci_TARGET_PED, i)
			
			SET_TEAM_OBJECTIVE_OUTCOME(iTeam, MC_serverBD_4.iPedPriority[i][iTeam], TRUE)
			
			MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_PROTECTED_PED
			
			PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_PED, i, iTeam, TRUE, iObjective)
			
			IF iHighestPriority < MC_serverBD_4.iPedPriority[i][iTeam]
				bProtectPass = TRUE
				iHighestPriority = MC_serverBD_4.iPedPriority[i][iTeam]
			ENDIF
		ELSE
			IF iHighestPriority < MC_serverBD_4.iPedPriority[i][iTeam]
				bProtectPass = FALSE
				iHighestPriority = MC_serverBD_4.iPedPriority[i][iTeam]
			ENDIF
			
			PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_PED, i, iTeam, bForcePass, iObjective)
		ENDIF
		
		PRINTLN("[OBJECTIVE_PROGRESSION] INVALIDATE_PED_OBJECTIVES - Up To And Including = ", bUpToAndIncluding, " - server setting as no longer valid: ", i)
	
	ENDFOR
	
ENDPROC

PROC INVALIDATE_VEHICLE_OBJECTIVES(INT iTeam, BOOL bForcePass, INT iObjective, BOOL bLimitForceEnd, BOOL bUpToAndIncluding, BOOL &bProtectPass, INT &iHighestPriority)
	
	IF MC_serverBD.iNumVehCreated <= 0
		EXIT
	ENDIF
	
	INT i
	FOR i = 0 TO MC_serverBD.iNumVehCreated - 1
		
		IF NOT SHOULD_INVALIDATE_THIS_OBJECTIVE(MC_serverBD_4.iVehPriority[i][iTeam], iObjective, bUpToAndIncluding)
			RELOOP
		ENDIF
		
		IF MC_serverBD_4.ivehRule[i][iTeam] = FMMC_OBJECTIVE_LOGIC_PROTECT
		AND bLimitForceEnd
			INCREMENT_SERVER_TEAM_SCORE(iTeam, MC_serverBD_4.iVehPriority[i][iTeam], GET_FMMC_POINTS_FOR_TEAM(iTeam, MC_serverBD_4.iVehPriority[i][iTeam]))
			
			BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_TEAM_PRO_VEH, 0, iTeam, -1, INVALID_PLAYER_INDEX(), ci_TARGET_VEHICLE, i)

			SET_TEAM_OBJECTIVE_OUTCOME(iTeam,MC_serverBD_4.iVehPriority[i][iTeam], TRUE)
			
			MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_PROTECTED_VEH
			
			PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_VEHICLE, i, iTeam, TRUE, iObjective)
			
			IF iHighestPriority < MC_serverBD_4.iVehPriority[i][iTeam]
				bProtectPass = TRUE
				iHighestPriority = MC_serverBD_4.iVehPriority[i][iTeam]
			ENDIF
		ELSE
			PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_VEHICLE, i, iTeam, bForcePass, iObjective)
			
			IF iHighestPriority < MC_serverBD_4.iVehPriority[i][iTeam]
				bProtectPass = FALSE
				iHighestPriority = MC_serverBD_4.iVehPriority[i][iTeam]
			ENDIF
		ENDIF
		
		PRINTLN("[OBJECTIVE_PROGRESSION] INVALIDATE_VEHICLE_OBJECTIVES - Up To And Including = ", bUpToAndIncluding, " - server setting as no longer valid: ", i)
	
	ENDFOR
	
ENDPROC

PROC INVALIDATE_OBJECT_OBJECTIVES(INT iTeam, BOOL bForcePass, INT iObjective, BOOL bLimitForceEnd, BOOL bUpToAndIncluding, BOOL &bProtectPass, INT &iHighestPriority)
	
	IF MC_serverBD.iNumObjCreated <= 0
		EXIT
	ENDIF
	
	INT i
	FOR i = 0 TO MC_serverBD.iNumObjCreated - 1
		
		IF NOT SHOULD_INVALIDATE_THIS_OBJECTIVE(MC_serverBD_4.iObjPriority[i][iTeam], iObjective, bUpToAndIncluding)
			RELOOP
		ENDIF
		
		IF MC_serverBD_4.iObjRule[i][iTeam] = FMMC_OBJECTIVE_LOGIC_PROTECT
		AND bLimitForceEnd
			INCREMENT_SERVER_TEAM_SCORE(iTeam, MC_serverBD_4.iObjPriority[i][iTeam], GET_FMMC_POINTS_FOR_TEAM(iTeam, MC_serverBD_4.iObjPriority[i][iTeam]))
			
			BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_TEAM_PRO_OBJ, 0, iTeam, -1, INVALID_PLAYER_INDEX(), ci_TARGET_OBJECT, i)

			SET_TEAM_OBJECTIVE_OUTCOME(iTeam, MC_serverBD_4.iObjPriority[i][iTeam], TRUE)
			
			MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_PROTECTED_OBJ
			
			PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_OBJECT, i, iTeam, TRUE, iObjective)
			
			IF iHighestPriority < MC_serverBD_4.iObjPriority[i][iTeam]
				bProtectPass = TRUE
				iHighestPriority = MC_serverBD_4.iObjPriority[i][iTeam]
			ENDIF
		ELSE
			IF MC_serverBD_4.iObjRule[i][iTeam] = FMMC_OBJECTIVE_LOGIC_GET_AND_HOLD
				IF MC_serverBD.iObjCarrier[i] != -1							
					CLEAR_BIT(MC_serverBD.iObjHoldPartPoints[i], MC_serverBD.iObjCarrier[i])
				ENDIF
				CLEAR_BIT(MC_serverBD.iObjAtYourHolding[iTeam], i)
			ENDIF
			
			IF iHighestPriority < MC_serverBD_4.iObjPriority[i][iTeam]
				bProtectPass = FALSE
				iHighestPriority = MC_serverBD_4.iObjPriority[i][iTeam]
			ENDIF
			
			PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_OBJECT, i, iTeam, bForcePass, iObjective)
		ENDIF
		
		PRINTLN("[OBJECTIVE_PROGRESSION] INVALIDATE_OBJECT_OBJECTIVES - Up To And Including = ", bUpToAndIncluding, " - server setting as no longer valid: ", i)
	
	ENDFOR
	
ENDPROC

FUNC BOOL INVALIDATE_UP_TO_AND_INCLUDING_OBJECTIVE(INT iTeam, INT iObjective, BOOL bLimitForceEnd = FALSE, BOOL bForcePass = FALSE, BOOL bUndoEarlierObjectives = FALSE)
	
	BOOL bProtectPass
	INT iHighestPriority
	
	DEBUG_PRINTCALLSTACK()
	PRINTLN("[OBJECTIVE_PROGRESSION] INVALIDATE_UP_TO_AND_INCLUDING_OBJECTIVE - called for team ", iTeam, " up to objective ", iObjective,", Limit Force End ", bLimitForceEnd, ", Force Pass ", bForcePass, ", Undo Earlier Objectives ", bUndoEarlierObjectives)
	
	IF (NOT bUndoEarlierObjectives)
	OR (iObjective >= MC_serverBD_4.iCurrentHighestPriority[iTeam])
		INVALIDATE_PLAYER_OBJECTIVES(iTeam, bForcePass, iObjective, bLimitForceEnd, TRUE, bProtectPass, iHighestPriority) //re-using bProtectPass for Holding Rules
		INVALIDATE_LOCATION_OBJECTIVES(iTeam, bForcePass, iObjective, TRUE)
		INVALIDATE_PED_OBJECTIVES(iTeam, bForcePass, iObjective, bLimitForceEnd, TRUE, bProtectPass, iHighestPriority)
		INVALIDATE_VEHICLE_OBJECTIVES(iTeam, bForcePass, iObjective, bLimitForceEnd, TRUE, bProtectPass, iHighestPriority)
		INVALIDATE_OBJECT_OBJECTIVES(iTeam, bForcePass, iObjective, bLimitForceEnd, TRUE, bProtectPass, iHighestPriority)
	ELSE
		REVALIDATE_BACK_TO_AND_INCLUDING_OBJECTIVE(iTeam, iObjective)
	ENDIF
	
	SET_PROGRESS_OBJECTIVE_FOR_TEAM(iTeam, FALSE)
	
	MC_serverBD.iNextObjective[iTeam] = -1
	
	RETURN bProtectPass 

ENDFUNC

FUNC BOOL INVALIDATE_SINGLE_OBJECTIVE(INT iTeam, INT iObjective, BOOL bLimitForceEnd = FALSE, BOOL bForcePass = FALSE)

	BOOL bProtectPass
	INT iHighestPriority
	
	PRINTLN("[OBJECTIVE_PROGRESSION] INVALIDATE_SINGLE_OBJECTIVE called for team ", iTeam, " up to objective ", iObjective, ", bLimitForceEnd ", bLimitForceEnd, ", bForcePass ", bForcePass)
	
	INVALIDATE_PLAYER_OBJECTIVES(iTeam, bForcePass, iObjective, bLimitForceEnd, FALSE, bProtectPass, iHighestPriority)
	INVALIDATE_LOCATION_OBJECTIVES(iTeam, bForcePass, iObjective, FALSE)
	INVALIDATE_PED_OBJECTIVES(iTeam, bForcePass, iObjective, bLimitForceEnd, FALSE, bProtectPass, iHighestPriority)
	INVALIDATE_VEHICLE_OBJECTIVES(iTeam, bForcePass, iObjective, bLimitForceEnd, FALSE, bProtectPass, iHighestPriority)
	INVALIDATE_OBJECT_OBJECTIVES(iTeam, bForcePass, iObjective, bLimitForceEnd, FALSE, bProtectPass, iHighestPriority)
	
	RETURN bProtectPass 

ENDFUNC

FUNC BOOL IS_THIS_OBJECTIVE_OVER(INT iType, INT iTeam, INT iPriority)

	IF iPriority != MC_serverBD_4.iCurrentHighestPriority[iTeam]
		PRINTLN("[OBJECTIVE_PROGRESSION] IS_THIS_OBJECTIVE_OVER - this is not the current priority: ", iPriority, " for team: ",iTeam)
		RETURN FALSE
	ENDIF
	
	INT i

	SWITCH iType
	
		CASE ci_TARGET_PLAYER
			
			FOR i = 0 TO MC_serverBD.iNumPlayerRuleCreated - 1
				
				IF MC_serverBD_4.iPlayerRule[i][iTeam] = FMMC_OBJECTIVE_LOGIC_NONE
					RELOOP
				ENDIF
				
				IF MC_serverBD_4.iPlayerRulePriority[i][iTeam] != MC_serverBD_4.iCurrentHighestPriority[iTeam]
				OR MC_serverBD_4.iPlayerRulePriority[i][iTeam] >= FMMC_MAX_RULES
					RELOOP
				ENDIF
				
				PRINTLN("[OBJECTIVE_PROGRESSION] IS_THIS_OBJECTIVE_OVER - OBJ_TYPE_PLAYER_KILL is not the current priority: ",iPriority," for team: ",iTeam) 
				RETURN FALSE
				
			ENDFOR

		BREAK
	
		CASE ci_TARGET_LOCATION
		
			FOR i = 0 TO MC_serverBD.iNumLocCreated - 1
				
				IF MC_serverBD_4.iGotoLocationDataRule[i][iTeam] = FMMC_OBJECTIVE_LOGIC_NONE
					RELOOP
				ENDIF
				
				IF MC_serverBD_4.iGotoLocationDataPriority[i][iTeam] != MC_serverBD_4.iCurrentHighestPriority[iTeam]
				OR MC_serverBD_4.iGotoLocationDataPriority[i][iTeam] >= FMMC_MAX_RULES
					RELOOP
				ENDIF

				PRINTLN("[OBJECTIVE_PROGRESSION] IS_THIS_OBJECTIVE_OVER - OBJ_TYPE_LOC is not the current priority: ",iPriority," for team: ",iTeam) 
				RETURN FALSE
				
			ENDFOR

		BREAK
		
		CASE ci_TARGET_PED
		
			FOR i = 0 TO MC_serverBD.iNumPedCreated - 1
			
				IF MC_serverBD_4.iPedRule[i][iTeam] = FMMC_OBJECTIVE_LOGIC_NONE
					RELOOP
				ENDIF
				
				IF MC_serverBD_4.iPedPriority[i][iTeam] != MC_serverBD_4.iCurrentHighestPriority[iTeam]
				OR MC_serverBD_4.iPedPriority[i][iTeam] >= FMMC_MAX_RULES
					RELOOP
				ENDIF
				
				IF IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[i])
					IF MC_serverBD_2.iCurrentPedRespawnLives[i] <= 0
						RELOOP
					ENDIF
				ENDIF
				
				PRINTLN("[OBJECTIVE_PROGRESSION] IS_THIS_OBJECTIVE_OVER - OBJ_TYPE_PED is not the current priority: ", iPriority, " for team: ", iTeam)
				RETURN FALSE
				
			ENDFOR

		BREAK
		
		CASE ci_TARGET_VEHICLE
		
			FOR i = 0 TO MC_serverBD.iNumVehCreated - 1
			
				IF MC_serverBD_4.iVehRule[i][iTeam] = FMMC_OBJECTIVE_LOGIC_NONE
					RELOOP
				ENDIF
				
				IF MC_serverBD_4.iVehPriority[i][iTeam] != MC_serverBD_4.iCurrentHighestPriority[iTeam]
				OR MC_serverBD_4.iVehPriority[i][iTeam] >= FMMC_MAX_RULES
					RELOOP
				ENDIF
				
				IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[i])
				OR NOT IS_VEHICLE_DRIVEABLE(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i]))
					IF GET_VEHICLE_RESPAWNS(i) <= 0
						RELOOP
					ENDIF
				ENDIF
				
				PRINTLN("[OBJECTIVE_PROGRESSION] IS_THIS_OBJECTIVE_OVER - OBJ_TYPE_VEH is not the current priority: ", iPriority, " for team: ", iTeam)
				RETURN FALSE
				
			ENDFOR

		BREAK
		
		CASE ci_TARGET_OBJECT
		
			FOR i = 0 TO MC_serverBD.iNumObjCreated - 1
				
				IF MC_serverBD_4.iObjRule[i][iTeam] = FMMC_OBJECTIVE_LOGIC_NONE
					RELOOP
				ENDIF
				
				IF MC_serverBD_4.iObjPriority[i][iTeam] != MC_serverBD_4.iCurrentHighestPriority[iTeam]
				OR MC_serverBD_4.iObjPriority[i][iTeam] >= FMMC_MAX_RULES
					RELOOP
				ENDIF
				
				IF NOT NETWORK_DOES_NETWORK_ID_EXIST(GET_OBJECT_NET_ID(i))
				OR IS_ENTITY_DEAD(NET_TO_OBJ(GET_OBJECT_NET_ID(i)))
					IF MC_serverBD_2.iCurrentObjRespawnLives[i] <= 0
						RELOOP
					ENDIF
				ENDIF
						
				PRINTLN("[OBJECTIVE_PROGRESSION] IS_THIS_OBJECTIVE_OVER - OBJ_TYPE_OBJ is not the current priority: ", iPriority, " for team: ", iTeam) 
				RETURN FALSE
				
			ENDFOR

		BREAK
		
	ENDSWITCH
	
	PRINTLN("[RCC MISSION] IS_THIS_OBJECTIVE_OVER is TRUE for priority: ",iPriority," for team: ",iTeam) 
	RETURN TRUE
	
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section: Photo Rule Processing -----------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: 
// ##### 
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC CREATE_PHOTO_TRACKED_POINT_FOR_COORD(INT &iTrackedPointID, VECTOR vPosition, FLOAT fRadius)

	IF ( iTrackedPointID = -1  )
		iTrackedPointID = CREATE_TRACKED_POINT()
		SET_TRACKED_POINT_INFO(iTrackedPointID, vPosition, fRadius)
		PRINTLN("[RCC MISSION] Created tracked point with id: ", iTrackedPointID, " for coord ", vPosition, " and radius ", fRadius, ".")
	ENDIF

ENDPROC

FUNC BOOL DOES_PHOTO_TRACKED_POINT_EXIST(INT &iTrackedPointID)
	RETURN iTrackedPointID != -1
ENDFUNC 

FUNC BOOL IS_PHOTO_TRACKED_POINT_VISIBLE(INT &iTrackedPointID)
	RETURN IS_TRACKED_POINT_VISIBLE(iTrackedPointID)
ENDFUNC

FUNC BOOL IS_POINT_IN_CELLPHONE_CAMERA_VIEW(VECTOR vPoint, FLOAT fRadius)

	FLOAT fXPosition, fYPosition

	IF IS_CELLPHONE_CAMERA_IN_USE()
		IF NOT IS_SNAPMATIC_NOT_IN_VIEWFINDER_MODE()
			IF IS_SPHERE_VISIBLE(vPoint, fRadius)
				IF GET_SCREEN_COORD_FROM_WORLD_COORD(vPoint, fXPosition, fYPosition)
					IF 	(fXPosition > 0.15) AND (fXPosition < 0.85)
					AND (fYPosition > 0.10) AND (fYPosition < 0.90)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF	

	RETURN FALSE

ENDFUNC

FUNC BOOL SHOULD_PUT_AWAY_PHONE_AFTER_PHOTO(INT iPriority, INT iEntityType = ci_TARGET_NONE)
	
	BOOL bPutAway = TRUE
	INT iTeam = MC_playerBD[iPartToUse].iteam
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFour[iPriority], ciBS_RULE4_PHOTO_DOESNT_CLOSE_PHONE)
		
		bPutAway = FALSE
		
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFour[iPriority], ciBS_RULE4_STILL_CLOSE_PHONE_ON_LAST_PHOTO)
			
			INT iLeft // Number of entities left on the rule
			
			SWITCH iEntityType
				CASE ci_TARGET_LOCATION
					iLeft = MC_serverBD.iNumLocHighestPriority[iTeam] - 1
				BREAK
				CASE ci_TARGET_OBJECT
					iLeft = MC_serverBD.iNumObjHighestPriority[iTeam] - 1
				BREAK
				CASE ci_TARGET_PED
					iLeft = MC_serverBD.iNumPedHighestPriority[iTeam] - 1
				BREAK
				CASE ci_TARGET_VEHICLE
					iLeft = MC_serverBD.iNumVehHighestPriority[iTeam] - 1
				BREAK
			ENDSWITCH
			
			IF iLeft <= 0
				bPutAway = TRUE
			ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTargetScore[iPriority] != FMMC_TARGET_SCORE_OFF
				//Check if the score will pass this rule:
				INT iNewScore = MC_serverBD.iScoreOnThisRule[iTeam] + GET_FMMC_POINTS_FOR_TEAM(iTeam, iPriority)
				INT iTarget
				
				IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTargetScore[iPriority] = FMMC_TARGET_SCORE_PLAYER
					iTarget = MC_serverBD.iNumberOfPlayingPlayers[iTeam]
				ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTargetScore[iPriority] = FMMC_TARGET_SCORE_PLAYER_HALF
					iTarget = IMAX(FLOOR(MC_serverBD.iNumberOfPlayingPlayers[iTeam] / 2.0), 1)
				ELSE
					iTarget = ROUND(GET_FMMC_OBJECTIVE_SCORE_LIMIT_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTargetScore[iPriority], MC_serverBD.iNumberOfPlayingPlayers[iTeam], MC_serverBD.iMaxNumPriorityEntitiesThisRule[iTeam])*GET_MULTIPLAYER_BASED_ON_PLAYERS_AND_CORONA_SETTING(iTeam, iPriority))
				ENDIF
				
				IF iNewScore >= iTarget
					bPutAway = TRUE
				ENDIF
			ENDIF
			
		ENDIF
	ENDIF
	
	RETURN bPutAway
	
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section: Get & Deliver --------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: 
// ##### 
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL SHOULD_VEH_CLEAN_UP_ON_DELIVERY( INT iveh, INT iteamdel,BOOL bCheckDontCleanupFlag = TRUE, BOOL bCheckShockLock = TRUE)
		
	IF bCheckDontCleanupFlag	
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitSet,ciFMMC_VEHICLE_DONTCLEANUPONDELIVER)
			PRINTLN("[RCC MISSION] Veh ", iveh, " shouldn't clean up on delivery due to creator setting")
			RETURN FALSE
		ENDIF
	ENDIF

	//If any teams still have rules involving this ped:
	INT iTeam,iteamtouse
	
	FOR iTeam = 0 TO FMMC_MAX_TEAMS - 1
		IF IS_FAKE_MULTIPLAYER_MODE_SET()
			iteamtouse = MC_playerBD[iPartToUse].iteam
			iTeam = FMMC_MAX_TEAMS // Only need to do this once!
		ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetFour, ciFMMC_VEHICLE4_DELIVERY_CLEANUPONLYCHECKSONETEAM)
			iteamtouse = iteamdel
			iTeam = FMMC_MAX_TEAMS // Only need to do this once!
		ELSE
			iteamtouse = iTeam
		ENDIF
		IF iteamtouse != iteamdel
			IF MC_serverBD_4.iVehPriority[iveh][iteamtouse] > MC_serverBD_4.iCurrentHighestPriority[iteamtouse]
			AND MC_serverBD_4.iVehPriority[iveh][iteamtouse] < FMMC_MAX_RULES
				PRINTLN("[RCC MISSION] Veh ", iveh, " shouldn't clean up on delivery due to veh still being required by team ", iteamtouse, " - veh's primary rule")
				RETURN FALSE
			ENDIF
		ENDIF
		IF (GET_NEXT_PRIORITY_FOR_ENTITY(ciRULE_TYPE_VEHICLE, iveh, iteamtouse, MC_serverBD_4.iCurrentHighestPriority[iteamtouse]) < FMMC_MAX_RULES)
			PRINTLN("[RCC MISSION] Veh ", iveh, " shouldn't clean up on delivery due to veh still being required by team ", iteamtouse, " - veh's extra rule")
			RETURN FALSE
		ENDIF
	ENDFOR
	
	IF bCheckShockLock
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFive, ciFMMC_VEHICLE5_SHOCKED_IF_LOCKED)
			PRINTLN("[RCC MISSION] Veh ", iveh, " shouldn't clean up on delivery due as it has the shock lock turned on")
			RETURN FALSE
		ENDIF
	ENDIF


	PRINTLN("[RCC MISSION] Veh ", iveh, " should clean up on delivery")
	RETURN TRUE
	
ENDFUNC

FUNC BOOL SHOULD_LOCK_VEHICLE_ON_DELIVERY( INT iVeh, INT iTeam )
	IF( SHOULD_VEH_CLEAN_UP_ON_DELIVERY( iVeh, iTeam, FALSE,FALSE ) )
		IF( IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[ iVeh ].iVehBitsetTwo, ciFMMC_VEHICLE2_LOCK_ON_RULE ) )
			// Lock Vehicle on Final Delivery
			
			PRINTLN("[RCC MISSION] [SHOULD_LOCK_VEHICLE_ON_DELIVERY] True - ciFMMC_VEHICLE2_LOCK_ON_RULE ")
			RETURN TRUE
		ENDIF
		IF IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[ iVeh ].iVehBitSet, ciFMMC_VEHICLE_DONTCLEANUPONDELIVER ) 
		
			PRINTLN("[RCC MISSION] [SHOULD_LOCK_VEHICLE_ON_DELIVERY] FALSE - ciFMMC_VEHICLE_DONTCLEANUPONDELIVER ")
			RETURN FALSE
		ELSE
			//-- Legacy, return true if nothing is set
			PRINTLN("[RCC MISSION] [SHOULD_LOCK_VEHICLE_ON_DELIVERY] TRUE - NOT ciFMMC_VEHICLE_DONTCLEANUPONDELIVER ")
			RETURN TRUE
		ENDIF 
	ELSE
		IF( g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[ iVeh ].iLockOnDeliveryTeam = iTeam
		AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[ iVeh ].iLockOnDeliveryRule = MC_serverBD_4.iCurrentHighestPriority[ iTeam ] )
			// Lock Vehicle on Delivery Rule
			PRINTLN("[RCC MISSION] [SHOULD_LOCK_VEHICLE_ON_DELIVERY] TRUE and shouldn't clean up..")
			RETURN TRUE
		ENDIF
	ENDIF
	PRINTLN("[RCC MISSION] [SHOULD_LOCK_VEHICLE_ON_DELIVERY] FALSE")
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_TURN_PLAYER_CONTROL_ON_AFTER_DELIVERY()
	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[ iTeam ]
	
	// Don't ant to turn on plauyer control if it's the last rule (2150083) or if the next rule is a cutscene (2172246)
	
	PRINTLN("[RCC MISSION] [SHOULD_TURN_PLAYER_CONTROL_ON_AFTER_DELIVERY] Called iTeam = ", iTeam, " iRule = ", iRule)
	
	IF MC_ServerBD.iNumVehHighestPriority[ iTeam ] > 1 
		//-- There's more cars to deliver
		
		PRINTLN("[RCC MISSION] [SHOULD_TURN_PLAYER_CONTROL_ON_AFTER_DELIVERY] TRUE -  more cars to deliver")
		RETURN TRUE
	ENDIF
	
	 IF iRule < MC_serverBD.iMaxObjectives[ iTeam ] 
	 	IF NOT IS_NEXT_RULE_A_CUTSCENE()
			//-- This isn't the last rule, and the next rule isn't a cutscene
			PRINTLN("[RCC MISSION] [SHOULD_TURN_PLAYER_CONTROL_ON_AFTER_DELIVERY] TRUE - Not last rule, and next rule isn't a cutscene")
			RETURN TRUE
		ENDIF
	 ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_DRIVER_PRESSING_ACCELERATE(INT iRule, INT iTeam)

	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTen[iRule], ciBS_RULE10_PROGRESS_RULE_ALL_DRIVER_ACCEL)
		PRINTLN("ARE_ALL_PLAYERS_IN_VEHICLE - ciBS_RULE10_PROGRESS_RULE_ALL_DRIVER_ACCEL = FALSE, return TRUE")
		RETURN TRUE
	ENDIF
	
	IF IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
		PRINTLN("ARE_ALL_PLAYERS_IN_VEHICLE - Pressed INPUT_VEH_ACCELERATE, return TRUE")
		REQUEST_OBJECTIVE_TEXT_DELAY(ciOBJECTIVE_TEXT_DELAY_VEHICLE, FALSE #IF IS_DEBUG_BUILD , "IS_DRIVER_PRESSING_ACCELERATE" #ENDIF )
		RETURN TRUE
	ENDIF
	
	PRINTLN("ARE_ALL_PLAYERS_IN_VEHICLE - Waiting for driver to press INPUT_VEH_ACCELERATE")
	RETURN FALSE

ENDFUNC

FUNC BOOL IS_TEAM_IN_VEHICLE_ACCURATE(INT iRule, INT iTeam)
	IF ( IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitsetThree[ iRule ], ciBS_RULE3_CANT_GET_OUT_NEAR_DELIVERY ) )
		PRINTLN( "[RCC MISSION] IS_TEAM_IN_VEHICLE_ACCURATE - Checking if all required teams in vehicle because ciBS_RULE3_CANT_GET_OUT_NEAR_DELIVERY is set on rule ", iRule )
		
		INT 			iVeh 	= MC_playerBD[ iPartToUse ].iVehNear
		NETWORK_INDEX	tempNet
		VEHICLE_INDEX 	tempVeh
		
		IF( iVeh <> -1 )
			tempNet = MC_serverBD_1.sFMMC_SBD.niVehicle[ iVeh ]
			IF( NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID( tempNet )
			AND MC_serverBD.iNumVehHighestPriority[ iTeam ] = 1 )
				tempVeh = NET_TO_VEH( tempNet )
			ELSE
				RETURN TRUE
			ENDIF
		ELSE
			RETURN TRUE
		ENDIF
		
		BOOL bCheckOnRule 	= TRUE
		INT  iTeamsToCheck 	= GET_VEHICLE_GOTO_TEAMS( iTeam, iVeh, bCheckOnRule )
		
		INT iPlayersToCheck, iPlayersChecked, iPlayersInVeh, iTeamLoop
		
		FOR iTeamLoop = 0 TO ( MC_serverBD.iNumberOfTeams - 1 )
			IF( IS_BIT_SET( iTeamsToCheck, iTeamLoop ) )
				iPlayersToCheck += MC_serverBD.iNumberOfPlayingPlayers[ iTeamLoop ]
			ENDIF
		ENDFOR
		
		INT iPart, iPartTeam
		PARTICIPANT_INDEX 	tempPart
		PLAYER_INDEX 		tempPlayer
		PED_INDEX 			tempPed
		
		PRINTLN("[PLAYER_LOOP] - IS_TEAM_IN_VEHICLE_ACCURATE")
		REPEAT GET_MAXIMUM_PLAYERS_ON_MISSION() iPart
			iPartTeam = MC_playerBD[ iPart ].iTeam
			
			IF( ( NOT IS_BIT_SET( MC_playerBD[ iPart ].iClientBitSet, PBBOOL_ANY_SPECTATOR )
			OR  IS_BIT_SET( MC_playerBD[ iPart ].iClientBitSet, PBBOOL_HEIST_SPECTATOR ) )
			AND IS_BIT_SET( iTeamsToCheck, iPartTeam ) )
				tempPart = INT_TO_PARTICIPANTINDEX( iPart )
				
				IF( NETWORK_IS_PARTICIPANT_ACTIVE( tempPart ) )
					//this player will be included in the server counts
					iPlayersChecked++
					tempPlayer = NETWORK_GET_PLAYER_INDEX( tempPart )
					
					IF( IS_NET_PLAYER_OK( tempPlayer ) )
						tempPed = GET_PLAYER_PED( tempPlayer )
						
						IF( NOT IS_PED_INJURED( tempPed ) )
							IF( IS_PED_IN_VEHICLE( tempPed, tempVeh ) )
								IF( IS_PED_SITTING_IN_VEHICLE( tempPed, tempVeh ) ) // Hopefully this checks that they're not jumping out or whatever
									iPlayersInVeh++

								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF( iPlayersChecked >= iPlayersToCheck )
						BREAKLOOP
					ENDIF
					
				ENDIF
			ENDIF
		ENDREPEAT
		
		IF( iPlayersInVeh >= iPlayersToCheck )
			RETURN TRUE
		ENDIF
		
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

//[FMMC2020] These delivery functions probably need to be accessed somewhere that will cause issues - may need to relocate. Originally from objective completion
FUNC BOOL SHOULD_OBJ_CLEAN_UP_ON_DELIVERY(INT iObj, INT idelTeam)
	
	//If any teams still have rules involving this obj:
	INT iTeam, iTeamtouse
	FOR iTeam = 0 TO FMMC_MAX_TEAMS - 1
		IF IS_FAKE_MULTIPLAYER_MODE_SET()
			iTeamtouse = MC_playerBD[iPartToUse].iTeam
		ELSE
			iTeamtouse = iTeam
		ENDIF
		
		IF iTeamtouse != idelTeam
		AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectBitSet, cibsOBJ_InvalidateAllTeams)
			IF MC_serverBD_4.iCurrentHighestPriority[iTeamtouse] < MC_serverBD_4.iObjPriority[iObj][iTeamtouse]
			AND MC_serverBD_4.iObjPriority[iObj][iTeamtouse] < FMMC_MAX_RULES
				PRINTLN("[RCC MISSION] iObj ", iObj, " shouldn't clean up on delivery due to iObj still being required by team ", iTeamtouse, " - iObj's primary rule")
				RETURN FALSE
			ENDIF
		ENDIF
		IF (GET_NEXT_PRIORITY_FOR_ENTITY(ciRULE_TYPE_OBJECT, iObj, iTeamtouse, -1) < FMMC_MAX_RULES)
			PRINTLN("[RCC MISSION] iObj ", iObj, " shouldn't clean up on delivery due to iObj still being required by team ", iTeamtouse, " - iObj's extra rule")
			RETURN FALSE
		ENDIF
	ENDFOR

	PRINTLN("[RCC MISSION] iObj ", iObj, " should clean up on delivery")
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_EXCLUSIVE_DELIVERY(INT iObj, INT iTeam, INT iRule)
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwelve[iRule], ciBS_RULE12_EXCLUSIVE_DELIVERY_OBJECT)
		RETURN TRUE
	ENDIF
	OBJECT_INDEX tempObj
	INT i
	REPEAT  MC_serverBD.iNumObjCreated i
		IF iObj = i
			RELOOP
		ENDIF
		
		IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID( GET_OBJECT_NET_ID(i))
			RELOOP
		ENDIF
		
		IF MC_serverBD.iObjCarrier[i] != -1
			PRINTLN("[RCC MISSION] [IS_EXCLUSIVE_DELIVERY] - Can't delivery the object ",iObj," because this ",i," is carried by player ",MC_serverBD.iObjCarrier[i])
			RETURN FALSE
		ENDIF
		
		tempObj = NET_TO_OBJ(GET_OBJECT_NET_ID(i))
		
		IF IS_ENTITY_ATTACHED(tempObj)
			PRINTLN("[RCC MISSION] [IS_EXCLUSIVE_DELIVERY] - Can't delivery the object ",iObj," because this ",i," is attached.")
			RETURN FALSE
		ENDIF
		
	ENDREPEAT
	RETURN TRUE
ENDFUNC

FUNC BOOL HAS_DELIVER_OBJECT_RULE_FINISHED(INT iObj)
	INT iTeam = MC_playerBD[iPartToUse].iTeam
	BOOL bReturn = FALSE

	IF MC_serverBD.iObjCarrier[iObj] = iPartToUse
		IF MC_serverBD_4.iObjRule[iObj][iTeam] = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER
			IF MC_serverBD_4.iObjPriority[iObj][iTeam] < FMMC_MAX_RULES
				IF NETWORK_DOES_NETWORK_ID_EXIST(GET_OBJECT_NET_ID(iObj))
					OBJECT_INDEX tempObj = NET_TO_OBJ(GET_OBJECT_NET_ID(iObj))
					
					IF IS_OBJECT_A_CONTAINER(tempObj)
					OR IS_ENTITY_ATTACHED_TO_ENTITY(tempObj, LocalPlayerPed) // Normal pickup
						IF IS_EXCLUSIVE_DELIVERY(iObj,iTeam,MC_serverBD_4.iObjPriority[iObj][iTeam])
							bReturn = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN bReturn
ENDFUNC

FUNC BOOL SHOULD_PED_ALWAYS_FOLLOW_TEAM(INT iped, INT iteam)
	
	SWITCH iteam
	
		CASE 0
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_AlwaysFollowTeam0)
				RETURN TRUE
			ENDIF
		BREAK
		CASE 1
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_AlwaysFollowTeam1)
				RETURN TRUE
			ENDIF
		BREAK
		CASE 2
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_AlwaysFollowTeam2)
				RETURN TRUE
			ENDIF
		BREAK
		CASE 3
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_AlwaysFollowTeam3)
				RETURN TRUE
			ENDIF
		BREAK
	
	ENDSWITCH

RETURN FALSE

ENDFUNC

FUNC BOOL SHOULD_PED_FOLLOW_TEAM_ON_RULE(INT iped, INT iTeam, INT iRule)
	
	BOOL bFollow = FALSE
	
	IF (iRule < FMMC_MAX_RULES)
	AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iFollowTeamOnRuleBS[iTeam], iRule)
		bFollow = TRUE
	ENDIF
	
	RETURN bFollow
	
ENDFUNC

FUNC BOOL SHOULD_PED_FOLLOW_TEAM_ON_CURRENT_RULE(INT iped, INT iTeam)
	
	RETURN SHOULD_PED_FOLLOW_TEAM_ON_RULE(iped, iTeam, MC_serverBD_4.iCurrentHighestPriority[iTeam])
	
ENDFUNC

FUNC BOOL IS_THIS_PEDS_LAST_DELIVERY_FOR_TEAM(INT iped,INT iteam)

	INT iextraObjectiveEntity
	INT iExtraObjectiveNum
	INT iRule
	
	FOR iextraObjectiveEntity = 0 TO (MAX_NUM_EXTRA_OBJECTIVE_ENTITIES-1)
		IF g_FMMC_STRUCT.iExtraObjectiveEntityType[iextraObjectiveEntity] = ciRULE_TYPE_PED
		AND g_FMMC_STRUCT.iExtraObjectiveEntityId[iextraObjectiveEntity] = iped
			FOR iExtraObjectiveNum = 0 TO (MAX_NUM_EXTRA_OBJECTIVE_PER_ENTITY-1)
				IF iExtraObjectiveNum >= MC_serverBD.iCurrentExtraObjective[iextraObjectiveEntity][iteam]
				OR MC_serverBD.iCurrentExtraObjective[iextraObjectiveEntity][iteam] = 0
					IF g_FMMC_STRUCT.iExtraObjectivePriority[iextraObjectiveEntity][iExtraObjectiveNum][iteam] < FMMC_MAX_RULES
					AND g_FMMC_STRUCT.iExtraObjectivePriority[iextraObjectiveEntity][iExtraObjectiveNum][iteam] >= 0
						iRule = g_FMMC_STRUCT.iExtraObjectiveRule[iextraObjectiveEntity][iExtraObjectiveNum][iteam]
						iRule = GET_FMMC_RULE_FROM_CREATOR_RULE(iRule)
						IF iRule = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER
							RETURN FALSE
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
		ENDIF
	ENDFOR
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL SHOULD_PED_LEAVE_GROUP_ON_DROP_OFF(INT iped)

	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo,ciPED_BSTwo_StopFollowingOnLastDelivery) 
		IF IS_THIS_PEDS_LAST_DELIVERY_FOR_TEAM(iped,MC_playerBD[iPartToUse].iteam)
			RETURN TRUE
		ENDIF
	ELSE
		IF (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iFollow != ciPED_FOLLOW_ON AND NOT SHOULD_PED_ALWAYS_FOLLOW_TEAM(iped,MC_playerBD[iPartToUse].iteam))
		//Will they leave my group on the next rule:
		AND NOT SHOULD_PED_FOLLOW_TEAM_ON_RULE(iped, MC_playerBD[iPartToUse].iteam, MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] + 1)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
													
ENDFUNC

FUNC BOOL SHOULD_PED_CLEAN_UP_ON_DELIVERY(INT iped, INT iteamdel,BOOL bOnlyCheckForLastDelivery = FALSE)
	
	IF NOT bOnlyCheckForLastDelivery
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitset,ciPED_BS_PedDoesntCleanUpOnDelivery)
			PRINTLN("[RCC MISSION] Ped ", iped, " shouldn't clean up on delivery due to creator setting")
			RETURN FALSE
		ENDIF
	ENDIF
	
	//If any teams still have rules involving this ped:
	INT iTeam
	IF IS_FAKE_MULTIPLAYER_MODE_SET()
		iTeam = MC_playerBD[iPartToUse].iteam
		IF iTeam != iteamdel
			IF MC_serverBD_4.iPedPriority[iped][iTeam] > MC_serverBD_4.iCurrentHighestPriority[iTeam]
			AND MC_serverBD_4.iPedPriority[iped][iTeam] < FMMC_MAX_RULES
				PRINTLN("[RCC MISSION] Ped ", iped, " shouldn't clean up on delivery due to ped still being required by team ", iTeam, " - ped's primary rule")
				RETURN FALSE
			ENDIF
		ENDIF
		IF (GET_NEXT_PRIORITY_FOR_ENTITY(ciRULE_TYPE_PED, iped, iTeam, -1) < FMMC_MAX_RULES)
			PRINTLN("[RCC MISSION] Ped ", iped, " shouldn't clean up on delivery due to ped still being required by team ", iTeam, " - ped's extra rule")
			RETURN FALSE
		ENDIF
	ELSE
		FOR iTeam = 0 TO FMMC_MAX_TEAMS - 1
		
			IF iTeam != iteamdel
				IF MC_serverBD_4.iPedPriority[iped][iTeam] > MC_serverBD_4.iCurrentHighestPriority[iTeam]
				AND MC_serverBD_4.iPedPriority[iped][iTeam] < FMMC_MAX_RULES
					PRINTLN("[RCC MISSION] Ped ", iped, " shouldn't clean up on delivery due to ped still being required by team ", iTeam, " - ped's primary rule")
					RETURN FALSE
				ENDIF
			ENDIF
			IF (GET_NEXT_PRIORITY_FOR_ENTITY(ciRULE_TYPE_PED, iped, iTeam, -1) < FMMC_MAX_RULES)
				PRINTLN("[RCC MISSION] Ped ", iped, " shouldn't clean up on delivery due to ped still being required by team ", iTeam, " - ped's extra rule")
				RETURN FALSE
			ENDIF
		ENDFOR
	ENDIF
	
	PRINTLN("[RCC MISSION] Ped ", iped, " should clean up on delivery")
	RETURN TRUE

ENDFUNC

PROC PROCESS_STOPPING_VEHICLE_FOR_DELIVERY(INT iRule, INT iTeam, BOOL bEnableShooting)
	
	BOOL bForeverStopped, bDurationStopped
	INT iMissionEntity
	VEHICLE_INDEX vehStopped
	IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)	
		vehStopped = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
		iMissionEntity = IS_ENTITY_A_MISSION_CREATOR_ENTITY(vehStopped)
		
		PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE - iVeh:  ", iMissionEntity)
		
		IF iMissionEntity > -1				
			IF IS_BIT_SET(iVehDeliveryStoppedCancelled, iMissionEntity)
				PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE - iVeh:  ", iMissionEntity, " Delivery Stopped Cancelled.")
				EXIT
			ENDIF			
			IF IS_BIT_SET(MC_serverBD_1.iBSVehicleStopForever, iMissionEntity)
				PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE - iVeh:  ", iMissionEntity, " iBSVehicleStopForever is set")
				bForeverStopped = TRUE
			ENDIF
			IF IS_BIT_SET(MC_serverBD_1.iBSVehicleStopDuration, iMissionEntity)
				PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE - iVeh:  ", iMissionEntity, " iBSVehicleStopDuration is set")
				bDurationStopped = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF bDurationStopped
	OR bForeverStopped
		PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE Is set!, iVehStopTimeToWait = ", MC_ServerBD_1.iVehStopTimeToWait)
		
		IF IS_VEHICLE_DRIVEABLE(vehStopped)
			IF iRule < FMMC_MAX_RULES
				IF bForeverStopped
					PRINTLN("[RCC MISSION] DISABLE_PLAYER_MISSION_VEHICLE_CONTROLS_THIS_FRAME called as can't move vehicle")
					BOOL bDisableHydraulics = IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitsetFive[ iRule ], ciBS_RULE5_DISABLE_HYDRAULICS_ON_STOP_VEH )
					DISABLE_PLAYER_MISSION_VEHICLE_CONTROLS_THIS_FRAME(FALSE, bEnableShooting, bDisableHydraulics )	
					SET_BIT(iLocalBoolCheck24, LBOOL24_BLOCK_VEHICLE_KERS_ON_RULE_CHECK)
					PRINTLN("[RCC MISSION] LBOOL24_BLOCK_VEHICLE_KERS_ON_RULE_CHECK setting")
					IF NETWORK_HAS_CONTROL_OF_ENTITY(vehStopped)
						SET_VEHICLE_HANDBRAKE(vehStopped, TRUE)
					ENDIF
				ENDIF
				IF bDurationStopped
					IF NOT HAS_NET_TIMER_STARTED(MC_serverBD_1.timeVehicleStop)
						PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE Waiting for timer to start...")
					ELSE
						IF HAS_NET_TIMER_EXPIRED(MC_serverBD_1.timeVehicleStop, MC_serverBD_1.iVehStopTimeToWait)
							PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE timeVehicleStop expired")
							IF NOT IS_BIT_SET(iLocalBoolCheck10, LBOOL10_STOP_CAR_FOREVER)
								IF NETWORK_HAS_CONTROL_OF_ENTITY(vehStopped)
									SET_VEHICLE_HANDBRAKE(vehStopped,FALSE)
									
									IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciDISABLE_VEHICLE_KERS)
										IF iRule < FMMC_MAX_RULES
											IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetNine[iRule], ciBS_RULE9_DISABLE_OPPRESSOR_BOOST)
												CLEAR_BIT(iLocalBoolCheck24, LBOOL24_BLOCK_VEHICLE_KERS_ON_RULE_CHECK)
												PRINTLN("[RCC MISSION] LBOOL24_BLOCK_VEHICLE_KERS_ON_RULE_CHECK Clearing")
											ENDIF
										ENDIF
									ENDIF									
									PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE removed handbrake")
								ELSE
									PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE I don't have control!")
								ENDIF
							ELSE
								PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE NOT LBOOL10_STOP_CAR_FOREVER")
							ENDIF
						ELSE
							IF NETWORK_HAS_CONTROL_OF_ENTITY(vehStopped)
								SET_VEHICLE_HANDBRAKE(vehStopped, TRUE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_STAY_IN_ON_DELIVERY()
	
	BOOL bStayIn = FALSE
	
	IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitset[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE_DONT_STOP_ON_DELIVERY)
		OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitsetTwo[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE2_STAY_IN_ON_DELIVERY)
			bStayIn = TRUE
		ELIF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitsetTwo[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE2_ONLY_STAY_IN_ON_LAST_DELIVERY)
			//If we're dropping off the last one:
			IF MC_serverBD.iNumVehHighestPriority[MC_playerBD[iPartToUse].iteam] = 1
			OR MC_serverBD.iNumPedHighestPriority[MC_playerBD[iPartToUse].iteam] = 1
			OR MC_serverBD.iNumObjHighestPriority[MC_playerBD[iPartToUse].iteam] = 1
				bStayIn = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN bStayIn
	
ENDFUNC

FUNC BOOL SHOULD_VEH_STOP_ON_DELIVERY()
	
	BOOL bStop = TRUE
	
	IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitset[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE_DONT_STOP_ON_DELIVERY)
			bStop = FALSE
		ENDIF
	ENDIF
	
	RETURN bStop
	
ENDFUNC

FUNC BOOL PROCESS_ALL_TEAM_MEMBERS_WITHIN_REQUIRED_DISTANCE_CHECKS(INT iTeam)
		
	IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_FIRST_UPDATE_DONE)
		RETURN TRUE
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(tdPassOutOfRangeTimer)
		IF HAS_NET_TIMER_EXPIRED(tdPassOutOfRangeTimer, OUT_OF_RANGE_TIME)
			RESET_NET_TIMER(tdPassOutOfRangeTimer)
		ELSE
			PRINTLN("[RCC MISSION] ARE_ALL_TEAM_MEMBERS_WITHIN_REQUIRED_DISTANCE - Returning TRUE as out of range for under ", OUT_OF_RANGE_TIME, " ms")
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitset3, PBBOOL3_ALL_TEAM_MEMBERS_WITHIN_REQDIST)
		PRINTLN("[RCC MISSION] ARE_ALL_TEAM_MEMBERS_WITHIN_REQUIRED_DISTANCE - Returning FALSE as local part ", iPartToUse, " doesn't have all team members near them.")
		RETURN FALSE
	ENDIF
	
	INT iPart = -1
	DO_PARTICIPANT_LOOP_FLAGS eFlags = GET_PARTICIPANT_LOOP_FLAGS_TO_CHECK_SINGLE_TEAM(iTeam)
	WHILE DO_PARTICIPANT_LOOP(iPart, eFlags | DPLF_EXCLUDE_LOCAL_PART)
		
		IF IS_BIT_SET(MC_playerBD[iPart].iClientBitset3, PBBOOL3_ALL_TEAM_MEMBERS_WITHIN_REQDIST)
			RELOOP
		ENDIF
		
		PRINTLN("[RCC MISSION] ARE_ALL_TEAM_MEMBERS_WITHIN_REQUIRED_DISTANCE - Returning FALSE as part ", iPart, " doesn't have all team members near them.")
		RETURN FALSE
		
	ENDWHILE
	
	REINIT_NET_TIMER(tdPassOutOfRangeTimer)
	RETURN TRUE
	
ENDFUNC

FUNC BOOL ARE_ALL_TEAM_MEMBERS_WITHIN_REQUIRED_DISTANCE(INT iTeam)

	IF IS_BIT_SET(iLocalBoolResetCheck1, LBOOL_RESET1_CHECKED_TEAM_MEMBERS_WITHIN_REQUIRED_DISTANCE_T0 + iTeam)
		RETURN IS_BIT_SET(iLocalBoolResetCheck1, LBOOL_RESET1_ALL_TEAM_MEMBERS_WITHIN_REQUIRED_DISTANCE_T0 + iTeam)
	ENDIF

	SET_BIT(iLocalBoolResetCheck1, LBOOL_RESET1_CHECKED_TEAM_MEMBERS_WITHIN_REQUIRED_DISTANCE_T0 + iTeam)
	
	IF PROCESS_ALL_TEAM_MEMBERS_WITHIN_REQUIRED_DISTANCE_CHECKS(iTeam)
		SET_BIT(iLocalBoolResetCheck1, LBOOL_RESET1_ALL_TEAM_MEMBERS_WITHIN_REQUIRED_DISTANCE_T0 + iTeam)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE: Draws the aerial checkpoint flag
PROC DRAW_AIR_MARKER( VECTOR vcheckPoint, BOOL bdropoff, INT iloc = -1 ) 
	INT iR, iG, iB, iA
	INT ialpha = 155
	VECTOR vCheckpointRotation = <<0,0,0>>
	
	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]
	
	IF bdropoff
		IF IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitset[ iRule ], ciBS_RULE_HIDE_DROPOFF_CORONA )
			EXIT
		ENDIF
	ENDIF
	
	vCheckpointRotation = NORMALISE_VECTOR( GET_ENTITY_COORDS( LocalPlayerPed ) - vcheckPoint )
	
	IF NOT IS_PED_IN_FLYING_VEHICLE(LocalPlayerPed)
		//Stop checkpoints pointing towards the floor when picked up on foot or in a car.
		vCheckpointRotation.z = 0
	ENDIF
	
	GET_HUD_COLOUR(HUD_COLOUR_YELLOW, iR, iG, iB, iA)

	// Implementing the ability to have checkpoints face the direction they were placed in
	IF iLoc >= 0
		IF IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ iloc ].iLocBS , ciLoc_BS_RotateManual )
			INT iCurrentTeam = MC_playerBD[ iPartToUse ].iTeam
		
			// Unfortunately this function gets called without iLoc being valid sometimes - need to guard against it
			vCheckpointRotation = GET_HEADING_AS_VECTOR( g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ iloc ].fHeading[ iCurrentTeam ] )
		ENDIF

		IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].fTeamMembersNeededWithin > 0
		AND NOT ARE_ALL_TEAM_MEMBERS_WITHIN_REQUIRED_DISTANCE(iTeam)
			GET_HUD_COLOUR(HUD_COLOUR_RED, iR, iG, iB, iA)
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS3, ciLoc_BS3_UseBlipColourForCorona)
			IF (g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].sLocBlipStruct.iBlipColourAggro != BLIP_COLOUR_DEFAULT AND HAS_TEAM_TRIGGERED_AGGRO(iTeam, g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iAggroIndexBS_Entity_Loc))
				GET_HUD_COLOUR(MC_GET_HUD_COLOUR_FROM_BLIP_COLOUR(GET_BLIP_COLOUR_FROM_CREATOR(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].sLocBlipStruct.iBlipColourAggro)), iR, iG, iB, iA)
			ELIF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].sLocBlipStruct.iBlipColour > 0
				GET_HUD_COLOUR(MC_GET_HUD_COLOUR_FROM_BLIP_COLOUR(GET_BLIP_COLOUR_FROM_CREATOR(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].sLocBlipStruct.iBlipColour)), iR, iG, iB, iA)
			ENDIF
		ENDIF

		IF IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iLocBS, ciLoc_BS_FlagMarker )			
			DRAW_MARKER(MARKER_RING_FLAG, vcheckPoint, vCheckpointRotation, <<0,0,0>>, <<8,8,8>>,iR, iG, iB,ialpha, FALSE, FALSE)
			DRAW_MARKER(MARKER_RING, vcheckPoint, vCheckpointRotation, <<0,0,0>>, <<8,8,8>>,iR, iG, iB,ialpha, FALSE, FALSE)
		ELIF IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iLocBS2, ciLoc_BS2_BeastFlagMarker )
			DRAW_MARKER(MARKER_BEAST, vcheckPoint, vCheckpointRotation, <<0,0,0>>, <<8,8,8>>,iR, iG, iB,ialpha, FALSE, FALSE)
			DRAW_MARKER(MARKER_RING, vcheckPoint, vCheckpointRotation, <<0,0,0>>, <<8,8,8>>,iR, iG, iB,ialpha, FALSE, FALSE)
		ELSE
			DRAW_MARKER(MARKER_RING, vcheckPoint, vCheckpointRotation, <<0,0,0>>, <<8,8,8>>,iR, iG, iB,ialpha, FALSE, FALSE)
		ENDIF
	ELSE
		DRAW_MARKER(MARKER_RING, vcheckPoint, vCheckpointRotation, <<0,0,0>>, <<8,8,8>>,iR, iG, iB,ialpha, FALSE, FALSE)
	ENDIF
	
ENDPROC

FUNC BOOL IS_PLAYER_IN_REQUIRED_LOCATION_VEHICLE(PED_INDEX piPlayerPed, INT iTeam, INT iLoc = -1)
	
	IF iLoc = -1 AND iTeam != -1
		iLoc = MC_serverBD.iPriorityLocation[iTeam]
	ENDIF
	
	IF (iLoc != -1 AND iTeam != -1)
	AND NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitset, PBBOOL_HEIST_SPECTATOR)
		MODEL_NAMES mnVeh = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].mnVehicleNeeded[iTeam]
		IF mnVeh != DUMMY_MODEL_FOR_SCRIPT
			IF IS_PED_IN_ANY_VEHICLE(piPlayerPed)
				IF GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_USING(piPlayerPed)) = mnVeh
					RETURN TRUE
				ENDIF
			ENDIF
		ELSE
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS2, ciLoc_BS2_NeedsCoronaVehicle)
			AND IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitset, ciBS_TEAM_USES_CORONA_VEHICLE_LIST )
				IF IS_PED_IN_ANY_VEHICLE(piPlayerPed)
				AND GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_USING(piPlayerPed)) = g_mnCoronaMissionVehicle
					RETURN TRUE
				ENDIF
			ELSE
				RETURN TRUE
			ENDIF
		ENDIF
		
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_PLAYER_IN_SPECIFIC_REQUIRED_LOCATION_VEHICLE_INDEX(PED_INDEX piPlayerPed, INT iTeam, INT iLoc = -1)
	
	IF iLoc = -1 AND iTeam != -1
		iLoc = MC_serverBD.iPriorityLocation[iTeam]
	ENDIF
	
	IF (iLoc != -1 AND iTeam != -1)
	AND g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocSpecificVehicleIndex > -1
	AND NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitset, PBBOOL_HEIST_SPECTATOR)
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocSpecificVehicleIndex])
			
			VEHICLE_INDEX viVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocSpecificVehicleIndex])
			
			IF IS_PED_IN_ANY_VEHICLE(piPlayerPed)
			AND DOES_ENTITY_EXIST(viVeh)
				IF GET_VEHICLE_PED_IS_USING(piPlayerPed) = viVeh
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC GET_CORONA_SIZE_AND_HEIGHT(FLOAT &fSize, FLOAT &fHeight, FLOAT fRadius, BOOL bDropOff, INT iLoc)
	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[ iTeam ]
	
	IF NOT bDropOff //Go to Locate
	
		fSize = fGoToLocateSize[iLoc]
		fHeight = fGoToLocateHeight[iLoc]
		
	ELSE
		
		INT iveh = -1//GET_VEHICLE_BEING_DROPPED_OFF(iteam, iPartToUse)
		IF getVehicleBeingDroppedOff != NULL
			iveh = CALL getVehicleBeingDroppedOff(iteam, iPartToUse)
		ENDIF
		
		IF iveh != -1
		AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].fVehDropOffVisualRadius > 0
			fHeight = 1.5
			fSize = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].fVehDropOffVisualRadius//*2
			IF fSize < 0.3
				fSize = 0.3
			ENDIF
		ELIF IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitsetThree[ iRule ], ciBS_RULE3_UseStandardSizeLocate )
			fHeight = 1.0 	// LOCATE_SIZE_HEIGHT
			fSize = 0.6 	// Default SP locate size
		ELIF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iSmallDropOffBitSet, iRule )
			//small corona
			fHeight = 1.0 // LOCATE_SIZE_HEIGHT
			fSize = fradius/10
			IF fSize < 0.3
				fSize = 0.3
			ENDIF
			IF fSize > 1.0
				fSize = 1.0
			ENDIF
		ELIF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iExactDropOffBitSet, iRule )
			//exact
			fHeight = 1.0 // LOCATE_SIZE_HEIGHT
			fSize = 2*fradius
			IF fSize < 0.3
				fSize = 0.3
			ENDIF
		ELSE //normal
			fHeight = 1.0 // LOCATE_SIZE_HEIGHT
			fSize = fradius/5
			IF fSize < 0.5
				fSize = 0.5
			ENDIF
			IF fSize > 5.0
				fSize = 5.0
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

FUNC FLOAT GET_DIRECTION_FOR_LOCATE_MARKER(INT iLoc)
	FLOAT fThisDirection
	
	fThisDirection = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].fDirection
	RETURN fThisDirection
ENDFUNC

FUNC BOOL HAS_CORONA_REACHED_NEW_STATE(INT i)

	IF corona_flare_data[i].fTransitionProgress  >= 1.0
		PRINTLN("AW SMOKE - HAS_CORONA_REACHED_NEW_STATE - TRUE")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

PROC PROCESS_CORONA_TRANSITION(INT i)
	

	//if the current state isn't equal to the new state that has been set
	IF corona_flare_data[i].iFlareState !=  corona_flare_data[i].iNewState
		
		IF HAS_CORONA_REACHED_NEW_STATE(i)
			corona_flare_data[i].iFlareState =  corona_flare_data[i].iNewState
		ELSE
			IF corona_flare_data[i].fRed != corona_flare_data[i].fNextRed
				corona_flare_data[i].fRed   = LERP_FLOAT(corona_flare_data[i].fRed,corona_flare_data[i].fNextRed,corona_flare_data[i].fTransitionProgress)
			ENDIF
			IF corona_flare_data[i].fGreen != corona_flare_data[i].fNextGreen
				corona_flare_data[i].fGreen = LERP_FLOAT(corona_flare_data[i].fGreen,corona_flare_data[i].fNextGreen,corona_flare_data[i].fTransitionProgress)
			ENDIF
			IF corona_flare_data[i].fBlue != flare_data[i].fNextBlue
				corona_flare_data[i].fBlue  = LERP_FLOAT(corona_flare_data[i].fBlue,corona_flare_data[i].fNextBlue,corona_flare_data[i].fTransitionProgress)
			ENDIF
			PRINTLN("AW SMOKE - PROCESS_CORONA_TRANSITION - fTransitionProgress:", corona_flare_data[i].fTransitionProgress)
			corona_flare_data[i].fTransitionProgress = corona_flare_data[i].fTransitionProgress + 0.01
		ENDIF
		
		IF DOES_PARTICLE_FX_LOOPED_EXIST( ptfxFlareIndex[corona_flare_data[i].iPTFXval])
			PRINTLN("AW SMOKE - PROCESS_CORONA_TRANSITION - fRed:", corona_flare_data[i].fRed, " fGreen:", corona_flare_data[i].fGreen," fBlue:", flare_data[i].fBlue)
			SET_PARTICLE_FX_LOOPED_COLOUR(ptfxFlareIndex[corona_flare_data[i].iPTFXval],corona_flare_data[i].fRed, corona_flare_data[i].fGreen, corona_flare_data[i].fBlue ,TRUE)
		ENDIF
	ENDIF


ENDPROC

//iColour 0 - R | 1 - G | 2 - B
FUNC INT GET_CORONA_TRANSITION(INT i, INT iColour)
	
	INT iRGB
	
	IF iColour = 0 
		iRGB = ROUND(corona_flare_data[i].fRed * 255)
		PRINTLN("AW SMOKE - GET_CORONA_TRANSITION =RED ",iRGB )
	ENDIF
	IF iColour = 1 
		iRGB = ROUND(corona_flare_data[i].fGreen * 255)	 
		PRINTLN("AW SMOKE - GET_CORONA_TRANSITION =BLUE ",iRGB )
	ENDIF
	IF iColour = 2 
		iRGB = ROUND(corona_flare_data[i].fBlue * 255)
		PRINTLN("AW SMOKE - GET_CORONA_TRANSITION =GREEN ",iRGB )
	ENDIF
	
	RETURN iRGB
ENDFUNC

FUNC BOOL IS_PREREQUISITE_FOR_LOCATION_COMPLETED(INT iLoc, BOOL bDoHelpText = FALSE)
	IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocPreReqRequired != ciPREREQ_None
		
		IF NOT IS_PREREQUISITE_COMPLETED(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocPreReqRequired)
			
			IF bDoHelpText
			AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS3, ciLoc_BS3_BlockPrereqHelp)
				bDoHelpText = FALSE
			ENDIF
			
			IF bDoHelpText
				TEXT_LABEL_23 tl23 = "FMMC_LPR_DEF"
				IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocPreReqHelpText != -1
					tl23 = GET_CUSTOM_STRING_LIST_TEXT_LABEL(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocPreReqHelpText)
				ENDIF
				
				IF NOT IS_THIS_PRINT_BEING_DISPLAYED(tl23)
					PRINT_HELP(tl23)
				ENDIF
			ENDIF
			
			PRINTLN("[RCC MISSION] CAN_PLAYER_PASS_THIS_LOCATION ",iLoc," = FALSE - Player hasn't completed required prerequisite ", g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocPreReqRequired)
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_DRAW_LOCATE_MARKER(INT iLoc, INT iTeam, INT iRule, BOOL bDropOff)
	
	// Don't draw the marker if the objective is blocked
	IF IS_OBJECTIVE_BLOCKED()
	AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS2, ciLoc_BS2_UseLineMarker)
		RETURN FALSE
	ENDIF
		
	IF NOT bDropOff
		IF(IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS, ciLoc_BS_HideMarker))
			RETURN FALSE
		ENDIF
	ELSE
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitset[iRule], ciBS_RULE_HIDE_DROPOFF_CORONA)
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS3, ciLoc_BS3_SpecificVehModelFullBlocker)
	AND NOT IS_PLAYER_IN_REQUIRED_LOCATION_VEHICLE(PlayerPedToUse, iTeam, iLoc)
		RETURN FALSE
	ENDIF
	
	IF NOT IS_PREREQUISITE_FOR_LOCATION_COMPLETED(iLoc)
		RETURN FALSE
	ENDIF
	
	IF NETWORK_IS_PLAYER_IN_MP_CUTSCENE(LocalPlayer)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

/// PURPOSE: This function draws the corona for a dropoff or location
///    If bDropoff is true, iLoc is not used
///    iAlphaOverride overrides the alpha value on this locate and has values of 0-255 or -1 if unused
PROC DRAW_LOCATE_MARKER( VECTOR vPos, FLOAT fradius, BOOL bdropoff, INT iloc, INT iAlphaOverride = -1, BOOL bEnemyMarker = FALSE, INT iTeamWhoOwnThis = -1)
	
	HUD_COLOURS TeamColour

	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	FLOAT fDirection
	
	IF NOT SHOULD_DRAW_LOCATE_MARKER(iLoc, iTeam, iRule, bDropOff)
		EXIT
	ENDIF
	
	INT iR, iG, iB, iA
	FLOAT fcoronaheight
	FLOAT fcoronasize
	
	GET_HUD_COLOUR(HUD_COLOUR_YELLOW, iR, iG, iB, iA)
		
	IF GET_MC_CLIENT_MISSION_STAGE(iPartToUse) = CLIENT_MISSION_STAGE_CAPTURE_AREA
		IF IS_BIT_SET(iLocalBoolCheck14, LBOOL14_VFX_CAPTURE_OWNER)
			IF g_iTheWinningTeam = -1
				//Show Yellow = Tied
				GET_HUD_COLOUR(HUD_COLOUR_YELLOW, iR, iG, iB, iA)
			ELSE
				TeamColour = GET_HUD_COLOUR_FOR_FMMC_TEAM(g_iTheWinningTeam, PlayerToUse)
				//Team 0 - Orange
				//Team 1 - Green
				//Team 2 - Pink
				//Team 3 - Purple
				PRINTLN( "[RCC MISSION][AW SMOKE] TEAM ",g_iTheWinningTeam, "is winning")
				GET_HUD_COLOUR(TeamColour, iR, iG, iB, iA)
			ENDIF
		ENDIF
		IF iRule < FMMC_MAX_RULES
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetNine[iRule], ciBS_RULE9_PROGRESS_VEHICLES_WITH_RESPAWNS)
				IF iTeamWhoOwnThis != -1
					GET_HUD_COLOUR(GET_HUD_COLOUR_FOR_FMMC_TEAM(iTeamWhoOwnThis, PlayerToUse), iR, iG, iB, iA)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].fTeamMembersNeededWithin > 0
	AND NOT ARE_ALL_TEAM_MEMBERS_WITHIN_REQUIRED_DISTANCE(iTeam)
		GET_HUD_COLOUR(HUD_COLOUR_RED, iR, iG, iB, iA)
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].sLocBlipStruct.iBlipColour > 0
	AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS3, ciLoc_BS3_UseBlipColourForCorona)
		INT iBlipColour = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].sLocBlipStruct.iBlipColour 
		
		IF (g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].sLocBlipStruct.iBlipColourAggro != BLIP_COLOUR_DEFAULT AND HAS_TEAM_TRIGGERED_AGGRO(iTeam, g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iAggroIndexBS_Entity_Loc))
			iBlipColour = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].sLocBlipStruct.iBlipColourAggro
		ENDIF
		
		GET_HUD_COLOUR(MC_GET_HUD_COLOUR_FROM_BLIP_COLOUR(GET_BLIP_COLOUR_FROM_CREATOR(iBlipColour)), iR, iG, iB, iA)
	ENDIF
	
	GET_CORONA_SIZE_AND_HEIGHT(fcoronasize, fcoronaheight,fradius,bdropoff,iloc)	
	
	IF iAlphaOverride > -1
	AND iAlphaOverride < 256
		iA = iAlphaOverride
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iLocateAlpha != -1
		iA = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iLocateAlpha
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iLocBS2,ciLoc_BS2_MakeMarkerMoreVisible)
		IF iRule < FMMC_MAX_RULES
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetNine[iRule], ciBS_RULE9_TEAM_COLOURED_LOCATES)
				iTeamWhoOwnThis = MC_serverBD.iLocOwner[iloc]
				
				// It doesn't matter if this sets a team twice, because we override that with the contested colour
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_INSTANT_COLOUR_CHANGES_FOR_INSIDE_LOCATE)
					INT i = 0
					FOR i = 0 TO FMMC_MAX_TEAMS-1
						IF MC_serverBD_1.inumberOfTeamInArea[iLoc][i] > 0
							iTeamWhoOwnThis = i
						ENDIF
					ENDFOR
				ENDIF
				
				IF iTeamWhoOwnThis > -1
					GET_HUD_COLOUR(GET_HUD_COLOUR_FOR_FMMC_TEAM(iTeamWhoOwnThis, PlayerToUse), iR, iG, iB, iA)
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciENABLE_CONTESTED_COLOUR_FOR_LOCATES)
				IF iTeamWhoOwnThis > -1
					INT iHostileTeamsInsideLocate
					INT i = 0
					
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_INSTANT_COLOUR_CHANGES_FOR_INSIDE_LOCATE)
						FOR i = 0 TO FMMC_MAX_TEAMS-1
							iHostileTeamsInsideLocate = -1
							IF MC_serverBD_1.inumberOfTeamInArea[iLoc][i] > 0
								iHostileTeamsInsideLocate++
							ENDIF
						ENDFOR
					ELSE			
						FOR i = 0 TO FMMC_MAX_TEAMS-1
							IF NOT DOES_TEAM_LIKE_TEAM(iTeamWhoOwnThis, i)
							AND iTeamWhoOwnThis != i
								IF MC_serverBD_1.inumberOfTeamInArea[iLoc][i] > 0
									iHostileTeamsInsideLocate++
								ENDIF
							ENDIF
						ENDFOR
					ENDIF
					
					IF iHostileTeamsInsideLocate > 0
						iR = 255
						iG = 0
						iB = 0
						iA = 200
					ENDIF
				ENDIF
			ENDIF
		ENDIF

		DRAW_MARKER(MARKER_SPHERE,
			vPos,
			<<0,0,0>>,
			<<0,0,0>>,
			<<fcoronasize,fcoronasize,fcoronaheight>>,
			iR,
			iG,
			iB,
			iA)
	ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iLocBS2,ciLoc_BS2_UseLineMarker) 
		
		IF bEnemyMarker
			GET_HUD_COLOUR(HUD_COLOUR_BLUE, iR, iG, iB, iA)
		ELSE
			GET_HUD_COLOUR(HUD_COLOUR_RED, iR, iG, iB, iA)
		ENDIF
		
		fDirection = GET_DIRECTION_FOR_LOCATE_MARKER(iLoc)
		
		DRAW_MARKER( MARKER_LINES,
			<<vPos.x, vPos.y, vPos.z+g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].fGroundClear>>,
			<<0,0,0>>,
			<<0,g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].fLocateTilt,fDirection>>,
			<<fradius,1,1>>,
			iR,
			iG,
			iB,
			iA )
	ELSE
		INT iHostileTeamsInsideLocate
		
		IF GET_MC_CLIENT_MISSION_STAGE(iPartToUse) = CLIENT_MISSION_STAGE_CAPTURE_AREA
		AND IS_BIT_SET(iLocalBoolCheck14, LBOOL14_VFX_CAPTURE_OWNER)
			IF iRule < FMMC_MAX_RULES
								
				iR = GET_CORONA_TRANSITION(0,0)
				iG = GET_CORONA_TRANSITION(0,1)
				iB = GET_CORONA_TRANSITION(0,2)
				
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetNine[iRule], ciBS_RULE9_TEAM_COLOURED_LOCATES)
					iTeamWhoOwnThis = MC_serverBD.iLocOwner[iloc]
					// It doesn't matter if this sets a team twice, because we override that with the contested colour
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_INSTANT_COLOUR_CHANGES_FOR_INSIDE_LOCATE)
						INT i = 0
						FOR i = 0 TO FMMC_MAX_TEAMS-1
							iTeamWhoOwnThis = i
						ENDFOR
					ENDIF
					
					IF iTeamWhoOwnThis > -1
						GET_HUD_COLOUR(GET_HUD_COLOUR_FOR_FMMC_TEAM(iTeamWhoOwnThis, PlayerToUse), iR, iG, iB, iA)
					ELSE
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciENABLE_CONTESTED_COLOUR_FOR_LOCATES)
							iHostileTeamsInsideLocate = 0
							INT i = 0
							IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_INSTANT_COLOUR_CHANGES_FOR_INSIDE_LOCATE)										
								iHostileTeamsInsideLocate = -1
								FOR i = 0 TO FMMC_MAX_TEAMS-1
									IF MC_serverBD_1.inumberOfTeamInArea[iLoc][i] > 0
										iHostileTeamsInsideLocate++
									ENDIF
								ENDFOR
							ELSE
								FOR i = 0 TO FMMC_MAX_TEAMS-1
									IF NOT DOES_TEAM_LIKE_TEAM(MC_playerBD[iLocalPart].iteam, i)
									AND MC_playerBD[iLocalPart].iteam != i
										IF MC_serverBD_1.inumberOfTeamInArea[iLoc][i] > 0
											iHostileTeamsInsideLocate++
										ENDIF
									ENDIF
								ENDFOR
							ENDIF
							
							IF (iHostileTeamsInsideLocate > 0
							AND MC_serverBD_1.inumberOfTeamInArea[iLoc][iTeam] > 0)
								IF iColourTeamCacheGotoLocateOld[iLoc] != -1
									iColourTeamCacheGotoLocateOld[iLoc] = -1
								ENDIF
								iR = 255
								iG = 0
								iB = 0
							ELSE
								iR = 255
								iG = 255
								iB = 0
							ENDIF
						ELSE
							iR = 255
							iG = 255
							iB = 0
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciENABLE_CONTESTED_COLOUR_FOR_LOCATES)				
					IF iTeamWhoOwnThis > -1
					OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_INSTANT_COLOUR_CHANGES_FOR_INSIDE_LOCATE)			
						iHostileTeamsInsideLocate = 0
						INT i = 0
									
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_INSTANT_COLOUR_CHANGES_FOR_INSIDE_LOCATE)										
							iHostileTeamsInsideLocate = -1
							FOR i = 0 TO FMMC_MAX_TEAMS-1
								IF MC_serverBD_1.inumberOfTeamInArea[iLoc][i] > 0
									iHostileTeamsInsideLocate++
								ENDIF
							ENDFOR
						ELSE
							FOR i = 0 TO FMMC_MAX_TEAMS-1
								IF NOT DOES_TEAM_LIKE_TEAM(iTeamWhoOwnThis, i)
								AND iTeamWhoOwnThis != i
									IF MC_serverBD_1.inumberOfTeamInArea[iLoc][i] > 0
										iHostileTeamsInsideLocate++
									ENDIF
								ENDIF
							ENDFOR
						ENDIF
					
						IF iHostileTeamsInsideLocate > 0
							IF iColourTeamCacheGotoLocateOld[iLoc] != -1
								iColourTeamCacheGotoLocateOld[iLoc] = -1
							ENDIF
							
							iR = 255
							iG = 0
							iB = 0
							iA = 200
						ENDIF
					ENDIF
				ENDIF
			ENDIF

			PROCESS_CORONA_TRANSITION(0)// might need to remove this.
			PRINTLN("[AW_SMOKE][DRAW_LOCATE_MARKER( ] INVALID TEAM SELECTED") 
			DRAW_MARKER(MARKER_CYLINDER,
			vPos,
			(<<0,0,0>>),
			(<<0,0,0>>),
			(<<fcoronasize,fcoronasize,fcoronaheight>>),
			iR,
			iG,
			iB,
			iA)			
		ELSE
			IF iRule < FMMC_MAX_RULES
				INT i = 0
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetNine[iRule], ciBS_RULE9_TEAM_COLOURED_LOCATES)
					iTeamWhoOwnThis = MC_serverBD.iLocOwner[iloc]
					
					// It doesn't matter if this sets a team twice, because we override that with the contested colour
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_INSTANT_COLOUR_CHANGES_FOR_INSIDE_LOCATE)
						FOR i = 0 TO FMMC_MAX_TEAMS-1
							IF MC_serverBD_1.inumberOfTeamInArea[iLoc][i] > 0
								iTeamWhoOwnThis = i
							ENDIF
						ENDFOR
					ENDIF
					
					IF iTeamWhoOwnThis > -1
						GET_HUD_COLOUR(GET_HUD_COLOUR_FOR_FMMC_TEAM(iTeamWhoOwnThis, PlayerToUse), iR, iG, iB, iA)
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciENABLE_CONTESTED_COLOUR_FOR_LOCATES)
					IF iTeamWhoOwnThis > -1
					OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_INSTANT_COLOUR_CHANGES_FOR_INSIDE_LOCATE)	
						iHostileTeamsInsideLocate = 0
									
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_INSTANT_COLOUR_CHANGES_FOR_INSIDE_LOCATE)
							iHostileTeamsInsideLocate = -1
							FOR i = 0 TO FMMC_MAX_TEAMS-1
								IF MC_serverBD_1.inumberOfTeamInArea[iLoc][i] > 0
									iHostileTeamsInsideLocate++
								ENDIF
							ENDFOR
						ELSE
							FOR i = 0 TO FMMC_MAX_TEAMS-1
								IF NOT DOES_TEAM_LIKE_TEAM(iTeamWhoOwnThis, i)
								AND iTeamWhoOwnThis != i
									IF MC_serverBD_1.inumberOfTeamInArea[iLoc][i] > 0
										iHostileTeamsInsideLocate++
									ENDIF
								ENDIF
							ENDFOR
						ENDIF
						
						IF iHostileTeamsInsideLocate > 0
							IF iColourTeamCacheGotoLocateOld[iLoc] != -1
								iColourTeamCacheGotoLocateOld[iLoc] = -1
							ENDIF
							
							iR = 255
							iG = 0
							iB = 0
							iA = 200
						ENDIF
					ELSE
						iR = 255
						iG = 255
						iB = 0
						iA = 200
					ENDIF
				ENDIF
				
				IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDropOffType[iRule] = ciFMMC_DROP_OFF_TYPE_CYLINDER
				AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThirteen[iRule], ciBS_RULE13_CHECK_CYLINDER)
					GET_GROUND_Z_FOR_3D_COORD(vPos, vPos.z)
				ENDIF
			ENDIF
			
			DRAW_MARKER(MARKER_CYLINDER,
			vPos,
			<<0,0,0>>,
			<<0,0,0>>,
			<<fcoronasize,fcoronasize,fcoronaheight>>,
			iR,
			iG,
			iB,
			iA)
		ENDIF
	ENDIF
	
ENDPROC


FUNC VECTOR GET_COORDS_FOR_LOCATE_MARKER(FLOAT &fRadius)

	VECTOR vcenter
	FLOAT fwater
	VECTOR vCoords 
	
	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]
	
	IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDropOffType[iRule] = ciFMMC_DROP_OFF_TYPE_CYLINDER
	OR g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDropOffType[iRule] = ciFMMC_DROP_OFF_TYPE_SPHERE
		vCoords = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vDropOff[iRule]
		
		INT iveh = MC_playerBD[iPartToUse].iVehNear
		
		IF iveh = -1 // I'm not in a vehicle, what else can we find:
			IF getVehicleBeingDroppedOff != NULL
				CALL getVehicleBeingDroppedOff(iTeam)
			ENDIF
		ENDIF
		
		IF iveh != -1
			IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].vDropOffOverride)
				vCoords = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].vDropOffOverride
				IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].fVehDropOffVisualRadius > 0
					fRadius = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].fVehDropOffVisualRadius
				ENDIF
			ELSE
				IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sFMMCEndConditions[iteam].vDropOff2[iRule])
					IF iveh != iFirstHighPriorityVehicleThisRule[iteam]
						PRINTLN("[RCC MISSION] GET_COORDS_FOR_LOCATE_MARKER - Radius drop off, using vDropOff2 as this isn't the first priority vehicle on this rule - vehicle dropping off = ",iveh,", iFirstHighPriorityVehicleThisRule = ",iFirstHighPriorityVehicleThisRule[iteam])
						vCoords = g_FMMC_STRUCT.sFMMCEndConditions[iteam].vDropOff2[iRule]
					ENDIF
				ENDIF
			ENDIF
			
		ELIF iObjDeliveryOverridesObjToUse > -1
			vCoords = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjDeliveryOverridesObjToUse].vObjDropOffOverride
			
			IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjDeliveryOverridesObjToUse].fObjDropOffVisualRadius > 0
				fRadius = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjDeliveryOverridesObjToUse].fObjDropOffVisualRadius
			ENDIF
		ENDIF
		
	ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDropOffType[iRule] = ciFMMC_DROP_OFF_TYPE_AREA
		vcenter = (g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vDropOff[iRule] + g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vDropOff2[iRule])
		vcenter *= << 0.5, 0.5, 0.5 >>
		IF GET_WATER_HEIGHT_NO_WAVES(vcenter, fwater)
			vcenter.z = fwater
		ELSE
			GET_GROUND_Z_FOR_3D_COORD(vcenter,vcenter.z)
		ENDIF
		
		vCoords = vcenter
		
	ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDropOffType[iRule] = ciFMMC_DROP_OFF_TYPE_PLACED_ZONE
		IF iCurrentDropOffZone[iTeam] > -1
			vCoords = GET_ZONE_RUNTIME_CENTRE(iCurrentDropOffZone[iTeam])
			GET_GROUND_Z_FOR_3D_COORD(vCoords, vCoords.z)
		ENDIF
	ENDIF
	
	RETURN vCoords
	
ENDFUNC

FUNC BOOL IS_PARTICIPANT_FACING_CORRECT_DIRECTION_FOR_LOCATION( INT iLoc, INT iParticipant )
	BOOL bReturn = FALSE
	
	FLOAT fTargetHeading 	= g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ iLoc ].fDir
	FLOAT fTolerance		= g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ iLoc ].fDirTolerance
	FLOAT fPlayerHeading
	PARTICIPANT_INDEX partID = INT_TO_PARTICIPANTINDEX( iParticipant )
	PLAYER_INDEX playerID = NETWORK_GET_PLAYER_INDEX( partID )
	
	IF IS_ENTITY_ALIVE(GET_PLAYER_PED( playerID ))
	 fPlayerHeading	= GET_ENTITY_HEADING( GET_PLAYER_PED( playerID ) )
	ENDIF

	IF ABSF( GET_ANGULAR_DIFFERENCE( fPlayerHeading, fTargetHeading - 180.0 ) )  <= fTolerance
		bReturn = TRUE
	ENDIF
	
	RETURN bReturn
ENDFUNC

PROC DRAW_ACTUAL_SIZE_ANGLED_AREA_DROP_OFF(INT iTeam, INT iRule, VECTOR vCoords)
	FLOAT fLength = GET_DISTANCE_BETWEEN_COORDS(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vDropOff[iRule], g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vDropOff2[iRule])
	FLOAT fHeading = GET_HEADING_BETWEEN_VECTORS(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vDropOff[iRule], g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vDropOff2[iRule])
	VECTOR vBoxSize = <<g_FMMC_STRUCT.sFMMCEndConditions[iTeam].fDropOffRadius[iRule], fLength, 0.85>>
	
	INT iR, iG, iB, iA
	GET_HUD_COLOUR(HUD_COLOUR_YELLOW, iR, iG, iB, iA)
	
	DRAW_MARKER(MARKER_BOXES,
	vCoords + <<0, 0, -0.25>>,
	<<0, 0, 0>>,
	<<0, 0, fHeading>>,
	vBoxSize,
	iR,
	iG,
	iB,
	iA)
ENDPROC

PROC DRAW_GROUND_DROP_OFF(FLOAT fRadius, INT iAlphaOverride = -1)
	
	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]
	VECTOR vCoords = GET_COORDS_FOR_LOCATE_MARKER(fRadius)
	
	IF IS_OBJECTIVE_BLOCKED()
		PRINTLN("DRAW_GROUND_DROP_OFF - Exiting due to IS_OBJECTIVE_BLOCKED")
		EXIT
	ENDIF
	
	SWITCH g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDropOffType[iRule]
		
		CASE ciFMMC_DROP_OFF_TYPE_CYLINDER
		CASE ciFMMC_DROP_OFF_TYPE_SPHERE
		CASE ciFMMC_DROP_OFF_TYPE_AREA
			IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDropOffType[iRule] = ciFMMC_DROP_OFF_TYPE_AREA
			AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iExactDropOffBitSet, iRule)
				DRAW_ACTUAL_SIZE_ANGLED_AREA_DROP_OFF(iTeam, iRule, vCoords)
			ELSE
				DRAW_LOCATE_MARKER(vCoords, fRadius, TRUE, 0, iAlphaOverride)
				PRINTLN("DRAW_GROUND_DROP_OFF - Called, vCoords: ", vCoords, " fRadius: ", fRadius)
			ENDIF
		BREAK
		
		CASE ciFMMC_DROP_OFF_TYPE_PLACED_ZONE
			DRAW_LOCATE_MARKER(vCoords, fRadius, TRUE, 0, iAlphaOverride)
			PRINTLN("DRAW_GROUND_DROP_OFF - (Zone) Called, vCoords: ", vCoords, " fRadius: ", fRadius)
		BREAK
		
	ENDSWITCH
	
ENDPROC

PROC DRAW_AIR_DROP_OFF()

	VECTOR vDropOff
	
	IF g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iDropOffType[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]] = ciFMMC_DROP_OFF_TYPE_CYLINDER
	OR g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iDropOffType[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]] = ciFMMC_DROP_OFF_TYPE_SPHERE
		FLOAT fTemp
		vDropOff = GET_COORDS_FOR_LOCATE_MARKER(fTemp)
	ELIF g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iDropOffType[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]] = ciFMMC_DROP_OFF_TYPE_AREA
		vDropOff = (g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].vDropOff[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]] + g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].vDropOff2[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]])
		vDropOff *= << 0.5, 0.5, 0.5 >>
	ELSE //It's a vehicle drop off
		//Shouldn't have a drop off corona drawn
		vDropOff = <<0, 0, 0>>
	ENDIF
	
	IF NOT IS_VECTOR_ZERO(vDropOff)
		DRAW_AIR_MARKER(vDropOff, TRUE)
	ENDIF
	
ENDPROC

FUNC BOOL IS_MY_VEHICLE_STOPPED(bool bSkipGetDeliveryCheck = false, FLOAT fStoppingDistance = DEFAULT_VEH_STOPPING_DISTANCE, BOOL bDisableExit = TRUE #IF IS_DEBUG_BUILD, BOOL bDoExtraDebug = FALSE #ENDIF)//FLOAT fStoppingDistance = BVTH_STOPPING_DISTANCE, INT iTimeToStopFor = BVTH_TIME_TO_STOP_FOR)
	
	IF SHOULD_VEHICLE_STOP_OR_BRING_TO_HALT_WAIT()
		RETURN FALSE
	ENDIF
	
	PRINTLN("[RCC MISSION] [IS_MY_VEHICLE_STOPPED] Called with extra debug")
	#IF IS_DEBUG_BUILD
		IF bDoExtraDebug
			PRINTLN("[RCC MISSION] [IS_MY_VEHICLE_STOPPED] Called with extra debug")
		ENDIF
	#ENDIF
	
	//bool passed in to skip the delivery rule check - mostly used when goto locates seperate from any rules want to stop the vehicle 		
	IF NOT bSkipGetDeliveryCheck
		IF NOT SHOULD_VEH_STOP_ON_DELIVERY()
			CPRINTLN(DEBUG_MISSION,"[RCC MISSION] IS_MY_VEHICLE_STOPPED: not SHOULD_VEH_STOP_ON_DELIVERY return true") 
			RETURN TRUE
		ENDIF
	ENDIF
	
	INT iTeam 
	INT iRule
	BOOL bEnableShooting = IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFour[iRule], ciBS_RULE4_ENABLE_WEAPONS_WHEN_STOPPED)
	
	IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		VEHICLE_INDEX tempVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
		
		IF IS_PED_IN_ANY_BOAT(LocalPlayerPed)
		OR IS_PED_IN_ANY_AMPHIBIOUS_VEHICLE(LocalPlayerPed) // url:bugstar:3498788
		OR IS_PED_IN_ANY_SEAPLANE_ON_WATER(LocalPlayerPed)
			IF IS_VEHICLE_DRIVEABLE(tempVeh)
				IF GET_PED_IN_VEHICLE_SEAT(tempVeh) = LocalPlayerPed
					IF NETWORK_HAS_CONTROL_OF_ENTITY(tempVeh)
						IF NOT IS_ENTITY_IN_AIR(tempVeh)
							FLOAT fSpeed = GET_ENTITY_SPEED(tempVeh)
							IF	NETWORK_HAS_CONTROL_OF_ENTITY(tempVeh)
								IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(tempVeh, fStoppingDistance, DEFAULT, DEFAULT, DEFAULT, bDisableExit, bEnableShooting)
								OR fSpeed < 2.5
									IF IS_PED_IN_ANY_BOAT(LocalPlayerPed)
										IF CAN_ANCHOR_BOAT_HERE(tempVeh)
											SET_BOAT_ANCHOR(tempVeh,TRUE)
											SET_BOAT_REMAINS_ANCHORED_WHILE_PLAYER_IS_DRIVER(tempVeh,TRUE)	//anchor boat if speed is reasonably low
										ENDIF
									ELSE
										SET_VEHICLE_HANDBRAKE(tempVeh,TRUE)
									ENDIF
									CPRINTLN(DEBUG_MISSION,"[RCC MISSION] IS_MY_VEHICLE_STOPPED: TRUE - CALLING SET_BOAT_ANCHOR() FOR BOAT")
									SET_BIT(iLocalBoolCheck2, LBOOL2_BRING_VEHICLE_TO_HALT_CALLED)
									RETURN TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELIF IS_PED_IN_ANY_HELI(LocalPlayerPed)
			IF IS_VEHICLE_DRIVEABLE(tempVeh)
				IF GET_PED_IN_VEHICLE_SEAT(tempVeh) = LocalPlayerPed
					IF NETWORK_HAS_CONTROL_OF_ENTITY(tempVeh)
						IF GET_ENTITY_SPEED(tempVeh) < 4.0
							iTeam = MC_playerBD[iPartToUse].iteam
	 						iRule = MC_serverBD_4.iCurrentHighestPriority[ iTeam ]
							IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iDropOffBitSet, iRule ) // Air drop off
								CPRINTLN(DEBUG_MISSION,"[RCC MISSION] IS_MY_VEHICLE_STOPPED: TRUE - HELICOPTER CASE - Air drop off")
								RETURN TRUE
							ELSE
								IF NOT IS_ENTITY_IN_AIR(tempVeh)
								AND IS_ENTITY_UPRIGHT(tempVeh, 20)
								AND (IS_VEHICLE_ON_ALL_WHEELS(tempVeh)
								OR (GET_VEHICLE_HAS_LANDING_GEAR(tempVeh) AND GET_LANDING_GEAR_STATE(tempVeh) = LGS_BROKEN)
								OR NOT GET_VEHICLE_HAS_LANDING_GEAR(tempVeh))
									CPRINTLN(DEBUG_MISSION,"[RCC MISSION] IS_MY_VEHICLE_STOPPED: TRUE - HELICOPTER CASE - Not in air")
									RETURN TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF IS_VEHICLE_DRIVEABLE(tempVeh)
				IF (GET_ENTITY_MODEL(tempVeh) = INT_TO_ENUM(MODEL_NAMES, HASH("TRAILERLARGE"))
				OR GET_ENTITY_MODEL(tempVeh) = INT_TO_ENUM(MODEL_NAMES, HASH("TRAILERSMALL2")))
				AND NOT IS_ENTITY_DEAD(GET_ENTITY_ATTACHED_TO(tempVeh))
					tempVeh = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_ATTACHED_TO(tempVeh))
					CPRINTLN(DEBUG_MISSION,"[RCC MISSION] IS_MY_VEHICLE_STOPPED: Vehicle is a trailer, checking the attached vehicle instead")
				ENDIF
				IF GET_ENTITY_MODEL(tempVeh) = INT_TO_ENUM(MODEL_NAMES, HASH("DELUXO"))
				AND IS_ENTITY_IN_AIR(tempVeh)
					CPRINTLN(DEBUG_MISSION,"[RCC MISSION] IS_MY_VEHICLE_STOPPED: Vehicle is a Deluxo, setting wheels down.")
					SET_SPECIAL_FLIGHT_MODE_TARGET_RATIO(tempVeh, 0)
				ENDIF
				IF NETWORK_HAS_CONTROL_OF_ENTITY(tempVeh)
					IF GET_PED_IN_VEHICLE_SEAT(tempVeh) = LocalPlayerPed
					
						IF NOT IS_VEHICLE_STOPPED(tempVeh)	
						
							IF NOT IS_BIT_SET(iLocalBoolCheck2, LBOOL2_BRING_VEHICLE_TO_HALT_CALLED)
								SET_VEHICLE_HANDBRAKE(tempVeh,TRUE)
								SET_BIT(iLocalBoolCheck2, LBOOL2_BRING_VEHICLE_TO_HALT_CALLED)
								CPRINTLN(DEBUG_MISSION,"[RCC MISSION] IS_MY_VEHICLE_STOPPED: CALLING BRING_VEHICLE_TO_HALT() FOR VEHICLE fStoppingDistance = ", fStoppingDistance, " Time ", GET_CLOUD_TIME_AS_INT())
								
							ELSE
								FLOAT fSpeed = GET_ENTITY_SPEED(tempVeh)
								if	NETWORK_HAS_CONTROL_OF_ENTITY(tempVeh)
									IF not BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(tempVeh,fStoppingDistance, 1,0.5,False,True, bEnableShooting)
										IF fSpeed > 2
											IF NOT HAS_NET_TIMER_STARTED(tdDeliverBackupTimer)
												REINIT_NET_TIMER(tdDeliverBackupTimer)
											ENDIF
											CPRINTLN(DEBUG_MISSION,"[RCC MISSION] IS_MY_VEHICLE_STOPPED: fSpeed > 2 ",fSpeed)								
										ELSE
											IF NOT HAS_NET_TIMER_STARTED(tdDeliverBackupTimer)
												REINIT_NET_TIMER(tdDeliverBackupTimer)
											ENDIF									
											CPRINTLN(DEBUG_MISSION,"[RCC MISSION] IS_MY_VEHICLE_STOPPED: new fSpeed <2 ",fSpeed)
										ENDIF	
										CPRINTLN(DEBUG_MISSION,"[RCC MISSION] IS_MY_VEHICLE_STOPPED: not stopped ",fSpeed)								
									endif
								endif
							ENDIF
						ENDIF										
					ELSE
						CPRINTLN(DEBUG_MISSION,"[RCC MISSION] IS_MY_VEHICLE_STOPPED: TRUE - PLAYER PED IS NOT THE DRIVER") 
						RETURN TRUE	
					ENDIF
				ENDIF	
			ENDIF
		ENDIF
		
		IF IS_VEHICLE_DRIVEABLE(tempVeh)
		AND NOT IS_PED_IN_ANY_HELI(LocalPlayerPed)
			FLOAT fSpeed = GET_ENTITY_SPEED(tempVeh)
			IF (fSpeed < 0.1 AND NOT IS_ENTITY_IN_AIR(tempVeh))
			OR (GET_ENTITY_MODEL(tempVeh) = OPPRESSOR2 AND fSpeed < 1.0)
				CPRINTLN(DEBUG_MISSION,"[RCC MISSION] IS_MY_VEHICLE_STOPPED: TRUE - VEHICLE IS STOPPED AND ON GROUND ",fSpeed) 
				if SHOULD_STAY_IN_ON_DELIVERY()
				and	NETWORK_HAS_CONTROL_OF_ENTITY(tempVeh)
					//Disable veh controls for 3 seconds 
					BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(tempVeh,fStoppingDistance,3, DEFAULT, DEFAULT,bDisableExit, bEnableShooting)
				endif
				RETURN TRUE
			ELSE
				#IF IS_DEBUG_BUILD
					IF bDoExtraDebug
						PRINTLN("[RCC MISSION] [IS_MY_VEHICLE_STOPPED] Called with extra debug, Going to return false fSpeed = ", fSpeed, " IS_ENTITY_IN_AIR = ", IS_ENTITY_IN_AIR(tempVeh))
					ENDIF
				#ENDIF
			ENDIF
		ENDIF
		
	ELSE
		CPRINTLN(DEBUG_MISSION,"[RCC MISSION] IS_MY_VEHICLE_STOPPED: TRUE - PLAYER PED NOT IN ANY VEHICLE") 
		RETURN TRUE	
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF bDoExtraDebug
			PRINTLN("[RCC MISSION] [IS_MY_VEHICLE_STOPPED] Called with extra debug, returning false")
		ENDIF
	#ENDIF
	
	RETURN FALSE

ENDFUNC

///PURPOSE: This does all the logic specific to dropping off entities at a vehicle
FUNC BOOL IS_PLAYER_IN_VEHICLE_DROPOFF(INT iTeam, INT iRule, FLOAT fRadius, FLOAT fCoronaHeight)
	
	BOOL bReturn = FALSE

	IF ( g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iDropOff_Vehicle[ iRule ] > -1 )
	
		INT iVeh = g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iDropOff_Vehicle[ iRule ]
		NETWORK_INDEX niVeh = MC_serverBD_1.sFMMC_SBD.niVehicle[ iVeh ]
		
		IF NETWORK_DOES_NETWORK_ID_EXIST(niVeh)
		AND IS_VEHICLE_DRIVEABLE(NET_TO_VEH(niVeh))
			VEHICLE_INDEX tempVeh = NET_TO_VEH( niVeh )
			IF NOT IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iVehDropOff_GetInBS, iRule )
				IF NOT IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iVehDropOff_HookUpBS, iRule )
					IF GET_DISTANCE_BETWEEN_ENTITIES(LocalPlayerPed, tempVeh) <= fradius
						IF IS_ENTITY_AT_COORD(LocalPlayerPed,GET_ENTITY_COORDS(tempVeh),<<fradius,fradius,fcoronaheight>>,FALSE)	
							bReturn = TRUE
						ENDIF
					ENDIF
				ENDIF
			ELSE
				
				IF IS_VEHICLE_DRIVEABLE(tempVeh)
					IF IS_PED_IN_VEHICLE(LocalPlayerPed,tempVeh)
						
						INT iFollowersInVeh
						
						IF IS_WHOLE_GROUP_IN_VEHICLE(giPlayerGroup, tempVeh, iFollowersInVeh)
							
							BOOL bCheckOnRule
							INT iTeamCheckBS = GET_RULE_GOTO_TEAMS(iTeam, iRule, bCheckOnRule)
							
							IF iTeamCheckBS != 0
							AND ( IS_BIT_SET(g_FMMC_STRUCT.iVehRuleWaitForTeamsBS[iRule][iTeam], ciFMMC_VehRuleTeamBS_DISABLE_TEAMINVEH_PASS_ON_FULL_VEH)
								OR NOT IS_VEHICLE_FULL(tempVeh) )
								
								INT iLoop, iPlayersNeeded, iPlayersChecked, iPlayersInCar
								
								FOR iLoop = 0 TO (FMMC_MAX_TEAMS - 1)
									IF IS_BIT_SET(iTeamCheckBS, iLoop)
										iPlayersNeeded += MC_serverBD.iNumberOfPlayingPlayers[iLoop]
									ENDIF
								ENDFOR
								
								PRINTLN("[PLAYER_LOOP] - IS_PLAYER_IN_VEHICLE_DROPOFF")
								FOR iLoop = 0 TO (GET_MAXIMUM_PLAYERS_ON_MISSION() - 1)
									
									IF iLoop != iPartToUse
										IF ( NOT IS_BIT_SET(MC_playerBD[iLoop].iClientBitSet,PBBOOL_ANY_SPECTATOR)
											 OR IS_BIT_SET(MC_playerBD[iLoop].iClientBitSet,PBBOOL_HEIST_SPECTATOR) )
										AND NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iLoop))
										AND IS_BIT_SET(iTeamCheckBS, MC_PlayerBD[iLoop].iteam)
											
											iPlayersChecked++
											
											PLAYER_INDEX tempPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iLoop))
											
											IF IS_NET_PLAYER_OK(tempPlayer)
												IF IS_PED_IN_VEHICLE(GET_PLAYER_PED(tempPlayer), tempVeh)
													IF IS_WHOLE_GROUP_IN_VEHICLE(GET_PLAYER_GROUP(tempPlayer), tempVeh, iFollowersInVeh)
														iPlayersInCar++
													#IF IS_DEBUG_BUILD
													ELSE
														PRINTLN("[RCC MISSION] IS_PLAYER_IN_VEHICLE_DROPOFF - Waiting for particpant ",iLoop,"'s group to enter the vehicle")
													#ENDIF
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ELSE
										iPlayersChecked++
										iPlayersInCar++
									ENDIF
									
									// If we've checked everyone we need to:
									IF iPlayersChecked >= iPlayersNeeded
										BREAKLOOP
									ENDIF
								ENDFOR
								
								IF iPlayersInCar >= iPlayersNeeded
									IF iFollowersInVeh >= MC_serverBD.iRequiredDeliveries[iTeam]
										bReturn = TRUE
										PRINTLN("[RCC MISSION] IS_PLAYER_IN_VEHICLE_DROPOFF - All needed / group peds are in the drop off vehicle with the player all players needed (iTeamCheckBS = ",iTeamCheckBS,"), returning true...")
									#IF IS_DEBUG_BUILD
									ELSE
										PRINTLN("[RCC MISSION] IS_PLAYER_IN_VEHICLE_DROPOFF - All players and their groups are in the car, but not enough peds are in! The group data is probably out of sync.")
									#ENDIF
									ENDIF
								ENDIF
								
							ELSE
								bReturn = TRUE
								PRINTLN("[RCC MISSION] IS_PLAYER_IN_VEHICLE_DROPOFF - All group peds are in the drop off vehicle with the player and no extra players needed in car (or vehicle is full), returning true...")
							ENDIF
						#IF IS_DEBUG_BUILD
						ELSE
							PRINTLN("[RCC MISSION] IS_PLAYER_IN_VEHICLE_DROPOFF - Not all of my peds are in the vehicle!")
						#ENDIF
						ENDIF
						
					ENDIF
				#IF IS_DEBUG_BUILD
				ELSE
					PRINTLN("[RCC MISSION] IS_PLAYER_IN_VEHICLE_DROPOFF - Drop off vehicle is no longer driveable!")
				#ENDIF
				ENDIF
			ENDIF
		#IF IS_DEBUG_BUILD
		ELSE
			PRINTLN("[RCC MISSION] IS_PLAYER_IN_VEHICLE_DROPOFF - Drop off vehicle doesn't exist!")
		#ENDIF
		ENDIF
	ENDIF
	
	RETURN bReturn
ENDFUNC

///PURPOSE: Returns true if the player is at one of the four kinds of dropoffs - 
///    Radius - at a coord
///    Property - at some predefined location, referred to as a "property"
///    Some other thing which is probably never used
///    Vehicle - this will either work if you're near a vehicle or if you (and all the peds (if any) following you) get into it
///    
FUNC BOOL IS_PLAYER_IN_DROP_OFF( FLOAT fradius, FLOAT fcoronaheight )
	
	BOOL bInDropOff = FALSE
	
	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[ iTeam ]
	#IF IS_DEBUG_BUILD
	IF bPlayerInDropOffPrints
		INT iLocalPartId = iLocalPart
		PRINTLN("[RCC MISSION] - IS_PLAYER_IN_DROP_OFF - being called.")
		PRINTLN("[RCC MISSION] - IS_PLAYER_IN_DROP_OFF - my participant ID = ", iLocalPartId)
		PRINTLN("[RCC MISSION] - IS_PLAYER_IN_DROP_OFF - iPartToUse = ", iPartToUse)
		PRINTLN("[RCC MISSION] - IS_PLAYER_IN_DROP_OFF - iTeam = ", iTeam)
		PRINTLN("[RCC MISSION] - IS_PLAYER_IN_DROP_OFF - iRule = ", iRule)
		PRINTLN("[RCC MISSION] - IS_PLAYER_IN_DROP_OFF - fradius = ", fradius)
		PRINTLN("[RCC MISSION] - IS_PLAYER_IN_DROP_OFF - fcoronaheight = ", fcoronaheight)
		PRINTLN("[RCC MISSION] - IS_PLAYER_IN_DROP_OFF - loc = ", g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].vDropOff[ iRule ])
	ENDIF
	#ENDIF
	
	IF iRule < FMMC_MAX_RULES
	
		#IF IS_DEBUG_BUILD
		IF bPlayerInDropOffPrints
		PRINTLN("[RCC MISSION] - IS_PLAYER_IN_DROP_OFF - iRule < FMMC_MAX_RULES (", FMMC_MAX_RULES, ").")
		ENDIF
		#ENDIF
		
		// Radius dropoff
		IF g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iDropOffType[ iRule ] = ciFMMC_DROP_OFF_TYPE_CYLINDER
		
			#IF IS_DEBUG_BUILD
			IF bPlayerInDropOffPrints
				PRINTLN("[RCC MISSION] - IS_PLAYER_IN_DROP_OFF - iDropOffType = ciFMMC_DROP_OFF_TYPE_CYLINDER.")
			ENDIF
			#ENDIF
			
			VECTOR vDropOff = GET_DROP_OFF_CENTER()
			VECTOR vPlayerCoords = GET_ENTITY_COORDS(LocalPlayerPed, FALSE)
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_USE_NEW_DROP_OFF_CHECKS)
				FLOAT fZDist = ABSF(vPlayerCoords.Z - vDropOff.Z)
				IF (fZDist >= 0 AND fZDist <= (fcoronaheight / 2)) OR fCoronaHeight = 0
					IF VDIST2(<<vPlayerCoords.x, vPlayerCoords.y, 0>>, <<vDropOff.x, vDropOff.y, 0>>) <= POW(fradius,2)
						bInDropOff = TRUE
					ENDIF
				ENDIF
			ELSE
				IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(LocalPlayerPed, vDropOff, FALSE) <= fradius
					IF IS_ENTITY_AT_COORD(LocalPlayerPed, vDropOff, <<fradius,fradius,fcoronaheight>>, FALSE)	
						bInDropOff = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			#IF IS_DEBUG_BUILD
			IF IS_BIT_SET(m_sharedDebugFlags, DBG_LOCATES_ON)
				IF fcoronaheight = 0
					DRAW_WIRE_CYLINDER(vDropOff, vDropoff + <<0,0,0.1>>, fradius, 0, 255, 0, 255)
				ELSE
					DRAW_WIRE_CYLINDER(vDropOff, vDropoff + <<0,0,fcoronaheight>>, fradius, 0, 255, 0, 255)
				ENDIF
			ENDIF
			#ENDIF
			
		ELIF g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iDropOffType[ iRule ] = ciFMMC_DROP_OFF_TYPE_SPHERE
			
			#IF IS_DEBUG_BUILD
			IF bPlayerInDropOffPrints
				PRINTLN("[RCC MISSION] - IS_PLAYER_IN_DROP_OFF - iDropOffType = ciFMMC_DROP_OFF_TYPE_SPHERE.")
			ENDIF
			#ENDIF
			
			IF IS_ENTITY_AT_COORD(LocalPlayerPed, GET_DROP_OFF_CENTER(), <<fradius,fradius,fradius>>)
			AND VDIST2(GET_DROP_OFF_CENTER(), GET_ENTITY_COORDS(LocalPlayerPed, FALSE)) <= POW(fradius,2)
				bInDropOff = TRUE
			ENDIF
			
			#IF IS_DEBUG_BUILD
			IF IS_BIT_SET(m_sharedDebugFlags, DBG_LOCATES_ON)
				DRAW_DEBUG_SPHERE(GET_DROP_OFF_CENTER(), fradius, 0, 255, 0, 120)
			ENDIF
			#ENDIF
			
		ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDropOffType[iRule] = ciFMMC_DROP_OFF_TYPE_PLACED_ZONE
			
			#IF IS_DEBUG_BUILD
			IF bPlayerInDropOffPrints
				PRINTLN("[RCC MISSION] - IS_PLAYER_IN_DROP_OFF - iDropOffType = ciFMMC_DROP_OFF_TYPE_PLACED_ZONE.")
			ENDIF
			#ENDIF
			
			IF iCurrentDropOffZone[iTeam] > -1
				
				IF IS_ZONE_TRIGGERING(iCurrentDropOffZone[iTeam])
					bInDropOff = TRUE
				ENDIF
				
				#IF IS_DEBUG_BUILD
				IF IS_BIT_SET(m_sharedDebugFlags, DBG_LOCATES_ON)
					DRAW_DEBUG_SPHERE(GET_DROP_OFF_CENTER(), fradius, 0, 255, 0, 120)
				ENDIF
				#ENDIF
			ENDIF
			
		// Area dropoff
		ELIF g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iDropOffType[ iRule ] = ciFMMC_DROP_OFF_TYPE_AREA
			
			#IF IS_DEBUG_BUILD
			IF bPlayerInDropOffPrints
			PRINTLN("[RCC MISSION] - IS_PLAYER_IN_DROP_OFF - iDropOffType = ciFMMC_DROP_OFF_TYPE_AREA.")
			ENDIF
			#ENDIF
		
			IF IS_ENTITY_IN_ANGLED_AREA( LocalPlayerPed,
										g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].vDropOff[ iRule ],
										g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].vDropOff2[ iRule ],
										g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].fDropOffRadius[ iRule ],
										FALSE )
				bInDropOff = TRUE
			ENDIF
		//We are dropping something off to a vehicle
		ELSE
			
			#IF IS_DEBUG_BUILD
			IF bPlayerInDropOffPrints
			PRINTLN("[RCC MISSION] - IS_PLAYER_IN_DROP_OFF - checking IS_PLAYER_IN_VEHICLE_DROPOFF.")
			ENDIF
			#ENDIF
			
			bInDropOff = IS_PLAYER_IN_VEHICLE_DROPOFF( iTeam, iRule, fRadius, fCoronaHeight )
			
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF bPlayerInDropOffPrints
	IF iRule < FMMC_MAX_RULES
	AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetNine[iRule], ciBS_RULE9_GET_DELIVER_LEAVE_AREA)	
		PRINTLN("[RCC MISSION] - IS_PLAYER_IN_DROP_OFF - bInDropOff final value = ", !bInDropOff)
	ELSE
		PRINTLN("[RCC MISSION] - IS_PLAYER_IN_DROP_OFF - bInDropOff final value = ", bInDropOff)
	ENDIF
	ENDIF
	#ENDIF
			
	IF iRule < FMMC_MAX_RULES
	AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetNine[iRule], ciBS_RULE9_GET_DELIVER_LEAVE_AREA)	
		RETURN !bInDropOff
	ELSE
		RETURN bInDropOff
	ENDIF
ENDFUNC


PROC REGISTER_AMBIENT_VEHICLE_AS_MISSION_ENTITY(INT iTeam, INT iRule)
	
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(OverwriteObjectiveNetID)
	AND IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		
		DeliveryVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
		
		IF IS_VEHICLE_DRIVEABLE(DeliveryVeh)
		AND NETWORK_HAS_CONTROL_OF_ENTITY(DeliveryVeh)
			IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].OverwriteEntityType[iRule] > 0
			AND g_FMMC_STRUCT.sFMMCEndConditions[iTeam].OverwriteEntityId[iRule] != -1
				IF NOT IS_ENTITY_A_MISSION_ENTITY(DeliveryVeh)
				AND NOT DECOR_EXIST_ON(DeliveryVeh,"Player_Vehicle")
					PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LOC / REGISTER_AMBIENT_VEHICLE_AS_MISSION_ENTITY - Calling SET_ENTITY_AS_MISSION_ENTITY with vehicle ",NATIVE_TO_INT(DeliveryVeh))
					SET_ENTITY_AS_MISSION_ENTITY(DeliveryVeh,FALSE,TRUE)
				#IF IS_DEBUG_BUILD
				ELSE
					IF IS_ENTITY_A_MISSION_ENTITY(DeliveryVeh)
						PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LOC / REGISTER_AMBIENT_VEHICLE_AS_MISSION_ENTITY - Not calling SET_ENTITY_AS_MISSION_ENTITY as vehicle is already a mission entity!")
					ENDIF
					IF DECOR_EXIST_ON(DeliveryVeh,"Player_Vehicle")
						PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LOC / REGISTER_AMBIENT_VEHICLE_AS_MISSION_ENTITY - Not calling SET_ENTITY_AS_MISSION_ENTITY as vehicle is a personal vehicle!")
					ENDIF
				#ENDIF
				ENDIF
				OverwriteObjectiveNetID= VEH_TO_NET(DeliveryVeh)
				SET_NETWORK_ID_EXISTS_ON_ALL_MACHINES(OverwriteObjectiveNetID , TRUE)
				PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LOC / REGISTER_AMBIENT_VEHICLE_AS_MISSION_ENTITY - ambient vehicle set as mission entity , net id: ",NATIVE_TO_INT(OverwriteObjectiveNetID))
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL ARE_ALL_PLAYERS_IN_SEPARATE_VEHICLES_AND_LOCATE(INT iRule, INT iTeam)
	IF (IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThree[iRule], ciBS_RULE3_ALL_IN_SEPARATE_VEHICLES))
		IF NOT IS_BIT_SET(MC_serverBD.iAllPlayersInSeparateVehiclesBS[iTeam], iRule)
			PRINTLN("[JS] ARE_ALL_PLAYERS_IN_SEPARATE_VEHICLES_AND_LOCATE - Waiting for players to get into separate vehicles at the location, iTeam: ", iTeam, " iRule: ", iRule)
		ELSE
			RETURN TRUE
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL ARE_ALL_PLAYERS_IN_UNIQUE_VEHICLES_AND_LOCATE(INT iRule, INT iTeam)
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetNine[iRule], ciBS_RULE9_PROGRESS_GAD_AT_SAME_TIME)
		IF NOT IS_BIT_SET(MC_serverBD.iAllPlayersInUniqueVehiclesBS[iTeam], iRule)
			PRINTLN("[JS] ARE_ALL_PLAYERS_IN_UNIQUE_VEHICLES_AND_LOCATE - Waiting for players to get into unique vehicles at the location")
		ELSE
			RETURN TRUE
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL ARE_ALL_PLAYERS_IN_DROP_OFF_CONDITIONS(INT iTeam, INT iRule, BOOL bIncludeLocalPlayer = TRUE)
	
	IF iRule >= FMMC_MAX_RULES	
		RETURN TRUE
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThirteen[iRule], ciBS_RULE13_DROP_OFF_REQUIRES_ALL_PLAYERS_PRESENCE)
		RETURN TRUE
	ENDIF
	
	BOOL bReturn = TRUE
	
	PARTICIPANT_INDEX tempPart
	INT iPart = 0 
	PRINTLN("[PLAYER_LOOP] - HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE ARE_ALL_PLAYERS_IN_DROP_OFF_CONDITIONS")
	FOR iPart = 0 TO (NUM_NETWORK_PLAYERS - 1)
		tempPart = INT_TO_PARTICIPANTINDEX(iPart)
		
		IF NOT bIncludeLocalPlayer
		AND iPart = iLocalPart
			RELOOP
		ENDIF
		
		IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
			
			IF NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_ANY_SPECTATOR) 
			AND NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_HEIST_SPECTATOR)
			AND NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_PLAYER_FAIL)
			AND NOT IS_BIT_SET( MC_playerBD[iPart].iClientBitSet, PBBOOL_FINISHED)
			AND IS_BIT_SET(MC_playerBD[iPart].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
				IF NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet2, PBBOOL2_IN_DROP_OFF)
					PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE ARE_ALL_PLAYERS_IN_DROP_OFF_CONDITIONS - Not all players are inside drop off. Waiting for iPart: ", iPart)
					bReturn = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN bReturn
ENDFUNC

FUNC BOOL DOES_THIS_PART_HAVE_THE_DROP_OFF_TARGET(INT iPart)
	IF NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_ANY_SPECTATOR) 
	AND NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_HEIST_SPECTATOR)
	AND NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_PLAYER_FAIL)
	AND NOT IS_BIT_SET( MC_playerBD[iPart].iClientBitSet, PBBOOL_FINISHED)
	AND IS_BIT_SET(MC_playerBD[iPart].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
		IF (MC_playerBD[iPart].iPedCarryCount > 0
		OR MC_playerBD[iPart].iVehCarryCount > 0
		OR MC_playerBD[iPart].iObjCarryCount > 0
		OR IS_BIT_SET( MC_playerBD[iPart].iClientBitSet, PBBOOL_WITH_CARRIER))
			PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE DOES_THIS_PART_HAVE_THE_DROP_OFF_TARGET iPart: ", iPart, " Has the/a target.")
			RETURN TRUE
		ENDIF
	ENDIF
			
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_ANY_PLAYER_HAVE_THE_DROP_OFF_TARGET()
			
	PARTICIPANT_INDEX tempPart
	INT iPart = 0 
	PRINTLN("[PLAYER_LOOP] - DOES_ANY_PLAYER_HAVE_THE_DROP_OFF_TARGET")
	FOR iPart = 0 TO (NUM_NETWORK_PLAYERS - 1)
		tempPart = INT_TO_PARTICIPANTINDEX(iPart)
				
		IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)			
			IF DOES_THIS_PART_HAVE_THE_DROP_OFF_TARGET(iPart)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN FALSE	
	
ENDFUNC

FUNC BOOL IS_ENTITY_IN_DROP_OFF_AREA(ENTITY_INDEX tempEnt, INT iteam, INT iPriority, INT iEntityType = ciRULE_TYPE_NONE, INT iEntityID = -1)

	BOOL bEntityIsInArea = FALSE
	
	FLOAT fradius = g_FMMC_STRUCT.sFMMCEndConditions[iteam].fDropOffRadius[iPriority]
	
	IF g_FMMC_STRUCT.sFMMCEndConditions[iteam].iDropOffType[iPriority] = ciFMMC_DROP_OFF_TYPE_CYLINDER
		VECTOR vDropOff = g_FMMC_STRUCT.sFMMCEndConditions[iteam].vDropOff[iPriority]
		
		SWITCH iEntityType
			CASE ciRULE_TYPE_VEHICLE
				IF iEntityID >= 0
				AND iEntityID < FMMC_MAX_VEHICLES
					IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iEntityID].vDropOffOverride)
						vDropOff = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iEntityID].vDropOffOverride
						IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iEntityID].fVehDropOffRadius > 0
							fradius = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iEntityID].fVehDropOffRadius
						ENDIF
					ELSE
						IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sFMMCEndConditions[iteam].vDropOff2[iPriority])
							IF iEntityID != iFirstHighPriorityVehicleThisRule[iteam]
								vDropOff = g_FMMC_STRUCT.sFMMCEndConditions[iteam].vDropOff2[iPriority]
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE ciRULE_TYPE_OBJECT
				IF iEntityID >= 0
				AND iEntityID < FMMC_MAX_NUM_OBJECTS
					IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iEntityID].vObjDropOffOverride)
						vDropOff = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iEntityID].vObjDropOffOverride
						IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iEntityID].fObjDropOffRadius > 0
							fradius = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iEntityID].fObjDropOffRadius
						ENDIF
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
		
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iteam].iRuleBitsetThirteen[iPriority], ciBS_RULE13_CHECK_CYLINDER)
			IF IS_ENTITY_AT_COORD(tempEnt, vDropOff, <<fradius,fradius,fradius>>, FALSE)	
				bEntityIsInArea = TRUE
			ENDIF
		ELSE
			VECTOR vEntityCoord = GET_ENTITY_COORDS(tempEnt)
			IF IS_POINT_IN_CYLINDER(vEntityCoord, vDropOff, fradius, g_FMMC_STRUCT.sFMMCEndConditions[iteam].fDropOffHeight[iPriority], FALSE)
				bEntityIsInArea = TRUE
			ENDIF 
		ENDIF
		
	ELIF g_FMMC_STRUCT.sFMMCEndConditions[iteam].iDropOffType[iPriority] = ciFMMC_DROP_OFF_TYPE_AREA
		IF DOES_ENTITY_EXIST(tempEnt)
			IF IS_ENTITY_IN_ANGLED_AREA(tempEnt,g_FMMC_STRUCT.sFMMCEndConditions[iteam].vDropOff[iPriority],g_FMMC_STRUCT.sFMMCEndConditions[iteam].vDropOff2[iPriority],fradius,FALSE)
				bEntityIsInArea = TRUE
			ENDIF
		ELSE
			PRINTLN("IS_ENTITY_IN_DROP_OFF_AREA - tempEnt doesn't exist")
		ENDIF
	ELIF g_FMMC_STRUCT.sFMMCEndConditions[iteam].iDropOffType[iPriority] = ciFMMC_DROP_OFF_TYPE_SPHERE
		IF VDIST2(GET_ENTITY_COORDS(tempEnt), g_FMMC_STRUCT.sFMMCEndConditions[iteam].vDropOff[iPriority]) < POW(fRadius,2)
			bEntityIsInArea = TRUE
		ENDIF
		
	ELIF g_FMMC_STRUCT.sFMMCEndConditions[iteam].iDropOffType[iPriority] = ciFMMC_DROP_OFF_TYPE_PLACED_ZONE
		IF iCurrentDropOffZone[iTeam] > -1
			RETURN IS_ENTITY_IN_FMMC_ZONE(tempEnt, iCurrentDropOffZone[iTeam], FALSE, FALSE)
		ENDIF
		
	ELSE //Delivering to a vehicle
		IF iPriority < FMMC_MAX_RULES
			IF (g_FMMC_STRUCT.sFMMCEndConditions[iteam].iDropOff_Vehicle[iPriority] > -1)
				
				NETWORK_INDEX niVeh = MC_serverBD_1.sFMMC_SBD.niVehicle[ g_FMMC_STRUCT.sFMMCEndConditions[iteam].iDropOff_Vehicle[iPriority] ]
				
				IF NETWORK_DOES_NETWORK_ID_EXIST(niVeh)
					
					VEHICLE_INDEX tempVeh = NET_TO_VEH( niVeh )
					
					IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iteam].iVehDropOff_GetInBS,iPriority)
						IF GET_DISTANCE_BETWEEN_ENTITIES(tempEnt, tempVeh) <= fradius
							IF IS_ENTITY_AT_COORD(tempEnt,GET_ENTITY_COORDS(tempVeh),<<fradius,fradius,fradius>>,FALSE)	
								bEntityIsInArea = TRUE
							ENDIF
						ENDIF
					ELSE //Need to get in to deliver stuff
						IF IS_VEHICLE_DRIVEABLE(tempVeh)
							IF IS_ENTITY_A_PED(tempEnt)
								IF IS_PED_IN_VEHICLE(GET_PED_INDEX_FROM_ENTITY_INDEX(tempEnt),tempVeh)
									bEntityIsInArea = TRUE
								ENDIF
							ELIF IS_ENTITY_AN_OBJECT(tempEnt)
								ENTITY_INDEX tempCarrier = GET_ENTITY_ATTACHED_TO(tempEnt)
								IF DOES_ENTITY_EXIST(tempCarrier)
									IF IS_ENTITY_A_PED(tempCarrier)
										IF IS_PED_IN_VEHICLE(GET_PED_INDEX_FROM_ENTITY_INDEX(tempCarrier),tempVeh)
											bEntityIsInArea = TRUE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN bEntityIsInArea
	
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section: Control --------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: 
// ##### 
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


PROC PROCESS_SECUROHACK_OBJECTIVE_CONTROL()

	IF GET_MC_CLIENT_MISSION_STAGE(iPartToUse) != CLIENT_MISSION_STAGE_CAPTURE_PED
	AND GET_MC_CLIENT_MISSION_STAGE(iPartToUse) != CLIENT_MISSION_STAGE_CAPTURE_VEH
		EXIT
	ENDIF
	
	IF NOT bReadyToCapture
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_SECUROSERV_HACKING)
			ASSERTLN("[FMMC_2020] - Securohack for vehicles/peds should be refactored")
			PRINTLN("[FMMC_2020] - Securohack for vehicles/peds should be refactored")
			IF iHackingPedHelpBitSet[0] + iHackingPedHelpBitSet[1] != 0
			OR iHackingVehHelpBitSet != 0
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_SPECIAL_TWO)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ROOF)
				
				IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH( HASH("appSecuroHack")) > 0
				AND IS_PHONE_ONSCREEN(TRUE))
				OR IS_FAKE_MULTIPLAYER_MODE_SET()
					bReadyToCapture = TRUE
					PRINTLN("[JS][SECUROHACK] App launched")
				ELSE
					IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_HEADLIGHT)
						IF NOT IS_PED_BEING_STUNNED(LocalPlayerPed)
							IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH( HASH("appSecuroHack")) = 0
							OR IS_FAKE_MULTIPLAYER_MODE_SET()
								IF NOT IS_PLAYER_RESPAWNING(LocalPlayer)
								AND NOT IS_PED_DEAD_OR_DYING(LocalPlayerPed)
									LAUNCH_CELLPHONE_APPLICATION( AppDummyApp0, TRUE, TRUE, FALSE )
									PRINTLN("[JS][SECUROHACK] Launching App")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			// No need to press any buttons or start hacking
			bReadyToCapture = TRUE
		ENDIF
		
		IF bReadyToCapture
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciFORCE_OBJECTIVE_MID_POINT_ON_HACK)
				IF NOT IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[MC_playerBD[iLocalPart].iTeam], MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iTeam])
					BROADCAST_OBJECTIVE_MID_POINT(MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iTeam], MC_playerBD[iLocalPart].iTeam)
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_SECUROSERV_HACKING)
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH( HASH("appSecuroHack")) = 0
			AND NOT IS_FAKE_MULTIPLAYER_MODE_SET()
				bReadyToCapture = FALSE
				PRINTLN("[JS][SECUROHACK] Turning bReadyToCapture off as app is closed")
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_SECUROSERV_HACKING)
	
		INT iPedLoop
		BOOL bSetProofs = FALSE
		IF (MC_serverBD_4.iPedExplodeKillOnCaptureBS[0] > 0 AND iHackingPedInRangeBitSet[0] > 0)
			FOR iPedLoop = 0 TO 31
				IF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iPedExplodeKillOnCaptureBS, iPedLoop)
					IF FMMC_IS_LONG_BIT_SET(iHackingPedInRangeBitSet, iPedLoop)
						bSetProofs = TRUE
						PRINTLN("[JS][SECUROHACK] In range of ped ",iPedLoop," due to explode")
					ENDIF
				ENDIF
			ENDFOR
		ENDIF
		
		IF (MC_serverBD_4.iPedExplodeKillOnCaptureBS[1] > 0 AND  iHackingPedInRangeBitSet[1] > 0)
			FOR iPedLoop = 32 TO FMMC_MAX_PEDS - 1
				IF FMMC_IS_LONG_BIT_SET(MC_serverBD_4.iPedExplodeKillOnCaptureBS, iPedLoop)
					IF FMMC_IS_LONG_BIT_SET(iHackingPedInRangeBitSet, iPedLoop)
						bSetProofs = TRUE
						PRINTLN("[JS][SECUROHACK] In range of ped ",iPedLoop," due to explode")
					ENDIF
				ENDIF
			ENDFOR
		ENDIF
		
		IF bSetProofs	
			IF NOT IS_BIT_SET(iLocalBoolCheck20, LBOOL20_PLAYER_PROOFS_ALTERED)
				PRINTLN("[JS][SECUROHACK] SETTING PROOFS")
				RUN_LOCAL_PLAYER_PROOF_SETTING(DEFAULT, TRUE)
				IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
					VEHICLE_INDEX tempVeh = GET_VEHICLE_PED_IS_USING(LocalPlayerPed)
					IF NETWORK_HAS_CONTROL_OF_ENTITY(tempVeh)
						SET_ENTITY_PROOFS(tempVeh, FALSE, FALSE, TRUE, FALSE, FALSE, FALSE)
					ENDIF
				ENDIF
				SET_BIT(iLocalBoolCheck20, LBOOL20_PLAYER_PROOFS_ALTERED)
			ENDIF
		ELSE
			IF IS_BIT_SET(iLocalBoolCheck20, LBOOL20_PLAYER_PROOFS_ALTERED)
				PRINTLN("[JS][SECUROHACK] CLEARING PROOFS")
				RUN_LOCAL_PLAYER_PROOF_SETTING(TRUE)
				IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
					VEHICLE_INDEX tempVeh = GET_VEHICLE_PED_IS_USING(LocalPlayerPed)
					IF NETWORK_HAS_CONTROL_OF_ENTITY(tempVeh)
						SET_ENTITY_PROOFS(tempVeh, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE)
					ENDIF
				ENDIF
				CLEAR_BIT(iLocalBoolCheck20, LBOOL20_PLAYER_PROOFS_ALTERED)
			ENDIF
		ENDIF
		
		BOOL bPlaySounds = FALSE
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH( HASH("appSecuroHack")) > 0
		AND IS_PHONE_ONSCREEN(TRUE)
			bPlaySounds = TRUE
		ENDIF
		
		IF NOT HAS_NET_TIMER_STARTED(tdHackingComplete)
			IF iHackPercentage >= 100
				IF bPlaySounds
					IF IS_BIT_SET(iLocalBoolCheck20, LBOOL20_HACKING_LOOP_STARTED)
						STOP_SOUND(iHackingLoopSoundID)
						RELEASE_SOUND_ID(iHackingLoopSoundID)
						iHackingLoopSoundID = -1
						CLEAR_BIT(iLocalBoolCheck20, LBOOL20_HACKING_LOOP_STARTED)
						
						iHackSuccessSoundProgress++
					ENDIF
					PLAY_SOUND_FRONTEND(-1, "Hack_Stop", "DLC_IE_SVM_Voltic2_Hacking_Sounds")
				ENDIF
				
				PRINTLN("[JS][SECUROHACK] Hack over")
				START_NET_TIMER(tdHackingComplete)
				
				iHackStage = ciSECUROSERV_HACK_STAGE_HACK_COMPLETE
				
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED ("FHHUD_DELUXOHACK")
					CLEAR_HELP()
					PRINTLN("[SECUROHACK] Clearing FHHUD_DELUXOHACK because the hack is complete!")
				ENDIF
			ELSE 
				IF NOT (iHackPercentage > 0)
					IF IS_BIT_SET(iLocalBoolCheck20, LBOOL20_HACKING_LOOP_STARTED)
						STOP_SOUND(iHackingLoopSoundID)
						RELEASE_SOUND_ID(iHackingLoopSoundID)
						iHackingLoopSoundID = -1
						CLEAR_BIT(iLocalBoolCheck20, LBOOL20_HACKING_LOOP_STARTED)
					ENDIF
				ENDIF
				IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciDISPLAY_HACK_PROGRESS_BAR)
					IF iHackingPedInRangeBitSet[0] = 0
					AND iHackingPedInRangeBitSet[1] = 0
					AND iHackingVehInRangeBitSet = 0
						IF iHackStage != ciSECUROSERV_HACK_STAGE_NO_SIGNAL
						AND iHackStage != ciSECUROSERV_HACK_STAGE_HACK_COMPLETE
							PRINTLN("[JS][SECUROHACK] not good, setting stage to no signal")
							PLAY_SOUND_FRONTEND(-1, "Hack_Stop", "DLC_IE_SVM_Voltic2_Hacking_Sounds")
							iHackStage = ciSECUROSERV_HACK_STAGE_NO_SIGNAL
							iHackPercentage = 0
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		ELSE
			IF iHackPercentage >= 100
			AND HAS_NET_TIMER_EXPIRED(tdHackingComplete, 500)
				IF bPlaySounds
				AND iHackSuccessSoundProgress > -1
					IF NOT IS_BIT_SET(iHackSuccessSoundPlayedBS, iHackSuccessSoundProgress)	
						PLAY_SOUND_FRONTEND(-1, "Hack_Complete", "DLC_IE_SVM_Voltic2_Hacking_Sounds")	
						SET_BIT(iHackSuccessSoundPlayedBS, iHackSuccessSoundProgress)
					ENDIF
				ENDIF
				
				iHackPercentage = 0
			ENDIF
			
			IF HAS_NET_TIMER_EXPIRED(tdHackingComplete, ciHACKING_COMPLETE_TIME)
				IF iHackStage != ciSECUROSERV_HACK_STAGE_HACKING
				AND iHackStage != ciSECUROSERV_HACK_STAGE_LOSING_SIGNAL
					PRINTLN("[JS][SECUROHACK] Going back to state 0")
					
					IF bPlaySounds
						IF NOT IS_HACK_STOP_SOUND_BLOCKED()
							PLAY_SOUND_FRONTEND(-1, "Hack_Stop", "DLC_IE_SVM_Voltic2_Hacking_Sounds")
						ENDIF
					ENDIF
					
					iHackStage = ciSECUROSERV_HACK_STAGE_NO_SIGNAL
					HANG_UP_AND_PUT_AWAY_PHONE()
				ENDIF
				RESET_NET_TIMER(tdHackingComplete)
			ENDIF
		ENDIF
		IF !bPlaySounds
		OR iHackStage = ciSECUROSERV_HACK_STAGE_NO_SIGNAL
			IF IS_BIT_SET(iLocalBoolCheck20, LBOOL20_HACKING_LOOP_STARTED)
				STOP_SOUND(iHackingLoopSoundID)
				RELEASE_SOUND_ID(iHackingLoopSoundID)
				iHackingLoopSoundID = -1
				CLEAR_BIT(iLocalBoolCheck20, LBOOL20_HACKING_LOOP_STARTED)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section: Player Rule Processing -----------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: 
// ##### 
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL IS_PLAYER_RULE_VALID(INT iteam)
	
	SWITCH iteam
	
		CASE 0
			IF NOT IS_TEAM_ACTIVE(1)
			AND NOT IS_TEAM_ACTIVE(2)
			AND NOT IS_TEAM_ACTIVE(3)
				RETURN FALSE
			ENDIF
		BREAK
		CASE 1
			IF NOT IS_TEAM_ACTIVE(0)
			AND NOT IS_TEAM_ACTIVE(2)
			AND NOT IS_TEAM_ACTIVE(3)
				RETURN FALSE
			ENDIF
		BREAK
		CASE 2
			IF NOT IS_TEAM_ACTIVE(0)
			AND NOT IS_TEAM_ACTIVE(1)
			AND NOT IS_TEAM_ACTIVE(3)
				RETURN FALSE
			ENDIF
		BREAK
		CASE 3
			IF NOT IS_TEAM_ACTIVE(0)
			AND NOT IS_TEAM_ACTIVE(1)
			AND NOT IS_TEAM_ACTIVE(2)
				RETURN FALSE
			ENDIF
		BREAK
		
	ENDSWITCH
	
	RETURN TRUE

ENDFUNC

PROC PROCESS_LOOT_THRESHOLD_OBJECTIVE(INT iTeam, INT iPlayerRule)

	IF IS_BIT_SET(MC_serverBD.iPlayerRuleBitset[iPlayerRule][iTeam], ciPLAYER_RULE_BITSET_UseBagCapacityForLootThreshold)
		
		IF !BAG_CAPACITY__IS_ENABLED()
			ASSERTLN("[LOOT RULE] PROCESS_LOOT_THRESHOLD_OBJECTIVE - Player rule: ", iPlayerRule, " Team: ", iTeam, " set as using capacity when capacity is disabled!")
			PRINTLN("[LOOT RULE] PROCESS_LOOT_THRESHOLD_OBJECTIVE - Player rule: ", iPlayerRule, " Team: ", iTeam, " set as using capacity when capacity is disabled!")
				
			#IF IS_DEBUG_BUILD
			ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_LOOT_THRESHOLD_INCORRECT_SETUP, ENTITY_RUNTIME_ERROR_TYPE_WARNING, "Player rule: # Team: # set as using capacity when capacity is disabled!", iPlayerRule, iTeam)
			#ENDIF
	
			EXIT
		ENDIF
		
		INT iLowestPercentage = ROUND((MC_serverBD.fLowestLootCapacity[iTeam] / BAG_CAPACITY__GET_MAX_CAPACITY()) * 100)
		IF iLowestPercentage < MC_serverBD.iPlayerRuleLimit[iPlayerRule][iTeam]
			EXIT
		ENDIF
		
		PRINTLN("[LOOT RULE] PROCESS_LOOT_THRESHOLD_OBJECTIVE - Player rule: ", iPlayerRule, " Team: ", iTeam, " passed, lowest capacity: ", iLowestPercentage, "% required: ", MC_serverBD.iPlayerRuleLimit[iPlayerRule][iTeam], "%")
	
	ELSE
		
		INT iLootStolen = GET_TOTAL_CASH_GRAB_TAKE() + GET_TOTAL_CASH_GRAB_DROPPED()
		INT iRequiredLoot = MC_serverBD.iPlayerRuleLimit[iPlayerRule][iTeam]
		
		IF IS_BIT_SET(MC_serverBD.iPlayerRuleBitset[iPlayerRule][iTeam], ciPLAYER_RULE_BITSET_ScaleLootThresholdByNumPlayers)
		AND MC_serverBD.iNumberOfPlayingPlayers[iTeam] > 0
			iRequiredLoot *= MC_serverBD.iNumberOfPlayingPlayers[iTeam]
		ENDIF
		
		IF iLootStolen < iRequiredLoot
			EXIT
		ENDIF
		
		PRINTLN("[LOOT RULE] PROCESS_LOOT_THRESHOLD_OBJECTIVE - Player rule: ", iPlayerRule, " Team: ", iTeam, " passed, loot grabbed: $", iLootStolen, " required: ", iRequiredLoot)
	
	ENDIF
	
	INT iPriority = MC_serverBD_4.iPlayerRulePriority[iPlayerRule][iTeam] 
	PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_PLAYER, iPlayerRule, iTeam, TRUE)
	MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_LOOT_GRABBED
	SET_TEAM_OBJECTIVE_OUTCOME(iTeam, iPriority, TRUE)
	IF IS_THIS_OBJECTIVE_OVER(ci_TARGET_PLAYER, iTeam, iPriority)
		SET_LOCAL_OBJECTIVE_END_MESSAGE(iTeam, iPriority, MC_serverBD.iReasonForObjEnd[iTeam])
		BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[iTeam], iTeam, iPriority, DID_TEAM_PASS_OBJECTIVE(iTeam, iPriority))
	ENDIF
	
ENDPROC

PROC PROCESS_POINTS_THRESHOLD_OBJECTIVE(INT iTeam, INT iPlayerRule)	
	INT iCurrentPoints  = MC_serverBD.iTeamScore[iTeam]
	INT iRequiredPoints = MC_serverBD.iPlayerRuleLimit[iPlayerRule][iTeam]
	
	IF iCurrentPoints < iRequiredPoints
		EXIT
	ENDIF
	
	PRINTLN("[POINTS RULE] PROCESS_POINTS_THRESHOLD_OBJECTIVE - Player rule: ", iPlayerRule, " Team: ", iTeam, " passed. Score: ", iCurrentPoints, ", points required: ", iRequiredPoints)
	
	INT iPriority = MC_serverBD_4.iPlayerRulePriority[iPlayerRule][iTeam] 
	PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_PLAYER, iPlayerRule, iTeam, TRUE)
	MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_POINTS_THRESHOLD_MET
	SET_TEAM_OBJECTIVE_OUTCOME(iTeam, iPriority, TRUE)
	IF IS_THIS_OBJECTIVE_OVER(ci_TARGET_PLAYER, iTeam, iPriority)
		SET_LOCAL_OBJECTIVE_END_MESSAGE(iTeam, iPriority, MC_serverBD.iReasonForObjEnd[iTeam])
		BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[iTeam], iTeam, iPriority, DID_TEAM_PASS_OBJECTIVE(iTeam, iPriority))
	ENDIF
ENDPROC
		

PROC PROCESS_GO_TO_PLAYER_OBJECTIVE(INT iTeam, INT iPlayerRule, INT iTargetTeam = -1)
	INT iPeopleAtPlayer
	
	INT iPartLoop, iPartsChecked
	PARTICIPANT_INDEX tempPart
	
	PRINTLN("[PLAYER_LOOP] - PROCESS_GO_TO_PLAYER_OBJECTIVE")
	FOR iPartLoop = 0 TO (GET_MAXIMUM_PLAYERS_ON_MISSION() - 1)
		
		IF MC_playerBD[iPartLoop].iteam = iTeam
			
			tempPart = INT_TO_PARTICIPANTINDEX(iPartLoop)
			
			IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
					
				IF (NOT IS_BIT_SET(MC_playerBD[iPartLoop].iClientBitSet,PBBOOL_ANY_SPECTATOR)
					OR IS_BIT_SET(MC_playerBD[iPartLoop].iClientBitSet,PBBOOL_HEIST_SPECTATOR))
				AND NOT IS_BIT_SET(MC_playerBD[iPartLoop].iClientBitSet,PBBOOL_PLAYER_FAIL)
					
					IF IS_BIT_SET(MC_playerBD[iPartLoop].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
					AND NOT IS_BIT_SET( MC_playerBD[iPartLoop].iClientBitSet, PBBOOL_FINISHED )
						
						IF MC_playerBD[iPartLoop].iPartNear != -1
						AND MC_playerBD[MC_playerBD[iPartLoop].iPartNear].iteam = iTargetTeam
							iPeopleAtPlayer++
						ENDIF
						
						iPartsChecked++
						
						IF iPartsChecked >= MC_serverBD.iNumberOfPlayingPlayers[iTeam]
							BREAKLOOP
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	BOOL bTargetTeamNotActive = FALSE
	IF iTargetTeam != -1
		IF NOT IS_TEAM_ACTIVE(iTargetTeam)
			bTargetTeamNotActive = TRUE
		ENDIF
	ENDIF
	
	IF (iPeopleAtPlayer > 0)
	OR bTargetTeamNotActive
		PRINTLN("[RCC MISSION] PROCESS_GO_TO_PLAYER_OBJECTIVE - go to player objective complete for team ",iTeam)
		INT iPriority = MC_serverBD_4.iPlayerRulePriority[iPlayerRule][iteam] 
		PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_PLAYER, iPlayerRule, iteam, TRUE)
		MC_serverBD.iReasonForObjEnd[iteam] = OBJ_END_REASON_PLAYERS_GONE_TO
		SET_TEAM_OBJECTIVE_OUTCOME(iteam, iPriority, TRUE)
		
		IF IS_THIS_OBJECTIVE_OVER(ci_TARGET_PLAYER, iteam, iPriority)
			SET_LOCAL_OBJECTIVE_END_MESSAGE(iteam, iPriority, MC_serverBD.iReasonForObjEnd[iteam])
			BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[iteam], iteam, iPriority, DID_TEAM_PASS_OBJECTIVE(iteam, iPriority))
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL HAS_TEAM_GOT_PLAYERS_FOR_NEXT_KILL_RULE(INT iRule, INT iTeam)
	
	INT i
	
	IF NOT IS_BIT_SET(MC_serverbd.iServerBitSet, SBBOOL_FIRST_UPDATE_DONE)
		RETURN TRUE
	ENDIF
	
	PRINTLN("[MMacK][NextRule] HAS_TEAM_GOT_PLAYERS_FOR_NEXT_KILL_RULE")
	PRINTLN("[MMacK][NextRule] HAS_TEAM_GOT_PLAYERS_FOR_NEXT_KILL_RULE : iRule = ", iRule)
	
	IF iRule = FMMC_OBJECTIVE_LOGIC_KILL_TEAM0
		PRINTLN("[MMacK][NextRule] HAS_TEAM_GOT_PLAYERS_FOR_NEXT_KILL_RULE : FMMC_OBJECTIVE_LOGIC_KILL_TEAM0")
		IF IS_TEAM_ACTIVE(0)
			RETURN TRUE
		ENDIF
	ELIF iRule = FMMC_OBJECTIVE_LOGIC_KILL_TEAM1
		PRINTLN("[MMacK][NextRule] HAS_TEAM_GOT_PLAYERS_FOR_NEXT_KILL_RULE : FMMC_OBJECTIVE_LOGIC_KILL_TEAM1")
		IF IS_TEAM_ACTIVE(1)
			RETURN TRUE
		ENDIF
	ELIF iRule = FMMC_OBJECTIVE_LOGIC_KILL_TEAM2
		PRINTLN("[MMacK][NextRule] HAS_TEAM_GOT_PLAYERS_FOR_NEXT_KILL_RULE : FMMC_OBJECTIVE_LOGIC_KILL_TEAM2")
		IF IS_TEAM_ACTIVE(2)
			RETURN TRUE
		ENDIF
	ELIF iRule = FMMC_OBJECTIVE_LOGIC_KILL_TEAM3
		PRINTLN("[MMacK][NextRule] HAS_TEAM_GOT_PLAYERS_FOR_NEXT_KILL_RULE : FMMC_OBJECTIVE_LOGIC_KILL_TEAM3")
		IF IS_TEAM_ACTIVE(3)
			RETURN TRUE
		ENDIF
	ELIF iRule = FMMC_OBJECTIVE_LOGIC_KILL_PLAYERS
		FOR i = 0 TO FMMC_MAX_TEAMS-1
			IF i != iTeam
				IF IS_TEAM_ACTIVE(i)
				AND NOT DOES_TEAM_LIKE_TEAM(iTeam, i)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDFOR
	ELSE
		PRINTLN("[MMacK][NextRule] HAS_TEAM_GOT_PLAYERS_FOR_NEXT_KILL_RULE : Not a kill rule, so this is fine - return TRUE")
		RETURN TRUE
	ENDIF
	
	PRINTLN("[MMacK][NextRule] HAS_TEAM_GOT_PLAYERS_FOR_NEXT_KILL_RULE : FALSE")
	RETURN FALSE
	
ENDFUNC

///PURPOSE: Set that iThisTeam should watch iCutsceneTeam's cutscene
///    
///PARAMETERS:
///    	* iThisTeam - The team that wants to watch the cutscene
///     * iCutsceneTeam - The team that owns the cutscene
///    
PROC SET_THIS_TEAM_SHOULD_WATCH_OTHER_TEAM_CUTSCENE( INT iThisTeam, INT iCutsceneTeam )
	
	IF iThisTeam < 0 OR iThisTeam > 3
		CASSERTLN( DEBUG_CONTROLLER, "[RCC MISSION] SET_THIS_TEAM_SHOULD_WATCH_OTHER_TEAM_CUTSCENE - iThisTeam is invalid - something has gone terribly wrong" )
	ENDIF
	
	IF iCutsceneTeam < 0 OR iCutsceneTeam > 3
		CASSERTLN( DEBUG_CONTROLLER, "[RCC MISSION] SET_THIS_TEAM_SHOULD_WATCH_OTHER_TEAM_CUTSCENE - iCutsceneTeam is invalid - something has gone terribly wrong" )
	ENDIF
	
	SWITCH iCutsceneTeam
		CASE 0
			SET_BIT( MC_serverBD.iNumOtherTeamsConsideredForTeamCutscene, ciTEAM0_CUTSCENE_SEEN_BY_TEAM0 + iThisTeam )	
		BREAK
		CASE 1
			SET_BIT( MC_serverBD.iNumOtherTeamsConsideredForTeamCutscene, ciTEAM1_CUTSCENE_SEEN_BY_TEAM0 + iThisTeam )	
		BREAK
		CASE 2
			SET_BIT( MC_serverBD.iNumOtherTeamsConsideredForTeamCutscene, ciTEAM2_CUTSCENE_SEEN_BY_TEAM0 + iThisTeam )	
		BREAK
		CASE 3
			SET_BIT( MC_serverBD.iNumOtherTeamsConsideredForTeamCutscene, ciTEAM3_CUTSCENE_SEEN_BY_TEAM0 + iThisTeam )	
		BREAK
	ENDSWITCH
	
	CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Setting team ", iThisTeam, " should watch team ", iCutsceneTeam,"'s cutscene." )
	CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] MC_serverBD.iNumOtherTeamsConsideredForTeamCutscene is now ", MC_serverBD.iNumOtherTeamsConsideredForTeamCutscene )
ENDPROC

///PURPOSE: This function returns true if the inputted iTeam should be in the inputted iCutscene
FUNC BOOL SHOULD_TEAM_BE_IN_CUTSCENE(FMMC_CUTSCENE_TYPE cutType, INT icutscene, INT iteam, INT iCutsceneTeam)
	PRINTLN("[RCC MISSION] SHOULD_TEAM_BE_IN_CUTSCENE icutscene = ", icutscene)	
	PRINTLN("[RCC MISSION] SHOULD_TEAM_BE_IN_CUTSCENE cutType = ", cutType)	
	PRINTLN("[RCC MISSION] SHOULD_TEAM_BE_IN_CUTSCENE iteam = ", iteam)

	INT iBitsetToCheck = -1
	
	IF icutscene = -1
	AND cutType != FMMCCUT_ENDMOCAP
		RETURN FALSE
	ENDIF
			
	IF iteam = iCutsceneTeam
		RETURN TRUE
	ENDIF
	
	SWITCH cutType
	
		CASE FMMCCUT_MOCAP
			iBitsetToCheck = g_FMMC_STRUCT.sMocapCutsceneData[icutscene].iCutsceneBitset
		BREAK
		
		CASE FMMCCUT_ENDMOCAP
			PRINTLN("[RCC MISSION] SHOULD_TEAM_BE_IN_CUTSCENE NOT ON HEIST")
			RETURN TRUE
		BREAK

		CASE FMMCCUT_SCRIPTED
			iBitsetToCheck = g_FMMC_STRUCT.sScriptedCutsceneData[icutscene].iCutsceneBitset
		BREAK

	ENDSWITCH

	SWITCH iteam
		CASE 0
			IF IS_BIT_SET(iBitsetToCheck, ci_CSBS_Pull_Team0_In)
				CPRINTLN(DEBUG_CONTROLLER, "[RCC MISSION] SHOULD_TEAM_BE_IN_CUTSCENE sMocapCutsceneData ci_CSBS_Pull_Team0_In TRUE ")	
				RETURN TRUE
			ENDIF
		BREAK
		CASE 1
			IF IS_BIT_SET(iBitsetToCheck, ci_CSBS_Pull_Team1_In)
				CPRINTLN(DEBUG_CONTROLLER, "[RCC MISSION] SHOULD_TEAM_BE_IN_CUTSCENE sMocapCutsceneData ci_CSBS_Pull_Team1_In TRUE ")
				RETURN TRUE
			ENDIF		
		BREAK
		CASE 2
			IF IS_BIT_SET(iBitsetToCheck, ci_CSBS_Pull_Team2_In)
				CPRINTLN(DEBUG_CONTROLLER, "[RCC MISSION] SHOULD_TEAM_BE_IN_CUTSCENE sMocapCutsceneData ci_CSBS_Pull_Team2_In TRUE ")
				RETURN TRUE
			ENDIF		
		BREAK
		CASE 3
			IF IS_BIT_SET(iBitsetToCheck, ci_CSBS_Pull_Team3_In)
				CPRINTLN(DEBUG_CONTROLLER, "[RCC MISSION] SHOULD_TEAM_BE_IN_CUTSCENE sMocapCutsceneData ci_CSBS_Pull_Team3_In TRUE ")
				RETURN TRUE
			ENDIF		
		BREAK
	ENDSWITCH

	RETURN FALSE
ENDFUNC

///PURPOSE: This function sets all the teams that should watch iCutsceneTeam's cutscene
PROC SET_TEAMS_THAT_WATCH_THIS_TEAMS_CUTSCENE( INT iCutsceneTeam )
	IF MC_serverBD.iCutsceneID[iCutsceneTeam] > -1
	
		CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] SET_TEAMS_THAT_WATCH_THIS_TEAMS_CUTSCENE() iCutsceneID value passed is ",  MC_serverBD.iCutsceneID[ iCutsceneTeam ], " for team " , iCutsceneTeam )
		
		INT iCutsceneIndex = -1
		FMMC_CUTSCENE_TYPE eCutType
		IF MC_serverBD.iCutsceneID[ iCutsceneTeam ] >= FMMC_GET_MAX_SCRIPTED_CUTSCENES() AND MC_serverBD.iCutsceneID[ iCutsceneTeam ] < FMMC_GET_MAX_CUTSCENES_WITH_MOCAP()
			iCutsceneIndex = MC_serverBD.iCutsceneID[ iCutsceneTeam ] - FMMC_GET_MAX_SCRIPTED_CUTSCENES()
			eCutType = FMMCCUT_MOCAP
		ELIF MC_serverBD.iCutsceneID[ iCutsceneTeam ] < FMMC_GET_MAX_SCRIPTED_CUTSCENES()
			iCutsceneIndex = MC_serverBD.iCutsceneID[ iCutsceneTeam ]
			eCutType = FMMCCUT_SCRIPTED
		ENDIF
		
		INT iTeamLoop = 0
		FOR iTeamLoop = 0 TO FMMC_MAX_TEAMS -1

			IF iCutsceneIndex > -1
				IF SHOULD_TEAM_BE_IN_CUTSCENE(eCutType, iCutsceneIndex, iTeamLoop ,iCutsceneTeam)
					SET_THIS_TEAM_SHOULD_WATCH_OTHER_TEAM_CUTSCENE( iTeamLoop, iCutsceneTeam )
				ENDIF
			ENDIF

		ENDFOR
	
	ENDIF
ENDPROC

PROC PROCESS_CUTSCENE_OBJECTIVE_LOGIC(INT iTeam, INT iPlayerRule)
	
	IF iPlayerRule = -1
		PRINTLN("[Cutscenes] - PROCESS_CUTSCENE_OBJECTIVE_LOGIC - iPlayerRule value passed is ",  iPlayerRule, " for team " , iteam, " Exitting.")
		EXIT
	ENDIF
	
	IF HAS_TEAM_FAILED(iteam)
		EXIT
	ENDIF

	IF MC_serverBD.iCutsceneID[iteam] != -1
		PRINTLN("[Cutscenes] - PROCESS_CUTSCENE_OBJECTIVE_LOGIC - iCutsceneID value passed is ",  MC_serverBD.iCutsceneID[iteam], " for team " , iteam, " already set so Exitting.")
		EXIT
	ENDIF
		
	IF MC_serverBD.iPlayerRuleLimit[iPlayerRule][iteam] <= -1
		PRINTLN("[Cutscenes] - PROCESS_CUTSCENE_OBJECTIVE_LOGIC - iPlayerRuleLimit value passed is ",  MC_serverBD.iPlayerRuleLimit[iPlayerRule][iteam], " for team " , iteam, " Exitting.")
		EXIT
	ENDIF
	
	IF MC_serverBD.iPlayerRuleLimit[iPlayerRule][iteam] < FMMC_GET_MAX_SCRIPTED_CUTSCENES()
		IF NOT IS_BIT_SET(MC_serverBD.iCutsceneFinished[iteam], MC_serverBD_4.iPlayerRulePriority[iPlayerRule][iteam])
			INT iScriptedCutscene = MC_serverBD.iPlayerRuleLimit[iPlayerRule][iteam]
			
			IF MC_serverBD.iOverriddenScriptedCutsceneRule[iTeam] > -1
			AND MC_serverBD.iOverriddenScriptedCutsceneIndex[iTeam] > -1
				IF MC_serverBD.iOverriddenScriptedCutsceneRule[iTeam] = MC_serverBD_4.iPlayerRulePriority[iPlayerRule][iTeam]
					iScriptedCutscene = MC_serverBD.iOverriddenScriptedCutsceneIndex[iTeam]
					PRINTLN("[Cutscenes] - PROCESS_CUTSCENE_OBJECTIVE_LOGIC - iScriptedCutscene being overridden from ", MC_serverBD.iPlayerRuleLimit[iPlayerRule][iteam]," to ", iScriptedCutscene, " for team ", iTeam, " on player rule ", iPlayerRule)
				ENDIF
			ENDIF
			
			IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sScriptedCutsceneData[iScriptedCutscene].tlName)
				MC_serverBD.iCutsceneID[iteam] = iScriptedCutscene
				PRINTLN("[Cutscenes] - PROCESS_CUTSCENE_OBJECTIVE_LOGIC - iCutsceneID value passed is ",  MC_serverBD.iCutsceneID[iteam], " for team " , iteam )
				SET_TEAMS_THAT_WATCH_THIS_TEAMS_CUTSCENE( iteam )
				REINIT_NET_TIMER(MC_serverBD.tdCutsceneStartTime[iteam],FALSE,TRUE)
			ENDIF
		ENDIF
	ELIF MC_serverBD.iPlayerRuleLimit[iPlayerRule][iteam] < FMMC_GET_MAX_CUTSCENES_WITH_MOCAP()
		IF NOT IS_BIT_SET(MC_serverBD.iCutsceneFinished[iteam], MC_serverBD_4.iPlayerRulePriority[iPlayerRule][iteam])
			INT iMocap = (MC_serverBD.iPlayerRuleLimit[iPlayerRule][iteam] - FMMC_GET_MAX_SCRIPTED_CUTSCENES())
			IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sMocapCutsceneData[iMocap].tlName)
				MC_serverBD.iCutsceneID[iteam] = MC_serverBD.iPlayerRuleLimit[iPlayerRule][iteam]
				SET_TEAMS_THAT_WATCH_THIS_TEAMS_CUTSCENE( iteam )
				PRINTLN("[Cutscenes] - PROCESS_CUTSCENE_OBJECTIVE_LOGIC - MOCAP iCutsceneID value passed is ",  MC_serverBD.iCutsceneID[iteam], " for team " , iteam )
				REINIT_NET_TIMER(MC_serverBD.tdCutsceneStartTime[iteam],FALSE,TRUE)
			ENDIF
		ENDIF
	ENDIF
		
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Taser Processing
// ##### Description: Processes the Taser System.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PLAY_TASER_PTFX_ON_ENTITY(ENTITY_INDEX eiEntity, INT iIndex, FMMC_TASERED_ENTITY_VARS& sTaserVars)
	
	FLOAT fScale = 1.0
	VECTOR vOffset = <<0,0,0>>
	STRING sEffectName = "scr_ch_finale_fusebox_overload"
	BOOL bLooping = FALSE
	
	IF IS_THIS_MODEL_A_FUSEBOX(GET_ENTITY_MODEL(eiEntity))
		fScale = 1.0
		vOffset = <<0,0,0>>
		sEffectName = "scr_ch_finale_fusebox_overload"
		bLooping = FALSE
		
	ELIF IS_THIS_MODEL_A_CCTV_CAMERA(GET_ENTITY_MODEL(eiEntity))
		fScale = 1.0
		vOffset = <<0, 0.0, 0.0>>
		sEffectName = "scr_ch_finale_camera_stun"
		bLooping = TRUE
		
	ENDIF
	
	PRINTLN("[Taser] Playing Taser effect!")
	USE_PARTICLE_FX_ASSET("scr_ch_finale")
	
	IF bLooping
		IF DOES_PARTICLE_FX_LOOPED_EXIST(sTaserVars.ptLoopingTaserFX[iIndex])
			PRINTLN("[Taser] ptLoopingTaserFX already exists! Exiting now")
			EXIT
		ENDIF
		
		sTaserVars.ptLoopingTaserFX[iIndex] = START_PARTICLE_FX_LOOPED_ON_ENTITY(sEffectName, eiEntity, vOffset, <<0,0,0>>, fScale)
	ELSE
		START_PARTICLE_FX_NON_LOOPED_ON_ENTITY(sEffectName, eiEntity, vOffset, <<0,0,0>>, fScale)
	ENDIF
	
ENDPROC

FUNC BOOL CAN_ENTITY_BE_TASERED(ENTITY_INDEX eiEntity, INT iIndex, FMMC_ELECTRONIC_ENTITY_TYPE eElectronicType)
	
	IF eElectronicType = FMMC_ELECTRONIC_ENTITY_TYPE__OBJECT
		IF SHOULD_DRONE_SYSTEMS_BE_ACTIVE(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iIndex].sObjDroneData)
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_THIS_MODEL_A_FUSEBOX(GET_ENTITY_MODEL(eiEntity))
	OR IS_THIS_MODEL_A_CCTV_CAMERA(GET_ENTITY_MODEL(eiEntity))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_ENTITY_BEEN_HIT_BY_TASER(INT iIndex, FMMC_ELECTRONIC_ENTITY_TYPE eElectronicType)
	SWITCH eElectronicType
		CASE FMMC_ELECTRONIC_ENTITY_TYPE__OBJECT
			RETURN IS_BIT_SET(sObjTaserVars.iHitByTaserBS, iIndex)
			
		CASE FMMC_ELECTRONIC_ENTITY_TYPE__DYNOPROP
			RETURN IS_BIT_SET(sDynoPropTaserVars.iHitByTaserBS, iIndex)
			
	ENDSWITCH
	
	PRINTLN("[Taser] HAS_ENTITY_BEEN_HIT_BY_TASER | INVALID!!")
	ASSERTLN("[Taser] HAS_ENTITY_BEEN_HIT_BY_TASER | INVALID!!")
	RETURN FALSE
ENDFUNC
FUNC INT GET_ENTITY_TASER_TIMER_DIFFERENCE_WITH_CURRENT_TIME(INT iIndex, FMMC_ELECTRONIC_ENTITY_TYPE eElectronicType)
	SWITCH eElectronicType
		CASE FMMC_ELECTRONIC_ENTITY_TYPE__OBJECT
			IF HAS_NET_TIMER_STARTED(sObjTaserVars.stHitByTaserTimers[iIndex])
				RETURN GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(sObjTaserVars.stHitByTaserTimers[iIndex])
			ENDIF
		BREAK
			
		CASE FMMC_ELECTRONIC_ENTITY_TYPE__DYNOPROP
			IF HAS_NET_TIMER_STARTED(sDynoPropTaserVars.stHitByTaserTimers[iIndex])
				RETURN GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(sDynoPropTaserVars.stHitByTaserTimers[iIndex])
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN -1
ENDFUNC

PROC SET_ENTITY_HIT_BY_TASER(INT iIndex, ENTITY_INDEX eiEntity, FMMC_ELECTRONIC_ENTITY_TYPE eElectronicType, BOOL bPlayPTFX = TRUE, BOOL bPermanent = FALSE)
	
	IF NOT CAN_ENTITY_BE_TASERED(eiEntity, iIndex, eElectronicType)
		EXIT
	ENDIF
	
	IF HAS_ENTITY_BEEN_HIT_BY_TASER(iIndex, eElectronicType)
		EXIT
	ENDIF
	
	// Set vars & start timers
	SWITCH eElectronicType
		CASE FMMC_ELECTRONIC_ENTITY_TYPE__OBJECT
		
			IF bPlayPTFX
				PLAY_TASER_PTFX_ON_ENTITY(eiEntity, iIndex, sObjTaserVars)
			ENDIF
						
			SET_BIT(sObjTaserVars.iHitByTaserBS, iIndex)
			IF NOT bPermanent
				REINIT_NET_TIMER(sObjTaserVars.stHitByTaserTimers[iIndex])
			ENDIF
			PRINTLN("[Taser] SET_ENTITY_HIT_BY_TASER | Obj ", iIndex, " has been hit!")
			
		BREAK
			
		CASE FMMC_ELECTRONIC_ENTITY_TYPE__DYNOPROP
		
			IF bPlayPTFX
				PLAY_TASER_PTFX_ON_ENTITY(eiEntity, iIndex, sDynoPropTaserVars)
			ENDIF
			
			SET_BIT(sDynoPropTaserVars.iHitByTaserBS, iIndex)
			IF NOT bPermanent
				REINIT_NET_TIMER(sDynoPropTaserVars.stHitByTaserTimers[iIndex])
			ENDIF
			PRINTLN("[Taser] SET_ENTITY_HIT_BY_TASER | Dynoprop ", iIndex, " has been hit!")
			
		BREAK
	ENDSWITCH
	
	IF IS_THIS_MODEL_A_FUSEBOX(GET_ENTITY_MODEL(eiEntity))
		iFuseBoxesCurrentlyTasered++
		PLAY_SOUND_FROM_COORD(-1, "Security_Box_Offline_Tazer", GET_ENTITY_COORDS(eiEntity), "dlc_ch_heist_finale_security_alarms_sounds", TRUE, 800)
		PRINTLN("[Taser] SET_ENTITY_HIT_BY_TASER | iFuseBoxesCurrentlyTasered is now ", iFuseBoxesCurrentlyTasered)
		
	ELIF IS_THIS_MODEL_A_CCTV_CAMERA(GET_ENTITY_MODEL(eiEntity))
		iCCTVCamsCurrentlyTasered++
		PLAY_SOUND_FROM_COORD(-1, "Camera_Offline", GET_ENTITY_COORDS(eiEntity), "dlc_ch_heist_finale_security_alarms_sounds", TRUE, 800)
		PRINTLN("[Taser] SET_ENTITY_HIT_BY_TASER | iCCTVCamsCurrentlyTasered is now ", iCCTVCamsCurrentlyTasered)
		
	ENDIF
ENDPROC

PROC CLEAR_ENTITY_HIT_BY_TASER(INT iIndex, FMMC_ELECTRONIC_ENTITY_TYPE eElectronicType)

	MODEL_NAMES mnEntityModel = DUMMY_MODEL_FOR_SCRIPT
	VECTOR vPosition
	
	SWITCH eElectronicType
		CASE FMMC_ELECTRONIC_ENTITY_TYPE__OBJECT
		
			IF DOES_PARTICLE_FX_LOOPED_EXIST(sObjTaserVars.ptLoopingTaserFX[iIndex])
				STOP_PARTICLE_FX_LOOPED(sObjTaserVars.ptLoopingTaserFX[iIndex], TRUE)
			ENDIF
			
			mnEntityModel = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iIndex].mn
			vPosition = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iIndex].vPos
			CLEAR_BIT(sObjTaserVars.iHitByTaserBS, iIndex)
			PRINTLN("[Taser] CLEAR_ENTITY_HIT_BY_TASER | Obj ", iIndex, " taser bit has been cleared!")
		BREAK
			
		CASE FMMC_ELECTRONIC_ENTITY_TYPE__DYNOPROP
		
			IF DOES_PARTICLE_FX_LOOPED_EXIST(sDynoPropTaserVars.ptLoopingTaserFX[iIndex])
				STOP_PARTICLE_FX_LOOPED(sDynoPropTaserVars.ptLoopingTaserFX[iIndex], TRUE)
			ENDIF
			
			mnEntityModel = g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iIndex].mn
			vPosition = g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iIndex].vPos
			CLEAR_BIT(sDynoPropTaserVars.iHitByTaserBS, iIndex)
			PRINTLN("[Taser] CLEAR_ENTITY_HIT_BY_TASER | Dynoprop ", iIndex, " taser bit has been cleared!")
		BREAK
			
	ENDSWITCH
		
	IF IS_THIS_MODEL_A_FUSEBOX(mnEntityModel)
		iFuseBoxesCurrentlyTasered--
		PLAY_SOUND_FROM_COORD(-1, "Security_Box_Online", vPosition, "dlc_ch_heist_finale_security_alarms_sounds", TRUE, 800)
		PRINTLN("[Taser] CLEAR_ENTITY_HIT_BY_TASER | iFuseBoxesCurrentlyTasered is now ", iFuseBoxesCurrentlyTasered)
		
	ELIF IS_THIS_MODEL_A_CCTV_CAMERA(mnEntityModel)
		iCCTVCamsCurrentlyTasered--
		PLAY_SOUND_FROM_COORD(-1, "Camera_Online", vPosition, "dlc_ch_heist_finale_security_alarms_sounds", TRUE, 800)
		PRINTLN("[Taser] CLEAR_ENTITY_HIT_BY_TASER | iCCTVCamsCurrentlyTasered is now ", iCCTVCamsCurrentlyTasered)
		
	ENDIF
ENDPROC

PROC SET_ENTITY_HAS_REACTED_TO_TASER(INT iIndex, FMMC_ELECTRONIC_ENTITY_TYPE eElectronicType)
	SWITCH eElectronicType
		CASE FMMC_ELECTRONIC_ENTITY_TYPE__OBJECT
			SET_BIT(sObjTaserVars.iReactedToTaserBS, iIndex)
		BREAK
		
		CASE FMMC_ELECTRONIC_ENTITY_TYPE__DYNOPROP
			SET_BIT(sDynoPropTaserVars.iReactedToTaserBS, iIndex)
		BREAK
		
	ENDSWITCH
ENDPROC

PROC CLEAR_ENTITY_HAS_REACTED_TO_TASER(INT iIndex, FMMC_ELECTRONIC_ENTITY_TYPE eElectronicType)
	SWITCH eElectronicType
		CASE FMMC_ELECTRONIC_ENTITY_TYPE__OBJECT
			CLEAR_BIT(sObjTaserVars.iReactedToTaserBS, iIndex)
		BREAK
		
		CASE FMMC_ELECTRONIC_ENTITY_TYPE__DYNOPROP
			CLEAR_BIT(sDynoPropTaserVars.iReactedToTaserBS, iIndex)
		BREAK
		
	ENDSWITCH
ENDPROC

FUNC BOOL HAS_ENTITY_REACTED_TO_TASER(INT iIndex, FMMC_ELECTRONIC_ENTITY_TYPE eElectronicType)
	SWITCH eElectronicType
		CASE FMMC_ELECTRONIC_ENTITY_TYPE__OBJECT
			RETURN IS_BIT_SET(sObjTaserVars.iReactedToTaserBS, iIndex)
			
		CASE FMMC_ELECTRONIC_ENTITY_TYPE__DYNOPROP
			RETURN IS_BIT_SET(sDynoPropTaserVars.iReactedToTaserBS, iIndex)
	ENDSWITCH
	
	PRINTLN("[Taser] HAS_ENTITY_REACTED_TO_TASER | INVALID!!")
	ASSERTLN("[Taser] HAS_ENTITY_REACTED_TO_TASER | INVALID!!")
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_ENTITY_TASER_TIMER_EXPIRED(INT iIndex, FMMC_ELECTRONIC_ENTITY_TYPE eElectronicType)
	SWITCH eElectronicType
		CASE FMMC_ELECTRONIC_ENTITY_TYPE__OBJECT
			RETURN HAS_NET_TIMER_EXPIRED(sObjTaserVars.stHitByTaserTimers[iIndex], g_FMMC_STRUCT_ENTITIES.sPlacedObject[iIndex].sObjElectronicData.iTaserDisableTime)
			
		CASE FMMC_ELECTRONIC_ENTITY_TYPE__DYNOPROP
			RETURN HAS_NET_TIMER_EXPIRED(sDynoPropTaserVars.stHitByTaserTimers[iIndex], g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iIndex].sDynoPropElectronicData.iTaserDisableTime)
	ENDSWITCH
	
	PRINTLN("[Taser] HAS_ENTITY_TASER_TIMER_EXPIRED | INVALID!!")
	ASSERTLN("[Taser] HAS_ENTITY_TASER_TIMER_EXPIRED | INVALID!!")
	RETURN FALSE
ENDFUNC

PROC PROCESS_TASERED_ENTITY(FMMC_TASERED_ENTITY_VARS& sTaserVars, FMMC_ELECTRONIC_DATA& sElectronicData, ELECTRONIC_PARAMS& sElectronicParams)
	
	UNUSED_PARAMETER(sElectronicData) // Only used for debug
	
	IF HAS_NET_TIMER_STARTED(sTaserVars.stHitByTaserTimers[sElectronicParams.iIndex])
		IF HAS_ENTITY_TASER_TIMER_EXPIRED(sElectronicParams.iIndex, sElectronicParams.eEntityType)
			CLEAR_ENTITY_HIT_BY_TASER(sElectronicParams.iIndex, sElectronicParams.eEntityType)
			RESET_NET_TIMER(sTaserVars.stHitByTaserTimers[sElectronicParams.iIndex])
			PRINTLN("[Taser] Taser timer has expired for entity ", sElectronicParams.iIndex)
		ELSE
			#IF IS_DEBUG_BUILD
			IF bObjectTaserDebug
				TEXT_LABEL_63 sTaserDebug = "TASED | "
				INT iTimeLeft = (sElectronicData.iTaserDisableTime - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(sTaserVars.stHitByTaserTimers[sElectronicParams.iIndex]))
				sTaserDebug += (iTimeLeft / 1000)
				DRAW_DEBUG_TEXT_ABOVE_ENTITY(sElectronicParams.oiElectronic, sTaserDebug, 0.1, 255, 255, 255)
			ENDIF
			#ENDIF
		ENDIF
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: DIRT
// ##### Description: Filth 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

//FMMC2020 - put this back in locates once the below if sorted
// Reset the locate_fade_out_data state back to before having faded in
PROC RESET_LOCATE(  INT iloc,
					locate_fade_out_data& toDisplay,
				   	INT iDebugTeam = -1)
	toDisplay.eState = eLocateFadeOutState_BEFORE_FADE_IN
	toDisplay.iAlphaValue = -1
	
	IF IS_BIT_SET(iCheckpointBS, iloc)
		DELETE_CHECKPOINT(ciLocCheckpoint[iloc])
		CLEAR_BIT(iCheckpointBS, iloc)
	ENDIF
	
	IF iloc > -1 AND iDebugTeam > -1
		CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] Locate ", iloc, " for team ", iDebugTeam, " has been reset." )
	ENDIF
ENDPROC


/// PURPOSE: Handles client-side logic that needs to occur when we're re-activating old objectives
///    
/// PARAMS:
///    iTeam - The team we're moving back
///    iLocResetBS - A bitset of locations that need their coronas resetting for that team
PROC PROCESS_CLIENT_OBJECTIVE_REVALIDATION(INT iTeam, INT iLocResetBS)
	
	INT i
	
	IF MC_playerBD[iPartToUse].iteam = iTeam
		
		IF MC_serverBD.iNumLocCreated > 0
			
			FOR i = 0 TO (MC_serverBD.iNumLocCreated - 1)
				IF IS_BIT_SET(iLocResetBS, i)
					PRINTLN("[RCC MISSION] PROCESS_CLIENT_OBJECTIVE_REVALIDATION, calling RESET_LOCATE on locate ",i)
					RESET_LOCATE(i, sFadeOutLocations[i], iTeam)
				ENDIF
			ENDFOR
			
		ENDIF
		
	ENDIF
	
ENDPROC

PROC CLEAR_COMPLETED_OBJECTIVE_DATA()
	INT iBitsetArrayBlank[1]
	IF IS_BIT_SET(MC_serverBD.iProcessJobCompBitset,iPartToUse)
		IF MC_playerBD[iPartToUse].iObjectiveTypeCompleted =JOB_COMP_ARRIVE_LOC
			MC_playerBD[iPartToUse].iCurrentLoc = -1
		ENDIF
		IF MC_playerBD[iPartToUse].iObjectiveTypeCompleted = JOB_COMP_DELIVER_PED
		OR MC_playerBD[iPartToUse].iObjectiveTypeCompleted =JOB_COMP_ARRIVE_PED
			iPedDeliveredBitset[0] = 0
			iPedDeliveredBitset[1] = 0
			iPedDeliveredBitset[2] = 0
			MC_playerBD[iPartToUse].iPedNear = -1
			iBroadcastPedNear = -1
		ENDIF
		IF MC_playerBD[iPartToUse].iObjectiveTypeCompleted =JOB_COMP_ARRIVE_VEH
			MC_playerBD[iPartToUse].iVehNear = -1
			iBroadcastVehNear = -1
		ENDIF
		IF MC_playerBD[iPartToUse].iObjectiveTypeCompleted = JOB_COMP_DELIVER_VEH
			CLEAR_BIT(MC_playerBD[ iPartToUse ].iClientBitSet2, PBBOOL2_DROPPING_OFF )
			MC_playerBD[iPartToUse].iVehDeliveryId = -1
		ENDIF
		IF MC_playerBD[iPartToUse].iObjectiveTypeCompleted = JOB_COMP_DELIVER_OBJ
		OR MC_playerBD[iPartToUse].iObjectiveTypeCompleted = JOB_COMP_ARRIVE_OBJ
			iObjDeliveredBitset = 0
			SET_OBJ_NEAR(-1)
		ENDIF
		IF MC_playerBD[iPartToUse].iObjectiveTypeCompleted = JOB_COMP_PHOTO_LOC
			MC_playerBD[iPartToUse].iLocPhoto = -1
		ENDIF
		IF MC_playerBD[iPartToUse].iObjectiveTypeCompleted = JOB_COMP_PHOTO_PED
			MC_playerBD[iPartToUse].iPedPhoto = -1
		ENDIF
		IF MC_playerBD[iPartToUse].iObjectiveTypeCompleted = JOB_COMP_PHOTO_VEH
			MC_playerBD[iPartToUse].iVehPhoto = -1
		ENDIF
		IF MC_playerBD[iPartToUse].iObjectiveTypeCompleted = JOB_COMP_PHOTO_OBJ
			MC_playerBD[iPartToUse].iObjPhoto = -1
		ENDIF
		IF MC_playerBD[iPartToUse].iObjectiveTypeCompleted = JOB_COMP_HACK_OBJ	
			MC_playerBD[iPartToUse].iObjHacked = -1
		ENDIF
		MC_playerBD[iPartToUse].iObjectiveTypeCompleted = -1
		iBroadcastPriority = -1
		iBroadcastRevalidate = -1
		SET_BIT(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_CLR_SRV_OBJ_BIT)
		IF HAS_NET_TIMER_STARTED(tdeventsafetytimer)
			RESET_NET_TIMER(tdeventsafetytimer)
		ENDIF
		PRINTLN("[RCC MISSION] CLEAR_COMPLETED_OBJECTIVE_DATA - iProcessJobCompBitset is set")
	ELSE
		IF HAS_NET_TIMER_STARTED(tdeventsafetytimer)
			IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdeventsafetytimer) > 3000 // Lowered from 5000
				
				INT iJobComp = MC_playerBD[iPartToUse].iObjectiveTypeCompleted
				BOOL bFailure
				
				SWITCH iJobComp
					CASE JOB_COMP_DELIVER_PED
						BROADCAST_FMMC_OBJECTIVE_COMPLETE(iJobComp,iPartToUse,-1,iBroadcastPriority, iBroadcastRevalidate, iPedDeliveredBitset)
					BREAK
					CASE JOB_COMP_DELIVER_VEH
						IF MC_playerBD[iPartToUse].iVehDeliveryId != -1
							BROADCAST_FMMC_OBJECTIVE_COMPLETE(iJobComp,iPartToUse,MC_playerBD[iPartToUse].iVehDeliveryId,iBroadcastPriority, iBroadcastRevalidate, iBitsetArrayBlank)
						ELSE
							CASSERTLN(DEBUG_CONTROLLER,"[RCC MISSION] CLEAR_COMPLETED_OBJECTIVE_DATA - Trying to resend JOB_COMP_DELIVER_VEH, but iVehDeliveryID is -1")
							PRINTLN("[RCC MISSION] CLEAR_COMPLETED_OBJECTIVE_DATA - Trying to resend JOB_COMP_DELIVER_VEH, but iVehDeliveryID is -1")
								CLEAR_BIT(MC_playerBD[ iPartToUse ].iClientBitSet2, PBBOOL2_IN_DROP_OFF)
								CLEAR_BIT(MC_playerBD[ iPartToUse ].iClientBitSet2, PBBOOL2_DROPPING_OFF )
							bFailure = TRUE
						ENDIF
					BREAK
					CASE JOB_COMP_DELIVER_OBJ
						BROADCAST_FMMC_OBJECTIVE_COMPLETE(iJobComp,iPartToUse,iObjDeliveredBitset,iBroadcastPriority, iBroadcastRevalidate, iBitsetArrayBlank)
					BREAK
					CASE JOB_COMP_ARRIVE_LOC
						IF MC_playerBD[iPartToUse].iCurrentLoc != -1
							BROADCAST_FMMC_OBJECTIVE_COMPLETE(iJobComp,iPartToUse,MC_playerBD[iPartToUse].iCurrentLoc,iBroadcastPriority, iBroadcastRevalidate, iBitsetArrayBlank)
						ELSE
							CASSERTLN(DEBUG_CONTROLLER,"[RCC MISSION] CLEAR_COMPLETED_OBJECTIVE_DATA - Trying to resend JOB_COMP_ARRIVE_LOC, but iCurrentLoc is -1")
							PRINTLN("[RCC MISSION] CLEAR_COMPLETED_OBJECTIVE_DATA - Trying to resend JOB_COMP_ARRIVE_LOC, but iCurrentLoc is -1")
							bFailure = TRUE
						ENDIF
					BREAK
					CASE JOB_COMP_ARRIVE_PED
						IF iBroadcastPedNear != -1
							BROADCAST_FMMC_OBJECTIVE_COMPLETE(iJobComp, iPartToUse, iBroadcastPedNear, iBroadcastPriority, iBroadcastRevalidate, iBitsetArrayBlank)
						ELSE
							CASSERTLN(DEBUG_CONTROLLER,"[RCC MISSION] CLEAR_COMPLETED_OBJECTIVE_DATA - Trying to resend JOB_COMP_ARRIVE_PED, but iBroadcastPedNear is -1")
							PRINTLN("[RCC MISSION] CLEAR_COMPLETED_OBJECTIVE_DATA - Trying to resend JOB_COMP_ARRIVE_PED, but iBroadcastPedNear is -1")
							iPedDeliveredBitset[0] = 0
							iPedDeliveredBitset[1] = 0
							iPedDeliveredBitset[2] = 0
							bFailure = TRUE
						ENDIF
					BREAK
					CASE JOB_COMP_ARRIVE_VEH
						IF iBroadcastVehNear != -1
							BROADCAST_FMMC_OBJECTIVE_COMPLETE(iJobComp, iPartToUse, iBroadcastVehNear, iBroadcastPriority, iBroadcastRevalidate, iBitsetArrayBlank)
						ELSE
							CASSERTLN(DEBUG_CONTROLLER,"[RCC MISSION] CLEAR_COMPLETED_OBJECTIVE_DATA - Trying to resend JOB_COMP_ARRIVE_VEH, but iBroadcastVehNear is -1")
							PRINTLN("[RCC MISSION] CLEAR_COMPLETED_OBJECTIVE_DATA - Trying to resend JOB_COMP_ARRIVE_VEH, but iBroadcastVehNear is -1")
							bFailure = TRUE
						ENDIF
					BREAK
					CASE JOB_COMP_ARRIVE_OBJ
						IF MC_playerBD[iPartToUse].iObjNear != -1
							BROADCAST_FMMC_OBJECTIVE_COMPLETE(iJobComp,iPartToUse,MC_playerBD[iPartToUse].iObjNear,iBroadcastPriority, iBroadcastRevalidate, iBitsetArrayBlank)
						ELSE
							CASSERTLN(DEBUG_CONTROLLER,"[RCC MISSION] CLEAR_COMPLETED_OBJECTIVE_DATA - Trying to resend JOB_COMP_ARRIVE_OBJ, but iObjNear is -1")
							PRINTLN("[RCC MISSION] CLEAR_COMPLETED_OBJECTIVE_DATA - Trying to resend JOB_COMP_ARRIVE_OBJ, but iObjNear is -1")
							iObjDeliveredBitset = 0
							bFailure = TRUE
						ENDIF
					BREAK
					CASE JOB_COMP_PHOTO_LOC
						IF MC_playerBD[iPartToUse].iLocPhoto != -1
							BROADCAST_FMMC_OBJECTIVE_COMPLETE(iJobComp,iPartToUse,MC_playerBD[iPartToUse].iLocPhoto,iBroadcastPriority, iBroadcastRevalidate, iBitsetArrayBlank)
						ELSE
							CASSERTLN(DEBUG_CONTROLLER,"[RCC MISSION] CLEAR_COMPLETED_OBJECTIVE_DATA - Trying to resend JOB_COMP_PHOTO_LOC, but iLocPhoto is -1")
							PRINTLN("[RCC MISSION] CLEAR_COMPLETED_OBJECTIVE_DATA - Trying to resend JOB_COMP_PHOTO_LOC, but iLocPhoto is -1")
							bFailure = TRUE
						ENDIF
					BREAK
					CASE JOB_COMP_PHOTO_PED
						IF MC_playerBD[iPartToUse].iPedPhoto != -1
							BROADCAST_FMMC_OBJECTIVE_COMPLETE(iJobComp,iPartToUse,MC_playerBD[iPartToUse].iPedPhoto,iBroadcastPriority, iBroadcastRevalidate, iBitsetArrayBlank)
						ELSE
							CASSERTLN(DEBUG_CONTROLLER,"[RCC MISSION] CLEAR_COMPLETED_OBJECTIVE_DATA - Trying to resend JOB_COMP_PHOTO_PED, but iPedPhoto is -1")
							PRINTLN("[RCC MISSION] CLEAR_COMPLETED_OBJECTIVE_DATA - Trying to resend JOB_COMP_PHOTO_PED, but iPedPhoto is -1")
							bFailure = TRUE
						ENDIF
					BREAK
					CASE JOB_COMP_PHOTO_VEH
						IF MC_playerBD[iPartToUse].iVehPhoto != -1
							BROADCAST_FMMC_OBJECTIVE_COMPLETE(iJobComp,iPartToUse,MC_playerBD[iPartToUse].iVehPhoto,iBroadcastPriority, iBroadcastRevalidate, iBitsetArrayBlank)
						ELSE
							CASSERTLN(DEBUG_CONTROLLER,"[RCC MISSION] CLEAR_COMPLETED_OBJECTIVE_DATA - Trying to resend JOB_COMP_PHOTO_VEH, but iVehPhoto is -1")
							PRINTLN("[RCC MISSION] CLEAR_COMPLETED_OBJECTIVE_DATA - Trying to resend JOB_COMP_PHOTO_VEH, but iVehPhoto is -1")
							bFailure = TRUE
						ENDIF
					BREAK
					CASE JOB_COMP_PHOTO_OBJ
						IF MC_playerBD[iPartToUse].iObjPhoto != -1
							BROADCAST_FMMC_OBJECTIVE_COMPLETE(iJobComp,iPartToUse,MC_playerBD[iPartToUse].iObjPhoto,iBroadcastPriority, iBroadcastRevalidate, iBitsetArrayBlank)
						ELSE
							CASSERTLN(DEBUG_CONTROLLER,"[RCC MISSION] CLEAR_COMPLETED_OBJECTIVE_DATA - Trying to resend JOB_COMP_PHOTO_OBJ, but iObjPhoto is -1")
							PRINTLN("[RCC MISSION] CLEAR_COMPLETED_OBJECTIVE_DATA - Trying to resend JOB_COMP_PHOTO_OBJ, but iObjPhoto is -1")
							bFailure = TRUE
						ENDIF
					BREAK
					CASE JOB_COMP_HACK_OBJ
						IF MC_playerBD[iPartToUse].iObjHacked != -1
							BROADCAST_FMMC_OBJECTIVE_COMPLETE(iJobComp,iPartToUse,MC_playerBD[iPartToUse].iObjHacked,iBroadcastPriority, iBroadcastRevalidate, iBitsetArrayBlank)
						ELSE
							CASSERTLN(DEBUG_CONTROLLER,"[RCC MISSION] CLEAR_COMPLETED_OBJECTIVE_DATA - Trying to resend JOB_COMP_HACK_OBJ, but iObjHacked is -1")
							PRINTLN("[RCC MISSION] CLEAR_COMPLETED_OBJECTIVE_DATA - Trying to resend JOB_COMP_HACK_OBJ, but iObjHacked is -1")
							bFailure = TRUE
						ENDIF
					BREAK
				ENDSWITCH
				
				IF NOT bFailure
					PRINTLN("[RCC MISSION] CLEAR_COMPLETED_OBJECTIVE_DATA - resending safety event since server not responded for 3 seconds, objective type: ",iJobComp,", priority: ",iBroadcastPriority,", revalidates: ",iBroadcastRevalidate," - my team (",MC_playerBD[iPartToUse].iTeam,") has current priority ",MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam])
					REINIT_NET_TIMER(tdeventsafetytimer)
				ELSE
					PRINTLN("[RCC MISSION] CLEAR_COMPLETED_OBJECTIVE_DATA - failed to resend safety event! clearing data...")
					RESET_NET_TIMER(tdEventSafetyTimer)
					MC_playerBD[iPartToUse].iObjectiveTypeCompleted = -1
					iBroadcastPriority = -1
					iBroadcastRevalidate = -1
					CLEAR_BIT(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_CLR_SRV_OBJ_BIT)
				ENDIF
			#IF IS_DEBUG_BUILD
			ELSE
				PRINTLN("[RCC MISSION] CLEAR_COMPLETED_OBJECTIVE_DATA - no response from server (been waiting for ",GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdeventsafetytimer),"ms, objective type ",MC_playerBD[iPartToUse].iObjectiveTypeCompleted,")")
			#ENDIF
			ENDIF
		#IF IS_DEBUG_BUILD
		ELSE
			IF MC_playerBD[iPartToUse].iObjectiveTypeCompleted != -1
				PRINTLN("[RCC MISSION] CLEAR_COMPLETED_OBJECTIVE_DATA - no response from server and tdeventsafetytimer hasn't started! MC_playerBD[iPartToUse].iObjectiveTypeCompleted = ",MC_playerBD[iPartToUse].iObjectiveTypeCompleted)
			ENDIF
		#ENDIF
		ENDIF
		
		IF IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_CLR_SRV_OBJ_BIT)
			PRINTLN("[RCC MISSION] CLEAR_COMPLETED_OBJECTIVE_DATA - MC_serverBD.iProcessJobCompBitset is no longer set, clear PBBOOL_CLR_SRV_OBJ_BIT")
			CLEAR_BIT(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_CLR_SRV_OBJ_BIT)
		ENDIF
		
	ENDIF
	
ENDPROC

//FMMC2020 - put this back in vehicles once the below if sorted
FUNC BOOL AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH(INT iPartOverride = -1)
	
	IF iPartOverride = -1
		iPartOverride = iPartToUse
	ENDIF

	#IF IS_DEBUG_BUILD
		IF bInVehicleDebug
			PRINTLN("[RCC MISSION] AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH Called with callstack. MC_playerBD[iPartToUse].iVehNear =  ", MC_playerBD[iPartToUse].iVehNear)
			DEBUG_PRINTCALLSTACK()
		ENDIF
	#ENDIF
	
	IF MC_playerBD[iPartOverride].iVehNear = -1 //Not in a vehicle yet	
		#IF IS_DEBUG_BUILD
			IF bInVehicleDebug
				PRINTLN("[RCC MISSION] AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH Called. False 1  ")
			ENDIF
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	BOOL bCheckOnRule = TRUE
	INT iTeamsToCheck = GET_VEHICLE_GOTO_TEAMS(MC_playerBD[iPartOverride].iteam, MC_playerBD[iPartOverride].iVehNear, bCheckOnRule)
	
	#IF IS_DEBUG_BUILD
		IF bInVehicleDebug
			PRINTLN("[RCC MISSION] AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH, veh ",MC_playerBD[iPartOverride].iVehNear," - iTeamsToCheck bitset = ",iTeamsToCheck,", bCheckOnRule ",bCheckOnRule)
		ENDIF
	#ENDIF
	
	IF iTeamsToCheck = 0 //Only a single player goto
		#IF IS_DEBUG_BUILD
			IF bInVehicleDebug
				PRINTLN("[RCC MISSION] AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH Called. False 2 ")
			ENDIF
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	INT iTeamLoop, iFreeSeats, iPlayersInVeh, iTotalPlayers
	
	//is everyone in a vehicle
	//or are all the seats taken
	
	FOR iTeamLoop = 0 TO (MC_serverBD.iNumberOfTeams - 1)
		IF IS_BIT_SET(iTeamsToCheck,iTeamLoop)
			IF (NOT bCheckOnRule)
			OR (MC_serverBD.iNumVehHighestPriority[iTeamLoop] > 0
				AND ((MC_serverBD_4.iVehMissionLogic[iTeamLoop] = FMMC_OBJECTIVE_LOGIC_GO_TO)
					 OR (MC_serverBD_4.iVehMissionLogic[iTeamLoop] = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER)))
				iPlayersInVeh += MC_serverBD.iTeamVehNumPlayers[iTeamLoop]
				iTotalPlayers += MC_serverBD.iNumberOfPlayingPlayers[iTeamLoop]
				iFreeSeats += MC_serverBD.iNumVehSeatsHighestPriority[iTeamLoop] - MC_serverBD.iTeamVehNumPlayers[iTeamLoop]
			ENDIF
		ENDIF
	ENDFOR
	
	IF MC_serverBD.iTeamUniqueVehHeldNum[MC_playerBD[iPartOverride].iTeam] > 1
	AND MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iPartOverride].iteam] < FMMC_MAX_RULES
	AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartOverride].iTeam].iRuleBitsetEleven[MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iPartOverride].iteam]], ciBS_RULE11_FORCE_SAME_VEHICLE_FOR_MID_POINT)
		#IF IS_DEBUG_BUILD
			IF bInVehicleDebug
				PRINTLN("[RCC MISSION] AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH, = TRUE because MC_serverBD.iTeamUniqueVehHeldNum[MC_playerBD[iPartOverride].iTeam] = ", MC_serverBD.iTeamUniqueVehHeldNum[MC_playerBD[iPartOverride].iTeam])
			ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF iFreeSeats = 0
		#IF IS_DEBUG_BUILD
			IF bInVehicleDebug
				PRINTLN("[RCC MISSION] AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH, veh ",MC_playerBD[iPartOverride].iVehNear," - Returning FALSE as iFreeSeats = 0 - iTeamsToCheck bitset = ",iTeamsToCheck,", bCheckOnRule ",bCheckOnRule)
			ENDIF
		#ENDIF
		RETURN FALSE
	ELIF (iPlayersInVeh >= iTotalPlayers)
		#IF IS_DEBUG_BUILD
			IF bInVehicleDebug
				PRINTLN("[RCC MISSION] AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH, veh ",MC_playerBD[iPartOverride].iVehNear," - Returning FALSE as iPlayersInVeh (",iPlayersInVeh,") >= iTotalPlayers (",iTotalPlayers,") - iTeamsToCheck bitset = ",iTeamsToCheck,", bCheckOnRule ",bCheckOnRule)
			ENDIF
		#ENDIF
		RETURN FALSE

	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF bInVehicleDebug
			PRINTLN("[RCC MISSION] AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH Called. returning True. iPlayersInVeh = ", iPlayersInVeh, " iTotalPlayers = ", iTotalPlayers, " iFreeSeats = ", iFreeSeats)  
		ENDIF
	#ENDIF
	
	RETURN TRUE
	
ENDFUNC

//FMMC2020 - put this back in vehicles once the below if sorted
FUNC BOOL FMMC_FORCE_EVERYONE_FROM_VEHICLE(INT iveh)
	
	IF SHOULD_STAY_IN_ON_DELIVERY() // force doesnt need to happen stay in the car and progress as if it has forced them out
		RETURN TRUE
	ENDIF
	
	IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
	AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitsetTwo[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE2_ONLY_STAY_IN_ON_LAST_DELIVERY)
		
		IF iveh != -1
		AND iDeliveryVehForcingOut = -1
			BROADCAST_FMMC_FORCING_EVERYONE_FROM_VEH(iveh, iLocalPart)
			iDeliveryVehForcingOut = iveh
			iPartSending_DeliveryVehForcingOut = iLocalPart
		ENDIF
		
		//Don't need to wait for everyone to be out, but need to keep calling force_everyone_etc
		// so it cleans up properly (that's what LBOOL5_KEEP_FORCE_EVERYONE_FROM_MY_CAR does)
		FORCE_EVERYONE_FROM_MY_CAR(TRUE,TRUE)
		MPGlobalsAmbience.g_bForceFromVehicle = TRUE
		SET_BIT(iLocalBoolCheck5, LBOOL5_KEEP_FORCE_EVERYONE_FROM_MY_CAR)
		RETURN TRUE
	ELSE
		
		IF iveh != -1
		AND iDeliveryVehForcingOut = -1
			BROADCAST_FMMC_FORCING_EVERYONE_FROM_VEH(iveh, iLocalPart)
			iDeliveryVehForcingOut = iveh
			iPartSending_DeliveryVehForcingOut = iLocalPart
			MPGlobalsAmbience.g_bForceFromVehicle = TRUE
		ENDIF
		
		IF FORCE_EVERYONE_FROM_MY_CAR(TRUE,FALSE)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

//FMMC2020 - put this back in locates once the below if sorted
FUNC BOOL DOES_THIS_LOCATION_HAVE_A_SPECIFIC_ENTRY_HEADING( INT iLoc )
	BOOL bReturn = FALSE
	
	IF ( IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ iLoc ].iLocBS, ciLoc_BS_DirectionFailObjective )
	OR IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ iLoc ].iLocBS, ciLoc_BS_DirectionFailMission )
	OR IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ iLoc ].iLocBS2, ciLoc_BS2_DirectionRestrictPass ) )
		bReturn = TRUE
	ENDIF
	
	RETURN bReturn
ENDFUNC

//FMMC2020 - put this back in locates once the below if sorted
FUNC BOOL IS_LOCATION_READY_FOR_PROCESSING(INT iLocation)
	IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLocation].iLocProcessingDelay > 0		
		IF NOT HAS_NET_TIMER_STARTED(tdLocationProcDelayTimer[iLocation])
		OR NOT HAS_NET_TIMER_EXPIRED_READ_ONLY(tdLocationProcDelayTimer[iLocation], g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLocation].iLocProcessingDelay*1000)		
			PRINTLN("SHOULD_PROCESS_LOCATION - IS_LOCATION_READY_FOR_PROCESSING location: ", iLocation, " has a processing delay that has not yet expired (Delay length ", g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLocation].iLocProcessingDelay, "). Returning False.")
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

//FMMC2020 - put this back in locates once the below if sorted
FUNC BOOL IS_DROPOFF_AN_AERIAL_FLAG( INT iLoc )
	RETURN g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].iArial[0] = 1
ENDFUNC

//FMMC2020 - put this back in locates once the below if sorted
FUNC BOOL IS_GO_TO_LOC_READY_TO_SEND_OUT_COMPLETION_EVENT(INT iTeam, INT iRule)
	
	BOOL bReady = FALSE
	
	INT iEntityType = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].OverwriteEntityType[iRule]
	INT iEntityID = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].OverwriteEntityId[iRule]
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(OverwriteObjectiveNetID)
		
		IF iEntityType > 0
		AND iEntityID != -1
			IF NOT HAS_NET_TIMER_STARTED(tdVehDropoffNetIDRegTimer)
				PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LOC / IS_VEHICLE_DROPOFF_READY_TO_SEND_OUT_COMPLETION_EVENT - OverwriteObjectiveNetID exists - iTeam ",iTeam,", iRule ",iRule,", iEntityType ",iEntityType,", iEntityID ",iEntityID," - all valid, broadcast net ID")
				BROADCAST_NETWORK_INDEX_FOR_LATER_RULE(OverwriteObjectiveNetID, iEntityType, iEntityID)
				REINIT_NET_TIMER(tdVehDropoffNetIDRegTimer)
			ELSE
				IF DOES_FMMC_NET_ENTITY_EXIST(iEntityType, iEntityID)
					
					PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LOC / IS_VEHICLE_DROPOFF_READY_TO_SEND_OUT_COMPLETION_EVENT - Entity w type ",iEntityType," & ID ",iEntityID," exists on the server, ready to move on!")
					OverwriteObjectiveNetID = NULL
					RESET_NET_TIMER(tdVehDropoffNetIDRegTimer)
					bReady = TRUE
					
				ELIF (GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdVehDropoffNetIDRegTimer) > 2000)
					
					PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LOC / IS_VEHICLE_DROPOFF_READY_TO_SEND_OUT_COMPLETION_EVENT - Give up waiting for server to register net ID, let's try again")
					BROADCAST_NETWORK_INDEX_FOR_LATER_RULE(OverwriteObjectiveNetID, iEntityType, iEntityID)
					RESET_NET_TIMER(tdVehDropoffNetIDRegTimer)
					
				#IF IS_DEBUG_BUILD
				ELSE
					PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LOC / IS_VEHICLE_DROPOFF_READY_TO_SEND_OUT_COMPLETION_EVENT - Waiting for server to register net ID, been waiting for ",GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdVehDropoffNetIDRegTimer),"ms")
				#ENDIF
				ENDIF
			ENDIF
		ELSE
			OverwriteObjectiveNetID = NULL
			bReady = TRUE
			PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LOC / IS_VEHICLE_DROPOFF_READY_TO_SEND_OUT_COMPLETION_EVENT - OverwriteObjectiveNetID exists - iTeam ",iTeam,", iRule ",iRule," - iEntityType ",iEntityType," / iEntityID ",iEntityID," not valid, clearing net ID")
		ENDIF
	ELSE
		bReady = TRUE
		
		IF iEntityType > 0
		AND iEntityID != -1
			IF NOT DOES_FMMC_NET_ENTITY_EXIST(iEntityType, iEntityID)
				IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
					VEHICLE_INDEX tempVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
					IF IS_VEHICLE_DRIVEABLE(tempVeh)
						IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(tempVeh)
							PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LOC / IS_VEHICLE_DROPOFF_READY_TO_SEND_OUT_COMPLETION_EVENT - Returning FALSE, OverwriteObjectiveNetID doesn't exist but we're in a vehicle which could be registered and don't have control!")
							bReady = FALSE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF iRule < FMMC_MAX_RULES
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFourteen[iRule], ciBS_RULE14_USE_LOCATION_COLLECTION_DELAY)
			IF bReady		
				bReady = FALSE
				IF NOT HAS_NET_TIMER_STARTED(tdLocalLocateDelayTimer)
				OR HAS_NET_TIMER_EXPIRED_READ_ONLY(tdLocalLocateDelayTimer, 1000)				
					bReady = TRUE				
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN bReady
	
ENDFUNC

PROC GET_LOCATION_REQUIREMENT_DATA(INT iLoc, INT iTeam, INT &iTeamsBS, INT &iPlayersNeeded, INT &iPlayersInLoc)
	
	IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iWholeTeamAtLocation[iteam] = ciGOTO_LOCATION_WHOLE_TEAM
						
		iPlayersNeeded = MC_serverBD.iNumberOfPlayingPlayers[iteam]
		iPlayersInLoc = MC_serverBD_1.iNumberOfTeamInArea[iLoc][iteam]
		
		SET_BIT(iTeamsBS, iteam)
		
	ELIF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iWholeTeamAtLocation[iteam] = ciGOTO_LOCATION_ALL_PLAYERS
		
		iPlayersNeeded = MC_serverBD.iNumberOfPlayingPlayers[0] + MC_serverBD.iNumberOfPlayingPlayers[1] + MC_serverBD.iNumberOfPlayingPlayers[2] + MC_serverBD.iNumberOfPlayingPlayers[3]
		iPlayersInLoc = MC_serverBD_1.inumberOfTeamInAnyArea[iLoc][0] + MC_serverBD_1.inumberOfTeamInAnyArea[iLoc][1] + MC_serverBD_1.inumberOfTeamInAnyArea[iLoc][2] + MC_serverBD_1.inumberOfTeamInAnyArea[iLoc][3]
		
		iTeamsBS = 15 //Set bit 0, 1, 2 and 3
		
	ELIF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iWholeTeamAtLocation[iteam] = ciGOTO_LOCATION_CUSTOM_TEAMS
		
		INT iTeamLoop
		
		FOR iTeamLoop = 0 TO (MC_serverBD.iNumberOfTeams - 1)
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS,ciLoc_BS_CustomTeams_T0_Needs_T0 + (iTeam * 4) + iTeamLoop)
				iPlayersNeeded += MC_serverBD.iNumberOfPlayingPlayers[iTeamLoop]
				iPlayersInLoc = MC_serverBD_1.inumberOfTeamInAnyArea[iLoc][iTeamLoop]
				
				SET_BIT(iTeamsBS, iTeamLoop)
			ENDIF
		ENDFOR
		
	ELSE //Custom no. players:
		
		iPlayersNeeded = 2 + (g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iWholeTeamAtLocation[iTeam] - ciGOTO_LOCATION_2_PLAYERS)
		iPlayersInLoc = MC_serverBD_1.inumberOfTeamInAnyArea[iLoc][0] + MC_serverBD_1.inumberOfTeamInAnyArea[iLoc][1] + MC_serverBD_1.inumberOfTeamInAnyArea[iLoc][2] + MC_serverBD_1.inumberOfTeamInAnyArea[iLoc][3]
		
		iTeamsBS = 15 //Set bit 0, 1, 2 and 3
		
	ENDIF
ENDPROC

FUNC CONTROL_ACTION GET_LOCATION_TRIGGER_INPUT(INT iInputTrigger)
	SWITCH iInputTrigger
		CASE LOCATIONS_TRIGGER_INPUT_BUTTON RETURN INPUT_CONTEXT
		CASE LOCATIONS_TRIGGER_INPUT_LEADER_ONLY RETURN INPUT_VEH_HORN
	ENDSWITCH
	
	RETURN INPUT_CONTEXT
ENDFUNC

FUNC BOOL IS_LEADER_VALID_FOR_TRIGGER()
	
	PLAYER_INDEX piLeader = GET_FMMC_LOBBY_LEADER()
	
	IF NOT NETWORK_IS_PLAYER_ACTIVE(piLeader)
		PRINTLN("IS_LEADER_VALID_FOR_TRIGGER - NETWORK_IS_PLAYER_ACTIVE")
		RETURN FALSE
	ENDIF
	
	PARTICIPANT_INDEX piPart = NETWORK_GET_PARTICIPANT_INDEX(piLeader)
	INT iLeader = NATIVE_TO_INT(piPart)
	
	IF iLeader != -1
	AND IS_BIT_SET(MC_playerBD[iLeader].iClientBitSet, PBBOOL_PLAYER_OUT_OF_LIVES)
		PRINTLN("IS_LEADER_VALID_FOR_TRIGGER - Leader Out of Lives")
		RETURN FALSE
	ENDIF
	
	
	RETURN TRUE
ENDFUNC

FUNC TEXT_LABEL_63 GET_TRIGGER_INPUT_HELP_TEXT_TO_SHOW(INT iLoc, STRING sHelpText)
	
	TEXT_LABEL_63 tl63 = sHelpText
	
	IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].sLocTriggerInput.iHelpText != -1
		tl63 = GET_CUSTOM_STRING_LIST_TEXT_LABEL(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].sLocTriggerInput.iHelpText)
	ENDIF
	
	RETURN tl63
ENDFUNC

FUNC BOOL DOES_PLAYER_MEET_TRIGGER_PARTICIPATION_REQUIREMENTS(INT iLoc, CONTROL_ACTION caInputReq, PED_INDEX piPlayerPed, BOOL bDoHelpText = TRUE, BOOL bDoObjTextOverride = TRUE)
	
	IF caInputReq = INPUT_VEH_HORN
		IF NOT IS_PED_DRIVING_ANY_VEHICLE(piPlayerPed)
			PRINTLN("[TriggerInput][Loc ",iLoc,"] DOES_PLAYER_MEET_TRIGGER_REQUIREMENTS - Player is not in vehicle/not driving the vehicle to use vehicle horn")
			IF IS_PED_IN_ANY_VEHICLE(piPlayerPed)
				IF bDoHelpText
				AND NOT IS_THIS_PRINT_BEING_DISPLAYED("FMMC_LIT_DRIV")
					DISPLAY_HELP_TEXT_THIS_FRAME("FMMC_LIT_DRIV", TRUE)
				ENDIF
				IF bDoObjTextOverride
					OVERRIDE_OBJECTIVE_TEXT_WITH_CUSTOM_STRING_THIS_FRAME(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].sLocTriggerInput.iWaitObjText)
				ENDIF
			ENDIF
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].sLocTriggerInput.iInputBitSet, ciLocTrigger_BS_LeaderOnly)
		PED_INDEX piLeader = GET_PLAYER_PED(GB_GET_LOCAL_PLAYER_GANG_BOSS()) 

		IF IS_LEADER_VALID_FOR_TRIGGER()
		AND piPlayerPed != piLeader
			PRINTLN("[TriggerInput][Loc ",iLoc,"] DOES_PLAYER_MEET_TRIGGER_REQUIREMENTS - Player is not gang boss")
			IF bDoHelpText
			AND NOT IS_THIS_PRINT_BEING_DISPLAYED("FMMC_LIT_LEAD")
				DISPLAY_HELP_TEXT_THIS_FRAME("FMMC_LIT_LEAD", TRUE)
			ENDIF
			IF bDoObjTextOverride
				OVERRIDE_OBJECTIVE_TEXT_WITH_CUSTOM_STRING_THIS_FRAME(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].sLocTriggerInput.iWaitObjText)
			ENDIF
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC INT GET_INPUT_TRIGGER_REQUIRED_FOR_LOCATION(INT iLoc)
	RETURN g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].sLocTriggerInput.iInputRequired
ENDFUNC

FUNC BOOL IS_INPUT_TRIGGER_REQUIRED_FOR_LOCATION(INT iLoc)
	INT iInput = GET_INPUT_TRIGGER_REQUIRED_FOR_LOCATION(iLoc)
	RETURN iInput != LOCATIONS_TRIGGER_INPUT_INVALID
ENDFUNC

FUNC BOOL HAS_PLAYER_TRIGGERED_LOCATION(INT iLoc, INT iTeam, PED_INDEX piPlayerPed, BOOL bDoHelpText = TRUE, BOOL bDoObjTextOverride = TRUE)
	
	IF NOT IS_INPUT_TRIGGER_REQUIRED_FOR_LOCATION(iLoc)
		RETURN TRUE
	ENDIF

	INT iInput = GET_INPUT_TRIGGER_REQUIRED_FOR_LOCATION(iLoc)
	
	PRINTLN("[TriggerInput][Loc ",iLoc,"] HAS_PLAYER_TRIGGERED_LOCATION - iInput: ", iInput, " Bitset: ", g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].sLocTriggerInput.iInputBitSet)
	
	CONTROL_ACTION caInputReq = GET_LOCATION_TRIGGER_INPUT(iInput)
	
	IF NOT DOES_PLAYER_MEET_TRIGGER_PARTICIPATION_REQUIREMENTS(iLoc, caInputReq, piPlayerPed, bDoHelpText, bDoObjTextOverride)
		IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iWholeTeamAtLocation[iTeam] != ciGOTO_LOCATION_INDIVIDUAL
			RETURN IS_BIT_SET(MC_serverBD_4.iLocationInputTriggeredBitset, iLoc)
		ELSE
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF piPlayerPed = LocalPlayerPed
		IF NOT IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, caInputReq)
			TEXT_LABEL_63 tl63 = "FMMC_LIT_"
			tl63 += iInput
			
			tl63 = GET_TRIGGER_INPUT_HELP_TEXT_TO_SHOW(iLoc, tl63)
			
			IF bDoHelpText
			AND NOT IS_THIS_PRINT_BEING_DISPLAYED(tl63)
				PRINT_HELP(tl63)
			ENDIF
			
			IF bDoObjTextOverride
				OVERRIDE_OBJECTIVE_TEXT_WITH_CUSTOM_STRING_THIS_FRAME(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].sLocTriggerInput.iPrimaryObjText)
			ENDIF
			
			PRINTLN("[TriggerInput][Loc ",iLoc,"] HAS_PLAYER_TRIGGERED_LOCATION - Waiting for press")
			RETURN FALSE
		ELSE
			BROADCAST_FMMC_LOCATION_INPUT_TRIGGERED(iLoc)
			PRINTLN("[TriggerInput][Loc ",iLoc,"] HAS_PLAYER_TRIGGERED_LOCATION - PRESSED")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN IS_BIT_SET(MC_serverBD_4.iLocationInputTriggeredBitset, iLoc)
ENDFUNC

FUNC BOOL CAN_PLAYER_PASS_THIS_LOCATION(PED_INDEX piPlayerPed, INT iLoc, INT iTeam, BOOL bDoHelpText = TRUE, BOOL bDoObjTextOverride = TRUE)
	
	IF IS_OBJECTIVE_BLOCKED()
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_PLAYER_TRIGGERED_LOCATION(iLoc, iTeam, piPlayerPed, bDoHelpText, bDoObjTextOverride)
		PRINTLN("[RCC MISSION][TriggerInput][Loc ",iLoc,"] IS_PLAYER_IN_LOCATION - Not triggered location yet")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_VALID_TO_PASS_LOCATION(INT iLoc, INT iTeam)
	
	IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iLocationInSecondaryLocation, iLoc)
		RETURN FALSE
	ENDIF
	
	IF NOT IS_LOCATION_READY_FOR_PROCESSING(iLoc)
		RETURN FALSE
	ENDIF
	
	IF NOT CAN_PLAYER_PASS_THIS_LOCATION(LocalPlayerPed, iLoc, iTeam)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_PLAYER_IN_LOCATION(PED_INDEX piPlayerPed, INT iLoc, INT iTeam)
	
	IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].vloc1)
	AND NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].vloc2)
		IF NOT IS_ENTITY_IN_ANGLED_AREA(piPlayerPed, g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].vloc1, g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].vloc2, g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].fWidth)
			
			#IF IS_DEBUG_BUILD
			IF bGotoLocPrints
				PRINTLN("[RCC MISSION] IS_PLAYER_IN_LOCATION ",iLoc," = FALSE - Ped isn't in angled area, v1 = ",g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].vloc1," v2 = ",g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].vloc2," width = ",g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].fWidth)
			ENDIF
			#ENDIF
			
			RETURN FALSE
		ENDIF
	ELSE
		FLOAT fDist
		VECTOR vLoc = GET_LOCATION_VECTOR(iLoc)
		fDist = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].fRadius[0] * g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].fRadius[0]
		IF VDIST2(GET_ENTITY_COORDS(piPlayerPed), vLoc) > fDist
			
			#IF IS_DEBUG_BUILD
			IF bGotoLocPrints
				PRINTLN("[RCC MISSION] IS_PLAYER_IN_LOCATION ",iLoc," = FALSE - Ped isn't in normal area, loc = ",vLoc," box size/radius = ",g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].fRadius[0])
			ENDIF
			#ENDIF
			
			RETURN FALSE
		ENDIF
	ENDIF

	IF MC_serverBD_4.iGotoLocationDataRule[iLoc][iTeam] != FMMC_OBJECTIVE_LOGIC_LEAVE_LOCATION
		IF NOT IS_PLAYER_IN_REQUIRED_LOCATION_VEHICLE(piPlayerPed, iTeam, iLoc)
			
			IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MC_WRG_VEH")
				PRINT_HELP("MC_WRG_VEH")
			ENDIF
			
			#IF IS_DEBUG_BUILD
			IF bGotoLocPrints
				PRINTLN("[RCC MISSION] IS_PLAYER_IN_LOCATION ",iLoc," = FALSE - Ped isn't in required location vehicle, model ", FMMC_GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].mnVehicleNeeded[iTeam]))
			ENDIF
			#ENDIF
			
			RETURN FALSE
		ENDIF
		
		IF NOT IS_PLAYER_IN_SPECIFIC_REQUIRED_LOCATION_VEHICLE_INDEX(piPlayerPed, iTeam, iLoc)
			#IF IS_DEBUG_BUILD
			IF bGotoLocPrints
				PRINTLN("[RCC MISSION] IS_PLAYER_IN_LOCATION ",iLoc," = FALSE - Ped isn't in specific required location vehicle index, index ", g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocSpecificVehicleIndex)
			ENDIF
			#ENDIF
			
			RETURN FALSE
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS, ciLoc_BS_ForceOnFoot)
		AND IS_PED_IN_ANY_VEHICLE(piPlayerPed)
			PRINTLN("[RCC MISSION] IS_PLAYER_IN_LOCATION ",iLoc," = FALSE - Ped isn't in on foot but should be")
			
			RETURN FALSE
		ENDIF
		
		IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].fTeamMembersNeededWithin > 0
			IF NOT ARE_ALL_TEAM_MEMBERS_WITHIN_REQUIRED_DISTANCE(iTeam)
				#IF IS_DEBUG_BUILD
				IF bGotoLocPrints
					PRINTLN("[RCC MISSION] IS_PLAYER_IN_LOCATION ",iLoc," = FALSE - Team members aren't all within ",g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ iLoc ].fTeamMembersNeededWithin,"m")
				ENDIF
				#ENDIF
				
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_PREREQUISITE_FOR_LOCATION_COMPLETED(iLoc, piPlayerPed = PlayerPedToUse)
		#IF IS_DEBUG_BUILD
		IF bGotoLocPrints
			PRINTLN("[RCC MISSION][TriggerInput][Loc ",iLoc,"] IS_PLAYER_IN_LOCATION - player not able to pass this location yet")
		ENDIF
		#ENDIF
		
		RETURN FALSE
	ENDIF

	#IF IS_DEBUG_BUILD
	IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].vloc1)
	AND NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].vloc2)
		PRINTLN("[RCC MISSION][Loc ",iLoc,"] IS_PLAYER_IN_LOCATION - RETURNING TRUE - AREA")
	ELSE
		PRINTLN("[RCC MISSION][Loc ",iLoc,"] IS_PLAYER_IN_LOCATION - RETURNING TRUE - LOCATION")
	ENDIF
	#ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_IN_LOCATION(INT iLoc, INT iTeam)

	IF IS_BIT_SET(iLocalPlayerInLocationBitset, iLoc)
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(iLocalPlayerNotInLocationBitset, iLoc)
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_IN_LOCATION(LocalPlayerPed, iLoc, iTeam)
		SET_BIT(iLocalPlayerInLocationBitset, iLoc)
		RETURN TRUE
	ELSE
		SET_BIT(iLocalPlayerNotInLocationBitset, iLoc)
		RETURN FALSE
	ENDIF
	
ENDFUNC

FUNC BOOL SHOULD_LOCAL_PLAYER_PROCESS_COMPLETED_OBJECTIVE(INT iTeam, INT iRule)
	
	IF MC_playerBD[iPartToUse].iObjectiveTypeCompleted != -1
		#IF IS_DEBUG_BUILD	
		IF bPrintExtraObjectiveCompletion
			PRINTLN("[RCC MISSION] SHOULD_LOCAL_PLAYER_PROCESS_COMPLETED_OBJECTIVE - HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE - RETURNING FALSE - iObjectiveTypeCompleted: ", MC_playerBD[iPartToUse].iObjectiveTypeCompleted)
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(MC_serverBD.iProcessJobCompBitset, iPartToUse)
		#IF IS_DEBUG_BUILD	
		IF bPrintExtraObjectiveCompletion
			PRINTLN("[RCC MISSION] SHOULD_LOCAL_PLAYER_PROCESS_COMPLETED_OBJECTIVE - HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE - RETURNING FALSE - iProcessJobCompBitset: ", IS_BIT_SET(MC_serverBD.iProcessJobCompBitset, iPartToUse))
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
		
	IF IS_OBJECTIVE_BLOCKED()
		#IF IS_DEBUG_BUILD	
		IF bPrintExtraObjectiveCompletion
			PRINTLN("[RCC MISSION] SHOULD_LOCAL_PLAYER_PROCESS_COMPLETED_OBJECTIVE - HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE - RETURNING FALSE - IS_OBJECTIVE_BLOCKED: ", IS_OBJECTIVE_BLOCKED())
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_FINISHED)
		#IF IS_DEBUG_BUILD	
		IF bPrintExtraObjectiveCompletion
			PRINTLN("[RCC MISSION] SHOULD_LOCAL_PLAYER_PROCESS_COMPLETED_OBJECTIVE - HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE - RETURNING FALSE - PBBOOL_FINISHED: ", IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_FINISHED))
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF iRule >= FMMC_MAX_RULES
		#IF IS_DEBUG_BUILD	
		IF bPrintExtraObjectiveCompletion
			PRINTLN("[RCC MISSION] SHOULD_LOCAL_PLAYER_PROCESS_COMPLETED_OBJECTIVE - HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE - RETURNING FALSE - iRule: ", iRule)
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(MC_serverBD_1.iObjectiveProgressionHeldUpBitset[iTeam], iRule)
		#IF IS_DEBUG_BUILD	
		IF bPrintExtraObjectiveCompletion
			PRINTLN("[RCC MISSION] SHOULD_LOCAL_PLAYER_PROCESS_COMPLETED_OBJECTIVE - HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE - RETURNING FALSE - iObjectiveProgressionHeldUpBitset: ", IS_BIT_SET(MC_serverBD_1.iObjectiveProgressionHeldUpBitset[iTeam], iRule), " iRule: ", iRule)
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(MC_serverBD.iServerBitSet, SSBOOL_TEAM0_FAILED + iTeam)
		#IF IS_DEBUG_BUILD	
		IF bPrintExtraObjectiveCompletion
			PRINTLN("[RCC MISSION] SHOULD_LOCAL_PLAYER_PROCESS_COMPLETED_OBJECTIVE - HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE - RETURNING FALSE - SSBOOL_TEAM0_FAILED: ", IS_BIT_SET(MC_serverBD.iServerBitSet, SSBOOL_TEAM0_FAILED + iTeam))
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF HAS_TEAM_FINISHED(iTeam)
		#IF IS_DEBUG_BUILD	
		IF bPrintExtraObjectiveCompletion
			PRINTLN("[RCC MISSION] SHOULD_LOCAL_PLAYER_PROCESS_COMPLETED_OBJECTIVE - HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE - RETURNING FALSE - HAS_TEAM_FINISHED: ", HAS_TEAM_FINISHED(iTeam))
		ENDIF
		#ENDIF	
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

// :(
FUNC BOOL HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE() 

	INT i
	INT iscoremulti
	BOOL bvalidreward, bInvalidReward, bcollect,bstopped,bdrawAirDropOff,bcargobobdrop,bInAir
	FLOAT fradius
	INT iDropOffEntity
	VEHICLE_INDEX tempVeh,trailerVeh,WinchVeh
	PED_INDEX tempPed
	FLOAT fDelXp 
	INT iDelXp
	FLOAT fStopDist
	INT iTimeToStop

	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[ iTeam ]
	BOOL bEnableShooting = FALSE
	FLOAT fMarkerDist2
	FLOAT fMarkerDisappear  = 3.0
	VECTOR vThisMarkerLoc
	INT iBitsetArrayBlank[1]
	
	BOOL bGroupDropOff
	BOOL bGroupDropOffValid
	
	CLEAR_BIT(MC_playerBD[ iPartToUse ].iClientBitSet2, PBBOOL2_IN_DROP_OFF)

	IF updateLocateFadeStruct != NULL
		CALL updateLocateFadeStruct(locateDropOff)
	ENDIF
	
	IF NOT bPlayerToUseOK
		CLEAR_BIT(iLocalBoolCheck2,LBOOL2_PED_DROPPED_OFF)
		RETURN FALSE
	ENDIF
	
	// Don't proceed if load scene is active, due to calls that give player control back
	IF IS_NEW_LOAD_SCENE_ACTIVE()
	OR IS_PLAYER_TELEPORT_ACTIVE()
		PRINTLN("HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE - Returning FALSE due to IS_NEW_LOAD_SCENE_ACTIVE")
		RETURN FALSE
	ENDIF

	IF iRule < FMMC_MAX_RULES
		
		bEnableShooting = IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFour[iRule], ciBS_RULE4_ENABLE_WEAPONS_WHEN_STOPPED) 

		bGroupDropOff = IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThirteen[iRule], ciBS_RULE13_DROP_OFF_REQUIRES_ALL_PLAYERS_PRESENCE)
		
		IF bGroupDropOff
			bGroupDropOffValid = DOES_ANY_PLAYER_HAVE_THE_DROP_OFF_TARGET()
		ENDIF
		
		IF bGroupDropOff
		AND bGroupDropOffValid
		AND DOES_THIS_PART_HAVE_THE_DROP_OFF_TARGET(iLocalPart)
			SET_BIT(iLocalBoolCheck30, LBOOL30_SHOW_CUSTOM_WAIT_TEXT)
		ELSE
			CLEAR_BIT(iLocalBoolCheck30, LBOOL30_SHOW_CUSTOM_WAIT_TEXT)
		ENDIF
		
		IF ((MC_playerBD[iPartToUse].iPedCarryCount > 0
			OR MC_playerBD[iPartToUse].iVehCarryCount > 0
			OR MC_playerBD[iPartToUse].iObjCarryCount > 0)
			OR IS_BIT_SET( MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_WITH_CARRIER)
			AND NOT IS_BIT_SET(MC_playerBD[ iPartToUse ].iClientBitSet2, PBBOOL2_DROPPING_OFF))
			OR bGroupDropOffValid
			
			PRINTLN("HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE() - Called -1 ")
			//If we have enough entities to drop off:
			fradius = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].fDropOffRadius[iRule]
			
			IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDropOffType[iRule] = ciFMMC_DROP_OFF_TYPE_PLACED_ZONE
			AND iCurrentDropOffZone[iTeam] > -1
			AND g_FMMC_STRUCT_ENTITIES.sPlacedZones[iCurrentDropOffZone[iTeam]].iAreaType = ciFMMC_ZONE_SHAPE__SPHERE
				fradius = g_FMMC_STRUCT_ENTITIES.sPlacedZones[iCurrentDropOffZone[iTeam]].fRadius
			ENDIF
			
			IF ((MC_serverBD.iRequiredDeliveries[iTeam] <= 1)
				OR ((MC_serverBD.iNumPedHighestPriorityHeld[iTeam] + MC_serverBD.iNumVehHighestPriorityHeld[iTeam] + MC_serverBD.iNumObjHighestPriorityHeld[iTeam]) >= MC_serverBD.iRequiredDeliveries[iTeam]))				
				
				PRINTLN("HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE() - Called -2")
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iDropOffBitSet, iRule )
					DRAW_AIR_DROP_OFF()
				ELSE
					PRINTLN("HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE() - Called -3")
					PRINTLN("HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE() - locateDropOff.iAlphaValue: ", locateDropOff.iAlphaValue)
					IF iSpectatorTarget !=-1
						IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iSpectatorTarget))
							PED_INDEX pedSpecPlayer = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iSpectatorTarget)))
							vThisMarkerLoc = GET_COORDS_FOR_LOCATE_MARKER(fradius)
							//fDirection = GET_DIRECTION_FOR_LOCATE_MARKER()
							fMarkerDist2 = VDIST2(GET_ENTITY_COORDS(pedSpecPlayer), vThisMarkerLoc)
							IF fMarkerDist2 <= (fMarkerDisappear * fMarkerDisappear)	
								PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE - Fading out locate for spectator fMarkerDist2 = ", fMarkerDist2)
								IF setLocateToFadeOut != NULL
									CALL setLocateToFadeOut(locateDropOff)
								ENDIF
							ELSE
								PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE - Displaying locate for spectator fMarkerDist2 = ", fMarkerDist2)
								IF setLocateToDisplay != NULL
									CALL setLocateToDisplay(locateDropOff)
								ENDIF
							ENDIF
						ENDIF	
					ENDIF
					
					DRAW_GROUND_DROP_OFF(fradius, locateDropOff.iAlphaValue)					
				ENDIF				
			ENDIF			
		ENDIF		
	ENDIF
	
	IF iSpectatorTarget != -1
	OR IS_LOCAL_PLAYER_ANY_SPECTATOR()
		RETURN FALSE
	ENDIF
	
	CLEAR_COMPLETED_OBJECTIVE_DATA()
	
	PROCESS_STOPPING_VEHICLE_FOR_DELIVERY(iRule, iTeam, bEnableShooting)
		
	IF SHOULD_LOCAL_PLAYER_PROCESS_COMPLETED_OBJECTIVE(iTeam, iRule)
	
		BOOL bHasVehDropoffChanged // This stores if we've hit a new rule and we're still in the middle of dropping a car off. This is so
								   // that we can stop the delivery from going through and passing a different rule
		
		IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam)
		AND MC_playerBD[iPartToUse].iVehDeliveryId != -1
			PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE - Vehicle delivery in progress, but this is a new rule (set bHasVehDropoffChanged)")
			bHasVehDropoffChanged = TRUE
		ENDIF
		
		IF iRule  < FMMC_MAX_RULES
			
			#IF IS_DEBUG_BUILD
			IF bPrintExtraObjectiveCompletion				
				PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE - iPedCarryCount: 				", MC_playerBD[iPartToUse].iPedCarryCount,
																			" iRequiredDeliveries: 			", MC_serverBD.iRequiredDeliveries[iTeam],
																			" iNumPedHighestPriorityHeld: 	", MC_serverBD.iNumPedHighestPriorityHeld[iTeam],
																			" IS_PLAYER_IN_DROP_OFF: 		", IS_PLAYER_IN_DROP_OFF( fradius, GET_DROP_OFF_HEIGHT(iTeam, iRule)) )
			ENDIF
			#ENDIF
			
			IF MC_playerBD[iPartToUse].iPedCarryCount > 0 // for dropping off peds you collect
			AND MC_serverBD.iNumPedHighestPriorityHeld[iTeam] >= MC_serverBD.iRequiredDeliveries[iTeam]
					
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iDropOffBitSet,  iRule ) // this is an air drop off
					bdrawAirDropOff = TRUE
				ENDIF
				
				FLOAT fTemp
				vThisMarkerLoc = GET_COORDS_FOR_LOCATE_MARKER(fTemp)
				fMarkerDist2 = VDIST2(GET_ENTITY_COORDS(LocalPlayerPed), vThisMarkerLoc)
			
				IF fMarkerDist2 <= (fMarkerDisappear * fMarkerDisappear)
					IF NOT IS_BIT_SET(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_FADE_OUT)
						PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED - Set  LBOOL10_DROP_OFF_LOC_FADE_OUT - I'm delivering Ped fMarkerDist2 = ", fMarkerDist2, " fMarkerDisappear = ", fMarkerDisappear, " vThisMarkerLoc = ", vThisMarkerLoc)
						SET_BIT(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_FADE_OUT)
						CLEAR_BIT(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_DISPLAY)
					ENDIF
				ELSE
					IF IS_BIT_SET(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_FADE_OUT)
						IF fMarkerDist2 >= ((fMarkerDisappear + 3.0) * (fMarkerDisappear + 3.0))	
							PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED - Set  LBOOL10_DROP_OFF_LOC_DISPLAY - I'm delivering Ped, marker had been removed but need to display fMarkerDist2 = ", fMarkerDist2, " fMarkerDisappear = ", (fMarkerDisappear + 3.0), " vThisMarkerLoc = ", vThisMarkerLoc)
							CLEAR_BIT(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_FADE_OUT)
							SET_BIT(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_DISPLAY)
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_PLAYER_IN_DROP_OFF( fradius, GET_DROP_OFF_HEIGHT(iTeam, iRule) )
					
					SET_BIT(MC_playerBD[ iPartToUse ].iClientBitSet2, PBBOOL2_IN_DROP_OFF)
					
					BOOL bValid = TRUE
					IF NOT ARE_ALL_PLAYERS_IN_DROP_OFF_CONDITIONS(iTeam, iRule)
						bValid = FALSE
					ENDIF
					
					PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED - In dropoff ")
					// Only slow the vehicle down if they're in a vehicle and the dropoff
					// is on the ground
					IF bValid
						IF IS_PED_IN_ANY_VEHICLE( LocalPlayerPed )
						AND SHOULD_VEH_STOP_ON_DELIVERY()
						AND NOT bdrawAirDropOff
													
							IF NOT IS_BIT_SET(iLocalBoolCheck9, LBOOL9_SHOULD_STOP_VEHICLE)
								fStopDist = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].fDropOffStopDistance[iRule]
								IF fStopDist <= 0.0
									fStopDist = DEFAULT_VEH_STOPPING_DISTANCE
								ELIF fStopDist > 20.0
									fStopDist = 20.0
								ENDIF
								IF NOT IS_MY_VEHICLE_STOPPED(DEFAULT, fStopDist, DEFAULT #IF IS_DEBUG_BUILD, TRUE #ENDIF)
									IF NOT IS_BIT_SET(iLocalBoolCheck11, LBOOL11_SET_SHOULD_STOP_IN_DROP_OFF)
										SET_BIT(iLocalBoolCheck9, LBOOL9_SHOULD_STOP_VEHICLE)
										SET_BIT(iLocalBoolCheck11, LBOOL11_SET_SHOULD_STOP_IN_DROP_OFF)
										PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED - Should stop veh on delivery - set LBOOL9_SHOULD_STOP_VEHICLE, set LBOOL11_SET_SHOULD_STOP_IN_DROP_OFF")
									ELSE
										PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED - IS_MY_VEHICLE_STOPPED is false but LBOOL11_SET_SHOULD_STOP_IN_DROP_OFF alrready set!")
									ENDIF
								ELSE
									IF NOT IS_BIT_SET(iLocalBoolCheck2,LBOOL2_PED_DROPPED_OFF)
										SET_BIT(iLocalBoolCheck2,LBOOL2_PED_DROPPED_OFF)
										PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED - IS_MY_VEHICLE_STOPPED is true but LBOOL9_SHOULD_STOP_VEHICLE Not Set somehow! - Setting LBOOL2_PED_DROPPED_OFF")
									ENDIF
								ENDIF
							ENDIF

						ELSE // Set the ped has been dropped off if the player is not in a vehicle or if they're in the air
							PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED - Don't need to stop a vehicle")
							SET_BIT( iLocalBoolCheck2, LBOOL2_PED_DROPPED_OFF )
						ENDIF
					ENDIF
				ELSE
					IF NOT IS_BIT_SET(iLocalBoolCheck9, LBOOL9_SHOULD_STOP_VEHICLE)
						IF NOT IS_BIT_SET(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_DISPLAY)
							IF NOT IS_BIT_SET(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_FADE_OUT)
								PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED Set LBOOL10_DROP_OFF_LOC_DISPLAY")
								SET_BIT(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_DISPLAY)
							ENDIF
						ENDIF
					ENDIF
					
					IF IS_BIT_SET(iLocalBoolCheck11, LBOOL11_SET_SHOULD_STOP_IN_DROP_OFF)
						CLEAR_BIT(iLocalBoolCheck11, LBOOL11_SET_SHOULD_STOP_IN_DROP_OFF)
						PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED Cleared LBOOL11_SET_SHOULD_STOP_IN_DROP_OFF as no longer in drop off")
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(iLocalBoolCheck9, LBOOL9_SHOULD_STOP_VEHICLE)
					IF IS_PED_IN_ANY_VEHICLE(localPlayerPed)
						tempVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
						IF IS_VEHICLE_DRIVEABLE(tempVeh)	
							IF GET_PED_IN_VEHICLE_SEAT(tempVeh) = LocalPlayerPed // IS_MY_VEHICLE_STOPPED returns true immediately for non-drivers
								IF NOT IS_BIT_SET(iLocalBoolCheck10, LBOOL10_DRIVING_AND_CARRYING_PED)
									SET_BIT(iLocalBoolCheck10, LBOOL10_DRIVING_AND_CARRYING_PED)
									PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED LBOOL9_SHOULD_STOP_VEHICLE is set, I'm driver, set LBOOL10_DRIVING_AND_CARRYING_PED")
								ENDIF
							ELSE
								//-- I'm not driver - Driver will stop vehicle
								PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED LBOOL9_SHOULD_STOP_VEHICLE is set, but I'm not driver")
								FLOAT fSpeed = GET_ENTITY_SPEED(tempVeh)
								FLOAT fSpeedToCheck = 2.5
								
								IF IS_PED_IN_ANY_BOAT(LocalPlayerPed)
									fSpeedToCheck = 2.5
								ELIF IS_PED_IN_ANY_HELI(LocalPlayerPed)
								OR IS_PED_IN_ANY_PLANE(LocalPlayerPed)
									fSpeedToCheck = 4.0
								ELSE
									fSpeedToCheck = 0.1
								ENDIF
								
								IF fSpeed < fSpeedToCheck
									
									PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED LBOOL9_SHOULD_STOP_VEHICLE is set, but I'm not driver, fSpeed  = ", fSpeed, " fSpeedToCheck = ", fSpeedToCheck)
									SET_BIT(iLocalBoolCheck2,LBOOL2_PED_DROPPED_OFF)
									CLEAR_BIT(iLocalBoolCheck9, LBOOL9_SHOULD_STOP_VEHICLE)
								ELSE
									PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE Trying to set LBOOL2_PED_DROPPED_OFF but fSpeed = ", fSpeed, " fSpeedToCheck = ", fSpeedToCheck)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
				ENDIF
				
				IF IS_BIT_SET(iLocalBoolCheck2,LBOOL2_PED_DROPPED_OFF)
					IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
					AND NOT bdrawAirDropOff
					AND SHOULD_VEH_STOP_ON_DELIVERY()
						tempVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
						IF IS_VEHICLE_DRIVEABLE(tempVeh)
							IF NETWORK_HAS_CONTROL_OF_ENTITY(tempVeh)
								IF GET_ENTITY_SPEED(tempVeh) > 2
									
									FLOAT fSlow = 0.6
									
									IF (GET_PED_IN_VEHICLE_SEAT(tempveh) != LocalPlayerPed)
										fSlow = 0.35
									ENDIF
									
									VECTOR vSlow = << fSlow, fSlow, 1 >>
									
									VECTOR vVel = GET_ENTITY_VELOCITY(tempVeh)
									vVel *= vSlow
									SET_ENTITY_VELOCITY(tempVeh,vVel)
								ELSE
									SET_ENTITY_VELOCITY(tempVeh,<<0,0,0>>)
								ENDIF
							ELSE
								NETWORK_REQUEST_CONTROL_OF_ENTITY(tempVeh)
							ENDIF
						ENDIF
					ENDIF
					FOR i = 0 TO (MC_serverBD.iNumPedCreated - 1)
						IF FMMC_IS_LONG_BIT_SET(MC_playerBD[iPartToUse].iPedFollowingBitset, i)
							IF NOT IS_NET_PED_INJURED(MC_serverBD_1.sFMMC_SBD.niPed[i])
								IF MC_serverBD_4.iPedRule[i][ iTeam ] = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER
									IF MC_serverBD_4.iPedPriority[i][ iTeam ] < FMMC_MAX_RULES
									
										BOOL bNeedControl = FALSE
										
										IF NOT bdrawAirDropOff
											IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iFollow != ciPED_FOLLOW_ON
											OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedBitsetTwo,ciPED_BSTwo_StopFollowingOnLastDelivery)
												IF SHOULD_PED_CLEAN_UP_ON_DELIVERY(i, iTeam)
													bNeedControl = TRUE
												ELIF SHOULD_PED_LEAVE_GROUP_ON_DROP_OFF(i)
													bNeedControl = TRUE
												ENDIF
											ENDIF
										ENDIF
										
										BOOL bHasControl
										
										IF bNeedControl
											bHasControl = NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niPed[i])
											IF NOT bHasControl
												NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niPed[i])
											ENDIF
										ENDIF
										
										IF (NOT bNeedControl)
										OR bHasControl
											FMMC_SET_LONG_BIT(iPedDeliveredBitset, i)
											IF NOT bdrawAirDropOff
												
												tempPed = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[i])
												
												IF (g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iFollow != ciPED_FOLLOW_ON
												OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedBitsetTwo, ciPED_BSTwo_StopFollowingOnLastDelivery))												
													// 
													IF SHOULD_PED_CLEAN_UP_ON_DELIVERY(i, iTeam)
														SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed,TRUE)
														
														REMOVE_PED_FROM_GROUP(tempPed)
														SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_ALWAYS_FIGHT, FALSE)
														SET_PED_FLEE_ATTRIBUTES(tempPed, FA_DISABLE_COWER,TRUE)
														SET_PED_FLEE_ATTRIBUTES(tempPed,  FA_COWER_INSTEAD_OF_FLEE,FALSE)
														SET_PED_FLEE_ATTRIBUTES(tempPed, FA_DISABLE_FLEE_FROM_INDIRECT_THREATS,FALSE)
														IF IS_PED_IN_ANY_VEHICLE(tempPed)
															IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedBitsetTwelve, ciPed_BSTwelve_StayInVehicleAfterDelivery)
																SET_PED_COMBAT_ATTRIBUTES(tempPed,CA_LEAVE_VEHICLES,TRUE)
																SET_PED_FLEE_ATTRIBUTES(tempPed,  FA_USE_VEHICLE,FALSE)
																SET_PED_FLEE_ATTRIBUTES(tempPed,  FA_FORCE_EXIT_VEHICLE,TRUE)
																
																OPEN_SEQUENCE_TASK(PedDropOffSequence)
															    	TASK_LEAVE_ANY_VEHICLE(NULL)
																    TASK_WANDER_STANDARD(NULL)
																CLOSE_SEQUENCE_TASK(PedDropOffSequence)
																TASK_PERFORM_SEQUENCE(tempPed,PedDropOffSequence)
																CLEAR_SEQUENCE_TASK(PedDropOffSequence)
															ENDIF
														ELSE
															TASK_WANDER_STANDARD(tempPed)
														ENDIF
														
														PLAY_PED_AMBIENT_SPEECH_AND_CLONE_NATIVE(tempPed,"GENERIC_THANKS","SPEECH_PARAMS_FORCE_SHOUTED_CRITICAL")
														
														PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED - remove ped from group on drop off; ped given task leave (should clean up): ", i)
													ELSE
														//If we don't want to keep following after delivery
														IF SHOULD_PED_LEAVE_GROUP_ON_DROP_OFF(i)	
															SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed,FALSE)
															
															REMOVE_PED_FROM_GROUP(tempPed)
															
															IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedBitsetTwelve, ciPed_BSTwelve_StayInVehicleAfterDelivery)
																IF IS_PED_IN_ANY_VEHICLE(tempPed)
																	TASK_LEAVE_ANY_VEHICLE(tempPed)
																ENDIF
															ENDIF
															
															SET_PED_USING_ACTION_MODE(tempPed, FALSE, -1, "DEFAULT_ACTION")
															SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_PLAY_REACTION_ANIMS, TRUE)
															
															IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedBitsetFour, ciPED_BSFour_DisableRagdolling)
																CLEAR_RAGDOLL_BLOCKING_FLAGS(tempPed, RBF_BULLET_IMPACT | RBF_PLAYER_IMPACT)
															ENDIF
															
															FMMC_CLEAR_LONG_BIT(MC_playerBD[iPartToUse].iPedFollowingBitset, i)
															
															PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED - remove ped from group on drop off; ped doesn't clean up on delivery, but does leave group: ", i)
														ELSE
															PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED - ped doesn't clean up or leave group on delivery: ", i)
														ENDIF
													ENDIF
												ENDIF
											ENDIF
											iDropOffEntity = i
											iscoremulti++
											bvalidreward = TRUE
										ELSE
											bInvalidReward = TRUE //Hold off on delivering them all until we've got control of everyone following us
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDFOR
					IF bvalidreward AND NOT bInvalidReward
						IF bdrawAirDropOff
							IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
							AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwo[iRule], ciBS_RULE2_DONT_GIVE_PLAYER_XP)
								IF iPedDeliveryRPCap < 20
									fDelXp = (100*MC_playerBD[iPartToUse].iPedCarryCount*g_sMPTunables.fxp_tunable_Deliver_a_package_bonus)
									iDelXp = ROUND(fDelXp)
									GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD, LocalPlayerPed,"XPT_MEDIUMT", XPTYPE_COMPLETE, XPCATEGORY_COMPLETE_MISSION_DELIVERED_PED, iDelXp,1)
									iPedDeliveryRPCap++
								ENDIF
							ENDIF
							BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_DELIVER_PED, iscoremulti, iTeam ,-1,INVALID_PLAYER_INDEX(),ci_TARGET_PED,iDropOffEntity)
							INCREMENT_MEDAL_OBJECTIVES_COMPLETED(iTeam, iRule)
							MC_playerBD[iPartToUse].iObjectiveTypeCompleted = JOB_COMP_DELIVER_PED
							iBroadcastPriority = iRule
							iBroadcastRevalidate = MC_serverBD_4.iTimesRevalidatedObjectives[iTeam]
							BROADCAST_FMMC_OBJECTIVE_COMPLETE(MC_playerBD[iPartToUse].iObjectiveTypeCompleted, iPartToUse, iPedDeliveredBitset[GET_LONG_BITSET_INDEX(iDropOffEntity)], iRule, MC_serverBD_4.iTimesRevalidatedObjectives[iTeam], iPedDeliveredBitset)
							REINIT_NET_TIMER(tdeventsafetytimer)
							
							REQUEST_INCREMENT_LOCAL_PLAYER_SCORE_FOR_CURRENT_OBJECTIVE(iscoremulti)
							PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE - JOB_COMP_DELIVER_PED + bdrawAirDropOff - Setting MC_playerBD[iPartToUse].iPedCarryCount as zero")
							MC_playerBD[iPartToUse].iPedCarryCount = 0
							CLEAR_BIT(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_WITH_CARRIER)
							CLEAR_BIT(iLocalBoolCheck2, LBOOL2_PED_DROPPED_OFF)
							IF IS_BIT_SET(iLocalBoolCheck2, LBOOL2_BRING_VEHICLE_TO_HALT_CALLED)
								SET_BIT(iLocalBoolCheck4, LBOOL4_DELIVERY_WAIT)
								REINIT_NET_TIMER(tdDeliverTimer)
							ELSE
								NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
							ENDIF
							RESET_NET_TIMER(tdDeliverBackupTimer)
							CLEAR_BIT(iLocalBoolCheck2, LBOOL2_BRING_VEHICLE_TO_HALT_CALLED)
							CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_IN_DROP_OFF)
							PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE - JOB_COMP_DELIVER_PED set locally by part: ",iPartToUse) 
							RETURN TRUE
						ELSE
							IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
							AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwo[iRule], ciBS_RULE2_DONT_GIVE_PLAYER_XP)
								IF iPedDeliveryRPCap < 20
									fDelXp = (100*MC_playerBD[iPartToUse].iPedCarryCount*g_sMPTunables.fxp_tunable_Deliver_a_package_bonus)
									iDelXp = ROUND(fDelXp)
									GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD, LocalPlayerPed,"XPT_MEDIUMT", XPTYPE_COMPLETE, XPCATEGORY_COMPLETE_MISSION_DELIVERED_PED, iDelXp,1)
									iPedDeliveryRPCap++
								ENDIF
							ENDIF
							BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_DELIVER_PED, iscoremulti, iTeam ,-1,INVALID_PLAYER_INDEX(),ci_TARGET_PED,iDropOffEntity)
							MC_playerBD[iPartToUse].iObjectiveTypeCompleted = JOB_COMP_DELIVER_PED
							BROADCAST_FMMC_OBJECTIVE_COMPLETE(MC_playerBD[iPartToUse].iObjectiveTypeCompleted, iPartToUse, iPedDeliveredBitset[GET_LONG_BITSET_INDEX(iDropOffEntity)], iRule, MC_serverBD_4.iTimesRevalidatedObjectives[iTeam], iPedDeliveredBitset)
							iBroadcastPriority = iRule
							iBroadcastRevalidate = MC_serverBD_4.iTimesRevalidatedObjectives[iTeam]
							INCREMENT_MEDAL_OBJECTIVES_COMPLETED(iTeam, iRule)
							REINIT_NET_TIMER(tdeventsafetytimer)
							
							REQUEST_INCREMENT_LOCAL_PLAYER_SCORE_FOR_CURRENT_OBJECTIVE(iscoremulti)
							MC_playerBD[iPartToUse].iPedCarryCount = 0
							PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE - JOB_COMP_DELIVER_PED - Setting MC_playerBD[iPartToUse].iPedCarryCount as zero")
							CLEAR_BIT(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_WITH_CARRIER)
							CLEAR_BIT(iLocalBoolCheck2, LBOOL2_PED_DROPPED_OFF)
							IF IS_BIT_SET(iLocalBoolCheck2, LBOOL2_BRING_VEHICLE_TO_HALT_CALLED)
								SET_BIT(iLocalBoolCheck4, LBOOL4_DELIVERY_WAIT)
								REINIT_NET_TIMER(tdDeliverTimer)
							ELSE
								NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
							ENDIF
							RESET_NET_TIMER(tdDeliverBackupTimer)
							CLEAR_BIT(iLocalBoolCheck2, LBOOL2_BRING_VEHICLE_TO_HALT_CALLED)
							CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_IN_DROP_OFF)
							PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE - JOB_COMP_DELIVER_PED set locally by part: ",iPartToUse)
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ELIF MC_serverBD.iNumPedHighestPriorityHeld[iTeam] > 0 
			AND MC_serverBD.iNumPedHighestPriorityHeld[iTeam] >= MC_serverBD.iRequiredDeliveries[iTeam]
				
				// My team has a ped to deliver, they're not following me. Have I still driven them into a drop-off?
				IF IS_PED_IN_ANY_VEHICLE( LocalPlayerPed )
				AND NOT IS_PED_IN_ANY_HELI(LocalPlayerPed)
				AND NOT IS_PED_IN_ANY_PLANE(LocalPlayerPed)
				
					VEHICLE_INDEX vehDropOff 	= GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
					MODEL_NAMES mDropOff		= GET_ENTITY_MODEL(vehDropOff)
					
					IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iDropOffBitSet,  iRule ) // this is an air drop off
						bdrawAirDropOff = TRUE
					ENDIF
					
					PED_INDEX pedDriving = GET_PED_IN_VEHICLE_SEAT(vehDropOff)
					PED_INDEX pedTemp
					PLAYER_INDEX playerTemp
					PARTICIPANT_INDEX partTemp
					INT ipart
					
					IF pedDriving = LocalPlayerPed
					
						IF IS_PLAYER_IN_DROP_OFF( fradius, GET_DROP_OFF_HEIGHT(iTeam, iRule) )
						
							PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED - In dropoff - ped not following me")
							// Only slow the vehicle down if they're in a vehicle and the dropoff
							// is on the ground
							IF SHOULD_VEH_STOP_ON_DELIVERY()
							AND NOT bdrawAirDropOff
								INT iNumSeats 		= GET_VEHICLE_MODEL_NUMBER_OF_SEATS(mDropOff)
								INT iNumPassengers	= GET_VEHICLE_NUMBER_OF_PASSENGERS(vehDropOff)
								INT iSeatLoop		= 0
								
								#IF IS_DEBUG_BUILD
									IF IS_MODEL_VALID(mDropOff)
										PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED - In dropoff - ped not following me, veh should stop, model = ",mDropOff, " iNumPassengers = ", iNumPassengers, " iNumSeats = ", iNumSeats)
									ENDIF
								#ENDIF
								
								IF iNumPassengers > 0
									FOR iSeatLoop = 0 TO iNumSeats
										IF NOT IS_BIT_SET(iLocalBoolCheck9, LBOOL9_SHOULD_STOP_VEHICLE)
											VEHICLE_SEAT vsTemp = INT_TO_ENUM(VEHICLE_SEAT, iSeatLoop)
											pedTemp = GET_PED_IN_VEHICLE_SEAT(vehDropOff, vsTemp)
											IF pedTemp <> NULL
												IF NOT IS_PED_INJURED(pedTemp)
													IF IS_PED_A_PLAYER(pedTemp)
														playerTemp = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedTemp)
														IF NETWORK_IS_PLAYER_A_PARTICIPANT(playerTemp)
															partTemp = NETWORK_GET_PARTICIPANT_INDEX(playerTemp)
															ipart = NATIVE_TO_INT(partTemp)
															IF MC_playerBD[ipart].iPedCarryCount > 0
																SET_BIT(iLocalBoolCheck9, LBOOL9_SHOULD_STOP_VEHICLE)
																PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED - In dropoff - ped not following me - Will stop car - This player is in my car ", GET_PLAYER_NAME(playerTemp), " They are part ", ipart, " MC_playerBD[ipart].iPedCarryCount = ", MC_playerBD[ipart].iPedCarryCount)
															ENDIF
														ENDIF
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDFOR
								ENDIF
								
							ENDIF
						ELSE
							IF NOT IS_BIT_SET(iLocalBoolCheck9, LBOOL9_SHOULD_STOP_VEHICLE)
								IF NOT IS_BIT_SET(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_FADE_OUT)
									IF NOT IS_BIT_SET(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_DISPLAY)
										PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED Set LBOOL10_DROP_OFF_LOC_DISPLAY (2)")
										SET_BIT(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_DISPLAY)
										CLEAR_BIT(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_FADE_OUT)
									ENDIF
								ENDIF
							ENDIF
							
							FLOAT fTemp
							vThisMarkerLoc = GET_COORDS_FOR_LOCATE_MARKER(fTemp)
							fMarkerDist2 = VDIST2(GET_ENTITY_COORDS(LocalPlayerPed), vThisMarkerLoc)
							
							IF fMarkerDist2 <= (fMarkerDisappear * fMarkerDisappear)
								IF NOT IS_BIT_SET(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_FADE_OUT)
									PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED - Set  LBOOL10_DROP_OFF_LOC_NO_DISPLAY - ped not following me fMarkerDist2 = ", fMarkerDist2, " fMarkerDisappear = ", fMarkerDisappear, " vThisMarkerLoc = ", vThisMarkerLoc)
									SET_BIT(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_FADE_OUT)
									CLEAR_BIT(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_DISPLAY)
								ENDIF
							ELSE
								IF IS_BIT_SET(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_FADE_OUT)
									IF fMarkerDist2 >= ((fMarkerDisappear + 3.0) * (fMarkerDisappear + 3.0))	
										PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED - Set  LBOOL10_DROP_OFF_LOC_DISPLAY - ped not following me, marker had been removed but need to display fMarkerDist2 = ", fMarkerDist2, " fMarkerDisappear = ", (fMarkerDisappear + 3.0), " vThisMarkerLoc = ", vThisMarkerLoc)
										CLEAR_BIT(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_FADE_OUT)
										SET_BIT(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_DISPLAY)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						// I'm not driving the vehicle, and the ai ped isn't following me. Check if the player the ai ped is following is driving my car. 
						IF NOT IS_PED_INJURED(pedDriving)
							
							IF IS_PLAYER_IN_DROP_OFF( fradius, GET_DROP_OFF_HEIGHT(iTeam, iRule) )
								
								PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED - In dropoff - ped not following me - I'm not driving")
								IF IS_PED_A_PLAYER(pedDriving)
									playerTemp = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedDriving)
									IF NETWORK_IS_PLAYER_A_PARTICIPANT(playerTemp)
										partTemp = NETWORK_GET_PARTICIPANT_INDEX(playerTemp)
										ipart = NATIVE_TO_INT(partTemp)
										IF MC_playerBD[ipart].iPedCarryCount > 0
											PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED - In dropoff - ped not following me - I'm not Driving - This player is in Driving ", GET_PLAYER_NAME(playerTemp), " They are part ", ipart, " MC_playerBD[ipart].iPedCarryCount = ", MC_playerBD[ipart].iPedCarryCount)
											IF IS_BIT_SET(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_DISPLAY)
												CLEAR_BIT(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_DISPLAY)
											ENDIF
											IF NOT IS_BIT_SET(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_FADE_OUT)
												PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED SET_LOCATE_TO_FADE_OUT - 4")
												SET_BIT(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_FADE_OUT)
											ENDIF																													
										ENDIF
									ENDIF
								ENDIF
							ELSE
								
								IF NOT IS_BIT_SET(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_DISPLAY)
									IF NOT IS_BIT_SET(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_FADE_OUT)
										PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED Set LBOOL10_DROP_OFF_LOC_DISPLAY (3)")
										SET_BIT(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_DISPLAY)
									ENDIF
								ENDIF
								
								FLOAT fTemp
								vThisMarkerLoc = GET_COORDS_FOR_LOCATE_MARKER(fTemp)
								fMarkerDist2 = VDIST2(GET_ENTITY_COORDS(LocalPlayerPed), vThisMarkerLoc)
								
								IF fMarkerDist2 <= fMarkerDisappear * fMarkerDisappear
									IF NOT IS_BIT_SET(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_FADE_OUT)
										PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED - Set  LBOOL10_DROP_OFF_LOC_FADE_OUT -ped not following me fMarkerDist2 = ", fMarkerDist2, " fMarkerDisappear = ", fMarkerDisappear, " vThisMarkerLoc = ", vThisMarkerLoc)
										SET_BIT(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_FADE_OUT)
										CLEAR_BIT(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_DISPLAY)
									ENDIF
								ELSE
									IF IS_BIT_SET(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_FADE_OUT)
										IF fMarkerDist2 >= ((fMarkerDisappear + 3.0) * (fMarkerDisappear + 3.0))
											PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED - Set  LBOOL10_DROP_OFF_LOC_DISPLAY -  ped not following me - I'm not driving marker had been removed but need to display fMarkerDist2 = ", fMarkerDist2, " fMarkerDisappear = ", (fMarkerDisappear + 3.0), " vThisMarkerLoc = ", vThisMarkerLoc)
											CLEAR_BIT(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_FADE_OUT)
											SET_BIT(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_DISPLAY)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF		
					ENDIF
				ELSE
					IF bGroupDropOff					
						fradius = g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].fDropOffRadius[ iRule ]
						IF fradius > 0
							IF IS_PLAYER_IN_DROP_OFF( fradius, GET_DROP_OFF_HEIGHT(iTeam, iRule))
								PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED - I am in the drop off area.")
								SET_BIT(MC_playerBD[iPartToUse].iClientBitSet2, PBBOOL2_IN_DROP_OFF)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF bGroupDropOff
					fradius = g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].fDropOffRadius[ iRule ]
					IF fradius > 0
						IF IS_PLAYER_IN_DROP_OFF( fradius, GET_DROP_OFF_HEIGHT(iTeam, iRule))
							PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED - I am in the drop off area.")
							SET_BIT(MC_playerBD[iPartToUse].iClientBitSet2, PBBOOL2_IN_DROP_OFF)
						ENDIF
					ENDIF
				ENDIF
			ENDIF // End of ped carrying
			
			IF IS_BIT_SET(iLocalBoolCheck9, LBOOL9_SHOULD_STOP_VEHICLE)
			
				IF IS_BIT_SET(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_DISPLAY)
					CLEAR_BIT(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_DISPLAY)
				ENDIF
				
				IF NOT IS_BIT_SET(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_FADE_OUT)
					IF IS_PLAYER_IN_DROP_OFF( fradius, GET_DROP_OFF_HEIGHT(iTeam, iRule) )
						PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED SET_LOCATE_TO_FADE_OUT - 2")
						SET_BIT(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_FADE_OUT)
					ENDIF
				ENDIF
				
				IF IS_PED_IN_ANY_VEHICLE(localPlayerPed)
					tempVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
					IF IS_VEHICLE_DRIVEABLE(tempVeh)	
						IF GET_PED_IN_VEHICLE_SEAT(tempVeh) = LocalPlayerPed // IS_MY_VEHICLE_STOPPED returns true immediately for non-drivers
							IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(tempVeh)
								NETWORK_REQUEST_CONTROL_OF_ENTITY(tempVeh)
							ELSE
								fStopDist = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].fDropOffStopDistance[iRule]
								IF fStopDist <= 0.0
									fStopDist = DEFAULT_VEH_STOPPING_DISTANCE
								ELIF fStopDist > 20.0
									fStopDist = 20.0
								ENDIF
								PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED - HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE, Trying to stop vehicle, fStopDist = ", fStopDist)
								IF IS_MY_VEHICLE_STOPPED(DEFAULT, fStopDist)
									IF IS_BIT_SET(iLocalBoolCheck10, LBOOL10_DRIVING_AND_CARRYING_PED)
										PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED - HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE,, I've stopped the car, set LBOOL2_PED_DROPPED_OFF")
										SET_BIT(iLocalBoolCheck2,LBOOL2_PED_DROPPED_OFF)
										CLEAR_BIT(iLocalBoolCheck10, LBOOL10_DRIVING_AND_CARRYING_PED)
									ENDIF
									CLEAR_BIT(iLocalBoolCheck9, LBOOL9_SHOULD_STOP_VEHICLE)
									
									//-- For disengaging the handbrake in the instances where I havcen't actually delivered the ped, but I've stopped the car
									SET_BIT(iLocalBoolCheck4, LBOOL4_DELIVERY_WAIT)
									REINIT_NET_TIMER(tdDeliverTimer)	
									
									
									PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED - HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE,, I've stopped the car, clearing LBOOL9_SHOULD_STOP_VEHICLE, set LBOOL4_DELIVERY_WAIT")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF MC_playerBD[ iPartToUse ].iObjCarryCount > 0
			AND MC_serverBD.iNumObjHighestPriorityHeld[iTeam] >= MC_serverBD.iRequiredDeliveries[iTeam]
			AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciPOINTLESS_MODE_ENABLED)
							
				BOOL bValid = TRUE
				IF NOT ARE_ALL_PLAYERS_IN_DROP_OFF_CONDITIONS(iTeam, iRule)
					bValid = FALSE
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iDropOffBitSet,  iRule )
					bdrawAirDropOff = TRUE
				ELSE
					IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
						tempVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
						IF IS_VEHICLE_DRIVEABLE(tempVeh)
							IF IS_ENTITY_IN_AIR(tempVeh)
								bInAir = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].fDropOffStopDistance[iRule] <> 1
					IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].fDropOffPlayerSpeed[iRule] >= 0.0
						IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDropOff_Vehicle[iRule] = -1 
							PRINTLN("[LH][PLAYERSPEED] Changing player speed to ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].fDropOffPlayerSpeed[iRule])
							RESET_PLAYER_STAMINA(LocalPlayer)
							SET_PED_MOVE_RATE_OVERRIDE(LocalPlayerPed, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].fDropOffPlayerSpeed[iRule])
							SET_PED_MOVE_RATE_IN_WATER_OVERRIDE(LocalPlayerPed, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].fDropOffPlayerSpeed[iRule])
						ENDIF
					ENDIF	
				ENDIF
				
				OBJECT_INDEX tempObj = NULL
				
				IF IS_PLAYER_IN_DROP_OFF(fradius, GET_DROP_OFF_HEIGHT(iTeam, iRule))
					PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE OBJ - In dropoff")
					SET_BIT(MC_playerBD[iPartToUse].iClientBitSet2, PBBOOL2_IN_DROP_OFF)
					
					IF IS_BIT_SET(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_DISPLAY)
						PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE OBJ - In dropoff - clear LBOOL10_DROP_OFF_LOC_DISPLAY")
						CLEAR_BIT(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_DISPLAY)
					ENDIF
					
					IF NOT IS_BIT_SET(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_FADE_OUT)
						PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE OBJ - In dropoff - set LBOOL10_DROP_OFF_LOC_FADE_OUT")
						SET_BIT(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_FADE_OUT)
					ENDIF
					
					FOR i = 0 TO (MC_serverBD.iNumObjCreated - 1)
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GET_OBJECT_NET_ID(i))
							tempObj = NET_TO_OBJ(GET_OBJECT_NET_ID(i))
							
							IF HAS_DELIVER_OBJECT_RULE_FINISHED(i)
								PRINTLN( "[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE OBJ - Finished dropping " )
								
								IF NOT IS_OBJECT_A_CONTAINER(tempObj)
									IF NOT bInAir
										IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitSetSix[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE6_GET_DELIVER_DONT_DROP_ON_DELIVER)
											CLEAR_BIT(iObjCollectHUDTriggeredBS, i)
										ENDIF
										
										SET_BIT(iObjDeliveredBitset, i)
										
										IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitSetSix[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE6_GET_DELIVER_DONT_DROP_ON_DELIVER)
											IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectBitSet, cibsOBJ_InvisibleOnFirstDelivery)
												IF SHOULD_OBJ_CLEAN_UP_ON_DELIVERY(i,  iTeam)
													IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(GET_OBJECT_NET_ID(i))
														HIDE_PORTABLE_PICKUP_WHEN_DETACHED(tempObj, TRUE) 
														PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE OBJ - HIDE_PORTABLE_PICKUP_WHEN_DETACHED")
													ENDIF
												ENDIF
											ENDIF

											IF IS_ENTITY_ATTACHED_TO_ANY_PED(tempObj)//Stop the assertFAILED: DETACH_PORTABLE_PICKUP_FROM_PED - the pickup is not attached
												DETACH_PORTABLE_PICKUP_FROM_PED(tempObj)
											ENDIF
											PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE OBJ - Dropped object off - Detached pickup")
											
											IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectBitSet, cibsOBJ_InvisibleOnFirstDelivery)
												IF SHOULD_OBJ_CLEAN_UP_ON_DELIVERY(i,  iTeam)
													SET_ENTITY_ALPHA(tempObj, 0, FALSE)
													PRINTLN( "[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE OBJ - NO ALPHA!")
													SET_ENTITY_VISIBLE(tempObj, FALSE)
													SET_ENTITY_COLLISION(tempObj, FALSE)
													
													PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE OBJ - Dropped object off - Setting invisible; SHOULD_OBJ_CLEAN_UP_ON_DELIVERY ",SHOULD_OBJ_CLEAN_UP_ON_DELIVERY(i, iTeam),", cibsOBJ_InvisibleOnFirstDelivery ",IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectBitSet, cibsOBJ_InvisibleOnFirstDelivery))
												ENDIF
											ENDIF
											
											IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_TRANSFORM_ASSETS)
											AND MC_serverBD.iObjCarrier[i] = iLocalPart
											AND (shouldCleanupObjectTransformationNow != NULL AND CALL shouldCleanupObjectTransformationNow())
												IF cleanupObjectTransformations != NULL
													CALL cleanupObjectTransformations()
												ENDIF
											ENDIF
											IF MC_serverBD.iObjCarrier[i] = iLocalPart
												CLEANUP_OBJECT_INVENTORIES()
											ENDIF
											
											SET_TEAM_PICKUP_OBJECT( tempObj,  iTeam , FALSE)
										ELSE
											PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE OBJ - ciBS_RULE6_GET_DELIVER_DONT_DROP_ON_DELIVER IS SET - OBJECT WILL NOT BE DROPPED BY THE PLAYER")
										ENDIF
										
										iDropOffEntity = i
										iscoremulti++
										bvalidreward = TRUE
									ENDIF
								ELSE
									IF IS_VEHICLE_DRIVEABLE(tempVeh)
										CLEAR_BIT(iObjCollectHUDTriggeredBS, i)
										SET_BIT(iObjDeliveredBitset, i)
										IF IS_VEHICLE_MODEL_A_CARGOBOB(GET_ENTITY_MODEL(tempVeh))
											DETACH_VEHICLE_FROM_CARGOBOB(tempVeh, tempVeh) //detach anything from this cargobob
											SET_CARGOBOB_PICKUP_MAGNET_ACTIVE(tempVeh, FALSE)
											PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE OBJ - detach being called for cargobob on crate: ",i) 
										ELIF GET_ENTITY_MODEL(tempVeh) = HANDLER
											DETACH_CONTAINER_FROM_HANDLER_FRAME(tempVeh)
										ELSE
											SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(tempVeh,TRUE)
											
											FORCE_EVERYONE_FROM_MY_CAR(TRUE,TRUE)
										ENDIF
										iDropOffEntity = i
										iscoremulti++
										bvalidreward = TRUE
										SET_BIT(iLocalBoolCheck2, LBOOL2_USE_CRATE_DELIVERY_DELAY)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDFOR
					IF bvalidreward 
					AND bValid
						INT iShardOption = ciTICKER_ONLY
						
						BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_DELIVER_OBJ, iscoremulti, iTeam ,-1,INVALID_PLAYER_INDEX(),ci_TARGET_OBJECT,iDropOffEntity,TRUE,iShardOption)
						IF  iRule  <  MC_serverBD.iMaxObjectives[ iTeam ] 
						OR MC_ServerBD.iNumObjHighestPriority[ iTeam ] > 1
						OR g_FMMC_STRUCT_ENTITIES.sPlacedObject[iDropOffEntity].sObjBlipStruct.iBlipStyle = FMMC_OBJECTBLIP_CTF
							IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
							AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwo[iRule], ciBS_RULE2_DONT_GIVE_PLAYER_XP)
								IF iPackageDelXPCount < 10

									fDelXp = (100*MC_playerBD[iPartToUse].iObjCarryCount*g_sMPTunables.fxp_tunable_Deliver_a_package_bonus)
									iDelXp = ROUND(fDelXp)
									GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD, LocalPlayerPed,"XPT_MEDIUMT", XPTYPE_COMPLETE, XPCATEGORY_COMPLETE_MISSION_DELIVERED_OBJ, iDelXp,1)
									#IF IS_DEBUG_BUILD
										iDebugXPDelObj= iDebugXPDelObj + iDelXp
									#ENDIF
									iPackageDelXPCount = iPackageDelXPCount + MC_playerBD[iPartToUse].iObjCarryCount
								ENDIF

							ENDIF
						ELSE
							IF iPackageDelXPCount < 10
								iDelayedDeliveryXPToAward++
							ENDIF
						ENDIF
						
						MC_playerBD[iPartToUse].iObjectiveTypeCompleted = JOB_COMP_DELIVER_OBJ
						IF IS_BIT_SET(iLocalBoolCheck9, LBOOL9_SHOULD_STOP_VEHICLE)
							CLEAR_BIT(iLocalBoolCheck9, LBOOL9_SHOULD_STOP_VEHICLE)
							PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED LBOOL9_SHOULD_STOP_VEHICLE was set, clearing...")
						ENDIF
						
						iBroadcastPriority = iRule
						iBroadcastRevalidate = MC_serverBD_4.iTimesRevalidatedObjectives[iTeam]
						BROADCAST_FMMC_OBJECTIVE_COMPLETE(MC_playerBD[iPartToUse].iObjectiveTypeCompleted, iPartToUse, iObjDeliveredBitset, iRule, MC_serverBD_4.iTimesRevalidatedObjectives[iTeam], iBitsetArrayBlank)
						REINIT_NET_TIMER(tdeventsafetytimer)
						
						REQUEST_INCREMENT_LOCAL_PLAYER_SCORE_FOR_CURRENT_OBJECTIVE(iscoremulti)
						INCREMENT_MEDAL_OBJECTIVES_COMPLETED(iTeam, iRule)
						
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_CTF_SCREEN_FILTER_ON_HOLDING_FLAG)
						AND NOT IS_BIT_SET(iLocalBoolCheck29, LBOOL29_DO_DELIVER_FX)
							SET_BIT(iLocalBoolCheck29, LBOOL29_DO_DELIVER_FX)
							PRINTLN("HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE - Setting LBOOL29_DO_DELIVER_FX")
						ENDIF
						
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciENABLE_IN_AND_OUT_SOUNDS)
							BROADCAST_IN_AND_OUT_SFX_PLAY(ciIN_AND_OUT_SOUND_DELIVERED, iTeam)
							SET_BIT(iLocalBoolCheck16, LBOOL16_IAO_BLOCK_PACKAGE_SOUNDS)
							REINIT_NET_TIMER(stBlockPackageSoundsTimer)
							
							CLEAR_BIT(iLocalBoolCheck16, LBOOL16_HAS_PLAYED_IAO_PICKUP_SOUND)
						ENDIF
						
						CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_IN_DROP_OFF)
						CLEAR_BIT(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_WITH_CARRIER)
						MC_playerBD[iPartToUse].iObjCarryCount = 0
						
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_TRANSFORM_ASSETS)
						AND (shouldCleanupObjectTransformationNow != NULL AND CALL shouldCleanupObjectTransformationNow())
							IF cleanupObjectTransformations != NULL
								CALL cleanupObjectTransformations()
							ENDIF
						ENDIF
						CLEANUP_OBJECT_INVENTORIES()

						PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE - JOB_COMP_DELIVER_OBJ set locally by part: ",iPartToUse) 
						RETURN TRUE
					ENDIF
				ELSE
					IF NOT IS_BIT_SET(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_FADE_OUT)
						IF NOT IS_BIT_SET(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_DISPLAY)
							PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE OBJ Set LBOOL10_DROP_OFF_LOC_DISPLAY")
							SET_BIT(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_DISPLAY)
							CLEAR_BIT(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_FADE_OUT)
						ENDIF
					ENDIF
					
					FLOAT fTemp
					vThisMarkerLoc = GET_COORDS_FOR_LOCATE_MARKER(fTemp)
					fMarkerDist2 = VDIST2(GET_ENTITY_COORDS(LocalPlayerPed), vThisMarkerLoc)

					IF fMarkerDist2 <= (fMarkerDisappear * fMarkerDisappear)
						IF NOT IS_BIT_SET(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_FADE_OUT)
							PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE OBJ - LBOOL10_DROP_OFF_LOC_FADE_OUT -Obj drop off fMarkerDist2 = ", fMarkerDist2, " fMarkerDisappear = ", fMarkerDisappear, " vThisMarkerLoc = ", vThisMarkerLoc)
							SET_BIT(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_FADE_OUT)
							CLEAR_BIT(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_DISPLAY)
						ENDIF
					ELSE
						
						IF IS_BIT_SET(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_FADE_OUT)
							IF fMarkerDist2 >= ((fMarkerDisappear + 3.0) * (fMarkerDisappear + 3.0))	
								PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE OBJ - Set  LBOOL10_DROP_OFF_LOC_DISPLAY - Obj drop off- had been removed but need to display fMarkerDist2 = ", fMarkerDist2, " fMarkerDisappear = ", (fMarkerDisappear + 3.0), " vThisMarkerLoc = ", vThisMarkerLoc)
								CLEAR_BIT(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_FADE_OUT)
								SET_BIT(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_DISPLAY)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF MC_playerBD[iPartToUse].iVehCarryCount > 0
			AND MC_serverBD.iNumVehHighestPriorityHeld[iTeam] >= MC_serverBD.iRequiredDeliveries[iTeam]
				BOOL bValid = TRUE
				IF NOT ARE_ALL_PLAYERS_IN_DROP_OFF_CONDITIONS(iTeam, iRule)
					bValid = FALSE
				ENDIF
				
				IF MC_playerBD[iPartToUse].iVehDeliveryId = -1
					
					IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
					
						tempVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
						
						IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iDropOffBitSet,  iRule )
							bdrawAirDropOff = TRUE
						ENDIF
						
						IF IS_PLAYER_IN_DROP_OFF(fradius, GET_DROP_OFF_HEIGHT(iTeam, iRule))
						AND IS_TEAM_IN_VEHICLE_ACCURATE(iRule, iTeam)
						
							#IF IS_DEBUG_BUILD
							IF bPlaneDropoffPrints
								PRINTLN("[RCC MISSION][tom] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE - 2 - In drop off!")
							ENDIF
							#ENDIF
							
							IF IS_BIT_SET(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_DISPLAY)
								CLEAR_BIT(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_DISPLAY)
							ENDIF
							
							IF NOT IS_BIT_SET(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_FADE_OUT)
								SET_BIT(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_FADE_OUT)
								PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE VEH SET_LOCATE_TO_FADE_OUT - 3, radius = ", g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].fDropOffRadius[ iRule ] )
							
								IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThree[iRule], ciBS_RULE3_ALL_IN_SEPARATE_VEHICLES)
								OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetNine[iRule], ciBS_RULE9_PROGRESS_GAD_AT_SAME_TIME)
									IF NOT IS_BIT_SET(iLocalBoolCheck2,LBOOL2_REQUEST_UPDATE)
										CLEAR_BIT(iLocalBoolCheck2,LBOOL2_WHOLE_UPDATE_DONE)
										SET_BIT(iLocalBoolCheck2,LBOOL2_REQUEST_UPDATE)
										PRINTLN("[JS] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE requesting update new vehnear(objectivecomp) = ", MC_playerBD[iPartToUse].iVehNear)
									ENDIF
								ENDIF
							ENDIF
				
							SET_BIT(MC_playerBD[ iPartToUse ].iClientBitSet2, PBBOOL2_IN_DROP_OFF)
							
							GET_VEHICLE_TRAILER_VEHICLE(tempVeh,trailerVeh)
							IF IS_VEHICLE_MODEL_A_CARGOBOB(GET_ENTITY_MODEL(tempVeh))
								GET_CURRENT_WINCHED_VEHICLE(tempVeh, Winchveh, TRUE)
								bcargobobdrop = TRUE
							ELSE
								IF IS_BIT_SET(iLocalBoolCheck,LBOOL_CARGOBOB_DROP)
									CLEAR_BIT(iLocalBoolCheck,LBOOL_CARGOBOB_DROP)
									PRINTLN("HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE - Clearing LBOOL_CARGOBOB_DROP because we're not in a cargobob any more")
								ENDIF
							ENDIF
							
							IF bValid
								#IF IS_DEBUG_BUILD
								IF bPlaneDropoffPrints
									PRINTLN("[RCC MISSION][tom] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE -1 - This isn't a cargobob dropoff")
								ENDIF
								#ENDIF
								
								IF NOT bdrawAirDropOff
									
									#IF IS_DEBUG_BUILD
									IF bPlaneDropoffPrints
										PRINTLN("[RCC MISSION][tom] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE 0 - This isn't an air dropoff")
									ENDIF
									#ENDIF
									
									IF IS_VEHICLE_DRIVEABLE(tempVeh)
										
										#IF IS_DEBUG_BUILD
										IF bPlaneDropoffPrints
											PRINTLN("[RCC MISSION][tom] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE 1 - The dropoff vehicle is driveable")
										ENDIF
										#ENDIF
										
										IF IS_PED_IN_ANY_HELI(LocalPlayerPed)
										OR (IS_PED_IN_ANY_PLANE(LocalPlayerPed) AND NOT IS_PED_IN_ANY_SEAPLANE_ON_WATER(LocalPlayerPed))
										
											
											#IF IS_DEBUG_BUILD
											IF bPlaneDropoffPrints
												PRINTLN("[RCC MISSION][tom] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE 2 - We're in a helicopter or plane, speed = ",GET_ENTITY_SPEED(tempVeh))
											ENDIF
											#ENDIF
											
											IF (GET_ENTITY_SPEED(tempVeh) < 5.0 
											OR NOT IS_ENTITY_IN_AIR(tempVeh))
												
												#IF IS_DEBUG_BUILD
												IF bPlaneDropoffPrints
													PRINTLN("[RCC MISSION][tom] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE 3 - We're slow enough or not in the air, my landing gear state = ",ENUM_TO_INT(GET_LANDING_GEAR_STATE(tempVeh)),", on all wheels = ",IS_VEHICLE_ON_ALL_WHEELS(tempVeh))
												ENDIF
												#ENDIF
												
												MODEL_NAMES mTempVeh = GET_ENTITY_MODEL(tempVeh)
												
												IF NOT IS_THIS_MODEL_A_PLANE(mTempVeh)
												OR (IS_THIS_MODEL_A_PLANE(mTempVeh) 
													AND ((GET_LANDING_GEAR_STATE(tempVeh) = LGS_BROKEN
													OR NOT IS_PLANE_LANDING_GEAR_INTACT(tempVeh))
													OR((GET_LANDING_GEAR_STATE(tempVeh) = LGS_LOCKED_DOWN) 
														AND IS_VEHICLE_ON_ALL_WHEELS(tempVeh))))
													
													#IF IS_DEBUG_BUILD
													IF bPlaneDropoffPrints
														PRINTLN("[RCC MISSION][tom] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE 4 - We're on all wheels, set bstopped = TRUE")
													ENDIF
													#ENDIF
													
													IF NETWORK_HAS_CONTROL_OF_ENTITY(tempVeh)
														
														#IF IS_DEBUG_BUILD
														IF bPlaneDropoffPrints
															PRINTLN("[RCC MISSION][tom] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE 5 - We have control of the vehicle")
														ENDIF
														#ENDIF
														
														SET_VEHICLE_HANDBRAKE(tempVeh,TRUE)
														
														IF NOT IS_BIT_SET(iLocalBoolCheck2, LBOOL2_BRING_VEHICLE_TO_HALT_CALLED)
															
															#IF IS_DEBUG_BUILD
															IF bPlaneDropoffPrints
																PRINTLN("[RCC MISSION][tom] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE 6 - The vehicle hasn't yet been called to a halt")
															ENDIF
															#ENDIF
															
															IF NOT IS_ENTITY_IN_AIR(tempVeh)
																#IF IS_DEBUG_BUILD
																IF bPlaneDropoffPrints
																	PRINTLN("[RCC MISSION][tom] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE 7 - We're not in the air, set the 'bring to halt called' bit")
																ENDIF
																#ENDIF
																SET_BIT(iLocalBoolCheck2, LBOOL2_BRING_VEHICLE_TO_HALT_CALLED)
															ENDIF
														ENDIF
													ENDIF
													bstopped = TRUE
												ELSE
													// in a plane, but landing gear not down
													CLEAR_BIT(MC_playerBD[ iPartToUse ].iClientBitSet2, PBBOOL2_IN_DROP_OFF)
												ENDIF
											ENDIF
										ELSE
											IF NETWORK_HAS_CONTROL_OF_ENTITY(tempVeh)
												IF NOT IS_PED_IN_ANY_BOAT(LocalPlayerPed)
												AND GET_ENTITY_MODEL(tempVeh) != SUBMERSIBLE
												AND GET_ENTITY_MODEL(tempVeh) != STROMBERG
													IF NOT IS_BIT_SET(iLocalBoolCheck2, LBOOL2_BRING_VEHICLE_TO_HALT_CALLED)
														SET_BIT(iLocalBoolCheck2, LBOOL2_BRING_VEHICLE_TO_HALT_CALLED)
													ENDIF
												ENDIF
											ENDIF
											bstopped = TRUE
										ENDIF
									ELSE
										bstopped = TRUE
									ENDIF
								ENDIF
							ENDIF
							IF bstopped
							OR bdrawAirDropOff
							OR bcargobobdrop
																
								PRINTLN("[RCC MISSION][tom] A - HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE - bstopped is true (or this is an air dropoff or cargobob dropoff)")
								
								FOR i = 0 TO (MC_serverBD.iNumVehCreated - 1)
									
									PRINTLN("[RCC MISSION][tom] B - HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE -  checking vehicle ",i)
									
									IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[i])
									AND IS_VEHICLE_DRIVEABLE(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i]))
										
										PRINTLN("[RCC MISSION][tom] C - HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE -  vehicle ",i," exists and is driveable")
										
										IF NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i]) = tempVeh
										OR NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i]) = trailerVeh
										OR NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i]) = WinchVeh
											
											PRINTLN("[RCC MISSION][tom] D - HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE -  vehicle ",i," is the dropoff vehicle (it's tempVeh, trailerVeh or WinchVeh)")
																					
											IF GET_PED_IN_VEHICLE_SEAT(tempVeh) = LocalPlayerPed
											AND IS_PED_SITTING_IN_ANY_VEHICLE(LocalPlayerPed)
											AND GET_SCRIPT_TASK_STATUS(LocalPlayerPed, SCRIPT_TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT) != PERFORMING_TASK
											AND GET_SCRIPT_TASK_STATUS(LocalPlayerPed, SCRIPT_TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT) != WAITING_TO_START_TASK
											AND GET_SCRIPT_TASK_STATUS(LocalPlayerPed, SCRIPT_TASK_LEAVE_VEHICLE) != PERFORMING_TASK
											AND GET_SCRIPT_TASK_STATUS(LocalPlayerPed, SCRIPT_TASK_LEAVE_VEHICLE) != WAITING_TO_START_TASK
											AND NOT GET_PED_RESET_FLAG(LocalPlayerPed, PRF_IsSeatShuffling) 
											
												PRINTLN("[RCC MISSION][tom] E - HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE -  I'm in the driver's seat")
												
												IF (MC_serverBD_4.ivehRule[i][iTeam] = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER
												OR MC_serverBD_4.ivehRule[i][iTeam] = FMMC_OBJECTIVE_LOGIC_GET_AND_HOLD)
												AND ARE_ALL_PLAYERS_IN_SEPARATE_VEHICLES_AND_LOCATE(iRule, iTeam)
												AND ARE_ALL_PLAYERS_IN_UNIQUE_VEHICLES_AND_LOCATE(iRule, iTeam)
												
													// block the transition for avenger
													IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTen[iRule], ciBS_RULE10_PROGRESS_RULE_ALL_DRIVER_ACCEL)
														IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_REQUEST_AVENGER_HOLD_TRANSITION_BLOCK)
															REQUEST_AVENGER_HOLD_TRANSITION_BLOCKED(TRUE)
														ENDIF
													ENDIF
													
													IF IS_AVENGER_HOLD_TRANSITION_BLOCKED_AND_INACTIVE(iRule, iTeam) // holds up waiting for all machines to block avenger transitions before allowing progression
													AND IS_DRIVER_PRESSING_ACCELERATE(iRule, iTeam)
														PRINTLN("[RCC MISSION][tom] F - HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE -  This vehicle is on a get & deliver (or collect and hold)")
														
														IF MC_serverBD_4.iVehPriority[i][ iTeam ] < FMMC_MAX_RULES
															
															PRINTLN("[RCC MISSION][tom] G - HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE -  This vehicle has a priority less than max rules")
															
															IF bdrawAirDropOff
															OR bcargobobdrop
															OR (NOT IS_PED_IN_ANY_HELI(LocalPlayerPed))
															OR (NOT IS_ENTITY_IN_AIR(tempVeh))
																
																PRINTLN("[RCC MISSION][tom] H - HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE -  This vehicle is either: bdrawAirDropOff, bcargobobdrop, dropoff to cargobob, not a heli, or landed")
																															
																MC_playerBD[iPartToUse].iVehNear = -1
																
																#IF IS_DEBUG_BUILD
																	IF bInVehicleDebug
																		PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH (-1 CHECK) (2) Setting MC_playerBD[iPartToUse].iVehNear = -1 for part ",iPartToUse, " who is player ",  GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPartToUse))))
																	ENDIF
																#ENDIF
																DeliveryVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i])
																
																IF NOT (bdrawAirDropOff OR bcargobobdrop)
																	IF IS_PED_IN_ANY_HELI(LocalPlayerPed)
																		IF NOT IS_ENTITY_IN_AIR(tempVeh)
																			MC_playerBD[iPartToUse].iVehDeliveryId = i
																			SET_BIT(MC_playerBD[ iPartToUse ].iClientBitSet2, PBBOOL2_DROPPING_OFF)
																		ENDIF
																	ELSE
																		MC_playerBD[iPartToUse].iVehDeliveryId = i
																		SET_BIT(MC_playerBD[ iPartToUse ].iClientBitSet2, PBBOOL2_DROPPING_OFF)
																	ENDIF
																	
																ELSE
																	MC_playerBD[iPartToUse].iVehDeliveryId = i
																ENDIF
																
																IF NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i]) = WinchVeh
																	SET_BIT(iLocalBoolCheck,LBOOL_CARGOBOB_DROP)
																ENDIF
																PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE VEH - Setting vehicle as ready for deliver: ", MC_playerBD[iPartToUse].iVehDeliveryId) 
																
																IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetNine[iRule], ciBS_RULE9_PROGRESS_GAD_AT_SAME_TIME)
																	PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE VEH - SETTING LBOOL24_BLOCK_OBJECTIVE_TEXT_UNTIL_RULE_CHANGE") 
																	SET_BIT(iLocalBoolCheck24, LBOOL24_BLOCK_OBJECTIVE_TEXT_UNTIL_RULE_CHANGE)
																ENDIF
																
																//Break out!
																i = MC_serverBD.iNumVehCreated
																
															ENDIF
														ENDIF
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDFOR
							ENDIF
						ELSE
							IF NOT IS_BIT_SET(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_FADE_OUT)
								IF NOT IS_BIT_SET(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_DISPLAY)
									PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE VEH Set LBOOL10_DROP_OFF_LOC_DISPLAY")
									SET_BIT(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_DISPLAY)
									CLEAR_BIT(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_FADE_OUT)
								ENDIF
							ENDIF
							
							FLOAT fTemp
							vThisMarkerLoc = GET_COORDS_FOR_LOCATE_MARKER(fTemp)
							fMarkerDist2 = VDIST2(GET_ENTITY_COORDS(LocalPlayerPed), vThisMarkerLoc)

							IF fMarkerDist2 <= (fMarkerDisappear * fMarkerDisappear)
								IF NOT IS_BIT_SET(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_FADE_OUT)
									PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE VEH - LBOOL10_DROP_OFF_LOC_FADE_OUT -Veh drop off fMarkerDist2 = ", fMarkerDist2, " fMarkerDisappear = ", fMarkerDisappear, " vThisMarkerLoc = ", vThisMarkerLoc)
									SET_BIT(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_FADE_OUT)
									CLEAR_BIT(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_DISPLAY)
								ENDIF
							ELSE
								
								IF IS_BIT_SET(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_FADE_OUT)
									IF fMarkerDist2 >= ((fMarkerDisappear + 3.0) * (fMarkerDisappear + 3.0))	
										PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE VEH - Set  LBOOL10_DROP_OFF_LOC_DISPLAY - Veh drop off- had been removed but need to display fMarkerDist2 = ", fMarkerDist2, " fMarkerDisappear = ", (fMarkerDisappear + 3.0), " vThisMarkerLoc = ", vThisMarkerLoc)
										CLEAR_BIT(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_FADE_OUT)
										SET_BIT(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_DISPLAY)
									ENDIF
								ENDIF
							ENDIF
							
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		IF NOT bvalidreward
			IF MC_playerBD[iPartToUse].iVehDeliveryId != -1
				
				IF IS_VEHICLE_DRIVEABLE(DeliveryVeh)
				AND NOT bHasVehDropoffChanged
					
					IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iDropOffBitSet,  iRule )
						bdrawAirDropOff = TRUE
					ENDIF
					
					IF MC_serverBD_4.ivehRule[MC_playerBD[iPartToUse].iVehDeliveryId][ iTeam ] = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER
						bcollect = TRUE
					ENDIF
					
					IF bdrawAirDropOff
						IF bcollect
							bvalidreward = TRUE
						ELSE
							DeliveryVeh = NULL
							MC_playerBD[iPartToUse].iVehDeliveryId = -1
							MC_playerBD[iPartToUse].iVehCarryCount = 0
						ENDIF
					ELSE
						IF IS_BIT_SET(iLocalBoolCheck,LBOOL_CARGOBOB_DROP)
							IF bcollect
								IF SHOULD_LOCK_VEHICLE_ON_DELIVERY(MC_playerBD[iPartToUse].iVehDeliveryId, iTeam )
									SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS( DeliveryVeh, TRUE )
									IF NETWORK_HAS_CONTROL_OF_ENTITY(DeliveryVeh)
										LOCK_DOORS_WHEN_NO_LONGER_NEEDED(DeliveryVeh)
									ENDIF									
									PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE VEH - CARGOBOB DELIVERY -LOCK VEH DOORS CALLED ON VEH: ",MC_playerBD[iPartToUse].iVehDeliveryId) 
								ENDIF
								DETACH_VEHICLE_FROM_ANY_CARGOBOB(DeliveryVeh)
								bvalidreward = TRUE
								CLEAR_BIT(iLocalBoolCheck,LBOOL_CARGOBOB_DROP)
							ELSE
								CLEAR_BIT(iLocalBoolCheck,LBOOL_CARGOBOB_DROP)
								DeliveryVeh = NULL
								MC_playerBD[iPartToUse].iVehDeliveryId =-1
								MC_playerBD[iPartToUse].iVehCarryCount = 0
							ENDIF
						ELSE
							fStopDist = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].fDropOffStopDistance[iRule]
							IF fStopDist <= 0.0
								fStopDist = DEFAULT_VEH_STOPPING_DISTANCE
							ELIF fStopDist > 20.0
								fStopDist = 20.0
							ENDIF
							
							PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE VEH - HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE, Trying to stop vehicle, fStopDist = ", fStopDist)
							
							IF NOT SHOULD_VEHICLE_STOP_OR_BRING_TO_HALT_WAIT()
							AND (NOT SHOULD_VEH_STOP_ON_DELIVERY() OR IS_MY_VEHICLE_STOPPED(DEFAULT, fStopDist))
								SET_BIT(MC_playerBD[ iPartToUse ].iClientBitSet2, PBBOOL2_IN_DROP_OFF)
								IF NOT HAS_NET_TIMER_STARTED(tdDeliverTimer)
									REINIT_NET_TIMER(tdDeliverTimer)
								ELSE
									IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdDeliverTimer) > 500
										IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
											GET_VEHICLE_TRAILER_VEHICLE(GET_VEHICLE_PED_IS_IN(LocalPlayerPed),trailerVeh)
										ENDIF
										IF DeliveryVeh != trailerVeh
											IF FMMC_FORCE_EVERYONE_FROM_VEHICLE(MC_playerBD[iPartToUse].iVehDeliveryId)
												IF IS_VEHICLE_DRIVEABLE(DeliveryVeh)
													IF NETWORK_HAS_CONTROL_OF_ENTITY(DeliveryVeh)
														IF SHOULD_STAY_IN_ON_DELIVERY()
															SET_VEHICLE_HANDBRAKE(DeliveryVeh,FALSE) 
														ENDIF
													ENDIF
												ENDIF
												
												//-- Only turn player control back on if it's not the last rule (2150083)
												//-- Otherwise players can drive around before the mission ends.
												IF SHOULD_STAY_IN_ON_DELIVERY()
													PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE VEH - SHOULD_STAY_IN_ON_DELIVERY check veh:", MC_playerBD[iPartToUse].iVehDeliveryId, " iRule = ", iRule , " iMaxObjectives = ", MC_serverBD.iMaxObjectives[iTeam]  ," iNumVehHighestPriority = ",MC_ServerBD.iNumVehHighestPriority[ iTeam ])
													
													IF SHOULD_TURN_PLAYER_CONTROL_ON_AFTER_DELIVERY()
														PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE VEH - Turning control back on after veh delivery (1) veh:", MC_playerBD[iPartToUse].iVehDeliveryId)
														NET_SET_PLAYER_CONTROL(LocalPlayer,TRUE)
														
														
														iTimeToStop = g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iDropOffStopTimer[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]]
														IF iTimeToStop > 0
															PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE VEH Will stop vehicle for duration veh: ",  MC_playerBD[iPartToUse].iVehDeliveryId, " Time: ", iTimeToStop, " Team: ", MC_playerBD[iPartToUse].iteam, " Rule: ", MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam])
															
															INT iMissionEntity = IS_ENTITY_A_MISSION_CREATOR_ENTITY(DeliveryVeh)
															IF iMissionEntity > -1
																PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE (1a) Setting iVeh: ", MC_playerBD[iPartToUse].iVehDeliveryId, " BROADCAST_FMMC_DELIVER_VEHICLE_STOP")
																BROADCAST_FMMC_DELIVER_VEHICLE_STOP(MC_playerBD[iPartToUse].iVehDeliveryId, iLocalPart, iTimeToStop)
															ENDIF
															
															SET_BIT(iLocalBoolCheck24, LBOOL24_BLOCK_VEHICLE_KERS_ON_RULE_CHECK)
															PRINTLN("[RCC MISSION] LBOOL24_BLOCK_VEHICLE_KERS_ON_RULE_CHECK setting")
														ELIF iTimeToStop = -1
															PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE VEH Will stop vehicle forever veh: ",  MC_playerBD[iPartToUse].iVehDeliveryId, " Time: ", iTimeToStop, " Team: ", MC_playerBD[iPartToUse].iteam, " Rule: ", MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam])
															
															SET_BIT(iLocalBoolCheck10, LBOOL10_STOP_CAR_FOREVER)					
															INT iMissionEntity = IS_ENTITY_A_MISSION_CREATOR_ENTITY(DeliveryVeh)
															IF iMissionEntity > -1
																PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE (1b) Setting iVeh: ", MC_playerBD[iPartToUse].iVehDeliveryId, " BROADCAST_FMMC_DELIVER_VEHICLE_STOP")
																BROADCAST_FMMC_DELIVER_VEHICLE_STOP(MC_playerBD[iPartToUse].iVehDeliveryId, iLocalPart, iTimeToStop)
															ENDIF																																
															
															SET_BIT(iLocalBoolCheck24, LBOOL24_BLOCK_VEHICLE_KERS_ON_RULE_CHECK)
															PRINTLN("[RCC MISSION] LBOOL24_BLOCK_VEHICLE_KERS_ON_RULE_CHECK setting")
														ENDIF
													ELSE
														PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE VEH - Not turning control back on after veh delivery veh:", MC_playerBD[iPartToUse].iVehDeliveryId)
														IF IS_NEXT_RULE_A_CUTSCENE()
															NET_SET_PLAYER_CONTROL(LocalPlayer,FALSE)
														ENDIF
													ENDIF
												ELSE
													PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE VEH - Turning control back on after veh delivery (2) veh:", MC_playerBD[iPartToUse].iVehDeliveryId)
													NET_SET_PLAYER_CONTROL(LocalPlayer,TRUE)
												ENDIF
												
												RESET_NET_TIMER(tdDeliverBackupTimer)
												
												IF SHUT_VEHICLE_DOORS(DeliveryVeh)
													PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE VEH - shut door true")
													
													IF bcollect
														bvalidreward = TRUE
														IF IS_VEHICLE_DRIVEABLE(DeliveryVeh)
														AND SHOULD_LOCK_VEHICLE_ON_DELIVERY(MC_playerBD[iPartToUse].iVehDeliveryId, iTeam )
															BROADCAST_FMMC_EVENT_FOR_INSTANCED_CONTENT(iLocalPart, g_ciInstancedContentEventType_AllPlayersToggleConsiderVehicle, MC_playerBD[iPartToUse].iVehDeliveryId)
															SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS( DeliveryVeh, TRUE )
															SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(DeliveryVeh, FALSE)
															LOCK_DOORS_WHEN_NO_LONGER_NEEDED( DeliveryVeh )
															CLEAR_LAST_DRIVEN_VEHICLE()
															PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE VEH - LOCK VEH DOORS CALLED ON VEH: ",MC_playerBD[iPartToUse].iVehDeliveryId)
														ENDIF
													ELSE
														DeliveryVeh = NULL
														MC_playerBD[iPartToUse].iVehDeliveryId = -1
														MC_playerBD[iPartToUse].iVehCarryCount = 0
													ENDIF
												ENDIF
											ELSE
												DISABLE_CELLPHONE_CAMERA_APP_THIS_FRAME_ONLY()
											ENDIF
										ELSE
											IF SHOULD_VEH_STOP_ON_DELIVERY() //Stops trailer from stopping instantly if we want to keep it
												IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
													
													SET_BIT(iLocalBoolCheck4, LBOOL4_DELIVERY_WAIT)
													REINIT_NET_TIMER(tdDeliverTimer)
													
													//-- Only detach the trailer if it's not the last rule (2156538)
													IF iRule < MC_serverBD.iMaxObjectives[ iTeam ] 
													OR MC_ServerBD.iNumVehHighestPriority[ iTeam ] > 1
														
														PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE VEH - Detaching trailer ")
														DETACH_VEHICLE_FROM_TRAILER(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
														
													ELSE
														PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE VEH - Not detaching trailer as iRule = ", iRule, "MC_serverBD.iMaxObjectives[ iTeam ] = ", MC_serverBD.iMaxObjectives[ iTeam ], " MC_ServerBD.iNumVehHighestPriority[ iTeam ] = ", MC_ServerBD.iNumVehHighestPriority[ iTeam ])
													ENDIF
														
												ELSE
													NET_SET_PLAYER_CONTROL(LocalPlayer,TRUE)
												ENDIF
												RESET_NET_TIMER(tdDeliverBackupTimer)
											
												SET_VEHICLE_DISABLE_TOWING(trailerVeh,TRUE)
												IF GET_ENTITY_BONE_INDEX_BY_NAME(trailerVeh, "attach_female") <> -1
												OR GET_ENTITY_BONE_INDEX_BY_NAME(trailerVeh, "attach_male") <> -1 //my addition
													SET_VEHICLE_AUTOMATICALLY_ATTACHES(trailerVeh, FALSE, FALSE)
													PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE VEH - setting no further attachment for veh ") 
												ENDIF
												
												IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciALLOW_MOC_ENTRY_THIS_MISSION)
													BROADCAST_FMMC_EXIT_MISSION_MOC()
												ENDIF
											ENDIF
											
											
											
											IF bcollect
												bvalidreward = TRUE
											ELSE
												DeliveryVeh = NULL
												MC_playerBD[iPartToUse].iVehDeliveryId =-1
												MC_playerBD[iPartToUse].iVehCarryCount = 0
											ENDIF
											
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF IS_BIT_SET(iLocalBoolCheck2, LBOOL2_BRING_VEHICLE_TO_HALT_CALLED)
					OR HAS_NET_TIMER_STARTED(tdDeliverBackupTimer)
						NET_SET_PLAYER_CONTROL(LocalPlayer,TRUE)
						
						IF IS_PLAYER_IN_VEH_SEAT(LocalPlayer,VS_DRIVER)
						AND NETWORK_HAS_CONTROL_OF_ENTITY(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
							SET_VEHICLE_HANDBRAKE(GET_VEHICLE_PED_IS_IN(LocalPlayerPed),FALSE)
						ENDIF
						
						RESET_NET_TIMER(tdDeliverBackupTimer)
						CLEAR_BIT(iLocalBoolCheck2,LBOOL2_BRING_VEHICLE_TO_HALT_CALLED)
						CLEAR_BIT(iLocalBoolCheck2,LBOOL2_PED_DROPPED_OFF)
						CLEAR_BIT(iLocalBoolCheck,LBOOL_CARGOBOB_DROP)
						CLEAR_BIT(MC_playerBD[ iPartToUse ].iClientBitSet2, PBBOOL2_IN_DROP_OFF)
						CLEAR_BIT(MC_playerBD[ iPartToUse ].iClientBitSet2, PBBOOL2_DROPPING_OFF)
					ENDIF
					DeliveryVeh = NULL
					MC_playerBD[iPartToUse].iVehDeliveryId =-1
					MC_playerBD[iPartToUse].iVehCarryCount = 0
				ENDIF
			ENDIF
		ENDIF
		IF bvalidreward
			IF MC_playerBD[iPartToUse].iVehDeliveryId != -1
				MC_playerBD[iPartToUse].iObjectiveTypeCompleted = JOB_COMP_DELIVER_VEH
				BROADCAST_FMMC_OBJECTIVE_COMPLETE(MC_playerBD[iPartToUse].iObjectiveTypeCompleted,iPartToUse,MC_playerBD[iPartToUse].iVehDeliveryId, iRule, MC_serverBD_4.iTimesRevalidatedObjectives[iTeam], iBitsetArrayBlank)
				iBroadcastPriority = iRule
				iBroadcastRevalidate = MC_serverBD_4.iTimesRevalidatedObjectives[iTeam]
				INCREMENT_MEDAL_OBJECTIVES_COMPLETED(iTeam, iRule)
				REINIT_NET_TIMER(tdeventsafetytimer)
				
				IF iRule < MC_serverBD.iMaxObjectives[ iTeam ]
				OR MC_ServerBD.iNumVehHighestPriority[ iTeam ] > 1
					IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
					AND ( (iRule >= FMMC_MAX_RULES) // This line is just a check so that the line below doesn't array overrun
					OR NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwo[iRule], ciBS_RULE2_DONT_GIVE_PLAYER_XP) )
						IF iPackageDelXPCount < 10
							fDelXp = (100*MC_playerBD[iPartToUse].iVehCarryCount*g_sMPTunables.fxp_tunable_Deliver_a_package_bonus)
							iDelXp = ROUND(fDelXp)
							GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD, LocalPlayerPed,"XPT_MEDIUMT", XPTYPE_COMPLETE, XPCATEGORY_COMPLETE_MISSION_DELIVERED_VEHICLE, iDelXp,1)
							#IF IS_DEBUG_BUILD
								iDebugXPDelVeh= iDebugXPDelVeh + iDelXp
							#ENDIF
							iPackageDelXPCount = iPackageDelXPCount + MC_playerBD[iPartToUse].iVehCarryCount
						ENDIF
						IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
							PLAY_SOUND_FRONTEND(-1, "ROUND_ENDING_STINGER_CUSTOM","CELEBRATION_SOUNDSET", FALSE)
						ENDIF
					ENDIF
				ELSE
					IF iPackageDelXPCount < 10
						iDelayedDeliveryXPToAward++
					ENDIF
					// Tell everyone not to take this vehicle back to freemode, since it's been delivered. Wouldn't make sense.
					BROADCAST_SCRIPT_EVENT_BLOCK_SAVE_EOM_CAR_BEEN_DELIVERED(MC_serverBD_1.sFMMC_SBD.niVehicle[MC_playerBD[iPartToUse].iVehDeliveryId])

				ENDIF
				
				BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_DELIVER_VEH, iscoremulti, iTeam ,-1,INVALID_PLAYER_INDEX(),ci_TARGET_VEHICLE,MC_playerBD[iPartToUse].iVehDeliveryId)
				MC_playerBD[iPartToUse].iVehCarryCount = 0
				CLEAR_BIT(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_WITH_CARRIER)
				CLEAR_BIT(iLocalBoolCheck,LBOOL_WITH_CARRIER_TEMP)
				CLEAR_BIT(iLocalBoolCheck2, LBOOL2_BRING_VEHICLE_TO_HALT_CALLED)
				CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_IN_DROP_OFF)
				DeliveryVeh = NULL
				RESET_NET_TIMER(tdDeliverTimer)
				PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE VEH - JOB_COMP_DELIVER_VEH set locally by part: ",iPartToUse)
				RETURN TRUE
			ENDIF
		ENDIF
		
		IF MC_playerBD[iPartToUse].iCurrentLoc != iOldLocate
			CLEAR_BIT(iLocalBoolCheck6,LBOOL6_IN_A_LOCATE)
			iOldLocate = MC_playerBD[iPartToUse].iCurrentLoc
		ENDIF
		
		IF MC_playerBD[iPartToUse].iCurrentLoc != -1
			IF MC_serverBD_4.iGotoLocationDataRule[MC_playerBD[iPartToUse].iCurrentLoc][ iTeam ] = FMMC_OBJECTIVE_LOGIC_GO_TO
				IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[MC_playerBD[iPartToUse].iCurrentLoc].iLocBS2 , ciLoc_BS2_DisplayMarkerAsVisualOnly)
					IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[MC_playerBD[iPartToUse].iCurrentLoc].iWholeTeamAtLocation[ iTeam ] = ciGOTO_LOCATION_INDIVIDUAL
						
						SET_BIT(iLocalBoolCheck6,LBOOL6_IN_A_LOCATE)
						
						IF iLocHit = - 1
							//Kill chase hint cam
							IF iNearestTarget = MC_playerBD[iPartToUse].iCurrentLoc
							AND iNearestTargetType = ci_TARGET_LOCATION
								SET_BIT(iLocalBoolCheck5, LBOOL5_OBJCOMP_KILLCHASEHINTCAM_1)
							ENDIF
							
							REGISTER_AMBIENT_VEHICLE_AS_MISSION_ENTITY(iTeam, iRule)
							
							// only set iLochit if we are in the correct direction. Or if we're not using ciLoc_BS2_DirectionRestrictPass, then set it anyway so that the fail procedures can take place.
							IF DOES_THIS_LOCATION_HAVE_A_SPECIFIC_ENTRY_HEADING(MC_playerBD[iPartToUse].iCurrentLoc)
							AND IS_PARTICIPANT_FACING_CORRECT_DIRECTION_FOR_LOCATION(MC_playerBD[iPartToUse].iCurrentLoc, iPartToUse)
							OR NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ MC_playerBD[iPartToUse].iCurrentLoc ].iLocBS2, ciLoc_BS2_DirectionRestrictPass)
								IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[MC_playerBD[iPartToUse].iCurrentLoc].iLocBS,ciLoc_BS_StopVehicle)
									
									iLocHit = MC_playerBD[iPartToUse].iCurrentLoc //It'll now do the bits below under IF(iLocHit != -1)
									
									PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LOC - Hit a stop vehicle locate, setting iLocHit = ",iLocHit)
								ELSE //Normal locate
									
									IF IS_LOCAL_PLAYER_VALID_TO_PASS_LOCATION(MC_playerBD[iPartToUse].iCurrentLoc, iTeam)
									AND IS_GO_TO_LOC_READY_TO_SEND_OUT_COMPLETION_EVENT(iTeam, iRule)
									
										IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iLocateSwapTeam[iRule] > -1
											IF NOT IS_BIT_SET(iLocalBoolCheck23, LBOOL23_LOCATE_TEAM_SWAP_REQUESTED)
												PRINTLN("[JS][LOCSWAP] - HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE - In locate, requesting swap")
												SET_BIT(iLocalBoolCheck23, LBOOL23_LOCATE_TEAM_SWAP_REQUESTED)
											ENDIF
										ENDIF
										
										IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_ALL_TEAMS_SHARE_LOCATES)
										AND IS_LOCAL_PLAYER_IN_LOCATION(MC_playerBD[iPartToUse].iCurrentLoc, iTeam)
										OR NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_ALL_TEAMS_SHARE_LOCATES)
											IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[MC_playerBD[iLocalPart].iCurrentLoc].iLocBS3, ciLoc_BS3_IncrementTimeRemaining)
												BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_ARRIVE_LOC, 0, iTeam ,-1,INVALID_PLAYER_INDEX(),ci_TARGET_LOCATION,MC_playerBD[iPartToUse].iCurrentLoc,FALSE)
											ENDIF
										ENDIF
										
										MC_playerBD[iPartToUse].iObjectiveTypeCompleted = JOB_COMP_ARRIVE_LOC
										BROADCAST_FMMC_OBJECTIVE_COMPLETE(MC_playerBD[iPartToUse].iObjectiveTypeCompleted,iPartToUse,MC_playerBD[iPartToUse].iCurrentLoc, iRule, MC_serverBD_4.iTimesRevalidatedObjectives[iTeam], iBitsetArrayBlank)
										iBroadcastPriority = iRule
										iBroadcastRevalidate = MC_serverBD_4.iTimesRevalidatedObjectives[iTeam]
										INCREMENT_MEDAL_OBJECTIVES_COMPLETED(iTeam, iRule)
										REINIT_NET_TIMER(tdeventsafetytimer)
										CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_IN_DROP_OFF)
										
										IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFIve, ciBEAST_SFX)
										AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitSetFive[ iRule ], ciBS_RULE5_ENABLE_BEAST_MODE)
											STRING sSoundSet = GET_BEAST_MODE_SOUNDSET()
											PLAY_SOUND_FRONTEND(-1, "Beast_Checkpoint", sSoundSet, FALSE)
										ELSE
											IF IS_DROPOFF_AN_AERIAL_FLAG( MC_playerBD[iPartToUse].iCurrentLoc )
												PLAY_SOUND_FRONTEND(-1, "Player_Collect","DLC_PILOT_MP_HUD_SOUNDS", FALSE)
												START_AUDIO_SCENE( "DLC_PILOT_MP_COLLECT_FLAG_SCENE" )
											ELSE 
												IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_TINY_RACERS_SOUNDS)
													PRINTLN("[RCC MISSION][TINY RACERS] Playing Checkpoint_Collect")
													PLAY_SOUND_FROM_COORD(-1,"Checkpoint_Collect", GET_LOCATION_VECTOR(MC_playerBD[iPartToUse].iCurrentLoc), "DLC_SR_TR_General_Sounds", TRUE)
												ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iHitSound[iRule] = GOTO_HIT_SOUND_DEFAULT
													PLAY_SOUND_FRONTEND(-1, "CHECKPOINT_PERFECT","HUD_MINI_GAME_SOUNDSET", FALSE)
												ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iHitSound[iRule] = GOTO_HIT_SOUND_ALT
													PLAY_SOUND_FRONTEND(-1, "CHECKPOINT_NORMAL", "HUD_MINI_GAME_SOUNDSET", FALSE)
												ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iHitSound[iRule] = GOTO_HIT_SOUND_BOMBDISARM
													IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
														PLAY_SOUND_FROM_ENTITY(-1, "Bomb_Disarmed", GET_VEHICLE_PED_IS_IN( LocalPlayerPed ), "GTAO_Speed_Convoy_Soundset", TRUE, 0 )
													ELSE
														PLAY_SOUND_FRONTEND(-1,"Bomb_Disarmed", "GTAO_Speed_Convoy_Soundset", FALSE)
													ENDIF
												ENDIF	
											ENDIF
										ENDIF
										RESET_NET_TIMER(tdLocalLocateDelayTimer)
										START_NET_TIMER(tdLocalLocateDelayTimer)
										PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LOC - MC_playerBD[iPartToUse].iCurrentLoc: ",MC_playerBD[iPartToUse].iCurrentLoc)
										PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LOC - JOB_COMP_ARRIVE_LOC set locally by part: ",iPartToUse) 
										RETURN TRUE
									ENDIF
								ENDIF
							
							ENDIF
							
						#IF IS_DEBUG_BUILD
						ELSE
							PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LOC - Can't complete current loc ",MC_playerBD[iPartToUse].iCurrentLoc,", as we're running stop car with iLocHit = ",iLocHit)
						#ENDIF
						ENDIF							
						
					ELSE // Not an individual at locate:
						
						//One off check
						IF NOT IS_BIT_SET(iLocalBoolCheck6, LBOOL6_IN_A_LOCATE)

							INT iLoc = MC_playerBD[iPartToUse].iCurrentLoc
							
							IF iLoc = - 1
								RETURN FALSE
							ENDIF
							
							INT iTeamCheckBS, iPlayersWantedInLoc, iPlayersinLoc
							BOOL bComplete
							
							GET_LOCATION_REQUIREMENT_DATA(iLoc, iTeam, iTeamCheckBS, iPlayersWantedInLoc, iPlayersinLoc)
							
							IF IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS2,ciLoc_BS2_CrossLineDisplayVFX ) 
								IF NOT ANIMPOSTFX_IS_RUNNING("CrossLine")
									PRINTLN("[MMacK][LocVFX] Playing VFX")
									PRINTLN("[CTLSFX] Playing Local")
									ANIMPOSTFX_PLAY("CrossLine", 0, TRUE)
									PLAY_SOUND_FRONTEND(-1,"Player_Enter_Line","GTAO_FM_Cross_The_Line_Soundset", FALSE)
								ENDIF
							ENDIF
							
							//Don't bother looping if we don't care what locate everyone is in
							IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetNine[iRule], ciBS_RULE9_ALL_PLAYERS_IN_ANY_LOCATE)
								IF iPlayersWantedInLoc > 0			
									
									IF iPlayersInLoc >= iPlayersWantedInLoc
										bComplete = TRUE
									ENDIF
									
								ELSE
									bComplete = TRUE
								ENDIF
							#IF IS_DEBUG_BUILD
							ELSE
								PRINTLN("[JS][ALLTEAMLOC] - I'm in a locate but ciBS_RULE9_ALL_PLAYERS_IN_ANY_LOCATE is set")
							#ENDIF
							ENDIF
							
							IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetNine[iRule], ciBS_RULE9_ALL_PLAYERS_IN_ANY_LOCATE)
								SET_BIT(iLocalBoolCheck6,LBOOL6_IN_A_LOCATE)
							ENDIF
							
							IF bComplete
								
								IF IS_LOCAL_PLAYER_VALID_TO_PASS_LOCATION(iLoc, iTeam)
								
									SET_BIT(iLocalBoolCheck6,LBOOL6_IN_A_LOCATE)
									
									REGISTER_AMBIENT_VEHICLE_AS_MISSION_ENTITY(iTeam, iRule)
									
									IF(IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS,ciLoc_BS_StopVehicle))
										iLocHit = iLoc //It'll now do the bits below under IF(iLocHit != -1)
										PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LOC - Hit a stop vehicle locate with WholeTeamAtLocation ",g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iWholeTeamAtLocation[iTeam],", setting iLocHit = ",iLocHit)
									ELSE //Normal locate
										
										IF IS_GO_TO_LOC_READY_TO_SEND_OUT_COMPLETION_EVENT(iTeam, iRule)
										
											
											IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iLocateSwapTeam[iRule] > -1
												IF NOT IS_BIT_SET(iLocalBoolCheck23, LBOOL23_LOCATE_TEAM_SWAP_REQUESTED)
													PRINTLN("[JS][LOCSWAP] - HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE 2 - In locate, requesting swap")
													SET_BIT(iLocalBoolCheck23, LBOOL23_LOCATE_TEAM_SWAP_REQUESTED)
												ENDIF
											ENDIF
											
											IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iWholeTeamAtLocation[iTeam] = ciGOTO_LOCATION_WHOLE_TEAM
												BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_TEAM_ARV_LOC,0,iTeam,iLoc,INVALID_PLAYER_INDEX(),ci_TARGET_LOCATION,iLoc)
											ENDIF
											
											IF IS_DROPOFF_AN_AERIAL_FLAG( MC_playerBD[iPartToUse].iCurrentLoc )
												PLAY_SOUND_FRONTEND(-1, "Player_Collect","DLC_PILOT_MP_HUD_SOUNDS", FALSE)
												START_AUDIO_SCENE( "DLC_PILOT_MP_COLLECT_FLAG_SCENE" )
											ELSE
												IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_TINY_RACERS_SOUNDS)
													PRINTLN("[RCC MISSION][TINY RACERS] Playing Checkpoint_Collect")
													PLAY_SOUND_FROM_COORD(-1,"Checkpoint_Collect", GET_LOCATION_VECTOR(iloc), "DLC_SR_TR_General_Sounds", TRUE)
												ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iHitSound[iRule] = GOTO_HIT_SOUND_DEFAULT
													PLAY_SOUND_FRONTEND(-1, "CHECKPOINT_PERFECT","HUD_MINI_GAME_SOUNDSET", FALSE)
												ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iHitSound[iRule] = GOTO_HIT_SOUND_ALT
													PLAY_SOUND_FRONTEND(-1, "CHECKPOINT_NORMAL", "HUD_MINI_GAME_SOUNDSET", FALSE)
												ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iHitSound[iRule] = GOTO_HIT_SOUND_BOMBDISARM
													IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
														PLAY_SOUND_FROM_ENTITY(-1, "Bomb_Disarmed", GET_VEHICLE_PED_IS_IN( LocalPlayerPed ), "GTAO_Speed_Convoy_Soundset", TRUE, 0 )
													ELSE
														PLAY_SOUND_FRONTEND(-1,"Bomb_Disarmed", "GTAO_Speed_Convoy_Soundset", FALSE)
													ENDIF
												ENDIF	
											ENDIF
												
											MC_playerBD[iPartToUse].iObjectiveTypeCompleted = JOB_COMP_ARRIVE_LOC
											BROADCAST_FMMC_OBJECTIVE_COMPLETE(MC_playerBD[iPartToUse].iObjectiveTypeCompleted, iPartToUse, iLoc, iRule, MC_serverBD_4.iTimesRevalidatedObjectives[iTeam], iBitsetArrayBlank)
											iBroadcastPriority = iRule
											iBroadcastRevalidate = MC_serverBD_4.iTimesRevalidatedObjectives[iTeam]
											INCREMENT_MEDAL_OBJECTIVES_COMPLETED(iTeam, iRule)
											REINIT_NET_TIMER(tdeventsafetytimer)
											CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_IN_DROP_OFF)
											PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LOC - MC_playerBD[iPartToUse].iCurrentLoc: ",iLoc)
											PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LOC - JOB_COMP_ARRIVE_LOC with WholeTeamAtLocation ",g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iWholeTeamAtLocation[iTeam]," set locally by part: ",iPartToUse)										
											RETURN TRUE
											
										ELSE
											CLEAR_BIT(iLocalBoolCheck6,LBOOL6_IN_A_LOCATE) // Otherwise we won't keep calling this
										ENDIF
									ENDIF
								ELSE
									PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LOC - All players are in location ",iLoc,", but being held up by other checks")
									
									RETURN FALSE
								
								ENDIF
							ELSE
								PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LOC - not ready for loc ",iLoc," w wholeteamatlocation ",g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iWholeTeamAtLocation[iTeam],", iPlayersInLoc = ",iPlayersInLoc," iPlayersWantedInLoc = ",iPlayersWantedInLoc)
								
								IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iLocateSwapTeam[iRule] > -1
									IF NOT IS_BIT_SET(iLocalBoolCheck23, LBOOL23_LOCATE_TEAM_SWAP_REQUESTED)
										PRINTLN("[JS][LOCSWAP] - HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE 3 - In locate, requesting swap")
										SET_BIT(iLocalBoolCheck23, LBOOL23_LOCATE_TEAM_SWAP_REQUESTED)
									ENDIF
								ENDIF
							ENDIF
						ELSE
							INT iLoc = MC_playerBD[iPartToUse].iCurrentLoc
							
							IF iLoc != -1
								IF IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS2,ciLoc_BS2_CrossLineDisplayVFX ) 
									IF NOT ANIMPOSTFX_IS_RUNNING("CrossLine")
										PRINTLN("[MMacK][LocVFX] Playing VFX Fallback")
										PRINTLN("[CTLSFX] Playing Local")
										ANIMPOSTFX_PLAY("CrossLine", 0, TRUE)
										PLAY_SOUND_FRONTEND(-1,"Player_Enter_Line","GTAO_FM_Cross_The_Line_Soundset", FALSE)
									ENDIF
								ENDIF
							ENDIF
							#IF IS_DEBUG_BUILD
								PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LOC - Unable to complete multi-player needed at locate iCurrentLoc ",MC_playerBD[iPartToUse].iCurrentLoc," as LBOOL6_IN_A_LOCATE is already set")
							#ENDIF
						ENDIF
						
					ENDIF
				#IF IS_DEBUG_BUILD
				ELIF bGotoLocPrints
					PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LOC - iCurrentLoc = ",MC_playerBD[iPartToUse].iCurrentLoc,", but it's not a go to - rule type = ",MC_serverBD_4.iGotoLocationDataRule[MC_playerBD[iPartToUse].iCurrentLoc][ iTeam ])
				#ENDIF
				ENDIF
			ENDIF
		ELSE
			IF ANIMPOSTFX_IS_RUNNING("CrossLine")
			AND iRule < FMMC_MAX_RULES
			AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitSetSix[ iRule ], ciBS_RULE6_GET_DELIVER_RED_FILTER)
				PRINTLN("[MMacK][LocVFX] Uncrossed the Line")
				ANIMPOSTFX_STOP("CrossLine")
				ANIMPOSTFX_PLAY("CrossLineOut", 0, FALSE)
				PRINTLN("[CTLSFX] Stopping Local")
				IF IS_BIT_SET(iLocalBoolCheck14, LBOOL14_VFX_CAPTURE_OWNER)
					STRING sSoundSet 
					sSoundSet = "DLC_Apartments_Drop_Zone_Sounds"
					PLAY_SOUND_FRONTEND(-1,"Exit_Capture_Zone",sSoundSet, FALSE)
				ELSE	
					PLAY_SOUND_FRONTEND(-1,"Player_Exit_Line","GTAO_FM_Cross_The_Line_Soundset", FALSE)
				ENDIF
				
				CLEAR_BIT(iLocalBoolCheck27, LBOOL27_RED_FILTER_SOUND_PLAYED)
			ENDIF
		ENDIF
		
		
		IF iLocHit != -1 //If we've hit a 'stop car' locate
			fStopDist = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLocHit].fStoppingDist
			IF fStopDist <= 0.0
				fStopDist = DEFAULT_VEH_STOPPING_DISTANCE
			ELIF fStopDist > 20.0
				fStopDist = 20.0
			ENDIF
			
			BOOL bSkipStopping = FALSE
			
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLocHit].iLocBS3, ciLoc_BS3_PassObjectiveBeforeStopping)
				IF NOT HAS_NET_TIMER_STARTED(stStopLocationContinueTimer)
					REINIT_NET_TIMER(stStopLocationContinueTimer)
				ELSE
					IF HAS_NET_TIMER_EXPIRED(stStopLocationContinueTimer, ciStopLocationContinue_Time)
						bSkipStopping = TRUE
						PRINTLN("HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE - Skipping the vehicle stop sequence now for location ", iLocHit, " because the timer has expired")
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT SHOULD_VEHICLE_STOP_OR_BRING_TO_HALT_WAIT()
			AND (bSkipStopping OR IS_MY_VEHICLE_STOPPED(IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLocHit].iLocBS,ciLoc_BS_StopVehicle), fStopDist))
			
				RESET_NET_TIMER(stStopLocationContinueTimer)
			
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLocHit].iLocBS,ciLoc_BS_PedsLeaveVehInLocation)				
					
					IF FORCE_EVERYONE_FROM_MY_CAR(TRUE, FALSE)
						
						IF IS_GO_TO_LOC_READY_TO_SEND_OUT_COMPLETION_EVENT(iTeam, iRule)
							
							IF IS_BIT_SET(iLocalBoolCheck2, LBOOL2_BRING_VEHICLE_TO_HALT_CALLED)
							OR HAS_NET_TIMER_STARTED(tdDeliverBackupTimer)
								IF IS_VEHICLE_DRIVEABLE(DeliveryVeh)
									IF NETWORK_HAS_CONTROL_OF_ENTITY(DeliveryVeh)
										PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LOC - Turning handbrake off (1) time ", GET_CLOUD_TIME_AS_INT())
										SET_VEHICLE_HANDBRAKE(DeliveryVeh,FALSE)
									ELSE
										PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LOC - Can't turn  handbrake off NETWORK_HAS_CONTROL_OF_ENTITY (1) time ", GET_CLOUD_TIME_AS_INT())
									ENDIF
								ENDIF
								
								IF IS_PED_IN_ANY_BOAT(LocalPlayerPed)
								OR IS_PED_IN_ANY_AMPHIBIOUS_VEHICLE(LocalPlayerPed) // url:bugstar:3498788
								OR IS_PED_IN_ANY_SEAPLANE_ON_WATER(LocalPlayerPed)
									VEHICLE_INDEX tempBoat = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
									IF IS_VEHICLE_DRIVEABLE(tempBoat)
										IF GET_PED_IN_VEHICLE_SEAT(tempBoat) = LocalPlayerPed
											IF NETWORK_HAS_CONTROL_OF_ENTITY(tempBoat)
												SET_BOAT_ANCHOR(tempBoat,FALSE)
												SET_BOAT_REMAINS_ANCHORED_WHILE_PLAYER_IS_DRIVER(tempBoat,FALSE)
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								
								NET_SET_PLAYER_CONTROL(LocalPlayer,TRUE)
								RESET_NET_TIMER(tdDeliverBackupTimer)
								CLEAR_BIT(iLocalBoolCheck2, LBOOL2_BRING_VEHICLE_TO_HALT_CALLED)
								DeliveryVeh = NULL
							ENDIF
							
							//Do 'objective completed' bits:
							BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_ARRIVE_LOC, iscoremulti, iTeam ,-1,INVALID_PLAYER_INDEX(),ci_TARGET_LOCATION,iLocHit,FALSE)
							MC_playerBD[iPartToUse].iObjectiveTypeCompleted = JOB_COMP_ARRIVE_LOC
							MC_playerBD[iPartToUse].iCurrentLoc = iLocHit
							PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE - 1 Setting iCurrentLoc to ", MC_playerBD[iLocalPart].iCurrentLoc)
							BROADCAST_FMMC_OBJECTIVE_COMPLETE(MC_playerBD[iPartToUse].iObjectiveTypeCompleted,iPartToUse,iLocHit, iRule, MC_serverBD_4.iTimesRevalidatedObjectives[iTeam], iBitsetArrayBlank)
							iBroadcastPriority = iRule
							iBroadcastRevalidate = MC_serverBD_4.iTimesRevalidatedObjectives[iTeam]
							INCREMENT_MEDAL_OBJECTIVES_COMPLETED(iTeam, iRule)
							REINIT_NET_TIMER(tdeventsafetytimer)
							REQUEST_INCREMENT_LOCAL_PLAYER_SCORE_FOR_CURRENT_OBJECTIVE()
							
							IF IS_DROPOFF_AN_AERIAL_FLAG( MC_playerBD[iPartToUse].iCurrentLoc )
								PLAY_SOUND_FRONTEND(-1, "Player_Collect","DLC_PILOT_MP_HUD_SOUNDS", FALSE)
								START_AUDIO_SCENE( "DLC_PILOT_MP_COLLECT_FLAG_SCENE" )
							ELSE
								IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_TINY_RACERS_SOUNDS)
									PRINTLN("[RCC MISSION][TINY RACERS] Playing Checkpoint_Collect")
									PLAY_SOUND_FROM_COORD(-1,"Checkpoint_Collect", GET_LOCATION_VECTOR(MC_playerBD[iPartToUse].iCurrentLoc), "DLC_SR_TR_General_Sounds", TRUE)
								ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iHitSound[iRule] = GOTO_HIT_SOUND_DEFAULT
									PLAY_SOUND_FRONTEND(-1, "CHECKPOINT_PERFECT","HUD_MINI_GAME_SOUNDSET", FALSE)
								ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iHitSound[iRule] = GOTO_HIT_SOUND_ALT
									PLAY_SOUND_FRONTEND(-1, "CHECKPOINT_NORMAL", "HUD_MINI_GAME_SOUNDSET", FALSE)
								ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iHitSound[iRule] = GOTO_HIT_SOUND_BOMBDISARM
									IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
										PLAY_SOUND_FROM_ENTITY(-1, "Bomb_Disarmed", GET_VEHICLE_PED_IS_IN( LocalPlayerPed ), "GTAO_Speed_Convoy_Soundset", TRUE, 0 )
									ELSE
										PLAY_SOUND_FRONTEND(-1,"Bomb_Disarmed", "GTAO_Speed_Convoy_Soundset", FALSE)
									ENDIF
								ENDIF	
							ENDIF
							PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LOC - Car stopped after hitting location: ",iLocHit)
							PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LOC - JOB_COMP_ARRIVE_LOC set locally by part: ",iPartToUse)
							RESET_NET_TIMER(tdLocalLocateDelayTimer)
							START_NET_TIMER(tdLocalLocateDelayTimer)
							CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_IN_DROP_OFF)
							iLocHit = -1 //Reset iLocHit, so the code above works again
							RETURN TRUE
						ENDIF
						
					#IF IS_DEBUG_BUILD
					ELSE
						PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LOC - Forcing everyone from my car after hitting location: ",iLocHit)
					#ENDIF
					ENDIF
				else //goto locate not forcing peds out
					
					IF IS_GO_TO_LOC_READY_TO_SEND_OUT_COMPLETION_EVENT(iTeam, iRule)
						
						iTimeToStop =  g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLocHit].iStopTimer
						IF iTimeToStop > 0
							PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LOC Will stop vehicle for duration iLocHit: ",  iLocHit, " Time: ", iTimeToStop, " Team: ", MC_playerBD[iPartToUse].iteam, " Rule: ", MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam])
	
							INT iMissionEntity = IS_ENTITY_A_MISSION_CREATOR_ENTITY(DeliveryVeh)
							IF iMissionEntity > -1
								PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE (2a) Setting iVeh: ", MC_playerBD[iPartToUse].iVehDeliveryId, " BROADCAST_FMMC_DELIVER_VEHICLE_STOP")
								BROADCAST_FMMC_DELIVER_VEHICLE_STOP(MC_playerBD[iPartToUse].iVehDeliveryId, iLocalPart, iTimeToStop)
							ENDIF
							SET_BIT(iLocalBoolCheck24, LBOOL24_BLOCK_VEHICLE_KERS_ON_RULE_CHECK)
							PRINTLN("[RCC MISSION] LBOOL24_BLOCK_VEHICLE_KERS_ON_RULE_CHECK setting")
							
						ELIF iTimeToStop = -1
							PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LOC Will stop vehicle forever iLocHit: ",  iLocHit, " Time: ", iTimeToStop, " Team: ", MC_playerBD[iPartToUse].iteam, " Rule: ", MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam])
							SET_BIT(iLocalBoolCheck10, LBOOL10_STOP_CAR_FOREVER)							
														
							INT iMissionEntity = IS_ENTITY_A_MISSION_CREATOR_ENTITY(DeliveryVeh)
							IF iMissionEntity > -1
								PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE (2b) Setting iVeh: ", MC_playerBD[iPartToUse].iVehDeliveryId, " BROADCAST_FMMC_DELIVER_VEHICLE_STOP")
								BROADCAST_FMMC_DELIVER_VEHICLE_STOP(MC_playerBD[iPartToUse].iVehDeliveryId, iLocalPart, iTimeToStop)
							ENDIF														
							SET_BIT(iLocalBoolCheck24, LBOOL24_BLOCK_VEHICLE_KERS_ON_RULE_CHECK)
							PRINTLN("[RCC MISSION] LBOOL24_BLOCK_VEHICLE_KERS_ON_RULE_CHECK setting")
							
						ENDIF
						
						IF IS_BIT_SET(iLocalBoolCheck2, LBOOL2_BRING_VEHICLE_TO_HALT_CALLED)
						OR HAS_NET_TIMER_STARTED(tdDeliverBackupTimer)
							IF IS_VEHICLE_DRIVEABLE(DeliveryVeh)
								IF NETWORK_HAS_CONTROL_OF_ENTITY(DeliveryVeh)
									SET_VEHICLE_HANDBRAKE(DeliveryVeh,FALSE)
									PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LOC - Turning handbrake off (2) time ", GET_CLOUD_TIME_AS_INT())
								ELSE
									PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LOC - Can't turn  handbrake off NETWORK_HAS_CONTROL_OF_ENTITY (2) time ", GET_CLOUD_TIME_AS_INT())
								ENDIF
							ENDIF
							IF IS_PED_IN_ANY_BOAT(LocalPlayerPed)
							OR IS_PED_IN_ANY_AMPHIBIOUS_VEHICLE(LocalPlayerPed) // url:bugstar:3498788
							OR IS_PED_IN_ANY_SEAPLANE_ON_WATER(LocalPlayerPed)
								VEHICLE_INDEX tempBoat = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
								IF IS_VEHICLE_DRIVEABLE(tempBoat)
									IF GET_PED_IN_VEHICLE_SEAT(tempBoat) = LocalPlayerPed
										IF NETWORK_HAS_CONTROL_OF_ENTITY(tempBoat)
											SET_BOAT_ANCHOR(tempBoat,FALSE)
											SET_BOAT_REMAINS_ANCHORED_WHILE_PLAYER_IS_DRIVER(tempBoat,FALSE)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
							NET_SET_PLAYER_CONTROL(LocalPlayer,TRUE)
							RESET_NET_TIMER(tdDeliverBackupTimer)
							CLEAR_BIT(iLocalBoolCheck2, LBOOL2_BRING_VEHICLE_TO_HALT_CALLED)
							DeliveryVeh = NULL
						ENDIF
						
						//Do 'objective completed' bits:
						BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_ARRIVE_LOC, iscoremulti, iTeam ,-1,INVALID_PLAYER_INDEX(),ci_TARGET_LOCATION,iLocHit,FALSE)
						MC_playerBD[iPartToUse].iObjectiveTypeCompleted = JOB_COMP_ARRIVE_LOC
						MC_playerBD[iPartToUse].iCurrentLoc = iLocHit
						PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE - 2 Setting iCurrentLoc to ", MC_playerBD[iLocalPart].iCurrentLoc)
						BROADCAST_FMMC_OBJECTIVE_COMPLETE(MC_playerBD[iPartToUse].iObjectiveTypeCompleted,iPartToUse,iLocHit, iRule, MC_serverBD_4.iTimesRevalidatedObjectives[iTeam], iBitsetArrayBlank)
						iBroadcastPriority = iRule
						iBroadcastRevalidate = MC_serverBD_4.iTimesRevalidatedObjectives[iTeam]
						INCREMENT_MEDAL_OBJECTIVES_COMPLETED(iTeam, iRule)
						REINIT_NET_TIMER(tdeventsafetytimer)						
						
						IF IS_DROPOFF_AN_AERIAL_FLAG( MC_playerBD[iPartToUse].iCurrentLoc )
							PLAY_SOUND_FRONTEND(-1, "Player_Collect","DLC_PILOT_MP_HUD_SOUNDS", FALSE)
							START_AUDIO_SCENE( "DLC_PILOT_MP_COLLECT_FLAG_SCENE" )
						ELSE
							IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_TINY_RACERS_SOUNDS)
								PRINTLN("[RCC MISSION][TINY RACERS] Playing Checkpoint_Collect")
								PLAY_SOUND_FROM_COORD(-1,"Checkpoint_Collect", GET_LOCATION_VECTOR(MC_playerBD[iPartToUse].iCurrentLoc), "DLC_SR_TR_General_Sounds", TRUE)
							ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iHitSound[iRule] = GOTO_HIT_SOUND_DEFAULT
								PLAY_SOUND_FRONTEND(-1, "CHECKPOINT_PERFECT","HUD_MINI_GAME_SOUNDSET", FALSE)
							ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iHitSound[iRule] = GOTO_HIT_SOUND_ALT
								PLAY_SOUND_FRONTEND(-1, "CHECKPOINT_NORMAL", "HUD_MINI_GAME_SOUNDSET", FALSE)
							ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iHitSound[iRule] = GOTO_HIT_SOUND_BOMBDISARM
								IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
									PLAY_SOUND_FROM_ENTITY(-1, "Bomb_Disarmed", GET_VEHICLE_PED_IS_IN( LocalPlayerPed ), "GTAO_Speed_Convoy_Soundset", TRUE, 0 )
								ELSE
									PLAY_SOUND_FRONTEND(-1,"Bomb_Disarmed", "GTAO_Speed_Convoy_Soundset", FALSE)
								ENDIF
							ENDIF	
						ENDIF
						PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LOC - Car stopped after hitting location: ",iLocHit)
						PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LOC - JOB_COMP_ARRIVE_LOC set locally by part: ",iPartToUse)
						RESET_NET_TIMER(tdLocalLocateDelayTimer)
						START_NET_TIMER(tdLocalLocateDelayTimer)
						iLocHit = -1 //Reset iLocHit, so the code above works again	
						CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_IN_DROP_OFF)
						RETURN TRUE
					ENDIF
				ENDIF
			#IF IS_DEBUG_BUILD
			ELSE
				PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LOC - Stopping car after hitting location: ",iLocHit)
			#ENDIF
			ENDIF
		ENDIF
		
		IF MC_playerBD[iPartToUse].iPedNear != -1
			IF MC_serverBD_4.iPedRule[MC_playerBD[iPartToUse].iPedNear][ iTeam ] = FMMC_OBJECTIVE_LOGIC_GO_TO
				//if playing football, check that the player is alive before sending ticker messages
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciUSE_FOOTBALL_HUD)
					IF IS_NET_PLAYER_OK(PLAYER_ID())
						BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_ARRIVE_PED, iscoremulti, iTeam ,-1,INVALID_PLAYER_INDEX(),ci_TARGET_PED,MC_playerBD[iPartToUse].iPedNear)
					ENDIF
				ELSE
					BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_ARRIVE_PED, iscoremulti, iTeam ,-1,INVALID_PLAYER_INDEX(),ci_TARGET_PED,MC_playerBD[iPartToUse].iPedNear)
				ENDIF
				MC_playerBD[iPartToUse].iObjectiveTypeCompleted = JOB_COMP_ARRIVE_PED
				BROADCAST_FMMC_OBJECTIVE_COMPLETE(MC_playerBD[iPartToUse].iObjectiveTypeCompleted,iPartToUse,MC_playerBD[iPartToUse].iPedNear, iRule, MC_serverBD_4.iTimesRevalidatedObjectives[iTeam], iBitsetArrayBlank)
				iBroadcastPriority = iRule
				iBroadcastRevalidate = MC_serverBD_4.iTimesRevalidatedObjectives[iTeam]
				iBroadcastPedNear = MC_playerBD[iPartToUse].iPedNear
				INCREMENT_MEDAL_OBJECTIVES_COMPLETED(iTeam, iRule)
				REINIT_NET_TIMER(tdeventsafetytimer)
				
				REQUEST_INCREMENT_LOCAL_PLAYER_SCORE_FOR_CURRENT_OBJECTIVE() 
				CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_IN_DROP_OFF)
				PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED - JOB_COMP_ARRIVE_PED set locally by part: ",iPartToUse) 
				RETURN TRUE
			ENDIF
		ENDIF
			
		IF MC_playerBD[iPartToUse].iVehNear != -1
			IF MC_serverBD_4.ivehRule[MC_playerBD[iPartToUse].iVehNear][ iTeam ] = FMMC_OBJECTIVE_LOGIC_GO_TO
				#IF IS_DEBUG_BUILD
					IF bInVehicleDebug
						PRINTLN("[RCC MISSION] AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH Checking if I've complete my goto objective and I'm on a go to")
					ENDIF
				#ENDIF
				
				BOOL bCheckOnRule // To allow GET_VEHICLE_GOTO_TEAMS to be used here
				IF (GET_VEHICLE_GOTO_TEAMS( iTeam ,MC_playerBD[iPartToUse].iVehNear, bCheckOnRule) = 0)
					#IF IS_DEBUG_BUILD
						IF bInVehicleDebug
							PRINTLN("[RCC MISSION] AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH Checking if I've complete my goto objective will return TRUE because GET_VEHICLE_GOTO_TEAMS( iTeam ,MC_playerBD[iPartToUse].iVehNear, bCheckOnRule) = 0")
						ENDIF
					#ENDIF
										
					IF IS_VEHICLE_WAITING_FOR_PLAYERS(MC_playerBD[iPartToUse].iVehNear, MC_playerBD[iLocalPart].iteam)
						PRINTLN("[RCC MISSION] AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH. RETURNING FALSE. Waiting on remaining players to enter the vehicle.")
						RETURN FALSE
					ENDIF
				
					BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_ARRIVE_VEH, iscoremulti, iTeam ,-1,INVALID_PLAYER_INDEX(),ci_TARGET_VEHICLE,MC_playerBD[iPartToUse].iVehNear)
					MC_playerBD[iPartToUse].iObjectiveTypeCompleted = JOB_COMP_ARRIVE_VEH
					BROADCAST_FMMC_OBJECTIVE_COMPLETE(MC_playerBD[iPartToUse].iObjectiveTypeCompleted,iPartToUse,MC_playerBD[iPartToUse].iVehNear, iRule, MC_serverBD_4.iTimesRevalidatedObjectives[iTeam], iBitsetArrayBlank)
					iBroadcastPriority = iRule
					iBroadcastRevalidate = MC_serverBD_4.iTimesRevalidatedObjectives[iTeam]
					iBroadcastVehNear = MC_playerBD[iPartToUse].iVehNear
					INCREMENT_MEDAL_OBJECTIVES_COMPLETED(iTeam, iRule) 
					REINIT_NET_TIMER(tdeventsafetytimer)
					CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_IN_DROP_OFF)
					PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE VEH - JOB_COMP_ARRIVE_VEH set locally by part: ",iPartToUse) 
					RETURN TRUE
				ELSE
					#IF IS_DEBUG_BUILD
						IF bInVehicleDebug
							PRINTLN("[RCC MISSION] AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH Checking if I've complete my goto objective  else GET_VEHICLE_GOTO_TEAMS( iTeam ,MC_playerBD[iPartToUse].iVehNear, bCheckOnRule) = 0")
						ENDIF
					#ENDIF
					
					tempVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[MC_playerBD[iPartToUse].iVehNear])
					IF IS_VEHICLE_DRIVEABLE(tempVeh)
						#IF IS_DEBUG_BUILD
							IF bInVehicleDebug
								PRINTLN("[RCC MISSION] AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH Checking if I've complete my goto objective IS_VEHICLE_DRIVEABLE(tempVeh)")
							ENDIF
						#ENDIF
					
						BOOL bComplete
						
						IF (amIWaitingForPlayersToEnterVeh != NULL AND NOT CALL amIWaitingForPlayersToEnterVeh())
							PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE VEH - JOB_COMP_ARRIVE_VEH All needed are in their cars, set by part: ",iPartToUse)
							bComplete = TRUE
						ELSE
							
							BOOL bRuleSetting = IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[MC_playerBD[iPartToUse].iVehNear].iVehBitSet,ciFMMC_VEHICLE_OVERRIDEWRULESETTING_GOTO)
							
							IF ( ((NOT bRuleSetting) AND (NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[MC_playerBD[iPartToUse].iVehNear].iVehBitsetTwo,ciFMMC_VEHICLE2_DISABLE_TEAMINVEH_PASS_ON_FULL_VEH)))
								OR (bRuleSetting AND (NOT IS_BIT_SET(g_FMMC_STRUCT.iVehRuleWaitForTeamsBS[MC_serverBD_4.iVehPriority[MC_playerBD[iPartToUse].iVehNear][ iTeam ]][ iTeam ],ciFMMC_VehRuleTeamBS_DISABLE_TEAMINVEH_PASS_ON_FULL_VEH))) )
							AND IS_VEHICLE_FULL(tempVeh)
								PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE VEH - JOB_COMP_ARRIVE_VEH All team needed but vehicle full, set by part: ",iPartToUse)
								bComplete = TRUE
							ENDIF
						ENDIF
						
						IF bComplete
							BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_ARRIVE_VEH, iscoremulti, iTeam ,1,INVALID_PLAYER_INDEX(),ci_TARGET_VEHICLE,MC_playerBD[iPartToUse].iVehNear)
							MC_playerBD[iPartToUse].iObjectiveTypeCompleted = JOB_COMP_ARRIVE_VEH
							BROADCAST_FMMC_OBJECTIVE_COMPLETE(MC_playerBD[iPartToUse].iObjectiveTypeCompleted,iPartToUse,MC_playerBD[iPartToUse].iVehNear, iRule, MC_serverBD_4.iTimesRevalidatedObjectives[iTeam], iBitsetArrayBlank)
							iBroadcastPriority = iRule
							iBroadcastRevalidate = MC_serverBD_4.iTimesRevalidatedObjectives[iTeam]
							iBroadcastVehNear = MC_playerBD[iPartToUse].iVehNear
							INCREMENT_MEDAL_OBJECTIVES_COMPLETED(iTeam, iRule)
							REINIT_NET_TIMER(tdeventsafetytimer)
							
							REQUEST_INCREMENT_LOCAL_PLAYER_SCORE_FOR_CURRENT_OBJECTIVE()
							CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_IN_DROP_OFF)
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
					IF bInVehicleDebug
						PRINTLN("[RCC MISSION] AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH Checking if I've complete my goto objective but MC_serverBD_4.ivehRule[MC_playerBD[iPartToUse].iVehNear][ iTeam ] = ", MC_serverBD_4.ivehRule[MC_playerBD[iPartToUse].iVehNear][ iTeam ])
					ENDIF
				#ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
				IF bInVehicleDebug
					PRINTLN("[RCC MISSION] AM_I_WAITING_FOR_PLAYERS_TO_ENTER_VEH Checking if I've complete my goto objective but MC_playerBD[iPartToUse].iVehNear = -1")
				ENDIF
			#ENDIF
		ENDIF
		
		// If we have a go-to-object rule 
		IF MC_playerBD[iPartToUse].iObjNear != -1
			PRINTLN("[RCC MISSION][Objects][Object ", MC_playerBD[iPartToUse].iObjNear, "] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE OBJ - We are near enough to collect iobj: ", MC_playerBD[iPartToUse].iObjNear)
			
			IF MC_serverBD_4.iObjRule[MC_playerBD[iPartToUse].iObjNear][iTeam] = FMMC_OBJECTIVE_LOGIC_GO_TO		
			
				IF NOT IS_BIT_SET(iSentObjectCollectionTickerBS, MC_playerBD[iPartToUse].iObjNear)
					PRINTLN("[Objects][Object ", MC_playerBD[iPartToUse].iObjNear, "] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE | Sending Object go-to ticker!")
					BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_ARRIVE_OBJ, iscoremulti, iTeam, -1, INVALID_PLAYER_INDEX(), ci_TARGET_OBJECT, MC_playerBD[iPartToUse].iObjNear)
					
					IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[MC_playerBD[iPartToUse].iObjNear].iGotoRange <= 2.0
						// The player is so close they're almost certainly already holding the object. Treat the "go-to" ticker as the collection ticker, ensuring that we don't send another ticker for holding the object if we move onto a Get and Deliver rule any time soon.
						SET_BIT(iSentObjectCollectionTickerBS, MC_playerBD[iPartToUse].iObjNear)
					ENDIF
				ENDIF
				
				MC_playerBD[iPartToUse].iObjectiveTypeCompleted = JOB_COMP_ARRIVE_OBJ
				BROADCAST_FMMC_OBJECTIVE_COMPLETE(MC_playerBD[iPartToUse].iObjectiveTypeCompleted,iPartToUse,MC_playerBD[iPartToUse].iObjNear, iRule, MC_serverBD_4.iTimesRevalidatedObjectives[iTeam], iBitsetArrayBlank)
				iBroadcastPriority = iRule
				iBroadcastRevalidate = MC_serverBD_4.iTimesRevalidatedObjectives[iTeam]
				INCREMENT_MEDAL_OBJECTIVES_COMPLETED(iTeam, iRule)
				REINIT_NET_TIMER(tdeventsafetytimer)
				
				REQUEST_INCREMENT_LOCAL_PLAYER_SCORE_FOR_CURRENT_OBJECTIVE()
				
				IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciENABLE_IN_AND_OUT_SOUNDS)
				AND NOT IS_BIT_SET(iobjCollectedBoolCheck, MC_playerBD[iPartToUse].iObjNear)
					PLAY_SOUND_FRONTEND(-1, "Friend_Pick_Up", "HUD_FRONTEND_MP_COLLECTABLE_SOUNDS", FALSE)
				ENDIF
				PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE OBJ - JOB_COMP_ARRIVE_OBJ set locally by part: ",iPartToUse)
				SET_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITICAL_ENTITY(TRUE)
				PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE OBJ - setting SET_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITICAL_ENTITY true - 7")
				
				SET_BIT(iobjCollectedBoolCheck, MC_playerBD[iPartToUse].iObjNear)				
				CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_IN_DROP_OFF)
				SET_OBJ_NEAR(-1)		
				RETURN TRUE
			ENDIF
		ENDIF


		IF MC_playerBD[iPartToUse].iLocPhoto != -1
			IF MC_serverBD_4.iGotoLocationDataRule[MC_playerBD[iPartToUse].iLocPhoto][ iTeam ] = FMMC_OBJECTIVE_LOGIC_PHOTO
				BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_PHOTO_LOC, iscoremulti, iTeam ,-1,INVALID_PLAYER_INDEX(),ci_TARGET_LOCATION,MC_playerBD[iPartToUse].iLocPhoto)
				MC_playerBD[iPartToUse].iObjectiveTypeCompleted = JOB_COMP_PHOTO_LOC
				BROADCAST_FMMC_OBJECTIVE_COMPLETE(MC_playerBD[iPartToUse].iObjectiveTypeCompleted,iPartToUse,MC_playerBD[iPartToUse].iLocPhoto, iRule, MC_serverBD_4.iTimesRevalidatedObjectives[iTeam], iBitsetArrayBlank)
				iBroadcastPriority = iRule
				iBroadcastRevalidate = MC_serverBD_4.iTimesRevalidatedObjectives[iTeam]
				INCREMENT_MEDAL_OBJECTIVES_COMPLETED(iTeam, iRule)
				REINIT_NET_TIMER(tdeventsafetytimer)
				
				REQUEST_INCREMENT_LOCAL_PLAYER_SCORE_FOR_CURRENT_OBJECTIVE()
				CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_IN_DROP_OFF)
				PLAY_SOUND_FRONTEND(-1, "CHECKPOINT_PERFECT","HUD_MINI_GAME_SOUNDSET", FALSE)
				PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LOC PHOTO - MC_playerBD[iPartToUse].iLocPhoto: ",MC_playerBD[iPartToUse].iLocPhoto)
				PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LOC PHOTO - JOB_COMP_PHOTO_LOC set locally by part: ",iPartToUse)
				RETURN TRUE
			ELSE
				PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LOC PHOTO - iLocPhoto is set to ",MC_playerBD[iPartToUse].iLocPhoto," but we aren't on a photo rule so clear it - for my part ",iPartToUse) 
				MC_playerBD[iPartToUse].iLocPhoto = -1
			ENDIF
		ENDIF
		
		IF MC_playerBD[iPartToUse].iVehPhoto != -1
			IF MC_serverBD_4.ivehRule[MC_playerBD[iPartToUse].iVehPhoto][ iTeam ] = FMMC_OBJECTIVE_LOGIC_PHOTO
				BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_PHOTO_VEH, iscoremulti, iTeam ,-1,INVALID_PLAYER_INDEX(),ci_TARGET_VEHICLE,MC_playerBD[iPartToUse].iVehPhoto)
				RESET_PHOTO_DATA()
				MC_playerBD[iPartToUse].iVehNear = -1
				MC_playerBD[iPartToUse].iObjectiveTypeCompleted = JOB_COMP_PHOTO_VEH
				BROADCAST_FMMC_OBJECTIVE_COMPLETE(MC_playerBD[iPartToUse].iObjectiveTypeCompleted,iPartToUse,MC_playerBD[iPartToUse].iVehPhoto, iRule, MC_serverBD_4.iTimesRevalidatedObjectives[iTeam], iBitsetArrayBlank)
				iBroadcastPriority = iRule
				iBroadcastRevalidate = MC_serverBD_4.iTimesRevalidatedObjectives[iTeam]
				INCREMENT_MEDAL_OBJECTIVES_COMPLETED(iTeam, iRule)
				REINIT_NET_TIMER(tdeventsafetytimer)
				PLAY_SOUND_FRONTEND(-1, "CHECKPOINT_PERFECT","HUD_MINI_GAME_SOUNDSET", FALSE)
				PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE VEH PHOTO - JOB_COMP_PHOTO_VEH set locally by part: ",iPartToUse) 
				RETURN TRUE
			ELSE
				PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE VEH PHOTO - iVehPhoto is set to ",MC_playerBD[iPartToUse].iVehPhoto," but we aren't on a photo rule so clear it - for my part ",iPartToUse) 
				MC_playerBD[iPartToUse].iVehPhoto = -1
			ENDIF
		ENDIF
		
		IF MC_playerBD[iPartToUse].iPedPhoto != -1
	        IF MC_serverBD_4.iPedRule[MC_playerBD[iPartToUse].iPedPhoto][ iTeam ] = FMMC_OBJECTIVE_LOGIC_PHOTO
                IF IS_BIT_SET(MC_serverBD.iDeadPedPhotoBitset[ iTeam ][GET_LONG_BITSET_INDEX(MC_playerBD[iPartToUse].iPedPhoto)], GET_LONG_BITSET_BIT(MC_playerBD[iPartToUse].iPedPhoto))
                    BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_PHOTO_PED, iscoremulti, iTeam ,0,INVALID_PLAYER_INDEX(),ci_TARGET_PED,MC_playerBD[iPartToUse].iPedPhoto)
                ELSE
                    BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_PHOTO_PED, iscoremulti, iTeam ,-1,INVALID_PLAYER_INDEX(),ci_TARGET_PED,MC_playerBD[iPartToUse].iPedPhoto)
                ENDIF
                RESET_PHOTO_DATA()
                MC_playerBD[iPartToUse].iPedNear = -1
                MC_playerBD[iPartToUse].iObjectiveTypeCompleted = JOB_COMP_PHOTO_PED
                BROADCAST_FMMC_OBJECTIVE_COMPLETE(MC_playerBD[iPartToUse].iObjectiveTypeCompleted,iPartToUse,MC_playerBD[iPartToUse].iPedPhoto, iRule, MC_serverBD_4.iTimesRevalidatedObjectives[iTeam], iBitsetArrayBlank)
               	iBroadcastPriority = iRule
				iBroadcastRevalidate = MC_serverBD_4.iTimesRevalidatedObjectives[iTeam]
				INCREMENT_MEDAL_OBJECTIVES_COMPLETED(iTeam, iRule)
				REINIT_NET_TIMER(tdeventsafetytimer)
				
		        REQUEST_INCREMENT_LOCAL_PLAYER_SCORE_FOR_CURRENT_OBJECTIVE() 
				
				CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_IN_DROP_OFF)
				PLAY_SOUND_FRONTEND(-1, "CHECKPOINT_PERFECT","HUD_MINI_GAME_SOUNDSET", FALSE)
                PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED PHOTO - JOB_COMP_PHOTO_PED set locally by part: ",iPartToUse) 
                RETURN TRUE
	        ELSE
				PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE PED PHOTO - iPedPhoto is set to ",MC_playerBD[iPartToUse].iPedPhoto," but we aren't on a photo rule so clear it - for my part ",iPartToUse) 
				MC_playerBD[iPartToUse].iPedPhoto = -1
			ENDIF
		ENDIF
		
		IF MC_playerBD[iPartToUse].iObjPhoto != -1
			IF MC_serverBD_4.iObjRule[MC_playerBD[iPartToUse].iObjPhoto][ iTeam ] = FMMC_OBJECTIVE_LOGIC_PHOTO
				BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_PHOTO_OBJ, iscoremulti, iTeam ,-1,INVALID_PLAYER_INDEX(),ci_TARGET_OBJECT,MC_playerBD[iPartToUse].iObjPhoto)
				CLEANUP_PHOTO_TRACKED_POINT(iObjPhotoTrackedPoint)
				SET_OBJ_NEAR(-1)
				MC_playerBD[iPartToUse].iObjectiveTypeCompleted = JOB_COMP_PHOTO_OBJ
				BROADCAST_FMMC_OBJECTIVE_COMPLETE(MC_playerBD[iPartToUse].iObjectiveTypeCompleted,iPartToUse,MC_playerBD[iPartToUse].iObjPhoto, iRule, MC_serverBD_4.iTimesRevalidatedObjectives[iTeam], iBitsetArrayBlank)
				iBroadcastPriority = iRule
				iBroadcastRevalidate = MC_serverBD_4.iTimesRevalidatedObjectives[iTeam]
				INCREMENT_MEDAL_OBJECTIVES_COMPLETED(iTeam, iRule)
				REINIT_NET_TIMER(tdeventsafetytimer)
				
				REQUEST_INCREMENT_LOCAL_PLAYER_SCORE_FOR_CURRENT_OBJECTIVE()
				CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_IN_DROP_OFF)
				PLAY_SOUND_FRONTEND(-1, "CHECKPOINT_PERFECT","HUD_MINI_GAME_SOUNDSET", FALSE)
				PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE OBJ PHOTO - JOB_COMP_PHOTO_OBJ set locally by part: ",iPartToUse) 
				
				RETURN TRUE
			ELSE
				PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE OBJ PHOTO - iObjPhoto is set to ",MC_playerBD[iPartToUse].iObjPhoto," but we aren't on a photo rule so clear it - for my part ",iPartToUse) 
				MC_playerBD[iPartToUse].iObjPhoto = -1
			ENDIF
		ENDIF
		
		IF MC_playerBD[iPartToUse].iObjHacked != -1
			IF MC_serverBD_4.iObjRule[MC_playerBD[iPartToUse].iObjHacked][ iTeam ] = FMMC_OBJECTIVE_LOGIC_MINIGAME
				BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_HACK_OBJ, iscoremulti, iTeam ,MC_serverBD_4.iObjMissionSubLogic[ iTeam ],INVALID_PLAYER_INDEX(),ci_TARGET_OBJECT,MC_playerBD[iPartToUse].iObjHacked)
				MC_playerBD[iPartToUse].iObjectiveTypeCompleted = JOB_COMP_HACK_OBJ
				BROADCAST_FMMC_OBJECTIVE_COMPLETE(MC_playerBD[iPartToUse].iObjectiveTypeCompleted,iPartToUse,MC_playerBD[iPartToUse].iObjHacked, iRule, MC_serverBD_4.iTimesRevalidatedObjectives[iTeam], iBitsetArrayBlank)
				iBroadcastPriority = iRule
				iBroadcastRevalidate = MC_serverBD_4.iTimesRevalidatedObjectives[iTeam]
				INCREMENT_MEDAL_OBJECTIVES_COMPLETED(iTeam, iRule)
				REINIT_NET_TIMER(tdeventsafetytimer)
				
				REQUEST_INCREMENT_LOCAL_PLAYER_SCORE_FOR_CURRENT_OBJECTIVE()
				CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_IN_DROP_OFF)
				PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE OBJ HACK - JOB_COMP_HACK_OBJ set locally by part: ",iPartToUse)
				
				RETURN TRUE
			ELSE
				PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE OBJ HACK - iObjHacked is set to ",MC_playerBD[iPartToUse].iObjHacked," but we aren't on a hack rule so clear it - for my part ",iPartToUse) 
				MC_playerBD[iPartToUse].iObjHacked = -1
			ENDIF
		ENDIF
		
	ELSE
		
		#IF IS_DEBUG_BUILD
		IF MC_playerBD[iPartToUse].iObjectiveTypeCompleted != -1
			PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE - Unable to complete objectives, iObjectiveTypeCompleted = ",MC_playerBD[iPartToUse].iObjectiveTypeCompleted)
		ENDIF
		IF IS_BIT_SET( MC_serverBD.iProcessJobCompBitset, iPartToUse )
			PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE - Unable to complete objectives, iProcessJobCompBitset is set for me, part ",iPartToUse)
		ENDIF
		IF IS_OBJECTIVE_BLOCKED()
			PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE - Unable to complete objectives, IS_OBJECTIVE_BLOCKED is TRUE")
		ENDIF
		IF (iRule < FMMC_MAX_RULES)
			IF IS_BIT_SET( MC_serverBD_1.iObjectiveProgressionHeldUpBitset[iTeam], iRule)
				PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE - Unable to complete objectives, iObjectiveProgressionHeldUpBitset is set for team ",iTeam," + rule ",iRule)
			ENDIF
		ENDIF
		#ENDIF
		
		IF ANIMPOSTFX_IS_RUNNING("CrossLine")
		AND iRule < FMMC_MAX_RULES
		AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitSetSix[ iRule ], ciBS_RULE6_GET_DELIVER_RED_FILTER)
			PRINTLN("[MMacK][LocVFX] Uncrossed the Line through nefarious means")
			ANIMPOSTFX_STOP("CrossLine")
			ANIMPOSTFX_PLAY("CrossLineOut", 0, FALSE)
			PRINTLN("[CTLSFX] Stopping Local")
			IF IS_BIT_SET(iLocalBoolCheck14, LBOOL14_VFX_CAPTURE_OWNER)
				STRING sSoundSet 
				sSoundSet = "DLC_Apartments_Drop_Zone_Sounds"
				PLAY_SOUND_FRONTEND(-1,"Exit_Capture_Zone",sSoundSet, FALSE)
			ELSE	
				PLAY_SOUND_FRONTEND(-1,"Player_Exit_Line","GTAO_FM_Cross_The_Line_Soundset", FALSE)
			ENDIF
			
			CLEAR_BIT(iLocalBoolCheck27, LBOOL27_RED_FILTER_SOUND_PLAYED)
		ENDIF
		
		//Clean up delivery progress - we've swapped out of the ability to deliver here
		IF HAS_NET_TIMER_STARTED(tdDeliverBackupTimer)
		OR IS_BIT_SET(iLocalBoolCheck2, LBOOL2_BRING_VEHICLE_TO_HALT_CALLED)
			NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
			
			IF IS_PLAYER_IN_VEH_SEAT(LocalPlayer,VS_DRIVER)
			AND NETWORK_HAS_CONTROL_OF_ENTITY(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
				SET_VEHICLE_HANDBRAKE(GET_VEHICLE_PED_IS_IN(LocalPlayerPed),FALSE)
			ENDIF
			
			IF IS_PED_IN_ANY_BOAT(LocalPlayerPed)
			OR IS_PED_IN_ANY_AMPHIBIOUS_VEHICLE(LocalPlayerPed) // url:bugstar:3498788
			OR IS_PED_IN_ANY_SEAPLANE_ON_WATER(LocalPlayerPed)
				VEHICLE_INDEX tempBoat = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
				IF IS_VEHICLE_DRIVEABLE(tempBoat)
					IF GET_PED_IN_VEHICLE_SEAT(tempBoat) = LocalPlayerPed
						IF NETWORK_HAS_CONTROL_OF_ENTITY(tempBoat)
							SET_BOAT_ANCHOR(tempBoat,FALSE)
							SET_BOAT_REMAINS_ANCHORED_WHILE_PLAYER_IS_DRIVER(tempBoat,FALSE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			MC_playerBD[iPartToUse].iVehDeliveryId =-1
			PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE - Clearing iVehDeliveryId as tdDeliverBackupTimer or LBOOL2_BRING_VEHICLE_TO_HALT_CALLED")
			DeliveryVeh = NULL
			
			RESET_NET_TIMER(tdDeliverBackupTimer)
			CLEAR_BIT(iLocalBoolCheck2,LBOOL2_BRING_VEHICLE_TO_HALT_CALLED)
			CLEAR_BIT(iLocalBoolCheck2,LBOOL2_PED_DROPPED_OFF)
			CLEAR_BIT(iLocalBoolCheck,LBOOL_CARGOBOB_DROP)
			CLEAR_BIT(MC_playerBD[ iPartToUse ].iClientBitSet2, PBBOOL2_DROPPING_OFF)
		ENDIF
		
	ENDIF
	
	//-- Fade out the drop-off marker
	IF IS_BIT_SET(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_FADE_OUT)
		#IF IS_DEBUG_BUILD
			IF bWdDropOffMarker
				PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LBOOL10_DROP_OFF_LOC_FADE_OUT is set eState = ", ENUM_TO_INT(locateDropOff.eState))
			ENDIF
		#ENDIF
		
		IF setLocateToFadeOut != NULL
			CALL setLocateToFadeOut(locateDropOff)
		ENDIF
				
	ENDIF
	
	//-- Turn on the drop-off marker.
	IF IS_BIT_SET(iLocalBoolCheck10, LBOOL10_DROP_OFF_LOC_DISPLAY)
		#IF IS_DEBUG_BUILD
			IF bWdDropOffMarker
				PRINTLN("[RCC MISSION] HAS_LOCAL_PLAYER_COMPLETED_OBJECTIVE LBOOL10_DROP_OFF_LOC_DISPLAY is set eState = ", ENUM_TO_INT(locateDropOff.eState))
			ENDIF
		#ENDIF
		IF setLocateToDisplay != NULL
			CALL setLocateToDisplay(locateDropOff)
		ENDIF
	ENDIF
		
	RETURN FALSE

ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section: Electronics ----------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Functions relating to electronics (e.g Fuse boxes & CCTV Cameras)
// ##### 
// ##### 
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
FUNC BOOL CAN_CCTV_BREAK(OBJECT_INDEX tempObj)
	
	MODEL_NAMES mn = GET_ENTITY_MODEL(tempObj)
	
	IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_cctv_cam_01a"))
	OR mn = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_cctv_cam_02a"))
	OR mn = reh_prop_reh_drone_02a
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_ELECTRONIC_BE_DISABLED_THIS_FRAME(ELECTRONIC_PARAMS& sElectronicParams)
	
	SWITCH sElectronicParams.eEntityType
		CASE FMMC_ELECTRONIC_ENTITY_TYPE__OBJECT
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[sElectronicParams.iIndex].sObjElectronicData.iElectronicBS, ciELECTRONIC_BS_AffectedByEMP)
				IF IS_ANY_EMP_CURRENTLY_ACTIVE()
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
		
		CASE FMMC_ELECTRONIC_ENTITY_TYPE__DYNOPROP
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[sElectronicParams.iIndex].sDynopropElectronicData.iElectronicBS, ciELECTRONIC_BS_AffectedByEMP)
				IF IS_ANY_EMP_CURRENTLY_ACTIVE()
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	IF HAS_ENTITY_BEEN_HIT_BY_TASER(sElectronicParams.iIndex, sElectronicParams.eEntityType)
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

PROC CLEANUP_CCTV_CAMERA_SOUND_EFFECT_FOR_DEATH(ELECTRONIC_PARAMS& sElectronicParams)
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_CCTVAlertSpottedSounds)
		IF iSoundAlarmCCTV[sElectronicParams.iCamIndex] > -1
		AND NOT HAS_SOUND_FINISHED(iSoundAlarmCCTV[sElectronicParams.iCamIndex])
			PRINTLN("[LM][PROCESS_CCTV_CAMERA_ALARM_SOUNDS] - Camera is destroyed for sElectronicParams.iCamIndex ", sElectronicParams.iCamIndex, " Stopping all sounds.")
			STOP_SOUND(iSoundAlarmCCTV[sElectronicParams.iCamIndex])
			SCRIPT_RELEASE_SOUND_ID(iSoundAlarmCCTV[sElectronicParams.iCamIndex])
			iSoundAlarmType[sElectronicParams.iCamIndex] = -1
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_CCTV_CAMERA_ALARM_SOUNDS(ELECTRONIC_PARAMS& sElectronicParams, BOOL bSpotted, BOOL bAlerted, BOOL bElectronicsDisabled)
	IF sElectronicParams.iCamIndex > -1
		IF (bSpotted OR bAlerted)
		AND NOT bElectronicsDisabled
		
			INT iSoundType = 0
			TEXT_LABEL_63 tl63SoundSet
			TEXT_LABEL_63 tl63SoundName
			
			IF bSpotted
				PRINTLN("[LM][PROCESS_CCTV_CAMERA_ALARM_SOUNDS] - bSpotted by iCam ", sElectronicParams.iCamIndex)
				iSoundType = 1
				
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFive, ciOptionsBS25_EnableCasinoHeistAudioBank)
					PRINTLN("[LM][PROCESS_CCTV_CAMERA_ALARM_SOUNDS] Using Camera_Alarm")
					tl63SoundSet = "dlc_ch_heist_finale_security_alarms_sounds"
					tl63SoundName = "Camera_Alarm"
					
				ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_UseCasinoCCTVAudio)
					tl63SoundSet = "dlc_vw_casino_finale_sounds"
					tl63SoundName = "CCTV_Alarm"
				ELSE
					tl63SoundSet = "ALARMS_SOUNDSET"
					tl63SoundName = "Klaxon_04"
				ENDIF
			ENDIF
						
			IF bAlerted
				PRINTLN("[LM][PROCESS_CCTV_CAMERA_ALARM_SOUNDS] - bAlerted by sElectronicParams.iCamIndex ", sElectronicParams.iCamIndex)
				iSoundType = 2
				
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFive, ciOptionsBS25_EnableCasinoHeistAudioBank)
					PRINTLN("[LM][PROCESS_CCTV_CAMERA_ALARM_SOUNDS] Using Camera_Alarm")
					tl63SoundSet = "dlc_ch_heist_finale_security_alarms_sounds"
					tl63SoundName = "Camera_Alarm"
					
				ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_UseCasinoCCTVAudio)
					PRINTLN("[LM][PROCESS_CCTV_CAMERA_ALARM_SOUNDS] Using Main_Alarm")
					tl63SoundSet = "dlc_vw_casino_finale_sounds"
					tl63SoundName = "Main_Alarm"
				ELSE
					PRINTLN("[LM][PROCESS_CCTV_CAMERA_ALARM_SOUNDS] Using Bell_01")
					tl63SoundSet = "ALARMS_SOUNDSET"
					tl63SoundName = "Bell_01"
				ENDIF
			ENDIF
			
			IF iSoundAlarmType[sElectronicParams.iCamIndex] != iSoundType
				iSoundAlarmType[sElectronicParams.iCamIndex] = iSoundType
			
				IF iSoundAlarmCCTV[sElectronicParams.iCamIndex] = -1
					PRINTLN("[LM][PROCESS_CCTV_CAMERA_ALARM_SOUNDS] - Getting sound ID for sElectronicParams.iCamIndex ", sElectronicParams.iCamIndex)
					iSoundAlarmCCTV[sElectronicParams.iCamIndex] = GET_SOUND_ID()
				ENDIF
				
				IF NOT HAS_SOUND_FINISHED(iSoundAlarmCCTV[sElectronicParams.iCamIndex])
					PRINTLN("[LM][PROCESS_CCTV_CAMERA_ALARM_SOUNDS] - Stopping Current Sound for sElectronicParams.iCamIndex ", sElectronicParams.iCamIndex)
					STOP_SOUND(iSoundAlarmCCTV[sElectronicParams.iCamIndex])
				ENDIF
				
				PRINTLN("[LM][PROCESS_CCTV_CAMERA_ALARM_SOUNDS] - Paying sound for sElectronicParams.iCamIndex ", sElectronicParams.iCamIndex)
				PLAY_SOUND_FROM_COORD(iSoundAlarmCCTV[sElectronicParams.iCamIndex], tl63SoundName, sElectronicParams.vElectronicCoords, tl63SoundSet)
			ENDIF
		ELSE
			IF iSoundAlarmCCTV[sElectronicParams.iCamIndex] > -1
			AND NOT HAS_SOUND_FINISHED(iSoundAlarmCCTV[sElectronicParams.iCamIndex])
				PRINTLN("[LM][PROCESS_CCTV_CAMERA_ALARM_SOUNDS] - Not alerted or spotted for sElectronicParams.iCamIndex ", sElectronicParams.iCamIndex, " Stopping all sounds.")
				STOP_SOUND(iSoundAlarmCCTV[sElectronicParams.iCamIndex])
				SCRIPT_RELEASE_SOUND_ID(iSoundAlarmCCTV[sElectronicParams.iCamIndex])
				iSoundAlarmType[sElectronicParams.iCamIndex] = -1
			ENDIF
		ENDIF		
	ENDIF
ENDPROC

PROC PROCESS_CCTV_VIEWCONE_MARKER(ELECTRONIC_PARAMS& sElectronicParams, BOOL bSpotted, BOOL bAlerted)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_ShowCCTVLightCones)
		EXIT
	ENDIF
	
	IF IS_BIT_SET(iCCTVBrokenBS, sElectronicParams.iCamIndex)
		EXIT
	ENDIF
	
	VECTOR vPosition, vDirection, vRotation, vScale
	FLOAT fLightIntensity = 5.0
	INT iR, iG, iB, iA
	HUD_COLOURS hudCol = HUD_COLOUR_WHITE
	
	IF bAlerted
		hudCol = HUD_COLOUR_RED
	ELIF bSpotted
		hudCol = HUD_COLOUR_YELLOW
	ENDIF
	
	GET_HUD_COLOUR(hudCol, iR, iG, iB, iA)
	
	IF bSpotted
	OR bAlerted
		iA = 60
	ELSE
		iA = 30
	ENDIF
	
	iA = ROUND((fLightIntensity / cf_CCTV_BaseLightIntensity) * iA)
	iA = CLAMP_INT(iA, 0, 80)
	
	vPosition = sElectronicParams.vElectronicCoords
	vDirection = <<0.0, 0.0, 0.5>>	
	vRotation = <<180.0, 0.0, GET_ENTITY_HEADING(sElectronicParams.oiElectronic)>>
	
	vPosition = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPosition, GET_ENTITY_HEADING(sElectronicParams.oiElectronic), <<0.0, -1.45, -0.27>>)
	
	vScale = <<4.0, 4.0, 1.75>>
	
	DRAW_MARKER_EX(MARKER_CONE, vPosition, vDirection, vRotation, vScale, iR, iG, iB, iA, FALSE, FALSE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
	
ENDPROC

PROC TRIGGER_ALL_CCTV_CAMERAS(INT iPartTriggered = -1)
	PRINTLN("[LM][TRIGGER_ALL_CCTV_CAMERAS] - Triggering all active CCTV cameras.")
	
	INT iCam = 0
	FOR iCam = 0 TO (MAX_NUM_CCTV_CAM - 1)
		SET_CCTV_CAMERA_SPOTTED_PLAYER(iCam)
		START_NET_TIMER(CCTVSpottingPlayerTimer[iCam])
		iCCTVCameraSpottedPlayer[iCam] = iPartTriggered
	ENDFOR
	
	SET_BIT(iLocalBoolCheck31, LBOOL31_HAS_TRIGGERED_ALL_CCTV_CAMERAS)
ENDPROC

PROC DISPLAY_CCTV_SPOTTED_HELP_TEXT(INT iCam, INT iAggroBS)
	
	IF IS_BIT_SET(iSeenCCTVCameraSpottedSomethingHelpTextBS, iCam)
	OR IS_BIT_SET(iLocalBoolCheck31, LBOOL31_AGGRO_HELPTEXT_PLAYED)
		// Already seen the aggro helptext
		PRINTLN("[CCTV_SPAM] DISPLAY_CCTV_SPOTTED_HELP_TEXT | Already seen the helptext")
		EXIT
	ENDIF
		
	SET_BIT(iSeenCCTVCameraSpottedSomethingHelpTextBS, iCam)
	SET_BIT(iLocalBoolCheck31, LBOOL31_AGGRO_HELPTEXT_PLAYED)
	CLEAR_HELP(TRUE)
	
	IF IS_BIT_SET(iCCTVCameraSpottedBodyBS, iCam)
	OR HAVE_CCTV_CAMERAS_WITH_AGGRO_BS_SPOTTED_DEAD_BODY(MC_PlayerBD[iPartToUse].iTeam, iAggroBS)
		// Spotted a body!
		PRINTLN("[CCTV] DISPLAY_CCTV_SPOTTED_HELP_TEXT | Displaying helptext - CCTV spotted a body")
		PRINT_HELP("SPOTEDCCTVB", 5000)
		
	ELIF iCCTVCameraSpottedPlayer[iCam] > -1
		// Spotted a player!
		PRINTLN("[CCTV] DISPLAY_CCTV_SPOTTED_HELP_TEXT | Displaying helptext - CCTV spotted a player (", iCCTVCameraSpottedPlayer[iCam], ")")
		PARTICIPANT_INDEX partTriggerer = INT_TO_PARTICIPANTINDEX(iCCTVCameraSpottedPlayer[iCam])
		PLAYER_INDEX piTriggerer = NETWORK_GET_PLAYER_INDEX(partTriggerer)	
		PRINT_HELP_WITH_PLAYER_NAME("SPOTEDCCTVC", GET_PLAYER_NAME(piTriggerer), HUD_COLOUR_WHITE, 5000)
	ENDIF
ENDPROC

FUNC BOOL SHOULD_DISPLAY_CCTV_HELPTEXT(FMMC_CCTV_DATA& sCCTVData)
	
	INT iTeam  = MC_playerBD[iPartToUse].iTeam
	INT iStartShowRule, iEndShowRule
	INT iCurrentRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	IF sCCTVData.iCCTVShowHelpTextFromThisRule = -1
		iStartShowRule = 0
	ELIF sCCTVData.iCCTVShowHelpTextFromThisRule > -1 
		iStartShowRule = sCCTVData.iCCTVShowHelpTextFromThisRule
	ELSE
		PRINTLN("[ML][SHOULD_DISPLAY_CCTV_HELPTEXT] Failsafe(1), iCCTVShowHelpTextFromThisRule is < -1")
	ENDIF
	
	IF sCCTVData.iCCTVBlockHelpTextFromThisRule = -1
		iEndShowRule = FMMC_MAX_RULES-1
	ELIF sCCTVData.iCCTVBlockHelpTextFromThisRule > -1 
		iEndShowRule = sCCTVData.iCCTVBlockHelpTextFromThisRule
	ELSE
		PRINTLN("[ML][SHOULD_DISPLAY_CCTV_HELPTEXT] Failsafe(2), iCCTVBlockHelpTextFromThisRule is < -1")
	ENDIF
	
	IF iStartShowRule > iEndShowRule
		PRINTLN("[ML][SHOULD_DISPLAY_CCTV_HELPTEXT] returning FALSE on rule ", iCurrentRule, " as iStartShowRule > iEndShowRule")
		RETURN FALSE
		
	ELIF iCurrentRule >= iStartShowRule
	AND iCurrentRule < iEndShowRule
		PRINTLN("[ML][SHOULD_DISPLAY_CCTV_HELPTEXT] returning TRUE on rule ", iCurrentRule)
		RETURN TRUE
		
	ELSE
		RETURN FALSE
	ENDIF
	
ENDFUNC

FUNC BOOL DOES_CAMERA_HAVE_CLEAR_LOS_TO_ENTITY(FMMC_CCTV_DATA& sCCTVData, ENTITY_INDEX entityIndexTo, ENTITY_INDEX entityIndexFrom)

	IF NOT IS_BIT_SET(sCCTVData.iCCTVBS, ciCCTV_BS_UseEntityLineOfSight)
		RETURN TRUE
	ENDIF
	
	IF HAS_ENTITY_CLEAR_LOS_TO_ENTITY(entityIndexTo, entityIndexFrom)
		PRINTLN("[CCTV] DOES_CAMERA_HAVE_CLEAR_LOS_TO_ENTITY - Has clear line of sight!")
		RETURN TRUE
	ELSE
		#IF IS_DEBUG_BUILD
		IF bCCTVDebug
			TEXT_LABEL_63 tlEntityDebug = "No CCTV LOS"
			DRAW_DEBUG_TEXT_ABOVE_ENTITY(entityIndexTo, tlEntityDebug, 0.2)
			DRAW_DEBUG_LINE(GET_ENTITY_COORDS(entityIndexFrom), GET_ENTITY_COORDS(entityIndexTo), 255, 0, 0)
		ENDIF
		#ENDIF
	ENDIF
	
	PRINTLN("[CCTV] DOES_CAMERA_HAVE_CLEAR_LOS_TO_ENTITY - Returning FALSE")
	RETURN FALSE
ENDFUNC

#IF IS_DEBUG_BUILD
PROC PROCESS_CCTV_DEBUG(ELECTRONIC_PARAMS& sElectronicParams, FMMC_CCTV_DATA& sCCTVData, INT iAggroBS)
	
	IF NOT bCCTVDebug
	OR MC_serverBD.iCameraNumber[sElectronicParams.iCamIndex] = -1
		EXIT
	ENDIF

	IF MC_serverBD.eCameraType[sElectronicParams.iCamIndex] = FMMC_ELECTRONIC_ENTITY_TYPE__OBJECT
		TEXT_LABEL_63 tlDebugText3D = "OBJ | CCTV Cam "
		tlDebugText3D += sElectronicParams.iCamIndex
		tlDebugText3D += "/ OBJ:"
		tlDebugText3D += sElectronicParams.iIndex
		IF IS_BIT_SET(iCCTVBrokenBS, sElectronicParams.iCamIndex)
			tlDebugText3D += " BROKEN"
		ENDIF
		DRAW_DEBUG_TEXT(tlDebugText3D, g_FMMC_STRUCT_ENTITIES.sPlacedObject[MC_serverBD.iCameraNumber[sElectronicParams.iCamIndex]].vPos, 0, 255, 0)
		
	ELIF MC_serverBD.eCameraType[sElectronicParams.iCamIndex] = FMMC_ELECTRONIC_ENTITY_TYPE__DYNOPROP
		TEXT_LABEL_63 tlDebugText3D = "DP | CCTV Cam "
		tlDebugText3D += sElectronicParams.iCamIndex
		tlDebugText3D += "/ DP:"
		tlDebugText3D += sElectronicParams.iIndex
		IF IS_BIT_SET(iCCTVBrokenBS, sElectronicParams.iCamIndex)
			tlDebugText3D += " BROKEN"
		ENDIF
		DRAW_DEBUG_TEXT(tlDebugText3D, g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[MC_serverBD.iCameraNumber[sElectronicParams.iCamIndex]].vPos, 0, 255, 0)
		
	ENDIF

	TEXT_LABEL_63 tlEntityDebug = "| "
	IF HAS_CCTV_CAMERA_SPOTTED_SOMETHING(sElectronicParams.iCamIndex)
		tlEntityDebug += "Spotted something!"
		
	ELSE
		IF HAS_NET_TIMER_STARTED(CCTVSpottingPlayerTimer[sElectronicParams.iCamIndex])
			tlEntityDebug += "Spotted Timer /"
		ENDIF
		IF HAS_NET_TIMER_STARTED(CCTVSpottingBodyTimer[sElectronicParams.iCamIndex])
			tlEntityDebug += "Body Timer /"
		ENDIF
		tlEntityDebug += " |"
	ENDIF
		
	INT iTeamLoop
	FOR iTeamLoop = 0 TO (MC_serverBD.iNumberOfTeams - 1)
		IF HAVE_CCTV_CAMERAS_WITH_AGGRO_BS_BEEN_ALERTED(iTeamLoop, iAggroBS)
			tlEntityDebug += " | ALERTED"
			tlEntityDebug += " by Team "
			tlEntityDebug += iTeamLoop
		ENDIF
	ENDFOR
	DRAW_DEBUG_TEXT_ABOVE_ENTITY(sElectronicParams.oiElectronic, tlEntityDebug, -0.2, 255, 0, 0)
	
	tlEntityDebug = "fCCTVCamInitialHeading: "
	tlEntityDebug += FLOAT_TO_STRING(fCCTVCamInitialHeading[sElectronicParams.iCamIndex])
	tlEntityDebug += " / "
	IF IS_BIT_SET(MC_serverBD.CCTV_TURNED_BITSET, ci_CCTV_FINISHED_TURN_CAM_0 + sElectronicParams.iCamIndex)
		tlEntityDebug += "Turned"
	ELSE
		tlEntityDebug += "Not turned"
	ENDIF
	DRAW_DEBUG_TEXT_ABOVE_ENTITY(sElectronicParams.oiElectronic, tlEntityDebug, -0.4, 255, 255, 255)
	
	tlEntityDebug = "fCCTVTurnAmount: "
	tlEntityDebug += FLOAT_TO_STRING(sCCTVData.fCCTVTurnAmount)
	DRAW_DEBUG_TEXT_ABOVE_ENTITY(sElectronicParams.oiElectronic, tlEntityDebug, -0.6, 255, 255, 255)
	
	tlEntityDebug = "Range: "
	tlEntityDebug += FLOAT_TO_STRING(sCCTVData.fCCTVVisionRange)
	tlEntityDebug += " / Width: "
	tlEntityDebug += FLOAT_TO_STRING(sCCTVData.fCCTVVisionWidth)
	DRAW_DEBUG_TEXT_ABOVE_ENTITY(sElectronicParams.oiElectronic, tlEntityDebug, -0.99, 255, 255, 255)
ENDPROC
#ENDIF

FUNC BOOL CACHE_CAM_INDEX_FOR_CCTV_ENTITY(ELECTRONIC_PARAMS& sElectronicParams, INT& iCachedCamIndex)
	INT i
	FOR i = 0 TO (MAX_NUM_CCTV_CAM - 1)
		IF IS_THIS_CAM_THIS_ENTITY(i, sElectronicParams.iIndex, sElectronicParams.eEntityType)
			iCachedCamIndex = i
			PRINTLN("[CCTV][CAM ", i, "] CACHE_CAM_INDEX_FOR_CCTV_ENTITY | Caching sElectronicParams.iIndex ", sElectronicParams.iIndex, " as camera ", i)
			RETURN TRUE
		ELSE
			RELOOP
		ENDIF
	ENDFOR
	
	IF iCachedCamIndex = -1
		ASSERTLN("[CCTV] CACHE_CAM_INDEX_FOR_CCTV_ENTITY | Can't find a proper cam for sElectronicParams.iIndex ", sElectronicParams.iIndex)
		PRINTLN("[CCTV] CACHE_CAM_INDEX_FOR_CCTV_ENTITY | Can't find a proper cam for sElectronicParams.iIndex ", sElectronicParams.iIndex)		
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC DESTROY_CCTV_CAMERA(ELECTRONIC_PARAMS& sElectronicParams, BOOL bHasControl, INT iAggroBS)
	
	IF bHasControl
		PRINTLN("[CCTV][CAM ", sElectronicParams.iCamIndex, "] DESTROY_CCTV_CAMERA | Physically breaking camera")
		
		IF CAN_CCTV_BREAK(sElectronicParams.oiElectronic)
			BREAK_OBJECT_FRAGMENT_CHILD(sElectronicParams.oiElectronic, 1, FALSE)
			BREAK_OBJECT_FRAGMENT_CHILD(sElectronicParams.oiElectronic, 2, FALSE)
			
			DAMAGE_OBJECT_FRAGMENT_CHILD(sElectronicParams.oiElectronic, 0)
			DAMAGE_OBJECT_FRAGMENT_CHILD(sElectronicParams.oiElectronic, 1)
			DAMAGE_OBJECT_FRAGMENT_CHILD(sElectronicParams.oiElectronic, 2)
		
			APPLY_FORCE_TO_ENTITY(sElectronicParams.oiElectronic, APPLY_TYPE_EXTERNAL_IMPULSE, (<<0, 0, 2>>), <<0,0,0>>, 0, FALSE, FALSE, FALSE)
			APPLY_FORCE_TO_ENTITY(sElectronicParams.oiElectronic, APPLY_TYPE_EXTERNAL_IMPULSE, (<<0, 0, 2>>), <<0,0,0>>, 1, FALSE, FALSE, FALSE)
			APPLY_FORCE_TO_ENTITY(sElectronicParams.oiElectronic, APPLY_TYPE_EXTERNAL_IMPULSE, (<<0, 0, 2>>), <<0,0,0>>, 2, FALSE, FALSE, FALSE)
		ENDIF
		
		SET_ENTITY_HEALTH(sElectronicParams.oiElectronic, 0)
	ENDIF
	
	IF bIsLocalPlayerHost
		MC_serverBD_2.iCamObjDestroyed++
		PRINTLN("[CCTV][CAM ", sElectronicParams.iCamIndex, "] DESTROY_CCTV_CAMERA | Host has detected that Cam ", sElectronicParams.iCamIndex, " is broken/dead | iCamObjDestroyed is now ", MC_serverBD_2.iCamObjDestroyed, " | g_FMMC_STRUCT.iCCTVDestroyCountToCauseAggro is ", g_FMMC_STRUCT.iCCTVDestroyCountToCauseAggro)
		
		IF NOT IS_PLAYER_TRIGGERED_EMP_CURRENTLY_ACTIVE()
			IF g_FMMC_STRUCT.iCCTVDestroyCountToCauseAggro > -1
			AND NOT HAS_TEAM_TRIGGERED_AGGRO(0, iAggroBS)
				IF MC_serverBD_2.iCamObjDestroyed >= g_FMMC_STRUCT.iCCTVDestroyCountToCauseAggro
					SET_TEAM_AS_TRIGGERED_AGGRO_FROM_BITSET(0, iAggroBS)
					SET_TEAM_AS_TRIGGERED_AGGRO_FROM_BITSET(1, iAggroBS)
					SET_TEAM_AS_TRIGGERED_AGGRO_FROM_BITSET(2, iAggroBS)
					SET_TEAM_AS_TRIGGERED_AGGRO_FROM_BITSET(3, iAggroBS)
					PRINTLN("[CCTV][CAM ", sElectronicParams.iCamIndex, "] DESTROY_CCTV_CAMERA - Destroyed too many cameras! Triggering aggro now")
				ENDIF
			ENDIF
		ELSE
			PRINTLN("[CCTV][CAM ", sElectronicParams.iCamIndex, "] DESTROY_CCTV_CAMERA | Not checking iCCTVDestroyCountToCauseAggro due to a player triggered EMP currently being active")
		ENDIF
	ENDIF
	
	SET_BIT(iCCTVBrokenBS, sElectronicParams.iCamIndex)
	PLAY_SOUND_FROM_COORD(-1, "Camera_Destroy", sElectronicParams.vElectronicCoords, "DLC_HEIST_FLEECA_SOUNDSET", FALSE, 0)
	PRINTLN("[CCTV][CAM ", sElectronicParams.iCamIndex, "] DESTROY_CCTV_CAMERA | Setting iCCTVBrokenBS ", sElectronicParams.iCamIndex)
ENDPROC

FUNC BOOL SHOULD_CCTV_CAMERA_HAVE_VISION_CONE(ELECTRONIC_PARAMS& sElectronicParams)
	IF SHOULD_ELECTRONIC_BE_DISABLED_THIS_FRAME(sElectronicParams)
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(iCCTVCameraSpottedPlayerBS, sElectronicParams.iCamIndex)
		RETURN FALSE
	ENDIF
	
	RETURN DOES_BLIP_EXIST(sElectronicParams.biElectronicBlip)
ENDFUNC

PROC PROCESS_CCTV_BLIP(ELECTRONIC_PARAMS& sElectronicParams, FMMC_CCTV_DATA& sCCTVData)
	
	IF NOT DOES_BLIP_EXIST(sElectronicParams.biElectronicBlip)
		REMOVE_CCTV_VISION_CONE(sElectronicParams.biElectronicBlip, sElectronicParams.iIndex, sElectronicParams.eEntityType)
		EXIT
	ENDIF
	
	SET_BLIP_ROTATION_WITH_FLOAT(sElectronicParams.biElectronicBlip, GET_ENTITY_HEADING(sElectronicParams.oiElectronic))
	
	IF IS_BIT_SET(sCCTVData.iCCTVBS, ciCCTV_BS_OverrideBlipColour)
		IF DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_CCTV_CAMERA(sCCTVData)
			SET_BLIP_COLOUR(sElectronicParams.biElectronicBlip, BLIP_COLOUR_WHITE)
		ELSE
			SET_BLIP_COLOUR(sElectronicParams.biElectronicBlip, BLIP_COLOUR_RED)
		ENDIF
	ENDIF
	
	IF SHOULD_CCTV_CAMERA_HAVE_VISION_CONE(sElectronicParams)
		FLOAT fWidthForCone = sCCTVData.fCCTVVisionWidth * 0.2
		
		IF NOT IS_BIT_SET(iCCTVConeBitSet, ci_CCTV_CONE_BLIP_ADDED_0 + sElectronicParams.iCamIndex)
			PRINTLN("[CCTV][CAM ", sElectronicParams.iCamIndex, "][RCC MISSION] PROCESS_CCTV_BLIP - sElectronicParams.iCamIndex: ", sElectronicParams.iCamIndex, " - Initial fake cone setup | fWidthForCone: ", fWidthForCone, " | fCCTVVisionRange: ", sCCTVData.fCCTVVisionRange)
			ADD_FAKE_VISION_CONE_TO_BLIP(sElectronicParams.biElectronicBlip, -1.0, 1.0, fWidthForCone, 1.0, (sCCTVData.fCCTVVisionRange * 1.0), DEG_TO_RAD(GET_ENTITY_HEADING(sElectronicParams.oiElectronic) + 180), TRUE)

			SET_BIT(iCCTVConeBitSet, ci_CCTV_CONE_BLIP_ADDED_0 + sElectronicParams.iCamIndex)
		ELSE
			FLOAT fRotation = DEG_TO_RAD(GET_ENTITY_HEADING(sElectronicParams.oiElectronic) + 180)
			SETUP_FAKE_CONE_DATA(sElectronicParams.biElectronicBlip, -1.0, 1.0, fWidthForCone, 1.0, (sCCTVData.fCCTVVisionRange * 1.0), fRotation, TRUE)
		ENDIF
	ELSE
		REMOVE_CCTV_VISION_CONE(sElectronicParams.biElectronicBlip, sElectronicParams.iIndex, sElectronicParams.eEntityType)
	ENDIF
	
ENDPROC

FUNC BOOL SHOULD_THIS_CCTV_CAMERA_BE_PROCESSED_THIS_FRAME(INT& iCachedCamIndex)
	// Process all even CCTV cameras one frame, and then all odd CCTV cameras the next frame
	RETURN (iCachedCamIndex % 2) = (GET_FRAME_COUNT() % 2)
ENDFUNC

FUNC BOOL CAN_CCTV_CAMERA_MOVE_THIS_FRAME(ELECTRONIC_PARAMS& sElectronicParams, FMMC_CCTV_DATA& sCCTVData)
	
	IF IS_BIT_SET(sCCTVData.iCCTVBS, ciCCTV_BS_StopMovingWhenSpotting)
		IF HAS_NET_TIMER_STARTED(CCTVSpottingBodyTimer[sElectronicParams.iCamIndex])
		OR HAS_NET_TIMER_STARTED(CCTVSpottingPlayerTimer[sElectronicParams.iCamIndex])
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF SHOULD_ELECTRONIC_BE_DISABLED_THIS_FRAME(sElectronicParams)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC PROCESS_CCTV_CAMERA_TURNING(ELECTRONIC_PARAMS& sElectronicParams, FMMC_CCTV_DATA& sCCTVData, BOOL bHasControl)

	SWITCH MC_serverBD.eCCTVCamState[sElectronicParams.iCamIndex]
		CASE CamState_Wait
		
			IF sCCTVData.fCCTVTurnAmount != 0.0
				IF bIsLocalPlayerHost
					IF NOT HAS_NET_TIMER_STARTED(CCTVCameraTurnTimer[sElectronicParams.iCamIndex])
						REINIT_NET_TIMER(CCTVCameraTurnTimer[sElectronicParams.iCamIndex])
						
					ELSE
						INT iWaitTimer
						iWaitTimer = ROUND(sCCTVData.fCCTVWaitTime * 1000)
						IF HAS_NET_TIMER_EXPIRED(CCTVCameraTurnTimer[sElectronicParams.iCamIndex], iWaitTimer)
							PRINTLN("[CCTV_MOVEMENT][CAM_MOVEMENT ", sElectronicParams.iCamIndex, "][RCC MISSION] - iCam: ", sElectronicParams.iCamIndex, " Sending Cam to Turning")
							
							MC_serverBD.eCCTVCamState[sElectronicParams.iCamIndex] = CamState_Turning
							RESET_NET_TIMER(CCTVCameraTurnTimer[sElectronicParams.iCamIndex])
						ENDIF
					ENDIF
				ENDIF
				
				IF fCCTVCamInitialHeading[sElectronicParams.iCamIndex] = 0.0
					fCCTVCamInitialHeading[sElectronicParams.iCamIndex] = GET_ENTITY_HEADING(sElectronicParams.oiElectronic)
					PRINTLN("[CCTV_MOVEMENT][CAM_MOVEMENT ", sElectronicParams.iCamIndex, "][RCC MISSION] - iCam: ", sElectronicParams.iCamIndex, " initialising fCCTVCamInitialHeading as ", fCCTVCamInitialHeading[sElectronicParams.iCamIndex])
				ENDIF
			ENDIF
			
		BREAK
		CASE CamState_Turning
		
			IF CAN_CCTV_CAMERA_MOVE_THIS_FRAME(sElectronicParams, sCCTVData)
				FLOAT fTargetHeading
				FLOAT fTempheading
				
				IF IS_BIT_SET(MC_serverBD.CCTV_TURNED_BITSET, ci_CCTV_FINISHED_TURN_CAM_0 + sElectronicParams.iCamIndex)
					fTargetHeading = fCCTVCamInitialHeading[sElectronicParams.iCamIndex]
				ELSE
					fTargetHeading = fCCTVCamInitialHeading[sElectronicParams.iCamIndex] + sCCTVData.fCCTVTurnAmount
				ENDIF
				
				IF fTargetHeading < 0
					fTargetHeading = 360-(ABSF(fTargetHeading))
				ELIF fTargetHeading > 360
					fTargetHeading = (fTargetHeading)-360
				ENDIF
				
				#IF IS_DEBUG_BUILD
				IF bCCTVDebug
					TEXT_LABEL_63 tlEntityDebug
					tlEntityDebug = "Heading: "
					tlEntityDebug += FLOAT_TO_STRING(GET_ENTITY_HEADING_FROM_EULERS(sElectronicParams.oiElectronic))
					DRAW_DEBUG_TEXT_ABOVE_ENTITY(sElectronicParams.oiElectronic, tlEntityDebug, -2.8)
					
					tlEntityDebug = "Target: "
					tlEntityDebug += FLOAT_TO_STRING(fTargetHeading)
					tlEntityDebug += " | Current: "
					tlEntityDebug += FLOAT_TO_STRING(GET_ENTITY_HEADING_FROM_EULERS(sElectronicParams.oiElectronic))
					DRAW_DEBUG_TEXT_ABOVE_ENTITY(sElectronicParams.oiElectronic, tlEntityDebug, -1.2)
				ENDIF
				#ENDIF
				
				IF IS_FLOAT_IN_RANGE(GET_ENTITY_HEADING(sElectronicParams.oiElectronic), fTargetHeading - cfCCTVRotationTargetBuffer, fTargetHeading + cfCCTVRotationTargetBuffer)
				OR (bIsLocalPlayerHost AND IS_BIT_SET(iCCTVReachedDestinationBS, sElectronicParams.iCamIndex))
					IF bIsLocalPlayerHost
						PRINTLN("[CCTV_MOVEMENT][CAM_MOVEMENT ", sElectronicParams.iCamIndex, "][RCC MISSION] - iCam: ", sElectronicParams.iCamIndex, " Sending Cam to Wait.")
						MC_serverBD.eCCTVCamState[sElectronicParams.iCamIndex] = CamState_Wait
						
						IF IS_BIT_SET(iCCTVReachedDestinationBS, sElectronicParams.iCamIndex)
							CLEAR_BIT(iCCTVReachedDestinationBS, sElectronicParams.iCamIndex)
							PRINTLN("[CCTV_SPAM][CAM_MOVEMENT ", sElectronicParams.iCamIndex, "_SPAM][RCC MISSION] - iCam: ", sElectronicParams.iCamIndex, " Host clearing its iCCTVReachedDestinationBS (1)")
						ENDIF
						
						IF IS_BIT_SET(MC_serverBD.CCTV_TURNED_BITSET, ci_CCTV_FINISHED_TURN_CAM_0+sElectronicParams.iCamIndex)
							CLEAR_BIT(MC_serverBD.CCTV_TURNED_BITSET, ci_CCTV_FINISHED_TURN_CAM_0+sElectronicParams.iCamIndex)
						ELSE
							SET_BIT(MC_serverBD.CCTV_TURNED_BITSET, ci_CCTV_FINISHED_TURN_CAM_0+sElectronicParams.iCamIndex)
						ENDIF
					ELSE
						IF NOT IS_BIT_SET(iCCTVReachedDestinationBS, sElectronicParams.iCamIndex)
							SET_BIT(iCCTVReachedDestinationBS, sElectronicParams.iCamIndex)
							PRINTLN("[CCTV_MOVEMENT][CAM_MOVEMENT ", sElectronicParams.iCamIndex, "][RCC MISSION] - iCam: ", sElectronicParams.iCamIndex, " Setting iCCTVReachedDestinationBS")
							
							IF bHasControl
								BROADCAST_FMMC_CCTV_REACHED_DESTINATION(sElectronicParams.iCamIndex)
								PRINTLN("[CCTV_MOVEMENT][CAM_MOVEMENT ", sElectronicParams.iCamIndex, "][RCC MISSION] - iCam: ", sElectronicParams.iCamIndex, " Sending event to say this cam has reached its destination.")
							ENDIF
						ELSE
							PRINTLN("[CCTV_MOVEMENT][CAM_MOVEMENT ", sElectronicParams.iCamIndex, "][RCC MISSION] - iCam: ", sElectronicParams.iCamIndex, " Waiting for host to notice that it has reached its destination.")
						ENDIF
					ENDIF
				ELSE
					
					IF IS_BIT_SET(iCCTVReachedDestinationBS, sElectronicParams.iCamIndex)
						CLEAR_BIT(iCCTVReachedDestinationBS, sElectronicParams.iCamIndex)
						PRINTLN("[CCTV_MOVEMENT][CAM_MOVEMENT ", sElectronicParams.iCamIndex, "_SPAM][RCC MISSION] - iCam: ", sElectronicParams.iCamIndex, " Clearing its iCCTVReachedDestinationBS (2)")
					ENDIF
					
					fTempheading = GET_ENTITY_HEADING_FROM_EULERS(sElectronicParams.oiElectronic)
					
					FLOAT fMovementThisFrame
					fMovementThisFrame = (fLastFrameTime * sCCTVData.fCCTVTurnSpeed * cfCCTVTurnSpeedScalar)
					
					IF (NOT IS_BIT_SET(MC_serverBD.CCTV_TURNED_BITSET, ci_CCTV_FINISHED_TURN_CAM_0 + sElectronicParams.iCamIndex) AND sCCTVData.fCCTVTurnAmount > 0.0)
					OR (IS_BIT_SET(MC_serverBD.CCTV_TURNED_BITSET, ci_CCTV_FINISHED_TURN_CAM_0 + sElectronicParams.iCamIndex) AND sCCTVData.fCCTVTurnAmount < 0.0)
						// Moving out
						IF fTargetHeading > fTempHeading
							fTempheading = CLAMP(CONVERT_FLOAT_TO_ROTATION_VALUE(fTempHeading) + fMovementThisFrame, fTempheading, fTargetHeading)
							
							#IF IS_DEBUG_BUILD
							IF bCCTVDebug
								PRINTLN("[CCTV_MOVEMENT][CAM_MOVEMENT ", sElectronicParams.iCamIndex, "][RCC MISSION] - Adding ", fMovementThisFrame, ", clamping at ", fTargetHeading)
							ENDIF
							#ENDIF
						ELSE
							fTempheading = fTempheading + fMovementThisFrame
							
							#IF IS_DEBUG_BUILD
							IF bCCTVDebug
								PRINTLN("[CCTV_MOVEMENT][CAM_MOVEMENT ", sElectronicParams.iCamIndex, "][RCC MISSION] - Adding ", fMovementThisFrame)
							ENDIF
							#ENDIF
						ENDIF
					ELSE
						// Coming back
						IF fTargetHeading < fTempHeading
							fTempheading = CLAMP(CONVERT_FLOAT_TO_ROTATION_VALUE(fTempHeading) - fMovementThisFrame, fTargetHeading, fTempheading)
							
							#IF IS_DEBUG_BUILD
							IF bCCTVDebug
								//PRINTLN("[CCTV_MOVEMENT][CAM_MOVEMENT ", sElectronicParams.iCamIndex, "][RCC MISSION] - Subtracting ", fMovementThisFrame, ", clamping at ", fTargetHeading)
							ENDIF
							#ENDIF
						ELSE
							fTempheading = fTempheading - fMovementThisFrame
							
							#IF IS_DEBUG_BUILD
							IF bCCTVDebug
								//PRINTLN("[CCTV_MOVEMENT][CAM_MOVEMENT ", sElectronicParams.iCamIndex, "][RCC MISSION] - Subtracting ", fMovementThisFrame)
							ENDIF
							#ENDIF
						ENDIF
					ENDIF
					
					IF bHasControl
						SET_ENTITY_HEADING(sElectronicParams.oiElectronic, fTempheading)
						
						#IF IS_DEBUG_BUILD
						IF bCCTVDebug
							//PRINTLN("[CCTV_MOVEMENT][CAM_MOVEMENT ", sElectronicParams.iCamIndex, "][RCC MISSION] - Setting heading to ", fTempheading)
						ENDIF
						#ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC PROCESS_CCTV_CAMERA_LIGHT(ELECTRONIC_PARAMS& sElectronicParams)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySix, ciOptionsBS26_DrawCCTVLights)
		EXIT
	ENDIF
	
	// Needs a refactor if lights are ever wanted again
	UNUSED_PARAMETER(sElectronicParams)
	
ENDPROC

FUNC BOOL IS_ENTITY_CURRENTLY_IN_CCTV_VISION_CONE(ELECTRONIC_PARAMS& sElectronicParams, FMMC_CCTV_DATA& sCCTVData, ENTITY_INDEX eiEntityToCheck, BOOL bElectronicsDisabled)
	
	IF bElectronicsDisabled
		RETURN FALSE
	ENDIF
	
	VECTOR vPosToCheck = GET_ENTITY_COORDS(eiEntityToCheck, FALSE)
	VECTOR vIgnoreRange = <<sCCTVData.fCCTVVisionRange * 1.5, sCCTVData.fCCTVVisionRange * 1.5, 0.0>>
	
	IF NOT IS_POINT_IN_AXIS_ALIGNED_AREA(vPosToCheck, sElectronicParams.vElectronicCoords - vIgnoreRange, sElectronicParams.vElectronicCoords + vIgnoreRange, FALSE)	
		// The point is too far away from the CCTV to matter!
		RETURN FALSE
	ENDIF

	VECTOR vLocalOffset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(sElectronicParams.oiElectronic, vPosToCheck)
	vLocalOffset.y = -vLocalOffset.y // CCTV models strangely point backwards, so we're flipping the offset's "forward" value here
	
	IF vLocalOffset.y < 0.1
		// No need to continue - the position is behind the camera
		RETURN FALSE
	ENDIF
	
	BOOL bInVisionCone = TRUE
	FLOAT fMaxX = LERP_FLOAT(sCCTVData.fCCTVVisionWidth * 0.05, sCCTVData.fCCTVVisionWidth + (sCCTVData.fCCTVVisionRange * 0.4), CLAMP((vLocalOffset.y / sCCTVData.fCCTVVisionRange), 0.0, 1.0)) // The side range should be bigger the further forward along the cone the point is
	FLOAT fMaxY = LERP_FLOAT(sCCTVData.fCCTVVisionRange * 0.9, sCCTVData.fCCTVVisionRange * 1.05, CLAMP(1.0 - (vLocalOffset.x / fMaxX), 0.0, 1.0)) // The forward range should be a bit smaller at the edges and a bit bigger in the very middle
	FLOAT fHeightBuffer = 1.5
	
	FLOAT fSafetyBuffer = 0.5 // Give the player some extra wiggle room
	fMaxX -= fSafetyBuffer
	
	IF ABSF(vLocalOffset.x) > fMaxX	
		// Too far to the side!
		bInVisionCone = FALSE
		PRINTLN("[CCTV_CONE][CAM_CONE ", sElectronicParams.iCamIndex, "][RCC MISSION] - Blocked by vPosToCheck being ", vPosToCheck , " / fMaxX: ", fMaxX)
	ENDIF
	
	IF vLocalOffset.y > fMaxY
		// Too far in front!
		bInVisionCone = FALSE
		PRINTLN("[CCTV_CONE][CAM_CONE ", sElectronicParams.iCamIndex, "][RCC MISSION] - Blocked by vPosToCheck being ", vPosToCheck , " / fMaxY: ", fMaxY)
	ENDIF
	
	IF vPosToCheck.z > sElectronicParams.vElectronicCoords.z + fHeightBuffer
	OR vPosToCheck.z < fCCTVCameraCachedGroundZ[sElectronicParams.iCamIndex] - fHeightBuffer
		// Too low/too high!
		bInVisionCone = FALSE
		PRINTLN("[CCTV_CONE][CAM_CONE ", sElectronicParams.iCamIndex, "][RCC MISSION] - Blocked by vPosToCheck being ", vPosToCheck , " / fCCTVCameraCachedGroundZ[", sElectronicParams.iCamIndex,"]: ", fCCTVCameraCachedGroundZ[sElectronicParams.iCamIndex])
	ENDIF
	
	IF bInVisionCone
		IF IS_BIT_SET(sCCTVData.iCCTVBS, ciCCTV_BS_UseEntityLineOfSight)
		AND NOT DOES_CAMERA_HAVE_CLEAR_LOS_TO_ENTITY(sCCTVData, sElectronicParams.oiElectronic, eiEntityToCheck)
			// Blocked by not having line-of-sight!
			bInVisionCone = FALSE
			PRINTLN("[CCTV_CONE][CAM_CONE ", sElectronicParams.iCamIndex, "][RCC MISSION] - Blocked by line-of-sight check")
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF debugState = eControllerDebugState_OVERVIEW
	OR bCCTVDebug
		TEXT_LABEL_63 tlVisualDebug = "vLocalOffset: "
		tlVisualDebug += FLOAT_TO_STRING(vLocalOffset.x)
		tlVisualDebug += ", "
		tlVisualDebug += FLOAT_TO_STRING(vLocalOffset.y)
		tlVisualDebug += ", "
		tlVisualDebug += FLOAT_TO_STRING(vLocalOffset.z)
		
		IF bInVisionCone
			DRAW_DEBUG_TEXT(tlVisualDebug, vPosToCheck - <<0,0,1>>, 255, 0, 0, 255)
		ELSE
			DRAW_DEBUG_TEXT(tlVisualDebug, vPosToCheck - <<0,0,1>>, 255, 255, 255, 255)
		ENDIF
		
		tlVisualDebug = "fMaxX: "
		tlVisualDebug += FLOAT_TO_STRING(fMaxX)
		tlVisualDebug += " || fMaxY: "
		tlVisualDebug += FLOAT_TO_STRING(fMaxY)
		
		DRAW_DEBUG_TEXT(tlVisualDebug, sElectronicParams.vElectronicCoords - <<0,0,4>>, 255, 255, 255, 255)
		
		VECTOR vForward = GET_ENTITY_FORWARD_VECTOR(sElectronicParams.oiElectronic)
		
		VECTOR vDebugCCTVPos = sElectronicParams.vElectronicCoords
		VECTOR vDebugForwardLimitPos = sElectronicParams.vElectronicCoords - (vForward * fMaxY)
		VECTOR vDebugEntityForwardPos = sElectronicParams.vElectronicCoords - (vForward * CLAMP(vLocalOffset.y, 0.0, fMaxY))
		
		vDebugCCTVPos.z = vPosToCheck.z
		vDebugForwardLimitPos.z = vPosToCheck.z
		vDebugEntityForwardPos.z = vPosToCheck.z
		
		VECTOR vRight = CROSS_PRODUCT(vForward, <<0,0,1>>)
		DRAW_DEBUG_LINE(vDebugCCTVPos, vDebugForwardLimitPos, 0, 255, 0)
		DRAW_DEBUG_LINE(vDebugEntityForwardPos - (vRight * fMaxX), vDebugEntityForwardPos + (vRight * fMaxX), 255, 0, 0)
	ENDIF
	#ENDIF
	
	RETURN bInVisionCone
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_CURRENTLY_IN_CCTV_VISION_CONE(ELECTRONIC_PARAMS& sElectronicParams, FMMC_CCTV_DATA& sCCTVData, BOOL bElectronicsDisabled)

	IF DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_CCTV_CAMERA(sCCTVData)
		PRINTLN("[CCTV][CAM ", sElectronicParams.iCamIndex, "] IS_LOCAL_PLAYER_CURRENTLY_IN_CCTV_VISION_CONE | Player has valid disguise for this camera!")
		RETURN FALSE
	ENDIF
	
	RETURN IS_ENTITY_CURRENTLY_IN_CCTV_VISION_CONE(sElectronicParams, sCCTVData, LocalPlayerPed, bElectronicsDisabled)
ENDFUNC

PROC PROCESS_CCTV_REPORTED_BY_PEDS(ELECTRONIC_PARAMS& sElectronicParams, FMMC_CCTV_DATA& sCCTVData, BOOL bElectronicsDisabled, INT iAggroBS)
	
	// Can process the events other people send out still.
	IF DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(LocalPlayer)
	OR bIsSCTV
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(sCCTVData.iCCTVBS, ciCCTV_BS_CanAggro)		
		EXIT
	ENDIF	
	
	IF HAS_CCTV_CAMERA_SPOTTED_SOMETHING(sElectronicParams.iCamIndex, FALSE)
		EXIT
	ENDIF
	
	IF iAggroBS = ciMISSION_AGGRO_INDEX_DEFAULT
		EXIT
	ENDIF
	
	IF NOT bElectronicsDisabled AND HAS_TEAM_TRIGGERED_CCTV_FROM_PEDS_AGGRO_BITSET(MC_PlayerBD[iLocalPart].iTeam, iAggroBS)
		
		PRINTLN("PROCESS_CCTV_REPORTED_BY_PEDS - Checking Cam: ", sElectronicParams.iCamIndex, " HAS_TEAM_TRIGGERED_CCTV_FROM_PEDS_AGGRO_BITSET is true..")
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_RespectCCTVSpottedDelay)
		
			INT iTime = g_FMMC_STRUCT.iCCTVDelay			
			IF SHOULD_USE_LONG_CCTV_SPOTTED_DELAY(sCCTVData)
				iTime = ciCCTV_LONG_DELAY_SPOTTED
			ENDIF
			
			IF NOT HAS_NET_TIMER_STARTED(CCTVReportedByPedTimer[sElectronicParams.iCamIndex])
				PRINTLN("[CCTV][CAM ", sElectronicParams.iCamIndex, "][RCC MISSION] PROCESS_CCTV_REPORTED_BY_PEDS - Starting reported timer!")
				REINIT_NET_TIMER(CCTVReportedByPedTimer[sElectronicParams.iCamIndex])
			ENDIF
			
			IF NOT HAS_NET_TIMER_EXPIRED_READ_ONLY(CCTVReportedByPedTimer[sElectronicParams.iCamIndex], iTime)
				// Waiting for the spotted timer!
				EXIT
			ENDIF
		ENDIF
		
		PRINTLN("[CCTV][CAM ", sElectronicParams.iCamIndex, "][RCC MISSION] PROCESS_CCTV_REPORTED_BY_PEDS - Broadcasting reported event!")
		BROADCAST_FMMC_CCTV_EVENT(sElectronicParams.iCamIndex, CCTV_EVENT_TYPE__REPORTED_BY_PED, iAggroBS, LocalPlayer)
		SET_CCTV_CAMERA_REPORTED_BY_PED(sElectronicParams.iCamIndex)
		
	ELSE
		IF HAS_NET_TIMER_STARTED(CCTVReportedByPedTimer[sElectronicParams.iCamIndex])
			RESET_NET_TIMER(CCTVReportedByPedTimer[sElectronicParams.iCamIndex])
			PRINTLN("[CCTV][CAM ", sElectronicParams.iCamIndex, "][RCC MISSION] PROCESS_CCTV_REPORTED_BY_PEDS - Resetting reported timer!")
		ENDIF
	ENDIF
	
ENDPROC


PROC PROCESS_CCTV_SPOTTING_PLAYERS(ELECTRONIC_PARAMS& sElectronicParams, FMMC_CCTV_DATA& sCCTVData, BOOL bElectronicsDisabled, INT iAggroBS)
	
	// Can process the events other people send out still.
	IF DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(LocalPlayer)
	OR bIsSCTV
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(sCCTVData.iCCTVBS, ciCCTV_BS_CanAggro)	
		EXIT
	ENDIF
	
	IF HAS_CCTV_CAMERA_SPOTTED_SOMETHING(sElectronicParams.iCamIndex, FALSE)
		EXIT
	ENDIF

	IF IS_LOCAL_PLAYER_CURRENTLY_IN_CCTV_VISION_CONE(sElectronicParams, sCCTVData, bElectronicsDisabled)
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_RespectCCTVSpottedDelay)
		
			INT iTime = g_FMMC_STRUCT.iCCTVDelay			
			IF SHOULD_USE_LONG_CCTV_SPOTTED_DELAY(sCCTVData)
				iTime = ciCCTV_LONG_DELAY_SPOTTED
			ENDIF
			
			IF NOT HAS_NET_TIMER_STARTED(CCTVSpottingPlayerTimer[sElectronicParams.iCamIndex])
				PRINTLN("[CCTV][CAM ", sElectronicParams.iCamIndex, "][RCC MISSION] PROCESS_CCTV_SPOTTING_PLAYERS - Starting spotted timer!")
				REINIT_NET_TIMER(CCTVSpottingPlayerTimer[sElectronicParams.iCamIndex])
			ENDIF
			
			IF NOT HAS_NET_TIMER_EXPIRED_READ_ONLY(CCTVSpottingPlayerTimer[sElectronicParams.iCamIndex], iTime)
				// Waiting for the spotted timer!
				EXIT
			ENDIF
		ENDIF
		
		PRINTLN("[CCTV][CAM ", sElectronicParams.iCamIndex, "][RCC MISSION] PROCESS_CCTV_SPOTTING_PLAYERS - Broadcasting spotted event!")
		BROADCAST_FMMC_CCTV_EVENT(sElectronicParams.iCamIndex, CCTV_EVENT_TYPE__SPOTTED_PLAYER, iAggroBS, LocalPlayer)
		SET_CCTV_CAMERA_SPOTTED_PLAYER(sElectronicParams.iCamIndex)
		
	ELSE
		IF HAS_NET_TIMER_STARTED(CCTVSpottingPlayerTimer[sElectronicParams.iCamIndex])
			RESET_NET_TIMER(CCTVSpottingPlayerTimer[sElectronicParams.iCamIndex])
			PRINTLN("[CCTV][CAM ", sElectronicParams.iCamIndex, "][RCC MISSION] PROCESS_CCTV_SPOTTING_PLAYERS - Resetting spotted timer!")
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_CCTV_SPOTTING_BODIES(ELECTRONIC_PARAMS& sElectronicParams, FMMC_CCTV_DATA& sCCTVData, BOOL bElectronicsDisabled, INT iAggroBS)
	
	// Can process the events other people send out still.
	IF DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(LocalPlayer)
	OR bIsSCTV
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(sCCTVData.iCCTVBS, ciCCTV_BS_CCTVTriggerWithBody)	
		EXIT
	ENDIF
	
	IF HAS_CCTV_CAMERA_SPOTTED_SOMETHING(sElectronicParams.iCamIndex, FALSE)
		EXIT
	ENDIF
	
	IF iStaggeredCCTVPedToCheck[sElectronicParams.iCamIndex] > -1
	AND iStaggeredCCTVPedToCheck[sElectronicParams.iCamIndex] < MC_serverBD.iNumPedCreated
	
		iStaggeredCCTVPedToCheck[sElectronicParams.iCamIndex]++
		IF iStaggeredCCTVPedToCheck[sElectronicParams.iCamIndex] >= MC_serverBD.iNumPedCreated
			iStaggeredCCTVPedToCheck[sElectronicParams.iCamIndex] = 0
		ENDIF
		
		IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[iStaggeredCCTVPedToCheck[sElectronicParams.iCamIndex]])
			PED_INDEX tempPed = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iStaggeredCCTVPedToCheck[sElectronicParams.iCamIndex]])
			
			IF DOES_ENTITY_EXIST(tempPed)
			AND IS_PED_INJURED(tempPed)
				IF IS_ENTITY_CURRENTLY_IN_CCTV_VISION_CONE(sElectronicParams, sCCTVData, tempPed, bElectronicsDisabled)
					IF NOT HAS_NET_TIMER_STARTED(CCTVSpottingBodyTimer[sElectronicParams.iCamIndex])
						PRINTLN("[CCTV][CAM ", sElectronicParams.iCamIndex, "][RCC MISSION] PROCESS_CCTV_SPOTTING_BODIES - Starting body spotted timers due to seeing the body of ped ", iStaggeredCCTVPedToCheck[sElectronicParams.iCamIndex])
						START_NET_TIMER(CCTVSpottingBodyTimer[sElectronicParams.iCamIndex])
						iCCTVCameraSpottedPedBody[sElectronicParams.iCamIndex] = iStaggeredCCTVPedToCheck[sElectronicParams.iCamIndex]
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	INT iTime = g_FMMC_STRUCT.iCCTVDelay			
	IF SHOULD_USE_LONG_CCTV_SPOTTED_DELAY(sCCTVData)
		iTime = ciCCTV_LONG_DELAY_SPOTTED
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(CCTVSpottingBodyTimer[sElectronicParams.iCamIndex])
	AND HAS_NET_TIMER_EXPIRED_READ_ONLY(CCTVSpottingBodyTimer[sElectronicParams.iCamIndex], iTime)
		BROADCAST_FMMC_CCTV_EVENT(sElectronicParams.iCamIndex, CCTV_EVENT_TYPE__SPOTTED_PED_BODY, iAggroBS, DEFAULT, iCCTVCameraSpottedPedBody[sElectronicParams.iCamIndex])
		SET_CCTV_CAMERA_SPOTTED_BODY(sElectronicParams.iCamIndex)
	ENDIF
	
ENDPROC

PROC PROCESS_CCTV_TASER_RESPONSE(ELECTRONIC_PARAMS& sElectronicParams, BOOL bElectronicsDisabled)
	
	IF bElectronicsDisabled
		IF NOT HAS_ENTITY_REACTED_TO_TASER(sElectronicParams.iIndex, sElectronicParams.eEntityType)			
			SET_ENTITY_HAS_REACTED_TO_TASER(sElectronicParams.iIndex, sElectronicParams.eEntityType)
			PRINTLN("[CCTV][CAM ", sElectronicParams.iCamIndex, "][RCC MISSION][Taser] PROCESS_CCTV_TASER_RESPONSE - iCam: ", sElectronicParams.iCamIndex, " | Setting sObjTaserVars.iReactedToTaserBS!")
		
			//fCCTV_CurrentTargetLightIntensity[sElectronicParams.iCamIndex] = cf_CCTV_TaserFlashIntensity
		ENDIF
	ELSE
		IF HAS_ENTITY_REACTED_TO_TASER(sElectronicParams.iIndex, sElectronicParams.eEntityType)
			REINIT_NET_TIMER(CCTVTaserReEnablingTimer[sElectronicParams.iCamIndex])
			
			CLEAR_ENTITY_HAS_REACTED_TO_TASER(sElectronicParams.iIndex, sElectronicParams.eEntityType)
			PRINTLN("[CCTV][CAM ", sElectronicParams.iCamIndex, "][RCC MISSION][Taser] PROCESS_CCTV_TASER_RESPONSE - iCam: ", sElectronicParams.iCamIndex, " | Starting CCTVTaserReEnablingTimer")
		ENDIF
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(CCTVTaserReEnablingTimer[sElectronicParams.iCamIndex])
		IF HAS_NET_TIMER_EXPIRED(CCTVTaserReEnablingTimer[sElectronicParams.iCamIndex], ROUND(cf_CCTV_ReenableFlickerTime * 1000))
			RESET_NET_TIMER(CCTVTaserReEnablingTimer[sElectronicParams.iCamIndex])
			PRINTLN("[CCTV][CAM ", sElectronicParams.iCamIndex, "][RCC MISSION][Taser] PROCESS_CCTV_TASER_RESPONSE - iCam: ", sElectronicParams.iCamIndex, " | Fully re-enabled!")
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_CCTV_RULE_SKIPPING_WHEN_ALERTED(FMMC_CCTV_DATA& sCCTVData, INT iAggroBS)	
	IF sCCTVData.iCCTVSkipToRule > -1
	AND sCCTVData.iCCTVSkipToRule < FMMC_MAX_RULES
		INT iTeamToSkip
		FOR iTeamToSkip = 0 TO FMMC_MAX_TEAMS-1
			IF HAVE_CCTV_CAMERAS_WITH_AGGRO_BS_BEEN_ALERTED(iTeamToSkip, iAggroBS)
				IF sCCTVData.iCCTVSkipToRule > MC_serverBD_4.iCurrentHighestPriority[iTeamToSkip] AND MC_serverBD_4.iCurrentHighestPriority[iTeamToSkip] < FMMC_MAX_RULES
					ASSERTLN("FMMC2020 - PROCESS_CCTV_RULE_SKIPPING_WHEN_ALERTED - Rule Skip Used!")
					INVALIDATE_UP_TO_AND_INCLUDING_OBJECTIVE(iTeamToSkip, sCCTVData.iCCTVSkipToRule - 1, FALSE, TRUE, FALSE)
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
ENDPROC

PROC PROCESS_CCTV_DIALOGUE(ELECTRONIC_PARAMS& sElectronicParams)
	IF g_FMMC_STRUCT.sDialogueTriggers[iCCTVStaggeredDialogueTriggerIndex].fTriggerInCCTVRange > 0.0
		IF VDIST2(sElectronicParams.vElectronicCoords, GET_ENTITY_COORDS(LocalPlayerPed)) < POW(g_FMMC_STRUCT.sDialogueTriggers[iCCTVStaggeredDialogueTriggerIndex].fTriggerInCCTVRange, 2.0)
			FMMC_SET_LONG_BIT(iCCTVInDialogueRangeBS, iCCTVStaggeredDialogueTriggerIndex)
			
			#IF IS_DEBUG_BUILD
			IF bCCTVDebug
				DRAW_DEBUG_SPHERE(sElectronicParams.vElectronicCoords, g_FMMC_STRUCT.sDialogueTriggers[iCCTVStaggeredDialogueTriggerIndex].fTriggerInCCTVRange, 255, 0, 0, 100)
			ENDIF
			#ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
			IF bCCTVDebug
				DRAW_DEBUG_SPHERE(sElectronicParams.vElectronicCoords, g_FMMC_STRUCT.sDialogueTriggers[iCCTVStaggeredDialogueTriggerIndex].fTriggerInCCTVRange, 0, 0, 155, 100)
			ENDIF
			#ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_CCTV_MOVING_CAMERAS(ELECTRONIC_PARAMS& sElectronicParams, FMMC_CCTV_DATA& sCCTVData, INT& iCachedCamIndex, BOOL bExists, BOOL bIsAlive, BOOL bHasControl, INT iAggroBS)
	
	IF NOT IS_BIT_SET(sCCTVData.iCCTVBS, ciCCTV_BS_EnableCCTVMovement)
		EXIT
	ENDIF
	
	IF iCachedCamIndex = -1
		IF CACHE_CAM_INDEX_FOR_CCTV_ENTITY(sElectronicParams, iCachedCamIndex)
			IF GET_GROUND_Z_EXCLUDING_OBJECTS_FOR_3D_COORD(sElectronicParams.vElectronicCoords, fCCTVCameraCachedGroundZ[iCachedCamIndex], TRUE)
				PRINTLN("[CCTV][CAM ", iCachedCamIndex, "] PROCESS_CCTV_MOVING_CAMERAS | Caching ground Z as ", fCCTVCameraCachedGroundZ[iCachedCamIndex])
			ENDIF
		ENDIF
		
		EXIT
	ENDIF
	
	IF NOT SHOULD_THIS_CCTV_CAMERA_BE_PROCESSED_THIS_FRAME(iCachedCamIndex)
		EXIT
	ENDIF
	
	IF NOT bExists
	OR NOT bIsAlive
		CLEANUP_CCTV_CAMERA_SOUND_EFFECT_FOR_DEATH(sElectronicParams)
	ENDIF
	
	BOOL bElectronicsDisabled = SHOULD_ELECTRONIC_BE_DISABLED_THIS_FRAME(sElectronicParams)
	BOOL bSpotted, bAlerted

	#IF IS_DEBUG_BUILD
	PROCESS_CCTV_DEBUG(sElectronicParams, sCCTVData, iAggroBS)
	#ENDIF
	
	IF IS_BIT_SET(iCCTVBrokenBS, sElectronicParams.iCamIndex)
		PRINTLN("[CCTV][CAM ", sElectronicParams.iCamIndex, "] PROCESS_CCTV_MOVING_CAMERAS | Camera ", sElectronicParams.iCamIndex, " is dead or broken and we've already reacted to that")
		EXIT
	ENDIF
	
	IF bExists
	AND NOT bIsAlive
		DESTROY_CCTV_CAMERA(sElectronicParams, bHasControl, iAggroBS)
		EXIT
	ENDIF
	
	IF NOT bExists
		EXIT
	ENDIF
	
	PROCESS_CCTV_BLIP(sElectronicParams, sCCTVData)
	
	PROCESS_CCTV_TASER_RESPONSE(sElectronicParams, bElectronicsDisabled)
		
	PROCESS_CCTV_CAMERA_TURNING(sElectronicParams, sCCTVData, bHasControl)
		
	PROCESS_CCTV_CAMERA_LIGHT(sElectronicParams)
	
	PROCESS_CCTV_SPOTTING_PLAYERS(sElectronicParams, sCCTVData, bElectronicsDisabled, iAggroBS)
	
	PROCESS_CCTV_SPOTTING_BODIES(sElectronicParams, sCCTVData, bElectronicsDisabled, iAggroBS)
	
	PROCESS_CCTV_REPORTED_BY_PEDS(sElectronicParams, sCCTVData, bElectronicsDisabled, iAggroBS)
	
	PROCESS_CCTV_RULE_SKIPPING_WHEN_ALERTED(sCCTVData, iAggroBS)
	
	PROCESS_CCTV_VIEWCONE_MARKER(sElectronicParams, bSpotted, bAlerted)
	
	PROCESS_CCTV_DIALOGUE(sElectronicParams)
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_CCTVAlertSpottedSounds)
		PROCESS_CCTV_CAMERA_ALARM_SOUNDS(sElectronicParams, bSpotted, bAlerted, bElectronicsDisabled)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_TriggerAllCCTVCameras)
	AND IS_BIT_SET(MC_serverBD.iServerBitSet7, SBBOOL7_CCTV_TEAM_AGGROED_PEDS)
		IF NOT IS_BIT_SET(iLocalBoolCheck31, LBOOL31_HAS_TRIGGERED_ALL_CCTV_CAMERAS)
			PRINTLN("[CCTV][CAM ", sElectronicParams.iCamIndex, "][RCC MISSION][PROCESS_CCTV_MOVING_CAMERAS] Peds have been aggro'd calling TRIGGER_ALL_CCTV_CAMERAS.")
			TRIGGER_ALL_CCTV_CAMERAS()
		ENDIF
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Player Objective Processing
// ##### Description: Client and Server Player Objective Processing
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
				
PROC PROCESS_PLAYER_OBJECTIVES_SERVER_FOR_TEAM(INT iPlayerRule, INT iTeam)

	IF NOT HAS_TEAM_GOT_PLAYERS_FOR_NEXT_KILL_RULE(MC_serverBD_4.iPlayerRule[iPlayerRule][iTeam], iTeam)
		EXIT
	ENDIF
	
	IF MC_serverBD_4.iPlayerRule[iPlayerRule][iTeam] = FMMC_OBJECTIVE_LOGIC_NONE
		EXIT
	ENDIF
		
	IF MC_serverBD_4.iPlayerRulePriority[iPlayerRule][iTeam] >= FMMC_MAX_RULES
		EXIT
	ENDIF
		
	IF MC_serverBD_4.iPlayerRulePriority[iPlayerRule][iTeam] > iCurrentHighPriority[iTeam]
		//Has an objective but the team has not reached it yet
		EXIT
	ENDIF
	
	PRINTLN("[OBJECTIVE_ENTITIES] PROCESS_PLAYER_OBJECTIVES_SERVER_FOR_TEAM - Player Rule: ", iPlayerRule, " Team: ", iTeam, " Objective is valid - Assigning. Priority: ",
				MC_serverBD_4.iPlayerRulePriority[iPlayerRule][iTeam], " Rule: ", MC_serverBD_4.iPlayerRule[iPlayerRule][iTeam] )
	
	iCurrentHighPriority[iTeam] = MC_serverBD_4.iPlayerRulePriority[iPlayerRule][iTeam]
	iTempPlayerRuleMissionLogic[iTeam] = MC_serverBD_4.iPlayerRule[iPlayerRule][iTeam] 
	iTempCurrentPlayerRule[iTeam] = iPlayerRule
	
	IF iOldHighPriority[iTeam] > iCurrentHighPriority[iTeam]
		iNumHighPriorityPlayerRule[iTeam] = 0
		iOldHighPriority[iTeam] = iCurrentHighPriority[iTeam]
	ENDIF
	
	iNumHighPriorityPlayerRule[iTeam]++
	
	RESET_TEMP_OBJECTIVE_VARS(iTeam, ciRULE_TYPE_PLAYER)
				
ENDPROC

PROC PROCESS_PLAYER_OBJECTIVES_SERVER(INT iPlayerRule, INT iTeam)
	
	IF !bIsLocalPlayerHost
		EXIT
	ENDIF
	
	INT iTotalPlayerKills

	PROCESS_PLAYER_OBJECTIVES_SERVER_FOR_TEAM(iPlayerRule, iTeam)
		
	IF NOT IS_BIT_SET(MC_serverbd.iServerBitSet, SBBOOL_FIRST_UPDATE_DONE)
		EXIT
	ENDIF
	
	IF MC_serverBD_4.iPlayerRulePriority[iPlayerRule][iTeam] > MC_serverBD_4.iCurrentHighestPriority[iTeam]
	OR MC_serverBD_4.iCurrentHighestPriority[iTeam] >= FMMC_MAX_RULES
		EXIT
	ENDIF
	
	INT iSecondaryTeam = 0
	INT iPriority = FMMC_PRIORITY_IGNORE
	INT iNumMasks = 0, iTotalPlayingPlayers = 0 
	
	SWITCH MC_serverBD_4.iPlayerRule[iPlayerRule][iTeam]
		
		CASE FMMC_OBJECTIVE_LOGIC_KILL_PLAYERS
			
			FOR iSecondaryTeam = 0 TO FMMC_MAX_TEAMS - 1
				IF NOT DOES_TEAM_LIKE_TEAM(iTeam, iSecondaryTeam)
					iTotalPlayerKills += MC_serverBD.iTeamPlayerKills[iTeam][iSecondaryTeam] 
				ENDIF
			ENDFOR
			
			IF (iTotalPlayerKills >= MC_serverBD.iPlayerRuleLimit[iPlayerRule][iTeam])
			AND (MC_serverBD.iPlayerRuleLimit[iPlayerRule][iTeam] != 0 AND MC_serverBD.iPlayerRuleLimit[iPlayerRule][iTeam] != UNLIMITED_CAPTURES_KILLS)
			OR NOT IS_PLAYER_RULE_VALID(iTeam)
				PRINTLN("[PLAYERRULES] PROCESS_PLAYER_OBJECTIVES_SERVER - kill player rule complete setting to ignore")
				FOR iSecondaryTeam = 0 TO FMMC_MAX_TEAMS - 1
					MC_serverBD.iTeamPlayerKills[iTeam][iSecondaryTeam] = 0
				ENDFOR
				iPriority = MC_serverBD_4.iPlayerRulePriority[iPlayerRule][iTeam] 
				PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_PLAYER, iPlayerRule, iTeam, TRUE)
				MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_PLAYERS_KILLED
				SET_TEAM_OBJECTIVE_OUTCOME(iTeam, iPriority, TRUE)
				IF IS_THIS_OBJECTIVE_OVER(ci_TARGET_PLAYER, iTeam, iPriority)
					SET_LOCAL_OBJECTIVE_END_MESSAGE(iTeam,iPriority,MC_serverBD.iReasonForObjEnd[iTeam] )
					BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[iTeam], iTeam, iPriority, DID_TEAM_PASS_OBJECTIVE(iTeam, iPriority))
				ENDIF
			ENDIF
			
		BREAK
		
		CASE FMMC_OBJECTIVE_LOGIC_KILL_TEAM0
		CASE FMMC_OBJECTIVE_LOGIC_KILL_TEAM1
		CASE FMMC_OBJECTIVE_LOGIC_KILL_TEAM2
		CASE FMMC_OBJECTIVE_LOGIC_KILL_TEAM3
		
			iSecondaryTeam = MC_serverBD_4.iPlayerRule[iPlayerRule][iTeam] - FMMC_OBJECTIVE_LOGIC_KILL_TEAM0
			
			IF (MC_serverBD.iTeamPlayerKills[iTeam][iSecondaryTeam] >= MC_serverBD.iPlayerRuleLimit[iPlayerRule][iTeam])
			AND (MC_serverBD.iPlayerRuleLimit[iPlayerRule][iTeam] != 0 AND MC_serverBD.iPlayerRuleLimit[iPlayerRule][iTeam] != UNLIMITED_CAPTURES_KILLS)
			OR NOT IS_TEAM_ACTIVE(iSecondaryTeam)
				PRINTLN("[PLAYERRULES] PROCESS_PLAYER_OBJECTIVES_SERVER - kill team ", iSecondaryTeam, " rule complete setting to ignore")
				MC_serverBD.iTeamPlayerKills[iTeam][iSecondaryTeam] = 0
				iPriority = MC_serverBD_4.iPlayerRulePriority[iPlayerRule][iTeam]
				PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_PLAYER, iPlayerRule, iTeam, TRUE)
				MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_PLAYERS_KILLED
				SET_TEAM_OBJECTIVE_OUTCOME(iTeam, iPriority, TRUE)
				IF IS_THIS_OBJECTIVE_OVER(ci_TARGET_PLAYER, iTeam, iPriority)
					SET_LOCAL_OBJECTIVE_END_MESSAGE(iTeam, iPriority, MC_serverBD.iReasonForObjEnd[iTeam])
					BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[iTeam], iTeam, iPriority, DID_TEAM_PASS_OBJECTIVE(iTeam, iPriority))
				ENDIF
			ENDIF
			
		BREAK
		
		CASE FMMC_OBJECTIVE_LOGIC_GET_MASKS
			
			IF MC_serverBD.iPlayerRuleLimit[iPlayerRule][iTeam] = ciALL_TEAM_MASK
				
				IF MC_serverBD.iNumOfMasks[iTeam] >= MC_serverBD.iNumberOfPlayingPlayers[iTeam]
					PRINTLN("[PLAYERRULES] PROCESS_PLAYER_OBJECTIVES_SERVER - team player mask rule complete setting to ignore")
					iPriority = MC_serverBD_4.iPlayerRulePriority[iPlayerRule][iTeam] 
					PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_PLAYER, iPlayerRule, iTeam, TRUE)
					MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_PLAYERS_GOT_MASKS
					SET_TEAM_OBJECTIVE_OUTCOME(iTeam, iPriority, TRUE)
					IF IS_THIS_OBJECTIVE_OVER(ci_TARGET_PLAYER, iTeam, iPriority)
						SET_LOCAL_OBJECTIVE_END_MESSAGE(iTeam, iPriority, MC_serverBD.iReasonForObjEnd[iTeam])
						BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[iTeam], iTeam, iPriority, DID_TEAM_PASS_OBJECTIVE(iTeam, iPriority))
						BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_TEAM_MASK, 0, iTeam, iPlayerRule, INVALID_PLAYER_INDEX())
					ENDIF
					
				ENDIF
				
			ELSE
				
				iNumMasks = 0
				iTotalPlayingPlayers = 0
				FOR iSecondaryTeam = 0 TO FMMC_MAX_TEAMS - 1
					iNumMasks += MC_serverBD.iNumOfMasks[iSecondaryTeam]
					iTotalPlayingPlayers += MC_serverBD.iNumberOfPlayingPlayers[iSecondaryTeam]
				ENDFOR
				
				IF iNumMasks >= iTotalPlayingPlayers
					PRINTLN("[PLAYERRULES] PROCESS_PLAYER_OBJECTIVES_SERVER - ALL player mask rule complete setting to ignore")
					iPriority = MC_serverBD_4.iPlayerRulePriority[iPlayerRule][iTeam] 
					
					PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_PLAYER, iPlayerRule, iTeam, TRUE)
					MC_serverBD.iReasonForObjEnd[iTeam] = OBJ_END_REASON_PLAYERS_GOT_MASKS
					SET_TEAM_OBJECTIVE_OUTCOME(iTeam, iPriority, TRUE)
					
					IF IS_THIS_OBJECTIVE_OVER(ci_TARGET_PLAYER, iTeam, iPriority)
						SET_LOCAL_OBJECTIVE_END_MESSAGE(iTeam, iPriority, MC_serverBD.iReasonForObjEnd[iTeam])
						BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[iTeam], iTeam, iPriority, DID_TEAM_PASS_OBJECTIVE(iTeam, iPriority))
						BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_TEAM_MASK, 0, iTeam, iPlayerRule, INVALID_PLAYER_INDEX())
					ENDIF
					
				ENDIF
				
			ENDIF
		BREAK
		
		CASE FMMC_OBJECTIVE_LOGIC_LOOT_THRESHOLD
			PROCESS_LOOT_THRESHOLD_OBJECTIVE(iTeam, iPlayerRule)
		BREAK
		
		CASE FMMC_OBJECTIVE_LOGIC_POINTS_THRESHOLD
			PROCESS_POINTS_THRESHOLD_OBJECTIVE(iTeam, iPlayerRule)
		BREAK
		
		CASE FMMC_OBJECTIVE_LOGIC_SCRIPTED_CUTSCENE
			PROCESS_CUTSCENE_OBJECTIVE_LOGIC(iTeam, iPlayerRule)
		BREAK
		
		CASE FMMC_OBJECTIVE_LOGIC_GO_TO
			PROCESS_GO_TO_PLAYER_OBJECTIVE(iTeam, iPlayerRule)
		BREAK
		
		CASE FMMC_OBJECTIVE_LOGIC_GO_TO_TEAM0
		CASE FMMC_OBJECTIVE_LOGIC_GO_TO_TEAM1
		CASE FMMC_OBJECTIVE_LOGIC_GO_TO_TEAM2
		CASE FMMC_OBJECTIVE_LOGIC_GO_TO_TEAM3
			
			iSecondaryTeam = MC_serverBD_4.iPlayerRule[iPlayerRule][iTeam] - FMMC_OBJECTIVE_LOGIC_GO_TO_TEAM0
			PROCESS_GO_TO_PLAYER_OBJECTIVE(iTeam, iPlayerRule, iSecondaryTeam)

		BREAK
		
	ENDSWITCH

ENDPROC
