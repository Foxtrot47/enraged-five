// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### System: GTA V Online Mission Controller - Gang Chase --------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Script Name: FM_Mission_Controller_2020.sc ------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Creation and processing of Gang Chase Units
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### This script was forked from FM_Mission_Controller.sc at the beginning of 2020		 																				  
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

USING "FM_Mission_Controller_Include_2020.sch"

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Gang Chase Helpers
// ##### Description: Helpers and wrappers for general Gang Chase functionality
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC FMMC_GANG_CHASE_TYPE GET_CUSTOM_GANG_CHASE_TYPE(INT iGangType)

	INT i
	IF GET_GANG_CHASE_UNIT_CREATOR_CONFIG_INDEX_FROM_GANG_TYPE(iGangType, i)
		RETURN g_FMMC_STRUCT.sGangChaseUnitConfigs[i].eType
	ENDIF
	
	RETURN FMMC_GANG_CHASE_TYPE_NONE
	
ENDFUNC

FUNC BOOL IS_GANG_CHASE_TYPE_ACTIVE_ON_RULE(INT iGangType, INT iTeam, INT iRule)

	IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iGangBackupType[iRule] = iGangType
		RETURN TRUE
	ENDIF
	
	INT i
	FOR i = 0 TO FMMC_MAX_GANG_CHASE_UNIT_TYPES - 1
		IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sMultipleGangChaseSettings[iRule][i].iType = iGangType
			RETURN TRUE
		ENDIF
	ENDFOR
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_GANG_CHASE_AIR(INT iGangType)
	
	SWITCH iGangType		
		CASE ciBACKUP_TYPE_AIR_BUZZ_MERRYWEATHER
		CASE ciBACKUP_TYPE_AIR_BUZZ_NOOSE
		CASE ciBACKUP_TYPE_AIR_BUZZ_PROFESSIONALS
		CASE ciBACKUP_TYPE_AIR_MAVERICK_NOOSE
		CASE ciBACKUP_TYPE_AIR_MAVERICK_COP
		CASE ciBACKUP_TYPE_AIR_FROGGER_MERRYWEATHER
		CASE ciBACKUP_TYPE_AIR_FROGGER_PROFESSIONALS
		CASE ciBACKUP_TYPE_AIR_FROGGER_LOST
		CASE ciBACKUP_TYPE_AIR_FROGGER_VAGOS
		CASE ciBACKUP_TYPE_AIR_FROGGER_KOREAN
		CASE ciBACKUP_TYPE_AIR_VALKYRIE_MERRYWEATHER
		CASE ciBACKUP_TYPE_AIR_VALKYRIE_PROFESSIONALS
		CASE ciBACKUP_TYPE_AIR_HUNTER_MERRYWEATHER
		CASE ciBACKUP_TYPE_AIR_HUNTER_PROFESSIONALS
		CASE ciBACKUP_TYPE_AIR_SAVAGE_MERRYWEATHER
		CASE ciBACKUP_TYPE_AIR_SAVAGE_PROFESSIONALS
		CASE ciBACKUP_TYPE_AIR_LAZER_MERRYWEATHER
		CASE ciBACKUP_TYPE_AIR_MOLOTOK_MERRYWEATHER
		CASE ciBACKUP_TYPE_AIR_BUZZ_AVON_GOON
			RETURN TRUE	
		
		CASE ciBACKUP_TYPE_CUSTOM_1
		CASE ciBACKUP_TYPE_CUSTOM_2
		CASE ciBACKUP_TYPE_CUSTOM_3
		CASE ciBACKUP_TYPE_CUSTOM_4
			RETURN (GET_CUSTOM_GANG_CHASE_TYPE(iGangType) = FMMC_GANG_CHASE_TYPE_AIR)
		BREAK
		
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_GANG_CHASE_SEA(INT iGangType)
	
	SWITCH iGangType		
		CASE ciBACKUP_TYPE_BOAT_KOREANS_DINGHY
		CASE ciBACKUP_TYPE_BOAT_PROFESSIONALS_DINGHY
			RETURN TRUE
					
		CASE ciBACKUP_TYPE_CUSTOM_1
		CASE ciBACKUP_TYPE_CUSTOM_2
		CASE ciBACKUP_TYPE_CUSTOM_3
		CASE ciBACKUP_TYPE_CUSTOM_4
			RETURN (GET_CUSTOM_GANG_CHASE_TYPE(iGangType) = FMMC_GANG_CHASE_TYPE_SEA)
		BREAK
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC
	
FUNC INT GET_GANG_CHASE_PED_ID(INT iGangChaseUnit, INT iPed)
	RETURN ciMAX_GANG_CHASE_PEDS_PER_VEHICLE * iGangChaseUnit + iPed
ENDFUNC

FUNC BOOL IS_VEHICLE_A_GANG_CHASE_VEHICLE(VEHICLE_INDEX viVehicle)
	
	INT i
	FOR i = 0 TO ciMAX_GANG_CHASE_VEHICLES - 1
		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sGangChase[i].niVeh)
			RELOOP
		ENDIF
		
		IF NET_TO_VEH(MC_serverBD_1.sGangChase[i].niVeh) = viVehicle
			RETURN TRUE
		ENDIF
	ENDFOR
	
	RETURN FALSE
	
ENDFUNC

PROC APPLY_GANG_CHASE_INTENSITY_MODIFIERS(INT &iIntensity, INT iTeam, INT iRule)
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFifteen[iRule], ciBS_RULE15_PLAYER_ABILITY_DISTRACTION)
	AND IS_PLAYER_ABILITY_ACTIVE_FOR_ANY_PLAYER_ON_MY_TEAM(PA_DISTRACTION)
		iIntensity /= 2
	ENDIF
	
	iIntensity = CLAMP_INT(iIntensity, 0, FMMC_MAX_GANG_CHASE_INTENSITY)
	
ENDPROC

FUNC INT GET_GANG_CHASE_INTENSITY(INT iTeam, INT iRule)
	
	INT iIntensity = ciGANG_CHASE_DEFAULT_INTENSITY
	INT iGangBackupNumber = RETURN_INT_CUSTOM_VARIABLE_VALUE(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iGangBackupNumber[iRule])

	IF iGangBackupNumber > 0
	
		iIntensity = iGangBackupNumber
		
	ELIF iGangBackupNumber = ciGANG_CHASE_PLAYER_VAR_INTENSITY
		
		iIntensity = MC_serverBD_1.iVariableNumBackupAllowed[iTeam]
		
	ELIF iGangBackupNumber = ciGANG_CHASE_PLAYER_VAR_PLUS_ONE_INTENSITY
		
		iIntensity = CLAMP_INT(MC_serverBD_1.iVariableNumBackupAllowed[iTeam] + 1, 0, FMMC_MAX_GANG_CHASE_INTENSITY)
		
	ENDIF
	
	APPLY_GANG_CHASE_INTENSITY_MODIFIERS(iIntensity, iTeam, iRule)
	
	RETURN iIntensity
	
ENDFUNC

FUNC INT GET_GANG_CHASE_TOTAL_UNIT_SPAWNS(INT iTeam, INT iRule)
	
	INT iTotalNumber = RETURN_INT_CUSTOM_VARIABLE_VALUE(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iGangMaxBackup[iRule])
	
	IF iTotalNumber = ciFMMC_GANG_CHASE_INTENSITY_SPAWNS
		iTotalNumber = GET_GANG_CHASE_INTENSITY(iTeam, iRule)
	ELIF iTotalNumber = ciFMMC_GANG_CHASE_INFINITE_SPAWNS
		RETURN ciGANG_CHASE_MAX_TOTAL_SPAWNS
	ELIF iTotalNumber = ciFMMC_GANG_CHASE_OFF
		RETURN 0
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFourteen[iRule], ciBS_RULE14_GANG_CHASE_TOTAL_SPAWN_PLAYER_MULTIPLIER)
		iTotalNumber *= MC_serverBD_1.iVariableNumBackupAllowed[iTeam]
	ENDIF
	
	RETURN iTotalNumber
	
ENDFUNC

FUNC PLAYER_INDEX GET_GANG_CHASE_TARGET_PLAYER_INDEX(INT iGangChaseUnit)

	IF MC_serverBD_1.sGangChase[iGangChaseUnit].iTargetPart = ciGANG_CHASE_TARGET_NONE
		RETURN INVALID_PLAYER_INDEX()
	ENDIF
	
	PARTICIPANT_INDEX tempPart = INT_TO_PARTICIPANTINDEX(MC_serverBD_1.sGangChase[iGangChaseUnit].iTargetPart)
	IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
		RETURN INVALID_PLAYER_INDEX()
	ENDIF
	
	RETURN NETWORK_GET_PLAYER_INDEX(tempPart)

ENDFUNC

FUNC PED_INDEX GET_GANG_CHASE_TARGET_OVERRIDE_PED_INDEX()
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niPed[MC_serverBD_1.iGangChasePedTargetOverride])
		RETURN NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[MC_serverBD_1.iGangChasePedTargetOverride])
	ENDIF
	
	RETURN NULL
	
ENDFUNC

FUNC PED_INDEX GET_GANG_CHASE_TARGET_PED_INDEX(INT iGangChaseUnit)
	
	IF MC_serverBD_1.iGangChasePedTargetOverride != -1
		RETURN GET_GANG_CHASE_TARGET_OVERRIDE_PED_INDEX()
	ENDIF

	IF MC_serverBD_1.sGangChase[iGangChaseUnit].iTargetPart = ciGANG_CHASE_TARGET_NONE
		RETURN NULL
	ENDIF

	PLAYER_INDEX tempPlayer = GET_GANG_CHASE_TARGET_PLAYER_INDEX(iGangChaseUnit)
	IF tempPlayer = INVALID_PLAYER_INDEX()
		RETURN NULL
	ENDIF
	
	RETURN GET_PLAYER_PED(tempPlayer)
	
ENDFUNC

FUNC FLOAT GET_GANG_CHASE_INSTANT_CLEANUP_DISTANCE(INT iGangType)
	
	IF IS_GANG_CHASE_AIR(iGangType)
		RETURN g_FMMC_STRUCT.fGangChaseUnitInstantCleanupDistAir
	ELIF IS_GANG_CHASE_SEA(iGangType)
		RETURN g_FMMC_STRUCT.fGangChaseUnitInstantCleanupDistSea
	ELIF GET_CUSTOM_GANG_CHASE_TYPE(iGangType) = FMMC_GANG_CHASE_TYPE_ON_FOOT
		RETURN g_FMMC_STRUCT.fGangChaseUnitInstantCleanupDistOnFoot
	ENDIF
	
	RETURN g_FMMC_STRUCT.fGangChaseUnitInstantCleanupDistLand
	
ENDFUNC

FUNC FLOAT GET_GANG_CHASE_TIMED_CLEANUP_DISTANCE(INT iGangType)
	
	IF IS_GANG_CHASE_AIR(iGangType)
		RETURN g_FMMC_STRUCT.fGangChaseUnitTimedCleanupDistAir
	ELIF IS_GANG_CHASE_SEA(iGangType)
		RETURN g_FMMC_STRUCT.fGangChaseUnitTimedCleanupDistSea
	ELIF GET_CUSTOM_GANG_CHASE_TYPE(iGangType) = FMMC_GANG_CHASE_TYPE_ON_FOOT
		RETURN g_FMMC_STRUCT.fGangChaseUnitTimedCleanupDistOnFoot
	ENDIF
	
	RETURN g_FMMC_STRUCT.fGangChaseUnitTimedCleanupDistLand
	
ENDFUNC

FUNC FLOAT GET_GANG_CHASE_SPAWN_DISTANCE(INT iGangType)
	
	IF IS_GANG_CHASE_AIR(iGangType)
		RETURN g_FMMC_STRUCT.fGangChaseUnitSpawnDistAir
	ELIF IS_GANG_CHASE_SEA(iGangType)
		RETURN g_FMMC_STRUCT.fGangChaseUnitSpawnDistSea
	ELIF GET_CUSTOM_GANG_CHASE_TYPE(iGangType) = FMMC_GANG_CHASE_TYPE_ON_FOOT
		RETURN g_FMMC_STRUCT.fGangChaseUnitSpawnDistOnFoot
	ENDIF
	
	RETURN g_FMMC_STRUCT.fGangChaseUnitSpawnDistLand
	
ENDFUNC

FUNC NODE_FLAGS GET_GANG_CHASE_NODE_FLAGS(INT iGangType)
	
	NODE_FLAGS eNodeFlags = NF_NONE
	
	IF IS_GANG_CHASE_AIR(iGangType)
	OR IS_GANG_CHASE_SEA(iGangType)
		eNodeFlags |= NF_INCLUDE_BOAT_NODES
	ENDIF
	
	IF IS_GANG_CHASE_SEA(iGangType)
		eNodeFlags |= NF_INCLUDE_SWITCHED_OFF_NODES
	ENDIF
	
	RETURN eNodeFlags
	
ENDFUNC

FUNC BOOL DOES_NODE_HAVE_SUITABLE_GANG_CHASE_HEADING(VECTOR &vNearNode, FLOAT fNearHeading, FLOAT& fNodeHeading, INT iGangType, FLOAT fTolerance = 45.0, FLOAT fMaxDistance = 9999.0)
	
	NODE_FLAGS eNodeFlags = GET_GANG_CHASE_NODE_FLAGS(iGangType)
	VECTOR vNodePos
	FLOAT fReturnedHeading
	fNodeHeading = 0
	VECTOR vResult = <<0, 1, 0>>
	ROTATE_VECTOR_FMMC(vResult, <<0, 0, fNearHeading>>)
	VECTOR vFacePos = vNearNode + vResult
	
	IF NOT GET_NTH_CLOSEST_VEHICLE_NODE_FAVOUR_DIRECTION(vNearNode, vFacePos, 0, vNodePos, fReturnedHeading, eNodeFlags)
		PRINTLN("[GANG_CHASE] DOES_NODE_HAVE_SUITABLE_GANG_CHASE_HEADING - No Nodes.")
		RETURN FALSE
	ENDIF
	
	IF GET_ABSOLUTE_DELTA_HEADING(fNearHeading, fReturnedHeading) > fTolerance
		PRINTLN("[GANG_CHASE] DOES_NODE_HAVE_SUITABLE_GANG_CHASE_HEADING - GET_ABSOLUTE_DELTA_HEADING = ", GET_ABSOLUTE_DELTA_HEADING(fNearHeading, fReturnedHeading), " > ", fTolerance)
		RETURN FALSE
	ENDIF
	
	IF VDIST2(vNodePos, vNearNode) > POW(fMaxDistance, 2)
		PRINTLN("[GANG_CHASE] DOES_NODE_HAVE_SUITABLE_GANG_CHASE_HEADING - VDIST2(vNodePos, vNearNode) = ", VDIST2(vNodePos, vNearNode), " > ", POW(fMaxDistance, 2))
		RETURN FALSE
	ENDIF
	
	IF IS_GANG_CHASE_SEA(iGangType)
		INT iDensityOut, iPropertiesOut
		IF NOT GET_VEHICLE_NODE_PROPERTIES(vNodePos, iDensityOut, iPropertiesOut)
			PRINTLN("[GANG_CHASE] DOES_NODE_HAVE_SUITABLE_GANG_CHASE_HEADING - Trying to spawn boat and couldn't get properties!")
			RETURN FALSE
		ENDIF
		
		IF (iPropertiesOut & ENUM_TO_INT(VNP_WATER)) = 0
			PRINTLN("[GANG_CHASE] DOES_NODE_HAVE_SUITABLE_GANG_CHASE_HEADING - Trying to spawn boat and this isnt a water node!")
			RETURN FALSE
		ENDIF
	ENDIF
	
	fNodeHeading = fReturnedHeading
	vNearNode = vNodePos
	
	RETURN TRUE

ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Lose Gang Chase Objective Processing
// ##### Description: Functions for processing losing a Gang Chase as a mission objective.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL DOES_LOCAL_PLAYER_NEED_TO_LOSE_GANG_CHASE()
	
	INT iTeam = MC_playerBD[iLocalPart].iTeam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam] 
	
	IF iRule >= FMMC_MAX_RULES
		RETURN FALSE
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThree[iRule], ciBS_RULE3_LOSE_ALL_GANG_CHASE_VEHICLES)
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iGangBackupAtMidBitset, iRule)
	AND NOT IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iTeam], iRule)
		RETURN FALSE
	ENDIF
	
	IF NOT IS_BIT_SET(iLocalBoolCheck9, LBOOL9_AT_LEAST_ONE_GANG_CHASE_UNIT_SPAWNED)
		IF MC_serverBD_1.iNumGangChaseUnitsChasing[iTeam] = 0
			RETURN FALSE
		ENDIF
		SET_BIT(iLocalBoolCheck9, LBOOL9_AT_LEAST_ONE_GANG_CHASE_UNIT_SPAWNED)
	ENDIF
	
	INT iTotalNumber = GET_GANG_CHASE_TOTAL_UNIT_SPAWNS(iTeam, iRule)
	
	IF MC_serverBD_1.iNumGangChaseUnitsSpawned[iTeam][iRule] >= iTotalNumber
	AND MC_serverBD_1.iNumGangChaseUnitsChasing[iTeam] <= 0
		RETURN FALSE
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF GET_FRAME_COUNT() % 10 = 0
		PRINTLN("[GANG_CHASE] DOES_LOCAL_PLAYER_NEED_TO_LOSE_GANG_CHASE - Spawned: ", MC_serverBD_1.iNumGangChaseUnitsSpawned[iTeam][iRule], "/", iTotalNumber, " Chasing: ", MC_serverBD_1.iNumGangChaseUnitsChasing[iTeam])
	ENDIF
	#ENDIF
	
	RETURN TRUE
	
ENDFUNC

PROC PROCESS_LOSE_GANG_CHASE_OBJECTIVE()
	
	IF DOES_LOCAL_PLAYER_NEED_TO_LOSE_GANG_CHASE()
		
		IF NOT IS_BIT_SET(iLocalBoolCheck, LBOOL_CURRENT_OBJECTIVE_LOSE_GANG_CHASE)
			PRINTLN("[GANG_CHASE] PROCESS_LOSE_GANG_CHASE_OBJECTIVE - Setting as objective!")
			SET_BIT(iLocalBoolCheck, LBOOL_CURRENT_OBJECTIVE_LOSE_GANG_CHASE)
		ENDIF
		
		IF iSpectatorTarget = -1
		AND NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_LOSE_GANG_CHASE)
			SET_BIT(MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_LOSE_GANG_CHASE)
		ENDIF
		
		EXIT
		
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck, LBOOL_CURRENT_OBJECTIVE_LOSE_GANG_CHASE)
		PRINTLN("[GANG_CHASE] PROCESS_LOSE_GANG_CHASE_OBJECTIVE - Setting as no longer the objective!")
		CLEAR_BIT(iLocalBoolCheck, LBOOL_CURRENT_OBJECTIVE_LOSE_GANG_CHASE)
	ENDIF
	
	IF iSpectatorTarget = -1
	AND IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_LOSE_GANG_CHASE)
		CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_LOSE_GANG_CHASE)
	ENDIF

ENDPROC
		
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Gang Chase State Helpers
// ##### Description: Helper functiosn for setting a Gang Chase Unit's state
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

DEBUGONLY FUNC STRING GET_DEBUG_NAME_FOR_GANG_CHASE_UNIT_STATE(INT iState)
	SWITCH iState
		CASE ciGANG_CHASE_UNIT_STATE_INIT 				RETURN "ciGANG_CHASE_UNIT_STATE_INIT"
		CASE ciGANG_CHASE_UNIT_STATE_FIND_TARGET 		RETURN "ciGANG_CHASE_UNIT_STATE_FIND_TARGET"
		CASE ciGANG_CHASE_UNIT_STATE_WAIT_FOR_CREATION 	RETURN "ciGANG_CHASE_UNIT_STATE_WAIT_FOR_CREATION"
		CASE ciGANG_CHASE_UNIT_STATE_ACTIVE				RETURN "ciGANG_CHASE_UNIT_STATE_ACTIVE"
	ENDSWITCH
	
	RETURN "INVALID GANG_CHASE_UNIT_STATE"
ENDFUNC

PROC SET_GANG_CHASE_STATE_FOR_UNIT(INT iGangChaseUnit, INT iState)
	IF MC_serverBD_1.sGangChase[iGangChaseUnit].iGangChaseState = iState
		EXIT
	ENDIF
	   
	PRINTLN("[GANG_CHASE] SET_GANG_CHASE_STATE_FOR_UNIT - Unit: ", iGangChaseUnit, " Going from State ", GET_DEBUG_NAME_FOR_GANG_CHASE_UNIT_STATE(MC_serverBD_1.sGangChase[iGangChaseUnit].iGangChaseState), " to ", GET_DEBUG_NAME_FOR_GANG_CHASE_UNIT_STATE(iState))
	MC_serverBD_1.sGangChase[iGangChaseUnit].iGangChaseState = iState
ENDPROC			

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Gang Chase Cleanup
// ##### Description: Functions dealing with the deletion and cleanup of gang chase units.
// ##### Note: The actual deletion function is in the _Cleanup header - CLEANUP_GANG_CHASE_UNIT
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
FUNC FLOAT GET_MIN_DISTANCE_BETWEEN_UNITS(INT iGangType)
	
	IF IS_GANG_CHASE_AIR(iGangType)
		RETURN g_FMMC_STRUCT.fGangChaseUnitMinDistanceBetweenUnitsAir
	ELIF IS_GANG_CHASE_SEA(iGangType)
		RETURN g_FMMC_STRUCT.fGangChaseUnitMinDistanceBetweenUnitsSea
	ELIF GET_CUSTOM_GANG_CHASE_TYPE(iGangType) = FMMC_GANG_CHASE_TYPE_ON_FOOT
		RETURN g_FMMC_STRUCT.fGangChaseUnitMinDistanceBetweenUnitsOnFoot
	ENDIF
	
	RETURN g_FMMC_STRUCT.fGangChaseUnitMinDistanceBetweenUnitsLand
	
ENDFUNC

PROC SET_GANG_CHASE_UNIT_RESPAWN_TIME(INT iGangChaseUnit, INT iTeam)
	
	IF iTeam < 0
		EXIT
	ENDIF
	
	IF MC_serverBD_4.iCurrentHighestPriority[iTeam] >= FMMC_MAX_RULES
		EXIT
	ENDIF
	
	IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iGangChaseRespawnDelay[MC_serverBD_4.iCurrentHighestPriority[iTeam]] = 0
		EXIT
	ENDIF
	
	MC_serverBD_1.iNextGangChaseRespawnTime = NATIVE_TO_INT(GET_NETWORK_TIME()) + (g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iGangChaseRespawnDelay[MC_serverBD_4.iCurrentHighestPriority[iTeam]] * 1000)
	
	PRINTLN("[GANG_CHASE] SET_GANG_CHASE_UNIT_RESPAWN_TIME - Unit: ", iGangChaseUnit, " can next spawn at: ", MC_serverBD_1.iNextGangChaseRespawnTime)

ENDPROC

PROC RESET_GANG_CHASE_DATA(INT iGangChaseUnit)

	PRINTLN("[GANG_CHASE] RESET_GANG_CHASE_DATA - Unit: ", iGangChaseUnit)
	
	SET_GANG_CHASE_STATE_FOR_UNIT(iGangChaseUnit, ciGANG_CHASE_UNIT_STATE_INIT)
	MC_serverBD_1.sGangChase[iGangChaseUnit].iSpawnTeam = -1
	MC_serverBD_1.sGangChase[iGangChaseUnit].iTargetPart = ciGANG_CHASE_TARGET_NONE
	MC_serverBD_1.sGangChase[iGangChaseUnit].iBitset = 0
	MC_serverBD_1.sGangChase[iGangChaseUnit].iGangType = ciBACKUP_TYPE_NONE
	MC_serverBD_1.sGangChase[iGangChaseUnit].iFleeDist = g_FMMC_STRUCT.iGangChaseUnitFleeDistance
	RESET_NET_TIMER(MC_serverBD_1.sGangChase[iGangChaseUnit].tdCleanupTimer)
	
ENDPROC

PROC RESET_LOCAL_GANG_CHASE_SPAWN_DATA(INT iGangChaseUnit)
	
	MC_PlayerBD[iLocalPart].vGangChaseSpawnCoords = <<0.0, 0.0, 0.0>>
	MC_PlayerBD[iLocalPart].iGangChaseChosenType = -1
	IF iSearchingForGangChase != -1
	AND (iSearchingForGangChase = iGangChaseUnit OR iGangChaseUnit = -1) 
		PRINTLN("[GANG_CHASE] RESET_LOCAL_GANG_CHASE_SPAWN_DATA - Unit: ", iGangChaseUnit, " Wiping iSearchingForGangChase, was: ", iSearchingForGangChase)
		vGangChaseSearchPos = <<0,0,0>>
		iSearchingForGangChase = -1
	ENDIF
	vGangChaseSpawnDirection = <<0.0, 0.0, 0.0>>
	iGangChaseForwardSpawnAttempts = 0
	iGangChaseTotalSpawnAttempts = 0
	iGangChaseAirStandardSpawnAttempts = 0
	
	IF iGangChaseUnit != -1
		CLEAR_BIT(MC_PlayerBD[iLocalPart].iGangChaseSpawnBitset, iGangChaseUnit)
		CLEAR_BIT(MC_PlayerBD[iLocalPart].iGangChaseSpawnBitset, PBGANGSPAWN_UNIT_0_TRY_FORWARDS + iGangChaseUnit)
		CLEAR_BIT(MC_PlayerBD[iLocalPart].iGangChaseSpawnBitset, PBGANGSPAWN_UNIT_0_SUCCEED_FORWARDS + iGangChaseUnit)
		CLEAR_BIT(MC_PlayerBD[iLocalPart].iGangChaseSpawnBitset, PBGANGSPAWN_UNIT_0_AIR_FAILED_HELISPAWN + iGangChaseUnit)
	ELSE
		MC_PlayerBD[iLocalPart].iGangChaseSpawnBitset = 0
	ENDIF
	
ENDPROC

FUNC BOOL IS_GANG_CHASE_UNIT_DEAD(INT iGangChaseUnit)
	
	INT iPed
	FOR iPed = 0 TO ciMAX_GANG_CHASE_PEDS_PER_VEHICLE - 1
		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sGangChase[iGangChaseUnit].niPeds[iPed])
			RELOOP
		ENDIF
		
		IF NOT IS_NET_PED_INJURED(MC_serverBD_1.sGangChase[iGangChaseUnit].niPeds[iPed])
			RETURN FALSE
		ENDIF
	ENDFOR
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL IS_GANG_CHASE_UNIT_DEAD_GET_FIRST_ALIVE(INT iGangChaseUnit, NETWORK_INDEX &niToReturn)
	
	niToReturn = NULL
	INT iPed
	FOR iPed = 0 TO ciMAX_GANG_CHASE_PEDS_PER_VEHICLE - 1
		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sGangChase[iGangChaseUnit].niPeds[iPed])
			RELOOP
		ENDIF
		
		IF NOT IS_NET_PED_INJURED(MC_serverBD_1.sGangChase[iGangChaseUnit].niPeds[iPed])
			niToReturn = MC_serverBD_1.sGangChase[iGangChaseUnit].niPeds[iPed]
			RETURN FALSE
		ENDIF
	ENDFOR
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL HAS_GANG_CHASE_UNIT_CLEANED_UP(INT iGangChaseUnit)

	IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sGangChase[iGangChaseUnit].niVeh)
		RETURN FALSE
	ENDIF

	INT iPed
	FOR iPed = 0 TO ciMAX_GANG_CHASE_PEDS_PER_VEHICLE - 1
		IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sGangChase[iGangChaseUnit].niPeds[iPed])
			RETURN FALSE
		ENDIF
	ENDFOR
	
	RETURN TRUE

ENDFUNC

PROC TASK_GANG_CHASE_UNIT_FLEE_COORDS(INT iGangChaseUnit, VECTOR vCoords, BOOL bBlockNonTempEvents = FALSE)
	
	INT iPed
	FOR iPed = 0 TO ciMAX_GANG_CHASE_PEDS_PER_VEHICLE - 1
		
		IF IS_NET_PED_INJURED(MC_serverBD_1.sGangChase[iGangChaseUnit].niPeds[iPed])
			RELOOP
		ENDIF
		
		PED_INDEX tempPed = NET_TO_PED(MC_serverBD_1.sGangChase[iGangChaseUnit].niPeds[iPed])
			
		IF IS_PED_PERFORMING_TASK(tempPed, SCRIPT_TASK_SMART_FLEE_POINT)
			RELOOP
		ENDIF
		
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed, bBlockNonTempEvents)
				
		SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_ALWAYS_FIGHT, FALSE)
		SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_ALWAYS_FLEE, TRUE)
		
		SET_PED_FLEE_ATTRIBUTES(tempPed, FA_DISABLE_COWER, TRUE)
		SET_PED_FLEE_ATTRIBUTES(tempPed, FA_COWER_INSTEAD_OF_FLEE, FALSE)
		SET_PED_FLEE_ATTRIBUTES(tempPed, FA_DISABLE_FLEE_FROM_INDIRECT_THREATS, FALSE)
				
		IF IS_VECTOR_ZERO(vCoords)
			PRINTLN("[GANG_CHASE] TASK_GANG_CHASE_UNIT_FLEE_COORDS - Unit: ", iGangChaseUnit, " Ped: ", iPed, " is not being tasked to flee but Combat Attributes set!")
			EXIT
		ENDIF
				
		TASK_SMART_FLEE_COORD(tempPed, vCoords, 5000, -1)
		   
		PRINTLN("[GANG_CHASE] TASK_GANG_CHASE_UNIT_FLEE_COORDS - Unit: ", iGangChaseUnit, " Ped: ", iPed, " is tasked to flee.")
		                
	ENDFOR
	
ENDPROC

FUNC BOOL SHOULD_OVERRIDE_FLEE_FOR_GANG_CHASE_UNIT(INT iGangChaseUnit, PED_INDEX piPed)
	
	INT iTeam = MC_serverBD_1.sGangChase[iGangChaseUnit].iSpawnTeam
	IF iTeam < 0
		RETURN FALSE
	ENDIF
	
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	IF iRule >= FMMC_MAX_RULES
		RETURN FALSE
	ENDIF
	
	IF IS_VECTOR_ZERO(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vDropOff[iRule])
		RETURN FALSE
	ENDIF
	
	VECTOR vDropOff = GET_DROP_OFF_CENTER_FOR_TEAM(DEFAULT, iTeam, MC_serverBD_1.sGangChase[iGangChaseUnit].iTargetPart)
	IF VDIST2(vDropOff, GET_ENTITY_COORDS(piPed, FALSE)) >= POW(TO_FLOAT(g_FMMC_STRUCT.iGangChaseUnitFleeDistance), 2)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

PROC CLEANUP_BACKUP_UNIT(INT iGangChaseUnit, BOOL bFlee = FALSE)
	
	PRINTLN("[GANG_CHASE] CLEANUP_BACKUP_UNIT - Unit: ", iGangChaseUnit, " Flee: ", BOOL_TO_STRING(bFlee))
	
	IF IS_GANG_CHASE_UNIT_DEAD(iGangChaseUnit)
		MC_serverBD_4.iGangChasePedsKilledThisRule++
	ENDIF
	
	INT iSpawnTeam = MC_serverBD_1.sGangChase[iGangChaseUnit].iSpawnTeam

	IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sGangChase[iGangChaseUnit].niVeh)
		PRINTLN("[GANG_CHASE] CLEANUP_BACKUP_UNIT - Unit: ", iGangChaseUnit, " Vehicle being cleaned up.")
		CLEANUP_NET_ID(MC_serverBD_1.sGangChase[iGangChaseUnit].niVeh)
	ENDIF
	
	//Cleanup collision on Ped 0
	PED_INDEX piCollisionPed
	IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sGangChase[iGangChaseUnit].niPeds[0])
		piCollisionPed = NET_TO_PED(MC_serverBD_1.sGangChase[iGangChaseUnit].niPeds[0])
	ENDIF
	CLEANUP_COLLISION_ON_GANG_CHASE_PED(iGangChaseUnit, piCollisionPed)
	
	INT iPed
	FOR iPed = 0 TO ciMAX_GANG_CHASE_PEDS_PER_VEHICLE - 1
	
		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sGangChase[iGangChaseUnit].niPeds[iPed])
			RELOOP
		ENDIF
	
		PED_INDEX tempPed = NET_TO_PED(MC_serverBD_1.sGangChase[iGangChaseUnit].niPeds[iped])
		
		IF !bFlee
		AND SHOULD_OVERRIDE_FLEE_FOR_GANG_CHASE_UNIT(iGangChaseUnit, tempPed)
			bFlee = TRUE
			PRINTLN("[GANG_CHASE] CLEANUP_BACKUP_UNIT - Unit: ", iGangChaseUnit, " Ped: ", iPed, " overriding flee to TRUE.")
		ENDIF
		
		IF bFlee
		AND MC_serverBD_1.sGangChase[iGangChaseUnit].iSpawnTeam != -1
			VECTOR vDropOff = GET_DROP_OFF_CENTER_FOR_TEAM(DEFAULT, MC_serverBD_1.sGangChase[iGangChaseUnit].iSpawnTeam, MC_serverBD_1.sGangChase[iGangChaseUnit].iTargetPart)
			TASK_GANG_CHASE_UNIT_FLEE_COORDS(iGangChaseUnit, vDropOff)
		ENDIF
		
		PRINTLN("[GANG_CHASE] CLEANUP_BACKUP_UNIT - Unit: ", iGangChaseUnit, " Ped: ", iPed, " being cleaned up.")
		CLEANUP_NET_ID(MC_serverBD_1.sGangChase[iGangChaseUnit].niPeds[iPed])
		
	ENDFOR
	
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sGangChase[iGangChaseUnit].niVeh)
		
		IF HAS_GANG_CHASE_UNIT_CLEANED_UP(iGangChaseUnit)
			PRINTLN("[GANG_CHASE] PROCESS_SERVER_GANG_CHASE_UNIT_STATE_ACTIVE - Unit: ", iGangChaseUnit, " Cleaned up")
			
			IF IS_BIT_SET(MC_serverBD_1.sGangChase[iGangChaseUnit].iBitset, SBBBOOL_RESERVATION_COMPLETE)
				PRINTLN("[GANG_CHASE] PROCESS_SERVER_GANG_CHASE_UNIT_STATE_ACTIVE - Unit: ", iGangChaseUnit, " Removing reserved entities.")
				IF GET_CUSTOM_GANG_CHASE_TYPE(MC_serverBD_1.sGangChase[iGangChaseUnit].iGangType) != FMMC_GANG_CHASE_TYPE_ON_FOOT
					RESERVE_NETWORK_MISSION_VEHICLES(GET_NUM_RESERVED_MISSION_VEHICLES() - 1)		
				ENDIF
				RESERVE_NETWORK_MISSION_PEDS(GET_NUM_RESERVED_MISSION_PEDS() - MC_serverBD_1.sGangChase[iGangChaseUnit].iMaxPeds)
			ENDIF
			
			IF iSpawnTeam != -1
			AND IS_BIT_SET(MC_serverBD_1.sGangChase[iGangChaseUnit].iBitset, SBBBOOL_CREATED)
				MC_serverBD_1.iNumGangChaseUnitsChasing[iSpawnTeam]--
				PRINTLN("[GANG_CHASE] CLEANUP_BACKUP_UNIT - Unit: ", iGangChaseUnit, " Num chasing team ", iSpawnTeam, ": ", MC_serverBD_1.iNumGangChaseUnitsChasing[iSpawnTeam])
			ENDIF
			
			SET_GANG_CHASE_STATE_FOR_UNIT(iGangChaseUnit, ciGANG_CHASE_UNIT_STATE_INIT)
			RESET_GANG_CHASE_DATA(iGangChaseUnit)
			SET_GANG_CHASE_UNIT_RESPAWN_TIME(iGangChaseUnit, iSpawnTeam)
			
		ENDIF
		
	ENDIF

ENDPROC

PROC RESET_GANG_CHASE_UNIT_SPAWN_DATA(INT iGangChaseUnit)

	PRINTLN("[GANG_CHASE] RESET_GANG_CHASE_UNIT_SPAWN_DATA - Unit: ", iGangChaseUnit)
	
	IF IS_BIT_SET(MC_serverBD_1.sGangChase[iGangChaseUnit].iBitset, SBBBOOL_RESERVATION_COMPLETE)
		PRINTLN("[GANG_CHASE] RESET_GANG_CHASE_UNIT_SPAWN_DATA - Unit: ", iGangChaseUnit, " had started spawning - cleaning up")
		CLEANUP_BACKUP_UNIT(iGangChaseUnit, TRUE)
	ENDIF
	
	MC_serverBD_1.iCurrentGangChaseUnitSpawn = -1
	vGangChaseSpawnDirection = <<0.0, 0.0, 0.0>>
	REINIT_NET_TIMER(tdGangChaseTargetTimer[iGangChaseUnit])
	MC_serverBD_1.sGangChase[iGangChaseUnit].iSpawnTeam = -1
	SET_GANG_CHASE_STATE_FOR_UNIT(iGangChaseUnit, ciGANG_CHASE_UNIT_STATE_INIT)
	MC_serverBD_1.sGangChase[iGangChaseUnit].iGangType = ciBACKUP_TYPE_NONE
	CLEAR_BIT(MC_serverBD_1.sGangChase[iGangChaseUnit].iBitset, SBBBOOL_SELECTED_GANG_TYPE)

ENDPROC

FUNC BOOL SHOULD_GANG_CHASE_UNIT_CLEANUP(INT iGangChaseUnit, BOOL &bFlee, BOOL &bTimedCleanup)

	bFlee = FALSE
	bTimedCleanup = FALSE
	
	INT iTargetTeam = MC_serverBD_1.sGangChase[iGangChaseUnit].iSpawnTeam
	IF iTargetTeam = -1
		RETURN TRUE
	ENDIF
	
	INT iTargetRule = MC_serverBD_4.iCurrentHighestPriority[iTargetTeam]
	IF iTargetRule >= FMMC_MAX_RULES
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(MC_serverBD_1.sGangChase[iGangChaseUnit].iBitset, SBBBOOL_DESPAWN_ON_NEW_TYPE)
	AND g_FMMC_STRUCT.sFMMCEndConditions[iTargetTeam].iGangBackupType[iTargetRule] != ciBACKUP_TYPE_NONE
	AND NOT IS_GANG_CHASE_TYPE_ACTIVE_ON_RULE(MC_serverBD_1.sGangChase[iGangChaseUnit].iGangType, iTargetTeam, iTargetRule)
		bFlee = TRUE
		RETURN TRUE
	ENDIF

	IF MC_serverBD_1.sGangChase[iGangChaseUnit].iTargetPart = ciGANG_CHASE_TARGET_NONE
		bFlee = TRUE
		RETURN TRUE
	ENDIF
				
	PED_INDEX tempPed = GET_GANG_CHASE_TARGET_PED_INDEX(iGangChaseUnit)
			
	IF tempPed = NULL
		RETURN TRUE
	ENDIF
								
	IF IS_PED_INJURED(tempPed)
		RETURN TRUE
	ENDIF
	
	IF IS_GANG_CHASE_UNIT_DEAD(iGangChaseUnit)
		RETURN TRUE
	ENDIF
	
	FLOAT fDist2 = 0.0
	
	IF GET_CUSTOM_GANG_CHASE_TYPE(MC_serverBD_1.sGangChase[iGangChaseUnit].iGangType) = FMMC_GANG_CHASE_TYPE_ON_FOOT
	OR NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sGangChase[iGangChaseUnit].niVeh)
		
		INT iPed
		FOR iPed = 0 TO ciMAX_GANG_CHASE_PEDS_PER_VEHICLE - 1
		
			IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sGangChase[iGangChaseUnit].niPeds[iPed])
				RELOOP
			ENDIF
			
			IF IS_NET_PED_INJURED(MC_serverBD_1.sGangChase[iGangChaseUnit].niPeds[iPed])
				RELOOP
			ENDIF
			
			fDist2 = GET_DIST2_BETWEEN_ENTITIES(NET_TO_ENT(MC_serverBD_1.sGangChase[iGangChaseUnit].niPeds[iPed]), tempPed)
			
			IF fDist2 > POW(GET_GANG_CHASE_INSTANT_CLEANUP_DISTANCE(MC_serverBD_1.sGangChase[iGangChaseUnit].iGangType), 2)
				PRINTLN("[GANG_CHASE] SHOULD_GANG_CHASE_UNIT_CLEANUP - Unit: ", iGangChaseUnit, " Instant cleanup (ped: ", iPed, ")!")
				RETURN TRUE
			ENDIF
			
			IF fDist2 > POW(GET_GANG_CHASE_TIMED_CLEANUP_DISTANCE(MC_serverBD_1.sGangChase[iGangChaseUnit].iGangType), 2)
				
				IF NOT HAS_NET_TIMER_STARTED(MC_serverBD_1.sGangChase[iGangChaseUnit].tdCleanupTimer)
					PRINTLN("[GANG_CHASE] SHOULD_GANG_CHASE_UNIT_CLEANUP - Unit: ", iGangChaseUnit, " Starting cleanup timer (ped: ", iPed, ")!")
					REINIT_NET_TIMER(MC_serverBD_1.sGangChase[iGangChaseUnit].tdCleanupTimer)
				ENDIF
				
				IF HAS_NET_TIMER_EXPIRED(MC_serverBD_1.sGangChase[iGangChaseUnit].tdCleanupTimer, ciGANG_CHASE_TIMED_CLEANUP_TIME)
					PRINTLN("[GANG_CHASE] SHOULD_GANG_CHASE_UNIT_CLEANUP - Unit: ", iGangChaseUnit, " Timer expired (ped: ", iPed, ")!")
					RETURN TRUE
				ENDIF
				
				bTimedCleanup = TRUE
				RETURN FALSE
				
			ENDIF
			
		ENDFOR
			
	ELSE
		
		fDist2 = GET_DIST2_BETWEEN_ENTITIES(NET_TO_ENT(MC_serverBD_1.sGangChase[iGangChaseUnit].niVeh), tempPed)
		
		IF fDist2 > POW(GET_GANG_CHASE_INSTANT_CLEANUP_DISTANCE(MC_serverBD_1.sGangChase[iGangChaseUnit].iGangType), 2)
			PRINTLN("[GANG_CHASE] SHOULD_GANG_CHASE_UNIT_CLEANUP - Unit: ", iGangChaseUnit, " instant cleanup!")
			RETURN TRUE
		ENDIF
		
		IF fDist2 > POW(GET_GANG_CHASE_TIMED_CLEANUP_DISTANCE(MC_serverBD_1.sGangChase[iGangChaseUnit].iGangType), 2)
			
			IF NOT HAS_NET_TIMER_STARTED(MC_serverBD_1.sGangChase[iGangChaseUnit].tdCleanupTimer)
				PRINTLN("[GANG_CHASE] SHOULD_GANG_CHASE_UNIT_CLEANUP - Unit: ", iGangChaseUnit, " Starting cleanup timer!")
				REINIT_NET_TIMER(MC_serverBD_1.sGangChase[iGangChaseUnit].tdCleanupTimer)
			ENDIF
			
			IF HAS_NET_TIMER_EXPIRED(MC_serverBD_1.sGangChase[iGangChaseUnit].tdCleanupTimer, ciGANG_CHASE_TIMED_CLEANUP_TIME)
				PRINTLN("[GANG_CHASE] SHOULD_GANG_CHASE_UNIT_CLEANUP - Unit: ", iGangChaseUnit, " timer expired!")
				RETURN TRUE
			ENDIF
			
			bTimedCleanup = TRUE
			
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Blips and UI
// ##### Description: All functions dealing with gang chase UI such as creating and maintaining blips
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
				
PROC CLEANUP_GANG_CHASE_PED_BLIP(INT iGangChaseUnit, INT iPed)
	
	IF IS_BIT_SET(MC_serverBD_1.sGangChase[iGangChaseUnit].iBitset, SBBBOOL_USES_COP_BLIPS)
		EXIT
	ENDIF
	
	INT iBlipIndex = GET_GANG_CHASE_PED_ID(iGangChaseUnit, iPed)
	
	IF biGangChasePedBlip[iBlipIndex].PedID = NULL
		EXIT
	ENDIF
	   
	PRINTLN("[GANG_CHASE] CLEANUP_GANG_CHASE_PED_BLIP - Cleaning up blip for Unit: ", iGangChaseUnit, " Ped: ", iPed)
	                
	CLEANUP_AI_PED_BLIP(biGangChasePedBlip[iBlipIndex])

ENDPROC

FUNC BOOL SHOULD_GANG_CHASE_BLIPS_BE_FORCED_ON(INT iTeam, INT iRule)
	
	IF IS_CURRENT_OBJECTIVE_LOSE_GANG_CHASE()
		RETURN TRUE
	ENDIF
	
	RETURN IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFive[iRule], ciBS_RULE5_GANG_CHASE_BLIP_FORCED_ON)
		
ENDFUNC

FUNC FLOAT GET_GANG_CHASE_BLIPS_NOTICABLE_RADIUS(INT iTeam, INT iRule)
	
	IF IS_CURRENT_OBJECTIVE_LOSE_GANG_CHASE()
		RETURN -1.0 //Default value - see UPDATE_ENEMY_NET_PED_BLIP
	ENDIF
	
	RETURN g_FMMC_STRUCT.sFMMCEndConditions[iTeam].fGangBlipNoticeRange[iRule]
		
ENDFUNC

PROC PROCESS_GANG_CHASE_PED_BLIPS(INT iGangChaseUnit, INT iPed) 
	
	IF IS_BIT_SET(MC_serverBD_1.sGangChase[iGangChaseUnit].iBitset, SBBBOOL_USES_COP_BLIPS)
		//If this is on gang chase blips don't need processed. 		
		EXIT
	ENDIF
	
	INT iTeam = MC_playerBD[iPartToUse].iTeam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	IF iRule >= FMMC_MAX_RULES
	OR IS_NET_PED_INJURED(MC_serverBD_1.sGangChase[iGangChaseUnit].niPeds[iPed])
		CLEANUP_GANG_CHASE_PED_BLIP(iGangChaseUnit, iPed)	
		EXIT
	ENDIF
	
	INT iBlipIndex = GET_GANG_CHASE_PED_ID(iGangChaseUnit, iPed)
	
	PED_INDEX piPed = NET_TO_PED(MC_serverBD_1.sGangChase[iGangChaseUnit].niPeds[iPed])

	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetNine[iRule], ciBS_RULE9_ONLY_BLIP_IN_INTERIOR)
	AND NOT IS_ENTITY_IN_SAME_INTERIOR_AS_LOCAL_PLAYER(piPed)
		EXIT
	ENDIF
			
	BOOL bForceBlipOn = SHOULD_GANG_CHASE_BLIPS_BE_FORCED_ON(iTeam, iRule)
	FLOAT fNoticableRadius	= GET_GANG_CHASE_BLIPS_NOTICABLE_RADIUS(iTeam, iRule)
	
	UPDATE_ENEMY_NET_PED_BLIP(MC_serverBD_1.sGangChase[iGangChaseUnit].niPeds[iPed], biGangChasePedBlip[iBlipIndex], fNoticableRadius, DEFAULT, bForceBlipOn)		
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Gang Chase Collision Loading
// ##### Description: Functions for loading collision around Gang Chase peds.
// ##### Note the collision cleanup functions are in the _Cleanup header
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
				
PROC LOAD_NAVMESH_ON_GANG_CHASE_PED_IF_POSSIBLE(INT iGangChaseUnit, PED_INDEX piPed)
	
	IF IS_BIT_SET(iGangChasePedNavmeshCheckedBS, iGangChaseUnit)
		EXIT
	ENDIF
	
	IF NOT IS_IT_SAFE_TO_LOAD_NAVMESH()
		PRINTLN("[GANG_CHASE] LOAD_NAVMESH_ON_GANG_CHASE_PED_IF_POSSIBLE - Unit: ", iGangChaseUnit, " Not safe to load Navmesh!")
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck8, LBOOL8_NAVMESH_REGION_REQUESTED)
		PRINTLN("[GANG_CHASE] LOAD_NAVMESH_ON_GANG_CHASE_PED_IF_POSSIBLE - Unit: ", iGangChaseUnit, " Navmesh Region Requested!")
		EXIT
	ENDIF
	
	
	PED_INDEX piNearbyPeds[ciGANG_CHASE_NEARBY_PED_ARRAY_SIZE]
	INT iNearbyPeds = GET_PED_NEARBY_PEDS(piPed, piNearbyPeds)
	VECTOR vPedCoords = GET_ENTITY_COORDS(piPed)
	
	PRINTLN("[GANG_CHASE] LOAD_NAVMESH_ON_GANG_CHASE_PED_IF_POSSIBLE - Unit: ", iGangChaseUnit, " Checking if Navmesh request required! Nearby Peds: ", iNearbyPeds)
				
	IF iNearbyPeds <= ciGANG_CHASE_NAVMESH_REQUIRED_PEDS
		SET_BIT(iGangChasePedNavmeshCheckedBS, iGangChaseUnit)
		EXIT
	ENDIF
	
	REL_GROUP_HASH relGroup = GET_PED_RELATIONSHIP_GROUP_HASH(piPed)
	INT iValidNearbyPeds = 0
	
	INT i
	FOR i = 0 TO iNearbyPeds - 1
	
		IF NOT DOES_ENTITY_EXIST(piNearbyPeds[i])
			RELOOP
		ENDIF
		
		IF IS_ENTITY_DEAD(piNearbyPeds[i])
			RELOOP
		ENDIF
		
		IF GET_PED_RELATIONSHIP_GROUP_HASH(piNearbyPeds[i]) != relGroup
			RELOOP
		ENDIF
		
		IF VDIST2(vPedCoords, GET_ENTITY_COORDS(piNearbyPeds[i])) >= POW(cfGANG_CHASE_NAVMESH_PED_RANGE, 2)
			RELOOP
		ENDIF
		
		iValidNearbyPeds++
		
	ENDFOR
	
	PRINTLN("[GANG_CHASE] LOAD_NAVMESH_ON_GANG_CHASE_PED_IF_POSSIBLE - Unit: ", iGangChaseUnit, " Valid Nearby Peds: ", iValidNearbyPeds)
	
	IF iValidNearbyPeds <= ciGANG_CHASE_NAVMESH_REQUIRED_PEDS
		EXIT
	ENDIF
		
	ADD_NAVMESH_REQUIRED_REGION(vPedCoords.x, vPedCoords.y, cfGANG_CHASE_NAVMESH_RADIUS)
	SET_BIT(iGangChasePedNavmeshLoadedBS, iGangChaseUnit)
	SET_BIT(iLocalBoolCheck8, LBOOL8_NAVMESH_REGION_REQUESTED)
	
ENDPROC

PROC LOAD_COLLISION_ON_GANG_CHASE_PED_IF_POSSIBLE(INT iGangChaseUnit, PED_INDEX piPed)
	
	IF IS_BIT_SET(iGangChasePedCollisionBitset, iGangChaseUnit)
		LOAD_NAVMESH_ON_GANG_CHASE_PED_IF_POSSIBLE(iGangChaseUnit, piPed)
		EXIT
	ENDIF
	
	IF NOT IS_ENTITY_WAITING_FOR_WORLD_COLLISION(piPed)
	AND NOT (IS_PED_IN_ANY_VEHICLE(piPed) AND IS_ENTITY_WAITING_FOR_WORLD_COLLISION(GET_VEHICLE_PED_IS_IN(piPed)))
		EXIT
	ENDIF
	
	IF iCollisionStreamingLimit > ciCOLLISION_STREAMING_MAX
		ASSERTLN("GANG_CHASE] LOAD_COLLISION_ON_GANG_CHASE_PED_IF_POSSIBLE - Unit: ", iGangChaseUnit, " Collision Streaming Limit Met: ", iCollisionStreamingLimit)		
		EXIT
	ENDIF
	
	SET_ENTITY_LOAD_COLLISION_FLAG(piPed, TRUE)
	SET_BIT(iGangChasePedCollisionBitset, iGangChaseUnit)
	iCollisionStreamingLimit++
	
	PRINTLN("[GANG_CHASE] LOAD_COLLISION_ON_GANG_CHASE_PED_IF_POSSIBLE - Loading Collision for Unit: ", iGangChaseUnit, " Current requests: ", iCollisionStreamingLimit)
				
	LOAD_NAVMESH_ON_GANG_CHASE_PED_IF_POSSIBLE(iGangChaseUnit, piPed)
	
ENDPROC

PROC PROCESS_GANG_CHASE_PED_COLLISION(INT iGangChaseUnit, INT iPed)
	
	IF IS_NET_PED_INJURED(MC_serverBD_1.sGangChase[iGangChaseUnit].niPeds[iPed])
	
		IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sGangChase[iGangChaseUnit].niPeds[iPed])
			CLEANUP_COLLISION_ON_GANG_CHASE_PED(iGangChaseUnit, NET_TO_PED(MC_serverBD_1.sGangChase[iGangChaseUnit].niPeds[iPed]))
		ELSE
			CLEANUP_COLLISION_ON_GANG_CHASE_PED(iGangChaseUnit, NULL)
		ENDIF
			
		EXIT
		
	ENDIF
	
	PED_INDEX piPed = NET_TO_PED(MC_serverBD_1.sGangChase[iGangChaseUnit].niPeds[iPed])
			
	IF (IS_BIT_SET(MC_serverBD_1.sGangChase[iGangChaseUnit].iBitset, SBBBOOL_ON_GOTO_TASK) OR IS_BIT_SET(MC_serverBD_1.sGangChase[iGangChaseUnit].iBitset, SBBBOOL_ATTACKING_PED_TARGET))
	AND NETWORK_HAS_CONTROL_OF_ENTITY(piPed)
		LOAD_COLLISION_ON_GANG_CHASE_PED_IF_POSSIBLE(iGangChaseUnit, piPed)
	ELSE
		CLEANUP_COLLISION_ON_GANG_CHASE_PED(iGangChaseUnit, piPed)
	ENDIF
	
ENDPROC

PROC PROCESS_GANG_CHASE_PED_TAGGING(INT iGangChaseUnit, INT iPed)
	FMMC_PED_STATE sPedState
	FILL_FMMC_GANG_CHASE_PED_STATE_STRUCT(sPedState,iGangChaseUnit, iPed)
	PROCESS_PED_TAGGING_HUD(sPedState)
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Every Frame Processing
// ##### Description: Functionality that needs to be called every frame for each gang chase unit
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_GANG_CHASE_UNIT_EVERY_FRAME(INT iGangChaseUnit)
	
	IF NOT IS_BIT_SET(MC_serverBD_1.sGangChase[iGangChaseUnit].iBitset, SBBBOOL_CREATED)
		//Unit doesnt exist
		EXIT
	ENDIF
	
	INT iPed = 0
	
	//Process collision on Ped 0 only
	PROCESS_GANG_CHASE_PED_COLLISION(iGangChaseUnit, iPed)

	FOR iPed = 0 TO ciMAX_GANG_CHASE_PEDS_PER_VEHICLE - 1
		PROCESS_GANG_CHASE_PED_BLIPS(iGangChaseUnit, iPed) 
		PROCESS_GANG_CHASE_PED_TAGGING(iGangChaseUnit, iPed)
	ENDFOR
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Gang Chase Target Processing
// ##### Description: Functions dealing with the selection and processing of the Gang Chase target.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL SHOULD_GANG_CHASE_BE_ACTIVE_FOR_TEAM(INT iTeam, BOOL bCheckOnly = FALSE)
	
	IF iOldGangChaseTimerPriority[iTeam] != MC_serverBD_4.iCurrentHighestPriority[iTeam]
		IF NOT bCheckOnly
			RESET_NET_TIMER(tdGangChaseDelay[iTeam])
			iOldGangChaseTimerPriority[iTeam] = MC_serverBD_4.iCurrentHighestPriority[iTeam]
		ENDIF
	ENDIF
	
	IF NOT IS_TEAM_ACTIVE(iTeam)
		RETURN FALSE
	ENDIF
	
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	IF iRule >= FMMC_MAX_RULES
		RETURN FALSE
	ENDIF
	
	BOOL bGangChaseSingleUnitTypeSet = FALSE
	BOOL bGangChaseMultiUnitTypeSet = FALSE
	
	// has a single gang chase unit been set?
	IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iGangBackupType[iRule] != ciBACKUP_TYPE_NONE  
		bGangChaseSingleUnitTypeSet = TRUE
	ENDIF
	
	// have multiple gang chase units been set?
	INT iGangChaseUnit
	FOR iGangChaseUnit = 0 TO FMMC_MAX_GANG_CHASE_UNIT_TYPES - 1
		IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sMultipleGangChaseSettings[iRule][iGangChaseUnit].iType != ciBACKUP_TYPE_NONE
			bGangChaseMultiUnitTypeSet = TRUE
			BREAKLOOP
		ENDIF
	ENDFOR
	
	// return false if neither single nor mutiple gang chase unit type(s) have been set
	IF NOT bGangChaseSingleUnitTypeSet
	AND NOT bGangChaseMultiUnitTypeSet
		RETURN FALSE
	ENDIF
	
	IF NOT IS_BIT_SET(MC_serverBD_1.iGangChaseAreaBitset[iTeam], iRule)
		#IF IS_DEBUG_BUILD
		IF GET_FRAME_COUNT() % 10 = 0
			PRINTLN("[GANG_CHASE] SHOULD_GANG_CHASE_BE_ACTIVE_FOR_TEAM - Team: ", iTeam, " - Waiting for area - Returning FALSE!")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThirteen[iRule], ciBS_RULE13_GANGCHASE_REQUIRES_AGGRO)
	AND NOT HAS_TEAM_TRIGGERED_AGGRO(iTeam, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iAggroIndexBS_GangChase_ReqAggro[iRule])
		#IF IS_DEBUG_BUILD
		IF GET_FRAME_COUNT() % 10 = 0
			PRINTLN("[GANG_CHASE] SHOULD_GANG_CHASE_BE_ACTIVE_FOR_TEAM - Team: ", iTeam, " - Waiting for aggro - Returning FALSE!")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEleven[iRule], ciBS_RULE11_GANG_CHASE_TRIGGER_ON_SPEED_FAIL)
	AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet6, SBBOOL6_ALLOW_GANGCHASE_AFTER_SPEED_FAIL)
		#IF IS_DEBUG_BUILD
		IF GET_FRAME_COUNT() % 10 = 0
			PRINTLN("[GANG_CHASE] SHOULD_GANG_CHASE_BE_ACTIVE_FOR_TEAM - Team: ", iTeam, " - Waiting for speed fail - Returning FALSE!")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iGangBackupAtMidBitset, iRule)
	AND NOT IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iTeam], iRule)
		#IF IS_DEBUG_BUILD
		IF GET_FRAME_COUNT() % 10 = 0
			PRINTLN("[GANG_CHASE] SHOULD_GANG_CHASE_BE_ACTIVE_FOR_TEAM - Team: ", iTeam, " - Waiting for midpoint - Returning FALSE!")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	INT iIntensity	= GET_GANG_CHASE_INTENSITY(iTeam, iRule)

	IF MC_serverBD_1.iNumGangChaseUnitsChasing[iTeam] >= iIntensity 
		#IF IS_DEBUG_BUILD
		IF GET_FRAME_COUNT() % 10 = 0
			PRINTLN("[GANG_CHASE] SHOULD_GANG_CHASE_BE_ACTIVE_FOR_TEAM - Team: ", iTeam, " - Too many active - Returning FALSE!")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
				
	INT iDelay = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iGangBackupDelay[iRule]
	IF iDelay != 0
		IF NOT HAS_NET_TIMER_STARTED(tdGangChaseDelay[iTeam])
		AND NOT bCheckOnly
			PRINTLN("[GANG_CHASE] SHOULD_GANG_CHASE_BE_ACTIVE_FOR_TEAM - Team: ", iTeam, " Starting delay timer (", iDelay, " seconds).")
			REINIT_NET_TIMER(tdGangChaseDelay[iTeam])
		ENDIF
		
		IF NOT HAS_NET_TIMER_STARTED_AND_EXPIRED(tdGangChaseDelay[iTeam], iDelay * 1000)
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(tdGangChaseSpawnIntervalTimer[iTeam])
	AND NOT HAS_NET_TIMER_EXPIRED(tdGangChaseSpawnIntervalTimer[iTeam], ciGANG_CHASE_SPAWN_INTERVAL)
		RETURN FALSE
	ENDIF
		
	RETURN TRUE
	
ENDFUNC

FUNC BOOL IS_ALREADY_CHASE_TARGET(INT iParticipant)
	
	INT i
	FOR i = 0 TO ciMAX_GANG_CHASE_VEHICLES - 1
		IF MC_serverBD_1.sGangChase[i].iTargetPart = iParticipant
			RETURN TRUE
		ENDIF
	ENDFOR
	
	RETURN FALSE

ENDFUNC

FUNC INT GET_NUMBER_OF_CHASE_BACKUP_CHASING_PART(INT iParticipant)
	
	INT i = 0, iCount = 0
	FOR i = 0 TO ciMAX_GANG_CHASE_VEHICLES - 1
		IF MC_serverBD_1.sGangChase[i].iTargetPart = iParticipant
			iCount++
		ENDIF
	ENDFOR
	
	RETURN iCount
	
ENDFUNC

FUNC BOOL IS_POTENTIAL_BACKUP_TARGET_NEAR_DROP_OFF(PED_INDEX piTargetPed, INT iTeam, INT iFleeDistance = -1, INT iParticipant = ciGANG_CHASE_TARGET_NONE)
	
	IF iFleeDistance = -1
		iFleeDistance = g_FMMC_STRUCT.iGangChaseUnitFleeDistance
	ENDIF
	
	VECTOR vDropOff = GET_DROP_OFF_CENTER_FOR_TEAM(DEFAULT, iTeam, iParticipant)
	
	IF IS_VECTOR_ZERO(vDropOff)
		RETURN FALSE
	ENDIF
	
	FLOAT fMaxDist = TO_FLOAT(iFleeDistance)
	FLOAT fDist = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(piTargetPed, vDropOff)
	
	IF fDist <= fMaxDist
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC INT GET_GANG_CHASE_TARGET_ON_TEAM(INT iTeam, INT iFleeDistance)
	
	IF NOT SHOULD_GANG_CHASE_BE_ACTIVE_FOR_TEAM(iTeam)
		PRINTLN("[GANG_CHASE] GET_GANG_CHASE_TARGET_ON_TEAM - Gang Chase no longer valid for team: ", iTeam)
		RETURN -1
	ENDIF
	
	//Check a bit further than the actual flee distance
	iFleeDistance += g_FMMC_STRUCT.iGangChaseUnitExtraFleeDistance
	
	INT iParticipant = -1, iTargetPart = -1
	INT iFewestChasing = HIGHEST_INT, iNumChasing = 0
	
	DO_PARTICIPANT_LOOP_FLAGS eFlags = GET_PARTICIPANT_LOOP_FLAGS_TO_CHECK_SINGLE_TEAM(iTeam)
	WHILE DO_PARTICIPANT_LOOP(iParticipant, eFlags | DPLF_CHECK_PLAYER_OK | DPLF_FILL_PLAYER_IDS)
		
		IF NOT IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet, PBBOOL_CHASE_TARGET)
			//Player not considered a Gang Chase target
			RELOOP
		ENDIF
		
		IF NOT IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet2, PBBOOL2_LOSE_GANG_CHASE)
		AND IS_POTENTIAL_BACKUP_TARGET_NEAR_DROP_OFF(piParticipantLoop_PedIndex, iTeam, iFleeDistance, iParticipant)
			RELOOP
		ENDIF
		
		IF IS_ALREADY_CHASE_TARGET(iParticipant)
			
			iNumChasing = GET_NUMBER_OF_CHASE_BACKUP_CHASING_PART(iParticipant)
			IF iNumChasing <= iFewestChasing
				iFewestChasing = iNumChasing
				iTargetPart = iParticipant
			ENDIF
			
			RELOOP
			
		ENDIF
		
		iTargetPart = iParticipant
		BREAKLOOP
		
	ENDWHILE
	
	RETURN iTargetPart

ENDFUNC
		
FUNC BOOL IS_TEAM_A_BETTER_GANG_CHASE_TARGET_THAN_TEAM(INT iTeam, INT iTeamToCompare)

	IF HAS_TEAM_FAILED(iTeam) 
	AND NOT HAS_TEAM_FAILED(iTeamToCompare)
		RETURN FALSE
	ENDIF
	
	IF MC_serverBD_1.iNumGangChaseCandidates[iTeam] = 0 
	AND MC_serverBD_1.iNumGangChaseCandidates[iTeamToCompare] != 0
		RETURN FALSE
	ENDIF
	
	IF MC_serverBD_1.iNumGangChaseUnitsChasing[iTeam] > MC_serverBD_1.iNumGangChaseUnitsChasing[iTeamToCompare]
		RETURN FALSE
	ENDIF
		
	IF MC_serverBD_1.iNumGangChaseUnitsChasing[iTeam] = MC_serverBD_1.iNumGangChaseUnitsChasing[iTeamToCompare]	
	AND MC_serverBD_1.iNumGangChaseCandidates[iTeam] < MC_serverBD_1.iNumGangChaseCandidates[iTeamToCompare]
		RETURN FALSE
	ENDIF
	
	RETURN TRUE

ENDFUNC

FUNC INT GET_BEST_GANG_CHASE_TARGET_TEAM(INT iTeamsToConsiderBS)

	IF iTeamsToConsiderBS = 0
		RETURN -1
	ENDIF
	
	IF iTeamsToConsiderBS = 1 // Team 0 only
		RETURN 0
	ENDIF
	
	IF iTeamsToConsiderBS = 2 // Team 1 only
		RETURN 1
	ENDIF
	
	IF iTeamsToConsiderBS = 4 // Team 2 only
		RETURN 2
	ENDIF
	
	IF iTeamsToConsiderBS = 8 // Team 3 only
		RETURN 3
	ENDIF
	
	INT iTeam, iTeamToCompare, iBestTargetTeam = -1
	FOR iTeam = 0 TO MC_serverBD.iNumberOfTeams - 1
		
		IF NOT IS_BIT_SET(iTeamsToConsiderBS, iTeam)
			RELOOP
		ENDIF
		
		FOR iTeamToCompare = 0 TO MC_serverBD.iNumberOfTeams - 1
			
			IF iTeamToCompare = iTeam
				RELOOP
			ENDIF
			
			IF NOT IS_BIT_SET(iTeamsToConsiderBS, iTeam)
				RELOOP
			ENDIF
			
			IF NOT IS_TEAM_A_BETTER_GANG_CHASE_TARGET_THAN_TEAM(iTeam, iTeamToCompare)
				BREAKLOOP
			ENDIF
			
			iBestTargetTeam = iTeam
			
		ENDFOR
		
	ENDFOR
	
	RETURN iBestTargetTeam
	
ENDFUNC		

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Staggered Server Gang Chase Unit Processing
// ##### Description: Server Gang Chase Unit processing done for one unit per frame
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		
PROC PROCESS_SERVER_GANG_CHASE_UNIT_STATE_INIT(INT iGangChaseUnit)
	
	IF MC_serverBD_1.iNextGangChaseRespawnTime > 0
		IF NATIVE_TO_INT(GET_NETWORK_TIME()) < MC_serverBD_1.iNextGangChaseRespawnTime
			PRINTLN("[GANG_CHASE] PROCESS_SERVER_GANG_CHASE_UNIT_STATE_INIT - Unit: ", iGangChaseUnit, " Respawn Delay Active")
			EXIT
		ENDIF
	ENDIF
	
	IF NOT HAS_NET_TIMER_STARTED(tdGangChaseTargetTimer[iGangChaseUnit])
		REINIT_NET_TIMER(tdGangChaseTargetTimer[iGangChaseUnit])
	ENDIF
	
	IF NOT HAS_NET_TIMER_EXPIRED(tdGangChaseTargetTimer[iGangChaseUnit], ciGANG_CHASE_TARGET_TIMER)
		EXIT
	ENDIF
	
	INT iTeamsToConsiderBS = 0
	
	INT iTeam
	FOR iTeam = 0 TO MC_serverBD.iNumberOfTeams - 1
		IF SHOULD_GANG_CHASE_BE_ACTIVE_FOR_TEAM(iTeam)
			SET_BIT(iTeamsToConsiderBS, iTeam)
			PRINTLN("[GANG_CHASE] PROCESS_SERVER_GANG_CHASE_UNIT_STATE_INIT - Unit: ", iGangChaseUnit, " should spawn for team: ", iTeam)
		ENDIF
	ENDFOR
			
	MC_serverBD_1.sGangChase[iGangChaseUnit].iSpawnTeam = GET_BEST_GANG_CHASE_TARGET_TEAM(iTeamsToConsiderBS)
			
	IF MC_serverBD_1.sGangChase[iGangChaseUnit].iSpawnTeam = -1
		EXIT
	ENDIF
	
	PRINTLN("[GANG_CHASE] PROCESS_SERVER_GANG_CHASE_UNIT_STATE_INIT - Unit: ", iGangChaseUnit, " Target Team: ", MC_serverBD_1.sGangChase[iGangChaseUnit].iSpawnTeam)
	
	INT iTargetTeam = MC_serverBD_1.sGangChase[iGangChaseUnit].iSpawnTeam
	
	INT iFleeDistance = g_FMMC_STRUCT.sFMMCEndConditions[iTargetTeam].iGangBackupFleeDist[MC_serverBD_4.iCurrentHighestPriority[iTargetTeam]]
	IF iFleeDistance = 0
		iFleeDistance = g_FMMC_STRUCT.iGangChaseUnitFleeDistance
	ENDIF
	
	MC_serverBD_1.sGangChase[iGangChaseUnit].iFleeDist = iFleeDistance
	PRINTLN("[GANG_CHASE] PROCESS_SERVER_GANG_CHASE_UNIT_STATE_INIT - Unit: ", iGangChaseUnit, " Flee Distance: ", MC_serverBD_1.sGangChase[iGangChaseUnit].iFleeDist)
	
	SET_GANG_CHASE_STATE_FOR_UNIT(iGangChaseUnit, ciGANG_CHASE_UNIT_STATE_FIND_TARGET)
	
ENDPROC

PROC PROCESS_SERVER_GANG_CHASE_UNIT_STATE_FIND_TARGET(INT iGangChaseUnit)
	
	MC_serverBD_1.sGangChase[iGangChaseUnit].iTargetPart = GET_GANG_CHASE_TARGET_ON_TEAM(MC_serverBD_1.sGangChase[iGangChaseUnit].iSpawnTeam, MC_serverBD_1.sGangChase[iGangChaseUnit].iFleeDist)
	
	PRINTLN("[GANG_CHASE] PROCESS_SERVER_GANG_CHASE_UNIT_STATE_FIND_TARGET - Unit: ", iGangChaseUnit, " Target Part: ", MC_serverBD_1.sGangChase[iGangChaseUnit].iTargetPart)
	
	IF MC_serverBD_1.sGangChase[iGangChaseUnit].iTargetPart = ciGANG_CHASE_TARGET_NONE
	
		REINIT_NET_TIMER(tdGangChaseTargetTimer[iGangChaseUnit])
		MC_serverBD_1.sGangChase[iGangChaseUnit].iSpawnTeam = -1
		SET_GANG_CHASE_STATE_FOR_UNIT(iGangChaseUnit, ciGANG_CHASE_UNIT_STATE_INIT)
		
		EXIT
		
	ENDIF
	
	SET_GANG_CHASE_STATE_FOR_UNIT(iGangChaseUnit, ciGANG_CHASE_UNIT_STATE_WAIT_FOR_CREATION)
	
ENDPROC

PROC PROCESS_SERVER_GANG_CHASE_UNIT_STATE_WAIT_FOR_CREATION(INT iGangChaseUnit)
	
	IF NOT IS_BIT_SET(MC_serverBD_1.sGangChase[iGangChaseUnit].iBitset, SBBBOOL_CREATED)
		
		IF NOT SHOULD_GANG_CHASE_BE_ACTIVE_FOR_TEAM(MC_serverBD_1.sGangChase[iGangChaseUnit].iSpawnTeam)
			PRINTLN("[GANG_CHASE] PROCESS_SERVER_GANG_CHASE_UNIT_STATE_WAIT_FOR_CREATION - Unit: ", iGangChaseUnit, " Gang Chase no longer active for team: ", MC_serverBD_1.sGangChase[iGangChaseUnit].iSpawnTeam)
			SET_GANG_CHASE_STATE_FOR_UNIT(iGangChaseUnit, ciGANG_CHASE_UNIT_STATE_INIT)
			RESET_GANG_CHASE_DATA(iGangChaseUnit)
		ENDIF
		
		EXIT
		
	ENDIF
	
	SET_GANG_CHASE_STATE_FOR_UNIT(iGangChaseUnit, ciGANG_CHASE_UNIT_STATE_ACTIVE)
	
ENDPROC

PROC PROCESS_GANG_CHASE_GOTO_TASK(INT iGangChaseUnit)

	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sGangChase[iGangChaseUnit].niPeds[0])
		CLEANUP_COLLISION_ON_GANG_CHASE_PED(iGangChaseUnit, NULL)
		CLEAR_BIT(MC_serverBD_1.sGangChase[iGangChaseUnit].iBitset, SBBBOOL_ON_GOTO_TASK)
		EXIT
	ENDIF
	
	PED_INDEX pedDriver = NET_TO_PED(MC_serverBD_1.sGangChase[iGangChaseUnit].niPeds[0])
	IF IS_PED_INJURED(pedDriver)
		CLEANUP_COLLISION_ON_GANG_CHASE_PED(iGangChaseUnit, pedDriver)
		CLEAR_BIT(MC_serverBD_1.sGangChase[iGangChaseUnit].iBitset, SBBBOOL_ON_GOTO_TASK)
		EXIT
	ENDIF
	
	PLAYER_INDEX piTarget = GET_GANG_CHASE_TARGET_PLAYER_INDEX(iGangChaseUnit)
	FLOAT fDist2 = VDIST2(GET_PLAYER_COORDS(piTarget), GET_ENTITY_COORDS(pedDriver))
	
	IF fDist2 >= POW(cfGANG_CHASE_COMBAT_TASK_DISTANCE, 2)
		EXIT
	ENDIF
				
	IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(pedDriver)
		PRINTLN("[GANG_CHASE] PROCESS_GANG_CHASE_GOTO_TASK - Unit: ", iGangChaseUnit, " Requesting control of driver for tasking.")
		NETWORK_REQUEST_CONTROL_OF_ENTITY(pedDriver)
		EXIT
	ENDIF
	
	PRINTLN("[GANG_CHASE] PROCESS_GANG_CHASE_GOTO_TASK - Unit: ", iGangChaseUnit, " Tasking driver with combat!")
	
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedDriver, FALSE)
	
	CLEAR_BIT(MC_serverBD_1.sGangChase[iGangChaseUnit].iBitset, SBBBOOL_ON_GOTO_TASK)
	
	IF MC_serverBD_1.iGangChasePedTargetOverride != -1
		PED_INDEX pedTarget = GET_GANG_CHASE_TARGET_PED_INDEX(iGangChaseUnit)
		IF NOT IS_PED_INJURED(pedTarget)
			PRINTLN("[GANG_CHASE] PROCESS_GANG_CHASE_GOTO_TASK - Unit: ", iGangChaseUnit, " Tasking ped to attack target override: ", MC_serverBD_1.iGangChasePedTargetOverride)
			TASK_COMBAT_PED(pedDriver, pedTarget, COMBAT_PED_PREVENT_CHANGING_TARGET | MC_GET_TASK_COMBAT_PED_FLAGS(pedDriver))
			SET_BIT(MC_serverBD_1.sGangChase[iGangChaseUnit].iBitset, SBBBOOL_ATTACKING_PED_TARGET)
			EXIT
		ENDIF
	ENDIF
		
	TASK_COMBAT_HATED_TARGETS_AROUND_PED(pedDriver, cfGANG_CHASE_PED_RANGE, MC_GET_TASK_COMBAT_PED_FLAGS(pedDriver))
	CLEANUP_COLLISION_ON_GANG_CHASE_PED(iGangChaseUnit, pedDriver)
	
ENDPROC

FUNC BOOL SHOULD_CLEAN_UP_GANG_CHASE_COMBAT_TARGET_TASK(INT iGangChaseUnit)
	
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sGangChase[iGangChaseUnit].niPeds[0])
		PRINTLN("[GANG_CHASE] PROCESS_GANG_CHASE_COMBAT_TARGET_TASK - Unit: ", iGangChaseUnit, " Driver doesn't exist.")
		CLEANUP_COLLISION_ON_GANG_CHASE_PED(iGangChaseUnit, NULL)
		RETURN TRUE
	ENDIF
	
	PED_INDEX pedDriver = NET_TO_PED(MC_serverBD_1.sGangChase[iGangChaseUnit].niPeds[0])
	IF IS_PED_INJURED(pedDriver)
		PRINTLN("[GANG_CHASE] PROCESS_GANG_CHASE_COMBAT_TARGET_TASK - Unit: ", iGangChaseUnit, " Driver is dead.")
		CLEANUP_COLLISION_ON_GANG_CHASE_PED(iGangChaseUnit, pedDriver)
		RETURN TRUE
	ENDIF

	RETURN FALSE
	
ENDFUNC

FUNC BOOL SHOULD_FLEE_GANG_CHASE_COMBAT_TARGET_TASK()
	
	IF MC_serverBD_1.iGangChasePedTargetOverride = -1
		PRINTLN("[GANG_CHASE] SHOULD_FLEE_GANG_CHASE_COMBAT_TARGET_TASK - No target.")
		RETURN TRUE
	ENDIF
	
	PED_INDEX pedTarget = GET_GANG_CHASE_TARGET_OVERRIDE_PED_INDEX()
	IF pedTarget = NULL
		PRINTLN("[GANG_CHASE] SHOULD_FLEE_GANG_CHASE_COMBAT_TARGET_TASK - Target ped null.")
		RETURN TRUE
	ENDIF
	
	IF IS_PED_INJURED(pedTarget)
		PRINTLN("[GANG_CHASE] SHOULD_FLEE_GANG_CHASE_COMBAT_TARGET_TASK - Target is dead.")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC PROCESS_GANG_CHASE_COMBAT_TARGET_TASK(INT iGangChaseUnit)
	
	IF SHOULD_CLEAN_UP_GANG_CHASE_COMBAT_TARGET_TASK(iGangChaseUnit)
		CLEAR_BIT(MC_serverBD_1.sGangChase[iGangChaseUnit].iBitset, SBBBOOL_ATTACKING_PED_TARGET)
		EXIT
	ENDIF
	
	PED_INDEX pedDriver = NET_TO_PED(MC_serverBD_1.sGangChase[iGangChaseUnit].niPeds[0])

	IF SHOULD_FLEE_GANG_CHASE_COMBAT_TARGET_TASK()
		IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(pedDriver)
			PRINTLN("[GANG_CHASE] PROCESS_GANG_CHASE_COMBAT_TARGET_TASK - Unit: ", iGangChaseUnit, " Requesting control of driver for flee tasking.")
			NETWORK_REQUEST_CONTROL_OF_ENTITY(pedDriver)
			EXIT
		ENDIF
	
		CLEANUP_COLLISION_ON_GANG_CHASE_PED(iGangChaseUnit, pedDriver)
		CLEAR_BIT(MC_serverBD_1.sGangChase[iGangChaseUnit].iBitset, SBBBOOL_ATTACKING_PED_TARGET)
		PLAYER_INDEX piTarget = GET_GANG_CHASE_TARGET_PLAYER_INDEX(iGangChaseUnit)
		VECTOR vFleeCoord = GET_PLAYER_COORDS(piTarget)
		PRINTLN("[GANG_CHASE] PROCESS_GANG_CHASE_COMBAT_TARGET_TASK - Unit: ", iGangChaseUnit, " Tasking to flee coord: ", vFleeCoord)
		TASK_GANG_CHASE_UNIT_FLEE_COORDS(iGangChaseUnit, vFleeCoord, TRUE)
		EXIT
	ENDIF
	
	PED_INDEX pedTarget = GET_GANG_CHASE_TARGET_OVERRIDE_PED_INDEX()
	IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(pedDriver)
		PRINTLN("[GANG_CHASE] PROCESS_GANG_CHASE_COMBAT_TARGET_TASK - Unit: ", iGangChaseUnit, " Requesting control of driver for combat tasking.")
		NETWORK_REQUEST_CONTROL_OF_ENTITY(pedDriver)
		EXIT
	ENDIF
	
	PED_INDEX pedPrevTarget = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_PED_TARGET_FROM_COMBAT_PED(pedDriver))
	IF pedPrevTarget != pedTarget
		PRINTLN("[GANG_CHASE] PROCESS_GANG_CHASE_COMBAT_TARGET_TASK - Unit: ", iGangChaseUnit, " Tasking ped to attack target override: ", MC_serverBD_1.iGangChasePedTargetOverride)
		CLEAR_PED_TASKS(pedDriver)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedDriver, FALSE)
		TASK_COMBAT_PED(pedDriver, pedTarget, COMBAT_PED_PREVENT_CHANGING_TARGET |  MC_GET_TASK_COMBAT_PED_FLAGS(pedDriver))
	ENDIF

ENDPROC

PROC PROCESS_GANG_CHASE_UNIT_TASKING(INT iGangChaseUnit)
	
	IF IS_BIT_SET(MC_serverBD_1.sGangChase[iGangChaseUnit].iBitset, SBBBOOL_ATTACKING_PED_TARGET)
		PROCESS_GANG_CHASE_COMBAT_TARGET_TASK(iGangChaseUnit)
	ELIF IS_BIT_SET(MC_serverBD_1.sGangChase[iGangChaseUnit].iBitset, SBBBOOL_ON_GOTO_TASK)
		PROCESS_GANG_CHASE_GOTO_TASK(iGangChaseUnit)
	ELSE
		IF MC_serverBD_1.iGangChasePedTargetOverride != -1
			IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sGangChase[iGangChaseUnit].niPeds[0])
				PED_INDEX pedDriver = NET_TO_PED(MC_serverBD_1.sGangChase[iGangChaseUnit].niPeds[0])
				IF NOT IS_PED_INJURED(pedDriver)
					PED_INDEX pedTarget = GET_GANG_CHASE_TARGET_PED_INDEX(iGangChaseUnit)
					IF NOT IS_PED_INJURED(pedTarget)
						IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(pedDriver)
							PRINTLN("[GANG_CHASE] PROCESS_GANG_CHASE_UNIT_TASKING - Unit: ", iGangChaseUnit, " Requesting control of driver for combat tasking.")
							NETWORK_REQUEST_CONTROL_OF_ENTITY(pedDriver)
							EXIT
						ENDIF
						PRINTLN("[GANG_CHASE] PROCESS_GANG_CHASE_UNIT_TASKING - Unit: ", iGangChaseUnit, " Tasking ped to attack target override: ", MC_serverBD_1.iGangChasePedTargetOverride)
						CLEAR_PED_TASKS(pedDriver)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedDriver, FALSE)
						TASK_COMBAT_PED(pedDriver, pedTarget, COMBAT_PED_PREVENT_CHANGING_TARGET | MC_GET_TASK_COMBAT_PED_FLAGS(pedDriver))
						SET_BIT(MC_serverBD_1.sGangChase[iGangChaseUnit].iBitset, SBBBOOL_ATTACKING_PED_TARGET)
						EXIT
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
				
ENDPROC

FUNC BOOL SHOULD_GANG_CHASE_UNIT_FLEE_DUE_TO_OBJECTIVE(INT iGangChaseUnit, INT iTeam, VECTOR &vDropoff)

	IF IS_CURRENT_OBJECTIVE_LOSE_GANG_CHASE()
		RETURN FALSE
	ENDIF
	
	PLAYER_INDEX piTarget = GET_GANG_CHASE_TARGET_PLAYER_INDEX(iGangChaseUnit)
	
	IF MC_serverBD_4.iObjMissionLogic[iTeam] = FMMC_OBJECTIVE_LOGIC_LEAVE_LOCATION
		IF MC_playerBD[MC_serverBD_1.sGangChase[iGangChaseUnit].iTargetPart].iLeaveLoc != -1
			RETURN FALSE
		ELSE
			vDropoff = GET_PLAYER_COORDS(piTarget)
			RETURN TRUE
		ENDIF
	ENDIF
	
	vDropOff = GET_DROP_OFF_CENTER_FOR_TEAM(DEFAULT, iTeam, MC_serverBD_1.sGangChase[iGangChaseUnit].iTargetPart)
	
	IF IS_VECTOR_ZERO(vDropOff)
		RETURN FALSE
	ENDIF
	
	FLOAT fDist2 = VDIST2(GET_PLAYER_COORDS(piTarget), vDropOff)
	FLOAT fMaxDist = TO_FLOAT(PICK_INT(MC_serverBD_1.sGangChase[iGangChaseUnit].iFleeDist > 0, MC_serverBD_1.sGangChase[iGangChaseUnit].iFleeDist, g_FMMC_STRUCT.iGangChaseUnitFleeDistance))
				
	IF fDist2 >= POW(fMaxDist, 2)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE

ENDFUNC

FUNC BOOL SHOULD_GANG_CHASE_UNIT_FLEE(INT iGangChaseUnit, INT iTeam, VECTOR &vFleeCoords)
	
	IF SHOULD_GANG_CHASE_UNIT_FLEE_DUE_TO_OBJECTIVE(iGangChaseUnit, iTeam, vFleeCoords)
		PRINTLN("[GANG_CHASE] SHOULD_GANG_CHASE_UNIT_FLEE - Unit: ", iGangChaseUnit, " flee due to objective.")
		RETURN TRUE
	ENDIF
	
	IF IS_ZONE_TYPE_TRIGGERING(ciFMMC_ZONE_TYPE__GANG_CHASE_FLEE)
		PRINTLN("[GANG_CHASE] SHOULD_GANG_CHASE_UNIT_FLEE - Unit: ", iGangChaseUnit, " flee due to zone.")
		vFleeCoords = GET_ZONE_RUNTIME_CENTRE(GET_TRIGGERING_ZONE_INDEX(ciFMMC_ZONE_TYPE__GANG_CHASE_FLEE))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC PROCESS_GANG_CHASE_UNIT_FLEEING(INT iGangChaseUnit)

	INT iTargetTeam = MC_serverBD_1.sGangChase[iGangChaseUnit].iSpawnTeam
	IF iTargetTeam = -1
		EXIT
	ENDIF
	
	INT iTargetRule = MC_serverBD_4.iCurrentHighestPriority[iTargetTeam]
	IF iTargetRule >= FMMC_MAX_RULES
		EXIT
	ENDIF
	
	VECTOR vFleeCoords
	IF NOT SHOULD_GANG_CHASE_UNIT_FLEE(iGangChaseUnit, iTargetTeam, vFleeCoords)
		EXIT
	ENDIF
	
	PRINTLN("[GANG_CHASE] PROCESS_GANG_CHASE_UNIT_TASKING - Unit: ", iGangChaseUnit, " Tasking to flee coord: ", vFleeCoords)
	TASK_GANG_CHASE_UNIT_FLEE_COORDS(iGangChaseUnit, vFleeCoords, TRUE)
	
ENDPROC

PROC PROCESS_SERVER_GANG_CHASE_UNIT_STATE_ACTIVE(INT iGangChaseUnit)

	BOOL bFlee, bTimedCleanup
	IF SHOULD_GANG_CHASE_UNIT_CLEANUP(iGangChaseUnit, bFlee, bTimedCleanup)
	
		CLEANUP_BACKUP_UNIT(iGangChaseUnit, bFlee)
		EXIT
		
	ENDIF
	
	IF !bTimedCleanup
	AND HAS_NET_TIMER_STARTED(MC_serverBD_1.sGangChase[iGangChaseUnit].tdCleanupTimer)
		PRINTLN("[GANG_CHASE] PROCESS_SERVER_GANG_CHASE_UNIT_STATE_ACTIVE - Unit: ", iGangChaseUnit, " Reseting cleanup timer.")
		RESET_NET_TIMER(MC_serverBD_1.sGangChase[iGangChaseUnit].tdCleanupTimer)									
	ENDIF
		
	PROCESS_GANG_CHASE_UNIT_TASKING(iGangChaseUnit)
		
	PROCESS_GANG_CHASE_UNIT_FLEEING(iGangChaseUnit)
	
ENDPROC
				
PROC PROCESS_SERVER_GANG_CHASE_UNIT(INT iGangChaseUnit)

	IF !bIsLocalPlayerHost
		EXIT
	ENDIF
	
	IF MC_serverBD_1.iNextGangChaseRespawnTime > 0
		IF MC_serverBD_1.sGangChase[iGangChaseUnit].iGangChaseState > ciGANG_CHASE_UNIT_STATE_INIT
		AND MC_serverBD_1.sGangChase[iGangChaseUnit].iGangChaseState < ciGANG_CHASE_UNIT_STATE_WAIT_FOR_CREATION
			IF NATIVE_TO_INT(GET_NETWORK_TIME()) < MC_serverBD_1.iNextGangChaseRespawnTime
				PRINTLN("[GANG_CHASE] PROCESS_SERVER_GANG_CHASE_UNIT - Unit: ", iGangChaseUnit, " Respawn Delay Active")
				SET_GANG_CHASE_STATE_FOR_UNIT(iGangChaseUnit, ciGANG_CHASE_UNIT_STATE_INIT)
				RESET_GANG_CHASE_DATA(iGangChaseUnit)
			ENDIF
		ENDIF
	ENDIF

	SWITCH MC_serverBD_1.sGangChase[iGangChaseUnit].iGangChaseState
		
		CASE ciGANG_CHASE_UNIT_STATE_INIT
			PROCESS_SERVER_GANG_CHASE_UNIT_STATE_INIT(iGangChaseUnit)
		BREAK
		
		CASE ciGANG_CHASE_UNIT_STATE_FIND_TARGET
			PROCESS_SERVER_GANG_CHASE_UNIT_STATE_FIND_TARGET(iGangChaseUnit)
		BREAK
							
		CASE ciGANG_CHASE_UNIT_STATE_WAIT_FOR_CREATION
			PROCESS_SERVER_GANG_CHASE_UNIT_STATE_WAIT_FOR_CREATION(iGangChaseUnit)
		BREAK
		
		CASE ciGANG_CHASE_UNIT_STATE_ACTIVE
			PROCESS_SERVER_GANG_CHASE_UNIT_STATE_ACTIVE(iGangChaseUnit)
		BREAK
		
	ENDSWITCH

ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Gang Chase Unit Config
// ##### Description: Functions used to generate the configuration of a gang chase unit
// ##### Weapon types, ped and vehicle models, number of peds etc.
// ##### Populates a struct of type GANG_CHASE_UNIT_CONFIG_DATA
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
FUNC MODEL_NAMES GET_GANG_CHASE_UNIT_VEHICLE_MODEL(INT iGangType)

	SWITCH iGangType
		CASE ciBACKUP_TYPE_MERRYWEATHER					RETURN PATRIOT
		CASE ciBACKUP_TYPE_LOST							RETURN DAEMON
		CASE ciBACKUP_TYPE_VAGOS						RETURN CAVALCADE2
		CASE ciBACKUP_TYPE_FAMILIES						RETURN BALLER
		CASE ciBACKUP_TYPE_PROFESSIONALS				RETURN GRANGER
		CASE ciBACKUP_TYPE_BALLAS						RETURN BALLER
		CASE ciBACKUP_TYPE_SALVA						RETURN EMPEROR
		CASE ciBACKUP_TYPE_ALTRUISTS					RETURN BODHI2
		CASE ciBACKUP_TYPE_FIB							RETURN FBI
		CASE ciBACKUP_TYPE_ONEIL_BROS					RETURN SANDKING
		CASE ciBACKUP_TYPE_KOREANS						RETURN FUGITIVE
		CASE ciBACKUP_TYPE_MERRYWEATHER_MESAS			RETURN MESA3
		CASE ciBACKUP_TYPE_KOREANS2						RETURN FELTZER2
		CASE ciBACKUP_TYPE_ARMY							RETURN MESA3
		CASE ciBACKUP_TYPE_LOST_SLAMVAN2				RETURN SLAMVAN2
		CASE ciBACKUP_TYPE_ARMY2						RETURN CRUSADER
		CASE ciBACKUP_TYPE_LOWRIDER_VAGOS_BLA			RETURN BLADE
		CASE ciBACKUP_TYPE_LOWRIDER_VAGOS_EMP			RETURN EMPEROR
		CASE ciBACKUP_TYPE_LOWRIDER_BALLAS				RETURN BUCCANEER
		CASE ciBACKUP_TYPE_AIR_BUZZ_MERRYWEATHER		RETURN BUZZARD
		CASE ciBACKUP_TYPE_AIR_BUZZ_NOOSE				RETURN BUZZARD2
		CASE ciBACKUP_TYPE_AIR_BUZZ_PROFESSIONALS		RETURN BUZZARD
		CASE ciBACKUP_TYPE_AIR_MAVERICK_NOOSE			RETURN POLMAV
		CASE ciBACKUP_TYPE_AIR_MAVERICK_COP				RETURN POLMAV
		CASE ciBACKUP_TYPE_AIR_FROGGER_MERRYWEATHER		RETURN FROGGER
		CASE ciBACKUP_TYPE_AIR_FROGGER_PROFESSIONALS	RETURN FROGGER
		CASE ciBACKUP_TYPE_AIR_FROGGER_LOST				RETURN FROGGER
		CASE ciBACKUP_TYPE_AIR_FROGGER_VAGOS			RETURN FROGGER
		CASE ciBACKUP_TYPE_AIR_FROGGER_KOREAN			RETURN FROGGER
		CASE ciBACKUP_TYPE_AIR_VALKYRIE_MERRYWEATHER	RETURN VALKYRIE2
		CASE ciBACKUP_TYPE_AIR_VALKYRIE_PROFESSIONALS	RETURN VALKYRIE2
		CASE ciBACKUP_TYPE_AIR_HUNTER_MERRYWEATHER		RETURN HUNTER
		CASE ciBACKUP_TYPE_AIR_HUNTER_PROFESSIONALS		RETURN HUNTER
		CASE ciBACKUP_TYPE_AIR_SAVAGE_MERRYWEATHER		RETURN SAVAGE
		CASE ciBACKUP_TYPE_AIR_SAVAGE_PROFESSIONALS		RETURN SAVAGE
		CASE ciBACKUP_TYPE_AIR_LAZER_MERRYWEATHER		RETURN LAZER
		CASE ciBACKUP_TYPE_AIR_MOLOTOK_MERRYWEATHER		RETURN MOLOTOK
		CASE ciBACKUP_TYPE_NIGHTSHARK_BOGDAN_GOON		RETURN NIGHTSHARK
		CASE ciBACKUP_TYPE_INSURGENT_BOGDAN_GOON		RETURN INSURGENT
		CASE ciBACKUP_TYPE_INSURGENT2_BOGDAN_GOON		RETURN INSURGENT2
		CASE ciBACKUP_TYPE_AIR_BUZZ_AVON_GOON			RETURN BUZZARD
		CASE ciBACKUP_TYPE_NIGHTSHARK_AVON_GOON			RETURN NIGHTSHARK
		CASE ciBACKUP_TYPE_DUBSTA3_AVON_GOON			RETURN DUBSTA3
		CASE ciBACKUP_TYPE_KAMACHO_PROFESSIONALS		RETURN KAMACHO
		CASE ciBACKUP_TYPE_CONTENDER_PROFESSIONALS		RETURN CONTENDER
		CASE ciBACKUP_TYPE_CARACARA_HILLBILLY			RETURN CARACARA2
		CASE ciBACKUP_TYPE_BOAT_KOREANS_DINGHY			RETURN DINGHY
		CASE ciBACKUP_TYPE_BOAT_PROFESSIONALS_DINGHY	RETURN DINGHY
		
		CASE ciBACKUP_TYPE_CUSTOM_1						
		CASE ciBACKUP_TYPE_CUSTOM_2						
		CASE ciBACKUP_TYPE_CUSTOM_3						
		CASE ciBACKUP_TYPE_CUSTOM_4					
			INT i
			IF GET_GANG_CHASE_UNIT_CREATOR_CONFIG_INDEX_FROM_GANG_TYPE(iGangType, i)
				IF g_FMMC_STRUCT.sGangChaseUnitConfigs[i].eType = FMMC_GANG_CHASE_TYPE_ON_FOOT
					RETURN DUMMY_MODEL_FOR_SCRIPT
				ENDIF
				RETURN g_FMMC_STRUCT.sGangChaseUnitConfigs[i].mnVehicleModel
			ENDIF
		BREAK
	ENDSWITCH

	ASSERTLN("[GANG_CHASE] GET_GANG_CHASE_UNIT_VEHICLE_MODEL - INVALID GANG TYPE: ", iGangType)
	
	#IF IS_DEBUG_BUILD
	ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_INVALID_GANG_TYPE, ENTITY_RUNTIME_ERROR_TYPE_WARNING, "Invalid Gang Type set up.")
	#ENDIF
	
	RETURN DUMMY_MODEL_FOR_SCRIPT

ENDFUNC

FUNC MODEL_NAMES GET_GANG_CHASE_UNIT_PED_MODEL(INT iGangType)

	SWITCH iGangType
		CASE ciBACKUP_TYPE_MERRYWEATHER					RETURN S_M_Y_BLACKOPS_01
		CASE ciBACKUP_TYPE_LOST							RETURN G_M_Y_LOST_01
		CASE ciBACKUP_TYPE_VAGOS						RETURN G_M_Y_MEXGOON_02
		CASE ciBACKUP_TYPE_FAMILIES						RETURN G_M_Y_FAMCA_01
		CASE ciBACKUP_TYPE_PROFESSIONALS				RETURN MP_G_M_PROS_01
		CASE ciBACKUP_TYPE_BALLAS						RETURN G_M_Y_BALLAORIG_01
		CASE ciBACKUP_TYPE_SALVA						RETURN G_M_Y_SALVAGOON_01
		CASE ciBACKUP_TYPE_ALTRUISTS					RETURN A_M_O_ACULT_02
		CASE ciBACKUP_TYPE_FIB							RETURN S_M_M_FIBSEC_01
		CASE ciBACKUP_TYPE_ONEIL_BROS					RETURN A_M_M_HILLBILLY_01
		CASE ciBACKUP_TYPE_KOREANS						RETURN G_M_Y_KOREAN_01
		CASE ciBACKUP_TYPE_MERRYWEATHER_MESAS			RETURN S_M_Y_BLACKOPS_01
		CASE ciBACKUP_TYPE_KOREANS2						RETURN G_M_Y_KOREAN_01
		CASE ciBACKUP_TYPE_ARMY							RETURN S_M_Y_MARINE_03
		CASE ciBACKUP_TYPE_LOST_SLAMVAN2				RETURN G_M_Y_LOST_01
		CASE ciBACKUP_TYPE_ARMY2						RETURN S_M_Y_MARINE_03
		CASE ciBACKUP_TYPE_LOWRIDER_VAGOS_BLA			RETURN G_M_Y_MEXGOON_02
		CASE ciBACKUP_TYPE_LOWRIDER_VAGOS_EMP			RETURN G_M_Y_MEXGOON_02
		CASE ciBACKUP_TYPE_LOWRIDER_BALLAS				RETURN G_M_Y_BALLAORIG_01
		CASE ciBACKUP_TYPE_AIR_BUZZ_MERRYWEATHER		RETURN S_M_Y_BLACKOPS_01
		CASE ciBACKUP_TYPE_AIR_BUZZ_NOOSE				RETURN S_M_Y_SWAT_01
		CASE ciBACKUP_TYPE_AIR_BUZZ_PROFESSIONALS		RETURN MP_G_M_PROS_01
		CASE ciBACKUP_TYPE_AIR_MAVERICK_NOOSE			RETURN S_M_Y_SWAT_01
		CASE ciBACKUP_TYPE_AIR_MAVERICK_COP				RETURN S_M_Y_COP_01
		CASE ciBACKUP_TYPE_AIR_FROGGER_MERRYWEATHER		RETURN S_M_Y_BLACKOPS_01
		CASE ciBACKUP_TYPE_AIR_FROGGER_PROFESSIONALS	RETURN MP_G_M_PROS_01
		CASE ciBACKUP_TYPE_AIR_FROGGER_LOST				RETURN G_M_Y_LOST_01
		CASE ciBACKUP_TYPE_AIR_FROGGER_VAGOS			RETURN G_M_Y_MEXGOON_02
		CASE ciBACKUP_TYPE_AIR_FROGGER_KOREAN			RETURN G_M_Y_KOREAN_01
		CASE ciBACKUP_TYPE_AIR_VALKYRIE_MERRYWEATHER	RETURN S_M_Y_BLACKOPS_01
		CASE ciBACKUP_TYPE_AIR_VALKYRIE_PROFESSIONALS	RETURN MP_G_M_PROS_01
		CASE ciBACKUP_TYPE_AIR_HUNTER_MERRYWEATHER		RETURN S_M_Y_BLACKOPS_01
		CASE ciBACKUP_TYPE_AIR_HUNTER_PROFESSIONALS		RETURN MP_G_M_PROS_01
		CASE ciBACKUP_TYPE_AIR_SAVAGE_MERRYWEATHER		RETURN S_M_Y_BLACKOPS_01
		CASE ciBACKUP_TYPE_AIR_SAVAGE_PROFESSIONALS		RETURN MP_G_M_PROS_01
		CASE ciBACKUP_TYPE_AIR_LAZER_MERRYWEATHER		RETURN S_M_Y_BLACKOPS_01
		CASE ciBACKUP_TYPE_AIR_MOLOTOK_MERRYWEATHER		RETURN S_M_Y_BLACKOPS_01
		CASE ciBACKUP_TYPE_NIGHTSHARK_BOGDAN_GOON		RETURN INT_TO_ENUM(MODEL_NAMES, HASH("MP_M_BogdanGoon"))
		CASE ciBACKUP_TYPE_INSURGENT_BOGDAN_GOON		RETURN INT_TO_ENUM(MODEL_NAMES, HASH("MP_M_BogdanGoon"))
		CASE ciBACKUP_TYPE_INSURGENT2_BOGDAN_GOON		RETURN INT_TO_ENUM(MODEL_NAMES, HASH("MP_M_BogdanGoon"))
		CASE ciBACKUP_TYPE_AIR_BUZZ_AVON_GOON			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_avongoon"))
		CASE ciBACKUP_TYPE_NIGHTSHARK_AVON_GOON			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_avongoon"))
		CASE ciBACKUP_TYPE_DUBSTA3_AVON_GOON			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_avongoon"))
		CASE ciBACKUP_TYPE_KAMACHO_PROFESSIONALS		RETURN MP_G_M_PROS_01
		CASE ciBACKUP_TYPE_CONTENDER_PROFESSIONALS		RETURN MP_G_M_PROS_01
		CASE ciBACKUP_TYPE_CARACARA_HILLBILLY			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("G_M_M_CasRN_01"))
		CASE ciBACKUP_TYPE_BOAT_KOREANS_DINGHY			RETURN G_M_Y_KOREAN_01
		CASE ciBACKUP_TYPE_BOAT_PROFESSIONALS_DINGHY	RETURN MP_G_M_PROS_01
		
		CASE ciBACKUP_TYPE_CUSTOM_1						
		CASE ciBACKUP_TYPE_CUSTOM_2						
		CASE ciBACKUP_TYPE_CUSTOM_3						
		CASE ciBACKUP_TYPE_CUSTOM_4					
			INT i
			IF GET_GANG_CHASE_UNIT_CREATOR_CONFIG_INDEX_FROM_GANG_TYPE(iGangType, i)
				RETURN g_FMMC_STRUCT.sGangChaseUnitConfigs[i].mnPedModel
			ENDIF
		BREAK
		
	ENDSWITCH

	ASSERTLN("[GANG_CHASE] GET_GANG_CHASE_UNIT_PED_MODEL - INVALID GANG TYPE: ", iGangType)
	
	#IF IS_DEBUG_BUILD
	ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_INVALID_GANG_TYPE, ENTITY_RUNTIME_ERROR_TYPE_WARNING, "Invalid Gang Type set up.")
	#ENDIF

	RETURN DUMMY_MODEL_FOR_SCRIPT

ENDFUNC	

FUNC WEAPON_TYPE GET_GANG_CHASE_UNIT_RANDOM_WEAPON_TYPE(INT iGangType)
	
	SWITCH iGangType
		CASE ciBACKUP_TYPE_MERRYWEATHER
		CASE ciBACKUP_TYPE_MERRYWEATHER_MESAS
			SWITCH GET_RANDOM_INT_IN_RANGE(0, 2)
				CASE 0 RETURN WEAPONTYPE_MICROSMG
				CASE 1 RETURN WEAPONTYPE_DLC_HEAVYPISTOL
			ENDSWITCH
		BREAK
		
		CASE ciBACKUP_TYPE_LOST
		CASE ciBACKUP_TYPE_LOST_SLAMVAN2
			SWITCH GET_RANDOM_INT_IN_RANGE(0, 3)
				CASE 0 RETURN WEAPONTYPE_MICROSMG
				CASE 1 RETURN WEAPONTYPE_PISTOL
				CASE 2 RETURN WEAPONTYPE_SAWNOFFSHOTGUN
			ENDSWITCH
		BREAK
		
		CASE ciBACKUP_TYPE_VAGOS
		CASE ciBACKUP_TYPE_FAMILIES
		CASE ciBACKUP_TYPE_BALLAS
		CASE ciBACKUP_TYPE_KOREANS
		CASE ciBACKUP_TYPE_NIGHTSHARK_BOGDAN_GOON
		CASE ciBACKUP_TYPE_INSURGENT_BOGDAN_GOON
		CASE ciBACKUP_TYPE_INSURGENT2_BOGDAN_GOON
		CASE ciBACKUP_TYPE_AIR_BUZZ_AVON_GOON
		CASE ciBACKUP_TYPE_NIGHTSHARK_AVON_GOON
		CASE ciBACKUP_TYPE_DUBSTA3_AVON_GOON
		CASE ciBACKUP_TYPE_CARACARA_HILLBILLY
			SWITCH GET_RANDOM_INT_IN_RANGE(0, 2)
				CASE 0 RETURN WEAPONTYPE_MICROSMG
				CASE 1 RETURN WEAPONTYPE_PISTOL
			ENDSWITCH
		BREAK
		
		CASE ciBACKUP_TYPE_PROFESSIONALS
		CASE ciBACKUP_TYPE_KOREANS2
		CASE ciBACKUP_TYPE_CONTENDER_PROFESSIONALS
		CASE ciBACKUP_TYPE_KAMACHO_PROFESSIONALS	
			SWITCH GET_RANDOM_INT_IN_RANGE(0, 2)
				CASE 0 RETURN WEAPONTYPE_MICROSMG
				CASE 1 RETURN WEAPONTYPE_COMBATPISTOL
			ENDSWITCH
		BREAK
		
		CASE ciBACKUP_TYPE_AIR_BUZZ_NOOSE
		CASE ciBACKUP_TYPE_AIR_MAVERICK_COP
			SWITCH GET_RANDOM_INT_IN_RANGE(0, 2)
				CASE 0 RETURN WEAPONTYPE_SMG
				CASE 1 RETURN WEAPONTYPE_DLC_SPECIALCARBINE
			ENDSWITCH
		BREAK
		
		CASE ciBACKUP_TYPE_SALVA
			SWITCH GET_RANDOM_INT_IN_RANGE(0, 3)
				CASE 0 RETURN WEAPONTYPE_MICROSMG
				CASE 1 RETURN WEAPONTYPE_DLC_SNSPISTOL
				CASE 2 RETURN WEAPONTYPE_PISTOL
			ENDSWITCH
		BREAK
		
		CASE ciBACKUP_TYPE_ALTRUISTS
			SWITCH GET_RANDOM_INT_IN_RANGE(0, 2)
				CASE 0 RETURN WEAPONTYPE_DLC_VINTAGEPISTOL
				CASE 1 RETURN WEAPONTYPE_PISTOL
			ENDSWITCH
		BREAK
		
		CASE ciBACKUP_TYPE_FIB
			SWITCH GET_RANDOM_INT_IN_RANGE(0, 2)
				CASE 0 RETURN WEAPONTYPE_DLC_HEAVYPISTOL
				CASE 1 RETURN WEAPONTYPE_PISTOL
			ENDSWITCH
		BREAK
		
		CASE ciBACKUP_TYPE_ONEIL_BROS
			SWITCH GET_RANDOM_INT_IN_RANGE(0, 3)
				CASE 0 RETURN WEAPONTYPE_MICROSMG
				CASE 1 RETURN WEAPONTYPE_PISTOL
				CASE 2 RETURN WEAPONTYPE_DLC_SNSPISTOL
			ENDSWITCH
		BREAK
		
		CASE ciBACKUP_TYPE_ARMY
		CASE ciBACKUP_TYPE_ARMY2
			SWITCH GET_RANDOM_INT_IN_RANGE(0, 2)
				CASE 0 RETURN WEAPONTYPE_APPISTOL
				CASE 1 RETURN WEAPONTYPE_PISTOL
			ENDSWITCH
		BREAK
		
		CASE ciBACKUP_TYPE_BOAT_KOREANS_DINGHY
		CASE ciBACKUP_TYPE_BOAT_PROFESSIONALS_DINGHY
			SWITCH GET_RANDOM_INT_IN_RANGE(0, 3)
				CASE 0 RETURN WEAPONTYPE_MICROSMG
				CASE 1 RETURN WEAPONTYPE_PISTOL
				CASE 2 RETURN WEAPONTYPE_DLC_MINISMG
			ENDSWITCH
		BREAK
		
		CASE ciBACKUP_TYPE_LOWRIDER_BALLAS
		CASE ciBACKUP_TYPE_LOWRIDER_VAGOS_BLA
		CASE ciBACKUP_TYPE_LOWRIDER_VAGOS_EMP
			//No Randomization
			RETURN WEAPONTYPE_MICROSMG
		BREAK
		
		CASE ciBACKUP_TYPE_CUSTOM_1						
		CASE ciBACKUP_TYPE_CUSTOM_2						
		CASE ciBACKUP_TYPE_CUSTOM_3						
		CASE ciBACKUP_TYPE_CUSTOM_4						
			INT i
			IF GET_GANG_CHASE_UNIT_CREATOR_CONFIG_INDEX_FROM_GANG_TYPE(iGangType, i)
				SWITCH GET_RANDOM_INT_IN_RANGE(0, 2)
					CASE 0 RETURN g_FMMC_STRUCT.sGangChaseUnitConfigs[i].wtRandomWeapon1
					CASE 1 RETURN g_FMMC_STRUCT.sGangChaseUnitConfigs[i].wtRandomWeapon2
				ENDSWITCH
			ENDIF
		BREAK
	ENDSWITCH
	
	ASSERTLN("[GANG_CHASE] GET_GANG_CHASE_UNIT_RANDOM_WEAPON_TYPE - INVALID GANG TYPE: ", iGangType)

	#IF IS_DEBUG_BUILD
	ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_INVALID_GANG_TYPE, ENTITY_RUNTIME_ERROR_TYPE_WARNING, "Invalid Gang Type set up.")
	#ENDIF
	
	RETURN WEAPONTYPE_INVALID
	
ENDFUNC

FUNC WEAPON_TYPE GET_GANG_CHASE_UNIT_WEAPON_TYPE(INT iGangType, BOOL bRandomWeapons)

	IF bRandomWeapons
		RETURN GET_GANG_CHASE_UNIT_RANDOM_WEAPON_TYPE(iGangType)
	ENDIF
	
	SWITCH iGangType
		CASE ciBACKUP_TYPE_MERRYWEATHER					RETURN WEAPONTYPE_MICROSMG
		CASE ciBACKUP_TYPE_LOST							RETURN WEAPONTYPE_PISTOL
		CASE ciBACKUP_TYPE_VAGOS						RETURN WEAPONTYPE_PISTOL
		CASE ciBACKUP_TYPE_FAMILIES						RETURN WEAPONTYPE_MICROSMG
		CASE ciBACKUP_TYPE_PROFESSIONALS				RETURN WEAPONTYPE_MICROSMG
		CASE ciBACKUP_TYPE_BALLAS						RETURN WEAPONTYPE_MICROSMG
		CASE ciBACKUP_TYPE_SALVA						RETURN WEAPONTYPE_MICROSMG
		CASE ciBACKUP_TYPE_ALTRUISTS					RETURN WEAPONTYPE_MICROSMG
		CASE ciBACKUP_TYPE_FIB							RETURN WEAPONTYPE_PISTOL
		CASE ciBACKUP_TYPE_ONEIL_BROS					RETURN WEAPONTYPE_PISTOL
		CASE ciBACKUP_TYPE_KOREANS						RETURN WEAPONTYPE_PISTOL
		CASE ciBACKUP_TYPE_MERRYWEATHER_MESAS			RETURN WEAPONTYPE_MICROSMG
		CASE ciBACKUP_TYPE_KOREANS2						RETURN WEAPONTYPE_MICROSMG
		CASE ciBACKUP_TYPE_ARMY							RETURN WEAPONTYPE_PISTOL
		CASE ciBACKUP_TYPE_LOST_SLAMVAN2				RETURN WEAPONTYPE_PISTOL
		CASE ciBACKUP_TYPE_ARMY2						RETURN WEAPONTYPE_PISTOL
		CASE ciBACKUP_TYPE_LOWRIDER_VAGOS_BLA			RETURN WEAPONTYPE_PISTOL
		CASE ciBACKUP_TYPE_LOWRIDER_VAGOS_EMP			RETURN WEAPONTYPE_PISTOL
		CASE ciBACKUP_TYPE_LOWRIDER_BALLAS				RETURN WEAPONTYPE_PISTOL
		CASE ciBACKUP_TYPE_AIR_BUZZ_MERRYWEATHER		RETURN WEAPONTYPE_MICROSMG
		CASE ciBACKUP_TYPE_AIR_BUZZ_NOOSE				RETURN WEAPONTYPE_MICROSMG
		CASE ciBACKUP_TYPE_AIR_BUZZ_PROFESSIONALS		RETURN WEAPONTYPE_MICROSMG
		CASE ciBACKUP_TYPE_AIR_MAVERICK_NOOSE			RETURN WEAPONTYPE_MICROSMG
		CASE ciBACKUP_TYPE_AIR_MAVERICK_COP				RETURN WEAPONTYPE_MICROSMG
		CASE ciBACKUP_TYPE_AIR_FROGGER_MERRYWEATHER		RETURN WEAPONTYPE_MICROSMG
		CASE ciBACKUP_TYPE_AIR_FROGGER_PROFESSIONALS	RETURN WEAPONTYPE_MICROSMG
		CASE ciBACKUP_TYPE_AIR_FROGGER_LOST				RETURN WEAPONTYPE_PISTOL
		CASE ciBACKUP_TYPE_AIR_FROGGER_VAGOS			RETURN WEAPONTYPE_PISTOL
		CASE ciBACKUP_TYPE_AIR_FROGGER_KOREAN			RETURN WEAPONTYPE_PISTOL
		CASE ciBACKUP_TYPE_AIR_VALKYRIE_MERRYWEATHER	RETURN WEAPONTYPE_MICROSMG
		CASE ciBACKUP_TYPE_AIR_VALKYRIE_PROFESSIONALS	RETURN WEAPONTYPE_MICROSMG
		CASE ciBACKUP_TYPE_AIR_HUNTER_MERRYWEATHER		RETURN WEAPONTYPE_MICROSMG
		CASE ciBACKUP_TYPE_AIR_HUNTER_PROFESSIONALS		RETURN WEAPONTYPE_MICROSMG
		CASE ciBACKUP_TYPE_AIR_SAVAGE_MERRYWEATHER		RETURN WEAPONTYPE_MICROSMG
		CASE ciBACKUP_TYPE_AIR_SAVAGE_PROFESSIONALS		RETURN WEAPONTYPE_MICROSMG
		CASE ciBACKUP_TYPE_AIR_LAZER_MERRYWEATHER		RETURN WEAPONTYPE_MICROSMG
		CASE ciBACKUP_TYPE_AIR_MOLOTOK_MERRYWEATHER		RETURN WEAPONTYPE_MICROSMG
		CASE ciBACKUP_TYPE_NIGHTSHARK_BOGDAN_GOON		RETURN WEAPONTYPE_MICROSMG
		CASE ciBACKUP_TYPE_INSURGENT_BOGDAN_GOON		RETURN WEAPONTYPE_MICROSMG
		CASE ciBACKUP_TYPE_INSURGENT2_BOGDAN_GOON		RETURN WEAPONTYPE_MICROSMG
		CASE ciBACKUP_TYPE_AIR_BUZZ_AVON_GOON			RETURN WEAPONTYPE_MICROSMG
		CASE ciBACKUP_TYPE_NIGHTSHARK_AVON_GOON			RETURN WEAPONTYPE_MICROSMG
		CASE ciBACKUP_TYPE_DUBSTA3_AVON_GOON			RETURN WEAPONTYPE_MICROSMG
		CASE ciBACKUP_TYPE_KAMACHO_PROFESSIONALS		RETURN WEAPONTYPE_MICROSMG
		CASE ciBACKUP_TYPE_CONTENDER_PROFESSIONALS		RETURN WEAPONTYPE_MICROSMG
		CASE ciBACKUP_TYPE_CARACARA_HILLBILLY			RETURN WEAPONTYPE_MICROSMG
		CASE ciBACKUP_TYPE_BOAT_KOREANS_DINGHY			RETURN WEAPONTYPE_DLC_MACHINEPISTOL
		CASE ciBACKUP_TYPE_BOAT_PROFESSIONALS_DINGHY	RETURN WEAPONTYPE_DLC_MACHINEPISTOL
		
		CASE ciBACKUP_TYPE_CUSTOM_1						
		CASE ciBACKUP_TYPE_CUSTOM_2						
		CASE ciBACKUP_TYPE_CUSTOM_3						
		CASE ciBACKUP_TYPE_CUSTOM_4					
			INT i
			IF GET_GANG_CHASE_UNIT_CREATOR_CONFIG_INDEX_FROM_GANG_TYPE(iGangType, i)		
				RETURN g_FMMC_STRUCT.sGangChaseUnitConfigs[i].wtDefaultWeapon				
			ENDIF
		BREAK
	ENDSWITCH

	ASSERTLN("[GANG_CHASE] GET_GANG_CHASE_UNIT_WEAPON_TYPE - INVALID GANG TYPE: ", iGangType)

	#IF IS_DEBUG_BUILD
	ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_INVALID_GANG_TYPE, ENTITY_RUNTIME_ERROR_TYPE_WARNING, "Invalid Gang Type set up.")
	#ENDIF
	
	RETURN WEAPONTYPE_INVALID

ENDFUNC

FUNC FLOAT GET_GANG_CHASE_UNIT_PED_ARMOUR(INT iGangType)
	SWITCH iGangType
		CASE ciBACKUP_TYPE_CUSTOM_1						
		CASE ciBACKUP_TYPE_CUSTOM_2						
		CASE ciBACKUP_TYPE_CUSTOM_3						
		CASE ciBACKUP_TYPE_CUSTOM_4					
			INT i
			IF GET_GANG_CHASE_UNIT_CREATOR_CONFIG_INDEX_FROM_GANG_TYPE(iGangType, i)		
				RETURN g_FMMC_STRUCT.sGangChaseUnitConfigs[i].fPedArmour * 0.01				
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN 0.0
ENDFUNC

FUNC BOOL GET_GANG_CHASE_UNIT_USE_ALTERNATE_COMPONENTS_FOR_PED_OUTFIT(INT iGangType)
	SWITCH iGangType
		CASE ciBACKUP_TYPE_CUSTOM_1						
		CASE ciBACKUP_TYPE_CUSTOM_2						
		CASE ciBACKUP_TYPE_CUSTOM_3						
		CASE ciBACKUP_TYPE_CUSTOM_4					
			INT i
			IF GET_GANG_CHASE_UNIT_CREATOR_CONFIG_INDEX_FROM_GANG_TYPE(iGangType, i)		
				RETURN g_FMMC_STRUCT.sGangChaseUnitConfigs[i].bUseAlternateComponentsForPedOutfit				
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_GANG_CHASE_UNIT_OVERRIDE_VEHICLE_COLOUR1_TO_USE(INT iGangType)
	SWITCH iGangType
		CASE ciBACKUP_TYPE_CUSTOM_1						
		CASE ciBACKUP_TYPE_CUSTOM_2						
		CASE ciBACKUP_TYPE_CUSTOM_3						
		CASE ciBACKUP_TYPE_CUSTOM_4					
			INT i
			IF GET_GANG_CHASE_UNIT_CREATOR_CONFIG_INDEX_FROM_GANG_TYPE(iGangType, i)		
				RETURN g_FMMC_STRUCT.sGangChaseUnitConfigs[i].iOverrideVehicleColour1				
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN -1
ENDFUNC

FUNC INT GET_GANG_CHASE_UNIT_OVERRIDE_VEHICLE_COLOUR2_TO_USE(INT iGangType)
	SWITCH iGangType
		CASE ciBACKUP_TYPE_CUSTOM_1						
		CASE ciBACKUP_TYPE_CUSTOM_2						
		CASE ciBACKUP_TYPE_CUSTOM_3						
		CASE ciBACKUP_TYPE_CUSTOM_4					
			INT i
			IF GET_GANG_CHASE_UNIT_CREATOR_CONFIG_INDEX_FROM_GANG_TYPE(iGangType, i)		
				RETURN g_FMMC_STRUCT.sGangChaseUnitConfigs[i].iOverrideVehicleColour2				
			ENDIF
		BREAK
	ENDSWITCH

	RETURN -1
ENDFUNC

FUNC INT GET_GANG_CHASE_UNIT_MOD_PRESET_TO_USE(INT iGangType)
	SWITCH iGangType
		CASE ciBACKUP_TYPE_CUSTOM_1						
		CASE ciBACKUP_TYPE_CUSTOM_2						
		CASE ciBACKUP_TYPE_CUSTOM_3						
		CASE ciBACKUP_TYPE_CUSTOM_4					
			INT i
			IF GET_GANG_CHASE_UNIT_CREATOR_CONFIG_INDEX_FROM_GANG_TYPE(iGangType, i)		
				RETURN g_FMMC_STRUCT.sGangChaseUnitConfigs[i].iVehicleModPresetToUse				
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN -1
ENDFUNC

FUNC INT GET_GANG_CHASE_UNIT_LIVERY_PRESET_TO_USE(INT iGangType)
	SWITCH iGangType
		CASE ciBACKUP_TYPE_CUSTOM_1						
		CASE ciBACKUP_TYPE_CUSTOM_2						
		CASE ciBACKUP_TYPE_CUSTOM_3						
		CASE ciBACKUP_TYPE_CUSTOM_4					
			INT i
			IF GET_GANG_CHASE_UNIT_CREATOR_CONFIG_INDEX_FROM_GANG_TYPE(iGangType, i)		
				RETURN g_FMMC_STRUCT.sGangChaseUnitConfigs[i].iVehicleLiveryToUse				
			ENDIF
		BREAK
		
		DEFAULT RETURN -1		
	ENDSWITCH
	RETURN -1
ENDFUNC

FUNC INT GET_GANG_CHASE_UNIT_MAX_PEDS(INT iGangType, BOOL bSoloBikes)

	SWITCH iGangType
		CASE ciBACKUP_TYPE_LOST
			IF bSoloBikes
			OR GET_RANDOM_INT_IN_RANGE(0, 10) < 6
				RETURN 1
			ENDIF
			
			RETURN 2
		BREAK
		CASE ciBACKUP_TYPE_MERRYWEATHER					RETURN 2
		CASE ciBACKUP_TYPE_VAGOS						RETURN 2
		CASE ciBACKUP_TYPE_FAMILIES						RETURN 2
		CASE ciBACKUP_TYPE_PROFESSIONALS				RETURN 2
		CASE ciBACKUP_TYPE_BALLAS						RETURN 2
		CASE ciBACKUP_TYPE_SALVA						RETURN 2
		CASE ciBACKUP_TYPE_ALTRUISTS					RETURN 2
		CASE ciBACKUP_TYPE_FIB							RETURN 2
		CASE ciBACKUP_TYPE_ONEIL_BROS					RETURN 2
		CASE ciBACKUP_TYPE_KOREANS						RETURN 2
		CASE ciBACKUP_TYPE_MERRYWEATHER_MESAS			RETURN 2
		CASE ciBACKUP_TYPE_KOREANS2						RETURN 2
		CASE ciBACKUP_TYPE_ARMY							RETURN 2
		CASE ciBACKUP_TYPE_LOST_SLAMVAN2				RETURN 2
		CASE ciBACKUP_TYPE_ARMY2						RETURN 2
		CASE ciBACKUP_TYPE_LOWRIDER_VAGOS_BLA			RETURN 2
		CASE ciBACKUP_TYPE_LOWRIDER_VAGOS_EMP			RETURN 2
		CASE ciBACKUP_TYPE_LOWRIDER_BALLAS				RETURN 2
		CASE ciBACKUP_TYPE_AIR_BUZZ_MERRYWEATHER		RETURN 2
		CASE ciBACKUP_TYPE_AIR_BUZZ_NOOSE				RETURN 2
		CASE ciBACKUP_TYPE_AIR_BUZZ_PROFESSIONALS		RETURN 2
		CASE ciBACKUP_TYPE_AIR_MAVERICK_NOOSE			RETURN 2
		CASE ciBACKUP_TYPE_AIR_MAVERICK_COP				RETURN 2
		CASE ciBACKUP_TYPE_AIR_FROGGER_MERRYWEATHER		RETURN 2
		CASE ciBACKUP_TYPE_AIR_FROGGER_PROFESSIONALS	RETURN 2
		CASE ciBACKUP_TYPE_AIR_FROGGER_LOST				RETURN 2
		CASE ciBACKUP_TYPE_AIR_FROGGER_VAGOS			RETURN 2 
		CASE ciBACKUP_TYPE_AIR_FROGGER_KOREAN			RETURN 2
		CASE ciBACKUP_TYPE_AIR_VALKYRIE_MERRYWEATHER	RETURN 4
		CASE ciBACKUP_TYPE_AIR_VALKYRIE_PROFESSIONALS	RETURN 4
		CASE ciBACKUP_TYPE_AIR_HUNTER_MERRYWEATHER		RETURN 2
		CASE ciBACKUP_TYPE_AIR_HUNTER_PROFESSIONALS		RETURN 2
		CASE ciBACKUP_TYPE_AIR_SAVAGE_MERRYWEATHER		RETURN 2
		CASE ciBACKUP_TYPE_AIR_SAVAGE_PROFESSIONALS		RETURN 2
		CASE ciBACKUP_TYPE_AIR_LAZER_MERRYWEATHER		RETURN 2
		CASE ciBACKUP_TYPE_AIR_MOLOTOK_MERRYWEATHER		RETURN 2
		CASE ciBACKUP_TYPE_NIGHTSHARK_BOGDAN_GOON		RETURN 2
		CASE ciBACKUP_TYPE_INSURGENT_BOGDAN_GOON		RETURN 2
		CASE ciBACKUP_TYPE_INSURGENT2_BOGDAN_GOON		RETURN 2
		CASE ciBACKUP_TYPE_AIR_BUZZ_AVON_GOON			RETURN 2
		CASE ciBACKUP_TYPE_NIGHTSHARK_AVON_GOON			RETURN 2
		CASE ciBACKUP_TYPE_DUBSTA3_AVON_GOON			RETURN 2
		CASE ciBACKUP_TYPE_KAMACHO_PROFESSIONALS		RETURN 2
		CASE ciBACKUP_TYPE_CONTENDER_PROFESSIONALS		RETURN 2 
		CASE ciBACKUP_TYPE_CARACARA_HILLBILLY			RETURN 2
		CASE ciBACKUP_TYPE_BOAT_KOREANS_DINGHY			RETURN 2
		CASE ciBACKUP_TYPE_BOAT_PROFESSIONALS_DINGHY	RETURN 2
		
		CASE ciBACKUP_TYPE_CUSTOM_1						
		CASE ciBACKUP_TYPE_CUSTOM_2						
		CASE ciBACKUP_TYPE_CUSTOM_3						
		CASE ciBACKUP_TYPE_CUSTOM_4					
			INT i
			IF GET_GANG_CHASE_UNIT_CREATOR_CONFIG_INDEX_FROM_GANG_TYPE(iGangType, i)		
				RETURN g_FMMC_STRUCT.sGangChaseUnitConfigs[i].iMaxPeds				
			ENDIF
		BREAK
	ENDSWITCH

	ASSERTLN("[GANG_CHASE] GET_GANG_CHASE_UNIT_MAX_PEDS - INVALID GANG TYPE: ", iGangType)
	
	#IF IS_DEBUG_BUILD
	ADD_MISSION_STATE_RUNTIME_ERROR(ENTITY_RUNTIME_ERROR_UNIQUE_ID_INVALID_GANG_TYPE, ENTITY_RUNTIME_ERROR_TYPE_WARNING, "Invalid Gang Type set up.")
	#ENDIF
	
	RETURN 0

ENDFUNC

FUNC FMMC_GANG_CHASE_TYPE GET_IDEAL_GANG_CHASE_SPAWN_TYPE_FOR_LOCAL_PLAYER()
	
	IF !bLocalPlayerPedOk
		RETURN FMMC_GANG_CHASE_TYPE_NONE
	ENDIF
	
	IF NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed, TRUE)
		IF IS_ENTITY_IN_DEEP_WATER(LocalPlayerPed)
			RETURN FMMC_GANG_CHASE_TYPE_SEA
		ENDIF
	
		RETURN FMMC_GANG_CHASE_TYPE_ON_FOOT
	ENDIF
	
	IF IS_PED_IN_FLYING_VEHICLE(LocalPlayerPed)
		RETURN FMMC_GANG_CHASE_TYPE_AIR
	ENDIF
	
	IF IS_PED_IN_ANY_BOAT(LocalPlayerPed)
		RETURN FMMC_GANG_CHASE_TYPE_SEA
	ENDIF
	
	VEHICLE_INDEX viVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed, TRUE)
	IF IS_ENTITY_ALIVE(viVeh)
		IF IS_ENTITY_IN_WATER(viVeh)
			RETURN FMMC_GANG_CHASE_TYPE_SEA
		ENDIF
	ENDIF
	
	RETURN FMMC_GANG_CHASE_TYPE_LAND
	
ENDFUNC

FUNC BOOL IS_SPAWN_TYPE_BLOCKED_BY_IDEAL_SPAWN_TYPE(FMMC_GANG_CHASE_TYPE eIdealType, FMMC_GANG_CHASE_TYPE eType)

	SWITCH eIdealType
	
		CASE FMMC_GANG_CHASE_TYPE_LAND
			
			IF eType = FMMC_GANG_CHASE_TYPE_SEA
				RETURN TRUE
			ENDIF
			
		BREAK
		
		CASE FMMC_GANG_CHASE_TYPE_SEA
			
			IF eType = FMMC_GANG_CHASE_TYPE_LAND
				RETURN TRUE
			ENDIF
			
			IF eType = FMMC_GANG_CHASE_TYPE_ON_FOOT
				RETURN TRUE
			ENDIF
			
		BREAK
		
		CASE FMMC_GANG_CHASE_TYPE_AIR
		
			IF eType = FMMC_GANG_CHASE_TYPE_LAND
				RETURN TRUE
			ENDIF
			
			IF eType = FMMC_GANG_CHASE_TYPE_ON_FOOT
				RETURN TRUE
			ENDIF
			
			IF eType = FMMC_GANG_CHASE_TYPE_SEA
				RETURN TRUE
			ENDIF
			
		BREAK
		
		CASE FMMC_GANG_CHASE_TYPE_ON_FOOT
			
			IF eType = FMMC_GANG_CHASE_TYPE_SEA
				RETURN TRUE
			ENDIF
			
		BREAK
		
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

FUNC INT GET_GANG_CHASE_UNIT_SPAWN_TYPE_SCORE(FMMC_GANG_CHASE_TYPE eIdealType, FMMC_GANG_CHASE_TYPE eType)

	SWITCH eIdealType
		CASE FMMC_GANG_CHASE_TYPE_LAND
			SWITCH eType
				CASE FMMC_GANG_CHASE_TYPE_LAND		RETURN 100
				CASE FMMC_GANG_CHASE_TYPE_AIR		RETURN 75
				CASE FMMC_GANG_CHASE_TYPE_ON_FOOT	RETURN 25
			ENDSWITCH
		BREAK
		
		CASE FMMC_GANG_CHASE_TYPE_SEA
			SWITCH eType
				CASE FMMC_GANG_CHASE_TYPE_SEA		RETURN 100
				CASE FMMC_GANG_CHASE_TYPE_AIR		RETURN 75
			ENDSWITCH
		BREAK
		
		CASE FMMC_GANG_CHASE_TYPE_AIR
			SWITCH eType
				CASE FMMC_GANG_CHASE_TYPE_AIR		RETURN 100
			ENDSWITCH
		BREAK
		
		CASE FMMC_GANG_CHASE_TYPE_ON_FOOT
			SWITCH eType
				CASE FMMC_GANG_CHASE_TYPE_ON_FOOT	RETURN 100
				CASE FMMC_GANG_CHASE_TYPE_AIR		RETURN 75
				CASE FMMC_GANG_CHASE_TYPE_LAND		RETURN 50
			ENDSWITCH
		BREAK
		
	ENDSWITCH

	RETURN 0
	
ENDFUNC

FUNC INT GET_GANG_CHASE_UNIT_PRIORITY_SCORE(INT iPriority)
	
	SWITCH iPriority
		CASE FMMC_MAX_GANG_CHASE_PRIORITY_LOW		RETURN 25
		CASE FMMC_MAX_GANG_CHASE_PRIORITY_DEFAULT	RETURN 50
		CASE FMMC_MAX_GANG_CHASE_PRIORITY_HIGH		RETURN 100
	ENDSWITCH
	
	RETURN 0
	
ENDFUNC

FUNC INT GET_NUMBER_OF_UNIT_TYPE_SPAWNED(INT iUnitType)
	INT i, iCount 
	FOR i = 0 TO ciMAX_GANG_CHASE_VEHICLES - 1
		
		IF MC_serverBD_1.sGangChase[i].iGangType = iUnitType
		AND IS_BIT_SET(MC_serverBD_1.sGangChase[i].iBitset, SBBBOOL_CREATED)
			iCount++
		ENDIF
		
	ENDFOR
	
	RETURN iCount
ENDFUNC

FUNC INT GET_NUMBER_OF_UNIT_CHASING_PLAYER(INT iUnitType, INT iPart)

	INT i, iCount 
	FOR i = 0 TO ciMAX_GANG_CHASE_VEHICLES - 1
		
		IF MC_serverBD_1.sGangChase[i].iGangType != iUnitType
			RELOOP
		ENDIF
		
		IF MC_serverBD_1.sGangChase[i].iTargetPart = iPart
		AND IS_BIT_SET(MC_serverBD_1.sGangChase[i].iBitset, SBBBOOL_CREATED)
			iCount++
		ENDIF
		
	ENDFOR
	
	RETURN iCount
	
ENDFUNC

FUNC INT GET_GANG_CHASE_UNIT_TYPE_MAX_INTENSITY(INT iTeam, INT iRule, INT iUnit)

	INT iIntensitySetting = RETURN_INT_CUSTOM_VARIABLE_VALUE(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sMultipleGangChaseSettings[iRule][iUnit].iIntensity)
	
	IF iIntensitySetting = FMMC_GANG_CHASE_INTENSITY_PLAYER_NUM
		iIntensitySetting = MC_serverBD_1.iVariableNumBackupAllowed[iTeam]
	ELIF iIntensitySetting = FMMC_GANG_CHASE_INTENSITY_PLAYER_NUM_PLUS_ONE
		iIntensitySetting = MC_serverBD_1.iVariableNumBackupAllowed[iTeam] + 1
	ELIF iIntensitySetting = FMMC_GANG_CHASE_INTENSITY_DEFAULT
		iIntensitySetting = GET_GANG_CHASE_INTENSITY(iTeam, iRule)
	ENDIF
	
	RETURN CLAMP_INT(iIntensitySetting, 0, FMMC_MAX_GANG_CHASE_INTENSITY)

ENDFUNC

FUNC BOOL CAN_GANG_CHASE_TYPE_SPAWN(INT iUnitType, INT iTeam, INT iRule, INT iIndex = -1)
	
	IF iIndex = -1
		INT i
		FOR i = 0 TO FMMC_MAX_GANG_CHASE_UNIT_TYPES - 1
			IF iUnitType = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sMultipleGangChaseSettings[iRule][i].iType
				iIndex = i
				BREAKLOOP
			ENDIF
		ENDFOR
	ENDIF
	
	IF iIndex = -1
		RETURN TRUE
	ENDIF
	
	IF GET_NUMBER_OF_UNIT_TYPE_SPAWNED(iUnitType) >= GET_GANG_CHASE_UNIT_TYPE_MAX_INTENSITY(iTeam, iRule, iIndex)
		RETURN FALSE
	ENDIF
	
	IF RETURN_INT_CUSTOM_VARIABLE_VALUE(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sMultipleGangChaseSettings[iRule][iIndex].iIntensity) = FMMC_GANG_CHASE_INTENSITY_PLAYER_NUM
		IF GET_NUMBER_OF_UNIT_CHASING_PLAYER(iUnitType, iLocalPart) >= 1
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC INT GET_SUITABLE_GANG_CHASE_UNIT_FOR_LOCAL_PLAYER(INT iTeam, INT iRule)
	
	INT iBestType = -1
	INT iBestTypesScore = -1
	
	FMMC_GANG_CHASE_TYPE eIdealSpawnType = GET_IDEAL_GANG_CHASE_SPAWN_TYPE_FOR_LOCAL_PLAYER()
	
	INT i
	FOR i = 0 TO FMMC_MAX_GANG_CHASE_UNIT_TYPES - 1
		
		INT iUnitType = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sMultipleGangChaseSettings[iRule][i].iType
		
		IF iUnitType = ciBACKUP_TYPE_NONE
			RELOOP
		ENDIF
		
		IF IS_SPAWN_TYPE_BLOCKED_BY_IDEAL_SPAWN_TYPE(eIdealSpawnType, GET_CUSTOM_GANG_CHASE_TYPE(iUnitType))
			RELOOP
		ENDIF
		
		IF NOT CAN_GANG_CHASE_TYPE_SPAWN(iUnitType, iTeam, iRule, i)
			RELOOP
		ENDIF
		
		INT iUnitScore = 0
		
		iUnitScore += GET_GANG_CHASE_UNIT_SPAWN_TYPE_SCORE(eIdealSpawnType, GET_CUSTOM_GANG_CHASE_TYPE(iUnitType))
		
		iUnitScore += GET_GANG_CHASE_UNIT_PRIORITY_SCORE(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sMultipleGangChaseSettings[iRule][i].iPriority)
		
		IF iUnitScore > iBestTypesScore
		OR (iUnitScore = iBestTypesScore AND GET_RANDOM_BOOL()) //If we have a tie give it a 50% chance to overwrite
			iBestTypesScore = iUnitScore
			iBestType = iUnitType
		ENDIF
		
	ENDFOR
	
	RETURN iBestType
	
ENDFUNC

FUNC INT GET_GANG_CHASE_UNIT_TYPE_FOR_PLAYER(INT iTeam, INT iRule)

	// check for single unit type
	IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iGangBackupType[iRule] != ciBACKUP_TYPE_NONE
		RETURN g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iGangBackupType[iRule]
	ENDIF
	
	IF NOT ARE_MULTIPLE_GANG_CHASE_UNIT_TYPES_AVAILABLE_FOR_TEAM(iTeam)
		RETURN -1
	ENDIF
	
	RETURN GET_SUITABLE_GANG_CHASE_UNIT_FOR_LOCAL_PLAYER(iTeam, iRule)
	
ENDFUNC

FUNC BOOL GET_GANG_CHASE_UNIT_WEAPONS_DISRUPTED(INT iGangType)

	INT i
	SWITCH iGangType
		CASE ciBACKUP_TYPE_CUSTOM_1
		CASE ciBACKUP_TYPE_CUSTOM_2
		CASE ciBACKUP_TYPE_CUSTOM_3
		CASE ciBACKUP_TYPE_CUSTOM_4
			IF GET_GANG_CHASE_UNIT_CREATOR_CONFIG_INDEX_FROM_GANG_TYPE(iGangType, i)		
				RETURN g_FMMC_STRUCT.sGangChaseUnitConfigs[i].bWeaponsDisrupted				
			ENDIF
		BREAK
		
		DEFAULT BREAK
	ENDSWITCH
	  
	 RETURN FALSE
ENDFUNC

PROC GET_GANG_CHASE_TYPE_DATA(GANG_CHASE_UNIT_CONFIG_DATA &sGangChaseUnit, INT iGangChaseUnitIndex, INT iChosenType)
	
	BOOL bSoloBikesOnly = IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[sGangChaseUnit.iTeam].iRuleBitsetThirteen[sGangChaseUnit.iRule], ciBS_RULE13_GANG_CHASE_SOLO_BIKES_ONLY)
	
	sGangChaseUnit.iGangType = GET_GANG_CHASE_UNIT_GANG_TYPE(sGangChaseUnit.iTeam, sGangChaseUnit.iRule, iGangChaseUnitIndex, iChosenType)
	sGangChaseUnit.mnPedModel = GET_GANG_CHASE_UNIT_PED_MODEL(sGangChaseUnit.iGangType)
	sGangChaseUnit.mnVehicleModel = GET_GANG_CHASE_UNIT_VEHICLE_MODEL(sGangChaseUnit.iGangType)
	sGangChaseUnit.iMaxPeds = GET_GANG_CHASE_UNIT_MAX_PEDS(sGangChaseUnit.iGangType, bSoloBikesOnly)
	sGangChaseUnit.fPedArmour = GET_GANG_CHASE_UNIT_PED_ARMOUR(sGangChaseUnit.iGangType)
	sGangChaseUnit.bUseAlternateComponentsForPedOutfit = GET_GANG_CHASE_UNIT_USE_ALTERNATE_COMPONENTS_FOR_PED_OUTFIT(sGangChaseUnit.iGangType)
	sGangChaseUnit.iOverrideVehicleColour1 = GET_GANG_CHASE_UNIT_OVERRIDE_VEHICLE_COLOUR1_TO_USE(sGangChaseUnit.iGangType)
	sGangChaseUnit.iOverrideVehicleColour2 = GET_GANG_CHASE_UNIT_OVERRIDE_VEHICLE_COLOUR2_TO_USE(sGangChaseUnit.iGangType)
	sGangChaseUnit.iVehicleModPresetToUse = GET_GANG_CHASE_UNIT_MOD_PRESET_TO_USE(sGangChaseUnit.iGangType)
	sGangChaseUnit.iVehicleLivery = GET_GANG_CHASE_UNIT_LIVERY_PRESET_TO_USE(sGangChaseUnit.iGangType)
	
	BOOL bRandomWeapons = (IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[sGangChaseUnit.iTeam].iRuleBitsetThree[sGangChaseUnit.iRule], ciBS_RULE3_RANDOMISE_GANG_CHASE_WEAPONS) OR GET_GANG_CHASE_UNIT_WEAPONS_DISRUPTED(sGangChaseUnit.iGangType))
	sGangChaseUnit.wtWeapon = GET_GANG_CHASE_UNIT_WEAPON_TYPE(sGangChaseUnit.iGangType, bRandomWeapons)
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Gang Chase Vehicle Spawning
// ##### Description: Functions dealing with creating gang chase vehicles
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_CREATED_GANG_CHASE_UNIT_HELI_BLADES(INT iGangChaseUnit, GANG_CHASE_UNIT_CONFIG_DATA &sGangChaseUnit)

	IF NOT IS_BIT_SET(MC_serverBD_1.sGangChase[iGangChaseUnit].iBitset, SBBBOOL_AIR_VEHICLE_IS_FROZEN)
		EXIT
	ENDIF

	IF IS_ENTITY_DEAD(sGangChaseUnit.viVehicle)
		EXIT
	ENDIF
	
	IF NOT IS_VEHICLE_DRIVEABLE(sGangChaseUnit.viVehicle)
		EXIT
	ENDIF
	
	IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(sGangChaseUnit.viVehicle)
		PRINTLN("[GANG_CHASE] PROCESS_CREATED_GANG_CHASE_UNIT_HELI_BLADES - Unit: ", iGangChaseUnit, " Requesting control to maintain the blades")
		NETWORK_REQUEST_CONTROL_OF_ENTITY(sGangChaseUnit.viVehicle)
		EXIT
	ENDIF
	
	SET_HELI_BLADES_FULL_SPEED(sGangChaseUnit.viVehicle)
		
ENDPROC

PROC SET_UP_GANG_CHASE_VEHICLE(INT iGangChaseUnit, GANG_CHASE_UNIT_CONFIG_DATA &sGangChaseUnit)
	
	SET_VEHICLE_HAS_STRONG_AXLES(sGangChaseUnit.viVehicle, TRUE)
	SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(sGangChaseUnit.viVehicle, TRUE)
	SET_VEHICLE_ON_GROUND_PROPERLY(sGangChaseUnit.viVehicle)
	SET_VEHICLE_ALLOW_NO_PASSENGERS_LOCKON(sGangChaseUnit.viVehicle, TRUE)
	SET_VEHICLE_IS_STOLEN(sGangChaseUnit.viVehicle, FALSE)
	SET_VEHICLE_MAY_BE_USED_BY_GOTO_POINT_ANY_MEANS(sGangChaseUnit.viVehicle, TRUE)
	SET_VEHICLE_CAN_BE_USED_BY_FLEEING_PEDS(sGangChaseUnit.viVehicle, TRUE)
	SET_VEHICLE_ENGINE_ON(sGangChaseUnit.viVehicle, TRUE, TRUE)
	
	IF IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(sGangChaseUnit.viVehicle))
	OR IS_THIS_MODEL_A_FIGHTER_JET(GET_ENTITY_MODEL(sGangChaseUnit.viVehicle))
		CONTROL_LANDING_GEAR(sGangChaseUnit.viVehicle, LGC_RETRACT_INSTANT)
		PRINTLN("[GANG_CHASE] SET_UP_GANG_CHASE_VEHICLE - Unit: ", iGangChaseUnit, " Setting vehicle's landing gear to retract.")
	ENDIF
	
	IF IS_GANG_CHASE_SEA(sGangChaseUnit.iGangType)
		SET_BOAT_IGNORE_LAND_PROBES(sGangChaseUnit.viVehicle, TRUE)
		ACTIVATE_PHYSICS(sGangChaseUnit.viVehicle)
		SET_ENTITY_DYNAMIC(sGangChaseUnit.viVehicle, TRUE)
		SET_BOAT_ANCHOR(sGangChaseUnit.viVehicle, FALSE)
		SET_BOAT_LOW_LOD_ANCHOR_DISTANCE(sGangChaseUnit.viVehicle, 99999.0)
	ENDIF	
	
	IF sGangChaseUnit.iOverrideVehicleColour1 > -1
	OR sGangChaseUnit.iOverrideVehicleColour2 > -1
		FMMC_SET_THIS_VEHICLE_COLOURS(sGangChaseUnit.viVehicle, GET_VEHICLE_COLOUR_FROM_SELECTION(sGangChaseUnit.iOverrideVehicleColour1), -1, DEFAULT, GET_VEHICLE_COLOUR_FROM_SELECTION(sGangChaseUnit.iOverrideVehicleColour2))
		PRINTLN("[GANG_CHASE] SET_UP_GANG_CHASE_VEHICLE - Unit: ", iGangChaseUnit, " Setting vehicle's colour from override settings: ", sGangChaseUnit.iOverrideVehicleColour1, " / ", sGangChaseUnit.iOverrideVehicleColour2)
	ELSE
		FMMC_SET_THIS_VEHICLE_COLOURS(sGangChaseUnit.viVehicle, GET_VEHICLE_COLOUR_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[sGangChaseUnit.iTeam].iGangBackupColour[sGangChaseUnit.iRule]), GET_VEHICLE_COLOUR_FROM_SELECTION(sGangChaseUnit.iOverrideVehicleColour1), -1)
		PRINTLN("[GANG_CHASE] SET_UP_GANG_CHASE_VEHICLE - Unit: ", iGangChaseUnit, " Setting vehicle's colour: ", g_FMMC_STRUCT.sFMMCEndConditions[sGangChaseUnit.iTeam].iGangBackupColour[sGangChaseUnit.iRule])
	ENDIF
	
	IF sGangChaseUnit.iVehicleLivery > -1
		SET_VEHICLE_MOD(sGangChaseUnit.viVehicle, MOD_LIVERY, CLAMP_INT(sGangChaseUnit.iVehicleLivery, -1, GET_NUM_VEHICLE_MODS(sGangChaseUnit.viVehicle, MOD_LIVERY) - 1), FALSE)
		PRINTLN("[GANG_CHASE] SET_UP_GANG_CHASE_VEHICLE - Unit: ", iGangChaseUnit, " Setting vehicle's livery: ", sGangChaseUnit.iVehicleLivery)
	ENDIF
	
	INT iGangVehHealth = RETURN_INT_CUSTOM_VARIABLE_VALUE(g_FMMC_STRUCT.sFMMCEndConditions[sGangChaseUnit.iTeam].iGangVehHealth[sGangChaseUnit.iRule])
	IF iGangVehHealth != 100
	AND iGangVehHealth > 0
		FLOAT fHealth = (iGangVehHealth / 100.0) * 1000.0
		SET_ENTITY_HEALTH(sGangChaseUnit.viVehicle, ROUND(fHealth))
		SET_VEHICLE_ENGINE_HEALTH(sGangChaseUnit.viVehicle, fHealth)
		SET_VEHICLE_PETROL_TANK_HEALTH(sGangChaseUnit.viVehicle, fHealth)
		SET_VEHICLE_BODY_HEALTH(sGangChaseUnit.viVehicle, fHealth)
		IF fHealth <= cfGANG_CHASE_VEHICLE_ENGINE_DAMAGE_THRESHOLD
			SET_FORCE_VEHICLE_ENGINE_DAMAGE_BY_BULLET(sGangChaseUnit.viVehicle, TRUE)
		ENDIF
		
		PRINTLN("[GANG_CHASE] SET_UP_GANG_CHASE_VEHICLE - Unit: ", iGangChaseUnit, " Setting vehicle's health: ", fHealth)
	ENDIF
	
	NETWORK_FADE_IN_ENTITY(sGangChaseUnit.viVehicle, TRUE, FALSE)
		
	IF sGangChaseUnit.iGangType = ciBACKUP_TYPE_KOREANS2
		SET_VEHICLE_MOD_KIT(sGangChaseUnit.viVehicle, 0)
		TOGGLE_VEHICLE_MOD(sGangChaseUnit.viVehicle, MOD_TOGGLE_TURBO, TRUE)
	ELIF sGangChaseUnit.iGangType = ciBACKUP_TYPE_DUBSTA3_AVON_GOON
		FMMC_SET_VEH_MOD_PRESET(sGangChaseUnit.viVehicle, DUBSTA3, 0)
	ELIF sGangChaseUnit.iGangType = ciBACKUP_TYPE_KAMACHO_PROFESSIONALS
		FMMC_SET_VEH_MOD_PRESET(sGangChaseUnit.viVehicle, KAMACHO, 0)
	ENDIF
		
	IF IS_GANG_CHASE_AIR(sGangChaseUnit.iGangType)
		SET_HELI_BLADES_FULL_SPEED(sGangChaseUnit.viVehicle)
		FREEZE_ENTITY_POSITION(sGangChaseUnit.viVehicle, TRUE)
		SET_BIT(MC_serverBD_1.sGangChase[iGangChaseUnit].iBitset, SBBBOOL_AIR_VEHICLE_IS_FROZEN)
		PRINTLN("[GANG_CHASE] SET_UP_GANG_CHASE_VEHICLE - Unit: ", iGangChaseUnit, " Air Vehicle Setup")
	ELSE
		FLOAT fForwardSpeed = PICK_FLOAT(sGangChaseUnit.bSpawnForwards, cfGANG_CHASE_VEHICLE_SPEED_FORWARDS, cfGANG_CHASE_VEHICLE_SPEED_BACKWARDS)
		SET_VEHICLE_FORWARD_SPEED(sGangChaseUnit.viVehicle, fForwardSpeed)
		PRINTLN("[GANG_CHASE] SET_UP_GANG_CHASE_VEHICLE - Unit: ", iGangChaseUnit, " Setting vehicle's forward speed: ", fForwardSpeed)
	ENDIF
	
	IF sGangChaseUnit.iVehicleModPresetToUse > -1
		FMMC_SET_VEH_MOD_PRESET(sGangChaseUnit.viVehicle, sGangChaseUnit.mnVehicleModel, sGangChaseUnit.iVehicleModPresetToUse, DEFAULT)
		PRINTLN("[GANG_CHASE] SET_UP_GANG_CHASE_VEHICLE - Unit: ", iGangChaseUnit, " Setting vehicle's mod preset: ", sGangChaseUnit.iVehicleModPresetToUse)
	ENDIF
	
ENDPROC

FUNC BOOL CREATE_GANG_CHASE_UNIT_VEHICLE(INT iGangChaseUnit, GANG_CHASE_UNIT_CONFIG_DATA &sGangChaseUnit)
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sGangChase[iGangChaseUnit].niVeh)
		VEHICLE_INDEX viVehicle = NET_TO_VEH(MC_serverBD_1.sGangChase[iGangChaseUnit].niVeh)
		sGangChaseUnit.viVehicle = viVehicle
		RETURN TRUE
	ENDIF
	
	sGangChaseUnit.bTriedForwards = IS_BIT_SET(MC_PlayerBD[sGangChaseUnit.iTargetPart].iGangChaseSpawnBitset, PBGANGSPAWN_UNIT_0_TRY_FORWARDS + iGangChaseUnit)
	sGangChaseUnit.bSpawnForwards = FALSE
	
	FLOAT fSpawnHeading
	FLOAT fSpawnPedHeading = GET_ENTITY_HEADING(sGangChaseUnit.piTargetPed)
	VECTOR vSpawnPedCoords = GET_ENTITY_COORDS(sGangChaseUnit.piTargetPed, FALSE)
		
	IF IS_BIT_SET(MC_PlayerBD[sGangChaseUnit.iTargetPart].iGangChaseSpawnBitset, PBGANGSPAWN_UNIT_0_SUCCEED_FORWARDS + iGangChaseUnit)
	AND DOES_NODE_HAVE_SUITABLE_GANG_CHASE_HEADING(sGangChaseUnit.vPosition, fSpawnPedHeading, fSpawnHeading, sGangChaseUnit.iGangType, DEFAULT, g_FMMC_STRUCT.fGangChaseUnitMaxDistance)
		sGangChaseUnit.bSpawnForwards = TRUE
		SET_BIT(MC_serverBD_1.sGangChase[iGangChaseUnit].iBitset, SBBBOOL_SPAWNING_FORWARDS)
		PRINTLN("[GANG_CHASE] CREATE_GANG_CHASE_UNIT_VEHICLE - Unit: ", iGangChaseUnit, " Spawning forward with heading: ", fSpawnHeading)
	ENDIF
	
	IF NOT sGangChaseUnit.bSpawnForwards
		IF sGangChaseUnit.bTriedForwards
			IF NOT DOES_NODE_HAVE_SUITABLE_GANG_CHASE_HEADING(sGangChaseUnit.vPosition, GET_HEADING_BETWEEN_VECTORS_2D(MC_PlayerBD[sGangChaseUnit.iTargetPart].vGangChaseSpawnCoords, vSpawnPedCoords), fSpawnHeading, sGangChaseUnit.iGangType, cfGANG_CHASE_VEHICLE_HEADING_TOLERANCE, g_FMMC_STRUCT.fGangChaseUnitMaxDistance)
				fSpawnHeading = GET_HEADING_BETWEEN_VECTORS_2D(MC_PlayerBD[sGangChaseUnit.iTargetPart].vGangChaseSpawnCoords, vSpawnPedCoords)
				PRINTLN("[GANG_CHASE] CREATE_GANG_CHASE_UNIT_VEHICLE - Unit: ", iGangChaseUnit, " Tried spawning forward, no suitable road node. Using normal heading: ", fSpawnHeading)
			ENDIF
		ENDIF
		CLEAR_BIT(MC_serverBD_1.sGangChase[iGangChaseUnit].iBitset, SBBBOOL_SPAWNING_FORWARDS)
	ENDIF
	
	IF NOT sGangChaseUnit.bTriedForwards
		fSpawnHeading = GET_HEADING_BETWEEN_VECTORS_2D(MC_PlayerBD[sGangChaseUnit.iTargetPart].vGangChaseSpawnCoords, vSpawnPedCoords)
		CLEAR_BIT(MC_serverBD_1.sGangChase[iGangChaseUnit].iBitset, SBBBOOL_SPAWNING_FORWARDS)
		PRINTLN("[GANG_CHASE] CREATE_GANG_CHASE_UNIT_VEHICLE - Unit: ", iGangChaseUnit, " Spawning backwards with heading: ", fSpawnHeading)
	ENDIF
	
	IF NOT FMMC_CREATE_NET_VEHICLE(MC_serverBD_1.sGangChase[iGangChaseUnit].niVeh, sGangChaseUnit.mnVehicleModel, sGangChaseUnit.vPosition, fSpawnHeading)
		PRINTLN("[GANG_CHASE] CREATE_GANG_CHASE_UNIT_VEHICLE - Unit: ", iGangChaseUnit, " Failed to create vehicle!")
		RETURN FALSE
	ENDIF
	
	VEHICLE_INDEX viVehicle = NET_TO_VEH(MC_serverBD_1.sGangChase[iGangChaseUnit].niVeh)
	sGangChaseUnit.viVehicle = viVehicle
		
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[sGangChaseUnit.iTeam].iRuleBitsetEleven[sGangChaseUnit.iRule], ciBS_RULE11_GANG_CHASE_DESPAWN_ON_NEW_GANG_CHASE_TYPE)
		SET_BIT(MC_serverBD_1.sGangChase[iGangChaseUnit].iBitset, SBBBOOL_DESPAWN_ON_NEW_TYPE)
		PRINTLN("[GANG_CHASE] CREATE_GANG_CHASE_UNIT_VEHICLE - Unit: ", iGangChaseUnit, " Will despawn if a gang chase of a different type starts. Current Type: ", sGangChaseUnit.iGangType)
	ENDIF	
		
	SET_UP_GANG_CHASE_VEHICLE(iGangChaseUnit, sGangChaseUnit)
	
	PRINTLN("[GANG_CHASE] CREATE_GANG_CHASE_UNIT_VEHICLE - Unit: ", iGangChaseUnit, " Vehicle created at: ", sGangChaseUnit.vPosition, " vehicle model = ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(sGangChaseUnit.mnVehicleModel))
	
	RETURN FALSE //Return false on the frame the vehicle is created to avoid making multiple entities per frame
	
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Gang Chase Ped Spawning
// ##### Description: Functions dealing with creating gang chase peds
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC FLOAT GET_GANG_CHASE_UNIT_DIFFICULTY_MODIFIER()

	FLOAT fDifficultyMod = TO_FLOAT(GET_TOTAL_STARTING_PLAYERS()) / TO_FLOAT(g_FMMC_STRUCT.iNumParticipants)
	
	IF MC_serverBD.iDifficulty = DIFF_EASY
		fDifficultyMod -= 1.0
	ELIF MC_serverBD.iDifficulty = DIFF_HARD
		fDifficultyMod += 1.0
	ENDIF
	
	RETURN fDifficultyMod
	
ENDFUNC

FUNC COMBAT_ABILITY_LEVEL GET_GANG_CHASE_UNIT_COMBAT_ABILITY(INT iBaseAccuracy)
	
	IF iBaseAccuracy < ciGANG_CHASE_PED_ACCURACY_NORMAL
		RETURN CAL_POOR
	ELIF iBaseAccuracy < ciGANG_CHASE_PED_ACCURACY_HARD
		RETURN CAL_AVERAGE
	ENDIF
	
	RETURN CAL_PROFESSIONAL
	
ENDFUNC

FUNC INT GET_GANG_CHASE_UNIT_BASE_ACCURACY(INT iTeam)
	
	INT iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]
	INT iGangAccuracy = RETURN_INT_CUSTOM_VARIABLE_VALUE(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iGangAccuracy[iRule])
	
	IF iGangAccuracy = FMMC_GANG_CHASE_PED_ACCURACY_DEFAULT 
		
		SWITCH MC_serverBD.iDifficulty
		
			CASE DIFF_EASY
				RETURN ciGANG_CHASE_PED_ACCURACY_EASY
			BREAK
			
			CASE DIFF_NORMAL
				RETURN ciGANG_CHASE_PED_ACCURACY_NORMAL
			BREAK
			
			CASE DIFF_HARD
				RETURN ciGANG_CHASE_PED_ACCURACY_HARD
			BREAK
			
		ENDSWITCH
		
	ENDIF
	
	RETURN iGangAccuracy

ENDFUNC

PROC PROCESS_GANG_CHASE_UNIT_HOMING_WEAPON_RESTRICTIONS(INT iGangChaseUnit, GANG_CHASE_UNIT_CONFIG_DATA &sGangChaseUnit, PED_INDEX piPed)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[sGangChaseUnit.iTeam].iRuleBitsetTen[sGangChaseUnit.iRule], ciBS_RULE10_GANG_CHASE_NO_HOMING_WEAPONS)
		EXIT
	ENDIF
	
	IF sGangChaseUnit.mnVehicleModel != BUZZARD
	AND sGangChaseUnit.mnVehicleModel != BUZZARD2
		EXIT
	ENDIF
	
	IF GET_PED_IN_VEHICLE_SEAT(sGangChaseUnit.viVehicle, VS_DRIVER) != piPed
		EXIT
	ENDIF
	
	PRINTLN("[GANG_CHASE] PROCESS_HOMING_WEAPON_RESTRICTIONS - Unit: ", iGangChaseUnit, " Disabling homing rockets.")
				
	DISABLE_VEHICLE_WEAPON(TRUE, WEAPONTYPE_VEHICLE_SPACE_ROCKET, sGangChaseUnit.viVehicle, piPed)
	SET_CURRENT_PED_VEHICLE_WEAPON(piPed, WEAPONTYPE_VEHICLE_PLAYER_BUZZARD)
	SET_PED_CAN_SWITCH_WEAPON(piPed, FALSE)

	SET_PED_FIRING_PATTERN(piPed, FIRING_PATTERN_BURST_FIRE_HELI)
	SET_PED_COMBAT_ATTRIBUTES(piPed, CA_FORCE_CHECK_ATTACK_ANGLE_FOR_MOUNTED_GUNS, TRUE)
	
ENDPROC

PROC SET_UP_GANG_CHASE_PED_TASK(INT iGangChaseUnit, GANG_CHASE_UNIT_CONFIG_DATA &sGangChaseUnit, PED_INDEX piPed)
		
	IF NOT (IS_GANG_CHASE_AIR(sGangChaseUnit.iGangType) OR IS_GANG_CHASE_SEA(sGangChaseUnit.iGangType))
	OR NOT (DOES_ENTITY_EXIST(sGangChaseUnit.viVehicle) AND GET_PED_IN_VEHICLE_SEAT(sGangChaseUnit.viVehicle, VS_DRIVER) = piPed)
		
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(piPed, FALSE)
		
		IF MC_serverBD_1.iGangChasePedTargetOverride != -1
			PED_INDEX piTarget = GET_GANG_CHASE_TARGET_PED_INDEX(iGangChaseUnit)
			IF NOT IS_PED_INJURED(piTarget)
				PRINTLN("[GANG_CHASE] SET_UP_GANG_CHASE_PED_TASK - Unit: ", iGangChaseUnit, " Tasking ped to attack target override: ", MC_serverBD_1.iGangChasePedTargetOverride)
				TASK_COMBAT_PED(piPed, piTarget, COMBAT_PED_PREVENT_CHANGING_TARGET | MC_GET_TASK_COMBAT_PED_FLAGS(piPed))
				SET_BIT(MC_serverBD_1.sGangChase[iGangChaseUnit].iBitset, SBBBOOL_ATTACKING_PED_TARGET)
				EXIT
			ENDIF
		ENDIF
		
		PRINTLN("[GANG_CHASE] SET_UP_GANG_CHASE_PED_TASK - Unit: ", iGangChaseUnit, " Tasking ped with regular combat.")
		TASK_COMBAT_HATED_TARGETS_AROUND_PED(piPed, cfGANG_CHASE_PED_RANGE, MC_GET_TASK_COMBAT_PED_FLAGS(piPed))
		
		EXIT
		
	ENDIF
	
	PRINTLN("[GANG_CHASE] SET_UP_GANG_CHASE_PED_TASK - Unit: ", iGangChaseUnit, " Tasking ped (driver) with air/sea goto target.")
		
	VEHICLE_INDEX viTargetVeh = NULL
	VECTOR vTargetCoords = GET_ENTITY_COORDS(sGangChaseUnit.piTargetPed, FALSE)
	
	IF sGangChaseUnit.iTargetPart > ciGANG_CHASE_TARGET_NONE
		IF IS_PED_IN_ANY_VEHICLE(sGangChaseUnit.piTargetPed)
			viTargetVeh = GET_VEHICLE_PED_IS_IN(sGangChaseUnit.piTargetPed)
		ENDIF
	ENDIF
	
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(piPed, TRUE)
	
	LOAD_COLLISION_ON_GANG_CHASE_PED_IF_POSSIBLE(iGangChaseUnit, piPed)
	
	IF NOT DOES_ENTITY_EXIST(sGangChaseUnit.viVehicle)
	OR NOT IS_VEHICLE_DRIVEABLE(sGangChaseUnit.viVehicle)		
		PRINTLN("[GANG_CHASE] SET_UP_GANG_CHASE_PED_TASK - Unit: ", iGangChaseUnit, " ped (driver) Heli does not exist or is not driveable.")
		EXIT
	ENDIF
	
	FLOAT fCruiseSpeed = FMAX(25.0, GET_VEHICLE_ESTIMATED_MAX_SPEED(sGangChaseUnit.viVehicle))
	
	IF IS_GANG_CHASE_AIR(sGangChaseUnit.iGangType)
		IF IS_THIS_MODEL_A_HELI(sGangChaseUnit.mnVehicleModel)
			TASK_HELI_MISSION(piPed, sGangChaseUnit.viVehicle, viTargetVeh, sGangChaseUnit.piTargetPed, vTargetCoords, MISSION_ATTACK, fCruiseSpeed, 5, -1, 0, 10, DEFAULT, HF_MaintainHeightAboveTerrain)
		ELSE
			TASK_PLANE_MISSION(piPed, sGangChaseUnit.viVehicle, viTargetVeh, sGangChaseUnit.piTargetPed, vTargetCoords, MISSION_ATTACK, fCruiseSpeed, 5, -1, 0, 10, DEFAULT)
		ENDIF
	ELSE
		TASK_BOAT_MISSION(piPed, sGangChaseUnit.viVehicle, viTargetVeh, sGangChaseUnit.piTargetPed, vTargetCoords, MISSION_ATTACK, fCruiseSpeed, DRIVINGMODE_AVOIDCARS, 5, BCF_OPENOCEANSETTINGS)
	ENDIF
	
	SET_BIT(MC_serverBD_1.sGangChase[iGangChaseUnit].iBitset, SBBBOOL_ON_GOTO_TASK)
	
ENDPROC

PROC SET_UP_GANG_CHASE_PED(INT iGangChaseUnit, INT iPed, GANG_CHASE_UNIT_CONFIG_DATA &sGangChaseUnit, PED_INDEX piPed, BOOL bOnFoot)
	
	INT iPedDecorInt = GET_GANG_CHASE_PED_ID(iGangChaseUnit, iPed)
	FLOAT fDifficultyMod = GET_GANG_CHASE_UNIT_DIFFICULTY_MODIFIER()
	
	AI_BLIP_STRUCT emptyAIBlipStruct
	biGangChasePedBlip[iPedDecorInt] = emptyAIBlipStruct
		
	#IF IS_DEBUG_BUILD
		TEXT_LABEL_31 tl31DebugName = "Gang["
		tl31DebugName += iGangChaseUnit
		tl31DebugName += "]["
		tl31DebugName += iPed
		tl31DebugName += "]"
		SET_PED_NAME_DEBUG(piPed, tl31DebugName)
	#ENDIF
	
	GIVE_DELAYED_WEAPON_TO_PED(piPed, sGangChaseUnit.wtWeapon, ciGANG_CHASE_PED_AMMO, TRUE)
	SET_CURRENT_PED_WEAPON(piPed, sGangChaseUnit.wtWeapon, TRUE)
		
	IF NETWORK_IS_GAME_IN_PROGRESS()
		INT iTeamLoop
		FOR iTeamLoop = 0 TO FMMC_MAX_TEAMS - 1
			SET_PED_CAN_BE_TARGETTED_BY_TEAM(piPed, iTeamLoop, TRUE)
		ENDFOR
	ENDIF
	
	SET_ENTITY_IS_TARGET_PRIORITY(piPed, TRUE)
	
	SET_PED_RELATIONSHIP_GROUP_HASH(piPed, rgFM_AiHatePlyrLikeAllAI)
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[sGangChaseUnit.iTeam].iRuleBitsetEight[sGangChaseUnit.iRule], ciBS_RULE8_GANG_CHASE_GET_COP_BLIPS)
		IF sGangChaseUnit.mnPedModel != MP_M_FIBSEC_01
			SET_PED_CONFIG_FLAG(piPed, PCF_DontBlip, FALSE)
			SET_PED_CONFIG_FLAG(piPed, PCF_DontBlipCop, FALSE)
			SET_PED_AS_COP(piPed, FALSE)
			SET_PED_CONFIG_FLAG(piPed, PCF_CanAttackNonWantedPlayerAsLaw, TRUE)
		ENDIF
		
		SET_BIT(MC_serverBD_1.sGangChase[iGangChaseUnit].iBitset, SBBBOOL_USES_COP_BLIPS)
		PRINTLN("[GANG_CHASE] SET_UP_GANG_CHASE_PED - Unit: ", iGangChaseUnit, " Ped (", iPed, ") is using cop blips")
	ELIF MC_IS_MODEL_AMBIENT_MISSION_COP(sGangChaseUnit.mnPedModel)
	OR sGangChaseUnit.mnPedModel = MP_M_FIBSEC_01
		SET_PED_CONFIG_FLAG(piPed, PCF_CanAttackNonWantedPlayerAsLaw, TRUE)		
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[sGangChaseUnit.iTeam].iRuleBitsetFourteen[sGangChaseUnit.iRule], ciBS_RULE14_GANG_CHASE_DISABLE_LAW_AND_COP_BEHAVIOURS)	
		PRINTLN("[GANG_CHASE] SET_UP_GANG_CHASE_PED - Unit: ", iGangChaseUnit, " Ped (", iPed, ") Forcing off Cop and Law behaviours")
		SET_PED_CONFIG_FLAG(piPed, PCF_DontInfluenceWantedLevel, TRUE) // Blocks the Death Event from triggering a SetWantedLevel in Code.
		SET_PED_CONFIG_FLAG(piPed, PCF_DontBehaveLikeLaw, TRUE)		  // Blocks Code from calling SetWantedLevel based purely on sight of the player comitting a crime.	
	ENDIF
	
	IF sGangChaseUnit.mnPedModel = MP_M_FIBSEC_01
		SET_PED_CONFIG_FLAG(piPed, PCF_DontBlip, FALSE)
		SET_PED_CONFIG_FLAG(piPed, PCF_DontBlipCop, FALSE)
		SET_PED_AS_COP(piPed, FALSE)
		SET_PED_TARGET_LOSS_RESPONSE(piPed, TLR_SEARCH_FOR_TARGET)
		SET_PED_CONFIG_FLAG(piPed, PCF_CanAttackNonWantedPlayerAsLaw, TRUE)
		SET_PED_CONFIG_FLAG(piPed, PCF_OnlyUpdateTargetWantedIfSeen, TRUE)
	ELSE
		SET_PED_TARGET_LOSS_RESPONSE(piPed, TLR_NEVER_LOSE_TARGET)
	ENDIF
	
	SET_PED_COMBAT_MOVEMENT(piPed, CM_WILLADVANCE)
	SET_PED_COMBAT_ATTRIBUTES(piPed, CA_CAN_USE_FRUSTRATED_ADVANCE, TRUE)
	SET_PED_COMBAT_ATTRIBUTES(piPed, CA_CAN_CHARGE, TRUE)
	
	SET_PED_COMBAT_ATTRIBUTES(piPed, CA_DISABLE_ALL_RANDOMS_FLEE, TRUE)
	
	SET_PED_COMBAT_ATTRIBUTES(piPed, CA_DISABLE_BLOCK_FROM_PURSUE_DURING_VEHICLE_CHASE, sGangChaseUnit.bSpawnBehind)
	SET_PED_COMBAT_ATTRIBUTES(piPed, CA_DISABLE_CRUISE_IN_FRONT_DURING_BLOCK_DURING_VEHICLE_CHASE, sGangChaseUnit.bSpawnBehind)

	SET_PED_COMBAT_ATTRIBUTES(piPed, CA_USE_VEHICLE, TRUE)
	SET_PED_COMBAT_ATTRIBUTES(piPed, CA_USE_VEHICLE_ATTACK_IF_VEHICLE_HAS_MOUNTED_GUNS, TRUE)
	SET_PED_COMBAT_ATTRIBUTES(piPed, CA_DO_DRIVEBYS, TRUE)
	SET_PED_COMBAT_ATTRIBUTES(piPed, CA_DISABLE_SEEK_DUE_TO_LINE_OF_SIGHT, FALSE)
	
	SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(piPed, KNOCKOFFVEHICLE_HARD)

	SET_PED_FLEE_ATTRIBUTES(piPed, FA_USE_VEHICLE, TRUE)
	SET_PED_FLEE_ATTRIBUTES(piPed, FA_DISABLE_COWER, TRUE)
	SET_PED_FLEE_ATTRIBUTES(piPed, FA_COWER_INSTEAD_OF_FLEE, FALSE)
	SET_PED_FLEE_ATTRIBUTES(piPed, FA_DISABLE_FLEE_FROM_INDIRECT_THREATS, FALSE)	
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[sGangChaseUnit.iTeam].iRuleBitsetNine[sGangChaseUnit.iRule], ciBS_RULE9_GANG_CHASE_PREVENT_COMMANDEERING_VEHICLES)
		SET_PED_COMBAT_ATTRIBUTES(piPed, CA_CAN_COMMANDEER_VEHICLES, FALSE)
		SET_PED_COMBAT_ATTRIBUTES(piPed, CA_USE_VEHICLE_ATTACK_IF_VEHICLE_HAS_MOUNTED_GUNS, FALSE)
		SET_PED_CONFIG_FLAG(piPed, PCF_PedsJackingMeDontGetIn, TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[sGangChaseUnit.iTeam].iRuleBitsetFifteen[sGangChaseUnit.iRule], ciBS_RULE15_GANG_CHASE_PREVENT_JACKING)
		SET_PED_CONFIG_FLAG(piPed, PCF_NotAllowedToJackAnyPlayers, TRUE)
	ENDIF
	
	SET_PED_CONFIG_FLAG(piPed, PCF_AICanDrivePlayerAsRearPassenger, TRUE)
	SET_PED_CONFIG_FLAG(piPed, PCF_KeepTargetLossResponseOnCleanup, TRUE)
	SET_PED_CONFIG_FLAG(piPed, PCF_KeepRelationshipGroupAfterCleanUp, TRUE)
	
	FLOAT fArmour = ciGANG_CHASE_PED_BASE_HEALTH * sGangChaseUnit.fPedArmour
	INT iHealth = ROUND((ciGANG_CHASE_PED_BASE_HEALTH + (fGangChasePedHealthMod * fDifficultyMod * (ciGANG_CHASE_PED_BASE_HEALTH - ciGANG_CHASE_PED_MIN_HEALTH)) * g_sMPTunables.fAiHealthModifier) + fArmour)
	iHealth = CLAMP_INT(iHealth, ciGANG_CHASE_PED_MIN_HEALTH, HIGHEST_INT)
	PRINTLN("[GANG_CHASE] SET_UP_GANG_CHASE_PED - Unit: ", iGangChaseUnit, " Ped: ", iPed, " Health: ", iHealth)
	SET_ENTITY_HEALTH(piPed, iHealth)
	
	INT iBaseAccuracy = GET_GANG_CHASE_UNIT_BASE_ACCURACY(sGangChaseUnit.iTeam)
	COMBAT_ABILITY_LEVEL eCombatAbility = GET_GANG_CHASE_UNIT_COMBAT_ABILITY(iBaseAccuracy)
	
	PRINTLN("[GANG_CHASE] SET_UP_GANG_CHASE_PED - Unit: ", iGangChaseUnit, " Ped: ", iPed, " Combat Ability: ", eCombatAbility)
	PRINTLN("[GANG_CHASE] SET_UP_GANG_CHASE_PED - Unit: ", iGangChaseUnit, " Ped: ", iPed, " Base Accuracy: ", iBaseAccuracy)
	
	// Peds that need Variations set on them.	
	IF DOES_PED_USE_RANDOM_VARIATIONS(sGangChaseUnit.mnPedModel) 
		PRINTLN("[GANG_CHASE] SET_UP_GANG_CHASE_PED - Unit: ", iGangChaseUnit, " Ped: ", iPed, " DOES_PED_USE_RANDOM_VARIATIONS TRUE")
		INT iMaxVariation = GET_NUMBER_OF_PED_VARIATIONS(sGangChaseUnit.mnPedModel)
		INT iVariation = GET_RANDOM_INT_IN_RANGE(0, iMaxVariation+1)
		FMMC_SET_PED_VARIATION(piPed, sGangChaseUnit.mnPedModel, iVariation)	
	ENDIF
	
	IF sGangChaseUnit.bUseAlternateComponentsForPedOutfit
		IF sGangChaseUnit.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("G_M_M_CartelGuards_01"))
			PRINTLN("[GANG_CHASE] SET_UP_GANG_CHASE_PED - Unit: ", iGangChaseUnit, "Ped: ", iPed, " Overriding the components of G_M_M_CartelGuards_01. Removing the Body Armour.")
			SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_SPECIAL2, 1, 0, 0)	
		ENDIF
		
		IF sGangChaseUnit.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("G_M_M_CartelGuards_02"))
			PRINTLN("[GANG_CHASE] SET_UP_GANG_CHASE_PED - Unit: ", iGangChaseUnit, "Ped: ", iPed, " Overriding the components of G_M_M_CartelGuards_02. Removing the Body Armour.")
			SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_SPECIAL2, 1, 0, 0)	
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[sGangChaseUnit.iTeam].iRuleBitsetFourteen[sGangChaseUnit.iRule], ciBS_RULE14_BLOCK_GANG_CHASE_PEDS_ABILITY_TO_CLIMB_LADDERS)
		PRINTLN("[GANG_CHASE] CREATE_GANG_CHASE_UNIT_VEHICLE - Unit: ", iGangChaseUnit, " Ped: ", iPed, " Disabling Ability to climb ladders.")
		SET_PED_CONFIG_FLAG(piPed, PCF_DisableLadderClimbing, TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[sGangChaseUnit.iTeam].iRuleBitsetEleven[sGangChaseUnit.iRule], ciBS_RULE11_USE_GANG_CHASE_BURST_FIRE_MODE)
		PRINTLN("[GANG_CHASE] CREATE_GANG_CHASE_UNIT_VEHICLE - Unit: ", iGangChaseUnit, " Ped: ", iPed, " Burst Fire Enabled")
		SET_PED_FIRING_PATTERN(piPed, FIRING_PATTERN_BURST_FIRE)
	ELIF eCombatAbility = CAL_POOR
		SET_PED_FIRING_PATTERN(piPed, FIRING_PATTERN_SHORT_BURSTS)
	ENDIF
	
	INT iAccuracy = CLAMP_INT(ROUND(iBaseAccuracy + (fGangChasePedAccuracyMod * iBaseAccuracy * fDifficultyMod)), 0, 100)
	
	PRINTLN("[GANG_CHASE] SET_UP_GANG_CHASE_PED - Unit: ", iGangChaseUnit, " Ped: ", iPed, " Scaled Accuracy: ", iAccuracy)
	SET_PED_COMBAT_ABILITY(piPed, eCombatAbility)
	SET_PED_ACCURACY(piPed, iAccuracy)
	
	IF sGangChaseUnit.mnPedModel = MP_G_M_PROS_01
		SET_PED_COMPONENT_VARIATION(piPed, PED_COMP_SPECIAL2, 0, 0)
	ENDIF
	
	SET_PED_DIES_WHEN_INJURED(piPed, TRUE)
	SET_PED_KEEP_TASK(piPed, TRUE)
	
	IF IS_GANG_CHASE_SEA(sGangChaseUnit.iGangType)
		SET_PED_DIES_IN_WATER(piPed, FALSE)
		SET_PED_DIES_INSTANTLY_IN_WATER(piPed, FALSE)
	ENDIF
	
	SET_PED_TO_INFORM_RESPECTED_FRIENDS(piPed, cfGANG_CHASE_PED_RANGE, 40)
	SET_PED_COMBAT_RANGE(piPed, CR_FAR)
	SET_PED_SEEING_RANGE(piPed, cfGANG_CHASE_PED_RANGE)
	
	IF sGangChaseUnit.bSpawnForwards
		SET_PED_COMBAT_ATTRIBUTES(piPed, CA_CAN_CRUISE_AND_BLOCK_IN_VEHICLE, TRUE)
		SET_COMBAT_FLOAT(piPed, CCF_AUTOMOBILE_SPEED_MODIFIER, 2.0)
		SET_DRIVER_AGGRESSIVENESS(piPed, 1.0)
		PRINTLN("[GANG_CHASE] SET_UP_GANG_CHASE_PED - Unit: ", iGangChaseUnit, " Ped: ", iPed, " Spawning Forwards.")
	ENDIF
	
	IF sGangChaseUnit.bSpawnBehind
		SET_PED_COMBAT_ATTRIBUTES(piPed, CA_CAN_CRUISE_AND_BLOCK_IN_VEHICLE, FALSE)
		SET_PED_COMBAT_ATTRIBUTES(piPed, CA_DISABLE_PULL_ALONGSIDE_DURING_VEHICLE_CHASE, TRUE)
	ENDIF
	
	MC_SET_ID_INT_DECOR_ON_ENTITY(GET_ENTITY_FROM_PED_OR_VEHICLE(piPed), iPedDecorInt, TRUE)
	
	SET_UP_GANG_CHASE_PED_TASK(iGangChaseUnit, sGangChaseUnit, piPed)
	
	IF !bOnFoot
	
		PROCESS_GANG_CHASE_UNIT_HOMING_WEAPON_RESTRICTIONS(iGangChaseUnit, sGangChaseUnit, piPed)
		
		IF IS_THIS_MODEL_A_BIKE(sGangChaseUnit.mnVehicleModel)
			SET_PED_LOD_MULTIPLIER(piPed, 2.0)
		ENDIF
		
		IF IS_GANG_CHASE_AIR(sGangChaseUnit.iGangType)
			SET_PED_COMBAT_ATTRIBUTES(piPed, CA_LEAVE_VEHICLES, FALSE)
		ENDIF
		
		IF IS_BIT_SET(MC_serverBD_1.sGangChase[iGangChaseUnit].iBitset, SBBBOOL_AIR_VEHICLE_IS_FROZEN)
			PRINTLN("[GANG_CHASE] SET_UP_GANG_CHASE_PED - Unit: ", iGangChaseUnit, " Ped (", iPed, ") is in the heli - un-freezing!")
			FREEZE_ENTITY_POSITION(sGangChaseUnit.viVehicle, FALSE)
			CLEAR_BIT(MC_serverBD_1.sGangChase[iGangChaseUnit].iBitset, SBBBOOL_AIR_VEHICLE_IS_FROZEN)
		ENDIF
		
	ENDIF
	
	MC_serverBD_1.sGangChase[iGangChaseUnit].mnPedModelNames[iPed] = sGangChaseUnit.mnPedModel

ENDPROC

FUNC BOOL CREATE_GANG_CHASE_UNIT_PED_ON_FOOT(INT iGangChaseUnit, INT iPed, GANG_CHASE_UNIT_CONFIG_DATA &sGangChaseUnit)
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sGangChase[iGangChaseUnit].niPeds[iPed])
		RETURN TRUE
	ENDIF
	
	VECTOR vPedPos = sGangChaseUnit.vPosition
	vPedPos.x += GET_RANDOM_FLOAT_IN_RANGE(-2.5, 2.5)
	vPedPos.y += GET_RANDOM_FLOAT_IN_RANGE(-2.5, 2.5)
	FLOAT fGroundZ
	IF GET_GROUND_Z_FOR_3D_COORD(vPedPos, fGroundZ)
		vPedPos.z = fGroundZ
	ENDIF
	
	FLOAT fPedHeading = GET_HEADING_BETWEEN_VECTORS_2D(vPedPos, GET_ENTITY_COORDS(sGangChaseUnit.piTargetPed, FALSE))
	
	IF NOT FMMC_CREATE_NET_PED(MC_serverBD_1.sGangChase[iGangChaseUnit].niPeds[iPed], PEDTYPE_MISSION, sGangChaseUnit.mnPedModel, vPedPos, fPedHeading)
		RETURN FALSE
	ENDIF
		
	PED_INDEX piPed = NET_TO_PED(MC_serverBD_1.sGangChase[iGangChaseUnit].niPeds[iPed])
		
	SET_UP_GANG_CHASE_PED(iGangChaseUnit, iPed, sGangChaseUnit, piPed, TRUE)
	
	MC_serverBD.iNumPedsSpawned++
		
	PRINTLN("[GANG_CHASE] CREATE_GANG_CHASE_UNIT_PED_ON_FOOT - Unit: ", iGangChaseUnit, " Ped (", iPed, ") created on foot!")

	RETURN FALSE  //Return false on the frame the ped is created to avoid making multiple entities per frame
	
ENDFUNC

FUNC VEHICLE_SEAT GET_GANG_CHASE_PED_VEHICLE_SEAT(VEHICLE_INDEX viVehicle)

	IF IS_ENTITY_DEAD(viVehicle)
		RETURN VS_ANY_PASSENGER
	ENDIF
	
	IF IS_VEHICLE_SEAT_FREE(viVehicle, VS_DRIVER)
		RETURN VS_DRIVER
	ENDIF
	
	INT iMaxSeats = GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(viVehicle)
	
	INT i
	VEHICLE_SEAT eFirstFreeSeat = VS_ANY_PASSENGER
	FOR i = 0 TO iMaxSeats - 1
		VEHICLE_SEAT eSeat = INT_TO_ENUM(VEHICLE_SEAT, i)
		IF IS_VEHICLE_SEAT_FREE(viVehicle, eSeat)
			IF IS_TURRET_SEAT(viVehicle, eSeat)
				RETURN eSeat
			ENDIF
			IF eFirstFreeSeat = VS_ANY_PASSENGER
				eFirstFreeSeat = eSeat
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN eFirstFreeSeat
	
ENDFUNC

FUNC BOOL CREATE_GANG_CHASE_UNIT_PED_IN_VEHICLE(INT iGangChaseUnit, INT iPed, GANG_CHASE_UNIT_CONFIG_DATA &sGangChaseUnit)

	IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sGangChase[iGangChaseUnit].niPeds[iPed])
		RETURN TRUE
	ENDIF

	IF NOT NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sGangChase[iGangChaseUnit].niVeh)
		PRINTLN("[GANG_CHASE] CREATE_GANG_CHASE_UNIT_PED_IN_VEHICLE - Unit: ", iGangChaseUnit, " Requesting control to create ped!")
		NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sGangChase[iGangChaseUnit].niVeh)
		RETURN FALSE
	ENDIF
	
	VEHICLE_SEAT eVehSeat = GET_GANG_CHASE_PED_VEHICLE_SEAT(sGangChaseUnit.viVehicle)
	
	IF NOT FMMC_CREATE_NET_PED_IN_VEHICLE(MC_serverBD_1.sGangChase[iGangChaseUnit].niPeds[iPed], MC_serverBD_1.sGangChase[iGangChaseUnit].niVeh, PEDTYPE_MISSION, sGangChaseUnit.mnPedModel, eVehSeat)
		RETURN FALSE
	ENDIF
	
	PED_INDEX piPed = NET_TO_PED(MC_serverBD_1.sGangChase[iGangChaseUnit].niPeds[iPed])
	
	SET_UP_GANG_CHASE_PED(iGangChaseUnit, iPed, sGangChaseUnit, piPed, FALSE)
	
	MC_serverBD.iNumPedsSpawned++
	
	PRINTLN("[GANG_CHASE] CREATE_GANG_CHASE_UNIT_PED_IN_VEHICLE - Unit: ", iGangChaseUnit, " Ped (", iPed, ") created in vehicle seat: ", eVehSeat)
	
	RETURN FALSE //Return false on the frame the ped is created to avoid making multiple entities per frame
	
ENDFUNC

FUNC BOOL CREATE_GANG_CHASE_UNIT_PED(INT iGangChaseUnit, INT iPed, GANG_CHASE_UNIT_CONFIG_DATA &sGangChaseUnit)
	
	IF GET_CUSTOM_GANG_CHASE_TYPE(sGangChaseUnit.iGangType) = FMMC_GANG_CHASE_TYPE_ON_FOOT
	OR (NOT IS_VEHICLE_DRIVEABLE(sGangChaseUnit.viVehicle) OR NOT ARE_ANY_VEHICLE_SEATS_FREE(sGangChaseUnit.viVehicle))
		RETURN CREATE_GANG_CHASE_UNIT_PED_ON_FOOT(iGangChaseUnit, iPed, sGangChaseUnit)
	ENDIF
	
	RETURN CREATE_GANG_CHASE_UNIT_PED_IN_VEHICLE(iGangChaseUnit, iPed, sGangChaseUnit)

ENDFUNC

FUNC BOOL CREATE_GANG_CHASE_UNIT_PEDS(INT iGangChaseUnit, GANG_CHASE_UNIT_CONFIG_DATA &sGangChaseUnit)

	sGangChaseUnit.bSpawnForwards = FALSE
	sGangChaseUnit.bTriedForwards = IS_BIT_SET(MC_PlayerBD[sGangChaseUnit.iTargetPart].iGangChaseSpawnBitset, PBGANGSPAWN_UNIT_0_TRY_FORWARDS + iGangChaseUnit)

	IF g_FMMC_STRUCT.sFMMCEndConditions[sGangChaseUnit.iTeam].iGangChaseForwardSpawnChance[sGangChaseUnit.iRule] > 0
	AND IS_BIT_SET(MC_serverBD_1.sGangChase[iGangChaseUnit].iBitset, SBBBOOL_SPAWNING_FORWARDS)
		sGangChaseUnit.bSpawnForwards = TRUE
	ENDIF
	
	sGangChaseUnit.bSpawnBehind = IS_BIT_SET(MC_PlayerBD[sGangChaseUnit.iTargetPart].iGangChaseSpawnBitset, PBGANGSPAWN_UNIT_0_SPAWN_BEHIND + iGangChaseUnit)
	
	INT iPed
	FOR iPed = 0 TO sGangChaseUnit.iMaxPeds - 1
		IF NOT CREATE_GANG_CHASE_UNIT_PED(iGangChaseUnit, iPed, sGangChaseUnit)
			RETURN FALSE //Only attempt to spawn one ped per frame
		ENDIF
	ENDFOR
	
	RETURN TRUE
	
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Gang Chase Unit Creation
// ##### Description: Functions dealing with creating gang chase units
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC CLEANUP_GANG_CHASE_UNIT_MODELS(GANG_CHASE_UNIT_CONFIG_DATA &sGangChaseUnit)

	IF GET_CUSTOM_GANG_CHASE_TYPE(sGangChaseUnit.iGangType) != FMMC_GANG_CHASE_TYPE_ON_FOOT
		SET_MODEL_AS_NO_LONGER_NEEDED(sGangChaseUnit.mnVehicleModel)
	ENDIF
	
	SET_MODEL_AS_NO_LONGER_NEEDED(sGangChaseUnit.mnPedModel)
	
ENDPROC

FUNC BOOL HAVE_GANG_CHASE_UNIT_ASSETS_LOADED(GANG_CHASE_UNIT_CONFIG_DATA &sGangChaseUnit)
	
	IF GET_CUSTOM_GANG_CHASE_TYPE(sGangChaseUnit.iGangType) != FMMC_GANG_CHASE_TYPE_ON_FOOT
		IF NOT REQUEST_LOAD_MODEL(sGangChaseUnit.mnVehicleModel)
			PRINTLN("[GANG_CHASE] HAVE_GANG_CHASE_UNIT_ASSETS_LOADED - Waiting to load vehicle model.")
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF NOT REQUEST_LOAD_MODEL(sGangChaseUnit.mnPedModel)
		PRINTLN("[GANG_CHASE] HAVE_GANG_CHASE_UNIT_ASSETS_LOADED - Waiting to load ped model.")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL RESERVE_GANG_CHASE_UNIT_ENTITIES(INT iGangChaseUnit, GANG_CHASE_UNIT_CONFIG_DATA &sGangChaseUnit)
	
	IF NOT IS_BIT_SET(MC_serverBD_1.sGangChase[iGangChaseUnit].iBitset, SBBBOOL_RESERVATION_COMPLETE)
		IF GET_CUSTOM_GANG_CHASE_TYPE(sGangChaseUnit.iGangType) != FMMC_GANG_CHASE_TYPE_ON_FOOT
			IF NOT CAN_RESERVE_NETWORK_VEHICLES_FOR_THIS_SCRIPT(GET_NUM_RESERVED_MISSION_VEHICLES() + 1, TRUE, TRUE)
				PRINTLN("[GANG_CHASE] RESERVE_GANG_CHASE_UNIT_ENTITIES - Unit: ", iGangChaseUnit, " Cannot reserve network vehicle.")
				RETURN FALSE
			ENDIF
		ENDIF
		
		IF NOT CAN_RESERVE_NETWORK_PEDS_FOR_THIS_SCRIPT(GET_NUM_RESERVED_MISSION_PEDS() + sGangChaseUnit.iMaxPeds, TRUE, TRUE)
			PRINTLN("[GANG_CHASE] RESERVE_GANG_CHASE_UNIT_ENTITIES - Unit: ", iGangChaseUnit, " Cannot reserve network peds.")
			RETURN FALSE
		ENDIF
		
		MC_serverBD_1.sGangChase[iGangChaseUnit].iMaxPeds = sGangChaseUnit.iMaxPeds
		
		IF GET_CUSTOM_GANG_CHASE_TYPE(sGangChaseUnit.iGangType) != FMMC_GANG_CHASE_TYPE_ON_FOOT
			RESERVE_NETWORK_MISSION_VEHICLES(GET_NUM_RESERVED_MISSION_VEHICLES() + 1)
		ENDIF
		
		RESERVE_NETWORK_MISSION_PEDS(GET_NUM_RESERVED_MISSION_PEDS() + sGangChaseUnit.iMaxPeds)
					
		SET_BIT(MC_serverBD_1.sGangChase[iGangChaseUnit].iBitset, SBBBOOL_RESERVATION_COMPLETE)
		PRINTLN("[GANG_CHASE] RESERVE_GANG_CHASE_UNIT_ENTITIES - Unit: ", iGangChaseUnit, " Network entity reservation complete. Total Vehicles: ", GET_NUM_RESERVED_MISSION_VEHICLES(), " Total Peds: ", GET_NUM_RESERVED_MISSION_PEDS())
	ENDIF
	
	INT iVehiclesToRegister = 1
	IF GET_CUSTOM_GANG_CHASE_TYPE(sGangChaseUnit.iGangType) = FMMC_GANG_CHASE_TYPE_ON_FOOT
		iVehiclesToRegister = 0
	ENDIF
	
	IF NOT CAN_REGISTER_MISSION_ENTITIES(sGangChaseUnit.iMaxPeds, iVehiclesToRegister, 0,  0)
		PRINTLN("[GANG_CHASE] RESERVE_GANG_CHASE_UNIT_ENTITIES - Unit: ", iGangChaseUnit, " Cannot register network entities.")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL CREATE_GANG_CHASE_UNIT(INT iGangChaseUnit, INT iTeam, VECTOR vPosition, PED_INDEX piTargetPed, INT iTargetPart)
	
	GANG_CHASE_UNIT_CONFIG_DATA sGangChaseUnit
	
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	IF iRule >= FMMC_MAX_RULES
		RETURN FALSE
	ENDIF
	
	sGangChaseUnit.iTeam = iTeam
	sGangChaseUnit.iRule = iRule
	sGangChaseUnit.vPosition = vPosition
	sGangChaseUnit.piTargetPed = piTargetPed
	sGangChaseUnit.iTargetPart = iTargetPart
	GET_GANG_CHASE_TYPE_DATA(sGangChaseUnit, iGangChaseUnit, MC_PlayerBD[iTargetPart].iGangChaseChosenType)
	
	IF MC_serverBD_1.sGangChase[iGangChaseUnit].iGangType != sGangChaseUnit.iGangType
		MC_serverBD_1.sGangChase[iGangChaseUnit].iGangType = sGangChaseUnit.iGangType
		SET_BIT(MC_serverBD_1.sGangChase[iGangChaseUnit].iBitset, SBBBOOL_SELECTED_GANG_TYPE)
	ENDIF
	
	IF NOT HAVE_GANG_CHASE_UNIT_ASSETS_LOADED(sGangChaseUnit)
		RETURN FALSE
	ENDIF
	
	IF NOT RESERVE_GANG_CHASE_UNIT_ENTITIES(iGangChaseUnit, sGangChaseUnit)
		RETURN FALSE
	ENDIF
	
	IF GET_CUSTOM_GANG_CHASE_TYPE(sGangChaseUnit.iGangType) != FMMC_GANG_CHASE_TYPE_ON_FOOT
	
		IF NOT CREATE_GANG_CHASE_UNIT_VEHICLE(iGangChaseUnit, sGangChaseUnit)
			RETURN FALSE
		ENDIF
	
		PROCESS_CREATED_GANG_CHASE_UNIT_HELI_BLADES(iGangChaseUnit, sGangChaseUnit)
		
	ENDIF
	
	IF NOT CREATE_GANG_CHASE_UNIT_PEDS(iGangChaseUnit, sGangChaseUnit)
		RETURN FALSE
	ENDIF
	
	CLEANUP_GANG_CHASE_UNIT_MODELS(sGangChaseUnit)
	
	PRINTLN("[GANG_CHASE] CREATE_GANG_CHASE_UNIT - Created Unit: ", iGangChaseUnit)
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL CAN_GANG_CHASE_UNIT_SPAWN(INT iGangChaseUnit)
	
	INT iTeam = MC_serverBD_1.sGangChase[iGangChaseUnit].iSpawnTeam
	IF iTeam = -1
		RETURN FALSE
	ENDIF
	
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	IF iRule >= FMMC_MAX_RULES
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEleven[iRule], ciBS_RULE11_GANG_CHASE_TRIGGER_ON_SPEED_FAIL)
	AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet6, SBBOOL6_ALLOW_GANGCHASE_AFTER_SPEED_FAIL)
		RETURN FALSE
	ENDIF
	
	IF MC_serverBD_1.iNumGangChaseUnitsChasing[iTeam] >= GET_GANG_CHASE_INTENSITY(iTeam, iRule)
		RETURN FALSE
	ENDIF
			
	INT iTotalNumber = GET_GANG_CHASE_TOTAL_UNIT_SPAWNS(iTeam, iRule)
	
	IF iTotalNumber >= 0
	AND MC_serverBD_1.iNumGangChaseUnitsSpawned[iTeam][iRule] >= iTotalNumber
		RETURN FALSE
	ENDIF
			
	RETURN TRUE
	
ENDFUNC
		
PROC PROCESS_GANG_CHASE_UNIT_SPAWNING(INT iGangChaseUnit)

	IF !bIsLocalPlayerHost
		EXIT
	ENDIF

	IF IS_BIT_SET(MC_serverBD_1.sGangChase[iGangChaseUnit].iBitset, SBBBOOL_CREATED)
		EXIT
	ENDIF

	IF NOT CAN_GANG_CHASE_UNIT_SPAWN(iGangChaseUnit)
		EXIT
	ENDIF
	
	IF MC_serverBD_1.iCurrentGangChaseUnitSpawn != -1
	AND MC_serverBD_1.iCurrentGangChaseUnitSpawn != iGangChaseUnit
		EXIT
	ENDIF
	
	IF MC_serverBD_1.sGangChase[iGangChaseUnit].iTargetPart = ciGANG_CHASE_TARGET_NONE
		EXIT
	ENDIF
	
	IF NOT SHOULD_GANG_CHASE_BE_ACTIVE_FOR_TEAM(MC_serverBD_1.sGangChase[iGangChaseUnit].iSpawnTeam)
		RESET_GANG_CHASE_UNIT_SPAWN_DATA(iGangChaseUnit)
		EXIT
	ENDIF
	
	IF MC_serverBD_1.sGangChase[iGangChaseUnit].iTargetPart < 0
		EXIT
	ENDIF
	
	IF MC_serverBD_1.sGangChase[iGangChaseUnit].iGangChaseState != ciGANG_CHASE_UNIT_STATE_WAIT_FOR_CREATION
		EXIT
	ENDIF
	
	INT iTargetPart = MC_serverBD_1.sGangChase[iGangChaseUnit].iTargetPart
	PARTICIPANT_INDEX tempPart = INT_TO_PARTICIPANTINDEX(iTargetPart)
	IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
		EXIT
	ENDIF
	
	PED_INDEX tempPed = GET_GANG_CHASE_TARGET_PED_INDEX(iGangChaseUnit)
			
	IF tempPed = NULL
		EXIT
	ENDIF
	
	MC_serverBD_1.iCurrentGangChaseUnitSpawn = iGangChaseUnit
	
	IF ARE_MULTIPLE_GANG_CHASE_UNIT_TYPES_AVAILABLE_FOR_TEAM(MC_serverBD_1.sGangChase[iGangChaseUnit].iSpawnTeam)	
		IF MC_PlayerBD[iTargetPart].iGangChaseChosenType = -1
			PRINTLN("[GANG_CHASE] PROCESS_GANG_CHASE_UNIT_SPAWNING - Unit: ", iGangChaseUnit, " is waiting for unit type from part: ", iTargetPart)
			IF IS_BIT_SET(MC_serverBD_1.sGangChase[iGangChaseUnit].iBitset, SBBBOOL_SELECTED_GANG_TYPE)
				RESET_GANG_CHASE_UNIT_SPAWN_DATA(iGangChaseUnit)
			ENDIF
			EXIT
		ENDIF
	ENDIF
	
	IF IS_VECTOR_ZERO(MC_PlayerBD[iTargetPart].vGangChaseSpawnCoords)
	OR NOT IS_BIT_SET(MC_PlayerBD[iTargetPart].iGangChaseSpawnBitset, iGangChaseUnit)
		PRINTLN("[GANG_CHASE] PROCESS_GANG_CHASE_UNIT_SPAWNING - Unit: ", iGangChaseUnit, " is waiting for spawn coords from part: ", iTargetPart)
		EXIT
	ENDIF

	IF CREATE_GANG_CHASE_UNIT(iGangChaseUnit, MC_serverBD_1.sGangChase[iGangChaseUnit].iSpawnTeam, MC_PlayerBD[iTargetPart].vGangChaseSpawnCoords, tempPed, iTargetPart)
		
		SET_BIT(MC_serverBD_1.sGangChase[iGangChaseUnit].iBitset, SBBBOOL_CREATED)
		CLEAR_BIT(MC_serverBD_1.sGangChase[iGangChaseUnit].iBitset, SBBBOOL_SELECTED_GANG_TYPE)
		
		MC_serverBD_1.iCurrentGangChaseUnitSpawn = -1
		vGangChaseSpawnDirection = <<0.0, 0.0, 0.0>>
		
		MC_serverBD_1.iNumGangChaseUnitsChasing[MC_serverBD_1.sGangChase[iGangChaseUnit].iSpawnTeam]++
		
		REINIT_NET_TIMER(tdGangChaseSpawnIntervalTimer[MC_serverBD_1.sGangChase[iGangChaseUnit].iSpawnTeam])
		
		INT iBackUpTeamPriority = MC_serverBD_4.iCurrentHighestPriority[MC_serverBD_1.sGangChase[iGangChaseUnit].iSpawnTeam]
		IF iBackUpTeamPriority < FMMC_MAX_RULES
			MC_serverBD_1.iNumGangChaseUnitsSpawned[MC_serverBD_1.sGangChase[iGangChaseUnit].iSpawnTeam][iBackUpTeamPriority]++
		ENDIF
		
		PRINTLN("[GANG_CHASE] PROCESS_GANG_CHASE_UNIT_SPAWNING - Unit: ", iGangChaseUnit, " Creation successful (Team: ", MC_serverBD_1.sGangChase[iGangChaseUnit].iSpawnTeam," Rule: ", iBackUpTeamPriority,"). Current Chasing: ", MC_serverBD_1.iNumGangChaseUnitsChasing[MC_serverBD_1.sGangChase[iGangChaseUnit].iSpawnTeam], " Total Spawned: ", MC_serverBD_1.iNumGangChaseUnitsSpawned[MC_serverBD_1.sGangChase[iGangChaseUnit].iSpawnTeam][iBackUpTeamPriority])
		
	ENDIF

ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Local Gang Chase Spawn Coord Processing
// ##### Description: Functions dealing with finding valid spawn coords for Gang Chase Units.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL SHOULD_GANG_CHASE_SPAWN_COORDS_BE_GENERATED()
	
	IF MC_serverBD_1.iCurrentGangChaseUnitSpawn = -1
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(MC_serverBD_1.sGangChase[MC_serverBD_1.iCurrentGangChaseUnitSpawn].iBitset, SBBBOOL_CREATED)
		RETURN FALSE
	ENDIF
	
	IF MC_serverBD_1.sGangChase[MC_serverBD_1.iCurrentGangChaseUnitSpawn].iTargetPart != iLocalPart
		RETURN FALSE
	ENDIF
	
	IF NOT bLocalPlayerOk
		RETURN FALSE
	ENDIF
	
	IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iTeam] >= FMMC_MAX_RULES
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC VECTOR RANDOMISE_ROTATION_FOR_AIR_GANG_CHASE(VECTOR vRotation)
	
	SET_RANDOM_SEED(NATIVE_TO_INT(GET_NETWORK_TIME()))
	INT iRandom = GET_RANDOM_INT_IN_RANGE(0, 361)
	
	vRotation.z += iRandom
	IF vRotation.z > 180.0
		vRotation.z -= 360.0
	ENDIF
	
	RETURN CONVERT_ROTATION_TO_DIRECTION_VECTOR(vRotation)
	
ENDFUNC

FUNC BOOL SHOULD_GANG_CHASE_UNIT_ATTEMPT_TO_SPAWN_FORWARDS(INT iGangChaseUnit, INT iTeam, INT iRule)
	
	IF NOT IS_VECTOR_ZERO(vGangChaseSpawnDirection)
		RETURN IS_BIT_SET(MC_PlayerBD[iLocalPart].iGangChaseSpawnBitset, PBGANGSPAWN_UNIT_0_SUCCEED_FORWARDS + iGangChaseUnit)
	ENDIF
	
	BOOL bTempSpawnForwards = FALSE
		
	IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iGangChaseForwardSpawnChance[iRule] > 0
	AND g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iGangChaseForwardSpawnChance[iRule] >= GET_RANDOM_INT_IN_RANGE(0, 100)
		bTempSpawnForwards = TRUE
	ENDIF
	
	VEHICLE_INDEX viVehicle
	IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		viVehicle = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
	ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(viVehicle)
		vGangChaseSpawnDirection = GET_ENTITY_FORWARD_VECTOR(viVehicle)
		
		IF bTempSpawnForwards
			IF ABSF(GET_ENTITY_SPEED(viVehicle)) < 5.0
				PRINTLN("[GANG_CHASE] SHOULD_GANG_CHASE_UNIT_ATTEMPT_TO_SPAWN_FORWARDS - Too slow to spawn forwards.")
				bTempSpawnForwards = FALSE
			ENDIF
		ELIF IS_GANG_CHASE_AIR(MC_serverBD_1.sGangChase[iGangChaseUnit].iGangType)
			vGangChaseSpawnDirection = RANDOMISE_ROTATION_FOR_AIR_GANG_CHASE(GET_ENTITY_ROTATION(viVehicle))
		ENDIF
	ELSE
		bTempSpawnForwards = FALSE
		vGangChaseSpawnDirection = GET_ENTITY_FORWARD_VECTOR(LocalPlayerPed)
	ENDIF
		
	IF bTempSpawnForwards
		SET_BIT(MC_PlayerBD[iLocalPart].iGangChaseSpawnBitset, PBGANGSPAWN_UNIT_0_TRY_FORWARDS + iGangChaseUnit)
		SET_BIT(MC_PlayerBD[iLocalPart].iGangChaseSpawnBitset, PBGANGSPAWN_UNIT_0_SUCCEED_FORWARDS + iGangChaseUnit)
		iGangChaseForwardSpawnAttempts = 0
		PRINTLN("[GANG_CHASE] SHOULD_GANG_CHASE_UNIT_ATTEMPT_TO_SPAWN_FORWARDS - Attempting to spawn forwards: ", iGangChaseUnit)
		RETURN TRUE
	ENDIF
	
	CLEAR_BIT(MC_PlayerBD[iLocalPart].iGangChaseSpawnBitset, PBGANGSPAWN_UNIT_0_TRY_FORWARDS + iGangChaseUnit)
	CLEAR_BIT(MC_PlayerBD[iLocalPart].iGangChaseSpawnBitset, PBGANGSPAWN_UNIT_0_SUCCEED_FORWARDS + iGangChaseUnit)
	
	RETURN FALSE

ENDFUNC

FUNC BOOL SHOULD_GANG_CHASE_UNIT_SPAWN_BEHIND(INT iGangChaseUnit, INT iTeam, INT iRule)
	
	IF IS_BIT_SET(MC_PlayerBD[iLocalPart].iGangChaseSpawnBitset, PBGANGSPAWN_UNIT_0_SPAWN_BEHIND + iGangChaseUnit)
		RETURN TRUE
	ENDIF
	
	IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iGangChaseSpawnBehindChance[iRule] <= 0
		RETURN FALSE
	ENDIF
	
	SET_RANDOM_SEED(NATIVE_TO_INT(GET_NETWORK_TIME_ACCURATE()))
	IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iGangChaseSpawnBehindChance[iRule] < GET_RANDOM_INT_IN_RANGE(0, 100)
		RETURN FALSE
	ENDIF
	
	PRINTLN("[GANG_CHASE] SHOULD_GANG_CHASE_UNIT_SPAWN_BEHIND - Spawning Behind: ", iGangChaseUnit)
	SET_BIT(MC_PlayerBD[iLocalPart].iGangChaseSpawnBitset, PBGANGSPAWN_UNIT_0_SPAWN_BEHIND + iGangChaseUnit)
	vGangChaseSpawnDirection = vGangChaseSpawnDirection * <<-1, -1, -1>>
	RETURN TRUE
	
ENDFUNC

FUNC FLOAT GET_GANG_CHASE_UNIT_SPAWN_DISTANCE(INT iGangType)
	
	FLOAT fSpawnDistance = GET_GANG_CHASE_SPAWN_DISTANCE(iGangType)
	
	IF GET_CUSTOM_GANG_CHASE_TYPE(iGangType) != FMMC_GANG_CHASE_TYPE_ON_FOOT
		IF vGangChaseSpawnDirection.z >= 0.0 AND vGangChaseSpawnDirection.z < 90.0
		OR vGangChaseSpawnDirection.z >= 270.0 AND vGangChaseSpawnDirection.z < 360.0
			fSpawnDistance += g_FMMC_STRUCT.fGangChaseUnitSpawnDistForwardExtra
		ENDIF
	ENDIF
	
	RETURN fSpawnDistance
	
ENDFUNC

FUNC VECTOR GET_GANG_CHASE_SEA_SPAWN_COORDS(VECTOR vInputCoords, FLOAT fSpawnDistance, INT iGangType)

	FLOAT fInputHeading = GET_ENTITY_HEADING(LocalPlayerPed)
	
	NearestCarNodeSearch sSearchParams
	sSearchParams.bConsiderOnlyActiveNodes = FALSE
	sSearchParams.bWaterNodesOnly = TRUE
	sSearchParams.CarModel = GET_GANG_CHASE_UNIT_VEHICLE_MODEL(iGangType)
	sSearchParams.bReturnNearestGoodNode = TRUE
	sSearchParams.fMinDistFromCoords = fSpawnDistance
	sSearchParams.fVisibleDistance = g_FMMC_STRUCT.fGangChaseSeaVisibleDistance
	sSearchParams.bCheckForLowestEnemiesNearPoint = TRUE
	
	GetNearestCarNode(vInputCoords, fInputHeading, sSearchParams)
	
	RETURN vInputCoords
	
ENDFUNC

FUNC VECTOR GET_GANG_CHASE_ON_FOOT_SPAWN_COORDS(VECTOR vInputCoords, FLOAT fSpawnDistance, INT iGangChaseUnit)
			
	SPAWN_SEARCH_PARAMS SpawnSearchParams
	SpawnSearchParams.bConsiderInteriors = TRUE
	SpawnSearchParams.fMinDistFromPlayer = fSpawnDistance
	SpawnSearchParams.vFacingCoords = vInputCoords
	
	VECTOR vReturnCoords			
	FLOAT fReturnHeading	
	
	IF CAN_RUN_ENTITY_SPAWN_POS_SEARCH(ciEntitySpawnSearchType_GangChase, iGangChaseUnit)
		IF iSearchingForGangChase = -1
			PRINTLN("[GANG_CHASE] GET_GANG_CHASE_ON_FOOT_SPAWN_COORDS - Searching for spawn point: ", iGangChaseUnit)
			iSearchingForGangChase = iGangChaseUnit
			vGangChaseSearchPos = vInputCoords
		ENDIF
		IF GET_SAFE_COORDS_IN_AREA_FOR_CREATING_ENTITY(vGangChaseSearchPos, fSpawnDistance, vReturnCoords, fReturnHeading, SpawnSearchParams)
			iSearchingForGangChase = -1
			vGangChaseSearchPos = <<0,0,0>>
			RETURN vReturnCoords
		ENDIF
	ENDIF
	
	PRINTLN("[GANG_CHASE] GET_GANG_CHASE_ON_FOOT_SPAWN_COORDS - Failed to find a spawn point for ", iGangChaseUnit)
	RETURN <<0.0, 0.0, 0.0>>
	
ENDFUNC

FUNC VECTOR GET_GANG_CHASE_UNIT_CANDIDATE_SPAWN_COORDS(INT iTeam, INT iRule, INT iGangChaseUnit, BOOL &bSpawnForwards, BOOL &bSpawnBehind, INT iChosenType = -1)

	INT iGangType = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iGangBackupType[iRule]
	IF iChosenType > -1
		iGangType = iChosenType
	ENDIF
	
	IF IS_GANG_CHASE_AIR(iGangType)
	AND NOT IS_BIT_SET(MC_PlayerBD[iLocalPart].iGangChaseSpawnBitset, PBGANGSPAWN_UNIT_0_AIR_FAILED_HELISPAWN + iGangChaseUnit)
		RETURN FIND_SPAWN_COORDINATES_FOR_HELI(LocalPlayerPed)
	ENDIF
	
	VECTOR vPlayerCoords = GET_ENTITY_COORDS(LocalPlayerPed)
	FLOAT fSpawnDistance = GET_GANG_CHASE_UNIT_SPAWN_DISTANCE(iGangType)
	
	IF IS_GANG_CHASE_SEA(iGangType)
		RETURN GET_GANG_CHASE_SEA_SPAWN_COORDS(vPlayerCoords, fSpawnDistance, iGangType)
	ENDIF
	
	IF GET_CUSTOM_GANG_CHASE_TYPE(iGangType) = FMMC_GANG_CHASE_TYPE_ON_FOOT
		RETURN GET_GANG_CHASE_ON_FOOT_SPAWN_COORDS(vPlayerCoords, fSpawnDistance, iGangChaseUnit)
	ENDIF
	
	bSpawnForwards = SHOULD_GANG_CHASE_UNIT_ATTEMPT_TO_SPAWN_FORWARDS(iGangChaseUnit, iTeam, iRule)
	
	bSpawnBehind = SHOULD_GANG_CHASE_UNIT_SPAWN_BEHIND(iGangChaseUnit, iTeam, iRule)
	
	VECTOR vSpawnCoords = <<0.0, 0.0, 0.0>>
	
	IF NOT FIND_SPAWN_POINT_IN_DIRECTION(vPlayerCoords, vGangChaseSpawnDirection, fSpawnDistance, vSpawnCoords)
		PRINTLN("[GANG_CHASE] GET_GANG_CHASE_UNIT_CANDIDATE_SPAWN_COORDS - Unit: ", iGangChaseUnit, " Failed to find a spawn point!")
		RETURN <<0.0, 0.0, 0.0>>
	ENDIF
	
	IF IS_GANG_CHASE_AIR(iGangType) 
		vSpawnCoords.z += FMAX(GET_ENTITY_HEIGHT_ABOVE_GROUND(LocalPlayerPed), g_FMMC_STRUCT.fGangChaseAirSpawnMinZOffset)
	ENDIF
	
	IF bSpawnForwards
		
		FLOAT fNodeHeading
		IF NOT DOES_NODE_HAVE_SUITABLE_GANG_CHASE_HEADING(vSpawnCoords, GET_ENTITY_HEADING(LocalPlayerPed), fNodeHeading, iGangType, DEFAULT, 100.0)
			PRINTLN("[GANG_CHASE] GET_GANG_CHASE_UNIT_CANDIDATE_SPAWN_COORDS - Unit: ", iGangChaseUnit, " Node heading unsuitable: ", fNodeHeading)
			RETURN <<0.0, 0.0, 0.0>>
		ENDIF
		
		RETURN vSpawnCoords
		
	ENDIF
	
	IF IS_GANG_CHASE_AIR(iGangType)
		RETURN vSpawnCoords
	ENDIF

	//Check the unit is spawning infront of or behind the player and not too far left or right
	ENTITY_INDEX eiOffsetEntity = LocalPlayerPed
	IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		VEHICLE_INDEX viVehicle = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
		IF IS_VEHICLE_DRIVEABLE(viVehicle)
			eiOffsetEntity = viVehicle
		ENDIF
	ENDIF
	VECTOR vOffsetFromPlayer = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(eiOffsetEntity, vSpawnCoords)
	IF ABSF(vOffsetFromPlayer.x) >= g_FMMC_STRUCT.fGangChaseUnitSpawnPointMaxOffset
		PRINTLN("[GANG_CHASE] GET_GANG_CHASE_UNIT_CANDIDATE_SPAWN_COORDS - Unit: ", iGangChaseUnit, " Offset from player unsuitable: ", vOffsetFromPlayer)
		RETURN <<0.0, 0.0, 0.0>>
	ENDIF
	
	RETURN vSpawnCoords
	
ENDFUNC

FUNC BOOL ARE_GANG_CHASE_SPAWN_COORDS_TOO_CLOSE_TO_OBJECTIVE(VECTOR &vSpawnCoords, INT iTeam, INT iGangChaseUnit, INT iGangType)
	
	IF IS_CURRENT_OBJECTIVE_LOSE_GANG_CHASE()
		RETURN FALSE
	ENDIF
	
	VECTOR vDropOff = GET_DROP_OFF_CENTER_FOR_TEAM(DEFAULT, iTeam, iLocalPart)
	IF IS_VECTOR_ZERO(vDropOff)
		RETURN FALSE
	ENDIF
	
	IF VDIST2(vSpawnCoords, vDropOff) >= POW(g_FMMC_STRUCT.fGangChaseUnitMinObjectiveDistance, 2)
		RETURN FALSE
	ENDIF
	
	PRINTLN("[GANG_CHASE] ARE_GANG_CHASE_SPAWN_COORDS_TOO_CLOSE_TO_OBJECTIVE - Spawn Point is too close to the team's objective drop off: ", vSpawnCoords)
	
	vGangChaseSpawnDirection = <<0.0, 0.0, 0.0>>
	
	IF IS_GANG_CHASE_AIR(iGangType)
	AND NOT IS_BIT_SET(MC_PlayerBD[iLocalPart].iGangChaseSpawnBitset, PBGANGSPAWN_UNIT_0_AIR_FAILED_HELISPAWN + iGangChaseUnit)
		SET_BIT(MC_PlayerBD[iLocalPart].iGangChaseSpawnBitset, PBGANGSPAWN_UNIT_0_AIR_FAILED_HELISPAWN + iGangChaseUnit)
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL ARE_ANY_GANG_CHASE_UNITS_TOO_CLOSE_TO_COORD(VECTOR vCoord, INT iGangType)
	
	FLOAT fMinDistance2 = POW(GET_MIN_DISTANCE_BETWEEN_UNITS(iGangType), 2)
	
	INT i
	FOR i = 0 TO ciMAX_GANG_CHASE_VEHICLES - 1
	
		IF GET_CUSTOM_GANG_CHASE_TYPE(MC_serverBD_1.sGangChase[i].iGangType) = FMMC_GANG_CHASE_TYPE_ON_FOOT
			
			INT iPed
			FOR iPed = 0 TO ciMAX_GANG_CHASE_PEDS_PER_VEHICLE - 1
			
				IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sGangChase[i].niPeds[iPed])
					RELOOP
				ENDIF
				
				IF IS_NET_PED_INJURED(MC_serverBD_1.sGangChase[i].niPeds[iPed])
					RETURN FALSE
				ENDIF
				
				IF VDIST2(GET_ENTITY_COORDS(NET_TO_PED(MC_serverBD_1.sGangChase[i].niPeds[iPed]), FALSE), vCoord) < fMinDistance2
					RETURN TRUE
				ENDIF
				
			ENDFOR
			
			RELOOP
			
		ENDIF
		
		IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sGangChase[i].niVeh)
			RELOOP
		ENDIF
		
		VEHICLE_INDEX viVehicle = NET_TO_VEH(MC_serverBD_1.sGangChase[i].niVeh)
		IF NOT IS_VEHICLE_DRIVEABLE(viVehicle)
			RELOOP
		ENDIF
		
		IF VDIST2(GET_ENTITY_COORDS(viVehicle, FALSE), vCoord) < fMinDistance2
			RETURN TRUE
		ENDIF
		
	ENDFOR
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL ARE_GANG_CHASE_SPAWN_COORDS_VALID(VECTOR &vSpawnCoords, INT iTeam, INT iRule, INT iGangChaseUnit, INT iChosenType = -1)

	IF IS_VECTOR_ZERO(vSpawnCoords)
		RETURN FALSE
	ENDIF
	
	INT iGangType = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iGangBackupType[iRule]
	IF iChosenType > -1
		iGangType = iChosenType
	ENDIF
	
	IF ARE_GANG_CHASE_SPAWN_COORDS_TOO_CLOSE_TO_OBJECTIVE(vSpawnCoords, iTeam, iGangChaseUnit, iGangType)
		RETURN FALSE
	ENDIF
	
	FLOAT fSpawnDist2FromPlayer = VDIST2(vSpawnCoords, GET_ENTITY_COORDS(LocalPlayerPed))
	FLOAT fMaxDistance = GET_GANG_CHASE_TIMED_CLEANUP_DISTANCE(iGangType)
	IF fSpawnDist2FromPlayer > POW(fMaxDistance, 2)
		RETURN FALSE
	ENDIF
	
	IF CAN_ANY_PLAYER_SEE_POINT(vSpawnCoords, 10.0, TRUE, TRUE, 200.0)
		RETURN FALSE
	ENDIF
	
	IF ARE_ANY_GANG_CHASE_UNITS_TOO_CLOSE_TO_COORD(vSpawnCoords, iGangType)
		RETURN FALSE
	ENDIF
	
	INT iZoneIndex
	FOR iZoneIndex = 0 TO ciFMMC_BLOCK_GANG_CHASE_SPAWN_ZONES_MAX - 1
		IF iZoneIndicesOfTypeBlockGangChaseSpawn[iZoneIndex] >= 0
		AND iZoneIndicesOfTypeBlockGangChaseSpawn[iZoneIndex] < ciFMMC_ZONE_TYPE__MAX
		AND ARE_COORDS_IN_FMMC_ZONE(vSpawnCoords, iZoneIndicesOfTypeBlockGangChaseSpawn[iZoneIndex])
			RETURN FALSE
		ENDIF
	ENDFOR
	
	IF NOT IS_POINT_OK_FOR_NET_ENTITY_CREATION(vSpawnCoords, DEFAULT, DEFAULT, DEFAULT, DEFAULT, FALSE, FALSE)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC FLOAT GET_GANG_CHASE_FOWARD_SPAWN_ROTATION_FROM_ATTEMPTS(INT iAttempts)
	
	SWITCH iAttempts
		CASE 1 	RETURN 	10.0 	//+10 Degrees
		CASE 2 	RETURN 	10.0	//+20 Degrees
		CASE 3 	RETURN 	-30.0	//-10 Degrees
		CASE 4 	RETURN 	-10.0 	//-20 Degrees
		CASE 5 	RETURN 	20.0	//Back to 0 Degrees
	ENDSWITCH
	
	RETURN 0.0
	
ENDFUNC

PROC PROCESS_SPAWN_ROTATION_CHANGE_FOR_GANG_CHASE_UNIT_AIR(VECTOR &vSpawnDirection, INT iGangChaseUnit)

	IF NOT IS_BIT_SET(MC_PlayerBD[iLocalPart].iGangChaseSpawnBitset, PBGANGSPAWN_UNIT_0_AIR_FAILED_HELISPAWN + iGangChaseUnit)
		PRINTLN("[GANG_CHASE] PROCESS_SPAWN_ROTATION_CHANGE_FOR_GANG_CHASE_UNIT_AIR - Unit: ", iGangChaseUnit, " Air spawn failed, setting bit.")
		SET_BIT(MC_PlayerBD[iLocalPart].iGangChaseSpawnBitset, PBGANGSPAWN_UNIT_0_AIR_FAILED_HELISPAWN + iGangChaseUnit)
		EXIT
	ENDIF
	
	ROTATE_VECTOR_ABOUT_Z(vSpawnDirection, cfGANG_CHASE_ROTATION_AMOUNT_ON_FAIL)
	
	iGangChaseAirStandardSpawnAttempts++
	IF iGangChaseAirStandardSpawnAttempts >= (360.0 / cfGANG_CHASE_ROTATION_AMOUNT_ON_FAIL)
		iGangChaseAirStandardSpawnAttempts = 0
		PRINTLN("[GANG_CHASE] PROCESS_SPAWN_ROTATION_CHANGE_FOR_GANG_CHASE_UNIT_AIR - Unit: ", iGangChaseUnit, " Max air spawning attempts used.")
		CLEAR_BIT(MC_PlayerBD[iLocalPart].iGangChaseSpawnBitset, PBGANGSPAWN_UNIT_0_AIR_FAILED_HELISPAWN + iGangChaseUnit)
	ENDIF

ENDPROC

PROC PROCESS_SPAWN_ROTATION_CHANGE_FOR_GANG_CHASE_UNIT(VECTOR &vSpawnDirection, INT iGangChaseUnit, INT iTeam, INT iRule, BOOL bSpawnForwards = FALSE, INT iChosenType = -1)
	
	INT iGangType = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iGangBackupType[iRule]
	IF iChosenType > -1
		iGangType = iChosenType
	ENDIF
	
	IF !bSpawnForwards
		IF IS_GANG_CHASE_AIR(iGangType)
			PROCESS_SPAWN_ROTATION_CHANGE_FOR_GANG_CHASE_UNIT_AIR(vSpawnDirection, iGangChaseUnit)
			EXIT
		ENDIF
		
		ROTATE_VECTOR_ABOUT_Z(vSpawnDirection, cfGANG_CHASE_ROTATION_AMOUNT_ON_FAIL)
		EXIT
	ENDIF
	
	iGangChaseForwardSpawnAttempts++
	FLOAT fZRot = GET_GANG_CHASE_FOWARD_SPAWN_ROTATION_FROM_ATTEMPTS(iGangChaseForwardSpawnAttempts)
	
	IF iGangChaseForwardSpawnAttempts >= ciGANG_CHASE_FORWARD_SPAWN_ATTEMPTS
		iGangChaseForwardSpawnAttempts = 0
		PRINTLN("[GANG_CHASE] PROCESS_SPAWN_ROTATION_CHANGE_FOR_GANG_CHASE_UNIT - Unit: ", iGangChaseUnit, " Max forward spawning attempts used.")
		CLEAR_BIT(MC_PlayerBD[iLocalPart].iGangChaseSpawnBitset, PBGANGSPAWN_UNIT_0_SUCCEED_FORWARDS + iGangChaseUnit)
	ENDIF
		
	IF fZRot != 0.0
		ROTATE_VECTOR_ABOUT_Z(vSpawnDirection, fZRot)
	ENDIF
	
ENDPROC

PROC PROCESS_GANG_CHASE_SPAWN_COORDS()
	
	IF bIsAnySpectator
		EXIT
	ENDIF
	
	IF NOT SHOULD_GANG_CHASE_SPAWN_COORDS_BE_GENERATED()
		RESET_LOCAL_GANG_CHASE_SPAWN_DATA(MC_serverBD_1.iCurrentGangChaseUnitSpawn)
		EXIT
	ENDIF
	
	INT iTeam = MC_playerBD[iLocalPart].iTeam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	IF MC_serverBD_4.iCurrentHighestPriority[iTeam] > FMMC_MAX_RULES
		EXIT
	ENDIF
	
	INT iGangChaseUnit = MC_serverBD_1.iCurrentGangChaseUnitSpawn

	IF MC_playerBD[iLocalPart].iGangChaseChosenType = -1
		INT iChosenType = GET_GANG_CHASE_UNIT_TYPE_FOR_PLAYER(iTeam, iRule)
		IF iChosenType = -1
			PRINTLN("[GANG_CHASE] PROCESS_GANG_CHASE_SPAWN_COORDS - Unit: ", iGangChaseUnit, " No valid types for player.")
			EXIT
		ENDIF
		MC_playerBD[iLocalPart].iGangChaseChosenType = iChosenType
		PRINTLN("[GANG_CHASE] PROCESS_GANG_CHASE_SPAWN_COORDS - Unit: ", iGangChaseUnit, " Chosen Type: ", MC_playerBD[iLocalPart].iGangChaseChosenType)
	ELIF NOT CAN_GANG_CHASE_TYPE_SPAWN(MC_playerBD[iLocalPart].iGangChaseChosenType, iTeam, iRule)
	AND NOT IS_BIT_SET(MC_PlayerBD[iLocalPart].iGangChaseSpawnBitset, MC_serverBD_1.iCurrentGangChaseUnitSpawn)
		PRINTLN("[GANG_CHASE] PROCESS_GANG_CHASE_SPAWN_COORDS - Unit: ", iGangChaseUnit, " Chosen Type Now Invalid: ", MC_playerBD[iLocalPart].iGangChaseChosenType)
		RESET_LOCAL_GANG_CHASE_SPAWN_DATA(iGangChaseUnit)
		EXIT
	ENDIF
	
	IF IS_BIT_SET(MC_PlayerBD[iLocalPart].iGangChaseSpawnBitset, MC_serverBD_1.iCurrentGangChaseUnitSpawn)
		EXIT
	ENDIF

	BOOL bSpawnForwards, bSpawnBehind
	VECTOR vCandidateSpawnCoords = GET_GANG_CHASE_UNIT_CANDIDATE_SPAWN_COORDS(iTeam, iRule, iGangChaseUnit, bSpawnForwards, bSpawnBehind, MC_playerBD[iLocalPart].iGangChaseChosenType)
	
	PRINTLN("[GANG_CHASE] PROCESS_GANG_CHASE_SPAWN_COORDS - Unit: ", iGangChaseUnit, " Candidate spawn point: ", vCandidateSpawnCoords)
	
	IF NOT ARE_GANG_CHASE_SPAWN_COORDS_VALID(vCandidateSpawnCoords, iTeam, iRule, iGangChaseUnit, MC_playerBD[iLocalPart].iGangChaseChosenType)
		IF MC_playerBD[iLocalPart].iGangChaseChosenType > -1
			iGangChaseTotalSpawnAttempts ++
			IF iGangChaseTotalSpawnAttempts > ciGANG_CHASE_UNIT_SPAWN_ATTEMPTS
				PRINTLN("[GANG_CHASE] PROCESS_GANG_CHASE_SPAWN_COORDS - Unit: ", iGangChaseUnit, " failed to spawn (type ", MC_playerBD[iLocalPart].iGangChaseChosenType, ") - try another type")
				RESET_LOCAL_GANG_CHASE_SPAWN_DATA(iGangChaseUnit)
				EXIT
			ENDIF
		ENDIF
		PROCESS_SPAWN_ROTATION_CHANGE_FOR_GANG_CHASE_UNIT(vGangChaseSpawnDirection, iGangChaseUnit, iTeam, iRule, bSpawnForwards, MC_playerBD[iLocalPart].iGangChaseChosenType)
		EXIT
	ENDIF
	
	MC_PlayerBD[iLocalPart].vGangChaseSpawnCoords = vCandidateSpawnCoords + <<0.0, 0.0, 0.5>>
	vGangChaseSpawnDirection = <<0.0, 0.0, 0.0>>
	iGangChaseTotalSpawnAttempts = 0
	iGangChaseForwardSpawnAttempts = 0
	iGangChaseAirStandardSpawnAttempts = 0
	SET_BIT(MC_PlayerBD[iLocalPart].iGangChaseSpawnBitset, iGangChaseUnit)
	
	PRINTLN("[GANG_CHASE] PROCESS_GANG_CHASE_SPAWN_COORDS - Unit: ", iGangChaseUnit, " Spawn points set to: ", MC_PlayerBD[iLocalPart].vGangChaseSpawnCoords)
		
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Local Gang Chase Target Processing
// ##### Description: Functions dealing with setting the player as a potential target for the gang chase.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL SHOULD_PLAYER_BE_GANG_CHASE_TARGET()
	
	IF bIsAnySpectator
		RETURN FALSE
	ENDIF
	
	IF MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iTeam] >= FMMC_MAX_RULES
		RETURN FALSE
	ENDIF
	
	IF MC_serverBD_4.iObjMissionLogic[MC_playerBD[iLocalPart].iTeam] = FMMC_OBJECTIVE_LOGIC_LEAVE_LOCATION
	AND MC_playerBD[iLocalPart].iLeaveLoc != -1
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_IN_CREATOR_AIRCRAFT(LocalPlayer)
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iRuleBitsetFourteen[MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iTeam]], ciBS_RULE14_GANG_CHASE_IGNORE_OBJECTIVE_TARGETING)
		//Players are always targets on this rule.
		RETURN TRUE
	ENDIF
	
	IF IS_POTENTIAL_BACKUP_TARGET_NEAR_DROP_OFF(PlayerPedToUse, MC_playerBD[iLocalPart].iTeam, -1, iLocalPart)
		RETURN FALSE
	ENDIF
	
	IF MC_serverBD.iNumPlayerRuleHighestPriority[MC_playerBD[iLocalPart].iTeam] > 0 
		RETURN TRUE
	ENDIF
	
	IF MC_serverBD.iNumLocHighestPriority[MC_playerBD[iLocalPart].iTeam] > 0 
		RETURN TRUE
	ENDIF
	
	IF MC_serverBD.iNumPedHighestPriority[MC_playerBD[iLocalPart].iTeam] > 0
		
		IF MC_serverBD.iNumPedHighestPriorityHeld[MC_playerBD[iLocalPart].iTeam] = 0
			RETURN TRUE
		ENDIF
		
		IF MC_serverBD_4.iPedMissionLogic[MC_playerBD[iLocalPart].iTeam] = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER 
		OR MC_serverBD_4.iPedMissionLogic[MC_playerBD[iLocalPart].iTeam] = FMMC_OBJECTIVE_LOGIC_GET_AND_HOLD 
		OR MC_serverBD_4.iPedMissionLogic[MC_playerBD[iLocalPart].iTeam] = FMMC_OBJECTIVE_LOGIC_CAPTURE
			IF MC_playerBD[iLocalPart].iPedCarryCount <= 0
			AND NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_WITH_CARRIER)
				RETURN FALSE
			ENDIF
		ENDIF
		
		RETURN TRUE
		
	ENDIF
	
	IF MC_serverBD.iNumVehHighestPriority[MC_playerBD[iLocalPart].iTeam] > 0
		
		IF MC_serverBD.iNumVehHighestPriorityHeld[MC_playerBD[iLocalPart].iTeam] = 0
			RETURN TRUE
		ENDIF
		
		IF MC_serverBD_4.iVehMissionLogic[MC_playerBD[iLocalPart].iTeam] = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER 
		OR MC_serverBD_4.iVehMissionLogic[MC_playerBD[iLocalPart].iTeam] = FMMC_OBJECTIVE_LOGIC_GET_AND_HOLD
			IF MC_playerBD[iLocalPart].iVehNear = -1
			OR NOT (amIWaitingForPlayersToEnterVeh != NULL AND NOT CALL amIWaitingForPlayersToEnterVeh())
				RETURN FALSE
			ENDIF
		ENDIF
				
		IF MC_serverBD_4.iVehMissionLogic[MC_playerBD[iLocalPart].iTeam]  = FMMC_OBJECTIVE_LOGIC_CAPTURE
			IF MC_playerBD[iLocalPart].iVehCarryCount <= 0  
			AND NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_WITH_CARRIER)
				RETURN FALSE
			ENDIF	
		ENDIF
		
		RETURN TRUE
		
	ENDIF
	
	IF MC_serverBD.iNumObjHighestPriority[MC_playerBD[iLocalPart].iTeam] > 0
		
		IF MC_serverBD.iNumObjHighestPriorityHeld[MC_playerBD[iLocalPart].iTeam] = 0
			RETURN TRUE
		ENDIF
		
		IF MC_serverBD_4.iObjMissionLogic[MC_playerBD[iLocalPart].iTeam] = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER 
		OR MC_serverBD_4.iObjMissionLogic[MC_playerBD[iLocalPart].iTeam] = FMMC_OBJECTIVE_LOGIC_GET_AND_HOLD //this high priority object is a collect target
		OR MC_serverBD_4.iObjMissionLogic[MC_playerBD[iLocalPart].iTeam] = FMMC_OBJECTIVE_LOGIC_CAPTURE
			IF MC_playerBD[iLocalPart].iObjCarryCount <= 0
			AND NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet,PBBOOL_WITH_CARRIER)
				RETURN FALSE
			ENDIF
		ENDIF
		
		IF MC_serverBD_4.iObjMissionLogic[MC_playerBD[iLocalPart].iTeam] = FMMC_OBJECTIVE_LOGIC_MINIGAME
			IF MC_playerBD[iLocalPart].iObjHacking = -1
				RETURN FALSE
			ENDIF
		ENDIF
		
		RETURN TRUE
		
	ENDIF
	
	IF IS_CURRENT_OBJECTIVE_LOSE_GANG_CHASE()
		RETURN TRUE
	ENDIF
		
	RETURN FALSE
	
ENDFUNC

PROC PROCESS_SETTING_PLAYER_AS_GANG_CHASE_TARGET()

	IF SHOULD_PLAYER_BE_GANG_CHASE_TARGET()
		
		IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_CHASE_TARGET)
			PRINTLN("[GANG_CHASE] PROCESS_SETTING_PLAYER_AS_GANG_CHASE_TARGET - Setting Local Player as Target.")
			SET_BIT(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_CHASE_TARGET)
		ENDIF
		
	ELIF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_CHASE_TARGET)
		
		PRINTLN("[GANG_CHASE] PROCESS_SETTING_PLAYER_AS_GANG_CHASE_TARGET - Setting Local Player No Longer a Target.")
		CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_CHASE_TARGET)
		
	ENDIF
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Process Gang Chase
// ##### Description: Central function for all gang chase processing
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_GANG_CHASE()
	
	#IF IS_DEBUG_BUILD
	START_MISSION_CONTROLLER_PROFILING(ciFMMC_PROFILER_GANGCHASE)
	#ENDIF
	
	//Local Processing
	PROCESS_LOSE_GANG_CHASE_OBJECTIVE()
	PROCESS_SETTING_PLAYER_AS_GANG_CHASE_TARGET()
	PROCESS_GANG_CHASE_SPAWN_COORDS()
	
	//Every Frame Processing
	INT i
	FOR i = 0 TO ciMAX_GANG_CHASE_VEHICLES - 1
		
		PROCESS_GANG_CHASE_UNIT_SPAWNING(i)
		
		PROCESS_GANG_CHASE_UNIT_EVERY_FRAME(i)
		
	ENDFOR
	
	//Staggered Processing
	IF iGangChaseUnitIterator >= ciMAX_GANG_CHASE_VEHICLES
		iGangChaseUnitIterator = 0
	ENDIF
	
	PROCESS_SERVER_GANG_CHASE_UNIT(iGangChaseUnitIterator)
	
	iGangChaseUnitIterator++
	
	#IF IS_DEBUG_BUILD
	PAUSE_MISSION_CONTROLLER_PROFILING(ciFMMC_PROFILER_GANGCHASE)
	
	//Caching for 6 menu
	bDebugGangChaseActiveForMyTeam = SHOULD_GANG_CHASE_BE_ACTIVE_FOR_TEAM(MC_playerBD[iLocalPart].iTeam)
	
	IF MC_ServerBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iTeam] < FMMC_MAX_RULES
		iDebugGangChaseIntensity = GET_GANG_CHASE_INTENSITY(MC_playerBD[iLocalPart].iTeam, MC_ServerBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iTeam])
		iDebugGangChaseTotalSpawns = GET_GANG_CHASE_TOTAL_UNIT_SPAWNS(MC_playerBD[iLocalPart].iTeam, MC_ServerBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iTeam])
	ELSE
		iDebugGangChaseIntensity = 0
		iDebugGangChaseTotalSpawns = 0
	ENDIF
	
	iDebugGangChaseUnitDeadBS = 0
	FOR i = 0 TO ciMAX_GANG_CHASE_VEHICLES - 1
		IF IS_GANG_CHASE_UNIT_DEAD_GET_FIRST_ALIVE(i, niDebugGangChaseTargetEntity[i])
			SET_BIT(iDebugGangChaseUnitDeadBS, i)
		ENDIF
	ENDFOR
	
	#ENDIF
	
ENDPROC
