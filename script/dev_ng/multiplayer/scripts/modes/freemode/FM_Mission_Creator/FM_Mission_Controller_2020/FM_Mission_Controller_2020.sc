// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### System: GTA V Online Mission Controller ---------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Script: FM_Mission_Controller_2020.sc -----------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### This script was forked from FM_Mission_Controller.sc at the beginning of 2020
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description:
// ##### Runs all instanced content created with the Mission Creator.
// ##### Runs pre-game initialisation and calls the main state machine update.
// ##### No additional functionality should be added here!
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
USING "FM_Mission_Controller_Main_2020.sch"

SCRIPT(MP_MISSION_DATA fmmcMissionData)
	
	#IF IS_DEBUG_BUILD
		INITIALISE_DEBUG()
	#ENDIF
	
	//All script initialisation prior to the main loop below
	IF NOT PROCESS_PRE_GAME(fmmcMissionData) 
		PRINTLN("FM_Mission_Controller_2020 - Failed to receive initial network broadcast. Cleaning up")
		MC_SCRIPT_CLEANUP(FALSE)
	ENDIF
	
	INIT_TEMP_FUNCTION_CALLBACKS()
	
	WHILE TRUE	
		
		//The only wait in the script.
		MP_LOOP_WAIT_ZERO()
		
		//Start Overall Profiling
		#IF IS_DEBUG_BUILD
		START_MISSION_CONTROLLER_PROFILING(ciFMMC_PROFILER_OVERALL)
		#ENDIF
		
		//Check if we need to bail before continuing processing.
		PROCESS_PRE_FRAME_BAIL_CHECKS()
		
		//Cache heavily used natives to variables
		PROCESS_PRE_FRAME_NATIVE_CACHING()
		
		//Process all events received this frame.
		PROCESS_EVENTS()
		
		//Client Processing
		PROCESS_CLIENT()
		
		//Server Processing
		PROCESS_SERVER()
		
		//End Overall Profiling
		#IF IS_DEBUG_BUILD
		PAUSE_MISSION_CONTROLLER_PROFILING(ciFMMC_PROFILER_OVERALL)
		#ENDIF
		
		//Debug Processing
		#IF IS_DEBUG_BUILD
			PROCESS_DEBUG()
		#ENDIF
		
	ENDWHILE
	
ENDSCRIPT
