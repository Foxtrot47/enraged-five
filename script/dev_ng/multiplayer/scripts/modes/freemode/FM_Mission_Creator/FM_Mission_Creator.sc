 #IF IS_FINAL_BUILD
script
endscript
#ENDIF

#IF IS_DEBUG_BUILD

USING "FM_Mission_Creator_Menu_Left.sch"
USING "FM_Mission_Creator_Menu_Right.sch"
USING "net_spawn.sch"

//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////              Script Clean UP	                //////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////

//Cleanup Mission Data
PROC SCRIPT_CLEANUP(BOOL bSetWorldActive)
 
	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("FM_Main_Menu")) = 0
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	ENDIF
	IF DOES_CAM_EXIST(sCamData.cam)
		DESTROY_CAM(sCamData.cam)
	ENDIF
	IF DOES_BLIP_EXIST(bPhotoBlip)
		REMOVE_BLIP(bPhotoBlip)
	ENDIF
	IF DOES_BLIP_EXIST(bCameraPanBlip)
		REMOVE_BLIP(bCameraPanBlip)
	ENDIF
	IF DOES_BLIP_EXIST(bCameraTriggerBlip)
		REMOVE_BLIP(bCameraTriggerBlip)
	ENDIF
	IF DOES_BLIP_EXIST(bCameraOutroBlip)
		REMOVE_BLIP(bCameraOutroBlip)
	ENDIF	
	IF DOES_BLIP_EXIST(bTeamCamPanBlip[0])
		REMOVE_BLIP(bTeamCamPanBlip[0])
	ENDIF
	IF DOES_BLIP_EXIST(bTeamCamPanBlip[1])
		REMOVE_BLIP(bTeamCamPanBlip[1])
	ENDIF
	IF DOES_BLIP_EXIST(bTeamCamPanBlip[2])
		REMOVE_BLIP(bTeamCamPanBlip[2])
	ENDIF
	IF DOES_BLIP_EXIST(bTeamCamPanBlip[3])
		REMOVE_BLIP(bTeamCamPanBlip[3])
	ENDIF
	IF IS_SELECTOR_DISABLED()
		enable_selector()
	ENDIF
	
	COMMON_FMMC_SCRIPT_CLEAN_UP(sPedStruct,	sVehStruct,	sWepStruct,	sObjStruct,sPropStruct,  sDynoPropStruct, sInvisibleObjects, sCurrentVarsStruct, sFMMCmenu, TRUE, bSetWorldActive)
	CLEANUP_ALL_CREATOR_INTERACTABLES(sInteractableCreationStruct)
	CLEANUP_ALL_CREATOR_TRAINS(sTrainCreationStruct)
	
	SET_CREATOR_AUDIO(FALSE, FALSE)
	iDoorSetupStage = 0	
	
	DELETE_BLIPS_AND_CHECKPOINTS()
	CLEANUP_MENU_ASSETS()
	
	DO_BLIMP_SIGNS(sBlimpSign, TRUE)
	
	#IF IS_DEBUG_BUILD
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(FALSE)
	#ENDIF
	INT i 
	FOR i = 0 TO (g_FMMC_STRUCT.iNumberOfSj -1)
		IF IS_BIT_SET(sCurrentVarsStruct.iStuntJumpBitSet, i)
			DELETE_STUNT_JUMP(sCurrentVarsStruct.sjsID[i])
			CLEAR_BIT(sCurrentVarsStruct.iStuntJumpBitSet, i)
		ENDIF
	ENDFOR
	CLEAR_CACHED_AVAILABLE_VEHICLE_COUNTS()
	DISABLE_STUNT_JUMP_SET(0)
	REMOVE_RELATIONSHIP_GROUPS(sRGH)
	
	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfDoors i
		RESET_DOOR_TO_WORLD_STATE(i, sCurrentVarsStruct, TRUE)
		IF NOT IS_SPECIAL_CASE_DOOR(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[i].mnDoorModel)
			IF IS_DOOR_REGISTERED_WITH_SYSTEM(sCurrentVarsStruct.iMissionControlledDoors[i])
				REMOVE_DOOR_FROM_SYSTEM(sCurrentVarsStruct.iMissionControlledDoors[i])
			ENDIF
		ENDIF
	ENDREPEAT
	
	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfWorldProps i
		LEGACY_CLEAR_WORLD_PROP_MODEL_SWAP(i)
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD 
	IF IS_ROCKSTAR_DEV() 
		SET_ALLOW_TILDE_CHARACTER_FROM_ONSCREEN_KEYBOARD(FALSE)
	ENDIF
	#ENDIF
	
	PRINTLN("TERMINATE THE MISSION CREATOR. WORLD ACTIVE = ", bSetWorldActive)
	
	//Cleanup IPLs
	IF IS_IPL_ACTIVE("hei_yacht_heist")
		REMOVE_IPL("hei_yacht_heist")
	ENDIF
	IF IS_IPL_ACTIVE("hei_yacht_heist_enginrm")
		REMOVE_IPL("hei_yacht_heist_enginrm")
	ENDIF
	IF IS_IPL_ACTIVE("hei_yacht_heist_Lounge")
		REMOVE_IPL("hei_yacht_heist_Lounge")
	ENDIF
	IF IS_IPL_ACTIVE("hei_yacht_heist_Bridge")
		REMOVE_IPL("hei_yacht_heist_Bridge")
	ENDIF
	IF IS_IPL_ACTIVE("hei_yacht_heist_Bar")
		REMOVE_IPL("hei_yacht_heist_Bar")
	ENDIF
	IF IS_IPL_ACTIVE("hei_yacht_heist_Bedrm")
		REMOVE_IPL("hei_yacht_heist_Bedrm")
	ENDIF
	IF IS_IPL_ACTIVE("hei_carrier")
		REMOVE_IPL("hei_carrier")
	ENDIF
	IF IS_IPL_ACTIVE("smboat")
		REMOVE_IPL("smboat")
	ENDIF
	IF IS_IPL_ACTIVE("lr_cs6_08_grave_open")
		REMOVE_IPL("lr_cs6_08_grave_open")
	ENDIF
	ARENA_CLEANUP(TRUE, DEFAULT, TRUE, TRUE)
		
	YachtData.iLocation = 34            // Paleto Bay
	YachtData.Appearance.iOption = 2    // The Aquaris
	YachtData.Appearance.iTint = 12     // Vintage
	YachtData.Appearance.iLighting = 5  // Blue Vivacious
	YachtData.Appearance.iRailing = 1   // Gold Fittings
	YachtData.Appearance.iFlag = -1     // no flag 
	DELETE_AND_CLEANUP_ASSETS_FOR_MISSION_CREATOR_YACHT(YachtData)
	HEIST_ISLAND_LOADING__UNLOAD_ISLAND_IPLS()
	UNLOAD_MISSION_ISLAND_CUSTOMISATION()
	
	REQUEST_IPL("CS1_02_cf_offmission")
	REQUEST_IPL("chemgrill_grp1")

	//Reset mission type
	g_FMMC_STRUCT.iMissionType = 0
	
	IF interiorCopsOld = NULL
		interiorCopsOld = GET_INTERIOR_AT_COORDS_WITH_TYPE( <<446.8809, -985.8868, 29.6895>>, "v_policehub")
	ENDIF
	
	IF interiorCopsOld <> NULL
		IF bOldinCopsInteriorDisabled
			DISABLE_INTERIOR(interiorCopsOld, FALSE)
			bOldinCopsInteriorDisabled = FALSE
		ENDIF
	ENDIF
	
	SET_ENTITY_PROOFS(PLAYER_PED_ID(), FALSE, FALSE, FALSE, FALSE, FALSE)
	
	RESET_SCRIPT_GFX_ALIGN()
	
	REMOVE_ALL_DUMMY_BLIPS()
	
	CLEANUP_CASINO_VAULT_DOOR(interiorCasinoVaultIndex)
	
	CLEANUP_CASINO_APARTMENT_SETUP(interiorCasinoApartIndex)
	
	CLEANUP_ALL_INTERIORS(g_ArenaInterior)
	
	CLEANUP_MISSION_FACTORIES(sMissionFactories)
	INT iYachtIndex
	FOR iYachtIndex = 0 TO ciYACHT_MAX_YACHTS - 1
		DELETE_AND_CLEANUP_ASSETS_FOR_MISSION_CREATOR_YACHT(sMissionYachtData[iYachtIndex])
	ENDFOR
	
	CLEAR_EXTRA_DESCRIPTION_DATA(sFMMCMenu)
	
	CASINO_HEIST_MISSION_CONFIGURATION_DATA sClear_Casinoheist
	g_sCasinoHeistMissionConfigData = sClear_Casinoheist
	PRINTLN("CLEARING g_sCasinoHeistMissionConfigData on exit of Mission Creator")
	
	BLOCK_BUILDING_CONTROLLER_ON_MISSION(FALSE)
	ENABLE_STADIUM_PROBES_THIS_FRAME(FALSE)
	SET_IN_ARENA_MODE(FALSE)
	SET_VEHICLE_COMBAT_MODE(FALSE)
	
	g_bFMMC_EnteredLegacyMissionCreator = FALSE
	g_Private_MultiplayerCreatorAllowIntro = FALSE
	g_Private_MultiplayerCreatorAllowDeathAndRespawning = FALSE
	
	TERMINATE_THIS_THREAD()
	
ENDPROC

PROC DRAW_MENU_SELECTION(INT iSelected)
	
	IF g_bShow_FMMC_mainMenu = TRUE
		EXIT
	ENDIF
	
	IF g_bShow_FMMC_rulesMenu
		EXIT
	ENDIF
	
	CLEAR_BIT(iHelpBitSet, biPickupEntityButton)
	CLEAR_BIT(iHelpBitSet, biDeleteEntityButton)
	CLEAR_BIT(iHelpBitSet, biRemovePedsButton)
	CLEAR_BIT(iHelpBitSet, biRemoveObjectsButton)
	CLEAR_BIT(iHelpBitSet, biRemoveVehButton)
	
	IF IS_BIT_SET(iLocalBitSet, biEntityInCorona)
		IF CAN_PICK_UP_PLACED_ENTITY(sCurrentVarsStruct, sFMMCmenu)
			SET_BIT(iHelpBitSet, biPickupEntityButton)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iLocalBitSet, biGotoPointInCorona)
	AND sFMMCMenu.iSelectedEntity = -1
		SET_BIT(iHelpBitSet, biPickupCoronaButton)
	ELSE
		CLEAR_BIT(iHelpBitSet, biPickupCoronaButton)
	ENDIF
	
	sCurrentVarsStruct.bSelectedAnEntity = FALSE
	IF IS_BIT_SET(iLocalBitSet, biEntityInCorona)
	AND sCurrentVarsStruct.iHoverEntityType != CREATION_TYPE_CHECKPOINT
	AND sCurrentVarsStruct.iHoverEntityType != CREATION_TYPE_PROPS
		sCurrentVarsStruct.bSelectedAnEntity = TRUE	
		
		IF CAN_DELETE_BUTTON_BE_PRESSED(sFMMCMenu, sCurrentVarsStruct)
			SET_BIT(iHelpBitSet, biDeleteEntityButton)
		ENDIF
	ENDIF
		
	IF sFMMCmenu.sActiveMenu = eFmmc_WARP_PORTALS_TRANSFORM_OPTIONS
	AND (GET_CREATOR_MENU_SELECTION(sFMMCmenu) = WARP_PORTAL_TRANSFORM_CATEGORY
	OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = WARP_PORTAL_TRANSFORM_TYPE)
		SET_BIT(iHelpBitSet, biDeleteEntityButton)	
	ENDIF
	
	IF sPedStruct.iSwitchingINT = CREATION_STAGE_PLACE
	OR sObjStruct.iSwitchingINT = CREATION_STAGE_PLACE
	OR sVehStruct.iSwitchingINT = CREATION_STAGE_PLACE
	OR sWepStruct.iSwitchingINT = CREATION_STAGE_PLACE
	OR sInteractableCreationStruct.iCreationState = CREATION_STAGE_PLACE
	OR GET_CURRENT_GENERIC_CREATION_STAGE() = CREATION_STAGE_PLACE
		SET_BIT(iHelpBitSet, biRotateCoronaButton)
	ELSE
		CLEAR_BIT(iHelpBitSet, biRotateCoronaButton)
	ENDIF
	
	IF IS_BIT_SET(iLocalBitSet, biEntityInCorona)		
		IF (sFMMCMenu.iSelectedEntity = -1)
			IF IS_ENTITY_A_VEHICLE(sCurrentVarsStruct.vCoronaHitEntity)
				VEHICLE_INDEX vehTemp
				vehTemp = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(sCurrentVarsStruct.vCoronaHitEntity)
				IF IS_VEHICLE_DRIVEABLE(vehTemp)
					INT i, iVeh = 0
					FOR i = 0 TO FMMC_MAX_VEHICLES - 1
						IF vehTemp = sVehStruct.veVehcile[i] 
							iVeh = i						
						ENDIF
					ENDFOR
					IF NOT IS_VEHICLE_EMPTY(vehTemp)
					AND sFMMCmenu.sActiveMenu != efMMC_CUTSCENE_ENTITIES
					AND sFMMCmenu.sActiveMenu != efMMC_CUTSCENE_MOCAP_ENTITIES
					AND sFMMCmenu.sActiveMenu != eFmmc_END_OF_MISSION_END_CUTSCENE_ENTITIES
						SET_BIT(iHelpBitSet, biRemovePedsButton)
					ELIF NOT CAN_VEHICLE_BE_ATTACHED_TO(sVehStruct, iVeh)
						SET_BIT(iHelpBitSet, biRemoveObjectsButton)
					ENDIF
				ENDIF
			ENDIF
			IF IS_ENTITY_AN_OBJECT(sCurrentVarsStruct.vCoronaHitEntity)
				OBJECT_INDEX objTemp
				objTemp = GET_OBJECT_INDEX_FROM_ENTITY_INDEX(sCurrentVarsStruct.vCoronaHitEntity)
				INT i, iObj = -1
				FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfObjects - 1
					IF objTemp = sObjStruct.oiObject[i] 
						iObj = i
					ENDIF
				ENDFOR
				
				IF iObj > -1
					IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectAttachmentPed != -1
						SET_BIT(iHelpBitSet, biRemoveObjectsButton)
					ELIF NOT CAN_OBJECT_BE_ATTACHED_TO(sVehStruct, sObjStruct, iObj)
						SET_BIT(iHelpBitSet, biRemoveVehButton)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	BOOL bRefresh
	IF iHelpBitSetOld != iHelpBitSet
		iHelpBitSetOld = iHelpBitSet
		bRefresh = TRUE
	ENDIF
	
	IF sFMMCMenu.bRuleMenuActive	 = FALSE
		IF sFMMCmenu.iCurrentMenuLength <= 10
			iTopItem = 0
		ENDIF
		SET_TOP_MENU_ITEM(iTopItem)
	ENDIF
	
	// Set the current item and draw menu each frame
	SET_CURRENT_MENU_ITEM(iSelected)
	
	IF sCurrentVarsStruct.bDisplayFailReason
		IF sCurrentVarsStruct.iFailDescriptionTimer > GET_GAME_TIMER() - 5000
			SET_CURRENT_MENU_ITEM_DESCRIPTION(sCurrentVarsStruct.sFailReason)			
		ELSE
			sCurrentVarsStruct.bDisplayFailReason = FALSE
		ENDIF
	ENDIF
	IF sCurrentVarsStruct.bDisplayFailReason = FALSE			
		IF IS_BIT_SET(sFMMCmenu.iPropBitset, ciFMMC_PROP_Position_Override)
		AND IS_BIT_SET(sFMMCmenu.iPropBitset, ciFMMC_PROP_Rotation_Override)
		AND (sFMMCmenu.sActiveMenu = eFmmc_PROP_BASE OR sFMMCmenu.sActiveMenu = eFmmc_PROP_ADVANCED_OPTIONS)
			SET_CURRENT_MENU_ITEM_DESCRIPTION("MC_H_OVRRPW", 0, MENU_ICON_ALERT)
		ELIF IS_BIT_SET(sFMMCmenu.iPropBitset, ciFMMC_PROP_Position_Override)
		AND (sFMMCmenu.sActiveMenu = eFmmc_PROP_BASE OR sFMMCmenu.sActiveMenu = eFmmc_PROP_ADVANCED_OPTIONS)
			SET_CURRENT_MENU_ITEM_DESCRIPTION("MC_H_OVRPW", 0, MENU_ICON_ALERT)
		ELIF IS_BIT_SET(sFMMCmenu.iPropBitset, ciFMMC_PROP_Rotation_Override)
		AND (sFMMCmenu.sActiveMenu = eFmmc_PROP_BASE OR sFMMCmenu.sActiveMenu = eFmmc_PROP_ADVANCED_OPTIONS)
			SET_CURRENT_MENU_ITEM_DESCRIPTION("MC_H_OVRRW", 0, MENU_ICON_ALERT)
			
		ELIF CURRENTLY_USING_FMMC_OVERRIDE_POSITION(sFMMCmenu)
		AND CURRENTLY_USING_FMMC_OVERRIDE_ROTATION(sFMMCmenu)
		AND sFMMCmenu.sActiveMenu = eFmmc_DYNOPROP_BASE
			SET_CURRENT_MENU_ITEM_DESCRIPTION("MC_H_OVRRDPW", 0, MENU_ICON_ALERT)
		ELIF CURRENTLY_USING_FMMC_OVERRIDE_POSITION(sFMMCmenu)
		AND sFMMCmenu.sActiveMenu = eFmmc_DYNOPROP_BASE
			SET_CURRENT_MENU_ITEM_DESCRIPTION("MC_H_OVRDPW", 0, MENU_ICON_ALERT)
		ELIF CURRENTLY_USING_FMMC_OVERRIDE_ROTATION(sFMMCmenu)
		AND sFMMCmenu.sActiveMenu = eFmmc_DYNOPROP_BASE
			SET_CURRENT_MENU_ITEM_DESCRIPTION("MC_H_OVRDRW", 0, MENU_ICON_ALERT)
			
		ELIF sCurrentVarsStruct.bDisplaySelected
		AND sFMMCmenu.iEntityCreation != CREATION_TYPE_NONE
			PROCESS_SHOWING_HIGHLIGHTED_ENTITY(sCurrentVarsStruct)
		ELIF sFMMCmenu.sActiveMenu = eFmmc_DOORS_BASE 
		AND sFMMCMenu.sCurrentDoor.mnDoorModel = DUMMY_MODEL_FOR_SCRIPT
			IF sFMMCMenu.iHighlightedEntity != -1
				SET_CURRENT_MENU_ITEM_DESCRIPTION("MC_H_DR_SEL", 0)
				ADD_CURRENT_MENU_ITEM_DESCRIPTION_INT(sFMMCMenu.iHighlightedEntity)
			ENDIF
			
		ELIF sFMMCmenu.sActiveMenu = eFmmc_DOORS_LEGACY
		AND sFMMCMenu.sCurrentDoor.mnDoorModel = DUMMY_MODEL_FOR_SCRIPT
			IF sFMMCMenu.iHighlightedEntity != -1
				SET_CURRENT_MENU_ITEM_DESCRIPTION("MC_H_DR_SEL", 0)
				ADD_CURRENT_MENU_ITEM_DESCRIPTION_INT(sFMMCMenu.iHighlightedEntity)
			ENDIF
			
		ELSE		
			TEXT_LABEL_23 tlDescription = GET_DESCRIPTION_AS_A_TEXT_LABEL(sFMMCmenu)
			IF NOT IS_STRING_NULL_OR_EMPTY(tlDescription)
				SET_CURRENT_MENU_ITEM_DESCRIPTION_AS_TEXT_LABEL(tlDescription, 0, MENU_ICON_DUMMY)
			ELSE
				SET_CURRENT_MENU_ITEM_DESCRIPTION(GET_MENU_ITEM_DESCRIPTION(), 0, DOES_THIS_DESCRIPTION_NEED_A_WARNING(GET_MENU_ITEM_DESCRIPTION()))				
			ENDIF
			
			SET_CURRENT_MENU_ITEM_DESCRIPTION_EXTRA(sFMMCMenu.sOptionDescriptionExtra)
			
			IF sCurrentVarsStruct.iSaveFailTeam != -1
			AND (GET_CREATOR_MENU_SELECTION(sFmmcmenu) = TOP_MENU_SAVE
				OR (GET_CREATOR_MENU_SELECTION(sFmmcmenu) = TOP_MENU_PUBLISH AND !g_FMMC_STRUCT.bMissionIsPublished))
				ADD_CURRENT_MENU_ITEM_DESCRIPTION_INT(sCurrentVarsStruct.iSaveFailTeam)
				ADD_CURRENT_MENU_ITEM_DESCRIPTION_INT(sCurrentVarsStruct.iSaveFailRule)
			ENDIF			
		ENDIF		
	ENDIF
	
	IF iSelectedTabOld != sFMMCmenu.iSelectedTab
		REFRESH_MENU(sFMMCmenu)
		iSelectedTabOld = sFMMCmenu.iSelectedTab
	ENDIF
	
	DRAW_MENU()		
	
	IF bshowRightmenu
		DRAW_RIGHT_SIDE_MISSION_ORDER_MENU(g_CreatorsSelDetails)
	ENDIF
	
	IF sFMMCMenu.bRuleMenuActive	 = FALSE
		iTopItem = GET_TOP_MENU_ITEM()
	ENDIF
	
	IF bRefresh
		sCurrentVarsStruct.bResetUpHelp = TRUE
	ENDIF
	
ENDPROC

PROC EDIT_OPTIONS_THAT_REQUIRE_FULL_EVERY_FRAME_INPUTS(INT &iUpOrDown)
	
	IF sFMMCmenu.sActiveMenu = eFmmc_DEV_ASSIST_BASE
		EDIT_DEV_ASSIST_MENU(iUpOrDown)
	ENDIF
		
	IF sFMMCmenu.sActiveMenu = eFmmc_DEV_LOAD_FROM_CONTENT_BASE
		EDIT_DEV_ASSIST_LOAD_FROM_CONTENT_BASE_MENU(iUpOrDown)
	ENDIF
			
	IF sFMMCmenu.sActiveMenu = eFmmc_ASS_OBJECTIVE_MENU_MAIN_POOL_GOTO_POS
	OR sFMMCmenu.sActiveMenu = eFmmc_ASS_OBJECTIVE_MENU_SET_POOL_GOTO_POS
		EDIT_ASSOCIATED_RULE_GOTO_POS_POOL_MAIN_MENU(sFMMCmenu, sFMMCendStage, iUpOrDown)
	ENDIF
	
	IF sFMMCmenu.sActiveMenu = eFmmc_END_OF_MISSION_END_CUTSCENE_MENU
		EDIT_END_CUTSCENE_COORDINATES_OPTIONS_MENU(sFMMCdata, sFMMCMenu, sFMMCendStage, sCurrentVarsStruct, iUpOrDown)
	ENDIF
	
	IF sFMMCmenu.sActiveMenu = eFmmc_ENTITY_SHARED_MODIFY_SPAWN_GROUP_ON_EVENT_SETTINGS
		EDIT_ENTITY_SHARED_MODIFY_SPAWN_GROUP_ON_EVENT_OPTIONS_MENU()
	ENDIF
	
	IF sFMMCmenu.sActiveMenu = eFmmc_ENTITY_SHARED_MODIFY_SUB_SPAWN_GROUP_ON_EVENT_SETTINGS
		EDIT_ENTITY_SHARED_MODIFY_SUB_SPAWN_GROUP_ON_EVENT_OPTIONS_MENU()	
	ENDIF
	
	IF sFMMCmenu.sActiveMenu = eFmmc_ENTITY_SHARED_SPAWN_GROUP_SETTINGS
		EDIT_ENTITY_SHARED_SPAWN_GROUP_OPTIONS_MENU()
	ENDIF
	
	IF sFMMCmenu.sActiveMenu = eFmmc_ENTITY_SHARED_SUB_SPAWN_GROUP_SETTINGS
		EDIT_ENTITY_SHARED_SUB_SPAWN_GROUP_OPTIONS_MENU()
	ENDIF
	
	IF sFMMCmenu.sActiveMenu = eFmmc_ENTITY_SHARED_SPAWN_BLOCKING_FLAG_SETTINGS
		EDIT_ENTITY_SHARED_SPAWN_CONDITION_FLAGS_OPTIONS_MENU(iUpOrDown)
	ENDIF		
	
	IF sFMMCmenu.sActiveMenu =  eFmmc_DEV_PROOFS_OPTIONS
		EDIT_DEV_PLAYER_PROOFS_OPTIONS_MENU(sFMMCMenu, sFMMCendStage, sCurrentVarsStruct, iUpOrDown)
	ENDIF
	
	IF sFMMCmenu.sActiveMenu = eFmmc_ROAMING_SPECTATOR_OPTIONS
		EDIT_ROAMING_SPECTATOR_OPTIONS_MENU(sFMMCmenu, sFMMCendStage, sCurrentVarsStruct, iUpOrDown)
	ENDIF
	
	IF sFMMCmenu.sActiveMenu = eFmmc_GLOBAL_GANG_CHASE_CONFIG_OPTIONS
		EDIT_GLOBAL_GANG_CHASE_CONFIG_OPTIONS_MENU(sFMMCmenu, sFMMCendStage, iUpOrDown)
	ENDIF
	
	IF sFMMCmenu.sActiveMenu = eFmmc_ENT_DMG_REL_GROUP_VEH
		EDIT_ENTITY_DAMAGE_RELGRP_OPTIONS_MENU(sFMMCmenu, sFMMCendStage, iUpOrDown)
	ENDIF
	
	IF sFMMCmenu.sActiveMenu = eFMMC_CUTSCENE_ANIM_SYNC_SCENE_OPTIONS
		EDIT_ANIM_SYNC_SCENE_OPTIONS_MENU(sFMMCMenu, sFMMCendSTage, sCurrentVarsStruct, iUpOrDown)
	ENDIF
	
	IF sFMMCmenu.sActiveMenu = eFmmc_WARP_PORTALS_BASE
		EDIT_WARP_PORTAL_BASE_MENU(sFMMCmenu, sFMMCendStage, sCurrentVarsStruct, iUpOrDown)
	ENDIF
	
	IF sFMMCmenu.sActiveMenu = eFmmc_WARP_PORTALS_PLACEMENTS_OPTIONS
		EDIT_WARP_PORTAL_PLACEMENTS_OPTIONS_MENU(sFMMCmenu, sFMMCendStage, sCurrentVarsStruct,  sPedStruct, sVehStruct, iUpOrDown)
	ENDIF
	
	IF sFMMCmenu.sActiveMenu = eFmmc_WARP_PORTALS_ADDITIONAL_OPTIONS
		EDIT_WARP_PORTAL_ADDITIONAL_WARP_OPTIONS_MENU(sFMMCmenu, sFMMCendStage, sCurrentVarsStruct, iUpOrDown)
	ENDIF
	
	IF sFMMCmenu.sActiveMenu = eFmmc_WARP_PORTALS_TRANSFORM_OPTIONS
		EDIT_WARP_PORTAL_TRANSFORM_WARP_OPTIONS_MENU(sFMMCmenu, sFMMCendStage, sCurrentVarsStruct, iUpOrDown)
	ENDIF
	
	IF sFMMCmenu.sActiveMenu = eFmmc_PLACED_MARKERS_BASE
		EDIT_PLACED_MARKERS_BASE_MENU(sFMMCmenu, sFMMCendStage, sCurrentVarsStruct, iUpOrDown)
	ENDIF
	
	IF sFMMCmenu.sActiveMenu = eFmmc_PTFX_BASE
		EDIT_PTFX_BASE_MENU(sFMMCmenu, sFMMCendStage, sCurrentVarsStruct, iUpOrDown)
	ENDIF
	
	IF sFMMCmenu.sActiveMenu = eFmmc_END_CUTSCENE_WARP_LOCATIONS
		EDIT_END_CUTSCENE_WARP_LOCATION_OPTIONS_MENU(iUpOrDown)
	ENDIF
	
	IF sFMMCmenu.sActiveMenu = eFmmc_WARP_LOCATION_SETTINGS
		EDIT_WARP_LOCATION_SETTINGS_MENU(iUpOrDown)
	ENDIF
	
	IF sFMMCmenu.sActiveMenu = eFmmc_VEH_WARP_LOCATIONS
		EDIT_VEH_WARP_LOCATION_OPTIONS_MENU(iUpOrDown)
	ENDIF
	
	IF sFMMCmenu.sActiveMenu = eFmmc_LOC_WARP_LOCATIONS
		EDIT_LOC_WARP_LOCATION_OPTIONS_MENU(iUpOrDown)
	ENDIF
	
	IF sFMMCmenu.sActiveMenu = eFmmc_PED_WARP_LOCATIONS
		EDIT_PED_WARP_LOCATION_OPTIONS_MENU(iUpOrDown)
	ENDIF
	
	IF sFMMCmenu.sActiveMenu = eFmmc_OBJ_WARP_LOCATIONS
		EDIT_OBJ_WARP_LOCATION_OPTIONS_MENU(iUpOrDown)
	ENDIF
	
	IF sFMMCmenu.sActiveMenu = eFmmc_INTERACTABLE_WARP_LOCATIONS
		EDIT_INTERACTABLE_WARP_LOCATION_OPTIONS_MENU(iUpOrDown)
	ENDIF
	
	IF sFMMCmenu.sActiveMenu = eFmmc_SURVIVAL_Wave_Base
		EDIT_SURVIVAL_WAVE_BASE_MENU(sFMMCendStage, sFMMCmenu)
	ENDIF
	
	IF sFMMCmenu.sActiveMenu = eFmmc_SURVIVAL_Veh_Config_base
		EDIT_SURVIVAL_VEH_BASE_MENU(sFMMCendStage, sFMMCmenu)
	ENDIF
	
	IF sFMMCmenu.sActiveMenu = eFmmc_ENEMY_DISABLE_PED_BEING_TARGETABLE_ON_RULE_OPTIONS
		EDIT_DISABLE_PED_IS_TARGETABLE_OPTIONS_MENU(iUpOrDown)
	ENDIF
	
	IF sFMMCmenu.sActiveMenu = eFmmc_ENEMY_ANIMATIONS_MENU
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_ANIMS_MENU_IDLE_ANIM_BREAKOUT_RANGE
			DRAW_MARKER_SPHERE(sCurrentVarsStruct.vCoronaPos, sFMMCMenu.fAnimBreakoutRange, 250, 250, 250, 0.2)
		ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_ANIMS_MENU_SPECIAL_ANIM_BREAKOUT_RANGE
			DRAW_MARKER_SPHERE(sCurrentVarsStruct.vCoronaPos, sFMMCMenu.fAnimSpecialBreakoutRange, 250, 250, 250, 0.2)
		ENDIF
	ENDIF
	
	IF sFMMCmenu.sActiveMenu =  eFMMC_CUTSCENE_WARP_ENTITY_LOCATIONS
		HANDLE_CUTSCENE_WARP_ENTITY_OPTIONS(sFMMCmenu, sFMMCendStage, sCurrentVarsStruct, iUpOrDown, g_FMMC_STRUCT.sCurrentSceneData.sCutsceneEntityWarpData)
	ENDIF
	
	IF sFMMCmenu.sActiveMenu =  eFMMC_CUTSCENE_MOCAP_WARP_ENTITY_LOCATION
		HANDLE_CUTSCENE_WARP_ENTITY_OPTIONS(sFMMCmenu, sFMMCendStage, sCurrentVarsStruct, iUpOrDown, g_FMMC_STRUCT.sCurrentMocapSceneData.sCutsceneEntityWarpData)
	ENDIF
	
	// Force a call of EDIT_MENU_OPTIONS
	IF (GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_SAS_LOAD_INDEX OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_SAS_SAVE_INDEX)
	AND sFMMCmenu.sActiveMenu = eFmmc_PED_StealthSystem
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
			iUpOrDown = 1
		ENDIF
	ENDIF
	
	IF sFMMCmenu.sActiveMenu = eFmmc_DIALOGUE_TRIGGER_BASE
	AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DIALOGUE_TRIGGER_RESET_TRIGGER_OPTIONS
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
			RESET_DIALOGUE_TRIGGER_MENU_VALUES(sFMMCmenu)
			REFRESH_MENU(sFMMCmenu)
		ENDIF
	ENDIF
	
#IF IS_DEBUG_BUILD
	IF sFMMCmenu.sActiveMenu = eFmmc_ACTORS_CUSTOM_VARIATION_BASE
	AND iUpOrDown = 0
	AND IS_BIT_SET(sFMMCMenu.sPedVariationSettings.iPedVariationBS, ciPV_USING_CUSTOM_VARIATION)
	AND DOES_ENTITY_EXIST(sPedStruct.piTempPed)
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) >= PED_CUSTOM_PROP_ANCHOR
		AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) <= PED_CUSTOM_PROP_TEXTURE
			FOCUS_CAMERA_ON_CURRENT_CUSTOM_PED_COMPONENT_OR_PROP(sFMMCMenu, sPedStruct.piTempPed, FALSE, TRUE)
		ELSE
			FOCUS_CAMERA_ON_CURRENT_CUSTOM_PED_COMPONENT_OR_PROP(sFMMCMenu, sPedStruct.piTempPed, FALSE, FALSE)
		ENDIF
	ENDIF
#ENDIF
	
ENDPROC

FUNC enumFMMC_CIRCLE_MENU_ACTIONS RETURN_CIRCLE_MENU_SELECTIONS()

	IF g_bShow_FMMC_mainMenu = TRUE
	OR g_bShow_FMMC_rulesMenu = TRUE
		PRINTLN("[Creator] Not processing RETURN_CIRCLE_MENU_SELECTIONS because we're in the main menu or the rules menu")		
		RETURN eFmmc_Action_Null
	ENDIF
	
	IF NOT bshowmenu
		PRINTLN("[Creator] Not processing RETURN_CIRCLE_MENU_SELECTIONS because bshowmenu is FALSE")			
		RETURN eFmmc_Action_Null
	ENDIF
	
	IF LOAD_MENU_ASSETS()
		DRAW_MENU_SELECTION(GET_CREATOR_MENU_SELECTION(sFMMCmenu))
		
		IF sFMMCmenu.sActiveMenu = eFMMC_Inventory_Weapons
		OR sFMMCmenu.sActiveMenu = eFMMC_Inventory_Component_List
			IF sWepStruct.iSelectedWeaponType != -1
			AND sFMMCmenu.iWepLibrary != FMMC_WEAPON_LIBRARY_SPECIAL
				WEAPON_TYPE wtWep = g_FMMC_STRUCT.sPlayerWeaponInventories[sFMMCMenu.iCurrentInventory].sWeaponStruct[sFMMCMenu.iCurrentInventoryWeapon].wtWeapon
				RENDER_WEAPON_STATS(sFMMCmenu, wtWep, sWeaponValues, sModValues, TRUE)
			ENDIF
		ENDIF
	ENDIF
	
	INT iPedToChange, i
	
	BOOL bSubmenuActive = NOT IS_MENU_A_RESET_MENU(sFMMCmenu.sMenuBack)
	
	IF sFMMCmenu.sActiveMenu = eFmmc_ASS_OBJECTIVE_MENU
	OR sFMMCmenu.sActiveMenu = eFmmc_ASS_OBJECTIVE_MENU_SET_PED_GOTO_POS	
	OR sFMMCmenu.sActiveMenu = eFMMC_ASS_OBJECTIVE_EXIT_VEHICLE_OPTIONS
	OR sFMMCmenu.sActiveMenu = eFMMC_ASS_OBJECTIVE_COVER_ONLY
	OR sFMMCmenu.sActiveMenu = eFMMC_ASS_OBJECTIVE_USE_COMBAT_GOTO
	OR sFMMCmenu.sActiveMenu = eFMMC_ASS_OBJECTIVE_FORCE_NAVMESH
	OR sFMMCmenu.sActiveMenu = eFMMC_ASS_OBJECTIVE_VEHICLE_DISTANCE
	OR sFMMCmenu.sActiveMenu = eFMMC_ASS_OBJECTIVE_WAIT_RULE_OPTIONS
	OR sFMMCmenu.sActiveMenu = eFMMC_ASS_OBJECTIVE_WAIT_TIME_OPTIONS
	OR sFMMCmenu.sActiveMenu = eFMMC_ASS_OBJECTIVE_PLANE_TAXI
	OR sFMMCmenu.sActiveMenu = eFMMC_ASS_OBJECTIVE_SLOW_AT_DISTANCE
	OR sFMMCmenu.sActiveMenu = eFMMC_ASS_OBJECTIVE_ACHIEVE_FINAL_HEADING
	OR sFMMCmenu.sActiveMenu = eFMMC_ASS_OBJECTIVE_TRIGGER_AREA
	OR sFMMCmenu.sActiveMenu = EFMMC_ASS_OBJECTIVE_SPEED_OPTIONS
	OR sFMMCmenu.sActiveMenu = EFMMC_ASS_OBJECTIVE_RADIUS_OPTIONS
	OR sFMMCmenu.sActiveMenu = eFMMC_ASS_OBJECTIVE_TARGET_OVERRIDE
	OR sFMMCmenu.sActiveMenu = eFmmc_ASS_OBJECTIVE_SECONDARY_GOTO_MENU
	OR sFMMCmenu.sActiveMenu = eFmmc_ASS_OBJECTIVE_MENU_TEAM_SETTINGS
	OR sFMMCmenu.sActiveMenu = eFMMC_ASS_OBJECTIVE_IGNORE_NAVMESH
	OR sFMMCmenu.sActiveMenu = eFMMC_ASS_OBJECTIVE_SPECIFIC_VEH_SETTINGS
	OR sFMMCmenu.sActiveMenu = eFMMC_IGNORE_PROXIMITY
	OR sFMMCmenu.sActiveMenu = eFmmc_VEHICLE_BLIP_OPTIONS
	OR sFMMCmenu.sActiveMenu = eFmmc_VEHICLE_SPAWN_OPTIONS
	OR sFMMCmenu.sActiveMenu = eFmmc_VEHICLE_CAPTURE_OPTIONS
	OR sFMMCmenu.sActiveMenu = eFmmc_VEHICLE_CARGOBOB_OPTIONS
	OR sFMMCmenu.sActiveMenu = eFmmc_VEHICLE_WEAPON_MISC_OPTIONS
	OR sFMMCmenu.sActiveMenu = eFmmc_VEHICLE_DOOR_MENU
	OR sFMMCmenu.sActiveMenu = eFmmc_VEHICLE_WINDOW_MENU
	OR sFMMCmenu.sActiveMenu = eFmmc_VEHICLE_DOOR_OPEN_CLOSED_MENU
	OR sFMMCmenu.sActiveMenu = eFmmc_VEHICLE_DOOR_BREAKABLE_MENU
	OR sFMMCmenu.sActiveMenu = eFmmc_VEHICLE_DOOR_DELAYED_OPEN_MENU
	OR sFMMCmenu.sActiveMenu = eFmmc_VEHICLE_EXTRAS
	OR sFMMCmenu.sActiveMenu = eFmmc_MISSION_CRITICAL_MENU
	OR sFMMCmenu.sActiveMenu = eFmmc_GO_TO_CONSEQUENCES
	OR sFMMCmenu.sActiveMenu = eFmmc_ACTOR_DIALOGUE_MENU
	OR sFMMCmenu.sActiveMenu = eFmmc_PED_AGGRO_RULE
	OR sFMMCmenu.sActiveMenu = eFmmc_PED_TEAM_AGGRO_SETTINGS
	OR sFMMCmenu.sActiveMenu = eFMMC_ENTITY_PROOFS_MENU
	OR sFMMCmenu.sActiveMenu = eFmmc_TEAM_REL_GROUPS
	OR sFMMCmenu.sActiveMenu = eFmmc_ENEMY_BASE
	OR sFMMCmenu.sActiveMenu = eFmmc_VEHICLES_BASE
	OR sFMMCmenu.sActiveMenu = eFmmc_DIALOGUE_TRIGGER_BASE
	OR sFMMCmenu.sActiveMenu = eFmmc_OBJECT_BASE
	OR sFMMCmenu.sActiveMenu = eFmmc_JUMP_TO_OBJECTIVE_LOCATION_MENU
	OR sFMMCmenu.sActiveMenu = eFmmc_JUMP_TO_OBJECTIVE_OBJECT_MENU
	OR sFMMCmenu.sActiveMenu = eFmmc_JUMP_TO_OBJECTIVE_PED_MENU
	OR sFMMCmenu.sActiveMenu = eFmmc_JUMP_TO_OBJECTIVE_PLAYER_RULES_MENU
	OR sFMMCmenu.sActiveMenu = eFmmc_JUMP_TO_OBJECTIVE_VEHICLE_MENU
	OR sFMMCmenu.sActiveMenu = eFMMC_MISSION_BRANCHING
	OR sFMMCmenu.sActiveMenu = eFMMC_SPAWN_NEAR_ENTITY
	OR sFMMCmenu.sActiveMenu = eFMMC_PROXIMITY_SPAWNING_OPTIONS
	OR sFMMCmenu.sActiveMenu = eFmmc_ALL_TEAM_AT_LOCATE_MENU
	OR sFMMCmenu.sActiveMenu = eFmmc_LINE_MARKERS_MENU
	OR sFMMCmenu.sActiveMenu = eFMMC_GENERIC_OVERRIDE_POSITION
	OR sFMMCmenu.sActiveMenu = eFMMC_PROP_BMB_OPTIONS
	OR sFMMCmenu.sActiveMenu = eFmmc_GENERIC_OVERRIDE_ROTATION
	OR sFMMCmenu.sActiveMenu = eFmmc_ENEMY_STOP_TASKS_MENU
	OR sFMMCmenu.sActiveMenu = eFmmc_PICKUP_BASE
	OR sFMMCmenu.sActiveMenu = eFmmc_VEHICLE_DELUXO_OPTIONS
	OR sFMMCmenu.sActiveMenu = eFmmc_MANIPULATE_TIME_REMAINING_OPTIONS
	OR sFMMCmenu.sActiveMenu = eFmmc_ENTITY_SHARED_SPAWN_SETTINGS
	OR sFMMCmenu.sActiveMenu = eFmmc_ENTITY_SHARED_SPAWN_GROUP_SETTINGS
	OR sFMMCmenu.sActiveMenu = eFmmc_ENTITY_SHARED_SUB_SPAWN_GROUP_SETTINGS
	OR sFMMCmenu.sActiveMenu = eFmmc_ENEMY_DISABLE_PED_BEING_TARGETABLE_ON_RULE_OPTIONS
	OR sFMMCmenu.sActiveMenu = eFmmc_VEHICLE_KINEMATIC_PHYSICS_OPTIONS
	OR sFMMCmenu.sActiveMenu = eFmmc_ENTITY_SHARED_MODIFY_SPAWN_GROUP_ON_EVENT_SETTINGS
	OR sFMMCmenu.sActiveMenu = eFmmc_ENTITY_SHARED_MODIFY_SUB_SPAWN_GROUP_ON_EVENT_SETTINGS
	OR sFMMCmenu.sActiveMenu = eFmmc_ENTITY_SHARED_SPAWN_BLOCKING_FLAG_SETTINGS
	OR sFMMCmenu.sActiveMenu = eFmmc_ENTITY_CLEAR_DATA
	OR sFMMCmenu.sActiveMenu = eFmmc_VEHICLE_RADIO	
	OR IS_MENU_A_CUSTOM_VARIABLE_MENU(sFMMCmenu.sActiveMenu)
	OR IS_MENU_A_ENTITY_PTFX_MENU(sFMMCmenu)
		iPedToChange = sFMMCMenu.iSelectedEntity
		IF sFMMCMenu.iSelectedEntity = -1
			IF sFMMCmenu.iEntityCreation = CREATION_TYPE_PEDS
				iPedToChange = g_FMMC_STRUCT_ENTITIES.iNumberOfPeds
			ELIF sFMMCmenu.iEntityCreation = CREATION_TYPE_VEHICLES
				iPedToChange = g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles
			ELIF sFMMCmenu.iEntityCreation = CREATION_TYPE_OBJECTS
				iPedToChange = g_FMMC_STRUCT_ENTITIES.iNumberOfObjects
			ELIF sFMMCmenu.iEntityCreation = CREATION_TYPE_PROPS
				iPedToChange = g_FMMC_STRUCT_ENTITIES.iNumberOfProps
			ELIF sFMMCmenu.iEntityCreation = CREATION_TYPE_DYNOPROPS
				iPedToChange = g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps
			ELIF sFMMCmenu.iEntityCreation = CREATION_TYPE_DIALOGUE_TRIGGER
				iPedToChange = g_FMMC_STRUCT.iNumberOfDialogueTriggers
			ELIF sFMMCmenu.iEntityCreation = CREATION_TYPE_DUMMY_BLIPS
				iPedToChange = g_FMMC_STRUCT_ENTITIES.iNumberOfDummyBlips
			ELIF sFMMCmenu.iEntityCreation = CREATION_TYPE_WEAPONS
				iPedToChange = g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons
			ELIF sFMMCmenu.iEntityCreation = CREATION_TYPE_INTERACTABLE
				iPedToChange = g_FMMC_STRUCT_ENTITIES.iNumberOfInteractables
			ELIF sFMMCmenu.iEntityCreation = CREATION_TYPE_TRAINS
				iPedToChange = g_FMMC_STRUCT_ENTITIES.iNumberOfTrains
			ELIF sFMMCmenu.iEntityCreation = CREATION_TYPE_FMMC_ZONE
				iPedToChange = g_FMMC_STRUCT_ENTITIES.iNumberOfZones
			ELIF sFMMCmenu.iEntityCreation = CREATION_TYPE_DEV_ASSIST
				iPedToChange = g_FMMC_STRUCT_ENTITIES.iNumberOfDevAssist
			ENDIF
		ENDIF
	ENDIF
	
	INT iFlag
	FOR iFlag = 0 TO ciSPAWN_CONDITION_FLAGS_MAX-1
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
		AND sFMMCMenu.sMenuBack = eFmmc_MAIN_MENU_BASE
			sFMMCmenu.eSpawnConditionFlag[iFlag] = SPAWN_CONDITION_FLAG_NOT_SET
		ENDIF
	ENDFOR
		
	IF JUST_ENTERED_MENU(sFMMCMenu)
		PRINTLN("Just entered a new menu!")		
		IF sFMMCmenu.sActiveMenu = eFmmc_CUSTOM_WEAPON_SELECTED_POOL
		AND sFMMCMenu.iSelectedCustomWeapon != -1
			PRINTLN("Refresh Libary and Type for weapon menu!")
			SET_WEAPON_LIBRARY_AND_TYPE_FROM_WEAPON_TYPE(sFMMCmenu, sWepStruct, g_FMMC_STRUCT.sCustomVariables.sWeaponPool[sFMMCMenu.iSelectedCustomWeapon].wt_Weapon)			
			REFRESH_MENU(sFMMCmenu)
		ENDIF
	ENDIF
	
	IF sFMMCMenu.bRuleMenuActive	 = FALSE
		
		IF IS_BIT_SET(sFMMCmenu.iFMMCMenuBitset2, ciFMMCmenuBS2_ClearSelectedEntityAssociatedGotoData)
			CLEAR_ALL_PED_ASSOCIATED_GOTO_DATA(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedToChange].sAssociatedGotoTaskData)
			CLEAR_BIT(sFMMCmenu.iFMMCMenuBitset2, ciFMMCmenuBS2_ClearSelectedEntityAssociatedGotoData)
		ENDIF
		
		IF MANAGE_PED_ASS_OBJ_GOTO_LOCATIONS(sFMMCmenu.sAssociatedGotoTaskData)
			
			IF sFMMCmenu.sActiveMenu = eFmmc_ASS_OBJECTIVE_MENU_SET_POOL_GOTO_POS
				INT iPoolIndex = sFMMCmenu.iSelectedEntity	
				IF iPoolIndex = -1
				OR iPoolIndex >= ASSOCIATED_GOTO_TASK_POOL
					iPoolIndex = 0
				ENDIF
				COPY_ASSOCIATED_GOTO_TASK_DATA(g_FMMC_STRUCT.sAssociatedGotoPool[iPoolIndex].sAssociatedGotoTaskData, sFMMCmenu.sAssociatedGotoTaskData)
			ELSE
				COPY_ASSOCIATED_GOTO_TASK_DATA(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedToChange].sAssociatedGotoTaskData, sFMMCmenu.sAssociatedGotoTaskData)
			ENDIF
			
			REFRESH_MENU(sFMMCmenu)
			
		ELIF NOT MANAGE_PED_ASS_OBJ_SECONDARY_GOTO(iPedToChange) 
		
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_X)
				
				IF sFMMCmenu.sActiveMenu = eFMMC_SHRINKING_SUDDEN_DEATH_OPTIONS
					IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.vSphereSpawnPoint)
						SET_ENTITY_COORDS(PLAYER_PED_ID(), g_FMMC_STRUCT.vSphereSpawnPoint + <<0,0,20>>)
					ENDIF
				ENDIF
				
				IF sFMMCmenu.sActiveMenu = eFmmc_TEAM_RESPAWN_OPTIONS
				AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_RESPAWN_OPT_RESPAWN_VEHICLE
					g_FMMC_STRUCT.mnVehicleModel[sFMMCMenu.iSelectedTeam] = DUMMY_MODEL_FOR_SCRIPT
				ENDIF
				
				IF sFMMCmenu.sActiveMenu = efMMC_CUTSCENE_ADD_SCENE
					IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) > ADD_CUTSCENE_MENU_ADD_SHOT
						INT iDeletedShot = GET_CREATOR_MENU_SELECTION(sFMMCmenu) - (ADD_CUTSCENE_MENU_ADD_SHOT+1)
						FOR i = iDeletedShot TO g_fmmc_struct.sCurrentSceneData.iNumCamShots-1
							CLEAR_SHOT_DATA(g_fmmc_struct.sCurrentSceneData, i)
							IF i < g_fmmc_struct.sCurrentSceneData.iNumCamShots-1
								COPY_SHOT_DATA(g_fmmc_struct.sCurrentSceneData, i+1, i)
							ENDIF
						ENDFOR
						g_FMMC_STRUCT.sCurrentSceneData.iNumCamShots = CLAMP_INT(g_fmmc_struct.sCurrentSceneData.iNumCamShots-1, 0, MAX_CAMERA_SHOTS)
						sFMMCmenu.iCurrentShot = 0
						SET_CREATOR_MENU_SELECTION(sFMMCmenu, GET_CREATOR_MENU_SELECTION(sFMMCmenu)-1)
					ENDIF
				ENDIF
											
				IF sFMMCmenu.sActiveMenu = efMMC_CUTSCENE_STARTS
					FOR i = 0 TO FMMC_MAX_TEAMS-1
						IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = i
							g_FMMC_STRUCT.sCurrentSceneData.vPlayerStartPos[i] = <<0,0,0>>
						ENDIF
					ENDFOR
				ENDIF
				
				IF sFMMCmenu.sActiveMenu = efMMC_CUTSCENE_ENDS
					FOR i = 0 TO FMMC_MAX_TEAMS-1
						IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = i
							g_FMMC_STRUCT.sCurrentSceneData.vPlayerEndPos[i] = <<0,0,0>>							
						ENDIF
						
						IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = i + CUTSCENE_END_POSITIONS_ALT_POS_0
							g_FMMC_STRUCT.sCurrentSceneData.vPlayerAltEndPos[i] = <<0,0,0>>
						ENDIF
					ENDFOR	
					
					IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = CUTSCENE_END_POSITIONS_ALT_POS_CONDITION
						g_FMMC_STRUCT.sCurrentSceneData.eUseAltEndPos = USE_ALT_END_POS_DISABLED
					ENDIF
				ENDIF
				
				IF sFMMCmenu.sActiveMenu = efMMC_CUTSCENE_MOCAP_STARTS
					FOR i = 0 TO FMMC_MAX_TEAMS-1
						IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = i
							g_FMMC_STRUCT.sCurrentMocapSceneData.vPlayerStartPos[i] = <<0,0,0>>
						ENDIF
					ENDFOR
				ENDIF
				
				IF sFMMCmenu.sActiveMenu = efMMC_CUTSCENE_MOCAP_ENDS
					FOR i = 0 TO FMMC_MAX_TEAMS-1
						IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = i
							g_FMMC_STRUCT.sCurrentMocapSceneData.vPlayerEndPos[i] = <<0,0,0>>						
						ENDIF
						
						IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = i + CUTSCENE_END_POSITIONS_ALT_POS_0
							g_FMMC_STRUCT.sCurrentMocapSceneData.vPlayerAltEndPos[i] = <<0,0,0>>
						ENDIF						
					ENDFOR
					
					IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = CUTSCENE_END_POSITIONS_ALT_POS_CONDITION
						g_FMMC_STRUCT.sCurrentMocapSceneData.eUseAltEndPos = USE_ALT_END_POS_DISABLED
					ENDIF
				ENDIF
				
				IF sFMMCmenu.sActiveMenu = efMMC_CUTSCENE_BASE
				
					INT iSlotToDelete
					
					IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) > 0 
					AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) < FMMC_GET_MAX_SCRIPTED_CUTSCENES() + 1
						iSlotToDelete = CLAMP_INT(GET_CREATOR_MENU_SELECTION(sFMMCmenu) - 1, 0, FMMC_GET_MAX_SCRIPTED_CUTSCENES() - 1)
						CLEAR_CUTSCENE_DATA(g_FMMC_STRUCT.sScriptedCutsceneData[iSlotToDelete])
					ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) > FMMC_GET_MAX_SCRIPTED_CUTSCENES() + 1
						iSlotToDelete = CLAMP_INT(GET_CREATOR_MENU_SELECTION(sFMMCmenu) - (FMMC_GET_MAX_SCRIPTED_CUTSCENES() + 2), 0, MAX_MOCAP_CUTSCENES - 1)
						CLEAR_MOCAP_CUTSCENE_DATA(g_FMMC_STRUCT.sMocapCutsceneData[iSlotToDelete])
					ENDIF
				ENDIF
				
				IF sFMMCmenu.sActiveMenu = eFmmc_PLANNING_MISSIONS
					INT iIndex = GET_PLANNING_MISSION_INDEX_FROM_MENU_SELECTION()
					IF iIndex != -1
						CLEAR_PLANNING_MISSION(g_FMMC_STRUCT.sPlanningMissions[iIndex])
						SORT_PLANNING_MISSIONS()
						SET_CREATOR_MENU_SELECTION(sFMMCmenu, GET_CREATOR_MENU_SELECTION(sFMMCmenu)-1)
					ENDIF
				ENDIF
				
				IF sFMMCmenu.sActiveMenu = eFmmc_NEXT_MISSION_SETTINGS
					IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = NEXT_MISSION_SETTINGS_CONTENT_ID
						g_FMMC_STRUCT.tl23NextContentID[sFMMCmenu.iNextMissionIndex] = ""
						REFRESH_MENU(sFMMCmenu)
					ENDIF
				ENDIF
								
				IF sFMMCmenu.sActiveMenu = eFmmc_NEXT_MISSION
					INT iIndex = GET_CREATOR_MENU_SELECTION(sFMMCmenu)
					IF iIndex > -1 
					AND iIndex < FMMC_MAX_STRAND_MISSIONS
						g_FMMC_STRUCT.tl23NextContentID[iIndex] = ""
						g_FMMC_STRUCT.eBranchingTransitionType[iIndex] = FMMC_BRANCHING_MISSION_TRANSITION_TYPE_MOCAP
						REFRESH_MENU(sFMMCmenu)
					ENDIF
				ENDIF
				
				IF sFMMCmenu.sActiveMenu = eFMMC_CUSTOM_PED_FAIL_NAMES
					INT iIndex = GET_CREATOR_MENU_SELECTION(sFMMCmenu)-1
					IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) > 0
						g_FMMC_STRUCT_ENTITIES.tlCustomPedName[iIndex] = ""
						SET_CREATOR_MENU_SELECTION(sFMMCmenu, GET_CREATOR_MENU_SELECTION(sFMMCmenu)-1)
					ENDIF
				ENDIF
				
				IF sFMMCmenu.sActiveMenu = eFMMC_CUSTOM_VEH_FAIL_NAMES
					INT iIndex = GET_CREATOR_MENU_SELECTION(sFMMCmenu)-1
					IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) > 0
						g_FMMC_STRUCT_ENTITIES.tlCustomVehName[iIndex] = ""
						SET_CREATOR_MENU_SELECTION(sFMMCmenu, GET_CREATOR_MENU_SELECTION(sFMMCmenu)-1)
					ENDIF
				ENDIF
				
				IF sFMMCmenu.sActiveMenu = eFMMC_CUSTOM_OBJ_FAIL_NAMES
					INT iIndex = GET_CREATOR_MENU_SELECTION(sFMMCmenu)-1
					IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) > 0
						g_FMMC_STRUCT_ENTITIES.tlCustomObjName[iIndex] = ""
						SET_CREATOR_MENU_SELECTION(sFMMCmenu, GET_CREATOR_MENU_SELECTION(sFMMCmenu)-1)
					ENDIF
				ENDIF
				
				IF sFMMCmenu.sActiveMenu = eFMMC_TEAM_POST_MISSION_PLAYER_SPAWN
					INT iTeam = sFMMCmenu.iSelectedTeam
					IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = FMMC_POST_MISSION_LOCATION
						g_FMMC_STRUCT.vPostMissionPos[iTeam] = <<0,0,0>>
					ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = FMMC_POST_MISSION_POINT_OF_INTEREST
						g_FMMC_STRUCT.vPostMissionPOI[iTeam] = <<0,0,0>>
					ENDIF
				ENDIF
				
				INT iBit
				
				IF sFMMCmenu.sActiveMenu = eFmmc_TEAM_OUTFIT_LIST
					IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) > 0	
						iBit = GET_NTH_AVAILABLE_OUTFIT(sFMMCmenu.iSelectedTeam, GET_CREATOR_MENU_SELECTION(sFMMCmenu)-1)
						IF iBit >= 0
						AND iBit < ENUM_TO_INT(OUTFIT_MAX_AMOUNT)
							CLEAR_BIT(g_FMMC_STRUCT.biOutFitsAvailableBitset[sFMMCmenu.iSelectedTeam][iBit/32], iBit%32)
							SET_CREATOR_MENU_SELECTION(sFMMCmenu, GET_CREATOR_MENU_SELECTION(sFMMCmenu)-1)
						ENDIF
					ENDIF
				ENDIF
				IF sFMMCmenu.sActiveMenu = eFmmc_TEAM_OUTFIT_STYLES_LIST
					IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) > 0
						iBit = GET_NTH_AVAILABLE_OUTFIT_STYLE(sFMMCmenu.iSelectedTeam, GET_CREATOR_MENU_SELECTION(sFMMCmenu)-1)
						IF iBit >= 0
						AND iBit < ENUM_TO_INT(OUTFIT_STYLE_MAX)
							CLEAR_BIT(g_FMMC_STRUCT.biStyleAvailableBitset[sFMMCmenu.iSelectedTeam][iBit/32], iBit%32)
						ENDIF
					ENDIF
				ENDIF
				
				IF sFMMCmenu.sActiveMenu = eFmmc_TEAM_MASKS_LIST
					IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) > 0
						iBit = GET_NTH_AVAILABLE_MASK(sFMMCmenu.iSelectedTeam, GET_CREATOR_MENU_SELECTION(sFMMCmenu)-1)
						PRINTLN("PD MASKS GET_NTH_AVAILABLE_MASK returns ", iBit)
						IF iBit >= 0
						AND iBit < ENUM_TO_INT(MASK_MAX)
							CLEAR_BIT(g_FMMC_STRUCT.biMaskAvailableBitset[sFMMCmenu.iSelectedTeam][iBit/32], iBit%32)
							PRINTLN("PD MASKS Clearing bit ", iBit%32, " in bitset ", iBit/32)
						ENDIF
					ENDIF
				ENDIF
								
				IF sFMMCmenu.sActiveMenu = eFMMC_TEAM_SETTINGS
					IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_SETTINGS_MULTI_RULE_TIMER_FAIL_STRINGS
						g_FMMC_STRUCT.tl63MultiRuleTimerFailString[sFMMCmenu.iSelectedTeam] = ""
					ENDIF
				ENDIF				
				
				IF sFMMCmenu.sActiveMenu = eFMMC_TEAM_OBJECTIVE_TIMER_FAIL_STRINGS
					g_FMMC_STRUCT.tl63ObjectiveTimerFailString[sFMMCmenu.iSelectedTeam][GET_CREATOR_MENU_SELECTION(sFMMCmenu)] = ""
				ENDIF
				
				IF sFMMCmenu.sActiveMenu = eFMMC_RANDOMISED_SPAWN_GROUP_TEXT_OVERRIDES
					g_FMMC_STRUCT.tl63ObjectiveText_SpawnGroupOverride[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] = ""
				ENDIF
				
				IF sFMMCmenu.sActiveMenu = eFMMC_TEAM_SETTINGS
					IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_SETTINGS_TEAM_TARGET_STRING
						g_FMMC_STRUCT.tl23CustomTargetString[sFMMCmenu.iSelectedTeam] = ""
					ENDIF
				ENDIF
				
				IF sFMMCmenu.sActiveMenu = eFMMC_TEAM_SETTINGS
					IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_SETTINGS_REMAINING_PLAYERS_HUD_STRING
						g_FMMC_STRUCT.tl23CustomTargetStringTwo[sFMMCmenu.iSelectedTeam] = ""
					ENDIF
				ENDIF
				VECTOR vIPLSelected
				IF sFMMCmenu.sActiveMenu = efmmc_IPL_LIST
				
					IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = IPL_MENU_CARRIER
						vIPLSelected = vCarrierPosition
					ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = IPL_MENU_YACHT
						RETURN eFmmc_Action_Null
					ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = IPL_MENU_YACHT_PALETO
						vIPLSelected = vYachtPaleto
					ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = IPL_MENU_YACHT_NEAR_PIER
						vIPLSelected = vYachtNearPierPosition
					ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = IPL_MENU_YACHT_CHUMASH
						vIPLSelected = vYachtNearChumash
					ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = IPL_MENU_MICHAELS_HOUSE
						vIPLSelected = vMichaelsHouse
					ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = IPL_MENU_FRANKLINS_HOUSE
						vIPLSelected = vFranklinsHouse
					ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = IPL_MENU_OPEN_GRAVE
						vIPLSelected = vOpenGravePosition
					ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = IPL_MENU_FORT_ZANCUDO_GATES
						vIPLSelected = vFortZancudoGates
					ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = IPL_MENU_TEQUI_LA_LA
						vIPLSelected = vTequiLaLaLocation
					ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = IPL_MENU_STRIP_CLUB
						vIPLSelected = vStripClubLocation
					ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = IPL_MENU_CHICKEN_FACTORY
						vIPLSelected = vChickenFactoryLocation
					ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = IPL_MENU_YACHT_AQUARIS
						vIPLSelected = vYachtAquaris
					ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = IPL_MENU_SILO
						vIPLSelected = vSilo
					ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = IPL_MENU_REMOVE_IAA_FACILITY_SATELITE_ENTRY_DOOR
						vIPLSelected = vIAAFaciltyDoor
					ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = IPL_MENU_CASINO_AIR_CONDITIONING_UNIT
						vIPLSelected = vCasinoAirCon
					ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = IPL_MENU_REMOVE_CASINO_EXTERIOR_CAMERAS
						vIPLSelected = vCasinoExtCameras
					ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = IPL_MENU_BUNKER_DOORS
						vIPLSelected = vBunkerDoors
					ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = IPL_MENU_LOST_MC_CLUB_HOUSE
						vIPLSelected = vLostMCClubHouse
					ENDIF
					IF NOT IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
						SWAP_CAMERAS(sCamData, sFMMCdata, sFMMCMenu, sCurrentVarsStruct, TRUE, FALSE, TRUE)
					ENDIF
					IF VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), vIPLSelected) > 100
						SET_ENTITY_COORDS(PLAYER_PED_ID(), vIPLSelected)
					ENDIF
				ELIF sFMMCmenu.sActiveMenu = efmmc_IPL_ISLAND
					IF IS_BIT_SET(iIPLLoadBitSet, ciIPLBS_IslandLoaded)
						
						vIPLSelected = GET_ISLAND_IPL_WARP_LOCATION(sFMMCmenu)
						
						IF NOT IS_VECTOR_ZERO(vIPLSelected)
							IF VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), vIPLSelected) > 100
								IF NOT IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
									SWAP_CAMERAS(sCamData, sFMMCdata, sFMMCMenu, sCurrentVarsStruct, TRUE, FALSE, TRUE)
								ENDIF
								SET_ENTITY_COORDS(PLAYER_PED_ID(), vIPLSelected)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF sFMMCmenu.sActiveMenu = efmmc_INTERIOR_LIST
				OR sFMMCmenu.sActiveMenu = efmmc_INTERIOR_BUNKER
				OR sFMMCmenu.sActiveMenu = efmmc_INTERIOR_SUB
				OR sFMMCmenu.sActiveMenu = efmmc_INTERIOR_IAA
				OR sFMMCmenu.sActiveMenu = efmmc_INTERIOR_HEISTS2
				OR sFMMCmenu.sActiveMenu = efmmc_ARENAMENUS_TOP
				OR sFMMCmenu.sActiveMenu = efmmc_CASINO_BASE
				OR sFMMCmenu.sActiveMenu = efmmc_CASINO_APARTMENT
				OR sFMMCmenu.sActiveMenu = efmmc_INTERIOR_NIGHTCLUB_BASE
				OR sFMMCmenu.sActiveMenu = efmmc_INTERIOR_CUSTOM_MOC
				OR sFMMCmenu.sActiveMenu = efmmc_INTERIOR_METH_LABS
				OR sFMMCMenu.sActiveMenu = efmmc_INTERIOR_WEED_FARM
				OR sFMMCMenu.sActiveMenu = efmmc_INTERIOR_COKE_FACTORY
				OR sFMMCMenu.sActiveMenu = efmmc_INTERIOR_RECORDING_STUDIO
				OR sFMMCmenu.sActiveMenu = efmmc_INTERIOR_MUSIC_LOCKER
				OR sFMMCmenu.sActiveMenu = efmmc_INTERIOR_FIB
				 	IF GET_INTERIOR_NAME_ENUM_FROM_MENU_OPTION(sFMMCmenu.sActiveMenu, GET_CREATOR_MENU_SELECTION(sFMMCmenu)) != INTERIOR_MAX_NUM
						sCurrentVarsStruct.iMenuState = MENU_STATE_WARP_TO_INTERIOR
					ENDIF
				ENDIF
				
				IF sFMMCmenu.sActiveMenu = eFmmc_ELEVATORS
				AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ELEVATOR_OPTIONS_INDEX
					RESET_ELEVATOR_STRUCT(sFMMCMenu.iSelectedElevator)
				ENDIF
				
				IF sFMMCmenu.sActiveMenu = efMMC_CUTSCENE_ENTITIES
					IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) != -1
						CLEAR_CUTSCENE_ENTITY_DATA(g_FMMC_STRUCT.sCurrentSceneData.sCutsceneEntities[GET_CREATOR_MENU_SELECTION(sFMMCmenu)])
						SORT_CUTSCENE_ENTITIES(g_FMMC_STRUCT.sCurrentSceneData.sCutsceneEntities)
					ENDIF
				ENDIF
				
				IF sFMMCmenu.sActiveMenu = efMMC_CUTSCENE_MOCAP_ENTITIES
					CLEAR_CUTSCENE_ENTITY_DATA(g_FMMC_STRUCT.sCurrentMocapSceneData.sCutsceneEntities[GET_CREATOR_MENU_SELECTION(sFMMCmenu)])
					SORT_CUTSCENE_ENTITIES(g_FMMC_STRUCT.sCurrentMocapSceneData.sCutsceneEntities)
				ENDIF
				
				IF sFMMCmenu.sActiveMenu = efMMC_CUTSCENE_ENTITIES
					CLEAR_CUTSCENE_ENTITY_DATA(g_FMMC_STRUCT.sCurrentSceneData.sCutsceneEntities[GET_CREATOR_MENU_SELECTION(sFMMCmenu)])
					SORT_CUTSCENE_ENTITIES(g_FMMC_STRUCT.sCurrentSceneData.sCutsceneEntities)
				ENDIF
				
				IF sFMMCmenu.sActiveMenu = eFmmc_END_OF_MISSION_END_CUTSCENE_ENTITIES
					CLEAR_CUTSCENE_ENTITY_DATA(g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[GET_CREATOR_MENU_SELECTION(sFMMCmenu)])
					SORT_CUTSCENE_ENTITIES(g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities)
				ENDIF
				
				IF sFMMCmenu.sActiveMenu = eFmmc_DIALOGUE_TRIGGER_BASE
					IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DIALOGUE_TRIGGER_FILE_ID
					AND NOT IS_STRING_NULL_OR_EMPTY(sFMMCmenu.sDialogueTrigger.tlBlock)
						sFMMCmenu.sDialogueTrigger.tlBlock= ""
					ENDIF
					IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DIALOGUE_TRIGGER_ID
					AND NOT IS_STRING_NULL_OR_EMPTY(sFMMCmenu.sDialogueTrigger.tlRoot)
						sFMMCmenu.sDialogueTrigger.tlRoot = ""
					ENDIF
					IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DIALOGUE_TRIGGER_ALSO_DISPLAY_HELPTEXT
					AND NOT IS_STRING_NULL_OR_EMPTY(sFMMCmenu.sDialogueTrigger.tlDialogueTriggerAdditionalHelpText)
						sFMMCmenu.sDialogueTrigger.tlDialogueTriggerAdditionalHelpText = ""
					ENDIF
				ENDIF
				
				IF sFMMCmenu.sActiveMenu = efmmc_SPEAKER_LIST
					IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sSpeakers[GET_CREATOR_MENU_SELECTION(sFMMCmenu)])
						g_FMMC_STRUCT.sSpeakers[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] = ""
					ENDIF
				ENDIF
				
				IF sFMMCmenu.sActiveMenu = eFMMC_AVAILABLE_VEHICLE_LIBRARIES
					INT iMenuLengthOffset = 0
					
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciUSE_SUB_TEAM_ROLES)
						iMenuLengthOffset = 1
					ENDIF
					
					IF (GET_CREATOR_MENU_SELECTION(sFMMCmenu) - (CORONA_VEHICLE_LIST_MAX + iMenuLengthOffset)) >= 0
						sFMMCmenu.iVehicleLibrary = GET_CREATOR_MENU_SELECTION(sFMMCmenu) - (CORONA_VEHICLE_LIST_MAX + iMenuLengthOffset)
					ENDIF
					
					//If it is then reset up the menu
					GO_TO_MENU(sFMMCmenu, sFMMCdata, eFMMC_AVAILABLE_VEHICLES)
					
				ENDIF
				
				IF sFMMCmenu.sActiveMenu = eFmmc_CLOTHING_OPTIONS
					IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) >= CLOTHING_OPTIONS_MAX_FIXED
						IF (GET_CREATOR_MENU_SELECTION(sFMMCmenu) - CLOTHING_OPTIONS_MAX_FIXED) % 2 = 0
							GO_TO_MENU(sFMMCmenu, sFMMCdata, eFMMC_AVAILABLE_VERSUS_OUTFITS)
						ENDIF
					ENDIF
				ENDIF
				
				IF sFMMCmenu.sActiveMenu = eFmmc_MISSION_VARIATION_OBJECT_MODEL_SWAP_SELECTION
					PROCESS_CLEARING_MISSION_VARIATION_OBJECT_MODEL_SWAP_OPTIONS(sFMMCMenu)
				ENDIF
				
				IF sFMMCmenu.sActiveMenu = eFMMC_DUMMY_BLIPS
					IF GET_CREATOR_MENU_SELECTION(sFMMCMenu) = DUMMY_BLIP_NAME
						g_FMMC_STRUCT.sCurrentDummyBlip.tlDBName = ""
					ENDIF
				ENDIF
				
				REFRESH_MENU(sFMMCmenu)
				
			ENDIF
			
			IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_Y)
				IF sFMMCmenu.sActiveMenu = efMMC_CUTSCENE_ADD_SCENE
					IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) > ADD_CUTSCENE_MENU_ADD_SHOT
						INT iSourceShot = GET_CREATOR_MENU_SELECTION(sFMMCmenu) - (ADD_CUTSCENE_MENU_ADD_SHOT+1)
						INT iDestShot = iSourceShot + 1
						IF iDestShot < MAX_CAMERA_SHOTS
							sFMMCmenu.iCurrentShot = iDestShot
							SET_ACTIVE_MENU(sFMMCMenu, efMMC_CUTSCENE_ADD_SHOT)
							sFMMCmenu.iReturnSelection = GET_CREATOR_MENU_SELECTION(sFMMCmenu)
							SET_CREATOR_MENU_SELECTION(sFMMCmenu, 0)
							g_fmmc_struct.sCurrentSceneData.vCamStartPos[iDestShot] = g_fmmc_struct.sCurrentSceneData.vCamEndPos[iSourceShot]
							g_fmmc_struct.sCurrentSceneData.vCamStartRot[iDestShot] = g_fmmc_struct.sCurrentSceneData.vCamEndRot[iSourceShot]
							g_fmmc_struct.sCurrentSceneData.fStartCamFOV[iDestShot] = g_fmmc_struct.sCurrentSceneData.fEndCamFOV[iSourceShot]
						ENDIF
					ENDIF
				ELIF sFMMCmenu.sActiveMenu = efMMC_CUTSCENE_MOCAP_ENTITIES
					IF g_fmmc_struct.sCurrentMocapSceneData.sCutsceneEntities[GET_CREATOR_MENU_SELECTION(sFMMCmenu)].iType != -1
						sFMMCmenu.iCurrentCutsceneEntity = GET_CREATOR_MENU_SELECTION(sFMMCmenu)
						GO_TO_MENU(sFMMCmenu, sFMMCdata, efMMC_CUTSCENE_MOCAP_ENTITY_OPTIONS)
						PRINTLN("PD ENTITY DEBUG - Going to efMMC_CUTSCENE_MOCAP_ENTITY_OPTIONS")
					ENDIF
				ELIF sFMMCmenu.sActiveMenu = efMMC_CUTSCENE_ENTITIES
					IF g_fmmc_struct.sCurrentSceneData.sCutsceneEntities[GET_CREATOR_MENU_SELECTION(sFMMCmenu)].iType != -1
						sFMMCmenu.iCurrentCutsceneEntity = GET_CREATOR_MENU_SELECTION(sFMMCmenu)
						GO_TO_MENU(sFMMCmenu, sFMMCdata, efMMC_CUTSCENE_ENTITIES_ENTITY_OPTIONS)
						PRINTLN("PD ENTITY DEBUG - Going to efMMC_CUTSCENE_MOCAP_ENTITY_OPTIONS")
					ENDIF
				ELIF sFMMCmenu.sActiveMenu = eFmmc_END_OF_MISSION_END_CUTSCENE_ENTITIES
					IF g_fmmc_struct.sEndMocapSceneData.sCutsceneentities[GET_CREATOR_MENU_SELECTION(sFMMCmenu)].iType != -1
						sFMMCmenu.iCurrentCutsceneEntity = GET_CREATOR_MENU_SELECTION(sFMMCmenu)
						GO_TO_MENU(sFMMCmenu, sFMMCdata, eFmmc_END_OF_MISSION_END_CUTSCENE_ENTITY_OPTIONS)
						PRINTLN("PD ENTITY DEBUG - Going to eFmmc_END_OF_MISSION_END_CUTSCENE_ENTITY_OPTIONS")
					ENDIF
				ELIF (sFMMCmenu.sActiveMenu = eFmmc_PED_SPAWN_OPTIONS AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_SPAWN_OPT_SECONDARY_SPAWN_POINT)
				OR (sFMMCmenu.sActiveMenu = eFmmc_VEHICLE_SPAWN_OPTIONS AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_SPAWN_SECOND_SPAWN_POINT)
				OR (sFMMCmenu.sActiveMenu = eFmmc_OBJECT_BASE AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OBJECT_SECOND_SPAWN_POINT)
					IF NOT IS_VECTOR_ZERO(sFMMCmenu.vSecondSpawnPosition)
						bSwitchingCam = TRUE
						vSwitchVec = sFMMCmenu.vSecondSpawnPosition
						fSwitchHeading = sFMMCmenu.fSecondSpawnHeading
					ENDIF
				ELIF sFMMCmenu.sActiveMenu = eFmmc_VEHICLE_RESTART_POINTS
				OR sFMMCmenu.sActiveMenu = eFmmc_ENEMY_RESTART_POINTS
					IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = FMMC_MAX_RESTART_CHECKPOINTS
					ELIF NOT IS_VECTOR_ZERO(sFMMCmenu.vMenuRestartPos[GET_CREATOR_MENU_SELECTION(sFMMCmenu)])
						bSwitchingCam = TRUE
						vSwitchVec = sFMMCmenu.vMenuRestartPos[GET_CREATOR_MENU_SELECTION(sFMMCmenu)]
						fSwitchHeading = sFMMCmenu.fMenuRestartHead[GET_CREATOR_MENU_SELECTION(sFMMCmenu)]
					ENDIF
				ELIF (sFMMCmenu.sActiveMenu = eFmmc_VEH_DROPOFF_DELIVERY_OPTIONS AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_DROPOFF_DELIVERY_OVERRIDE_POS)
					IF NOT IS_VECTOR_ZERO(sFMMCmenu.vVehDropOffOverride)
						bSwitchingCam = TRUE
						vSwitchVec = sFMMCmenu.vVehDropOffOverride
					ENDIF
				ELIF (sFMMCmenu.sActiveMenu = eFmmc_OBJECT_DROPOFF_DELIVERY_OPTIONS AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OBJECT_DROPOFF_DELIVERY_OVERRIDE_POS)
					IF NOT IS_VECTOR_ZERO(sFMMCmenu.vVehDropOffOverride)
						bSwitchingCam = TRUE
						vSwitchVec = sFMMCmenu.vVehDropOffOverride
					ENDIF
				ENDIF
				REFRESH_MENU(sFMMCmenu)
			ENDIF
			
			IF IS_DELETE_BUTTON_AS_GO_TO_MENU(sFMMCMenu)
				PROCESS_DELETE_BUTTON_AS_GO_TO_MENU(sFMMCMenu, sFMMCdata)
						
			ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
				IF sFMMCmenu.sActiveMenu = eFmmc_MAIN_OPTIONS_BASE
					IF IS_THIS_OPTION_SELECTABLE()					
						IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_NAME
							sCurrentVarsStruct.iMenuState = MENU_STATE_TITLE
							SET_NOTIFY_LOCALISATION()
						ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_DESCRIPTION
							sCurrentVarsStruct.iMenuState = MENU_STATE_DESCRIPTION
							SET_NOTIFY_LOCALISATION()
						ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_TAGS
							sCurrentVarsStruct.iMenuState = MENU_STATE_TAGS
							SET_NOTIFY_LOCALISATION()
						ENDIF
						
						IF sFMMCmenu.sMenuGoto[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] != eFmmc_Null_item						
							sCurrentVarsStruct.bResetUpHelp = TRUE
							PLAY_SOUND_FRONTEND(-1, "SELECT", GET_SOUND_SET_FROM_CREATOR_TYPE())
							PRINTSTRING("CONFIRM SOUND EFFECT!!!")PRINTNL()
							GO_TO_MENU(sFMMCmenu, sFMMCdata)
							IF sFMMCmenu.sActiveMenu = eFmmc_PAN_CAM_BASE
							OR sFMMCmenu.sActiveMenu = eFmmc_PHOTO_CAM_BASE
							OR sFMMCmenu.sActiveMenu = eFmmc_OUT_CAM_BASE
							OR sFMMCmenu.sActiveMenu = eFmmc_DM_TEAM_CAMERAS
							OR sFMMCmenu.sActiveMenu = eFmmc_SET_ELEVATOR_CAM
							OR sFMMCmenu.sActiveMenu = efMMC_CUTSCENE_SET_CAM_1
							OR sFMMCmenu.sActiveMenu = efMMC_CUTSCENE_SET_CAM_2
							OR sFMMCmenu.sActiveMenu = efMMC_CUTSCENE_SET_CAM_3
							OR sFMMCmenu.sActiveMenu = efMMC_TRIP_SKIP_CAM
								IF IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
									sCurrentVarsStruct.iMenuState = MENU_STATE_SWITCH_CAM
								ENDIF
							ENDIF
							RETURN eFmmc_Action_Null	
						ENDIF
					ENDIF
				ELIF sFMMCMenu.sMenuAction[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] = eFmmc_Edit_Vector
				AND NOT IS_BIT_SET(sFMMCmenu.iFMMCMenuBitset, ciFMMCMenuBS_CurrentVectorItemIsEmpty)
					sFMMCmenu.iEditVectorAxis ++
					IF sFMMCmenu.iEditVectorAxis >= ciMAX_EDIT_VECTOR_AXIS
						sFMMCmenu.iEditVectorAxis = 0
					ENDIF
				ELIF sFMMCMenu.sMenuAction[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] = eFmmc_Edit_RGBA
					sFMMCmenu.iEditRGBAValue++
					IF sFMMCmenu.iEditRGBAValue >= ciMAX_EDIT_RGBA_VALUES
						sFMMCmenu.iEditRGBAValue = -1
					ENDIF
				ELIF sFMMCmenu.sActiveMenu = eFmmc_ENTITY_SHARED_SPAWN_GROUP_SETTINGS					
				OR sFMMCmenu.sActiveMenu = eFmmc_ENTITY_SHARED_SUB_SPAWN_GROUP_SETTINGS
				OR sFMMCmenu.sActiveMenu = eFmmc_ENTITY_SHARED_MODIFY_SPAWN_GROUP_ON_EVENT_SETTINGS
				OR sFMMCmenu.sActiveMenu = eFmmc_ENTITY_SHARED_MODIFY_SUB_SPAWN_GROUP_ON_EVENT_SETTINGS
				OR sFMMCmenu.sActiveMenu = eFmmc_ENTITY_SHARED_SPAWN_BLOCKING_FLAG_SETTINGS
					// keep this.
					
				ELIF sFMMCmenu.sActiveMenu = eFmmc_MISSION_CONTINUITY_DELETE_OPTIONS
					EDIT_MISSION_CONTINUITY_DELETE_OPTIONS_MENU(sFMMCmenu)
				ELIF sFMMCmenu.sActiveMenu = efmmc_DIALOGUE_TRIGGER_VEHICLE_HACK_LIST
					EDIT_CREATOR_LINKED_ENTITY_BITSET_MENU_ITEM(sFMMCendStage, sFMMCmenu.sDialogueTrigger.iVehicleHacked, GET_CREATOR_MENU_SELECTION(sFMMCmenu), ciFMMC_ENTITY_LINK_TYPE_VEHICLE)
				ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_CUSTOM_WASTED_SHARD_TITLE
				AND sFMMCmenu.sActiveMenu = eFMMC_TEAM_CUSTOM_WASTED_SHARD
					sCurrentVarsStruct.iMenuState = MENU_STATE_CUSTOM_WASTED_TITLE_TL
				ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_CUSTOM_WASTED_SHARD_STRAPLINE
				AND sFMMCmenu.sActiveMenu = eFMMC_TEAM_CUSTOM_WASTED_SHARD
					sCurrentVarsStruct.iMenuState = MENU_STATE_CUSTOM_WASTED_STRAPLINE_TL
				ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_CUSTOM_WASTED_SHARD_CLEAR
				AND sFMMCmenu.sActiveMenu = eFMMC_TEAM_CUSTOM_WASTED_SHARD
					g_FMMC_STRUCT.tlCustomWastedTitle[sFMMCmenu.iSelectedTeam] = ""
					g_FMMC_STRUCT.tlCustomWastedStrapline[sFMMCmenu.iSelectedTeam] = ""
					g_FMMC_STRUCT.iCustomWastedShardType[sFMMCmenu.iSelectedTeam] = 0
				ELIF sFMMCmenu.sActiveMenu = eFmmc_TEAM_OUTFIT_LIST
					INT iBit
					IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 0
						iBit = sFMMCmenu.iCurrentOutFit
						IF NOT IS_BIT_SET(g_FMMC_STRUCT.biOutFitsAvailableBitset[sFMMCmenu.iSelectedTeam][iBit/32], iBit%32)
							SET_BIT(g_FMMC_STRUCT.biOutFitsAvailableBitset[sFMMCmenu.iSelectedTeam][iBit/32], iBit%32)
						ENDIF
					ENDIF
				ELIF sFMMCmenu.sActiveMenu = eFmmc_TEAM_OUTFIT_STYLES_LIST
					INT iBit
					IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 0
						iBit = sFMMCmenu.iCurrentOutfitStyle
						IF NOT IS_BIT_SET(g_FMMC_STRUCT.biStyleAvailableBitset[sFMMCmenu.iSelectedTeam][iBit/32], iBit%32)
							SET_BIT(g_FMMC_STRUCT.biStyleAvailableBitset[sFMMCmenu.iSelectedTeam][iBit/32], iBit%32)
						ENDIF
					ENDIF
				ELIF sFMMCmenu.sActiveMenu = eFMMC_SHRINKING_SUDDEN_DEATH_OPTIONS
				AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) != SHRINKING_SUDDEN_DEATH_CLEAR
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciSUDDEN_DEATH_THAT_ENDS_WHEN_ONLY_ONE_TEAM_ALIVE)
						g_FMMC_STRUCT.vSphereSpawnPoint = sCurrentVarsStruct.vCoronaPos
					ENDIF
				ELIF sFMMCmenu.sActiveMenu = eFMMC_KILLALLENEMIES_SUDDEN_DEATH_PENNED_IN_OPTIONS
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciSUDDEN_DEATH_KILLALLENEMIES_PENNED_IN)
						g_FMMC_STRUCT.vSphereSpawnPoint = sCurrentVarsStruct.vCoronaPos
					ENDIF
				ELIF sFMMCmenu.sActiveMenu = eFMMC_SHRINKING_SUDDEN_DEATH_OPTIONS
				AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = SHRINKING_SUDDEN_DEATH_CLEAR
					CLEAR_SHRINKING_SUDDEN_DEATH_AREA()
				ELIF sFMMCmenu.sActiveMenu = eFmmc_TEAM_MASKS_LIST
					INT iBit
					IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 0
						iBit = sFMMCmenu.iCurrentMask
						IF NOT IS_BIT_SET(g_FMMC_STRUCT.biMaskAvailableBitset[sFMMCmenu.iSelectedTeam][iBit/32], iBit%32)
							SET_BIT(g_FMMC_STRUCT.biMaskAvailableBitset[sFMMCmenu.iSelectedTeam][iBit/32], iBit%32)
						ENDIF
					ENDIF
				ELIF sFMMCmenu.sActiveMenu = efMMC_CUTSCENE_ADD_SCENE
				AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ADD_CUTSCENE_MENU_TRANSITION_COORDS
					SET_POSITION_FROM_CREATOR(sFMMCdata, sCurrentVarsStruct, g_FMMC_STRUCT.sCurrentSceneData.vIntroCoOrds)
				ELIF sFMMCmenu.sActiveMenu = efMMC_CUTSCENE_ADD_SHOT
				AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ADD_SHOT_MENU_STREAM_POS
					SET_POSITION_FROM_CREATOR(sFMMCdata, sCurrentVarsStruct, g_FMMC_STRUCT.sCurrentSceneData.vStreamStartPos[sFMMCmenu.iCurrentShot])
				ELIF sFMMCmenu.sActiveMenu = efMMC_CUTSCENE_ADD_SHOT
				AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ADD_SHOT_MENU_PLAYER_WALK_TO_LOCATION
					SET_POSITION_FROM_CREATOR(sFMMCdata, sCurrentVarsStruct, g_FMMC_STRUCT.sCurrentSceneData.vWalkToPosition[sFMMCmenu.iCurrentShot])
				ELIF sFMMCmenu.sActiveMenu = efMMC_CUTSCENE_ADD_SHOT
				AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ADD_SHOT_MENU_PLAYER_DRIVE_TO_LOCATION
					g_FMMC_STRUCT.sCurrentSceneData.vDriveToPosition[sFMMCmenu.iCurrentShot] = sCurrentVarsStruct.vCoronaPos
				ELIF sFMMCmenu.sActiveMenu = efMMC_CUTSCENE_ADD_SCENE
				AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ADD_CUTSCENE_MENU_SET_NAME
					sCurrentVarsStruct.iMenuState = MENU_STATE_CUTSCENE_NAME
				ELIF sFMMCmenu.sActiveMenu = eFMMC_CUSTOM_SHARD_OPTIONS
					IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_CUSTOM_SHARD_TITLE_TEXT
						sCurrentVarsStruct.iMenuState = MENU_STATE_CUSTOM_SHARD_TITLE_TL
					ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_CUSTOM_SHARD_STRAPLINE_TEXT
						sCurrentVarsStruct.iMenuState = MENU_STATE_CUSTOM_SHARD_STRAPLINE_TL
					ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_CUSTOM_SHARD_TITLE2_TEXT
						sCurrentVarsStruct.iMenuState = MENU_STATE_CUSTOM_SHARD_TITLE_TL2
					ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_CUSTOM_SHARD_STRAPLINE2_TEXT
						sCurrentVarsStruct.iMenuState = MENU_STATE_CUSTOM_SHARD_STRAPLINE_TL2
					ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_CUSTOM_SHARD_TITLE3_TEXT
						sCurrentVarsStruct.iMenuState = MENU_STATE_CUSTOM_SHARD_TITLE_TL3
					ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_CUSTOM_SHARD_STRAPLINE3_TEXT
						sCurrentVarsStruct.iMenuState = MENU_STATE_CUSTOM_SHARD_STRAPLINE_TL3
					ENDIF
				ELIF sFMMCmenu.sActiveMenu = efMMC_CUTSCENE_ADD_SHOT
				AND (GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ADD_SHOT_MENU_SET_LOCATION_NAME OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ADD_SHOT_MENU_SET_CAM_NAME)
					sCurrentVarsStruct.iMenuState = MENU_STATE_CAM_STRINGS
				ELIF sFMMCmenu.sActiveMenu = eFMMC_TEAM_SETTINGS
				AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_SETTINGS_TEAM_TARGET_STRING
					sCurrentVarsStruct.iMenuState = MENU_STATE_TARGET_STRINGS
				ELIF sFMMCmenu.sActiveMenu = eFMMC_TEAM_SETTINGS
				AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_SETTINGS_REMAINING_PLAYERS_HUD_STRING
					sCurrentVarsStruct.iMenuState = MENU_STATE_CUSTOM_PHUD_STRING
				ELIF sFMMCmenu.sActiveMenu = eFMMC_DUMMY_BLIPS
				AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DUMMY_BLIP_NAME
					sCurrentVarsStruct.iMenuState = MENU_STATE_DUMMY_BLIP_NAME
				ELIF GET_CREATOR_MENU_SELECTION(sFMMCMenu) = WORLD_PROPS_SELECTED_MODEL_ENTER_MANUALLY
				AND sFMMCmenu.sActiveMenu = efMMC_WORLD_PROPS
				AND IS_BIT_SET(sFMMCmenu.sCurrentWorldProp.iBitset, ciFMMC_WorldPropManuallyPlaced)
					sCurrentVarsStruct.iMenuState = MENU_STATE_ENTER_SIMPLE_TL63
				ELIF sFMMCmenu.sActiveMenu = eFMMC_RANDOMISED_SPAWN_GROUP_TEXT_OVERRIDES
					sCurrentVarsStruct.iMenuState = MENU_STATE_ENTER_SIMPLE_TL63
				ELIF sFMMCmenu.sActiveMenu = eFmmc_DEV_ASSIST_BASE
				AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DEV_ASSIST_MENU_DESCRIPTION
					sCurrentVarsStruct.iMenuState = MENU_STATE_ENTER_SIMPLE_TL63
				ELIF sFMMCmenu.sActiveMenu = eFmmc_DEV_ASSIST_BASE
				AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DEV_ASSIST_MENU_TITLE
					sCurrentVarsStruct.iMenuState = MENU_STATE_ENTER_SIMPLE_TL63
				ELIF sFMMCmenu.sActiveMenu = eFmmc_DEV_ASSIST_BASE
				AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DEV_ASSIST_MENU_TAG	
					sCurrentVarsStruct.iMenuState = MENU_STATE_ENTER_SIMPLE_TL63	
				ELIF sFMMCmenu.sActiveMenu = eFmmc_DEV_ASSIST_BASE
				AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DEV_ASSIST_MENU_DEV_NOTES_CONTENT_ID	
					sCurrentVarsStruct.iMenuState = MENU_STATE_ENTER_SIMPLE_TL63					
				ELIF sFMMCmenu.sActiveMenu = eFmmc_DEV_LOAD_FROM_CONTENT_BASE
				AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DEV_ASSIST_LOAD_FROM_CONTENT_MENU_ID
					sCurrentVarsStruct.iMenuState = MENU_STATE_ENTER_SIMPLE_TL63		
				ELIF sFMMCmenu.sActiveMenu = eFMMC_TEAM_OBJECTIVE_TIMER_FAIL_STRINGS
					sCurrentVarsStruct.iMenuState = MENU_STATE_ENTER_SIMPLE_TL63
				ELIF sFMMCmenu.sActiveMenu = eFMMC_TEAM_SETTINGS
				AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_SETTINGS_MULTI_RULE_TIMER_FAIL_STRINGS
					sCurrentVarsStruct.iMenuState = MENU_STATE_ENTER_SIMPLE_TL63
				ELIF sFMMCmenu.sActiveMenu = eFmmc_DIALOGUE_TRIGGER_BASE
				AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DIALOGUE_TRIGGER_AMBIENT_VOICE_NAME
					sCurrentVarsStruct.iMenuState = MENU_STATE_ENTER_SIMPLE_TL63
				ELIF sFMMCmenu.sActiveMenu = efMMC_CUTSCENE_MOCAP_ENTITIES_HIDE
				AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) >= MOCAP_CUTSCENE_ENTITY_HIDE_ENTITY
				AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) < MOCAP_CUTSCENE_ENTITY_HIDE_ENTITY_MAX
					sCurrentVarsStruct.iMenuState = MENU_STATE_ENTER_SIMPLE_TL63
				ELIF sFMMCmenu.sActiveMenu = eFmmc_END_OF_MISSION_END_CUTSCENE_ENTITIES_HIDE
				AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) >= END_CUTSCENE_ENTITY_HIDE_ENTITY
				AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) < END_CUTSCENE_ENTITY_HIDE_ENTITY_MAX
					sCurrentVarsStruct.iMenuState = MENU_STATE_ENTER_SIMPLE_TL63
				ELIF sFMMCmenu.sActiveMenu = efMMC_CUTSCENE_ADD_MOCAP_SCENE
				AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ADD_MOCAP_CUTSCENE_MENU_SET_NAME
					sCurrentVarsStruct.iMenuState = MENU_STATE_CUTSCENE_NAME
				ELIF sFMMCmenu.sActiveMenu = eFMMC_TEAM_SETTINGS
				AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_SETTINGS_QUICK_RESTART_ANIMATION
					sCurrentVarsStruct.iMenuState = MENU_STATE_PLANNING_MISSION_NAME
				ELIF sFMMCmenu.sActiveMenu = eFmmc_PLANNING_MISSIONS
				AND IS_CLEAR_ALL_LEVELS_SELECTED()
					FOR i = 0 TO FMMC_MAX_PLANNING_MISSIONS-1
						g_FMMC_STRUCT.sPlanningMissions[i].tlID = ""
						g_fmmc_STRUCT.sPlanningMissions[i].iLevel = 0
						g_fmmc_STRUCT.sPlanningMissions[i].iVar = -1
					ENDFOR
					SET_CREATOR_MENU_SELECTION(sFMMCmenu, 0)
				ELIF sFMMCmenu.sActiveMenu = eFmmc_PLANNING_MISSIONS
				AND sFMMCMenu.sMenuAction[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] = efMMC_action_empty_planning
					INT iNumMissions
					FOR i = 0 TO FMMC_MAX_PLANNING_MISSIONS-1
						IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sPlanningMissions[i].tlID)
							iNumMissions++
						ENDIF
					ENDFOR
					IF iNumMissions < FMMC_MAX_PLANNING_MISSIONS
						sCurrentVarsStruct.iMenuState = MENU_STATE_PLANNING_MISSION_NAME
					ENDIF
				ELIF sFMMCmenu.sActiveMenu = eFmmc_NEXT_MISSION_SETTINGS
				AND sFMMCMenu.sMenuAction[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] = efMMC_action_empty_next_mission
					sCurrentVarsStruct.iMenuState = MENU_STATE_NEXT_MISSION_NAME
				ELIF sFMMCmenu.sActiveMenu = efmmc_DIALOGUE_TRIGGER_APPEND_OPTIONS
				AND sFMMCMenu.sMenuAction[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] = eFmmc_Action_Enter_Dialogue_Append
					sCurrentVarsStruct.iMenuState = MENU_STATE_ENTER_DIALOGUE_APPEND				
				ELIF sFMMCmenu.sActiveMenu = eFMMC_CUSTOM_PED_FAIL_NAMES
				AND sFMMCMenu.sMenuAction[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] = efMMC_action_empty_next_mission
					INT iNumNames
					FOR i = 0 TO FMMC_MAX_CUSTOM_FAIL_NAMES -1
						IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT_ENTITIES.tlCustomPedName[i])
							iNumNames++
						ENDIF
					ENDFOR
					IF iNumNames < FMMC_MAX_CUSTOM_FAIL_NAMES
						sCurrentVarsStruct.iMenuState = MENU_STATE_CUSTOM_PED_FAIL_NAME
					ENDIF
				ELIF sFMMCmenu.sActiveMenu = eFMMC_CUSTOM_VEH_FAIL_NAMES
				AND sFMMCMenu.sMenuAction[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] = efMMC_action_empty_next_mission
					INT iNumNames
					FOR i = 0 TO FMMC_MAX_CUSTOM_FAIL_NAMES -1
						IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT_ENTITIES.tlCustomVehName[i])
							iNumNames++
						ENDIF
					ENDFOR
					IF iNumNames < FMMC_MAX_CUSTOM_FAIL_NAMES
						sCurrentVarsStruct.iMenuState = MENU_STATE_CUSTOM_VEH_FAIL_NAME
					ENDIF
				ELIF sFMMCmenu.sActiveMenu = eFMMC_CUSTOM_OBJ_FAIL_NAMES
				AND sFMMCMenu.sMenuAction[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] = efMMC_action_empty_next_mission
					INT iNumNames
					FOR i = 0 TO FMMC_MAX_CUSTOM_FAIL_NAMES -1
						IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT_ENTITIES.tlCustomObjName[i])
							iNumNames++
						ENDIF
					ENDFOR
					IF iNumNames < FMMC_MAX_CUSTOM_FAIL_NAMES
						sCurrentVarsStruct.iMenuState = MENU_STATE_CUSTOM_OBJ_FAIL_NAME
					ENDIF
				ELIF sFMMCmenu.sActiveMenu = efmmc_SPEAKER_LIST
				OR (sFMMCmenu.sActiveMenu = eFmmc_DIALOGUE_TRIGGER_BASE AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DIALOGUE_TRIGGER_FILE_ID)
				OR (sFMMCmenu.sActiveMenu = eFmmc_DIALOGUE_TRIGGER_BASE AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DIALOGUE_TRIGGER_ID)
				OR (sFMMCmenu.sActiveMenu = eFmmc_DIALOGUE_TRIGGER_BASE AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DIALOGUE_TRIGGER_ALSO_DISPLAY_HELPTEXT)
					sCurrentVarsStruct.iMenuState = MENU_STATE_DIALOGUE_ENTRY
				ELIF sFMMCmenu.sActiveMenu = efMMC_APARTMENT_WARP
					IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = APARTMENT_WARP_SET_ENTRY_LOCATION
						g_FMMC_STRUCT.vBuildingWarp = sCurrentVarsStruct.vCoronaPos
					ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = APARTMENT_WARP_WARP_TO_APARTMENT
					AND sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
						sCurrentVarsStruct.iMenuState = MENU_STATE_WARP_TO_INTERIOR
					ENDIF
				ELIF sFMMCmenu.sActiveMenu = eFmmc_TEAM_GEAR_LIST
					INT iBit
					IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) > 0
						iBit = GET_NTH_AVAILABLE_GEAR(sFMMCmenu.iSelectedTeam, GET_CREATOR_MENU_SELECTION(sFMMCmenu)-1)
						IF iBit >= 0
						AND iBit < ENUM_TO_INT(GEAR_MAX_AMOUNT)
							CLEAR_LONG_BIT(g_FMMC_STRUCT.biGearAvailableBitset[sFMMCmenu.iSelectedTeam], iBit)
						ENDIF
					ELSE
						iBit = sFMMCmenu.iCurrentGear
						IF NOT IS_LONG_BIT_SET(g_FMMC_STRUCT.biGearAvailableBitset[sFMMCmenu.iSelectedTeam], iBit)
							SET_LONG_BIT(g_FMMC_STRUCT.biGearAvailableBitset[sFMMCmenu.iSelectedTeam], iBit)
						ENDIF
					ENDIF
				ELIF sFMMCmenu.sActiveMenu = eFmmc_SYNOPSIS
					sCurrentVarsStruct.iMenuState = MENU_STATE_SYNOPSIS
					SET_NOTIFY_LOCALISATION()
				ELIF sFMMCmenu.sActiveMenu = eFmmc_SMS_OPTIONS
					IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 6
						sCurrentVarsStruct.iMenuState = MENU_STATE_SMS_MESSAGE
					ENDIF
				ELIF sFMMCmenu.sActiveMenu = eFmmc_VEHICLE_EXTRAS
					IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 0
					OR IS_BIT_SET(sFMMCmenu.iVehBitsetTwo, ciFMMC_VEHICLE2_USE_CUSTOM_EXTRAS)
						IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) > 0
						AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) < 15
							EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCmenu.iVehicleExtrasBitset, GET_CREATOR_MENU_SELECTION(sFMMCmenu)-1)
						ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 0
							EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCmenu.iVehBitsetTwo, ciFMMC_VEHICLE2_USE_CUSTOM_EXTRAS)
						ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 15
							sFMMCmenu.iVehicleExtrasBitset = 0
						ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 16
							FOR i = 0 TO 31
								SET_BIT(sFMMCmenu.iVehicleExtrasBitset, i)
							ENDFOR
						ENDIF
					ENDIF
				
				ELIF sFMMCmenu.sActiveMenu = eFMMC_DUMMYBLIP_OVERRIDE_POSITION
					
					IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = POS_DUMMYBLIP_OVERRIDE_USE_OVERRIDE
						BOOL bUsingOverride = FALSE
						PRINTLN("[ML] Currently selecting POS_DUMMYBLIP_OVERRIDE_USE_OVERRIDE")
						IF sFMMCMenu.iEntityCreation = CREATION_TYPE_DUMMY_BLIPS
							TOGGLE_MENU_BIT(g_FMMC_STRUCT.sCurrentDummyBlip.iBitSet, ciDUMMY_BLIP_POSITION_OVERRIDE)
							IF IS_BIT_SET(g_FMMC_STRUCT.sCurrentDummyBlip.iBitSet, ciDUMMY_BLIP_POSITION_OVERRIDE)
								bUsingOverride = TRUE
								g_FMMC_STRUCT.sCurrentDummyBlip.vOverridePosition = sCurrentVarsStruct.vCoronaPos
							ENDIF	
						ENDIF
						
						IF NOT bUsingOverride
							g_FMMC_STRUCT.sCurrentDummyBlip.vOverridePosition = <<0,0,0>>
						ENDIF
					ENDIF

				ELIF sFMMCmenu.sActiveMenu = eFmmc_ASS_OBJECTIVE_MENU
				AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_MENU_PROP_UPDATE_ROTATION_CLEAR
				AND sFMMCmenu.iEntityCreation = CREATION_TYPE_PROPS	
					sFMMCmenu.fUpdateZRotation = -1.0
					sFMMCmenu.iUpdateRotationTime = 0
				ELIF sFMMCmenu.sActiveMenu = efMMC_VISGROUPS
					IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) < ciBS_visGroup_MAX
						EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCmenu.iVisGroupBitSet, GET_CREATOR_MENU_SELECTION(sFMMCmenu))
					ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciBS_visGroup_MAX
						FOR i = 0 TO ciBS_visGroup_MAX
							SET_BIT(sFMMCmenu.iVisGroupBitSet, i)
						ENDFOR
					ELSE
						FOR i = 0 TO ciBS_visGroup_MAX
							CLEAR_BIT(sFMMCmenu.iVisGroupBitSet, i)
						ENDFOR
					ENDIF
				ELIF sFMMCmenu.sActiveMenu = eFMMC_AVAILABLE_VEHICLES
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciUSE_SUB_TEAM_ROLES)
						EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iAvailableMissionVehiclesForRole[sFMMCmenu.iSelectedTeam][sFMMCmenu.iSelectedRole][sFMMCmenu.iVehicleLibrary][GET_LONG_BITSET_INDEX(GET_CREATOR_MENU_SELECTION(sFMMCmenu))], GET_LONG_BITSET_BIT(GET_CREATOR_MENU_SELECTION(sFMMCmenu)))
						
						FOR i = 0 TO VEHICLE_LIBRARY_MAX - 1
							IF g_FMMC_STRUCT.iAvailableMissionVehiclesForRole[sFMMCmenu.iSelectedTeam][sFMMCmenu.iSelectedRole][g_FMMC_STRUCT.iDefaultMissionVehicleTypeForRole[sFMMCmenu.iSelectedTeam][sFMMCmenu.iSelectedRole]][0] = 0
							AND g_FMMC_STRUCT.iAvailableMissionVehiclesForRole[sFMMCmenu.iSelectedTeam][sFMMCmenu.iSelectedRole][g_FMMC_STRUCT.iDefaultMissionVehicleTypeForRole[sFMMCmenu.iSelectedTeam][sFMMCmenu.iSelectedRole]][1] = 0
								EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iDefaultMissionVehicleTypeForRole[sFMMCmenu.iSelectedTeam][sFMMCmenu.iSelectedRole], 1, VEHICLE_LIBRARY_MAX)
							ELSE
								BREAKLOOP
							ENDIF
						ENDFOR
						FOR i = 0 TO GET_NUMBER_OF_VEHICLES_IN_LIBRARY(g_FMMC_STRUCT.iDefaultMissionVehicleTypeForRole[sFMMCmenu.iSelectedTeam][sFMMCmenu.iSelectedRole]) - 1
							IF g_FMMC_STRUCT.iDefaultMissionVehicleTypeForRole[sFMMCmenu.iSelectedTeam][sFMMCmenu.iSelectedRole] >= 0
								IF NOT IS_BIT_SET(g_FMMC_STRUCT.iAvailableMissionVehiclesForRole[sFMMCmenu.iSelectedTeam][sFMMCmenu.iSelectedRole][g_FMMC_STRUCT.iDefaultMissionVehicleTypeForRole[sFMMCmenu.iSelectedTeam][sFMMCmenu.iSelectedRole]][GET_LONG_BITSET_INDEX(g_FMMC_STRUCT.iDefaultMissionVehicleForRole[sFMMCmenu.iSelectedTeam][sFMMCmenu.iSelectedRole])], GET_LONG_BITSET_BIT(g_FMMC_STRUCT.iDefaultMissionVehicleForRole[sFMMCmenu.iSelectedTeam][sFMMCmenu.iSelectedRole]))
									EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iDefaultMissionVehicleForRole[sFMMCmenu.iSelectedTeam][sFMMCmenu.iSelectedRole], 1, GET_NUMBER_OF_VEHICLES_IN_LIBRARY(g_FMMC_STRUCT.iDefaultMissionVehicleTypeForRole[sFMMCmenu.iSelectedTeam][sFMMCmenu.iSelectedRole]))
								ELSE
									BREAKLOOP
								ENDIF
							ELSE
								PRINTLN("eFMMC_AVAILABLE_VEHICLES 2 - g_FMMC_STRUCT.iDefaultMissionVehicleTypeForRole[",sFMMCmenu.iSelectedTeam,"][",sFMMCmenu.iSelectedRole,"] = ", g_FMMC_STRUCT.iDefaultMissionVehicleTypeForRole[sFMMCmenu.iSelectedTeam][sFMMCmenu.iSelectedRole])
							ENDIF
						ENDFOR
					ELSE
						EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iAvailableCommonVehicles[sFMMCmenu.iSelectedTeam][sFMMCmenu.iVehicleLibrary][GET_LONG_BITSET_INDEX(GET_CREATOR_MENU_SELECTION(sFMMCmenu))], GET_LONG_BITSET_BIT(GET_CREATOR_MENU_SELECTION(sFMMCmenu)))
						
						FOR i = 0 TO VEHICLE_LIBRARY_MAX - 1
							IF g_FMMC_STRUCT.iAvailableCommonVehicles[sFMMCmenu.iSelectedTeam][g_FMMC_STRUCT.iDefaultCommonVehicleType[sFMMCmenu.iSelectedTeam]][0] = 0
							AND g_FMMC_STRUCT.iAvailableCommonVehicles[sFMMCmenu.iSelectedTeam][g_FMMC_STRUCT.iDefaultCommonVehicleType[sFMMCmenu.iSelectedTeam]][1] = 0
								EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iDefaultCommonVehicleType[sFMMCmenu.iSelectedTeam], 1, VEHICLE_LIBRARY_MAX)
							ELSE
								BREAKLOOP
							ENDIF
						ENDFOR
						FOR i = 0 TO GET_NUMBER_OF_VEHICLES_IN_LIBRARY(g_FMMC_STRUCT.iDefaultCommonVehicleType[sFMMCmenu.iSelectedTeam]) - 1
							IF NOT IS_BIT_SET(g_FMMC_STRUCT.iAvailableCommonVehicles[sFMMCmenu.iSelectedTeam][g_FMMC_STRUCT.iDefaultCommonVehicleType[sFMMCmenu.iSelectedTeam]][GET_LONG_BITSET_INDEX(g_FMMC_STRUCT.iDefaultCommonVehicle[sFMMCmenu.iSelectedTeam])], GET_LONG_BITSET_BIT(g_FMMC_STRUCT.iDefaultCommonVehicle[sFMMCmenu.iSelectedTeam]))
								EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iDefaultCommonVehicle[sFMMCmenu.iSelectedTeam], 1, GET_NUMBER_OF_VEHICLES_IN_LIBRARY(g_FMMC_STRUCT.iDefaultCommonVehicleType[sFMMCmenu.iSelectedTeam]))
							ELSE
								BREAKLOOP
							ENDIF
						ENDFOR
					ENDIF
					REFRESH_MENU(sFMMCMenu)
				ELIF sFMMCmenu.sActiveMenu = efmmc_PED_HELI_ESCORT_OFFSET
				AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_HELI_ESCORT_OFFSET_CLEAR
					sFMMCMenu.vPedHeliEscortOffset = <<0,0,0>>
					REFRESH_MENU(sFMMCMenu)
				ELIF sFMMCmenu.sActiveMenu = eFMMC_AVAILABLE_VEHICLE_LIBRARIES
				AND sFMMCmenu.iCurrentSelection >= CORONA_VEHICLE_LIST_MAX
					INT iMenuLengthOffset = 0
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciUSE_SUB_TEAM_ROLES)
						iMenuLengthOffset = 1
					ENDIF
					INT iSelectedLibrary = GET_CREATOR_MENU_SELECTION(sFMMCmenu) - (CORONA_VEHICLE_LIST_MAX + iMenuLengthOffset)
					IF iSelectedLibrary >= 0
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciUSE_SUB_TEAM_ROLES)
							IF g_FMMC_STRUCT.iAvailableMissionVehiclesForRole[sFMMCmenu.iSelectedTeam][sFMMCmenu.iSelectedRole][iSelectedLibrary][0] != 0
							OR g_FMMC_STRUCT.iAvailableMissionVehiclesForRole[sFMMCmenu.iSelectedTeam][sFMMCmenu.iSelectedRole][iSelectedLibrary][1] != 0
								g_FMMC_STRUCT.iAvailableMissionVehiclesForRole[sFMMCmenu.iSelectedTeam][sFMMCmenu.iSelectedRole][iSelectedLibrary][0] = 0
								g_FMMC_STRUCT.iAvailableMissionVehiclesForRole[sFMMCmenu.iSelectedTeam][sFMMCmenu.iSelectedRole][iSelectedLibrary][1] = 0
							ELSE
								FOR i = 0 TO GET_NUMBER_OF_VEHICLES_IN_LIBRARY(iSelectedLibrary)-1
									SET_BIT(g_FMMC_STRUCT.iAvailableMissionVehiclesForRole[sFMMCmenu.iSelectedTeam][sFMMCmenu.iSelectedRole][iSelectedLibrary][GET_LONG_BITSET_INDEX(i)], GET_LONG_BITSET_BIT(i))
								ENDFOR
							ENDIF
							FOR i = 0 TO VEHICLE_LIBRARY_MAX - 1
								IF g_FMMC_STRUCT.iAvailableMissionVehiclesForRole[sFMMCmenu.iSelectedTeam][sFMMCmenu.iSelectedRole][g_FMMC_STRUCT.iDefaultMissionVehicleTypeForRole[sFMMCmenu.iSelectedTeam][sFMMCmenu.iSelectedRole]][0] = 0
								AND g_FMMC_STRUCT.iAvailableMissionVehiclesForRole[sFMMCmenu.iSelectedTeam][sFMMCmenu.iSelectedRole][g_FMMC_STRUCT.iDefaultMissionVehicleTypeForRole[sFMMCmenu.iSelectedTeam][sFMMCmenu.iSelectedRole]][1] = 0
									EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iDefaultMissionVehicleTypeForRole[sFMMCmenu.iSelectedTeam][sFMMCmenu.iSelectedRole], 1, VEHICLE_LIBRARY_MAX)
								ELSE
									BREAKLOOP
								ENDIF
							ENDFOR
							FOR i = 0 TO GET_NUMBER_OF_VEHICLES_IN_LIBRARY(g_FMMC_STRUCT.iDefaultMissionVehicleTypeForRole[sFMMCmenu.iSelectedTeam][sFMMCmenu.iSelectedRole]) - 1
								IF NOT IS_BIT_SET(g_FMMC_STRUCT.iAvailableMissionVehiclesForRole[sFMMCmenu.iSelectedTeam][sFMMCmenu.iSelectedRole][g_FMMC_STRUCT.iDefaultCommonVehicleType[sFMMCmenu.iSelectedTeam]][GET_LONG_BITSET_INDEX(g_FMMC_STRUCT.iDefaultMissionVehicleForRole[sFMMCmenu.iSelectedTeam][sFMMCmenu.iSelectedRole])], GET_LONG_BITSET_BIT(g_FMMC_STRUCT.iDefaultMissionVehicleForRole[sFMMCmenu.iSelectedTeam][sFMMCmenu.iSelectedRole]))
									EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iDefaultMissionVehicleForRole[sFMMCmenu.iSelectedTeam][sFMMCmenu.iSelectedRole], 1, GET_NUMBER_OF_VEHICLES_IN_LIBRARY(g_FMMC_STRUCT.iDefaultMissionVehicleTypeForRole[sFMMCmenu.iSelectedTeam][sFMMCmenu.iSelectedRole]))
								ELSE
									BREAKLOOP
								ENDIF
							ENDFOR
						ELSE
							IF g_FMMC_STRUCT.iAvailableCommonVehicles[sFMMCmenu.iSelectedTeam][iSelectedLibrary][0] != 0
							OR g_FMMC_STRUCT.iAvailableCommonVehicles[sFMMCmenu.iSelectedTeam][iSelectedLibrary][1] != 0
								g_FMMC_STRUCT.iAvailableCommonVehicles[sFMMCmenu.iSelectedTeam][iSelectedLibrary][0] = 0
								g_FMMC_STRUCT.iAvailableCommonVehicles[sFMMCmenu.iSelectedTeam][iSelectedLibrary][1] = 0
							ELSE
								FOR i = 0 TO GET_NUMBER_OF_VEHICLES_IN_LIBRARY(iSelectedLibrary)-1
									SET_BIT(g_FMMC_STRUCT.iAvailableCommonVehicles[sFMMCmenu.iSelectedTeam][iSelectedLibrary][GET_LONG_BITSET_INDEX(i)], GET_LONG_BITSET_BIT(i))
								ENDFOR
							ENDIF
							FOR i = 0 TO VEHICLE_LIBRARY_MAX - 1
								IF g_FMMC_STRUCT.iAvailableCommonVehicles[sFMMCmenu.iSelectedTeam][g_FMMC_STRUCT.iDefaultCommonVehicleType[sFMMCmenu.iSelectedTeam]][0] = 0
								AND	g_FMMC_STRUCT.iAvailableCommonVehicles[sFMMCmenu.iSelectedTeam][g_FMMC_STRUCT.iDefaultCommonVehicleType[sFMMCmenu.iSelectedTeam]][1] = 0
									EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iDefaultCommonVehicleType[sFMMCmenu.iSelectedTeam], 1, VEHICLE_LIBRARY_MAX)
								ELSE
									BREAKLOOP
								ENDIF
							ENDFOR
							FOR i = 0 TO GET_NUMBER_OF_VEHICLES_IN_LIBRARY(g_FMMC_STRUCT.iDefaultCommonVehicleType[sFMMCmenu.iSelectedTeam]) - 1
								IF NOT IS_BIT_SET(g_FMMC_STRUCT.iAvailableCommonVehicles[sFMMCmenu.iSelectedTeam][g_FMMC_STRUCT.iDefaultCommonVehicleType[sFMMCmenu.iSelectedTeam]][GET_LONG_BITSET_INDEX(g_FMMC_STRUCT.iDefaultCommonVehicle[sFMMCmenu.iSelectedTeam])], GET_LONG_BITSET_BIT(g_FMMC_STRUCT.iDefaultCommonVehicle[sFMMCmenu.iSelectedTeam]))
									EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.iDefaultCommonVehicle[sFMMCmenu.iSelectedTeam], 1, GET_NUMBER_OF_VEHICLES_IN_LIBRARY(g_FMMC_STRUCT.iDefaultCommonVehicleType[sFMMCmenu.iSelectedTeam]))
								ELSE
									BREAKLOOP
								ENDIF
							ENDFOR
						ENDIF
					ENDIF
					REFRESH_MENU(sFMMCmenu)
				ELIF sFMMCmenu.sActiveMenu = eFMMC_DEFAULT_VEHICLE_LIBRARIES
					//Put something in here maybe
					
				ELIF sFMMCmenu.sActiveMenu = eFmmc_PED_FOLLOW_TEAM_OPTIONS
					IF NOT IS_BIT_SET(sFMMCmenu.iActorMenuBitsetTwo, ciPED_BSTwo_AlwaysFollowTeam0+sFMMCMenu.iSelectedTeam)
						IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_FOLLOW_TEAM_OPT_CLEAR_FOLLOW_RULES
							sFMMCmenu.iFollowTeamOnRuleBS[sFMMCMenu.iSelectedTeam] = 0
						ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) >= PED_FOLLOW_TEAM_OPT_FOLLOW_ON_RULE
							INT iRule = GET_CREATOR_MENU_SELECTION(sFMMCmenu) - PED_FOLLOW_TEAM_OPT_FOLLOW_ON_RULE
							EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCmenu.iFollowTeamOnRuleBS[sFMMCMenu.iSelectedTeam], iRule)
						ENDIF
					ENDIF
				ELIF sFMMCmenu.sActiveMenu = eFMMC_AVAILABLE_VERSUS_OUTFITS
					IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) < sFMMCmenu.iCurrentMenuLength - 1
						IF IS_VERSUS_OUTFIT_STYLE_VALID(sFMMCMenu.iSelectedVsStyle, INT_TO_ENUM(VERSUS_OUTFIT_STYLE, GET_CREATOR_MENU_SELECTION(sFMMCmenu)), g_fmmc_Struct.iMaxNumberOfTeams)
						OR IS_BIT_SET(g_FMMC_STRUCT.iBS_MissionTeamRelations, FMMC_TEAM_BALANCING)
							TOGGLE_MENU_BIT(g_FMMC_STRUCT.biVSAvailableOutfit[sFMMCmenu.iSelectedVsStyle][GET_LONG_BITSET_INDEX(GET_CREATOR_MENU_SELECTION(sFMMCmenu))], GET_LONG_BITSET_BIT(GET_CREATOR_MENU_SELECTION(sFMMCmenu)))
						ENDIF
					ELSE
						TOGGLE_MENU_BIT(g_FMMC_STRUCT.biVSAllowFutureOutfits, sFMMCmenu.iSelectedVsStyle)
					ENDIF
				ELIF sFMMCmenu.sActiveMenu = eFmmc_CLOTHING_OPTIONS
				AND sFMMCMenu.sMenuGoto[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] = eFmmc_Null_item
					IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) >= CLOTHING_OPTIONS_MAX_FIXED
						IF (GET_CREATOR_MENU_SELECTION(sFMMCmenu) - CLOTHING_OPTIONS_MAX_FIXED)%2 = 0
						AND sFMMCMenu.iSelectedVsStyle != -1
							IF g_fmmc_struct.biVSAvailableOutfit[sFMMCMenu.iSelectedVsStyle][GET_LONG_BITSET_INDEX(sFMMCMenu.iSelectedVsStyle)] != 0
								g_fmmc_struct.biVSAvailableOutfit[sFMMCMenu.iSelectedVsStyle][GET_LONG_BITSET_INDEX(sFMMCMenu.iSelectedVsStyle)] = 0
								g_FMMC_STRUCT.iVSDefaultOutfit[sFMMCMenu.iSelectedVsStyle] = -1
							ELSE
								REPEAT VS_CLASSIC_MAX i
									IF IS_VERSUS_OUTFIT_STYLE_VALID(sFMMCMenu.iSelectedVsStyle, INT_TO_ENUM(VERSUS_OUTFIT_STYLE, i), g_FMMC_STRUCT.iMaxNumberOfTeams)
									OR IS_BIT_SET(g_FMMC_STRUCT.iBS_MissionTeamRelations, FMMC_TEAM_BALANCING)										
										SET_BIT(g_fmmc_struct.biVSAvailableOutfit[sFMMCMenu.iSelectedVsStyle][GET_LONG_BITSET_INDEX(sFMMCMenu.iSelectedVsStyle)], GET_LONG_BITSET_BIT(i))
									ENDIF
								ENDREPEAT
							ENDIF
						ENDIF
					ENDIF
				ELIF sFMMCmenu.sActiveMenu = eFmmc_TURF_WAR_OPTIONS
				AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_TURF_WAR_SET_STATIC_TOPDOWN_CAM
					PRINTLN("[LM][TURF WAR] - Saving this position and rotation in the Creator for the static Turf War Cam.")
					
					IF IS_VECTOR_ZERO(g_FMMC_STRUCT.vTWCamStaticPosition)
						PRINTLN("[LM][TURF WAR] - We didn't have a position prior to this.")
						SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_TURF_WAR_TOP_DOWN_CAMERA)
					ENDIF
					g_FMMC_STRUCT.vTWCamStaticPosition = GET_CAM_COORD(sCamData.cam)
					g_FMMC_STRUCT.vTWCamStaticRotation = GET_CAM_ROT(sCamData.cam)
					
					PRINTLN("[LM][TURF WAR] - New Cam Pos: ", g_FMMC_STRUCT.vTWCamStaticPosition)
					PRINTLN("[LM][TURF WAR] - New Cam Rot: ", g_FMMC_STRUCT.vTWCamStaticRotation)
					
				ELIF sFMMCmenu.sActiveMenu = eFmmc_SCRIPT_CAM_OPTIONS
				AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_CREATE_CAMERA_LOCATION
					
					IF IS_VECTOR_ZERO(g_FMMC_STRUCT.vBEVCamFSLPos[sFMMCmenu.iSelectedStaticCamLocation])
						PRINTLN("[LM][TURF WAR] - We didn't have a position prior to this.")
					ENDIF
					g_FMMC_STRUCT.vBEVCamFSLPos[sFMMCmenu.iSelectedStaticCamLocation] = GET_CAM_COORD(sCamData.cam)
					g_FMMC_STRUCT.vBEVCamFSLRotation[sFMMCmenu.iSelectedStaticCamLocation] = GET_CAM_ROT(sCamData.cam)
					
				ELIF sFMMCmenu.sActiveMenu = eFmmc_SCRIPT_CAM_OPTIONS
				AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_CREATE_CAMERA_LOCATION_DEBUG
					
					IF IS_VECTOR_ZERO(g_FMMC_STRUCT.vBEVCamFSLPos[sFMMCmenu.iSelectedStaticCamLocation])
						PRINTLN("[LM][TURF WAR] - We didn't have a position prior to this.")
					ENDIF
					g_FMMC_STRUCT.vBEVCamFSLPos[sFMMCmenu.iSelectedStaticCamLocation] = GET_CAM_COORD(GET_DEBUG_CAM())
					g_FMMC_STRUCT.vBEVCamFSLRotation[sFMMCmenu.iSelectedStaticCamLocation] = GET_CAM_ROT(GET_DEBUG_CAM())
					
				ELIF sFMMCmenu.sActiveMenu = eFmmc_SCRIPT_CAM_OPTIONS
				AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_DELETE_CAMERA_LOCATION
					
					IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.vBEVCamFSLPos[sFMMCmenu.iSelectedStaticCamLocation])
						PRINTLN("[LM][TURF WAR] - We are deleting a valid point.")
					ENDIF
					g_FMMC_STRUCT.vBEVCamFSLPos[sFMMCmenu.iSelectedStaticCamLocation] = <<0.0, 0.0, 0.0>>
					g_FMMC_STRUCT.vBEVCamFSLRotation[sFMMCmenu.iSelectedStaticCamLocation] = <<0.0, 0.0, 0.0>>
					
				ELIF sFMMCmenu.sActiveMenu = eFmmc_OVERTIME_OPTIONS_POINTS_ZONE
				AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_OT_PAZ_CREATE_AND_MODIFY_NEW_POINTS_ZONE
					// Create
					PRINTLN("[LM][OVERTIME][CREATE_POINTS_ZONE] - Button Pressed to create point allocation zone.")
				
				ELIF sFMMCmenu.sActiveMenu = eFmmc_OVERTIME_OPTIONS_POINTS_ZONE_EDIT
				AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_OT_PAZ_DELETE_POINT_ZONE_INDEX
				AND sFMMCMenu.sMenuGoto[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] = eFmmc_Null_item
					// Delete
					PRINTLN("[LM][OVERTIME][EDIT_POINTS_ZONE] - DELETE OVERTIME ZONE.")
					DELETE_THIS_OVERTIME_ZONE(sFMMCMenu.iSelectedOvertimeZone, sFMMCmenu)

				ELIF sFMMCmenu.sActiveMenu = eFMMC_QRC_SPAWN_POINTS
				AND (GET_CREATOR_MENU_SELECTION(sFMMCmenu) >= QRC_SPAWN_POINT_MENU_CHECKPOINT_0) AND (GET_CREATOR_MENU_SELECTION(sFMMCmenu) <= QRC_SPAWN_POINT_MENU_CHECKPOINT_3)
					INT iCheckpoint = GET_CREATOR_MENU_SELECTION(sFMMCmenu) - QRC_SPAWN_POINT_MENU_CHECKPOINT_0
					TOGGLE_MENU_BIT(sFMMCMenu.iQRCSpawnPoint_ActiveOnCheckpointBS, iCheckpoint)
				ELIF sFMMCmenu.sActiveMenu = eFMMC_QRC_SPAWN_POINTS
				AND (GET_CREATOR_MENU_SELECTION(sFMMCmenu) >= QRC_SPAWN_POINT_MENU_SPAWN_GROUP_1) AND (GET_CREATOR_MENU_SELECTION(sFMMCmenu) <= QRC_SPAWN_POINT_MENU_SPAWN_GROUP_10)
					INT iSpawnGroup = GET_CREATOR_MENU_SELECTION(sFMMCmenu) - QRC_SPAWN_POINT_MENU_SPAWN_GROUP_1
					TOGGLE_MENU_BIT(sFMMCMenu.iQRCSpawnPoint_ActiveWithSpawnGroupBS, iSpawnGroup)
				ELIF sFMMCmenu.sActiveMenu = eFMMC_TEAM_RESPAWN_IN_LAST_VEHICLE_LIST
					EDIT_CREATOR_LINKED_ENTITY_BITSET_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.sFMMCEndConditions[sFMMCMenu.iSelectedTeam].iRespawnInLastMissionVehicleFromBS, GET_CREATOR_MENU_SELECTION(sFMMCmenu), ciFMMC_ENTITY_LINK_TYPE_VEHICLE)
				ELIF sFMMCmenu.sActiveMenu = eFmmc_OBJECT_ATTACH_OFFSET
				AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = EXPLICIT_VECTOR_CLEAR
					sFMMCmenu.vObjectAttachOffset = <<0,0,0>>
					sFMMCmenu.vObjectAttachOffsetRotation = <<0,0,0>>
				ELIF sFMMCmenu.sActiveMenu = eFMMC_TEAM_POST_MISSION_PLAYER_SPAWN
					INT iTeam = sFMMCmenu.iSelectedTeam
					IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = FMMC_POST_MISSION_LOCATION
						SET_POSITION_FROM_CREATOR(sFMMCdata, sCurrentVarsStruct, g_FMMC_STRUCT.vPostMissionPos[iTeam])
					ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = FMMC_POST_MISSION_POINT_OF_INTEREST
						SET_POSITION_FROM_CREATOR(sFMMCdata, sCurrentVarsStruct, g_FMMC_STRUCT.vPostMissionPOI[iTeam])
					ENDIF
				ELIF sFMMCmenu.sActiveMenu = eFmmc_ELEVATORS
				AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ELEVATOR_OPTIONS_INDEX
					IF IS_VECTOR_ZERO(g_FMMC_STRUCT.sElevators[sFMMCMenu.iSelectedElevator].vActivationZonePosition[0])
					AND IS_VECTOR_ZERO(g_FMMC_STRUCT.sElevators[sFMMCMenu.iSelectedElevator].vActivationZonePosition[1])
						VECTOR vTempElevatorPos
						SET_POSITION_FROM_CREATOR(sFMMCdata, sCurrentVarsStruct, vTempElevatorPos)
						g_FMMC_STRUCT.sElevators[sFMMCMenu.iSelectedElevator].vActivationZonePosition[0] = vTempElevatorPos + <<1.0, 1.0, 2.0>>
						g_FMMC_STRUCT.sElevators[sFMMCMenu.iSelectedElevator].vActivationZonePosition[1] = vTempElevatorPos - <<1.0, 1.0, 0.0>>
						g_FMMC_STRUCT.sElevators[sFMMCMenu.iSelectedElevator].vLiftCallPosition = vTempElevatorPos
						g_FMMC_STRUCT.sElevators[sFMMCMenu.iSelectedElevator].fActivationZoneWidth = 1.0
						INT iPlayerWarp
						FOR iPlayerWarp = 0 TO FMMC_MAX_ELEVATOR_PLAYERS - 1
							g_FMMC_STRUCT.sElevators[sFMMCMenu.iSelectedElevator].vPlayerWarpPositions[iPlayerWarp] = vTempElevatorPos
						ENDFOR
					ENDIF		
				ELSE
					IF IS_THIS_OPTION_SELECTABLE()
						sCurrentVarsStruct.bResetUpHelp = TRUE
						PRINTSTRING("INPUT_FRONTEND_ACCEPT")PRINTNL()
						IF sFMMCmenu.sActiveMenu = eFmmc_MAIN_MENU_BASE
							PLAY_SOUND_FRONTEND(-1, "SELECT", GET_SOUND_SET_FROM_CREATOR_TYPE())
							PRINTSTRING("CONFIRM SOUND EFFECT!!!")PRINTNL()
						ENDIF
						
						IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) != -1
							
							IF sFMMCmenu.sActiveMenu = efMMC_CUTSCENE_ADD_SCENE
								IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) > ADD_CUTSCENE_MENU_ADD_SHOT
									sFMMCmenu.iCurrentShot = GET_CREATOR_MENU_SELECTION(sFMMCmenu)-(ADD_CUTSCENE_MENU_ADD_SHOT+1)
								ELSE
									sFMMCmenu.iCurrentShot = g_fmmc_struct.sCurrentSceneData.iNumCamShots
								ENDIF
								IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ADD_CUTSCENE_MENU_CONFIRM_SCENE
									COPY_CUTSCENE_DATA(g_fmmc_struct.sCurrentSceneData, g_FMMC_STRUCT.sScriptedCutsceneData[sFMMCmenu.iCurrentCutScene])
									CLEAR_CUTSCENE_DATA(g_fmmc_struct.sCurrentSceneData)
								ENDIF
							ENDIF
							
							IF sFMMCmenu.sActiveMenu = efMMC_CUTSCENE_ADD_MOCAP_SCENE
								IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ADD_MOCAP_CUTSCENE_MENU_CONFIRM_SCENE
									g_FMMC_STRUCT.sMocapCutsceneData[sFMMCmenu.iCurrentCutScene] = g_fmmc_struct.sCurrentMocapSceneData
									CLEAR_MOCAP_CUTSCENE_DATA(g_fmmc_struct.sCurrentMocapSceneData)
								ENDIF
							ENDIF
							
							IF sFMMCmenu.sActiveMenu = efMMC_CUTSCENE_ADD_SHOT
								IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ADD_SHOT_MENU_CONFIRM_SHOT
									IF g_fmmc_struct.sCurrentSceneData.iNumCamShots = sFMMCmenu.iCurrentShot
									AND g_fmmc_struct.sCurrentSceneData.iNumCamShots < MAX_CAMERA_SHOTS
										g_fmmc_struct.sCurrentSceneData.iNumCamShots++
									ENDIF
								ENDIF
							ENDIF
							
							IF sFMMCmenu.sMenuGoto[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] != eFmmc_Null_item
							AND NOT (IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, GET_CREATOR_RT_BUTTON())	AND IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, GET_CREATOR_LT_BUTTON()))
								IF sFMMCmenu.sActiveMenu != eFmmc_MAIN_MENU_BASE
									PLAY_SOUND_FRONTEND(-1, "SELECT", GET_SOUND_SET_FROM_CREATOR_TYPE())
									PRINTSTRING("CONFIRM SOUND EFFECT!!!")PRINTNL()
								ENDIF
																
								IF sFMMCmenu.sActiveMenu = eFmmc_STARTING_TEAM_INVENTORY
								OR sFMMCmenu.sActiveMenu = eFmmc_MIDMISSION_TEAM_INVENTORY
								OR sFMMCmenu.sActiveMenu = eFmmc_MIDMISSION_TEAM_INVENTORY_2
									IF sFMMCmenu.sMenuGoto[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] = eFMMC_MKII_WEAPON_AMMO
									OR sFMMCmenu.sMenuGoto[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] = eFMMC_INVENTORY_WEAPON_SEL
										sFMMCmenu.sMenuBack = sFMMCmenu.sActiveMenu
									ENDIF
								ENDIF
								
								IF sFMMCmenu.sActiveMenu = efMMC_CUTSCENE_BASE
									BOOL bMocap = FALSE
									INT iSelection = GET_CREATOR_MENU_SELECTION(sFMMCmenu)
									IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) > FMMC_GET_MAX_SCRIPTED_CUTSCENES()
										iSelection -= (FMMC_GET_MAX_SCRIPTED_CUTSCENES() + 1)
										bMocap = TRUE
									ENDIF
									IF iSelection > 0
										sFMMCmenu.iCurrentCutScene = iSelection - 1
										IF !bMocap
											IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sScriptedCutsceneData[sFMMCmenu.iCurrentCutScene].tlName)
												COPY_CUTSCENE_DATA(g_FMMC_STRUCT.sScriptedCutsceneData[sFMMCmenu.iCurrentCutScene], g_fmmc_struct.sCurrentSceneData)
											ENDIF
										ELSE
											IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sMocapCutsceneData[sFMMCmenu.iCurrentCutScene].tlName)
												g_fmmc_struct.sCurrentMocapSceneData = g_FMMC_STRUCT.sMocapCutsceneData[sFMMCmenu.iCurrentCutScene]
											ENDIF
										ENDIF
									ELSE
										IF bMocap
											FOR i = 0 TO MAX_MOCAP_CUTSCENES - 1
												IF IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sMocapCutsceneData[i].tlName)
													sFMMCmenu.iCurrentCutScene = i
													BREAKLOOP
												ENDIF
											ENDFOR
										ELSE
											FOR i = 0 TO FMMC_GET_MAX_SCRIPTED_CUTSCENES() - 1
												IF IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sScriptedCutsceneData[i].tlName)
													sFMMCmenu.iCurrentCutScene = i
													BREAKLOOP
												ENDIF
											ENDFOR
										ENDIF
									ENDIF
								ENDIF
								
								IF sFMMCmenu.sActiveMenu = eFMMC_Inventory_Base
									sFMMCMenu.iCurrentInventory = GET_CREATOR_MENU_SELECTION(sFMMCmenu)
									PRINTLN("[INVENTORY] sFMMCMenu.iCurrentInventory set to: ", sFMMCMenu.iCurrentInventory)
								ELIF sFMMCmenu.sActiveMenu = eFMMC_Inventory_Weapon_List
									IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) < FMMC_MAX_INVENTORY_WEAPONS
										sFMMCMenu.iCurrentInventoryWeapon = GET_CREATOR_MENU_SELECTION(sFMMCmenu)
										WEAPON_TYPE wtWep = g_FMMC_STRUCT.sPlayerWeaponInventories[sFMMCMenu.iCurrentInventory].sWeaponStruct[sFMMCMenu.iCurrentInventoryWeapon].wtWeapon
										IF wtWep != WEAPONTYPE_INVALID
											SET_WEAPON_LIBRARY_AND_TYPE_FROM_WEAPON_TYPE(sFMMCmenu, sWepStruct, wtWep)
										ELSE
											sFMMCmenu.iWepLibrary = -1
											sFMMCmenu.iCurrentEntitySelection[CREATION_TYPE_WEAPON_LIBRARY] = -1
										ENDIF
										PRINTLN("[INVENTORY] sFMMCMenu.iCurrentInventoryWeapon set to: ", sFMMCMenu.iCurrentInventoryWeapon)
									ENDIF
								ENDIF
								
								IF sFMMCmenu.sMenuGoto[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] = efmmc_IPL_ISLAND_CONTINUITY
									sFMMCmenu.iIPLContinuityConditional = 0
								ENDIF
																
								GO_TO_MENU(sFMMcmenu, sFMMCdata)
																
								IF sFMMCmenu.sActiveMenu = eFmmc_PAN_CAM_BASE
								OR sFMMCmenu.sActiveMenu = eFmmc_OUT_CAM_BASE
								OR sFMMCmenu.sActiveMenu = eFmmc_DM_TEAM_CAMERAS
								OR sFMMCmenu.sActiveMenu = eFmmc_SET_ELEVATOR_CAM
								OR sFMMCmenu.sActiveMenu = efMMC_CUTSCENE_SET_CAM_1
								OR sFMMCmenu.sActiveMenu = efMMC_CUTSCENE_SET_CAM_2
								OR sFMMCmenu.sActiveMenu = efMMC_CUTSCENE_SET_CAM_3
								OR sFMMCmenu.sActiveMenu = efMMC_TRIP_SKIP_CAM
									IF IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
										sCurrentVarsStruct.iMenuState = MENU_STATE_SWITCH_CAM
									ENDIF
								ENDIF
								
								CLEAR_BIT(sFMMCdata.iBitSet, bPerformingPlacementShapeTest)
								RETURN eFmmc_Action_Null
								
							ENDIF
						ENDIF		
						
						IF sFMMCmenu.sMenuAction[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] != eFmmc_Action_Null
						
							IF (sFMMCmenu.sActiveMenu = eFmmc_PAN_CAM_BASE AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 0)								
							OR (sFMMCmenu.sActiveMenu = eFmmc_DM_TEAM_CAMERAS AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) != 2)
							
								VECTOR vStart 
								IF sFMMCmenu.sActiveMenu = eFmmc_DM_TEAM_CAMERAS
									IF sFMMCmenu.iSelectedCameraTeam != -1
										IF g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[sFMMCmenu.iSelectedCameraTeam] > 0
											vStart = g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sFMMCmenu.iSelectedCameraTeam][0].vPos 
										ENDIF
									ENDIF
								ELSE
									vStart = g_FMMC_STRUCT.vStartPos
								ENDIF
								
								IF bInitialIntroCamSetup
									RETURN eFmmc_Action_Null
								ELIF GET_DISTANCE_BETWEEN_COORDS(vStart, GET_ENTITY_COORDS(PLAYER_PED_ID())) > 100
									PLAY_SOUND_FRONTEND(-1, "ERROR", GET_SOUND_SET_FROM_CREATOR_TYPE())
									PRINTSTRING("ERROR SOUND EFFECT")PRINTNL()
									sCurrentVarsStruct.bDisplayFailReason = TRUE
									sCurrentVarsStruct.iFailDescriptionTimer = GET_GAME_TIMER()
									sCurrentVarsStruct.sFailReason = "FMMC_ER_023"
									RETURN eFmmc_Action_Null
								ENDIF
								
							ENDIF
							
							RETURN  sFMMCmenu.sMenuAction[GET_CREATOR_MENU_SELECTION(sFMMCmenu)]
						ENDIF
					ELSE
						PLAY_SOUND_FRONTEND(-1, "ERROR", GET_SOUND_SET_FROM_CREATOR_TYPE())
						PRINTSTRING("ERROR SOUND EFFECT")PRINTNL()
					ENDIF
				ENDIF
				REFRESH_MENU(sFMMCmenu)
			ENDIF
			
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
			OR SHOULD_FORCE_MENU_BACKOUT(sFMMCMenu)
				
				IF sFMMCmenu.sActiveMenu = efMMC_CUTSCENE_ADD_SHOT
					IF sFMMCmenu.iCurrentShot = g_fmmc_struct.sCurrentSceneData.iNumCamShots
						CLEAR_SHOT_DATA(g_fmmc_struct.sCurrentSceneData, sFMMCmenu.iCurrentShot)
					ENDIF
				ENDIF
				
				IF sFMMCmenu.sActiveMenu = efMMC_CUTSCENE_ADD_SCENE
					IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sScriptedCutsceneData[sFMMCmenu.iCurrentCutScene].tlName)
						CLEAR_CUTSCENE_DATA(g_fmmc_struct.sCurrentSceneData)
					ENDIF
				ENDIF
				
				IF sFMMCmenu.sActiveMenu = efMMC_CUTSCENE_ADD_MOCAP_SCENE
					IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sMocapCutsceneData[sFMMCmenu.iCurrentCutScene].tlName)
						CLEAR_MOCAP_CUTSCENE_DATA(g_fmmc_struct.sCurrentMocapSceneData)
					ENDIF
				ENDIF
				
				IF sFMMCmenu.sActiveMenu = eFmmc_SNAPPING_OPTIONS
				OR sFMMCmenu.sActiveMenu = eFmmc_PROP_ADVANCED_OPTIONS
					CHECK_IF_SNAPPING_SHOULD_BE_TURNED_OFF(sFMMCmenu)
				ENDIF
				
				IF sFMMCmenu.sActiveMenu = eFmmc_ASS_OBJECTIVE_MENU_SET_POOL_GOTO_POS
					INT iPoolIndex = sFMMCmenu.iSelectedEntity
					IF iPoolIndex != -1
					AND iPoolIndex < ASSOCIATED_GOTO_TASK_POOL
						COPY_ASSOCIATED_GOTO_TASK_DATA(g_FMMC_STRUCT.sAssociatedGotoPool[iPoolIndex].sAssociatedGotoTaskData, sFMMCmenu.sAssociatedGotoTaskData)	
					ENDIF
				ENDIF
				
				IF sFMMCmenu.sActiveMenu = eFmmc_ASS_OBJECTIVE_MENU_MAIN_POOL_GOTO_POS				
					CLEAR_ALL_PED_ASSOCIATED_GOTO_DATA(sFMMCmenu.sAssociatedGotoTaskData)
					IF sFMMCmenu.sMenuBack != eFmmc_Null_item
						PRINTLN("CANCEL_ENTITY_CREATION - Pool GOTO Going back to menu ", ENUM_TO_INT(sFMMCmenu.sMenuBack))
						PLAY_SOUND_FRONTEND(-1, "BACK", GET_SOUND_SET_FROM_CREATOR_TYPE())
						GO_BACK_TO_MENU(sFMMCmenu)
					ENDIF
				ENDIF
				
				IF sFMMCmenu.sActiveMenu = eFmmc_PROP_BASE
				OR sFMMCmenu.iEntityCreation != CREATION_TYPE_PROPS
					sFMMCmenu.bChainSnapBroken = FALSE
					sPropStruct.iRotTimerStart = -1
					sFMMCmenu.bForceRotate = FALSE
				ENDIF
				
				IF sFMMCmenu.sActiveMenu = eFmmc_VEHICLES_BASE
					sFMMCmenu.bPlacementOverride = FALSE
				ENDIF
				
				UPDATE_MENU_FOR_TEST_MISSION_STATE()
				sCurrentVarsStruct.bDisplayFailReason = FALSE
				sFMMCmenu.iNewRule = 0				
				sCurrentVarsStruct.bResetUpHelp = TRUE
				INT iSaveEntityType = sFMMCmenu.iEntityCreation
				PRINTLN("iSaveEntityType = ", iSaveEntityType)
				
				IF IS_MENU_A_RESET_MENU(sFMMCmenu.sMenuBack)
					CANCEL_ENTITY_CREATION(sFMMCmenu.iEntityCreation)
				ELSE
					PRINTLN("Not calling CANCEL_ENTITY_CREATION because we're just coming out of a submenu")
				ENDIF
				
				IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION
				AND bSubmenuActive
					SET_CREATION_TYPE(sFMMCMenu, iSaveEntityType)
				ENDIF
				
				IF (sFMMCMenu.iSelectedEntity != -1 AND (NOT bSubmenuActive OR sFMMCmenu.sActiveMenu = eFMMC_ROCKET_POSITIONS))
				OR (sFMMCmenu.sActiveMenu = eFmmc_DOORS_LEGACY AND sFMMCMenu.sCurrentDoor.mnDoorModel != DUMMY_MODEL_FOR_SCRIPT)
				OR (sFMMCmenu.sActiveMenu = eFmmc_DOORS_BASE AND sFMMCMenu.sCurrentDoor.mnDoorModel != DUMMY_MODEL_FOR_SCRIPT)
				OR (sFMMCmenu.sActiveMenu = efMMC_WORLD_PROPS AND sFMMCMenu.sCurrentWorldProp.mn != DUMMY_MODEL_FOR_SCRIPT)
					PRINTLN("CANCEL_ENTITY_CREATION - Setting iSelectedEntity to -1")
					sFMMCMenu.iSelectedEntity 		= -1
					
				ELSE
					IF sFMMCmenu.sMenuBack != eFmmc_Null_item
						PRINTLN("CANCEL_ENTITY_CREATION - Going back to menu ", ENUM_TO_INT(sFMMCmenu.sMenuBack))
						PLAY_SOUND_FRONTEND(-1, "BACK", GET_SOUND_SET_FROM_CREATOR_TYPE())
						GO_BACK_TO_MENU(sFMMCmenu)
					ENDIF
				ENDIF
				
				REFRESH_MENU(sFMMCmenu)
				
			ENDIF
		ENDIF
		
		IF MENU_CONTROL_DOWN_CREATOR(menuScrollController)
			SET_CREATOR_MENU_SELECTION(sFMMCmenu, GET_CREATOR_MENU_SELECTION(sFMMCmenu)+1)		
			
			WHILE DEAL_WITH_SKIPPING_EMPTY_OPTIONS_MC(1)
				PRINTLN("Skipping options using DEAL_WITH_SKIPPING_EMPTY_OPTIONS_MC...")
			ENDWHILE
			
			IF g_FMMC_STRUCT.bMissionIsPublished
				IF sFMMCmenu.sActiveMenu = eFmmc_TOP_MENU
				AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TOP_MENU_SAVE
					SET_CREATOR_MENU_SELECTION(sFMMCmenu, GET_CREATOR_MENU_SELECTION(sFMMCmenu)+1)
				ENDIF
			ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) > sFMMCmenu.iCurrentMenuLength-1
				SET_CREATOR_MENU_SELECTION(sFMMCmenu, 0)
			ENDIF
			
			sCurrentVarsStruct.bResetUpHelp = TRUE
			IF sFMMCmenu.iCurrentMenuLength > 1
				PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", GET_SOUND_SET_FROM_CREATOR_TYPE())
			ENDIF
			
			IF sFMMCmenu.sActiveMenu = eFmmc_JUMP_TO_OBJECTIVE_PLAYER_RULES_MENU
			OR sFMMCmenu.sActiveMenu = eFmmc_JUMP_TO_OBJECTIVE_LOCATION_MENU
			OR sFMMCmenu.sActiveMenu = eFmmc_JUMP_TO_OBJECTIVE_PED_MENU
			OR sFMMCmenu.sActiveMenu = eFmmc_JUMP_TO_OBJECTIVE_VEHICLE_MENU
			OR sFMMCmenu.sActiveMenu = eFmmc_JUMP_TO_OBJECTIVE_OBJECT_MENU			
				INT iTeam = GET_CREATOR_MENU_SELECTION(sFMMCmenu)/2
				
				IF sFMMCMenu.iSelectedEntity = -1
					IF sFMMCmenu.sActiveMenu = eFmmc_JUMP_TO_OBJECTIVE_PLAYER_RULES_MENU
						iPedToChange = g_FMMC_STRUCT.iNumberOfPlayerRules[iTeam]
					ELIF sFMMCmenu.sActiveMenu = eFmmc_JUMP_TO_OBJECTIVE_LOCATION_MENU
						iPedToChange = g_FMMC_STRUCT_ENTITIES.iNumberOfGoToLocations[iTeam]
					ELIF sFMMCmenu.sActiveMenu = eFmmc_JUMP_TO_OBJECTIVE_PED_MENU
						iPedToChange = g_FMMC_STRUCT_ENTITIES.iNumberOfPeds
					ELIF sFMMCmenu.sActiveMenu = eFmmc_JUMP_TO_OBJECTIVE_VEHICLE_MENU
						iPedToChange = g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles
					ELIF sFMMCmenu.sActiveMenu = eFmmc_JUMP_TO_OBJECTIVE_OBJECT_MENU
						iPedToChange = g_FMMC_STRUCT_ENTITIES.iNumberOfObjects
					ENDIF
					IF iPedToChange = (-1)
						iPedToChange = 0
					ENDIF					
				ELSE
					iPedToChange = sFMMCMenu.iSelectedEntity
				ENDIF
				
			ENDIF
			REFRESH_MENU(sFMMCmenu)
		ENDIF
		
		IF MENU_CONTROL_UP_CREATOR(menuScrollController)
			SET_CREATOR_MENU_SELECTION(sFMMCmenu, GET_CREATOR_MENU_SELECTION(sFMMCmenu)-1)	
			WHILE DEAL_WITH_SKIPPING_EMPTY_OPTIONS_MC(-1)
				PRINTLN("Skipping options using DEAL_WITH_SKIPPING_EMPTY_OPTIONS_MC...")
			ENDWHILE
			
			IF g_FMMC_STRUCT.bMissionIsPublished
				IF sFMMCmenu.sActiveMenu = eFmmc_TOP_MENU
				AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TOP_MENU_SAVE
					SET_CREATOR_MENU_SELECTION(sFMMCmenu, GET_CREATOR_MENU_SELECTION(sFMMCmenu)-1)
				ENDIF
			ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) < 0
				SET_CREATOR_MENU_SELECTION(sFMMCmenu, sFMMCmenu.iCurrentMenuLength - 1)
			ENDIF
			
			sCurrentVarsStruct.bResetUpHelp = TRUE
			IF sFMMCmenu.iCurrentMenuLength > 1
				PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", GET_SOUND_SET_FROM_CREATOR_TYPE())
			ENDIF
			
			IF sFMMCmenu.sActiveMenu = eFmmc_JUMP_TO_OBJECTIVE_PLAYER_RULES_MENU
			OR sFMMCmenu.sActiveMenu = eFmmc_JUMP_TO_OBJECTIVE_LOCATION_MENU
			OR sFMMCmenu.sActiveMenu = eFmmc_JUMP_TO_OBJECTIVE_PED_MENU
			OR sFMMCmenu.sActiveMenu = eFmmc_JUMP_TO_OBJECTIVE_VEHICLE_MENU
			OR sFMMCmenu.sActiveMenu = eFmmc_JUMP_TO_OBJECTIVE_OBJECT_MENU		
				INT iTeam = GET_CREATOR_MENU_SELECTION(sFMMCmenu)/2
				
				IF sFMMCMenu.iSelectedEntity = -1
					IF sFMMCmenu.sActiveMenu = eFmmc_JUMP_TO_OBJECTIVE_PLAYER_RULES_MENU
						iPedToChange = g_FMMC_STRUCT.iNumberOfPlayerRules[iTeam]
					ELIF sFMMCmenu.sActiveMenu = eFmmc_JUMP_TO_OBJECTIVE_LOCATION_MENU
						iPedToChange = g_FMMC_STRUCT_ENTITIES.iNumberOfGoToLocations[iTeam]
					ELIF sFMMCmenu.sActiveMenu = eFmmc_JUMP_TO_OBJECTIVE_PED_MENU
						iPedToChange = g_FMMC_STRUCT_ENTITIES.iNumberOfPeds
					ELIF sFMMCmenu.sActiveMenu = eFmmc_JUMP_TO_OBJECTIVE_VEHICLE_MENU
						iPedToChange = g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles
					ELIF sFMMCmenu.sActiveMenu = eFmmc_JUMP_TO_OBJECTIVE_OBJECT_MENU
						iPedToChange = g_FMMC_STRUCT_ENTITIES.iNumberOfObjects
					ENDIF
					IF iPedToChange = (-1)
						iPedToChange = 0
					ENDIF					
				ELSE
					iPedToChange = sFMMCMenu.iSelectedEntity
				ENDIF
				
			ENDIF
			REFRESH_MENU(sFMMCmenu)
		ENDIF
		
		INT iUpOrDown = 0		
		IF MENU_CONTROL_LEFT_CREATOR(menuScrollController)
			iUpOrDown = -1
		ELIF MENU_CONTROL_RIGHT_CREATOR(menuScrollController)
			iUpOrDown = 1
		ENDIF
		
		IF iChangeValueStep != 0
			IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
			AND NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
				iChangeValueStep = 0
			ENDIF
		ENDIF
		
		IF sFMMCmenu.sActiveMenu = eFmmc_PED_SPAWN_OPTIONS
		AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_SPAWN_OPT_RESPAWN_RANGE
			FLOAT fChange
			IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
				fChange = -0.5
			ELIF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
				fChange = 0.5
			ENDIF
			IF fChange != 0
				sFMMCmenu.fPedRespawnRange += fChange
				IF sFMMCmenu.fPedRespawnRange > 500
					sFMMCmenu.fPedRespawnRange = 500
				ELIF sFMMCmenu.fPedRespawnRange < 2
					sFMMCmenu.fPedRespawnRange = 2
				ENDIF
				REFRESH_MENU(sFMMCMenu)		
			ENDIF							
		ENDIF
		
		//Activation Range Override
		IF sFMMCmenu.sActiveMenu = eFmmc_OBJECT_BASE
		AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OBJECT_ACTIVATION_RANGE_OVERRIDE
			IF DOES_ENTITY_EXIST(sObjStruct.viCoronaObj)
				INT iR, iG, iB, iA
				FLOAT fA = 0.5
				
				GET_HUD_COLOUR(HUD_COLOUR_YELLOW, iR, iG, iB, iA)
				
				DRAW_MARKER_SPHERE(GET_ENTITY_COORDS(sObjStruct.viCoronaObj), sFMMCMenu.fActivationRangeOverride, iR, iG, iB, fA)
			ENDIF
		ENDIF
		
		IF sFMMCmenu.sActiveMenu = eFmmc_ZONES_BASE
		AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ZONES_MENU_BLIP_IN_RANGE
			
			INT iR, iG, iB, iA
			FLOAT fA = 0.5
			
			GET_HUD_COLOUR(HUD_COLOUR_YELLOW, iR, iG, iB, iA)
			
			DRAW_MARKER_SPHERE(GET_FMMC_ZONE_CENTRE_FROM_STRUCT(sFMMCMenu.sMenuZone), sFMMCMenu.sMenuZone.fBlipRange, iR, iG, iB, fA)
		ENDIF
		
		IF sFMMCmenu.sActiveMenu = eFMMC_DUMMY_BLIPS
		AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DUMMY_BLIP_GPS_END_RANGE
			
			INT iR, iG, iB, iA
			FLOAT fA = 0.5
			
			GET_HUD_COLOUR(HUD_COLOUR_YELLOW, iR, iG, iB, iA)
			
			DRAW_MARKER_SPHERE(g_FMMC_STRUCT.sCurrentDummyBlip.vPos, TO_FLOAT(g_FMMC_STRUCT.sCurrentDummyBlip.iGPSRange), iR, iG, iB, fA)
		ENDIF 
		
		IF sFMMCmenu.sActiveMenu = eFmmc_MISSION_VARIATION_VEHICLE_MODEL_SWAP_MAIN
		AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) >= OPTION_MISSION_VAR_VEHICLE_MODEL_SWAP_MAIN_1
			IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_X)
				INT iIndexToDelete = GET_CREATOR_MENU_SELECTION(sFMMCmenu) - OPTION_MISSION_VAR_VEHICLE_MODEL_SWAP_MAIN_1
				g_FMMC_STRUCT.sMissionVariation.iVarVehicleModelSwapIndex[iIndexToDelete] = 0
				REFRESH_MENU(sFMMCmenu)
				PRINTLN("Deleting iVarVehicleModelSwapIndex[", iIndexToDelete, "] now")
			ENDIF
		ENDIF
		
		IF sFMMCmenu.sActiveMenu = eFMMC_FALLBACK_SPAWN_POINTS
		AND (GET_CREATOR_MENU_SELECTION(sFMMCmenu) = FALLBACK_SPAWN_POINTS_PLACE OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = FALLBACK_SPAWN_POINTS_CYCLE)
		AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciFALLBACK_SPAWN_POINTS_ENABLE)
			IF NOT sFMMCmenu.bFallbackSpawnDecals
				IF sFMMCmenu.iFallbackSpawnDecal = FMMC_MAX_SPAWNPOINTS - 1
					sFMMCmenu.iFallbackSpawnDecal = 0
					
					sFMMCmenu.bFallbackSpawnDecals = TRUE
				ENDIF
				
				FOR i = sFMMCmenu.iFallbackSpawnDecal TO FMMC_MAX_SPAWNPOINTS - 1
					sFMMCmenu.iFallbackSpawnDecal = i
					
					IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sFallbackSpawnPoint[i].vPosition)
						CREATE_FALLBACK_SPAWN_POINT_DECAL(i)
						
						sFMMCmenu.iFallbackSpawnDecal++
						
						BREAKLOOP
					ENDIF
				ENDFOR
			ELSE
				INT iPlacementBlocked = -1
				
				INT iFallbackSpawnPointCount = 0
				
				FLOAT fShortestDistance = 2.0
				
				REPEAT FMMC_MAX_SPAWNPOINTS i
					IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sFallbackSpawnPoint[i].vPosition)
						IF GET_DISTANCE_BETWEEN_COORDS(sCurrentVarsStruct.vCoronaPos, g_FMMC_STRUCT_ENTITIES.sFallbackSpawnPoint[i].vPosition) < fShortestDistance
							iPlacementBlocked = i
							
							BREAKLOOP
						ENDIF
						
						iFallbackSpawnPointCount++
					ENDIF
				ENDREPEAT
				
				//Rotate Fallback Spawn
				IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RB)
					sCurrentVarsStruct.fCreationHeading = (sCurrentVarsStruct.fCreationHeading - 5) % 360
				ELIF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LB)
					sCurrentVarsStruct.fCreationHeading = (sCurrentVarsStruct.fCreationHeading + 5) % 360
				ENDIF
				
				IF sCurrentVarsStruct.fCreationHeading  < 0
					sCurrentVarsStruct.fCreationHeading = 360 + sCurrentVarsStruct.fCreationHeading
				ENDIF
				
				//Add Fallback Spawn
				IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
					IF iPlacementBlocked = -1
						IF iFallbackSpawnPointCount < FMMC_MAX_SPAWNPOINTS
							g_FMMC_STRUCT_ENTITIES.sFallbackSpawnPoint[iFallbackSpawnPointCount].vPosition = sCurrentVarsStruct.vCoronaPos
							g_FMMC_STRUCT_ENTITIES.sFallbackSpawnPoint[iFallbackSpawnPointCount].fHeading = sCurrentVarsStruct.fCreationHeading
							
							CREATE_FALLBACK_SPAWN_POINT_DECAL(iFallbackSpawnPointCount)
						ENDIF
					ENDIF
					
					REFRESH_MENU(sFMMCmenu)
				ENDIF
				
				//Delete Fallback Spawn
				IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_X)
					IF iPlacementBlocked != -1
						g_FMMC_STRUCT_ENTITIES.sFallbackSpawnPoint[iPlacementBlocked].vPosition = <<0.0, 0.0, 0.0>>
						g_FMMC_STRUCT_ENTITIES.sFallbackSpawnPoint[iPlacementBlocked].fHeading = 0.0
						
						DELETE_FALLBACK_SPAWN_POINT_DECAL(iPlacementBlocked)
						
						IF iPlacementBlocked < FMMC_MAX_SPAWNPOINTS - 1
							FOR i = iPlacementBlocked TO FMMC_MAX_SPAWNPOINTS - 2
								g_FMMC_STRUCT_ENTITIES.sFallbackSpawnPoint[i].vPosition = g_FMMC_STRUCT_ENTITIES.sFallbackSpawnPoint[i + 1].vPosition
								g_FMMC_STRUCT_ENTITIES.sFallbackSpawnPoint[i].fHeading = g_FMMC_STRUCT_ENTITIES.sFallbackSpawnPoint[i + 1].fHeading
								g_FMMC_STRUCT_ENTITIES.sFallbackSpawnPoint[i].diDecal = g_FMMC_STRUCT_ENTITIES.sFallbackSpawnPoint[i + 1].diDecal
							ENDFOR
							
							g_FMMC_STRUCT_ENTITIES.sFallbackSpawnPoint[FMMC_MAX_SPAWNPOINTS - 1].vPosition = <<0.0, 0.0, 0.0>>
							g_FMMC_STRUCT_ENTITIES.sFallbackSpawnPoint[FMMC_MAX_SPAWNPOINTS - 1].fHeading = 0.0
							DELETE_FALLBACK_SPAWN_POINT_DECAL(FMMC_MAX_SPAWNPOINTS - 1)
						ENDIF
					ENDIF
					
					REFRESH_MENU(sFMMCmenu)
				ENDIF
				
				//Cycle Points
				IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = FALLBACK_SPAWN_POINTS_CYCLE
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
						REPEAT FMMC_MAX_SPAWNPOINTS i
							sFMMCmenu.iFallbackSpawnPoint--
							
							IF sFMMCmenu.iFallbackSpawnPoint < 0
								sFMMCmenu.iFallbackSpawnPoint = FMMC_MAX_SPAWNPOINTS - 1
							ENDIF
							
							IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sFallbackSpawnPoint[sFMMCmenu.iFallbackSpawnPoint].vPosition)
								IF DOES_CAM_EXIST(sCamData.cam)
									SET_CAM_COORD(sCamData.cam, g_FMMC_STRUCT_ENTITIES.sFallbackSpawnPoint[sFMMCmenu.iFallbackSpawnPoint].vPosition + <<0.0, 0.0, 15.0>>)
									
									IF GET_DISTANCE_BETWEEN_COORDS(GET_CAM_COORD(sCamData.cam), g_FMMC_STRUCT_ENTITIES.sFallbackSpawnPoint[sFMMCmenu.iFallbackSpawnPoint].vPosition) > 100
										sFMMCmenu.bFallbackSpawnDecals = FALSE
									ENDIF
								ENDIF
								
								BREAKLOOP
							ENDIF
						ENDREPEAT
						
						REFRESH_MENU(sFMMCmenu)
					ENDIF
					
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
						REPEAT FMMC_MAX_SPAWNPOINTS i
							sFMMCmenu.iFallbackSpawnPoint++
							
							IF sFMMCmenu.iFallbackSpawnPoint > FMMC_MAX_SPAWNPOINTS - 1
								sFMMCmenu.iFallbackSpawnPoint = 0
							ENDIF
							
							IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sFallbackSpawnPoint[sFMMCmenu.iFallbackSpawnPoint].vPosition)
								IF DOES_CAM_EXIST(sCamData.cam)
									SET_CAM_COORD(sCamData.cam, g_FMMC_STRUCT_ENTITIES.sFallbackSpawnPoint[sFMMCmenu.iFallbackSpawnPoint].vPosition + <<0.0, 0.0, 15.0>>)
									
									IF GET_DISTANCE_BETWEEN_COORDS(GET_CAM_COORD(sCamData.cam), g_FMMC_STRUCT_ENTITIES.sFallbackSpawnPoint[sFMMCmenu.iFallbackSpawnPoint].vPosition) > 100
										sFMMCmenu.bFallbackSpawnDecals = FALSE
									ENDIF
								ENDIF
								
								BREAKLOOP
							ENDIF
						ENDREPEAT
						
						REFRESH_MENU(sFMMCmenu)
					ENDIF
				ENDIF
				
				//Draw Marker
				INT iR, iG, iB, iA
				
				IF iPlacementBlocked = -1
					iR = 255
					iG = 255
					iB = 255
					iA = 255
				ELSE
					iR = 255
					iG = 0
					iB = 0
					iA = 255
				ENDIF
				
				DRAW_MARKER(MARKER_HALO_POINT, sCurrentVarsStruct.vCoronaPos + <<0.0, 0.0, 0.4>>, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, sCurrentVarsStruct.fCreationHeading>>, <<2.0, 2.0, 2.0>>, iR, iG, iB, iA)
			ENDIF
			
			iUpOrDown = 0
		ELSE
			IF sFMMCmenu.bFallbackSpawnDecals
				REPEAT FMMC_MAX_SPAWNPOINTS i
					IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sFallbackSpawnPoint[i].vPosition)
						DELETE_FALLBACK_SPAWN_POINT_DECAL(i)
					ENDIF
				ENDREPEAT
				
				sFMMCmenu.iFallbackSpawnDecal = 0
				
				sFMMCmenu.bFallbackSpawnDecals = FALSE
			ENDIF
		ENDIF
		
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LB)
		OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RB)
			IF IS_MULTI_TEAMS_MENU(sFMMCmenu)
				IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LB)
					sFMMCmenu.iSelectedTeam--
				ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RB)
					sFMMCmenu.iSelectedTeam++
				ENDIF
				CAP_FMMC_MENU_ITEM(g_FMMC_STRUCT.iMaxNumberOfTeams, sFMMCmenu.iSelectedTeam)
				REFRESH_MENU(sFMMCmenu)
			ELIF sFMMCmenu.sActiveMenu = eFMMC_SPAWN_SUBGROUPS_IN_GROUP
				IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LB)
					sFMMCmenu.iSelectedSpawnSubgroup--
				ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RB)
					sFMMCmenu.iSelectedSpawnSubgroup++
				ENDIF
				CAP_FMMC_MENU_ITEM(g_FMMC_STRUCT.iRandomEntitySpawn_MaxNumber, sFMMCmenu.iSelectedSpawnSubgroup)
				REFRESH_MENU(sFMMCmenu)
			ELIF sFMMCmenu.sActiveMenu = eFmmc_SURVIVAL_Squad_Config
				IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LB)
					sFMMCmenu.iSurvivalDiffSelection--
				ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RB)
					sFMMCmenu.iSurvivalDiffSelection++
				ENDIF
				CAP_FMMC_MENU_ITEM(3, sFMMCmenu.iSurvivalDiffSelection)
				REFRESH_MENU(sFMMCmenu)
			ENDIF
			iUpOrDown = 0
		ENDIF
		
		EDIT_OPTIONS_THAT_REQUIRE_FULL_EVERY_FRAME_INPUTS(iUpOrDown)		
		
		IF iUpOrDown != 0
		OR IS_A_FORCE_EDIT_SET_UP()
		OR SHOULD_EDIT_MENU_OPTIONS_TRIGGER_WITH_SHOULDER_BUTTONS(sFMMCmenu)
		OR SHOULD_EDIT_MENU_OPTIONS_TRIGGER_WITH_DELETE_BUTTONS(sFMMCmenu)
		OR SHOULD_EDIT_MENU_OPTIONS_TRIGGER_WITH_ACCEPT_BUTTONS(sFMMCmenu)
			EDIT_MENU_OPTIONS(iUpOrDown)
			REFRESH_MENU(sFMMCmenu)
		ENDIF
		
		IF sFMMCmenu.sActiveMenu = eFmmc_MISSION_VARIATION_PRIMARY_OBJECTIVE_TEXT_OVERRIDE
			EDIT_MISSION_VARIATION_PRIMARY_OBJECTIVE_TEXT_OVERRIDE_OPTIONS_MENU()
			
		ELIF sFMMCmenu.sActiveMenu = eFmmc_STRING_LIST
			EDIT_CUSTOM_STRING_LIST_MENU()
			
		ELIF sFMMCMenu.bOverrideObjTextKeyboardActive
			sFMMCMenu.bOverrideObjTextKeyboardActive = FALSE
			
		ENDIF
		
		IF sFMMCmenu.sActiveMenu = eFmmc_TURF_WAR_OPTIONS
		AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_TURF_WAR_SET_STATIC_TOPDOWN_CAM
			SET_CAM_FOV(sCamData.Cam, 45)
			PRINTLN("[LM][TURF WAR][CREATOR_CAM] - We are on the Birdseye Option, set the FOV to match BirdsEyeTWFOV (45)")
			SET_BIT(sFMMCdata.iBitSet, bCameraBirdseyeActive)
		ELIF sFMMCmenu.sActiveMenu = eFmmc_SCRIPT_CAM_OPTIONS
		AND (GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_CREATE_CAMERA_LOCATION
			OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_CREATE_CAMERA_LOCATION_DEBUG)
			SET_CAM_FOV(sCamData.Cam, 45)
			PRINTLN("[LM][TURF WAR][CREATOR_CAM] - We are on the Birdseye Option, set the FOV to match BirdsEyeTWFOV (45)")
			SET_BIT(sFMMCdata.iBitSet, bCameraBirdseyeActive)
		ELSE
			// If we've changed it to 65, then override it and set to 45.
			IF IS_BIT_SET(sFMMCdata.iBitSet, bCameraBirdseyeActive)
				SET_CAM_FOV(sCamData.Cam, 45)
				PRINTLN("[LM][TURF WAR][CREATOR_CAM] - We are no longer on the Birdseye Option, default the FOV back to normal creator value (45)")
				CLEAR_BIT(sFMMCdata.iBitSet, bCameraBirdseyeActive)
			ENDIF
		ENDIF
		
		IF sFMMCmenu.sActiveMenu = eFmmc_ENEMY_ANIMATIONS_MENU
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_ANIMS_MENU_IDLE_ANIM_TYPE
				IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_Y)
				AND sFMMCMenu.iPedIdleAnim != ciPED_IDLE_ANIM__CUSTOM_ANIM
					sFMMCMenu.iPedIdleAnim_CurrentCategory = ciPED_ANIM_CATEGORY__CUSTOM
					sFMMCMenu.iPedIdleAnim = ciPED_IDLE_ANIM__CUSTOM_ANIM
					REFRESH_MENU(sFMMCMenu)
				ENDIF
			ENDIF
		ENDIF
		
		IF sFMMCmenu.sActiveMenu = eFmmc_ENEMY_ANIMATIONS_MENU
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_ANIMS_MENU_SPECIAL_ANIM_TYPE
				IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_Y)
				AND sFMMCMenu.iSpecialAnim != ciPED_IDLE_ANIM__CUSTOM_ANIM
					sFMMCMenu.iSpecialAnim_CurrentCategory = ciPED_ANIM_CATEGORY__CUSTOM
					sFMMCMenu.iSpecialAnim = ciPED_IDLE_ANIM__CUSTOM_ANIM
					REFRESH_MENU(sFMMCMenu)
				ENDIF
			ENDIF
		ENDIF
		
		IF JUST_ENTERED_MENU(sFMMCMenu)
			WHILE DEAL_WITH_SKIPPING_EMPTY_OPTIONS_MC(1)
				PRINTLN("Just entered a menu, skipping options using DEAL_WITH_SKIPPING_EMPTY_OPTIONS_MC...")
			ENDWHILE
		ENDIF
	ENDIF
		
	IF NOT IS_BIT_SET(sFMMCdata.iBitSet, biTestMissionActive)
		IF sFMMCMenu.bRuleMenuActive	 = FALSE
			IF GET_GAME_TIMER() > iSelectCoolOffTimer
				IF sFMMCmenu.sActiveMenu = eFmmc_MAIN_MENU_BASE
				OR sFMMCmenu.sActiveMenu = eFmmc_MAIN_OPTIONS_BASE
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RB)
						sCurrentVarsStruct.bResetUpHelp = TRUE
						iSelectCoolOffTimer = GET_GAME_TIMER() + 250
						IF sFMMCmenu.iSelectedTab = ciSELECTED_TAB_CREATION
							IF bshowmenu
								PRINTLN("ciSELECTED_TAB_CREATION")
								sFMMCmenu.iMenuSelectionBeforeSwitch = GET_CREATOR_MENU_SELECTION(sFMMCmenu)
								IF sFMMCmenu.iOptionsSelectionBeforeSwitch != -1
									SET_CREATOR_MENU_SELECTION(sFMMCmenu, sFMMCmenu.iOptionsSelectionBeforeSwitch)
								ELSE
									SET_CREATOR_MENU_SELECTION(sFMMCmenu, 0)
								ENDIF
								sFMMCmenu.iSelectedTab = ciSELECTED_TAB_OPTIONS
							ENDIF
						ELIF(sFMMCmenu.iSelectedTab = ciSELECTED_TAB_OPTIONS)
						AND	sFMMCMenu.bRuleMenuActive	 = FALSE
							IF bshowRightmenu
								sFMMCmenu.iOptionsSelectionBeforeSwitch = GET_CREATOR_MENU_SELECTION(sFMMCmenu)
								SET_CREATOR_MENU_SELECTION(sFMMCmenu, -1)
								SET_CURRENT_MENU_ITEM(GET_CREATOR_MENU_SELECTION(sFMMCmenu))
								sFMMCMenu.bRuleMenuActive	 = TRUE
								g_CreatorsSelDetails.iSelectedTeam = 0
								PRINTLN("sFMMCMenu.bRuleMenuActive	 = TRUE")
							ENDIF
						ELIF sFMMCMenu.bRuleMenuActive	 = TRUE
						AND g_FMMC_STRUCT.iMaxNumberOfTeams > 0
							IF bshowmenu
								g_CreatorsSelDetails.iSelectedTeam++
								IF g_CreatorsSelDetails.iSelectedTeam > (g_FMMC_STRUCT.iMaxNumberOfTeams-1)
									g_CreatorsSelDetails.iSelectedTeam = (g_FMMC_STRUCT.iMaxNumberOfTeams-1)
									sFMMCMenu.bRuleMenuActive	 = FALSE
									sFMMCmenu.iSelectedTab = ciSELECTED_TAB_CREATION
									UPDATE_MENU_FOR_TEST_MISSION_STATE()
									IF sFMMCmenu.iMenuSelectionBeforeSwitch != -1
										SET_CREATOR_MENU_SELECTION(sFMMCmenu, sFMMCmenu.iMenuSelectionBeforeSwitch)
									ELSE
										SET_CREATOR_MENU_SELECTION(sFMMCmenu, 0)
									ENDIF
								ELSE
									IF g_CreatorsSelDetails.iRow >= g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].iNumberOfActiveRows
										g_CreatorsSelDetails.iRow = (g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].iNumberOfActiveRows-1)
										IF g_CreatorsSelDetails.iRow < 0
											g_CreatorsSelDetails.iRow = 0
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ELSE
							IF bshowmenu
								sFMMCMenu.bRuleMenuActive	 = FALSE
								sFMMCmenu.iSelectedTab = ciSELECTED_TAB_CREATION
								UPDATE_MENU_FOR_TEST_MISSION_STATE()
								IF sFMMCmenu.iMenuSelectionBeforeSwitch != -1
									SET_CREATOR_MENU_SELECTION(sFMMCmenu, sFMMCmenu.iMenuSelectionBeforeSwitch	)
								ELSE
									SET_CREATOR_MENU_SELECTION(sFMMCmenu, 0)
								ENDIF
							ENDIF
						ENDIF								
					ENDIF
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LB)	
						sCurrentVarsStruct.bResetUpHelp = TRUE				
						iSelectCoolOffTimer = GET_GAME_TIMER() + 250
						IF sFMMCmenu.iSelectedTab = ciSELECTED_TAB_CREATION
						AND sFMMCMenu.bRuleMenuActive	 = FALSE
							IF bshowRightmenu
								sFMMCmenu.iMenuSelectionBeforeSwitch = GET_CREATOR_MENU_SELECTION(sFMMCmenu)
								SET_CREATOR_MENU_SELECTION(sFMMCmenu, -1)
								SET_CURRENT_MENU_ITEM(GET_CREATOR_MENU_SELECTION(sFMMCmenu))
								sFMMCMenu.bRuleMenuActive	 = TRUE
								g_CreatorsSelDetails.iSelectedTeam = (g_FMMC_STRUCT.iMaxNumberOfTeams-1)
							ENDIF
						ELIF(sFMMCMenu.bRuleMenuActive	 = TRUE)
							IF bshowmenu
								g_CreatorsSelDetails.iSelectedTeam--
								IF g_CreatorsSelDetails.iSelectedTeam < 0
									sFMMCMenu.bRuleMenuActive	 = FALSE
									g_CreatorsSelDetails.iSelectedTeam = 0
									sFMMCmenu.iSelectedTab = ciSELECTED_TAB_OPTIONS
									UPDATE_MENU_FOR_TEST_MISSION_STATE()
									IF sFMMCmenu.iOptionsSelectionBeforeSwitch != 0
										SET_CREATOR_MENU_SELECTION(sFMMCmenu, sFMMCmenu.iOptionsSelectionBeforeSwitch)
									ELSE
										SET_CREATOR_MENU_SELECTION(sFMMCmenu, 0)
									ENDIF
								ELSE
									IF g_CreatorsSelDetails.iRow >= g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].iNumberOfActiveRows
										g_CreatorsSelDetails.iRow = (g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].iNumberOfActiveRows-1)
									ENDIF
								ENDIF
							ENDIF	
						ELSE
							sFMMCmenu.iSelectedTab = ciSELECTED_TAB_CREATION
							sFMMCmenu.iSelectedTab = ciSELECTED_TAB_CREATION		
							UPDATE_MENU_FOR_TEST_MISSION_STATE()
							sFMMCmenu.iOptionsSelectionBeforeSwitch = GET_CREATOR_MENU_SELECTION(sFMMCmenu)
							IF sFMMCmenu.iMenuSelectionBeforeSwitch != -1
								SET_CREATOR_MENU_SELECTION(sFMMCmenu, sFMMCmenu.iMenuSelectionBeforeSwitch)
							ELSE
								SET_CREATOR_MENU_SELECTION(sFMMCmenu, 0)
							ENDIF
						ENDIF								
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
		
	if sFMMCMenu.iSelectedEntity = -1		
		IF IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
			SET_BIT(iHelpBitSet, biWarpToCameraButton) 
		ENDIF
	ENDIF

	IF NOT IS_BIT_SET(sFMMCdata.iBitSet, biTestMissionActive)
		IF sFMMCmenu.sActiveMenu != eFmmc_PAN_CAM_BASE
		AND sFMMCmenu.sActiveMenu != eFmmc_PHOTO_CAM_BASE
		AND sFMMCmenu.sActiveMenu != eFmmc_OUT_CAM_BASE
		AND sFMMCmenu.sActiveMenu != eFmmc_DM_TEAM_CAMERAS
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_SELECT)		
				IF NOT sFMMCMenu.bRuleMenuActive				
					sCurrentVarsStruct.iMenuState = MENU_STATE_SWITCH_CAM
				ENDIF
			ENDIF	
		ENDIF
	ENDIF
	
	RETURN eFmmc_Action_Null
ENDFUNC

FUNC BOOL DO_END_MENU()	
	
	PRINTSTRING("DO_END_MENU - being called")PRINTNL()
	
	IF IS_HELP_MESSAGE_ON_SCREEN()
		CLEAR_HELP()
	ENDIF
	
	IF NOT IS_SCREEN_FADING_OUT()
		IF NOT IS_SCREEN_FADED_OUT()
			IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
			AND NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)		
				DO_SCREEN_FADE_OUT(500)
			ENDIF
		ELSE
			DRAW_END_MENU()
			
			IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
				PRINTSTRING("CONFIRM SOUND EFFECT!!!")PRINTNL()
				SET_ENTITY_CREATION_STATUS(STAGE_ENTITY_PLACEMENT_SETUP)
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				DO_SCREEN_FADE_IN(500)
				sCurrentVarsStruct.bSwitching = FALSE
				sCurrentVarsStruct.stiScreenCentreLOS = NULL
			ENDIF			
		
			IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
				PRINTSTRING("CONFIRM SOUND EFFECT!!!")PRINTNL()
				SET_ENTITY_CREATION_STATUS(STAGE_SET_UP_RULES_PAGE)
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
	
ENDFUNC

PROC RESET_TESTING_MC_MISSION_MENU_DATA(STRUCT_TEST_MENU_DATA &structMenuData)
	STRUCT_TEST_MENU_DATA sTemp
	structMenuData = sTemp
	NET_PRINT("[WJK] - Mission Creator - RESET_TESTING_MC_MISSION_MENU_DATA() has been called.")NET_NL()
ENDPROC

PROC DEAL_WITH_TEST_BEING_ACTIVE(STRUCT_TEST_MENU_DATA &structMenuData)
	
	// Load menu.
	IF LOAD_MENU_ASSETS()
		
		// Process menu state.
		SWITCH structMenuData.eTestMcMissionMenuState
			
			// Wait for player to press up to activate menu. While waiting display help informing player they can do this.
			CASE eTESTINGMCMISSIONMENUSTATE_DISPLAY_HELP				
				
				IF NOT IS_PHONE_ONSCREEN()
					IF NOT structMenuData.bDisplayedHelp
						IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
							NET_PRINT("[WJK] - Mission Creator -called PRINT_HELP_FOREVER(MC_TEST3)")NET_NL()
							PRINT_HELP_FOREVER("MC_TEST")
							structMenuData.bDisplayedHelp = TRUE
						ENDIF
					ENDIF
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
						DISABLE_CELLPHONE(TRUE)
						structMenuData.eTestMcMissionMenuState = eTESTINGMCMISSIONMENUSTATE_SETUP_MENU
						NET_PRINT("[WJK] - Mission Creator - pressed INPUT_FRONTEND_UP, going to eTESTINGMCMISSIONMENUSTATE_SETUP_MENU")NET_NL()
					ENDIF
				ELSE
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MC_TEST")
						CLEAR_HELP()
						structMenuData.bDisplayedHelp = FALSE
					ENDIF
				ENDIF
				
			BREAK
			
			// Setup the menu.
			CASE eTESTINGMCMISSIONMENUSTATE_SETUP_MENU
				
				CLEAR_MENU_DATA()
				SET_MENU_TITLE("FMMCC_TTITLE")
				SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT, MENU_ITEM_TEXT)
				SET_MENU_ITEM_JUSTIFICATION(FONT_LEFT, FONT_RIGHT)
				ADD_MENU_ITEM_TEXT(0, "MC_TEST2")
				
				structMenuData.eTestMcMissionMenuState = eTESTINGMCMISSIONMENUSTATE_SHOW_MENU
				
				NET_PRINT("[WJK] - Mission Creator - menu setup, going to eTESTINGMCMISSIONMENUSTATE_SHOW_MENU")NET_NL()
				
			BREAK
			
			// While menu is showing, wait for inputs from player to either exit the menu or exit testing the mission.
			CASE eTESTINGMCMISSIONMENUSTATE_SHOW_MENU
				
				SET_CURRENT_MENU_ITEM_DESCRIPTION("FMMCC_EXTEST")
				// Draw the menu.
				DRAW_MENU()
				
				IF NOT IS_PAUSE_MENU_ACTIVE() //BC: 1789172 where the exit test menu buttons are listening with the pause menu active. 
					// Check for input.
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
						DISABLE_CELLPHONE(FALSE)
						RESET_TESTING_MC_MISSION_MENU_DATA(structMenuData)
						NET_PRINT("[WJK] - Mission Creator - pressed INPUT_FRONTEND_UP, going to eTESTINGMCMISSIONMENUSTATE_DISPLAY_HELP")NET_NL()
					ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
						RESET_TESTING_MC_MISSION_MENU_DATA(structMenuData)
						REFRESH_MENU(sFMMCmenu)
						g_FMMC_STRUCT.g_b_QuitTest = TRUE
						NET_PRINT("[WJK] - Mission Creator - pressed INPUT_FRONTEND_ACCEPT, setting g_FMMC_STRUCT.g_b_QuitTest = TRUE.")NET_NL()
					ENDIF
				ENDIF
				
			BREAK
			
		ENDSWITCH
	ELSE
		NET_PRINT("[WJK] - Mission Creator - waiting for LOAD_MENU_ASSETS() to return TRUE.")NET_NL()
	ENDIF
ENDPROC

PROC PROCESS_PRE_GAME()

	SET_CREATOR_AUDIO(TRUE, FALSE)
	
	SET_BIT(sFMMCdata.iBitSet, bCameraActive)
	SETUP_FMMC_SCRIPT(sFMMCMenu, sHCS, sCurrentVarsStruct, FALSE, TRUE)
	INITIALISE_MODEL_ARRAYS(sPedStruct,	sWepStruct, sObjStruct, sFMMCmenu, TRUE)
	CLEANUP_MENU_ASSETS()
	
	sFMMCmenu.vCameraOffset			= <<0,0,0>>
	sFMMCmenu.iCurrentGoToLocation 	= -1
	sFMMCmenu.bExitAtCurrentGoTo	= FALSE
	sFMMCmenu.vCurrentGoToPos		= <<0,0,0>>
	sFMMCmenu.fCurrentGoToHeight	= 0.0
	sFMMCmenu.iUnlockAfterRule 		= -1
	sFMMCmenu.iProximityCheck		= 0
	sFMMCmenu.iEngineHealth			= 1001
	sFMMCmenu.iPetrolTankHealth		= 1001
	sFMMCmenu.iBodyHealth			= 1001
	CLEAR_BIT(sFMMCmenu.iActorMenuBitsetTwo, ciPED_BSTwo_BlipOnFollow)
	sFMMCmenu.iBlipSpriteOverride = 0
	sFMMCmenu.iBlipSpriteOverrideCategory = -1
	
	sFMMCmenu.iSelectedObjectLibrary = OBJECT_LIBRARY_CAPTURE+1
				
	sFMMCmenu.iCutSceneRespawnScene = -1
	sFMMCmenu.iCutSceneRespawnShot = 0
	sFMMCmenu.iCutSceneTaskActivationScene = -1
	sFMMCmenu.iCutSceneTaskActivationShot = -1
	
	CLEAR_DOOR_STRUCT(sFMMCMenu.sCurrentDoor)
	
	sFMMCmenu.wtCurrentWeapon = WEAPONTYPE_PISTOL
	CLEAR_CURRENT_FMMC_WEAPON_COMPONENTS(sFMMCmenu)
	
	iMaxSpawnsPerTeam[0] = FMMC_MAX_TEAMSPAWNPOINTS
	iMaxSpawnsPerTeam[1] = FMMC_MAX_TEAMSPAWNPOINTS
	iMaxSpawnsPerTeam[2] = FMMC_MAX_TEAMSPAWNPOINTS
	iMaxSpawnsPerTeam[3] = FMMC_MAX_TEAMSPAWNPOINTS
	
	g_CreatorsSelDetails.iMaxNumberOfTeams = g_FMMC_STRUCT.iMaxNumberOfTeams 
	//Always display the patrol ranges.
	SET_BIT(sFMMCdata.iBitSet, bDM_Display_Ped_Patrol_range)
	
	//Set up the relationship groups
	SET_UP_RELATIONSHIP_GROUPS(sRGH)
	
	DISABLE_VEHICLE_DISTANTLIGHTS(TRUE)
	
	//Deal with the mapf
	sFMMCmenu.iScriptCreationType 	= SCRIPT_CREATION_TYPE_MISSION
	
	g_FMMC_STRUCT.sFMMCEndConditions[0].iPointsPerKill = 0
	g_FMMC_STRUCT.sFMMCEndConditions[1].iPointsPerKill = 0
	g_FMMC_STRUCT.sFMMCEndConditions[2].iPointsPerKill = 0
	g_FMMC_STRUCT.sFMMCEndConditions[3].iPointsPerKill = 0
	
	g_FMMC_STRUCT.iNumPlayersPerTeam[0] = 1
	
	g_FMMC_STRUCT.tl63MissionName = ""
	g_FMMC_STRUCT.tl63MissionDecription[0] = ""
	
	sFMMCmenu.iCurrentEntitySelection[CREATION_TYPE_ENEMY_RULE]	 	= ciDEFAULT_PED_RULE
	sFMMCmenu.iCurrentEntitySelection[CREATION_TYPE_VEHICLE_RULE]	= ciDEFAULT_VEHICLE_RULE
	sFMMCmenu.iCurrentEntitySelection[CREATION_TYPE_OBJECT_RULE] 	= ciDEFAULT_OBJECT_RULE
	
	FILL_BLIP_SPRITE_CREATOR_ARRAY(sFMMCmenu)
	
	INT iTemp, iTemp2
	FOR iTemp = 0 TO FMMC_MAX_TEAMS - 1
	
		sFMMCmenu.sAssociatedRuleSettings[iTemp].iRule = -1
		sFMMCmenu.sAssociatedRuleSettings[iTemp].iSpawn = 0
		sFMMCmenu.sAssociatedRuleSettings[iTemp].iActionStart = 0
		sFMMCmenu.sAssociatedRuleSettings[iTemp].iScoreRequired = 0		
		sFMMCmenu.sAssociatedRuleSettings[iTemp].iAlwaysForceSpawnOnRule = -1
		sFMMCmenu.sAssociatedRuleSettings[iTemp].iRuleEnd = -1
		sFMMCmenu.iAggroRule[iTemp] = -1
		sFMMCmenu.iPedSkipToRuleWhenAggro[iTemp] = -1
		
		g_FMMC_STRUCT.biOutFitsAvailableBitset[iTemp][0] = 0
		g_FMMC_STRUCT.biOutFitsAvailableBitset[iTemp][1] = 0
		g_FMMC_STRUCT.biOutFitsAvailableBitset[iTemp][2] = 0
		FOR iTemp2 = 0 TO MAX_HEIST_GEAR_BITSETS-1
			g_FMMC_STRUCT.biGearAvailableBitset[iTemp][iTemp2] = 0
		ENDFOR
		
		g_FMMC_STRUCT.iCriticalMinimumForTeam[iTemp] = 0
		
		g_FMMC_STRUCT.mnVehicleModel[iTemp] = DUMMY_MODEL_FOR_SCRIPT	//Gauntlet
		sFMMCmenu.sMenuZone.iZoneStartPriority[iTemp] = -1
		sFMMCmenu.sMenuZone.iZoneEndPriority[iTemp] = FMMC_PRIORITY_IGNORE
		
		sFMMCmenu.iGotoProgressesRule[iTemp] = ciFMMC_GOTO_CONSEQUENCE_PASS_RULE_OFF
		
	ENDFOR
	FOR iTemp = 0 TO PROP_LIBRARY_MAX_ITEMS-1
		sFMMCmenu.iNumberOfPropsInLibrary[iTemp] = -1
	ENDFOR
	CLEAR_CACHED_AVAILABLE_VEHICLE_COUNTS()
	
	sFMMCmenu.iHackFailsForAggro = -1
	
	sFMMCmenu.sMenuZone.fRadius = 1.0
			
	sFMMCmenu.iOnScreenPopupWindowBS_Result = -1
	INITIALISE_INT_ARRAY(sFMMCmenu.iOnScreenPopupWindowBSLong_Result, -1)
	
	g_FMMC_STRUCT.iTraffic = 2
	g_FMMC_STRUCT.iPedDensity = 2
	
	g_FMMC_STRUCT.iBS_MissionTeamRelations = 0
	
	SET_BIT(g_FMMC_STRUCT.iBS_MissionTeamRelations, FMMC_TEAM_REL_1_1_LIKE)
	SET_BIT(g_FMMC_STRUCT.iBS_MissionTeamRelations, FMMC_TEAM_REL_2_2_LIKE)
	SET_BIT(g_FMMC_STRUCT.iBS_MissionTeamRelations, FMMC_TEAM_REL_3_3_LIKE)
	SET_BIT(g_FMMC_STRUCT.iBS_MissionTeamRelations, FMMC_TEAM_REL_4_4_LIKE)
	
	SET_LONG_BIT(sFMMCMenu.iBitActive,PAN_CAM_PLACE)
	
	IF g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_HEIST
	OR g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_CONTACT
	OR g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_PLANNING
		CLEAR_BIT(sFMMCmenu.iOptionsMenuBitSet, OPTION_MISSION_POINTS_HUD)
	ELSE
		SET_BIT(sFMMCmenu.iOptionsMenuBitSet, OPTION_MISSION_POINTS_HUD)
	ENDIF
	
	SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSet, ciVOTING_ENABLED)
	SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSet, ciGANGS_DISABLED) 
	
	g_FMMC_STRUCT.iMusic = GET_RANDOM_INT_IN_RANGE(0, ciRC_MAX_TYPE_RACE_RADIO)
	STRING sTempRadio
	sTempRadio = GET_RADIO_STATION_NAME(g_FMMC_STRUCT.iMusic)
	g_FMMC_STRUCT.iMusic = GET_HASH_KEY(sTempRadio)
	
	SET_MOBILE_RADIO_ENABLED_DURING_GAMEPLAY(TRUE)
	
	#if IS_DEBUG_BUILD 
	IF IS_ROCKSTAR_DEV() 
		SET_ALLOW_TILDE_CHARACTER_FROM_ONSCREEN_KEYBOARD(TRUE)
	ENDIF
	#ENDIF
	
	sFMMCmenu.sDialogueTrigger.iRule = -1
	sFMMCmenu.sDialogueTrigger.iTeam = ciDIALOGUE_TRIGGER_TEAM_OFF
	sFMMCmenu.sDialogueTrigger.fRadius = 3.0
	sFMMCmenu.sDialogueTrigger.iPedVoice = -1
	sFMMCmenu.sDialogueTrigger.iSecondPedVoice = -1
	sFMMCmenu.sDialogueTrigger.iPedAnimation = 0
	sFMMCmenu.sDialogueTrigger.iDialogueBitset = 0
	sFMMCmenu.sDialogueTrigger.iPlayForRoleBS = 0
	sFMMCmenu.sDialogueTrigger.iPoint = -1
	sFMMCmenu.sDialogueTrigger.iVehMinigameIndex = -1
	sFMMCmenu.sDialogueTrigger.iVehicleTrigger = -1
	sFMMCmenu.sDialogueTrigger.iPrerequisite = -1
	sFMMCmenu.sDialogueTrigger.iVehicleDamage = 0
	sFMMCmenu.sDialogueTrigger.iCrowdControlKills = 0
	sFMMCmenu.sDialogueTrigger.iPlayerCountRequirement = 0
	sFMMCmenu.sDialogueTrigger.iPlayerCountComparison = ciDT_PLAYER_COUNT_COMPARISON_EQUALS
	sFMMCmenu.sDialogueTrigger.iPlayerCountType = ciDT_PLAYER_COUNT_TYPE_CURRENT
	sFMMCmenu.sDialogueTrigger.iGangChasePedsToKill = -1
	sFMMCmenu.sDialogueTrigger.iVehicleHacked = 0
	sFMMCmenu.sDialogueTrigger.iPedKilled = -1
	sFMMCmenu.sDialogueTrigger.iWaterDistance = 0
	sFMMCmenu.sDialogueTrigger.iFiresExtinguished = -1
	sFMMCmenu.sDialogueTrigger.iMultiruleTimeToReach = -1
	sFMMCmenu.sDialogueTrigger.iPedTrigger = -1
	sFMMCmenu.sDialogueTrigger.iPedDamage = 0
	sFMMCmenu.sDialogueTrigger.iPedFleeType = -1
	sFMMCmenu.sDialogueTrigger.iEntityExistsType = 0
	sFMMCmenu.sDialogueTrigger.iEntityExistsIndex = -1
	sFMMCmenu.sDialogueTrigger.iContinuityId = -1
	sFMMCmenu.sDialogueTrigger.iLinkedObjectContinuityId = -1
	sFMMCmenu.sDialogueTrigger.iLinkedLocationContinuityId = -1
	CLEAR_DIALOGUE_TRIGGER_DATA(sFMMCmenu.sDialogueTrigger)
	CLEAR_CUTSCENE_DATA(g_fmmc_struct.sCurrentSceneData)
	CLEAR_MOCAP_CUTSCENE_DATA(g_fmmc_struct.sCurrentMocapSceneData)
	
	CLEAR_ALL_EXTRA_OBJECTIVE_ENTITIES()
	
	FORCE_CORONER_INTERIOR_IPL_ON()
	
	IF g_FMMC_STRUCT.iDLCRelease = -1
	AND IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.tl63MissionName)
		IF g_bFMMC_EnteredLegacyMissionCreator
			g_FMMC_STRUCT.iDLCRelease = ciDLC_CASINO_HEIST
		ELSE	
			g_FMMC_STRUCT.iDLCRelease = ciDLC_MAX-1
		ENDIF
	ENDIF
	IF NOT IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_DLC_1_2020)
		g_bFMMC_EnteredLegacyMissionCreator = TRUE
	ENDIF
	
	CLEAR_ENTITY_BLIP_DATA_STRUCT(sFMMCmenu.sEntityBlipStruct)
	
	CLEAR_VEHICLE_CUSTOM_HEALTH_SCALE(sFMMCmenu.fVehCustomHealthScale)
	
	RESET_PLACED_PROP_ARRAY()
	
	INT i
	FOR i = 0 TO MAX_MENU_LAYERS-1
		sFMMCMenu.sMenuBackConditional[i] = eFmmc_Null_item
	ENDFOR
	
	INIT_CREATOR_BUDGET()
	NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()
	
	// Setup widgets. Make sure this is done after PROCESS_PRE_GAME()
	#IF IS_DEBUG_BUILD
		CREATE_WIDGETS()
	#ENDIF
ENDPROC

PROC ADD_NEW_ENTITY_RULE(INT iType)
	//If there are no rules set up then get out of here
	IF sCurrentVarsStruct.iUpdateRule = 0
		EXIT
	ENDIF 
	
	IF IS_BIT_SET(sCurrentVarsStruct.iUpdateRule, ciRESET_ALL_RULES)
		SET_MISSION_STARTING_MENU_ITEMS_ACTIVE(sFMMCmenu.iBitActive)
		RESET_ALL_RULES()
		sCurrentVarsStruct.iUpdateRule = 0		
		
		MENU_INITILISATION()
		EXIT
	ENDIF
	
	IF IS_BIT_SET(sCurrentVarsStruct.iUpdateRule, ciENTITY_REMOVED_CHECK_RULES)		
		CHECK_ENTITY_AND_LOCATION_RULES()
		CLEAR_BIT(sCurrentVarsStruct.iUpdateRule, ciENTITY_REMOVED_CHECK_RULES)
		IF NOT IS_BIT_SET(sCurrentVarsStruct.iUpdateRule, ciADD_NEW_RULE)
			EXIT
		ENDIF
	ENDIF
	
	sFMMCendStage.bMajorEditOnLoadedMission = TRUE
	
	INT iPreviousRow
	INT iTeam 			
	INT iRule
	BOOL bCanAddRuleOfThisType
	BOOL bStopProcessingLoop
	
	FOR iTeam = 0 TO (FMMC_MAX_TEAMS - 1)
		IF g_CreatorsSelDetails.sTeamOptions[iTeam].iNumberOfActiveRows < 0 //url:bugstar:4884749
			PRINTLN("ADD_NEW_ENTITY_RULE g_CreatorsSelDetails.sTeamOptions[",iTeam,"].iNumberOfActiveRows ",g_CreatorsSelDetails.sTeamOptions[iTeam].iNumberOfActiveRows," need to be reset.")
			ASSERTLN("ADD_NEW_ENTITY_RULE g_CreatorsSelDetails.sTeamOptions[",iTeam,"].iNumberOfActiveRows ",g_CreatorsSelDetails.sTeamOptions[iTeam].iNumberOfActiveRows," need to be reset.")
			g_CreatorsSelDetails.sTeamOptions[iTeam].iNumberOfActiveRows = 0
		ENDIF
		bStopProcessingLoop = FALSE
		iPreviousRow 	= (g_CreatorsSelDetails.sTeamOptions[iTeam].iNumberOfActiveRows - 1)
		
		SWITCH iType
		
			CASE CREATION_TYPE_PEDS
				PRINTLN("CREATION_TYPE_PEDS")
				IF g_FMMC_STRUCT_ENTITIES.iNumberOfPeds > 0
					bCanAddRuleOfThisType = TRUE
					iRule = GET_RULE_SELECTION_FROM_RULE(g_FMMC_STRUCT_ENTITIES.sPlacedPed[g_FMMC_STRUCT_ENTITIES.iNumberOfPeds -1].iRule[iTeam], CREATION_TYPE_PEDS) 	
					IF iRule = ciSELECTION_NONE
						PRINTLN("iRule = ciSELECTION_NONE")
						bStopProcessingLoop = TRUE
					ENDIF
				ELSE
					bStopProcessingLoop = TRUE
				ENDIF
			BREAK
			
			CASE CREATION_TYPE_VEHICLES
				PRINTLN("CREATION_TYPE_VEHICLES")
				IF g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles > 0
					bCanAddRuleOfThisType = TRUE
					iRule = GET_RULE_SELECTION_FROM_RULE(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles -1].iRule[iTeam], CREATION_TYPE_VEHICLES) 	
					IF iRule = ciSELECTION_NONE
						PRINTLN("iRule = ciSELECTION_NONE")
						bStopProcessingLoop = TRUE
					ENDIF
				ELSE
					bStopProcessingLoop = TRUE
				ENDIF
			BREAK
			
			CASE CREATION_TYPE_GOTO_LOC
				PRINTLN("CREATION_TYPE_GOTO_LOC")
				IF g_FMMC_STRUCT_ENTITIES.iNumberOfGoToLocations[0] > 0
					bCanAddRuleOfThisType = TRUE
					iRule = GET_RULE_SELECTION_FROM_RULE(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[g_FMMC_STRUCT_ENTITIES.iNumberOfGoToLocations[0] -1].iRule[iTeam], CREATION_TYPE_GOTO_LOC) 	
					IF iRule = ciSELECTION_NONE
						PRINTLN("iRule = ciSELECTION_NONE")
						bStopProcessingLoop = TRUE
					ENDIF
				ELSE
					bStopProcessingLoop = TRUE
				ENDIF
			BREAK
			
			CASE CREATION_TYPE_OBJECTS
				PRINTLN("CREATION_TYPE_OBJECTS")
				IF g_FMMC_STRUCT_ENTITIES.iNumberOfObjects > 0
					bCanAddRuleOfThisType = TRUE
					iRule = GET_RULE_SELECTION_FROM_RULE(g_FMMC_STRUCT_ENTITIES.sPlacedObject[g_FMMC_STRUCT_ENTITIES.iNumberOfObjects -1].iRule[iTeam], CREATION_TYPE_OBJECTS) 
					IF iRule = ciSELECTION_NONE
						PRINTLN("iRule = ciSELECTION_NONE")
						bStopProcessingLoop = TRUE
					ENDIF
				ELSE
					bStopProcessingLoop = TRUE
				ENDIF
			BREAK
		ENDSWITCH
		
		IF bStopProcessingLoop = FALSE
			#IF IS_DEBUG_BUILD
			PRINTLN("iRule = ", GET_RULE_DEBUG_STRING_FROM_SELECTION(iRule), " = ", iRule)
			#ENDIF
			//If a new rule is to be added
			IF IS_BIT_SET(sCurrentVarsStruct.iUpdateRule, ciADD_NEW_RULE)
				IF g_CreatorsSelDetails.sTeamOptions[iTeam].iNumberOfActiveRows <= ciMAX_RULES
					IF bCanAddRuleOfThisType
						//The last rule is the same
						IF iPreviousRow >= 0
						AND g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[iPreviousRow].iRule = iRule
						AND sFMMCmenu.iNewRule = 0
						
							PRINTLN("ADD NEW RULE")
							SWITCH iType
							CASE CREATION_TYPE_PEDS 
								g_FMMC_STRUCT_ENTITIES.sPlacedPed[g_FMMC_STRUCT_ENTITIES.iNumberOfPeds -1].iPriority[iTeam] = (g_CreatorsSelDetails.sTeamOptions[iTeam].iNumberOfActiveRows - 1)
								FMMC_SET_LONG_BIT(g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[iPreviousRow].iEntityBitSet, g_FMMC_STRUCT_ENTITIES.iNumberOfPeds -1)
							BREAK
							CASE CREATION_TYPE_VEHICLES
								g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles -1].iPriority[iTeam]  = (g_CreatorsSelDetails.sTeamOptions[iTeam].iNumberOfActiveRows - 1)
								FMMC_SET_LONG_BIT(g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[iPreviousRow].iEntityBitSet, g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles -1)
							BREAK
							CASE CREATION_TYPE_OBJECTS
								g_FMMC_STRUCT_ENTITIES.sPlacedObject[g_FMMC_STRUCT_ENTITIES.iNumberOfObjects -1].iPriority[iTeam]  = (g_CreatorsSelDetails.sTeamOptions[iTeam].iNumberOfActiveRows - 1)
								FMMC_SET_LONG_BIT(g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[iPreviousRow].iEntityBitSet, g_FMMC_STRUCT_ENTITIES.iNumberOfObjects -1)
							BREAK
							CASE CREATION_TYPE_GOTO_LOC 
								//Don't make a new rule add to existing
								PRINTLN("CREATION_TYPE_GOTO_LOC")
								g_FMMC_STRUCT_ENTITIES.sGotoLocationData[g_FMMC_STRUCT_ENTITIES.iNumberOfGoToLocations[0] - 1].iPriority[iTeam]	= (g_CreatorsSelDetails.sTeamOptions[iTeam].iNumberOfActiveRows -1)
								FMMC_SET_LONG_BIT(g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[iPreviousRow].iEntityBitSet, g_FMMC_STRUCT_ENTITIES.iNumberOfGoToLocations[0] - 1)
							BREAK	
							ENDSWITCH
						ELIF g_CreatorsSelDetails.sTeamOptions[iTeam].iNumberOfActiveRows < ciMAX_RULES
							IF iType = CREATION_TYPE_GOTO_LOC
								FMMC_SET_LONG_BIT(g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[g_CreatorsSelDetails.sTeamOptions[iTeam].iNumberOfActiveRows].iEntityBitSet, g_FMMC_STRUCT_ENTITIES.iNumberOfGoToLocations[0] - 1)
								
								g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjectiveScore[g_CreatorsSelDetails.sTeamOptions[iTeam].iNumberOfActiveRows] 				= GET_SCORE_FROM_RULE(g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[g_CreatorsSelDetails.sTeamOptions[iTeam].iNumberOfActiveRows].iRule)
								g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTakeoverTime[g_CreatorsSelDetails.sTeamOptions[iTeam].iNumberOfActiveRows]					= GET_CAPTURE_TIME_FROM_RULE(g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[g_CreatorsSelDetails.sTeamOptions[iTeam].iNumberOfActiveRows].iRule)
								g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iReCapture[g_CreatorsSelDetails.sTeamOptions[iTeam].iNumberOfActiveRows]					= GET_MAX_CAPTURES_FROM_RULE(g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[g_CreatorsSelDetails.sTeamOptions[iTeam].iNumberOfActiveRows].iRule)
								g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iOutfit[g_CreatorsSelDetails.sTeamOptions[iTeam].iNumberOfActiveRows]						= -1
								g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjectiveTimeLimitRule[g_CreatorsSelDetails.sTeamOptions[iTeam].iNumberOfActiveRows]		= GET_TIME_FROM_RULE(g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[g_CreatorsSelDetails.sTeamOptions[iTeam].iNumberOfActiveRows].iRule)
								g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjectiveScore[g_CreatorsSelDetails.sTeamOptions[iTeam].iNumberOfActiveRows] 				= GET_SCORE_FROM_RULE(g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[g_CreatorsSelDetails.sTeamOptions[iTeam].iNumberOfActiveRows].iRule)
								g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTakeoverTime[g_CreatorsSelDetails.sTeamOptions[iTeam].iNumberOfActiveRows]					= GET_CAPTURE_TIME_FROM_RULE(g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[g_CreatorsSelDetails.sTeamOptions[iTeam].iNumberOfActiveRows].iRule)
								g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iReCapture[g_CreatorsSelDetails.sTeamOptions[iTeam].iNumberOfActiveRows]					= GET_MAX_CAPTURES_FROM_RULE(g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[g_CreatorsSelDetails.sTeamOptions[iTeam].iNumberOfActiveRows].iRule)
								g_FMMC_STRUCT_ENTITIES.sGotoLocationData[g_CreatorsSelDetails.sTeamOptions[iTeam].iNumberOfActiveRows].mnVehicleNeeded[iTeam]				= DUMMY_MODEL_FOR_SCRIPT
								
								CLEAR_BIT(g_FMMC_STRUCT.iTimerStart[iTeam], g_CreatorsSelDetails.sTeamOptions[iTeam].iNumberOfActiveRows)
								CLEAR_BIT(g_FMMC_STRUCT.iTimerStop[iTeam], g_CreatorsSelDetails.sTeamOptions[iTeam].iNumberOfActiveRows)
								
								PRINTLN("iObjectiveTimeLimitRule[", g_CreatorsSelDetails.sTeamOptions[iTeam].iNumberOfActiveRows,"] = ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjectiveTimeLimitRule[g_CreatorsSelDetails.sTeamOptions[iTeam].iNumberOfActiveRows]	)
								PRINTLN("iObjectiveScore[", g_CreatorsSelDetails.sTeamOptions[iTeam].iNumberOfActiveRows,"]         = ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjectiveScore[g_CreatorsSelDetails.sTeamOptions[iTeam].iNumberOfActiveRows] 			)
								PRINTLN("iTakeoverTime[", g_CreatorsSelDetails.sTeamOptions[iTeam].iNumberOfActiveRows,"]           = ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTakeoverTime[g_CreatorsSelDetails.sTeamOptions[iTeam].iNumberOfActiveRows]			)
								PRINTLN("iReCapture[", g_CreatorsSelDetails.sTeamOptions[iTeam].iNumberOfActiveRows,"]              = ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iReCapture[g_CreatorsSelDetails.sTeamOptions[iTeam].iNumberOfActiveRows]				)
								
								SET_DEFAULT_VALUES_FROM_CLOUD(iTeam, g_CreatorsSelDetails.sTeamOptions[iTeam].iNumberOfActiveRows)
								
								g_CreatorsSelDetails.sTeamOptions[iTeam].iNumberOfActiveRows++
								IF g_CreatorsSelDetails.sTeamOptions[iTeam].iNumberOfActiveRows > ciMAX_RULES
									g_CreatorsSelDetails.sTeamOptions[iTeam].iNumberOfActiveRows = ciMAX_RULES 
								ENDIF
							ELSE
								PRINTLN("sFMMCmenu.iNewRule = ", sFMMCmenu.iNewRule)
								//It's a new rule 
								INT iRow = g_CreatorsSelDetails.sTeamOptions[iTeam].iNumberOfActiveRows
								g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[iRow].iRule = iRule
								g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjectiveTimeLimitRule[iRow]			= GET_TIME_FROM_RULE(iRule)
								g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjectiveScore[iRow] 					= GET_SCORE_FROM_RULE(iRule)
								g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTargetScore[iRow] 					= 0
								g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMaxDeliveryEntitiesCanCarry[iRow] 		= 1
								g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iOutfit[iRow] 							= -1
								g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTakeoverTime[iRow]						= GET_CAPTURE_TIME_FROM_RULE(iRule)
								g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iReCapture[iRow]						= GET_MAX_CAPTURES_FROM_RULE(iRule)
								
								CLEAR_BIT(g_FMMC_STRUCT.iTimerStart[iTeam], g_CreatorsSelDetails.sTeamOptions[iTeam].iNumberOfActiveRows)
								CLEAR_BIT(g_FMMC_STRUCT.iTimerStop[iTeam], g_CreatorsSelDetails.sTeamOptions[iTeam].iNumberOfActiveRows)
								
								PRINTLN("iObjectiveTimeLimitRule[", iRow,"] = ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjectiveTimeLimitRule[iRow])
								PRINTLN("iObjectiveScore[", iRow,"]         = ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjectiveScore[iRow])
								PRINTLN("iTakeoverTime[", iRow,"]           = ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTakeoverTime[iRow])
								PRINTLN("iReCapture[", iRow,"]              = ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iReCapture[iRow])
								PRINTLN("iOutfit[", iRow,"]             	= ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iOutfit[iRow])
								
								SET_DEFAULT_VALUES_FROM_CLOUD(iTeam, iRow)
								
								//Set the priority of the thing
								SWITCH iType
								CASE CREATION_TYPE_PEDS 
									g_FMMC_STRUCT_ENTITIES.sPlacedPed[g_FMMC_STRUCT_ENTITIES.iNumberOfPeds -1].iPriority[iTeam] = g_CreatorsSelDetails.sTeamOptions[iTeam].iNumberOfActiveRows
									FMMC_SET_LONG_BIT(g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[g_CreatorsSelDetails.sTeamOptions[iTeam].iNumberOfActiveRows].iEntityBitSet, g_FMMC_STRUCT_ENTITIES.iNumberOfPeds -1)
									#IF IS_DEBUG_BUILD
										PRINTLN("g_FMMC_STRUCT_ENTITIES.sPlacedPed[", g_FMMC_STRUCT_ENTITIES.iNumberOfPeds -1, "].iPriority[", iTeam, "] =", g_FMMC_STRUCT_ENTITIES.sPlacedPed[g_FMMC_STRUCT_ENTITIES.iNumberOfPeds -1].iPriority[iTeam])
									#ENDIF
									g_CreatorsSelDetails.sTeamOptions[iTeam].iNumberOfActiveRows++
								BREAK
								CASE CREATION_TYPE_VEHICLES
									g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles -1].iPriority[iTeam]  = g_CreatorsSelDetails.sTeamOptions[iTeam].iNumberOfActiveRows
									FMMC_SET_LONG_BIT(g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[g_CreatorsSelDetails.sTeamOptions[iTeam].iNumberOfActiveRows].iEntityBitSet, g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles -1)
									#IF IS_DEBUG_BUILD
										PRINTLN("g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[", g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles -1, "].iPriority[", iTeam, "] =", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles -1].iPriority[iTeam])
									#ENDIF
									g_CreatorsSelDetails.sTeamOptions[iTeam].iNumberOfActiveRows++
								BREAK
								CASE CREATION_TYPE_OBJECTS
									g_FMMC_STRUCT_ENTITIES.sPlacedObject[g_FMMC_STRUCT_ENTITIES.iNumberOfObjects -1].iPriority[iTeam]  = g_CreatorsSelDetails.sTeamOptions[iTeam].iNumberOfActiveRows
									FMMC_SET_LONG_BIT(g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[g_CreatorsSelDetails.sTeamOptions[iTeam].iNumberOfActiveRows].iEntityBitSet, g_FMMC_STRUCT_ENTITIES.iNumberOfObjects -1)
									#IF IS_DEBUG_BUILD
										PRINTLN("g_FMMC_STRUCT_ENTITIES.sPlacedObject[", g_FMMC_STRUCT_ENTITIES.iNumberOfObjects -1, "].iPriority[", iTeam, "] =", g_FMMC_STRUCT_ENTITIES.sPlacedObject[g_FMMC_STRUCT_ENTITIES.iNumberOfObjects -1].iPriority[iTeam])
									#ENDIF
									g_CreatorsSelDetails.sTeamOptions[iTeam].iNumberOfActiveRows++
								BREAK
								ENDSWITCH
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
			//if a rule is to be removed
			ELIF IS_BIT_SET(sCurrentVarsStruct.iUpdateRule, ciREMOVE_RULE)
		
			ENDIF
		ENDIF
	ENDFOR	
	
	sFMMCmenu.iNewRule = 0	
	sCurrentVarsStruct.iUpdateRule = 0
	RESET_ENTITY_CREATION_MENU_ASAP(sCurrentVarsStruct)
ENDPROC

PROC SET_FMMC_GLOBALS_TO_MENU_OPTIONS()
	//Team 0
	INT iOrder
	INT iEntity
	INT iTeam
	
	CHECK_FOR_BAD_EXTRA_OBJECTIVE_DATA()
	
	FMMC_GET_LAUNCH_TIMES_FROM_BITSET(g_FMMC_STRUCT.iLaunchTimesBit, g_FMMC_STRUCT.iLaunchStartTime, g_FMMC_STRUCT.iLaunchEndTime)
	PRINTLN("MISSION START TIME - ", g_FMMC_STRUCT.iLaunchStartTime, " END TIME - ", g_FMMC_STRUCT.iLaunchEndTime)
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciMISSION_OPTION_BS_HEIST_HIGH_END)
		g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_HEIST
	ELIF IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciMISSION_OPTION_BS_HEIST_PLANNING)
		g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_PLANNING
	ELIF IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciMISSION_OPTION_BS_CONTACT_MISSION)
		g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_CONTACT
	ELIF IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciMISSION_OPTION_BS_RANDOM_EVENT)
		g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_RANDOM
	ELIF IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciMISSION_OPTION_BS_RANDOM_VERSUS)
		g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_VERSUS
	ELIF IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciMISSION_OPTION_BS_LTS)
		g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_LTS
	ELIF IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciMISSION_OPTION_BS_CTF)
		g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_CTF
	ELIF IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciMISSION_OPTION_BS_COOP)
		g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_COOP
	ELIF IS_3D_EXPLODER_CREATOR()
		g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_EXPLODER
	ELSE
		g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_STANDARD
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciMISSION_OPTION_BS_VOTING_DISABLED) 
		CLEAR_BIT(g_FMMC_STRUCT.iOptionsMenuBitSet, ciVOTING_ENABLED)
	ELSE
		SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSet, ciVOTING_ENABLED)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciMISSION_OPTION_BS_START_AT_ALT_LOC) 
		SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSet, ciSTART_AT_ALTERNATE_LOC)
	ELSE	
		CLEAR_BIT(g_FMMC_STRUCT.iOptionsMenuBitSet, ciSTART_AT_ALTERNATE_LOC)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSet, ciTEAM_LIVES_OPTION)
		sFMMCmenu.iTeamLivesOption = ciMISSION_LIVES_OPTION_TEAM
	ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSet, ciALL_LIVES_POOL_OPTION)
		sFMMCmenu.iTeamLivesOption = ciMISSION_LIVES_OPTION_POOL	
	ELSE
		sFMMCmenu.iTeamLivesOption = ciMISSION_LIVES_OPTION_PLAYER
	ENDIF
	
	CLEAR_BIT(g_FMMC_STRUCT.iOptionsMenuBitSet, ciTEAM_LIVES_OPTION)
	CLEAR_BIT(g_FMMC_STRUCT.iOptionsMenuBitSet, ciALL_LIVES_POOL_OPTION)							
	IF sFMMCmenu.iTeamLivesOption = ciMISSION_LIVES_OPTION_PLAYER
		
	ELIF sFMMCmenu.iTeamLivesOption = ciMISSION_LIVES_OPTION_TEAM
		SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSet, ciTEAM_LIVES_OPTION)
	ELIF sFMMCmenu.iTeamLivesOption = ciMISSION_LIVES_OPTION_POOL
		SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSet, ciALL_LIVES_POOL_OPTION)
	ENDIF
	
	IF g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_HEIST
	OR g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_CONTACT
	OR g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_PLANNING
		CLEAR_BIT(sFMMCmenu.iOptionsMenuBitSet, OPTION_MISSION_POINTS_HUD)
	ELSE
		SET_BIT(sFMMCmenu.iOptionsMenuBitSet, OPTION_MISSION_POINTS_HUD)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciMISSION_OPTION_BS_TUTORIAL)
		g_FMMC_STRUCT.iMissionTutorial = 1
	ELSE
		g_FMMC_STRUCT.iMissionTutorial = 0
	ENDIF
	
	IF g_FMMC_STRUCT.iCashReward >= FMMC_CASH_BAND_MAX
		g_FMMC_STRUCT.iCashReward = 0
	ENDIF
	
	PRINTLN("MISSION TYPE = ", g_FMMC_STRUCT.iMissionSubType, " TUTORIAL = ", g_FMMC_STRUCT.iMissionTutorial)
	
	INT iTempRule
	
	FOR iEntity = 0 TO FMMC_MAX_RULES-1
		FOR iTeam = 0 TO FMMC_MAX_TEAMS-1
			PRINTLN("GOTODEBUG2: g_FMMC_STRUCT_ENTITIES.sGotoLocationData[",iEntity,"].iRule[",iTeam,"] = 		", g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iEntity].iRule[iTeam])
			PRINTLN("GOTODEBUG2: g_FMMC_STRUCT_ENTITIES.sGotoLocationData[",iEntity,"].iPriority[",iTeam,"] = 	", g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iEntity].iPriority[iTeam])
		ENDFOR
	ENDFOR
	
	INT j
	
	FOR iTeam = 0 TO (g_FMMC_STRUCT.iMaxNumberOfTeams -1)
		//Convert the number of rules to the numver of rows
		g_CreatorsSelDetails.sTeamOptions[iTeam].iNumberOfActiveRows = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iNumberOfTeamRules
		FOR iOrder = 0 TO (g_CreatorsSelDetails.sTeamOptions[iTeam].iNumberOfActiveRows -1)
		
			FOR iEntity = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfPeds - 1)
				IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iEntity].iPriority[iTeam] = iOrder
					FMMC_SET_LONG_BIT(g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[iOrder].iEntityBitSet, iEntity)
					g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[iOrder].iRule =  GET_RULE_SELECTION_FROM_RULE(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iEntity].iRule[iTeam], CREATION_TYPE_PEDS)
				ENDIF
				IF IS_ENTITY_AN_EXTRA_OBJECTIVE_ENTITY(iEntity, ciRULE_TYPE_PED)
					FOR j = 0 TO MAX_NUM_EXTRA_OBJECTIVE_PER_ENTITY-1
						IF g_FMMC_STRUCT.iExtraObjectivePriority[GET_ENTITY_EXTRA_OBJECTIVE_ENTITY_INDEX(iEntity, ciRULE_TYPE_PED)][j][iTeam] = iOrder
							FMMC_SET_LONG_BIT(g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[iOrder].iEntityBitSet, iEntity)
							g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[iOrder].iRule =  g_FMMC_STRUCT.iExtraObjectiveRule[GET_ENTITY_EXTRA_OBJECTIVE_ENTITY_INDEX(iEntity, ciRULE_TYPE_PED)][j][iTeam]
						ENDIF
					ENDFOR
				ENDIF
			ENDFOR
			
			WAIT(0)
			
			FOR iEntity = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles - 1)
				IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iEntity].iPriority[iTeam] = iOrder
					iTempRule = GET_RULE_SELECTION_FROM_RULE(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iEntity].iRule[iTeam], CREATION_TYPE_VEHICLES)
					FMMC_SET_LONG_BIT(g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[iOrder].iEntityBitSet, iEntity)
					g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[iOrder].iRule =   iTempRule
				ENDIF
				IF IS_ENTITY_AN_EXTRA_OBJECTIVE_ENTITY(iEntity, ciRULE_TYPE_VEHICLE)
					FOR j = 0 TO MAX_NUM_EXTRA_OBJECTIVE_PER_ENTITY-1
						IF g_FMMC_STRUCT.iExtraObjectivePriority[GET_ENTITY_EXTRA_OBJECTIVE_ENTITY_INDEX(iEntity, ciRULE_TYPE_VEHICLE)][j][iTeam] = iOrder
							FMMC_SET_LONG_BIT(g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[iOrder].iEntityBitSet, iEntity)
							g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[iOrder].iRule =  g_FMMC_STRUCT.iExtraObjectiveRule[GET_ENTITY_EXTRA_OBJECTIVE_ENTITY_INDEX(iEntity, ciRULE_TYPE_VEHICLE)][j][iTeam]
						ENDIF
					ENDFOR
				ENDIF
			ENDFOR
			
			WAIT(0)
			
			FOR iEntity = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfObjects - 1)
				IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iEntity].iPriority[iTeam] = iOrder
					FMMC_SET_LONG_BIT(g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[iOrder].iEntityBitSet, iEntity)
					PRINTLN("[ExtraObjectives] (Standard) Setting bit ", GET_LONG_BITSET_BIT(iEntity), " in rule ", iOrder)
					g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[iOrder].iRule =  GET_RULE_SELECTION_FROM_RULE(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iEntity].iRule[iTeam], CREATION_TYPE_OBJECTS)
				ENDIF 
				IF IS_ENTITY_AN_EXTRA_OBJECTIVE_ENTITY(iEntity, ciRULE_TYPE_OBJECT)
					FOR j = 0 TO MAX_NUM_EXTRA_OBJECTIVE_PER_ENTITY-1
						IF g_FMMC_STRUCT.iExtraObjectivePriority[GET_ENTITY_EXTRA_OBJECTIVE_ENTITY_INDEX(iEntity, ciRULE_TYPE_OBJECT)][j][iTeam] = iOrder
							FMMC_SET_LONG_BIT(g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[iOrder].iEntityBitSet, iEntity)
							PRINTLN("[ExtraObjectives] (EXTRA) Setting bit ", GET_LONG_BITSET_BIT(iEntity), " in rule ", iOrder)
							g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[iOrder].iRule =  g_FMMC_STRUCT.iExtraObjectiveRule[GET_ENTITY_EXTRA_OBJECTIVE_ENTITY_INDEX(iEntity, ciRULE_TYPE_OBJECT)][j][iTeam]
						ENDIF
					ENDFOR
				ENDIF
			ENDFOR
			
			WAIT(0)
			
			FOR iEntity = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfGoToLocations[0] - 1)
				IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iEntity].iPriority[iTeam]	= iOrder
					FMMC_SET_LONG_BIT(g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[iOrder].iEntityBitSet, iEntity)
					g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[iOrder].iRule =  GET_RULE_SELECTION_FROM_RULE(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iEntity].iRule[iTeam], CREATION_TYPE_GOTO_LOC)
					PRINTLN("GO TO DEBUG: g_CreatorsSelDetails.sTeamOptions[", iTeam, "].sSelection[", iOrder, "].iRule = ", g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[iOrder].iRule)
				ENDIF
				IF IS_ENTITY_AN_EXTRA_OBJECTIVE_ENTITY(iEntity, ciRULE_TYPE_GOTO)
					FOR j = 0 TO MAX_NUM_EXTRA_OBJECTIVE_PER_ENTITY-1
						IF g_FMMC_STRUCT.iExtraObjectivePriority[GET_ENTITY_EXTRA_OBJECTIVE_ENTITY_INDEX(iEntity, ciRULE_TYPE_GOTO)][j][iTeam] = iOrder
							FMMC_SET_LONG_BIT(g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[iOrder].iEntityBitSet, iEntity)
							g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[iOrder].iRule =  g_FMMC_STRUCT.iExtraObjectiveRule[GET_ENTITY_EXTRA_OBJECTIVE_ENTITY_INDEX(iEntity, ciRULE_TYPE_GOTO)][j][iTeam]
						ENDIF
					ENDFOR
				ENDIF
			ENDFOR
			
			FOR iEntity = 0 TO (g_FMMC_STRUCT.iNumberOfPlayerRules[iTeam] - 1)
				IF g_FMMC_STRUCT.sPlayerRuleData[iEntity].iPriority[iTeam] = iOrder
					FMMC_SET_LONG_BIT(g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[iOrder].iEntityBitSet, iEntity)
					g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[iOrder].iRule =  GET_RULE_SELECTION_FROM_RULE(g_FMMC_STRUCT.sPlayerRuleData[iEntity].iRule[iTeam], -1)
					g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_SELECTED_ENTITES] = g_FMMC_STRUCT.sPlayerRuleData[iEntity].iPlayerRuleLimit[iTeam]
					g_CreatorsSelDetails.iPlayerRuleMapping[iTeam][iOrder] = iEntity
				ENDIF
			ENDFOR
			
			//Set up the rules
			g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_TIME]  						= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjectiveTimeLimitRule[iOrder]
			g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_SCORE] 						= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjectiveScore[iOrder]
			g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_TARGET_SCORE] 				= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTargetScore[iOrder]
			g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_NUM_CARRYABLE] 				= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMaxDeliveryEntitiesCanCarry[iOrder] - 1
			g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_HACK_SCREEN] 				= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iHackScreen[iOrder]
			g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_SECONDARY_ANIM] 				= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iSecondaryAnim[iOrder]
			g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_OUTFIT]						= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iOutfit[iOrder]
			g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_OUTFIT_VARIATIONS]			= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iOutfitVariations[iOrder]
			g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_PROGRESS_MINIGAME]			= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iProgressMinigame[iOrder]
			g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_HIT_SOUND]					= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iHitSound[iOrder]
			g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_REQUIRED_DELIVERIES]			= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRequiredDeliveries[iOrder] - 1
			g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_OVERWRITE_TYPE]				= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].OverwriteEntityType[iOrder]
			g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_OVERWRITE_ID]				= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].OverwriteEntityID[iOrder]
			g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_NEAREST_TARGET_THRESHOLD]	= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iNearestTargetThreshold[iOrder]
			g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_TRAFFIC]						= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleTraffic[iOrder]
			g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_PED_DENSITY]					= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRulePedDensity[iOrder]
			g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_MIDPOINT_FAIL_TIMER]			= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMidpointFailTime[iOrder]
			g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_TEAM_LIVES_OVERRIDE]			= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iPlayerLivesPerRule[iOrder]
			g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_SUDDEN_DEATH_TIMER]			= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjectiveSuddenDeathTimeLimit[iOrder]
			g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_VEHICLE_DAMAGE_THRESHOLD]	= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMinVehHealth[iOrder]
			g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_VEHICLE_DAMAGE_SPOOK_ID]		= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMinVehHealthSpookID[iOrder]
			g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_VEHICLE_DAMAGE_SPOOK_DIST]	= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMinVehHealthSpookDist[iOrder]
			g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_DRAW_PLAYER_POSITION]		= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamWithPlayerPositionToDraw[iOrder]
			g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_MAX_HEALTH]					= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMaxHealth[iOrder]
			g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_CARGOBOB_MANUAL_RESPAWN]		= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iCBManRespawnExclusionRadius[iOrder]
			g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_RESPAWN_IN_MISSION_VEH] 		= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRespawnInMissionVehicle[iOrder]
			g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_SWAP_TEAM_IN_LOCATE]			= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iLocateSwapTeam[iOrder]			
			g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_AMBIENT_POLICE_ACCURACY]		= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iAmbientPoliceAccuracy[iOrder]			
			g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_PLAY_DOWNLOAD_SOUND_AFTER_DIALOGUE]	= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iPlayDownloadSoundAfterDialogue[iOrder]
			g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_MINIGAME_TAKE_PROGRESSION]			= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTakeProgressionAmount[iOrder]
						
			IF IS_BIT_SET(g_FMMC_STRUCT.iTimerStart[iTeam], iOrder)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_MULTI_RULE_TIMER] = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTimeLimit[iOrder]+1
			ELIF IS_BIT_SET(g_FMMC_STRUCT.iTimerStop[iTeam], iOrder)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_MULTI_RULE_TIMER] = 1
			ELSE
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_MULTI_RULE_TIMER] = 0
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitset[iOrder], ciBS_RULE_ADVANCE_ON_AIM)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_ADVANCE_ON_AIM] = 1
			ELSE
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_ADVANCE_ON_AIM] = 0
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitset[iOrder], ciBS_RULE_HIDE_GPS_ON_SECONDARY)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_HIDE_GPS_ON_SECONDARY] = 1
			ELSE
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_HIDE_GPS_ON_SECONDARY] = 0
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThree[iOrder], ciBS_RULE3_CANT_GET_OUT_NEAR_DELIVERY)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_CANT_EXIT_NEAR_DELIVERY] = 1
			ELSE
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_CANT_EXIT_NEAR_DELIVERY] = 0
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThree[iOrder], ciBS_RULE3_OBJECTIVE_TIMER_FAIL1)
			AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThree[iOrder], ciBS_RULE3_OBJECTIVE_TIMER_FAIL2)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_OBJECITVE_TIMER_FAIL_TEXT] = 1
			ELIF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThree[iOrder], ciBS_RULE3_OBJECTIVE_TIMER_FAIL1)
			AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThree[iOrder], ciBS_RULE3_OBJECTIVE_TIMER_FAIL2)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_OBJECITVE_TIMER_FAIL_TEXT] = 2
			ELIF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThree[iOrder], ciBS_RULE3_OBJECTIVE_TIMER_FAIL1)
			AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThree[iOrder], ciBS_RULE3_OBJECTIVE_TIMER_FAIL2)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_OBJECITVE_TIMER_FAIL_TEXT] = 0
			ENDIF
						
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitset[iOrder], ciBS_RULE_HIDE_DROPOFF_CORONA)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_HIDE_DROPOFF_CORONA] = 1 
			ELSE
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_HIDE_DROPOFF_CORONA] = 0
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitset[iOrder], ciBS_RULE_USE_SCORE_TEXT)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_USE_SCORE_TEXT] = 1 
			ELSE
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_USE_SCORE_TEXT] = 0
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitset[iOrder], ciBS_RULE_HIDE_ON_PLANNING_BOARD)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_HIDE_ON_PLANNING_BOARD] = 1
			ELSE
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_HIDE_ON_PLANNING_BOARD] = 0
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwo[iOrder], ciBS_RULE2_HIDE_ON_MAP)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_HIDE_ON_MAP] = 1
			ELSE
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_HIDE_ON_MAP] = 0
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitset[iOrder], ciBS_RULE_BLOCK_MECHANIC_DELIVERY)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_BLOCK_MECHANIC] = 1
			ELSE
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_BLOCK_MECHANIC] = 0
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitset[iOrder], ciBS_RULE_VEHPHOTO_NUMPLATES)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_PHOTOGRAPH_LICENSE_PLATE] = 1
			ELSE
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_PHOTOGRAPH_LICENSE_PLATE] = 0
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitset[iOrder], ciBS_RULE_ENABLE_SPECTATE)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_ENABLE_SPECTATE] = 1
			ELSE
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_ENABLE_SPECTATE] = 0
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitset[iOrder], ciBS_RULE_CUSTOM_CROWD_CONTROL_FLEECA)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_CUSTOM_CROWD_CONTROL] = 2
			ELIF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitset[iOrder], ciBS_RULE_CUSTOM_CROWD_CONTROL_ORNATE)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_CUSTOM_CROWD_CONTROL] = 1
			ELSE
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_CUSTOM_CROWD_CONTROL] = 0
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitset[iOrder], ciBS_RULE_ENABLETRACKIFY)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_ENABLE_TRACKIFY] = 1
			ELSE
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_ENABLE_TRACKIFY] = 0
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwo[iOrder], ciBS_RULE2_SLIPSTREAM)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_SLIPSTREAM] = 1
			ELSE
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_SLIPSTREAM] = 0
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFour[iOrder], ciBS_RULE4_DontExplodeWhenUnderMinSpeed)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][ciBS_RULE4_DontExplodeWhenUnderMinSpeed] = 1
			ELSE
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][ciBS_RULE4_DontExplodeWhenUnderMinSpeed] = 0
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iLinkedDoorSwingFree1, iOrder)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_SWING_FREE_1] = 1
			ELSE
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_SWING_FREE_1] = 0
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iLinkedDoorSwingFree2, iOrder)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_SWING_FREE_2] = 1
			ELSE
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_SWING_FREE_2] = 0
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitset[iOrder], ciBS_RULE_INVALIDATE_OTHER_TEAMS)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_INVALIDATE_OTHER_TEAMS] = 1
			ELSE
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_INVALIDATE_OTHER_TEAMS] = 0
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iSmallDropOffBitSet, iOrder)
			AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iExactDropOffBitSet, iOrder)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_DROP_OFF_CORONA] 		= 0
			ELIF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iSmallDropOffBitSet, iOrder)
			AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iExactDropOffBitSet, iOrder)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_DROP_OFF_CORONA] 		= 2
			ELSE
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_DROP_OFF_CORONA] 		= 1
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRespawnInVehicleBitset, iOrder)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_RESPAWN_IN_VEH] 		= 1
			ELSE
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_RESPAWN_IN_VEH] 		= 0
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iPhotoCanBeDead, iOrder)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_DEAD_PHOTO] 		= 1
			ELSE
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_DEAD_PHOTO] 		= 0
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamFailBitset, iOrder)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_FAIL_OBJECTIVE] 		= 1
				PRINTLN("g_CreatorsSelDetails.sSubRowOptions[", iTeam, "].iSelection[", iOrder, "][FMMC_SUB_OPTIONS_FAIL_OBJECTIVE] = 1")
			ELSE
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_FAIL_OBJECTIVE] 		= 0
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDropOffGPSBitSet, iOrder)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_DROP_OFF_GPS] 		= 1
				PRINTLN("g_CreatorsSelDetails.sSubRowOptions[", iTeam, "].iSelection[", iOrder, "][FMMC_SUB_OPTIONS_DROP_OFF_GPS] = 1")
			ELSE
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_DROP_OFF_GPS] 		= 0
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSix[iOrder], ciBS_RULE6_COUNTDOWN_TIMER_SOUND_THIRTYSECONDS)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_COUNTDOWN_SOUNDS] = 4
			ELIF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwo[iOrder], ciBS_RULE2_COUNTDOWN_TIMER_SOUND_TENSECOND)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_COUNTDOWN_SOUNDS] = 3
			ELIF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwo[iOrder], ciBS_RULE2_COUNTDOWN_TIMER_SOUND_FIVESECOND)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_COUNTDOWN_SOUNDS] = 2
			ELIF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwo[iOrder], ciBS_RULE2_COUNTDOWN_TIMER_SOUND_ONESHOT)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_COUNTDOWN_SOUNDS] = 1
			ELSE
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_COUNTDOWN_SOUNDS] = 0
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSix[iOrder], 	ciBS_RULE6_SD_CD_10_AND_5_SECONDS)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_SUDDENDEATH_CD_SOUNDS] = 3
				
			ELIF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSix[iOrder], ciBS_RULE6_SD_CD_10_SECONDS)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_SUDDENDEATH_CD_SOUNDS] = 2
				
			ELIF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSix[iOrder], ciBS_RULE6_SD_CD_5_SECONDS)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_SUDDENDEATH_CD_SOUNDS] = 1
			ELSE
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_SUDDENDEATH_CD_SOUNDS] = 0
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThree[iOrder], ciBS_RULE3_LOOP_ALL_DIALOGUE_TRIGGERS)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_LOOP_ALL_DIALOGUE_TRIGGERS] = 1
			ELSE
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_LOOP_ALL_DIALOGUE_TRIGGERS] = 0
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThree[iOrder], ciBS_RULE3_PROGRESS_IN_SAME_PLACE)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_PROGRESS_IN_SAME_PLACE] = 1
			ELSE
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_PROGRESS_IN_SAME_PLACE] = 0
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwo[iOrder], ciBS_RULE2_RESTRICT_VOICE_CHAT)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_RESTRICT_VOICE_CHAT] = 1
			ELSE
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_RESTRICT_VOICE_CHAT] = 0
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwo[iOrder], ciBS_RULE2_ENABLE_EMERGENCY_CALLS)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_ENABLE_EMERGENCY_CALLS] = 1
			ELSE
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_ENABLE_EMERGENCY_CALLS] = 0
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwo[iOrder], ciBS_RULE2_DISABLE_CROWD_FEAR)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_DISABLE_CROWD_FEAR] = 1
			ELSE
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_DISABLE_CROWD_FEAR] = 0
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwo[iOrder], ciBS_RULE2_QUICK_EQUIP_MASK)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_QUICK_EQUIP_MASK] = 1
			ELSE
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_QUICK_EQUIP_MASK] = 0
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThree[iOrder], ciBS_RULE3_QUICK_EQUIP_REBREATHER)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_QUICK_EQUIP_REBREATHER] = 1
			ELSE
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_QUICK_EQUIP_REBREATHER] = 0
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwo[iOrder], ciBS_RULE2_RALLY_MODE_ARROWS)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_RALLY_MODE_ARROWS] = 1
			ELSE
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_RALLY_MODE_ARROWS] = 0
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThree[iOrder], ciBS_RULE3_RALLY_MODE_ARROWS_DRIVER)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_RALLY_DRIVER] = 1
			ELSE
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_RALLY_DRIVER] = 0
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThree[iOrder], ciBS_RULE3_OVERRIDE_COLLECTION_TEXT)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_OVERRIDE_COLLECTION] = 1
			ELSE
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_OVERRIDE_COLLECTION] = 0
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThree[iOrder], ciBS_RULE3_DONT_COUNT_TOWARDS_SCORE)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_COUNTS_TOWARDS_MEDAL_SCORE] = 1
			ELSE
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_COUNTS_TOWARDS_MEDAL_SCORE] = 0
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwo[iOrder], ciBS_RULE2_DONT_GIVE_PLAYER_XP)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_DONT_GIVE_XP] = 1
			ELSE
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_DONT_GIVE_XP] = 0
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwo[iOrder], ciBS_RULE2_DISABLE_MASK_AND_GEAR_CHANGING)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_DISABLE_MASK_AND_GEAR_CHANGING] = 1
			ELSE
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_DISABLE_MASK_AND_GEAR_CHANGING] = 0
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwo[iOrder], ciBS_RULE2_FORCE_COMBAT_MODE)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_FORCE_COMBAT_MODE] = 1
			ELIF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThree[iOrder], ciBS_RULE3_FORCE_COMBAT_MODE_OFF)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_FORCE_COMBAT_MODE] = 2
			ELSE
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_FORCE_COMBAT_MODE] = 0
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFourteen[iOrder], ciBS_RULE14_FORCE_WEAPON_IN_HAND_ON_RULE)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_FORCE_WEAPON_IN_HAND_THIS_RULE] = 1
			ELSE
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_FORCE_WEAPON_IN_HAND_THIS_RULE] = 0
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFifteen[iOrder], ciBS_RULE15_FORCE_STARTING_WEAPON_IN_HAND_ON_RULE)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_FORCE_WEAPON_IN_HAND_THIS_RULE] = 1
			ELSE
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_FORCE_WEAPON_IN_HAND_THIS_RULE] = 0
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iVehRuleWaitForTeamsBS[iOrder][iTeam],ciFMMC_VehRuleTeamBS_IGNORE_CHECK_FOR_ON_VEH_RULE)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_WAIT_FOR_VEHICLE_RULE] = 1
			ELSE
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_WAIT_FOR_VEHICLE_RULE] = 0
			ENDIF
						
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwo[iOrder],ciBS_RULE2_USE_TRIP_SKIP)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_USE_TRIP_SKIP] = 1
			ELSE
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_USE_TRIP_SKIP] = 0
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThree[iOrder],ciBS_RULE3_DISABLE_PV_SPAWNING)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_DISABLE_PV] = 1
			ELSE
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_DISABLE_PV] = 0
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFour[iOrder],ciBS_RULE4_CAN_DROP_PACKAGE)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_CAN_DROP_PACKAGE] = 1
			ELSE
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_CAN_DROP_PACKAGE] = 0
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFour[iOrder],ciBS_RULE4_DISPLAY_ICON_FOR_CARRIER)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_PACKAGE_HOLDER_ICON] = 1
			ELSE
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_PACKAGE_HOLDER_ICON] = 0
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThree[iOrder],ciBS_RULE3_DISABLE_SHOPS)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_DISABLE_SHOPS] = 1
			ELSE
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_DISABLE_SHOPS] = 0
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThree[iOrder],ciBS_RULE3_BLIP_AFTER_DELIVERY)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_BLIPPED_AFTER_DELIVERY] = 1
			ELSE
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_BLIPPED_AFTER_DELIVERY] = 0
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFour[iOrder], ciBS_RULE4_FAIL_IF_DETECTED_BY_POLICE)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_FAIL_IF_DETECTED_BY_POLICE] = 1
			ELSE
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_FAIL_IF_DETECTED_BY_POLICE] = 0
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThirteen[iOrder], ciBS_RULE13_TOGGLE_QUICKGPS)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_TOGGLE_QUICK_GPS] = 1
			ELSE
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_TOGGLE_QUICK_GPS] = 0
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThirteen[iOrder], ciBS_RULE13_TOGGLE_HINTCAM)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_TOGGLE_HINT_CAM] = 1
			ELSE
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_TOGGLE_HINT_CAM] = 0
			ENDIF			
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThirteen[iOrder], ciBS_RULE13_HALF_HINT_CAM_FOCUS_RANGE)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_HALF_HINT_CAM_FOCUS_RANGE] = 1
			ELSE
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_HALF_HINT_CAM_FOCUS_RANGE] = 0
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThirteen[iOrder], ciBS_RULE13_START_ALL_ZONE_TIMERS)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_START_ALL_ZONE_TIMERS] = 1
			ELSE
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_START_ALL_ZONE_TIMERS] = 0
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThirteen[iOrder], ciBS_RULE13_STOP_ALL_ZONE_TIMERS)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_STOP_ALL_ZONE_TIMERS] = 1
			ELSE
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_STOP_ALL_ZONE_TIMERS] = 0
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThirteen[iOrder], ciBS_RULE13_STOP_POISON_GAS)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_STOP_POISON_GAS] = 1
			ELSE
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_STOP_POISON_GAS] = 0
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThirteen[iOrder], ciBS_RULE13_EXPLODE_VAULT_DOOR)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_EXPLODE_VAULT_DOOR] = 1
			ELSE
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_EXPLODE_VAULT_DOOR] = 0
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFourteen[iOrder], ciBS_RULE14_USE_LOCATION_COLLECTION_DELAY)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_USE_LOCATION_COLLECTION_DELAY] = 1
			ELSE
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_USE_LOCATION_COLLECTION_DELAY] = 0
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFourteen[iOrder], ciBS_RULE14_ATTACH_FLARE_TO_BACK)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_ATTACH_FLARE_TO_BACK] = 1
			ELSE
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_ATTACH_FLARE_TO_BACK] = 0
			ENDIF
						
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFifteen[iOrder], ciBS_RULE15_END_OF_MISSION_INVINCIBILITY)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_END_OF_MISSION_INVINCIBILITY] = 1
			ELSE
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_END_OF_MISSION_INVINCIBILITY] = 0
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEleven[iOrder], ciBS_RULE11_RESPAWN_IN_LAST_MISSION_VEHICLE_FROM_LIST)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_RESPAWN_IN_LAST_MISSION_VEH_FROM_LIST] = 1
			ELSE
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iOrder][FMMC_SUB_OPTIONS_RESPAWN_IN_LAST_MISSION_VEH_FROM_LIST] = 0
			ENDIF
			
			IF iOrder = 13 AND iTeam = 2
				PRINTLN("g_CreatorsSelDetails.sTeamOptions[",iTeam,"].sSelection[",iOrder,"].iRule = ",g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[iOrder].iRule)
				PRINTLN("g_CreatorsSelDetails.sTeamOptions[",iTeam,"].sSelection[",iOrder,"].iType = ",g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[iOrder].iType)
			ENDIF
	
			
		ENDFOR

	ENDFOR
	
ENDPROC

PROC CLEAN_UP_TOGGLE_ENTITY_EDITTED_STATUS()
	INT i, i2
	FOR i = 0 TO FMMC_MAX_PEDS_BITSET-1
		sPedStruct.iBS_EdittedToggle[i] = 0
	ENDFOR
	FOR i = 0 TO FMMC_MAX_WEAPON_BITSET-1
		sWepStruct.iBS_EdittedToggle[i] = 0
	ENDFOR
	FOR i = 0 TO FMMC_MAX_PROP_BITSET-1
		sPropStruct.iBS_EdittedToggle[i] = 0
	ENDFOR	
	FOR i = 0 TO FMMC_MAX_INTERACTABLE_BITSET-1
		sInteractableCreationStruct.iBS_EdittedToggle[i] = 0
	ENDFOR	
	FOR i = 0 TO FMMC_MAX_TEAMS-1
		FOR i2 = 0 TO FMMC_MAX_TEAM_SPAWN_POINT_BITSET-1
			sTeamSpawnStruct[i].iBS_EdittedToggle[i2] = 0
		ENDFOR
	ENDFOR	
	FOR i = 0 TO FMMC_MAX_DIALOGUE_TRIGGER_BITSET-1
		sCurrentVarsStruct.iDialogueTrigger_iBS_EdittedToggle[i] = 0
	ENDFOR
	sVehStruct.iBS_EdittedToggle = 0	
	sObjStruct.iBS_EdittedToggle = 0
	sDynoPropStruct.iBS_EdittedToggle = 0
	sLocStruct.iBS_EdittedToggle = 0
ENDPROC

PROC CLEAN_UP_ENTITY_EDITTED_STATUS()
	INT i, i2
	FOR i = 0 TO FMMC_MAX_PEDS_BITSET-1
		sPedStruct.iBS_Editted[i] = 0
	ENDFOR
	FOR i = 0 TO FMMC_MAX_WEAPON_BITSET-1
		sWepStruct.iBS_Editted[i] = 0
	ENDFOR
	FOR i = 0 TO FMMC_MAX_PROP_BITSET-1
		sPropStruct.iBS_Editted[i] = 0
	ENDFOR	
	FOR i = 0 TO FMMC_MAX_TEAMS-1
		FOR i2 = 0 TO FMMC_MAX_TEAM_SPAWN_POINT_BITSET-1
			sTeamSpawnStruct[i].iBS_Editted[i2] = 0
		ENDFOR
	ENDFOR	
	FOR i = 0 TO FMMC_MAX_DIALOGUE_TRIGGER_BITSET-1
		sCurrentVarsStruct.iDialogueTrigger_iBS_Editted[i] = 0
	ENDFOR	
	FOR i = 0 TO FMMC_MAX_INTERACTABLE_BITSET - 1
		sInteractableCreationStruct.iBS_Editted[i] = 0
	ENDFOR	
	sVehStruct.iBS_Editted = 0	
	sObjStruct.iBS_Editted = 0
	sLocStruct.iBS_Editted = 0	
	sDynoPropStruct.iBS_Editted = 0
	
	CLEAN_UP_TOGGLE_ENTITY_EDITTED_STATUS()
ENDPROC

PROC CLEAN_UP_FOR_TEST_MODE()
	IF DOES_MISSION_HAVE_ANY_PLACED_TURRETS()
		ARENA_CONTESTANT_TURRET_STACK_CLEAR()
	ENDIF
	
	SET_TEAM_SPAWN_VEHICLE_DATA_FROM_CREATOR_SELECTION(sVehStruct)
	
	COMMON_FMMC_SCRIPT_CLEAN_UP(sPedStruct,	sVehStruct,	sWepStruct,	sObjStruct,sPropStruct, sDynoPropStruct, sInvisibleObjects, sCurrentVarsStruct, sFMMCmenu, FALSE)	
	CLEANUP_ALL_CREATOR_INTERACTABLES(sInteractableCreationStruct)
	CLEANUP_ALL_CREATOR_TRAINS(sTrainCreationStruct)
	
	DELETE_BLIPS_AND_CHECKPOINTS()
	REMOVE_TEMP_ENTITES_IF_NOT_NEEDED(sFMMCmenu.iEntityCreation)
	SET_CORONA_ENTITIES_TO_DUMMY_MODELS()
	REMOVE_EXTRA_LINKED_DOORS_FROM_RULES()	
	RELEASE_CACHED_DOOR_OBJECTS(TRUE)
ENDPROC

PROC MAINTAIN_TEST_MISSION_STATE()

	IF IS_BIT_SET(sFMMCdata.iBitSet, biSetUpTestMission)
		bTextSetUp = FALSE
		
		IF NOT IS_SCREEN_FADED_OUT()
		AND NOT IS_SCREEN_FADING_OUT()
			DO_SCREEN_FADE_OUT(500)
			CLEAN_UP_FOR_TEST_MODE()
		ENDIF
		
		IF IS_SCREEN_FADED_OUT()
			
			IF IS_FAKE_MULTIPLAYER_MODE_SET() = FALSE
				SET_FAKE_MULTIPLAYER_MODE(TRUE)
			ENDIF
			
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_FM_MISSION_CONTROLLER_SCRIPT_NAME_HASH()) = 0
				REQUEST_SCRIPT(GET_FM_MISSION_CONTROLLER_SCRIPT_NAME())				
				IF HAS_SCRIPT_LOADED(GET_FM_MISSION_CONTROLLER_SCRIPT_NAME())
				AND IS_FAKE_MULTIPLAYER_MODE_SET()	
				AND NETWORK_IS_GAME_IN_PROGRESS()
					GlobalplayerBD[0].iGameState = MAIN_GAME_STATE_RUNNING
					START_NEW_SCRIPT(GET_FM_MISSION_CONTROLLER_SCRIPT_NAME(), MULTIPLAYER_FREEMODE_STACK_SIZE)
					PRINTLN("----------------------------------------------------------------------------")
					PRINTLN("--                                                                        --")
					PRINTLN("--                                                                        --")
					PRINTLN("--                                                                        --")
					PRINTLN("--                   LAUNCHING MISSION CONTROLLER                         --")
					PRINTLN("--                                                                        --")
					PRINTLN("--                                                                        --")
					PRINTLN("--                                                                        --")
					PRINTLN("----------------------------------------------------------------------------")
					SET_SCRIPT_AS_NO_LONGER_NEEDED(GET_FM_MISSION_CONTROLLER_SCRIPT_NAME())
					SET_TEAM_SPAWN_VEHICLE_DATA_FROM_CREATOR_SELECTION(sVehStruct)
					SET_MENU_OPTIONS_TO_FMMC_GLOBALS()
					SET_BIT(sFMMCdata.iBitSet, biSetUpTestMission)
					iDoorSetupStage = 0
					REMOVE_SCENARIO_BLOCKING_AREAS()
					SET_MOBILE_RADIO_ENABLED_DURING_GAMEPLAY(FALSE)
					SET_MOBILE_PHONE_RADIO_STATE(FALSE)
					#IF IS_DEBUG_BUILD
					PRINT_FMMC_DEBUGDATA(FALSE,TRUE)     
					#ENDIF
					
					IF NOT IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
						SWAP_CAMERAS(sCamData, sFMMCdata, sFMMCMenu, sCurrentVarsStruct, TRUE, TRUE)
					ENDIF
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
						SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)
						SET_PED_DIES_IN_WATER(PLAYER_PED_ID(), FALSE)
						SET_ENTITY_PROOFS(PLAYER_PED_ID(), TRUE, TRUE, TRUE, TRUE, TRUE)						
					ENDIF					
					
					IF g_Private_MultiplayerCreatorAllowDeathAndRespawning
						SET_LOCAL_PLAYER_RESPAWN_STATE(RESPAWN_STATE_PLAYING)
					ENDIF
					
					CLEAR_BIT(sFMMCdata.iBitSet, biSetUpTestMission)
					SET_CREATOR_AUDIO(FALSE, FALSE)
					REMOVE_ALL_DUMMY_BLIPS()
					g_FMMC_STRUCT.g_b_QuitTest = FALSE
					bMpNeedsCleanedUp = TRUE
					bTextSetUp = FALSE
					g_bFMMC_InTestMode_MissionController = TRUE
					g_TransitionSessionNonResetVars.iRestartBitset = 0
					
					// SETTING UP PLAYERS HAIR FOR CELEBRATION SCREEN
					IF GET_MP_BOOL_CHARACTER_STAT(MP_STAT_USING_HAIR_SA_STAT)
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iMyCurrentHair 		= GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_STORED_HAIRDO_SA) 
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iHairTintPrime 		= GET_MP_INT_CHARACTER_STAT(MP_STAT_HAIR_TINT)
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iHairTintSecondary 	= GET_MP_INT_CHARACTER_STAT(MP_STAT_SEC_HAIR_TINT)
					ELSE
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iMyCurrentHair 		= GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_STORED_HAIRDO) 
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iHairTintPrime 		= GET_MP_INT_CHARACTER_STAT(MP_STAT_HAIR_TINT)
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iHairTintSecondary 	= GET_MP_INT_CHARACTER_STAT(MP_STAT_SEC_HAIR_TINT)
					ENDIF
					
					INT iLocTeam
					INT iLocation
					FOR iLocation = 0 TO FMMC_MAX_GO_TO_LOCATIONS-1
						FOR iLocTeam = 0 TO FMMC_MAX_TEAMS-1
				            vSavedLoc[iLocation][iLocTeam] 		= g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLocation].vloc[iLocTeam]
							fSavedHeading[iLocation][iLocTeam]  = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLocation].fHeading[iLocTeam]
						ENDFOR
			            vSavedLoc1[iLocation] = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLocation].vloc1
			            vSavedLoc2[iLocation] = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLocation].vloc2				
					ENDFOR
					
					CLEAN_UP_ENTITY_EDITTED_STATUS()
					
					PED_COMP_NAME_ENUM eMyCurrentHair = INT_TO_ENUM(PED_COMP_NAME_ENUM, GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iMyCurrentHair)
					IF (eMyCurrentHair != DUMMY_PED_COMP)
						PED_COMP_NAME_ENUM eGRHairItem = DUMMY_PED_COMP
						IF (GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_M_FREEMODE_01)
							eGRHairItem = GET_MALE_HAIR(eMyCurrentHair)
						ELIF (GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_F_FREEMODE_01)
							eGRHairItem = GET_FEMALE_HAIR(eMyCurrentHair)
						ENDIF
						
						IF (eGRHairItem != DUMMY_PED_COMP)
						AND (eMyCurrentHair != eGRHairItem)
							PRINTLN("[MAINTAIN_STATE][CREATOR] gr_hair: replacing hair enum ", eMyCurrentHair, " with gunrunning hair enum ", eGRHairItem)
							GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iMyCurrentHair = ENUM_TO_INT(eGRHairItem)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF NETWORK_IS_GAME_IN_PROGRESS()
			IF bTextSetUp = FALSE
				Initialise_MP_Objective_Text()
				Initialise_MP_Communications()
				NET_PRINT("init objective text and comms called") NET_NL()
				bTextSetUp = TRUE
			ENDIF
			
			SET_PED_UNABLE_TO_DROWN(PLAYER_PED_ID())
			IF NOT GET_PLAYER_INVINCIBLE(PLAYER_ID())				
				IF NOT g_Private_MultiplayerCreatorAllowDeathAndRespawning
					SET_PLAYER_INVINCIBLE(PLAYER_ID(), TRUE)
				ENDIF
			ENDIF
			
		
			IF g_Private_MultiplayerCreatorAllowDeathAndRespawning
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
				SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), FALSE)
				SET_PED_DIES_IN_WATER(PLAYER_PED_ID(), FALSE)
				SET_ENTITY_PROOFS(PLAYER_PED_ID(), FALSE, FALSE, FALSE, FALSE, FALSE)
				SET_PLAYER_INVINCIBLE(PLAYER_ID(), FALSE)
				SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), FALSE)
				
				// -# Copied from net_debug.sch
				IF IS_DEBUG_KEY_JUST_PRESSED(KEY_K, KEYBOARD_MODIFIER_SHIFT, "Kill local player in Creator")
					IF NOT GET_PLAYER_INVINCIBLE(PLAYER_ID())
						NET_PRINT_TIME() NET_PRINT("UPDATE_GAME_DATA_WIDGETS Kill player.")	NET_NL()	
						
						SCRIPT_EVENT_DATA_TICKER_MESSAGE cheatTickerEventData
						cheatTickerEventData.TickerEvent = TICKER_EVENT_PLAYER_CHEATED_SUICIDE
						cheatTickerEventData.playerID = PLAYER_ID()
						BROADCAST_TICKER_EVENT(cheatTickerEventData, ALL_PLAYERS())
												
						CLEAR_ENTITY_LAST_DAMAGE_ENTITY(PLAYER_PED_ID())
						SET_ENTITY_HEALTH(PLAYER_PED_ID(), 0)
					ELSE
						NET_PRINT_TIME() NET_PRINT("UPDATE_GAME_DATA_WIDGETS Can't Kill Player - They are Invincible")	NET_NL()
					ENDIF
				ENDIF
				// -#				
			ENDIF
		ENDIF
		IF bTestModeControllerScriptStarted = FALSE
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_FM_MISSION_CONTROLLER_SCRIPT_NAME_HASH()) >= 1
			OR GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("FM_Survival_Controller")) >= 1
				PRINTLN("----------------------------------------------------------------------------")
				PRINTLN("--                                                                        --")
				PRINTLN("--                                                                        --")
				PRINTLN("--                                                                        --")
				PRINTLN("--       GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH          --")
				PRINTLN("--                                                                        --")
				PRINTLN("--                                                                        --")
				PRINTLN("--                                                                        --")
				PRINTLN("----------------------------------------------------------------------------")
				bTestModeControllerScriptStarted = TRUE
				SET_BIGMAP_ACTIVE(FALSE, FALSE)
				IF DOES_BLIP_EXIST(sStartEndBlips.biStart)
					REMOVE_BLIP(sStartEndBlips.biStart)
				ENDIF
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
				ENDIF
				DISABLE_CELLPHONE(FALSE)
			ENDIF
		ELSE
			IF bMpNeedsCleanedUp = TRUE
				IF bMpModeCleanedUp = FALSE
					// Do menu here.
					DEAL_WITH_TEST_BEING_ACTIVE(structTestMcMissionMenuData)
					IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_FM_MISSION_CONTROLLER_SCRIPT_NAME_HASH()) = 0
					AND GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("FM_Survival_Controller")) = 0
						RESET_TESTING_MC_MISSION_MENU_DATA(structTestMcMissionMenuData)
						SET_FAKE_MULTIPLAYER_MODE(FALSE)
						IF IS_FAKE_MULTIPLAYER_MODE_SET() = FALSE
							IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
							AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
								SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
								SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
							ENDIF
							CLEANUP_LEADERBOARD_CAM()
							REFRESH_MENU(sFMMCmenu)
							bMpModeCleanedUp = TRUE
							// Turn off the leaderboard camera.
							CLEANUP_LEADERBOARD_CAM()
							IF DOES_CAM_EXIST(g_TransitionSessionNonResetVars.sPostMissionCleanupData.ciLeaderBoardCam)
								IF IS_CAM_ACTIVE(g_TransitionSessionNonResetVars.sPostMissionCleanupData.ciLeaderBoardCam)
									IF IS_CAM_RENDERING(g_TransitionSessionNonResetVars.sPostMissionCleanupData.ciLeaderBoardCam)
										RENDER_SCRIPT_CAMS(FALSE, FALSE, 1000, FALSE, TRUE)
										SET_CAM_ACTIVE(g_TransitionSessionNonResetVars.sPostMissionCleanupData.ciLeaderBoardCam, FALSE)
									ENDIF
								ENDIF
								DESTROY_CAM(g_TransitionSessionNonResetVars.sPostMissionCleanupData.ciLeaderBoardCam, TRUE)
							ENDIF
							
							g_TransitionSessionNonResetVars.iRestartBitset = 0
							
							INT iLocTeam
							INT iLocation
							FOR iLocation = 0 TO FMMC_MAX_GO_TO_LOCATIONS-1
								FOR iLocTeam = 0 TO FMMC_MAX_TEAMS-1
						            g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLocation].vloc[iLocTeam]		= vSavedLoc[iLocation][iLocTeam]
									g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLocation].fHeading[iLocTeam]	= fSavedHeading[iLocation][iLocTeam]
								ENDFOR
					            g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLocation].vloc1 = vSavedLoc1[iLocation] 
					            g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLocation].vloc2 = vSavedLoc2[iLocation]				
							ENDFOR
							
							CLEAN_UP_ENTITY_EDITTED_STATUS()
							g_bFMMC_InTestMode_MissionController = FALSE
							
							IF IS_PED_INJURED(PLAYER_PED_ID())
								NETWORK_RESURRECT_LOCAL_PLAYER(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), 0.0, 0, FALSE, TRUE) 
								SET_CURRENT_GAMEMODE(GAMEMODE_CREATOR)
							ENDIF
							
							IF g_Private_MultiplayerCreatorAllowDeathAndRespawning
								FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
								SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)
								SET_PED_DIES_IN_WATER(PLAYER_PED_ID(), FALSE)
								SET_ENTITY_PROOFS(PLAYER_PED_ID(), TRUE, TRUE, TRUE, TRUE, TRUE)
								SET_PLAYER_INVINCIBLE(PLAYER_ID(), TRUE)
								SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)
							ENDIF
							
							PRINTLN("----------------------------------------------------------------------------")
							PRINTLN("--                                                                        --")
							PRINTLN("--                                                                        --")
							PRINTLN("--                                                                        --")
							PRINTLN("--                       MODE CLEANED UP = TRUE                           --")
							PRINTLN("--                                                                        --")
							PRINTLN("--                                                                        --")
							PRINTLN("--                                                                        --")
							PRINTLN("----------------------------------------------------------------------------")
						ENDIF
					ENDIF
				ELSE
					IF NETWORK_IS_GAME_IN_PROGRESS() = FALSE
						SETUP_FMMC_SCRIPT(sFMMCMenu, sHCS, sCurrentVarsStruct, TRUE)
						CLEAR_BIT(sFMMCdata.iBitSet, biTestMissionActive)
						CLEAR_BIT(sFMMCdata.iBitSet, bTestHelpTextDone)
						bMpNeedsCleanedUp = FALSE
						bTestModeControllerScriptStarted = FALSE
						bTextSetUp = FALSE
						bMpModeCleanedUp = FALSE
						SET_CREATOR_AUDIO(TRUE, FALSE)						
						SET_ENTITY_CREATION_STATUS(STAGE_DEAL_WITH_RECREATING_MISSION)
						sCurrentVarsStruct.creationStats.iTimesTestedLoc++
						sCurrentVarsStruct.creationStats.bMadeAChange = FALSE	
						bMultiplayerMapSet = FALSE
						ANIMPOSTFX_STOP_ALL()
					ELSE
						PRINTLN("Mission creator - MAINTAIN_TEST_MISSION_STATE - NETWORK_IS_GAME_IN_PROGRESS() = TRUE")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_PLACED_TEAM_CAM_BLIPS()
	INT i 
	TEXT_LABEL_15 blipName
	FOR i = 0 to FMMC_MAX_TEAMS - 1
		IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[i][0].vPos)
		AND g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[i] > 0
			g_FMMC_STRUCT.sFMMCEndConditions[i].vStartPos = g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[i][0].vPos
			g_FMMC_STRUCT.sFMMCEndConditions[i].fStartHeading = g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[i][0].fHead
		ELIF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.vStartPos)
			g_FMMC_STRUCT.sFMMCEndConditions[i].vStartPos = g_FMMC_STRUCT.vStartPos
		ELSE
			g_FMMC_STRUCT.sFMMCEndConditions[i].vStartPos = <<0,0,0>>
		ENDIF
		IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[i][0].vPos)
		AND NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sFMMCEndConditions[i].vCameraPos)
			IF DOES_BLIP_EXIST(bTeamCamPanBlip[i])
				IF GET_DISTANCE_BETWEEN_COORDS(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[i][0].vPos, g_FMMC_STRUCT.sFMMCEndConditions[i].vCameraPos) > 100
				OR NOT ARE_VECTORS_EQUAL(GET_BLIP_COORDS(bTeamCamPanBlip[i]), g_FMMC_STRUCT.sFMMCEndConditions[i].vCameraPos)
					REMOVE_BLIP(bTeamCamPanBlip[i])
				ENDIF
			ELSE
				IF GET_DISTANCE_BETWEEN_COORDS(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[i][0].vPos, g_FMMC_STRUCT.sFMMCEndConditions[i].vCameraPos) < 100
					CREATE_FMMC_BLIP(bTeamCamPanBlip[i], g_FMMC_STRUCT.sFMMCEndConditions[i].vCameraPos, HUD_COLOUR_WHITE, "FMMC_B_14", 1)
					SET_BLIP_SPRITE(bTeamCamPanBlip[i], RADAR_TRACE_CAMERA)
					SET_BLIP_COLOUR(bTeamCamPanBlip[i], getColourForSpawnBlip(i))
					blipName = "FMMC_B_TC"
					blipName += i
					SET_BLIP_NAME_FROM_TEXT_FILE(bTeamCamPanBlip[i], blipName)
				ENDIF
			ENDIF
		ELSE
			IF DOES_BLIP_EXIST(bTeamCamPanBlip[i])
				REMOVE_BLIP(bTeamCamPanBlip[i])
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

PROC MISSION_CREATOR_INITIALISATION()
	#IF IS_DEBUG_BUILD		
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_useNewUgcSaveSystem")
			g_buseNewUgcSaveSystem = TRUE
		ENDIF
		SET_UP_FMMC_SKIP_NAMES(sFMMCendStage.SkipMenu)
	#ENDIF
	
	sCurrentVarsStruct.creationStats.iStartTimeMS = GET_GAME_TIMER()
	sCurrentVarsStruct.creationStats.iCurrentTimeMS = GET_GAME_TIMER()
	
	REQUEST_IPL("CS1_02_cf_onmission1")
	REQUEST_IPL("CS1_02_cf_onmission2")
	REQUEST_IPL("CS1_02_cf_onmission3")
	REQUEST_IPL("CS1_02_cf_onmission4")
	REQUEST_IPL("chemgrill_grp1")
	REQUEST_IPL("vw_dlc_casino_door")

	#IF IS_DEBUG_BUILD 
	INITIALIZE_ADDITIONAL_DEBUG_AND_WIDGETS()
	CACHE_CREATOR_ENTITY_ROTATION_ACCEL_MODIFIER_PC()
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_CreatorEntityRotationAccelModifierPC")
		g_fCreatorEntityRotationAccelModifierPC = CLAMP(GET_COMMANDLINE_PARAM_FLOAT("sc_CreatorEntityRotationAccelModifierPC"), 0, 1)
	ENDIF	
	#ENDIF
	
	IF NOT IS_LEGACY_MISSION_CREATOR()
	AND NOT IS_3D_EXPLODER_CREATOR()
		// A bunch of variables are set here. When loading existing content, these defaults are overwritten again when we load the cloud loader data slightly later on.
		SET_NEW_2020_MISSION_CREATOR_DEFAULTS(TRUE)
	ENDIF
	
	INT i
	FOR i = 0 TO 31
		SET_BIT(sFMMCmenu.iOptionsMenuBitSet, i)
	ENDFOR
	
	CHANGE_OBJECT_TYPE(sFMMCmenu, sObjStruct, 0)
	
	CLEAN_UP_PAUSE_MENU_MISSION_CREATOR_DATA()
ENDPROC

PROC PROCESS_POST_LOAD_DATA_SETUP()
	LIMIT_ASSOCIATED_RULES_TO_NUMBER_OF_TEAMS()
	PRINTLN("PD START WEAPON DEBUG: weapon pallet is ", g_FMMC_STRUCT_ENTITIES.iWeaponPallet)
	sFMMCmenu.iForcedWeapon = GET_CREATOR_WEAPON_INDEX_FROM_WEAPON_TYPE(GET_DEATHMATCH_CREATOR_RESPAWN_WEAPON(g_FMMC_STRUCT_ENTITIES.iWeaponPallet))
	
	// Might need to functionalise this.
	INT i
	FOR i = 0 TO FMMC_MAX_MC_VAR_SIZE_ENEMY_INVENTORY-1
		sFMMCmenu.iVarWeaponPedOverride[i] =  GET_CREATOR_WEAPON_INDEX_FROM_WEAPON_TYPE(INT_TO_ENUM(WEAPON_TYPE, g_FMMC_STRUCT.sMissionVariation.iVarEnemyInventoryIndex[i]))
	ENDFOR
	
	FOR i = 0 TO FMMC_MAX_MC_VAR_SIZE_VEHICLE_MODEL_SWAP-1
		IF g_FMMC_STRUCT.sMissionVariation.iVarVehicleModelSwapIndex[i] != 0
			MODEL_NAMES mnVeh
			mnVeh = INT_TO_ENUM(MODEL_NAMES, g_FMMC_STRUCT.sMissionVariation.iVarVehicleModelSwapIndex[i])
			sFMMCmenu.iVarVehModelSwapLibrary[i] = GET_CREATOR_VEHICLE_LIBRARY_FROM_MODEL(mnVeh)
			sFMMCmenu.iVarVehModelSwapType[i] = GET_CREATOR_VEHICLE_TYPE_FROM_MODEL(mnVeh)
		ENDIF
	ENDFOR	
	
	sFMMCmenu.fSpawnHeadingOverride = ciSPAWN_POINT_HEADING_OVERRIDE_NOT_SET
	
	SORT_PLANNING_MISSIONS()
	TEMPORARY_FLOATING_PED_FIX()
	
	SET_ENTITY_CREATION_STATUS(STAGE_DEAL_WITH_LOADING)
	IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.vStartPos)
		sCamData.camPos = g_FMMC_STRUCT.vStartPos + <<0,-0.5, 20>>
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF g_bFMMC_EnteredLoadFromPack
		g_FMMC_STRUCT.bMissionIsPublished = FALSE
		PRINTLN("PROCESS_POST_LOAD_DATA_SETUP - Clearing g_FMMC_STRUCT.bMissionIsPublished as we have accessed content through 'Load from Pack' menu")
	ENDIF
	#ENDIF
ENDPROC

PROC PROCESS_CHECK_TO_LOAD_CREATION_STAGE()
	IF g_bFMMC_LoadFromMpSkyMenu = TRUE
		IF LOAD_A_MISSION_INTO_THE_CREATOR(sGetUGC_content, sFMMCendStage, g_sFMMC_LoadedMission)
			PROCESS_POST_LOAD_DATA_SETUP()
		ENDIF
	ELSE
		SET_ENTITY_CREATION_STATUS(STAGE_ENTITY_PLACEMENT_LOAD)
	ENDIF
ENDPROC

PROC PROCESS_PLACEMENT_LOAD()
	IF HAS_ADDITIONAL_TEXT_LOADED(MENU_TEXT_SLOT)
		IF LOAD_MENU_ASSETS()
			SET_MISSION_STARTING_MENU_ITEMS_ACTIVE(sFMMCmenu.iBitActive)
			MENU_INITILISATION()
			IF g_FMMC_STRUCT.iXPReward = 0
				SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSet, ciDEFAULT_XP)
				PRINTLN("THIS MISSION USES ciDEFAULT_XP because XP = 0")
			ELSE
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSet, ciDEFAULT_XP)
					g_FMMC_STRUCT.iXPReward = 0
					PRINTLN("THIS MISSION USES ciDEFAULT_XP because nit is set")
				ELSE
					PRINTLN("THIS MISSION IS NOT USING ciDEFAULT_XP")
				ENDIF
			ENDIF
			
			PRINTSTRING("EveryThingLoaded")PRINTNL()
			PRINTLN("LOADED g_FMMC_STRUCT.iMissionType = ", g_FMMC_STRUCT.iMissionType)
			
			SET_ENTITY_CREATION_STATUS(STAGE_CG_TO_NG_WARNING)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_REMOVING_ENTITIES_FROM_VEHICLES_AND_PEDS()
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_Y)
			
		IF IS_BIT_SET(iHelpBitSet, biRemovePedsButton)
			INT iVeh
			FOR iVeh = 0 TO FMMC_MAX_VEHICLES - 1
				IF DOES_ENTITY_EXIST(sCurrentVarsStruct.vCoronaHitEntity)
				AND DOES_ENTITY_EXIST(sVehStruct.veVehcile[iVeh])
					VEHICLE_INDEX vehTemp
					IF IS_ENTITY_A_VEHICLE(sCurrentVarsStruct.vCoronaHitEntity)
						vehTemp = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(sCurrentVarsStruct.vCoronaHitEntity)
						IF vehTemp = sVehStruct.veVehcile[iVeh]
							REMOVE_ALL_PEDS_FROM_VEHICLE(sPedStruct, sVehStruct, iVeh)
							CLEAR_BIT(iHelpBitSet, biRemovePedsButton)
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
		ELIF IS_BIT_SET(iHelpBitSet, biRemoveVehButton)
			INT iObj
			FOR iObj = 0 TO FMMC_MAX_NUM_OBJECTS - 1
				IF DOES_ENTITY_EXIST(sCurrentVarsStruct.vCoronaHitEntity)
				AND DOES_ENTITY_EXIST(sObjStruct.oiObject[iObj])
					OBJECT_INDEX objTemp
					IF IS_ENTITY_AN_OBJECT(sCurrentVarsStruct.vCoronaHitEntity)
						objTemp = GET_OBJECT_INDEX_FROM_ENTITY_INDEX(sCurrentVarsStruct.vCoronaHitEntity)
						IF objTemp = sObjStruct.oiObject[iObj]
							REMOVE_VEHICLE_FROM_OBJECT(sObjStruct, sVehStruct, iObj)
							CLEAR_BIT(iHelpBitSet, biRemoveVehButton)
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
		ELIF IS_BIT_SET(iHelpBitSet, biRemoveObjectsButton)
			INT iVeh, iObj
			FOR iVeh = 0 TO FMMC_MAX_VEHICLES - 1
				IF DOES_ENTITY_EXIST(sCurrentVarsStruct.vCoronaHitEntity)
				AND DOES_ENTITY_EXIST(sVehStruct.veVehcile[iVeh])
					VEHICLE_INDEX vehTemp
					IF IS_ENTITY_A_VEHICLE(sCurrentVarsStruct.vCoronaHitEntity)
						vehTemp = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(sCurrentVarsStruct.vCoronaHitEntity)
						IF vehTemp = sVehStruct.veVehcile[iVeh]
							REMOVE_ALL_OBJECTS_FROM_VEHICLE(sObjStruct, sVehStruct, iVeh)
							REMOVE_ALL_SPAWN_POINTS_FROM_VEHICLE(sVehStruct, iVeh)
							CLEAR_BIT(iHelpBitSet, biRemoveObjectsButton)
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
			FOR iObj = 0 TO FMMC_MAX_NUM_OBJECTS - 1
				IF DOES_ENTITY_EXIST(sCurrentVarsStruct.vCoronaHitEntity)
				AND DOES_ENTITY_EXIST(sObjStruct.oiObject[iObj])
					OBJECT_INDEX objTemp
					IF IS_ENTITY_AN_OBJECT(sCurrentVarsStruct.vCoronaHitEntity)
						objTemp = GET_OBJECT_INDEX_FROM_ENTITY_INDEX(sCurrentVarsStruct.vCoronaHitEntity)
						IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectAttachmentPed > -1
							IF objTemp = sObjStruct.oiObject[iObj]
								REMOVE_ALL_OBJECTS_FROM_PED(sObjStruct, sPedStruct, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectAttachmentPed)
								CLEAR_BIT(iHelpBitSet, biRemoveObjectsButton)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
		ENDIF
		
	ENDIF
ENDPROC

PROC PROCESS_CAMERA_PLACEMENT()
	IF PPS.iStage != WAIT_PREVIEW_PHOTO
	AND PPS.iStage != CLEANUP_PREVIEW_PHOTO
	AND sFMMCendStage.sTakePhotoVars.iTakePhotoStage != ciFMMC_TAKE_PHOTO_STAGE_OPEN_SHUT
		PPS.iStage = CLEANUP_PREVIEW_PHOTO
		PPS.iPreviewPhotoDelayCleanup = 0
	ENDIF
	IF sFMMCmenu.sActiveMenu = eFmmc_PHOTO_CAM_BASE								
		REMOVE_DECALS_FOR_PHOTOS(sFMMCendStage.sTakePhotoVars.iTakePhotoStage, sStartEndBlips, sPedStruct, sTeamSpawnStruct, sWepStruct, sObjStruct, sHCS.hcStartCoronaColour)
	ENDIF
	IF TAKE_PHOTO_FOR_FMMC(sFMMCendStage.sTakePhotoVars, SF_Movie_Gallery_Shutter_Index, sFMMCmenu.sActiveMenu = eFmmc_PHOTO_CAM_BASE)
		IF sFMMCmenu.sActiveMenu = eFmmc_PAN_CAM_BASE
			g_FMMC_STRUCT.vCameraPanPos = GET_CAM_COORD(GET_RENDERING_CAM())
			g_FMMC_STRUCT.vCameraPanRot = GET_CAM_ROT(GET_RENDERING_CAM())	
			VECTOR vRot
			vRot = GET_CAM_ROT(GET_RENDERING_CAM())
			g_FMMC_STRUCT.fCameraPanHead = vRot.z
			PRINTLN("PAN CAM POSITIONED Rot - ", g_FMMC_STRUCT.vCameraPanRot, " Vector - ", g_FMMC_STRUCT.vCameraPanPos)
		ELIF sFMMCmenu.sActiveMenu = eFmmc_OUT_CAM_BASE
			g_FMMC_STRUCT.vCameraOutroPos = GET_CAM_COORD(GET_RENDERING_CAM())
			g_FMMC_STRUCT.vCameraOutroRot = GET_CAM_ROT(GET_RENDERING_CAM())	
			PRINTLN("OUTRO CAM POSITIONED Rot - ", g_FMMC_STRUCT.vCameraOutroRot, " Vector - ", g_FMMC_STRUCT.vCameraOutroPos)
		ELIF sFMMCmenu.sActiveMenu = eFmmc_DM_TEAM_CAMERAS
			g_FMMC_STRUCT.sFMMCEndConditions[sFMMCmenu.iSelectedCameraTeam].vCameraPos = GET_CAM_COORD(GET_RENDERING_CAM())
			g_FMMC_STRUCT.sFMMCEndConditions[sFMMCmenu.iSelectedCameraTeam].vCameraRot = GET_CAM_ROT(GET_RENDERING_CAM())
			PRINTLN("Team = ", sFMMCmenu.iSelectedCameraTeam," PAN CAM POSITIONED Rot - ", g_FMMC_STRUCT.sFMMCEndConditions[sFMMCmenu.iSelectedCameraTeam].vCameraRot, " Vector - ", g_FMMC_STRUCT.sFMMCEndConditions[sFMMCmenu.iSelectedCameraTeam].vCameraPos)
		ELIF sFMMCmenu.sActiveMenu = efMMC_TRIP_SKIP_CAM
			IF sFMMCmenu.iSelectedTeam < g_FMMC_STRUCT.iMaxNumberOfTeams
				g_FMMC_STRUCT.vTripSkipCamPos[sFMMCmenu.iSelectedTeam] = GET_CAM_COORD(GET_RENDERING_CAM())
				g_FMMC_STRUCT.vTripSkipCamRot[sFMMCmenu.iSelectedTeam] = GET_CAM_ROT(GET_RENDERING_CAM())
				g_FMMC_STRUCT.fTripSkipCamFOV[sFMMCmenu.iSelectedTeam] = GET_CAM_FOV(GET_RENDERING_CAM())
			ELSE
				g_FMMC_STRUCT.vAutoTripSkipCamPos = GET_CAM_COORD(GET_RENDERING_CAM())
				g_FMMC_STRUCT.vAutoTripSkipCamRot = GET_CAM_ROT(GET_RENDERING_CAM())
				g_FMMC_STRUCT.fAutoTripSkipCamFOV = GET_CAM_FOV(GET_RENDERING_CAM())
			ENDIF
			REFRESH_MENU(sFMMCmenu)
			
		ELIF sFMMCmenu.sActiveMenu = eFmmc_SET_ELEVATOR_CAM	
			g_FMMC_STRUCT.sElevators[sFMMCMenu.iSelectedElevator].sCamera.vPosition = GET_CAM_COORD(GET_RENDERING_CAM())
			g_FMMC_STRUCT.sElevators[sFMMCMenu.iSelectedElevator].sCamera.vRotation = GET_CAM_ROT(GET_RENDERING_CAM())	
			g_FMMC_STRUCT.sElevators[sFMMCMenu.iSelectedElevator].sCamera.fFOV = GET_CAM_FOV(GET_RENDERING_CAM())
			SET_ACTIVE_MENU(sFMMCMenu, eFmmc_ELEVATORS)
			REFRESH_MENU(sFMMCmenu)
		ELIF sFMMCmenu.sActiveMenu = efMMC_CUTSCENE_SET_CAM_1
			g_FMMC_STRUCT.sCurrentSceneData.vCamStartPos[sFMMCmenu.iCurrentShot] = GET_CAM_COORD(GET_RENDERING_CAM())
			g_FMMC_STRUCT.sCurrentSceneData.vCamStartRot[sFMMCmenu.iCurrentShot] = GET_CAM_ROT(GET_RENDERING_CAM())	
			g_FMMC_STRUCT.sCurrentSceneData.fStartCamFOV[sFMMCmenu.iCurrentShot] = GET_CAM_FOV(GET_RENDERING_CAM())
			IF sFMMCmenu.sActiveMenu =	efMMC_CUTSCENE_SET_CAM_1
				IF IS_VECTOR_ZERO(g_FMMC_STRUCT.sCurrentSceneData.vCamEndPos[sFMMCmenu.iCurrentShot])
					SET_ACTIVE_MENU(sFMMCMenu, efMMC_CUTSCENE_SET_CAM_2)
					REFRESH_MENU(sFMMCmenu)
				ELSE
					SET_ACTIVE_MENU(sFMMCMenu, efMMC_CUTSCENE_ADD_SHOT)
					REFRESH_MENU(sFMMCmenu)
				ENDIF
			ENDIF
		ELIF sFMMCmenu.sActiveMenu = efMMC_CUTSCENE_SET_CAM_2
			g_FMMC_STRUCT.sCurrentSceneData.vCamEndPos[sFMMCmenu.iCurrentShot] = GET_CAM_COORD(GET_RENDERING_CAM())
			g_FMMC_STRUCT.sCurrentSceneData.vCamEndRot[sFMMCmenu.iCurrentShot] = GET_CAM_ROT(GET_RENDERING_CAM())	
			g_FMMC_STRUCT.sCurrentSceneData.fEndCamFOV[sFMMCmenu.iCurrentShot] = GET_CAM_FOV(GET_RENDERING_CAM())
			SET_ACTIVE_MENU(sFMMCMenu, efMMC_CUTSCENE_ADD_SHOT)
			REFRESH_MENU(sFMMCmenu)
		ELIF sFMMCmenu.sActiveMenu = efMMC_CUTSCENE_SET_CAM_3
			VECTOR vCamRot, vEntRot
						
			g_FMMC_STRUCT.sCurrentSceneData.vOffsetPos[sFMMCmenu.iCurrentShot] = sFMMCmenu.vCameraOffset
			
			VECTOR vOffsetStart = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(sVehStruct.veVehcile[g_FMMC_STRUCT.sCurrentSceneData.iVehicleToAttachTo[sFMMCMenu.iCurrentShot]]), GET_ENTITY_HEADING(sVehStruct.veVehcile[g_FMMC_STRUCT.sCurrentSceneData.iVehicleToAttachTo[sFMMCMenu.iCurrentShot]]), sFMMCmenu.vCameraOffset)
						
			vCamRot = GET_CAM_ROT( GET_RENDERING_CAM() )
			vEntRot = GET_ENTITY_ROTATION(sVehStruct.veVehcile[iSwitchCam])
			g_FMMC_STRUCT.sCurrentSceneData.vOffsetRot[sFMMCmenu.iCurrentShot].x = vCamRot.x - vEntRot.x
			g_FMMC_STRUCT.sCurrentSceneData.vOffsetRot[sFMMCmenu.iCurrentShot].y = vCamRot.y - vEntRot.y
			g_FMMC_STRUCT.sCurrentSceneData.vOffsetRot[sFMMCmenu.iCurrentShot].z = vCamRot.z - vEntRot.z

			g_FMMC_STRUCT.sCurrentSceneData.fStartCamFOV[sFMMCmenu.iCurrentShot] = GET_CAM_FOV(GET_RENDERING_CAM())
			
			g_FMMC_STRUCT.sCurrentSceneData.vLookAtPos[sFMMCmenu.iCurrentShot].x = g_FMMC_STRUCT.fLookAtPosX[sFMMCmenu.iCurrentShot]
			g_FMMC_STRUCT.sCurrentSceneData.vLookAtPos[sFMMCmenu.iCurrentShot].y = g_FMMC_STRUCT.fLookAtPosY[sFMMCmenu.iCurrentShot]
			g_FMMC_STRUCT.sCurrentSceneData.vLookAtPos[sFMMCmenu.iCurrentShot].z = g_FMMC_STRUCT.fLookAtPosZ[sFMMCmenu.iCurrentShot]

			g_FMMC_STRUCT.sCurrentSceneData.iVehicleToAttachTo[sFMMCmenu.iCurrentShot] = iSwitchCam
			
			PRINTLN("[CamOffset] - Assigning Value - vOffsetWorld : ", vOffsetStart)
			PRINTLN("[CamOffset] - Assigning Value - vOffsetPos : ", g_FMMC_STRUCT.sCurrentSceneData.vOffsetPos[sFMMCmenu.iCurrentShot])
			PRINTLN("[CamOffset] - Assigning Value - vOffsetRot : ", g_FMMC_STRUCT.sCurrentSceneData.vOffsetRot[sFMMCmenu.iCurrentShot])
			PRINTLN("[CamOffset] - Assigning Value - iVehicleToAttachTo : ", g_FMMC_STRUCT.sCurrentSceneData.iVehicleToAttachTo[sFMMCmenu.iCurrentShot])
			PRINTLN("[CamOffset] - Assigning Value - vLookAtPos : ", g_FMMC_STRUCT.sCurrentSceneData.vLookAtPos[sFMMCmenu.iCurrentShot])
			
			SET_ACTIVE_MENU(sFMMCMenu, efMMC_CUTSCENE_ADD_SHOT)
			REFRESH_MENU(sFMMCmenu)
		ENDIF
		sFMMCendStage.bMajorEditOnLoadedMission = TRUE
		REFRESH_MENU(sFMMCmenu)
		sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
	ENDIF
ENDPROC

FUNC BOOL IS_OKAY_TO_FORCE_EDIT()
	
	IF NOT sFMMCmenu.bRuleMenuActive
		RETURN TRUE
	ENDIF
	
	IF IS_THIS_CURRENT_RHM_MENU(eRHM_CameraShaking)
	AND IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_DLC_2_2022)
		RETURN TRUE
	ENDIF

	RETURN FALSE

ENDFUNC

PROC PROCESS_DEFAULT_MENU_STATE()
	
	IF sFMMCmenu.sActiveMenu != eFmmc_TOP_MENU
	AND HAS_CREATOR_SEARCH_BEEN_TRIGGERED()
		sCurrentVarsStruct.iMenuState = MENU_STATE_SEARCH
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF IS_OKAY_TO_FORCE_EDIT()
		PROCESS_FORCE_EDIT()
	ENDIF
	#ENDIF
	
	SET_FAR_RIGHT_TITLE_POSITION_HUD_THIS_FRAME()
	
	INT iFailBitSet		
	iFailBitSet = 0
	SET_FAIL_BIT_SET(iFailBitSet)
	
	IF iFailBitSet = 0	
	AND NOT IS_BIT_SET_ALERT(sFMMCmenu)
	AND NOT ARE_ANY_RULES_EMPTY(sCurrentVarsStruct.iSaveFailTeam, sCurrentVarsStruct.iSaveFailRule)
		SET_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_PUBLISH)
	ELSE
		CLEAR_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_PUBLISH)
	ENDIF
	
	IF g_FMMC_STRUCT.bMissionIsPublished
		CLEAR_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_SAVE)
	ENDIF
	
	IF IS_MENU_A_RESET_MENU(sFMMCmenu.sActiveMenu)
		SET_AREA_GOOD_FOR_PLACEMENT(sCurrentVarsStruct, FALSE)
		sCurrentVarsStruct.bFirstShapeTestCheckDone = FALSE
		sCurrentVarsStruct.sCoronaShapeTest.bDoStartShapeTest = FALSE
		sCurrentVarsStruct.sCoronaShapeTest.stiShapeTest  = NULL		
	ENDIF
	
	IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
	
		IF SHOULD_MENU_BE_DRAWN(sFMMCData, sFMMCMenu, sCurrentVarsStruct, menuScrollController, 1, sFMMCmenu.sActiveMenu = eFmmc_TOP_MENU, bShowMenu)
		AND NOT FMMC_PROCESS_ENTITY_LIST_MENU(sFMMCMenu, sFMMCData, sCurrentVarsStruct)	
			
			FMMC_DO_MENU_ACTIONS(	RETURN_CIRCLE_MENU_SELECTIONS(),
									sCurrentVarsStruct, sFMMCmenu, 
									sFMMCData, sFMMCendStage, sHCS)
									
			IF bMoveCameraActionPressed
				DEAL_WITH_CAMERA_SWITCH_SELECTION(TRUE, 300, -2)
				bMoveCameraActionPressed = FALSE
			ENDIF
			
			PROCESS_REMOVING_ENTITIES_FROM_VEHICLES_AND_PEDS()
			
			IF sFMMCmenu.iEntityCreation != CREATION_TYPE_CUTSCENE_CAM
			AND sFMMCmenu.iEntityCreation != CREATION_TYPE_TRIP_SKIP_CAM
			AND sFMMCmenu.iEntityCreation != CREATION_TYPE_ELEVATOR_CAM
				IF DOES_ENTITY_EXIST(objCutscenePlacement)
					DELETE_OBJECT(objCutscenePlacement)
				ENDIF
				IF DOES_CAM_EXIST(camCutscenePlacement)
					DESTROY_CAM(camCutscenePlacement)
				ENDIF
				IF iCutsceneCamStage != CREATOR_CUTSCENE_CAM_INIT
					iCutsceneCamStage = CREATOR_CUTSCENE_CAM_INIT
					DISPLAY_RADAR(TRUE)
				ENDIF
			ENDIF
			
			SWITCH sFMMCmenu.iEntityCreation
				CASE -1
					sCurrentVarsStruct.iHoverEntityType  = -1
					IF sFMMCmenu.sActiveMenu != eFmmc_TOP_MENU
					AND sFMMCmenu.sActiveMenu != eFmmc_MAIN_OPTIONS_BASE
						UPDATE_THE_CORONA_DISC_VALUES(sCurrentVarsStruct, NULL, TRUE, TRUE, fDEFAULT_PLACEMENT_DISC_HEIGHT)
					ELSE
						UPDATE_THE_CORONA_DISC_VALUES(sCurrentVarsStruct, NULL, TRUE, TRUE, fDEFAULT_PLACEMENT_DISC_HEIGHT, FALSE)
					ENDIF
					IF g_FMMC_STRUCT_ENTITIES.iNumberOfZones	< 10
						IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedZones[g_FMMC_STRUCT_ENTITIES.iNumberOfZones].vPos[0])
							IF IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedZones[g_FMMC_STRUCT_ENTITIES.iNumberOfZones].vPos[1])
								PRINTLN("PD ZONES SET .vPos[0] to zero 1")
								g_FMMC_STRUCT_ENTITIES.sPlacedZones[g_FMMC_STRUCT_ENTITIES.iNumberOfZones].vPos[0] = <<0,0,0>>
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE CREATION_TYPE_PAN_CAM 									
					BOOL bResetCamMenu
					bResetCamMenu = FALSE
					MAINTAIN_PLACING_INTRO_CAMERAS(sFMMCmenu, bCameraTriggerBlip, bResetCamMenu, bInitialIntroCamSetup)
					UPDATE_THE_CORONA_DISC_VALUES(sCurrentVarsStruct, NULL, TRUE, TRUE, fDEFAULT_PLACEMENT_DISC_HEIGHT, FALSE)
					IF bResetCamMenu
						REFRESH_MENU(sFMMCmenu)
					ENDIF
				BREAK
					
				CASE CREATION_TYPE_TRIP_SKIPS
					
				BREAK
					
				CASE CREATION_TYPE_CUTSCENE_CAM
					MAINTAIN_PLACING_CUTSCENE_CAMERAS(sVehStruct, sFMMCmenu, camCutscenePlacement, objCutscenePlacement, iCutsceneCamStage, iCamMovementSpeed, iCamTurnRate, IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround))
					UPDATE_THE_CORONA_DISC_VALUES(sCurrentVarsStruct, NULL, TRUE, TRUE, fDEFAULT_PLACEMENT_DISC_HEIGHT, FALSE)
				BREAK
					
				CASE CREATION_TYPE_TRIP_SKIP_CAM
					MAINTAIN_PLACING_TRIP_SKIP_CAMERA(sFMMCmenu, camCutscenePlacement, objCutscenePlacement, iCutsceneCamStage, iCamMovementSpeed, iCamTurnRate)
					UPDATE_THE_CORONA_DISC_VALUES(sCurrentVarsStruct, NULL, TRUE, TRUE, fDEFAULT_PLACEMENT_DISC_HEIGHT, FALSE)
				BREAK
					
				CASE CREATION_TYPE_ELEVATOR_CAM
					MAINTAIN_PLACING_ELEVATOR_CAMERA(sFMMCmenu, camCutscenePlacement, objCutscenePlacement, iCutsceneCamStage, iCamMovementSpeed, iCamTurnRate, IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround))
					UPDATE_THE_CORONA_DISC_VALUES(sCurrentVarsStruct, NULL, TRUE, TRUE, fDEFAULT_PLACEMENT_DISC_HEIGHT, FALSE)
				BREAK
				
				CASE CREATION_TYPE_TRIGGER
					DEAL_WITH_SELECTING_TRIGGER_LOCATION()
				BREAK
					
				CASE CREATION_TYPE_PEDS
					DO_PED_CREATION(sPedStruct, sVehStruct, sCurrentVarsStruct, sHCS, sFMMCmenu, sFMMCdata, sFMMCendStage, sTrainCreationStruct)
					
					IF sFMMCMenu.iSelectedEntity = -1
					AND g_FMMC_STRUCT_ENTITIES.iNumberOfPeds = FMMC_MAX_PEDS
						WHILE sFMMCmenu.sActiveMenu != eFmmc_ENEMY_BASE
							GO_BACK_TO_MENU(sFMMCmenu)
						ENDWHILE
					ENDIF
				BREAK
				
				CASE CREATION_TYPE_VEHICLES
					DO_VEHICLE_CREATION(sVehStruct, sPedStruct, sObjStruct, sTrainCreationStruct, sCurrentVarsStruct, sHCS, sFMMCmenu, sFMMCdata, sFMMCendStage, FMMC_MAX_VEHICLES, iLocalBitSet)
				BREAK
				
				CASE CREATION_TYPE_TRAINS
					DO_FMMC_TRAIN_CREATION(sTrainCreationStruct, sCurrentVarsStruct, sFMMCmenu)
				BREAK
					
				CASE CREATION_TYPE_WEAPONS 
					DO_WEAPON_CREATION(sWepStruct, sObjStruct, sCurrentVarsStruct, sHCS, sFMMCmenu, sFMMCdata,sFMMCendStage, FMMC_MAX_WEAPONS, TRUE, iLocalBitSet)
				BREAK
					
				CASE CREATION_TYPE_OBJECTS 
					DO_OBJECT_CREATION(sObjStruct, sVehStruct, sWepStruct, sCurrentVarsStruct, sHCS, sFMMCmenu, sFMMCdata, sFMMCendStage, sTrainCreationStruct)
				BREAK
					
				CASE CREATION_TYPE_PROPS
					DO_PROP_CREATION(sPropStruct, sVehStruct, sObjStruct, sCurrentVarsStruct, sHCS, sFMMCmenu, sFMMCdata,sFMMCendStage, sCamData, sPedStruct, sTrainCreationStruct, GET_FMMC_MAX_NUM_PROPS(), iLocalBitSet)
				BREAK
					
				CASE CREATION_TYPE_DYNOPROPS 
					DO_DYNOPROP_CREATION(sDynoPropStruct, sPropStruct, sVehStruct, sCurrentVarsStruct, sHCS, sFMMCmenu, sFMMCdata,sFMMCendStage, sCamData, sPedStruct, sTrainCreationStruct, FMMC_MAX_NUM_DYNOPROPS, iLocalBitSet)
				BREAK
					
				CASE CREATION_TYPE_INTERACTABLE
					DO_INTERACTABLE_CREATION(sInteractableCreationStruct, sCurrentVarsStruct, sHCS, sFMMCmenu, sFMMCData, sFMMCendStage, sTrainCreationStruct)
				BREAK
					
				CASE CREATION_TYPE_STUNT_JUMPS
					DO_STUNTJUMP_CREATION(sStuntJumpStruct, sCurrentVarsStruct, sFMMCmenu, sFMMCdata, sFMMCendStage, fStuntCamZ)
				BREAK
					
				CASE CREATION_TYPE_DIALOGUE_TRIGGER
					DO_DIALOGUE_TRIGGER(g_CreatorsSelDetails, sCurrentVarsStruct, sFMMCmenu, sFMMCdata, sFMMCendStage)
				BREAK
					
				CASE CREATION_TYPE_DUMMY_BLIPS
					DO_DUMMY_BLIP_CREATION(g_CreatorsSelDetails, sCurrentVarsStruct, sFMMCmenu, sFMMCdata, sFMMCendStage)
				BREAK				
					
				CASE CREATION_TYPE_GOTO_LOC
					DO_LOCATION_CREATION(g_CreatorsSelDetails, sLocStruct, sCurrentVarsStruct, sFMMCmenu, sFMMCdata, sFMMCendStage, FMMC_MAX_GO_TO_LOCATIONS)
				BREAK
					
				CASE CREATION_TYPE_PLAYER_RULE
					SET_UP_PLAYER_RULE(sPlayerKills)
				BREAK
					
				CASE CREATION_TYPE_FMMC_ZONE
					DO_ZONE_CREATION(sCurrentVarsStruct, sFMMCmenu, sFMMCendStage)
					DRAW_GENERIC_BIG_DOUBLE_NUMBER(g_FMMC_STRUCT_ENTITIES.iNumberOfZones,  FMMC_MAX_NUM_ZONES,	"FMMCCMENU_18", -1, HUD_COLOUR_GREEN)
				BREAK
				
				CASE CREATION_TYPE_OVERTIME_ZONE
					IF sFMMCmenu.sPlacedOvertimeZones.fRadiusWidth > 0
						DO_OVERTIME_ZONE_CREATION(sCurrentVarsStruct, sFMMCmenu, sCurrentVarsStruct.iZoneCreationStage, sFMMCendStage)
						DRAW_GENERIC_BIG_DOUBLE_NUMBER(g_FMMC_STRUCT.iNumberOfOvertimeZones,  ci_OVERTIME_ZONE_MAX,	"FMMCCMENU_18", -1, HUD_COLOUR_GREEN)
					ENDIF
				BREAK
				
				CASE CREATION_TYPE_DOOR
					DO_DOOR_SELECTION(sCurrentVarsStruct, sFMMCmenu, sFMMCdata)
				BREAK
				
				CASE CREATION_TYPE_COVER
					DO_COVER_POINT_CREATION(sCoverStruct, sCurrentVarsStruct, sFMMCmenu, sFMMCendStage)												
				BREAK
				
				CASE CREATION_TYPE_TEAM_SPAWN_LOCATION 									
					IF IS_BIT_SET(g_FMMC_STRUCT.iBS_MissionTeamRelations, FMMC_TEAM_SPAWN_AS_GRID)
						DO_GRID_SPAWN_POINT_CREATION(sTeamSpawnStruct, sCurrentVarsStruct, sHCS, sFMMCmenu, sFMMCdata, sFMMCendStage, iMaxSpawnsPerTeam, IS_BIT_SET(iLocalBitSet, biCanEditEntityInCorona))
					ELIF IS_BIT_SET(sFMMCmenu.iMiscBitSet, bsUseArialSpawnGrid)
						DO_TEAM_GRID_SPAWN_POINT_CREATION(sTeamSpawnStruct[sFMMCMenu.iSelectedTeam], sCurrentVarsStruct, sHCS, sFMMCmenu, sFMMCdata, sFMMCendStage, iMaxSpawnsPerTeam, IS_BIT_SET(iLocalBitSet, biCanEditEntityInCorona))
					ELSE
						DO_TEAM_SPAWN_POINT_CREATION(sTeamSpawnStruct[sFMMCMenu.iSelectedTeam], sCurrentVarsStruct, sHCS, sFMMCmenu, sFMMCdata, sFMMCendStage,sVehStruct, iMaxSpawnsPerTeam, IS_BIT_SET(iLocalBitSet, biCanEditEntityInCorona), TRUE)
					ENDIF
				BREAK
				
				CASE CREATION_TYPE_RESTART_POINTS
					DO_RESTART_POINT_CREATION(sCurrentVarsStruct, sHCS, sFMMCmenu, sFMMCmenu.iSelectedTeam)
				BREAK
				
				CASE CREATION_TYPE_QRC_SPAWN_POINTS
					DO_QRC_SPAWN_POINT_CREATION(sCurrentVarsStruct, sHCS, sFMMCmenu)
				BREAK
				
				CASE CREATION_TYPE_PERSONAL_VEHICLE_POSITIONS
					DO_PERSONAL_VEHICLE_POSITION_CREATION(sCurrentVarsStruct, sHCS, sFMMCmenu, sFMMCmenu.iSelectedTeam)
				BREAK
				
				CASE CREATION_TYPE_WORLD_PROPS
					DO_WORLD_PROP_SELECTION(sFMMCmenu, sCurrentVarsStruct)
				BREAK
				
				CASE CREATION_TYPE_ROCKET_POSITIONS
					DO_ROCKET_POSITION_CREATION(g_CreatorsSelDetails, sCurrentVarsStruct, sFMMCmenu, sFMMCendStage)
				BREAK	
				
				CASE CREATION_TYPE_DEV_ASSIST
					DO_DEV_ASSIST_CREATION(sDevAssistCreationStruct, sCurrentVarsStruct, sHCs, sFMMCmenu, sFMMCData, sFMMCendStage)
				BREAK
				
				CASE CREATION_TYPE_UNIT
					DO_UNIT_CREATION(sCurrentVarsStruct, sUnitStruct, sFMMCMenu, sFMMCdata, sHCS, iLocalBitset)
				BREAK
			ENDSWITCH
			
			SET_INSTRUCTIONAL_BUTTONS_UNDER_HUD_THIS_FRAME(2)
		ELSE
			//Reset stuff. 
			sPedStruct.iSwitchingINT = CREATION_STAGE_WAIT
			sWepStruct.iSwitchingINT = CREATION_STAGE_WAIT
			sVehStruct.iSwitchingINT = CREATION_STAGE_WAIT	
			sObjStruct.iSwitchingINT = CREATION_STAGE_WAIT	
			sDevAssistCreationStruct.iCreationState = CREATION_STAGE_WAIT	
			SET_CURRENT_GENERIC_CREATION_STAGE(CREATION_STAGE_WAIT)		
			sStuntJumpStruct.iSwitchingINT = CREATION_STAGE_WAIT								
			
			IF DOES_ENTITY_EXIST(sVehStruct.viCoronaVeh)
				DELETE_VEHICLE(sVehStruct.viCoronaVeh)
			ENDIF
			IF DOES_ENTITY_EXIST(sWepStruct.viCoronaWep)
				DELETE_OBJECT(sWepStruct.viCoronaWep)
			ENDIF
			IF DOES_PICKUP_EXIST(sWepStruct.piTempPickupForModel)
				REMOVE_PICKUP(sWepStruct.piTempPickupForModel)
			ENDIF
			IF DOES_ENTITY_EXIST(sPropStruct.viCoronaObj)
				DELETE_OBJECT(sPropStruct.viCoronaObj)
			ENDIF
			DELETE_CURRENT_PROP_TEMPLATE_OBJECTS(sPropStruct)
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())	
			BOOL bReloadMenu
			bReloadMenu = FALSE
			IF NOT IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
				DEAL_WITH_SKY_CAM_SWITCH(sFMMCdata,bSwitchingCam,vSwitchVec,sCamData, bReloadMenu)
			ELSE
				DEAL_WITH_PLAYER_WARP_TO_ITEM(sFMMCdata,bSwitchingCam,vSwitchVec,fSwitchHeading, bReloadMenu)
			ENDIF
			IF bReloadMenu
				REFRESH_MENU(sFMMCmenu)
			ENDIF
		ENDIF
	ENDIF
	
	CHECK_FOR_MENU_SET_UP(sCurrentVarsStruct, sPedStruct, sVehStruct, sWepStruct, sObjStruct, sFMMCmenu, g_CreatorsSelDetails.iSelectedTeam)
	IF sCurrentVarsStruct.bResetUpHelp
		RESET_UP_HELP()
		sCurrentVarsStruct.bResetUpHelp  = FALSE
	ENDIF
	
	ADD_NEW_ENTITY_RULE(sFMMCmenu.iEntityCreation)
ENDPROC

PROC PROCESS_ENTERING_MENU_STATE_VECTOR_DATA()
	IF IS_BIT_SET(sFMMCmenu.iFMMCMenuBitset, ciFMMCmenuBS_IsOnScreenVectorKeyboardValid)
		SET_BIT(iHelpBitset, biVectorInputTextButton)
	ELSE
		CLEAR_BIT(iHelpBitSet, biVectorInputTextButton)
	ENDIF
	IF IS_BIT_SET(sFMMCmenu.iFMMCMenuBitset, ciFMMCmenuBS_JumpToVectorKeyboard)
		sCurrentVarsStruct.iMenuState = MENU_STATE_ENTER_SIMPLE_VECTOR			
		CLEAR_BIT(sFMMCmenu.iFMMCMenuBitset, ciFMMCmenuBS_JumpToVectorKeyboard)
	ENDIF
ENDPROC

PROC PROCESS_ENTERING_MENU_STATE_VECTOR()
	INT iSelection = GET_CREATOR_MENU_SELECTION(sFmmcmenu) 
	BOOL bPressed = IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_Y)
	BOOL bValid	= FALSE
	
	UNUSED_PARAMETER(iSelection)
	
	// Special Cases where we cannot call ADD_CREATOR_VECTOR_INPUT_FROM_ONSCREEN_KEYBOARD, we can enter the selecitoon and menu as shown in the example commented out below.
	/*SWITCH sFMMCmenu.sActiveMenu
		CASE eFmmc_END_OF_MISSION_END_CUTSCENE_MENU			
			IF iSelection = OPTION_MISSION_END_CUTSCENE_POSITION
				bValid = TRUE				
				
				IF NOT IS_VECTOR_ZERO(sFMMCmenu.vOnScreenResult)
					g_fmmc_struct.sEndMocapSceneData.vCutscenePosition = sFMMCmenu.vOnScreenResult
					sFMMCmenu.vOnScreenResult = <<0.0, 0.0, 0.0>>
					bValid = FALSE
					REFRESH_MENU(sFMMCmenu)
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH*/
	
	IF bValid
		SET_BIT(sFMMCmenu.iFMMCMenuBitset, ciFMMCmenuBS_IsOnScreenVectorKeyboardValid)
		IF bPressed	
			sCurrentVarsStruct.iMenuState = MENU_STATE_ENTER_SIMPLE_VECTOR
		ENDIF
	ENDIF

ENDPROC

PROC PROCESS_ENTERING_MENU_STATE_ON_SCREEN_POPUP_WINDOW_DATA()
	IF IS_BIT_SET(sFMMCmenu.iFMMCMenuBitset, ciFMMCmenuBS_IsOnScreenPopupValid)
		SET_BIT(iHelpBitset, biEditDataInputTextButton)
	ELSE
		CLEAR_BIT(iHelpBitSet, biEditDataInputTextButton)
	ENDIF
	
	IF IS_BIT_SET(sFMMCmenu.iFMMCMenuBitset, ciFMMCmenuBS_JumpToOnScreenPopup)
		sCurrentVarsStruct.iMenuState = MENU_STATE_ENTER_ON_SCREEN_POPUP_WINDOW			
		CLEAR_BIT(sFMMCmenu.iFMMCMenuBitset, ciFMMCmenuBS_JumpToOnScreenPopup)
	ENDIF
ENDPROC

PROC PROCESS_ENTERING_MENU_STATE_ON_SCREEN_POPUP_WINDOW()
	INT iSelection = GET_CREATOR_MENU_SELECTION(sFmmcmenu) 
	BOOL bPressed = IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_Y)
	BOOL bValid	= FALSE
	
	UNUSED_PARAMETER(iSelection)
	
	// Special Cases where we cannot call ADD_CREATOR_VECTOR_INPUT_FROM_ONSCREEN_KEYBOARD, we can enter the selecitoon and menu as shown in the example commented out below.
	/*SWITCH sFMMCmenu.sActiveMenu
		CASE eFmmc_END_OF_MISSION_END_CUTSCENE_MENU			
			IF iSelection = OPTION_MISSION_END_CUTSCENE_POSITION
				bValid = TRUE				
				
				IF NOT IS_VECTOR_ZERO(sFMMCmenu.vOnScreenResult)
					g_fmmc_struct.sEndMocapSceneData.vCutscenePosition = sFMMCmenu.vOnScreenResult
					sFMMCmenu.vOnScreenResult = <<0.0, 0.0, 0.0>>
					bValid = FALSE
					REFRESH_MENU(sFMMCmenu)
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH*/
	
	IF bValid
		SET_BIT(sFMMCmenu.iFMMCMenuBitset, ciFMMCmenuBS_IsOnScreenPopupValid)
		IF bPressed	
			sCurrentVarsStruct.iMenuState = MENU_STATE_ENTER_ON_SCREEN_POPUP_WINDOW
		ENDIF
	ENDIF

ENDPROC

PROC PROCESS_POST_MAINTAIN_ENTITIES()
	IF IS_BIT_SET(sFMMCmenu.iFMMCMenuBitset, ciFMMCmenuBS_RefreshPlacedTrainAttachments)
		CLEAR_BIT(sFMMCmenu.iFMMCMenuBitset, ciFMMCmenuBS_RefreshPlacedTrainAttachments)
		PRINTLN("[Creator][Train] PROCESS_POST_MAINTAIN_ENTITIES | Clearing ciFMMCmenuBS_RefreshPlacedTrainAttachments")
	ENDIF
ENDPROC

PROC PROCESS_MAIN_STATE()
				
	DEAL_WITH_HITTING_MAX_ENTITES(sCurrentVarsStruct, sFMMCmenu)
		
	//Deal with the camera	
	IF NOT IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
		MAINTAIN_FMMC_CAMERA(sFMMCdata, sCamData, sCurrentVarsStruct.iMenuState)
	ENDIF
						
	IF SHOULD_PROCESS_CURSOR()
	
		VECTOR vDummyRot
		IF DOES_CAM_EXIST(sCamData.cam)
			vDummyRot = GET_CAM_ROT(sCamData.cam)
		ELSE
			vDummyRot = <<0,0,0>>
		ENDIF
		
		BOOL bForceOnGroundCoronaToGround = TRUE
		
		IF sFMMCmenu.iEntityCreation = CREATION_TYPE_OBJECTS
			IF DOES_ENTITY_EXIST(sObjStruct.viCoronaObj)
				IF DOES_MODEL_FIT_TO_WALL(GET_ENTITY_MODEL(sObjStruct.viCoronaObj))
					bForceOnGroundCoronaToGround = FALSE
				ENDIF
			ENDIF
		
		ELIF sFMMCmenu.iEntityCreation = CREATION_TYPE_INTERACTABLE
			IF DOES_ENTITY_EXIST(sInteractableCreationStruct.oiCoronaInteractable)
				IF DOES_MODEL_FIT_TO_WALL(GET_ENTITY_MODEL(sInteractableCreationStruct.oiCoronaInteractable))
					bForceOnGroundCoronaToGround = FALSE
				ENDIF
			ENDIF
			
		ELIF sFMMCmenu.sActiveMenu = eFmmc_COVER_POINT_BASE
			bForceOnGroundCoronaToGround = FALSE
		ENDIF
		
		INT iTempBitsetOkToPlace = sCurrentVarsStruct.bitsetOkToPlace
		MAINTAIN_SCREEN_CENTER_COORD(vDummyRot, sInvisibleObjects, sCurrentVarsStruct, sFMMCmenu, TRUE, sHCS.hcCurrentCoronaColour, sFMMCmenu.fCreationHeightIncrease, sFMMCmenu.fCreationHeightIncrease != 0.0, FALSE, IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround), g_CreatorsSelDetails.bSettingDropOff, FALSE, sFMMCmenu.sActiveMenu = eFmmc_OBJECT_BASE, bForceOnGroundCoronaToGround)
		
		IF iTempBitsetOkToPlace != sCurrentVarsStruct.bitsetOkToPlace
			RESET_UP_HELP()
		ENDIF
	ENDIF
	
	IF !bshowRightmenu
		IF g_IsMainCreatorCameraRunning
		AND NOT IS_ARCADE_PLACEMENT_CREATOR()
			bshowRightmenu = TRUE
		ENDIF
	ELSE
		IF IS_ARCADE_PLACEMENT_CREATOR()
			
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(sFMMCdata.iBitSet, biTestMissionActive)
				
		// [LM][CopyAndPaste] - Needs to be above the MAINTAIN placed entity kind of functions.
		PROCESS_COPY_PASTE_FRONTEND_LOGIC(sCurrentVarsStruct)
		sFMMCMenu.iCopyFromEntity = -1
		sFMMCMenu.iCopyFromEntityTeam = -1
		
		// [LM] Each Entity Type seems to have weird conditions where blips can be recreated. This call will ensure that the "toggle" bs / bslong is reset to accomodate this without
		// Having to put a clear to those entity bs's all over the place for each entity type. A Fix for each individual issue should be worked on at some point.
		// Known problem areas. Props seem to recreate blips when placing one or exiting the place menu. Team Spawn Points seem to do it when placing.
		IF GET_FRAME_COUNT() % 60 = 0
			CLEAN_UP_TOGGLE_ENTITY_EDITTED_STATUS()
		ENDIF
		
		HANDLE_MENU_MARKERS()
		
		RESET_MAINTAIN_PLACED_ENTITY_VARIABLES(iLocalBitSet, sCurrentVarsStruct)
		
		RESET_SHARED_PLACEMENT_ENTITY_MENU_VARIABLES(sFMMCmenu)
		
		MAINTAIN_PLACED_TRIGGER_LOCATION(g_FMMC_STRUCT.vStartPos, sStartEndBlips.biStart, sStartEndBlips.ciStartType, g_FMMC_STRUCT.vCameraPanPos, bCameraPanBlip, bPhotoBlip)
		
		MAINTAIN_PLACED_TEAM_CAM_BLIPS()	
		
		INT j
		REPEAT FMMC_MAX_TEAMS j
			IF IS_LEGACY_MISSION_CREATOR()
				IF IS_BIT_SET(sFMMCmenu.iMiscBitSet, bsUseArialSpawnGrid)
					iMaxSpawnsPerTeam[j] = 8
				ELIF g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_VERSUS
				OR g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_LTS
				OR g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_CTF
				OR g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_PLANNING
					IF g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_PLANNING
						iMaxSpawnsPerTeam[j] = 4
					ELSE
						iMaxSpawnsPerTeam[j] = FMMC_MAX_TEAMSPAWNPOINTS
					ENDIF
				ELIF g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_HEIST
				OR g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_CONTACT
				OR g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_COOP
					iMaxSpawnsPerTeam[j] = FMMC_MAX_TEAMSPAWNPOINTS
				ELSE
					iMaxSpawnsPerTeam[j] = 1
				ENDIF
			ELSE
				IF IS_THIS_A_HEIST_MISSION()
				OR IS_THIS_A_VERSUS_MISSION()
				OR IS_THIS_A_CONTACT_MISSION()
					iMaxSpawnsPerTeam[j] = FMMC_MAX_TEAMSPAWNPOINTS
					
				ELIF IS_BIT_SET(sFMMCmenu.iMiscBitSet, bsUseArialSpawnGrid)
					iMaxSpawnsPerTeam[j] = 8
					
				ELSE
					iMaxSpawnsPerTeam[j] = g_FMMC_STRUCT.iMinNumParticipants / g_FMMC_STRUCT.iNumberOfTeams
					
				ENDIF
			ENDIF
			
			MAINTAIN_PLACED_TEAM_SPAWNPOINTS(sTeamSpawnStruct[j], j, sInvisibleObjects, sHCS, sCurrentVarsStruct, sFMMCmenu, sFMMCdata, iLocalBitSet, sFMMCendStage, sVehStruct)
			MAINTAIN_PLACED_TEAM_GRID_SPAWNS(sFMMCmenu, sCurrentVarsStruct, sFMMCdata, sTeamSpawnStruct[j], sHCS, sFMMCendStage, sInvisibleObjects, iLocalBitSet, j)
		ENDREPEAT
		
		MAINTAIN_PLACED_ENTITIES(sFMMCmenu, sHCS, sCurrentVarsStruct, sPedStruct, sVehStruct, sWepStruct, sObjStruct, sLocStruct, sTrainCreationStruct, sPropStruct, sDynoPropStruct, sInvisibleObjects, sFMMCdata, g_CreatorsSelDetails, sFMMCendStage, sCamData, iLocalBitSet, g_CreatorsSelDetails.iSelectedTeam)
		MAINTAIN_PLACED_TRAINS(sTrainCreationStruct, sCurrentVarsStruct, sFMMCmenu, sFMMCdata, sFMMCendStage, sHCS, iLocalBitSet)
		MAINTAIN_PLACED_INTERACTABLES(sCurrentVarsStruct, sFMMCmenu, sFMMCdata, sFMMCendStage, sHCS, sInteractableCreationStruct, sTrainCreationStruct, iLocalBitset)
		MAINTAIN_PLACED_ZONES(sCurrentVarsStruct, iLocalBitSet, sFMMCmenu)
		MAINTAIN_PLACED_DOORS(biDoors, sCurrentVarsStruct, iLocalBitSet, sFMMCmenu)
		MAINTAIN_PLACED_COVER(sCoverStruct, sCurrentVarsStruct, iLocalBitSet, sFMMCmenu)
		
		//Entity warp data
		IF 	sFMMCmenu.sActiveMenu = eFMMC_CUTSCENE_WARP_ENTITY_LOCATIONS
			MAINTAIN_PLACED_ENTITY_WARP_LOCATIONS_FOR_CUTSCENE(sFMMCmenu, g_FMMC_STRUCT.sCurrentSceneData.sCutsceneEntityWarpData)
		ELIF sFMMCmenu.sActiveMenu = eFMMC_CUTSCENE_MOCAP_WARP_ENTITY_LOCATION
			MAINTAIN_PLACED_ENTITY_WARP_LOCATIONS_FOR_CUTSCENE(sFMMCmenu, g_FMMC_STRUCT.sCurrentMocapSceneData.sCutsceneEntityWarpData)
		ENDIF
		
		MAINTAIN_PLACED_MOCAP_CUTSCENES(sFMMCendStage, sFMMCmenu, sCurrentVarsStruct, sFMMCdata, sVehStruct, sPedStruct, sObjStruct, sPropStruct, sDynoPropStruct, fMenuX, fAddY)
		MAINTAIN_PLACED_END_MOCAP_CUTSCENE(sFMMCendStage, sFMMCmenu, sCurrentVarsStruct, sFMMCdata, sVehStruct, sPedStruct, sObjStruct, sPropStruct, sDynoPropStruct, fMenuX, fAddY)
		MAINTAIN_PLACED_GRID_SPAWNS(sFMMCmenu, sCurrentVarsStruct, sFMMCdata, sTeamSpawnStruct, sHCS, sFMMCendStage, sInvisibleObjects, iLocalBitSet)
		MAINTAIN_PLACED_DUMMY_BLIPS(sCurrentVarsStruct, sFMMCmenu, sFMMCdata, sHCS, iLocalBitSet, sFMMCendStage)
		MAINTAIN_WORLD_PROPS(sFMMCmenu, sCurrentVarsStruct, iLocalBitSet)
		MAINTAIN_ROCKET_POSITIONS()
		MAINTAIN_TRIP_SKIP_PLACEMENT()
		MAINTAIN_PLACED_OVERTIME_ZONES(sCurrentVarsStruct, iLocalBitSet, sFMMCmenu) 
		MAINTAIN_PLACED_WARP_PORTALS(sFMMCmenu, biWarpPortalStart, sPedStruct, sVehStruct, sTrainCreationStruct, sObjStruct)
		MAINTAIN_PLACED_ELEVATORS(sFMMCmenu, biElevators#IF IS_DEBUG_BUILD , bDisableElevatorDrawing #ENDIF )
		MAINTAIN_PLACED_PTFX(sFMMCmenu, biPlacedPTFX)
		MAINTAIN_PLACED_MARKERS_ENTITY(sFMMCmenu, biPlacedMarker)
		MAINTAIN_BLIP_SPRITE_MENU_TEXT_PREVIEW(sFMMCmenu, sCurrentVarsStruct)		
		MAINTAIN_PRECISE_MARKER_FOR_MENUS(sCurrentVarsStruct, sFMMCmenu)
		MAINTAIN_CREATOR_OPTIONS_RANGE_MARKERS(sFMMCmenu)
		MAINTAIN_PLACED_DEV_ASSIST_ENTITIES(sCurrentVarsStruct, sFMMCmenu, sFMMCdata, sFMMCendStage, sHCS, sDevAssistCreationStruct, iLocalBitset)
		MAINTAIN_PLACED_END_CUTSCENE(sCurrentVarsStruct, sFMMCmenu, sFMMCdata, sFMMCendStage, sHCS)
		
		PROCESS_POST_MAINTAIN_ENTITIES()
		
		PROCESS_GANG_CHASE_PREVIEW_ENTITY(sEntityPreviewStruct, sFMMCmenu)
		
		INT iPoint
		INT iTeamCounter
		FOR iTeamCounter = 0 TO FMMC_MAX_TEAMS-1
			FOR iPoint = 0 TO FMMC_MAX_TEAMSPAWNPOINTS-1
				IF iPoint != sFMMCmenu.iSelectedEntity
				AND	iPoint < g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[iTeamCounter]
					MAINTAIN_CUSTOM_RESPAWN_DECALS(sCurrentVarsStruct, sFMMCmenu, FALSE, iTeamCounter, iPoint)
				ENDIF
			ENDFOR
		ENDFOR
		
		MAINTAIN_TURF_WAR_STATIC_CAM_MARKER(sFMMCmenu.iCurrentMarkersDrawn)
		MAINTAIN_FORCED_STATIC_LOCATION_CAM_MARKER(sFMMCmenu.iCurrentMarkersDrawn)
		
		UPDATE_MENU_FOR_TEST_MISSION_STATE()
		
		MAINTAIN_PLACED_CUTSCENES(sFMMCendStage, sFMMCmenu, sCurrentVarsStruct, sFMMCdata, sVehStruct, sPedStruct, sObjStruct, sPropStruct, sDynoPropStruct, fMenuX, fAddY)
		
		PROCESS_EVERY_FRAME_MENU_SETTINGS(sFMMCmenu)
		
		PROCESS_BLIP_INFO_STRUCT_DEFAULTS()
		
		IF NOT DOES_BLIP_EXIST(bCameraOutroBlip)
			IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.vCameraOutroPos)
				CREATE_FMMC_BLIP(bCameraOutroBlip, g_FMMC_STRUCT.vCameraOutroPos, HUD_COLOUR_WHITE, "FMMC_B_7", 1)
				SET_BLIP_SPRITE(bCameraOutroBlip, RADAR_TRACE_CAMERA)
				SET_BLIP_NAME_FROM_TEXT_FILE(bCameraOutroBlip, "FMMC_B_7")
			ENDIF
		ELSE
			IF IS_VECTOR_ZERO(g_FMMC_STRUCT.vCameraOutroPos)
			OR NOT ARE_VECTORS_EQUAL(GET_BLIP_COORDS(bCameraOutroBlip), g_FMMC_STRUCT.vCameraOutroPos)
				REMOVE_BLIP(bCameraOutroBlip)
			ENDIF
		ENDIF
		
		IF g_sMenuData.bHelpCreated = FALSE
			RESET_UP_HELP()
		ENDIF
		
		IF iMarkersDrawn != sFMMCmenu.iCurrentMarkersDrawn
			iMarkersDrawn = sFMMCmenu.iCurrentMarkersDrawn
		ENDIF
		
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			REMOVE_ALL_PED_WEAPONS(PLAYER_PED_ID())
		ENDIF
		
	ENDIF
	
	BOOL bRightMenuAirborne
	bRightMenuAirborne = FALSE
	IF sFMMCMenu.bRuleMenuActive	
		IF (g_CreatorsSelDetails.bSettingDropOff AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iDropOffBitSet, g_CreatorsSelDetails.iRow))
		OR (g_CreatorsSelDetails.bSettingDropOff AND g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iDropOffType[g_CreatorsSelDetails.iRow] = ciFMMC_DROP_OFF_TYPE_CYLINDER)
			bRightMenuAirborne = TRUE
		ENDIF
	ENDIF
	
	BOOL bCanSetHeight
	bCanSetHeight = CAN_HEIGHT_BE_SET_FOR_ENTITY(sFMMCmenu, sFMMCmenu.iEntityCreation, bRightMenuAirborne)
	DEAL_WITH_SELECTING_START_END_LOCATIONS()
	DEAL_WITH_SETTING_HEIGHT_ON_ENTITY(sFMMCmenu, sCamData, sCurrentVarsStruct, IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround), bCanSetheight)
	
	REMOVE_TEMP_ENTITES_IF_NOT_NEEDED(sFMMCmenu.iEntityCreation)
	
	IF MAINTAIN_FMMC_RADIO(sFMMCmenu, menuScrollController)
		REFRESH_MENU(sFMMCMenu)
		sCurrentVarsStruct.bResetUpHelp  = TRUE
	ENDIF
	
	PRINTLN("[Creator_Spam] sCurrentVarsStruct.iMenuState = ", sCurrentVarsStruct.iMenuState)
	SWITCH sCurrentVarsStruct.iMenuState
		CASE MENU_STATE_DEFAULT
			PROCESS_DEFAULT_MENU_STATE()
		BREAK
		
		CASE MENU_STATE_ADD_CUSTOM_PROP
			IF bDelayAFrame = FALSE
				IF DEAL_WITH_SETTING_MISSION_TEXT(sFMMCendStage, FALSE, FALSE, FALSE, TRUE)
					REFRESH_MENU(sFMMCMenu)
					sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
					bDelayAFrame = TRUE
					SET_KEYBOARD_MODE(KEYBOARD_MODE_DEBUG)
				ENDIF
			ELSE
				bDelayAFrame = FALSE
			ENDIF
		BREAK
		
		CASE MENU_STATE_ADD_CUSTOM_PED
			IF bDelayAFrame = FALSE
				IF DEAL_WITH_ADDING_CUSTOM_MODEL(sFMMCendStage, sFMMCmenu, TRUE)
					REFRESH_MENU(sFMMCMenu)
					sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
					bDelayAFrame = TRUE
				ENDIF
			ELSE
				bDelayAFrame = FALSE
			ENDIF
		BREAK
		
		CASE MENU_STATE_ADD_CUSTOM_OBJECT
			IF bDelayAFrame = FALSE
				IF DEAL_WITH_ADDING_CUSTOM_MODEL(sFMMCendStage, sFMMCmenu, TRUE, FALSE, TRUE)
					REFRESH_MENU(sFMMCMenu)
					sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
					bDelayAFrame = TRUE
					SET_KEYBOARD_MODE(KEYBOARD_MODE_DEBUG)
				ENDIF
			ELSE
				bDelayAFrame = FALSE
			ENDIF
		BREAK
		
		CASE MENU_STATE_GET_CONTENT_TO_LOAD_LOCALLY
			IF bDelayAFrame = FALSE
				IF DEAL_WITH_SETTING_LOAD_MISSION_TEXT(sFMMCendStage, sLocalLoadSaveStruct.bLocalLoadCancelled, sLocalLoadSaveStruct.tlContentToLoad)
					sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
					REFRESH_MENU(sFMMCmenu)
					bDelayAFrame = TRUE
					
					IF NOT IS_STRING_NULL_OR_EMPTY(sLocalLoadSaveStruct.tlContentToLoad)
						sCurrentVarsStruct.iEntityCreationStatus = STAGE_DEAL_WITH_LOCAL_LOADING
					ENDIF
				ENDIF
			ELSE
				RESET_LOCAL_LOAD_SAVE_DATA(sLocalLoadSaveStruct)
				bDelayAFrame = FALSE
			ENDIF
		BREAK
		
		CASE MENU_STATE_WARP_TO_INTERIOR
			HANDLE_PLAYER_INTERIOR_WARP(iWarpState, iWarpStateTimeoutStart, sFMMCmenu, sFMMCdata, sCamData, sCurrentVarsStruct)
		BREAK
		
		CASE MENU_STATE_PLACE_CAM
			PROCESS_CAMERA_PLACEMENT()
		BREAK
		
		CASE MENU_STATE_PAN_CAM
			IF sFMMCmenu.sActiveMenu != eFmmc_OUT_CAM_BASE
				IF DEAL_WITH_CREATOR_CAMERA_PAN_PREVIEW(iCamPanState, jobIntroData, vInitialPreviewPos)	
					sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
				ENDIF
			ELSE
				SET_CAM_COORD(sCamData.cam, g_FMMC_STRUCT.vCameraOutroPos)
				SET_CAM_ROT(sCamData.cam, g_FMMC_STRUCT.vCameraOutroRot)
				sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
			ENDIF
		BREAK
		
		CASE MENU_STATE_TITLE
			IF bDelayAFrame = FALSE
				IF DEAL_WITH_SETTING_MISSION_TEXT(sFMMCendStage, TRUE)	
					sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
					REFRESH_MENU(sFMMCmenu)
					bDelayAFrame = TRUE
				ENDIF
			ELSE
				bDelayAFrame = FALSE
			ENDIF
		BREAK
		
		CASE MENU_STATE_CUTSCENE_NAME
			IF bDelayAFrame = FALSE
				IF DEAL_WITH_SETTING_CUTSCENE_NAME(sFMMCendStage, sFMMCmenu.sActiveMenu = efMMC_CUTSCENE_ADD_MOCAP_SCENE, sFMMCmenu.sActiveMenu = eFmmc_END_OF_MISSION_END_CUTSCENE_MENU)	
					sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
					REFRESH_MENU(sFMMCmenu)
					bDelayAFrame = TRUE
				ENDIF
			ELSE
				bDelayAFrame = FALSE
			ENDIF
		BREAK
		
		CASE MENU_STATE_DUMMY_BLIP_NAME
			IF bDelayAFrame = FALSE
				IF DEAL_WITH_SETTING_DUMMY_BLIP_NAME(sFMMCendStage)	
					sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
					REFRESH_MENU(sFMMCmenu)
					bDelayAFrame = TRUE
				ENDIF
			ELSE
				bDelayAFrame = FALSE
			ENDIF
		BREAK
		
		CASE MENU_STATE_CUSTOM_SHARD_TITLE_TL
			IF bDelayAFrame = FALSE
				IF DEAL_WITH_SETTING_CUSTOM_SHARD_TEXT(sFMMCendStage, g_FMMC_STRUCT.tlCustomShardTitle)
					sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
					REFRESH_MENU(sFMMCmenu)
					bDelayAFrame = TRUE
				ENDIF
			ELSE
				bDelayAFrame = FALSE
			ENDIF
		BREAK
		
		CASE MENU_STATE_CUSTOM_SHARD_STRAPLINE_TL
			IF bDelayAFrame = FALSE
				IF DEAL_WITH_SETTING_CUSTOM_SHARD_TEXT(sFMMCendStage, g_FMMC_STRUCT.tlCustomShardStrapline)
					sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
					REFRESH_MENU(sFMMCmenu)
					bDelayAFrame = TRUE
				ENDIF
			ELSE
				bDelayAFrame = FALSE
			ENDIF
		BREAK
		
		CASE MENU_STATE_CUSTOM_SHARD_TITLE_TL2
			IF bDelayAFrame = FALSE
				IF DEAL_WITH_SETTING_CUSTOM_SHARD_TEXT(sFMMCendStage, g_FMMC_STRUCT.tlCustomShardTitle2)
					sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
					REFRESH_MENU(sFMMCmenu)
					bDelayAFrame = TRUE
				ENDIF
			ELSE
				bDelayAFrame = FALSE
			ENDIF
		BREAK
		
		CASE MENU_STATE_CUSTOM_SHARD_STRAPLINE_TL2
			IF bDelayAFrame = FALSE
				IF DEAL_WITH_SETTING_CUSTOM_SHARD_TEXT(sFMMCendStage, g_FMMC_STRUCT.tlCustomShardStrapline2)
					sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
					REFRESH_MENU(sFMMCmenu)
					bDelayAFrame = TRUE
				ENDIF
			ELSE
				bDelayAFrame = FALSE
			ENDIF
		BREAK
		
		CASE MENU_STATE_CUSTOM_SHARD_TITLE_TL3
			IF bDelayAFrame = FALSE
				IF DEAL_WITH_SETTING_CUSTOM_SHARD_TEXT(sFMMCendStage, g_FMMC_STRUCT.tlCustomShardTitle3)
					sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
					REFRESH_MENU(sFMMCmenu)
					bDelayAFrame = TRUE
				ENDIF
			ELSE
				bDelayAFrame = FALSE
			ENDIF
		BREAK
		
		CASE MENU_STATE_CUSTOM_SHARD_STRAPLINE_TL3
			IF bDelayAFrame = FALSE
				IF DEAL_WITH_SETTING_CUSTOM_SHARD_TEXT(sFMMCendStage, g_FMMC_STRUCT.tlCustomShardStrapline3)
					sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
					REFRESH_MENU(sFMMCmenu)
					bDelayAFrame = TRUE
				ENDIF
			ELSE
				bDelayAFrame = FALSE
			ENDIF
		BREAK
		
		CASE MENU_STATE_CAM_STRINGS
			IF bDelayAFrame = FALSE
				IF DEAL_WITH_SETTING_CAM_NAME_AND_LOCATION(sFMMCendStage, GET_CREATOR_MENU_SELECTION(sFMMCmenu), sFMMCmenu.iCurrentShot)	
					sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
					REFRESH_MENU(sFMMCmenu)
					bDelayAFrame = TRUE
				ENDIF
			ELSE
				bDelayAFrame = FALSE
			ENDIF
		BREAK
		
		CASE MENU_STATE_TARGET_STRINGS
			IF bDelayAFrame = FALSE
				IF DEAL_WITH_SETTING_TEAM_TARGET_STRINGS(sFMMCendStage, sFMMCmenu.iSelectedTeam)	
					sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
					REFRESH_MENU(sFMMCmenu)
					bDelayAFrame = TRUE
				ENDIF
			ELSE
				bDelayAFrame = FALSE
			ENDIF
		BREAK
		
		CASE MENU_STATE_CUSTOM_PHUD_STRING
			IF bDelayAFrame = FALSE
				IF DEAL_WITH_SETTING_REMAINING_PLAYER_HUD_STRINGS(sFMMCendStage, sFMMCmenu.iSelectedTeam)	
					sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
					REFRESH_MENU(sFMMCmenu)
					bDelayAFrame = TRUE
				ENDIF
			ELSE
				bDelayAFrame = FALSE
			ENDIF
		BREAK
		
		CASE MENU_STATE_ENTER_SIMPLE_TL63
			IF bDelayAFrame = FALSE
			
				TEXT_LABEL_63 tl63Return
				INT iCap
				iCap = -1
				
				IF sFMMCmenu.sActiveMenu = eFMMC_TEAM_SETTINGS
				AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_SETTINGS_MULTI_RULE_TIMER_FAIL_STRINGS
					tl63Return = g_FMMC_STRUCT.tl63MultiRuleTimerFailString[sFMMCmenu.iSelectedTeam]
				ELIF sFMMCmenu.sActiveMenu = eFMMC_RANDOMISED_SPAWN_GROUP_TEXT_OVERRIDES
					tl63Return = g_FMMC_STRUCT.tl63ObjectiveText_SpawnGroupOverride[GET_CREATOR_MENU_SELECTION(sFMMCmenu)]
				ELIF sFMMCmenu.sActiveMenu = eFMMC_TEAM_OBJECTIVE_TIMER_FAIL_STRINGS
					tl63Return = g_FMMC_STRUCT.tl63ObjectiveTimerFailString[sFMMCmenu.iSelectedTeam][GET_CREATOR_MENU_SELECTION(sFMMCmenu)]
				ELIF sFMMCmenu.sActiveMenu = eFmmc_DIALOGUE_TRIGGER_BASE
				AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DIALOGUE_TRIGGER_AMBIENT_VOICE_NAME
					tl63Return = sFMMCMenu.sDialogueTrigger.tlCustomVoice
					iCap = 15
				ELIF sFMMCmenu.sActiveMenu = eFmmc_DEV_ASSIST_BASE
				AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DEV_ASSIST_MENU_DESCRIPTION
					tl63Return = g_FMMC_STRUCT_ENTITIES.sEditedDevAssist.tl31_Description
					iCap = 25
				ELIF sFMMCmenu.sActiveMenu = eFmmc_DEV_ASSIST_BASE
				AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DEV_ASSIST_MENU_TITLE
					tl63Return = g_FMMC_STRUCT_ENTITIES.sEditedDevAssist.tl31_Title
					iCap = 25
				ELIF sFMMCmenu.sActiveMenu = eFmmc_DEV_ASSIST_BASE
				AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DEV_ASSIST_MENU_TAG
					tl63Return = g_FMMC_STRUCT_ENTITIES.tl31_DevAssistTag				
					iCap = 25
				ELIF sFMMCmenu.sActiveMenu = eFmmc_DEV_ASSIST_BASE
				AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DEV_ASSIST_MENU_DEV_NOTES_CONTENT_ID
					tl63Return = g_FMMC_STRUCT_ENTITIES.tl63_DevNotesLoadFromContentID						
				ELIF sFMMCmenu.sActiveMenu = eFmmc_DEV_LOAD_FROM_CONTENT_BASE
				AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DEV_ASSIST_LOAD_FROM_CONTENT_MENU_ID
					tl63Return = g_FMMC_STRUCT_ENTITIES.tl63_LoadFromContentID
				ELIF sFMMCmenu.sActiveMenu = efMMC_CUTSCENE_MOCAP_ENTITIES_HIDE
					tl63Return = g_fmmc_struct.sCurrentMocapSceneData.tl31_HideCutsceneHandles[GET_CREATOR_MENU_SELECTION(sFMMCmenu)]
					iCap = 31
				ELIF sFMMCmenu.sActiveMenu = eFmmc_END_OF_MISSION_END_CUTSCENE_ENTITIES_HIDE
					tl63Return = g_fmmc_struct.sEndMocapSceneData.tl31_HideCutsceneHandles[GET_CREATOR_MENU_SELECTION(sFMMCmenu)]
					iCap = 31
				ELIF GET_CREATOR_MENU_SELECTION(sFMMCMenu) = WORLD_PROPS_SELECTED_MODEL_ENTER_MANUALLY
				AND sFMMCmenu.sActiveMenu = efMMC_WORLD_PROPS
					IF sFMMCmenu.iSelectedEntity != -1
						tl63Return = FMMC_GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT_ENTITIES.sWorldProps[sFMMCmenu.iSelectedEntity].mn)
					ELSE
						tl63Return = FMMC_GET_MODEL_NAME_FOR_DEBUG(sFMMCmenu.sCurrentWorldProp.mn)
					ENDIF
					iCap = 63
				ENDIF
					
				IF DEAL_WITH_SETTING_SIMPLE_TL63(sFMMCendStage, tl63Return, iCap)
					
					IF sFMMCmenu.sActiveMenu = eFMMC_TEAM_SETTINGS
					AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_SETTINGS_MULTI_RULE_TIMER_FAIL_STRINGS
						g_FMMC_STRUCT.tl63MultiRuleTimerFailString[sFMMCmenu.iSelectedTeam] = tl63Return
					ELIF sFMMCmenu.sActiveMenu = eFMMC_RANDOMISED_SPAWN_GROUP_TEXT_OVERRIDES
						g_FMMC_STRUCT.tl63ObjectiveText_SpawnGroupOverride[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] = tl63Return
					ELIF sFMMCmenu.sActiveMenu = eFMMC_TEAM_OBJECTIVE_TIMER_FAIL_STRINGS
						g_FMMC_STRUCT.tl63ObjectiveTimerFailString[sFMMCmenu.iSelectedTeam][GET_CREATOR_MENU_SELECTION(sFMMCmenu)] = tl63Return
					ELIF sFMMCmenu.sActiveMenu = eFmmc_DIALOGUE_TRIGGER_BASE
					AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DIALOGUE_TRIGGER_AMBIENT_VOICE_NAME
						sFMMCMenu.sDialogueTrigger.tlCustomVoice = tl63Return
					ELIF sFMMCmenu.sActiveMenu = eFmmc_DEV_ASSIST_BASE
					AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DEV_ASSIST_MENU_DESCRIPTION
						g_FMMC_STRUCT_ENTITIES.sEditedDevAssist.tl31_Description = tl63Return
					ELIF sFMMCmenu.sActiveMenu = eFmmc_DEV_ASSIST_BASE
					AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DEV_ASSIST_MENU_TITLE
						g_FMMC_STRUCT_ENTITIES.sEditedDevAssist.tl31_Title = tl63Return
					ELIF sFMMCmenu.sActiveMenu = eFmmc_DEV_ASSIST_BASE
					AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DEV_ASSIST_MENU_TAG
						g_FMMC_STRUCT_ENTITIES.tl31_DevAssistTag = tl63Return						
					ELIF sFMMCmenu.sActiveMenu = eFmmc_DEV_ASSIST_BASE
					AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DEV_ASSIST_MENU_DEV_NOTES_CONTENT_ID
						g_FMMC_STRUCT_ENTITIES.tl63_DevNotesLoadFromContentID = tl63Return									
					ELIF sFMMCmenu.sActiveMenu = eFmmc_DEV_LOAD_FROM_CONTENT_BASE
					AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DEV_ASSIST_LOAD_FROM_CONTENT_MENU_ID
						g_FMMC_STRUCT_ENTITIES.tl63_LoadFromContentID = tl63Return
					ELIF sFMMCmenu.sActiveMenu = efMMC_CUTSCENE_MOCAP_ENTITIES_HIDE
						g_fmmc_struct.sCurrentMocapSceneData.tl31_HideCutsceneHandles[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] = tl63Return
					ELIF sFMMCmenu.sActiveMenu = eFmmc_END_OF_MISSION_END_CUTSCENE_ENTITIES_HIDE
						g_fmmc_struct.sEndMocapSceneData.tl31_HideCutsceneHandles[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] = tl63Return
					ELIF GET_CREATOR_MENU_SELECTION(sFMMCMenu) = WORLD_PROPS_SELECTED_MODEL_ENTER_MANUALLY
					AND sFMMCmenu.sActiveMenu = efMMC_WORLD_PROPS
						IF sFMMCmenu.iSelectedEntity != -1
							g_FMMC_STRUCT_ENTITIES.sWorldProps[sFMMCmenu.iSelectedEntity].mn = INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY(tl63return))
						ELSE
							sFMMCmenu.sCurrentWorldProp.mn = INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY(tl63return))
						ENDIF						
					ENDIF
					
					SET_KEYBOARD_MODE(KEYBOARD_MODE_DEBUG)
					
					tl63return = ""
					sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
					REFRESH_MENU(sFMMCmenu)
					bDelayAFrame = TRUE
				ENDIF
			ELSE
				bDelayAFrame = FALSE
			ENDIF
		BREAK
		
		CASE MENU_STATE_ENTER_SIMPLE_VECTOR
			PRINTLN("MENU_STATE_ENTER_SIMPLE_VECTOR - Inside menu state.")
			
			IF bDelayAFrame = FALSE	
				
				TEXT_LABEL_63 tl63Return
				
				PRINTLN("MENU_STATE_ENTER_SIMPLE_VECTOR - Calling DEAL_WITH_SETTING_SIMPLE_TL63")
					
				IF DEAL_WITH_SETTING_SIMPLE_TL63(sFMMCendStage, tl63Return)
					
					IF ARE_STRINGS_EQUAL(tl63Return, "CANCELLED_")
						SET_BIT(sFMMCmenu.iFMMCMenuBitset2, ciFMMCmenuBS2_VectorCancelledFromKeyboard)
					ELSE
						SET_BIT(sFMMCmenu.iFMMCMenuBitset, ciFMMCmenuBS_VectorSetFromKeyboard)
					ENDIF
					
					sFMMCmenu.vOnScreenResult = GET_VECTOR_FROM_STRING(tl63Return)
					
					PRINTLN("MENU_STATE_ENTER_SIMPLE_VECTOR - tl63Return: ", tl63Return, " sFMMCmenu.vOnScreenResult: ", sFMMCmenu.vOnScreenResult)
					
					tl63return = ""
					sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
					REFRESH_MENU(sFMMCmenu)
					bDelayAFrame = TRUE
					
					SET_KEYBOARD_MODE(KEYBOARD_MODE_DEBUG)
				ENDIF
			ELSE
				bDelayAFrame = FALSE
			ENDIF
		BREAK
		
		CASE MENU_STATE_ENTER_ON_SCREEN_POPUP_WINDOW
			PRINTLN("MENU_STATE_ENTER_ON_SCREEN_POPUP_WINDOW - Inside menu state.")
			
			IF bDelayAFrame = FALSE					
				PRINTLN("MENU_STATE_ENTER_ON_SCREEN_POPUP_WINDOW - Calling DEAL_WITH_SETTING_SIMPLE_TL63")
				
				REMOVE_MENU_HELP_KEYS()
				IF IS_PC_VERSION()
					SET_MENU_HELP_CLEAR_SPACE(32)
				ELSE
					SET_MENU_HELP_CLEAR_SPACE(34)
				ENDIF
				
				IF PROCESS_ON_SCREEN_POPUP_WINDOW(sFMMCmenu)					
					PRINTLN("MENU_STATE_ENTER_ON_SCREEN_POPUP_WINDOW - TURE")										
					sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
					REFRESH_MENU(sFMMCmenu)
					bDelayAFrame = TRUE
					SET_BIT(sFMMCmenu.iFMMCMenuBitset, ciFMMCmenuBS_BlockOnScreenPopupThisFrame)
					SET_KEYBOARD_MODE(KEYBOARD_MODE_DEBUG)
				ENDIF
				
				RESET_SCRIPT_GFX_ALIGN()
				
				IF LOAD_MENU_ASSETS()
					INT iScreenX, iScreenY
					GET_SCREEN_RESOLUTION(iScreenX, iScreenY)
					DRAW_MENU_HELP_SCALEFORM(iScreenX)
					SET_INSTRUCTIONAL_BUTTONS_UNDER_HUD_THIS_FRAME(1)
				ENDIF
			ELSE
				bDelayAFrame = FALSE
			ENDIF
		BREAK
		
		CASE MENU_STATE_ENTER_FORCE_EDIT_STRING
			PRINTLN("[CreatorMenu_FMMCForceEdit] MENU_STATE_ENTER_FORCE_EDIT_STRING - Inside menu state.")
			
			IF bDelayAFrame = FALSE
				TEXT_LABEL_63 tl63Return
				PRINTLN("[CreatorMenu_FMMCForceEdit] MENU_STATE_ENTER_FORCE_EDIT_STRING - Calling DEAL_WITH_SETTING_SIMPLE_TL63")
				
				IF DEAL_WITH_SETTING_SIMPLE_TL63(sFMMCendStage, tl63Return)
					
					SET_UP_FORCE_EDIT_INT(GET_INT_FROM_STRING(tl63Return))
					SET_UP_FORCE_EDIT_FLOAT(GET_FLOAT_FROM_STRING(tl63Return))
					
					g_bFMMCForceEditEnteringString = FALSE
					PRINTLN("[CreatorMenu_FMMCForceEdit] MENU_STATE_ENTER_FORCE_EDIT_STRING - tl63Return: ", tl63Return)
					
					tl63return = ""
					sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
					SET_KEYBOARD_MODE(KEYBOARD_MODE_DEBUG)
					
					REFRESH_MENU(sFMMCmenu)
					bDelayAFrame = TRUE
				ENDIF
			ELSE
				PRINTLN("[CreatorMenu_FMMCForceEdit] MENU_STATE_ENTER_FORCE_EDIT_STRING - Waiting for frame delay.")
				bDelayAFrame = FALSE
			ENDIF
		BREAK
		
		CASE MENU_STATE_POINTS_TITLE
			IF bDelayAFrame = FALSE
				IF DEAL_WITH_SETTING_SIMPLE_TL63(sFMMCendStage, g_FMMC_STRUCT.tl63PointsTitle)
					sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
					REFRESH_MENU(sFMMCmenu)
					bDelayAFrame = TRUE
				ENDIF
			ELSE
				bDelayAFrame = FALSE
			ENDIF
		BREAK
		
		#IF IS_DEBUG_BUILD
		CASE MENU_STATE_SCRIPTED_ENTITY_HANDLE
			IF bDelayAFrame = FALSE
				IF DEAL_WITH_SETTING_SCRIPTED_CUTSCENE_ENTITY_HANDLE(sFMMCendStage, sFMMCmenu.iCurrentCutsceneEntity)	
					sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
					REFRESH_MENU(sFMMCmenu)
					bDelayAFrame = TRUE
				ENDIF
			ELSE
				bDelayAFrame = FALSE
			ENDIF
		BREAK
		#ENDIF
		
		CASE MENU_STATE_MOCAP_ENTITY_HANDLE
			IF bDelayAFrame = FALSE
				IF DEAL_WITH_SETTING_MOCAP_ENTITY_HANDLE(sFMMCendStage, sFMMCmenu.iCurrentCutsceneEntity)	
					sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
					REFRESH_MENU(sFMMCmenu)
					bDelayAFrame = TRUE
				ENDIF
			ELSE
				bDelayAFrame = FALSE
			ENDIF
		BREAK
		
		CASE MENU_STATE_END_MOCAP_ENTITY_HANDLE
			IF bDelayAFrame = FALSE
				IF DEAL_WITH_SETTING_END_MOCAP_ENTITY_HANDLE(sFMMCendStage, sFMMCmenu.iCurrentCutsceneEntity)	
					sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
					REFRESH_MENU(sFMMCmenu)
					bDelayAFrame = TRUE
				ENDIF
			ELSE
				bDelayAFrame = FALSE
			ENDIF
		BREAK
		
		CASE MENU_STATE_CUTSCENE_PLAYER_OPTIONS_INDIVIDUAL_PROP_HANDLE
			IF bDelayAFrame = FALSE
				IF DEAL_WITH_CUTSCENE_PLAYER_OPTIONS_INDIVIDUAL_PROP_HANDLE(sFMMCendStage, sFMMCmenu)	
					sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
					REFRESH_MENU(sFMMCmenu)
					bDelayAFrame = TRUE
				ENDIF
			ELSE
				bDelayAFrame = FALSE
			ENDIF
		BREAK
		
		CASE MENU_STATE_PLANNING_MISSION_NAME
			IF bDelayAFrame = FALSE
				IF DEAL_WITH_SETTING_PLANNING_MISSION_NAME(sFMMCendStage, GET_ADD_LEVEL_SELECTION())
					SORT_PLANNING_MISSIONS()
					sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
					REFRESH_MENU(sFMMCmenu)
					bDelayAFrame = TRUE
				ENDIF
			ELSE
				bDelayAFrame = FALSE
			ENDIF
		BREAK
		
		CASE MENU_STATE_NEXT_MISSION_NAME
			IF bDelayAFrame = FALSE
				IF DEAL_WITH_SETTING_NEXT_MISSION_NAME(sFMMCendStage, sFMMCmenu)
					sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
					REFRESH_MENU(sFMMCmenu)
					bDelayAFrame = TRUE
				ENDIF
			ELSE
				bDelayAFrame = FALSE
			ENDIF
		BREAK
		
		CASE MENU_STATE_ENTER_DIALOGUE_APPEND
			IF bDelayAFrame = FALSE
				
				TEXT_LABEL_3 tl3Assign
				IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DIALOGUE_TRIGGER_APPEND_OPTIONS_CUSTOM_CHARACTER
					tl3Assign = sFMMCMenu.sDialogueTrigger.tlAppendCustomCharacter
				ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DIALOGUE_TRIGGER_APPEND_OPTIONS_APPEND_ON_AGGRO
					tl3Assign = sFMMCMenu.sDialogueTrigger.tl3AppendOnAggro
				ENDIF
				
				IF DEAL_WITH_SETTING_DIALOGUE_APPEND(sFMMCendStage, tl3Assign)
				
					IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DIALOGUE_TRIGGER_APPEND_OPTIONS_CUSTOM_CHARACTER
						sFMMCMenu.sDialogueTrigger.tlAppendCustomCharacter = tl3Assign
					ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DIALOGUE_TRIGGER_APPEND_OPTIONS_APPEND_ON_AGGRO	
						sFMMCMenu.sDialogueTrigger.tl3AppendOnAggro = tl3Assign
					ENDIF
					
					sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
					REFRESH_MENU(sFMMCmenu)
					bDelayAFrame = TRUE
				ENDIF
			ELSE
				bDelayAFrame = FALSE
			ENDIF
		BREAK
		
		CASE MENU_STATE_CUSTOM_PED_FAIL_NAME
			IF bDelayAFrame = FALSE
				IF DEAL_WITH_SETTING_CUSTOM_FAIL_NAME(sFMMCendStage, CREATION_TYPE_PEDS)
					sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
					REFRESH_MENU(sFMMCmenu)
					bDelayAFrame = TRUE
				ENDIF
			ELSE
				bDelayAFrame = FALSE
			ENDIF
		BREAK
		CASE MENU_STATE_CUSTOM_VEH_FAIL_NAME
			IF bDelayAFrame = FALSE
				IF DEAL_WITH_SETTING_CUSTOM_FAIL_NAME(sFMMCendStage, CREATION_TYPE_VEHICLES)
					sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
					REFRESH_MENU(sFMMCmenu)
					bDelayAFrame = TRUE
				ENDIF
			ELSE
				bDelayAFrame = FALSE
			ENDIF
		BREAK
		CASE MENU_STATE_CUSTOM_OBJ_FAIL_NAME
			IF bDelayAFrame = FALSE
				IF DEAL_WITH_SETTING_CUSTOM_FAIL_NAME(sFMMCendStage, CREATION_TYPE_OBJECTS)
					sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
					REFRESH_MENU(sFMMCmenu)
					bDelayAFrame = TRUE
				ENDIF
			ELSE
				bDelayAFrame = FALSE
			ENDIF
		BREAK
		
		CASE MENU_STATE_IPL
			IF bDelayAFrame = FALSE
				IF DEAL_WITH_SETTING_CUSTOM_TEAM_NAME(sFMMCendStage, GET_CREATOR_MENU_SELECTION(sFMMCmenu))
					sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
					REFRESH_MENU(sFMMCmenu)
					bDelayAFrame = TRUE
				ENDIF
			ELSE
				bDelayAFrame = FALSE
			ENDIF
		BREAK
		
		CASE MENU_STATE_DIALOGUE_ENTRY
			IF bDelayAFrame = FALSE
				TEXT_LABEL_31 tlTemp
				IF DEAL_WITH_SETTING_DIALOGUE_TEXT(sFMMCendStage, tlTemp)
					IF NOT IS_STRING_NULL_OR_EMPTY(tlTemp)
						IF sFMMCmenu.sActiveMenu = efmmc_SPEAKER_LIST
							g_FMMC_STRUCT.sSpeakers[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] = tlTemp
						ELIF sFMMCmenu.sActiveMenu = eFmmc_DIALOGUE_TRIGGER_BASE
							IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DIALOGUE_TRIGGER_FILE_ID
								sFMMCmenu.sDialogueTrigger.tlBlock = tlTemp
							ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DIALOGUE_TRIGGER_ID
								sFMMCmenu.sDialogueTrigger.tlRoot = tlTemp
							ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DIALOGUE_TRIGGER_ALSO_DISPLAY_HELPTEXT
								sFMMCmenu.sDialogueTrigger.tlDialogueTriggerAdditionalHelpText = tlTemp
							ENDIF
						ENDIF
					ENDIF
					sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
					REFRESH_MENU(sFMMCmenu)
					bDelayAFrame = TRUE
				ENDIF
			ELSE
				bDelayAFrame = FALSE
			ENDIF
		BREAK
		
		CASE MENU_STATE_CUSTOM_TEAMNAME
			IF bDelayAFrame = FALSE
				IF DEAL_WITH_SETTING_CUSTOM_TEAM_NAME(sFMMCendStage, sFMMCmenu.iSelectedTeam)
					sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
					REFRESH_MENU(sFMMCmenu)
					bDelayAFrame = TRUE
				ENDIF
			ELSE
				bDelayAFrame = FALSE
			ENDIF
		BREAK
		
		CASE MENU_STATE_DESCRIPTION
			IF bDelayAFrame = FALSE
				IF DEAL_WITH_SETTING_MISSION_TEXT(sFMMCendStage, FALSE)	
					sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
					REFRESH_MENU(sFMMCmenu)
					bDelayAFrame = TRUE
				ENDIF
			ELSE
				bDelayAFrame = FALSE
			ENDIF
		BREAK
		
		CASE MENU_STATE_SEARCH
			IF bDelayAFrame = FALSE
				IF DEAL_WITH_MENU_SEARCH(sFMMCendStage, sFMMCmenu)
					sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
					REFRESH_MENU(sFMMCmenu)
					bDelayAFrame = TRUE
					#IF IS_DEBUG_BUILD
						SET_KEYBOARD_MODE(KEYBOARD_MODE_DEBUG)
					#ENDIF
				ENDIF
			ELSE
				bDelayAFrame = FALSE
			ENDIF
		BREAK
		
		CASE MENU_STATE_TAGS
			IF bDelayAFrame = FALSE
				IF DEAL_WITH_SETTING_MISSION_TEXT(sFMMCendStage, FALSE, TRUE)	
					sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
					REFRESH_MENU(sFMMCmenu)
					bDelayAFrame = TRUE
				ENDIF
			ELSE
				bDelayAFrame = FALSE
			ENDIF
		BREAK

		CASE MENU_STATE_CUSTOM_TIMER_STRING
			IF bDelayAFrame = FALSE
				IF DEAL_WITH_SETTING_CUSTOM_TIMER(sFMMCendStage)	
					sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
					REFRESH_MENU(sFMMCmenu)
					bDelayAFrame = TRUE
				ENDIF
			ELSE
				bDelayAFrame = FALSE
			ENDIF
		BREAK
		
		CASE MENU_STATE_SMS_MESSAGE
			IF bDelayAFrame = FALSE
				IF DEAL_WITH_SETTING_MISSION_SMS_MESSAGE(sFMMCendStage, sFMMCmenu.iCurrentSMS)	
					sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
					REFRESH_MENU(sFMMCmenu)
					bDelayAFrame = TRUE
				ENDIF
			ELSE
				bDelayAFrame = FALSE
			ENDIF
		BREAK
			
		CASE MENU_STATE_CUSTOM_TEXT
			IF bDelayAFrame = FALSE
				IF DEAL_WITH_SETTING_MISSION_CUSTOM_TEXT(sFMMCendStage, sFMMCmenu)	
					sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
					REFRESH_MENU(sFMMCmenu)
					bDelayAFrame = TRUE
				ENDIF
			ELSE
				bDelayAFrame = FALSE
			ENDIF
		BREAK
		
		CASE MENU_STATE_ENTER_RULE_SYSTEM_BAR_TEXT
			IF bDelayAFrame = FALSE	
				TEXT_LABEL_63 tl63_TextOut
				INT iTeam 
				INT iOrder
				iTeam = g_CreatorsSelDetails.iSelectedTeam
				iOrder = g_CreatorsSelDetails.iRow
				
				IF DEAL_WITH_SETTING_SIMPLE_TL63(sFMMCendStage, tl63_TextOut)
					
					IF IS_THIS_CURRENT_RHM_MENU(eRHM_AltitudeBarSystemMenuOptions)
						g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sAltitudeBar[iOrder].tl63_MainBar = tl63_TextOut
					ENDIF
					
					sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
					REFRESH_MENU(sFMMCmenu)
					bDelayAFrame = TRUE
				ENDIF
			ELSE
				bDelayAFrame = FALSE
			ENDIF
		BREAK
		
		CASE MENU_STATE_ENTER_RULE_SYSTEM_BAR_CONS_TEXT
			IF bDelayAFrame = FALSE	
				TEXT_LABEL_63 tl63_TextOut
				INT iTeam 
				INT iOrder
				iTeam = g_CreatorsSelDetails.iSelectedTeam
				iOrder = g_CreatorsSelDetails.iRow
				
				IF DEAL_WITH_SETTING_SIMPLE_TL63(sFMMCendStage, tl63_TextOut)
					
					IF IS_THIS_CURRENT_RHM_MENU(eRHM_AltitudeBarSystemMenuOptions)
						g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sAltitudeBar[iOrder].tl63_ConsequenceBar = tl63_TextOut
					ENDIF
					
					sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
					REFRESH_MENU(sFMMCmenu)
					bDelayAFrame = TRUE
				ENDIF
			ELSE
				bDelayAFrame = FALSE
			ENDIF
		BREAK
		
		CASE MENU_STATE_ENTER_PED_CUSTOM_ANIM_DICT
			IF bDelayAFrame = FALSE	
				IF sFMMCMenu.iSelectedTeam > -1
					IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_ANIMS_MENU_IDLE_ANIM_TYPE
					OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_ANIMS_MENU_IDLE_ANIM_CUSTOM_DICT
					OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_ANIMS_MENU_IDLE_ANIM_CUSTOM_NAME						
						IF DEAL_WITH_SETTING_SIMPLE_TL63(sFMMCendStage, g_FMMC_STRUCT_ENTITIES.sEditedPed.tlCustomIdleAnimDict)										
							sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
							REFRESH_MENU(sFMMCmenu)
							bDelayAFrame = TRUE
						ENDIF
						
					ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_ANIMS_MENU_SPECIAL_ANIM_TYPE
					OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_ANIMS_MENU_SPECIAL_ANIM_CUSTOM_DICT
					OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_ANIMS_MENU_SPECIAL_ANIM_CUSTOM_NAME
						IF DEAL_WITH_SETTING_SIMPLE_TL63(sFMMCendStage, g_FMMC_STRUCT_ENTITIES.sEditedPed.tlCustomSpecialAnimDict)										
							sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
							REFRESH_MENU(sFMMCmenu)
							bDelayAFrame = TRUE
						ENDIF
					ENDIF
				ENDIF
			ELSE
				bDelayAFrame = FALSE
			ENDIF
		BREAK
		
		CASE MENU_STATE_ENTER_PED_CUSTOM_ANIM_NAME
			IF bDelayAFrame = FALSE	
				IF sFMMCMenu.iSelectedTeam > -1
					IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_ANIMS_MENU_IDLE_ANIM_TYPE
					OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_ANIMS_MENU_IDLE_ANIM_CUSTOM_DICT
					OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_ANIMS_MENU_IDLE_ANIM_CUSTOM_NAME	
						IF DEAL_WITH_SETTING_SIMPLE_TL63(sFMMCendStage, g_FMMC_STRUCT_ENTITIES.sEditedPed.tlCustomIdleAnimName)										
							sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
							REFRESH_MENU(sFMMCmenu)
							bDelayAFrame = TRUE
						ENDIF
						
					ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_ANIMS_MENU_SPECIAL_ANIM_TYPE
					OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_ANIMS_MENU_SPECIAL_ANIM_CUSTOM_DICT
					OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_ANIMS_MENU_SPECIAL_ANIM_CUSTOM_NAME
						IF DEAL_WITH_SETTING_SIMPLE_TL63(sFMMCendStage, g_FMMC_STRUCT_ENTITIES.sEditedPed.tlCustomSpecialAnimName)										
							sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
							REFRESH_MENU(sFMMCmenu)
							bDelayAFrame = TRUE
						ENDIF
					ENDIF
				ENDIF
			ELSE
				bDelayAFrame = FALSE
			ENDIF
		BREAK
		
		CASE MENU_STATE_SYNOPSIS
			IF bDelayAFrame = FALSE
				BOOL bCancelled
				bCancelled = false
				IF DEAL_WITH_ENTERING_OBJECTIVE_TEXT(6, bCancelled)	
					sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
					REFRESH_MENU(sFMMCmenu)
					bDelayAFrame = TRUE
				ENDIF
				IF bCancelled
					sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
					REFRESH_MENU(sFMMCmenu)
					bDelayAFrame = TRUE
				ENDIF
			ELSE
				bDelayAFrame = FALSE
			ENDIF
		BREAK
		
		CASE MENU_STATE_TEST_MISSION
			// MAINTAIN_TEST_MISSION_STATE()
		BREAK
									
		CASE MENU_STATE_SWITCH_CAM
			IF NOT IS_SCREEN_FADED_OUT()
				PRINTSTRING("screen not faded out yet, calling fade out")PRINTNL()
				DO_SCREEN_FADE_OUT(500)
			ELSE
				PRINTSTRING("Screen is faded out do swap cams")PRINTNL()
				IF NOT IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
					SWAP_CAMERAS(sCamData, sFMMCdata, sFMMCMenu, sCurrentVarsStruct,TRUE,FALSE,TRUE,FALSE)
				ELSE
					SWAP_CAMERAS(sCamData, sFMMCdata, sFMMCMenu, sCurrentVarsStruct,FALSE,FALSE,TRUE,FALSE)
				ENDIF
				NEW_LOAD_SCENE_START_SPHERE(GET_ENTITY_COORDS(PLAYER_PED_ID()), 20)
				
				sCurrentVarsStruct.iMenuState = MENU_STATE_LOADING_AREA
			ENDIF							
		BREAK
		
		CASE MENU_STATE_LOADING_AREA
			IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
			OR IS_NEW_LOAD_SCENE_LOADED()		
				PRINTSTRING("LOADED, FADE IN")PRINTNL()
				DO_SCREEN_FADE_IN(500)
				NEW_LOAD_SCENE_STOP()
				RESET_INVISIBLE_OBJECT_POSITIONS(sInvisibleObjects)
				IF IS_BIT_sET(sFMMCdata.iBitSet, bOnTheGround)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
					FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
				ENDIF
				sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT		
			ELSE
				PRINTSTRING("WAITING FOR SCENE TO LOAD")PRINTNL()								
			ENDIF
		BREAK
		
		CASE MENU_STATE_CUSTOM_WASTED_TITLE_TL
			IF bDelayAFrame = FALSE
				IF DEAL_WITH_SETTING_CUSTOM_SHARD_TEXT(sFMMCendStage, g_FMMC_STRUCT.tlCustomWastedTitle[sFMMCmenu.iSelectedTeam])
					sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
					REFRESH_MENU(sFMMCmenu)
					bDelayAFrame = TRUE
				ENDIF
			ELSE
				bDelayAFrame = FALSE
			ENDIF
		BREAK
		
		CASE MENU_STATE_CUSTOM_WASTED_STRAPLINE_TL
			IF bDelayAFrame = FALSE
				IF DEAL_WITH_SETTING_CUSTOM_SHARD_TEXT(sFMMCendStage, g_FMMC_STRUCT.tlCustomWastedStrapline[sFMMCmenu.iSelectedTeam])
					sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
					REFRESH_MENU(sFMMCmenu)
					bDelayAFrame = TRUE
				ENDIF
			ELSE
				bDelayAFrame = FALSE
			ENDIF
		BREAK
		
		CASE MENU_STATE_BLIMP_MESSAGE
			IF bDelayAFrame = FALSE
				IF DEAL_WITH_SETTING_MISSION_TEXT(sFMMCendStage, FALSE, FALSE, TRUE)
					REFRESH_MENU(sFMMCMenu)
					sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
					bDelayAFrame = TRUE
					CLEANUP_BLIMP(sBlimpSign)
				ENDIF
			ELSE
				bDelayAFrame = FALSE
			ENDIF
		BREAK

		CASE MENU_STATE_CUSTOM_TEMPLATE_NAME
			IF bDelayAFrame = FALSE
				IF DEAL_WITH_SETTING_CUSTOM_TEMPLATE_NAME(sFMMCendStage, g_FMMC_STRUCT.iNumberOfPropTemplates-1)
					sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
					REFRESH_MENU(sFMMCmenu)
					bDelayAFrame = TRUE
				ENDIF
			ELSE
				bDelayAFrame = FALSE
			ENDIF
		BREAK 
		CASE MENU_STATE_SET_VEHICLE_MODEL_NAME
			IF bDelayAFrame = FALSE	
				IF DEAL_WITH_SETTING_SIMPLE_TL63(sFMMCendStage, g_FMMC_STRUCT.tl63_CachedForPlacementMenu)
					
					// Validity Checking.
					STRING sString
					sString = TEXT_LABEL_INTO_STRING(g_FMMC_STRUCT.tl63_CachedForPlacementMenu)
					MODEL_NAMES mn
					mn = INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY(sString))										
					IF NOT IS_MODEL_VALID(mn)
						g_FMMC_STRUCT.tl63_CachedForPlacementMenu = ""
					ELSE
						IF NOT IS_MODEL_A_VEHICLE(mn)
							g_FMMC_STRUCT.tl63_CachedForPlacementMenu = ""
						ENDIF
					ENDIF
					
					sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
					REFRESH_MENU(sFMMCmenu)
					bDelayAFrame = TRUE
				ENDIF
			ELSE
				bDelayAFrame = FALSE
			ENDIF
		BREAK	
		CASE MENU_STATE_ENTER_RULE_CUSTOM_NAME
			PRINTLN("MENU_STATE_ENTER_RULE_CUSTOM_NAME")
			IF bDelayAFrame = FALSE	
				IF sFMMCMenu.iSelectedTeam > -1
					IF DEAL_WITH_SETTING_SIMPLE_TL63(sFMMCendStage, g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].tl63CustomRuleNames[g_CreatorsSelDetails.iRow])										
						sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
						REFRESH_MENU(sFMMCmenu)
						bDelayAFrame = TRUE
					ENDIF
				ENDIF
			ELSE
				bDelayAFrame = FALSE
			ENDIF
		BREAK
		
		CASE MENU_STATE_SET_SPEC_OBJ_TEXT
			PRINTLN("MENU_STATE_SET_SPEC_OBJ_TEXT")
			IF bDelayAFrame = FALSE	
				IF sFMMCMenu.iSelectedTeam > -1
					IF DEAL_WITH_SETTING_SIMPLE_TL63(sFMMCendStage, g_FMMC_STRUCT.tl63_RoamingSpectatorObjectiveTextOverride[sFMMCMenu.iSelectedTeam])										
						sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
						REFRESH_MENU(sFMMCmenu)
						bDelayAFrame = TRUE
					ENDIF
				ENDIF
			ELSE
				bDelayAFrame = FALSE
			ENDIF
		BREAK
		CASE MENU_STATE_SET_SPEC_OBJ_TEXT_RULE
			PRINTLN("MENU_STATE_SET_SPEC_OBJ_TEXT_RULE")
			IF bDelayAFrame = FALSE	
				g_CreatorsSelDetails.bColouringText = TRUE
				IF g_CreatorsSelDetails.iSelectedTeam = -1
				OR g_CreatorsSelDetails.iRow = -1
				OR g_CreatorsSelDetails.iRow >= FMMC_MAX_RULES
					sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
					EXIT
				ENDIF
				BOOL bCancelled
				bCancelled = FALSE
				IF DEAL_WITH_ENTERING_OBJECTIVE_TEXT(13, bCancelled)								
					sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
					REFRESH_MENU(sFMMCmenu)
					bDelayAFrame = TRUE
				ENDIF
				IF bCancelled
					sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
					REFRESH_MENU(sFMMCmenu)
					bDelayAFrame = TRUE
				ENDIF
			ELSE
				bDelayAFrame = FALSE
			ENDIF
		BREAK
		CASE MENU_STATE_CONFIRMATION_SCREEN
			HANDLE_CONFIRMATION_SCREEN(sFMMCmenu, sCurrentVarsStruct, sFMMCendStage, sPedStruct, sTeamSpawnStruct[sFMMCMenu.iSelectedTeam], iLocalBitSet)
		BREAK
	ENDSWITCH
	
	BOOL bHideMarker
	IF NOT SHOULD_PROCESS_CURSOR()
		bHideMarker = TRUE
	ELSE
		bHideMarker = FALSE
	ENDIF
	MAINTAIN_PLACEMENT_DISC(sCurrentVarsStruct, sFMMCmenu, iLocalBitSet, sFMMCdata.iBitSet, bHideMarker, sFMMCmenu.iEntityCreation = -1)	
			
	IF g_bShow_FMMC_rulesMenu = TRUE
		IF g_CreatorsSelDetails.bMenuSetUp = FALSE
			//AUTO_POPULATE_RULES_MENU(g_CreatorsSelDetails)
			g_CreatorsSelDetails.bMenuSetUp = TRUE
		ENDIF
		
		IF DO_RULES_END_MENU()
 			g_bShow_FMMC_rulesMenu = FALSE			
			g_CreatorsSelDetails.bMenuSetUp = FALSE
		ENDIF
	ENDIF
	
	BLIP_INDEX biFurthestBlip
	IF sFMMCmenu.bZoomedOutRadar
		biFurthestBlip = GET_FURTHEST_BLIP_TO_PLAYER(sPedStruct.biPedBlip, sVehStruct.biVehicleBlip, sWepStruct.biWeaponBlip, sPropStruct.biObject, sDynoPropStruct.biObject, sStartEndBlips.biStart, bCameraPanBlip, sCurrentVarsStruct.biLocateBlip)
	ENDIF
	
	CONTROL_PLAYER_BLIP(SHOULD_HIDE_PLAYER_BLIP(), sCurrentVarsStruct, sFMMCmenu.iEntityCreation, sFMMCmenu.bZoomedOutRadar OR IS_BIT_SET(sFMMCdata.iBitSet, biTestMissionActive), biFurthestBlip)
	
	PROCESS_PLACED_ENTITY_LABELS(sPedStruct, sVehStruct, sWepStruct, sObjStruct, sDynoPropStruct, sInteractableCreationStruct, sDevAssistCreationStruct)
		
	IF IS_BIT_SET(sGetUGC_content.iAdditionalDataLoadBS, ciUGC_ADDITIONAL_DATA_LOAD_BEGIN)
		SET_ENTITY_CREATION_STATUS(STAGE_PULL_DATA_FROM_OTHER_MISSIONS_RESET)
	ENDIF
ENDPROC

FUNC BOOL SAVE_LOCAL_DATA(STRING sContentName)

	SWITCH sLocalLoadSaveStruct.eSaveState
		CASE LOCAL_SAVE_STATE_WRITE_DATA
			IF SAVEOUT_DATA(sFMMCendStage.dataStruct)
				SET_LOCAL_SAVE_STATE(sLocalLoadSaveStruct, LOCAL_SAVE_STATE_SAVE_TO_DISC)
			ENDIF
		BREAK
		
		CASE LOCAL_SAVE_STATE_SAVE_TO_DISC
			IF DATAFILE_SAVE_OFFLINE_UGC(sContentName)
				SET_LOCAL_SAVE_STATE(sLocalLoadSaveStruct, LOCAL_SAVE_STATE_SAVE_SUCCESSFUL)
			ELSE
				sLocalLoadSaveStruct.iFailedSaveErrorCheck++
				IF sLocalLoadSaveStruct.iFailedSaveErrorCheck > 10
					SET_LOCAL_SAVE_STATE(sLocalLoadSaveStruct, LOCAL_SAVE_STATE_SAVE_FAILED)
				ENDIF
			ENDIF
		BREAK
		
		CASE LOCAL_SAVE_STATE_SAVE_SUCCESSFUL
			SET_WARNING_MESSAGE("PM_SAVED", FE_WARNING_OK, "MC_H_SVS")
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
				RESET_LOCAL_LOAD_SAVE_DATA(sLocalLoadSaveStruct)
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE LOCAL_SAVE_STATE_SAVE_FAILED
			SET_WARNING_MESSAGE("FMMC_ALERT", FE_WARNING_OK, "MC_H_FTSV")
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
				RESET_LOCAL_LOAD_SAVE_DATA(sLocalLoadSaveStruct)
				RETURN TRUE
			ENDIF
		BREAK
		
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

SCRIPT

	IF IS_PAUSE_MENU_REQUESTING_TO_EDIT_A_MISSION()
		PRINTLN("[JA@PAUSEMENU] Set up which file to load into creator", GET_PAUSE_MENU_MISSION_FILE_TO_LOAD())
		sFMMCendStage.iLoadDeleteStage  = ciLOAD_DELETE_STAGE_LOAD
		sFMMCendStage.iMenuReturn		= GET_PAUSE_MENU_MISSION_FILE_TO_LOAD()
	ENDIF
	
	PROCESS_PRE_GAME()
	MISSION_CREATOR_INITIALISATION()
	
	WHILE TRUE
	
		PRE_FRAME_CREATOR_PROCESSING()
		PRE_FRAME_CREATOR_DEBUG()
		
		WAIT(0) 		
		
		#IF IS_DEBUG_BUILD
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD1)
		AND GET_COMMANDLINE_PARAM_EXISTS("sc_CreatorInventoryDebug")
			GO_TO_MENU(sFMMCmenu, sFMMCdata, eFMMC_Inventory_Base)
			REFRESH_MENU_ASAP(sFMMCmenu)
		ENDIF		
		
		IF iDebugGotoMenuASAP > 0
			GO_TO_MENU(sFMMCmenu, sFMMCdata, INT_TO_ENUM(eFMMC_MENU_ENUM, iDebugGotoMenuASAP))
			REFRESH_MENU_ASAP(sFMMCmenu)
			iDebugGotoMenuASAP = -1
		ENDIF
		#ENDIF
		
		// Prevent recording whilst in the creator
		REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
		
		DISPLAY_ALL_CURRENT_ROCKSTAR_CREATED(sFMMCendStage.sRocStarCreatedVars, sGetUGC_content)
						
		// Clean up if the cloud bails on us
		IF SHOULD_CREATOR_BAIL_DUE_TO_CLOUD_BEING_DOWN(sFMMCendStage.sClodBailStruct)
			SCRIPT_CLEANUP(TRUE)
		ENDIF
		
		IF g_TurnOnCreatorHud
		
			IF IS_LEGACY_MISSION_CREATOR()
				CREATOR_DOORS_SETUP(iDoorSlowLoop, iDoorSetupStage, FMMC_TYPE_MISSION)
			ELSE
				IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
					CREATOR_DOORS_SETUP(iDoorSlowLoop, iDoorSetupStage, FMMC_TYPE_MISSION)
				ENDIF
			ENDIF
			
			IF NOT bTestModeControllerScriptStarted			
				
				IF sFMMCmenu.bCopyPasteSwapCameras
					VECTOR vWarp = GET_POSITION_OF_ENTITY_AND_CREATION_TYPE(sFMMCmenu.iEntityCreation, sFMMCmenu.iCopyFromEntityOverride, sFMMCmenu.iSelectedTeam)
					IF IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)	
						vWarp.z += 1.0
						SET_ENTITY_COORDS(PLAYER_PED_ID(), vWarp)
					ELSE
						VECTOR vCamRot = GET_CAM_ROT(sCamData.cam)
						vWarp.z += 5.0
						SET_CAM_COORD(sCamData.cam, vWarp)
						SET_CAM_ROT(sCamData.cam, <<-85.0, 0.0, vCamRot.z>>)
						sCamData.camRot = <<-85.0, 0.0, vCamRot.z>>
					ENDIF
					sFMMCmenu.bCopyPasteSwapCameras = FALSE
				ENDIF
				
				// Fix for the time of day changing and for the weather
				FMMC_MAINTAIN_TIME_AND_WEATHER(g_FMMC_STRUCT.iTimeOfDay)
				FM_MAINTAIN_MISSION_DENSITYS_THIS_FRAME()
				
				IF IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)	
					DISABLE_ADDITIONAL_CONTROLS()
				ENDIF
			
				#IF SCRIPT_PROFILER_ACTIVE
				SCRIPT_PROFILER_START_OF_FRAME()
				#ENDIF
				
				MAINTAIN_CAMERA_ROTATION_LIMITS(sCamData, sFMMCmenu.sActiveMenu, sCurrentVarsStruct, bInitialIntroCamSetup, bInitialIntroCamWarp, sFMMCmenu.iEntityCreation)
				
				// Tracking time in Creator mode
				IF SCRIPT_IS_CLOUD_AVAILABLE()
					TRACK_TIME_IN_CREATOR_MODE()
				ENDIF
				
				UPDATE_WIDGETS()
				PROCESS_RHM_SCALING()
					
				#IF SCRIPT_PROFILER_ACTIVE 
				ADD_SCRIPT_PROFILE_MARKER("UPDATE_WIDGETS")
				#ENDIF
				
				IF g_Cellphone.PhoneDS != PDS_DISABLED				
				AND NOT IS_BIT_SET(sFMMCdata.iBitSet, biSetUpTestMission)
					DISABLE_CELLPHONE(TRUE)
				ENDIF
				
				#IF SCRIPT_PROFILER_ACTIVE 
				ADD_SCRIPT_PROFILE_MARKER("disable selector and cellphone")
				#ENDIF
				
				IF sCurrentVarsStruct.iEntityCreationStatus != STAGE_DEAL_WITH_FINISHING
				AND sCurrentVarsStruct.iEntityCreationStatus != STAGE_SET_UP_RULES_PAGE
					IF IS_PAUSE_MENU_REQUESTING_TRANSITION()
						SET_ENTITY_CREATION_STATUS(STAGE_SET_UP_RULES_PAGE)
					ENDIF
					IF IS_TRANSITION_TELLING_CREATOR_TO_CLEANUP()
						SET_ENTITY_CREATION_STATUS(STAGE_SET_UP_RULES_PAGE)
					ENDIF
				ENDIF
						
				IF SHOULD_HUD_AND_RADAR_BE_HIDDEN()
					HIDE_HUD_AND_RADAR_THIS_FRAME()
					SET_BIGMAP_ACTIVE(FALSE, FALSE)
				ELSE
					TURN_OFF_IDLE_TICKERS()
					THEFEED_HIDE_THIS_FRAME()
					IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
						SET_BIGMAP_ACTIVE(TRUE, FALSE)
					ENDIF
				ENDIF
				
				DEAL_WITH_PHOTO_PREVIEW(PPS, sFMMCendStage.sTakePhotoVars, sCurrentVarsStruct.iMenuState, SHOULD_DRAW_PHOTO_PREVIEW(), sFMMCmenu.sActiveMenu = eFmmc_PHOTO_CAM_BASE)
				DEAL_WITH_THE_PLACING_INTRO_CAM(SF_Movie_Gallery_Shutter_Index, sFMMCmenu.sActiveMenu, iIntroCamPlacementStage, bInitialIntroCamSetup, bShowMenu, sCurrentVarsStruct.iMenuState, sCurrentVarsStruct.iEntityCreationStatus)
				PROCESS_MAINTAIN_PRECISE_PLACEMENT_DEBUG(sCurrentVarsStruct)
				
				SWITCH sCurrentVarsStruct.iEntityCreationStatus
					
					CASE STAGE_PULL_DATA_FROM_OTHER_MISSIONS_RESET					
						DRAW_UGC_CONTENT_ADDITIONAL_DATA_TEXT_MAIN("Resetting Content")
						
						RESET_THE_MISSION_UP_SAFELY()
							
						IF iResetState = RESET_STATE_CLEAR
							SET_ENTITY_CREATION_STATUS(STAGE_PULL_DATA_FROM_OTHER_MISSIONS_LOAD)
						ENDIF
					BREAK
					
					CASE STAGE_PULL_DATA_FROM_OTHER_MISSIONS_LOAD						
						IF LOAD_UGC_CONTENT_ADDITIONAL_DATA(sGetUGC_content)
							SET_ENTITY_CREATION_STATUS(STAGE_PULL_DATA_FROM_OTHER_MISSIONS_CREATE)
						ELSE
							PRINTLN("STAGE_PULL_DATA_FROM_OTHER_MISSIONS_LOAD - Waiting..")
						ENDIF
					BREAK
					
					CASE STAGE_PULL_DATA_FROM_OTHER_MISSIONS_CREATE
						DRAW_UGC_CONTENT_ADDITIONAL_DATA_TEXT_MAIN("Recreating Content")
				
						IF RESET_THE_MISSION_UP_SAFELY()
							sGetUGC_content.iAdditionalDataLoadBS = 0
							SET_RESET_MISSION_STATE(RESET_STATE_FADE)
							SET_ENTITY_CREATION_STATUS(STAGE_ENTITY_PLACEMENT_SETUP)	
						ENDIF
					BREAK
					
					CASE STAGE_CHECK_TO_LOAD_CREATION
						PROCESS_CHECK_TO_LOAD_CREATION_STAGE()
					BREAK						
										
					CASE STAGE_ENTITY_PLACEMENT_LOAD
						PROCESS_PLACEMENT_LOAD()
					BREAK
					
					CASE STAGE_CG_TO_NG_WARNING
						IF g_FMMC_STRUCT.bIsUGCjobNG
						OR IS_STRING_EMPTY(g_FMMC_STRUCT.tl63MissionName)
						OR DRAW_CG_TO_NG_WARNING()
							SET_BIT(sGetUGC_content.iAdditionalDataLoadBS, ciUGC_ADDITIONAL_DATA_LOAD_DEV_NOTES)
							SET_ENTITY_CREATION_STATUS(STAGE_CHECK_TO_LOAD_CREATION_ADDITIONAL)
						ENDIF
					BREAK
						
					CASE STAGE_CHECK_TO_LOAD_CREATION_ADDITIONAL
						IF LOAD_UGC_CONTENT_ADDITIONAL_DATA(sGetUGC_content, TRUE)
							SET_ENTITY_CREATION_STATUS(STAGE_ENTITY_PLACEMENT_SETUP)
						ELSE
							PRINTLN("STAGE_CHECK_TO_LOAD_CREATION_ADDITIONAL - Waiting..")
						ENDIF
					BREAK
					
					CASE STAGE_ENTITY_PLACEMENT_SETUP			
						PROCESS_MAIN_STATE()
					BREAK
					
					CASE STAGE_DELETE_ALL
					CASE STAGE_DELETE_PEDS
					CASE STAGE_DELETE_VEHICLES
					CASE STAGE_DELETE_WEAPONS
					CASE STAGE_DELETE_PROPS
					CASE STAGE_DELETE_DYNOPROPS
					CASE STAGE_DELETE_OBJECTS
					CASE STAGE_DELETE_ZONES
					CASE STAGE_DELETE_INTERACTABLES
					CASE STAGE_DELETE_STUNT_JUMPS
					CASE STAGE_DELETE_TEAM_START
					CASE STAGE_DELETE_DOORS
					CASE STAGE_DELETE_DIALOGUE_TRIGGERS
					CASE STAGE_DELETE_REMOVED_FIXTURES
						DO_CONFIRMATION_MENU(sPedStruct, sTeamSpawnStruct, sVehStruct, sWepStruct, sObjStruct, sPropStruct, sDynoPropStruct, sLocStruct, sCurrentVarsStruct, sFMMCmenu, sInvisibleObjects, sFMMCEndStage, ButtonPressed, iLocalBitSet)
					BREAK
					
					//Deal with the End Menu
					CASE STAGE_ENTITY_PLACEMENT_END_MENU
					
						//Deal with the end menu
						PRINTLN("Setting iEntityCreation = -1  loc2")
						SET_CREATION_TYPE(sFMMCMenu, CREATION_TYPE_NONE)
						sCurrentVarsStruct.iMenuState = 0
						MAINTAIN_FMMC_CAMERA(sFMMCdata, sCamData, sCurrentVarsStruct.iMenuState)
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							REMOVE_ALL_PED_WEAPONS(PLAYER_PED_ID())
						ENDIF
						
						IF DO_END_MENU()
				 			SET_ENTITY_CREATION_STATUS(STAGE_SET_UP_RULES_PAGE)
							#IF IS_DEBUG_BUILD
							PRINT_FMMC_DEBUGDATA(FALSE)	
							#ENDIF
						ENDIF
					BREAK
					
					CASE STAGE_CLOUD_FAILURE	
						FMMC_DRAW_CLOUD_FAIL_WARNING(bCreatorLimitedCloudDown, bSignedOut)
						IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
							SET_ENTITY_CREATION_STATUS(STAGE_ENTITY_PLACEMENT_SETUP)
							REFRESH_MENU(sFMMCmenu)
						ENDIF
					BREAK
					
					//Deal With LOADING a previous mission
					CASE STAGE_DEAL_WITH_LOADING
					
						IF SET_SKYSWOOP_UP(TRUE)
							IF RESET_THE_MISSION_UP_SAFELY()
								SET_RESET_MISSION_STATE(RESET_STATE_FADE)
								IF sFMMCmenu.iRadioState = FMMC_RADIO_MAX_OPTIONS_STATE
									SET_MOBILE_RADIO_ENABLED_DURING_GAMEPLAY(TRUE)
									SET_MOBILE_PHONE_RADIO_STATE(TRUE)
								ENDIF			
								CLEAR_HELP()
								
								//Clean up
								REFRESH_MENU(sFMMCmenu)
								
								SET_ENTITY_CREATION_STATUS(STAGE_ENTITY_PLACEMENT_LOAD)
								sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
								
								WARP_CAMERA_TO_START_LOCATION(sCamData, 110.0)
								
								SET_FMMC_GLOBALS_TO_MENU_OPTIONS()
								
								sCurrentVarsStruct.creationStats.bEditingACreation = TRUE
								
								#IF IS_DEBUG_BUILD
								IF g_bFMMC_EnteredLoadFromPack
									sCurrentVarsStruct.creationStats.bEditingACreation = FALSE
									PRINTLN("STAGE_DEAL_WITH_LOADING - Clearing sCurrentVarsStruct.creationStats.bEditingACreation as we have accessed content through 'Load from Pack' menu")
								ENDIF
								#ENDIF
								
								INT iTeam
								FOR iTeam = 0 TO FMMC_MAX_TEAMS - 1									
									if iTeam >= g_FMMC_STRUCT.iMaxNumberOfTeams	
										PRINTLN("iTeam = ", iTeam, " AND IS GREATER THAN THE MAX = ", g_FMMC_STRUCT.iMaxNumberOfTeams)
										g_FMMC_STRUCT.sFMMCEndConditions[iTeam].bOkToDrawStartPosCorona = FALSE
										g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vStartPos = g_FMMC_STRUCT.vStartPos
									ELSE
										IF ARE_VECTORS_EQUAL(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vStartPos, g_FMMC_STRUCT.vStartPos)
											PRINTLN("iTeam = ", iTeam, " AND IS SAME VECTOR AS TRIGGER")
											g_FMMC_STRUCT.sFMMCEndConditions[iTeam].bOkToDrawStartPosCorona = FALSE
										ELSE
											PRINTLN("iTeam = ", iTeam, " AND NOT SAME SO THE BOOL IS TRUE")
											g_FMMC_STRUCT.sFMMCEndConditions[iTeam].bOkToDrawStartPosCorona = TRUE
										ENDIF
									ENDIF
								ENDFOR	
								
								sFMMCmenu.iSelectedTab = 1
								iSelectedTabOld = 0
								
																
								CLEAR_BIT(sFMMCdata.iBitSet, biTestMissionActive)
								CLEAR_BIT(sFMMCdata.iBitSet, bTestHelpTextDone)
								SET_BIT(sFMMCdata.iBitSet, bCameraActive)
								bMpNeedsCleanedUp = FALSE
								bTestModeControllerScriptStarted = FALSE
								
								bTextSetUp = FALSE
								
								bMpModeCleanedUp = FALSE	
								
								#IF IS_DEBUG_BUILD
								MAINTAIN_ENTITY_DETAIL_EXPORT(g_FMMC_STRUCT.bOutputEntityDetailsOnLoad)
								#ENDIF
								
								IF IS_LOADING_ICON_ACTIVE()
									SET_LOADING_ICON_INACTIVE()
								ENDIF
								
								DO_SCREEN_FADE_IN(1000)
								
							ENDIF
						ENDIF	
					BREAK
					
					//Deal with the rules page
					CASE STAGE_SET_UP_RULES_PAGE
						IF DO_RULES_END_MENU()
				 			SET_ENTITY_CREATION_STATUS(STAGE_DEAL_WITH_FINISHING)				
							#IF IS_DEBUG_BUILD
							PRINT_FMMC_DEBUGDATA(FALSE)	
							#ENDIF
						ENDIF
					BREAK
					
					//Deal With ENDING THE the FMMC Script
					CASE STAGE_DEAL_WITH_SAVING	
					
						IF bContentReadyForUGC = FALSE
							SET_TEAM_SPAWN_VEHICLE_DATA_FROM_CREATOR_SELECTION(sVehStruct)
							SET_MENU_OPTIONS_TO_FMMC_GLOBALS()
							bContentReadyForUGC = TRUE
							PRINTLN("SET_MENU_OPTIONS_TO_FMMC_GLOBALS Done. Time to Save")
						ELSE
							//New save method
							#IF IS_DEBUG_BUILD
							IF g_buseNewUgcSaveSystem
							
							ELSE
							#ENDIF
							
							LIMIT_ASSOCIATED_RULES_TO_NUMBER_OF_TEAMS()
							REMOVE_EXTRA_LINKED_DOORS_FROM_RULES()
							
							//Old save method
							
							SORT_PLANNING_MISSIONS()
							
							IF SAVE_THIS_CREATION(sGetUGC_content, sFMMCendStage, sCurrentVarsStruct.creationStats)
								#IF IS_DEBUG_BUILD
								PRINT_FMMC_DEBUGDATA(FALSE)	
								#ENDIF
								SET_ENTITY_CREATION_STATUS(STAGE_ENTITY_PLACEMENT_SETUP)
								sFMMCendStage.iEndStage = ciFMMC_END_STAGE_SET_UP
								SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
								sCurrentVarsStruct.bSwitching = FALSE
								sCurrentVarsStruct.stiScreenCentreLOS = NULL
								sFMMCendStage.iEndStage = 0
								sFMMCendStage.dataStruct.iLoadStage = 0
								sFMMCendStage.iPublishConformationStage = 0
								sFMMCendStage.iPublishStage = 0
								sFMMCendStage.iButtonBitSet = 0
								DO_SCREEN_FADE_IN(200)
								IF NOT IS_STRING_NULL_OR_EMPTY(sFMMCendStage.tl23ReturnedContexID)
									sFMMCendStage.tl23LoadedContexID = sFMMCendStage.tl23ReturnedContexID						
									// Removed for: 2162073/2162036 CREATOR_WRITE_TO_LEADERBOARD(sCurrentVarsStruct.creationStats, sFMMCendStage.tl23ReturnedContexID)
								ENDIF
								//sCurrentVarsStruct.creationStats.bEditingACreation = true
								IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSet, ciDEFAULT_XP)
									g_FMMC_STRUCT.iXPReward = 0
									PRINTLN("ciDEFAULT_XP RESET TO = ", g_FMMC_STRUCT.iXPReward)
								ENDIF
								bContentReadyForUGC = FALSE
							ENDIF
							#IF IS_DEBUG_BUILD
							ENDIF
							#ENDIF
							IF sFMMCendStage.oskStatus = OSK_CANCELLED
								sFMMCendStage.oskStatus = OSK_PENDING
								sFMMCendStage.iKeyBoardStatus = 0
								SET_ENTITY_CREATION_STATUS(STAGE_ENTITY_PLACEMENT_SETUP)
								sFMMCendStage.iEndStage = ciFMMC_END_STAGE_SET_UP
								SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
								sCurrentVarsStruct.bSwitching = FALSE
								sCurrentVarsStruct.stiScreenCentreLOS = NULL
								DO_SCREEN_FADE_IN(200)
								IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSet, ciDEFAULT_XP)
									g_FMMC_STRUCT.iXPReward = 0
									PRINTLN("ciDEFAULT_XP RESET TO = ", g_FMMC_STRUCT.iXPReward)
								ENDIF
								bContentReadyForUGC = FALSE
								#IF IS_DEBUG_BUILD
								PRINTLN("oskStatus = OSK_CANCELLED")
								#ENDIF
							ENDIF
						ENDIF
					BREAK	
					
					//Deal With ENDING THE the FMMC Script
					CASE STAGE_ENTITY_PLACEMENT_PUBLISH
						IF bContentReadyForUGC = FALSE
							SET_TEAM_SPAWN_VEHICLE_DATA_FROM_CREATOR_SELECTION(sVehStruct)
							SET_MENU_OPTIONS_TO_FMMC_GLOBALS()
							CLEAR_BIT(g_FMMC_STRUCT.iMissionTypeBitSet, ciMISSION_OPTION_BS_MISSION_IS_NOT_PLAYABLE)
							bContentReadyForUGC = TRUE
							PRINTLN("SET_MENU_OPTIONS_TO_FMMC_GLOBALS Done. Time to Publish")
						ELSE
							SORT_PLANNING_MISSIONS()
							IF PUBLISH_THIS_CREATION(sGetUGC_content, sFMMCendStage, sCurrentVarsStruct.creationStats)
								PRINTLN("WE HAVE FINISHED PUBLISHING. TIME TO GET OUT OF HERE.")
								sFMMCendStage.iEndStage = 0
								sFMMCendStage.dataStruct.iLoadStage = 0
								sFMMCendStage.iPublishConformationStage = 0
								sFMMCendStage.iPublishStage = 0
								sFMMCendStage.iButtonBitSet = 0
								
								#IF IS_DEBUG_BUILD
								PRINT_FMMC_DEBUGDATA(FALSE)	
								#ENDIF
					 			SET_ENTITY_CREATION_STATUS(STAGE_ENTITY_PLACEMENT_SETUP)	
								sFMMCendStage.iEndStage = ciFMMC_END_STAGE_SET_UP
								SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
								sCurrentVarsStruct.bSwitching = FALSE
								sCurrentVarsStruct.stiScreenCentreLOS = NULL
								IF sCurrentVarsStruct.creationStats.bSuccessfulSave = TRUE
									IF NOT IS_STRING_NULL_OR_EMPTY(sFMMCendStage.tl23ReturnedContexID)
										sFMMCendStage.tl23LoadedContexID = sFMMCendStage.tl23ReturnedContexID						
										// Removed for: 2162073/2162036 CREATOR_WRITE_TO_LEADERBOARD(sCurrentVarsStruct.creationStats, sFMMCendStage.tl23ReturnedContexID)
									ENDIF
								ENDIF
								
								bContentReadyForUGC = FALSE
								IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSet, ciDEFAULT_XP)
									g_FMMC_STRUCT.iXPReward = 0
									PRINTLN("ciDEFAULT_XP RESET TO = ", g_FMMC_STRUCT.iXPReward)
								ENDIF
								CLEAR_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_SAVE)
								REFRESH_MENU(sFMMCmenu)
								DO_SCREEN_FADE_IN(200)
							ENDIF
							IF sFMMCendStage.oskStatus = OSK_CANCELLED
								sFMMCendStage.oskStatus = OSK_PENDING
								sFMMCendStage.iKeyBoardStatus = 0
								SET_ENTITY_CREATION_STATUS(STAGE_ENTITY_PLACEMENT_SETUP)
								sFMMCendStage.iEndStage = ciFMMC_END_STAGE_SET_UP
								SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
								sCurrentVarsStruct.bSwitching = FALSE
								sCurrentVarsStruct.stiScreenCentreLOS = NULL
								bContentReadyForUGC = FALSE
								DO_SCREEN_FADE_IN(200)
								IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSet, ciDEFAULT_XP)
									g_FMMC_STRUCT.iXPReward = 0
									PRINTLN("ciDEFAULT_XP RESET TO = ", g_FMMC_STRUCT.iXPReward)
								ENDIF
								#IF IS_DEBUG_BUILD
								PRINTLN("oskStatus = OSK_CANCELLED")
								#ENDIF					
							ENDIF
						ENDIF
					BREAK
								
					//Deal With ENDING THE the FMMC Script
					CASE STAGE_DEAL_WITH_FINISHING 
					
						IF IS_TRANSITION_TELLING_CREATOR_TO_CLEANUP()	
							DO_SCREEN_FADE_IN(200)
							PRINTLN("[JA@PAUSEMENU] Clean up mission creator due to pause menu requesting transition, or just a normal quit ")
							SCRIPT_CLEANUP(TRUE)
						ELIF DEAL_WITH_FINISHING_CREATOR(sFMMCendStage)		
							DO_SCREEN_FADE_IN(200)	
							PRINTLN("[END CREATOR] DONE")
							SCRIPT_CLEANUP(FALSE)
						ENDIF
						
					BREAK
					
					CASE STAGE_DEAL_WITH_LOCAL_SAVING
						IF bContentReadyForUGC = FALSE
							SET_TEAM_SPAWN_VEHICLE_DATA_FROM_CREATOR_SELECTION(sVehStruct)
							SET_MENU_OPTIONS_TO_FMMC_GLOBALS()
							bContentReadyForUGC = TRUE
						ELSE
							IF SAVE_LOCAL_DATA(g_FMMC_STRUCT.tl63MissionName)
								SET_ENTITY_CREATION_STATUS(STAGE_ENTITY_PLACEMENT_SETUP)
								bContentReadyForUGC = FALSE
							ENDIF
						ENDIF
					BREAK
					
					CASE STAGE_DEAL_WITH_LOCAL_LOADING
						IF NOT sLocalLoadSaveStruct.bLocalLoadCancelled
							IF NOT IS_STRING_NULL_OR_EMPTY(sLocalLoadSaveStruct.tlContentToLoad)
								IF LOAD_LOCAL_FMMC_CONTENT_DATA(sLocalLoadSaveStruct.tlContentToLoad, sLocalLoadSaveStruct)
									PROCESS_POST_LOAD_DATA_SETUP()
								ENDIF
							ENDIF
						ELSE
							SET_ENTITY_CREATION_STATUS(STAGE_ENTITY_PLACEMENT_SETUP)
						ENDIF
					BREAK
					
					CASE STAGE_DEAL_WITH_RECREATING_MISSION
						IF NOT IS_SCREEN_FADING_OUT()	
							IF RESET_THE_MISSION_UP_SAFELY()
								SET_RESET_MISSION_STATE(RESET_STATE_FADE)
								IF sFMMCmenu.iRadioState = FMMC_RADIO_MAX_OPTIONS_STATE
									SET_MOBILE_RADIO_ENABLED_DURING_GAMEPLAY(TRUE)
									SET_MOBILE_PHONE_RADIO_STATE(TRUE)
								ENDIF
								CLEAR_HELP()
								CLEAR_BIT(sFMMCdata.iBitSet, biTestMissionActive)
								CLEAR_BIT(sFMMCdata.iBitSet, biSetUpTestMission)	
								//Clean up
								
								REFRESH_MENU(sFMMCmenu)
								SORT_PLANNING_MISSIONS()
								SET_ENTITY_CREATION_STATUS(STAGE_ENTITY_PLACEMENT_SETUP)
								sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
								
								DO_SCREEN_FADE_IN(500)	
								SET_SKYBLUR_CLEAR()
								PRINTLN("DO SCREEN FADE IN")
								
								UNLOCK_MENU_ITEMS_AND_ALLOW_MISSION_TEST()
								ENABLE_ALL_MP_HUD()
								CLEAR_BIT(sFMMCdata.iBitSet, bTestHelpTextDone)
								SET_BIT(sFMMCdata.iBitSet, bCameraActive)
								bMpNeedsCleanedUp = FALSE
								bTestModeControllerScriptStarted = FALSE
								bTextSetUp = FALSE
								bMpModeCleanedUp = FALSE
								
							ENDIF
						ENDIF
					BREAK
					
				ENDSWITCH
				
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE 
					ADD_SCRIPT_PROFILE_MARKER("process main logic")
				#ENDIF
				#ENDIF
				
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE 
					ADD_SCRIPT_PROFILE_MARKER("PLACE_STUNTJUMPS")
				#ENDIF
				#ENDIF
				
				//Kill this script
				#IF IS_DEBUG_BUILD	
					IF IS_DEBUG_KEY_JUST_PRESSED(KEY_NUMPAD1, KEYBOARD_MODIFIER_SHIFT, "TURN ON MISSION CREATOR DEBUG")
						g_bRunMissionCreatorInDebugMode = TRUE
					ENDIF				
					
					Maintain_Hud_Creator_Tool(wgGroup)
					
					IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
						IF IS_FAKE_MULTIPLAYER_MODE_SET() = FALSE
							IF NETWORK_IS_GAME_IN_PROGRESS() = FALSE
								SET_ENTITY_CREATION_STATUS(STAGE_ENTITY_PLACEMENT_END_MENU)
								//SCRIPT_CLEANUP(TRUE)
							ELSE
								PRINTLN("NETWORK_IS_GAME_IN_PROGRESS() ")
							ENDIF
						ELSE
							PRINTLN("IS_FAKE_MULTIPLAYER_MODE_SET()")
						ENDIF
					ENDIF
				#ENDIF
				
				IF g_CreatorsSelDetails.iMaxNumberOfTeams != g_FMMC_STRUCT.iMaxNumberOfTeams 
					g_CreatorsSelDetails.iMaxNumberOfTeams = g_FMMC_STRUCT.iMaxNumberOfTeams 
					PRINTLN("g_CreatorsSelDetails.iMaxNumberOfTeams has changed, it is now = ", g_FMMC_STRUCT.iMaxNumberOfTeams)
				ENDIF
				
			ELSE
				IF NETWORK_IS_GAME_IN_PROGRESS()
					MAINTAIN_MP_AMBIENT_MANAGER()
					IF bTextSetUp = TRUE
						Maintain_MP_Objective_Text()
						Maintain_MP_Communications()
					ENDIF
					HANDLE_FORCE_PLAYERS_FROM_CAR(PLAYER_ID())
					MAINTAIN_FORCE_OUT_OF_VEHICLE()
					#IF IS_DEBUG_BUILD	
						MAINTAIN_J_SKIP_WARP()
					#ENDIF
					RENDER_INVENTORY_HUD(on_mission_gang_box)
					MAINTAIN_BIG_MESSAGE() 
					PROCESS_LEADERBOARD_CAM()
				ENDIF
			ENDIF
		ENDIF
		
		MANAGE_OPTION_PRESSED_TIMER(sFMMCmenu)
		
		PROCESS_ENTERING_MENU_STATE_VECTOR()
		PROCESS_ENTERING_MENU_STATE_VECTOR_DATA()
		
		PROCESS_ENTERING_MENU_STATE_ON_SCREEN_POPUP_WINDOW_DATA()
		PROCESS_ENTERING_MENU_STATE_ON_SCREEN_POPUP_WINDOW()
		
		IF IS_BIT_SET(sFMMCdata.iBitSet, bMenuJustChangedPlacementBlocker)
			PRINTLN("[LM] - Creator Bit - Clearing bMenuJustChangedPlacementBlocker")
			CLEAR_BIT(sFMMCdata.iBitSet, bMenuJustChangedPlacementBlocker)
		ENDIF
		
		CLEAR_BIT(sCurrentVarsStruct.iCreationBitset, ciCREATION_VARS_BS_HIDE_PLACEMENT_ENTITY)
		
		//If we've been signed out the wait for Brenda to dealwith cleanup
		IF g_bKillFMMC_SignedOut
			SCRIPT_CLEANUP(TRUE)
			PRINTLN("g_bKillFMMC_SignedOut = TRUE")
		ENDIF
		
		IF sCurrentVarsStruct.iEntityCreationStatus != STAGE_DEAL_WITH_RECREATING_MISSION
			MAINTAIN_TEST_MISSION_STATE()
		ENDIF

		#IF IS_DEBUG_BUILD
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD2)
				sFMMCendStage.oskStatus = OSK_CANCELLED
				PRINT_PLANNING_MISSIONS(g_FMMC_STRUCT.sPlanningMissions)
			ENDIF
			
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD4)
				SORT_PLANNING_MISSIONS()
			ENDIF
			
			IF IS_DEBUG_KEY_JUST_RELEASED(KEY_X, KEYBOARD_MODIFIER_CTRL_SHIFT, "Exploder Creator")
				g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_EXPLODER
			ENDIF
			IF IS_3D_EXPLODER_CREATOR()
				DRAW_DEBUG_TEXT_2D("3D Exploder Creator", <<0.8,0.8,0.0>>)
			ENDIF
			
			#IF IS_DEBUG_BUILD
			MAINTAIN_ENTITY_DETAIL_EXPORT()
			MAINTAIN_ENTITY_DETAIL_EXPORT_ALTERNATE(sVehStruct)
			MAINTAIN_ENTITY_DETAIL_EXPORT_PLACEHOLDER(sVehStruct)
			#ENDIF
			
			#IF SCRIPT_PROFILER_ACTIVE
			SCRIPT_PROFILER_END_OF_FRAME()
			#ENDIF
			
			IF g_bDisableCreatorForceTest
				sCurrentVarsStruct.creationStats.bMadeAChange = FALSE
				sCurrentVarsStruct.creationStats.iTimesTestedLoc = 1
			ENDIF
		#ENDIF
		
	ENDWHILE
ENDSCRIPT

#ENDIF
