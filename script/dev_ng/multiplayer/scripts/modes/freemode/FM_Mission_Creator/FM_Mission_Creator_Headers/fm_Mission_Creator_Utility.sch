#IF IS_DEBUG_BUILD
USING "FM_Mission_Creator_Using.sch"
USING "FM_Mission_Creator_Debug.sch"
USING "FM_Mission_Creator_Menu_Utility.sch"

FUNC INT GET_NEXT_SET_BIT_IN_BITSET(INT &iBitSet[], INT iStart, BOOL bUp)
	
	INT i
	INT iMaxIndex = COUNT_OF(iBitSet)*32
	
	IF bUp
		FOR i = iStart TO iMaxIndex-1
			IF IS_BIT_SET(iBitset[i/32], i%32)
				RETURN i
			ENDIF
		ENDFOR
	ELSE
		FOR i = iStart TO 0 STEP -1
			IF IS_BIT_SET(iBitset[i/32], i%32)
				RETURN i
			ENDIF
		ENDFOR
	ENDIF
	
	RETURN -1
	
ENDFUNC

FUNC INT GET_NTH_BIT_SET_IN_DOUBLE_BITSET(INT n, INT iBitset1, INT iBitset2)
	PRINTLN("PD NTH BIT SET - input is ", n)
	INT iBitsetFull[2]
	iBitSetFull[0] = iBitset1
	iBitSetFull[1] = iBitset2
	INT i, iSum = 0
	REPEAT 64 i
		IF IS_BIT_SET(iBitSetFull[i/32], i%32)
			iSum++
			IF iSum = n
				PRINTLN("PD NTH BIT SET - return is ", i)
				RETURN i
			ENDIF
		ENDIF
	ENDREPEAT
	PRINTLN("PD NTH BIT SET - return is ", -1)
	RETURN -1
ENDFUNC

FUNC BOOL SHOULD_HUD_AND_RADAR_BE_HIDDEN()

	IF sFMMCMenu.sActiveMenu = eFmmc_TOP_MENU
	OR sFMMCMenu.sActiveMenu = eFmmc_MAIN_OPTIONS_BASE
	OR sFMMCMenu.sActiveMenu = eFmmc_RADIO_MENU
	OR sFMMCMenu.sActiveMenu = eFmmc_Number_Of_lives_item
	OR sFMMCMenu.sActiveMenu = eFmmc_TEAM_RESPAWN_OPTIONS
	OR sFMMCMenu.sActiveMenu = eFmmc_SYNOPSIS
	OR sFMMCMenu.sActiveMenu = eFmmc_STARTING_TEAM_INVENTORY
	OR sFMMCMenu.sActiveMenu = eFmmc_MIDMISSION_TEAM_INVENTORY
	OR sFMMCMenu.sActiveMenu = efmmc_UNARMED_ANIM_ON_CHECKPOINT
	OR sFMMCMenu.sActiveMenu = eFmmc_SMS_OPTIONS
	OR sFMMCMenu.sActiveMenu = eFmmc_TEAM_RELATIONSHIP_OPTION
	OR sFMMCMenu.sActiveMenu = eFMMC_AVAILABLE_VEHICLE_LIBRARIES
	OR sFMMCMenu.sActiveMenu = eFMMC_DEFAULT_VEHICLE_LIBRARIES
	OR sFMMCMenu.sActiveMenu = eFMMC_AVAILABLE_VEHICLES
	OR sFMMCMenu.sActiveMenu = eFmmc_EARN_POINTS
	OR sFMMCMenu.sActiveMenu = eFmmc_TEAM_DETAILS
	OR sFMMCMenu.sActiveMenu = eFMMC_TEAM_STREAMING
	OR sFMMCMenu.sActiveMenu = eFMMC_TEAM_SHOW_TEAM_SCOREBOARD
	OR sFMMCMenu.sActiveMenu = eFMMC_TEAM_SHOW_LAPS_HUD
	OR sFMMCMenu.sActiveMenu = eFMMC_TEAM_DRAW_RACE_POSITION
	OR sFMMCMenu.sActiveMenu = eFMMC_TEAM_CROSS_TEAM_CHAT
	OR sFMMCMenu.sActiveMenu = eFMMC_TEAM_CROSS_TEAM_MARKER_VISIBILITY
	OR sFMMCMenu.sActiveMenu = eFmmc_TEAM_OUTFIT_LIST
	OR sFMMCMenu.sActiveMenu = eFmmc_TEAM_OUTFIT_STYLES_LIST
	OR sFMMCMenu.sActiveMenu = eFmmc_TEAM_DEFAULT_MASKS
	OR sFMMCMenu.sActiveMenu = eFmmc_TEAM_MASKS_LIST
	OR sFMMCMenu.sActiveMenu = eFmmc_CNC_OPTIONS
	OR sFMMCMenu.sActiveMenu = eFMMC_TEAM_SETTINGS
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_PROCESS_CURSOR()
	
	IF IS_PLAYER_SWITCH_IN_PROGRESS()
		RETURN FALSE
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_PAN_CAM_BASE
	OR sFMMCMenu.sActiveMenu = eFmmc_PHOTO_CAM_BASE
	OR sFMMCMenu.sActiveMenu = eFmmc_DM_TEAM_CAMERAS
	OR sFMMCMenu.sActiveMenu = eFmmc_SET_ELEVATOR_CAM
	OR sFMMCMenu.sActiveMenu = efMMC_CUTSCENE_SET_CAM_1
	OR sFMMCMenu.sActiveMenu = efMMC_CUTSCENE_SET_CAM_2
	OR sFMMCMenu.sActiveMenu = efMMC_CUTSCENE_SET_CAM_3
	OR sFMMCMenu.sActiveMenu = efMMC_TRIP_SKIP_CAM
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_HIDE_PLAYER_BLIP()

	IF IS_BIT_SET(sFMMCdata.iBitSet, biTestMissionActive)
		RETURN TRUE
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_PAN_CAM_BASE
	OR sFMMCMenu.sActiveMenu = eFmmc_PHOTO_CAM_BASE
	OR sFMMCMenu.sActiveMenu = eFmmc_DM_TEAM_CAMERAS
	OR sFMMCMenu.sActiveMenu = eFmmc_MAIN_OPTIONS_BASE
	OR sFMMCMenu.sActiveMenu = eFmmc_TOP_MENU
	OR sFMMCMenu.sActiveMenu = eFmmc_RADIO_MENU
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_MC_SPAWN_GROUP_OVERRIDES()
	INT i = 0
	INT ii = 0
	FOR	i = 0 TO FMMC_MAX_RANDOM_ENTITY_SPAWN_GROUPS-1
		IF bBoolTemp[i]
			SET_BIT(g_iMissionForcedSpawnGroup, i)
		ELSE
			CLEAR_BIT(g_iMissionForcedSpawnGroup, i)
		ENDIF
		FOR	ii = 0 TO ciMAX_NUM_FMMC_ENTITY_SUB_SPAWN_GROUPS-1
			IF i < ciMAX_NUM_FMMC_ENTITY_SUB_SPAWN_GROUPS
				IF bBoolSubTemp[i][ii]
					SET_BIT(g_iMissionForcedSpawnSubGroup[i], ii)
				ELSE
					CLEAR_BIT(g_iMissionForcedSpawnSubGroup[i], ii)
				ENDIF
			ENDIF
		ENDFOR
	ENDFOR
	
	IF bBoolTempClear
		g_iMissionForcedSpawnGroup = 0		
		FOR i = 0 TO ciMAX_NUM_FMMC_ENTITY_SUB_SPAWN_GROUPS-1			
			g_iMissionForcedSpawnSubGroup[i] = 0
		ENDFOR
		FOR	i = 0 TO FMMC_MAX_RANDOM_ENTITY_SPAWN_GROUPS-1
			bBoolTemp[i] = FALSE				
			FOR	ii = 0 TO ciMAX_NUM_FMMC_ENTITY_SUB_SPAWN_GROUPS-1
				bBoolSubTemp[i][ii] = FALSE
			ENDFOR
		ENDFOR
		bBoolTempClear = FALSE
	ENDIF	
ENDPROC

PROC MOVE_CUSTOM_RULE_NAMES_DOWN_ONE(INT iPriority, INT iTeam)
	INT iCount
	FOR iCount = iPriority TO (FMMC_MAX_RULES - 2)
		g_FMMC_STRUCT.sFMMCEndConditions[iTeam].tl63CustomRuleNames[iCount] = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].tl63CustomRuleNames[iCount + 1]
	ENDFOR
ENDPROC
PROC MOVE_PED_RULES_DOWN_ONE(INT iPriority, INT iTeam)
	INT iCount
	FOR iCount = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfPeds - 1)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iCount].iPriority[iTeam] > iPriority
		AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iCount].iPriority[iTeam] != FMMC_PRIORITY_IGNORE
			g_FMMC_STRUCT_ENTITIES.sPlacedPed[iCount].iPriority[iTeam]--
			PRINTLN("g_FMMC_STRUCT_ENTITIES.sPlacedPed[", iCount, "].iPriority[", iTeam, "] = ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iCount].iPriority[iTeam])
		ENDIF
	ENDFOR
ENDPROC
PROC MOVE_VEHICLE_RULES_DOWN_ONE(INT iPriority, INT iTeam)
	INT iCount
	FOR iCount = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles - 1)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iCount].iPriority[iTeam] > iPriority
		AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iCount].iPriority[iTeam] != FMMC_PRIORITY_IGNORE
			g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iCount].iPriority[iTeam]--
			PRINTLN("g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[", iCount, "].iPriority[", iTeam, "] = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iCount].iPriority[iTeam])
		ENDIF
	ENDFOR
ENDPROC
PROC MOVE_OBJECT_RULES_DOWN_ONE(INT iPriority, INT iTeam)
	INT iCount
	FOR iCount = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfObjects - 1)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iCount].iPriority[iTeam] > iPriority
		AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[iCount].iPriority[iTeam] != FMMC_PRIORITY_IGNORE
			g_FMMC_STRUCT_ENTITIES.sPlacedObject[iCount].iPriority[iTeam]--
			PRINTLN("g_FMMC_STRUCT_ENTITIES.sPlacedObject[", iCount, "].iPriority[", iTeam, "] = ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[iCount].iPriority[iTeam])
		ENDIF
	ENDFOR
ENDPROC
PROC MOVE_GOTO_RULES_DOWN_ONE(INT iPriority, INT iTeam)
	INT iCount
	FOR iCount = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfGoToLocations[0] - 1)
		IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iCount].iPriority[iTeam] > iPriority
		AND g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iCount].iPriority[iTeam] != FMMC_PRIORITY_IGNORE
			g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iCount].iPriority[iTeam]--
			PRINTLN("g_FMMC_STRUCT_ENTITIES.sGotoLocationData[", iCount, "].iPriority[", iTeam, "] = ", g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iCount].iPriority[iTeam])
		ENDIF
	ENDFOR
ENDPROC
PROC MOVE_PLAYER_RULES_DOWN_ONE(INT iPriority, INT iTeam)
	INT iCount
	FOR iCount = 0 TO (g_FMMC_STRUCT.iNumberOfPlayerRules[iTeam] - 1)
		IF g_FMMC_STRUCT.sPlayerRuleData[iCount].iPriority[iTeam] > iPriority
		AND g_FMMC_STRUCT.sPlayerRuleData[iCount].iPriority[iTeam] != FMMC_PRIORITY_IGNORE
			g_FMMC_STRUCT.sPlayerRuleData[iCount].iPriority[iTeam]--
			PRINTLN("g_FMMC_STRUCT.sPlayerRuleData[", iCount, "].iPriority[", iTeam, "] = ", g_FMMC_STRUCT.sPlayerRuleData[iCount].iPriority[iTeam])
		ENDIF
	ENDFOR
ENDPROC
PROC MOVE_OTHER_RULES_OF_THIS_TYPE_DOWN_ONE(SELECTION_DETAILS_STRUCT &g_CreatorsSelDetailsPassed, INT iRow, INT iTeam)
	SWITCH GET_RULE_TYPE_FROM_RULE(g_CreatorsSelDetailsPassed.sTeamOptions[iTeam].sSelection[iRow].iRule)
		CASE ciRULE_TYPE_PED
			MOVE_PED_RULES_DOWN_ONE(iRow, iTeam)
		BREAK
		CASE ciRULE_TYPE_VEHICLE
			MOVE_VEHICLE_RULES_DOWN_ONE(iRow, iTeam)
		BREAK
		CASE ciRULE_TYPE_OBJECT
			MOVE_OBJECT_RULES_DOWN_ONE(iRow, iTeam)
		BREAK
		CASE ciRULE_TYPE_GOTO
			MOVE_GOTO_RULES_DOWN_ONE(iRow, iTeam)
		BREAK
		CASE ciRULE_TYPE_GET_MASKS
		CASE ciRULE_TYPE_PLAYER
			MOVE_PLAYER_RULES_DOWN_ONE(iRow, iTeam)
		BREAK
		DEFAULT
			PRINTLN("MOVE_OTHER_RULES_OF_THIS_TYPE_DOWN_ONE - default")
		BREAK
	ENDSWITCH
ENDPROC

// YACHT MANAGER //
PROC PROCESS_FMMC_YACHT(INT iYachtIndex)
	
	IF IS_FAKE_MULTIPLAYER_MODE_SET()
	OR g_FMMC_STRUCT.sMissionYachtInfo[iYachtIndex].iMYacht_ModelIndex = ciYACHT_MODEL__OFF
		EXIT
	ENDIF
	
	TEXT_LABEL_63 tlDebugText3D = "Y"
	tlDebugText3D += iYachtIndex
	tlDebugText3D += " | "
	
	sMissionYachtData[iYachtIndex].iLocation = (g_FMMC_STRUCT.sMissionYachtInfo[iYachtIndex].iMYacht_AreaIndex * 3) + g_FMMC_STRUCT.sMissionYachtInfo[iYachtIndex].iMYacht_PositionIndex
	sMissionYachtData[iYachtIndex].Appearance.iOption = g_FMMC_STRUCT.sMissionYachtInfo[iYachtIndex].iMYacht_ModelIndex
	sMissionYachtData[iYachtIndex].Appearance.iTint = g_FMMC_STRUCT.sMissionYachtInfo[iYachtIndex].iMYacht_ColourIndex
	sMissionYachtData[iYachtIndex].Appearance.iLighting = g_FMMC_STRUCT.sMissionYachtInfo[iYachtIndex].iMYacht_LightingIndex
	sMissionYachtData[iYachtIndex].Appearance.iRailing = g_FMMC_STRUCT.sMissionYachtInfo[iYachtIndex].iMYacht_FittingsIndex
	sMissionYachtData[iYachtIndex].Appearance.iFlag = g_FMMC_STRUCT.sMissionYachtInfo[iYachtIndex].iMYacht_FlagIndex
	
	MP_PROP_OFFSET_STRUCT tempLocation = GET_YACHT_EXTERIOR_MAP_LOCATION(sMissionYachtData[iYachtIndex].iLocation)
	
	IF sMissionYachtVars[iYachtIndex].eMissionYachtSpawnState != FMMC_YACHT_SPAWN_STATE__COMPLETE
		REQUEST_ASSETS_FOR_MISSION_CREATOR_YACHT(sMissionYachtData[iYachtIndex])
		PRINTLN("PROCESS_FMMC_YACHT - Requesting Yacht assets")
		tlDebugText3D += "Loading | "
		
		IF HAVE_ASSETS_LOADED_FOR_MISSION_CREATOR_YACHT(sMissionYachtData[iYachtIndex])
        	CREATE_YACHT_FOR_MISSION_CREATOR(sMissionYachtData[iYachtIndex])
			PRINTLN("PROCESS_FMMC_YACHT - Setting ciMYACHT__LOADED")
			
			sMissionYachtVars[iYachtIndex].eMissionYachtSpawnState = FMMC_YACHT_SPAWN_STATE__COMPLETE
			
			IF VDIST2(GET_CAM_COORD(sCamData.cam), tempLocation.vLoc) > 10000
				SET_CAM_COORD(sCamData.cam, tempLocation.vLoc)
			ENDIF
		ENDIF
	ELSE
		BOOL bRuleDelayed		
		IF NOT bRuleDelayed
			PRINTLN("PROCESS_FMMC_YACHT - Creating Yacht on INIT")
			UPDATE_YACHT_FOR_MISSION_CREATOR(sMissionYachtData[iYachtIndex])
		ENDIF
		
		tlDebugText3D += "ciMYACHT__LOADED | "
	ENDIF
	
	DRAW_DEBUG_TEXT(tlDebugText3D, tempLocation.vLoc)
	
ENDPROC

FUNC BOOL FORCE_CORONER_INTERIOR_IPL_ON()
	REMOVE_IPL("Coroner_Int_off")
	REQUEST_IPL("Coroner_Int_on")
	RETURN IS_IPL_ACTIVE("Coroner_int_on")
ENDFUNC

PROC LOAD_ALL_ACTIVE_INTERIORS()
	INT i
	REPEAT INTERIOR_MAX_NUM i
		IF IS_INTERIOR_CREATOR_OPTION_ACTIVE(INT_TO_ENUM(INTERIOR_NAME_ENUM, i))
			SET_INTERIOR_STATE(INT_TO_ENUM(INTERIOR_NAME_ENUM, i), TRUE)
		ENDIF
	ENDREPEAT
ENDPROC

PROC TOGGLE_INTERIOR_OPTION(INTERIOR_NAME_ENUM eInterior, INT &iBitSet, INT iBit)
	TOGGLE_BIT(iBitset, iBit)
	SET_INTERIOR_STATE(eInterior, IS_BIT_SET(iBitset, iBit))
	SET_LONG_BIT(iInteriorInitializingExtrasBS, iBit)
ENDPROC

// didn't need this but might at some point.
PROC SET_UP_INTERIOR_AFTER_TEST()
	PRINTLN("Mission creator - SET_UP_INTERIOR_AFTER_TEST!")
		
	SET_INTERIOR_STATE(INTERIOR_V_CORONER						, IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_CORONER))
	SET_INTERIOR_STATE(INTERIOR_V_RECYCLE						, IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_RECYCLEPLANT))
	SET_INTERIOR_STATE(INTERIOR_V_ABATTOIR 						, IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_ABATTOIR))
	SET_INTERIOR_STATE(INTERIOR_V_FARMHOUSE 					, IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_FARMHOUSE))
	SET_INTERIOR_STATE(INTERIOR_DT1_03_CARPARK 					, IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_UNION_DEPOSITORY))
	SET_INTERIOR_STATE(INTERIOR_V_BUNKER 						, IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_BUNKER))
	SET_INTERIOR_STATE(INTERIOR_V_SUB 							, IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SUB))
	SET_INTERIOR_STATE(INTERIOR_V_IAA 							, IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_IAA))
	SET_INTERIOR_STATE(INTERIOR_V_LIFEINVADER 					, IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_LIFEINVADER))
	SET_INTERIOR_STATE(INTERIOR_V_FOUNDRY 						, IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_FOUNDRY))
	SET_INTERIOR_STATE(INTERIOR_V_SILO_1 						, IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SILO_01))
	SET_INTERIOR_STATE(INTERIOR_V_SILO_2 						, IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SILO_02))
	SET_INTERIOR_STATE(INTERIOR_V_SILO_3 						, IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SILO_03))
	SET_INTERIOR_STATE(INTERIOR_V_SERVER_FARM 					, IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SERVER_FARM))
	SET_INTERIOR_STATE(INTERIOR_V_SILO_TUNNEL 					, IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SILO_TUNNEL))
	SET_INTERIOR_STATE(INTERIOR_V_SILO_LOOP 					, IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SILO_LOOP))
	SET_INTERIOR_STATE(INTERIOR_V_SILO_ENTRANCE 				, IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SILO_ENTRANCE))
	SET_INTERIOR_STATE(INTERIOR_V_SILO_BASE 					, IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SILO_BASE))
	SET_INTERIOR_STATE(INTERIOR_V_OSPREY 						, IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_OSPREY))
	SET_INTERIOR_STATE(INTERIOR_V_HANGAR 						, IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_HANGAR))
	SET_INTERIOR_STATE(INTERIOR_V_IMPORT_WAREHOUSE 				, IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_IMPORT_WAREHOUSE))
	SET_INTERIOR_STATE(INTERIOR_V_WAREHOUSE_UNDRGRND_FACILITY 	, IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_WAREHOUSE_UNDRGRND_FACILITY))
	SET_INTERIOR_STATE(INTERIOR_V_SPECIAL_CARGO_WAREHOUSE_S 	, IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SPECIAL_CARGO_WAREHOUSE_S))
	SET_INTERIOR_STATE(INTERIOR_V_SPECIAL_CARGO_WAREHOUSE_M 	, IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SPECIAL_CARGO_WAREHOUSE_M))
	SET_INTERIOR_STATE(INTERIOR_V_SPECIAL_CARGO_WAREHOUSE_L 	, IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SPECIAL_CARGO_WAREHOUSE_L))
	SET_INTERIOR_STATE(INTERIOR_V_STILT_APARTMENT 				, IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_STILT_APARTMENT))
	SET_INTERIOR_STATE(INTERIOR_V_HIGH_END_APARTMENT 			, IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_HIGH_END_APARTMENT))
	SET_INTERIOR_STATE(INTERIOR_V_MEDIUM_END_APARTMENT 			, IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_MEDIUM_END_APARTMENT))
	SET_INTERIOR_STATE(INTERIOR_V_LOW_END_APARTMENT 			, IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_LOW_END_APARTMENT))
	SET_INTERIOR_STATE(INTERIOR_V_FIB_OFFICE 					, IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_FIB_OFFICE))
	SET_INTERIOR_STATE(INTERIOR_V_FIB_OFFICE_2 					, IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_FIB_OFFICE_2))	
	SET_INTERIOR_STATE(INTERIOR_V_FIB_LOBBY 					, IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR_FIB_LOBBY))
	SET_INTERIOR_STATE(INTERIOR_V_HIGH_END_GARAGE 				, IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR_HIGH_END_GARAGE))
	SET_INTERIOR_STATE(INTERIOR_V_GARAGEM 					, IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR_MEDIUM_GARAGE))
	SET_INTERIOR_STATE(INTERIOR_V_GARAGES 					, IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR_SMALL_GARAGE))
	SET_INTERIOR_STATE(INTERIOR_V_TUNNEL_ENTRY 					, IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_TUNNEL_ENTRANCE))
	SET_INTERIOR_STATE(INTERIOR_V_TUNNEL_STRAIGHT_0 			, IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_TUNNEL_STRAIGHT))
	SET_INTERIOR_STATE(INTERIOR_V_TUNNEL_SLOPE_FLAT_0			, IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_TUNNEL_FLAT_SLOPE))
	SET_INTERIOR_STATE(INTERIOR_V_TUNNEL_FLAT_SLOPE_0 			, IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_TUNNEL_FLAT_SLOPE_2))
	SET_INTERIOR_STATE(INTERIOR_V_TUNNEL_30D_RIGHT_0 			, IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_TUNNEL_30D_R))
	SET_INTERIOR_STATE(INTERIOR_V_TUNNEL_30D_LEFT_0 			, IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_TUNNEL_30D_L))
	//SET_INTERIOR_STATE(INTERIOR_V_ARENA 						, IS_BIT_SET(nt, g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_ARENA) BREAK))
	SET_INTERIOR_STATE(INTERIOR_V_WEED_FARM 					, IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_WEED_FARM))
	SET_INTERIOR_STATE(INTERIOR_V_ALT_WEED_FARM 				, IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_ALT_WEED_FARM))
	SET_INTERIOR_STATE(INTERIOR_V_COCAINE_FACTORY 				, IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_COCAINE_FACTORY))
	SET_INTERIOR_STATE(INTERIOR_V_METH_FACTORY 					, IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_METH_FACTORY))
	SET_INTERIOR_STATE(INTERIOR_V_CASINO						, IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO))
	SET_INTERIOR_STATE(INTERIOR_V_CASINO_CAR_PARK				, IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO_CAR_PARK))
	SET_INTERIOR_STATE(INTERIOR_V_CASINO_APARTMENT				, IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO_APARTMENT))
	SET_INTERIOR_STATE(INTERIOR_V_CASINO_GARAGE					, IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO_GARAGE))
	SET_INTERIOR_STATE(INTERIOR_V_CASINO_MAIN					, IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO_MAIN))
	SET_INTERIOR_STATE(INTERIOR_V_CASINO_ARCADE					, IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO_ARCADE))
	SET_INTERIOR_STATE(INTERIOR_V_CASINO_HEIST_PLANNING			, IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO_HEIST_PLANNING))
	SET_INTERIOR_STATE(INTERIOR_V_CASINO_TUNNEL					, IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO_TUNNEL))
	SET_INTERIOR_STATE(INTERIOR_V_CASINO_BACK_AREA				, IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO_BACK_AREA))
	SET_INTERIOR_STATE(INTERIOR_V_CASINO_HOTEL_FLOOR			, IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO_HOTEL_FLOOR))
	SET_INTERIOR_STATE(INTERIOR_V_CASINO_LOADING_BAY			, IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO_LOADING_BAY))
	SET_INTERIOR_STATE(INTERIOR_V_CASINO_VAULT					, IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO_VAULT))
	SET_INTERIOR_STATE(INTERIOR_V_CASINO_UTILITY_LIFT			, IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO_UTILITY_LIFT))
	SET_INTERIOR_STATE(INTERIOR_V_CASINO_LIFT_SHAFT				, IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO_LIFT_SHAFT))
	SET_INTERIOR_STATE(INTERIOR_V_NIGHTCLUB_BASEMENT			, IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_NIGHTCLUB_BASEMENT))
	SET_INTERIOR_STATE(INTERIOR_V_CUSTOM_MISSION_MOC			, IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_CUSTOM_MISSION_MOC))
	SET_INTERIOR_STATE(INTERIOR_V_METH_FACTORY_MURRIETA			, IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_METH_FACTORY_MURRIETA))
	SET_INTERIOR_STATE(INTERIOR_V_METH_FACTORY_EAST_VINEWOOD	, IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_METH_FACTORY_EAST_VINEWOOD))
	SET_INTERIOR_STATE(INTERIOR_V_METH_FACTORY_SENORA_DESERT_1	, IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_METH_FACTORY_SENORA_DESERT_1))
	SET_INTERIOR_STATE(INTERIOR_V_METH_FACTORY_SENORA_DESERT_2	, IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_METH_FACTORY_SENORA_DESERT_2))
	SET_INTERIOR_STATE(INTERIOR_V_TUNER_CAR_MEET				, IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_TUNER_CAR_MEET))
	SET_INTERIOR_STATE(INTERIOR_V_TUNER_MOD_GARAGE				, IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_TUNER_MOD_GARAGE))
	SET_INTERIOR_STATE(INTERIOR_V_RECORDING_STUDIO				, IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_RECORDING_STUDIO))
	SET_INTERIOR_STATE(INTERIOR_V_MUSIC_LOCKER					, IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_MUSIC_LOCKER))
	SET_INTERIOR_STATE(INTERIOR_V_ARENA_WAR_GARAGE_LEVEL_1		, IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_ARENA_WAR_GARAGE_LEVEL_1))
	SET_INTERIOR_STATE(INTERIOR_V_CUSTOM_B_4					, IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_CUSTOM_HIGH_END_APARTMENT))
	SET_INTERIOR_STATE(INTERIOR_V_MISSION_BASEMENT				, IS_MISSION_BASEMENT_INTERIOR_ENABLED())
ENDPROC

FUNC BOOL SHOULD_DISPLAY_LIVERY_NAME()
	IF IS_ENTITY_ALIVE(sVehStruct.viCoronaVeh)	
		IF GET_VEHICLE_LIVERY_COUNT(sVehStruct.viCoronaVeh) > -1
		OR (IS_VEHICLE_MODEL(sVehStruct.viCoronaVeh, MULE) AND NOT IS_BIT_SET(sFMMCmenu.iVehBitSet, ciFMMC_VEHICLE_EXTRA1))
		OR GET_NUM_VEHICLE_MODS(sVehStruct.viCoronaVeh, MOD_LIVERY) > 0
		OR IS_VEHICLE_MODEL(sVehStruct.viCoronaVeh, BURRITO)
		OR IS_VEHICLE_MODEL(sVehStruct.viCoronaVeh, PONY)
			IF IS_VEHICLE_MODEL(sVehStruct.viCoronaVeh, MULE)
			OR IS_VEHICLE_MODEL(sVehStruct.viCoronaVeh, SANCHEZ)
			OR IS_VEHICLE_MODEL(sVehStruct.viCoronaVeh, POLMAV)
			OR IS_VEHICLE_MODEL(sVehStruct.viCoronaVeh, SHAMAL)
			OR IS_VEHICLE_MODEL(sVehStruct.viCoronaVeh, RUMPO)
			OR IS_VEHICLE_MODEL(sVehStruct.viCoronaVeh, STUNT)
			OR IS_VEHICLE_MODEL(sVehStruct.viCoronaVeh, GBURRITO2)
			OR IS_VEHICLE_MODEL(sVehStruct.viCoronaVeh, JET)
			OR IS_VEHICLE_MODEL(sVehStruct.viCoronaVeh, WINDSOR)
			OR (IS_VEHICLE_MODEL(sVehStruct.viCoronaVeh, BURRITO) AND IS_ROCKSTAR_DEV())
			OR (IS_VEHICLE_MODEL(sVehStruct.viCoronaVeh, PONY) AND IS_ROCKSTAR_DEV())
			OR (IS_VEHICLE_MODEL(sVehStruct.viCoronaVeh, AMBULANCE) AND IS_ROCKSTAR_DEV())
			OR (IS_VEHICLE_MODEL(sVehStruct.viCoronaVeh, POLICE3) AND IS_ROCKSTAR_DEV())
			OR (GET_NUM_VEHICLE_MODS(sVehStruct.viCoronaVeh, MOD_LIVERY) > 0 AND IS_ROCKSTAR_DEV())
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


PROC REMOVE_TEMP_CREATION_ENTITIES()
	IF DOES_ENTITY_EXIST(sPedStruct.piTempPed)
		DELETE_PED(sPedStruct.piTempPed)
	ENDIF	
	IF DOES_ENTITY_EXIST(sVehStruct.viCoronaVeh)
		DELETE_VEHICLE(sVehStruct.viCoronaVeh)
	ENDIF
	IF DOES_ENTITY_EXIST(sWepStruct.viCoronaWep)
		DELETE_OBJECT(sWepStruct.viCoronaWep)
	ENDIF
	IF DOES_PICKUP_EXIST(sWepStruct.piTempPickupForModel)
		REMOVE_PICKUP(sWepStruct.piTempPickupForModel)
	ENDIF
	IF DOES_ENTITY_EXIST(sObjStruct.viCoronaObj)
		DELETE_OBJECT(sObjStruct.viCoronaObj)
	ENDIF
	IF DOES_ENTITY_EXIST(sPropStruct.viCoronaObj)
		DELETE_OBJECT(sPropStruct.viCoronaObj)
	ENDIF
	DELETE_CURRENT_PROP_TEMPLATE_OBJECTS(sPropStruct)
ENDPROC

PROC REMOVE_TEMP_ENTITES_IF_NOT_NEEDED(INT iCurrentState)

	MODEL_NAMES oldModel
	
	IF iCurrentState != CREATION_TYPE_PEDS
		IF DOES_ENTITY_EXIST(sPedStruct.piTempPed)
			oldModel = GET_ENTITY_MODEL(sPedStruct.piTempPed)
			DELETE_PED(sPedStruct.piTempPed)
			SET_MODEL_AS_NO_LONGER_NEEDED(oldModel)
		ENDIF	
	ENDIF
	
	IF iCurrentState != CREATION_TYPE_VEHICLES
		IF DOES_ENTITY_EXIST(sVehStruct.viCoronaVeh)
			oldModel = GET_ENTITY_MODEL(sVehStruct.viCoronaVeh)
			DELETE_VEHICLE(sVehStruct.viCoronaVeh)
			SET_MODEL_AS_NO_LONGER_NEEDED(oldModel)
		ENDIF
		if DOES_ENTITY_EXIST(sVehStruct.oiCoronaCrate)			
			oldModel = GET_ENTITY_MODEL(sVehStruct.oiCoronaCrate)
			DELETE_OBJECT(sVehStruct.oiCoronaCrate)
			SET_MODEL_AS_NO_LONGER_NEEDED(oldModel)
		ENDIF
	ENDIF
	IF iCurrentState !=CREATION_TYPE_WEAPONS
		IF DOES_ENTITY_EXIST(sWepStruct.viCoronaWep)
			oldModel = GET_ENTITY_MODEL(sWepStruct.viCoronaWep)
			DELETE_OBJECT(sWepStruct.viCoronaWep)					
			SET_MODEL_AS_NO_LONGER_NEEDED(oldModel)
		ENDIF
		
		IF DOES_PICKUP_EXIST(sWepStruct.piTempPickupForModel)
			REMOVE_PICKUP(sWepStruct.piTempPickupForModel)
		ENDIF
	ENDIF
	IF iCurrentState != CREATION_TYPE_OBJECTS
		IF DOES_ENTITY_EXIST(sObjStruct.viCoronaObj)
			oldModel = GET_ENTITY_MODEL(sObjStruct.viCoronaObj)
			DELETE_OBJECT(sObjStruct.viCoronaObj)					
			SET_MODEL_AS_NO_LONGER_NEEDED(oldModel)
		ENDIF
	ENDIF
	IF iCurrentState != CREATION_TYPE_PROPS
		IF DOES_ENTITY_EXIST(sPropStruct.viCoronaObj)
			oldModel = GET_ENTITY_MODEL(sPropStruct.viCoronaObj)
			DELETE_OBJECT(sPropStruct.viCoronaObj)			
			SET_MODEL_AS_NO_LONGER_NEEDED(oldModel)
		ENDIF
		DELETE_CURRENT_PROP_TEMPLATE_OBJECTS(sPropStruct)
	ENDIF
	IF iCurrentState != CREATION_TYPE_DYNOPROPS
		IF DOES_ENTITY_EXIST(sDynoPropStruct.viCoronaObj)
			oldModel = GET_ENTITY_MODEL(sDynoPropStruct.viCoronaObj)
			DELETE_OBJECT(sDynoPropStruct.viCoronaObj)
			SET_MODEL_AS_NO_LONGER_NEEDED(oldModel)
			
			// [ChildDyno]
			IF GET_MODEL_NAME_OF_CHILD_PROP_FOR_DYNO(oldModel) != DUMMY_MODEL_FOR_SCRIPT
				IF DOES_ENTITY_EXIST(sDynoPropStruct.oiChildTemp)
					DELETE_OBJECT(sDynoPropStruct.oiChildTemp)
				ENDIF
				SET_MODEL_AS_NO_LONGER_NEEDED(oldModel)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC CLEAN_LAST_GO_TO_LOCATION()
	PRINTLN("CLEAN_LAST_GO_TO_LOCATION")
	IF g_FMMC_STRUCT_ENTITIES.iNumberOfGoToLocations[0] < FMMC_MAX_GANG_HIDE_LOCATIONS
		g_FMMC_STRUCT_ENTITIES.sGotoLocationData[g_FMMC_STRUCT_ENTITIES.iNumberOfGoToLocations[0]].vLoc[0]			= <<0.0, 0.0, 0.0>>
		g_FMMC_STRUCT_ENTITIES.sGotoLocationData[g_FMMC_STRUCT_ENTITIES.iNumberOfGoToLocations[0]].fRadius[0]			= 0.0
		g_FMMC_STRUCT_ENTITIES.sGotoLocationData[g_FMMC_STRUCT_ENTITIES.iNumberOfGoToLocations[0]].vLoc1				= <<0.0, 0.0, 0.0>>
		g_FMMC_STRUCT_ENTITIES.sGotoLocationData[g_FMMC_STRUCT_ENTITIES.iNumberOfGoToLocations[0]].vLoc2				= <<0.0, 0.0, 0.0>>
		g_FMMC_STRUCT_ENTITIES.sGotoLocationData[g_FMMC_STRUCT_ENTITIES.iNumberOfGoToLocations[0]].fWidth				=  1.0
		INT iTeam
		FOR iTeam = 0 TO (FMMC_MAX_TEAMS - 1)
			g_FMMC_STRUCT_ENTITIES.sGotoLocationData[g_FMMC_STRUCT_ENTITIES.iNumberOfGoToLocations[0]].iRule[iTeam] 		= FMMC_OBJECTIVE_LOGIC_NONE
			g_FMMC_STRUCT_ENTITIES.sGotoLocationData[g_FMMC_STRUCT_ENTITIES.iNumberOfGoToLocations[0]].iPriority[iTeam]	= FMMC_PRIORITY_IGNORE
			g_FMMC_STRUCT_ENTITIES.sGotoLocationData[g_FMMC_STRUCT_ENTITIES.iNumberOfGoToLocations[0]].fHeading[iTeam]	= 0.0
		ENDFOR
	ENDIF
ENDPROC

PROC TRACK_TIME_IN_CREATOR_MODE()

	IF NETWORK_CLAN_SERVICE_IS_VALID()  
		INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_TOTAL_TIME_MISSION_CREATO, TIMERA())
		SETTIMERA(0)
	ENDIF
	
ENDPROC

PROC CLEAR_PLANNING_MISSION(PLANNING_MISSION_STRUCT &sPlanningMission)
	sPlanningMission.tlID = ""
	sPlanningMission.iLevel = 0
	sPlanningMission.iVar = -1
ENDPROC

PROC PRINT_PLANNING_MISSIONS(PLANNING_MISSION_STRUCT &sPlanningMissions[])
	INT i
	PRINTNL()
	PRINTLN("*************PLANNING MISSIONS*************")
	FOR i = 0 TO FMMC_MAX_PLANNING_MISSIONS-1
		PRINTLN("Planning Mission ", i, ": ", sPlanningMissions[i].tlID, " lvl ", sPlanningMissions[i].iLevel, " var ", sPlanningMissions[i].iVar)
	ENDFOR
	PRINTLN("*******************************************")
	PRINTNL()
ENDPROC

PROC SORT_PLANNING_MISSIONS()

	PRINTLN("Planning Missions Pre Sort")
	PRINT_PLANNING_MISSIONS(g_FMMC_STRUCT.sPlanningMissions)

	INT i, i2, iLevel = 0
	PLANNING_MISSION_STRUCT sTempArray[FMMC_MAX_PLANNING_MISSIONS]
	FOR i = 0 TO FMMC_MAX_PLANNING_MISSIONS-1
		sTempArray[i] = g_FMMC_STRUCT.sPlanningMissions[i]
		CLEAR_PLANNING_MISSION(g_FMMC_STRUCT.sPlanningMissions[i])
	ENDFOR
	
	PRINTLN("Planning Missions Temp Array")
	PRINT_PLANNING_MISSIONS(sTempArray)
	
	INT iCurrentIndex = 0
	FOR iLevel = 0  TO 2			//Cycle the three levels
		FOR i = 0 TO FMMC_MAX_PLANNING_MISSIONS-1		//cycle all entries
			IF NOT IS_STRING_NULL_OR_EMPTY(sTempArray[i].tlID)	//if there's a planning mission id 
			AND sTempArray[i].iLevel = iLevel
				g_fmmc_struct.sPlanningMissions[iCurrentIndex] = sTempArray[i]
				iCurrentIndex++
				IF sTempArray[i].iVar != -1
					FOR i2 = i+1 TO FMMC_MAX_PLANNING_MISSIONS-1
						IF i2 < FMMC_MAX_PLANNING_MISSIONS
							IF NOT IS_STRING_NULL_OR_EMPTY(sTempArray[i2].tlID)
							AND sTempArray[i2].iLevel = iLevel
							AND sTempArray[i2].iVar = sTempArray[i].iVar
								g_fmmc_struct.sPlanningMissions[iCurrentIndex] = sTempArray[i2]
								CLEAR_PLANNING_MISSION(sTempArray[i2])
								iCurrentIndex++
							ENDIF
						ENDIF
					ENDFOR
				ENDIF
				CLEAR_PLANNING_MISSION(sTempArray[i])
			ENDIF
		ENDFOR
	ENDFOR
	
	PRINTLN("Planning Missions Post Sort")
	PRINT_PLANNING_MISSIONS(g_FMMC_STRUCT.sPlanningMissions)
	
ENDPROC

PROC LIMIT_ASSOCIATED_RULES_TO_NUMBER_OF_TEAMS()
	INT i
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfPeds-1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAssociatedTeam >= g_FMMC_STRUCT.iMaxNumberOfTeams
			g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAssociatedTeam = -1
			g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAssociatedObjective = 0
			g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAssociatedSpawn = 0
			g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iAssociatedActionStart = 0
		ENDIF
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iSecondAssociatedTeam >= g_FMMC_STRUCT.iMaxNumberOfTeams
			g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iSecondAssociatedTeam = -1
			g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iSecondAssociatedObjective = 0
			g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iSecondAssociatedSpawn = 0
			g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iSecondActionStart = 0
		ENDIF
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iThirdAssociatedTeam >= g_FMMC_STRUCT.iMaxNumberOfTeams
			g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iThirdAssociatedTeam = -1
			g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iThirdAssociatedObjective = 0
			g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iThirdAssociatedSpawn = 0
			g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iThirdActionStart = 0
		ENDIF
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iFourthAssociatedTeam >= g_FMMC_STRUCT.iMaxNumberOfTeams
			g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iFourthAssociatedTeam = -1
			g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iFourthAssociatedObjective = 0
			g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iFourthAssociatedSpawn = 0
			g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iFourthActionStart = 0
		ENDIF
	ENDFOR

	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles-1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAssociatedTeam >= g_FMMC_STRUCT.iMaxNumberOfTeams
			g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAssociatedTeam = -1
			g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAssociatedObjective = 0
			g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAssociatedSpawn = 0
		ENDIF
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iSecondAssociatedTeam >= g_FMMC_STRUCT.iMaxNumberOfTeams
			g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iSecondAssociatedTeam = -1
			g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iSecondAssociatedObjective = 0
			g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iSecondAssociatedSpawn = 0
		ENDIF
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iThirdAssociatedTeam >= g_FMMC_STRUCT.iMaxNumberOfTeams
			g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iThirdAssociatedTeam = -1
			g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iThirdAssociatedObjective = 0
			g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iThirdAssociatedSpawn = 0
		ENDIF
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iFourthAssociatedTeam >= g_FMMC_STRUCT.iMaxNumberOfTeams
			g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iFourthAssociatedTeam = -1
			g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iFourthAssociatedObjective = 0
			g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iFourthAssociatedSpawn = 0
		ENDIF
	ENDFOR

	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfObjects-1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAssociatedTeam >= g_FMMC_STRUCT.iMaxNumberOfTeams
			g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAssociatedTeam = -1
			g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAssociatedObjective = 0
			g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAssociatedSpawn = 0
		ENDIF
		IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSecondAssociatedTeam >= g_FMMC_STRUCT.iMaxNumberOfTeams
			g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSecondAssociatedTeam = -1
			g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSecondAssociatedObjective = 0
			g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iSecondAssociatedSpawn = 0
		ENDIF
		IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iThirdAssociatedTeam >= g_FMMC_STRUCT.iMaxNumberOfTeams
			g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iThirdAssociatedTeam = -1
			g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iThirdAssociatedObjective = 0
			g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iThirdAssociatedSpawn = 0
		ENDIF
		IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iFourthAssociatedTeam >= g_FMMC_STRUCT.iMaxNumberOfTeams
			g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iFourthAssociatedTeam = -1
			g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iFourthAssociatedObjective = 0
			g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iFourthAssociatedSpawn = 0
		ENDIF
	ENDFOR
	
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfTrains - 1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedTrains[i].iTrain_AssociatedTeam >= g_FMMC_STRUCT.iMaxNumberOfTeams
			g_FMMC_STRUCT_ENTITIES.sPlacedTrains[i].iTrain_AssociatedTeam = -1
			g_FMMC_STRUCT_ENTITIES.sPlacedTrains[i].iTrain_AssociatedRule = 0
			g_FMMC_STRUCT_ENTITIES.sPlacedTrains[i].iTrain_AssociatedRuleSpawnLimit = 0
		ENDIF
	ENDFOR
	
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfInteractables - 1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].iInteractable_AssociatedTeam >= g_FMMC_STRUCT.iMaxNumberOfTeams
			g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].iInteractable_AssociatedTeam = -1
			g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].iInteractable_AssociatedRule = 0
			g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].iInteractable_AssociatedRuleSpawnLimit = 0
		ENDIF
	ENDFOR
	
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfProps-1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].iAssociatedTeam >= g_FMMC_STRUCT.iMaxNumberOfTeams
			g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].iAssociatedTeam = -1
			g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].iAssociatedObjective = 0
			g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].iAssociatedSpawn = 0
			g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].iAssociatedActionStart = 0
		ENDIF
		IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].iSecondAssociatedTeam >= g_FMMC_STRUCT.iMaxNumberOfTeams
			g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].iSecondAssociatedTeam = -1
			g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].iSecondAssociatedObjective = 0
			g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].iSecondAssociatedSpawn = 0
			g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].iSecondActionStart = 0
		ENDIF
		IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].iThirdAssociatedTeam >= g_FMMC_STRUCT.iMaxNumberOfTeams
			g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].iThirdAssociatedTeam = -1
			g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].iThirdAssociatedObjective = 0
			g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].iThirdAssociatedSpawn = 0
			g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].iThirdActionStart = 0
		ENDIF
		IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].iFourthAssociatedTeam >= g_FMMC_STRUCT.iMaxNumberOfTeams
			g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].iFourthAssociatedTeam = -1
			g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].iFourthAssociatedObjective = 0
			g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].iFourthAssociatedSpawn = 0
			g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].iFourthActionStart = 0
		ENDIF
	ENDFOR
	
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons-1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iAssociatedTeam >= g_FMMC_STRUCT.iMaxNumberOfTeams
			g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iAssociatedTeam = -1
			g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iAssociatedObjective = 0
			g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iAssociatedSpawn = 0
		ENDIF
		IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iSecondAssociatedTeam >= g_FMMC_STRUCT.iMaxNumberOfTeams
			g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iSecondAssociatedTeam = -1
			g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iSecondAssociatedObjective = 0
			g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iSecondAssociatedSpawn = 0
		ENDIF
		IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iThirdAssociatedTeam >= g_FMMC_STRUCT.iMaxNumberOfTeams
			g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iThirdAssociatedTeam = -1
			g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iThirdAssociatedObjective = 0
			g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iThirdAssociatedSpawn = 0
		ENDIF
		IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iFourthAssociatedTeam >= g_FMMC_STRUCT.iMaxNumberOfTeams
			g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iFourthAssociatedTeam = -1
			g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iFourthAssociatedObjective = 0
			g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iFourthAssociatedSpawn = 0
		ENDIF
	ENDFOR
ENDPROC


PROC EDIT_AND_CAP_INT(INT &iValue, INT iChange, INT iMinValue, INT iMaxValue)
	iValue += iChange
	
	IF iValue < iMinValue
		iValue = iMaxValue
	ELIF iValue > iMaxValue
		iValue = iMinValue
	ENDIF
ENDPROC

PROC EDIT_AND_CAP_FLOAT(FLOAT &fValue, FLOAT  fChange, FLOAT fMinValue, FLOAT fMaxValue)
	fValue += fChange
	
	IF fValue < fMinValue
		fValue = fMaxValue
	ELIF fValue > fMaxValue
		fValue = fMinValue
	ENDIF
ENDPROC

PROC SET_THE_TEMPORARY_BOOL(INT iBitset, INT i, BOOL &bValue)
	IF IS_BIT_SET(iBitset, i)
		bValue = TRUE
	ELSE
		bValue = FALSE
	ENDIF
ENDPROC

PROC SWITCH_THE_BITS(INT &iBitset, INT i1, INT i2)
	
	IF IS_BIT_SET(iBitset, i1)
		SET_BIT(iBitset, i2)
	ELSE
		CLEAR_BIT(iBitset, i2)
	ENDIF
	
ENDPROC

PROC SET_THE_BIT_TO_THE_TEMP_BOOL(INT &iBitset, INT i, BOOL bValue)
	IF bValue
		SET_BIT(iBitset, i)
	ELSE
		CLEAR_BIT(iBitset, i)
	ENDIF
ENDPROC

PROC MOVE_RULE_BITS_ONE_POS(INT iRow, BOOL bUp = TRUE)
	
	BOOL bValue = FALSE
	
	IF bUp
		IF iRow > 0
		
			SET_THE_TEMPORARY_BOOL(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iWantedAtMidBitset, iRow-1, bValue)			
			SWITCH_THE_BITS(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iWantedAtMidBitset, iRow, iRow-1)			
			SET_THE_BIT_TO_THE_TEMP_BOOL(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iWantedAtMidBitset, iRow, bValue)
		
			SET_THE_TEMPORARY_BOOL(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iLoseWantedBitset, iRow-1, bValue)			
			SWITCH_THE_BITS(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iLoseWantedBitset, iRow, iRow-1)			
			SET_THE_BIT_TO_THE_TEMP_BOOL(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iLoseWantedBitset, iRow, bValue)
		
			SET_THE_TEMPORARY_BOOL(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iSurpressWantedBitset, iRow-1, bValue)			
			SWITCH_THE_BITS(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iSurpressWantedBitset, iRow, iRow-1)			
			SET_THE_BIT_TO_THE_TEMP_BOOL(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iSurpressWantedBitset, iRow, bValue)
		
			SET_THE_TEMPORARY_BOOL(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iGangBackupAtMidBitset, iRow-1, bValue)			
			SWITCH_THE_BITS(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iGangBackupAtMidBitset, iRow, iRow-1)			
			SET_THE_BIT_TO_THE_TEMP_BOOL(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iGangBackupAtMidBitset, iRow, bValue)
			
			SET_THE_TEMPORARY_BOOL(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iPlayAreaBitset, iRow-1, bValue)			
			SWITCH_THE_BITS(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iPlayAreaBitset, iRow, iRow-1)			
			SET_THE_BIT_TO_THE_TEMP_BOOL(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iPlayAreaBitset, iRow, bValue)
			
			SET_THE_TEMPORARY_BOOL(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iLeaveAreaBitset, iRow-1, bValue)			
			SWITCH_THE_BITS(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iLeaveAreaBitset, iRow, iRow-1)			
			SET_THE_BIT_TO_THE_TEMP_BOOL(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iLeaveAreaBitset, iRow, bValue)
			
			SET_THE_TEMPORARY_BOOL(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iPlayArea2Bitset, iRow-1, bValue)			
			SWITCH_THE_BITS(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iPlayArea2Bitset, iRow, iRow-1)			
			SET_THE_BIT_TO_THE_TEMP_BOOL(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iPlayArea2Bitset, iRow, bValue)
			
			SET_THE_TEMPORARY_BOOL(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iLeaveArea2Bitset, iRow-1, bValue)			
			SWITCH_THE_BITS(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iLeaveArea2Bitset, iRow, iRow-1)			
			SET_THE_BIT_TO_THE_TEMP_BOOL(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iLeaveArea2Bitset, iRow, bValue)
			
			SET_THE_TEMPORARY_BOOL(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iSpawnAreaBitset, iRow-1, bValue)			
			SWITCH_THE_BITS(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iSpawnAreaBitset, iRow, iRow-1)			
			SET_THE_BIT_TO_THE_TEMP_BOOL(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iSpawnAreaBitset, iRow, bValue)
			
			SET_THE_TEMPORARY_BOOL(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iSpawnWhileInArea, iRow-1, bValue)			
			SWITCH_THE_BITS(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iSpawnWhileInArea, iRow, iRow-1)			
			SET_THE_BIT_TO_THE_TEMP_BOOL(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iSpawnWhileInArea, iRow, bValue)
			
			SET_THE_TEMPORARY_BOOL(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iWantedLevelBounds, iRow-1, bValue)			
			SWITCH_THE_BITS(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iWantedLevelBounds, iRow, iRow-1)			
			SET_THE_BIT_TO_THE_TEMP_BOOL(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iWantedLevelBounds, iRow, bValue)
			
			SET_THE_TEMPORARY_BOOL(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iWantedLevelBounds2, iRow-1, bValue)			
			SWITCH_THE_BITS(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iWantedLevelBounds2, iRow, iRow-1)			
			SET_THE_BIT_TO_THE_TEMP_BOOL(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iWantedLevelBounds2, iRow, bValue)
			
			SET_THE_TEMPORARY_BOOL(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iDropOffBitSet, iRow-1, bValue)			
			SWITCH_THE_BITS(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iDropOffBitSet, iRow, iRow-1)			
			SET_THE_BIT_TO_THE_TEMP_BOOL(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iDropOffBitSet, iRow, bValue)
		ENDIF
	ELSE
		IF iRow < (g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].iNumberOfActiveRows - 1)
		
			SET_THE_TEMPORARY_BOOL(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iWantedAtMidBitset, iRow+1, bValue)			
			SWITCH_THE_BITS(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iWantedAtMidBitset, iRow, iRow+1)			
			SET_THE_BIT_TO_THE_TEMP_BOOL(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iWantedAtMidBitset, iRow, bValue)
		
			SET_THE_TEMPORARY_BOOL(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iLoseWantedBitset, iRow+1, bValue)			
			SWITCH_THE_BITS(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iLoseWantedBitset, iRow, iRow+1)			
			SET_THE_BIT_TO_THE_TEMP_BOOL(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iLoseWantedBitset, iRow, bValue)
		
			SET_THE_TEMPORARY_BOOL(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iWantedAtMidBitset, iRow+1, bValue)			
			SWITCH_THE_BITS(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iWantedAtMidBitset, iRow, iRow+1)			
			SET_THE_BIT_TO_THE_TEMP_BOOL(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iWantedAtMidBitset, iRow, bValue)
		
			SET_THE_TEMPORARY_BOOL(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iSurpressWantedBitset, iRow+1, bValue)			
			SWITCH_THE_BITS(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iSurpressWantedBitset, iRow, iRow+1)			
			SET_THE_BIT_TO_THE_TEMP_BOOL(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iSurpressWantedBitset, iRow, bValue)
		
			SET_THE_TEMPORARY_BOOL(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iGangBackupAtMidBitset, iRow+1, bValue)			
			SWITCH_THE_BITS(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iGangBackupAtMidBitset, iRow, iRow+1)			
			SET_THE_BIT_TO_THE_TEMP_BOOL(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iGangBackupAtMidBitset, iRow, bValue)
			
			SET_THE_TEMPORARY_BOOL(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iPlayAreaBitset, iRow+1, bValue)			
			SWITCH_THE_BITS(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iPlayAreaBitset, iRow, iRow+1)			
			SET_THE_BIT_TO_THE_TEMP_BOOL(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iPlayAreaBitset, iRow, bValue)
			
			SET_THE_TEMPORARY_BOOL(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iLeaveAreaBitset, iRow+1, bValue)			
			SWITCH_THE_BITS(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iLeaveAreaBitset, iRow, iRow+1)			
			SET_THE_BIT_TO_THE_TEMP_BOOL(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iLeaveAreaBitset, iRow, bValue)
			
			SET_THE_TEMPORARY_BOOL(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iPlayArea2Bitset, iRow+1, bValue)			
			SWITCH_THE_BITS(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iPlayArea2Bitset, iRow, iRow+1)			
			SET_THE_BIT_TO_THE_TEMP_BOOL(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iPlayArea2Bitset, iRow, bValue)
			
			SET_THE_TEMPORARY_BOOL(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iLeaveArea2Bitset, iRow+1, bValue)			
			SWITCH_THE_BITS(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iLeaveArea2Bitset, iRow, iRow+1)			
			SET_THE_BIT_TO_THE_TEMP_BOOL(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iLeaveArea2Bitset, iRow, bValue)
			
			SET_THE_TEMPORARY_BOOL(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iSpawnAreaBitset, iRow+1, bValue)			
			SWITCH_THE_BITS(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iSpawnAreaBitset, iRow, iRow+1)			
			SET_THE_BIT_TO_THE_TEMP_BOOL(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iSpawnAreaBitset, iRow, bValue)
			
			SET_THE_TEMPORARY_BOOL(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iSpawnWhileInArea, iRow+1, bValue)			
			SWITCH_THE_BITS(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iSpawnWhileInArea, iRow, iRow+1)			
			SET_THE_BIT_TO_THE_TEMP_BOOL(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iSpawnWhileInArea, iRow, bValue)
			
			SET_THE_TEMPORARY_BOOL(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iWantedLevelBounds, iRow+1, bValue)			
			SWITCH_THE_BITS(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iWantedLevelBounds, iRow, iRow+1)			
			SET_THE_BIT_TO_THE_TEMP_BOOL(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iWantedLevelBounds, iRow, bValue)
			
			SET_THE_TEMPORARY_BOOL(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iWantedLevelBounds2, iRow+1, bValue)			
			SWITCH_THE_BITS(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iWantedLevelBounds2, iRow, iRow+1)			
			SET_THE_BIT_TO_THE_TEMP_BOOL(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iWantedLevelBounds2, iRow, bValue)
			
			SET_THE_TEMPORARY_BOOL(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iDropOffBitSet, iRow+1, bValue)			
			SWITCH_THE_BITS(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iDropOffBitSet, iRow, iRow+1)			
			SET_THE_BIT_TO_THE_TEMP_BOOL(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iDropOffBitSet, iRow, bValue)
			
		ELSE
			PRINTLN("iRow >=  (g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].iNumberOfActiveRows - 1)")
		ENDIF
	ENDIF
	
ENDPROC

FUNC INT GET_NEXT_EMPTY_MOCAP_CUTSCENE()
	INT iLoop
	FOR iLoop = 0 TO MAX_MOCAP_CUTSCENES - 1
		IF IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sMocapCutsceneData[iLoop].tlName)
			RETURN iLoop
		ENDIF
	ENDFOR
	RETURN -1
ENDFUNC


///
///    Passing in 0 will return the int version of the outfit enum currently - OUTFIT_ORNATE_CASANOVA_FRUITY.
FUNC INT GET_NTH_AVAILABLE_OUTFIT(INT iTeam, INT n)
	INT iLoop, iCount
	FOR iLoop = 0 TO ENUM_TO_INT(OUTFIT_MAX_AMOUNT)-1
		IF IS_BIT_SET(g_FMMC_STRUCT.biOutFitsAvailableBitset[iTeam][iLoop/32], iLoop%32)
			IF n = iCount
				RETURN iLoop
			ENDIF
			iCOunt++
		ENDIF
	ENDFOR
	SCRIPT_ASSERT("GET_NTH_AVAILABLE_OUTFIT nth Outfit is unhandled enum")
	RETURN -1  //returns default outfit if error
ENDFUNC

FUNC INT GET_NTH_AVAILABLE_OUTFIT_STYLE(INT iTeam, INT n)
	INT iLoop, iCount
	FOR iLoop = 0 TO ENUM_TO_INT(OUTFIT_STYLE_MAX)-1
		IF IS_BIT_SET(g_FMMC_STRUCT.biStyleAvailableBitset[iTeam][iLoop/32], iLoop%32)
			IF n = iCount
				RETURN iLoop
			ENDIF
			iCount++
		ENDIF
	ENDFOR
	SCRIPT_ASSERT("GET_NTH_AVAILABLE_OUTFIT_STYLE nth Outfit Style is unhandled enum")
	RETURN -1  //returns default outfit style if error
ENDFUNC

FUNC INT GET_NTH_AVAILABLE_MASK(INT iTeam, INT n)
	INT iLoop, iCount
	FOR iLoop = 0 TO ENUM_TO_INT(MASK_MAX)-1
		IF IS_BIT_SET(g_FMMC_STRUCT.biMaskAvailableBitset[iTeam][iLoop/32], iLoop%32)
			IF n = iCount
				RETURN iLoop
			ENDIF
			iCount++
		ENDIF
	ENDFOR
	SCRIPT_ASSERT("GET_NTH_AVAILABLE_MASK nth Mask is unhandled enum")
	RETURN -1  //returns default outfit style if error
ENDFUNC

FUNC INT GET_NTH_AVAILABLE_GEAR(INT iTeam, INT n)
	INT iLoop, iCount
	FOR iLoop = 0 TO ENUM_TO_INT(OUTFIT_MAX_AMOUNT)-1
		IF GET_LONG_BITSET_INDEX(iLoop) < MAX_HEIST_GEAR_BITSETS
			IF IS_LONG_BIT_SET(g_FMMC_STRUCT.biGearAvailableBitset[iTeam], iLoop)
				IF n = iCount
					RETURN iLoop
				ENDIF
				iCount++
			ENDIF
		ELSE
			SCRIPT_ASSERT("GET_NTH_AVAILABLE_OUTFIT | iLoop is too high")
		ENDIF
	ENDFOR
	
	SCRIPT_ASSERT("GET_NTH_AVAILABLE_OUTFIT nth Outfit is unhandled enum")
	RETURN -1  //returns default outfit if error
ENDFUNC

PROC UNCOLOUR_OBJECTIVE_TEXT(TEXT_LABEL_63 &tl63Obj)
	
	INT i
	INT iLengthOfString = GET_LENGTH_OF_LITERAL_STRING(tl63Obj)
	BOOL bWaitingForEndTilda
	TEXT_LABEL_63 tl63ReturnLabel
	STRING sChar
	
	REPEAT iLengthOfString i
		sChar = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME(tl63Obj, i, i+1)
		IF NOT ARE_STRINGS_EQUAL("~", sChar)
			IF NOT bWaitingForEndTilda
				tl63ReturnLabel += sChar
			ELSE
				// Do nothing, removing everything between tildas.
			ENDIF
		ELSE
			IF NOT bWaitingForEndTilda
				bWaitingForEndTilda = TRUE
			ELSE
				bWaitingForEndTilda = FALSE
			ENDIF
		ENDIF
	ENDREPEAT
	
	tl63Obj = tl63ReturnLabel
	
ENDPROC

FUNC INT GET_NUMBER_OF_PLACED_VEHICLES()
	INT i = 0
	INT iVeh = 0
	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles i	
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn != DUMMY_MODEL_FOR_SCRIPT
			iVeh++
		ENDIF
	ENDREPEAT
	RETURN iVeh
ENDFUNC


FUNC BOOL IS_NEAR_START_END_LOCATION(CREATION_VARS_STRUCT &sCurrentVarsStructPassed, BOOL bIsStart = TRUE, FLOAT fRadius = 10.0)
	IF bIsStart
		IF  (sCurrentVarsStructPassed.vCoronaPos.x + fRadius) >  (g_FMMC_STRUCT.sFMMCEndConditions[0].vEndPos.x)
		AND (sCurrentVarsStructPassed.vCoronaPos.y + fRadius) >  (g_FMMC_STRUCT.sFMMCEndConditions[0].vEndPos.y)
		AND (sCurrentVarsStructPassed.vCoronaPos.z + fRadius) >  (g_FMMC_STRUCT.sFMMCEndConditions[0].vEndPos.z)
		AND (sCurrentVarsStructPassed.vCoronaPos.x - fRadius) <  (g_FMMC_STRUCT.sFMMCEndConditions[0].vEndPos.x)
		AND (sCurrentVarsStructPassed.vCoronaPos.y - fRadius) <  (g_FMMC_STRUCT.sFMMCEndConditions[0].vEndPos.y)
		AND (sCurrentVarsStructPassed.vCoronaPos.z - fRadius) <  (g_FMMC_STRUCT.sFMMCEndConditions[0].vEndPos.z)
			IF NOT IS_VECTOR_ZERO(sCurrentVarsStructPassed.vCoronaPos)
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE
		IF  (sCurrentVarsStructPassed.vCoronaPos.x + fRadius) >  (g_FMMC_STRUCT.vStartPos.x)
		AND (sCurrentVarsStructPassed.vCoronaPos.y + fRadius) >  (g_FMMC_STRUCT.vStartPos.y)
		AND (sCurrentVarsStructPassed.vCoronaPos.z + fRadius) >  (g_FMMC_STRUCT.vStartPos.z)
		AND (sCurrentVarsStructPassed.vCoronaPos.x - fRadius) <  (g_FMMC_STRUCT.vStartPos.x)
		AND (sCurrentVarsStructPassed.vCoronaPos.y - fRadius) <  (g_FMMC_STRUCT.vStartPos.y)
		AND (sCurrentVarsStructPassed.vCoronaPos.z - fRadius) <  (g_FMMC_STRUCT.vStartPos.z)
			IF NOT IS_VECTOR_ZERO(sCurrentVarsStructPassed.vCoronaPos)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_VECTOR_POINT_IN_CORONA(VECTOR Vpassed, FLOAT fRadius = 2.3)
	IF  (sCurrentVarsStruct.vCoronaPos.x + fRadius) >  (Vpassed.x)
	AND (sCurrentVarsStruct.vCoronaPos.y + fRadius) >  (Vpassed.y)
	AND (sCurrentVarsStruct.vCoronaPos.z + fRadius) >  (Vpassed.z)
	AND (sCurrentVarsStruct.vCoronaPos.x - fRadius) <  (Vpassed.x)
	AND (sCurrentVarsStruct.vCoronaPos.y - fRadius) <  (Vpassed.y)
	AND (sCurrentVarsStruct.vCoronaPos.z - fRadius) <  (Vpassed.z)
		IF NOT IS_VECTOR_ZERO(Vpassed)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC SET_FAIL_TEAM_AND_RULE(CREATION_VARS_STRUCT &sCurrentVarsStructPassed, INT iTeam, INT iRule)
	IF sCurrentVarsStructPassed.iSaveFailTeam = -1
		sCurrentVarsStructPassed.iSaveFailTeam = iTeam
		sCurrentVarsStructPassed.iSaveFailRule = iRule
	ENDIF
ENDPROC

PROC SET_ENTITY_CREATION_STATUS(INT iNewState)
	DEBUG_PRINTCALLSTACK()
	PRINTLN("[MISSION CREATOR] - SET_ENTITY_CREATION_STATUS - Setting entity creation state from ", sCurrentVarsStruct.iEntityCreationStatus, " to ", iNewState)
	sCurrentVarsStruct.iEntityCreationStatus = iNewState
ENDPROC

PROC SET_FAIL_BIT_SET(INT &iFailBitSet)//, BOOL bIgnoreTest = FALSE)
	
	CLEAR_BIT_ALERT(sFMMCmenu)
	IF IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.tl63MissionName)
		SET_BIT(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_TITLE)
	ENDIF
	IF NOT SCRIPT_IS_CLOUD_AVAILABLE()
		IF bCreatorLimitedCloudDown = FALSE
			SET_ENTITY_CREATION_STATUS(STAGE_CLOUD_FAILURE)
		ENDIF
		SET_BIT(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_NO_CLOUD)
		CLEAR_BIT(sFMMCmenu.iOptionsMenuBitSet, OPTION_MISSION_NAME)
		CLEAR_BIT(sFMMCmenu.iOptionsMenuBitSet, OPTION_MISSION_DESCRIPTION)
		CLEAR_BIT(sFMMCmenu.iOptionsMenuBitSet, OPTION_MISSION_TAGS)
		CLEAR_BIT(sFMMCmenu.iOptionsMenuBitSet, OPTION_MISSION_SYNOPSIS)
	ELSE
		IF bCreatorLimitedCloudDown = TRUE
			SET_ENTITY_CREATION_STATUS(STAGE_CLOUD_FAILURE)
		ENDIF
		SET_BIT(sFMMCmenu.iOptionsMenuBitSet, OPTION_MISSION_NAME)
		SET_BIT(sFMMCmenu.iOptionsMenuBitSet, OPTION_MISSION_DESCRIPTION)
		SET_BIT(sFMMCmenu.iOptionsMenuBitSet, OPTION_MISSION_TAGS)
		SET_BIT(sFMMCmenu.iOptionsMenuBitSet, OPTION_MISSION_SYNOPSIS)	
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iPhoto, FMMC_PHOTO_TAKEN)
		SET_BIT(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_PHOTO)
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.iPhoto, FMMC_PHOTO_TAKEN)
	AND g_FMMC_STRUCT.iPhotoPath != UGC_PATH_PHOTO_NG
	AND !bNextGenPhotoTaken 
		SET_BIT(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_LAST_GEN_PHOTO)
	ENDIF
	IF IS_VECTOR_ZERO(g_FMMC_STRUCT.vCameraPanPos)
		SET_BIT(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_INTROCAM)
	ENDIF
	IF g_FMMC_STRUCT.iMissionSubType != FMMC_MISSION_TYPE_VERSUS
	AND g_FMMC_STRUCT.iMissionSubType != FMMC_MISSION_TYPE_LTS
	AND g_FMMC_STRUCT.iMissionSubType != FMMC_MISSION_TYPE_CTF
		sFMMCmenu.iSelectedCameraTeam = 0
	ENDIF
	IF IS_MISSION_DESCIPTION_EMPTY()
		SET_BIT(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_DESC)
	ENDIF			
	IF IS_VECTOR_ZERO(g_FMMC_STRUCT.vStartPos)
		SET_BIT(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_TRIGGER)
	ENDIF	
	
	IF NOT IS_BIT_SET_ALERT(sFMMCmenu)
		CLEAR_BIT(g_FMMC_STRUCT.iMissionTypeBitSet, ciMISSION_OPTION_BS_MISSION_IS_NOT_PLAYABLE)
	ELSE
		SET_BIT(g_FMMC_STRUCT.iMissionTypeBitSet, ciMISSION_OPTION_BS_MISSION_IS_NOT_PLAYABLE)
	ENDIF
	IF g_FMMC_STRUCT.bMissionIsPublished 
		CLEAR_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_SAVE)
	ELSE
		IF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_TITLE)
		OR IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_NO_CLOUD)
		OR ARE_ANY_RULES_EMPTY(sCurrentVarsStruct.iSaveFailTeam, sCurrentVarsStruct.iSaveFailRule)
			CLEAR_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_SAVE)
			IF NOT g_FMMC_STRUCT.bMissionIsPublished
				CLEAR_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_PUBLISH)
			ENDIF
		ELSE
			SET_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_SAVE)
		ENDIF
	ENDIF
		
	INT iTeam, iRule
	FOR iTeam = 0 TO (g_FMMC_STRUCT.iMaxNumberOfTeams - 1)
		IF g_CreatorsSelDetails.sTeamOptions[iTeam].iNumberOfActiveRows = 0
			SET_BIT(iFailBitSet, ciFAIL_BIT_SET_NOT_ANY_RULES_TEAM_1 + iTeam)
		ENDIF
	ENDFOR
	
	//Check to see if we need a wee drop off!
	FOR iTeam = 0 TO (g_FMMC_STRUCT.iMaxNumberOfTeams - 1)
		IF g_CreatorsSelDetails.sTeamOptions[iTeam].iNumberOfActiveRows != 0
			FOR iRule = 0 TO (g_CreatorsSelDetails.sTeamOptions[iTeam].iNumberOfActiveRows -1)
				IF IS_VECTOR_ZERO(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vDropOff[iRule])
					IF g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[iRule].iRule = ciSELECTION_COLLECT_PED
					OR g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[iRule].iRule = ciSELECTION_COLLECT_VEHICLE
					OR g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[iRule].iRule = ciSELECTION_COLLECT_OBJECT					
					OR g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[iRule].iRule = ciSELECTION_COLLECT_HOLD_OBJECT					
					OR g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[iRule].iRule = ciSELECTION_COLLECT_HOLD_PED
					OR g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[iRule].iRule = ciSELECTION_COLLECT_HOLD_VEHICLE
						IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDropOffType[iRule] = ciFMMC_DROP_OFF_TYPE_CYLINDER
						OR g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDropOffType[iRule] = ciFMMC_DROP_OFF_TYPE_AREA
						OR(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDropOffType[iRule] = ciFMMC_DROP_OFF_TYPE_VEHICLE AND g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDropOff_Vehicle[iRule] = -1)
							PRINTLN("No Drop off for rule ", iRule, " for team ", iTeam)
							SET_BIT(iFailBitSet, ciFAIL_BIT_SET_NO_DOFF_BUT_NEEDED_TEAM_1 + iTeam)
						ENDIF
					ENDIF
				ENDIF			
			ENDFOR
		ENDIF
	ENDFOR
	
	PRINTLN("iFailBitSet: ", iFailBitSet)
	
	IF iFailBitSet = 0
		SET_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_TEST)	
	ELSE
		CLEAR_BIT(sFMMCmenu.iTopMenuActive, TOP_MENU_TEST)		
	ENDIF
	
ENDPROC

PROC UNLOCK_MENU_ITEMS_AND_ALLOW_MISSION_TEST()
	iTestFailBitSet = 0
    SET_FAIL_BIT_SET(iTestFailBitSet)//, TRUE)
	PRINTLN("SET_ALL_MISSION_MENU_ITEMS_ACTIVE iFailBitSet = ", iTestFailBitSet)
    SET_ALL_MISSION_MENU_ITEMS_ACTIVE(sFMMCmenu.iBitActive)//, iFailBitSet)
ENDPROC

PROC CLEANUP_MAP_SWAP(SELECTION_DETAILS_STRUCT &g_CreatorsSelDetailsPassed)
	IF g_CreatorsSelDetailsPassed.bMapSwapped = TRUE 
		IF GET_DPADDOWN_ACTIVATION_STATE() = DPADDOWN_SECOND
			SET_DPADDOWN_ACTIVATION_STATE(DPADDOWN_FIRST)
			g_CreatorsSelDetailsPassed.bMapSwapped = FALSE
		ENDIF
	ENDIF
ENDPROC
PROC SETUP_OUTOF_BOUNDS_BLIP(SELECTION_DETAILS_STRUCT &g_CreatorsSelDetailsPassed)
	SET_BLIP_ALPHA(g_CreatorsSelDetailsPassed.biBlipBounds, 180)
	SET_BLIP_COLOUR(g_CreatorsSelDetailsPassed.biBlipBounds, BLIP_COLOUR_RED)
	SHOW_HEIGHT_ON_BLIP(g_CreatorsSelDetailsPassed.biBlipBounds, FALSE)
ENDPROC

FUNC BOOL DOES_THIS_RULE_HAVE_TWO_TEXT_COLOURS(INT iRule)
	SWITCH iRule
		CASE ciSELECTION_COLLECT_PED
		CASE ciSELECTION_COLLECT_VEHICLE
		CASE ciSELECTION_COLLECT_OBJECT
		CASE ciSELECTION_COLLECT_HOLD_OBJECT
		CASE ciSELECTION_COLLECT_HOLD_PED
		CASE ciSELECTION_COLLECT_HOLD_VEHICLE
			RETURN TRUE
	ENDSWITCH
	RETURN FALSE
ENDFUNC

PROC UP_DATE_GLOBAL_AFTER_RULE_CHANGE(INT iType, INT iOrder)
	PRINTLN("UP_DATE_GLOBAL_AFTER_RULE_CHANGE")
	INT iEntity
	SWITCH iType
		//Peds
		CASE ciRULE_TYPE_PED
			FOR iEntity = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfPeds - 1)
				IF FMMC_IS_LONG_BIT_SET(g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[iOrder].iEntityBitSet, iEntity)
					g_FMMC_STRUCT_ENTITIES.sPlacedPed[iEntity].iRule[g_CreatorsSelDetails.iSelectedTeam]		= GET_RULE_FROM_RULE_SELECTION(g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[iOrder].iRule)
					g_FMMC_STRUCT_ENTITIES.sPlacedPed[iEntity].iPriority[g_CreatorsSelDetails.iSelectedTeam] 	= iOrder
				ENDIF
			ENDFOR
		BREAK
		
		//Vehicles
		CASE ciRULE_TYPE_VEHICLE	
			FOR iEntity = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles - 1)
				IF FMMC_IS_LONG_BIT_SET(g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[iOrder].iEntityBitSet, iEntity)
					g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iEntity].iRule[g_CreatorsSelDetails.iSelectedTeam]		= GET_RULE_FROM_RULE_SELECTION(g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[iOrder].iRule)
					g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iEntity].iPriority[g_CreatorsSelDetails.iSelectedTeam] 	= iOrder
				ENDIF
			ENDFOR
		BREAK		
		
		//Objects
		CASE ciRULE_TYPE_OBJECT		
			FOR iEntity = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfObjects - 1)
				IF FMMC_IS_LONG_BIT_SET(g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[iOrder].iEntityBitSet, iEntity)
					g_FMMC_STRUCT_ENTITIES.sPlacedObject[iEntity].iRule[g_CreatorsSelDetails.iSelectedTeam]		= GET_RULE_FROM_RULE_SELECTION(g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[iOrder].iRule)
					g_FMMC_STRUCT_ENTITIES.sPlacedObject[iEntity].iPriority[g_CreatorsSelDetails.iSelectedTeam]	= iOrder
				ENDIF
			ENDFOR
		BREAK		
		
		//GOTO
		CASE ciRULE_TYPE_GOTO		
			FOR iEntity = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfGoToLocations[0] - 1)
				IF FMMC_IS_LONG_BIT_SET(g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[iOrder].iEntityBitSet, iEntity)
					g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iEntity].iRule[g_CreatorsSelDetails.iSelectedTeam] 		= GET_RULE_FROM_RULE_SELECTION(g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[iOrder].iRule)
					g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iEntity].iPriority[g_CreatorsSelDetails.iSelectedTeam]	= iOrder
				ENDIF
			ENDFOR
		BREAK	

		//KILL PLAYER
		CASE ciRULE_TYPE_PLAYER		
		CASE ciRULE_TYPE_GET_MASKS		
			FOR iEntity = 0 TO (g_FMMC_STRUCT.iNumberOfPlayerRules[g_CreatorsSelDetails.iSelectedTeam] - 1)
				IF FMMC_IS_LONG_BIT_SET(g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[iOrder].iEntityBitSet, iEntity)
					g_FMMC_STRUCT.sPlayerRuleData[iEntity].iRule[g_CreatorsSelDetails.iSelectedTeam] 		= GET_RULE_FROM_RULE_SELECTION(g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[iOrder].iRule)
					PRINTLN("PD UP_DATE_GLOBAL_AFTER_RULE_CHANGE g_FMMC_STRUCT.sPlayerRuleData[",iEntity,"].iRule[",g_CreatorsSelDetails.iSelectedTeam,"] = ", g_FMMC_STRUCT.sPlayerRuleData[iEntity].iRule[g_CreatorsSelDetails.iSelectedTeam])
					g_FMMC_STRUCT.sPlayerRuleData[iEntity].iPriority[g_CreatorsSelDetails.iSelectedTeam]		= iOrder
					PRINTLN("PD UP_DATE_GLOBAL_AFTER_RULE_CHANGE g_FMMC_STRUCT.sPlayerRuleData[",iEntity,"].iPriority[",g_CreatorsSelDetails.iSelectedTeam,"] = ", g_FMMC_STRUCT.sPlayerRuleData[iEntity].iPriority[g_CreatorsSelDetails.iSelectedTeam])
				ENDIF
			ENDFOR
		BREAK	
	ENDSWITCH
ENDPROC

FUNC BOOL IS_THIS_A_PLAYER_RULE(INT iRule)
	IF iRule = ciSELECTION_PLAYER_RULE_KILL_ANY	
	OR iRule = ciSELECTION_PLAYER_RULE_KILL_TEAM0	
	OR iRule = ciSELECTION_PLAYER_RULE_KILL_TEAM1	
	OR iRule = ciSELECTION_PLAYER_RULE_KILL_TEAM2	
	OR iRule = ciSELECTION_PLAYER_RULE_KILL_TEAM3	
	OR iRule = ciSELECTION_PLAYER_RULE_GET_MASKS	
	OR iRule = ciSELECTION_PLAYER_RULE_SCRIPTED_CUTSCENE
	OR iRule = ciSELECTION_PLAYER_RULE_ARREST_TEAM0
	OR iRule = ciSELECTION_PLAYER_RULE_ARREST_TEAM1
	OR iRule = ciSELECTION_PLAYER_RULE_ARREST_TEAM2
	OR iRule = ciSELECTION_PLAYER_RULE_ARREST_TEAM3
	OR iRule = ciSELECTION_PLAYER_RULE_ARREST_ALL
	OR iRule = ciSELECTION_PLAYER_RULE_GO_TO_TEAM0
	OR iRule = ciSELECTION_PLAYER_RULE_GO_TO_TEAM1
	OR iRule = ciSELECTION_PLAYER_RULE_GO_TO_TEAM2
	OR iRule = ciSELECTION_PLAYER_RULE_GO_TO_TEAM3
	OR iRule = ciSELECTION_PLAYER_RULE_LOOT_THRESHOLD
	OR iRule = ciSELECTION_PLAYER_RULE_POINTS_THRESHOLD
	OR iRule = ciSELECTION_PLAYER_RULE_HOLDING_RULE
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

//(Probs don't need this..)
FUNC STRING GET_WEAPON_LIBRARY_NAME_FROM_INT(INT iIndex)
	SWITCH iIndex
		CASE 0							RETURN "FMMC_GRLT_LIB_0"
		CASE 1							RETURN "FMMC_GRLT_LIB_1"
		CASE 2							RETURN "FMMC_GRLT_LIB_2"
		CASE 3							RETURN "FMMC_GRLT_LIB_3"
		CASE 4							RETURN "FMMC_GRLT_LIB_4"
		CASE 5							RETURN "FMMC_GRLT_LIB_5"
		CASE 6							RETURN "FMMC_GRLT_LIB_6"
		CASE 7							RETURN "FMMC_GRLT_LIB_7"
		CASE 8 							RETURN "FMMC_GRLT_LIB_8"
	ENDSWITCH
	RETURN ""
ENDFUNC

PROC REMOVE_EXTRA_LINKED_DOORS_FROM_RULES()
	INT iEnt, iTeam 
	FOR iEnt = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfPeds - 1
		FOR iTeam = 0 TO FMMC_MAX_TEAMS - 1
			IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iEnt].iPriority[iTeam] != -1
			AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iEnt].iPriority[iTeam] < FMMC_MAX_RULES
				IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iEnt].iRule[iTeam] != FMMC_OBJECTIVE_LOGIC_MINIGAME
					IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iLinkedDoor[g_FMMC_STRUCT_ENTITIES.sPlacedPed[iEnt].iPriority[iTeam]] != -1
						g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iLinkedDoor[g_FMMC_STRUCT_ENTITIES.sPlacedPed[iEnt].iPriority[iTeam]] = -1
					ENDIF
					IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iLinkedDoor2[g_FMMC_STRUCT_ENTITIES.sPlacedPed[iEnt].iPriority[iTeam]] != -1
						g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iLinkedDoor2[g_FMMC_STRUCT_ENTITIES.sPlacedPed[iEnt].iPriority[iTeam]] = -1
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
	ENDFOR
	FOR iEnt = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles - 1
		FOR iTeam = 0 TO FMMC_MAX_TEAMS - 1
			IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iEnt].iPriority[iTeam] != -1
			AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iEnt].iPriority[iTeam] < FMMC_MAX_RULES
				IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iEnt].iRule[iTeam] != FMMC_OBJECTIVE_LOGIC_MINIGAME
					IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iLinkedDoor[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iEnt].iPriority[iTeam]] != -1
						g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iLinkedDoor[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iEnt].iPriority[iTeam]] = -1
					ENDIF
					IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iLinkedDoor2[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iEnt].iPriority[iTeam]] != -1
						g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iLinkedDoor2[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iEnt].iPriority[iTeam]] = -1
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
	ENDFOR
	FOR iEnt = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfObjects - 1
		FOR iTeam = 0 TO FMMC_MAX_TEAMS - 1
			IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iEnt].iPriority[iTeam] != -1
			AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[iEnt].iPriority[iTeam] < FMMC_MAX_RULES
				IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iEnt].iRule[iTeam] != FMMC_OBJECTIVE_LOGIC_MINIGAME
					IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iLinkedDoor[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iEnt].iPriority[iTeam]] != -1
						g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iLinkedDoor[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iEnt].iPriority[iTeam]] = -1
					ENDIF
					IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iLinkedDoor2[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iEnt].iPriority[iTeam]] != -1
						g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iLinkedDoor2[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iEnt].iPriority[iTeam]] = -1
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
	ENDFOR
	FOR iTeam = 0 TO FMMC_MAX_TEAMS - 1
		FOR iEnt = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfGoToLocations[iTeam] - 1
			IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iEnt].iPriority[iTeam] != -1
			AND g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iEnt].iPriority[iTeam] < FMMC_MAX_RULES
				IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iEnt].iRule[iTeam] != FMMC_OBJECTIVE_LOGIC_MINIGAME
					IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iLinkedDoor[g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iEnt].iPriority[iTeam]] != -1
						g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iLinkedDoor[g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iEnt].iPriority[iTeam]] = -1
					ENDIF
					IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iLinkedDoor2[g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iEnt].iPriority[iTeam]] != -1
						g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iLinkedDoor2[g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iEnt].iPriority[iTeam]] = -1
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
	ENDFOR
	FOR iTeam = 0 TO FMMC_MAX_TEAMS - 1
		FOR iEnt = 0 TO g_FMMC_STRUCT.iNumberOfPlayerRules[iTeam]-1	
			IF g_FMMC_STRUCT.sPlayerRuleData[iEnt].iPriority[iTeam] != -1
			AND g_FMMC_STRUCT.sPlayerRuleData[iEnt].iPriority[iTeam] < FMMC_MAX_RULES
			
				IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iLinkedDoor[g_FMMC_STRUCT.sPlayerRuleData[iEnt].iPriority[iTeam]] != -1
					g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iLinkedDoor[g_FMMC_STRUCT.sPlayerRuleData[iEnt].iPriority[iTeam]] = -1
				ENDIF
				IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iLinkedDoor2[g_FMMC_STRUCT.sPlayerRuleData[iEnt].iPriority[iTeam]] != -1
					g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iLinkedDoor2[g_FMMC_STRUCT.sPlayerRuleData[iEnt].iPriority[iTeam]] = -1
				ENDIF
			ENDIF
		ENDFOR
	ENDFOR
ENDPROC

FUNC INT CONVERT_FMMC_PLAYER_RULE_TYPE_TO_SELECTION(INT iRule)
	SWITCH iRule
		CASE FMMC_OBJECTIVE_LOGIC_KILL_PLAYERS			RETURN	 ciSELECTION_PLAYER_RULE_KILL_ANY
		CASE FMMC_OBJECTIVE_LOGIC_KILL_TEAM0  			RETURN	 ciSELECTION_PLAYER_RULE_KILL_TEAM0
		CASE FMMC_OBJECTIVE_LOGIC_KILL_TEAM1  			RETURN	 ciSELECTION_PLAYER_RULE_KILL_TEAM1
		CASE FMMC_OBJECTIVE_LOGIC_KILL_TEAM2  			RETURN	 ciSELECTION_PLAYER_RULE_KILL_TEAM2
		CASE FMMC_OBJECTIVE_LOGIC_KILL_TEAM3  			RETURN	 ciSELECTION_PLAYER_RULE_KILL_TEAM3
		CASE FMMC_OBJECTIVE_LOGIC_GET_MASKS  			RETURN	 ciSELECTION_PLAYER_RULE_GET_MASKS
		CASE FMMC_OBJECTIVE_LOGIC_SCRIPTED_CUTSCENE  	RETURN	 ciSELECTION_PLAYER_RULE_SCRIPTED_CUTSCENE
		CASE FMMC_OBJECTIVE_LOGIC_ARREST_TEAM0  		RETURN	 ciSELECTION_PLAYER_RULE_ARREST_TEAM0
		CASE FMMC_OBJECTIVE_LOGIC_ARREST_TEAM1  		RETURN	 ciSELECTION_PLAYER_RULE_ARREST_TEAM1
		CASE FMMC_OBJECTIVE_LOGIC_ARREST_TEAM2  		RETURN	 ciSELECTION_PLAYER_RULE_ARREST_TEAM2
		CASE FMMC_OBJECTIVE_LOGIC_ARREST_TEAM3  		RETURN	 ciSELECTION_PLAYER_RULE_ARREST_TEAM3
		CASE FMMC_OBJECTIVE_LOGIC_ARREST_ALL  			RETURN	 ciSELECTION_PLAYER_RULE_ARREST_ALL
		CASE FMMC_OBJECTIVE_LOGIC_LOOT_THRESHOLD  		RETURN	 ciSELECTION_PLAYER_RULE_LOOT_THRESHOLD
		CASE FMMC_OBJECTIVE_LOGIC_POINTS_THRESHOLD  	RETURN	 ciSELECTION_PLAYER_RULE_POINTS_THRESHOLD
		CASE FMMC_OBJECTIVE_LOGIC_HOLDING_RULE  		RETURN	 ciSELECTION_PLAYER_RULE_HOLDING_RULE
	ENDSWITCH
	RETURN -1
ENDFUNC

FUNC BOOL DOES_ENTITY_HAVE_THIS_RULE_WITH_THIS_PRIORITY(INT iTeam, INT iPriority)
	INT iCount	
	IF g_FMMC_STRUCT_ENTITIES.iNumberOfPeds > 0
		FOR iCount = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfPeds - 1)
			IF iPriority = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iCount].iPriority[iTeam]
				PRINTLN("g_CreatorsSelDetails.sTeamOptions[", iTeam, "].sSelection[", iPriority, "].iRule = ", g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[iPriority].iRule)
				PRINTLN("GET_RULE_SELECTION_FROM_RULE(g_FMMC_STRUCT_ENTITIES.sPlacedPed[", iCount, "].iRule[", iTeam, "], CREATION_TYPE_PEDS) = ", GET_RULE_SELECTION_FROM_RULE(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iCount].iRule[iTeam], CREATION_TYPE_PEDS))
				IF g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[iPriority].iRule = GET_RULE_SELECTION_FROM_RULE(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iCount].iRule[iTeam], CREATION_TYPE_PEDS)
					PRINTLN("DOES_ENTITY_HAVE_THIS_RULE_WITH_THIS_PRIORITY PEDS TRUE")
					RETURN TRUE
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	IF g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles > 0
		FOR iCount = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles - 1)
			IF iPriority = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iCount].iPriority[iTeam]
				IF g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[iPriority].iRule = GET_RULE_SELECTION_FROM_RULE(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iCount].iRule[iTeam], CREATION_TYPE_VEHICLES)
					PRINTLN("DOES_ENTITY_HAVE_THIS_RULE_WITH_THIS_PRIORITY VEHICLES TRUE")
					RETURN TRUE
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	IF g_FMMC_STRUCT_ENTITIES.iNumberOfObjects > 0
		FOR iCount = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfObjects - 1)
			IF iPriority = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iCount].iPriority[iTeam]
				IF g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[iPriority].iRule = GET_RULE_SELECTION_FROM_RULE(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iCount].iRule[iTeam], CREATION_TYPE_OBJECTS)
					PRINTLN("DOES_ENTITY_HAVE_THIS_RULE_WITH_THIS_PRIORITY OBJECTS TRUE")
					RETURN TRUE
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	IF g_FMMC_STRUCT_ENTITIES.iNumberOfGoToLocations[0] > 0
		PRINTLN("g_FMMC_STRUCT_ENTITIES.iNumberOfGoToLocations[0] = ", g_FMMC_STRUCT_ENTITIES.iNumberOfGoToLocations[0])
		FOR iCount = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfGoToLocations[0] - 1)
			PRINTLN("iCount = ", iCount)
			PRINTLN("iPriority = ", iPriority)
			PRINTLN("g_FMMC_STRUCT_ENTITIES.sGotoLocationData[", iCount, "].iPriority[",iTeam,"] = ",  g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iCount].iPriority[iTeam])
			IF iPriority = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iCount].iPriority[iTeam]
				PRINTLN("g_CreatorsSelDetails.sTeamOptions[", iTeam, "].sSelection[", iPriority, "].iRule = ", g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[iPriority].iRule)
				PRINTLN("g_FMMC_STRUCT_ENTITIES.sGotoLocationData[", iCount, "].iRule[", iTeam, "] = ", g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iCount].iRule[iTeam])
				PRINTLN("GET_RULE_SELECTION_FROM_RULE(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[", iCount, "].iRule[", iTeam, "], CREATION_TYPE_GOTO_LOC) = ", GET_RULE_SELECTION_FROM_RULE(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iCount].iRule[iTeam], CREATION_TYPE_GOTO_LOC))
				IF g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[iPriority].iRule = GET_RULE_SELECTION_FROM_RULE(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iCount].iRule[iTeam], CREATION_TYPE_GOTO_LOC)
					PRINTLN("DOES_ENTITY_HAVE_THIS_RULE_WITH_THIS_PRIORITY GO TO LOCATION TRUE")
					RETURN TRUE
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	
	PRINTLN("g_CreatorsSelDetails.sTeamOptions[",iTeam, "].sSelection[", iPriority, "].iRule = ", g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[iPriority].iRule)
	PRINTLN("iPriority = ", iPriority)
	IF g_FMMC_STRUCT.iNumberOfPlayerRules[iTeam] > 0
		FOR iCount = 0 TO (g_FMMC_STRUCT.iNumberOfPlayerRules[iTeam] - 1)
			PRINTLN("g_FMMC_STRUCT.sPlayerRuleData[", iCount, "].iPriority[", iTeam, "] = ", g_FMMC_STRUCT.sPlayerRuleData[iCount].iPriority[iTeam])
			IF iPriority = g_FMMC_STRUCT.sPlayerRuleData[iCount].iPriority[iTeam]
				PRINTLN("CONVERT_FMMC_PLAYER_RULE_TYPE_TO_SELECTION(g_FMMC_STRUCT.sPlayerRuleData[", iPriority, "].iRule[", iTeam, "]) = ", CONVERT_FMMC_PLAYER_RULE_TYPE_TO_SELECTION(g_FMMC_STRUCT.sPlayerRuleData[iCount].iRule[iTeam]))
				IF g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[iPriority].iRule = CONVERT_FMMC_PLAYER_RULE_TYPE_TO_SELECTION(g_FMMC_STRUCT.sPlayerRuleData[iCount].iRule[iTeam])
					PRINTLN("DOES_ENTITY_HAVE_THIS_RULE_WITH_THIS_PRIORITY PLAYER KILL RULE TRUE")
					RETURN TRUE
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	
	//Checking if the rule contains any Extra Objectives
	INT i, j
	FOR i = 0 TO MAX_NUM_EXTRA_OBJECTIVE_ENTITIES-1 
		IF g_FMMC_STRUCT.iExtraObjectiveEntityID[i] != -1
			FOR j = 0 TO MAX_NUM_EXTRA_OBJECTIVE_PER_ENTITY-1
				IF g_FMMC_STRUCT.iExtraObjectivePriority[i][j][iTeam] = iPriority
					PRINTLN("DOES_ENTITY_HAVE_THIS_RULE_WITH_THIS_PRIORITY Extra objective entitity rule RETURNING TRUE")
					RETURN TRUE
				ENDIF
			ENDFOR
		ENDIF
	ENDFOR
	PRINTLN("DOES_ENTITY_HAVE_THIS_RULE_WITH_THIS_PRIORITY FALSE")
	RETURN FALSE
ENDFUNC

FUNC STRING GET_FMMC_DOOR_STRING(INT i)
	SWITCH i
		CASE -1
			RETURN "FMMC_SS_RL_21A"
		BREAK
		CASE ciFMMC_DOOR_BANK_1
			RETURN "FMMC_SS_RL_21B"
		BREAK
		CASE ciFMMC_DOOR_BANK_VAULT
			RETURN "FMMC_SS_RL_21C"
		BREAK
		CASE ciFMMC_DOOR_BANK_LOBBY
			RETURN "FMMC_SS_RL_21K"
		BREAK
		CASE ciFMMC_DOOR_CHEMICAL_GARAGE
			RETURN "FMMC_SS_RL_21D"
		BREAK
		CASE ciFMMC_DOOR_VINEWOOD_GARAGE
			RETURN "FMMC_SS_RL_21E"
		BREAK
		CASE ciFMMC_DOOR_DOCK_CONTROL
			RETURN "FMMC_SS_RL_21F"
		BREAK
		CASE ciFMMC_DOOR_PLAYBOY
			RETURN "FMMC_SS_RL_21G"
		BREAK
		CASE ciFMMC_DOOR_DOCK_CONTROL_2
			RETURN "FMMC_SS_RL_21H"
		BREAK
		CASE ciFMMC_DOOR_GOV_FACILITY_GATE
			RETURN "FMMC_SS_RL_21I"
		BREAK
		CASE ciFMMC_DOOR_AIRPORT_GATE_1
			RETURN "FMMC_SS_RL_21J"
		BREAK
		CASE ciFMMC_DOOR_FLECCA_RFHILLS1
		CASE ciFMMC_DOOR_FLECCA_PERSHING1
		CASE ciFMMC_DOOR_FLECCA_CHUMASH1
			RETURN "FMMC_SS_RL_21M"
		BREAK
		CASE ciFMMC_DOOR_FLECCA_RFHILLS2
		CASE ciFMMC_DOOR_FLECCA_PERSHING2
		CASE ciFMMC_DOOR_FLECCA_CHUMASH2
			RETURN "FMMC_SS_RL_21N"
		BREAK
		CASE ciFMMC_DOOR_FLECCA_RFHILLSEXT
		CASE ciFMMC_DOOR_FLECCA_PERSHINGEXT
		CASE ciFMMC_DOOR_FLECCA_CHUMASHEXT
			RETURN "FMMC_SS_RL_21L"
		BREAK
		CASE ciFMMC_DOOR_BIOLAB_SHUTTERS_LEFT
			RETURN "FMMC_SS_RL_21O"
		BREAK
		CASE ciFMMC_DOOR_BIOLAB_SHUTTERS_RIGHT
			RETURN "FMMC_SS_RL_21P"
		BREAK
		CASE ciFMMC_DOOR_PRISON_GATE
			RETURN "FMMC_SS_RL_21Q"
		BREAK	
		CASE ciFMMC_DOOR_ORNATE_BANK_VAULT
			RETURN "FMMC_SS_RL_21R"
		BREAK
		CASE ciFMMC_DOOR_POLICE_STATION_SECURITY
			RETURN "FMMC_SS_RL_21S"
		BREAK
		CASE ciFMMC_DOOR_BIOLAB_LAB1_DOOR_LEFT
			RETURN "FMMC_SS_RL_21T"
		BREAK
		CASE ciFMMC_DOOR_BIOLAB_LAB1_DOOR_RIGHT
			RETURN "FMMC_SS_RL_21U"
		BREAK
		CASE ciFMMC_DOOR_PRISON_MANSION_GATE_LEFT
			RETURN "FMMC_SS_RL_21V"
		BREAK
		CASE ciFMMC_DOOR_PRISON_MANSION_GATE_RIGHT
			RETURN "FMMC_SS_RL_21W"
		BREAK
		CASE ciFMMC_DOOR_POLICE_LOCKER_ROOM
			RETURN "FMMC_SS_RL_21X"
		BREAK
		CASE ciFMMC_DOOR_POLICE_LOCKER_ROOM_2
			RETURN "FMMC_SS_RL_21Y"
		BREAK
		CASE ciFMMC_DOOR_DOCKS_GATE_RIGHT
			RETURN "FMMC_DR_DGR"
		BREAK
		CASE ciFMMC_DOOR_DOCKS_GATE_LEFT
			RETURN "FMMC_DR_DGL"
		BREAK
	ENDSWITCH
	
	RETURN "FMMC_SS_RL_21A"
ENDFUNC

FUNC INT GET_FIRST_BIT_SET_IN_BITSET(INT iBitset)
	INT i
	FOR i = 0 TO 31
		IF IS_BIT_SET(iBitset, i)
			RETURN i
		ENDIF
	ENDFOR
	
	RETURN -1
ENDFUNC

FUNC BOOL IS_DROP_OFF_TYPE_VALID_FOR_RULE_TYPE(INT iDropOffType, INT iRule)
		
	IF iRule = ciSELECTION_COLLECT_VEHICLE
	OR iRule = ciSELECTION_COLLECT_HOLD_VEHICLE
	OR iRule = ciSELECTION_COLLECT_PED
	OR iRule = ciSELECTION_COLLECT_OBJECT
	OR iRule = ciSELECTION_COLLECT_HOLD_PED
	OR iRule = ciSELECTION_COLLECT_HOLD_OBJECT
		RETURN TRUE
	ELSE
		IF iDropOffType < ciFMMC_DROP_OFF_TYPE_PROPERTY
		OR iDropOffType >= ciFMMC_DROP_OFF_TYPE_SPHERE
			RETURN TRUE
		ENDIF
	ENDIF
	
	PRINTLN("[DropOffs] IS_DROP_OFF_TYPE_VALID_FOR_RULE_TYPE | Returning FALSE for iDropOffType ", iDropOffType, " on rule ", iRule)
	RETURN FALSE	
ENDFUNC

PROC CHANGE_AREA_TYPE(INT &iType, INT iChange, INT iRuleType)
	EDIT_CREATOR_INT_MENU_ITEM(sFMMCEndstage, iType, iChange, ciFMMC_DROP_OFF_TYPE_MAX, 0, TRUE, FALSE)
	IF NOT IS_DROP_OFF_TYPE_VALID_FOR_RULE_TYPE(iType, iRuleType)
		CHANGE_AREA_TYPE(iType, iChange, iRuleType)
	ENDIF
ENDPROC

PROC RESET_UP_HELP()

	DEBUG_PRINTCALLSTACK()

	INT iBitSet, iBitSet2
	REMOVE_MENU_HELP_KEYS()
	
	IF sFMMCMenu.bRuleMenuActive	 = FALSE
	
		IF sFMMCMenu.sActiveMenu = eFmmc_ASS_OBJECTIVE_MENU_SET_PED_GOTO_POS
		OR sFMMCMenu.sActiveMenu = eFmmc_ASS_OBJECTIVE_MENU_SET_POOL_GOTO_POS
		OR sFMMCMenu.sActiveMenu = eFMMC_ASS_OBJECTIVE_EXIT_VEHICLE_OPTIONS
		OR sFMMCMenu.sActiveMenu = eFMMC_ASS_OBJECTIVE_COVER_ONLY
		OR sFMMCMenu.sActiveMenu = eFMMC_ASS_OBJECTIVE_USE_COMBAT_GOTO
		OR sFMMCMenu.sActiveMenu = eFMMC_ASS_OBJECTIVE_WAIT_RULE_OPTIONS
		OR sFMMCMenu.sActiveMenu = eFMMC_ASS_OBJECTIVE_WAIT_TIME_OPTIONS
		OR sFMMCMenu.sActiveMenu = eFMMC_ASS_OBJECTIVE_TRIGGER_AREA
		OR sFMMCMenu.sActiveMenu = EFMMC_ASS_OBJECTIVE_SPEED_OPTIONS
		OR sFMMCMenu.sActiveMenu = EFMMC_ASS_OBJECTIVE_RADIUS_OPTIONS
		OR sFMMCMenu.sActiveMenu = eFMMC_ASS_OBJECTIVE_FORCE_NAVMESH
		OR sFMMCMenu.sActiveMenu = eFMMC_ASS_OBJECTIVE_VEHICLE_DISTANCE
		OR sFMMCMenu.sActiveMenu = eFMMC_ASS_OBJECTIVE_IGNORE_NAVMESH
		OR sFMMCMenu.sActiveMenu = eFMMC_ASS_OBJECTIVE_SLOW_AT_DISTANCE
		OR sFMMCMenu.sActiveMenu = eFMMC_ASS_OBJECTIVE_ACHIEVE_FINAL_HEADING
		OR sFMMCMenu.sActiveMenu = eFMMC_ASS_OBJECTIVE_PLANE_TAXI
		OR sFMMCMenu.sActiveMenu = eFMMC_ASS_OBJECTIVE_SPECIFIC_VEH_SETTINGS		
		
			iBitSet = 0
			iBitSet2 = 0
			
			IF sFMMCmenu.iCurrentGoToLocation != -1
			OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) != -1 
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_CANCEL)
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_DELETE)
			ELSE
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_BACK)
			ENDIF
			INT iClosest = GET_CLOSEST_PED_ASS_OBJ_GOTO_LOCATION(sFMMCmenu, sCurrentVarsStruct.vCoronaPos)
			
			IF VDIST(GET_ASSOCIATED_GOTO_TASK_DATA__POSITION(-1, sFMMCmenu.sAssociatedGotoTaskData, iClosest), sCurrentVarsStruct.vCoronaPos) < 0.75
			AND sFMMCmenu.iCurrentGoToLocation = -1
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_EDIT)
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_DELETE)
			ELIF IS_PED_ASS_OBJ_LINE_SELECTED(sFMMCmenu.sAssociatedGotoTaskData, sCurrentVarsStruct.vCoronaPos, iClosest)
			AND GET_ASSOCIATED_GOTO_TASK_DATA__NUMBER_OF_GOTOS(-1, sFMMCmenu.sAssociatedGotoTaskData) < MAX_ASSOCIATED_GOTO_TASKS
			AND GET_ASSOCIATED_GOTO_TASK_DATA__NUMBER_OF_GOTOS(-1, sFMMCmenu.sAssociatedGotoTaskData) > 1
			AND sFMMCmenu.iCurrentGoToLocation = -1
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_INSERT)
			ELSE
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_PLACE)
			ENDIF
			
			IF sFMMCMenu.sActiveMenu = EFMMC_ASS_OBJECTIVE_RADIUS_OPTIONS
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_ADJUST)
			ENDIF
					
		ELIF sFMMCMenu.sActiveMenu = eFmmc_ASS_OBJECTIVE_SECONDARY_GOTO_MENU
		
			iBitSet = 0
			
			IF NOT IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_ZOOM)	
			ENDIF
			
			IF sCurrentVarsStruct.bitsetOkToPlace = BS_CAN_PLACE	
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_SWITCH_CAM)
			ENDIF
			
			SET_BIT(iBitSet, FMMC_HELP_BUTTON_BACK)
			SET_BIT(iBitSet, FMMC_HELP_BUTTON_PLACE)
			SET_BIT(iBitSet, FMMC_HELP_BUTTON_HEIGHT)
			
			IF NOT IS_VECTOR_ZERO(sFMMCmenu.vPedSecondaryGotoPos)
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_DELETE)
			ENDIF
			
		ELIF sFMMCMenu.sActiveMenu = efMMC_CUTSCENE_ENTITIES 
		OR   sFMMCMenu.sActiveMenu = efMMC_CUTSCENE_MOCAP_ENTITIES
		
			SET_BIT(iBitSet, FMMC_HELP_BUTTON_BACK)
			SET_BIT(iBitSet, FMMC_HELP_BUTTON_ZOOM)
			SET_BIT(iBitSet, FMMC_HELP_BUTTON_SELECT)
			
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) > -1
			AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) < FMMC_MAX_CUTSCENE_ENTITIES
				IF g_FMMC_STRUCT.sCurrentMocapSceneData.sCutsceneEntities[GET_CREATOR_MENU_SELECTION(sFMMCmenu)].iType != -1
					IF sFMMCMenu.sActiveMenu = efMMC_CUTSCENE_MOCAP_ENTITIES
						SET_BIT(iBitSet, FMMC_HELP_BUTTON_OPTIONS)
						SET_BIT(iBitSet, FMMC_HELP_BUTTON_DELETE)
					ENDIF
				ENDIF
				IF g_FMMC_STRUCT.sCurrentSceneData.sCutsceneEntities[GET_CREATOR_MENU_SELECTION(sFMMCmenu)].iType != -1
					IF sFMMCMenu.sActiveMenu = efMMC_CUTSCENE_ENTITIES
						SET_BIT(iBitSet, FMMC_HELP_BUTTON_OPTIONS)
						SET_BIT(iBitSet, FMMC_HELP_BUTTON_DELETE)
					ENDIF
				ENDIF
			ENDIF
			
		ELIF sFMMCMenu.sActiveMenu = eFmmc_END_OF_MISSION_END_CUTSCENE_ENTITIES
		
			SET_BIT(iBitSet, FMMC_HELP_BUTTON_BACK)
			SET_BIT(iBitSet, FMMC_HELP_BUTTON_ZOOM)
			SET_BIT(iBitSet, FMMC_HELP_BUTTON_SELECT)
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) > -1
			AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) < FMMC_MAX_CUTSCENE_ENTITIES
				IF g_FMMC_STRUCT.sEndMocapSceneData.sCutsceneEntities[GET_CREATOR_MENU_SELECTION(sFMMCmenu)].iType != -1
					SET_BIT(iBitSet, FMMC_HELP_BUTTON_OPTIONS)
					SET_BIT(iBitSet, FMMC_HELP_BUTTON_DELETE)
				ENDIF
			ENDIF
			
		ELSE
			IF IS_BIT_SET(iHelpBitSet, biPickupEntityButton)
			AND sFMMCMenu.iSelectedEntity = -1
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_EDIT)
			ELSE
				IF sFMMCmenu.sActiveMenu = eFmmc_TOP_MENU
				OR sFMMCmenu.sActiveMenu = eFmmc_MAIN_MENU_BASE
				OR sFMMCmenu.sActiveMenu = eFmmc_MAIN_OPTIONS_BASE
					IF IS_THIS_OPTION_SELECTABLE()
						IF IS_THIS_OPTION_A_MENU_GOTO()
						OR IS_THIS_OPTION_A_MENU_ACTION()
							SET_BIT(iBitSet, FMMC_HELP_BUTTON_SELECT)
						ENDIF
					ENDIF
				ELIF sFMMCmenu.sActiveMenu = eFmmc_DELETE_ENTITIES	
					SET_BIT(iBitSet, FMMC_HELP_BUTTON_SELECT)
				ELIF NOT IS_LONG_BIT_SET(sCurrentVarsStruct.iTooManyEntities, ciDRAW_CORONA)
				OR sFMMCMenu.iSelectedEntity != -1
					if sFMMCmenu.sActiveMenu != eFmmc_RADIO_MENU
					AND sCurrentVarsStruct.bitsetOkToPlace = BS_CAN_PLACE
						SET_BIT(iBitSet, FMMC_HELP_BUTTON_PLACE)
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_ZOOM)	
			ENDIF
			
			IF sCurrentVarsStruct.bitsetOkToPlace = BS_CAN_PLACE	
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_SWITCH_CAM)
			ENDIF
			
			IF IS_BIT_SET(iHelpBitSet, biDeleteEntityButton)
			AND sFMMCMenu.iSelectedEntity = -1
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_DELETE)		
			ENDIF
			
			IF IS_BIT_SET(iHelpBitSet, biRemovePedsButton)
			AND sFMMCMenu.iSelectedEntity = -1
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_REMOVE_PEDS)		
			ENDIF
			
			IF IS_BIT_SET(iHelpBitSet, biRemoveVehButton)
			AND sFMMCMenu.iSelectedEntity = -1
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_REMOVE_VEH)		
			ENDIF
			
			IF IS_BIT_SET(iHelpBitSet, biRemoveObjectsButton)
			AND sFMMCMenu.iSelectedEntity = -1
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_REMOVE_OBJECTS)		
			ENDIF
			
			IF IS_BIT_SET(iHelpBitset, biVectorInputTextButton)
				SET_BIT(iBitSet2, FMMC_HELP2_BUTTON_INPUT_VECTOR_MANUALLY)
			ENDIF
			
			IF IS_BIT_SET(iHelpBitset, biEditDataInputTextButton)
				SET_BIT(iBitSet2, FMMC_HELP2_BUTTON_EDIT_DATA)
			ENDIF
			
			IF IS_THIS_OPTION_SELECTABLE()
			AND IS_THIS_OPTION_TOGGLEABLE()
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_ADJUST)
			ENDIF
			
			IF IS_MULTI_TEAMS_MENU(sFMMCmenu)
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_SWITCH_TEAMS)
			ENDIF
			
			IF sFMMCmenu.sActiveMenu = eFmmc_VEH_DROPOFF_DELIVERY_OPTIONS
			AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_DROPOFF_DELIVERY_OVERRIDE_POS
			AND NOT IS_VECTOR_ZERO(sFMMCMenu.vVehDropOffOverride)
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_FOCUS_CAMERA)
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_DELETE)
			ENDIF
			
			IF sFMMCmenu.iCurrentMenuLength > 1
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_SCROLL)
			ENDIF		
			
			IF sFMMCmenu.sMenuBack != eFmmc_Null_item
				IF sFMMCMenu.iSelectedEntity = -1
					SET_BIT(iBitSet, FMMC_HELP_BUTTON_BACK)
				ELSE
					SET_BIT(iBitSet, FMMC_HELP_BUTTON_CANCEL)
				ENDIF
			ENDIF
			
			IF g_CreatorsSelDetails.iSelectedTeam > -1
			AND CAN_HEIGHT_BE_SET_FOR_ENTITY(sFMMCmenu, sFMMCmenu.iEntityCreation, IS_THIS_CURRENT_RHM_MENU(eRHM_DropOff) AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iDropOffBitSet, g_CreatorsSelDetails.iRow))
			AND NOT IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_HEIGHT)
			ENDIF
			
			IF sFMMCmenu.sActiveMenu = eFmmc_ENTITY_SHARED_SPAWN_GROUP_SETTINGS
			OR sFMMCmenu.sActiveMenu = eFMMC_DEFINE_SPAWN_GROUP_ACTIVATION			
			OR sFMMCmenu.sActiveMenu = eFmmc_ENTITY_SHARED_MODIFY_SPAWN_GROUP_ON_EVENT_SETTINGS
				SET_BIT(iBitset2, FMMC_HELP2_JUMP_TO_MENU_ON_DELETE)
			ENDIF
			
			IF IS_MENU_A_CUSTOM_VARIABLE_DATA_INDEX_SELECTOR_MENU(sFMMCmenu.sActiveMenu)
				SET_BIT(iBitSet2, FMMC_HELP2_MOVE_INDEX_WITH_LB_RB)
				SET_BIT(iBitSet2, FMMC_HELP2_CLEAR_INDEX_WITH_DELETE_BUTTON)
			ENDIF
			
			IF NOT IS_LONG_BIT_SET(sCurrentVarsStruct.iTooManyEntities, ciDRAW_CORONA)
			OR sFMMCMenu.iSelectedEntity != -1
				IF sFMMCmenu.sActiveMenu = eFmmc_ENEMY_BASE
				OR sFMMCmenu.sActiveMenu = eFmmc_VEHICLES_BASE
				OR sFMMCmenu.sActiveMenu = eFmmc_PROP_BASE
				OR sFMMCmenu.sActiveMenu = eFmmc_DYNOPROP_BASE
				OR sFMMCmenu.sActiveMenu = eFmmc_OBJECT_BASE
				OR sFMMCmenu.sActiveMenu = eFmmc_LOCATION_BASE
				OR sFMMCmenu.sActiveMenu = eFmmc_DM_TEAM_SPAWN_POINT
					SET_BIT(iBitSet, FMMC_HELP_BUTTON_ROTATE)
				ENDIF
			ENDIF
			
			IF sFMMCmenu.sActiveMenu = eFmmc_PAN_CAM_BASE		// I've added this clause to update PAN_CAM_BASE and PHOTO_CAM_BASE handling to match the race/deathmtach creator.
			OR sFMMCmenu.sActiveMenu = eFmmc_PHOTO_CAM_BASE
				iBitSet = 0
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_CAMERA_HEIGHT)
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_SCROLL)
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_BACK)
				
				IF sFMMCmenu.sActiveMenu = eFmmc_PAN_CAM_BASE
					IF NOT DOES_BLIP_EXIST(bCameraTriggerBlip)
						IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 0
							SET_BIT(iBitSet, FMMC_HELP_BUTTON_PLACE)
						ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 1
							SET_BIT(iBitSet, FMMC_HELP_BUTTON_SELECT)
						ENDIF	
					ENDIF
				ELSE
					IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 0
						SET_BIT(iBitSet, FMMC_HELP_BUTTON_SELECT)
					ENDIF	
					SET_BIT(iBitSet, FMMC_HELP_BUTTON_RESET_FOV)
					SET_BIT(iBitSet, FMMC_HELP_BUTTON_CAMERA_FOV)			
				ENDIF
				
			ELIF sFMMCmenu.sActiveMenu = eFmmc_DM_TEAM_CAMERAS
			OR   sFMMCmenu.sActiveMenu = eFmmc_OUT_CAM_BASE		// I'm leaving the DM_TEAM_CAMERAS and OUT_CAM_BASE settings as they are, as I don't know if they should be changed to match. (SamH)
			
				iBitSet = 0
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_CAMERA_HEIGHT)
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_SCROLL)
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_BACK)
				IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) > -1
				AND sFMMCmenu.sMenuAction[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] = eFmmc_Action_Preview_Pan_Cam
					IF sFMMCmenu.sActiveMenu != eFmmc_PHOTO_CAM_BASE
						SET_BIT(iBitSet, FMMC_HELP_BUTTON_SELECT)
					ENDIF
				ELSE
					SET_BIT(iBitSet, FMMC_HELP_BUTTON_PLACE)
				ENDIF
				
			ENDIF
			
			IF sFMMCmenu.sActiveMenu = efMMC_CUTSCENE_SET_CAM_1 		// I've added this clause to update PAN_CAM_BASE and PHOTO_CAM_BASE handling to match the race/deathmtach creator.
			OR sFMMCmenu.sActiveMenu = efMMC_CUTSCENE_SET_CAM_2
				iBitSet = 0
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_RESET_FOV)
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_CAMERA_FOV)
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_PLACE)
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_BACK)
			ENDIF
			
			IF sFMMCmenu.sActiveMenu = efMMC_CUTSCENE_SET_CAM_3
				iBitSet = 0
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_PLACE)
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_BACK)
			ENDIF
			
			IF sFMMCMenu.sActiveMenu = eFmmc_ASS_OBJECTIVE_MENU
			AND sFMMCmenu.iEntityCreation = CREATION_TYPE_PEDS
				INT iPed = g_FMMC_STRUCT_ENTITIES.iNumberOfPeds
				IF sFMMCMenu.iSelectedEntity != -1
					iPed = g_FMMC_STRUCT_ENTITIES.iNumberOfPeds				
				ENDIF
				IF iPed < FMMC_MAX_PEDS
					IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_MENU_PED_COVER_FROM_POINT
					AND NOT IS_VECTOR_ZERO(sFMMCmenu.vCurrentCoverFromPos)
						SET_BIT(iBitSet, FMMC_HELP_BUTTON_DELETE)
					ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_MENU_PED_COVER_POINT
					AND NOT IS_VECTOR_ZERO(sFMMCmenu.vCurrentCoverPos)
						SET_BIT(iBitSet, FMMC_HELP_BUTTON_DELETE)
					ENDIF
				ENDIF
			ENDIF
			
			IF sFMMCMenu.sActiveMenu = efmmc_INTERIOR_LIST
			OR sFMMCMenu.sActiveMenu = efmmc_INTERIOR_BUNKER
			OR sFMMCMenu.sActiveMenu = efmmc_INTERIOR_SUB
			OR sFMMCMenu.sActiveMenu = efmmc_INTERIOR_IAA
			OR sFMMCMenu.sActiveMenu = efmmc_ARENAMENUS_TOP
			OR sFMMCMenu.sActiveMenu = efmmc_CASINO_BASE
			OR sFMMCMenu.sActiveMenu = efmmc_CASINO_APARTMENT
			OR sFMMCMenu.sActiveMenu = efmmc_INTERIOR_NIGHTCLUB_BASE
			OR sFMMCMenu.sActiveMenu = efmmc_INTERIOR_CUSTOM_MOC
			OR sFMMCMenu.sActiveMenu = efmmc_INTERIOR_METH_LABS
			OR sFMMCMenu.sActiveMenu = efmmc_INTERIOR_WEED_FARM
			OR sFMMCMenu.sActiveMenu = efmmc_INTERIOR_COKE_FACTORY
				iBitSet = 0
				IF GET_INTERIOR_NAME_ENUM_FROM_MENU_OPTION(sFMMCMenu.sActiveMenu, GET_CREATOR_MENU_SELECTION(sFMMCmenu)) != INTERIOR_MAX_NUM
					INTERIOR_INSTANCE_INDEX iInteriorIndex		
					INTERIOR_DATA_STRUCT structInteriorData	
					structInteriorData = GET_INTERIOR_DATA(GET_INTERIOR_NAME_ENUM_FROM_MENU_OPTION(sFMMCMenu.sActiveMenu, GET_CREATOR_MENU_SELECTION(sFMMCmenu)))			
					iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
					IF iInteriorIndex != NULL
					AND IS_INTERIOR_READY(iInteriorIndex)
						SET_BIT(iBitSet, FMMC_HELP_BUTTON_WARP)
					ENDIF
				ELSE
					SET_BIT(iBitSet, FMMC_HELP_BUTTON_WARP)
				ENDIF
			ENDIF
			
		ENDIF
		
		IF sFMMCMenu.sActiveMenu = eFmmc_MISSION_VARIATION_PRIMARY_OBJECTIVE_TEXT_OVERRIDE
		AND g_CreatorsSelDetails.bColouringText = TRUE
			ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_ACCEPT, "BB_SELECT")
			ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_CANCEL, "FMMC_AI_TSKDN")
			ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_FRONTEND_DPAD_UD, "FMMC_VEH_CT")	
		ENDIF
		
		IF sFMMCmenu.sActiveMenu = eFmmc_TEAM_TEST
			SET_BIT(iBitSet, FMMC_HELP_BUTTON_SELECT)			
		ENDIF
		
		IF sFMMCmenu.sActiveMenu = efMMC_CUTSCENE_ADD_SHOT
		AND (GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ADD_SHOT_MENU_STREAM_POS
		OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ADD_SHOT_MENU_STREAM_RADIUS)
			SET_BIT(iBitSet, FMMC_HELP_BUTTON_DELETE)			
		ENDIF
		
		IF sFMMCmenu.sActiveMenu = efMMC_CUTSCENE_MOCAP_CLEAR
		OR sFMMCmenu.sActiveMenu = efMMC_END_CUTSCENE_CLEAR
			SET_BIT(iBitSet, FMMC_HELP_BUTTON_DELETE)			
		ENDIF
		
		IF sFMMCmenu.sActiveMenu = eFMMC_FALLBACK_SPAWN_POINTS
		AND (GET_CREATOR_MENU_SELECTION(sFMMCmenu) = FALLBACK_SPAWN_POINTS_PLACE OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = FALLBACK_SPAWN_POINTS_CYCLE)
		AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciFALLBACK_SPAWN_POINTS_ENABLE)
			SET_BIT(iBitSet, FMMC_HELP_BUTTON_DELETE)
		ENDIF
		
		IF sFMMCmenu.sActiveMenu = eFmmc_TOP_MENU
			CLEAR_BIT(iBitSet, FMMC_HELP_BUTTON_BACK)
		ENDIF
		
		IF sFMMCmenu.sActiveMenu = eFmmc_TEAM_RESPAWN_OPTIONS
			IF GET_CREATOR_MENU_SELECTION(sFMMCMenu) = TEAM_RESPAWN_OPT_RESPAWN_VEHICLE
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_EDIT)
				IF g_FMMC_STRUCT.mnVehicleModel[GET_CREATOR_MENU_SELECTION(sFMMCMenu)] != DUMMY_MODEL_FOR_SCRIPT
					SET_BIT(iBitSet, FMMC_HELP_BUTTON_DELETE)
				ENDIF
			ENDIF
		ENDIF
			
		IF sFMMCmenu.iPropLibrary = PROP_LIBRARY_CUSTOM_PROPS
		OR sFMMCmenu.iPropLibrary = PROP_LIBRARY_CUSTOM_DYNO_PROPS
			SET_BIT(iBitSet, FMMC_HELP_BUTTON_DELETE)
		ENDIF
		
		IF sFMMCMenu.sActiveMenu = eFmmc_MISSION_VARIATION_VEHICLE_MODEL_SWAP_MAIN
		AND GET_CREATOR_MENU_SELECTION(sFMMCMenu) >= OPTION_MISSION_VAR_VEHICLE_MODEL_SWAP_MAIN_1
			SET_BIT(iBitSet, FMMC_HELP_BUTTON_DELETE)
		ENDIF
		
		IF CAN_PROP_ROTATION_BE_RESET(sFMMCmenu, sCurrentVarsStruct)
		AND sFMMCMenu.sActiveMenu = eFmmc_PROP_BASE
			SET_BIT(iBitSet, FMMC_HELP_BUTTON_CLEAR_ROTATION)
		ENDIF
		
		IF sFMMCMenu.sMenuAction[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] = eFmmc_Edit_Vector
			IF IS_BIT_SET(sFMMCmenu.iFMMCMenuBitset, ciFMMCMenuBS_CurrentVectorItemIsEmpty)
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_PLACE)
			ELSE
				CLEAR_BIT(iBitSet, FMMC_HELP_BUTTON_PLACE)
			ENDIF
			
			SET_BIT(iBitSet, FMMC_HELP_BUTTON_DELETE)
		ENDIF
		
		IF sFMMCMenu.sActiveMenu = eFMMC_DUMMY_BLIPS
		AND GET_CREATOR_MENU_SELECTION(sFMMCMenu) = DUMMY_BLIP_NAME
			SET_BIT(iBitSet2, FMMC_HELP2_BUTTON_RESET)
		ENDIF
		
		CREATE_FMMC_INSTRUCTIONAL_BUTTONS(sFMMCmenu, iBitSet, DEFAULT, iBitSet2)
		
	ELSE
		IF g_CreatorsSelDetails.bColouringText = TRUE
			
			IF IS_BIT_SET(sFMMCmenu.iFMMCMenuBitset, ciFMMCmenuBS_IsOnScreenPopupValidThisFrame)
			OR IS_BIT_SET(sFMMCmenu.iFMMCMenuBitset, ciFMMCmenuBS_RequestOnScreenInstructionalButtonPrompt)
				ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT, "FMMC_OSPW_EDD")
			ELSE
				ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_ACCEPT, "BB_SELECT")
			ENDIF
			
			ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_CANCEL, "FMMC_AI_TSKDN")
			ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_FRONTEND_DPAD_UD, "FMMC_VEH_CT")	
		ELSE
			IF IS_BIT_SET(sFMMCmenu.iFMMCMenuBitset, ciFMMCmenuBS_IsOnScreenPopupValidThisFrame)
			OR IS_BIT_SET(sFMMCmenu.iFMMCMenuBitset, ciFMMCmenuBS_RequestOnScreenInstructionalButtonPrompt)
				ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT, "FMMC_OSPW_EDD")
			ELSE
				ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_ACCEPT, "BB_SELECT")
			ENDIF
			
			ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_CANCEL, "BB_BACK")
			
			IF NOT IS_RHM_IN_A_SUB_MENU()
				ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_X, "FMMC_RL_OP") //Rule Options
			ENDIF
			
			IF IS_THIS_CURRENT_RHM_MENU(eRHM_ObjText)
				ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_X, "FMMC_SS_W_1") //Clear Text Field
			ENDIF
			
			IF IS_THIS_CURRENT_RHM_MENU(eRHM_VehicleMenuOptions)
				IF GET_CURRENT_RHM_SELECTION() >= ciVEH_MENU_RIGHT_HAND_ROCKETS
				AND GET_CURRENT_RHM_SELECTION() <= ciVEH_MENU_RIGHT_HAND_SPIKES
					IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].iRuleBitsetSix[g_CreatorsSelDetails.iRow], ciBS_RULE6_VEHICLE_MENU_WEAPON_ADD_WEAPON1 + (GET_CURRENT_RHM_SELECTION() - ciVEH_MENU_RIGHT_HAND_ROCKETS))
						ADD_MENU_HELP_KEY_INPUT(INPUT_FRONTEND_X, "FMMC_VEHMENU_ST") //Set Vehicle Weapon Ammo
					ELSE
						ADD_MENU_HELP_KEY_INPUT(INPUT_FRONTEND_X, "FMMC_VEHMENU_AT") //Add Vehicle Weapon Ammo
					ENDIF
				ENDIF
			ENDIF
			
			IF g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].iNumberOfActiveRows > 0
			OR g_FMMC_STRUCT.iMaxNumberOfTeams > 1
				ADD_MENU_HELP_KEY_WITH_MULTIPLE_BUTTONS("FMMC_MC3", GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_RB), GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_LB))
			ENDIF
		ENDIF
		
	ENDIF
	
	
	RESET_SCRIPT_GFX_ALIGN()
	
ENDPROC

FUNC BOOL MANAGE_PED_ASS_OBJ_SECONDARY_GOTO(INT iPedNumber)
	IF sFMMCMenu.sActiveMenu = eFmmc_ASS_OBJECTIVE_SECONDARY_GOTO_MENU
		RESET_UP_HELP()
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
			sFMMCmenu.vPedSecondaryGotoPos = sCurrentVarsStruct.vCoronaPos
			
			FLOAT fWaterHeight,fGroundZ
						
			IF NOT GET_WATER_HEIGHT_NO_WAVES(sCurrentVarsStruct.vCoronaPos + <<0,0,1>>,fWaterHeight)
				fWaterHeight = -999
			ENDIF
			
			IF NOT GET_GROUND_Z_FOR_3D_COORD(sCurrentVarsStruct.vCoronaPos + <<0,0,1>>,fGroundZ)
				fGroundZ = -999
			ENDIF
			
			g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].fSecondaryVectorGroundHeight = GET_ENTITY_HEIGHT_ABOVE_GROUND(sPedStruct.piTempPed)
			
			IF (fWaterHeight > fGroundZ)
				g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].fSecondaryVectorGroundHeight -= (fWaterHeight-fGroundZ)
			ENDIF

			RESET_ENTITY_CREATION_MENU_ASAP(sCurrentVarsStruct)
			RETURN TRUE
		ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_X)
			sFMMCmenu.vPedSecondaryGotoPos = <<0,0,0>>
			g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].fSecondaryVectorGroundHeight = 0
			RESET_ENTITY_CREATION_MENU_ASAP(sCurrentVarsStruct)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

//************************PED HEALTH OPTIONS HACK 
//
//The following functions are used because ped health options were originally set up using an index that referred to a real value
//e.g. 0 = 100 , 3 = 300 etc. However as we've had to add more granular increments we now have to use these double lookups to convert
//a health value of 0 (100%) to it's index in the order of health values and back again to make the menu option work - PD

FUNC INT GET_INITIAL_HEALTH_INDEX_FROM_SAVE_VALUE(INT iSaveValue)

	SWITCH iSaveValue
		DEFAULT RETURN -1
		CASE ciPED_HEALTH_25 RETURN 0
		CASE ciPED_HEALTH_50 RETURN 1
		CASE ciPED_HEALTH_75 RETURN 2
		CASE ciPED_HEALTH_100 RETURN 3
		CASE ciPED_HEALTH_150 RETURN 4
		CASE ciPED_HEALTH_200 RETURN 5
		CASE ciPED_HEALTH_250 RETURN 6
		CASE ciPED_HEALTH_300 RETURN 7
		CASE ciPED_HEALTH_350 RETURN 8
		CASE ciPED_HEALTH_400 RETURN 9
		CASE ciPED_HEALTH_450 RETURN 10
		CASE ciPED_HEALTH_500 RETURN 11
		CASE ciPED_HEALTH_600 RETURN 12
		CASE ciPED_HEALTH_700 RETURN 13
		CASE ciPED_HEALTH_800 RETURN 14
		CASE ciPED_HEALTH_900 RETURN 15
		CASE ciPED_HEALTH_1000 RETURN 16
	ENDSWITCH
	
ENDFUNC

FUNC INT GET_SAVE_VALUE_FROM_INITIAL_HEALTH_INDEX(INT iIndex)

	INT i
	REPEAT ciPED_HEALTH_MAX_OPTIONS i
		IF iIndex = GET_INITIAL_HEALTH_INDEX_FROM_SAVE_VALUE(i)
			RETURN i
		ENDIF
	ENDREPEAT
	
	RETURN -1
	
ENDFUNC


PROC WARP_CAMERA_TO_START_LOCATION(FMMC_CAM_DATA &sCamDataPassed, FLOAT fHeight = 40.0)
	IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.vStartPos)
		IF DOES_CAM_EXIST(sCamDataPassed.cam)
			SET_CAM_COORD(sCamDataPassed.cam, g_FMMC_STRUCT.vStartPos+<<0.0,0.0, fHeight>>)
		ENDIF
	ELIF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedPed[0].vPos)
		IF DOES_CAM_EXIST(sCamDataPassed.cam)
			SET_CAM_COORD(sCamDataPassed.cam, g_FMMC_STRUCT_ENTITIES.sPlacedPed[0].vPos+<<0.0,0.0, fHeight>>)
		ENDIF
	ENDIF
ENDPROC

PROC RESET_NON_ARRAYED_VARS()
	
	//Do not add to the below list of vars, just init it to the default value below...
	INT 					iRow
	INT 					iCol
	INT 					iMaxNumberOfTeams = 1
	INT 					iSelectedTab
	INT 					iSelectedTeam
	INT 					iCurrentEntity
	BOOL					bEntitySubOptionActive
	BOOL 					bMenuSetUp
	INT						iSwitchingINT
	BOOL 					bSubMenuActive
	BOOL 					bSelectingEntites
	BOOL 					bKeyboardMenuActive
	BOOL 					bColouringText
	INT 					iStartSpace = -1
	INT						iTotalSpace = -1
	INT 					iNumberOfWords = 1
	BOOL 					bSelectionActive
	BOOL 					bRuleOptionsMenu
	INT						iWordStartSaved 	
	INT						iWordEndSaved 	
	INT 					iRulesOptionMenuSelection
	FLOAT 					fBoundsRadius	= 30.0
	FLOAT 					fBoundsRadiusOld	= 30.0
	BLIP_INDEX				biBlipBounds
	BOOL 					bEnteringSecondColour
	BOOL 					bMapSwapped
	//Do not add to the above list of vars, just init it to the default value below...
	
	g_CreatorsSelDetails.iRow									= iRow
	g_CreatorsSelDetails.iCol									=iCol									
	g_CreatorsSelDetails.iMaxNumberOfTeams 						= iMaxNumberOfTeams 
	g_CreatorsSelDetails.iSelectedTab							= iSelectedTab
	g_CreatorsSelDetails.iSelectedTeam							= iSelectedTeam
	g_CreatorsSelDetails.iCurrentEntity							= iCurrentEntity
	g_CreatorsSelDetails.bEntitySubOptionActive					= bEntitySubOptionActive
	g_CreatorsSelDetails.bMenuSetUp								= bMenuSetUp
	SET_CURRENT_GENERIC_CREATION_STAGE(iSwitchingINT)
	g_CreatorsSelDetails.bSubMenuActive							= bSubMenuActive
	g_CreatorsSelDetails.bSelectingEntites						= bSelectingEntites
	g_CreatorsSelDetails.bKeyboardMenuActive					= bKeyboardMenuActive
	g_CreatorsSelDetails.bColouringText							= bColouringText
	g_CreatorsSelDetails.iStartSpace 							= iStartSpace 
	g_CreatorsSelDetails.iTotalSpace 							= iTotalSpace 
	g_CreatorsSelDetails.iNumberOfWords 						= iNumberOfWords 
	g_CreatorsSelDetails.bSelectionActive						= bSelectionActive
	g_CreatorsSelDetails.iRestartCheckpoint						= -1
	g_CreatorsSelDetails.bRuleOptionsMenu						= bRuleOptionsMenu
	g_CreatorsSelDetails.iWordStartSaved 						= iWordStartSaved 	
	g_CreatorsSelDetails.iWordEndSaved 							= iWordEndSaved 	
	g_CreatorsSelDetails.iRulesOptionMenuSelection				= iRulesOptionMenuSelection
	g_CreatorsSelDetails.fBoundsRadius							= fBoundsRadius	
	g_CreatorsSelDetails.fBoundsRadiusOld						= fBoundsRadiusOld	
	g_CreatorsSelDetails.biBlipBounds							= biBlipBounds
	g_CreatorsSelDetails.bMapSwapped							= bMapSwapped
	g_CreatorsSelDetails.bEnteringSecondColour					= bEnteringSecondColour
	
	INT iLoop
	FOR iLoop = 0 TO FMMC_MAX_RHM_DEPTH-1
		g_CreatorsSelDetails.iRHMSelection[iLoop] = 1
		g_CreatorsSelDetails.eRHMMenus[iLoop] = eRHM_Null_Item
	ENDFOR
	g_CreatorsSelDetails.iCurrentRHMDepth = 0
ENDPROC
PROC RESET_ALL_RULES_RESET_BLIPS()
	BLIP_INDEX				biDropOffBlip
	INT iLoop
	FOR iLoop = 0 TO (ciMAX_RULES -1)
		g_CreatorsSelDetails.biDropOffBlip[iLoop]				= biDropOffBlip
	ENDFOR
ENDPROC
PROC RESET_TEAM_TAB_OPTIONS()
	INT iLoop
	INT iTeam
	SELECTED_ITEMS_STRUCT 	sSelection
	FOR iTeam = 0 TO (FMMC_MAX_TEAMS -1)
		g_CreatorsSelDetails.sTeamOptions[iTeam].iNumberOfActiveRows = 0
		FOR iLoop = 0 TO (ciMAX_RULES -1)
			g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[iLoop]	= sSelection
		ENDFOR
	ENDFOR
ENDPROC
PROC RESET_TEAM_SUB_OPTIONS()
	INT iLoop
	INT iTeam
	INT iSubOptions 
	FOR iTeam = 0 TO (FMMC_MAX_TEAMS -1)
		FOR iLoop = 0 TO (ciMAX_RULES -1)
			FOR iSubOptions = 0 TO (FMMC_MAX_SUB_OPTIONS -1)
				g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iLoop][iSubOptions] = 0	
			ENDFOR
		ENDFOR
	ENDFOR
ENDPROC

PROC RESET_ALL_RULES()
	RESET_NON_ARRAYED_VARS()
	RESET_ALL_RULES_RESET_BLIPS()
	RESET_TEAM_TAB_OPTIONS()
	RESET_TEAM_SUB_OPTIONS()
ENDPROC

PROC CHECK_ENTITY_AND_LOCATION_RULES()

	INT iRuleTypeThatWasRemoved
	//Peds
	IF IS_BIT_SET(sCurrentVarsStruct.iUpdateRule, ciPED_REMOVED_CHECK_RULES)
		iRuleTypeThatWasRemoved = ciRULE_TYPE_PED
		CLEAR_BIT(sCurrentVarsStruct.iUpdateRule, ciPED_REMOVED_CHECK_RULES)
	//Vehicles	
	ELIF IS_BIT_SET(sCurrentVarsStruct.iUpdateRule, ciVEH_REMOVED_CHECK_RULES)
		iRuleTypeThatWasRemoved = ciRULE_TYPE_VEHICLE
		CLEAR_BIT(sCurrentVarsStruct.iUpdateRule, ciVEH_REMOVED_CHECK_RULES)
	//Objects	
	ELIF IS_BIT_SET(sCurrentVarsStruct.iUpdateRule, ciOBJ_REMOVED_CHECK_RULES)
		iRuleTypeThatWasRemoved = ciRULE_TYPE_OBJECT
		CLEAR_BIT(sCurrentVarsStruct.iUpdateRule, ciOBJ_REMOVED_CHECK_RULES)
	//Rules. 	
	ELIF IS_BIT_SET(sCurrentVarsStruct.iUpdateRule, ciGOTO_REMOVED_CHECK_RULES)
		iRuleTypeThatWasRemoved = ciRULE_TYPE_GOTO
		CLEAR_BIT(sCurrentVarsStruct.iUpdateRule, ciGOTO_REMOVED_CHECK_RULES)
	ENDIF
	
	PRINTLN("iRuleTypeThatWasRemoved = ", iRuleTypeThatWasRemoved)
	
	INT iPriority
	INT iTeam
	FOR iTeam = 0 TO (FMMC_MAX_TEAMS - 1)
		PRINTLN("")
		PRINTLN("")
		PRINTLN("")
		PRINTLN("")
		PRINTLN("TEAM = ", iTeam)
		
		IF iRuleTypeThatWasRemoved = ciRULE_TYPE_VEHICLE
			FOR iPriority = 0 TO FMMC_MAX_RULES-1
				IF (g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDropOffType[iPriority] = ciFMMC_DROP_OFF_TYPE_VEHICLE OR g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDropOffType[iPriority] = ciFMMC_DROP_OFF_TYPE_LEADER_GARAGE)
				AND g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDropOff_Vehicle[iPriority] != -1
					IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDropOff_Vehicle[iPriority] = sCurrentVarsStruct.iEntityRemoved
						g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDropOff_Vehicle[iPriority] = -1
					ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDropOff_Vehicle[iPriority] > sCurrentVarsStruct.iEntityRemoved
						g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDropOff_Vehicle[iPriority]--
					ENDIF				
				ENDIF
			ENDFOR 
		ENDIF
		
		FOR iPriority = 0 TO (g_CreatorsSelDetails.sTeamOptions[iTeam].iNumberOfActiveRows - 1)
			IF GET_RULE_TYPE_FROM_RULE(g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[iPriority].iRule) = iRuleTypeThatWasRemoved
				#IF IS_DEBUG_BUILD
				TEXT_LABEL_63 tl63 = "CHECK_ENTITY_AND_LOCATION_RULES - BEFORE (entity removed: "
				tl63 += sCurrentVarsStruct.iEntityRemoved
				tl63 += ") Team: "
				tl63 += iTeam
				tl63 += " Priority: "
				tl63 += iPriority
				PRINT_LONG_BITSET(g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[iPriority].iEntityBitSet, tl63)
				#ENDIF
				INT iMask0 = 0, iMask1 = 0, iLongBitsetArray = 0, iLongBitsetIndex = 0
				IF sCurrentVarsStruct.iEntityRemoved >= 32
					iLongBitsetArray = sCurrentVarsStruct.iEntityRemoved / 32
					iLongBitsetIndex = sCurrentVarsStruct.iEntityRemoved % 32
					GET_ENTITY_LINK_BITSET_MASKS(iLongBitsetIndex, iMask0, iMask1)
				ELIF sCurrentVarsStruct.iEntityRemoved != -1
					iLongBitsetArray = 0
					iLongBitsetIndex = sCurrentVarsStruct.iEntityRemoved
					GET_ENTITY_LINK_BITSET_MASKS(sCurrentVarsStruct.iEntityRemoved, iMask0, iMask1)
				ENDIF
				UPDATE_ENTITY_LINK_LONG_BITSET_AFTER_DELETION(iLongBitsetIndex, FALSE, g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[iPriority].iEntityBitSet, iMask0, iMask1, iLongBitsetArray)
				#IF IS_DEBUG_BUILD
				tl63 = "CHECK_ENTITY_AND_LOCATION_RULES - AFTER Team: "
				tl63 += iTeam
				tl63 += " Priority: "
				tl63 += iPriority
				PRINT_LONG_BITSET(g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[iPriority].iEntityBitSet, tl63)
				#ENDIF
			ENDIF
		ENDFOR
		
		SORT_ALL_OBJECTIVES(g_CreatorsSelDetails, iTeam)
	ENDFOR
ENDPROC


PROC SET_DEFAULT_VALUES_FROM_CLOUD(INT iTeam, INT iRow)

	g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iRow][FMMC_SUB_OPTIONS_TIME]											= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjectiveTimeLimitRule[iRow]
	g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iRow][FMMC_SUB_OPTIONS_SECONDARY_ANIM]								= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iSecondaryAnim[iRow]
	g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iRow][FMMC_SUB_OPTIONS_SCORE]											= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjectiveScore[iRow]
	g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iRow][FMMC_SUB_OPTIONS_TARGET_SCORE]									= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTargetScore[iRow]
	g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iRow][FMMC_SUB_OPTIONS_NUM_CARRYABLE]									= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMaxDeliveryEntitiesCanCarry[iRow] - 1
	g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iRow][FMMC_SUB_OPTIONS_OUTFIT]										= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iOutfit[iRow]	
	g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iRow][FMMC_SUB_OPTIONS_OUTFIT_VARIATIONS]								= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iOutfitVariations[iRow]	
	g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iRow][FMMC_SUB_OPTIONS_PROGRESS_MINIGAME]								= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iProgressMinigame[iRow]
	g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iRow][FMMC_SUB_OPTIONS_HIT_SOUND]										= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iHitSound[iRow]
	g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iRow][FMMC_SUB_OPTIONS_REQUIRED_DELIVERIES]							= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRequiredDeliveries[iRow] -1
	g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iRow][FMMC_SUB_OPTIONS_OVERWRITE_TYPE]								= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].OverwriteEntityType[iRow] 
	g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iRow][FMMC_SUB_OPTIONS_OVERWRITE_ID]									= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].OverwriteEntityID[iRow] 
	g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iRow][FMMC_SUB_OPTIONS_NEAREST_TARGET_THRESHOLD]						= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iNearestTargetThreshold[iRow] 
	g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iRow][FMMC_SUB_OPTIONS_TRAFFIC]										= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleTraffic[iRow]
	g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iRow][FMMC_SUB_OPTIONS_PED_DENSITY]									= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRulePedDensity[iRow]
	g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iRow][FMMC_SUB_OPTIONS_TEAM_LIVES_OVERRIDE]   						= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iPlayerLivesPerRule[iRow]
	g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iRow][FMMC_SUB_OPTIONS_SUDDEN_DEATH_TIMER]    						= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjectiveSuddenDeathTimeLimit[iRow]
	g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iRow][FMMC_SUB_OPTIONS_VEHICLE_DAMAGE_THRESHOLD]   					= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMinVehHealth[iRow]
	g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iRow][FMMC_SUB_OPTIONS_VEHICLE_DAMAGE_SPOOK_ID]   					= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMinVehHealthSpookID[iRow]
	g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iRow][FMMC_SUB_OPTIONS_VEHICLE_DAMAGE_SPOOK_DIST]   					= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMinVehHealthSpookDist[iRow]
	g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iRow][FMMC_SUB_OPTIONS_DRAW_PLAYER_POSITION]   						= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamWithPlayerPositionToDraw[iRow]
	g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iRow][FMMC_SUB_OPTIONS_MAX_HEALTH]   									= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMaxHealth[iRow]
	g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iRow][FMMC_SUB_OPTIONS_CARGOBOB_MANUAL_RESPAWN]   					= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iCBManRespawnExclusionRadius[iRow]
	g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iRow][FMMC_SUB_OPTIONS_RESPAWN_IN_MISSION_VEH] 						= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRespawnInMissionVehicle[iRow]
	g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iRow][FMMC_SUB_OPTIONS_SWAP_TEAM_IN_LOCATE]							= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iLocateSwapTeam[iRow]
	g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iRow][FMMC_SUB_OPTIONS_AMBIENT_POLICE_ACCURACY]						= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iAmbientPoliceAccuracy[iRow]
	g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iRow][FMMC_SUB_OPTIONS_PLAY_DOWNLOAD_SOUND_AFTER_DIALOGUE]			= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iPlayDownloadSoundAfterDialogue[iRow]
	g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iRow][FMMC_SUB_OPTIONS_MINIGAME_TAKE_PROGRESSION]						= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTakeProgressionAmount[iRow]

ENDPROC

PROC DELETE_BLIPS_AND_CHECKPOINTS()
	INT i, iTeam
	
	FOR i = 0 TO (COUNT_OF(sWepStruct.biWeaponBlip)-1)
		IF DOES_BLIP_EXIST(sWepStruct.biWeaponBlip[i])
			REMOVE_BLIP(sWepStruct.biWeaponBlip[i])
		ENDIF
	ENDFOR
	IF DOES_BLIP_EXIST(sStartEndBlips.biStart)
       REMOVE_BLIP(sStartEndBlips.biStart)
	ENDIF
	
	IF sStartEndBlips.ciStartType != NULL
		DELETE_CHECKPOINT(sStartEndBlips.ciStartType)
		sStartEndBlips.ciStartType = NULL
	ENDIF
	
	INT iLoop
	FOR iLoop = 0 TO (ciMAX_RULES -1)
		If DOES_BLIP_EXIST(g_CreatorsSelDetails.biDropOffBlip[iLoop])
			REMOVE_BLIP(g_CreatorsSelDetails.biDropOffBlip[iLoop])
			PRINTLN("DELETE A DROP OFF BLIP!")
		ENDIF
	ENDFOR
	
	IF DOES_BLIP_EXIST(bPhotoBlip)
		REMOVE_BLIP(bPhotoBlip)
	ENDIF
	IF DOES_BLIP_EXIST(bCameraPanBlip)
		REMOVE_BLIP(bCameraPanBlip)
	ENDIF
	IF DOES_BLIP_EXIST(bCameraOutroBlip)
		REMOVE_BLIP(bCameraOutroBlip)
	ENDIF
	
	FOR i = 0 TO (FMMC_MAX_PEDS-1)
		IF DOES_BLIP_EXIST(sPedStruct.biPedBlip[i] )
		     REMOVE_BLIP(sPedStruct.biPedBlip[i] )
		ENDIF
	ENDFOR
	FOR i = 0 TO (FMMC_MAX_NUM_OBJECTS-1)
		IF DOES_BLIP_EXIST(sObjStruct.biObject[i] )
		     REMOVE_BLIP(sObjStruct.biObject[i] )
		ENDIF
	ENDFOR
	FOR i = 0 TO (FMMC_MAX_VEHICLES-1)
		IF DOES_BLIP_EXIST(svehStruct.biVehicleBlip[i] )
		     REMOVE_BLIP(svehStruct.biVehicleBlip[i] )
		ENDIF
	ENDFOR
	FOR i = 0 TO (GET_FMMC_MAX_NUM_PROPS()-1)
		IF DOES_BLIP_EXIST(spropStruct.biObject[i] )
		     REMOVE_BLIP(spropStruct.biObject[i] )
		ENDIF
	ENDFOR
	FOR i = 0 TO (FMMC_MAX_GO_TO_LOCATIONS - 1)
		IF DOES_BLIP_EXIST(sLocStruct.biGoToBlips[i])
			REMOVE_BLIP(sLocStruct.biGoToBlips[i])
		ENDIF
	ENDFOR
	FOR i = 0 TO (FMMC_MAX_GO_TO_LOCATIONS - 1)
		IF DOES_BLIP_EXIST(sLocStruct.biGoToBlipCaptureRadius[i])
			REMOVE_BLIP(sLocStruct.biGoToBlipCaptureRadius[i])
		ENDIF
	ENDFOR
	
	FOR i = 0 TO FMMC_MAX_NUM_COVER - 1
		IF DOES_BLIP_EXIST(sCoverStruct.biCover[i])
			REMOVE_BLIP(sCoverStruct.biCover[i])
		ENDIF
	ENDFOR
	
	FOR iTeam = 0 TO FMMC_MAX_TEAMS-1
		FOR i = 0 TO FMMC_MAX_TEAMSPAWNPOINTS-1
			IF DOES_BLIP_EXIST(sTeamSpawnStruct[iTeam].biPedBlip[i])
				REMOVE_BLIP(sTeamSpawnStruct[iTeam].biPedBlip[i])
			ENDIF
		ENDFOR
	ENDFOR
	
	//Vehicles
	IF DOES_ENTITY_EXIST(sVehStruct.viCoronaVeh)
		DELETE_VEHICLE(sVehStruct.viCoronaVeh)
	ENDIF

ENDPROC

PROC SET_RESET_MISSION_STATE(RESET_STATE eNewState)
	PRINTLN("SET_RESET_MISSION_STATE - Setting iResetState from ", iResetState, " to ", eNewState)
	iResetState = eNewState
ENDPROC

FUNC BOOL RESET_THE_MISSION_UP_SAFELY()

	INT i, i2

	SWITCH iResetState
		CASE RESET_STATE_FADE
			PRINTLN("RESET_STATE_FADE")
			IF IS_FAKE_MULTIPLAYER_MODE_SET() = TRUE 
			    SET_FAKE_MULTIPLAYER_MODE(FALSE)
			ENDIF
			IF IS_FAKE_MULTIPLAYER_MODE_SET() = FALSE 
				SET_RESET_MISSION_STATE(RESET_STATE_CLEAR)
			ENDIF
			RESET_INVISIBLE_OBJECT_POSITIONS(sInvisibleObjects)
			
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())				
				SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(PLAYER_PED_ID(), KNOCKOFFVEHICLE_DEFAULT)
				SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_WillFlyThroughWindscreen, FALSE)	
			ENDIF
			IF IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
				SWAP_CAMERAS(sCamData, sFMMCdata, sFMMCMenu, sCurrentVarsStruct, FALSE)
			ENDIF
		BREAK
		
		CASE RESET_STATE_CLEAR
			PRINTLN("RESET_STATE_CLEAR")
			IF IS_FAKE_MULTIPLAYER_MODE_SET() = FALSE 
				IF NOT NETWORK_IS_GAME_IN_PROGRESS()
					FOR i = 0 TO (FMMC_MAX_PEDS - 1)
						IF DOES_BLIP_EXIST(sPedStruct.biPedBlip[i])
							REMOVE_BLIP(sPedStruct.biPedBlip[i])
						ENDIF
						IF DOES_ENTITY_EXIST(sPedStruct.piPed[i])
							DELETE_PED(sPedStruct.piPed[i])
						ENDIF
					ENDFOR
					FOR i = 0 TO (FMMC_MAX_VEHICLES - 1)
						IF DOES_BLIP_EXIST(sVehStruct.biVehicleBlip[i])
							REMOVE_BLIP(sVehStruct.biVehicleBlip[i])
						ENDIF
						IF DOES_ENTITY_EXIST(sVehStruct.veVehcile[i])
							DELETE_VEHICLE(sVehStruct.veVehcile[i])
						ENDIF
					ENDFOR				
					FOR i = 0 TO (GET_FMMC_MAX_NUM_PROPS() - 1)
						IF DOES_BLIP_EXIST(sPropStruct.biObject[i])
							REMOVE_BLIP(sPropStruct.biObject[i])
						ENDIF
						if DOES_ENTITY_EXIST(sPropStruct.oiObject[i])
							DELETE_OBJECT(sPropStruct.oiObject[i])
						ENDIF
					ENDFOR						
					FOR i = 0 TO (FMMC_MAX_NUM_DYNOPROPS - 1)
						IF DOES_BLIP_EXIST(sDynoPropStruct.biObject[i])
							REMOVE_BLIP(sDynoPropStruct.biObject[i])
						ENDIF
						if DOES_ENTITY_EXIST(sDynoPropStruct.oiObject[i])
							DELETE_OBJECT(sDynoPropStruct.oiObject[i])
						ENDIF
					ENDFOR	
					FOR i = 0 TO (FMMC_MAX_WEAPONS - 1)
						IF DOES_PICKUP_EXIST(sWepStruct.Pickups[i])
							REMOVE_PICKUP(sWepStruct.Pickups[i])
						ENDIF
						IF DOES_BLIP_EXIST(sWepStruct.biWeaponBlip[i])
							REMOVE_BLIP(sWepStruct.biWeaponBlip[i])
						ENDIF
						if DOES_ENTITY_EXIST(sWepStruct.oiWeapon[i])
							DELETE_OBJECT(sWepStruct.oiWeapon[i])
						ENDIF
					ENDFOR
					FOR i = 0 TO (FMMC_MAX_NUM_OBJECTS - 1)
						IF DOES_ENTITY_EXIST(sObjStruct.oiObject[i])
							DELETE_OBJECT(sObjStruct.oiObject[i])
						ENDIF
						IF DOES_BLIP_EXIST(sObjStruct.biObject[i])
							REMOVE_BLIP(sObjStruct.biObject[i])
						ENDIF
					ENDFOR
					FOR i = 0 TO (FMMC_MAX_GANG_HIDE_LOCATIONS - 1)
						IF DOES_BLIP_EXIST(sLocStruct.biGoToBlips[i])
							REMOVE_BLIP(sLocStruct.biGoToBlips[i])
						ENDIF
					ENDFOR
					IF DOES_BLIP_EXIST(sStartEndBlips.biStart)
						REMOVE_BLIP(sStartEndBlips.biStart)
					ENDIF							
					IF sStartEndBlips.ciStartType != NULL
						DELETE_CHECKPOINT(sStartEndBlips.ciStartType)
						sStartEndBlips.ciStartType = NULL
					ENDIF
					
					FOR i = 0 TO (FMMC_MAX_NUM_DEV_ASSIST - 1)
						IF DOES_ENTITY_EXIST(sDevAssistCreationStruct.oi[i])
							DELETE_OBJECT(sDevAssistCreationStruct.oi[i])
						ENDIF
						IF DOES_BLIP_EXIST(sDevAssistCreationStruct.biBlip[i])
							REMOVE_BLIP(sDevAssistCreationStruct.biBlip[i])
						ENDIF
					ENDFOR
					
					DELETE_BLIPS_AND_CHECKPOINTS()
					
					RESET_PLACED_PROP_ARRAY()
					
					SET_RESET_MISSION_STATE(RESET_STATE_INTERIORS)
				ENDIF
			ENDIF
		BREAK
		
		CASE RESET_STATE_INTERIORS
			PRINTLN("RESET_STATE_INTERIORS")			
			IF FORCE_CORONER_INTERIOR_IPL_ON()
				LOAD_ALL_ACTIVE_INTERIORS()
				sFMMCData.bRefreshInteriors = TRUE
				SET_RESET_MISSION_STATE(RESET_STATE_WEAPONS)
			ENDIF
		BREAK
		
		CASE RESET_STATE_WEAPONS
			PRINTLN("RESET_STATE_WEAPONS")
			IF CREATE_ALL_CURRENT_PICKUPS(sWepStruct, TRUE, sCurrentVarsStruct)
				SET_RESET_MISSION_STATE(RESET_STATE_PROPS)
			ENDIF
		BREAK
		
		CASE RESET_STATE_PROPS
			PRINTLN("RESET_STATE_PROPS")
			IF CREATE_ALL_CREATOR_PROPS(sPropStruct, iPropModelTimers)
				SET_RESET_MISSION_STATE(RESET_STATE_DYNOPROPS)
			ENDIF
		BREAK
		
		CASE RESET_STATE_DYNOPROPS
			PRINTLN("RESET_STATE_DYNOPROPS")
			IF CREATE_ALL_CREATOR_DYNOPROPS(sDynoPropStruct)
				SET_RESET_MISSION_STATE(RESET_STATE_VEHICLES)
			ENDIF
		BREAK
		
		CASE RESET_STATE_VEHICLES
			PRINTLN("RESET_STATE_VEHICLES")
			IF CREATE_ALL_CREATOR_VEHICLES(sVehStruct, iVehicleModelLoadTimers, FALSE)
				SET_RESET_MISSION_STATE(RESET_STATE_CRATES)
			ENDIF
		BREAK
		
		CASE RESET_STATE_CRATES
			PRINTLN("RESET_STATE_CRATES")
			IF CREATE_ALL_CREATOR_VEHICLE_CRATES(sVehStruct)
				SET_RESET_MISSION_STATE(RESET_STATE_OBJECTS)
			ENDIF			
		BREAK	
		
		CASE RESET_STATE_OBJECTS
			PRINTLN("RESET_STATE_OBJECTS")
			IF CREATE_ALL_CREATOR_OBJECTS(sObjStruct, sCurrentVarsStruct)
				SET_RESET_MISSION_STATE(RESET_STATE_PEDS)
			ENDIF			
		BREAK
		
		CASE RESET_STATE_PEDS
			PRINTLN("RESET_STATE_PEDS")
			IF CREATE_ALL_CREATOR_PEDS(sPedStruct, sVehStruct, sFMMCmenu)
				SET_RESET_MISSION_STATE(RESET_STATE_ATTACH)
			ENDIF			
		BREAK
		
		CASE RESET_STATE_ATTACH
			PRINTLN("RESET_STATE_ATTACH")
			FOR i = 0 TO FMMC_MAX_VEHICLES-1
				IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAttachParent >= 0
					IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAttachParentType = CREATION_TYPE_VEHICLES
						IF GET_ENTITY_MODEL(sVehStruct.veVehcile[i]) = CARGOBOB2
							FMMC_ATTACH_CARGOBOB_TO_VEHICLE(sVehStruct.veVehcile[i],sVehStruct.veVehcile[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAttachParent])
						ELSE
							FMMC_ATTACH_VEHICLE_TO_VEHICLE(sVehStruct.veVehcile[i], sVehStruct.veVehcile[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAttachParent])
						ENDIF
					ELIF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAttachParentType = CREATION_TYPE_OBJECTS
						FMMC_ATTACH_VEHICLE_TO_OBJECT(sVehStruct.veVehcile[i], sObjStruct.oiObject[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAttachParent])
					ENDIF
				ENDIF
			ENDFOR
			FOR i = 0 TO (FMMC_MAX_NUM_OBJECTS - 1)
				IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAttachParent >= 0
					IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAttachParentType = CREATION_TYPE_VEHICLES
						ATTACH_OBJECT_TO_FLATBED(sObjStruct.oiObject[i], sVehStruct.veVehcile[g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iAttachParent], g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].vAttachOffset, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].vAttachOffsetRotation)
					ENDIF
				ENDIF
			ENDFOR
			SET_RESET_MISSION_STATE(RESET_STATE_TEAM_SPAWNS)
		BREAK
		
		CASE RESET_STATE_TEAM_SPAWNS
			PRINTLN("RESET_STATE_TEAM_SPAWNS")		
			FOR i = 0 TO (FMMC_MAX_TEAMS - 1)		
				FOR i2 = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[i] - 1)
					IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[i][i2].vPos)		
						IF NOT DOES_BLIP_EXIST(sTeamSpawnStruct[i].biPedBlip[i2])
							CREATE_FMMC_BLIP(sTeamSpawnStruct[i].biPedBlip[i2], g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[i][i2].vPos, HUD_COLOUR_WHITE, getNameForTeamSpawnBlip(i), 1)
							SET_BLIP_COLOUR(sTeamSpawnStruct[i].biPedBlip[i2], getColourForSpawnBlip(i))
							CREATE_TEAM_SPAWN_POINT_DECAL(sTeamSpawnStruct[i], i2, FALSE, sCurrentVarsStruct.bCanCreateADecalThisFrame, i)
							
							PRINTLN("CREATED A TEAM SPAWN BLIP AND DECAL ", i)	
							
							RETURN FALSE
						ENDIF
					ENDIF
				ENDFOR
			ENDFOR
			SET_TEAM_SPAWN_VEHICLE_FROM_SAVED_DATA(sVehStruct)
			
			SET_RESET_MISSION_STATE(RESET_STATE_OTHER)
			
		BREAK
		
		CASE RESET_STATE_OTHER
			PRINTLN("RESET_STATE_OTHER")	
			REQUEST_STREAMED_TEXTURE_DICT("MPMissMarkers256")
			IF HAS_STREAMED_TEXTURE_DICT_LOADED("MPMissMarkers256")
				HAS_PATCHED_START_LOGO_STATE_SWAPPED(TRUE, 3)
				
				g_FMMC_STRUCT.iContactChar = CONVERT_CHARACTER_INT_TO_MENU_OPTION(g_FMMC_STRUCT.iContactCharEnum)
				
				IF g_FMMC_STRUCT.iContactChar != 0
				AND NOT IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciMISSION_OPTION_BS_HEIST_HIGH_END)
				AND NOT IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciMISSION_OPTION_BS_CONTACT_MISSION)
				AND NOT IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciMISSION_OPTION_BS_HEIST_PLANNING)
					PRINTLN("SETTING CONTACT CHAR TO 0")
					g_FMMC_STRUCT.iContactChar = 0
					g_FMMC_STRUCT.iContactCharEnum = Get_Hash_Of_enumCharacterList_Contact(NO_CHARACTER)
				ENDIF
				
				FOR i = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfCoverPoints - 1)
					PRINTLN("Cover i = ", i)
					IF IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedCoverPoints[i].vPos)
						DELETE_THIS_COVER(i, sCoverStruct, sFMMCmenu.iEntityCreation)
						i -= 1
						PRINTLN("Cover deleted i = ", i)
					ELSE
						CREATE_FMMC_BLIP(sCoverStruct.biCover[i], g_FMMC_STRUCT_ENTITIES.sPlacedCoverPoints[i].vPos, HUD_COLOUR_BLUE, "FMMC_CP_M", 1)
						PRINTLN("Cover blipped i = ", i)
					ENDIF
				ENDFOR
				
				BLIP_ALL_FMMC_LOCATIONS_SP(sLocStruct.biGoToBlips, sLocStruct.biGoToBlipCaptureRadius)
				if NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.vStartPos)					
					CREATE_TRIGGER_BLIP_AND_CORONA_WITH_DECAL(sStartEndBlips.biStart, sHCS.hcStartCoronaColour, sStartEndBlips.ciStartType)					
				ELSE
					SET_DM_STARTING_MENU_ITEMS_ACTIVE(sFMMCmenu)
				ENDIF
					
				SET_RESET_MISSION_STATE(RESET_STATE_FINISH)
			
			ENDIF
		BREAK
		CASE RESET_STATE_FINISH
			PRINTLN("RESET_STATE_FINISH")			
			RETURN TRUE
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

PROC SET_CORONA_ENTITIES_TO_DUMMY_MODELS()
	INT i
	FOR i = g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles TO FMMC_MAX_VEHICLES - 1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn != DUMMY_MODEL_FOR_SCRIPT
			g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn = DUMMY_MODEL_FOR_SCRIPT
			PRINTLN("SET TO DUMMY VEHICLE MODEL ", i)
		ENDIF
	ENDFOR
	FOR i = g_FMMC_STRUCT_ENTITIES.iNumberOfPeds TO FMMC_MAX_PEDS - 1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].mn != DUMMY_MODEL_FOR_SCRIPT
			g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].mn = DUMMY_MODEL_FOR_SCRIPT
			PRINTLN("SET TO DUMMY PED MODEL ", i)
		ENDIF
	ENDFOR
	FOR i = g_FMMC_STRUCT_ENTITIES.iNumberOfObjects TO FMMC_MAX_NUM_OBJECTS - 1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].mn != DUMMY_MODEL_FOR_SCRIPT
			g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].mn = DUMMY_MODEL_FOR_SCRIPT
			PRINTLN("SET TO DUMMY OBJECT MODEL ", i)
		ENDIF
	ENDFOR
	FOR i = g_FMMC_STRUCT_ENTITIES.iNumberOfInteractables TO FMMC_MAX_NUM_INTERACTABLES - 1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].mnInteractable_Model != DUMMY_MODEL_FOR_SCRIPT
			g_FMMC_STRUCT_ENTITIES.sPlacedInteractable[i].mnInteractable_Model = DUMMY_MODEL_FOR_SCRIPT
			PRINTLN("SET TO DUMMY INTERACTABLE MODEL ", i)
		ENDIF
	ENDFOR
	FOR i = g_FMMC_STRUCT_ENTITIES.iNumberOfProps TO GET_FMMC_MAX_NUM_PROPS() - 1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn != DUMMY_MODEL_FOR_SCRIPT
			g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn = DUMMY_MODEL_FOR_SCRIPT
			PRINTLN("SET TO DUMMY PROP MODEL ", i)
		ENDIF
	ENDFOR
	FOR i = g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps TO FMMC_MAX_NUM_DYNOPROPS - 1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].mn != DUMMY_MODEL_FOR_SCRIPT
			g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].mn = DUMMY_MODEL_FOR_SCRIPT
			PRINTLN("SET TO DUMMY DYNOPROP MODEL ", i)
		ENDIF
	ENDFOR
ENDPROC

PROC RELEASE_CACHED_DOOR_OBJECTS(BOOL bDelete = FALSE)
	INT i
	FOR i = 0 TO FMMC_MAX_NUM_DOORS - 1
		IF DOES_ENTITY_EXIST(sCurrentVarsStruct.oiCachedDoors[i])
			IF bDelete
				DELETE_OBJECT(sCurrentVarsStruct.oiCachedDoors[i])
				PRINTLN("[Doors][Doors_Creator][Door ", i, "] RELEASE_CACHED_DOOR_OBJECTS | Deleting cached door ", i)
			ELSE
				SET_OBJECT_AS_NO_LONGER_NEEDED(sCurrentVarsStruct.oiCachedDoors[i])
				PRINTLN("[Doors][Doors_Creator][Door ", i, "] RELEASE_CACHED_DOOR_OBJECTS | Setting cached door as no longer needed ", i)
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

PROC SET_UP_PLAYER_RULE(PLAYER_RULE_LOCAL_VARS &sPlayerRulesPassed)
	SWITCH sPlayerRulesPassed.iSwitch
		CASE 0
			sPlayerRulesPassed.iSwitch++
			PRINTLN("SET_UP_PLAYER_RULE()")
			PRINTLN("sPlayerRulesPassed.iSwitch = ", sPlayerRulesPassed.iSwitch)
		BREAK
		
		CASE 1
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
			
				PRINTLN("SET_UP_PLAYER_RULE()")
				
				INT iTeam
				FOR iTeam = 0 TO (FMMC_MAX_TEAMS - 1)
					IF g_CreatorsSelDetails.sTeamOptions[iTeam].iNumberOfActiveRows < ciMAX_RULES
					AND g_FMMC_STRUCT.iNumberOfPlayerRules[iTeam] < FMMC_MAX_RULES
						g_FMMC_STRUCT.sPlayerRuleData[g_FMMC_STRUCT.iNumberOfPlayerRules[iTeam]].iRule[iTeam] = GET_PLAYER_RULE_TYPE_FROM_SELECTION(sFMMCmenu.iPlayerRule)
						g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[g_CreatorsSelDetails.sTeamOptions[iTeam].iNumberOfActiveRows].iRule = GET_PLAYER_RULE_FROM_SELECTION(sFMMCmenu.iPlayerRule)
						IF iTeam = GET_TEAM_TO_KILL_FROM_RULE(g_FMMC_STRUCT.sPlayerRuleData[g_FMMC_STRUCT.iNumberOfPlayerRules[iTeam]].iRule[iTeam])
							g_FMMC_STRUCT.sPlayerRuleData[g_FMMC_STRUCT.iNumberOfPlayerRules[iTeam]].iRule[iTeam] = FMMC_OBJECTIVE_LOGIC_KILL_PLAYERS
							g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[g_CreatorsSelDetails.sTeamOptions[iTeam].iNumberOfActiveRows].iRule = ciSELECTION_PLAYER_RULE_KILL_ANY
						ENDIF
						g_FMMC_STRUCT.sPlayerRuleData[g_FMMC_STRUCT.iNumberOfPlayerRules[iTeam]].iPriority[iTeam]									= g_CreatorsSelDetails.sTeamOptions[iTeam].iNumberOfActiveRows
						
						IF g_FMMC_STRUCT.sPlayerRuleData[g_FMMC_STRUCT.iNumberOfPlayerRules[iTeam]].iRule[iTeam] = FMMC_OBJECTIVE_LOGIC_GET_MASKS
						OR g_FMMC_STRUCT.sPlayerRuleData[g_FMMC_STRUCT.iNumberOfPlayerRules[iTeam]].iRule[iTeam] = FMMC_OBJECTIVE_LOGIC_ARREST_ALL
						OR g_FMMC_STRUCT.sPlayerRuleData[g_FMMC_STRUCT.iNumberOfPlayerRules[iTeam]].iRule[iTeam] = FMMC_OBJECTIVE_LOGIC_ARREST_TEAM0
						OR g_FMMC_STRUCT.sPlayerRuleData[g_FMMC_STRUCT.iNumberOfPlayerRules[iTeam]].iRule[iTeam] = FMMC_OBJECTIVE_LOGIC_ARREST_TEAM1
						OR g_FMMC_STRUCT.sPlayerRuleData[g_FMMC_STRUCT.iNumberOfPlayerRules[iTeam]].iRule[iTeam] = FMMC_OBJECTIVE_LOGIC_ARREST_TEAM2
						OR g_FMMC_STRUCT.sPlayerRuleData[g_FMMC_STRUCT.iNumberOfPlayerRules[iTeam]].iRule[iTeam] = FMMC_OBJECTIVE_LOGIC_ARREST_TEAM3
						OR g_FMMC_STRUCT.sPlayerRuleData[g_FMMC_STRUCT.iNumberOfPlayerRules[iTeam]].iRule[iTeam] = FMMC_OBJECTIVE_LOGIC_SCRIPTED_CUTSCENE
						OR g_FMMC_STRUCT.sPlayerRuleData[g_FMMC_STRUCT.iNumberOfPlayerRules[iTeam]].iRule[iTeam] = FMMC_OBJECTIVE_LOGIC_LOOT_THRESHOLD
						OR g_FMMC_STRUCT.sPlayerRuleData[g_FMMC_STRUCT.iNumberOfPlayerRules[iTeam]].iRule[iTeam] = FMMC_OBJECTIVE_LOGIC_POINTS_THRESHOLD
						OR g_FMMC_STRUCT.sPlayerRuleData[g_FMMC_STRUCT.iNumberOfPlayerRules[iTeam]].iRule[iTeam] = FMMC_OBJECTIVE_LOGIC_HOLDING_RULE
							g_FMMC_STRUCT.sPlayerRuleData[g_FMMC_STRUCT.iNumberOfPlayerRules[iTeam]].iPlayerRuleLimit[iTeam]							= 0
						ELSE
							g_FMMC_STRUCT.sPlayerRuleData[g_FMMC_STRUCT.iNumberOfPlayerRules[iTeam]].iPlayerRuleLimit[iTeam]							= 5
						ENDIF
						
						g_FMMC_STRUCT.sPlayerRuleData[g_FMMC_STRUCT.iNumberOfPlayerRules[iTeam]].iEndPassMocap = g_FMMC_STRUCT.iPlayerEndPassMocap
						g_FMMC_STRUCT.sPlayerRuleData[g_FMMC_STRUCT.iNumberOfPlayerRules[iTeam]].iEndFailMocap = g_FMMC_STRUCT.iPlayerEndFailMocap
						
						g_FMMC_STRUCT.sPlayerRuleData[g_FMMC_STRUCT.iNumberOfPlayerRules[iTeam]].iBitset[iTeam] = sFMMCmenu.iPlayerRuleBitset
						
						SET_DEFAULT_VALUES_FROM_CLOUD(iTeam, g_CreatorsSelDetails.sTeamOptions[iTeam].iNumberOfActiveRows)
						
						PRINTLN("g_FMMC_STRUCT.sPlayerRuleData[", g_FMMC_STRUCT.iNumberOfPlayerRules[iTeam], "].iRule[", iTeam, "]            = ", g_FMMC_STRUCT.sPlayerRuleData[g_FMMC_STRUCT.iNumberOfPlayerRules[iTeam]].iRule[iTeam]			)
						PRINTLN("g_FMMC_STRUCT.sPlayerRuleData[", g_FMMC_STRUCT.iNumberOfPlayerRules[iTeam], "].iPriority[", iTeam, "]        = ", g_FMMC_STRUCT.sPlayerRuleData[g_FMMC_STRUCT.iNumberOfPlayerRules[iTeam]].iPriority[iTeam]		)
						PRINTLN("g_FMMC_STRUCT.sPlayerRuleData[", g_FMMC_STRUCT.iNumberOfPlayerRules[iTeam], "].iPlayerRuleLimit[", iTeam, "] = ",g_FMMC_STRUCT.sPlayerRuleData[g_FMMC_STRUCT.iNumberOfPlayerRules[iTeam]].iPlayerRuleLimit[iTeam] )
						PRINTLN("g_FMMC_STRUCT.sPlayerRuleData[", g_FMMC_STRUCT.iNumberOfPlayerRules[iTeam], "].iPriority[", iTeam, "]) = ", g_FMMC_STRUCT.sPlayerRuleData[g_FMMC_STRUCT.iNumberOfPlayerRules[iTeam]].iPriority[iTeam])
						
						g_CreatorsSelDetails.iPlayerRuleMapping[iTeam][g_CreatorsSelDetails.sTeamOptions[iTeam].iNumberOfActiveRows] = g_FMMC_STRUCT.iNumberOfPlayerRules[iTeam]
						
						g_FMMC_STRUCT.iNumberOfPlayerRules[iTeam]++
						
						FMMC_SET_LONG_BIT(g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[g_CreatorsSelDetails.sTeamOptions[iTeam].iNumberOfActiveRows].iEntityBitSet, g_FMMC_STRUCT.iNumberOfPlayerRules[iTeam] -1)
						
						g_CreatorsSelDetails.sTeamOptions[iTeam].iNumberOfActiveRows++
						
						PRINTLN("g_FMMC_STRUCT.iNumberOfPlayerRules[",iTeam, "] = ", g_FMMC_STRUCT.iNumberOfPlayerRules[iTeam])
						IF g_CreatorsSelDetails.sTeamOptions[iTeam].iNumberOfActiveRows > ciMAX_RULES
							g_CreatorsSelDetails.sTeamOptions[iTeam].iNumberOfActiveRows = ciMAX_RULES 
						ENDIF
						IF g_FMMC_STRUCT.iNumberOfPlayerRules[iTeam] > FMMC_MAX_RULES
							g_FMMC_STRUCT.iNumberOfPlayerRules[iTeam] = FMMC_MAX_RULES
						ENDIF
						sFMMCmenu.iPlayerRuleBitset = 0
					ENDIF
				ENDFOR
				sPlayerRulesPassed.iSwitch = 0
				PRINTLN("sPlayerRulesPassed.iSwitch = ", sPlayerRulesPassed.iSwitch)
			ENDIF
		BREAK
	ENDSWITCH
	
ENDPROC

PROC TEMPORARY_FLOATING_PED_FIX()
	INT  i
	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfPeds i
		IF VDIST2(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].vPos, <<1157.0, 5499.8, -100.5>>) < 4
			DELETE_LAST_PED_PLACED(sPedStruct, sFMMCmenu, i)
			FMMC_DELETE_ANY_OBJECTS_ON_PED(sObjStruct, sFMMCmenu, i)
			PRINTLN("REMOVED FLOATING PED - ", i)
			i--
		ENDIF
	ENDREPEAT
ENDPROC

PROC HANDLE_DRAWING_AND_PLACING_FIREWORK_TRIGGERS()
	INT iProp
	FOR iProp = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfProps -1)
		IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].vFireworkTriggerPos)
		AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropBitSet2, ciFMMC_PROP2_UsePlacedFireworkZone)
			DRAW_DEBUG_SPHERE(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].vFireworkTriggerPos, g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].fFireworkTriggerSize, 255, 155, 0, 60)
		ENDIF
	ENDFOR
	
	IF IS_BIT_SET(sFMMCMenu.iCreatorInfoBitset, ciCIB_PLACING_FIREWORK_ZONE)
		sFMMCmenu.vFireworkTriggerPos = sCurrentVarsStruct.vCoronaPos
	ENDIF
	
	DRAW_DEBUG_SPHERE(sFMMCmenu.vFireworkTriggerPos, sFMMCmenu.fFireworkTriggerSize, 255, 155, 0, 150)
ENDPROC

PROC MAINTAIN_SETTING_RETAINED_BITSETS()
	INT i, i2
	FOR i = 0 TO FMMC_MAX_TEAMS - 1
		FOR i2 = 0 TO FMMC_MAX_GANG_HIDE_LOCATIONS - 1
			IF bLocBitSet[i][i2]
				SET_BIT(g_FMMC_STRUCT.sFMMCEndConditions[i].iAreaRetained, i2)		
			ENDIF 
		ENDFOR	
	ENDFOR	
ENDPROC

PROC HANDLE_REQUESTED_IPL(BOOL bIsSet, STRING sIPLRequested)
	IF bIsSet
	AND NOT IS_IPL_ACTIVE(sIPLRequested)
		PRINTLN("HANDLE_REQUESTED_IPL: Requesting IPL: ",sIPLRequested)
		REQUEST_IPL(sIPLRequested)
	ELIF NOT bIsSet
	AND IS_IPL_ACTIVE(sIPLRequested)
		PRINTLN("HANDLE_REQUESTED_IPL: Removing IPL: ",sIPLRequested)
		REMOVE_IPL(sIPLRequested)
	ENDIF
ENDPROC

PROC HANDLE_INTERIOR_ENTITY_SET(BOOL bActivate, INTERIOR_INSTANCE_INDEX iInteriorInstance, STRING sEntitySetName)
	IF bActivate
		IF NOT IS_INTERIOR_ENTITY_SET_ACTIVE(iInteriorInstance, sEntitySetName)
			ACTIVATE_INTERIOR_ENTITY_SET(iInteriorInstance, sEntitySetName)
			PRINTLN("ACTIVATE_INTERIOR_ENTITY_SET - \"", sEntitySetName, "\"")
			
			REFRESH_INTERIOR(iInteriorInstance)
		ENDIF
	ELSE
		IF IS_INTERIOR_ENTITY_SET_ACTIVE(iInteriorInstance, sEntitySetName)
			DEACTIVATE_INTERIOR_ENTITY_SET(iInteriorInstance, sEntitySetName)
			PRINTLN("DEACTIVATE_INTERIOR_ENTITY_SET - \"", sEntitySetName, "\"")
			
			REFRESH_INTERIOR(iInteriorInstance)
		ENDIF
	ENDIF
ENDPROC

PROC HANDLE_INTERIOR_ENTITY_SETS()
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_BUNKER)
		INTERIOR_INSTANCE_INDEX interiorBunkerID
		interiorBunkerID = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<938.307678, -3196.111572, -100.000000>>, "gr_grdlc_int_02")
		
		HANDLE_INTERIOR_ENTITY_SET(IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciBUNKER_STANDARD_SET), interiorBunkerID, "Standard_Bunker_Set")
		HANDLE_INTERIOR_ENTITY_SET(IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciBUNKER_UPGRADE_SET), interiorBunkerID, "Upgrade_Bunker_Set")
		HANDLE_INTERIOR_ENTITY_SET(IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciBUNKER_STYLE_A), interiorBunkerID, "Bunker_Style_A")
		HANDLE_INTERIOR_ENTITY_SET(IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciBUNKER_STYLE_B), interiorBunkerID, "Bunker_Style_B")
		HANDLE_INTERIOR_ENTITY_SET(IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciBUNKER_STYLE_C), interiorBunkerID, "Bunker_Style_C")
		HANDLE_INTERIOR_ENTITY_SET(IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciBUNKER_OFFICE_UPGRADE_SET), interiorBunkerID, "Office_Upgrade_Set")
		HANDLE_INTERIOR_ENTITY_SET(IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciBUNKER_OFFICE_BLOCKER_SET), interiorBunkerID, "Office_Blocker_Set")
		HANDLE_INTERIOR_ENTITY_SET(IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciBUNKER_GUN_RANGE_BLOCKER_SET), interiorBunkerID, "Gun_Range_Blocker_Set")
		HANDLE_INTERIOR_ENTITY_SET(IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciBUNKER_GUN_LOCKER_UPGRADE), interiorBunkerID, "Gun_locker_upgrade")
		HANDLE_INTERIOR_ENTITY_SET(IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciBUNKER_STANDARD_SECURITY_SET), interiorBunkerID, "Standard_Security_Set")
		HANDLE_INTERIOR_ENTITY_SET(IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciBUNKER_SECURITY_UPGRADE), interiorBunkerID, "Security_upgrade")
		HANDLE_INTERIOR_ENTITY_SET(IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciBUNKER_GUN_WALL_BLOCKER), interiorBunkerID, "Gun_wall_blocker")
		HANDLE_INTERIOR_ENTITY_SET(IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciBUNKER_STANDARD_SET_MORE), interiorBunkerID, "Standard_Bunker_Set_More")
		HANDLE_INTERIOR_ENTITY_SET(IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciBUNKER_UPGRADE_SET_MORE), interiorBunkerID, "Upgrade_Bunker_Set_More")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_HANGAR)
		INTERIOR_DATA_STRUCT structInteriorData	= GET_INTERIOR_DATA(INTERIOR_V_HANGAR)			
		INTERIOR_INSTANCE_INDEX iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		SET_UP_FMMC_HANGAR_INTERIOR_EXTRAS(iInteriorIndex)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_IMPORT_WAREHOUSE)
		INTERIOR_DATA_STRUCT structInteriorData	= GET_INTERIOR_DATA(INTERIOR_V_IMPORT_WAREHOUSE)			
		INTERIOR_INSTANCE_INDEX iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		SET_UP_FMMC_IMPORT_WAREHOUSE_INTERIOR_EXTRAS(iInteriorIndex)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_METH_FACTORY_MURRIETA)		
		INTERIOR_INSTANCE_INDEX iInteriorIndex		
		INTERIOR_DATA_STRUCT structInteriorData	
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_METH_FACTORY_MURRIETA)			
		iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		IF iInteriorIndex = NULL
			PRINTLN("[RCC MISSION] INTERIOR_V_METH_FACTORY_MURRIETA iInteriorIndex = NULL Set up failed")
		ENDIF
		
		IF IS_VALID_INTERIOR(iInteriorIndex)		
			IF g_FMMC_STRUCT.iMethLabInstanceWallTints[ciMETH_LAB_INSTANCE_MURRIETA] != -1 
				IF iMethLabInstanceWallTintsCache[ciMETH_LAB_INSTANCE_MURRIETA] != g_FMMC_STRUCT.iMethLabInstanceWallTints[ciMETH_LAB_INSTANCE_MURRIETA]
					
					PRINTLN("HANDLE_INTERIOR_ENTITY_SETS - Setting up INTERIOR_V_METH_FACTORY_MURRIETA TINTS")
					
					TURN_ON_INTERIOR_ENTITY_SET(iInteriorIndex, "tintable_walls")
					SET_INTERIOR_ENTITY_SET_TINT_INDEX(iInteriorIndex, "tintable_walls", g_FMMC_STRUCT.iMethLabInstanceWallTints[ciMETH_LAB_INSTANCE_MURRIETA])					
					REFRESH_INTERIOR(iInteriorIndex)
					PIN_INTERIOR_IN_MEMORY(iInteriorIndex)
					
					iMethLabInstanceWallTintsCache[ciMETH_LAB_INSTANCE_MURRIETA] = g_FMMC_STRUCT.iMethLabInstanceWallTints[ciMETH_LAB_INSTANCE_MURRIETA]
				ENDIF
			ELSE 
				TURN_OFF_INTERIOR_ENTITY_SET(iInteriorIndex, "tintable_walls")
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_METH_FACTORY_EAST_VINEWOOD)
		PRINTLN("[RCC MISSION][MISSIONFACTORY] Setting up INTERIOR3_METH_FACTORY_EAST_VINEWOOD")
		INTERIOR_INSTANCE_INDEX iInteriorIndex		
		INTERIOR_DATA_STRUCT structInteriorData	
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_METH_FACTORY_EAST_VINEWOOD)			
		iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		IF iInteriorIndex = NULL
			PRINTLN("[RCC MISSION] INTERIOR_V_METH_FACTORY_EAST_VINEWOOD iInteriorIndex = NULL Set up failed")
		ENDIF		
		IF IS_VALID_INTERIOR(iInteriorIndex)		
			IF g_FMMC_STRUCT.iMethLabInstanceWallTints[ciMETH_LAB_INSTANCE_EAST_VINEWOOD] != -1 
				IF iMethLabInstanceWallTintsCache[ciMETH_LAB_INSTANCE_EAST_VINEWOOD] != g_FMMC_STRUCT.iMethLabInstanceWallTints[ciMETH_LAB_INSTANCE_EAST_VINEWOOD]
					
					PRINTLN("HANDLE_INTERIOR_ENTITY_SETS - Setting up INTERIOR_V_METH_FACTORY_EAST_VINEWOOD TINTS")
					
					TURN_ON_INTERIOR_ENTITY_SET(iInteriorIndex, "tintable_walls")
					SET_INTERIOR_ENTITY_SET_TINT_INDEX(iInteriorIndex, "tintable_walls", g_FMMC_STRUCT.iMethLabInstanceWallTints[ciMETH_LAB_INSTANCE_EAST_VINEWOOD])					
					REFRESH_INTERIOR(iInteriorIndex)
					PIN_INTERIOR_IN_MEMORY(iInteriorIndex)
					
					iMethLabInstanceWallTintsCache[ciMETH_LAB_INSTANCE_EAST_VINEWOOD] = g_FMMC_STRUCT.iMethLabInstanceWallTints[ciMETH_LAB_INSTANCE_EAST_VINEWOOD]
				ENDIF
			ELSE 
				TURN_OFF_INTERIOR_ENTITY_SET(iInteriorIndex, "tintable_walls")
			ENDIF
		ENDIF
	ENDIF
		
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_METH_FACTORY_SENORA_DESERT_1)
		PRINTLN("[RCC MISSION][MISSIONFACTORY] Setting up INTERIOR3_METH_FACTORY_SENORA_DESERT_1")
		INTERIOR_INSTANCE_INDEX iInteriorIndex		
		INTERIOR_DATA_STRUCT structInteriorData	
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_METH_FACTORY_SENORA_DESERT_1)			
		iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		IF iInteriorIndex = NULL
			PRINTLN("[RCC MISSION] INTERIOR_V_METH_FACTORY_SENORA_DESERT_1 iInteriorIndex = NULL Set up failed")
		ENDIF
		IF IS_VALID_INTERIOR(iInteriorIndex)		
			IF g_FMMC_STRUCT.iMethLabInstanceWallTints[ciMETH_LAB_INSTANCE_SENORA_DESERT_1] != -1 
				IF iMethLabInstanceWallTintsCache[ciMETH_LAB_INSTANCE_SENORA_DESERT_1] != g_FMMC_STRUCT.iMethLabInstanceWallTints[ciMETH_LAB_INSTANCE_SENORA_DESERT_1]
					
					PRINTLN("HANDLE_INTERIOR_ENTITY_SETS - Setting up INTERIOR_V_METH_FACTORY_SENORA_DESERT_1 TINTS")
					
					TURN_ON_INTERIOR_ENTITY_SET(iInteriorIndex, "tintable_walls")
					SET_INTERIOR_ENTITY_SET_TINT_INDEX(iInteriorIndex, "tintable_walls", g_FMMC_STRUCT.iMethLabInstanceWallTints[ciMETH_LAB_INSTANCE_SENORA_DESERT_1])					
					REFRESH_INTERIOR(iInteriorIndex)
					PIN_INTERIOR_IN_MEMORY(iInteriorIndex)
					
					iMethLabInstanceWallTintsCache[ciMETH_LAB_INSTANCE_SENORA_DESERT_1] = g_FMMC_STRUCT.iMethLabInstanceWallTints[ciMETH_LAB_INSTANCE_SENORA_DESERT_1]
				ENDIF
			ELSE 
				TURN_OFF_INTERIOR_ENTITY_SET(iInteriorIndex, "tintable_walls")
			ENDIF
		ENDIF		
	ENDIF
		
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_METH_FACTORY_SENORA_DESERT_2)
		PRINTLN("[RCC MISSION][MISSIONFACTORY] Setting up INTERIOR3_METH_FACTORY_SENORA_DESERT_2")
		INTERIOR_INSTANCE_INDEX iInteriorIndex		
		INTERIOR_DATA_STRUCT structInteriorData	
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_METH_FACTORY_SENORA_DESERT_2)			
		iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		IF iInteriorIndex = NULL
			PRINTLN("[RCC MISSION] INTERIOR_V_METH_FACTORY_SENORA_DESERT_2 iInteriorIndex = NULL Set up failed")
		ENDIF
		
		IF IS_VALID_INTERIOR(iInteriorIndex)		
			IF g_FMMC_STRUCT.iMethLabInstanceWallTints[ciMETH_LAB_INSTANCE_SENORA_DESERT_2] != -1 
				IF iMethLabInstanceWallTintsCache[ciMETH_LAB_INSTANCE_SENORA_DESERT_2] != g_FMMC_STRUCT.iMethLabInstanceWallTints[ciMETH_LAB_INSTANCE_SENORA_DESERT_2]
					
					PRINTLN("HANDLE_INTERIOR_ENTITY_SETS - Setting up INTERIOR_V_METH_FACTORY_SENORA_DESERT_2 TINTS")
					
					TURN_ON_INTERIOR_ENTITY_SET(iInteriorIndex, "tintable_walls")
					SET_INTERIOR_ENTITY_SET_TINT_INDEX(iInteriorIndex, "tintable_walls", g_FMMC_STRUCT.iMethLabInstanceWallTints[ciMETH_LAB_INSTANCE_SENORA_DESERT_2])					
					REFRESH_INTERIOR(iInteriorIndex)
					PIN_INTERIOR_IN_MEMORY(iInteriorIndex)
					
					iMethLabInstanceWallTintsCache[ciMETH_LAB_INSTANCE_SENORA_DESERT_2] = g_FMMC_STRUCT.iMethLabInstanceWallTints[ciMETH_LAB_INSTANCE_SENORA_DESERT_2]
				ENDIF
			ELSE 
				TURN_OFF_INTERIOR_ENTITY_SET(iInteriorIndex, "tintable_walls")
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_TUNER_MOD_GARAGE)
		PRINTLN("[RCC MISSION] Setting up INTERIOR3_TUNER_MOD_GARAGE")
		
		INTERIOR_INSTANCE_INDEX iInteriorIndex		
		INTERIOR_DATA_STRUCT structInteriorData	
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_TUNER_MOD_GARAGE)			
		iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		IF iInteriorIndex = NULL
			PRINTLN("[RCC MISSION] INTERIOR3_TUNER_MOD_GARAGE iInteriorIndex = NULL Set up failed")
		ENDIF
		
		IF IS_VALID_INTERIOR(iInteriorIndex)		
			HANDLE_INTERIOR_ENTITY_SET(TRUE, iInteriorIndex, "entity_set_bedroom")
			HANDLE_INTERIOR_ENTITY_SET(TRUE, iInteriorIndex, "entity_set_bedroom_empty")
			HANDLE_INTERIOR_ENTITY_SET(TRUE, iInteriorIndex, "entity_set_box_clutter")
			HANDLE_INTERIOR_ENTITY_SET(TRUE, iInteriorIndex, "entity_set_car_lift_cutscene")
			HANDLE_INTERIOR_ENTITY_SET(TRUE, iInteriorIndex, "entity_set_car_lift_default")
			HANDLE_INTERIOR_ENTITY_SET(TRUE, iInteriorIndex, "entity_set_car_lift_purchase")
			HANDLE_INTERIOR_ENTITY_SET(TRUE, iInteriorIndex, "entity_set_chalkboard")
			HANDLE_INTERIOR_ENTITY_SET(TRUE, iInteriorIndex, "entity_set_cut_seats")
			HANDLE_INTERIOR_ENTITY_SET(TRUE, iInteriorIndex, "entity_set_style_1")
			HANDLE_INTERIOR_ENTITY_SET(TRUE, iInteriorIndex, "entity_set_table")
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_RECORDING_STUDIO)
		INTERIOR_INSTANCE_INDEX interiorRecordingStudioID
		interiorRecordingStudioID = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<-1010, -70, -99.4>>, "sf_dlc_studio_sec")
		
		HANDLE_INTERIOR_ENTITY_SET(NOT IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_RECORDING_STUDIO_ST_SET), interiorRecordingStudioID, "entity_set_fix_stu_ext_p1")
		HANDLE_INTERIOR_ENTITY_SET(IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_RECORDING_STUDIO_ST_SET), interiorRecordingStudioID, "entity_set_fix_trip1_int_p2")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_MUSIC_LOCKER)
		INTERIOR_INSTANCE_INDEX interiorMusicLockerID
		interiorMusicLockerID = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<1550, 250, -50>>, "h4_dlc_int_02_h4")
		
		HANDLE_INTERIOR_ENTITY_SET(IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_MUSIC_LOCKER_ENABLE_DJ_SET), interiorMusicLockerID, "dj_04_lights_04")
		HANDLE_INTERIOR_ENTITY_SET(IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_MUSIC_LOCKER_ENABLE_DJ_SET), interiorMusicLockerID, "entityset_dj_lighting")
		HANDLE_INTERIOR_ENTITY_SET(IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_MUSIC_LOCKER_ENABLE_DJ_SET), interiorMusicLockerID, "int01_ba_bar_content")
		HANDLE_INTERIOR_ENTITY_SET(IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_MUSIC_LOCKER_ENABLE_DJ_SET), interiorMusicLockerID, "int01_ba_dj04")
		HANDLE_INTERIOR_ENTITY_SET(IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_MUSIC_LOCKER_ENABLE_DJ_SET), interiorMusicLockerID, "int01_ba_dry_ice")
		HANDLE_INTERIOR_ENTITY_SET(IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_MUSIC_LOCKER_ENABLE_DJ_SET), interiorMusicLockerID, "int01_ba_equipment_upgrade")
		HANDLE_INTERIOR_ENTITY_SET(IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_MUSIC_LOCKER_ENABLE_DJ_SET), interiorMusicLockerID, "int01_ba_security_upgrade")
		HANDLE_INTERIOR_ENTITY_SET(IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_MUSIC_LOCKER_ENABLE_DJ_SET), interiorMusicLockerID, "int01_ba_style02_podium")
		HANDLE_INTERIOR_ENTITY_SET(IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_MUSIC_LOCKER_ENABLE_DJ_SET), interiorMusicLockerID, "int01_ba_lightgrid_01")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_ARENA_WAR_GARAGE_LEVEL_1)
		PRINTLN("[RCC MISSION] Setting up INTERIOR3_ARENA_WAR_GARAGE_LEVEL_1")
		INTERIOR_INSTANCE_INDEX iInteriorIndex		
		INTERIOR_DATA_STRUCT structInteriorData	
		structInteriorData = GET_INTERIOR_DATA(INTERIOR_V_ARENA_WAR_GARAGE_LEVEL_1)			
		iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(structInteriorData.vPos, structInteriorData.sInteriorName)
		IF iInteriorIndex = NULL
			PRINTLN("[RCC MISSION] INTERIOR_V_ARENA_WAR_GARAGE_LEVEL_1 iInteriorIndex = NULL Set up failed")
		ENDIF
		
		HANDLE_INTERIOR_ENTITY_SET(IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_ARENA_WAR_GARAGE_LEVEL_1), iInteriorIndex, "set_int_mod2_b_tint")
		SET_INTERIOR_ENTITY_SET_TINT_INDEX(iInteriorIndex, "set_int_mod2_b_tint", 8)
		HANDLE_INTERIOR_ENTITY_SET(IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_ARENA_WAR_GARAGE_LEVEL_1), iInteriorIndex, "set_int_mod2_b1")
		SET_INTERIOR_ENTITY_SET_TINT_INDEX(iInteriorIndex, "set_int_mod2_b1", 8)
		HANDLE_INTERIOR_ENTITY_SET(IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_ARENA_WAR_GARAGE_LEVEL_1), iInteriorIndex, "set_mod2_style_01")
	ENDIF
	
ENDPROC


PROC REMOVE_ROCKET_POSITION(INT iNumberPassed)

	INT i
	FOR i = iNumberPassed TO FMMC_MAX_NUM_ZONE_ROCKET_POS-1
		IF i < FMMC_MAX_NUM_ZONE_ROCKET_POS-1
			g_FMMC_STRUCT_ENTITIES.vFMMCZoneRocketPositions[i] = g_FMMC_STRUCT_ENTITIES.vFMMCZoneRocketPositions[i+1]
			g_FMMC_STRUCT_ENTITIES.iFMMCZoneRocketZoneBS[i] = g_FMMC_STRUCT_ENTITIES.iFMMCZoneRocketZoneBS[i+1]
			g_FMMC_STRUCT_ENTITIES.iFMMCZoneRocketLinkedObj[i] = g_FMMC_STRUCT_ENTITIES.iFMMCZoneRocketLinkedObj[i+1]
			g_FMMC_STRUCT_ENTITIES.fFMMCZoneRocketAccuracy[i] = g_FMMC_STRUCT_ENTITIES.fFMMCZoneRocketAccuracy[i+1]
			g_FMMC_STRUCT_ENTITIES.iFMMCZoneRocketCustomAccuracyOnlyInThisVeh[i] = g_FMMC_STRUCT_ENTITIES.iFMMCZoneRocketCustomAccuracyOnlyInThisVeh[i+1]
			
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.iFMMCZoneRocketNotHomingBS, i+1)
				SET_BIT(g_FMMC_STRUCT_ENTITIES.iFMMCZoneRocketNotHomingBS, i)
			ELSE
				CLEAR_BIT(g_FMMC_STRUCT_ENTITIES.iFMMCZoneRocketNotHomingBS, i)
			ENDIF
			
		ELSE
			g_FMMC_STRUCT_ENTITIES.vFMMCZoneRocketPositions[i] = <<0,0,0>>
			g_FMMC_STRUCT_ENTITIES.iFMMCZoneRocketZoneBS[i] = 0
			g_FMMC_STRUCT_ENTITIES.iFMMCZoneRocketLinkedObj[i] = -1
			g_FMMC_STRUCT_ENTITIES.fFMMCZoneRocketAccuracy[i] = 100.0
			g_FMMC_STRUCT_ENTITIES.iFMMCZoneRocketCustomAccuracyOnlyInThisVeh[i] = -1
			CLEAR_BIT(g_FMMC_STRUCT_ENTITIES.iFMMCZoneRocketNotHomingBS, i)
		ENDIF
	ENDFOR
	
	g_FMMC_STRUCT_ENTITIES.iNumberOfRocketPositions--
	
ENDPROC

PROC MAINTAIN_ROCKET_POSITIONS()
	INT i, iR, iG, iB, iA
	FLOAT fX, fY
	BOOL bTemp
	
	FOR i = 0 TO FMMC_MAX_NUM_ZONE_ROCKET_POS - 1
		IF i != sFMMCMenu.iSelectedEntity
		AND sFMMCmenu.iEntityCreation = CREATION_TYPE_ROCKET_POSITIONS
			IF IS_ENTITY_IN_THE_CORONA_NEW(sFMMCmenu, sHCS, sCurrentVarsStruct, iLocalBitSet, CREATION_TYPE_ROCKET_POSITIONS, i, NULL, bTemp, HUD_COLOUR_YELLOWDARK, sFMMCdata, sFMMCendStage)
				IF IS_BIT_SET(iLocalBitSet, biPickupButtonPressed)
					PICK_UP_PLACED_ENTITY(sCurrentVarsStruct, sFMMCmenu, CREATION_TYPE_ROCKET_POSITIONS, i)
					CLEAR_BIT(iLocalBitSet, biPickupButtonPressed)
					
				ELIF IS_BIT_SET(iLocalBitSet, biDeleteButtonPressed)
					REMOVE_ROCKET_POSITION(i)
					CLEAR_BIT(iLocalBitSet, biDeleteButtonPressed)
					
					IF sFMMCMenu.sActiveMenu = eFmmc_ROCKET_POSITIONS
					OR sFMMCMenu.sActiveMenu = eFMMC_ROCKET_POSITION_LINKED_ZONES
						REFRESH_MENU(sFMMCMenu)
						SET_CURRENT_GENERIC_CREATION_STAGE(CREATION_STAGE_WAIT)
					ENDIF
				ENDIF
			ELSE
				IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.vFMMCZoneRocketPositions[i])
					IF NOT DOES_BLIP_EXIST(bRocketPositionBlips[i])
						CREATE_FMMC_BLIP(bRocketPositionBlips[i], g_FMMC_STRUCT_ENTITIES.vFMMCZoneRocketPositions[i], HUD_COLOUR_YELLOWDARK, "Rocket Position", BLIP_SIZE_COORD)
						SET_BLIP_COLOUR(bRocketPositionBlips[i], GET_BLIP_COLOUR_FROM_HUD_COLOUR(HUD_COLOUR_YELLOWDARK))
					ENDIF
					
					IF GET_SCREEN_COORD_FROM_WORLD_COORD(g_FMMC_STRUCT_ENTITIES.vFMMCZoneRocketPositions[i], fX, fY)	
						GET_HUD_COLOUR(HUD_COLOUR_YELLOWDARK, iR, iG, iB, iA)
						DRAW_MARKER(MARKER_SPHERE, g_FMMC_STRUCT_ENTITIES.vFMMCZoneRocketPositions[i], <<0,0,0>>, <<0,0,0>>, <<1.5, 1.5, 1.5>>, iR, iG, iB, 200)
					ENDIF
				ELSE
					IF DOES_BLIP_EXIST(bRocketPositionBlips[i])
						REMOVE_BLIP(bRocketPositionBlips[i])
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF DOES_BLIP_EXIST(bRocketPositionBlips[i])
				REMOVE_BLIP(bRocketPositionBlips[i])
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

PROC MAINTAIN_TRIP_SKIP_PLACEMENT()

	INT iTeam, iR, iG, iB, iA
	FLOAT fX, fY
	BOOL bTemp 
		
	
	CLAMP_INT(sFMMCmenu.iSelectedTeam, 0, FMMC_MAX_TEAMS)
	
	IF sFMMCmenu.sActiveMenu = efMMC_TRIP_SKIPS
		IF sFMMCmenu.iSelectedTeam < g_FMMC_STRUCT.iMaxNumberOfTeams
			GET_HUD_COLOUR(GET_TEAM_SPAWN_COLOUR(sFMMCmenu.iSelectedTeam),iR, iG, iB, iA)
			DRAW_MARKER(MARKER_HALO_POINT, <<sCurrentVarsStruct.vCoronaPos.x, sCurrentVarsStruct.vCoronaPos.y, sCurrentVarsStruct.vCoronaPos.z>> + <<0,0,0.7>>, <<0,0,0>>, <<0,0,g_FMMC_STRUCT.fTripSkipHeading[sFMMCmenu.iSelectedTeam]>>, <<1.0,1.0,1.0>>, iR, iG, iB, 200)
		ELSE
			GET_HUD_COLOUR(HUD_COLOUR_WHITE,iR, iG, iB, iA)
			DRAW_MARKER(MARKER_HALO_POINT, <<sCurrentVarsStruct.vCoronaPos.x, sCurrentVarsStruct.vCoronaPos.y, sCurrentVarsStruct.vCoronaPos.z>> + <<0,0,0.7>>, <<0,0,0>>, <<0,0,g_FMMC_STRUCT.fAutoTripSkipHeading>>, <<1.0,1.0,1.0>>, iR, iG, iB, 200)
		ENDIF
	ENDIF
	
	IF sCurrentVarsStruct.iMenuState != MENU_STATE_TEST_MISSION
	
		
		IF sFMMCmenu.sActiveMenu != efMMC_TRIP_SKIP_CAM
			
			FOR iTeam = 0 TO FMMC_MAX_TEAMS-1
								
				IF IS_ENTITY_IN_THE_CORONA_NEW(sFMMCmenu, sHCS, sCurrentVarsStruct, iLocalBitSet, CREATION_TYPE_TRIP_SKIPS, iTeam, NULL, bTemp, HUD_COLOUR_PURPLE, sFMMCdata, sFMMCendStage, 2.0)
				
					IF IS_BIT_SET(iLocalBitSet, biDeleteButtonPressed)
						IF iTeam < g_FMMC_STRUCT.iMaxNumberOfTeams
							IF (GET_CREATOR_MENU_SELECTION(sFMMCmenu) != TRIP_SKIP_SET_CAMERA OR IS_VECTOR_ZERO(g_FMMC_STRUCT.vTripSkipCamPos[iTeam]))
								g_FMMC_STRUCT.vTripSkipPos[iTeam]  	= <<0,0,0>>
								g_FMMC_STRUCT.vTripSkipCamPos[iTeam] 	= <<0,0,0>>
								g_FMMC_STRUCT.vTripSkipCamRot[iTeam] 	= <<0,0,0>>
								g_FMMC_STRUCT.fTripSkipCamFOV[iTeam] 	= 45.0
							ENDIF
						ELSE
							IF (GET_CREATOR_MENU_SELECTION(sFMMCmenu) != TRIP_SKIP_SET_CAMERA OR IS_VECTOR_ZERO(g_FMMC_STRUCT.vAutoTripSkipCamPos))
								g_FMMC_STRUCT.vAutoTripSkipPos 		= <<0,0,0>>
								g_FMMC_STRUCT.vAutoTripSkipCamPos 	= <<0,0,0>>
								g_FMMC_STRUCT.vAutoTripSkipCamRot 	= <<0,0,0>>
								g_FMMC_STRUCT.fAutoTripSkipCamFOV 	= 45.0
							ENDIF
						ENDIF
					ENDIF
					
				ELSE
					
					IF IS_VECTOR_ZERO(g_FMMC_STRUCT.vTripSkipPos[iTeam])
						RELOOP
					ENDIF
					
					IF iTeam < g_FMMC_STRUCT.iMaxNumberOfTeams
					
						GET_HUD_COLOUR(GET_TEAM_SPAWN_COLOUR(iTeam),iR, iG, iB, iA)
						IF GET_SCREEN_COORD_FROM_WORLD_COORD(g_FMMC_STRUCT.vTripSkipPos[iTeam], fX, fY)
							DRAW_MARKER(MARKER_HALO_POINT, 	<<g_FMMC_STRUCT.vTripSkipPos[iTeam].x, g_FMMC_STRUCT.vTripSkipPos[iTeam].y, g_FMMC_STRUCT.vTripSkipPos[iTeam].z>> + <<0,0,0.5>>, <<0,0,0>>, <<0,0,g_FMMC_STRUCT.fTripSkipHeading[iTeam]>>, 	<<1.0,1.0,1.0>>, iR, iG, iB, 255)
						ENDIF
						
						IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.vTripSkipCamPos[iTeam])
							IF GET_SCREEN_COORD_FROM_WORLD_COORD(g_FMMC_STRUCT.vTripSkipCamPos[iTeam], fX, fY)
							OR GET_SCREEN_COORD_FROM_WORLD_COORD(g_FMMC_STRUCT.vTripSkipPos[iTeam], fX, fY)
								DRAW_MARKER(MARKER_SPHERE, g_FMMC_STRUCT.vTripSkipCamPos[iTeam], <<0,0,0>>, <<0,0,0>>, 	<<0.7,0.7,0.7>>, iR, iG, iB, 125)
								DRAW_LINE(g_FMMC_STRUCT.vTripSkipCamPos[iTeam], g_FMMC_STRUCT.vTripSkipPos[iTeam] + <<0,0,0.5>>, iR, iG, iB, 120)
							ENDIF
						ENDIF
						
					ELSE
					
						GET_HUD_COLOUR(HUD_COLOUR_WHITE,iR, iG, iB, iA)
						IF GET_SCREEN_COORD_FROM_WORLD_COORD(g_FMMC_STRUCT.vAutoTripSkipPos, fX, fY)
							DRAW_MARKER(MARKER_HALO_POINT, 	<<g_FMMC_STRUCT.vAutoTripSkipPos.x, g_FMMC_STRUCT.vAutoTripSkipPos.y, g_FMMC_STRUCT.vAutoTripSkipPos.z>> + <<0,0,0.5>>, <<0,0,0>>, <<0,0,g_FMMC_STRUCT.fAutoTripSkipHeading>>, 	<<1.0,1.0,1.0>>, iR, iG, iB, 255)
						ENDIF
						
						IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.vAutoTripSkipCamPos)
							IF GET_SCREEN_COORD_FROM_WORLD_COORD(g_FMMC_STRUCT.vAutoTripSkipCamPos, fX, fY)
							OR GET_SCREEN_COORD_FROM_WORLD_COORD(g_FMMC_STRUCT.vAutoTripSkipPos, fX, fY)
								DRAW_MARKER(MARKER_SPHERE, g_FMMC_STRUCT.vAutoTripSkipCamPos, <<0,0,0>>, <<0,0,0>>, 	<<0.7,0.7,0.7>>, iR, iG, iB, 125)
								DRAW_LINE(g_FMMC_STRUCT.vAutoTripSkipCamPos, g_FMMC_STRUCT.vAutoTripSkipPos + <<0,0,0.5>>, iR, iG, iB, 120)
							ENDIF
						ENDIF
						
					ENDIF
					
				ENDIF
				
			ENDFOR
			
		ENDIF
		
		IF sFMMCmenu.sActiveMenu = efMMC_TRIP_SKIPS
		
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) != TRIP_SKIP_SET_CAMERA
			AND IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
				IF sFMMCmenu.iSelectedTeam < g_FMMC_STRUCT.iMaxNumberOfTeams
					g_FMMC_STRUCT.vTripSkipPos[sFMMCmenu.iSelectedTeam] = sCurrentVarsStruct.vCoronaPos
				ELSE
					g_FMMC_STRUCT.vAutoTripSkipPos = sCurrentVarsStruct.vCoronaPos
				ENDIF
			ENDIF
			
			IF sFMMCmenu.iSelectedTeam < g_FMMC_STRUCT.iMaxNumberOfTeams
				IF IS_VECTOR_ZERO(g_FMMC_STRUCT.vTripSkipPos[sFMMCmenu.iSelectedTeam])
					IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LB)
						g_FMMC_STRUCT.fTripSkipHeading[sFMMCmenu.iSelectedTeam] += 5.0
					ELIF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RB)
						g_FMMC_STRUCT.fTripSkipHeading[sFMMCmenu.iSelectedTeam] -= 5.0
					ENDIF
				ENDIF
			ELSE
				IF IS_VECTOR_ZERO(g_FMMC_STRUCT.vAutoTripSkipPos)
					IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LB)
						g_FMMC_STRUCT.fAutoTripSkipHeading += 5.0
					ELIF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RB)
						g_FMMC_STRUCT.fAutoTripSkipHeading -= 5.0
					ENDIF
				ENDIF
			ENDIF
			
			UPDATE_THE_CORONA_DISC_VALUES(sCurrentVarsStruct, NULL, TRUE, FALSE, fDEFAULT_PLACEMENT_DISC_HEIGHT, FALSE)
			
		ENDIF
		
		VECTOR vPos
		FLOAT fHead
		IF sFMMCmenu.iSelectedTeam < g_FMMC_STRUCT.iMaxNumberOfTeams
			vPos = g_FMMC_STRUCT.vTripSkipPos[sFMMCmenu.iSelectedTeam]
			fHead = g_FMMC_STRUCT.fTripSkipHeading[sFMMCmenu.iSelectedTeam]
		ELSE
			vPos = g_FMMC_STRUCT.vAutoTripSkipPos
			fHead = g_FMMC_STRUCT.fAutoTripSkipHeading
		ENDIF
		
		IF sFMMCmenu.sActiveMenu = efMMC_TRIP_SKIP_CAM
		AND NOT IS_VECTOR_ZERO(vPos)
			IF NOT DOES_ENTITY_EXIST(vehTripSkip)
				REQUEST_MODEL(TAILGATER)
				IF HAS_MODEL_LOADED(TAILGATER)
					vehTripSkip = CREATE_VEHICLE(TAILGATER, vPos, fHead)
					SET_VEHICLE_ON_GROUND_PROPERLY(vehTripSkip)
					FREEZE_ENTITY_POSITION(vehTripSkip, TRUE)
					SET_ENTITY_COLLISION(vehTripSkip, FALSE)
					SET_ENTITY_ALPHA(vehTripSkip, 200, FALSE)
				ENDIF
			ENDIF
		ELSE
			IF DOES_ENTITY_EXIST(vehTripSkip)
				DELETE_VEHICLE(vehTripSkip)
			ENDIF
		ENDIF
		
	ENDIF
	
ENDPROC

PROC  DEAL_WITH_SELECTING_TRIGGER_LOCATION()
	IF DO_TRIGGER_CREATION(sFMMCmenu, sCurrentVarsStruct, sFMMCdata, sStartEndBlips.biStart, sHCS.hcStartCoronaColour,sStartEndBlips.ciStartType, sFMMCendStage,iTriggerCreationStage,  -1 #IF IS_DEBUG_BUILD, bDoTriggerRestricted #ENDIF)	
		// If the start positions haven't been set yet, make them the same as the start trigger.
		// Note we FALSE g_FMMC_STRUCT.sFMMCEndConditions[iTeam].bOkToDrawStartPosCorona, we only want the team starts to be drawn once the user has placed themselves.
		INT iTeam
		REPEAT FMMC_MAX_TEAMS iTeam
			g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vStartPos = g_FMMC_STRUCT.vStartPos
		ENDREPEAT
		
		UNLOCK_MENU_ITEMS_AND_ALLOW_MISSION_TEST()
	ENDIF
ENDPROC

PROC DEAL_WITH_SELECTING_START_END_LOCATIONS()
	
	#IF IS_DEBUG_BUILD
		IF bDrawMenu
			EXIT
		ENDIF
	#ENDIF
	
	INT iTeam

	IF sFMMCMenu.bRuleMenuActive	 = FALSE
		IF sFMMCMenu.iSelectedEntity = -1
		AND sFMMCmenu.iEntityCreation = - 1
			IF sCurrentVarsStruct.iReSelectTimer < GET_GAME_TIMER() 		
				REPEAT FMMC_MAX_TEAMS iTeam
					IF IS_VECTOR_POINT_IN_CORONA(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vStartPos)
						IF NOT ARE_VECTORS_EQUAL(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vStartPos, g_FMMC_STRUCT.vStartPos)
							SET_BIT(iLocalBitSet, biEntityInCorona)
							IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_X)
								g_FMMC_STRUCT.sFMMCEndConditions[iTeam].fStartHeading = 0
								g_FMMC_STRUCT.sFMMCEndConditions[iTeam].bOkToDrawStartPosCorona = FALSE
								g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vStartPos = g_FMMC_STRUCT.vStartPos
							ENDIF
						ENDIF
					ENDIF
				ENDREPEAT
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC


PROC MOVE_PED_ASS_OBJ_GOTO_LOCATIONS(ASSOCIATED_GOTO_TASK_DATA &sAssociatedGotoTaskData, INT iStart, BOOL bUp = TRUE)

	INT i, iNum, iArrEnd
	iNum = GET_ASSOCIATED_GOTO_TASK_DATA__NUMBER_OF_GOTOS(-1, sAssociatedGotoTaskData)
	
	IF bUp
	
		IF iNum < MAX_ASSOCIATED_GOTO_TASKS								//check that there are less than MAX_ASSOCIATED_GOTO_TASKS goto location
		
			IF iNum > 1
			
				iArrEnd = iNum-1				
				FOR i = iArrEnd TO iStart STEP - 1	//go from end of the array to start point of shift
					PRINTLN("PD MOVE_PED_GOTO_LOCATIONS UP: writing info at ", i, " to ", i+1)					
					COPY_ASSOCIATED_GOTO_TASK_INDEX_DATA(sFMMCmenu.sAssociatedGotoTaskData, i+1, i)
					CLEAR_PED_ASSOCIATED_GOTO_DATA(sFMMCmenu.sAssociatedGotoTaskData, i)					
				ENDFOR
				
			ENDIF
			
		ENDIF
		
	ELSE
	
		iArrEnd = iNum-1
		FOR i = iStart TO iArrEnd		
			PRINTLN("PD MOVE_PED_GOTO_LOCATIONS DOWN: writing info at ", i+1, " to ", i)	
			IF i+1 < MAX_ASSOCIATED_GOTO_TASKS
				COPY_ASSOCIATED_GOTO_TASK_INDEX_DATA(sFMMCmenu.sAssociatedGotoTaskData, i, i+1)
				CLEAR_PED_ASSOCIATED_GOTO_DATA(sFMMCmenu.sAssociatedGotoTaskData, i+1)
			ENDIF
		ENDFOR
		
	ENDIF
	
ENDPROC

PROC DELETE_PED_ASS_OBJ_GOTO_LOCATION(ASSOCIATED_GOTO_TASK_DATA &sAssociatedGotoTaskData, INT iIndex)
	
	IF iIndex >= 0
	AND iIndex <= MAX_ASSOCIATED_GOTO_TASKS
		MOVE_PED_ASS_OBJ_GOTO_LOCATIONS(sAssociatedGotoTaskData, iIndex, FALSE)		
	ENDIF
	
ENDPROC

FUNC BOOL INSERT_PED_ASS_OBJ_GOTO_LOCATION(ASSOCIATED_GOTO_TASK_DATA &sAssociatedGotoTaskData, VECTOR vPos, FLOAT fHeight)

	INT iIndex = sFMMCmenu.iCurrentGoToLocation
	IF iIndex = -1
		IF GET_ASSOCIATED_GOTO_TASK_DATA__NUMBER_OF_GOTOS(-1, sAssociatedGotoTaskData) >= MAX_ASSOCIATED_GOTO_TASKS
			RETURN FALSE
		ENDIF
		iIndex = CLAMP_INT(GET_ASSOCIATED_GOTO_TASK_DATA__NUMBER_OF_GOTOS(-1, sAssociatedGotoTaskData), 0, MAX_ASSOCIATED_GOTO_TASKS)
	ELSE
		PRINTLN("PD INSERT GOTO: iCurrentGoToLocation = ", sFMMCmenu.iCurrentGoToLocation)
	ENDIF
	
	IF iIndex >= 0
	AND iIndex <= MAX_ASSOCIATED_GOTO_TASKS-1
		PRINTLN("PD INSERT GOTO: Inserting goto at ", iIndex)
		IF sFMMCmenu.iCurrentGoToLocation = -1  //if not editing a point move the points up the array
			MOVE_PED_ASS_OBJ_GOTO_LOCATIONS(sAssociatedGotoTaskData, iIndex)
		ENDIF
		IF sFMMCmenu.bInsertingPoint
			MOVE_PED_ASS_OBJ_GOTO_LOCATIONS(sAssociatedGotoTaskData, iIndex)
		ENDIF
		PRINTLN("PD INSERT GOTO: setting goto ", iIndex, " to ", vPos)
		SET_ASSOCIATED_GOTO_VECTOR(sAssociatedGotoTaskData, iIndex, vPos, fHeight)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL MANAGE_PED_ASS_OBJ_GOTO_LOCATIONS(ASSOCIATED_GOTO_TASK_DATA &sAssociatedGotoTaskData)

	IF sFMMCMenu.sActiveMenu = eFMMC_ASS_OBJECTIVE_ACHIEVE_FINAL_HEADING
		IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RB)
			sFMMCmenu.fCurrentGoToHeading = (sFMMCmenu.fCurrentGoToHeading - 2.5) % 360
		ELIF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LB)
			sFMMCmenu.fCurrentGoToHeading = (sFMMCmenu.fCurrentGoToHeading + 2.5) % 360
		ENDIF			
		IF sFMMCmenu.fCurrentGoToHeading  < 0
			sFMMCmenu.fCurrentGoToHeading = 360 + sFMMCmenu.fCurrentGoToHeading
		ENDIF
	ENDIF
	
	IF (sFMMCMenu.sActiveMenu = eFmmc_ASS_OBJECTIVE_MENU_SET_PED_GOTO_POS
	OR sFMMCMenu.sActiveMenu = eFmmc_ASS_OBJECTIVE_MENU_SET_POOL_GOTO_POS
	OR sFMMCMenu.sActiveMenu = eFMMC_ASS_OBJECTIVE_WAIT_RULE_OPTIONS
	OR sFMMCMenu.sActiveMenu = eFMMC_ASS_OBJECTIVE_WAIT_TIME_OPTIONS
	OR sFMMCMenu.sActiveMenu = eFMMC_ASS_OBJECTIVE_TRIGGER_AREA
	OR sFMMCMenu.sActiveMenu = EFMMC_ASS_OBJECTIVE_SPEED_OPTIONS
	OR sFMMCMenu.sActiveMenu = EFMMC_ASS_OBJECTIVE_RADIUS_OPTIONS
	OR sFMMCMenu.sActiveMenu = eFMMC_ASS_OBJECTIVE_EXIT_VEHICLE_OPTIONS
	OR sFMMCMenu.sActiveMenu = eFMMC_ASS_OBJECTIVE_USE_COMBAT_GOTO
	OR sFMMCMenu.sActiveMenu = eFMMC_ASS_OBJECTIVE_FORCE_NAVMESH
	OR sFMMCMenu.sActiveMenu = eFMMC_ASS_OBJECTIVE_VEHICLE_DISTANCE
	OR sFMMCMenu.sActiveMenu = eFMMC_ASS_OBJECTIVE_IGNORE_NAVMESH
	OR sFMMCMenu.sActiveMenu = eFMMC_ASS_OBJECTIVE_COVER_ONLY
	OR sFMMCMenu.sActiveMenu = eFMMC_ASS_OBJECTIVE_SLOW_AT_DISTANCE
	OR sFMMCMenu.sActiveMenu = eFMMC_ASS_OBJECTIVE_ACHIEVE_FINAL_HEADING
	OR sFMMCMenu.sActiveMenu = eFMMC_ASS_OBJECTIVE_PLANE_TAXI
	OR sFMMCMenu.sActiveMenu = eFMMC_ASS_OBJECTIVE_SPECIFIC_VEH_SETTINGS)
	
	AND (sFMMCmenu.iEntityCreation = CREATION_TYPE_PEDS
	OR sFMMCMenu.sActiveMenu = eFmmc_ASS_OBJECTIVE_MENU_SET_POOL_GOTO_POS)
	
		RESET_UP_HELP()
						
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
		AND sFMMCmenu.sMenuAction[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] = eFmmc_Action_Set_Ass_Vector
		
			PRINTLN("PED ASS OBJ GOTO: Accept pressed")
			PRINTLN("PED ASS OBJ GOTO: current amount of goto points is ", GET_ASSOCIATED_GOTO_TASK_DATA__NUMBER_OF_GOTOS(-1, sAssociatedGotoTaskData))
			
			INT iClosest = GET_CLOSEST_PED_ASS_OBJ_GOTO_LOCATION(sFMMCmenu, sCurrentVarsStruct.vCoronaPos)
			
			IF sFMMCMenu.sActiveMenu = eFMMC_ASS_OBJECTIVE_TRIGGER_AREA
			AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) % 2 = 0
				sAssociatedGotoTaskData.sPointData[GET_CREATOR_MENU_SELECTION(sFMMCmenu)/2].vPlayerActivated_Pos = sCurrentVarsStruct.vCoronaPos
				SET_UP_ASSOCIATED_RULE_GOTO_TRIGGER_AREA_MENU(sFMMCmenu)
			ELIF VDIST(GET_ASSOCIATED_GOTO_TASK_DATA__POSITION(-1, sAssociatedGotoTaskData, iClosest), sCurrentVarsStruct.vCoronaPos) < 0.75
			AND sFMMCmenu.iCurrentGoToLocation = -1
			
				PRINTLN("PED ASS OBJ GOTO: Edit closest")
				
				sFMMCmenu.iCurrentGoToLocation = iClosest
				sFMMCmenu.bExitAtCurrentGoTo = IS_ASSOCIATED_GOTO_TASK_DATA__BIT_SET(-1, sAssociatedGotoTaskData, iClosest, PED_ASSOCIATED_GOTO_TASK_TYPE_EXIT_VEHICLE) 
				sFMMCmenu.vCurrentGoToPos = GET_ASSOCIATED_GOTO_TASK_DATA__POSITION(-1, sAssociatedGotoTaskData, iClosest)
				sFMMCmenu.fCurrentGoToHeight = GET_ASSOCIATED_GOTO_TASK_DATA__ARRIVAL_HEIGHT(-1, sAssociatedGotoTaskData, iClosest)
				sFMMCmenu.fCurrentGoToHeading = sAssociatedGotoTaskData.sPointData[iClosest].fPedAchieveFinalHeading
				
				RETURN TRUE
				
			ELIF IS_PED_ASS_OBJ_LINE_SELECTED(sAssociatedGotoTaskData, sCurrentVarsStruct.vCoronaPos, iClosest)
			AND GET_ASSOCIATED_GOTO_TASK_DATA__NUMBER_OF_GOTOS(-1, sAssociatedGotoTaskData) < MAX_ASSOCIATED_GOTO_TASKS
			AND GET_ASSOCIATED_GOTO_TASK_DATA__NUMBER_OF_GOTOS(-1, sAssociatedGotoTaskData) > 1
			AND sFMMCmenu.iCurrentGoToLocation = -1
			
				INT iNextPoint = iClosest+1
				
				IF iNextPoint >= GET_ASSOCIATED_GOTO_TASK_DATA__NUMBER_OF_GOTOS(-1, sAssociatedGotoTaskData)
					iNextPoint = 0
					PRINTLN("PD INSERT GOTO: inserting at end on looped line")
				ENDIF
				
				sFMMCmenu.bInsertingPoint = TRUE
				sFMMCmenu.iCurrentGoToLocation = iNextPoint
				FLOAT fHeight
				IF sFMMCmenu.fCreationHeightIncrease > 0.0
				AND DOES_ENTITY_EXIST(sPedStruct.piTempPed)
					fHeight = GET_ENTITY_HEIGHT_ABOVE_GROUND(sPedStruct.piTempPed)
				ENDIF
				
				INSERT_PED_ASS_OBJ_GOTO_LOCATION(sAssociatedGotoTaskData, sCurrentVarsStruct.vCoronaPos, fHeight)
				PRINTLN("PED ASS OBJ GOTO: Insert along line at point ", iNextPoint)
				
				RETURN TRUE
				
			ELSE
			
				PRINTLN("PED ASS OBJ GOTO: Add new go to")
				
				IF sCurrentVarsStruct.bitsetOkToPlace = BS_CAN_PLACE
					IF sFMMCmenu.bInsertingPoint
						sFMMCmenu.bInsertingPoint = FALSE
					ENDIF
					FLOAT fHeight
					IF sFMMCmenu.fCreationHeightIncrease > 0.0
					AND DOES_ENTITY_EXIST(sPedStruct.piTempPed)
						fHeight = GET_ENTITY_HEIGHT_ABOVE_GROUND(sPedStruct.piTempPed)
					ENDIF
					IF INSERT_PED_ASS_OBJ_GOTO_LOCATION(sAssociatedGotoTaskData, sCurrentVarsStruct.vCoronaPos, fHeight)
						IF sFMMCmenu.iCurrentGoToLocation != -1
							PRINTLN("PD INSERT GOTO: setting inserted goto to menu settings and clearing menu settings")							
							sAssociatedGotoTaskData.sPointData[sFMMCmenu.iCurrentGoToLocation].fPedAchieveFinalHeading = sFMMCmenu.fCurrentGoToHeading
							sFMMCmenu.bExitAtCurrentGoTo = FALSE
							sFMMCmenu.iCurrentGoToLocation = -1
							sFMMCmenu.vCurrentGoToPos = <<0,0,0>>
							sFMMCmenu.fCurrentGoToHeight = 0.0
							sFMMCmenu.iCurrentGoToLocation = -1
						ENDIF
					ENDIF
				ENDIF
				RETURN TRUE
				
			ENDIF
			
		ENDIF
		
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
		
			PRINTLN("PED ASS OBJ GOTO: Cancel pressed")
			
			IF sFMMCmenu.iCurrentGoToLocation != -1
				IF sFMMCmenu.bInsertingPoint
					DELETE_PED_ASS_OBJ_GOTO_LOCATION(sAssociatedGotoTaskData, sFMMCmenu.iCurrentGoToLocation)
					sFMMCmenu.iCurrentGoToLocation = -1
					sFMMCmenu.bInsertingPoint = FALSE
					RETURN TRUE
				ELSE
					SET_ASSOCIATED_GOTO_VECTOR(sAssociatedGotoTaskData, sFMMCmenu.iCurrentGoToLocation, sFMMCmenu.vCurrentGoToPos, sFMMCmenu.fCurrentGoToHeight)
					sFMMCmenu.iCurrentGoToLocation = -1
					sFMMCmenu.vCurrentGoToPos = <<0,0,0>>
					sFMMCmenu.fCurrentGoToHeight = 0.0
					sFMMCmenu.bExitAtCurrentGoTo = FALSE
					sFMMCmenu.bInsertingPoint = FALSE
					RETURN TRUE
				ENDIF
			ENDIF
			
		ENDIF
		
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_Y)
		AND sFMMCMenu.sActiveMenu = eFMMC_ASS_OBJECTIVE_TRIGGER_AREA
			PRINTLN("PED ASS OBJ GOTO: CLEAR pressed")
			INT iGotoLoc = GET_CREATOR_MENU_SELECTION(sFMMCmenu) / 2

			sAssociatedGotoTaskData.sPointData[iGotoLoc].vPlayerActivated_Pos 	= <<0,0,0>>
			sAssociatedGotoTaskData.sPointData[iGotoLoc].iPlayerActivated_Radius = 3
		ENDIF
		
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_X)
		
			INT iClosestWaitLoc = GET_CLOSEST_PED_ASS_OBJ_WAIT_LOCATION(sFMMCmenu, sCurrentVarsStruct.vCoronaPos, 2.5)
			
			PRINTLN("PED ASS OBJ GOTO: Delete pressed || iClosestWaitLoc: ", iClosestWaitLoc)
			
			IF sFMMCMenu.sActiveMenu = eFMMC_ASS_OBJECTIVE_TRIGGER_AREA
			AND iClosestWaitLoc > -1
				sAssociatedGotoTaskData.sPointData[iClosestWaitLoc].vPlayerActivated_Pos = <<0,0,0>>
				REFRESH_MENU(sFMMCMenu)
				
			ELSE
				
				IF iClosestWaitLoc > -1
				AND VDIST2(sAssociatedGotoTaskData.sPointData[iClosestWaitLoc].vPlayerActivated_Pos, sCurrentVarsStruct.vCoronaPos) < sAssociatedGotoTaskData.sPointData[iClosestWaitLoc].iPlayerActivated_Radius*sAssociatedGotoTaskData.sPointData[iClosestWaitLoc].iPlayerActivated_Radius
					sAssociatedGotoTaskData.sPointData[iClosestWaitLoc].vPlayerActivated_Pos = <<0,0,0>>
					sAssociatedGotoTaskData.sPointData[iClosestWaitLoc].iAssociatedRule = 0
					IF sFMMCMenu.sActiveMenu = eFMMC_ASS_OBJECTIVE_TRIGGER_AREA
						SET_UP_ASSOCIATED_RULE_GOTO_TRIGGER_AREA_MENU(sFMMCmenu)
					ENDIF
					
				ELIF sFMMCmenu.iCurrentGoToLocation != -1
				AND sFMMCMenu.sActiveMenu != eFMMC_ASS_OBJECTIVE_TRIGGER_AREA
					DELETE_PED_ASS_OBJ_GOTO_LOCATION(sAssociatedGotoTaskData, sFMMCmenu.iCurrentGoToLocation)
					sFMMCmenu.iCurrentGoToLocation = -1
					sFMMCmenu.bInsertingPoint = FALSE
					
					RETURN TRUE
					
				ELIF sFMMCMenu.sActiveMenu != eFMMC_ASS_OBJECTIVE_TRIGGER_AREA
					INT iClosest = GET_CLOSEST_PED_ASS_OBJ_GOTO_LOCATION(sFMMCmenu, sCurrentVarsStruct.vCoronaPos)
					IF VDIST2(GET_ASSOCIATED_GOTO_TASK_DATA__POSITION(-1, sAssociatedGotoTaskData, iClosest), sCurrentVarsStruct.vCoronaPos) < 4.0
						DELETE_PED_ASS_OBJ_GOTO_LOCATION(sAssociatedGotoTaskData, iClosest)
						RETURN TRUE
					ENDIF
				ENDIF			
				
				INT iGotoLoc = GET_ASSOCIATED_GOTO_TASK_DATA__NUMBER_OF_GOTOS(-1, sAssociatedGotoTaskData)-1 
				
				IF sFMMCMenu.sActiveMenu != eFmmc_ASS_OBJECTIVE_MENU_SET_PED_GOTO_POS
				AND sFMMCMenu.sActiveMenu != eFmmc_ASS_OBJECTIVE_MENU_SET_POOL_GOTO_POS
					iGotoLoc = GET_CREATOR_MENU_SELECTION(sFMMCmenu)				
				ENDIF
				
				IF sFMMCMenu.sActiveMenu = eFMMC_ASS_OBJECTIVE_TRIGGER_AREA 
					iGotoLoc = iGotoLoc / 2
				ENDIF
				
				IF iGotoLoc != -1
				AND sFMMCMenu.sActiveMenu != eFMMC_ASS_OBJECTIVE_TRIGGER_AREA
					PRINTLN("PED ASS OBJ GOTO: DELETING: ", iGotoLoc)
					
					DELETE_PED_ASS_OBJ_GOTO_LOCATION(sAssociatedGotoTaskData, iGotoLoc)
					sFMMCmenu.iCurrentGoToLocation = -1
					sFMMCmenu.bInsertingPoint = FALSE
					
					IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) > 0
						sFMMCmenu.iCurrentSelection--
					ENDIF
					
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC CANCEL_ENTITY_CREATION(INT iTypePassed)

	PRINTLN("CANCEL_ENTITY_CREATION 2 iTypePassed = ", iTypePassed, " sFMMCMenu.iSelectedEntity = ",  sFMMCMenu.iSelectedEntity)
	
	SET_LONG_BIT(sFMMCMenu.iBitActive,PAN_CAM_PLACE)
	IF DOES_BLIP_EXIST(bCameraTriggerBlip)
		REMOVE_BLIP(bCameraTriggerBlip)
	ENDIF
	
	IF sFMMCMenu.iSelectedEntity = -1
	
		INT i
		IF sFMMCMenu.sActiveMenu = eFmmc_VEHICLES_BASE
		OR sFMMCMenu.sActiveMenu = eFmmc_ENEMY_BASE
		OR sFMMCMenu.sActiveMenu = eFmmc_OBJECT_BASE
			sFMMCmenu.vSecondSpawnPosition = <<0,0,0>>
			sFMMCmenu.fSecondSpawnHeading = 0.0
			sFMMCmenu.iSecondSpawnInterior = -1
			
			FOR i = 0 TO FMMC_MAX_RESTART_CHECKPOINTS-1
				sFMMCmenu.vMenuRestartPos[i] = <<0,0,0>>
				sFMMCmenu.fMenuRestartHead[i] = 0.0 	
				sFMMCmenu.iMenuRestartInterior[i] = -1
			ENDFOR
			
			sFMMCmenu.vVehDropOffOverride = <<0,0,0>>
			sFMMCmenu.fVehDropOffRadius			= 0.0
			sFMMCMenu.fVehDropOffVisualRadius	= 0.0
		ENDIF
		
		IF sCurrentVarsStruct.mnThingToPlaceForBudget != DUMMY_MODEL_FOR_SCRIPT
			REMOVE_MODEL_FROM_CREATOR_BUDGET(sCurrentVarsStruct.mnThingToPlaceForBudget)
			sCurrentVarsStruct.mnThingToPlaceForBudget = DUMMY_MODEL_FOR_SCRIPT
		ENDIF
		
		IF sFMMCmenu.iEntityCreation = CREATION_TYPE_GOTO_LOC
			CLEAN_LAST_GO_TO_LOCATION()
			sFMMCmenu.vLocAAVec1 = <<0,0,0>>
		ENDIF
		
		IF iTypePassed = CREATION_TYPE_VEHICLES
			UNLOAD_ALL_VEHICLE_MODELS()
			FOR i = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles - 1)
				CREATE_FMMC_BLIP(sVehStruct.biVehicleBlip[i], g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].vPos, HUD_COLOUR_BLUEDARK, "FMMC_B_10", 1)
			ENDFOR
		ELIF iTypePassed	=  CREATION_TYPE_WEAPONS
			FLOAT fSize
			FOR i = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons - 1)
				IF IS_SPECIAL_WEAPON_BLIP(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].pt)
					fSize = 1
				ELSE
					fSize = 1
				ENDIF
				CREATE_FMMC_BLIP(sWepStruct.biWeaponBlip[i], GET_ENTITY_COORDS(sWepStruct.oiWeapon[i]), HUD_COLOUR_GREEN, "", fSize)
				SET_BLIP_SPRITE(sWepStruct.biWeaponBlip[i] , GET_CORRECT_BLIP_SPRITE_FMMC(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].pt,sWepStruct.iSubType, g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iCustomPickupType))
				IF NOT IS_PICKUP_TYPE_INVALID_NAME(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].pt)
					SET_WEAPON_BLIP_NAME_FROM_PICKUP_TYPE(sWepStruct.biWeaponBlip[i], g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].pt)
				ENDIF
			ENDFOR
			UNLOAD_ALL_FMMC_WEAPON_ASSETS(TRUE, sWepStruct)
		ELIF iTypePassed	=  CREATION_TYPE_PROPS
			IF DOES_ENTITY_EXIST(sCurrentVarsStruct.oiDummyPropForDoorHack)
				DELETE_OBJECT(sCurrentVarsStruct.oiDummyPropForDoorHack)
			ENDIF
			FOR i = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfProps - 1)
				IF DOES_ENTITY_EXIST(sPropStruct.oiObject[i])
					CREATE_FMMC_BLIP(sPropStruct.biObject[i], GET_ENTITY_COORDS(sPropStruct.oiObject[i]), HUD_COLOUR_PURPLE, "FMMC_B_5", 1)
				ENDIF
			ENDFOR
			UNLOAD_ALL_PROP_MODELS(sFMMCmenu)
		ELIF iTypePassed	=  CREATION_TYPE_DYNOPROPS
			FOR i = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps - 1)
				CREATE_FMMC_BLIP(sDynoPropStruct.biObject[i], GET_ENTITY_COORDS(sDynoPropStruct.oiObject[i]), HUD_COLOUR_NET_PLAYER2, "FMMC_B_13", 1)
			ENDFOR
			UNLOAD_ALL_PROP_MODELS(sFMMCmenu)
		ELIF iTypePassed	=  CREATION_TYPE_TRIGGER
			IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.vStartPos)
				CREATE_FMMC_BLIP(sStartEndBlips.biStart, g_FMMC_STRUCT.vStartPos, HUD_COLOUR_WHITE, "FMMC_B_4", 1)		
				BLIP_SPRITE blip = GET_TRIGGER_RADAR_BLIP() 
				IF blip != RADAR_TRACE_INVALID
					SET_BLIP_SPRITE(sStartEndBlips.biStart, blip)
					SET_BLIP_NAME_FROM_TEXT_FILE(sStartEndBlips.biStart, "FMMC_B_4")	
				ENDIF
			ENDIF
		ELIF iTypePassed	=  CREATION_TYPE_APARTMENT_WARP
			IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.vStartPos)
				CREATE_FMMC_BLIP(sStartEndBlips.biStart, g_FMMC_STRUCT.vStartPos, HUD_COLOUR_WHITE, "FMMC_B_4", 1)		
				BLIP_SPRITE blip = GET_TRIGGER_RADAR_BLIP() 
				IF blip != RADAR_TRACE_INVALID
					SET_BLIP_SPRITE(sStartEndBlips.biStart, blip)
					SET_BLIP_NAME_FROM_TEXT_FILE(sStartEndBlips.biStart, "FMMC_B_4")	
				ENDIF
			ENDIF
		ELIF iTypePassed = CREATION_TYPE_DOOR
			IF IS_FAKE_DOOR(sFMMCMenu.sCurrentDoor.mnDoorModel)
				IF DOES_ENTITY_EXIST(sCurrentVarsStruct.oiCurrentDoor)
					SET_ENTITY_ROTATION(sCurrentVarsStruct.oiCurrentDoor, <<0,0,sFMMCMenu.sCurrentDoor.fOriginalRotation>>)
					SET_ENTITY_AS_MISSION_ENTITY(sCurrentVarsStruct.oiCurrentDoor, TRUE, TRUE)
				ENDIF
			ELSE
				IF NOT IS_IPL_DOOR(sFMMCMenu.sCurrentDoor.mnDoorModel)
				AND IS_DOOR_REGISTERED_WITH_SYSTEM(sCurrentVarsStruct.iCurrentDoorHash)
					DOOR_SYSTEM_SET_OPEN_RATIO(sCurrentVarsStruct.iCurrentDoorHash, 0.0, FALSE,FALSE)
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(sCurrentVarsStruct.oiCurrentDoor)
				sCurrentVarsStruct.oiCurrentDoor = NULL
			ENDIF
			
			sFMMCMenu.iHighlightedEntity = -1
		ELIF iTypePassed 	= CREATION_TYPE_DIALOGUE_TRIGGER
		
		ELIF iTypePassed 	= CREATION_TYPE_TRAINS
			IF DOES_ENTITY_EXIST(sTrainCreationStruct.viCoronaTrain)
				DELETE_MISSION_TRAIN(sTrainCreationStruct.viCoronaTrain)
				PRINTLN("[Trains] CANCEL_ENTITY_CREATION | Deleting sTrainCreationStruct.viCoronaTrain")
			ENDIF
			
		ELIF iTypePassed	=  CREATION_TYPE_INTERACTABLE
			IF DOES_ENTITY_EXIST(sInteractableCreationStruct.oiCoronaInteractable)
				SET_MODEL_AS_NO_LONGER_NEEDED(GET_ENTITY_MODEL(sInteractableCreationStruct.oiCoronaInteractable))
				DELETE_OBJECT(sInteractableCreationStruct.oiCoronaInteractable)
				PRINTLN("[Interactables] CANCEL_ENTITY_CREATION | Deleting sInteractableCreationStruct.oiCoronaInteractable")
			ENDIF
			
		ELIF iTypePassed	=  CREATION_TYPE_DEV_ASSIST
			IF DOES_ENTITY_EXIST(sDevAssistCreationStruct.oiCorona)
				SET_MODEL_AS_NO_LONGER_NEEDED(GET_ENTITY_MODEL(sDevAssistCreationStruct.oiCorona))
				DELETE_OBJECT(sDevAssistCreationStruct.oiCorona)
				PRINTLN("[DevAssist] CANCEL_ENTITY_CREATION | Deleting sDevAssistCreationStruct.oiCorona")
			ENDIF 
		
		ELIF iTypePassed	=  CREATION_TYPE_OBJECTS
			UNLOAD_ALL_OBJ_MODELS(sObjStruct)
		ELIF iTypePassed = CREATION_TYPE_WORLD_PROPS
			CLEAR_WORLD_PROP_DATA(sFMMCmenu.sCurrentWorldProp)
		ENDIF
		
		sPedStruct.iSwitchingINT 						= CREATION_STAGE_WAIT
		sVehStruct.iSwitchingINT 						= CREATION_STAGE_WAIT
		sObjStruct.iSwitchingINT 						= CREATION_STAGE_WAIT
		sInteractableCreationStruct.iCreationState 		= CREATION_STAGE_WAIT
		sWepStruct.iSwitchingINT 						= CREATION_STAGE_WAIT
		SET_CURRENT_GENERIC_CREATION_STAGE(CREATION_STAGE_WAIT)
		sCurrentVarsStruct.iZoneCreationStage 			= CREATION_STAGE_WAIT
		sCoverStruct.iSwitchingINT						= CREATION_STAGE_WAIT
		iTriggerCreationStage							= CREATION_STAGE_WAIT
		sPlayerKills.iSwitch							= 0
		
		IF IS_MENU_A_RESET_MENU(sFMMCmenu.sActiveMenu)
		OR IS_MENU_A_RESET_MENU(sFMMCmenu.sMenuBack)
			IF sFMMCMenu.sActiveMenu != eFmmc_GO_TO_CONSEQUENCES
			AND sFMMCMenu.sActiveMenu != eFmmc_VEH_WARP_LOCATIONS
			AND sFMMCMenu.sActiveMenu != eFmmc_PED_WARP_LOCATIONS
			AND sFMMCMenu.sActiveMenu != eFmmc_OBJ_WARP_LOCATIONS
			AND sFMMCMenu.sActiveMenu != eFmmc_LOC_WARP_LOCATIONS			
			AND sFMMCMenu.sActiveMenu != eFmmc_INTERACTABLE_WARP_LOCATIONS
			AND sFMMCmenu.sActiveMenu != eFmmc_WARP_LOCATION_SETTINGS
			AND sFMMCmenu.sActiveMenu != eFmmc_ENTITY_CLEAR_DATA
				PRINTLN("Setting sFMMCmenu.iEntityCreation =  -1 loc3")
				SET_CREATION_TYPE(sFMMCMenu, CREATION_TYPE_NONE)
			ENDIF
		ENDIF
		
		sCurrentVarsStruct.fCheckPointSize 				= ciDEFAULT_CHECK_POINT_SIZE
		sHCS.hcCurrentCoronaColour 						= sHCS.hcDefaultCoronaColour
		
		sFMMCmenu.iCustomFailName						= -1
		REPEAT FMMC_MAX_TEAMS i 
			sFMMCmenu.iMissionCriticalRemovalRule[i] = 0
		ENDREPEAT
		sFMMCmenu.iMissionCriticalLinkedVeh = -1
		
		IF sFMMCMenu.sActiveMenu != eFMMC_GENERIC_OVERRIDE_POSITION
		AND sFMMCMenu.sActiveMenu != eFmmc_GENERIC_OVERRIDE_ROTATION
		AND sFMMCMenu.sActiveMenu != eFmmc_PROP_ADVANCED_OPTIONS
		AND sFMMCMenu.sActiveMenu != eFmmc_SNAPPING_OPTIONS
		AND sFMMCMenu.sActiveMenu != eFMMC_OFFSET_POSITION
		AND sFMMCMenu.sActiveMenu != eFMMC_OFFSET_ROTATION
			CLEAR_BIT(sFMMCmenu.iObjMenuBitset, cibsOBJ_Rotation_Override)
			CLEAR_BIT(sFMMCmenu.iObjMenuBitset, cibsOBJ_Position_Override)
		ENDIF
		
		IF sFMMCMenu.sActiveMenu != eFMMC_DUMMYBLIP_OVERRIDE_POSITION
			CLEAR_BIT(g_FMMC_STRUCT.sCurrentDummyBlip.iBitSet, ciDUMMY_BLIP_POSITION_OVERRIDE)	
		ENDIF
		
		IF sFMMCMenu.sActiveMenu = eFmmc_PROP_BASE
		OR sFMMCMenu.sActiveMenu = eFmmc_DYNOPROP_BASE
			CLEAR_BIT(sFMMCmenu.iPropBitset, ciFMMC_PROP_Rotation_Override)
			CLEAR_BIT(sFMMCmenu.iPropBitset, ciFMMC_PROP_Position_Override)
			
			CLEAR_BIT(sFMMCmenu.iDynoPropBitset, ciFMMC_DYNOPROP_Position_Override)
			CLEAR_BIT(sFMMCmenu.iDynoPropBitset, ciFMMC_DYNOPROP_Rotation_Override)
		ENDIF
		
		IF sFMMCMenu.sActiveMenu != eFMMC_GENERIC_OVERRIDE_POSITION
		AND sFMMCMenu.sActiveMenu != eFmmc_GENERIC_OVERRIDE_ROTATION
			IF CURRENTLY_USING_FMMC_OVERRIDE_POSITION(sFMMCmenu)
				PRINTLN("Didn't set vOverridePosition or vOverrideRotation to 0,0,0 because the user is still overriding the position of this entity")
			ELSE
				SET_OVERRIDE_POSITION_COORDS(sFMMCmenu, <<0,0,0>>)
				SET_OVERRIDE_ROTATION_VECTOR(sFMMCmenu, <<0,0,0>>)
			ENDIF
		ENDIF
		
		IF sStuntJumpStruct.iSwitchingINT != CREATION_STAGE_WAIT
			IF IS_VECTOR_ZERO(g_FMMC_STRUCT.sStuntJumps[g_FMMC_STRUCT.iNumberOfSj].vCameraPos)
				g_FMMC_STRUCT.sStuntJumps[g_FMMC_STRUCT.iNumberOfSj].vStartPos = <<0,0,0>>
				g_FMMC_STRUCT.sStuntJumps[g_FMMC_STRUCT.iNumberOfSj].vEndPos = <<0,0,0>>
			ENDIF
			sStuntJumpStruct.iSwitchingINT 					= CREATION_STAGE_WAIT
		ENDIF
		
		sFMMCmenu.iCustomFailName = -1
		sFMMCmenu.iExplodeTime = - 1
		
		REMOVE_TEMP_ENTITES_IF_NOT_NEEDED(sFMMCmenu.iEntityCreation)
		
	ELSE
	
		PLAY_SOUND_FRONTEND(-1, "EDIT", GET_CREATOR_SPECIFIC_SOUND_SET())
		
		IF iTypePassed	= CREATION_TYPE_PEDS
		
			IF IS_MENU_A_RESET_MENU(sFMMCmenu.sMenuBack)
			
				PRINTLN("TypePassed	= CREATION_TYPE_PEDS")
				PRINTLN("sFMMCMenu.iSelectedEntity = ", sFMMCMenu.iSelectedEntity)	
				
				IF DOES_ENTITY_EXIST(sPedStruct.piPed[sFMMCMenu.iSelectedEntity])	
					DELETE_PED(sPedStruct.piPed[sFMMCMenu.iSelectedEntity])
				ENDIF
				g_FMMC_STRUCT_ENTITIES.sPlacedPed[sFMMCMenu.iSelectedEntity] = g_FMMC_STRUCT_ENTITIES.sEditedPed	
				
				INT iTeam
				FOR iTeam = 0 TO FMMC_MAX_TEAMS - 1
					IF sPedStruct.bSavedMissionCriticalValues[iTeam]
						SET_BIT(g_FMMC_STRUCT.iMissionCriticalPedDoubleBS[iTeam][GET_LONG_BITSET_INDEX(sFMMCMenu.iSelectedEntity)], GET_LONG_BITSET_BIT(sFMMCMenu.iSelectedEntity))
					ELSE
						CLEAR_BIT(g_FMMC_STRUCT.iMissionCriticalPedDoubleBS[iTeam][GET_LONG_BITSET_INDEX(sFMMCMenu.iSelectedEntity)], GET_LONG_BITSET_BIT(sFMMCMenu.iSelectedEntity))
					ENDIF
				ENDFOR
				
				IF CREATE_PED_FMMC(sPedStruct.piPed[sFMMCMenu.iSelectedEntity], sFMMCMenu.iSelectedEntity, DEFAULT, FALSE)
					PRINTLN("CANCEL_ENTITY_CREATION - new ped created at vPos: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[sFMMCMenu.iSelectedEntity].vPos)
					
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sFMMCMenu.iSelectedEntity].sPedVariationSettings.iPedVariationBS, ciPV_USING_CUSTOM_VARIATION)
						FMMC_SET_PED_CUSTOM_VARIATION(sPedStruct.piPed[sFMMCMenu.iSelectedEntity], g_FMMC_STRUCT_ENTITIES.sPlacedPed[sFMMCMenu.iSelectedEntity].sPedVariationSettings)
					ELSE
						FMMC_SET_PED_VARIATION(sPedStruct.piPed[sFMMCMenu.iSelectedEntity], g_FMMC_STRUCT_ENTITIES.sPlacedPed[sFMMCMenu.iSelectedEntity].mn, g_FMMC_STRUCT_ENTITIES.sPlacedPed[sFMMCMenu.iSelectedEntity].iModelVariation)
					ENDIF
										
					SET_ENTITY_COLLISION(sPedStruct.piPed[sFMMCMenu.iSelectedEntity] , TRUE)					
					CREATE_FMMC_BLIP(sPedStruct.biPedBlip[sFMMCMenu.iSelectedEntity], GET_ENTITY_COORDS(sPedStruct.piPed[sFMMCMenu.iSelectedEntity]), HUD_COLOUR_RED, "FMMC_B_9", 1)
					SET_BLIP_SPRITE(sPedStruct.biPedBlip[sFMMCMenu.iSelectedEntity], RADAR_TRACE_AI)
										
					IF IS_ROCKSTAR_DEV()
						FLOAT fGround
						IF GET_GROUND_Z_FOR_3D_COORD(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sFMMCMenu.iSelectedEntity].vPos, fGround)
							IF fGround > 2.5
								FREEZE_ENTITY_POSITION(sPedStruct.piPed[sFMMCMenu.iSelectedEntity], TRUE)
								PRINTLN("CANCEL_ENTITY_CREATION - Freezing Ped in the air.")
							ELSE
								PRINTLN("CANCEL_ENTITY_CREATION - Ped is only slightly in the air, allowing to fall to ground: ", fGround)
							ENDIF
						ELSE
							PRINTLN("CANCEL_ENTITY_CREATION - Ped was not in the air.")
						ENDIF
					ENDIF
					
					sPedStruct.iSwitchingINT 				= CREATION_STAGE_WAIT
				ENDIF
				
				IF g_FMMC_STRUCT_ENTITIES.iNumberOfPeds >= FMMC_MAX_PEDS
					IF DOES_ENTITY_EXIST(sPedStruct.piTempPed)
						DELETE_PED(sPedStruct.piTempPed)
					ENDIF
				ENDIF
				
				SET_MODEL_AS_NO_LONGER_NEEDED(g_FMMC_STRUCT_ENTITIES.sEditedPed.mn)
				
				sFMMCmenu.iCutSceneRespawnScene = -1
				sFMMCmenu.iCutSceneRespawnShot 	= 0
				sFMMCmenu.iCutSceneTaskActivationScene = -1
				sFMMCmenu.iCutSceneTaskActivationShot = -1
				sFMMCmenu.iCustomFailName		= -1
				sFMMCmenu.iAssociatedAction 	= ciACTION_NOTHING
				sFMMCmenu.iPedEscortRange 		= 0
				sFMMCmenu.iCleanupObjective 	= -1
				sFMMCmenu.iCleanupTeam 			= -1
				sFMMCmenu.iDriveSpeed 			= -1
				
				CLEAR_ALL_PED_ASSOCIATED_GOTO_DATA(sFMMCmenu.sAssociatedGotoTaskData)
			
			ENDIF
			
		ELIF iTypePassed	= CREATION_TYPE_TEAM_SPAWN_LOCATION
			PRINTLN("TypePassed	= CREATION_TYPE_TEAM_SPAWN_LOCATION")
			PRINTLN("sFMMCMenu.iSelectedEntity = ", sFMMCMenu.iSelectedEntity)	
			g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[g_FMMC_STRUCT_ENTITIES.sEditedSpawnPoint.iTeam][sFMMCMenu.iSelectedEntity] = g_FMMC_STRUCT_ENTITIES.sEditedSpawnPoint
			
			PRINTLN("g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[sFMMCMenu.iSelectedEntity].iTeam  = ", g_FMMC_STRUCT_ENTITIES.sEditedSpawnPoint.iTeam)
			
			CREATE_FMMC_BLIP(sTeamSpawnStruct[g_FMMC_STRUCT_ENTITIES.sEditedSpawnPoint.iTeam].biPedBlip[sFMMCMenu.iSelectedEntity], g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[g_FMMC_STRUCT_ENTITIES.sEditedSpawnPoint.iTeam][sFMMCMenu.iSelectedEntity].vPos, HUD_COLOUR_WHITE, getNameForTeamSpawnBlip(g_FMMC_STRUCT_ENTITIES.sEditedSpawnPoint.iTeam), 1)
			SET_BLIP_COLOUR(sTeamSpawnStruct[g_FMMC_STRUCT_ENTITIES.sEditedSpawnPoint.iTeam].biPedBlip[sFMMCMenu.iSelectedEntity] , getColourForSpawnBlip(g_FMMC_STRUCT_ENTITIES.sEditedSpawnPoint.iTeam))

			sTeamSpawnStruct[g_FMMC_STRUCT_ENTITIES.sEditedSpawnPoint.iTeam].iSwitchingINT = CREATION_STAGE_WAIT
		ELIF iTypePassed	=  CREATION_TYPE_VEHICLES		
			IF IS_MENU_A_RESET_MENU(sFMMCmenu.sMenuBack)
			
				PRINTLN("IF iTypePassed	= CREATION_TYPE_VEHICLES")
				PRINTLN("sFMMCMenu.iSelectedEntity = ", sFMMCMenu.iSelectedEntity)	
				INT iTeam
				FOR iTeam = 0 TO FMMC_MAX_TEAMS - 1
					IF sVehStruct.bSavedMissionCriticalValues[iTeam]
						SET_BIT(g_FMMC_STRUCT.iMissionCriticalVehBS[iTeam], sFMMCMenu.iSelectedEntity)
					ELSE
						CLEAR_BIT(g_FMMC_STRUCT.iMissionCriticalVehBS[iTeam], sFMMCMenu.iSelectedEntity)
					ENDIF
				ENDFOR
				sFMMCmenu.vSecondSpawnPosition = <<0,0,0>>
				sFMMCmenu.fSecondSpawnHeading = 0.0
				sFMMCmenu.iSecondSpawnInterior = -1
				sFMMCmenu.iCleanupObjective = -1
				sFMMCmenu.iCleanupTeam = -1
				INT i
				FOR i = 0 TO FMMC_MAX_RESTART_CHECKPOINTS-1
					sFMMCmenu.vMenuRestartPos[i] = <<0,0,0>>
					sFMMCmenu.fMenuRestartHead[i] = 0.0 	
					sFMMCmenu.iMenuRestartInterior[i] = -1
				ENDFOR
				sFMMCmenu.vVehDropOffOverride = <<0,0,0>>
				sFMMCmenu.fVehDropOffRadius			= 0.0
				sFMMCMenu.fVehDropOffVisualRadius	= 0.0
				
				sFMMCmenu.iCustomFailName	= -1
				CANCEL_FROM_EDITING_VEHICLE(sVehStruct, sFMMCMenu.iSelectedEntity)
				IF sFMMCMenu.sActiveMenu = eFmmc_MISSION_CRITICAL_MENU
					SET_UP_MISSION_CRITICAL_OPTIONS_MENU(sFMMCmenu)
				ENDIF
				sVehStruct.iSwitchingINT = CREATION_STAGE_WAIT
				sFMMCmenu.iCutSceneRespawnScene = -1
				sFMMCmenu.iCutSceneRespawnShot = 0
			ENDIF
		ELIF iTypePassed	=  CREATION_TYPE_WEAPONS
			PRINTLN("IF iTypePassed	= CREATION_TYPE_WEAPONS")
			PRINTLN("sFMMCMenu.iSelectedEntity = ", sFMMCMenu.iSelectedEntity)	
			CANCEL_FROM_EDITING_WEAPON(sFMMCmenu, sWepStruct, sFMMCMenu.iSelectedEntity, TRUE, sCurrentVarsStruct)
			sWepStruct.iSwitchingINT 				= CREATION_STAGE_WAIT
		ELIF iTypePassed	=  CREATION_TYPE_OBJECTS
			IF IS_MENU_A_RESET_MENU(sFMMCmenu.sMenuBack)
			
				PRINTLN("IF iTypePassed	= CREATION_TYPE_OBJECTS")
				PRINTLN("sFMMCMenu.iSelectedEntity = ", sFMMCMenu.iSelectedEntity)	
				sFMMCmenu.iCustomFailName	= -1
				sFMMCmenu.iCleanupObjective 	= -1
				sFMMCmenu.iCleanupTeam 			= -1
				sFMMCmenu.vVehDropOffOverride = <<0,0,0>>
				sFMMCmenu.fVehDropOffRadius			= 0.0
				sFMMCMenu.fVehDropOffVisualRadius	= 0.0
				CANCEL_FROM_EDITING_OBJECT(sObjStruct, sFMMCMenu.iSelectedEntity, sCurrentVarsStruct)
				sObjStruct.iSwitchingINT 				= CREATION_STAGE_WAIT
			ENDIF
			IF sFMMCmenu.iEntityCreation = CREATION_TYPE_OBJECTS
			AND sFMMCMenu.sActiveMenu = eFmmc_OBJECT_BASE
				SET_UP_OBJECT_BASE_MENU(sFMMCmenu)
			ENDIF
		ELIF iTypePassed	=  CREATION_TYPE_PROPS
			IF IS_MENU_A_RESET_MENU(sFMMCmenu.sMenuBack)
			
				sFMMCmenu.iCleanupObjective = -1
				sFMMCmenu.iCleanupTeam = -1
				PRINTLN("IF iTypePassed	= CREATION_TYPE_PROPS")
				PRINTLN("sFMMCMenu.iSelectedEntity = ", sFMMCMenu.iSelectedEntity)	
				CANCEL_FROM_EDITING_PROP(sPropStruct, sFMMCMenu.iSelectedEntity)
				SET_CURRENT_GENERIC_CREATION_STAGE(CREATION_STAGE_WAIT)
				IF DOES_ENTITY_EXIST(sCurrentVarsStruct.oiDummyPropForDoorHack)
					DELETE_OBJECT(sCurrentVarsStruct.oiDummyPropForDoorHack)
				ENDIF
			ENDIF
		ELIF iTypePassed	=  CREATION_TYPE_DYNOPROPS
			IF IS_MENU_A_RESET_MENU(sFMMCmenu.sMenuBack)
			
				sFMMCmenu.iCleanupObjective = -1
				sFMMCmenu.iCleanupTeam = -1
				PRINTLN("IF iTypePassed	= CREATION_TYPE_DYNOPROPS")
				PRINTLN("sFMMCMenu.iSelectedEntity = ", sFMMCMenu.iSelectedEntity)	
				CANCEL_FROM_EDITING_DYNOPROP(sPropStruct, sDynoPropStruct, sFMMCMenu.iSelectedEntity)
				SET_CURRENT_GENERIC_CREATION_STAGE(CREATION_STAGE_WAIT)
			ENDIF
			
		ELIF iTypePassed =  CREATION_TYPE_INTERACTABLE
			IF IS_MENU_A_RESET_MENU(sFMMCmenu.sMenuBack)
				CANCEL_FROM_EDITING_INTERACTABLE(sInteractableCreationStruct, sFMMCMenu.iSelectedEntity)
				SET_CURRENT_GENERIC_CREATION_STAGE(CREATION_STAGE_WAIT)
			ENDIF
			
		ELIF iTypePassed =  CREATION_TYPE_DEV_ASSIST
			IF IS_MENU_A_RESET_MENU(sFMMCmenu.sMenuBack)
				CANCEL_FROM_EDITING_DEV_ASSIST(sDevAssistCreationStruct, sFMMCMenu.iSelectedEntity)
				SET_CURRENT_GENERIC_CREATION_STAGE(CREATION_STAGE_WAIT)
			ENDIF
		
		ELIF iTypePassed	=  CREATION_TYPE_GOTO_LOC
			IF IS_MENU_A_RESET_MENU(sFMMCmenu.sMenuBack)
			
				PRINTLN("IF iTypePassed	= CREATION_TYPE_GOTO_LOC")
				PRINTLN("sFMMCMenu.iSelectedEntity = ", sFMMCMenu.iSelectedEntity)
				CANCEL_FROM_EDITING_GOTO(sLocStruct, sFMMCMenu.iSelectedEntity)
			
				IF DOES_BLIP_EXIST(sLocStruct.biGoToBlipCaptureRadius[sFMMCMenu.iSelectedEntity])
					REMOVE_BLIP(sLocStruct.biGoToBlipCaptureRadius[sFMMCMenu.iSelectedEntity])
				ENDIF
				SET_CURRENT_GENERIC_CREATION_STAGE(CREATION_STAGE_WAIT)
			ENDIF
		ELIF iTypePassed = CREATION_TYPE_DIALOGUE_TRIGGER
			IF IS_MENU_A_RESET_MENU(sFMMCmenu.sMenuBack)
			
				CANCEL_FROM_EDITING_DIALOGUE_TRIGGER(sFMMCmenu, sFMMCMenu.iSelectedEntity)
				SET_CURRENT_GENERIC_CREATION_STAGE(CREATION_STAGE_WAIT)
			ENDIF
		ELIF iTypePassed = CREATION_TYPE_DUMMY_BLIPS
			IF IS_MENU_A_RESET_MENU(sFMMCmenu.sMenuBack)
				sFMMCmenu.bEntityGrabbedForEdit = FALSE
				SET_CURRENT_GENERIC_CREATION_STAGE(CREATION_STAGE_WAIT)
			ENDIF
		ELIF iTypePassed = CREATION_TYPE_DOOR
			RESET_DOOR_TO_WORLD_STATE(sFMMCmenu.iSelectedEntity, sCurrentVarsStruct)
			IF DOES_ENTITY_EXIST(sCurrentVarsStruct.oiCurrentDoor )
				SET_OBJECT_AS_NO_LONGER_NEEDED(sCurrentVarsStruct.oiCurrentDoor)
				sCurrentVarsStruct.oiCurrentDoor = NULL
			ENDIF
			SET_CURRENT_GENERIC_CREATION_STAGE(WORLD_SELECTION_STAGE_INIT)
			
		ELIF iTypePassed = CREATION_TYPE_ROCKET_POSITIONS
			IF IS_MENU_A_RESET_MENU(sFMMCmenu.sMenuBack)
				sFMMCmenu.iRocketPos_ZoneBS = 0
				sFMMCmenu.iRocketPos_LinkedObj = -1
				sFMMCmenu.fRocketPos_Accuracy = 100.0
				sFMMCmenu.bRocketPos_NotHoming = FALSE
				sFMMCmenu.iRocketPos_AccuracyVeh = -1
				sFMMCmenu.bEntityGrabbedForEdit = FALSE
				SET_CURRENT_GENERIC_CREATION_STAGE(CREATION_STAGE_WAIT)
			ENDIF
			
		ELIF iTypePassed = CREATION_TYPE_QRC_SPAWN_POINTS
			INT ii
			sFMMCMenu.iSpawnGroup = 0
			FOR ii = 0 TO ciMAX_NUM_FMMC_ENTITY_SUB_SPAWN_GROUPS-1
				sFMMCMEnu.iSubSpawnGroupBS[ii] = 0
			ENDFOR
			sFMMCMenu.iQRCSpawnPoint_SpawnPointBS = 0
			sFMMCMenu.iQRCSpawnPoint_ActiveForTeam = 0
			sFMMCMenu.iQRCSpawnPoint_ActiveOnCheckpointBS = 0
			sFMMCMenu.iQRCSpawnPoint_ActiveWithSpawnGroupBS = 0
			sFMMCmenu.bEntityGrabbedForEdit = FALSE
			SET_CURRENT_GENERIC_CREATION_STAGE(CREATION_STAGE_WAIT)
		ELIF iTypePassed = CREATION_TYPE_WORLD_PROPS
			CLEAR_WORLD_PROP_DATA(sFMMCmenu.sCurrentWorldProp)
			sCoverStruct.iSwitchingINT = CREATION_STAGE_WAIT
		ENDIF
		
		REFRESH_MENU(sFMMCmenu)
		
		SET_BIT(sCurrentVarsStruct.iUpdateRule, ciENTITY_REMOVED_CHECK_RULES)
	ENDIF
	
	IF IS_MENU_A_RESET_MENU(sFMMCmenu.sMenuBack)
		sFMMCMenu.iContinuityId = -1
		CLEAR_ALL_FMMC_ENTITY_OVERRIDES(sFMMCmenu)
	ENDIF
	
ENDPROC

PROC DELETE_BLIP_ARRAY_FOR_TEST(BLIP_INDEX& blipArray[], INT iLength)
	INT i
	
	FOR i = 0 TO iLength - 1
		IF DOES_BLIP_EXIST(blipArray[i])
			REMOVE_BLIP(blipArray[i])
		ENDIF
	ENDFOR
ENDPROC
PROC DELETE_ALL_BLIPS_FOR_TEST()
	
	PRINTLN("DELETE_ALL_BLIPS_FOR_TEST")
	
	DELETE_BLIP_ARRAY_FOR_TEST(sTeamSpawnStruct[0].biPedBlip, FMMC_MAX_TEAMSPAWNPOINTS)
	DELETE_BLIP_ARRAY_FOR_TEST(sTeamSpawnStruct[1].biPedBlip, FMMC_MAX_TEAMSPAWNPOINTS)
	DELETE_BLIP_ARRAY_FOR_TEST(sTeamSpawnStruct[2].biPedBlip, FMMC_MAX_TEAMSPAWNPOINTS)
	DELETE_BLIP_ARRAY_FOR_TEST(sTeamSpawnStruct[3].biPedBlip, FMMC_MAX_TEAMSPAWNPOINTS)
	
	DELETE_BLIP_ARRAY_FOR_TEST(biDoors, FMMC_MAX_NUM_DOORS)
	DELETE_BLIP_ARRAY_FOR_TEST(biElevators, FMMC_MAX_ELEVATORS)
	
	DELETE_BLIP_ARRAY_FOR_TEST(sPedStruct.biPedBlip, FMMC_MAX_PEDS)
	DELETE_BLIP_ARRAY_FOR_TEST(sVehStruct.biVehicleBlip, FMMC_MAX_VEHICLES)
	DELETE_BLIP_ARRAY_FOR_TEST(sPropStruct.biObject, GET_FMMC_MAX_NUM_PROPS())
	DELETE_BLIP_ARRAY_FOR_TEST(sObjStruct.biObject, FMMC_MAX_NUM_OBJECTS)
	DELETE_BLIP_ARRAY_FOR_TEST(sInteractableCreationStruct.biInteractableBlips, FMMC_MAX_NUM_INTERACTABLES)
	DELETE_BLIP_ARRAY_FOR_TEST(sVehStruct.biVehicleBlip, FMMC_MAX_VEHICLES)
	DELETE_BLIP_ARRAY_FOR_TEST(sLocStruct.biGoToBlips, FMMC_MAX_GO_TO_LOCATIONS)
	
	INT i
	FOR i = 0 TO FMMC_MAX_DIALOGUES - 1
		IF DOES_BLIP_EXIST(g_FMMC_STRUCT.sDialogueTriggers[i].biBlip)
			REMOVE_BLIP(g_FMMC_STRUCT.sDialogueTriggers[i].biBlip)
		ENDIF
	ENDFOR
ENDPROC

#IF IS_DEBUG_BUILD
PROC DRAW_MENU_ENTRY_MODIFIER_DEBUG()

	IF NOT IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD6)
		EXIT
	ENDIF
	
	INT iLayer = 0
	
	FOR iLayer = 0 TO MENU_MAX_DEPTH - 1
		TEXT_LABEL_63 tlDebugText
		tlDebugText = "sFMMCmenu.eMenuEntryModifiers["
		tlDebugText += iLayer
		tlDebugText += "]: "
		tlDebugText += ENUM_TO_INT(sFMMCmenu.eMenuEntryModifiers[iLayer])
		
		FLOAT fYOffset = TO_FLOAT(iLayer) * 0.03
		DRAW_DEBUG_TEXT_2D(tlDebugText, <<0.45, 0.25 + fYOffset, 0.5>>)
	ENDFOR
ENDPROC

PROC DRAW_CREATOR_MENU_DEBUG()
	IF NOT IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD5)
		EXIT
	ENDIF
	
	INT iLayer = 0
	FOR iLayer = 0 TO MENU_MAX_DEPTH - 1
		TEXT_LABEL_63 tlDebugText
		tlDebugText = "sFMMCmenu.iReturnSelections["
		tlDebugText += iLayer
		tlDebugText += "]: "
		tlDebugText += sFMMCmenu.iReturnSelections[iLayer]
		
		FLOAT fYOffset = TO_FLOAT(iLayer) * 0.0225
		
		IF iLayer <= sFMMCmenu.iCurrentDepth
			DRAW_DEBUG_TEXT_2D(tlDebugText, <<0.7, 0.15 + fYOffset, 0.5>>, 255, 255, 255)
		ELSE
			DRAW_DEBUG_TEXT_2D(tlDebugText, <<0.7, 0.15 + fYOffset, 0.5>>, 0, 0, 0, 200)
		ENDIF
	ENDFOR
	
	INT iMenuOption = 0
	FOR iMenuOption = 0 TO sFMMCmenu.iCurrentMenuLength - 1
		TEXT_LABEL_63 tlDebugText
		tlDebugText = "sFMMCmenu.sMenuAction["
		tlDebugText += iMenuOption
		tlDebugText += "]: "
		tlDebugText += ENUM_TO_INT(sFMMCmenu.sMenuAction[iMenuOption])
		
		FLOAT fYOffset = TO_FLOAT(iMenuOption) * 0.0225
		
		IF iMenuOption = GET_CREATOR_MENU_SELECTION(sFMMCMenu)
			DRAW_DEBUG_TEXT_2D(tlDebugText, <<0.3, 0.15 + fYOffset, 0.5>>, 255, 255, 255)
		ELSE
			DRAW_DEBUG_TEXT_2D(tlDebugText, <<0.3, 0.15 + fYOffset, 0.5>>, 0, 0, 0, 200)
		ENDIF
	ENDFOR
	
	TEXT_LABEL_63 tlDebugText = "sFMMCMenu.iCurrentMenuSection: "
	tlDebugText += sFMMCMenu.iCurrentMenuSection
	DRAW_DEBUG_TEXT_2D(tlDebugText, <<0.6, 0.8, 0.5>>)
	
	tlDebugText = "sFMMCMenu.sMenuBack: "
	tlDebugText += ENUM_TO_INT(sFMMCMenu.sMenuBack)
	DRAW_DEBUG_TEXT_2D(tlDebugText, <<0.6, 0.775, 0.5>>)
	
	tlDebugText = "sFMMCMenu.iEntityCreation: "
	tlDebugText += sFMMCMenu.iEntityCreation
	DRAW_DEBUG_TEXT_2D(tlDebugText, <<0.6, 0.75, 0.5>>)
	
	tlDebugText = "sFMMCmenu.iSelectedEntity: "
	tlDebugText += sFMMCmenu.iSelectedEntity
	DRAW_DEBUG_TEXT_2D(tlDebugText, <<0.6, 0.725, 0.5>>)
	
ENDPROC

PROC PROCESS_FORCE_EDIT()
	IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_E)	OR IS_GAME_KEYBOARD_KEY_JUST_PRESSED(KEY_E))
	AND (IS_KEYBOARD_KEY_PRESSED(KEY_LCONTROL) OR IS_GAME_KEYBOARD_KEY_PRESSED(KEY_LCONTROL))
	OR IS_DEBUG_KEY_PRESSED(KEY_E, KEYBOARD_MODIFIER_CTRL, "Activate the state where you can type in a value to be passed to the menu")
		sCurrentVarsStruct.iMenuState = MENU_STATE_ENTER_FORCE_EDIT_STRING
		g_bFMMCForceEditEnteringString = TRUE
		bDelayAFrame = TRUE
		PRINTLN("[CreatorMenu_FMMCForceEdit] PROCESS_FORCE_EDIT | Setting iMenuState to MENU_STATE_ENTER_FORCE_EDIT_STRING")
	ENDIF
ENDPROC
#ENDIF

PROC PRE_FRAME_CREATOR_DEBUG()
	
	iPrintNum = 0
	
	INT iTemp
	
	#IF IS_DEBUG_BUILD
	IF IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD5)
		INT iCustomPropDebug
		FOR iCustomPropDebug = 0 TO g_FMMC_STRUCT.iCurrentNumberOfDevProps - 1
			TEXT_LABEL_63 tlDebugTextLeft_Custom = "Prop "
			tlDebugTextLeft_Custom += iCustomPropDebug
			tlDebugTextLeft_Custom += ": "
			tlDebugTextLeft_Custom += GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT.sCustomProps[iCustomPropDebug].mnCustomPropName)
			DRAW_DEBUG_TEXT_2D(tlDebugTextLeft_Custom, <<0.725, 0.25 + (TO_FLOAT(iCustomPropDebug) * 0.025), 0.5>>, 255)
		ENDFOR
	ENDIF
	
	DRAW_PROP_LINE(sCurrentVarsStruct,sPropStruct)
	PROCESS_DIRTY_DOOR_DELETER()
	PROCESS_SCRIPTED_CUTSCENE_COPY_PASTER()
	
	MAINTAIN_MC_SPAWN_GROUP_OVERRIDES()
	
	IF sFMMCMenu.sActiveMenu = eFmmc_ENEMY_FIXATION_SETTINGS
	AND sFMMCMenu.fPedFixationInterruptPlayerRange > 0.0
		DRAW_DEBUG_SPHERE(sCurrentVarsStruct.vCoronaPos, sFMMCMenu.fPedFixationInterruptPlayerRange, 255, 0, 0, 100)
	ENDIF
	
	IF bGetSpPlaylist
		IF GET_UGC_BY_CONTENT_ID(sGetUGC_content, "",UGC_TYPE_GTA5_MISSION_PLAYLIST, TRUE, TRUE)
			bGetSpPlaylist = FALSE
			PRINTLN("TRY_TO_LOAD_PLAYLIST ->GET_UGC_BY_CONTENT_ID")
		ENDIF
	ENDIF
	
	IF bDealWithNewSave
	
		STRING stWidgetContents 
		STRING OverWriteName = ""
		stWidgetContents = GET_CONTENTS_OF_TEXT_WIDGET(twID)
		IF bVersionMissionNew
		stWidgetContents = "KH5P0cE-1U-VaYpT6RqktQ"
		ENDIF
		IF bOverWriteMissionNew
		stWidgetContents = "gwECCtApW0ebJyQzEFOZ5A"
		ENDIF
		
		IF DEAL_WITH_SAVING_UGC_SERVER(sGetUGC_content, sFMMCendStage, sCurrentVarsStruct.creationStats, bPublishMissionNew, bVersionMissionNew, bOverWriteMissionNew, stWidgetContents, OverWriteName)
			sFMMCendStage.iEndStage = 0
			sFMMCendStage.iSaveStage = 0				
			sFMMCendStage.iPublishStage = 0				
			sFMMCendStage.iEndStage = ciFMMC_END_STAGE_SET_UP
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			sCurrentVarsStruct.bSwitching = FALSE
			sCurrentVarsStruct.stiScreenCentreLOS = NULL
			sFMMCendStage.iEndStage = 0
			sFMMCendStage.dataStruct.iLoadStage = 0
			sFMMCendStage.iPublishConformationStage = 0
			sFMMCendStage.iPublishStage = 0
			sFMMCendStage.iButtonBitSet = 0
			bDealWithNewSave = FALSE
		ENDIF
		
	ENDIF
	
	IF bCopyContentByID
		IF COPY_UGC_BY_CONTENT_ID(sGetUGC_content, "gwECCtApW0ebJyQzEFOZ5A")
			bCopyContentByID = FALSE
		ENDIF
	ENDIF
	
	IF bEndCreator 
		SET_ENTITY_CREATION_STATUS(STAGE_SET_UP_RULES_PAGE)
		bEndCreator = FALSE
	ENDIF
	
	IF bGetMyUGC
		IF GET_UGC_CONTENT_BY_TYPE(sGetUGC_content, iTemp, iContentType, 0, INT_TO_ENUM(UGC_CATEGORY, iCATEGORYtype), INT_TO_ENUM(UGC_TYPE, iUGCtype))
			#IF IS_DEBUG_BUILD
				IF bGetHeader1
					SET_UP_FMMC_SKIP_NAMES(sFMMCendStage.SkipMenu, FALSE, TRUE)
				ENDIF
			#ENDIF
			bGetMyUGC = FALSE
		ENDIF
	ENDIF
	
	IF bLoadOldData
		STRING stWidgetContents 
		stWidgetContents = GET_CONTENTS_OF_TEXT_WIDGET(twID)
		IF IS_STRING_NULL(stWidgetContents)
			bLoadOldData = FALSE
		ENDIF
		IF LOAD_DATA_OLD(dataStruct, PLAYER_ID(), 0, "RSN_BobbyW_2", stWidgetContents)
			bLoadOldData = FALSE
		ENDIF	
	ENDIF
	
	IF bGetMyPlaylists
		IF GET_HEADER_DATA_ROCKSTAR_CREATED(sGetUGC_content, 0, UGC_TYPE_GTA5_MISSION_PLAYLIST, MAX_NUMBER_FMMC_SAVES)
			bGetMyPlaylists = FALSE
		ENDIF
	ENDIF
	
	IF bGetRockstarCandidateUGC
		IF SAVE_OUT_UGC_PLAYER_DATA("512f9751235bdb12a46ee3ed", sSaveOutVars)
			bGetRockstarCandidateUGC = FALSE
		ENDIF
	ENDIF
	
	IF (bGetUGC_byID
	OR bGetUGC_byIDWithString)
	AND NOT bLoadFromOtherUaer
		STRING stWidgetContents 
		IF bGetUGC_byIDWithString
			stWidgetContents = GET_CONTENTS_OF_TEXT_WIDGET(twID)
			IF IS_STRING_NULL(stWidgetContents)
				stWidgetContents = "510907328fc43205fc3ca8cb"
			ENDIF
		ELSE
			stWidgetContents = "510907328fc43205fc3ca8cb"
		ENDIF
		IF GET_UGC_BY_CONTENT_ID(sGetUGC_content, stWidgetContents )			
			bGetUGC_byID = FALSE
			bGetUGC_byIDWithString = FALSE
		ENDIF
	ENDIF
	
	IF bGetUGC_byIDWithString
	AND bLoadFromOtherUaer
		STRING stWidgetContents 
		stWidgetContents = GET_CONTENTS_OF_TEXT_WIDGET(twID)
		IF IS_STRING_NULL(stWidgetContents)
			bGetUGC_byIDWithString = FALSE
		ENDIF
		IF GET_UGC_BY_CONTENT_ID(sGetUGC_content, stWidgetContents)			
			bGetUGC_byIDWithString = FALSE
			CORRECT_MAX_FOR_MENUS_FROM_UGC(g_FMMC_STRUCT.iMissionType, g_FMMC_STRUCT.iNumParticipants, g_FMMC_STRUCT.iRaceType)
			SET_ENTITY_CREATION_STATUS(STAGE_DEAL_WITH_LOADING)
		ENDIF
	ENDIF
	
	IF bDelete1
		STRING stWidgetContents 
		stWidgetContents = GET_CONTENTS_OF_TEXT_WIDGET(twID)
		IF IS_STRING_NULL(stWidgetContents)
			bDelete1 = FALSE
		ENDIF
		IF DELETE_UGC_BY_CONTENT_ID(sGetUGC_content, stWidgetContents)
			bDelete1 = FALSE
		ENDIF	
	ENDIF
	
	IF bGetChallenges
		IF GET_UGC_CONTENT_BY_TYPE( sGetUGC_content, iTemp, ciUGC_GET_GET_BY_CATEGORY, 0, UGC_CATEGORY_ROCKSTAR_VERIFIED_CANDIDATE, UGC_TYPE_GTA5_CHALLENGE, FMMC_MAX_PLAY_LIST_LENGTH)
			bGetChallenges = FALSE
		ENDIF
	ENDIF
	IF bGetAChallenge
		STRING stWidgetContents 
		stWidgetContents = GET_CONTENTS_OF_TEXT_WIDGET(twID)
		IF IS_STRING_NULL(stWidgetContents)
			bGetAChallenge = FALSE
		ENDIF
		IF GET_UGC_BY_CONTENT_ID(sGetUGC_content, stWidgetContents, UGC_TYPE_GTA5_CHALLENGE)//g_sLoadedPlaylistDetails.tl31szContentID[iPlayList])
			bGetAChallenge = FALSE
		ENDIF
	ENDIF
	
	DRAW_MENU_ENTRY_MODIFIER_DEBUG()
	DRAW_CREATOR_MENU_DEBUG()
	#ENDIF
ENDPROC

PROC PRE_FRAME_CREATOR_IPL_PROCESSING()

	IF USING_FMMC_YACHT()
		INT iYachtIndex
		FOR iYachtIndex = 0 TO ciYACHT_MAX_YACHTS - 1
			PROCESS_FMMC_YACHT(iYachtIndex)
		ENDFOR
	ENDIF
	
	IF bMultiplayerMapSet
		HANDLE_REQUESTED_IPL(IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions, 		IPL_YACHT_CHUMASH), "hei_yacht_heist" )
		HANDLE_REQUESTED_IPL(IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions, 		IPL_YACHT_CHUMASH), "hei_yacht_heist_enginrm" )
		HANDLE_REQUESTED_IPL(IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions, 		IPL_YACHT_CHUMASH), "hei_yacht_heist_Lounge" )
		HANDLE_REQUESTED_IPL(IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions, 		IPL_YACHT_CHUMASH), "hei_yacht_heist_Bridge" )
		HANDLE_REQUESTED_IPL(IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions, 		IPL_YACHT_CHUMASH), "hei_yacht_heist_Bar" )
		HANDLE_REQUESTED_IPL(IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions, 		IPL_YACHT_CHUMASH), "hei_yacht_heist_Bedrm" )
		
		HANDLE_REQUESTED_IPL(IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions,	IPL_YACHT_PALETO), "gr_Heist_Yacht2" )
		HANDLE_REQUESTED_IPL(IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions,	IPL_YACHT_PALETO), "gr_Heist_Yacht2_Bridge" )
		HANDLE_REQUESTED_IPL(IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions,	IPL_YACHT_PALETO), "gr_Heist_Yacht2_Bar" )
		HANDLE_REQUESTED_IPL(IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions,	IPL_YACHT_PALETO), "gr_Heist_Yacht2_Bedrm" )
		HANDLE_REQUESTED_IPL(IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions,	IPL_YACHT_PALETO), "gr_Heist_Yacht2_Lounge" )
		HANDLE_REQUESTED_IPL(IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions,	IPL_YACHT_PALETO), "gr_Heist_Yacht2_enginrm" )
	ENDIF
	
	HANDLE_REQUESTED_IPL(IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions, 		IPL_CARRIER), "hei_carrier" )
	HANDLE_REQUESTED_IPL(IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions, 		IPL_YACHT_NEAR_PIER), "smboat" )
	HANDLE_REQUESTED_IPL(NOT IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions,	IPL_FORT_ZANCUDO_GATES), "cs3_07_mpgates" )
	
	HANDLE_REQUESTED_IPL(IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_LIFEINVADER), "facelobby")
	HANDLE_REQUESTED_IPL(NOT IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_LIFEINVADER), "facelobbyfake")
	
	HANDLE_REQUESTED_IPL(IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions,	IPL_OPEN_GRAVE), "lr_cs6_08_grave_open" )
	HANDLE_REQUESTED_IPL(IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions,	IPL_SILO), "xm_siloentranceclosed_x17" )
	HANDLE_REQUESTED_IPL(IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions,	IPL_SILO), "xm_hatches_terrain" )
	
	HANDLE_REQUESTED_IPL(NOT IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions,	IPL_REMOVE_IAA_FACILITY_SATELITE_ENTRY_DOOR), "xm_bunkerentrance_door")
	
	REQUEST_OR_REMOVE_BUNKER_DOOR_IPLS(IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions, IPL_BUNKER_DOORS))
	
	HANDLE_REQUESTED_IPL(IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions, IPL_DEVIN_HANGAR_DOOR), "sf_dlc_fixer_hanger_door")
	
	HANDLE_REQUESTED_IPL(IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions,	IPL_LOST_MC_CLUB_HOUSE), "bkr_bi_hw1_13_int")
	HANDLE_REQUESTED_IPL(NOT IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions,	IPL_LOST_MC_CLUB_HOUSE), "hei_bi_hw1_13_door")
	
	IF NOT IS_LEGACY_MISSION_CREATOR()
		HANDLE_REQUESTED_IPL(NOT IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions, IPL_CHICKEN_FACTORY), "cs1_02_cf_offmission")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions, IPL_CASINO_AIR_CONDITIONING_UNIT)
		HANDLE_REQUESTED_IPL(TRUE, "ch_dlc_casino_aircon_broken")
	ENDIF
	
	HANDLE_REQUESTED_IPL(NOT IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions,	IPL_REMOVE_CASINO_EXTERIOR_CAMERAS), "ch_h3_casino_cameras")
	
	HANDLE_REQUESTED_IPL(IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO), "vw_casino_main")
	HANDLE_REQUESTED_IPL(IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO_CAR_PARK), "vw_casino_carpark")
	HANDLE_REQUESTED_IPL(IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO_APARTMENT), "vw_casino_penthouse")
	HANDLE_REQUESTED_IPL(IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO_GARAGE), "vw_casino_garage")
	
	HANDLE_REQUESTED_IPL(NOT IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_UNION_DEPOSITORY), "DT1_03_Shutter")
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions, IPL_YACHT_AQUARIS)
		YachtData.iLocation = 34            // Paleto Bay
		YachtData.Appearance.iOption = 2    // The Aquaris
		YachtData.Appearance.iTint = 12     // Vintage
		YachtData.Appearance.iLighting = 5 // Blue Vivacious
		YachtData.Appearance.iRailing = 1   // Gold Fittings
		YachtData.Appearance.iFlag = -1     // no 
		//FILL_YACHT_MODEL_TRANSLATION		<<-777.487,6566.91,5.42995>>
		IF NOT IS_BIT_SET(iIPLLoadBitSet, ciIPLBS_YachtAquarisAssetsLoaded)
			REQUEST_ASSETS_FOR_MISSION_CREATOR_YACHT(YachtData)
			
			SET_BIT(iIPLLoadBitSet, ciIPLBS_YachtAquarisAssetsRequested)
			
			IF HAVE_ASSETS_LOADED_FOR_MISSION_CREATOR_YACHT(YachtData)
	        	CREATE_YACHT_FOR_MISSION_CREATOR(YachtData)
				
				SET_BIT(iIPLLoadBitSet, ciIPLBS_YachtAquarisAssetsLoaded)
			ENDIF
		ELSE
			UPDATE_YACHT_FOR_MISSION_CREATOR(YachtData)
		ENDIF
	ELSE
		IF IS_BIT_SET(iIPLLoadBitSet, ciIPLBS_YachtAquarisAssetsRequested)
			YachtData.iLocation = 34            // Paleto Bay
			YachtData.Appearance.iOption = 2    // The Aquaris
			YachtData.Appearance.iTint = 12     // Vintage
			YachtData.Appearance.iLighting = 5 // Blue Vivacious
			YachtData.Appearance.iRailing = 1   // Gold Fittings
			YachtData.Appearance.iFlag = -1     // no flag 
			
			DELETE_AND_CLEANUP_ASSETS_FOR_MISSION_CREATOR_YACHT(YachtData)
			
			CLEAR_BIT(iIPLLoadBitSet, ciIPLBS_YachtAquarisAssetsRequested)
			CLEAR_BIT(iIPLLoadBitSet, ciIPLBS_YachtAquarisAssetsLoaded)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions, IPL_YACHT_CHUMASH)
		SET_AMBIENT_ZONE_LIST_STATE_PERSISTENT("AZL_DLC_HEI_MILO_YACHT_ZONES",TRUE,TRUE)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions, IPL_ISLAND)
		IF NOT IS_BIT_SET(iIPLLoadBitSet, ciIPLBS_IslandLoaded)
		OR IS_BIT_SET(sFMMCmenu.iFMMCMenuBitset, ciFMMCmenuBS_RefreshIPL)
			IF HEIST_ISLAND_LOADING__LOAD_ISLAND_IPLS()
			AND SET_MISSION_ISLAND_CUSTOMISATION()
				SET_BIT(iIPLLoadBitSet, ciIPLBS_IslandLoaded)
				PRINTLN("PRE_FRAME_CREATOR_IPL_PROCESSING - ciIPLBS_IslandLoaded now set")
				CLEAR_BIT(sFMMCmenu.iFMMCMenuBitset, ciFMMCmenuBS_RefreshIPL)
			ENDIF
		ENDIF
	ELSE
		IF IS_BIT_SET(iIPLLoadBitSet, ciIPLBS_IslandLoaded)
			HEIST_ISLAND_LOADING__UNLOAD_ISLAND_IPLS()
			UNLOAD_MISSION_ISLAND_CUSTOMISATION()
			CLEAR_BIT(iIPLLoadBitSet, ciIPLBS_IslandLoaded)
			PRINTLN("PRE_FRAME_CREATOR_IPL_PROCESSING - ciIPLBS_IslandLoaded cleared")
		ENDIF
	ENDIF
	
	HANDLE_REQUESTED_IPL(IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR_FIB_LOBBY), "fiblobby")
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR_FIB_LOBBY)
		IF IS_IPL_ACTIVE("fiblobbyfake")
			REMOVE_IPL("fiblobbyfake")
		ENDIF
	ELSE
		IF NOT IS_IPL_ACTIVE("fiblobbyfake")
			REQUEST_IPL("fiblobbyfake")
		ENDIF
	ENDIF
	
	HANDLE_REQUESTED_IPL(IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_ARENA_WAR_GARAGE_LEVEL_1), "xs_arena_interior_mod_2")
	
	HANDLE_REQUESTED_IPL(IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_CUSTOM_HIGH_END_APARTMENT), "apa_v_mp_h_04_b")
	
ENDPROC

PROC PRE_FRAME_CREATOR_INTERIOR_PROCESSING()

	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("fm_mission_controller_2020")) > 0
		EXIT
	ENDIF

	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO_VAULT)
		IF interiorCasinoVaultIndex = NULL
			interiorCasinoVaultIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<2488.3, -267.4, -70.6>>, "ch_DLC_Casino_Vault")
		ELSE
			INIT_CASINO_VAULT_DOOR(interiorCasinoVaultIndex)
			IF IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions, IPL_CASINO_VAULT_DRESSING)
				TURN_ON_INTERIOR_ENTITY_SET(interiorCasinoVaultIndex, "Set_Vault_Dressing")
			ELSE
				TURN_OFF_INTERIOR_ENTITY_SET(interiorCasinoVaultIndex, "Set_Vault_Dressing")
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO_APARTMENT)
		IF interiorCasinoApartIndex = NULL
			interiorCasinoApartIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<976.636414, 70.294762, 115.164131>>, "vw_dlc_casino_apart")
		ELSE
			IF iWarpState != INTERIOR_WARP_STATE_WAIT_FOR_READY
				INIT_CASINO_APARTMENT_CREATOR_SETUP(interiorCasinoApartIndex)
			ENDIF
		ENDIF
	ENDIF
	
	HANDLE_ARENA_INTERIOR_ENTITY_CHANGE(sFMMCMenu, sCamData, vArenaCamCoords, g_ArenaInterior, sPropStruct)
	PROCESS_CREATOR_ARENA(sFMMCdata, sFMMCmenu, g_ArenaInterior, interiorArena_VIPLounge, sCamData, sFMMCMenu.bRefreshArena)
	
	IF sFMMCData.bArenaCamStuck
		REFRESH_ARENA(sFMMCMenu, FALSE, TRUE)		
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_CUSTOM_MISSION_MOC)
		IF NOT LOAD_CUSTOM_MISSION_MOC(iInteriorInitializedBS, 0, sMissionMOCProps, TRUE)
			PRINTLN("[RCC MISSION] PRE_FRAME_CREATOR_INTERIOR_PROCESSING - Waiting on Custom MOC")
		ENDIF
	ELSE
		IF sFMMCData.bRefreshInteriors
		OR IS_BIT_SET(sFMMCdata.iBitSet, biTestMissionActive)
			DELETE_MISSION_MOC_PROPS(sMissionMOCProps)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_WEED_FARM)
		IF interiorWeedFarm = NULL
			interiorWeedFarm = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<1049.6, -3196.6, -38.5>>, "bkr_biker_dlc_int_ware02")
		ENDIF
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_COCAINE_FACTORY)
		IF interiorCokeFactory = NULL
			interiorCokeFactory = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<1093.6, -3196.6, -38.5>>, "bkr_biker_dlc_int_ware03")
		ENDIF
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_RECORDING_STUDIO)
		IF interiorMusicStudio = NULL
			interiorMusicStudio = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<-1010, -70, -99.4>>, "sf_dlc_studio_sec")
		ELSE
			IF NOT IS_INTERIOR_ENTITY_SET_ACTIVE(interiorMusicStudio, "Entity_Set_Fire")
				DEACTIVATE_INTERIOR_ENTITY_SET(interiorMusicStudio, "Entity_Set_Default")
				ACTIVATE_INTERIOR_ENTITY_SET(interiorMusicStudio, "Entity_Set_Fire")
				IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, INTERIOR3_RECORDING_STUDIO_ST_SET)
					ACTIVATE_INTERIOR_ENTITY_SET(interiorMusicStudio, "entity_set_fix_trip1_int_p2")
				ELSE
					ACTIVATE_INTERIOR_ENTITY_SET(interiorMusicStudio, "entity_set_fix_stu_ext_p1")
				ENDIF
				REFRESH_INTERIOR(interiorMusicStudio)
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_MISSION_BASEMENT_INTERIOR_ENABLED()
		IF interiorMissionBasement = NULL
			interiorMissionBasement = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<850.0, -3000.0, -50.0>>, "reh_dlc_int_04_sum2")
		ENDIF
	ENDIF
	
	IF sFMMCData.bRefreshInteriors
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO_APARTMENT)
			IF interiorCasinoApartIndex != NULL
				DEACTIVATE_INTERIOR_ENTITY_SET(interiorCasinoApartIndex, "Set_Pent_Tint_Shell")
				DEACTIVATE_INTERIOR_ENTITY_SET(interiorCasinoApartIndex, "Set_Pent_Pattern_01")
				DEACTIVATE_INTERIOR_ENTITY_SET(interiorCasinoApartIndex, "Set_Pent_Pattern_02")
				DEACTIVATE_INTERIOR_ENTITY_SET(interiorCasinoApartIndex, "Set_Pent_Pattern_03")
				DEACTIVATE_INTERIOR_ENTITY_SET(interiorCasinoApartIndex, "Set_Pent_Pattern_04")
				DEACTIVATE_INTERIOR_ENTITY_SET(interiorCasinoApartIndex, "Set_Pent_Pattern_05")
				DEACTIVATE_INTERIOR_ENTITY_SET(interiorCasinoApartIndex, "Set_Pent_Pattern_06")
				DEACTIVATE_INTERIOR_ENTITY_SET(interiorCasinoApartIndex, "Set_Pent_Pattern_07")
				DEACTIVATE_INTERIOR_ENTITY_SET(interiorCasinoApartIndex, "Set_Pent_Pattern_08")
				DEACTIVATE_INTERIOR_ENTITY_SET(interiorCasinoApartIndex, "Set_Pent_Pattern_09")
				DEACTIVATE_INTERIOR_ENTITY_SET(interiorCasinoApartIndex, "set_pent_bar_light_0")
				DEACTIVATE_INTERIOR_ENTITY_SET(interiorCasinoApartIndex, "set_pent_bar_light_01")
				DEACTIVATE_INTERIOR_ENTITY_SET(interiorCasinoApartIndex, "set_pent_bar_light_02")
				DEACTIVATE_INTERIOR_ENTITY_SET(interiorCasinoApartIndex, "Set_Pent_Arcade_Retro")
				DEACTIVATE_INTERIOR_ENTITY_SET(interiorCasinoApartIndex, "Set_Pent_Arcade_Modern")
				DEACTIVATE_INTERIOR_ENTITY_SET(interiorCasinoApartIndex, "Set_Pent_Bar_Clutter")
				DEACTIVATE_INTERIOR_ENTITY_SET(interiorCasinoApartIndex, "Set_Pent_Clutter_01")
				DEACTIVATE_INTERIOR_ENTITY_SET(interiorCasinoApartIndex, "Set_Pent_Clutter_02")
				DEACTIVATE_INTERIOR_ENTITY_SET(interiorCasinoApartIndex, "Set_Pent_Clutter_03")
				DEACTIVATE_INTERIOR_ENTITY_SET(interiorCasinoApartIndex, "set_pent_bar_party_0")
				DEACTIVATE_INTERIOR_ENTITY_SET(interiorCasinoApartIndex, "set_pent_bar_party_1")
				DEACTIVATE_INTERIOR_ENTITY_SET(interiorCasinoApartIndex, "set_pent_bar_party_2")
				DEACTIVATE_INTERIOR_ENTITY_SET(interiorCasinoApartIndex, "Set_Pent_Spa_Bar_Closed")
				DEACTIVATE_INTERIOR_ENTITY_SET(interiorCasinoApartIndex, "Set_Pent_Spa_Bar_Open")
			ENDIF
		ENDIF
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_WEED_FARM)
		AND interiorWeedFarm != NULL
			CLEANUP_MISSION_FACTORY(sMissionFactories.sWeedFarm)
			SET_LONG_BIT(iInteriorInitializingExtrasBS, INTERIOR2_WEED_FARM)
			REFRESH_INTERIOR(interiorWeedFarm)
		ENDIF
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_COCAINE_FACTORY)
		AND interiorCokeFactory != NULL
			CLEANUP_MISSION_FACTORY(sMissionFactories.sCokeFactory)
			SET_LONG_BIT(iInteriorInitializingExtrasBS, INTERIOR2_COCAINE_FACTORY)
			REFRESH_INTERIOR(interiorCokeFactory)
		ENDIF
		
		sFMMCData.bRefreshInteriors = FALSE
	ENDIF
	
	PROCESS_MISSION_FACTORY_INTERIORS(sMissionFactories, iInteriorInitializingExtrasBS)
	HANDLE_INTERIOR_ENTITY_SETS()
	
ENDPROC

PROC SET_NEW_2020_MISSION_CREATOR_TEAM_DEFAULTS()
	
	INT iTeam
	FOR iTeam = 0 TO FMMC_MAX_TEAMS - 1
		g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iPlayerLives = ciNUMBER_OF_LIVES__DIFFICULTY_VARIABLE
		
		g_FMMC_STRUCT.iNumPlayersPerTeam[iTeam] = 1
		g_FMMC_STRUCT.iMaxNumPlayersPerTeam[iTeam] = 4
		
		SET_BIT(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitset, ciBS_Player_SteamProofFlag)
	ENDFOR
ENDPROC

PROC SET_NEW_2020_MISSION_CREATOR_RULE_DEFAULTS()

	INT iRule, iTeam
	FOR iRule = 0 TO FMMC_MAX_RULES - 1
		FOR iTeam = 0 TO FMMC_MAX_TEAMS - 1
			SET_BIT(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitset[iRule], ciBS_RULE_HIDE_HUD)
			SET_BIT(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitset[iRule], ciBS_RULE_HIDE_TICKERS)
			SET_BIT(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitset[iRule], ciBS_RULE_OVERRIDE_HIDEHUD_SHOW_MULTIRULE_TIMER)
			
			g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjectiveScore[iRule] = 0
			g_CreatorsSelDetails.sSubRowOptions[iTeam].iSelection[iRule][FMMC_SUB_OPTIONS_SCORE] = 0
		
			SET_BIT(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFive[iRule], ciBLIP_SECURITY_PEDS_DURING_WANTED)
			SET_BIT(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEleven[iRule], ciBS_RULE11_GANG_CHASE_DESPAWN_ON_NEW_GANG_CHASE_TYPE)
			SET_BIT(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFive[iRule], ciBS_RULE5_GANG_CHASE_BLIP_FORCED_ON)
			g_FMMC_STRUCT.sFMMCEndConditions[iTeam].fGangBlipNoticeRange[iRule] = 300.0
			
			SET_BIT(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwo[iRule], ciBS_RULE2_PLAY_AREA_TRIGGER_ON_START)
			SET_BIT(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFour[iRule], ciBS_RULE4_KILL_PLAYER_ON_BOUNDS_FAIL)
			g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].iMinimapAlpha = 60
			
			SET_BIT(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThree[iRule], ciBS_RULE3_PLAY_AREA_2_TRIGGER_ON_START)
			g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule].iMinimapAlpha = 60
			
			g_FMMC_STRUCT.sFMMCEndConditions[iTeam].fDropOffHeight[iRule] = 5.0
			
			g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iGangMaxBackup[iRule] = ciFMMC_GANG_CHASE_INFINITE_SPAWNS
			
		ENDFOR
		
		g_FMMC_STRUCT.sFMMCEndConditions[0].sLightBarStruct[iRule].iLBred = 255
		g_FMMC_STRUCT.sFMMCEndConditions[0].sLightBarStruct[iRule].iLBgreen = 128
		
		g_FMMC_STRUCT.sFMMCEndConditions[1].sLightBarStruct[iRule].iLBred = 128
		g_FMMC_STRUCT.sFMMCEndConditions[1].sLightBarStruct[iRule].iLBblue = 255
		
		g_FMMC_STRUCT.sFMMCEndConditions[2].sLightBarStruct[iRule].iLBred = 255
		g_FMMC_STRUCT.sFMMCEndConditions[2].sLightBarStruct[iRule].iLBblue = 255
		
		g_FMMC_STRUCT.sFMMCEndConditions[3].sLightBarStruct[iRule].iLBgreen = 255
	ENDFOR
ENDPROC

PROC SET_NEW_2020_MISSION_CREATOR_CUTSCENE_DEFAULTS()
	INT i = 0
	
	// Mission Globals.
	FOR i = 0 TO _POST_CASINO_HEIST__MAX_SCRIPTED_CUTSCENES-1
		g_FMMC_STRUCT.sScriptedCutsceneData[i].iCutscenePlayerInclusionRange = 5000		
	ENDFOR
	FOR i = 0 TO MAX_MOCAP_CUTSCENES-1
		g_FMMC_STRUCT.sMocapCutsceneData[i].iCutscenePlayerInclusionRange = 5000		
	ENDFOR	
	g_FMMC_STRUCT.sEndMocapSceneData.iCutscenePlayerInclusionRange = 5000
	
	// Menu
	g_FMMC_STRUCT.sCurrentMocapSceneData.iCutscenePlayerInclusionRange = 5000
	g_FMMC_STRUCT.sCurrentSceneData.iCutscenePlayerInclusionRange = 5000
ENDPROC

PROC SET_NEW_2020_MISSION_CREATOR_WARP_PORTAL_DEFAULTS()
	INT i
	FOR i = 0 TO FMMC_MAX_WARP_PORTALS-1
		g_FMMC_STRUCT.sWarpPortals[i].fVisualRangeOverride = -2.0
	ENDFOR
ENDPROC

PROC SET_NEW_2020_MISSION_CREATOR_DEFAULTS(BOOL bIsInitCall = FALSE)
	
	IF NOT bIsInitCall
		// If it's a mid-creator call, only reset entity creation stuff
		SET_NEW_2020_ENTITY_CREATION_DEFAULTS(sFMMCmenu, bIsInitCall, sObjStruct)
		EXIT
		
	ELSE
		SET_BIT(g_FMMC_STRUCT.iMissionTypeBitSet, ciMISSION_OPTION_BS_MISSION_FMMC2020)
	ENDIF
	
	SET_NEW_2020_ENTITY_CREATION_DEFAULTS(sFMMCmenu, bIsInitCall, sObjStruct)
	
	SET_NEW_2020_MISSION_CREATOR_TEAM_DEFAULTS()
	SET_NEW_2020_MISSION_CREATOR_RULE_DEFAULTS()
	SET_NEW_2020_MISSION_CREATOR_CUTSCENE_DEFAULTS()
	SET_NEW_2020_MISSION_CREATOR_WARP_PORTAL_DEFAULTS()
	
	g_FMMC_STRUCT.iNumParticipants = 4
	g_FMMC_STRUCT.iMinNumParticipants = 1
	g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_CONTACT
		
	INT iLoop
	FOR iLoop = FMMC_MENU_REL_1_2_HATE TO FMMC_MENU_REL_3_4_HATE
		SET_BIT(g_FMMC_STRUCT.iBS_MissionTeamRelations, iLoop)
		SET_THE_RELATIONSHIP_BITS(iLoop, TRUE)
	ENDFOR
	
	INT iTeam, iRule
	FOR iTeam = 0 TO FMMC_MAX_TEAMS-1
		FOR iRule = 0 TO FMMC_MAX_RULES-1
			FOR iLoop = FMMC_MENU_REL_1_2_HATE TO FMMC_MENU_REL_3_4_HATE
				SET_BIT(g_fmmc_struct.sFMMCEndConditions[iTeam].iBS_MissionTeamRelations_OnRuleSetting[iRule], iLoop)
				SET_BIT(g_fmmc_struct.sFMMCEndConditions[iTeam].iBS_MissionTeamRelations_OnRuleSetting[iRule], iLoop)
				SET_BIT(g_fmmc_struct.sFMMCEndConditions[iTeam].iBS_MissionTeamRelations_OnRuleSetting[iRule], iLoop)
				SET_BIT(g_fmmc_struct.sFMMCEndConditions[iTeam].iBS_MissionTeamRelations_OnRuleSetting[iRule], iLoop)
				SET_BIT(g_FMMC_STRUCT.iBS_MissionTeamRelations, iLoop)
				SET_THE_RELATIONSHIP_BITS_FOR_RULE(iLoop, TRUE, iRule, iTeam)
			ENDFOR
		ENDFOR
	ENDFOR
	
	sFMMCMenu.iTeamLivesOption = ciMISSION_LIVES_OPTION_TEAM
	
	SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetThree,  ciRESPAWN_WITH_LAST_WEAPON)
	
	g_FMMC_STRUCT.fCashGrabSlowCap[ciCASH_GRAB_SLOW_VALUES_LIGHT] = 1.65
	g_FMMC_STRUCT.fCashGrabSlowCap[ciCASH_GRAB_SLOW_VALUES_HEAVY] = 1.55
	g_FMMC_STRUCT.iCashGrabSlowTrigger = 20
	
	SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyNine, ciOptionsBS29_CustomTimerNameIsTextLabel)
	
	SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_DISABLE_PV_NODES_OUTSIDE_PROPERTY)
	
	SET_BIT(g_FMMC_STRUCT.iRuleOptionsBitSet, ciDISABLE_MERCENARIES)
	SET_BIT(g_FMMC_STRUCT.iRuleOptionsBitSet, ciDISABLE_AMMO_DROP)
	SET_BIT(g_FMMC_STRUCT.iRuleOptionsBitSet, ciDISABLE_REVEAL_PLAYERS)
	SET_BIT(g_FMMC_STRUCT.iRuleOptionsBitSet, ciDISABLE_REQUEST_VEHICLE)
	SET_BIT(g_FMMC_STRUCT.iRuleOptionsBitSet, ciDISABLE_HELI_PICKUP)
	SET_BIT(g_FMMC_STRUCT.iRuleOptionsBitSet, ciDISABLE_BACKUP_HELI)
	SET_BIT(g_FMMC_STRUCT.iRuleOptionsBitSet, ciDISABLE_BOAT_PICKUP)
	SET_BIT(g_FMMC_STRUCT.iRuleOptionsBitSet, ciDISABLE_BULL_SHARK)
	SET_BIT(g_FMMC_STRUCT.iRuleOptionsBitSet, ciDISABLE_AIRSTRIKE)
	SET_BIT(g_FMMC_STRUCT.iRuleOptionsBitSet, ciDISABLE_PEGASUS_VEHICLE)
	
	SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetFive,  ciPARACHUTE_EQUIP_LAST_WEAPON)
	SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciOPTIONS4_MAINTAIN_SPAWN_GROUPS_ON_RESTART)
	
	CLEAR_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetTen, ciDISABLE_BIKE_COMBAT)
	
	SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciLOWER_OBJECT_CLEANUP)
	SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFive, ciOptionsBS25_DontHideScriptObjectsInModelHide)
	SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_DefaultForceQuickRestart)
	SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_PlayEndCutsceneForAllPlayers)
	SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_RespectCCTVSpottedDelay)
	SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_CCTVAlertSpottedSounds)
	SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_TriggerAllCCTVCameras)
	SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFive, ciOptionsBS25_TriggerCCTVAggroWithoutPeds)
	SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyNine, ciOptionsBS29_SavedAfterForceJoinRoadChanges)
	SET_BIT(sFMMCmenu.iFMMCMenuBitset, ciFMMCMenuBS_HideCreatorBlipsInTest)
	
	sFMMCmenu.iCurrentAlignment = FREE_POSITION_LOCAL
	sFMMCmenu.iCurrentAlignment_Rotation = FREE_POSITION_LOCAL
	
	sFMMCMenu.iCoverType = 2 //Wall to Both
	
	REFRESH_MENU(sFMMCMenu)
ENDPROC

PROC PRE_FRAME_CREATOR_PROCESSING()
	
	IF IS_BIT_SET(sFMMCmenu.iFMMCMenuBitset, ciFMMCmenuBS_RequestOnScreenInstructionalButtonPrompt)
		RESET_UP_HELP()
		CLEAR_BIT(sFMMCmenu.iFMMCMenuBitset, ciFMMCmenuBS_RequestOnScreenInstructionalButtonPrompt)		
	ENDIF
	
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
	OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
	OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
	OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
	OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
		IF IS_BIT_SET(sFMMCmenu.iFMMCMenuBitset, ciFMMCmenuBS_IsOnScreenVectorKeyboardValidThisFrame)
			CLEAR_BIT(sFMMCmenu.iFMMCMenuBitset, ciFMMCmenuBS_IsOnScreenVectorKeyboardValidThisFrame)
		ELIF IS_BIT_SET(sFMMCmenu.iFMMCMenuBitset, ciFMMCmenuBS_IsOnScreenVectorKeyboardValid)
			CLEAR_BIT(sFMMCmenu.iFMMCMenuBitset, ciFMMCmenuBS_IsOnScreenVectorKeyboardValid)
		ENDIF
		IF IS_BIT_SET(sFMMCmenu.iFMMCMenuBitset, ciFMMCmenuBS_IsOnScreenPopupValidThisFrame)
			CLEAR_BIT(sFMMCmenu.iFMMCMenuBitset, ciFMMCmenuBS_IsOnScreenPopupValidThisFrame)	
		ELIF IS_BIT_SET(sFMMCmenu.iFMMCMenuBitset, ciFMMCmenuBS_IsOnScreenPopupValid)
			CLEAR_BIT(sFMMCmenu.iFMMCMenuBitset, ciFMMCmenuBS_IsOnScreenPopupValid)	
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(sFMMCMenu.iFMMCMenuBitset, ciFMMCMenuBS_ResetEntityCreationDefaultsASAP)
		IF NOT IS_LEGACY_MISSION_CREATOR()
		AND NOT IS_3D_EXPLODER_CREATOR()
			SET_NEW_2020_MISSION_CREATOR_DEFAULTS()
		ENDIF
		
		CLEAR_BIT(sFMMCMenu.iFMMCMenuBitset, ciFMMCMenuBS_ResetEntityCreationDefaultsASAP)
	ENDIF
	
	IF NOT IS_BIT_SET(sFMMCdata.iBitSet, biTestMissionActive)
		sCurrentVarsStruct.creationStats.iCreatingTimeMS += GET_GAME_TIMER() - sCurrentVarsStruct.creationStats.iCurrentTimeMS
	ENDIF
	sCurrentVarsStruct.creationStats.iCurrentTimeMS = GET_GAME_TIMER()
	
	PROCESS_SINGLEPLAYER_MODEL_ON_CREATOR(bIsCreatorModelSetSPTC,bIsMaleSPTC)
	
	IF sFMMCmenu.iPassBranch != -1
	AND sFMMCmenu.iFailBranch != -1
	AND sFMMCMenu.sActiveMenu = eFmmc_MAIN_MENU_BASE
		sFMMCmenu.iPassBranch = -1
	 	sFMMCmenu.iFailBranch = -1
	ENDIF
	
	IF sFMMCmenu.sActiveMenu != eFMMC_PERSONAL_VEHICLE_POSITIONS
	AND sFMMCmenu.iCurrentPVPosSlot != -1
		sFMMCmenu.iCurrentPVPosSlot = -1
		PRINTLN("[slotchange] set to -1")
	ENDIF
	
	IF interiorCopsOld = NULL
		interiorCopsOld = GET_INTERIOR_AT_COORDS_WITH_TYPE( <<446.8809, -985.8868, 29.6895>>, "v_policehub")
	ENDIF
	
	INTERIOR_INSTANCE_INDEX interiorRockID
	IF interiorRockID = NULL
		interiorRockID = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< -556.5089, 286.3181, 81.1763 >>, "v_rockclub")
		
		IF IS_INTERIOR_DISABLED(interiorRockID)
		OR IS_INTERIOR_CAPPED(interiorRockID)
			SET_INTERIOR_CAPPED(INTERIOR_V_ROCKCLUB, FALSE)
			SET_INTERIOR_DISABLED(INTERIOR_V_ROCKCLUB, FALSE)
			REFRESH_INTERIOR(interiorRockID)
			SET_DOOR_STATE(DOORNAME_TEQUILA_CLUB_DOOR_F, DOORSTATE_UNLOCKED)
			SET_DOOR_STATE(DOORNAME_TEQUILA_CLUB_DOOR_R, DOORSTATE_UNLOCKED)
		ENDIF
	ENDIF
		
	IF IS_BIT_SET(sFMMCmenu.iFMMCMenuBitset, ciFMMCMenuBS_HideCreatorBlipsInTest)
		IF IS_BIT_SET(iLocalBitset, biDeletedBlipsForTest)
			IF NOT IS_BIT_SET(sFMMCdata.iBitSet, biTestMissionActive)
				CLEAR_BIT(iLocalBitset, biDeletedBlipsForTest)
			ENDIF
		ELSE
			IF IS_BIT_SET(sFMMCdata.iBitSet, biTestMissionActive)
				DELETE_ALL_BLIPS_FOR_TEST()
				SET_BIT(iLocalBitset, biDeletedBlipsForTest)
			ENDIF
		ENDIF
	ENDIF
	
	DO_BLIMP_SIGNS(sBlimpSign, sCurrentVarsStruct.iMenuState = STAGE_DEAL_WITH_FINISHING)
	
	IF sFMMCMenu.sActiveMenu != eFmmc_OBJECT_BASE
	AND sFMMCMenu.sActiveMenu != eFmmc_PROP_BASE
		IF sFMMCmenu.iObjectOffsetTweak != 0
			sFMMCmenu.iObjectOffsetTweak = 0
		ENDIF			
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_FIREWORK_OPTIONS
		HANDLE_DRAWING_AND_PLACING_FIREWORK_TRIGGERS()
	ENDIF
	
	IF interiorCopsOld <> NULL
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSet, ciUseCNCInteriors)
			IF NOT bOldinCopsInteriorDisabled
				DISABLE_INTERIOR(interiorCopsOld, TRUE)
				bOldinCopsInteriorDisabled = TRUE
			ENDIF
		ELSE
			IF bOldinCopsInteriorDisabled
				DISABLE_INTERIOR(interiorCopsOld, FALSE)
				bOldinCopsInteriorDisabled = FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF (IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions, IPL_CARRIER) OR IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions, IPL_YACHT_CHUMASH ) OR IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions, IPL_YACHT_PALETO ) )
	AND NOT bMultiplayerMapSet
		PRINTLN("PRE_FRAME_CREATOR_PROCESSING - bMultiplayerMapSet - swapping map changesets")
		SET_MP_MAP_LOADED(TRUE)
		//REVERT_CONTENT_CHANGESET_GROUP_FOR_ALL(HASH("GROUP_MAP_SP"))
		//EXECUTE_CONTENT_CHANGESET_GROUP_FOR_ALL(HASH("GROUP_MAP"))
		bMultiplayerMapSet = TRUE
	ENDIF
	
	IF ARE_SPAWN_GROUPS_USING_CREATOR_DEFINE_SETTINGS()	
		g_FMMC_STRUCT.iRandomEntitySpawn_MaxNumber = ciMAX_NUM_FMMC_ENTITY_SUB_SPAWN_GROUPS-1
		bForcedSpawnGroups = TRUE
	ELIF bForcedSpawnGroups
		g_FMMC_STRUCT.iRandomEntitySpawn_MaxNumber = 0
		bForcedSpawnGroups = FALSE
	ENDIF
			
	MAINTAIN_SETTING_RETAINED_BITSETS()
	sGetUGC_content.iPublishedSelection = iPublishedSelection1
	
	PRE_FRAME_CREATOR_IPL_PROCESSING()
	PRE_FRAME_CREATOR_INTERIOR_PROCESSING()
	
	PROCESS_ON_DEMAND_MENU_REFRESHING(sFMMCMenu)
	
	sCurrentVarsStruct.bDiscVisible = FALSE
	sCurrentVarsStruct.bCanCreateADecalThisFrame = TRUE

	IF iPropListsCacheStaggeredLoop <= (PROP_LIBRARY_MAX_ITEMS - 1)
		GET_NUMBER_OF_PROPS_IN_LIBRARY(iPropListsCacheStaggeredLoop, sFMMCmenu)
		iPropListsCacheStaggeredLoop++
	ENDIF
	
ENDPROC

PROC PROCESS_BLIP_INFO_STRUCT_DEFAULTS()
	IF iCreationTypeLastFrame != sFMMCmenu.iEntityCreation
		iCreationTypeLastFrame = sFMMCmenu.iEntityCreation
		
		CLEAR_ENTITY_BLIP_DATA_STRUCT(sFMMCmenu.sEntityBlipStruct)
		SET_BLIP_INFO_STRUCT_DEFAULTS_FOR_CREATION_TYPE(sFMMCmenu.sEntityBlipStruct, GET_BLIP_TYPE_FROM_MENU_SECTION(sFMMCmenu))
		sFMMCmenu.iBlipSpriteOverride = -1
		sFMMCmenu.iBlipSpriteOverrideCategory = -1
		sFMMCmenu.iBlipSpriteOverrideType = -1
		PRINTLN("PROCESS_BLIP_INFO_STRUCT_DEFAULTS - Updating entity blip struct for creation type: ", sFMMCmenu.iEntityCreation)
	ENDIF
ENDPROC

FUNC BOOL IS_MUSIC_MOOD_INVALID(INT iMood)
	
	SWITCH iMood
		CASE MUSIC_FAIL
		CASE MUSIC_LTS_ENDING
		CASE MUSIC_CTF_ENDING
		CASE MUSIC_MID_COUNTDOWN
		CASE MUSIC_COCK_SONG
		CASE MUSIC_WAVERY
		CASE MUSIC_MOOD_NONE
			RETURN TRUE
		BREAK		
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC CHANGE_MC_MUSIC_TYPE(INT &iAssign, INT iSelectionChange)
	
	iAssign += iSelectionChange
	CAP_FMMC_MENU_ITEM(MUSIC_MAX, iAssign, -1)
	
	WHILE IS_MUSIC_MOOD_INVALID(iAssign)		
		iAssign += iSelectionChange
		CAP_FMMC_MENU_ITEM(MUSIC_MAX, iAssign, -1)
	ENDWHILE	

ENDPROC

#ENDIF
