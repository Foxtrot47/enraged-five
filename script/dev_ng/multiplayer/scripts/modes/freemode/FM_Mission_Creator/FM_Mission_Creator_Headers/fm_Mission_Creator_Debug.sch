
#IF IS_DEBUG_BUILD

USING "FM_Mission_Creator_Using.sch"

PROC CREATE_MC_SPAWN_GROUP_OVERRIDES()
	INT i = 0
	INT ii = 0
	TEXT_LABEL_23 tl15 = ""	
			
	START_WIDGET_GROUP("MC Spawn Group Overrides")
		ADD_WIDGET_BOOL("Clear Settings", bBoolTempClear)		
		
		FOR	i = 0 TO FMMC_MAX_RANDOM_ENTITY_SPAWN_GROUPS-1
			tl15 = "Spawn Group "
			tl15 += (i)
			
			START_WIDGET_GROUP(tl15)
			
				ADD_WIDGET_BOOL(tl15, bBoolTemp[i])
				
				FOR	ii = 0 TO ciMAX_NUM_FMMC_ENTITY_SUB_SPAWN_GROUPS-1
					IF i < ciMAX_NUM_FMMC_ENTITY_SUB_SPAWN_GROUPS
						tl15 = "Spawn SubGroup "
						tl15 += (ii)
					
						ADD_WIDGET_BOOL(tl15, bBoolSubTemp[i][ii])
					ENDIF
				ENDFOR
				
			STOP_WIDGET_GROUP()
		ENDFOR
	STOP_WIDGET_GROUP()
ENDPROC

PROC DRAW_DIRECTIONAL_ARROW(VECTOR vPos, VECTOR vVec2, FLOAT fOffset = 0.0, INT iR = 255, INT iG = 0, INT iB = 0, INT iA = 140)
	FLOAT fHeading = GET_HEADING_FROM_VECTOR_2D(vPos.x - vVec2.x, vPos.y - vVec2.y)
	
	DRAW_MARKER(MARKER_CHEVRON_1, 
			GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPos, fHeading , <<0.0, (fOffset/2), 0.0>>) + <<0.0, 0.0, 0.5>>, 
			<<0.0, 0.0, 0.0>>, 
			<<90.0, fHeading - 180.0, 0.0>>, 
			<<2.0, 2.0, 1.0>>,
			iR, iG, iB, iA, FALSE, FALSE, EULER_XYZ)
ENDPROC

PROC CREATE_CREATOR_RANDOM_CHECKPOINT_AS_SPAWN_OVERRIDE()
	TEXT_LABEL_23 tl15 = ""				
	START_WIDGET_GROUP("MC Random Checkpoint Spawn Override")
		tl15 = "Checkpoint Random Spawn "					
		ADD_WIDGET_INT_SLIDER(tl15, g_iMissionForcedCheckpointSpawn, -1, 4, 1)
	STOP_WIDGET_GROUP()
ENDPROC

PROC PROCESS_DIRTY_DOOR_DELETER()
	IF NOT bDirtyDoorDeleterEnabled
		EXIT
	ENDIF
	
	IF iDirtyDoorToDelete < 0
	OR iDirtyDoorToDelete >= FMMC_MAX_NUM_DOORS
		EXIT
	ENDIF
	
	DRAW_DEBUG_TEXT_ABOVE_COORDS(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDirtyDoorToDelete].vPos, "DIRTY DOOR DELETION TARGET", 1.0, 255, 0, 0, 255)
	
	IF bDirtyDoorDelete
		PRINTLN("PROCESS_DIRTY_DOOR_DELETER - DELETING DOOR ", iDirtyDoorToDelete)
		bDirtyDoorDelete = FALSE
		DELETE_THIS_DOOR(iDirtyDoorToDelete, biDoors, sCurrentVarsStruct)
	ENDIF
ENDPROC

PROC PROCESS_SCRIPTED_CUTSCENE_COPY_PASTER()

	IF bScriptedCutsceneCopy
		
		IF iScriptedCutsceneIndexToCopy = -1
			PRINTLN("PROCESS_SCRIPTED_CUTSCENE_COPY_PASTER - Wiping cutscene data")
			CLEAR_CUTSCENE_DATA(g_FMMC_STRUCT.sCopiedScriptedCutsceneData)
		ELSE
			PRINTLN("PROCESS_SCRIPTED_CUTSCENE_COPY_PASTER - Copying cutscene data from index: ", iScriptedCutsceneIndexToCopy)
			COPY_CUTSCENE_DATA(g_FMMC_STRUCT.sScriptedCutsceneData[iScriptedCutsceneIndexToCopy], g_FMMC_STRUCT.sCopiedScriptedCutsceneData)
		ENDIF
		
		bScriptedCutsceneCopy = FALSE
		
	ENDIF
	
	IF bScriptedCutscenePaste
		
		PRINTLN("PROCESS_SCRIPTED_CUTSCENE_COPY_PASTER - Pasting cutscene data to index: ", iScriptedCutsceneIndexToCopy)
		COPY_CUTSCENE_DATA(g_FMMC_STRUCT.sCopiedScriptedCutsceneData, g_FMMC_STRUCT.sScriptedCutsceneData[iScriptedCutsceneIndexToPasteTo])
	
		bScriptedCutscenePaste = FALSE
		
	ENDIF

ENDPROC

PROC HANDLE_MENU_MARKERS()

	IF sFMMCmenu.iCurrentShot < -1 OR sFMMCmenu.iCurrentShot >= MAX_CAMERA_SHOTS
		EXIT
	ENDIF

	FLOAT fRadius = g_FMMC_STRUCT.sCurrentSceneData.fStreamRadius[sFMMCmenu.iCurrentShot]
	
	IF sFMMCMenu.sActiveMenu = efMMC_CUTSCENE_ADD_SHOT
		IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sCurrentSceneData.vStreamStartPos[sFMMCmenu.iCurrentShot])
			DRAW_MARKER(MARKER_SPHERE, g_FMMC_STRUCT.sCurrentSceneData.vStreamStartPos[sFMMCmenu.iCurrentShot], <<0,0,0>>, <<0,0,0>>, <<fRadius,fRadius,fRadius>>, 255, 182, 193, 100, FALSE, TRUE)
		ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = efMMC_CUTSCENE_ADD_SHOT
		IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sCurrentSceneData.vWalkToPosition[sFMMCmenu.iCurrentShot])
			FLOAT fWalkToRadius = 3.0
			IF g_FMMC_STRUCT.sCurrentSceneData.fWalkToPositionRadius[sFMMCmenu.iCurrentShot] >= 1.0
				fWalkToRadius = g_FMMC_STRUCT.sCurrentSceneData.fWalkToPositionRadius[sFMMCmenu.iCurrentShot]
			ENDIF
			DRAW_MARKER(MARKER_SPHERE, g_FMMC_STRUCT.sCurrentSceneData.vWalkToPosition[sFMMCmenu.iCurrentShot], <<0,0,0>>, <<0,0,0>>, <<fWalkToRadius,fWalkToRadius,fWalkToRadius>>, 255, 0, 0, 100, FALSE, TRUE)
		ENDIF
		IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sCurrentSceneData.vDriveToPosition[sFMMCmenu.iCurrentShot])
			DRAW_MARKER(MARKER_SPHERE, g_FMMC_STRUCT.sCurrentSceneData.vDriveToPosition[sFMMCmenu.iCurrentShot], <<0,0,0>>, <<0,0,0>>, <<3,3,3>>, 255, 0, 255, 100, FALSE, TRUE)
		ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = efMMC_CUTSCENE_CLEAR		
		IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sCurrentSceneData.vClearCoOrds)
			fRadius = g_FMMC_STRUCT.sCurrentSceneData.fClearRadius
			DRAW_MARKER(MARKER_SPHERE, g_FMMC_STRUCT.sCurrentSceneData.vClearCoOrds, <<0,0,0>>, <<0,0,0>>, <<fRadius,fRadius,fRadius>>, 120, 0, 0, 100, FALSE, TRUE)
		ENDIF
		
		IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sCurrentSceneData.vReplacementArea)
			fRadius = g_FMMC_STRUCT.sCurrentSceneData.fClearRadius
			DRAW_MARKER(MARKER_SPHERE, g_FMMC_STRUCT.sCurrentSceneData.vReplacementArea, <<0,0,0>>, <<0,0,0>>, <<fRadius,fRadius,fRadius>>, 0, 120, 0, 100, FALSE, TRUE)
		ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = efMMC_END_CUTSCENE_CLEAR		
		IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sEndMocapSceneData.vClearCoOrds)
			fRadius = g_FMMC_STRUCT.sEndMocapSceneData.fClearRadius
			DRAW_MARKER(MARKER_SPHERE, g_FMMC_STRUCT.sEndMocapSceneData.vClearCoOrds, <<0,0,0>>, <<0,0,0>>, <<fRadius,fRadius,fRadius>>, 120, 0, 0, 100, FALSE, TRUE)
		ENDIF
		
		IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sEndMocapSceneData.vReplacementArea)
			fRadius = g_FMMC_STRUCT.sEndMocapSceneData.fClearRadius
			DRAW_MARKER(MARKER_SPHERE, g_FMMC_STRUCT.sEndMocapSceneData.vReplacementArea, <<0,0,0>>, <<0,0,0>>, <<fRadius,fRadius,fRadius>>, 0, 120, 0, 100, FALSE, TRUE)
		ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = efMMC_CUTSCENE_MOCAP_CLEAR		
		IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sCurrentMocapSceneData.vClearCoOrds)
			fRadius = g_FMMC_STRUCT.sCurrentMocapSceneData.fClearRadius
			DRAW_MARKER(MARKER_SPHERE, g_FMMC_STRUCT.sCurrentMocapSceneData.vClearCoOrds, <<0,0,0>>, <<0,0,0>>, <<fRadius,fRadius,fRadius>>, 120, 0, 0, 100, FALSE, TRUE)
		ENDIF
		
		IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sCurrentMocapSceneData.vReplacementArea)
			fRadius = g_FMMC_STRUCT.sCurrentMocapSceneData.fClearRadius
			DRAW_MARKER(MARKER_SPHERE, g_FMMC_STRUCT.sCurrentMocapSceneData.vReplacementArea, <<0,0,0>>, <<0,0,0>>, <<fRadius,fRadius,fRadius>>, 0, 120, 0, 100, FALSE, TRUE)
		ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFMMC_TEAM_POST_MISSION_PLAYER_SPAWN
		fRadius = g_FMMC_STRUCT.fPostMissionSpawnRadius[sFMMCmenu.iSelectedTeam]
		IF fRadius < 1.0
			fRadius = 1.0
		ENDIF
		IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.vPostMissionPos[sFMMCmenu.iSelectedTeam])
			DRAW_MARKER(MARKER_SPHERE, g_FMMC_STRUCT.vPostMissionPos[sFMMCmenu.iSelectedTeam], <<0,0,0>>, <<0,0,0>>, <<fRadius,fRadius,fRadius>>, 0, 0, 255, 100, FALSE, TRUE)
		ENDIF
		IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.vPostMissionPOI[sFMMCmenu.iSelectedTeam])
			DRAW_MARKER(MARKER_SPHERE, g_FMMC_STRUCT.vPostMissionPOI[sFMMCmenu.iSelectedTeam], <<0,0,0>>, <<0,0,0>>, <<1.0,1.0,1.0>>, 0, 255, 0, 100, FALSE, TRUE)
		ENDIF
	ENDIF
ENDPROC

PROC CREATE_WIDGETS()

	INT i, i2//, i3
	TEXT_LABEL_15 tl15
	wgGroup= START_WIDGET_GROUP( " FMMC Mission Creator")
		
		ADD_WIDGET_BOOL(" Add RockStar Blips", sFMMCendStage.sRocStarCreatedVars.bDisplayRockstarUGC)
		
		ADD_WIDGET_INT_SLIDER("Go to Menu", iDebugGotoMenuASAP, -1, ENUM_TO_INT(eFmmc_Null_item), 1)
		
		ADD_WIDGET_INT_READ_ONLY("iAttachparent", sVehStruct.iAttachParent)
		ADD_WIDGET_INT_READ_ONLY("iAttachParentType", sVehStruct.iAttachParentType)
		ADD_WIDGET_INT_READ_ONLY("Number of hidden props", g_FMMC_STRUCT_ENTITIES.iNumberOfWorldProps)
		ADD_WIDGET_INT_READ_ONLY("sFMMCmenu.iCurrentSelection", sFMMCmenu.iCurrentSelection)
		ADD_WIDGET_INT_READ_ONLY("sFMMCmenu.iPropColour", sFMMCmenu.iPropColour)
		ADD_WIDGET_INT_READ_ONLY("sFMMCmenu.iObjectColour", sFMMCmenu.iObjectColour)
		ADD_WIDGET_FLOAT_SLIDER("Rectangle offset", fRectStartX, 0, 1.0, 0.014)
		ADD_WIDGET_INT_SLIDER("iWeaponPallet", g_FMMC_STRUCT_ENTITIES.iWeaponPallet, -1, 100, 0)
		ADD_WIDGET_INT_SLIDER("iSwitchCam", iSwitchCam, -1, 100, 0)
		ADD_WIDGET_INT_SLIDER("sFMMCmenu.iSwitchCam", sFMMCmenu.iSwitchCam, -1, 100, 0)
		
		ADD_WIDGET_INT_READ_ONLY("Weapon selection", sWepStruct.iSelectedWeaponType)
		ADD_WIDGET_INT_READ_ONLY("Number of world props", g_FMMC_STRUCT_ENTITIES.iNumberOfWorldProps)
		ADD_WIDGET_BOOL("Quick restart", g_sFMMCEOM.bQuickRestart )
		ADD_WIDGET_INT_READ_ONLY("Race type", g_FMMC_STRUCT.iRaceType)
		ADD_WIDGET_BOOL("Disable Elevator Debug Drawing", bDisableElevatorDrawing)
		
		ADD_WIDGET_FLOAT_SLIDER("fMenuX", fMenuX, 0.0, 1.0, 0.01)
		ADD_WIDGET_FLOAT_SLIDER("fAddY", fAddY, 0.0, 1.0, 0.01)
		
		REPEAT ciMAX_NUM_OBJECTIVETEXT_RSG_OVERRIDES i
			tl15 = "otxt ovr"
			tl15 = i
			twObjectiveoverrides[i] = ADD_TEXT_WIDGET(tl15)
		ENDREPEAT

		CREATE_MC_SPAWN_GROUP_OVERRIDES()
		
		CREATE_CREATOR_RANDOM_CHECKPOINT_AS_SPAWN_OVERRIDE()
		
		START_WIDGET_GROUP("DIRTY DOOR DELETER")
			ADD_WIDGET_BOOL("Enable Dirty Door Deleter", bDirtyDoorDeleterEnabled)
			ADD_WIDGET_INT_SLIDER("Door To Delete", iDirtyDoorToDelete, 0, FMMC_MAX_NUM_DOORS, 1)
			ADD_WIDGET_BOOL("DELETE SELECTED DOOR", bDirtyDoorDelete)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Cross Mission Scripted Cutscene Copy")
			ADD_WIDGET_INT_SLIDER("Scripted Cutscene Index to Copy: ", iScriptedCutsceneIndexToCopy, -1, FMMC_GET_MAX_SCRIPTED_CUTSCENES(), 1)
			ADD_WIDGET_BOOL("Copy Cutscene To Globals", bScriptedCutsceneCopy)
			
			ADD_WIDGET_INT_SLIDER("Scripted Cutscene Index to Paste Into: ", iScriptedCutsceneIndexToPasteTo, 0, FMMC_GET_MAX_SCRIPTED_CUTSCENES(), 1)
			ADD_WIDGET_BOOL("Paste Cutscene From Globals", bScriptedCutscenePaste)
		
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("VisGroups")
			ADD_WIDGET_INT_SLIDER("Number of Markers Drawn", iMarkersDrawn, 0, 10000, 0)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("New Door System")
			ADD_WIDGET_INT_READ_ONLY("Stage", g_CreatorsSelDetails.iSwitchingINT)
			ADD_WIDGET_INT_READ_ONLY("Selected Door", sFMMCMenu.iSelectedEntity)
			ADD_WIDGET_INT_READ_ONLY("Highlighted Door", sFMMCMenu.iHighlightedEntity)
			REPEAT FMMC_MAX_NUM_DOORS i
				tl15 = "Door hash"
				tl15 += i
				ADD_WIDGET_INT_READ_ONLY(tl15, sCurrentVarsStruct.iMissionControlledDoors[i])
			ENDREPEAT
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Entity mission branching")
			ADD_WIDGET_INT_SLIDER("iAggroBranch", sFMMCmenu.iAggroBranch, -1, 100, 0)
			ADD_WIDGET_INT_SLIDER("iPassBranch", sFMMCmenu.iPassBranch, -1, 100, 0)
			ADD_WIDGET_INT_SLIDER("iFailBranch", sFMMCmenu.iFailBranch, -1, 100, 0)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Selection stack")
			ADD_WIDGET_INT_READ_ONLY("sFMMCmenu.iCurrentDepth", sFMMCmenu.iCurrentDepth)
			REPEAT COUNT_OF(sFMMCmenu.iReturnSelections) i
				tl15 = "["
				tl15 += i
				tl15 += "]"
				ADD_WIDGET_INT_READ_ONLY(tl15, sFMMCmenu.iReturnSelections[i])		
			ENDREPEAT
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("New Menu Debug")
			ADD_WIDGET_INT_SLIDER("Current Section", sFMMCmenu.iCurrentMenuSection, 0, 100, 0)
			ADD_WIDGET_INT_SLIDER("Current Menu Length", sFMMCmenu.iCurrentMenuLength, 0, 100, 0)
			START_WIDGET_GROUP("Selection stack")
				ADD_WIDGET_INT_READ_ONLY("sFMMCmenu.iCurrentDepth", sFMMCmenu.iCurrentDepth)
				REPEAT COUNT_OF(sFMMCmenu.iReturnSelections) i
					tl15 = "["
					tl15 += i
					tl15 += "]"
					ADD_WIDGET_INT_READ_ONLY(tl15, sFMMCmenu.iReturnSelections[i])		
				ENDREPEAT
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Vehicle Positions")
			FOR i = 0 TO FMMC_MAX_VEHICLES-1
				tl15 = "veh "
				tl15 += i
				tl15 += " position"
				ADD_WIDGET_VECTOR_SLIDER(tl15, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].vPos, 0, 10000, 0.0)
			ENDFOR
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Rocket Positions")
			ADD_WIDGET_INT_READ_ONLY("Number of Rocket Positions", g_FMMC_STRUCT_ENTITIES.iNumberOfRocketPositions)
			FOR i = 0 TO FMMC_MAX_NUM_ZONE_ROCKET_POS-1
				tl15 = "rocket "
				tl15 += i
				ADD_WIDGET_VECTOR_SLIDER(tl15, g_FMMC_STRUCT_ENTITIES.vFMMCZoneRocketPositions[i], 0, 10000, 0.0)
			ENDFOR
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Cutscene entities")
			FOR i = 0 TO FMMC_MAX_CUTSCENE_ENTITIES-1
				tl15 = "entity "
				tl15 += i
				START_WIDGET_GROUP(tl15)
					ADD_WIDGET_INT_READ_ONLY("Index", g_fmmc_struct.sCurrentSceneData.sCutsceneEntities[i].iIndex)
					ADD_WIDGET_INT_READ_ONLY("Type", g_fmmc_struct.sCurrentSceneData.sCutsceneEntities[i].iType)
				STOP_WIDGET_GROUP()
			ENDFOR
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Player rules")
			REPEAT FMMC_MAX_RULES i
				tl15 = "Rule "
				tl15 += i
				START_WIDGET_GROUP(tl15)
					REPEAT FMMC_MAX_TEAMS i2
						tl15 = "Team "
						tl15 += i2
						START_WIDGET_GROUP(tl15)
							ADD_WIDGET_INT_READ_ONLY("rule", 	g_FMMC_STRUCT.sPlayerRuleData[i].iRule[0])
							ADD_WIDGET_INT_READ_ONLY("Priority", g_FMMC_STRUCT.sPlayerRuleData[i].iPriority[0])	
							ADD_WIDGET_INT_READ_ONLY("Entity bit set", iSetPlayerrule[i][i2])
						STOP_WIDGET_GROUP()
					ENDREPEAT
				STOP_WIDGET_GROUP()
			ENDREPEAT
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Extra objective Entities")
			FOR i = 0 TO MAX_NUM_EXTRA_OBJECTIVE_ENTITIES-1
				tl15 = "Extra Entity "
				tl15 += i
				START_WIDGET_GROUP(tl15)
				ADD_WIDGET_INT_READ_ONLY("Entity ID", g_FMMC_STRUCT.iExtraObjectiveEntityID[i])
				ADD_WIDGET_INT_READ_ONLY("Entity Type", g_FMMC_STRUCT.iExtraObjectiveEntityType[i])
				FOR i2 = 0 TO MAX_NUM_EXTRA_OBJECTIVE_PER_ENTITY-1
					tl15 = "Priority["
					tl15 += i2 
					tl15 += "]"
					ADD_WIDGET_INT_READ_ONLY(tl15, 	g_FMMC_STRUCT.iExtraObjectivePriority[i][i2][0])
					tl15 = "Rule Type["
					tl15 += i2 
					tl15 += "]"
					ADD_WIDGET_INT_READ_ONLY(tl15, 	g_FMMC_STRUCT.iExtraObjectiveRule[i][i2][0])
				ENDFOR
				STOP_WIDGET_GROUP()
			ENDFOR
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("OUTFIT WIDGET")
			FOR i = 0 TO FMMC_MAX_TEAMS-1
				tl15 = "Team "
				tl15 += i+1
				tl15 += " outfits"
				START_WIDGET_GROUP(tl15)
				ADD_BIT_FIELD_WIDGET("bitset 1", g_FMMC_STRUCT.biOutFitsAvailableBitset[i][0])
				ADD_BIT_FIELD_WIDGET("bitset 2", g_FMMC_STRUCT.biOutFitsAvailableBitset[i][1])
				ADD_BIT_FIELD_WIDGET("bitset 3", g_FMMC_STRUCT.biOutFitsAvailableBitset[i][2])
				STOP_WIDGET_GROUP()
			ENDFOR
		STOP_WIDGET_GROUP()
		
		ADD_WIDGET_VECTOR_SLIDER("1st Drop off VECTOR", g_FMMC_STRUCT.sFMMCEndConditions[0].vDropOff[0], -9999.0, 9999.0,0.100 )			
		START_WIDGET_GROUP("START LOCATIONS")		
			ADD_WIDGET_VECTOR_SLIDER("TRIGGER VECTOR", g_FMMC_STRUCT.vStartPos, -9999.0, 9999.0,0.100 )		
			ADD_WIDGET_BOOL("Check Trigger Restriction", bDoTriggerRestricted)	
			FOR i = 0 TO FMMC_MAX_TEAMS - 1
				tl15 = "Team - "
				tl15 +=i	
				START_WIDGET_GROUP(tl15)
					ADD_WIDGET_BOOL("BEEN PLACED", g_FMMC_STRUCT.sFMMCEndConditions[i].bOkToDrawStartPosCorona)
					ADD_WIDGET_FLOAT_READ_ONLY("HEADING", g_FMMC_STRUCT.sFMMCEndConditions[i].fStartHeading)
					ADD_WIDGET_VECTOR_SLIDER("VECTOR", g_FMMC_STRUCT.sFMMCEndConditions[i].vStartPos, -9999.0, 9999.0,0.100 )
				STOP_WIDGET_GROUP()		
			ENDFOR
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("STATS AND SAVING")
			ADD_WIDGET_BOOL("Made a change", sCurrentVarsStruct.creationStats.bMadeAChange)
			ADD_WIDGET_INT_SLIDER("Times tested", sCurrentVarsStruct.creationStats.iTimesTestedLoc, 0, 50, 1)
			ADD_WIDGET_BOOL("Editing a Saved Creation", sCurrentVarsStruct.creationStats.bEditingACreation)
			ADD_WIDGET_BOOL("Previously Published", g_FMMC_STRUCT.bMissionIsPublished)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("COVER POINTS")
			ADD_WIDGET_FLOAT_SLIDER("Arrow X Offset", sCoverStruct.xOffset, -2, 2, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("Arrow X Offset", sCoverStruct.yOffset, -2, 2, 0.1)
			
			ADD_WIDGET_FLOAT_SLIDER("Arrow X Rot", sCoverStruct.xRot, -180, 180, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("Arrow Y Rot", sCoverStruct.yRot, -180, 180, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("Arrow Z Rot", sCoverStruct.zRot, -180, 180, 0.1)
			
			ADD_WIDGET_FLOAT_SLIDER("Arrow X Dir", sCoverStruct.xDir, -180, 180, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("Arrow Y Dir", sCoverStruct.yDir, -180, 180, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("Arrow Z Dir", sCoverStruct.zDir, -180, 180, 0.1)
			
		STOP_WIDGET_GROUP()

		START_WIDGET_GROUP( "iAreaRetained") 
			ADD_WIDGET_INT_READ_ONLY("iAreaRetained 0", g_FMMC_STRUCT.sFMMCEndConditions[0].iAreaRetained)
			ADD_WIDGET_INT_READ_ONLY("iAreaRetained 1", g_FMMC_STRUCT.sFMMCEndConditions[1].iAreaRetained)
			ADD_WIDGET_INT_READ_ONLY("iAreaRetained 2", g_FMMC_STRUCT.sFMMCEndConditions[2].iAreaRetained)
			ADD_WIDGET_INT_READ_ONLY("iAreaRetained 3", g_FMMC_STRUCT.sFMMCEndConditions[3].iAreaRetained)
			FOR i = 0 TO FMMC_MAX_TEAMS - 1
				tl15 = "Team - "
				tl15 +=i	
				START_WIDGET_GROUP(tl15)
					FOR i2 = 0 TO FMMC_MAX_GANG_HIDE_LOCATIONS - 1
						tl15 = "Loc - "
						tl15 +=i2	
						ADD_WIDGET_BOOL(tl15, bLocBitSet[i][i2])
					ENDFOR	
				STOP_WIDGET_GROUP()		
			ENDFOR	
			
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP( "Time Bit Set") 
			ADD_BIT_FIELD_WIDGET("Available times", g_FMMC_STRUCT.iLaunchTimesBit)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP( "   NEW UGC") 
			ADD_WIDGET_BOOL(".bGetSpPlaylist", bGetSpPlaylist)
			
			START_WIDGET_GROUP( "    GET_UGC_CONTENT_BY_TYPE") 
			
				ADD_WIDGET_BOOL(".bGetMyUGC", bGetMyUGC)
				ADD_WIDGET_INT_SLIDER("GET Type ", iContentType, 0, 30, 1)
				ADD_WIDGET_INT_SLIDER("CATEGORY Type", iCATEGORYtype, 0, 30, 1)
				ADD_WIDGET_INT_SLIDER("UGC Type", iUGCtype, 0, 30, 1)
			STOP_WIDGET_GROUP()
			
			ADD_WIDGET_BOOL(".bLoadOldData", bLoadOldData)
			ADD_WIDGET_BOOL(".bGetAChallenge", bGetAChallenge)
			ADD_WIDGET_BOOL(".bGetChallenges", bGetChallenges)
			ADD_WIDGET_BOOL("bEndCreator", bEndCreator )
			ADD_WIDGET_BOOL("bGetMyPlaylists", bGetMyPlaylists )
			ADD_WIDGET_BOOL("..SAVE_MISSION_TO_UGC_SERVER", bGetMyPlaylistsDetails )
			ADD_WIDGET_INT_SLIDER("iPlayList ", iPlayList, 0, 30, 1)
			
			START_WIDGET_GROUP( " UGC_GET_GET_BY_CONTENT_ID") 
				twID = ADD_TEXT_WIDGET("")
				ADD_WIDGET_BOOL("Get it", bGetUGC_byIDWithString )
				ADD_WIDGET_BOOL("From another user", bLoadFromOtherUaer )
				
			STOP_WIDGET_GROUP()
			
			ADD_WIDGET_INT_SLIDER("iPublishedSelection1 ", iPublishedSelection1, -1, 200, 1)
			
			ADD_WIDGET_BOOL("bSkipName", sFMMCendStage.bSkipName)
			ADD_WIDGET_BOOL("  bGetUGC_byID", bGetUGC_byID )
			
			ADD_WIDGET_BOOL(" b948049Repo", b948049Repo )
			ADD_WIDGET_BOOL(" bDelete1", bDelete1 )
			ADD_WIDGET_BOOL(" bUse_GET_GET_BY_CONTENT_ID", bUse_GET_GET_BY_CONTENT_ID )
			ADD_WIDGET_BOOL(" bGetRockstarCandidateUGC", bGetRockstarCandidateUGC )
			ADD_WIDGET_BOOL(" bGetMyUGC", bGetMyUGC )
			ADD_WIDGET_BOOL(" bGetHeader1", bGetHeader1 )
			
			ADD_WIDGET_BOOL("bSaveToCloud", bSaveToCloud )
			ADD_WIDGET_BOOL("Call DATAFILE_PUBLISH_UGC every frame", bCallDATAFILE_PUBLISH_UGC ) 
			ADD_WIDGET_INT_SLIDER("iMenuReturn ", sFMMCendStage.iMenuReturn, 0, 30, 1)
		STOP_WIDGET_GROUP()
		
		ADD_WIDGET_INT_READ_ONLY("Number Go To Locs", g_FMMC_STRUCT_ENTITIES.iNumberOfGoToLocations[0])
		ADD_WIDGET_BOOL("Move Header To New Location", bReLoadHeaderData )
		ADD_WIDGET_BOOL("bMoveHeaderCross ", sFMMCendStage.headerDataStruct.bMoveHeaderCross)

	
		START_WIDGET_GROUP( " FMMC Mission Creator") 
			ADD_WIDGET_BOOL("bSelectionActive", g_CreatorsSelDetails.bSelectionActive)
			ADD_WIDGET_INT_SLIDER("iStartSpace ", g_CreatorsSelDetails.iStartSpace, -1, 10, 1)
			ADD_WIDGET_INT_SLIDER("iTotalSpace ", g_CreatorsSelDetails.iTotalSpace, -1, 10, 1)
			ADD_WIDGET_INT_SLIDER("iNumberOfWords ", g_CreatorsSelDetails.iNumberOfWords, -1, 10, 1)
		STOP_WIDGET_GROUP()
		
		ADD_WIDGET_INT_SLIDER("iRadarZoom ", iRadarZoom, 0, 1100, 1)
		ADD_WIDGET_INT_SLIDER("tiCOMMON_MENU_BG_ALPHA ", tiCOMMON_MENU_BG_ALPHA, 0, 255, 1)
		ADD_WIDGET_BOOL("FORCE RAMPAGE", bFORCE_RAMPAGE)
		
		ADD_WIDGET_INT_SLIDER("iSelectedTab ", sFMMCmenu.iSelectedTab , 0, 1, 1)
		
		START_WIDGET_GROUP( "VehicleRules")
			FOR i = 0 TO 19
				tl15 = "VEH - "
				tl15 +=i
				START_WIDGET_GROUP(tl15)
					ADD_WIDGET_INT_SLIDER("iPriority ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPriority[0] , -1, 9999999, 1)
					ADD_WIDGET_INT_SLIDER("iRule ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iRule[0] , -1, 9999999, 1)
				STOP_WIDGET_GROUP()				
			ENDFOR
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Go To Locs")
			FOR i = 0 TO 9
				tl15 = "Go To - "
				tl15 +=i
				START_WIDGET_GROUP(tl15)
					ADD_WIDGET_VECTOR_SLIDER("Vector", g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].vLoc[0], -9999.0, 9999.0,0.100 )
					ADD_WIDGET_BOOL("BLIP EXISTS", bGoToBlipExists[i])
				STOP_WIDGET_GROUP()				
			ENDFOR
		STOP_WIDGET_GROUP()
				
		ADD_WIDGET_INT_SLIDER("iMaxNumberOfTeams ", g_FMMC_STRUCT.iMaxNumberOfTeams , -1, 99, 1)
		ADD_WIDGET_INT_SLIDER("iEditingEntityNum ", sFMMCMenu.iSelectedEntity , -1, 99, 1)
		ADD_WIDGET_INT_SLIDER("sFMMCmenu.iEntityCreation ", sFMMCmenu.iEntityCreation , -1, 99, 1)
		ADD_WIDGET_INT_SLIDER("Cloud Load Version", g_FMMC_STRUCT.iVersion, -1, 99, 1)
		ADD_WIDGET_INT_SLIDER("Cloud Load Folder", g_FMMC_STRUCT.iFolder, 0, 99, 1)
		ADD_WIDGET_STRING("Entity Numbers")
		ADD_WIDGET_INT_READ_ONLY("MIN ON TEAM 1", g_FMMC_STRUCT.iNumPlayersPerTeam[0])
		ADD_WIDGET_INT_READ_ONLY("MIN ON TEAM 2", g_FMMC_STRUCT.iNumPlayersPerTeam[1])
		ADD_WIDGET_INT_READ_ONLY("MIN ON TEAM 3", g_FMMC_STRUCT.iNumPlayersPerTeam[2])
		ADD_WIDGET_INT_READ_ONLY("MIN ON TEAM 4", g_FMMC_STRUCT.iNumPlayersPerTeam[3])
		ADD_WIDGET_INT_SLIDER("iNumberOfPeds", g_FMMC_STRUCT_ENTITIES.iNumberOfPeds, 0, 99, 1)
		ADD_WIDGET_INT_SLIDER("iNumberOfVehicles", g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles, 0, 99, 1)
		ADD_WIDGET_INT_SLIDER("iNumberOfWeapons", g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons, 0, 99, 1)
		
		START_WIDGET_GROUP( "XML")
			ADD_WIDGET_BOOL("Save Xml",sCurrentVarsStruct.bSaveXml)
			ADD_WIDGET_BOOL("Load XML",bLoadXML)
			ADD_WIDGET_INT_SLIDER("Xml To Save", iXmlToSave, 0, 31, 1)
			ADD_WIDGET_INT_SLIDER("Xml To Load", iXmlToLoad, 0, 31, 1)
		STOP_WIDGET_GROUP()
		
		ADD_WIDGET_STRING("Switch Statement States")
		ADD_WIDGET_INT_SLIDER("MENU STATE", sCurrentVarsStruct.iMenuState, -1, 99, 1)
		ADD_WIDGET_INT_SLIDER("sFMMCmenu.iEntityCreation,", sFMMCmenu.iEntityCreation, -1, 99, 1)
		ADD_WIDGET_INT_SLIDER("sCurrentVarsStruct.iEntityCreationStatus,", sCurrentVarsStruct.iEntityCreationStatus, -1, 500, 1)
		
		ADD_WIDGET_STRING("Entity Creation Switch Statements")
		ADD_WIDGET_INT_SLIDER("sPedStruct.iSwitchingINT", sPedStruct.iSwitchingINT, 0, 99, 1)
		ADD_WIDGET_INT_SLIDER("sWepStruct.iSwitchingINT", sWepStruct.iSwitchingINT, 0, 99, 1)
		ADD_WIDGET_INT_SLIDER("sVehStruct.iSwitchingINT", sVehStruct.iSwitchingINT, 0, 99, 1)
		ADD_WIDGET_INT_SLIDER("sObjStruct.iSwitchingINT", sObjStruct.iSwitchingINT, 0, 99, 1)
		ADD_WIDGET_INT_SLIDER("g_CreatorsSelDetails.iSwitchingINT", g_CreatorsSelDetails.iSwitchingINT, 0, 99, 1)
		
		SET_UP_FMMC_GLOBAL_WIDGETS()
		
		START_WIDGET_GROUP( "Everything else")  
			ADD_WIDGET_FLOAT_SLIDER("sCurrentVarsStruct.fCheckPointSize", sCurrentVarsStruct.fCheckPointSize, 0, 30, 0.1)
			ADD_WIDGET_INT_SLIDER("sPedStruct.iCurrentRelGroupSelection[0]", sPedStruct.iCurrentRelGroupSelection[0], 0, 99, 1)
			ADD_WIDGET_INT_SLIDER("sPedStruct.iCurrentRelGroupSelection[1]", sPedStruct.iCurrentRelGroupSelection[1], 0, 99, 1)	
			ADD_WIDGET_INT_SLIDER("sPedStruct.iCurrentRelGroupSelection[2]", sPedStruct.iCurrentRelGroupSelection[2], 0, 99, 1)	
			ADD_WIDGET_INT_SLIDER("sPedStruct.iCurrentRelGroupSelection[3]", sPedStruct.iCurrentRelGroupSelection[3], 0, 99, 1)			
			ADD_WIDGET_INT_READ_ONLY("Contact Character",g_FMMC_STRUCT.iContactCharEnum)	
			ADD_WIDGET_INT_SLIDER("sFMMCmenu.iCurrentEntitySelection[CREATION_TYPE_PEDS]", sFMMCmenu.iCurrentEntitySelection[CREATION_TYPE_PEDS], 0, 99, 1)
			ADD_WIDGET_INT_SLIDER("sFMMCmenu.iCurrentEntitySelection[CREATION_TYPE_WEAPONS]", sFMMCmenu.iCurrentEntitySelection[CREATION_TYPE_WEAPONS], 0, 99, 1)
			ADD_WIDGET_INT_SLIDER("sFMMCmenu.iCurrentEntitySelection[CREATION_TYPE_VEHICLES]", sFMMCmenu.iCurrentEntitySelection[CREATION_TYPE_VEHICLES], 0, 99, 1)
			ADD_WIDGET_INT_SLIDER("sFMMCmenu.iCurrentEntitySelection[CREATION_TYPE_OBJECTS]", sFMMCmenu.iCurrentEntitySelection[CREATION_TYPE_OBJECTS], 0, 99, 1)
			ADD_WIDGET_INT_SLIDER("g_FMMC_STRUCT.iNumberOfCheckPoints", g_FMMC_STRUCT.iNumberOfCheckPoints, 0, 99, 1)
			ADD_WIDGET_INT_SLIDER("sFMMCmenu.iSelectedTeam", sFMMCmenu.iSelectedTeam, 0, 99, 0)
			ADD_WIDGET_FLOAT_SLIDER("g_FMMC_STRUCT.fGridLength", g_FMMC_STRUCT.fGridLength, 0, 9999, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("g_FMMC_STRUCT.fGridWidth", g_FMMC_STRUCT.fGridWidth, 0, 9999, 0.01)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP( "Entity Debug")  
			ADD_WIDGET_BOOL("Display Spawn Rule", sCurrentVarsStruct.bDrawEntitySpawn)
			ADD_WIDGET_BOOL("Display Cleanup Rule", sCurrentVarsStruct.bDrawEntityCleanup)
		STOP_WIDGET_GROUP()

		START_WIDGET_GROUP( "g_FMMC_STRUCT Check point coordinates") 
			START_WIDGET_GROUP( "vCheckPoint") 
				FOR i = 0 TO (GET_FMMC_MAX_NUM_CHECKPOINTS() - 1)
					tl15 = "vCheckPoint"
					tl15 += i
					ADD_WIDGET_VECTOR_SLIDER(tl15, g_FMMC_STRUCT.sPlacedCheckpoint[i].vCheckPoint, -9999.0, 9999.0,0.100 )
				ENDFOR
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
		
		#IF SCRIPT_PROFILER_ACTIVE
		CREATE_SCRIPT_PROFILER_WIDGET()
		#ENDIF
		
		
		ADD_WIDGET_INT_READ_ONLY("photo stage", sFMMCendStage.sTakePhotoVars.iTakePhotoStage)
	
		START_WIDGET_GROUP("Export") 
			ADD_WIDGET_BOOL("Export for FM Mission", g_bExportCreatorEntityDetail)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("    RHM Tweaking")
			ADD_WIDGET_BOOL("Set menu scale", bUsingRHMScaleWidget)
			ADD_WIDGET_FLOAT_SLIDER("RHM_EXTRA_MENU_WIDTH", RHM_EXTRA_MENU_WIDTH, 0.0, 0.26, 0.01)
			ADD_WIDGET_FLOAT_READ_ONLY("MAIN_WIDTH_RIGHT_MENU", MAIN_WIDTH_RIGHT_MENU)
			ADD_WIDGET_FLOAT_READ_ONLY("MAIN_X_POS", MAIN_X_POS)
			ADD_WIDGET_FLOAT_READ_ONLY("MAIN_WIDTH", MAIN_WIDTH)
			ADD_WIDGET_FLOAT_READ_ONLY("SELECTION_BG_HEIGHT", SELECTION_BG_HEIGHT)
			ADD_WIDGET_FLOAT_READ_ONLY("SELECTION_BG_START", SELECTION_BG_START)
			ADD_WIDGET_FLOAT_READ_ONLY("TEXT_START_Y", TEXT_START_Y)
			ADD_WIDGET_FLOAT_READ_ONLY("TEXT_X", TEXT_X)
			ADD_WIDGET_FLOAT_READ_ONLY("TEXT_X_COL_WIDTH", TEXT_X_COL_WIDTH)
			
		STOP_WIDGET_GROUP()
		
	STOP_WIDGET_GROUP()
ENDPROC

PROC UPDATE_WIDGETS()
	INT i, i2
	FOR i = 0 TO 9
		IF DOES_BLIP_EXIST(sLocStruct.biGoToBlips[i])	
			bGoToBlipExists[i] = TRUE	
		ELSE 
			bGoToBlipExists[i] = FALSE
		ENDIF
	ENDFOR
	REPEAT ciMAX_NUM_OBJECTIVETEXT_RSG_OVERRIDES i
		SET_CONTENTS_OF_TEXT_WIDGET(twObjectiveoverrides[i], g_FMMC_STRUCT.tl63ObjectiveText_SpawnGroupOverride[i])
	ENDREPEAT
	REPEAT FMMC_MAX_RULES i 
		REPEAT FMMC_MAX_TEAMS i2
			iSetPlayerrule[i][i2] = GET_RULE_FOR_PLAYER_RULE_FROM_ENTITY_BITSET(i, i2)
		ENDREPEAT
	ENDREPEAT
	
	IF bCHUpdateGlobals
		
		g_sCasinoHeistMissionConfigData.eChosenApproachType = INT_TO_ENUM(CASINO_HEIST_APPROACH_TYPE, iCHApproach)
		
		g_sCasinoHeistMissionConfigData.eCrewWeaponsExpertChosen = INT_TO_ENUM(CASINO_HEIST_WEAPON_EXPERTS, iCHWeapons)
		IF bCHAltWep
			g_sCasinoHeistMissionConfigData.eCrewWeaponsLoadoutChosen = CASINO_HEIST_WEAPON_LOADOUT__ALTERNATE
		ELSE	
			g_sCasinoHeistMissionConfigData.eCrewWeaponsLoadoutChosen = CASINO_HEIST_WEAPON_LOADOUT__DEFAULT
		ENDIF
		
		g_sCasinoHeistMissionConfigData.eCrewDriverChosen = INT_TO_ENUM(CASINO_HEIST_DRIVERS, iCHVehicle)
		IF bCHAltVeh
			g_sCasinoHeistMissionConfigData.eCrewVehiclesLoadoutChosen = CASINO_HEIST_VEHICLE_SELECTION__ALTERNATE
		ELSE	
			g_sCasinoHeistMissionConfigData.eCrewVehiclesLoadoutChosen = CASINO_HEIST_VEHICLE_SELECTION__DEFAULT
		ENDIF
		
		g_sCasinoHeistMissionConfigData.eCrewHackerChosen = INT_TO_ENUM(CASINO_HEIST_HACKERS, iCHHacker)
		
		bCHUpdateGlobals = FALSE	
	ENDIF
	
	IF bCHClearGlobals
		CASINO_HEIST_MISSION_CONFIGURATION_DATA sClear_Casinoheist
		g_sCasinoHeistMissionConfigData = sClear_Casinoheist
		
		iCHWeapons = -1
		bCHAltWep = FALSE
		
		iCHVehicle = -1
		bCHAltVeh = FALSE
		
		iCHHacker = -1
		
		bCHClearGlobals = FALSE
	ENDIF
ENDPROC
#ENDIF
