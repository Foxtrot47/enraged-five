#IF IS_DEBUG_BUILD

USING "rage_builtins.sch"
USING "globals.sch"
USING "script_player.sch"
USING "commands_pad.sch"
USING "FMMC_header.sch"
USING "FMMC_keyboard.sch"
USING "net_objective_text.sch"
USING "PM_MissionCreator_Public.sch"
USING "FM_Post_Mission_Cleanup.sch"
USING "net_private_yacht_private.sch"
USING "Freemode_Content_Generics\fm_content_generic_load_data.sch"

USING "hud_creator_tool.sch"
USING "profiler.sch"
USING "select_mission_stage.sch"
USING "FMMC_Launcher_Menu.sch"

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section: RHM Drawing ---------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Description: Variables used to draw the right hand menu --------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FLOAT TEXT_START_Y				=	cfTEXT_START_Y	//Y value to start text from
FLOAT TEXT_X 					=	cfTEXT_X	//X value to start first part text from
FLOAT TEXT_X_COL_WIDTH			=	cfTEXT_X_COL_WIDTH	//X value to start second part of text from, added to TEXT_X
FLOAT MAIN_WIDTH_RIGHT_MENU 	=	cfMAIN_WIDTH_RIGHT_MENU
FLOAT MAIN_X_POS 				=	cfMAIN_X_POS	//Starting point screen space
FLOAT MAIN_WIDTH 				=	cfMAIN_WIDTH	//Width of menu, from centre
FLOAT SELECTION_BG_HEIGHT 		=	cfSELECTION_BG_HEIGHT	//Height of box around entity selection
FLOAT SELECTION_BG_START		= 	cfSELECTION_BG_START	//Y Value to start selection from
FLOAT RHM_EXTRA_MENU_WIDTH		= 0.0

FLOAT fMenuScaleSmooth

TYPEDEF FUNC STRING GET_RHM_OPTIONS_MENU_STRING(INT iMenuItem)

BOOL bUsingRHMScaleWidget

sHudStatValues sModValues
sHudStatValues sWeaponValues

// Bit Fields
INT iLocalBitSet
INT iHelpBitSetOld 								= - 1
INT iHelpBitSet
INT iTestFailBitSet								= 0
INT iVehicleModelLoadTimers[FMMC_MAX_VEHICLES]
INT iPropModelTimers[FMMC_MAX_NUM_PROPS]

// iHelpBitSet Values
CONST_INT biPickupEntityButton 						0
CONST_INT biPickupCoronaButton						3
CONST_INT biRotateCoronaButton						4
CONST_INT biDeleteEntityButton						5
CONST_INT biWarpToCameraButton 						7
CONST_INT biWarpToCoronaButton 						8
CONST_INT biRemovePedsButton						9
CONST_INT biRemoveObjectsButton						10
CONST_INT biRemoveVehButton							11
CONST_INT biVectorInputTextButton					12
CONST_INT biEditDataInputTextButton					13

// Trigger Stuff
INT iTriggerCreationStage = CREATION_STAGE_WAIT

// Photo and Lobby Cameras
SCALEFORM_INDEX SF_Movie_Gallery_Shutter_Index
INT iIntroCamPlacementStage
VECTOR vInitialPreviewPos
INT iCamPanState = PAN_CAM_SETUP
PREVIEW_PHOTO_STRUCT PPS
BLIP_INDEX bCameraTriggerBlip
BLIP_INDEX bCameraPanBlip
BLIP_INDEX bPhotoBlip
BLIP_INDEX bTeamCamPanBlip[FMMC_MAX_TEAMS]
BLIP_INDEX bCameraOutroBlip
BLIP_INDEX bRocketPositionBlips[FMMC_MAX_NUM_ZONE_ROCKET_POS]

GET_UGC_CONTENT_STRUCT sGetUGC_content
JOB_INTRO_CUT_DATA jobIntroData
BOOL bInitialIntroCamSetup = TRUE
BOOL bInitialIntroCamWarp = FALSE
BOOL bMultiplayerMapSet = FALSE

INTERIOR_INSTANCE_INDEX interiorCopsOld
//INTERIOR_INSTANCE_INDEX interiorWarp[MAX_BUILDING_WARPS]
//INT iCurrentWarpInterior = -1
INTERIOR_INSTANCE_INDEX interiorArena_VIPLounge
VECTOR vArenaCamCoords
//cutscene creation vars
CAMERA_INDEX camCutscenePlacement
OBJECT_INDEX objCutscenePlacement
INT	iCutsceneCamStage = 0
INT iCamMovementSpeed = 1
INT iCamTurnRate = 1
BOOL bOldinCopsInteriorDisabled = FALSE
INT iWarpState
INT iWarpStateTimeoutStart = 0
INT iInteriorInitializedBS
INT iInteriorInitializingExtrasBS[INTERIOR_BITSETS]

INTERIOR_INSTANCE_INDEX interiorCasinoVaultIndex = NULL
INTERIOR_INSTANCE_INDEX interiorCasinoApartIndex = NULL
INTERIOR_INSTANCE_INDEX interiorWeedFarm = NULL
INTERIOR_INSTANCE_INDEX interiorCokeFactory = NULL
INTERIOR_INSTANCE_INDEX interiorMusicStudio = NULL
INTERIOR_INSTANCE_INDEX interiorMissionBasement = NULL

//Factory
MISSION_FACTORIES sMissionFactories

//Yacht
FMMC_YACHT_VARS sMissionYachtVars[ciYACHT_MAX_YACHTS]
YACHT_DATA sMissionYachtData[ciYACHT_MAX_YACHTS]
YACHT_DATA YachtData

// Mission MOC
MISSION_MOC_PROPS sMissionMOCProps

// Test
BOOL bTestModeControllerScriptStarted
mission_display_struct on_mission_gang_box
BOOL bTextSetUp

/////////////////////////////////////////////
//THE MENU STRUCT
PLAYER_RULE_LOCAL_VARS sPlayerKills

INT iSelectCoolOffTimer
int iTopItem
BOOL bContentReadyForUGC = FALSE
BOOL bCreatorLimitedCloudDown = FALSE
BOOL bSignedOut = FALSE

BLIP_INDEX biDoors[FMMC_MAX_NUM_DOORS]

BLIP_INDEX biWarpPortalStart[FMMC_MAX_WARP_PORTALS]

BLIP_INDEX biPlacedPTFX[FMMC_MAX_PLACED_PTFX]
BLIP_INDEX biPlacedMarker[FMMC_MAX_PLACED_MARKERS]
BLIP_INDEX biElevators[FMMC_MAX_ELEVATORS]

BOOL bDelayAFrame = TRUE

BOOL bTriggerText = FALSE

VECTOR vCarrierPosition			= <<3048.3081, -4658.6313, 29.5800>>
VECTOR vYachtNearPierPosition	= <<-2032, -1035, 5>>
VECTOR vFortZancudoGates		= <<-1613.29, 2815.12, 33>>
VECTOR vYachtNearChumash		= <<-3271.7927, -295.8148, 65.2440>>
VECTOR vMichaelsHouse			= <<-811.2679, 179.3344, 75.7408>>
VECTOR vFranklinsHouse			= <<7.0256, 537.3075, 175.0281>>
VECTOR vOpenGravePosition		= <<-282.54, 2837.5, 53>>
VECTOR vTequiLaLaLocation		= <<-555.5934, 285.7738, 81.1763>>
VECTOR vStripClubLocation		= <<119.6, -1286.6, 29.3>>
VECTOR vChickenFactoryLocation	= <<-76.6618, 6222.1914, 32.2412>>
VECTOR vYachtAquaris			= <<-777.487, 6566.91, 100.0>>
VECTOR vYachtPaleto				= <<-1392.7, 6744.0, 100.0>>
VECTOR vSilo					= <<565.7, 5554.6, 767>>
VECTOR vIAAFaciltyDoor 			= <<2049.1, 2950.1,47.1>>
VECTOR vCasinoAirCon 			= <<939.9, 6.4, 120.1>>
VECTOR vCasinoExtCameras		= <<908.3, 35.9, 127.4>>
VECTOR vBunkerDoors				= <<52.3, 2933.3, 55.4>>
VECTOR vLostMCClubHouse			= <<983.2747, -99.3188, 73.8454>>

INT iIPLLoadBitSet

CONST_INT ciIPLBS_YachtAquarisAssetsRequested		0
CONST_INT ciIPLBS_YachtAquarisAssetsLoaded			1
CONST_INT ciIPLBS_IslandLoaded						2

FLOAT fMenuX										= 0.245
FLOAT fAddY											= 0.015
FLOAT fStuntCamZ 									= 0.0

FLOAT fRectStartX									= 0.290

//INT iStuntJumpCreationType
INT iSelectedTabOld

INT iMarkersDrawn = 0

CONST_INT MAX_OBJECTIVE_TEXT_LENGTH					63-12 // Reserve characters for colouring e.g. "~r~~s~~y~~s~"

ENUM RESET_STATE 
	RESET_STATE_FADE,
	RESET_STATE_CLEAR,
	RESET_STATE_INTERIORS,
	RESET_STATE_WEAPONS,
	RESET_STATE_VEHICLES,
	RESET_STATE_PROPS,
	RESET_STATE_DYNOPROPS,
	RESET_STATE_OBJECTS,
	RESET_STATE_CRATES,
	RESET_STATE_PEDS,
	RESET_STATE_ATTACH,
	RESET_STATE_TEAM_SPAWNS,
	RESET_STATE_OTHER,
	RESET_STATE_FINISH
ENDENUM
RESET_STATE iResetState

STRUCT_REL_GROUP_HASH 					sRGH
FMMC_LOCAL_STRUCT 						sFMMCdata
FMMC_CAM_DATA 							sCamData
PED_CREATION_STRUCT 					sPedStruct
TEAM_SPAWN_CREATION_STRUCT				sTeamSpawnStruct[FMMC_MAX_TEAMS]
VEHICLE_CREATION_STRUCT					sVehStruct
WEAPON_CREATION_STRUCT					sWepStruct
OBJECT_CREATION_STRUCT					sObjStruct
PROP_CREATION_STRUCT					sPropStruct
DYNOPROP_CREATION_STRUCT				sDynoPropStruct
INTERACTABLE_CREATION_STRUCT			sInteractableCreationStruct
TRAIN_CREATION_STRUCT					sTrainCreationStruct
INVISIBLE_OBJECT_STRUCT					sInvisibleObjects
LOCATION_CREATION_STRUCT				sLocStruct
STUNT_JUMP_CREATION_STRUCT				sStuntJumpStruct
COVER_CREATION_STRUCT					sCoverStruct
ENTITY_PREVIEW_STRUCT					sEntityPreviewStruct
FMMC_UP_DOWN_STRUCT						menuScrollController
HUD_COLOURS_STRUCT 						sHCS
CREATION_VARS_STRUCT 					sCurrentVarsStruct
structFMMC_MENU_ITEMS 					sFMMCmenu
START_END_BLIPS							sStartEndBlips
FMMC_COMMON_MENUS_VARS 					sFMMCendStage
LOCAL_LOAD_SAVE_STRUCT					sLocalLoadSaveStruct
DEV_ASSIST_CREATION_STRUCT				sDevAssistCreationStruct
UNIT_CREATION_STRUCT					sUnitStruct

BOOL ButtonPressed

INT iMaxSpawnsPerTeam[FMMC_MAX_TEAMS]

INT iMethLabInstanceWallTintsCache[ciMETH_LAB_INSTANCE_MAX]

BLIMP_SIGN sBlimpSign

//variables for the mission creator tutorial //////////////
BOOL bshowmenu = TRUE
BOOL bshowRightmenu = FALSE
BOOL bMpNeedsCleanedUp
BOOL bMpModeCleanedUp

FLOAT fGangAreaMinHeight
FLOAT fGangAreaMaxHeight

INT iSwitchCam = 0
VECTOR vSwitchVec = <<0,0,0>>
FLOAT fSwitchHeading = 0
BOOL bSwitchingCam = FALSE

INT iDoorSetupStage
INT iDoorSlowLoop

INT iChangeValueStep

INT iCHWeapons, iCHVehicle, iCHHacker, iCHApproach
BOOL bCHAltWep, bCHAltVeh, bCHUpdateGlobals, bCHClearGlobals

VEHICLE_INDEX vehTripSkip 

BOOL bGoToBlipExists[10]

SAVE_OUT_UGC_PLAYER_DATA_VARS sSaveOutVars

BOOL bDisableElevatorDrawing

BOOL bDirtyDoorDeleterEnabled
INT iDirtyDoorToDelete
BOOL bDirtyDoorDelete

INT iScriptedCutsceneIndexToCopy = -1
INT iScriptedCutsceneIndexToPasteTo = 0
BOOL bScriptedCutsceneCopy = FALSE
BOOL bScriptedCutscenePaste = FALSE

WIDGET_GROUP_ID wgGroup
BOOL bLoadXML, bFORCE_RAMPAGE
INT iXmlToLoad, iXmlToSave, iRadarZoom

BOOL bReLoadHeaderData, bGetUGC_byIDWithString, bLoadFromOtherUaer

BOOL bGetMyUGC, bSaveToCloud, b948049Repo, bGetChallenges, bGetSpPlaylist, bGetAChallenge, bUse_GET_GET_BY_CONTENT_ID, bCallDATAFILE_PUBLISH_UGC, bGetHeader1, bEndCreator, 	bLoadOldData, bGetRockstarCandidateUGC, bDelete1, bGetUGC_byID,/* bUSE_NEW_UGC_SYSTEM_Local, */bGetMyPlaylists,	bGetMyPlaylistsDetails
BOOL bDealWithNewSave, bPublishMissionNew, bVersionMissionNew, bOverWriteMissionNew, bCopyContentByID
BOOL bDoTriggerRestricted = TRUE
STRUCT_DATA_FILE dataStruct
BOOL bForcedSpawnGroups

INT iContentType, iPlayList, iCATEGORYtype, iUGCtype
INT iPublishedSelection1 = -1
BOOL bLocBitSet[FMMC_MAX_TEAMS][FMMC_MAX_GANG_HIDE_LOCATIONS]

INT iSetPlayerrule[FMMC_MAX_RULES][FMMC_MAX_TEAMS]

TEXT_WIDGET_ID twID, twObjectiveoverrides[ciMAX_NUM_OBJECTIVETEXT_RSG_OVERRIDES]

INT iDebugGotoMenuASAP = -1

BOOL bIsMaleSPTC
BOOL bIsCreatorModelSetSPTC

BOOL bBoolTempClear
BOOL bBoolTemp[FMMC_MAX_RANDOM_ENTITY_SPAWN_GROUPS]
BOOL bBoolSubTemp[FMMC_MAX_RANDOM_ENTITY_SPAWN_GROUPS][ciMAX_NUM_FMMC_ENTITY_SUB_SPAWN_GROUPS]

ENUM eTESTING_MC_MISSION_MENU_STATE
	eTESTINGMCMISSIONMENUSTATE_DISPLAY_HELP = 0,
	eTESTINGMCMISSIONMENUSTATE_SETUP_MENU,
	eTESTINGMCMISSIONMENUSTATE_SHOW_MENU
ENDENUM

INT iPropListsCacheStaggeredLoop = 0

STRUCT STRUCT_TEST_MENU_DATA
	eTESTING_MC_MISSION_MENU_STATE eTestMcMissionMenuState
	BOOL bDisplayedHelp
ENDSTRUCT
STRUCT_TEST_MENU_DATA structTestMcMissionMenuData

INT iHighPriorityPedsCount

VECTOR vSavedLoc[FMMC_MAX_GO_TO_LOCATIONS][FMMC_MAX_TEAMS]
VECTOR vSavedLoc1[FMMC_MAX_GO_TO_LOCATIONS]
VECTOR vSavedLoc2[FMMC_MAX_GO_TO_LOCATIONS]
FLOAT fSavedHeading[FMMC_MAX_GO_TO_LOCATIONS][FMMC_MAX_TEAMS]

INT iCreationTypeLastFrame = -1

#ENDIF
