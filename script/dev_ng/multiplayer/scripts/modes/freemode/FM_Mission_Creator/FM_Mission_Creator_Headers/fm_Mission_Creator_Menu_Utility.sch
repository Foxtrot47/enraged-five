#IF IS_DEBUG_BUILD
USING "FM_Mission_Creator_Using.sch"
USING "FM_Mission_Creator_Debug.sch"

FUNC BOOL DEAL_WITH_SKIPPING_EMPTY_OPTIONS_MC(INT iDirection)
	
	// To prevent instruction limit crashes
	INT iMenuMax = PICK_INT(sFMMCMenu.iCurrentMenuLength >= MAX_MENU_ITEMS, MAX_MENU_ITEMS, sFMMCMenu.iCurrentMenuLength)
	
	IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) > iMenuMax - 1
		SET_CREATOR_MENU_SELECTION(sFMMCmenu, 0)
	ENDIF
	IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) < 0
		SET_CREATOR_MENU_SELECTION(sFMMCmenu, iMenuMax - 1)
	ENDIF

	IF SHOULD_CURRENT_SELECTION_BE_SKIPPED(sFMMCmenu)
		SKIP_EMPTY_OPTION(sFMMCmenu, iDirection)
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

FUNC BOOL IS_THIS_OPTION_A_MENU_GOTO()	
	IF sFMMCmenu.sMenuGoto[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] != eFmmc_Null_item
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_THIS_OPTION_A_MENU_ACTION()	
	IF sFMMCmenu.sMenuAction[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] != eFmmc_Action_Null
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_THIS_OPTION_TOGGLEABLE()	
	IF sFMMCMenu.sActiveMenu = eFmmc_MAIN_OPTIONS_BASE
		IF IS_THIS_OPTION_A_MENU_ACTION()
			RETURN FALSE
		ELSE
			RETURN TRUE
		ENDIF
	ELIF sFMMCmenu.sActiveMenu = eFmmc_TOP_MENU
	OR sFMMCmenu.sActiveMenu = eFmmc_MAIN_MENU_BASE
	OR sFMMCmenu.sActiveMenu = eFmmc_SELECT_START_END
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC CAP_ITEM(INT iMaxValue, INT &iToCap, BOOL bRoundUp = FALSE, INT iLowest = 0)
	IF bRoundUp
		IF iToCap >= iMaxValue
			iToCap = iMaxValue -1
			IF iToCap < iLowest
				iToCap = iLowest
			ENDIF
		ELIF iToCap < iLowest
			iToCap = iLowest
		ENDIF
	ELSE
		IF iToCap >= iMaxValue
			iToCap = iLowest
		ELIF iToCap < iLowest
			iToCap = iMaxValue-1
			IF iToCap < iLowest
				iToCap = iLowest
			ENDIF
		ENDIF
	ENDIF
ENDPROC
#ENDIF

FUNC BOOL IS_THIS_OPTION_SELECTABLE()

	IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) < 0
		SET_CREATOR_MENU_SELECTION(sFMMCmenu, 0)
		PRINTLN("sFMMCmenu.iCurrentSelection CAME BACK AS -1")
		RETURN FALSE	
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_MAIN_MENU_BASE
		IF NOT IS_LONG_BIT_SET(sFMMCMenu.iBitActive, GET_CREATOR_MENU_SELECTION(sFMMCmenu))
			RETURN FALSE
		ENDIF
	ELIF sFMMCMenu.sActiveMenu = eFmmc_TOP_MENU
		IF NOT IS_BIT_SET(sFMMCmenu.iTopMenuActive, GET_CREATOR_MENU_SELECTION(sFMMCmenu))
			RETURN FALSE
		ENDIF
	ELIF sFMMCMenu.sActiveMenu = eFmmc_MAIN_OPTIONS_BASE
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) >= 31
			RETURN TRUE
		ELIF NOT IS_BIT_SET(sFMMCmenu.iOptionsMenuBitSet, GET_CREATOR_MENU_SELECTION(sFMMCmenu))
			RETURN FALSE
		ENDIF
	ELIF sFMMCMenu.sActiveMenu = eFmmc_ENTITY_SHARED_SPAWN_GROUP_SETTINGS
	OR sFMMCMenu.sActiveMenu = eFmmc_ENTITY_SHARED_SUB_SPAWN_GROUP_SETTINGS
	OR sFMMCMenu.sActiveMenu = eFmmc_ENTITY_SHARED_MODIFY_SPAWN_GROUP_ON_EVENT_SETTINGS
	OR sFMMCMenu.sActiveMenu = eFmmc_ENTITY_SHARED_MODIFY_SUB_SPAWN_GROUP_ON_EVENT_SETTINGS
		RETURN FALSE
	ELIF sFMMCMenu.sActiveMenu = eFmmc_DELETE_ENTITIES
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DELETE_ENTITY_MENU__PEDS
		AND g_FMMC_STRUCT_ENTITIES.iNumberOfPeds = 0
			RETURN FALSE
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DELETE_ENTITY_MENU__WEAPONS
		AND g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons = 0
			RETURN FALSE
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DELETE_ENTITY_MENU__PROPS
		AND g_FMMC_STRUCT_ENTITIES.iNumberOfProps = 0
			RETURN FALSE
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DELETE_ENTITY_MENU__DYNOPROPS
		AND g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps = 0
			RETURN FALSE
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DELETE_ENTITY_MENU__VEHICLES
		AND g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles = 0
			RETURN FALSE
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DELETE_ENTITY_MENU__OBJECTS
		AND g_FMMC_STRUCT_ENTITIES.iNumberOfObjects = 0
			RETURN FALSE
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DELETE_ENTITY_MENU__INTERACTABLES
		AND g_FMMC_STRUCT_ENTITIES.iNumberOfInteractables = 0
			RETURN FALSE
			
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DELETE_ENTITY_MENU__TEAM_START_POSITIONS
		AND g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[0] = 0
		AND g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[1] = 0
		AND g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[2] = 0
		AND g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[3] = 0
			RETURN FALSE
			
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DELETE_ENTITY_MENU__STUNT_JUMPS
		AND g_FMMC_STRUCT.iNumberOfSj = 0
			RETURN FALSE
			
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DELETE_ENTITY_MENU__ZONES
		AND g_FMMC_STRUCT_ENTITIES.iNumberOfZones = 0
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC FLOAT GET_STRING_WIDTH_MISSION_CREATOR(STRING stPassed)
	BEGIN_TEXT_COMMAND_GET_SCREEN_WIDTH_OF_DISPLAY_TEXT("STRING")
		ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY(stPassed)
	RETURN END_TEXT_COMMAND_GET_SCREEN_WIDTH_OF_DISPLAY_TEXT(TRUE)
ENDFUNC

FUNC INT GET_PLANNING_MISSION_INDEX_FROM_MENU_SELECTION()

	INT iNumberOfMissions[3], i, iLevel = -1, iLevelCount, iLevelSelection
	FOR i = 0 TO FMMC_MAX_PLANNING_MISSIONS-1
		IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sPlanningMissions[i].tlID)
			iNumberOfMissions[g_FMMC_STRUCT.sPlanningMissions[i].iLevel]++
		ENDIF
	ENDFOR
	
	PRINTLN("PD planning mission: Number of level 0 missions is ", iNumberOfMissions[0])
	PRINTLN("PD planning mission: Number of level 1 missions is ", iNumberOfMissions[1])
	PRINTLN("PD planning mission: Number of level 2 missions is ", iNumberOfMissions[2])
	
	IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) > 0
	AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) < 1+iNumberOfMissions[0]
		iLevel = 0
		iLevelSelection = GET_CREATOR_MENU_SELECTION(sFMMCmenu) - 1		
	ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) > 2+iNumberOfMissions[0]
	AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) < 3+iNumberOfMissions[0]+iNumberOfMissions[1]
		iLevel = 1
		iLevelSelection = GET_CREATOR_MENU_SELECTION(sFMMCmenu) - (2+iNumberOfMissions[0]+1)
	ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) > 4+iNumberOfMissions[0]+iNumberOfMissions[1]
	AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) < 5+iNumberOfMissions[0]+iNumberOfMissions[1]+iNumberOfMissions[2]
		iLevel = 2
		iLevelSelection = GET_CREATOR_MENU_SELECTION(sFMMCmenu) - (4+iNumberOfMissions[0]+iNumberOfMissions[1]+1)
	ENDIF
	
	IF iLevel = -1
		RETURN iLevel
	ENDIF
	
	PRINTLN("PD planning mission: iLevelSelection is ", iLevelselection)
	PRINTLN("PD planning mission: iLevel is ", iLevel)
	
	FOR i = 0 TO FMMC_MAX_PLANNING_MISSIONS-1
		IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sPlanningMissions[i].tlID)
		AND g_FMMC_STRUCT.sPlanningMissions[i].iLevel = iLevel
			PRINTLN("PD planning mission: found planning mission at level ", iLevel, " iLevelSelection is ", iLevelSelection, " iLevelCount is ", iLevelCount)
			IF iLevelSelection = iLevelCount
				RETURN i
			ENDIF
			iLevelCount++
		ENDIF
	ENDFOR
	
	RETURN -1
	
ENDFUNC

FUNC BOOL ARE_ANY_RULES_EMPTY(INT &iFailTeam, INT &iFailRule)
	iFailTeam = 0
	iFailRule = 0
	INT iTeam, iRule, iType
	FOR iTeam = 0 TO (g_FMMC_STRUCT.iMaxNumberOfTeams - 1)
		IF g_CreatorsSelDetails.sTeamOptions[iTeam].iNumberOfActiveRows != 0
			FOR iRule = 0 TO (g_CreatorsSelDetails.sTeamOptions[iTeam].iNumberOfActiveRows -1)
				iType = GET_RULE_TYPE_FROM_RULE(g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[iRule].iRule)
				IF iType = ciRULE_TYPE_PED
				OR iType = ciRULE_TYPE_VEHICLE
				OR iType = ciRULE_TYPE_OBJECT
				OR iType = ciRULE_TYPE_GOTO
					IF IS_LONG_BITSET_EMPTY(g_CreatorsSelDetails.sTeamOptions[iTeam].sSelection[iRule].iEntityBitSet)
						PRINTLN("No Entity for rule ", iRule, " for team ", iTeam)
						iFailRule = iRule
						iFailTeam = iTeam
						RETURN TRUE
					ENDIF
				ENDIF
			ENDFOR
		ENDIF
	ENDFOR
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_DRAW_PHOTO_PREVIEW()
	RETURN sFMMCMenu.sActiveMenu = eFmmc_PHOTO_CAM_BASE AND iIntroCamPlacementStage = 2 AND NOT IS_BIT_SET(sFMMCmenu.iMiscBitSet, bsHidePreviewPhoto)
ENDFUNC

/// PURPOSE:
///  Deals with increasing / decreasing the selection number. 
///  Number is used to set a vector vSwitchVec - This is used inside DEAL_WITH_SKY_CAM_SWITCH
///  Also sets BOOL bSwitchingCam = TRUE - This starts the switch camera action inside DEAL_WITH_SKY_CAM_SWITCH
PROC DEAL_WITH_CAMERA_SWITCH_SELECTION(BOOL bIncrease, INT iMaximumValue, INt iSetAs = -1)
	VECTOR vTemp
	
	INT iAdd = 1
	IF !bIncrease
		iAdd = -1
	ENDIF
	BOOL bFound = FALSE
	
	IF NOT HAS_NET_TIMER_STARTED(sFMMCmenu.tdCycleSwitchCooldownTimer)
		START_NET_TIMER(sFMMCmenu.tdCycleSwitchCooldownTimer)
	ELIF HAS_NET_TIMER_STARTED(sFMMCmenu.tdCycleSwitchCooldownTimer)
		REINIT_NET_TIMER(sFMMCmenu.tdCycleSwitchCooldownTimer)
	ENDIF
	
	PRINTLN("DEAL_WITH_CAMERA_SWITCH_SELECTION called with max value " , iMaximumValue)
	
	IF iMaximumValue > 0
		IF bSwitchingCam = FALSE
		AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()
			
			PLAY_EDIT_MENU_ITEM_SOUND()
			
			IF sFMMCMenu.sActiveMenu = eFMMC_TRIP_SKIPS
				bFound = FALSE
				WHILE !bFound
					iSwitchCam += iAdd
					IF iSwitchCam >= iMaximumValue
						iSwitchCam = 0
					ELIF iSwitchCam < 0
						iSwitchCam = iMaximumValue-1
					ENDIF
					IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.vTripSkipPos[iSwitchCam])
						bFound = TRUE
					ENDIF
				ENDWHILE
				vSwitchVec = g_FMMC_STRUCT.vTripSkipPos[iSwitchCam]
				fSwitchHeading = g_FMMC_STRUCT.fTripSkipHeading[iSwitchCam]
			ELIF sFMMCMenu.sActiveMenu = eFMMC_TEAM_RESTART_POINTS
				iSwitchCam+=iAdd
				IF iSwitchCam >= iMaximumValue
					iSwitchCam = 0
				ELIF iSwitchCam < 0
					iSwitchCam = iMaximumValue - 1
				ENDIF
				INT iCurrentPoint = 0, i, j, k
				FOR i = 0 TO FMMC_MAX_TEAMS-1
					FOR j = 0 TO FMMC_MAX_RESTART_CHECKPOINTS-1
						FOR k = 0 TO FMMC_MAX_RESTART_POINTS_PER_RESTART - 1
							IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.vTeamRestartPoint[i][j][k])
								IF iCurrentPoint = iSwitchCam
									vSwitchVec = g_FMMC_STRUCT.vTeamRestartPoint[i][j][k]
									fSwitchHeading = g_FMMC_STRUCT.fTeamRestartHeading[i][j][k]
									i = FMMC_MAX_TEAMS
									j = FMMC_MAX_RESTART_CHECKPOINTS
									k = FMMC_MAX_RESTART_POINTS_PER_RESTART
								ENDIF
								iCurrentPoint++
							ENDIF
						ENDFOR
					ENDFOR
				ENDFOR
			ELIF sFMMCMenu.sActiveMenu = eFMMC_QRC_SPAWN_POINTS
				iSwitchCam+=iAdd
				IF iSwitchCam >= iMaximumValue
					iSwitchCam = 0
				ELIF iSwitchCam < 0
					iSwitchCam = iMaximumValue - 1
				ENDIF
				
				vSwitchVec = g_FMMC_STRUCT.sQRCSpawnPoint[iSwitchCam].vSpawnPos
				fSwitchHeading = g_FMMC_STRUCT.sQRCSpawnPoint[iSwitchCam].fSpawnHeading
				
			ELIF sFMMCMenu.sActiveMenu = eFMMC_PERSONAL_VEHICLE_POSITIONS
				iSwitchCam+=iAdd
				IF iSwitchCam >= iMaximumValue
					iSwitchCam = 0
				ELIF iSwitchCam < 0
					iSwitchCam = iMaximumValue - 1
				ENDIF
				
				INT iSlot, iCurrentPoint
				FOR iSlot = 0 TO FMMC_PERSONAL_VEHICLE_PLACEMENT_PER_INSTANCE-1
					IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.vPersonalVehiclePosition[sFMMCMenu.iSelectedTeam][sFMMCMenu.iCurrentPVPosType][iSlot])
						IF iCurrentPoint = iSwitchCam
							vSwitchVec = g_FMMC_STRUCT.vPersonalVehiclePosition[sFMMCMenu.iSelectedTeam][sFMMCMenu.iCurrentPVPosType][iSlot]
							fSwitchHeading = g_FMMC_STRUCT.fPersonalVehicleHeading[sFMMCMenu.iSelectedTeam][sFMMCMenu.iCurrentPVPosType][iSlot]
							
							BREAKLOOP
						ENDIF
						iCurrentPoint++
					ENDIF
				ENDFOR
			ELSE
				IF bIncrease
					PRINTLN("INCEASING SELECTION")
					iSwitchCam ++
					IF iSwitchCam >= iMaximumValue
						iSwitchCam = 0
					ENDIF
				ELSE
					PRINTLN("DECREASING SELECTION")
					iSwitchCam --
					IF iSwitchCam < 0
						iSwitchCam = iMaximumValue - 1
					ENDIF
				ENDIF
				
				sFMMCmenu.iSwitchCam = iSwitchCam
			ENDIF
			
			IF sFMMCMenu.sActiveMenu = eFmmc_PICKUP_BASE
				IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PICKUP_MENU_CYCLE_THROUGH_PROPS
				AND sFMMCMenu.iVehicleWeaponPickupType = ciVEH_WEP_DETONATE 
				AND ARE_STRINGS_EQUAL(sFMMCMenu.sSubTypeName[CREATION_TYPE_WEAPONS], "WEP_VEH_PICKUP")
					vSwitchVec = g_FMMC_STRUCT_ENTITIES.sPlacedProp[iSwitchCam].vPos
					fSwitchHeading = g_FMMC_STRUCT_ENTITIES.sPlacedProp[iSwitchCam].fHead
				ELSE
					vSwitchVec = g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iSwitchCam].vPos
					fSwitchHeading = g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iSwitchCam].vRot.z
				ENDIF
				
			ELIF sFMMCMenu.sActiveMenu = eFmmc_PROP_BASE
				vSwitchVec = g_FMMC_STRUCT_ENTITIES.sPlacedProp[iSwitchCam].vPos
				fSwitchHeading = g_FMMC_STRUCT_ENTITIES.sPlacedProp[iSwitchCam].fHead
			ELIF sFMMCMenu.sActiveMenu = eFmmc_DYNOPROP_BASE
				vSwitchVec = g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iSwitchCam].vPos
				fSwitchHeading = g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iSwitchCam].fHead
			ELIF sFMMCMenu.sActiveMenu = efMMC_WORLD_PROPS
				vSwitchVec = g_FMMC_STRUCT_ENTITIES.sWorldProps[iSwitchCam].vPos
				fSwitchHeading = 0
			ELIF sFMMCMenu.sActiveMenu = eFmmc_ROCKET_POSITIONS
				vSwitchVec = g_FMMC_STRUCT_ENTITIES.vFMMCZoneRocketPositions[iSwitchCam]
				fSwitchHeading = 0
			ELIF sFMMCMenu.sActiveMenu = eFmmc_DIALOGUE_TRIGGER_BASE
				vSwitchVec = g_FMMC_STRUCT.sDialogueTriggers[iSwitchCam].vPosition
				fSwitchHeading = 0
			ELIF sFMMCMenu.sActiveMenu = eFmmc_ASS_OBJECTIVE_MENU_SET_PED_GOTO_POS
			OR sFMMCMenu.sActiveMenu = eFmmc_ASS_OBJECTIVE_MENU_SET_POOL_GOTO_POS
				INT iSelectedEntity = sFMMCMenu.iSelectedEntity
				IF sFMMCMenu.sActiveMenu != eFmmc_ASS_OBJECTIVE_MENU_SET_POOL_GOTO_POS
					IF sFMMCMenu.iSelectedEntity = -1
						iSelectedEntity = g_FMMC_STRUCT_ENTITIES.iNumberOfPeds
					ENDIF
					vSwitchVec = GET_ASSOCIATED_GOTO_TASK_DATA__POSITION(-1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iSelectedEntity].sAssociatedGotoTaskData, iSwitchCam)
				ELSE
					vSwitchVec = GET_ASSOCIATED_GOTO_TASK_DATA__POSITION(-1, g_FMMC_STRUCT.sAssociatedGotoPool[iSelectedEntity].sAssociatedGotoTaskData, iSwitchCam)
				ENDIF
				fSwitchHeading = 0
			
			ELIF sFMMCMenu.sActiveMenu = efMMC_CUTSCENE_ADD_SHOT
				vSwitchVec = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iSwitchCam].vPos
				fSwitchHeading = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iSwitchCam].fHead	
				g_FMMC_STRUCT.sCurrentSceneData.iVehicleToAttachTo[sFMMCMenu.iCurrentShot] = iSwitchCam
			ELIF sFMMCMenu.sActiveMenu = eFmmc_ZONES_BASE
				vTemp = GET_FMMC_ZONE_PLACED_CENTRE(iSwitchCam)
				IF NOT IS_VECTOR_ZERO(vTemp)
					vSwitchVec = vTemp
				ELSE
					EXIT
				ENDIF
				
			ELIF sFMMCMenu.sActiveMenu = eFmmc_DM_TEAM_SPAWN_POINT
				vSwitchVec = g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sFMMCMenu.iSelectedTeam][iSwitchCam].vPos
				
				IF IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sFMMCMenu.iSelectedTeam][iSwitchCam].vPos)
					vSwitchVec = g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[sFMMCMenu.iSelectedTeam][0].vPos
				ENDIF
				
				vSwitchVec.z += 30
				
				fSwitchHeading = 0
			ELIF sFMMCMenu.sActiveMenu = eFmmc_LOCATION_BASE
				IF iSetAs > -1
					vSwitchVec = g_FMMC_STRUCT_ENTITIES.sPlacedProp[iSetAs].vPos
				ELSE
					vSwitchVec = g_FMMC_STRUCT_ENTITIES.sPlacedProp[iSwitchCam].vPos
				ENDIF
				
				vSwitchVec.z += 20
				
				fSwitchHeading = 0
			ELIF sFMMCmenu.sActivemenu = eFmmc_SCRIPT_CAM_OPTIONS
				vSwitchVec = g_FMMC_STRUCT.vBEVCamFSLPos[iSetAs]
				fSwitchHeading = 0 //g_FMMC_STRUCT.vBEVCamFSLRotation[iSetAs]
				
				vSwitchVec.z += 3
			ELIF sFMMCmenu.sActivemenu = eFmmc_VEHICLE_CARGOBOB_ATTACHMENT
				iSetAs = sFMMCmenu.iCargobobEntitySelectID
				IF iSetAs > -1
					SWITCH sFMMCmenu.iCargobobEntitySelectType
						CASE ciRULE_TYPE_PED
							vSwitchVec = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iSetAs].vPos
							fSwitchHeading = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iSetAs].fHead
						BREAK
						CASE ciRULE_TYPE_VEHICLE
							vSwitchVec = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iSetAs].vPos
							fSwitchHeading = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iSetAs].fHead
						BREAK
						CASE ciRULE_TYPE_OBJECT
							vSwitchVec = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iSetAs].vPos
							fSwitchHeading = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iSetAs].fHead
						BREAK
						DEFAULT
							EXIT
						BREAK
					ENDSWITCH
				ELSE
					EXIT
				ENDIF
				
				vSwitchVec.z += 10
			ELIF sFMMCmenu.sActivemenu = eFmmc_DOORS_BASE
			OR sFMMCmenu.sActivemenu = eFmmc_DOORS_LEGACY
				vSwitchVec = g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iSwitchCam].vPos + <<0.3, 0.3, 0.3>>
				
			ENDIF
			
			bSwitchingCam = TRUE 
		ENDIF
	ENDIF
	
ENDPROC

PROC DISPLAY_TEXT_WITH_MISSION_STRING_FMMC(FLOAT fXpos, FLOAT fYpos, STRING pTextLabel, STRING pMissionNameString, BOOL bCenter = FALSE)
	BEGIN_TEXT_COMMAND_DISPLAY_TEXT(pTextLabel)
		ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY(pMissionNameString)
		SET_TEXT_CENTRE(bCenter)		
	END_TEXT_COMMAND_DISPLAY_TEXT(fXpos, fYpos)	
ENDPROC

PROC SETUP_TEXT_COLOUR_TEXT()
	SET_TEXT_SCALE(0.4000, 0.4000)
	SET_TEXT_COLOUR(255, 255, 255, 255)
	SET_TEXT_CENTRE(TRUE)
	SET_TEXT_DROPSHADOW(0, 0, 0, 0, 0)
	SET_TEXT_EDGE(0, 0, 0, 0, 0)
ENDPROC

PROC GET_TEXT_COLOUR_FROM_OBJECTIVE( INT iRule, INT &iR, INT &iG, INT &iB, INT &iA, INT iColour, INT iTextType )
	IF iRule = ciSELECTION_LOCATION_DESTROY
	AND iTextType != 8
		GET_HUD_COLOUR(HUD_COLOUR_PINK, iR,iG,iB,iA)
	ELSE
		SWITCH iColour
			CASE 0	
				GET_HUD_COLOUR(HUD_COLOUR_RED, iR,iG,iB,iA)
			BREAK
			CASE 1	
				GET_HUD_COLOUR(HUD_COLOUR_YELLOW, iR,iG,iB,iA)
			BREAK
			CASE 2	
				GET_HUD_COLOUR(HUD_COLOUR_BLUE, iR,iG,iB,iA)
			BREAK
			CASE 3			
				GET_HUD_COLOUR(HUD_COLOUR_GREEN, iR,iG,iB,iA)
			BREAK
			CASE 4			
				GET_HUD_COLOUR(HUD_COLOUR_BLUEDARK, iR,iG,iB,iA)
			BREAK
			CASE 5			
				GET_HUD_COLOUR(HUD_COLOUR_FRIENDLY, iR,iG,iB,iA)
			BREAK
			CASE 6
				GET_HUD_COLOUR(HUD_COLOUR_ORANGE, iR,iG,iB,iA)
			BREAK
			CASE 7
				GET_HUD_COLOUR(HUD_COLOUR_PINKLIGHT, iR,iG,iB,iA)
			BREAK
			CASE 8
				GET_HUD_COLOUR(HUD_COLOUR_PURPLE, iR,iG,iB,iA)
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

PROC CLEANUP_ENTERING_TEXT()
	g_CreatorsSelDetails.bKeyboardMenuActive = FALSE
	g_CreatorsSelDetails.bColouringText = FALSE
	g_CreatorsSelDetails.iStartSpace = -1 
	g_CreatorsSelDetails.iStartSpace = -1
	g_CreatorsSelDetails.iTotalSpace = -1
	g_CreatorsSelDetails.iNumberOfWords = 1
	g_CreatorsSelDetails.bSelectionActive = FALSE
	sCurrentVarsStruct.bResetUpHelp = TRUE
ENDPROC

FUNC TEXT_LABEL_15 GET_STRING_FROM_HUD_COLOUR(HUD_COLOURS hudC)
	TEXT_LABEL_15 sLabel = "FMMC_HUDC"
	INT iColour = ENUM_TO_INT(hudC)
	IF iColour > 27 //If you want to support more you have to add the text
		RETURN sLabel
	ENDIF
	sLabel += "_"
	sLabel += iColour
	RETURN sLabel
ENDFUNC

FUNC STRING GET_COLOUR_STRING(INT iRule, BOOL bSecondryColour, INT iColour)
 	
	UNUSED_PARAMETER(iRule)
	
	SWITCH iColour				
		CASE 0	
			RETURN "~r~"	
		CASE 1	
			RETURN "~y~"	
		CASE 2	
			RETURN "~b~"	
		CASE 3			
			RETURN "~g~"
		CASE 4	
			RETURN "~d~"
		CASE 5	
			RETURN "~f~"
		CASE 6
			RETURN "~o~"
		CASE 7
			RETURN "~q~"
		CASE 8
			RETURN "~p~"
	ENDSWITCH
	
 	IF bSecondryColour = TRUE
		RETURN "~y~"
	ENDIF
	
	RETURN ""
ENDFUNC

FUNC BOOL CONTROL_COLOURING_OBJECTIVE_TEXT(TEXT_LABEL_63 &tl63, INT iTextType = -1, BOOL bOverride = FALSE)
	INT iLengthOfString = GET_LENGTH_OF_LITERAL_STRING(tl63) 
	INT iCurrentSpace 	= 0
	INT iLoop
	INT iWordStart 	= -1
	INT iWordEnd 	= -1
	
	IF g_CreatorsSelDetails.iStartSpace = -1
		iWordStart = 0
	ENDIF
	
	FOR iLoop = 0 TO (iLengthOfString  -1)
		IF ARE_STRINGS_EQUAL(" ", GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME(tl63, iLoop, iLoop+1))
			IF g_CreatorsSelDetails.iStartSpace = iCurrentSpace
				IF iWordStart = -1
					iWordStart = (iLoop+1)
				ENDIF
				
			ELIF (g_CreatorsSelDetails.iStartSpace+g_CreatorsSelDetails.iNumberOfWords) = iCurrentSpace
				IF iWordEnd = -1
					iWordEnd = iLoop
				ENDIF
				
			ENDIF
			iCurrentSpace++
		ENDIF
	ENDFOR
	
	g_CreatorsSelDetails.iTotalSpace = iCurrentSpace
	IF iWordEnd = -1
		iWordEnd = iLengthOfString
	ENDIF
	
	DRAW_RECT(0.5000, 0.5000, 0.4930, 0.1550, 0, 0, 0, 255)
	
	//Get the totall width of the string
	SETUP_TEXT_COLOUR_TEXT()
	FLOAT fTotat = GET_STRING_WIDTH_MISSION_CREATOR(tl63)
	SETUP_TEXT_COLOUR_TEXT()
	FLOAT fFirstPart 
	FLOAT fSelectionWidth  
	FLOAT fSpaceWidth
	
	IF bOverride
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
	ENDIF
	
	IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
		g_CreatorsSelDetails.iWordColour++
		CAP_FMMC_MENU_ITEM(9, g_CreatorsSelDetails.iWordColour)
	ENDIF
	IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
		g_CreatorsSelDetails.iWordColour--
		CAP_FMMC_MENU_ITEM(9, g_CreatorsSelDetails.iWordColour)
	ENDIF	
	
	IF iWordStart != -1
	AND iWordEnd != -1
		SETUP_TEXT_COLOUR_TEXT()
		fSpaceWidth = (GET_STRING_WIDTH_MISSION_CREATOR(" ")/2)
		IF g_CreatorsSelDetails.iStartSpace = -1
			fFirstPart  = 0.0
		ELSE
			SETUP_TEXT_COLOUR_TEXT()
			fFirstPart = GET_STRING_WIDTH_MISSION_CREATOR(GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME(tl63, 0, iWordStart)) - (fSpaceWidth/2)
		ENDIF
		SETUP_TEXT_COLOUR_TEXT()
		fSelectionWidth = GET_STRING_WIDTH_MISSION_CREATOR(GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME(tl63, iWordStart, (iWordEnd - 1))) + fSpaceWidth
		INT iR, iG, iB, iA
		GET_TEXT_COLOUR_FROM_OBJECTIVE(g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule, iR, iG, iB, iA, g_CreatorsSelDetails.iWordColour, iTextType )
		IF g_CreatorsSelDetails.iWordColour = 5 //~f~ friendly - same colour as blue but with different fucntionality
			DRAW_RECT(0.5000 - (fTotat/2) + fFirstPart +(fSelectionWidth/2) +(fSpaceWidth/4), 0.5000, fSelectionWidth + 0.005, 0.06, 255, 255, 255, 200)
		ENDIF
		DRAW_RECT(0.5000 - (fTotat/2) + fFirstPart +(fSelectionWidth/2) +(fSpaceWidth/4), 0.5000, fSelectionWidth, 0.0550, iR, iG, iB, 200)
	ELSE
		PRINTLN("CONTROL_COLOURING_OBJECTIVE_TEXT | iWordStart = ", iWordStart)
		PRINTLN("CONTROL_COLOURING_OBJECTIVE_TEXT | iWordEnd   = ",  iWordEnd )
	ENDIF
	
	DISPLAY_TEXT_WITH_MISSION_STRING_FMMC(0.5000, 0.4850, "STRING", tl63, TRUE)
	IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
		IF g_CreatorsSelDetails.bSelectionActive = TRUE
			g_CreatorsSelDetails.iNumberOfWords--
			IF g_CreatorsSelDetails.iNumberOfWords < 1
				g_CreatorsSelDetails.iNumberOfWords = 1
			ENDIF
		ELSE
			g_CreatorsSelDetails.iStartSpace--
			IF g_CreatorsSelDetails.iStartSpace < -1 
				g_CreatorsSelDetails.iStartSpace = (g_CreatorsSelDetails.iTotalSpace -1)
			ENDIF
		ENDIF
	ENDIF
	IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
		IF g_CreatorsSelDetails.bSelectionActive = TRUE
			g_CreatorsSelDetails.iNumberOfWords++
			IF g_CreatorsSelDetails.iNumberOfWords > (g_CreatorsSelDetails.iTotalSpace - g_CreatorsSelDetails.iStartSpace)
				g_CreatorsSelDetails.iNumberOfWords = (g_CreatorsSelDetails.iTotalSpace - g_CreatorsSelDetails.iStartSpace)
			ENDIF
		ELSE
			g_CreatorsSelDetails.iStartSpace++
			IF g_CreatorsSelDetails.iStartSpace >= g_CreatorsSelDetails.iTotalSpace
				g_CreatorsSelDetails.iStartSpace = -1
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
		IF g_CreatorsSelDetails.bSelectionActive = TRUE
			
			IF g_CreatorsSelDetails.bEnteringSecondColour
				IF g_CreatorsSelDetails.iWordStartSaved > g_CreatorsSelDetails.iStartSpace 
				AND (g_CreatorsSelDetails.iStartSpace + g_CreatorsSelDetails.iNumberOfWords) > g_CreatorsSelDetails.iWordStartSaved
					PRINTLN("CONTROL_COLOURING_OBJECTIVE_TEXT | NOT OK !!!!1")
					RETURN FALSE
				ENDIF
			ENDIF
			
			//Build the string
			TEXT_LABEL_63 tl63Return = ""
			
			IF iWordStart > 0
				tl63Return += GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME(tl63, 0, iWordStart)
			ENDIF
			
			tl63Return += GET_COLOUR_STRING(g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule, g_CreatorsSelDetails.bEnteringSecondColour, g_CreatorsSelDetails.iWordColour)
			tl63Return += GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME(tl63, iWordStart, iWordEnd)
			tl63Return += "~s~"
			
			IF iWordEnd < iLengthOfString
				tl63Return += GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME(tl63, iWordEnd, iLengthOfString)
			ENDIF
			
			PRINTLN("CONTROL_COLOURING_OBJECTIVE_TEXT | tl63Return = ", tl63Return, " / iWordStart: ", iWordStart, " / iWordEnd: ", iWordEnd, " / iLengthOfString: ", iLengthOfString)
			tl63 = tl63Return
			
			IF NOT bOverride
			AND (g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule = ciSELECTION_PLAYER_RULE_KILL_ANY		
			OR g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule = ciSELECTION_PLAYER_RULE_KILL_TEAM0		
			OR g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule = ciSELECTION_PLAYER_RULE_KILL_TEAM1		
			OR g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule = ciSELECTION_PLAYER_RULE_KILL_TEAM2		
			OR g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule = ciSELECTION_PLAYER_RULE_KILL_TEAM3	
			OR g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule = ciSELECTION_PLAYER_RULE_GET_MASKS	
			OR g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule = ciSELECTION_PLAYER_RULE_SCRIPTED_CUTSCENE
			OR g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule = ciSELECTION_PLAYER_RULE_ARREST_TEAM0
			OR g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule = ciSELECTION_PLAYER_RULE_ARREST_TEAM1
			OR g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule = ciSELECTION_PLAYER_RULE_ARREST_TEAM2
			OR g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule = ciSELECTION_PLAYER_RULE_ARREST_TEAM3
			OR g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule = ciSELECTION_PLAYER_RULE_ARREST_ALL)
				g_CreatorsSelDetails.bEnteringSecondColour = FALSE
				g_CreatorsSelDetails.bSelectionActive = FALSE
				g_CreatorsSelDetails.iWordStartSaved = 0
				g_CreatorsSelDetails.iWordEndSaved 	= 0
				RETURN TRUE
			ELSE
				g_CreatorsSelDetails.iWordStartSaved = g_CreatorsSelDetails.iStartSpace
				g_CreatorsSelDetails.iWordEndSaved 	= g_CreatorsSelDetails.iNumberOfWords
				g_CreatorsSelDetails.bEnteringSecondColour = TRUE
				g_CreatorsSelDetails.iNumberOfWords = 1
				PRINTLN("CONTROL_COLOURING_OBJECTIVE_TEXT | DOES_THIS_RULE_HAVE_TWO_TEXT_COLOURS")
				g_CreatorsSelDetails.bSelectionActive = FALSE			
			ENDIF
			
		ELSE
			IF g_CreatorsSelDetails.bEnteringSecondColour = TRUE
				IF g_CreatorsSelDetails.iStartSpace > g_CreatorsSelDetails.iWordStartSaved
				AND g_CreatorsSelDetails.iStartSpace < (g_CreatorsSelDetails.iWordStartSaved +g_CreatorsSelDetails.iWordEndSaved)
					PRINTLN("CONTROL_COLOURING_OBJECTIVE_TEXT | STAR LOCATION NOT OK 1111")		
				ELIF g_CreatorsSelDetails.iWordStartSaved != g_CreatorsSelDetails.iStartSpace
					PRINTLN("CONTROL_COLOURING_OBJECTIVE_TEXT | STAR LOCATION OK")
					g_CreatorsSelDetails.bSelectionActive = TRUE						
				ELSE
					PRINTLN("CONTROL_COLOURING_OBJECTIVE_TEXT | STAR LOCATION NOT OK")
				ENDIF
			ELSE
				g_CreatorsSelDetails.bSelectionActive = TRUE
			ENDIF
		ENDIF
	ENDIF
	IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
		IF g_CreatorsSelDetails.bSelectionActive = TRUE
			g_CreatorsSelDetails.bSelectionActive = FALSE
			g_CreatorsSelDetails.iNumberOfWords = 1
		ELSE	
			g_CreatorsSelDetails.bEnteringSecondColour = FALSE
			g_CreatorsSelDetails.bSelectionActive = FALSE
			g_CreatorsSelDetails.iWordStartSaved = 0
			g_CreatorsSelDetails.iWordEndSaved 	= 0
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC STRING GET_DESCRIPTION_OF_ENTITY(INT type, INT id)
	IF id = -1
	OR type = ciRULE_TYPE_NONE
		RETURN ""
	ELSE
		SWITCH type
			CASE ciRULE_TYPE_PED
				RETURN GET_CREATOR_NAME_FOR_PED_MODEL(g_FMMC_STRUCT_ENTITIES.sPlacedPed[id].mn)
			BREAK
			CASE ciRULE_TYPE_VEHICLE
				RETURN GET_CREATOR_NAME_FOR_VEHICLE_MODEL(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[id].mn)
			BREAK
			CASE ciRULE_TYPE_OBJECT
				RETURN GET_CREATOR_NAME_FOR_PROP_MODEL(g_FMMC_STRUCT_ENTITIES.sPlacedObject[id].mn)
			BREAK
			CASE ciRULE_TYPE_TRAIN			
				RETURN "TRAIN"
			BREAK
			DEFAULT
				RETURN ""
			BREAK
		ENDSWITCH
	ENDIF
	RETURN ""
ENDFUNC

FUNC STRING GET_PACK_SPECIFIC_OPTIONS_DESCRIPTION(INT iRow)
	IF iRow = ciPACK_SPECIFIC_CASINO_HEIST 			RETURN "" ENDIF
	RETURN ""
ENDFUNC

FUNC STRING GET_CASINO_HEIST_OPTIONS_DESCRIPTION(INT iRow)
	IF iRow = ciCASINO_HEIST_ELEVATOR_RAPPEL 		RETURN "MC_H_RL_SRRE" ENDIF
	RETURN ""
ENDFUNC

FUNC STRING GET_ALTERNATE_VARIABLES_OPTIONS_DESCRIPTION_ISLAND_HEIST(INT iBoundsSelection)
	SWITCH iBoundsSelection
		CASE OPTION_ALT_VAR_RULE_TITLE		RETURN "MC_H_AVA_RL_T"
		CASE OPTION_ALT_VAR_RULE_CONFIG		
			TEXT_LABEL_15 tl15
			tl15 = "MC_AVA_CON_"
			tl15 += ENUM_TO_INT(g_FMMC_STRUCT.sPoolVars.eConfig)
			RETURN TEXT_LABEL_TO_STRING(tl15)
		CASE OPTION_ALT_VAR_RULE_USE_ISLAND_HEIST_TARGET_OBJECTIVE_PRIMARY_TEXT_OVERRIDE			RETURN "MCH_AVA_RUL_POTTR"
		CASE OPTION_ALT_VAR_RULE_USE_ISLAND_HEIST_TARGET_OBJECTIVE_SECONDARY_TEXT_OVERRIDE			RETURN "MCH_AVA_RUL_SOTTR"
		CASE OPTION_ALT_VAR_RULE_USE_ISLAND_HEIST_TARGET_OBJECTIVE_WAIT_TEXT_OVERRIDE				RETURN "MCH_AVA_RUL_WOTTR"
		CASE OPTION_ALT_VAR_RULE_USE_ISLAND_HEIST_TARGET_OBJECTIVE_PRIMARY_TEXT_OVERRIDE_ALT		RETURN "MCH_AVA_RUL_POTTRA"
		CASE OPTION_ALT_VAR_RULE_USE_ISLAND_HEIST_TARGET_OBJECTIVE_SECONDARY_TEXT_OVERRIDE_ALT		RETURN "MCH_AVA_RUL_SOTTRA"
		CASE OPTION_ALT_VAR_RULE_USE_ISLAND_HEIST_TARGET_OBJECTIVE_WAIT_TEXT_OVERRIDE_ALT			RETURN "MCH_AVA_RUL_WOTTRA"
		CASE OPTION_ALT_VAR_RULE_USE_ISLAND_HEIST_EXIT_OBJECTIVE_TEXT_PRIMARY_OVERRIDE				RETURN "MCH_AVA_RUL_POTEX"
		CASE OPTION_ALT_VAR_RULE_USE_ISLAND_HEIST_EXIT_OBJECTIVE_TEXT_SECONDARY_OVERRIDE			RETURN "MCH_AVA_RUL_SOTEX"
		CASE OPTION_ALT_VAR_RULE_USE_ISLAND_HEIST_EXIT_OBJECTIVE_TEXT_WAIT_OVERRIDE					RETURN "MCH_AVA_RUL_WOTEX"		
		CASE OPTION_ALT_VAR_RULE_USE_ISLAND_HEIST_INTRO_CUTSCENE_OVERRIDE_APPROACH					RETURN "MCH_AVA_RUL_ICUT"
		CASE OPTION_ALT_VAR_RULE_GANG_CHASE_PEDS_AFFECTED_BY_ACCURACY_POISON 						RETURN "MCH_AVA_RUL_GCPA"
		CASE OPTION_ALT_VAR_RULE_GANG_CHASE_PEDS_AFFECTED_BY_HEALTH_POISON 							RETURN "MCH_AVA_RUL_GCPH"
		CASE OPTION_ALT_VAR_RULE_GANG_CHASE_PEDS_AFFECTED_BY_ARMOUR_DISRUPTION						RETURN "MCH_AVA_RUL_GCAD"
		CASE OPTION_ALT_VAR_RULE_GANG_CHASE_AFFECTED_BY_HELICOPTER_DISRUPTION						RETURN "MCH_AVA_RUL_GCHD"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC STRING GET_ALTERNATE_VARIABLES_OPTIONS_DESCRIPTION_TUNER(INT iBoundsSelection)
	SWITCH iBoundsSelection
		CASE OPTION_ALT_VAR_RULE_TITLE		RETURN "MC_H_AVA_RL_T"
		CASE OPTION_ALT_VAR_RULE_CONFIG		
			TEXT_LABEL_15 tl15
			tl15 = "MC_AVA_CON_"
			tl15 += ENUM_TO_INT(g_FMMC_STRUCT.sPoolVars.eConfig)
			RETURN TEXT_LABEL_TO_STRING(tl15)
		CASE OPTION_ALT_VAR_TUNER_RULE_USE_BUNKER_LOCATION_FOR_CUTSCENE_INDEX 					RETURN "MCH_AVA_RUL_BNKSC"
		CASE OPTION_ALT_VAR_TUNER_RULE_PLAYER_ABILITY_REPAIR_REQUIRES_LEVEL_10_CAR_CLUB			RETURN "MCH_AVA_RUL_ABRQ1"
		CASE OPTION_ALT_VAR_TUNER_RULE_PLAYER_ABILITY_DISTRACTION_REQUIRES_LEVEL_15_CAR_CLUB	RETURN "MCH_AVA_RUL_ABRQ2"
		CASE OPTION_ALT_VAR_TUNER_RULE_PLAYER_ABILITY_AMBUSH_REQUIRES_LEVEL_25_CAR_CLUB			RETURN "MCH_AVA_RUL_ABRQ3"
		CASE OPTION_ALT_VAR_TUNER_RULE_PLAYER_ABILITY_RIDEALONG_REQUIRES_LEVEL_30_CAR_CLUB		RETURN "MCH_AVA_RUL_ABRQ4"
		BREAK
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC STRING GET_ALTERNATE_VARIABLES_OPTIONS_DESCRIPTION_FIXER(INT iBoundsSelection)
	SWITCH iBoundsSelection
		CASE OPTION_ALT_VAR_RULE_TITLE		RETURN "MC_H_AVA_RL_T"
		CASE OPTION_ALT_VAR_RULE_CONFIG		
			TEXT_LABEL_15 tl15
			tl15 = "MC_AVA_CON_"
			tl15 += ENUM_TO_INT(g_FMMC_STRUCT.sPoolVars.eConfig)
			RETURN TEXT_LABEL_TO_STRING(tl15)			
		BREAK
		CASE OPTION_ALT_VAR_FIXER_RULE_USE_PROPERTY_OWNED_POOL_VARS_FOR_CUTSCENE_INDEX
			RETURN "MCH_AVA_RUL_ENTCD"
		BREAK
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC STRING GET_ALTERNATE_VARIABLES_OPTIONS_DESCRIPTION(INT iBoundsSelection)	
	SWITCH g_FMMC_STRUCT.sPoolVars.eConfig
		CASE FMMC_ALT_VAR_CONFIG_ISLAND_HEIST
			RETURN GET_ALTERNATE_VARIABLES_OPTIONS_DESCRIPTION_ISLAND_HEIST(iBoundsSelection)
		BREAK
		CASE FMMC_ALT_VAR_CONFIG_TUNER
			RETURN GET_ALTERNATE_VARIABLES_OPTIONS_DESCRIPTION_TUNER(iBoundsSelection)
		BREAK		
		CASE FMMC_ALT_VAR_CONFIG_FIXER
			RETURN GET_ALTERNATE_VARIABLES_OPTIONS_DESCRIPTION_FIXER(iBoundsSelection)
		BREAK
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC STRING GET_LEFT_HAND_MENU_ITEM_DESCRIPTION()
	
	IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) != -1
	AND NOT IS_STRING_NULL_OR_EMPTY(sFMMCmenu.sOptionDescription[GET_CREATOR_MENU_SELECTION(sFMMCmenu)])
		RETURN sFMMCmenu.sOptionDescription[GET_CREATOR_MENU_SELECTION(sFMMCmenu)]
	ENDIF

	IF sFMMCMenu.sActiveMenu = eFmmc_MAIN_MENU_BASE
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MENU_ITEM_MISSION_SSL RETURN "MC_H_SSL" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MENU_ITEM_MISSION_PANCAM RETURN "DMC_H_33" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MENU_ITEM_MISSION_TMSTRT RETURN "MC_H_TMSTRT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MENU_ITEM_MISSION_TMCAMS		 RETURN "DMC_H_36F" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MENU_ITEM_MISSION_OUTCAM RETURN "MC_H_OUT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MENU_ITEM_MISSION_GOTO RETURN "MC_H_GOTO2" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MENU_ITEM_MISSION_PLYR RETURN "MC_H_PLYR"	 ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MENU_ITEM_MISSION_WEP RETURN "MC_H_WEP" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MENU_ITEM_MISSION_PED RETURN "MC_H_PED" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MENU_ITEM_MISSION_VEH RETURN "MC_H_VEH" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MENU_ITEM_MISSION_INT RETURN "MC_H_INT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MENU_ITEM_MISSION_OBJ RETURN "MC_H_OBJ" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MENU_ITEM_MISSION_PRP RETURN "MC_H_PRP" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MENU_ITEM_MISSION_DYNOPRP RETURN "MC_H_DYNPRP" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MENU_ITEM_MISSION_BOUNDS RETURN "MC_H_BOUNDS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MENU_ITEM_MISSION_ZONES RETURN "MC_H_ZONE" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MENU_ITEM_MISSION_COVER RETURN "FMMC_CP_D" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MENU_ITEM_MISSION_DOORS RETURN "MC_H_DOOR" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MENU_ITEM_MISSION_DIALOGUE RETURN "MC_H_DTRIG" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MENU_ITEM_MISSION_MAP RETURN "MC_H_MAP" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MENU_ITEM_MISSION_USJ RETURN "MC_H_USJ" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MENU_ITEM_MISSION_RST RETURN "MC_H_RST"	 ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MENU_ITEM_MISSION_QRC_SPAWN_POINTS RETURN "MC_H_QRC" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MENU_ITEM_MISSION_WARP_PORTALS RETURN "FMMC_MENH_WRP" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MENU_ITEM_MISSION_VFX RETURN "FMMC_MENH_VFX" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MENU_ITEM_MISSION_PLACED_MARKERS RETURN "FMMC_MENH_MRK" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MENU_ITEM_MISSION_DEV_ASSISTS RETURN "FMMC_MENH_DVAS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MENU_ITEM_MISSION_DEV_ASSISTS_LOAD_CONTENT_MENU RETURN "FMMC_MENH_DVLO" ENDIF
	ENDIF
	

	IF sFMMCMenu.sActiveMenu = eFmmc_PTFX_BASE
		RETURN GET_PTFX_DESCRIPTION(sFMMCmenu)
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_ZONES_BASE
		RETURN GET_ZONE_BASE_MENU_DESCRIPTION(sFMMCmenu)
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFMMC_ZONE_TYPE_SPECIFIC_OPTIONS
		RETURN GET_ZONE_TYPE_SPECIFIC_MENU_DESCRIPTIONS(sFMMCmenu)
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFMMC_ZONE_ACTIVATION
		RETURN "MC_H_ZNE_ACR"
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFMMC_ZONE_DEACTIVATION
		RETURN "MC_H_ZNE_DER"
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFMMC_ROCKET_POSITIONS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ROCKET_POSITIONS_MENU_HOMING_ROCKETS RETURN "MC_H_RP_HOM" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ROCKET_POSITIONS_MENU_LINKED_OBJECT RETURN "MC_H_RP_LOB" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ROCKET_POSITIONS_MENU_LINKED_ZONES RETURN "MC_H_RP_LZO" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ROCKET_POSITIONS_MENU_ACCURACY_VEH RETURN "MC_H_RP_VAC" ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_PAN_CAM_BASE
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 0
			IF DOES_BLIP_EXIST(bCameraTriggerBlip)
				RETURN "DMC_H_33B"
			ELSE
				RETURN "DMC_H_33"	
			ENDIF			
		ELSE
			RETURN "DMC_H_35"
		ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_DM_TEAM_CAMERAS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 0
			IF DOES_BLIP_EXIST(bCameraTriggerBlip)
				RETURN "DMC_H_36B"
			ELSE
				IF g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[sFMMCmenu.iSelectedCameraTeam] > 0
					RETURN "DMC_H_36A"	
				ELSE
					RETURN "DMC_H_36C"
				ENDIF
			ENDIF	
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 1
			RETURN "DMC_H_0"
		ELSE
			IF NOT DOES_BLIP_EXIST(bTeamCamPanBlip[sFMMCmenu.iSelectedCameraTeam])
				RETURN "DMC_H_36E"
			ELSE
				RETURN "DMC_H_36D"
			ENDIF
		ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_DOORS_LEGACY
		IF sFMMCMenu.sCurrentDoor.mnDoorModel != DUMMY_MODEL_FOR_SCRIPT
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DOORS_LEGACY_MODEL_NAME RETURN "MC_H_DOOR0" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DOORS_LEGACY_POSITION RETURN "MC_H_DOOR1" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DOORS_LEGACY_OPEN_RATIO RETURN "MC_H_DOOR3" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DOORS_LEGACY_SWING_FREE RETURN "MC_H_DOOR5" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DOORS_LEGACY_AUTOMATIC_RATE RETURN "MC_H_DR_AUR" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DOORS_LEGACY_AUTOMATIC_DISTANCE RETURN "MC_H_DR_AUD" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DOORS_LEGACY_UPDATE_ONLY_ON_CHECKPOINT_1 RETURN "MC_H_DR_UOCP1" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DOORS_LEGACY_UPDATE_ONLY_ON_CHECKPOINT_2 RETURN "MC_H_DR_UOCP2" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DOORS_LEGACY_UPDATE_ONLY_ON_CHECKPOINT_3 RETURN "MC_H_DR_UOCP3" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DOORS_LEGACY_UPDATE_ONLY_ON_CHECKPOINT_4 RETURN "MC_H_DR_UOCP4" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DOORS_LEGACY_UPDATE_EVERY_FRAME RETURN "MC_H_DR_UEF" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DOORS_LEGACY_UPDATE_FROM_OBJECT RETURN "MC_H_DR_UFO" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DOORS_LEGACY_UPDATE_AUTOMATIC_RATE RETURN "MC_H_DR_UAUR" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DOORS_LEGACY_UPDATE_AUTOMATIC_DISTANCE RETURN "MC_H_DR_UAUD" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DOORS_LEGACY_UPDATE_DELAY RETURN "MC_H_DR_DEL" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DOORS_LEGACY_UPDATE_ON_MIDPOINT RETURN "MC_H_DOOR6" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DOORS_LEGACY_CONFIRM_DOOR RETURN "MC_H_DOOR4" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DOORS_LEGACY_START_Z RETURN "MC_H_DR_STR" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DOORS_LEGACY_END_Z RETURN "MC_H_DR_END" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DOORS_LEGACY_CONTINUITY_ID RETURN "MC_H_MICO_ID" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DOORS_LEGACY_UPDATE_FROM_PREVIOUS_MISSION RETURN "MC_H_DOOR_UPM" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DOORS_LEGACY_UPDATE_TIMER RETURN "MC_H_DOOR_UDT" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DOORS_LEGACY_DIRECTIONAL_LOCK RETURN "MC_H_DOOR_DLOCK" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DOORS_LEGACY_DIRECTIONAL_LOCK_UPDATE RETURN "MC_H_DOOR_UDLOCK" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DOORS_LEGACY_DOOR_IS_FLIPPED RETURN "MC_H_DOOR_FLP" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DOORS_LEGACY_LINKED_ELEVATOR RETURN "MC_H_DOOR_LE" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DOORS_LEGACY_LOCK_IF_NO_LINKED_OBJ RETURN "MC_H_DOOR_NLOL" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DOORS_LEGACY_UNLOCK_IF_NO_LINKED_OBJ RETURN "MC_H_DOOR_NLOU" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DOORS_LEGACY_OPEN_RATIO_IF_NO_LINKED_OBJ RETURN "MC_H_DOOR_OROU" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DOORS_LEGACY_HIDE_DURING_CUTSCENE RETURN "MC_H_DOOR_CUT" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DOORS_LEGACY_CREATE_COVER_BLOCKING_AREA RETURN "MC_H_DOOR_COV" ENDIF			
		ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_SELF_DESTRUCT_SETTINGS
		RETURN GET_SELF_DESTRUCT_SETTINGS_HELP(sFMMCmenu)
	ENDIF
		
	IF sFMMCMenu.sActiveMenu = eFmmc_ENTITY_SHARED_SPAWN_SETTINGS
		RETURN DRAW_HELP_ENTITY_SHARED_SPAWN_OPTIONS(sFMMCmenu)
	ENDIF	
	IF sFMMCMenu.sActiveMenu = eFmmc_ENTITY_SHARED_SPAWN_GROUP_SETTINGS
		RETURN DRAW_HELP_ENTITY_SHARED_SPAWN_GROUP_OPTIONS(sFMMCmenu)
	ENDIF	
	IF sFMMCMenu.sActiveMenu = eFmmc_ENTITY_SHARED_SUB_SPAWN_GROUP_SETTINGS
		RETURN DRAW_HELP_ENTITY_SHARED_SUB_SPAWN_GROUP_OPTIONS(sFMMCmenu)
	ENDIF	
	IF sFMMCMenu.sActiveMenu = eFmmc_ENTITY_SHARED_MODIFY_SPAWN_GROUP_ON_EVENT_SETTINGS
		RETURN DRAW_HELP_ENTITY_SHARED_MODIFY_SPAWN_GROUP_ON_EVENT_OPTIONS(sFMMCmenu)
	ENDIF	
	IF sFMMCMenu.sActiveMenu = eFmmc_ENTITY_SHARED_MODIFY_SUB_SPAWN_GROUP_ON_EVENT_SETTINGS
		RETURN DRAW_HELP_ENTITY_SHARED_MODIFY_SUB_SPAWN_GROUPS_ON_EVENT_OPTIONS(sFMMCmenu)
	ENDIF
	IF sFMMCMenu.sActiveMenu = eFmmc_ENTITY_SHARED_SPAWN_BLOCKING_FLAG_SETTINGS
		RETURN DRAW_HELP_ENTITY_SHARED_SPAWN_CONDITION_FLAGS_OPTIONS(sFMMCmenu)
	ENDIF	
	IF sFMMCmenu.sActiveMenu = eFmmc_WARP_PORTALS_PLACEMENTS_OPTIONS
		RETURN GET_WARP_PORTALS_PLACEMENTS_DESCRIPTIONS(sFMMCmenu)
	ENDIF
	
	// Help Text
	IF sFMMCMenu.sActiveMenu = eFmmc_WARP_PORTALS_BASE
		RETURN GET_WARP_PORTALS_DESCRIPTION(sFMMCmenu)
	ENDIF	
	// Help Text
	IF sFMMCMenu.sActiveMenu = eFmmc_WARP_PORTALS_ADDITIONAL_OPTIONS
		RETURN GET_WARP_PORTALS_ADDITIONAL_WARP_OPTIONS_DESCRIPTION(sFMMCmenu)
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_SELECT_START_END
		RETURN "MC_H_SSL"
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_LOCATION_BASE
	
        IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = LOCATIONS_TYPE
			IF sFMMCmenu.iGOTOSelection = LOCATION_RULE_GO_TO RETURN "MC_H_GOTO01" ENDIF
            IF sFMMCmenu.iGOTOSelection = LOCATION_RULE_CAPTURE RETURN "MC_H_GOTO02" ENDIF
            IF sFMMCmenu.iGOTOSelection = LOCATION_RULE_LEAVE RETURN "MC_H_GOTO04" ENDIF
        ENDIF
		
        IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = LOCATIONS_NEW_RULE RETURN "MC_H_VEH4G" ENDIF
        IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = LOCATIONS_AREA_TYPE RETURN "MC_H_LOC_ART" ENDIF
        IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = LOCATIONS_RADIUS RETURN "MC_H_GOTO1" ENDIF
        IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = LOCATIONS_ANGLED_AREA_WIDTH RETURN "MC_H_LOC_WDTH" ENDIF
        IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = LOCATIONS_ANGLED_AREA_MIN_HEIGHT RETURN "MC_H_LOC_MINH" ENDIF
        IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = LOCATIONS_ANGLED_AREA_MAX_HEIGHT RETURN "MC_H_LOC_MAXH" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = LOCATIONS_VEHICLE_OPTIONS RETURN "MC_H_LOC_VO" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = LOCATIONS_CLEAR_GPS RETURN "FMMCCMENU_GPH" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = LOCATIONS_SMALL_CORONA_SIZE RETURN "FMMCCMENU_CSH" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = LOCATIONS_HIDE_MARKER RETURN "MC_H_GOTO_HDM" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = LOCATIONS_HIDE_LOCATE RETURN "MC_H_GOTO_HDL" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = LOCATIONS_JUMP_TO_OBJECTIVE RETURN "FMMC_JTO_DESCR" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = LOCATIONS_RETURN_TO_HEIST_APARTMENT RETURN "MC_H_GOTO_RTHA" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = LOCATIONS_ALL_TEAM_AT_LOCATE RETURN "MC_H_LOC_TAL" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = LOCATIONS_REMOVE_BLIP_WHEN_IN_LOCATE RETURN "MC_H_GOTO_RBWIL" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = LOCATIONS_AERIAL RETURN "MC_H_GOTOAER" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = LOCATIONS_FLAG RETURN "MC_H_GOTOFLG" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = LOCATIONS_BEAST_FLAG RETURN "MC_H_BSTFLG" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = LOCATIONS_ROTATE_TO_PLAYER RETURN "MC_H_GOTORTP" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = LOCATIONS_TEAM_MEMBERS_NEEDED_WITHIN RETURN "FMMC_LOC_TMWT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = LOCATIONS_TEAM_MEMBERS_NEEDED_WITHIN_INC RETURN "FMMC_LOC_MPTI" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = LOCATIONS_TEAM_SIZE_THRESHOLD RETURN "FMMC_LOC_LTST" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = LOCATIONS_FULL_RANGE_CORONA_SIZE RETURN "FMMC_H_LOC_CRT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = LOCATIONS_CORONA_SCALER RETURN "MC_H_LOC_LCS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = LOCATIONS_USE_ALTERNATIVE_CAPTURE_LOGIC RETURN "FMMC_LOC_UACL" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = LOCATIONS_LOCATE_HEIGHT RETURN "MC_H_LOC_LZ" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = LOCATIONS_LOCATE_DEPTH RETURN "MC_H_LOC_LH" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = LOCATIONS_LOCATE_ALPHA RETURN "MC_H_LOC_LA" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = LOCATIONS_SPAWN_GROUP RETURN "MC_H_LOC_SPG" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = LOCATIONS_SPAWN_SUBGROUP RETURN "MC_H_LOC_SSG" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = LOCATIONS_FAKE_TEAM_LOC_OWNERSHIP RETURN "MC_H_LOC_FKOWN" ENDIF	
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = LOCATIONS_USE_PROCESSING_DELAY	RETURN "MC_H_LOC_LPDD"	 ENDIF
	ELIF sFMMCMenu.sActiveMenu = eFmmc_GenericEntityBlipOptions
		RETURN GET_ENTITY_BLIP_DESCRIPTION(sFMMCmenu)
	ELIF sFMMCMenu.sActiveMenu = eFmmc_LOCATION_BLIP_OPTIONS
		RETURN GET_LOCATION_BLIP_DESCRIPTION(sFMMCmenu)
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_ALL_TEAM_AT_LOCATE_MENU
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciALL_TEAM_AT_LOCATE_ALL_TEAM
            RETURN "MC_H_TAL_AT"
		ELSE
			RETURN "MC_H_TAL_TM"
		ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_LINE_MARKERS_MENU
		RETURN GET_DESCRIPTION_MENU_LINE_MARKERS(sFMMCMenu)
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_MISSION_LOC_ADDITIONAL_SPAWNING_SUBMENU
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciADDITIONAL_LOCATION_SPAWNING_CHECK_LOCAL_PLAYER_OUTFIT								RETURN "MC_H_SPWOO_OUT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciADDITIONAL_LOCATION_SPAWNING_REQUIRES_STARTING_OUTFIT_FOR_UNLIMITED_BLIP_RANGE		RETURN "MC_H_SPWOO_OUO" ENDIF
	ENDIF

	IF sFMMCMenu.sActiveMenu = eFmmc_PICKUP_BASE
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PICKUP_MENU_LIBRARY RETURN "MC_H_WEP0" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PICKUP_MENU_TYPE RETURN "MC_H_WEP1" ENDIF
		IF IS_ROCKSTAR_DEV()
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PICKUP_MENU_AMMO RETURN "MC_H_WEP3" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PICKUP_MENU_AMMO_TYPE RETURN "MC_H_WEP_AT" ENDIF 
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PICKUP_MENU_DAMAGE_MULTIPLIER RETURN "MC_H_WEP_DMGM" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PICKUP_MENU_CUSTOM_PICKUP_TYPE RETURN "MC_H_WP_CUST" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PICKUP_MENU_CUSTOM_MODEL_SEL	RETURN "MC_H_WP_CMOD" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) >= PICKUP_MENU_RESTRICTIONS_TEAM1
			AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) <= PICKUP_MENU_RESTRICTIONS_TEAM4
				RETURN "MC_H_WEP4"
			ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PICKUP_MENU_RUINER_ROCKETS_FOR_PICKUP RETURN "FMMC_WP_H_URR" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PICKUP_MENU_SPAWN_GROUP 		RETURN "MC_H_WEP_SPG" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PICKUP_MENU_SUB_SPAWN_GROUP	RETURN "MC_H_WEP_SSG" ENDIF
			
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PICKUP_MENU_PLAY_RESPAWN_SOUND	RETURN "MC_H_VEH_PPRS" ENDIF
			
			RETURN "MC_H_WEP2"
		ENDIF
	ENDIF
	IF sFMMCMenu.sActiveMenu = eFmmc_PICKUP_BLIP_OPTIONS
		RETURN GET_PICKUP_BLIP_DESCRIPTION(sFMMCmenu)
	ENDIF
	
	IF sFMMCMenu.sActiveMenu =eFmmc_ENEMY_BASE
	
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_LIBRARY RETURN "MC_H_PED00" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_TYPE RETURN "MC_H_PED0" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_VARIATION RETURN "MC_H_PED01" ENDIF
		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_RULE
			IF sFMMCmenu.iSelectedPedRule = FMMC_OBJECTIVE_LOGIC_NONE RETURN "MC_H_PED30" ENDIF
			IF sFMMCmenu.iSelectedPedRule = FMMC_OBJECTIVE_LOGIC_KILL RETURN "MC_H_PED31" ENDIF
			IF sFMMCmenu.iSelectedPedRule = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER RETURN "MC_H_PED32" ENDIF
			IF sFMMCmenu.iSelectedPedRule = FMMC_OBJECTIVE_LOGIC_CAPTURE RETURN "MC_H_PED33" ENDIF
			IF sFMMCmenu.iSelectedPedRule = FMMC_OBJECTIVE_LOGIC_PROTECT RETURN "MC_H_PED34" ENDIF
			IF sFMMCmenu.iSelectedPedRule = FMMC_OBJECTIVE_LOGIC_GO_TO RETURN "MC_H_PED35" ENDIF
			IF sFMMCmenu.iSelectedPedRule = FMMC_OBJECTIVE_LOGIC_CROWD_CONTROL RETURN "MC_H_PED36" ENDIF
			IF sFMMCmenu.iSelectedPedRule = FMMC_OBJECTIVE_LOGIC_CHARM RETURN "MC_H_PED37" ENDIF
			IF sFMMCmenu.iSelectedPedRule = FMMC_OBJECTIVE_LOGIC_LEAVE_ENTITY RETURN "MC_H_PED38" ENDIF			
		ENDIF
		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_FLASHLIGHT_IS_NG_ONLY RETURN "MC_H_PED_FNO" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_JOIN_GROUP_FROM_VAN RETURN "MC_H_PED_JGFV" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_COMBAT_OPTIONS RETURN "MC_H_PED_CMB" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_PATROL_RANGE RETURN "MC_H_PED2" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_BLIP_OPTIONS RETURN "MC_H_PED_BLP" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_FLEE_OPTIONS RETURN "MC_H_PED_FLE"		 ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_ANIMATIONS RETURN "MC_H_PED_ANM" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_HEALTH RETURN "FMMC_HLT_H" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_NEW_RULE RETURN "MC_H_PED8" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_RELATIONSHIPS RETURN "MC_H_PED9" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_ASS_OBJECTIVE RETURN "MC_H_PED10" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_ARRIVED_ON_DEATH RETURN "MC_H_PED12" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_GO_TO_CONSEQUENCES RETURN "FMMC_ASSRL8H" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_CRITICAL RETURN "MC_H_PEDC" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_AGGRO_RULE RETURN "MC_H_AGRORULE" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_DIALOGUE RETURN "MC_H_PEDD" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_JUMP_TO_OBJECTIVE RETURN "FMMC_JTO_DESCR" ENDIF
		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_VEHICLE_OPTIONS	RETURN "MC_H_PED_VEHO" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_SPOOK_OPTIONS 		RETURN "MC_H_PED_SPKO" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_DISABLE_OPTIONS 	RETURN "MC_H_PED_DISO" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_OVERHEAD_OPTIONS 	RETURN "MC_H_PED_OVHO" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_CLOAKING_OPTIONS 	RETURN "MC_H_PED_CLKO" ENDIF
		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_ARRIVAL_RANGE RETURN "MC_H_PED_ARR" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_INVENTORY RETURN "FMMC_INV_H" ENDIF		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_LOOK_AT_NEARBY_PLAYERS RETURN "MC_H_PED_LNP" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_HEADTRACK_CLOSEST_PLAYER_NO_PERCEPTION RETURN "MC_H_PED_HPN" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_WARP_TIMER RETURN "MC_H_PED_PWT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_CASH_AMOUNT RETURN "MC_H_PED_CSH" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_GO_TO_CONSEQUENCES_TRIGGER_ON_TASK_COMPLETE RETURN "FMMC_GOTOC_04D"		 ENDIF
		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_CANT_BE_DAMAGED_BY_PLAYERS RETURN "MC_H_PED_CBDBP" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_NEVER_LOSE_TARGET RETURN "MC_H_PED_FNLT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_USE_DAMAGE_SCORE_CONTRIBUTION RETURN "MC_H_PED_UDFS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_FORCE_AGGRO_AVOIDANCE_ON_NON_AGGRO RETURN "MC_PED_FAV" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_ENABLE_PLOUGH_THROUGH_MODE RETURN "MC_PED_PTM" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_CONTINUITY_ID RETURN "MC_H_MICO_ID" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_BLOCK_DIALOGUE_TRIGGER RETURN "MC_H_PBDT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_COP_DECOY_OPTIONS RETURN "MC_H_PCDO" ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFMMC_CUSTOM_SHARD_OPTIONS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_CUSTOM_SHARD_COLOUR_REPLACEMENT RETURN "FMMC_CSO_H_VCR" ENDIF
		RETURN "MC_H_CSTM"
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_ENEMY_MINIGAME_OPTIONS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_MINIGAME_OPT_PHOTO_RANGE RETURN "MC_H_PED_PHOR" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_MINIGAME_OPT_CROWD_CONTROL_INIT_BY_ANY_TEAM RETURN "MC_H_PED_CCAT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_MINIGAME_OPT_CROWD_CONTROL_ID RETURN "MC_H_PED_CCID" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_MINIGAME_OPT_CROWD_CONTROL_ACTION RETURN "MC_H_PED_CCA" ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_GO_TO_CONSEQUENCES
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_GO_TO_CONSEQUENCES_TEAM RETURN "FMMC_GOTOC_00D" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_GO_TO_CONSEQUENCES_CAN_FAIL_MISSION RETURN "FMMC_GOTOC_01D" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_GO_TO_CONSEQUENCES_PASS_ON_RULE RETURN "FMMC_GOTOC_02D" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_GO_TO_CONSEQUENCES_TEAM_NOT_ON_RULE RETURN "FMMC_GOTOC_03D" ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_PED_SPAWN_OPTIONS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_SPAWN_OPT_RESPAWN RETURN "MC_H_PED5" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_SPAWN_OPT_RESPAWN_RANGE RETURN "FMMCCPD_P7H" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_SPAWN_OPT_RESPAWN_DELAY RETURN "H_RESP_DELY" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_SPAWN_OPT_SPAWN_ON_SCENE RETURN "FMMC_SPOP_0D" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_SPAWN_OPT_SPAWN_ON_SHOT RETURN "FMMC_SPOP_1D" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_SPAWN_OPT_SPAWN_NEAR_ENTITY RETURN "MC_H_SNE_P" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_SPAWN_OPT_OVERRIDE_SPAWN_POINT RETURN "MC_H_VEH_OVSP" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_SPAWN_OPT_SECONDARY_SPAWN_POINT RETURN "FMMC_SPOP_2D" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_SPAWN_OPT_SPAWN_ON_CHECKPOINT_NONE RETURN "MC_H_SPN_CHK" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_SPAWN_OPT_SPAWN_ON_CHECKPOINT_0 RETURN "FMMC_SPOP_3D" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_SPAWN_OPT_SPAWN_ON_CHECKPOINT_1 RETURN "FMMC_SPOP_4D" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_SPAWN_OPT_SPAWN_ON_CHECKPOINT_2 RETURN "FMMC_SPOCK3D" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_SPAWN_OPT_SPAWN_ON_CHECKPOINT_3 RETURN "FMMC_SPOCK4D" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_SPAWN_OPT_SPOOK_ON_SPAWN RETURN "FMMC_SPOP_5D" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_SPAWN_OPT_DYNAMIC_APARTMENT_SPAWN_OVERRIDE RETURN "MC_H_PED_DAS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_SPAWN_OPT_PREVENT_SPAWN_PLAYER_IN_RANGE RETURN "MC_H_PED_DSPIA" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_SPAWN_OPT_USE_RESTART_POINTS_AS_RANDOM_SPAWN RETURN "MC_H_PED_UCHRS" ENDIF		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_SPAWN_OPT_START_IN_COVER RETURN "MC_H_PED_COVE" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_SPAWN_OPT_ROLLING_START RETURN "MC_H_PED_RLSTR" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_SPAWN_OPT_STARTS_DEAD RETURN "MC_H_PED_DEAD" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_SPAWN_OPT_STARTS_DEAD_PHYSICS RETURN "MC_H_PED_DEYD" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_SPAWN_OPT_RESTART_CHECKPOINT_ON_FOOT RETURN "MC_H_PED_RSOF" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_SPAWN_OPT_SPAWN_PLAYER_NUM	RETURN "MC_H_PED_PLR" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_SPAWN_OPT_ZONE_BLOCKING_ENTITY_SPAWN	RETURN "MC_H_SPWNZOBLK" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_SPAWN_OPT_ZONE_BLOCKING_ENTITY_SPAWN_PLAYERS	RETURN "MC_H_SPWNZOBLP" ENDIF		
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_PED_FOLLOW_OPTIONS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_FOLLOW_OPT_FOLLOW_PLAYER RETURN "MC_H_PED7" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_FOLLOW_OPT_FOLLOW_TEAM_OPTIONS RETURN "FMMC_FOOP_0D" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_FOLLOW_OPT_GROUP_ALONGSIDE RETURN "MC_H_PED13" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_FOLLOW_OPT_GROUP_FOLLOW_RANGE RETURN "FMMC_FOOP_4D" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_FOLLOW_OPT_FOLLOW_RANGE RETURN "FMMC_FOOP_5D" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_FOLLOW_OPT_ALLOW_TASKING_WHILE_FOLLOWING RETURN "MC_H_PED_ATWF" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_FOLLOW_OPT_PLAY_AMBIENT_DIALOGUE_ON_JOIN RETURN "MC_H_PED_PALJG" ENDIF	
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_PED_FOLLOW_TEAM_OPTIONS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_FOLLOW_TEAM_OPT_TEAM RETURN "FMMC_FOOP_03D" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_FOLLOW_TEAM_OPT_FOLLOW_ON_ANY_RULE RETURN "FMMC_FOOP_04D" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_FOLLOW_TEAM_OPT_CLEAR_FOLLOW_RULES RETURN "FMMC_FOOP_05D" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) >= PED_FOLLOW_TEAM_OPT_FOLLOW_ON_RULE RETURN "FMMC_FOOP_01D" ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_TEAM_REL_GROUPS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_RELATIONSHIPS_NEW_RELATIONSHIP_GROUP_ON_RULE_MENU RETURN "MC_H_PED_NRGR" ENDIF		
		
		INT iPedToChange = sFMMCMenu.iSelectedEntity
		IF iPedToChange = -1
			iPedToChange = g_FMMC_STRUCT_ENTITIES.iNumberOfPeds
		ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) >= ENEMY_RELATIONSHIPS_TEAM_0
		AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) <= ENEMY_RELATIONSHIPS_TEAM_3
			IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedToChange].iTeam[GET_CREATOR_MENU_SELECTION(sFMMCmenu)-1] = ciPED_RELATION_SHIP_HATE RETURN "MC_H_PED91" ENDIF
			IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedToChange].iTeam[GET_CREATOR_MENU_SELECTION(sFMMCmenu)-1] = ciPED_RELATION_SHIP_DISLIKE RETURN "MC_H_PED92" ENDIF
			IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedToChange].iTeam[GET_CREATOR_MENU_SELECTION(sFMMCmenu)-1] = ciPED_RELATION_SHIP_LIKE RETURN "MC_H_PED93" ENDIF
		ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFMMC_PHONE_SERVICES
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MENU_PHONE_SERVICES_MERCENARIES RETURN "MC_H_DPS_MER" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MENU_PHONE_SERVICES_AMMO_DROP RETURN "MC_H_DPS_AMO" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MENU_PHONE_SERVICES_REVEAL_PLAYERS RETURN "MC_H_DPS_RVL" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MENU_PHONE_SERVICES_REQUEST_VEHICLE RETURN "MC_H_DPS_VEH" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MENU_PHONE_SERVICES_HELI_PICKUP RETURN "MC_H_DPS_HCP" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MENU_PHONE_SERVICES_BACKUP_HELI RETURN "MC_H_DPS_HCB" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MENU_PHONE_SERVICES_AIRSTRIKE RETURN "MC_H_DPS_AST" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MENU_PHONE_DO_NOT_HANG_UP_IN_WATER RETURN "MC_H_DPS_WAT" ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFMMC_PHONE_EMP_SETTINGS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MENU_PHONEEMP_AVAILABLE_FROM_CASINO_SETUP 	RETURN "MC_H_PEMP_CSN"	ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MENU_PHONEEMP_DURATION 						RETURN "MC_H_PEMP_TI" 	ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MENU_PHONEEMP_PERCEPTION_RANGE_PERCENTAGE 	RETURN "MC_H_PEMP_SR" 	ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MENU_PHONEEMP_AUTOMATICALLY_TOGGLE_NV 		RETURN "MC_H_PEMP_NVT" 	ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFMMC_GENERIC_OVERRIDE_POSITION
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = POS_OVERRIDE_USE_OVERRIDE
			RETURN "MC_H_OVR_LOK"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = POS_OVERRIDE_X_VALUE
		OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = POS_OVERRIDE_Y_VALUE
		OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = POS_OVERRIDE_Z_VALUE
			RETURN "MC_H_OVR_AXIS"
		ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_GENERIC_OVERRIDE_ROTATION
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ROT_OVERRIDE_USE_OVERRIDE
			RETURN "MC_H_OVR_LOKR"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ROT_OVERRIDE_X_VALUE
		OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ROT_OVERRIDE_Y_VALUE
		OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ROT_OVERRIDE_Z_VALUE
			RETURN "MC_H_OVR_AXISR"
		ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFMMC_BONUS_CASH_REWARDS_MENU
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MENU_BONUS_CASH_REWARDS_TIME RETURN "MC_H_BCR_TIM" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MENU_BONUS_CASH_REWARDS_TIME_REQUIRED RETURN "MC_H_BCR_TIMR" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MENU_BONUS_CASH_REWARDS_TIME_TEAM RETURN "MC_H_BCR_TIMT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MENU_BONUS_CASH_REWARDS_TIME_RULE RETURN "MC_H_BCR_TMRL" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MENU_BONUS_CASH_REWARDS_SWAT RETURN "MC_H_BCR_SWAT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MENU_BONUS_CASH_REWARDS_NO_OF_KILLS RETURN "MC_H_BCR_NOK" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MENU_BONUS_CASH_REWARDS_NO_OF_KILLS_REQUIRED RETURN "MC_H_BCR_NOKR" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MENU_BONUS_CASH_REWARDS_CASH_BAG_DAMAGE RETURN "MC_H_BCR_CBD" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MENU_BONUS_CASH_REWARDS_PED_DAMAGE RETURN "MC_H_BCR_PD" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MENU_BONUS_CASH_REWARDS_MAX_PED_DAMAGE RETURN "MC_H_BCR_PDR" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MENU_BONUS_CASH_REWARDS_VEH_DAMAGE RETURN "MC_H_BCR_VD" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MENU_BONUS_CASH_REWARDS_MAX_VEH_DAMAGE RETURN "MC_H_BCR_VDR" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MENU_BONUS_CASH_REWARDS_PLAYER_HEALTH RETURN "MC_H_BCR_PH" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MENU_BONUS_CASH_REWARDS_MAX_PLAYER_HEALTH RETURN "MC_H_BCR_PHR" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MENU_BONUS_CASH_REWARDS_REWARD_AMOUNT RETURN "MC_H_BCR_RA" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MENU_BONUS_CASH_REWARDS_MID_MISSION_TIMER RETURN "MC_H_BCR_MMT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MENU_BONUS_CASH_REWARDS_MMT_TEAM RETURN "MC_H_BCR_MMTT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MENU_BONUS_CASH_REWARDS_MMT_START RETURN "MC_H_BCR_MMTS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MENU_BONUS_CASH_REWARDS_MMT_END RETURN "MC_H_BCR_MMTE" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MENU_BONUS_CASH_REWARDS_MMT_TIME RETURN "MC_H_BCR_MMTR" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MENU_BONUS_CASH_REWARDS_VOID_ON_DEATH RETURN "MC_H_BCR_VOD" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MENU_BONUS_CASH_REWARDS_NO_HACK_FAILS RETURN "MC_H_BCR_MGF" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MENU_BONUS_CASH_REWARDS_NO_OF_HACK_FAILS_REQUIRED RETURN "MC_H_BCR_MGFR" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MENU_BONUS_CASH_REWARDS_NO_OF_HEADSHOTS RETURN "MC_H_BCR_NOH" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MENU_BONUS_CASH_REWARDS_NO_OF_HEADSHOTS_REQUIRED RETURN "MC_H_BCR_NOHR" ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFMMC_TEAM_POST_MISSION_PLAYER_SPAWN
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = FMMC_POST_MISSION_LOCATION RETURN "MC_H_PMP_SPL" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = FMMC_POST_MISSION_RADIUS RETURN "MC_H_PMP_SPR" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = FMMC_POST_MISSION_POINT_OF_INTEREST RETURN "MC_H_PMP_POI" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = FMMC_POST_MISSION_IGNORE_EXCLUSIONS RETURN "MC_H_PMP_IGN" ENDIF		
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_JUMP_TO_OBJECTIVE_PLAYER_RULES_MENU
	OR sFMMCMenu.sActiveMenu = eFmmc_JUMP_TO_OBJECTIVE_LOCATION_MENU
	OR sFMMCMenu.sActiveMenu = eFmmc_JUMP_TO_OBJECTIVE_PED_MENU
	OR sFMMCMenu.sActiveMenu = eFmmc_JUMP_TO_OBJECTIVE_VEHICLE_MENU
	OR sFMMCMenu.sActiveMenu = eFmmc_JUMP_TO_OBJECTIVE_OBJECT_MENU
		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 0
		OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 2
		OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 4
		OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 6
			RETURN "MC_H_JOP"
		ELSE
			RETURN "MC_H_JOF"
		ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_ENEMY_FLEE_OPTIONS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_FLEE_OPTIONS_WILL_FLEE RETURN "MC_H_PED2D" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_FLEE_OPTIONS_DONT_FLEE_DROP_OFFS RETURN "FMMCCPD_H6" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_FLEE_OPTIONS_FLEE_IN_VEHICLE RETURN "MC_H_FLE_FIV" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_FLEE_OPTIONS_FLEE_IF_VEHICLE_IS_STUCK RETURN "MC_H_PED_FVS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_FLEE_OPTIONS_FLEE_ON_CLEANUP RETURN "MC_H_PED_FCU" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_FLEE_FLEE_WHEN_UNARMED RETURN "MC_H_PED_FWUA" ENDIF
	ENDIF
		
	IF sFMMCMenu.sActiveMenu = eFmmc_ENEMY_BLIP_OPTIONS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_BLIP_OPTIONS_BLIP RETURN "FMMC_A_BLIP" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_BLIP_OPTIONS_SET_UP RETURN "MC_H_GBLIP" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_BLIP_OPTIONS_AI_BLIPS_ONLY RETURN "MC_H_PEDAIB" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_BLIP_OPTIONS_SOLID_BLIP RETURN "MC_H_PED_SBL" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_BLIP_OPTIONS_BLIP_ON_FOLLOW RETURN "" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_BLIP_OPTIONS_SHOW_BLIP_CONE RETURN "MC_H_PED_BLCN" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_BLIP_OPTIONS_BLIP_NOTICE_RANGE RETURN "MC_H_PED_BNR" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_BLIP_OPTIONS_BLIP_ONLY_IN_RANGE RETURN "MC_H_PED_BON" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_BLIP_OPTIONS_DISABLE_HELICOPTER_BLIP RETURN "MC_H_PED_DHB" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_BLIP_OPTIONS_BLIP_ON_AGGRO RETURN "" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_BLIP_HEIGHT_INDICATOR_IN_RANGE RETURN "MC_H_PED_BHR" ENDIF 
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_BLIP_SCRIPTED_GUN_TURRET_ONLY RETURN "MC_H_PED_BOIC" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_BLIP_ONLY_BLIP_IN_SAME_INTERIOR RETURN "MC_H_PED_OBISI" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_BLIP_HIDE_MARKER_FROM_ORBITAL_CANNON RETURN "MC_H_PED_OBTH"	 ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_BLIP_HIDE_MARKER_FROM_ORBITAL_CANNON_IN_VEH RETURN "MC_H_PED_OBTV" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_BLIP_USE_FAIL_NAME_FOR_BLIP RETURN "MC_H_PED_FLNMM" ENDIF	
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_BLIP_FORCE_BLIP_WHEN_INVALID_VEHICLE_LEADER RETURN "MC_H_PED_FBWIVL" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_BLIP_CLEANUP_AI_BLIPS RETURN "MC_H_PED_CAB" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_BLIP_OPTIONS_HIDE_WHEN_AGGROD	RETURN "MC_H_PED_RMBA" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_BLIP_IGNORE_VEHICLE_BLIP	RETURN "MC_H_PED_IVB" ENDIF 
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_ENEMY_NEW_RELATIONSHIP_GROUP_ON_RULE_SETTINGS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_OPTIONS_NEW_RELATIONSHIP_GROUP_ON_RULE 	RETURN "MC_H_PED_NRELR" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_OPTIONS_NEW_RELATIONSHIP_GROUP_TYPE 		RETURN "MC_H_PED_NRELT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_OPTIONS_NEW_RELATIONSHIP_MAKE_COP 			RETURN "MC_H_PED_NRELC" ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_ENEMY_COMBAT_OPTIONS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_COMBAT_OPTIONS_WEAPON RETURN "MC_H_PED1" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_COMBAT_OPTIONS_WEAPON_HOLSTERED RETURN "MC_H_PEDH" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_COMBAT_OPTIONS_ACCURACY RETURN "MC_H_PED4" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_COMBAT_OPTIONS_LG_ACCURACY RETURN "" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_COMBAT_OPTIONS_STATIONARY_UNTIL_COMBAT RETURN "MC_H_PED2A" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_COMBAT_OPTIONS_VEHICLE_DEFENSIVE_AREA RETURN "MC_H_PED_DAIV" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_COMBAT_OPTIONS_FREE_MOVEMENT_DURING_COMBAT RETURN "MC_H_PED2B" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_COMBAT_OPTIONS_COMBAT_STYLE RETURN "MC_H_PED2C" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_COMBAT_OPTIONS_USE_COMBAT_MODE_WHEN_ARMED RETURN "MC_H_GWR_CBT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_COMBAT_OPTIONS_COMBAT_MODE_IN_PLAYER_GROUP RETURN "MC_H_GWR_CIG" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_COMBAT_OPTIONS_HATE_ALL_OTHER_GROUPS RETURN "MC_H_PED_HOG" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_COMBAT_OPTIONS_DISABLE_BLIND_FIRING RETURN "" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_COMBAT_OPTIONS_REACT_TO_FRIENDLY_GUNFIRE RETURN "" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_COMBAT_OPTIONS_DISABLE_BUZZARD_ROCKET_USE RETURN "FMMC_PED_DBRUD" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_COMBAT_OPTIONS_DISABLE_DRIVEBYS RETURN "" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_COMBAT_OPTIONS_ROCKETS_ONLY_AGAINST_VEHICLES RETURN "MC_H_PEDR" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_COMBAT_OPTIONS_USE_ROCKETS_IN_JETS RETURN "MC_H_PED_URJ" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_COMBAT_OPTIONS_PREFERS_AIR_COMBAT RETURN "MC_H_PED_PAC" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_COMBAT_OPTIONS_PREFERS_NON_AIRCRAFT_TARGETS RETURN "MC_H_PED_PGT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_COMBAT_OPTIONS_ALLOW_DOG_FIGHTING RETURN "MC_H_PED_ADF" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_COMBAT_OPTIONS_LOWER_FIRING_RATE RETURN "MC_H_ASS_LFR" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_COMBAT_OPTIONS_TAKE_COVER_ACTION_ALWAYS RETURN "MC_H_ASS_TCA" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_COMBAT_OPTIONS_DONT_COMBAT_ON_ACTIVATION_RANGE RETURN "MC_H_ASS_DCA" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_COMBAT_OPTIONS_MAX_SHOOTING_RANGE RETURN "MC_H_PED_MSR" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_COMBAT_OPTIONS_USE_STRICT_LOS_AIM_CHECKS RETURN "MC_H_PED_SLOS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_COMBAT_OPTIONS_PED_INFORM_RANGE RETURN "MC_H_PED_PIR" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_COMBAT_OPTIONS_ONLY_RECEIVE_EVENTS_IN_INFORM_RANGE RETURN "MC_H_PED_OIR" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_COMBAT_OPTIONS_EXTENDED_LOS RETURN "FMMCPD_XLOSH" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_COMBAT_OPTIONS_EXTEND_LOS_ON_COMBAT RETURN "MC_H_PED_ELSC" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_COMBAT_OPTIONS_PERCEPTION RETURN "MC_H_PED_PER" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_COMBAT_OPTIONS_COMBATVEHICLE_DONT_CHANGE_SHOOTING_SKILL RETURN "MC_H_PED_CVS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_COMBAT_OPTIONS_COMBAT_STYLE_CHANGE_TEAM RETURN "MC_H_PED_CSCT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_COMBAT_OPTIONS_COMBAT_STYLE_CHANGE_RULE RETURN "MC_H_PED_CSCR" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_COMBAT_OPTIONS_COMBAT_STYLE_CHANGE_NEW_STYLE RETURN "MC_H_PED_CSCS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_COMBAT_OPTIONS_COMBAT_STYLE_CHANGE_FREE_MOVEMENT RETURN "MC_H_PED_CSCF" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_COMBAT_OPTIONS_COMBAT_MIN_DIST RETURN "MC_H_PED_MVDT" ENDIF		
		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_COMBAT_OPTIONS_PREVENT_VEHICLE_WEAPON_USAGE RETURN "MC_H_PED_PVWU" ENDIF	
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_COMBAT_OPTIONS_REENABLE_VEHICLE_WEAPONS_WHEN_ALONE_IN_VEH RETURN "MC_H_PED_RVWA" ENDIF	
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_COMBAT_OPTIONS_IGNORE_MAX_NUMBER_OF_MELEE_COMBATANTS RETURN "MC_H_PED_IGNM" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_COMBAT_OPTIONS_DISABLE_RAGDOLLING_FROM_PLAYER_IMPACTS RETURN "MC_H_PED_RGDL" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_COMBAT_OPTIONS_DISABLE_COWERING RETURN "MC_H_PED_DSCW" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_COMBAT_OPTIONS_ENABLE_AUTO_COMBAT_STATE_IN_COMBAT_VEHICLE RETURN "MC_H_PED_AUCM" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_COMBAT_ALLOW_PED_IN_VEH_COORD_ATTACK RETURN "FMMC_PED_AVCA" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_COMBAT_REACT_TO_EMP_FLASHLIGHT 	RETURN "MC_H_PED_RTEF" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_COMBAT_REMOVE_DEFENSIVE_AREA_ON_COMBAT 	RETURN "MC_H_PED_RDAC" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_COMBAT_FORCE_ACTION_MODE_AND_UNHOLSTER_WEAPON 	RETURN "MC_H_PED_ACTH" ENDIF		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_COMBAT_DISABLE_PED_BEING_TARGETABLE	RETURN "MC_H_PED_DSTR" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_COMBAT_FORCE_PED_TO_BE_CIVILIAN	RETURN "MC_H_PED_CIVY" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_COMBAT_STRAFE_AND_SHOOT			RETURN "MC_H_PED_USAS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_COMBAT_USE_KINE_PHYSICS_ON_COVER	RETURN "MC_H_PED_UKPC" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_COMBAT_DONT_CRY_WHEN_STUNNED		RETURN "MC_H_PED_DCWS" ENDIF
	ENDIF
		
	IF sFMMCMenu.sActiveMenu = eFmmc_ENEMY_FIXATION_SETTINGS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_FIXATION_SETTINGS_PED_FIXATION_INDEX RETURN "MC_H_FIXA_0" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_FIXATION_SETTINGS_PLAYER_INTERRUPT_RANGE RETURN "MC_H_FIXA_1" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_FIXATION_SETTINGS_HATE_PLAYERS_ON_INTERVENTION RETURN "MC_H_FIXA_2" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_FIXATION_SETTINGS_START_RULE RETURN "MC_H_FIXA_3" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_FIXATION_SETTINGS_END_RULE RETURN "MC_H_FIXA_4" ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_ENTITY_CUSTOM_POINTS_OPTIONS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_CUSTOM_POINTS_SETTINGS_POINTS_TO_GIVE RETURN "MC_H_PNTS_0" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_CUSTOM_POINTS_SETTINGS_RULE_START RETURN "MC_H_PNTS_1" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_CUSTOM_POINTS_SETTINGS_RULE_END RETURN "MC_H_PNTS_2" ENDIF
	ENDIF	
	
	IF sFMMCMenu.sActiveMenu = eFmmc_ENEMY_HELI_GOTO_OPTIONS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_HELICOPTER_HEIGHT RETURN "FMMC_PED_MFHD" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_ENABLE_CUSTOM_HELI_GOTOS RETURN "FMMC_PED_HCGD" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_HELI_DONT_DO_AVOIDANCE RETURN "FMMC_PED_HAD" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_HELI_DISABLE_HEIGHT_AVOIDANCE RETURN "FMMC_PH_DMA" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_HELI_FORCE_HEIGHT_AVOIDANCE RETURN "FMMC_PH_FHMA" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_HELI_MIN_HEIGHT_ABOVE_TERRAIN RETURN "FMMC_PH_MHATI" ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_ENEMY_ANIMATIONS_MENU
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_ANIMS_MENU_IDLE_ANIM_TEAM RETURN "MC_H_ANM_IDT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_ANIMS_MENU_IDLE_ANIM_RULE RETURN "MC_H_ANM_IDR" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_ANIMS_MENU_IDLE_ANIM_ON_MIDPOINT RETURN "MC_H_ANM_IDM" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_ANIMS_MENU_IDLE_ANIM_DURATION RETURN "MC_H_ANM_IDD" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_ANIMS_MENU_SPECIAL_ANIM_DURATION RETURN "MC_H_ANM_SDD" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_ANIMS_MENU_IDLE_ANIM_BREAKOUT_RANGE RETURN "MC_H_ANM_BRKI" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_ANIMS_MENU_IDLE_PRIORITIZE_OVER_TASK RETURN "MC_H_ANM_IDLT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_ANIMS_MENU_SPECIAL_ANIM_TEAM RETURN "MC_H_ANM_SAT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_ANIMS_MENU_SPECIAL_ANIM_RULE RETURN "MC_H_ANM_SAR" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_ANIMS_MENU_SPECIAL_ANIM_ON_MIDPOINT RETURN "MC_H_ANM_SAM" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_ANIMS_MENU_SPECIAL_ANIM_RANGE RETURN "MC_H_ANM_SATR" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_ANIMS_MENU_SPECIAL_ANIM_RANGE_TEAM RETURN "MC_H_ANM_SART" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_ANIMS_MENU_SPECIAL_ANIM_BREAKOUT_RANGE RETURN "MC_H_ANM_BRKS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_ANIMS_MENU_SPECIAL_ANIM_BREAKOUT_TIME RETURN "MC_H_ANM_TIBRS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_ANIMS_MENU_SPECIAL_PRIORITIZE_OVER_TASK RETURN "MC_H_ANM_SPCT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_ANIMS_MENU_SPECIAL_BREAKOUT_WHEN_SPOOKED RETURN "MC_H_ANM_BRKP" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_ANIMS_MENU_DONT_DISRUPT_ANIM RETURN "MC_H_ANM_DDA" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_ANIMS_MENU_IDLE_DEAD
			IF IS_BIT_SET(sFMMCMenu.iActorMenuBitsetFive, ciPED_BSFive_StartDead)
				RETURN "MC_H_ANM_DEAD"
			ELSE
				RETURN "MC_H_ANM_NDED"
			ENDIF
		ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_ANIMS_MENU_SPOOK_AFTER_ANIMATION RETURN "MC_H_ANM_SPOK" ENDIF		
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_ENEMY_ANIMATIONS_MENU_SPECIAL_ANIM_RANGE_TEAM
		INT iTeam
		FOR iTeam = 0 TO (FMMC_MAX_TEAMS - 1)
			RETURN "MC_H_ANM_SART0"
		ENDFOR
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_ENEMY_HEALTH_MENU			
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_HEALTH_PRECISE_INITIAL_HEALTH RETURN "MC_HHLT_PINH" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_HEALTH_INITIAL_HEALTH RETURN "MC_H_HLT_0" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_HEALTH_LG_HEALTH RETURN "MC_H_HLT_LGH" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_HEALTH_SUFFER_CRIT_OVERRIDE RETURN "MC_H_HLT_1" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_HEALTH_ONLY_DAMAGED_BY_PLAYERS RETURN "MC_H_HLT_DBP" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_HEALTH_ONE_HIT_KILL RETURN "MC_H_HLT_OHK" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_HEALTHBAR_TURN_RED_AT_PERCENT RETURN "MC_H_ENT_HBTR" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_HEALTH_CAN_ONLY_BE_DAMAGED_BY_PLAYERS RETURN "MC_H_HLT_DBP" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_IGNORE_DIFFICULTY_HEALTH_MODIFIER RETURN "MC_H_HLT_IDHM" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_HEALTH_PERCENT_DAMAGE_FOR_RULE RETURN "MC_H_HLT_DRFR" ENDIF
	ENDIF	
	
	IF sFMMCMenu.sActiveMenu = eFmmc_ENEMY_INVENTORY_MENU
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_INVENTORY_DROP_INVENTORY_ON_DEATH RETURN "MC_H_INV_DID" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_INVENTORY_GIVE_WEAPON_ON_RULE RETURN "MC_H_INV_GWR" ENDIF
		RETURN "MC_H_INV_0"
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_ENEMY_INVENTORY_GIVE_WEAPON_ON_RULE_MENU
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_INVENTORY_GIVE_WEAPON_ON_RULE_WEAPON RETURN "MC_H_GWR_WPN" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_INVENTORY_GIVE_WEAPON_ON_RULE_TEAM RETURN "MC_H_GWR_TEA" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_INVENTORY_GIVE_WEAPON_ON_RULE_RULE RETURN "MC_H_GWR_RUL" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_INVENTORY_GIVE_WEAPON_ON_RULE_ON_MIDPOINT RETURN "MC_H_GWR_MDP" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_INVENTORY_GIVE_WEAPON_ON_RULE_ON_AGGRO RETURN "MC_H_GWR_AGG" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_INVENTORY_GIVE_WEAPON_ON_RULE_HOLSTERED RETURN "MC_H_GWR_HOL" ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_ENEMY_ASS_RULE_ACTION_ACTIVATION_MENU
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_ACTION_ACTIVATION_SPHERICAL_RANGE RETURN "MC_H_ACA_ACR" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_ACTION_ACTIVATION_AA_WIDTH RETURN "MC_H_ACA_WDTH" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_ACTION_ACTIVATION_AA_MIN_HEIGHT RETURN "MC_H_ACA_MINH" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_ACTION_ACTIVATION_AA_MAX_HEIGHT RETURN "MC_H_ACA_MAXH" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_ACTION_ACTIVATION_LOS_CHECK RETURN "MC_H_ACA_LOS" ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_ASS_OBJECTIVE_MENU_SET_PED_GOTO_POS
	OR sFMMCMenu.sActiveMenu = eFmmc_ASS_OBJECTIVE_MENU_SET_POOL_GOTO_POS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_PRIMARY_GOTO_MENU_ACTION_ON_GOTO_INDEX
			RETURN "FMMC_AOGT_AOGID"
		ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_PRIMARY_GOTO_MENU_PAUSE_GOTO_ON_SPEECH
			RETURN "MC_H_AOGT_PGTS"
		ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_VEHICLE_RESTART_POINTS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = RESTART_POINTS_RESTART_WITH_TRAILER RETURN "FMMC_VEH_RSWTD" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = RESTART_POINTS_DONT_RESTART_WITH_TRAILER_MISSION_START RETURN "FMMC_VEH_DRWTD" ENDIF
		RETURN "MC_H_VEH_RSP"
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_ENEMY_STOP_TASKS_MENU
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_STOP_TASKS_TEAM RETURN "MC_H_STT_TM" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_STOP_TASKS_RULE RETURN "MC_H_STT_RL" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_STOP_TASKS_ON_MIDPOINT RETURN "MC_H_STT_MID" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_STOP_TASKS_WHEN_TRAILER_DETACHED RETURN "MC_H_STT_TR1" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_STOP_TASKS_CLEAN_UP_PED RETURN "MC_H_STT_TR2" ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_ACTOR_DIALOGUE_MENU
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ACTOR_DIALOGUE_LOS_CHECK RETURN "MC_H_DLG_LOS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ACTOR_DIALOGUE_COWERING_SPEECH RETURN "MC_H_DLG_CSP" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ACTOR_DIALOGUE_WIDTH RETURN "MC_H_DLG_WDTH" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ACTOR_DIALOGUE_MIN_HEIGHT RETURN "MC_H_DLG_MINH" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ACTOR_DIALOGUE_MAX_HEIGHT RETURN "MC_H_DLG_MAXH" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ACTOR_DIALOGUE_COMBAT_FOREVER RETURN "MC_H_DLG_ACF" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ACTOR_DIALOGUE_BYPASS_TURN_FACE_TASKING RETURN "MC_H_DLG_BTFT" ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = efMMC_APARTMENT_WARP
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = APARTMENT_WARP_SET_ENTRY_LOCATION RETURN "MC_H_WRP_SET" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = APARTMENT_WARP_SET_WARP_BUILDING RETURN "MC_H_WRP_LOC" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = APARTMENT_WARP_WARP_TO_APARTMENT RETURN "MC_H_WRP_WRP" ENDIF
		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = APARTMENT_WARP_FIRST_RULE_TEAM_1
		OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = APARTMENT_WARP_FIRST_RULE_TEAM_2
		OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = APARTMENT_WARP_FIRST_RULE_TEAM_3
		OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = APARTMENT_WARP_FIRST_RULE_TEAM_4
			RETURN "MC_H_WRP_FRL"
		ENDIF
		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = APARTMENT_WARP_LAST_RULE_TEAM_1
		OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = APARTMENT_WARP_LAST_RULE_TEAM_2
		OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = APARTMENT_WARP_LAST_RULE_TEAM_3
		OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = APARTMENT_WARP_LAST_RULE_TEAM_4
			RETURN "MC_H_WRP_LRL"
		ENDIF
		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = APARTMENT_WARP_HIDE_MARKER RETURN "MC_H_WRP_HMRK" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = APARTMENT_WARP_DRAW_BLIP RETURN "MC_H_WRP_DBLP" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = APARTMENT_WARP_DRAW_GPS_ROUTE RETURN "MC_H_WRP_DGPS" ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_ELEVATORS
		RETURN GET_MENU_ELEVATORS_DESCRIPTIONS(sFMMCmenu)
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_ELEVATOR_PLAYER_WARPS
		RETURN GET_MENU_ELEVATOR_PLAYER_WARPS_DESCRIPTIONS(sFMMCmenu)
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_PED_AGGRO_RULE
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_AGGRO_TEAM_SETTINGS RETURN "FMMC_TEAGS_08" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_AGGRO_INCLUDE_ALL_PEDS RETURN "MC_H_AGRO_ALL" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_AGGRO_INCLUDES_EQUIPPED_WEAPONS RETURN "MC_H_AGRO_WEAP" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_AGGRO_IGNORES_VEHICLE_BLOCKING RETURN "MC_H_AGRO_VEH" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_AGGRO_FAILS_ON_AGGRO RETURN "MC_H_AGRO_NOTF" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_AGGRO_TRIGGER_TASKS_ON_AGGRO RETURN "MC_H_AGRO_TTA" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_AGGRO_TRIGGER_ON_HEARING RETURN "MC_H_AGRO_TOH" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_AGGRO_DELAY_ON_FAIL RETURN "MC_H_AGRO_DOF" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_AGGRO_SPRINT_ON_AGGRO RETURN "MC_H_AGRO_SOA" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_AGGRO_TRIGGERS_ON_FINDING_BODY RETURN "MC_H_AGRO_TFB" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_AGGRO_IGNORE_SHOT_FIRED_EVENT RETURN "MC_H_AGRO_ISF" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_AGGRO_AGGRO_STAY_IN_TASKS RETURN "MC_H_AGRSIT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_AGGRO_ACTIVATE_SIREN RETURN "MC_H_AGRSIR" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_AGGRO_IGNORE_SILENCED_CLONED_BULLETS RETURN "MC_H_AGG_ISCB" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_AGGRO_STOP_DROPPED_WEAPON_FIRING RETURN "MC_H_AGG_SDWF" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_AGGRO_AGGRO_OTHER_AGGRO_PEDS RETURN "MC_H_AGG_AOAP" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_AGGRO_BLOCK_AGGRO_DUE_TO_OTHER_PED RETURN "MC_H_AGG_BAOP" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_AGGRO_BLOCK_AGGRO_DUE_TO_OTHER_AMBIENT_PEDS RETURN "MC_H_AGG_BAAP" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_AGGRO_AGGRO_ON_HURT_KILLED RETURN "MC_H_AGG_AHK" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_AGGRO_AGGRO_ON_WANTED RETURN "MC_H_AGG_WNT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_AGGRO_CHECK_FOR_AGGRO_SPAWN RETURN "MC_H_AGG_CAS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_AGGRO_FAIL_DELAY_FOR_WANTED RETURN "MC_H_AGG_FDW" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_AGGRO_BLOCK_THREATEN_EVENTS RETURN "MC_H_AGG_BAFTE" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_AGGRO_ONLY_WITH_SPECIAL_SCRIPT_EVENTS RETURN "MC_H_AGG_AOWSE" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_AGGRO_IF_AGGRO_EVENT_WAS_EVER_SENT_OUT RETURN "MC_H_AGG_AOSIAE" ENDIF		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_AGGRO_ALWAYS_SET_SPOOK_REASON RETURN "MC_H_AGG_ASSR" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_AGGRO_AGGRO_PED_ON_CCTV_CAMERAS_DESTROYED RETURN "MC_H_AGG_CCTV" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_AGGRO_EXPLOSION_DETECTION_RANGE RETURN "MC_H_AGG_EXPR" ENDIF	
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_PED_TEAM_AGGRO_SETTINGS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_TEAM_AGGRO_SETTING_TEAM RETURN "FMMC_TEAGS_09" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_TEAM_AGGRO_SETTING_RULE RETURN "MC_H_AGROTEAM0" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_TEAM_AGGRO_SWITCH_ON_RULE RETURN "MC_H_TEAGS_SWI" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_TEAM_AGGRO_SETTING_DURATION RETURN "MC_H_AGROTEAM1" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_TEAM_AGGRO_SETTING_IGNORE_CONTACT RETURN "MC_H_AGRO_CONT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_TEAM_AGGRO_SETTING_IGNORE_POTENTIAL_RUNOVER RETURN "MC_H_AGRO_RUN" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_TEAM_AGGRO_SETTING_AGGRO_IF_OUT_OF_ANIM RETURN "MC_H_AGRANM" ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu =eFmmc_ASS_OBJECTIVE_MENU
	
		INT iPedToChange = sFMMCMenu.iSelectedEntity
		IF iPedToChange = -1
			iPedToChange = g_FMMC_STRUCT_ENTITIES.iNumberOfPeds
		ENDIF
		
		IF sFMMCmenu.iEntityCreation = CREATION_TYPE_PEDS
			
			iPedToChange = CLAMP_INT(iPedToChange, 0, FMMC_MAX_PEDS-1)
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_MENU_PED_CLEANUP_TEAM RETURN "MC_H_ENT000" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_MENU_PED_CLEANUP_RULE RETURN "MC_H_ENT001" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_MENU_PED_CLEANUP_RANGE RETURN "MC_H_CLN_RNG" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_MENU_PED_CREATE_ALL RETURN "FMMC_SS_RL50H" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_MENU_PED_CLEANUP_AT_MIDPOINT RETURN "MC_H_ASS_CMP" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_MENU_PED_CLEANUP_AT_MISSION_END RETURN "MC_H_ASS_CME" ENDIF
			
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_MENU_PED_ACTION
				IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedToChange].iAssociatedAction = ciACTION_NOTHING RETURN "MC_H_PED1040" ENDIF
				IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedToChange].iAssociatedAction = ciACTION_GOTO RETURN "MC_H_PED1041" ENDIF
				IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedToChange].iAssociatedAction = ciACTION_DEFEND RETURN "MC_H_PED1042" ENDIF
				IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedToChange].iAssociatedAction = ciACTION_ATTACK RETURN "MC_H_PED1043" ENDIF
				IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedToChange].iAssociatedAction = ciACTION_COMBAT_MODE RETURN "MC_H_PED1044" ENDIF
			ENDIF
			
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_MENU_PED_TASK_CUSTOM_TARGET RETURN "MC_H_PED_TTO" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_MENU_PED_WALK RETURN "FMMC_ASSRL12H" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_MENU_PED_ESCORT_RANGE RETURN "MC_H_PED111" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_MENU_PED_ACTION_ACTIVATION RETURN "FMMC_ACA_H" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_MENU_PED_GOTO_TYPE RETURN "FMMC_ASSRL13H" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_MENU_PED_GOTO_POS RETURN "MC_H_PED108" ENDIF
			
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_MENU_PED_RAPPEL RETURN "MC_H_RAP" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_MENU_PED_ALLOW_PASSENGER_RAPPEL RETURN "MC_H_ASS_APR" ENDIF
			
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_MENU_PED_STOP_TASKS RETURN "FMMC_STT_H" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_MENU_PED_SPEED RETURN "MC_H_PED110" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_MENU_PED_SECONDARY_GOTO RETURN "MC_H_PED_GTI" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_MENU_PED_CLEANUP_DELIVERY RETURN "MC_H_ASS_CUD" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_MENU_PED_SHOOT_ON_GOTO RETURN "MC_H_GOTO_ATK" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_MENU_PED_STOP_FOLLOWING_LAST_DEL RETURN "MC_H_ASS_STF" ENDIF
			
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_MENU_PED_CANCEL_TASK_TO_COMBAT_PLAYER
				IF IS_BIT_SET(sFMMCmenu.iActorMenuBitsetThree, ciPED_BSThree_CancelTasksWSpookedChecks) RETURN "MC_H_ASS_CTC2" ENDIF
				IF IS_BIT_SET(sFMMCmenu.iActorMenuBitsetThree, ciPED_BSThree_CancelTasksToCombatPlayers) RETURN "MC_H_ASS_CTC1" ENDIF
				RETURN "MC_H_ASS_CTC0"
			ENDIF
			
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_MENU_PED_DONT_EXIT_GOTO RETURN "MC_H_ASS_EGT" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_MENU_PED_CLEANUP_ON_DEATH RETURN "MC_H_ASS_CLD" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_MENU_PED_BECOME_AGGRESSIVE_ON_PATH_FAIL RETURN "MC_H_ASS_APF" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_MENU_PED_STAY_IN_VEH_AFTER_GOTO_ACTION RETURN "MC_H_ASS_SVA" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_MENU_PED_DONT_ABANDON_MOVING_VEHICLE_DURING_GOTO RETURN "MC_H_ASS_DAV" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_MENU_PED_NEVER_SHOOT_ON_GOTO RETURN "MC_H_ASS_DSG" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_MENU_PED_SPAWN_GROUP RETURN "MC_H_PED_SPG" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_MENU_PED_SPAWN_SUB_GROUP RETURN "MC_H_PED_SSG" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_MENU_PED_COMBAT_GO_STRAIGHT_TO_TARGET_BOAT RETURN "MC_H_ASS_NNB" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_MENU_PED_FORCE_VEHICLE_HEIGHT_OFFSET_GOTO RETURN "MC_H_ASS_HOAH" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_MENU_PED_FORCE_VEHICLE_HEIGHT_OFFSET_AT_HEALTH RETURN "MC_H_ASS_AVBH" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_MENU_PED_SHUFFLE_TO_TURRET_SEAT_ON_ALERT RETURN "MC_H_ASS_SITOA" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_MENU_PED_WAIT_FOR_TASK_TARGET_TO_SPAWN RETURN "MC_H_ASS_WTTS" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_MENU_PED_DISABLE_DRIVING_INTO_ONCOMING_TRAFFIC RETURN "FMMC_H_ASS_DDIO" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_MENU_PED_STAY_IN_VEHICLE_AFTER_DELIVERY RETURN "FMMC_H_ASS_SVAD" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_MENU_PED_SMALL_SPOOK_RANGE_IN_AIR_VEHICLE RETURN "MC_H_AS_SPOP" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_MENU_PED_KILL_PED_IF_IN_UNDRIVEABLE_VEHICLE RETURN "MC_H_AS_KUVV" ENDIF			
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_MENU_PED_KILL_PED_IF_IN_UNDRIVEABLE_VEHICLE RETURN "MC_H_AS_DREFO" ENDIF		
		ELSE
		
			IF sFMMCMenu.iSelectedEntity = -1
				iPedToChange = g_FMMC_STRUCT_ENTITIES.iNumberOfPeds
			ELSE
				iPedToChange = sFMMCMenu.iSelectedEntity
			ENDIF
			iPedToChange = CLAMP_INT(iPedToChange, 0, FMMC_MAX_PEDS-1)
			
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_MENU_IGNORE_VIS_CHECK RETURN "MC_H_PED1401" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_MENU_CLEANUP_TEAM RETURN "MC_H_ENT000" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_MENU_CLEANUP_RULE RETURN "MC_H_ENT001" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_MENU_CLEANUP_RANGE RETURN "MC_H_CLN_RAN" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_MENU_CREATE_ALL RETURN "FMMC_SS_RL50H" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_MENU_CLEANUP_AT_MIDPOINT RETURN "MC_H_ASS_CMP" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_MENU_CLEANUP_AT_MISSION_END RETURN "MC_H_ASS_CME" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_MENU_CLEANUP_ON_DEATH_IMMEDIATELY RETURN "MC_H_ASS_CIOD" ENDIF
			
			IF sFMMCmenu.iEntityCreation = CREATION_TYPE_VEHICLES
				IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_MENU_VEH_CLEANUP_DELIVERY RETURN "MC_H_VEH_CUD" ENDIF
				IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_MENU_VEH_SPAWN_GROUP RETURN "MC_H_VEH_SPG" ENDIF
				IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_MENU_VEH_SPAWN_SUB_GROUP RETURN "MC_H_VEH_SSG" ENDIF
				IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_MENU_VEH_RADIO_SEQUENCE_TYPE RETURN "MC_H_ASS_PRS" ENDIF
				IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_MENU_VEH_RADIO_SEQUENCE_DELAY RETURN "MC_H_ASS_PRSD" ENDIF
				IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_MENU_VEH_RADIO_SEQUENCE_DELAY RETURN "MC_H_AS_DREFO" ENDIF
			ENDIF
			
			IF sFMMCmenu.iEntityCreation = CREATION_TYPE_OBJECTS
				IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_MENU_OBJ_SPAWN_GROUP RETURN "MC_H_OBJ_SPG" ENDIF
				IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_MENU_OBJ_SPAWN_SUB_GROUP RETURN "MC_H_OBJ_SSG" ENDIF
				IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_MENU_OBJ_DONT_REVALIDATE_ENTITY_OBJECTIVE RETURN "MC_H_AS_DREFO" ENDIF
				IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_MENU_OBJ_CLEANUP_DELAY RETURN "MC_H_AS_OBJCD" ENDIF
			ENDIF
			
			IF sFMMCmenu.iEntityCreation = CREATION_TYPE_PROPS
				IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_MENU_PROP_UPDATE_ROTATION_Z RETURN "MC_H_PRPAR_UZR" ENDIF
				IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_MENU_PROP_UPDATE_ROTATION_TIME RETURN "MC_H_PRPAR_URT" ENDIF
				IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_MENU_PROP_UPDATE_ROTATION_INVERT RETURN "MC_H_PRPAR_IR" ENDIF
				IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_MENU_PROP_UPDATE_ROTATION_CLEAR RETURN "MC_H_PRPAR_CLR" ENDIF				
			ENDIF
			
		ENDIF
		
	ENDIF
		
	IF sFMMCMenu.sActiveMenu = eFMMC_ASS_OBJECTIVE_TARGET_OVERRIDE
		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_TARGET_OVERRIDE_MENU_OVERRIDE_TYPE RETURN "MC_H_GTI_TOT" ENDIF
		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_TARGET_OVERRIDE_MENU_OVERRIDE_ID
			STRING ret = GET_DESCRIPTION_OF_ENTITY(sFMMCmenu.iPedPrimaryTargetOverrideType, sFMMCMenu.iPedPrimaryTargetOverrideID)
			IF IS_STRING_NULL_OR_EMPTY(ret)
				RETURN "MC_H_GTI_TOID"
			ELSE
				RETURN ret
			ENDIF
		ENDIF
		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_TARGET_OVERRIDE_MENU_TRIGGER_DISTANCE_FROM_TARGET RETURN "MC_H_GTI_TDFT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_TARGET_OVERRIDE_MENU_DEACTIVATE_TASK_OUT_OF_TRIGGER_DISTANCE RETURN "MC_H_GTI_DTTD" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_TARGET_OVERRIDE_MENU_DEACTIVATE_TASK_ON_PLAYER_CONTROL_OF_TARGET RETURN "MC_H_GTI_STPC" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_TARGET_OVERRIDE_MENU_ACTIVATE_TASK_ON_TARGET_DEATH RETURN "MC_H_GTI_AOD" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_TARGET_OVERRIDE_MENU_DEACTIVATE_TASK_ON_TARGET_DEATH RETURN "MC_H_GTI_DOD" ENDIF		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_TARGET_OVERRIDE_MENU_DO_NOT_ENTER_TARGET_VEHICLE RETURN "MC_H_GTI_IGNV" ENDIF		
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_ASS_OBJECTIVE_SECONDARY_GOTO_MENU
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_SECONDARY_GOTO_MENU_TRIGGER_TEAM RETURN "MC_H_GTI_TRT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_SECONDARY_GOTO_MENU_TRIGGER_RULE RETURN "MC_H_GTI_TRR" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_SECONDARY_GOTO_MENU_ACTION_START_ON RETURN "MC_H_GTI_ASO" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_SECONDARY_GOTO_MENU_ACTION RETURN "MC_H_GTI_ACT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_SECONDARY_GOTO_MENU_TARGET_OVERRIDE_TYPE RETURN "MC_H_GTI_TOT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_SECONDARY_GOTO_MENU_TARGET_OVERRIDE_ID
			STRING ret = GET_DESCRIPTION_OF_ENTITY(sFMMCmenu.iSecondaryGotoTargetOverrideType, sFMMCMenu.iSecondaryGotoTargetOverrideID)
			IF IS_STRING_NULL_OR_EMPTY(ret)
				RETURN "MC_H_GTI_TOID"
			ELSE
				RETURN ret
			ENDIF
		ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_SECONDARY_GOTO_MENU_TRIGGER_DISTANCE_FROM_TARGET RETURN "MC_H_GTI_TDFT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_SECONDARY_GOTO_MENU_DEACTIVATE_TASK_OUT_OF_TRIGGER_DISTANCE RETURN "MC_H_GTI_DTTD" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_SECONDARY_GOTO_MENU_DEACTIVATE_TASK_ON_PLAYER_CONTROL_OF_TARGET RETURN "MC_H_GTI_STPC" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_SECONDARY_GOTO_MENU_DEACTIVATE_TASK_ON_COMPLETION RETURN "FMMC_GTI_DTOCD"  ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_SECONDARY_GOTO_MENU_CLEANUP_ON_TASK_COMPLETION RETURN "FMMC_GTI_COTCD"  ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_SECONDARY_GOTO_MENU_DONT_EXIT_ON_ARRIVAL RETURN "MC_H_GRI_DEV" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_SECONDARY_GOTO_MENU_PASS_RULE_ON_TASK_COMPLETION RETURN "FMMC_GTI_PROTCD" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_SECONDARY_GOTO_MENU_FAIL_ON_TASK_COMPLETION RETURN "FMMC_GTI_FOTCD" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_SECONDARY_GOTO_MENU_SPOOKED_DRIVE_SPEED RETURN "MC_H_GTI_SDS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciASS_SECONDARY_GOTO_MENU_WAIT_FOR_VEHICLE_TO_BE_EMPTY_OF_PLAYERS RETURN "FMMC_GTI_WEMPD" ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_VEHICLE_WEAPON_MODS
		RETURN "MC_H_VEH_VWM"
		
	ELIF sFMMCMenu.sActiveMenu = eFmmc_VEHICLE_RESTRICTION_MENU
		RETURN "MC_H_VEH_RST0"
		
	ELIF sFMMCMenu.sActiveMenu = eFMMC_VEHICLE_MODS_WATER_BOMBS
		RETURN GET_VEHICLE_MODS_WATER_BOMBS_DESCRIPTION(sFMMCmenu)
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_VEHICLES_BASE
		IF IS_BIT_SET(iLocalBitSet, biDLCLocked) RETURN "FMMC_ER_031" ENDIF
		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_MENU_LIBRARY RETURN "MC_H_VEH01" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_MENU_TYPE RETURN "MC_H_VEH0" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_MENU_COLOR RETURN "MC_H_VEH1" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_MENU_LIVERY RETURN "MC_H_VEH1A" ENDIF
		
		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_MENU_RULE
			IF sVehStruct.iSelectedVehicleRule[g_CreatorsSelDetails.iSelectedTeam]  =  FMMC_OBJECTIVE_LOGIC_NONE RETURN "MC_H_VEH20" ENDIF
			IF sVehStruct.iSelectedVehicleRule[g_CreatorsSelDetails.iSelectedTeam]  =  FMMC_OBJECTIVE_LOGIC_KILL RETURN "MC_H_VEH21" ENDIF
			IF sVehStruct.iSelectedVehicleRule[g_CreatorsSelDetails.iSelectedTeam]  =  FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER RETURN "MC_H_VEH22" ENDIF
			IF sVehStruct.iSelectedVehicleRule[g_CreatorsSelDetails.iSelectedTeam]  =  FMMC_OBJECTIVE_LOGIC_CAPTURE RETURN "MC_H_VEH23" ENDIF
			IF sVehStruct.iSelectedVehicleRule[g_CreatorsSelDetails.iSelectedTeam]  =  FMMC_OBJECTIVE_LOGIC_PROTECT RETURN "MC_H_VEH24" ENDIF
			IF sVehStruct.iSelectedVehicleRule[g_CreatorsSelDetails.iSelectedTeam]  =  FMMC_OBJECTIVE_LOGIC_GO_TO RETURN "MC_H_VEH25" ENDIF
			IF sVehStruct.iSelectedVehicleRule[g_CreatorsSelDetails.iSelectedTeam]  =  FMMC_OBJECTIVE_LOGIC_LEAVE_ENTITY RETURN "MC_H_VEH26" ENDIF		
			IF sVehStruct.iSelectedVehicleRule[g_CreatorsSelDetails.iSelectedTeam]  =  FMMC_OBJECTIVE_LOGIC_DAMAGE 	RETURN "MC_H_DAMV" ENDIF
		ENDIF
		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_MENU_ALL_TEAM RETURN "MC_H_VEHAT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_MENU_NEW_RULE RETURN "MC_H_VEH4G" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_MENU_CRITICAL RETURN "MC_H_VEH4I" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_MENU_PLACEMENT_OVERRIDE RETURN "MC_H_VEH_PO" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_MENU_VEHICLE_BLIP_OPTIONS RETURN "MC_H_VEH_BLPO" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_MENU_VEHICLE_SPAWN_OPTIONS RETURN "MC_H_VEH_VSR" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_MENU_ALLOW_MOC_ENTRY RETURN "MC_H_VEH_ATE" ENDIF
		
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_VEH_BURN_SETTINGS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_BURN_SETTINGS_BURNING_LIFETIME RETURN "MC_H_VEH_BRNLF"	 ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_BURN_SETTINGS_SPAWN_BURNING RETURN "MC_H_VEH_SBURN" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_BURN_SETTINGS_PROGRESS_RULE_IF_FIRE_PUT_OUT RETURN "MC_H_VEH_BRNRUL" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_BURN_SETTINGS_BURN_TEAM RETURN "MC_H_VEH_RL_T" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_BURN_SETTINGS_BURN_RULE RETURN "MC_H_VEH_RL_R" ENDIF
	ENDIF

	IF sFMMCMenu.sActiveMenu = eFmmc_VEH_WARP_LOCATIONS
				
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciVEH_MENU_WARP_LOCATIONS_USE_SUB_SPAWN_GROUP_AS_INDEX RETURN "MC_H_OBVHL_c" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciVEH_MENU_WARP_LOCATIONS_USE_RANDOM_POOL_AS_INITIAL_SPAWN RETURN "MC_H_WP_INISP" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciVEH_MENU_WARP_LOCATIONS_USE_RANDOM_POOL_AS_RESPAWN_SPAWN RETURN "MC_H_WP_RESPN" ENDIF
		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) % 2 = 0
			RETURN "MC_H_WPVHL_a"
		ELSE
			RETURN "MC_H_WPVHL_b"
		ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_END_CUTSCENE_WARP_LOCATIONS
				
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciENDCUTSCENE_MENU_WARP_LOCATIONS_USE_SUB_SPAWN_GROUP_AS_INDEX RETURN "MC_H_OBVHL_c" ENDIF
		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) % 2 = 0
			RETURN "MC_H_ECUT_a"
		ELSE
			RETURN "MC_H_ECUT_b"
		ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_PED_WARP_LOCATIONS
		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciPED_MENU_WARP_LOCATIONS_USE_SUB_SPAWN_GROUP_AS_INDEX RETURN "MC_H_OBVHL_c" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciPED_MENU_WARP_LOCATIONS_USE_RANDOM_POOL_AS_INITIAL_SPAWN RETURN "MC_H_WP_INISP" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciPED_MENU_WARP_LOCATIONS_USE_RANDOM_POOL_AS_RESPAWN_SPAWN RETURN "MC_H_WP_RESPN" ENDIF
	
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) % 2 = 0
			RETURN "MC_H_PPVHL_a"
		ELSE
			RETURN "MC_H_PPVHL_b"
		ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_OBJ_WARP_LOCATIONS
		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciOBJ_MENU_WARP_LOCATIONS_USE_SUB_SPAWN_GROUP_AS_INDEX RETURN "MC_H_OBVHL_c" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciOBJ_MENU_WARP_LOCATIONS_USE_RANDOM_POOL_AS_INITIAL_SPAWN RETURN "MC_H_WP_INISP" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciOBJ_MENU_WARP_LOCATIONS_USE_RANDOM_POOL_AS_RESPAWN_SPAWN RETURN "MC_H_WP_RESPN" ENDIF
		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) < ciOBJ_MENU_WARP_LOCATIONS_USE_SUB_SPAWN_GROUP_AS_INDEX
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) % 2 = 0
				RETURN "MC_H_OBVHL_a"
			ELSE
				RETURN "MC_H_OBVHL_b"
			ENDIF
		ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_INTERACTABLE_WARP_LOCATIONS
	
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciINTERACTABLE_MENU_WARP_LOCATIONS_USE_SUB_SPAWN_GROUP_AS_INDEX RETURN "MC_H_OBVHL_c" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciINTERACTABLE_MENU_WARP_LOCATIONS_USE_RANDOM_POOL_AS_INITIAL_SPAWN RETURN "MC_H_WP_INISP" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciINTERACTABLE_MENU_WARP_LOCATIONS_USE_RANDOM_POOL_AS_RESPAWN_SPAWN RETURN "MC_H_WP_RESPN" ENDIF
		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) < ciINTERACTABLE_MENU_WARP_LOCATIONS_USE_SUB_SPAWN_GROUP_AS_INDEX
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) % 2 = 0
				RETURN "MC_H_INVHL_a"
			ELSE
				RETURN "MC_H_INVHL_b"
			ENDIF
		ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_LOC_WARP_LOCATIONS
		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciLOC_MENU_WARP_LOCATIONS_USE_SUB_SPAWN_GROUP_AS_INDEX RETURN "MC_H_OBVHL_c" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciLOC_MENU_WARP_LOCATIONS_USE_RANDOM_POOL_AS_INITIAL_SPAWN RETURN "MC_H_WP_INISP" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciLOC_MENU_WARP_LOCATIONS_USE_RANDOM_POOL_AS_RESPAWN_SPAWN RETURN "MC_H_WP_RESPN" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciLOC_MENU_WARP_LOCATIONS_USE_ADD_SPWN_POS_ONE_AS_END_CUTSCENE_POS	RETURN "MC_H_LOC_ENCS"	 ENDIF
		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) % 2 = 0
			RETURN "MC_H_LWVHL_a"
		ELSE
			RETURN "MC_H_LWVHL_b"
		ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_PED_COP_DECOY_OPTIONS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciCOP_DECOY_MENU_SET_DECOY			RETURN "MC_H_SADC"	 	 ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciCOP_DECOY_MENU_DECOY_LIFETIME		RETURN "MC_H_DCLT"	 	 ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciCOP_DECOY_MENU_2_STAR_REDUCTION	RETURN "MC_H_2SRC"	 	 ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_PED_StealthSystem
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_SAS_ENABLE_SYSTEM							RETURN "MCH_STLTH_ENA"	 	 ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_SAS_ENABLE_FROM_RULE							RETURN "MCH_STLTH_RLS"	 	 ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_SAS_ENABLE_END_ON_RULE						RETURN "MCH_STLTH_RLE"	 	 ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_SAS_DETECTION_RANGE							RETURN "MCH_STLTH_DER"	 	 ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_SAS_DETECTION_ANGLE							RETURN "MCH_STLTH_DEA"	 	 ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_SAS_DETECTION_HEIGHT_MAX						RETURN "MCH_STLTH_DEHEA" 	 ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_SAS_DETECTION_HEIGHT_MIN						RETURN "MCH_STLTH_DEHEI" 	 ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_SAS_DETECTION_DELAY							RETURN "MCH_STLTH_DED"	 	 ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_SAS_INSTANT_DETECTION_RANGE					RETURN "MCH_STLTH_IDER"	 	 ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_SAS_INSTANT_DETECTION_ANGLE					RETURN "MCH_STLTH_IDEA"	 	 ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_SAS_INSTANT_DETECTION_HEIGHT_MAX				RETURN "MCH_STLTH_IDHEA" 	 ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_SAS_INSTANT_DETECTION_HEIGHT_MIN				RETURN "MCH_STLTH_IDHEI" 	 ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_SAS_WEAPON_USAGE_RESPONDING_DISTANCE			RETURN "MCH_STLTH_GSRD"	 	 ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_SAS_USE_RADAR_CONE_BLIPS						RETURN "MCH_STLTH_CON"	 	 ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_SAS_RADAR_CONE_BLIPS_USE_INSTANT_VALUES		RETURN "MCH_STLTH_CONI"	 	 ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_SAS_USE_STEALTH_EYE_MARKERS					RETURN "MCH_STLTH_EYE"	 	 ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_SAS_USE_STEALTH_DEBUG_LINES					RETURN "MCH_STLTH_LINES"	 ENDIF		
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_PED_StealthSystem_Additional_Options		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_SAS_ADD_OPTIONS_INSTANT_LOS_SUSPICION_RESET					RETURN "MCH_STLTH_ILOS"	 	 ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_SAS_ADD_OPTIONS_RESPOND_TO_BULLET_IMPACT_EVENTS				RETURN "MCH_STLTH_REBI"	 	 ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_SAS_ADD_OPTIONS_RESPOND_TO_BULLET_WHIZZED_BY_EVENTS			RETURN "MCH_STLTH_REWB"	 	 ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_SAS_ADD_OPTIONS_RESPOND_TO_DEAD_BODY_EVENTS					RETURN "MCH_STLTH_REDB"	 	 ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_SAS_ADD_OPTIONS_RESPOND_TO_SEEN_PED_KILLED_EVENTS			RETURN "MCH_STLTH_PEDK"	 	 ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_SAS_ADD_OPTIONS_RESPOND_TO_SEEN_HACKING_EVENTS				RETURN "MCH_STLTH_HAQQ"	 	 ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_SAS_ADD_OPTIONS_RESPOND_EXPLOSION_EVENTS						RETURN "MCH_STLTH_EXPLO"	 ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_SAS_ADD_OPTIONS_RESPOND_FULL_TEAM_AGGROD						RETURN "MCH_STLTH_FTAA"	 	 ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_SAS_ADD_OPTIONS_USE_DETECTION_DELAY_FOR_OTHER_EVENTS			RETURN "MCH_STLTH_DDOE"	 	 ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_SAS_ADD_OPTIONS_BLOCK_TASK_ACTIVATION_WHILE_ACTIVE			RETURN "MCH_STLTH_BTAUA"	 ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_SAS_ADD_OPTIONS_USE_RED_BLIP_CONES							RETURN "MCH_STLTH_URBC"	 	 ENDIF		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_SAS_ADD_OPTIONS_USE_VERY_WIDE_ELEVATION_ANGLES				RETURN "MCH_STLTH_THIK"	 	 ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PED_SAS_ADD_OPTIONS_PREVENT_IMMEDIATE_SAS_SYSTEM_AGGRO			RETURN "MCH_STLTH_PSAS"	 	 ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_VEH_DROPOFF_DELIVERY_OPTIONS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_DROPOFF_DELIVERY_OVERRIDE_POS		 RETURN "H_ENT_DRPOF_0"	 ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_DROPOFF_DELIVERY_OVERRIDE_RADIUS		 RETURN "H_ENT_DRPOF_1"	 ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_DROPOFF_DELIVERY_OVERRIDE_VISUAL_SIZE RETURN "H_ENT_DRPOF_2"	 ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_DROPOFF_UNSTUCK_UNLOCK_VEHICLE		 RETURN "H_ENT_DRPOF_3"	 ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_DROPOFF_LOCK_VEHICLE					 RETURN "H_ENT_DRPOF_4"	 ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_DROPOFF_VEHICLE_ALLOWED_IN_MODSHOP	 RETURN "H_ENT_DRPOF_5"	 ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_DROPOFF_SPECIAL_BUYER_VEHICLE		 RETURN "H_ENT_DRPOF_6"	 ENDIF		
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_VEHICLE_HEALTH_MENU
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_HEALTH_EASY_EXPLODE RETURN "MC_H_VEHEX" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_HEALTH_ENGINE RETURN "MC_H_VEHEH" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_HEALTH_PETROL_TANK RETURN "MC_H_VEHTH" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_HEALTH_BODY RETURN "MC_H_VEHBH" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_HEALTH_CAN_PETROL_BE_DAMAGED RETURN "MC_H_PTRLDMG" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_HEALTH_OVERALL RETURN "MC_H_VEH4A" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_HEALTH_OCCUPANTS_EXPLOSION_PROOF RETURN "MC_H_VEHOEP" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_HEALTH_COMPONENTS_SCALE_WITH_PLAYERS RETURN "MC_H_VEH4J" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_HEALTH_CANT_BE_DAMAGED_BY_PLAYERS RETURN "FMMC_VEH_CBDBP" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_TRACK_DAMAGE RETURN "MC_H_VEH_TRD" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_HEALTH_DISABLE_BREAKING RETURN "FMMC_VEH_DBD" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_LINKED_DESTROY RETURN "MC_H_VEH_EWVD" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_LINKED_SPAWN RETURN "MC_H_VEH_SWLV" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_HEALTH_DAMAGE_MAP_SCALARS RETURN "MC_H_SCL_VE" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_EXPLODE_IN_WATER RETURN "MC_H_VEH_EVIW" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_ENGINE_SMOKE_ON_LOW_BODY_HEALTH RETURN "MC_H_VEH_ESLBH" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_HEALTH_FOR_RULE_OBJECTIVE RETURN "MC_H_HLT_DRFR" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_HEALTH_LINKED_ENTITY_SUBMENU RETURN "MC_H_HLT_DRFRM" ENDIF
	ENDIF
		
	IF sFMMCMenu.sActiveMenu = eFmmc_VEHICLE_CARGOBOB_ATTACHMENT
		IF (GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_CARGOBOB_ATTACHMENT_ENTITY_ID
		OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_CARGOBOB_ATTACHMENT_ENTITY_CAMERA_SWITCH)
		AND sFMMCMenu.iCargobobEntitySelectID > -1
			RETURN GET_DESCRIPTION_OF_ENTITY(sFMMCmenu.iCargobobEntitySelectType, sFMMCMenu.iCargobobEntitySelectID)
		ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_VEHICLE_DOOR_MENU
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_DOORS_MENU_LOCKED RETURN "MC_H_VEH4F" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_DOORS_MENU_BREAKABLE RETURN "MC_H_VEHUBD" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_DOORS_MENU_OPEN_CLOSED RETURN "MC_H_VEHOPN" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_OPEN_PERSONAL_VEHICLE_DOORS RETURN "MC_H_OPVDDESC" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_LOCK_ON_RULE RETURN "MC_H_VEH_LVR" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_LOCK_ON_DELIVERY_TEAM RETURN "MC_H_VEH_LVDT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_LOCK_ON_DELIVERY_RULE RETURN "MC_H_VEH_LVDR" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_MENU_SHOCK_LOCK RETURN "MC_H_VEH_SHL" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_DOORS_DISABLE_FLEEING_PEDS RETURN "MC_H_VEH_DFP" ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_VEHICLE_CUSTOM_MOD_OPTIONS
		//IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_DOORS_MENU_LOCKED RETURN "MC_H_VEH4F" ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_VEHICLE_BLIP_OPTIONS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_MENU_BLIP RETURN "FMMC_V_BLIP" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_MENU_DONT_BLIP_WITH_TRAILER RETURN "MC_H_VEH_DBT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_AUTO_GPS_VEHICLE RETURN "MC_H_VEH_AGV" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_IGNORE_NO_GPS_FLAG RETURN "MC_H_VEH_ING" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_TEAM_COLOURED_BLIP RETURN "MC_H_VEH_TCB" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_IGNORE_RANGE_IF_OCCUPIED RETURN "MC_H_VEH_IRO" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_BLIP_ON_MAP RETURN "MC_H_VEH_BMR" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_MENU_HIDE_MARKER_IN_ORBITAL_CANNON RETURN "MC_H_VEH_OBTH" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_FORCE_BLIP_HEIGHT_INDICATOR RETURN "MC_H_VEH_FBH" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_MENU_BLIP_RUN_BLIP_UPDATES RETURN "MC_H_VEH_RBU" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_MENU_BLIP_USE_CUSTOM_FAIL_NAME RETURN "MC_H_VEH_FLNMM" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_USE_SKULL_BLIP RETURN "MC_H_VUSB" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_INHERIT_OCCUPIERS_BLIP_COLOUR RETURN "MC_H_IO_BC" ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_GenericEntityBlipOptions
		RETURN GET_ENTITY_BLIP_DESCRIPTION(sFMMCmenu)
	ELIF sFMMCMenu.sActiveMenu = eFmmc_DYNOPROP_BLIP_OPTIONS
		RETURN HANDLE_DYNOPROP_BLIP_OPTIONS_MENU_HELPTEXT(sFMMCmenu)
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_VEHICLE_WEAPON_MISC_OPTIONS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_MENU_HOMING_ROCKET_PRIORITY_TARGET RETURN "MC_H_VEH_HRP" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_MENU_DISABLE_VEHICLE_WEAPONS RETURN "MC_H_VEH_DVW" ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_VEHICLE_DELUXO_OPTIONS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_DELUXO_START_HOVERING RETURN "MC_H_VEH_DELO4" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_DELUXO_INDEX RETURN "MC_H_VEH_DLXIND" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_DELUXO_MISSION_SEAT_PREF_OVERRIDE RETURN "MC_H_VEH_DMSPO" ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_VEHICLE_DOOR_OPEN_CLOSED_MENU
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_DOORS_FRONT_LEFT
		OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_DOORS_FRONT_RIGHT
		OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_DOORS_REAR_LEFT
		OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_DOORS_REAR_RIGHT
		OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_DOORS_HOOD
		OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_DOORS_TRUNC
			RETURN "MC_H_VED_CLD"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_DOORS_LOCKED
			RETURN "MC_H_VED_LOC"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_DOORS_KEEP_OPEN
			RETURN "MC_H_VED_KO"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_DOORS_SET_ON_RULE
			RETURN "MC_H_VEHD_R"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_DOORS_SET_ON_RULE_MIDPOINT
			RETURN "MC_H_VEHD_RM"
		ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_VEHICLE_DOOR_BREAKABLE_MENU
		RETURN "MC_H_VEHUBD"
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_VEHICLE_DOOR_DELAYED_OPEN_MENU
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_DOORS_DELAYED_ENABLE RETURN "FMMC_VDDO_ENA_D" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_DOORS_DELAYED_RANGE RETURN "FMMC_VDDO_R_D" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_DOORS_DELAYED_TIME RETURN "FMMC_VDDO_T_D" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_DOORS_DELAYED_RULE RETURN "FMMC_VDDO_RL_D" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_DOORS_DELAYED_SECOND_RANGE_CHECK RETURN "FMMC_VDDO_SRC_D" ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_VEHICLE_ALL_TEAM_MENU
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_ALL_TEAM_MENU_ALL_TEAM RETURN "MC_H_VEH_ALL0" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_ALL_TEAM_MENU_DISABLE_FULL_CHECK RETURN "MC_H_VEH_ALL_FV" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = VEHICLE_ALL_TEAM_MENU_WAIT_FOR_VEHICLE_RULE RETURN "MC_H_VEH_WVR" ENDIF
		RETURN "MC_H_VEH_ALL_CT"
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_VEHICLE_EXTRAS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 0 RETURN "MC_H_VEH_EXTA" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) < 14 RETURN "MC_H_VEH_EXTO" ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_COVER_POINT_BASE
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciCOVER_COVER_TYPE RETURN "FMMC_CP_TYPED" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciCOVER_COVER_HEIGHT RETURN "FMMC_CP_HIGHD" ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu =eFmmc_OBJECT_BASE
	
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OBJECT_TYPE RETURN "MC_H_OBJ0" ENDIF
		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OBJECT_RULE
			IF sObjStruct.iSelectedObjectRule[g_CreatorsSelDetails.iSelectedTeam] =  FMMC_OBJECTIVE_LOGIC_NONE 				RETURN "MC_H_OBJ10" ENDIF
			IF sObjStruct.iSelectedObjectRule[g_CreatorsSelDetails.iSelectedTeam] =  FMMC_OBJECTIVE_LOGIC_KILL 				RETURN "MC_H_OBJ11" ENDIF
			IF sObjStruct.iSelectedObjectRule[g_CreatorsSelDetails.iSelectedTeam] =  FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER 	RETURN "MC_H_OBJ12" ENDIF
			IF sObjStruct.iSelectedObjectRule[g_CreatorsSelDetails.iSelectedTeam] =  FMMC_OBJECTIVE_LOGIC_CAPTURE 			RETURN "MC_H_OBJ13" ENDIF
			IF sObjStruct.iSelectedObjectRule[g_CreatorsSelDetails.iSelectedTeam] =  FMMC_OBJECTIVE_LOGIC_PROTECT			RETURN "MC_H_OBJ14" ENDIF
			IF sObjStruct.iSelectedObjectRule[g_CreatorsSelDetails.iSelectedTeam] =  FMMC_OBJECTIVE_LOGIC_GO_TO 			RETURN "MC_H_OBJ15" ENDIF
			IF sObjStruct.iSelectedObjectRule[g_CreatorsSelDetails.iSelectedTeam] =  FMMC_OBJECTIVE_LOGIC_LEAVE_ENTITY 		RETURN "MC_H_OBJ16" ENDIF
			IF sObjStruct.iSelectedObjectRule[g_CreatorsSelDetails.iSelectedTeam] =  FMMC_OBJECTIVE_LOGIC_DAMAGE 			RETURN "MC_H_DAMO" ENDIF
		ENDIF
		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OBJECT_VEHICLE_PU RETURN "MC_H_OBJ19" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OBJECT_FORCE_FLOOR RETURN "MC_H_OBJ20" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OBJECT_VEHICLE_PU RETURN "MC_H_OBJ17" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OBJECT_ATTACHED_PED RETURN "MC_H_ATT_TPED" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OBJECT_RESPAWN RETURN "MC_H_OBJ2" ENDIF
		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OBJECT_NEW_RULE RETURN "MC_H_OBJ4" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OBJECT_ASS_OBJECTIVE RETURN "MC_H_OBJ5" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OBJECT_JUMP_TO_OBJECTIVE RETURN "FMMC_JTO_DESCR" ENDIF
		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OBJECT_SPAWN_NEAR_ENTITY RETURN "MC_H_SNE_O" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OBJECT_COMPLETION_IGNORES_PRIORITY RETURN "MC_H_OBJ_CIP" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OBJECT_INVISIBLE_ON_DELIVER RETURN "MC_H_OBJ_IOD" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OBJECT_SNAP_TO_GROUND RETURN "MC_H_STG" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OBJECT_USE_PICKUP_PROXIMITY RETURN "MC_H_UPP" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OBJECT_USE_ALTERNATIVE_PROXIMITY RETURN "MC_H_OBJ_UAP" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OBJECT_ENABLE_TRACKIFY RETURN "MC_H_ENT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) =  OBJECT_FREEZE_POSITION RETURN "FMMC_OBJ_FOP" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OBJECT_SPAWN_PLAYER_LIMIT RETURN "MC_H_SPWN_LMT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) =  OBJECT_SET_AS_CONTAINER RETURN "FMMC_OBJ_SAC" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) =  OBJECT_SET_INVISIBLE RETURN "FMMC_OBJ_INV" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) =  OBJECT_DONT_RESET_TIMER_WHEN_DROPPED RETURN "MC_H_OBJ_TPJ" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) =  OBJECT_DONT_TEAM_CONTROL_BAR_OBJECT RETURN "MC_H_OBJ_DRS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) =  OBJECT_TRANSFORM_JUGGERNAUT RETURN "MC_H_OBJ_TPJ" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) =  OBJECT_TRANSFORM_JUGGERNAUT_HEALTH RETURN "MC_H_OBJ_TPJH" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) =  OBJECT_TRANSFORM_BEAST RETURN "MC_H_OBJ_TPB" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) =  OBJECT_UPGRADE_VEHICLE RETURN "MC_H_OBJ_UGV" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OBJECT_EXPLODE_TIME RETURN "FMMC_OBJ_EWOH" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OBJECT_CUSTOM_PICKUP_SHARD RETURN "MC_H_OBJ_SCS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OBJECT_INVENTORY RETURN "MC_H_OBJ_INV" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OBJECT_SUDDEN_DEATH_HIDE RETURN "MC_H_OBJ_HODS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OBJECT_ACTIVATION_RANGE_OVERRIDE RETURN "MC_H_OBJ_ARO" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OBJECT_ALLOW_TAGGING RETURN "MC_H_OBJ_ATT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OBJECT_TAG_COLOUR RETURN "MC_H_TAG_STC" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OBJECT_ALLOW_LOCKING_ON RETURN "MC_H_OBJ_ALO" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OBJECT_ONLY_PICKUP_ONCE RETURN "MC_H_OBJ_OSO" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OBJECT_CLEAR_UP_MISSION_CRITICAL_VEH RETURN "FMMC_O_CUMCV_D" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OBJECT_DESTROY_EXPLOSION_RADIUS RETURN "MC_H_OBJ_DER" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OBJECT_EXPLODE_ONLY_VOLATOL_WIDE RETURN "MC_H_OBJ_EOVW" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OBJECT_OVERRIDE_LOD RETURN "MC_H_OBJ_OLOD" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OBJECT_DONT_SPAWN_IF_DESTROYED_QR RETURN "MC_H_OBJ_DSID" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OBJECT_FIREPROOF RETURN "MC_H_OBJ_FRPF" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OBJECT_DONT_CLEAN_UP_WHEN_DEAD RETURN "MC_H_OBJ_DCUD" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OBJECT_VISIBILITY_BY_RULE RETURN "MC_H_OBJ_VBR" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OBJECT_SPAWN_IN_SUDDEN_DEATH RETURN "MC_H_OBJ_SDND" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OBJECT_VEHICLE_SWAP_MODEL RETURN "MC_H_OBJ_VEHS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OBJECT_TRANSFORM_VEHICLE_INDEX RETURN "MC_H_OBJ_TVI" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OBJECT_MISSION_VARIATION_OVERRIDE_OPTIONS RETURN "MH_OBJVAR_0" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OBJECT_CONTINUITY_ID RETURN "MC_H_MICO_ID" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OBJECT_ZONE_BLOCKING_ENTITY_SPAWN RETURN "MC_H_SPWNZOBLK" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OBJECT_ZONE_BLOCKING_ENTITY_SPAWN_PLAYER_REQ RETURN "MC_H_SPWNZOBLP" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OBJECT_ELECTRONIC_OPTIONS RETURN "MC_ELEC_SM_H" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OBJECT_WARP_LOCATIONS RETURN "MC_H_WRPS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OBJECT_SPOOK_NEARBY_PEDS_WHEN_USED RETURN "MC_H_SNBPU" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OBJECT_RESPAWN_DELAY RETURN "H_RESP_DELY" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OBJECT_SUPPRESS_PICKUP_SOUND RETURN "MC_H_SPPKSND" ENDIF				
	ENDIF
	
	IF sFMMCmenu.sActiveMenu = eFmmc_OBJECT_HEALTH_OPTIONS
		RETURN HANDLE_OBJECT_HEALTH_MENU_HELPTEXT(sFMMCmenu)
	ENDIF
	
	IF sFMMCmenu.sActiveMenu = eFmmc_OBJ_Blip
		RETURN SET_UP_OBJ_BLIP_OPTION_DESCRIPTIONS(sFMMCmenu)
	ENDIF
	
	IF sFMMCmenu.sActiveMenu = eFMMC_MISSION_BRANCHING
		GET_MISSION_BRANCHING_OPTIONS_DESCRIPTION(sFMMCmenu)
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_OBJ_Minigame
		RETURN SET_UP_OBJ_MINIGAME_OPTION_DESCRIPTIONS(sFMMCmenu)
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_DIALOGUE_TRIGGER_BASE
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DIALOGUE_TRIGGER_PLAY_AS_AMBIENT_SPEECH RETURN "MC_H_DLT_AMB_P" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DIALOGUE_TRIGGER_AMBIENT_VOICE_NAME RETURN "MC_H_DLT_AMB_V" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DIALOGUE_TRIGGER_TEAM RETURN "MC_H_DLT_TM" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DIALOGUE_TRIGGER_HEARD_BY_TEAM RETURN "MC_H_DLT_HBT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DIALOGUE_TRIGGER_RULE RETURN "MC_H_DLT_RL" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DIALOGUE_TRIGGER_PRIORITY RETURN "MC_H_DLT_PR" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DIALOGUE_TRIGGER_SKIP_REVALIDATION_AFTER_DELAY RETURN "MC_H_DLT_SKI" ENDIF		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DIALOGUE_TRIGGER_ACTUAL_POINTS RETURN "MC_DT_ACP_H" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DIALOGUE_TRIGGER_TRIGGER_ON_LATER_RULES RETURN "MC_H_DLT_TLR" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DIALOGUE_TRIGGER_TEAM_LIMITS RETURN "MC_H_DLT_LIM" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DIALOGUE_TRIGGER_TRIGGER_NEAR_APARTMENT RETURN "MC_H_DLT_APT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DIALOGUE_TRIGGER_TRIGGER_ON_WANTED RETURN "MC_H_DLT_TOW" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DIALOGUE_TRIGGER_WANTED_TRIGGER_FOR_CARRIER RETURN "MC_H_DLT_WTC" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DIALOGUE_TRIGGER_INTERRUPT_OTHER_DIALOGUE RETURN "MC_H_DLT_INT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DIALOGUE_TRIGGER_INSTANTLY_INTERRUPT RETURN "MC_H_DLT_INTI" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DIALOGUE_TRIGGER_HELP_TEXT RETURN "MC_H_DLT_HLP" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DIALOGUE_TRIGGER_ON_PHONE RETURN "MC_H_DLT_PHN" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DIALOGUE_TRIGGER_AERIAL RETURN "MC_H_DLT_AER" ENDIF
		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DIALOGUE_TRIGGER_CROWD_CONTROL_KILLS RETURN "MC_H_DLT_CROWD" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DIALOGUE_TRIGGER_ENABLE_QUICK_MASK RETURN "MC_H_DLT_EQM" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DIALOGUE_TRIGGER_INTERRUPT_ON_PLAYER_ACTION RETURN "MC_H_DLT_IPA" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DIALOGUE_TRIGGER_LOCAL_PLAYER_IN_RANGE RETURN "MC_H_DLT_LPR" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DIALOGUE_TRIGGER_HELP_TEXT_CLEAR_EXISTING RETURN "MC_H_DLT_HPCL" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DIALOGUE_TRIGGER_HELP_TEXT_NG_ONLY RETURN "MC_H_DLT_HTNG" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DIALOGUE_TRIGGER_AIM_FIRE_DRIVE_BY RETURN "MC_H_DLT_AFDB" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DIALOGUE_TRIGGER_FIRST_PERSON_ONLY RETURN "MC_H_DLT_FPO" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DIALOGUE_TRIGGER_BLOCK_ON_CAMERA_LOCK RETURN "MC_H_DLT_BOCL" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DIALOGUE_TRIGGER_IS_WEARING_MASK RETURN "MC_H_DLT_WIM" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DIALOGUE_TRIGGER_MASK_PROMPT RETURN "MC_H_DLT_RMP" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DIALOGUE_TRIGGER_TRIGGER_WITH_SPAWN_GROUPS RETURN "MC_H_DLT_TSG" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DIALOGUE_TRIGGER_GANG_CHASE_KILLED RETURN "MC_H_DLT_GCK" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DIALOGUE_TRIGGER_IGNORE_RULE RETURN "MC_H_DLT_IGR"	 ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DIALOGUE_TRIGGER_THERMAL_VISION_ACTIVATED RETURN "MC_H_DLT_TVT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DIALOGUE_TRIGGER_ON_MULTIRULE_TIMER_TIME RETURN "MC_H_DLT_WMRTS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DIALOGUE_TRIGGER_NOT_ON_MID_POINT_YET RETURN "MC_H_DLT_TBMPT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DIALOGUE_TRIGGER_EXPEDITE_AND_DONT_REVALIDATE RETURN "MC_H_DLT_EADR" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DIALOGUE_TRIGGER_NOT_FOR_SPECTATORS RETURN "MC_H_DLT_NSPC" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DIALOGUE_TRIGGER_NOT_FOR_ANY_SPECTATORS RETURN "MC_H_DLT_NSPC" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DIALOGUE_TRIGGER_BLOCK_OBJECTIVE_WHILE_PLAYING_DIALOGUE RETURN "MC_H_DLT_BLCK" ENDIF		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DIALOGUE_TRIGGER_IF_PLAYER_USED_ELEVATOR	 RETURN "MC_H_DLT_UAE" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DIALOGUE_TRIGGER_IF_COP_DECOY_NEAR_END	 RETURN "MC_H_DLT_TDNE" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DIALOGUE_TRIGGER_IF_COP_DECOY_ENDED	 RETURN "MC_H_DLT_TDCE" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DIALOGUE_TRIGGER_IF_COP_DECOY_FLED	 RETURN "MC_H_DLT_TDCF" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DIALOGUE_TRIGGER_GANG_LEADER_ONLY	 RETURN "MC_H_DLT_GLON" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DIALOGUE_TRIGGER_DOES_ENTITY_EXIST	 RETURN "MC_H_DLT_ENX" ENDIF  
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DIALOGUE_TRIGGER_BLOCK_TRIGGER_IF_COMPLETELY_SUBMERGED	 RETURN "MC_H_DLT_SUBA" ENDIF  
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DIALOGUE_TRIGGER_BLOCK_TRIGGER_IF_NOT_YET_BEEN_SUBMERGED_ON_RULE	 RETURN "MC_H_DLT_SUBB" ENDIF  
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DIALOGUE_TRIGGER_OBJECT_CONTINUITY	 RETURN "MC_H_DT_OCID" ENDIF  
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DIALOGUE_TRIGGER_LOCATION_CONTINUITY	 RETURN "MC_H_DT_LCID" ENDIF  
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DIALOGUE_TRIGGER_CASH_GRAB_TAKE_LOWER_LIMIT	 RETURN "MC_H_DLT_TTLL" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DIALOGUE_TRIGGER_CASH_GRAB_TAKE_UPPER_LIMIT	 RETURN "MC_H_DLT_TTUL" ENDIF

	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFMMC_ZONE_OVERRIDE_POS
		RETURN HANDLE_ZONE_OVERRIDE_POSITION_HELPTEXT(sFMMCMenu)
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFMMC_ZONE_ZONETIMER
		RETURN HANDLE_ZONE_TIMER_MENU_HELPTEXT(sFMMCMenu)
	ENDIF
		
	IF sFMMCMenu.sActiveMenu = eFmmc_DIALOGUE_TRIGGER_VEHICLE_OPTIONS
		RETURN SHOW_DIALOGUE_TRIGGER_VEHICLE_DESCRIPTION(sFMMCMenu)
	ENDIF

	IF sFMMCMenu.sActiveMenu = eFmmc_DIALOGUE_TRIGGER_VEH_INHABITANT
		RETURN SHOW_DIALOGUE_TRIGGER_IN_VEH_DESCRIPTIONS(sFMMCMenu)
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_DIALOGUE_TRIGGER_OBJECTIVE_VEH
		RETURN SHOW_DIALOGUE_TRIGGER_OBJ_VEH_DESCRIPTIONS(sFMMCMenu)
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_DIALOGUE_TRIGGER_PED_OPTIONS
		RETURN SHOW_DIALOGUE_TRIGGER_PED_DESCRIPTIONS(sFMMCMenu)
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_DIALOGUE_TRIGGER_OBJECT_OPTIONS
		RETURN SHOW_DIALOGUE_TRIGGER_OBJ_DESCRIPTIONS(sFMMCMenu)
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_DIALOGUE_TRIGGER_ZONE_OPTIONS
		RETURN SHOW_DIALOGUE_TRIGGER_ZONE_DESCRIPTIONS(sFMMCMenu)
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_DIALOGUE_TRIGGER_MINIGAME_OPTIONS
		RETURN SHOW_DIALOGUE_TRIGGER_MINIGAME_DESCRIPTIONS(sFMMCMenu)
	ENDIF
	IF sFMMCMenu.sActiveMenu = eFmmc_DIALOGUE_TRIGGER_CALL
		RETURN SHOW_DIALOGUE_TRIGGER_CALL_DESCRIPTION(sFMMCMenu)
	ENDIF
	IF sFMMCMenu.sActiveMenu = eFmmc_DIALOGUE_TRIGGER_Requirements
		RETURN GET_DESCRIPTION_DIALOGUE_TRIGGER_MENU_CASINO_HEIST_REQ(sFMMCMenu)
	ENDIF
	IF sFMMCMenu.sActiveMenu = eFmmc_DIALOGUE_TRIGGER_DISGUISES
		RETURN GET_HELP_TEXT_FOR_DIALOGUE_DISGUISES_MENU(sFMMCMenu)
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_DM_TEAM_SPAWN_POINT
		IF NOT IS_LEGACY_MISSION_CREATOR()
		AND IS_THIS_A_MISSION()
		AND IS_ROCKSTAR_DEV()
		
		ELSE
			RETURN GET_TEAM_SPAWN_POINT_DESCRIPTION(sFMMCMenu)
		ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_DIALOGUE_TRIGGER_VEHILE_SPECIFIC_OPTIONS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DIALOGUE_TRIGGER_VEHICLE_SPECIFIC_CATEGORY	 RETURN "MC_H_SPVH_0" ENDIF	
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DIALOGUE_TRIGGER_VEHICLE_SPECIFIC_TYPE		 RETURN "MC_H_SPVH_1" ENDIF	
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DIALOGUE_TRIGGER_VEHICLE_SPECIFIC_IF_INSIDE	 RETURN "MC_H_SPVH_2" ENDIF	
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DIALOGUE_TRIGGER_VEHICLE_SPECIFIC_IF_OWNED	 RETURN "MC_H_SPVH_3" ENDIF	
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_DIALOGUE_TRIGGER_TEAM_LIMITS
		RETURN "MC_H_DLT_LIM"
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_DIALOGUE_TRIGGER_HEARD_BY_TEAM
		RETURN "MC_H_DLT_HBT_0"
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_DIALOGUE_TRIGGER_TRIGGER_WITH_SPAWN_GROUPS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) < g_FMMC_STRUCT.iRandomEntitySpawn_MaxNumber
			RETURN "MC_H_TSG_TSG"
		ELSE
			RETURN "MC_H_TSG_RASG"
		ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_DIALOGUE_TRIGGER_ENTITY_EXISTS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DIALOGUE_TRIGGER_ENTITY_EXISTS_TYPE	 	RETURN "MC_H_DT_ENX_ET" ENDIF	
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DIALOGUE_TRIGGER_ENTITY_EXISTS_INDEX		RETURN "MC_H_DT_ENX_EI" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DIALOGUE_TRIGGER_ENTITY_DOES_NOT_EXIST  	RETURN "MC_H_DT_ENX_DNE" ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_DIALOGUE_TRIGGER_BLOCK_TRIGGER_CONDITIONS
		RETURN GET_DIALOGUE_TRIGGER_MENU_BLOCK_TRIGGER_CONDITIONS_HELP_TEXT(sFMMCMenu)
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_DIALOGUE_TRIGGER_PLAYER_ABILITY_OPTIONS
		RETURN GET_HELP_TEXT_FOR_DIALOGUE_TRIGGER_MENU_PLAYER_ABILITY_OPTIONS(sFMMCMenu)
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFMMC_DUMMY_BLIPS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DUMMY_BLIP_SIZE RETURN "MC_H_DBL_BLS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DUMMY_BLIP_GPS_ROUTE RETURN "MC_H_DBL_GPS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DUMMY_BLIP_OVERRIDE_QUICK_GPS RETURN "MC_H_DBL_OQG" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DUMMY_BLIP_DISPLAY_WHEN_IN_RANGE RETURN "MC_H_DBL_DIR" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DUMMY_BLIP_DISPLAY_WHEN_IN_HEIGHT_RANGE RETURN "MC_H_DBL_DIHR" ENDIF	
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DUMMY_BLIP_HIDE_BLIP_HEIGHT_INDICATOR RETURN "MC_H_DB_HBHI" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DUMMY_BLIP_MISSION_VARIATION_OPTIONS RETURN "MH_DMBVAR_0" ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu =eFmmc_DELETE_ENTITIES
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 0	RETURN "MC_H_RSTA" ENDIF // All 
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 1	RETURN "MC_H_RSTN" ENDIF // Peds 
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 2	RETURN "MC_H_RSTW" ENDIF // Weapons
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 3	RETURN "MC_H_RSTP" ENDIF // Props 
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 4	RETURN "MC_H_RSTO" ENDIF // Dynamic Props 
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 5	RETURN "MC_H_RSTV" ENDIF // Vehicles 
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFMMC_SPAWN_NEAR_ENTITY
		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = SPAWN_NEAR_ENTITY_TYPE
			IF sFMMCmenu.iEntityCreation = CREATION_TYPE_PEDS RETURN "MC_H_SNE_ETP_P" ENDIF
			IF sFMMCmenu.iEntityCreation = CREATION_TYPE_VEHICLES RETURN "MC_H_SNE_ETP_V" ENDIF
			IF sFMMCmenu.iEntityCreation = CREATION_TYPE_OBJECTS RETURN "MC_H_SNE_ETP_O" ENDIF
			RETURN "MC_H_SNE_ETP"
		ENDIF
		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = SPAWN_NEAR_ENTITY_ID
			IF sFMMCmenu.iEntityCreation = CREATION_TYPE_PEDS RETURN "MC_H_SNE_EID_P" ENDIF
			IF sFMMCmenu.iEntityCreation = CREATION_TYPE_VEHICLES RETURN "MC_H_SNE_EID_V" ENDIF
			IF sFMMCmenu.iEntityCreation = CREATION_TYPE_OBJECTS RETURN "MC_H_SNE_EID_O" ENDIF
			RETURN "MC_H_SNE_EID"
		ENDIF
		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = SPAWN_NEAR_ENTITY_MIN_RANGE
			IF sFMMCmenu.iEntityCreation = CREATION_TYPE_PEDS RETURN "MC_H_SNE_MNR_P" ENDIF
			IF sFMMCmenu.iEntityCreation = CREATION_TYPE_VEHICLES RETURN "MC_H_SNE_MNR_V" ENDIF
			IF sFMMCmenu.iEntityCreation = CREATION_TYPE_OBJECTS RETURN "MC_H_SNE_MNR_O" ENDIF
			RETURN "MC_H_SNE_MNR"
		ENDIF
		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = SPAWN_NEAR_ENTITY_IGNORE_INITIAL
			RETURN "MC_H_SNE_IOI"
		ENDIF
		
	ENDIF
		
	IF sFMMCMenu.sActiveMenu =eFmmc_PROP_BASE
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_STACKING RETURN "MC_H_PRP_STK" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_ROTATION_TYPE RETURN GET_PROP_ROTATION_STRING(sFMMCmenu, GET_CREATOR_PROP_MODEL(sFMMCMenu.iPropLibrary, sFMMCMenu.iPropType)) ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SIGN_TRIGGER_OPTIONS_MS RETURN "FMMC_H_PRP_TSPO" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_COLOUR RETURN "MC_H_PRP_CLR" ENDIF
		IF IS_STUNT_LIBRARY(sFMMCMenu.iPropLibrary)
			
			IF IS_STUNT_TRACK_LIBRARY(sFMMCMenu.iPropLibrary)
				IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_SNAPPING
					RETURN "MC_H_PRP_SNP"
				ENDIF
			ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_ST_ASSOCIATED_SPAWN RETURN "MC_H_PRP_ATS" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_ST_SPAWN_TEAM_NUMBER RETURN "MC_H_PRP_TSN" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_ST_SPAWN_NUMBER RETURN "MC_H_PRP_ASN" ENDIF
		ELSE
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_NS_ASSOCIATED_SPAWN RETURN "MC_H_PRP_ATS" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_NS_SPAWN_TEAM_NUMBER RETURN "MC_H_PRP_TSN" ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_NS_SPAWN_NUMBER RETURN "MC_H_PRP_ASN" ENDIF
		ENDIF
		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = sFMMCMenu.iPropDisplayIndexes_MenuPos RETURN "MC_H_D_DPI" ENDIF
		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_NS_ASSOCIATED_SPAWN RETURN "FMMC_PRP_ATS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_NS_SPAWN_TEAM_NUMBER RETURN "MC_H_PRP_TSN" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_NS_SPAWN_NUMBER RETURN "MC_H_PRP_ASN" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DYNOPROP_MENU_KAMAKAZI_EXPLODE RETURN "FMMC_H_KMKZI" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DYNOPROP_MENU_KAMAKAZI_TEAM_NUMBER RETURN "FMMC_H_KMKTM" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_TAGGING RETURN "MC_H_PRP_ATT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_NS_CCTV_TRIGGER RETURN "MC_H_PRP_TVC" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_NS_ALARM_TEAM RETURN "MC_H_PRP_TAFT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = sFMMCMenu.iPropBlipAsSafeMenuPos RETURN "MC_H_PRP_BLAS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = sFMMCMenu.iPropHidePropInCutsceneMenuPos RETURN "MC_H_PRP_HCUT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = sFMMCmenu.iPropPaintingIndexMenuPos RETURN "MC_H_PRP_PI" ENDIF		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = sFMMCmenu.iDynoSpawnGroupStart RETURN "MC_H_PRP_SPNG" ENDIF	
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = sFMMCmenu.iDynoSpawnSubGroupStart RETURN "MC_H_PRP_SSNG" ENDIF
		RETURN "MC_H_PRP"
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_PROP_SIGN_TRIGGER_OPTIONS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = STM_PROP_OPT_TRIG_SPEED RETURN "MC_H_PRP_TSP" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = STM_PROP_OPT_TRIG_DISTANCE RETURN "MC_H_PRP_TDS" ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_SOUND_TRIGGER_OPTIONS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = SND_PROP_OPT_SND_ID RETURN "FMMC_H_SNDT_ID" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = SND_PROP_OPT_SND_PREVIEW RETURN "FMMC_H_SNDT_PV" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = SND_PROP_OPT_TRIG_RADIUS RETURN "FMMC_H_SNDT_STR" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = SND_PROP_OPT_TRIG_LIMIT RETURN "FMMC_H_SNDT_APL" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = SND_PROP_OPT_TRIG_ONCE_PER_LAP RETURN "FMMC_H_SNDT_OPL" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = SND_PROP_OPT_TRIG_IS_INVISIBLE RETURN "FMMC_H_SNDT_STI" ENDIF
	ENDIF	
	
	IF sFMMCMenu.sActiveMenu = eFmmc_PROP_ADVANCED_OPTIONS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 1	RETURN "MC_H_PRP_PSO" ENDIF //Position offset 
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 2	RETURN "MC_H_PRP_RSO" ENDIF //Rotation offset 
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 3	RETURN "MC_H_PRP_SNPO" ENDIF //Snapping 
		IF (GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 6 AND IS_STUNT_TRACK_LIBRARY(sFMMCMenu.iPropLibrary))   //Cabinet Index full menu
		OR (GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 5 AND NOT IS_STUNT_TRACK_LIBRARY(sFMMCMenu.iPropLibrary)) //Cabinet Index smaller menu
			RETURN "MC_H_PRP_CABI"
		ENDIF
		IF (GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 7 AND IS_STUNT_TRACK_LIBRARY(sFMMCMenu.iPropLibrary))   //Spawn Group full menu
		OR (GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 6 AND NOT IS_STUNT_TRACK_LIBRARY(sFMMCMenu.iPropLibrary)) //Spawn Group smaller menu
			RETURN "MC_H_PRP_SPNG"
		ENDIF
		IF (GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 8 AND IS_STUNT_TRACK_LIBRARY(sFMMCMenu.iPropLibrary))   //Sub Spawn Group full menu
		OR (GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 7 AND NOT IS_STUNT_TRACK_LIBRARY(sFMMCMenu.iPropLibrary)) //Sub Spawn Group smaller menu
			RETURN "MC_H_PRP_SSNG"
		ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_SNAPPING_OPTIONS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SNAPPING_PROXIMITY RETURN "MC_H_PRP_DPX" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SNAPPING_TRIGGERED RETURN "MC_H_PRP_DTR" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SNAPPING_CHAIN RETURN "MC_H_PRP_DCH" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SNAPPING_OVERRIDE RETURN "MC_H_PRP_ORS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SNAPPING_ANGLE RETURN "MC_H_PRP_SNPANG" ENDIF
	ENDIF
	
	IF sFMMCmenu.sActiveMenu = eFMMC_GENERIC_OVERRIDE_POSITION
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = POS_OVERRIDE_USE_OVERRIDE RETURN "MC_H_ORPU" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = POS_OVERRIDE_ALIGNMENT RETURN "MC_H_ORPL" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = POS_OVERRIDE_X_VALUE RETURN "MC_H_ORPX" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = POS_OVERRIDE_Y_VALUE RETURN "MC_H_ORPY" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = POS_OVERRIDE_Z_VALUE RETURN "MC_H_ORPZ" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = POS_OVERRIDE_RESET RETURN "MC_H_ORPR" ENDIF
	ENDIF
	
	IF (sFMMCmenu.sActiveMenu = eFmmc_GENERIC_OVERRIDE_ROTATION)
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ROT_OVERRIDE_USE_OVERRIDE RETURN "MC_H_ORRU" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ROT_OVERRIDE_X_VALUE RETURN "MC_H_ORRX" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ROT_OVERRIDE_Y_VALUE RETURN "MC_H_ORRY" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ROT_OVERRIDE_Z_VALUE RETURN "MC_H_ORRZ" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ROT_OVERRIDE_RESET RETURN "MC_H_ORRR" ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_PROP_BASE
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_ST_FLARE_LAPS RETURN "MC_H_PRP_ENS" ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_PROP_ADVANCED_OPTIONS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_ST_FLARE_LAPS RETURN "MC_H_PRP_ENS" ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFMMC_OFFSET_POSITION
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_OFFSET_USE_OFFSET RETURN "MC_H_PRP_UPO" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_OFFSET_FREE_CAMERA RETURN "MC_H_PRP_PTF" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_OFFSET_LOCK_POSITION RETURN "MC_H_PRP_UFC" ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFMMC_OFFSET_ROTATION
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_OFFSET_USE_OFFSET RETURN "MC_H_PRP_URO" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_OFFSET_FREE_CAMERA RETURN "MC_H_PRP_UFC" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_OFFSET_LOCK_POSITION RETURN "MC_H_PRP_LRO" ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_Number_Of_lives_item
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = FMMC_LIVES_OPTIONS_TYPE
			RETURN "MC_H_NOL1"
			
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = FMMC_LIVES_OPTIONS_COUNT
			IF g_FMMC_STRUCT.sFMMCEndConditions[GET_CREATOR_MENU_SELECTION(sFMMCmenu) - FMMC_LIVES_OPTIONS_COUNT].iPlayerLives = ciNUMBER_OF_LIVES__PLAYER_NUM_VARIABLE	
			OR g_FMMC_STRUCT.sFMMCEndConditions[GET_CREATOR_MENU_SELECTION(sFMMCmenu) - FMMC_LIVES_OPTIONS_COUNT].iPlayerLives = ciNUMBER_OF_LIVES__HALF_PLAYER_NUM_VARIABLE
				RETURN "MC_H_NOL3"
			ELIF g_FMMC_STRUCT.sFMMCEndConditions[GET_CREATOR_MENU_SELECTION(sFMMCmenu) - FMMC_LIVES_OPTIONS_COUNT].iPlayerLives = ciNUMBER_OF_LIVES__DIFFICULTY_VARIABLE
				RETURN "MC_H_NOL4"
			ELSE
				RETURN "MC_H_NOL2"
			ENDIF
			
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = FMMC_LIVES_OPTIONS_DONT_CARRY_DEATHS_ACROSS
			RETURN "MC_H_TESE_18B"
			
		ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_STARTING_TEAM_INVENTORY
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_STARTING_TEAM_INVENTORY_TEAM					
			RETURN "MC_H_STI_TEAM"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_STARTING_TEAM_INVENTORY_START_UNARMED
			RETURN "MC_H_STI_SU"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_STARTING_TEAM_INVENTORY_APPLY_FLASHLIGHT
			RETURN "MC_H_RL_AFL"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_STARTING_TEAM_INVENTORY_TURN_ON_FLASHLIGHT_BY_DEFAULT
			RETURN "MC_H_RL_TFBD"
		ELSE
			RETURN "MC_H_STI_ITEM"
		ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = efmmc_UNARMED_ANIM_ON_CHECKPOINT
		RETURN "MC_H_UACP"
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_MIDMISSION_TEAM_INVENTORY
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MIDMISSION_TEAM_INVENTORY_TEAM RETURN "MC_H_MTI_TEAM" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MIDMISSION_TEAM_INVENTORY_ONLY_ON_RESTART RETURN "MC_H_MTI_RST" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MIDMISSION_TEAM_INVENTORY_TRIGGER_TEAM RETURN "MC_H_MTI_TTM" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MIDMISSION_TEAM_INVENTORY_TRIGGER_RULE RETURN "MC_H_MTI_TRL" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MIDMISSION_TEAM_INVENTORY_ON_PICKUP RETURN "MC_H_MTI_OPU" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MIDMISSION_TEAM_INVENTORY_REMOVE_CURRENT_WEAPONS RETURN "MC_H_MTI_RAW" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MIDMISSION_TEAM_INVENTORY_FORCE_NO_ADDONS RETURN "FMMC_H_MTI_FNA"				 ENDIF
		RETURN "MC_H_MTI_ITEM"
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_MIDMISSION_TEAM_INVENTORY_2
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MIDMISSION_TEAM_INVENTORY_TEAM RETURN "MC_H_MTI_TEAM" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MIDMISSION_TEAM_INVENTORY_ONLY_ON_RESTART RETURN "MC_H_MTI_RST" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MIDMISSION_TEAM_INVENTORY_TRIGGER_TEAM RETURN "MC_H_MTI_TTM" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MIDMISSION_TEAM_INVENTORY_TRIGGER_RULE RETURN "MC_H_MTI_TRL" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MIDMISSION_TEAM_INVENTORY_REMOVE_CURRENT_WEAPONS RETURN "MC_H_MTI_RAW" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MIDMISSION_TEAM_INVENTORY_FORCE_NO_ADDONS RETURN "FMMC_H_MTI_FNA"				 ENDIF
		RETURN "MC_H_MTI_ITEM"
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = efMMC_CUTSCENE_ADD_SCENE
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ADD_CUTSCENE_MENU_CUTSCENE_RANGE RETURN "MC_H_CS_RNG" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ADD_CUTSCENE_MENU_EQUIP_WEAPON_AT_END RETURN "MC_H_CS_EWE" ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFMMC_CUTSCENE_PLAYER_SPECIFIC_OPTIONS
		RETURN GET_HELP_FOR_PLAYER_SPECIFIC_OPTIONS(sFMMCmenu)
	ENDIF

	IF sFMMCMenu.sActiveMenu = eFMMC_CUTSCENE_ANIM_SYNC_SCENE_OPTIONS
		RETURN GET_HELP_FOR_ANIM_SYNC_SCENE_OPTIONS(sFMMCmenu)
	ENDIF

	IF sFMMCMenu.sActiveMenu = eFMMC_CUTSCENE_LIFT_OPTIONS
		IF IS_LEGACY_MISSION_CREATOR()
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = CUTSCENE_LIFT_MENU_LIFT_TYPE
				IF IS_BIT_SET(g_FMMC_STRUCT.sCurrentSceneData.iCutsceneBitset2, ci_CSBS2_UseServerFarmLift)
					RETURN "MC_H_LFT_SRF"
				ELIF IS_BIT_SET(g_FMMC_STRUCT.sCurrentSceneData.iCutsceneBitset2, ci_CSBS2_UseIAABaseLift)
					RETURN "MC_H_LFT_IAB"
				ELSE
					RETURN "MC_H_LFT_NON"
				ENDIF
			ENDIF
		ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = CUTSCENE_LIFT_MENU_MOVEMENT_ON_SHOT RETURN "MC_H_CLF_SHT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = CUTSCENE_LIFT_MENU_START_OFFSET RETURN "MC_H_CLF_STO" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = CUTSCENE_LIFT_MENU_END_OFFSET RETURN "MC_H_CLF_ENO" ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFMMC_CUTSCENE_MOCAP_END_SETTINGS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MOCAP_CUTSCENE_END_SETTINGS_ROLLING_START				RETURN "MC_H_CSA_VAR" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MOCAP_CUTSCENE_END_SETTINGS_ROLLING_START_SPEE			RETURN "MC_H_CS_EWE" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MOCAP_CUTSCENE_END_SETTINGS_GET_NEXT_RULE_OUTFIT			RETURN "MC_H_CLF_STO" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MOCAP_CUTSCENE_END_SETTINGS_EQUIP_WEAPON_AT_END			RETURN "MC_H_CLF_STO" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MOCAP_CUTSCENE_END_SETTINGS_SYNC_ALL_PLAYERS_ON_END		RETURN "MC_H_CLF_STO" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MOCAP_CUTSCENE_END_SETTINGS_USE_DAMAGED_CASINO_DOORS_IPL	RETURN "MC_H_CLF_STO" ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFMMC_CUTSCENE_MOCAP_VARIATION_SETTINGS
		RETURN "MC_H_CSA_VARI"
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = efMMC_CUTSCENE_ADD_MOCAP_SCENE
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ADD_MOCAP_CUTSCENE_MENU_CLEAR_AREA RETURN "MC_H_CCA" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ADD_MOCAP_CUTSCENE_MENU_CUTSCENE_RANGE RETURN "MC_H_CS_RNG" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ADD_MOCAP_CUTSCENE_MENU_INTERRUPT_ON_FAIL RETURN "MC_H_CS_IOF" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ADD_MOCAP_CUTSCENE_KEEP_HATS_MASKS_ETC RETURN "MC_H_CSA_KPH" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ADD_MOCAP_CUTSCENE_FORCE_REG_ENTS_VISIBLE RETURN "MC_H_CSA_FRC" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ADD_MOCAP_CUTSCENE_BLOCK_VOICE_CHAT RETURN "MC_H_CSA_BVC" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ADD_MOCAP_CUTSCENE_PASSENGERS_NOT_SUITABLE RETURN "MC_H_CSA_SGP" ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFMMC_TEAM_SETTINGS	
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_SETTINGS_NUMBER_OF_LIVES RETURN "MC_H_NOL" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_SETTINGS_INITIAL_POINTS RETURN "MC_H_MD4" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_SETTINGS_POINTS_PER_KILL RETURN "MC_H_PPK" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_SETTINGS_MISSION_TARGET_SCORES RETURN "MC_H_MTP" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_SETTINGS_TARGET_SCORE_CRITICAL RETURN "MC_H_TSC" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_SETTINGS_MINIMUM_PLAYERS_ON_TEAM  RETURN "MC_H_MD2" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_SETTINGS_STARTING_HEALTH  RETURN "FMMC_T0_M41H" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_SETTINGS_OVERRIDE_TEAM_COLOR RETURN "MC_H_MD4" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_SETTINGS_SHOW_SCOREBOARD RETURN "MC_H_TEM_STS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_SETTINGS_RESPAWN_OPTIONS  RETURN "FMMC_REOP_6D" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_SETTINGS_TEAM_NAME 			 RETURN "MC_H_TEA_ATFR" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_SETTINGS_CROSS_TEAM_CHAT RETURN "MC_H_CTCHAT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_SETTINGS_CROSS_TEAM_MARKER_VISIBILTY RETURN "MC_H_CTMARK" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_SETTINGS_NUM_RULE_ATTEMPTS RETURN "MC_H_NORA" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_SETTINGS_TEAM_HEALTH_BARS_MENU RETURN "FMMC_TD_THB" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_SETTINGS_TEAM_VEHICLE_MODS_MENU RETURN "MC_H_TD_TVMS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_SETTINGS_DISABLE_HELMET_ARMOUR RETURN "MC_H_DHAMR" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_SETTINGS_CHECKPOINT_0_RESTART_UNARMED RETURN "MC_H_TD_CKUN" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_SETTINGS_CHECKPOINT_1_RESTART_UNARMED RETURN "MC_H_TD_CKUN" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_SETTINGS_CHECKPOINT_2_RESTART_UNARMED RETURN "MC_H_TD_CKUN" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_SETTINGS_CHECKPOINT_3_RESTART_UNARMED RETURN "MC_H_TD_CKUN" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_SETTINGS_PLAYER_PREVENT_FALLS_OUT_WHEN_KILLED RETURN "MC_H_PFOWK" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_SETTINGS_DISABLED_SEAT_SWAPPING RETURN "MC_H_DTSS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_SETTINGS_PLAYER_VEHICLE_RESISTS_EXPLOSIONS RETURN "MC_H_TVRE" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_SETTINGS_START_MISSION_WALKING RETURN "MC_H_PFOWK" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_SETTINGS_TRIGGER_AGGRO_ON_HACK_FAILS RETURN "MC_H_TAHF" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_SETTINGS_SET_LIMITED_THERMAL_CHARGES RETURN "MC_H_SLTC" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_SETTINGS_PER_PLAYER_THERMAL_CHARGES RETURN "MC_H_PPTC" ENDIF
	ENDIF		
	
	IF sFMMCMenu.sActiveMenu = eFmmc_STRING_LIST
		RETURN "MC_H_STRL_OP"
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFMMC_ENTITY_OPTIONS
		RETURN GET_ENTITY_OPTIONS_HELPTEXT(sFMMCMenu)
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_MAIN_OPTIONS_BASE
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_NAME 
			IF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_NO_CLOUD)
				IF bSignedOut
					RETURN GET_DISCONNECT_HELP_MESSAGE()
				ELSE
					RETURN "FMMCNO_CLOUD"
				ENDIF
			ELSE
				RETURN "MC_H_MD0"
			ENDIF
		ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_DESCRIPTION
			IF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_NO_CLOUD)
				IF bSignedOut
					RETURN GET_DISCONNECT_HELP_MESSAGE()
				ELSE
					RETURN "FMMCNO_CLOUD"
				ENDIF
			ELSE
				RETURN "MC_H_MD1"
			ENDIF
		ENDIF
		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_PHOTO RETURN "DMC_H_40" ENDIF
		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_TAGS
			IF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_NO_CLOUD)
				IF bSignedOut
					RETURN GET_DISCONNECT_HELP_MESSAGE()
				ELSE
					RETURN "FMMCNO_CLOUD"
				ENDIF
			ELSE
				RETURN "DMC_H_42"
			ENDIF
		ENDIF
		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_NO_PLAYERS RETURN "MC_H_NOP" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_NO_TEAMS RETURN "MC_H_NOT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_RANK RETURN "MC_H_RANK" ENDIF
		//IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_DETAILS_TEAM_RESPAWN_VEHICLE_STRONG RETURN "MC_H_TRVS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_PSEUDORACE_START_CORONA	 RETURN "MC_H_PSU_RCE" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_BLOCK_REWARD_ON_FAIL RETURN "MC_H_BLK_REW" ENDIF
		//IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_DETAILS_SPAWN_PLAYER_IN_TEAM_VEHICLE RETURN "FMMC_H_SIV1" ENDIF
		//IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_DETAILS_USE_PERSONAL_VEHICLES RETURN "FMMC_H_UPV" ENDIF
		//IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_DETAILS_LIMIT_CUSTOM_VEHICLE_SELECTION RETURN "FMMC_H_LVS" ENDIF		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_DISABLE_SPECTATOR_CAMERA RETURN "MC_H_DI_CAM" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_DISABLE_JIP_SPECTATORS RETURN "MC_H_T0_BJS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_END RETURN "FMMC_T0_M39H" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_TUTORIAL RETURN "MC_H_MD8" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_ONE_PLAY RETURN "FMMC_MH_D43" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_TEAM_DETAILS RETURN "FMMC_T0_M37H" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_ENTITY_OPTIONS RETURN "MC_H_ENTOP" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_FORCE_AT_RANK RETURN "FMMC_T0_M40H" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_AMBIENT RETURN "MC_H_MD20" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_SYNOPSIS RETURN "FMMC_T0_M42H" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_RADAR_SETTINGS RETURN "FMMC_MH_D41" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_MANUAL_RESPAWN_DELAY RETURN "MC_H_MD_MRD" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_SPEED_RACE_CHECKPOINT_TIMER RETURN "MC_H_MD_SRCT" ENDIF
		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_TYPE
			IF g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_HEIST RETURN "MC_H_TYPE1" ENDIF
			IF g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_CONTACT RETURN "MC_H_TYPE2" ENDIF
			IF g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_VERSUS RETURN "MC_H_TYPE3" ENDIF
			IF g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_RANDOM RETURN "MC_H_TYPE4" ENDIF
			IF g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_STANDARD RETURN "MC_H_TYPE0" ENDIF
			IF g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_LTS RETURN "MC_H_TYPE5" ENDIF
			IF g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_CTF RETURN "MC_H_TYPE6" ENDIF
			IF g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_PLANNING RETURN "MC_H_TYPE7" ENDIF
			IF g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_FLOW_MISSION RETURN "MC_H_TYPE8" ENDIF
			IF g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_COOP RETURN "MC_H_TYPE9" ENDIF
		ENDIF
		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_CONTACT_CHAR RETURN "FMMC_MH_D14" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_START_POSITION RETURN "FMMC_MH_D39" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_SMS_OPTIONS RETURN "MC_H_MD10" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_CUSTOM_FAIL_TEXT_OPTIONS RETURN "MC_D_CST_FAL" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_COP_PERCEPTION_OVERRIDE RETURN "MC_D_COP_PER" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_COP_DELAYED_REPORT RETURN "MC_D_COP_DELAY" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_CLOTHING_OPTIONS RETURN "MC_H_CLT_MENU" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_START_TIME RETURN "MC_H_MD11" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_RUN_AS RETURN "MC_H_RA_OPT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_IS_GANG_MISSION RETURN "MC_H_RA_GANG" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_IS_STRAND_MISSION RETURN "MC_H_RA_STRAND" ENDIF
		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_END_TIME RETURN "MC_H_MD12" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_END_TYPE
			IF g_FMMC_STRUCT.iMissionEndType = ciMISSION_SCORING_TYPE_HIGHEST RETURN "MC_H_ENDT1" ENDIF
			IF g_FMMC_STRUCT.iMissionEndType =  ciMISSION_SCORING_TYPE_TIME RETURN "MC_H_ENDT3" ENDIF
			IF g_FMMC_STRUCT.iMissionEndType = ciMISSION_SCORING_TYPE_FIRST_FINISH RETURN "MC_H_ENDT4" ENDIF
			IF g_FMMC_STRUCT.iMissionEndType =  ciMISSION_SCORING_TYPE_ALL_VS_1 RETURN "MC_H_ENDT5" ENDIF
			IF g_FMMC_STRUCT.iMissionEndType =  ciMISSION_SCORING_TYPE_LAST_ALIVE RETURN "MC_H_ENDT6" ENDIF
		ENDIF
		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_CONSTANT_VOICE_CHAT RETURN "MC_H_CVCHAT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_SCRIPT_CAMERA_OPTIONS RETURN "MC_H_SCAM" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_SUDDEN_DEATH_OPTIONS RETURN "MC_H_SDO" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_GAMEPLAY_OPTIONS RETURN "MC_H_GPO" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_AGGRO_INDEX_OPTIONS RETURN "MC_H_AGR_A" ENDIF		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_DISABLED_PLAYER_OPTIONS RETURN "MC_H_DPO" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_CORONA_OPTIONS RETURN "MC_H_CORO" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_MODE_SPECIFIC_OPTIONS RETURN "MC_H_MSPO" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_HEIST_SPECIFIC_OPTIONS RETURN "MC_H_HEISTS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_HUD_OPTIONS RETURN "MC_H_HUDO" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_INIT_AND_INTRO_OPTIONS RETURN "FMMC_IAI" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_TEXT_OPTIONS RETURN "MC_H_HTS" ENDIF
		//IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_DETAILS_SELECTABLE_TEAM_NUMBERS RETURN "MC_H_STN" ENDIF
		//IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_DETAILS_MINIMUM_TEAM_NUMBERS RETURN "MC_H_MTN" ENDIF
		//IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_DETAILS_DEFAULT_TEAM_NUMBERS RETURN "MC_H_DTN" ENDIF
		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_NEGATIVE_SUICIDE_SCORE RETURN "MC_H_NPFS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_NEGATIVE_SUICIDE_SCORE_RULE_CAPPED RETURN "MC_H_NPFSC" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_NEGATIVE_SUICIDE_INCLUDE_DROWNING RETURN "FMMC_H_NPDIS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_LAST_DELIVERER_SHARD RETURN "MC_H_SLDS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_MULTIRULE_TIMER_SUDDEN_DEATH RETURN "MC_H_MRTSD" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_BLOCK_MR_RADIO_IN_LAST_30_SEC RETURN "MC_H_BRLTS"	 ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_DONT_INCREASE_SPAWN_PROTECTION RETURN "MC_H_DMIT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_DUNE_EASY_EXPLOSIONS RETURN "MC_H_DUNE_FLP" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_DISABLE_SPECIAL_VEHICLE_WEAPONS RETURN "MC_H_DSVP" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_TURN_OFF_LIGHTS_AT_START RETURN "FMMC_H_TOLAS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_HALLOWEEN_LIGHTS_OFF RETURN "MC_H_UHLO" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_START_IN_CUTSCENE RETURN "FMMC_H_SICS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_ALLOW_MOC_ENTRY RETURN "FMMC_H_AMOCE"  ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_ENABLE_CARRY_SMOKE_TRAIL RETURN "FMMC_H_ECST"  ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_SMOKE_TRAIL_SCALE RETURN "FMMC_H_CSTS"  ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_ACTUAL_NUM_PLAYERS_RESPAWN RETURN "FMMC_H_AANPR" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_ENABLE_TEAM_TURNS RETURN "MC_H_ETT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_TURNS_USE_FIRST_SPAWN RETURN "MC_H_TUFS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_LOWER_OBJECT_CLEAN_UP RETURN "MC_H_LOCD" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_INCREASED_RANDOM_PHASE_INTRO RETURN "MC_H_ICIRP" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_ROAMINGSPECTATOR_OPTIONS  RETURN "MC_H_ROAMSPEC" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_MISSION_CONTINUITY_OPTIONS  RETURN "MC_H_MICO" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_CUSTOM_STRING_LIST  RETURN "MC_H_STRL_OP" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_DONT_HIDE_SCRIPT_OBJECTS_IN_MODEL_HIDE RETURN "MC_H_DHSOIMH" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_PLAYER_ABILITY_OPTIONS RETURN "FMMC_PAO" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_GLOBAL_GANG_CHASE_CONFIG_OPTIONS RETURN "MC_H_GGCC" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_CUSTOM_VARIABLE_LIST  RETURN "MC_H_CVAR_OP" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_CUSTOM_WEAPON_POOL  RETURN "MC_H_CWEP_OP" ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_END_OF_MISSION_END_CUTSCENE_MENU
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_END_CUTSCENE_NAME RETURN "MC_H_ECS_NAM" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_END_CUTSCENE_PRIMARY_CHARACTER RETURN "MC_H_ECS_PRC" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_END_CUTSCENE_ENTITIES RETURN "MC_H_ECS_CSE" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_END_CUTSCENE_CLEAR_AREA RETURN "MC_H_CCA" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_END_CUTSCENE_CUTSCENE_RANGE RETURN "MC_H_CS_RNG" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_END_QUICK_MOCAP_FADE RETURN "MC_H_CS_UQMF" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_END_KEEP_HATS_GLASSES_ETC RETURN "MC_H_CSA_KPH" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_END_FORCE_REG_ENTS_VISIBLE RETURN "MC_H_CSA_FRC" ENDIF	
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_END_CUTSCENE_STOP_MUSIC RETURN "MC_H_CSA_MUS" ENDIF			
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_END_OF_MISSION_END_CUTSCENE_ENTITIES
		RETURN "MC_H_ECSE"
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = efMMC_END_CUTSCENE_CLEAR
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = END_CUTSCENE_CLEAR_POS RETURN "MC_H_CCA_POS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = END_CUTSCENE_CLEAR_RADIUS RETURN "MC_H_CCA_RAD" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = END_CUTSCENE_CLEAR_NEW_AREA RETURN "MC_H_CCA_NEW" ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_END_OF_MISSION_END_CUTSCENE_ENTITY_OPTIONS
	OR sFMMCMenu.sActiveMenu = efMMC_CUTSCENE_MOCAP_ENTITY_OPTIONS
	OR sFMMCMenu.sActiveMenu = efMMC_CUTSCENE_ENTITIES_ENTITY_OPTIONS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MOCAP_CUTSCENE_ENTITY_ADD_HANDLE RETURN "MC_H_CSEO_HND" ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = efMMC_CUTSCENE_MOCAP_CLEAR
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MOCAP_CUTSCENE_CLEAR_POS RETURN "MC_H_CCA_POS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MOCAP_CUTSCENE_CLEAR_RADIUS RETURN "MC_H_CCA_RAD" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = MOCAP_CUTSCENE_CLEAR_NEW_AREA RETURN "MC_H_CCA_NEW" ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_TEAM_DETAILS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_DETAILS_ADJUSTABLE_TEAMS RETURN "MC_H_MD7" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_DETAILS_DISABLE_TEAM_BALANCING RETURN "MC_H_MD72" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_DETAILS_USE_GRID_START RETURN "MC_H_MD73" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_DETAILS_MOVE_WINNER_TO_SMALL_TEAM RETURN "MC_H_MWTST" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_DETAILS_TEAM_RELATIONSHIPS RETURN "MC_H_MD3" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_DETAILS_LOCK_STYLE_ALL_TEAMS RETURN "MC_H_TD_LSAT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_DETAILS_INDIVIDUAL_SPAWNS RETURN "MC_H_TD_SAIS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_DETAILS_FORCE_TEAM_OUTFITS RETURN "MC_H_TD_FTO" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_DETAILS_POST_MISSION_SPAWN_ON_FAIL RETURN "MC_H_PMPSF" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_DETAILS_POST_MISSION_PLAYER_SPAWN RETURN "MC_H_PMP_TM"	 ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_DETAILS_ALWAYS_SWAP_SMALL_TEAM RETURN "MC_H_ASST"	 ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_DETAILS_FLASH_PICKUPS_FOR_ALL_TEAMS RETURN "FMMC_H_HPT"	 ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_DETAILS_CRITICAL_TEAM_KILL_SELF RETURN "MC_H_CTKS"	 ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_DETAILS_DROP_ZONE_TEAM_COLOURS RETURN "MC_H_DZTC"	 ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_DETAILS_MISSION_TEAM_BALANCE 	RETURN "MC_H_TEA_BAL"	 ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_DETAILS_TEAM_NAME_FROM_COLOUR RETURN "MC_H_GNFC"	 ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_DETAILS_ALTERNATIVE_TEAM_NAMING RETURN "MC_H_GNFC"	 ENDIF // Re-use the same description as TEAM_DETAILS_TEAM_NAME_FROM_COLOUR
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_DETAILS_CLEAR_UP_VEHICLE_ON_GAME_LEAVE RETURN "MC_H_TD_CUVA" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_DETAILS_USE_AVAILABLE_TEAMS_BALANCING RETURN "MC_H_TBKB" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_DETAILS_RESPAWN_HYDRAS_IN_FLIGHT_MODE RETURN "MC_H_RHFM" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_DETAILS_USE_SMOKE_ON_OUTFIT_CHANGE RETURN "MC_H_USOOC" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_DETAILS_PROOFS_INVALIDATED_ON_FOOT RETURN "FMMC_H_PIOF" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_DETAILS_SEQUENTIAL_PLAYER_START_SPAWN RETURN "MC_H_SPSS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_DETAILS_PUT_PLAYER_IN_VEH_FAST RETURN "MC_H_PPIVF" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_DETAILS_TEAM_ALLOW_VEH_COLOUR RETURN "MC_H_TVC" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_DETAILS_TEAM_ALLOW_VEH_MOD_PRESET RETURN "MC_H_MPTV" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_DETAILS_USE_LIMITED_TEAM_LIVES RETURN "MC_H_ULTL" ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_TEAM_RESPAWN_OPTIONS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_RESPAWN_OPT_RESPAWN_VEHICLE RETURN "MC_H_MD6" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_RESPAWN_OPT_RESPAWN_VEHICLE_COLOUR RETURN "FMMC_REOP_0D" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_RESPAWN_OPT_RESPAWN_VEHICLE_SECONDARY_COLOUR RETURN "FMMC_REOP_0D" ENDIF		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_RESPAWN_OPT_LOCK_RESPAWN_VEHICLES RETURN "FMMC_REOP_1D" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_RESPAWN_OPT_BLIP_RESPAWN_VEHICLES RETURN "MC_H_TD_BRV" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_RESPAWN_OPT_RESPAWN_VEHICLE_HEALTH RETURN "FMMC_REOP_2D" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_RESPAWN_OPT_RESPAWN_VEHICLE_WEAPON_DAMAGE_SCALE RETURN "FMMC_REOP_2DB" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_RESPAWN_OPT_RESPAWN_VEHICLE_TYRE_TYPE RETURN "MC_H_TD_RVTT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_RESPAWN_OPT_RESTART_IN_CORONA_VEHICLE RETURN "FMMC_REOP_3D" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_RESPAWN_OPT_RESPAWN_WHERE_OUT_OF_VEHICLE RETURN "MC_H_ROUTV" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_RESPAWN_OPT_RESPAWN_WHEN_VEHICLE_UNDRIVEABLE RETURN "MC_H_RUNDV" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_RESPAWN_OPT_LOSE_LIFE_FORCE_RESPAWN RETURN "MC_H_RLLOR" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_RESPAWN_OPT_RESPAWN_MINIMUM_DISTANCE RETURN "FMMC_REOP_4D" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_RESPAWN_OPT_FORCE_RESPAWN_FOR_SUBMERGED_VEHICLE RETURN "MC_H_FRSUBV" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_RESPAWN_OPT_MANUAL_RESPAWN RETURN "FMMC_REOP_5D" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_RESPAWN_OPT_BLOCK_HELP_TEXT RETURN "FMMC_REOP_7D" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_RESPAWN_OPT_ALLOW_MANUAL_RESPAWN_WITH_PACKAGES RETURN "FMMC_H_AMRWP" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_RESPAWN_OPT_RESTRICT_VEHICLE_ENTRY RETURN "FMMC_REOP_8D" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_RESPAWN_OPT_KILL_OUT_OF_VEH RETURN "FMMC_REOP_15D" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_RESPAWN_FORCE_KEEP_HELMETS	 RETURN "FMMC_REOP_18D" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_RESPAWN_IN_PLACED_VEH_CHECKPOINT_0 RETURN "FMMC_REOP_21D" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_RESPAWN_IN_PLACED_VEH_CHECKPOINT_1 RETURN "FMMC_REOP_22D" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_RESPAWN_IN_PLACED_VEH_CHECKPOINT_2 RETURN "FMMC_SIVCK3D" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_RESPAWN_IN_PLACED_VEH_CHECKPOINT_3 RETURN "FMMC_SIVCK4D" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_RESPAWN_BLOCK_WEAPON_SWAP_ON_PICKUP RETURN "FMMC_REOP_26D" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_RESPAWN_ENV_EFF_SCALE RETURN "MC_H_VEH_ENVEFF" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_RESPAWN_SAME_VEHICLE_MENU RETURN "MC_H_TRSVO"	 ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_RESPAWN_DISABLE_HOMING_MISSILES RETURN "FMMC_H_AMRWP" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_RESPAWN_OPT_RESPAWN_VEHICLE_SPACE_ROCKETS RETURN "MC_H_VEH_DSR" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_RESPAWN_CUSTOM_RESPAWN_FACE_NEAREST_OBJECTIVE RETURN "MC_H_VEH_RPFO" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_RESPAWN_ALLOW_SPAWN_VEHICLE_TO_TARGET_OBJECTS RETURN "MC_H_VEH_ATO" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_RESPAWN_LOCK_VEHICLE_FOR_ALL_BUT_PLAYER RETURN "MC_H_VEH_PERV" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_RESPAWN_IN_LAST_VEHICLE_LIST RETURN "MC_H_TLVL_OPT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_RESPAWN_PREFER_FRONT_SEATS RETURN "MC_H_VEH_PFS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_RESPAWN_DELUXO_HOVER RETURN "MC_H_VEH_DHVM" ENDIF 
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_RESPAWN_MOD_PRESET RETURN "MC_H_VEH_TVPM" ENDIF 
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFMMC_TEAM_RESPAWN_SAME_VEHICLE
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_MULTIPLE_PLAYER_SAME_VEHICLE_RESPAWN RETURN "MC_H_EPRSV" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_SPAWN_DRIV_SAME_AVAILABLE_VEH RETURN "MC_H_SDSAV" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_FORCE_PASSENGERS_TO_RESP_ON_DRIV_RESP RETURN "MC_H_FPRODD" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_OPT_RESPAWN_VEHICLES_MAX_NUM_FOR_TEAM RETURN "MC_H_REOP_23" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_CHOOSE_SEAT_PREFERENCE_IN_CORONA RETURN "MC_H_PSPIC"	 ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_STOP_PLAYER_FROM_SHUFFLING_SEATS RETURN "MC_H_DNASS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_OLD_TEAM_VEHICLE_CLEANUP RETURN "MC_H_DDCUOV" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_OLD_TEAM_VEHICLE_DELETION RETURN "MC_H_DDOOVE" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_PRIORITIZE_WEAPONIZED_SEATS RETURN "MC_H_PPVWS"	 ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_ALLOW_PLAYERS_TO_SWAP_SEATS RETURN "MC_H_APTSS"		 ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_SEAT_SWAP_REQUIRES_CONSENT RETURN "MC_H_SSRC" ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = efmmc_TEAM_DISABLE_PLAYER_OWNED_OUTFIT
		RETURN "MC_H_DPOO"
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = efmmc_INTERIOR_METH_LABS
		RETURN GET_METH_LABS_HELP_TEXT(sFMMCmenu)
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = efmmc_CASINO_BASE
		RETURN GET_CASINO_HELP_TEXT(sFMMCmenu)
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = efmmc_CASINO_ENTITY_SETS
		RETURN GET_CASINO_ENTITY_SETS_HELP_TEXT(sFMMCmenu)
	ENDIF
	
	IF sFMMCmenu.sActiveMenu = efmmc_ARENAMENUS_TOP
	OR sFMMCmenu.sActiveMenu = efmmc_ARENAMENUS_LVL1
	OR sFMMCmenu.sActiveMenu = efmmc_ARENAMENUS_LVL2
	OR sFMMCmenu.sActiveMenu = efmmc_ARENAMENUS_LVL3
	OR sFMMCmenu.sActiveMenu = efmmc_ARENAMENUS_LVL4
	OR sFMMCmenu.sActiveMenu = efmmc_ARENAMENUS_MISC
	OR sFMMCmenu.sActiveMenu = efmmc_ARENAMENUS_EXTRAS
	OR sFMMCmenu.sActiveMenu = efmmc_ARENAMENUS_SETTINGS
		RETURN GET_ARENA_HELPTEXT(sFMMCmenu)
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = efmmc_ARENAMENUS_SETTINGS
		RETURN EDIT_ARENA_OPTIONS_MENU_HELPTEXT(sFMMCmenu)
	ENDIF
	
	//Survival Descriptions
	IF sFMMCMenu.sActiveMenu = eFmmc_SURVIVAL_Wave_Adv_Config
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_SURVIVAL_Squad_Config
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_SURVIVAL_Veh_Config_base
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_TEAM_OUTFIT_STYLES_LIST
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 0
			RETURN "MC_H_THOS_OS"
		ELSE
			RETURN "MC_H_THOS_OSI"
		ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_TEAM_MASKS_LIST
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 0
			RETURN "MC_H_TMSK_MS"
		ELSE
			RETURN "MC_H_TMSK_MSI"
		ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_VEHICLE_TEAM_SEATING_PREFERENCES_MENU
		INT iTeam
		FOR iTeam = 0 TO (FMMC_MAX_TEAMS - 1)
			RETURN "MC_H_VEH_TSP0"
		ENDFOR
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFMMC_TEAM_SHOW_TEAM_SCOREBOARD
		RETURN "MC_H_STS_TM"
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFMMC_TEAM_SHOW_LAPS_HUD
		RETURN "MC_H_DLH_TM"
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFMMC_TEAM_SET_LAP_LIMIT
		RETURN "MC_H_SLL_TM"	
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFMMC_TEAM_DRAW_RACE_POSITION
		RETURN "MC_H_DRP_TM"
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFMMC_TEAM_CROSS_TEAM_CHAT
		RETURN "MC_H_CTCHAT_TM"
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFMMC_TEAM_CROSS_TEAM_MARKER_VISIBILITY
		RETURN "MC_H_CTMARK_TM"
	ENDIF
	
	IF sFMMCmenu.sActiveMenu = eFMMC_AVAILABLE_VERSUS_OUTFITS
		IF IS_VERSUS_OUTFIT_STYLE_VALID(sFMMCMenu.iSelectedVsStyle, INT_TO_ENUM(VERSUS_OUTFIT_STYLE, GET_CREATOR_MENU_SELECTION(sFMMCmenu)), 3)
		AND IS_VERSUS_OUTFIT_STYLE_VALID(sFMMCMenu.iSelectedVsStyle, INT_TO_ENUM(VERSUS_OUTFIT_STYLE, GET_CREATOR_MENU_SELECTION(sFMMCmenu)), 4)
			RETURN "FMMC_AVL_234"
		ENDIF
		IF IS_VERSUS_OUTFIT_STYLE_VALID(sFMMCMenu.iSelectedVsStyle, INT_TO_ENUM(VERSUS_OUTFIT_STYLE, GET_CREATOR_MENU_SELECTION(sFMMCmenu)), 4) RETURN "FMMC_AVL_24" ENDIF
		IF IS_VERSUS_OUTFIT_STYLE_VALID(sFMMCMenu.iSelectedVsStyle, INT_TO_ENUM(VERSUS_OUTFIT_STYLE, GET_CREATOR_MENU_SELECTION(sFMMCmenu)), 3) RETURN "FMMC_AVL_23" ENDIF
		RETURN "FMMC_AVL_2"
	ENDIF
		
	IF sFMMCMenu.sActiveMenu = eFmmc_TEAM_CLOTHING_OPTIONS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_CLOTHING_OPTIONS_TEAM_OUTFITS RETURN "MC_H_OUTFITS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_CLOTHING_OPTIONS_TEAM_HEIST_OUTFIT_STYLES RETURN "MC_H_TD_HOS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_CLOTHING_OPTIONS_TEAM_HEIST_MASKS RETURN "MC_H_TD_MSK" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_CLOTHING_OPTIONS_TEAM_DEFAULT_MASK_GROUP RETURN "" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_CLOTHING_OPTIONS_TEAM_GEAR RETURN "" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_CLOTHING_OPTIONS_GIVE_NIGHTVISION RETURN "MC_H_TD_GNIA" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_CLOTHING_OPTIONS_USE_BAG_VARIATION RETURN "MC_H_TD_UCHB" ENDIF 
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_CLOTHING_OPTIONS_LOCK_TEAM_OUTFIT RETURN "" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_CLOTHING_OPTIONS_DISABLE_PLAYER_OWNED_OUTFIT RETURN "MC_H_TD_DPOO" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_CLOTHING_OPTIONS_HIDE_FACEPAINT RETURN "" ENDIF
		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_CLOTHING_OPTIONS_MASK_DONT_APPLY 		RETURN "MC_G_DRM" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_CLOTHING_OPTIONS_MASK_DONT_APPLY_CP_0 	RETURN "MC_G_DRM1" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_CLOTHING_OPTIONS_MASK_DONT_APPLY_CP_1 	RETURN "MC_G_DRM2" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_CLOTHING_OPTIONS_MASK_DONT_APPLY_CP_2 	RETURN "MC_G_DRM3" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TEAM_CLOTHING_OPTIONS_MASK_DONT_APPLY_CP_3 	RETURN "MC_G_DRM4" ENDIF
		
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_SMS_OPTIONS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_SMS_TREAT_AS_TEXT_LABEL RETURN "MC_H_SMS_TL" ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_WEAPON_OPTIONS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_WO_LOCKWEP RETURN "Add description 1" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_WO_STARTWEP RETURN "Add description 2" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_WO_RESTRICTIONS RETURN "Add description 3" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_WO_STARTING_TEAM_INVENTORY RETURN "MC_H_STI" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_WO_MIDMISSION_TEAM_INVENTORY RETURN "MC_H_MTI" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_WO_MIDMISSION_TEAM_INVENTORY_2 RETURN "MC_H_MTI2" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_WO_USE_CASINO_HEIST_INVENTORY RETURN "MC_H_UCHVI" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_WO_HELI_CAM RETURN "Add description 4" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_WO_BLOCK_DROP_WEAPON_ON_WHEEL RETURN "MC_H_BLK_DOW" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_WO_BLOCK_PLAYER_DROPPED_AMMO RETURN "MC_H_BLK_PAMO" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_WO_MELEE_DAMAGE_MODIFIER RETURN "MC_H_MLE_DM" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_WO_BLOCK_AMMO_PURCHASE RETURN "MC_H_BIMA" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_WO_RUGBY_RESPAWN RETURN "MC_H_RRW" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_WO_MINIGUN_AMMO_OVERRIDE RETURN "MC_H_MCSO" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_WO_RESET_INVENTORY_ON_DEATH RETURN "MC_H_MD_RID" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_WO_STUNGUN_CHARGE RETURN "MC_H_W_STUNGC" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_WO_STICKY_BOMB_AOE RETURN "MC_H_W_AOESB" ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_CUSTOM_FAIL_TEXT_OPTIONS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_CFT_PED_FAIL_NAMES RETURN "Add description 1" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_CFT_VEH_FAIL_NAMES RETURN "Add description 2" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_CFT_OBJ_FAIL_NAMES RETURN "Add description 3" ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_PED_PRIORITY_OPTIONS
		IF IS_LEGACY_MISSION_CREATOR()
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_PP_HIGH_PRIORITY_PED
			OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_PP_2ND_HIGH_PRIORITY_PED
			OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_PP_3RD_HIGH_PRIORITY_PED
				RETURN "MC_H_MD_HPP"
			ENDIF
		ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_PERSONAL_VEH_OPTIONS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_PV_DISABLE_PERSONAL_VEH RETURN "MC_H_DIS_PVH" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_PV_PERSONAL_VEH_AT_START RETURN "MC_H_PVH_STR" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_PV_PERSONAL_VEH_AT_CHECKPOINT_0 RETURN "MC_H_PVH_QR" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_PV_PERSONAL_VEH_AT_CHECKPOINT_1 RETURN "MC_H_PVH_CH1" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_PV_PERSONAL_VEH_AT_CHECKPOINT_2 RETURN "MC_H_PVH_CH2" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_PV_PERSONAL_VEH_AT_CHECKPOINT_3 RETURN "MC_H_PVH_CH3" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_PV_SPAWN_AT_PROPERTY RETURN "MC_H_PVH_SAP" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_PV_ADD_TEMPORARY_NODES RETURN "MC_H_PVH_TPVN" ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_DISABLE_VEHICLE_WEAPON_OPTIONS
		SWITCH GET_CREATOR_MENU_SELECTION(sFMMCmenu)
			CASE OPTION_MISSION_DISABLE_VALKYRIE_FRONT_GUN
				RETURN "FMMC_MD_DVFG"
			CASE OPTION_MISSION_DISABLE_RUINER_ROCKETS
				RETURN "FMMC_MD_DRR"
		ENDSWITCH
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_STUNT_OPTIONS
		SWITCH GET_CREATOR_MENU_SELECTION(sFMMCmenu)
			CASE OPTION_STUNTS_ENABLE 		RETURN "MC_H_SO_EST"
			CASE OPTION_STUNTS_ENABLE_BOOST RETURN "MC_H_SO_ESB"
			CASE OPTION_STUNTS_BOOST_AMOUNT RETURN "MC_H_SO_SBM"
		ENDSWITCH
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_COP_PERCEPTION_OVERRIDE
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_CPO_SEEING_RANGE RETURN "MC_D_COP_PER_SR" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_CPO_PERIPHERAL_RANGE RETURN "MC_D_COP_PER_PR" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_CPO_HEARING_RANGE RETURN "MC_D_COP_PER_HR" ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFMMC_BMB_OPTIONS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_BMB_ENABLED RETURN "MC_D_BMB_MBE" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_BMB_BOMBS_PER_PLAYER RETURN "MC_D_BMB_BPP" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_BMB_BOMB_BLAST_RADIUS RETURN "MC_D_BMB_BBR" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_BMB_BOMB_TIMER RETURN "MC_D_BMB_BMT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_BMB_EXPLOSION_STAGGER_INTERVAL RETURN "MC_D_BMB_ESI" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_BMB_EXPLOSION_DISTANCE_INCREMENT RETURN "MC_D_BMB_EDI" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_BMB_EXPLOSION_INITIAL_OFFSET RETURN "MC_D_BMB_EIO" ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_DAWN_RAID_OPTIONS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_DAWN_RAID_ENABLE_MODE RETURN "MC_D_DR_ENBL" ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_HOSTILE_TAKEOVER_OPTIONS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_HT_ENABLE_BLIP_COLOURS RETURN "FMMC_HT_H_EBCC" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_HT_ENABLE_CONTESTED_COLOUR_LOCATE RETURN "MC_HT_H_ELCC" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_HT_ENABLE_PERSONAL_LOCATE_POINTS RETURN "FMMC_HT_H_CPLP" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_HT_ENABLE_INSTANT_COLOUR_CHANGES RETURN "MC_HT_H_ELICC" ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_BOMBUSHKA_OPTIONS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_BOMBUSHKA_CHECK_SCORE_FINAL_LAP	 RETURN "MC_H_USKA_1" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_BOMBUSHKA_GIVE_POINT_HIGHEST_SCORER	 RETURN "MC_H_USKA_2" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_BOMBUSHKA_END_EARLY_LAPS	 RETURN "MC_H_USKA_3" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_BOMBUSHKA_END_EARLY_ROUND	 RETURN "MC_H_USKA_4"	 ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_OVERTIME_OPTIONS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_OVERTIME_WAIT_YOUR_TURN RETURN "MC_D_OT_WYT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_OVERTIME_POINTS_ALLOCATION_ZONE RETURN "MC_D_OT_PAZ" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_OVERTIME_PROP_SHAPE_TEST RETURN "MC_D_OT_PTG" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_OVERTIME_VEH_MUST_BE_STOPPED_TO_PASS_LOCATE RETURN "MC_D_OT_VMTS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_OVERTIME_VEH_ALL_STOPPED_TO_PASS_LOCATE RETURN "MC_D_OT_AVMS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_OVERTIME_ROUNDS RETURN "MC_D_OT_NOR" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_OVERTIME_ROUNDS_SCALE_BY_PLAYERS RETURN "MC_D_OT_SRBP" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_OVERTIME_ROUNDS_POINTS_CLEAR RETURN "MC_D_OT_WSM" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_OVERTIME_SUDDEN_DEATH_ROUNDS RETURN "MC_D_OT_SD" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_OVERTIME_GHOST_INACTIVE_PLAYERS RETURN "MC_D_OT_GIP" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_OVERTIME_DISABLE_NEW_TURN_SHARD RETURN "MC_D_OT_HNTS"  ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_OVERTIME_USE_JUMP_SHARD RETURN "MC_D_OT_URS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_OVERTIME_START_IN_SPECTATE RETURN "MC_D_OT_SIS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_OVERTIME_TAREGT_SHARD_SINGULAR RETURN "MC_D_OT_USST" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_OVERTIME_BLIMP_PLAYER_NAME RETURN "MC_D_OT_PNOB" ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_OVERTIME_OPTIONS_POINTS_ZONE
	OR sFMMCMenu.sActiveMenu = eFmmc_OVERTIME_OPTIONS_POINTS_ZONE_EDIT
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_OT_PAZ_CREATE_AND_MODIFY_NEW_POINTS_ZONE RETURN "MC_D_PAZ_CRE" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_OT_PAZ_DELETE_POINT_ZONE_INDEX RETURN "MC_D_PAZ_DEL" ENDIF
		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_OT_PAZ_DISABLE_CAR_ACTIONS_ACCELERATING
		OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_OT_PAZ_DISABLE_CAR_ACTIONS_BRAKING
		OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_OT_PAZ_DISABLE_CAR_ACTIONS_JUMPING
		OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_OT_PAZ_DISABLE_CAR_ACTIONS_STEERING
			RETURN "MC_D_PAZ_DIS"
		ENDIF
		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_OT_PAZ_POINTS_TO_GIVE RETURN "MC_D_PAZ_PTG"			 ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_OT_PAZ_ZONE_SHAPE RETURN "MC_D_PAZ_SHA"			 ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_OT_PAZ_ZONE_RADIUS RETURN "MC_D_PAZ_RAD" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_OT_PAZ_ZONE_PRIORITY_LAYER RETURN "MC_D_PAZ_PLP"			 ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_OT_PAZ_OFFSET_HEIGHT RETURN "MC_D_PAZ_HEI"			 ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_PRON_MODE
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_PRON_MODE RETURN "MC_H_PRON_MDE" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_PRON_MODE_VEH_VENDETTA RETURN "MC_H_PRON_VVD" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_PRON_MODE_DEATHMATCH_TOGGLE RETURN "MC_H_PRON_DMT" ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_SCRIPT_CAM_OPTIONS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_ENABLE_BIRDS_EYE RETURN "MC_H_SCAM_EBE" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_BIRDS_EYE_LOCK_NORTH RETURN "MC_H_SCAM_LCN" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_ENABLE_SCRIPTED_RACE_CAMS RETURN "MC_H_SCAM_SRC" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_ALLOW_PLAYER_TOGGLE RETURN "MC_H_SCAM_APT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_FORCE_STATIC_CAMERA_LOCATIONS RETURN "MC_H_SCAM_FSL" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_CREATE_CAMERA_LOCATION RETURN "MC_H_SCAM_CRE" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_CREATE_CAMERA_LOCATION_DEBUG RETURN "MC_H_SCAM_DRE" ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_SUDDEN_DEATH_OPTIONS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_LOCATION_ELIMINATION RETURN "MC_H_LELM" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_LOCATION_ELIMINATION_HIGHEST_DRAWING RETURN "MC_H_LOC_OHDT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_DONT_OVERRIDE_BOUNDS_TIMERS_SD RETURN "MC_H_LOC_URSD" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_LOCATION_SHRINKING_AREA_SUDDEN_DEATH RETURN "MC_H_SASD" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_BLIP_PLAYERS_AT_SUDDEN_DEATH RETURN "MC_H_BPASD" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_TEAM_PASS_FAIL_SUDDEN_DEATH RETURN "MC_H_PFSD" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_OVERTIME_RUMBLE_SUDDEN_DEATH RETURN "MC_H_ORSD" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_POWER_PLAY_SUDDEN_DEATH_CAPTURE RETURN "MC_H_PPSDC" ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFMMC_OVERTIME_RUMBLE_SUDDEN_DEATH_OPTIONS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_OTRSD_ENABLED RETURN "MC_H_OSDE" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_OTRSD_START_RULE RETURN "MC_H_OSDS" ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_INIT_AND_INTRO_OPTIONS
		RETURN GET_INIT_AND_INTRO_OPTIONS_DESCRIPTION(sFMMCmenu)
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_GAMEPLAY_OPTIONS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_REDUCE_DIFFICULTY_ON_RETRY RETURN "MC_H_RDO_R" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_MULTI_RULE_FAILS_EVERYONE RETURN "MC_H_MRTFE" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_USE_LIVES_REMAINING  RETURN "MC_H_ULR" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_ENABLE_ASYMMETRIC_TEAM_BALANCING RETURN "MC_H_EATB" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_RULE_BASED_SCORE_SCALING RETURN "FMMC_H_ERTSS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_FORCE_PASSENGERS_TO_RESP_ON_DRIV_RESP RETURN "MC_H_FPRODD" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_FORCE_TEAM_BASED_CONTROL_OBJ_BARS RETURN "MC_H_FCOTB" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_ENABLE_EVERY_FRAME_DROP_OFF RETURN "MC_H_EEFDO" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_ENABLE_PASS_MISSION_TEAM_OWNS_ALL_OBJ RETURN "MC_H_PMOBJ"	 ENDIF		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_ALTERNATE_GD_SCORING RETURN "MC_H_EAGDS"  ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_POINT_PER_PACKAGE RETURN "MC_H_GPPP" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_DELAY_RECAPTURE_OF_LOCATES RETURN "MC_H_DROLC" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_SYNC_CAPTURE_POINTS RETURN "MC_H_SCS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_FORCE_START_POSITIONS_ON_HEIST_STRAND RETURN "MC_H_FSPOH" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_STROMBERG_SUBMARINE_GPS RETURN "MC_H_SSGPS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_AIRQUOTA_ALT_SEQUENCE_OPTION RETURN "MC_H_ALTAQOPT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_IMMEDIATE_UNDRIVEABLE_DEATH RETURN "MC_H_IMUDD" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_EXPEDITE_AND_DONT_REVALIDATE_DIAG_TRIGGERS RETURN "MC_H_EDRDT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_MIN_SPEED_LOCAL_ONLY RETURN "MC_H_MSLON" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_SPAWN_PROTECTION_DURATION RETURN "MC_H_SPAD" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_RESYNC_MAIN_TIMERS RETURN "MC_H_RSYNC" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_ALLOW_AI_AIRSTRIKES RETURN "FMMC_H_AAIA" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_ENABLE_TAGGING_TARGETS RETURN "MC_H_EETT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_ALL_TEAMS_ALL_LAPS RETURN "MC_H_ATAL" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_ALL_TEAMS_FINISH_EARLY_BASED_ON_LAPS RETURN "MC_H_FETSL" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_DONT_CANCEL_MANUAL_RESPAWN_AT_END RETURN "MC_H_DCMRAE" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_FORCE_CUSTOM_SPAWNPOINTS_AT_END RETURN "MC_H_FUCRAE" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_MR_TIMER_END_ROUND_RESTART RETURN "MC_H_EMRTE" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_POINTS_STEAL_ON_BIKE_MELEE RETURN "FMMC_H_PSBM" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_MAKE_MR_TIMER_WAIT_FOR_OBJ_TIMER RETURN "MC_H_DMROB" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_321_RESET_ON_ROUND_RESTART RETURN "FMMC_RES321" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_GAME_LOGIC_RACE_INTRO RETURN "MC_H_MD_NIN" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_GAME_LOGIC_RACE_BOOST RETURN "FMMC_MD_RBS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_GAME_LOGIC_HEALTH_ON_KILL RETURN "MC_H_HBOK" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_GAME_LOGIC_ADVANCED_CAPTURE_STATE_LOGIC RETURN "MC_H_EACL" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_GAME_LOGIC_CUSTOM_TEXT_CAPTURE_BAR RETURN "MC_H_CTCB" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_GAME_LOGIC_PERFORM_INITIAL_SCORE RETURN "MC_H_PISC" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_GAME_LOGIC_BLOCK_MAN_RESPAWN_AT_END RETURN "MC_H_BMRAE" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_GAME_LOGIC_KEEP_RANDOM_OBJECTIVE_SEED RETURN "MC_H_RNOQR" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_GAME_LOGIC_CRITICAL_LOBBY_HOST RETURN "MC_H_LHICP" ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_SHOWDOWN_OPTIONS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_SHOWDOWN_ENABLE RETURN "MC_H_SD_ENABLE" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_SHOWDOWN_TICK_RATE RETURN "MC_H_SD_TICK" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_SHOWDOWN_DAMAGE RETURN "MC_H_SD_DMG" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_SHOWDOWN_STARTINGPOINTS RETURN "MC_H_SD_STP" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_SHOWDOWN_MAXIMUMPOINTS RETURN "MC_H_SD_MXP" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_SHOWDOWN_DRAIN_RATE_TWOPLAYERS RETURN "MC_H_SD_TICK_2P" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_SHOWDOWN_RESPAWN_COST RETURN "MC_H_SD_RECOST" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_SHOWDOWN_IDLE_TICK_DAMAGE RETURN "MC_H_SD_IDLEDMG" ENDIF
	ENDIF
		
	IF sFMMCMenu.sActiveMenu = eFmmc_MODE_TOWER_DEFENCE_OPTIONS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_TOWER_DEFENCE_ENABLE RETURN "MC_H_TDD_ENBL" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_TOWER_DEFENCE_TOWERS_LEFT_UI RETURN "MC_H_TDD_TOWL" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_TOWER_DEFENCE_TOWER_HEALTH_UI RETURN "MC_H_TDD_TOWH" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_TOWER_DEFENCE_FORCE_RESPAWN_VEH_AS_START_VEH RETURN "MC_H_TDD_RSF" ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_MODE_MONSTER_JAM_OPTIONS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MONSTER_JAM_ENABLE_TIME_BALANCING RETURN "MC_H_MJ_ETB" ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_BOMB_FOOTBALL_OPTIONS
		//IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_BMBF_GOAL_POINTS RETURN "BF_H_GOALP" ENDIF
	ENDIF

	IF sFMMCMenu.sActiveMenu = eFmmc_MODE_TAG_TEAM_OPTIONS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_TAG_TEAM_ENABLE_TURN_ROTOR RETURN "MC_H_TAGT_ROT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_TAG_TEAM_ENABLE_SUDDEN_DEATH RETURN "MC_H_TAGT_SD" ENDIF		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_TAG_TEAM_DISABLE_CORONAS_UNTIL_TAG_OUT RETURN "MC_H_TAGT_COR" ENDIF	
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_TAG_TEAM_COOLDOWN_FOR_TAG_OUT RETURN "MC_H_TAGT_CLD" ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_MODE_HANDHELD_DRILL_MINIGAME_OPTIONS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_HANDHELD_DRILL_MINIGAME_ENABLE RETURN "MCH_DRIL_1" ENDIF	
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_HANDHELD_DRILL_MINIGAME_GIVE_ALL_PLAYERS_DRILL RETURN "MCH_DRIL_2" ENDIF	
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_HANDHELD_DRILL_MINIGAME_SAFETY_DEPOSIT_BOX_DRILL_TIME RETURN "MCH_DRIL_3" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_HANDHELD_DRILL_MINIGAME_ASSOCIATED_SPAWN_GROUP RETURN "MCH_DRIL_4" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_HANDHELD_DRILL_MINIGAME_FORCE_SPAWN_GROUP_ACTIVE RETURN "MCH_DRIL_5" ENDIF		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_HANDHELD_DRILL_MINIGAME_MAINTAIN_SELECTED_SPAWN_GROUP RETURN "MCH_DRIL_6" ENDIF				
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_MODE_CASINO_HEIST_OPTIONS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_CASINO_HEIST_ALARM_ON_AGGRO RETURN "MC_H_CHO_A" ENDIF	
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_CASINO_HEIST_ALARM_AT_START RETURN "MC_H_CHO_AST" ENDIF	
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_CASINO_HEIST_MAINTAIN_LIVES_AIRLOCK RETURN "MC_H_CHO_LHA" ENDIF	
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_CASINO_HEIST_MAINTAIN_THERMAL_AIRLOCK RETURN "MC_H_CHO_CHA" ENDIF	
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_CASINO_HEIST_MAINTAIN_TAKE_AIRLOCK RETURN "MC_H_CHO_THA" ENDIF	
	ENDIF
		
	IF sFMMCMenu.sActiveMenu = eFmmc_MODE_GAME_MASTERS_OPTIONS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_GAME_MASTERS_ENABLE RETURN "MC_H_GAM_ENBL" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_GAME_MASTERS_SEND_TO_BOOTH_WHEN_DEAD RETURN "MC_H_GAM_BTH1" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_GAME_MASTERS_TEAM_1Rule_WHEN_TEAM_1_FAIL RETURN "MC_H_GAM_TM1A" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_GAME_MASTERS_TEAM_2Rule_WHEN_TEAM_1_FAIL RETURN "MC_H_GAM_TM2A" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_GAME_MASTERS_TEAM_1Rule_WHEN_TEAM_2_FAIL RETURN "MC_H_GAM_TM1B" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_GAME_MASTERS_TEAM_2Rule_WHEN_TEAM_2_FAIL RETURN "MC_H_GAM_TM2B" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_GAME_MASTERS_PLAYING_START_TEAM RETURN "MC_H_GAM_BTH2" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_GAME_MASTERS_ROLE_RESTARTS RETURN "MC_H_GAM_RSTR" ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_MANIPULATE_TIME_REMAINING_OPTIONS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_TIMEM_GOTO_INCREMENT RETURN "MC_H_MTR_IL" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_TIMEM_INCREMENT_AMOUNT RETURN "MC_H_MTR_AT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_TIMEM_TEAM_ONLY RETURN "MC_H_MTR_TO" ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_DISABLED_PLAYER_OPTIONS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_DISABLE_INTERACTION_GAMEPLAY RETURN "FM_H_DGAO" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_DISABLE_INTERACTION_ARMOUR RETURN "FM_H_DIMA" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_DISABLE_HEADLIGHT_CONTROL RETURN "MC_H_DHLCR" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_ENABLE_DRONE_DEPLOYMENT RETURN "MC_H_DPDRO" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_ENABLE_DRONE_DEPLOYMENT_AFTER_BRANCH RETURN "MC_H_DPDROB" ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_CORONA_OPTIONS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_DISABLE_CORONA_REST RETURN "MC_H_LKW_3" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_PSEUDORACE_START_CORONA RETURN "MC_H_PSU_RCE" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_PLAYER_NAME_NOT_CREW_NAME RETURN "MC_H_PNNCN" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_DISGUISE_TEAM_SELECTION RETURN "MC_H_TPC" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_SELECTABLE_TARGET_SCORES RETURN "MC_H_SLTS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_SELECTABLE_ROUND_LENGTH RETURN "MC_H_CCRL" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_MAXIMUM_TEAM_DIFFERENCE RETURN "MC_H_CMTD" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_CORONA_PERSONAL_VEHICLES RETURN "MC_H_PVA" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_CORONA_GUNSMITH_OVERRIDE RETURN "FMMC_H_GLOS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_CORONA_LAPS_OPTION RETURN "FMMC_H_ANLO" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_CORONA_AUTO_NUMBER_OF_TEAMS RETURN "FMMC_H_ANT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_CORONA_POINTS_INSTEAD_OF_KILLS RETURN "FMMC_H_PIK" ENDIF
	ENDIF
		
	IF sFMMCMenu.sActiveMenu = eFmmc_OBJ_ELECTRONIC_OPTIONS
		RETURN GET_ELECTRONIC_OPTIONS_HELPTEXT(sFMMCmenu, FMMC_ELECTRONIC_ENTITY_TYPE__OBJECT)
	ELIF sFMMCMenu.sActiveMenu = eFmmc_DYNOPROP_ELECTRONIC_OPTIONS
		RETURN GET_ELECTRONIC_OPTIONS_HELPTEXT(sFMMCmenu, FMMC_ELECTRONIC_ENTITY_TYPE__DYNOPROP)
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_MISSION_CONTINUITY_OPTIONS
		RETURN GET_MISSION_CONTINUITY_DESCRIPTIONS(sFMMCmenu)
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_MISSION_CONTINUITY_DELETE_OPTIONS
		RETURN GET_MISSION_CONTINUITY_DELETE_OPTIONS_DESCRIPTIONS(sFMMCmenu)
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_MISSION_CONTINUITY_SPAWN_GROUP_OVERRIDE_OPTIONS
		RETURN GET_MISSION_CONTINUITY_SPAWN_GROUP_OVERRIDE_OPTIONS_DESCRIPTIONS(sFMMCmenu)
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_MISSION_CONTINUITY_SPAWN_GROUPS_OPTIONS
		RETURN GET_MISSION_CONTINUITY_SPAWN_GROUPS_OPTIONS_DESCRIPTIONS()
	ENDIF
	
	STRING sDescriptionText = GET_HELP_TEXT_FOR_MISSION_VARIATION_OPTIONS_ALL(sFMMCmenu)
	IF NOT IS_STRING_NULL_OR_EMPTY(sDescriptionText)
		RETURN sDescriptionText
	ENDIF
	
	sDescriptionText = GET_HELP_TEXT_FOR_ALT_VAR_OPTIONS_ALL(sFMMCmenu)
	IF NOT IS_STRING_NULL_OR_EMPTY(sDescriptionText)
		RETURN sDescriptionText
	ENDIF
		
	IF sFMMCMenu.sActiveMenu = eFmmc_CoronaModOptions
		RETURN GET_DISABLE_CORONA_MOD_DESCRIPTIONS(sFMMCmenu)
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_HEIST_OPTIONS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_EARN_POINTS RETURN "FMMC_MH_D38" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_BONUS_CASH_REWARDS RETURN "MC_H_BCR" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_SPAWN_IN_COVER RETURN "MC_H_MD_SCP" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_UNARMED_ANIM_ON_CHECKPOINT RETURN "MC_H_MD_UACP0" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_DELAY_NEXT_MISSION_PRELOAD RETURN "MC_H_MD_DMP" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_CASH_TAKE_PERSISTS RETURN "MC_H_MD_CTT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_ALLOW_MASK_AFTER_TEAM_AGGRO RETURN "MC_H_MD_MAA" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_FADE_OUT_AND_WARP_BETWEEN_TWO_MISSION_TRANSITIONS RETURN "MC_H_MD_WARP" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_ALLOW_REBREATHERS_IN_NON_HEIST RETURN "" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_NUMBER_OF_REBREATHERS RETURN "" ENDIF 
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_VEHICLE_OPTIONS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_PERSONAL_VEH_OPTIONS RETURN "MC_D_PER_VEH" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_FORCE_START_VEHICLE RETURN "MC_H_MD_FSV" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_ALLOW_HYDRO_DURING_COUNTDOWN RETURN "MC_H_RA_AHC" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_VEHICLE_EXPLOSION_PROOF RETURN "FMMC_H_VEP" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_MODDED_CORONA_VEHS RETURN "FMMC_H_MCV" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_DELETE_ON_STUCK RETURN "FMMC_H_DVOS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_VEHICLE_RESPAWN_POOL RETURN "FMMC_H_VRP" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_COUNTERMEASURE_UI RETURN "MC_H_CMUI" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_BOMBUSHKA_TURRETS_IN_MISSION RETURN "FMMC_H_ABTU"  ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_MINI_ROUNDS_WAIT_FOR_VEH_RESPAWN RETURN "MC_H_MRVRS"  ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_STUNT_OPTIONS RETURN "MC_H_VO_SO"  ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_EXTRA_VEHICLE_COLLISION_DAMAGE RETURN "MC_H_EECDFV"  ENDIF 
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_NO_VEHICLE_EXPLOSION_DAMAGE RETURN "FMMC_H_ERV"  ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_MONSTER_INCREASED_CRUSH_DAMAGE RETURN "FMMC_H_MICD"  ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_SHOW_FIRSTPERSON_VEHICLE_HEALTHBAR RETURN "FMMC_H_FPVHB"  ENDIF 
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_ENABLE_VEHICLE_AMMO RETURN "FMMC_H_EAVA"  ENDIF		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_DISABLE_HIGH_EXPLOSION_DMG_1_SHOT RETURN "MC_H_PPROF_HEX"  ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_USE_WEAZEL_NEWS_CAM_FILTER RETURN "MC_H_CAM_FILT"  ENDIF		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_USE_BIG_EXPLOSIONS RETURN "MC_H_BIG_EXP"  ENDIF	
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_BLOCK_MOD_SHOP_OPTIONS RETURN "MC_H_BLK_MOD"  ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_VEH_SMASH_CAR RETURN "FMMC_H_SCO" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_VEH_ONLY_DELETE_ALIVE_EMPTY_VEHS RETURN "FMMC_H_DAEV" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_VEH_DONT_CLEAR_LAST_VEH_INFO RETURN "MC_H_DCLVI" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_VEH_ALLOW_PASSENGER_BOMBING RETURN "MC_H_ALPSBM" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_VEH_RETAIN_WEAPON_ON_SPAWN RETURN "MC_H_RVWOR" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_VEH_AUTO_BOMB_DOORS RETURN "MC_H_VABBD" ENDIF
	ENDIF

	IF sFMMCMenu.sActiveMenu = eFmmc_OBJECT_OPTIONS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_DONT_REPEAT_SPAWN_GROUP RETURN "MC_H_OBJ_DRSG" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_DONT_REPEAT_SPAWN_SUB_GROUP RETURN "MC_H_OBJ_DRSSG" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_ANY_MANUAL_RESPAWN_DROP_PACKAGES RETURN "MC_H_OBJ_DPOFR" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_OBJECT_CONTROLLED_VEHICLE_SWAPS RETURN "MC_H_OBJ_EOCVS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_OBJECT_CONTROLLED_VEHICLE_SWAPS_CD RETURN "MC_H_OBJ_OSCTT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_OBJECT_SCREEN_FILTER_ON_HOLDING_FLAG RETURN "CTF_H_CTL_VFX" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_OBJECT_FLAG_SMOKE_REQUIRES_MOVEMENT RETURN "MC_H_CSTRM" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_OBJECT_SHOW_CCTV_LIGHT_CONES RETURN "MC_H_ULCOCC" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_OBJECT_RESPECT_CCTV_SPOTTED_DELAY_TIMER RETURN "MC_H_R5SCT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_OBJECT_CCTV_SPOTTED_DELAY_TIMER_LENGTH RETURN "MC_H_RSSCT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_OBJECT_CCTV_ALERT_SOUNDS RETURN "MC_H_SPTALRT" ENDIF		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_OBJECT_TRIGGER_ALL_CCTV_CAMS RETURN "MC_H_SPTALLL" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_OBJECT_CCTV_TRIGGER_AGGRO_WITHOUT_PEDS RETURN "MC_H_CCTV_GA" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_OBJECT_CCTV_DESTROY_AGGRO_LIMIT RETURN "MC_H_CCTV_AOD" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_OBJECT_CASH_GRAB_SLOW_LIGHT RETURN "MC_H_CG_LGS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_OBJECT_CASH_GRAB_SLOW_HEAVY RETURN "MC_H_CG_HGS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_OBJECT_CASH_GRAB_SLOW_TRIGGER_NUM RETURN "MC_H_CG_SLT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_OBJECT_CASH_GRAB_LIMITED_BAG_SIZE RETURN "MC_H_CG_LB" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_OBJECT_CASH_GRAB_MINIMUM_HIT_LOSS RETURN "MC_H_CG_HLMI" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_OBJECT_CASH_GRAB_MAXIMUM_HIT_LOSS RETURN "MC_H_CG_HLMA" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_OBJECT_ENABLE_OBJ_STEALING RETURN "MC_H_EOSB" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_OBJECT_ENABLE_OBJ_RETURNING RETURN "MC_H_OERO" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_OBJECT_KEEP_OBJ_IN_DROP_OFF RETURN "MC_H_KOID" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_OBJECT_FLASH_BLIP_WHEN_CARRYING RETURN "MC_H_FBWC" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_OBJECT_FADE_BLIP_ON_HACK RETURN "FMMC_FBWH" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_OBJECT_SHOW_BLOCK_COLLECTING_HELP_TEXT RETURN "MC_H_BOFCH" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_OBJECT_DRAW_CCTV_LIGHTS RETURN "MC_H_KOID" ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_PROP_OPTIONS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_PROP_COLOUR_CHANGE_DELAY RETURN "MC_H_PRP_CLDL" ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_LOCATION_OPTIONS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_LOCATION_EVERY_FRAME_CAPTURE_CHECK 	RETURN "" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_LOCATION_DONT_REMOVE_LOCATES 			RETURN "MC_H_DRL" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_LOCATION_ALL_TEAMS_SHARE_LOCATES 		RETURN "MC_H_AGTLS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_LOCATION_REACTIVATE_LOCATES 			RETURN "MC_H_RAL" ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_LEADERBOARD_OPTIONS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_LO_LEADERBOARD_STYLE RETURN "MC_H_LDBS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_LO_HIDE_LEADERBOARD_KILLS_AND_DEATHS RETURN "MC_H_TD_HKD" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_LO_SHOW_ASSISTS_ON_LEADERBOARD RETURN "MC_H_TD_SAOL" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_LO_NO_LEADERBOARD_SCORE_OR_KILLS RETURN "MC_H_TD_NSK" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_LO_SHOW_SCORE_COLUMN2 RETURN "MC_H_MD_LBS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_POINTS_TITLE RETURN "MC_H_MD_PT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_LO_IGNORE_DEATH_TEAM_SORTING RETURN "MC_H_MD_IDTS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_LO_INDIVIDUAL_CAPTURE_TIME RETURN "MC_H_MD_LBCT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_LO_INCLUDE_CAPTURE_PROGRESS RETURN "MC_H_MD_ICPS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_LO_SHOW_TIME_INSTEAD_OF_SCORE RETURN "MC_H_MD_LBSTC" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_LO_DPD_SHOW_HEADSHOTS 		RETURN "MC_H_TW_SHTAT" ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_MODE_SPECIFIC_OPTIONS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_HEIST_OPTIONS RETURN "MC_H_HEISTS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_PRON_MODE_OPTIONS RETURN "MC_H_PRON" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_RAGE_PICKUP_TIMERS RETURN "MC_H_RPUT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_RUGBY_SETTINGS RETURN "FMMC_RS_H" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_CASINO_HEIST_OPTIONS RETURN "MC_H_CHO" ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_AUDIO_OPTIONS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_AO_DRONE_SOUNDS_RULE RETURN "FMMC_DSOURD" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_AO_KEEP_RADIO_ON_RESPAWN RETURN "MC_H_KRSR" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_AO_USE_BLACKOPS_VOICE RETURN "MC_H_AO_UBV" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_AO_LOCAL_CASH_GRAB RETURN "MC_H_AO_LCGSO" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_AO_CASINO_HEIST_AUDIO_BANK RETURN "MC_H_AO_CASHH" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_AO_AMBIENT_CASINO_FLOOR_ZONE RETURN "MC_H_AO_ACFS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_AO_ENABLE_HEADSET_BEEP RETURN "MC_H_AO_ENHB" ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_ADV_MODE_AUDIO_OPTIONS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_AAO_ENABLE_CONDEMNED RETURN "MC_H_CNDDSOU" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_AAO_ENABLE_HOSTILE_TAKEOVER RETURN "MC_H_HSSTSOU" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_AAO_ENABLE_STOCKPILE RETURN "MC_H_STKSOU" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_AAO_ENABLE_TRAP_DOOR_SOUNDS RETURN "MC_H_TRPDR" ENDIF	
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_AAO_ENABLE_VENETIAN_JOB_SOUNDS RETURN "MC_H_VJS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_AAO_ENABLE_TRADING_PLACES_REMIX_SOUNDS RETURN "MC_H_TPRS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_AAO_ENABLE_RUNNING_BACK_REMIX_SOUNDS RETURN "MC_H_RNBKSOU" ENDIF	
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_AAO_ENABLE_COME_OUT_TO_PLAY_REMIX_SOUNDS RETURN "MC_H_COTPSOU" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_AAO_ENABLE_HUNTING_PACK_REMIX_SOUNDS RETURN "MC_H_HPRSOU" ENDIF		
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_TEXT_OPTIONS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_TRIP_SKIP_HELP_DIALOGUE RETURN "MC_H_MD_TSHD" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_SHOW_LARGER_OUT_OF_BOUNDS_SHARD RETURN "MC_H_SL_OOBS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_V_DAY_TICKERS RETURN "MC_H_VDTK" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_BLOCK_SPECTATOR_TICKER RETURN "MC_H_BST" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_USE_IN_AND_OUT_HELPTEXT RETURN "MC_H_UIOH" ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_HUD_OPTIONS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_POINTS_HUD RETURN "MC_H_MD5" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_USE_FOOTBALL_HUD RETURN "MC_H_TD_VFH" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_SHOW_PACKAGE_HUD_FOR_TEAM RETURN "MC_H_SDPH" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_DPAD_KILL_BREAKDOWN RETURN "MC_H_DPKB" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_SHOW_PLAYERS_USING_TRACKIFY RETURN "MC_H_SPUT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_BLIP_PLAYERS_WHO_ARE_BEASTS RETURN "FMMC_BPWA" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_USE_SMALL_WEP_BLIPS RETURN "MC_H_USWB" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_DONT_DISPLAY_TEAM_LIVES RETURN "FMMC_H_DDTL"		 ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_DONT_FLASH_BLIP_ON_PLAYERS RETURN "FMMC_H_DFPB" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_HIDE_RP_BAR RETURN "MC_H_HRPB" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_USE_SLIPSTREAM_HUD  RETURN "MC_H_USH" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_PRIORITISE_TEAM_BLIPS RETURN "MC_H_PYTB" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_SHOW_ALL_PLAYERS_HEALTH_BARS_ON_TEAM RETURN "MC_H_SAHB" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_SHOW_VEHICLE_HEALTH_BARS_INSTEAD RETURN "MC_H_SAHV" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_HIDE_VERSUS_TEAM_SHARD RETURN "MC_H_HUD_HVTS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_USE_POWER_MAD_HUD RETURN "MC_H_HUD_PM" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_FORCE_TARGET_SCORE_HUD RETURN "MC_H_HUD_FTS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_NEVER_SHOW_CAPTURE_BARS_EVER RETURN "MC_H_HUD_NSBC" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_ONLY_SHOW_CAPTURE_BAR_IN_AREA RETURN "MC_H_HUD_OSCBIA" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_SHARD_ON_OBJECT_STOLEN RETURN "MC_H_HUD_OSS"  ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_CIRCLE_HUD_MENU RETURN "MC_H_HUD_CHM"  ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_USE_TEAM_COLOUR_ON_WASTED_SHARD RETURN "MC_H_HUD_ATTE" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_SHOW_ATTEMPTS_INSTEAD_OF_LAPS RETURN "" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_ENABLE_VEHICLE_TARGETTING_RETICULE RETURN "FMMC_HUD_RETT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_KEEP_STEALTH_VEHICLE_BLIPS RETURN "MC_H_HUD_SSVB" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_DISABLE_RED_PLATFORM_SHARD RETURN "MC_H_HUD_DRPS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_ONE_SHOT_BOUNDS_SHARD RETURN "MC_H_HUD_OSBS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_SHOW_BOUNDS_TIMER_IN_SECONDARY_BOUNDS RETURN "FMMC_H_SBTIB" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_SHOW_DPAD_DOWN_IN_HEIST_SPECTATE RETURN "FMMC_H_SDDHS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_PREFIX_TEAM_NAMES_IN_LAP_COUNTER RETURN "MC_H_STNLC" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_DONT_DISPLAY_TEAM_LIVES RETURN "MC_H_AUTN" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_OFFSET_LAPS_BY_RULE_T2_DEC RETURN "MC_H_OTABR_1" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_OFFSET_LAPS_BY_RULE_T1_INC RETURN "MC_H_OTABR_2" ENDIF		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_OVERRIDE_PAUSE_CAM_DISTANCE RETURN "MC_H_ODFCPC" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_HUD_VEHICLE_BLIP_REMOVAL_CHECKS RETURN "MC_H_DVBRC" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_HUD_SHOW_ALL_MULTIRULE_TIMERS RETURN "MC_H_HUD_SAMR" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_HUD_USE_FLOATING_SCORES RETURN "MC_H_HUD_UFS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_HUD_USE_BIKER_ARROW_FOR_OVERHEAD_CARRY RETURN "MC_H_UBA_CAP" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_HUD_CAPTURED_FLAG_SHARD RETURN "MC_H_HUD_SCFS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_HUD_SHOW_HUD_CASH_GRAB RETURN "MC_HUD_SHCT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_HUD_DISABLE_NIGHTVISION_TOGGLE_IN_VEH RETURN "MC_HUD_DNTV" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_HUD_CLEANUP_AI_BLIPS_ALL_PEDS RETURN "MC_H_PED_CABM" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_HUD_BLOCK_DEATH_TICKERS RETURN "MC_H_HUD_BDET" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_HUD_HIDE_RESPAWN_BAR RETURN "MC_H_HUD_HDRS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_HUD_CUSTOM_HUD_TEXT_CAPTURE_BAR RETURN "MC_H_HUD_CHTC" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_HUD_ALLOW_HUD_TOGGLE RETURN "MC_H_HUD_AHT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_HUD_BLOCK_HINT_CAM RETURN "MC_H_HUD_BHC" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_HUD_BLOCK_QUICK_GPS RETURN "MC_H_HUD_BQG" ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFMMC_CIRCLE_HUD_OPTIONS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_CIRCLE_SCORE_HUD RETURN "MC_H_CHUD_CSH" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_CIRCLE_TIMER_NO_SCORES RETURN "MC_H_CHUD_CTH" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_CIRCLE_IGNORE_MULTIRULE RETURN "MC_H_CHUD_IMRT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_CIRCLE_SCORE_HUD_WITH_TIMER RETURN "MC_H_CHUD_SHWT" ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_CARNAGE_BAR_OPTIONS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_CARNAGE_BAR_RULE_PASS_WHEN_CARNAGE_FULL	RETURN "H_CRNBAR_00" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_CARNAGE_BAR_RULE_PASS_WHEN_HELI_FULL		RETURN "H_CRNBAR_01" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_CARNAGE_BAR_HELI_DRAIN_RATE				RETURN "H_CRNBAR_02" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_CARNAGE_BAR_HELI_POSITIVE_THRESH			RETURN "H_CRNBAR_03" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_CARNAGE_BAR_HELI_FILL_RATE				RETURN "H_CRNBAR_04" ENDIF		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_CARNAGE_BAR_CAR_DRAIN_RATE				RETURN "H_CRNBAR_05" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_CARNAGE_BAR_CAR_SPEED_THRESHOLD			RETURN "H_CRNBAR_06" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_CARNAGE_BAR_CAR_SPEED_FILL_RATE			RETURN "H_CRNBAR_07" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_CARNAGE_BAR_CAR_PED_FILL_RATE				RETURN "H_CRNBAR_08" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_CARNAGE_BAR_CAR_VEH_FILL_RATE				RETURN "H_CRNBAR_09" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_CARNAGE_BAR_CAR_STUNT_FILL_RATE			RETURN "H_CRNBAR_10" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_CARNAGE_BAR_ALLOW_PERSONAL_VEHICLES		RETURN "H_CRNBAR_11" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_CARNAGE_BAR_ANY_VEHICLES					RETURN "H_CRNBAR_12" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_CARNAGE_USE_HELI_CAM						RETURN "H_CRNBAR_13" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_CARNAGE_VEHICLE_TARGET_1					RETURN "H_CRNBAR_14 "ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_CARNAGE_VEHICLE_TARGET_2					RETURN "H_CRNBAR_15" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_CARNAGE_VEHICLE_TARGET_3					RETURN "H_CRNBAR_16" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_CARNAGE_VEHICLE_TARGET_4					RETURN "H_CRNBAR_17" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_CARNAGE_TOTAL_CARNAGE						RETURN "H_CRNBAR_18" ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFMMC_HWN_OPTIONS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_HWN_HEARTBEAT_VIBR_ENABLED RETURN "MC_D_HWN_HVE" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_HWN_HEARTBEAT_DISTANCE RETURN "MC_D_HWN_HD" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_HWN_HEARTBEAT_MIN_DIST RETURN "MC_D_HWN_HMD" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_HWN_FRIENDLY_VIBR_ENABLED RETURN "MC_D_HWN_FVE" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_HWN_FRIENDLY_DISTANCE RETURN "MC_D_HWN_FD" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_HWN_FRIENDLY_COOLDOWN RETURN "MC_D_HWN_FC" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_HWN_ENABLE_BOTH_TEAMS RETURN "MC_D_HWN_BT" ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_AMBIENT_MENU	
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_AM_TIME RETURN "MC_H_MD15" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_AM_WEATHER RETURN "DMC_H_WETH" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_AM_SERVICES  RETURN "MC_H_SRV" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_AM_TRAFFIC RETURN "MC_H_MD17" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_AM_PEDS RETURN "MC_H_MD18" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_AM_MUSIC RETURN "MC_H_MD16" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_AM_RADIO RETURN "FMMC_HELP_RAD" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_AM_GANGS RETURN "MC_H_MD21" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_AM_BLOCK_ON_FOOT_DISPATCH RETURN "MC_H_DOFD" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_AM_WANTED_MAX RETURN "MC_H_POL" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_AM_WANTED_CLEAR RETURN "FMMC_MH_D40" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_AM_LOCK_TOD_FIRST_PLAY_ONLY RETURN "MC_H_T0_TLFP" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_AM_LOCK_WEATHER_FIRST_PLAY_ONLY RETURN "MC_H_T0_WLFP" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_AM_PRISON_ALARMS RETURN "FMMC_H0_TRIG_AL" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_AM_PROGRESS_TOD RETURN "MC_H_T0_PTOD" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_AM_DISABLE_TRAINS_AND_TRAMS RETURN "MC_H_MD_DTT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_AM_LOUD_PLANE_NOISES RETURN "MC_H_MD_LPS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_AM_DISABLE_MOD_SHOP  RETURN "MC_H_DI_MOD" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_AM_TURN_OFF_SCRIPT_VEHICLE_SPAWNS  RETURN "FMMC_TOVS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_AM_TURN_OFF_PRISON_AMBIENCE RETURN "FMMC_TOPY" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_DISABLE_PLAYER_FACILITY_PV_SPAWN_NODES RETURN "MC_H_DPVOP" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_CLEAN_UP_LEAVERS_ON_MISSION RETURN "MC_H_CULFM" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_BLOCK_ARMY_BASE RETURN "MC_H_BAB" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_DISABLE_WARP_OUT_OF_GARAGE RETURN "MC_H_DAWFG" ENDIF		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_ENABLE_WEAPONS_IN_FREEMODE_SHOPS RETURN "MC_H_EWIFS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_AM_ENABLE_MOD_SHOP RETURN "MC_H_ENMOSH" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_AM_GLOBAL_FIRE_SPREADING_INTENSITY RETURN "MC_H_FIRSPR" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_AM_ALLOW_CASINO_VALET_SCENARIOS RETURN "MC_H_ACVS" ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_ROAMING_SPECTATOR_OPTIONS
		RETURN GET_ROAMING_SPECTATOR_DESCRIPTIONS(sFMMCmenu)
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_SYNOPSIS
		RETURN "FMMC_T0_M42H"
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_RADAR_MENU	
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_RDR_ZOOM RETURN "FMMC_RDR_H0" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_RDR_BLIP_PICKUP RETURN "FMMC_RDR_H1" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_RDR_BLIP_SPRINT RETURN "FMMC_RDR_H2" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_RDR_ALLOW_BLIP_DISABLING RETURN "MC_H_MD_ABD" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_RDR_USE_ALT_PLAYER_BLIPS_COLOURS RETURN "MC_H_UA_PBC" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_RDR_BLIP_AFTER_ENTERING_VEHICLE RETURN "MC_H_BAEV" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_RDR_BLOCK_FREEMODE_MOC_BLIP RETURN "MC_H_DMOCB" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_RDR_HIDE_PV_BLIP_IN_INTERIORS RETURN "MC_H_HPVBI"	 ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_RDR_HIDE_INTERIOR_PED_BLIPS_IN_CANNON RETURN "MC_H_HIPBIC"	 ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_RDR_EVERY_FRAME_INVISIBLE_PED_BLIPS RETURN "MC_H_EFIPB"	 ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_RDR_PROCESS_VEH_BLIPS_IN_PREGAME RETURN "MC_H_PVBPG"	 ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_RDR_DISABLE_HEIGHT_ON_PLAYER_BLIPS RETURN "MC_H_SHBOP"	 ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_RDR_FORCE_GPS_TO_NEAREST_MOD_SHOP RETURN "MC_H_FGMS"	 ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_PHOTO_CAM_BASE
		RETURN "DMC_H_40"
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_TEST_BASE
		RETURN "DMC_H_44P"
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_TOP_MENU
	
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TOP_MENU_OPTIONS RETURN "DMC_H_23" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TOP_MENU_CREATOR RETURN "DMC_H_24" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TOP_MENU_RADIO RETURN "DMC_H_20" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TOP_MENU_EXIT RETURN "DMC_H_25" ENDIF
		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TOP_MENU_TEST
			IF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_TRIGGER)
				RETURN "DMC_H_30"
			ELIF NOT IS_BIT_SET(sFMMCmenu.iTopMenuActive, TOP_MENU_TEST)
				IF IS_BIT_SET(iTestFailBitSet, ciFAIL_BIT_SET_NO_DOFF_BUT_NEEDED_TEAM_1)
				OR IS_BIT_SET(iTestFailBitSet, ciFAIL_BIT_SET_NO_DOFF_BUT_NEEDED_TEAM_2)
				OR IS_BIT_SET(iTestFailBitSet, ciFAIL_BIT_SET_NO_DOFF_BUT_NEEDED_TEAM_3)
				OR IS_BIT_SET(iTestFailBitSet, ciFAIL_BIT_SET_NO_DOFF_BUT_NEEDED_TEAM_4)
					RETURN "FMMC_ER_NODRPOF"
				ELSE
					RETURN "FMMC_ER_005"
				ENDIF
			ELSE
				IF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_UNTESTED)
					RETURN "DMC_H_41"
				ELSE
					RETURN "DMC_H_17"
				ENDIF
			ENDIF
		ENDIF
		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TOP_MENU_SAVE
			IF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_NO_CLOUD)
				IF bSignedOut
					RETURN GET_DISCONNECT_HELP_MESSAGE()
				ELSE
					RETURN "FMMCNO_CLOUD"
				ENDIF
			ELIF ARE_ANY_RULES_EMPTY(sCurrentVarsStruct.iSaveFailTeam, sCurrentVarsStruct.iSaveFailRule)
				RETURN "FMMC_ER_NOENTS"
			ELSE
				IF g_FMMC_STRUCT.bMissionIsPublished
					RETURN "DMC_H_29C"
				ELSE
					IF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_TITLE)
						RETURN "DMC_H_38B"
					ELSE
						IF IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciMISSION_OPTION_BS_MISSION_IS_NOT_PLAYABLE)
							RETURN "DMC_H_38"
						ELSE
							RETURN "DMC_H_28"
						ENDIF	
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = TOP_MENU_PUBLISH
			IF IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_NO_CLOUD)
				IF bSignedOut
					RETURN GET_DISCONNECT_HELP_MESSAGE()
				ELSE
					RETURN "FMMCNO_CLOUD"
				ENDIF
			ELIF ARE_ANY_RULES_EMPTY(sCurrentVarsStruct.iSaveFailTeam, sCurrentVarsStruct.iSaveFailRule)
				RETURN "FMMC_ER_NOENTS"
			ELSE
				IF NOT IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_CHEATER)
					IF NOT IS_BIT_SET(sFMMCmenu.iTopMenuActive, TOP_MENU_PUBLISH)
						RETURN "DMC_H_38C"
					ELSE
						IF g_FMMC_STRUCT.bMissionIsPublished
							RETURN "DMC_H_29B"
						ELSE						
							RETURN "DMC_H_29"
						ENDIF
					ENDIF
				ELSE
					RETURN "DMC_H_38A"
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF
		
	IF sFMMCMenu.sActiveMenu = eFmmc_RAGE_PICKUP_TIMERS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_RATC_CUSTOM_RANDOM RETURN "MC_H_RPUT8" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_RATC_SWAP_TYPE RETURN "MC_H_RATCSW" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_RATC_ASSETS RETURN "MC_H_RATCPPA" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_RATC_BEAST_COLOUR RETURN "MC_H_RATCBEC" ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_MISSION_CRITICAL_MENU
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciMISSION_CRITICAL_FAILS_AFTER_RESPAWNS
			RETURN "MC_H_CRIT_RSP"
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ciMISSION_CRITICAL_WHEN_VEH_DESTROYED
			RETURN "MC_H_CRIT_CVD"
		ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_OBJ_CCTV_CAM_OPTIONS
	OR sFMMCMenu.sActiveMenu = eFmmc_DYNOPROP_CCTV_CAM_OPTIONS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = CCTV_ENABLE RETURN "MC_H_CCTV_E" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = CCTV_TURN_AMOUNT RETURN "MC_H_CCTV_TA" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) =  CCTV_TURN_SPEED RETURN "MC_H_CCTV_TS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = CCTV_WAIT_TIME RETURN "MC_H_CCTV_WT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = CCTV_VISION_RANGE RETURN "MC_H_CCTV_RNG" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = CCTV_VISION_WIDTH RETURN "MC_H_CCTV_WDT" ENDIF			
		IF NOT IS_LEGACY_MISSION_CREATOR()
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = CCTV_AGGRO_PEDS RETURN "FMMC_CCTV_PLA" ENDIF
		ELSE
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = CCTV_AGGRO_PEDS RETURN "MC_H_CCTV_CA" ENDIF
		ENDIF		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = CCTV_TRIGGER_FIND_BODY RETURN "MC_H_CCTV_TFB" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = CCTV_ACTIVATE_SIREN RETURN "MC_H_CCTV_AS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = CCTV_SKIP_TO_RULE RETURN "MC_H_CCTV_STR" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = CCTV_STOP_MOVING_WHEN_SPOTTING RETURN "MC_H_CCTV_SMWS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = CCTV_SHOW_HELPTEXT_FROM_RULE RETURN "MC_H_CCTV_SHT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = CCTV_BLOCK_HELPTEXT_FROM_RULE RETURN "MC_H_CCTV_BHT" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = CCTV_USE_ENTITY_LINE_OF_SIGHT_CHECKS RETURN "MC_H_CCTV_LOS" ENDIF
		
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFMMC_QRC_SPAWN_POINTS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = QRC_SPAWN_POINT_MENU_JUST_ONE_SPAWN_GROUP
			RETURN "MC_H_QRC_OSG"
		ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFMMC_DEFINE_SPAWN_GROUP_ACTIVATION
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DEFINED_SPAWN_GROUP_SETTINGS_ENABLE	RETURN "MC_H_DEFS_EN" ENDIF
		
		INT i
		REPEAT FMMC_MAX_RANDOM_ENTITY_SPAWN_GROUPS i
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DEFINED_SPAWN_GROUP_SETTINGS_START+i	
				RETURN GET_SPAWN_GROUP_DEFINED_ACTIVATION_DESCRIPTION_TEXT(g_FMMC_STRUCT.eRandomEntitySpawn_DefineActivation[i])
			ENDIF
		ENDREPEAT
	ENDIF
	IF sFMMCMenu.sActiveMenu = eFMMC_DEFINE_SUB_SPAWN_GROUP_ACTIVATION		
		INT i
		REPEAT ciMAX_NUM_FMMC_ENTITY_SUB_SPAWN_GROUPS i
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DEFINED_SUB_SPAWN_GROUP_SETTINGS_START+i	
				RETURN GET_SPAWN_GROUP_DEFINED_ACTIVATION_DESCRIPTION_TEXT(g_FMMC_STRUCT.eRandomEntitySpawnSub_DefineActivation[sFMMCMenu.iSpawnGroupIndex][i])
			ENDIF			
		ENDREPEAT
	ENDIF	
	
	IF sFMMCMenu.sActiveMenu = eFMMC_RANDOMISED_SPAWN_GROUPS
	
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = RANDOMISED_SPAWN_GROUPS_NUMBER_OF_SPAWN_GROUPS RETURN "MC_H_RSG_GTS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = RANDOMISED_SPAWN_GROUPS_GROUPS_TO_SPAWN RETURN "MC_H_RSG_GTS" ENDIF
		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = RANDOMISED_SPAWN_GROUPS_INCLUDE_TEAM_1_PLAYERS
		OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = RANDOMISED_SPAWN_GROUPS_INCLUDE_TEAM_2_PLAYERS
		OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = RANDOMISED_SPAWN_GROUPS_INCLUDE_TEAM_3_PLAYERS
		OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = RANDOMISED_SPAWN_GROUPS_INCLUDE_TEAM_4_PLAYERS
			RETURN "MC_H_RSG_ITP"
		ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = RANDOMISED_SPAWN_GROUPS_DEFINE_GROUPS_TO_SPAWN RETURN "MC_H_RSG_DEFS" ENDIF
		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = RANDOMISED_SPAWN_GROUPS_MAINTAIN_OVER_QUICK_RESTARTS RETURN "MC_H_MG_OR" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = RANDOMISED_SPAWN_GROUPS_RANDOMISED_SPAWN_SUBGROUPS RETURN "MC_H_RSG_RSSG" ENDIF		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = RANDOMISED_SPAWN_GROUPS_MAINTAIN_OVER_STRAND_SAVE_ONLY RETURN "MC_H_RSG_STRS" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = RANDOMISED_SPAWN_GROUPS_MAINTAIN_OVER_STRAND_LOAD_ONLY RETURN "MC_H_RSG_STRL" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = RANDOMISED_SPAWN_GROUPS_MAINTAIN_OVER_STRAND_DONT_CLEAR_SAVED RETURN "MC_H_RSG_STRD" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = RANDOMISED_SPAWN_GROUPS_MAINTAIN_OVER_STRAND_ADD_TO_LOADED RETURN "MC_H_RSG_STRDA" ENDIF
		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = RANDOMISED_SPAWN_GROUPS_BUILD_DURING_PLAY
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_BuildSpawnGroupsDuringPlay)
				RETURN "MC_H_RSG_BDP1"
			ELSE
				RETURN "MC_H_RSG_BDP0"
			ENDIF
		ENDIF
		
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFMMC_RANDOMISED_SPAWN_SUBGROUPS
		RETURN "MC_H_SGTS"
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFMMC_SPAWN_SUBGROUPS_IN_GROUP
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = SUBGROUPS_TO_SPAWN_IN_GROUP_NUMBER_OF_SPAWN_SUBGROUPS RETURN "MC_H_NSSG" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = SUBGROUPS_TO_SPAWN_IN_GROUP_SUBGROUPS_TO_SPAWN RETURN "MC_H_SGTS_SGTS" ENDIF
		
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = SUBGROUPS_TO_SPAWN_IN_GROUP_TEAM_1_PLAYERS_INCLUDED
		OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = SUBGROUPS_TO_SPAWN_IN_GROUP_TEAM_2_PLAYERS_INCLUDED
		OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = SUBGROUPS_TO_SPAWN_IN_GROUP_TEAM_3_PLAYERS_INCLUDED
		OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = SUBGROUPS_TO_SPAWN_IN_GROUP_TEAM_4_PLAYERS_INCLUDED
			RETURN "MC_H_RSSG_ITP"
		ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFMMC_SMASH_CAR_OPTIONS
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = SMASH_CAR_OPTIONS_ENABLE RETURN "FMMC_H_SC_E" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = SMASH_CAR_OPTIONS_MAX_H_FORCE RETURN "FMMC_H_SC_HF" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = SMASH_CAR_OPTIONS_MAX_H_PUSH RETURN "FMMC_H_SC_MAXHP" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = SMASH_CAR_OPTIONS_MIN_H_PUSH RETURN "FMMC_H_SC_MINHP" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = SMASH_CAR_OPTIONS_MIN_SPEED_FORWARD RETURN "FMMC_H_SC_MSFF" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = SMASH_CAR_OPTIONS_DROP_OBJECT_ENABLE RETURN "FMMC_H_SC_DOE" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = SMASH_CAR_OPTIONS_STEAL_OBJECT_ENABLE RETURN "FMMC_H_SC_SOE" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = SMASH_CAR_OPTIONS_RETURN_OBJECT_ENABLE RETURN "MC_H_SC_ERO" ENDIF
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_PED_Vehicle
		RETURN SET_UP_PED_VEHICLE_MENU_DESCRIPTIONS(sFMMCmenu)
	ENDIF
	IF sFMMCMenu.sActiveMenu = eFmmc_PED_Spook
		RETURN SET_UP_PED_SPOOK_MENU_DESCRIPTIONS(sFMMCmenu)
	ENDIF
	IF sFMMCMenu.sActiveMenu = eFmmc_PED_Disable
		RETURN SET_UP_PED_DISABLE_MENU_DESCRIPTIONS(sFMMCmenu)
	ENDIF
	IF sFMMCMenu.sActiveMenu = eFmmc_PED_Overheads
		RETURN SET_UP_PED_OVERHEAD_MENU_DESCRIPTIONS(sFMMCmenu)
	ENDIF
	IF sFMMCMenu.sActiveMenu = eFmmc_PED_Cloaking
		RETURN SET_UP_PED_CLOAKING_MENU_DESCRIPTIONS(sFMMCmenu)
	ENDIF
	IF sFMMCMenu.sActiveMenu = eFmmc_NEXT_MISSION_SETTINGS
		RETURN SET_UP_NEXT_MISSION_SETTINGS_MENU_DESCRIPTIONS(sFMMCmenu)
	ENDIF
	IF sFMMCMenu.sActiveMenu = eFmmc_NEXT_MISSION
		RETURN SET_UP_NEXT_MISSION_MENU_DESCRIPTIONS(sFMMCmenu)
	ENDIF
	
	IF sFMMCMenu.sActiveMenu = eFmmc_PLAYER_ABILITY_AIR_STRIKE_OPTIONS		
		IF GET_CREATOR_MENU_SELECTION(sFMMCMenu) = OPTION_MISSION_PA_AS_COOLDOWN_DURATION RETURN "MC_H_PACD" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCMenu) = OPTION_MISSION_PA_AS_NUM_USES RETURN "MC_H_PANU" ENDIF		
		IF GET_CREATOR_MENU_SELECTION(sFMMCMenu) = OPTION_MISSION_PA_AS_HOST_ONLY RETURN "MC_H_PAHO" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCMenu) = OPTION_MISSION_PA_AS_REQUIRED_PREREQ RETURN "MC_H_PARP" ENDIF
	ENDIF
	
	IF sFMMCmenu.sActiveMenu = eFmmc_PLAYER_ABILITY_HEAVY_LOADOUT_OPTIONS		
		IF GET_CREATOR_MENU_SELECTION(sFMMCMenu) = OPTION_MISSION_PA_HL_COOLDOWN_DURATION RETURN "MC_H_PACD" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCMenu) = OPTION_MISSION_PA_HL_NUM_USES RETURN "MC_H_PANU" ENDIF		
		IF GET_CREATOR_MENU_SELECTION(sFMMCMenu) = OPTION_MISSION_PA_HL_HOST_ONLY RETURN "MC_H_PAHO" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCMenu) = OPTION_MISSION_PA_HL_REQUIRED_PREREQ RETURN "MC_H_PARP" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCMenu) = OPTION_MISSION_PA_HL_CONTAINER_INDEX RETURN "MC_H_PAHLCI" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCMenu) = OPTION_MISSION_PA_HL_PICKUP_INDEX_1 RETURN "MC_H_PAHLPI1" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCMenu) = OPTION_MISSION_PA_HL_PICKUP_INDEX_2 RETURN "MC_H_PAHLPI2" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCMenu) = OPTION_MISSION_PA_HL_PICKUP_INDEX_3 RETURN "MC_H_PAHLPI3" ENDIF
	ENDIF
	
	IF sFMMCmenu.sActiveMenu = eFmmc_PLAYER_ABILITY_HELI_BACKUP_OPTIONS		
		IF GET_CREATOR_MENU_SELECTION(sFMMCMenu) = OPTION_MISSION_PA_HB_COOLDOWN_DURATION RETURN "MC_H_PACD" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCMenu) = OPTION_MISSION_PA_HB_NUM_USES RETURN "MC_H_PANU" ENDIF		
		IF GET_CREATOR_MENU_SELECTION(sFMMCMenu) = OPTION_MISSION_PA_HB_HOST_ONLY RETURN "MC_H_PAHO" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCMenu) = OPTION_MISSION_PA_HB_REQUIRED_PREREQ RETURN "MC_H_PARP" ENDIF
	ENDIF
	
	IF sFMMCmenu.sActiveMenu = eFmmc_PLAYER_ABILITY_RECON_DRONE_OPTIONS
		IF GET_CREATOR_MENU_SELECTION(sFMMCMenu) = OPTION_MISSION_PA_RD_ACTIVE_DURATION RETURN "MC_H_PAAD" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCMenu) = OPTION_MISSION_PA_RD_COOLDOWN_DURATION RETURN "MC_H_PACD" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCMenu) = OPTION_MISSION_PA_RD_NUM_USES RETURN "MC_H_PANU" ENDIF		
		IF GET_CREATOR_MENU_SELECTION(sFMMCMenu) = OPTION_MISSION_PA_RD_HOST_ONLY RETURN "MC_H_PAHO" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCMenu) = OPTION_MISSION_PA_RD_REQUIRED_PREREQ RETURN "MC_H_PARP" ENDIF
	ENDIF
	
	IF sFMMCmenu.sActiveMenu = eFmmc_PLAYER_ABILITY_SUPPORT_SNIPER_OPTIONS		
		IF GET_CREATOR_MENU_SELECTION(sFMMCMenu) = OPTION_MISSION_PA_SS_COOLDOWN_DURATION RETURN "MC_H_PACD" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCMenu) = OPTION_MISSION_PA_SS_NUM_USES RETURN "MC_H_PANU" ENDIF		
		IF GET_CREATOR_MENU_SELECTION(sFMMCMenu) = OPTION_MISSION_PA_SS_HOST_ONLY RETURN "MC_H_PAHO" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCMenu) = OPTION_MISSION_PA_SS_REQUIRED_PREREQ RETURN "MC_H_PARP" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCMenu) = OPTION_MISSION_PA_SS_DEPLOY_DURATION RETURN "MC_H_PASSDD" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCMenu) = OPTION_MISSION_PA_SS_EXTRACTION_DURATION RETURN "MC_H_PASSED" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCMenu) = OPTION_MISSION_PA_SS_PLAYER_SPOTTED_PENALTY RETURN "MC_H_PASSPSP" ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCMenu) = OPTION_MISSION_PA_SS_AIM_DURATION RETURN "MC_H_PASSAD" ENDIF
	ENDIF
	
	RETURN ""
	
ENDFUNC

FUNC STRING GET_RIGHT_HAND_RESTART_CHECKPOINTS_LEGACY_MENU_DESCRIPTION()

	SWITCH GET_CURRENT_RHM_SELECTION()
		CASE ciRESTART_CHECKPOINTS_LEGACY_UPDATE_CHECKPOINT_1 			RETURN "MC_H_UPD_SCP" 
		CASE ciRESTART_CHECKPOINTS_LEGACY_UPDATE_CHECKPOINT_2 			RETURN "MC_H_UPD_SCP" 
		CASE ciRESTART_CHECKPOINTS_LEGACY_UPDATE_CHECKPOINT_3 			RETURN "MC_H_UPD_SCP" 
		CASE ciRESTART_CHECKPOINTS_LEGACY_UPDATE_CHECKPOINT_4 			RETURN "MC_H_UPD_SCP" 
		CASE ciRESTART_CHECKPOINTS_LEGACY_UPDATE_CHECKPOINT_ON_MIDPOINT RETURN "MC_H_RL_CPMP" 
		CASE ciRESTART_CHECKPOINTS_LEGACY_MULTI_RULE_TIMER_1 			RETURN "MC_H_MRCP1" 
		CASE ciRESTART_CHECKPOINTS_LEGACY_MULTI_RULE_TIMER_2 			RETURN "MC_H_MRCP2" 
		CASE ciRESTART_CHECKPOINTS_LEGACY_MULTI_RULE_TIMER_3 			RETURN "MC_H_MRCP3" 
		CASE ciRESTART_CHECKPOINTS_LEGACY_MULTI_RULE_TIMER_4 			RETURN "MC_H_MRCP4" 
	ENDSWITCH
	
	RETURN ""
	
ENDFUNC

FUNC STRING GET_RIGHT_HAND_RESTART_CHECKPOINT_SETTINGS_DESCRIPTION()

	SWITCH GET_CURRENT_RHM_SELECTION()
		CASE ciRESTART_CHECKPOINTS_UPDATE_CHECKPOINT_ON_PASS 			RETURN "MC_H_MRC_UP" 
		CASE ciRESTART_CHECKPOINTS_UPDATE_CHECKPOINT_ON_FAIL 			RETURN "MC_H_MRC_UF" 
		CASE ciRESTART_CHECKPOINTS_UPDATE_CHECKPOINT_ON_MIDPOINT 		RETURN "MC_H_MRC_UM" 
		CASE ciRESTART_CHECKPOINTS_UPDATE_CHECKPOINT_MULTIRULE_TIMER	RETURN "MC_H_MRC_UR" 
	ENDSWITCH
	
	RETURN ""
	
ENDFUNC

FUNC STRING GET_RIGHT_HAND_RESTART_CHECKPOINTS_MENU_DESCRIPTION()

	SWITCH GET_CURRENT_RHM_SELECTION()
		CASE ciRESTART_CHECKPOINTS_UPDATE_CHECKPOINT_OPTIONS_0 			RETURN "MC_H_MRC_O0" 
		CASE ciRESTART_CHECKPOINTS_UPDATE_CHECKPOINT_OPTIONS_1 			RETURN "MC_H_MRC_O1" 
		CASE ciRESTART_CHECKPOINTS_UPDATE_CHECKPOINT_OPTIONS_2 			RETURN "MC_H_MRC_O2" 
		CASE ciRESTART_CHECKPOINTS_UPDATE_CHECKPOINT_OPTIONS_3 			RETURN "MC_H_MRC_O3" 
		CASE ciRESTART_CHECKPOINTS_LEGACY_OPTIONS 						RETURN "MC_H_MRC_LEG" 
		CASE ciRESTART_CHECKPOINTS_UPDATE_CHECKPOINT_0_LEG 				RETURN "MC_H_UPD_SCP" 
		CASE ciRESTART_CHECKPOINTS_UPDATE_CHECKPOINT_1_LEG 				RETURN "MC_H_UPD_SCP" 
		CASE ciRESTART_CHECKPOINTS_UPDATE_CHECKPOINT_2_LEG 				RETURN "MC_H_UPD_SCP" 
		CASE ciRESTART_CHECKPOINTS_UPDATE_CHECKPOINT_3_LEG 				RETURN "MC_H_UPD_SCP" 
		CASE ciRESTART_CHECKPOINTS_UPDATE_CHECKPOINT_ON_MIDPOINT_LEG 	RETURN "MC_H_RL_CPMP" 
		CASE ciRESTART_CHECKPOINTS_MULTI_RULE_TIMER_0_LEG 				RETURN "MC_H_MRCP1" 
		CASE ciRESTART_CHECKPOINTS_MULTI_RULE_TIMER_1_LEG 				RETURN "MC_H_MRCP2" 
		CASE ciRESTART_CHECKPOINTS_MULTI_RULE_TIMER_2_LEG 				RETURN "MC_H_MRCP3" 
		CASE ciRESTART_CHECKPOINTS_MULTI_RULE_TIMER_3_LEG 				RETURN "MC_H_MRCP4" 
	ENDSWITCH
	
	RETURN ""
	
ENDFUNC

FUNC STRING GET_RIGHT_HAND_GANG_CHASE_LEGACY_MENU_DESCRIPTION()
	
	SWITCH GET_CURRENT_RHM_SELECTION()
		CASE ciGANG_CHASE_LEGACY_TYPE 									RETURN "MC_H_G0" 
		CASE ciGANG_CHASE_LEGACY_NUM 									RETURN "MC_H_G1" 
		CASE ciGANG_CHASE_LEGACY_MAX_VEH 								RETURN "MC_H_G2" 
		CASE ciGANG_CHASE_LEGACY_NG_NUM 								RETURN "MC_H_GNG_NGI" 
		CASE ciGANG_CHASE_LEGACY_NG_MAX_VEH 							RETURN "MC_H_GNG_NGM" 
		CASE ciGANG_CHASE_LEGACY_LG_NUM 								RETURN "MC_H_GNG_LGI" 
		CASE ciGANG_CHASE_LEGACY_LG_MAX_VEH 							RETURN "MC_H_GNG_LGM" 
		CASE ciGANG_CHASE_LEGACY_COLOUR 								RETURN "MC_H_G3" 
		CASE ciGANG_CHASE_LEGACY_TIME 									RETURN "MC_H_G4" 
		CASE ciGANG_CHASE_LEGACY_DELAY 									RETURN "MC_H_G5" 
		CASE ciGANG_CHASE_LEGACY_ACCURACY 								RETURN "MC_H_GCH_ACC" 
		CASE ciGANG_CHASE_LEGACY_LOSE_CHASERS 							RETURN "MC_H_GCH_LSE" 
		CASE ciGANG_CHASE_LEGACY_VEH_HEALTH 							RETURN "MC_H_GCH_HLT" 
		CASE ciGANG_CHASE_LEGACY_FLEE_DISTANCE 							RETURN "MC_H_GCH_FLD" 
		CASE ciGANG_CHASE_LEGACY_LIMIT_CHASERS 							RETURN "MC_H_GCH_LMC" 
		CASE ciGANG_CHASE_LEGACY_RANDOMISE_WEAPONS		 				RETURN "MC_H_GCH_RWP" 
		CASE ciGANG_CHASE_LEGACY_ALT_OBJECTIVE_TEXT 					RETURN "MC_H_GCH_AOT" 
		CASE ciGANG_CHASE_LEGACY_AREA 									RETURN "MC_H_G6" 
		CASE ciGANG_CHASE_LEGACY_BLIP_FORCED_ON 						RETURN "MC_H_GCH_BFO" 
		CASE ciGANG_CHASE_LEGACY_BLIP_NOTICE_RANGE 						RETURN "MC_H_GCH_BNR" 
		CASE ciGANG_CHASE_LEGACY_GET_COP_BLIPS 							RETURN "MC_H_GCH_CPB" 
		CASE ciGANG_CHASE_LEGACY_PREVENT_COMMANDEERING 					RETURN "MC_H_GCH_PCV" 
		CASE ciGANG_CHASE_LEGACY_BLIP_ONLY_IN_INTERIOR 					RETURN "MC_H_GCH_OBII" 
		CASE ciGANG_CHASE_LEGACY_NO_HOMING 								RETURN "MC_H_GCH_DHW" 
		CASE ciGANG_CHASE_LEGACY_CLEANS_UP_ON_TYPE_CHANGE 				RETURN "MC_H_GCH_CTC" 
		CASE ciGANG_CHASE_LEGACY_TRIGGER_ON_SPEED_FAIL 					RETURN "MC_H_GCH_TOS" 
		CASE ciGANG_CHASE_LEGACY_USE_BURST_FIRING						RETURN "MC_H_GCH_BRSTF" 
		CASE ciGANG_CHASE_LEGACY_PLAYER_VARIABLE_INTENSITY_MULTIPLIER 	RETURN "MC_H_GCH_MULT" 
		CASE ciGANG_CHASE_LEGACY_SOLO_BIKES_ONLY 						RETURN "MC_H_GCH_SBO" 
		CASE ciGANG_CHASE_LEGACY_AGGRO_NEEDED 							RETURN "MC_H_GCH_OIAG" 
		CASE ciGANG_CHASE_LEGACY_PEDS_DISABLE_LADDERS					RETURN "MC_H_GC_P_LA"
	ENDSWITCH
	
	RETURN ""

ENDFUNC

FUNC STRING GET_RIGHT_HAND_GANG_CHASE_MENU_DESCRIPTION()
	
	SWITCH GET_CURRENT_RHM_SELECTION()
		CASE ciGANG_CHASE_TYPE									RETURN "MC_H_GC_UT"
		CASE ciGANG_CHASE_MULTI_TYPE							RETURN "MC_H_GC_MUT"
		CASE ciGANG_CHASE_INTENSITY								RETURN "MC_H_GC_I"
		CASE ciGANG_CHASE_MAX_UNIT_SPAWNS						RETURN "MC_H_GC_MU"
		CASE ciGANG_CHASE_MULTIPLY_MAX_SPAWNS_BY_PLAYERS		RETURN "MC_H_GC_MBP"
		CASE ciGANG_CHASE_BLOCK_OBJECTIVE_PROGRESSION			RETURN "MC_H_GC_BOP"
		CASE ciGANG_CHASE_IGNORE_OBJECTIVE_TARGETING			RETURN "MC_H_GC_IOT"
		CASE ciGANG_CHASE_FORWARD_SPAWNING_CHANCE				RETURN "MC_H_GC_FSS"
		CASE ciGANG_CHASE_SPAWN_BEHIND_CHANCE					RETURN "MC_H_GC_SBC"
		CASE ciGANG_CHASE_VEHICLES_COLOR						RETURN "MC_H_GC_V_C"
		CASE ciGANG_CHASE_VEHICLES_HEALTH						RETURN "MC_H_GC_V_H"
		CASE ciGANG_CHASE_VEHICLES_BLOCK_HELI_HOMING_ROCKETS	RETURN "MC_H_GC_V_BH"
		CASE ciGANG_CHASE_VEHICLES_BLOCK_BIKE_PASSENGERS		RETURN "MC_H_GC_V_BP"
		CASE ciGANG_CHASE_PEDS_ACCURACY							RETURN "MC_H_GC_P_A"
		CASE ciGANG_CHASE_PEDS_RANDOMIZE_WEAPONS				RETURN "MC_H_GC_P_RW"
		CASE ciGANG_CHASE_PEDS_BLOCK_COMMANDEERING				RETURN "MC_H_GC_P_BC"
		CASE ciGANG_CHASE_PEDS_BLOCK_JACKING					RETURN "MC_H_GC_P_BJ"
		CASE ciGANG_CHASE_PEDS_USE_BURST_FIRE					RETURN "MC_H_GC_P_B"
		CASE ciGANG_CHASE_PEDS_DISABLE_LADDERS					RETURN "MC_H_GC_P_LA"
		CASE ciGANG_CHASE_ACTIVATION_MIDPOINT					RETURN "MC_H_GC_A_MP"
		CASE ciGANG_CHASE_ACTIVATION_DELAY						RETURN "MC_H_GC_A_D"
		CASE ciGANG_CHASE_ACTIVATION_FLEE_DISTANCE				RETURN "MC_H_GC_A_FD"
		CASE ciGANG_CHASE_ACTIVATION_AREA						RETURN "MC_H_GC_A_AT"
		CASE ciGANG_CHASE_ACTIVATION_CLEANUP_ON_NEW_TYPE		RETURN "MC_H_GC_A_NT"
		CASE ciGANG_CHASE_ACTIVATION_ON_SPEED_BAR_FAIL			RETURN "MC_H_GC_A_SB"
		CASE ciGANG_CHASE_ACTIVATION_ON_AGGRO					RETURN "MC_H_GC_A_OA"
		CASE ciGANG_CHASE_UI_ALT_OBJECTIVE_TEXT					RETURN "MC_H_GC_U_O"
		CASE ciGANG_CHASE_UI_OVERRIDE_OBJECTIVE_TEXT			RETURN "MC_H_GC_U_T"
		CASE ciGANG_CHASE_UI_BLIPS_FORCE_ON						RETURN "MC_H_GC_U_BF"
		CASE ciGANG_CHASE_UI_BLIPS_NOTICE_RADIUS				RETURN "MC_H_GC_U_BN"
		CASE ciGANG_CHASE_UI_BLIPS_SAME_INTERIOR				RETURN "MC_H_GC_U_BI"
		CASE ciGANG_CHASE_UI_BLIPS_COPS							RETURN "MC_H_GC_U_BC"
		CASE ciGANG_CHASE_DISABLE_LAW_AND_COP_BEHAVIOURS		RETURN "MC_H_GC_U_LAW"
		CASE ciGANG_CHASE_RESPAWN_DELAY							RETURN "MC_H_GC_RD"
	ENDSWITCH
	
	RETURN ""
	
ENDFUNC

FUNC STRING GET_RIGHT_SIDE_PED_MENU_DESCRIPTIONS()
	
	SWITCH GET_CURRENT_RHM_SELECTION()
		CASE ciPED_MENU_RIGHT_HAND_SCORE_VALID_WITH_OBJ				RETURN "MC_H_PE_SVWO"
		CASE ciPED_MENU_RIGHT_HAND_LEAVE_PED_ENTIRE_TEAM			RETURN "MC_H_PE_ALLE"
		CASE ciPED_MENU_RIGHT_HAND_VEHICLE_SPEED_OVERRIDE			RETURN "MC_H_PE_OVVSP"
		CASE ciPED_MENU_RIGHT_HAND_PED_PROGRESS_SYNC_SCENE_STAGE	RETURN "MC_H_PE_PASFP"
		CASE ciPED_MENU_RIGHT_HAND_ASSOCIATED_GOTO_ASSIGNMENT 		RETURN "MC_H_PE_SGPO"
	ENDSWITCH
	
	RETURN ""
	
ENDFUNC

FUNC STRING GET_RIGHT_SIDE_OBJ_MENU_DESCRIPTIONS()
	
	SWITCH GET_CURRENT_RHM_SELECTION()
		CASE ciOBJ_MENU_RIGHT_HAND_RULE_WARPS				RETURN "MC_H_OB_WRPED"
		CASE ciOBJ_MENU_RIGHT_HAND_REQUIRED_FOR_RULE_PASS	RETURN "MC_H_OB_RFR"
		CASE ciOBJ_MENU_RIGHT_HAND_LEAVE_OBJ_ENTIRE_TEAM	RETURN "MC_H_OB_ALLE"
	ENDSWITCH
	
	RETURN ""
	
ENDFUNC

FUNC STRING GET_RIGHT_SIDE_LOC_MENU_DESCRIPTIONS()
	
	SWITCH GET_CURRENT_RHM_SELECTION()
		CASE ciLOC_MENU_RIGHT_HAND_RULE_WARPS				RETURN "MC_H_LO_WRPED"
		CASE ciLOC_MENU_RIGHT_HAND_OUT_OF_ALL				RETURN "MC_H_RLOC_AL"
	ENDSWITCH
	
	RETURN ""
	
ENDFUNC

FUNC STRING GET_ALTITUDE_SYSTEM_BAR_OPTIONS_MENU_HELP_STRING(INT iRow)
	SWITCH iRow
		CASE ciALTITUDE_BAR_SYSTEM_TITLE							RETURN "MCH_RL_ALT_T"
		CASE ciALTITUDE_BAR_SYSTEM_ENABLE							RETURN "MCH_RL_ALT_ENA"
		CASE ciALTITUDE_BAR_SYSTEM_MINIMUM							RETURN "MCH_RL_ALT_MIN"
		CASE ciALTITUDE_BAR_SYSTEM_THRESHOLD						RETURN "MCH_RL_ALT_THR"
		CASE ciALTITUDE_BAR_SYSTEM_MAXIMUM							RETURN "MCH_RL_ALT_MAX"
		CASE ciALTITUDE_BAR_SYSTEM_REQUIRES_VEHICLE					RETURN "MCH_RL_ALT_RVE"
		CASE ciALTITUDE_BAR_SYSTEM_DRAW_ALTIMETER					RETURN "MCH_RL_ALT_DAL"
		CASE ciALTITUDE_BAR_SYSTEM_SPECIFIC_VEHICLE					RETURN "MCH_RL_ALT_SBV"
		CASE ciALTITUDE_BAR_SYSTEM_FAIL_TIME						RETURN "MCH_RL_ALT_SBFT"
		CASE ciALTITUDE_BAR_SYSTEM_BAR_VISIBLILITY_T1				RETURN "MCH_RL_ALT_VT1"
		CASE ciALTITUDE_BAR_SYSTEM_BAR_VISIBLILITY_T2				RETURN "MCH_RL_ALT_VT2"
		CASE ciALTITUDE_BAR_SYSTEM_BAR_VISIBLILITY_T3				RETURN "MCH_RL_ALT_VT3"
		CASE ciALTITUDE_BAR_SYSTEM_BAR_VISIBLILITY_T4				RETURN "MCH_RL_ALT_VT4"
		CASE ciALTITUDE_BAR_SYSTEM_TEXT_FOR_BAR						RETURN "MCH_RL_ALT_TFB"
		CASE ciALTITUDE_BAR_SYSTEM_TEXT_FOR_CONSEQUENCE_BAR			RETURN "MCH_RL_ALT_TFBC"
		CASE ciALTITUDE_BAR_SYSTEM_EXPLODE_WHEN_BAR_IS_FULL			RETURN "MCH_RL_ALT_EXPF"
		CASE ciALTITUDE_BAR_SYSTEM_AIRSTRIKE_WHEN_BAR_IS_FULL		RETURN "MCH_RL_ALT_AIRF"
		CASE ciALTITUDE_BAR_SYSTEM_GANGCHASE_WHEN_BAR_IS_FULL		RETURN "MCH_RL_ALT_GANF"
		CASE ciALTITUDE_BAR_SYSTEM_WANTED_WHEN_BAR_IS_FULL			RETURN "MCH_RL_ALT_WANTF"
		CASE ciALTITUDE_BAR_SYSTEM_ALWAYS_BLIP						RETURN "MCH_RL_ALT_BLA"
		CASE ciALTITUDE_BAR_SYSTEM_BLIP_WHEN_THRESHOLD_MET			RETURN "MCH_RL_ALT_BLT"
		CASE ciALTITUDE_BAR_SYSTEM_BLIP_TIME						RETURN "MCH_RL_ALT_BT"
	ENDSWITCH	
	RETURN ""
ENDFUNC

FUNC STRING GET_MUSIC_MENU_HELP_STRING(INT iRow)
	SWITCH iRow
		CASE ciMUSIC_USE_MID_POINT_STYLE_ON_SCORE_REACHED		RETURN "MC_H_SS_AU_SCR"
		CASE ciMUSIC_AGGRO_MOOD									RETURN "MC_H_SS_AU_AGG"
		CASE ciMUSIC_ON_FOOT_MOOD								RETURN "MC_H_SS_AU_OFT"
		CASE ciMUSIC_IN_AIRCRAFT_MOOD							RETURN "MC_H_SS_AU_IAR"
		CASE ciMUSIC_PARACHUTING_MOOD							RETURN "MC_H_SS_AU_PAR"
		CASE ciMUSIC_SWIMMING_MOOD								RETURN "MC_H_SS_AU_SWI"
		CASE ciMUSIC_BOATING_MOOD								RETURN "MC_H_SS_AU_BOA"
		CASE ciMUSIC_DRIVING_MOOD								RETURN "MC_H_SS_AU_DRI"
		CASE ciMUSIC_INTERIOR_SPACE_MOOD						RETURN "MC_H_SS_AU_INT"
		CASE ciMUSIC_INTERIOR_SPACE_ALT_1_MOOD					RETURN "MC_H_SS_AU_INT1"
		CASE ciMUSIC_INTERIOR_SPACE_ALT_2_MOOD					RETURN "MC_H_SS_AU_INT2"
		CASE ciMUSIC_IS_INTERACTING_MOOD						RETURN "MC_H_SS_AU_HAK"
		CASE ciMUSIC_ON_FOOT_AGGRO_MOOD							RETURN "MC_H_SS_AU_OFTA"
		CASE ciMUSIC_IN_AIRCRAFT_AGGRO_MOOD						RETURN "MC_H_SS_AU_IARA"
		CASE ciMUSIC_PARACHUTING_AGGRO_MOOD						RETURN "MC_H_SS_AU_PARA"
		CASE ciMUSIC_SWIMMING_AGGRO_MOOD						RETURN "MC_H_SS_AU_SWIA"
		CASE ciMUSIC_BOATING_AGGRO_MOOD							RETURN "MC_H_SS_AU_BOAA"
		CASE ciMUSIC_DRIVING_AGGRO_MOOD							RETURN "MC_H_SS_AU_DRIA"
		CASE ciMUSIC_INTERIOR_SPACE_AGGRO_MOOD					RETURN "MC_H_SS_AU_INTA"
		CASE ciMUSIC_INTERIOR_SPACE_AGGRO_ALT_1_MOOD			RETURN "MC_H_SS_AU_INTA1"
		CASE ciMUSIC_INTERIOR_SPACE_AGGRO_ALT_2_MOOD			RETURN "MC_H_SS_AU_INTA2"
		CASE ciMUSIC_IS_INTERACTING_AGGRO_MOOD					RETURN "MC_H_SS_AU_HAKA"
		CASE ciMUSIC_CHASED_BY_GANG_MOOD						RETURN "MC_H_SS_AU_CHS"
		CASE ciMUSIC_CHASED_BY_GANG_AGGRO_MOOD					RETURN "MC_H_SS_AU_CHSA"
		CASE ciMUSIC_WANTED_GREATER_THAN_1_MOOD					RETURN "MC_H_SS_AU_WNT1"
		CASE ciMUSIC_WANTED_GREATER_THAN_1_AGGRO_MOOD			RETURN "MC_H_SS_AU_WNT1A"
		CASE ciMUSIC_AGGRO_INDEX								RETURN "MCH_AGRID_MUS"
		CASE ciMUSIC_DISABLE_ALL_MUSIC							RETURN "MC_H_MC_DAM"
	ENDSWITCH
	RETURN ""
ENDFUNC
#IF FEATURE_DLC_1_2022
FUNC STRING GET_ON_RULE_START_OPTIONS_MENU_DESCRIPTION(INT iRow)
	SWITCH iRow
		CASE ciORS_LIVES							RETURN "MC_H_RL_ORS_L"
		CASE ciORS_TIME								RETURN "MC_H_RL_ORS_T"
		CASE ciORS_RAGDOLL_DURATION 				RETURN "MC_H_ORS_RD"
		CASE ciORS_FADE_OUT_DURATION				RETURN "MC_H_ORS_FOD"
		CASE ciORS_FADE_IN_DURATION					RETURN "MC_H_ORS_FID"
		CASE ciORS_RULE_RELATIONSHIP_GROUPS			RETURN "MC_H_ORS_FID"
	ENDSWITCH
	RETURN ""
ENDFUNC

FUNC STRING GET_RELATIONSHIP_ON_RULE_MENU_DESCRIPTION(INT iRow)
	SWITCH iRow
		CASE ciRELGROUP_ON_RULE_TITLE									RETURN "MC_H_REL_RGR"
		CASE ciRELGROUP_ON_RULE_ENABLED									RETURN "MC_H_REL_ENA"
		CASE ciRELGROUP_ON_RULE_ENABLED_WHEN_USING_MODEL_SWAP_ONLY		RETURN "MC_H_REL_MENA"
	ENDSWITCH
	RETURN ""
ENDFUNC

FUNC STRING GET_ON_RULE_LIVES_OPTIONS_MENU_DESCRIPTION(INT iRow)
	SWITCH iRow
		CASE ciORSL_ENABLE		RETURN "MC_H_ORSL_E"
		CASE ciORSL_SCALING		RETURN "MC_H_ORSL_S"
		CASE ciORSL_TYPE		RETURN "MC_H_ORSL_LT"
		DEFAULT					RETURN "MC_H_ORSL_B"
	ENDSWITCH
	RETURN ""
ENDFUNC

FUNC STRING GET_ON_RULE_TIME_OPTIONS_MENU_DESCRIPTION(INT iRow)
	SWITCH iRow
		CASE ciORST_ENABLE		RETURN "MC_H_ORST_E"
		CASE ciORST_SCALING		RETURN "MC_H_ORST_S"
		CASE ciORST_TYPE		RETURN "MC_H_ORST_TT"
		DEFAULT					RETURN "MC_H_ORST_B"
	ENDSWITCH
	RETURN ""
ENDFUNC

FUNC STRING GET_PLAYER_MODEL_SWAP_OPTIONS_MENU_DESCRIPTION(INT iRow)
	SWITCH iRow
		CASE ciPMSR_ON_RULE_START	RETURN "MC_H_PMS_ORS"
		CASE ciPMSR_ON_KILL			RETURN "MC_H_PMS_OK"
		CASE ciPMSR_ON_SPAWN		RETURN "MC_H_PMS_OR"
		CASE ciPMSR_PREREQ			RETURN "MC_H_PMS_PRQ"
	ENDSWITCH
	RETURN ""
ENDFUNC

FUNC STRING GET_INTERACTABLE_OPTIONS_MENU_DESCRIPTION(INT iRow)
	SWITCH iRow
		CASE ciINTOP_ORIP		RETURN "MC_H_RL_ORIP"
	ENDSWITCH
	RETURN ""
ENDFUNC

FUNC STRING GET_LIGHTING_OPTIONS_MENU_DESCRIPTION(INT iRow)
	SWITCH iRow
		CASE ciLIGHTING_OPTIONS_ARTIFICAL_LIGHTS				RETURN "FMMC_SS_RL_58H"
		CASE ciLIGHTING_OPTIONS_ARTIFICAL_LIGHTS_BLOCK_SFX		RETURN "FMMC_SS_RL_5LH"
	ENDSWITCH
	RETURN ""
ENDFUNC

FUNC STRING GET_CACHED_VEHICLE_OPTIONS_MENU_DESCRIPTION(INT iRow)
	SWITCH iRow
		CASE ciCACHED_VEHICLE_OPTIONS_CALL_VEHICLE					RETURN "MC_H_VE_WLUV"
		CASE ciCACHED_VEHICLE_OPTIONS_CALL_VEHICLE_BUTTON_TIMER		RETURN "HMC_SS_RL_CVBHT"
		CASE ciCACHED_VEHICLE_OPTIONS_HEALING_RANGE					RETURN "HMC_SS_RL_CVHR"
		CASE ciCACHED_VEHICLE_OPTIONS_HEALING_RATE					RETURN "HMC_SS_RL_CVHRT"
		CASE ciCACHED_VEHICLE_OPTIONS_HEALING_PERCENTAGE			RETURN "HMC_SS_RL_CVHP"
		CASE ciCACHED_VEHICLE_OPTIONS_DAMAGE_MULTIPLIER				RETURN "HMC_SS_RL_CVDM"
	ENDSWITCH
	RETURN ""
ENDFUNC

#ENDIF

FUNC STRING GET_RIGHT_HAND_MENU_ITEM_DESCRIPTION()
	
	IF g_CreatorsSelDetails.iRow = -1
		RETURN ""
	ENDIF
	
	IF NOT IS_RHM_IN_A_SUB_MENU()
		IF g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule = ciSELECTION_COLLECT_PED RETURN "MC_H_COLP" ENDIF
		IF g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule = ciSELECTION_KILL_PED RETURN "MC_H_KILP" ENDIF
		IF g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule = ciSELECTION_DAMAGE_PED RETURN "MC_H_DAMP" ENDIF
		IF g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule = ciSELECTION_DAMAGE_VEHICLE RETURN "MC_H_DAMV" ENDIF
		IF g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule = ciSELECTION_DAMAGE_OBJECT RETURN "MC_H_DAMO" ENDIF
		IF g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule = ciSELECTION_PROTECT_PED RETURN "MC_H_PROP" ENDIF
		IF g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule = ciSELECTION_GO_TO_PED RETURN "MC_H_GOP" ENDIF
		IF g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule = ciSELECTION_CROWD_CONTROL RETURN "MC_H_CRCN" ENDIF
		IF g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule = ciSELECTION_CAPTURE_PED RETURN "MC_H_CONP" ENDIF
		IF g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule = ciSELECTION_CHARM_PED RETURN "MC_H_CHP" ENDIF
		IF g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule = ciSELECTION_COLLECT_VEHICLE RETURN "MC_H_COLV" ENDIF
		IF g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule = ciSELECTION_KILL_VEHICLE RETURN "MC_H_KILV" ENDIF
		IF g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule = ciSELECTION_PROTECT_VEHICLE RETURN "MC_H_PROV" ENDIF
		IF g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule = ciSELECTION_GO_TO_VEHICLE RETURN "MC_H_GOV" ENDIF
		IF g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule = ciSELECTION_CAPTURE_VEHICLE RETURN "MC_H_CONV" ENDIF
		IF g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule = ciSELECTION_COLLECT_OBJECT RETURN "MC_H_COLO"	 ENDIF
		IF g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule = ciSELECTION_KILL_OBJECT RETURN "MC_H_KILO" ENDIF
		IF g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule = ciSELECTION_PROTECT_OBJECT RETURN "MC_H_PROO" ENDIF
		IF g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule = ciSELECTION_GO_TO_OBJECT RETURN "MC_H_GOVO" ENDIF
		IF g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule = ciSELECTION_CAPTURE_OBJECT		 RETURN "MC_H_CONO" ENDIF
		IF g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule = ciSELECTION_LOCATION_GO_TO RETURN "MC_H_GOTO2" ENDIF
		IF g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule = ciSELECTION_LOCATION_CAPTURE			 RETURN "MC_H_LOCC" ENDIF
		IF g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule = ciSELECTION_PLAYER_RULE_KILL_ANY RETURN "MC_H_KPA" ENDIF
		IF g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule = ciSELECTION_PLAYER_RULE_KILL_TEAM0 RETURN "MC_H_KPT1" ENDIF
		IF g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule = ciSELECTION_PLAYER_RULE_KILL_TEAM1		 RETURN "MC_H_KPT2" ENDIF
		IF g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule = ciSELECTION_PLAYER_RULE_KILL_TEAM2 RETURN "MC_H_KPT3" ENDIF
		IF g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule = ciSELECTION_PLAYER_RULE_KILL_TEAM3			 RETURN "MC_H_KPT4" ENDIF
		IF g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule = ciSELECTION_PLAYER_RULE_GET_MASKS			 RETURN "MC_H_KPT5" ENDIF
		IF g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule = ciSELECTION_LEAVE_PED					 	RETURN "MC_H_PED38" ENDIF
		IF g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule = ciSELECTION_LEAVE_VEHICLE					RETURN "MC_H_VEH26" ENDIF
		IF g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule = ciSELECTION_LEAVE_OBJECT						RETURN "MC_H_OBJ16" ENDIF
	ENDIF	
	
	IF IS_THIS_CURRENT_RHM_MENU(eRHM_OutOfBounds)
	OR IS_THIS_CURRENT_RHM_MENU(eRHM_SecOutOfBounds)
		IF IS_THIS_CURRENT_RHM_MENU(eRHM_OnePlayerOutOfBoundsTeam) RETURN "MC_H_OBT"	ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciLEGACY_BOUNDS_SET_TYPE RETURN "MC_H_B0" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciLEGACY_BOUNDS_SET_AREA RETURN "MC_H_B1" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciLEGACY_BOUNDS_SET_AREA_MAXHEIGHT RETURN "MC_H_B2" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciLEGACY_BOUNDS_SET_AREA_MINHEIGHT RETURN "MC_H_B3" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciLEGACY_BOUNDS_PLAY_LEAVE_AREA_TOGGLE RETURN "MC_H_B_PLA" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciLEGACY_BOUNDS_PLAY_AREA_TIMER RETURN "MC_H_B11" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciLEGACY_BOUNDS_LOOK_AT_ENTITY RETURN "MC_H_B11" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciLEGACY_BOUNDS_PLAY_AREA_TRIGGER_ON_START RETURN "MC_H_B_TOS" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciLEGACY_BOUNDS_SPAWN_AREA_TOGGEL RETURN "MC_H_B5" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciLEGACY_BOUNDS_SPAWN_AREA_HEADING RETURN "MC_H_B_SHV" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciLEGACY_BOUNDS_WANTED_AREA_TOGGEL RETURN "MC_H_B7" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciLEGACY_BOUNDS_WANTED_TO_GIVE RETURN "MC_H_B8" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciLEGACY_BOUNDS_IGNORE_WANTED_VEH RETURN "MC_H_B9" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciLEGACY_BOUNDS_IGNORE_OUTFIT RETURN "MC_H_B10" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciLEGACY_BOUNDS_SPAWN_AHEAD RETURN "MC_H_BSA" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciLEGACY_BOUNDS_SHOW_BLIP_ON_FAIL RETURN "MC_H_B_BLP" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciLEGACY_BOUNDS_ENTITY_TYPE RETURN "MC_H_B_ETYP" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciLEGACY_BOUNDS_ONE_PLAYER_FROM_TEAM_MENU RETURN "MC_H_OBT" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciLEGACY_BOUNDS_ENTITY_ID RETURN "MC_H_B_EID" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciLEGACY_BOUNDS_MINIMAP_OPACITY RETURN "MC_H_BMMO" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciLEGACY_BOUNDS_CENTRE_LINE_RESPAWN RETURN "MC_H_BCLR" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciLEGACY_BOUNDS_CENTRE_LINE_OFFSET RETURN "MC_H_BCLO" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciLEGACY_BOUNDS_CENTRE_LINE_RADIUS RETURN "MC_H_BCLR" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciLEGACY_BOUNDS_CENTRE_LINE_PLAYABLE_AREA RETURN "MC_H_BCLPA" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciLEGACY_BOUNDS_KILLPLAYER RETURN "MC_H_BMKP" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciLEGACY_BOUNDS_ONLY_AFFECT_OT_PLAYER RETURN "MC_H_OAAOT" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciLEGACY_BOUNDS_DONT_FAIL_THROUGH_MAP_BOUNDS RETURN "MC_H_DFMP" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciLEGACY_BOUNDS_FAIL_OBJECTIVE_NOT_MISSION	RETURN "MC_H_FONM" ENDIF	
		IF GET_CURRENT_RHM_SELECTION() = ciLEGACY_BOUNDS_IGNORE_Z_HEIGHT_CHECKS_SPHERES RETURN "MC_H_DCZH" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciLEGACY_BOUNDS_IGNORE_LEAVE_AREA_VEH RETURN "MC_H_ILAV" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciLEGACY_BOUNDS_LINKED_SPAWN_GROUP RETURN "MC_H_BUND_SG" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciLEGACY_BOUNDS_BLIP_HEIGHT_THRESHOLD RETURN "MC_H_BUND_BHT" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciLEGACY_BOUNDS_COLOUR RETURN "MC_H_BUND_BC" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciLEGACY_BOUNDS_KEEP_ENTITY_SPAWN_IN_BOUNDS RETURN "MC_H_BUND_KESB" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciLEGACY_BOUNDS_ONLY_ALLOW_ONE_PLAYER_PER_TEAM RETURN "MC_H_BUND_OPPB" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciLEGACY_BOUNDS_REGEN_VEHICLES_IN_BOUNDS RETURN "MC_H_BUND_RVHC" ENDIF	
		IF GET_CURRENT_RHM_SELECTION() = ciLEGACY_BOUNDS_BOUND_ACTIVATION_DELAY RETURN "MC_H_BUND_DAT" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciLEGACY_BOUNDS_FORCE_GPS_FOR_BLIP RETURN "MC_H_BUND_DAT" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciLEGACY_BOUNDS_COPY_TO_RULE_X RETURN "MC_H_BCSTX" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciLEGACY_BOUNDS_USE_SETTINGS_FOR_ALL_RULES RETURN "MC_H_BUSFAR" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciLEGACY_BOUNDS_DELETE RETURN "MC_H_B6" ENDIF
	ENDIF
	
	IF IS_THIS_CURRENT_RHM_MENU(eRHM_MusicCues)
		RETURN GET_MUSIC_MENU_HELP_STRING(GET_CURRENT_RHM_SELECTION())
	ENDIF
	
	IF IS_THIS_CURRENT_RHM_MENU(eRHM_RuleBounds_SpecifiedZones)
		RETURN ""
	ENDIF
	
	IF IS_THIS_CURRENT_RHM_MENU(eRHM_OnePlayerOutOfBoundsTeam)
		RETURN "MC_H_OBT"
	ENDIF
	
	IF IS_THIS_CURRENT_RHM_MENU(eRHM_RuleBounds)
		IF GET_CURRENT_RHM_SELECTION() = ciRULE_BOUNDS_TIME_ALLOWED_OUT_OF_BOUNDS RETURN "MC_H_B11" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciRULE_BOUNDS_CUSTOM_OOB_STRING_INDEX RETURN "MC_H_BUND_COOBT" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciRULE_BOUNDS_LOOK_AT_ENTITY RETURN "MC_H_B11" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciRULE_BOUNDS_TRIGGER_ON_START RETURN "MC_H_B_TOS" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciRULE_BOUNDS_SUSTAIN_WANTED_LEVEL RETURN "MC_H_B7" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciRULE_BOUNDS_WANTED_TO_GIVE RETURN "MC_H_B8" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciRULE_BOUNDS_IGNORE_WANTED_VEH RETURN "MC_H_B9" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciRULE_BOUNDS_IGNORE_OUTFIT RETURN "MC_H_B10" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciRULE_BOUNDS_HIDE_VISUALS_WHEN_IN_BOUNDS RETURN "MC_H_BMOOB" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciRULE_BOUNDS_SHOW_OOB_BLIPS RETURN "MC_H_B_BLP" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciRULE_BOUNDS_OOB_BLIP_TYPE RETURN "MC_H_B_BLPT" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciRULE_BOUNDS_ONE_PLAYER_FROM_TEAM_MENU RETURN "MC_H_OBT" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciRULE_BOUNDS_KILLPLAYER RETURN "MC_H_BMKP" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciRULE_BOUNDS_DONT_FAIL_THROUGH_MAP_BOUNDS RETURN "MC_H_DFMP" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciRULE_BOUNDS_FAIL_OBJECTIVE_NOT_MISSION	RETURN "MC_H_FONM" ENDIF	
		IF GET_CURRENT_RHM_SELECTION() = ciRULE_BOUNDS_IGNORE_LEAVE_AREA_VEH RETURN "MC_H_ILAV" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciRULE_BOUNDS_LINKED_SPAWN_GROUP RETURN "MC_H_BUND_SG" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciRULE_BOUNDS_OOB_BLIP_HEIGHT_THRESHOLD RETURN "MC_H_BUND_BHT" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciRULE_BOUNDS_KEEP_ENTITY_SPAWN_IN_BOUNDS RETURN "MC_H_BUND_KESB" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciRULE_BOUNDS_ONLY_ALLOW_ONE_PLAYER_PER_TEAM RETURN "MC_H_BUND_OPPB" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciRULE_BOUNDS_REGEN_VEHICLES_IN_BOUNDS RETURN "MC_H_BUND_RVHC" ENDIF	
		IF GET_CURRENT_RHM_SELECTION() = ciRULE_BOUNDS_OOB_BLIP_FORCE_GPS RETURN "MC_H_BUND_DAT" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciRULE_BOUNDS_COPY_TO_RULE_X RETURN "MC_H_BCSTX" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciRULE_BOUNDS_USE_SETTINGS_FOR_ALL_RULES RETURN "MC_H_BUSFAR" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciRULE_BOUNDS_DELETE RETURN "MC_H_B6" ENDIF
	ENDIF
	
	IF IS_THIS_CURRENT_RHM_MENU(eRHM_RadarBlips) RETURN "MC_H_RDR_TM" ENDIF
	IF IS_THIS_CURRENT_RHM_MENU(eRHM_HUDVisibility) RETURN "MC_H_SOT_TM" ENDIF
	IF IS_THIS_CURRENT_RHM_MENU(eRHM_TeamHealthBars) RETURN "MC_H_SHB_TM" ENDIF
	IF IS_THIS_CURRENT_RHM_MENU(eRHM_OtherTeamLivesVisibility) RETURN "MC_H_DOT_TM" ENDIF
	
	IF IS_THIS_CURRENT_RHM_MENU(eRHM_ChaseGangArea)
		IF GET_CURRENT_RHM_SELECTION() = ciGANGAREA_TYPE RETURN "MC_H_G6" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciGANGAREA_WIDTH RETURN "MC_H_G7" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciGANGAREA_MAXHEIGHT RETURN "MC_H_G8" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciGANGAREA_MINHEIGHT RETURN "MC_H_G9" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciGANGAREA_CLEAR RETURN "MC_H_G10" ENDIF
	ENDIF
	
	IF IS_THIS_CURRENT_RHM_MENU(eRHM_ChaseGangMultipleTypes)
		RETURN "MC_H_GC_MUTS"
	ENDIF
	
	IF IS_THIS_CURRENT_RHM_MENU(eRHM_ChaseGang)
		IF IS_LEGACY_MISSION_CREATOR()
			RETURN GET_RIGHT_HAND_GANG_CHASE_LEGACY_MENU_DESCRIPTION()
		ELSE
			RETURN GET_RIGHT_HAND_GANG_CHASE_MENU_DESCRIPTION()
		ENDIF
	ENDIF
	
	IF IS_THIS_CURRENT_RHM_MENU(eRHM_RestartCheckpoints)
		IF IS_LEGACY_MISSION_CREATOR()
			RETURN GET_RIGHT_HAND_RESTART_CHECKPOINTS_LEGACY_MENU_DESCRIPTION()
		ELSE
			RETURN GET_RIGHT_HAND_RESTART_CHECKPOINTS_MENU_DESCRIPTION()
		ENDIF
	ENDIF
	
	IF IS_THIS_CURRENT_RHM_MENU(eRHM_RestartCheckpointSettings)
		RETURN GET_RIGHT_HAND_RESTART_CHECKPOINT_SETTINGS_DESCRIPTION()
	ENDIF
	
	IF IS_THIS_CURRENT_RHM_MENU(eRHM_Filters)
		IF GET_CURRENT_RHM_SELECTION() = ciFILTERS_PLAYER_SELECTION RETURN "MC_H_SFM_PS" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciFILTERS_FORCED RETURN "MC_H_SFM_FF" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciFILTERS_LOW_HEALTH RETURN "MC_H_SFM_LH" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciFILTERS_CONTROL RETURN "MC_H_SFM_CTRL" ENDIF
		RETURN "MC_H_RL_FM"
	ENDIF
	
	IF IS_THIS_CURRENT_RHM_MENU(eRHM_FiltersControl)
		SWITCH GET_CURRENT_RHM_SELECTION()
			CASE ciFILTER_CONTROLS_FADE_IN 		RETURN "MC_H_FCM_FI"
			CASE ciFILTER_CONTROLS_FADE_OUT 		RETURN "MC_H_FCM_FO"
			CASE ciFILTER_CONTROLS_MAX_STRENGTH 	RETURN "MC_H_FCM_STR"
			CASE ciFILTER_CONTROLS_TARGET_POINTS	RETURN "MC_H_FCM_FBOP"
			CASE ciFILTER_CONTROLS_COUNT_POINTS_TEAM_0
			CASE ciFILTER_CONTROLS_COUNT_POINTS_TEAM_1
			CASE ciFILTER_CONTROLS_COUNT_POINTS_TEAM_2
			CASE ciFILTER_CONTROLS_COUNT_POINTS_TEAM_3
				RETURN "MC_H_FCM_IT"
			CASE ciFILTER_CONTROLS_ACCEL_OUT_TYPE	RETURN "MC_H_FCM_AFO"
			CASE ciFILTER_CONTROLS_ACCEL_OUT_TIME	RETURN "MC_H_FCM_AFOT"
		ENDSWITCH
	ENDIF
	
	IF IS_THIS_CURRENT_RHM_MENU(eRHM_STOD)
		IF GET_CURRENT_RHM_SELECTION() = ciSPECIFIC_TOD_HOUR RETURN "MC_H_RL_STOD_0" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciSPECIFIC_TOD_CHANGE_ON_RULE RETURN "MC_H_RL_STOD_1" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciSPECIFIC_TOD_PROGRESS_UPDATE RETURN "MC_H_STOD_UPTP" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciSPECIFIC_TOD_MINUTE RETURN "MC_H_RL_STOD_2" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciSPECIFIC_TOD_SPEED RETURN "MC_H_RL_STOD_3" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciSPECIFIC_TOD_ENABLE_IS_START_TIME RETURN "MC_H_RL_STOD_4" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciSPECIFIC_TOD_START_HOUR RETURN "MC_H_RL_STOD_5" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciSPECIFIC_TOD_WEATHER_CLEAR_OVERRIDE_DELAY RETURN "MC_H_RL_STOD_9" ENDIF
		RETURN "MC_H_RL_STOD"
	ENDIF
	
	IF IS_THIS_CURRENT_RHM_MENU(eRHM_MusicCues)
		IF GET_CURRENT_RHM_SELECTION() = ciMUSIC_START 									RETURN "MC_H_MC" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciMUSIC_MIDPOINT 								RETURN "MC_H_MC" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciMUSIC_CAPTURE_BAR_CUES 						RETURN "MC_H_MC_CBC" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciMUSIC_USE_MID_POINT_STYLE_ON_SCORE_REACHED 	RETURN "MC_H_SS_AU_SCR" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciMUSIC_AGGRO_MOOD 								RETURN "MC_H_SS_AU_AGG" ENDIF		
		IF GET_CURRENT_RHM_SELECTION() = ciMUSIC_ON_FOOT_MOOD								RETURN "MC_H_SS_AU_OFT" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciMUSIC_IN_AIRCRAFT_MOOD							RETURN "MC_H_SS_AU_IAR" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciMUSIC_PARACHUTING_MOOD							RETURN "MC_H_SS_AU_PAR" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciMUSIC_SWIMMING_MOOD							RETURN "MC_H_SS_AU_SWI" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciMUSIC_BOATING_MOOD								RETURN "MC_H_SS_AU_BOA" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciMUSIC_DRIVING_MOOD								RETURN "MC_H_SS_AU_DRI" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciMUSIC_INTERIOR_SPACE_MOOD						RETURN "MC_H_SS_AU_INT" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciMUSIC_INTERIOR_SPACE_ALT_1_MOOD				RETURN "MC_H_SS_AU_INT1" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciMUSIC_INTERIOR_SPACE_ALT_2_MOOD				RETURN "MC_H_SS_AU_INT2" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciMUSIC_IS_INTERACTING_MOOD						RETURN "MC_H_SS_AU_HAK" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciMUSIC_ON_FOOT_AGGRO_MOOD						RETURN "MC_H_SS_AU_OFTA" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciMUSIC_IN_AIRCRAFT_AGGRO_MOOD					RETURN "MC_H_SS_AU_IARA" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciMUSIC_PARACHUTING_AGGRO_MOOD					RETURN "MC_H_SS_AU_PARA" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciMUSIC_SWIMMING_AGGRO_MOOD						RETURN "MC_H_SS_AU_SWIA" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciMUSIC_BOATING_AGGRO_MOOD						RETURN "MC_H_SS_AU_BOAA" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciMUSIC_DRIVING_AGGRO_MOOD						RETURN "MC_H_SS_AU_DRIA" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciMUSIC_INTERIOR_SPACE_AGGRO_MOOD				RETURN "MC_H_SS_AU_INTA" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciMUSIC_INTERIOR_SPACE_AGGRO_ALT_1_MOOD			RETURN "MC_H_SS_AU_INTA1" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciMUSIC_INTERIOR_SPACE_AGGRO_ALT_2_MOOD			RETURN "MC_H_SS_AU_INTA2" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciMUSIC_IS_INTERACTING_AGGRO_MOOD				RETURN "MC_H_SS_AU_HAKA" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciMUSIC_AGGRO_INDEX								RETURN "MCH_AGRID_MUS" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciMUSIC_DISABLE_ALL_MUSIC						RETURN "MC_H_MC_DAM" ENDIF
	ENDIF
	
	IF IS_THIS_CURRENT_RHM_MENU(eRHM_ProgressLogic)
		IF GET_CURRENT_RHM_SELECTION() = ciPROG_LOGIC_TEAM1FUTURE
		OR GET_CURRENT_RHM_SELECTION() = ciPROG_LOGIC_TEAM2FUTURE
		OR GET_CURRENT_RHM_SELECTION() = ciPROG_LOGIC_TEAM3FUTURE
		OR GET_CURRENT_RHM_SELECTION() = ciPROG_LOGIC_TEAM4FUTURE
			RETURN "MC_H_OPFO"
		ENDIF
		RETURN "MC_H_FRCMP"
	ENDIF
	
	IF IS_THIS_CURRENT_RHM_MENU(eRHM_ForcedMidpoints)
		RETURN "MC_H_FRCMP"
	ENDIF
	
	IF IS_THIS_CURRENT_RHM_MENU(eRHM_CameraShaking)
		IF GET_CURRENT_RHM_SELECTION() = ciCAMERA_SHAKING_TITLE RETURN "" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciCAMERA_SHAKING_DURATION RETURN "MC_H_SHA_0" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciCAMERA_SHAKING_TYPE RETURN "MC_H_SHA_1" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciCAMERA_SHAKING_INTENSITY RETURN "MC_H_SHA_2" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciCAMERA_SHAKING_SECONDARY_TYPE RETURN "MC_H_SHA_3" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciCAMERA_SHAKING_SECONDARY_INTENSITY RETURN "MC_H_SHA_4" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciCAMERA_SHAKING_SECONDARY_INTERVAL RETURN "MC_H_SHA_5" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciCAMERA_SHAKING_SECONDARY_DURATION RETURN "MC_H_SHA_6" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciCAMERA_SHAKING_WHILE_AIMING RETURN "MC_H_SHA_7" ENDIF
	ENDIF
	
	IF IS_THIS_CURRENT_RHM_MENU(eRHM_RandomNextObjective)
		RETURN "MC_H_RL_RNO"
	ENDIF
	
	IF IS_THIS_CURRENT_RHM_MENU(eRHM_BeastMode)
		IF GET_CURRENT_RHM_SELECTION() = ciBEASTMODE_TITLE RETURN "MC_H_RL_BMOM" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciBEASTMODE_ENABLE_BEAST_MODE RETURN "MC_H_RL_EBM" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciBEASTMODE_INCREASE_HEALTH RETURN "MC_H_RL_BMOM_6" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciBEASTMODE_MAX_HEALTH RETURN "MC_H_RL_BMOM_1" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciBEASTMODE_INCREASE_SPEED RETURN "MC_H_RL_BMOM_7" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciBEASTMODE_SPEED_MOD RETURN "MC_H_RL_BMOM_2" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciBEASTMODE_INCREASE_DAMAGE RETURN "MC_H_RL_BMOM_10" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciBEASTMODE_MELEE_DAMAGE_MOD RETURN "MC_H_RL_BMOM_15" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciBEASTMODE_ENABLE_STEALTH RETURN "MC_H_RL_BMOM_9" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciBEASTMODE_STEALTH_DAMAGE RETURN "MC_H_RL_BMOM_4" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciBEASTMODE_ENABLE_SUPER_JUMP RETURN "MC_H_RL_BMOM_8" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciBEASTMODE_SUPER_JUMP_DAMAGE RETURN "MC_H_RL_BMOM_3" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciBEASTMODE_GIVE_HEALTH_ON_KILL RETURN "MC_H_RL_BMOM_13" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciBEASTMODE_HEALTH_REGEN_RATE RETURN "MC_H_RL_BMOM_11" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciBEASTMODE_MIN_POWER_HEALTH RETURN "MC_H_RL_BMOM_5" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciBEASTMODE_ENABLE_TRACKIFY_ON_BEAST RETURN "MC_H_RL_BMOM_12" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciBEASTMODE_TOGGLE_BEAST_OUTFIT RETURN "MC_H_RL_BCOST" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciBEASTMODE_TOGGLE_BEAST_SOUNDS RETURN "MC_H_RL_BMOM_14" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciBEASTMODE_TOGGLE_FALL_DAMAGE RETURN "MC_H_RL_BMOM_15" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciBEASTMODE_HIDE_BLIP_IN_STEALTH RETURN "MC_H_RL_BMOM_17" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciBEASTMODE_IGNORE_SPAWN_PROTECTION RETURN "MC_H_RL_BMOM_18" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciBEASTMODE_BLOCK_BEAST_HELP_TEXT RETURN "MC_H_RL_BMOM_19" ENDIF
	ENDIF
	
	IF IS_THIS_CURRENT_RHM_MENU(eRHM_TaggingOptions)
		IF GET_CURRENT_RHM_SELECTION() = ciTAGGING_ENABLE RETURN "MC_H_RL_TAG_0" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciTAGGING_CLEAN_UP RETURN "MC_H_RL_TAG_1" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciTAGGING_PASS_RULE RETURN "MC_H_RL_TAG_2" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciTAGGING_HIDE_NOT_ACTIVE RETURN "MC_H_RL_TAG_3" ENDIF
	ENDIF
	
	IF IS_THIS_CURRENT_RHM_MENU(eRHM_Regen)
		IF GET_CURRENT_RHM_SELECTION() = ciREGEN_TITLE RETURN "MC_H_RL_RGNM" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciREGEN_DISTANCE RETURN "MC_H_RGN_1"	 ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciREGEN_RATE RETURN "MC_H_RGN_2" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciREGEN_CAP RETURN "MC_H_RGN_3" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciREGEN_HUD RETURN "MC_H_RGN_4" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciREGEN_FRIENDLY RETURN "MC_H_RGN_5" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciREGEN_DAMAGE RETURN "MC_H_RGN_6" ENDIF
	ENDIF
	
	IF IS_THIS_CURRENT_RHM_MENU(eRHM_Drain)
		IF GET_CURRENT_RHM_SELECTION() = ciDRAIN_TITLE RETURN "MC_H_RL_HDRN" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciDRAIN_ENABLE RETURN "MC_H_HDRN_1" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciDRAIN_NORMAL RETURN "MC_H_HDRN_2" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciDRAIN_REDUCED_1 RETURN "MC_H_HDRN_3_1" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciDRAIN_REDUCED_2 RETURN "MC_H_HDRN_3_2" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciDRAIN_REDUCED_3 RETURN "MC_H_HDRN_3_3" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciDRAIN_REDUCED_4 RETURN "MC_H_HDRN_3_4" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciDRAIN_PLAYER RETURN "MC_H_HDRN_4" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciDRAIN_PED RETURN "MC_H_HDRN_5" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciDRAIN_VEHICLE RETURN "MC_H_HDRN_6" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciDRAIN_OBJECT RETURN "MC_H_HDRN_7" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciDRAIN_HEALTH_DMG_RESTORE RETURN "MC_H_HDRN_8" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciDRAIN_BEAST_ONLY RETURN "MC_H_HDRN_9" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciDRAIN_OOB_ONLY RETURN "MC_H_HDRN_10" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciDRAIN_MISSED_SHOTS RETURN "MC_H_HDRN_11" ENDIF
	ENDIF
	
	IF IS_THIS_CURRENT_RHM_MENU(eRHM_GunRoulette)
		IF GET_CURRENT_RHM_SELECTION() = ciGUNROULETTE_TITLE RETURN "MC_H_RL_GRLT" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciGUNROULETTE_ENABLE RETURN "MC_H_RL_GRLT_0" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciGUNROULETTE_ENABLE_UI_METERS RETURN "MC_H_RL_GRLT_0b" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciGUNROULETTE_YOUR_TEAM_AT_TOP RETURN "MC_H_RL_GRLT_0c" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciGUNROULETTE_WEAPON_TO_FORCE_CATEGORY RETURN "MC_H_RL_GRLT_1" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciGUNROULETTE_WEAPON_TO_FORCE RETURN "MC_H_RL_GRLT_2" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciGUNROULETTE_AMMO_TO_GIVE RETURN "MC_H_RL_GRLT_3" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciGUNROULETTE_RANDOM_OVERRIDE RETURN "MC_H_RL_GRLT_4" ENDIF	
	ENDIF
	
	IF IS_THIS_CURRENT_RHM_MENU(eRHM_LightBar)
		IF GET_CURRENT_RHM_SELECTION() = ciLIGHTBAR_TITLE RETURN "MC_H_RL_LBO" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciLIGHTBAR_ENABLE RETURN "MC_H_RL_LBO_0" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciLIGHTBAR_COLOUR_R RETURN "MC_H_RL_LBO_1" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciLIGHTBAR_COLOUR_G RETURN "MC_H_RL_LBO_2" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciLIGHTBAR_COLOUR_B RETURN "MC_H_RL_LBO_3" ENDIF
	ENDIF
	
	IF IS_THIS_CURRENT_RHM_MENU(eRHM_TurfWar)
		IF GET_CURRENT_RHM_SELECTION() = ciTURFWAR_TITLE RETURN "MC_H_RL_TW" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciTURFWAR_RESET_PROPS_ON_RULE_START RETURN "MC_H_RL_TWEN" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciTURFWAR_GIVE_POINTS_ON_RULE_START RETURN "MC_H_RL_TWPO" ENDIF
	ENDIF
	
	IF IS_THIS_CURRENT_RHM_MENU(eRHM_VehicleSwap)
		IF GET_CURRENT_RHM_SELECTION() = ciVEHICLE_SWAP_TITLE RETURN "MC_H_RL_VS" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciVEHICLE_SWAP_ENABLE RETURN "MC_H_RL_VSE" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciVEHICLE_SWAP_SELECTION RETURN "MC_H_RL_VSS" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciVEHICLE_SWAP_UPDATE_HEALTH RETURN "MC_H_RL_VSU" ENDIF
	ENDIF
	
	IF IS_THIS_CURRENT_RHM_MENU(eRHM_SpecialVehicleWeapons)
		IF GET_CURRENT_RHM_SELECTION() = ciSPECIAL_VEHICLE_WEAPONS_TITLE RETURN "MC_H_RL_SVW0" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciSPECIAL_VEHICLE_WEAPONS_MG RETURN "MC_H_RL_SVW1" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciSPECIAL_VEHICLE_WEAPONS_MG_RELOAD_ON_RULE_CHANGE RETURN "MC_H_RL_SVW2" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciSPECIAL_VEHICLE_WEAPONS_ROCKET RETURN "MC_H_RL_SVW1" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciSPECIAL_VEHICLE_WEAPONS_ROCKET_RELOAD_ON_RULE_CHANGE RETURN "MC_H_RL_SVW2" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciSPECIAL_VEHICLE_WEAPONS_H_ROCKET RETURN "MC_H_RL_SVW1" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciSPECIAL_VEHICLE_WEAPONS_H_ROCKET_RELOAD_ON_RULE_CHANGE RETURN "MC_H_RL_SVW2" ENDIF
	ENDIF
	
	IF IS_THIS_CURRENT_RHM_MENU(eRHM_Overtime)
		IF GET_CURRENT_RHM_SELECTION() = ciOVERTIME_OPTIONS_TITLE RETURN "MC_H_RL_OTRT" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciOVERTIME_OPTIONS_SCORE_HUD RETURN "MC_H_RL_OTSH" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciOVERTIME_OPTIONS_TICK_HUD RETURN "MC_H_RL_OTTH" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciOVERTIME_OPTIONS_RUMBLE_HUD RETURN "MC_H_RL_OTRH" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciOVERTIME_OPTIONS_COUNTS_AS_TURN RETURN "MC_H_RL_OTRC" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciOVERTIME_OPTIONS_PROGRESS_ON_DEATH RETURN "MC_H_RL_OTPR" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciOVERTIME_OPTIONS_HIDE_RUMBLE_SCORE_SHARD RETURN "MC_H_RL_OTRS" ENDIF
	ENDIF
	
	IF IS_THIS_CURRENT_RHM_MENU(eRHM_HideLocationMarker)
		IF GET_CURRENT_RHM_SELECTION() = ciLOCMARKER_TITLE RETURN "MC_H_RL_SLMO" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciLOCMARKER_ENTITYID RETURN "MC_H_SLMO_LE" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciLOCMARKER_HIDE RETURN "MC_H_SLMO_HI" ENDIF
	ENDIF
	
	IF IS_THIS_CURRENT_RHM_MENU(eRHM_VehicleDamageModifier )
		IF GET_CURRENT_RHM_SELECTION() = ciVEHICLE_DAMAGE_MOD_TITLE RETURN "MC_H_RL_VDM" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciVEHICLE_DAMAGE_MOD_DAMAGE_GIVEN RETURN "MC_H_RL_VDMO" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciVEHICLE_DAMAGE_MOD_DAMAGE_RECEIVED RETURN "MC_H_RL_VDMD" ENDIF
	ENDIF
	
	IF IS_THIS_CURRENT_RHM_MENU(eRHM_HUDItems)
		IF GET_CURRENT_RHM_SELECTION() = ciHUD_OFF  RETURN "FMMC_HUDS1D" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciHUD_USE_CUSTOM_STRING RETURN "MC_H_HUDS_CTS" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciHUD_TICKER_OFF RETURN "FMMC_HUDS2D" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciHUD_DELIVERY_TICKER_OFF RETURN "MC_H_HUD_HDLT" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciHUD_HIDE_DEATH_TICKERS RETURN "MC_H_HUD_HDT" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciHUD_RADAR_OFF RETURN "FMMC_HUDSRD" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciHUD_DISPLAY_REMAINING RETURN "MC_H_HUDF_SFH" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciHUD_DISPLAY_REMAINING_LIVES RETURN "MC_H_HUDS_DTL" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciHUD_SHOW_MULTIRULE_TIMER RETURN "MC_H_HUDS_MRL" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciHUD_SHOW_FOR_OTHER_TEAM RETURN "MC_H_HUDS_SOT" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciHUD_SHOW_VEHICLE_POOL RETURN "MC_H_HUDS_SVPR" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciHUD_SHOW_TEAM_HEALTH_BARS RETURN "MC_H_HUDS_SHB" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciHUD_DISPLAY_OTHER_TEAM_LIVES RETURN "MC_H_HUDS_DOT" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciHUD_SHOW_POINTS_HUD RETURN "MC_H_HUDS_SPH" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciHUD_SHOW_POINTS_HUD_AS_METER RETURN "MC_H_HUDS_SPAM" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciHUD_USE_TOTAL_POINTS RETURN "MC_H_HUDS_TSFH" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciHUD_SHOW_FRACTION_HUD RETURN "MC_H_HUDF_SFH" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciHUD_COMBINE_TEAMS_HUD RETURN "MC_H_HUDS_CTH" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciHUD_USE_BLIP_NAME RETURN "MC_H_HUDS_UBN" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciHUD_IGNORE_GPS_BLOCKING RETURN "MC_H_HUDS_IGB" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciHUD_INDIVIDUAL_SCORES  RETURN "MC_H_HUDS_INSC"	 ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciHUD_INDIVIDUAL_SCORES_WHITE_LOWER_CASE RETURN "MC_H_HUDS_WLC" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciHUD_INDIVIDUAL_KILLS RETURN "MC_H_HUDS_INDK" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciHUD_INDIVIDUAL_KILLS_LOWER_CASE RETURN "MC_H_HUDS_KLC" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciHUD_GET_AND_DELIVER_NUMBERS  RETURN "MC_H_HUDS_GADN"	 ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciHUD_HIDE_LIVES_ON_SD  RETURN "MC_H_HUDS_HTLSD"	 ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciHUD_ENABLE_TOGGLE_SHOW_HUD RETURN "MC_H_HUDS_ETSH" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciHUD_ENABLE_START_WITH_EXTRA_HUD RETURN "MC_H_HUDS_SWEH"	 ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciHUD_ENABLE_GPS_TRACKING_METER RETURN "MC_H_HUDS_GTDM" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciHUD_FLASH_DISTANCE_METRE_PERCENTAGE RETURN "MC_H_HUDS_GTDM" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciHUD_BLOCK_TEAM_SCORE_ON_THIS_RULE RETURN "MC_H_HUDS_BTSCU" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciHUD_HIDE_SCORE_HUD RETURN "MC_H_HUDS_HBRS" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciHUD_DONT_COMBINE_SCORES RETURN "MC_H_HUDS_DCS" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciHUD_OBJECT_DESTROYED_HUD RETURN "MC_H_HUDS_SODH" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciHUD_TEAM_REMAINING_FRACTION RETURN "MC_H_HUDS_FRTL" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciHUD_TEAM_HIDE_LAPS_UI RETURN "MC_H_HUDS_BTLU" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciHUD_TEAM_SHOW_THERMITE_REMAINING RETURN "MC_H_HUDS_STHR" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciHUD_TEAM_SHOW_OBJ_CARRY_HUD RETURN "MC_H_HUD_SOCH" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciHUD_TEAM_SHOW_TICKER_ON_RULE_START RETURN "MC_H_HUD_STRS" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciHUD_TEAM_CUSTOM_COMPLETION_TICKER RETURN "MC_H_HUD_CCT" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciHUD_TEAM_CUSTOM_MID_COMPLETION_TICKER RETURN "MC_H_HUD_CMCT" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciHUD_TEAM_CUSTOM_SCORE_HUD_COLOUR RETURN "MC_H_HUD_CSHC" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciHUD_TEAM_FAKE_FULL_CAPACITY RETURN "MC_H_HUD_FFCB" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciHUD_OPTIONS_DO_DIVE_HELP RETURN "MC_H_RL_DDH" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciHUD_OPTIONS_SHOW_CUSTOM_HUD RETURN "MC_H_RL_SCHU" ENDIF
		
	ENDIF
	
	IF IS_THIS_CURRENT_RHM_MENU(eRHM_AllTeamInVehicle)
		IF GET_CURRENT_RHM_SELECTION() = ciALL_TEAM_IN_VEHICLE_ALL_TEAM RETURN "MC_H_RL_ALT1" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciALL_TEAM_IN_VEHICLE_DISABLE_FULL_CHECK RETURN "MC_H_VEH_RUL_FV" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciALL_TEAM_IN_VEHICLE_SEPARATE_VEHICLE RETURN "MC_H_VEH_VEHSEP" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciALL_TEAM_IN_VEHICLE_FORCE_SAME_VEHICLE RETURN "MC_H_VEFSAM" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciALL_TEAM_IN_VEHICLE_PLAYERS_IN_DROPOFF RETURN "MC_H_VEHDLOC" ENDIF		
		
		RETURN "MC_H_VEH_ALL_CT"
	ENDIF
	
	IF IS_THIS_CURRENT_RHM_MENU(eRHM_WantedDetails)
		IF GET_CURRENT_RHM_SELECTION() = ciWANTED_MENU_CHANGE RETURN "MC_H_WTD" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciWANTED_MENU_LOSE RETURN "MC_H_WTDL" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciWANTED_MENU_APPLIED RETURN "MC_H_OP2" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciWANTED_MENU_WANTED_ON_FAIL RETURN "MC_H_WANT_PRF" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciWANTED_MENU_AVAIL RETURN "MC_H_OP4" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciWANTED_MENU_END_OBJ RETURN "FMMC_WANT5H" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciWANTED_MENU_FAKE_LEVEL RETURN "FMMC_WANT7H" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciWANTED_MENU_FAKE_APPLIED RETURN "FMMC_WANT8H" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciWANTED_MENU_AGGRO_WANT RETURN "FMMC_WANT9H" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciWANTED_MENU_FAKE_AGGRO_WANT RETURN "FMMC_WANT10H" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciWANTED_MENU_DELAY_WANTED RETURN "MC_H_WANT_DWL" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciWANTED_MENU_DELAY_WANTED_HUD RETURN "MC_H_WANT_DWLH" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciWANTED_MENU_SKIP_WANTED_DELAY_IF_WANTED RETURN "MC_H_WANT_SWDW" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciWANTED_MENU_TIME_TO_LOSE_COPS RETURN "MC_H_WANT_TLC" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciWANTED_MENU_TIME_TO_LOSE_COPS_MODE RETURN "MC_H_WANT_TLCM" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciWANTED_MENU_FAIL_ON_WANTED RETURN "FMMC_WANT_FAILH" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciWANTED_MENU_HELI_RESPONSE RETURN "MC_H_WANT_HLR" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciWANTED_MENU_BLIP_SECURITY RETURN "FMMC_H_WANT_BSW" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciWANTED_MENU_CHECK_FRIENDLY_TEAMS RETURN "FMMC_H_WANT_CFT" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciWANTED_MENU_WANTED_LEVEL_REQUIRED RETURN "MC_H_SS_RL_5B" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciWANTED_MENU_BLOCK_WANTED_CONE_CLEAR RETURN "MC_H_WANT_BCRC" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciWANTED_MENU_BLOCK_COP_HELI_LANDING RETURN "MC_H_WANT_BHLD" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciWANTED_MENU_FORCE_SEARCH_NO_AMBIENT_COPS RETURN "MC_H_WANT_FSWC" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciWANTED_MENU_SEARCH_WHEN_CANT_LOSE RETURN "MC_H_WANT_SWCL" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciWANTED_MENU_IGNORED_WHEN_WANTED RETURN "MC_H_WANT_IGWW" ENDIF
	ENDIF
	
	IF IS_THIS_CURRENT_RHM_MENU(eRHM_VehicleRuleWarp)
		RETURN "MC_H_PE_WRPED"
	ENDIF
	
	IF IS_THIS_CURRENT_RHM_MENU(eRHM_PedRuleWarp)
		RETURN "MC_H_PE_WRPED"
	ENDIF
		
	IF IS_THIS_CURRENT_RHM_MENU(eRHM_AltitudeBarSystemMenuOptions)
		RETURN GET_ALTITUDE_SYSTEM_BAR_OPTIONS_MENU_HELP_STRING(GET_CURRENT_RHM_SELECTION())
	ENDIF
	
	IF IS_THIS_CURRENT_RHM_MENU(eRHM_PedMenuOptions)
		RETURN GET_RIGHT_SIDE_PED_MENU_DESCRIPTIONS()
	ENDIF
	
	IF IS_THIS_CURRENT_RHM_MENU(eRHM_ObjectMenuOptions)
		RETURN GET_RIGHT_SIDE_OBJ_MENU_DESCRIPTIONS()
	ENDIF
	
	IF IS_THIS_CURRENT_RHM_MENU(eRHM_LocationMenuOptions)
		RETURN GET_RIGHT_SIDE_LOC_MENU_DESCRIPTIONS()
	ENDIF
	
	IF IS_THIS_CURRENT_RHM_MENU(eRHM_ObjRuleWarp)
		RETURN "MC_H_OB_WRPED"
	ENDIF
	
	IF IS_THIS_CURRENT_RHM_MENU(eRHM_LocRuleWarp)
		RETURN "MC_H_LO_WRPED"
	ENDIF
	
	IF IS_THIS_CURRENT_RHM_MENU(eRHM_ObjText)
		IF GET_CURRENT_RHM_SELECTION() = ciOBJ_TEXT_PRIM  RETURN "MC_H_TXT0" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciOBJ_TEXT_SEC RETURN "MC_H_TXT1" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciOBJ_TEXT_TERT  RETURN "MC_H_TXT2" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciOBJ_TEXT_BLIP RETURN "MC_H_TXT3" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciOBJ_TEXT_SING RETURN "MC_H_TXT4" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciOBJ_TEXT_PLURAL  RETURN "MC_H_TXT5" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciOBJ_TEXT_BOUNDS_FAIL  RETURN "MC_H_TXT7" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciOBJ_TEXT_VS_SHARD  RETURN "MC_H_TXT_VSS" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciOBJ_TEXT_WAIT  RETURN "MC_RL_KEY_CWT" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciOBJ_TEXT_OVERRIDE_TEAM RETURN "MC_H_TXT8" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciOBJ_TEXT_VEHICLE_HUD RETURN "MC_RL_TXT9" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciOBJ_TEXT_COPY RETURN "MC_H_TXT_UAR" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciOBJ_TEXT_ALTERNATIVE_VEH_GO_TO RETURN "MC_H_TXT_UAGT" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciOBJ_TEXT_ALTERNATIVE_TEXT_IN_MOC RETURN "MC_H_TXT_SAIM" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciOBJ_TEXT_SECONDARY_TEXT_IN_ORBITAL_CANNON RETURN "MC_H_TXT_SIOC" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciOBJ_TEXT_USE_ALT_TEXT_IF_LAST_ALIVE RETURN "MC_H_LST_ALI" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciOBJ_TEXT_USE_TAG_TEAM_BOUNDS_SHARD RETURN "MC_H_TAG_SHD" ENDIF		
		IF GET_CURRENT_RHM_SELECTION() = ciOBJ_TEXT_MISSIONVAR_OVERRIDE_PRIMARY_TEXT RETURN "MC_H_MVAR_TO" ENDIF		
		IF GET_CURRENT_RHM_SELECTION() = ciOBJ_TEXT_OVERRIDE_TEXT_ON_OUTFIT_SWAP RETURN "FMMC_UAT_OS" ENDIF		
		IF GET_CURRENT_RHM_SELECTION() = ciOBJ_TEXT_CACHE_ALT_FOR_AIRLOCK RETURN "MC_H_CAT_AL" ENDIF		
	ENDIF
	
	IF IS_THIS_CURRENT_RHM_MENU(eRHM_IncrementalFailTimer)
		IF GET_CURRENT_RHM_SELECTION() = ciINC_TIMER_FAIL_TIME_START  RETURN "MCH_RL_RFTI_1" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciINC_TIMER_FAIL_PLACED_PED_THRESHHOLD  RETURN "MCH_RL_RFTI_2" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciINC_TIMER_FAIL_PLACED_PED_MULTIPLIER  RETURN "MCH_RL_RFTI_3" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciINC_TIMER_FAIL_PASS_INSTEAD  RETURN "MCH_RL_RFTI_4" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciINC_TIMER_FAIL_DEPLETE_INSTEAD_OF_FILL  RETURN "MCH_RL_RFTI_5" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciINC_TIMER_FAIL_ENABLE_CHOPPY_MODE  RETURN "MCH_RL_RFTI_6" ENDIF
	ENDIF
	
	IF IS_THIS_CURRENT_RHM_MENU(eRHM_SpeedBarOptions)
		IF GET_CURRENT_RHM_SELECTION() = ciSPEEDBAR_MIN_SPEED_METER_MAX 	RETURN "MC_H_RL_MSM" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciSPEEDBAR_MIN_SPEED_LAP_EXTA	RETURN "MC_H_RL_TPL" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciSPEEDBAR_ALT_STRING 			RETURN "FMMC_RLH_ETTS" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciSPEEDBAR_NOT_STEALTHED 		RETURN "MC_H_RL_SBIS" ENDIF
	ENDIF
	
	IF IS_THIS_CURRENT_RHM_MENU(eRHM_NightvisionOptions)
		IF GET_CURRENT_RHM_SELECTION() = ciNIGHTVISION_ENABLE_NV RETURN "MC_H_NV" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciNIGHTVISION_NV_TOGGLE RETURN "MC_H_RL_ANVT" ENDIF 
		IF GET_CURRENT_RHM_SELECTION() = ciNIGHTVISION_NV_RANGE RETURN "MC_H_RL_NVOR" ENDIF 
	ENDIF
	
	IF IS_THIS_CURRENT_RHM_MENU(eRHM_ThermalOptions)
		IF GET_CURRENT_RHM_SELECTION() = ciTHERMAL_TOGGLE RETURN "MC_H_RL_EVAT" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciTHERMAL_START_DIST RETURN "MC_H_RL_TVSD" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciTHERMAL_END_DIST RETURN "MC_H_RL_TVED" ENDIF 
		IF GET_CURRENT_RHM_SELECTION() = ciTHERMAL_FORCED RETURN "MC_H_RL_FTV" ENDIF 
		IF GET_CURRENT_RHM_SELECTION() = ciTHERMAL_CLEAR_SWAP RETURN "MC_H_RL_TSC" ENDIF
	ENDIF
	
	IF IS_THIS_CURRENT_RHM_MENU(eRHM_VisualOptions)
		IF GET_CURRENT_RHM_SELECTION() = ciVISUALS_NIGHTVISION RETURN "MC_H_RL_NVO" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciVISUALS_THERMAL RETURN "MC_H_RL_TVO" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciVISUALS_ALT_SFX RETURN "MC_H_RL_THNVA" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciVISUALS_CLEAR_VISUAL_AIDS_ON_DEATH RETURN "MC_H_RL_CVAOD" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciVISUALS_RED_FILTER_CAPTURE RETURN "MC_H_RL_RFC" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciVISUALS_SHOW_FILTER_CAPTURED RETURN "MC_H_RL_RFWC" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciVISUALS_FILTERS RETURN "MC_H_RL_SFM" ENDIF
	ENDIF
	
	IF IS_THIS_CURRENT_RHM_MENU(eRHM_DisableControlsOptions)
		IF GET_CURRENT_RHM_SELECTION() = ciDISCTRL_INPUT_JUMP				RETURN "" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciDISCTRL_JUMPING				RETURN "" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciDISCTRL_CLIMBING				RETURN "" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciDISCTRL_COMBAT_ROLL			RETURN "" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciDISCTRL_CONTROL				RETURN "MC_H_DPC" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciDISCTRL_CONTROL_TIMER			RETURN "" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciDISCTRL_VEHICLE_CONTROLS		RETURN "MC_H_DWIV" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciDISCTRL_SPECIFIC_VEH_CONTROL	RETURN "MC_H_DSIV" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciDISCTRL_HANDBRAKE				RETURN "" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciDISCTRL_OPP_BOOST				RETURN "" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciDISCTRL_OPP_WINGS				RETURN "" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciDISCTRL_DEL_FLIGHT				RETURN "FMMC_RL_H_DEFM" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciDISCTRL_DEL_HOVER				RETURN "FMMC_RL_H_EFAH" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciDISCTRL_DEL_LAND				RETURN "FMMC_RL_H_DTRI" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciDISCTRL_VEH_JUMP				RETURN "" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciDISCTRL_VEH_ENTRY				RETURN "MC_RL_H_DVEHE" ENDIF
	ENDIF
	
	IF IS_THIS_CURRENT_RHM_MENU(eRHM_ControlsOptions)
		IF GET_CURRENT_RHM_SELECTION() = ciCONTROLS_DISABLE_CONTROLS				RETURN "MC_H_RL_DCO" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciCONTROLS_CANNOT_LEAVE_VEHICLE			RETURN "" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciCONTROLS_ENABLE_WEAPONS_WHEN_STOPPED		RETURN "MC_H_RL_STWP" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciCONTROLS_BLOCK_SNACKS					RETURN "" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciCONTROLS_FORCE_AIM_LOCK					RETURN "MC_H_RL_ELOA" ENDIF	
		IF GET_CURRENT_RHM_SELECTION() = ciCONTROLS_SLOW_DOWN_PLAYER				RETURN "MC_H_RL_SLW" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciCONTROLS_BLOCK_WEAPONS_ON_CAPTURE_VEH	RETURN "MC_H_RL_CAPWB" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciCONTROLS_SET_DRUNK_LEVEL					RETURN "" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciCONTROLS_MOVE_SPEED_OVERRIDE				RETURN "" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciCONTROLS_BLOCK_MANUAL_RESPAWN			RETURN "MC_H_SS_RL_19b" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciCONTROLS_ALLOW_VEH_MOVEMENT_NO_CONTROL 	RETURN "MC_H_ALW_MVD" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciCONTROLS_RESTRICT_LADDERS 				RETURN "" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciCONTROLS_DISABLE_TURRET_TOWER_ACCESS 	RETURN "MC_H_RL_DTTA" ENDIF		
		IF GET_CURRENT_RHM_SELECTION() = ciCONTROLS_DISABLE_HOMING_WEAPON_LOCKON 	RETURN "MC_	H_RL_DHWL" ENDIF		
		IF GET_CURRENT_RHM_SELECTION() = ciCONTROLS_DISABLE_TAKEDOWNS 				RETURN "MC_H_RL_SMTT" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciCONTROLS_REACT_TO_SMOKE					RETURN "MC_H_RL_RTS" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciCONTROLS_PLAYERS_WAKE_UP					RETURN "MC_H_RL_PWU" ENDIF
	ENDIF
	
	IF IS_THIS_CURRENT_RHM_MENU(eRHM_HUDOptions)
		IF GET_CURRENT_RHM_SELECTION() = ciHUDOP_BLIPS					RETURN "" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciHUDOP_OBJ_TEXT					RETURN "" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciHUDOP_SHARDS					RETURN "" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciHUDOP_SHOW_HEALTH_BAR			RETURN "MC_H_VEH_HBR" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciHUDOP_STAMINA_BAR				RETURN "MC_H_RL_SCSB" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciHUDOP_CAPTURE_BARS				RETURN "" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciHUDOP_CAMERA_HELP				RETURN "MC_H_RL_SCH" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciHUDOP_HIDE_HEALTH_BAR			RETURN "MC_H_RL_HHB" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciHUDOP_ENABLE_FULL_GPS			RETURN "" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciHUDOP_HIDE_LOC_MARKER			RETURN "" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciHUDOP_SHOW_HACKING_PROGRESS	RETURN "" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciHUDOP_GREEN_TEAM_HUD			RETURN "MC_H_RL_MTHG" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciHUDOP_HIDE_RESPAWN_BAR			RETURN "" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciHUDOP_SHOW_SPOTTED_HELPTEXT	RETURN "MC_H_RL_SPSHT" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciHUDOP_FORCE_MINIMAP_INTERIOR	RETURN "MC_H_RL_FMMI" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciHUDOP_HIDE_PLAYER_RADAR		RETURN "MC_H_RL_HPRD" ENDIF
	ENDIF
	
	IF IS_THIS_CURRENT_RHM_MENU(eRHM_BlipOptions)
		IF GET_CURRENT_RHM_SELECTION() = ciBLIPOP_BLIP_ALL_PLAYERS				RETURN "" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciBLIPOP_ALWAYS_BLIP_DELIVERY			RETURN "MC_H_RL_ABDP" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciBLIPOP_SHOW_IDLE_BLIP					RETURN "MC_H_RL_SIBH" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciBLIPOP_IDLE_BLIP_TIMER					RETURN "MC_H_RL_IBT" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciBLIPOP_BLIP_PLAYERS_AT_CP				RETURN "MC_H_RL_BONL" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciBLIPOP_BLIP_COLLECT_PLAYERS			RETURN "MC_H_RL_BOC" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciBLIPOP_BLIP_PLAYERS_ON_LOCATE_TIME		RETURN "MC_H_RL_BONLT" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciBLIPOP_OVERRIDE_BLIP_RANGE				RETURN "MC_H_RL_OOBR" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciBLIPOP_ENABLE_SKULL_BLIPS				RETURN "MC_H_RL_TOSB" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciBLIPOP_PLAYER_LOCATE_BLIP				RETURN "MC_H_RL_SPRB" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciBLIPOP_BLIP_YACHT						RETURN "" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciBLIPOP_UNBLIP_OTHER_ENTITY				RETURN "MC_H_UOE" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciBLIPOP_OVERRIDE_PED_BLIP_RANGE			RETURN "MC_H_RL_OPBR" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciBLIPOP_ALWAYS_SHOW_1_ENEMY_BLIP		RETURN "MC_H_RL_ALWBL" ENDIF
	ENDIF
	
	IF IS_THIS_CURRENT_RHM_MENU(eRHM_ShardOptions)
		IF GET_CURRENT_RHM_SELECTION() = ciSHARDOP_SHARD_TEXT			RETURN "" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciSHARDOP_WRONG_WAY_SHARD	RETURN "" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciSHARDOP_TEAM_REMINDER		RETURN "" ENDIF
	ENDIF
	
	IF IS_THIS_CURRENT_RHM_MENU(eRHM_ObjTextOptions)
		IF GET_CURRENT_RHM_SELECTION() = ciOBJTOP_VEHICLE_ENTRY_TEXT		RETURN "" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciOBJTOP_DIALOGUE_BLOCKER		RETURN "" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciOBJTOP_DIALOGUE_BLOCKER_MID	RETURN "" ENDIF
	ENDIF
	
	IF IS_THIS_CURRENT_RHM_MENU(eRHM_GameplayOptions)
		IF GET_CURRENT_RHM_SELECTION() = ciGAMEPLAY_COMBAT									RETURN "MC_H_RL_CMBTO" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciGAMEPLAY_HOLDING_EFFECTS							RETURN "MC_H_RL_HEO" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciGAMEPLAY_PLAYER_ABILITY_OPTIONS					RETURN "MC_H_RL_PAO" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciGAMEPLAY_LAP_PASSED_ON_RULE						RETURN "MC_H_RL_PPL" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciGAMEPLAY_TIME_PENALTY_DEATH						RETURN "MC_H_RL_TPOD" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciGAMEPLAY_TIME_PENALTY_THRESH						RETURN "MC_H_RL_TPTH" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciGAMEPLAY_RESET_PLAYER_LIVES						RETURN "MC_H_RL_RPL" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciGAMEPLAY_REMOVE_REBREATHER						RETURN "FMMC_RLH_RROR" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciGAMEPLAY_SEND_EMAIL								RETURN "MC_H_RL_OREMAIL" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciGAMEPLAY_FORCED_STEALTH							RETURN "" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciGAMEPLAY_POST_CUTSCENE_COVER						RETURN "" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciGAMEPLAY_DONT_START_TIMER_UNTIL_SCORED			RETURN "MC_H_DSTUS" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciGAMEPLAY_DONT_START_TIMER_UNTIL_SCORED_POINTS	RETURN "MC_H_DSTUSP" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciGAMEPLAY_SET_AS_A_HOLDING_RULE 					RETURN "MC_H_SRAHR" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciGAMEPLAY_DONT_FAIL_MULTIRULE						RETURN "MC_H_DFMR" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciGAMEPLAY_CARNAGE_BAR_ACTIVE						RETURN "MC_H_RCRNBAR" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciGAMEPLAY_DESTROYED_CASINO_IPL_DOORS				RETURN "MC_H_IPLDCAS" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciGAMEPLAY_CASINO_AMBIENT_PED_SHOCKED_DIALOGUE		RETURN "MC_H_CASAPSD" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciGAMEPLAY_STOP_CCTV_CAMERAS_TRIGGERING			RETURN "MC_H_STPCCTV" ENDIF		
		IF GET_CURRENT_RHM_SELECTION() = ciGAMEPLAY_REVALIDATE_CUTSCENES_AFTER_THIS_RULE	RETURN "MC_H_REVCRS"  ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciGAMEPLAY_CATCH_FIRE_DISTANCE						RETURN "MC_H_CFDIST"  ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciGAMEPLAY_CATCH_FIRE_DELAY						RETURN "MC_H_CFCD"    ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciGAMEPLAY_STOP_FIRE_DELAY							RETURN "MC_H_CFSD"    ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciGAMEPLAY_SET_TEAM_AS_FIREPROOF					RETURN "MC_H_STAF"    ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciGAMEPLAY_TAKEDOWN_DEFENSE_MODIFIER				RETURN "MC_H_TTDM"    ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciGAMEPLAY_LIGHTNING_TIME_MIN						RETURN "MC_H_LSMIN"   ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciGAMEPLAY_LIGHTNING_TIME_MAX						RETURN "MC_H_LSMAX"   ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciGAMEPLAY_LIGHTNING_BLIP_TIME						RETURN "MC_H_LSBT"    ENDIF
	ENDIF
	
	IF IS_THIS_CURRENT_RHM_MENU(eRHM_VehicleMenuOptions)
		IF GET_CURRENT_RHM_SELECTION() = ciVEH_MENU_RIGHT_HAND_PROGRESS_RULE_ALL_TEAM_IN_VEH	RETURN "MC_H_VEHMENU_11"	ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciVEH_MENU_RIGHT_HAND_PROGRESS_HELP_TEXT				RETURN "MC_H_VEHMENU_PHT"	ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciVEH_MENU_RIGHT_HAND_LEAVE_VEH_ENTIRE_TEAM			RETURN "MC_H_VE_ALLE"		ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciVEH_MENU_RIGHT_HAND_ALLOW_REMOTE_EXPLOSIVE			RETURN "MC_H_VE_AREV"		ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciVEH_MENU_RIGHT_HAND_DO_VEH_DRIVEBY_SEQUENCE			RETURN "MC_H_VE_VDBS"		ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciVEH_MENU_RIGHT_HAND_CALL_CACHED_VEHICLE				RETURN "MC_H_VE_WLUV"		ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciVEH_MENU_RIGHT_HAND_ALLOW_REAR_ENTRY					RETURN "MC_H_VE_ARE"		ENDIF
	ENDIF
	
	IF IS_THIS_CURRENT_RHM_MENU(eRHM_CombatOptions)
		IF GET_CURRENT_RHM_SELECTION() = ciCOMBAT_REMOVE_WEAPONS					RETURN "MC_H_RL_RWEP" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciCOMBAT_START_UNARMED					RETURN "MC_H_RL_SUA" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciCOMBAT_MELEE_DAMAGE_MOD				RETURN "" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciCOMBAT_MELEE_WEAPON_MOD				RETURN "" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciCOMBAT_WEAPON_LOADOUT					RETURN "MC_H_RL_WPLO" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciCOMBAT_ARMOUR							RETURN "FMMC_H_RL_ARMR" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciCOMBAT_TURN_ON_BIKE_COMBAT				RETURN "FMMC_H_RL_TOBC" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciCOMBAT_MINIGUN_DAMAGE					RETURN "" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciCOMBAT_TEAM_ELIMINATION_POINTS			RETURN "MC_H_RL_PFTE" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciCOMBAT_KILL_PLAYER_BY_TOUCH			RETURN "FMMC_H_RL_KPBT" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciCOMBAT_EXPLODE_NEARBY_ROCKETS			RETURN "FMMC_H_RL_EPRD" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciCOMBAT_EXPLODE_NEARBY_ROCKETS_CHANCE	RETURN "FMMC_H_RL_EPRC" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciCOMBAT_DISABLE_MELEE					RETURN "MC_H_RL_DMEL" ENDIF		
	ENDIF
	
	IF IS_THIS_CURRENT_RHM_MENU(eRHM_HoldingEffectsOptions)
		IF GET_CURRENT_RHM_SELECTION() = ciHOLDING_DisableSprint			RETURN "MC_H_HEO_DS"   ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciHOLDING_DisableJumping			RETURN "MC_H_HEO_DJ"   ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciHOLDING_DisableWeapons			RETURN "MC_H_HEO_DW"   ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciHOLDING_SlowDownPlayer			RETURN "MC_H_HEO_SDP"  ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciHOLDING_SlowDownVehicle			RETURN "MC_H_HEO_SDV"  ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciHOLDING_SlowVehicleDownDrag		RETURN "MC_H_HEO_SDVA" ENDIF
	ENDIF
	
	IF IS_THIS_CURRENT_RHM_MENU(eRHM_PlayerAbilityOptions)
		IF GET_CURRENT_RHM_SELECTION() = ciABILITY_EnableAirstrike			RETURN "MC_H_RL_AEAS" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciABILITY_EnableHeavyLoadout		RETURN "MC_H_RL_AEHL" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciABILITY_EnableHeliBackup			RETURN "MC_H_RL_AEHB" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciABILITY_EnableReconDrone			RETURN "MC_H_RL_AERD" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciABILITY_EnableSupportSniper		RETURN "MC_H_RL_AESS" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciABILITY_EnableVehicleRepair		RETURN "MC_H_RL_AEVR" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciABILITY_EnableRideAlong			RETURN "MC_H_RL_AERA" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciABILITY_EnableAmbush				RETURN "MC_H_RL_AEAB" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciABILITY_EnableDistraction		RETURN "MC_H_RL_AEDS" ENDIF
	ENDIF
	
	IF IS_THIS_CURRENT_RHM_MENU(eRHM_RespawnOptions)
		IF GET_CURRENT_RHM_SELECTION() = ciRESPAWN_RESPAWN_IN_LAST_VEHICLE 					RETURN "" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciRESPAWN_RESPAWN_ON_END							RETURN "" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciRESPAWN_USE_CHECKPOINT							RETURN "" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciRESPAWN_USE_YELLOW_RESTART_EFFECTS 				RETURN "" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciRESPAWN_HIDE_HUD_DURING_RESTART 					RETURN "" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciRESPAWN_PAUSE_ON_PLAYER_DURING_RESTART 			RETURN "" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciRESPAWN_AT_START_POINT 							RETURN "" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciRESPAWN_IN_NEARBY_WATER_ON_FOOT_ONLY				RETURN "MC_H_RL_RNW"   ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciRESPAWN_IN_NEARBY_WATER_VEHICLE_ONLY 			RETURN "MC_H_RL_RNWVO" ENDIF	
		IF GET_CURRENT_RHM_SELECTION() = ciRESPAWN_NEAR_OBJECTIVE 							RETURN "MC_H_RL_RNOBJ" ENDIF	
		IF GET_CURRENT_RHM_SELECTION() = ciRESPAWN_IGNORE_START_POINT_FOR_THIS_RULE 		RETURN "MC_H_RL_ISPS"  ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciRESPAWN_IN_NEARBY_WATER_VEHICLE_WITH_DISTANCE 	RETURN "MC_H_RL_RNWVD" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciRESPAWN_RESPAWN_ALL_PLAYERS_ON_KILL			 	RETURN "MC_H_RL_RTOK"  ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciRESPAWN_PLAYER_DEATH_USES_WHITEOUT_FADING		RETURN "MC_H_RL_PDUWF" ENDIF
	ENDIF
	
	IF IS_THIS_CURRENT_RHM_MENU(eRHM_PackSpecificOptions)
		RETURN GET_PACK_SPECIFIC_OPTIONS_DESCRIPTION(GET_CURRENT_RHM_SELECTION())
	ENDIF
	
	IF IS_THIS_CURRENT_RHM_MENU(eRHM_CasinoHeistOptions)
		RETURN GET_CASINO_HEIST_OPTIONS_DESCRIPTION(GET_CURRENT_RHM_SELECTION())
	ENDIF
	
	IF IS_THIS_CURRENT_RHM_MENU(eRHM_WorldMenuOptions)
		IF GET_CURRENT_RHM_SELECTION() = ciWORLD_MENU_RIGHT_HAND_ENABLE_CASINO_PARTY_PEDS		RETURN "MC_H_RLNT_WOPP" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciWORLD_MENU_RIGHT_HAND_ENABLE_HIDE_CASINO_PARTY_PEDS_DISABLE		RETURN "MC_H_RLNT_WHPP" ENDIF		
	ENDIF
	
	IF IS_THIS_CURRENT_RHM_MENU(eRHM_ForcedHintCamOptions)
		IF GET_CURRENT_RHM_SELECTION() = ciFORCEHINT_MENU_RIGHT_HAND_ENTITY_TYPE	RETURN "MC_H_DT_FHCT" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciFORCEHINT_MENU_RIGHT_HAND_ENTITY_ID		RETURN "MC_H_DT_FHCI" ENDIF		
		IF GET_CURRENT_RHM_SELECTION() = ciFORCEHINT_MENU_RIGHT_HAND_DURATION		RETURN "MC_H_DT_FHCD" ENDIF		
	ENDIF
		
	IF IS_THIS_CURRENT_RHM_MENU(eRHM_RoamingSpectatorOptions)
		IF GET_CURRENT_RHM_SELECTION() = ciRule_OPTIONS_ROAMING_SPECTATOR_TITLE						RETURN "MC_H_ROAMSPEC_R" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciRule_OPTIONS_ROAMING_SPECTATOR_DISABLE_DRONE				RETURN "MC_H_ROAMS_R_0" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciRule_OPTIONS_ROAMING_SPECTATOR_DISABLE_RCCAR				RETURN "MC_H_ROAMS_R_1" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciRule_OPTIONS_ROAMING_SPECTATOR_DISABLE_TRAPCAM			RETURN "MC_H_ROAMS_R_2" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciRule_OPTIONS_ROAMING_SPECTATOR_DISABLE_TURRETCAM			RETURN "MC_H_ROAMS_R_3" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciRule_OPTIONS_ROAMING_SPECTATOR_OBJECTIVE_TEXT_OVERRIDE	RETURN "MC_H_ROAMS_R_4" ENDIF
		IF GET_CURRENT_RHM_SELECTION() = ciRule_OPTIONS_ROAMING_SPECTATOR_INTRO_CAM_BOX				RETURN "MC_H_ROAMS_R_5" ENDIF
	ENDIF
	
	SWITCH GET_CURRENT_RHM_MENU()
		CASE eRHM_AlternateVariableRuleOptions RETURN GET_ALTERNATE_VARIABLES_OPTIONS_DESCRIPTION(GET_CURRENT_RHM_SELECTION())
		#IF FEATURE_DLC_1_2022
		CASE eRHM_OnRuleStartOptions		RETURN GET_ON_RULE_START_OPTIONS_MENU_DESCRIPTION(GET_CURRENT_RHM_SELECTION())
		CASE eRHM_OnRuleStartLives			RETURN GET_ON_RULE_LIVES_OPTIONS_MENU_DESCRIPTION(GET_CURRENT_RHM_SELECTION())
		CASE eRHM_OnRuleStartTime			RETURN GET_ON_RULE_TIME_OPTIONS_MENU_DESCRIPTION(GET_CURRENT_RHM_SELECTION())
		CASE eRHM_InteractableMenuOptions	RETURN GET_INTERACTABLE_OPTIONS_MENU_DESCRIPTION(GET_CURRENT_RHM_SELECTION())
		CASE eRHM_LightingOptions			RETURN GET_LIGHTING_OPTIONS_MENU_DESCRIPTION(GET_CURRENT_RHM_SELECTION())
		CASE eRHM_CachedVehicleOptions		RETURN GET_CACHED_VEHICLE_OPTIONS_MENU_DESCRIPTION(GET_CURRENT_RHM_SELECTION())
		#ENDIF
		CASE eRHM_OnRuleStartRelationshipGroups		RETURN GET_RELATIONSHIP_ON_RULE_MENU_DESCRIPTION(GET_CURRENT_RHM_SELECTION())
		CASE eRHM_PlayerModelSwap 			RETURN GET_PLAYER_MODEL_SWAP_OPTIONS_MENU_DESCRIPTION(GET_CURRENT_RHM_SELECTION())
	ENDSWITCH
	
	IF GET_CURRENT_RHM_SELECTION() = FMMC_SUB_OPTIONS_RULE_TYPE
		IF g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule = ciSELECTION_COLLECT_PED RETURN "MC_H_COLP" ENDIF
		IF g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule = ciSELECTION_KILL_PED RETURN "MC_H_KILP" ENDIF
		IF g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule = ciSELECTION_DAMAGE_PED RETURN "MC_H_DAMP" ENDIF		
		IF g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule = ciSELECTION_DAMAGE_VEHICLE RETURN "MC_H_DAMV" ENDIF
		IF g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule = ciSELECTION_DAMAGE_OBJECT RETURN "MC_H_DAMO" ENDIF
		IF g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule = ciSELECTION_PROTECT_PED RETURN "MC_H_PROP" ENDIF
		IF g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule = ciSELECTION_GO_TO_PED RETURN "MC_H_GOP" ENDIF
		IF g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule = ciSELECTION_CAPTURE_PED RETURN "MC_H_CONP" ENDIF
		IF g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule = ciSELECTION_CROWD_CONTROL RETURN "MC_H_CRCN" ENDIF
		IF g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule = ciSELECTION_CHARM_PED RETURN "MC_H_CHP" ENDIF
		IF g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule = ciSELECTION_COLLECT_VEHICLE RETURN "MC_H_COLV" ENDIF
		IF g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule = ciSELECTION_KILL_VEHICLE RETURN "MC_H_KILV" ENDIF
		IF g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule = ciSELECTION_PROTECT_VEHICLE RETURN "MC_H_PROV" ENDIF
		IF g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule = ciSELECTION_GO_TO_VEHICLE RETURN "MC_H_GOV" ENDIF
		IF g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule = ciSELECTION_CAPTURE_VEHICLE RETURN "MC_H_CONV" ENDIF
		IF g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule = ciSELECTION_COLLECT_OBJECT RETURN "MC_H_COLO"	 ENDIF
		IF g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule = ciSELECTION_KILL_OBJECT RETURN "MC_H_KILO" ENDIF
		IF g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule = ciSELECTION_PROTECT_OBJECT RETURN "MC_H_PROO" ENDIF
		IF g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule = ciSELECTION_GO_TO_OBJECT RETURN "MC_H_GOVO" ENDIF
		IF g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule = ciSELECTION_CAPTURE_OBJECT		 RETURN "MC_H_CONO" ENDIF
		IF g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule = ciSELECTION_LOCATION_GO_TO RETURN "MC_H_GOTO2" ENDIF
		IF g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule = ciSELECTION_LOCATION_CAPTURE			 RETURN "MC_H_LOCC" ENDIF
		IF g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule = ciSELECTION_PLAYER_RULE_KILL_ANY RETURN "MC_H_KPA" ENDIF
		IF g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule = ciSELECTION_PLAYER_RULE_KILL_TEAM0 RETURN "MC_H_KPT1" ENDIF
		IF g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule = ciSELECTION_PLAYER_RULE_KILL_TEAM1		 RETURN "MC_H_KPT2" ENDIF
		IF g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule = ciSELECTION_PLAYER_RULE_KILL_TEAM2 RETURN "MC_H_KPT3" ENDIF
		IF g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule = ciSELECTION_PLAYER_RULE_KILL_TEAM3			 RETURN "MC_H_KPT4" ENDIF
		IF g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule = ciSELECTION_PLAYER_RULE_GET_MASKS RETURN "MC_H_KPT5" ENDIF
		IF g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule = ciSELECTION_LEAVE_PED					 	RETURN "MC_H_PED38" ENDIF
		IF g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule = ciSELECTION_LEAVE_VEHICLE					RETURN "MC_H_VEH26" ENDIF
		IF g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule = ciSELECTION_LEAVE_OBJECT						RETURN "MC_H_OBJ16" ENDIF
		IF g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule = ciSELECTION_PED_GOTO_LOCATION RETURN "MC_H_PGOTL" ENDIF
	ENDIF
		
	IF IS_THIS_CURRENT_RHM_MENU(eRHM_PedAssGotoPoolAssignment)
		IF GET_CURRENT_RHM_SELECTION() % ciPED_MENU_RIGHT_HAND_PED_ASS_POOL_ASSIGNMENT_STARTING_GOTO = 0
			RETURN "MCH_PE_SGPO_B"	
		ELIF GET_CURRENT_RHM_SELECTION() % ciPED_MENU_RIGHT_HAND_PED_ASS_POOL_ASSIGNMENT_INDEX = 0
			RETURN "MCH_PE_SGPO_A"		
		ENDIF
	ENDIF
			
	IF IS_THIS_CURRENT_RHM_MENU(eRHM_DroneAssGotoPoolAssignment)
		IF GET_CURRENT_RHM_SELECTION() % ciOBJ_MENU_RIGHT_HAND_DRONE_ASS_POOL_ASSIGNMENT_STARTING_GOTO = 0
			RETURN "MCH_OB_SGPO_B"	
		ELIF GET_CURRENT_RHM_SELECTION() % ciOBJ_MENU_RIGHT_HAND_DRONE_ASS_POOL_ASSIGNMENT_INDEX = 0
			RETURN "MCH_OB_SGPO_A"		
		ENDIF
	ENDIF
	
	IF GET_CURRENT_RHM_SELECTION() = FMMC_SUB_OPTIONS_SELECTED_ENTITES
	
		IF g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule = ciSELECTION_PLAYER_RULE_LOOT_THRESHOLD
			IF IS_BIT_SET(g_FMMC_STRUCT.sPlayerRuleData[g_CreatorsSelDetails.iPlayerRuleMapping[g_CreatorsSelDetails.iSelectedTeam][g_CreatorsSelDetails.iRow]].iBitset[g_CreatorsSelDetails.iSelectedTeam], ciPLAYER_RULE_BITSET_UseBagCapacityForLootThreshold)
				RETURN "MC_H_PR_LC" 
			ELIF  IS_BIT_SET(g_FMMC_STRUCT.sPlayerRuleData[g_CreatorsSelDetails.iPlayerRuleMapping[g_CreatorsSelDetails.iSelectedTeam][g_CreatorsSelDetails.iRow]].iBitset[g_CreatorsSelDetails.iSelectedTeam], ciPLAYER_RULE_BITSET_ScaleLootThresholdByNumPlayers)
				RETURN "MC_H_PR_LS" 
			ENDIF
			RETURN "MC_H_PR_L" 
		ENDIF
		
		IF g_CreatorsSelDetails.sTeamOptions[g_CreatorsSelDetails.iSelectedTeam].sSelection[g_CreatorsSelDetails.iRow].iRule = ciSELECTION_PLAYER_RULE_POINTS_THRESHOLD
			RETURN "MC_H_PR_P"
		ENDIF
		
		RETURN "MC_H_SELE" 
		
	ENDIF
	
	SWITCH GET_CURRENT_RHM_SELECTION()
	
		CASE FMMC_SUB_OPTIONS_MULTI_RULE_TIMER RETURN "MC_H_MRTM"
		CASE FMMC_SUB_OPTIONS_TIME RETURN "MC_H_TIME"
		CASE FMMC_SUB_OPTIONS_SCORE RETURN "MC_H_PNT"
		CASE FMMC_SUB_OPTIONS_TARGET_SCORE RETURN "MC_H_PNT1"
		CASE FMMC_SUB_OPTIONS_MULTIPLIER RETURN "MC_H_CAPM"
		CASE FMMC_SUB_OPTIONS_FAIL_OBJECTIVE RETURN "MC_H_OBJF"
		CASE FMMC_SUB_OPTIONS_RULE_BOUNDS RETURN "MC_H_BOUND"
		CASE FMMC_SUB_OPTIONS_LEGACY_OUT_OF_BOUNDS RETURN "MC_H_BOUND"
		CASE FMMC_SUB_OPTIONS_TEXT RETURN "MC_H_CTXT"
		CASE FMMC_SUB_OPTIONS_SECONDARY_TEXT RETURN "MC_H_CTXT1"
		CASE FMMC_SUB_OPTIONS_DROP_OFF_POINT RETURN "MC_H_DROP"
		CASE FMMC_SUB_OPTIONS_DROP_OFF_CORONA RETURN "FMMCCMENU_CSH"
		CASE FMMC_SUB_OPTIONS_DROP_OFF_GPS RETURN "MC_H_GPS"
		CASE FMMC_SUB_OPTIONS_CLEAR_GPS RETURN "FMMCCMENU_GPH"
		CASE FMMC_SUB_OPTIONS_MUSIC_CUES RETURN "MC_H_MC"
		CASE FMMC_SUB_OPTIONS_GANG_B_MENU RETURN "MC_H_OP0"
		CASE FMMC_SUB_OPTIONS_RESTART_CHECKPOINTS_MENU RETURN "MC_H_MRC_T"
		CASE FMMC_SUB_OPTIONS_RESPAWN_IN_VEH RETURN "MC_H_OP1"
		CASE FMMC_SUB_OPTIONS_RESPAWN_IN_MISSION_VEH RETURN "MC_H_RL_RIMV"
		CASE FMMC_SUB_OPTIONS_RESPAWN_IN_LAST_MISSION_VEH_FROM_LIST RETURN "MC_H_RL_RLVL"
		CASE FMMC_SUB_OPTIONS_WANTED_L_MENU RETURN "FMMC_WANTH"
		CASE FMMC_SUB_OPTIONS_NUM_CARRYABLE RETURN "MC_H_OP3"
		CASE FMMC_SUB_OPTIONS_REQUIRED_DELIVERIES RETURN "MC_H_CSA_RQE"
		CASE FMMC_SUB_OPTIONS_AUTOCOMPLETE RETURN "MC_H_OP5"
		CASE FMMC_SUB_OPTIONS_FORCE_MIDPOINT RETURN "MC_H_FRCMP"
		CASE FMMC_SUB_OPTIONS_HUD RETURN "MC_H_OP6"
		CASE FMMC_SUB_OPTIONS_ALL_TEAM_IN_VEHICLE RETURN "MC_H_RL_ALT"
		CASE FMMC_SUB_OPTIONS_DYNAMIC_TEAM RETURN "FMMC_SS_RL_61H"
		CASE FMMC_SUB_OPTIONS_DESTRUCTION RETURN "FMMC_SS_RL_62H"
		CASE FMMC_SUB_OPTIONS_BLOCK_MECHANIC RETURN "MC_H_BMD"
		CASE FMMC_SUB_OPTIONS_PHOTOGRAPH_LICENSE_PLATE RETURN "MC_H_PLP"
		CASE FMMC_SUB_OPTIONS_RESTRICT_VOICE_CHAT RETURN "MC_H_RL_DVR"
		CASE FMMC_SUB_OPTIONS_ENABLE_EMERGENCY_CALLS RETURN "MC_H_RL_EEC"
		CASE FMMC_SUB_OPTIONS_SWING_FREE_1 RETURN "MC_H_SF1"
		CASE FMMC_SUB_OPTIONS_SWING_FREE_2 RETURN "MC_H_SF2"
		CASE FMMC_SUB_OPTIONS_OUTFIT_VARIATIONS RETURN "MC_H_OV"
		CASE FMMC_SUB_OPTIONS_CUSTOM_CROWD_CONTROL RETURN "MC_H_RL_CCC"
		CASE FMMC_SUB_OPTIONS_COUNTDOWN_SOUNDS RETURN "MC_H_RL_CDS"
		CASE FMMC_SUB_OPTIONS_SUDDENDEATH_CD_SOUNDS RETURN "DESC_RL_SDCD"
		CASE FMMC_SUB_OPTIONS_HIT_SOUND RETURN "MC_H_RL_HTS"
		CASE FMMC_SUB_OPTIONS_LOOP_ALL_DIALOGUE_TRIGGERS RETURN "MC_H_RL_LADT"
		CASE FMMC_SUB_OPTIONS_PROGRESS_IN_SAME_PLACE RETURN "MC_H_RL_PSP"
		CASE FMMC_SUB_OPTIONS_PROGRESS_MINIGAME RETURN "MC_H_RL_PMG"
		CASE FMMC_SUB_OPTIONS_MAKE_HARD_INCREASE_WANTED_LEVEL RETURN "MC_H_RL_IWL"
		CASE FMMC_SUB_OPTIONS_DONT_FAIL_ON_TIMER_EXPIRE RETURN "MC_H_RL_DFT"
		CASE FMMC_SUB_OPTIONS_TRAFFIC RETURN "MC_H_RL_TRF"
		CASE FMMC_SUB_OPTIONS_PED_DENSITY RETURN "MC_H_RL_PEDD"
		CASE FMMC_SUB_OPTIONS_FORCE_COMBAT_MODE RETURN "MC_H_RL_FCM"
		CASE FMMC_SUB_OPTIONS_QUICK_EQUIP_REBREATHER RETURN "MC_H_RL_QER"
		CASE FMMC_SUB_OPTIONS_RANDOM_NEXT_OBJECTIVE RETURN "MC_H_RL_RNO"
		CASE FMMC_SUB_OPTIONS_TEAM_LIVES_OVERRIDE RETURN "MC_H_RL_PPL"
		CASE FMMC_SUB_OPTIONS_SUDDEN_DEATH_TIMER RETURN "MC_H_RL_SDT"
		CASE FMMC_SUB_OPTIONS_RESPAWN_WITH_LIMITED_Z				 RETURN "FMMC_H_RL_RSZ"
		CASE FMMC_SUB_OPTIONS_DISABLE_HWN_HEARTBEAT RETURN "MC_H_RL_BFJH"
		CASE FMMC_SUB_OPTIONS_DISABLE_HWN_TEAM_VIBRATE RETURN "MC_H_RL_BFJFV"
		CASE FMMC_SUB_OPTIONS_SPAWN_GROUP_OVERRIDE RETURN "MC_H_RL_SGOVR"
		CASE FMMC_SUB_OPTIONS_DRAW_PLAYER_POSITION RETURN "MC_H_RL_DPP"
		CASE FMMC_SUB_OPTIONS_USE_APP_AS_PRESIDENT RETURN "MC_H_RL_UAP"
		CASE FMMC_SUB_OPTIONS_IGNORE_GOTO_RADIUS RETURN "MC_H_RL_IGR"
		CASE FMMC_SUB_OPTIONS_MAX_HEALTH RETURN "MC_H_RL_IMH"
		CASE FMMC_SUB_OPTIONS_SCALE_MAX_HEALTH RETURN "MC_H_RL_SMH"
		CASE FMMC_SUB_OPTIONS_BLOCK_EXPLOSION_RAGDOLL RETURN "MC_H_RL_BRE"
		CASE FMMC_SUB_OPTIONS_GET_DELIVER_FILTER RETURN "MC_H_RL_GDF"
		CASE FMMC_SUB_OPTIONS_CARGOBOB_MANUAL_RESPAWN RETURN "MC_H_RL_CBMR"	
		CASE FMMC_SUB_OPTIONS_GET_DELIVER_ICON RETURN "MC_H_RL_GDI"
		CASE FMMC_SUB_OPTIONS_GET_DELIVER_ICON_LARGE RETURN "MC_H_RL_GDL"
		CASE FMMC_SUB_OPTIONS_GET_DELIVER_ICON_LOCAL RETURN "MC_H_RL_GDIL"
		CASE FMMC_SUB_OPTIONS_GET_DELIVER_EXCLUSIVE_DELIVER RETURN "FMMC_H_RL_GDED"
		CASE FMMC_SUB_OPTIONS_SET_SPECIFIC_TOD RETURN "MC_H_RL_STOD"
		CASE FMMC_SUB_OPTIONS_SUBTRACT_POINTS_FROM_TEAM RETURN "FMMC_H_RL_SPFT"
		CASE FMMC_SUB_OPTIONS_WAIT_FOR_ALL_PASS_CONSQ RETURN "FMMC_H_RL_WPPC"
		CASE FMMC_SUB_OPTIONS_KILL_PLAYERS_NOT_AT_LOCATE RETURN "FMMC_H_RL_KPNAL"
		CASE FMMC_SUB_OPTIONS_SHOW_SCORES_TO_SPECTATORS RETURN "MC_H_RL_SSTS"
		CASE FMMC_SUB_OPTIONS_VIBRATE_LAST_ALIVE RETURN "MC_H_RL_VILA"
		CASE FMMC_SUB_OPTIONS_DONT_FAIL_ALL_SPECTATE RETURN "MC_H_RL_DFTAS"
		CASE FMMC_SUB_OPTIONS_ALL_PLAYERS_IN_ANY_LOCATE RETURN "MC_H_RL_APAL"
		CASE FMMC_SUB_OPTIONS_USE_NEXT_RULES_CUSTOM_SPAWNPOINTS RETURN "FMMC_RLH_NRS"
		CASE FMMC_SUB_OPTIONS_SWAP_TEAM_IN_LOCATE RETURN "FMMC_RLH_STIL"
		CASE FMMC_SUB_OPTIONS_GRANULAR_CAPTURE RETURN "FMMC_RLH_UGC"
		CASE FMMC_SUB_OPTIONS_CLEANUP_UNDELIVERED_VEHICLES RETURN "FMMC_RLH_CUDV"
		CASE FMMC_SUB_OPTIONS_AMBIENT_POLICE_ACCURACY RETURN "FMMC_RLH_APA"
		CASE FMMC_SUB_OPTIONS_FINAL_RULE_RP_BONUS RETURN "FMMC_RLH_FORB"
		CASE FMMC_SUB_OPTIONS_WAIT_FOR_ALL_IN_MOC RETURN "FMMC_RLH_WFPM"
		CASE FMMC_SUB_OPTIONS_PROGRESS_VEHICLES_WITH_RESPAWNS RETURN "FMMC_RLH_PVWR"
		CASE FMMC_SUB_OPTIONS_PROGRESS_WHEN_ALL_IN_LOCATE RETURN "FMMC_RLH_PAIL"
		CASE FMMC_SUB_OPTIONS_TEAM_COLOURED_LOCATES RETURN "FMMC_RLH_TCL"
		CASE FMMC_SUB_OPTIONS_RESET_AND_CACHE_SCORE RETURN "FMMC_RL_RACSS"
		CASE FMMC_SUB_OPTIONS_CHECK_CACHED_SCORES_WHEN_WE_START_RULE RETURN "FMMC_RL_CCSWS"
		CASE FMMC_SUB_OPTIONS_END_BOMBUSHKA_EARLY RETURN "FMMC_RLH_EEFBB"
		CASE FMMC_SUB_OPTIONS_FORCE_HEIST_SPECTATE RETURN "FMMC_RLH_FHS"
		CASE FMMC_SUB_OPTIONS_LEAVE_VEHICLE_ON_RESTART RETURN "MC_H_SS_RL_19c"
		CASE FMMC_SUB_OPTIONS_EXPLODE_SUBMARINE RETURN "MC_H_EXPSUB"
		CASE FMMC_SUB_OPTIONS_PASS_PROTECT_VIA_AIM RETURN "FMMC_RL_H_PPVA"
		CASE FMMC_SUB_OPTIONS_BLOCK_PHOTO_HELP RETURN "MC_RL_H_BPH"
		CASE FMMC_SUB_OPTIONS_INVALIDATE_MULTIRULE_TIMERS RETURN "MC_H_IMRT"
		CASE FMMC_SUB_OPTIONS_SECUROHACK_REQUIRES_KILL RETURN "MC_H_SHREK"
		CASE FMMC_SUB_OPTIONS_PLAY_DOWNLOAD_SOUND_AFTER_DIALOGUE RETURN "MC_H_DSAFTERDL"
		CASE FMMC_SUB_OPTIONS_CONTROL_OPTIONS RETURN "MC_H_RL_PCO"
		CASE FMMC_SUB_OPTIONS_HUD_OPTIONS RETURN "MC_H_RL_HUDO"
		CASE FMMC_SUB_OPTIONS_GAMEPLAY_OPTIONS RETURN "MC_H_RL_GMPO"
		CASE FMMC_SUB_OPTIONS_RESPAWN_OPTIONS RETURN "MC_H_RL_RSPO"
		CASE FMMC_SUB_OPTIONS_PACK_SPECIFIC_OPTIONS RETURN "MC_H_RL_PSO"	
		CASE FMMC_SUB_OPTIONS_HEALTH_THRESHOLD_FOR_DAMAGE_ENTITY_OVERRIDE RETURN "MC_H_RL_DEHT"						
		CASE FMMC_SUB_OPTIONS_RULE_ROAMING_SPECTATOR_OPTIONS RETURN "MC_H_ROAMSPEC_R"
		CASE FMMC_SUB_OPTIONS_TOGGLE_QUICK_GPS RETURN "MC_H_RL_TGPS"
		CASE FMMC_SUB_OPTIONS_TOGGLE_HINT_CAM RETURN "MC_H_RL_THC"
		CASE FMMC_SUB_OPTIONS_HALF_HINT_CAM_FOCUS_RANGE RETURN "MC_H_RL_HHCFR"
		CASE FMMC_SUB_OPTIONS_NEAREST_TARGET_THRESHOLD RETURN "MC_H_RL_NTT"
		CASE FMMC_SUB_OPTIONS_START_ALL_ZONE_TIMERS RETURN "MC_H_RL_STZNT"
		CASE FMMC_SUB_OPTIONS_STOP_ALL_ZONE_TIMERS RETURN "MC_H_RL_STPZNT"
		CASE FMMC_SUB_OPTIONS_STOP_POISON_GAS RETURN "MC_H_RL_SPG"
		CASE FMMC_SUB_OPTIONS_EXPLODE_VAULT_DOOR RETURN "MC_H_RL_EVD"
		CASE FMMC_SUB_OPTIONS_USE_LOCATION_COLLECTION_DELAY RETURN "MC_H_RL_ULCDE"
		CASE FMMC_SUB_OPTIONS_MINIGAME_TAKE_PROGRESSION RETURN "MC_H_RL_TKPR"
		CASE FMMC_SUB_OPTIONS_ATTACH_FLARE_TO_BACK RETURN "MC_H_RL_PFLB"
		CASE FMMC_SUB_OPTIONS_END_OF_MISSION_INVINCIBILITY RETURN "MC_H_RL_EOMI"
		CASE FMMC_SUB_OPTIONS_CLEAR_PREREQS_ON_RULE RETURN "MC_H_RL_CLPRR"
		CASE FMMC_SUB_OPTIONS_FORCE_WEAPON_IN_HAND_THIS_RULE RETURN "MC_H_RL_FWPP"
		CASE FMMC_SUB_OPTIONS_FORCE_STARTING_WEAPON_IN_HAND_THIS_RULE RETURN "MC_H_RL_FSWPP"
		CASE FMMC_SUB_OPTIONS_PED_OPTIONS RETURN "MC_H_RLNT_PE"
		CASE FMMC_SUB_OPTIONS_VEHICLE_OPTIONS RETURN "MC_H_RLNT_VE"
		CASE FMMC_SUB_OPTIONS_OBJECTS_OPTIONS RETURN "MC_H_RLNT_OB"
		CASE FMMC_SUB_OPTIONS_LOCATIONS_OPTIONS RETURN "MC_H_RLNT_LO"
		CASE FMMC_SUB_OPTIONS_PICKUPS_OPTIONS RETURN "MC_H_RLNT_PI"
		CASE FMMC_SUB_OPTIONS_PROPS_OPTIONS RETURN "MC_H_RLNT_PR"
		CASE FMMC_SUB_OPTIONS_WORLD_OPTIONS RETURN "MC_H_RLNT_WO"
		CASE FMMC_SUB_OPTIONS_ALTERNATE_VARIABLES_OPTIONS RETURN "MC_H_AVA_RL_T"
		CASE FMMC_SUB_OPTIONS_AUDIO_OPTIONS RETURN "MC_H_RLNT_AUDO"
		CASE FMMC_SUB_OPTIONS_FORCED_HINT_OPTIONS RETURN "MC_H_RLNT_FHC"
		CASE FMMC_SUB_OPTIONS_ON_RULE_START_OPTIONS	RETURN "MC_H_RL_ORS"
		CASE FMMC_SUB_OPTIONS_INTERACTABLE_OPTIONS RETURN "MC_H_RL_INTO"
		CASE FMMC_SUB_OPTIONS_INTERIOR_OPTIONS RETURN "HMC_SS_RL_IN"
	ENDSWITCH
	RETURN ""
ENDFUNC

FUNC MENU_ICON_TYPE DOES_THIS_DESCRIPTION_NEED_A_WARNING(STRING description)
	IF ARE_STRINGS_EQUAL(description, "DMC_H_40") AND (IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_PHOTO) OR IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_LAST_GEN_PHOTO))
	OR ARE_STRINGS_EQUAL(description, "MC_H_PANF") AND IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_INTROCAM)
	OR ARE_STRINGS_EQUAL(description, "MC_H_SSL") AND IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_TRIGGER)
	OR ARE_STRINGS_EQUAL(description, "MC_H_MD0") AND IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_TITLE)
	OR ARE_STRINGS_EQUAL(description, "MC_H_MD1") AND IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_DESC)
	OR ARE_STRINGS_EQUAL(description, "FMMCNO_CLOUD")	
	OR ARE_STRINGS_EQUAL(description, GET_DISCONNECT_HELP_MESSAGE())	
	OR ARE_STRINGS_EQUAL(description, "MC_VAROPT_OFF")
	OR ARE_STRINGS_EQUAL(description, "FMMC_NAIL")
	OR ARE_STRINGS_EQUAL(description, "FMMC_CDM_ONP")
		RETURN MENU_ICON_ALERT
	ENDIF
	
	RETURN MENU_ICON_DUMMY
ENDFUNC

FUNC STRING GET_MENU_ITEM_DESCRIPTION()
	IF NOT sFMMCMenu.bRuleMenuActive	 
		RETURN GET_LEFT_HAND_MENU_ITEM_DESCRIPTION()
	ENDIF
	
	RETURN GET_RIGHT_HAND_MENU_ITEM_DESCRIPTION()
ENDFUNC

PROC PROCESS_RHM_SCALING()
	
	IF fMenuScaleSmooth != RHM_EXTRA_MENU_WIDTH
		INTERP_FLOAT_TO_TARGET_AT_SPEED(fMenuScaleSmooth, RHM_EXTRA_MENU_WIDTH, 20.0)
	ENDIF

	IF fMenuScaleSmooth = 0.0
		MAIN_X_POS = cfMAIN_X_POS
		MAIN_WIDTH = cfMAIN_WIDTH
		TEXT_X = cfTEXT_X
		TEXT_X_COL_WIDTH = cfTEXT_X_COL_WIDTH
		EXIT
	ENDIF
	
	MAIN_X_POS 			= cfMAIN_X_POS - fMenuScaleSmooth
	MAIN_WIDTH 			= cfMAIN_WIDTH + (fMenuScaleSmooth *2)
	TEXT_X 				= cfTEXT_X - (fMenuScaleSmooth *2)
	TEXT_X_COL_WIDTH 	= cfTEXT_X_COL_WIDTH + (fMenuScaleSmooth *2) + 0.01
ENDPROC

/// PURPOSE: Set the width to add to this menu 
///    
/// PARAMS:
///    fExtraMenuWidth - Go up in 0.01s
PROC SET_RHM_MENU_EXTRA_WIDTH(FLOAT fExtraMenuWidth)
	IF !bUsingRHMScaleWidget
		#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_FixedRightMenuSize")
			IF fExtraMenuWidth != 0.0
				fExtraMenuWidth = 0.03
			ENDIF
		ENDIF
		#ENDIF
		RHM_EXTRA_MENU_WIDTH = fExtraMenuWidth
	ENDIF
ENDPROC

