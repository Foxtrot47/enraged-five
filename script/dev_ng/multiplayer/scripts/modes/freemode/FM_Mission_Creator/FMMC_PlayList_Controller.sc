//////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:		FREEMODE mission launcher/blip controller										   	//
// Description: Controls the custom missions in free mode that have been created with the creator   //
// Written by:  Robert Wright/Rowan Cockcroft														//
// Date: 15/11/2012																				 	//
//////////////////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"

//Game Headers
USING "commands_network.sch"

USING "leader_board_common.sch"
//Network Headers

USING "net_mission.sch"
USING "net_include.sch" 

USING "FMMC_Cloud_loader.sch"
USING "FM_Playlist_Header.sch"
USING "FMMC_MP_Setup_Mission.sch"
USING "FMMC_Launcher_Menu.sch"
USING "net_job_intro.sch"
USING "fmmc_restart_header.sch"
USING "net_celebration_screen.sch"
USING "net_interactions.sch"
USING "fmmc_corona_controller.sch"
USING "net_wait_zero.sch"
USING "net_cloud_reward_system.sch"
USING "net_cash_transactions.sch"
USING "fmmc_header.sch"

#IF IS_DEBUG_BUILD
USING "profiler.sch"
#ENDIF


CONST_INT ciLEADER_BOARD_END_TIME 		30000
CONST_INT ciLEADER_BOARD_MIN_END_TIME 	15000
CONST_INT ciRaceFail	HIGHEST_INT/16
GET_UGC_CONTENT_STRUCT				  sGetUGC_content
SAVE_OUT_UGC_PLAYER_DATA_VARS		   sSaveOutVars
LBD_VARS lbdVars
//For forsing the host to not be a spectator
INT iManageSpechost = 0
INT iDesiredHost = -1

DPAD_VARS dpadVars

////////////////////////////////////////////////////////////
////////////////// VARIABLES FOR PLAYLIST //////////////////
//Const ints
CONST_INT GAME_STATE_INI									0
CONST_INT GAME_STATE_SET_UP_PLAYER_DATA					 	1
CONST_INT GAME_STATE_LOAD_FROM_CLOUD						2
CONST_INT GAME_STATE_RUNNING								3
CONST_INT GAME_STATE_END_LEADER_BOARD					    4
CONST_INT GAME_STATE_PLAYLIST_RESTART					    5
CONST_INT GAME_STATE_END									6

CONST_INT PLAYLIST_STATE_INIT							    0
CONST_INT PLAYLIST_STATE_WAIT_TO_START_MISSION			    1
CONST_INT PLAYLIST_STATE_CLEANUP_WHEN_ON_MISSION			2
CONST_INT PLAYLIST_STATE_WAIT_TILL_END_OF_MISSION		   	3
CONST_INT PLAYLIST_STATE_WAIT_TILL_OFF_ROUNDS_MISSION		4
CONST_INT PLAYLIST_STATE_WAIT_TILL_OFF_MISSION			  	5
CONST_INT PLAYLIST_STATE_WAIT_TILL_SERVER_READY			 	6
CONST_INT PLAYLIST_STATE_DISPLAY_LEADERBOARD				7
CONST_INT PLAYLIST_CHECK_CHALLENGE_IS_OPEN				  	8

CONST_INT ciEND_LEADER_BOARD_SAVE_TO_SC_LEADER_BOARD		0
CONST_INT ciEND_LEADER_BOARD_SET_UP_DATA					1
CONST_INT ciEND_LEADER_BOARD_SAVE_TO_MEMBER_SPACE		   	2
CONST_INT ciEND_LEADER_BOARD_SAVE_TO_UGC					3
CONST_INT ciEND_LEADER_BOARD_SAVE_TO_UGC_WAIT_FOR_FINISH	4
CONST_INT ciEND_LEADER_BOARD_SAVE_TO_UGC_ERROR			  	5

CONST_FLOAT cfVOTE_PASS_RATIO							   0.5
UGC_TYPE nTYPE = UGC_TYPE_GTA5_MISSION_PLAYLIST

//leaderboard bitset.
CONST_INT ci_FINISH_LB									  	0
CONST_INT ci_INIT_LB										1


STRUCT PLAY_LIST_DETAILS_STRUCT
	TEXT_LABEL_23   tl23MissionOwner
	TEXT_LABEL_23   tl23MissionFileName
ENDSTRUCT

////For the end leaderborasd
//DATAFILE_DICT   dfdMainDict
//DATAFILE_DICT   dfDataDict
//DATAFILE_DICT   dfTargetDict
//DATAFILE_ARRAY  dfaMain
//DATAFILE_DICT   dfdArray[FMMC_MAX_PLAY_LIST_LENGTH]
INT iTotalScoreForTurnament
INT iServerPlaylistSortStage
// The server broadcast data.
// Everyone can read this data, only the server can update it. 
STRUCT ServerBroadcastData
	TEXT_LABEL_23		   tl23PlayListToPlay
	TEXT_LABEL_31		   tl31ChallengeName
	BOOL 					bNameSet
	INT					 iServerVoteBitSet	   //The server bit set
	INT					 iServerSetUpBitSet
	INT					 iServerClearPlayerBitSet
	INT					 iInstanceId
	INT					 iNumberVotedMenu
	INT					 iVotedBitSet
	INT					 iGameState
	BOOL				 bPlayListSorted
	INT					 iCurrentPlayListPosition 
	INT					 iLength
	
	//playlist leaderboard data
	SCRIPT_TIMER			tdLeaderboardTimer
	TEXT_LABEL_63			tlWinningPlayer
	PLAYER_INDEX			piWinningPlayer
	INT					 iLBBitset
	INT					 iMissionLbDone
	INT					 iTotalNumOfLBPlayers
	INT					 iNumberOfLBPlayers[FMMC_MAX_TEAMS]
	INT					 iNumPlaylistTeams
	INT					 iWinningTeam
	INT					 iSecondTeam
	INT					 iThirdTeam
	INT					 iLosingTeam
	INT					 iBestPlayerOrder[NUM_NETWORK_PLAYERS]
	INT					 iteamScore[FMMC_MAX_TEAMS]
	INT					 iteamKills[FMMC_MAX_TEAMS]
	INT					 iteamDeaths[FMMC_MAX_TEAMS]
	INT					 iteamHeadshots[FMMC_MAX_TEAMS]
	
	//For dealing with shuffel option.
	BOOL					bPlaylistShuffeled
	BOOL					bQualifingPlaylist
	//Tournament
	BOOL					bIsTournamentPlaylist
	BOOL					bIsSctvControlledPlaylist
	BOOL					bIsLiveStreamPlaylist
	BOOL					bIsFinalRoundPlaylist
	BOOL					bRunLeaderBoard
	BOOL 					bCloseLeaderBoard
	INT					 iMissionPlayedBitSet
	INT					 iNumberOfMissionsPlayed
	
	//For the challenges
	BOOL					bPlaylistDoingChallenge
	BOOL					bDmTournament
	BOOL					bPlaylistSettingChallenge
	INT						 iSaveChallengeToUGC
	BOOL					bChallengeFailed
	FLOAT				   fChallengeDif
	
	//For Head 2 Head
	BOOL					bPlaylistDoingHeadToHead
	BOOL					bRestartPlayList = FALSE
	BOOL					bQuitPlayList = FALSE
	
	INT					 iTeamCrew[MAX_HEAD_TO_HEAD_CREWS]
	
	INT					 iScoreToBeat
	INT					 iCashBet
	INT					 iCashBetPlayer
	INT					 iTimeToBeat 
	INT					 iBestScore[FMMC_MAX_PLAY_LIST_LENGTH]
	INT					 iBestTime[FMMC_MAX_PLAY_LIST_LENGTH]
	
//   INT					 iPreviousPlaylistScore[NUM_NETWORK_PLAYERS][NUM_PREVIOUS_SCORES]  //moved to THE_LEADERBOARD_STRUCT
	
	BOOL					bAllPlayersReady
	
	//Reset Stuff
	BOOL bServerResetFlags
	
	BOOL bProgressPastLB
	BOOL bReSetLateFlags
	INT iMacaddresshash 
	INT iPosixtime 
	#IF IS_DEBUG_BUILD
	BOOl bOnePlayer
	#ENDIF  
	CELEB_SERVER_DATA sCelebServer
ENDSTRUCT
ServerBroadcastData serverBD

//For the leaderboards
STRUCT ServerBroadcastData_LB
	THE_LEADERBOARD_STRUCT   sleaderboard[NUM_NETWORK_PLAYERS]
ENDSTRUCT
ServerBroadcastData_LB serverBD_LB

CONST_INT ciServerVoteCast				  0
CONST_INT ciServerVoteRestart			   1
CONST_INT ciServerVoteNext				  2
CONST_INT ciServerMenuVotePassed			3
CONST_INT ciServerMissionHasLaunched		4

CONST_INT ciServer_SET_UP_PlayListNameSet   0

// The client broadcast data.
// Everyone can read this data, only the local player only the server can update it. 
STRUCT PlayerBroadcastData
	//Main game state
	INT					 iGameState		  = GAME_STATE_INI
	INT					 iPlayListState	  = PLAYLIST_STATE_WAIT_TO_START_MISSION
	INT					 iSaveToUgc		  = GAME_STATE_INI
	INT					 iteam			   = -1
	INT 				 iCurrentPlayListPos = -1
	INT					 iPlayerBitSet
	INT					 iMyRankPrediction
	INT					 iTotatlUniquePlays
	BOOL				 bJoinedAsSpectator
	//TEXT_LABEL_31		   tl31ChallengeName
ENDSTRUCT	
PlayerBroadcastData playerBD[NUM_NETWORK_PLAYERS]

CONST_INT ciPLAYER_BS_PROGRESS_PASSED_LB		0
CONST_INT ciPLAYER_GRABBED_LEADERBOARD_RANKS	1
CONST_INT ciPLAYER_CLEAR_MY_LB_DATA_PLEASE		2
CONST_INT ciPLAYER_BS_PASSED_LB					3

//SET_UP_JOINING_PLAYLIST_VARS  sJoiningPlaylistVars
//GET_UGC_CONTENT_STRUCT		sGetUGC_content
//SAVE_OUT_UGC_PLAYER_DATA_VARS sSaveOutVars

BOOL bVoteCleanedUp = TRUE
BOOL bLeaderBoardWriteCompleate
BOOL bCelebrationCompleate
BOOL bQualificationCelebrationSummaryComplete
BOOL bCelebrationSummaryComplete
BOOL bPlayedFullPlayList = TRUE
STRUCT PLAYER_STATUS_COUNTS
	INT iNumberOnPlayList
	INT iNumberRestart
	INT iNumberNext
	INT iNumberAtEndOfMissionCount
	INT iNumberVoted
	INT iNumberVotedMenu
	INT iNumberAtEndOfPlaylistCount	 
	INT iNumberWantToRestartPlaylist	
	INT iNumberWantToQuitPlaylist   
	INT iNumberWantToProgressPastLB
	INT iNumberNotPlaying
	INT iNumberAtEndOfMissionNotPlaying
ENDSTRUCT
PLAYER_STATUS_COUNTS sPlayerCounts
//
STRUCT LOCAL_PLAYER_SCORES_STRUCT
	INT		 iTime[FMMC_MAX_PLAY_LIST_LENGTH]
	INT		 iScore[FMMC_MAX_PLAY_LIST_LENGTH]
	INT		 iKills[FMMC_MAX_PLAY_LIST_LENGTH]
	INT		 iDeaths[FMMC_MAX_PLAY_LIST_LENGTH]	
ENDSTRUCT

LOCAL_PLAYER_SCORES_STRUCT sLocalPlayersScores
SCRIPT_TIMER QualifyingMessageTimer
//leaderboard stuff
PLAYLIST_LB_VARIABLES sPLLB_Vars
INT iLBPlayerSelection// = -1
TIME_DATATYPE timeScrollDelay 
LEADERBOARD_PLACEMENT_TOOLS LBPlacement 
TEXT_LABEL_63 tParticipantNames[MAX_NUM_MC_PLAYERS]
INT iSavedRow[NUM_NETWORK_PLAYERS]
//player card variables
FMMC_EOM_DETAILS playlistEOMvars
//BOOL bLaunchedPlaylist
BOOL bRebuildHelp
INT iStuck

//Celebration Screen
CELEBRATION_SCREEN_DATA sCelebrationData
JOB_OUTRO_CUT_DATA sJobOutroCutData
CAMERA_INDEX ciCelebrationCam

INT iChallengeCashPrize

//Bool to make sure that we display qualifying playlist feed message only once - Steve T.
BOOL b_DoneQualifyingPlaylistFeedMessage = FALSE
BOOL b_DoQualifyingPlaylistWontCountFeedMessage
INT iTotalBestScore
INT iTotalBestTime

#IF IS_DEBUG_BUILD
INT iPosixtimeDebug
INT iMacaddresshashDebug
BOOL bWidgetsCreated
BOOL bKillScript
BOOL bSetATime, bSetCloseTime
BOOL bBeatChallenge 
INT iSetScore = 100000
INT iBeatScore = 99000
WIDGET_GROUP_ID wgGroup
//Shitty widgets.
PROC CREATE_WIDGETS()

	wgGroup= wgGroup
	//INT iMyPartID = NATIVE_TO_INT(PLAYER_ID())	
	wgGroup= START_WIDGET_GROUP("  FMMC Playlist Controller") 
	//  ADD_WIDGET_BOOL("b_forcePlayList", b_forcePlayList)
		ADD_WIDGET_INT_SLIDER("iStuck", iStuck, -1, 99, 1)
		ADD_WIDGET_INT_SLIDER("iPlayListLength", serverBD.iLength, -1, 99, 1)
		ADD_WIDGET_INT_SLIDER("iCurrentPlayListPosition", g_sCurrentPlayListDetails.iCurrentPlayListPosition, -1, 99, 1)
		ADD_WIDGET_INT_SLIDER("iPlaylistProgress", g_sCurrentPlayListDetails.iPlaylistProgress, -1, 99, 1)
		ADD_WIDGET_INT_SLIDER("iCurrentPlayListPosition", serverBD.iCurrentPlayListPosition, -1, 99, 1)
		START_WIDGET_GROUP("  Debugging challenges") 
			ADD_WIDGET_BOOL("bSetATime", bSetATime)
			ADD_WIDGET_BOOL("bBeatChallenge", bBeatChallenge)
			ADD_WIDGET_BOOL("bSetCloseTime", bSetCloseTime)
			ADD_WIDGET_INT_SLIDER("iSetScore", iSetScore, -1, 1000000, 1)
			ADD_WIDGET_INT_SLIDER("iBeatScore", iBeatScore, -1, 1000000, 1)
		STOP_WIDGET_GROUP()
		//ADD_WIDGET_BOOL("bLaunchedPlaylist", bLaunchedPlaylist)
		ADD_WIDGET_BOOL("bOnePlayer", serverBD.bOnePlayer)
		ADD_WIDGET_BOOL("bQualifingPlaylist", serverBD.bQualifingPlaylist)
		ADD_WIDGET_INT_READ_ONLY("Server State", serverBD.iGameState) 
		ADD_WIDGET_INT_READ_ONLY("iServerVoteBitSet", serverBD.iServerVoteBitSet)
		ADD_WIDGET_INT_READ_ONLY("iLBBitset", serverBD.iLBBitset) 
		ADD_WIDGET_BOOL("HAS_NET_TIMER_STARTED", serverBD.tdLeaderboardTimer.bInitialisedTimer)
		ADD_WIDGET_BOOL("sPLLB_Vars.bWinnerFound", sPLLB_Vars.bWinnerFound)
		ADD_WIDGET_INT_READ_ONLY("iTotalNumOfLBPlayers", serverBD.iTotalNumOfLBPlayers) 
		ADD_WIDGET_INT_SLIDER("iPlayListLength", serverBD.iLength, -1, 99, 1)
		ADD_WIDGET_INT_SLIDER("iNumberOfMissionsPlayed", serverBD.iNumberOfMissionsPlayed, -1, 99, 1)
		
		
		ADD_WIDGET_BOOL("bRunLeaderBoard", serverBD.bRunLeaderBoard)
		ADD_WIDGET_BOOL("bShowPlaylistLbdDebug", bShowPlaylistLbdDebug)
		
		START_WIDGET_GROUP(" Vote Details")
			ADD_WIDGET_INT_READ_ONLY("No On Playlist", sPlayerCounts.iNumberOnPlayList)
			ADD_WIDGET_INT_READ_ONLY("No At End", sPlayerCounts.iNumberAtEndOfMissionCount)
			ADD_WIDGET_INT_READ_ONLY("No Voted", sPlayerCounts.iNumberVoted)
			ADD_WIDGET_INT_READ_ONLY("No Restart", sPlayerCounts.iNumberRestart)
			ADD_WIDGET_INT_READ_ONLY("No Next", sPlayerCounts.iNumberNext)
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP(" Player details Details")
			TEXT_LABEL_23 tl23
			INT i
			FOR i = 0 TO (NUM_NETWORK_PLAYERS - 1)
				tl23 = ""
				IF i = PARTICIPANT_ID_TO_INT()
					tl23 += "**"
				ENDIF
				tl23 += "Player-"
				tl23 += i
				START_WIDGET_GROUP(tl23)
					ADD_WIDGET_INT_READ_ONLY("currentMissionData.iBitSet", GlobalplayerBD_FM[i].currentMissionData.iBitSet)
					ADD_WIDGET_INT_SLIDER("iGameState", playerBD[i].iGameState, -1, 99, 1)
					ADD_WIDGET_INT_SLIDER("iPlayListState", playerBD[i].iPlayListState, -1, 99, 1)
				STOP_WIDGET_GROUP()
			ENDFOR
		STOP_WIDGET_GROUP()
		
		ADD_WIDGET_BOOL("bOnePlayerDeathmatch", GlobalServerBD_DM.bOnePlayerDeathmatch)
		ADD_WIDGET_BOOL("bKillScript", bKillScript)
		
		#IF SCRIPT_PROFILER_ACTIVE
		CREATE_SCRIPT_PROFILER_WIDGET()
		#ENDIF
	STOP_WIDGET_GROUP()
	
ENDPROC

PROC DEBUG_RENDER_PLAYLIST_LEADERBOARD()

	IF bShowPlaylistLbdDebug
	
	OR GET_COMMANDLINE_PARAM_EXISTS("sc_PlaylistDebugLbd")

		INT i
		FLOAT fDrawX, fDrawY
		INT iRows
		INT r,g,b,a
		INT iCol
		
		// Debug starting positions
		FLOAT fWindowX = 0.318
		FLOAT fWindowY = 0.472

		// Black box values
		FLOAT fBlackX = 0.370
		FLOAT fBlackY = 0.654
		FLOAT fBlackW = 0.315
		FLOAT fBlackH = 0.551
		
		FLOAT colW = 0.38
		
		// Shrink the box by the number of empty rows
		INT emptyRows = FMMC_MAX_NUM_RACERS - (NETWORK_GET_NUM_PARTICIPANTS() + 1)
		FLOAT emptySpace = g_SpawnData.MissionSpawnDetails.SpawnWindowRowY + (TO_FLOAT(emptyRows) * g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerY)
		fWindowY += emptySpace
		fBlackY += emptySpace*0.5
		fBlackH -= emptySpace
		
		// draw black box
		DRAW_RECT(fBlackX, fBlackY, fBlackW, fBlackH, 0, 0, 0, 128)
		
		// HEADINGS ///////////////////////////////////////////////////////////////////////
		
		// draw title
		fDrawX = fWindowX + g_SpawnData.MissionSpawnDetails.SpawnWindowColumnX
		fDrawY = fWindowY + g_SpawnData.MissionSpawnDetails.SpawnWindowRowY + (TO_FLOAT(iRows) * g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerY)		
		SET_TEXT_SCALE(0.0000, g_SpawnData.MissionSpawnDetails.SpawnWindowScale)
		GET_HUD_COLOUR(HUD_COLOUR_WHITE, r,g,b,a)
		SET_TEXT_COLOUR(r,g,b,a)
		DISPLAY_TEXT_WITH_PLAYER_NAME(fDrawX, fDrawY, "STRING", "Playlist Lbd", HUD_COLOUR_WHITE)
		
		// player ID title
		fDrawX = fWindowX + g_SpawnData.MissionSpawnDetails.SpawnWindowColumnX + (g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerX + ((g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerX * colW) * iCol))
		fDrawY = fWindowY + g_SpawnData.MissionSpawnDetails.SpawnWindowRowY + (TO_FLOAT(iRows) * g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerY)		
		SET_TEXT_SCALE(0.0000, g_SpawnData.MissionSpawnDetails.SpawnWindowScale)
		GET_HUD_COLOUR(HUD_COLOUR_YELLOW, r,g,b,a)
		SET_TEXT_COLOUR(r,g,b,a)
		DISPLAY_TEXT_WITH_PLAYER_NAME(fDrawX, fDrawY, "STRING", "PlyId", HUD_COLOUR_YELLOW)	
		iCol += 1
		
		// part id title
		fDrawX = fWindowX + g_SpawnData.MissionSpawnDetails.SpawnWindowColumnX + (g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerX + ((g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerX * colW) * iCol))
		fDrawY = fWindowY + g_SpawnData.MissionSpawnDetails.SpawnWindowRowY + (TO_FLOAT(iRows) * g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerY)		
		SET_TEXT_SCALE(0.0000, g_SpawnData.MissionSpawnDetails.SpawnWindowScale)
		GET_HUD_COLOUR(HUD_COLOUR_PINK, r,g,b,a)
		SET_TEXT_COLOUR(r,g,b,a)
		DISPLAY_TEXT_WITH_PLAYER_NAME(fDrawX, fDrawY, "STRING", "PrtId", HUD_COLOUR_PINK)		
		iCol += 1
		
		iRows += 1	
		
		// POPULATE /////////////////////////////////////////////////////////////////////////////
		
		PLAYER_INDEX playerId
		
		REPEAT COUNT_OF(serverBD_LB.sleaderboard) i
		
			playerId = serverBD_LB.sleaderboard[i].playerID
			IF IS_NET_PLAYER_OK(playerId)
			AND serverBD_LB.sleaderboard[i].iParticipant != -1
			
//				PRINTLN("[3343328] playerID = ", GET_PLAYER_NAME(playerID))
			
				fDrawX = fWindowX + g_SpawnData.MissionSpawnDetails.SpawnWindowColumnX
				fDrawY = fWindowY + g_SpawnData.MissionSpawnDetails.SpawnWindowRowY + (TO_FLOAT(iRows) * g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerY)		
						
				// player name
				SET_TEXT_SCALE(0.0000, g_SpawnData.MissionSpawnDetails.SpawnWindowScale)	
				
				IF IS_NET_PLAYER_OK(playerID)
					HUD_COLOURS hcPlayer = GET_PLAYER_HUD_COLOUR(playerID)
					IF NETWORK_GET_PARTICIPANT_INDEX(playerId) = NETWORK_GET_HOST_OF_THIS_SCRIPT()
						hcPlayer = HUD_COLOUR_GREENLIGHT
					ENDIF
					DISPLAY_TEXT_WITH_PLAYER_NAME(fDrawX, fDrawY, "STRING", GET_PLAYER_NAME(playerID), hcPlayer)
				ELSE
					DISPLAY_TEXT_WITH_PLAYER_NAME(fDrawX, fDrawY, "STRING", "invalid", HUD_COLOUR_GREY)
				ENDIF
						
				// other details
				iCol = 0
				GET_HUD_COLOUR(HUD_COLOUR_WHITE, r,g,b,a)	
				
				// player id
				SET_TEXT_SCALE(0.0000, g_SpawnData.MissionSpawnDetails.SpawnWindowScale)
				GET_HUD_COLOUR(HUD_COLOUR_YELLOW, r,g,b,a)
				SET_TEXT_COLOUR(r,g,b,a)
				DISPLAY_TEXT_WITH_NUMBER(fDrawX + (g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerX + ((g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerX * colW) * iCol)), fDrawY, "NUMBR", NATIVE_TO_INT(playerId))
				iCol += 1
				
				// participant id
				SET_TEXT_SCALE(0.0000, g_SpawnData.MissionSpawnDetails.SpawnWindowScale)
				GET_HUD_COLOUR(HUD_COLOUR_PINK, r,g,b,a)
				SET_TEXT_COLOUR(r,g,b,a)
				DISPLAY_TEXT_WITH_NUMBER(fDrawX + (g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerX + ((g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerX * colW) * iCol)), fDrawY, "NUMBR", serverBD_LB.sleaderboard[i].iParticipant)
				iCol += 1
				
				iRows += 1
//			ELSE
//			
//				PRINTLN("[3343328] -1 = ", i)
			ENDIF

		ENDREPEAT
	ENDIF
ENDPROC

#ENDIF

PROC SET_CONTROL_END_LEADER_BOARD_STAGE(INT iSTAGE)
	#IF IS_DEBUG_BUILD
	SWITCH iSTAGE
		CASE ciEND_LEADER_BOARD_SAVE_TO_SC_LEADER_BOARD
			PRINTLN("[PLC] SET_CONTROL_END_LEADER_BOARD_STAGE - ciEND_LEADER_BOARD_SAVE_TO_SC_LEADER_BOARD ")
		BREAK
		CASE ciEND_LEADER_BOARD_SET_UP_DATA
			PRINTLN("[PLC] SET_CONTROL_END_LEADER_BOARD_STAGE - ciEND_LEADER_BOARD_SET_UP_DATA ")
		BREAK
		CASE ciEND_LEADER_BOARD_SAVE_TO_MEMBER_SPACE
			PRINTLN("[PLC] SET_CONTROL_END_LEADER_BOARD_STAGE - ciEND_LEADER_BOARD_SAVE_TO_MEMBER_SPACE ")
		BREAK
		CASE ciEND_LEADER_BOARD_SAVE_TO_UGC
			PRINTLN("[PLC] SET_CONTROL_END_LEADER_BOARD_STAGE - ciEND_LEADER_BOARD_SAVE_TO_UGC ")
		BREAK
		CASE ciEND_LEADER_BOARD_SAVE_TO_UGC_WAIT_FOR_FINISH
			PRINTLN("[PLC] SET_CONTROL_END_LEADER_BOARD_STAGE - ciEND_LEADER_BOARD_SAVE_TO_UGC_WAIT_FOR_FINISH ")
		BREAK
		CASE ciEND_LEADER_BOARD_SAVE_TO_UGC_ERROR
			PRINTLN("[PLC] SET_CONTROL_END_LEADER_BOARD_STAGE - ciEND_LEADER_BOARD_SAVE_TO_UGC_ERROR ")
		BREAK
	ENDSWITCH
	#ENDIF
	serverBD.iSaveChallengeToUGC = iSTAGE
ENDPROC
//Set the main stage of the playlist
PROC SET_FM_PLAYLIST_STATE(PlayerBroadcastData &PBDpassed[], INT msmsPassed)
	#IF IS_DEBUG_BUILD  
		SWITCH msmsPassed
			CASE PLAYLIST_STATE_WAIT_TO_START_MISSION			PRINTLN("[PLC] SET_FM_PLAYLIST_STATE - WAIT_TO_START_MISSION     - T - ", GET_CLOUD_TIME_AS_INT(), ", F - ", GET_FRAME_COUNT(), " *")	BREAK
			CASE  PLAYLIST_STATE_CLEANUP_WHEN_ON_MISSION		PRINTLN("[PLC] SET_FM_PLAYLIST_STATE - CLEANUP_WHEN_ON_MISSION   - T - ", GET_CLOUD_TIME_AS_INT(), ", F - ", GET_FRAME_COUNT(), " *")	BREAK
			CASE PLAYLIST_STATE_WAIT_TILL_END_OF_MISSION		PRINTLN("[PLC] SET_FM_PLAYLIST_STATE - WAIT_TILL_END_OF_MISSION  - T - ", GET_CLOUD_TIME_AS_INT(), ", F - ", GET_FRAME_COUNT(), " *")	BREAK
			CASE PLAYLIST_STATE_WAIT_TILL_OFF_MISSION			PRINTLN("[PLC] SET_FM_PLAYLIST_STATE - WAIT_TILL_OFF_MISSION     - T - ", GET_CLOUD_TIME_AS_INT(), ", F - ", GET_FRAME_COUNT(), " *")	BREAK
			CASE PLAYLIST_STATE_WAIT_TILL_OFF_ROUNDS_MISSION	PRINTLN("[PLC] SET_FM_PLAYLIST_STATE - WAIT_OFF_ROUNDS_MISSION   - T - ", GET_CLOUD_TIME_AS_INT(), ", F - ", GET_FRAME_COUNT(), " *")	BREAK
			CASE PLAYLIST_STATE_DISPLAY_LEADERBOARD				PRINTLN("[PLC] SET_FM_PLAYLIST_STATE - DISPLAY_LEADERBOARD       - T - ", GET_CLOUD_TIME_AS_INT(), ", F - ", GET_FRAME_COUNT(), " *")	BREAK
			CASE PLAYLIST_STATE_WAIT_TILL_SERVER_READY			PRINTLN("[PLC] SET_FM_PLAYLIST_STATE - WAIT_TILL_SERVER_READY    - T - ", GET_CLOUD_TIME_AS_INT(), ", F - ", GET_FRAME_COUNT(), " *")	BREAK
			CASE PLAYLIST_CHECK_CHALLENGE_IS_OPEN				PRINTLN("[PLC] SET_FM_PLAYLIST_STATE - CHECK_CHALLENGE_IS_OPEN   - T - ", GET_CLOUD_TIME_AS_INT(), ", F - ", GET_FRAME_COUNT(), " *")	BREAK
		ENDSWITCH
	#ENDIF
	PBDpassed[PARTICIPANT_ID_TO_INT()].iPlayListState = msmsPassed
ENDPROC


//Has a challenge failed
FUNC BOOL HAS_CHALLENGE_BEEN_FAILED()
	RETURN (serverBD.bChallengeFailed = TRUE)
ENDFUNC

//has the playlist finished
FUNC BOOL IS_THIS_THE_LAST_MISSION_ON_A_PLAYLIST()
	RETURN (serverbd.iNumberOfMissionsPlayed + 1 = serverBD.iLength)
ENDFUNC

//has the playlist finished
FUNC BOOL HAS_PLAY_LIST_FINISHED()
	RETURN (serverbd.iNumberOfMissionsPlayed >= serverBD.iLength)
ENDFUNC
//are we doing a challenge
FUNC BOOL SB_PLAYLIST_DOING_CHALLENGE()
	RETURN serverBD.bPlaylistDoingChallenge
ENDFUNC

//are we doing a head to head
FUNC BOOL SB_PLAYLIST_DOING_HEAD_TO_HEAD()
	RETURN serverBD.bPlaylistDoingHeadToHead
ENDFUNC

//is this playlist shufferled!
FUNC BOOL SB_PLAYLIST_SHUFFELED()
	RETURN serverBD.bPlaylistShuffeled 
ENDFUNC

//is this a Qualifing playlist!
FUNC BOOL SB_IS_QUALIFYING_PLAYLIST()
	RETURN serverBD.bQualifingPlaylist
ENDFUNC
//is this a Tournament playlist!
FUNC BOOL SB_IS_TOURNAMENT_PLAYLIST()
	RETURN serverBD.bIsTournamentPlaylist
ENDFUNC
//is this a SCTV CONTROLLED playlist!
FUNC BOOL SB_IS_SCTV_CONTROLLED_PLAYLIST()
	RETURN serverBD.bIsSctvControlledPlaylist
ENDFUNC
//is this a Tournament playlist!
FUNC BOOL SB_IS_FINAL_ROUND_TOURNAMENT_PLAYLIST()
	RETURN serverBD.bIsFinalRoundPlaylist
ENDFUNC

//is this a Live Stream playlist!
FUNC BOOL SB_IS_LIVESTREAM_PLAYLIST()
	RETURN serverBD.bIsLiveStreamPlaylist
ENDFUNC

//are we setting a challenge time!
FUNC BOOL SB_PLAYLIST_SETTING_CHALLENGE_TIME()
	RETURN serverBD.bPlaylistSettingChallenge 
ENDFUNC

FUNC BOOL ARE_ALL_PLAYERS_READY()
	RETURN serverBD.bAllPlayersReady
ENDFUNC

PROC SET_ALL_PLAYERS_READY()
	serverBD.bAllPlayersReady = TRUE
	PRINTLN("[PLC] serverBD.bAllPlayersReady = TRUE")
ENDPROC

//PROC GRAB_PLAYER_TOTAL_SCORE_FOR_TOURNAMENT()
//	INT i
//	REPEAT NUM_NETWORK_PLAYERS i
//		IF serverBD_LB.sleaderboard[i].playerID = PLAYER_ID()
//			iTotalScoreForTurnament = serverBD_LB.sleaderboard[i].iPlayerScore
//		ENDIF
//	ENDREPEAT	
//	PRINTLN("[PLC] iTotalScoreForTurnament = ", iTotalScoreForTurnament)
//ENDPROC

//Should we clean up the 
FUNC BOOL CLEANUP_NONE_RESTART_DATA()
	//not in a game, 
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		RETURN TRUE
	ELSE
		//At the end and restarting
		IF playerBD[PARTICIPANT_ID_TO_INT()].iGameState >= GAME_STATE_END_LEADER_BOARD
		AND NOT serverBD.bRestartPlayList
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE	
ENDFUNC

//Cleanup Mission Data
PROC SCRIPT_CLEANUP()   
	PRINTLN("[PLC] SCRIPT_CLEANUP")
	
	FLAG_PLAYER_CONTEXT_IN_TOURNAMENT(FALSE)
	PRINTLN("[PLC] FLAG_PLAYER_CONTEXT_IN_TOURNAMENT(FALSE)")
	SET_PLAYLIST_LEADERBOARD_ACTIVE(FALSE)
	//Stop hiding the HUD
	IF IS_PLAYER_SCTV(PLAYER_ID())
		HIDE_SPECTATOR_HUD(FALSE)
	ENDIF
	
	ARENA_CLEANUP(TRUE, TRUE)
	
	SET_CELEB_BLOCK_RESURRECT_RENDERPHASE_UNPAUSE(FALSE)
		
	//set crap if we finished and are not restarting
	IF CLEANUP_NONE_RESTART_DATA()
		SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_OkToMoveOn)
		SET_PLAYER_ON_A_PLAYLIST(FALSE)
		//SET_CURRENT_FM_MISSION_NEEDS_CLEANUP(TRUE)
		SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_CleanUp)
		PRINTLN("[PLC] SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_CleanUp)")
		SET_LEAVE_MISSION_WITH_EVERYBODY()
		SET_TRANSITION_SESSIONS_RUN_FREEMODE_RELAUNCH()
		SET_ENDED_MISSION_AT_LEADERBOARD_VOTE_OR_TIME_OUT()
		SET_DO_TRANSITION_TO_GAME()
	ENDIF
	
	// If you did not finish the playlist you get a bad sport
 	IF NOT IS_TRANSITION_SESSIONS_PLAYLIST_SAFE_TO_QUIT()
	AND NOT IS_PLAYER_SCTV(PLAYER_ID())
	AND NOT IS_LOCAL_PLAYER_SPECTATOR()
		
		IF HAS_TRANSITION_SESSIONS_PLAYER_QUIT_FROM_HUD()
		OR g_HasCancelledJobOnPhone
		
			// Give the bad sport
			IF IS_THIS_TRANSITION_SESSION_IS_A_LIVESTREAM_PLAYLIST()
				
				PRINTLN("[BCCHEAT] RUN_BAD_SPORT_DEGREDATION: Quitting Event Playlist, increasing Bad Sport level by ", g_sMPTunables.fBadSportQuittingEventPlaylist)

				INCREMENT_BY_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_BADSPORT, g_sMPTunables.fBadSportQuittingEventPlaylist)
			ELSE
				PRINTLN("[BCCHEAT] RUN_BAD_SPORT_DEGREDATION: Quitting Standard Playlist, increasing Bad Sport level by ", g_sMPTunables.fBadSportQuittingPlaylist)

				INCREMENT_BY_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_BADSPORT, g_sMPTunables.fBadSportQuittingPlaylist)
			ENDIF
		#IF IS_DEBUG_BUILD
		ELSE
			PRINTLN("[PLC] Player has not quit from the playlist legitimately")
		#ENDIF
			
		ENDIF
		
	ENDIF
	
	// Clear our flag above
	CLEAR_TRANSITION_SESSIONS_PLAYLIST_HAS_FINISHED()
	CLEAR_TRANSITION_SESSIONS_PLAYLIST_VALID_FOR_BAD_SPORT()
	CLEAR_TRANSITION_SESSIONS_PLAYER_QUIT_FROM_HUD()
	CLEAR_TRANSITION_SESSIONS_PLAYER_PAID_H2H_WAGER()
	
	CLEAR_SC_COMMUNITY_PLAYLIST_LAUNCHING_BY_TYPE(ciQUALIFING_TOURNAMENT_PLAYLIST)
	CLEAR_SC_COMMUNITY_PLAYLIST_LAUNCHING_BY_TYPE(ciEVENT_TOURNAMENT_PLAYLIST)
	CLEAR_SC_COMMUNITY_PLAYLIST_LAUNCHING_BY_TYPE(ciEVENT_COMMUNITY_PLAYLIST_LIVE)
	CLEAR_SC_COMMUNITY_PLAYLIST_LAUNCHING_BY_TYPE(ciEVENT_COMMUNITY_PLAYLIST_SWEEP)
	
	g_sScComunityPlaylist.tl23ActiveEventType = ""
	PRINTLN("[PLC] g_sScComunityPlaylist.tl23ActiveEventType = ", g_sScComunityPlaylist.tl23ActiveEventType)
	g_sCurrentPlayListDetails.iPlaylistProgress = 1
	PRINTLN("[PLC] g_sCurrentPlayListDetails.iPlaylistProgress = ", g_sCurrentPlayListDetails.iPlaylistProgress)
	
	CLEAR_SC_COMMUNITY_PLAYLIST_ACTIVE_TYPE()
	/// Speirs lbd ///
	SET_ON_LBD_GLOBAL(FALSE)
	LBD_SCENE_CLEANUP_PLAYLIST_ONLY()
	CLEANUP_CELEBRATION_SCREEN(sCelebrationData)
	SET_CELEBRATION_SCREEN_AS_ACTIVE(FALSE)
	RESET_WEATHER_AND_TIME_AFTER_CELEBRATION_WINNER() //In case the script terminated before the winner screen cleaned up.
	
	IF DOES_CAM_EXIST(ciCelebrationCam)
		DESTROY_CAM(ciCelebrationCam)
	ENDIF
	
	STOP_AUDIO_SCENE("MP_CELEB_SCREEN_SCENE")
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciENABLE_POWERPLAY_PICKUP_SOUNDS)
		STOP_AUDIO_SCENE("MP_LEADERBOARD_SCENE")
	ELSE
		IF IS_AUDIO_SCENE_ACTIVE("MP_LEADERBOARD_PP_SCENE")
			STOP_AUDIO_SCENE("MP_LEADERBOARD_PP_SCENE")
		ENDIF
	ENDIF
	
	CLEAR_JOB_CHAIN_INT_STRUCT()
	
	///
	BOOL bDoReset
	CLEAR_BIT(g_PauseMenuMissionCreatorData.iBS_PauseMenuFlags, bsPauseMenuStartPlaylist)
	CLEAR_PLAYLIST_BITSET()
	g_sCurrentPlayListDetails.iPlaylistStartPosCache = 0
	g_sCurrentPlayListDetails.iCashBetCache = 0
	IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_CleanUp)
		bDoReset = TRUE
	ENDIF
	GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlayListBitSet = 0
	
	GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iPlaylistWager = ciMIN_CHALLENGE_CASH_BET
		
	g_sCurrentPlayListDetails.iCurrentPlayListPosition = 0
	//Clean up the voice chat

	//CLIENT_SET_UP_PROXIMITY_CHAT(CHAT_PROXIMITY_FM) 
	CLIENT_SET_UP_EVERYONE_CHAT() // 1465640
	
	UNLOAD_ALL_COMMON_LEADERBOARD_ELEMENTS(LBPlacement)
	
	//Clear that we are waiting for the leaderbaord to be set up
	CLEAR_PLAYLIST_LEADERBOARD_BEING_SETTING_UP()
	
	//Clear that I joined the mission late
	CLEAR_JOINED_PLAYLIST_AFTER_INITILISATION()
	
	//If the playlist is restarting
	IF serverBD.bRestartPlayList
		PRINTLN("[PLC] SCRIPT_CLEANUP - Playlist Restarting")
		SET_PLAYER_ON_A_PLAYLIST(TRUE)
		
		SET_TRANSITION_SESSIONS_RUN_FREEMODE_RELAUNCH()
		SET_LEAVE_MISSION_WITH_EVERYBODY()
		SET_TRANSITION_SESSIONS_RESTART_AFTER_DO_TRANSITION_TO_NEW_GAME()   
		SET_TRANSITION_SESSIONS_RESTART_PLAYLIST()	  
		SET_TRANSITION_SESSIONS_RESTART_PLAYLIST_CONTENT_ID(serverBD.tl23PlayListToPlay)
		SET_TRANSITION_SESSIONS_RESTART_PLAYLIST_LOCATION(g_sCurrentPlayListDetails.sLoadedMissionDetails[0].vStartPos)	 
		CALL_NETWORK_SESSION_SET_GAMEMODE_BEFORE_TO_GAME()
		IF NETWORK_DO_TRANSITION_TO_NEW_GAME(TRUE, NUM_NETWORK_PLAYERS, FALSE)
			PRINTLN("[TS] [PLC] NETWORK_DO_TRANSITION_TO_NEW_GAME = TRUE")
		ELSE
			PRINTLN("[TS] [PLC] NETWORK_DO_TRANSITION_TO_NEW_GAME = FALSE")
		ENDIF
		g_sFMMCEOM.bDoWarp = FALSE
		
		//Set up the last vote captian
		PLAYER_INDEX piPlayer = INT_TO_NATIVE(PLAYER_INDEX, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iLastVoteCaptain)
		IF piPlayer != INVALID_PLAYER_INDEX()
			IF NETWORK_IS_PLAYER_ACTIVE(piPlayer)
				g_TransitionSessionNonResetVars.ghLastVoteCaptain = GET_GAMER_HANDLE_PLAYER(piPlayer)		
			#IF IS_DEBUG_BUILD
			ELSE
				PRINTLN("iLastVoteCaptain - RESET_FMMC_MISSION_VARIABLES - NETWORK_IS_PLAYER_ACTIVE = FALSE")
				#ENDIF
			ENDIF
		#IF IS_DEBUG_BUILD
		ELSE
			PRINTLN("iLastVoteCaptain - RESET_FMMC_MISSION_VARIABLES - piPlayer = INVALID_PLAYER_INDEX()")
		#ENDIF
		ENDIF
		
		//g_sFMMCEOM.vWarp = g_sCurrentPlayListDetails.sLoadedMissionDetails[0].vStartPos
	//If's ended and people don't want to restart   
	ELSE
		SET_PLAYER_ON_A_PLAYLIST(FALSE)
		PRINTLN("[PLC] SCRIPT_CLEANUP - Playlist Quitting")
//		IF GET_CORONA_STATUS() = CORONA_STATUS_IDLE 
//		AND NOT IS_PLAYER_SCTV(PLAYER_ID())
//			NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_CLEAR_TASKS  | NSPC_CAN_BE_TARGETTED | NSPC_ALLOW_PLAYER_DAMAGE)
//		ENDIF
		PRINTLN("[PLC] GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlayListInstance = DEFAULT_MISSION_INSTANCE")
		PRINTLN("[PLC] GlobalplayerBD_FM[iMyGBD].iLastVoteCaptain = -1")
		GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iLastVoteCaptain = -1
		IF bDoReset 
			SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_CleanUp)
		ENDIF
		IF NOT AM_I_ACCEPTING_TRANSITION_SESSIONS_INVITE()
		AND NOT AM_I_ACCEPTING_TRANSITION_SESSIONS_INVITE_FROM_MP()
			CLEAR_MY_TRANSITION_SESSION_CONTENT_ID()
		ENDIF
		
		g_sCurrentPlayListDetails.tl31PlaylistName = ""
		g_sCurrentPlayListDetails.tl31szContentID = ""
		g_sCurrentPlayListDetails.iH2HCrewID1 = -1
		g_sCurrentPlayListDetails.iH2HCrewID2 = -1
		
		//set the vote status
		IF HAS_PLAY_LIST_FINISHED()
		OR (SB_PLAYLIST_DOING_CHALLENGE() AND HAS_CHALLENGE_BEEN_FAILED())
			g_sFMMCEOM.iVoteStatus = ciFMMC_EOM_VOTE_STATUS_CONTINUE
			PRINTLN("[PLC] g_sFMMCEOM.iVoteStatus = ciFMMC_EOM_VOTE_STATUS_CONTINUE")
			SET_TRANSITION_SESSION_VOTE_STATUS(ciFMMC_EOM_VOTE_STATUS_CONTINUE)			 
		ELSE
			g_sFMMCEOM.iVoteStatus = ciFMMC_EOM_VOTE_STATUS_QUIT
			SET_TRANSITION_SESSION_VOTE_STATUS(ciFMMC_EOM_VOTE_STATUS_QUIT)			 
			PRINTLN("[PLC] g_sFMMCEOM.iVoteStatus = ciFMMC_EOM_VOTE_STATUS_QUIT")
		ENDIF
		
		//set that it needs all cleaned up
		//SET_CURRENT_FM_MISSION_NEEDS_CLEANUP(TRUE)
		
		//Clear the playlist restart vars
		CLEAR_TRANSITION_SESSIONS_RESTART_PLAYLIST()
		CLEAR_TRANSITION_SESSIONS_RESTART_PLAYLIST_LOCATION()
		CLEAR_TRANSITION_SESSIONS_RESTART_PLAYLIST_CONTENT_ID()
		
		IF NOT IS_TRANSITION_SESSION_QUITING_CORONA()
			CLEAR_MY_PLAYLIST_TEAM()
			SET_PLAYERS_PLAYLIST_INSTANCE(DEFAULT_MISSION_INSTANCE)
			CLEAR_PLAYLIST_MISSION_HAS_LOADED()
			//If the transition session is not cleaning up after a walk out
			IF NOT SHOULD_TRANSITION_SESSIONS_CLEANUP_AFTER_CORONA_WALK_OUT()
			AND NOT IS_TRANSITION_SESSIONS_ACCEPTING_INVITE_WAIT_FOR_CLEAN()
			AND NOT IS_PS4_LIVE_STREAM_LAUNCH_ACTIVE()
				CLEAR_TRANSITION_SESSIONS_BITSET()
				RESET_TRANSITION_SESSION_NON_RESET_VARS()   
			ENDIF
		#IF IS_DEBUG_BUILD
		ELSE
			PRINTLN("[PLC] SCRIPT_CLEANUP - GET_CORONA_STATUS = CORONA_STATUS_WALK_OUT OR CORONA_STATUS_QUIT")
		#ENDIF
		ENDIF
		
	ENDIF
	
	IF NOT IS_TRANSITION_SESSIONS_ACCEPTING_INVITE_WAIT_FOR_CLEAN()
		IF GET_CORONA_STATUS() = CORONA_STATUS_IDLE 
			SET_TRANSITION_SESSIONS_HAS_DONE_INITIAL_TRANSITION()
		ENDIF   
	ELSE
		SET_PLAYLIST_START_POSITION(-1)
	ENDIF   
	
	CLEAR_PLAYLIST_IS_RESTARTING()
	SET_PLAYLIST_LEADERBOARD_ACTIVE(FALSE)
	PRINTLN("Called SET_PLAYLIST_LEADERBOARD_ACTIVE(FALSE)")
	//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bMissionRestartInProgress = FALSE 
	CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bMissionRestartInProgress)
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

//Set the serverbroadcast data
PROC SET_UP_PLAYLIST_TO_LOAD()
	IF NOT IS_BIT_SET(serverBD.iServerSetUpBitSet, ciServer_SET_UP_PlayListNameSet)
		serverBD.tl23PlayListToPlay = g_sCurrentPlayListDetails.tl31szContentID
		PRINTLN("[PLC] serverBD.tl23PlayListToPlay = ", serverBD.tl23PlayListToPlay)
		SET_BIT(serverBD.iServerSetUpBitSet, ciServer_SET_UP_PlayListNameSet)
	ENDIF   
ENDPROC

//Set this mussion as played.
PROC SET_MISSION_AS_PLAYED_IN_PLAYLIST(INT iMission)
	PRINTLN("[PLC] SET_MISSION_AS_PLAYED_IN_PLAYLIST - ", iMission)
	SET_BIT(serverBD.iMissionPlayedBitSet, iMission)
ENDPROC

//Get a rando mission to play in the playlist 
FUNC INT GET_RANDOM_NEXT_IN_PLAYLIST()
	IF serverBD.iLength = 1
		RETURN 0
	ENDIF
	
	//If all done?
	INT iLoop
	INT iPlayed
	FOR iLoop = 0 TO (serverBD.iLength - 1)
		IF IS_BIT_SET(serverBD.iMissionPlayedBitSet, iLoop) 
			iPlayed++
		ENDIF
	ENDFOR
	//If the number played is the playlist length
	IF iPlayed = serverBD.iLength
		RETURN 0
	ENDIF
	
	CONST_INT ciSTARTING_MAX	1000
	PRINTLN("[PLC] GET_RANDOM_NEXT_IN_PLAYLIST")
	BOOL bGotPlayList
	INT iRandom
	
	WHILE bGotPlayList = FALSE
		iRandom = GET_RANDOM_INT_IN_RANGE(ciSTARTING_MAX, ciSTARTING_MAX+serverBD.iLength)
		PRINTLN("[PLC] iRandom = ", iRandom)
		iRandom -= ciSTARTING_MAX
		PRINTLN("[PLC] iRandom = ", iRandom)
		IF NOT IS_BIT_SET(serverBD.iMissionPlayedBitSet, iRandom)
			bGotPlayList = TRUE
			PRINTLN("[PLC] RETURN ", iRandom)
			RETURN iRandom
		ELSE
			PRINTLN("[PLC] GET_RANDOM_NEXT_IN_PLAYLIST")
			PRINTLN("[PLC] IS_BIT_SET(serverBD.iMissionPlayedBitSet, ", iRandom, ")")
		ENDIF
	ENDWHILE

	RETURN iRandom
ENDFUNC

PROC CLEAR_LEADERBOARD_SLOT_DATA(PLAYER_INDEX tempPlayer)
INT i
	FOR i = 0 TO (NUM_NETWORK_PLAYERS-1)
		IF serverBD_LB.sleaderboard[i].playerID != INVALID_PLAYER_INDEX()
			IF serverBD_LB.sleaderboard[i].playerID = tempPlayer
				NET_PRINT("CLEARING TO OLD LEADERBOAR SLOT FOR PLAYER: ") NET_PRINT_INT(NATIVE_TO_INT(serverBD_LB.sleaderboard[i].playerID)) NET_NL()
				serverBD_LB.sleaderboard[i].playerID = INVALID_PLAYER_INDEX()
				IF serverBD_LB.sleaderboard[i].iparticipant !=-1
					tParticipantNames[serverBD_LB.sleaderboard[i].iparticipant] = ""
				ENDIF
				serverBD_LB.sleaderboard[i].iparticipant =-1
				serverBD_LB.sleaderboard[i].iteam = -1
				serverBD_LB.sleaderboard[i].iPlayerScore = 0
				serverBD_LB.sleaderboard[i].iCurrentPlaylistScore = 0
				serverBD_LB.sleaderboard[i].iMissionTime = 0
				serverBD_LB.sleaderboard[i].iKills = 0
				serverBD_LB.sleaderboard[i].ideaths =0
				serverBD_LB.sleaderboard[i].fKDRatio =0
				serverBD_LB.sleaderboard[i].iHeadShots = 0
				serverBD_LB.sleaderboard[i].iBadgeRank = 0
				serverBD_LB.sleaderboard[i].iArenaPoints = 0
//				serverBD_LB.sleaderboard[i].bInitialise = FALSE
				CLEAR_BIT(serverBD_LB.sleaderboard[i].iLbdBitSet, ciLBD_BIT_INITIALISE)
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

PROC SERVER_INIT_PLAYERS()
	INT i
	FOR i = 0 TO (NUM_NETWORK_PLAYERS-1)
		serverBD_LB.sleaderboard[i].playerID = INVALID_PLAYER_INDEX()
	ENDFOR
	PRINTLN("[2001564] SERVER_INIT_PLAYERS ")
ENDPROC

//Do necessary pre game start ini.
PROC PROCESS_PRE_GAME(MP_MISSION_DATA fmmcMissionData)
	INT iLoop 
	
	//Clear the team crew
	serverBD.iTeamCrew[0] = -1
	serverBD.iTeamCrew[1] = -1
	
	//Cleans up the old values for the leaderboard
	INT iRepeat
	PRINTLN("[PLC] PROCESS_PRE_GAME - g_TransitionSessionNonResetVars.sPLLB_Globals.iEventscore")
	REPEAT NUM_NETWORK_PLAYERS iRepeat
		g_TransitionSessionNonResetVars.sPLLB_Globals.iEventscore[iRepeat] = 0
	 	g_TransitionSessionNonResetVars.sPLLB_Globals.iEventtime[iRepeat] = 0
	ENDREPEAT	
	//Set the cash winnings to 0 so this can be set at the end of the session when the server decides that it has finished. 
	g_sCurrentPlayListDetails.iCashWinnings = 0
	PRINTLN("[PLC] g_sCurrentPlayListDetails.iCashWinnings  = 0")
	// This marks the script as a net script, and handles any instancing setup. 
	NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE, -1)   
	SERVER_INIT_PLAYERS()
	// This makes sure the net script is active, waits until it is.
	HANDLE_NET_SCRIPT_INITIALISATION()
	//Register the Server Boradcast data
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD_LB, SIZE_OF(serverBD_LB))
	//Register the Player Boradcast data
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))
	SERVER_INIT_PLAYERS()
	
	// KGM: Wait for the first network broadcast before moving on
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		PRINTLN("[PLC] PROCESS_PRE_GAME - FAILED TO RECEIVE AN INITIAL NETWORK BROADCAST - CLEANING UP")
		SCRIPT_CLEANUP()
	ENDIF
	
	PRINTLN("[PLC] PROCESS_PRE_GAME - fmmcMissionData.mdID.idCreator = ", fmmcMissionData.mdID.idCreator)
	PRINTLN("[PLC] PROCESS_PRE_GAME - fmmcMissionData.iInstanceId	= ", fmmcMissionData.iInstanceId)
	
	//Set a sweet stat for the number of playlists that we've done. 
	SET_MP_BOOL_CHARACTER_STAT(MP_STAT_CL_PLAY_A_PLAYLIST, TRUE)
	PRINTLN("[PLC] PROCESS_PRE_GAME - SET_MP_BOOL_CHARACTER_STAT - MP_STAT_CL_PLAY_A_PLAYLIST")
	
	// Clear the playlist has finished flags
	CLEAR_TRANSITION_SESSIONS_PLAYLIST_HAS_FINISHED()
	CLEAR_TRANSITION_SESSIONS_PLAYER_QUIT_FROM_HUD()
	
	IF IS_PLAYLIST_DOING_CHALLENGE()
		nTYPE = UGC_TYPE_GTA5_CHALLENGE
	ELSE
		nTYPE = UGC_TYPE_GTA5_MISSION_PLAYLIST
	ENDIF
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		serverBD.iInstanceId					= fmmcMissionData.iInstanceId
		IF fmmcMissionData.mdID.idCreator != -1
		AND fmmcMissionData.mdID.idCreator < FMMC_MAX_PLAY_LIST_LENGTH
			serverBD.iCurrentPlayListPosition	   = fmmcMissionData.mdID.idCreator
		ENDIF
		serverBD.iLength					= g_sCurrentPlayListDetails.iLength
		IF IS_PLAYLIST_IS_A_TOURNAMENT_PLAYLIST()	
			BOOL bDmTournament = TRUE
			FOR iLoop = 0 TO (g_sCurrentPlayListDetails.iLength - 1)
				IF g_sCurrentPlayListDetails.sLoadedMissionDetails[iLoop].iType != FMMC_TYPE_DEATHMATCH
					bDmTournament = FALSE
				ENDIF
			ENDFOR
			serverBD.bDmTournament				= bDmTournament
		ENDIF
		serverBD.bPlaylistDoingChallenge	= IS_PLAYLIST_DOING_CHALLENGE()
		serverBD.bPlaylistShuffeled		 	= IS_PLAYLIST_SHUFFELED()
		serverBD.bQualifingPlaylist		 	= IS_PLAYLIST_IS_A_QUALIFYING_PLAYLIST()		
		serverBD.bIsTournamentPlaylist		= IS_PLAYLIST_IS_A_TOURNAMENT_PLAYLIST()
		serverBD.bIsSctvControlledPlaylist	= IS_PLAYLIST_IS_A_SCTV_CONTROLLED_PLAYLIST()
		serverBD.bIsFinalRoundPlaylist		= IS_THIS_TOURNAMENT_A_FINAL_ROUND()
		serverBD.bIsLiveStreamPlaylist		= IS_PLAYLIST_IS_A_LIVESTREAM_PLAYLIST()		
		serverBD.bPlaylistSettingChallenge  = IS_PLAYLIST_SETTING_CHALLENGE_TIME()
		serverBD.bPlaylistDoingHeadToHead   = IS_PLAYLIST_DOING_HEAD_TO_HEAD()
		serverBD.iMacaddresshash = g_TransitionSessionNonResetVars.sJobChain.iMacaddresshash
		serverBD.iPosixtime = g_TransitionSessionNonResetVars.sJobChain.iPosixtime
		IF SB_PLAYLIST_SETTING_CHALLENGE_TIME()
			serverBD.iCashBet	   = g_sCurrentPlayListDetails.iCashBet
		ELIF SB_PLAYLIST_DOING_CHALLENGE()
//		  serverBD.iScoreToBeat  = g_sCurrentPlayListDetails.iTotatlScore
			serverBD.iTimeToBeat	= g_sCurrentPlayListDetails.iTotatlTime
			serverBD.iScoreToBeat   = g_sCurrentPlayListDetails.iTotatlScore
			serverBD.iCashBet	   = g_sCurrentPlayListDetails.iCashBet
			FOR iLoop = 0 TO (serverBD.iLength -1)
				serverBD.iBestScore[iLoop] = g_sCurrentPlayListDetails.sLoadedMissionDetails[iLoop].iBestScore
				serverBD.iBestTime[iLoop] = g_sCurrentPlayListDetails.sLoadedMissionDetails[iLoop].iBestTime
			ENDFOR
		ELIF SB_PLAYLIST_DOING_HEAD_TO_HEAD()
			serverBD.iTeamCrew[0]	   = GET_PLAYER_CREW_INT_CL(PLAYER_ID())
			serverBD.iNumPlaylistTeams  = MAX_HEAD_TO_HEAD_CREWS
			serverBD.iCashBet		   = g_sCurrentPlayListDetails.iCashBet
		//serverBD.bPlaylistShuffeled = TRUE
		ELIF SB_PLAYLIST_SHUFFELED()
			//IF serverBD.iCurrentPlayListPosition = 0
			//	serverBD.iCurrentPlayListPosition = GET_RANDOM_NEXT_IN_PLAYLIST()
			//ENDIF
		ENDIF
		//If it's not shuffeled 
		IF NOT SB_PLAYLIST_SHUFFELED()
			//and we are starting on a mission greater than 0
			IF serverBD.iCurrentPlayListPosition > 0 
				//Then set the number of missions played 
				serverBD.iNumberOfMissionsPlayed = serverBD.iCurrentPlayListPosition
				PRINTLN("[PLC] serverBD.iCurrentPlayListPosition > 0 ")
				PRINTLN("[PLC] serverBD.iNumberOfMissionsPlayed = ", serverBD.iNumberOfMissionsPlayed)
			ENDIF
		ENDIF
		SET_MISSION_AS_PLAYED_IN_PLAYLIST(serverBD.iCurrentPlayListPosition)
		#IF IS_DEBUG_BUILD
		IF serverBD.bPlaylistDoingChallenge
		AND serverBD.bPlaylistSettingChallenge
			PRINTLN("[PLC] DoingChallenge AND SettingChallenge are both TRUE Some thing broke, bug for bobby wright")
			SCRIPT_ASSERT("DoingChallenge AND SettingChallenge are both TRUE Some thing broke, bug for bobby wright")
		ENDIF
		PRINTLN("[PLC] serverBD.iPosixtime				  = ", serverBD.iPosixtime)
		PRINTLN("[PLC] serverBD.iMacaddresshash			  = ", serverBD.iMacaddresshash)
		PRINTLN("[PLC] serverBD.iLength				      = ", serverBD.iLength)
		PRINTLN("[PLC] serverBD.iCashBet				  = ", serverBD.iCashBet)	   
		PRINTLN("[PLC] serverBD.bDmTournament    		  = ", serverBD.bDmTournament)	   
		PRINTLN("[PLC] serverBD.iCurrentPlayListPosition  = ", serverBD.iCurrentPlayListPosition)
		
		PRINTLN("[PLC] serverBD.bPlaylistShuffeled	      = ", serverBD.bPlaylistShuffeled)
		PRINTLN("[PLC] serverBD.bPlaylistDoingChallenge   = ", serverBD.bPlaylistDoingChallenge)
		PRINTLN("[PLC] serverBD.bPlaylistSettingChallenge = ", serverBD.bPlaylistSettingChallenge)
		PRINTLN("[PLC] serverBD.bPlaylistDoingHeadToHead  = ", serverBD.bPlaylistDoingHeadToHead)
		PRINTLN("[PLC] serverBD.bQualifingPlaylist		  = ", serverBD.bQualifingPlaylist)
		PRINTLN("[PLC] serverBD.bIsTournamentPlaylist	  = ", serverBD.bIsTournamentPlaylist)
		PRINTLN("[PLC] serverBD.bIsSctvControlledPlaylist = ", serverBD.bIsSctvControlledPlaylist)
		PRINTLN("[PLC] serverBD.bIsFinalRoundPlaylist	  = ", serverBD.bIsFinalRoundPlaylist)
		PRINTLN("[PLC] serverBD.bIsLiveStreamPlaylist	  = ", serverBD.bIsLiveStreamPlaylist)
		PRINTLN("[PLC] serverBD.bQualifingPlaylist	 	  = ", serverBD.bQualifingPlaylist)
		IF SB_PLAYLIST_DOING_CHALLENGE()
			PRINTLN("[PLC] serverBD.iScoreToBeat			  = ", serverBD.iScoreToBeat)
			PRINTLN("[PLC] serverBD.iTimeToBeat			   = ", serverBD.iTimeToBeat)
			FOR iLoop = 0 TO (serverBD.iLength -1)
				PRINTLN("[PLC] serverBD.iBestScore[", iLoop, "] = ", serverBD.iBestScore[iLoop])
				PRINTLN("[PLC] serverBD.iBestTime[", iLoop, "]  = ", serverBD.iBestTime[iLoop])
			ENDFOR
		ENDIF
		#ENDIF
		SET_UP_PLAYLIST_TO_LOAD()
	ENDIF   
	
	PRINTLN("[PLC] serverBD.tl23PlayListToPlay = ", serverBD.tl23PlayListToPlay)
	
	//clean the playlist scores
	FOR iLoop = 0 TO (NUM_NETWORK_PLAYERS - 1)					
		g_TransitionSessionNonResetVars.sPLLB_Globals.iChallengeScore[iLoop] = 0
	ENDFOR
	
	//Did I JIP on Leaderboard
	BOOL bJippEdOnLb = DID_THIS_PLAYER_PLAYLIST_JIPPED_WHEN_ON_PLAYLIST_LB(PLAYER_ID()) 
	
	
	CLEAR_PLAYLIST_DONE_FIRST_MISSION()
	CLEAR_MY_PLAYLIST_TEAM()
	GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlayListBitSet = 0
	SET_PLAYER_ON_A_PLAYLIST(TRUE)
	RESET_PAUSE_MENU_WARP_REQUESTS()

	//restor LB as needed
	IF bJippEdOnLb
		SET_PLAYLIST_JIPPED_WHEN_ON_PLAYLIST_LB()
		bPlayedFullPlayList = FALSE
		PRINTLN("[PLC] PLAYLIST_EVENT_GIFT_ACTIVE bJippEdOnLb - bPlayedFullPlayList = FALSE")
	ENDIF
	
	g_sCurrentPlayListDetails.iPlaylistStartPos = serverBD.iNumberOfMissionsPlayed
	
	//Doing a challeng then incrament the stat
	IF IS_PLAYLIST_DOING_CHALLENGE()
		INCREMENT_MP_INT_PLAYER_STAT(MPPLY_CREW_CHALLENGE_ATTEMPTS)
		PRINTLN("[PLC] INCREMENT_MP_INT_PLAYER_STAT(MPPLY_CREW_CHALLENGE_ATTEMPTS)")
	ENDIF
	
	// Check if we have joined a launched playlist and update player array
	IF DID_TRANSITION_SESSION_JOINED_LAUNCHED_PLAYLIST_FOR_CORONA()
		PRINTLN("[PLC] SET_UP_PLAYER_LIST_AFTER_JIP_PLAYLIST - fmmc_playlist_controller.sc")
		SET_UP_PLAYER_LIST_AFTER_JIP_PLAYLIST()
		CLEAR_TRANSITION_SESSION_JOINED_LAUNCHED_PLAYLIST_FOR_CORONA()
		b_DoneQualifyingPlaylistFeedMessage = TRUE
		b_DoQualifyingPlaylistWontCountFeedMessage = TRUE
		bPlayedFullPlayList = FALSE
		PRINTLN("[PLC] PLAYLIST_EVENT_GIFT_ACTIVE DID_TRANSITION_SESSION_JOINED_LAUNCHED_PLAYLIST_FOR_CORONA - bPlayedFullPlayList = FALSE")
	ENDIF
	
	//did we join a launched playlist
	IF DID_TRANSITION_SESSION_JOINED_LAUNCHED_PLAYLIST()
	OR IS_PLAYER_SCTV(PLAYER_ID())	
		PRINTLN("[PLC] DID_TRANSITION_SESSION_JOINED_LAUNCHED_PLAYLIST = TRUE")
		CLEAR_TRANSITION_SESSION_JOINED_LAUNCHED_PLAYLIST()
//		CLEAR_LEADERBOARD_SLOT_DATA(PLAYER_ID())
		IF PARTICIPANT_ID_TO_INT() != -1
			INT i
			FOR i = 0 TO (NUM_NETWORK_PLAYERS-1)
				IF serverBD_LB.sleaderboard[i].playerID != INVALID_PLAYER_INDEX()
					IF serverBD_LB.sleaderboard[i].playerID = PLAYER_ID()
						NET_PRINT("CLEARING TO OLD LEADERBOAR SLOT FOR PLAYER: ") NET_PRINT_INT(NATIVE_TO_INT(serverBD_LB.sleaderboard[i].playerID)) NET_NL()
						SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, ciPLAYER_CLEAR_MY_LB_DATA_PLEASE)
						PRINTLN("[PLC] ciPLAYER_CLEAR_MY_LB_DATA_PLEASE, SET_BIT(playerBD[", PARTICIPANT_ID_TO_INT(), "].iPlayerBitSet, ciPLAYER_CLEAR_MY_LB_DATA_PLEASE)")
					ENDIF
				ENDIF
			ENDFOR
		ENDIF
		g_TransitionSessionNonResetVars.sJobChain.iMacaddresshash = serverBD.iMacaddresshash
		g_TransitionSessionNonResetVars.sJobChain.iPosixtime = serverBD.iPosixtime
		PRINTLN("[PLC] g_TransitionSessionNonResetVars.sJobChain.iMacaddresshash = ", g_TransitionSessionNonResetVars.sJobChain.iMacaddresshash)
		PRINTLN("[PLC] g_TransitionSessionNonResetVars.sJobChain.iPosixtime = ", g_TransitionSessionNonResetVars.sJobChain.iPosixtime)
		g_sCurrentPlayListDetails.iPlaylistStartPos = serverBD.iNumberOfMissionsPlayed + 1
		//bLaunchedPlaylist = TRUE
		SET_FM_PLAYLIST_STATE(playerBD, PLAYLIST_STATE_WAIT_TILL_OFF_ROUNDS_MISSION)		
		PRINTLN("[PLC] playerBD[PARTICIPANT_ID_TO_INT()].iPlayListState = PLAYLIST_STATE_WAIT_TILL_OFF_ROUNDS_MISSION")	
		SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_JoinedLaunched_Playlist)
		b_DoneQualifyingPlaylistFeedMessage = TRUE
		b_DoQualifyingPlaylistWontCountFeedMessage = TRUE
		SET_PLAYLIST_DONE_FIRST_MISSION()
		bPlayedFullPlayList = FALSE
		PRINTLN("[PLC] PLAYLIST_EVENT_GIFT_ACTIVE DID_TRANSITION_SESSION_JOINED_LAUNCHED_PLAYLIST - bPlayedFullPlayList = FALSE")
	ENDIF
	
	IF g_TransitionSessionNonResetVars.sJobChain.iMacaddresshash = 0
	AND g_TransitionSessionNonResetVars.sJobChain.iPosixtime = 0
	AND serverBD.iMacaddresshash != 0
	AND serverBD.iPosixtime != 0
		g_TransitionSessionNonResetVars.sJobChain.iMacaddresshash = serverBD.iMacaddresshash
		g_TransitionSessionNonResetVars.sJobChain.iPosixtime = serverBD.iPosixtime
		PRINTLN("[TEL] PLAYSTATS_JOB_CHAIN - PROCESS_PRE_GAME - g_TransitionSessionNonResetVars.sJobChain.iMacaddresshash     = ", g_TransitionSessionNonResetVars.sJobChain.iMacaddresshash)	
		PRINTLN("[TEL] PLAYSTATS_JOB_CHAIN - PROCESS_PRE_GAME - g_TransitionSessionNonResetVars.sJobChain.iPosixtime          = ", g_TransitionSessionNonResetVars.sJobChain.iPosixtime)	
	ENDIF
	
	
	PRINTLN("[PLC] g_sCurrentPlayListDetails.iPlaylistStartPos = ", g_sCurrentPlayListDetails.iPlaylistStartPos)
	
	IF IS_STRING_NULL_OR_EMPTY(g_sCurrentPlayListDetails.tl31ChallengeName)
		g_sCurrentPlayListDetails.tl31ChallengeName = g_sCurrentPlayListDetails.tl31PlaylistName
	ENDIF
	//playerBD[PARTICIPANT_ID_TO_INT()].tl31ChallengeName = g_sCurrentPlayListDetails.tl31ChallengeName
	PRINTLN("[PLC] g_sCurrentPlayListDetails.tl31ChallengeName = ", g_sCurrentPlayListDetails.tl31ChallengeName)	
	
	SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	
	//If we quit the current playlist
	IF TRANSITION_SESSIONS_QUIT_CURRENT_PLAYLIST()
		CLEAR_TRANSITION_SESSIONS_QUIT_CURRENT_PLAYLIST()
	ENDIF
	
	IF SB_IS_QUALIFYING_PLAYLIST()
		SET_SC_COMMUNITY_PLAYLIST_ACTIVE_TYPE(ciQUALIFING_TOURNAMENT_PLAYLIST)
		g_sQualifyTourFeed.iTotalTime = 0
		IF b_DoQualifyingPlaylistWontCountFeedMessage
			PRINTLN("[PLC] MAINTAIN_TORUNAMENT_FEED_MESSAGES - b_DoQualifyingPlaylistWontCountFeedMessage = TRUE")
			PRINTLN("[PLC] MAINTAIN_TORUNAMENT_FEED_MESSAGES - g_sQualifyTourFeed.bFailedToSetTime = TRUE")
			g_sQualifyTourFeed.bFailedToSetTime = TRUE
			g_sQualifyTourFeed.bSetTime = FALSE		
		ELSE
			g_sQualifyTourFeed.bSetTime = TRUE		
			g_sQualifyTourFeed.bFailedToSetTime = FALSE		 
		ENDIF
	ELIF IS_PLAYLIST_IS_A_TOURNAMENT_PLAYLIST()
		SET_SC_COMMUNITY_PLAYLIST_ACTIVE_TYPE(ciEVENT_TOURNAMENT_PLAYLIST)
		//Set the flag to say that we've been in a tournament. 
		IF NOT IS_PLAYER_SCTV(PLAYER_ID())
			IF NOT GET_MP_BOOL_CHARACTER_STAT(MP_STAT_TOURNAMENT_1_PARTICIPANT)
				PRINTLN("[PLC] PROCESS_PRE_GAME - setting MP_STAT_TOURNAMENT_1_PARTICIPANT")
				SET_MP_BOOL_CHARACTER_STAT(MP_STAT_TOURNAMENT_1_PARTICIPANT, TRUE)
			ENDIF
		ENDIF
		FLAG_PLAYER_CONTEXT_IN_TOURNAMENT(TRUE)
		PRINTLN("[PLC] FLAG_PLAYER_CONTEXT_IN_TOURNAMENT(TRUE)")
	ELIF IS_PLAYLIST_IS_A_LIVESTREAM_PLAYLIST()
		SET_SC_COMMUNITY_PLAYLIST_ACTIVE_TYPE(ciEVENT_COMMUNITY_PLAYLIST_LIVE)
	ENDIF
	
	g_sScComunityPlaylist.tl23ActiveEventType = GET_LEADER_BOARD_ENVENT_TYPE_STRING()
	
	IF serverBD.bIsTournamentPlaylist
	AND NOT IS_PLAYLIST_IS_A_TOURNAMENT_PLAYLIST()
		PRINTLN("[PLC] serverBD.bIsTournamentPlaylist = TRUE but IS_PLAYLIST_IS_A_TOURNAMENT_PLAYLIST = FALSE ")
		SET_PLAYLIST_IS_A_TOURNAMENT_PLAYLIST()
	ENDIF
	
	PRINTLN("[PLC] g_sScComunityPlaylist.tl23ActiveEventType = ", g_sScComunityPlaylist.tl23ActiveEventType)	
		
ENDPROC

//Maintain the syncing of the Challenge name
PROC MAINTAIN_SYNCING_CHALLENGE_NAME()   
//	///If it's not set up
//	IF serverBD.bNameSet = FALSE
//		//And we're doing a challenge
//		IF SB_PLAYLIST_SETTING_CHALLENGE_TIME()
//			//And we're out the corona
//			IF GET_CORONA_STATUS() = CORONA_STATUS_IDLE
//				//And it's not been set
//				IF IS_STRING_NULL_OR_EMPTY(serverBD.tl31ChallengeName)
//					//If we've got a last corona host
//					IF GET_PLAYERS_HOST_OF_LAST_CORONA(PLAYER_ID()) != -1
//						//Get the player
//						PLAYER_INDEX piPlayer = INT_TO_NATIVE(PLAYER_INDEX, GET_PLAYERS_HOST_OF_LAST_CORONA(PLAYER_ID()))
//						IF piPlayer != INVALID_PLAYER_INDEX()
//							//if they are active
//							IF NETWORK_IS_PLAYER_ACTIVE(piPlayer)
//								//And they are on this script
//								IF NETWORK_IS_PLAYER_A_PARTICIPANT(piPlayer)
//									PARTICIPANT_INDEX 	partTemp  = NETWORK_GET_PARTICIPANT_INDEX(piPlayer)
//									IF NOT IS_STRING_NULL_OR_EMPTY(playerBD[NATIVE_TO_INT(partTemp)].tl31ChallengeName)
//										serverBD.tl31ChallengeName = playerBD[NATIVE_TO_INT(partTemp)].tl31ChallengeName
//										serverBD.bNameSet = TRUE						
//										PRINTLN("[PLC] MAINTAIN_SYNCING_CHALLENGE_NAME - NETWORK_IS_PLAYER_A_PARTICIPANT = TRUE")
//										PRINTLN("[PLC] MAINTAIN_SYNCING_CHALLENGE_NAME - serverBD.tl31ChallengeName = ", serverBD.tl31ChallengeName)	
//									ENDIF
//								ELSE
//									PRINTLN("[PLC] MAINTAIN_SYNCING_CHALLENGE_NAME - NETWORK_IS_PLAYER_A_PARTICIPANT = FALSE")
//									serverBD.tl31ChallengeName = playerBD[PARTICIPANT_ID_TO_INT()].tl31ChallengeName
//									serverBD.bNameSet = TRUE
//									PRINTLN("[PLC] MAINTAIN_SYNCING_CHALLENGE_NAME - serverBD.tl31ChallengeName = ", serverBD.tl31ChallengeName)  
//								ENDIF
//							ELSE
//								PRINTLN("[PLC] MAINTAIN_SYNCING_CHALLENGE_NAME NETWORK_IS_PLAYER_ACTIVE = FALSE")
//								serverBD.tl31ChallengeName = playerBD[PARTICIPANT_ID_TO_INT()].tl31ChallengeName
//								serverBD.bNameSet = TRUE
//								PRINTLN("[PLC] MAINTAIN_SYNCING_CHALLENGE_NAME - serverBD.tl31ChallengeName = ", serverBD.tl31ChallengeName)	
//							ENDIF
//						#IF IS_DEBUG_BUILD
//						ELSE
//						PRINTLN("[PLC] MAINTAIN_SYNCING_CHALLENGE_NAME - piPlayer = INVALID_PLAYER_INDEX()")
//						#ENDIF
//						ENDIF
//					ELSE
//						PRINTLN("[PLC] MAINTAIN_SYNCING_CHALLENGE_NAME GET_PLAYERS_HOST_OF_LAST_CORONA = -1")
//						serverBD.tl31ChallengeName = playerBD[PARTICIPANT_ID_TO_INT()].tl31ChallengeName
//						serverBD.bNameSet = TRUE
//						PRINTLN("[PLC] MAINTAIN_SYNCING_CHALLENGE_NAME - serverBD.tl31ChallengeName = ", serverBD.tl31ChallengeName)	
//					ENDIF
//				ENDIF
//			ENDIF
//		ELSE
//			serverBD.bNameSet = TRUE		
//		ENDIF
//	ENDIF
ENDPROC

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////					Set the stage					//////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
PROC SET_FM_PLAYLIST_PLAYER_GAME_STATE(PlayerBroadcastData &PBDpassed[], INT msmsPassed)
	#IF IS_DEBUG_BUILD  
		IF msmsPassed = GAME_STATE_INI
			NET_PRINT("[PLC] SET_FM_PLAYLIST_PLAYER_GAME_STATE - GAME_STATE_INI") NET_NL()
		ELIF msmsPassed = GAME_STATE_SET_UP_PLAYER_DATA
			NET_PRINT("[PLC] SET_FM_PLAYLIST_PLAYER_GAME_STATE - GAME_STATE_SET_UP_PLAYER_DATA") NET_NL()
		ELIF msmsPassed = GAME_STATE_LOAD_FROM_CLOUD
			NET_PRINT("[PLC] SET_FM_PLAYLIST_PLAYER_GAME_STATE - GAME_STATE_LOAD_FROM_CLOUD") NET_NL()
		ELIF msmsPassed = GAME_STATE_RUNNING
			NET_PRINT("[PLC] SET_FM_PLAYLIST_PLAYER_GAME_STATE - GAME_STATE_RUNNING") NET_NL()
		ELIF msmsPassed = GAME_STATE_END_LEADER_BOARD
			NET_PRINT("[PLC] SET_FM_PLAYLIST_PLAYER_GAME_STATE - GAME_STATE_END_LEADER_BOARD") NET_NL()
		ELIF msmsPassed = GAME_STATE_PLAYLIST_RESTART
			NET_PRINT("[PLC] SET_FM_PLAYLIST_PLAYER_GAME_STATE - GAME_STATE_PLAYLIST_RESTART") NET_NL()	 
			
		ELIF msmsPassed = GAME_STATE_END
			NET_PRINT("[PLC] SET_FM_PLAYLIST_PLAYER_GAME_STATE - GAME_STATE_END") NET_NL()  
		ENDIF 
	#ENDIF
	PBDpassed[PARTICIPANT_ID_TO_INT()].iGameState = msmsPassed
ENDPROC

PROC SET_FM_PLAYLIST_SERVER_GAME_STATE(ServerBroadcastData &SBDpassed, INT msmsPassed)
	#IF IS_DEBUG_BUILD  
		IF msmsPassed = GAME_STATE_INI
			NET_PRINT("RCC... SET_FM_PLAYLIST_SERVER_GAME_STATE - GAME_STATE_INI") NET_NL()
		ELIF msmsPassed = GAME_STATE_LOAD_FROM_CLOUD
			NET_PRINT("RCC... SET_FM_PLAYLIST_SERVER_GAME_STATE- GAME_STATE_LOAD_FROM_CLOUD") NET_NL()
		ELIF msmsPassed = GAME_STATE_RUNNING
			NET_PRINT("RCC... SET_FM_PLAYLIST_SERVER_GAME_STATE - GAME_STATE_RUNNING") NET_NL()
		ELIF msmsPassed = GAME_STATE_END_LEADER_BOARD
			NET_PRINT("RCC... SET_FM_PLAYLIST_SERVER_GAME_STATE - GAME_STATE_END_LEADER_BOARD") NET_NL()
		ELIF msmsPassed = GAME_STATE_END
			NET_PRINT("RCC... SET_FM_PLAYLIST_SERVER_GAME_STATE - GAME_STATE_END") NET_NL()
		ENDIF 
	#ENDIF
	SBDpassed.iGameState = msmsPassed
ENDPROC

FUNC BOOL PROCESS_PLAYLIST_OVERVIEW_CUT(JOB_INTRO_CUT_DATA &jobIntroDataPassed, BOOL &bSwitchPassed)

	DISABLE_ALL_MP_HUD_THIS_FRAME()
	//HIDE_HUD_AND_RADAR_THIS_FRAME()
	
	SWITCH jobIntroDataPassed.jobIntroStage
	
		CASE JOB_CUT_LOAD

			//DISABLE_SELECTOR()

			VECTOR cameraStartPos
			FLOAT fCameraXRot
			
			cameraStartPos = g_FMMC_STRUCT.vCameraPanPos
			fCameraXRot = 0.0
						
			IF IS_VECTOR_ZERO(cameraStartPos)
				NET_PRINT("[JA@RACEINTRO] PROCESS_RACE_OVERVIEW_CUT - camera start position is ZERO, use starting grid") NET_NL()		   
				cameraStartPos = g_FMMC_STRUCT.vStartingGrid
			ENDIF
			IF IS_VECTOR_ZERO(cameraStartPos)
				NET_PRINT("[JA@RACEINTRO] PROCESS_RACE_OVERVIEW_CUT - camera start position is ZERO, use starting grid") NET_NL()		   
				cameraStartPos = g_FMMC_STRUCT.vStartPos
			ENDIF
			
			// Set up our cutscene for basejumps
			IF IS_VECTOR_ZERO(cameraStartPos)
				cameraStartPos = GET_PLAYER_COORDS(PLAYER_ID())
				fCameraXRot = -45.0
				g_FMMC_STRUCT.fCameraPanHead = GET_ENTITY_HEADING(PLAYER_PED_ID())
			ENDIF
			cameraStartPos.z += 8.0
			
			NET_PRINT("[JA@RACEINTRO] PROCESS_RACE_OVERVIEW_CUT - Camera start position: ") NET_PRINT_VECTOR(cameraStartPos) NET_NL()
			
			IF NOT DOES_CAM_EXIST(jobIntroDataPassed.jobIntroCam)
				jobIntroDataPassed.jobIntroCam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", FALSE)
			ENDIF

			SET_CAM_ACTIVE(jobIntroDataPassed.jobIntroCam, TRUE)
			SET_CAM_PARAMS(jobIntroDataPassed.jobIntroCam, cameraStartPos, <<fCameraXRot,0,g_FMMC_STRUCT.fCameraPanHead + 90>>, 40.0)
			SET_CAM_PARAMS(jobIntroDataPassed.jobIntroCam, cameraStartPos, <<fCameraXRot,0,g_FMMC_STRUCT.fCameraPanHead - 90>>, 40.0, 16000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
			
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			
			RESET_NET_TIMER(jobIntroDataPassed.jobIntroTimer)
			START_NET_TIMER(jobIntroDataPassed.jobIntroTimer)
			
			CLEAR_HELP()
			bSwitchPassed = FALSE
			
			jobIntroDataPassed.jobIntroStage = JOB_CUT_PLAY
		BREAK
		
		CASE JOB_CUT_PLAY
			IF (NOT IS_CAM_INTERPOLATING(jobIntroDataPassed.jobIntroCam)
			OR HAS_NET_TIMER_EXPIRED(jobIntroDataPassed.jobIntroTimer, 16000))
			#IF IS_DEBUG_BUILD
			AND NOT bDelayRaceIntroCleanup
			#ENDIF
				NET_PRINT("[JA@RACEINTRO] PROCESS_RACE_OVERVIEW_CUT - time up, return true so cut finished") NET_NL()
				VECTOR vCamRot 
				vCamRot = GET_CAM_ROT(jobIntroDataPassed.jobIntroCam)
				cameraStartPos  = GET_CAM_COORD(jobIntroDataPassed.jobIntroCam)
				IF bSwitchPassed = FALSE
					SET_CAM_PARAMS(jobIntroDataPassed.jobIntroCam, cameraStartPos, <<vCamRot.x, 0, vCamRot.x - 180>>, 40.0, 16000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
					bSwitchPassed = TRUE
				ELSE
					SET_CAM_PARAMS(jobIntroDataPassed.jobIntroCam, cameraStartPos, <<vCamRot.x, 0, vCamRot.x + 180>>, 40.0, 16000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
					bSwitchPassed = FALSE
				ENDIF
				RESET_NET_TIMER(jobIntroDataPassed.jobIntroTimer)
				START_NET_TIMER(jobIntroDataPassed.jobIntroTimer)	   
			ENDIF
		BREAK
			
	ENDSWITCH

	RETURN FALSE
ENDFUNC


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////					 LEADER BOARD					//////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
FUNC INT GET_TEAM_FINISH_POSITION(INT iteam)
	
	IF serverBD.iNumPlaylistTeams = 4
		IF serverBD.iWinningTeam = iteam
			RETURN 1
		ELIF serverBD.iSecondTeam = iteam
			RETURN 2
		ELIF serverBD.iThirdTeam = iteam
			RETURN 3
		ELSE
			RETURN 4
		ENDIF
	ELIF serverBD.iNumPlaylistTeams = 3
		IF serverBD.iWinningTeam = iteam
			RETURN 1
		ELIF serverBD.iSecondTeam = iteam
			RETURN 2
		ELSE
			RETURN 3
		ENDIF
	ELIF serverBD.iNumPlaylistTeams = 2
		IF serverBD.iWinningTeam = iteam
			RETURN 1
		ELSE
			RETURN 2
		ENDIF
	ELSE
		RETURN 1
	ENDIF

	RETURN 1
	
	
ENDFUNC


FUNC BOOL SHOULD_DISPLAY_PLAYLIST_LEADERBOARD()
	BOOL bRet=FALSE
	
	INT i
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
		//don't display on empty LB
		IF serverBD_LB.sleaderboard[i].iParticipant != -1
			PRINTLN("Valid participant found at ", i, "with participant index serverBD_LB.sleaderboard[i].iParticipant")
			bRet = TRUE
		ENDIF
	ENDREPEAT
	
	RETURN bRet
ENDFUNC

FUNC BOOL HAS_SERVER_POPULATED_LBD()
	RETURN (serverBD.bPlayListSorted)
ENDFUNC

PROC DISPLAY_LEADERBOARD()
	INT iSlotCounter
	INT i	
	PLAYER_INDEX tempplayer
	INT iLocalTeam, iEnemyTeam, iLbdPlayer
	LBD_SUB_MODE submode
	submode = SUB_PLAYLIST
	BOOL bDraw[FMMC_MAX_TEAMS]
	
	INT iScoreVals[NUM_PREVIOUS_SCORES+1]
	
	#IF IS_DEBUG_BUILD
	BOOL bSpam = IS_DEBUG_KEY_PRESSED(KEY_NUMPAD2, KEYBOARD_MODIFIER_NONE, "")
	#ENDIF
	
	IF HAVE_ALL_COMMON_LEADERBOARD_ELEMENTS_LOADED(LBPlacement)
		IF HAS_SERVER_POPULATED_LBD()
			RENDER_LEADERBOARD(lbdVars, LBPlacement, submode, FALSE, g_sCurrentPlayListDetails.tl31PlaylistName, GET_NUM_LBD_COLUMNS(SUB_PLAYLIST), -1, FALSE, TRUE)
			
			lbdVars.iPlaylistJobCount = serverBD.iLength
			
			REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
			
				FLOAT fkilldeathratio 
				FLOAT fkills = TO_FLOAT(serverBD_LB.sleaderboard[i].iKills)
				FLOAT fdeaths = TO_FLOAT(serverBD_LB.sleaderboard[i].iDeaths)
				
				IF fdeaths > 0
					fkilldeathratio = fkills/fdeaths
				ELIF fdeaths = 0
					IF fkills > 0
						fkilldeathratio =fkills/1.0
					ENDIF
				ENDIF
				
				IF fkilldeathratio < 0.0
					fkilldeathratio = 0.0
				ENDIF
				
				IF serverBD_LB.sleaderboard[i].iparticipant != -1
					IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(serverBD_LB.sleaderboard[i].iparticipant))
					AND NETWORK_IS_HOST_OF_THIS_SCRIPT()
						//tempplayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(serverBD_LB.sleaderboard[i].iparticipant))
						tempplayer = INT_TO_PLAYERINDEX(serverBD_LB.sleaderboard[i].iparticipant) // 1687664
						CLEAR_LEADERBOARD_SLOT_DATA(tempplayer)
						serverBD_LB.sleaderboard[i].iTeam =-1
						serverBD_LB.sleaderboard[i].iparticipant =-1
						serverBD_LB.sleaderboard[i].playerID = INVALID_PLAYER_INDEX()
						PRINTLN("[CS_PLY_SPAM] CLEAR_LEADERBOARD_SLOT_DATA for tempplayer = ", NATIVE_TO_INT(tempplayer))
					ENDIF
				ENDIF
				
				IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
					tempplayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
					STRING sPlayerName = GET_PLAYER_NAME(tempplayer)
					IF IS_STRING_NULL_OR_EMPTY(tParticipantNames[i])
					AND NOT IS_STRING_NULL_OR_EMPTY(sPlayerName)
						tParticipantNames[i] = sPlayerName
						STORE_GAMER_HANDLES(lbdVars, tempplayer, i)
						PRINTLN(" [2034613] DEFINE_NAMES_ARRAY,  iparticipant = ", i, " tParticipantNames = ", tParticipantNames[i])
					ENDIF
					
					#IF IS_DEBUG_BUILD
					IF bSpam
						PRINTLN("[CS_PLY_SPAM] GET_PLAYER_NAME = ", GET_PLAYER_NAME(tempplayer))
					ENDIF
					#ENDIF
				ENDIF
				
				LBD_VOTE lbdvPlayListVote
				
	//			IF (serverBD_LB.sleaderboard[i].playerID <> INVALID_PLAYER_INDEX()) // DON'T USE THIS CHECK OR ENTER A WORLD OF PAIN
				IF (serverBD_LB.sleaderboard[i].iparticipant <> -1)
					
					// An extra check added as a potential fix for 2233911, for SCTV tournament playlists only. Take it out if it causes issues.
//					IF ( NOT SB_IS_TOURNAMENT_PLAYLIST() AND NOT SB_IS_SCTV_CONTROLLED_PLAYLIST() )
					IF (serverBD_LB.sleaderboard[i].playerID <> INVALID_PLAYER_INDEX())

						iLbdPlayer = NATIVE_TO_INT(serverBD_LB.sleaderboard[i].playerID)
						iSavedRow[iLbdPlayer] = iSlotCounter
						
						IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(serverBD_LB.sleaderboard[i].playerID)].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_continue_to_next)
							lbdvPlayListVote = LBD_VOTE_CONTINUE
						ELIF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(serverBD_LB.sleaderboard[i].playerID)].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_WantToRestartPlaylist)
							lbdvPlayListVote = LBD_VOTE_REPLAY
						ELSE
							lbdvPlayListVote = LBD_VOTE_NOT_USED
						ENDIF
						
						INT iTemp
						REPEAT COUNT_OF(iScoreVals) iTemp
							iScoreVals[iTemp] = EMPTY_LB_ENTRY
							INT iProgToIndex
							iProgToIndex = CLAMP_INT(g_sCurrentPlayListDetails.iPlaylistProgress - 2, 0, NUM_PREVIOUS_SCORES)
							IF iTemp < iProgToIndex
							AND iTemp < NUM_PREVIOUS_SCORES
								iScoreVals[iTemp] = serverBD_LB.sleaderboard[i].iPreviousPlaylistScore[iProgToIndex - (iTemp+1)]
							ENDIF
						ENDREPEAT
						
						iScoreVals[CLAMP_INT(g_sCurrentPlayListDetails.iPlaylistProgress - 2, 0, COUNT_OF(iScoreVals)-1)] = serverBD_LB.sleaderboard[i].iCurrentPlaylistScore
						
						SET_LBD_SELECTION_TO_PLAYER_POS(lbdVars, serverBD_LB.sleaderboard[i].playerID, iLBPlayerSelection, iSlotCounter, serverBD.iTotalNumOfLBPlayers)
						
						POPULATE_COLUMN_LBD(lbdVars,
											LBPlacement, 
											submode,
											lbdvPlayListVote,
											tParticipantNames,
											GET_NUM_LBD_COLUMNS(SUB_PLAYLIST),
											fkilldeathratio,
											0, //1
											iScoreVals[0],//2
											iScoreVals[1],//3 
											iScoreVals[2], //4
											iScoreVals[3],//5
											serverBD_LB.sleaderboard[i].iPlayerScore,//6
											iLocalTeam,
											iEnemyTeam,
											iSlotCounter,
											serverBD_LB.sleaderboard[i].iRank, 
											serverBD_LB.sleaderboard[i].iParticipant,
											serverBD_LB.sleaderboard[i].iBadgeRank,
											serverBD_LB.sleaderboard[i].playerID,
											FALSE,
											bDraw[0],
											-1,
											-1)
						
						iSlotCounter++			
						PRINTLN("[CS_GC] iSlotCounter = ", iSlotCounter, " iLbdPlayer = ", iLbdPlayer)

					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
					IF bSpam
						PRINTLN("[CS_PLY_SPAM] INVALID playerID = ", NATIVE_TO_INT(serverBD_LB.sleaderboard[i].playerID))
					ENDIF
					#ENDIF
				ENDIF
			ENDREPEAT	
			
			// Check for player card press
			HANDLE_THE_PLAYER_CARDS(serverBD_LB.sleaderboard, lbdVars, iSavedRow, FALSE)
		ELSE
			PRINTLN("[CS_PLY_SPAM] HAS_SERVER_POPULATED_LBD = FALSE, iServerPlaylistSortStage = ", iServerPlaylistSortStage)
		ENDIF
	ENDIF
ENDPROC

//return the amount of time left in seconds. 
FUNC INT GET_LB_END_TIME()
	INT iTimer
	//Get the timer as seconds
	iTimer = (GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), serverBD.tdLeaderboardTimer.Timer))///1000 
	// Count downwards 
	iTimer = (ciLEADER_BOARD_END_TIME - iTimer) ///1000
	//return
	RETURN iTimer
ENDFUNC

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////			Server leaderboard functions			 //////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///    
///    
FUNC BOOL IS_THIS_PLAYER_SPECTATOR(PLAYER_INDEX tempPlayer)

	IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(tempPlayer)].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_JoinedAsSpectator)
	
	
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_SCTV(tempPlayer)
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

#IF IS_DEBUG_BUILD
PROC DEBUG_OUTPUT_PLAYLIST_VARS()

	INT i, iPlayer
	STRING sName
	
	PARTICIPANT_INDEX iPartPlaylistHost = NETWORK_GET_HOST_OF_THIS_SCRIPT()
	sName = GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(iPartPlaylistHost))
	PRINTNL()
	PRINTLN(" DEBUG_OUTPUT_PLAYLIST_VARS ______________________________")
	PRINTNL()
	PRINTLN(" PLAYLIST_HOST            = ", sName)
	PRINTLN(" iPlaylistProgress        = ", g_sCurrentPlayListDetails.iPlaylistProgress)
	PRINTLN(" iCurrentPlayListPosition = ", g_sCurrentPlayListDetails.iCurrentPlayListPosition)
	
	DEBUG_PRINT_PLAYERS()
	
	REPEAT NUM_NETWORK_PLAYERS i
		PRINTNL()
		iPlayer = NATIVE_TO_INT(serverBD_LB.sleaderboard[i].playerID)
		PRINTLN(" playerID               = ", iPlayer)
		IF serverBD_LB.sleaderboard[i].playerID != INVALID_PLAYER_INDEX()
			sName = GET_PLAYER_NAME(serverBD_LB.sleaderboard[i].playerID)
		ELSE
			sName = "INVALID_PLAYER_INDEX()"
		ENDIF
		PRINTLN(" sName     = ", sName)
		PRINTLN(" iCurrentPlaylistScore     = ", serverBD_LB.sleaderboard[i].iCurrentPlaylistScore)
		PRINTLN(" iPreviousPlaylistScore[0] = ", serverBD_LB.sleaderboard[i].iPreviousPlaylistScore[0])
		PRINTLN(" iPreviousPlaylistScore[1] = ", serverBD_LB.sleaderboard[i].iPreviousPlaylistScore[1])
		PRINTLN(" iPreviousPlaylistScore[2] = ", serverBD_LB.sleaderboard[i].iPreviousPlaylistScore[2])
		PRINTNL()
	ENDREPEAT
						
	PRINTNL()
	PRINTLN("_____________________________________________________________")
	PRINTNL()
ENDPROC
#ENDIF

PROC SERVER_POPULATE_PLAYLIST_LB_DATA()

PARTICIPANT_INDEX tempPart
PLAYER_INDEX tempPlayer
INT i,iglobal,iplayer
BOOL bAlreadyGotSlot[NUM_NETWORK_PLAYERS]
BOOL bPlayerUsed[NUM_NETWORK_PLAYERS]

	NET_PRINT("SERVER_POPULATE_PLAYLIST_LB_DATA") NET_NL()
	
	FOR i = 0 TO (NUM_NETWORK_PLAYERS-1)
		IF serverBD_LB.sleaderboard[i].playerID  != INVALID_PLAYER_INDEX()
			IF IS_NET_PLAYER_OK(serverBD_LB.sleaderboard[i].playerID,FALSE)
				iplayer = NATIVE_TO_INT(serverBD_LB.sleaderboard[i].playerID)
				IF NETWORK_IS_PLAYER_A_PARTICIPANT(serverBD_LB.sleaderboard[i].playerID)
					tempPart = NETWORK_GET_PARTICIPANT_INDEX(serverBD_LB.sleaderboard[i].playerID)
					IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
						PRINTLN("[PLY_BUG] PARTICIPANT NOT ACTIVE so clearing data for player: ",NATIVE_TO_INT(serverBD_LB.sleaderboard[i].playerID))
						CLEAR_LEADERBOARD_SLOT_DATA(serverBD_LB.sleaderboard[i].playerID)
					ENDIF
				ELSE
					PRINTLN("[PLY_BUG] player not PARTICIPANT so clearing data: ",NATIVE_TO_INT(serverBD_LB.sleaderboard[i].playerID))
					CLEAR_LEADERBOARD_SLOT_DATA(serverBD_LB.sleaderboard[i].playerID)
				ENDIF
			ELSE
				PRINTLN("[PLY_BUG] player not ok so clearing data: ",NATIVE_TO_INT(serverBD_LB.sleaderboard[i].playerID))
				CLEAR_LEADERBOARD_SLOT_DATA(serverBD_LB.sleaderboard[i].playerID)
			ENDIF
		ENDIF
	ENDFOR
	
	FOR iglobal = 0 TO (NUM_NETWORK_PLAYERS-1)
		IF NOT bAlreadyGotSlot[iglobal]
			tempPlayer = g_TransitionSessionNonResetVars.sPLLB_Globals.PlayerID[iglobal]
			IF tempPlayer != INVALID_PLAYER_INDEX()
				IF IS_NET_PLAYER_OK(tempPlayer,FALSE)
					iplayer = NATIVE_TO_INT(tempPlayer)
					IF NOT IS_THIS_PLAYER_SPECTATOR(tempPlayer)
						IF NETWORK_IS_PLAYER_A_PARTICIPANT(tempPlayer)
							tempPart = NETWORK_GET_PARTICIPANT_INDEX(tempPlayer)
							IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
								FOR i = 0 TO (NUM_NETWORK_PLAYERS-1)
									IF serverBD_LB.sleaderboard[i].playerID = tempPlayer
									AND NOT bPlayerUsed[iplayer]
										bAlreadyGotSlot[iglobal] = TRUE
										bPlayerUsed[iplayer] = TRUE
										NET_PRINT("WRITING TO OLD LEADERBOAR SLOT: ") NET_PRINT_INT(i) NET_PRINT(" FOR PLAYER: ") NET_PRINT_INT(iplayer) NET_NL()
										serverBD_LB.sleaderboard[i].iparticipant = NATIVE_TO_INT(tempPart)
										NET_PRINT("serverBD_LB.sleaderboard[i].iparticipant= ") NET_PRINT_INT(NATIVE_TO_INT(tempPart)) NET_NL()
										IF SB_PLAYLIST_DOING_HEAD_TO_HEAD()
											serverBD_LB.sleaderboard[i].iteam = GlobalplayerBD_FM[NATIVE_TO_INT(tempPlayer)].sPlaylistVars.iPlaylistTeam
										ELSE
											serverBD_LB.sleaderboard[i].iteam = 0//g_TransitionSessionNonResetVars.sPLLB_Globals.iteamId[i]
										ENDIF
										NET_PRINT("serverBD_LB.sleaderboard[i].iteam = ") NET_PRINT_INT(serverBD_LB.sleaderboard[i].iteam) NET_NL()
										NET_PRINT(" g_TransitionSessionNonResetVars.sPLLB_Globals.iPositionInLB[iplayer] = ") NET_PRINT_INT(g_TransitionSessionNonResetVars.sPLLB_Globals.iPositionInLB[iglobal]) NET_NL()
										NET_PRINT(" OLD serverBD_LB.sleaderboard[i].iPlayerScore = ") NET_PRINT_INT(serverBD_LB.sleaderboard[i].iPlayerScore) NET_NL()
										serverBD_LB.sleaderboard[i].iPlayerScore = serverBD_LB.sleaderboard[i].iPlayerScore + GET_POINTS_FOR_POSITION(g_TransitionSessionNonResetVars.sPLLB_Globals.iPositionInLB[iglobal], iplayer)
										NET_PRINT(" NEW serverBD_LB.sleaderboard[i].iPlayerScore = ") NET_PRINT_INT(serverBD_LB.sleaderboard[i].iPlayerScore) NET_NL()
										
										serverBD_LB.sleaderboard[i].iPreviousPlaylistScore[2] = serverBD_LB.sleaderboard[i].iPreviousPlaylistScore[1]
										serverBD_LB.sleaderboard[i].iPreviousPlaylistScore[1] = serverBD_LB.sleaderboard[i].iPreviousPlaylistScore[0]
										serverBD_LB.sleaderboard[i].iPreviousPlaylistScore[0] = serverBD_LB.sleaderboard[i].iCurrentPlaylistScore
										NET_PRINT(" NEW serverBD_LB.sleaderboard[i].iPreviousPlaylistScore[0] = ") NET_PRINT_INT(serverBD_LB.sleaderboard[i].iPreviousPlaylistScore[0]) NET_NL()
										NET_PRINT(" NEW serverBD_LB.sleaderboard[i].iPreviousPlaylistScore[1] = ") NET_PRINT_INT(serverBD_LB.sleaderboard[i].iPreviousPlaylistScore[1]) NET_NL()
										NET_PRINT(" NEW serverBD_LB.sleaderboard[i].iPreviousPlaylistScore[2] = ") NET_PRINT_INT(serverBD_LB.sleaderboard[i].iPreviousPlaylistScore[2]) NET_NL()
										
										serverBD_LB.sleaderboard[i].iCurrentPlaylistScore = GET_POINTS_FOR_POSITION(g_TransitionSessionNonResetVars.sPLLB_Globals.iPositionInLB[iglobal], iplayer)
										NET_PRINT(" NEW serverBD_LB.sleaderboard[i].iCurrentPlaylistScore = ") NET_PRINT_INT(serverBD_LB.sleaderboard[i].iCurrentPlaylistScore) NET_NL()
										serverBD_LB.sleaderboard[i].iMissionTime = g_TransitionSessionNonResetVars.sPLLB_Globals.iEventtime[iplayer]
	//									NET_PRINT(" NEW serverBD_LB.sleaderboard[i].iMissionTime = ") NET_PRINT_INT(serverBD_LB.sleaderboard[i].iMissionTime) NET_NL()
	//									serverBD_LB.sleaderboard[i].iKills =serverBD_LB.sleaderboard[i].iKills + g_TransitionSessionNonResetVars.sPLLB_Globals.ikills[iplayer]
	//									NET_PRINT("serverBD_LB.sleaderboard[i].iKills = ") NET_PRINT_INT(serverBD_LB.sleaderboard[i].iKills) NET_NL()
										serverBD_LB.sleaderboard[i].ideaths =serverBD_LB.sleaderboard[i].ideaths +  g_TransitionSessionNonResetVars.sPLLB_Globals.ideaths[iplayer]
										NET_PRINT("serverBD_LB.sleaderboard[i].ideaths = ") NET_PRINT_INT(serverBD_LB.sleaderboard[i].ideaths) NET_NL()
										
										FLOAT fkilldeathratio
										IF serverBD_LB.sleaderboard[i].iDeaths > 0
											fkilldeathratio = TO_FLOAT(serverBD_LB.sleaderboard[i].iKills/serverBD_LB.sleaderboard[i].iDeaths)
										ELIF serverBD_LB.sleaderboard[i].iDeaths = 0
											IF serverBD_LB.sleaderboard[i].iKills > 0
												fkilldeathratio =TO_FLOAT(serverBD_LB.sleaderboard[i].iKills/1)
											ENDIF
										ENDIF
										
										IF fkilldeathratio < 0.0
											fkilldeathratio = 0.0
										ENDIF
										
										serverBD_LB.sleaderboard[i].fKDRatio =fkilldeathratio
										NET_PRINT("serverBD_LB.sleaderboard[i].fKDRatio = ") NET_PRINT_FLOAT(serverBD_LB.sleaderboard[i].fKDRatio) NET_NL()
										serverBD_LB.sleaderboard[i].iHeadShots = serverBD_LB.sleaderboard[i].iHeadShots + g_TransitionSessionNonResetVars.sPLLB_Globals.iHeadShots[iplayer]
										NET_PRINT("serverBD_LB.sleaderboard[i].iHeadShots = ") NET_PRINT_INT(serverBD_LB.sleaderboard[i].iHeadShots) NET_NL()
										serverBD_LB.sleaderboard[i].iBadgeRank = GlobalplayerBD_FM[iplayer].scoreData.iRank
										NET_PRINT("serverBD_LB.sleaderboard[i].iBadgeRank = ") NET_PRINT_INT(serverBD_LB.sleaderboard[i].iBadgeRank) NET_NL()

//										serverBD_LB.sleaderboard[i].bInitialise = TRUE
										SET_BIT(serverBD_LB.sleaderboard[i].iLbdBitSet, ciLBD_BIT_INITIALISE)
										
									ENDIF
								ENDFOR
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	FOR iglobal = 0 TO (NUM_NETWORK_PLAYERS-1)
		IF NOT bAlreadyGotSlot[iglobal]
			tempPlayer = g_TransitionSessionNonResetVars.sPLLB_Globals.PlayerID[iglobal]
			#IF IS_DEBUG_BUILD
			IF tempPlayer != INVALID_PLAYER_INDEX()
				IF IS_NET_PLAYER_OK(tempPlayer,FALSE)
					IF NETWORK_IS_PLAYER_ACTIVE(tempPlayer)
						PRINTLN("[PLY_BUG] tempPlayer = ", GET_PLAYER_NAME(tempPlayer))
					ENDIF
				ENDIF
			ELSE
				PRINTLN("[PLC] SERVER_POPULATE_PLAYLIST_LB_DATA - piPlayer = INVALID_PLAYER_INDEX()")
			ENDIF
			#ENDIF
			IF tempPlayer != INVALID_PLAYER_INDEX()
				IF IS_NET_PLAYER_OK(tempPlayer,FALSE)
					IF NOT IS_THIS_PLAYER_SPECTATOR(tempPlayer)
						iplayer = NATIVE_TO_INT(tempPlayer)
						IF NETWORK_IS_PLAYER_A_PARTICIPANT(tempPlayer)
							tempPart = NETWORK_GET_PARTICIPANT_INDEX(tempPlayer)
							IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
								FOR i = 0 TO (NUM_NETWORK_PLAYERS-1)
//									IF NOT serverBD_LB.sleaderboard[i].bInitialise
									IF NOT IS_BIT_SET(serverBD_LB.sleaderboard[i].iLbdBitSet, ciLBD_BIT_INITIALISE)
									AND NOT bPlayerUsed[iplayer]
									AND serverBD_LB.sleaderboard[i].playerID  = INVALID_PLAYER_INDEX()
										bAlreadyGotSlot[iglobal] = TRUE
										bPlayerUsed[iplayer] = TRUE
										NET_PRINT("iglobal: ") NET_PRINT_INT(iglobal) NET_NL()
										NET_PRINT("[PLY_BUG] i: ") NET_PRINT_INT(i) NET_NL()
										NET_PRINT("WRITING TO FRESH LEADERBOAR SLOT FOR PLAYER: ") NET_PRINT_INT(iplayer) NET_NL()
										serverBD_LB.sleaderboard[i].playerID = tempPlayer
										PRINTLN("[CS_PLY_ID] serverBD_LB.sleaderboard[i].playerID = tempPlayer, FRESH ")
										NET_PRINT("sPLLB_Globals.PlayerID[i] = ") NET_PRINT_INT(iplayer) NET_NL()
										serverBD_LB.sleaderboard[i].iparticipant = NATIVE_TO_INT(tempPart)
										NET_PRINT("serverBD_LB.sleaderboard[i].iparticipant= ") NET_PRINT_INT(NATIVE_TO_INT(tempPart)) NET_NL()
										IF SB_PLAYLIST_DOING_HEAD_TO_HEAD()
											serverBD_LB.sleaderboard[i].iteam = GlobalplayerBD_FM[NATIVE_TO_INT(tempPlayer)].sPlaylistVars.iPlaylistTeam
										ELSE
											serverBD_LB.sleaderboard[i].iteam = 0//g_TransitionSessionNonResetVars.sPLLB_Globals.iteamId[i]
										ENDIF
										NET_PRINT("serverBD_LB.sleaderboard[i].iteam = ") NET_PRINT_INT(serverBD_LB.sleaderboard[i].iteam) NET_NL()
										NET_PRINT("OLD serverBD_LB.sleaderboard[i].iPlayerScore = ") NET_PRINT_INT(serverBD_LB.sleaderboard[i].iPlayerScore) NET_NL()
										NET_PRINT("g_TransitionSessionNonResetVars.sPLLB_Globals.iPositionInLB[iplayer] = ") NET_PRINT_INT(g_TransitionSessionNonResetVars.sPLLB_Globals.iPositionInLB[iglobal]) NET_NL()
										serverBD_LB.sleaderboard[i].iPlayerScore = serverBD_LB.sleaderboard[i].iPlayerScore + GET_POINTS_FOR_POSITION(g_TransitionSessionNonResetVars.sPLLB_Globals.iPositionInLB[iglobal], iplayer)
										NET_PRINT("serverBD_LB.sleaderboard[i].iPlayerScore = ") NET_PRINT_INT(serverBD_LB.sleaderboard[i].iPlayerScore) NET_NL()
										
										
										serverBD_LB.sleaderboard[i].iPreviousPlaylistScore[2] = serverBD_LB.sleaderboard[i].iPreviousPlaylistScore[1]
										serverBD_LB.sleaderboard[i].iPreviousPlaylistScore[1] = serverBD_LB.sleaderboard[i].iPreviousPlaylistScore[0]
										serverBD_LB.sleaderboard[i].iPreviousPlaylistScore[0] = serverBD_LB.sleaderboard[i].iCurrentPlaylistScore
										NET_PRINT(" NEW serverBD_LB.sleaderboard[i].iPreviousPlaylistScore[0] = ") NET_PRINT_INT(serverBD_LB.sleaderboard[i].iPreviousPlaylistScore[0]) NET_NL()
										NET_PRINT(" NEW serverBD_LB.sleaderboard[i].iPreviousPlaylistScore[1] = ") NET_PRINT_INT(serverBD_LB.sleaderboard[i].iPreviousPlaylistScore[1]) NET_NL()
										NET_PRINT(" NEW serverBD_LB.sleaderboard[i].iPreviousPlaylistScore[2] = ") NET_PRINT_INT(serverBD_LB.sleaderboard[i].iPreviousPlaylistScore[2]) NET_NL()
										
										serverBD_LB.sleaderboard[i].iCurrentPlaylistScore = GET_POINTS_FOR_POSITION(g_TransitionSessionNonResetVars.sPLLB_Globals.iPositionInLB[iglobal], iplayer)
										NET_PRINT(" NEW serverBD_LB.sleaderboard[i].iCurrentPlaylistScore = ") NET_PRINT_INT(serverBD_LB.sleaderboard[i].iCurrentPlaylistScore) NET_NL()
										
										serverBD_LB.sleaderboard[i].iMissionTime = g_TransitionSessionNonResetVars.sPLLB_Globals.iEventtime[iplayer]
										NET_PRINT(" NEW serverBD_LB.sleaderboard[i].iMissionTime = ") NET_PRINT_INT(serverBD_LB.sleaderboard[i].iMissionTime) NET_NL()
										
										serverBD_LB.sleaderboard[i].iKills =serverBD_LB.sleaderboard[i].iKills + g_TransitionSessionNonResetVars.sPLLB_Globals.ikills[iplayer]
										NET_PRINT("serverBD_LB.sleaderboard[i].iKills = ") NET_PRINT_INT(serverBD_LB.sleaderboard[i].iKills) NET_NL()
										serverBD_LB.sleaderboard[i].ideaths =serverBD_LB.sleaderboard[i].ideaths + g_TransitionSessionNonResetVars.sPLLB_Globals.ideaths[iplayer]
										NET_PRINT("serverBD_LB.sleaderboard[i].ideaths = ") NET_PRINT_INT(serverBD_LB.sleaderboard[i].ideaths) NET_NL()
										FLOAT fkilldeathratio 
										IF serverBD_LB.sleaderboard[i].iDeaths > 0
											fkilldeathratio = TO_FLOAT(serverBD_LB.sleaderboard[i].iKills/serverBD_LB.sleaderboard[i].iDeaths)
										ELIF serverBD_LB.sleaderboard[i].iDeaths = 0
											IF serverBD_LB.sleaderboard[i].iKills > 0
												fkilldeathratio =TO_FLOAT(serverBD_LB.sleaderboard[i].iKills/1)
											ENDIF
										ENDIF
										IF fkilldeathratio < 0.0
											fkilldeathratio = 0.0
										ENDIF   
										serverBD_LB.sleaderboard[i].fKDRatio =fkilldeathratio
										NET_PRINT("serverBD_LB.sleaderboard[i].fKDRatio = ") NET_PRINT_FLOAT(serverBD_LB.sleaderboard[i].fKDRatio) NET_NL()
										serverBD_LB.sleaderboard[i].iHeadShots = serverBD_LB.sleaderboard[i].iHeadShots + g_TransitionSessionNonResetVars.sPLLB_Globals.iHeadShots[iplayer]
										NET_PRINT("serverBD_LB.sleaderboard[i].iHeadShots = ") NET_PRINT_INT(serverBD_LB.sleaderboard[i].iHeadShots) NET_NL()
										serverBD_LB.sleaderboard[i].iBadgeRank = GlobalplayerBD_FM[iplayer].scoreData.iRank
										NET_PRINT("serverBD_LB.sleaderboard[i].iBadgeRank = ") NET_PRINT_INT(serverBD_LB.sleaderboard[i].iBadgeRank) NET_NL()
										
//										serverBD_LB.sleaderboard[i].bInitialise = TRUE
										SET_BIT(serverBD_LB.sleaderboard[i].iLbdBitSet, ciLBD_BIT_INITIALISE)
									#IF IS_DEBUG_BUILD
									ELSE
//										IF serverBD_LB.sleaderboard[i].bInitialise
//											PRINTLN("[PLY_BUG] serverBD_LB.sleaderboard[i].bInitialise = ", i)
//										ENDIF

										PRINTLN("[PLY_BUG] test[iplayer] = ", iplayer)
										
										IF bPlayerUsed[iplayer]
											PRINTLN("[PLY_BUG] bPlayerUsed[iplayer] = ", iplayer)
										ENDIF
										
										IF serverBD_LB.sleaderboard[i].playerID <> INVALID_PLAYER_INDEX()
											PRINTLN("[PLY_BUG] playerID = ", iplayer)
										ENDIF
									#ENDIF
									ENDIF
								ENDFOR
							ELSE
								PRINTLN("[PLY_BUG] NETWORK_IS_PARTICIPANT_ACTIVE = ", iplayer)
							ENDIF
						ELSE
							PRINTLN("[PLY_BUG] NETWORK_IS_PLAYER_A_PARTICIPANT = ", iplayer)
						ENDIF
					ELSE
						PRINTLN("[PLY_BUG] IS_THIS_PLAYER_SPECTATOR = ", iplayer)
					ENDIF
				ELSE
					PRINTLN("[PLY_BUG] IS_NET_PLAYER_OK ", NATIVE_TO_INT(tempPlayer))
				ENDIF
			ELSE
				PRINTLN("[PLY_BUG] tempPlayer = INVALID ", NATIVE_TO_INT(tempPlayer))
			ENDIF
		ELSE
			PRINTLN("[PLY_BUG] bAlreadyGotSlot, iglobal = ", iglobal)
		ENDIF
	ENDFOR
ENDPROC

PROC SERVER_CALCULATE_TEAM_SCORES()

	INT i
	PRINTLN("SERVER_CALCULATE_TEAM_SCORES")

	FOR i = 0 TO (FMMC_MAX_TEAMS-1)
		serverBD.iNumberOfLBPlayers[i] =0
		serverBD.iteamScore[i] =0
		serverBD.iteamKills[i] =0
		serverBD.iteamDeaths[i] =0
		serverBD.iteamHeadshots[i] =0
	ENDFOR

	FOR i = 0 TO (NUM_NETWORK_PLAYERS-1)
		IF serverBD_LB.sleaderboard[i].iTeam >= 0
		AND serverBD_LB.sleaderboard[i].iTeam < FMMC_MAX_TEAMS
//		AND serverBD_LB.sleaderboard[i].bInitialise
		AND	IS_BIT_SET(serverBD_LB.sleaderboard[i].iLbdBitSet, ciLBD_BIT_INITIALISE)
			serverBD.iNumberOfLBPlayers[serverBD_LB.sleaderboard[i].iteam]++
			serverBD.iteamScore[serverBD_LB.sleaderboard[i].iteam] = serverBD.iteamScore[serverBD_LB.sleaderboard[i].iteam] + serverBD_LB.sleaderboard[i].iCurrentPlaylistScore
			serverBD.iteamKills[serverBD_LB.sleaderboard[i].iteam] = serverBD.iteamKills[serverBD_LB.sleaderboard[i].iteam] + serverBD_LB.sleaderboard[i].iKills
			serverBD.iteamDeaths[serverBD_LB.sleaderboard[i].iteam] = serverBD.iteamDeaths[serverBD_LB.sleaderboard[i].iteam] + serverBD_LB.sleaderboard[i].iDeaths
			serverBD.iteamHeadshots[serverBD_LB.sleaderboard[i].iteam] = serverBD.iteamHeadshots[serverBD_LB.sleaderboard[i].iteam] + serverBD_LB.sleaderboard[i].iHeadshots 
		ENDIF
	ENDFOR
	
	#IF IS_DEBUG_BUILD
		FOR i = 0 TO (FMMC_MAX_TEAMS-1)
			IF serverBD_LB.sleaderboard[i].iTeam >=0
			AND  serverBD_LB.sleaderboard[i].iTeam < FMMC_MAX_TEAMS
//			AND serverBD_LB.sleaderboard[i].bInitialise
			AND	IS_BIT_SET(serverBD_LB.sleaderboard[i].iLbdBitSet, ciLBD_BIT_INITIALISE)
				PRINTLN("serverBD.iNumberOfLBPlayers[serverBD_LB.sleaderboard[", i, "].iteam]: ", serverBD.iNumberOfLBPlayers[serverBD_LB.sleaderboard[i].iteam])
				PRINTLN("serverBD.iteamScore[serverBD_LB.sleaderboard[", i, "].iteam]        : ", serverBD.iteamScore[serverBD_LB.sleaderboard[i].iteam])
				PRINTLN("serverBD.iteamKills[serverBD_LB.sleaderboard[", i, "].iteam]        : ", serverBD.iteamKills[serverBD_LB.sleaderboard[i].iteam])
				PRINTLN("serverBD.iteamDeaths[serverBD_LB.sleaderboard[", i, "].iteam]       : ", serverBD.iteamDeaths[serverBD_LB.sleaderboard[i].iteam])
				PRINTLN("serverBD.iteamHeadshots[serverBD_LB.sleaderboard[", i, "].iteam]    : ", serverBD.iteamHeadshots[serverBD_LB.sleaderboard[i].iteam])
			ENDIF
		ENDFOR
	#ENDIF
	
ENDPROC



FUNC BOOL IS_TEAM_BEATING_TEAM(INT iteam1, INT iteam2)


	IF serverBD.iTeamScore[iteam1] > serverBD.iTeamScore[iteam2]  
		RETURN TRUE
	ELIF serverBD.iTeamScore[iteam1]  = serverBD.iTeamScore[iteam2]   
		IF serverBD.iteamKills[iteam1] > serverBD.iteamKills[iteam2] 
			RETURN TRUE
		ELIF serverBD.iteamKills[iteam1] = serverBD.iteamKills[iteam2] 
			IF serverBD.iteamDeaths[iteam1] < serverBD.iteamDeaths[iteam2] 
				RETURN TRUE
			ELIF serverBD.iteamDeaths[iteam1] = serverBD.iteamDeaths[iteam2] 
				IF serverBD.iteamHeadshots[iteam1] > serverBD.iteamHeadshots[iteam2] 
					RETURN TRUE
				ELIF serverBD.iteamHeadshots[iteam1] = serverBD.iteamHeadshots[iteam2] 

					FLOAT fkilldeathratio1 
					IF  serverBD.iteamDeaths[iteam1] > 0
						fkilldeathratio1 = TO_FLOAT(serverBD.iteamKills[iteam1]/serverBD.iteamDeaths[iteam1])
					ELIF serverBD.iteamDeaths[iteam1]= 0
						IF serverBD.iteamKills[iteam1] > 0
							fkilldeathratio1 =TO_FLOAT(serverBD.iteamKills[iteam1]/1)
						ENDIF
					ENDIF
					IF fkilldeathratio1 < 0.0
						fkilldeathratio1 = 0.0
					ENDIF
					FLOAT fkilldeathratio2
					IF  serverBD.iteamDeaths[iteam2] > 0
						fkilldeathratio2 = TO_FLOAT(serverBD.iteamKills[iteam2]/serverBD.iteamDeaths[iteam2])
					ELIF serverBD.iteamDeaths[iteam2]= 0
						IF serverBD.iteamKills[iteam2] > 0
							fkilldeathratio2 =TO_FLOAT(serverBD.iteamKills[iteam2]/1)
						ENDIF
					ENDIF
					IF fkilldeathratio2 < 0.0
						fkilldeathratio2 = 0.0
					ENDIF
					IF fkilldeathratio1 > fkilldeathratio2 
						RETURN TRUE
					ELIF fkilldeathratio1 = fkilldeathratio2 
						IF iteam1 < iteam2
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE


ENDFUNC


FUNC BOOL IS_PLAYER_BEATING_PLAYER(INT iPlayer1, INT iPlayer2)


	IF serverBD_LB.sleaderboard[iPlayer1].iPlayerScore > serverBD_LB.sleaderboard[iPlayer2].iPlayerScore
		RETURN TRUE
	ELIF serverBD_LB.sleaderboard[iPlayer1].iPlayerScore = serverBD_LB.sleaderboard[iPlayer2].iPlayerScore
		IF serverBD_LB.sleaderboard[iPlayer1].iKills > serverBD_LB.sleaderboard[iPlayer2].iKills
			RETURN TRUE
		ELIF serverBD_LB.sleaderboard[iPlayer1].iKills = serverBD_LB.sleaderboard[iPlayer2].iKills 
			IF serverBD_LB.sleaderboard[iPlayer1].iDeaths < serverBD_LB.sleaderboard[iPlayer2].iDeaths 
				RETURN TRUE
			ELIF serverBD_LB.sleaderboard[iPlayer1].iDeaths = serverBD_LB.sleaderboard[iPlayer2].iDeaths 
				IF serverBD_LB.sleaderboard[iPlayer1].iHeadshots > serverBD_LB.sleaderboard[iPlayer2].iHeadshots 
					RETURN TRUE
				ELIF serverBD_LB.sleaderboard[iPlayer1].iHeadshots = serverBD_LB.sleaderboard[iPlayer2].iHeadshots 
					FLOAT fkilldeathratio1 
					IF  serverBD_LB.sleaderboard[iPlayer1].iDeaths > 0
						fkilldeathratio1 = TO_FLOAT(serverBD_LB.sleaderboard[iPlayer1].iKills/serverBD_LB.sleaderboard[iPlayer1].iDeaths)
					ELIF serverBD_LB.sleaderboard[iPlayer1].iDeaths= 0
						IF serverBD_LB.sleaderboard[iPlayer1].iKills > 0
							fkilldeathratio1 =TO_FLOAT(serverBD_LB.sleaderboard[iPlayer1].iKills/1)
						ENDIF
					ENDIF
					IF fkilldeathratio1 < 0.0
						fkilldeathratio1 = 0.0
					ENDIF
					FLOAT fkilldeathratio2
					IF  serverBD_LB.sleaderboard[iPlayer2].iDeaths > 0
						fkilldeathratio2= TO_FLOAT(serverBD_LB.sleaderboard[iPlayer2].iKills/serverBD_LB.sleaderboard[iPlayer2].iDeaths)
					ELIF serverBD_LB.sleaderboard[iPlayer2].iDeaths= 0
						IF serverBD_LB.sleaderboard[iPlayer2].iKills > 0
							fkilldeathratio2 =TO_FLOAT(serverBD_LB.sleaderboard[iPlayer2].iKills/1)
						ENDIF
					ENDIF
					IF fkilldeathratio2 < 0.0
						fkilldeathratio2 = 0.0
					ENDIF

					IF fkilldeathratio1 > fkilldeathratio2 
						RETURN TRUE
					ELIF fkilldeathratio1 = fkilldeathratio2 
						IF iPlayer1 < iPlayer2
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE


ENDFUNC


PROC SERVER_SORT_LEADERBOARD_TEAMS()

INT ilowest,ilow1,ilow2,imiddle1,imiddle2,ihigh1,ihigh2,ihighest

	NET_PRINT("SERVER_SORT_LEADERBOARD_TEAMS") NET_NL()
	
	IF serverBD.iNumPlaylistTeams = 4
		IF IS_TEAM_BEATING_TEAM(1,0)
			ilow1 = 0
			ihigh1 = 1
		ELSE
			ilow1 = 1
			ihigh1 = 0
		ENDIF
		IF IS_TEAM_BEATING_TEAM(3,2)
			ilow2 = 2
			ihigh2 = 3
		ELSE
			ilow2 = 3
			ihigh2 = 2
		ENDIF
		IF IS_TEAM_BEATING_TEAM(ilow2,ilow1)
			ilowest = ilow1
			imiddle1 = ilow2
		ELSE
			ilowest = ilow2
			imiddle1 = ilow1
		ENDIF
		IF IS_TEAM_BEATING_TEAM(ihigh1,ihigh2)
			ihighest = ihigh1
			imiddle2 = ihigh2
		ELSE
			ihighest = ihigh2
			imiddle2 = ihigh1
		ENDIF
		IF IS_TEAM_BEATING_TEAM(imiddle2,imiddle1)
			serverBD.iWinningTeam = ihighest
			serverBD.iSecondTeam = imiddle2
			serverBD.iThirdTeam = imiddle1
			serverBD.iLosingTeam =ilowest
		ELSE
			serverBD.iWinningTeam = ihighest
			serverBD.iSecondTeam = imiddle1
			serverBD.iThirdTeam = imiddle2
			serverBD.iLosingTeam =ilowest
		ENDIF
	ELIF serverBD.iNumPlaylistTeams = 3
		IF IS_TEAM_BEATING_TEAM(1,0)	
			IF IS_TEAM_BEATING_TEAM(0,2)
				IF IS_TEAM_BEATING_TEAM(2,1)
					serverBD.iWinningTeam = 2
					serverBD.iSecondTeam = 1
					serverBD.iLosingTeam = 0
				ELSE
					serverBD.iWinningTeam = 1
					serverBD.iSecondTeam = 2
					serverBD.iLosingTeam = 0
				ENDIF
			ELSE
				serverBD.iWinningTeam = 1
				serverBD.iSecondTeam = 0
				serverBD.iLosingTeam = 2
			ENDIF
		ELSE
			IF IS_TEAM_BEATING_TEAM(2,1)
				IF IS_TEAM_BEATING_TEAM(2,0)
					serverBD.iWinningTeam = 2
					serverBD.iSecondTeam = 0
					serverBD.iLosingTeam = 1
				ELSE
					serverBD.iWinningTeam = 0
					serverBD.iSecondTeam = 2
					serverBD.iLosingTeam = 1
				ENDIF
			ELSE
				serverBD.iWinningTeam = 0
				serverBD.iSecondTeam = 1
				serverBD.iLosingTeam = 2
			ENDIF
		ENDIF

	ELIF serverBD.iNumPlaylistTeams = 2
		IF IS_TEAM_BEATING_TEAM(1,0)
			serverBD.iWinningTeam = 1
			serverBD.iLosingTeam = 0
		ELSE
			serverBD.iWinningTeam = 0
			serverBD.iLosingTeam = 1
		ENDIF
	ELSE
		serverBD.iWinningTeam = 0
	ENDIF
	

	NET_PRINT("serverBD.iWinningTeam =: ") NET_PRINT_INT(serverBD.iWinningTeam) NET_NL()
	NET_PRINT("serverBD.iSecondTeam =: ") NET_PRINT_INT(serverBD.iSecondTeam) NET_NL()
	NET_PRINT("serverBD.iThirdTeam =: ") NET_PRINT_INT(serverBD.iThirdTeam) NET_NL()
	NET_PRINT("serverBD.iLosingTeam  =: ") NET_PRINT_INT(serverBD.iLosingTeam) NET_NL()

	
ENDPROC

PROC CLEANUP_ALL_PLAYLIST_SORTING_VARS()

	INT iPlayerLoop
	
	sPLLB_Vars.bWinnerFound = FALSE
//	iServerPlaylistSortStage = 0
	sPLLB_Vars.iAlreadyUsedPlayer = 0
	sPLLB_Vars.iNotBestPlayer = 0
	sPLLB_Vars.iCurrentPlayerToCheck = 0
	sPLLB_Vars.iCurrentLBPosition = 0
	iStuck = 0
	
	FOR iPlayerLoop = 0 TO (NUM_NETWORK_PLAYERS-1)
		sPLLB_Vars.iBestPlayerOrder[iPlayerLoop] = -1
	ENDFOR
	PRINTLN("[PLC] CLEARING ALL SORTING VARS")
	
ENDPROC

//Calculate the challenge difficulty
PROC CALCULATE_CHALLENGE_DIFFICULTY(INT iCurrentPos)
	PRINTLN("[PLC] CALCULATE_CHALLENGE_DIFFICULTY") 
	FLOAT fLastMissionDifficulty
	
	//If the participant has been initilised
	IF serverBD_LB.sleaderboard[0].iParticipant != -1
		INT iMyRankPrediction = playerBD[serverBD_LB.sleaderboard[0].iParticipant].iMyRankPrediction
		INT iTotatlUniquePlays = playerBD[serverBD_LB.sleaderboard[0].iParticipant].iTotatlUniquePlays
		//If there have been more than 10 plays
		IF iTotatlUniquePlays > 10
			SWITCH g_sCurrentPlayListDetails.sLoadedMissionDetails[iCurrentPos].iType 
				CASE FMMC_TYPE_MISSION
					fLastMissionDifficulty = TO_FLOAT(iTotatlUniquePlays/iMyRankPrediction)		 
				BREAK
				
				CASE FMMC_TYPE_RACE
					fLastMissionDifficulty = TO_FLOAT(iTotatlUniquePlays/iMyRankPrediction)		 
				BREAK
				
				CASE FMMC_TYPE_BASE_JUMP
					fLastMissionDifficulty = TO_FLOAT(iTotatlUniquePlays/iMyRankPrediction) 
				BREAK
				
				CASE FMMC_TYPE_SURVIVAL
					fLastMissionDifficulty = TO_FLOAT(serverBD.iBestScore[iCurrentPos]/200000)
				BREAK
			ENDSWITCH
		//less than ten plays then set it to %50
		ELSE
			fLastMissionDifficulty = 0.5
		ENDIF
	//less than ten plays then set it to %50
	ELSE
		fLastMissionDifficulty = 0.5
	ENDIF
	
	//Cap it
	IF fLastMissionDifficulty > 1.0
		fLastMissionDifficulty = 1.0
	ENDIF
	//Add them up
	serverBD.fChallengeDif += fLastMissionDifficulty

ENDPROC


FUNC BOOL SERVER_SORT_LEADERBOARD_PLAYERS()
	
	PRINTLN("[PLC] [SERVER_SORT_LEADERBOARD_PLAYERS], iServerPlaylistSortStage = ", iServerPlaylistSortStage)
	
	#IF IS_DEBUG_BUILD
	BOOL bSpam = FALSE  
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_NUMPAD1, KEYBOARD_MODIFIER_NONE, "")
	OR GET_COMMANDLINE_PARAM_EXISTS("sc_PlcSpam")
		bSpam = TRUE
	ENDIF
	#ENDIF
	
	INT iPlayerToCheck
	INT iPlayerLoop,iLBLoop
	THE_LEADERBOARD_STRUCT stempLB[NUM_NETWORK_PLAYERS]
	REPEAT NUM_NETWORK_PLAYERS iLBLoop
		stempLB[iLBLoop].iTeam =-1
		stempLB[iLBLoop].iparticipant =-1
		stempLB[iLBLoop].playerID = INVALID_PLAYER_INDEX()
	ENDREPEAT


	SWITCH iServerPlaylistSortStage
	
		CASE 0
			
			CLEANUP_ALL_PLAYLIST_SORTING_VARS()
			iServerPlaylistSortStage++
			PRINTLN("[PLC] [SERVER_SORT_LEADERBOARD_PLAYERS]")
			
			#IF IS_DEBUG_BUILD
			FOR iLBLoop = 0 TO (NUM_NETWORK_PLAYERS-1)
				IF NATIVE_TO_INT(serverBD_LB.sleaderboard[iLBLoop].playerID) <> -1
					PRINTLN("[PLC] [SERVER_SORT_LEADERBOARD_PLAYERS] playerID = ", GET_PLAYER_NAME(serverBD_LB.sleaderboard[iLBLoop].playerID))
				ENDIF
			ENDFOR		
			#ENDIF
			
		BREAK
	
		CASE 1
		
			#IF IS_DEBUG_BUILD
			IF bSpam = TRUE
			PRINTLN("[PLC] [SERVER_SORT_LEADERBOARD_PLAYERS] - 1")
			ENDIF
			#ENDIF
			
			sPLLB_Vars.bWinnerFound = FALSE
			
			// Get players that are not the best
			FOR iPlayerToCheck = 0 TO (NUM_NETWORK_PLAYERS-1)
//				IF serverBD_LB.sleaderboard[iPlayerToCheck].bInitialise
				IF IS_BIT_SET(serverBD_LB.sleaderboard[iPlayerToCheck].iLbdBitSet, ciLBD_BIT_INITIALISE)
					FOR iPlayerLoop = 0 TO (NUM_NETWORK_PLAYERS-1)
//						IF serverBD_LB.sleaderboard[iPlayerLoop].bInitialise
						IF IS_BIT_SET(serverBD_LB.sleaderboard[iPlayerLoop].iLbdBitSet, ciLBD_BIT_INITIALISE)
							IF NOT IS_BIT_SET(sPLLB_Vars.iAlreadyUsedPlayer,iPlayerToCheck)
							AND NOT IS_BIT_SET(sPLLB_Vars.iAlreadyUsedPlayer,iPlayerLoop)
							AND NOT IS_BIT_SET(sPLLB_Vars.iNotBestPlayer,iPlayerToCheck)
								IF iPlayerToCheck <> iPlayerLoop
									IF IS_PLAYER_BEATING_PLAYER(iPlayerLoop, iPlayerToCheck)
										SET_BIT(sPLLB_Vars.iNotBestPlayer,iPlayerToCheck)
										PRINTLN("[PLC] [SERVER_SORT_LEADERBOARD_PLAYERS], THIS_PLAYER_IS_NOT_THE_BESTEST ", iPlayerToCheck)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDFOR
				ENDIF
				
			ENDFOR
			iServerPlaylistSortStage++
			
			iStuck = 0
		BREAK
		
		CASE 2	  
		
			#IF IS_DEBUG_BUILD
			IF bSpam = TRUE
			PRINTLN("[PLC] [SERVER_SORT_LEADERBOARD_PLAYERS] - 2")
			ENDIF
			#ENDIF
			
			// Assign the best player to current top array position
			IF sPLLB_Vars.bWinnerFound = FALSE
				FOR iPlayerLoop = 0 TO (NUM_NETWORK_PLAYERS-1)
//					IF serverBD_LB.sleaderboard[iPlayerLoop].bInitialise
					IF IS_BIT_SET(serverBD_LB.sleaderboard[iPlayerLoop].iLbdBitSet, ciLBD_BIT_INITIALISE)
						IF NOT IS_BIT_SET(sPLLB_Vars.iNotBestPlayer,iPlayerLoop)
						AND NOT IS_BIT_SET(sPLLB_Vars.iAlreadyUsedPlayer,iPlayerLoop)
							sPLLB_Vars.iBestPlayerOrder[sPLLB_Vars.iCurrentLBPosition] = iPlayerLoop
							
							SET_BIT(sPLLB_Vars.iAlreadyUsedPlayer,iPlayerLoop)
							
							PRINTLN("[PLC] [SERVER_SORT_LEADERBOARD_PLAYERS], PLAYER IS NEXT BEST ", iPlayerLoop," BEING PUT INTO LB SLOT (POSITION): ",sPLLB_Vars.iCurrentLBPosition)
							
							sPLLB_Vars.iCurrentLBPosition++
							sPLLB_Vars.iCurrentPlayerToCheck++
							
							sPLLB_Vars.bWinnerFound = TRUE
							
							RETURN FALSE
						ENDIF
					ELSE
						IF NOT IS_BIT_SET(sPLLB_Vars.iAlreadyUsedPlayer,iPlayerLoop)
							sPLLB_Vars.iCurrentPlayerToCheck++
							PRINTLN("[PLC] [SERVER_SORT_LEADERBOARD_PLAYERS], THIS PLAYER HAS NO DATA MOVING ON TO: ", sPLLB_Vars.iCurrentPlayerToCheck)
							SET_BIT(sPLLB_Vars.iAlreadyUsedPlayer,iPlayerLoop)
							sPLLB_Vars.bWinnerFound = TRUE
						ENDIF
					ENDIF
				ENDFOR
			ELSE
				IF sPLLB_Vars.iCurrentPlayerToCheck < (NUM_NETWORK_PLAYERS-1)
					sPLLB_Vars.bWinnerFound = FALSE
					sPLLB_Vars.iNotBestPlayer = 0
					iStuck = 0
					iServerPlaylistSortStage = 1
					
					
					PRINTLN("[PLC] [SERVER_SORT_LEADERBOARD_PLAYERS], MOVING ONTO NEXT PLAYER: ", sPLLB_Vars.iCurrentPlayerToCheck)
				ELSE
					FOR iPlayerLoop = 0 TO (NUM_NETWORK_PLAYERS-1)
//						IF serverBD_LB.sleaderboard[iPlayerLoop].bInitialise
						IF IS_BIT_SET(serverBD_LB.sleaderboard[iPlayerLoop].iLbdBitSet, ciLBD_BIT_INITIALISE)
							IF NOT IS_BIT_SET(sPLLB_Vars.iAlreadyUsedPlayer,iPlayerLoop)
							
								sPLLB_Vars.iBestPlayerOrder[sPLLB_Vars.iCurrentLBPosition] = iPlayerLoop
								
								PRINTLN("[PLC] [SERVER_SORT_LEADERBOARD_PLAYERS], CURRENT LB POSITION: ", sPLLB_Vars.iCurrentLBPosition)
								PRINTLN("[PLC] [SERVER_SORT_LEADERBOARD_PLAYERS], WORST PLAYER: ", iPlayerLoop)
								
							ENDIF
						ENDIF
					ENDFOR

					iServerPlaylistSortStage++
				ENDIF
			ENDIF
			
			iStuck++
			IF iStuck > 500
				iStuck = 0
				iServerPlaylistSortStage++
				SCRIPT_ASSERT("[PLC] HIT STUCK CHECK: ")
				RETURN FALSE
			ENDIF
		BREAK
		
		CASE 3 // update leaderboard data	
		
			#IF IS_DEBUG_BUILD
			IF bSpam = TRUE
			PRINTLN("[PLC] [SERVER_SORT_LEADERBOARD_PLAYERS] - 3")
			ENDIF
			#ENDIF
			
			FOR iLBLoop = 0 TO (NUM_NETWORK_PLAYERS-1)
				IF sPLLB_Vars.iBestPlayerOrder[iLBLoop] !=-1
					stempLB[iLBLoop] =  serverBD_LB.sleaderboard[sPLLB_Vars.iBestPlayerOrder[iLBLoop]]
					PRINTLN("[PLC] [SERVER_SORT_LEADERBOARD_PLAYERS], PUTTING PLAYER: ", sPLLB_Vars.iBestPlayerOrder[iLBLoop]," INTO POSITION: ",iLBLoop)
				ENDIF
			ENDFOR
			
			FOR iLBLoop = 0 TO (NUM_NETWORK_PLAYERS-1)
				serverBD_LB.sleaderboard[iLBLoop] = stempLB[iLBLoop] 
				serverBD_LB.sleaderboard[iLBLoop].iRank = (iLBLoop+1)
				IF NATIVE_TO_INT(serverBD_LB.sleaderboard[iLBLoop].playerID) <> -1
					g_iPlaylistRank[NATIVE_TO_INT(serverBD_LB.sleaderboard[iLBLoop].playerID)] = serverBD_LB.sleaderboard[iLBLoop].iRank
					IF g_iPlaylistRank[NATIVE_TO_INT(serverBD_LB.sleaderboard[iLBLoop].playerID)] = 1
						serverBD.piWinningPlayer = serverBD_LB.sleaderboard[iLBLoop].playerID
						serverBD.tlWinningPlayer = GET_PLAYER_NAME(serverBD_LB.sleaderboard[iLBLoop].playerID)
				   		PRINTLN("[PLC] [SERVER_SORT_LEADERBOARD_PLAYERS]serverBD.tlWinningPlayer = ", serverBD.tlWinningPlayer)
					ENDIF
					PRINTLN("[SERVER_SORT_LEADERBOARD_PLAYERS] PLAYER ID = ", NATIVE_TO_INT(serverBD_LB.sleaderboard[iLBLoop].playerID))
					PRINTLN("[SERVER_SORT_LEADERBOARD_PLAYERS] serverBD_LB.sleaderboard[iLBLoop].iRank = ", serverBD_LB.sleaderboard[iLBLoop].iRank)
				ENDIF
			ENDFOR

			IF serverBD.iCurrentPlayListPosition < FMMC_MAX_PLAY_LIST_LENGTH				
				//Are we doing a challenge?
				
				INT iCurrentPos
				iCurrentPos = (serverBD.iCurrentPlayListPosition - 1)
				IF iCurrentPos < 0
					iCurrentPos = 0
				ENDIF
				INT iLoop
				
				IF SB_PLAYLIST_DOING_CHALLENGE()
					
					//get type of score for challenge
					IF IS_THIS_A_SCORE_BASED_MISSION(g_sCurrentPlayListDetails.sLoadedMissionDetails[iCurrentPos].iType)
						//Challenge failed
						serverBD.bChallengeFailed = TRUE
						FOR iLoop = 0 TO (NUM_NETWORK_PLAYERS - 1)					
							IF g_TransitionSessionNonResetVars.sPLLB_Globals.iChallengeScore[iLoop] > serverBD.iBestScore[iCurrentPos]	
								serverBD.bChallengeFailed = FALSE
							ENDIF
						ENDFOR						
					ELSE	
						//Challenge failed?
						IF serverBD_LB.sleaderboard[0].iMissionTime > serverBD.iBestTime[iCurrentPos]				  
							serverBD.bChallengeFailed = TRUE
						ENDIF
					ENDIF
				//Not on a challenge!   
				ELSE
					serverBD.iBestScore[iCurrentPos] = 0 
					serverBD.iBestTime[iCurrentPos] = 0					
					IF IS_THIS_A_SCORE_BASED_MISSION(g_sCurrentPlayListDetails.sLoadedMissionDetails[iCurrentPos].iType)
						FOR iLoop = 0 TO (NUM_NETWORK_PLAYERS - 1)					
							IF g_TransitionSessionNonResetVars.sPLLB_Globals.iChallengeScore[iLoop] > serverBD.iBestScore[iCurrentPos]
								serverBD.iBestScore[iCurrentPos] = g_TransitionSessionNonResetVars.sPLLB_Globals.iChallengeScore[iLoop]
					  			PRINTLN("[PLC] [SERVER_SORT_LEADERBOARD_PLAYERS] ", iLoop, " - serverBD.iBestScore[", iCurrentPos, "]  = ", serverBD.iBestScore[iCurrentPos])
							ENDIF
						ENDFOR
					ELSE
						FOR iLoop = 0 TO (NUM_NETWORK_PLAYERS - 1)	
							IF (serverBD_LB.sleaderboard[iLoop].iMissionTime < serverBD.iBestTime[iCurrentPos]
							AND serverBD_LB.sleaderboard[iLoop].iMissionTime > 0)
							OR (serverBD.iBestTime[iCurrentPos] = 0 AND serverBD_LB.sleaderboard[iLoop].iMissionTime > 0)
								serverBD.iBestTime[iCurrentPos] = serverBD_LB.sleaderboard[iLoop].iMissionTime	
					   			PRINTLN("[PLC] [SERVER_SORT_LEADERBOARD_PLAYERS] ", iLoop, " - serverBD.iBestTime[", iCurrentPos, "]   = ", serverBD.iBestTime[iCurrentPos])
							ENDIF
						ENDFOR
					ENDIF	
					
					PRINTLN("[PLC] [SERVER_SORT_LEADERBOARD_PLAYERS] serverBD.iCurrentPlayListPosition = ", iCurrentPos)
					PRINTLN("[PLC] [SERVER_SORT_LEADERBOARD_PLAYERS] serverBD.iBestScore[", iCurrentPos, "]  = ", serverBD.iBestScore[iCurrentPos])
					PRINTLN("[PLC] [SERVER_SORT_LEADERBOARD_PLAYERS] serverBD.iBestTime[", iCurrentPos, "]   = ", serverBD.iBestTime[iCurrentPos])
					//If we are setting a challenge time calculate the difficulty
					IF SB_PLAYLIST_SETTING_CHALLENGE_TIME()
						CALCULATE_CHALLENGE_DIFFICULTY(iCurrentPos)					 
					ENDIF
				ENDIF
			ENDIF
			
			CLEANUP_ALL_PLAYLIST_SORTING_VARS()
			
			PRINTLN("[PLC] [SERVER_SORT_LEADERBOARD_PLAYERS], RETURNING TRUE: ")
			serverBD.bPlayListSorted = TRUE
			RETURN TRUE	 
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_PLAYLIST_LEADERBOARD_TIMEOUT(BOOL bMinTime = FALSE)
	IF SB_PLAYLIST_SETTING_CHALLENGE_TIME()
		RETURN 1000
	ENDIF
	IF bMinTime 
		RETURN ciLEADER_BOARD_MIN_END_TIME
	ENDIF
	RETURN ciLEADER_BOARD_END_TIME
ENDFUNC


PROC SERVER_INIT_PLAYLIST_LEADERBOARD()
	#IF IS_DEBUG_BUILD
	BOOL bSpam = FALSE  
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_NUMPAD1, KEYBOARD_MODIFIER_NONE, "")
	OR GET_COMMANDLINE_PARAM_EXISTS("sc_PlcSpam")
		bSpam = TRUE
		PRINTLN("[PLC] SERVER_INIT_PLAYLIST_LEADERBOARD")
	ENDIF
	#ENDIF
	
	//IF NOT HAS_NET_TIMER_STARTED(serverBD.sCoronaTimer)
	IF serverBD.bRunLeaderBoard
		#IF IS_DEBUG_BUILD
		IF bSpam = TRUE
		PRINTLN("[PLC] SERVER_INIT_PLAYLIST_LEADERBOARD - serverBD.bRunLeaderBoard")
		ENDIF
		#ENDIF
		IF NOT IS_BIT_SET(serverBD.iLBBitset,ci_FINISH_LB)
			#IF IS_DEBUG_BUILD
			IF bSpam = TRUE
			PRINTLN("[PLC] SERVER_INIT_PLAYLIST_LEADERBOARD - IF NOT IS_BIT_SET(serverBD.iLBBitset,ci_FINISH_LB)")
			ENDIF
			#ENDIF
			IF NOT HAS_NET_TIMER_STARTED(serverBD.tdLeaderboardTimer)	   
				#IF IS_DEBUG_BUILD
				IF bSpam = TRUE
				PRINTLN("[PLC] SERVER_INIT_PLAYLIST_LEADERBOARD - IF NOT HAS_NET_TIMER_STARTED(serverBD.tdLeaderboardTimer)")
				ENDIF
				#ENDIF
				IF NOT IS_BIT_SET(serverBD.iLBBitset,ci_INIT_LB)
					#IF IS_DEBUG_BUILD
					IF bSpam = TRUE
					PRINTLN("[PLC] SERVER_INIT_PLAYLIST_LEADERBOARD - IF NOT IS_BIT_SET(serverBD.iLBBitset,ci_INIT_LB)")
					ENDIF
					#ENDIF
					IF g_TransitionSessionNonResetVars.sPLLB_Globals.binitalise
						iServerPlaylistSortStage = 0
						serverBD.bPlayListSorted = FALSE
						SERVER_POPULATE_PLAYLIST_LB_DATA()
						SERVER_CALCULATE_TEAM_SCORES()
						SERVER_SORT_LEADERBOARD_TEAMS()
						PRINTLN("[PLC] Running leaderboard init: ")
						SET_BIT(serverBD.iLBBitset,ci_INIT_LB)
					#IF IS_DEBUG_BUILD
					ELSE
						PRINTLN("[PLC] SERVER_INIT_PLAYLIST_LEADERBOARD - g_TransitionSessionNonResetVars.sPLLB_Globals.binitalise = false")
					#ENDIF					
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
					IF bSpam = TRUE
					PRINTLN("[PLC] SERVER_INIT_PLAYLIST_LEADERBOARD - ELSE")
					ENDIF
					#ENDIF
					IF SERVER_SORT_LEADERBOARD_PLAYERS()
						REINIT_NET_TIMER(serverBD.tdLeaderboardTimer)
						PRINTLN("[PLC] [LB_TIMER] Running leaderboard sort - true: ")
					ENDIF
				ENDIF
			ELSE
				IF ((GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.tdLeaderboardTimer) > GET_PLAYLIST_LEADERBOARD_TIMEOUT()
				OR serverBD.bProgressPastLB)
				AND serverBD.bCloseLeaderBoard = TRUE)
				OR (NOT HAS_PLAY_LIST_FINISHED() AND HAS_SCTV_SAID_TO_CONTINUE_PLAYLIST())
					PRINTLN("[PLC] [LB_TIMER] ci_FINISH_LB, SET cause longer than 15 secs: ")
					RESET_NET_TIMER(serverBD.tdLeaderboardTimer)
					CLEAR_BIT(serverBD.iLBBitset,ci_INIT_LB)
					SET_BIT(serverBD.iLBBitset,ci_FINISH_LB)
				#IF IS_DEBUG_BUILD
				ELSE
					PRINTLN("[PLC] [LB_TIMER] serverBD.bCloseLeaderBoard  = ", serverBD.bCloseLeaderBoard)
					PRINTLN("[PLC] [LB_TIMER] serverBD.bProgressPastLB    = ", serverBD.bProgressPastLB)
					PRINTLN("[PLC] [LB_TIMER] serverBD.tdLeaderboardTimer = ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.tdLeaderboardTimer) > GET_PLAYLIST_LEADERBOARD_TIMEOUT())
					#ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC
PROC SERVER_CLEAR_LEADERBOARD_DATA_END_OF_PLAYLIST()
	THE_LEADERBOARD_STRUCT sleaderboard
	INT i

	FOR i = 0 TO (NUM_NETWORK_PLAYERS-1)
		serverBD_LB.sleaderboard[i] = sleaderboard
	ENDFOR
	
	FOR i = 0 TO (FMMC_MAX_TEAMS-1)
		serverBD.iteamScore[i] =0
		serverBD.iteamKills[i] =0
		serverBD.iteamDeaths[i] =0
		serverBD.iteamHeadshots[i] =0
	ENDFOR
	PRINTLN("SERVER_CLEAR_LEADERBOARD_DATA_END_OF_PLAYLIST ")
ENDPROC



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////				Control playlist stage			   //////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//FUNC BOOL START_FM_MISSION_PLAYLIST()
//  IF GET_CORONA_STATUS() = CORONA_STATUS_IDLE
//	  IF NOT IS_SCREEN_FADED_OUT()
//	  AND NOT IS_SCREEN_FADING_OUT()
//		  DO_SCREEN_FADE_OUT(1000)
//	  ENDIF
//  ENDIF
//  IF NET_WARP_TO_COORD(g_sCurrentPlayListDetails.sLoadedMissionDetails[serverBD.iCurrentPlayListPosition].vStartPos, 0.0, FALSE, FALSE)
//	  CLEANUP_LEADERBOARD_CAM(FALSE)
//	  RETURN TRUE
//	  //bWarpDone = TRUE
//  ELSE
//	  PRINTLN("[PLC] Waiting on - NET_WARP_TO_COORD")
//	  RETURN FALSE
//  ENDIF
//  RETURN FALSE
//ENDFUNC



CONST_INT ciSTART_FM_MISSION_PLAYLIST_SKYSWOOP_UP	   0
CONST_INT ciSTART_FM_MISSION_PLAYLIST_SKYSWOOP_MOVE_PED 1
CONST_INT ciSTART_FM_MISSION_PLAYLIST_SET_UP_MISSION	2
CONST_INT ciSTART_FM_MISSION_PLAYLIST_SKYSWOOP_DOWN	 3
CONST_INT ciSTART_FM_MISSION_PLAYLIST_WAIT			  4

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////				Control playlist stage			   //////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
INT iSkyCamStage

//Deal with warping to a coord
FUNC BOOL START_FM_MISSION_PLAYLIST()

	BOOL bCameraIsDone
	//stop the front end
	DISABLE_FRONTEND_THIS_FRAME()
	SWITCH_TYPE sSwitchType = SWITCH_TYPE_LONG
	
	//IF playerBD[PARTICIPANT_ID_TO_INT()].iPlayListState != PLAYLIST_STATE_DISPLAY_LEADERBOARD
	//	DO_FMMC_CORONA_SPINNER(bSetUpSpinner,  "FMMC_PLYLOAD", ENUM_TO_INT(LOADING_ICON_SPINNER))
   // ENDIF
   
	DISABLE_HUD_ELEMENTS_DURING_TRANSITION_SESSION_CHANGE(TRUE, FALSE)
	//switch that stage
	SWITCH iSkyCamStage
	
		//Swoop up
		CASE ciSTART_FM_MISSION_PLAYLIST_SKYSWOOP_UP
			IF PLAYLIST_JIPPED_INTO_CORONA()
			AND NOT IS_SKYSWOOP_AT_GROUND()
				SET_PLAYLIST_JIPPED_INTO_CORONA_DO_CAM_DOWN()
			ENDIF
			IF PLAYLIST_JIPPED_INTO_CORONA_DO_CAM_DOWN()
				IF IM_ABOUT_TO_JIP_PLAYLIST_MISSION()
					PRINTLN("[PLC] START_FM_MISSION_PLAYLIST - PLAYLIST_JIPPED_INTO_CORONA - CLEAR_IM_ABOUT_TO_JIP_PLAYLIST_MISSION")
					CLEAR_IM_ABOUT_TO_JIP_PLAYLIST_MISSION()
				ENDIF
				IF IM_NOT_GOING_TO_TAKE_PART_IN_THIS_CORONA()
					PRINTLN("[PLC] START_FM_MISSION_PLAYLIST - PLAYLIST_JIPPED_INTO_CORONA - CLEAR_IM_NOT_GOING_TO_TAKE_PART_IN_THIS_CORONA")
					CLEAR_IM_NOT_GOING_TO_TAKE_PART_IN_THIS_CORONA()
				ENDIF
				IF IS_THIS_PLAYER_ON_JIP_WARNING(PLAYER_ID())
					PRINTLN("[PLC] START_FM_MISSION_PLAYLIST - PLAYLIST_JIPPED_INTO_CORONA - CLEAR_THIS_PLAYER_ON_JIP_WARNING")
					CLEAR_THIS_PLAYER_ON_JIP_WARNING()
				ENDIF
				IF SET_SKYSWOOP_DOWN(NULL, FALSE, FALSE, TRUE)	
					PRINTLN("[PLC] START_FM_MISSION_PLAYLIST - PLAYLIST_JIPPED_INTO_CORONA - SET_SKYSWOOP_DOWN - done")
					CLEAR_PLAYLIST_JIPPED_INTO_CORONA_DO_CAM_DOWN()
					RETURN TRUE
				ELSE
					PRINTLN("[PLC] START_FM_MISSION_PLAYLIST - PLAYLIST_JIPPED_INTO_CORONA - SET_SKYSWOOP_DOWN")
					RETURN FALSE
				ENDIF
			ELIF (HAVE_DONE_FIRST_PLAYLIST_MISSION() = FALSE)
			OR PLAYLIST_JIPPED_INTO_CORONA()
				CLEAR_PLAYLIST_JIPPED_INTO_CORONA_DO_CAM_DOWN()
				RETURN TRUE
			ELSE
				//If the player is ok the start the swoop
				IF IS_NET_PLAYER_OK(PLAYER_ID())
					sSwitchType = GET_SWITCH_TYPE_FOR_START_AND_END_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), g_sCurrentPlayListDetails.sLoadedMissionDetails[serverBD.iCurrentPlayListPosition].vStartPos)  
					//Swoop up
					IF SET_SKYSWOOP_UP(TRUE, FALSE, TRUE, sSwitchType, FALSE, g_sCurrentPlayListDetails.sLoadedMissionDetails[serverBD.iCurrentPlayListPosition].vStartPos.x, g_sCurrentPlayListDetails.sLoadedMissionDetails[serverBD.iCurrentPlayListPosition].vStartPos.y, g_sCurrentPlayListDetails.sLoadedMissionDetails[serverBD.iCurrentPlayListPosition].vStartPos.z)
					OR (IS_PLAYER_SCTV(PLAYER_ID()) AND IS_PLAYER_IN_CORONA())
						PRINTLN("[PLC] START_FM_MISSION_PLAYLIST - CLEANUP_LEADERBOARD_CAM")
						CLEANUP_LEADERBOARD_CAM(TRUE, TRUE, FALSE, FALSE)
						iSkyCamStage = ciSTART_FM_MISSION_PLAYLIST_SKYSWOOP_MOVE_PED
						PRINTLN("[PLC] START_FM_MISSION_PLAYLIST - iSkyCamStage = ciSTART_FM_MISSION_PLAYLIST_SKYSWOOP_MOVE_PED")
					ENDIF
				//if they are not ok then spam
				ELSE
					PRINTLN("[PLC] START_FM_MISSION_PLAYLIST - IS_NET_PLAYER_OK = FALSE")
				ENDIF
			ENDIF
		BREAK
		
		//Warp
		CASE ciSTART_FM_MISSION_PLAYLIST_SKYSWOOP_MOVE_PED
			IF playerBD[PARTICIPANT_ID_TO_INT()].iPlayListState != PLAYLIST_STATE_DISPLAY_LEADERBOARD
			AND playerBD[PARTICIPANT_ID_TO_INT()].iPlayListState != PLAYLIST_CHECK_CHALLENGE_IS_OPEN
			AND playerBD[PARTICIPANT_ID_TO_INT()].iPlayListState != PLAYLIST_STATE_WAIT_TILL_SERVER_READY
				//Warp
				IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
					IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
						PRINTLN("[PLC] START_FM_MISSION_PLAYLIST - SET_ENTITY_COORDS - g_sCurrentPlayListDetails.sLoadedMissionDetails[", serverBD.iCurrentPlayListPosition, "].vStartPos = ", g_sCurrentPlayListDetails.sLoadedMissionDetails[serverBD.iCurrentPlayListPosition].vStartPos)
						SET_ENTITY_COORDS(PLAYER_PED_ID(), g_sCurrentPlayListDetails.sLoadedMissionDetails[serverBD.iCurrentPlayListPosition].vStartPos)
						g_vInitialSpawnLocation = << 0.0, 0.0, 0.0 >>
						PRINTLN("g_vInitialSpawnLocation = ", g_vInitialSpawnLocation)				  
						SET_ENTITY_HEADING(PLAYER_PED_ID(), 0.0)
						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
					ENDIF
				ENDIF
				//Set that the playlist want the mission at coords to set this up
				SET_PLAYLIST_READY_FOR_MISSION_AT_COORDS()
				//move stage
				iSkyCamStage = ciSTART_FM_MISSION_PLAYLIST_SET_UP_MISSION
				PRINTLN("[PLC] START_FM_MISSION_PLAYLIST - iSkyCamStage = ciSTART_FM_MISSION_PLAYLIST_SET_UP_MISSION")
			ELSE
				PRINTLN("[PLC] START_FM_MISSION_PLAYLIST - playerBD[PARTICIPANT_ID_TO_INT()].iPlayListState != PLAYLIST_STATE_DISPLAY_LEADERBOARD")
			ENDIF
		BREAK
		
		//Wait for the mission to be set up
		CASE ciSTART_FM_MISSION_PLAYLIST_SET_UP_MISSION
			//Has the mission at coords set this up.
			IF HAS_PLAYLIST_NEXT_MISSION_AT_COORDS_BEEN_SETUP()
				//Clear the wait flags
				CLEAR_PLAYLIST_READY_FOR_MISSION_AT_COORDS()
				CLEAR_PLAYLIST_NEXT_MISSION_AT_COORDS_BEEN_SETUP()
				//move stage
				iSkyCamStage = ciSTART_FM_MISSION_PLAYLIST_SKYSWOOP_DOWN				
				IF IM_ABOUT_TO_JIP_PLAYLIST_MISSION()
					CLEAR_IM_ABOUT_TO_JIP_PLAYLIST_MISSION()
				ENDIF
				PRINTLN("[PLC] START_FM_MISSION_PLAYLIST - iSkyCamStage = ciSTART_FM_MISSION_PLAYLIST_SKYSWOOP_DOWN")
			ENDIF
		BREAK
		
		//Swoop down
		CASE ciSTART_FM_MISSION_PLAYLIST_SKYSWOOP_DOWN
		
			bCameraIsDone = FALSE 
// url:bugstar:2355619 - GTAOTV - Between race jobs in a playlist the spectator's camera travels to the origin before loading the next corona, resulting in slow streaming
//			IF IS_PLAYER_SCTV(PLAYER_ID())
//				IF SET_SKYSWOOP_DOWN_PROPERTY(DEFAULT, FALSE, DEFAULT, FALSE, TRUE)
//					bCameraIsDone = TRUE
//				ENDIF
//				
//				IF IS_SKYCAM_ON_LAST_CUT()
//					SET_SKYFREEZE_FROZEN()
//				ENDIF
//				
			IF GET_CORONA_STATUS() != CORONA_STATUS_IDLE
				IF SET_SKYSWOOP_DOWN(NULL, FALSE, FALSE, TRUE)	
					bCameraIsDone = TRUE
				ENDIF
			ENDIF
			 
			IF bCameraIsDone

				//Clear up that there spinner if we are idle
				IF GET_CORONA_STATUS() = CORONA_STATUS_IDLE
					BUSYSPINNER_OFF()
					PRINTLN("[PLC] START_FM_MISSION_PLAYLIST - BUSYSPINNER_OFF")
				ENDIF
				STOP_AUDIO_SCENE("MP_JOB_CHANGE_RADIO_MUTE")
				PRINTLN("[PLC] START_FM_MISSION_PLAYLIST - called STOP_AUDIO_SCENE(MP_JOB_CHANGE_RADIO_MUTE).")
				//bSetUpSpinner = FALSE
				//Done
				PRINTLN("[PLC] START_FM_MISSION_PLAYLIST - iSkyCamStage = ciSTART_FM_MISSION_PLAYLIST_WAIT")
				iSkyCamStage = ciSTART_FM_MISSION_PLAYLIST_WAIT	 
			ENDIF
		BREAK
		
		CASE ciSTART_FM_MISSION_PLAYLIST_WAIT
			IF playerBD[PARTICIPANT_ID_TO_INT()].iPlayListState = PLAYLIST_STATE_WAIT_TO_START_MISSION
			AND GET_CORONA_STATUS() != CORONA_STATUS_IDLE	   
				PRINTLN("[PLC] START_FM_MISSION_PLAYLIST - START_FM_MISSION_PLAYLIST Returing TRUE")
				CLEAR_PLAYLIST_JIPPED_INTO_CORONA_DO_CAM_DOWN()
				RETURN TRUE
			ENDIF
		BREAK
		
	ENDSWITCH
	
	//not ready yet
	RETURN FALSE
ENDFUNC

//draw the count down timer
PROC MAINTAIN_LEADER_BOARD_COUNT_DOWN_TIMER(INT iLeaderBoardTime)
	INT iScreenX, iScreenY
	//Draw the menu help
	GET_SCREEN_RESOLUTION(iScreenX, iScreenY)
	DRAW_MENU_HELP_SCALEFORM(iScreenX, -1, iLeaderBoardTime, TRUE, TRUE, TRUE)	
	
	PRINTLN("MAINTAIN_LEADER_BOARD_COUNT_DOWN_TIMER iLeaderBoardTime		 = ", iLeaderBoardTime)
	PRINTLN("MAINTAIN_LEADER_BOARD_COUNT_DOWN_TIMER g_sMenuData.iHelpCount   = ", g_sMenuData.iHelpCount)
	PRINTLN("MAINTAIN_LEADER_BOARD_COUNT_DOWN_TIMER g_sMenuData.bHelpCreated = ",  g_sMenuData.bHelpCreated)
ENDPROC
#IF IS_DEBUG_BUILD
PROC PRINT_LEADER_BOARD_DATA()
	PRINTLN("[PLC] ServerBD.iNumPlaylistTeams	 = ", ServerBD.iNumPlaylistTeams)
	IF ServerBD.iNumPlaylistTeams > FMMC_MAX_TEAMS
		SCRIPT_ASSERT("iNumPlaylistTeams > FMMC_MAX_TEAMS")
	ENDIF
	PRINTLN("[PLC] ServerBD.iWinningTeam		  = ", ServerBD.iWinningTeam )
	IF ServerBD.iWinningTeam > FMMC_MAX_TEAMS
		SCRIPT_ASSERT("iWinningTeam > FMMC_MAX_TEAMS")
	ENDIF
	PRINTLN("[PLC] ServerBD.iSecondTeam		   = ", ServerBD.iSecondTeam)
	IF ServerBD.iSecondTeam > FMMC_MAX_TEAMS
		SCRIPT_ASSERT("iSecondTeam > FMMC_MAX_TEAMS")
	ENDIF
	PRINTLN("[PLC] ServerBD.iThirdTeam			= ", ServerBD.iThirdTeam)
	IF ServerBD.iThirdTeam > FMMC_MAX_TEAMS
		SCRIPT_ASSERT("iThirdTeam > FMMC_MAX_TEAMS")
	ENDIF
	PRINTLN("[PLC] ServerBD.iLosingTeam		   = ", ServerBD.iLosingTeam)
	IF ServerBD.iLosingTeam > FMMC_MAX_TEAMS
		SCRIPT_ASSERT("iLosingTeam > FMMC_MAX_TEAMS")
	ENDIF
	INT iLoop 
	FOR iLoop = 0 TO (NUM_NETWORK_PLAYERS - 1)
	PRINTLN("[PLC] serverBD_LB.sleaderboard[", iLoop, "].iTeam = ", serverBD_LB.sleaderboard[iLoop].iTeam)
	IF serverBD_LB.sleaderboard[iLoop].iTeam > FMMC_MAX_TEAMS
		SCRIPT_ASSERT(" serverBD_LB.sleaderboard[iLoop].iTeam > FMMC_MAX_TEAMS")
	ENDIF
	ENDFOR		  
ENDPROC 

PROC CHECK_CHALLENGE_CONTENT_ID()
	IF NOT ARE_STRINGS_EQUAL(g_sCurrentPlayListDetails.tl23ChallengeContentID, serverBD.tl23PlayListToPlay)
		PRINTLN("[PLC] NOT ARE_STRINGS_EQUAL(", g_sCurrentPlayListDetails.tl23ChallengeContentID, ", ", serverBD.tl23PlayListToPlay, ")")
		PRINTLN("[PLC] NOT ARE_STRINGS_EQUAL(g_sCurrentPlayListDetails.tl23ChallengeContentID, serverBD.tl23PlayListToPlay)")
		SCRIPT_ASSERT("ARE_STRINGS_EQUAL(g_sCurrentPlayListDetails.tl23ChallengeContentID, serverBD.tl23PlayListToPlay)")
	ENDIF
ENDPROC


#ENDIF


PROC SET_UP_PL_LEADER_BOARD_HELP(INT iLeaderBoardTime)
	REMOVE_MENU_HELP_KEYS()
	//Add a spinner
	PRINTLN("[PLC] CONTROL_PLAYLIST - PLAYLIST_STATE_WAIT_TILL_SERVER_READY - ADD_MENU_HELP_KEY - ICON_SPINNER")	
	IF NOT SB_IS_TOURNAMENT_PLAYLIST()
	AND NOT SB_IS_SCTV_CONTROLLED_PLAYLIST()
		ADD_MENU_HELP_KEY("", "END_LBD_CONTN", iLeaderBoardTime)
	ENDIF
	//are we at the end of a playlist add a restart option
	IF HAS_PLAY_LIST_FINISHED()
	AND NOT SB_PLAYLIST_DOING_CHALLENGE()
	AND NOT SB_PLAYLIST_DOING_HEAD_TO_HEAD()
	AND NOT SB_PLAYLIST_SETTING_CHALLENGE_TIME()
	AND NOT SB_IS_TOURNAMENT_PLAYLIST()
	AND NOT SB_IS_LIVESTREAM_PLAYLIST()
	AND NOT SB_IS_QUALIFYING_PLAYLIST()
	AND NOT SB_IS_SCTV_CONTROLLED_PLAYLIST()
		PRINTLN("[PLC] CONTROL_PLAYLIST - PLAYLIST_STATE_WAIT_TILL_SERVER_READY - ADD_MENU_HELP_KEY - GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)")					   
		ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT, "FMMC_END_CONT")//CONTINUE
		PRINTLN("[PLC] CONTROL_PLAYLIST - PLAYLIST_STATE_WAIT_TILL_SERVER_READY - ADD_MENU_HELP_KEY - GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_X)")					   
		ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_X, "HUD_TITLE35")//RESTART
	ELSE
		PRINTLN("[PLC] CONTROL_PLAYLIST - PLAYLIST_STATE_WAIT_TILL_SERVER_READY - ADD_MENU_HELP_KEY - GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)")					   
		ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT, "FMMC_END_CONT")//CONTINUE						 
	ENDIF
	STRING sProfile = GET_PROFILE_BUTTON_NAME()
	ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_SELECT,	sProfile)	   //PLAYER CARD
ENDPROC

//Host reset stuff
PROC RESET_HOST_LEADER_BOARD_VARS()
	IF IS_BIT_SET(serverBD.iLBBitset,ci_FINISH_LB)
		CLEAR_BIT(serverBD.iLBBitset,ci_FINISH_LB)
		PRINTLN("[PLC]  clearing ci_FINISH_LB, since on mission")
	ENDIF			   
	IF IS_BIT_SET(serverBD.iLBBitset,ci_INIT_LB)
		CLEAR_BIT(serverBD.iLBBitset,ci_INIT_LB)				
		PRINTLN("[PLC]  clearing ci_INIT_LB, since on mission")
	ENDIF
	IF serverBD.bReSetLateFlags
		serverBD.bReSetLateFlags = FALSE
		PRINTLN("[PLC]  serverBD.bReSetLateFlags")
	ENDIF
ENDPROC

//Store out the current playlist times for tournaments
PROC STORE_OUT_CURRENT_PLAYLIST_TIMES_AND_SCORES()
	INT iPos = (serverBD.iNumberOfMissionsPlayed - 1)
	IF iPos < 0
	iPos = 0
	ENDIF
	//Get the time
	IF g_TransitionSessionNonResetVars.sPLLB_Globals.iEventtime[NATIVE_TO_INT(PLAYER_ID())] > 0
	AND NOT DID_I_JOIN_MISSION_AS_SPECTATOR()
	AND NOT IS_PLAYER_SCTV(PLAYER_ID())
		sLocalPlayersScores.iTime[iPos] = g_TransitionSessionNonResetVars.sPLLB_Globals.iEventtime[NATIVE_TO_INT(PLAYER_ID())]
	//if it's 0 or a DNF then use MAX INT
	ELSE 
		INT iHighest
		INT iLoop
		FOR iLoop = 0 TO (NUM_NETWORK_PLAYERS -1)
			IF g_TransitionSessionNonResetVars.sPLLB_Globals.iEventtime[iLoop] > iHighest
				iHighest = g_TransitionSessionNonResetVars.sPLLB_Globals.iEventtime[iLoop]
			ENDIF
		ENDFOR
		iHighest += 60000 
		PRINTLN("[PLC] sLocalPlayersScores.iTime[", iPos, "] = iHighest = ", iHighest)
		sLocalPlayersScores.iTime[iPos] = iHighest
//		IF SB_IS_QUALIFYING_PLAYLIST()
//			g_sQualifyTourFeed.bFailedToSetTime = TRUE
//			g_sQualifyTourFeed.bSetTime = FALSE		
//			g_sQualifyTourFeed.iTotalTime = 0
//		ENDIF
	ENDIF
	//Get the score, something that's - is wrong
	IF g_TransitionSessionNonResetVars.sPLLB_Globals.iEventscore[NATIVE_TO_INT(PLAYER_ID())] > 0
		sLocalPlayersScores.iScore[iPos] = g_TransitionSessionNonResetVars.sPLLB_Globals.iEventscore[NATIVE_TO_INT(PLAYER_ID())]
	ENDIF
	IF serverBD.bDmTournament
		sLocalPlayersScores.iKills[iPos] = g_TransitionSessionNonResetVars.sPLLB_Globals.ikills[NATIVE_TO_INT(PLAYER_ID())]
		sLocalPlayersScores.iDeaths[iPos] = g_TransitionSessionNonResetVars.sPLLB_Globals.iDeaths[NATIVE_TO_INT(PLAYER_ID())]
	ENDIF
	PRINTLN("[PLC] sLocalPlayersScores.iTime[", iPos, "]   = ", sLocalPlayersScores.iTime[iPos])
	PRINTLN("[PLC] sLocalPlayersScores.iScore[", iPos, "]  = ", sLocalPlayersScores.iScore[iPos])
	PRINTLN("[PLC] sLocalPlayersScores.iKills[", iPos, "]  = ", sLocalPlayersScores.iKills[iPos])
	PRINTLN("[PLC] sLocalPlayersScores.iDeaths[", iPos, "] = ", sLocalPlayersScores.iDeaths[iPos])
	PRINTLN("[PLC] serverBD.iNumberOfMissionsPlayed = ", serverBD.iNumberOfMissionsPlayed)   
ENDPROC

PROC GET_LEADERBOARD_RANKS()
	
	IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, ciPLAYER_GRABBED_LEADERBOARD_RANKS)
		//Need to get ranks to make grid
		INT i
		FOR i = 0 TO (NUM_NETWORK_PLAYERS-1)
			IF NATIVE_TO_INT(serverBD_LB.sleaderboard[i].playerID) <> -1
				g_iPlaylistRank[NATIVE_TO_INT(serverBD_LB.sleaderboard[i].playerID)] = serverBD_LB.sleaderboard[i].iRank
			ENDIF
		ENDFOR
		SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, ciPLAYER_GRABBED_LEADERBOARD_RANKS)
	ENDIF

ENDPROC
//Should we warp to the start of the mission
FUNC BOOL SHOULD_WE_WARP_TO_START_OF_NEXT_MISSION()
	//if the server has not cast the vote
	IF NOT IS_BIT_SET(serverBD.iServerVoteBitSet, ciServerVoteCast)
		RETURN FALSE
	ENDIF   
	//if the leader bord has finished
	IF HAS_PLAY_LIST_FINISHED()
		RETURN FALSE
	ENDIF
	
	//if this is a challenge
	IF SB_PLAYLIST_DOING_CHALLENGE()
		//If the timer has not started then get out
		IF NOT HAS_NET_TIMER_STARTED(serverBD.tdLeaderboardTimer)
			RETURN FALSE
		ENDIF
		//if the challenge has been failed.
		IF HAS_CHALLENGE_BEEN_FAILED() 
			RETURN FALSE			
		ENDIF
	ENDIF
	RETURN TRUE
ENDFUNC

//Returns true if the server has set this mission as played. 
FUNC BOOL HAS_SERVER_SET_MISSION_AS_PLAYED()
	IF PARTICIPANT_ID_TO_INT() = -1
		RETURN FALSE
	ENDIF	
	RETURN IS_BIT_SET(serverBD.iMissionLbDone , playerBD[PARTICIPANT_ID_TO_INT()].iCurrentPlayListPos)
ENDFUNC

//Should a playlist leaderboard progress. 
FUNC BOOL SHOULD_PLAYLIST_LEADER_BOARD_PROGRESS()
	//If it's a tournament and SCTV have said continue
	IF SB_IS_TOURNAMENT_PLAYLIST()
	OR SB_IS_SCTV_CONTROLLED_PLAYLIST()
		//If we've not finished the playlist
		IF NOT HAS_PLAY_LIST_FINISHED()
			IF HAS_SCTV_SAID_TO_CONTINUE_PLAYLIST()
			OR NOT IS_DEV_SPECTATOR_PRESENT()
				RETURN TRUE
			ENDIF
		ENDIF
	//Not a tournament then return TRUE when all this stuf is true
	ELSE
		IF (IS_BIT_SET(serverBD.iLBBitset,ci_FINISH_LB) //AND IS_BIT_SET(serverBD.iServerVoteBitSet, ciServerVoteCast)) 					
		//OR HAS_SERVER_SET_MISSION_AS_PLAYED())
		AND NOT HAS_PLAY_LIST_FINISHED())			
		OR (NOT SHOULD_DISPLAY_PLAYLIST_LEADERBOARD() AND IS_BIT_SET(serverBD.iLBBitset,ci_INIT_LB) AND NOT HAS_PLAY_LIST_FINISHED() AND HAS_NET_TIMER_STARTED(serverBD.tdLeaderboardTimer))
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC


//The SCTV player needs to wait for the person they are spectating to be in a corona, this will do that.
FUNC BOOL SHOULD_SCTV_PLAYER_PROGRESS()
	PLAYER_INDEX piSctvTarget
	IF IS_PLAYER_SCTV(PLAYER_ID())
		piSctvTarget = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].playerSCTVTarget
		IF piSctvTarget != INVALID_PLAYER_INDEX()
		AND NETWORK_IS_PLAYER_ACTIVE(piSctvTarget)
			IF GET_PLAYER_CORONA_STATUS(piSctvTarget) != CORONA_STATUS_IDLE
				RETURN TRUE
			ENDIF
			RETURN FALSE
		ENDIF
	ENDIF
	RETURN TRUE
ENDFUNC

//Warp the player to the start of a rounds mission
INT iWarpRoundsMission
INT iWaitCount
BOOL bSafeToStart
BOOL bDoingSkyCam
PROC WARP_TO_START_OF_ROUNDS_MISSION()
	//Warp the player back to the start of the corna
	SWITCH iWarpRoundsMission
		//Reset
		CASE 0
			iWaitCount = 0
			iWarpRoundsMission++
			bSafeToStart = FALSE
			bDoingSkyCam = FALSE
		BREAK
		
		//hack to wait for the MAC to clear SET_BIT(g_sAtCoordsMP[paramSlot].matcStateBitflags, MATC_BITFLAG_CURRENT_PLAYLIST_ACTIVITY)
		//to remove hac3e please remove all referances to iWaitCount and it'll be all good. 
		CASE 1
			iWaitCount++
			IF iWaitCount > 20
				PRINTLN("[PLC] WARP_TO_START_OF_ROUNDS_MISSION - SET_PLAYLIST_READY_FOR_MISSION_AT_COORDS")
				iWaitCount = 0
				iWarpRoundsMission++
				IF IS_PLAYER_SCTV(PLAYER_ID())
				AND GET_SCTV_MODE() = SCTV_MODE_FREE_CAM	
					SWITCH_SCTV_MODE(SCTV_MODE_FOLLOW_CAM)
					PRINTLN("[PLC] CONTROL_PLAYLIST - SWITCH_SCTV_MODE(SCTV_MODE_FOLLOW_CAM)")
				ENDIF
			ENDIF
		BREAK
		
		//move them
		CASE 2
			//Sky cam is in the sky
			IF IS_SKYSWOOP_IN_SKY()	
			AND NOT bDoingSkyCam
				
				IF NOT IS_PLAYER_SCTV(PLAYER_ID())
				OR SHOULD_SCTV_PLAYER_PROGRESS()
					//Move the player
					IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
						IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
							PRINTLN("[PLC] WARP_TO_START_OF_ROUNDS_MISSION - SET_ENTITY_COORDS(PLAYER_PED_ID(), g_FMMC_STRUCT.vStartPos) - g_sCurrentPlayListDetails.sLoadedMissionDetails[", serverBD.iCurrentPlayListPosition, "].vStartPos = ", g_sCurrentPlayListDetails.sLoadedMissionDetails[serverBD.iCurrentPlayListPosition].vStartPos)				 
							SET_ENTITY_COORDS(PLAYER_PED_ID(), g_sCurrentPlayListDetails.sLoadedMissionDetails[serverBD.iCurrentPlayListPosition].vStartPos)
							g_vInitialSpawnLocation = << 0.0, 0.0, 0.0 >>
							PRINTLN("g_vInitialSpawnLocation = ", g_vInitialSpawnLocation)				  
							SET_ENTITY_HEADING(PLAYER_PED_ID(), 0.0)
							FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
							SET_PLAYLIST_READY_FOR_MISSION_AT_COORDS()						
							iWarpRoundsMission++
						ENDIF
					ENDIF
				ENDIF
			//Waiting for the sky cam
			ELSE
				//Wait for it to be at the ground before starting
				IF bSafeToStart = FALSE
					PRINTLN("[PLC] - WARP_TO_START_OF_ROUNDS_MISSION - IS_SKYSWOOP_AT_GROUND")
					IF IS_SKYSWOOP_AT_GROUND()
						bSafeToStart = TRUE
						bDoingSkyCam = TRUE
					ENDIF
				ELSE
					//Call swoop up untill it's done.
					PRINTLN("[PLC] - WARP_TO_START_OF_ROUNDS_MISSION - SET_SKYSWOOP_UP")
					IF SET_SKYSWOOP_UP(TRUE, FALSE, TRUE, SWITCH_TYPE_LONG, FALSE, g_sCurrentPlayListDetails.sLoadedMissionDetails[serverBD.iCurrentPlayListPosition].vStartPos.x, g_sCurrentPlayListDetails.sLoadedMissionDetails[serverBD.iCurrentPlayListPosition].vStartPos.y, g_sCurrentPlayListDetails.sLoadedMissionDetails[serverBD.iCurrentPlayListPosition].vStartPos.z)
						bDoingSkyCam = FALSE
					ENDIF
				ENDIF
			ENDIF
		BREAK	
		
		CASE 3		
			//Has the mission at coords set this up.
			IF HAS_PLAYLIST_NEXT_MISSION_AT_COORDS_BEEN_SETUP()
				//Clear the wait flags
				CLEAR_PLAYLIST_READY_FOR_MISSION_AT_COORDS()
				CLEAR_PLAYLIST_NEXT_MISSION_AT_COORDS_BEEN_SETUP()
				//move stage
				iWarpRoundsMission++				
				PRINTLN("[PLC] WARP_TO_START_OF_ROUNDS_MISSION - HAS_PLAYLIST_NEXT_MISSION_AT_COORDS_BEEN_SETUP")
			ENDIF
		BREAK
		
		//Wait for the corona
		CASE 4
			IF GET_CORONA_STATUS() != CORONA_STATUS_IDLE	   
				PRINTLN("[PLC] WARP_TO_START_OF_ROUNDS_MISSION - cGET_CORONA_STATUS() != CORONA_STATUS_IDLE	")				 
				iWarpRoundsMission++
				iWaitCount = 0			
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC 



//Controll the playlist. 
PROC CONTROL_PLAYLIST()
	g_sCurrentPlayListDetails.iCurrentPlayListPosition = serverBD.iCurrentPlayListPosition
	g_sCurrentPlayListDetails.iPlaylistProgress = (serverBD.iNumberOfMissionsPlayed + 1)
	
	GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iCurrentPlayListPosition = g_sCurrentPlayListDetails.iCurrentPlayListPosition
	
	//Check to see if I can clear my clear flag
	IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, ciPLAYER_CLEAR_MY_LB_DATA_PLEASE)
		IF IS_BIT_SET(serverBD.iServerClearPlayerBitSet, PARTICIPANT_ID_TO_INT())
			CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, ciPLAYER_CLEAR_MY_LB_DATA_PLEASE)
			PRINTLN("[PLC] ciPLAYER_CLEAR_MY_LB_DATA_PLEASE, CLEAR_BIT(playerBD[", PARTICIPANT_ID_TO_INT(), "].iPlayerBitSet, ciPLAYER_CLEAR_MY_LB_DATA_PLEASE)")
		ENDIF
	ENDIF
	
	INT iLeaderBoardTime
	#IF IS_DEBUG_BUILD
	BOOL bSpam = FALSE  
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_NUMPAD1, KEYBOARD_MODIFIER_NONE, "")
		bSpam = TRUE
		PRINTLN("[PLC] CONTROL_PLAYLIST, iPlayListState = ", playerBD[PARTICIPANT_ID_TO_INT()].iPlayListState )
	ENDIF
	#ENDIF
	//Switch the playlist state
	SWITCH playerBD[PARTICIPANT_ID_TO_INT()].iPlayListState
		
		//warp to the start of the playlist
		CASE PLAYLIST_STATE_WAIT_TO_START_MISSION
			#IF IS_DEBUG_BUILD
			IF bSpam = TRUE
			PRINTLN("[PLC] CONTROL_PLAYLIST - PLAYLIST_STATE_WAIT_TO_START_MISSION")
			ENDIF
			#ENDIF
			//Clear the reset flags
			IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
				IF serverBD.bServerResetFlags = TRUE
					PRINTLN("[PLC] CONTROL_PLAYLIST - serverBD.bServerResetFlags = FALSE 1")
					serverBD.bServerResetFlags = FALSE
				ENDIF
			ENDIF
			
			//If we are not on a mission
			IF (NOT IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID(), FALSE)
			AND NOT IS_FM_MISSION_LAUNCH_IN_PROGRESS() )
			//Or we are in the corona and we've not done the first mission
			OR (GET_CORONA_STATUS() > CORONA_STATUS_IN_CORONA
			AND NOT HAVE_DONE_FIRST_PLAYLIST_MISSION())
				//Clean up the vote
				IF bVoteCleanedUp = TRUE
					FM_PLAY_LIST_CLEAN_VARS()
					bVoteCleanedUp = FALSE
					PRINTLN("[PLC] SET_PLAYER_CONTROL = FALSE")
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				ENDIF
				
				//If it's a tournament
				IF IS_PLAYLIST_IS_A_TOURNAMENT_PLAYLIST()
					//If it's not a DM tournament
					IF NOT serverBD.bDmTournament
						IF NOT GET_MP_BOOL_CHARACTER_STAT(MP_STAT_UNLOCK_RACE_HIPSTER_TSHIRT)
							SET_MP_BOOL_CHARACTER_STAT(MP_STAT_UNLOCK_RACE_HIPSTER_TSHIRT, TRUE)
							PRINTLN("[PLC] PROCESS_PRE_GAME - SET_MP_BOOL_CHARACTER_STAT(MP_STAT_UNLOCK_RACE_HIPSTER_TSHIRT TRUE")
						ENDIF
					//If it's a dem_tournament
					ELSE
						IF NOT GET_MP_BOOL_CHARACTER_STAT(MP_STAT_UNLOCK_DM_HIPSTER_TSHIRT)
							PRINTLN("[PLC] PROCESS_PRE_GAME - SET_MP_BOOL_CHARACTER_STAT(MP_STAT_UNLOCK_DM_HIPSTER_TSHIRT TRUE")
							SET_MP_BOOL_CHARACTER_STAT(MP_STAT_UNLOCK_DM_HIPSTER_TSHIRT, TRUE)
						ENDIF
					ENDIF
				ENDIF
				
				//set the mission owner and file name
				GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].tl23CurrentMissionOwner	   = g_sCurrentPlayListDetails.sLoadedMissionDetails[serverBD.iCurrentPlayListPosition].tlOwner  
				GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].tl31CurrentMissionFileName	= g_sCurrentPlayListDetails.tl31ContentIDs.szContentID[serverBD.iCurrentPlayListPosition]
				//warp the player to the start of a mission
				IF START_FM_MISSION_PLAYLIST()//sFMMCdataFile, serverBD.iInstanceId, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].tl23CurrentMissionOwner, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].tl31CurrentMissionFileName)
					iSkyCamStage = ciSTART_FM_MISSION_PLAYLIST_SKYSWOOP_UP	  
					PRINTLN("CONTROL_PLAYLIST - [PLC] START_FM_MISSION_PLAYLIST - iSkyCamStage = ciSTART_FM_MISSION_PLAYLIST_SKYSWOOP_UP")
					//DO_SCREEN_FADE_IN(50)
					PRINTLN("[PLC] START_FM_MISSION_PLAYLIST = TRUE")
					//clear that we started a launched PL
					CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_JoinedLaunched_Playlist)
					PRINTLN("CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_JoinedLaunched_Playlist)")
					IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
						RESET_NET_TIMER(serverBD.tdLeaderboardTimer)
						PRINTLN("[PLC]  [LB_TIMER] RESET_NET_TIMER(serverBD.tdLeaderboardTimer)")
					ENDIF
					
					#IF IS_DEBUG_BUILD
					DEBUG_OUTPUT_PLAYLIST_VARS()
					#ENDIF			
					
					SET_FM_PLAYLIST_STATE(playerBD, PLAYLIST_STATE_CLEANUP_WHEN_ON_MISSION)
					EXIT
				ENDIF
			ENDIF
		BREAK
		
		//Reset things and move on
		CASE PLAYLIST_STATE_CLEANUP_WHEN_ON_MISSION
			#IF IS_DEBUG_BUILD
			IF bSpam = TRUE
			PRINTLN("[PLC] CONTROL_PLAYLIST - PLAYLIST_STATE_CLEANUP_WHEN_ON_MISSION")
			ENDIF
			#ENDIF
			IF (IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID(), FALSE))
			AND (GET_CORONA_STATUS() = CORONA_STATUS_IDLE)  
				//Host reset stuff
				IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
					IF serverBD.bServerResetFlags = FALSE
						//Host reset stuff
						PRINTLN("[PLC] CONTROL_PLAYLIST - RESET_HOST_LEADER_BOARD_VARS")
						RESET_HOST_LEADER_BOARD_VARS()
						serverBD.bServerResetFlags = TRUE
					ENDIF
				ENDIF
				IF IS_PLAYER_SCTV(PLAYER_ID())
					HIDE_SPECTATOR_HUD(FALSE)
				ENDIF
				CLEAR_PLAYLIST_JIPPED_WHEN_ON_PLAYLIST_LB()
				CLEAR_SCTV_SAID_TO_CONTINUE_PLAYLIST()
				//If this is the last mission then set that it is
				IF IS_THIS_THE_LAST_MISSION_ON_A_PLAYLIST()
					SET_THAT_THIS_IS_THE_LAST_PLAYLIST_MISSION()
				ENDIF
				
				//Get the playlist stary pos
				IF PARTICIPANT_ID_TO_INT() != -1
					IF playerBD[PARTICIPANT_ID_TO_INT()].iCurrentPlayListPos != serverBD.iCurrentPlayListPosition
						PRINTLN("[PLC] CONTROL_PLAYLIST - playerBD[PARTICIPANT_ID_TO_INT()].iCurrentPlayListPos = ", serverBD.iCurrentPlayListPosition)
						playerBD[PARTICIPANT_ID_TO_INT()].iCurrentPlayListPos = serverBD.iCurrentPlayListPosition
					ENDIF
				ENDIF
				#IF IS_DEBUG_BUILD
				IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, ciPLAYER_BS_PASSED_LB)
					PRINTLN("[PLC] CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, ciPLAYER_BS_PASSED_LB)")
				ENDIF
				#ENDIF
				CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, ciPLAYER_BS_PASSED_LB)
				SET_FM_PLAYLIST_STATE(playerBD, PLAYLIST_STATE_WAIT_TILL_OFF_ROUNDS_MISSION)
			ENDIF
		BREAK
		
		//If it's a rounds mission then we wait
		CASE PLAYLIST_STATE_WAIT_TILL_OFF_ROUNDS_MISSION
			IF NOT IS_THIS_A_ROUNDS_MISSION()
			OR SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD()
				SET_FM_PLAYLIST_STATE(playerBD, PLAYLIST_STATE_WAIT_TILL_END_OF_MISSION)	
			ELSE
				//If we are no longer on the mission
				IF NOT IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID(), FALSE)
				AND NOT IS_FM_MISSION_LAUNCH_IN_PROGRESS()
					WARP_TO_START_OF_ROUNDS_MISSION()
				ELSE
					#IF IS_DEBUG_BUILD
					IF iWarpRoundsMission != 0
						PRINTLN("[PLC] WARP_TO_START_OF_ROUNDS_MISSION - iWarpRoundsMission = ", iWarpRoundsMission)				 
					ENDIF
					#ENDIF
					iWarpRoundsMission = 0
				ENDIF
				#IF IS_DEBUG_BUILD
				IF bSpam = TRUE
					PRINTLN("[PLC] CONTROL_PLAYLIST - IS_THIS_A_ROUNDS_MISSION = TRUE")
				ENDIF
				#ENDIF
			ENDIF
		BREAK

		//Wait till the server vote is cast
		CASE PLAYLIST_STATE_WAIT_TILL_END_OF_MISSION	
		
			//Then we can print something - 1563993 - Feed Message at start of first job of Qualifying Playlist - telling players they are launching a qualifying playlist
			//If this is a qualifying playlist
			IF SB_IS_QUALIFYING_PLAYLIST()
				IF b_DoQualifyingPlaylistWontCountFeedMessage = TRUE
					IF NOT HAS_NET_TIMER_STARTED(QualifyingMessageTimer)
						START_NET_TIMER(QualifyingMessageTimer)
					ELIF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(QualifyingMessageTimer) > 30000
						//Steve T. 1563993
						BEGIN_TEXT_COMMAND_THEFEED_POST ("SC_PLAY_FJQSN")	
						END_TEXT_COMMAND_THEFEED_POST_MPTICKER (FALSE, TRUE)
						PRINTLN("SC_PLAY_FJQSN - Displaying Qualifying Playlist feed message")
						b_DoQualifyingPlaylistWontCountFeedMessage = FALSE
					ENDIF
				ENDIF
				//and we've not done the first mission
				IF NOT HAVE_DONE_FIRST_PLAYLIST_MISSION()					
					IF b_DoneQualifyingPlaylistFeedMessage = FALSE
						IF NOT HAS_NET_TIMER_STARTED(QualifyingMessageTimer)
							START_NET_TIMER(QualifyingMessageTimer)
						ELIF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(QualifyingMessageTimer) > 30000
							//Steve T. 1563993
							BEGIN_TEXT_COMMAND_THEFEED_POST ("SC_PLAY_FJQL")	
							END_TEXT_COMMAND_THEFEED_POST_MPTICKER (FALSE, TRUE)
							PRINTLN("FMMC_PL_CONT - Displaying Qualifying Playlist feed message")
							b_DoneQualifyingPlaylistFeedMessage = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			//I love Paul
			IF DID_I_JOIN_MISSION_AS_SPECTATOR()
			OR IS_PLAYER_SCTV(PLAYER_ID())			
				IF NOT playerBD[PARTICIPANT_ID_TO_INT()].bJoinedAsSpectator
					playerBD[PARTICIPANT_ID_TO_INT()].bJoinedAsSpectator = TRUE
				ENDIF
			ENDIF
			
			IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
				IF serverBD.bServerResetFlags = FALSE
					//Host reset stuff
					PRINTLN("[PLC] CONTROL_PLAYLIST - RESET_HOST_LEADER_BOARD_VARS")
					RESET_HOST_LEADER_BOARD_VARS()
					serverBD.bServerResetFlags = TRUE
				ENDIF
			ENDIF
			
			IF serverBD.bReSetLateFlags
				IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_JoinedLaunched_Playlist)
					CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_JoinedLaunched_Playlist)
					PRINTLN("[PLC] CONTROL_PLAYLIST - clearing this flag ciFMMC_MISSION_DATA_BitSet_JoinedLaunched_Playlist")
				ENDIF
			ENDIF
			#IF IS_DEBUG_BUILD
			IF bSpam = TRUE
			PRINTLN("[PLC] CONTROL_PLAYLIST - PLAYLIST_STATE_WAIT_TILL_END_OF_MISSION")
			ENDIF
			#ENDIF  
			
			//Move on if we're spectating!
			IF IS_PLAYER_SPECTATING(PLAYER_ID())
				IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_OkToMoveOn)			
					STORE_OUT_CURRENT_PLAYLIST_TIMES_AND_SCORES()
					PRINTLN("[PLC] CONTROL_PLAYLIST - ciFMMC_MISSION_DATA_BitSet_Playlist_OkToMoveOn 1")
			        SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_OkToMoveOn)
			        FM_PLAY_LIST_SET_ME_AS_VOTED()
			        SET_FM_PLAYLIST_STATE(playerBD, PLAYLIST_STATE_WAIT_TILL_OFF_MISSION) 
				ENDIF
			ENDIF
			
			//when the vote has been cast by the server
			IF IS_BIT_SET(serverBD.iServerVoteBitSet, ciServerVoteCast)
				//the vote for next has been passed
				IF IS_BIT_SET(serverBD.iServerVoteBitSet, ciServerVoteNext)
					STORE_OUT_CURRENT_PLAYLIST_TIMES_AND_SCORES()
					PRINTLN("[PLC] CONTROL_PLAYLIST - ciFMMC_MISSION_DATA_BitSet_Playlist_OkToMoveOn 1")
			        SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_OkToMoveOn)
			        FM_PLAY_LIST_SET_ME_AS_VOTED()
			        SET_FM_PLAYLIST_STATE(playerBD, PLAYLIST_STATE_WAIT_TILL_OFF_MISSION) 
				//The vote for restart has been passed
				ELIF IS_BIT_SET(serverBD.iServerVoteBitSet, ciServerVoteRestart)
					STORE_OUT_CURRENT_PLAYLIST_TIMES_AND_SCORES()
					PRINTLN("[PLC] CONTROL_PLAYLIST - ciFMMC_MISSION_DATA_BitSet_Playlist_OkToMoveOn 2")
			        SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_OkToMoveOn)
			        FM_PLAY_LIST_SET_ME_AS_VOTED()
			        SET_FM_PLAYLIST_STATE(playerBD, PLAYLIST_STATE_WAIT_TILL_OFF_MISSION)
				ENDIF 
			#IF IS_DEBUG_BUILD
			ELIF bSpam = TRUE
			PRINTLN("[PLC] CONTROL_PLAYLIST - IF IS_BIT_SET(serverBD.iServerVoteBitSet, ciServerVoteCast) = FALSE")
			#ENDIF  
			ENDIF   
		BREAK
		
		//wait till the mission has cleaned up
		CASE PLAYLIST_STATE_WAIT_TILL_OFF_MISSION
			#IF IS_DEBUG_BUILD
			IF bSpam = TRUE
			PRINTLN("[PLC] CONTROL_PLAYLIST - PLAYLIST_STATE_WAIT_TILL_OFF_MISSION")
			ENDIF
			#ENDIF
			
			//Set that the leaderboard is being set up for the end of mission camera. 
			IF NOT IS_PLAYLIST_LEADERBOARD_BEING_SETTING_UP()
				SET_PLAYLIST_LEADERBOARD_BEING_SETTING_UP()
			ENDIF
			
			//We are not on a mission then we are ready to move on
			IF NOT IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID(), FALSE)
			AND NOT IS_FM_MISSION_LAUNCH_IN_PROGRESS()
			
				IF NOT HAVE_DONE_FIRST_PLAYLIST_MISSION()
					SET_PLAYLIST_DONE_FIRST_MISSION()
				ENDIF
				
				IF IS_PLAYER_SCTV(PLAYER_ID())
				AND GET_SCTV_MODE() = SCTV_MODE_FREE_CAM	
					SWITCH_SCTV_MODE(SCTV_MODE_FOLLOW_CAM)
					PRINTLN("[PLC] CONTROL_PLAYLIST - SWITCH_SCTV_MODE(SCTV_MODE_FOLLOW_CAM)")
				ENDIF
				SHOW_GAME_TOOL_TIPS(FALSE) 
				THEFEED_FLUSH_QUEUE()
				 g_b_ReapplyStickySaveFailedFeed = FALSE
				CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_continue_to_next)
				
				//So we can talk to every one
				CLIENT_SET_UP_EVERYONE_CHAT()
				
				// We want the radio to play
				STOP_AUDIO_SCENE("MP_JOB_CHANGE_RADIO_MUTE")
				
				IF SB_PLAYLIST_SETTING_CHALLENGE_TIME()
					playerBD[PARTICIPANT_ID_TO_INT()].iMyRankPrediction		 = g_sCHALLENGE_DIFFICULTY.iMyRankPrediction
					playerBD[PARTICIPANT_ID_TO_INT()].iTotatlUniquePlays	= g_sCHALLENGE_DIFFICULTY.iTotatlUniquePlays
					PRINTLN("[PLC] CONTROL_PLAYLIST - playerBD[PARTICIPANT_ID_TO_INT()].iMyRankPrediction		= ", playerBD[PARTICIPANT_ID_TO_INT()].iMyRankPrediction)
					PRINTLN("[PLC] CONTROL_PLAYLIST - playerBD[PARTICIPANT_ID_TO_INT()].iTotatlUniquePlays = ", playerBD[PARTICIPANT_ID_TO_INT()].iTotatlUniquePlays)
				ENDIF
				
				//Clear the wanted level
				IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
					SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
					PRINTLN("[PLC] CONTROL_PLAYLIST - SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)")
					SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
					PRINTLN("[PLC] CONTROL_PLAYLIST - SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())")
				ENDIF
				HIDE_SCRIPTED_HUD_COMPONENT_THIS_FRAME(HUD_MP_RANK_BAR)
				SET_FM_PLAYLIST_STATE(playerBD, PLAYLIST_STATE_WAIT_TILL_SERVER_READY)
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_SET_INVISIBLE | NSPC_CLEAR_TASKS | NSPC_NO_COLLISION | NSPC_FREEZE_POSITION)
				CLEANUP_MENU_ASSETS()
				CLEAR_MENU_DATA() 				
				DELETE_WAYPOINTS_FROM_THIS_PLAYER()
				PRINTLN("[PLC] CONTROL_PLAYLIST - CLEANUP_MENU_ASSETS")
				SET_PLAYLIST_BETWEEN_LEADERBOARDS()
				
				DISABLE_DPADDOWN_THIS_FRAME()
				DISABLE_CELLPHONE_THIS_FRAME_ONLY()
				DISABLE_SELECTOR_THIS_FRAME() 
				DISABLE_HUD_ELEMENTS_DURING_TRANSITION_SESSION_CHANGE()
			ENDIF
			
		BREAK
		
		//wait till the server has started the leaderboard timer
		CASE PLAYLIST_STATE_WAIT_TILL_SERVER_READY
		
			#IF IS_DEBUG_BUILD
			IF bSpam = TRUE
			PRINTLN("[PLC] CONTROL_PLAYLIST - PLAYLIST_STATE_WAIT_TILL_SERVER_READY")
			ENDIF
			#ENDIF   
			DISABLE_DPADDOWN_THIS_FRAME()
			DISABLE_CELLPHONE_THIS_FRAME_ONLY()
			DISABLE_SELECTOR_THIS_FRAME() 
			DISABLE_HUD_ELEMENTS_DURING_TRANSITION_SESSION_CHANGE()
			IF IS_PLAYER_SCTV(PLAYER_ID())	
			OR LOAD_MENU_ASSETS()
				IF HAS_NET_TIMER_STARTED(serverBD.tdLeaderboardTimer)
					iLeaderBoardTime = GET_LB_END_TIME() 
					//If the timer is more than 0
					IF iLeaderBoardTime >= 0
						PRINTLN("[PLC] CONTROL_PLAYLIST - PLAYLIST_STATE_WAIT_TILL_SERVER_READY - REMOVE_MENU_HELP_KEYS")					   
						
						//Set up the help
						IF NOT IS_PLAYER_SCTV(PLAYER_ID())	
							SET_UP_PL_LEADER_BOARD_HELP(iLeaderBoardTime)
						ENDIF
						//If we should set that we're on the end of a playlist then do so
//						IF HAS_PLAY_LIST_FINISHED()
//						AND NOT SB_PLAYLIST_DOING_CHALLENGE()
//						AND NOT SB_PLAYLIST_DOING_HEAD_TO_HEAD()
//						AND NOT SB_PLAYLIST_SETTING_CHALLENGE_TIME()
//						AND NOT SB_IS_TOURNAMENT_PLAYLIST()
//						AND NOT SB_IS_SCTV_CONTROLLED_PLAYLIST()
//						AND NOT SB_IS_LIVESTREAM_PLAYLIST()
//						AND NOT SB_IS_QUALIFYING_PLAYLIST()
//						ENDIF
						
						IF IS_SCREEN_FADED_OUT()
						OR IS_SCREEN_FADING_OUT()
							DO_SCREEN_FADE_IN(500)
						ENDIF
						NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_SET_INVISIBLE | NSPC_CLEAR_TASKS | NSPC_NO_COLLISION | NSPC_FREEZE_POSITION)
						SET_FM_PLAYLIST_STATE(playerBD, PLAYLIST_CHECK_CHALLENGE_IS_OPEN)
						SET_PLAYLIST_WAIT_FOR_LEADER_BOARD_TO_FINISH()
						
						//clear that the leaderboard is being set up
						//IF NOT HAS_PLAY_LIST_FINISHED()
						CLEAR_PLAYLIST_LEADERBOARD_BEING_SETTING_UP()
						//ENDIF
						//GRAB_PLAYER_TOTAL_SCORE_FOR_TOURNAMENT()
						#IF IS_DEBUG_BUILD
						PRINT_LEADER_BOARD_DATA()
						#ENDIF
					//Spam the time
					#IF IS_DEBUG_BUILD
					ELSE
						PRINTLN("[PLC] GET_LB_END_TIME = ", iLeaderBoardTime)
					#ENDIF
					ENDIF
				ELSE
					PRINTLN("[PLC] HAS_NET_TIMER_STARTED = FALSE")
				ENDIF
			ENDIF
		BREAK
		
		//Check that the challenge is still open
		CASE PLAYLIST_CHECK_CHALLENGE_IS_OPEN
		
			//Hide stuff
			DISABLE_HUD_ELEMENTS_DURING_TRANSITION_SESSION_CHANGE()			
			
			
			bRebuildHelp = FALSE
			#IF IS_DEBUG_BUILD
			IF bSpam = TRUE
			PRINTLN("[PLC] CONTROL_PLAYLIST - PLAYLIST_CHECK_CHALLENGE_IS_OPEN")
			ENDIF
			#ENDIF  
			//Not doing a challenge
			IF SB_PLAYLIST_DOING_CHALLENGE()
				//get the content
				IF GET_UGC_BY_CONTENT_ID(sGetUGC_content, serverBD.tl23PlayListToPlay, UGC_TYPE_GTA5_CHALLENGE, DEFAULT, DEFAULT, DEFAULT, TRUE)
					//it is still open, go back to the start
					IF g_sCurrentPlayListDetails.bChallengeIsStillOpen = TRUE
						PRINTLN("[PLC] g_sCurrentPlayListDetails.bChallengeIsStillOpen = TRUE")
						lbdVars.bPlayerRowGrabbed = FALSE
						SET_FM_PLAYLIST_STATE(playerBD, PLAYLIST_STATE_DISPLAY_LEADERBOARD)
					ELSE
						PRINTLN("[PLC] g_sCurrentPlayListDetails.bChallengeIsStillOpen = FALSE")
						//Close, go to the end leader board
						SET_FM_PLAYLIST_PLAYER_GAME_STATE(playerBD, GAME_STATE_END_LEADER_BOARD)
					ENDIF
					//Check that a challenge content ID is ok
					#IF IS_DEBUG_BUILD
					CHECK_CHALLENGE_CONTENT_ID()
					#ENDIF
				ENDIF
			ELSE
				IF NOT HAS_PLAY_LIST_FINISHED()	 
					lbdVars.bPlayerRowGrabbed = FALSE
					SET_FM_PLAYLIST_STATE(playerBD, PLAYLIST_STATE_DISPLAY_LEADERBOARD)
					
					IF SHOULD_DO_TRANSITION_MENU_AUDIO()
						RUN_TRANSITION_MENU_AUDIO(TRUE)
					ENDIF
				ELSE
					lbdVars.bPlayerRowGrabbed = FALSE
					SET_FM_PLAYLIST_STATE(playerBD, PLAYLIST_STATE_DISPLAY_LEADERBOARD)
				ENDIF
			ENDIF
		BREAK
		
		//display the leaderboard then go back to the start when the timer expiires. 
		CASE PLAYLIST_STATE_DISPLAY_LEADERBOARD
		
			//Hide stuff
			DISABLE_HUD_ELEMENTS_DURING_TRANSITION_SESSION_CHANGE()		
			
			CLEAR_PLAYLIST_BETWEEN_LEADERBOARDS()
			
			IF SHOULD_WE_WARP_TO_START_OF_NEXT_MISSION()
				START_FM_MISSION_PLAYLIST()
			ENDIF
			
			CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_JoinedLaunched_Playlist)
			#IF IS_DEBUG_BUILD
			IF bSpam = TRUE
			PRINTLN("[PLC] CONTROL_PLAYLIST - PLAYLIST_STATE_DISPLAY_LEADERBOARD")
			ENDIF
			#ENDIF
			
			iLeaderBoardTime = GET_LB_END_TIME() 
			IF NOT IS_PLAYER_SCTV(PLAYER_ID())	
				IF LOAD_MENU_ASSETS()
					IF bRebuildHelp
						bRebuildHelp = FALSE
						SET_UP_PL_LEADER_BOARD_HELP(iLeaderBoardTime)
					ENDIF
					MAINTAIN_LEADER_BOARD_COUNT_DOWN_TIMER(iLeaderBoardTime)
				ELSE
					bRebuildHelp = TRUE
				ENDIF
			ENDIF
			DISABLE_DPADDOWN_THIS_FRAME()
			DISABLE_CELLPHONE_THIS_FRAME_ONLY()
			DISABLE_SELECTOR_THIS_FRAME() 
			
			GET_LEADERBOARD_RANKS()
			
			//Disable the front end this frame
			//IF GET_CORONA_STATUS() = CORONA_STATUS_IDLE
			//  DISABLE_FRONTEND_THIS_FRAME()
			//ENDIF
			
			IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, ciPLAYER_BS_PROGRESS_PASSED_LB)
				IF HAS_MISSION_END_CONTINUE_BEEN_PRESSED()
					//If a player is SCTV and they say ready then send an event to say that that they are!
					IF IS_PLAYER_SCTV(PLAYER_ID())						
						BROADCAST_GENERAL_EVENT(GENERAL_EVENT_PLAYLIST_SCTV_GO, ALL_PLAYERS())
					ENDIF
					SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, ciPLAYER_BS_PROGRESS_PASSED_LB)
					REMOVE_MENU_HELP_KEYS()
					//Add a spinner
					IF NOT SB_IS_TOURNAMENT_PLAYLIST()
					AND NOT SB_IS_SCTV_CONTROLLED_PLAYLIST()
						ADD_MENU_HELP_KEY("", "END_LBD_CONTN", iLeaderBoardTime)
					ENDIF
					SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_continue_to_next)
					PLAY_SOUND_FRONTEND(-1,"CONTINUE", "HUD_FRONTEND_DEFAULT_SOUNDSET")
					PRINTLN("[PLC] CONTROL_PLAYLIST - Pressed Continue")					
				ENDIF
			ENDIF
			
			//Disable idle kick when on the LB
			SET_IDLE_KICK_DISABLED_THIS_FRAME()

			IF SHOULD_PLAYLIST_LEADER_BOARD_PROGRESS()			
				PRINTLN("[PLC] ci_FINISH_LB, set so moving to start next mission: ")
				SET_PLAYLIST_LEADERBOARD_ACTIVE(FALSE)
				CLIENT_CLEAR_LB_DATA_START_OF_MISSION(sPLLB_Vars)
				FM_PLAY_LIST_CLEAN_VARS()
				CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, ciPLAYER_BS_PROGRESS_PASSED_LB)
				SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, ciPLAYER_BS_PASSED_LB)
				PRINTLN("[PLC] SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, ciPLAYER_BS_PASSED_LB)")
				CLEAR_PLAYLIST_WAIT_FOR_LEADER_BOARD_TO_FINISH()
				playerBD[PARTICIPANT_ID_TO_INT()].bJoinedAsSpectator = FALSE
				//Hide the hud
				IF IS_PLAYER_SCTV(PLAYER_ID())
					HIDE_SPECTATOR_HUD(TRUE)
				ENDIF
				SET_FM_PLAYLIST_STATE(playerBD, PLAYLIST_STATE_WAIT_TO_START_MISSION)
				RUN_TRANSITION_MENU_AUDIO(FALSE)
				SET_ON_LBD_GLOBAL(FALSE)
			ELSE
				#IF IS_DEBUG_BUILD
				IF bSpam = TRUE
				PRINTLN("[PLC] CONTROL_PLAYLIST - IS_BIT_SET(serverBD.iLBBitset,ci_FINISH_LB) = FALSE")
				ENDIF
				#ENDIF
				
				//CHECK_IS_GAMER_TAG_IS_ACTIVATED(playlistEOMvars.bShowPlayerCard, playlistEOMvars.bPlayercardTriggered )
				
				IF NOT playlistEOMvars.bShowPlayerCard
					//Only display the playlist leaderboard when not setting a challenge time
					IF NOT SB_PLAYLIST_SETTING_CHALLENGE_TIME()
						DISPLAY_LEADERBOARD()
						PROCESS_LBD_SCROLL_LOGIC(lbdVars, timeScrollDelay, iLBPlayerSelection, 0, 0, serverBD.iTotalNumOfLBPlayers, serverBD.iNumPlaylistTeams, FALSE)//, TRUE)
						TRACK_LBD_ROW_FOR_SCROLL(lbdVars, ServerBD.iNumPlaylistTeams, serverBD.iTotalNumOfLBPlayers)
					ENDIF
					#IF IS_DEBUG_BUILD
					IF bSpam = TRUE
					//PRINTLN("[PLC] DISPLAY_LEADERBOARD")
					PRINTLN("[PLC] CONTROL_PLAYLIST - iTotalNumOfLBPlayers = ", serverBD.iTotalNumOfLBPlayers)
					PRINTLN("[PLC] CONTROL_PLAYLIST - iNumPlaylistTeams    = ", serverBD.iNumPlaylistTeams)
					PRINTLN("[PLC] CONTROL_PLAYLIST - iLBPlayerSelection    = ", iLBPlayerSelection)
					ENDIF
					#ENDIF
					//are we at the end of a playlist add a restart option
					IF HAS_PLAY_LIST_FINISHED()
					AND NOT SB_PLAYLIST_DOING_CHALLENGE()
					AND NOT SB_PLAYLIST_DOING_HEAD_TO_HEAD()
					AND NOT SB_PLAYLIST_SETTING_CHALLENGE_TIME()
					AND NOT SB_IS_TOURNAMENT_PLAYLIST()
					AND NOT SB_IS_SCTV_CONTROLLED_PLAYLIST()
					AND NOT SB_IS_QUALIFYING_PLAYLIST()
					AND NOT SB_IS_LIVESTREAM_PLAYLIST()
						//Set that we are at the end of the playlist
						IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_AtEndOfPlayList)
							PRINTLN("[PLC] SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_AtEndOfPlayList)")
							SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_AtEndOfPlayList)	
						ENDIF
						IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_VotedAtEndOfPlaylist)
							PRINTLN("[PLC] IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_VotedAtEndOfPlaylist) = FALSE")
							//If we press restart
							IF HAS_MISSION_END_RESTART_BUTTON_BEEN_PRESSED()
								PRINTLN("[PLC] HAS_MISSION_END_RESTART_BUTTON_BEEN_PRESSED - SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_WantToRestartPlaylist)")
								SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_WantToRestartPlaylist)
								SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_VotedAtEndOfPlaylist)
								PRINTLN("[PLC] SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_VotedAtEndOfPlaylist)")
								PRINTLN("[PLC] SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_WantToRestartPlaylist)")
								//remove the old help
								REMOVE_MENU_HELP_KEYS()
								//Add a spinner
								IF NOT SB_IS_TOURNAMENT_PLAYLIST()
								AND NOT SB_IS_SCTV_CONTROLLED_PLAYLIST()
									ADD_MENU_HELP_KEY("", "END_LBD_CONTN", iLeaderBoardTime)
								ENDIF
								PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET")
//								iLBPlayerSelection = -1
							ENDIF
							//If we press continue
							IF HAS_MISSION_END_CONTINUE_BEEN_PRESSED()
								PRINTLN("[PLC] HAS_MISSION_END_CONTINUE_BEEN_PRESSED - SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_WantToQuitPlaylist)")
//								iLBPlayerSelection = -1
								SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_WantToQuitPlaylist)
								SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_VotedAtEndOfPlaylist)
								PRINTLN("[PLC] SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_VotedAtEndOfPlaylist)")
								PRINTLN("[PLC] SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_WantToQuitPlaylist)")
								//remove the old help
								REMOVE_MENU_HELP_KEYS()
								//Add a spinner
								IF NOT SB_IS_TOURNAMENT_PLAYLIST()
								AND NOT SB_IS_SCTV_CONTROLLED_PLAYLIST()
									ADD_MENU_HELP_KEY("", "END_LBD_CONTN", iLeaderBoardTime)
								ENDIF
								PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
			ENDIF
		BREAK
		
	ENDSWITCH
	
ENDPROC

//Is everyone ready
PROC SERVER_HANDLE_CLIENT_READY_CHECK()
	IF NOT ARE_ALL_PLAYERS_READY()
		BOOL bAllPlayersReady = TRUE
		PARTICIPANT_INDEX tempPart
		PLAYER_INDEX playerId
		INT iParticipant
		REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant  
			tempPart = INT_TO_PARTICIPANTINDEX(iParticipant)
			IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
				playerId = NETWORK_GET_PLAYER_INDEX(tempPart)
				IF NOT IS_PLAYER_SCTV(playerId)
					IF playerBD[iParticipant].iGameState < GAME_STATE_PLAYLIST_RESTART
						bAllPlayersReady = FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		IF bAllPlayersReady
			SET_ALL_PLAYERS_READY()
		ENDIF
	ENDIF
ENDPROC


//Is everyone ready
FUNC INT GET_CHALLENGE_CASH_PRIZE()
	PARTICIPANT_INDEX tempPart
	PLAYER_INDEX playerId
	INT iParticipant, iPlayerCount
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant  
		tempPart = INT_TO_PARTICIPANTINDEX(iParticipant)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
			playerId = NETWORK_GET_PLAYER_INDEX(tempPart)
			IF NOT DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(playerId)
			AND NOT playerBD[iParticipant].bJoinedAsSpectator
			AND NOT IS_PLAYER_SCTV(playerId)
				iPlayerCount++
			ENDIF
		ENDIF
	ENDREPEAT	
	RETURN (serverBD.iCashBet/iPlayerCount + g_sCurrentPlayListDetails.iInitialCashWager)
ENDFUNC


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////				   MAIN SERVER LOOP				  //////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
PROC SERVER_HANDLE_CLIENT_VOTING()

	INT iParticipant
	INT iLoop
	PARTICIPANT_INDEX tempPart
	PLAYER_INDEX playerId
	INT iPlayerSlot 
	
	sPlayerCounts.iNumberOnPlayList				 	= 0
	sPlayerCounts.iNumberAtEndOfMissionCount		= 0
	sPlayerCounts.iNumberVoted					  	= 0
	sPlayerCounts.iNumberRestart					= 0
	sPlayerCounts.iNumberNext					   	= 0
	sPlayerCounts.iNumberVotedMenu				  	= 0
		
	sPlayerCounts.iNumberAtEndOfPlaylistCount	   	= 0
	sPlayerCounts.iNumberWantToRestartPlaylist	  	= 0
	sPlayerCounts.iNumberWantToQuitPlaylist		 	= 0
	sPlayerCounts.iNumberWantToProgressPastLB	   	= 0
	
	sPlayerCounts.iNumberNotPlaying				 	= 0
	sPlayerCounts.iNumberAtEndOfMissionNotPlaying   = 0
	
	serverBD.iVotedBitSet						   	= 0
	serverBD.iTotalNumOfLBPlayers				   	= 0
	
	BOOL resetcoronatimer = TRUE
	
	#IF IS_DEBUG_BUILD
	BOOL bSpam = FALSE  
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_NUMPAD1, KEYBOARD_MODIFIER_NONE, "")
	OR GET_COMMANDLINE_PARAM_EXISTS("sc_PlcSpam")
		bSpam = TRUE
		PRINTLN("[PLC] SERVER_HANDLE_CLIENT_VOTING")
	ENDIF
	#ENDIF
	
	BOOL bSB_TOURNAMENT_PLY 	= SB_IS_TOURNAMENT_PLAYLIST()
	BOOL bSB_PLAYLIST_DOING_H2H = SB_PLAYLIST_DOING_HEAD_TO_HEAD()
	BOOL bPLAYLIST_FINISHED		= HAS_PLAY_LIST_FINISHED()
	
	IF bSB_PLAYLIST_DOING_H2H
		serverBD.iNumPlaylistTeams = MAX_HEAD_TO_HEAD_CREWS  		  
	ENDIF
	
	serverBD.bRunLeaderBoard = TRUE
	serverBD.bCloseLeaderBoard = TRUE
	BOOL bLookFromCrew
	INT iPlayerCrew
	
	IF bSB_PLAYLIST_DOING_H2H
		IF serverBD.iTeamCrew[1] = -1
			bLookFromCrew = TRUE
		ENDIF
	ENDIF
	
	BOOL bAllPlayersReady = TRUE
	BOOL bHoldForRounds = FALSE
	BOOl bFound = FALSE
	
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant  
//		IF serverBD_LB.sleaderboard[iParticipant].bInitialise
		IF IS_BIT_SET(serverBD_LB.sleaderboard[iParticipant].iLbdBitSet, ciLBD_BIT_INITIALISE)
			serverBD.iTotalNumOfLBPlayers++
		ENDIF
		tempPart = INT_TO_PARTICIPANTINDEX(iParticipant)
		bFound = FALSE
		IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
			//If the player has asked to be cleaned then please let them
			//But only if not a tournamnet
			IF NOT bSB_TOURNAMENT_PLY
				IF IS_BIT_SET(playerBD[iParticipant].iPlayerBitSet, ciPLAYER_CLEAR_MY_LB_DATA_PLEASE)
					IF NOT IS_BIT_SET(serverBD.iServerClearPlayerBitSet, iParticipant)
						REPEAT NUM_NETWORK_PLAYERS iLoop
							IF bFound = FALSE
							AND serverBD_LB.sleaderboard[iLoop].iParticipant = iParticipant
								serverBD_LB.sleaderboard[iLoop].iPlayerScore = 0
								serverBD_LB.sleaderboard[iLoop].iPreviousPlaylistScore[0] = 0
								serverBD_LB.sleaderboard[iLoop].iPreviousPlaylistScore[1] = 0
								serverBD_LB.sleaderboard[iLoop].iPreviousPlaylistScore[2] = 0
								serverBD_LB.sleaderboard[iLoop].iCurrentPlaylistScore = 0
								PRINTLN("[PLC] SERVER_HANDLE_CLIENT_VOTING, ciPLAYER_CLEAR_MY_LB_DATA_PLEASE, SET_BIT(serverBD.iServerClearPlayerBitSet, ", iParticipant, ")")
								bFound = TRUE
							ENDIF
						ENDREPEAT
						SET_BIT(serverBD.iServerClearPlayerBitSet, iParticipant)
						PRINTLN("[PLC] SERVER_HANDLE_CLIENT_VOTING, ciPLAYER_CLEAR_MY_LB_DATA_PLEASE, DONE ")
					ENDIF
				ELSE
					CLEAR_BIT(serverBD.iServerClearPlayerBitSet, iParticipant)
				ENDIF
			ENDIF
			
			playerId = NETWORK_GET_PLAYER_INDEX(tempPart)
			iPlayerSlot = NATIVE_TO_INT(playerId)
			IF playerBD[iParticipant].iPlayListState = PLAYLIST_STATE_WAIT_TILL_END_OF_MISSION
			OR playerBD[iParticipant].iPlayListState = PLAYLIST_STATE_WAIT_TILL_OFF_MISSION
			OR playerBD[iParticipant].iPlayListState = PLAYLIST_STATE_WAIT_TILL_SERVER_READY
			OR playerBD[iParticipant].iPlayListState = PLAYLIST_STATE_DISPLAY_LEADERBOARD
			OR playerBD[iParticipant].iPlayListState = PLAYLIST_CHECK_CHALLENGE_IS_OPEN
				resetcoronatimer = FALSE
			ENDIF
			
			IF playerBD[iParticipant].iPlayListState = PLAYLIST_STATE_WAIT_TILL_OFF_ROUNDS_MISSION
				bHoldForRounds = TRUE
			ENDIF
			IF NOT IS_PLAYER_SCTV(playerId)
				//If a player is not ready for us then we dont run the leaderboard
				IF playerBD[iParticipant].iPlayListState < PLAYLIST_STATE_WAIT_TILL_SERVER_READY
					IF NOT IS_BIT_SET(GlobalplayerBD_FM[iPlayerSlot].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_JIPED_ON_LB)
					AND NOT IS_BIT_SET(playerBD[iParticipant].iPlayerBitSet, ciPLAYER_BS_PASSED_LB)					
						serverBD.bRunLeaderBoard = FALSE
						PRINTLN("[PLC] serverBD.bRunLeaderBoard = FALSE - player ", GET_PLAYER_NAME(playerId), " is not yet ready!")
					ENDIF
				ENDIF
				//If a player is not ready to close the leaderboard then we need to be ready for it
				IF playerBD[iParticipant].iPlayListState < PLAYLIST_STATE_DISPLAY_LEADERBOARD
					IF NOT IS_BIT_SET(GlobalplayerBD_FM[iPlayerSlot].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_JIPED_ON_LB)
					//But only if they JIPed
					AND IS_BIT_SET(GlobalplayerBD_FM[iPlayerSlot].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_JoinedLaunched_Playlist)
						serverBD.bCloseLeaderBoard = FALSE
						PRINTLN("[PLC] serverBD.bCloseLeaderBoard = FALSE - player ", GET_PLAYER_NAME(playerId), " is not yet ready!")
					ENDIF
				ENDIF
				IF playerBD[iParticipant].iGameState < GAME_STATE_PLAYLIST_RESTART
					bAllPlayersReady = FALSE
				ENDIF
				//If we need to look for a second crew
				IF bLookFromCrew
					iPlayerCrew = GET_PLAYER_CREW_INT_CL(playerId)
					IF iPlayerCrew  != serverBD.iTeamCrew[0]
						serverBD.iTeamCrew[1] = iPlayerCrew  
						PRINTLN("[PLC] serverBD.iTeamCrew[1]  = ", serverBD.iTeamCrew[1])
						bLookFromCrew = FALSE
					ENDIF
				ENDIF
				IF NOT IS_BIT_SET(GlobalplayerBD_FM[iPlayerSlot].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_JoinedLaunched_Playlist)
				AND NOT IS_BIT_SET(GlobalplayerBD_FM[iPlayerSlot].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_JIPED_ON_LB)
					sPlayerCounts.iNumberOnPlayList++   
					IF IS_BIT_SET(GlobalplayerBD_FM[iPlayerSlot].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_PlaylistAtEndOfMission)
						sPlayerCounts.iNumberAtEndOfMissionCount++
						IF IS_BIT_SET(GlobalplayerBD_FM[iPlayerSlot].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_Voted)
							sPlayerCounts.iNumberVoted++
							IF IS_BIT_SET(GlobalplayerBD_FM[iPlayerSlot].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_WantToRestart)
								sPlayerCounts.iNumberRestart++
							ELIF IS_BIT_SET(GlobalplayerBD_FM[iPlayerSlot].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_WantNext)
								sPlayerCounts.iNumberNext++
							ENDIF
						ENDIF
					ENDIF
					IF IS_BIT_SET(GlobalplayerBD_FM[iPlayerSlot].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_Voted_On_Menu)
						sPlayerCounts.iNumberVotedMenu++
						SET_BIT(serverBD.iVotedBitSet, iParticipant)
					ENDIF
					//If we are at the end of a playlist
					IF bPLAYLIST_FINISHED
						//I f they've voted at the end
						IF IS_BIT_SET(GlobalplayerBD_FM[iPlayerSlot].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_AtEndOfPlayList)
							PRINTLN("[PLC] sPlayerCounts.iNumberAtEndOfPlaylistCount ", GET_PLAYER_NAME(playerId), " is ready")
							sPlayerCounts.iNumberAtEndOfPlaylistCount++
							//if they want to restart
							IF IS_BIT_SET(GlobalplayerBD_FM[iPlayerSlot].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_WantToRestartPlaylist)
								sPlayerCounts.iNumberWantToRestartPlaylist++
								PRINTLN("[PLC] sPlayerCounts.iNumberWantToRestartPlaylist ", GET_PLAYER_NAME(playerId), " Wants to restart")
							//If they want to quit
							ELIF IS_BIT_SET(GlobalplayerBD_FM[iPlayerSlot].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_WantToQuitPlaylist)
								sPlayerCounts.iNumberWantToQuitPlaylist++
								PRINTLN("[PLC] sPlayerCounts.iNumberWantToRestartPlaylist ", GET_PLAYER_NAME(playerId), " Wants to quit")
							ENDIF
						ENDIF
					ENDIF
				ELSE
					sPlayerCounts.iNumberNotPlaying++   
					IF IS_BIT_SET(GlobalplayerBD_FM[iPlayerSlot].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_PlaylistAtEndOfMission)
						sPlayerCounts.iNumberAtEndOfMissionNotPlaying++
					ENDIF
				ENDIF
				IF IS_BIT_SET(playerBD[iParticipant].iPlayerBitSet, ciPLAYER_BS_PROGRESS_PASSED_LB)
					sPlayerCounts.iNumberWantToProgressPastLB++
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	serverBD.iNumberVotedMenu = sPlayerCounts.iNumberVotedMenu 
	
	IF bAllPlayersReady
		//IF NOT ARE_ALL_PLAYERS_READY()
			SET_ALL_PLAYERS_READY()
	//	ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF bSpam = TRUE
		PRINTLN("[PLC] sPlayerCounts.iNumberOnPlayList		   = ", sPlayerCounts.iNumberOnPlayList)
		PRINTLN("[PLC] sPlayerCounts.iNumberAtEndOfMissionCount  = ", sPlayerCounts.iNumberAtEndOfMissionCount)
		PRINTLN("[PLC] sPlayerCounts.iNumberVoted				= ", sPlayerCounts.iNumberVoted)
		PRINTLN("[PLC] sPlayerCounts.iNumberRestart			  = ", sPlayerCounts.iNumberRestart)
		PRINTLN("[PLC] sPlayerCounts.iNumberNext				 = ", sPlayerCounts.iNumberNext)
		PRINTLN("[PLC] sPlayerCounts.iNumberVotedMenu			= ", sPlayerCounts.iNumberVotedMenu)
		PRINTLN("[PLC] sPlayerCounts.iNumberWantToQuitPlaylist		   = ", sPlayerCounts.iNumberWantToQuitPlaylist)
		PRINTLN("[PLC] sPlayerCounts.iNumberWantToRestartPlaylist		   = ", sPlayerCounts.iNumberWantToRestartPlaylist)
		PRINTLN("[PLC] sPlayerCounts.iNumberAtEndOfPlaylistCount		   = ", sPlayerCounts.iNumberAtEndOfPlaylistCount)
		PRINTLN("[PLC] sPlayerCounts.iNumberNotPlaying		   = ", sPlayerCounts.iNumberNotPlaying)
		PRINTLN("[PLC] sPlayerCounts.iNumberAtEndOfMissionNotPlaying			= ", sPlayerCounts.iNumberAtEndOfMissionNotPlaying)
		PRINTLN("[PLC] serverBD.iVotedBitSet					 = ", serverBD.iVotedBitSet)
		PRINTLN("[PLC] serverBD.iTotalNumOfLBPlayers			 = ", serverBD.iTotalNumOfLBPlayers)
		REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant  
			IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
				playerId = NETWORK_GET_PLAYER_INDEX(tempPart)
				PRINTLN("[PLC] iParticipant = ", iParticipant, " GET_PLAYER_NAME(", GET_PLAYER_NAME(playerId), ")")
				IF IS_BIT_SET(GlobalplayerBD_FM[iPlayerSlot].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_PlaylistAtEndOfMission)
					PRINTLN("[PLC] iParticipant = ", iParticipant, " GET_PLAYER_NAME(", GET_PLAYER_NAME(playerId), ") - AtEndOfMission")
				ENDIF
				IF IS_BIT_SET(GlobalplayerBD_FM[iPlayerSlot].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_WantToRestart)
					PRINTLN("[PLC] iParticipant = ", iParticipant, " GET_PLAYER_NAME(", GET_PLAYER_NAME(playerId), ") - WantToRestart")
				ENDIF
				IF IS_BIT_SET(GlobalplayerBD_FM[iPlayerSlot].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_WantNext)
					PRINTLN("[PLC] iParticipant = ", iParticipant, " GET_PLAYER_NAME(", GET_PLAYER_NAME(playerId), ") - WantNext")
				ENDIF
				IF IS_BIT_SET(GlobalplayerBD_FM[iPlayerSlot].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_WantToRestartPlaylist)
					PRINTLN("[PLC] iParticipant = ", iParticipant, " GET_PLAYER_NAME(", GET_PLAYER_NAME(playerId), ") - WantToRestart PL")
				ENDIF
				IF IS_BIT_SET(GlobalplayerBD_FM[iPlayerSlot].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_WantToQuitPlaylist)
					PRINTLN("[PLC] iParticipant = ", iParticipant, " GET_PLAYER_NAME(", GET_PLAYER_NAME(playerId), ") - Want Quit")
				ENDIF
				PRINTLN("[PLC] iParticipant = ", iParticipant, " GET_PLAYER_NAME(", GET_PLAYER_NAME(playerId), ") - iPlayListBitSet = ", GlobalplayerBD_FM[iPlayerSlot].sPlaylistVars.iPlayListBitSet)
			ENDIF
		ENDREPEAT
	ENDIF
	#ENDIF
	
	//IF HAS_PLAY_LIST_FINISHED()
	//AND SET_BIT(serverBD.iServerVoteBitSet, ciServerVoteNext)
	//ELSE
	
	IF serverbd.iNumberOfMissionsPlayed <= serverBD.iLength
		//If enough people have voted
		IF (TO_FLOAT(sPlayerCounts.iNumberWantToProgressPastLB)/TO_FLOAT(sPlayerCounts.iNumberOnPlayList ))>= cfVOTE_PASS_RATIO
		//And we've passed the 15s min timer
		AND GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.tdLeaderboardTimer) > GET_PLAYLIST_LEADERBOARD_TIMEOUT(TRUE)				
		//And mroe than one has voted or 
		AND (sPlayerCounts.iNumberWantToProgressPastLB > 1  OR  sPlayerCounts.iNumberOnPlayList = 1)
			IF serverBD.bProgressPastLB = FALSE
				serverBD.bProgressPastLB = TRUE
				PRINTLN("[PLC] Setting serverBD.bProgressPastLB = TRUE")
			ENDIF
		ELSE
			IF serverBD.bProgressPastLB
				serverBD.bProgressPastLB = FALSE
				PRINTLN("[PLC] Setting serverBD.bProgressPastLB = FALSE")   
			ENDIF
			#IF IS_DEBUG_BUILD
				IF bSpam = TRUE
					PRINTLN("[PLC] sPlayerCounts.iNumberOnPlayList           = ", sPlayerCounts.iNumberOnPlayList)   
					PRINTLN("[PLC] sPlayerCounts.iNumberWantToProgressPastLB = ", sPlayerCounts.iNumberWantToProgressPastLB)   
					PRINTLN("[PLC] sPlayerCounts.tdLeaderboardTimer          = ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.tdLeaderboardTimer) > GET_PLAYLIST_LEADERBOARD_TIMEOUT(TRUE))   
				ENDIF
			#ENDIF
		ENDIF
	ENDIF
	
	IF serverbd.iNumberOfMissionsPlayed <= serverBD.iLength
		IF NOT IS_BIT_SET(serverBD.iServerVoteBitSet, ciServerVoteCast)
			IF serverBD.bReSetLateFlags = FALSE
				IF sPlayerCounts.iNumberOnPlayList = 0
				AND sPlayerCounts.iNumberNotPlaying > 0 
					PRINTLN("[PLC] iNumberOnPlayList = 0,  sPlayerCounts.iNumberNotPlaying = ",  sPlayerCounts.iNumberNotPlaying)   
					IF sPlayerCounts.iNumberNotPlaying = sPlayerCounts.iNumberAtEndOfMissionNotPlaying
						serverBD.bReSetLateFlags = TRUE
						PRINTLN("[PLC] serverBD.bReSetLateFlags = TRUE")
					ENDIF
				ENDIF
			ENDIF
						
			IF sPlayerCounts.iNumberOnPlayList = sPlayerCounts.iNumberAtEndOfMissionCount
			AND NOT bHoldForRounds			
				IF (TO_FLOAT(sPlayerCounts.iNumberVoted)/TO_FLOAT(sPlayerCounts.iNumberOnPlayList ))>= cfVOTE_PASS_RATIO
				AND (sPlayerCounts.iNumberVoted > 1  OR  sPlayerCounts.iNumberOnPlayList = 1)
					IF (TO_FLOAT(sPlayerCounts.iNumberNext)/TO_FLOAT(sPlayerCounts.iNumberOnPlayList ))>= cfVOTE_PASS_RATIO
						PRINTLN("[PLC] sPlayerCounts.iNumberVoted	   = ", sPlayerCounts.iNumberVoted )
						PRINTLN("[PLC] sPlayerCounts.iNumberOnPlayList  = ", sPlayerCounts.iNumberOnPlayList )
						PRINTLN("[PLC] sPlayerCounts.iNumberNext		= ", sPlayerCounts.iNumberNext )
						PRINTLN("[PLC] sPlayerCounts.iNumberRestart	 = ", sPlayerCounts.iNumberRestart )
						PRINTLN("[PLC] SET_BIT(serverBD.iServerVoteBitSet, ciServerVoteNext)")
						PRINTLN("[PLC] SET_BIT(serverBD.iServerVoteBitSet, ciServerVoteCast)")
						SET_BIT(serverBD.iServerVoteBitSet, ciServerVoteNext)
						SET_BIT(serverBD.iServerVoteBitSet, ciServerVoteCast)
						SET_BIT(serverBD.iMissionLbDone, serverBD.iCurrentPlayListPosition)
						IF SB_PLAYLIST_SHUFFELED()
							serverBD.iCurrentPlayListPosition = GET_RANDOM_NEXT_IN_PLAYLIST()
						ELSE
							serverBD.iCurrentPlayListPosition++
						ENDIF
						serverBD.iNumberOfMissionsPlayed++
						SET_MISSION_AS_PLAYED_IN_PLAYLIST(serverBD.iCurrentPlayListPosition)
						
					ELIF (TO_FLOAT(sPlayerCounts.iNumberRestart)/TO_FLOAT(sPlayerCounts.iNumberOnPlayList ))>= cfVOTE_PASS_RATIO
						PRINTLN("[PLC] sPlayerCounts.iNumberVoted	   = ", sPlayerCounts.iNumberVoted )
						PRINTLN("[PLC] sPlayerCounts.iNumberOnPlayList  = ", sPlayerCounts.iNumberOnPlayList )
						PRINTLN("[PLC] sPlayerCounts.iNumberNext		= ", sPlayerCounts.iNumberNext )
						PRINTLN("[PLC] sPlayerCounts.iNumberRestart	 = ", sPlayerCounts.iNumberRestart )
						PRINTLN("[PLC] SET_BIT(serverBD.iServerVoteBitSet, ciServerVoteRestart)")
						PRINTLN("[PLC] SET_BIT(serverBD.iServerVoteBitSet, ciServerVoteCast)")
						SET_BIT(serverBD.iServerVoteBitSet, ciServerVoteRestart)
						SET_BIT(serverBD.iServerVoteBitSet, ciServerVoteCast)
					ENDIF
				ELSE
					PRINTLN("[PLC] sPlayerCounts.iNumberVoted: ", sPlayerCounts.iNumberVoted)
					PRINTLN("[PLC] sPlayerCounts.iNumberOnPlayList: ", sPlayerCounts.iNumberOnPlayList)
				ENDIF
			ELSE
				PRINTLN("[PLC] bHoldForRounds: ", bHoldForRounds)
				PRINTLN("[PLC] sPlayerCounts.iNumberOnPlayList: ", sPlayerCounts.iNumberOnPlayList)
				PRINTLN("[PLC] sPlayerCounts.iNumberAtEndOfMissionCount: ", sPlayerCounts.iNumberAtEndOfMissionCount)
			ENDIF
		ENDIF
	ELSE
		PRINTLN("[PLC] serverbd.iNumberOfMissionsPlayed > serverBD.iLength, (serverBD.iLength = ", serverBD.iLength, ") (serverbd.iNumberOfMissionsPlayed = ", serverbd.iNumberOfMissionsPlayed, ")" )
	ENDIF
	
	//if the playlist has finished
	IF bPLAYLIST_FINISHED
		PRINTLN("[PLC] HAS_PLAY_LIST_FINISHED = TRUE" )
		IF serverBD.bRestartPlayList = FALSE
		AND serverBD.bQuitPlayList = FALSE
			//and the number at the end of the playlist is equat to the number at on the playlist
			IF sPlayerCounts.iNumberOnPlayList = sPlayerCounts.iNumberAtEndOfPlaylistCount
				PRINTLN("[PLC] sPlayerCounts.iNumberOnPlayList = sPlayerCounts.iNumberAtEndOfPlaylistCount" )
				//and the number voted to restart is more than %50
				IF (TO_FLOAT(sPlayerCounts.iNumberWantToRestartPlaylist)/TO_FLOAT(sPlayerCounts.iNumberOnPlayList ))>= cfVOTE_PASS_RATIO
				AND (sPlayerCounts.iNumberWantToRestartPlaylist > 1  OR  sPlayerCounts.iNumberOnPlayList = 1)
					//set the flag to say that the playlist will restart
					serverBD.bRestartPlayList = TRUE
					PRINTLN("[PLC] serverBD.bRestartPlayList = TRUE")
				
				ELIF (TO_FLOAT(sPlayerCounts.iNumberWantToQuitPlaylist)/TO_FLOAT(sPlayerCounts.iNumberOnPlayList ))>= cfVOTE_PASS_RATIO
				AND (sPlayerCounts.iNumberWantToQuitPlaylist > 1  OR  sPlayerCounts.iNumberOnPlayList = 1)
					serverBD.bQuitPlayList = TRUE
					PRINTLN("[PLC] serverBD.bQuitPlayList = TRUE")
				
				ENDIF
				IF sPlayerCounts.iNumberWantToRestartPlaylist > sPlayerCounts.iNumberWantToQuitPlaylist
					PRINTLN("sPlayerCounts.iNumberWantToRestartPlaylist > sPlayerCounts.iNumberWantToQuitPlaylist")
					serverBD.bQuitPlayList = FALSE
					serverBD.bRestartPlayList = TRUE				
				ENDIF
			ENDIF
		ENDIF
		//Calculate the cash bet per player
		IF serverBD.iCashBetPlayer = 0
		AND serverBD.iCashBet != 0
			INT iNumberOnPlayList
			REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant  
				tempPart = INT_TO_PARTICIPANTINDEX(iParticipant)
				IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
					playerId = NETWORK_GET_PLAYER_INDEX(tempPart)
					iPlayerSlot = NATIVE_TO_INT(playerId)
					IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)	
						IF NOT IS_PLAYER_SCTV(playerId)
							IF NOT IS_BIT_SET(GlobalplayerBD_FM[iPlayerSlot].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_JoinedLaunched_Playlist)
							AND NOT IS_BIT_SET(GlobalplayerBD_FM[iPlayerSlot].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_BitSet_Playlist_JIPED_ON_LB)
								iNumberOnPlayList++   
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
			iNumberOnPlayList = (iNumberOnPlayList/2)
			IF iNumberOnPlayList < 1
				iNumberOnPlayList = 1
			ENDIF
			serverBD.iCashBetPlayer = serverBD.iCashBet/iNumberOnPlayList
			PRINTLN("[PLC] serverBD.iCashBetPlayer = 0")
			PRINTLN("[PLC] iNumberOnPlayList       = ", iNumberOnPlayList)
			PRINTLN("[PLC] serverBD.iCashBet       = ", serverBD.iCashBet)
			PRINTLN("[PLC] serverBD.iCashBetPlayer = serverBD.iCashBet/iNumberOnPlayList = " , serverBD.iCashBet, "/", iNumberOnPlayList, " = ", serverBD.iCashBetPlayer)
			PRINTLN("[PLC] serverBD.iCashBetPlayer = ", serverBD.iCashBetPlayer)
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(serverBD.iServerVoteBitSet, ciServerMenuVotePassed)
		IF (TO_FLOAT(sPlayerCounts.iNumberVotedMenu)/TO_FLOAT(sPlayerCounts.iNumberOnPlayList ))>= cfVOTE_PASS_RATIO
		AND (sPlayerCounts.iNumberVotedMenu > 1  OR  sPlayerCounts.iNumberOnPlayList = 1)
			SET_BIT(serverBD.iServerVoteBitSet, ciServerMenuVotePassed)
		ENDIF
	ENDIF
	
	IF resetcoronatimer
		IF serverBD.iServerVoteBitSet != 0
			PRINTLN("[PLC]   serverBD.iServerVoteBitSet = ", serverBD.iServerVoteBitSet)
		ENDIF
		serverBD.iServerVoteBitSet = 0
	ENDIF
ENDPROC

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////				Control playlist stage			   //////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//PROC WRITE_TO_CHALLENGE_LEADERBOARD(BOOL bStart)
//	PRINTLN("[PLC] WRITE_TO_CHALLENGE_LEADERBOARD")
//	TEXT_LABEL_23 uniqueIdentifiers[2]
//	TEXT_LABEL_31 categoryNames[2]
//	
//	//If it's the end and we've los then don't bother writing
//	IF NOT bStart
//		IF HAS_CHALLENGE_BEEN_FAILED()
//			EXIT
//		ENDIF
//	ENDIF
//	
//	//If they've won:
//	IF NOT bStart
//		//Check that a challenge content ID is ok
//		#IF IS_DEBUG_BUILD
//		CHECK_CHALLENGE_CONTENT_ID()
//		#ENDIF
//		PRINTLN("[PLC] WRITE_TO_CHALLENGE_LEADERBOARD - g_sCurrentPlayListDetails.tl23ChallengeContentID = ", g_sCurrentPlayListDetails.tl23ChallengeContentID)
//		uniqueIdentifiers[0] = g_sCurrentPlayListDetails.tl23ChallengeContentID
//		categoryNames[0] = "Challenge"
//		INT iPosX
//		iPosX = GET_CLOUD_TIME_AS_INT()
//		//Write to crew challenge leaderboard
//		IF INIT_LEADERBOARD_WRITE(LEADERBOARD_FREEMODE_CREW_CHALLENGES,uniqueIdentifiers,categoryNames,1)   
//			// Write score
//			LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_SCORE,-iPosX, 0.0)
//		ENDIF		   
//		PRINTLN("WRITE_TO_CHALLENGE_LEADERBOARD(): Writing to crew challenge leader board score: ", iPosX)
//	ENDIF
//	uniqueIdentifiers[0] = ""
//	uniqueIdentifiers[0] += g_sCurrentPlayListDetails.iClanID
//	categoryNames[0] = "OposingCrew"
//	categoryNames[1] = "SeasonId"
//	uniqueIdentifiers[1] = g_sMPTunables.iEloSeason
//	IF INIT_LEADERBOARD_WRITE(LEADERBOARD_FREEMODE_CREW_CHALLENGES_OVERALL,uniqueIdentifiers,categoryNames,2)   
//		//If it's the start
//		IF bStart
//			// Num wins (total)
//			LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_WINS,0, 0.0)
//			// Num Lose (total)
//			LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_LOSES,1, 0.0)
//			// Num wins
//			LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_CHALLENGE_WINS,0,0.0)
//			// Num Loses
//			LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_CHALLENGE_LOSES,1,0.0)
//		//We've won
//		ELSE	
//			// Num wins (total)
//			LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_WINS,1, 0.0)
//			// Num Lose (total)
//			LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_LOSES,-1, 0.0)
//			// Num wins
//			LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_CHALLENGE_WINS,1,0.0)
//			// Num Loses
//			LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_CHALLENGE_LOSES,-1,0.0)
//		ENDIF
//		//challenges data not set
//		LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_H2H_WINS,0, 0.0)
//		LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_H2H_LOSES,0, 0.0)
//	ENDIF
//	
//ENDPROC
//
//PROC WRITE_TO_HEAD_TO_HEAD_LEADERBOARDS()
//	TEXT_LABEL_23 uniqueIdentifiers[2]
//	TEXT_LABEL_31 categoryNames[2]
//	INT iOverrideCrewID
//	INT i
//		
//	REPEAT MAX_HEAD_TO_HEAD_CREWS i
//		uniqueIdentifiers[0] = ""
//		IF i = 1
//			uniqueIdentifiers[0] += serverBD.iTeamCrew[0]
//			iOverrideCrewID = serverBD.iTeamCrew[1]
//		ELIF i= 0
//			uniqueIdentifiers[0] += serverBD.iTeamCrew[1]
//			iOverrideCrewID = serverBD.iTeamCrew[0]
//		ENDIF
//		categoryNames[0] = "OposingCrew"
//		
//		IF INIT_LEADERBOARD_WRITE(LEADERBOARD_FREEMODE_CREW_CHALLENGES_H2H,uniqueIdentifiers,categoryNames,1,iOverrideCrewID )   
//			// Num matches
//			LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_SCORE,1, 0.0)
//			
//			IF serverBD.iWinningTeam = i
//				// Num wins
//				LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_WINS,1, 0.0)
//				// Num Lose
//				LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_LOSES,0, 0.0)
//			ELSE
//				// Num wins
//				LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_WINS,0, 0.0)
//				// Num Lose
//				LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_LOSES,1, 0.0)
//			ENDIF
//		ENDIF
//			
//		categoryNames[1] = "SeasonId"
//		uniqueIdentifiers[1] = g_sMPTunables.iEloSeason
//		IF INIT_LEADERBOARD_WRITE(LEADERBOARD_FREEMODE_CREW_CHALLENGES_OVERALL,uniqueIdentifiers,categoryNames,2,iOverrideCrewID )   
//			
//			IF serverBD.iWinningTeam = i
//				// Num wins (total)
//				LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_WINS,1, 0.0)
//				// Num Lose (total)
//				LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_LOSES,0, 0.0)
//				// Num wins
//				LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_H2H_WINS,1, 0.0)
//				// Num Lose
//				LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_H2H_LOSES,0, 0.0)
//			ELSE
//				// Num wins (total)
//				LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_WINS,0, 0.0)
//				// Num Lose (total)
//				LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_LOSES,1, 0.0)
//				// Num wins
//				LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_H2H_WINS,0, 0.0)
//				// Num Lose
//				LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_H2H_LOSES,1, 0.0)
//			ENDIF
//			//challenges data not set
//			LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_CHALLENGE_WINS,0,0.0)
//			LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_CHALLENGE_LOSES,0,0.0)
//		ENDIF
//	ENDREPEAT
//ENDPROC



PROC WRITE_TO_TOURNAMENT_LEADERBOARD()
	//write to leaderboard here.
	TEXT_LABEL_23 uniqueIdentifiers[1]
	TEXT_LABEL_31 categoryNamesDummy[1]
	//STRING eventType
	
	//IF SB_IS_QUALIFYING_PLAYLIST()
	//	eventType = "TournamentQualifying"  
	//ELSE
	//	eventType= "Tournament"  		
	//ENDIF
	uniqueIdentifiers[0] = ""
	INT iTotalTime = 0
	INT iTotalDeaths = 0
	INT iTotalKills = 0
	
	
	INT i	
	//If it's a DM tournament then we need to write the score rather than the time
	IF serverBD.bDmTournament
		REPEAT serverBD.iLength i			
			iTotalKills += sLocalPlayersScores.iKills[i]
			iTotalDeaths += sLocalPlayersScores.iDeaths[i]
			PRINTLN("[PLC] sLocalPlayersScores.iScore[", i, "] = ", sLocalPlayersScores.iScore[i])
		ENDREPEAT
	ELSE
		REPEAT serverBD.iLength i
			//If the total time is less than 0 then make it a DNF
			IF sLocalPlayersScores.iTime[i] <= 0
				sLocalPlayersScores.iTime[i] = ciRaceFail
				IF SB_IS_QUALIFYING_PLAYLIST()
					g_sQualifyTourFeed.bFailedToSetTime = TRUE
					g_sQualifyTourFeed.bSetTime = FALSE		
					g_sQualifyTourFeed.iTotalTime = 0
				ENDIF
			ENDIF
			iTotalTime += sLocalPlayersScores.iTime[i]
			PRINTLN("[PLC] sLocalPlayersScores.iTime[", i, "] = ", sLocalPlayersScores.iTime[i])
		ENDREPEAT
		PRINTLN("[PLC] iTotalTime ", iTotalTime)
		IF SB_IS_QUALIFYING_PLAYLIST()
			IF g_sQualifyTourFeed.bFailedToSetTime = FALSE
				g_sQualifyTourFeed.iTotalTime = iTotalTime
				g_sQualifyTourFeed.bSetTime = TRUE
				PRINTLN("[PLC] SC PLAYLIST - g_sQualifyTourFeed.iTotalTime = ", iTotalTime)
			ENDIF
		ENDIF
	ENDIF
	IF serverBD.bDmTournament
		IF INIT_LEADERBOARD_WRITE(LEADERBOARD_FREEMODE_DEATHMATCH_TOURNAMENT_QUALIFICATION, uniqueIdentifiers, categoryNamesDummy,0, -1, FALSE, FALSE) //all writes use the events now. removed old command
			LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_SCORE, iTotalScoreForTurnament, 0) //negation so lowest value is highest on board.
			PRINTLN("[PLC] Writing total score of : ",iTotalScoreForTurnament)
			LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_KILLS, iTotalKills,0)
			LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_DEATHS, iTotalDeaths,0)
			LEADERBOARDS_WRITE_ADD_COLUMN_LONG(LB_INPUT_COL_MATCH_ID,0,0) 
			REPEAT FMMC_MAX_PLAY_LIST_LENGTH i			
				LEADERBOARDS_WRITE_ADD_COLUMN(INT_TO_ENUM(LEADERBOARD_INPUTS, (ENUM_TO_INT(LB_INPUT_SCORE_COLUMN_GAME_1) + i)), sLocalPlayersScores.iScore[i],0)
			ENDREPEAT
		ENDIF
	ELSE
		IF INIT_LEADERBOARD_WRITE(LEADERBOARD_FREEMODE_RACE_TOURNAMENT_QUALIFICATION, uniqueIdentifiers, categoryNamesDummy,0, -1, FALSE, FALSE) //all writes use the events now. removed old command
			LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_SCORE, iTotalScoreForTurnament, 0) //negation so lowest value is highest on board.
			PRINTLN("[PLC] Writing total score of : ",iTotalScoreForTurnament)
			//I am not sure why there are these other columns on this board...
			LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_BEST_TIME,0,0)
			LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_TOTAL_TIME,-iTotalTime,0)
			PRINTLN("[PLC] Writing total time of : ",-iTotalTime)
			LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_VEHICLE_ID,0,0)
			LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_CUSTOM_VEHICLE,0,0)
			LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_NUM_LAPS,0,0)
			LEADERBOARDS_WRITE_ADD_COLUMN_LONG(LB_INPUT_COL_MATCH_ID,0,0) 
		ENDIF
	ENDIF
ENDPROC
//get the
FUNC INT GET_RP_FOR_PLAYLIST_WIN()
	RETURN (serverBD.iLength * g_sMPTunables.iPLAYLIST_WINNER_RP)
ENDFUNC
//Give players RP
PROC GIVE_PLAYERS_RP_FOR_PLAYLIST_WIN()	
	GIVE_LOCAL_PLAYER_FM_XP(eXPTYPE_STANDARD, "", XPTYPE_PLAYLIST, XPCATEGORY_COMPLETE_PLAYLIST_WON, GET_RP_FOR_PLAYLIST_WIN(), 1)
ENDPROC
//Deal with giving stuff to the player if they played an event to the end
PROC DEAL_WITH_PLAYLIST_EVENT_GIFT_ACTIVE()
	
	INT i
	INT iPlaylistTuneableValue
	INT iCurrentPlaylistHash = GET_HASH_KEY(g_sCurrentPlayListDetails.tl31szContentID)
	
	PRINTLN("[PLC] PLAYLIST_EVENT_GIFT_ACTIVE - DEAL_WITH_PLAYLIST_EVENT_GIFT_ACTIVE called for playlist: ", iCurrentPlaylistHash)
	
	TEXT_LABEL_31 tlPlaylistReward
	TEXT_LABEL_31 tlPlaylistItemLabel
	TEXT_LABEL_63 strContext
	TEXT_LABEL_63 label
	
	INT contextID = NETWORK_GET_CONTENT_MODIFIER_LIST_ID(iCurrentPlaylistHash)
	PRINTLN("[PLC] PLAYLIST_EVENT_GIFT_ACTIVE - contextID = ", contextID)
	
	
	PED_COMP_TYPE_ENUM ePedCompTypeEnum
	PED_COMP_NAME_ENUM ePedCompNameEnum
	
	INT iAmmo
	WEAPON_TYPE eWeapon
	AMMO_TYPE AmmoType
	GUNCLUB_WEAPON_DATA_STRUCT	sWeaponStruct
	
	BOOL  bDisplayAmmo
	
	FOR i = 0 TO (ciPLAYLIST_MAX_NUM_OF_REWARDS-1) STEP 1
	
		strContext = Get_Tunable_Context_As_String(TUNE_CONTEXT_MP_GLOBALS)
		
		tlPlaylistReward = "RewardPlaylist_"
		tlPlaylistReward += i
		
		IF contextID > -1	
		#IF IS_DEBUG_BUILD
		OR GET_COMMANDLINE_PARAM_EXISTS("sc_fPLAYLIST_EVENT_GIFT")
			IF contextID < 0
				contextID = 0
			ENDIF
			#ENDIF	
			label = "CONTENT_MODIFIER_"
			label += contextID

			strContext = label
			PRINTLN("[PLC] DEAL_WITH_PLAYLIST_EVENT_GIFT_ACTIVE - NETWORK_DOES_TUNABLE_EXIST(", label, ", ", tlPlaylistReward, ") - strContext = ", strContext)
		ENDIF
		
		// First check if the playlist tuneable is active
		IF NETWORK_ACCESS_TUNABLE_INT(strContext, tlPlaylistReward, iPlaylistTuneableValue)
			PRINTLN("[PLC] DEAL_WITH_PLAYLIST_EVENT_GIFT_ACTIVE - NETWORK_ACCESS_TUNABLE_INT(", strContext, ", ", tlPlaylistReward, ", ", iPlaylistTuneableValue, ")")
			
			//...yes, check if the playlist hash is the same as the one we are in
			IF iCurrentPlaylistHash = iPlaylistTuneableValue
			
				PRINTLN("[PLC] DEAL_WITH_PLAYLIST_EVENT_GIFT_ACTIVE - We have found playlist tuneable matching our playlist: ", iPlaylistTuneableValue)
			
				tlPlaylistReward = "RewardPlaylistItem_"
				tlPlaylistReward += i
			
				//...yes, grab the item hash and award the item
				IF NETWORK_ACCESS_TUNABLE_INT(strContext, tlPlaylistReward, iPlaylistTuneableValue)
					PRINTLN("[PLC] DEAL_WITH_PLAYLIST_EVENT_GIFT_ACTIVE - Reward the item associated with this playlist. Hash(", iPlaylistTuneableValue, ")")
					
					AmmoType = INT_TO_ENUM(AMMO_TYPE, iPlaylistTuneableValue)
					
					// Is the item the High Flyer Chute bag for the Flight School
					IF iPlaylistTuneableValue = 668033595 // HASH("PS_BAG_13")
						SET_PACKED_STAT_BOOL(PACKED_MP_STAT_UNLOCK_PILOT_SCHOOL_REWARD_BAG, TRUE)
					
					// Is the item a piece of valid clothing?
					ELIF GET_MP_REWARD_CLOTHING_FROM_TUNABLE(GET_PLAYER_MODEL(), iPlaylistTuneableValue, ePedCompTypeEnum, ePedCompNameEnum)
						
						//...yes, give the item to the player
						GIVE_MP_REWARD_CLOTHING(ePedCompTypeEnum, ePedCompNameEnum, tlPlaylistItemLabel)
						PRINTLN("[PLC] DEAL_WITH_PLAYLIST_EVENT_GIFT_ACTIVE - Rewarded item associated with this playlist (Clothing). ", tlPlaylistItemLabel)
						DISPLAY_TSHIRT_AWARD_MESSGE_AFTER_TRANSITION(ePedCompTypeEnum)
						g_playlist_reward_item[i] = iPlaylistTuneableValue
						
						// Fix 2666715 PC ONLY - January 2016 Tunables - Neither player received a bulletproof helmet for completion of the playlist: "FW_MODIFIERTEST"
						IF iPlaylistTuneableValue =26785663 //Female  -945256745 
						OR iPlaylistTuneableValue = 1751120680 // male
							SET_PACKED_STAT_BOOL(PACKED_MP_STAT_AWARD_BULLET_PROOF_HELMET_PC, TRUE)
						ENDIF
						
						
					//Is the item armour	
					ELIF iPlaylistTuneableValue = -197740536 //HASH("WT_BA_0")
						bDisplayAmmo = TRUE
						g_playlist_reward_item[i] = iPlaylistTuneableValue
						INT iPedArmour = GET_PED_ARMOUR(PLAYER_PED_ID())
						IF iPedArmour < 0 
							iPedArmour = 0
						ENDIF
						PRINTLN("[PLC] DEAL_WITH_PLAYLIST_EVENT_GIFT_ACTIVE - iPedArmour = ", iPedArmour, " ")
						INT iarmour_reward_gift = 0
						g_playlist_ArmourGiven = 0
						NETWORK_ACCESS_TUNABLE_INT(strContext, "ARMOUR_REWARD_GIFT", iarmour_reward_gift)
						PRINTLN("[PLC] DEAL_WITH_PLAYLIST_EVENT_GIFT_ACTIVE - iarmour_reward_gift = ", iarmour_reward_gift, " ")
						IF iarmour_reward_gift > 0
							g_playlist_ArmourGiven = iarmour_reward_gift+iPedArmour
							IF g_playlist_ArmourGiven > 100
								g_playlist_ArmourGiven = 100-iPedArmour
							ENDIF
						ENDIF
						PRINTLN("[PLC] DEAL_WITH_PLAYLIST_EVENT_GIFT_ACTIVE - g_playlist_ArmourGiven Cached = ", g_playlist_ArmourGiven, " ")
						
						SET_PED_ARMOUR(PLAYER_PED_ID(), iarmour_reward_gift + iPedArmour)
						PRINTLN("[PLC] DEAL_WITH_PLAYLIST_EVENT_GIFT_ACTIVE - SET_PED_ARMOUR(PLAYER_PED_ID(),", iarmour_reward_gift + iPedArmour, ")")
					//It's an ammo gift
					ELIF IS_AMMO_TYPE_VALID_FOR_GIFT(AmmoType)
						bDisplayAmmo = TRUE
						//iAmmo = GET_PED_AMMO_BY_TYPE(PLAYER_PED_ID(), AmmoType)
						//PRINTLN("[PLC] DEAL_WITH_PLAYLIST_EVENT_GIFT_ACTIVE - iAmmo = GET_PED_AMMO_BY_TYPE(PLAYER_PED_ID, AmmoType) = ", iAmmo)						
						iAmmo = GET_AMMO_FOR_GIFT_FROM_TUNABLE(AmmoType)
						PRINTLN("[PLC] DEAL_WITH_PLAYLIST_EVENT_GIFT_ACTIVE - = GET_AMMO_FOR_GIFT_FROM_TUNABLE(AmmoType) = ", iAmmo)
						SET_PED_AMMO_BY_TYPE(PLAYER_PED_ID(), AmmoType, iAmmo)
						PRINTLN("[PLC] DEAL_WITH_PLAYLIST_EVENT_GIFT_ACTIVE - SET_PED_AMMO_BY_TYPE(PLAYER_PED_ID, AmmoType, ", iAmmo, ")")
						DISPLAY_AMMO_AWARD_MESSGE_AFTER_TRANSITION()
						g_playlist_reward_item[i] = iPlaylistTuneableValue
					ELSE
						eWeapon = INT_TO_ENUM(WEAPON_TYPE, iPlaylistTuneableValue)
						
						//...no, is the item a weapon?
						IF IS_WEAPON_VALID(eWeapon)
							
							PRINTLN("[PLC] DEAL_WITH_PLAYLIST_EVENT_GIFT_ACTIVE - Rewarded item is a valid weapon. Get data. (Weapon). ", GET_WEAPON_NAME(eWeapon))
						
							IF GET_GUNCLUB_WEAPON_DATA_FROM_WEAPON_TYPE(eWeapon, sWeaponStruct)
										
								iAmmo = GET_AMMO_FOR_GIFT_WEAPON_FROM_TUNABLE(eWeapon)
								//If the ammo is 0 then set it to the default weapon type
								IF 	iAmmo = 0					
									IF eWeapon != WEAPONTYPE_MINIGUN
										iAmmo = sWeaponStruct.iDefaultClipSize * 2
									ELSE
										iAmmo = 300
									ENDIF
								ENDIF
								//...give the weapon
								GIVE_DELAYED_WEAPON_TO_PED(PLAYER_PED_ID(), eWeapon, iAmmo, TRUE)
								SET_MP_WEAPON_PURCHASED(eWeapon, TRUE)
								DISPLAY_AMMO_AWARD_MESSGE_AFTER_TRANSITION()
								bDisplayAmmo = TRUE
								g_playlist_reward_item[i] = iPlaylistTuneableValue
								PRINTLN("[PLC] DEAL_WITH_PLAYLIST_EVENT_GIFT_ACTIVE - Rewarded item associated with this playlist (Weapon). ", GET_WEAPON_NAME(eWeapon), ", iAmmo = ", iAmmo)
							ENDIF
						ENDIF
					ENDIF
					
				ENDIF
			ENDIF
		ENDIF
	ENDFOR

	IF bDisplayAmmo
		DISPLAY_AMMO_AWARD_MESSGE_AFTER_TRANSITION()
	ENDIF
	
ENDPROC

CONST_INT ciEND_PLAYLIST_SAVE_PLAYER_DATA_INT   0
CONST_INT ciEND_PLAYLIST_SAVE_PLAYER_DATA_SAVE  1


FUNC BOOL CONTROL_END_LEADERBOARD_AND_CHALLENGE_WRITE()
	   //For conor for - 1393682
	
	//This is the best score for the player
	//sLocalPlayersScores.iTime[FMMC_MAX_PLAY_LIST_LENGTH]
	//sLocalPlayersScores.iScore[FMMC_MAX_PLAY_LIST_LENGTH]
	
	//This is the best score for a crew
	//serverBD.iBestScore[FMMC_MAX_PLAY_LIST_LENGTH]
	//serverBD.iBestTime[FMMC_MAX_PLAY_LIST_LENGTH]
   	// SLOCALPLAYERSSCORES = SLOCALPLAYERSSCORES
	//This is the tournament ID
	//serverBD.tl23PlayListToPlay
	
//	NETWORK_CLAN_DESC   retDesc
//	GAMER_HANDLE		GamerHandle 
//	IF SB_PLAYLIST_DOING_CHALLENGE()
//
//		SWITCH playerBD[PARTICIPANT_ID_TO_INT()].iSaveToUgc
//			CASE ciEND_PLAYLIST_SAVE_PLAYER_DATA_INT
//				IF HAS_CHALLENGE_BEEN_FAILED()
//				OR g_sCurrentPlayListDetails.bChallengeIsStillOpen = FALSE			
//					REQUEST_SYSTEM_ACTIVITY_TYPE_PLAYED_CHALLENGE_PLAYLIST(g_sCurrentPlayListDetails.tl31PlaylistName)
//					g_FMMC_Current_Playlist_Cloud_Data.iFailed++
//					PRINTLN("[PLC] g_FMMC_Current_Playlist_Cloud_Data.iFailed = ", g_FMMC_Current_Playlist_Cloud_Data.iFailed)
//				ELSE
//					
//					//If the launch time is 0 then set the character stat
//					IF GET_MP_INT_CHARACTER_STAT(MP_STAT_CHALLENGE_LAUNCH_TIME) = 0
//						SET_MP_INT_CHARACTER_STAT(MP_STAT_CHALLENGE_LAUNCH_TIME, GET_CLOUD_TIME_AS_INT())
//						SET_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_OF_CHALLENGES_TODAY, 0)
//						PRINTLN("[PLC] CHALLENGE RESTRICTION - SET_MP_INT_CHARACTER_STAT(MP_STAT_CHALLENGE_LAUNCH_TIME, ", GET_CLOUD_TIME_AS_INT(), ")")
//						PRINTLN("[PLC] CHALLENGE RESTRICTION - SET_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_OF_CHALLENGES_TODAY, 0)")
//					ENDIF
//					//Incrament the number of challenges that we've compleated
//					INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_OF_CHALLENGES_TODAY, 1)
//					PRINTLN("[PLC] CHALLENGE RESTRICTION - GET_MP_INT_CHARACTER_STAT(MP_STAT_CHALLENGE_LAUNCH_TIME)      = ", GET_MP_INT_CHARACTER_STAT(MP_STAT_CHALLENGE_LAUNCH_TIME))
//					PRINTLN("[PLC] CHALLENGE RESTRICTION - GET_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_OF_CHALLENGES_TODAY) = ", GET_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_OF_CHALLENGES_TODAY))
//					//The fix for 2146215- [NT] [EXPLOIT] [PUBLIC REPORTED] Remote player receives $250K bet wins by spectating local player successfully complete a crew challenge.
//					//The tunable to enable it is FALSE
//					IF g_sMPTunables.bChallengeSpectatorCheck = FALSE
//					//Or we joined as a spectator
//					OR (NOT IS_PLAYER_SCTV(PLAYER_ID())
//					AND NOT DID_I_JOIN_MISSION_AS_SPECTATOR()
//					AND NOT playerBD[PARTICIPANT_ID_TO_INT()].bJoinedAsSpectator)
//						IF iChallengeCashPrize = 0
//							iChallengeCashPrize = GET_CHALLENGE_CASH_PRIZE()
//						ENDIF
//						IF iChallengeCashPrize > 0
//							PRINTLN("[PLC] NETWORK_EARN_FROM_CHALLENGE_WIN(", iChallengeCashPrize, ", ", serverBD.tl23PlayListToPlay, ", FALSE)")
//							IF USE_SERVER_TRANSACTIONS()
//								INT iScriptTransactionIndex
//								TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_CHALLENGE_WIN, GET_CHALLENGE_CASH_PRIZE(), iScriptTransactionIndex, DEFAULT, DEFAULT)
//								g_cashTransactionData[iScriptTransactionIndex].cashInfo.tl23Challenge = serverBD.tl23PlayListToPlay
//								g_cashTransactionData[iScriptTransactionIndex].cashInfo.bHead2Head = FALSE
//							ELSE
//							NETWORK_EARN_FROM_CHALLENGE_WIN(GET_CHALLENGE_CASH_PRIZE(), serverBD.tl23PlayListToPlay, FALSE)
//						ENDIF
//							
//						ENDIF
//						REQUEST_SYSTEM_ACTIVITY_TYPE_WON_CHALLENGE_PLAYLIST(g_sCurrentPlayListDetails.tl31PlaylistName, iChallengeCashPrize)
//						GIVE_PLAYERS_RP_FOR_PLAYLIST_WIN()	
//						INCREMENT_MP_INT_PLAYER_STAT(MPPLY_CREW_CHALLENGE_WIN) 
//						PRINTLN("[PLC] INCREMENT_MP_INT_PLAYER_STAT(MPPLY_CREW_CHALLENGE_WIN)")
//					ENDIF
//				ENDIF
//				PRINTLN("[PLC] playerBD[PARTICIPANT_ID_TO_INT()].iSaveToUgc = ciEND_PLAYLIST_SAVE_PLAYER_DATA_SAVE")
//				playerBD[PARTICIPANT_ID_TO_INT()].iSaveToUgc = ciEND_PLAYLIST_SAVE_PLAYER_DATA_SAVE
//				#IF IS_DEBUG_BUILD
//				IF bBeatChallenge = FALSE
//				#ENDIF
//					IF g_FMMC_Current_Playlist_Cloud_Data.bWon = FALSE
//						IF HAS_CHALLENGE_BEEN_FAILED()
//							g_FMMC_Current_Playlist_Cloud_Data.bWon = FALSE
//						ELSE
//							g_FMMC_Current_Playlist_Cloud_Data.bWon = TRUE
//						ENDIF
//						
//					ENDIF
//				#IF IS_DEBUG_BUILD
//				ELSE
//					g_FMMC_Current_Playlist_Cloud_Data.bWon = TRUE
//				ENDIF
//				#ENDIF
//				
//				IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
//					WRITE_TO_CHALLENGE_LEADERBOARD(FALSE)
//				ENDIF
//			BREAK
//			CASE ciEND_PLAYLIST_SAVE_PLAYER_DATA_SAVE
//				IF SAVE_OUT_UGC_PLAYER_DATA(serverBD.tl23PlayListToPlay, sSaveOutVars, UGC_TYPE_GTA5_CHALLENGE)
//					GIVE_LOCAL_PLAYER_FM_CASH(serverBD.iCashBet)
//					RETURN TRUE
//				ENDIF
//			BREAK
//		ENDSWITCH
//		RETURN FALSE
//	ELIF SB_PLAYLIST_DOING_HEAD_TO_HEAD()
//		SWITCH playerBD[PARTICIPANT_ID_TO_INT()].iSaveToUgc
//			CASE ciEND_PLAYLIST_SAVE_PLAYER_DATA_INT
//				IF serverBD.iWinningTeam = playerBD[PARTICIPANT_ID_TO_INT()].iteam
//					PRINTLN("[PLC] NETWORK_EARN_FROM_CHALLENGE_WIN(", serverBD.iCashBetPlayer, ", ", serverBD.tl23PlayListToPlay, " TRUE)")
//					IF serverBD.iCashBetPlayer > 0
//						IF USE_SERVER_TRANSACTIONS()
//							INT iScriptTransactionIndex
//							TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_CHALLENGE_WIN, serverBD.iCashBetPlayer, iScriptTransactionIndex, DEFAULT, DEFAULT)
//							g_cashTransactionData[iScriptTransactionIndex].cashInfo.tl23Challenge = serverBD.tl23PlayListToPlay
//							g_cashTransactionData[iScriptTransactionIndex].cashInfo.bHead2Head = TRUE
//						ELSE
//						NETWORK_EARN_FROM_CHALLENGE_WIN(serverBD.iCashBetPlayer, serverBD.tl23PlayListToPlay, TRUE)
//					ENDIF
//					ENDIF
//					GIVE_PLAYERS_RP_FOR_PLAYLIST_WIN()	
//				ELSE
//				ENDIF
//				PRINTLN("[PLC] playerBD[PARTICIPANT_ID_TO_INT()].iSaveToUgc = ciEND_PLAYLIST_SAVE_PLAYER_DATA_SAVE")
//				playerBD[PARTICIPANT_ID_TO_INT()].iSaveToUgc = ciEND_PLAYLIST_SAVE_PLAYER_DATA_SAVE
//			BREAK
//			CASE ciEND_PLAYLIST_SAVE_PLAYER_DATA_SAVE
//				IF SAVE_OUT_UGC_PLAYER_DATA(serverBD.tl23PlayListToPlay, sSaveOutVars, UGC_TYPE_GTA5_MISSION_PLAYLIST)
//					IF serverBD.iWinningTeam = playerBD[PARTICIPANT_ID_TO_INT()].iteam
//						INCREMENT_MP_INT_PLAYER_STAT(MPPLY_CREW_HEAD2HEAD_WIN) 
//			   			PRINTLN("[PLC] INCREMENT_MP_INT_PLAYER_STAT(MPPLY_CREW_HEAD2HEAD_WIN)")
//					ENDIF
////					IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
////						WRITE_TO_HEAD_TO_HEAD_LEADERBOARDS()
////					ENDIF
//					RETURN TRUE
//				ENDIF
//			BREAK
//		ENDSWITCH
//		RETURN FALSE
//	ENDIF

	IF NOT SB_PLAYLIST_SETTING_CHALLENGE_TIME()
	AND NOT SB_PLAYLIST_DOING_CHALLENGE()
		SWITCH playerBD[PARTICIPANT_ID_TO_INT()].iSaveToUgc	 
			CASE 0
				playerBD[PARTICIPANT_ID_TO_INT()].iSaveToUgc++
			BREAK
			CASE 1
				IF NOT IS_STRING_NULL_OR_EMPTY(serverBD.tl23PlayListToPlay)
					IF SAVE_OUT_UGC_PLAYER_DATA(serverBD.tl23PlayListToPlay, sSaveOutVars, UGC_TYPE_GTA5_MISSION_PLAYLIST)

						//If we've played a full playlist
						IF bPlayedFullPlayList
								//If we've played to the end of a tournament playlist then set the activity feed
								IF SB_IS_TOURNAMENT_PLAYLIST()
									//If I won
									IF serverBD.iWinningTeam = playerBD[PARTICIPANT_ID_TO_INT()].iteam
										REQUEST_SYSTEM_ACTIVITY_TYPE_WON_TOURNAMENT()
									ELSE
										REQUEST_SYSTEM_ACTIVITY_TYPE_PLAYED_TOURNAMENT()
									ENDIF
								//If we've played to the end of a tournament qualifer playlist then set the activity feed
								ELIF SB_IS_QUALIFYING_PLAYLIST()
									REQUEST_SYSTEM_ACTIVITY_TYPE_PLAYED_TOURNAMENT_QUALIFIER()
								//If we've played to the end of a event playlist then set the activity feed
								ELSE
									REQUEST_SYSTEM_ACTIVITY_TYPE_PLAY_EVENT_PLAYLIST(g_sCurrentPlayListDetails.tl31PlaylistName)
								ENDIF
							PRINTLN("[PLC] PLAYLIST_EVENT_GIFT_ACTIVE - Player played full playlist to the end")
							//And the tunable gift is active then call something
							IF g_sMPTunables.bEventPlayListGiftActive
							#IF IS_DEBUG_BUILD
							OR GET_COMMANDLINE_PARAM_EXISTS("sc_fPLAYLIST_EVENT_GIFT")
							#ENDIF
								PRINTLN("[PLC] PLAYLIST_EVENT_GIFT_ACTIVE - The g_sMPTunables.bEventPlayListGiftActive is active")
								DEAL_WITH_PLAYLIST_EVENT_GIFT_ACTIVE()
							ENDIF
						ENDIF
							
						IF SB_IS_TOURNAMENT_PLAYLIST()
						OR SB_IS_QUALIFYING_PLAYLIST()
						OR SB_IS_LIVESTREAM_PLAYLIST()
							WRITE_TO_TOURNAMENT_LEADERBOARD()
						ENDIF
						
						RETURN TRUE
					ENDIF
				ELSE
					PRINTLN("[PLC] CONTROL_END_LEADER_BOARD - SAVE_OUT_UGC_PLAYER_DATA - serverBD.tl23PlayListToPlay = NULL")
					SCRIPT_ASSERT("[PLC] CONTROL_END_LEADER_BOARD - SAVE_OUT_UGC_PLAYER_DATA - serverBD.tl23PlayListToPlay = NULL")	 
					RETURN TRUE
				ENDIF
			BREAK
		ENDSWITCH
		RETURN FALSE
	ENDIF

	
//	#IF IS_DEBUG_BUILD
//	//If the command line sc_DontDownLoadUGC is running
//	IF g_bSkipDownloadingUGC
//		PRINTLN("[PLC] sc_DontDownLoadUGC")
//		RETURN TRUE
//	ENDIF
//	#ENDIF
//	
//	INT iCloudTime  
//	INT iCount	  
//	UGC_DESCRIPTION szDesc, szTagz 
//	UGC_PATHS_STRUCT szFilePaths
//	//Only the host creates the challenge time
//	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
//			
//		//Don't do anything if the cloud is down
//		IF IS_CLOUD_DOWN_CLOUD_LOADER()
//			SET_CONTROL_END_LEADER_BOARD_STAGE(ciEND_LEADER_BOARD_SAVE_TO_UGC_ERROR)
//		ENDIF
//		SWITCH serverBD.iSaveChallengeToUGC
//			CASE ciEND_LEADER_BOARD_SAVE_TO_SC_LEADER_BOARD
//				#IF IS_DEBUG_BUILD
//				IF bSetATime
//					FOR iCount = 0 TO (serverBD.iLength - 1)
//						serverBD.iBestScore[iCount] = 0
//						serverBD.iBestTime[iCount]  = iSetScore
//					ENDFOR
//				ENDIF
//				#ENDIF
//				REQUEST_SYSTEM_ACTIVITY_TYPE_SETUP_CHALLENGE_PLAYLIST()
//				CLEANUP_DATA_FILE()
//				SET_CONTROL_END_LEADER_BOARD_STAGE(ciEND_LEADER_BOARD_SET_UP_DATA)
//			BREAK
//			
//			CASE ciEND_LEADER_BOARD_SET_UP_DATA
//				PRINTLN("[PLC] SET_UP_PLAYLIST_DATA")
//				
//				//Set up loading the data from the cloud
//				DATAFILE_CREATE()
//				
//				//Get a handel on the top level dictionay
//				dfdMainDict = DATAFILE_GET_FILE_DICT()
//				//Create the main dict.
//				dfDataDict	  = DATADICT_CREATE_DICT(dfdMainDict, "data")
//				//how this challenge expites
//				//DATADICT_SET_STRING(dfDataDict, "type", "BEAT")			   
//				//set the crew who set the challenge
//				GamerHandle = GET_LOCAL_GAMER_HANDLE()
//				NETWORK_CLAN_PLAYER_GET_DESC(retDesc, SIZE_OF(retDesc), GamerHandle)
//				//crew name
//				DATADICT_SET_STRING(dfDataDict, "crewnm", retDesc.ClanName)
//				//crew ID
//				DATADICT_SET_INT(dfDataDict, "crew", retDesc.Id)
//				//get the current cloud time
//				iCloudTime = GET_CLOUD_TIME_AS_INT()
//				//Start time and end time
//				DATADICT_SET_INT(dfDataDict, "str", iCloudTime)
//				//If we are debugging a close expire time then set it for 60s
//				#IF IS_DEBUG_BUILD
//					IF bSetCloseTime
//						DATADICT_SET_INT(dfDataDict, "end", iCloudTime + (60))			  
//					ELSE
//				#ENDIF
//						//Other wise set it as a week!
//						DATADICT_SET_INT(dfDataDict, "end", iCloudTime + (60 * 60 * 24 * 7))
//				#IF IS_DEBUG_BUILD			  
//					ENDIF
//				#ENDIF
//				//add false beaten bool
//				DATADICT_SET_BOOL(dfDataDict, "open", TRUE)
//				//the cash bet
//				DATADICT_SET_INT(dfDataDict, "cash", serverBD.iCashBet)
//				//The playlist this was set on
//				DATADICT_SET_STRING(dfDataDict, "pid", serverBD.tl23PlayListToPlay)
//				//Set the difficulty
//				//get the avrage
//				IF serverBD.fChallengeDif != 0.0
//					serverBD.fChallengeDif /= serverBD.iLength
//				ENDIF
//				DATADICT_SET_FLOAT(dfDataDict, "dif", serverBD.fChallengeDif)
//				//Create a target dict
//				dfTargetDict	= DATADICT_CREATE_DICT(dfDataDict, "trg")
//				//add target time and score
//				serverBD.iTimeToBeat	= 0 
//				serverBD.iScoreToBeat   = 0
//				FOR iCount = 0 TO (serverBD.iLength - 1)
//					serverBD.iTimeToBeat  += serverBD.iBestTime[iCount]
//					serverBD.iScoreToBeat += serverBD.iBestScore[iCount]
//				ENDFOR					  
//				DATADICT_SET_INT(dfTargetDict, "time", serverBD.iTimeToBeat)
//				DATADICT_SET_INT(dfTargetDict, "scr",  serverBD.iScoreToBeat)
//				PRINTLN("[PLC] serverBD.iTimeToBeat  = ", serverBD.iTimeToBeat)
//				PRINTLN("[PLC] serverBD.iScoreToBeat = ", serverBD.iScoreToBeat)
//				
//				//set up the playlist details
//				dfaMain		 = DATADICT_CREATE_ARRAY(dfTargetDict, "list")
//				//Fill in the Arrays
//				FOR iCount = 0 TO (serverBD.iLength - 1)
//					dfdArray[iCount] = DATAARRAY_ADD_DICT(dfaMain)
//					IF dfdArray[iCount] != NULL
//						DATADICT_SET_STRING(dfdArray[iCount], "cid", g_sCurrentPlayListDetails.tl31ContentIDs.szContentID[iCount])
//						DATADICT_SET_INT(dfdArray[iCount], "scr", serverBD.iBestScore[iCount])
//						DATADICT_SET_INT(dfdArray[iCount], "time", serverBD.iBestTime[iCount])
//					ENDIF
//				ENDFOR						  
//				//Set up loading the data from the cloud
//				//SET_UP_CLOUD_DIRECTORY_STRING_TIME(tl63, tl23FileAndPath, UGC_TYPE_GTA5_MISSION_PLAYLIST)
//				//PRINTSTRING(" SAVEOUT_PLAYLIST_DETAILS_TO_CLOUD - Attempting to save the data to - ")PRINTSTRING(tl63)PRINTNL()
//				//DATAFILE_START_SAVE_TO_CLOUD(tl63)
//				SET_CONTROL_END_LEADER_BOARD_STAGE(ciEND_LEADER_BOARD_SAVE_TO_UGC)		  
//			BREAK
//			
////		  CASE ciEND_LEADER_BOARD_SAVE_TO_MEMBER_SPACE
////			  IF NOT DATAFILE_UPDATE_SAVE_TO_CLOUD(bSuccess)
////				  IF bSuccess
////					  PRINTSTRING(" SAVEOUT_PLAYLIST_DETAILS_TO_CLOUD - Save sucesfull")PRINTNL()
////					  //CLEANUP_DATA_FILE()
////					  SET_CONTROL_END_LEADER_BOARD_STAGE(ciEND_LEADER_BOARD_SAVE_TO_UGC)
////				  ELSE
////					  PRINTSTRING(" SAVEOUT_PLAYLIST_DETAILS_TO_CLOUD - Save Failed")PRINTNL()
////					  //make this better
////					  SET_CONTROL_END_LEADER_BOARD_STAGE(ciEND_LEADER_BOARD_SAVE_TO_UGC_ERROR)
////					  CLEANUP_DATA_FILE()
////					  RETURN TRUE
////				  ENDIF
////			  ELSE
////				  PRINTSTRING(" SAVEOUT_PLAYLIST_DETAILS_TO_CLOUD - Waiting for DATAFILE_UPDATE_SAVE_TO_CLOUD(bSuccess)")PRINTNL()
////			  ENDIF
////		  BREAK
//
//			CASE ciEND_LEADER_BOARD_SAVE_TO_UGC 
//				szDesc.TextLabel[0] = serverBD.tl23PlayListToPlay
//				IF UGC_CREATE_CONTENT(szFilePaths, 1, serverBD.tl31ChallengeName, szDesc, szTagz, GET_UGC_TYPE_STRING(UGC_TYPE_GTA5_CHALLENGE), TRUE)
//					PRINTLN("[PLC] UGC_CREATE_CONTENT")
//					SET_CONTROL_END_LEADER_BOARD_STAGE(ciEND_LEADER_BOARD_SAVE_TO_UGC_WAIT_FOR_FINISH)
//				ELSE
//					SET_CONTROL_END_LEADER_BOARD_STAGE(ciEND_LEADER_BOARD_SAVE_TO_UGC_ERROR)					
//				ENDIF
//			BREAK
//			
//			CASE ciEND_LEADER_BOARD_SAVE_TO_UGC_WAIT_FOR_FINISH
//				IF UGC_HAS_CREATE_FINISHED()
//					PRINTLN("[PLC] IF UGC_HAS_CREATE_FINISHED()")
//					IF UGC_DID_CREATE_SUCCEED()
//						PRINTLN("[PLC] UGC_DID_CREATE_SUCCEED()")
//						UGC_CLEAR_CREATE_RESULT()
//						RETURN TRUE					 
//					ELSE
//						PRINTLN("[PLC] UGC_DID_CREATE_SUCCEED() = FALSE")
//						SCRIPT_ASSERT("UGC_DID_CREATE_SUCCEED() = FALSE")
//						SET_CONTROL_END_LEADER_BOARD_STAGE(ciEND_LEADER_BOARD_SAVE_TO_UGC_ERROR)
//					ENDIF
//				ELSE
//					PRINTLN("[PLC] CREATE_FMMC_MISSION_ON_CLOUD() = FALSE")
//				ENDIF
//			BREAK
//			
//			CASE ciEND_LEADER_BOARD_SAVE_TO_UGC_ERROR
//				SET_WARNING_MESSAGE_WITH_HEADER("CWS_WARNING", "FMMC_CHALFA", FE_WARNING_OK)				
//				IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
//					UGC_CLEAR_CREATE_RESULT()
//					NETWORK_REFUND_CASH_TYPE(serverBD.iCashBet, MP_REFUND_TYPE_CHALLENGE, MP_REFUND_REASON_SERVER_ERROR)
//					RETURN TRUE
//				ENDIF
//			BREAK
//		ENDSWITCH
//	ELSE
//		REQUEST_SYSTEM_ACTIVITY_TYPE_SETUP_CHALLENGE_PLAYLIST()
//		RETURN TRUE
//	ENDIF
	RETURN FALSE
ENDFUNC

PROC CALCULATE_BEST_TIME_AND_SCORE()
	INT iLoop
	FOR iLoop = 0 TO (FMMC_MAX_PLAY_LIST_LENGTH - 1)
		iTotalBestScore += serverBD.iBestScore[iLoop]
		IF serverBD.iBestTime[iLoop] > 0
			iTotalBestTime  += serverBD.iBestTime[iLoop]
		ENDIF
	ENDFOR
ENDPROC

FUNC PLAYER_INDEX GET_PLAYER_IN_LEADERBOARD_POS(INT iPos)
	PLAYER_INDEX PlayerToGet
	
	// Added to fix 311089
	IF serverBD_LB.sleaderboard[iPos].iParticipant <> -1
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(serverBD_LB.sleaderboard[iPos].iParticipant))
			PlayerToGet = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(serverBD_LB.sleaderboard[iPos].iParticipant))
		ENDIF
	ENDIF
	
	RETURN PlayerToGet
ENDFUNC

FUNC BOOL DID_PLAYER_WIN_CHALLENGE()
	BOOL bWonChallenge

	IF IS_PLAYLIST_DOING_HEAD_TO_HEAD()
		IF GET_PLAYER_CREW_INT_CL(PLAYER_ID()) = GET_PLAYER_CREW_INT_CL(serverBD.piWinningPlayer)
			CDEBUG1LN(DEBUG_MISSION, "DID_PLAYER_WIN_CHALLENGE - Head to Head: Player's crew is the same as the winning crew.")
		
			bWonChallenge = TRUE
		ELSE
			CDEBUG1LN(DEBUG_MISSION, "DID_PLAYER_WIN_CHALLENGE - Head to Head: Player's crew is the same as the winning crew.")
		
			bWonChallenge = FALSE
			serverBD.iCashBet = 0
		ENDIF
	ELIF IS_PLAYLIST_DOING_CHALLENGE()						
		IF HAS_CHALLENGE_BEEN_FAILED()
		OR g_sCurrentPlayListDetails.bChallengeIsStillOpen = FALSE
			CDEBUG1LN(DEBUG_MISSION, "DID_PLAYER_WIN_CHALLENGE - Crew Challenge: Player failed the challenge.")
		
			bWonChallenge = FALSE
			serverBD.iCashBet = 0
		ELSE
			CDEBUG1LN(DEBUG_MISSION, "DID_PLAYER_WIN_CHALLENGE - Crew Challenge: Player passed the challenge.")
		
			bWonChallenge = TRUE
		ENDIF
	ELSE
		IF ARE_STRINGS_EQUAL(serverBD.tlWinningPlayer, GET_PLAYER_NAME(PLAYER_ID()))
			CDEBUG1LN(DEBUG_MISSION, "DID_PLAYER_WIN_CHALLENGE - Playlist: Player is the winner.")
		
			bWonChallenge = TRUE
		ELSE
			CDEBUG1LN(DEBUG_MISSION, "DID_PLAYER_WIN_CHALLENGE - Playlist: Player is not the winner.")
		
			bWonChallenge = FALSE
		ENDIF
	ENDIF
	
	RETURN bWonChallenge
ENDFUNC


/// PURPOSE:
///    Creates a list of players that were on the winning team, in order of their position (if it's not a team event then just one player is stored).
FUNC BOOL GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN(PLAYER_INDEX &winningPlayers[MAX_NUM_CELEBRATION_PEDS], INT &iNumberPlayersFound)
	INT iNumPlayersFound = 0
	INT i = 0

	//First wipe the list (default value is zero which is a valid player index).
	REPEAT COUNT_OF(winningPlayers) i
		winningPlayers[i] = INVALID_PLAYER_INDEX()
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
	BOOL bDoingHeadToHead = IS_PLAYLIST_DOING_HEAD_TO_HEAD()
	BOOL bOnTournamentPlaylist = IS_PLAYLIST_IS_A_TOURNAMENT_PLAYLIST()
	PRINTLN("[NETCELEBRATION] ServerBD.iNumPlaylistTeams = ", ServerBD.iNumPlaylistTeams)
	PRINTLN("[NETCELEBRATION] IS_PLAYLIST_DOING_HEAD_TO_HEAD() = ", bDoingHeadToHead)
	PRINTLN("[NETCELEBRATION] IS_PLAYLIST_IS_A_TOURNAMENT_PLAYLIST() ", bOnTournamentPlaylist)
	PRINTLN("[NETCELEBRATION] ServerBD.iWinningTeam", ServerBD.iWinningTeam)
	#ENDIF
	
	IF ServerBD.iNumPlaylistTeams > 1   
	AND IS_PLAYLIST_DOING_HEAD_TO_HEAD()
		REPEAT COUNT_OF(serverBD_LB.sleaderboard) i
			IF iNumPlayersFound < COUNT_OF(winningPlayers)
				INT iParticipant = serverBD_LB.sleaderboard[i].iParticipant
				
				PRINTLN("[NETCELEBRATION] leaderboard player ", i, ", iParticipant = ", iParticipant)
				
				IF iParticipant > -1
					
					PRINTLN("[NETCELEBRATION] leaderboard player ", i, ", iteam  = ", playerBD[iParticipant].iteam)
					PRINTLN("[NETCELEBRATION] leaderboard player ", i, ", bJoinedAsSpectator = ", playerBD[iParticipant].bJoinedAsSpectator)
				
					IF playerBD[iParticipant].iteam > -1
					AND NOT playerBD[iParticipant].bJoinedAsSpectator
						IF playerBD[iParticipant].iteam = ServerBD.iWinningTeam
							PLAYER_INDEX currentPlayer = GET_PLAYER_IN_LEADERBOARD_POS(i)
							
							IF currentPlayer != INVALID_PLAYER_INDEX()
								IF IS_NET_PLAYER_OK(currentPlayer, FALSE)
									CDEBUG1LN(DEBUG_MISSION, "[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - Player being added to list of winners:")
									CDEBUG1LN(DEBUG_MISSION, "[NETCELEBRATION] Name: ", GET_PLAYER_NAME(currentPlayer))
									CDEBUG1LN(DEBUG_MISSION, "[NETCELEBRATION] iNumPlayersFound: ", iNumPlayersFound)
									CDEBUG1LN(DEBUG_MISSION, "[NETCELEBRATION] array index: ", i)
								
									winningPlayers[iNumPlayersFound] = currentPlayer
									iNumPlayersFound++
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				PRINTLN("[NETCELEBRATION] iNumPlayersFound >= COUNT_OF(winningPlayers). iNumPlayersFound = ", iNumPlayersFound, ", COUNT_OF(winningPlayers) = ", COUNT_OF(winningPlayers))
			ENDIF
		ENDREPEAT
	ELIF SB_PLAYLIST_DOING_CHALLENGE()
		//If doing a crew challenge then only check if the player won the challenge.
		CDEBUG1LN(DEBUG_MISSION, "[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - Crew challenge: checking if player won.")
	
		//If it's not a head to head then it's a case of either the player beat the challenge or they didn't.
		IF DID_PLAYER_WIN_CHALLENGE()
			CDEBUG1LN(DEBUG_MISSION, "[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - Player beat the challenge, adding them to the list.")
			
			winningPlayers[0] = PLAYER_ID()
			
			IF IS_NET_PLAYER_OK(winningPlayers[0], FALSE)
				iNumPlayersFound++
			ENDIF
		ENDIF
	ELSE
		//If doing a playlist then just get the person in first place on the leaderboard.
		CDEBUG1LN(DEBUG_MISSION, "[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - Playlist: grabbing top player from leaderboard.")
		
		IF IS_NET_PLAYER_OK(serverBD.piWinningPlayer, FALSE)
			winningPlayers[0] = serverBD.piWinningPlayer
		
			CDEBUG1LN(DEBUG_MISSION, "[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - Playlist: Top player is valid, adding to list.")
			CDEBUG1LN(DEBUG_MISSION, "[NETCELEBRATION] Name: ", GET_PLAYER_NAME(serverBD.piWinningPlayer))
		
			iNumPlayersFound++
		ENDIF
	ENDIF
	
	iNumberPlayersFound = iNumPlayersFound
	
	IF iNumPlayersFound > 0
		RETURN TRUE
	ENDIF
	
	CDEBUG1LN(DEBUG_MISSION, "[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - No valid winners found.")
	
	RETURN FALSE
ENDFUNC


//Deal with the tournament playlist intro celebration screen
FUNC BOOL RUN_TOURNAMENT_OUTRO_CELEBRATION_SCREEN(JOB_OUTRO_CUT_DATA &jobOutroData)
	
	PRINTLN("[Qual_Celebration] - running tournament qualifying celebration screen.")
	
	// Main switch.
	SWITCH jobOutroData.iDrawTextStage	
		
		// Setup screen.
		CASE 0		
			
			// Request assets.
			IF NOT jobOutroData.bRequestedCelebrationScaleform
				jobOutroData.sfForeground = REQUEST_SCALEFORM_MOVIE("MP_CELEBRATION_BG")
				jobOutroData.sfBackground = REQUEST_SCALEFORM_MOVIE("MP_CELEBRATION_FG")
				jobOutroData.sfCelebration = REQUEST_SCALEFORM_MOVIE("MP_CELEBRATION")
				PRINTLN("[Qual_Celebration] - requested job outro scale form movies.")
				jobOutroData.bRequestedCelebrationScaleform = TRUE
			ENDIF
			
			// Check the assets have loaded.
			IF HAS_SCALEFORM_MOVIE_LOADED(jobOutroData.sfForeground)
			AND HAS_SCALEFORM_MOVIE_LOADED(jobOutroData.sfBackground)
			AND HAS_SCALEFORM_MOVIE_LOADED(jobOutroData.sfCelebration)
				
				SET_NEWS_FEED_SPLASH_SCREEN_ACTIVE(FALSE)
				
				// Function variables.
				INT i, iTotalTime
				FLOAT fTimeInSeconds
				STRING strScreenID, strBackgroundColour, strType, strType2
				
				// Init values.
				strScreenID = "ch"
				strBackgroundColour = "HUD_COLOUR_BLACK"
				
				// Get players total time.
				REPEAT FMMC_MAX_PLAY_LIST_LENGTH i
					IF sLocalPlayersScores.iTime[i] > 0
						iTotalTime += sLocalPlayersScores.iTime[i]
					ENDIF
				ENDREPEAT
				
				PRINTLN("[Qual_Celebration] - iTotalTime = ", iTotalTime)
				
				// Assert if our time is negative.
				#IF IS_DEBUG_BUILD
				IF iTotalTime < 0
					SCRIPT_ASSERT("Total tournament time < 0.")
				ENDIF
				#ENDIF
				
				// Type of screen.
				strType = "CELEB_QUALIFICATION_COMPLETE"
				strType2 = "CELEB_TOTAL_TIME"
				
				PRINTLN("[Qual_Celebration] - strType = ", strType, ", strType2 = ", strType2)
				
				PRINTLN("[TS] SET_UP_JOINING_TRANSITION_SESSION - CREATE_STAT_WALL")
				
				// Create initial outro screen.
				BEGIN_SCALEFORM_MOVIE_METHOD(jobOutroData.sfForeground, "CREATE_STAT_WALL")
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
						ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
					END_TEXT_COMMAND_SCALEFORM_STRING()
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
						ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strBackgroundColour)
					END_TEXT_COMMAND_SCALEFORM_STRING()
					PRINTLN("[Qual_Celebration] - created initial foreground.")
				END_SCALEFORM_MOVIE_METHOD()
				
				BEGIN_SCALEFORM_MOVIE_METHOD(jobOutroData.sfBackground, "CREATE_STAT_WALL")
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
						ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
					END_TEXT_COMMAND_SCALEFORM_STRING()
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
						ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strBackgroundColour)
					END_TEXT_COMMAND_SCALEFORM_STRING()
					PRINTLN("[Qual_Celebration] - created initial background.")
				END_SCALEFORM_MOVIE_METHOD()
				
				BEGIN_SCALEFORM_MOVIE_METHOD(jobOutroData.sfCelebration, "CREATE_STAT_WALL")
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
						ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
					END_TEXT_COMMAND_SCALEFORM_STRING()
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
						ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strBackgroundColour)
					END_TEXT_COMMAND_SCALEFORM_STRING()
					PRINTLN("[Qual_Celebration] - created initial main celebration layer.")
				END_SCALEFORM_MOVIE_METHOD()
				
				// Setup pause time. 
				PRINTLN("[TS] SET_UP_JOINING_TRANSITION_SESSION - SET_PAUSE_DURATION")
				fTimeInSeconds = 3.0
				PRINTLN("[Qual_Celebration] - pause time = ", fTimeInSeconds)
				
				BEGIN_SCALEFORM_MOVIE_METHOD(jobOutroData.sfForeground, "SET_PAUSE_DURATION")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fTimeInSeconds)
				END_SCALEFORM_MOVIE_METHOD()					
				BEGIN_SCALEFORM_MOVIE_METHOD(jobOutroData.sfBackground, "SET_PAUSE_DURATION")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fTimeInSeconds)
				END_SCALEFORM_MOVIE_METHOD()					
				BEGIN_SCALEFORM_MOVIE_METHOD(jobOutroData.sfCelebration, "SET_PAUSE_DURATION")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fTimeInSeconds)
				END_SCALEFORM_MOVIE_METHOD()
				
				// Setup foreground.
				PRINTLN("[TS] SET_UP_JOINING_TRANSITION_SESSION - ADD_TOURNAMENT_TO_WALL - sfForeground")
				BEGIN_SCALEFORM_MOVIE_METHOD(jobOutroData.sfForeground, "ADD_TOURNAMENT_TO_WALL")
					// Screen ID.
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
						ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
					END_TEXT_COMMAND_SCALEFORM_STRING()
					PRINTLN("[Qual_Celebration] - added screen ID to foreground. strScreenID = ", strScreenID)
					// Playlist name.
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
						ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_sCurrentPlayListDetails.tl31PlaylistName)
					END_TEXT_COMMAND_SCALEFORM_STRING()
					PRINTLN("[Qual_Celebration] - added playlist name to foreground. g_sCurrentPlayListDetails.tl31PlaylistName = ", g_sCurrentPlayListDetails.tl31PlaylistName)
					// Screen type name (qualification complete).
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
						ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strType)
					END_TEXT_COMMAND_SCALEFORM_STRING()
					PRINTLN("[Qual_Celebration] - added screen name to foreground. strType = ", strType)
					// Total time value.
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(strType2)
					PRINTLN("[Qual_Celebration] - added score label to foreground. strType2 = ", strType2)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTotalTime)
					PRINTLN("[Qual_Celebration] - added total time to foreground. iTotalTime = ", iTotalTime)
				END_SCALEFORM_MOVIE_METHOD()	
				
				// Setup background.
				PRINTLN("[TS] SET_UP_JOINING_TRANSITION_SESSION - ADD_TOURNAMENT_TO_WALL - sfBackground")
				BEGIN_SCALEFORM_MOVIE_METHOD(jobOutroData.sfBackground, "ADD_TOURNAMENT_TO_WALL")
					// Screen ID.
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
						ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
					END_TEXT_COMMAND_SCALEFORM_STRING()
					PRINTLN("[Qual_Celebration] - added screen ID to background. strScreenID = ", strScreenID)
					// Playlist name.
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
						ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_sCurrentPlayListDetails.tl31PlaylistName)
					END_TEXT_COMMAND_SCALEFORM_STRING()
					PRINTLN("[Qual_Celebration] - added playlist name to background. g_sCurrentPlayListDetails.tl31PlaylistName = ", g_sCurrentPlayListDetails.tl31PlaylistName)
					// Screen type name (qualification complete).
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
						ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strType)
					END_TEXT_COMMAND_SCALEFORM_STRING()
					PRINTLN("[Qual_Celebration] - added screen name to background. strType = ", strType)
					// Total time label.
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(strType2)
					PRINTLN("[Qual_Celebration] - added score label to background. strType2 = ", strType2)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTotalTime)
					PRINTLN("[Qual_Celebration] - added total time to background. iTotalTime = ", iTotalTime)
				END_SCALEFORM_MOVIE_METHOD()	
				
				// Setup main.
				PRINTLN("[TS] SET_UP_JOINING_TRANSITION_SESSION - ADD_TOURNAMENT_TO_WALL - sfCelebration")
				BEGIN_SCALEFORM_MOVIE_METHOD(jobOutroData.sfCelebration, "ADD_TOURNAMENT_TO_WALL")
					// Screen ID.
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
						ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
					END_TEXT_COMMAND_SCALEFORM_STRING()
					PRINTLN("[Qual_Celebration] - added screen ID to main layer. strScreenID = ", strScreenID)
					// Playlist name.
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
						ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_sCurrentPlayListDetails.tl31PlaylistName)
					END_TEXT_COMMAND_SCALEFORM_STRING()
					PRINTLN("[Qual_Celebration] - added playlist name to main layer. g_sCurrentPlayListDetails.tl31PlaylistName = ", g_sCurrentPlayListDetails.tl31PlaylistName)
					// Screen type name (qualification complete).
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
						ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strType)
					END_TEXT_COMMAND_SCALEFORM_STRING()
					PRINTLN("[Qual_Celebration] - added screen name to main layer. strType = ", strType)
					// Total time label.
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(strType2)
					PRINTLN("[Qual_Celebration] - added screen name to main layer. strType2 = ", strType2)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTotalTime)
					PRINTLN("[Qual_Celebration] - added total time to main layer. iTotalTime = ", iTotalTime)
				END_SCALEFORM_MOVIE_METHOD()			
				
				// Add background to the outro screen.
				PRINTLN("[TS] SET_UP_JOINING_TRANSITION_SESSION - ADD_BACKGROUND_TO_WALL")
				BEGIN_SCALEFORM_MOVIE_METHOD(jobOutroData.sfForeground, "ADD_BACKGROUND_TO_WALL")
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
						ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
					END_TEXT_COMMAND_SCALEFORM_STRING()
				END_SCALEFORM_MOVIE_METHOD()
				BEGIN_SCALEFORM_MOVIE_METHOD(jobOutroData.sfBackground, "ADD_BACKGROUND_TO_WALL")
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
						ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
					END_TEXT_COMMAND_SCALEFORM_STRING()
				END_SCALEFORM_MOVIE_METHOD()
				BEGIN_SCALEFORM_MOVIE_METHOD(jobOutroData.sfCelebration, "ADD_BACKGROUND_TO_WALL")
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
						ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
					END_TEXT_COMMAND_SCALEFORM_STRING()
				END_SCALEFORM_MOVIE_METHOD()
				PRINTLN("[Qual_Celebration] - added background to screen.")
				
				BEGIN_SCALEFORM_MOVIE_METHOD(jobOutroData.sfForeground, "SHOW_STAT_WALL")
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
						ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
					END_TEXT_COMMAND_SCALEFORM_STRING()
				END_SCALEFORM_MOVIE_METHOD()
				PRINTLN("[Qual_Celebration] - called SHOW_STAT_WALL for foreground screen.")
				
				BEGIN_SCALEFORM_MOVIE_METHOD(jobOutroData.sfBackground, "SHOW_STAT_WALL")
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
						ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
					END_TEXT_COMMAND_SCALEFORM_STRING()
				END_SCALEFORM_MOVIE_METHOD()
				PRINTLN("[Qual_Celebration] - called SHOW_STAT_WALL for background screen.")
				
				BEGIN_SCALEFORM_MOVIE_METHOD(jobOutroData.sfCelebration, "SHOW_STAT_WALL")
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
						ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strScreenID)
					END_TEXT_COMMAND_SCALEFORM_STRING()
				END_SCALEFORM_MOVIE_METHOD()
				PRINTLN("[Qual_Celebration] - called SHOW_STAT_WALL for main layer screen.")
				
				PRINTLN("[Qual_Celebration] - TRANSITION SESSIONS - SET_UP_JOINING_TRANSITION_SESSION - HAS_INTRO_SCREEN_LOADED")
				
				RESET_NET_TIMER(jobOutroData.jobIntroTextTimer)
				START_NET_TIMER(jobOutroData.jobIntroTextTimer)				
				PRINTLN("[Qual_Celebration] - DRAW_JOB_TEXT : jobOutroData.iDrawTextStage = 1")				
				jobOutroData.iDrawTextStage = 1		
				
			ENDIF			
		BREAK
		
		// Draw screen.
		CASE 1
			
			DRAW_SCALEFORM_MOVIE_FULLSCREEN_MASKED(jobOutroData.sfForeground, jobOutroData.sfBackground, 255, 255, 255, 255)
			DRAW_SCALEFORM_MOVIE_FULLSCREEN(jobOutroData.sfCelebration, 255, 255, 255, 255)
			PRINTLN("[Qual_Celebration] - drawing movie.")
			
			//Once the timer finishes then progress: this depends on how many elements were added to the screen.
			IF HAS_NET_TIMER_EXPIRED(jobOutroData.jobIntroTextTimer, 6000)
				jobOutroData.iDrawTextStage = 2
//				SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(jobOutroData.sfForeground)
//				SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(jobOutroData.sfBackground)
//				SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(jobOutroData.sfCelebration)
				PRINTLN("[Qual_Celebration] - timer reached 6 seconds.")
				RETURN TRUE
			ENDIF
			
		BREAK	
		
		// Complete.
		CASE 2
			
			PRINTLN("[Qual_Celebration] - totally done.")
			RETURN TRUE
			
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

//Summary before the main celebration screen that shows how much RP you won.
FUNC BOOL HAS_CELEBRATION_SUMMARY_FINISHED()		
	HIDE_UI_FOR_CELEBRATION_SCREEN_THIS_FRAME()
	
	//Only do this for playlists
	IF SB_PLAYLIST_DOING_CHALLENGE()
	OR SB_PLAYLIST_SETTING_CHALLENGE_TIME()
	OR SB_PLAYLIST_DOING_HEAD_TO_HEAD()
		RETURN TRUE
	ENDIF
	
	IF (serverBD.piWinningPlayer != PLAYER_ID())
		RETURN TRUE
	ENDIF
	
	SWITCH sCelebrationData.eCurrentStage
		CASE CELEBRATION_STAGE_SETUP
			REQUEST_CELEBRATION_SCREEN(sCelebrationData)
			SET_CELEB_BLOCK_RESURRECT_RENDERPHASE_UNPAUSE(TRUE)
			IF HAS_CELEBRATION_SCREEN_LOADED(sCelebrationData)
			AND IS_SCREEN_FADED_IN()
			AND NOT ANIMPOSTFX_IS_RUNNING("DeathFailMPIn")
				
				STRING strScreenName, strBackgroundColour
				INT iRPWon, iCurrentRP, iCurrentLvl, iNextLvl, iRPToReachCurrentLvl, iRPToReachNextLvl 
				
				//Retrieve all the required stats for the race end screen.
				strScreenName = "SUMMARY"
				strBackgroundColour = "HUD_COLOUR_BLACK"
				iRPWon = GET_RP_FOR_PLAYLIST_WIN()
				iCurrentRP = GET_PLAYER_FM_XP(NETWORK_GET_PLAYER_INDEX(PARTICIPANT_ID())) - iRPWon
				iCurrentLvl = GET_FM_RANK_FROM_XP_VALUE(iCurrentRP) //We don't want the current level, but their level before the XP was given.
				iNextLvl = iCurrentLvl + 1
				iRPToReachCurrentLvl = GET_XP_NEEDED_FOR_FM_RANK(iCurrentLvl)
				iRPToReachNextLvl = GET_XP_NEEDED_FOR_FM_RANK(iNextLvl)
				sCelebrationData.iEstimatedScreenDuration = CELEBRATION_SCREEN_STAT_WIPE_TIME * 2
				
				//Build the screen
				CREATE_CELEBRATION_SCREEN(sCelebrationData, strScreenName, strBackgroundColour)
				
				IF DID_PLAYER_WIN_CHALLENGE()
					ADD_REP_POINTS_AND_RANK_BAR_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName, iRPWon, iCurrentRP, 
											   	   					  iRPToReachCurrentLvl, iRPToReachNextLvl, iCurrentLvl, iNextLvl)
					sCelebrationData.iEstimatedScreenDuration += CELEBRATION_SCREEN_STAT_DISPLAY_TIME //RP screen is guaranteed.
					
					IF iCurrentRP + iRPWon > iRPToReachNextLvl
						sCelebrationData.iEstimatedScreenDuration += ((CELEBRATION_SCREEN_STAT_DISPLAY_TIME / 2) + (CELEBRATION_SCREEN_STAT_DISPLAY_TIME / 4)) //If we levelled up do an extra screen.
					ENDIF
				ENDIF
				
				sCelebrationData.iEstimatedScreenDuration += CELEBRATION_SCREEN_STAT_WIPE_TIME
				
				ADD_BACKGROUND_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName)
				SHOW_CELEBRATION_SCREEN(sCelebrationData, strScreenName)
				
				RESET_NET_TIMER(sCelebrationData.sCelebrationTimer)
				START_NET_TIMER(sCelebrationData.sCelebrationTimer)
				
				CLEAR_HELP()
				
				CDEBUG1LN(DEBUG_MISSION, "[NETCELEBRATION] Adding stats ---------------------------")
				CDEBUG1LN(DEBUG_MISSION, "strScreenName ", strScreenName)
				CDEBUG1LN(DEBUG_MISSION, "iCurrentRP ", iCurrentRP)
				CDEBUG1LN(DEBUG_MISSION, "iCurrentLvl ", iCurrentLvl)
				CDEBUG1LN(DEBUG_MISSION, "iNextLvl ", iNextLvl)
				CDEBUG1LN(DEBUG_MISSION, "iRPToReachCurrentLvl ", iRPToReachCurrentLvl)
				CDEBUG1LN(DEBUG_MISSION, "iRPToReachNextLvl ", iRPToReachNextLvl)
				CDEBUG1LN(DEBUG_MISSION, "sCelebrationData.iEstimatedScreenDuration ", sCelebrationData.iEstimatedScreenDuration)
				CDEBUG1LN(DEBUG_MISSION, "---------------------------------------------------------")
				RESET_CELEBRATION_PRE_LOAD(sCelebrationData)
				sCelebrationData.eCurrentStage = CELEBRATION_STAGE_PLAYING
				
			ELSE
			
				IF ANIMPOSTFX_IS_RUNNING("DeathFailMPIn")
					IF NOT HAS_NET_TIMER_STARTED(sCelebrationData.timerDeathPostFxDelay)
						START_NET_TIMER(sCelebrationData.timerDeathPostFxDelay)
						PRINTLN("[NETCELEBRATION] HAS_CELEBRATION_WINNER_FINISHED - DeathFailMPIn is running, started timer timerDeathPostFxDelay.")
					ELSE
						IF HAS_NET_TIMER_EXPIRED(sCelebrationData.timerDeathPostFxDelay, 1000)
							PRINTLN("[NETCELEBRATION] HAS_CELEBRATION_WINNER_FINISHED - DeathFailMPIn is running, timer timerDeathPostFxDelay >= 1000. Calling CLEAR_KILL_STRIP_DEATH_EFFECTS.")
							CLEAR_KILL_STRIP_DEATH_EFFECTS()
						ENDIF
					ENDIF	
				ENDIF
			
			ENDIF
		BREAK
		
		CASE CELEBRATION_STAGE_PLAYING
			DRAW_CELEBRATION_SCREEN_THIS_FRAME(sCelebrationData, FALSE)
			
			//If we're going straight from the summary to a winner screen then do an early flash to help the transition.
			IF NOT sCelebrationData.bTriggeredEarlyFlash
//				IF HAS_NET_TIMER_EXPIRED(sCelebrationData.sCelebrationTimer, sCelebrationData.iEstimatedScreenDuration - (CELEBRATION_SCREEN_STAT_WIPE_TIME * 2))
				IF HAS_CELEBRATION_SHARD_FINISHED_FALL_AWAY()
					ANIMPOSTFX_PLAY("MP_Celeb_Win_Out", 0, FALSE)
					
					sCelebrationData.bTriggeredEarlyFlash = TRUE
				ENDIF
			ENDIF
			
			//Once the timer finishes then progress: this depends on how many elements were added to the screen.
			IF HAS_NET_TIMER_EXPIRED(sCelebrationData.sCelebrationTimer, sCelebrationData.iEstimatedScreenDuration)
				sCelebrationData.eCurrentStage = CELEBRATION_STAGE_FINISHED
				
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE CELEBRATION_STAGE_FINISHED
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

////
/// Playing playlist	
///  name		g_sCurrentPlayListDetails.tl31PlaylistName 
///  winner		serverBD.tlWinningPlayer
///	
///Playing Challenge
///  name		g_sCurrentPlayListDetails.tl31PlaylistName 
///  winner		serverBD.tlWinningPlayer
///  cash		serverBD.iCashBet
///	 
///Playing head 2 Head	 
///  name		g_sCurrentPlayListDetails.tl31PlaylistName 
///  winner   	serverBD.tlWinningPlayer
///  cash  		serverBD.iCashBet
///	
///Setting Challenge
///  name		serverBD.tl31ChallengeName
///  winner		serverBD.tlWinningPlayer
FUNC BOOL CONTROL_PLAYLIST_CELEBRATION(BOOL bKeepDrawingForSkycamTransition = FALSE)
	HIDE_UI_FOR_CELEBRATION_SCREEN_THIS_FRAME()
	
	PLAYER_INDEX winningPlayers[MAX_NUM_CELEBRATION_PEDS]
	BOOL bSuccessfullyGrabbedWinners = GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN(winningPlayers, sCelebrationData.iNumPlayerFoundForWinnerScene)
	INT iWinningPlayer
	BOOL bNeedToPreLoadCelebrationAnims
	BOOL bCelebrationAnimsLoaded
	TEXT_LABEL_63 tl63_AnimDict, tl63_AnimName
	TEXT_LABEL_63 tl63_IdleAnimDict, tl63_IdleAnimName
	
	IF bSuccessfullyGrabbedWinners
		iWinningPlayer = NATIVE_TO_INT(winningPlayers[0])
		IF NOT sCelebrationData.bCreateWinnerSceneEntities
			sCelebrationData.iDisplayingLocalPlayerAsLoser = 1
			PRINTLN("[NETCELEBRATION] - could not grab list of winners, setting flag to indicate we are displaying fallback loser screen.")
		ENDIF
	ELSE
		//If we failed to grab then just use the local player and a dummy winner index, so we can do a "Loser" screen instead.
		winningPlayers[0] = PLAYER_ID()
		iWinningPlayer = NATIVE_TO_INT(serverBD.piWinningPlayer)
		IF NOT sCelebrationData.bCreateWinnerSceneEntities
			sCelebrationData.iDisplayingLocalPlayerAsLoser = 0
			PRINTLN("[NETCELEBRATION] - could not grab list of winners, setting flag to indicate we are not displaying fallback loser screen.")
		ENDIF
	ENDIF
	
	IF sCelebrationData.bAllowPlayerNameToggles
		
		CELEB_SET_PLAYERS_INVISIBLE_THIS_FRAME()
		DRAW_THE_PLAYER_LIST(dpadVars)
		MAINTAIN_CELEBRATION_INSTRUCTIONAL_BUTTONS(sCelebrationData)
		
		IF IS_XBOX360_VERSION()
		OR IS_PS3_VERSION()
			DISPLAY_CELEBRATION_PLAYER_NAMES(sCelebrationData.pedWinnerClones, sCelebrationData.tl31_pedWinnerClonesNames, sCelebrationData.eCurrentStage, sCelebrationData.sfCelebration)
		ELSE
			DRAW_NG_CELEBRATION_PLAYER_NAMES(	sCelebrationData, sCelebrationData.iDrawNamesStage, sCelebrationData.iNumNamesToDisplay, 
												sCelebrationData.pedWinnerClones, sCelebrationData.tl31_pedWinnerClonesNames, 
												sCelebrationData.eCurrentStage, sCelebrationData.sfCelebration,
												sCelebrationData.playerNameMovies, sCelebrationData.bToggleNames)
		ENDIF
	ENDIF
	
	BOOL bQuickCelebrationScreen = SB_PLAYLIST_SETTING_CHALLENGE_TIME()
	IF CONTENT_IS_USING_ARENA()
		bQuickCelebrationScreen = TRUE // url:bugstar:5516654
	ENDIF
	
	HIDE_PLAYERS_FOR_CELEBRATION_WINNER_SCENE(sCelebrationData) // Fix for B* 2278693 - stop player models being seen during winner scene, only want ot see ped clones.
	
	SWITCH sCelebrationData.eCurrentStage
		CASE CELEBRATION_STAGE_SETUP
			BOOL bCreatedWinnerScene
			
			REQUEST_CELEBRATION_SCREEN(sCelebrationData)
		
			IF LOAD_WINNER_SCENE_INTERIOR(sCelebrationData)
				
				IF bQuickCelebrationScreen
					bCreatedWinnerScene = TRUE
				ELSE
					bCreatedWinnerScene = CREATE_WINNER_SCENE_ENTITIES(serverBD.sCelebServer, sCelebrationData, winningPlayers, DEFAULT, NOT bSuccessfullyGrabbedWinners, NULL, FALSE, SB_IS_FINAL_ROUND_TOURNAMENT_PLAYLIST(), FALSE, FALSE, DUMMY_MODEL_FOR_SCRIPT, GlobalplayerBD[iWinningPlayer].iInteractionAnim)
					bNeedToPreLoadCelebrationAnims = TRUE
				ENDIF
				
				IF bNeedToPreLoadCelebrationAnims
					IF bCreatedWinnerScene
						GET_CELEBRATION_ANIM_TO_PLAY(GlobalplayerBD[iWinningPlayer].iInteractionAnim, IS_PED_MALE(sCelebrationData.pedWinnerClones[0]), FALSE, tl63_AnimDict, tl63_AnimName, INT_TO_ENUM(INTERACTION_ANIM_TYPES, GlobalplayerBD[iWinningPlayer].iInteractionType), INT_TO_ENUM(CREW_INTERACTIONS, GlobalplayerBD[iWinningPlayer].iCrewAnim))
						GET_CELEBRATION_IDLE_ANIM_TO_USE(sCelebrationData, tl63_IdleAnimDict, tl63_IdleAnimName, IS_PED_MALE(sCelebrationData.pedWinnerClones[0]))
						REQUEST_ANIM_DICT(tl63_AnimDict)
						REQUEST_ANIM_DICT(tl63_IdleAnimDict)
						IF ( HAS_ANIM_DICT_LOADED(tl63_AnimDict) AND HAS_ANIM_DICT_LOADED(tl63_IdleAnimDict) )
						OR HAS_NG_FAILSAFE_TIMER_EXPIRED(sCelebrationData)
							bCelebrationAnimsLoaded = TRUE
						ENDIF
					ENDIF
				ELSE
					bCelebrationAnimsLoaded = TRUE
				ENDIF
				
				IF HAS_CELEBRATION_SCREEN_LOADED(sCelebrationData)
				AND bCreatedWinnerScene
				AND IS_SCREEN_FADED_IN()
				AND bCelebrationAnimsLoaded
				AND NOT ANIMPOSTFX_IS_RUNNING("DeathFailMPIn")
				
					IF NOT IS_STRING_NULL_OR_EMPTY(tl63_AnimDict)
						sCelebrationData.fAnimLength = GET_ANIM_DURATION(tl63_AnimDict, tl63_AnimName)
						PRINTLN("[NETCELEBRATION] - HAS_CELEBRATION_WINNER_FINISHED - fAnimLength = ", sCelebrationData.fAnimLength)

						sCelebrationData.fAnimLength = GET_ANIM_DURATION(tl63_AnimDict, tl63_AnimName)
						PRINTLN("[NETCELEBRATION] - HAS_CELEBRATION_WINNER_FINISHED - fAnimLength = ", sCelebrationData.fAnimLength)
					ENDIF
							
					BOOL bReadyToStart
					CELEBRATION_SCREEN_STAGE eDesiredStage
				
					IF bQuickCelebrationScreen
						bReadyToStart = TRUE
						eDesiredStage = CELEBRATION_STAGE_TRANSITIONING
					ELSE
						RESET_NET_TIMER(sCelebrationData.sCelebrationTransitionTimer)
						START_NET_TIMER(sCelebrationData.sCelebrationTransitionTimer)

						STOP_CELEBRATION_SCREEN_ANIM(sCelebrationData, "SUMMARY")
						FLASH_CELEBRATION_SCREEN(sCelebrationData, 0.333, 0.15, 0.333)

						//1665147 - Leaderboard music may be running from the previous job, make sure it doesn't play over the celebration screen.
						RUN_TRANSITION_MENU_AUDIO(FALSE)

						//Cache the winner ID for later use in the anims, so that if the player leaves we still have the clone and anim array index.
						IF bSuccessfullyGrabbedWinners
						AND iWinningPlayer > -1
							sCelebrationData.iWinnerPlayerID = iWinningPlayer
						ENDIF

						CDEBUG1LN(DEBUG_MISSION, "[NETCELEBRATION] HAS_CELEBRATION_WINNER_FINISHED - Started flash.")

						eDesiredStage = CELEBRATION_STAGE_DOING_FLASH
						bReadyToStart = TRUE
					ENDIF

					IF bReadyToStart
						RESET_NET_TIMER(sCelebrationData.sCelebrationTimer)
						START_NET_TIMER(sCelebrationData.sCelebrationTimer)

						sCelebrationData.eCurrentStage = eDesiredStage
					ENDIF
				ELSE
					
					IF ANIMPOSTFX_IS_RUNNING("DeathFailMPIn")
						IF NOT HAS_NET_TIMER_STARTED(sCelebrationData.timerDeathPostFxDelay)
							START_NET_TIMER(sCelebrationData.timerDeathPostFxDelay)
							PRINTLN("[NETCELEBRATION] HAS_CELEBRATION_WINNER_FINISHED - DeathFailMPIn is running, started timer timerDeathPostFxDelay.")
						ELSE
							IF HAS_NET_TIMER_EXPIRED(sCelebrationData.timerDeathPostFxDelay, 1000)
								PRINTLN("[NETCELEBRATION] HAS_CELEBRATION_WINNER_FINISHED - DeathFailMPIn is running, timer timerDeathPostFxDelay >= 1000. Calling CLEAR_KILL_STRIP_DEATH_EFFECTS.")
								CLEAR_KILL_STRIP_DEATH_EFFECTS()
							ENDIF
						ENDIF	
					ENDIF
					
				ENDIF
				
				IF HAS_CELEBRATION_SCREEN_LOADED(sCelebrationData)
					DRAW_CELEBRATION_SCREEN_THIS_FRAME(sCelebrationData)
				ENDIF
					
			ENDIF
			
		BREAK
		
		CASE CELEBRATION_STAGE_DOING_FLASH //Should only hit this case if showing the winning player.
			DRAW_CELEBRATION_SCREEN_THIS_FRAME(sCelebrationData)
			
			IF (HAS_NET_TIMER_EXPIRED(sCelebrationData.sCelebrationTransitionTimer, 375) OR IS_SCREEN_FADED_OUT())
				IF NOT bQuickCelebrationScreen
					CLEANUP_LEADERBOARD_CAM(TRUE, TRUE, TRUE, TRUE, FALSE, FALSE)
					ANIMPOSTFX_STOP_ALL()
					SET_SKYFREEZE_CLEAR(TRUE)
					SET_SKYBLUR_CLEAR()
					
					//Cancel the transition.
					STOP_PLAYER_SWITCH()
					SET_SKYSWOOP_STAGE(SKYSWOOP_NONE)
					TAKE_CONTROL_OF_TRANSITION(FALSE)
					SET_SELECTOR_CAM_ACTIVE(FALSE)
					//SET_MINIMAP_IN_SPECTATOR_MODE(FALSE, NULL)
					//SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
					//SET_LOADING_ICON_INACTIVE()
					
					IF IS_NET_PLAYER_OK(PLAYER_ID())
						REMOVE_PED_HELMET(PLAYER_PED_ID(), TRUE) // url:bugstar:2244903 - remove helmet mid-flash.
					ENDIF
				
					sCelebrationData.bUseFullBodyWinnerAnim = SHOULD_WINNER_SCREEN_USE_FULL_BODY_ANIM(sCelebrationData, iWinningPlayer)
					PLACE_WINNER_SCENE_CAMERA(serverBD.sCelebServer, sCelebrationData, ciCelebrationCam, DEFAULT, iWinningPlayer, sCelebrationData.bUseFullBodyWinnerAnim)
					FORCE_WEATHER_AND_TIME_FOR_CELEBRATION_WINNER()
				ENDIF
			
				RESET_NET_TIMER(sCelebrationData.sCelebrationTimer)
				START_NET_TIMER(sCelebrationData.sCelebrationTimer)

				CDEBUG1LN(DEBUG_MISSION, "[NETCELEBRATION] HAS_CELEBRATION_WINNER_FINISHED - Transitioning to winner screen.")

				sCelebrationData.eCurrentStage = CELEBRATION_STAGE_TRANSITIONING
			ENDIF
		BREAK
		
		CASE CELEBRATION_STAGE_TRANSITIONING
			BOOL bReadyToProgress
			
			IF bQuickCelebrationScreen
				DRAW_CELEBRATION_SCREEN_THIS_FRAME(sCelebrationData)
				SET_HIDE_PLAYERS_FOR_CELEBRATION_WINNER_SCENE(sCelebrationData)
				bReadyToProgress = TRUE
			ELSE
				DRAW_CELEBRATION_SCREEN_THIS_FRAME(sCelebrationData)
				IF HAS_NET_TIMER_EXPIRED(sCelebrationData.sCelebrationTimer, 375)
					SET_HIDE_PLAYERS_FOR_CELEBRATION_WINNER_SCENE(sCelebrationData)
				ENDIF
				IF HAS_NET_TIMER_EXPIRED(sCelebrationData.sCelebrationTimer, 750)
					bReadyToProgress = TRUE
				ENDIF
			ENDIF
					
			IF bReadyToProgress
				STRING strScreenName, strBackgroundColour
				TEXT_LABEL_63 strCrewName
				TEXT_LABEL_63 strPlayerName
				TEXT_LABEL_63 strChallengeName
				BOOL bWonChallenge
				BOOL bSetChallengeScores
				INT iCashToDisplay
				
				CALCULATE_BEST_TIME_AND_SCORE()
			
				strScreenName = "PLAYWINNER"
				strCrewName = ""
				strPlayerName = ""
				sCelebrationData.iEstimatedScreenDuration = CELEBRATION_SCREEN_STAT_WIPE_TIME * 2 //Time for the final wipe plus a bit of a gap.
				
				//Retrieve all the required stats for the race end screen.
				IF bSuccessfullyGrabbedWinners
				AND IS_NET_PLAYER_OK(winningPlayers[0], FALSE)
					strCrewName = GET_CREW_NAME_FOR_CELEBRATION_SCREEN(winningPlayers[0])
					strPlayerName = GET_PLAYER_NAME(winningPlayers[0])
					strPlayerName += "<br>"
					strPlayerName += strCrewName
				ENDIF

				//Audio scenes: see B*1642903 for implementation notes.
				IF NOT IS_AUDIO_SCENE_ACTIVE("MP_CELEB_SCREEN_SCENE")
					START_AUDIO_SCENE("MP_CELEB_SCREEN_SCENE")
				ENDIF

				//Build the screen
				IF SB_PLAYLIST_SETTING_CHALLENGE_TIME()
					IF iTotalBestScore > -1 OR iTotalBestTime > -1
						strBackgroundColour = "HUD_COLOUR_BLACK"
						strChallengeName = serverBD.tl31ChallengeName
						
						CREATE_CELEBRATION_SCREEN(sCelebrationData, strScreenName, strBackgroundColour)
						ADD_CHALLENGE_VALUES_SET_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName, strChallengeName, iTotalBestScore, iTotalBestTime)
						sCelebrationData.iEstimatedScreenDuration += CELEBRATION_SCREEN_STAT_DISPLAY_TIME
						bSetChallengeScores = TRUE
					ELSE
						bSetChallengeScores = FALSE
					ENDIF
					
					ADD_BACKGROUND_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName)
					
					CDEBUG1LN(DEBUG_MISSION, "[NETCELEBRATION] CONTROL_PLAYLIST_CELEBRATION - Adding score and time to playlist screen: ", iTotalBestScore, " ", iTotalBestTime)
				ELSE
					bWonChallenge = DID_PLAYER_WIN_CHALLENGE()
				
					IF bWonChallenge
						strBackgroundColour = "HUD_COLOUR_FRIENDLY"
						PLAY_MISSION_COMPLETE_AUDIO("FRANKLIN_BIG_01")
					ELSE
						IF bSuccessfullyGrabbedWinners
							CDEBUG1LN(DEBUG_MISSION, "[NETCELEBRATION] Lost the challenge but successfully grabbed a winner.")
						
							bWonChallenge = TRUE //We're going to show the playlist winner, so don't display "LOSER".
						ENDIF
					
						strBackgroundColour = "HUD_COLOUR_NET_PLAYER1"
						serverBD.iCashBet = 0
						PLAY_MISSION_COMPLETE_AUDIO("GENERIC_FAILED")
					ENDIF
				
					strChallengeName = g_sCurrentPlayListDetails.tl31PlaylistName
				
					CREATE_CELEBRATION_SCREEN(sCelebrationData, strScreenName, strBackgroundColour)
					SET_CELEBRATION_SCREEN_STAT_DISPLAY_TIME(sCelebrationData, CELEBRATION_SCREEN_WINNER_STAT_DISPLAY_TIME - CELEBRATION_SCREEN_STAT_WIPE_TIME)
				
					IF SB_PLAYLIST_DOING_CHALLENGE()
						PRINTLN("[NETCELEBRATION] - SB_PLAYLIST_DOING_CHALLENGE = TRUE.")
						IF iChallengeCashPrize = 0
							iChallengeCashPrize = GET_CHALLENGE_CASH_PRIZE()
							PRINTLN("[NETCELEBRATION] - iChallengeCashPrize = GET_CHALLENGE_CASH_PRIZE = ", iChallengeCashPrize)
						ENDIF
						IF NOT bWonChallenge
							iChallengeCashPrize = (iChallengeCashPrize*(-1))
							PRINTLN("[NETCELEBRATION] - bWonChallenge = FALSE, making prize negative = ", iChallengeCashPrize)
						ENDIF
						iCashToDisplay = iChallengeCashPrize
						IF IS_NET_PLAYER_OK(PLAYER_ID())
							IF IS_PLAYER_SCTV(PLAYER_ID())
								iCashToDisplay = -1
							ENDIF
						ENDIF
						ADD_CHALLENGE_WINNER_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName, CHALLENGE_TYPE_CREW, bWonChallenge, strPlayerName, strChallengeName, iCashToDisplay) //serverBD.iCashBet)
						PRINTLN("[NETCELEBRATION] - called ADD_CHALLENGE_WINNER_TO_CELEBRATION_SCREEN with args: ")
						PRINTLN("[NETCELEBRATION] - strScreenName, ", strScreenName)
						PRINTLN("[NETCELEBRATION] - challenge type, ", CHALLENGE_TYPE_CREW)
						PRINTLN("[NETCELEBRATION] - bWonChallenge, ", bWonChallenge)
						PRINTLN("[NETCELEBRATION] - strPlayerName, ", strPlayerName)
						PRINTLN("[NETCELEBRATION] - strChallengeName, ", strChallengeName)
						PRINTLN("[NETCELEBRATION] - iChallengeCashPrize, ", iChallengeCashPrize)
					ELIF SB_PLAYLIST_DOING_HEAD_TO_HEAD()
						PRINTLN("[NETCELEBRATION] - SB_PLAYLIST_DOING_HEAD_TO_HEAD = TRUE.")
						IF bWonChallenge
							ADD_CHALLENGE_WINNER_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName, CHALLENGE_TYPE_HEAD_TO_HEAD, bWonChallenge, strPlayerName, strChallengeName, g_sCurrentPlayListDetails.iCashWinnings)
							PRINTLN("[NETCELEBRATION] - called ADD_CHALLENGE_WINNER_TO_CELEBRATION_SCREEN with args: ")
							PRINTLN("[NETCELEBRATION] - strScreenName, ", strScreenName)
							PRINTLN("[NETCELEBRATION] - challenge type, ", CHALLENGE_TYPE_HEAD_TO_HEAD)
							PRINTLN("[NETCELEBRATION] - bWonChallenge, ", bWonChallenge)
							PRINTLN("[NETCELEBRATION] - strPlayerName, ", strPlayerName)
							PRINTLN("[NETCELEBRATION] - strChallengeName, ", strChallengeName)
							PRINTLN("[NETCELEBRATION] - g_sCurrentPlayListDetails.iCashWinnings, ", g_sCurrentPlayListDetails.iCashWinnings)
						ELSE
							ADD_CHALLENGE_WINNER_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName, CHALLENGE_TYPE_HEAD_TO_HEAD, bWonChallenge, strPlayerName, strChallengeName, 0)
							PRINTLN("[NETCELEBRATION] - called ADD_CHALLENGE_WINNER_TO_CELEBRATION_SCREEN with args: ")
							PRINTLN("[NETCELEBRATION] - strScreenName, ", strScreenName)
							PRINTLN("[NETCELEBRATION] - challenge type, ", CHALLENGE_TYPE_HEAD_TO_HEAD)
							PRINTLN("[NETCELEBRATION] - bWonChallenge, ", bWonChallenge)
							PRINTLN("[NETCELEBRATION] - strPlayerName, ", strPlayerName)
							PRINTLN("[NETCELEBRATION] - strChallengeName, ", strChallengeName)
							PRINTLN("[NETCELEBRATION] - iCashWon, ", 0)
						ENDIF
					ELIF IS_PLAYLIST_IS_A_TOURNAMENT_PLAYLIST()
						PRINTLN("[NETCELEBRATION] - IS_PLAYLIST_IS_A_TOURNAMENT_PLAYLIST = TRUE.")
						ADD_CHALLENGE_WINNER_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName, CHALLENGE_TYPE_TOURNAMENT, bWonChallenge, strPlayerName, strChallengeName, 0)
						PRINTLN("[NETCELEBRATION] - called ADD_CHALLENGE_WINNER_TO_CELEBRATION_SCREEN with args: ")
						PRINTLN("[NETCELEBRATION] - strScreenName, ", strScreenName)
						PRINTLN("[NETCELEBRATION] - challenge type, ", CHALLENGE_TYPE_TOURNAMENT)
						PRINTLN("[NETCELEBRATION] - bWonChallenge, ", bWonChallenge)
						PRINTLN("[NETCELEBRATION] - strPlayerName, ", strPlayerName)
						PRINTLN("[NETCELEBRATION] - strChallengeName, ", strChallengeName)
						PRINTLN("[NETCELEBRATION] - iCashWon, ", 0)
					ELSE
						PRINTLN("[NETCELEBRATION] - doing standard playlist screen.")
						ADD_CHALLENGE_WINNER_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName, CHALLENGE_TYPE_PLAYLIST, bWonChallenge, strPlayerName, strChallengeName, 0)
						PRINTLN("[NETCELEBRATION] - called ADD_CHALLENGE_WINNER_TO_CELEBRATION_SCREEN with args: ")
						PRINTLN("[NETCELEBRATION] - strScreenName, ", strScreenName)
						PRINTLN("[NETCELEBRATION] - challenge type, ", CHALLENGE_TYPE_TOURNAMENT)
						PRINTLN("[NETCELEBRATION] - bWonChallenge, ", bWonChallenge)
						PRINTLN("[NETCELEBRATION] - strPlayerName, ", strPlayerName)
						PRINTLN("[NETCELEBRATION] - strChallengeName, ", strChallengeName)
						PRINTLN("[NETCELEBRATION] - iCashWon, ", 0)
					ENDIF
					
					sCelebrationData.iEstimatedScreenDuration += CELEBRATION_SCREEN_WINNER_STAT_DISPLAY_TIME
					
					IF serverBD.iCashBet != 0
						sCelebrationData.iEstimatedScreenDuration += CELEBRATION_SCREEN_WINNER_STAT_DISPLAY_TIME
					ENDIF
					
					//1637943 - Delay all winner screens by a bit.
					sCelebrationData.iEstimatedScreenDuration += CELEBRATION_SCREEN_STAT_WIPE_TIME * 2
					
					ADD_BACKGROUND_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName, 75)

					bSetChallengeScores = TRUE
				ENDIF
				
				CDEBUG1LN(DEBUG_MISSION, "[NETCELEBRATION] Adding playlist stats:")
				CDEBUG1LN(DEBUG_MISSION, "[NETCELEBRATION] strScreenName ", strScreenName)
				CDEBUG1LN(DEBUG_MISSION, "[NETCELEBRATION] strPlayerName ", strPlayerName)
				CDEBUG1LN(DEBUG_MISSION, "[NETCELEBRATION] strChallengeName ", strChallengeName)
				CDEBUG1LN(DEBUG_MISSION, "[NETCELEBRATION] serverBD.iCashBet ", serverBD.iCashBet)
				CDEBUG1LN(DEBUG_MISSION, "[NETCELEBRATION] g_sCurrentPlayListDetails.iCashWinnings", g_sCurrentPlayListDetails.iCashWinnings)
				CDEBUG1LN(DEBUG_MISSION, "[NETCELEBRATION] iTotalBestScore ", iTotalBestScore)
				CDEBUG1LN(DEBUG_MISSION, "[NETCELEBRATION] iTotalBestTime ", iTotalBestTime)
				CDEBUG1LN(DEBUG_MISSION, "[NETCELEBRATION] sCelebrationData.iEstimatedScreenDuration ", sCelebrationData.iEstimatedScreenDuration)
				
				
				IF bSetChallengeScores
					//Still seems to end a bit early.
					sCelebrationData.iEstimatedScreenDuration += CELEBRATION_SCREEN_STAT_WIPE_TIME * 2
					
					SHOW_CELEBRATION_SCREEN(sCelebrationData, strScreenName)
					
					RESET_NET_TIMER(sCelebrationData.sCelebrationTimer)
					START_NET_TIMER(sCelebrationData.sCelebrationTimer)
					
					CDEBUG1LN(DEBUG_MISSION, "[NETCELEBRATION] CONTROL_PLAYLIST_CELEBRATION - Starting playback of winner screen.")
					CDEBUG1LN(DEBUG_MISSION, "[NETCELEBRATION] iEstimatedScreenDuration: ", sCelebrationData.iEstimatedScreenDuration)
					
					ADJUST_ESTIMATED_TIME_BASED_ON_ANIM_LENGTH(sCelebrationData)
					RESET_CELEBRATION_PRE_LOAD(sCelebrationData)
					sCelebrationData.eCurrentStage = CELEBRATION_STAGE_PLAYING
					
					RETURN FALSE
				ELSE
					CDEBUG1LN(DEBUG_MISSION, "[NETCELEBRATION] CONTROL_PLAYLIST_CELEBRATION - Not safe to display playlist end screen.")
					RESET_CELEBRATION_PRE_LOAD(sCelebrationData)
					sCelebrationData.iEstimatedScreenDuration = 0
					MP_TEXT_CHAT_DISABLE(TRUE)
					sCelebrationData.eCurrentStage = CELEBRATION_STAGE_PLAYING
					
					RETURN FALSE
				ENDIF
			ENDIF
		BREAK
		
		CASE CELEBRATION_STAGE_PLAYING
			IF bQuickCelebrationScreen
				DRAW_CELEBRATION_SCREEN_THIS_FRAME(sCelebrationData)
			ELSE				
				VECTOR vCamRot
				vCamRot = GET_FINAL_RENDERED_CAM_ROT()
			
				//Don't draw the screen if we're in skycam.
				IF ABSF(vCamRot.x) < 45.0
					
					DRAW_CELEBRATION_SCREEN_USING_3D_BACKGROUND(sCelebrationData, <<0.0, -0.5, 0.0>>, <<0.0, 0.0, 0.0>>, <<10.0, 5.0, 5.0>>)
					
					DRAW_BLACK_RECT_FOR_WINNER_SCREEN_END_TRANSITION(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), sCelebrationData.sCelebrationTimer.Timer), 
																 	 sCelebrationData.iEstimatedScreenDuration)
																	 
					//Block the switch PostFX as it comes on during the winner screen.
					ANIMPOSTFX_STOP_ALL()
					RESET_ADAPTATION(1)
				ENDIF
			ENDIF
			
			//Once the timer finishes then progress: this depends on how many elements were added to the screen.
			IF HAS_NET_TIMER_EXPIRED(sCelebrationData.sCelebrationTimer, sCelebrationData.iEstimatedScreenDuration)
				IF NOT bKeepDrawingForSkycamTransition
					sCelebrationData.eCurrentStage = CELEBRATION_STAGE_FINISHED
				ENDIF
				
				RESET_WEATHER_AND_TIME_AFTER_CELEBRATION_WINNER()
				
				IF NOT CONTENT_IS_USING_ARENA()
					IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
						VECTOR vCamCoords
						vCamCoords = GET_CAM_COORD(ciCelebrationCam)
						vCamCoords.z = 1000.0
						SET_CAM_COORD(ciCelebrationCam, vCamCoords)
						SET_ENTITY_COORDS(PLAYER_PED_ID(), vCamCoords, FALSE)
						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
						PRINTLN("[NETCELEBRATION] - put winner cam and player ped at ", vCamCoords)
					ENDIF
				ENDIF
				
				sCelebrationData.bAllowPlayerNameToggles = FALSE
				
				IF IS_PLAYER_SCTV(PLAYER_ID())
					CLEAR_SPECTATOR_OVERRIDE_COORDS()
				ENDIF
				
				MP_TEXT_CHAT_DISABLE(FALSE)
				RETURN TRUE
			ELSE
				sCelebrationData.bAllowPlayerNameToggles = TRUE
			ENDIF
		BREAK
		
		CASE CELEBRATION_STAGE_FINISHED		
			IF IS_PLAYER_SCTV(PLAYER_ID())
				CLEAR_SPECTATOR_OVERRIDE_COORDS()
			ENDIF
			MP_TEXT_CHAT_DISABLE(FALSE)
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	//1645969 - The anim needs to work if the winning player leaves half way through, their ID will be cached so we only need to check the following:
	// - The clone was created successfully.
	// - The clone is not the player.
	// - The clone is the player and the player won the race (i.e. we're not doing the fallback "Loser" screen).
	IF sCelebrationData.iDisplayingLocalPlayerAsLoser != 0
		IF DOES_ENTITY_EXIST(sCelebrationData.pedWinnerClones[0])
			IF NOT IS_PED_INJURED(sCelebrationData.pedWinnerClones[0])
				IF sCelebrationData.pedWinnerClones[0] != PLAYER_PED_ID()
				OR (sCelebrationData.pedWinnerClones[0] = PLAYER_PED_ID() AND bSuccessfullyGrabbedWinners)
					IF sCelebrationData.eCurrentStage > CELEBRATION_STAGE_SETUP
					AND sCelebrationData.eCurrentStage < CELEBRATION_STAGE_FINISHED
						IF DOES_CAM_EXIST(ciCelebrationCam)
							IF IS_CAM_RENDERING(ciCelebrationCam)
								UPDATE_CELEBRATION_WINNER_ANIMS(sCelebrationData, sCelebrationData.pedWinnerClones[0], sCelebrationData.iWinnerPlayerID, 0, FALSE, FALSE, FALSE, PAIRED_CELEBRATION_GENDER_COMBO_NOT_SET)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIf
	
	RETURN FALSE
	
ENDFUNC 


//Maintains the end leader board and celebration!
FUNC BOOL CONTROL_END_LEADER_BOARD_AND_CELEBRATION()
	//Control the leaderboard write
	IF NOT bLeaderBoardWriteCompleate
		IF CONTROL_END_LEADERBOARD_AND_CHALLENGE_WRITE()
			bLeaderBoardWriteCompleate = TRUE
			PRINTLN("[PLC] CONTROL_END_LEADERBOARD_AND_CHALLENGE_WRITE() = TRUE")
		ENDIF
	ENDIF
	

	//Hide stuff
	DISABLE_HUD_ELEMENTS_DURING_TRANSITION_SESSION_CHANGE()	
	
	//controll 
 	IF NOT bCelebrationCompleate 
		BOOL bSafeToContinue
		
		//Only display the winner screen if the player passed the mission.
		IF NOT bCelebrationSummaryComplete
			IF HAS_CELEBRATION_SUMMARY_FINISHED() //This only triggers if the player won the playlist.
				RESET_CELEBRATION_SCREEN_STATE(sCelebrationData)
						
				RESET_NET_TIMER(sCelebrationData.sCelebrationTransitionTimer)
				START_NET_TIMER(sCelebrationData.sCelebrationTransitionTimer)
				
				bCelebrationSummaryComplete = TRUE
			ENDIF
		ELSE
			IF NOT bQualificationCelebrationSummaryComplete
				PRINTLN("[Qual_Celebration] - bQualificationCelebrationSummaryComplete = FALSE")
				IF IS_PLAYLIST_IS_A_QUALIFYING_PLAYLIST()
					PRINTLN("[Qual_Celebration] - IS_PLAYLIST_IS_A_QUALIFYING_PLAYLIST = TRUE")
					IF RUN_TOURNAMENT_OUTRO_CELEBRATION_SCREEN(sJobOutroCutData)
						PRINTLN("[Qual_Celebration] - RUN_TOURNAMENT_OUTRO_CELEBRATION_SCREEN = TRUE")
						bQualificationCelebrationSummaryComplete = TRUE
					ENDIF
				ELSE
					PRINTLN("[Qual_Celebration] - IS_PLAYLIST_IS_A_QUALIFYING_PLAYLIST = FALSE")
					bQualificationCelebrationSummaryComplete = TRUE
				ENDIF
			ELSE
				IF CONTROL_PLAYLIST_CELEBRATION(TRUE)
					IF SB_PLAYLIST_SETTING_CHALLENGE_TIME()
						bSafeToContinue = TRUE
					ELSE
						IF SET_SKYSWOOP_UP(FALSE, TRUE, FALSE, DEFAULT, TRUE)
							bSafeToContinue = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	
		IF bSafeToContinue	
			//Audio scenes: see B*1642903 for implementation notes.
			IF IS_AUDIO_SCENE_ACTIVE("MP_CELEB_SCREEN_SCENE")
				STOP_AUDIO_SCENE("MP_CELEB_SCREEN_SCENE")
			ENDIF
	
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciENABLE_POWERPLAY_PICKUP_SOUNDS)
				IF NOT IS_AUDIO_SCENE_ACTIVE("MP_LEADERBOARD_SCENE")
					START_AUDIO_SCENE("MP_LEADERBOARD_SCENE")
				ENDIF
			ELSE
				IF NOT IS_AUDIO_SCENE_ACTIVE("MP_LEADERBOARD_PP_SCENE")
					START_AUDIO_SCENE("MP_LEADERBOARD_PP_SCENE")
				ENDIF
			ENDIF
		
			IF SB_PLAYLIST_SETTING_CHALLENGE_TIME()
				ANIMPOSTFX_PLAY("MinigameEndNeutral", 0, FALSE)
				PLAY_SOUND_FRONTEND(-1, "Hit_1", "LONG_PLAYER_SWITCH_SOUNDS")
			ENDIF
			
			STOP_CELEBRATION_SCREEN_ANIM(sCelebrationData, "PLAYWINNER")
			CLEANUP_CELEBRATION_SCREEN(sCelebrationData)
			SET_CELEBRATION_SCREEN_AS_ACTIVE(FALSE)
		
			IF DOES_CAM_EXIST(ciCelebrationCam)
				DESTROY_CAM(ciCelebrationCam)
			ENDIF
		
			bCelebrationCompleate = TRUE
			PRINTLN("[PLC] CONTROL_PLAYLIST_CELEBRATION = TRUE")
		ENDIF
	ENDIF
	
	//Both done then move
 	IF bLeaderBoardWriteCompleate
	AND bCelebrationCompleate
		sJobOutroCutData.iDrawTextStage = 0
		sJobOutroCutData.bRequestedCelebrationScaleform = FALSE
		RESET_NET_TIMER(sJobOutroCutData.jobIntroTextTimer)
		RETURN TRUE
	ENDIF

	RETURN FALSE 
ENDFUNC




//Set up my team for the playlist
PROC SET_UP_PLAYLIST_CONTROLLER_TEAM()
	IF serverBD.iTeamCrew[0] = -1
		PRINTLN("[PLC] SET_UP_PLAYLIST_CONTROLLER_TEAM serverBD.iTeamCrew[0] = -1")
		EXIT
	ENDIF
	PRINTLN("[PLC] SET_UP_PLAYLIST_CONTROLLER_TEAM")
	IF serverBD.iTeamCrew[0] = GET_PLAYER_CREW_INT_CL(PLAYER_ID())
		playerBD[PARTICIPANT_ID_TO_INT()].iteam = 0
	ELSE
		playerBD[PARTICIPANT_ID_TO_INT()].iteam = 1
	ENDIF
	SET_MY_PLAYLIST_TEAM(playerBD[PARTICIPANT_ID_TO_INT()].iteam)
	PRINTLN("[PLC] GET_MY_PLAYLIST_TEAM() = ", GET_MY_PLAYLIST_TEAM())
	PRINTLN("[PLC] serverBD.iTeamCrew[0]  = ", serverBD.iTeamCrew[0])
	PRINTLN("[PLC] playerBD[", PARTICIPANT_ID_TO_INT(), "].iteam	  = ", playerBD[PARTICIPANT_ID_TO_INT()].iteam)
ENDPROC


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////						Main Loop					//////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
SCRIPT(MP_MISSION_DATA fmmcMissionData) 

INT i

//  IF NOT IS_SCREEN_FADED_OUT()
//  AND NOT IS_SCREEN_FADING_OUT()
//	  DO_SCREEN_FADE_OUT(1000)
//	  PRINTLN("[PLC] DO_SCREEN_FADE_OUT(1000)")
//  ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS() 
		PROCESS_PRE_GAME(fmmcMissionData)	
//		//Write to the leaderboard with a lose so we can't cheat
//		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
//			IF SB_PLAYLIST_DOING_CHALLENGE()
//				WRITE_TO_CHALLENGE_LEADERBOARD(TRUE)
//			ENDIF
//		ENDIF
	ENDIF
	
	// Main loop.
	WHILE TRUE
	
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		SCRIPT_PROFILER_END_OF_FRAME()
		#ENDIF
		#ENDIF	
	
		//Get the cash bet from the server based on the number of players in the session
		IF g_sCurrentPlayListDetails.iCashWinnings = 0
			IF serverBD.iCashBetPlayer != 0
				g_sCurrentPlayListDetails.iCashWinnings = serverBD.iCashBetPlayer
				PRINTLN("[PLC] serverBD.iCashBetPlayer                 = ", serverBD.iCashBetPlayer)
				PRINTLN("[PLC] g_sCurrentPlayListDetails.iCashWinnings = ", g_sCurrentPlayListDetails.iCashWinnings)
			ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
		
			DEBUG_RENDER_PLAYLIST_LEADERBOARD()
		
			IF bWidgetsCreated = FALSE
				IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(PLAYER_ID())
					CREATE_WIDGETS()
					bWidgetsCreated = TRUE
				ENDIF
			ENDIF
			IF  bKillScript
				SCRIPT_CLEANUP()
			ENDIF
			IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
				IF IS_DEBUG_KEY_JUST_PRESSED(KEY_NUMPAD1, KEYBOARD_MODIFIER_SHIFT, "ONE PLAYER")
					 IF serverBD.bOnePlayer
						serverBD.bOnePlayer = FALSE
					ELSE
						serverBD.bOnePlayer = TRUE
					ENDIF
				ENDIF
			ENDIF
		#ENDIF
		
		// One wait to rule them all. This can be the ONLY wait from here on in.....
		MP_LOOP_WAIT_ZERO() 
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		SCRIPT_PROFILER_START_OF_FRAME()
		#ENDIF
		#ENDIF	
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
		OPEN_SCRIPT_PROFILE_MARKER_GROUP("ply client stuff") 
		#ENDIF
		#ENDIF
		
		IF NETWORK_IS_GAME_IN_PROGRESS()
			MAINTAIN_SPEC_NOT_HOST(iManageSpechost, iDesiredHost)
		ENDIF
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_SPEC_NOT_HOST") 
		#ENDIF
		#ENDIF
		
		//NETWORK_PREVENT_SCRIPT_HOST_MIGRATION()
			
		//if we quit then kill the playlist
		IF IS_FORCE_ABANDON_CURRENT_MP_MISSION_SET()
			//Set that we are quitting a tournament if that's what's happening
			IF IS_THIS_TRANSITION_SESSION_IS_A_TOURNAMENT_PLAYLIST()
				SET_I_AM_QUITTING_A_TRANSITION_SESSION_IS_A_TOURNAMENT_PLAYLIST()
			ENDIF
			SET_PLAYER_ON_A_PLAYLIST(FALSE) 
			CLEAR_LEAVE_MISSION_WITH_EVERYBODY()
			SET_UGC_PLAYER_DATA_PLAYER_QUIT_MISSION(nTYPE)
			IF SB_IS_QUALIFYING_PLAYLIST()
				g_sQualifyTourFeed.bFailedToSetTime = TRUE
				g_sQualifyTourFeed.bSetTime = FALSE		
				g_sQualifyTourFeed.iTotalTime = 0
			ENDIF
			SCRIPT_CLEANUP()
		ENDIF
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("SCRIPT_CLEANUP 1") 
		#ENDIF
		#ENDIF
		
		// If we have a match end event, bail.
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			PRINTLN("[PLC] SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE = TRUE")
			CLEAR_LEAVE_MISSION_WITH_EVERYBODY()
			SET_UGC_PLAYER_DATA_PLAYER_QUIT_MISSION(nTYPE)
			SCRIPT_CLEANUP()
		ENDIF
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("SCRIPT_CLEANUP 2") 
		#ENDIF
		#ENDIF
		
		// If we have a match end event, bail.
		IF NOT IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID())
			//If the playlist is not restarting
			IF playerBD[PARTICIPANT_ID_TO_INT()].iGameState != GAME_STATE_PLAYLIST_RESTART
				PRINTLN("[PLC] IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID()) = FALSE")
				CLEAR_LEAVE_MISSION_WITH_EVERYBODY()
				SCRIPT_CLEANUP()
			ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("SCRIPT_CLEANUP 3") 
		#ENDIF
		#ENDIF
		
		IF SHOULD_PLAYER_LEAVE_MP_MISSION() 
			SET_TRANSITION_SESSIONS_QUIT_CURRENT_PLAYLIST()
			PRINTLN("[PLC] SHOULD_PLAYER_LEAVE_MP_MISSION = TRUE")
			CLEAR_LEAVE_MISSION_WITH_EVERYBODY()
			SCRIPT_CLEANUP()
		ENDIF
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("SCRIPT_CLEANUP 4") 
		#ENDIF
		#ENDIF
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
		CLOSE_SCRIPT_PROFILE_MARKER_GROUP() 
		#ENDIF
		#ENDIF
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
		OPEN_SCRIPT_PROFILE_MARKER_GROUP("ply client stuff") 
		#ENDIF
		#ENDIF
		
		//All player stuff in here
		SWITCH playerBD[PARTICIPANT_ID_TO_INT()].iGameState 
		
			CASE GAME_STATE_INI		 
				IF GET_MY_PLAYLIST_TEAM() = -1
				AND SB_PLAYLIST_DOING_HEAD_TO_HEAD()
					SET_UP_PLAYLIST_CONTROLLER_TEAM()
				ELIF ServerBD.iGameState> GAME_STATE_INI
					SET_FM_PLAYLIST_PLAYER_GAME_STATE(playerBD, GAME_STATE_RUNNING)
					GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].bHasPlayerVoted = FALSE							
				ENDIF
				
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE 
					ADD_SCRIPT_PROFILE_MARKER("GAME_STATE_INI") 
				#ENDIF
				#ENDIF
			BREAK
			
			//Control the play list.
			CASE GAME_STATE_RUNNING
				IF ServerBD.iGameState < GAME_STATE_END_LEADER_BOARD
					CONTROL_PLAYLIST()		 
					
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE 
						ADD_SCRIPT_PROFILE_MARKER("CONTROL_PLAYLIST") 
					#ENDIF
					#ENDIF
				ELSE
					
					// Set the playlist global as finished
					SET_TRANSITION_SESSIONS_PLAYLIST_HAS_FINISHED()
				
					//Hide stuff
					DISABLE_HUD_ELEMENTS_DURING_TRANSITION_SESSION_CHANGE()	
					INITIALISE_CELEBRATION_SCREEN_DATA(sCelebrationData)
					SET_FM_PLAYLIST_PLAYER_GAME_STATE(playerBD, GAME_STATE_END_LEADER_BOARD)
				ENDIF
				#IF IS_DEBUG_BUILD
				IF bBeatChallenge
				OR bSetATime
					PRINTLN("[PLC] *******************************************************************")
					PRINTLN("[PLC] *****														 *****")
					PRINTLN("[PLC] *****														 *****")
					IF bBeatChallenge
						PRINTLN("[PLC] *****				  bBeatChallenge				   *****")
					ELSE
						PRINTLN("[PLC] *****					   bSetATime						 *****")
					ENDIF
					PRINTLN("[PLC] *****														 *****")
					PRINTLN("[PLC] *****														 *****")
					PRINTLN("[PLC] *******************************************************************")
					SET_FM_PLAYLIST_PLAYER_GAME_STATE(playerBD, GAME_STATE_END_LEADER_BOARD)
				ENDIF
				#ENDIF
				
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE 
					ADD_SCRIPT_PROFILE_MARKER("GAME_STATE_RUNNING") 
				#ENDIF
				#ENDIF
			BREAK
			
			CASE GAME_STATE_END_LEADER_BOARD
				IF CONTROL_END_LEADER_BOARD_AND_CELEBRATION()
					#IF IS_DEBUG_BUILD
						PRINTLN("[PLC] *******************************************************************")
						PRINTLN("[PLC] *****														 *****")
						PRINTLN("[PLC] *****														 *****")
						PRINTLN("[PLC] *****				  PLAYLIST_HAS_FINISHED				  *****")
						IF serverBD.bRestartPlayList
						PRINTLN("[PLC] *****					RESTARTING = TRUE					*****")
						ELSE
						PRINTLN("[PLC] *****					RESTARTING = FALSE				   *****")
						ENDIF
						PRINTLN("[PLC] *****														 *****")
						PRINTLN("[PLC] *****														 *****")
						PRINTLN("[PLC] *******************************************************************")
					#ENDIF		  
					//Deal with saving stats
					//Only is the playlist should
					IF SHOULD_PLAYLIST_SAVE_STATS()
						IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM //Only save if actually in FM, don't save in teh creator. BC: 19/03/2014
						AND NOT IS_TRANSITION_ACTIVE() //Don't bother if the transition is running, that will handle saving. BC 11/06/2014 bug 1885657
							REQUEST_SAVE(SSR_REASON_INSTANCED_CONTENT, STAT_SAVETYPE_END_MATCH)
						ENDIF
						CLEAR_PLAYLIST_SAVE_STATS()
					ENDIF
					
					CLEANUP_LEADERBOARD_CAM()				   
					//If the playlist is restarting
					IF serverBD.bRestartPlayList
						CLEAR_PLAYLIST_DONE_FIRST_MISSION()
						CLEAR_PLAYLIST_HAS_BEEN_SETUP()
						SET_FM_PLAYLIST_PLAYER_GAME_STATE(playerBD, GAME_STATE_PLAYLIST_RESTART)
						CLEAR_PLAYLIST_HAS_DONE_INITIAL_TRANSITION()
						CLEAR_PLAYLIST_MISSION_HAS_LOADED()
						CLEAR_TRANSITION_SESSIONS_BITSET()
						RESET_TRANSITION_SESSION_NON_RESET_VARS()   
						SET_PLAYLIST_IS_RESTARTING()
					//If's ended and people don't want to restart   
					ELSE
						SET_FM_PLAYLIST_PLAYER_GAME_STATE(playerBD, GAME_STATE_PLAYLIST_RESTART)
					ENDIF
				ENDIF
				
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE 
					ADD_SCRIPT_PROFILE_MARKER("GAME_STATE_END_LEADER_BOARD") 
				#ENDIF
				#ENDIF
			BREAK
			
			CASE GAME_STATE_PLAYLIST_RESTART
				IF ARE_ALL_PLAYERS_READY()
					IF serverBD.bRestartPlayList
						SET_FM_PLAYLIST_PLAYER_GAME_STATE(playerBD, GAME_STATE_END)
						SCRIPT_CLEANUP()
					ELSE
						SCRIPT_CLEANUP()						
					ENDIF
				ENDIF	
				
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE 
					ADD_SCRIPT_PROFILE_MARKER("GAME_STATE_PLAYLIST_RESTART") 
				#ENDIF
				#ENDIF
			BREAK
			
			CASE GAME_STATE_END			 
				SET_LEAVE_MISSION_WITH_EVERYBODY()
				SET_TRANSITION_SESSIONS_RUN_FREEMODE_RELAUNCH()
				SET_DO_TRANSITION_TO_GAME()
				SCRIPT_CLEANUP()
				
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE 
					ADD_SCRIPT_PROFILE_MARKER("GAME_STATE_END") 
				#ENDIF
				#ENDIF
			BREAK
			
		ENDSWITCH
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
		CLOSE_SCRIPT_PROFILE_MARKER_GROUP() 
		#ENDIF
		#ENDIF
		
		//All server stuff in here. 
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
			OPEN_SCRIPT_PROFILE_MARKER_GROUP("ply server stuff") 
			#ENDIF
			#ENDIF
		
			SET_UP_PLAYLIST_TO_LOAD()
			
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
				ADD_SCRIPT_PROFILE_MARKER("SET_UP_PLAYLIST_TO_LOAD") 
			#ENDIF
			#ENDIF
			
			MAINTAIN_SYNCING_CHALLENGE_NAME()
			
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
				ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_SYNCING_CHALLENGE_NAME") 
			#ENDIF
			#ENDIF
			
			//Set the job chain
			IF serverBD.iMacaddresshash = 0
			OR serverBD.iPosixtime = 0
				IF g_TransitionSessionNonResetVars.sJobChain.iMacaddresshash != 0
				AND g_TransitionSessionNonResetVars.sJobChain.iPosixtime != 0
					serverBD.iMacaddresshash = g_TransitionSessionNonResetVars.sJobChain.iMacaddresshash
					serverBD.iPosixtime = g_TransitionSessionNonResetVars.sJobChain.iPosixtime
					PRINTLN("[PLC] serverBD.iPosixtime				  = ", serverBD.iPosixtime)
					PRINTLN("[PLC] serverBD.iMacaddresshash			  = ", serverBD.iMacaddresshash)
				ENDIF
			ENDIF
			
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
				ADD_SCRIPT_PROFILE_MARKER("JOB_CHAIN") 
			#ENDIF
			#ENDIF
			
			//All player stuff in here
			SWITCH ServerBD.iGameState  
				//Set stuff up
				CASE GAME_STATE_INI
				
					//serverbd.iCurrentPlayListPosition = 0
					SET_FM_PLAYLIST_SERVER_GAME_STATE(serverBD, GAME_STATE_RUNNING)
					
					FOR i = 0 TO (NUM_NETWORK_PLAYERS-1)
						serverBD_LB.sleaderboard[i].iTeam =-1
						serverBD_LB.sleaderboard[i].iparticipant =-1
						serverBD_LB.sleaderboard[i].playerID = INVALID_PLAYER_INDEX()
						PRINTLN("[CS_PLY_ID] INVALID_PLAYER_INDEX, GAME_STATE_INI ")
					ENDFOR
					
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE 
						ADD_SCRIPT_PROFILE_MARKER("GAME_STATE_INI") 
					#ENDIF
					#ENDIF
					
				BREAK
				
				//Control the play list.
				CASE GAME_STATE_RUNNING
					//If the playlist has come to an end
		 
					//And the leaderboard has finished
					IF IS_BIT_SET(serverBD.iLBBitset,ci_FINISH_LB)
					AND ( HAS_PLAY_LIST_FINISHED() OR HAS_CHALLENGE_BEEN_FAILED() )		
						SERVER_CLEAR_LEADERBOARD_DATA_END_OF_PLAYLIST()
						//Tell logs what happened.
						#IF IS_DEBUG_BUILD
						IF HAS_CHALLENGE_BEEN_FAILED()
							PRINTLN("[PLC] HAS_CHALLENGE_BEEN_FAILED = TRUE")
						ELSE
							PRINTLN("[PLC] HAS_PLAY_LIST_FINISHED = TRUE")
						ENDIF
						#ENDIF
						SET_FM_PLAYLIST_SERVER_GAME_STATE(serverBD, GAME_STATE_END_LEADER_BOARD)
					ELSE
						SERVER_HANDLE_CLIENT_VOTING()
						
						#IF IS_DEBUG_BUILD
						#IF SCRIPT_PROFILER_ACTIVE 
							ADD_SCRIPT_PROFILE_MARKER("SERVER_HANDLE_CLIENT_VOTING") 
						#ENDIF
						#ENDIF
						
						
						SERVER_INIT_PLAYLIST_LEADERBOARD()
						
						#IF IS_DEBUG_BUILD
						#IF SCRIPT_PROFILER_ACTIVE 
							ADD_SCRIPT_PROFILE_MARKER("SERVER_INIT_PLAYLIST_LEADERBOARD") 
						#ENDIF
						#ENDIF
					ENDIF
				BREAK
				
				CASE GAME_STATE_END_LEADER_BOARD
					SERVER_HANDLE_CLIENT_VOTING()
					
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE 
						ADD_SCRIPT_PROFILE_MARKER("SERVER_HANDLE_CLIENT_VOTING") 
					#ENDIF
					#ENDIF
					
					SERVER_HANDLE_CLIENT_READY_CHECK()
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE 
						ADD_SCRIPT_PROFILE_MARKER("SERVER_HANDLE_CLIENT_READY_CHECK") 
					#ENDIF
					#ENDIF
				BREAK
				
				CASE GAME_STATE_END
				
				BREAK	
				
			ENDSWITCH
			
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
			CLOSE_SCRIPT_PROFILE_MARKER_GROUP() 
			#ENDIF
			#ENDIF
			
		#IF IS_DEBUG_BUILD
		ELSE
			IF iPosixtimeDebug != serverBD.iPosixtime
			OR iMacaddresshashDebug != serverBD.iMacaddresshash
				PRINTLN("[PLC] serverBD.iPosixtime				  = ", serverBD.iPosixtime)
				PRINTLN("[PLC] serverBD.iMacaddresshash			  = ", serverBD.iMacaddresshash)
				iPosixtimeDebug = serverBD.iPosixtime
				iMacaddresshashDebug = serverBD.iMacaddresshash
			ENDIF
		#ENDIF // #IF IS_DEBUG_BUILD
		
		ENDIF
		
	ENDWHILE
	
// End of Mission
ENDSCRIPT

// notes
// SERVER_POPULATE_PLAYLIST_LB_DATA
// SERVER_SORT_LEADERBOARD_PLAYERS
// 2233911 search up


