//////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        FREEMODE mission launcher/blip controller 											//
// Description: Controls the custom missions in free mode that have been created with the creator	//
// Written by:  Robert Wright/Rowan Cockcroft														//
// Date: 15/11/2012																					//
//////////////////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"
//Game Headers
USING "commands_network.sch"
//Network Headers
USING "net_mission.sch"
USING "net_include.sch" 
//Freemode Headers
USING "net_common_functions.sch"
USING "fm_in_corona_header.sch"
USING "net_transition_sessions.sch"
USING "transition_common.sch"
USING "streamed_scripts.sch"
USING "fmmc_corona_controller.sch"
USING "net_wait_zero.sch"
USING "commands_audio.sch"
USING "script_misc.sch"
USING "net_corona_V2.sch"
#IF IS_DEBUG_BUILD
USING "profiler.sch"
#ENDIF

USING "net_arena_career_wall.sch"

//Const ints
CONST_INT GAME_STATE_INI						0
CONST_INT GAME_STATE_RUNNING					1
CONST_INT GAME_STATE_SET_GLOBALS				2
CONST_INT GAME_STATE_SET_WAIT					3
CONST_INT GAME_STATE_END                        4

//Delay timer
CONST_INT WAIT_TIME_FOR_EVERYONE_TO_JOIN_SCRIPT 10
//Server bit sets
CONST_INT SERV_BITSET_STOP_TEAM_BALANCING		0
//Client bit sets
CONST_INT CLIENT_BITSET_SETUP_TEAM_ROLES		0
CONST_INT CLIENT_BITSET_SETUP_RALLY_DRIVER		1
CONST_INT CLIENT_BITSET_SETUP_RALLY_PASSENGER	2
CONST_INT CLIENT_BITSET_GOT_TEAM				3

// ** Corona Map Constants and Variables **
CONST_INT CORONA_MAP_STATE_INITIALISE		0
CONST_INT CORONA_MAP_STATE_MAINTAIN			1
CONST_INT CORONA_MAP_STATE_VOID				2

INT localCoronaMapStage = CORONA_MAP_STATE_INITIALISE
FLOAT localCoronaMapX
FLOAT localCoronaMapY
BOOL doForcedSizeTeamBalancing
BOOL bAudioSceneEnabled

// States of our audio scenes during a heist corona
CONST_INT ciCORONA_AMBIENT_AUDIO_STATE_WAIT_FOR_LAUNCH				0
CONST_INT ciCORONA_AMBIENT_AUDIO_STATE_SUPPRESS_FREEZE_OF_MIC		1
CONST_INT ciCORONA_AMBIENT_AUDIO_STATE_BRING_IN_RADIO				2
CONST_INT ciCORONA_AMBIENT_AUDIO_STATE_WAIT_FOR_CUT_POST_LAUNCH		3
CONST_INT ciCORONA_AMBIENT_AUDIO_STATE_WAIT_FOR_END_OF_CORONA		4


INT iArenaWarsTvJobIndex = -1
// Our state and flag to indicate when freeze microphone has stopped
INT coronaAmbientAudioState	= ciCORONA_AMBIENT_AUDIO_STATE_WAIT_FOR_LAUNCH
BOOL bFreezingMicHasStopped = FALSE
BOOL bTeamsReaCapped
INT iMaxNumPlayersPerTeamLocal[FMMC_MAX_TEAMS]
INT iAlpha
// The server broadcast data.
// Everyone can read this data, only the server can update it. 
STRUCT ServerBroadcastData
	INT				iGameState	
	//INT 			iInstanceId
	//BOOL 			bTutorial
	BOOL 			bForceSmallerTeams
	BOOL 			bPlayListCorona
	BOOL 			bSkipBalance
	INT 			iMissionType
	INT 			iMaxNumperOfPart
	INT  			iMissionSubType
	INT 			iNumberOfTeams
	INT 			iTeamBalancing	 = DM_TEAM_BALANCING_ON
	INT 			iPlayerCount
	// TRANSITION SESSIONS
	INT 			iBalanceProgress
	INT 			iFindLargestProgress
	INT 			iTempBalancedTeam[MAX_NUM_MC_PLAYERS]
	BOOL 			bNotBiggest[MAX_TB_TEAMS]
	BOOL 			bAlreadyUsed[MAX_TB_TEAMS]
	INT 			iTeamOrder[MAX_TB_TEAMS]
	INT 			iCurrentDMTeam
	INT 			iteamBalanceCount[MAX_TB_TEAMS]
	BOOL 			bTeamSizeSorted
	VECTOR 			vImpromptuCoord
	//SCRIPT_TIMER 	tdJoinTimer
	INT 			iBalancedTeam[MAX_NUM_MC_PLAYERS]
	INT 			iServerBitSet
	BOOL			bTeamsSplit
	BOOL 			bTeamGtaRace
	INT				iDivideProgress
	INT				iNumberOnEachTeam
	INT				iCurrentTeam
	
	#IF IS_DEBUG_BUILD
	BOOL  			bSituationb
	BOOL 			bBug1708849
	BOOL			bBug1861235
	//INT 			iScenario	= -1
	BOOL 			bForceSameTeamGTA
	#ENDIF
ENDSTRUCT
ServerBroadcastData serverBD

// The client broadcast data.
// Everyone can read this data, only the local player only the server can update it. 
STRUCT PlayerBroadcastData
	//Main game state
	INT 			iCrewTeam = -1
	INT 			iMenuTeam
	INT 			iPPrace
	INT 			iProle	
	GAMER_HANDLE	gHandle
	PLAYER_INDEX	playerTeammate
	#IF IS_DEBUG_BUILD
	INT	iPlayerTeammate
	#ENDIF
	INT 			iTeammate
ENDSTRUCT
PlayerBroadcastData playerBD[NUM_NETWORK_PLAYERS]

#IF IS_DEBUG_BUILD
STRUCT DEBUG_TRANSITION_VARIABLES
	INT iMissionType = 2
	INT iNumberOfTeams = 0
	BOOL bPlayListCorona = FALSE
	BOOL bForceSmallerTeams = FALSE
	INT iMissionSubType = 2
	INT iTeamBalancing = 1
	INT iNumberOfJoinedPlayers = 5
ENDSTRUCT
#ENDIF

//All the local vars in one struct
STRUCT LOCAL_VARS_STRUCT
	INT iTeamsCountBalancing
	INT iPAUSED_RENDERPHASES_Timer
	INT iGameState			= GAME_STATE_INI
	INT iLocalBitSet
	BOOL bSET_USE_HI_DOFSetUp
	BOOL bPAUSED_RENDERPHASES
	BOOL bSetUpSpinner
	BOOL bLoadSceneStarted
	BOOL bStillNeedToSetFocus
	INT iTutorialCameraProg
	VECTOR vFocusPos
	BOOL bOnCallCameraSetUp
	SCRIPT_TIMER timeTutCameraProg
	
	BOOL bRunningMissionCut	// Dave W, for CnC
	
	#IF IS_DEBUG_BUILD
	BOOL bForceRallyTeams
	DEBUG_TRANSITION_VARIABLES debugTransitionVars
	#ENDIF
ENDSTRUCT
LOCAL_VARS_STRUCT sLocalVars

BOOL bAlreadyMovingFirstPlayer

//INT iMyPartID

#IF IS_DEBUG_BUILD

BOOL bKillScript
WIDGET_GROUP_ID wgGroup
PROC CREATE_WIDGETS()
	wgGroup= wgGroup
	//INT iMyPartID = NATIVE_TO_INT(PLAYER_ID())	
	wgGroup= START_WIDGET_GROUP("  maintain_transition_players") 
		ADD_WIDGET_BOOL("Bug - 2068523", ServerBD.bBug1861235)
		ADD_WIDGET_INT_SLIDER("iHighPriorityTeamForBalancing", g_FMMC_STRUCT.iHighPriorityTeamForBalancing, 0, 9999999, 1)
		
		ADD_WIDGET_BOOL("bForceSameTeamGTA", serverBD.bForceSameTeamGTA)
		ADD_WIDGET_BOOL("g_IgnoreTeamGTARaceCoronaRules", g_IgnoreTeamGTARaceCoronaRules)
		ADD_WIDGET_BOOL("g_b4357710", g_b4357710)
		ADD_WIDGET_BOOL("bKillScript", bKillScript)
		
		ADD_WIDGET_BOOL("bForceRallyTeams", sLocalVars.bForceRallyTeams)
		ADD_WIDGET_BOOL("bForceSmallerTeams", ServerBD.bForceSmallerTeams)
		ADD_WIDGET_BOOL("bSituationb", ServerBD.bSituationb)
		ADD_WIDGET_BOOL("bBug1708849", ServerBD.bBug1708849)
		
		ADD_WIDGET_INT_SLIDER("ServerBD.iGameState		", ServerBD.iGameState, -10, 10000, 1)
		ADD_WIDGET_INT_SLIDER("iGameState	", sLocalVars.iGameState	, -10, 10000, 1)
		TEXT_LABEL_15 tl15
		INT i
		START_WIDGET_GROUP("Transition Members")
			ADD_WIDGET_INT_SLIDER("iNumberOfJoinedPlayers", g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers, 0, NUM_NETWORK_PLAYERS, 1)
			FOR i = 0 TO (NUM_NETWORK_PLAYERS - 1)
				tl15 = "Transition["
				tl15 += i 
				tl15 += "]"
				START_WIDGET_GROUP(tl15)
					ADD_WIDGET_INT_SLIDER("Team/Part", g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[i].iPlayerOptions[ciRC_CLIENT_OPTION_PARTNER], -1, NUM_NETWORK_PLAYERS, 1)
					ADD_WIDGET_INT_READ_ONLY("Role     ", g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[i].iPlayerOptions[ciRC_CLIENT_OPTION_ROLE])
					ADD_WIDGET_INT_SLIDER("Crew ID  ", g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[i].iCrewID, 0, HIGHEST_INT, 1)
					ADD_WIDGET_INT_SLIDER("Friends  ", g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[i].iPlayerFriends, 0, HIGHEST_INT, 1)
					ADD_WIDGET_INT_SLIDER("Party    ", g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[i].iPlayerPartyMembers, 0, HIGHEST_INT, 1)
				STOP_WIDGET_GROUP()
			ENDFOR		
		STOP_WIDGET_GROUP()
		FOR i = 0 TO (NUM_NETWORK_PLAYERS - 1)			
			tl15 = "Part "
			tl15 += i 
			START_WIDGET_GROUP(tl15)
				ADD_WIDGET_INT_SLIDER("iMenuTeam", playerBD[i].iMenuTeam, -10, 10000, 1)
				ADD_WIDGET_INT_SLIDER("iPPrace", playerBD[i].iPPrace, -10, 10000, 1)
				ADD_WIDGET_INT_SLIDER("iProle	", playerBD[i].iProle	, -10, 10000, 1)
				ADD_WIDGET_INT_SLIDER("iTeammate", playerBD[i].iTeammate, -10, 10000, 1)
				ADD_WIDGET_INT_SLIDER("iPlayerTeammate", playerBD[i].iPlayerTeammate, -10, 10000, 1)
			STOP_WIDGET_GROUP()
		ENDFOR
		
		#IF SCRIPT_PROFILER_ACTIVE
			CREATE_SCRIPT_PROFILER_WIDGET()
		#ENDIF
		
	STOP_WIDGET_GROUP()
ENDPROC		

PROC DO_BALANCING_DEBUG()
	IF sLocalVars.bForceRallyTeams
		//Mission type, race, DM, mission
		serverBD.iMissionType           = sLocalVars.debugTransitionVars.iMissionType
		//number of teams (for Dm and missions)
		serverBD.iNumberOfTeams         = sLocalVars.debugTransitionVars.iNumberOfTeams
		//playlist corona
		serverBD.bPlayListCorona        = sLocalVars.debugTransitionVars.bPlayListCorona
		//force small teams
		serverBD.bForceSmallerTeams     = sLocalVars.debugTransitionVars.bForceSmallerTeams
		//mission sub type
		serverBD.iMissionSubType   		 = sLocalVars.debugTransitionVars.iMissionSubType						
		//team balancing 0 = off 1 = on
		serverBD.iTeamBalancing   		 = sLocalVars.debugTransitionVars.iTeamBalancing
		//Number of players
		g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers = sLocalVars.debugTransitionVars.iNumberOfJoinedPlayers
	ENDIF			
ENDPROC
#ENDIF

PROC SET_CLIENT_GAME_STATE(INT iState)
	#IF IS_DEBUG_BUILD
		SWITCH iState
			CASE GAME_STATE_INI 	
				PRINTLN("[TEAM BAL DATA] - SET_CLIENT_GAME_STATE - GAME_STATE_INI         - at the UNIX TIME - (", GET_CLOUD_TIME_AS_INT(),")")
			BREAK
			CASE GAME_STATE_RUNNING	
				PRINTLN("[TEAM BAL DATA] - SET_CLIENT_GAME_STATE - GAME_STATE_RUNNING     - at the UNIX TIME - (", GET_CLOUD_TIME_AS_INT(),")")
			BREAK
			CASE GAME_STATE_END		
				PRINTLN("[TEAM BAL DATA] - SET_CLIENT_GAME_STATE - GAME_STATE_END         - at the UNIX TIME - (", GET_CLOUD_TIME_AS_INT(),")")
			BREAK
			CASE GAME_STATE_SET_GLOBALS
				PRINTLN("[TEAM BAL DATA] - SET_CLIENT_GAME_STATE - GAME_STATE_SET_GLOBALS - at the UNIX TIME - (", GET_CLOUD_TIME_AS_INT(),")")
			BREAK
			CASE GAME_STATE_SET_WAIT
				PRINTLN("[TEAM BAL DATA] - SET_CLIENT_GAME_STATE - GAME_STATE_SET_WAIT    - at the UNIX TIME - (", GET_CLOUD_TIME_AS_INT(),")")
			BREAK
		ENDSWITCH
	#ENDIF

	sLocalVars.iGameState = iState
ENDPROC

FUNC BOOL IS_THIS_A_TEAM_MISSION_TYPE(ServerBroadcastData &SBDpassed)
	IF SBDpassed.iMissionType = FMMC_TYPE_MISSION
	OR SBDpassed.iMissionType = FMMC_TYPE_DEATHMATCH
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_THIS_A_RACE_TYPE(ServerBroadcastData &SBDpassed)
	RETURN (SBDpassed.iMissionType = FMMC_TYPE_RACE)
ENDFUNC

FUNC BOOL IS_THIS_A_DEATHMATCH_TYPE(ServerBroadcastData &SBDpassed)
	RETURN (SBDpassed.iMissionType = FMMC_TYPE_DEATHMATCH)
ENDFUNC

/// PURPOSE:
///    Setup our audio scene for the corona
PROC SET_TRANSITION_SESSION_AUDIO_SCENE(FMMC_CORONA_DATA_MAINTAIN_SCRIPT_VARS &fmmcCoronaMissionData, BOOL bActive = TRUE, BOOL bForceOn = FALSE)
	
	IF fmmcCoronaMissionData.bIsHeistMission
		
		
		PRINTLN("[CORONA] SET_TRANSITION_SESSION_AUDIO_SCENE - Scene: DLC_MPHEIST_LOBBY_SCENE, bActive = ", PICK_STRING(bActive, "TRUE", "FALSE"))
		
		IF (NOT IS_TRANSITION_SESSIONS_IS_FOR_HEIST_CUTSCENE() 
			AND NOT IS_CORONA_BIT_SET(CORONA_GLOBAL_HEIST_FINALE_FLAG))
		OR (bForceOn)
			
			PRINTLN("[CORONA] SET_TRANSITION_SESSION_AUDIO_SCENE - Enabling the scene: DLC_MPHEIST_LOBBY_SCENE, bForceOn = ", PICK_STRING(bForceOn, "TRUE", "FALSE"))
			IF bActive
			
				bAudioSceneEnabled = TRUE
				IF NOT IS_AUDIO_SCENE_ACTIVE("DLC_MPHEIST_LOBBY_SCENE") 
					START_AUDIO_SCENE("DLC_MPHEIST_LOBBY_SCENE") 
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT bActive
			
			IF IS_AUDIO_SCENE_ACTIVE("DLC_MPHEIST_LOBBY_SCENE") 
				PRINTLN("[CORONA] SET_TRANSITION_SESSION_AUDIO_SCENE - Disabling the scene: DLC_MPHEIST_LOBBY_SCENE")
				
				STOP_AUDIO_SCENE("DLC_MPHEIST_LOBBY_SCENE")
			ENDIF
		ENDIF
	ELSE
		IF bActive
			
			bAudioSceneEnabled = TRUE
			IF IS_ARENA_WARS_JOB()
			#IF FEATURE_CASINO_HEIST
			OR CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_MISSION_FINALE()
			#ENDIF
			#IF FEATURE_HEIST_ISLAND
			OR HEIST_ISLAND_FLOW_IS_CURRENT_MISSION_HEIST_ISLAND_MISSION_FINALE()
			#ENDIF
				IF NOT IS_AUDIO_SCENE_ACTIVE("MP_LOBBY_ALLOW_SPEECH_SCENE") 
					START_AUDIO_SCENE("MP_LOBBY_ALLOW_SPEECH_SCENE")
					SET_CORONA_BIT(CORONA_ARENA_WARS_LOBBY_AUDIO_SCENE_ACTIVE)
				ENDIF
			ELSE
				IF NOT IS_AUDIO_SCENE_ACTIVE("MP_LOBBY_SCENE") 
					START_AUDIO_SCENE("MP_LOBBY_SCENE") 
				ENDIF
			ENDIF
		ELSE
			IF IS_CORONA_BIT_SET(CORONA_ARENA_WARS_LOBBY_AUDIO_SCENE_ACTIVE)
				IF IS_AUDIO_SCENE_ACTIVE("MP_LOBBY_ALLOW_SPEECH_SCENE") 
					STOP_AUDIO_SCENE("MP_LOBBY_ALLOW_SPEECH_SCENE")
				ENDIF
				CLEAR_CORONA_BIT(CORONA_ARENA_WARS_LOBBY_AUDIO_SCENE_ACTIVE)
			ELSE		
				IF IS_AUDIO_SCENE_ACTIVE("MP_LOBBY_SCENE") 
					STOP_AUDIO_SCENE("MP_LOBBY_SCENE") 
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//Cleanup Mission Data
PROC SCRIPT_CLEANUP(INT iMissionType, FMMC_CORONA_DATA_MAINTAIN_SCRIPT_VARS &fmmcCoronaMissionData)
	
	IF NOT AM_I_TRANSITION_SESSIONS_STARTING_ON_CALL()
		CLEANUP_CORONA_MAP()
	ENDIF
	
	IF NOT IS_AN_AIRLOCK_STRAND_TRANSITION_RUNNING()
		DISPLAY_RADAR(TRUE)
	ENDIF
	
	PRINTLN("[CORONA] CLEAR_GPS_FLAGS()")	
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_SoloStrandNoFade")
		IF DOES_CAM_EXIST(g_sTransitionSessionData.ciPedCam)
			SET_CAM_MOTION_BLUR_STRENGTH(g_sTransitionSessionData.ciPedCam, 0.0)
		ENDIF
		IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
			ANIMPOSTFX_STOP("MP_job_load")	
		ENDIF
		IF DOES_CAM_EXIST(g_sTransitionSessionData.ciCam)
			SET_CAM_MOTION_BLUR_STRENGTH(g_sTransitionSessionData.ciCam, 0.0)
		ENDIF
		IF NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
			ANIMPOSTFX_STOP("MP_job_load")
		ENDIF
		SET_GAMEPLAY_CAM_MAX_MOTION_BLUR_STRENGTH_THIS_UPDATE(0.0)	
		iMissionType = -1
		SET_SKYFREEZE_CLEAR(TRUE)
	ENDIF
	#ENDIF
	CLEAN_UP_INTRO_STAGE_CAM(g_sTransitionSessionData.ciCam,iMissionType)
	CLEAN_UP_FM_RESTART_SCRIPTS()
	PRINTLN("[TEAM BAL DATA] - SCRIPT_CLEANUP - FM_maintain_transition_players.sc")
	CLEAR_TEAM_BALANCING_SHOULD_RUN()
	CLEAR_TRANSITION_SESSIONS_CORONA_CONTROLLER_IS_RUNNING()
	CLEAR_TRANSITION_SESSION_TEAM_BALANCING_SHOULD_RUN()
	CLEAR_TRANSITION_GOT_END_LAUNCH_EVENT_FOR_MAINTAIN_SCRIPT()	 
	CLEAR_ON_CALL_TRANSITION_SESSION_SETUP_CAMERA()
	CLEAR_TRANSITION_SESSIONS_CORONA_CONTROLLER_MAINTAIN_CAMERA()
	
	SET_TRANSITION_SESSION_AUDIO_SCENE(fmmcCoronaMissionData, FALSE)
	CLEAR_TRANSITION_SESSIONS_CLEANUP_CORONA_SCRIPT()
	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
	
	IF g_sArenaBgVars.iCoronaLobbyVideoStage != ciCORONA_LOBBY_VIDEO_STAGE_IDLE
		STOP_ARENA_WARS_TV_LOBBY_PLAYLIST()
		PRINTLN("[TS] [CORONA_LOBBY_VIDEO] SCRIPT_CLEANUP - g_sArenaBgVars.iCoronaLobbyVideoStage = ciCORONA_LOBBY_VIDEO_STAGE_IDLE")
		g_sArenaBgVars.iCoronaLobbyVideoStage = ciCORONA_LOBBY_VIDEO_STAGE_IDLE
	ENDIF
	
	// Make sure we kill any scenes we have triggered here
	STOP_AUDIO_SCENE("DLC_MPHEIST_TRANSITION_TO_APT_FADE_IN_RADIO_SCENE")
	STOP_AUDIO_SCENE("DLC_MPHEIST_TRANSITION_TO_APT_FADE_OUT_AMB_SCENE")
ENDPROC


PROC SET_FM_IN_CORONA_SERVER_GAME_STATE(ServerBroadcastData &SBDpassed, INT msmsPassed)
	#IF IS_DEBUG_BUILD	
		IF msmsPassed = GAME_STATE_INI
			PRINTLN("[TEAM BAL DATA] - RCC... SET_FM_IN_CORONA_SERVER_GAME_STATE - GAME_STATE_INI")
		ELIF msmsPassed = GAME_STATE_RUNNING
			PRINTLN("[TEAM BAL DATA] - RCC... SET_FM_IN_CORONA_SERVER_GAME_STATE - GAME_STATE_RUNNING ")
		ELIF msmsPassed = GAME_STATE_END
			PRINTLN("[TEAM BAL DATA] - RCC... SET_FM_IN_CORONA_SERVER_GAME_STATE - GAME_STATE_END ")
		ELIF msmsPassed = GAME_STATE_SET_WAIT
			PRINTLN("[TEAM BAL DATA] - RCC... SET_FM_IN_CORONA_SERVER_GAME_STATE - GAME_STATE_SET_WAIT ")
		ENDIF 
	#ENDIF
	SBDpassed.iGameState = msmsPassed
	PRINTLN("[TEAM BAL DATA] - RCC SBDpassed.iGameState = ", SBDpassed.iGameState)
ENDPROC


//+-----------------------------------------------------------------------------+
//¦				BALANCING	DM													¦
//+-----------------------------------------------------------------------------+

CONST_INT SORT_STAGE_FIND_SMALLEST 	0
CONST_INT SORT_STAGE_ASSIGN_LARGEST 1
CONST_INT SORT_STAGE_FINISH_UP		2

FUNC INT GET_TEAM_SORT_STAGE(ServerBroadcastData &serverBDPassed)

	RETURN serverBDPassed.iFindLargestProgress
ENDFUNC	

PROC PRINT_TEAM_SORT_STAGE(INT iLocalServerState)
	SWITCH iLocalServerState
		CASE SORT_STAGE_FIND_SMALLEST 	NET_PRINT(" SORT_STAGE_FIND_SMALLEST  ")	BREAK
		CASE SORT_STAGE_ASSIGN_LARGEST	NET_PRINT(" SORT_STAGE_ASSIGN_LARGEST  ")	BREAK
		CASE SORT_STAGE_FINISH_UP		NET_PRINT(" SORT_STAGE_FINISH_UP  ")		BREAK
	ENDSWITCH
ENDPROC

PROC GOTO_TEAM_SORT_STAGE(ServerBroadcastData &serverBDPassed, INT iStage)

	#IF IS_DEBUG_BUILD
		//NET_SCRIPT_HOST_ONLY_COMMAND_ASSERT("GOTO_TEAM_SORT_STAGE Called on client")
	
		SWITCH iStage
			CASE SORT_STAGE_FIND_SMALLEST
				NET_PRINT("[TEAM BAL DATA] - Previous team sort State = ")
				PRINT_TEAM_SORT_STAGE(GET_TEAM_SORT_STAGE(serverBDPassed))
				NET_PRINT_TIME() NET_PRINT_STRINGS("GOTO_TEAM_SORT_STAGE  >>> SORT_STAGE_FIND_SMALLEST  FM_maintain_transition_players.sc", "") NET_NL()
			BREAK
			CASE SORT_STAGE_ASSIGN_LARGEST
				NET_PRINT("[TEAM BAL DATA] - Previous team sort State = ")
				PRINT_TEAM_SORT_STAGE(GET_TEAM_SORT_STAGE(serverBDPassed))
				NET_PRINT_TIME() NET_PRINT_STRINGS("GOTO_TEAM_SORT_STAGE  >>> SORT_STAGE_ASSIGN_LARGEST  FM_maintain_transition_players.sc", "") NET_NL()
			BREAK
			CASE SORT_STAGE_FINISH_UP
				NET_PRINT("[TEAM BAL DATA] - Previous team sort State = ")
				PRINT_TEAM_SORT_STAGE(GET_TEAM_SORT_STAGE(serverBDPassed))
				NET_PRINT_TIME() NET_PRINT_STRINGS("GOTO_TEAM_SORT_STAGE  >>> SORT_STAGE_FINISH_UP  FM_maintain_transition_players.sc", "") NET_NL()
			BREAK
			DEFAULT
				#IF IS_DEBUG_BUILD
					SCRIPT_ASSERT("GOTO_TEAM_SORT_STAGE Invalid option net_deathmatch")
				#ENDIF
			BREAK
		ENDSWITCH
	#ENDIF
	
	serverBDPassed.iFindLargestProgress = iStage
ENDPROC

FUNC INT FIND_TEAM_WITH_SPACE(ServerBroadcastData &serverBDpassed)
	INT iTeams
	REPEAT serverBDpassed.iNumberOfTeams iTeams
		IF serverBDpassed.iteamBalanceCount[iTeams] < iMaxNumPlayersPerTeamLocal[iTeams]
			PRINTLN("[TEAM BAL DATA] - FIND_TEAM_WITH_SPACE - RETURNING ", iTeams)
			RETURN iTeams
		ENDIF
	ENDREPEAT
	RETURN 0
ENDFUNC
FUNC BOOL ALL_TEAMS_HAVE_ONE_PLAYER(ServerBroadcastData &serverBDpassed)
	INT iLoop
	REPEAT serverBDpassed.iNumberOfTeams iLoop
		IF serverBDpassed.iteamBalanceCount[iLoop] = 0
			RETURN FALSE
		ENDIF
	ENDREPEAT
	RETURN TRUE
ENDFUNC
FUNC BOOL ALL_TEAMS_UNDER_MAX(ServerBroadcastData &serverBDpassed)
	INT iLoop
	REPEAT serverBDpassed.iNumberOfTeams iLoop
		IF serverBDpassed.iteamBalanceCount[iLoop] > iMaxNumPlayersPerTeamLocal[iLoop]
			RETURN FALSE
		ENDIF
	ENDREPEAT
	RETURN TRUE
ENDFUNC

FUNC BOOL ARE_THERE_NO_MORE_PLAYERS_TO_MOVE(ServerBroadcastData &serverBDpassed)
	INT iLoop
	REPEAT serverBDpassed.iNumberOfTeams iLoop
		IF iLoop != serverBDpassed.iTeamOrder[0]
			IF serverBDpassed.iteamBalanceCount[iLoop] != 1
				RETURN FALSE
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN TRUE
ENDFUNC

FUNC BOOL HAVE_TEAM_SIZES_BEEN_SORTED(ServerBroadcastData &serverBDpassed) // ROWAN
	INT iTeamToCheck
	INT iTeamLoop

	SWITCH GET_TEAM_SORT_STAGE(serverBDpassed)
	
		CASE SORT_STAGE_FIND_SMALLEST
			serverBDpassed.bTeamSizeSorted = FALSE
			// Getsmallest team
			FOR iTeamToCheck = 0 TO (serverBDpassed.iNumberOfTeams -1)
				FOR iTeamLoop = 0 TO (serverBDpassed.iNumberOfTeams -1)
					IF NOT serverBDpassed.bAlreadyUsed[iTeamToCheck] 
					AND NOT serverBDpassed.bAlreadyUsed[iTeamLoop] 
						IF iTeamToCheck <> iTeamLoop
							IF serverBDpassed.iteamBalanceCount[iTeamToCheck] < serverBDpassed.iteamBalanceCount[iTeamLoop] // IS_TEAM_BEST_TEAM serverBDpassed.iteamBalanceCount[iTeamLoop], serverBDpassed.iteamBalanceCount[iTeamToCheck]
								serverBDpassed.bNotBiggest[iTeamToCheck] = TRUE
								PRINTLN("[TEAM BAL DATA] - HAVE_TEAM_SIZES_BEEN_SORTED, THIS_TEAM_IS_NOT_THE_BIGGEST ", iTeamToCheck)
							ENDIF
						ENDIF
					ENDIF
				ENDFOR
			ENDFOR			
			GOTO_TEAM_SORT_STAGE(serverBDpassed, SORT_STAGE_ASSIGN_LARGEST)
		BREAK
		
		CASE SORT_STAGE_ASSIGN_LARGEST
			// Assign the biggest team to array position 0
			IF serverBDpassed.bTeamSizeSorted = FALSE
				FOR iTeamLoop = 0 TO (serverBDpassed.iNumberOfTeams -1)
					IF serverBDpassed.bNotBiggest[iTeamLoop] = FALSE
					AND	serverBDpassed.bAlreadyUsed[iTeamLoop] = FALSE
						serverBDpassed.iTeamOrder[serverBDpassed.iCurrentDMTeam] = iTeamLoop
						serverBDpassed.bAlreadyUsed[iTeamLoop] = TRUE						
						PRINTLN("[TEAM BAL DATA] - HAVE_TEAM_SIZES_BEEN_SORTED, THIS_TEAM_IS_THE_BIGGEST ", iTeamLoop)
						serverBDpassed.iCurrentDMTeam++
						serverBDpassed.bTeamSizeSorted = TRUE
						RETURN FALSE 
					ENDIF
				ENDFOR
			ELSE
				IF serverBDpassed.iCurrentDMTeam < (serverBDpassed.iNumberOfTeams -1)
					GOTO_TEAM_SORT_STAGE(serverBDpassed, SORT_STAGE_FIND_SMALLEST)
					serverBDpassed.bTeamSizeSorted = FALSE
					// Reset biggest
					FOR iTeamLoop = 0 TO (serverBDpassed.iNumberOfTeams -1)
						serverBDpassed.bNotBiggest[iTeamLoop] = FALSE
					ENDFOR
					PRINTLN("[TEAM BAL DATA] - HAVE_TEAM_SIZES_BEEN_SORTED, MOVING ONTO NEXT TEAM ", serverBDpassed.iCurrentDMTeam)
				ELSE
					FOR iTeamLoop = 0 TO (serverBDpassed.iNumberOfTeams -1)
						IF serverBDpassed.bAlreadyUsed[iTeamLoop] = FALSE
							serverBDpassed.iTeamOrder[serverBDpassed.iCurrentDMTeam] = iTeamLoop
							PRINTLN("[TEAM BAL DATA] - HAVE_TEAM_SIZES_BEEN_SORTED, smallest team ", iTeamLoop)
						ENDIF
					ENDFOR
					GOTO_TEAM_SORT_STAGE(serverBDpassed, SORT_STAGE_FINISH_UP)
				ENDIF
			ENDIF
		BREAK
		
		CASE SORT_STAGE_FINISH_UP		
			RETURN TRUE
		BREAK
		
		DEFAULT
			#IF IS_DEBUG_BUILD
				SCRIPT_ASSERT("HAVE_TEAM_SIZES_BEEN_SORTED, dodgy serverBDpassed.iNumberOfTeams ")
			#ENDIF
			
			RETURN FALSE
		BREAK
	ENDSWITCH

	RETURN FALSE
ENDFUNC

FUNC BOOL SET_BALANCED_TEAM(ServerBroadcastData &serverBDpassed, INT iLoop)
	IF NOT IS_PLAYER_SCTV(PLAYER_ID())
		IF serverBDpassed.iBalancedTeam[iLoop] <> -1
			PRINTLN("[TEAM BAL DATA] - SET_BALANCED_TEAM = ", serverBDpassed.iBalancedTeam[iLoop])
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen = serverBDpassed.iBalancedTeam[iLoop]
			//SET_LOCAL_PLAYER_TEAM(serverBDpassed.iBalancedTeam[iMyPartID])
			PRINTLN("[TEAM BAL DATA] - SET_PED_CAN_BE_TARGETTED_BY_TEAM, false ", serverBDpassed.iBalancedTeam[iLoop])
			//SET_PED_CAN_BE_TARGETTED_BY_TEAM(PLAYER_PED_ID(), serverBDpassed.iBalancedTeam[iMyPartID], FALSE)
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL IS_THIS_TEAM_MISSION(ServerBroadcastData &serverBDPassed)
	IF serverBDpassed.iMissionType = FMMC_TYPE_MISSION
		IF serverBDpassed.iNumberOfTeams > 1
		
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL IS_THIS_NON_TEAM_MISSION(ServerBroadcastData &serverBDPassed)
	IF serverBDpassed.iMissionType = FMMC_TYPE_MISSION
		IF serverBDpassed.iNumberOfTeams <= 1
		
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL IS_THIS_TEAM_DEATHMATCH_OR_TEAM_MISSION(ServerBroadcastData &serverBDPassed)
	IF IS_THIS_TEAM_MISSION(serverBDPassed)
		IF serverBDpassed.iTeamBalancing = DM_TEAM_BALANCING_ON
			RETURN TRUE
		ENDIF
	ELSE
		IF serverBDpassed.iTeamBalancing = DM_TEAM_BALANCING_ON
			RETURN (serverBDPassed.iMissionSubType = DEATHMATCH_TYPE_TDM OR serverBDPassed.iMissionSubType = DEATHMATCH_TYPE_KOTH_TEAM)
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC ASSIGN_LOCAL_PLAYER_TEAM(ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[], INT iMyArrayPos)


	IF NOT IS_THIS_TEAM_DEATHMATCH_OR_TEAM_MISSION(serverBDPassed)
	OR IS_BIT_SET(sLocalVars.iLocalBitSet, CLIENT_BITSET_GOT_TEAM)
		EXIT
	ENDIF
	
	SWITCH serverBDpassed.iTeamBalancing
						
		CASE DM_TEAM_BALANCING_OFF
			
			IF (playerBDPassed[iMyArrayPos].iMenuTeam > -1)
				PRINTLN("[TEAM BAL DATA] - ASSIGN_LOCAL_PLAYER_TEAM, iMenuTeam(DM_TEAM_BALANCING_OFF) = ", playerBDPassed[iMyArrayPos].iMenuTeam)
				//SET_LOCAL_PLAYER_TEAM(playerBDPassed[iMyPartID].iMenuTeam)
				//SET_PED_CAN_BE_TARGETTED_BY_TEAM(PLAYER_PED_ID(), playerBDPassed[iMyPartID].iMenuTeam, FALSE)
				SET_BIT(sLocalVars.iLocalBitSet ,CLIENT_BITSET_GOT_TEAM)
			ENDIF
		BREAK
		
		CASE DM_TEAM_BALANCING_ON
			PRINTLN("[TEAM BAL DATA] - ASSIGN_LOCAL_PLAYER_TEAM, STARTING_MENU_TEAM(DM_TEAM_BALANCING_ON) = ", playerBDPassed[iMyArrayPos].iMenuTeam)
			IF IS_BIT_SET(serverBDpassed.iServerBitSet, SERV_BITSET_STOP_TEAM_BALANCING)
				SET_BALANCED_TEAM(serverBDpassed, iMyArrayPos)
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC
FUNC BOOL HAVE_ALL_PLAYERS_BEEN_ASSIGNED_TEAM(ServerBroadcastData &serverBDpassed)
	
	IF NOT IS_THIS_TEAM_DEATHMATCH_OR_TEAM_MISSION(serverBDPassed)
		PRINTLN("[TEAM BAL DATA] - HAVE_ALL_PLAYERS_BEEN_ASSIGNED_TEAM, RETURN TRUE, IS_TEAM_DEATHMATCH ", IS_THIS_TEAM_DEATHMATCH_OR_TEAM_MISSION(serverBDPassed))
		RETURN TRUE
	ENDIF
	
	INT iParticipant
	REPEAT g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers iParticipant
		//IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
		//	PLAYER_INDEX playerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))
		//	IF NOT IS_PLAYER_SCTV(playerId)
				IF g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iParticipant].iPlayerOptions[ciMISSION_CLIENT_OPTION_TEAM] = -1
				AND g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iParticipant].iPlayerOptions[ciMISSION_CLIENT_OPTION_TEAM] < MAX_TB_TEAMS
					//PRINTLN("[TEAM BAL DATA] - Printing player teams in case it get stuck ", iParticipant, "( ", , " ) ")
					RETURN FALSE
				ENDIF
		//	ENDIF
		//ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

CONST_INT TEAM_BALANCE_PUT_CREW_TOGETHER	0
CONST_INT TEAM_BALANCE_COUNT_TOTAL_PLAYERS 	1
CONST_INT TEAM_BALANCE_INITIALISE 			2
CONST_INT TEAM_BALANCE_COUNT 				3
CONST_INT RACE_TEAMS_UGLY					4
CONST_INT TEAM_BALANCE_ASSIGN				5
CONST_INT TEAM_BALANCE_FINISH				6
CONST_INT TEAMS_MUTUAL_PARTNERS				7
CONST_INT RACE_TEAMS_UNREQUITED				8

FUNC INT GET_BALANCE_STATE(ServerBroadcastData &serverBDPassed)
	RETURN serverBDPassed.iBalanceProgress
ENDFUNC	

FUNC STRING GET_PRINT_BALANCE_STAGE(INT iLocalServerState)
	SWITCH iLocalServerState
		CASE TEAM_BALANCE_PUT_CREW_TOGETHER 	RETURN "TEAM_BALANCE_PUT_CREW_TOGETHER  "
		CASE TEAM_BALANCE_COUNT_TOTAL_PLAYERS 	RETURN "TEAM_BALANCE_COUNT_TOTAL_PLAYERS  "
		CASE TEAM_BALANCE_INITIALISE 			RETURN "TEAM_BALANCE_INITIALISE  "
		CASE TEAMS_MUTUAL_PARTNERS				RETURN "TEAMS_MUTUAL_PARTNERS  "	
		CASE RACE_TEAMS_UNREQUITED				RETURN "RACE_TEAMS_UNREQUITED  "	
		CASE TEAM_BALANCE_COUNT					RETURN "TEAM_BALANCE_COUNT  "		
		CASE RACE_TEAMS_UGLY					RETURN "RACE_TEAMS_UGLY  "	
		CASE TEAM_BALANCE_ASSIGN				RETURN "TEAM_BALANCE_ASSIGN  "		
		CASE TEAM_BALANCE_FINISH				RETURN "TEAM_BALANCE_FINISH  "		
	ENDSWITCH
	RETURN ""
ENDFUNC

PROC GOTO_TEAM_BALANCE_STAGE(ServerBroadcastData &serverBDPassed, INT iStage)

	#IF IS_DEBUG_BUILD
		SWITCH iStage
			CASE TEAM_BALANCE_PUT_CREW_TOGETHER		PRINTLN("[TEAM BAL DATA] - Previous State = ", GET_PRINT_BALANCE_STAGE(serverBDPassed.iBalanceProgress), GET_TIME_AS_STRING(GET_NETWORK_TIME()),  "  GOTO_TEAM_BALANCE_STAGE  >>> TEAM_BALANCE_PUT_CREW_TOGETHER")BREAK
			CASE TEAM_BALANCE_COUNT_TOTAL_PLAYERS	PRINTLN("[TEAM BAL DATA] - Previous State = ", GET_PRINT_BALANCE_STAGE(serverBDPassed.iBalanceProgress), GET_TIME_AS_STRING(GET_NETWORK_TIME()),  "  GOTO_TEAM_BALANCE_STAGE  >>> TEAM_BALANCE_COUNT_TOTAL_PLAYERS")BREAK
			CASE TEAM_BALANCE_INITIALISE			PRINTLN("[TEAM BAL DATA] - Previous State = ", GET_PRINT_BALANCE_STAGE(serverBDPassed.iBalanceProgress), GET_TIME_AS_STRING(GET_NETWORK_TIME()),  "  GOTO_TEAM_BALANCE_STAGE  >>> TEAM_BALANCE_INITIALISE")			BREAK
			CASE TEAMS_MUTUAL_PARTNERS				PRINTLN("[TEAM BAL DATA] - Previous State = ", GET_PRINT_BALANCE_STAGE(serverBDPassed.iBalanceProgress), GET_TIME_AS_STRING(GET_NETWORK_TIME()),  "  GOTO_TEAM_BALANCE_STAGE  >>> TEAMS_MUTUAL_PARTNERS")			BREAK
			CASE RACE_TEAMS_UNREQUITED				PRINTLN("[TEAM BAL DATA] - Previous State = ", GET_PRINT_BALANCE_STAGE(serverBDPassed.iBalanceProgress), GET_TIME_AS_STRING(GET_NETWORK_TIME()),  "  GOTO_TEAM_BALANCE_STAGE  >>> RACE_TEAMS_UNREQUITED")			BREAK
			CASE TEAM_BALANCE_COUNT					PRINTLN("[TEAM BAL DATA] - Previous State = ", GET_PRINT_BALANCE_STAGE(serverBDPassed.iBalanceProgress), GET_TIME_AS_STRING(GET_NETWORK_TIME()),  "  GOTO_TEAM_BALANCE_STAGE  >>> TEAM_BALANCE_COUNT")				BREAK
			CASE RACE_TEAMS_UGLY					PRINTLN("[TEAM BAL DATA] - Previous State = ", GET_PRINT_BALANCE_STAGE(serverBDPassed.iBalanceProgress), GET_TIME_AS_STRING(GET_NETWORK_TIME()),  "  GOTO_TEAM_BALANCE_STAGE  >>> RACE_TEAMS_UGLY")					BREAK
			CASE TEAM_BALANCE_ASSIGN				PRINTLN("[TEAM BAL DATA] - Previous State = ", GET_PRINT_BALANCE_STAGE(serverBDPassed.iBalanceProgress), GET_TIME_AS_STRING(GET_NETWORK_TIME()),  "  GOTO_TEAM_BALANCE_STAGE  >>> TEAM_BALANCE_ASSIGN")				BREAK
			CASE TEAM_BALANCE_FINISH				PRINTLN("[TEAM BAL DATA] - Previous State = ", GET_PRINT_BALANCE_STAGE(serverBDPassed.iBalanceProgress), GET_TIME_AS_STRING(GET_NETWORK_TIME()),  "  GOTO_TEAM_BALANCE_STAGE  >>> TEAM_BALANCE_FINISH")				BREAK	
			DEFAULT
				#IF IS_DEBUG_BUILD
					SCRIPT_ASSERT("GOTO_TEAM_BALANCE_STAGE Invalid option")
				#ENDIF
			BREAK
		ENDSWITCH
	#ENDIF
	serverBDPassed.iBalanceProgress = iStage
ENDPROC

//Force the team count if that was selected in the creator. 
PROC CORRECT_TEAM_COUNT_BASSED_ON_PLAYER_COUNT(ServerBroadcastData &serverBDpassed)	
	PRINTLN("[TEAM BAL DATA] - CORRECT_TEAM_COUNT_BASSED_ON_PLAYER_COUNT")
	PRINTLN("[TEAM BAL DATA] - serverBDpassed.iNumberOfTeams   = ", serverBDpassed.iNumberOfTeams)
	PRINTLN("[TEAM BAL DATA] - serverBDpassed.iPlayerCount     = ", serverBDpassed.iPlayerCount)
	PRINTLN("[TEAM BAL DATA] - serverBDpassed.iMaxNumperOfPart = ", serverBDpassed.iMaxNumperOfPart)
	
	//Player count as a rtaion of max
	FLOAT fPlayCountAsRatio = TO_FLOAT(serverBDpassed.iPlayerCount)/TO_FLOAT(serverBDpassed.iMaxNumperOfPart)
	
	PRINTLN("[TEAM BAL DATA] - fPlayCountAsRatio = ", fPlayCountAsRatio)
	
	//If it's a three team mission 
	IF serverBDpassed.iNumberOfTeams = 3
		//If there's less than a third of the people then make it a two team mission
		IF fPlayCountAsRatio <= 0.66
			PRINTLN("[TEAM BAL DATA] - serverBDpassed.iNumberOfTeams = 2")
			serverBDpassed.iNumberOfTeams = 2			
		ENDIF
	//If it's a three team mission 
	ELIF serverBDpassed.iNumberOfTeams = 4
		//If there's less than a half of the people then make it a two team mission
		IF fPlayCountAsRatio <= 0.5
			serverBDpassed.iNumberOfTeams = 2
			PRINTLN("[TEAM BAL DATA] - serverBDpassed.iNumberOfTeams = 2")
		//If there's less than two thirds of the people then make it a three team mission
		ELIF fPlayCountAsRatio <= 0.75
			serverBDpassed.iNumberOfTeams = 3
			PRINTLN("[TEAM BAL DATA] - serverBDpassed.iNumberOfTeams = 3")
		ENDIF	
	ENDIF
	IF g_FMMC_STRUCT.iMaxNumberOfTeams != serverBDpassed.iNumberOfTeams
		g_FMMC_STRUCT.iMaxNumberOfTeams = serverBDpassed.iNumberOfTeams
		IF g_FMMC_STRUCT.iNumberOfTeams > g_FMMC_STRUCT.iMaxNumberOfTeams
			g_FMMC_STRUCT.iNumberOfTeams = g_FMMC_STRUCT.iMaxNumberOfTeams
			PRINTLN("[SST] * [TEAM BAL DATA] - CORRECT_TEAM_COUNT_BASSED_ON_PLAYER_COUNT - g_FMMC_STRUCT.iNumberOfTeams = g_FMMC_STRUCT.iMaxNumberOfTeams = ", g_FMMC_STRUCT.iNumberOfTeams)
		ENDIF
		PRINTLN("[SST] * [TEAM BAL DATA] - CORRECT_TEAM_COUNT_BASSED_ON_PLAYER_COUNT - g_FMMC_STRUCT.iMaxNumberOfTeams = serverBDpassed.iNumberOfTeams = ", serverBDpassed.iNumberOfTeams)
		bTeamsReaCapped = TRUE
	ELIF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciSELECTABLE_TEAM_NUMBERS)
		PRINTLN("[SST] * [TEAM BAL DATA] - CORRECT_TEAM_COUNT_BASSED_ON_PLAYER_COUNT - g_FMMC_STRUCT.iMaxNumberOfTeams = serverBDpassed.iNumberOfTeams = ", 0)
		g_sTransitionSessionData.sMissionRoundData.iMaxNumberOfTeams  = 0 
	ENDIF
ENDPROC	

PROC CAP_DEATHMATCH_TEAMS(ServerBroadcastData &serverBDpassed, INT iPlayerCount)
	PRINTLN("[TEAM BAL DATA] - CAP_DEATHMATCH_TEAMS ")
	PRINTLN("[TEAM BAL DATA] - serverBDpassed.iNumberOfTeams = ", serverBDpassed.iNumberOfTeams)
	PRINTLN("[TEAM BAL DATA] - iPlayerCount = ", iPlayerCount)
	IF serverBDpassed.iNumberOfTeams > iPlayerCount
		serverBDpassed.iNumberOfTeams = iPlayerCount
		PRINTLN("[TEAM BAL DATA], serverBDpassed.iNumberOfTeams CAPPED_AND_EQUALS : ", serverBDpassed.iNumberOfTeams)
	ENDIF
ENDPROC
//
////Gets the INT of a players crew
//FUNC INT GET_PLAYER_CREW_INT(GAMER_HANDLE &GamerHandle)
//	NETWORK_CLAN_DESC retDesc
//	NETWORK_CLAN_PLAYER_GET_DESC( retDesc, 0, GamerHandle)
//	RETURN retDesc.Id
//ENDFUNC

CONST_INT ciSWAP_POINT_FRIEND	5
CONST_INT ciSWAP_POINT_CREW		2
CONST_INT ciSWAP_POINT_PARTY	1
CONST_INT ciSWAP_POINT_RANK		1

// NEW TEAM BALANCE STUFF START //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
INT iTeamRankTotal[MAX_TB_TEAMS]
INT iTeamPlayerTotal[MAX_TB_TEAMS]
INT iAverageTeamRank[MAX_TB_TEAMS]

FUNC INT GET_TEAM_SWAP_SCORE(PlayerBroadcastData &playerBDPassed[], INT iParticipant, INT iTeamToMoveInto)

	INT iRemotePlayerCrewID 
	INT iScore
	INT iLoopParticipant
	INT iTeam
	INT i
	
	
	PRINTNL()
	PRINTLN("[TEAM BAL DATA] GET_TEAM_SWAP_SCORE, GRAB_AVERAGE_RANKS_START_______________________ ")
	
	
	// Reset everything doh
	REPEAT MAX_TB_TEAMS i
		iTeamRankTotal[i]	= 0
		iTeamPlayerTotal[i]	= 0
		iAverageTeamRank[i]	= 0
	ENDREPEAT
	
	PRINTLN("[TRANSITION SESSIONS] [TEAM BAL DATA] GET_TEAM_SWAP_SCORE, iparticipant    = ", iparticipant)
	PRINTLN("[TRANSITION SESSIONS] [TEAM BAL DATA] GET_TEAM_SWAP_SCORE, iTeamToMoveInto = ", iTeamToMoveInto)
	PRINTLN("[TRANSITION SESSIONS] [TEAM BAL DATA] GET_TEAM_SWAP_SCORE, iMenuTeam       = ", playerBDPassed[iparticipant].iMenuTeam)
	
	// Grab average ranks
	REPEAT g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers iLoopParticipant
		IF playerBDPassed[iLoopParticipant].iMenuTeam > -1

			iTeamRankTotal[playerBDPassed[iLoopParticipant].iMenuTeam] = (iTeamRankTotal[playerBDPassed[iLoopParticipant].iMenuTeam] + g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iLoopParticipant].iRank) 
				
			PRINTLN("[TEAM BAL DATA][RANK] GET_TEAM_SWAP_SCORE iLoopParticipant = ", iLoopParticipant, " iTeamRankTotal = ", iTeamRankTotal[playerBDPassed[iLoopParticipant].iMenuTeam], " TEAM = ", playerBDPassed[iLoopParticipant].iMenuTeam)	
				
			iTeamPlayerTotal[playerBDPassed[iLoopParticipant].iMenuTeam]++
			PRINTLN("[TEAM BAL DATA][RANK] GET_TEAM_SWAP_SCORE iLoopParticipant = ", iLoopParticipant, " iTeamPlayerTotal = ", iTeamPlayerTotal[playerBDPassed[iLoopParticipant].iMenuTeam], " TEAM = ", playerBDPassed[iLoopParticipant].iMenuTeam)
		ELSE
			PRINTLN("[TEAM BAL DATA][RANK] GET_TEAM_SWAP_SCORE FAIL (iMenuTeam = -1)  iLoopParticipant = ", iLoopParticipant)
		ENDIF
	ENDREPEAT
	PRINTNL()
	PRINTLN("___________________________________________________________________________________________________________________ ")
	PRINTNL()
	
	PRINTNL()
	PRINTLN("[TEAM BAL DATA] GET_TEAM_SWAP_SCORE, PRINT_TEAMS_AVERAGE_RANK_START_______________________ ")
	REPEAT MAX_TB_TEAMS iTeam
		iAverageTeamRank[iTeam] = (iTeamRankTotal[iTeam] / iTeamPlayerTotal[iTeam])
		PRINTLN("[TEAM BAL DATA][RANK] GET_TEAM_SWAP_SCORE iTeam = ", iTeam, " iAverageTeamRank = ", iAverageTeamRank[iTeam])
	ENDREPEAT	
	PRINTLN("___________________________________________________________________________________________________________________ ")
	PRINTNL()

	PRINTNL()
	PRINTLN("[TEAM BAL DATA] GET_TEAM_SWAP_SCORE, FRIENDS_AND_CREW_START_______________________ ")
	// Give player score for team swaps
	REPEAT g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers iLoopParticipant
		// Same team
		IF playerBDPassed[iparticipant].iMenuTeam = playerBDPassed[iLoopParticipant].iMenuTeam
			IF iLoopParticipant <> iparticipant

				iRemotePlayerCrewID  = g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iLoopParticipant].iCrewID
				
				PRINTLN("[TRANSITION SESSIONS] [TEAM BAL DATA] GET_TEAM_SWAP_SCORE, iLoopParticipant = ", iLoopParticipant, " iRemotePlayerCrewID = ", iRemotePlayerCrewID)
				
				// Friends
				IF NOT IS_THIS_TRANSITION_SESSION_DOING_HEAD_TO_HEAD()
				OR IS_THIS_AN_ADVERSARY_MODE_MISSION(g_FMMC_STRUCT.iRootContentIDHash, g_FMMC_STRUCT.iAdversaryModeType)
				#IF IS_DEBUG_BUILD
				AND NOT GET_COMMANDLINE_PARAM_EXISTS("sc_ForceHead")	
				#ENDIF
					IF DOES_TRANSITION_PLAYER_HAVE_RELATIONSHIP(iparticipant, iLoopParticipant, ciTRANS_REL_FRIEND)
		
					
						iScore = (iScore+ciSWAP_POINT_FRIEND)
						
						PRINTLN("[TRANSITION SESSIONS] [TEAM BAL DATA] GET_TEAM_SWAP_SCORE, iParticipant     = ", iParticipant, " iLoopParticipant = ", iLoopParticipant, "    FRIENDS iScore = ", iScore)
					ENDIF
				ENDIF
				
				// Crew
				IF iRemotePlayerCrewID = g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iParticipant].iCrewID
				AND iRemotePlayerCrewID != 0
				
					iScore = (iScore+ciSWAP_POINT_CREW)
					
					PRINTLN("[TRANSITION SESSIONS] [TEAM BAL DATA] GET_TEAM_SWAP_SCORE, iParticipant     = ", iParticipant, " iLoopParticipant = ", iLoopParticipant, "    CREW iScore = ", iScore)
				ENDIF

				IF NOT IS_THIS_TRANSITION_SESSION_DOING_HEAD_TO_HEAD()
				OR IS_THIS_AN_ADVERSARY_MODE_MISSION(g_FMMC_STRUCT.iRootContentIDHash, g_FMMC_STRUCT.iAdversaryModeType)
				#IF IS_DEBUG_BUILD
				AND NOT GET_COMMANDLINE_PARAM_EXISTS("sc_ForceHead")	
				#ENDIF
					IF DOES_TRANSITION_PLAYER_HAVE_RELATIONSHIP(iparticipant, iLoopParticipant, ciTRANS_REL_PARTY)
						PRINTLN("[TRANSITION SESSIONS] [TEAM BAL DATA] GET_TEAM_SWAP_SCORE, iParticipant     = ", iParticipant, " iLoopParticipant = ", iLoopParticipant, " PARTY iScore = ", iScore)
						iScore = (iScore+ciSWAP_POINT_PARTY)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			PRINTLN("[TEAM BAL DATA] GET_TEAM_SWAP_SCORE, TEAM_NOT_EQUAL, iParticipant = ", iParticipant, " iLoopParticipant = ", iLoopParticipant)
		ENDIF
	ENDREPEAT
	PRINTNL()
	PRINTLN("___________________________________________________________________________________________________________________ ")
	PRINTNL()
	PRINTLN("[TEAM BAL DATA] GET_TEAM_SWAP_SCORE, RANK_START_______________________ ")
	PRINTLN("[TEAM BAL DATA] GET_TEAM_SWAP_SCORE, iTeamToMoveInto = ", iTeamToMoveInto)
	PRINTLN("[TEAM BAL DATA] GET_TEAM_SWAP_SCORE, iMenuTeam = ", playerBDPassed[iparticipant].iMenuTeam)
	
	// Average rank 
	IF iAverageTeamRank[playerBDPassed[iparticipant].iMenuTeam] > iAverageTeamRank[iTeamToMoveInto]
		// Find low ranked participant
		IF g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iparticipant].iRank <= iAverageTeamRank[iTeamToMoveInto]
		AND iAverageTeamRank[iTeamToMoveInto] != 0
			iScore = (iScore+ciSWAP_POINT_RANK)
			PRINTLN("[TRANSITION SESSIONS][RANK] [TEAM BAL DATA] GET_TEAM_SWAP_SCORE iparticipant = ", iparticipant, " LOWRANK iScore = ", iScore)
		ENDIF
	ELSE
		// Find high ranked participant
		IF g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iparticipant].iRank >= iAverageTeamRank[iTeamToMoveInto]
		AND iAverageTeamRank[iTeamToMoveInto] != 0
			iScore = (iScore+ciSWAP_POINT_RANK)
			PRINTLN("[TRANSITION SESSIONS][RANK] [TEAM BAL DATA] GET_TEAM_SWAP_SCORE iparticipant = ", iparticipant, " HIGHRANK iScore = ", iScore)
		ENDIF
	ENDIF
	
	PRINTLN("[TRANSITION SESSIONS] [TEAM BAL DATA] GET_TEAM_SWAP_SCORE, iParticipant     = ", iParticipant, "                                TOTAL ***** iScore = ", iScore)
	
	PRINTLN("[TEAM BAL DATA], GET_TEAM_SWAP_SCORE, playerBDPassed[iparticipant].iMenuTeam = ", playerBDPassed[iparticipant].iMenuTeam)
	PRINTLN("[TEAM BAL DATA], GET_TEAM_SWAP_SCORE, iTeamToMoveInto= ", iTeamToMoveInto)
	PRINTLN("___________________________________________________________________________________________________________________ ")
	PRINTNL()
	
	RETURN iScore
ENDFUNC

FUNC INT GET_TEAM_SWAP_SCORE_TO_TEAM(PlayerBroadcastData &playerBDPassed[], INT iParticipant, INT iTeamToMoveInto)

	INT iRemotePlayerCrewID 
	INT iScore
	INT iLoopParticipant
	INT i
	
	// Reset everything doh
	REPEAT MAX_TB_TEAMS i
		iTeamRankTotal[i]	= 0
		iTeamPlayerTotal[i]	= 0
		iAverageTeamRank[i]	= 0
	ENDREPEAT

	PRINTLN("[TEAM BAL DATA] GET_TEAM_SWAP_SCORE_TO_TEAM, FRIENDS_AND_CREW_START_______________________ ")
	// Give player score for team swaps
	REPEAT g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers iLoopParticipant
		PRINTLN("[TRANSITION SESSIONS] [TEAM BAL DATA] GET_TEAM_SWAP_SCORE_TO_TEAM, iParticipant     = ", iParticipant, " iLoopParticipant = ", iLoopParticipant)
		PRINTLN("[TRANSITION SESSIONS] [TEAM BAL DATA] GET_TEAM_SWAP_SCORE_TO_TEAM, iTeamToMoveInto  = ", iTeamToMoveInto, " playerBDPassed[", iLoopParticipant, "].iMenuTeam = ", playerBDPassed[iLoopParticipant].iMenuTeam)
		// Same team
		IF iTeamToMoveInto = playerBDPassed[iLoopParticipant].iMenuTeam
			IF iLoopParticipant <> iparticipant

				iRemotePlayerCrewID  = g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iLoopParticipant].iCrewID
				
				PRINTLN("[TEAM BAL DATA] GET_TEAM_SWAP_SCORE_TO_TEAM, iLoopParticipant = ", iLoopParticipant, " iRemotePlayerCrewID = ", iRemotePlayerCrewID)
				
				// Friends
				IF NOT IS_THIS_TRANSITION_SESSION_DOING_HEAD_TO_HEAD()
				OR IS_THIS_AN_ADVERSARY_MODE_MISSION(g_FMMC_STRUCT.iRootContentIDHash, g_FMMC_STRUCT.iAdversaryModeType)
				#IF IS_DEBUG_BUILD
				AND NOT GET_COMMANDLINE_PARAM_EXISTS("sc_ForceHead")	
				#ENDIF
					IF DOES_TRANSITION_PLAYER_HAVE_RELATIONSHIP(iLoopParticipant, iparticipant, ciTRANS_REL_FRIEND)
						iScore = (iScore+ciSWAP_POINT_FRIEND)						
						PRINTLN("[TRANSITION SESSIONS] [TEAM BAL DATA] GET_TEAM_SWAP_SCORE_TO_TEAM, iParticipant     = ", iParticipant, " iLoopParticipant = ", iLoopParticipant, "    FRIENDS iScore = ", iScore)
					ELSE
						PRINTLN("[TEAM BAL DATA] GET_TEAM_SWAP_SCORE_TO_TEAM, iLoopParticipant = ", iLoopParticipant, " DOES_TRANSITION_PLAYER_HAVE_RELATIONSHIP - ciTRANS_REL_FRIEND = FALSE")
					ENDIF
				ENDIF
				
				// Crew
				IF iRemotePlayerCrewID = g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iParticipant].iCrewID
				AND iRemotePlayerCrewID != 0
					iScore = (iScore+ciSWAP_POINT_CREW)
					
					PRINTLN("[TRANSITION SESSIONS] [TEAM BAL DATA] GET_TEAM_SWAP_SCORE_TO_TEAM, iParticipant     = ", iParticipant, " iLoopParticipant = ", iLoopParticipant, "    CREW iScore = ", iScore)
				ENDIF

				IF NOT IS_THIS_TRANSITION_SESSION_DOING_HEAD_TO_HEAD()
				OR IS_THIS_AN_ADVERSARY_MODE_MISSION(g_FMMC_STRUCT.iRootContentIDHash, g_FMMC_STRUCT.iAdversaryModeType)
				#IF IS_DEBUG_BUILD
				AND NOT GET_COMMANDLINE_PARAM_EXISTS("sc_ForceHead")	
				#ENDIF
					IF DOES_TRANSITION_PLAYER_HAVE_RELATIONSHIP(iparticipant, iLoopParticipant, ciTRANS_REL_PARTY)
						PRINTLN("[TRANSITION SESSIONS] [TEAM BAL DATA] GET_TEAM_SWAP_SCORE_TO_TEAM, iParticipant     = ", iParticipant, " iLoopParticipant = ", iLoopParticipant, " PARTY iScore = ", iScore)
						iScore = (iScore+ciSWAP_POINT_PARTY)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			PRINTLN("[TEAM BAL DATA] GET_TEAM_SWAP_SCORE_TO_TEAM, TEAM_NOT_EQUAL, iParticipant = ", iParticipant, " iLoopParticipant = ", iLoopParticipant, " iTeamToMoveInto = ", iTeamToMoveInto)
		ENDIF
	ENDREPEAT
	
	PRINTLN("[TRANSITION SESSIONS] [TEAM BAL DATA] GET_TEAM_SWAP_SCORE_TO_TEAM, iParticipant     = ", iParticipant, "                                           TOTAL ***** iScore = ", iScore)
	
	PRINTNL()
	PRINTLN("___________________________________________________________________________________________________________________ ")
	PRINTNL()
	PRINTLN("[TEAM BAL DATA] GET_TEAM_SWAP_SCORE_TO_TEAM, RANK_START_______________________ ")
	PRINTLN("[TEAM BAL DATA] GET_TEAM_SWAP_SCORE_TO_TEAM, iTeamToMoveInto = ", iTeamToMoveInto)
	PRINTLN("[TEAM BAL DATA] GET_TEAM_SWAP_SCORE_TO_TEAM, iMenuTeam = ", playerBDPassed[iparticipant].iMenuTeam)

	PRINTLN("[TEAM BAL DATA], GET_TEAM_SWAP_SCORE_TO_TEAM, playerBDPassed[iparticipant].iMenuTeam = ", playerBDPassed[iparticipant].iMenuTeam)
	PRINTLN("[TEAM BAL DATA], GET_TEAM_SWAP_SCORE_TO_TEAM, iTeamToMoveInto= ", iTeamToMoveInto)
	PRINTLN("___________________________________________________________________________________________________________________ ")
	PRINTNL()
	
	RETURN iScore
ENDFUNC
INT iLowestBitSet, iSameSwapToTeamScore, iPartMoveBitSet, iSameSwapTeamScore
INT iTeamSwapScore[NUM_NETWORK_PLAYERS]
INT iStuckCount

FUNC INT GET_LOWEST_SWAP_SCORE_PARTICIPANT(BOOL &bRunTeamSwapScore)
	INT iLoopParticipant
	INT iLoopParticipant2
	INT iReturnParticipant
	INT iTeamSwapScoreTemp[NUM_NETWORK_PLAYERS]
	iLowestBitSet = 0
	iSameSwapTeamScore = 0

	REPEAT g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers iLoopParticipant
		REPEAT g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers iLoopParticipant2
			IF iLoopParticipant <> iLoopParticipant2
				IF iTeamSwapScore[iLoopParticipant] > iTeamSwapScore[iLoopParticipant2]
				
					SET_BIT(iLowestBitSet, iLoopParticipant)
					
					#IF IS_DEBUG_BUILD
					//IF IS_DEBUG_KEY_PRESSED(KEY_NUMPAD6, KEYBOARD_MODIFIER_SHIFT, "")
						PRINTLN("[TRANSITION SESSIONS] [TEAM BAL DATA] - GET_LOWEST_SWAP_SCORE_PARTICIPANT, THIS_PART_IS_NOT_THE_BESTEST ", iLoopParticipant)
					//ENDIF
					#ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDREPEAT

	REPEAT g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers iLoopParticipant
		IF NOT IS_BIT_SET(iLowestBitSet, iLoopParticipant)
			
			iTeamSwapScoreTemp[iLoopParticipant] = iTeamSwapScore[iLoopParticipant]
			
			iReturnParticipant = iLoopParticipant

			#IF IS_DEBUG_BUILD
			//IF IS_DEBUG_KEY_PRESSED(KEY_NUMPAD6, KEYBOARD_MODIFIER_SHIFT, "")
				PRINTLN("[TRANSITION SESSIONS] [TEAM BAL DATA] - GET_LOWEST_SWAP_SCORE_PARTICIPANT, THIS_PART_IS_THE_BESTEST_SWAP ", iReturnParticipant)
			//ENDIF
			#ENDIF
		ENDIF
	ENDREPEAT
			
	IF bAlreadyMovingFirstPlayer
		REPEAT g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers iLoopParticipant
			REPEAT g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers iLoopParticipant2
				IF iLoopParticipant != iLoopParticipant2
					IF iTeamSwapScoreTemp[iLoopParticipant] = iTeamSwapScoreTemp[iLoopParticipant2]
					AND iTeamSwapScoreTemp[iLoopParticipant] != 0
						PRINTLN("[TRANSITION SESSIONS] [TEAM BAL DATA] - GET_LOWEST_SWAP_SCORE_PARTICIPANT more than one player has the same team swap score")
						SET_BIT(iSameSwapTeamScore, iLoopParticipant)
						iStuckCount++
						bRunTeamSwapScore = TRUE
					ENDIF
				ENDIF
			ENDREPEAT
		ENDREPEAT
	ENDIF
	
	bAlreadyMovingFirstPlayer =  TRUE	
	IF iStuckCount > 100000
		PRINTLN("[TRANSITION SESSIONS] [TEAM BAL DATA] - iStuckCount > 100000")
		bRunTeamSwapScore = FALSE
	ENDIF 	
	RETURN iReturnParticipant
ENDFUNC

FUNC INT GET_HIGHEST_SWAP_SCORE_PARTICIPANT(PlayerBroadcastData &playerBDPassed[], INT iTeamToMoveInto)
	INT iLoopParticipant
	INT iLoopParticipant2
	INT iReturnParticipant = -1
	iLowestBitSet = 0
	iSameSwapToTeamScore = 0

	REPEAT g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers iLoopParticipant
		IF iTeamToMoveInto != playerBDPassed[iLoopParticipant].iMenuTeam
			REPEAT g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers iLoopParticipant2
				IF iLoopParticipant != iLoopParticipant2
				AND iTeamToMoveInto != playerBDPassed[iLoopParticipant2].iMenuTeam
					IF iTeamSwapScore[iLoopParticipant] < iTeamSwapScore[iLoopParticipant2]
						SET_BIT(iLowestBitSet, iLoopParticipant)
						#IF IS_DEBUG_BUILD
						PRINTLN("[TRANSITION SESSIONS] [TEAM BAL DATA] - GET_HIGHEST_SWAP_SCORE_PARTICIPANT, THIS_PART_IS_NOT_THE_BESTEST ", iLoopParticipant, " iLoopParticipant2 = ", iLoopParticipant2)
						#ENDIF
					ELIF iTeamSwapScore[iLoopParticipant] = iTeamSwapScore[iLoopParticipant2]
						PRINTLN("[TRANSITION SESSIONS] [TEAM BAL DATA] - GET_HIGHEST_SWAP_SCORE_PARTICIPANT, SAME_SWAP_SCORE              ", iLoopParticipant, " iLoopParticipant2 = ", iLoopParticipant2)
						SET_BIT(iSameSwapToTeamScore , iLoopParticipant)						
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
	ENDREPEAT

	REPEAT g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers iLoopParticipant
		IF NOT IS_BIT_SET(iLowestBitSet, iLoopParticipant)
		AND iTeamToMoveInto != playerBDPassed[iLoopParticipant].iMenuTeam		
		AND NOT IS_BIT_SET(iPartMoveBitSet, iLoopParticipant)
			#IF IS_DEBUG_BUILD
			PRINTLN("[TRANSITION SESSIONS] [TEAM BAL DATA] - GET_HIGHEST_SWAP_SCORE_PARTICIPANT, THIS_PART_IS_THE_BESTEST_SWAP ", iLoopParticipant)
			#ENDIF
			RETURN iLoopParticipant
		ENDIF
	ENDREPEAT
	
	IF iReturnParticipant = -1	
		REPEAT g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers iLoopParticipant
			IF IS_BIT_SET(iSameSwapToTeamScore, iLoopParticipant)
			AND iTeamToMoveInto != playerBDPassed[iLoopParticipant].iMenuTeam		
			AND NOT IS_BIT_SET(iPartMoveBitSet, iLoopParticipant)
			AND IS_BIT_SET(iSameSwapTeamScore, iLoopParticipant)
				#IF IS_DEBUG_BUILD
				PRINTLN("[TRANSITION SESSIONS] [TEAM BAL DATA] - GET_HIGHEST_SWAP_SCORE_PARTICIPANT, iSameSwapToTeamScore - THIS_PART_IS_THE_BESTEST_SWAP ", iLoopParticipant)
				#ENDIF
				RETURN iLoopParticipant
			ENDIF
		ENDREPEAT
	ENDIF
	

	REPEAT g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers iLoopParticipant
		IF IS_BIT_SET(iSameSwapTeamScore, iLoopParticipant)
		AND iTeamToMoveInto != playerBDPassed[iLoopParticipant].iMenuTeam		
			PRINTLN("[TRANSITION SESSIONS] [TEAM BAL DATA] - GET_HIGHEST_SWAP_SCORE_PARTICIPANT, Fail safe - THIS_PART_IS_THE_BESTEST_SWAP ", iLoopParticipant)
			RETURN iLoopParticipant			
		ENDIF
	ENDREPEAT
	
	
	PRINTLN("[TRANSITION SESSIONS] [TEAM BAL DATA] - GET_HIGHEST_SWAP_SCORE_PARTICIPANT return -1 - it's broken!")
	RETURN iReturnParticipant
ENDFUNC

INT iBalanceProgress

FUNC INT GET_BEST_TEAM_SWAP_PARTICIPANT(PlayerBroadcastData &playerBDPassed[], INT iLargestTeam, INT iSmallestTeam)
	INT iReturnParticipant = -1
	INT iLoopParticipant
	BOOL bRunTeamSwapScore
	
	SWITCH iBalanceProgress
	
		CASE 0
			// Initialise teams high
			REPEAT g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers iLoopParticipant
				iTeamSwapScore[iLoopParticipant] = HIGHEST_INT
			ENDREPEAT
			
			iBalanceProgress = 1
			PRINTLN("[TRANSITION SESSIONS] [TEAM BAL DATA] - GET_BEST_TEAM_SWAP_PARTICIPANT, iBalanceProgress = 1")
			PRINTLN("[TRANSITION SESSIONS] [TEAM BAL DATA] - GET_BEST_TEAM_SWAP_PARTICIPANT, iLargestTeam = ", iLargestTeam)
			PRINTLN("[TRANSITION SESSIONS] [TEAM BAL DATA] - GET_BEST_TEAM_SWAP_PARTICIPANT, iSmallestTeam = ",iSmallestTeam)
		BREAK
	
		CASE 1
			// Getting swap scores for participant
			REPEAT g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers iLoopParticipant
			
				IF playerBDPassed[iLoopParticipant].iMenuTeam = iLargestTeam
				
					iTeamSwapScore[iLoopParticipant] = GET_TEAM_SWAP_SCORE(playerBDPassed, iLoopParticipant, iSmallestTeam)
					
				ENDIF
			ENDREPEAT
		
			iBalanceProgress = 2
			
			PRINTLN("[TRANSITION SESSIONS] [TEAM BAL DATA] - GET_BEST_TEAM_SWAP_PARTICIPANT, iBalanceProgress = 2")
		BREAK
		
		CASE 2
			// Finding lowest swap score
			iReturnParticipant = GET_LOWEST_SWAP_SCORE_PARTICIPANT(bRunTeamSwapScore)
			
			IF bRunTeamSwapScore = TRUE
				REPEAT g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers iLoopParticipant
			
					IF playerBDPassed[iLoopParticipant].iMenuTeam = iLargestTeam
					
						iTeamSwapScore[iLoopParticipant] = GET_TEAM_SWAP_SCORE_TO_TEAM(playerBDPassed, iLoopParticipant, iSmallestTeam)
						
						PRINTLN("[TRANSITION SESSIONS] [TEAM BAL DATA] - GET_BEST_TEAM_SWAP_PARTICIPANT, iLoopParticipant = ", iLoopParticipant, "  iTeamSwapScore[] = ", iTeamSwapScore[iLoopParticipant])
					ENDIF
				ENDREPEAT
				iBalanceProgress = 3		
				RETURN -1
			ELSE
				PRINTLN("[TRANSITION SESSIONS] [TEAM BAL DATA] - GET_BEST_TEAM_SWAP_PARTICIPANT, bRunTeamSwapScore = FALSE" )
				iBalanceProgress = 0
				RETURN iReturnParticipant
			ENDIF
		BREAK
		
		CASE 3
			iReturnParticipant = GET_HIGHEST_SWAP_SCORE_PARTICIPANT(playerBDPassed, iSmallestTeam)
			iBalanceProgress = 0		
			RETURN iReturnParticipant
		BREAK
	ENDSWITCH
	
	RETURN iReturnParticipant
ENDFUNC
// NEW TEAM BALANCE STUFF END //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


PROC MAINTAIN_PLAYER_CORONA_OPTIONS()	
	INT iLoop
	IF serverBD.iMissionType = FMMC_TYPE_RACE
		FOR iLoop = 0 TO (g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers - 1)
			playerBD[iLoop].iPPrace		= g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iLoop].iPlayerOptions[ciRC_CLIENT_OPTION_PARTNER]
			playerBD[iLoop].iProle		= g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iLoop].iPlayerOptions[ciRC_CLIENT_OPTION_ROLE]
			playerBD[iLoop].gHandle		= g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iLoop].hGamer
		ENDFOR
	ELSE
		FOR iLoop = 0 TO (g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers - 1)
			playerBD[iLoop].iMenuTeam 	= g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iLoop].iPlayerOptions[ciMISSION_CLIENT_OPTION_TEAM]
			playerBD[iLoop].iProle	 	= g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iLoop].iPlayerOptions[ciRC_CLIENT_OPTION_ROLE]
			playerBD[iLoop].gHandle		= g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iLoop].hGamer
		ENDFOR
	ENDIF
ENDPROC

PROC CHECK_TEAM_CAPS_BROKEN()	
	INT iLoop, iTeamCount[MAX_TB_TEAMS]
	IF serverBD.iMissionType = FMMC_TYPE_MISSION
		FOR iLoop = 0 TO (g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers - 1)
			IF playerBD[iLoop].iMenuTeam >=0 
			AND playerBD[iLoop].iMenuTeam < MAX_TB_TEAMS
				iTeamCount[playerBD[iLoop].iMenuTeam]++	
			ENDIF
		ENDFOR
		BOOL bFlattenTeams
		FOR iLoop = 0 TO (serverBD.iNumberOfTeams - 1)
			IF iTeamCount[iLoop] > iMaxNumPlayersPerTeamLocal[iLoop]
				PRINTLN("[TEAM BAL DATA] CHECK_TEAM_CAPS_BROKEN - bFlattenTeams = TRUE - iLoop = ", iLoop)
				bFlattenTeams = TRUE
			ENDIF
		ENDFOR
		IF bFlattenTeams
			PRINTLN("[TEAM BAL DATA] CHECK_TEAM_CAPS_BROKEN - playerBD[iLoop].iMenuTeam = 0")
			FOR iLoop = 0 TO (serverBD.iNumberOfTeams - 1)
				playerBD[iLoop].iMenuTeam = 0
			ENDFOR
		ENDIF
	ENDIF
ENDPROC
#IF IS_DEBUG_BUILD
PROC INT_DEBUG_DATA_FOR_TEST(FMMC_CORONA_DATA_MAINTAIN_SCRIPT_VARS &fmmcCoronaMissionData)
	IF NOT ServerBD.bSituationb
	AND NOT ServerBD.bBug1708849
	AND NOT ServerBD.bBug1861235
	//AND ServerBD.iScenario = -1
		EXIT
	ENDIF
	PRINTLN("[TEAM BAL DATA] INT_DEBUG_DATA_FOR_TEST")
	IF ServerBD.bBug1861235
		g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers = 11
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[0].iPlayerBalancedTeam = 0
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[0].iPlayerOptions[0]   = 0
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[0].iPlayerOptions[1]   = 2
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[1].iPlayerBalancedTeam = 0
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[1].iPlayerOptions[0]   = 0
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[1].iPlayerOptions[1]   = 0
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[2].iPlayerBalancedTeam = 0
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[2].iPlayerOptions[0]   = 0
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[2].iPlayerOptions[1]   = 0
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[3].iPlayerBalancedTeam = 0
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[3].iPlayerOptions[0]   = 0
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[3].iPlayerOptions[1]   = 0
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[4].iPlayerBalancedTeam = 0
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[4].iPlayerOptions[0]   = 0
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[4].iPlayerOptions[1]   = 1
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[5].iPlayerBalancedTeam = 0
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[5].iPlayerOptions[0]   = 0
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[5].iPlayerOptions[1]   = 1
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[6].iPlayerBalancedTeam = 0
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[6].iPlayerOptions[0]   = 0
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[6].iPlayerOptions[1]   = 0
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[7].iPlayerBalancedTeam = 0
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[7].iPlayerOptions[0]   = 0
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[7].iPlayerOptions[1]   = 1
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[8].iPlayerBalancedTeam = 0
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[8].iPlayerOptions[0]   = 0
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[8].iPlayerOptions[1]   = 0
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[9].iPlayerBalancedTeam = 0
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[9].iPlayerOptions[0]   = 0
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[9].iPlayerOptions[1]   = 0
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[10].iPlayerBalancedTeam = 0
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[10].iPlayerOptions[0]   = 0
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[10].iPlayerOptions[1]   = 0
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[0].iCrewID = 48920
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[1].iCrewID = 0
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[2].iCrewID = 45833
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[3].iCrewID = 4412
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[4].iCrewID = 0
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[5].iCrewID = 0
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[6].iCrewID = 48920
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[7].iCrewID = 0
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[8].iCrewID = 0
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[9].iCrewID = 34760
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[10].iCrewID = 48920
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[0].iRank   = 123
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[1].iRank   = 120
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[2].iRank   = 125
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[3].iRank   = 120
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[4].iRank   = 121
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[5].iRank   = 120
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[6].iRank   = 120
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[7].iRank   = 120
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[8].iRank   = 120
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[9].iRank   = 5
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[10].iRank   = 4
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[0].iPlayerFriends, 2)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[0].iPlayerFriends, 9)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[0].iPlayerFriends, 10)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[2].iPlayerFriends, 0)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[2].iPlayerFriends, 3)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[2].iPlayerFriends, 4)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[2].iPlayerFriends, 5)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[2].iPlayerFriends, 7)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[3].iPlayerFriends, 2)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[3].iPlayerFriends, 6)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[3].iPlayerFriends, 9)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[4].iPlayerFriends, 2)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[5].iPlayerFriends, 2)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[6].iPlayerFriends, 3)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[6].iPlayerFriends, 8)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[6].iPlayerFriends, 9)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[6].iPlayerFriends, 10)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[7].iPlayerFriends, 2)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[8].iPlayerFriends, 6)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[8].iPlayerFriends, 9)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[9].iPlayerFriends, 0)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[9].iPlayerFriends, 3)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[9].iPlayerFriends, 6)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[9].iPlayerFriends, 8)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[9].iPlayerFriends, 10)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[10].iPlayerFriends, 0)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[10].iPlayerFriends, 6)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[10].iPlayerFriends, 9)
	ELIF ServerBD.bBug1708849
		g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers = 9
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[0].iPlayerBalancedTeam = 0
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[0].iPlayerOptions[0]   = 0
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[0].iPlayerOptions[1]   = 1
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[1].iPlayerBalancedTeam = 0
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[1].iPlayerOptions[0]   = 0
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[1].iPlayerOptions[1]   = 0
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[2].iPlayerBalancedTeam = 0
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[2].iPlayerOptions[0]   = 0
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[2].iPlayerOptions[1]   = 0
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[3].iPlayerBalancedTeam = 0
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[3].iPlayerOptions[0]   = 0
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[3].iPlayerOptions[1]   = 0
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[4].iPlayerBalancedTeam = 0
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[4].iPlayerOptions[0]   = 0
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[4].iPlayerOptions[1]   = 0
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[5].iPlayerBalancedTeam = 0
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[5].iPlayerOptions[0]   = 0
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[5].iPlayerOptions[1]   = 1
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[6].iPlayerBalancedTeam = 0
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[6].iPlayerOptions[0]   = 0
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[6].iPlayerOptions[1]   = 0
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[7].iPlayerBalancedTeam = 0
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[7].iPlayerOptions[0]   = 0
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[7].iPlayerOptions[1]   = 0
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[8].iPlayerBalancedTeam = 0
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[8].iPlayerOptions[0]   = 0
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[8].iPlayerOptions[1]   = 0
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[0].iCrewID = 7417
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[1].iCrewID = 33487
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[2].iCrewID = 4410
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[3].iCrewID = 7417
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[4].iCrewID = 4411
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[5].iCrewID = 1641
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[6].iCrewID = 7417
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[7].iCrewID = 0
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[8].iCrewID = 33653
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[0].iRank   = 108
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[1].iRank   = 68
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[2].iRank   = 112
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[3].iRank   = 38
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[4].iRank   = 106
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[5].iRank   = 102
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[6].iRank   = 101
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[7].iRank   = 121
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[8].iRank   = 102
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[0].iPlayerFriends, 1)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[0].iPlayerFriends, 2)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[0].iPlayerFriends, 3)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[0].iPlayerFriends, 4)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[0].iPlayerFriends, 5)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[0].iPlayerFriends, 6)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[0].iPlayerFriends, 7)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[0].iPlayerFriends, 8)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[1].iPlayerFriends, 0)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[1].iPlayerFriends, 2)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[1].iPlayerFriends, 3)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[1].iPlayerFriends, 4)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[1].iPlayerFriends, 5)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[1].iPlayerFriends, 6)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[1].iPlayerFriends, 7)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[1].iPlayerFriends, 8)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[2].iPlayerFriends, 0)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[2].iPlayerFriends, 1)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[2].iPlayerFriends, 3)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[2].iPlayerFriends, 4)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[2].iPlayerFriends, 5)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[2].iPlayerFriends, 8)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[3].iPlayerFriends, 0)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[3].iPlayerFriends, 1)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[3].iPlayerFriends, 2)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[3].iPlayerFriends, 4)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[3].iPlayerFriends, 7)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[3].iPlayerFriends, 8)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[4].iPlayerFriends, 0)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[4].iPlayerFriends, 1)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[4].iPlayerFriends, 2)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[4].iPlayerFriends, 3)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[4].iPlayerFriends, 5)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[4].iPlayerFriends, 6)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[4].iPlayerFriends, 7)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[4].iPlayerFriends, 8)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[5].iPlayerFriends, 0)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[5].iPlayerFriends, 1)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[5].iPlayerFriends, 2)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[5].iPlayerFriends, 4)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[5].iPlayerFriends, 7)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[5].iPlayerFriends, 8)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[6].iPlayerFriends, 0)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[6].iPlayerFriends, 1)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[6].iPlayerFriends, 4)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[6].iPlayerFriends, 7)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[7].iPlayerFriends, 0)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[7].iPlayerFriends, 1)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[7].iPlayerFriends, 3)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[7].iPlayerFriends, 4)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[7].iPlayerFriends, 5)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[7].iPlayerFriends, 6)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[8].iPlayerFriends, 0)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[8].iPlayerFriends, 1)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[8].iPlayerFriends, 2)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[8].iPlayerFriends, 3)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[8].iPlayerFriends, 4)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[8].iPlayerFriends, 5)
	ELSE
		g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers = 4
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[0].iPlayerBalancedTeam = 0
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[0].iPlayerOptions[0]   = 0
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[0].iPlayerOptions[1]   = 0
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[1].iPlayerBalancedTeam = 0
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[1].iPlayerOptions[0]   = 0
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[1].iPlayerOptions[1]   = 0
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[2].iPlayerBalancedTeam = 0
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[2].iPlayerOptions[0]   = 0
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[2].iPlayerOptions[1]   = 0
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[3].iPlayerBalancedTeam = 0
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[3].iPlayerOptions[0]   = 0
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[3].iPlayerOptions[1]   = 0
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[0].iCrewID = 4419
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[1].iCrewID = 22668
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[2].iCrewID = 4411
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[3].iCrewID = 0
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[0].iRank   = 87
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[1].iRank   = 100
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[2].iRank   = 101
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[3].iRank   = 101
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[0].iPlayerFriends, 1)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[0].iPlayerFriends, 2)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[0].iPlayerFriends, 3)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[1].iPlayerFriends, 0)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[1].iPlayerFriends, 2)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[1].iPlayerFriends, 3)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[2].iPlayerFriends, 0)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[2].iPlayerFriends, 1)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[3].iPlayerFriends, 0)
		SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[3].iPlayerFriends, 1)
		fmmcCoronaMissionData.bIsHeistMission = TRUE
		iMaxNumPlayersPerTeamLocal[0] = 1
		iMaxNumPlayersPerTeamLocal[1] = 3
		iMaxNumPlayersPerTeamLocal[2] = 0
		iMaxNumPlayersPerTeamLocal[3] = 0
		serverBD.iTeamBalancing = DM_TEAM_BALANCING_ON
		serverBD.iMissionSubType = DEATHMATCH_TYPE_TDM
		serverBD.iNumberOfTeams     = 2
	ENDIF
	MAINTAIN_PLAYER_CORONA_OPTIONS()
ENDPROC
PROC PRINT_ALL_PLAYER_RELATIONSHIPS()
	INT iLoop, iLoop2
	PRINTLN("[TRANSITION SESSIONS] ************************")
	PRINTLN("[TRANSITION SESSIONS] *S       START         *")
	PRINTLN("[TRANSITION SESSIONS] ************************")
	PRINTLN("[TRANSITION SESSIONS] [TEAM BAL DATA] - g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers = ", g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers)
	REPEAT g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers iLoop
		PRINTLN("[TRANSITION SESSIONS] [TEAM BAL DATA] - g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[", iLoop, "].iPlayerBalancedTeam = ", g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iLoop].iPlayerBalancedTeam)
		PRINTLN("[TRANSITION SESSIONS] [TEAM BAL DATA] - g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[", iLoop, "].iPlayerOptions[0]   = ", g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iLoop].iPlayerOptions[0])
		PRINTLN("[TRANSITION SESSIONS] [TEAM BAL DATA] - g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[", iLoop, "].iPlayerOptions[1]   = ", g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iLoop].iPlayerOptions[1])
	ENDREPEAT
	REPEAT g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers iLoop
		PRINTLN("[TRANSITION SESSIONS] [TEAM BAL DATA] - g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[", iLoop, "].iCrewID = ", g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iLoop].iCrewID)
	ENDREPEAT
	REPEAT g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers iLoop
		PRINTLN("[TRANSITION SESSIONS] [TEAM BAL DATA] - g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[", iLoop, "].iRank   = ", g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iLoop].iRank)
	ENDREPEAT
	REPEAT g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers iLoop
		REPEAT g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers iLoop2
			IF IS_BIT_SET(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iLoop].iPlayerFriends, iLoop2)
				PRINTLN("[TRANSITION SESSIONS] [TEAM BAL DATA] - SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[", iLoop, "].iPlayerFriends, ", iLoop2, ")")
			ENDIF
		ENDREPEAT
	ENDREPEAT
	REPEAT g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers iLoop
		REPEAT g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers iLoop2
			IF IS_BIT_SET(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iLoop].iPlayerPartyMembers, iLoop2)
				PRINTLN("[TRANSITION SESSIONS] [TEAM BAL DATA] - SET_BIT(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[", iLoop, "].iPlayerPartyMembers, ", iLoop2, ")")
			ENDIF
		ENDREPEAT
	ENDREPEAT		
ENDPROC

#ENDIF

FUNC BOOL ARE_ALL_TEAMS_SIZES_UNCAPPED()
	INT iLoop
	FOR iLoop = 0 TO (g_FMMC_STRUCT.iMaxNumberOfTeams - 1)
		IF iMaxNumPlayersPerTeamLocal[iLoop] <> 0
			RETURN FALSE
		ENDIF
	ENDFOR
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_FORCES_SIZED_TEAM_BALANCING_RUN()	
	INT iLoop
	
	IF g_FMMC_STRUCT.iMissionType != FMMC_TYPE_MISSION
		RETURN FALSE
	ENDIF
	
	FOR iLoop = 0 TO (g_FMMC_STRUCT.iMaxNumberOfTeams - 1)
		IF iMaxNumPlayersPerTeamLocal[iLoop] <> 0
			RETURN TRUE
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC


FUNC BOOL DOES_TEAM_BALANCING_PASS_FOR_SIX_PLAYERS_FOR_ASYMMETRIC_TEAM_BALANCING(ServerBroadcastData &serverBDpassed, BOOL bDoCapCheck = FALSE)	
	
	IF g_FMMC_STRUCT.iMissionType != FMMC_TYPE_MISSION
		PRINTLN("[TEAM BAL DATA] - DOES_TEAM_BALANCING_PASS_FOR_SIX_PLAYERS_FOR_ASYMMETRIC_TEAM_BALANCING - g_FMMC_STRUCT.iMissionType != FMMC_TYPE_MISSION")
		RETURN FALSE
	ENDIF
	
	IF NOT DOES_THIS_ROCKSTAR_MISSION_REQUIRE_ASYMMETRIC_TEAM_BALANCING()
		PRINTLN("[TEAM BAL DATA] - DOES_TEAM_BALANCING_PASS_FOR_SIX_PLAYERS_FOR_ASYMMETRIC_TEAM_BALANCING - DOES_THIS_ROCKSTAR_MISSION_REQUIRE_ASYMMETRIC_TEAM_BALANCING = FALSE")
		RETURN FALSE
	ENDIF
	
	IF g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers = 6
		PRINTLN("[TEAM BAL DATA] - DOES_TEAM_BALANCING_PASS_FOR_SIX_PLAYERS_FOR_ASYMMETRIC_TEAM_BALANCING - g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers")
		PRINTLN("[TEAM BAL DATA] - DOES_TEAM_BALANCING_PASS_FOR_SIX_PLAYERS_FOR_ASYMMETRIC_TEAM_BALANCING - [",  iMaxNumPlayersPerTeamLocal[serverBDpassed.iTeamOrder[0]], " = ", serverBDpassed.iteamBalanceCount[serverBDpassed.iTeamOrder[0]] + 1, "] = ",  iMaxNumPlayersPerTeamLocal[serverBDpassed.iTeamOrder[0]] = serverBDpassed.iteamBalanceCount[serverBDpassed.iTeamOrder[0]] + 1)
		IF bDoCapCheck
			IF iMaxNumPlayersPerTeamLocal[serverBDpassed.iTeamOrder[0]] = serverBDpassed.iteamBalanceCount[serverBDpassed.iTeamOrder[0]]
				PRINTLN("[TEAM BAL DATA] - DOES_TEAM_BALANCING_PASS_FOR_SIX_PLAYERS_FOR_ASYMMETRIC_TEAM_BALANCING - bDoCapCheck - return TRUE")
				RETURN TRUE
			ENDIF
		
		ELSE
			IF iMaxNumPlayersPerTeamLocal[serverBDpassed.iTeamOrder[0]] = serverBDpassed.iteamBalanceCount[serverBDpassed.iTeamOrder[0]] + 1
				PRINTLN("[TEAM BAL DATA] - DOES_TEAM_BALANCING_PASS_FOR_SIX_PLAYERS_FOR_ASYMMETRIC_TEAM_BALANCING - return TRUE")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC
FUNC BOOL DOES_NORMAL_TEAM_BALANCE_CHECK_PASS(ServerBroadcastData &serverBDpassed, INT iTeam)

	IF (serverBDpassed.iteamBalanceCount[serverBDpassed.iTeamOrder[0]] - serverBDpassed.iteamBalanceCount[serverBDpassed.iTeamOrder[(iTeam)]]) > 1
		//If both teams have a cap
		IF iMaxNumPlayersPerTeamLocal[serverBDpassed.iTeamOrder[0]] != 0 AND iMaxNumPlayersPerTeamLocal[serverBDpassed.iTeamOrder[(iTeam)]] != 0
		//And both teams are one off the max
			IF g_FMMC_STRUCT.iHighPriorityTeamForBalancing != 0
				IF ALL_TEAMS_HAVE_ONE_PLAYER(serverBDpassed)
				AND ALL_TEAMS_UNDER_MAX(serverBDpassed)
					IF DOES_TEAM_BALANCING_PASS_FOR_SIX_PLAYERS_FOR_ASYMMETRIC_TEAM_BALANCING(serverBDpassed, TRUE)
						RETURN FALSE
					ENDIF
					
					IF IS_BIT_SET(g_FMMC_STRUCT.iHighPriorityTeamForBalancing, serverBDpassed.iTeamOrder[0])
					AND (iMaxNumPlayersPerTeamLocal[serverBDpassed.iTeamOrder[0]] = serverBDpassed.iteamBalanceCount[serverBDpassed.iTeamOrder[0]]
					OR ARE_THERE_NO_MORE_PLAYERS_TO_MOVE(serverBDpassed))
						PRINTLN("[TEAM BAL DATA] - DOES_NORMAL_TEAM_BALANCE_CHECK_PASS, RETURN TRUE ")
						RETURN TRUE	
					ELSE
						PRINTLN("[TEAM BAL DATA] - DOES_NORMAL_TEAM_BALANCE_CHECK_PASS, IS_BIT_SET(g_FMMC_STRUCT.iHighPriorityTeamForBalancing, serverBDpassed.iTeamOrder[0]) RETURN FALSE serverBDpassed.iTeamOrder[0] = ", serverBDpassed.iTeamOrder[0])
						RETURN FALSE	
					ENDIF
				ELSE
					PRINTLN("[TEAM BAL DATA] - DOES_NORMAL_TEAM_BALANCE_CHECK_PASS, ALL_TEAMS_HAVE_ONE_PLAYER RETURN FALSE ")
					RETURN FALSE	
				ENDIF
			ELIF (iMaxNumPlayersPerTeamLocal[serverBDpassed.iTeamOrder[0]] - serverBDpassed.iteamBalanceCount[serverBDpassed.iTeamOrder[0]]) + (iMaxNumPlayersPerTeamLocal[serverBDpassed.iTeamOrder[(iTeam)]] - serverBDpassed.iteamBalanceCount[serverBDpassed.iTeamOrder[(iTeam)]]) <= 1
				IF serverBDpassed.iteamBalanceCount[serverBDpassed.iTeamOrder[0]] != 0
				AND serverBDpassed.iteamBalanceCount[serverBDpassed.iTeamOrder[(iTeam)]] != 0
					PRINTLN("[TEAM BAL DATA] - DOES_NORMAL_TEAM_BALANCE_CHECK_PASS, RETURN TRUE ")
					RETURN TRUE	
				ELSE
					PRINTLN("[TEAM BAL DATA] - DOES_NORMAL_TEAM_BALANCE_CHECK_PASS, RETURN FALSE ")
					RETURN FALSE	
				ENDIF
			ELSE
				PRINTLN("[TEAM BAL DATA] - DOES_NORMAL_TEAM_BALANCE_CHECK_PASS, RETURN FALSE ")
				RETURN FALSE
			ENDIF
		ELSE
			PRINTLN("[TEAM BAL DATA] - DOES_NORMAL_TEAM_BALANCE_CHECK_PASS, RETURN FALSE ")
			RETURN FALSE	
		ENDIF
	ENDIF
	
	//And we've not blowen the cap	
	IF serverBDpassed.iteamBalanceCount[serverBDpassed.iTeamOrder[(iTeam)]] > iMaxNumPlayersPerTeamLocal[serverBDpassed.iTeamOrder[(iTeam)]]
	AND iMaxNumPlayersPerTeamLocal[serverBDpassed.iTeamOrder[(iTeam)]] != 0
		PRINTLN("[TEAM BAL DATA] - DOES_NORMAL_TEAM_BALANCE_CHECK_PASS, RETURN FALSE - serverBDpassed.iTeamOrder[(iTeam)] - serverBDpassed.iteamBalanceCount[", serverBDpassed.iTeamOrder[(iTeam)], "] >= iMaxNumPlayersPerTeamLocal[", serverBDpassed.iTeamOrder[(iTeam)], "]")
		RETURN FALSE
	ENDIF	
	IF serverBDpassed.iteamBalanceCount[serverBDpassed.iTeamOrder[0]] > iMaxNumPlayersPerTeamLocal[serverBDpassed.iTeamOrder[0]]
	AND iMaxNumPlayersPerTeamLocal[serverBDpassed.iTeamOrder[0]] != 0
		PRINTLN("[TEAM BAL DATA] - DOES_NORMAL_TEAM_BALANCE_CHECK_PASS, RETURN FALSE - serverBDpassed.iTeamOrder[0] - serverBDpassed.iteamBalanceCount[", serverBDpassed.iTeamOrder[0], "] >= iMaxNumPlayersPerTeamLocal[", serverBDpassed.iTeamOrder[0], "] ")
		RETURN FALSE
	ENDIF
	
	PRINTLN("[TEAM BAL DATA] - DOES_NORMAL_TEAM_BALANCE_CHECK_PASS, 0 size = ", serverBDpassed.iteamBalanceCount[serverBDpassed.iTeamOrder[0]])
	PRINTLN("[TEAM BAL DATA] - DOES_NORMAL_TEAM_BALANCE_CHECK_PASS, iTeam  = ", iTeam)
	PRINTLN("[TEAM BAL DATA] - DOES_NORMAL_TEAM_BALANCE_CHECK_PASS, i size = ", serverBDpassed.iteamBalanceCount[serverBDpassed.iTeamOrder[(iTeam)]])

	RETURN TRUE
ENDFUNC

FUNC BOOL ARE_THE_TEAM_NUMBERS_BALANCED(ServerBroadcastData &serverBDpassed, BOOl bRunForcedSizeBalancing)

	INT iTEAMSMALL	= (serverBDpassed.iNumberOfTeams -1)
//	INT iTEAMMED	= (serverBDpassed.iNumberOfTeams -2)
//	INT iTEAMBIG	= (serverBDpassed.iNumberOfTeams -3)
	
	IF serverBDpassed.iNumberOfTeams = 1
	
		RETURN TRUE
	ENDIF
	
	INT i
	
	//If it's a heist
	IF bRunForcedSizeBalancing
	AND NOT ARE_ALL_TEAMS_SIZES_UNCAPPED()
	AND iMaxNumPlayersPerTeamLocal[serverBDpassed.iTeamOrder[(iTEAMSMALL)]] != 0	//And the max number on a team is not 0
		
		PRINTLN("[TEAM BAL DATA] - ARE_THE_TEAM_NUMBERS_BALANCED, START *************")
		//If it's a two team mission work out who's the largest
		INT iLargestTeam = 0
		IF serverBDpassed.iTeamOrder[(iTEAMSMALL)] = 0
			iLargestTeam = 1
		ENDIF
		INT iMaxNumPlayersPerTeam 
		FOR i = iTEAMSMALL TO 0 STEP -1
			iMaxNumPlayersPerTeam = iMaxNumPlayersPerTeamLocal[serverBDpassed.iTeamOrder[(i)]]
			IF iMaxNumPlayersPerTeam = 0
				iMaxNumPlayersPerTeam = NUM_NETWORK_REAL_PLAYERS()
			ENDIF
			IF serverBDpassed.iteamBalanceCount[serverBDpassed.iTeamOrder[(i)]] < iMaxNumPlayersPerTeam
				IF DOES_NORMAL_TEAM_BALANCE_CHECK_PASS(serverBDpassed, i)					
					PRINTLN("[TEAM BAL DATA] - ARE_THE_TEAM_NUMBERS_BALANCED, DOES_NORMAL_TEAM_BALANCE_CHECK_PASS, RETURN TRUE ")
					
					RETURN TRUE	
				ELSE
					PRINTLN("[TEAM BAL DATA] - //ARE_THE_TEAM_NUMBERS_BALANCED, Team Being Processed 	: ", serverBDpassed.iTeamOrder[(i)], " i = ", i)
					PRINTLN("[TEAM BAL DATA] - //ARE_THE_TEAM_NUMBERS_BALANCED, Team Count 				: ", serverBDpassed.iteamBalanceCount[serverBDpassed.iTeamOrder[(i)]])
					PRINTLN("[TEAM BAL DATA] - //ARE_THE_TEAM_NUMBERS_BALANCED, Team Max 				: ", iMaxNumPlayersPerTeam)
				
					RETURN FALSE
				ENDIF
			ELIF serverBDpassed.iteamBalanceCount[serverBDpassed.iTeamOrder[(i)]] > iMaxNumPlayersPerTeam
				PRINTLN("[TEAM BAL DATA] - ARE_THE_TEAM_NUMBERS_BALANCED, GREATER THAN, RETURN FALSE ")
				PRINTLN("[TEAM BAL DATA] - ARE_THE_TEAM_NUMBERS_BALANCED, Team Being Processed 	: ", serverBDpassed.iTeamOrder[(i)], " i = ", i)
				PRINTLN("[TEAM BAL DATA] - ARE_THE_TEAM_NUMBERS_BALANCED, Team Count 			: ", serverBDpassed.iteamBalanceCount[serverBDpassed.iTeamOrder[(i)]])
				PRINTLN("[TEAM BAL DATA] - ARE_THE_TEAM_NUMBERS_BALANCED, Team Max 				: ", iMaxNumPlayersPerTeam)
			
				RETURN FALSE
			//if it's a two team mission and the number of players is = to the cap 
			ELIF serverBDpassed.iteamBalanceCount[serverBDpassed.iTeamOrder[(i)]] = iMaxNumPlayersPerTeam
			AND serverBDpassed.iNumberOfTeams = 2
			//and the cap on the other team is 0
			AND iMaxNumPlayersPerTeamLocal[iLargestTeam] = 0
			//and there's enough people on it
			AND serverBDpassed.iteamBalanceCount[iLargestTeam] >= serverBDpassed.iteamBalanceCount[serverBDpassed.iTeamOrder[(i)]]
				PRINTLN("[TEAM BAL DATA] - ARE_THE_TEAM_NUMBERS_BALANCED, EQUAL TOO, RETURN TRUE ")
				PRINTLN("[TEAM BAL DATA] - ARE_THE_TEAM_NUMBERS_BALANCED, Team Being Processed 	: ", serverBDpassed.iTeamOrder[(i)], " i = ", i)
				PRINTLN("[TEAM BAL DATA] - ARE_THE_TEAM_NUMBERS_BALANCED, Team Count 			: ", serverBDpassed.iteamBalanceCount[serverBDpassed.iTeamOrder[(i)]])
				PRINTLN("[TEAM BAL DATA] - ARE_THE_TEAM_NUMBERS_BALANCED, Team Max 				: ", iMaxNumPlayersPerTeam)
				PRINTLN("[TEAM BAL DATA] - iMaxNumPlayersPerTeamLocal[iLargestTeam] 	: ", iMaxNumPlayersPerTeamLocal[iLargestTeam])
				PRINTLN("[TEAM BAL DATA] - serverBDpassed.iNumberOfTeams                    	: ", serverBDpassed.iNumberOfTeams)
				RETURN TRUE
			ENDIF
		ENDFOR
	ELSE
		//If the difference is more than 1 (do normal checks)
		IF NOT DOES_NORMAL_TEAM_BALANCE_CHECK_PASS(serverBDpassed, iTEAMSMALL)
		
			PRINTLN("[TEAM BAL DATA] - ARE_THE_TEAM_NUMBERS_BALANCED, DOES_NORMAL_TEAM_BALANCE_CHECK_PASS, RETURN FALSE ")
		
			RETURN FALSE
		ENDIF
	ENDIF
	
	PRINTLN("[TEAM BAL DATA] - ARE_THE_TEAM_NUMBERS_BALANCED, RETURN TRUE ")
	
	//YES!
	RETURN TRUE
ENDFUNC

//FUNC BOOL ARE_THE_TEAM_NUMBERS_BALANCED(ServerBroadcastData &serverBDpassed, BOOl bRunForcedSizeBalancing)
//
//	INT iTEAMSMALL	= (serverBDpassed.iNumberOfTeams -1)
//	INT iTEAMMED	= (serverBDpassed.iNumberOfTeams -2)
//	INT iTEAMBIG	= (serverBDpassed.iNumberOfTeams -3)
//	
//	IF serverBDpassed.iNumberOfTeams = 1
//	
//		RETURN TRUE
//	ENDIF
//	
//	//If it's a heits
//	IF bRunForcedSizeBalancing
//	AND NOT ARE_ALL_TEAMS_SIZES_UNCAPPED()
//	AND iMaxNumPlayersPerTeamLocal[serverBDpassed.iTeamOrder[(iTEAMSMALL)]] != 0	//And the max number on a team is not 0
//		
//		PRINTLN("[TEAM BAL DATA] - ARE_THE_TEAM_NUMBERS_BALANCED, START *************")
//		
//		//If the smallest team is full
//		IF iMaxNumPlayersPerTeamLocal[serverBDpassed.iTeamOrder[(iTEAMSMALL)]] != serverBDpassed.iteamBalanceCount[serverBDpassed.iTeamOrder[(iTEAMSMALL)]]
//			
//			IF serverBDpassed.iNumberOfTeams = 2
//				IF (serverBDpassed.iteamBalanceCount[serverBDpassed.iTeamOrder[(iTEAMSMALL - 1)]] - serverBDpassed.iteamBalanceCount[serverBDpassed.iTeamOrder[(iTEAMSMALL)]]) > 1
//				
//					PRINTLN("[TEAM BAL DATA] - ARE_THE_TEAM_NUMBERS_BALANCED, iTEAMSMALL		: ", serverBDpassed.iTeamOrder[(iTEAMSMALL)])
//					PRINTLN("[TEAM BAL DATA] - ARE_THE_TEAM_NUMBERS_BALANCED, iTEAMSMALL-1 		: ", serverBDpassed.iTeamOrder[(iTEAMSMALL - 1)])
//				
//					RETURN FALSE
//				ENDIF
//			ELSE
//				IF (serverBDpassed.iteamBalanceCount[serverBDpassed.iTeamOrder[0]] - serverBDpassed.iteamBalanceCount[serverBDpassed.iTeamOrder[(iTEAMSMALL)]]) > 1
//					PRINTLN("[TEAM BAL DATA] - ARE_THE_TEAM_NUMBERS_BALANCED, iTEAMSMALL///	: ", serverBDpassed.iTeamOrder[(iTEAMSMALL)])
//					
//					RETURN FALSE
//				ENDIF
//			ENDIF
//		ELSE
//			IF iTEAMMED > 0
//				IF iMaxNumPlayersPerTeamLocal[serverBDpassed.iTeamOrder[(iTEAMMED)]] != 0	
//					IF iMaxNumPlayersPerTeamLocal[serverBDpassed.iTeamOrder[(iTEAMMED)]] != serverBDpassed.iteamBalanceCount[serverBDpassed.iTeamOrder[(iTEAMMED)]]
//						IF serverBDpassed.iNumberOfTeams = 3
//							IF (serverBDpassed.iteamBalanceCount[serverBDpassed.iTeamOrder[(iTEAMMED - 1)]] - serverBDpassed.iteamBalanceCount[serverBDpassed.iTeamOrder[(iTEAMMED)]]) > 1
//
//								PRINTLN("[TEAM BAL DATA] - ARE_THE_TEAM_NUMBERS_BALANCED, iTEAMMED		: ", serverBDpassed.iTeamOrder[(iTEAMMED)])
//								PRINTLN("[TEAM BAL DATA] - ARE_THE_TEAM_NUMBERS_BALANCED, iTEAMMED-1 	: ", serverBDpassed.iTeamOrder[(iTEAMMED - 1)])
//								RETURN FALSE
//							ENDIF
//						ELSE
//							PRINTLN("[TEAM BAL DATA] - ARE_THE_TEAM_NUMBERS_BALANCED, iTEAMMED	: ", serverBDpassed.iTeamOrder[(iTEAMMED)])
//							RETURN FALSE
//						ENDIF
//					ELSE
//						IF iTEAMBIG > 0
//							IF iMaxNumPlayersPerTeamLocal[serverBDpassed.iTeamOrder[(iTEAMBIG)]] != 0	
//								IF iMaxNumPlayersPerTeamLocal[serverBDpassed.iTeamOrder[(iTEAMBIG)]] != serverBDpassed.iteamBalanceCount[serverBDpassed.iTeamOrder[(iTEAMBIG)]]
//									IF (iTEAMBIG - 1) >= 0
//										IF (serverBDpassed.iteamBalanceCount[serverBDpassed.iTeamOrder[(iTEAMBIG - 1)]] - serverBDpassed.iteamBalanceCount[serverBDpassed.iTeamOrder[(iTEAMBIG)]]) > 1
//								
//											PRINTLN("[TEAM BAL DATA] - ARE_THE_TEAM_NUMBERS_BALANCED, iTEAMBIG		: ", serverBDpassed.iTeamOrder[(iTEAMBIG)])
//											PRINTLN("[TEAM BAL DATA] - ARE_THE_TEAM_NUMBERS_BALANCED, iTEAMBIG-1 	: ", serverBDpassed.iTeamOrder[(iTEAMBIG - 1)])
//								
//											RETURN FALSE
//										ENDIF
//									ENDIF
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
//	ELSE
//		//If the diffrence is more than 1 (do normal checks)
//		IF (serverBDpassed.iteamBalanceCount[serverBDpassed.iTeamOrder[0]] - serverBDpassed.iteamBalanceCount[serverBDpassed.iTeamOrder[(iTEAMSMALL)]]) > 1
//		
//			PRINTLN("[TEAM BAL DATA] - ARE_THE_TEAM_NUMBERS_BALANCED, (do normal checks) ")
//		
//			RETURN FALSE
//		ENDIF
//	ENDIF
//	
//	PRINTLN("[TEAM BAL DATA] - ARE_THE_TEAM_NUMBERS_BALANCED, RETURN TRUE ")
//	
//	//YES!
//	RETURN TRUE
//ENDFUNC

FUNC BOOL PROCESS_TEAM_BALANCING(ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[], BOOL bRunForcedSizeBalancing)

	IF IS_THIS_TRANSITION_SESSION_DOING_HEAD_TO_HEAD()
	AND NOT IS_THIS_AN_ADVERSARY_MODE_MISSION(g_FMMC_STRUCT.iRootContentIDHash, g_FMMC_STRUCT.iAdversaryModeType)
	#IF IS_DEBUG_BUILD
	OR GET_COMMANDLINE_PARAM_EXISTS("sc_ForceHead")	
	#ENDIF
		RETURN TRUE
	ENDIF
	
	INT iparticipant
//	PARTICIPANT_INDEX tempPart
//	PLAYER_INDEX PlayerId
	INT iResetLoop
	INT iteam
	INT iPartToSwap = -1
	INT iLargestTeam, iSmallestTeam
	SWITCH GET_BALANCE_STATE(serverBDpassed)
	
		CASE TEAM_BALANCE_PUT_CREW_TOGETHER
			GOTO_TEAM_BALANCE_STAGE(serverBDpassed, TEAM_BALANCE_COUNT_TOTAL_PLAYERS)
			IF serverBDpassed.iNumberOnEachTeam = 0
				serverBDpassed.iNumberOnEachTeam = g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers/serverBDpassed.iNumberOfTeams
				PRINTLN("[TRANSITION SESSIONS] [TEAM BAL DATA] - serverBDpassed.iNumberOnEachTeam =  ", serverBDpassed.iNumberOnEachTeam)
			ENDIF
		BREAK
	
		//Count all the players to see if the teams need to be forced smaller
		CASE TEAM_BALANCE_COUNT_TOTAL_PLAYERS
			IF serverBDpassed.bForceSmallerTeams
				serverBDpassed.iPlayerCount = g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers
				CORRECT_TEAM_COUNT_BASSED_ON_PLAYER_COUNT(serverBDpassed)
			ELIF ( IS_THIS_ROCKSTAR_MISSION_NEW_VS_BVS1(g_FMMC_STRUCT.iRootContentIDHash)	
			OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_IN_AND_OUT(g_FMMC_STRUCT.iRootContentIDHash)
			OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_MONSTER_JAM(g_FMMC_STRUCT.iAdversaryModeType)
			OR (IS_THIS_ROCKSTAR_MISSION_NEW_VS_ENTOURAGE(g_FMMC_STRUCT.iAdversaryModeType)	AND IS_JOB_IN_CASINO()) 
			OR IS_THIS_ROCKSTAR_MISSION_NEW_SIEGE_MENTALITY_FL(g_FMMC_STRUCT.iAdversaryModeType))
				CAP_TEAM_SIZES_FOR_NEW_VS_MODE(g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers, iMaxNumPlayersPerTeamLocal)
			ELIF serverBDpassed.iMissionType = FMMC_TYPE_DEATHMATCH
				serverBDpassed.iPlayerCount = g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers
				CAP_DEATHMATCH_TEAMS(serverBDpassed, serverBDpassed.iPlayerCount)
			ENDIF
			GOTO_TEAM_BALANCE_STAGE(serverBDpassed, TEAM_BALANCE_INITIALISE)
		BREAK
		
		CASE TEAM_BALANCE_INITIALISE
			REPEAT g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers iparticipant
				IF serverBDpassed.bForceSmallerTeams
					IF playerBDPassed[iparticipant].iMenuTeam >= serverBDpassed.iNumberOfTeams
						PRINTLN("[TEAM BAL DATA] - playerBDPassed[", iparticipant, "].iMenuTeam >= serverBDpassed.iNumberOfTeams ", serverBDpassed.iNumberOfTeams)
						serverBDpassed.iTempBalancedTeam[iparticipant] = 0
						PRINTLN("[TEAM BAL DATA] - serverBDpassed.iTempBalancedTeam[", iparticipant, "] = ", serverBDpassed.iTempBalancedTeam[iparticipant])
					ELSE
						PRINTLN("[TEAM BAL DATA] - bForceSmallerTeams iTempBalancedTeam =  ", playerBDPassed[iparticipant].iMenuTeam)
						serverBDpassed.iTempBalancedTeam[iparticipant] = playerBDPassed[iparticipant].iMenuTeam
					ENDIF
				ELSE
					PRINTLN("[TEAM BAL DATA] - iMenuTeam  START=  ", playerBDPassed[iparticipant].iMenuTeam)
					// We don't want to give the players teams greater than the number of teams so cap it and assert (PT Fix 1489727)
					IF playerBDPassed[iparticipant].iMenuTeam >= serverBDpassed.iNumberOfTeams
						PRINTLN("[TEAM BAL DATA] - CAPPING_START playerBDPassed[", iparticipant, "].iMenuTeam >= serverBDpassed.iNumberOfTeams ",serverBDpassed.iNumberOfTeams)
						#IF IS_DEBUG_BUILD
							SCRIPT_ASSERT(" iMenuTeam is coming through from the corona as greater than the number of teams see 1489727 ")
						#ENDIF							
						serverBDpassed.iTempBalancedTeam[iparticipant] = 0
						PRINTLN("[TEAM BAL DATA] - CAPPING_END serverBDpassed.iTempBalancedTeam[", iparticipant, "] = ", serverBDpassed.iTempBalancedTeam[iparticipant])
					ELSE
						serverBDpassed.iTempBalancedTeam[iparticipant] = playerBDPassed[iparticipant].iMenuTeam
						PRINTLN("[TEAM BAL DATA] - OK iTempBalancedTeam =  ", playerBDPassed[iparticipant].iMenuTeam)
					ENDIF
				ENDIF
				PRINTLN("[TEAM BAL DATA] - PROCESS_TEAM_BALANCING, iparticipant = ", iparticipant, " serverBDpassed.iTempBalancedTeam[iparticipant] = ", serverBDpassed.iTempBalancedTeam[iparticipant] )
				#IF IS_DEBUG_BUILD
				IF serverBDpassed.iTempBalancedTeam[iparticipant] < 0
					serverBDpassed.iTempBalancedTeam[iparticipant] = 0
				ENDIF
				IF serverBDpassed.iTempBalancedTeam[iparticipant] >= MAX_TB_TEAMS
					serverBDpassed.iTempBalancedTeam[iparticipant] = (MAX_TB_TEAMS -1)
				ENDIF
				#ENDIF
			ENDREPEAT
			GOTO_TEAM_BALANCE_STAGE(serverBDpassed, TEAM_BALANCE_COUNT)
		BREAK
		
		CASE TEAM_BALANCE_COUNT			
			REPEAT g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers iparticipant
				//If it's a heist
				IF bRunForcedSizeBalancing
				//And this team has a player cap
				AND iMaxNumPlayersPerTeamLocal[serverBDpassed.iTempBalancedTeam[iparticipant]] != 0
				AND g_FMMC_STRUCT.iHighPriorityTeamForBalancing = 0
					//And we've exceded it
					IF serverBDpassed.iteamBalanceCount[serverBDpassed.iTempBalancedTeam[iparticipant]] >= iMaxNumPlayersPerTeamLocal[serverBDpassed.iTempBalancedTeam[iparticipant]]
						//then move them out
						serverBDpassed.iTempBalancedTeam[iparticipant] = FIND_TEAM_WITH_SPACE(serverBDpassed)
					ENDIF
				ENDIF
				serverBDpassed.iteamBalanceCount[serverBDpassed.iTempBalancedTeam[iparticipant]]++ 
			ENDREPEAT			
			FOR iteam = 0 to (serverBDpassed.iNumberOfTeams -1)
				PRINTNL()
				PRINTLN("[TEAM BAL DATA]")
				PRINTLN("[TEAM BAL DATA] - PROCESS_TEAM_BALANCING CASE 1, serverBDpassed.iteamBalanceCount for team: ",iteam, " is = ",serverBDpassed.iteamBalanceCount[iteam])
			ENDFOR			
			PRINTLN("[TEAM BAL DATA] - PROCESS_TEAM_BALANCING CASE 1, serverBDpassed.iNumberOfTeams = ", serverBDpassed.iNumberOfTeams)
			PRINTLN("[TEAM BAL DATA]")
			PRINTNL()
			GOTO_TEAM_BALANCE_STAGE(serverBDpassed, RACE_TEAMS_UGLY)
		BREAK
		
		CASE RACE_TEAMS_UGLY
			IF HAVE_TEAM_SIZES_BEEN_SORTED(serverBDpassed)
				IF NOT ARE_THE_TEAM_NUMBERS_BALANCED(serverBDpassed, bRunForcedSizeBalancing)
					IF serverBDpassed.iNumberOfTeams > 2
					AND serverBDpassed.bTeamsSplit = FALSE
						iLargestTeam  = 0
						IF serverBDpassed.iCurrentTeam < 1
							serverBDpassed.iCurrentTeam = 1
							PRINTLN("[TRANSITION SESSIONS] [TEAM BAL DATA] - serverBDpassed.iCurrentTeam < 1 - serverBDpassed.iCurrentTeam = 1")
						ENDIF
						IF g_FMMC_STRUCT.iHighPriorityTeamForBalancing != 0
						AND NOT ALL_TEAMS_HAVE_ONE_PLAYER(serverBDpassed)
						AND serverBDpassed.iteamBalanceCount[serverBDpassed.iCurrentTeam] >0
							PRINTLN("[TRANSITION SESSIONS] [TEAM BAL DATA] - g_FMMC_STRUCT.iHighPriorityTeamForBalancing != 0 AND NOT ALL_TEAMS_HAVE_ONE_PLAYER - serverBDpassed.iCurrentTeam = ", serverBDpassed.iCurrentTeam)
							serverBDpassed.iCurrentTeam++
						ENDIF
						IF serverBDpassed.iteamBalanceCount[serverBDpassed.iCurrentTeam] >= serverBDpassed.iNumberOnEachTeam
							serverBDpassed.iCurrentTeam++
						ENDIF
						IF DOES_TEAM_BALANCING_PASS_FOR_SIX_PLAYERS_FOR_ASYMMETRIC_TEAM_BALANCING(serverBDpassed)
							serverBDpassed.iCurrentTeam++
							PRINTLN("[TRANSITION SESSIONS] [TEAM BAL DATA] - DOES_TEAM_BALANCING_PASS_FOR_SIX_PLAYERS_FOR_ASYMMETRIC_TEAM_BALANCING(serverBDpassed) - serverBDpassed.iCurrentTeam = ", serverBDpassed.iCurrentTeam)
						ENDIF
						PRINTLN("[TRANSITION SESSIONS] [TEAM BAL DATA] - serverBDpassed.iCurrentTeam = ", serverBDpassed.iCurrentTeam)
						IF serverBDpassed.iCurrentTeam < serverBDpassed.iNumberOfTeams
							iSmallestTeam = serverBDpassed.iCurrentTeam
							PRINTLN("[TRANSITION SESSIONS] [TEAM BAL DATA] - iSmallestTeam = serverBDpassed.iCurrentTeam = ", serverBDpassed.iCurrentTeam)
						ELSE
							PRINTLN("[TRANSITION SESSIONS] [TEAM BAL DATA] - serverBDpassed.iCurrentTeam >= serverBDpassed.iNumberOfTeams")
							serverBDpassed.bTeamsSplit = TRUE								
							PRINTLN("[TRANSITION SESSIONS] [TEAM BAL DATA] -serverBDpassed.bTeamsSplit = TRUE")
							iLargestTeam  = serverBDpassed.iTeamOrder[0]
							iSmallestTeam = serverBDpassed.iTeamOrder[(serverBDpassed.iNumberOfTeams -1)]
						ENDIF
					ELSE
						iLargestTeam  = serverBDpassed.iTeamOrder[0]
						iSmallestTeam = serverBDpassed.iTeamOrder[(serverBDpassed.iNumberOfTeams -1)]						
					ENDIF	
					
					//If it's a heist
					IF bRunForcedSizeBalancing
					//and there's a cap on the number of players in the team
					AND iMaxNumPlayersPerTeamLocal[iSmallestTeam] != 0
						//and we;ve reached that cap then find the next smallest team
						IF serverBDpassed.iteamBalanceCount[iSmallestTeam] >= iMaxNumPlayersPerTeamLocal[iSmallestTeam]
							IF (serverBDpassed.iNumberOfTeams -2) > 0
								iSmallestTeam = (serverBDpassed.iNumberOfTeams - 2)
								IF serverBDpassed.iteamBalanceCount[iSmallestTeam] >= iMaxNumPlayersPerTeamLocal[iSmallestTeam]
									IF (serverBDpassed.iNumberOfTeams -3) > 0
										iSmallestTeam = (serverBDpassed.iNumberOfTeams - 3)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					iPartToSwap = GET_BEST_TEAM_SWAP_PARTICIPANT(playerBDPassed, iLargestTeam, iSmallestTeam)
					IF iPartToSwap <> -1
						SET_BIT(iPartMoveBitSet, iPartToSwap)
					
						PRINTLN("[TRANSITION SESSIONS] [TEAM BAL DATA] - PROCESS_TEAM_BALANCING CASE 2, moving participant: ", iPartToSwap, " from team: ", serverBDpassed.iTeamOrder[playerBDPassed[iPartToSwap].iMenuTeam], " to team: ",iSmallestTeam )
						serverBDpassed.iTempBalancedTeam[iPartToSwap] = iSmallestTeam

						// Added to fix 1518563
						playerBDPassed[iPartToSwap].iMenuTeam = serverBDpassed.iTempBalancedTeam[iPartToSwap]
						PRINTLN("[TRANSITION SESSIONS] [TEAM BAL DATA] - PROCESS_TEAM_BALANCING CASE 2, iMenuTeam = ", playerBDPassed[iPartToSwap].iMenuTeam)

						GOTO_TEAM_SORT_STAGE(serverBDpassed, SORT_STAGE_FIND_SMALLEST)
						
						serverBDpassed.iCurrentDMTeam	 		= 0
						serverBDpassed.bTeamSizeSorted			= FALSE
						
						// Reset everything
						FOR iResetLoop = 0 TO (serverBDpassed.iNumberOfTeams -1)
							serverBDpassed.iteamBalanceCount[iResetLoop] = 0
							serverBDpassed.bNotBiggest[iResetLoop] 			= FALSE
							serverBDpassed.bAlreadyUsed[iResetLoop] 		= FALSE
							serverBDpassed.iTeamOrder[iResetLoop] 			= -1
							serverBDpassed.iteamBalanceCount[iResetLoop] 	= 0
						ENDFOR
						iBalanceProgress = 0
						GOTO_TEAM_BALANCE_STAGE(serverBDpassed, TEAM_BALANCE_COUNT)
						
						RETURN FALSE
					ENDIF
				ELSE
					GOTO_TEAM_BALANCE_STAGE(serverBDpassed, TEAM_BALANCE_ASSIGN)						
					RETURN FALSE
				ENDIF
			ENDIF
		BREAK	
		
		CASE TEAM_BALANCE_ASSIGN
			REPEAT g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers iparticipant
				serverBDpassed.iBalancedTeam[iparticipant] = serverBDpassed.iTempBalancedTeam[iparticipant]
				PRINTLN("[TEAM BAL DATA] - PROCESS_TEAM_BALANCING CASE 3, giving participant: ", iparticipant, " balanced team: ", serverBDpassed.iBalancedTeam[iparticipant])
			ENDREPEAT
			
			GOTO_TEAM_BALANCE_STAGE(serverBDpassed, TEAM_BALANCE_FINISH)
			
			RETURN TRUE
		BREAK
		
		CASE TEAM_BALANCE_FINISH
			PRINTLN("[TEAM BAL DATA] - PROCESS_TEAM_BALANCING CASE 4, RETURN TRUE")
			
			RETURN TRUE
		BREAK
			
	ENDSWITCH

	RETURN FALSE
ENDFUNC

CONST_INT DIVIDE_STAGE_CAP_TEAMS 	0
CONST_INT DIVIDE_STAGE_GIVE_TEAMS 	1
CONST_INT DIVIDE_STAGE_END			2

FUNC BOOL JUST_DIVIDE_PLAYERS_BETWEEN_TEAMS(ServerBroadcastData &serverBDpassed)
	INT iparticipant
	INT iTeamCount
	
	SWITCH serverBDpassed.iDivideProgress
	
		CASE DIVIDE_STAGE_CAP_TEAMS
			serverBDpassed.iPlayerCount = g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers
			CAP_DEATHMATCH_TEAMS(serverBDpassed, serverBDpassed.iPlayerCount)
			PRINTLN("[TEAM BAL DATA] - serverBDpassed.iDivideProgress = DIVIDE_STAGE_GIVE_TEAMS ")
			PRINTLN("[TEAM BAL DATA] - iNumberOfJoinedPlayers = ", g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers)
			serverBDpassed.iDivideProgress = DIVIDE_STAGE_GIVE_TEAMS
		BREAK
		
		CASE DIVIDE_STAGE_GIVE_TEAMS
			REPEAT g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers iparticipant
				serverBDpassed.iBalancedTeam[iparticipant] = iTeamCount
				PRINTLN("[TEAM BAL DATA] - JUST_DIVIDE_PLAYERS_BETWEEN_TEAMS, iparticipant = ", iparticipant, " TEAM =  ", serverBDpassed.iBalancedTeam[iparticipant])
				IF iTeamCount >= (serverBDpassed.iNumberOfTeams -1)
					iTeamCount = 0	
				ELSE
					iTeamCount ++
				ENDIF
			ENDREPEAT
			
			PRINTLN("[TEAM BAL DATA] - serverBDpassed.iDivideProgress = DIVIDE_STAGE_END ")
			serverBDpassed.iDivideProgress = DIVIDE_STAGE_END
		BREAK
		
		CASE DIVIDE_STAGE_END
		
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SERVER_TEAM_BALANCING_DM_MISSION(ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[], BOOL bRunForcedSizeBalancing)
	
	IF (NOT IS_THIS_TEAM_DEATHMATCH_OR_TEAM_MISSION(serverBDPassed)
	OR serverBDpassed.iTeamBalancing = DM_TEAM_BALANCING_OFF)
	AND NOT serverBDPassed.bSkipBalance
		IF ((serverBDPassed.iMissionSubType <> DEATHMATCH_TYPE_TDM OR serverBDPassed.iMissionSubType <> DEATHMATCH_TYPE_KOTH_TEAM) AND serverBDpassed.iMissionType <> FMMC_TYPE_MISSION)
		OR JUST_DIVIDE_PLAYERS_BETWEEN_TEAMS(serverBDpassed)
			SET_BIT(serverBDpassed.iServerBitSet, SERV_BITSET_STOP_TEAM_BALANCING)
			PRINTLN("[TEAM BAL DATA] - SERVER_TEAM_BALANCING_DM_MISSION OR serverBDpassed.iTeamBalancing = DM_TEAM_BALANCING_OFF")
			PRINTLN("[TEAM BAL DATA] - serverBDpassed.iTeamBalancing = ", serverBDpassed.iTeamBalancing)
			RETURN TRUE
		ENDIF
	ENDIF
	IF NOT IS_BIT_SET(serverBDpassed.iServerBitSet, SERV_BITSET_STOP_TEAM_BALANCING)
		IF PROCESS_TEAM_BALANCING(serverBDpassed, playerBDPassed, bRunForcedSizeBalancing)
			PRINTLN("[TEAM BAL DATA] - SERVER_TEAM_BALANCING_DM_MISSION, SERV_BITSET_STOP_TEAM_BALANCING")
			SET_BIT(serverBDpassed.iServerBitSet, SERV_BITSET_STOP_TEAM_BALANCING)
			RETURN TRUE
		ENDIF	
	ELSE
		PRINTLN("[TEAM BAL DATA] - IS_BIT_SET(serverBDpassed.iServerBitSet, SERV_BITSET_STOP_TEAM_BALANCING)")
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC 


PROC SERVER_SET_BROADCAST_DATA(ServerBroadcastData &serverBDpassed)
	#IF IS_DEBUG_BUILD
	PRINTLN("[TEAM BAL DATA] - SERVER_SET_BROADCAST_DATA - at the UNIX TIME - (", GET_CLOUD_TIME_AS_INT(),")")
	#ENDIF
	IF serverBDpassed.iMissionType = FMMC_TYPE_MISSION
		serverBDpassed.iMissionSubType		= 0
		serverBDpassed.iNumberOfTeams		= g_FMMC_STRUCT.iMaxNumberOfTeams
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciSELECTABLE_TEAM_NUMBERS)
			IF g_sTransitionSessionOptions.iSelection[ciMISSION_HOST_OPTION_NUM_TEAMS] = -1
				serverBDpassed.iNumberOfTeams = g_FMMC_STRUCT.iMaxNumberOfTeams
				PRINTLN("[SST] * [TEAM BAL DATA] - g_sTransitionSessionOptions.iSelection[ciMISSION_HOST_OPTION_NUM_TEAMS] = -1 serverBDpassed.iNumberOfTeams = ", g_FMMC_STRUCT.iNumberOfTeams)
			ELSE
				serverBDpassed.iNumberOfTeams	= g_sTransitionSessionOptions.iSelection[ciMISSION_HOST_OPTION_NUM_TEAMS]
				IF g_FMMC_STRUCT.iMaxNumberOfTeams != serverBDpassed.iNumberOfTeams
					g_FMMC_STRUCT.iMaxNumberOfTeams = serverBDpassed.iNumberOfTeams
					IF g_FMMC_STRUCT.iNumberOfTeams > g_FMMC_STRUCT.iMaxNumberOfTeams
						g_FMMC_STRUCT.iNumberOfTeams = g_FMMC_STRUCT.iMaxNumberOfTeams
						PRINTLN("[SST] * [TEAM BAL DATA] - SERVER_SET_BROADCAST_DATA - g_FMMC_STRUCT.iNumberOfTeams = g_FMMC_STRUCT.iMaxNumberOfTeams = ", g_FMMC_STRUCT.iNumberOfTeams)
					ENDIF
				ENDIF
			ENDIF
			PRINTLN("[TEAM BAL DATA] - SERVER_SET_BROADCAST_DATA - Override iNumberOfTeams for mission host selection - serverBDpassed.iNumberOfTeams = ", serverBDpassed.iNumberOfTeams)
		ENDIF
		IF serverBDpassed.iNumberOfTeams > FMMC_MAX_TEAMS
			serverBDpassed.iNumberOfTeams = FMMC_MAX_TEAMS
		ENDIF
		serverBDpassed.iTeamBalancing		= DM_TEAM_BALANCING_ON//g_sTransitionSessionOptions.iSelection[ciMISSION_HOST_OPTION_TEAM_BAL]
	ELIF serverBDpassed.iMissionType = FMMC_TYPE_DEATHMATCH
		serverBDpassed.iMissionSubType		= g_sTransitionSessionOptions.iSelection[ciDM_HOST_OPTION_TYPE]
		serverBDpassed.iNumberOfTeams		= g_sTransitionSessionOptions.iSelection[ciDM_HOST_OPTION_NUM_TEAMS] + 2
		IF serverBDpassed.iNumberOfTeams > MAX_NUM_DM_TEAMS
			serverBDpassed.iNumberOfTeams = MAX_NUM_DM_TEAMS
		ENDIF
		serverBDpassed.iTeamBalancing		= DM_TEAM_BALANCING_ON//g_sTransitionSessionOptions.iSelection[ciDM_HOST_OPTION_TEAM_BAL]
	ELIF serverBDpassed.iMissionType = FMMC_TYPE_RACE
		serverBDpassed.iMissionSubType		= g_sTransitionSessionOptions.iSelection[ciRC_HOST_OPTION_TYPE]
		IF g_sTransitionSessionOptions.iSelection[ciRC_HOST_OPTION_GTA_TEAM] = ciRACE_GTA_TEAMS_OFF
			serverBDpassed.bTeamGtaRace = FALSE
		ELSE
			serverBDpassed.bTeamGtaRace = TRUE
		ENDIF
		IF serverBDpassed.iMissionSubType = ciRACE_SUB_TYPE_RALLY
			serverBDpassed.iTeamBalancing		= DM_TEAM_BALANCING_ON
		ENDIF
		//serverBDpassed.iNumberOfTeams		= GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sMenuSelection.iSelection[]
	ENDIF
	
	//If this is a team mission
	IF IS_THIS_A_TEAM_MISSION_TYPE(serverBDpassed)
	AND ARE_ALL_TEAMS_EVEN()
		//And there is more than one team
		IF g_FMMC_STRUCT.iMaxNumberOfTeams > 1
			INT iLoop
			INT iCount
			//Loop through all players
			FOR iLoop = 0 TO (g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers - 1)
				//Move them up one team if they were last on a team mission
				IF IS_BIT_SET(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iLoop].iTranPlayerBitSet, ciTRANSITION_SESSIONS_PBS_WAS_JUST_ON_A_VS_MISSION)
					iCount++
					IF IS_BIT_SET(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iLoop].iTranPlayerBitSet, ciTRANSITION_SESSIONS_PBS_WAS_JUST_ON_TEAM_1)
						g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iLoop].iPlayerOptions[ciMISSION_CLIENT_OPTION_TEAM]  = 1
					ELIF IS_BIT_SET(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iLoop].iTranPlayerBitSet, ciTRANSITION_SESSIONS_PBS_WAS_JUST_ON_TEAM_2)
						g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iLoop].iPlayerOptions[ciMISSION_CLIENT_OPTION_TEAM]  = 2
					ELIF IS_BIT_SET(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iLoop].iTranPlayerBitSet, ciTRANSITION_SESSIONS_PBS_WAS_JUST_ON_TEAM_3)
						g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iLoop].iPlayerOptions[ciMISSION_CLIENT_OPTION_TEAM]  = 3
					ELIF IS_BIT_SET(g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iLoop].iTranPlayerBitSet, ciTRANSITION_SESSIONS_PBS_WAS_JUST_ON_TEAM_4)
						g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iLoop].iPlayerOptions[ciMISSION_CLIENT_OPTION_TEAM]  = 0
					ENDIF 
					IF g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iLoop].iPlayerOptions[ciMISSION_CLIENT_OPTION_TEAM] > (serverBDpassed.iNumberOfTeams - 1)
						g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iLoop].iPlayerOptions[ciMISSION_CLIENT_OPTION_TEAM] = (0)
					ENDIF
					PRINTLN("[TEAM BAL DATA] - ", g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iLoop].szGamerTag, " new team is = g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[", iLoop, "].iPlayerOptions[", ciMISSION_CLIENT_OPTION_TEAM, "] = ", g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iLoop].iPlayerOptions[ciMISSION_CLIENT_OPTION_TEAM])
				ENDIF
			ENDFOR
			IF iCount > 1
				serverBDpassed.bSkipBalance = TRUE
			ENDIF
		ENDIF
	ENDIF	
	MAINTAIN_PLAYER_CORONA_OPTIONS()
	#IF IS_DEBUG_BUILD
	PRINTLN("[TEAM BAL DATA] - serverBDpassed.iMissionSubType    = ", serverBDpassed.iMissionSubType)
	PRINTLN("[TEAM BAL DATA] - serverBDpassed.iNumberOfTeams     = ", serverBDpassed.iNumberOfTeams)
	PRINTLN("[TEAM BAL DATA] - serverBDpassed.iTeamBalancing     = ", serverBDpassed.iTeamBalancing)
	#ENDIF
ENDPROC

// True for Rally and GTA races
FUNC BOOL IS_THIS_TEAM_RACE(ServerBroadcastData &serverBDPassed)

	IF IS_TARGET_ASSAULT_RACE()
		RETURN TRUE
	ENDIF

	IF serverBDPassed.iMissionSubType = ciRACE_SUB_TYPE_RALLY
	
		RETURN TRUE	
	ENDIF
	
	IF IS_THIS_CORONA_A_GTA_TEAM_RACE(g_sTransitionSessionOptions)

		RETURN TRUE	
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC  BOOL IS_THIS_A_RALLY_RACE_TRANSITION_SESSION(ServerBroadcastData &serverBDpassed)
	RETURN (serverBDPassed.iMissionSubType = ciRACE_SUB_TYPE_RALLY)
ENDFUNC

PROC SET_CLIENT_BALANCED_TEAM_GLOBALS(ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[])
	#IF IS_DEBUG_BUILD
	PRINTLN("[TRANSITION SESSIONS] [TEAM BAL DATA] -    SET_CLIENT_BALANCED_TEAM_GLOBALS       --")
	//INT iMyGBD  = NATIVE_TO_INT(PLAYER_ID())
	#ENDIF
	INT iLoop, iLoopTwo
	
	//Check to see if the teams should be balanced
	BOOL bBalanceTeams
	IF IS_THIS_A_TEAM_MISSION_TYPE(serverBDpassed)
		IF IS_THIS_TEAM_DEATHMATCH_OR_TEAM_MISSION(serverBDpassed)
			PRINTLN("[TRANSITION SESSIONS] [TEAM BAL DATA] - bBalanceTeams = TRUE")
			bBalanceTeams = TRUE
		ENDIF
	ELIF IS_THIS_A_RACE_TYPE(serverBDpassed)
		IF IS_THIS_TEAM_RACE(serverBDpassed)
			PRINTLN("[TRANSITION SESSIONS] [TEAM BAL DATA] - bBalanceTeams = TRUE")
			bBalanceTeams = TRUE
		ENDIF
	ENDIF
	
	INT iCurrentTeam
	
	IF bBalanceTeams
		PRINTLN("[TRANSITION SESSIONS] [TEAM BAL DATA] - --      bBalanceTeams = TRUE       --")
		INT iHostID = TRANSITION_SESSIONS_NETWORK_GET_TRANSITION_HOST()
		PRINTLN("[TRANSITION SESSIONS] [TEAM BAL DATA] - iHostID = ", iHostID)
		INT iHostsCrewID = 	g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iHostID].iCrewID	
		PRINTLN("[TRANSITION SESSIONS] [TEAM BAL DATA] - iHostsCrewID = ", iHostsCrewID)
		//set up the players teams and prefered partners
		FOR iLoop = 0 TO (g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers - 1)
			//If it's a mission
			IF IS_THIS_A_TEAM_MISSION_TYPE(serverBDpassed)				
				IF IS_THIS_TRANSITION_SESSION_DOING_HEAD_TO_HEAD()
				AND NOT IS_THIS_AN_ADVERSARY_MODE_MISSION(g_FMMC_STRUCT.iRootContentIDHash, g_FMMC_STRUCT.iAdversaryModeType)
				#IF IS_DEBUG_BUILD
				OR GET_COMMANDLINE_PARAM_EXISTS("sc_ForceHead")	
				#ENDIF
					IF g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iLoop].iCrewID	 = iHostsCrewID
						g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iLoop].iPlayerOptions[ciMISSION_CLIENT_OPTION_TEAM] 	= 0
					ELSE
						g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iLoop].iPlayerOptions[ciMISSION_CLIENT_OPTION_TEAM] 	= 1
					ENDIF
				ELSE
					//Set the balanced team
					g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iLoop].iPlayerOptions[ciMISSION_CLIENT_OPTION_TEAM] 	= serverBDpassed.iBalancedTeam[iLoop] //playerBDPassed[iMyPart].iMenuTeam
					g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iLoop].iPlayerOptions[ciRC_CLIENT_OPTION_ROLE] 		= playerBDPassed[iLoop].iProle
				ENDIF
			//If it's a race
			ELIF IS_THIS_A_RACE_TYPE(serverBDpassed)
				//If it's a rally then set peoples teams
				//IF IS_THIS_A_RALLY_RACE_TRANSITION_SESSION(serverBDPassed)
				
					//Don't need to do anything to this as it's set above				
					//g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iLoop].iPlayerOptions[ciRC_CLIENT_OPTION_PARTNER] = 
					
					g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iLoop].iPlayerOptions[ciRC_CLIENT_OPTION_ROLE]		= playerBDPassed[iLoop].iProle 
					g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iLoop].iPlayerOptions[ciRC_CLIENT_OPTION_PARTNER]	= playerBDPassed[iLoop].iTeammate
					g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iLoop].iPlayerBalancedTeam 							= serverBDpassed.iBalancedTeam[iLoop]
					
				//If it's NOT a rally then set people on individual teams
				//ELSE
				//	g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iLoop].iPlayerBalancedTeam = iLoop
				//ENDIF
			ENDIF
		ENDFOR		
	ELSE
		PRINTLN("[TRANSITION SESSIONS] [TEAM BAL DATA] - --      bBalanceTeams = FALSE       --")
		//If it's a race set players in individual teams for corona
		IF IS_THIS_A_RACE_TYPE(serverBDpassed)
			PRINTLN("[TRANSITION SESSIONS] [TEAM BAL DATA] - NON TEAM RACE SETTING - ")
			FOR iLoop = 0 TO (g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers - 1)
				g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iLoop].iPlayerOptions[ciMISSION_CLIENT_OPTION_TEAM]	= iLoop
				g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iLoop].iPlayerBalancedTeam						 	= iLoop
			ENDFOR
		//If it's a NON team DM then put people on indivdual teams
		ELIF IS_THIS_A_DEATHMATCH_TYPE(serverBDpassed)
			//If it's a free for all.
			IF (serverBDPassed.iMissionSubType = DEATHMATCH_TYPE_TDM OR serverBDPassed.iMissionSubType = DEATHMATCH_TYPE_KOTH_TEAM)
				PRINTLN("[TRANSITION SESSIONS] [TEAM BAL DATA] - DEATHMATCH_TYPE_TDM")
				FOR iLoop = 0 TO (g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers - 1)
					g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iLoop].iPlayerOptions[ciMISSION_CLIENT_OPTION_TEAM] = serverBDpassed.iBalancedTeam[iLoop]
				ENDFOR				
			ELSE
				PRINTLN("[TRANSITION SESSIONS] [TEAM BAL DATA] - NON TEAM DM SETTING - every body on diffrent teams")
				FOR iLoop = 0 TO (g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers - 1)
					g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iLoop].iPlayerOptions[ciMISSION_CLIENT_OPTION_TEAM]	 		= iLoop
				ENDFOR
			ENDIF
		ELIF IS_THIS_TEAM_MISSION(serverBDpassed)
			PRINTLN("[TRANSITION SESSIONS] [TEAM BAL DATA] - IS_THIS_TEAM_MISSION")
			FOR iLoop = 0 TO (g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers - 1)
				g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iLoop].iPlayerOptions[ciMISSION_CLIENT_OPTION_TEAM] = serverBDpassed.iBalancedTeam[iLoop]
				g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iLoop].iPlayerOptions[ciRC_CLIENT_OPTION_ROLE] 	   = playerBDPassed[iLoop].iProle 
			ENDFOR 
		ELIF IS_THIS_NON_TEAM_MISSION(serverBDpassed)
			PRINTLN("[TRANSITION SESSIONS] [TEAM BAL DATA] - IS_THIS_NON_TEAM_MISSION")
			FOR iLoop = 0 TO (g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers - 1)
				g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iLoop].iPlayerOptions[ciMISSION_CLIENT_OPTION_TEAM] = 0
			ENDFOR	
		ELIF((serverBDpassed.iMissionType = FMMC_TYPE_MG_ARM_WRESTLING)
		OR	(serverBDpassed.iMissionType = FMMC_TYPE_MG_DARTS)
		OR	(serverBDpassed.iMissionType = FMMC_TYPE_MG_GOLF)
		OR	(serverBDpassed.iMissionType = FMMC_TYPE_MG_SHOOTING_RANGE)
		OR	(serverBDpassed.iMissionType = FMMC_TYPE_MG_TENNIS)
		OR	(serverBDpassed.iMissionType = FMMC_TYPE_SURVIVAL)
		OR	(serverBDpassed.iMissionType = FMMC_TYPE_MG_PILOT_SCHOOL)
		OR	(serverBDpassed.iMissionType = FMMC_TYPE_BASE_JUMP))
 			FOR iLoop = 0 TO (g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers - 1)
			PRINTLN("[TRANSITION SESSIONS] [TEAM BAL DATA] - everyhthing ELSE g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[", iLoop, "].iPlayerOptions[ciMISSION_CLIENT_OPTION_TEAM]	 = ", iLoop)
				g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iLoop].iPlayerOptions[ciMISSION_CLIENT_OPTION_TEAM]	 		= iLoop
			ENDFOR
		ENDIF
	ENDIF
	
	// Catch when teams are set to -1
	IF IS_THIS_A_TEAM_MISSION_TYPE(serverBDpassed)				
		FOR iLoop = 0 TO (g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers - 1)
			IF g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iLoop].iPlayerOptions[ciMISSION_CLIENT_OPTION_TEAM] < 0
				PRINTLN("[TRANSITION SESSIONS] [TEAM BAL DATA] - g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[", iLoop, "].iPlayerOptions[ciMISSION_CLIENT_OPTION_TEAM] = ",g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iLoop].iPlayerOptions[ciMISSION_CLIENT_OPTION_TEAM])
				SCRIPT_ASSERT("g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iLoop].iPlayerOptions[ciMISSION_CLIENT_OPTION_TEAM] < 0 ")
				g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iLoop].iPlayerOptions[ciMISSION_CLIENT_OPTION_TEAM] = 0
			ENDIF
		ENDFOR
	ELIF IS_THIS_A_RACE_TYPE(serverBDpassed)
		IF IS_THIS_A_RALLY_RACE_TRANSITION_SESSION(serverBDPassed)
			FOR iLoop = 0 TO (g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers - 1)
				IF g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iLoop].iPlayerBalancedTeam = -1
					PRINTLN("[TRANSITION SESSIONS] [TEAM BAL DATA] - g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[", iLoop, "].iPlayerBalancedTeam = -1")
					SCRIPT_ASSERT("g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iLoop].iPlayerBalancedTeam = -1 ")
					g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iLoop].iPlayerBalancedTeam = iCurrentTeam
					iCurrentTeam++
					PRINTLN("[TRANSITION SESSIONS] [TEAM BAL DATA] - g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[", iLoop, "].iPlayerBalancedTeam = ", g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iLoop].iPlayerBalancedTeam)
				ENDIF
			ENDFOR
		//If this is a team gta race
		ELIF serverBD.bTeamGtaRace
			IF g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers = 2
				PRINTLN("[TRANSITION SESSIONS] [TEAM BAL DATA] - g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers bTeamGtaRace = TRUE")
				g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[0].iPlayerBalancedTeam = 0
				g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[1].iPlayerBalancedTeam = 1
				//Set then want to play with them selves
				g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[0].iPlayerOptions[ciRC_CLIENT_OPTION_PARTNER] = 0
				g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[1].iPlayerOptions[ciRC_CLIENT_OPTION_PARTNER] = 1
				//set they want to be a drive.
				g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[0].iPlayerOptions[ciRC_CLIENT_OPTION_ROLE] = 0
				g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[1].iPlayerOptions[ciRC_CLIENT_OPTION_ROLE] = 0
				#IF IS_DEBUG_BUILD
					IF serverBD.bForceSameTeamGTA
					OR g_IgnoreTeamGTARaceCoronaRules
						g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[0].iPlayerBalancedTeam = 0
						g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[1].iPlayerBalancedTeam = 0
						//Set then want to play with them selves
						g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[0].iPlayerOptions[ciRC_CLIENT_OPTION_PARTNER] = 1
						g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[1].iPlayerOptions[ciRC_CLIENT_OPTION_PARTNER] = 0
						//set they want to be a drive.
						g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[0].iPlayerOptions[ciRC_CLIENT_OPTION_ROLE] = 0
						g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[1].iPlayerOptions[ciRC_CLIENT_OPTION_ROLE] = 1
					ENDIF
				#ENDIF
			//Check that no one is alone
			ELSE
				INT iBitSetTemp
				//If they are make them not a codriver
				//Loop all players
				FOR iLoop = 0 TO (g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers - 1)
					//Check agains the other players
					FOR iLoopTwo = 0 TO (g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers - 1)
						//If the teams are the same then set a bit
						IF g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iLoop].iPlayerBalancedTeam = g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iLoopTwo].iPlayerBalancedTeam
							PRINTLN("[TRANSITION SESSIONS] [TEAM BAL DATA] - Player ", iLoop, " is Not alone")
							SET_BIT(iBitSetTemp, g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iLoop].iPlayerBalancedTeam)
						ENDIF
					ENDFOR
				ENDFOR
				//Loop all players
				FOR iLoop = 0 TO (g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers - 1)
					//check that the player is not alone
					IF NOT IS_BIT_SET(iBitSetTemp, g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iLoop].iPlayerBalancedTeam)
						//They are, poor bastartd
						//If they are a co driver then make them a driver!
						IF g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iLoop].iPlayerOptions[ciRC_CLIENT_OPTION_ROLE] = 1
							g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iLoop].iPlayerOptions[ciRC_CLIENT_OPTION_ROLE] = 0
							PRINTLN("[TRANSITION SESSIONS] [TEAM BAL DATA] - Player ", iLoop, "is lone player set as a codriver! g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iLoop].iPlayerOptions[ciRC_CLIENT_OPTION_ROLE] = 1")
						ENDIF
					ENDIF
				ENDFOR
			ENDIF
		ENDIF
	ENDIF
	//print it all out
	#IF IS_DEBUG_BUILD
		FOR iLoop = 0 TO (g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers - 1)
			PRINTLN("[TRANSITION SESSIONS] [TEAM BAL DATA] - g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[", iLoop, "].iPlayerBalancedTeam = ", g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iLoop].iPlayerBalancedTeam)
			PRINTLN("[TRANSITION SESSIONS] [TEAM BAL DATA] - g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[", iLoop, "].iPlayerOptions[0]   = ", g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iLoop].iPlayerOptions[0])
			PRINTLN("[TRANSITION SESSIONS] [TEAM BAL DATA] - g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[", iLoop, "].iPlayerOptions[1]   = ", g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iLoop].iPlayerOptions[1])
		ENDFOR		
		INT iLoop2
		PRINTLN("[TRANSITION SESSIONS] [TEAM BAL DATA] - ")							
		PRINTLN("[TRANSITION SESSIONS] [TEAM BAL DATA] - ")							
		FOR iLoop2 = 0 TO (serverBDpassed.iNumberOfTeams - 1)
			PRINTLN("[TRANSITION SESSIONS] [TEAM BAL DATA] - TEAM ", iLoop2)
			FOR iLoop = 0 TO (g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers - 1)
				IF  g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iLoop].iPlayerOptions[0] = iLoop2
					PRINTLN("[TRANSITION SESSIONS] [TEAM BAL DATA] - ", iLoop)					
				ENDIF
			ENDFOR		
			PRINTLN("[TRANSITION SESSIONS] [TEAM BAL DATA] - ")							
		ENDFOR		
	#ENDIF
ENDPROC
//+-----------------------------------------------------------------------------+
//¦				BALANCING	RACE												¦
//+-----------------------------------------------------------------------------+
FUNC BOOL SERVER_SETS_RACE_TEAMS(ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[]) // ROWAN

	INT iparticipant
//	PARTICIPANT_INDEX tempPart
//	PLAYER_INDEX playerId
	INT j
	INT iPreferredPartner = -1
	BOOL bFoundTeamMate = FALSE
	
	SWITCH GET_BALANCE_STATE(serverBDpassed)
		
		CASE TEAM_BALANCE_PUT_CREW_TOGETHER
			GOTO_TEAM_BALANCE_STAGE(serverBDpassed, TEAM_BALANCE_COUNT_TOTAL_PLAYERS)
		BREAK
		
		CASE TEAM_BALANCE_COUNT_TOTAL_PLAYERS
			PRINTLN("[TEAM BAL DATA] - g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers = ", g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers)
			GOTO_TEAM_BALANCE_STAGE(serverBDpassed, TEAM_BALANCE_INITIALISE)
		BREAK
		
		// Just intialise all the teams as -1
		CASE TEAM_BALANCE_INITIALISE
			
			REPEAT g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers iparticipant			
				serverBDpassed.iTempBalancedTeam[iparticipant] = -1				
				PRINTLN("[TEAM BAL DATA] - serverBDpassed.iTempBalancedTeam[", iparticipant, "] =  ", serverBDpassed.iTempBalancedTeam[iparticipant])
			ENDREPEAT
			REPEAT g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers iparticipant			
				PRINTLN("[TEAM BAL DATA] - playerBDPassed[", iparticipant, "].iPPrace =  ", playerBDPassed[iparticipant].iPPrace)
				IF playerBDPassed[iparticipant].iPPrace = iparticipant
					playerBDPassed[iparticipant].iPPrace = TEAM_RACE_CHOSE_ANY
					PRINTLN("[TEAM BAL DATA] - ", iparticipant, " PREFERS HIMSELF SETTING TO TEAM_RACE_CHOSE_ANY")
				ENDIF
				PRINTLN("[TEAM BAL DATA] - playerBDPassed[", iparticipant, "].iProle  =  ", playerBDPassed[iparticipant].iProle)
			ENDREPEAT
			GOTO_TEAM_BALANCE_STAGE(serverBDpassed, TEAMS_MUTUAL_PARTNERS)
		BREAK
		
		// Players chose each other in corona
		CASE TEAMS_MUTUAL_PARTNERS

			REPEAT g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers iparticipant
				IF serverBDpassed.iTempBalancedTeam[iparticipant] = -1
					PRINTLN("[TEAM BAL DATA] - TEAMS_MUTUAL_PARTNERS - iparticipant = ", iparticipant)
					iPreferredPartner = (playerBDPassed[iparticipant].iPPrace)
					IF serverBD.iMissionType = FMMC_TYPE_RACE
						IF serverBDPassed.iMissionSubType = ciRACE_SUB_TYPE_RALLY
							IF iPreferredPartner = TEAM_RACE_CHOSE_NONE
								iPreferredPartner = TEAM_RACE_CHOSE_ANY
								PRINTLN("iPreferredPartner = TEAM_RACE_CHOSE_NONE")
								SCRIPT_ASSERT("iPreferredPartner = TEAM_RACE_CHOSE_NONE")
							ENDIF
						ENDIF
					ENDIF
						
					IF iPreferredPartner >= 0															
						PRINTLN("[TEAM BAL DATA] - TEAMS_MUTUAL_PARTNERS playerBDPassed[", iPreferredPartner, "].iPPrace (iPreferredPartner) = ", playerBDPassed[iPreferredPartner].iPPrace)
						// PAIR PEOPLE WHO CHOSE EACH OTHER 
						IF playerBDPassed[iPreferredPartner].iPPrace = iparticipant	// If like each other
						AND playerBDPassed[iPreferredPartner].iPPrace <> TEAM_RACE_CHOSE_ANY		// And no one picked 'any'
						AND serverBDpassed.iTempBalancedTeam[iPreferredPartner] = -1 				// Not already got a team

							serverBDpassed.iTempBalancedTeam[iparticipant] 		= sLocalVars.iTeamsCountBalancing
							
							serverBDpassed.iTempBalancedTeam[iPreferredPartner] = sLocalVars.iTeamsCountBalancing
							
							PRINTNL()
							PRINTLN("[TEAM BAL DATA] - TEAMS_MUTUAL_PARTNERS // SETTING TEAM")
							PRINTLN("[TEAM BAL DATA] - TEAMS_MUTUAL_PARTNERS iTempBalancedTeam = ", serverBDpassed.iTempBalancedTeam[iparticipant], " iparticipant      = ", iparticipant)
							PRINTLN("[TEAM BAL DATA] - TEAMS_MUTUAL_PARTNERS iTempBalancedTeam = ", serverBDpassed.iTempBalancedTeam[iPreferredPartner], " iPreferredPartner = ", iparticipant)
							PRINTNL()
							
							sLocalVars.iTeamsCountBalancing++
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
			#IF IS_DEBUG_BUILD
			REPEAT g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers iparticipant
				PRINTLN("[TEAM BAL DATA] - TEAMS_MUTUAL_PARTNERS serverBDpassed.iTempBalancedTeam[", iparticipant, "] = ", serverBDpassed.iTempBalancedTeam[iparticipant])
			ENDREPEAT
			#ENDIF
			GOTO_TEAM_BALANCE_STAGE(serverBDpassed, RACE_TEAMS_UNREQUITED)
		BREAK
		
		// These players chose a player who didn't choose them
		CASE RACE_TEAMS_UNREQUITED
			REPEAT g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers iparticipant
				IF serverBDpassed.iTempBalancedTeam[iparticipant] = -1
					PRINTLN("[TEAM BAL DATA] - RACE_TEAMS_UNREQUITED - iparticipant = ", iparticipant)
					iPreferredPartner = playerBDPassed[iparticipant].iPPrace
					
					IF iPreferredPartner >=0
					PRINTLN("[TEAM BAL DATA] - RACE_TEAMS_UNREQUITED playerBDPassed[", iPreferredPartner, "].iPPrace (iPreferredPartner) = ", playerBDPassed[iPreferredPartner].iPPrace)
					ENDIF
					PRINTLN("[TEAM BAL DATA] - RACE_TEAMS_UNREQUITED playerBDPassed[", iparticipant, "].iPPrace (iparticipant)      = ", playerBDPassed[iparticipant].iPPrace)
						
					IF iPreferredPartner >= 0							
						// PAIR PEOPLE WHO CHOSE ONE 
						IF serverBDpassed.iTempBalancedTeam[iPreferredPartner] = -1

							serverBDpassed.iTempBalancedTeam[iparticipant] = sLocalVars.iTeamsCountBalancing
							
							serverBDpassed.iTempBalancedTeam[iPreferredPartner] = sLocalVars.iTeamsCountBalancing	
							
							PRINTNL()
							PRINTLN("[TEAM BAL DATA] - RACE_TEAMS_UNREQUITED // SETTING TEAM")
							PRINTLN("[TEAM BAL DATA] - RACE_TEAMS_UNREQUITED iTempBalancedTeam = ", serverBDpassed.iTempBalancedTeam[iparticipant], " iparticipant = ", iparticipant)
							PRINTLN("[TEAM BAL DATA] - RACE_TEAMS_UNREQUITED iTempBalancedTeam = ", serverBDpassed.iTempBalancedTeam[iPreferredPartner], " iPreferredPartner = ", iparticipant)
														
							PRINTNL()
							
							sLocalVars.iTeamsCountBalancing++
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
			#IF IS_DEBUG_BUILD
			REPEAT g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers iparticipant
				PRINTLN("[TEAM BAL DATA] - RACE_TEAMS_UNREQUITED serverBDpassed.iTempBalancedTeam[", iparticipant, "] = ", serverBDpassed.iTempBalancedTeam[iparticipant])
			ENDREPEAT
			#ENDIF
			GOTO_TEAM_BALANCE_STAGE(serverBDpassed, RACE_TEAMS_UGLY)
		BREAK
		
		// Everyone else
		CASE RACE_TEAMS_UGLY
			REPEAT g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers iparticipant
				IF serverBDpassed.iTempBalancedTeam[iparticipant] = -1
					bFoundTeamMate = FALSE
					PRINTLN("[TEAM BAL DATA] - RACE_TEAMS_UGLY - iparticipant = ", iparticipant)
					PRINTLN("[TEAM BAL DATA] - RACE_TEAMS_UGLY playerBDPassed[", iparticipant, "].iPPrace      = ", playerBDPassed[iparticipant].iPPrace)
					REPEAT g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers j
						//////// NEED ONE PLAYER CHECK HERE FOR J ELSE NO TEAMS GET SET
						IF j <> iparticipant
							IF serverBDpassed.iTempBalancedTeam[j] = -1
							AND serverBDpassed.iTempBalancedTeam[iparticipant] = -1
							AND bFoundTeamMate = FALSE
								serverBDpassed.iTempBalancedTeam[iparticipant] = sLocalVars.iTeamsCountBalancing					
								serverBDpassed.iTempBalancedTeam[j] = sLocalVars.iTeamsCountBalancing
								
								PRINTNL()
								PRINTLN("[TEAM BAL DATA] - RACE_TEAMS_UGLY // SETTING TEAM")
								PRINTLN("[TEAM BAL DATA] - RACE_TEAMS_UGLY iTempBalancedTeam = ", serverBDpassed.iTempBalancedTeam[iparticipant], " iparticipant = ", iparticipant)
								PRINTLN("[TEAM BAL DATA] - RACE_TEAMS_UGLY iTempBalancedTeam = ", serverBDpassed.iTempBalancedTeam[j], " j = ", iparticipant)
								PRINTNL()
								
								sLocalVars.iTeamsCountBalancing++
								bFoundTeamMate = TRUE
							ENDIF
						ENDIF
					ENDREPEAT
				ENDIF
			ENDREPEAT			
			#IF IS_DEBUG_BUILD
			REPEAT g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers iparticipant
				PRINTLN("[TEAM BAL DATA] - RACE_TEAMS_UGLY serverBDpassed.iTempBalancedTeam[", iparticipant, "] = ", serverBDpassed.iTempBalancedTeam[iparticipant])
			ENDREPEAT
			#ENDIF
			GOTO_TEAM_BALANCE_STAGE(serverBDpassed, TEAM_BALANCE_ASSIGN)
		BREAK	
		
		// Anyone who still doesn't have a team, give them one
		CASE TEAM_BALANCE_ASSIGN
			REPEAT g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers iparticipant
				IF serverBDpassed.iTempBalancedTeam[iparticipant] = -1						
					PRINTLN("[TEAM BAL DATA] - TEAM_BALANCE_ASSIGN playerBDPassed[", iparticipant, "].iPPrace      = ", playerBDPassed[iparticipant].iPPrace)
					serverBDpassed.iTempBalancedTeam[iparticipant] 	= sLocalVars.iTeamsCountBalancing				
					PRINTNL()
					PRINTLN("[TEAM BAL DATA] - TEAM_BALANCE_ASSIGN // SETTING TEAM")
					PRINTLN("[TEAM BAL DATA] - TEAM_BALANCE_ASSIGN iTempBalancedTeam = ", serverBDpassed.iTempBalancedTeam[iparticipant], " iparticipant = ", iparticipant)
					PRINTNL()
					sLocalVars.iTeamsCountBalancing++
				ENDIF
			ENDREPEAT
			
			REPEAT g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers iparticipant
				serverBDpassed.iBalancedTeam[iparticipant] = serverBDpassed.iTempBalancedTeam[iparticipant] 	
				//PRINTLN("[TEAM BAL DATA] - serverBDpassed.iBalancedTeam[", iparticipant, "] = ", serverBDpassed.iBalancedTeam[iparticipant])
			ENDREPEAT
			
//			#IF IS_DEBUG_BUILD
//			REPEAT g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers iparticipant
//				PRINTLN("[TEAM BAL DATA] - g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[", iParticipant, "].iPlayerOptions[ciRC_CLIENT_OPTION_PARTNER] = ", g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iParticipant].iPlayerOptions[ciMISSION_CLIENT_OPTION_TEAM])
//			ENDREPEAT
//			#ENDIF
			
			GOTO_TEAM_BALANCE_STAGE(serverBDpassed, TEAM_BALANCE_FINISH)
			RETURN TRUE
		
		// Finish
		CASE TEAM_BALANCE_FINISH
			PRINTLN("[TEAM BAL DATA] - SERVER_SETS_RACE_TEAMS CASE 4, RETURN TRUE")
			RETURN TRUE
			
	ENDSWITCH

	RETURN FALSE
ENDFUNC



FUNC BOOL SERVER_TEAM_BALANCING_RACE(ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[])

	IF NOT IS_THIS_TEAM_RACE(serverBDpassed)
		PRINTLN("[TEAM BAL DATA] - NOT IS_THIS_TEAM_RACE(serverBDpassed)")
		SET_BIT(serverBDpassed.iServerBitSet, SERV_BITSET_STOP_TEAM_BALANCING)
		RETURN TRUE
	ENDIF
	
	IF NOT IS_BIT_SET(serverBDpassed.iServerBitSet, SERV_BITSET_STOP_TEAM_BALANCING)
		IF SERVER_SETS_RACE_TEAMS(serverBDpassed, playerBDPassed)
			PRINTLN("[TEAM BAL DATA] - SERVER_TEAM_BALANCING, SERV_BITSET_STOP_TEAM_BALANCING")
			SET_BIT(serverBDpassed.iServerBitSet, SERV_BITSET_STOP_TEAM_BALANCING)
			RETURN TRUE
		//ELSE
			//PRINTLN("[TEAM BAL DATA] - SERVER_SETS_RACE_TEAMS ")
		ENDIF
	ELSE
		PRINTLN("[TEAM BAL DATA] - IS_BIT_SET(serverBDpassed.iServerBitSet, SERV_BITSET_STOP_TEAM_BALANCING)")
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC INT GET_TEAMMATE_PLAYER(INT iLoop, ServerBroadcastData &serverBDpassed)
	INT iParticipant
	INT returnPlayer = - 1 //INVALID_PLAYER_INDEX()
	REPEAT g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers iParticipant
		IF iLoop <> iParticipant
			IF serverBDpassed.iBalancedTeam[iparticipant] = serverBDpassed.iBalancedTeam[iLoop] 
				returnPlayer = iParticipant
			ENDIF
		ENDIF
	ENDREPEAT
	#IF IS_DEBUG_BUILD
	IF returnPlayer = -1
		PRINTLN("[TEAM BAL DATA] - GET_TEAMMATE_PLAYER iLoop = ", iLoop ," - returnPlayer = -1")
//		SCRIPT_ASSERT("[TEAM BAL DATA] - GET_TEAMMATE_PLAYER - returnPlayer = -1")
		returnPlayer = iLoop // Return the player if no teammate 
	ENDIF
	#ENDIF
	RETURN returnPlayer
ENDFUNC


			
//PROC ASSIGN_PLAYER_TEAM_RACE(ServerBroadcastData &serverBDpassed)
//
//	IF NOT IS_THIS_TEAM_RACE(serverBDpassed)
//		EXIT
//	ENDIF
//	
//	INT iLoop
//	FOR iLoop = 0 TO (g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers - 1)
//		IF serverBDpassed.iBalancedTeam[iLoop] <> -1
//			PRINTLN("[TEAM BAL DATA] - SET_BALANCED_TEAM = ", serverBDpassed.iBalancedTeam[iLoop])
//			g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iLoop].iPlayerOptions[ciMISSION_CLIENT_OPTION_TEAM] = serverBDpassed.iBalancedTeam[iLoop]
//			PRINTLN("[TEAM BAL DATA] - SET_PED_CAN_BE_TARGETTED_BY_TEAM, false ", serverBDpassed.iBalancedTeam[iLoop])
//		ENDIF
//	ENDFOR
//ENDPROC

PROC SET_RACE_TEAM_VARIABLES(ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[])
	IF NOT IS_THIS_TEAM_RACE(serverBDpassed)
		EXIT
	ENDIF

	INT iLoop
	FOR iLoop = 0 TO (g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers - 1)
		// Store local team for leaderboards
		//playerBDPassed[iLoop].iMenuTeam = g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iLoop].iPlayerOptions[ciMISSION_CLIENT_OPTION_TEAM]
		//PRINTLN("[TEAM BAL DATA] - SET_RACE_TEAM_VARIABLES, iMenuTeam = ", playerBDPassed[iLoop].iMenuTeam)
		// Grab teammate INT
		playerBDPassed[iLoop].iTeammate =  GET_TEAMMATE_PLAYER(iLoop, serverBDpassed)
	ENDFOR
ENDPROC

PROC SET_CLIENT_BITSET_SETUP_RALLY_DRIVER()	
	SET_RACE_DRIVER_GLOBAL(TRUE)
	SET_BIT(sLocalVars.iLocalBitSet, CLIENT_BITSET_SETUP_RALLY_DRIVER)
	PRINTLN("[TEAM BAL DATA] - SET_CLIENT_BITSET_SETUP_RALLY_DRIVER ")
ENDPROC

PROC SET_CLIENT_BITSET_SETUP_RALLY_PASSENGER()
	SET_BIT(sLocalVars.iLocalBitSet, CLIENT_BITSET_SETUP_RALLY_PASSENGER)
	SET_RACE_PASSENGER_GLOBAL(TRUE)
	PRINTLN("[TEAM BAL DATA] - SET_CLIENT_BITSET_SETUP_RALLY_PASSENGER ")
ENDPROC
PROC ASSIGN_PLAYER_SEAT_POSITION(ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[])

	IF NOT IS_THIS_TEAM_RACE(serverBDpassed)
		EXIT
	ENDIF
	
	INT iLoop
	FOR iLoop = 0 TO (g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers - 1)
		INT iTeammateINT = playerBDPassed[iLoop].iTeammate
		IF iTeammateINT != - 1
//			// (1379851) Give players with custom vehicles driver pos
//			IF DOES_TRANSITION_PLAYER_HAVE_CUSTOM_VEHICLE_FOR_CLASS(iLoop, g_sTransitionSessionOptions.iSelection[ciRC_HOST_OPTION_VEHICLE_CLASS])
//			AND NOT DOES_TRANSITION_PLAYER_HAVE_CUSTOM_VEHICLE_FOR_CLASS(iTeammateINT, g_sTransitionSessionOptions.iSelection[ciRC_HOST_OPTION_VEHICLE_CLASS])
//				PRINTLN("[TEAM BAL DATA] - ASSIGN_PLAYER_SEAT_POSITION, Giving_player_driver_as_he_has_custom_veh ", iLoop)
//				SET_CLIENT_BITSET_SETUP_RALLY_DRIVER()	
//				playerBDPassed[iLoop].iProle = ciPREFERRED_POSITION_DRIVER
//				playerBDPassed[iTeammateINT].iProle = ciPREFERRED_POSITION_PASSENGER
//			ENDIF
			IF playerBDPassed[iLoop].iProle = ciPREFERRED_POSITION_DRIVER
				IF playerBDPassed[iTeammateINT].iProle = ciPREFERRED_POSITION_DRIVER
					IF playerBDPassed[iLoop].iTeammate = -1
						playerBDPassed[iLoop].iProle = ciPREFERRED_POSITION_DRIVER
					ELSE
						IF iLoop <= iTeammateINT // randomise
							playerBDPassed[iLoop].iProle = ciPREFERRED_POSITION_DRIVER
						ELSE
							playerBDPassed[iLoop].iProle = ciPREFERRED_POSITION_PASSENGER
						ENDIF
					ENDIF
				ELSE
					SET_CLIENT_BITSET_SETUP_RALLY_DRIVER()	
				ENDIF
			ELSE
				IF playerBDPassed[iTeammateINT].iProle = ciPREFERRED_POSITION_PASSENGER
					IF playerBDPassed[iLoop].iTeammate = -1
						playerBDPassed[iLoop].iProle = ciPREFERRED_POSITION_DRIVER
					ELSE
						IF iLoop <= iTeammateINT
							playerBDPassed[iLoop].iProle = ciPREFERRED_POSITION_PASSENGER
						ELSE
							playerBDPassed[iLoop].iProle = ciPREFERRED_POSITION_DRIVER
						ENDIF
					ENDIF
				ELSE
					playerBDPassed[iLoop].iProle = ciPREFERRED_POSITION_PASSENGER
				ENDIF
			ENDIF

		ELSE
			playerBDPassed[iLoop].iProle = ciPREFERRED_POSITION_DRIVER
		ENDIF
	ENDFOR
	FOR iLoop = 0 TO (g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers - 1)
		PRINTLN("[TEAM BAL DATA] - ASSIGN_PLAYER_SEAT_POSITION playerBDPassed[", iLoop, "].iProle    = ", playerBDPassed[iLoop].iProle)
		PRINTLN("[TEAM BAL DATA] - ASSIGN_PLAYER_SEAT_POSITION playerBDPassed[", iLoop, "].iTeammate = ", playerBDPassed[iLoop].iTeammate)
	ENDFOR
ENDPROC

FUNC BOOL HAS_SERVER_BALANCED_TEAMS(ServerBroadcastData &serverBDpassed)
	RETURN IS_BIT_SET(serverBDpassed.iServerBitSet, SERV_BITSET_STOP_TEAM_BALANCING)
ENDFUNC

PROC PROCESS_PRE_GAME(FMMC_CORONA_DATA_MAINTAIN_SCRIPT_VARS &fmmcCoronaMissionData)	
	#IF IS_DEBUG_BUILD
	PRINTLN("[TEAM BAL DATA] - --        FM_maintain_transition_players        --")
	#ENDIF
	
	//Clear old stuff
	CLEAR_MY_GLOBALS_HAVE_BEEN_TEAM_BALANCED()
	CLEAR_ALL_GLOBALS_HAVE_BEEN_TEAM_BALANCED()
	CLEAR_TRANSITION_SESSION_TEAMS_BALANCED()
	CLEAR_TRANSITION_SESSION_SCRIPT_HAS_BALANCED_TEAMS()
	
	//Set up this script
	NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()
	SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	
	IF NOT HAS_ON_CALL_TRANSITION_SESSION_BEEN_SET_UP()
		SET_TRANSITION_SESSION_AUDIO_SCENE(fmmcCoronaMissionData, TRUE)
	ENDIF
	
	INT iMyGbd 	= NATIVE_TO_INT(PLAYER_ID())
	//serverBD.iInstanceId 					= fmmcMissionData.iInstanceId
	serverBD.iMissionType					= fmmcCoronaMissionData.iMissionType
	//serverBD.bTutorial						= fmmcCoronaMissionData.bTutorial
	serverBD.iMaxNumperOfPart				= g_FMMC_STRUCT.iNumParticipants
	serverBD.iMissionSubType				= 0 //Comes from the corona
	serverBD.iNumberOfTeams					= g_sTransitionSessionOptions.iSelection[ciDM_HOST_OPTION_NUM_TEAMS]
	IF SVM_FLOW_IS_CURRENT_MISSION_SVM_FLOW()
	AND IS_CORONA_INITIALISING_A_QUICK_RESTART()
		serverBD.iNumberOfTeams	= g_FMMC_STRUCT.iNumberOfTeams 
	ENDIF
	serverBD.iTeamBalancing					= DM_TEAM_BALANCING_ON// g_sTransitionSessionOptions.iSelection[ciDM_HOST_OPTION_TEAM_BAL]
	IF serverBD.iNumberOfTeams = 0
		serverBD.iTeamBalancing	= DM_TEAM_BALANCING_OFF
	ENDIF
	IF g_sTransitionSessionOptions.iSelection[ciRC_HOST_OPTION_GTA_TEAM] = ciRACE_GTA_TEAMS_OFF
		serverBD.bTeamGtaRace = FALSE
	ELSE
		serverBD.bTeamGtaRace = TRUE
	ENDIF
	serverBD.bPlayListCorona				= IS_PLAYER_ON_A_PLAYLIST_INT(iMyGbd)
	
	IF serverBD.iMissionType = FMMC_TYPE_MISSION
	//AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_DCTL(g_FMMC_STRUCT.iAdversaryModeType)
		serverBD.bForceSmallerTeams	= IS_BIT_SET(g_FMMC_STRUCT.iBS_MissionTeamRelations, FMMC_TEAM_BALANCING)
	ELSE
		serverBD.bForceSmallerTeams = FALSE
	ENDIF
	
	IF fmmcCoronaMissionData.bIsHeistMission = TRUE
		SET_CORONA_BIT(CORONA_NO_RADIO)
		PRINTLN("FM_MAINTAIN_TRANSITION_PLAYERS - PROCESS_PRE_GAME - CORONA_NO_RADIO SET")
	ENDIF
	
	//Print out stuff
	#IF IS_DEBUG_BUILD
	PRINTLN("[TEAM BAL DATA] - PROCESS_PRE_GAME - at the UNIX TIME - (", GET_CLOUD_TIME_AS_INT(),")")
	//PRINTLN("[TEAM BAL DATA] - serverBD.iInstanceId            = ", serverBD.iInstanceId)
	PRINTLN("[TEAM BAL DATA] - serverBD.iMissionType           = ", serverBD.iMissionType)
	PRINTLN("[TEAM BAL DATA] - serverBD.iMissionSubType        = ", serverBD.iMissionSubType)
	PRINTLN("[TEAM BAL DATA] - serverBD.iNumberOfTeams         = ", serverBD.iNumberOfTeams)
	PRINTLN("[TEAM BAL DATA] - serverBD.iTeamBalancing         = ", serverBD.iTeamBalancing)
	PRINTLN("[TEAM BAL DATA] - serverBD.bPlayListCorona        = ", serverBD.bPlayListCorona)
	PRINTLN("[TEAM BAL DATA] - serverBD.bForceSmallerTeams     = ", serverBD.bForceSmallerTeams)
	#ENDIF
	// This script will not be paused if another script calls PAUSE_GAME
ENDPROC


//Should this script quit?
FUNC BOOL SHOULD_CORONA_SCRIPT_TERMINATE()
	CORONA_STATUS_ENUM eCoronaStatus = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].eCoronaStatus
	SWITCH eCoronaStatus
		CASE CORONA_STATUS_IDLE
			IF NOT HAS_ON_CALL_TRANSITION_SESSION_BEEN_SET_UP()
				PRINTLN("[TS] CORONA_STATUS_IDLE")
				RETURN TRUE
			ENDIF
		BREAK
			
		CASE CORONA_STATUS_WALK_OUT			
			PRINTLN("[TS] CORONA_STATUS_WALK_OUT")
			RETURN TRUE
			
		CASE CORONA_STATUS_QUIT		
			PRINTLN("[TS] CORONA_STATUS_QUIT")
			RETURN TRUE
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_DO_HEIST_CORONA_CAM(FMMC_CORONA_DATA_MAINTAIN_SCRIPT_VARS& fmmcCoronaMissionData)

	BOOL bDoCam = TRUE

	IF fmmcCoronaMissionData.bIsHeistMission
	
		PRINTLN("[AMEC][HEIST_MISC] - SHOULD_DO_HEIST_CORONA_CAM - fmmcCoronaMissionData.bIsHeistMission = TRUE, check if heist cam is needed.")
		
		IF GET_HEIST_PROPERTY_CAM_STAGE() != HEIST_PROPERTY_START
		AND GET_HEIST_PROPERTY_CAM_STAGE() != HEIST_PROPERTY_FINISHED
			PRINTLN("[AMEC][HEIST_MISC] - SHOULD_DO_HEIST_CORONA_CAM - Heist property cam is NOT in idle state: ", GET_HEIST_PROPERTY_CAM_STATE_NAME(GET_HEIST_PROPERTY_CAM_STAGE()))
			bDoCam = FALSE
		ENDIF
		
		IF IS_HEIST_ANIM_PLAYING(g_HeistSharedClient.sHeistAnimFirst)
		OR IS_HEIST_ANIM_PLAYING(g_HeistSharedClient.sHeistAnimSecond)
		OR IS_HEIST_ANIM_PLAYING(g_HeistSharedClient.sHeistAnimThird)
			PRINTLN("[AMEC][HEIST_MISC] - SHOULD_DO_HEIST_CORONA_CAM - A heist intro animation is playing, leave camera focus alone.")
			bDoCam = FALSE
		ENDIF
		
		IF GET_HEIST_TUTORIAL_MID_CUTSCENE_PRELOAD()
			PRINTLN("[AMEC][HEIST_MISC] - SHOULD_DO_HEIST_CORONA_CAM - GET_HEIST_TUTORIAL_MID_CUTSCENE_PRELOAD = TRUE, don't need camera/postFX.")
			bDoCam = FALSE
		ENDIF

	ENDIF
	
	RETURN bDoCam

ENDFUNC


BOOL bScriptNeedsLaunched

//Relaunch the main freemode script
PROC MAINTAIN_LAUNCHING_FREEMODE_SCRIPT(FMMC_CORONA_DATA_MAINTAIN_SCRIPT_VARS &fmmcCoronaMissionData)
	IF bScriptNeedsLaunched = TRUE
		SET_CURRENT_GAMEMODE(GAMEMODE_FM) 				
		IF NOT NETWORK_IS_SCRIPT_ACTIVE(GET_LAUNCH_SCRIPT_NAME(), -1, TRUE)
			PRINTLN("FM_MAINTAIN_TRANSITION_PLAYERS - GET_FRAME_COUNT START_LAUNCH_SCRIPT = ", GET_FRAME_COUNT())
			IF START_LAUNCH_SCRIPT()
				DISABLE_SCRIPT_BRAIN_SET(SCRIPT_BRAIN_GROUP_SINGLE_PLAYER)
				DISABLE_SCRIPT_BRAIN_SET(SCRIPT_BRAIN_GROUP_CNC)
				ENABLE_SCRIPT_BRAIN_SET(SCRIPT_BRAIN_GROUP_MULTIPLAYER)
				ENABLE_SCRIPT_BRAIN_SET(SCRIPT_BRAIN_GROUP_FREEMODE)
				ENTERING_FRESH_GAME()
				bScriptNeedsLaunched = FALSE
			ENDIF
		//ELSE
			//PRINTLN("[TS] NETWORK_IS_SCRIPT_ACTIVE(GET_LAUNCH_SCRIPT_NAME(), -1, TRUE) = TRUE")
		ENDIF
	ELSE
		//If this is an activity session
		IF NETWORK_IS_ACTIVITY_SESSION()
		//And we've got the end session event then load
		AND HAS_TRANSITION_GOT_END_LAUNCH_EVENT_FOR_MAINTAIN_SCRIPT()
		//And a transition session is launching
		AND IS_TRANSITION_SESSION_LAUNCHING()
			PRINTLN("[TS] NETWORK_IS_ACTIVITY_SESSION - HAS_TRANSITION_GOT_END_LAUNCH_EVENT_FOR_MAINTAIN_SCRIPT - IS_TRANSITION_SESSION_LAUNCHING - bScriptNeedsLaunched = FALSE")
			PRINTLN("FM_MAINTAIN_TRANSITION_PLAYERS - GET_FRAME_COUNT bScriptNeedsLaunched = ", GET_FRAME_COUNT())
			SET_CURRENT_GAMEMODE(GAMEMODE_FM)
			REQUEST_FM_RESTART_SCRIPTS(fmmcCoronaMissionData.bIsHeistMission)
			bScriptNeedsLaunched = TRUE			
		ENDIF
	ENDIF
ENDPROC

//
PROC MAINTAIN_SCRIPT_CLEANUP_CALLS(INT iMissionType, FMMC_CORONA_DATA_MAINTAIN_SCRIPT_VARS &fmmcCoronaMissionData)
	IF NOT IS_TRANSITION_SESSION_LAUNCHING()
		// If we have a match end event, bail.
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			PRINTLN("[TS] SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE - MAINTAIN_SCRIPT_CLEANUP_CALLS")
			SCRIPT_CLEANUP(iMissionType, fmmcCoronaMissionData)
		ENDIF
		// If we are not in the corona then put the script in the bin. 
		IF NOT HAS_ON_CALL_TRANSITION_SESSION_BEEN_SET_UP()
			IF NOT IS_PLAYER_IN_CORONA()
				PRINTLN("[TS] NOT IS_PLAYER_IN_CORONA() - MAINTAIN_SCRIPT_CLEANUP_CALLS")
				SCRIPT_CLEANUP(iMissionType, fmmcCoronaMissionData)
			ENDIF
		ENDIF
		
		// If we are cleaning up an on call state. Clear this script now
		IF SHOULD_TRANSITION_SESSIONS_CLEAN_UP_ON_CALL_NOW()
			PRINTLN("[TS] SHOULD_TRANSITION_SESSIONS_CLEAN_UP_ON_CALL_NOW() - MAINTAIN_SCRIPT_CLEANUP_CALLS")
			SCRIPT_CLEANUP(iMissionType, fmmcCoronaMissionData)
		ENDIF
		
		IF SHOULD_CORONA_SCRIPT_TERMINATE()
			PRINTLN("[TS] SHOULD_CORONA_SCRIPT_TERMINATE() - MAINTAIN_SCRIPT_CLEANUP_CALLS")
			SCRIPT_CLEANUP(iMissionType, fmmcCoronaMissionData)
		ENDIF
	ELSE
		//We got a bail event then get out.
		IF HAS_TRANSITION_SESSIONS_RECEIVED_BAIL_EVENT()
			PRINTLN("[TS] HAS_TRANSITION_SESSIONS_RECEIVED_BAIL_EVENT() - MAINTAIN_SCRIPT_CLEANUP_CALLS")
			PRINTLN("[TS] HAS_TRANSITION_SESSIONS_RECEIVED_BAIL_EVENT() - SET_FRONTEND_ACTIVE(FALSE)")
			
			// Make sure we kill the frontend corona immediately
			SET_FRONTEND_ACTIVE(FALSE)
			
			RESET_TRANSITION_SESSION_NON_RESET_VARS()
			CLEAR_TRANSITION_SESSIONS_BITSET()
			CLEAR_TRANSITION_SESSION_LAUNCHING()
			SCRIPT_CLEANUP(-1, fmmcCoronaMissionData)
		ENDIF
	ENDIF
	IF SHOULD_TRANSITION_SESSIONS_CLEANUP_CORONA_SCRIPT()
		PRINTLN("[TS] SHOULD_CORONA_SCRIPT_TERMINATE() - SHOULD_TRANSITION_SESSIONS_CLEANUP_CORONA_SCRIPT()")
		SCRIPT_CLEANUP(-1, fmmcCoronaMissionData)			
	ENDIF
	IF IS_TRANSITION_SESSIONS_ACCEPTING_INVITE_WAIT_FOR_CLEAN()
		PRINTLN("[TS] SHOULD_CORONA_SCRIPT_TERMINATE() - MAINTAIN_SCRIPT_CLEANUP_CALLS")
		SCRIPT_CLEANUP(-1, fmmcCoronaMissionData)	
	ENDIF
	
	IF RECEIVED_EVENT(EVENT_NETWORK_END_MATCH)	
		NET_PRINT_TIME() NET_PRINT_THREAD_ID() NET_PRINT(GET_THIS_SCRIPT_NAME())
		NET_PRINT(" SHOULD_THIS_MULTIPLAYER_SCRIPT_TERMINATE RECEIVED match end event - MAINTAIN_SCRIPT_CLEANUP_CALLS") NET_NL()		
		SCRIPT_CLEANUP(-1, fmmcCoronaMissionData)
	ENDIF
ENDPROC

/// PURPOSE: Waits for global flag from corona to indicate music should begin
PROC MAINTAIN_PERSISTENT_CORONA_MUSIC()
	
	IF IS_CORONA_BIT_SET(CORONA_TRIGGER_PERSISTENT_MUSIC)
	
		PRINTLN("[CORONA] MAINTAIN_CORONA_SCREEN_DURING_TRANSITION - trigger Radio: ", GET_MY_CHOSEN_RADIO_NAME(), " T(", GET_CLOUD_TIME_AS_INT(), ")")
	
		SET_MOBILE_PHONE_RADIO_STATE(TRUE)
		SET_AUDIO_FLAG("MobileRadioInGame", TRUE)
		SET_RADIO_TO_MY_CHOSEN()	//SET_RADIO_TO_STATION_INDEX(g_iMyChosenRadioStation)
		
		CLEAR_CORONA_BIT(CORONA_TRIGGER_PERSISTENT_MUSIC)
	ENDIF
ENDPROC

/// PURPOSE: Sets a microphone in place of the invite exterior camera
PROC MAINTAIN_PERSISTENT_CORONA_AMBIENT_AUDIO()

	// Handle freezing of the microphone
	IF bUseHeistTransitionInviteMic = TRUE
	OR SHOULD_TRANSITION_SESSIONS_CORONA_CONTROLLER_MAINTAIN_CAMERA()											// Handle the transition while launching for all Jobs
	OR (NETWORK_IS_ACTIVITY_SESSION() AND IS_HEIST_ANIM_PLAYING(g_HeistSharedClient.sHeistAnimFirst))			// Handle the first intro following a launch
		
		IF coronaAmbientAudioState != ciCORONA_AMBIENT_AUDIO_STATE_SUPPRESS_FREEZE_OF_MIC
			//CDEBUG2LN(DEBUG_SAFEHOUSE, "MAINTAIN_PERSISTENT_CORONA_AMBIENT_AUDIO: FREEZE_MICROPHONE")
			FREEZE_MICROPHONE()
		ENDIF
	ELSE
		IF NETWORK_IS_ACTIVITY_SESSION()
			IF NOT bFreezingMicHasStopped
				CDEBUG2LN(DEBUG_SAFEHOUSE, "MAINTAIN_PERSISTENT_CORONA_AMBIENT_AUDIO: bFreezingMicHasStopped = TRUE")
				bFreezingMicHasStopped = TRUE
			ENDIF
		ENDIF
	ENDIF

	// ---------- Handle client state through the heist flow ----------
	
	// If we are the host, no more to process
	IF IS_CORONA_BIT_SET(CORONA_ACTIVE_HOST_OF_MENU)
		EXIT
	ENDIF
	
	// Only process the audio scenes for heists
	IF NOT IS_CORONA_BIT_SET(CORONA_GLOBAL_HEIST_FLAG)
		EXIT
	ENDIF
	
	// Base state machines for these new audio scenes that are running beneath the Heist coronas
	SWITCH coronaAmbientAudioState
	
		// In this state we are waiting for the transition session to launch
		CASE ciCORONA_AMBIENT_AUDIO_STATE_WAIT_FOR_LAUNCH
			
			// Wait until we are told the transition session is launching so control is with this script
			IF SHOULD_TRANSITION_SESSIONS_CORONA_CONTROLLER_MAINTAIN_CAMERA()
				IF bUseHeistTransitionInviteMic
				
					PRINTLN("[CORONA] MAINTAIN_PERSISTENT_CORONA_AMBIENT_AUDIO - Move to ciCORONA_AMBIENT_AUDIO_STATE_SUPPRESS_FREEZE_OF_MIC to begin audio scene sequence")
					
					// Move to first state for audio scenes		
					coronaAmbientAudioState = ciCORONA_AMBIENT_AUDIO_STATE_SUPPRESS_FREEZE_OF_MIC
				ENDIF
			
			ENDIF
		BREAK
		
		CASE ciCORONA_AMBIENT_AUDIO_STATE_SUPPRESS_FREEZE_OF_MIC
			
			PRINTLN("[CORONA] MAINTAIN_PERSISTENT_CORONA_AMBIENT_AUDIO - Move to ciCORONA_AMBIENT_AUDIO_STATE_BRING_IN_RADIO start both audio scenes")
			
			START_AUDIO_SCENE("DLC_MPHEIST_TRANSITION_TO_APT_FADE_OUT_AMB_SCENE")
			START_AUDIO_SCENE("DLC_MPHEIST_TRANSITION_TO_APT_FADE_IN_RADIO_SCENE")
			
			coronaAmbientAudioState = ciCORONA_AMBIENT_AUDIO_STATE_BRING_IN_RADIO
		BREAK
		
		CASE ciCORONA_AMBIENT_AUDIO_STATE_BRING_IN_RADIO
			
			PRINTLN("[CORONA] MAINTAIN_PERSISTENT_CORONA_AMBIENT_AUDIO - Stop audio scene: DLC_MPHEIST_TRANSITION_TO_APT_FADE_IN_RADIO_SCENE")
			STOP_AUDIO_SCENE("DLC_MPHEIST_TRANSITION_TO_APT_FADE_IN_RADIO_SCENE")
			
			coronaAmbientAudioState = ciCORONA_AMBIENT_AUDIO_STATE_WAIT_FOR_CUT_POST_LAUNCH
		BREAK
		
		// In this state we are waiting for the player to be active in the corona again
		CASE ciCORONA_AMBIENT_AUDIO_STATE_WAIT_FOR_CUT_POST_LAUNCH
			
			IF bFreezingMicHasStopped
				
				PRINTLN("[CORONA] MAINTAIN_PERSISTENT_CORONA_AMBIENT_AUDIO - Client stopping audio scene: DLC_MPHEIST_TRANSITION_TO_APT_FADE_IN_RADIO_SCENE")
				
				// Start a fade in of the radio in the apartment
				STOP_AUDIO_SCENE("DLC_MPHEIST_TRANSITION_TO_APT_FADE_OUT_AMB_SCENE")
				coronaAmbientAudioState = ciCORONA_AMBIENT_AUDIO_STATE_WAIT_FOR_END_OF_CORONA
			ENDIF
		BREAK
	
		// Call freeze as long as we are active in the corona
		CASE ciCORONA_AMBIENT_AUDIO_STATE_WAIT_FOR_END_OF_CORONA
		
			// Freeze each frame
			IF IS_PLAYER_IN_CORONA()
				FREEZE_MICROPHONE()
			ENDIF
		BREAK	
	ENDSWITCH

ENDPROC
/// PURPOSE: Sets the lobby audio scene on camera freeze
PROC MAINTAIN_PERSISTENT_CORONA_LOBBY_AUDIO_SCENE(FMMC_CORONA_DATA_MAINTAIN_SCRIPT_VARS &fmmcCoronaMissionData)
	IF IS_THIS_MISSION_A_HEIST(fmmcCoronaMissionData.iMissionType, fmmcCoronaMissionData.bIsHeistMission)
		IF NOT GET_TOGGLE_PAUSED_RENDERPHASES_STATUS()
			IF NOT IS_AUDIO_SCENE_ACTIVE("DLC_MPHEIST_LOBBY_SCENE")
				SET_TRANSITION_SESSION_AUDIO_SCENE(fmmcCoronaMissionData, TRUE, TRUE)
			ENDIF
		ELSE
			IF IS_AUDIO_SCENE_ACTIVE("DLC_MPHEIST_LOBBY_SCENE")
				SET_TRANSITION_SESSION_AUDIO_SCENE(fmmcCoronaMissionData, FALSE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC
/// PURPOSE:
///    Called every frame to keep the map zoomed to a specific distance
PROC MAINTAIN_CORONA_MAP_ZOOM()

	// Suppress this lock if we are in full screen map mode
	IF IS_CORONA_BIT_SET(CORONA_FULL_SCREEN_MAP_ACTIVE)
		EXIT
	ENDIF

	IF g_fRaceRadarDistance != 0
		SET_RADAR_ZOOM_TO_DISTANCE(g_fRaceRadarDistance)
	ENDIF
	
	LOCK_MINIMAP_POSITION(localCoronaMapX, localCoronaMapY)
ENDPROC

/// PURPOSE:
///    Take the mission data and calculates the size of the map and the zoom we should use
PROC INITIALISE_CORONA_MAP_SIZE()
	INT iMaxNodesToCheck = GET_CORONA_MAP_MAX_NUMBER_OF_NODES(g_FMMC_STRUCT.iMissionType)
	VECTOR vVectorOfNode
	
	INT i
	VECTOR vNodeMax
	VECTOR vNodeMin
	
	// Loop through the nodes we will be showing, getting the max and min values
	FOR i = 0 TO (iMaxNodesToCheck-1) STEP 1
	
		vVectorOfNode = GET_CORONA_MAP_VECTOR_TO_CHECK(i)
	
		IF NOT IS_VECTOR_ZERO(vVectorOfNode)
			PRINTLN("[CORONA] INITIALISE_CORONA_MAP_SIZE - vVectorOfNode[",i, "]", vVectorOfNode)
			
			IF i = 0
				vNodeMax.x = vVectorOfNode.x
				vNodeMin.x = vVectorOfNode.x
				vNodeMax.y = vVectorOfNode.y
				vNodeMin.y = vVectorOfNode.y
			ELSE
				IF vVectorOfNode.x > vNodeMax.x
					vNodeMax.x = vVectorOfNode.x
				ENDIF
				IF vVectorOfNode.x < vNodeMin.x
					vNodeMin.x = vVectorOfNode.x
				ENDIF
				IF vVectorOfNode.y > vNodeMax.y
					vNodeMax.y = vVectorOfNode.y
				ENDIF
				IF vVectorOfNode.y < vNodeMin.y
					vNodeMin.y = vVectorOfNode.y
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	// Calculate our range and get the correct zoom.
	localCoronaMapX = (vNodeMax.x + vNodeMin.x)/2
	localCoronaMapY = (vNodeMax.y + vNodeMin.y)/2
	PRINTLN("[CORONA] INITIALISE_CORONA_MAP_SIZE - AverageX = ", localCoronaMapX)
	PRINTLN("[CORONA] INITIALISE_CORONA_MAP_SIZE - AverageY = ", localCoronaMapY)

	LOCK_MINIMAP_POSITION(localCoronaMapX, localCoronaMapY)
	LOCK_MINIMAP_ANGLE(0)
	
	//Store Map Zoom Distance as global
	FLOAT DistanceX = (vNodeMax.x - vNodeMin.x)
	FLOAT DistanceY = (vNodeMax.y - vNodeMin.y)
		
	IF DistanceX > DistanceY
		g_fRaceRadarDistance = (DistanceX/1.5)
		PRINTLN("[CORONA] INITIALISE_CORONA_MAP_SIZE - g_fRaceRadarDistance USING X/1.5 = ", g_fRaceRadarDistance)
	ELSE
		g_fRaceRadarDistance = (DistanceY/1.5)
		PRINTLN("[CORONA] INITIALISE_CORONA_MAP_SIZE - g_fRaceRadarDistance USING Y/1.5 = ", g_fRaceRadarDistance)
	ENDIF
	
	
ENDPROC

/// PURPOSE:
///    Initialises the display of the map, hiding blips, setting up blips we want
PROC INITIALISE_CORONA_MAP_DISPLAY()
		
	DELETE_WAYPOINTS_FROM_THIS_PLAYER()
	SET_WAYPOINT_OFF()
	CLEAR_GPS_CUSTOM_ROUTE()
	HIDE_ALL_SHOP_BLIPS(TRUE)
	SET_POLICE_RADAR_BLIPS(FALSE)
	
	SWITCH g_FMMC_STRUCT.iMissionType
		CASE FMMC_TYPE_RACE
			PRINTLN("[CORONA] INITIALISE_CORONA_MAP_DISPLAY - set up minimap for GPS route")
			
			SETUP_CORONA_MAP_FOR_GPS_ROUTE()
		BREAK
		
		CASE FMMC_TYPE_DEATHMATCH
		CASE FMMC_TYPE_MISSION
		CASE FMMC_TYPE_SURVIVAL
			PRINTLN("[CORONA] INITIALISE_CORONA_MAP_DISPLAY - set up minimap for weapon pickups")
			
			SETUP_CORONA_MAP_FOR_WEAPON_PICKUPS()
		BREAK
		
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Setups all the params we need for the custom map in the corona
PROC INITIALISE_CORONA_MAP()
	
	// Wait until we are in a state where we can begin setting up the map
	IF GET_CORONA_STATUS() < CORONA_STATUS_INTRO
	AND NOT IS_TRANSITION_SESSION_RESTARTING()
		EXIT
	ENDIF
	
	// Check if the map should even be available to you
	IF NOT IS_CORONA_MAP_AVAILABLE()
		localCoronaMapStage = CORONA_MAP_STATE_VOID
		PRINTLN("[CORONA] INITIALISE_CORONA_MAP - Map is not available for this mission type: ", g_FMMC_STRUCT.iMissionType)
		EXIT
	ENDIF
	
	// Use the data to set up our map.
	INITIALISE_CORONA_MAP_SIZE()
	
	// Initialise all our blips from data
	INITIALISE_CORONA_MAP_DISPLAY()
	
	// Set the zoom now
	MAINTAIN_CORONA_MAP_ZOOM()
	
	// Move our states on
	localCoronaMapStage = CORONA_MAP_STATE_MAINTAIN
	PRINTLN("[CORONA] INITIALISE_CORONA_MAP - Map initialised, move to CORONA_MAP_STATE_MAINTAIN ")
ENDPROC

/// PURPOSE:
///    Called when we want to show the corona map
PROC TURN_ON_CORONA_MAP()
	DISPLAY_RADAR(TRUE)
	CUSTOM_MINIMAP_SET_ACTIVE(TRUE)	
ENDPROC

/// PURPOSE:
///    Called when we want to hide the corona map
PROC TURN_OFF_CORONA_MAP()
	DISPLAY_RADAR(FALSE)
	CUSTOM_MINIMAP_SET_ACTIVE(FALSE)
ENDPROC

/// PURPOSE:
///    Maintains the corona map, listening for when it should be displayed / hidden and always controlling the zoom
PROC PROCESS_CORONA_MAP()

	IF IS_CORONA_BIT_SET(CORONA_DISPLAY_MAP)
	
		PRINTLN("[CORONA] MAINTAIN_CORONA_MAP - Corona map needs to be displayed.")
	
		TURN_ON_CORONA_MAP()
		
		CLEAR_CORONA_BIT(CORONA_DISPLAY_MAP)
	ENDIF
	
	IF IS_CORONA_BIT_SET(CORONA_HIDE_MAP)
		
		PRINTLN("[CORONA] MAINTAIN_CORONA_MAP - Corona map needs to be hidden.")
		
		TURN_OFF_CORONA_MAP()
		
		CLEAR_CORONA_BIT(CORONA_HIDE_MAP)
	ENDIF

	// Keep the zoom
	MAINTAIN_CORONA_MAP_ZOOM()
	
ENDPROC


/// PURPOSE: This maintains the map throughout the corona
PROC MAINTAIN_CORONA_MAP()

	SWITCH localCoronaMapStage
	
		CASE CORONA_MAP_STATE_INITIALISE
			INITIALISE_CORONA_MAP()
		BREAK
		
		CASE CORONA_MAP_STATE_MAINTAIN
			PROCESS_CORONA_MAP()
		BREAK
		
		CASE CORONA_MAP_STATE_VOID
			// Nothing to do here as we don't need the map
		BREAK
	
	ENDSWITCH

ENDPROC

INTERIOR_INSTANCE_INDEX iArenaInteriorIndex
INTERIOR_INSTANCE_INDEX iArenaInterior_VIPLoungeIndex

/// PURPOSE: Trigger a load scene if we need to.
PROC MAINTAIN_CORONA_LOAD_SCENE(FMMC_CORONA_DATA_MAINTAIN_SCRIPT_VARS &fmmcCoronaMissionData)
	
	IF DID_TRANSITION_SESSION_RECIEVED_START_LAUNCH()
		
		IF DOES_JOB_REQUIRE_IPL_LOAD()
			BOOL bExit = TRUE
			IF NOT IS_CORONA_BIT_SET(CORONA_LOADED_JOB_IPL)
				IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
					SET_CORONA_CENTRE_POINT_FOR_SPECIAL_CASES(fmmcCoronaMissionData.vStartPoint, fmmcCoronaMissionData.bIsContactMission)
					
					IF IS_ARENA_WARS_JOB()
					
						IF IS_ARENA_WARS_JOB(TRUE)
							IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION
								g_FMMC_STRUCT.sArenaInfo.iArena_Lighting = g_sTransitionSessionOptions.iSelection[ciMISSION_HOST_OPTION_WEATHER]
								PRINTLN("[CORONA][CORONA_ARENA_LIGHTING] MAINTAIN_CORONA_LOAD_SCENE - Mission, g_FMMC_STRUCT.sArenaInfo.iArena_Lighting = ", GET_ARENA_LIGHTING_STRING(g_FMMC_STRUCT.sArenaInfo.iArena_Lighting))
							ELIF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_DEATHMATCH
								g_FMMC_STRUCT.sArenaInfo.iArena_Lighting = g_sTransitionSessionOptions.iSelection[ciDM_HOST_OPTION_WEATHER]
								PRINTLN("[CORONA][CORONA_ARENA_LIGHTING] MAINTAIN_CORONA_LOAD_SCENE - Deathmatch, g_FMMC_STRUCT.sArenaInfo.iArena_Lighting = ", GET_ARENA_LIGHTING_STRING(g_FMMC_STRUCT.sArenaInfo.iArena_Lighting))
							ELIF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_RACE
								g_FMMC_STRUCT.sArenaInfo.iArena_Lighting = g_sTransitionSessionOptions.iSelection[ciRC_HOST_OPTION_WEATHER]
								PRINTLN("[CORONA][CORONA_ARENA_LIGHTING] MAINTAIN_CORONA_LOAD_SCENE - Race, g_FMMC_STRUCT.sArenaInfo.iArena_Lighting = ", GET_ARENA_LIGHTING_STRING(g_FMMC_STRUCT.sArenaInfo.iArena_Lighting))
							ENDIF
						ENDIF
						
						IF LOAD_UGC_ARENA(iArenaInteriorIndex, iArenaInterior_VIPLoungeIndex)
							g_ArenaInterior = iArenaInteriorIndex
							PRINTLN("[CORONA][GENERIC_JOB] MAINTAIN_CORONA_LOAD_SCENE - Loaded the arena")
							SET_CORONA_BIT(CORONA_LOADED_JOB_IPL)
							bExit = FALSE
						ENDIF
						
					ELIF CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_CAYO_PERICO_SERIES(g_FMMC_STRUCT.iRootContentIDHash)
					
						IF FMMC_LOAD_ISLAND_IPLS()
							PRINTLN("[CORONA][GENERIC_JOB] MAINTAIN_CORONA_LOAD_SCENE - Loaded Cayo Perico")
							SET_CORONA_BIT(CORONA_LOADED_JOB_IPL)
							bExit = FALSE
						ENDIF
					
					ENDIF
				ENDIF
				
				IF bExit
					EXIT
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT IS_CORONA_BIT_SET(CORONA_TRIGGERED_EARLY_LOAD_SCENE)
			
			IF DOES_CORONA_MISSION_TYPE_REQUIRE_LOAD_SCENE(g_FMMC_STRUCT.iMissionType, fmmcCoronaMissionData.vStartPoint, fmmcCoronaMissionData.bIsContactMission, fmmcCoronaMissionData.bIsHeistMission)
			AND NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
			AND NOT AM_I_TRANSITION_SESSIONS_STARTING_HEIST_QUICK_MATCH()
			AND NOT AM_I_TRANSITION_SESSIONS_STARTING_GANG_OPS_QUICK_MATCH()
			AND NOT fmmcCoronaMissionData.bIsHeistMission					// Do not do warp and load scene if its a heist: 2281048 & 2281083
				IF IS_SKYSWOOP_AT_GROUND()
				AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()
				
					// Overwrite loading location if valid.
					SET_CORONA_CENTRE_POINT_FOR_SPECIAL_CASES(fmmcCoronaMissionData.vStartPoint, fmmcCoronaMissionData.bIsContactMission)
				
					VECTOR vAreaToLoad = GET_CORONA_CENTRE_POSITION(fmmcCoronaMissionData.vStartPoint, fmmcCoronaMissionData.vScenePos)
					vAreaToLoad = GET_CORONA_VECTOR_FOR_LOAD_SCENE(vAreaToLoad, fmmcCoronaMissionData.bIsContactMission)
					
					IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_FLOW()
						vAreaToLoad = GANGOPS_GET_GANG_OPS_BOARD_START_POS()
						PRINTLN("[CORONA] MAINTAIN_CORONA_LOAD_SCENE - GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_FLOW, set vAreaToLoad: ", vAreaToLoad)
					ENDIF
					#IF FEATURE_CASINO_HEIST
					IF IS_THIS_MISSION_A_CASINO_HEIST_MISSION(g_FMMC_STRUCT.iRootContentIDHash)
						vAreaToLoad = GET_CASINO_HEIST_BOARD_START_POS()
						PRINTLN("[CORONA] MAINTAIN_CORONA_LOAD_SCENE - IS_THIS_MISSION_A_CASINO_HEIST_MISSION, set vAreaToLoad: ", vAreaToLoad)
					ENDIF
					#ENDIF
					#IF FEATURE_HEIST_ISLAND
					IF IS_THIS_MISSION_A_HEIST_ISLAND_MISSION(g_FMMC_STRUCT.iRootContentIDHash)
						vAreaToLoad = GET_HEIST_ISLAND_BOARD_START_POS()
						PRINTLN("[CORONA] MAINTAIN_CORONA_LOAD_SCENE - IS_THIS_MISSION_A_HEIST_ISLAND_MISSION, set vAreaToLoad: ", vAreaToLoad)
					ENDIF
					#ENDIF
					IF IS_THIS_MISSION_A_TUNER_ROBBERY_FINALE(g_FMMC_STRUCT.iRootContentIDHash)
						vAreaToLoad = GET_TUNER_BOARD_START_POS()
						PRINTLN("[CORONA] MAINTAIN_CORONA_LOAD_SCENE - IS_THIS_MISSION_A_TUNER_ROBBERY_FINALE, set vAreaToLoad: ", vAreaToLoad)
					ENDIF
					IF IS_THIS_ROOT_CONTENT_ID_A_FIXER_STORY_MISSION_IN_HQ()
						vAreaToLoad = GET_FIXER_STORY_START_POS()
						PRINTLN("[CORONA] MAINTAIN_CORONA_LOAD_SCENE - IS_THIS_ROOT_CONTENT_ID_A_FIXER_STORY_MISSION_IN_HQ, set vAreaToLoad: ", vAreaToLoad)
					ENDIF
					IF IS_THIS_ROOT_CONTENT_ID_A_FIXER_STORY_MISSION_NOT_IN_HQ()
						vAreaToLoad = GET_FIXER_STORY_START_POS_NOT_IN_HQ()
						PRINTLN("[CORONA] MAINTAIN_CORONA_LOAD_SCENE - IS_THIS_ROOT_CONTENT_ID_A_FIXER_STORY_MISSION_NOT_IN_HQ, set vAreaToLoad: ", vAreaToLoad)
					ENDIF
					IF IS_THIS_ROOT_CONTENT_ID_A_ULP_MISSION(g_FMMC_STRUCT.iRootContentIDHash)
						vAreaToLoad = GET_ULP_MISSION_LOCATION()
						PRINTLN("[CORONA] MAINTAIN_CORONA_LOAD_SCENE - IS_THIS_ROOT_CONTENT_ID_A_ULP_MISSION, set vAreaToLoad: ", vAreaToLoad)
					ENDIF
					#IF FEATURE_DLC_2_2022
					IF IS_THIS_ROOT_CONTENT_ID_A_XMAS22_STORY_MISSION(g_FMMC_STRUCT.iRootContentIDHash)
						vAreaToLoad = GET_XMAS22_STORY_START_POS()
						PRINTLN("[CORONA] MAINTAIN_CORONA_LOAD_SCENE - IS_THIS_ROOT_CONTENT_ID_A_XMAS22_STORY_MISSION, set vAreaToLoad: ", vAreaToLoad)
					ENDIF
					#ENDIF
					PRINTLN("MAINTAIN_CORONA_LOAD_SCENE - Vector: (", vAreaToLoad, "), ScenePos: ",  fmmcCoronaMissionData.vScenePos, ", T(", GET_CLOUD_TIME_AS_INT(), "), F(", GET_FRAME_COUNT())
					
					IF NOT IS_VECTOR_ZERO(vAreaToLoad)
						
						BOOL bSCTVRestriction
						IF IS_PLAYER_SCTV(PLAYER_ID())
							IF NOT g_bUpdateSpectatorPosition
								bSCTVRestriction = TRUE
								PRINTLN("MAINTAIN_CORONA_LOAD_SCENE - bSCTVRestriction = TRUE")
							ENDIF
						ENDIF
						
						IF bSCTVRestriction = FALSE
							IF fmmcCoronaMissionData.bIsContactMission
							OR HAS_ON_CALL_TRANSITION_SESSION_BEEN_SET_UP()
								
								PRINTLN("MAINTAIN_CORONA_LOAD_SCENE - Warp player to v: (", vAreaToLoad, ") , T(", GET_CLOUD_TIME_AS_INT(), "), F(", GET_FRAME_COUNT())
								PRINTLN("MAINTAIN_CORONA_LOAD_SCENE - CONTACT MISSION: ", PICK_STRING(fmmcCoronaMissionData.bIsContactMission, "TRUE", "FALSE"))
								PRINTLN("MAINTAIN_CORONA_LOAD_SCENE - ON CALL: ", PICK_STRING(HAS_ON_CALL_TRANSITION_SESSION_BEEN_SET_UP(), "TRUE", "FALSE"))
								
								SET_ENTITY_COORDS(PLAYER_PED_ID(), vAreaToLoad, FALSE, FALSE, FALSE, TRUE)
							ENDIF
						
							CLEAR_FOCUS()
							PRINTLN("MAINTAIN_CORONA_LOAD_SCENE - CLEAR_FOCUS()")
						ENDIF
						
						PRINTLN("MAINTAIN_CORONA_LOAD_SCENE - Triggered LOAD SCENE as launch has begun, v: (", vAreaToLoad, ") , T(", GET_CLOUD_TIME_AS_INT(), "), F(", GET_FRAME_COUNT())
						
						NEW_LOAD_SCENE_START_SPHERE(vAreaToLoad, GET_CORONA_LOAD_SCENE_RADIUS(), GET_CORONA_LOAD_SCENE_FLAGS())
						SET_TRANSITION_SESSIONS_STARTED_LOAD_SCENE()
						CLEAR_TRANSITION_SESSIONS_LOADED_SCENE()
						SET_CORONA_BIT(CORONA_TRIGGERED_EARLY_LOAD_SCENE)
						CLEAR_TRANSITION_SESSION_RECIEVED_START_LAUNCH()
						sLocalVars.bLoadSceneStarted = TRUE
					#IF IS_DEBUG_BUILD
					ELSE
						PRINTLN("MAINTAIN_CORONA_LOAD_SCENE - vAreaToLoad = <<0,0,0>>, not starting load scene. T(", GET_CLOUD_TIME_AS_INT(), "), F(", GET_FRAME_COUNT())
					#ENDIF
					ENDIF
				ELSE
					PRINTLN("MAINTAIN_CORONA_LOAD_SCENE - IS_SKYSWOOP_AT_GROUND() = FALSE Player is in camera switch so can't load in another scene, T(", GET_CLOUD_TIME_AS_INT(), "), F(", GET_FRAME_COUNT())

					CLEAR_TRANSITION_SESSION_RECIEVED_START_LAUNCH()
					SET_CORONA_BIT(CORONA_TRIGGERED_EARLY_LOAD_SCENE)
				ENDIF
			ELSE
				PRINTLN("MAINTAIN_CORONA_LOAD_SCENE - Mission does not need a load scene. Mission Type: ", g_FMMC_STRUCT.iMissionType, " T(", GET_CLOUD_TIME_AS_INT(), "), F(", GET_FRAME_COUNT())
				
				#IF IS_DEBUG_BUILD
					IF IS_CONTACT_MISSION_LAUNCHING_IN_APARTMENT()
						PRINTLN("MAINTAIN_CORONA_LOAD_SCENE - Mission does not need a load scene. Contact mission from apartment, T(", GET_CLOUD_TIME_AS_INT(), "), F(", GET_FRAME_COUNT())
					ENDIF
				#ENDIF
				
				CLEAR_TRANSITION_SESSION_RECIEVED_START_LAUNCH()
				SET_CORONA_BIT(CORONA_TRIGGERED_EARLY_LOAD_SCENE)
			ENDIF
		ENDIF
	ENDIF
	
	//If we started the load scene, and it's not finished then check that it's done
	IF HAS_TRANSITION_SESSIONS_STARTED_LOAD_SCENE()
		IF NOT HAS_TRANSITION_SESSIONS_LOADED_SCENE()
			IF sLocalVars.bLoadSceneStarted
				IF IS_NEW_LOAD_SCENE_ACTIVE()
					IF IS_NEW_LOAD_SCENE_LOADED()
						PRINTLN("MAINTAIN_CORONA_LOAD_SCENE Load scene has finished. calling SET_TRANSITION_SESSIONS_LOADED_SCENE")
						NEW_LOAD_SCENE_STOP()
						SET_TRANSITION_SESSIONS_LOADED_SCENE()
						CLEAR_TRANSITION_SESSIONS_STARTED_LOAD_SCENE()
					//ELSE
						//PRINTLN("MAINTAIN_CORONA_LOAD_SCENE - IS_NEW_LOAD_SCENE_LOADED = FALSE")
					ENDIF
				ELSE
					PRINTLN("MAINTAIN_CORONA_LOAD_SCENE Load scene has finished. IS_NEW_LOAD_SCENE_ACTIVE = FALSE, clear our bit")
					CLEAR_TRANSITION_SESSIONS_STARTED_LOAD_SCENE()	// If it load scene is no longer active / clear our bit as failed.
				ENDIF
			ENDIF
		//ELSE
			//PRINTLN("MAINTAIN_CORONA_LOAD_SCENE - HAS_TRANSITION_SESSIONS_LOADED_SCENE = TRUE")
		ENDIF
	//ELSE
		//PRINTLN("MAINTAIN_CORONA_LOAD_SCENE - HAS_TRANSITION_SESSIONS_STARTED_LOAD_SCENE = FALSE")
	ENDIF
ENDPROC

CONST_INT ciSET_QUITTING_CORONA_AFTER_TRANSITION_SESSION_HAS_LAUNCHED 		0
CONST_INT ciSET_UP_DO_QUITTING_CORONA_TOGGLE_RENDERPHASES					1
CONST_INT ciSET_UP_DO_QUITTING_CORONA_AFTER_TRANSITION_SESSION_HAS_LAUNCHED	2
CONST_INT ciWAIT_DO_QUITTING_CORONA_AFTER_TRANSITION_SESSION_HAS_LAUNCHED	3

//Set up bailing out of a corona if we're stuck
FUNC BOOL SET_UP_CORONA_CLEAN_UP_AFTER_TRANSITION_SESSION_HAS_LAUNCHED(INT &iTranisitionSessionSetUpStage, BOOL bReturnControl = FALSE)
	//Switch the joining stage of the transition session
	SWITCH iTranisitionSessionSetUpStage
		//Set up the transition
		CASE ciSET_QUITTING_CORONA_AFTER_TRANSITION_SESSION_HAS_LAUNCHED
			IF NETWORK_IS_ACTIVITY_SESSION()
				PRINTLN("[TS] SET_UP_CORONA_CLEAN_UP_AFTER_TRANSITION_SESSION_HAS_LAUNCHED - NETWORK_DO_TRANSITION_TO_GAME - SETUP_MATCHMAKING_RULES")
				IF SHOULD_SWITCH_TO_A_PUBLIC_SESSION_ON_QUITTING_CORONA()
					g_Private_Players_FM_SESSION_Menu_Choice_LAST_ENTRY = FM_SESSION_MENU_CHOICE_JOIN_PUBLIC_SESSION
				ENDIF	
				SET_CURRENT_TRANSITION_FM_SESSION_MENU_CHOICE(g_Private_Players_FM_SESSION_Menu_Choice_LAST_ENTRY)
				SET_TRANSITION_SESSIONS_RUN_FREEMODE_RELAUNCH()
				SET_TRANSITION_SESSIONS_CLEANUP_AFTER_CORONA_WALK_OUT()
			ENDIF
			CLEAR_PLAYLIST_HAS_DONE_INITIAL_TRANSITION()
			CLEAR_JOB_HAS_LAUNCHED_INITIAL_TRANSITION_SESSION()
			CLEAR_PLAYLIST_BITSET()
			//Turn on the news feed
			IF NOT SHOULD_DO_TRANSIITON_TO_NEW_FREEMODE_SESSION_WITH_ALL_PLAYERS_AFTER_CORONA()
				SET_NEWS_FEED_SPLASH_SCREEN_ACTIVE(TRUE, TRUE)	
			ENDIF
			//Set up my followers
			SET_UP_SCTV_FOLLOWERS_FOR_SESSION_CHANGE()			
			iTranisitionSessionSetUpStage++
			PRINTLN("[TS] SET_UP_CORONA_CLEAN_UP_AFTER_TRANSITION_SESSION_HAS_LAUNCHED - TRIGGER_SCREENBLUR_FADE_IN")	
		BREAK		
		
		//Toggel the render phases
		CASE ciSET_UP_DO_QUITTING_CORONA_TOGGLE_RENDERPHASES
			SETUP_MATCHMAKING_RULES(GAMEMODE_FM)		
			iTranisitionSessionSetUpStage++
		BREAK
		
		//call the number of players that are coming with me
		CASE ciSET_UP_DO_QUITTING_CORONA_AFTER_TRANSITION_SESSION_HAS_LAUNCHED
			IF NETWORK_IS_ACTIVITY_SESSION()
				//If we are going to a new FM with all players
				IF IS_THIS_A_SOLO_SESSION()
					CALL_NETWORK_SESSION_SET_GAMEMODE_BEFORE_TO_GAME()
					IF NETWORK_DO_TRANSITION_TO_NEW_GAME(FALSE, NUM_NETWORK_PLAYERS, TRUE)
						//If we have transitioned then we need to clean it all up
						IF HAS_TRANSITION_SESSIONS_HAS_DONE_INITIAL_TRANSITION()
						//Or we are in a corona after a restart/random
						OR IS_TRANSITION_SESSION_IN_CORONA_AFTER_RANDOM_RESTART()
						OR CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_ARENA_SERIES(g_FMMC_STRUCT.iRootContentIDHash)
							SET_TRANSITION_SESSION_PULL_CAM_UP_FOR_SESSION_CHANGE()
						ENDIF
						IF bReturnControl
							NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_CLEAR_TASKS  | NSPC_FREEZE_POSITION | NSPC_CAN_BE_TARGETTED)		
						ENDIF
						SET_TRANSITION_SESSIONS_CLEANUP_AFTER_MISSION()
						PRINTLN("[TS] SET_UP_CORONA_CLEAN_UP_AFTER_TRANSITION_SESSION_HAS_LAUNCHED - NETWORK_DO_TRANSITION_TO_GAME(FALSE, NUM_NETWORK_PLAYERS) = TRUE")
						iTranisitionSessionSetUpStage++
						RETURN TRUE
					ELSE
						PRINTLN("[TS] SET_UP_CORONA_CLEAN_UP_AFTER_TRANSITION_SESSION_HAS_LAUNCHED - NETWORK_DO_TRANSITION_TO_GAME(FALSE, NUM_NETWORK_PLAYERS) = FALSE")
					ENDIF
				ELSE
					CALL_NETWORK_SESSION_SET_GAMEMODE_BEFORE_TO_GAME()
					IF NETWORK_DO_TRANSITION_TO_GAME(FALSE, NUM_NETWORK_PLAYERS)
						//If we have transitioned then we need to clean it all up
						IF HAS_TRANSITION_SESSIONS_HAS_DONE_INITIAL_TRANSITION()
						//Or we are in a corona after a restart/random
						OR IS_TRANSITION_SESSION_IN_CORONA_AFTER_RANDOM_RESTART()
							SET_TRANSITION_SESSION_PULL_CAM_UP_FOR_SESSION_CHANGE()
						ENDIF
						IF bReturnControl
							NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_CLEAR_TASKS  | NSPC_FREEZE_POSITION | NSPC_CAN_BE_TARGETTED)		
						ENDIF
						SET_TRANSITION_SESSIONS_CLEANUP_AFTER_MISSION()
						PRINTLN("[TS] SET_UP_CORONA_CLEAN_UP_AFTER_TRANSITION_SESSION_HAS_LAUNCHED - NETWORK_DO_TRANSITION_TO_GAME(FALSE, NUM_NETWORK_PLAYERS) = TRUE")
						iTranisitionSessionSetUpStage++
						RETURN TRUE
					ELSE
						PRINTLN("[TS] SET_UP_CORONA_CLEAN_UP_AFTER_TRANSITION_SESSION_HAS_LAUNCHED - NETWORK_DO_TRANSITION_TO_GAME(FALSE, NUM_NETWORK_PLAYERS) = FALSE")
					ENDIF
				ENDIF
			ELSE
				SET_PAUSE_MENU_REQUESTING_A_WARP()
	            SET_PAUSE_MENU_REQUESTING_A_NEW_SESSION()
	            SET_PAUSE_MENU_WARP_TO_MODE(GAMEMODE_FM)
	            SET_SOMETHING_QUITTING_MP(TRUE)
	            SET_CURRENT_TRANSITION_FM_MENU_CHOICE(FM_MENU_CHOICE_JOIN_NEW_SESSION)// – Whatever type of session you want. 
	            SET_CURRENT_TRANSITION_FM_SESSION_MENU_CHOICE(g_Private_Players_FM_SESSION_Menu_Choice_LAST_ENTRY)
	            SET_JOINING_GAMEMODE(GAMEMODE_FM)
				iTranisitionSessionSetUpStage++
				RETURN TRUE
			ENDIF
		BREAK
		
		//Nothing
		CASE ciWAIT_DO_QUITTING_CORONA_AFTER_TRANSITION_SESSION_HAS_LAUNCHED
			RETURN TRUE
		
	ENDSWITCH
	RETURN FALSE
ENDFUNC



SCRIPT_TIMER stCoronaLaunchSaftyBailTimer
INT iQuickRestartStuckStage
//Matain the corona launch safty bail
PROC MAINTAIN_CORONA_LAUNCH_SAFTY_BAIL()
	//If we are launching a corona
	IF (IS_TRANSITION_SESSION_LAUNCHING()
	OR IS_TRANSITION_SESSION_RESTARTING())
	AND NOT IS_PLAYER_IN_CORONA()
		//Start the timer
		IF NOT HAS_NET_TIMER_STARTED(stCoronaLaunchSaftyBailTimer)
		AND NETWORK_IS_ACTIVITY_SESSION()
			PRINTLN("[TS] MAINTAIN_CORONA_LAUNCH_SAFTY_BAIL - START_NET_TIMER")
			START_NET_TIMER(stCoronaLaunchSaftyBailTimer, TRUE)
		//If it's expired
		ELIF HAS_NET_TIMER_STARTED(stCoronaLaunchSaftyBailTimer)
		AND HAS_NET_TIMER_EXPIRED(stCoronaLaunchSaftyBailTimer, g_sMPTunables.iCoronaLaunchSaftyBailTimeOut, TRUE)
			IF iQuickRestartStuckStage = ciSET_QUITTING_CORONA_AFTER_TRANSITION_SESSION_HAS_LAUNCHED
				PRINTLN("[TS] *****************************************************************")
				PRINTLN("[TS] *  MAINTAIN_CORONA_LAUNCH_SAFTY_BAIL - HAS_NET_TIMER_EXPIRED - T - ", GET_CLOUD_TIME_AS_INT(), ", F - ", GET_FRAME_COUNT(), " *")
				#IF USE_FINAL_PRINTS PRINTLN_FINAL("[TS] *  MAINTAIN_CORONA_LAUNCH_SAFTY_BAIL - HAS_NET_TIMER_EXPIRED - T - ", GET_CLOUD_TIME_AS_INT(), ", F - ", GET_FRAME_COUNT(), " *")#ENDIF
				PRINTLN("[TS] *****************************************************************")
				//PRINT_HELP("CWO_ERROR_NETL")
			ENDIF
			//Main tain pulling us out the session via a to game
			IF SET_UP_CORONA_CLEAN_UP_AFTER_TRANSITION_SESSION_HAS_LAUNCHED(iQuickRestartStuckStage)
				//wont get here as the session will be cleaned up before that hapens
			ENDIF
		ENDIF
	ENDIF
ENDPROC



PROC MAINTAIN_CORONA_SCREEN_DURING_TRANSITION(INT iMissionType) //, BOOL bTutorial)
	
	PRINTLN("[CORONA] MAINTAIN_CORONA_SCREEN_DURING_TRANSITION")
	
	DISABLE_HUD_ELEMENTS_DURING_TRANSITION_SESSION_CHANGE(TRUE, FALSE)
	DISABLE_ALL_CONTROL_ACTIONS(PLAYER_CONTROL)
	DISABLE_ALL_CONTROL_ACTIONS(CAMERA_CONTROL)
	
	PRINTLN(" MAINTAIN_CORONA_SCREEN_DURING_TRANSITION - DISABLE_ALL_CONTROL_ACTIONS(CAMERA_CONTROL)")
	
	DISABLE_FRONTEND_THIS_FRAME()
	HIDE_ALL_PEDS_AND_VEHICLES_DURING_SET_UP()
	
	//Draw the downloading spinner
	IF GET_CORONA_STATUS() < CORONA_STATUS_WAIT_FOR_ACTIVE
		DO_FMMC_CORONA_SPINNER(sLocalVars.bSetUpSpinner,  "FMMC_STARTTRAN", ENUM_TO_INT(LOADING_ICON_LOADING), "FMMC_STTRANOC")
	ELSE
		IF sLocalVars.bSetUpSpinner
			IF iMissionType != FMMC_TYPE_MISSION
				PRINTLN("MAINTAIN_CORONA_SCREEN_DURING_TRANSITION - BUSYSPINNER_OFF")
				BUSYSPINNER_OFF()
				sLocalVars.bSetUpSpinner = FALSE
			ENDIF 
		ENDIF 
	ENDIF
	
	// 2522554: If we have never initialised the corona menus, hide during transition
	IF NOT IS_CORONA_BIT_SET(CORONA_DETAILS_SCREEN_HAS_BEEN_VISITED)
		GAMER_HANDLE localGH = GET_LOCAL_GAMER_HANDLE()
		IF IS_GAMER_HANDLE_VALID(localGH)
		AND NETWORK_IS_ACTIVITY_SPECTATOR_FROM_HANDLE(localGH)
			PRINTLN("MAINTAIN_CORONA_SCREEN_DURING_TRANSITION - Suppress frontend from rendering")
			CORONA_SUPPRESS_FRONTEND_RENDERING_THIS_FRAME()
		ENDIF
	ENDIF
ENDPROC

PROC SET_LOCAL_MAX_TEAM_COUNTS()
	INT iLoop
	REPEAT FMMC_MAX_TEAMS iLoop
		iMaxNumPlayersPerTeamLocal[iLoop] = g_FMMC_STRUCT.iMaxNumPlayersPerTeam[iLoop]
		PRINTLN("[TEAM BAL DATA] SET_LOCAL_MAX_TEAM_COUNTS - g_FMMC_STRUCT.iMaxNumPlayersPerTeam[", iLoop, "] = ", g_FMMC_STRUCT.iMaxNumPlayersPerTeam[iLoop])
	ENDREPEAT
ENDPROC

FUNC BOOL NEED_TO_RESET_LOCAL_MAX_TEAM_COUNTS()
	INT iLoop
	REPEAT FMMC_MAX_TEAMS iLoop
		IF iMaxNumPlayersPerTeamLocal[iLoop] != g_FMMC_STRUCT.iMaxNumPlayersPerTeam[iLoop]
			PRINTLN("[TEAM BAL DATA] NEED_TO_RESET_LOCAL_MAX_TEAM_COUNTS - iMaxNumPlayersPerTeamLocal[", iLoop, "] != , g_FMMC_STRUCT.iMaxNumPlayersPerTeam[", iLoop, "]")
			RETURN TRUE
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

INT tiTimeOfDayH, tiTimeOfDayM, tiTimeOfDayS

FUNC BOOL SKIP_CAM_BECAUSE_JIP_SVM()
	IF AM_I_ACCEPTING_TRANSITION_SESSIONS_INVITE_FROM_MP()
	AND SVM_FLOW_IS_CURRENT_MISSION_SVM_FLOW()
		PRINTLN("[TEAM BAL DATA] SKIP_CAM_BECAUSE_JIP_SVM")
		RETURN TRUE
	ENDIF
	PRINTLN("[TEAM BAL DATA] SKIP_CAM_BECAUSE_JIP_SVM")
	RETURN FALSE
ENDFUNC


PROC CALL_ASSIGN_PLAYER_SEAT_POSITION_MISSION()
	//Pull the data out of the various structs so it can be passed in:
	INT iPlayerTeams[NUM_NETWORK_PLAYERS]
	INT iPlayerRole[NUM_NETWORK_PLAYERS]
	INT iLoopPlayers
	REPEAT NUM_NETWORK_PLAYERS iLoopPlayers
		iPlayerTeams[iLoopPlayers] = g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iLoopPlayers].iPlayerOptions[ciMISSION_CLIENT_OPTION_TEAM]
		iPlayerRole[iLoopPlayers] = playerBD[iLoopPlayers].iProle
	ENDREPEAT
	
	//call the function to assign the team roles
	ASSIGN_PLAYER_SEAT_POSITION_MISSION(g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers, serverBD.iNumberOfTeams, iPlayerTeams, iPlayerRole)

	//pass the assigned data back into the variables we passed in
	REPEAT NUM_NETWORK_PLAYERS iLoopPlayers
		playerBD[iLoopPlayers].iProle = iPlayerRole[iLoopPlayers]
		g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[iLoopPlayers].iPlayerOptions[ciRC_CLIENT_OPTION_ROLE] = playerBD[iLoopPlayers].iProle
		PRINTLN("[TEAM BAL DATA] [SEAT] g_TransitionSessionNonResetVars.sTransVars.sPlayerVars[", iLoopPlayers, "].iPlayerOptions[ciRC_CLIENT_OPTION_ROLE] = ", playerBD[iLoopPlayers].iProle)
	ENDREPEAT
ENDPROC
SCRIPT_TIMER stGangOpsLaunchTimer
FUNC BOOL GANG_OPS_SCREEN_FADED_OUT()
	IF NOT IS_SCREEN_FADED_OUT()
	AND NOT IS_SCREEN_FADING_OUT()
		PRINTLN("[TS] GANG_OPS_SCREEN_FADED_OUT() - DO_SCREEN_FADE_OUT")	
		DO_SCREEN_FADE_OUT(250)
	ENDIF
	IF IS_SCREEN_FADED_OUT()
		PRINTLN("[TS] GANG_OPS_SCREEN_FADED_OUT() - IS_SCREEN_FADED_OUT")	
		RETURN TRUE
	ENDIF
	IF NOT HAS_NET_TIMER_STARTED(stGangOpsLaunchTimer)
		START_NET_TIMER(stGangOpsLaunchTimer, TRUE)
		PRINTLN("[TS] GANG_OPS_SCREEN_FADED_OUT() - START_NET_TIMER")	
	ELIF HAS_NET_TIMER_EXPIRED(stGangOpsLaunchTimer, 500, TRUE)
		PRINTLN("[TS] GANG_OPS_SCREEN_FADED_OUT() - HAS_NET_TIMER_EXPIRED")	
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC


FUNC BOOL IS_PLAYER_IN_SAFE_CORONA_STATUS_FOR_LOBBY_VIDEO()
	IF IS_PLAYER_IN_CORONA()
	AND GET_CORONA_STATUS() <= CORONA_STATUS_BALANCE_AND_LOAD
	OR IS_TRANSITION_SESSION_LAUNCHING()
		RETURN TRUE
	ENDIF

//	SWITCH GET_CORONA_STATUS()
//		
//		
//		
//		CASE CORONA_STATUS_HEIST_PLANNING
//		CASE CORONA_STATUS_HEIST_CUTSCENE
//		CASE CORONA_STATUS_HEIST_PLANNING_CUTSCENE
//		CASE CORONA_STATUS_HEIST_TUTORIAL_CUTSCENE
//		CASE CORONA_STATUS_FLOW_CUTSCENE
//		CASE CORONA_STATUS_GANG_OPS_HEIST_PLANNING
//		CASE CORONA_STATUS_BETTING
//		CASE CORONA_STATUS_IN_CORONA
//		CASE CORONA_STATUS_TEAM_DM
//		CASE CORONA_STATUS_IN_GENERIC_JOB_CORONA
//			RETURN FALSE
//			
////		CASE CORONA_STATUS_IN_CORONA
////		CASE CORONA_STATUS_TEAM_DM
////		CASE CORONA_STATUS_IN_GENERIC_JOB_CORONA
////			IF IS_ARENA_WARS_JOB()
////				IF IS_CORONA_BIT_SET(CORONA_POST_TRANSITION_CAMERA_CUT_DONE)
////					RETURN FALSE
////				ENDIF
////			ELSE
////				RETURN FALSE
////			ENDIF
////		BREAK
//	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

BOOL bStopDrawingLobbyVideo = FALSE
BOOL bLobbyVideoDrawn = FALSE

FUNC BOOL SHOULD_PLAY_CORONA_LOBBY_VIDEO()
	IF g_sArenaBgVars.bDisableCoronaBinkMovie
		PRINTLN("[CORONA_LOBBY_VIDEO] SHOULD_PLAY_CORONA_LOBBY_VIDEO - global disabled")
		RETURN FALSE
	ENDIF
		
	IF bLobbyVideoDrawn
	AND DID_TRANSITION_SESSION_RECIEVED_START_LAUNCH()
	AND bStopDrawingLobbyVideo = FALSE
		SET_TRANSITION_SESSIONS_DRAW_ARENA_BACKGROUND()
		MAINTAIN_ARENA_LOBBY_BACKGROUND()
		bStopDrawingLobbyVideo = TRUE
		PRINTLN("[CORONA_LOBBY_VIDEO] SHOULD_PLAY_CORONA_LOBBY_VIDEO - DID_TRANSITION_SESSION_RECIEVED_START_LAUNCH")
		RETURN FALSE
	ENDIF

	IF bStopDrawingLobbyVideo
		PRINTLN("[CORONA_LOBBY_VIDEO] SHOULD_PLAY_CORONA_LOBBY_VIDEO - DID_TRANSITION_SESSION_RECIEVED_START_LAUNCH")
		RETURN FALSE
	ENDIF

	IF IS_PLAYER_IN_SAFE_CORONA_STATUS_FOR_LOBBY_VIDEO()
	AND NOT IS_THIS_A_ROUNDS_MISSION_FOR_CORONA()
	AND NOT TRANSITION_SESSIONS_STARTED_EVENT_FAILED()
		IF NOT DOES_CORONA_MISSION_HAVE_LOBBY_VIDEO()
			PRINTLN("[CORONA_LOBBY_VIDEO] SHOULD_PLAY_CORONA_LOBBY_VIDEO - job doesnt have movie")
			RETURN FALSE
		ENDIF
		bLobbyVideoDrawn = TRUE
		PREPARE_ARENA_LOBBY_BACKGROUND()		
		PRINTLN("[CORONA_LOBBY_VIDEO] SHOULD_PLAY_CORONA_LOBBY_VIDEO - safe")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SET_CORONA_LOBBY_TV_JOB_INDEX()
	iArenaWarsTvJobIndex = GET_ARENA_WARS_TV_LOBBY_JOB_INDEX()
	PRINTLN("[TS] [CORONA_LOBBY_VIDEO] SET_CORONA_LOBBY_TV_JOB_INDEX - iArenaWarsTvJobIndex = ", iArenaWarsTvJobIndex)
	IF iArenaWarsTvJobIndex = -1
		iArenaWarsTvJobIndex = GET_ARENA_JOB_ARRAY_POS_FROM_ROOT_ID(g_FMMC_STRUCT.iRootContentIDHash)
		CREATE_ARENA_WARS_TV_LOBBY_PLAYLIST(iArenaWarsTvJobIndex)
		PRINTLN("[TS] [CORONA_LOBBY_VIDEO] SET_CORONA_LOBBY_TV_JOB_INDEX - iArenaWarsTvJobIndex (adjusted) = ", iArenaWarsTvJobIndex)
	ENDIF
ENDPROC

PROC MAINTAIN_CORONA_LOBBY_VIDEO()
	
	IF TRANSITION_SESSIONS_DRAW_ARENA_BACKGROUND()
		MAINTAIN_ARENA_LOBBY_BACKGROUND()
	ENDIF
	
	SWITCH g_sArenaBgVars.iCoronaLobbyVideoStage
		CASE ciCORONA_LOBBY_VIDEO_STAGE_IDLE
			IF SHOULD_PLAY_CORONA_LOBBY_VIDEO()
				g_sArenaBgVars.iCoronaLobbyVideoStage = ciCORONA_LOBBY_VIDEO_STAGE_REQUEST
				PRINTLN("[TS] [CORONA_LOBBY_VIDEO] MAINTAIN_CORONA_LOBBY_VIDEO - g_sArenaBgVars.iCoronaLobbyVideoStage. ciCORONA_LOBBY_VIDEO_STAGE_IDLE > ciCORONA_LOBBY_VIDEO_STAGE_REQUEST")
			#IF FEATURE_GEN9_STANDALONE
			ELSE
				// Make sure we have an image to display as the background as coming 
				// in from the landing page can endup using the black loading screen
				IF IS_TRANSITION_SESSION_SRLINK_REQUESTED_JOB_ACTIVE()
					IF IS_SCREEN_FADED_OUT()
						PRINTLN("[TS] [CORONA_LOBBY_VIDEO] MAINTAIN_CORONA_LOBBY_VIDEO - Fade in as were faded out")
						DO_SCREEN_FADE_IN(0)
					ELSE
						g_sArenaBgVars.iCoronaLobbyVideoStage = ciCORONA_LOBBY_VIDEO_STAGE_RENDERPHASES_TOGGLE
						PRINTLN("[TS] [CORONA_LOBBY_VIDEO] MAINTAIN_CORONA_LOBBY_VIDEO - g_sArenaBgVars.iCoronaLobbyVideoStage. ciCORONA_LOBBY_VIDEO_STAGE_IDLE > ciCORONA_LOBBY_VIDEO_STAGE_RENDERPHASES_TOGGLE")
					ENDIF
				ENDIF
			#ENDIF				
			ENDIF
		BREAK
		CASE ciCORONA_LOBBY_VIDEO_STAGE_REQUEST
			IF SHOULD_PLAY_CORONA_LOBBY_VIDEO()
				IF IS_CORONA_BIT_SET(CORONA_BINK_VIDEO_MUSIC_EVENT_TRIGGERED)
					PRINTLN("[TS] [CORONA_LOBBY_VIDEO] MAINTAIN_CORONA_LOBBY_VIDEO - Music event AW_LOBBY_MUSIC_START has already been triggered.")
					
					SET_CORONA_LOBBY_TV_JOB_INDEX()
					
					g_sArenaBgVars.iCoronaLobbyVideoStage = ciCORONA_LOBBY_VIDEO_STAGE_RENDER
					PRINTLN("[TS] [CORONA_LOBBY_VIDEO] MAINTAIN_CORONA_LOBBY_VIDEO - g_sArenaBgVars.iCoronaLobbyVideoStage. ciCORONA_LOBBY_VIDEO_STAGE_REQUEST > ciCORONA_LOBBY_VIDEO_STAGE_RENDER")
			
				ELIF PREPARE_MUSIC_EVENT("AW_LOBBY_MUSIC_START")
					PRINTLN("[TS] [CORONA_LOBBY_VIDEO] MAINTAIN_CORONA_LOBBY_VIDEO - Music event AW_LOBBY_MUSIC_START prepared.")
				
					SET_CORONA_LOBBY_TV_JOB_INDEX()
					
					g_sArenaBgVars.iCoronaLobbyVideoStage = ciCORONA_LOBBY_VIDEO_STAGE_RENDER
					PRINTLN("[TS] [CORONA_LOBBY_VIDEO] MAINTAIN_CORONA_LOBBY_VIDEO - g_sArenaBgVars.iCoronaLobbyVideoStage. ciCORONA_LOBBY_VIDEO_STAGE_REQUEST > ciCORONA_LOBBY_VIDEO_STAGE_RENDER")
				ELSE
					PRINTLN("[TS] [CORONA_LOBBY_VIDEO] MAINTAIN_CORONA_LOBBY_VIDEO - Preparing music event AW_LOBBY_MUSIC_START ...")
				ENDIF
			ELSE
				IF CANCEL_MUSIC_EVENT("AW_LOBBY_MUSIC_START")
					PRINTLN("[TS] [CORONA_LOBBY_VIDEO] MAINTAIN_CORONA_LOBBY_VIDEO - Music event AW_LOBBY_MUSIC_START cancelled.")
				ELSE
					PRINTLN("[TS] [CORONA_LOBBY_VIDEO] MAINTAIN_CORONA_LOBBY_VIDEO - Music event AW_LOBBY_MUSIC_START was not requested or not found.")
				ENDIF
				
				g_sArenaBgVars.iCoronaLobbyVideoStage = ciCORONA_LOBBY_VIDEO_STAGE_CLEANUP
				PRINTLN("[TS] [CORONA_LOBBY_VIDEO] MAINTAIN_CORONA_LOBBY_VIDEO - g_sArenaBgVars.iCoronaLobbyVideoStage. ciCORONA_LOBBY_VIDEO_STAGE_REQUEST > ciCORONA_LOBBY_VIDEO_STAGE_CLEANUP")
			ENDIF
		BREAK
		CASE ciCORONA_LOBBY_VIDEO_STAGE_RENDER
			IF SHOULD_PLAY_CORONA_LOBBY_VIDEO()
				IF NOT IS_CORONA_BIT_SET(CORONA_BINK_VIDEO_MUSIC_EVENT_TRIGGERED)
					TRIGGER_MUSIC_EVENT("AW_LOBBY_MUSIC_START")
					PRINTLN("[TS] [CORONA_LOBBY_VIDEO] MAINTAIN_CORONA_LOBBY_VIDEO - Music event AW_LOBBY_MUSIC_START triggered.")
					SET_CORONA_BIT(CORONA_BINK_VIDEO_MUSIC_EVENT_TRIGGERED)
				ENDIF
				
				IF NOT IS_CORONA_BIT_SET(CORONA_BINK_VIDEO_WORLD_MUTED)
					START_AUDIO_SCENE("DLC_AW_Arena_Lobby_Bink_Playing_Scene")
					PRINTLN("[TS] [CORONA_LOBBY_VIDEO] MAINTAIN_CORONA_LOBBY_VIDEO - Started audio scene DLC_AW_Arena_Lobby_Bink_Playing_Scene")
					SET_CORONA_BIT(CORONA_BINK_VIDEO_WORLD_MUTED)
				ENDIF
				
				#IF IS_DEBUG_BUILD
				IF GET_ARENA_WARS_TV_LOBBY_JOB_INDEX() <> iArenaWarsTvJobIndex
					PRINTLN("[TS] [CORONA_LOBBY_VIDEO][ARENA_WARS_TV] MAINTAIN_CORONA_LOBBY_VIDEO - GET_ARENA_WARS_TV_PLAYLIST_JOB() = ", GET_ARENA_WARS_TV_LOBBY_JOB_INDEX(), " iArenaWarsTvJobIndex = ", iArenaWarsTvJobIndex)
				ENDIF
				#ENDIF
				
				DRAW_ARENA_WARS_TV_LOBBY_PLAYLIST()
				IF GET_ARENA_WARS_TV_LOBBY_STATE() > AWTVS_CREATE					 
					SET_FADE_ARENA_LOBBY_BACKGROUND_TO_DRAW_AFTER_MISSION()
				ENDIF
				PREPARE_ARENA_LOBBY_BACKGROUND()
				
				IF GET_FM_JOB_ENTERY_TYPE() != ciMISSION_ENTERY_TYPE_ARENA_SERIES_CORONA_WALL
					IF GET_ARENA_WARS_TV_LOBBY_STATE() > AWTVS_LOOP_FADE_IN
						IF DOES_CAM_EXIST(g_sTransitionSessionData.ciCam)
							IF NOT ARE_VECTORS_ALMOST_EQUAL(GET_CAM_COORD(g_sTransitionSessionData.ciCam), <<-194.2056, -1853.7499, 70.3345>>, 3.0)
								SET_CAM_COORD(g_sTransitionSessionData.ciCam, <<-194.2056, -1853.7499, 70.3345>>)
								SET_CAM_ROT(g_sTransitionSessionData.ciCam, <<-10.0011, -0.0000, 131.0271>>)
								SET_CAM_FOV(g_sTransitionSessionData.ciCam, 42.6052)
								PRINTLN("[TS] [CORONA_LOBBY_VIDEO] MAINTAIN_CORONA_LOBBY_VIDEO - Moved corona camera")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				PRINTLN("[TS] [CORONA_LOBBY_VIDEO] MAINTAIN_CORONA_LOBBY_VIDEO - Shouldnt draw any more. Moving to ciCORONA_LOBBY_VIDEO_STAGE_IDLE")
				IF NOT IS_CORONA_BIT_SET(CORONA_BINK_VIDEO_MUSIC_EVENT_TRIGGERED)
					CANCEL_MUSIC_EVENT("AW_LOBBY_MUSIC_START")
					PRINTLN("[TS] [CORONA_LOBBY_VIDEO] MAINTAIN_CORONA_LOBBY_VIDEO - Music event AW_LOBBY_MUSIC_START cancelled in render stage.")
				ENDIF
				
				IF IS_CORONA_BIT_SET(CORONA_BINK_VIDEO_WORLD_MUTED)
					IF IS_AUDIO_SCENE_ACTIVE("DLC_AW_Arena_Lobby_Bink_Playing_Scene")
						STOP_AUDIO_SCENE("DLC_AW_Arena_Lobby_Bink_Playing_Scene")
						PRINTLN("[TS] [CORONA_LOBBY_VIDEO] MAINTAIN_CORONA_LOBBY_VIDEO - Stopped audio scene DLC_AW_Arena_Lobby_Bink_Playing_Scene")
					ENDIF
					CLEAR_CORONA_BIT(CORONA_BINK_VIDEO_WORLD_MUTED)
				ENDIF
				
				STOP_ARENA_WARS_TV_LOBBY_PLAYLIST()
				g_sArenaBgVars.iCoronaLobbyVideoStage = ciCORONA_LOBBY_VIDEO_STAGE_CLEANUP
				PRINTLN("[TS] [CORONA_LOBBY_VIDEO] MAINTAIN_CORONA_LOBBY_VIDEO - g_sArenaBgVars.iCoronaLobbyVideoStage. ciCORONA_LOBBY_VIDEO_STAGE_RENDER > ciCORONA_LOBBY_VIDEO_STAGE_CLEANUP")
			ENDIF
		BREAK
		CASE ciCORONA_LOBBY_VIDEO_STAGE_CLEANUP
			PRINTLN("[TS] [CORONA_LOBBY_VIDEO] MAINTAIN_CORONA_LOBBY_VIDEO - g_sArenaBgVars.iCoronaLobbyVideoStage. ciCORONA_LOBBY_VIDEO_STAGE_CLEANUP > ciCORONA_LOBBY_VIDEO_STAGE_IDLE")
			g_sArenaBgVars.iCoronaLobbyVideoStage = ciCORONA_LOBBY_VIDEO_STAGE_IDLE
		BREAK
		#IF FEATURE_GEN9_STANDALONE
		CASE ciCORONA_LOBBY_VIDEO_STAGE_RENDERPHASES_TOGGLE
			IF NOT GET_TOGGLE_PAUSED_RENDERPHASES_STATUS()
				TOGGLE_PAUSED_RENDERPHASES(TRUE)
				g_sArenaBgVars.iCoronaLobbyVideoStage = ciCORONA_LOBBY_VIDEO_STAGE_RENDERPHASES_RENDER
				PRINTLN("[TS] [CORONA_LOBBY_VIDEO] MAINTAIN_CORONA_LOBBY_VIDEO - g_sArenaBgVars.iCoronaLobbyVideoStage. ciCORONA_LOBBY_VIDEO_STAGE_RENDERPHASES_TOGGLE > ciCORONA_LOBBY_VIDEO_STAGE_RENDERPHASES_RENDER")
			ELSE
				g_sArenaBgVars.iCoronaLobbyVideoStage = ciCORONA_LOBBY_VIDEO_STAGE_RENDERPHASES_WAIT
				PRINTLN("[TS] [CORONA_LOBBY_VIDEO] MAINTAIN_CORONA_LOBBY_VIDEO - g_sArenaBgVars.iCoronaLobbyVideoStage. ciCORONA_LOBBY_VIDEO_STAGE_RENDERPHASES_TOGGLE > ciCORONA_LOBBY_VIDEO_STAGE_RENDERPHASES_WAIT")
			ENDIF
		BREAK
		CASE ciCORONA_LOBBY_VIDEO_STAGE_RENDERPHASES_RENDER
			TOGGLE_PAUSED_RENDERPHASES(FALSE)
			g_sArenaBgVars.iCoronaLobbyVideoStage = ciCORONA_LOBBY_VIDEO_STAGE_RENDERPHASES_WAIT
			PRINTLN("[TS] [CORONA_LOBBY_VIDEO] MAINTAIN_CORONA_LOBBY_VIDEO - g_sArenaBgVars.iCoronaLobbyVideoStage. ciCORONA_LOBBY_VIDEO_STAGE_RENDERPHASES_RENDER > ciCORONA_LOBBY_VIDEO_STAGE_RENDERPHASES_WAIT")
		BREAK
		CASE ciCORONA_LOBBY_VIDEO_STAGE_RENDERPHASES_WAIT
			IF DID_TRANSITION_SESSION_RECIEVED_START_LAUNCH()
				PRINTLN("[TS] [CORONA_LOBBY_VIDEO] MAINTAIN_CORONA_LOBBY_VIDEO - g_sArenaBgVars.iCoronaLobbyVideoStage. ciCORONA_LOBBY_VIDEO_STAGE_RENDERPHASES_WAIT > ciCORONA_LOBBY_VIDEO_STAGE_IDLE")
				g_sArenaBgVars.iCoronaLobbyVideoStage = ciCORONA_LOBBY_VIDEO_STAGE_IDLE
			ENDIF
		BREAK
		#ENDIF
	ENDSWITCH
ENDPROC

#IF IS_DEBUG_BUILD
BOOL bDebugDrawBackgroundTest
PROC MAINTAIN_BACKGROUND_TEST()
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_ArenaTextureTesting")
		IF IS_DEBUG_KEY_JUST_RELEASED(KEY_NUMPAD1, KEYBOARD_MODIFIER_CTRL_SHIFT, "Enable background")
			bDebugDrawBackgroundTest = !bDebugDrawBackgroundTest
		ENDIF
	ELSE
		bDebugDrawBackgroundTest = FALSE
	ENDIF

	IF bDebugDrawBackgroundTest
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_UP, KEYBOARD_MODIFIER_NONE, "increase adjust")
			fArenaCentreAdjust += 0.00001
		ELIF IS_DEBUG_KEY_JUST_PRESSED(KEY_DOWN, KEYBOARD_MODIFIER_NONE, "decrease adjust")
			fArenaCentreAdjust -= 0.00001
		ELIF IS_DEBUG_KEY_JUST_PRESSED(KEY_LEFT, KEYBOARD_MODIFIER_NONE, "decrease threshold")
			fArenaCentrePosMin += 0.0001
			fArenaCentrePosMax -= 0.0001
		ELIF IS_DEBUG_KEY_JUST_PRESSED(KEY_RIGHT, KEYBOARD_MODIFIER_NONE, "increase threshold")
			fArenaCentrePosMin -= 0.0001
			fArenaCentrePosMax += 0.0001
		ELIF IS_DEBUG_KEY_JUST_PRESSED(KEY_NUMPAD0, KEYBOARD_MODIFIER_NONE, "reset values")
			fArenaCentreAdjust = 0.0001
			fArenaCentrePosMin = 0.475
			fArenaCentrePosMax = 0.525
		ELIF IS_DEBUG_KEY_JUST_PRESSED(KEY_NUMPAD1, KEYBOARD_MODIFIER_NONE, "print values")
			PRINTLN("MAINTAIN_BACKGROUND_TEST - PRINT VALUES: ")
			PRINTLN("MAINTAIN_BACKGROUND_TEST - fArenaCentreAdjust: ", fArenaCentreAdjust)
			PRINTLN("MAINTAIN_BACKGROUND_TEST - fArenaCentrePosMin: ", fArenaCentrePosMin)
			PRINTLN("MAINTAIN_BACKGROUND_TEST - fArenaCentrePosMax: ", fArenaCentrePosMax)
		ENDIF
	
		MAINTAIN_ARENA_LOBBY_BACKGROUND()
	ENDIF
ENDPROC
#ENDIF


//Maintain the script clean up			
SCRIPT(FMMC_CORONA_DATA_MAINTAIN_SCRIPT_VARS fmmcCoronaMissionData)	 

	//Set that this script is now running
	SET_TRANSITION_SESSIONS_CORONA_CONTROLLER_IS_RUNNING()
	
	PRINTLN("[TEAM BAL DATA] fmmcCoronaMissionData.bIsHeistMission  = ", fmmcCoronaMissionData.bIsHeistMission)
	PRINTLN("[TEAM BAL DATA] g_FMMC_STRUCT.iMaxNumPlayersPerTeam[0] = ", g_FMMC_STRUCT.iMaxNumPlayersPerTeam[0])
	PRINTLN("[TEAM BAL DATA] g_FMMC_STRUCT.iMaxNumPlayersPerTeam[1] = ", g_FMMC_STRUCT.iMaxNumPlayersPerTeam[1])
	PRINTLN("[TEAM BAL DATA] g_FMMC_STRUCT.iMaxNumPlayersPerTeam[2] = ", g_FMMC_STRUCT.iMaxNumPlayersPerTeam[2])
	PRINTLN("[TEAM BAL DATA] g_FMMC_STRUCT.iMaxNumPlayersPerTeam[3] = ", g_FMMC_STRUCT.iMaxNumPlayersPerTeam[3])
	SET_LOCAL_MAX_TEAM_COUNTS()
	//Set up the camera
	IF fmmcCoronaMissionData.bTutorial = FALSE
	AND NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
	
		IF NOT SHOULD_DO_HEIST_CORONA_CAM(fmmcCoronaMissionData)
		OR SKIP_CAM_BECAUSE_JIP_SVM()
			PRINTLN("[AMEC][HEIST_MISC] - FM_MAINTAIN_TRANSITION_PLAYERS - Camera is outside or un-initialised. Skip corona camera setup. GET_HEIST_PROPERTY_CAM_STAGE() = ", GET_HEIST_PROPERTY_CAM_STATE_NAME(GET_HEIST_PROPERTY_CAM_STAGE()))
		ELSE
			INT iCameraSetupBS
			IF SVM_FLOW_IS_CURRENT_MISSION_SVM_FLOW()
				SET_BIT(iCameraSetupBS, ciCORONA_CAMERA_SETUP_BS_SVM)
			ENDIF
		
			SETUP_CORONA_CAMERA( 	g_sTransitionSessionData.ciCam, fmmcCoronaMissionData.iMissionType, fmmcCoronaMissionData.iMissionVariation, 
									fmmcCoronaMissionData.vStartPoint, fmmcCoronaMissionData.vCamPos, 
									fmmcCoronaMissionData.vCamHead, fmmcCoronaMissionData.bIsContactMission,
									 fmmcCoronaMissionData.bIsHeistMission, 
									sLocalVars.vFocusPos, sLocalVars.bStillNeedToSetFocus, FALSE, DEFAULT, iCameraSetupBS)
									
			// Set this flag in case we have moved into corona from beginning
			sLocalVars.bOnCallCameraSetUp = TRUE
			
		ENDIF
		
	ENDIF
	
	//Blur the screen if we are not on the tutorial
//	IF fmmcCoronaMissionData.bTutorial = FALSE		
//	#IF IS_DEBUG_BUILD
//	AND g_b_UseNewCoronaFlow = FALSE
//	#ENDIF	
//		IF IS_SKYSWOOP_AT_GROUND()
//			TRIGGER_SCREENBLUR_FADE_IN(0)
//			PRINTLN("[TS] TRIGGER_SCREENBLUR_FADE_IN(0)")			
//		ENDIF
//	#IF IS_DEBUG_BUILD
//	ELSE	
//		PRINTLN("[TS] fmmcCoronaMissionData.bTutorial - TRIGGER_SCREENBLUR_FADE_IN Not called")		
//	#ENDIF
//	ENDIF
	
	//Process pre game
	PROCESS_PRE_GAME(fmmcCoronaMissionData)	
	
	VECTOR vOnCallCamera
	
	//make widgets
	#IF IS_DEBUG_BUILD
	CREATE_WIDGETS()
	#ENDIF	
	
	SET_THIS_THREAD_PRIORITY(THREAD_PRIO_LOWEST)
	PRINTLN("[TS] SET_THIS_THREAD_PRIORITY(THREAD_PRIO_LOWEST)")
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("FMMC TransitionPly start...")
	#ENDIF
	#ENDIF	
	
	g_sArenaBgVars.iCoronaLobbyVideoStage = ciCORONA_LOBBY_VIDEO_STAGE_IDLE
	
	// Main loop.
	WHILE TRUE
	
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		SCRIPT_PROFILER_END_OF_FRAME()
		#ENDIF
		#ENDIF	
		
		// One wait to rule them all. This can be the ONLY wait from here on in.....
		MP_LOOP_WAIT_ZERO()
		
		#IF IS_DEBUG_BUILD
		MAINTAIN_BACKGROUND_TEST()
		#ENDIF
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		SCRIPT_PROFILER_START_OF_FRAME()
		#ENDIF
		#ENDIF	
		
//		#IF IS_DEBUG_BUILD
//		#IF SCRIPT_PROFILER_ACTIVE
//		CORONA_SUPPRESS_FRONTEND_RENDERING_THIS_FRAME()
//		#ENDIF
//		#ENDIF	
		
		IF NOT HAS_ON_CALL_TRANSITION_SESSION_BEEN_SET_UP()
		AND NOT AM_I_TRANSITION_SESSIONS_STARTING_ON_CALL()
			//Not on call then disable
			REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME() 
		ENDIF
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME")
		#ENDIF
		#ENDIF
		
		//Tell main persistant we are running!		
		IF NOT IS_TRANSITION_SESSIONS_CORONA_CONTROLLER_IS_RUNNING()
			SET_TRANSITION_SESSIONS_CORONA_CONTROLLER_IS_RUNNING()
		ENDIF	
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("SET_TRANSITION_SESSIONS_CORONA_CONTROLLER_IS_RUNNING")
		#ENDIF
		#ENDIF
		
		//If we still need to set the focus then wait for the switch to finish.
		IF sLocalVars.bStillNeedToSetFocus = TRUE
			IF NOT IS_PLAYER_SWITCH_IN_PROGRESS() 
			AND NOT IS_THIS_PLAYER_LEAVING_THE_CORONA(PLAYER_ID())
			AND IS_PLAYER_IN_CORONA()
				sLocalVars.bStillNeedToSetFocus = FALSE
				SET_UP_CAMERA_FOCUS_POS_AND_VEL(sLocalVars.vFocusPos)	
			ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("SET_UP_CAMERA_FOCUS_POS_AND_VEL")
		#ENDIF
		#ENDIF
		
		// If we are setting up corona in heist room and stil to set up the camera then hide all players
		IF fmmcCoronaMissionData.bIsHeistMission
		AND NOT AM_I_TRANSITION_SESSIONS_STARTING_HEIST_QUICK_MATCH()
		AND NOT AM_I_TRANSITION_SESSIONS_STARTING_GANG_OPS_QUICK_MATCH()
			IF NOT sLocalVars.bSET_USE_HI_DOFSetUp
			OR NOT sLocalVars.bPAUSED_RENDERPHASES
				HIDE_ALL_PLAYERS_FOR_CORONA()
			ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("HIDE_ALL_PLAYERS_FOR_CORONA")
		#ENDIF
		#ENDIF
			
//		//Toggle those renderfases?
		IF fmmcCoronaMissionData.bTutorial = FALSE
		AND NOT HAS_FIRST_STRAND_MISSION_BEEN_PASSED()
		AND NOT IS_A_STRAND_MISSION_BEING_INITIALISED() 
			IF NOT sLocalVars.bRunningMissionCut
				IF IS_SKYSWOOP_AT_GROUND()
					IF NOT sLocalVars.bSET_USE_HI_DOFSetUp
						sLocalVars.bSET_USE_HI_DOFSetUp = TRUE
						sLocalVars.bPAUSED_RENDERPHASES = FALSE					
						//TRIGGER_SCREENBLUR_FADE_IN(0)
					ELIF sLocalVars.bPAUSED_RENDERPHASES = FALSE	
					AND NOT FM_FLOW_DISABLE_DPAD_RIGHT_AND_WALK_IN_FOR_CORONA()
						IF NOT IS_CORONA_INITIALISING_A_QUICK_RESTART()
						AND NOT IS_THIS_A_ROUNDS_MISSION_FOR_CORONA()
						
							NET_NL()NET_PRINT("[TS] SHUTDOWN_LOADING_SCREEN ")NET_NL()
							SHUTDOWN_LOADING_SCREEN()
						
							IF NOT SHOULD_DO_HEIST_CORONA_CAM(fmmcCoronaMissionData)
								sLocalVars.bPAUSED_RENDERPHASES = TRUE
								PRINTLN("[AMEC][HEIST_MISC] - FM_MAINTAIN_TRANSITION_PLAYERS - Camera is outside or this is the leader. Skip TOGGLE_RENDERPHASES. GET_HEIST_PROPERTY_CAM_STAGE() = ", GET_HEIST_PROPERTY_CAM_STATE_NAME(GET_HEIST_PROPERTY_CAM_STAGE()))
							
	//							IF GET_HEIST_PROPERTY_CAM_STAGE() = HEIST_PROPERTY_HOLD_STAGE
	//								TOGGLE_RENDERPHASES(FALSE) 
	//								TOGGLE_RENDERPHASES(FALSE)	
	//								PRINTLN("[AMEC][HEIST_MISC] - FM_MAINTAIN_TRANSITION_PLAYERS - Paused renderphases early for external heist camera shot.")
	//							ENDIF
							ELSE
								sLocalVars.bPAUSED_RENDERPHASES = TRUE
								IF NOT IS_BIT_SET(g_GangOpsPlanning.iPlanningBitSet, GANG_OPS_BITSET_TOGGLED_RENDERPHASES)
								AND NOT CORONA_SHOULD_FRONTEND_BE_HIDDEN(TRUE)
									TOGGLE_RENDERPHASES(FALSE) 
									TOGGLE_RENDERPHASES(FALSE)		
									PRINTLN("[TS] TOGGLE_RENDERPHASES(FALSE)")					
									PRINTLN("[TS] TOGGLE_RENDERPHASES(FALSE)")	
								ENDIF
							ENDIF
							
						ENDIF	
					ENDIF	
				ENDIF	
			ENDIF
		ENDIF	
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("Toggle those renderfases?")
		#ENDIF
		#ENDIF
		
		//do we need to repause the render phases
		IF SHOULD_TRANSITION_SESSIONS_CORONA_NEEDS_TO_REPAUSE_RENDERPHASES()
			PRINTLN("[TS] SHOULD_TRANSITION_SESSIONS_CORONA_NEEDS_TO_REPAUSE_RENDERPHASES = TRUE, resetting render flag")					
			sLocalVars.bSET_USE_HI_DOFSetUp = FALSE
			CLEAR_TRANSITION_SESSIONS_CORONA_NEEDS_TO_REPAUSE_RENDER_PHASES()
		ENDIF
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("CLEAR_TRANSITION_SESSIONS_CORONA_NEEDS_TO_REPAUSE_RENDER_PHASES")
		#ENDIF
		#ENDIF
		
		INT iSetupBS = 0
		
		// The player is in a corona, so we don't need to set up a camera
		IF IS_PLAYER_IN_CORONA()
		AND NOT IS_TRANSITION_SESSION_RESTARTING()
			IF HAS_ON_CALL_TRANSITION_SESSION_SETUP_CAMERA()
				IF NOT AM_I_TRANSITION_SESSIONS_STARTING_HEIST_QUICK_MATCH()
				AND NOT fmmcCoronaMissionData.bIsHeistMission
				AND NOT AM_I_TRANSITION_SESSIONS_STARTING_GANG_OPS_QUICK_MATCH()
					IF IS_SKYSWOOP_AT_GROUND()
						IF NOT sLocalVars.bOnCallCameraSetUp
							PRINTLN("[TS] FM_Maintain_Transition_Players.sc Player now in corona but camera needs to be set up, camera not yet set up.")			
							IF SVM_FLOW_IS_CURRENT_MISSION_SVM_FLOW()
								SET_BIT(iSetupBS, ciCORONA_CAMERA_SETUP_BS_SVM)
							ENDIF
							SETUP_CORONA_CAMERA( 	g_sTransitionSessionData.ciCam, fmmcCoronaMissionData.iMissionType, fmmcCoronaMissionData.iMissionVariation, 
									fmmcCoronaMissionData.vStartPoint, vOnCallCamera, 
									fmmcCoronaMissionData.vCamHead, TRUE,
									 fmmcCoronaMissionData.bIsHeistMission, 
									sLocalVars.vFocusPos, sLocalVars.bStillNeedToSetFocus, FALSE, DEFAULT, iSetupBS)
					
							sLocalVars.bOnCallCameraSetUp = TRUE
							sLocalVars.bPAUSED_RENDERPHASES = FALSE
							
						ELIF sLocalVars.bPAUSED_RENDERPHASES = FALSE
							
							CLEAR_ON_CALL_TRANSITION_SESSION_SETUP_CAMERA()
							
							sLocalVars.bPAUSED_RENDERPHASES = TRUE
							TOGGLE_RENDERPHASES(FALSE) 
							TOGGLE_RENDERPHASES(FALSE)
							
							PRINTLN("[TS] FM_Maintain_Transition_Players.sc Player now in corona but freeze has not occurred")		
						ENDIF
					
					ENDIF
	
				ENDIF
			ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("IS_PLAYER_IN_CORONA")
		#ENDIF
		#ENDIF
		
		//hide everything and disable all the players during the transition
		IF SHOULD_TRANSITION_SESSIONS_CORONA_CONTROLLER_MAINTAIN_CAMERA()
		
			// The player is in a corona, so we don't need to set up a camera
			IF IS_PLAYER_IN_CORONA()
				IF NOT sLocalVars.bOnCallCameraSetUp
					sLocalVars.bOnCallCameraSetUp = TRUE
				ENDIF
			ENDIF
			IF HAS_ON_CALL_TRANSITION_SESSION_BEEN_SET_UP()
				IF NOT sLocalVars.bOnCallCameraSetUp
					IF NOT AM_I_TRANSITION_SESSIONS_STARTING_GANG_OPS_QUICK_MATCH()
					OR GANG_OPS_SCREEN_FADED_OUT()
							//Clean up the sky cam
						IF NOT IS_SKYSWOOP_AT_GROUND()
							PRINTLN("[TS] FM_Maintain_Transition_Players - HAS_ON_CALL_TRANSITION_SESSION_BEEN_SET_UP - KILL_SKYCAM")
							KILL_SKYCAM()
						ENDIF
						PRINTLN("[TS] HAS_ON_CALL_TRANSITION_SESSION_BEEN_SET_UP = TRUE, camera not yet set up. Trigger freeze above player's head")			
					
						// Ensure our pause menu is clean if not already
						CLEAR_PAUSE_MENU_FOR_CORONA_IF_ACTIVE()
					
						IF SVM_FLOW_IS_CURRENT_MISSION_SVM_FLOW()
							SET_BIT(iSetupBS, ciCORONA_CAMERA_SETUP_BS_SVM)
						ENDIF
					
						SETUP_CORONA_CAMERA( 	g_sTransitionSessionData.ciCam, fmmcCoronaMissionData.iMissionType, fmmcCoronaMissionData.iMissionVariation, 
								fmmcCoronaMissionData.vStartPoint, vOnCallCamera, 
								fmmcCoronaMissionData.vCamHead, TRUE,
								 fmmcCoronaMissionData.bIsHeistMission, 
								sLocalVars.vFocusPos, sLocalVars.bStillNeedToSetFocus, FALSE, DEFAULT, iSetupBS)
						
						IF AM_I_TRANSITION_SESSIONS_STARTING_GANG_OPS_QUICK_MATCH()
							REQUEST_SCRIPT("am_mp_smpl_interior_int")
							REQUEST_SCRIPT("am_mp_smpl_interior_ext")
							REQUEST_SCRIPT("am_mp_defunct_base")
							SET_ENTITY_COORDS(PLAYER_PED_ID(), fmmcCoronaMissionData.vStartPoint)
							
							INTERIOR_INSTANCE_INDEX interiorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<345.0041, 4842.0010, -59.9997>>, "xm_x17dlc_int_02")
							PIN_INTERIOR_IN_MEMORY_VALIDATE(interiorIndex)
						ENDIF
						
						sLocalVars.bOnCallCameraSetUp = TRUE
						sLocalVars.bPAUSED_RENDERPHASES = FALSE
						
						SET_TRANSITION_SESSION_AUDIO_SCENE(fmmcCoronaMissionData, TRUE)
						
						PRINTLN("[TS] SHOULD_TRANSITION_SESSIONS_CORONA_CONTROLLER_MAINTAIN_CAMERA = TRUE, on call, disable map")
						DISPLAY_RADAR(FALSE)
					ENDIF
				ELIF sLocalVars.bPAUSED_RENDERPHASES = FALSE
					
					sLocalVars.bPAUSED_RENDERPHASES = TRUE
					TOGGLE_RENDERPHASES(FALSE) 
					TOGGLE_RENDERPHASES(FALSE)
					
					PRINTLN("[TS] SHOULD_TRANSITION_SESSIONS_CORONA_CONTROLLER_MAINTAIN_CAMERA - TOGGLE_RENDERPHASES(FALSE) x2 for on call freeze")
					
				ENDIF
			ELSE
				IF NOT bAudioSceneEnabled
					SET_TRANSITION_SESSION_AUDIO_SCENE(fmmcCoronaMissionData, TRUE, TRUE)
				ENDIF
			ENDIF
		
			MAINTAIN_CORONA_SCREEN_DURING_TRANSITION(fmmcCoronaMissionData.iMissionType) //, fmmcCoronaMissionData.bTutorial)
			
			//Do a time out and bail if we get stuck here
			MAINTAIN_CORONA_LAUNCH_SAFTY_BAIL()
		ENDIF	
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("SHOULD_TRANSITION_SESSIONS_CORONA_CONTROLLER_MAINTAIN_CAMERA")
		#ENDIF
		#ENDIF
		
		IF HAS_LOCAL_PLAYER_DONE_RACE_TUTORIAL_CORONA_SWOOP() 
			IF NOT sLocalVars.bRunningMissionCut
				IF NOT sLocalVars.bSET_USE_HI_DOFSetUp
	//				SET_CAM_ACTIVE(g_sTransitionSessionData.ciCam, FALSE)
	//				RENDER_SCRIPT_CAMS(FALSE, FALSE, DEFAULT_INTERP_TO_FROM_GAME, TRUE, TRUE)
	//				DESTROY_CAM(g_sTransitionSessionData.ciCam)	
	//				g_sTransitionSessionData.ciCam = NULL
					CLEAN_UP_INTRO_STAGE_CAM(g_sTransitionSessionData.ciCam, -1, TRUE, TRUE)
					IF SVM_FLOW_IS_CURRENT_MISSION_SVM_FLOW()
						SET_BIT(iSetupBS, ciCORONA_CAMERA_SETUP_BS_SVM)
					ENDIF
					SETUP_CORONA_CAMERA( 	g_sTransitionSessionData.ciCam, fmmcCoronaMissionData.iMissionType, fmmcCoronaMissionData.iMissionVariation, 
											fmmcCoronaMissionData.vStartPoint, fmmcCoronaMissionData.vCamPos, 
											fmmcCoronaMissionData.vCamHead, fmmcCoronaMissionData.bIsContactMission,
											 fmmcCoronaMissionData.bIsHeistMission, 
											sLocalVars.vFocusPos, sLocalVars.bStillNeedToSetFocus, TRUE, TRUE, iSetupBS)		
					sLocalVars.bSET_USE_HI_DOFSetUp = TRUE
					sLocalVars.bPAUSED_RENDERPHASES = FALSE					
					//TRIGGER_SCREENBLUR_FADE_IN(0)
				ELIF sLocalVars.bPAUSED_RENDERPHASES = FALSE
					sLocalVars.bPAUSED_RENDERPHASES = TRUE		
					TOGGLE_RENDERPHASES(FALSE) 
					TOGGLE_RENDERPHASES(FALSE)		
				ENDIF	
			ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("HAS_LOCAL_PLAYER_DONE_RACE_TUTORIAL_CORONA_SWOOP")
		#ENDIF
		#ENDIF
		
		// Track any update from corona on starting music
		MAINTAIN_PERSISTENT_CORONA_MUSIC()
		MAINTAIN_PERSISTENT_CORONA_AMBIENT_AUDIO()
		MAINTAIN_PERSISTENT_CORONA_LOBBY_AUDIO_SCENE(fmmcCoronaMissionData)
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_PERSISTENT_CORONA_MUSIC")
		#ENDIF
		#ENDIF
		
		MAINTAIN_CORONA_LOBBY_VIDEO()
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_CORONA_LOBBY_VIDEO")
		#ENDIF
		#ENDIF
		
		// Listen for corona prompt to begin load scene
		MAINTAIN_CORONA_LOAD_SCENE(fmmcCoronaMissionData)
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_CORONA_LOAD_SCENE")
		#ENDIF
		#ENDIF
		
		MAINTAIN_CORONA_MAP()
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_CORONA_MAP")
		#ENDIF
		#ENDIF
		
		IF NOT NETWORK_IS_ACTIVITY_SESSION()
		AND NETWORK_IS_IN_TRANSITION()
		AND NOT NETWORK_IS_TRANSITION_BUSY()
			IF g_TransitionSessionNonResetVars.iLastCoronaHost != GET_TRANSITION_SESSION_HOST()
			AND GET_TRANSITION_SESSION_HOST() != -1
				g_TransitionSessionNonResetVars.iLastCoronaHost = GET_TRANSITION_SESSION_HOST()
				g_TransitionSessionNonResetVars.ghLastCoronaHost = GET_TRANSITION_PLAYER_GAMER_HANDLE(GET_TRANSITION_SESSION_HOST())
				PRINTLN("[TS] -  FM_MAINTAIN_TRANSITION_PLAYERS - g_TransitionSessionNonResetVars.iLastCoronaHost = ", g_TransitionSessionNonResetVars.iLastCoronaHost)	
			ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("NETWORK_IS_ACTIVITY_SESSION")
		#ENDIF
		#ENDIF
		
		//Maintain the BG
		
		CV2_MAINTAIN_DRAW_STUNT_RACE_CORONA_BG_FLAG(iAlpha)
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("CV2_MAINTAIN_DRAW_STUNT_RACE_CORONA_BG_FLAG")
		#ENDIF
		#ENDIF
		
		//If a network game is in progress we do this
		IF NETWORK_IS_GAME_IN_PROGRESS()
			tiTimeOfDayH = GET_CLOCK_HOURS()
			tiTimeOfDayM = GET_CLOCK_MINUTES()
			tiTimeOfDayS = GET_CLOCK_SECONDS()
			//Relaunch the main freemode script
			MAINTAIN_LAUNCHING_FREEMODE_SCRIPT(fmmcCoronaMissionData)
			
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_LAUNCHING_FREEMODE_SCRIPT")
		#ENDIF
		#ENDIF
				
			//Maintain the script clean up			
			MAINTAIN_SCRIPT_CLEANUP_CALLS(fmmcCoronaMissionData.iMissionType, fmmcCoronaMissionData)
			
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_SCRIPT_CLEANUP_CALLS")
		#ENDIF
		#ENDIF
			
			//All player stuff in here
			SWITCH sLocalVars.iGameState
			//Set stuff up
				CASE GAME_STATE_INI	
					IF ServerBD.iGameState = GAME_STATE_INI
						MAINTAIN_PLAYER_CORONA_OPTIONS()
					ENDIF
					IF HAS_SERVER_BALANCED_TEAMS(serverBD)
						IF serverBD.bPlayListCorona	
							SET_PLAYER_ON_A_PLAYLIST(TRUE)
						ENDIF
						SET_CLIENT_GAME_STATE(GAME_STATE_RUNNING)			
					ENDIF
					
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE 
						ADD_SCRIPT_PROFILE_MARKER("GAME_STATE_INI")
					#ENDIF
					#ENDIF
				BREAK
				
				//Control the play list.
				CASE GAME_STATE_RUNNING
					IF ServerBD.iGameState >= GAME_STATE_END
						IF IS_THIS_A_RACE_TYPE(serverBD)	
							//ASSIGN_PLAYER_TEAM_RACE()
							SET_RACE_TEAM_VARIABLES(serverBD, playerBD)
							ASSIGN_PLAYER_SEAT_POSITION(serverBD, playerBD)
						ENDIF
						SET_CLIENT_GAME_STATE(GAME_STATE_SET_GLOBALS)
					ENDIF
					
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE 
						ADD_SCRIPT_PROFILE_MARKER("GAME_STATE_RUNNING")
					#ENDIF
					#ENDIF
				BREAK
				
				CASE GAME_STATE_SET_GLOBALS
					//IF iMyArrayPos != -1
						IF serverBD.bPlayListCorona	
							PRINTLN("[TS] serverBD.bPlayListCorona = TRUE")
							SET_PLAYER_ON_A_PLAYLIST(TRUE)
						ENDIF
						SET_CLIENT_BALANCED_TEAM_GLOBALS(serverBD, playerBD)
						IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_STOCKPILE(g_FMMC_STRUCT.iAdversaryModeType)
						OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_BOMBUSHKA(g_FMMC_STRUCT.iAdversaryModeType)
						#IF IS_DEBUG_BUILD
						OR ServerBD.bBug1861235 = TRUE
						#ENDIF
							CALL_ASSIGN_PLAYER_SEAT_POSITION_MISSION()
						ENDIF
						//Set my globals
						//Set my globals have been set flag
						SET_TRANSITION_SESSION_TEAMS_BALANCED()
						
						//set that we've balanced our teams
						SET_TRANSITION_SESSION_SCRIPT_HAS_BALANCED_TEAMS()
						SET_CLIENT_GAME_STATE(GAME_STATE_SET_WAIT)
						PRINTLN("[TRANSITION SESSIONS] ************************")
						PRINTLN("[TRANSITION SESSIONS] *E       END           *")
						PRINTLN("[TRANSITION SESSIONS] ************************")
					//ELSE
					//	PRINTLN("iMyArrayPos = ", iMyArrayPos)
					//ENDIF
				BREAK
				
				CASE GAME_STATE_SET_WAIT
					//Check to see if all players are ready.
				BREAK
				
				CASE GAME_STATE_END
					SCRIPT_CLEANUP(fmmcCoronaMissionData.iMissionType, fmmcCoronaMissionData)
				BREAK
			ENDSWITCH
			
			//All server stuff in here. 
			SWITCH ServerBD.iGameState	
				//Set stuff up and wait
				CASE GAME_STATE_INI
					IF SHOULD_TRANSITION_SESSION_TEAM_BALANCING_RUN()
						SERVER_SET_BROADCAST_DATA(serverBD)
						PRINTLN("[TS] START_NET_TIMER")
						//START_NET_TIMER(serverBD.tdJoinTimer)
						SET_FM_IN_CORONA_SERVER_GAME_STATE(serverBD, GAME_STATE_RUNNING)
//						IF fmmcCoronaMissionData.bIsHeistMission
						IF SHOULD_FORCES_SIZED_TEAM_BALANCING_RUN()
							doForcedSizeTeamBalancing = TRUE
						ENDIF
						IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_MONSTER_JAM(g_FMMC_STRUCT.iAdversaryModeType)
							FORCE_TEAM_MAX_SIZES_BASED_ON_RATIO(g_TransitionSessionNonResetVars.sTransVars.iNumberOfJoinedPlayers)
						ENDIF
						IF NEED_TO_RESET_LOCAL_MAX_TEAM_COUNTS()
							SET_LOCAL_MAX_TEAM_COUNTS()
						ENDIF
						//Set up soem crap data
						#IF IS_DEBUG_BUILD
						INT_DEBUG_DATA_FOR_TEST(fmmcCoronaMissionData)
						PRINT_ALL_PLAYER_RELATIONSHIPS()
						#ENDIF
						CHECK_TEAM_CAPS_BROKEN()
						
						#IF IS_DEBUG_BUILD
						#IF SCRIPT_PROFILER_ACTIVE 
							ADD_SCRIPT_PROFILE_MARKER("SERVER_GAME_STATE_INI")
						#ENDIF
						#ENDIF
					ENDIF
				BREAK

				//Deal with the balancing
				CASE GAME_STATE_RUNNING
					//PRINTLN("[TS] GAME_STATE_RUNNING")
					IF IS_THIS_A_TEAM_MISSION_TYPE(serverBD)
						//PRINTLN("[TS] IS_THIS_A_TEAM_MISSION_TYPE")
						IF SERVER_TEAM_BALANCING_DM_MISSION(serverBD, playerBD, doForcedSizeTeamBalancing)
							IF g_sTransitionSessionData.sMissionRoundData.iNumberOfRounds != 0
							AND bTeamsReaCapped
							OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciSELECTABLE_TEAM_NUMBERS)
								PRINTLN("[SST] * [TEAM BAL DATA] - CORRECT_TEAM_COUNT_BASSED_ON_PLAYER_COUNT - g_sTransitionSessionData.sMissionRoundData.iMaxNumberOfTeams = g_FMMC_STRUCT.iMaxNumberOfTeams = ", g_FMMC_STRUCT.iMaxNumberOfTeams)
								GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].sJobRoundData.iMaxNumberOfTeams = g_FMMC_STRUCT.iMaxNumberOfTeams
								PRINTLN("[TS] [SST] * FM_MTP GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].sJobRoundData.iMaxNumberOfTeams= ", g_FMMC_STRUCT.iMaxNumberOfTeams)
								g_sTransitionSessionData.sMissionRoundData.iMaxNumberOfTeams = g_FMMC_STRUCT.iMaxNumberOfTeams
								g_FMMC_STRUCT.iNumberOfTeams = g_FMMC_STRUCT.iMaxNumberOfTeams
								IF IS_THIS_TRANSITION_SESSION_NOT_A_PLAYLIST()
									SET_ACTIVITY_PLAYER_MAX()
								ENDIF
							ENDIF
							SET_FM_IN_CORONA_SERVER_GAME_STATE(serverBD, GAME_STATE_END)
						//ELSE
							//PRINTLN("[TS] SERVER_TEAM_BALANCING_DM_MISSION = FALSE")						
						ENDIF
					ELIF IS_THIS_A_RACE_TYPE(serverBD)
						//PRINTLN("[TS] IS_THIS_A_RACE_TYPE")
						#IF IS_DEBUG_BUILD
						DO_BALANCING_DEBUG()
						#ENDIF
						IF SERVER_TEAM_BALANCING_RACE(serverBD, playerBD)
							SET_FM_IN_CORONA_SERVER_GAME_STATE(serverBD, GAME_STATE_END)
						//ELSE
							//PRINTLN("[TS] SERVER_TEAM_BALANCING_RACE = FALSE")	
						ENDIF
					ELSE
						PRINTLN("[TS] NONE TEAM BALANCED MISSION - SET_BIT(serverBDpassed.iServerBitSet, SERV_BITSET_STOP_TEAM_BALANCING)")
						SET_BIT(serverBD.iServerBitSet, SERV_BITSET_STOP_TEAM_BALANCING)
						SET_FM_IN_CORONA_SERVER_GAME_STATE(serverBD, GAME_STATE_END)
					ENDIF
					
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE 
						ADD_SCRIPT_PROFILE_MARKER("SERVER_GAME_STATE_RUNNING")
					#ENDIF
					#ENDIF
				BREAK
				
				//End
				CASE GAME_STATE_END
				BREAK
			ENDSWITCH
		
		//Not in a network game request launch script and restart
		ELSE
			SET_CLOCK_TIME(tiTimeOfDayH, tiTimeOfDayM, tiTimeOfDayS)
			IF IS_TRANSITION_SESSION_LAUNCHING()
				PRINTLN("[TS] fm_maintain_transition_players.sc - waiting for NETWORK_IS_GAME_IN_PROGRESS() ")
				IF bScriptNeedsLaunched = FALSE
					PRINTLN("FM_MAINTAIN_TRANSITION_PLAYERS - GET_FRAME_COUNT bScriptNeedsLaunched = ", GET_FRAME_COUNT())
					SET_CURRENT_GAMEMODE(GAMEMODE_FM)
					REQUEST_FM_RESTART_SCRIPTS(fmmcCoronaMissionData.bIsHeistMission)
					bScriptNeedsLaunched = TRUE
				ENDIF
				//We got a bail event then get out.
				IF HAS_TRANSITION_SESSIONS_RECEIVED_BAIL_EVENT()
					PRINTLN("[TS] HAS_TRANSITION_SESSIONS_RECEIVED_BAIL_EVENT()")
					RESET_TRANSITION_SESSION_NON_RESET_VARS()
					CLEAR_TRANSITION_SESSIONS_BITSET()
					CLEAR_TRANSITION_SESSION_LAUNCHING()
					SCRIPT_CLEANUP(fmmcCoronaMissionData.iMissionType, fmmcCoronaMissionData)
				ENDIF
				
				IF NOT HAS_ON_CALL_TRANSITION_SESSION_BEEN_SET_UP()
					IF fmmcCoronaMissionData.bTutorial
						SET_CLOCK_TIME(g_fmTriggerIntro.iRaceClockHours, 0, 0)
						PRINTLN("[dsw] Setting SP clock hours to ", g_fmTriggerIntro.iRaceClockHours)
					ENDIF
				ENDIF
			ELSE
				PRINTLN("[TS] FM Maintain Players In Corona - IS_TRANSITION_SESSION_LAUNCHING() = FALSE")
				SCRIPT_CLEANUP(fmmcCoronaMissionData.iMissionType, fmmcCoronaMissionData)
			ENDIF
			
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
				ADD_SCRIPT_PROFILE_MARKER("SCRIPT_CLEANUP")
			#ENDIF
			#ENDIF
		ENDIF
		
		//Kill this script
		#IF IS_DEBUG_BUILD
			IF  bKillScript
				SCRIPT_CLEANUP(fmmcCoronaMissionData.iMissionType, fmmcCoronaMissionData)
			ENDIF
		#ENDIF
	ENDWHILE
// End of Mission
ENDSCRIPT
