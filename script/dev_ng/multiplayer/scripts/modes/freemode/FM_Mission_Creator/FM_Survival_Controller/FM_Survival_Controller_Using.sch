USING "globals.sch"
USING "FMMC_HEADER.sch"
USING "net_spectator_cam.sch"
USING "net_celebration_screen.sch"

// *********************************** \\
// *********************************** \\
//                                     \\
// 			 LOCAL VARIABLES           \\
//                                     \\
// *********************************** \\
// *********************************** \\

// Game States
CONST_INT SURVIVAL_GAME_STATE_INIT                                     	0
CONST_INT SURVIVAL_GAME_STATE_INIT_SPAWN                      			1
CONST_INT SURVIVAL_GAME_FADE_IN 										2
CONST_INT SURVIVAL_GAME_TEAMCUT_CUTSCENE								3
CONST_INT SURVIVAL_GAME_INTRO_CUTSCENE									4
CONST_INT SURVIVAL_GAME_INTRO_CAMERA_BLEND								5
CONST_INT SURVIVAL_GAME_STATE_RUNNING                             		6
CONST_INT SURVIVAL_GAME_STATE_WAITING_TO_LEAVE                          7
CONST_INT SURVIVAL_GAME_STATE_LEAVE                                 	8
CONST_INT SURVIVAL_GAME_STATE_END                                    	9

CONST_INT NUM_SURVIVAL_SUB_STAGES										3
CONST_INT NUMBER_HORDE_WAVES                                         	2
CONST_INT ABSOLUTE_MAX_HORDE_WAVES										50
CONST_INT TEST_MODE_DELAY_BETWEEN_WAVES									5000
CONST_INT DELAY_BETWEEN_WAVES                                          	20000
CONST_INT GET_BACK_TO_HORDE_TIME_LIMIT                                	30000
CONST_INT GET_BACK_TO_HORDE_IN_WATER_TIMER								10000
CONST_INT TAKING_PART_XP                                              	100
CONST_FLOAT AT_HORDE_DISTANCE                                          	200.0
CONST_INT NUM_LAST_ENEMIES_TO_BLIP                                     	4
CONST_INT MAX_NUM_SURVIVAL_HELIS                                       	3
CONST_INT NUM_HELI_PEDS													3
CONST_INT SURVIVAL_HELI_0_START_CIRC_COUNT                             	0
CONST_INT SURVIVAL_HELI_1_START_CIRC_COUNT                              4
CONST_INT SURVIVAL_HELI_2_START_CIRC_COUNT                             	8
CONST_INT MAX_NUM_SQUAD_PEDS                                           	28
CONST_INT MAX_NUM_ENEMY_SQUADS											MAX_SURV_ENEMY_SQUADS//*NUM_SURVIVAL_SUB_STAGES
CONST_INT NUM_LAND_VEH_PEDS												4
CONST_INT MAX_NUM_LAND_VEHICLES                                       	4
CONST_INT END_OF_WAVE_XP_MULTIPLIER										50
CONST_INT MAX_SURVIVAL_START_POS										4		
CONST_INT MAX_OVERLAP_ENEMIES											10
CONST_INT MAX_NUM_ACTIVE_PEDS											(MAX_SURV_PEDS_IN_SQUAD*MAX_SURV_ENEMY_SQUADS) + (NUM_HELI_PEDS*MAX_SURV_HELI) + (NUM_LAND_VEH_PEDS*MAX_SURV_LAND_VEH) + MAX_OVERLAP_ENEMIES
CONST_INT MAX_NUM_HEAVIES_ON_STAGE										15

CONST_INT MAX_NUMBER_OF_KILLS_WITH_XP									100

CONST_INT MAX_NUM_ACTIVE_SQUAD_PEDS										30
CONST_INT MAX_NUM_MELEE_ENEMIES_PER_PLAYER								10


PLAYER_INDEX localPlayer
PED_INDEX localPlayerPed
BOOL bIsLocalPlayerHost
BOOL bLocalPlayerOK
BOOL bLocalPlayerPedOK
INT iLocalPart
INT iLocalPartInTeam = -1

INT iPartToUse
INT iSpectatorTarget
PLAYER_INDEX PlayerToUse
PED_INDEX PlayerPedToUse

INT iWaveToUse = 1
CONST_INT ciEndlessStage_ArmourRemoved	1
CONST_INT ciEndlessStage_Combat			2
CONST_INT ciEndlessStage_Enemies		3
CONST_INT ciEndlessStage_HealthBonus	4
CONST_INT ciEndlessStage_HealthRemoved	5
CONST_INT ciEndlessStage_Rotate			6
INT iEndlessRotateState
CONST_INT ciEndlessRotateState_Combat	0
CONST_INT ciEndlessRotateState_Enemies	1
CONST_INT ciEndlessRotateState_Health	2

SCRIPT_TIMER tdNetTimer  				//  The network timer
SCRIPT_TIMER stLeaveHordeTimer
SCRIPT_TIMER stTaxtTerminateFailsafe
SCRIPT_TIMER stJipTimer
SCRIPT_TIMER stEndOfWaveDelay
SCRIPT_TIMER stSurvivalTime

//AI Vars
INT iStaggeredPedBrain[NUM_SURVIVAL_SUB_STAGES][MAX_SURV_ENEMY_SQUADS]
INT iStaggeredPedBody[NUM_SURVIVAL_SUB_STAGES][MAX_SURV_ENEMY_SQUADS]

INT iStaggeredProp
CONST_INT ciMAX_UFO_LIGHTS 5
CONST_INT ciMAX_UFO_SOUNDS 20
SCRIPT_TIMER tdUFOLights[ciMAX_UFO_LIGHTS]
INT iUFOIndex[ciMAX_UFO_LIGHTS]
INT iUFOSoundIndex[FMMC_MAX_NUM_PROPS]	//Prop number
INT iUFOSoundID[ciMAX_UFO_SOUNDS]		//Sound ID
INT iUFOSoundBS
CONST_INT ciMAX_UFO_RINGS 3
ENUM eUFOIntroState
	eUFO_INIT,
	eUFO_PROCESSING,
	eUFO_CLEANUP,
	eUFO_FINISHED
ENDENUM
STRUCT STRUCT_UFO
	FLOAT fRadius
	SCRIPT_TIMER tdUFOIntro
	SCRIPT_TIMER tdRadiusShrinkTimer
	SCRIPT_TIMER tdBeamDownTimer
	SCRIPT_TIMER tdRingTimer[ciMAX_UFO_RINGS]
	SCRIPT_TIMER tdFadeIn
	INT iR, iG, iB, iA
	eUFOIntroState eUFOState
ENDSTRUCT
STRUCT_UFO eBeamDownUFO
CONST_FLOAT cfUFO_INTRO 9000.0
CONST_FLOAT cfUFO_BEAM	5000.0
CONST_FLOAT cfUFO_FADE	1000.0
HUD_COLOURS hcUFOLight = HUD_COLOUR_ORANGE



LBD_VARS lbdVars
SAVE_OUT_UGC_PLAYER_DATA_VARS sSaveOutVars 
INT iLBPlayerSelection = -1
TIME_DATATYPE timeScrollDelay 

JOB_INTRO_CUT_DATA jobIntroData

//Local Bool Checks
CONST_INT ciMaxLocalBools 1
INT iLocalBoolArray[ciMaxLocalBools]

CONST_INT ciLocalBool_bStartFadeInTriggered 			0
CONST_INT ciLocalBool_bAllPlayersLostAllLivesFlag		1
CONST_INT ciLocalBool_bClearedObjectiveTextForRewards	2
CONST_INT ciLocalBool_bGotWaveProgressLabel				3
CONST_INT ciLocalBool_bGivenEndModeRewards				4
CONST_INT ciLocalBool_bGivenEndOfWaveXp					5
CONST_INT ciLocalBool_bGivenTakingPartXp				6
CONST_INT ciLocalBool_bIamQuittingCoronaSpectator		7
CONST_INT ciLocalBool_bDealtWithWasted					8
CONST_INT ciLocalBool_bModeEndedSpecCamTranisition		9
CONST_INT ciLocalBool_bIncrementedTestedWaveCounter		10
CONST_INT ciLocalBool_bRespawningBetweenWaves			11
CONST_INT ciLocalBool_bCleanedUpEnemies					12
CONST_INT ciLocalBool_bInvalidWaveForKillRP				13
CONST_INT ciLocalBool_bLocalWaveDataSetUp				14
CONST_INT ciLocalBool_bEndlessShardDone					15
CONST_INT ciLocalBool_bAllPedSpawnersDone				16
CONST_INT ciLocalBool_bAllVehicleSpawnersDone			17
CONST_INT ciLocalBool_bBeamedIn							18
CONST_INT ciLocalBool_bhasAshPFX						19
CONST_INT ciLocalBool_bResetHUDColours					20
CONST_INT ciLocalBool_bShowCarrierMap					21
CONST_INT ciLocalBool_bBigMapWasZoomedCarrier			22
CONST_INT ciLocalBool_bBigMapDoZoomCarrier				23
CONST_INT ciLocalBool_bCarrerLightsPreloaded			24
CONST_INT ciLocalBool_bDeadAtEnd						25
CONST_INT ciLocalBool_bBeamParticle						26
CONST_INT ciLocalBool_bUFOIntroAnimStarted				27
CONST_INT ciLocalBool_bAppearanceUpdated				28
CONST_INT ciLocalBool_bInTest							29
CONST_INT ciLocalBool_UFOSpawnSound						30
//BOOL bStartFadeInTriggered
//BOOL bAllPlayersLostAllLivesFlag
//BOOL bClearedObjectiveTextForRewards
//BOOL bGotWaveProgressLabel
//BOOL bGivenEndModeRewards
//BOOL bGivenEndOfWaveXp
//BOOL bGivenTakingPartXp
//BOOL bIamQuittingCoronaSpectator
//BOOL bDealtWithWasted
//BOOL bModeEndedSpecCamTranisition
BOOL bSkipTaxiScriptCheck
BOOL bIWentFromSpectatingToPassedWaveStage
BOOL bTurnedOffSpecCam
BOOL bDoWaveCompleteHudPrints
BOOL bProcessedMostSurvivalKillsAward
BOOL bSavedMatchstartPlayStatsData
BOOL bGotFirstTimeForHoldValue
BOOL bBeenInPauseMenuTooLongOnWavePassed
BOOL bDisplayedNoVehsHelp
BOOL bAllAwardsBeenShown
BOOL bDidMatchEnd
BOOL bSetupInitialSpawnPos
BOOL bResetReminderTimer
BOOL bFinalCelebrationScreenHasDisplayed
BOOL bSetupSpecificSpawnCoords
BOOL bMutedNearbyRadios
BOOL bContinueToCut
BOOL bAlreadyTriggeredCelebrationPostFX
BOOL bPrintPartListInfo
BOOL bSetMissionOverAndOnLbForSctv
BOOL bSavedOutPlayerUgcData
BOOL bSetSctvTickerOn
BOOL bTurnedOffRoadNodes
BOOL bDoneMatchEnd
BOOL bDoUnhide
BOOL bIHaveDamagedAnEnemy
BOOL bSetBlockingAreas
BOOL bOkToDoOutro
BOOL bDoneOutro
BOOL bDidJipSpecFade
BOOL bNewJobScreens
BOOL bMoveToEnd


INT iStaggeredPickUpBlipCount
INT iStaggeredAlldidEndOfWaveHudCount
INT iRespawningStage
INT iTrackDeathsStage
INT iCountDownAudioStage
INT iWarpToStartPosStage
INT iCreatePickupsStage
INT iEndOfWaveHudStage
INT iTimeInPauseMenu
INT iProcessPickupBlipsStage
INT iWeaponPickupCount
INT iFeedbackStage
INT iTotalWaveAwardXp
INT iStartHits
INT iStartShots
INT iPlayerAbandondedSurvivalTickerBit
INT iEndCelebrationScreenScore
INT iPlayerIsNetOkBitset
INT iParticpantIsDeadBitset
INT iGiveWeaponsStage

SCRIPT_TIMER tdWeaponTimer

FLOAT fFurthestTargetDist = 50.0
FLOAT fOldFurthestTargetDist = 50.0
FLOAT fPartDis
FLOAT fHeightMapHeight = -1.0

INT iLocalZoneBS
INT iNavBlockerZone[FMMC_MAX_NUM_ZONES]
	
#IF IS_DEBUG_BUILD
INT iStaggeredPartCountDebug
#ENDIF

TIME_DATATYPE iTimeForHold

TEXT_LABEL_15 tl15_waveProgressLabel

AI_BLIP_STRUCT structSquadEnemyBlip[MAX_NUM_ENEMY_SQUADS][MAX_SURV_PEDS_IN_SQUAD][NUM_SURVIVAL_SUB_STAGES]
AI_BLIP_STRUCT structLandVehicleEnemyBlip[MAX_NUM_LAND_VEHICLES][NUM_LAND_VEH_PEDS]
BLIP_INDEX biPickupBlip[ciMAX_CREATED_WEAPONS]
BLIP_INDEX biShootOutBlip
BLIP_INDEX biHeliBlip[MAX_NUM_SURVIVAL_HELIS]
OBJECT_INDEX oihordeProp[FMMC_MAX_NUM_PROPS]
OBJECT_INDEX oihordePropChildren[FMMC_MAX_NUM_PROP_CHILDREN]
PICKUP_INDEX piPickup[ciMAX_CREATED_WEAPONS]
INT iPickupParticleBitSet
INT iPickupSoundBitSet
INT iPickupOneShotSoundBitSet

BLIP_INDEX biLastEnemy
ENTITY_INDEX eiLastEnemy

BLIP_INDEX biPlayArea
FLOAT fPlayAreaAlpha
INT iPlayAreaChange

//Sounds
INT iBoundsTimerSound = GET_SOUND_ID()
INT iPickupSounds[ciMAX_CREATED_WEAPONS]
INT iCountDownSound = GET_SOUND_ID()

BLIMP_SIGN sBlimpSign

SCENARIO_BLOCKING_INDEX sbiScenarioBlockingIndex

INT iDoorID[FMMC_MAX_NUM_DOORS]

//Celebration Screen
CELEBRATION_SCREEN_DATA sCelebrationData
CAMERA_INDEX camCelebrationScreen

SCRIPT_TIMER jobUnderTenSecondsTimer

INT iWorldPropIterator
LEGACY_RUNTIME_WORLD_PROP_DATA sRuntimeWorldPropData

ENUM eHORDE_STAGE
    eHORDESTAGE_JIP_IN = 0,
    eHORDESTAGE_SETUP_MISSION,
    eHORDESTAGE_START_FADE_IN,
    eHORDESTAGE_FIGHTING,
    eHORDESTAGE_PASSED_WAVE,
    eHORDESTAGE_DELAY_BEFORE_NEXT_WAVE,
    eHORDESTAGE_SPECTATING,
    eHORDESTAGE_SCTV,
    eHORDESTAGE_REWARDS_AND_FEEDACK
ENDENUM

ENUM eEND_HORDE_REASON
    eENDHORDEREASON_NOT_FINISHED_YET = 0,
    eENDHORDEREASON_BEAT_ALL_WAVES,
    eENDHORDEREASON_ALL_PLAYERS_DEAD
ENDENUM

ENUM eENEMY_STATE
    eENEMYSTATE_DORMANT = 0,
    eENEMYSTATE_SPAWNING,
    eENEMYSTATE_FIGHTING,
    eENEMYSTATE_DEAD
ENDENUM

ENUM eKILL_STREAK_STATE
    eKILLSTREAKSTATE_NONE = 0,
    eKILLSTREAKSTATE_SMALL,
    eKILLSTREAKSTATE_MEDIUM,
    eKILLSTREAKSTATE_LARGE
ENDENUM

ENUM eWAVE_STAGE
    eWAVESTAGE_EASY = 0,
    eWAVESTAGE_MEDIUM,
    eWAVESTAGE_HARD
ENDENUM

ENUM eDELAY_BEFORE_NEXT_WAVE_STAGE
    eDELAYBEFORENEXTWAVESTAGE_ADD_NEW_FIGHTING_PARTICIPANTS = 0,
    eDELAYBEFORENEXTWAVESTAGE_WAIT_FOR_ALL_FIGHTERS_TO_FADE_IN,
    eDELAYBEFORENEXTWAVESTAGE_DO_COUNT_DOWN
ENDENUM

ENUM eMINI_LEADER_BOARD_STATE
    eMINILEADERBOARDSTATE_OFF = 0,
    eMINILEADERBOARDSTATE_DO_FIRST_SORT,
    eMINILEADERBOARDSTATE_ON
ENDENUM

ENUM eSURVIVAL_HELI_STATE
    eSURVIVALHELISTATE_NOT_EXIST = 0,
    eSURVIVALHELISTATE_SPAWNING,
	eSURVIVALHELISTATE_FLYING_TO_SURVIVAL,
    eSURVIVALHELISTATE_FLYING_FIGHTING,
	eSURVIVALHELISTATE_MAKE_SURE_BLOWN_UP,
    eSURVIVALHELISTATE_DEAD
ENDENUM

ENUM ENUM_MUSIC_EVENT
	eMUSICEVENT_START = 0,
	eMUSICEVENT_WAVE_40P,
	eMUSICEVENT_WAVE_70P,
	eMUSICEVENT_WAVE_COMPLETE,
	eMUSICEVENT_STOP
ENDENUM

CONST_INT NUM_MISSION_MUSIC_EVENTS COUNT_OF(ENUM_MUSIC_EVENT)

ENUM eSQUAD_STATE
	eSQUADSTATE_DORMANT = 0,
	eSQUADSTATE_ACTIVE
ENDENUM

ENUM eVEHICLE_PED_STATE
	eVEHICLEPEDSTATE_DORMANT = 0,
	eVEHICLEPEDSTATE_SPAWNING,
	eVEHICLEPEDSTATE_DRIVING_TO_SURVIVAL,
	eVEHICLEPEDSTATE_FIGHTING,
	eVEHICLEPEDSTATE_DEAD
ENDENUM

ENUM eSURVIVAL_LAND_VEHICLE_STATE
	eSURVIVALLANDVEHICLESTATE_DOES_NOT_EXIST = 0,
	eSURVIVALLANDVEHICLESTATE_SPAWNING,
	eSURVIVALLANDVEHICLESTATE_DRIVING,
	eSURVIVALLANDVEHICLESTATE_ARRIVED,
	eSURVIVALLANDVEHICLESTATE_DEAD
ENDENUM

ENUM eMIGHT_NOT_BE_PLAYING_FLAGS
	eMIGHTNOTBEPLAYINGFLAGS_NOT_DAMAGED_ENEMY = 0,
	eMIGHTNOTBEPLAYINGFLAGS_IDLE
ENDENUM

STRUCT STRUCT_MUSIC_EVENT
	ENUM_MUSIC_EVENT eEvent
	TEXT_LABEL_31 tl31Name
	BOOL bTriggered
ENDSTRUCT

STRUCT STRUCT_SURVIVAL_VEHICLE_PED
    NETWORK_INDEX netId
    MODEL_NAMES eModel = MP_S_M_Armoured_01
	eVEHICLE_PED_STATE eState
	BOOL bDied
	BOOL bMelee
	WEAPON_TYPE wtWeapon
ENDSTRUCT

STRUCT STRUCT_SURVIVAL_HELI_DATA
    NETWORK_INDEX netId
    MODEL_NAMES eModel = BUZZARD
    eSURVIVAL_HELI_STATE eState
    STRUCT_SURVIVAL_VEHICLE_PED sPed[NUM_HELI_PEDS]
    VECTOR vSpawnCoords
    FLOAT fHeading
    BOOL bActive
    INT iCircCount
    INT iActivatingStag
	SCRIPT_TIMER stBlownUpTimer
	PED_INDEX piLastDamager
ENDSTRUCT

STRUCT STRUCT_SURVIVAL_LAND_VEHICLE_DATA
	NETWORK_INDEX netId
	MODEL_NAMES eModel = SANDKING2
	eSURVIVAL_LAND_VEHICLE_STATE eState
	STRUCT_SURVIVAL_VEHICLE_PED sPed[NUM_LAND_VEH_PEDS]
	VECTOR vSpawnCoords
    FLOAT fHeading
	INT iActivatingStage
	INT iNumPeds = -1
	BOOL bSpawned
	BOOL bEmpty
ENDSTRUCT

STRUCT STRUCT_SQUAD_ID_DATA
	INT iSquad = -1
	INT iSquadSlot
ENDSTRUCT

STRUCT STRUCT_ENEMY_PED_HORDE_DATA
	NETWORK_INDEX netId
    eENEMY_STATE eState = eENEMYSTATE_DORMANT
    STRUCT_SQUAD_ID_DATA sSquadIdData
    INT iAccuracy = 10
	VECTOR vCurrentSpawnCoords
	BOOL bPlayedAngryShout
	INT iEnemyTrackKillsStage
	BOOL bHeavy
	BOOL bMelee
ENDSTRUCT

// Squad struct. Contains data about a squad.
STRUCT STRUCT_AI_SQUAD
	eSQUAD_STATE eState
	STRUCT_ENEMY_PED_HORDE_DATA sPed[MAX_SURV_PEDS_IN_SQUAD]
	VECTOR vSquadSpawnPos
	INT iSquadSpawnLocation
ENDSTRUCT

STRUCT STRUCT_HEAVY_PED_DATA
	INT iHeavyPeds[NUM_SURVIVAL_SUB_STAGES][MAX_NUM_ENEMY_SQUADS][MAX_SURV_PEDS_IN_SQUAD]
	INT iNumHeavies[NUM_SURVIVAL_SUB_STAGES]
ENDSTRUCT

STRUCT STRUCT_WAVE_DATA
    INT iWaveCount = 1
	INT iEndlessWave
    eWAVE_STAGE eStage
    INT iNumRequiredKills
    INT iNumRequiredSubstageKills[NUM_SURVIVAL_SUB_STAGES]
    INT iNumPedsInSquad[NUM_SURVIVAL_SUB_STAGES]
    INT iNumSquads[NUM_SURVIVAL_SUB_STAGES]
	INT iTotalSquads
    INT iMaintainSquadsStage
	INT iNumLandVehicles
	INT iLVehSpawnLocation[MAX_SURV_LAND_VEH]
	VECTOR vSquadSpawnPos[MAX_SURV_ENEMY_SPAWNERS]
	VECTOR vLVehSpawnPos[MAX_SURV_VEH_SPAWNERS]
	eWAVE_STAGE eHighestSubstage
	INT iSpawnChangeAttempted[NUM_SURVIVAL_SUB_STAGES]
ENDSTRUCT

STRUCT STRUCT_HORDE_PARTICIPANT_DATA
    INT iParticipant[MAX_NUMBER_HORDE_FIGHTING_PARTICIPANTS]
    INT iNumFightingParticipants
	int iNumPotentialfightingParticipants
	INT iNumFightingParticipantsBeginningOfWave
    INT iAddedBitset
	INT iPotentialFighterBitset
ENDSTRUCT

STRUCT STRUCT_HORDE_BOARD_DATA
    BOOL bPopulatedHudPlayerArray
    PLAYER_INDEX hudPlayers[4]
    INT iScores[4]
	INT iTeam[4]
	INT iDeaths[4]
	INT iHeadshots[4]
ENDSTRUCT
STRUCT_HORDE_BOARD_DATA sEndBoardData

STRUCT STRUCT_KILL_STREAK_DATA
    eKILL_STREAK_STATE eState
    INT iNumKillsInARow
    INT iBigMessageBitset
ENDSTRUCT
STRUCT_KILL_STREAK_DATA structKillStreakData

STRUCT STRUCT_DELAY_BEFORE_NEXT_WAVE_DATA
    eDELAY_BEFORE_NEXT_WAVE_STAGE eStage
ENDSTRUCT

STRUCT STRUCT_MINI_LEADERBOARD_DATA
    eMINI_LEADER_BOARD_STATE eMiniLeaderBoardState
    LEADERBOARD_PLACEMENT_TOOLS sPlacement
    THE_LEADERBOARD_STRUCT miniLeaderBoardStruct[MAX_NUMBER_HORDE_FIGHTING_PARTICIPANTS]
    STRUCT_HORDE_BOARD_DATA sMiniLeaderBoardSortedData
	DPAD_VARS sDpadVars
	SCALEFORM_INDEX siMovie
ENDSTRUCT
STRUCT STRUCT_ALLOW_WASED_MESSAGE_DATA
    INT iWastedMessageStage
    SCRIPT_TIMER stWastedDisplayTime
ENDSTRUCT

STRUCT STRUCT_SURVIVAL_BOUNDS_DATA
	
	VECTOR vMin
	VECTOR vMax
	FLOAT fWidth
	VECTOR vBase
	
ENDSTRUCT

STRUCT_SURVIVAL_BOUNDS_DATA 			sSurvivalBoundsData
STRUCT_ALLOW_WASED_MESSAGE_DATA 		sAllowWastedMessageData
STRUCT_MINI_LEADERBOARD_DATA 			eMiniLeaderBoardData
STRUCT_MUSIC_EVENT 						structMusicEvent[NUM_MISSION_MUSIC_EVENTS]
FMMC_EOM_DETAILS 						sEndOfMission
PLAYER_CURRENT_STATUS_COUNTS 			sPlayerCounts
SC_LEADERBOARD_CONTROL_STRUCT 			scLB_control//, scLB_controlFriendComp

SCALEFORM_INDEX							scLB_scaleformID

THE_LEADERBOARD_STRUCT 				HordeLeaderboard[NUM_NETWORK_PLAYERS]
LEADERBOARD_PLACEMENT_TOOLS 			LeaderboardPlacement
TEXT_LABEL_63 							tl63LeaderboardPlayerNames[NUM_NETWORK_PLAYERS]
INT iSavedRow[NUM_NETWORK_PLAYERS]
INT iSavedSpectatorRow[NUM_NETWORK_PLAYERS]
INT iSpectatorBit

// Might not be playing vars.
SCRIPT_TIMER stIdleTimer
SCRIPT_TIMER stNotDamagedEnemyTimer
CONST_INT MIGHT_NOT_BE_PLAYING_TIMER_LIMIT 2*60*1000
INT iPrintedLeecherRemindeTickerForfightingParticipant
INT iHeliAttackRangePartCount

STRUCT SURVIVAL_INTERIOR_DATA
	INTERIOR_INSTANCE_INDEX iInteriorIndex
	INTERIOR_DATA_STRUCT structInteriorData	
	INTERIOR_NAME_ENUM interiorName = INTERIOR_MAX_NUM
ENDSTRUCT
SURVIVAL_INTERIOR_DATA sInterior

//STRUCT_REL_GROUP_HASH sRGH
STRUCT ENTITY_PROOFS_STRUCT
	BOOL bBulletProof
	BOOL bFireProof
	BOOL bExplosionProof
	BOOL bCollisionProof
	BOOL bMeleeProof
	BOOL bSteamProof
	BOOL bDontResetDamageFlags
	BOOL bSmokeProof
ENDSTRUCT

// *********************************** \\
// *********************************** \\
//                                     \\
//				SERVER BD              \\
//                                     \\
// *********************************** \\
// *********************************** \\

ENUM ENDLESS_WAVE_MOD_TYPE
	eENDLESS_ARMOUR_REMOVAL,
	eENDLESS_HEALTH_REMOVAL,
	eENDLESS_COMBAT_INCREASE,
	eENDLESS_ENEMY_INCREASE,
	eENDLESS_HEALTH_INCREASE,
	eENDLESS_HEAVY_INCREASE
ENDENUM


STRUCT ENDLESS_BONUS
	INT iHealth[MAX_SURV_WAVE_DIFF]
	INT iArmour[MAX_SURV_WAVE_DIFF]
	INT iEnemiesInSquad[MAX_SURV_WAVE_DIFF]
	INT iSquads[MAX_SURV_WAVE_DIFF]
	INT iAccuracy[MAX_SURV_WAVE_DIFF]
	INT iHeavies[MAX_SURV_WAVE_DIFF]
	ENDLESS_WAVE_MOD_TYPE eModType 
ENDSTRUCT

STRUCT ServerBroadcastData
    
    INT iServerGameState
   	
    SCRIPT_TIMER TerminationTimer
    SCRIPT_TIMER stNextWaveDelay
    SCRIPT_TIMER stNextSubStageTimer
	SCRIPT_TIMER tdWaveLength
	SCRIPT_TIMER tdSubStageLength[NUM_SURVIVAL_SUB_STAGES]
	SCRIPT_TIMER tdSurvivalLength
    eHORDE_STAGE eStage
    eEND_HORDE_REASON eEndReason
	
    FMMC_SERVER_DATA_STRUCT sFMMC_SBD
    
    STRUCT_WAVE_DATA sWaveData
    STRUCT_HORDE_PARTICIPANT_DATA sFightingParticipantsList
    eDELAY_BEFORE_NEXT_WAVE_STAGE sDelayBeforeNextWaveStageData
    SERVER_EOM_VARS sServerFMMC_EOM
	
	INT iNumberOfWaves = NUMBER_HORDE_WAVES
	
    INT iKills
    INT iKillsThisWave
	INT iKillsThisWaveSubStage[NUM_SURVIVAL_SUB_STAGES]
	INT iNumCreatedEnemies
	INT iNumCreatedEnemiesThisStage[NUM_SURVIVAL_SUB_STAGES]
	
	INT iSearchingForPed = -1
	INT iSearchingForChopper = -1
	INT iSearchingForLandVehicle = -1
	INT iSquadSpawningStage
	
    INT iBitset
    
    INT iNumAliveSquadEnemies
	INT iNumberOfMeleeEnemies
    INT iNumAliveHelis
    INT iNumAliveLandVehiclePeds
	INT iNumAliveTotalEnemies
	INT iNumVehiclesCreatedThisStage[NUM_SURVIVAL_SUB_STAGES]
	
	INT iMatchHistoryId[2]
		
	INT iNumKillsThisWaveFromEvents
	INT iNumKillsThisSubStageFromEvents[NUM_SURVIVAL_SUB_STAGES]
	
    #IF IS_DEBUG_BUILD
    INT iSfSkipParticipant = -1
    BOOL bSkipWave
    #ENDIF
	
	VECTOR vStartPos[MAX_SURVIVAL_START_POS]
	FLOAT fStartHeading[MAX_SURVIVAL_START_POS]
    
	VECTOR vBoundsMiddle
	
	INT iWeather
	INT iTimeOfDay
	
	ENDLESS_BONUS sEndlessBonuses
	INT iPopulationHandle = -1
	INT iNumberOfStartingPlayers
ENDSTRUCT
ServerBroadcastData serverBD

STRUCT Squad_Enemy_ServerBroadcastData
	STRUCT_AI_SQUAD structAiSquad[MAX_NUM_ENEMY_SQUADS][NUM_SURVIVAL_SUB_STAGES]
	STRUCT_HEAVY_PED_DATA sHeavyData
ENDSTRUCT
Squad_Enemy_ServerBroadcastData serverBD_Enemies_S

STRUCT Veh_Enemy_ServerBroadcastData
	STRUCT_SURVIVAL_HELI_DATA sSurvivalHeli[MAX_NUM_SURVIVAL_HELIS]
	STRUCT_SURVIVAL_LAND_VEHICLE_DATA sSurvivalLandVehicle[MAX_NUM_LAND_VEHICLES]
	
	SCRIPT_TIMER stVehicleNotInAreaTimer
	SCRIPT_TIMER stBailFromTurretTimer[MAX_SURV_LAND_VEH]
	SCRIPT_TIMER stNoSpawningEnemiesTimer
ENDSTRUCT
Veh_Enemy_ServerBroadcastData serverBD_Enemies_V

CONST_INT BITSET_SERVER_CURRENT_WAVE_DEFEATED                           0
CONST_INT BITSET_SERVER_DID_FIRST_WAVE_SETUP                            1
CONST_INT BITSET_SERVER_NEW_PLAYERS_GO_STRAIGHT_TO_SPEC_CAM     		2
CONST_INT BITSET_SERVER_READY_FOR_DELAY_COUNTDOWN                       3
CONST_INT BITSET_SERVER_ENDLESS_WAVES_MODE								4
CONST_INT BITSET_SERVER_VEHICLE_SPAWNED_THIS_FRAME	                    5
CONST_INT BITSET_SERVER_ENDLESS_STOP_ARMOUR								6
CONST_INT BITSET_SERVER_ENDLESS_STOP_HEALTH								7
CONST_INT BITSET_SERVER_ENDLESS_STOP_ENEMY_INCREASE						8
CONST_INT BITSET_SERVER_ENDLESS_WAVE_EXTRAS_DONE						9
CONST_INT BITSET_SERVER_RUN_LAST_STAGE_TIMER							10
CONST_INT BITSET_SERVER_ALL_VEHICLE_PEDS_STUCK_OOB						11
CONST_INT BITSET_SERVER_START_VEHICLE_FAIL_SAFE_TIMER					12
CONST_INT BITSET_SERVER_FIGHTING_PART_LIST_INITIALISED					13
CONST_INT BITSET_SERVER_HALLOWEEN_MODE									14

// Dpad refresh
INT iStoredRefreshDpadValue 
INT iRefreshDpad 

NEXT_JOB_STRUCT nextJobStruct

STRUCT PLAYER_DATA_STRUCT
	INT iPart
	INT iPartInTeam
	BOOL bPlayerOK
	PED_INDEX piPlayerPedID
	PLAYER_INDEX piPlayerID
ENDSTRUCT

// *********************************** \\
// *********************************** \\
//                                     \\
// 				PLAYER BD              \\
//                                     \\
// *********************************** \\
// *********************************** \\

ENUM eSPECTATOR_STATE
	ci_SpectatorState_IDLE = 0,
	ci_SpectatorState_WAIT,
	ci_SpectatorState_INIT,
	ci_SpectatorState_SET_TO_SPECTATOR,
	ci_SpectatorState_MAINTAIN,
	ci_SpectatorState_REJOINING,
	ci_SpectatorState_CLEANUP,
	ci_SpectatorState_END
ENDENUM

STRUCT PlayerBroadcastData
        
    INT iGameState
    INT iBitset
    
    INT iMyKills
    INT iMyKillsThisWave
	INT iMyVehicleKills
    INT iDeaths
	INT iDeathsThisWave
	INT iSurvivalScore = -1
	INT iMyHeadshots
	INT iXpGained
	INT iKillsWithXP
	
    eHORDE_STAGE eStage
    
	BOOL bCompletedPassedSurvivalCutscene
	
	INT iMightNotBePlayingBitset
	
	PLAYER_DATA_STRUCT sPlayerData
	
    #IF IS_DEBUG_BUILD
        BOOL bUsedFSkip
        BOOL bUsedSSkip
        BOOL bSkipWave
    #ENDIF
    
	eSPECTATOR_STATE eSpectatorState
ENDSTRUCT
PlayerBroadcastData playerBD[NUM_NETWORK_PLAYERS]

CONST_INT BITSET_PLAYER_I_HAVE_DEALT_WITH_FEEDBACK                      0
CONST_INT BITSET_PLAYER_READY_FOR_START_FADE_IN                         1
CONST_INT BITSET_PLAYER_I_HAVE_COMPLETED_END_OF_WAVE_HUD        		2
CONST_INT BITSET_PLAYER_COMPLETED_START_FADE_IN                         3
CONST_INT BITSET_PLAYER_ABANDONED_HORDE                               	4
CONST_INT BITSET_PLAYER_I_WENT_STRAIGHT_TO_SPEC_CAM                     5
CONST_INT BITSET_PLAYER_I_AM_ALIVE_AND_FADED_IN                         6
CONST_INT BITSET_PLAYER_DID_INITIAL_FIGHTER_STATUS_SETUP            	7
CONST_INT BITSET_PLAYER_WENT_TO_SPEC_CAM_FOR_OUT_OF_BOUNDS				8
CONST_INT BITSET_PLAYER_INVINCIBILITY_BETWEEN_WAVES						9

CONST_INT ci_SpectatorBS_Quitting		0
CONST_INT ci_SpectatorBS_Ending			1
CONST_INT ci_SpectatorBS_RequiresInit	2
CONST_INT ci_SpectatorBS_Warped			3
CONST_INT ci_SpectatorBS_QuitFadedOut	4
INT iSpectatorBitset
SCRIPT_TIMER tdSpectatorStartTimer

PICKUP_TYPE ptWaveLoadout[FMMC_MAX_WEAPONS]

INT iManageSpechost = 0
INT iDesiredHost = -1

CONST_FLOAT cf_PedSpawnRadius 4.0

INT iNavmeshBlocker = -1

//Spawn Test
INT iPedSpawnNumber
INT iPedSpawnTested
INT iVehicleSpawnNumber
INT iVehicleSpawnTested

WEAPON_TYPE wtLastWeapon

//Debug only
#IF IS_DEBUG_BUILD

BOOL bJSkipped
BOOL bDoingWarpToHorde
BOOL bForceWavePassedOffset
//BOOL bNoBlipsOutput
INT iCreateWidgetsStage
BOOL bCreateWidgets
BOOL bDoMiniBoardDebugPrints
BOOL bDoRespawnMissionPedPrints
BOOL bDoProgressBarPrints
WIDGET_GROUP_ID survivalWidgetsId
BOOL bWaveStageDataWidgets
BOOL bDoEndWaveWidgets
BOOL bBlockDistanceCheck
BOOL bPutEnemiesOutsideHordeArea
BOOL bPedSpawningBug
BOOL bForceEnemyBlips
VECTOR vHeliOffset
VECTOR vCamRotation
FLOAT fCamFov
BOOL bPauseSceneOnShot3
INT iEndOfWaveXp[ABSOLUTE_MAX_HORDE_WAVES]
INT iKillsXp[ABSOLUTE_MAX_HORDE_WAVES]
INT iVehsDestroyedXp[ABSOLUTE_MAX_HORDE_WAVES]
INT iTakePartXp
BOOL bDebugBlipAllAi
BOOL bDebugUnblipAllAi = TRUE
INT iMyPartId
BOOL bForceMakeSureBlownUpState[MAX_NUM_SURVIVAL_HELIS]
INT iOverheadOverrideState
BOOL bUpdateOverheadOverride
BOOL bDoAllCompletedWavePrints

BLIP_INDEX biDebugFootEnemyBlip[MAX_NUM_ENEMY_SQUADS][MAX_SURV_PEDS_IN_SQUAD]
BLIP_INDEX biDebugHeliBlip[MAX_NUM_SURVIVAL_HELIS]
BLIP_INDEX biDebugHeliEnemyBlip[MAX_NUM_SURVIVAL_HELIS][NUM_HELI_PEDS]
BLIP_INDEX biDebugLandVehBlip[MAX_NUM_LAND_VEHICLES]
BLIP_INDEX biDebugLandVehEnemyBlip[MAX_NUM_LAND_VEHICLES][NUM_LAND_VEH_PEDS]

// Horde widget variables.
TEXT_WIDGET_ID twID_HordeStageServer
TEXT_WIDGET_ID twID_HordeStageClient
TEXT_WIDGET_ID twID_endReason
TEXT_WIDGET_ID twID_gameStateServer
TEXT_WIDGET_ID twID_gameStateClient
TEXT_WIDGET_ID twID_killStreakState
TEXT_WIDGET_ID twID_WaveSubStage
TEXT_WIDGET_ID twID_NextWaveDelayStage
TEXT_WIDGET_ID twID_minBoardState
TEXT_WIDGET_ID twID_heliState[MAX_NUM_SURVIVAL_HELIS]
TEXT_WIDGET_ID twID_landVehicleState[MAX_NUM_LAND_VEHICLES]
BOOL bActivateOverlapPeds

BLIP_INDEX biEnemy[MAX_NUM_ENEMY_SQUADS][MAX_SURV_PEDS_IN_SQUAD][NUM_SURVIVAL_SUB_STAGES]

BOOL bCurrentWaveDefeated
BOOL bDidFirstWaveSetup
BOOL bDealtWithFeedback
BOOL bReadyForstartFadeIn
BOOL bCompletedEndWaveHud
BOOL bForceBlipAllEnemies
BOOL bForceBlippedEnemies

INT iHudPlayers[4]
INT iWaveStageKillLimit

BOOL bDo893474Prints
BOOL bShowOnScreenDebug = TRUE
INT iDebugRow

INT iWarpPed
INT iWarpSquad

BOOL bSimulateScaled
TEXT_LABEL_23 tlCurrentMusic

#ENDIF
