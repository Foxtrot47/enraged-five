USING "FM_Survival_Controller_Using.sch"
USING "FM_Survival_Controller_Utility.sch"
USING "FM_Survival_Controller_Waves.sch"

#IF IS_DEBUG_BUILD

PROC ADD_XP_BREAKDOWN_WIDGETS()
	
	INT i
	TEXT_LABEL_63 tlTemp
	
	tlTemp = "Take Part Xp"
	ADD_WIDGET_INT_READ_ONLY(tlTemp, iTakePartXp)
		
	REPEAT serverBD.iNumberOfWaves i 
		
		tlTemp = "End of wave "
		tlTemp += i+1
		tlTemp += " XP"
		ADD_WIDGET_INT_READ_ONLY(tlTemp, iEndOfWaveXp[i])
		
		tlTemp = "Kill during wave "
		tlTemp += i+1
		tlTemp += " XP"
		ADD_WIDGET_INT_READ_ONLY(tlTemp, iKillsXp[i])
		
		tlTemp = "Vehs destroyed during wave "
		tlTemp += i+1
		tlTemp += " XP"
		ADD_WIDGET_INT_READ_ONLY(tlTemp, iVehsDestroyedXp[i])
		
	ENDREPEAT
	
ENDPROC

PROC ADD_SQUAD_AI_WIDGETS(STRUCT_AI_SQUAD &sSquadData, TEXT_WIDGET_ID &squadStateWidget)
	UNUSED_PARAMETER(sSquadData)
	START_WIDGET_GROUP("Squad AI")
		squadStateWidget = ADD_TEXT_WIDGET("State")
	STOP_WIDGET_GROUP()
	
ENDPROC

FUNC STRING GET_ENEMY_STATE_NAME(eENEMY_STATE eState)
    
    SWITCH eState
            CASE eENEMYSTATE_DORMANT                RETURN "DORMANT"
            CASE eENEMYSTATE_SPAWNING               RETURN "SPAWNING"
            CASE eENEMYSTATE_FIGHTING               RETURN "FIGHTING"
            CASE eENEMYSTATE_DEAD                   RETURN "DEAD"
    ENDSWITCH
    
    RETURN "NOT_IN_SWITCH"
    
ENDFUNC

FUNC STRING GET_KILL_STREAK_STATE_NAME(eKILL_STREAK_STATE eState)
    
    SWITCH eState
            CASE eKILLSTREAKSTATE_NONE              RETURN "NONE"
            CASE eKILLSTREAKSTATE_SMALL             RETURN "SMALL"
            CASE eKILLSTREAKSTATE_MEDIUM    		RETURN "MEDIUM"
            CASE eKILLSTREAKSTATE_LARGE             RETURN "LARGE"
    ENDSWITCH
    
    RETURN "NOT_IN_SWITCH"
    
ENDFUNC

FUNC STRING GET_WAVE_SUB_STAGE_NAME(eWAVE_STAGE eWaveStage)
    
    SWITCH eWaveStage
            CASE eWAVESTAGE_EASY RETURN "EASY"
            CASE eWAVESTAGE_MEDIUM RETURN "MEDIUM"
            CASE eWAVESTAGE_HARD RETURN "HARD"
    ENDSWITCH
    
    RETURN "NOT_IN_SWITCH"
    
ENDFUNC

FUNC STRING GET_DELAY_BEFORE_NEXT_WAVE_SUB_STAGE_NAME(eDELAY_BEFORE_NEXT_WAVE_STAGE eStage)
    
    SWITCH eStage
            CASE eDELAYBEFORENEXTWAVESTAGE_ADD_NEW_FIGHTING_PARTICIPANTS            RETURN "ADD_NEW_FIGHTING_PARTICIPANTS"
            CASE eDELAYBEFORENEXTWAVESTAGE_WAIT_FOR_ALL_FIGHTERS_TO_FADE_IN         RETURN "WAIT_FOR_ALL_FIGHTERS_TO_FADE_IN"
            CASE eDELAYBEFORENEXTWAVESTAGE_DO_COUNT_DOWN                            RETURN "DO_COUNT_DOWN" 
    ENDSWITCH
    
    RETURN "NOT_IN_SWITCH"
    
ENDFUNC

FUNC STRING GET_MINI_BOARD_STAGE_NAME(eMINI_LEADER_BOARD_STATE eState)
    
    SWITCH eState
            CASE eMINILEADERBOARDSTATE_OFF                          RETURN "OFF"
            CASE eMINILEADERBOARDSTATE_DO_FIRST_SORT        		RETURN "DO_FIRST_SORT"
            CASE eMINILEADERBOARDSTATE_ON                           RETURN "ON"     
    ENDSWITCH
    
    RETURN "NOT_IN_SWITCH"
    
ENDFUNC

FUNC STRING GET_SURVIVAL_HELI_STATE_NAME(eSURVIVAL_HELI_STATE eState)
    
    SWITCH eState
            CASE eSURVIVALHELISTATE_NOT_EXIST                       RETURN "NOT_EXIST"
            CASE eSURVIVALHELISTATE_SPAWNING                        RETURN "SPAWNING"
			CASE eSURVIVALHELISTATE_FLYING_TO_SURVIVAL				RETURN "FLYING_TO_SURVIVAL"
            CASE eSURVIVALHELISTATE_FLYING_FIGHTING                 RETURN "FLYING_FIGHTING"
			CASE eSURVIVALHELISTATE_MAKE_SURE_BLOWN_UP				RETURN "MAKE_SURE_BLOWN_UP"
            CASE eSURVIVALHELISTATE_DEAD                            RETURN "DEAD"
    ENDSWITCH
    
    RETURN "NOT_IN_SWITCH"
    
ENDFUNC

FUNC STRING GET_LAND_VEHICLE_STATE_NAME(eSURVIVAL_LAND_VEHICLE_STATE eState)
	
	SWITCH eState
		CASE eSURVIVALLANDVEHICLESTATE_DOES_NOT_EXIST	RETURN "DOES_NOT_EXIST"
		CASE eSURVIVALLANDVEHICLESTATE_SPAWNING			RETURN "SPAWNING"
		CASE eSURVIVALLANDVEHICLESTATE_DRIVING			RETURN "DRIVING"
		CASE eSURVIVALLANDVEHICLESTATE_ARRIVED			RETURN "ARRIVED"
		CASE eSURVIVALLANDVEHICLESTATE_DEAD				RETURN "DEAD"
	ENDSWITCH
	
	RETURN "NOT_IN_SWITCH"
	
ENDFUNC

PROC MAINTAIN_S_AND_F_SKIPS()

    IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iStaggeredPartCountDebug))
        
        IF playerBD[iStaggeredPartCountDebug].bUsedSSkip
            PRINTLN("[Survival] - participant ", iStaggeredPartCountDebug, "used S skip." )
            SET_END_HORDE_REASON(eENDHORDEREASON_BEAT_ALL_WAVES)
			serverBD.sServerFMMC_EOM.bAllowedToVote = TRUE
            SET_SERVER_HORDE_STAGE(eHORDESTAGE_REWARDS_AND_FEEDACK)
            serverBD.iSfSkipParticipant = iStaggeredPartCountDebug
            
        ELIF playerBD[iStaggeredPartCountDebug].bUsedFSkip
            PRINTLN("[Survival] - participant ", iStaggeredPartCountDebug, "used F skip." )
            SET_END_HORDE_REASON(eENDHORDEREASON_ALL_PLAYERS_DEAD)
			serverBD.sServerFMMC_EOM.bAllowedToVote = TRUE
            SET_SERVER_HORDE_STAGE(eHORDESTAGE_REWARDS_AND_FEEDACK)
            serverBD.iSfSkipParticipant = iStaggeredPartCountDebug
            
        ELIF playerBD[iStaggeredPartCountDebug].bSkipWave
            IF GET_SERVER_HORDE_STAGE() = eHORDESTAGE_FIGHTING
                serverBD.bSkipWave = TRUE
				PRINTLN("SKIPPING WAVE - ", serverBD.sWaveData.iWaveCount)
            ENDIF
        
        ENDIF
        
    ENDIF
    
    iStaggeredPartCountDebug++
    
    IF iStaggeredPartCountDebug >= NETWORK_GET_MAX_NUM_PARTICIPANTS()
        iStaggeredPartCountDebug = 0
    ENDIF
    
ENDPROC

PROC MAINTAIN_CLIENT_J_SKIPS()
    
    // S and F skipping for quick pass/fail, J skipping.
    #IF IS_DEBUG_BUILD
        IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
            IF NOT bJSkipped
                playerBD[iLocalPart].bUsedSSkip = TRUE
            ENDIF
        ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
            IF NOT bJSkipped
                playerBD[iLocalPart].bUsedFSkip = TRUE
            ENDIF
        ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
			IF NOT HAS_NET_TIMER_STARTED(serverBD.stNextWaveDelay)
          		bDoingWarpToHorde = TRUE
			ELSE
				serverBD.stNextWaveDelay.Timer = GET_TIME_OFFSET(serverBD.stNextWaveDelay.Timer, -GET_DELAY_BETWEEN_WAVES_LENGTH())
			ENDIF
        ELIF IS_DEBUG_KEY_JUST_PRESSED(KEY_J, KEYBOARD_MODIFIER_SHIFT, "Skip Wave")
            bJSkipped = TRUE        
        ENDIF
        IF NOT bJSkipped
            IF bDoingWarpToHorde
                IF NET_WARP_TO_COORD(serverBD.vStartPos[0], 0.0)
                    bDoingWarpToHorde = FALSE
                ENDIF 
            ENDIF
        ENDIF
        IF NOT bDoingWarpToHorde
            IF bJSkipped
                playerBD[iLocalPart].bSkipWave = TRUE
                bJSkipped = FALSE
            ENDIF
        ENDIF
    #ENDIF
    
ENDPROC

PROC REMOVE_PREVIOUS_WAVE_ENTITIES_DEBUG()

	INT i, j, iStage
	BOOL bAllCleared = TRUE

	IF serverBD.bSkipWave
	    
		FOR iStage = 0 TO NUM_SURVIVAL_SUB_STAGES-1
			REPEAT serverBD.sWaveData.iTotalSquads j
				REPEAT serverBD.sWaveData.iNumPedsInSquad[iStage] i
			        IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD_Enemies_S.structAiSquad[j][iStage].sPed[i].netId)
			                bAllCleared = FALSE
			            IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD_Enemies_S.structAiSquad[j][iStage].sPed[i].netId)
			                DELETE_NET_ID(serverBD_Enemies_S.structAiSquad[j][iStage].sPed[i].netId)
			            ELSE
			                NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverBD_Enemies_S.structAiSquad[j][iStage].sPed[i].netId)
			            ENDIF
			        ENDIF
			    ENDREPEAT
		    ENDREPEAT
		ENDFOR
		
	    REPEAT MAX_NUM_SURVIVAL_HELIS i
	        IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD_Enemies_V.sSurvivalHeli[i].netId)
	            bAllCleared = FALSE
	            IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD_Enemies_V.sSurvivalHeli[i].netId)
	                DELETE_NET_ID(serverBD_Enemies_V.sSurvivalHeli[i].netId)
	            ELSE
	                NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverBD_Enemies_V.sSurvivalHeli[i].netId)
	            ENDIF
	        ENDIF
			REPEAT NUM_HELI_PEDS j
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD_Enemies_V.sSurvivalHeli[i].sPed[j].netId)
		            bAllCleared = FALSE
		            IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD_Enemies_V.sSurvivalHeli[i].sPed[j].netId)
		                DELETE_NET_ID(serverBD_Enemies_V.sSurvivalHeli[i].sPed[j].netId)
		            ELSE
		                NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverBD_Enemies_V.sSurvivalHeli[i].sPed[j].netId)
		            ENDIF
		        ENDIF
			ENDREPEAT
	    ENDREPEAT
	    
		REPEAT MAX_NUM_LAND_VEHICLES i
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD_Enemies_V.sSurvivalLandVehicle[i].netId)
	            bAllCleared = FALSE
	            IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD_Enemies_V.sSurvivalLandVehicle[i].netId)
	                DELETE_NET_ID(serverBD_Enemies_V.sSurvivalLandVehicle[i].netId)
	            ELSE
	                NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverBD_Enemies_V.sSurvivalLandVehicle[i].netId)
	            ENDIF
	        ENDIF
			REPEAT NUM_LAND_VEH_PEDS j
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD_Enemies_V.sSurvivalLandVehicle[i].sPed[j].netId)
		            bAllCleared = FALSE
		            IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD_Enemies_V.sSurvivalLandVehicle[i].sPed[j].netId)
		                DELETE_NET_ID(serverBD_Enemies_V.sSurvivalLandVehicle[i].sPed[j].netId)
		            ELSE
		                NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverBD_Enemies_V.sSurvivalLandVehicle[i].sPed[j].netId)
		            ENDIF
		        ENDIF
			ENDREPEAT
		ENDREPEAT
		
	    IF bAllCleared
	        serverBD.bSkipWave = FALSE
	    ENDIF

	ENDIF
	    
ENDPROC

PROC DEBUG_FORCE_BLIP_ALL_ENEMIES(BOOL bOnOff)
    
    INT i, j, iStage
    
    IF bOnOff
		
		FOR iStage = 0 TO NUM_SURVIVAL_SUB_STAGES-1
			REPEAT serverBD.sWaveData.iTotalSquads j
				REPEAT serverBD.sWaveData.iNumPedsInSquad[iStage] i
		            IF NOT IS_NET_PED_INJURED(serverBD_Enemies_S.structAiSquad[j][iStage].sPed[i].netId)
		                IF NOT DOES_BLIP_EXIST(biEnemy[j][i][iStage])
		                    biEnemy[j][i][iStage] = ADD_BLIP_FOR_ENTITY(NET_TO_PED(serverBD_Enemies_S.structAiSquad[j][iStage].sPed[i].netId))
		                ENDIF
		            ENDIF
				ENDREPEAT
	        ENDREPEAT
		ENDFOR

    ELSE
    	FOR iStage = 0 TO NUM_SURVIVAL_SUB_STAGES-1
	      	REPEAT serverBD.sWaveData.iTotalSquads j
				REPEAT serverBD.sWaveData.iNumPedsInSquad[iStage] i
		            IF DOES_BLIP_EXIST(biEnemy[j][i][iStage])
		                REMOVE_BLIP(biEnemy[j][i][iStage])
		            ENDIF
				ENDREPEAT
	        ENDREPEAT
		ENDFOR
    
    ENDIF
    
ENDPROC

/// PURPOSE:
///    Updates Horde widgets.
PROC UPDATE_WIDGETS()
    
    INT i, j, iStage
    
	iMyPartId = iLocalPart
	
    SET_CONTENTS_OF_TEXT_WIDGET(twID_HordeStageServer, GET_HORDE_STAGE_NAME(GET_SERVER_HORDE_STAGE()))
    SET_CONTENTS_OF_TEXT_WIDGET(twID_HordeStageClient, GET_HORDE_STAGE_NAME(GET_CLIENT_HORDE_STAGE(PARTICIPANT_ID())))
    SET_CONTENTS_OF_TEXT_WIDGET(twID_endReason, GET_END_HORDE_REASON_NAME(GET_END_HORDE_REASON()))
    SET_CONTENTS_OF_TEXT_WIDGET(twID_gameStateServer, GET_GAME_STATE_NAME(GET_SERVER_GAME_STATE()))
    SET_CONTENTS_OF_TEXT_WIDGET(twID_gameStateClient, GET_GAME_STATE_NAME(GET_CLIENT_GAME_STATE(iLocalPart)))
    SET_CONTENTS_OF_TEXT_WIDGET(twID_killStreakState, GET_KILL_STREAK_STATE_NAME(structKillStreakData.eState))
    SET_CONTENTS_OF_TEXT_WIDGET(twID_WaveSubStage, GET_WAVE_SUB_STAGE_NAME(serverBD.sWaveData.eStage))
    SET_CONTENTS_OF_TEXT_WIDGET(twID_NextWaveDelayStage, GET_DELAY_BEFORE_NEXT_WAVE_SUB_STAGE_NAME(serverBD.sDelayBeforeNextWaveStageData))
    SET_CONTENTS_OF_TEXT_WIDGET(twID_minBoardState, GET_MINI_BOARD_STAGE_NAME(eMiniLeaderBoardData.eMiniLeaderBoardState))
    
	IF bUpdateOverheadOverride
		IF (INT_TO_ENUM(eOVERHEAD_OVERRIDE_STATE, iOverheadOverrideState) != MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.eOverheadOverrideState[0])
			MPGlobalsHud_TitleUpdate.DisplayInfo_TitleUpdate.eOverheadOverrideState[0] = INT_TO_ENUM(eOVERHEAD_OVERRIDE_STATE, iOverheadOverrideState)
		ENDIF
		bUpdateOverheadOverride = FALSE
	ENDIF
	
	REPEAT MAX_NUM_SURVIVAL_HELIS i
		IF bIsLocalPlayerHost
			IF bForceMakeSureBlownUpState[i]
				IF serverBD_Enemies_V.sSurvivalHeli[i].eState < eSURVIVALHELISTATE_MAKE_SURE_BLOWN_UP
					RESET_NET_TIMER(serverBD_Enemies_V.sSurvivalHeli[i].stBlownUpTimer)
					START_NET_TIMER(serverBD_Enemies_V.sSurvivalHeli[i].stBlownUpTimer)
		            serverBD_Enemies_V.sSurvivalHeli[i].eState = eSURVIVALHELISTATE_MAKE_SURE_BLOWN_UP
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
    REPEAT MAX_NUM_SURVIVAL_HELIS i
        SET_CONTENTS_OF_TEXT_WIDGET(twID_heliState[i], GET_SURVIVAL_HELI_STATE_NAME(serverBD_Enemies_V.sSurvivalHeli[i].eState))
    ENDREPEAT
	
	REPEAT MAX_NUM_LAND_VEHICLES i
		SET_CONTENTS_OF_TEXT_WIDGET(twID_landVehicleState[i], GET_LAND_VEHICLE_STATE_NAME(serverBD_Enemies_V.sSurvivalLandVehicle[i].eState))
	ENDREPEAT
	
    bCurrentWaveDefeated = IS_BIT_SET(serverBD.iBitset, BITSET_SERVER_CURRENT_WAVE_DEFEATED)
    bDidFirstWaveSetup = IS_BIT_SET(serverBD.iBitset, BITSET_SERVER_CURRENT_WAVE_DEFEATED)
	
    bDealtWithFeedback = IS_BIT_SET(playerBD[iLocalPart].iBitset, BITSET_PLAYER_I_HAVE_DEALT_WITH_FEEDBACK)
    bReadyForstartFadeIn = IS_BIT_SET(playerBD[iLocalPart].iBitset, BITSET_PLAYER_READY_FOR_START_FADE_IN)
    bCompletedEndWaveHud = IS_BIT_SET(playerBD[iLocalPart].iBitset, BITSET_PLAYER_I_HAVE_COMPLETED_END_OF_WAVE_HUD)
    
    iWaveStageKillLimit = GET_COMPLETE_WAVE_STAGE_KILL_LIMIT(serverBD.sWaveData.eStage)
    
    IF bForceBlipAllEnemies
        IF NOT bForceBlippedEnemies
            DEBUG_FORCE_BLIP_ALL_ENEMIES(TRUE)
            bForceBlippedEnemies = TRUE
        ENDIF
    ELSE
        IF bForceBlippedEnemies
            DEBUG_FORCE_BLIP_ALL_ENEMIES(FALSE)
            bForceBlippedEnemies = FALSE
        ENDIF
    ENDIF
	
    IF bPutEnemiesOutsideHordeArea
		REPEAT serverBD.sWaveData.iTotalSquads j
			REPEAT serverBD.sWaveData.iNumPedsInSquad[iStage] i
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD_Enemies_S.structAiSquad[j][iStage].sPed[i].netId)
					SET_ENTITY_COORDS(NET_TO_PED(serverBD_Enemies_S.structAiSquad[j][iStage].sPed[i].netId), <<-85.4480, 2803.1404, 52.3173>>)
				ENDIF
			ENDREPEAT
		ENDREPEAT
		bPutEnemiesOutsideHordeArea = FALSE
	ENDIF
	
	IF bDebugBlipAllAi
		
		bDebugUnblipAllAi = TRUE
		
		FOR iStage = 0 TO NUM_SURVIVAL_SUB_STAGES-1
			REPEAT serverBD.sWaveData.iTotalSquads i
				REPEAT serverBD.sWaveData.iNumPedsInSquad[iStage] j
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD_Enemies_S.structAiSquad[i][iStage].sPed[j].netId)
						IF NOT IS_NET_PED_INJURED(serverBD_Enemies_S.structAiSquad[i][iStage].sPed[j].netId)
							IF NOT DOES_BLIP_EXIST(biDebugFootEnemyBlip[i][j])
								biDebugFootEnemyBlip[i][j] = ADD_BLIP_FOR_ENTITY(NET_TO_PED(serverBD_Enemies_S.structAiSquad[i][iStage].sPed[j].netId))
								SET_BLIP_COLOUR_FROM_HUD_COLOUR(biDebugFootEnemyBlip[i][j],HUD_COLOUR_NET_PLAYER22)
							ENDIF
						ELSE
							IF DOES_BLIP_EXIST(biDebugFootEnemyBlip[i][j])
								REMOVE_BLIP(biDebugFootEnemyBlip[i][j])
							ENDIF
						ENDIF
					ELSE
						IF DOES_BLIP_EXIST(biDebugFootEnemyBlip[i][j])
							REMOVE_BLIP(biDebugFootEnemyBlip[i][j])
						ENDIF
					ENDIF
				ENDREPEAT
			ENDREPEAT
		ENDFOR
		REPEAT MAX_NUM_SURVIVAL_HELIS i
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD_Enemies_V.sSurvivalHeli[i].netId)
				IF IS_NET_VEHICLE_DRIVEABLE(serverBD_Enemies_V.sSurvivalHeli[i].netId)
					IF NOT DOES_BLIP_EXIST(biDebugHeliBlip[i])
						biDebugHeliBlip[i] = ADD_BLIP_FOR_ENTITY(NET_TO_VEH(serverBD_Enemies_V.sSurvivalHeli[i].netId))
						SET_BLIP_COLOUR_FROM_HUD_COLOUR(biDebugHeliBlip[i],HUD_COLOUR_NET_PLAYER22)
					ENDIF
				ELSE
					IF DOES_BLIP_EXIST(biDebugHeliBlip[i])
						REMOVE_BLIP(biDebugHeliBlip[i])
					ENDIF
				ENDIF
			ELSE
				IF DOES_BLIP_EXIST(biDebugHeliBlip[i])
					REMOVE_BLIP(biDebugHeliBlip[i])
				ENDIF
			ENDIF
			REPEAT NUM_HELI_PEDS j
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD_Enemies_V.sSurvivalHeli[i].sPed[j].netId)
					IF NOT IS_NET_PED_INJURED(serverBD_Enemies_V.sSurvivalHeli[j].sPed[i].netId)
						IF NOT DOES_BLIP_EXIST(biDebugHeliEnemyBlip[i][j])
							biDebugHeliEnemyBlip[i][j] = ADD_BLIP_FOR_ENTITY(NET_TO_PED(serverBD_Enemies_V.sSurvivalHeli[i].sPed[j].netId))
							SET_BLIP_COLOUR_FROM_HUD_COLOUR(biDebugHeliEnemyBlip[i][j],HUD_COLOUR_NET_PLAYER22)
						ENDIF
					ELSE
						IF DOES_BLIP_EXIST(biDebugHeliEnemyBlip[i][j])
							REMOVE_BLIP(biDebugHeliEnemyBlip[i][j])
						ENDIF
					ENDIF
				ELSE
					IF DOES_BLIP_EXIST(biDebugHeliEnemyBlip[i][j])
						REMOVE_BLIP(biDebugHeliEnemyBlip[i][j])
					ENDIF
				ENDIF
			ENDREPEAT
		ENDREPEAT
				
		REPEAT MAX_NUM_LAND_VEHICLES i
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD_Enemies_V.sSurvivalLandVehicle[i].netId)
				IF IS_NET_VEHICLE_DRIVEABLE(serverBD_Enemies_V.sSurvivalLandVehicle[i].netId)
					IF NOT DOES_BLIP_EXIST(biDebugLandVehBlip[i])
						biDebugLandVehBlip[i] = ADD_BLIP_FOR_ENTITY(NET_TO_VEH(serverBD_Enemies_V.sSurvivalLandVehicle[i].netId))
						SET_BLIP_COLOUR_FROM_HUD_COLOUR(biDebugLandVehBlip[i],HUD_COLOUR_NET_PLAYER22)
					ENDIF
				ELSE
					IF DOES_BLIP_EXIST(biDebugLandVehBlip[i])
						REMOVE_BLIP(biDebugLandVehBlip[i])
					ENDIF
				ENDIF
			ELSE
				IF DOES_BLIP_EXIST(biDebugLandVehBlip[i])
					REMOVE_BLIP(biDebugLandVehBlip[i])
				ENDIF
			ENDIF
			REPEAT NUM_LAND_VEH_PEDS j
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD_Enemies_V.sSurvivalLandVehicle[i].sPed[j].netId)
					IF NOT IS_NET_PED_INJURED(serverBD_Enemies_V.sSurvivalLandVehicle[i].sPed[j].netId)
						IF NOT DOES_BLIP_EXIST(biDebugLandVehEnemyBlip[i][j])
							biDebugLandVehEnemyBlip[i][j] = ADD_BLIP_FOR_ENTITY(NET_TO_PED(serverBD_Enemies_V.sSurvivalLandVehicle[i].sPed[j].netId))
							SET_BLIP_COLOUR_FROM_HUD_COLOUR(biDebugLandVehEnemyBlip[i][j],HUD_COLOUR_NET_PLAYER22)
						ENDIF
					ELSE
						IF DOES_BLIP_EXIST(biDebugLandVehEnemyBlip[i][j])
							REMOVE_BLIP(biDebugLandVehEnemyBlip[i][j])
						ENDIF
					ENDIF
				ELSE
					IF DOES_BLIP_EXIST(biDebugLandVehEnemyBlip[i][j])
						REMOVE_BLIP(biDebugLandVehEnemyBlip[i][j])
					ENDIF
				ENDIF
			ENDREPEAT
		ENDREPEAT
		
	ELSE
		
		IF bDebugUnblipAllAi
			
			FOR iStage = 0 TO NUM_SURVIVAL_SUB_STAGES-1
				REPEAT serverBD.sWaveData.iTotalSquads j
					REPEAT serverBD.sWaveData.iNumPedsInSquad[iStage] i
						IF DOES_BLIP_EXIST(biDebugFootEnemyBlip[j][i])
							REMOVE_BLIP(biDebugFootEnemyBlip[j][i])
						ENDIF
					ENDREPEAT
				ENDREPEAT
			ENDFOR
			
			REPEAT MAX_NUM_SURVIVAL_HELIS i
				IF DOES_BLIP_EXIST(biDebugHeliBlip[i])
					REMOVE_BLIP(biDebugHeliBlip[i])
				ENDIF
				REPEAT NUM_HELI_PEDS j
					IF DOES_BLIP_EXIST(biDebugHeliEnemyBlip[i][j])
						REMOVE_BLIP(biDebugHeliEnemyBlip[i][j])
					ENDIF
				ENDREPEAT
			ENDREPEAT
					
			REPEAT MAX_NUM_LAND_VEHICLES i
				IF DOES_BLIP_EXIST(biDebugLandVehBlip[i])
					REMOVE_BLIP(biDebugLandVehBlip[i])
				ENDIF
				REPEAT NUM_LAND_VEH_PEDS j
					IF DOES_BLIP_EXIST(biDebugLandVehEnemyBlip[i][j])
						REMOVE_BLIP(biDebugLandVehEnemyBlip[i][j])
					ENDIF
				ENDREPEAT
			ENDREPEAT
				
			bDebugUnblipAllAi = FALSE
			
		ENDIF
		
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Creates Horde widgets.
PROC MAINTAIN_WIDGETS()
    
    INT i
    TEXT_LABEL_63 tl63
    
    SWITCH iCreateWidgetsStage
            
            // Create widgets widget.
            CASE 0
				survivalWidgetsId = START_WIDGET_GROUP("Survival")
				ADD_WIDGET_BOOL("Create Survival Widgets", bCreateWidgets)
				STOP_WIDGET_GROUP()
				iCreateWidgetsStage++
            BREAK
            
            CASE 1
                IF bCreateWidgets
                    DELETE_WIDGET_GROUP(survivalWidgetsId)
                    
                    survivalWidgetsId = START_WIDGET_GROUP("Survival")
                            
							ADD_WIDGET_INT_SLIDER("Overhead Override", iOverheadOverrideState, 0, 2, 1)
							ADD_WIDGET_BOOL("Update Overhead override", bUpdateOverheadOverride)
							
							ADD_WIDGET_INT_READ_ONLY("My Participant ID", iMyPartId)
							
							START_WIDGET_GROUP("Blip All")
								ADD_WIDGET_BOOL("Blip All Enemies", bDebugBlipAllAi)
							STOP_WIDGET_GROUP()
							
							START_WIDGET_GROUP("XP Breakdown")
								ADD_XP_BREAKDOWN_WIDGETS()
							STOP_WIDGET_GROUP()
							
							ADD_WIDGET_BOOL("All Completed End of Wave Prints", bDoAllCompletedWavePrints)
							
							ADD_WIDGET_BOOL("bDoWaveCompleteHudPrints", bDoWaveCompleteHudPrints)
                            ADD_WIDGET_BOOL("bDo893474Prints", bDo893474Prints)
                            ADD_WIDGET_BOOL("bDoMiniBoardDebugPrints", bDoMiniBoardDebugPrints)
                            ADD_WIDGET_BOOL("bDoRespawnMissionPedPrints", bDoRespawnMissionPedPrints)
                            ADD_WIDGET_BOOL("bDoProgressBarPrints", bDoProgressBarPrints)
                            ADD_WIDGET_BOOL("bWaveStageDataWidgets", bWaveStageDataWidgets)
							ADD_WIDGET_BOOL("bDoEndWaveWidgets", bDoEndWaveWidgets)
							ADD_WIDGET_BOOL("bPutEnemiesOutsideHordeArea", bPutEnemiesOutsideHordeArea)
							ADD_WIDGET_BOOL("bPedSpawningBug", bPedSpawningBug)
							ADD_WIDGET_BOOL("bForceEnemyBlips", bForceEnemyBlips)
							ADD_WIDGET_BOOL("bPrintPartListInfo", bPrintPartListInfo)
							
							ADD_WIDGET_BOOL("bPauseSceneOnShot3", bPauseSceneOnShot3)
							ADD_WIDGET_VECTOR_SLIDER("Cam Offset From Heli", vHeliOffset, -10000.0, 10000.0, 0.01)
							ADD_WIDGET_VECTOR_SLIDER("Cam Rotation", vCamRotation, -10000.0, 10000.0, 0.01)
							ADD_WIDGET_FLOAT_SLIDER("Cam FOV", fCamFov, -10000.0, 10000.0, 0.01)
							
							ADD_WIDGET_INT_READ_ONLY("My Deaths", playerBD[iLocalPart].iDeaths)
							START_WIDGET_GROUP("Distance Fail Checks")
								ADD_WIDGET_BOOL("Block check", bBlockDistanceCheck)
							STOP_WIDGET_GROUP()
							
                            START_WIDGET_GROUP("Overlap Peds")
                                    ADD_WIDGET_BOOL("Activate Overlap Peds", bActivateOverlapPeds)
                            STOP_WIDGET_GROUP()
                            
                            START_WIDGET_GROUP("AI Helis")
                                    REPEAT MAX_NUM_SURVIVAL_HELIS i
                                            twID_heliState[i] = ADD_TEXT_WIDGET("State")
                                            ADD_WIDGET_BOOL("Spawn Heli", serverBD_Enemies_V.sSurvivalHeli[i].bActive)
											ADD_WIDGET_BOOL("bForceMakeSureBlownUpState", bForceMakeSureBlownUpState[i])
                                    ENDREPEAT
                            STOP_WIDGET_GROUP()
                            
							START_WIDGET_GROUP("AI Land Vehicles")
                                    REPEAT MAX_NUM_LAND_VEHICLES i
                                        twID_landVehicleState[i] = ADD_TEXT_WIDGET("State")
										ADD_WIDGET_INT_READ_ONLY("Num Peds", serverBD_Enemies_V.sSurvivalLandVehicle[i].iNumPeds)
                                    ENDREPEAT
                            STOP_WIDGET_GROUP()
							
                            START_WIDGET_GROUP("Mini Board")
                                    twID_minBoardState = ADD_TEXT_WIDGET("State")
                            STOP_WIDGET_GROUP()
                            
                            START_WIDGET_GROUP("HUD")
                                  //  ADD_WIDGET_BOOL("bGotWaveProgressLabel", bGotWaveProgressLabel)
                                    ADD_WIDGET_BOOL("Force End Wave Text Respect Offset", bForceWavePassedOffset)
                            STOP_WIDGET_GROUP()
                            
                            START_WIDGET_GROUP("Kill Streaks")
                                    twID_killStreakState = ADD_TEXT_WIDGET("State")
                            STOP_WIDGET_GROUP()
                            
                            START_WIDGET_GROUP("Fighting Participants Data")
                                    ADD_WIDGET_INT_READ_ONLY("Num Fighting Participants", serverBD.sFightingParticipantsList.iNumFightingParticipants)
									ADD_WIDGET_INT_READ_ONLY("Num Potential Fighting Participants", serverBD.sFightingParticipantsList.iNumPotentialfightingParticipants)
									ADD_WIDGET_INT_READ_ONLY("Num Start Wave Fighting Participants", serverBD.sFightingParticipantsList.iNumFightingParticipantsBeginningOfWave)
                                    REPEAT MAX_NUMBER_HORDE_FIGHTING_PARTICIPANTS i
                                            tl63 = "fighting Participant Slot "
                                            tl63 += i
                                            ADD_WIDGET_INT_READ_ONLY(tl63, serverBD.sFightingParticipantsList.iParticipant[i])
                                    ENDREPEAT
                            STOP_WIDGET_GROUP()
                            
                            START_WIDGET_GROUP("Board Data")
                                    ADD_WIDGET_INT_READ_ONLY("My Kills", playerBD[iLocalPart].iMyKills)
                                    ADD_WIDGET_BOOL("Populated Player Array", sEndBoardData.bPopulatedHudPlayerArray)
                                    ADD_WIDGET_INT_READ_ONLY("Player 0", iHudPlayers[0])
                                    ADD_WIDGET_INT_READ_ONLY("Score", sEndBoardData.iScores[0])
                                    ADD_WIDGET_INT_READ_ONLY("Player 1", iHudPlayers[1])
                                    ADD_WIDGET_INT_READ_ONLY("Score", sEndBoardData.iScores[1])
                                    ADD_WIDGET_INT_READ_ONLY("Player 2", iHudPlayers[2])
                                    ADD_WIDGET_INT_READ_ONLY("Score", sEndBoardData.iScores[2])
                                    ADD_WIDGET_INT_READ_ONLY("Player 3", iHudPlayers[3])
                                    ADD_WIDGET_INT_READ_ONLY("Score", sEndBoardData.iScores[3])
                            STOP_WIDGET_GROUP()
                            
                            START_WIDGET_GROUP("Bitsets and Flags")
									START_WIDGET_GROUP("Non Broadcast")
										ADD_WIDGET_BOOL("bIWentFromSpectatingToPassedWaveStage", bIWentFromSpectatingToPassedWaveStage)
									STOP_WIDGET_GROUP()
                                    START_WIDGET_GROUP("Client")
                                            ADD_WIDGET_BOOL("Dealt with EOM Feedback", bDealtWithFeedback)
                                            ADD_WIDGET_BOOL("Ready For Start Fade In", bReadyForstartFadeIn)
                                            ADD_WIDGET_BOOL("Did End Of Wave Hud", bCompletedEndWaveHud)
                                    STOP_WIDGET_GROUP()
                                    START_WIDGET_GROUP("Server")
                                            ADD_WIDGET_BOOL("Current Wave Defeated", bCurrentWaveDefeated)
                                            ADD_WIDGET_BOOL("First Wave Setup", bDidFirstWaveSetup)
                                    STOP_WIDGET_GROUP()
                            STOP_WIDGET_GROUP()
                            
                            START_WIDGET_GROUP("Respawning")
                                    ADD_WIDGET_INT_READ_ONLY("Ped Respawn Check", serverBd.iSearchingForPed)
                                    ADD_WIDGET_INT_READ_ONLY("Respawn Stage", iRespawningStage)
                            STOP_WIDGET_GROUP()
                            
                            START_WIDGET_GROUP("Wave Data")
                                    ADD_WIDGET_INT_SLIDER("Wave Count", serverBD.sWaveData.iWaveCount, 1, 20, 1)
									ADD_WIDGET_INT_READ_ONLY("Num Created Enemies", serverBD.iNumCreatedEnemies)
									ADD_WIDGET_INT_READ_ONLY("Alive Squad Enemies", serverBD.iNumAliveSquadEnemies)
                                    ADD_WIDGET_INT_READ_ONLY("Alive Helis", serverBD.iNumAliveHelis)
									ADD_WIDGET_INT_READ_ONLY("Alive Bike Peds", serverBD.iNumAliveLandVehiclePeds)
									ADD_WIDGET_INT_READ_ONLY("Wave Kills Required", serverBD.sWaveData.iNumRequiredKills)
                                    ADD_WIDGET_INT_READ_ONLY("Kills This Wave", serverBD.iKillsThisWave)
									ADD_WIDGET_INT_READ_ONLY("Kills This Wave Events", serverBd.iNumKillsThisWaveFromEvents)
                                    twID_WaveSubStage = ADD_TEXT_WIDGET("Sub Stage")
                                    ADD_WIDGET_INT_READ_ONLY("Sub Stage Kills Required", iWaveStageKillLimit)
                                    ADD_WIDGET_INT_READ_ONLY("Sub Stage Easy Kills", serverBD.iKillsThisWaveSubStage[eWAVESTAGE_EASY])
									ADD_WIDGET_INT_READ_ONLY("Sub Stage Easy Kills", serverBD.iKillsThisWaveSubStage[eWAVESTAGE_MEDIUM])
									ADD_WIDGET_INT_READ_ONLY("Sub Stage Easy Kills", serverBD.iKillsThisWaveSubStage[eWAVESTAGE_HARD])
									ADD_WIDGET_INT_READ_ONLY("Kills This Sub Stage Events", serverBd.iNumKillsThisSubStageFromEvents[eWAVESTAGE_EASY])
									ADD_WIDGET_INT_READ_ONLY("Kills This Sub Stage Events", serverBd.iNumKillsThisSubStageFromEvents[eWAVESTAGE_MEDIUM])
									ADD_WIDGET_INT_READ_ONLY("Kills This Sub Stage Events", serverBd.iNumKillsThisSubStageFromEvents[eWAVESTAGE_HARD])
                                    ADD_WIDGET_INT_READ_ONLY("My Kills This Wave", playerBD[iLocalPart].iMyKillsThisWave)
                                    ADD_WIDGET_INT_READ_ONLY("Total Mode Kills", serverBd.iKills)
                            STOP_WIDGET_GROUP()
                            
                            START_WIDGET_GROUP("Stages")
                                    twID_HordeStageServer = ADD_TEXT_WIDGET("Server Main")
                                    twID_NextWaveDelayStage = ADD_TEXT_WIDGET("Server Delay Next Wave")
                                    twID_HordeStageClient = ADD_TEXT_WIDGET("Local")
                            STOP_WIDGET_GROUP()
                            
                            START_WIDGET_GROUP("Game States")
                                    twID_gameStateServer = ADD_TEXT_WIDGET("Server")
                                    twID_gameStateClient = ADD_TEXT_WIDGET("Client")
                            STOP_WIDGET_GROUP()
                            
                            START_WIDGET_GROUP("End Reason")
                                    twID_endReason = ADD_TEXT_WIDGET("Reason")
                            STOP_WIDGET_GROUP()
							
							START_WIDGET_GROUP("Debug")
								ADD_WIDGET_BOOL("Show advanced spawning spew", g_SpawnData.bShowAdvancedSpew)
							STOP_WIDGET_GROUP()
                                                        
							CREATE_CELEBRATION_SCREEN_WIDGETS()

                    STOP_WIDGET_GROUP()
                    
                    iCreateWidgetsStage++
                    
                ENDIF
            BREAK
            
            CASE 2
				UPDATE_WIDGETS()
            BREAK
            
    ENDSWITCH
ENDPROC

PROC PRINT_INJURED_PED_DATA(NETWORK_INDEX &netId)
            
        INT iNetId, iDamager, iPedId
        TEXT_LABEL_31 tl31_weapon
        WEAPON_TYPE weapon
        PLAYER_INDEX damager 
        
        iNetId = NATIVE_TO_INT(netId)
        iPedId = NATIVE_TO_INT(NET_TO_PED(netId))
        damager = NETWORK_GET_DESTROYER_OF_NETWORK_ID(netId, weapon)
        iDamager = NATIVE_TO_INT(damager)
        tl31_weapon = GET_WEAPON_NAME(weapon)
        
        PRINTLN("[Survival] - [AI Spawning] - ||  Injured Ped Info --------------------||")
        PRINTLN("[Survival] - [AI Spawning] - ||  Dead ped net ID:       ", iNetId)
        PRINTLN("[Survival] - [AI Spawning] - ||  Dead ped ped ID:       ", iPedId)
        PRINTLN("[Survival] - [AI Spawning] - ||  Dead ped damager:      ", iDamager)
        PRINTLN("[Survival] - [AI Spawning] - ||  Dead ped killed with:  ", tl31_weapon)
        PRINTLN("[Survival] - [AI Spawning] - ||---------------------------------------||")
        
ENDPROC
	
PROC DUMP_WAVE_DATA_INFO()
	INT i, i2
	PRINTLN("**************************************")
	PRINTLN("********* SURVIVAL DATA DUMP *********")
	PRINTLN("**************************************")
	
	INT iSize = SIZE_OF(serverBD)
	PRINTLN("Size of serverBD: ", (iSize * 8))
	iSize = SIZE_OF(serverBD_Enemies_S)
	PRINTLN("Size of serverBD_Enemies_S: ", (iSize * 8))
	iSize = SIZE_OF(serverBD_Enemies_V)
	PRINTLN("Size of serverBD_Enemies_V: ", (iSize * 8))
	
	PRINTLN("Mission: ", g_FMMC_STRUCT.tl63MissionName)
	PRINTLN("Description Hash: ", g_sCurrentMissionDesc.descHash)
	PRINTLN("iRootContentIDHash: ", g_FMMC_STRUCT.iRootContentIDHash)
	PRINTLN("Number of waves: ", g_FMMC_STRUCT.sSurvivalWaveData.iNumberOfWaves)
	
	PRINTLN("Survival Bitset: ", g_FMMC_STRUCT.sSurvivalWaveData.iBitset)
	
	IF g_FMMC_STRUCT.sSurvivalWaveData.iBoundsType = SURV_BOUNDS_NON_AXIS_AREA
		PRINTLN("Play Area is a Non Axis Aligned Area")
		VECTOR vTemp = GET_SURVIVAL_BOUNDS_POINT_WITH_HEIGHT()
		VECTOR vBase = GET_SURVIVAL_BOUNDS_POINT_WITH_LOW_OFFSET()
		PRINTLN("Play Area: Point 0 - ", g_FMMC_STRUCT.sSurvivalWaveData.vBounds[0], " Point 1 - ", g_FMMC_STRUCT.sSurvivalWaveData.vBounds[1], " Width - ", g_FMMC_STRUCT.sSurvivalWaveData.fBoundsRadius, " Height: ", g_FMMC_STRUCT.sSurvivalWaveData.fBoundsHeight,"(", vTemp,") Base: ",g_FMMC_STRUCT.sSurvivalWaveData.fBoundsBaseOffset,"(",vBase,")")
	ELSE
		PRINTLN("Play Area is a Cylinder")
		PRINTLN("Play Area: Point 0 - ", g_FMMC_STRUCT.sSurvivalWaveData.vBounds[0], " Width - ", g_FMMC_STRUCT.sSurvivalWaveData.fBoundsRadius)
	ENDIF
	
	
	PRINTLN("_________________")
	PRINTLN("Enemy Spawn Points")
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfPeds-1
		VECTOR vPos = g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].vPos
		PRINTLN("Enemy Spawn ", i, " Position: ", vPos)
	ENDFOR
	PRINTLN("_________________")
	PRINTLN("Land Vehicle Spawn Points")
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles-1
		VECTOR vPos = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].vPos
		PRINTLN("Land Vehicle Spawn ", i, " Position: ", vPos)
	ENDFOR
	
	FOR i = 1 TO g_FMMC_STRUCT.sSurvivalWaveData.iNumberOfWaves
		PRINTLN("_________________")
		PRINTLN("Wave: ", i)
		PRINTLN("Enemies left to progress substage automatically: ", g_FMMC_STRUCT.sSurvivalWaveData.iKillsToProgressStage[i])
		FOR i2 = 0 TO 2
			PRINTLN("***Difficulty ", i2)
			PRINTLN("Number of squads: ", g_FMMC_STRUCT.sSurvivalWaveData.iNumberOfSquads[i][i2])
			PRINTLN("Peds per squad: ", g_FMMC_STRUCT.sSurvivalWaveData.iPedsPerSquad[i][i2])
			PRINTLN("Heavy Units: ", g_FMMC_STRUCT.sSurvivalWaveData.iNumberOfHeavies[i][i2])
		ENDFOR
		PRINTLN("Number of Vehicles: ", g_FMMC_STRUCT.sSurvivalWaveData.iNumVehicles[i])
		FOR i2 = 0 TO g_FMMC_STRUCT.sSurvivalWaveData.iNumVehicles[i]-1
			PRINTLN("****Land Vehicle ", i2)
			PRINTLN("Model: ", GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT.sSurvivalWaveData.mnVeh[i][i2]))
			PRINTLN("Number of peds: ", g_FMMC_STRUCT.sSurvivalWaveData.iNumVehPeds[i][i2])
			PRINTLN("Health: ", g_FMMC_STRUCT.sSurvivalWaveData.iVehHealth[i][i2])
		ENDFOR
		PRINTLN("Number of Helicopters: ", g_FMMC_STRUCT.sSurvivalWaveData.iNumberOfHelis[i])
		FOR i2 = 0 TO g_FMMC_STRUCT.sSurvivalWaveData.iNumberOfHelis[i]-1
			PRINTLN("****Air Vehicle ", i2)
			PRINTLN("Model: ", GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT.sSurvivalWaveData.mnAir[i][i2]))
			PRINTLN("Health: ", g_FMMC_STRUCT.sSurvivalWaveData.iAirHealth[i][i2])
		ENDFOR
	ENDFOR
	
	PRINTLN("**************************************")
	PRINTLN("************** END DUMP **************")
	PRINTLN("**************************************")
ENDPROC

PROC ADD_DEBUG_LINE(TEXT_LABEL_63 tl63)
	VECTOR vScreenPos = <<0.79, 0.2, 0.0>>
	vScreenPos.y += 0.015*iDebugRow
	DRAW_DEBUG_TEXT_2D(tl63, vScreenPos)
	iDebugRow++
ENDPROC

PROC MAINTAIN_ON_SCREEN_DEBUG()
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_MULTIPLY, KEYBOARD_MODIFIER_NONE, "Toggle Survival on-screen debug")
		bShowOnScreenDebug = !bShowOnScreenDebug
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(bShowOnScreenDebug)
		IF bShowOnScreenDebug = FALSE
		AND NOT IS_ROCKSTAR_DEV()
			SCRIPT_ASSERT("Survival * Debug has been turned off.")
		ENDIF
	ENDIF
	
	IF !bShowOnScreenDebug
	AND IS_DEBUG_KEY_JUST_PRESSED(KEY_SPACE, KEYBOARD_MODIFIER_NONE, "")
		bShowOnScreenDebug = TRUE
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(bShowOnScreenDebug)
	ENDIF
	
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_NUMPAD7, KEYBOARD_MODIFIER_NONE, "Toggle Simulate Scaling")
		bSimulateScaled = !bSimulateScaled
	ENDIF
	
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_NUMPAD1, KEYBOARD_MODIFIER_NONE, "Kill a heli")
	AND IS_ROCKSTAR_DEV()
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD_Enemies_S.structAiSquad[iWarpSquad][serverBD.sWaveData.eStage].sPed[iWarpPed].netId)
			SET_ENTITY_COORDS(NET_TO_PED(serverBD_Enemies_S.structAiSquad[iWarpSquad][serverBD.sWaveData.eStage].sPed[iWarpPed].netId), GET_CAM_COORD(GET_DEBUG_CAM()))
		ENDIF
		iWarpPed++
		IF iWarpPed > 3
			iWarpPed = 0
			iWarpSquad++
		ENDIF
		IF iWarpSquad > 3
			iWarpSquad = 0
		ENDIF
	ENDIF
	
	iDebugRow = 0
	TEXT_LABEL_63 tl63
	IF bShowOnScreenDebug
		tl63 = "*******Survival Debug*******"
		ADD_DEBUG_LINE(tl63)
		tl63 = "Wave: "
		tl63 += serverBD.sWaveData.iWaveCount
		tl63 += " Stage: "
		tl63 += GET_SUB_STAGE_NAME(serverBd.sWaveData.eStage)
		ADD_DEBUG_LINE(tl63)
		
		IF NETWORK_IS_GAME_IN_PROGRESS()
			tl63 = "C Horde State: "
			tl63 += ENUM_TO_INT(GET_CLIENT_HORDE_STAGE(PARTICIPANT_ID()))
			ADD_DEBUG_LINE(tl63)
		ENDIF
		tl63 = "S Horde State: "
		tl63 += ENUM_TO_INT(GET_SERVER_HORDE_STAGE())
		ADD_DEBUG_LINE(tl63)
		
		tl63 = "Fighting Participants: "
		tl63 += serverBD.sFightingParticipantsList.iNumFightingParticipants
		ADD_DEBUG_LINE(tl63)
		
		tl63 = "Required Kills Wave: "
		tl63 += serverBD.sWaveData.iNumRequiredKills
		ADD_DEBUG_LINE(tl63)
		tl63 = "Current Kills Wave: "
		tl63 += serverBD.iKillsThisWave
		ADD_DEBUG_LINE(tl63)
		
		tl63 = "Required Kills SubStage: "
		tl63 += serverBD.sWaveData.iNumRequiredSubstageKills[serverBD.sWaveData.eStage]
		ADD_DEBUG_LINE(tl63)
		tl63 = "Current Kills SubStage: "
		tl63 += serverBD.iKillsThisWaveSubStage[serverBD.sWaveData.eStage]
		ADD_DEBUG_LINE(tl63)
		
		tl63 = "---------"
		ADD_DEBUG_LINE(tl63)
		tl63 = "Peds: "
		tl63 += serverBD.iNumAliveSquadEnemies
		ADD_DEBUG_LINE(tl63)
		tl63 = "Land Veh Peds: "
		tl63 += serverBD.iNumAliveLandVehiclePeds
		ADD_DEBUG_LINE(tl63)
		tl63 = "Air Veh: "
		tl63 += serverBD.iNumAliveHelis
		ADD_DEBUG_LINE(tl63)
		tl63 = "Total Remaining: "
		tl63 += serverBD.iNumAliveTotalEnemies
		ADD_DEBUG_LINE(tl63)
		tl63 = "Melee Remaining: "
		tl63 += serverBD.iNumberOfMeleeEnemies
		ADD_DEBUG_LINE(tl63)
		tl63 = "---------"
		ADD_DEBUG_LINE(tl63)
		
		tl63 = "Searching Ped: "
		tl63 += serverBd.iSearchingForPed
		ADD_DEBUG_LINE(tl63)
		tl63 = "Searching L Veh: "
		tl63 += serverBd.iSearchingForLandVehicle
		ADD_DEBUG_LINE(tl63)
		tl63 = "Searching A Veh: "
		tl63 += serverBd.iSearchingForChopper
		ADD_DEBUG_LINE(tl63)
		
		IF (serverBD.iNumAliveSquadEnemies + serverBD.iNumAliveLandVehiclePeds + serverBD.iNumAliveHelis) = 1
		AND DOES_BLIP_EXIST(biLastEnemy)
			tl63 = "Last Enemy: "
			VECTOR vLastEnemy = GET_BLIP_COORDS(biLastEnemy)
			tl63 += FLOAT_TO_STRING(vLastEnemy.x)
			tl63 += ", "
			tl63 += FLOAT_TO_STRING(vLastEnemy.y)
			tl63 += ", "
			tl63 += FLOAT_TO_STRING(vLastEnemy.z)
			ADD_DEBUG_LINE(tl63)
		ENDIF
		
		tl63 = "Current Mission Peds: "
		tl63 += GET_NUM_CREATED_MISSION_PEDS()
		ADD_DEBUG_LINE(tl63)
		
		tl63 = "Current Music: "
		tl63 += tlCurrentMusic
		ADD_DEBUG_LINE(tl63)
		
		IF HAS_NET_TIMER_STARTED(serverBD.tdSurvivalLength)
			tl63 = "Current time played: "
			tl63 += FLOOR(TO_FLOAT(GET_ABS_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.tdSurvivalLength))/60000)
			ADD_DEBUG_LINE(tl63)
		ENDIF
		tl63 = "Total Kills: "
		tl63 += serverBD.iKills
		ADD_DEBUG_LINE(tl63)
	ENDIF
	
	IF bSimulateScaled
		tl63 = "SIMULATING WAVE SCALING"
		DRAW_DEBUG_TEXT_2D(tl63, <<0.4, 0.1, 0.0>>)
	ENDIF
ENDPROC

#ENDIF
