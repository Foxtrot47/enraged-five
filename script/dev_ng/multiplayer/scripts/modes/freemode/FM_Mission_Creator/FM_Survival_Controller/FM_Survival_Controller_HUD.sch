USING "FM_Survival_Controller_Using.sch"
USING "FM_Survival_Controller_Utility.sch"

PROC ADD_SHOOTOUT_BLIP()
    
    IF NOT DOES_BLIP_EXIST(biShootOutBlip)
        PRINTLN("[Survival] - ADD_SHOOTOUT_BLIP - serverBD.vStartPos[0] = ", serverBD.vStartPos[0])           
        biShootOutBlip = ADD_BLIP_FOR_COORD(serverBD.vStartPos[0])
        SET_BLIP_ROUTE(biShootOutBlip, TRUE)
        SET_BLIP_SCALE(biShootOutBlip, BLIP_SIZE_NETWORK_COORD)
        SHOW_HEIGHT_ON_BLIP(biShootOutBlip, TRUE)
		SET_BLIP_NAME_FROM_TEXT_FILE(biShootOutBlip, "SC_SHTO")
    ENDIF
    
ENDPROC

PROC REMOVE_SHOOTOUT_BLIP()
    
    IF DOES_BLIP_EXIST(biShootOutBlip)
        SET_BLIP_ROUTE(biShootOutBlip, FALSE)
        REMOVE_BLIP(biShootOutBlip)
    ENDIF
    
ENDPROC

PROC REMOVE_ON_FOOT_PED_BLIPS()
	
	INT i, j, iStage
	
	FOR iStage = 0 TO NUM_SURVIVAL_SUB_STAGES-1
		REPEAT serverBD.sWaveData.iTotalSquads j
			REPEAT serverBD.sWaveData.iNumPedsInSquad[iStage] i
	        	CLEANUP_AI_PED_BLIP(structSquadEnemyBlip[j][i][iStage])
	    	ENDREPEAT
		ENDREPEAT
	ENDFOR
	
ENDPROC

PROC REMOVE_VEHICLE_PED_BLIPS()
	
	INT i, j
	
	REPEAT MAX_NUM_LAND_VEHICLES j
		REPEAT NUM_LAND_VEH_PEDS i
			CLEANUP_AI_PED_BLIP(structLandVehicleEnemyBlip[j][i])
		ENDREPEAT
	ENDREPEAT
	
ENDPROC

PROC REMOVE_HORDE_BLIPS(BOOL bRemoveGetToShootOutBlip = TRUE)
    
	INT i
	
    REMOVE_ON_FOOT_PED_BLIPS()
	REMOVE_VEHICLE_PED_BLIPS()
	
    REPEAT ciMAX_CREATED_WEAPONS i
        IF DOES_BLIP_EXIST(biPickupBlip[i])
            REMOVE_BLIP(biPickupBlip[i])
        ENDIF
    ENDREPEAT
    
    IF bRemoveGetToShootOutBlip
        REMOVE_SHOOTOUT_BLIP()
    ENDIF
    
ENDPROC

/// PURPOSE:
///    Broadcasts a ticker to all players.
/// PARAMS:
///    aTicker - ticker to broadcast.
PROC BROADCAST_FMMC_TICKER_TO_ALL_PLAYERS(TICKER_EVENTS aTicker)
    SCRIPT_EVENT_DATA_TICKER_CUSTOM_MESSAGE TickerMessage
    TickerMessage.TickerEvent = aTicker
    BROADCAST_CUSTOM_TICKER_EVENT(TickerMessage, ALL_PLAYERS_ON_SCRIPT())
	PRINTLN("[Survival] - BROADCAST_FMMC_TICKER_TO_ALL_PLAYERS - sent killed ped ticker.")        
ENDPROC

/// PURPOSE:
///    Fades the screen in.
/// RETURNS:
///    TRUE if screen is faded in, FALSE if not.
FUNC BOOL FADE_SCREEN_IN()
    
    IF IS_SCREEN_FADED_OUT()
    OR IS_SCREEN_FADING_OUT()
        DO_SCREEN_FADE_IN(500)
    ELIF IS_SCREEN_FADED_IN()
        RETURN TRUE
    ENDIF
    
    RETURN FALSE
    
ENDFUNC

FUNC BOOL IS_ONLY_ONE_ENEMY_REMAINING()
	IF serverBD.sWaveData.eStage = serverBD.sWaveData.eHighestSubstage
	AND serverBD.iNumAliveTotalEnemies <= 1
	AND serverBD.iNumCreatedEnemies > 1
	AND (serverBD.iNumCreatedEnemiesThisStage[serverBD.sWaveData.eStage] > 1 OR serverBD.iNumVehiclesCreatedThisStage[serverBD.sWaveData.eStage] > 1)
	AND HAS_ENOUGH_TIME_PASSED_ON_SUBSTAGE()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL FORCE_ENEMY_BLIPS_ON()

	IF DOES_BLIP_EXIST(biLastEnemy)
		RETURN FALSE
	ENDIF
	
    IF (serverBD.sWaveData.iNumRequiredKills - serverBD.iKillsThisWave) <= NUM_LAST_ENEMIES_TO_BLIP
		#IF IS_DEBUG_BUILD
			bForceEnemyBlips = TRUE
		#ENDIF
    	RETURN TRUE
    ENDIF
    
	#IF IS_DEBUG_BUILD
		bForceEnemyBlips = FALSE
	#ENDIF
	
    RETURN FALSE
    
ENDFUNC

PROC DISPLAY_SCOREBOARD(STRUCT_HORDE_BOARD_DATA &sBoardData, FLOAT fRespectUpOffset, INT iXp, BOOL bDoEndModeBoard)
    
    INT iScoretoDisplay
    
    // Xp reward.
    SET_TEXT_FONT(FONT_CURSIVE)
    SET_TEXT_SCALE(0.6000, 0.6000)
    SET_TEXT_COLOUR(255, 255, 255, 255)
    SET_TEXT_CENTRE(FALSE)
    SET_TEXT_DROPSHADOW(ENUM_TO_INT(DROPSTYLE_OUTLINEONLY), 0, 0, 0, 255)
    SET_TEXT_EDGE(0, 0, 0, 0, 0)
    DISPLAY_TEXT(0.38, 0.4250+fRespectUpOffset, "HRD_ENDWVXP")
    
    SET_TEXT_FONT(FONT_CURSIVE)
    SET_TEXT_SCALE(0.6000, 0.6000)
    SET_TEXT_COLOUR(255, 255, 255, 255)
    SET_TEXT_CENTRE(FALSE)
    SET_TEXT_DROPSHADOW(ENUM_TO_INT(DROPSTYLE_OUTLINEONLY), 0, 0, 0, 255)
    SET_TEXT_EDGE(0, 0, 0, 0, 0)
    DISPLAY_TEXT_WITH_NUMBER(0.60, 0.4250+fRespectUpOffset, "HRD_KILLSPLYAM", iXp)
    
    // Team kills.
    SET_TEXT_FONT(FONT_CURSIVE)
    SET_TEXT_SCALE(0.6000, 0.6000)
    SET_TEXT_COLOUR(255, 255, 255, 255)
    SET_TEXT_CENTRE(FALSE)
    SET_TEXT_DROPSHADOW(ENUM_TO_INT(DROPSTYLE_OUTLINEONLY), 0, 0, 0, 255)
    SET_TEXT_EDGE(0, 0, 0, 0, 0)
    DISPLAY_TEXT(0.38, 0.4750+fRespectUpOffset, "HRD_KILLSTEAM")
    
    SET_TEXT_FONT(FONT_CURSIVE)
    SET_TEXT_SCALE(0.6000, 0.6000)
    SET_TEXT_COLOUR(255, 255, 255, 255)
    SET_TEXT_CENTRE(FALSE)
    SET_TEXT_DROPSHADOW(ENUM_TO_INT(DROPSTYLE_OUTLINEONLY), 0, 0, 0, 255)
    SET_TEXT_EDGE(0, 0, 0, 0, 0)
    
    IF bDoEndModeBoard
    	iScoreToDisplay = serverBD.iKills
    ELSE
    	iScoreToDisplay = serverBD.iKillsThisWave
    ENDIF
    
    DISPLAY_TEXT_WITH_NUMBER(0.60, 0.4750+fRespectUpOffset, "HRD_KILLSPLYAM", iScoreToDisplay)
    
    // Player in 1st.
    IF sBoardData.hudPlayers[0] != INVALID_PLAYER_INDEX()
        IF IS_NET_PLAYER_OK(sBoardData.hudPlayers[0], FALSE)
                
            SET_TEXT_FONT(FONT_CURSIVE)
            SET_TEXT_SCALE(0.6000, 0.6000)
            SET_TEXT_COLOUR(255, 255, 255, 255)
            SET_TEXT_CENTRE(FALSE)
            SET_TEXT_DROPSHADOW(ENUM_TO_INT(DROPSTYLE_OUTLINEONLY), 0, 0, 0, 255)
            SET_TEXT_EDGE(0, 0, 0, 0, 0)
            DISPLAY_TEXT_WITH_PLAYER_NAME(0.38, 0.5250+fRespectUpOffset, "HRD_KILLSPLYNM", GET_PLAYER_NAME(sBoardData.hudPlayers[0]), HUD_COLOUR_WHITE)
            
            SET_TEXT_FONT(FONT_CURSIVE)
            SET_TEXT_SCALE(0.6000, 0.6000)
            SET_TEXT_COLOUR(255, 255, 255, 255)
            SET_TEXT_CENTRE(FALSE)
            SET_TEXT_DROPSHADOW(ENUM_TO_INT(DROPSTYLE_OUTLINEONLY), 0, 0, 0, 255)
            SET_TEXT_EDGE(0, 0, 0, 0, 0)
            DISPLAY_TEXT_WITH_NUMBER(0.60, 0.5250+fRespectUpOffset, "HRD_KILLSPLYAM", sBoardData.iScores[0])
                
        ENDIF
    ENDIF
    
    // Player in 2nd.
    IF sBoardData.hudPlayers[1] != INVALID_PLAYER_INDEX()
        IF IS_NET_PLAYER_OK(sBoardData.hudPlayers[1], FALSE)
                
            SET_TEXT_FONT(FONT_CURSIVE)
            SET_TEXT_SCALE(0.6000, 0.6000)
            SET_TEXT_COLOUR(255, 255, 255, 255)
            SET_TEXT_CENTRE(FALSE)
            SET_TEXT_DROPSHADOW(ENUM_TO_INT(DROPSTYLE_OUTLINEONLY), 0, 0, 0, 255)
            SET_TEXT_EDGE(0, 0, 0, 0, 0)
            DISPLAY_TEXT_WITH_PLAYER_NAME(0.38, 0.5750+fRespectUpOffset, "HRD_KILLSPLYNM",GET_PLAYER_NAME(sBoardData.hudPlayers[1]), HUD_COLOUR_WHITE)
            
            SET_TEXT_FONT(FONT_CURSIVE)
            SET_TEXT_SCALE(0.6000, 0.6000)
            SET_TEXT_COLOUR(255, 255, 255, 255)
            SET_TEXT_CENTRE(FALSE)
            SET_TEXT_DROPSHADOW(ENUM_TO_INT(DROPSTYLE_OUTLINEONLY), 0, 0, 0, 255)
            SET_TEXT_EDGE(0, 0, 0, 0, 0)
            DISPLAY_TEXT_WITH_NUMBER(0.60, 0.5750+fRespectUpOffset, "HRD_KILLSPLYAM", sBoardData.iScores[1])
                
        ENDIF
    ENDIF
    
    // Player in 3rd.
    IF sBoardData.hudPlayers[2] != INVALID_PLAYER_INDEX()
        IF IS_NET_PLAYER_OK(sBoardData.hudPlayers[2], FALSE)
                
            SET_TEXT_FONT(FONT_CURSIVE)
            SET_TEXT_SCALE(0.6000, 0.6000)
            SET_TEXT_COLOUR(255, 255, 255, 255)
            SET_TEXT_CENTRE(FALSE)
            SET_TEXT_DROPSHADOW(ENUM_TO_INT(DROPSTYLE_OUTLINEONLY), 0, 0, 0, 255)
            SET_TEXT_EDGE(0, 0, 0, 0, 0)
            DISPLAY_TEXT_WITH_PLAYER_NAME(0.38, 0.6250+fRespectUpOffset, "HRD_KILLSPLYNM", GET_PLAYER_NAME(sBoardData.hudPlayers[2]), HUD_COLOUR_WHITE)
            
            SET_TEXT_FONT(FONT_CURSIVE)
            SET_TEXT_SCALE(0.6000, 0.6000)
            SET_TEXT_COLOUR(255, 255, 255, 255)
            SET_TEXT_CENTRE(FALSE)
            SET_TEXT_DROPSHADOW(ENUM_TO_INT(DROPSTYLE_OUTLINEONLY), 0, 0, 0, 255)
            SET_TEXT_EDGE(0, 0, 0, 0, 0)
            DISPLAY_TEXT_WITH_NUMBER(0.60, 0.6250+fRespectUpOffset, "HRD_KILLSPLYAM", sBoardData.iScores[2])
                
        ENDIF
    ENDIF
    
    // Player in 4th.
    IF sBoardData.hudPlayers[3] != INVALID_PLAYER_INDEX()
        IF IS_NET_PLAYER_OK(sBoardData.hudPlayers[3], FALSE)
                
            SET_TEXT_FONT(FONT_CURSIVE)
            SET_TEXT_SCALE(0.6000, 0.6000)
            SET_TEXT_COLOUR(255, 255, 255, 255)
            SET_TEXT_CENTRE(FALSE)
            SET_TEXT_DROPSHADOW(ENUM_TO_INT(DROPSTYLE_OUTLINEONLY), 0, 0, 0, 255)
            SET_TEXT_EDGE(0, 0, 0, 0, 0)
            DISPLAY_TEXT_WITH_PLAYER_NAME(0.38, 0.6750+fRespectUpOffset, "HRD_KILLSPLYNM", GET_PLAYER_NAME(sBoardData.hudPlayers[3]), HUD_COLOUR_WHITE)
            
            SET_TEXT_FONT(FONT_CURSIVE)
            SET_TEXT_SCALE(0.6000, 0.6000)
            SET_TEXT_COLOUR(255, 255, 255, 255)
            SET_TEXT_CENTRE(FALSE)
            SET_TEXT_DROPSHADOW(ENUM_TO_INT(DROPSTYLE_OUTLINEONLY), 0, 0, 0, 255)
            SET_TEXT_EDGE(0, 0, 0, 0, 0)
            DISPLAY_TEXT_WITH_NUMBER(0.60, 0.6750+fRespectUpOffset, "HRD_KILLSPLYAM",sBoardData.iScores[3])
                
        ENDIF
    ENDIF
    
    
ENDPROC

FUNC TEXT_LABEL_15 GET_WAVE_PROGRESS_LABEL()
    TEXT_LABEL_15 tl15 = "HRD_WAVE1PROG"
	IF IS_INT_IN_RANGE(serverBD.sWaveData.iWaveCount, 1, MAX_SURVIVAL_WAVES)
		tl15 = "HRD_WAVE"
		tl15 += serverBD.sWaveData.iWaveCount
		tl15 += "PROG"
		PRINTLN("[Survival] - Using wave label: ", tl15)
	ENDIF
    
    RETURN tl15
    
ENDFUNC

PROC MAINTAIN_SPAWN_TEST_HUD()
	IF g_iTestType = 1
		DRAW_GENERIC_BIG_DOUBLE_NUMBER(iPedSpawnTested, g_FMMC_STRUCT_ENTITIES.iNumberOfPeds,"SC_PED_TEST", DEFAULT, DEFAULT, HUDORDER_EIGHTHBOTTOM)
		IF g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles > 0
			DRAW_GENERIC_BIG_DOUBLE_NUMBER(iVehicleSpawnTested, g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles,"SC_VEH_TEST", DEFAULT, DEFAULT, HUDORDER_SEVENTHBOTTOM)
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_WAVE_PROGRESS_BAR(HUDORDER eOrder)
    
    IF NOT IS_LOCAL_BIT_SET(ciLocalBool_bGotWaveProgressLabel)
    
        #IF IS_DEBUG_BUILD
        IF bDoProgressBarPrints
        PRINTLN("[Survival] - ciLocalBool_bGotWaveProgressLabel = FALSE.")
        ENDIF
        #ENDIF
        
        tl15_waveProgressLabel = "SC_WAVE"
        SET_LOCAL_BIT(ciLocalBool_bGotWaveProgressLabel)
            
    ELSE
        
        #IF IS_DEBUG_BUILD
        IF bDoProgressBarPrints
        PRINTLN("[Survival] - ciLocalBool_bGotWaveProgressLabel = TRUE.")
        ENDIF
        #ENDIF
        
		IF NOT IS_THIS_SPECTATOR_CAM_ACTIVE(g_BossSpecData.specCamData)				//If spectator not on
		OR ((NOT IS_SPECTATOR_HUD_HIDDEN())										//or spectator is on but it's not hidden or on news
			AND (g_BossSpecData.specHUDData.eCurrentFilter <> SPEC_HUD_FILTER_NEWS)
			AND (g_BossSpecData.specHUDData.eCurrentFilter <> SPEC_HUD_FILTER_GTAOTV))
            
            #IF IS_DEBUG_BUILD
            IF bDoProgressBarPrints
            PRINTLN("[Survival] - IS_SPECTATOR_HUD_HIDDEN() bit is not set - calling DRAW_GENERIC_METER().")
            ENDIF
            #ENDIF
            
            DRAW_GENERIC_METER(serverBD.iKillsThisWave, serverBD.sWaveData.iNumRequiredKills, tl15_waveProgressLabel, HUD_COLOUR_WHITE, (-1), eOrder, -1, -1, FALSE, TRUE, HUDFLASHING_NONE, 0, TRUE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, serverBD.sWaveData.iWaveCount)
            
        #IF IS_DEBUG_BUILD
        ELSE
    
            IF bDoProgressBarPrints
            PRINTLN("[Survival] - IS_SPECTATOR_HUD_HIDDEN() bit is set.")
            ENDIF
            #ENDIF
                
        ENDIF
    ENDIF
    
ENDPROC

PROC BLIP_LAST_REMAINING_ENEMY()
	
	IF DOES_ENTITY_EXIST(eiLastEnemy)
	AND NOT IS_ENTITY_ALIVE(eiLastEnemy)
		IF DOES_BLIP_EXIST(biLastEnemy)
			REMOVE_BLIP(biLastEnemy)
		ENDIF
		eiLastEnemy = NULL
		PRINTLN("BLIP_LAST_REMAINING_ENEMY - Last enemy is dead. Remove the blip!")
		EXIT
	ENDIF
	
	IF DOES_BLIP_EXIST(biLastEnemy)
	
		IF SHOULD_BLIP_BE_MANUALLY_ROTATED(biLastEnemy)
			IF IS_ENTITY_ALIVE(eiLastEnemy)
				IF IS_ENTITY_A_VEHICLE(eiLastEnemy)
					SET_BLIP_ROTATION(biLastEnemy, ROUND(GET_ENTITY_HEADING(eiLastEnemy)))
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_ENTITY_A_PED(eiLastEnemy)
			PED_INDEX piLastEnemy = GET_PED_INDEX_FROM_ENTITY_INDEX(eiLastEnemy)
			IF DOES_ENTITY_EXIST(piLastEnemy)
				IF IS_PED_IN_ANY_VEHICLE(piLastEnemy)
					BLIP_INDEX biTemp
					biTemp = GET_BLIP_FROM_ENTITY(GET_VEHICLE_PED_IS_IN(piLastEnemy))
					REMOVE_BLIP(biTemp)
					SET_BLIP_SCALE(biLastEnemy, 1.0)
				ELSE
					SET_BLIP_SCALE(biLastEnemy, 0.75)
				ENDIF
			ENDIF
		ENDIF
		
		
		EXIT
	ENDIF
	
	IF NOT HAS_ENOUGH_TIME_PASSED_ON_SUBSTAGE()
		EXIT
	ENDIF
	
	ENTITY_INDEX eiToUse
	INT iStage, j, i
	BLIP_INDEX tempBlip
	VEHICLE_INDEX viTemp
	BLIP_SPRITE blipSprite = RADAR_TRACE_AI
	FLOAT fBlipScale = 0.75
	STRING sBlipName = NULL
	IF serverBD.iNumAliveSquadEnemies > 0
		FOR iStage = 0 TO NUM_SURVIVAL_SUB_STAGES-1
			REPEAT serverBD.sWaveData.iTotalSquads j
				REPEAT serverBD.sWaveData.iNumPedsInSquad[iStage] i
		            IF NOT IS_NET_PED_INJURED(serverBD_Enemies_S.structAiSquad[j][iStage].sPed[i].netId)
		                IF NOT DOES_BLIP_EXIST(biLastEnemy)
							eiToUse = NET_TO_PED(serverBD_Enemies_S.structAiSquad[j][iStage].sPed[i].netId)
							IF serverBD_Enemies_S.structAiSquad[j][iStage].sPed[i].bHeavy
								blipSprite = RADAR_TRACE_BAT_ASSASSINATE
								sBlipName = "SC_HVY_BLP"
							ENDIF
							PRINTLN("[survival] - BLIP_LAST_REMAINING_ENEMY - Adding blip to Squad Ped")
							BREAKLOOP
						ENDIF
					ENDIF
				ENDREPEAT
			ENDREPEAT
		ENDFOR
	ELIF serverBD.iNumAliveHelis > 0
		FOR i = 0 TO MAX_SURV_HELI-1
			IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD_Enemies_V.sSurvivalHeli[i].netId)
			AND IS_NET_VEHICLE_DRIVEABLE(serverBD_Enemies_V.sSurvivalHeli[i].netId)
				IF NOT DOES_BLIP_EXIST(biLastEnemy)
					eiToUse = NET_TO_VEH(serverBD_Enemies_V.sSurvivalHeli[i].netId)
					blipSprite = GET_CORRECT_PED_BLIP_SPRITE_FOR_VEHICLE_MODEL(g_FMMC_STRUCT.sSurvivalWaveData.mnAir[iWaveToUse][i])
					fBlipScale = 1.0
					PRINTLN("[survival] - BLIP_LAST_REMAINING_ENEMY - Adding blip to Air Vehicle")
					BREAKLOOP
				ENDIF
			ENDIF
		ENDFOR
	ELIF serverBD.iNumAliveLandVehiclePeds > 0
		FOR i = 0 TO serverBD.sWaveData.iNumLandVehicles-1
			FOR j = 0 TO serverBD_Enemies_V.sSurvivalLandVehicle[i].iNumPeds-1
				IF NOT IS_NET_PED_INJURED(serverBD_Enemies_V.sSurvivalLandVehicle[i].sPed[j].netId)
		            IF NOT DOES_BLIP_EXIST(biLastEnemy)
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD_Enemies_V.sSurvivalLandVehicle[i].netId)
							viTemp = NET_TO_VEH(serverBD_Enemies_V.sSurvivalLandVehicle[i].netId)
						ENDIF
						IF DOES_ENTITY_EXIST(viTemp)
						AND IS_COMBAT_VEHICLE(viTemp)
						AND IS_VEHICLE_DRIVEABLE(viTemp)
							eiToUse = viTemp
							fBlipScale = 1.0
							ENTITY_INDEX entTempPed = NET_TO_ENT(serverBD_Enemies_V.sSurvivalLandVehicle[i].sPed[j].netId)
							IF DOES_ENTITY_EXIST(entTempPed)
								tempBlip = GET_BLIP_FROM_ENTITY(entTempPed)
								IF DOES_BLIP_EXIST(tempBlip)
									REMOVE_BLIP(tempBlip)
								ENDIF
							ENDIF
							PRINTLN("[survival] - BLIP_LAST_REMAINING_ENEMY - Adding blip to Land Combat Vehicle")
						ELSE
							eiToUse = NET_TO_PED(serverBD_Enemies_V.sSurvivalLandVehicle[i].sPed[j].netId)
							PRINTLN("[survival] - BLIP_LAST_REMAINING_ENEMY - Adding blip to Vehicle Ped")
						ENDIF
						BREAKLOOP
					ENDIF
				ENDIF
			ENDFOR
		ENDFOR
	ENDIF
	
	IF DOES_ENTITY_EXIST(eiToUse)
		tempBlip = GET_BLIP_FROM_ENTITY(eiToUse)
		REMOVE_BLIP(tempBlip)
		IF NOT DOES_BLIP_EXIST(tempBlip)
			eiLastEnemy = eiToUse
			biLastEnemy = ADD_BLIP_FOR_ENTITY(eiToUse)
			SHOW_HEIGHT_ON_BLIP(biLastEnemy, TRUE)
			IF IS_VALID_INTERIOR(sInterior.iInteriorIndex)
				SET_BLIP_SHORT_HEIGHT_THRESHOLD(biLastEnemy, TRUE)
				PRINTLN("[survival] - BLIP_LAST_REMAINING_ENEMY - Set short height threshold on last enemy blip")
			ENDIF
			IF (BlipSprite != GET_STANDARD_BLIP_ENUM_ID())
			AND BlipSprite != RADAR_TRACE_INVALID
				SET_BLIP_SPRITE(biLastEnemy, BlipSprite)
				SET_BLIP_COLOUR(biLastEnemy, BLIP_COLOUR_HUDCOLOUR_RED)
				SET_BLIP_SCALE(biLastEnemy, fBlipScale)
				IF NOT IS_STRING_NULL_OR_EMPTY(sBlipName)
					SET_BLIP_NAME_FROM_TEXT_FILE(biLastEnemy, sBlipName)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_SURVIVAL_OBJECTIVE_TEXT()
	TEXT_LABEL_15 tl15_obj = "HRD_OBJ"
	
	//Last Enemy remaining
    IF IS_ONLY_ONE_ENEMY_REMAINING()
     	tl15_obj = "HRD_OBJSING"
		BLIP_LAST_REMAINING_ENEMY()
	ELSE
		IF DOES_BLIP_EXIST(biLastEnemy)
			REMOVE_BLIP(biLastEnemy)
			PRINTLN("[survival] - MAINTAIN_SURVIVAL_OBJECTIVE_TEXT - BLIP_LAST_REMAINING_ENEMY - Removing biLastEnemy")
		ENDIF
    ENDIF
    
    Print_Objective_Text(tl15_obj)
ENDPROC

FUNC INT GET_OUT_OF_BOUNDS_TIMER_LIMIT()
//	IF IS_ENTITY_IN_DEEP_WATER(PlayerPedToUse)
//		RETURN GET_BACK_TO_HORDE_IN_WATER_TIMER
//	ENDIF
	RETURN GET_BACK_TO_HORDE_TIME_LIMIT
ENDFUNC

PROC MAINTAIN_GET_BACK_TO_SHOOT_OUT_OBJECTIVE(BOOL bAtShootout)
	
	INT iTime
	HUD_COLOURS eHudColour
	
	IF bAtShootout
		// Remove get to shootout blip.
	    REMOVE_SHOOTOUT_BLIP()
	    // Reset leave horde timer.
	    IF HAS_NET_TIMER_STARTED(stLeaveHordeTimer)
			CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_LEAVING_SURVIVAL)
	        RESET_NET_TIMER(stLeaveHordeTimer)
	    ENDIF
		STOP_SOUND(iBoundsTimerSound)
	ELSE
	    
	    // Say get back to shootout.
	    Print_Objective_Text("HRD_GOSHT")
	    // Run away from shootout timer. If away for too long, set bit to kick off horde mode.
	    IF NOT HAS_NET_TIMER_STARTED(stLeaveHordeTimer)
	        // Start the timer.
	        START_NET_TIMER(stLeaveHordeTimer)
	        
			SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_LEAVING_SURVIVAL)
	    ELSE
	        iTime = GET_ABS_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stLeaveHordeTimer)
	        
			IF iTime <= 0
				iTime = 0
			ENDIF
			
			IF (GET_OUT_OF_BOUNDS_TIMER_LIMIT()-iTime) < 10000
				eHudColour = HUD_COLOUR_RED
			ELSE
				eHudColour = HUD_COLOUR_WHITE
			ENDIF
			
			STRING stTimeTitle
			IF NOT IS_PLAYER_ON_A_PLAYLIST(localPlayer)
				stTimeTitle = "HRD_LVHRD"
			ELSE
				stTimeTitle = "HRD_LVWAVE"
			ENDIF
			INT iTimeToShow =  CLAMP_INT(GET_OUT_OF_BOUNDS_TIMER_LIMIT()-iTime, 0, 9999999)
			
			IF iBoundsTimerSound >= 0
			AND HAS_SOUND_FINISHED(iBoundsTimerSound)
				FLOAT fSeconds = iTimeToShow/1000.0
				PLAY_SOUND_FRONTEND(iBoundsTimerSound, "Out_of_Bounds", "MP_MISSION_COUNTDOWN_SOUNDSET", FALSE)
				SET_VARIABLE_ON_SOUND(iBoundsTimerSound, "Time", fSeconds)
				PRINTLN("[Survival] - SOUNDS - Out_of_Bounds playing from frontend - fSeconds: ", fSeconds)
			ENDIF
			
	    	DRAW_GENERIC_TIMER(iTimeToShow, stTimeTitle, 0, TIMER_STYLE_DONTUSEMILLISECONDS, -1, PODIUMPOS_NONE, HUDORDER_BOTTOM, FALSE, eHudColour)
			
	        // If stay away for long enough, I have abandoned horde mode.
			IF NOT IS_PLAYER_SCTV(localPlayer)
			AND NOT IS_PLAYER_SPECTATING(localPlayer)
		        IF HAS_NET_TIMER_EXPIRED(stLeaveHordeTimer, GET_OUT_OF_BOUNDS_TIMER_LIMIT())
		            Clear_Any_Objective_Text_From_This_Script()
					IF IS_FAKE_MULTIPLAYER_MODE_SET()
						g_iSurvivalEndReason = 1
					ENDIF
					IF IS_PLAYER_ON_A_PLAYLIST(localPlayer)
						PRINTLN("[Survival] - iRejoinFromSpecCamStage = 0, call 20.")
						SET_BIT(playerBD[iLocalPart].iBitset, BITSET_PLAYER_WENT_TO_SPEC_CAM_FOR_OUT_OF_BOUNDS)
						SET_EARLY_END_SURVIVAL_SPECTATE(ci_SpectatorState_WAIT)
			        	SET_CLIENT_HORDE_STAGE(eHORDESTAGE_SPECTATING)
					ELSE
			            SET_CLIENT_HORDE_STAGE(eHORDESTAGE_REWARDS_AND_FEEDACK)
			            SET_BIT(playerBD[iLocalPart].iBitset, BITSET_PLAYER_ABANDONED_HORDE)       
					ENDIF
		        ENDIF
			ENDIF
	    ENDIF
	ENDIF
	
ENDPROC

PROC PLAY_COUNTDOWN_BEEP()
    
    IF HAS_SOUND_FINISHED(-1)
		IF NOT IS_SOUND_ID_VALID(iCountDownSound)
			iCountDownSound = GET_SOUND_ID()
		ELSE
			PRINTLN("[Survival] PLAY_COUNTDOWN_BEEP - SOUNDS - iCountDownAudioStage - ", iCountDownAudioStage)
			IF iCountDownAudioStage = 9
				PLAY_SOUND_FRONTEND(iCountDownSound,"Countdown_Tick_Last", GET_SOUND_SET_FOR_SURVIVAL())
				PRINTLN("[Survival] PLAY_COUNTDOWN_BEEP - SOUNDS - Countdown_Tick_Last playing from Frontend")
			ELSE
				PLAY_SOUND_FRONTEND(iCountDownSound,"Countdown_Tick", GET_SOUND_SET_FOR_SURVIVAL())
				PRINTLN("[Survival] PLAY_COUNTDOWN_BEEP - SOUNDS - Countdown_Tick playing from Frontend")
			ENDIF
		ENDIF
    ENDIF
    
ENDPROC

PROC MAINTAIN_COUNTDOWN_AUDIO(SCRIPT_TIMER &sTimer)
    
    INT iTime = (GET_DELAY_BETWEEN_WAVES_LENGTH() - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(sTimer))
    
	IF iCountDownAudioStage != -1
	AND iCountDownAudioStage <= 9
		IF iTime <= (10000 - iCountDownAudioStage*1000)
			PLAY_COUNTDOWN_BEEP()
            iCountDownAudioStage++
		ENDIF
	ELSE
		CLEAR_SOUND_ID(iCountDownSound)
	ENDIF
ENDPROC

PROC MAINTAIN_NEXT_WAVE_HUD(HUDORDER eOrder, BOOL bdrawObj = TRUE)
    
    TEXT_LABEL_15 tl15_waveText
    TEXT_LABEL_15 tl15_objective
	BOOL bPlayerAtShootout = NOT IS_PED_OUT_OF_BOUNDS(PlayerPedToUse)
	
	// Tell player to get back to the shoot out if not near it.
	MAINTAIN_GET_BACK_TO_SHOOT_OUT_OBJECTIVE(bPlayerAtShootout)
	
	INT iTime
	
    IF HAS_NET_TIMER_STARTED(serverBD.stNextWaveDelay)
	
		iTime = (GET_DELAY_BETWEEN_WAVES_LENGTH()-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.stNextWaveDelay))
		
        MAINTAIN_COUNTDOWN_AUDIO(serverBD.stNextWaveDelay)
        IF NOT HAS_NET_TIMER_EXPIRED(serverBD.stNextWaveDelay, GET_DELAY_BETWEEN_WAVES_LENGTH())
			IF (serverBD.sWaveData.iWaveCount <= 1)
                tl15_waveText = "HRD_FIRST"
                tl15_objective = "HRD_PREP0"
            ELSE
                tl15_waveText = "HRD_NEXT"
                tl15_objective = "HRD_PREP1"
            ENDIF
            IF bdrawObj
			AND bPlayerAtShootout
                Print_Objective_Text(tl15_objective)
            ENDIF
			DRAW_GENERIC_TIMER(iTime, tl15_waveText, 0, TIMER_STYLE_DONTUSEMILLISECONDS, -1, PODIUMPOS_NONE, eOrder)
        ELSE
            Clear_Any_Objective_Text_From_This_Script()
        ENDIF
    ENDIF
ENDPROC

PROC MAINTAIN_SPECTATOR_HORDE_HUD()
	BOOL bSpecTargetOutBounds
    IF NOT g_bCelebrationScreenIsActive 
		INT iHordeSpecHudDisplayState // 0 = fightingstage, 1 = delay for enxt wave stage, 2 = get back to shoot out.
		
		IF GET_SERVER_HORDE_STAGE() = eHORDESTAGE_FIGHTING
			iHordeSpecHudDisplayState = 0
			IF PlayerToUse !=  INVALID_PLAYER_INDEX()
				bSpecTargetOutBounds = IS_PED_OUT_OF_BOUNDS(PlayerPedToUse)
				IF bSpecTargetOutBounds
					iHordeSpecHudDisplayState = 2
				ENDIF
			ENDIF
		ELIF GET_SERVER_HORDE_STAGE() = eHORDESTAGE_DELAY_BEFORE_NEXT_WAVE
			iHordeSpecHudDisplayState = 1
		ENDIF
		
		SWITCH iHordeSpecHudDisplayState
			
			CASE 0
			    MAINTAIN_WAVE_PROGRESS_BAR(HUDORDER_BOTTOM) // Wave progress bar.
				MAINTAIN_GET_BACK_TO_SHOOT_OUT_OBJECTIVE(TRUE)
				IF !bSpecTargetOutBounds
					MAINTAIN_SURVIVAL_OBJECTIVE_TEXT()
				ENDIF
			BREAK
			
			CASE 1
				CLEAR_LOCAL_BIT(ciLocalBool_bGotWaveProgressLabel)
			   	MAINTAIN_NEXT_WAVE_HUD(HUDORDER_BOTTOM) // Next wave countdown.
				MAINTAIN_GET_BACK_TO_SHOOT_OUT_OBJECTIVE(TRUE)
			BREAK
			
			CASE 2
				MAINTAIN_GET_BACK_TO_SHOOT_OUT_OBJECTIVE(FALSE) // Tell player to get back to the shoot out if not near it.
			BREAK
			
		ENDSWITCH
		
	ENDIF
	
ENDPROC

PROC PROCESS_PARTICPANT_ABANDONDED_SURVIVAL_TICKER(INT iParticipant, PARTICIPANT_INDEX participantId, BOOL bPartActive)
	IF NOT IS_BIT_SET(iPlayerAbandondedSurvivalTickerBit, iParticipant)
		IF IS_BIT_SET(playerBD[iParticipant].iBitset, BITSET_PLAYER_ABANDONED_HORDE)
			participantId = INT_TO_PARTICIPANTINDEX(iParticipant)
			IF bPartActive
				PRINT_TICKER_WITH_PLAYER_NAME("LFT_SRVL", NETWORK_GET_PLAYER_INDEX(participantId))
				SET_BIT(iPlayerAbandondedSurvivalTickerBit, iParticipant)
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_BIT_SET(playerBD[iParticipant].iBitset, BITSET_PLAYER_ABANDONED_HORDE)
			CLEAR_BIT(iPlayerAbandondedSurvivalTickerBit, iParticipant)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_KICK_LEECHERS_REMINDER_TICKER()
	
	INT i, iFightingPart
	PARTICIPANT_INDEX partId
	
	IF serverBD.sFightingParticipantsList.iNumFightingParticipants = 1
		EXIT
	ENDIF
	
	// Loop through fighting participants and see if they have hit any of the potential leecher triggers.
	REPEAT MAX_NUMBER_HORDE_FIGHTING_PARTICIPANTS i
		IF serverBD.sFightingParticipantsList.iParticipant[i] != (-1)
			
			iFightingPart = serverBD.sFightingParticipantsList.iParticipant[i]
			
			IF NOT IS_BIT_SET(serverBD.sFightingParticipantsList.iPotentialFighterBitset, iFightingPart)
				IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_NATIVE(PARTICIPANT_INDEX, iFightingPart))
					
					partId = INT_TO_NATIVE(PARTICIPANT_INDEX, iFightingPart)
					
					IF GET_PARTICIPANT_IS_ANY_MIGHT_NOT_BE_PLAYING_FLAG_SET(iFightingPart)
						IF NOT IS_BIT_SET(iPrintedLeecherRemindeTickerForfightingParticipant, iFightingPart)
							IF iFightingPart != iLocalPart
								PRINT_TICKER_WITH_PLAYER_NAME("LEECHRMDR", NETWORK_GET_PLAYER_INDEX(partId))
								PRINTLN("[Survival] - printing ticker LEECHRMDR for participant ", iFightingPart)
							ENDIF
							SET_BIT(iPrintedLeecherRemindeTickerForfightingParticipant, iFightingPart)
							PRINTLN("[Survival] - setting bit iPrintedLeecherRemindeTickerForfightingParticipant for participant ", iFightingPart)
						ENDIF
					ELSE
						
						IF IS_BIT_SET(iPrintedLeecherRemindeTickerForfightingParticipant, iFightingPart)
							PRINTLN("[Survival] - clearing bit iPrintedLeecherRemindeTickerForfightingParticipant for participant ", iFightingPart)
							CLEAR_BIT(iPrintedLeecherRemindeTickerForfightingParticipant, iFightingPart)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///   (SERVER ONLY: Maintains the leaderboard data)
PROC SERVER_MAINTAIN_HORDE_LEADERBOARD_FOR_PARTICIPANT(THE_LEADERBOARD_STRUCT& leaderBoard[], INT iParticipant)
    
    INT i
    THE_LEADERBOARD_STRUCT EmptyLeaderBoardStruct
    THE_LEADERBOARD_STRUCT StoredLeaderBoardStruct
    THE_LEADERBOARD_STRUCT TempLeaderBoardStruct

    // remove this participant from leader board before we re-add them.
    BOOL bParticipantRemoved = FALSE
    REPEAT NUM_NETWORK_PLAYERS i 
		IF NOT IS_BIT_SET(leaderBoard[i].iLbdBitSet, ciLBD_BIT_INITIALISE)
            leaderBoard[i].playerID                 = INVALID_PLAYER_INDEX()
            leaderBoard[i].iParticipant     = -1
			SET_BIT(leaderBoard[i].iLbdBitSet, ciLBD_BIT_INITIALISE)
        ENDIF

        IF NOT (bParticipantRemoved)
            IF (leaderBoard[i].iParticipant = iParticipant)
                leaderBoard[i] = EmptyLeaderBoardStruct
                bParticipantRemoved = TRUE
            ENDIF
        ELSE
            TempLeaderBoardStruct = leaderBoard[i]
            leaderBoard[i-1] =      TempLeaderBoardStruct
            leaderBoard[i] = EmptyLeaderBoardStruct
        ENDIF
    ENDREPEAT
	
    INT iCurrentRank = 1
    // add this participant to the leader board
    BOOL bParticipantInserted = FALSE
    REPEAT NUM_NETWORK_PLAYERS i 
        IF NOT (bParticipantInserted)
            IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_NATIVE(PARTICIPANT_INDEX, iParticipant))
                PLAYER_INDEX PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_NATIVE(PARTICIPANT_INDEX, iParticipant))
                IF IS_NET_PLAYER_OK(PlayerId, FALSE, FALSE)
                    IF NOT IS_PLAYER_SCTV(PlayerId)
						IF IS_BIT_SET(playerBD[iParticipant].iBitset, BITSET_PLAYER_DID_INITIAL_FIGHTER_STATUS_SETUP)
	                        IF NOT IS_BIT_SET(playerBD[iParticipant].iBitset, BITSET_PLAYER_I_WENT_STRAIGHT_TO_SPEC_CAM)
	                            BOOL bGoodToInsert = TRUE
	                            // if there is already a someone there?
	                            IF (leaderBoard[i].playerID <> playerID)
	                                // check that person is still active and compare their position
	                                IF NATIVE_TO_INT(leaderBoard[i].playerID) <> -1
	                                    IF IS_NET_PLAYER_OK(leaderBoard[i].playerID, FALSE, FALSE)
	                                    AND NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_NATIVE(PARTICIPANT_INDEX,leaderBoard[i].iParticipant))                                                                     
	                                        // Who has most cash?
	                                        IF (leaderBoard[i].iKills > playerBD[iParticipant].iMyKills)
												iCurrentRank++
	                                            bGoodToInsert = FALSE
	                                        ENDIF
	                                    ENDIF
	                                ENDIF
	                            ENDIF
	                    
	                            // are we still good to insert this person into the leader board?
	                            IF (bGoodToInsert)
	                                StoredLeaderBoardStruct = leaderBoard[i]
	                                leaderBoard[i].playerID = PlayerId
	                                leaderBoard[i].iParticipant = iParticipant                                                      
	                                leaderBoard[i].iKills = playerBD[iParticipant].iMyKillsThisWave
	                                leaderBoard[i].iDeaths = playerBD[iParticipant].iDeathsThisWave
	                                leaderBoard[i].iHeadshots = 0
	                                leaderBoard[i].iJobCash = 0
	                                leaderBoard[i].iJobXP = playerBD[iParticipant].iXpGained
									
	                                leaderBoard[i].iBadgeRank = GlobalplayerBD_FM[NATIVE_TO_INT(PlayerId)].scoreData.iRank

	                                // NOT CONVINCED THIS RANK BIT IS WORKING CORRECTLY ***********************
	                                leaderBoard[i].iRank = iCurrentRank
	                        
	                                bParticipantInserted = TRUE
	                            ENDIF
	                        ENDIF
						ENDIF
                    ENDIF
                ENDIF
            ENDIF
        ELSE
            // shift everyone else in the leaderboard down
            TempLeaderBoardStruct = leaderBoard[i]
            leaderBoard[i] = StoredLeaderBoardStruct
            StoredLeaderBoardStruct = TempLeaderBoardStruct
        ENDIF
    ENDREPEAT
ENDPROC

PROC PROCESS_LEADERBOARD()
	
    INT iSlotCounter
    INT i, iSpectatorNumber
    PLAYER_INDEX PlayerId
	STRING sSpectatorName	
	BOOL bDraw[FMMC_MAX_TEAMS]
    
	//check if correct, add spectators
	INT iNumberOfLeaderboardPlayers = serverBD.sFightingParticipantsList.iNumFightingParticipants + serverBD.sFightingParticipantsList.iNumPotentialfightingParticipants
	
    REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
		DEFINE_FREEMODE_NAMES_ARRAY(tl63LeaderboardPlayerNames, i)
		SERVER_MAINTAIN_HORDE_LEADERBOARD_FOR_PARTICIPANT(HordeLeaderboard, i)
    ENDREPEAT
	DEFINE_LBD_NAMES_ARRAY(lbdVars, tl63LeaderboardPlayerNames, TRUE)
	
	TRACK_LBD_ROW_FOR_SCROLL(lbdVars, 0, iNumberOfLeaderboardPlayers, FALSE)
	
	PROCESS_LBD_SCROLL_LOGIC(lbdVars, timeScrollDelay, iLBPlayerSelection, 0, 0, iNumberOfLeaderboardPlayers, 0, FALSE)
	
	IF g_i_ActualLeaderboardPlayerSelected = -1
		g_i_ActualLeaderboardPlayerSelected = 0
	ENDIF
	
	IF HAVE_ALL_COMMON_LEADERBOARD_ELEMENTS_LOADED(LeaderboardPlacement)
    
	    // Draw the leaderboard
		RENDER_LEADERBOARD(lbdVars, LeaderboardPlacement, SUB_HORDE, FALSE, GET_LEADERBOARD_TITLE(SUB_HORDE, g_FMMC_STRUCT.tl63MissionName), GET_NUM_LBD_COLUMNS(SUB_HORDE), DEFAULT, DEFAULT, DEFAULT, FALSE)
		
		// Check for player card press
		
		IF g_i_ActualLeaderboardPlayerSelected > -1
		AND g_i_ActualLeaderboardPlayerSelected < 32
		AND IS_BIT_SET(iSpectatorBit, g_i_ActualLeaderboardPlayerSelected)
			HANDLE_SPECTATOR_PLAYER_CARDS(lbdVars, iSavedSpectatorRow)
		ELSE
			BOOL bJobSpectating = (GET_SPECTATOR_HUD_STAGE(g_BossSpecData.specHUDData) = SPEC_HUD_STAGE_LEADERBOARD)
			HANDLE_THE_PLAYER_CARDS(HordeLeaderboard, lbdVars, iSavedRow, bJobSpectating)
		ENDIF
		
		GRAB_SPECTATOR_PLAYER_SELECTED(HordeLeaderboard, iSavedRow)
	    
	    REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
		
			// Draw spectators
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))				
				PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
				
				POPULATE_HEADSHOTS_LBD(PlayerId)
				
				IF IS_NET_PLAYER_OK(PlayerId,FALSE)
					INT iSpectatorRow = GET_SPECTATOR_ROW(FALSE, serverBD.sFightingParticipantsList.iNumFightingParticipants, 0, iSpectatorNumber)
					IF IS_BIT_SET(playerBD[i].iBitset, BITSET_PLAYER_I_WENT_STRAIGHT_TO_SPEC_CAM)
						sSpectatorName = GET_PLAYER_NAME(playerId)
						DRAW_SPECTATOR_ROW(lbdVars, LeaderboardPlacement, SUB_HORDE, playerId, sSpectatorName, GlobalplayerBD_FM[NATIVE_TO_INT(PlayerId)].scoreData.iRank, GET_SPECTATOR_ROW(FALSE, serverBd.sFightingParticipantsList.iNumFightingParticipants, 1, iSpectatorNumber), i)
						iSavedSpectatorRow[NATIVE_TO_INT(PlayerId)] = iSpectatorRow	
						SET_BIT(iSpectatorBit, iSpectatorRow)
						iSpectatorNumber ++
					ENDIF
				ENDIF
			ENDIF
			
	        IF NATIVE_TO_INT(HordeLeaderboard[i].playerID) <> -1
	            IF IS_NET_PLAYER_OK(HordeLeaderboard[i].playerID, FALSE)
					
					iSavedRow[NATIVE_TO_INT(HordeLeaderboard[i].playerID)] = iSlotCounter
					
					SET_LBD_SELECTION_TO_PLAYER_POS(lbdVars, 
						HordeLeaderboard[i].playerID, 
						iLBPlayerSelection, 
						iSlotCounter, 
						iNumberOfLeaderboardPlayers)
					
					POPULATE_COLUMN_LBD(lbdVars,
										LeaderboardPlacement,
										SUB_HORDE,
										LBD_VOTE_NOT_USED,
										tl63LeaderboardPlayerNames,
										GET_NUM_LBD_COLUMNS(SUB_HORDE),
										GET_KILL_DEATH_RATIO(HordeLeaderboard[i].iKills, HordeLeaderboard[i].iDeaths),
										HordeLeaderboard[i].iKills,
										HordeLeaderboard[i].iDeaths,
										HordeLeaderboard[i].iJobXP,
										0,
										0,
										0,
										0,
										0,
										iSlotCounter,
										i+1,
										HordeLeaderboard[i].iParticipant,
										HordeLeaderboard[i].iBadgeRank, 
										HordeLeaderboard[i].playerID, 
										FALSE,
										bDraw[0],
										-1,
										-1)
	                iSlotCounter++
	            ENDIF
	        ENDIF
	    ENDREPEAT
	ENDIF
ENDPROC

PROC MAINTAIN_SPECTATOR_HORDE_CAM()
    // Turn on the spectator camera.
    IF NOT IS_THIS_SPECTATOR_CAM_ACTIVE(g_BossSpecData.specCamData)
        IF IS_SAFE_TO_TURN_ON_SPECTATOR_CAM()
			SET_GAME_STATE_ON_DEATH(AFTERLIFE_SET_SPECTATORCAM)
			ACTIVATE_SPECTATOR_CAM( g_BossSpecData, SPEC_MODE_PLAYERS_STAY_DEAD_EVENT, NULL, ASCF_HIDE_LOCAL_PLAYER|ASCF_QUIT_SCREEN|ASCF_ONLY_FIND_TARGET_IF_PLAYER_LIST_POPULATED )
        ENDIF
	ELSE
    	MAINTAIN_SPECTATOR_HORDE_HUD()
	    IF IS_BIT_SET(MPSpecGlobals.iBitset, GLOBAL_SPEC_BS_READY_FOR_LEADERBOARD)
	        PROCESS_LEADERBOARD()
		ELSE
			SET_ON_LBD_GLOBAL(FALSE)
	    ENDIF
    ENDIF
ENDPROC

PROC DO_THE_MICS(DPAD_VARS &sDPadVars, SCALEFORM_INDEX &siMovie)
	INT i
	INT iIcon
	PLAYER_INDEX PlayerId
	REPEAT NUM_NETWORK_PLAYERS i
		PlayerId = INT_TO_PLAYERINDEX(i)
		IF PlayerId <> INVALID_PLAYER_INDEX()
			IF IS_NET_PLAYER_OK(playerId, FALSE)
				IF sDPadVars.sDpadMics[i].iRow <> -1
					IF NETWORK_IS_PLAYER_A_PARTICIPANT(playerId)
					AND NETWORK_IS_PARTICIPANT_ACTIVE(NETWORK_GET_PARTICIPANT_INDEX(playerId))				
						iIcon = GET_DPAD_PLAYER_VOICE_ICON(sDPadVars.sDpadMics[i].iVoiceChatState)
						SET_PLAYER_MIC(siMovie, sDPadVars.sDpadMics[i].iRow, iIcon, GlobalplayerBD_FM[i].scoreData.iRank) 	// SET_ICON
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC DRAW_DPAD_LEADERBOARD(DPAD_VARS &sDpadVars)

    INT i, iRank, iPlayer
    INT iSlotCounter
	TEXT_LABEL_15 tlCrewTag
	STRING strHeadshotTxd
	PEDHEADSHOT_ID pedHeadshot
	PLAYER_INDEX playerId
	PARTICIPANT_INDEX partId
	
	g_i_NumDpadLbdPlayers = 0
	
    REPEAT MAX_NUMBER_HORDE_FIGHTING_PARTICIPANTS i
        
		IF (eMiniLeaderBoardData.miniLeaderBoardStruct[i].playerID != INVALID_PLAYER_INDEX())
			
            IF IS_NET_PLAYER_OK(eMiniLeaderBoardData.miniLeaderBoardStruct[i].playerID, FALSE)
				
				partId = NETWORK_GET_PARTICIPANT_INDEX(eMiniLeaderBoardData.miniLeaderBoardStruct[i].playerID)
				
				IF NOT IS_BIT_SET(playerBD[NATIVE_TO_INT(partId)].iBitset, BITSET_PLAYER_ABANDONED_HORDE)
					
					playerId = eMiniLeaderBoardData.miniLeaderBoardStruct[i].playerID
				
					iPlayer = NATIVE_TO_INT(playerId)
					
					IF playerId = localPlayer
						sDpadVars.iPlayersRow = iSlotCounter
					ENDIF
					       
					sDpadVars.iScore = eMiniLeaderBoardData.miniLeaderBoardStruct[i].iKills
					sDpadVars.tl63LeaderboardPlayerNames = tl63LeaderboardPlayerNames[i]
					
					IF IS_ENTITY_DEAD(GET_PLAYER_PED(playerId))
						SET_BIT(sDpadVars.iDpadBitSet, ciDPAD_PLAYER_DEAD)
					ELSE
						CLEAR_BIT(sDpadVars.iDpadBitSet, ciDPAD_PLAYER_DEAD)
					ENDIF
					
					// Rank
					iRank = GlobalplayerBD_FM[iPlayer].scoreData.iRank
					
					// Attempt to get player headshot
					pedHeadshot = Get_HeadshotID_For_Player(playerId)
					strHeadshotTxd = ""
					IF pedHeadshot <> NULL
						
						strHeadshotTxd = GET_PEDHEADSHOT_TXD_STRING(pedHeadshot)
						PRINTLN("[Survival] - [CS_DPAD] OK strHeadshotTxd = ", strHeadshotTxd)
					ENDIF
	
					PRINTLN("[Survival] - [CS_DPAD] Survival about to call POPULATE_NEW_FREEMODE_DPAD_ROWS with row = ", iSlotCounter, " player name = ", sDpadVars.tl63LeaderboardPlayerNames, " iPlayer = ", iPlayer)
	
					POPULATE_NEW_FREEMODE_DPAD_ROWS(SUB_HORDE, eMiniLeaderBoardData.siMovie, sDpadVars, iSlotCounter, tlCrewTag, strHeadshotTxd, iRank, eMiniLeaderBoardData.miniLeaderBoardStruct[i].iKills, FRIENDLY_DM_COLOUR(), DISPLAY_HEADSHOT, -1)
					
					// Store out mics row
					sDpadVars.sDpadMics[iPlayer].iRow =	iSlotCounter		
					IF MAINTAIN_DPAD_VOICE_CHAT(sDPadVars)
						DO_THE_MICS(sDpadVars, eMiniLeaderBoardData.siMovie)
					ENDIF
					
					g_i_NumDpadLbdPlayers ++															
	                iSlotCounter++
				ENDIF
            ENDIF
		ENDIF
    ENDREPEAT
ENDPROC

FUNC BOOL HAS_PLAYER_ALREADY_BEEN_ASSIGNED_HUD_PLAYER_ARRAY_INDEX(STRUCT_HORDE_BOARD_DATA &sBoardData, PLAYER_INDEX playerId)
    
    INT i
    
    REPEAT MAX_NUMBER_HORDE_FIGHTING_PARTICIPANTS i
      	IF sBoardData.hudPlayers[i] = playerId
        	RETURN TRUE
      	ENDIF
    ENDREPEAT
    
    RETURN FALSE
    
ENDFUNC

PROC POPULATE_HUD_PLAYER_ARRAY(STRUCT_HORDE_BOARD_DATA &sBoardData, BOOL bDoDebugPrints = FALSE, BOOL bTotalModeScore = FALSE)
    
    INT i, j
    INT ilow1 = -1
    INT ihigh1 = -1
    INT imiddle1 = -1
    INT ilow2 = -1
    INT ihigh2 = -1
    INT ilowest = -1
    INT ihighest = -1
    INT imiddle2 = -1
    
    PARTICIPANT_INDEX partId
    PLAYER_INDEX playerId
    
    REPEAT MAX_NUMBER_HORDE_FIGHTING_PARTICIPANTS i
        sBoardData.hudPlayers[i] = INVALID_PLAYER_INDEX()
        IF bDoDebugPrints
            PRINTLN("[Survival] - [Hud Player Array] - sBoardData.hudPlayersindex ", i, " = ", NATIVE_TO_INT(sBoardData.hudPlayers[i]))
            PRINTLN("[Survival] - [Hud Player Array] - sBoardData.iScores index ", i, " = ", sBoardData.iScores[i])
        ENDIF
    ENDREPEAT
    
    IF sBoardData.iScores[0] < sBoardData.iScores[1] 
	    ilow1 = sBoardData.iScores[0]
	    ihigh1 = sBoardData.iScores[1]
        IF bDoDebugPrints
            PRINTLN("[Survival] - [Hud Player Array] - sBoardData.iScores[0] < sBoardData.iScores[1]:")
            PRINTLN("[Survival] - [Hud Player Array] - ilow1 = sBoardData.iScores[0]")
            PRINTLN("[Survival] - [Hud Player Array] - ihigh1 = sBoardData.iScores[1]")
        ENDIF
    ELSE
	    ilow1 = sBoardData.iScores[1]
	    ihigh1 = sBoardData.iScores[0]
        IF bDoDebugPrints
            PRINTLN("[Survival] - [Hud Player Array] - sBoardData.iScores[0] >= sBoardData.iScores[1]:")
            PRINTLN("[Survival] - [Hud Player Array] - ilow1 = sBoardData.iScores[1]")
            PRINTLN("[Survival] - [Hud Player Array] - ihigh1 = sBoardData.iScores[0]")
        ENDIF
   ENDIF
    
    IF sBoardData.iScores[2] < sBoardData.iScores[3] 
	    ilow2 = sBoardData.iScores[2]
	    ihigh2 = sBoardData.iScores[3]
        IF bDoDebugPrints
            PRINTLN("[Survival] - [Hud Player Array] - sBoardData.iScores[2] < sBoardData.iScores[3]:")
           	PRINTLN("[Survival] - [Hud Player Array] - ilow2 = sBoardData.iScores[2]")
            PRINTLN("[Survival] - [Hud Player Array] - ihigh2 = sBoardData.iScores[3]")
        ENDIF
    ELSE
	    ilow2 = sBoardData.iScores[3]
	    ihigh2 = sBoardData.iScores[2]
        IF bDoDebugPrints
            PRINTLN("[Survival] - [Hud Player Array] - sBoardData.iScores[2] >= sBoardData.iScores[3]:")
            PRINTLN("[Survival] - [Hud Player Array] - ilow2 = sBoardData.iScores[3]")
            PRINTLN("[Survival] - [Hud Player Array] - ihigh2 = sBoardData.iScores[2]")
        ENDIF
   ENDIF
    
    IF ilow1 < ilow2
	    ilowest = ilow1
	    imiddle1 = ilow2
        IF bDoDebugPrints
            PRINTLN("[Survival] - [Hud Player Array] - ilow1 < ilow2:")
            PRINTLN("[Survival] - [Hud Player Array] - ilowest = ilow1")
           	PRINTLN("[Survival] - [Hud Player Array] - imiddle1 = ilow2")
        ENDIF
    ELSE
	    ilowest = ilow2
	    imiddle1 = ilow1
        IF bDoDebugPrints
            PRINTLN("[Survival] - [Hud Player Array] - ilow1 >= ilow2:")
            PRINTLN("[Survival] - [Hud Player Array] - ilowest = ilow2")
            PRINTLN("[Survival] - [Hud Player Array] - imiddle1 = ilow1")
        ENDIF
    ENDIF
    
    IF ihigh1 > ihigh2
    	ihighest = ihigh1
   		imiddle2 = ihigh2
        IF bDoDebugPrints
            PRINTLN("[Survival] - [Hud Player Array] - ihigh1 > ihigh2:")
            PRINTLN("[Survival] - [Hud Player Array] - ihighest = ihigh1")
            PRINTLN("[Survival] - [Hud Player Array] - imiddle2 = ihigh2")
        ENDIF
    ELSE
	    ihighest = ihigh2
	    imiddle2 = ihigh1
        IF bDoDebugPrints
            PRINTLN("[Survival] - [Hud Player Array] - ihigh1 <= ihigh2:")
            PRINTLN("[Survival] - [Hud Player Array] - ihighest = ihigh2")
            PRINTLN("[Survival] - [Hud Player Array] - imiddle2 = ihigh1")
        ENDIF
    ENDIF
    
    REPEAT MAX_NUMBER_HORDE_FIGHTING_PARTICIPANTS j
	
        IF ( serverBD.sFightingParticipantsList.iParticipant[j] != (-1) )
            i = serverBD.sFightingParticipantsList.iParticipant[j]
            IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
				IF NOT IS_BIT_SET(serverBD.sFightingParticipantsList.iPotentialFighterBitset, serverBD.sFightingParticipantsList.iParticipant[j])
				OR NOT bTotalModeScore
                	partId = INT_TO_PARTICIPANTINDEX(i)
					IF IS_BIT_SET(playerBD[i].iBitset, BITSET_PLAYER_DID_INITIAL_FIGHTER_STATUS_SETUP)
		                IF NOT IS_BIT_SET(playerBD[i].iBitset, BITSET_PLAYER_I_WENT_STRAIGHT_TO_SPEC_CAM)
		                    IF GET_CLIENT_HORDE_STAGE(partId) != eHORDESTAGE_SCTV
		                        IF IS_NET_PLAYER_OK(NETWORK_GET_PLAYER_INDEX(partId), FALSE)
		                            playerId = NETWORK_GET_PLAYER_INDEX(partId)
		                            IF GET_PARTICIPANT_SCORE(i, bTotalModeScore) = ihighest
		                                IF bDoDebugPrints
		                              	  PRINTLN("[Survival] - [Hud Player Array] - participant ", i, " GET_PARTICIPANT_SCORE = ", ihighest, " = ihighest")
		                                ENDIF
		                                IF sBoardData.hudPlayers[0] = INVALID_PLAYER_INDEX()
		                                    IF bDoDebugPrints
		                                   		PRINTLN("[Survival] - [Hud Player Array] - sBoardData.hudPlayers[0] = INVALID_PLAYER_INDEX()")
		                                    ENDIF
		                                    IF NOT HAS_PLAYER_ALREADY_BEEN_ASSIGNED_HUD_PLAYER_ARRAY_INDEX(sBoardData, playerId)
		                                        IF bDoDebugPrints
		                                      		PRINTLN("[Survival] - [Hud Player Array] - participant not been assigned slot in player array already, setting to sBoardData.hudPlayers[0].")
		                                  		ENDIF
												sBoardData.hudPlayers[0] = playerId
												sBoardData.iScores[0] = ihighest
		                                    ENDIF
		                                ENDIF
		                            ENDIF
									
		                            IF GET_PARTICIPANT_SCORE(i, bTotalModeScore) = imiddle1
		                                IF bDoDebugPrints
		                                	PRINTLN("[Survival] - [Hud Player Array] - participant ", i, " playerBD[i].iMyKillsThisWave = ", playerBD[i].iMyKillsThisWave, " = imiddle1")
		                                ENDIF
		                                IF imiddle1 > imiddle2
		                                    IF bDoDebugPrints
		                                    	PRINTLN("[Survival] - [Hud Player Array] - imiddle1 > imiddle2.")
		                                    ENDIF
		                                    IF sBoardData.hudPlayers[1] = INVALID_PLAYER_INDEX()
		                                        IF bDoDebugPrints
		                                        	PRINTLN("[Survival] - [Hud Player Array] - sBoardData.hudPlayers[1] = INVALID_PLAYER_INDEX()")
		                                        ENDIF
		                                        IF NOT HAS_PLAYER_ALREADY_BEEN_ASSIGNED_HUD_PLAYER_ARRAY_INDEX(sBoardData, playerId)
		                                            IF bDoDebugPrints
			                                            PRINTLN("[Survival] - [Hud Player Array] - participant not been assigned slot in player array already, setting to sBoardData.hudPlayers[1].")
			                                  		ENDIF
		                                            sBoardData.hudPlayers[1] = playerId
		                                            sBoardData.iScores[1] = imiddle1
		                                        ENDIF
		                                    ENDIF
		                                ELSE
		                                    IF bDoDebugPrints
		                                    	PRINTLN("[Survival] - [Hud Player Array] - imiddle1 <= imiddle2.")
		                                    ENDIF
		                                    IF sBoardData.hudPlayers[2] = INVALID_PLAYER_INDEX()
		                                        IF bDoDebugPrints
		                                        	PRINTLN("[Survival] - [Hud Player Array] - sBoardData.hudPlayers[2] = INVALID_PLAYER_INDEX()")
		                                        ENDIF
		                                        IF NOT HAS_PLAYER_ALREADY_BEEN_ASSIGNED_HUD_PLAYER_ARRAY_INDEX(sBoardData, playerId)
		                                            IF bDoDebugPrints
		                                            	PRINTLN("[Survival] - [Hud Player Array] - participant not been assigned slot in player array already, setting to sBoardData.hudPlayers[2].")
		                           					ENDIF
		                                            sBoardData.hudPlayers[2] = playerId
		                                            sBoardData.iScores[2] = imiddle1
		                                        ENDIF
		                                    ENDIF
		                                ENDIF
		                            ENDIF
									
		                            IF GET_PARTICIPANT_SCORE(i, bTotalModeScore) = imiddle2
		                                IF bDoDebugPrints
		                                	PRINTLN("[Survival] - [Hud Player Array] - participant ", i, " playerBD[i].iMyKillsThisWave = ", playerBD[i].iMyKillsThisWave, " = imiddle2")
		                                ENDIF
		                                IF imiddle1 > imiddle2
		                                    IF bDoDebugPrints
		                                    	PRINTLN("[Survival] - [Hud Player Array] - imiddle1 > imiddle2.")
		                                    ENDIF
		                                    IF sBoardData.hudPlayers[2] = INVALID_PLAYER_INDEX()
		                                        IF bDoDebugPrints
		                                        	PRINTLN("[Survival] - [Hud Player Array] - sBoardData.hudPlayers[2] = INVALID_PLAYER_INDEX()")
		                                        ENDIF
		                                        IF NOT HAS_PLAYER_ALREADY_BEEN_ASSIGNED_HUD_PLAYER_ARRAY_INDEX(sBoardData, playerId)
		                                            IF bDoDebugPrints
		                                            	PRINTLN("[Survival] - [Hud Player Array] - participant not been assigned slot in player array already, setting to sBoardData.hudPlayers[2].")
		                            				ENDIF
		                                            sBoardData.hudPlayers[2] = playerId
		                                            sBoardData.iScores[2] = imiddle2
		                                        ENDIF
		                                    ENDIF
		                            	ELSE
		                                    IF bDoDebugPrints
		                                    	PRINTLN("[Survival] - [Hud Player Array] - imiddle1 <= imiddle2.")
		                                    ENDIF
		                                    IF sBoardData.hudPlayers[1] = INVALID_PLAYER_INDEX()
		                                        IF bDoDebugPrints
		                                        	PRINTLN("[Survival] - [Hud Player Array] - sBoardData.hudPlayers[1] = INVALID_PLAYER_INDEX()")
		                                        ENDIF
		                                        IF NOT HAS_PLAYER_ALREADY_BEEN_ASSIGNED_HUD_PLAYER_ARRAY_INDEX(sBoardData, playerId)
		                                            IF bDoDebugPrints
		                                            	PRINTLN("[Survival] - [Hud Player Array] - participant not been assigned slot in player array already, setting to sBoardData.hudPlayers[1].")
		                           					ENDIF
		                                            sBoardData.hudPlayers[1] = playerId
		                                            sBoardData.iScores[1] = imiddle2
		                                        ENDIF
		                                    ENDIF
		                           		ENDIF
		                            ENDIF
									
		                        	IF GET_PARTICIPANT_SCORE(i, bTotalModeScore) = ilowest
		                               	IF bDoDebugPrints
		                              		PRINTLN("[Survival] - [Hud Player Array] - participant ", i, " playerBD[i].iMyKillsThisWave = ", playerBD[i].iMyKillsThisWave, " = ilowest")
		                               	ENDIF
		                               	IF sBoardData.hudPlayers[3] = INVALID_PLAYER_INDEX()
		                                   	IF bDoDebugPrints
		                                  		PRINTLN("[Survival] - [Hud Player Array] - sBoardData.hudPlayers[3] = INVALID_PLAYER_INDEX()")
		                                  	ENDIF
		                                   	IF NOT HAS_PLAYER_ALREADY_BEEN_ASSIGNED_HUD_PLAYER_ARRAY_INDEX(sBoardData, playerId)
		                                       	IF bDoDebugPrints
		                                      		PRINTLN("[Survival] - [Hud Player Array] - participant not been assigned slot in player array already, setting to sBoardData.hudPlayers[3].")
		                                        ENDIF
		                                       	sBoardData.hudPlayers[3] = playerId
		                                       	sBoardData.iScores[3] = ilowest
		                                   	ENDIF
		                               	ENDIF
		                        	ENDIF
		                        ENDIF
		                    ENDIF
						ENDIF
					ENDIF
                ENDIF
            ENDIF
        ENDIF
    ENDREPEAT
    
    #IF IS_DEBUG_BUILD
		IF bDoDebugPrints
		    PRINTLN("[Survival] - [Hud Player Array] - ****************************************************************************")
		    PRINTLN("[Survival] - [Hud Player Array] - 								FINAL POPULATED DATA ")
		    PRINTLN("[Survival] - [Hud Player Array] - ****************************************************************************")
		    REPEAT MAX_NUMBER_HORDE_FIGHTING_PARTICIPANTS i
		        IF bDoDebugPrints
		        	PRINTLN("[Survival] - [Hud Player Array] - 						sBoardData.hudPlayersindex ", i, " = ", NATIVE_TO_INT(sBoardData.hudPlayers[i]))
		        	PRINTLN("[Survival] - [Hud Player Array] - 						sBoardData.iScores ", i, " = ", sBoardData.iScores[i])
		        ENDIF
		    ENDREPEAT
			PRINTLN("[Survival] - [Hud Player Array] - ****************************************************************************")
		ENDIF
    #ENDIF
    
ENDPROC

PROC CLEAR_HUD_PLAYER_ARRAY(STRUCT_HORDE_BOARD_DATA &sBoardData)
    STRUCT_HORDE_BOARD_DATA sTemp
    sBoardData = sTemp
    sBoardData.iScores[0] = (-1)
    sBoardData.iScores[1] = (-1)
    sBoardData.iScores[2] = (-1)
    sBoardData.iScores[3] = (-1)
ENDPROC

PROC COPY_SORTED_DATA_INTO_MINI_LEADERBOARD_DATA()
    INT i
    
    REPEAT MAX_NUMBER_HORDE_FIGHTING_PARTICIPANTS i
        eMiniLeaderBoardData.miniLeaderBoardStruct[i].iParticipant      = (-1)
        eMiniLeaderBoardData.miniLeaderBoardStruct[i].playerID          = INVALID_PLAYER_INDEX()
        eMiniLeaderBoardData.miniLeaderBoardStruct[i].iPlayerScore      = (-1)
        eMiniLeaderBoardData.miniLeaderBoardStruct[i].iKills            = (-1)
        IF (eMiniLeaderBoardData.sMiniLeaderBoardSortedData.hudPlayers[i] != INVALID_PLAYER_INDEX())
            eMiniLeaderBoardData.miniLeaderBoardStruct[i].iParticipant      = NATIVE_TO_INT( NETWORK_GET_PARTICIPANT_INDEX( eMiniLeaderBoardData.sMiniLeaderBoardSortedData.hudPlayers[i] ) )
			tl63LeaderboardPlayerNames[i]									= GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(NETWORK_GET_PARTICIPANT_INDEX( eMiniLeaderBoardData.sMiniLeaderBoardSortedData.hudPlayers[i])))
            eMiniLeaderBoardData.miniLeaderBoardStruct[i].playerID          = eMiniLeaderBoardData.sMiniLeaderBoardSortedData.hudPlayers[i]
            eMiniLeaderBoardData.miniLeaderBoardStruct[i].iPlayerScore      = eMiniLeaderBoardData.sMiniLeaderBoardSortedData.iScores[i]
            eMiniLeaderBoardData.miniLeaderBoardStruct[i].iKills            = eMiniLeaderBoardData.sMiniLeaderBoardSortedData.iScores[i]
                      
	        PRINTLN("[Survival] - COPY_SORTED_DATA_INTO_MINI_LEADERBOARD_DATA - miniLeaderBoardStruct[", i, "].iParticipant = ", eMiniLeaderBoardData.miniLeaderBoardStruct[i].iParticipant)
	        PRINTLN("[Survival] - COPY_SORTED_DATA_INTO_MINI_LEADERBOARD_DATA -  = tl63LeaderboardPlayerNames[", i, "] = ", tl63LeaderboardPlayerNames[i])
			PRINTLN("[Survival] - COPY_SORTED_DATA_INTO_MINI_LEADERBOARD_DATA - miniLeaderBoardStruct[", i, "].iPlayerScore = ", eMiniLeaderBoardData.miniLeaderBoardStruct[i].iPlayerScore)
	        PRINTLN("[Survival] - COPY_SORTED_DATA_INTO_MINI_LEADERBOARD_DATA - miniLeaderBoardStruct[", i, "].playerID = ", NATIVE_TO_INT(eMiniLeaderBoardData.miniLeaderBoardStruct[i].playerID))
	        PRINTLN("[Survival] - COPY_SORTED_DATA_INTO_MINI_LEADERBOARD_DATA - miniLeaderBoardStruct[", i, "].iKills = ", eMiniLeaderBoardData.miniLeaderBoardStruct[i].iKills)
        ENDIF
    ENDREPEAT
ENDPROC

PROC MAINTAIN_MINI_LEADER_BOARD()
    
	INT i,iIcon
	PLAYER_INDEX playerId
	
    // Temp release build compile fix - Dave R
    BOOL bDebugPrints 
    #IF IS_DEBUG_BUILD bDebugPrints = bDoMiniBoardDebugPrints #ENDIF
    
    IF GET_CLIENT_HORDE_STAGE(PARTICIPANT_ID()) = eHORDESTAGE_FIGHTING
	OR GET_CLIENT_HORDE_STAGE(PARTICIPANT_ID()) = eHORDESTAGE_DELAY_BEFORE_NEXT_WAVE
        SWITCH eMiniLeaderBoardData.eMiniLeaderBoardState
            // Do not display leader board.
            CASE eMINILEADERBOARDSTATE_OFF
				IF SHOULD_DPAD_LBD_DISPLAY(eMiniLeaderBoardData.siMovie, SUB_HORDE, eMiniLeaderBoardData.sDpadVars, serverBD.sFightingParticipantsList.iNumFightingParticipants )
					eMiniLeaderBoardData.eMiniLeaderBoardState = eMINILEADERBOARDSTATE_DO_FIRST_SORT
				ENDIF
            BREAK
            
            // Init leader board data.
            CASE eMINILEADERBOARDSTATE_DO_FIRST_SORT
                // Sort players and copy data into leaderboard array.
				IF SHOULD_DPAD_LBD_DISPLAY(eMiniLeaderBoardData.siMovie, SUB_HORDE, eMiniLeaderBoardData.sDpadVars, serverBD.sFightingParticipantsList.iNumFightingParticipants)
					IF NOT eMiniLeaderBoardData.sMiniLeaderBoardSortedData.bPopulatedHudPlayerArray
	                    // Sort data.
	                    GET_PLAYER_KILLS(eMiniLeaderBoardData.sMiniLeaderBoardSortedData, FALSE)
	                    POPULATE_HUD_PLAYER_ARRAY(eMiniLeaderBoardData.sMiniLeaderBoardSortedData, bDebugPrints)
	                    COPY_SORTED_DATA_INTO_MINI_LEADERBOARD_DATA()
	                    eMiniLeaderBoardData.sMiniLeaderBoardSortedData.bPopulatedHudPlayerArray = TRUE
	                ELSE
	                    PRINTLN("[Survival] - sMiniLeaderBoardSortedData.bPopulatedHudPlayerArray = TRUE, going to mini leader board state eMINILEADERBOARDSTATE_ON.")
	                    eMiniLeaderBoardData.eMiniLeaderBoardState = eMINILEADERBOARDSTATE_ON
	                ENDIF
				ELSE
					PRINTLN("[Survival] - SHOULD_DPAD_LBD_DISPLAY = FALSE, going to mini leader board state eMINILEADERBOARDSTATE_OFF.")
	                eMiniLeaderBoardData.eMiniLeaderBoardState = eMINILEADERBOARDSTATE_OFF
				ENDIF
            BREAK
            
            // Draw leader board and check conditions for turning it off.
            CASE eMINILEADERBOARDSTATE_ON
				IF SHOULD_DPAD_LBD_DISPLAY(eMiniLeaderBoardData.siMovie, SUB_HORDE, eMiniLeaderBoardData.sDpadVars, serverBD.sFightingParticipantsList.iNumFightingParticipants)
					SET_SCRIPT_GFX_ALIGN(UI_ALIGN_LEFT, UI_ALIGN_TOP)
					
	                // Sort data.
	                GET_PLAYER_KILLS(eMiniLeaderBoardData.sMiniLeaderBoardSortedData, FALSE)
	                POPULATE_HUD_PLAYER_ARRAY(eMiniLeaderBoardData.sMiniLeaderBoardSortedData, bDebugPrints)
	                COPY_SORTED_DATA_INTO_MINI_LEADERBOARD_DATA()
	                
	                // Draw leader board.
	                DRAW_DPAD_LEADERBOARD(eMiniLeaderBoardData.sDpadVars)
	                
					IF NOT IS_BIT_SET(eMiniLeaderBoardData.sDpadVars.iDpadBitSet, ciDPAD_IS_UPDATED)
						HIGHLIGHT_LOCAL_PLAYER(eMiniLeaderBoardData.sDpadVars, eMiniLeaderBoardData.siMovie)
						DISPLAY_VIEW(eMiniLeaderBoardData.siMovie)
						SET_BIT(eMiniLeaderBoardData.sDpadVars.iDpadBitSet, ciDPAD_IS_UPDATED)
					ENDIF
					
					HIGHLIGHT_LOCAL_PLAYER(eMiniLeaderBoardData.sDpadVars, eMiniLeaderBoardData.siMovie)
					
					// Draw the mics
					REPEAT NUM_NETWORK_PLAYERS i
						PlayerId = INT_TO_PLAYERINDEX(i)
						IF PlayerId <> INVALID_PLAYER_INDEX()
							IF IS_NET_PLAYER_OK(playerId, FALSE)
								IF eMiniLeaderBoardData.sDpadVars.sDpadMics[i].iRow <> -1
									iIcon = GET_DPAD_PLAYER_VOICE_ICON(eMiniLeaderBoardData.sDpadVars.sDpadMics[i].iVoiceChatState)
									SET_PLAYER_MIC(eMiniLeaderBoardData.siMovie, eMiniLeaderBoardData.sDpadVars.sDpadMics[i].iRow, iIcon, GlobalplayerBD_FM[i].scoreData.iRank)
								ENDIF
							ENDIF
						ENDIF
					ENDREPEAT
					
					IF HAS_SCALEFORM_MOVIE_LOADED(eMiniLeaderBoardData.siMovie)
						DRAW_SCALEFORM_MOVIE(eMiniLeaderBoardData.siMovie, SDPAD_X, SDPAD_Y, SDPAD_W, SDPAD_H, 255, 255, 255, 255)
					ENDIF
										
					RESET_SCRIPT_GFX_ALIGN()
				ELSE
					PRINTLN("[Survival] - SHOULD_DPAD_LBD_DISPLAY = FALSE, going to mini leader board state eMINILEADERBOARDSTATE_OFF.")
	                eMiniLeaderBoardData.eMiniLeaderBoardState = eMINILEADERBOARDSTATE_OFF
					RESET_DPAD_REFRESH()
					
					IF IS_BIT_SET(eMiniLeaderBoardData.sDpadVars.iDpadBitSet, ciDPAD_ROW_HIGHLIGHT)
						CLEAR_BIT(eMiniLeaderBoardData.sDpadVars.iDpadBitSet, ciDPAD_ROW_HIGHLIGHT)
					ENDIF
				ENDIF
            BREAK
        ENDSWITCH
    ELSE
        // If we're not in the fighting stage, reset.
		TEXT_LABEL_23 tl23_Temp
		REPEAT MAX_NUMBER_HORDE_FIGHTING_PARTICIPANTS i
			tl63LeaderboardPlayerNames[i] = tl23_Temp
		ENDREPEAT
        eMiniLeaderBoardData.eMiniLeaderBoardState = eMINILEADERBOARDSTATE_OFF
    ENDIF
	
ENDPROC

PROC MAINTAIN_BLIPS_FOR_PLACED_AREAS()
	
	IF IS_PED_INJURED(PlayerPedToUse)
	OR (IS_PLAYER_RESPAWNING(PLAYER_ID()) AND NOT IS_FAKE_MULTIPLAYER_MODE_SET())
	OR NOT IS_SKYSWOOP_AT_GROUND()
		PRINTLN("[MAINTAIN_BLIPS_FOR_PLACED_AREAS] exit")
		EXIT
	ENDIF
	
	// Spawn Area
	IF HAS_SURVIVAL_BOUNDS_BEEN_PLACED()
		IF NOT DOES_BLIP_EXIST(biPlayArea)
			SWITCH g_FMMC_STRUCT.sSurvivalWaveData.iBoundsType
				CASE SURV_BOUNDS_NON_AXIS_AREA
					biPlayArea = ADD_BLIP_FOR_AREA_FROM_EDGES(g_FMMC_STRUCT.sSurvivalWaveData.vBounds[0], GET_SURVIVAL_BOUNDS_POINT_WITH_HEIGHT(), g_FMMC_STRUCT.sSurvivalWaveData.fBoundsRadius)
				BREAK
				CASE SURV_BOUNDS_CYLINDER
					biPlayArea = ADD_BLIP_FOR_RADIUS(g_FMMC_STRUCT.sSurvivalWaveData.vBounds[0], g_FMMC_STRUCT.sSurvivalWaveData.fBoundsRadius/2)
				BREAK
			ENDSWITCH
			SET_BLIP_COLOUR(biPlayArea, BLIP_COLOUR_YELLOW)
			SET_BLIP_PRIORITY(biPlayArea, BLIPPRIORITY_MED)
			SET_BLIP_ALPHA(biPlayArea, 0)
			
			PRINTLN("[MAINTAIN_BLIPS_FOR_PLACED_AREAS] - Creating Blip (biPlayArea).")
		ELSE
			IF IS_PED_OUT_OF_BOUNDS(PlayerPedToUse)
				INT iR, iG, iB, iA
				GET_HUD_COLOUR(HUD_COLOUR_YELLOW, iR, iG, iB, iA)
				IF fPlayAreaAlpha <= 20
					iPlayAreaChange = 200
				ELIF fPlayAreaAlpha >= 120
					iPlayAreaChange = 0
				ENDIF				
				INTERP_FLOAT_TO_TARGET_AT_SPEED(fPlayAreaAlpha, TO_FLOAT(iPlayAreaChange), 1.25)
				iA = ROUND(fPlayAreaAlpha)
				VECTOR vBounds1 = GET_SURVIVAL_BOUNDS_POINT_WITH_LOW_OFFSET()
				IF NOT HAS_SURVIVAL_BOUNDS_GOT_HEIGHT()
					vBounds1.z = -1
				ENDIF

				SWITCH g_FMMC_STRUCT.sSurvivalWaveData.iBoundsType
					CASE SURV_BOUNDS_NON_AXIS_AREA
						IF NOT HAS_SURVIVAL_BOUNDS_GOT_HEIGHT()
						AND NOT IS_BIT_SET(g_FMMC_STRUCT.sSurvivalWaveData.iBitset, ciSurvivalBS_BoundsHeightSet)
							DRAW_BOX_MARKER_FROM_ANGLED_AREA(vBounds1, GET_SURVIVAL_BOUNDS_POINT_WITH_HEIGHT(), g_FMMC_STRUCT.sSurvivalWaveData.fBoundsRadius, HUD_COLOUR_YELLOW, 0.0, 50.0, iA, DEFAULT, TRUE)
						ELSE
							INT iRInner, iBInner, iGInner, iAlphaInner
							GET_HUD_COLOUR(HUD_COLOUR_YELLOW, iRInner, iGInner, iBInner, iAlphaInner)
							iAlphaInner = 50
							DRAW_ANGLED_AREA_FROM_FACES(GET_SURVIVAL_BOUNDS_POINT_WITH_LOW_OFFSET(), GET_SURVIVAL_BOUNDS_POINT_WITH_HEIGHT(), g_FMMC_STRUCT.sSurvivalWaveData.fBoundsRadius, iRInner, iGInner, iBInner, iAlphaInner)
						ENDIF
					BREAK
					CASE SURV_BOUNDS_CYLINDER
						FLOAT fHeight
						fHeight = 40.0
						IF HAS_SURVIVAL_BOUNDS_GOT_HEIGHT()
						OR IS_BIT_SET(g_FMMC_STRUCT.sSurvivalWaveData.iBitset, ciSurvivalBS_BoundsHeightSet)
							DRAW_CYLINDER(vBounds1, GET_SURVIVAL_BOUNDS_POINT_WITH_HEIGHT(), g_FMMC_STRUCT.sSurvivalWaveData.fBoundsRadius/2, iR, iG, iB, iA)
						ELSE
							DRAW_CYLINDER_MARKER_FROM_VECTOR(vBounds1, g_FMMC_STRUCT.sSurvivalWaveData.fBoundsRadius, fHeight, HUD_COLOUR_YELLOW, 120, TRUE)
						ENDIF
						
					BREAK
				ENDSWITCH
				SET_BLIP_ALPHA(biPlayArea, 100)
				
				PRINTLN("[MAINTAIN_BLIPS_FOR_PLACED_AREAS] - Out of Area - Draw Map Marker.")
			ELSE
				SET_BLIP_ALPHA(biPlayArea, 0)			
			ENDIF
		ENDIF
	ELSE
		PRINTLN("[MAINTAIN_BLIPS_FOR_PLACED_spam_AREAS] zero")
		IF DOES_BLIP_EXIST(biPlayArea)			
			REMOVE_BLIP(biPlayArea)
			
			PRINTLN("[MAINTAIN_BLIPS_FOR_PLACED_AREAS] - Removing Blip (biPlayArea).")
		ENDIF
	ENDIF
		
ENDPROC

PROC MAINTAIN_CANNOT_USE_VEHICLES_HELP()
	
	VEHICLE_INDEX vehIndex
	BOOL bDisplayHelp
	
	// Tell players they cannot use vehicles.
	IF NOT bDisplayedNoVehsHelp
		IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
			IF DOES_ENTITY_EXIST(LocalPlayerPed)
				IF NOT IS_ENTITY_DEAD(LocalPlayerPed)
					IF IS_PED_TRYING_TO_ENTER_A_LOCKED_VEHICLE(LocalPlayerPed)
						bDisplayHelp = TRUE
					ENDIF
					
					vehIndex = GET_VEHICLE_PED_IS_ENTERING(LocalPlayerPed)
					
					IF DOES_ENTITY_EXIST(vehIndex)
						IF IS_VEHICLE_DRIVEABLE(vehIndex)
							IF GET_VEHICLE_DOOR_LOCK_STATUS(vehIndex) = VEHICLELOCK_LOCKED
								bDisplayHelp = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF bDisplayHelp
		PRINT_HELP("HRD_NOJACK")
		bDisplayedNoVehsHelp = TRUE
	ENDIF
	
ENDPROC

PROC SET_ALL_ENTITY_BLIP_DISPLAY(INTERIOR_INSTANCE_INDEX idxInteriorCoords, BLIP_DISPLAY bDisplay)
	INT i, iStage, iSquad, iPed
	ENTITY_INDEX entIndex
	FOR iStage = 0 TO NUM_SURVIVAL_SUB_STAGES-1
		FOR iSquad = 0 TO serverBD.sWaveData.iTotalSquads-1
			FOR iPed = 0 TO serverBD.sWaveData.iNumPedsInSquad[iStage]-1
				IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD_Enemies_S.structAiSquad[iSquad][iStage].sPed[iPed].netId)
					entIndex = NET_TO_ENT(serverBD_Enemies_S.structAiSquad[iSquad][iStage].sPed[iPed].netId)	
					IF DOES_ENTITY_EXIST(entIndex)
						BLIP_INDEX biPedBlip = GET_BLIP_FROM_ENTITY(entIndex)
						IF DOES_BLIP_EXIST(biPedBlip)
						AND GET_INTERIOR_FROM_ENTITY(entIndex) = idxInteriorCoords
							SET_BLIP_DISPLAY(biPedBlip, bDisplay)
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
		ENDFOR
	ENDFOR
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons-1
		IF DOES_BLIP_EXIST(biPickupBlip[i])
		AND DOES_PICKUP_EXIST(piPickup[i])
			IF GET_INTERIOR_AT_COORDS(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].vPos) = idxInteriorCoords
				SET_BLIP_DISPLAY(biPickupBlip[i], bDisplay)
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

FUNC INT GET_INTERIOR_LEVEL_FOR_MINIMAP()
	INT iFloor = 0
	VECTOR vPlayerPos = GET_ENTITY_COORDS(LocalPlayerPed)
	FLOAT fPlayerZ = vPlayerPos.z
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SILO_03)
		IF fPlayerZ <= -156
			iFloor = 0
		ELIF fPlayerZ <= -152
			iFloor = 1
		ELIF fPlayerZ <= -148
			iFloor = 2
		ELSE
			iFloor = 3
		ENDIF
	ENDIF
	
	RETURN iFloor
ENDFUNC

PROC PROCESS_INTERIOR_MINIMAP()
	
	IF NOT IS_VALID_INTERIOR(sInterior.iInteriorIndex)
		EXIT
	ENDIF

	PRINTLN("PROCESS_INTERIOR_MINIMAP - calling function...")
	INTERIOR_DATA_STRUCT structInteriorData = sInterior.structInteriorData

	INT iInteriorHash = GET_HASH_KEY(structInteriorData.sInteriorName)
	INT iFloor = GET_INTERIOR_LEVEL_FOR_MINIMAP()
	VECTOR vInteriorPos = structInteriorData.vPos
	FLOAT fRotation = -0
	
	BOOL bShowInterior = TRUE
	BOOL bStaggerChange = (GET_FRAME_COUNT() % 5) = 0
	
	IF IS_PAUSE_MENU_ACTIVE()
		IF NOT IS_PAUSEMAP_IN_INTERIOR_MODE()
			bShowInterior = FALSE
		ENDIF
	ELIF g_PlayerBlipsData.bBigMapIsActive
		bShowInterior = FALSE
	ENDIF
	
	IF bShowInterior
		SET_RADAR_AS_INTERIOR_THIS_FRAME(iInteriorHash, vInteriorPos.x, vInteriorPos.y, ROUND(fRotation), iFloor)
		SET_RADAR_ZOOM(100)
		HIDE_MINIMAP_EXTERIOR_MAP_THIS_FRAME()
		IF bStaggerChange
			SET_ALL_ENTITY_BLIP_DISPLAY(sInterior.iInteriorIndex, DISPLAY_BLIP)
			g_PlayerBlipsData.bHideAllPlayerBlips = FALSE
		ENDIF
	ELSE
		HIDE_PLAYER_ARROW_THIS_FRAME()
		HIDE_MINIMAP_INTERIOR_MAP_THIS_FRAME()
	ENDIF
ENDPROC

PROC PROCESS_IPL_CARRIER_MAP(VECTOR vPlayer)
	
	VECTOR vCarrierCentre = <<3014.45, -4661.85, 18.85>>
	
	FLOAT fDistance2 = VDIST2(<<vPlayer.x, vPlayer.y, 0>>, <<vCarrierCentre.x, vCarrierCentre.y, 0>>)
	
	IF NOT IS_LOCAL_BIT_SET(ciLocalBool_bShowCarrierMap)
		
		IF IS_LOCAL_BIT_SET(ciLocalBool_bBigMapWasZoomedCarrier)
			SET_RADAR_ZOOM_PRECISE(0)
			CLEAR_LOCAL_BIT(ciLocalBool_bBigMapWasZoomedCarrier)
		ENDIF
		
		//Distance check to initialise:
		IF fDistance2 < (1500 * 1500)
			PRINTLN("[RCC MISSION] PROCESS_IPL_MAP_DISPLAY, IPL_CARRIER - Within 1500m of Carrier, initialising map")
			SET_LOCAL_BIT(ciLocalBool_bShowCarrierMap)
			SET_LOCAL_BIT(ciLocalBool_bBigMapDoZoomCarrier)
		ENDIF
	ELSE
		INT iFloor = -2
		BOOL bInternal = FALSE
		
		SET_LOCAL_BIT(ciLocalBool_bBigMapDoZoomCarrier)
		
		IF (fDistance2 <= (300 * 300))
			
			IF (GET_ROOM_KEY_FROM_ENTITY(PlayerPedToUse) != 0)
				
				bInternal = TRUE
				
				IF vPlayer.z <= 3.0
					iFloor = -1
				ELIF vPlayer.z <= 7.3
					iFloor = 0
				ELIF vPlayer.z <= 12.6
					iFloor = 1
				ELIF vPlayer.z <= 15.724
					iFloor = 2
				ELIF vPlayer.z <= 18.756
					iFloor = 3
				ELIF vPlayer.z <= 21.73
					iFloor = 4
				ELIF vPlayer.z <= 24.633
					iFloor = 5
				ELIF vPlayer.z <= 27.64
					iFloor = 6
				ELIF vPlayer.z <= 32.756
					iFloor = 7
				ELIF vPlayer.z <= 52.938
					iFloor = 8
				ELSE
					iFloor = 2
				ENDIF
				
				IF IS_AUDIO_SCENE_ACTIVE("DLC_HEISTS_BIOLAB_STEAL_EMP_FLYOVER_SCENE")
					PRINTLN("[MJM MISSION] STOP_AUDIO_SCENE(DLC_HEISTS_BIOLAB_STEAL_EMP_FLYOVER_SCENE")
					STOP_AUDIO_SCENE("DLC_HEISTS_BIOLAB_STEAL_EMP_FLYOVER_SCENE")
				ENDIF
			ELSE					
				IF vPlayer.z <= 3.0
					IF IS_POINT_IN_ANGLED_AREA(vPlayer, <<3103.206543, -4805.419434, 5>>, <<3080.537109, -4811.319336, -2>>, 29.5) //Back entrance, dock
					OR IS_POINT_IN_ANGLED_AREA(vPlayer, <<3098.5, -4791.1, 6.6>>, <<3100.8, -4800.0, 0.0>>, 5.5) //Back entrance, stairs
						bInternal = TRUE
						iFloor = -1
					ELIF IS_POINT_IN_ANGLED_AREA(vPlayer, <<3109, -4825, 5>>,<<3082, -4832, -10>>, 130, TRUE) //Back entrance
						iFloor = -1
					ELIF IS_POINT_IN_ANGLED_AREA(vPlayer, <<3098.3, -4748.5, 2.55>>, <<3105.6, -4775.6, 10.3>>, 9.25) //Hole opposite rear platform
						bInternal = TRUE
						iFloor = 0
					ELSE
						iFloor = 2
					ENDIF
				ELIF vPlayer.z <= 11
					IF IS_POINT_IN_ANGLED_AREA(vPlayer, <<3072.1, -4825.9, 3.0>>, <<3107.2, -4810.5, 9.9>>, 28.7) //Back entrance, upper floor
					OR IS_POINT_IN_ANGLED_AREA(vPlayer, <<3082.2, -4659.9, 3.5>>, <<3075.1, -4632.6, 10.3>>, 22.25) //Front side platform
					OR IS_POINT_IN_ANGLED_AREA(vPlayer, <<3063.7, -4809.4, 3.5>>, <<3056.3, -4781.9, 10.3>>, 22.25) //Rear side platform
					OR IS_POINT_IN_ANGLED_AREA(vPlayer, <<3098.3, -4748.5, 2.55>>, <<3105.6, -4775.6, 10.3>>, 9.25) //Hole opposite rear platform
						bInternal = TRUE
						iFloor = 0
					ELIF ( IS_POINT_IN_ANGLED_AREA(vPlayer, <<3103.206543, -4805.419434, 5>>, <<3080.537109, -4811.319336, -2>>, 29.5) //Back entrance, dock
						   OR IS_POINT_IN_ANGLED_AREA(vPlayer, <<3098.5, -4791.1, 6.6>>, <<3100.8, -4800.0, 0.0>>, 5.5) ) //Back entrance, stairs
						bInternal = TRUE
						iFloor = -1
					ELSE
						iFloor = 2
					ENDIF
				ELSE
					iFloor = 2
				ENDIF
			ENDIF
			
			IF g_PlayerBlipsData.bBigMapIsActive
				SET_LOCAL_BIT(ciLocalBool_bBigMapWasZoomedCarrier)
				IF IS_LOCAL_BIT_SET(ciLocalBool_bBigMapDoZoomCarrier)
					IF bInternal
						SET_RADAR_ZOOM_PRECISE(55.0)
					ELSE
						SET_RADAR_ZOOM_PRECISE(92.0)
					ENDIF
				ELSE
					SET_RADAR_ZOOM_PRECISE(0.0)
				ENDIF
			ELSE
				IF IS_LOCAL_BIT_SET(ciLocalBool_bBigMapWasZoomedCarrier)
					SET_RADAR_ZOOM_PRECISE(0)
					CLEAR_LOCAL_BIT(ciLocalBool_bBigMapWasZoomedCarrier)
				ENDIF
			ENDIF
			
			IF NOT IS_LOCAL_BIT_SET(ciLocalBool_bCarrerLightsPreloaded)
				PRESET_INTERIOR_AMBIENT_CACHE("int_carrier_hanger")
				SET_LOCAL_BIT(ciLocalBool_bCarrerLightsPreloaded)
			ENDIF
			
		ELSE					
			IF IS_LOCAL_BIT_SET(ciLocalBool_bBigMapWasZoomedCarrier)
				SET_RADAR_ZOOM_PRECISE(0.0)
				CLEAR_LOCAL_BIT(ciLocalBool_bBigMapWasZoomedCarrier)
			ENDIF
			
			IF IS_LOCAL_BIT_SET(ciLocalBool_bCarrerLightsPreloaded)
				CLEAR_LOCAL_BIT(ciLocalBool_bCarrerLightsPreloaded)
			ENDIF
			
			IF fDistance2 <= (2000 * 2000)
				iFloor = 2
			ELSE
				//Out of range, we could get in other interiors now - so clear this stuff from the map
				PRINTLN("[RCC MISSION] PROCESS_IPL_MAP_DISPLAY, IPL_CARRIER - Over 2000m from Carrier, clearing map")
				iFloor = -2
				CLEAR_LOCAL_BIT(ciLocalBool_bShowCarrierMap)
			ENDIF
		ENDIF
		
		IF iFloor >= -1
			SET_RADAR_AS_INTERIOR_THIS_FRAME(1596791599,3050.0,-4650.0,DEFAULT,iFloor)
			IF NOT bInternal
				SET_RADAR_AS_EXTERIOR_THIS_FRAME()
			ENDIF
		ENDIF				
	ENDIF
	
ENDPROC

PROC PROCESS_MINIMAP()
	BOOL bRunInteriorMiniMap = TRUE
	
	VECTOR vPlayer = GET_ENTITY_COORDS(PlayerPedToUse)
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions,IPL_CARRIER)
		PROCESS_IPL_CARRIER_MAP(vPlayer)
	ENDIF
	
	IF bRunInteriorMiniMap
		PROCESS_INTERIOR_MINIMAP()
	ENDIF
	
ENDPROC
