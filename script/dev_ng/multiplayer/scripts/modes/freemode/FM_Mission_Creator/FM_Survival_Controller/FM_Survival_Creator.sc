//////////////////////////////////////////////////////////////
// Name: FM_Survival_Creator.sc								//
// Description: Make your own survival mission				//
// Written by:  Jack Thallon								//
// Date: 24/01/2019											//
//////////////////////////////////////////////////////////////

USING "rage_builtins.sch"
USING "globals.sch"
USING "script_player.sch"
USING "commands_pad.sch"
USING "commands_camera.sch" 
USING "FMMC_header.sch"
USING "FMMC_keyboard.sch"
USING "FMMC_MP_In_Game_Menu.sch"
USING "FM_Tutorials.sch"
USING "PM_MissionCreator_Public.sch"
USING "rc_helper_functions.sch"
#IF IS_DEBUG_BUILD
USING "profiler.sch"
#ENDIF

USING "FM_Survival_Creator_Utility.sch"

FMMC_LOCAL_STRUCT 						sFMMCdata
FMMC_CAM_DATA 							sCamData
PED_CREATION_STRUCT 					sPedStruct
TEAM_SPAWN_CREATION_STRUCT				sTeamSpawnStruct[FMMC_MAX_TEAMS]
VEHICLE_CREATION_STRUCT					sVehStruct
WEAPON_CREATION_STRUCT					sWepStruct
OBJECT_CREATION_STRUCT					sObjStruct
PROP_CREATION_STRUCT					sPropStruct
DYNOPROP_CREATION_STRUCT				sDynoPropStruct
INVISIBLE_OBJECT_STRUCT					sInvisibleObjects
LOCATION_CREATION_STRUCT				sLocStruct
TRAIN_CREATION_STRUCT					sTrainStruct
HUD_COLOURS_STRUCT 						sHCS
CREATION_VARS_STRUCT 					sCurrentVarsStruct
structFMMC_MENU_ITEMS 					sFMMCmenu
START_END_BLIPS							sStartEndBlips
FMMC_COMMON_MENUS_VARS 					sFMMCendStage
FMMC_UP_DOWN_STRUCT						menuScrollController
sHudStatValues 							sWeaponValues
sHudStatValues 							sModValues
TRAIN_CREATION_STRUCT 					sTrainCreationStruct

GET_UGC_CONTENT_STRUCT sGetUGC_content

ENUM RESET_STATE 
	RESET_STATE_FADE,
	RESET_STATE_CLEAR,
	RESET_STATE_INTERIORS,
	RESET_STATE_INIT_BUDGET,
	RESET_STATE_WEAPONS,
	RESET_STATE_UNLOAD_WEAPONS,
	RESET_STATE_VEHICLES,
	RESET_STATE_UNLOAD_VEHICLES,
	RESET_STATE_PROPS,
	RESET_STATE_DYNOPROPS,
	RESET_STATE_UNLOAD_PROPS,
	RESET_STATE_ATTACH,
	RESET_STATE_SPAWNS,
	RESET_STATE_PEDS,
	RESET_STATE_TEAM_SPAWNS,
	RESET_STATE_OTHER,
	RESET_STATE_FINISH
ENDENUM
RESET_STATE iResetState

STRUCT_DL_PHOTO_VARS_LITE sDownloadPhotoVars

ENUM eTESTING_MC_MISSION_MENU_STATE
	eTESTINGMCMISSIONMENUSTATE_DISPLAY_HELP = 0,
	eTESTINGMCMISSIONMENUSTATE_SETUP_MENU,
	eTESTINGMCMISSIONMENUSTATE_SHOW_MENU
ENDENUM
STRUCT STRUCT_TEST_MENU_DATA
	eTESTING_MC_MISSION_MENU_STATE eTestMcMissionMenuState
	BOOL bDisplayedHelp
ENDSTRUCT
STRUCT_TEST_MENU_DATA structTestMcMissionMenuData

INT iLocalBitSet
INT iDoorSetupStage, iDoorSlowLoop
INT iLoadingOverrideTimer
INT iWarpState, iWarpStateTimeoutStart
INT iVehicleModelLoadTimers[FMMC_MAX_VEHICLES]
INT iPropModelTimers[FMMC_MAX_NUM_PROPS]

BOOL bInitialIntroCamSetup = TRUE
BOOL bInitialIntroCamWarp = FALSE
BOOL bIsMaleSPTC, bIsCreatorModelSetSPTC
BOOL bLastUseMouseKeyboard = FALSE
BOOL bOnGroundBeforeTest
BOOL bContentReadyForUGC
BOOL bInitialRadioRefreshDone

//Photo and Lobby Cameras
PREVIEW_PHOTO_STRUCT PPS
INT iIntroCamPlacementStage
SCALEFORM_INDEX SF_Movie_Gallery_Shutter_Index
INT iCamPanState = PAN_CAM_SETUP
JOB_INTRO_CUT_DATA jobIntroData
VECTOR vInitialPreviewPos

SCALEFORM_INDEX sfSummaryMenu

SUMMARY_WINDOW_DATA sSummaryData

BOOL bShowMenu = TRUE
BOOL bShowMenuHighlight = FALSE
BOOL bDelayAFrame = TRUE

//Test Mode
BOOL bMpModeCleanedUp, bMpNeedsCleanedUp, bTestModeControllerScriptStarted, bTextSetUp
mission_display_struct on_mission_gang_box
INT iTestAlertStartFrame = -1
SCRIPT_TIMER tdNoHelpTimer

// Camera Switching Stuff
VECTOR vSwitchVec = <<0,0,0>>
FLOAT fSwitchHeading = 0
BOOL bSwitchingCam = FALSE

//Trigger
INT iTriggerCreationStage = CREATION_STAGE_WAIT

BOOL bFailHit

//Blips
BLIP_INDEX bCameraTriggerBlip
BLIP_INDEX bCameraPanBlip
BLIP_INDEX bPhotoBlip
BLIP_INDEX biPlayArea
FLOAT fCreatedPlayAreaWidth = -1.0
BLIP_INDEX biSpawnBlips[MAX_SURVIVAL_SPAWN_POINTS]

BOOL ButtonPressed
BOOL bCreatorLimitedCloudDown = FALSE
BOOL bSignedOut = FALSE

//Help
INT iHelpBitSet
INT iHelpBitSetOld
CONST_INT biPickupEntityButton 						0
CONST_INT biDeleteEntityButton 						4
CONST_INT biWarpToCameraButton 						6
CONST_INT biWarpToCoronaButton 						7

INT iMaxSpawnsPerTeam[FMMC_MAX_TEAMS]
SCRIPT_TIMER tdZoneTimer
//Menus
INT iTopItem

BLIMP_SIGN sBlimpSign

PED_INDEX pedSelection
VEHICLE_INDEX vehSelection
SCRIPT_TIMER tdPedUpdate
CONST_INT ciPedUpdateTime 150

MODEL_NAMES mnVehSelectionModel

INT iNumberOfPedsLastFrame
structNGCreatorTutorial sSurvTutorial
BOOL bRunTutorial

BOOL bMultiplayerMapSet
VECTOR vCarrierPosition			= <<3048.3081, -4658.6313, 29.5800>>
VECTOR vIAAFaciltyDoor 			= <<2049.1, 2950.1,47.1>>

#IF IS_DEBUG_BUILD
BLIP_INDEX biDoors[FMMC_MAX_NUM_DOORS]
PROC UPDATE_WIDGETS()
	
ENDPROC

#ENDIF

PROC SET_ENTITY_CREATION_STATUS(INT iNewState)
	DEBUG_PRINTCALLSTACK()
	PRINTLN("[SURVIVAL CREATOR] - SET_ENTITY_CREATION_STATUS - Setting entity creation state from ", sCurrentVarsStruct.iEntityCreationStatus, " to ", iNewState)
	sCurrentVarsStruct.iEntityCreationStatus = iNewState
ENDPROC

PROC TRACK_TIME_IN_CREATOR_MODE()

	IF NETWORK_CLAN_SERVICE_IS_VALID()  
		INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_TOTAL_TIME_MISSION_CREATO, TIMERA())
		SETTIMERA(0)
	ENDIF
	
ENDPROC

PROC DEAL_WITH_AMBIENT_AND_HUD()
	IF NOT IS_BIT_SET(sFMMCdata.iBitSet, biTestMissionActive)
		sCurrentVarsStruct.creationStats.iCreatingTimeMS += GET_GAME_TIMER() - sCurrentVarsStruct.creationStats.iCurrentTimeMS
		
		FMMC_MAINTAIN_TIME_AND_WEATHER(g_FMMC_STRUCT.iTimeOfDay)
		FM_MAINTAIN_MISSION_DENSITYS_THIS_FRAME(DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)	
		IF IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)	
			DISABLE_ADDITIONAL_CONTROLS()
		ENDIF
		BOOL bHideRadar
		FMMC_HIDE_HUD_ELEMENTS(sFMMCdata, sFMMCMenu, bHideRadar)
		MAINTAIN_CAMERA_ROTATION_LIMITS(sCamData, sFMMCmenu.sActiveMenu, sCurrentVarsStruct, bInitialIntroCamSetup, bInitialIntroCamWarp)
		IF bHideRadar
			SET_BIGMAP_ACTIVE(FALSE, FALSE)
		ELSE
			SET_BIGMAP_ACTIVE(TRUE, FALSE)
		ENDIF
	ENDIF
	sCurrentVarsStruct.creationStats.iCurrentTimeMS = GET_GAME_TIMER()
	
	DEAL_WITH_PHOTO_PREVIEW(PPS, sFMMCendStage.sTakePhotoVars, sCurrentVarsStruct.iMenuState, sFMMCmenu.sActiveMenu = eFmmc_PHOTO_CAM_BASE AND iIntroCamPlacementStage = 2 AND NOT IS_BIT_SET(sFMMCmenu.iMiscBitSet, bsHidePreviewPhoto),sFMMCmenu.sActiveMenu = eFmmc_PHOTO_CAM_BASE)
	DEAL_WITH_THE_PLACING_INTRO_CAM(SF_Movie_Gallery_Shutter_Index, sFMMCmenu.sActiveMenu, iIntroCamPlacementStage, bInitialIntroCamSetup, bShowMenu, sCurrentVarsStruct.iMenuState, sCurrentVarsStruct.iEntityCreationStatus)
	
	CREATOR_DOORS_SETUP(iDoorSlowLoop,iDoorSetupStage, FMMC_TYPE_SURVIVAL)
	
	IF SHOULD_DISPLAY_WAVE_SUMMARY(sFMMCmenu)
		IF MAINTAIN_SUMMARY_PHOTO(sDownloadPhotoVars)
			PROCESS_SUMMARY_WINDOW(sfSummaryMenu, sFMMCmenu, PPS, sSummaryData)
		ENDIF
	ELSE
		IF IS_BIT_SET(sFMMCmenu.iMiscBitSet, bsShowSummaryWindow)
		
			RESET_STRUCT_DL_PHOTO_VARS_LITE(sDownLoadPhotoVars)
			
			CLEAR_BIT(sFMMCmenu.iMiscBitSet, bsShowSummaryWindow)
			SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sfSummaryMenu)
			sFMMCmenu.iSurvivalSummaryWaveSelection = -1
		ENDIF
	ENDIF
	
	IF SCRIPT_IS_CLOUD_AVAILABLE()
		TRACK_TIME_IN_CREATOR_MODE()
	ENDIF
ENDPROC

PROC DRAW_BOX_AREA_MARKER()

	VECTOR vFrontPos = g_FMMC_STRUCT.sSurvivalWaveData.vBounds[0]
	VECTOR vEndPos = g_FMMC_STRUCT.sSurvivalWaveData.vBounds[1]
				
	IF sFMMCMenu.iBoundsEditPointIndex = 0
		vFrontPos = sCurrentVarsStruct.vCoronaPos
	ELIF sFMMCMenu.iBoundsEditPointIndex = 1
		vEndPos = sCurrentVarsStruct.vCoronaPos
	ENDIF
	
	BOOL bBox = HAS_SURVIVAL_BOUNDS_GOT_HEIGHT()
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sSurvivalWaveData.iBitset, ciSurvivalBS_BoundsHeightSet)
		bBox = TRUE
	ENDIF
	

	IF !bBox
		IF HAS_SURVIVAL_BOUNDS_BEEN_PLACED()
			DRAW_BOX_MARKER_FROM_ANGLED_AREA(vFrontPos, vEndPos, g_FMMC_STRUCT.sSurvivalWaveData.fBoundsRadius, HUD_COLOUR_YELLOW, -20.0, 40.0, DEFAULT, DEFAULT, TRUE)
		ENDIF
		
		IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sSurvivalWaveData.vBounds[0]) AND IS_VECTOR_ZERO(g_FMMC_STRUCT.sSurvivalWaveData.vBounds[1])
		AND CAN_PLACE_BOUNDS_ON_THIS_OPTION(sFMMCmenu)
		AND sFMMCmenu.sActiveMenu = eFmmc_SURVIVAL_WARNING_AREA
			DRAW_BOX_MARKER_FROM_ANGLED_AREA(vFrontPos, sCurrentVarsStruct.vCoronaPos, g_FMMC_STRUCT.sSurvivalWaveData.fBoundsRadius, HUD_COLOUR_YELLOW, -20.0, 40.0, DEFAULT, DEFAULT, TRUE)
		ENDIF
	ELSE
		VECTOR vBoxEndPos
		VECTOR vBoxStartPos = vFrontPos
		
		INT iRInner, iBInner, iGInner, iAlphaInner
		GET_HUD_COLOUR(HUD_COLOUR_YELLOW, iRInner, iGInner, iBInner, iAlphaInner)
		iAlphaInner = 50
		
		IF HAS_SURVIVAL_BOUNDS_BEEN_PLACED()
			vBoxEndPos = vEndPos
		ENDIF
		IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sSurvivalWaveData.vBounds[0]) AND IS_VECTOR_ZERO(g_FMMC_STRUCT.sSurvivalWaveData.vBounds[1])
		AND CAN_PLACE_BOUNDS_ON_THIS_OPTION(sFMMCmenu)
		AND sFMMCmenu.sActiveMenu = eFmmc_SURVIVAL_WARNING_AREA
			vBoxEndPos = sCurrentVarsStruct.vCoronaPos
		ENDIF
		vBoxStartPos.z -= g_FMMC_STRUCT.sSurvivalWaveData.fBoundsBaseOffset
		vBoxEndPos.z += g_FMMC_STRUCT.sSurvivalWaveData.fBoundsHeight
		DRAW_ANGLED_AREA_FROM_FACES(vBoxStartPos, vBoxEndPos, g_FMMC_STRUCT.sSurvivalWaveData.fBoundsRadius, iRInner, iGInner, iBInner, iAlphaInner)
	ENDIF
	
	IF sFMMCmenu.sActiveMenu = eFmmc_SURVIVAL_WARNING_AREA
	
		FLOAT fSphereScale = 5.0
		
		IF GET_CREATOR_MENU_SELECTION(sFMMCMenu) = OPTION_SURV_BOUNDS_EDIT_FRONT
			DRAW_MARKER(MARKER_SPHERE, vFrontPos, <<0,0,0>>, <<0,0,0>>, <<fSphereScale, fSphereScale, fSphereScale>>)
			
			IF bBox
			AND g_FMMC_STRUCT.sSurvivalWaveData.fBoundsBaseOffset > 5
				vFrontPos = GET_SURVIVAL_BOUNDS_POINT_WITH_LOW_OFFSET()
				DRAW_MARKER(MARKER_SPHERE, vFrontPos, <<0,0,0>>, <<0,0,0>>, <<fSphereScale, fSphereScale, fSphereScale>>)
			ENDIF
		ENDIF
		IF GET_CREATOR_MENU_SELECTION(sFMMCMenu) = OPTION_SURV_BOUNDS_EDIT_BACK
			VECTOR vTempEndPos = vEndPos
			vTempEndPos.z = vFrontPos.z
			DRAW_MARKER(MARKER_SPHERE, vTempEndPos, <<0,0,0>>, <<0,0,0>>, <<fSphereScale, fSphereScale, fSphereScale>>)
			
			IF bBox
			AND g_FMMC_STRUCT.sSurvivalWaveData.fBoundsHeight > 5
				vTempEndPos = GET_SURVIVAL_BOUNDS_POINT_WITH_HEIGHT()
				DRAW_MARKER(MARKER_SPHERE, vTempEndPos, <<0,0,0>>, <<0,0,0>>, <<fSphereScale, fSphereScale, fSphereScale>>)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC DRAW_AREA_CYLINDER_MARKER()
	FLOAT fHeight
	fHeight = 40.0
	
	BOOL bUseHeight = HAS_SURVIVAL_BOUNDS_GOT_HEIGHT()
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sSurvivalWaveData.iBitset, ciSurvivalBS_BoundsHeightSet)
		bUseHeight = TRUE
	ENDIF
	
	IF !bUseHeight
		IF HAS_SURVIVAL_BOUNDS_BEEN_PLACED()
			IF sFMMCMenu.iBoundsEditPointIndex = -1
				DRAW_CYLINDER_MARKER_FROM_VECTOR(g_FMMC_STRUCT.sSurvivalWaveData.vBounds[0], g_FMMC_STRUCT.sSurvivalWaveData.fBoundsRadius, fHeight, HUD_COLOUR_YELLOW, 120, TRUE)
			ELSE
				DRAW_CYLINDER_MARKER_FROM_VECTOR(sCurrentVarsStruct.vCoronaPos, g_FMMC_STRUCT.sSurvivalWaveData.fBoundsRadius, fHeight, HUD_COLOUR_YELLOW, 120, TRUE)
			ENDIF
		ELSE
			IF (CAN_PLACE_BOUNDS_ON_THIS_OPTION(sFMMCmenu)
			OR sFMMCMenu.iBoundsEditPointIndex != -1)
			AND sFMMCmenu.sActiveMenu = eFmmc_SURVIVAL_WARNING_AREA
				DRAW_CYLINDER_MARKER_FROM_VECTOR(sCurrentVarsStruct.vCoronaPos, g_FMMC_STRUCT.sSurvivalWaveData.fBoundsRadius, fHeight, HUD_COLOUR_YELLOW, 120, TRUE)
			ENDIF
		ENDIF
	ELSE
		VECTOR vHighPoint = g_FMMC_STRUCT.sSurvivalWaveData.vBounds[0]
		VECTOR vStartPoint
		INT iR, iG, iB, iAlpha
		GET_HUD_COLOUR(HUD_COLOUR_YELLOW, iR, iG, iB, iAlpha)
		iAlpha = 120
		IF HAS_SURVIVAL_BOUNDS_BEEN_PLACED()
		AND sFMMCMenu.iBoundsEditPointIndex = -1 
			vStartPoint = g_FMMC_STRUCT.sSurvivalWaveData.vBounds[0]
			vHighPoint = g_FMMC_STRUCT.sSurvivalWaveData.vBounds[0]
		ELSE
			vStartPoint = sCurrentVarsStruct.vCoronaPos
			vHighPoint = sCurrentVarsStruct.vCoronaPos
		ENDIF
		vHighPoint.z += g_FMMC_STRUCT.sSurvivalWaveData.fBoundsHeight
		vStartPoint.z -= g_FMMC_STRUCT.sSurvivalWaveData.fBoundsBaseOffset
		DRAW_CYLINDER(vStartPoint, vHighPoint, g_FMMC_STRUCT.sSurvivalWaveData.fBoundsRadius/2, iR, iG, iB, iAlpha)
	ENDIF
ENDPROC

PROC MAINTAIN_PLACED_MARKERS()
	MAINTAIN_PLACED_TRIGGER_LOCATION(g_FMMC_STRUCT.vStartPos, sStartEndBlips.biStart, sStartEndBlips.ciStartType, g_FMMC_STRUCT.vCameraPanPos, bCameraPanBlip, bPhotoBlip)
	
	//Survival Bounds
	SWITCH g_FMMC_STRUCT.sSurvivalWaveData.iBoundsType
		CASE SURV_BOUNDS_NON_AXIS_AREA
			DRAW_BOX_AREA_MARKER()
		BREAK
		CASE SURV_BOUNDS_CYLINDER
			DRAW_AREA_CYLINDER_MARKER()
		BREAK
	ENDSWITCH
	
	IF sFMMCmenu.iEntityCreation != -1
		INT i
		FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles-1
			IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].vPos)
			AND sFMMCMenu.iSelectedEntity != i
				DRAW_CYLINDER_MARKER_FROM_VECTOR(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].vPos, ciLARGE_VEHICLE_CHECK_POINT_SIZE, 5, HUD_COLOUR_BLUEDARK, 120)
			ENDIF
		ENDFOR
		
		FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfPeds-1
			IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].vPos)
			AND sFMMCMenu.iSelectedEntity != i
				DRAW_CYLINDER_MARKER_FROM_VECTOR(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].vPos, ciLARGE_PED_CHECK_POINT_SIZE, 3, HUD_COLOUR_WHITE, 120)
			ENDIF
		ENDFOR
	ENDIF
	
ENDPROC

PROC SET_ALL_MENU_ITEMS_ACTIVE()
	SET_BIT(sFMMCmenu.iTopMenuActive, SURV_TOP_MENU_SURV_CONFIG)
	SET_BIT(sFMMCmenu.iTopMenuActive, SURV_TOP_MENU_CREATOR)
	IF g_FMMC_STRUCT.bMissionIsPublished = FALSE
	AND SCRIPT_IS_CLOUD_AVAILABLE()
		SET_BIT(sFMMCmenu.iTopMenuActive, SURV_TOP_MENU_SAVE)
	ENDIF
	SET_BIT(sFMMCmenu.iTopMenuActive, SURV_TOP_MENU_TEST)
	SET_BIT(sFMMCmenu.iTopMenuActive, SURV_TOP_MENU_RADIO)
	SET_BIT(sFMMCmenu.iTopMenuActive, SURV_TOP_MENU_OPTIONS)
	SET_BIT(sFMMCmenu.iTopMenuActive, SURV_TOP_MENU_EXIT)
ENDPROC

FUNC BOOL CAN_SURVIVAL_BE_TESTED()
	
	IF NETWORK_IS_SIGNED_ONLINE() = FALSE
	OR NETWORK_IS_SIGNED_IN() = FALSE
	OR IS_BIT_SET(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_NO_CLOUD)
		RETURN FALSE
	ENDIF
	
	IF bRunTutorial
		IF sSurvTutorial.eSurvivalTutStage < SURV_CREATOR_TUTORIAL_STAGE_TEST
		AND NOT IS_BIT_SET(sSurvTutorial.iTutorialBS, ciCREATOR_TUTORIAL__CONTENT_SAVED)
			RETURN FALSE
		ENDIF
	ENDIF

	INT iWave, iEntity, i
	BOOL bCanTest = TRUE
	IF IS_VECTOR_ZERO(g_FMMC_STRUCT.vStartPos)
		SET_MENU_ALERT(sFMMCmenu, ciMENU_ALERT_TRIGGER)
		bCanTest = FALSE
	ELSE
		CLEAR_MENU_ALERT(sFMMCmenu, ciMENU_ALERT_TRIGGER)
	ENDIF
	
	IF NOT HAS_SURVIVAL_BOUNDS_BEEN_PLACED()
		SET_MENU_ALERT(sFMMCmenu, ciMENU_ALERT_PLAYBOUNDS)
		bCanTest = FALSE
	ELSE
		CLEAR_MENU_ALERT(sFMMCmenu, ciMENU_ALERT_PLAYBOUNDS)
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.iNumberOfPeds < MIN_SURV_ENEMY_SPAWNERS
		SET_MENU_ALERT_2(sFMMCmenu, ciMENU_ALERT_2_ENEMY_SPAWN)
		bCanTest = FALSE
	ELSE
		CLEAR_MENU_ALERT_2(sFMMCmenu, ciMENU_ALERT_2_ENEMY_SPAWN)
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[0] != 4
		SET_MENU_ALERT(sFMMCmenu, ciMENU_ALERT_SPAWNS)
		bCanTest = FALSE
	ELSE
		CLEAR_MENU_ALERT(sFMMCmenu, ciMENU_ALERT_SPAWNS)
	ENDIF
	
	IF DO_VEHICLE_SPAWNS_NEED_TO_BE_PLACED()
	AND g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles < MIN_SURV_VEH_SPAWNERS
		SET_MENU_ALERT_2(sFMMCmenu, ciMENU_ALERT_2_NOT_ENOUGH_VEHICLE_SPAWNS)
		bCanTest = FALSE
	ELSE
		CLEAR_MENU_ALERT_2(sFMMCmenu, ciMENU_ALERT_2_NOT_ENOUGH_VEHICLE_SPAWNS)
	ENDIF
	
	FOR iWave = 1 TO g_FMMC_STRUCT.sSurvivalWaveData.iNumberOfWaves
	
		IF g_FMMC_STRUCT.sSurvivalWaveData.iNumberOfSquads[iWave][0] = 0
			SET_MENU_ALERT_2(sFMMCmenu, ciMENU_ALERT_2_SQUADS)
			bCanTest = FALSE
			BREAKLOOP
		ELSE
			CLEAR_MENU_ALERT_2(sFMMCmenu, ciMENU_ALERT_2_SQUADS)
		ENDIF
		
		IF GET_NUMBER_OF_ENEMIES_ON_WAVE(iWave) < 10
			PRINTLN("Wave ", iWave, " doesn't have enough enemies")
			SET_MENU_ALERT_2(sFMMCmenu, ciMENU_ALERT_2_PED_IN_SQUAD)
			bCanTest = FALSE
			BREAKLOOP
		ELSE
			CLEAR_MENU_ALERT_2(sFMMCmenu, ciMENU_ALERT_2_PED_IN_SQUAD)
		ENDIF
		
		FOR iEntity = 0 TO g_FMMC_STRUCT.sSurvivalWaveData.iNumVehicles[iWave]-1
			IF (g_FMMC_STRUCT.sSurvivalWaveData.mnVeh[iWave][iEntity] = DUMMY_MODEL_FOR_SCRIPT)
				SET_MENU_ALERT_2(sFMMCmenu, ciMENU_ALERT_2_INVALID_LAND_VEH)
				bCanTest = FALSE
				BREAKLOOP
			ELSE
				CLEAR_MENU_ALERT_2(sFMMCmenu, ciMENU_ALERT_2_INVALID_LAND_VEH)
			ENDIF
		ENDFOR
		
		FOR iEntity = 0 TO g_FMMC_STRUCT.sSurvivalWaveData.iNumberOfHelis[iWave]-1
			IF g_FMMC_STRUCT.sSurvivalWaveData.mnAir[iWave][iEntity] = DUMMY_MODEL_FOR_SCRIPT
				SET_MENU_ALERT_2(sFMMCmenu, ciMENU_ALERT_2_INVALID_AIR_VEH)
				bCanTest = FALSE
				BREAKLOOP
			ELSE
				CLEAR_MENU_ALERT_2(sFMMCmenu, ciMENU_ALERT_2_INVALID_AIR_VEH)
			ENDIF
		ENDFOR
	ENDFOR	
	
	IF g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[0] > 0
		FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[0]-1
			IF NOT SURVIVAL_IS_POINT_IN_PLAY_AREA(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[0][i].vPos)
				SET_MENU_ALERT_2(sFMMCmenu, ciMENU_ALERT_2_SPAWN_NOT_IN_BOUNDS)
				bCanTest = FALSE
				PRINTLN("CAN_SURVIVAL_BE_TESTED - Spawn point ", i, " is out of bounds")
				BREAKLOOP
			ELSE
				CLEAR_MENU_ALERT_2(sFMMCmenu, ciMENU_ALERT_2_SPAWN_NOT_IN_BOUNDS)
			ENDIF
			IF HAS_SURVIVAL_BOUNDS_GOT_HEIGHT()
				VECTOR vNearTop = GET_SURVIVAL_BOUNDS_POINT_WITH_HEIGHT()
				vNearTop -= g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[0][i].vPos
				IF vNearTop.z < 2.0
					SET_MENU_ALERT(sFMMCmenu, ciMENU_ALERT_2_SPAWN_CLOSE_TO_TOP)
					bCanTest = FALSE
					BREAKLOOP
				ELSE
					CLEAR_MENU_ALERT_2(sFMMCmenu, ciMENU_ALERT_2_SPAWN_CLOSE_TO_TOP)
				ENDIF
			ENDIF
		ENDFOR
	ELSE
		CLEAR_MENU_ALERT_2(sFMMCmenu, ciMENU_ALERT_2_SPAWN_NOT_IN_BOUNDS)
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons > 0
		FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons - 1
			IF NOT SURVIVAL_IS_POINT_IN_PLAY_AREA(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].vPos)
				SET_MENU_ALERT_2(sFMMCmenu, ciMENU_ALERT_2_PICKUP_POSITION_OUT_OF_BOUNDS)
				bCanTest = FALSE
				PRINTLN("CAN_SURVIVAL_BE_TESTED - Pickup Position ", i, " is out of bounds")
				BREAKLOOP
			ELSE
				CLEAR_MENU_ALERT_2(sFMMCmenu, ciMENU_ALERT_2_PICKUP_POSITION_OUT_OF_BOUNDS)
			ENDIF
		ENDFOR
	ELSE
		CLEAR_MENU_ALERT_2(sFMMCmenu, ciMENU_ALERT_2_PICKUP_POSITION_OUT_OF_BOUNDS)
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles > 0
		FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles-1
			FLOAT fDist = VDIST2(SURVIVAL_GET_PLACED_BOUNDS_MIDDLE(), g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].vPos)
			//In play area
			IF SURVIVAL_IS_POINT_IN_PLAY_AREA(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].vPos)
				SET_MENU_ALERT_2(sFMMCmenu, ciMENU_ALERT_2_VEHICLE_IN_PLAY_AREA)
				bCanTest = FALSE
				PRINTLN("CAN_SURVIVAL_BE_TESTED - Vehicle Spawn Point ", i, " is in play area")
				BREAKLOOP
			ELSE
				CLEAR_MENU_ALERT_2(sFMMCmenu, ciMENU_ALERT_2_VEHICLE_IN_PLAY_AREA)
			ENDIF
			//Out of placement range
			IF fDist > POW(MAX_SURVIVAL_BOUNDS_SIZE, 2)
				SET_MENU_ALERT_2(sFMMCmenu, ciMENU_ALERT_2_VEHICLE_SPAWN_OUT_OF_RANGE)
				bCanTest = FALSE
				PRINTLN("CAN_SURVIVAL_BE_TESTED - Vehicle Spawn Point ", i, " is not close enough to play area")
				BREAKLOOP
			ELSE
				CLEAR_MENU_ALERT_2(sFMMCmenu, ciMENU_ALERT_2_VEHICLE_SPAWN_OUT_OF_RANGE)
			ENDIF
		ENDFOR
	ELSE
		CLEAR_MENU_ALERT_2(sFMMCmenu, ciMENU_ALERT_2_VEHICLE_SPAWN_OUT_OF_RANGE)
		CLEAR_MENU_ALERT_2(sFMMCmenu, ciMENU_ALERT_2_VEHICLE_IN_PLAY_AREA)
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.iNumberOfPeds > 0
		FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfPeds-1
			IF VDIST2(SURVIVAL_GET_PLACED_BOUNDS_MIDDLE(), g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].vPos) > POW(GET_SURVIVAL_ENEMY_SPAWN_PLACEMENT_RANGE(), 2)
				SET_MENU_ALERT_2(sFMMCmenu, ciMENU_ALERT_2_ENEMY_SPAWN_NOT_IN_RANGE)
				bCanTest = FALSE
				PRINTLN("CAN_SURVIVAL_BE_TESTED - Enemy Spawn Point ", i, " is not close enough to play area")
				BREAKLOOP
			ELSE
				CLEAR_MENU_ALERT_2(sFMMCmenu, ciMENU_ALERT_2_ENEMY_SPAWN_NOT_IN_RANGE)
			ENDIF
		ENDFOR
	ELSE
		CLEAR_MENU_ALERT_2(sFMMCmenu, ciMENU_ALERT_2_ENEMY_SPAWN_NOT_IN_RANGE)
	ENDIF
	
	
	IF sSurvTutorial.eSurvivalTutStage < SURV_CREATOR_TUTORIAL_STAGE_TEST
	AND bRunTutorial
		bCanTest = FALSE
	ENDIF
	
	RETURN bCanTest
ENDFUNC

FUNC BOOL CAN_SURVIVAL_BE_SAVED()
	IF IS_ROCKSTAR_DEV()
		RETURN TRUE
	ENDIF
	IF IS_STRING_EMPTY(g_FMMC_STRUCT.tl63MissionName)
		SET_MENU_ALERT(sFMMCmenu, ciMENU_ALERT_TITLE)
		RETURN FALSE
	ELSE
		CLEAR_MENU_ALERT(sFMMCmenu, ciMENU_ALERT_TITLE)		
	ENDIF
	
	IF IS_STRING_EMPTY(g_FMMC_STRUCT.tl63MissionDecription[0])
		SET_MENU_ALERT(sFMMCmenu, ciMENU_ALERT_DESC)
		RETURN FALSE
	ELSE
		CLEAR_MENU_ALERT(sFMMCmenu, ciMENU_ALERT_DESC)
	ENDIF
	IF IS_VECTOR_ZERO(g_FMMC_STRUCT.vStartPos)
		SET_MENU_ALERT(sFMMCmenu, ciMENU_ALERT_TRIGGER)
		RETURN FALSE
	ELSE
		CLEAR_MENU_ALERT(sFMMCmenu, ciMENU_ALERT_TRIGGER)
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC HANDLE_TEST_MODE_AVAILABLE()
	IF CAN_SURVIVAL_BE_TESTED()
		IF NOT IS_BIT_SET(sFMMCmenu.iTopMenuActive, SURV_TOP_MENU_TEST)
			PRINTLN("HANDLE_TEST_MODE_AVAILABLE - Test mode is now available")
			SET_BIT(sFMMCmenu.iTopMenuActive, SURV_TOP_MENU_TEST)
			REFRESH_MENU_ASAP(sFMMCmenu)
		ENDIF
	ELSE
		IF IS_BIT_SET(sFMMCmenu.iTopMenuActive, SURV_TOP_MENU_TEST)
			CLEAR_BIT(sFMMCmenu.iTopMenuActive, SURV_TOP_MENU_TEST)
		ENDIF
	ENDIF
ENDPROC

PROC HANDLE_SAVE_AVAILABLE()
	IF CAN_SURVIVAL_BE_SAVED()
		IF NOT IS_BIT_SET(sFMMCmenu.iTopMenuActive, SURV_TOP_MENU_SAVE)
			PRINTLN("HANDLE_SAVE_AVAILABLE - Save is now available")
			SET_BIT(sFMMCmenu.iTopMenuActive, SURV_TOP_MENU_SAVE)
		ENDIF
	ELSE
		IF IS_BIT_SET(sFMMCmenu.iTopMenuActive, SURV_TOP_MENU_SAVE)
			CLEAR_BIT(sFMMCmenu.iTopMenuActive, SURV_TOP_MENU_SAVE)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL CAN_SURVIVAL_BE_PUBLISHED()
	
	IF IS_STRING_EMPTY(g_FMMC_STRUCT.tl63MissionName)
		SET_MENU_ALERT(sFMMCmenu, ciMENU_ALERT_TITLE)
	ELSE
		CLEAR_MENU_ALERT(sFMMCmenu, ciMENU_ALERT_TITLE)		
	ENDIF
	
	IF IS_STRING_EMPTY(g_FMMC_STRUCT.tl63MissionDecription[0])
		SET_MENU_ALERT(sFMMCmenu, ciMENU_ALERT_DESC)
	ELSE
		CLEAR_MENU_ALERT(sFMMCmenu, ciMENU_ALERT_DESC)
	ENDIF
	
	IF IS_VECTOR_ZERO(g_FMMC_STRUCT.vStartPos)
		SET_MENU_ALERT(sFMMCmenu, ciMENU_ALERT_TRIGGER)
	ELSE
		CLEAR_MENU_ALERT(sFMMCmenu, ciMENU_ALERT_TRIGGER)
	ENDIF

	IF IS_VECTOR_ZERO(g_FMMC_STRUCT.vCameraPanPos)
		SET_MENU_ALERT(sFMMCmenu, ciMENU_ALERT_INTROCAM)
	ELSE
		CLEAR_MENU_ALERT(sFMMCmenu, ciMENU_ALERT_INTROCAM)
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iPhoto, FMMC_PHOTO_TAKEN)
		SET_MENU_ALERT(sFMMCmenu, ciMENU_ALERT_PHOTO)
	ELSE
		CLEAR_MENU_ALERT(sFMMCmenu, ciMENU_ALERT_PHOTO)
	ENDIF
	
	IF NOT SCRIPT_IS_CLOUD_AVAILABLE()
		IF NOT IS_BIT_SET(sFMMCdata.iBitSet, biTestMissionActive)
			IF bCreatorLimitedCloudDown = FALSE
				sCurrentVarsStruct.iEntityCreationStatus = STAGE_CLOUD_FAILURE
			ENDIF
		ENDIF
		SET_BIT(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_NO_CLOUD)
		CLEAR_BIT(sFMMCmenu.iOptionsMenuBitSet, OPTION_SURVIVAL_TITLE)
		CLEAR_BIT(sFMMCmenu.iOptionsMenuBitSet, OPTION_SURVIVAL_DESCRIPTION)
	ELSE
		IF NOT IS_BIT_SET(sFMMCdata.iBitSet, biTestMissionActive)
			IF bCreatorLimitedCloudDown = TRUE
				sCurrentVarsStruct.iEntityCreationStatus = STAGE_CLOUD_FAILURE
			ENDIF
		ENDIF
		CLEAR_BIT(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_NO_CLOUD)
		SET_BIT(sFMMCmenu.iOptionsMenuBitSet, OPTION_SURVIVAL_TITLE)
		SET_BIT(sFMMCmenu.iOptionsMenuBitSet, OPTION_SURVIVAL_DESCRIPTION)
	ENDIF
		
	IF g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[0] != 4
		SET_MENU_ALERT(sFMMCmenu, ciMENU_ALERT_SPAWNS)
	ELSE
		CLEAR_MENU_ALERT(sFMMCmenu, ciMENU_ALERT_SPAWNS)
	ENDIF	
	
	IF NETWORK_PLAYER_IS_CHEATER()		
		SET_MENU_ALERT(sFMMCmenu, ciMENU_ALERT_CHEATER)
	ENDIF
	
	IF SCRIPT_IS_CLOUD_AVAILABLE()
		IF NOT CAN_I_PUBLISH_WITH_CURRENT_PRIVILEGES()
			SET_BIT(sFMMCmenu.iBitMenuItemAlert, ciMENU_ALERT_BLOCKED)
			PRINTLN("[ALERTBITS] ciMENU_ALERT_BLOCKED set")
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.biTeamTestComplete, 0)
	AND IS_BIT_SET(g_FMMC_STRUCT.sSurvivalWaveData.iTestedWavesBitSet, 0)
		CLEAR_MENU_ALERT(sFMMCmenu, ciMENU_ALERT_UNTESTED)
	ELSE
		SET_MENU_ALERT(sFMMCmenu, ciMENU_ALERT_UNTESTED)
	ENDIF
	
	
	IF NOT IS_ROCKSTAR_DEV()
		IF NOT IS_BIT_SET_ALERT(sFMMCmenu)
			CLEAR_BIT(g_FMMC_STRUCT.iMissionTypeBitSet, ciMISSION_OPTION_BS_MISSION_IS_NOT_PLAYABLE)
		ELSE
			SET_BIT(g_FMMC_STRUCT.iMissionTypeBitSet, ciMISSION_OPTION_BS_MISSION_IS_NOT_PLAYABLE)
		ENDIF
	
		IF IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciMISSION_OPTION_BS_MISSION_IS_NOT_PLAYABLE)
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC HANDLE_PUBLISH_AVAILABLE()
	IF CAN_SURVIVAL_BE_PUBLISHED()
		IF NOT IS_BIT_SET(sFMMCmenu.iTopMenuActive, SURV_TOP_MENU_PUBLISH)
			PRINTLN("HANDLE_SAVE_AVAILABLE - Publish is now available")
			SET_BIT(sFMMCmenu.iTopMenuActive, SURV_TOP_MENU_PUBLISH)
		ENDIF
	ELSE
		IF IS_BIT_SET(sFMMCmenu.iTopMenuActive, SURV_TOP_MENU_PUBLISH)
			CLEAR_BIT(sFMMCmenu.iTopMenuActive, SURV_TOP_MENU_PUBLISH)
		ENDIF
	ENDIF
ENDPROC

PROC INIT_LOADOUT_VARS()
	INT iWave, iWeapon
	
	FOR iWave = 0 TO g_FMMC_STRUCT.sSurvivalWaveData.iNumberOfWaves
		
		PRINTLN("INIT_LOADOUT_VARS - Wave ", iWave, " =========================")
		FOR iWeapon = 0 TO MAX_LOADOUT_WEAPONS-1
		
			PICKUP_TYPE ptPickup = GET_PICKUP_TYPE_FROM_WEAPON_HASH(g_FMMC_STRUCT.sSurvivalWaveData.wtLoadoutWeapons[iWave][iWeapon])
			INT iType = GET_WEAPON_SELECTION_FROM_PICK_UP_TYPE(sWepStruct, ptPickup, TRUE, g_FMMC_STRUCT_ENTITIES.sEditedWeapon.iSubType) 
			INT iLibrary = GET_SELECTED_WEAPON_LIBRARY(iType, TRUE)
			
			g_FMMC_STRUCT.sSurvivalWaveData.iLoadoutWeaponCat[iWave][iWeapon]	= iLibrary
			g_FMMC_STRUCT.sSurvivalWaveData.iLoadoutWeaponType[iWave][iWeapon]		= iType
			PRINTLN("INIT_LOADOUT_VARS - Setting iLoadoutWeaponCat[",iWave,"][",iWeapon,"] to ", iLibrary, " | Setting iLoadoutWeaponType[",iWave,"][,",iWeapon,"] to ", iType)
		ENDFOR
	ENDFOR
ENDPROC

FUNC MODEL_NAMES FIND_FIRST_VALID_VEHICLE()
	INT iWave
	FOR iWave = 0 TO g_FMMC_STRUCT.sSurvivalWaveData.iNumberOfWaves-1
		IF g_FMMC_STRUCT.sSurvivalWaveData.iNumVehicles[iWave] > 0
			RETURN g_FMMC_STRUCT.sSurvivalWaveData.mnVeh[iWave][0]
		ENDIF
	ENDFOR
	
	RETURN g_FMMC_STRUCT.sSurvivalWaveData.mnVeh[0][0]
ENDFUNC

PROC PROCESS_PRE_GAME()

	SET_BIT(sFMMCdata.iBitSet, bCameraActive)
	SET_CREATOR_AUDIO(TRUE, FALSE)
	SETUP_FMMC_SCRIPT(sFMMCMenu, sHCS, sCurrentVarsStruct)
	INITIALISE_MODEL_ARRAYS(sPedStruct,	sWepStruct, sObjStruct, sFMMCmenu)
	
	sFMMCmenu.iScriptCreationType	= SCRIPT_CREATION_TYPE_MISSION
	INT i, i2
	IF g_FMMC_STRUCT_ENTITIES.iNumberOfSpawnPoints > 0
		FMMCSpawnPointStruct sTempPedStruct
		FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfSpawnPoints-1
			IF DOES_BLIP_EXIST(sPedStruct.biPedBlip[i])
				REMOVE_BLIP(sPedStruct.biPedBlip[i])
			ENDIF
			REMOVE_DECAL(sPedStruct.diDecal[i])
			DELETE_PED(sPedStruct.piPed[i])
			sPedStruct.biPedBlip[i] 	= NULL
			g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[i] = sTempPedStruct
			
		ENDFOR
		g_FMMC_STRUCT_ENTITIES.iNumberOfSpawnPoints = 0
		PRINTLN("PROCESS_PRE_GAME - CLEARING ALL OLD SPAWNPOINT DATA")
	ENDIF
	
	FOR i = 0 TO PROP_LIBRARY_MAX_ITEMS-1
		sFMMCmenu.iNumberOfPropsInLibrary[i] = -1
	ENDFOR
	
	CLEAR_CACHED_AVAILABLE_VEHICLE_COUNTS()
	FOR i = 0 TO VEHICLE_LIBRARY_MAX-1
		GET_NUMBER_OF_VEHICLES_IN_LIBRARY(i)
	ENDFOR
	g_FMMC_STRUCT.tl63MissionName = ""
	g_FMMC_STRUCT.tl63MissionDecription[0] = ""
	g_FMMC_STRUCT.iMinNumParticipants = 1
	g_FMMC_STRUCT.iMaxNumberOfTeams = 1
	
	IF g_FMMC_STRUCT.iMusic = -1
		g_FMMC_STRUCT.iMusic = GET_RANDOM_INT_IN_RANGE(0, ciRC_MAX_TYPE_RACE_RADIO)
		STRING sTempRadio
		sTempRadio = GET_RADIO_STATION_NAME(g_FMMC_STRUCT.iMusic)
		g_FMMC_STRUCT.iMusic = GET_HASH_KEY(sTempRadio)
	ENDIF
	
	sCurrentVarsStruct.mnCurrentSurvivalVeh = FIND_FIRST_VALID_VEHICLE()
	
	g_iStartingWave = 1
	g_iTestType = 0
	
	SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetSix,  ciBLOCK_AMMO_PURCHASE)
	SET_BIT(g_FMMC_STRUCT.iRuleOptionsBitSet,ciDISABLE_MERCENARIES)
	SET_BIT(g_FMMC_STRUCT.iRuleOptionsBitSet, ciDISABLE_AMMO_DROP)
	SET_BIT(g_FMMC_STRUCT.iRuleOptionsBitSet, ciDISABLE_REVEAL_PLAYERS)
	SET_BIT(g_FMMC_STRUCT.iRuleOptionsBitSet, ciDISABLE_REQUEST_VEHICLE)
	SET_BIT(g_FMMC_STRUCT.iRuleOptionsBitSet, ciDISABLE_HELI_PICKUP)
	SET_BIT(g_FMMC_STRUCT.iRuleOptionsBitSet, ciDISABLE_BACKUP_HELI)
	SET_BIT(g_FMMC_STRUCT.iRuleOptionsBitSet, ciDISABLE_AIRSTRIKE)
	SET_BIT(g_FMMC_STRUCT.iRuleOptionsBitSet, ciDISABLE_BULL_SHARK)
	SET_BIT(g_FMMC_STRUCT.iRuleOptionsBitSet, ciDISABLE_BOAT_PICKUP)
	SET_BIT(g_FMMC_STRUCT.iRuleOptionsBitSet, ciDISABLE_PEGASUS_VEHICLE)
	
	g_FMMC_STRUCT.iAdversaryModeType = ciNEWVS_NEW_SURVIVAL
	
	IF g_FMMC_STRUCT.sSurvivalWaveData.mnPedModel[0][0] = DUMMY_MODEL_FOR_SCRIPT
		g_FMMC_STRUCT.sSurvivalWaveData.mnPedModel[0][0] = GET_SURVIVAL_MODEL_DATA_DEFAULT(ciSURVIVAL_DATA_PED_MODEL, 0, 0)
	ENDIF
	
	IF g_FMMC_STRUCT.sSurvivalWaveData.mnVeh[0][0] = DUMMY_MODEL_FOR_SCRIPT
		g_FMMC_STRUCT.sSurvivalWaveData.mnVeh[0][0] = GET_SURVIVAL_MODEL_DATA_DEFAULT(ciSURVIVAL_DATA_VEH_MODEL, 0, 0)
	ENDIF
	
	IF g_FMMC_STRUCT.sSurvivalWaveData.mnAir[0][0] = DUMMY_MODEL_FOR_SCRIPT
		g_FMMC_STRUCT.sSurvivalWaveData.mnAir[0][0] = GET_SURVIVAL_MODEL_DATA_DEFAULT(ciSURVIVAL_DATA_AIR_MODEL, 0, 0)
	ENDIF

	FOR i = 0 TO FMMC_MAX_TEAMS-1
		iMaxSpawnsPerTeam[i] = MAX_SURVIVAL_SPAWN_POINTS
	ENDFOR
	
	IF g_bFMMC_TutorialSelectedFromMpSkyMenu = TRUE
		bRunTutorial = TRUE
	ENDIF
	
	//Data Correction
	FOR i = 0 TO MAX_SURVIVAL_WAVES-1
		FOR i2 = 0 TO g_FMMC_STRUCT.sSurvivalWaveData.iNumVehicles[i]-1
			IF g_FMMC_STRUCT.sSurvivalWaveData.iVehColour[i][i2] = ciNUMBER_OF_VEHICLE_COLOURS
				g_FMMC_STRUCT.sSurvivalWaveData.iVehColour[i][i2] = -1
				PRINTLN("PROCESS_PRE_GAME - Correcting vehicle colour from 47 to -1 for vehicle ", i)
			ENDIF
		ENDFOR
		
		FOR i2 = 0 TO g_FMMC_STRUCT.sSurvivalWaveData.iNumberOfHelis[i]-1
			IF g_FMMC_STRUCT.sSurvivalWaveData.iAirColour[i][i2] = ciNUMBER_OF_VEHICLE_COLOURS
				g_FMMC_STRUCT.sSurvivalWaveData.iAirColour[i][i2] = -1
				PRINTLN("PROCESS_PRE_GAME - Correcting air vehicle colour from 47 to -1 for vehicle ", i)
			ENDIF
		ENDFOR
		
		FOR i2 = 0 TO MAX_SURV_WAVE_DIFF-1
			IF g_FMMC_STRUCT.sSurvivalWaveData.iHealth[i][i2] < 125
				g_FMMC_STRUCT.sSurvivalWaveData.iHealth[i][i2] = 125
				PRINTLN("PROCESS_PRE_GAME - Correcting squad health for wave ", i, " Stage: ", i2)
			ENDIF
		ENDFOR
	ENDFOR
	
	SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOPTIONSBS18_DISABLE_ATM_USE_IN_MISSIONS)
	
	INIT_LOADOUT_VARS()
	
	g_FMMC_STRUCT.iTeamDeathmatch = 0
	
	g_iSurvivalEndReason = -1
	PRINTLN("Survival Creator PROCESS_PRE_GAME - Setting g_iSurvivalEndReason to -1")
	
	PRINTLN("Survival Creator PROCESS_PRE_GAME done.")
	
ENDPROC

PROC SCRIPT_CLEANUP(BOOL bSetWorldActive)

	DEBUG_PRINTCALLSTACK()
	
	INT i
	
	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("FM_Main_Menu")) = 0
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	ENDIF
	
	SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(SF_Movie_Gallery_Shutter_Index)	
	
	IF DOES_CAM_EXIST(sCamData.cam)
		DESTROY_CAM(sCamData.cam)
	ENDIF
	
	IF DOES_BLIP_EXIST(sStartEndBlips.biStart)
		REMOVE_BLIP(sStartEndBlips.biStart)
	ENDIF
	
	IF sStartEndBlips.ciStartType != NULL
		DELETE_CHECKPOINT(sStartEndBlips.ciStartType)
		sStartEndBlips.ciStartType = NULL
	ENDIF
	
	IF DOES_BLIP_EXIST(bCameraPanBlip)
		REMOVE_BLIP(bCameraPanBlip)
	ENDIF
	IF DOES_BLIP_EXIST(bPhotoBlip)
		REMOVE_BLIP(bPhotoBlip)
	ENDIF
	
	IF DOES_ENTITY_EXIST(sCurrentVarsStruct.viMapEscapeVeh)
		PRINTLN("SCRIPT_CLEANUP - Cleaning up viMapEscapeVeh")
		DELETE_VEHICLE(sCurrentVarsStruct.viMapEscapeVeh)
	ENDIF
	
	CLEAR_CACHED_AVAILABLE_VEHICLE_COUNTS()
	
	g_bFMMC_TutorialSelectedFromMpSkyMenu = FALSE
	COMMON_FMMC_SCRIPT_CLEAN_UP(sPedStruct,	sVehStruct,	sWepStruct,	sObjStruct,sPropStruct, sDynoPropStruct, sInvisibleObjects, sCurrentVarsStruct, sFMMCmenu, TRUE, bSetWorldActive)
	iDoorSetupStage = 0
	
	CLEANUP_MENU_ASSETS()
	
	IF IS_FAKE_MULTIPLAYER_MODE_SET()
		IF SCRIPT_IS_CLOUD_AVAILABLE()
			PRINTLN("SCRIPT_IS_CLOUD_AVAILABLE()")
		    SET_FAKE_MULTIPLAYER_MODE(FALSE)
		ELSE
			PRINTLN("NOT SCRIPT_IS_CLOUD_AVAILABLE()")
			IF NOT IS_ANY_TRANSITION_SESSION_INVITE_BEING_ACTIONED()
				NETWORK_SESSION_LEAVE_SINGLE_PLAYER() 
			ENDIF
			g_Private_IsMultiplayerCreatorRunning = FALSE
			g_Private_MultiplayerCreatorNeedsToEnd = TRUE
		ENDIF
	ENDIF
	
	SET_CREATOR_AUDIO(FALSE, FALSE)
	
	CLEANUP_ALL_INTERIORS(g_ArenaInterior)
	
	g_FMMC_STRUCT.iMissionType = 0
	
	DO_BLIMP_SIGNS(sBlimpSign, TRUE)
	
	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfDoors i
		RESET_DOOR_TO_WORLD_STATE(i, sCurrentVarsStruct, TRUE)
		IF NOT IS_SPECIAL_CASE_DOOR(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[i].mnDoorModel)
			IF IS_DOOR_REGISTERED_WITH_SYSTEM(sCurrentVarsStruct.iMissionControlledDoors[i])
				REMOVE_DOOR_FROM_SYSTEM(sCurrentVarsStruct.iMissionControlledDoors[i])
			ENDIF
		ENDIF
	ENDREPEAT
	
	DELETE_PED(pedSelection)
	DELETE_VEHICLE(vehSelection)
	
	g_bOnHorde = FALSE
	
	RESET_SCRIPT_GFX_ALIGN()
	
	TERMINATE_THIS_THREAD()
ENDPROC

PROC HANDLE_ENTITY_CREATION()
	BOOL bCanDoPlacementCircle 
	SWITCH sFMMCmenu.iEntityCreation
		CASE -1
			sCurrentVarsStruct.iHoverEntityType  = -1
			IF sFMMCmenu.sActiveMenu != eFmmc_TOP_MENU
			AND sFMMCmenu.sActiveMenu != eFmmc_MAIN_OPTIONS_BASE
				UPDATE_THE_CORONA_DISC_VALUES(sCurrentVarsStruct, NULL, TRUE, TRUE, fDEFAULT_PLACEMENT_DISC_HEIGHT)
			ELSE
				UPDATE_THE_CORONA_DISC_VALUES(sCurrentVarsStruct, NULL, TRUE, TRUE, fDEFAULT_PLACEMENT_DISC_HEIGHT, FALSE)
			ENDIF
			
			IF HAS_NET_TIMER_STARTED(tdZoneTimer)
				RESET_NET_TIMER(tdZoneTimer)
			ENDIF
		BREAK
		
		CASE CREATION_TYPE_TRIGGER
			IF DO_TRIGGER_CREATION(sFMMCmenu, sCurrentVarsStruct, sFMMCdata, sStartEndBlips.biStart, sHCS.hcStartCoronaColour,sStartEndBlips.ciStartType, sFMMCendStage, iTriggerCreationStage, 3)
				SET_ALL_MENU_ITEMS_ACTIVE()
			ENDIF
		BREAK
		
		CASE CREATION_TYPE_PAN_CAM
			BOOL bResetCamMenu
			MAINTAIN_PLACING_INTRO_CAMERAS(sFMMCMenu, bCameraTriggerBlip, bResetCamMenu, bInitialIntroCamSetup)
			UPDATE_THE_CORONA_DISC_VALUES(sCurrentVarsStruct, NULL, TRUE, TRUE, fDEFAULT_PLACEMENT_DISC_HEIGHT, FALSE)
			IF bResetCamMenu
				REFRESH_MENU(sFMMCMenu)
				sCurrentVarsStruct.bResetUpHelp = TRUE
			ENDIF
		BREAK
		
		CASE CREATION_TYPE_VEHICLES
			bCanDoPlacementCircle = TRUE
			
			IF bRunTutorial
				IF sSurvTutorial.eSurvivalTutStage = SURV_CREATOR_TUTORIAL_STAGE_VEHICLE_SPAWN_POINTS
					IF sSurvTutorial.iSubstage < 4
						bCanDoPlacementCircle = FALSE
					ENDIF
				ELIF sSurvTutorial.eSurvivalTutStage < SURV_CREATOR_TUTORIAL_STAGE_VEHICLE_SPAWN_POINTS
					bCanDoPlacementCircle = FALSE
				ENDIF
			ENDIF
			IF NOT DOES_ENTITY_EXIST(sVehStruct.viCoronaVeh)
				bCanDoPlacementCircle = FALSE
			ENDIF
			IF bCanDoPlacementCircle
				HUD_COLOURS hcRingColour
				IF sCurrentVarsStruct.bAreaIsGoodForPlacement = FALSE
					hcRingColour = HUD_COLOUR_RED
				ELSE
					hcRingColour = HUD_COLOUR_BLUEDARK
				ENDIF
				VECTOR vFront, vBack
				vFront = sCurrentVarsStruct.vCoronaPos
				vBack = sCurrentVarsStruct.vCoronaPos
				vFront = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sVehStruct.viCoronaVeh, <<0.0,8.75, -2>>)
				vBack = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sVehStruct.viCoronaVeh, <<0.0,-8.75, -2>>)
				DRAW_BOX_MARKER_FROM_ANGLED_AREA(vFront, vBack, ciLARGE_VEHICLE_CHECK_POINT_SIZE/2, hcRingColour, DEFAULT, DEFAULT, DEFAULT, 5.0)
			ENDIF
			DO_VEHICLE_CREATION(sVehStruct, sPedStruct, sObjStruct, sTrainCreationStruct, sCurrentVarsStruct, sHCS, sFMMCmenu, sFMMCdata, sFMMCendStage, GET_MAX_VEHICLE_SPAWNS_FOR_SURVIVAL(), iLocalBitSet)
		BREAK
		
		CASE CREATION_TYPE_WEAPONS
			DO_WEAPON_CREATION(sWepStruct, sObjStruct, sCurrentVarsStruct, sHCS, sFMMCmenu, sFMMCdata,sFMMCendStage, MAX_SURVIVAL_WEAPONS, g_FMMC_STRUCT.iVehicleDeathmatch = 0, iLocalBitSet)
		BREAK
		
		CASE CREATION_TYPE_PROPS
			DO_PROP_CREATION(sPropStruct, sVehStruct, sObjStruct, sCurrentVarsStruct, sHCS, sFMMCmenu, sFMMCdata,sFMMCendStage, sCamData, sPedStruct, sTrainCreationSTruct, GET_FMMC_MAX_NUM_PROPS(), iLocalBitSet)
		BREAK
		
		CASE CREATION_TYPE_DYNOPROPS
			DO_DYNOPROP_CREATION(sDynoPropStruct, sPropStruct, sVehStruct, sCurrentVarsStruct, sHCS, sFMMCmenu, sFMMCdata,sFMMCendStage, sCamData, sPedStruct, sTrainCreationStruct, FMMC_MAX_NUM_DYNOPROPS, iLocalBitSet)
		BREAK
		
		CASE CREATION_TYPE_GOTO_LOC
			DO_LOCATION_CREATION(g_CreatorsSelDetails, sLocStruct, sCurrentVarsStruct, sFMMCmenu, sFMMCdata, sFMMCendStage, FMMC_MAX_SURVIVAL_LOCATIONS)
		BREAK
		
		CASE CREATION_TYPE_SPAWN_LOCATION 									
			DO_SPAWN_POINT_CREATION(sPedStruct, sCurrentVarsStruct, sHCS, sFMMCmenu, sFMMCdata, sFMMCendStage, MAX_SURVIVAL_SPAWN_POINTS, FALSE, IS_BIT_SET(iLocalBitSet, biCanEditEntityInCorona))
		BREAK
		
		CASE CREATION_TYPE_TEAM_SPAWN_LOCATION
			DO_TEAM_SPAWN_POINT_CREATION(sTeamSpawnStruct[0], sCurrentVarsStruct, sHCS, sFMMCmenu, sFMMCdata, sFMMCendStage,sVehStruct, iMaxSpawnsPerTeam, IS_BIT_SET(iLocalBitSet, biCanEditEntityInCorona))
		BREAK
		
		CASE CREATION_TYPE_WORLD_PROPS
			DO_WORLD_PROP_SELECTION(sFMMCmenu, sCurrentVarsStruct)
		BREAK
		
		CASE CREATION_TYPE_DOOR
			DO_DOOR_SELECTION(sCurrentVarsStruct, sFMMCmenu, sFMMCdata)
		BREAK
		
		CASE CREATION_TYPE_PEDS
			sCurrentVarsStruct.bForceCoronaSizeCheck = TRUE
			sCurrentVarsStruct.fCheckPointSize = ciLARGE_PED_CHECK_POINT_SIZE
			bCanDoPlacementCircle = TRUE
			
			IF bRunTutorial
				IF sSurvTutorial.eSurvivalTutStage = SURV_CREATOR_TUTORIAL_STAGE_ENEMY_SPAWN_POINTS
					IF sSurvTutorial.iSubstage < 4
						bCanDoPlacementCircle = FALSE
					ENDIF
				ELIF sSurvTutorial.eSurvivalTutStage < SURV_CREATOR_TUTORIAL_STAGE_ENEMY_SPAWN_POINTS
					bCanDoPlacementCircle = FALSE
				ENDIF
			ENDIF
			
			IF bCanDoPlacementCircle
				HUD_COLOURS hcRingColour
				IF sCurrentVarsStruct.bAreaIsGoodForPlacement = FALSE
					hcRingColour = HUD_COLOUR_RED
				ELSE
					hcRingColour = HUD_COLOUR_WHITE
				ENDIF
				DRAW_CYLINDER_MARKER_FROM_VECTOR(sCurrentVarsStruct.vCoronaPos, ciLARGE_PED_CHECK_POINT_SIZE, 3, hcRingColour, 120)
			ENDIF
			DO_PED_CREATION(sPedStruct, sVehStruct, sCurrentVarsStruct, sHCS, sFMMCmenu, sFMMCdata, sFMMCendStage, sTrainCreationStruct, GET_MAX_PED_SPAWNS_FOR_SURVIVAL())
		BREAK
		
		CASE CREATION_TYPE_FMMC_ZONE
			IF NOT HAS_NET_TIMER_STARTED(tdZoneTimer)
				REINIT_NET_TIMER(tdZoneTimer)
			ELIF HAS_NET_TIMER_EXPIRED(tdZoneTimer, 500)
				DO_ZONE_CREATION(sCurrentVarsStruct, sFMMCmenu, sFMMCendStage)
				DRAW_GENERIC_BIG_DOUBLE_NUMBER(g_FMMC_STRUCT_ENTITIES.iNumberOfZones,  GET_MAX_PLACED_ZONES(),	"FMMCCMENU_18")
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC CONTROL_CAMERA_AND_CORONA()
	IF NOT IS_BIT_SET(sFMMCdata.iBitSet, biTestMissionActive)
		
		CLEAR_LONG_BIT(sCurrentVarsStruct.iTooManyEntities, ciDRAW_CORONA)

		DEAL_WITH_HITTING_MAX_ENTITES(sCurrentVarsStruct, sFMMCmenu)
		
		IF NOT IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
			MAINTAIN_FMMC_CAMERA(sFMMCdata, sCamData, sCurrentVarsStruct.iMenuState, DEFAULT, FALSE)
		ELSE
			IF IS_PLAYER_IN_A_MAP_ESCAPE(PLAYER_PED_ID())
				SWAP_CAMERAS(sCamData, sFMMCdata, sFMMCMenu, sCurrentVarsStruct,FALSE,FALSE,TRUE,FALSE)
			ENDIF
		ENDIF
		
		IF sFMMCmenu.sActiveMenu = eFmmc_SURVIVAL_WARNING_AREA
		OR sFMMCmenu.sActiveMenu = eFmmc_SURVIVAL_ENEMY_AREA
			IF NOT sCurrentVarsStruct.bAreaIsGoodForPlacement
				sHCS.hcCurrentCoronaColour = sHCS.hcColourCantPlace
			ENDIF
		ENDIF
		
		IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()	
		AND sFMMCmenu.sActiveMenu != eFmmc_PAN_CAM_BASE
		AND sFMMCmenu.sActiveMenu != eFmmc_PHOTO_CAM_BASE
			
			VECTOR vDummyRot = <<0,0,0>>
			IF DOES_CAM_EXIST(sCamData.cam)
				vDummyRot = GET_CAM_ROT(sCamData.cam)
			ENDIF
			
			MAINTAIN_SCREEN_CENTER_COORD(vDummyRot, sInvisibleObjects, sCurrentVarsStruct, sFMMCmenu, TRUE, 	sHCS.hcCurrentCoronaColour, sFMMCmenu.fCreationHeightIncrease, sFMMCmenu.fCreationHeightIncrease != 0.0, FALSE, IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround), FALSE, g_FMMC_STRUCT.iVehicleDeathmatch = 1)
		ELSE
			DISABLE_CAMERA_VIEW_MODE_CYCLE(PLAYER_ID())
		ENDIF
	ENDIF
ENDPROC

PROC HANDLE_FAIL_REASONS()
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
		VECTOR vAreaMid
		vAreaMid = SURVIVAL_GET_PLACED_BOUNDS_MIDDLE()
		vAreaMid.z = g_FMMC_STRUCT.sSurvivalWaveData.vBounds[0].z
		FLOAT fDistance = VDIST2(vAreaMid, sCurrentVarsStruct.vCoronaPos)
		IF sFMMCmenu.sActiveMenu = eFmmc_SURVIVAL_ENEMY_AREA
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_SURV_E_SPWN_PLACE
				IF fDistance > POW(MAX_SURVIVAL_BOUNDS_SIZE, 2)
					bFailHit = TRUE
					SET_THE_PLACEMENT_FAIL_AS_THIS_REASON(sCurrentVarsStruct, bNotInRangeOfBounds)
				ENDIF
			ENDIF
		ELIF sFMMCmenu.sActiveMenu = eFmmc_SURVIVAL_WARNING_AREA
			IF (CAN_PLACE_BOUNDS_ON_THIS_OPTION(sFMMCmenu) AND sFMMCMenu.iBoundsEditPointIndex = -1)
			AND NOT HAS_SURVIVAL_BOUNDS_BEEN_PLACED()
				IF fDistance < POW(MIN_SURVIVAL_BOUNDS_SIZE, 2)
					bFailHit = TRUE
					SET_THE_PLACEMENT_FAIL_AS_THIS_REASON(sCurrentVarsStruct, bBoundsTooSmall)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF bFailHit
		PLAY_SOUND_FRONTEND(-1, "ERROR", GET_SOUND_SET_FROM_CREATOR_TYPE())
		PRINTSTRING("ERROR SOUND EFFECT")PRINTNL()
		sCurrentVarsStruct.bDisplayFailReason = TRUE
		sCurrentVarsStruct.iFailDescriptionTimer = GET_GAME_TIMER()
		sCurrentVarsStruct.sFailReason = GET_GENERIC_PLACEMENT_FAIL_DESCRIPTION(sCurrentVarsStruct)
		bFailHit = FALSE
	ENDIF
ENDPROC

PROC DRAW_MENU_SELECTION()
	
	// Clear all the help bits before checking
	clear_BIT(iHelpBitSet, biWarpToCoronaButton) 
	clear_BIT(iHelpBitSet, biWarpToCameraButton) 	
	CLEAR_BIT(iHelpBitSet, biPickupEntityButton)
	CLEAR_BIT(iHelpBitSet, biDeleteEntityButton)	
	sCurrentVarsStruct.bSelectedAnEntity = FALSE
		
	IF IS_BIT_SET(iLocalBitSet, biEntityInCorona)
		sCurrentVarsStruct.bSelectedAnEntity = TRUE
	ENDIF
	IF sFMMCMenu.iSelectedEntity = -1
		IF IS_BIT_SET(iLocalBitSet, biEntityInCorona)
		AND (sCurrentVarsStruct.iHoverEntityType = sFMMCmenu.iEntityCreation)
			IF IS_BIT_SET(iLocalBitSet, biCanEditEntityInCorona)
				SET_BIT(iHelpBitSet, biPickupEntityButton)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(iLocalBitSet, biEntityInCorona)
			SET_BIT(iHelpBitSet, biDeleteEntityButton)
		ENDIF
	ENDIF
	
	IF sFMMCMenu.iSelectedEntity = -1		
		IF IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
			SET_BIT(iHelpBitSet, biWarpToCameraButton) 
		ELSE
			SET_BIT(iHelpBitSet, biWarpToCoronaButton)
		ENDIF
	ENDIF
	
	BOOL bRefresh
	IF iHelpBitSetOld != iHelpBitSet
		iHelpBitSetOld = iHelpBitSet
		PRINTLN("bRefresh = TRUE")
		bRefresh = TRUE
	ENDIF
	
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		IF NOT IS_GAMEPLAY_CAM_RENDERING()
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, GET_CREATOR_CAMERA_ORBIT_TOGGLE_BUTTON())
			OR IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, GET_CREATOR_CAMERA_ORBIT_TOGGLE_BUTTON())
				PRINTLN("bRefresh = TRUE - FORCED BY HOLDING LSHIFT DOWN")
				bRefresh = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	// Set the current item and draw menu each frame
	IF sFMMCmenu.iCurrentMenuLength <= 10
		iTopItem = 0
	ENDIF
	SET_TOP_MENU_ITEM(iTopItem)
	SET_CURRENT_MENU_ITEM(GET_CREATOR_MENU_SELECTION(sFMMCmenu))
		
	IF sFMMCmenu.sActiveMenu = eFmmc_PHOTO_CAM_BASE
	OR sFMMCmenu.sActiveMenu = eFmmc_PAN_CAM_BASE
		sCurrentVarsStruct.bDisplaySelected = FALSE
	ENDIF
	
	PRINTLN("sFailReason = ", sCurrentVarsStruct.sFailReason)
	IF sCurrentVarsStruct.bDisplayFailReason
		IF sCurrentVarsStruct.iFailDescriptionTimer > GET_GAME_TIMER() - 5000
			SET_CURRENT_MENU_ITEM_DESCRIPTION(sCurrentVarsStruct.sFailReason)
		ELSE
			sCurrentVarsStruct.bDisplayFailReason = FALSE
		ENDIF
	ENDIF
	
	IF sCurrentVarsStruct.bDisplayFailReason = FALSE
		IF IS_BIT_SET(sFMMCmenu.iPropBitset, ciFMMC_PROP_Position_Override)
		AND IS_BIT_SET(sFMMCmenu.iPropBitset, ciFMMC_PROP_Rotation_Override)
		AND (sFMMCmenu.sActiveMenu = eFmmc_PROP_BASE OR sFMMCmenu.sActiveMenu = eFmmc_PROP_ADVANCED_OPTIONS)
			SET_CURRENT_MENU_ITEM_DESCRIPTION("MC_H_OVRRPW", 0, MENU_ICON_ALERT)
		ELIF IS_BIT_SET(sFMMCmenu.iPropBitset, ciFMMC_PROP_Position_Override)
		AND (sFMMCmenu.sActiveMenu = eFmmc_PROP_BASE OR sFMMCmenu.sActiveMenu = eFmmc_PROP_ADVANCED_OPTIONS)
			SET_CURRENT_MENU_ITEM_DESCRIPTION("MC_H_OVRPW", 0, MENU_ICON_ALERT)
		ELIF IS_BIT_SET(sFMMCmenu.iPropBitset, ciFMMC_PROP_Rotation_Override)
		AND (sFMMCmenu.sActiveMenu = eFmmc_PROP_BASE OR sFMMCmenu.sActiveMenu = eFmmc_PROP_ADVANCED_OPTIONS)
			SET_CURRENT_MENU_ITEM_DESCRIPTION("MC_H_OVRRW", 0, MENU_ICON_ALERT)
			
		ELIF CURRENTLY_USING_FMMC_OVERRIDE_POSITION(sFMMCmenu)
		AND CURRENTLY_USING_FMMC_OVERRIDE_ROTATION(sFMMCmenu)
		AND sFMMCmenu.sActiveMenu = eFmmc_DYNOPROP_BASE
			SET_CURRENT_MENU_ITEM_DESCRIPTION("MC_H_OVRRDPW", 0, MENU_ICON_ALERT)
		ELIF CURRENTLY_USING_FMMC_OVERRIDE_POSITION(sFMMCmenu)
		AND sFMMCmenu.sActiveMenu = eFmmc_DYNOPROP_BASE
			SET_CURRENT_MENU_ITEM_DESCRIPTION("MC_H_OVRDPW", 0, MENU_ICON_ALERT)
		ELIF CURRENTLY_USING_FMMC_OVERRIDE_ROTATION(sFMMCmenu)
		AND sFMMCmenu.sActiveMenu = eFmmc_DYNOPROP_BASE
			SET_CURRENT_MENU_ITEM_DESCRIPTION("MC_H_OVRDRW", 0, MENU_ICON_ALERT)
			
		ELIF sCurrentVarsStruct.bDisplaySelected
		AND sFMMCmenu.iEntityCreation != CREATION_TYPE_NONE
			PROCESS_SHOWING_HIGHLIGHTED_ENTITY(sCurrentVarsStruct)
		ELSE
			SET_CURRENT_MENU_ITEM_DESCRIPTION(GET_MENU_ITEM_DESCRIPTION(sFMMCmenu, bSignedOut), 0, DOES_THIS_DESCRIPTION_NEED_A_WARNING(GET_MENU_ITEM_DESCRIPTION(sFMMCmenu, bSignedOut), sFMMCmenu)) // Pass in "" to clear
			IF ARE_STRINGS_EQUAL(GET_MENU_ITEM_DESCRIPTION(sFMMCmenu, bSignedOut), "DMC_H_14AT")
			OR ARE_STRINGS_EQUAL(GET_MENU_ITEM_DESCRIPTION(sFMMCmenu, bSignedOut), "DMC_H_14T1")
				ADD_CURRENT_MENU_ITEM_DESCRIPTION_INT(((g_FMMC_STRUCT.iNumParticipants + 2) * 2) - g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[0])
				IF ((g_FMMC_STRUCT.iNumParticipants + 2) * 2) - g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[0] > 1
					ADD_CURRENT_MENU_ITEM_DESCRIPTION_STRING("FMMC_SPP")
				ELSE
					ADD_CURRENT_MENU_ITEM_DESCRIPTION_STRING("FMMC_SPS")
				ENDIF
			ELIF sFMMCmenu.sActiveMenu = efmmc_PROP_TEMPLATE_GRAB_PROPS
			AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 1
				ADD_CURRENT_MENU_ITEM_DESCRIPTION_STRING("FM_NXT_DM")
			ENDIF
		ENDIF
	ENDIF	
		
	INT iLines = 10
	
	SET_MAX_MENU_ROWS_TO_DISPLAY(iLines)
	
	DRAW_MENU()
	
	iTopItem = GET_TOP_MENU_ITEM()
	
	//If the help text need to be redrawn then do so.
	IF bRefresh
		sCurrentVarsStruct.bResetUpHelp = TRUE
	ENDIF
ENDPROC

PROC HANDLE_SWITCH_TO_GROUND_CAMERA()
	IF NOT IS_BIT_SET(sFMMCdata.iBitSet, biTestMissionActive)
		IF sFMMCmenu.sActiveMenu != eFmmc_PAN_CAM_BASE
		AND sFMMCmenu.sActiveMenu != eFmmc_PHOTO_CAM_BASE
		AND NOT (HAS_CURRENT_PROP_BEEN_FREE_ROTATED(sFMMCmenu) AND IS_PC_VERSION() AND IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL))
		AND NOT (HAS_NET_TIMER_STARTED(sFMMCmenu.tdCycleSwitchCooldownTimer) AND NOT HAS_NET_TIMER_EXPIRED_READ_ONLY(sFMMCmenu.tdCycleSwitchCooldownTimer, 750))
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, GET_CREATOR_SWITCH_CAMERA_BUTTON())		
				IF sCurrentVarsStruct.bitsetOkToPlace = BS_CAN_PLACE
				AND sCurrentVarsStruct.bCanSwapCams
					sCurrentVarsStruct.iMenuState = MENU_STATE_SWITCH_CAM
				ELSE
					PLAY_SOUND_FRONTEND(-1, "ERROR", GET_SOUND_SET_FROM_CREATOR_TYPE())
					PRINTSTRING("CANT SWITCH ERROR SOUND EFFECT!!!")PRINTNL()
					sCurrentVarsStruct.bDisplayFailReason = TRUE
					sCurrentVarsStruct.iFailDescriptionTimer = GET_GAME_TIMER()
					IF sCurrentVarsStruct.bCanSwapCams
						sCurrentVarsStruct.sFailReason = "FMMC_ER_026"
					ELSE
						sCurrentVarsStruct.sFailReason = "FMMC_ER_CAMSWP"
					ENDIF
				ENDIF
			ENDIF	
		ENDIF
	ENDIF
ENDPROC

PROC DEAL_WITH_CAMERA_SWITCH_SELECTION(BOOL bIncrease, INT iMaximumValue)
	
	IF NOT HAS_NET_TIMER_STARTED(sFMMCmenu.tdCycleSwitchCooldownTimer)
		START_NET_TIMER(sFMMCmenu.tdCycleSwitchCooldownTimer)
	ELIF HAS_NET_TIMER_STARTED(sFMMCmenu.tdCycleSwitchCooldownTimer)
		REINIT_NET_TIMER(sFMMCmenu.tdCycleSwitchCooldownTimer)
	ENDIF
	
	IF NOT IS_ROCKSTAR_DEV()
		IF SHOULD_CYCLE_ITEMS_BE_BLOCKED(sFMMCMenu)
			EXIT
		ENDIF
	ENDIF
	
	IF iMaximumValue > 0
		IF bSwitchingCam = FALSE
		AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()
			
			IF bIncrease
				sFMMCmenu.iSwitchCam ++
				IF sFMMCmenu.iSwitchCam < 0
					sFMMCmenu.iSwitchCam = 1
				ENDIF
			ELSE
				sFMMCmenu.iSwitchCam --
				IF sFMMCmenu.iSwitchCam < 0
					sFMMCmenu.iSwitchCam = iMaximumValue - 1
				ENDIF
			ENDIF
			IF sFMMCmenu.iSwitchCam > iMaximumValue - 1
				sFMMCmenu.iSwitchCam = 0
			ENDIF
			
			PLAY_EDIT_MENU_ITEM_SOUND()
			
			IF sFMMCmenu.sActiveMenu = eFmmc_SURVIVAL_Spawn_Points
				vSwitchVec = g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[0][sFMMCmenu.iSwitchCam].vPos + <<0.0, -0.8, 8.1>>
				fSwitchHeading = g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[0][sFMMCmenu.iSwitchCam].fHead
				IF IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
					sCurrentVarsStruct.iMenuState = MENU_STATE_SWITCH_CAM
				ENDIF
			ELIF sFMMCmenu.sActiveMenu = eFmmc_PICKUP_BASE
				vSwitchVec = g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[sFMMCmenu.iSwitchCam].vPos + <<0.0, 0.0, 8.1>>
				fSwitchHeading = g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[sFMMCmenu.iSwitchCam].vRot.z
				IF IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
					sCurrentVarsStruct.iMenuState = MENU_STATE_SWITCH_CAM
				ENDIF
			ELIF sFMMCmenu.sActiveMenu = eFmmc_PROP_BASE
				vSwitchVec = g_FMMC_STRUCT_ENTITIES.sPlacedProp[sFMMCmenu.iSwitchCam].vPos
				fSwitchHeading = g_FMMC_STRUCT_ENTITIES.sPlacedProp[sFMMCmenu.iSwitchCam].fHead
				IF IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
					sCurrentVarsStruct.iMenuState = MENU_STATE_SWITCH_CAM
				ENDIF
			ELIF sFMMCmenu.sActiveMenu = eFmmc_DYNOPROP_BASE
				vSwitchVec = g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[sFMMCmenu.iSwitchCam].vPos + <<0.0, 0.0, 8.1>>
				fSwitchHeading = g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[sFMMCmenu.iSwitchCam].fHead
				IF IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
					sCurrentVarsStruct.iMenuState = MENU_STATE_SWITCH_CAM
				ENDIF
			ELIF sFMMCmenu.sActiveMenu = eFmmc_VEHICLES_BASE
			OR sFMMCmenu.sActiveMenu = eFmmc_SURVIVAL_Vehicle_Spawn
				vSwitchVec = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sFMMCmenu.iSwitchCam].vPos
				fSwitchHeading = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[sFMMCmenu.iSwitchCam].fHead
				IF IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
					sCurrentVarsStruct.iMenuState = MENU_STATE_SWITCH_CAM
				ENDIF
			ELIF sFMMCmenu.sActiveMenu = efMMC_WORLD_PROPS
				vSwitchVec = g_FMMC_STRUCT_ENTITIES.sWorldProps[sFMMCmenu.iSwitchCam].vPos
				fSwitchHeading = 0
				IF IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
					sCurrentVarsStruct.iMenuState = MENU_STATE_SWITCH_CAM
				ENDIF
			ELIF sFMMCmenu.sActiveMenu = eFmmc_DOORS_BASE
				vSwitchVec = g_FMMC_STRUCT_ENTITIES.sSelectedDoor[sFMMCmenu.iSwitchCam].vPos
				fSwitchHeading = 0
				IF IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
					sCurrentVarsStruct.iMenuState = MENU_STATE_SWITCH_CAM
				ENDIF
			ELIF sFMMCmenu.sActiveMenu = eFmmc_SURVIVAL_ENEMY_AREA
				vSwitchVec = g_FMMC_STRUCT_ENTITIES.sPlacedPed[sFMMCmenu.iSwitchCam].vPos
				fSwitchHeading = 0
				IF IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
					sCurrentVarsStruct.iMenuState = MENU_STATE_SWITCH_CAM
				ENDIF
			ENDIF
			
			bSwitchingCam = TRUE 
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL IS_THIS_OPTION_SELECTABLE()
	SWITCH sFMMCmenu.sActiveMenu
		CASE eFmmc_TOP_MENU
			IF NOT IS_BIT_SET(sFMMCmenu.iTopMenuActive, GET_CREATOR_MENU_SELECTION(sFMMCmenu))
				RETURN FALSE
			ENDIF
		BREAK
		CASE eFmmc_MAIN_MENU_BASE
			IF IS_VECTOR_ZERO(g_FMMC_STRUCT.vStartPos)
			AND GET_CREATOR_MENU_SELECTION(sFMMCMenu) != 0
				RETURN FALSE
			ELSE
				IF (sFMMCMenu.sMenuGoto[sFMMCMenu.iCurrentMenuLength] = eFmmc_SURVIVAL_ENEMY_AREA
				OR sFMMCMenu.sMenuGoto[sFMMCMenu.iCurrentMenuLength] = eFmmc_SURVIVAL_Spawn_Points)
				AND NOT HAS_SURVIVAL_BOUNDS_BEEN_PLACED()
					RETURN FALSE
				ENDIF
			ENDIF
		BREAK
		CASE eFmmc_SURVIVAL_Squad_Config
			IF g_FMMC_STRUCT.sSurvivalWaveData.iNumberOfSquads[sFMMCmenu.iSurvivalWaveSelection][sFMMCmenu.iSurvivalDiffSelection] = 0
			AND GET_CREATOR_MENU_SELECTION(sFMMCMenu) != OPTION_CONFIG_SQUADS
				RETURN FALSE
			ENDIF
		BREAK
		CASE eFmmc_SURVIVAL_Wave_Config
			IF (GET_CREATOR_MENU_SELECTION(sFMMCMenu) = OPTION_WAVE_BASE_LAND_MOD
			AND (IS_BIT_SET(g_FMMC_STRUCT.sSurvivalWaveData.iLandModChangeBitset, sFMMCmenu.iSurvivalWaveSelection) OR g_FMMC_STRUCT.sSurvivalWaveData.iNumVehicles[sFMMCmenu.iSurvivalWaveSelection] = 0))
			OR (GET_CREATOR_MENU_SELECTION(sFMMCMenu) = OPTION_WAVE_BASE_AIR_MOD
			AND (IS_BIT_SET(g_FMMC_STRUCT.sSurvivalWaveData.iAirModChangeBitset, sFMMCmenu.iSurvivalWaveSelection) OR g_FMMC_STRUCT.sSurvivalWaveData.iNumberOfHelis[sFMMCmenu.iSurvivalWaveSelection] = 0))
				RETURN FALSE
			ENDIF
		BREAK
		CASE eFmmc_DELETE_ENTITIES
			RETURN g_sMenuData.bIsSelectable[GET_CREATOR_MENU_SELECTION(sFMMCmenu)]
		BREAK
		
		CASE eFmmc_SURVIVAL_WARNING_AREA
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_SURV_BOUNDS_CLEAR
				RETURN NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sSurvivalWaveData.vBounds[0])
			ENDIF
		BREAK
		
		CASE eFmmc_SURVIVAL_Wave_Base
			IF g_bFMMC_TutorialSelectedFromMpSkyMenu
			AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) != 0
				RETURN FALSE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_THIS_OPTION_TOGGLEABLE()
	RETURN g_sMenuData.bItemToggleable[1]
ENDFUNC

PROC EDIT_SURVIVAL_VEH_CONFIG_SELECTION_MENU(INT iChange)
	IF GET_CREATOR_MENU_SELECTION(sFMMCMenu) 	= OPTION_CONFIG_VEH_CATEGORY
		EDIT_CREATOR_VEHICLE_LIBRARY_MENU_ITEM(sFMMCmenu, sFMMCmenu.iVehicleLibrary, sFMMCMenu.iVehicleType, iChange)
		g_FMMC_STRUCT.sSurvivalWaveData.mnVeh[sFMMCmenu.iSurvivalWaveSelection][sFMMCmenu.iSurvivalVehicleSelection] = GET_VEHICLE_MODEL_FROM_CREATOR_SELECTION(sFMMCMenu.iVehicleLibrary, sFMMCMenu.iVehicleType)
		sFMMCendStage.bRetestNeededWithChange = TRUE
		SET_BIT(g_FMMC_STRUCT.sSurvivalWaveData.iLandModChangeBitset, sFMMCmenu.iSurvivalWaveSelection)
	ELIF GET_CREATOR_MENU_SELECTION(sFMMCMenu) 	= OPTION_CONFIG_VEH_MODEL
		EDIT_CREATOR_VEHICLE_TYPE_MENU_ITEM(sFMMCmenu, sFMMCmenu.iVehicleLibrary, sFMMCMenu.iVehicleType, iChange)
		g_FMMC_STRUCT.sSurvivalWaveData.mnVeh[sFMMCmenu.iSurvivalWaveSelection][sFMMCmenu.iSurvivalVehicleSelection] = GET_VEHICLE_MODEL_FROM_CREATOR_SELECTION(sFMMCMenu.iVehicleLibrary, sFMMCMenu.iVehicleType)
		SET_BIT(g_FMMC_STRUCT.sSurvivalWaveData.iLandModChangeBitset, sFMMCmenu.iSurvivalWaveSelection)
		sFMMCendStage.bRetestNeededWithChange = TRUE
	ENDIF
	
ENDPROC

PROC HANDLE_SURVIVAL_AIR_CONFIG_SELECTION(INT iChange)
	IF GET_CREATOR_MENU_SELECTION(sFMMCMenu) 	= OPTION_CONFIG_VEH_CATEGORY
		EDIT_CREATOR_VEHICLE_LIBRARY_MENU_ITEM(sFMMCmenu, sFMMCmenu.iVehicleLibrary, sFMMCMenu.iVehicleType, iChange)
		g_FMMC_STRUCT.sSurvivalWaveData.mnAir[sFMMCmenu.iSurvivalWaveSelection][sFMMCmenu.iSurvivalAirVehSelection] = GET_VEHICLE_MODEL_FROM_CREATOR_SELECTION(sFMMCMenu.iVehicleLibrary, sFMMCMenu.iVehicleType)
		SET_BIT(g_FMMC_STRUCT.sSurvivalWaveData.iAirModChangeBitset, sFMMCmenu.iSurvivalWaveSelection)
		sFMMCendStage.bRetestNeededWithChange = TRUE
	ELIF GET_CREATOR_MENU_SELECTION(sFMMCMenu) 	= OPTION_CONFIG_VEH_MODEL
		EDIT_CREATOR_VEHICLE_TYPE_MENU_ITEM(sFMMCmenu, sFMMCmenu.iVehicleLibrary, sFMMCMenu.iVehicleType, iChange)
		g_FMMC_STRUCT.sSurvivalWaveData.mnAir[sFMMCmenu.iSurvivalWaveSelection][sFMMCmenu.iSurvivalAirVehSelection] = GET_VEHICLE_MODEL_FROM_CREATOR_SELECTION(sFMMCMenu.iVehicleLibrary, sFMMCMenu.iVehicleType)
		SET_BIT(g_FMMC_STRUCT.sSurvivalWaveData.iAirModChangeBitset, sFMMCmenu.iSurvivalWaveSelection)
		sFMMCendStage.bRetestNeededWithChange = TRUE
	ENDIF
ENDPROC

PROC HANDLE_SURVIVAL_HEAVY_MODEL_SELECTION_MENU(INT iChange)
	INT iWaveToUse = sFMMCmenu.iSurvivalWaveSelection
	IF iWaveToUse = -1
		iWaveToUse = 1
	ENDIF
	BOOL bCanChange = TRUE
	IF IS_MENU_A_SURVIVAL_OVERRIDE_MENU(sFMMCmenu)
		IF NOT IS_BIT_SET(iLocalBitSet, biHaveShownConfirmScreenInMenu)
			TRIGGER_CONFIRMATION_SCREEN(sCurrentVarsStruct, "SC_AYS", "SC_CONF_CAW")
		ENDIF
		IF NOT IS_BIT_SET(iLocalBitSet, biHaveShownConfirmScreenInMenu)
			bCanChange = FALSE
		ENDIF
	ENDIF
	
	IF bCanChange
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_LIBRARY
			INT iMaxPedSelection = PED_LIBRARY_F_SPECIAL
			IF IS_ROCKSTAR_DEV()
				iMaxPedSelection = PED_LIBRARY_MAX_PUBLIC
			ENDIF
			EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, sFMMCmenu.iPedLibrary, iChange, iMaxPedSelection)
			IF sFMMCmenu.iPedType >= sFMMCmenu.iPedModelTailIndex[sFMMCmenu.iPedLibrary]
				sFMMCmenu.iPedType = 0
			ENDIF
			SKIP_OVER_INVALID_PEDS(1, sFMMCmenu, sFMMCendStage)
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_TYPE
			EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, sFMMCmenu.iPedType, iChange, sFMMCmenu.iPedModelTailIndex[sFMMCmenu.iPedLibrary])
			SKIP_OVER_INVALID_PEDS(iChange, sFMMCmenu, sFMMCendStage)
		ENDIF
		
		IF g_FMMC_STRUCT.sSurvivalWaveData.mnHeavyModel[iWaveToUse][sFMMCmenu.iSurvivalDiffSelection] != GET_PED_MODEL_FROM_CREATOR_SELECTION(sFMMCMenu)
			g_FMMC_STRUCT.sSurvivalWaveData.mnHeavyModel[iWaveToUse][sFMMCmenu.iSurvivalDiffSelection] = GET_PED_MODEL_FROM_CREATOR_SELECTION(sFMMCMenu)
			IF NOT IS_MENU_A_SURVIVAL_OVERRIDE_MENU(sFMMCmenu)
			AND NOT IS_MENU_A_SURVIVAL_WAVE_MENU(sFMMCmenu)
				SET_BIT(g_FMMC_STRUCT.sSurvivalWaveData.iHeavyModChangeBitset, sFMMCmenu.iSurvivalWaveSelection)
			ENDIF
		ENDIF
		INT i
		IF IS_MENU_A_SURVIVAL_OVERRIDE_MENU(sFMMCmenu)
			INT i2
			FOR i = 0 TO MAX_SURVIVAL_WAVES-1
				FOR i2 = 0 TO MAX_SURV_WAVE_DIFF-1
					IF g_FMMC_STRUCT.sSurvivalWaveData.mnHeavyModel[i][i2] != GET_PED_MODEL_FROM_CREATOR_SELECTION(sFMMCMenu)
						g_FMMC_STRUCT.sSurvivalWaveData.mnHeavyModel[i][i2] = GET_PED_MODEL_FROM_CREATOR_SELECTION(sFMMCMenu)
					ENDIF
				ENDFOR
			ENDFOR
		ELSE
			IF IS_MENU_A_SURVIVAL_WAVE_MENU(sFMMCmenu)
				FOR i = 0 TO MAX_SURV_WAVE_DIFF-1
					IF g_FMMC_STRUCT.sSurvivalWaveData.mnHeavyModel[iWaveToUse][i] != g_FMMC_STRUCT.sSurvivalWaveData.mnHeavyModel[iWaveToUse][0]
						g_FMMC_STRUCT.sSurvivalWaveData.mnHeavyModel[iWaveToUse][i] = g_FMMC_STRUCT.sSurvivalWaveData.mnHeavyModel[iWaveToUse][0]
					ENDIF
				ENDFOR
			ENDIF
		ENDIF
	ENDIF
 
ENDPROC

PROC EDIT_SURVIVAL_SQUAD_MODEL_SELECTION_MENU(INT iChange)
	IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_LIBRARY
		INT iMaxPedSelection = PED_LIBRARY_F_SPECIAL
		IF IS_ROCKSTAR_DEV()
			iMaxPedSelection = PED_LIBRARY_MAX_PUBLIC
		ENDIF
		EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, sFMMCmenu.iPedLibrary, iChange, iMaxPedSelection)
		IF sFMMCmenu.iPedType >= sFMMCmenu.iPedModelTailIndex[sFMMCmenu.iPedLibrary]
			sFMMCmenu.iPedType = 0
		ENDIF
		SET_BIT(g_FMMC_STRUCT.sSurvivalWaveData.iPedModelChangeBitset, sFMMCmenu.iSurvivalWaveSelection)
		SKIP_OVER_INVALID_PEDS(iChange, sFMMCmenu, sFMMCendStage)
	ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_TYPE
		EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, sFMMCmenu.iPedType, iChange, sFMMCmenu.iPedModelTailIndex[sFMMCmenu.iPedLibrary])
		SET_BIT(g_FMMC_STRUCT.sSurvivalWaveData.iPedModelChangeBitset, sFMMCmenu.iSurvivalWaveSelection)
		SKIP_OVER_INVALID_PEDS(iChange, sFMMCmenu, sFMMCendStage)
	ENDIF
	
	IF g_FMMC_STRUCT.sSurvivalWaveData.mnPedModel[sFMMCmenu.iSurvivalWaveSelection][sFMMCmenu.iSurvivalDiffSelection] != GET_PED_MODEL_FROM_CREATOR_SELECTION(sFMMCMenu)
		g_FMMC_STRUCT.sSurvivalWaveData.mnPedModel[sFMMCmenu.iSurvivalWaveSelection][sFMMCmenu.iSurvivalDiffSelection] = GET_PED_MODEL_FROM_CREATOR_SELECTION(sFMMCMenu)
	ENDIF
 
ENDPROC

PROC EDIT_SURVIVAL_VEH_WAVE_MODEL_MENU(INT iChange)
	INT iVeh
	INT iWaveToUse = sFMMCmenu.iSurvivalWaveSelection
	IF iWaveToUse = -1
		iWaveToUse = 1
	ENDIF
	BOOL bWaveModel = TRUE
	BOOL bCanChange = TRUE
	IF IS_MENU_A_SURVIVAL_OVERRIDE_MENU(sFMMCmenu)
		bWaveModel = FALSE
	ENDIF
	
	IF IS_MENU_A_SURVIVAL_OVERRIDE_MENU(sFMMCmenu)
		IF NOT IS_BIT_SET(iLocalBitSet, biHaveShownConfirmScreenInMenu)
			TRIGGER_CONFIRMATION_SCREEN(sCurrentVarsStruct, "SC_AYS", "SC_CONF_CAW")
		ENDIF
		IF NOT IS_BIT_SET(iLocalBitSet, biHaveShownConfirmScreenInMenu)
			bCanChange = FALSE
		ENDIF
	ENDIF
	
	IF bCanChange
		IF GET_CREATOR_MENU_SELECTION(sFMMCMenu) 	= OPTION_CONFIG_VEH_CATEGORY
			EDIT_CREATOR_VEHICLE_LIBRARY_MENU_ITEM(sFMMCmenu, sFMMCmenu.iVehicleLibrary, sFMMCMenu.iVehicleType, iChange)
			FOR iVeh = 0 TO MAX_SURV_LAND_VEH-1
				g_FMMC_STRUCT.sSurvivalWaveData.mnVeh[iWaveToUse][iVeh] = GET_VEHICLE_MODEL_FROM_CREATOR_SELECTION(sFMMCMenu.iVehicleLibrary, sFMMCMenu.iVehicleType)
			ENDFOR
			sFMMCendStage.bRetestNeededWithChange = TRUE
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCMenu) 	= OPTION_CONFIG_VEH_MODEL
			EDIT_CREATOR_VEHICLE_TYPE_MENU_ITEM(sFMMCmenu, sFMMCmenu.iVehicleLibrary, sFMMCMenu.iVehicleType, iChange)
			FOR iVeh = 0 TO MAX_SURV_LAND_VEH-1
				g_FMMC_STRUCT.sSurvivalWaveData.mnVeh[iWaveToUse][iVeh] = GET_VEHICLE_MODEL_FROM_CREATOR_SELECTION(sFMMCMenu.iVehicleLibrary, sFMMCMenu.iVehicleType)
			ENDFOR
			sFMMCendStage.bRetestNeededWithChange = TRUE
		ENDIF
		
		IF !bWaveModel
			INT i, i2
			FOR i = 0 TO MAX_SURVIVAL_WAVES-1
				FOR i2 = 0 TO MAX_SURV_LAND_VEH-1
					IF g_FMMC_STRUCT.sSurvivalWaveData.mnVeh[i][i2] != GET_VEHICLE_MODEL_FROM_CREATOR_SELECTION(sFMMCMenu.iVehicleLibrary, sFMMCMenu.iVehicleType)
						g_FMMC_STRUCT.sSurvivalWaveData.mnVeh[i][i2] = GET_VEHICLE_MODEL_FROM_CREATOR_SELECTION(sFMMCMenu.iVehicleLibrary, sFMMCMenu.iVehicleType)
						CLEAR_BIT(g_FMMC_STRUCT.sSurvivalWaveData.iTestedWavesBitSet, i)
						CLEAR_BIT(g_FMMC_STRUCT.sSurvivalWaveData.iLandModChangeBitset, i)
						PRINTLN("EDIT_SURVIVAL_VEH_WAVE_MODEL_MENU - Updating Wave ", i, " vehicle ", i2)
					ENDIF
				ENDFOR
			ENDFOR
		ENDIF
	ENDIF
ENDPROC

PROC EDIT_SURVIVAL_AIR_WAVE_MODEL_MENU(INT iChange)
	INT iWaveToUse = sFMMCmenu.iSurvivalWaveSelection
	IF iWaveToUse = -1
		iWaveToUse = 1
	ENDIF
	BOOL bWaveModel = TRUE
	IF IS_MENU_A_SURVIVAL_OVERRIDE_MENU(sFMMCmenu)
		bWaveModel = FALSE
	ENDIF
	BOOL bCanChange = TRUE
	IF IS_MENU_A_SURVIVAL_OVERRIDE_MENU(sFMMCmenu)
		IF NOT IS_BIT_SET(iLocalBitSet, biHaveShownConfirmScreenInMenu)
			TRIGGER_CONFIRMATION_SCREEN(sCurrentVarsStruct, "SC_AYS", "SC_CONF_CAW")
		ENDIF
		IF NOT IS_BIT_SET(iLocalBitSet, biHaveShownConfirmScreenInMenu)
			bCanChange = FALSE
		ENDIF
	ENDIF
	
	IF bCanChange
		IF GET_CREATOR_MENU_SELECTION(sFMMCMenu) 	= OPTION_CONFIG_VEH_CATEGORY
			EDIT_CREATOR_VEHICLE_LIBRARY_MENU_ITEM(sFMMCmenu, sFMMCmenu.iVehicleLibrary, sFMMCMenu.iVehicleType, iChange)
			g_FMMC_STRUCT.sSurvivalWaveData.mnAir[iWaveToUse][0] = GET_VEHICLE_MODEL_FROM_CREATOR_SELECTION(sFMMCMenu.iVehicleLibrary, sFMMCMenu.iVehicleType)
			sFMMCendStage.bRetestNeededWithChange = TRUE
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCMenu) 	= OPTION_CONFIG_VEH_MODEL
			EDIT_CREATOR_VEHICLE_TYPE_MENU_ITEM(sFMMCmenu, sFMMCmenu.iVehicleLibrary, sFMMCMenu.iVehicleType, iChange)
			g_FMMC_STRUCT.sSurvivalWaveData.mnAir[iWaveToUse][0] = GET_VEHICLE_MODEL_FROM_CREATOR_SELECTION(sFMMCMenu.iVehicleLibrary, sFMMCMenu.iVehicleType)
			sFMMCendStage.bRetestNeededWithChange = TRUE
		ENDIF
		
		IF !bWaveModel
			INT i, i2
			FOR i = 0 TO MAX_SURVIVAL_WAVES-1
				FOR i2 = 0 TO MAX_SURV_HELI-1
					IF g_FMMC_STRUCT.sSurvivalWaveData.mnAir[i][i2] != GET_VEHICLE_MODEL_FROM_CREATOR_SELECTION(sFMMCMenu.iVehicleLibrary, sFMMCMenu.iVehicleType)
						g_FMMC_STRUCT.sSurvivalWaveData.mnAir[i][i2] = GET_VEHICLE_MODEL_FROM_CREATOR_SELECTION(sFMMCMenu.iVehicleLibrary, sFMMCMenu.iVehicleType)
						CLEAR_BIT(g_FMMC_STRUCT.sSurvivalWaveData.iTestedWavesBitSet, i)
						CLEAR_BIT(g_FMMC_STRUCT.sSurvivalWaveData.iAirModChangeBitset, i)
						PRINTLN("EDIT_SURVIVAL_AIR_WAVE_MODEL_MENU - Updating Wave ", i, " air vehicle ", i2)
					ENDIF
				ENDFOR
			ENDFOR
		ENDIF
	ENDIF
ENDPROC

PROC EDIT_SURVIVAL_PED_WAVE_MODEL_MENU(INT iChange)
	INT iWaveToUse = sFMMCmenu.iSurvivalWaveSelection
	IF iWaveToUse = -1
		iWaveToUse = 1
	ENDIF
	BOOL bWaveModel = TRUE
	
	IF IS_MENU_A_SURVIVAL_OVERRIDE_MENU(sFMMCmenu)
		bWaveModel = FALSE
	ENDIF
	BOOL bCanChange = TRUE
	
	IF IS_MENU_A_SURVIVAL_OVERRIDE_MENU(sFMMCmenu)
		IF NOT IS_BIT_SET(iLocalBitSet, biHaveShownConfirmScreenInMenu)
			TRIGGER_CONFIRMATION_SCREEN(sCurrentVarsStruct, "SC_AYS", "SC_CONF_CAW")
		ENDIF
		IF NOT IS_BIT_SET(iLocalBitSet, biHaveShownConfirmScreenInMenu)
			bCanChange = FALSE
		ENDIF
	ENDIF
	
	IF bCanChange
		IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_LIBRARY
			INT iMaxPedSelection = PED_LIBRARY_F_SPECIAL
			IF IS_ROCKSTAR_DEV()
				iMaxPedSelection = PED_LIBRARY_MAX_PUBLIC
			ENDIF
			EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, sFMMCmenu.iPedLibrary, iChange, iMaxPedSelection)
			IF sFMMCmenu.iPedType >= sFMMCmenu.iPedModelTailIndex[sFMMCmenu.iPedLibrary]
				sFMMCmenu.iPedType = 0
			ENDIF
			SKIP_OVER_INVALID_PEDS(1, sFMMCmenu, sFMMCendStage)
		ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ENEMY_TYPE
			EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, sFMMCmenu.iPedType, iChange, sFMMCmenu.iPedModelTailIndex[sFMMCmenu.iPedLibrary])
			SKIP_OVER_INVALID_PEDS(iChange, sFMMCmenu, sFMMCendStage)
		ENDIF
		
		INT i, i2
		IF !bWaveModel
			IF g_FMMC_STRUCT.sSurvivalWaveData.mnPedModel[1][0] != GET_PED_MODEL_FROM_CREATOR_SELECTION(sFMMCMenu)
				FOR i = 0 TO MAX_SURVIVAL_WAVES-1
					FOR i2 = 0 TO MAX_SURV_WAVE_DIFF-1
						g_FMMC_STRUCT.sSurvivalWaveData.mnPedModel[i][i2] = GET_PED_MODEL_FROM_CREATOR_SELECTION(sFMMCMenu)
						CLEAR_BIT(g_FMMC_STRUCT.sSurvivalWaveData.iPedModelChangeBitset, i)
						PRINTLN("EDIT_SURVIVAL_PED_WAVE_MODEL_MENU - Updating Wave ", i, " Stage ", i2)
					ENDFOR
				ENDFOR
			ENDIF
		ELSE
			IF g_FMMC_STRUCT.sSurvivalWaveData.mnPedModel[iWaveToUse][0] != GET_PED_MODEL_FROM_CREATOR_SELECTION(sFMMCMenu)
				FOR i = 0 TO MAX_SURV_WAVE_DIFF-1
					g_FMMC_STRUCT.sSurvivalWaveData.mnPedModel[iWaveToUse][i] = GET_PED_MODEL_FROM_CREATOR_SELECTION(sFMMCMenu)
				ENDFOR
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC HANDLE_PLAY_AREA_BOUNDS_PLACEMENT()
	BOOL bCylinderBounds = g_FMMC_STRUCT.sSurvivalWaveData.iBoundsType = SURV_BOUNDS_CYLINDER
	
	IF (CAN_PLACE_BOUNDS_ON_THIS_OPTION(sFMMCmenu) AND NOT HAS_SURVIVAL_BOUNDS_BEEN_PLACED())
	OR sFMMCMenu.iBoundsEditPointIndex > -1
		BOOL bWater
		BOOL bInRange = TRUE
		
		SET_AREA_GOOD_FOR_PLACEMENT(sCurrentVarsStruct, TRUE)
		
		IF sCurrentVarsStruct.bCoronaOnWater
		OR IS_CORONA_UNDERWATER()
			SET_AREA_GOOD_FOR_PLACEMENT(sCurrentVarsStruct, FALSE)
			bWater = TRUE
		ENDIF
		
		IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sSurvivalWaveData.vBounds[0])
		AND IS_VECTOR_ZERO(g_FMMC_STRUCT.sSurvivalWaveData.vBounds[1])
		AND !bCylinderBounds
			FLOAT fDistance = VDIST2(g_FMMC_STRUCT.sSurvivalWaveData.vBounds[0], sCurrentVarsStruct.vCoronaPos)
			IF fDistance > POW(MAX_SURVIVAL_BOUNDS_SIZE, 2)
				bInRange = FALSE
				SET_AREA_GOOD_FOR_PLACEMENT(sCurrentVarsStruct, FALSE)
				SET_THE_PLACEMENT_FAIL_AS_THIS_REASON(sCurrentVarsStruct, bNotSurvivalBoundsTooBig)
			ELIF fDistance < POW(MIN_SURVIVAL_BOUNDS_SIZE, 2)
			AND NOT HAS_SURVIVAL_BOUNDS_BEEN_PLACED()
				bInRange = FALSE
				SET_AREA_GOOD_FOR_PLACEMENT(sCurrentVarsStruct, FALSE)
				SET_THE_PLACEMENT_FAIL_AS_THIS_REASON(sCurrentVarsStruct, bBoundsTooSmall)
			ENDIF
		ENDIF
		
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
			
			IF bWater
				SET_THE_PLACEMENT_FAIL_AS_THIS_REASON(sCurrentVarsStruct, bWaterFail)
				bFailHit = TRUE
				EXIT
			ENDIF
			
			IF NOT bInRange
				bFailHit = TRUE
				EXIT
			ENDIF
			
			IF sFMMCMenu.iBoundsEditPointIndex = -1
				IF IS_VECTOR_ZERO(g_FMMC_STRUCT.sSurvivalWaveData.vBounds[0])
					g_FMMC_STRUCT.sSurvivalWaveData.vBounds[0] = sCurrentVarsStruct.vCoronaPos
					
					IF bCylinderBounds
						SET_CREATOR_MENU_SELECTION(sFMMCmenu, OPTION_SURV_BOUNDS_EDIT_FRONT)
					ENDIF
				ELIF IS_VECTOR_ZERO(g_FMMC_STRUCT.sSurvivalWaveData.vBounds[1])
				AND (IS_ROCKSTAR_DEV() OR !bCylinderBounds)
					BOOL bCanPlace = TRUE
					IF IS_ROCKSTAR_DEV()
					AND bCylinderBounds
					AND sFMMCmenu.fCreationHeightIncrease = 0.0
						bCanPlace = FALSE
					ENDIF
					
					IF bCanPlace
						FLOAT fDistance = VDIST2(g_FMMC_STRUCT.sSurvivalWaveData.vBounds[0], sCurrentVarsStruct.vCoronaPos)
						IF fDistance <= POW(MAX_SURVIVAL_BOUNDS_SIZE, 2)
						AND fDistance >= POW(MIN_SURVIVAL_BOUNDS_SIZE, 2)
							
							IF sFMMCmenu.fCreationHeightIncrease = 0.0
								CLEAR_BIT(g_FMMC_STRUCT.sSurvivalWaveData.iBitset, ciSurvivalBS_BoundsHeightSet)
							ENDIF
							
							g_FMMC_STRUCT.sSurvivalWaveData.vBounds[1] = sCurrentVarsStruct.vCoronaPos
							SET_CREATOR_MENU_SELECTION(sFMMCmenu, OPTION_SURV_BOUNDS_EDIT_FRONT)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			REFRESH_MENU_ASAP(sFMMCMenu)
		ENDIF
		
		IF (NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sSurvivalWaveData.vBounds[0])
		AND IS_VECTOR_ZERO(g_FMMC_STRUCT.sSurvivalWaveData.vBounds[1])
		AND !bCylinderBounds)
		OR sFMMCMenu.iBoundsEditPointIndex > -1
			INT iA, iR, iG, iB
			VECTOR vSpherePos = g_FMMC_STRUCT.sSurvivalWaveData.vBounds[0]
			
			IF sFMMCMenu.iBoundsEditPointIndex = 0
				vSpherePos = g_FMMC_STRUCT.sSurvivalWaveData.vBounds[1]
			ELIF sFMMCMenu.iBoundsEditPointIndex = 1
				vSpherePos = g_FMMC_STRUCT.sSurvivalWaveData.vBounds[0]
			ENDIF
			
			GET_RGB_FROM_INT(GET_INT_FROM_HUD_COLOUR(HUD_COLOUR_YELLOW), iR, iG, iB, iA)
			iA = 50
			DRAW_MARKER_SPHERE(vSpherePos, 200.0, iR, iG, iB, 0.25)
		ENDIF
	ELSE
		SET_AREA_GOOD_FOR_PLACEMENT(sCurrentVarsStruct, FALSE)
	ENDIF
	
	IF HAS_SURVIVAL_BOUNDS_BEEN_PLACED()
	AND sFMMCMenu.iBoundsEditPointIndex = -1
	AND g_FMMC_STRUCT.sSurvivalWaveData.iBoundsType = SURV_BOUNDS_NON_AXIS_AREA
	AND NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sSurvivalWaveData.vBounds[1])
		VECTOR vLow = g_FMMC_STRUCT.sSurvivalWaveData.vBounds[0]
		
		IF g_FMMC_STRUCT.sSurvivalWaveData.vBounds[1].z < vLow.z
			VECTOR vNewLow = g_FMMC_STRUCT.sSurvivalWaveData.vBounds[1]
			g_FMMC_STRUCT.sSurvivalWaveData.vBounds[0] = vNewLow
			g_FMMC_STRUCT.sSurvivalWaveData.vBounds[1] = vLow
			PRINTLN("HANDLE_PLAY_AREA_BOUNDS_PLACEMENT - Swapped low and high around!")
		ENDIF
	ENDIF
	
	IF HAS_SURVIVAL_BOUNDS_BEEN_PLACED()
	AND sFMMCMenu.iBoundsEditPointIndex = -1
		IF sFMMCmenu.sActiveMenu = eFmmc_SURVIVAL_WARNING_AREA
		AND ((GET_CREATOR_MENU_SELECTION(sFMMCMenu) = OPTION_SURV_BOUNDS_HEIGHT AND g_FMMC_STRUCT.sSurvivalWaveData.fBoundsHeight != 0.0)
		OR (GET_CREATOR_MENU_SELECTION(sFMMCMenu) = OPTION_SURV_BOUNDS_BASE_OFFSET AND g_FMMC_STRUCT.sSurvivalWaveData.fBoundsBaseOffset != 0.0))
			VECTOR vZDiff = GET_SURVIVAL_BOUNDS_POINT_WITH_HEIGHT() - GET_SURVIVAL_BOUNDS_POINT_WITH_LOW_OFFSET()
			IF vZDiff.z < 5
				IF GET_CREATOR_MENU_SELECTION(sFMMCMenu) = OPTION_SURV_BOUNDS_HEIGHT
					g_FMMC_STRUCT.sSurvivalWaveData.fBoundsBaseOffset += 5.0
				ELIF GET_CREATOR_MENU_SELECTION(sFMMCMenu) = OPTION_SURV_BOUNDS_BASE_OFFSET
					g_FMMC_STRUCT.sSurvivalWaveData.fBoundsHeight += 5.0
				ENDIF
				REFRESH_MENU(sFMMCMenu)
			ENDIF
		ENDIF
	ENDIF
	
	IF sFMMCMenu.iBoundsEditPointIndex > -1
	AND !bCylinderBounds
		IF NOT IS_EDITED_SURVIVAL_BOUNDS_POINT_IN_RANGE(sFMMCmenu, sCurrentVarsStruct)
			SET_AREA_GOOD_FOR_PLACEMENT(sCurrentVarsStruct, FALSE)
		ENDIF
	ENDIF
	
ENDPROC

PROC HANDLE_ENEMY_SPAWN_AREA_PLACEMENT()
	sFMMCmenu.iPedLibrary = GET_PED_LIBRARY_FROM_MODEL(g_FMMC_STRUCT.sSurvivalWaveData.mnPedModel[g_iStartingWave][0], sFMMCMenu)
	sFMMCmenu.iPedType = GET_PED_TYPE_FROM_MODEL(g_FMMC_STRUCT.sSurvivalWaveData.mnPedModel[g_iStartingWave][0], sFMMCMenu, sFMMCmenu.iPedLibrary)
	
	VECTOR vAreaMid = SURVIVAL_GET_PLACED_BOUNDS_MIDDLE()
	
	INT iA, iR, iG, iB
	GET_RGB_FROM_INT(GET_INT_FROM_HUD_COLOUR(HUD_COLOUR_RED), iR, iG, iB, iA)
	iA = 50
	DRAW_MARKER_SPHERE(vAreaMid, GET_SURVIVAL_ENEMY_SPAWN_PLACEMENT_RANGE(), iR, iG, iB, 0.25)
	
ENDPROC

PROC HANDLE_VEHICLE_SPAWN_PLACEMENT_AREA()
	VECTOR vAreaMid = SURVIVAL_GET_PLACED_BOUNDS_MIDDLE()
	
	INT iA, iR, iG, iB
	GET_RGB_FROM_INT(GET_INT_FROM_HUD_COLOUR(HUD_COLOUR_RED), iR, iG, iB, iA)
	iA = 50
	DRAW_MARKER_SPHERE(vAreaMid, MAX_SURVIVAL_BOUNDS_SIZE, iR, iG, iB, 0.25)
ENDPROC

PROC HANDLE_DISPLAYING_PED_MODEL()
	INT iWaveToUse
	INT iStageToUse
	iWaveToUse = sFMMCmenu.iSurvivalWaveSelection
	IF iWaveToUse = -1
		iWaveToUse = 0
	ENDIF
	iStageToUse = sFMMCmenu.iSurvivalDiffSelection
	IF iStageToUse = -1
		iStageToUse = 0
	ENDIF
	
	MODEL_NAMES mnToUse = g_FMMC_STRUCT.sSurvivalWaveData.mnPedModel[iWaveToUse][iStageToUse]
	IF sFMMCmenu.sActiveMenu = eFmmc_SURVIVAL_Heavy_Model
		mnToUse = g_FMMC_STRUCT.sSurvivalWaveData.mnHeavyModel[iWaveToUse][iStageToUse]
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(pedSelection)
	AND mnToUse != DUMMY_MODEL_FOR_SCRIPT
		REQUEST_MODEL(mnToUse)
		IF HAS_MODEL_LOADED(mnToUse)
			pedSelection = CREATE_PED(PEDTYPE_MISSION, mnToUse, sCurrentVarsStruct.vCoronaPos, sCurrentVarsStruct.fCreationHeading,FALSE,FALSE)
			INT iVariation = 0
			IF mnToUse = INT_TO_ENUM(MODEL_NAMES, HASH("U_M_Y_Juggernaut_01"))
				iVariation = 1
				FMMC_SET_PED_VARIATION(pedSelection, mnToUse, iVariation)
			ELIF mnToUse = HC_GUNMAN
				iVariation = 4
				FMMC_SET_PED_VARIATION(pedSelection, mnToUse, iVariation)
			ENDIF
			IF IS_ENTITY_ALIVE(pedSelection)
				SET_ENTITY_HEADING(pedSelection, sCurrentVarsStruct.fCreationHeading)
				SET_ENTITY_COLLISION(pedSelection, FALSE)	
				SET_MODEL_AS_NO_LONGER_NEEDED(mnToUse)
				SET_ENTITY_VISIBLE(pedSelection, FALSE)
			ENDIF
		ENDIF
	ELSE
		IF IS_ENTITY_ALIVE(pedSelection)
			SET_ENTITY_COORDS(pedSelection, sCurrentVarsStruct.vCoronaPos, TRUE)
			IF HAS_NET_TIMER_EXPIRED(tdPedUpdate, ciPedUpdateTime)
				SET_ENTITY_VISIBLE(pedSelection, TRUE)
			ENDIF
			FLOAT fHeadingToAdd = 0.0
			fHeadingToAdd = ADD_TO_CURRENT_HEADING(sCurrentVarsStruct.fRotationVeloc, DEFAULT, DEFAULT, sFMMCmenu.bForceRotate)
			IF fHeadingToAdd != 0.0
				FLOAT fHeading = GET_ENTITY_HEADING(pedSelection) - fHeadingToAdd
				sCurrentVarsStruct.fCreationHeading = fHeading
				SET_ENTITY_HEADING(pedSelection, fHeading)
			ENDIF
		ENDIF
		IF (DOES_ENTITY_EXIST(pedSelection)
		AND GET_ENTITY_MODEL(pedSelection) != mnToUse)
		OR FMMC_IS_CANCEL_JUST_PRESSED(sFMMCmenu)
			DELETE_PED(pedSelection)
		ENDIF
	ENDIF
ENDPROC
PROC HANDLE_DISPLAYING_LAND_VEH_MODEL()
	IF bRunTutorial
	AND sSurvTutorial.iSubstage < 6
	AND sSurvTutorial.eSurvivalTutStage = SURV_CREATOR_TUTORIAL_STAGE_LAND_VEHICLES
		EXIT
	ENDIF
	INT iWaveToUse, iVehToUse
	iWaveToUse = sFMMCmenu.iSurvivalWaveSelection
	IF iWaveToUse = -1
		iWaveToUse = 0
	ENDIF
	iVehToUse = sFMMCmenu.iSurvivalVehicleSelection
	IF iVehToUse = -1
		iVehToUse = 0
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(vehSelection)
		IF g_FMMC_STRUCT.sSurvivalWaveData.mnVeh[iWaveToUse][iVehToUse] != DUMMY_MODEL_FOR_SCRIPT
			IF mnVehSelectionModel != g_FMMC_STRUCT.sSurvivalWaveData.mnVeh[iWaveToUse][iVehToUse]
			AND mnVehSelectionModel != DUMMY_MODEL_FOR_SCRIPT
				SET_MODEL_AS_NO_LONGER_NEEDED(mnVehSelectionModel)
			ENDIF
			REQUEST_MODEL(g_FMMC_STRUCT.sSurvivalWaveData.mnVeh[iWaveToUse][iVehToUse])
			mnVehSelectionModel = g_FMMC_STRUCT.sSurvivalWaveData.mnVeh[iWaveToUse][iVehToUse]
			IF HAS_MODEL_LOADED(g_FMMC_STRUCT.sSurvivalWaveData.mnVeh[iWaveToUse][iVehToUse])
				vehSelection = CREATE_VEHICLE(g_FMMC_STRUCT.sSurvivalWaveData.mnVeh[iWaveToUse][iVehToUse], sCurrentVarsStruct.vCoronaPos, sCurrentVarsStruct.fCreationHeading, FALSE, FALSE)
				IF IS_ENTITY_ALIVE(vehSelection)
					SET_ENTITY_HEADING(vehSelection, sCurrentVarsStruct.fCreationHeading)
					SET_ENTITY_COLLISION(vehSelection, FALSE)	
					SET_MODEL_AS_NO_LONGER_NEEDED(g_FMMC_STRUCT.sSurvivalWaveData.mnVeh[iWaveToUse][iVehToUse])
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF IS_ENTITY_ALIVE(vehSelection)
			SET_ENTITY_COORDS(vehSelection, sCurrentVarsStruct.vCoronaPos, TRUE)
			SET_ENTITY_HEADING(vehSelection, sCurrentVarsStruct.fCreationHeading)
			FLOAT fHeadingToAdd = 0.0
			fHeadingToAdd = ADD_TO_CURRENT_HEADING(sCurrentVarsStruct.fRotationVeloc, DEFAULT, DEFAULT, sFMMCmenu.bForceRotate)
			IF fHeadingToAdd != 0.0
				FLOAT fHeading = GET_ENTITY_HEADING(vehSelection) - fHeadingToAdd
				sCurrentVarsStruct.fCreationHeading = fHeading
				SET_ENTITY_HEADING(vehSelection, fHeading)
			ENDIF
			SURVIVAL_SET_VEHICLE_COLOUR(vehSelection, iWaveToUse, iVehToUse, FALSE)
		ENDIF
		IF GET_ENTITY_MODEL(vehSelection) != g_FMMC_STRUCT.sSurvivalWaveData.mnVeh[iWaveToUse][iVehToUse]
		OR FMMC_IS_CANCEL_JUST_PRESSED(sFMMCmenu)
			DELETE_VEHICLE(vehSelection)
		ENDIF
	ENDIF
ENDPROC
PROC HANDLE_DISPLAYING_AIR_VEH_MODEL()
	INT iWaveToUse, iVehToUse
	iWaveToUse = sFMMCmenu.iSurvivalWaveSelection
	IF iWaveToUse = -1
		iWaveToUse = 0
	ENDIF
	iVehToUse = sFMMCmenu.iSurvivalAirVehSelection
	IF iVehToUse = -1
		iVehToUse = 0
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(vehSelection)
	
		IF mnVehSelectionModel != g_FMMC_STRUCT.sSurvivalWaveData.mnAir[iWaveToUse][iVehToUse]
		AND mnVehSelectionModel != DUMMY_MODEL_FOR_SCRIPT
			SET_MODEL_AS_NO_LONGER_NEEDED(mnVehSelectionModel)
		ENDIF
		REQUEST_MODEL(g_FMMC_STRUCT.sSurvivalWaveData.mnAir[iWaveToUse][iVehToUse])
		mnVehSelectionModel = g_FMMC_STRUCT.sSurvivalWaveData.mnAir[iWaveToUse][iVehToUse]
		
		IF HAS_MODEL_LOADED(g_FMMC_STRUCT.sSurvivalWaveData.mnAir[iWaveToUse][iVehToUse])
			vehSelection = CREATE_VEHICLE(g_FMMC_STRUCT.sSurvivalWaveData.mnAir[iWaveToUse][iVehToUse], sCurrentVarsStruct.vCoronaPos, sCurrentVarsStruct.fCreationHeading, FALSE, FALSE)
			IF IS_ENTITY_ALIVE(vehSelection)
				SET_ENTITY_HEADING(vehSelection, sCurrentVarsStruct.fCreationHeading)
				SET_ENTITY_COLLISION(vehSelection, FALSE)
				IF GET_ENTITY_MODEL(vehSelection) = POLMAV
					SET_VEHICLE_LIVERY(vehSelection, 0) //Keeps it as a police vehicle rather than an Air Ambulance
				ENDIF
				SET_MODEL_AS_NO_LONGER_NEEDED(g_FMMC_STRUCT.sSurvivalWaveData.mnAir[iWaveToUse][iVehToUse])
			ENDIF
		ENDIF
	ELSE
		IF IS_ENTITY_ALIVE(vehSelection)
			SET_ENTITY_COORDS(vehSelection, sCurrentVarsStruct.vCoronaPos, TRUE)
			SET_ENTITY_HEADING(vehSelection, sCurrentVarsStruct.fCreationHeading)
			FLOAT fHeadingToAdd = 0.0
			fHeadingToAdd = ADD_TO_CURRENT_HEADING(sCurrentVarsStruct.fRotationVeloc, DEFAULT, DEFAULT, sFMMCmenu.bForceRotate)
			IF fHeadingToAdd != 0.0
				FLOAT fHeading = GET_ENTITY_HEADING(vehSelection) - fHeadingToAdd
				sCurrentVarsStruct.fCreationHeading = fHeading
				SET_ENTITY_HEADING(vehSelection, fHeading)
			ENDIF
			SURVIVAL_SET_VEHICLE_COLOUR(vehSelection, iWaveToUse, iVehToUse, TRUE)
		ENDIF
		IF GET_ENTITY_MODEL(vehSelection) != g_FMMC_STRUCT.sSurvivalWaveData.mnAir[iWaveToUse][iVehToUse]
		OR FMMC_IS_CANCEL_JUST_PRESSED(sFMMCmenu)
			DELETE_VEHICLE(vehSelection)
		ENDIF
	ENDIF
ENDPROC

PROC CLEANUP_SELECTED_MODEL_PREVIEW_IF_NEEDED()
	IF DOES_ENTITY_EXIST(pedSelection)
	AND sFMMCmenu.sActiveMenu != eFmmc_SURVIVAL_Wave_Ped_Model
	AND sFMMCmenu.sActiveMenu != eFmmc_SURVIVAL_Squad_Model
	AND sFMMCmenu.sActiveMenu != eFmmc_SURVIVAL_Heavy_Model
		DELETE_PED(pedSelection)
		PRINTLN("CLEANUP_SELECTED_MODEL_PREVIEW_IF_NEEDED - Removing pedSelection as not in a valid menu for it")
	ENDIF
	
	IF DOES_ENTITY_EXIST(vehSelection)
	AND sFMMCmenu.sActiveMenu != eFmmc_SURVIVAL_Veh_Wave_Model
	AND sFMMCmenu.sActiveMenu != eFmmc_SURVIVAL_Veh_Select
	AND sFMMCmenu.sActiveMenu != eFmmc_SURVIVAL_Air_Wave_Model
	AND sFMMCmenu.sActiveMenu != eFmmc_SURVIVAL_Air_Select
		DELETE_VEHICLE(vehSelection)
		PRINTLN("CLEANUP_SELECTED_MODEL_PREVIEW_IF_NEEDED - Removing vehSelection as not in a valid menu for it")
	ENDIF
ENDPROC

PROC GET_WEAPON_INFO_FOR_OVERRIDE_WAVE_SWAP()
	PICKUP_TYPE ptPickup
	IF sFMMCmenu.iSelectedEntity = -1
		ptPickup = g_FMMC_STRUCT_ENTITIES.sEditedWeapon.ptSurvival_WeaponOverridePerWave[sFMMCmenu.iSurvivalWaveSelection]
		sWepStruct.iSelectedWeaponType = GET_WEAPON_SELECTION_FROM_PICK_UP_TYPE(sWepStruct, ptPickup, TRUE, g_FMMC_STRUCT_ENTITIES.sEditedWeapon.iSubType) 
		sFMMCmenu.iWepLibrary = GET_SELECTED_WEAPON_LIBRARY(sWepStruct.iSelectedWeaponType, TRUE)
	ELSE
		ptPickup = g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[sFMMCmenu.iSelectedEntity].ptSurvival_WeaponOverridePerWave[sFMMCmenu.iSurvivalWaveSelection]
		sWepStruct.iSelectedWeaponType = GET_WEAPON_SELECTION_FROM_PICK_UP_TYPE(sWepStruct, ptPickup, TRUE, g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[sFMMCmenu.iSelectedEntity].iSubType) 
		sFMMCmenu.iWepLibrary = GET_SELECTED_WEAPON_LIBRARY(sWepStruct.iSelectedWeaponType, TRUE)
	ENDIF
	
	sFMMCmenu.iCurrentEntitySelection[CREATION_TYPE_WEAPONS] = sWepStruct.iSelectedWeaponType
	sFMMCmenu.iCurrentEntitySelection[CREATION_TYPE_WEAPON_LIBRARY] = sFMMCmenu.iWepLibrary
	sFMMCmenu.sSubTypeName[CREATION_TYPE_WEAPONS] = GET_WEAPON_NAME_FROM_WEAPON_TYPE(sWepStruct.wtGunType[sWepStruct.iSelectedWeaponType])
ENDPROC

PROC HANDLE_EVERY_FRAME_MENU_FUNCTIONALITY()
	SWITCH sFMMCmenu.sActiveMenu
		CASE eFmmc_SURVIVAL_Wave_Base
			EDIT_SURVIVAL_WAVE_BASE_MENU(sFMMCendStage, sFMMCmenu)
		BREAK
		
		CASE eFmmc_SURVIVAL_Veh_Config_base
			EDIT_SURVIVAL_VEH_BASE_MENU(sFMMCendStage, sFMMCmenu)
		BREAK
		
		CASE eFmmc_SURVIVAL_Veh_Config
			EDIT_SURVIVAL_VEH_CONFIG_MENU(sFMMCendStage, sFMMCmenu, 0)
		BREAK
		
		CASE eFmmc_SURVIVAL_WARNING_AREA
			HANDLE_PLAY_AREA_BOUNDS_PLACEMENT()
		BREAK
		
		CASE eFmmc_SURVIVAL_ENEMY_AREA
			HANDLE_ENEMY_SPAWN_AREA_PLACEMENT()
			HANDLE_SURVIVAL_E_SPWN_MENU(sFMMCendStage, sFMMCmenu, 0)
		BREAK
		
		CASE eFmmc_SURVIVAL_Vehicle_Spawn
			HANDLE_VEHICLE_SPAWN_PLACEMENT_AREA()
		BREAK
		
		CASE eFmmc_SURVIVAL_Air_Base
			EDIT_SURVIVAL_AIR_VEH_BASE_MENU(sFMMCendStage, sFMMCmenu)
		BREAK
		
		CASE eFmmc_SURVIVAL_Squad_Weapons
			EDIT_SURVIVAL_SQUAD_WEAPONS_MENU(sFMMCmenu, sWepStruct)
		BREAK
		
		CASE eFmmc_SURVIVAL_Squad_Weapon_Select
			EDIT_SURVIVAL_WEAPON_DATA_SELECTION_MENU(sFMMCmenu, sWepStruct)
			IF sWepStruct.iSelectedWeaponType != -1
			AND sFMMCmenu.iWepLibrary != FMMC_WEAPON_LIBRARY_SPECIAL
				RENDER_WEAPON_STATS(sFMMCmenu, sWepStruct.wtGunType[sWepStruct.iSelectedWeaponType], sWeaponValues, sModValues)
			ENDIF
		BREAK
		
		CASE eFmmc_SURVIVAL_Wave_Ped_Model
		CASE eFmmc_SURVIVAL_Squad_Model
		CASE eFmmc_SURVIVAL_Heavy_Model
			HANDLE_DISPLAYING_PED_MODEL()
		BREAK
		
		CASE eFmmc_SURVIVAL_Veh_Wave_Model
		CASE eFmmc_SURVIVAL_Veh_Select
			HANDLE_DISPLAYING_LAND_VEH_MODEL()
		BREAK
		
		CASE eFmmc_SURVIVAL_Air_Wave_Model
		CASE eFmmc_SURVIVAL_Air_Select
			HANDLE_DISPLAYING_AIR_VEH_MODEL()
		BREAK

		CASE eFmmc_SURVIVAL_Loadout_Weapons
			IF sFMMCmenu.iSurvivalWeaponSelection != GET_CREATOR_MENU_SELECTION(sFMMCMenu) - 1
				IF GET_CREATOR_MENU_SELECTION(sFMMCMenu) > 0
					sFMMCmenu.iSurvivalWeaponSelection = GET_CREATOR_MENU_SELECTION(sFMMCMenu) - 1
				ENDIF
			ENDIF
		BREAK
		CASE eFmmc_SURVIVAL_Loadout_Weapon_Select
			EDIT_SURVIVAL_LOADOUT_SET_PICKUP_MENU_SELECTION_MENU(sFMMCmenu, sWepStruct)
			IF sWepStruct.iSelectedWeaponType != -1
			AND sFMMCmenu.iWepLibrary != FMMC_WEAPON_LIBRARY_SPECIAL
				RENDER_WEAPON_STATS(sFMMCmenu, sWepStruct.wtGunType[sWepStruct.iSelectedWeaponType], sWeaponValues, sModValues)
			ENDIF
		BREAK
		
		CASE eFmmc_SURVIVAL_Pickup_Placement_Override
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_X)
			AND g_FMMC_STRUCT_ENTITIES.sEditedWeapon.ptSurvival_WeaponOverridePerWave[GET_CREATOR_MENU_SELECTION(sFMMCmenu) + 1] != NUM_PICKUPS
				g_FMMC_STRUCT_ENTITIES.sEditedWeapon.ptSurvival_WeaponOverridePerWave[GET_CREATOR_MENU_SELECTION(sFMMCmenu) + 1] = NUM_PICKUPS
				REFRESH_MENU(sFMMCmenu)
			ENDIF
			
			HANDLE_SURVIVAL_WEAPON_PLACEMENT_OVERRIDE_WEAPON_SELECTION(sFMMCmenu, sWepStruct)
		BREAK
		CASE eFmmc_SURVIVAL_Pickup_Placement_Choose_Weapon
			EDIT_SURVIVAL_WEAPON_OVERRIDE_TYPE_SELECTION_MENU(sFMMCmenu, sWepStruct)
			IF sWepStruct.iSelectedWeaponType != -1
			AND sFMMCmenu.iWepLibrary != FMMC_WEAPON_LIBRARY_SPECIAL
				RENDER_WEAPON_STATS(sFMMCmenu, sWepStruct.wtGunType[sWepStruct.iSelectedWeaponType], sWeaponValues, sModValues)
			ENDIF
		BREAK
		
	ENDSWITCH
ENDPROC

PROC EDIT_MENU_OPTIONS(INT iChange)
	INT iType
	SWITCH sFMMCmenu.sActiveMenu
		CASE eFmmc_TOP_MENU
			
		BREAK
		
		CASE eFmmc_MAIN_OPTIONS_BASE
			EDIT_SURVIVAL_BASE_OPTIONS_MENU(sFMMCendStage, sFMMCmenu, iChange)
		BREAK
		
		CASE eFmmc_PHOTO_CAM_BASE
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 1
				IF IS_BIT_SET(sFMMCmenu.iMiscBitSet, bsHidePreviewPhoto)
					CLEAR_BIT(sFMMCmenu.iMiscBitSet, bsHidePreviewPhoto)
				ELSE
					SET_BIT(sFMMCmenu.iMiscBitSet, bsHidePreviewPhoto)
				ENDIF
			ENDIF
		BREAK
		
		CASE eFmmc_SURVIVAL_Wave_Base
			EDIT_SURVIVAL_WAVE_BASE_MENU(sFMMCendStage, sFMMCmenu)
		BREAK
		
		CASE eFmmc_SURVIVAL_Wave_Config
			EDIT_SURVIVAL_WAVE_CONFIG_MENU(sFMMCendStage, sFMMCmenu, iChange)
		BREAK
		
		CASE eFmmc_SURVIVAL_Wave_Adv_Config
			EDIT_SURVIVAL_ADV_WAVE_CONFIG_MENU(sFMMCendStage, sFMMCmenu, iChange)
		BREAK
		
		CASE eFmmc_SURVIVAL_Veh_Wave_Model
			EDIT_SURVIVAL_VEH_WAVE_MODEL_MENU(iChange)
		BREAK
		
		CASE eFmmc_SURVIVAL_Air_Wave_Model
			EDIT_SURVIVAL_AIR_WAVE_MODEL_MENU(iChange)
		BREAK
		
		CASE eFmmc_SURVIVAL_Wave_Ped_Model
			EDIT_SURVIVAL_PED_WAVE_MODEL_MENU(iChange)
		BREAK
		
		CASE eFmmc_SURVIVAL_Squad_Config
			EDIT_SURVIVAL_SQUAD_CONFIG_MENU(sFMMCendStage, sFMMCmenu, iChange)
		BREAK
		
		CASE eFmmc_SURVIVAL_Squad_Model
			EDIT_SURVIVAL_SQUAD_MODEL_SELECTION_MENU(iChange)
		BREAK
		
		CASE eFmmc_SURVIVAL_Veh_Config_Base
			EDIT_SURVIVAL_VEH_BASE_MENU(sFMMCendStage, sFMMCmenu)
		BREAK
		
		CASE eFmmc_SURVIVAL_Veh_Select
			EDIT_SURVIVAL_VEH_CONFIG_SELECTION_MENU(iChange)
		BREAK
		
		CASE eFmmc_SURVIVAL_Veh_Config
			EDIT_SURVIVAL_VEH_CONFIG_MENU(sFMMCendStage, sFMMCmenu, iChange)
		BREAK
		
		CASE eFmmc_SURVIVAL_WARNING_AREA
			EDIT_SURVIVAL_BOUNDS_MENU(sFMMCendStage, sFMMCmenu, iChange)
			IF iChange != 0
			AND GET_CREATOR_MENU_SELECTION(sFMMCMenu) = OPTION_SURV_BOUNDS_TYPE
			AND NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sSurvivalWaveData.vBounds[0])
			AND sFMMCMenu.iBoundsEditPointIndex = -1
				sCurrentVarsStruct.iLastChangeValue = iChange
				TRIGGER_CONFIRMATION_SCREEN(sCurrentVarsStruct, "SC_AYS", "SC_RESET_CPAT")
			ENDIF
		BREAK
		
		CASE eFmmc_SURVIVAL_ENEMY_AREA
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_SURV_E_SPWN_CYCLE
				DEAL_WITH_CAMERA_SWITCH_SELECTION(iChange > 0, g_FMMC_STRUCT_ENTITIES.iNumberOfPeds)
			ENDIF
		BREAK
		
		CASE eFmmc_SURVIVAL_Vehicle_Spawn
			EDIT_SURVIVAL_VEH_SPAWN_MENU(sFMMCendStage, sFMMCmenu, iChange)
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_SURV_VEH_SPAWN_CYCLE
				DEAL_WITH_CAMERA_SWITCH_SELECTION(iChange > 0, g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles)
			ENDIF
		BREAK
		
		CASE eFmmc_SURVIVAL_Air_Base
			EDIT_SURVIVAL_AIR_VEH_BASE_MENU(sFMMCendStage, sFMMCmenu)
		BREAK
		
		CASE eFmmc_SURVIVAL_Air_Config
			HANDLE_SURVIVAL_AIR_CONFIG_MENU(sFMMCendStage, sFMMCmenu, iChange)
		BREAK
		
		CASE eFmmc_SURVIVAL_Air_Select
			HANDLE_SURVIVAL_AIR_CONFIG_SELECTION(iChange)
		BREAK
		
		CASE eFmmc_SURVIVAL_Ambient
			HANDLE_SURVIVAL_AMIBENT_SETTINGS(sFMMCendStage, sFMMCmenu, iChange)
		BREAK
		
		CASE eFmmc_SURVIVAL_Heavy_Config
			HANDLE_SURVIVAL_HEAVY_MENU(sFMMCendStage, sFMMCmenu, iChange)
		BREAK
		
		CASE eFmmc_SURVIVAL_Heavy_Model
			HANDLE_SURVIVAL_HEAVY_MODEL_SELECTION_MENU(iChange)
		BREAK
		
		CASE eFmmc_SURVIVAL_Zone_Menu
			HANDLE_SURVIVAL_ZONES_MENU(sFMMCendStage, sFMMCmenu, iChange)
		BREAK
		
		CASE eFmmc_SURVIVAL_Loadout_Weapons
			HANDLE_SURVIVAL_LOADOUT_WEAPONS_MENU(sFMMCendStage, sFMMCmenu, iChange)
		BREAK
		
		CASE eFmmc_SURVIVAL_Dev_Menu
			HANDLE_SURVIVAL_DEV_ONLY_MENU(sFMMCendStage, sFMMCmenu, iChange)
		BREAK
		
		CASE eFmmc_SURVIVAL_Halloween_Menu
			HANDLE_SURVIVAL_HALLOWEEN_MENU(sFMMCendStage, sFMMCmenu, iChange)
		BREAK
		
		CASE eFmmc_SURVIVAL_Defaults_Menu
			HANDLE_SURVIVAL_DEFAULTS_MENU(sFMMCendStage, sFMMCmenu, iChange)
		BREAK
		
		CASE eFmmc_SURVIVAL_Copy_Wave
			HANDLE_SURVIVAL_COPY_WAVE_MENU(sFMMCendStage, sFMMCmenu, iChange)
		BREAK
		
		CASE eFmmc_DEV_AMBIENT_OPTIONS
			EDIT_DEV_AMBIENT_OPTIONS(sFMMCmenu, sFMMCendStage, iChange)
		BREAK
		
		CASE eFmmc_PICKUP_BASE
		CASE eFmmc_SURVIVAL_Squad_Weapon_Select
		CASE eFmmc_SURVIVAL_Loadout_Weapon_Select
		
			IF sFMMCmenu.sActiveMenu =	eFmmc_PICKUP_BASE
			
				IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = SURVIVAL_PICKUP_MENU_LIBRARY
					EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, sFMMCmenu.iSurvivalPickup_Category, iChange, SURVIVAL_PICKUP_CATEGORY_MAX)
				ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = SURVIVAL_PICKUP_MENU_CYCLE
					DEAL_WITH_CAMERA_SWITCH_SELECTION(iChange > 0, g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons)
				ENDIF
			
				EXIT
			ENDIF
			
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 0
				iType = CREATION_TYPE_WEAPON_LIBRARY
				sFMMCmenu.iCurrentEntitySelection[iType] = sFMMCmenu.iWepLibrary
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 1
				iType = CREATION_TYPE_WEAPONS
			ELSE
				DEAL_WITH_CAMERA_SWITCH_SELECTION(iChange > 0, g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons)
			ENDIF
		BREAK
		
		CASE eFmmc_SURVIVAL_Pickup_Placement_Choose_Weapon	
			
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 0
								
				EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, sFMMCmenu.iSurvivalWaveSelection, iChange, g_FMMC_STRUCT.sSurvivalWaveData.iNumberOfWaves+1, 1)
				
				GET_WEAPON_INFO_FOR_OVERRIDE_WAVE_SWAP()
				
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 1
				iType = CREATION_TYPE_WEAPON_LIBRARY
				sFMMCmenu.iCurrentEntitySelection[iType] = sFMMCmenu.iWepLibrary
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 2
				iType = CREATION_TYPE_WEAPONS
			ELSE
				DEAL_WITH_CAMERA_SWITCH_SELECTION(iChange > 0, g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons)
			ENDIF
		BREAK
		
		CASE eFmmc_PROP_BASE
			MODEL_NAMES mnTemp 
			mnTemp = GET_CREATOR_PROP_MODEL(sFMMCMenu.iPropLibrary, sFMMCMenu.iPropType)
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_LIBRARY
				CLEAR_LONG_BITSET(sCurrentVarsStruct.iTooManyEntities)
				CHANGE_PROP_LIBRARY(sFMMCmenu, iChange)
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_CREATE
				CHANGE_PROP_TYPE(sFMMCmenu, iChange)
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_SET_INVISIBLE
				EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCmenu.iPropBitSet, ciFMMC_PROP_SetInvisible)
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_COLOUR AND NOT g_sMPTunables.bDisable_Prop_Colour
				
				MODEL_NAMES mnProp 
				mnProp = GET_CREATOR_PROP_MODEL(sFMMCMenu.iPropLibrary, sFMMCMenu.iPropType)
				
				IF sFMMCMenu.iPropLibrary = PROP_LIBRARY_STUNT_NEON_TUBE
					CHANGE_PROP_TYPE(sFMMCmenu, iChange*PROP_LIBRARY_STUNT_NEON_TUBE_TYPES)
					REFRESH_MENU(sFMMCmenu)
				ELSE
					INT i
					
					REPEAT MAX_COLOUR_PALETTE i
						EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, sFMMCmenu.iPropColour, iChange, MAX_COLOUR_PALETTE)
						
						IF IS_PROP_COLOUR_VALID(mnProp, sFMMCmenu.iPropColour)
							BREAKLOOP
						ENDIF
					ENDREPEAT
				ENDIF
			ENDIF
			IF NOT IS_STUNT_LIBRARY(sFMMCMenu.iPropLibrary)
				IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_ROTATION_TYPE
					IF (NOT IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround) OR IS_ROCKSTAR_DEV())
					AND IS_PROP_SUITABLE_FOR_ROTATION(mnTemp,sFMMCMenu.bPropSnapped)
						CHANGE_FREE_ROTATION_AXIS(sFMMCmenu, iChange, sPropStruct.viCoronaObj)
						PRINTLN("[LH] CHANGE_FREE_ROTATION_AXIS")
					ENDIF
				ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_STACKING
					IF NOT IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
						EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCmenu.iPlacementOverrides, ciBS_PLACEMENT_OVERRIDE_STACK)
						REFRESH_MENU(sFMMCmenu)
					ENDIF
				ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_SNAPPING
				AND (IS_ROCKSTAR_DEV() AND sFMMCmenu.iPropLibrary = PROP_LIBRARY_TEMPLATES)
					EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCmenu.iSnappingBitset, ciBS_SNAPPING_ACTIVE)
					ENSURE_A_SNAP_MODE_IS_ACTIVE(sFMMCmenu)
				ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = sFMMCMenu.iMenuLengthForSnappingOptions
				AND (sFMMCMenu.iPropLibrary = PROP_LIBRARY_TEMPLATES)
					IF IS_ROCKSTAR_DEV()
						EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCmenu.iSnappingBitset, ciBS_SNAPPING_ACTIVE)
						ENSURE_A_SNAP_MODE_IS_ACTIVE(sFMMCmenu)
					ENDIF				
				ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = sFMMCMenu.iMenuLengthForSnappingEntity
				AND (sFMMCMenu.iPropLibrary = PROP_LIBRARY_TEMPLATES)
					IF IS_ROCKSTAR_DEV()
						EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, sFMMCmenu.iPropSnapParentEntity, iChange, GET_NUMBER_OF_PROPS_IN_SAVED_TEMPLATE(sFMMCmenu.iProptype), -1)
					ENDIF
				ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = sFMMCMenu.iMenuLengthForPressurePad
					EDIT_PRESSUREPAD_TRAP_SELECTION_MENU_ITEM(sFMMCendStage, sFMMCMenu, iChange)
				ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = sFMMCMenu.iMenuLengthForPressurePadDelay
				AND sFMMCMenu.iObjectPressurePadIndex >= 0
					EDIT_TRAP_PRESSUREPAD_DELAY_MENU_ITEM(sFMMCMenu, sFMMCendStage, iChange)
				ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = sFMMCMenu.iPropDisplayIndexes_MenuPos
					IF IS_ROCKSTAR_DEV()
						EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCMenu.iCreatorInfoBitset, ciCIB_DRAW_PROP_NUMBERS)
					ENDIF
					
				ELIF sFMMCmenu.iPropLibrary = PROP_LIBRARY_TEMPLATES
				AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = GET_CORRECT_CYCLE_PROPS_OPTION(sFMMCmenu)
					DEAL_WITH_CAMERA_SWITCH_SELECTION(iChange > 0, g_FMMC_STRUCT_ENTITIES.iNumberOfProps)
				ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = GET_CORRECT_CYCLE_PROPS_OPTION(sFMMCmenu)
					DEAL_WITH_CAMERA_SWITCH_SELECTION(iChange > 0, g_FMMC_STRUCT_ENTITIES.iNumberOfProps)
				ELIF sFMMCMenu.iPropLibrary = PROP_LIBRARY_DEV2
				AND IS_ROCKSTAR_DEV()
				AND (mnTemp = INT_TO_ENUM(MODEL_NAMES, HASH("imp_prop_ship_01a")) 
					OR mnTemp = INT_TO_ENUM(MODEL_NAMES, HASH("gr_Prop_DamShip_01a"))
					OR mnTemp = INT_TO_ENUM(MODEL_NAMES, HASH("p_spinning_anus_s")))
					IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = sFMMCMenu.iUFOPropOptionsStart
						EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCMenu.iPropBitSet2, ciFMMC_PROP2_UFO_Spin)
					ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = sFMMCMenu.iUFOPropOptionsStart+1
						EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCMenu.iPropBitSet2, ciFMMC_PROP2_UFO_Light)
					ENDIF
				ENDIF
			ELSE
				MODEL_NAMES mnProp 
				mnProp = GET_CREATOR_PROP_MODEL(sFMMCMenu.iPropLibrary, sFMMCMenu.iPropType)
				IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_ROTATION_TYPE
					IF (NOT IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround) OR IS_ROCKSTAR_DEV())
					AND IS_PROP_SUITABLE_FOR_ROTATION(mnTemp,sFMMCMenu.bPropSnapped)
						CHANGE_FREE_ROTATION_AXIS(sFMMCmenu, iChange, sPropStruct.viCoronaObj)
						PRINTLN("[LH] CHANGE_FREE_ROTATION_AXIS")
					ENDIF
				ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_STACKING
					IF NOT IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
						EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCmenu.iPlacementOverrides, ciBS_PLACEMENT_OVERRIDE_STACK)
						REFRESH_MENU(sFMMCmenu)
					ENDIF			
				ELIF IS_STUNT_TRACK_LIBRARY(sFMMCMenu.iPropLibrary)
				OR IS_SNAPPABLE_STUNT_PROP(mnProp)
				OR (IS_ROCKSTAR_DEV() AND sFMMCmenu.iPropLibrary = PROP_LIBRARY_TEMPLATES)
					IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SHARED_SNAPPING
						EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCmenu.iSnappingBitset, ciBS_SNAPPING_ACTIVE)
						ENSURE_A_SNAP_MODE_IS_ACTIVE(sFMMCmenu)
					ENDIF
				ELIF sFMMCmenu.iPropLibrary = PROP_LIBRARY_STUNT_SPECIAL
					IF mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("PROP_FLARE_01"))
					OR mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("ind_prop_firework_01"))
					OR mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("ind_prop_firework_03"))
						IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_ST_FLARE_PLAYERS AND NOT g_sMPTunables.bDisable_Fireworks_players_to_trigger_for
							EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCMenu.iPropBitSet,  ciFMMC_PROP_TriggerForAllRacers)
						ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_ST_FLARE_LAPS AND NOT g_sMPTunables.bDisable_Fireworks_Laps_to_Trigger_on
							EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCMenu.iPropBitSet,  ciFMMC_PROP_TriggerOnEachLap)
						ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_ST_FLARE_RADIUS
							EDIT_CREATOR_FLOAT_MENU_ITEM(sFMMCendStage, sFMMCMenu, sFMMCmenu.fPTFXTriggerRadius, TO_FLOAT(iChange*1),100.0)
						ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_ST_FLARE_SUB_TYPE
							EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, sFMMCmenu.iPropEffectSubType, iChange, ciSTUNT_FIREWORK_VFX_MAX, -1)
						ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_ST_FLARE_COLOUR
							EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, sFMMCmenu.iPropColouring, iChange, ciHUD_COLOUR_MAX -1)
						ENDIF
						
					ELIF mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_race_tannoy"))
						IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = sFMMCMenu.iAlarmTypePos
							EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, sFMMCmenu.iAlarmSound, iChange, 12)
							DEAL_WITH_CHANGING_STUNT_ALARM_PREVIEW(sFMMCmenu, TRUE)
						ENDIF	
					ENDIF
				ENDIF
				IF IS_SPEED_BOOST_PROP(mnProp)
					IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = sFMMCmenu.iSpeedBoostPropMenuPosition
						EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, sFMMCmenu.iSpeedBoostAmount, iChange, 6, 1, TRUE)
					ENDIF		
				ENDIF
				IF mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_SlowDown"))
				OR mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_SlowDown_T1"))
				OR mnProp = INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_track_SlowDown_T2")) 
					IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = sFMMCmenu.iSpeedBoostPropMenuPosition
						EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, sFMMCmenu.iSpeedBoostAmount, iChange, 4, 1, TRUE)
					ENDIF		
				ENDIF			
				IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = GET_CORRECT_CYCLE_PROPS_OPTION(sFMMCmenu)
					DEAL_WITH_CAMERA_SWITCH_SELECTION(iChange > 0, g_FMMC_STRUCT_ENTITIES.iNumberOfProps)
				ENDIF
				IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = sFMMCMenu.iMenuLengthForPressurePad
					EDIT_PRESSUREPAD_TRAP_SELECTION_MENU_ITEM(sFMMCendStage, sFMMCMenu, iChange)
				ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = sFMMCMenu.iMenuLengthForPressurePadDelay
				AND sFMMCMenu.iObjectPressurePadIndex >= 0
					EDIT_TRAP_PRESSUREPAD_DELAY_MENU_ITEM(sFMMCMenu, sFMMCendStage, iChange)
				ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = sFMMCMenu.iPropTurretTower_MenuPos
					EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCMenu.iPropBitSet2,  ciFMMC_PROP2_UseAsTurret)
				ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = sFMMCMenu.iPropTurretTower_Heading
					EDIT_CREATOR_FLOAT_MENU_ITEM(sFMMCendStage, sFMMCMenu, sFMMCMenu.fTurretHeading, TO_FLOAT(iChange)*5, 360.0, 0.0)
				ENDIF
			ENDIF
		BREAK
		
		CASE eFmmc_DYNOPROP_BASE
			MODEL_NAMES mnProp 
			mnProp = GET_CREATOR_PROP_MODEL(sFMMCMenu.iPropLibrary, sFMMCMenu.iPropType)
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DYNOPROP_MENU_LIBRARY
				CHANGE_PROP_LIBRARY(sFMMCmenu, iChange)
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DYNOPROP_MENU_CREATE
				CHANGE_PROP_TYPE(sFMMCmenu, iChange)
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DYNOPROP_MENU_ROTATION_TYPE
				IF (NOT IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround) OR IS_ROCKSTAR_DEV())
				AND IS_PROP_SUITABLE_FOR_ROTATION(mnProp,sFMMCMenu.bPropSnapped)
					CHANGE_FREE_ROTATION_AXIS(sFMMCmenu, iChange, sDynoPropStruct.viCoronaObj)
					PRINTLN("[LH] CHANGE_FREE_ROTATION_AXIS (Dynoprop)")
				ENDIF
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = DYNOPROP_MENU_COLOUR AND DOES_PROP_HAVE_EXTRA_COLOURS(mnProp)
				INT i
						
				REPEAT MAX_COLOUR_PALETTE i
					EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, sFMMCmenu.iPropColour, iChange, MAX_COLOUR_PALETTE)
					
					IF IS_PROP_COLOUR_VALID(mnProp, sFMMCmenu.iPropColour)
						BREAKLOOP
					ENDIF
				ENDREPEAT
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = (sFMMCMenu.iCurrentMenuLength - 1)
				DEAL_WITH_CAMERA_SWITCH_SELECTION(iChange > 0, g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps)
			ENDIF
		BREAK
		
		CASE eFmmc_DELETE_ENTITIES
			
		BREAK
		
		CASE eFmmc_TEAM_TEST
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 0
			AND NOT g_bFMMC_TutorialSelectedFromMpSkyMenu
				EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, g_iStartingWave, iChange, g_FMMC_STRUCT.sSurvivalWaveData.iNumberOfWaves+1, 1)
			ENDIF
		BREAK
		
		CASE eFMMC_GENERIC_OVERRIDE_POSITION
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = POS_OVERRIDE_ALIGNMENT
				IF sFMMCmenu.iCurrentAlignment = FREE_POSITION_WORLD
					sFMMCmenu.iCurrentAlignment = FREE_POSITION_LOCAL
				ELSE
					sFMMCmenu.iCurrentAlignment = FREE_POSITION_WORLD
				ENDIF
			ENDIF
		BREAK
		
		CASE eFmmc_GENERIC_OVERRIDE_ROTATION
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ROT_OVERRIDE_ALIGNMENT
				IF IS_BIT_SET(sFMMCmenu.iPropBitset, ciFMMC_PROP_Rotation_Override)
					IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = ROT_OVERRIDE_ALIGNMENT
						IF sFMMCmenu.iCurrentAlignment_Rotation = FREE_POSITION_WORLD
							sFMMCmenu.iCurrentAlignment_Rotation = FREE_POSITION_LOCAL
						ELSE
							sFMMCmenu.iCurrentAlignment_Rotation = FREE_POSITION_WORLD
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE eFmmc_SNAPPING_OPTIONS
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SNAPPING_PROXIMITY
				EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCmenu.iSnappingBitset, ciBS_SNAPPING_PROXIMITY)
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SNAPPING_TRIGGERED
				EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCmenu.iSnappingBitset, ciBS_SNAPPING_TRIGGERED)
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SNAPPING_CHAIN
				EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCmenu.iSnappingBitset, ciBS_SNAPPING_CHAIN)
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SNAPPING_OVERRIDE
				EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCmenu.iSnappingBitset, ciBS_SNAPPING_OVERRIDES)
			ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = PROP_MENU_SNAPPING_ANGLE
				//Sets the angle to 0 first if it's currently -1
				IF sFMMCmenu.iCustomSnapRotationAngle = -1
					EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, sFMMCmenu.iCustomSnapRotationAngle, 1, 0, 0, FALSE)
				ENDIF
				
				EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, sFMMCmenu.iCustomSnapRotationAngle, iChange * ciFMMC_SnapRotationAngleIncrement, 270, 0, FALSE)
				
				//Sets the angle to -1 if it's been set to 0
				IF sFMMCmenu.iCustomSnapRotationAngle = 0
					EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, sFMMCmenu.iCustomSnapRotationAngle, -1, -1, -1, FALSE)
				ENDIF
			ENDIF
		BREAK
		
		CASE eFmmc_SURVIVAL_Spawn_Points
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_SURV_SPAWN_CYCLE
				DEAL_WITH_CAMERA_SWITCH_SELECTION(iChange > 0, g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[0])
			ENDIF
		BREAK	
		
		CASE efMMC_WORLD_PROPS
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 1
				DEAL_WITH_CAMERA_SWITCH_SELECTION(iChange > 0, g_FMMC_STRUCT_ENTITIES.iNumberOfWorldProps)
			ENDIF
		BREAK
		
		CASE eFmmc_DOORS_BASE
			EDIT_DOORS_LITE_MENU(sFMMCendStage, sFMMCMenu, iChange)
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 1
			AND sFMMCMenu.sCurrentDoor.mnDoorModel = DUMMY_MODEL_FOR_SCRIPT
				DEAL_WITH_CAMERA_SWITCH_SELECTION(TRUE, g_FMMC_STRUCT_ENTITIES.iNumberOfDoors)
			ENDIF
		BREAK
		
		CASE eFmmc_PROP_ADVANCED_OPTIONS
			//Lock Delete
			IF (GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 3 AND (IS_STUNT_TRACK_LIBRARY(sFMMCMenu.iPropLibrary) OR IS_CURRENT_PROP_LIBRARY_AN_ARENA_LIBRARY(sFMMCMenu)))   //Lock Delete full menu
			OR (GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 2 AND NOT (IS_STUNT_TRACK_LIBRARY(sFMMCMenu.iPropLibrary) OR IS_CURRENT_PROP_LIBRARY_AN_ARENA_LIBRARY(sFMMCMenu))) //Lock Delete smaller menu
				EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCmenu.iPropBitSet, ciFMMC_PROP_Lock_Delete)
			ENDIF
			//LOD override
			IF (GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 4 AND IS_STUNT_TRACK_LIBRARY(sFMMCMenu.iPropLibrary))   //LOD Distance full menu
			OR (GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 3 AND NOT IS_STUNT_TRACK_LIBRARY(sFMMCMenu.iPropLibrary)) //LOD Distance smaller menu
				EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, sFMMCmenu.iLODDistance, iChange * 15, 5001)
			ENDIF
			
			//No Collision
			IF (GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 5 AND IS_STUNT_TRACK_LIBRARY(sFMMCMenu.iPropLibrary))   //No collision full menu
			OR (GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 4 AND NOT IS_STUNT_TRACK_LIBRARY(sFMMCMenu.iPropLibrary)) //No collision smaller menu
				EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, sFMMCmenu.iPropBitSet2, ciFMMC_PROP2_NoCollision)
			ENDIF
		BREAK
		
		CASE efmmc_INTERIOR_LIST
		CASE efmmc_INTERIOR_IAA
		CASE efmmc_INTERIOR_MOC
		CASE efmmc_INTERIOR_BUNKER
		CASE efmmc_INTERIOR_SUB
		CASE efmmc_INTERIOR_HEISTS2
		CASE efmmc_INTERIOR_FIB
			EDIT_INTERIOR_OPTIONS_MENU(iChange, sFMMCmenu, sFMMCendStage)
		BREAK
		
		CASE efmmc_IPL_LIST
			EDIT_IPL_LIST_MENU(sFMMCmenu, sFMMCendStage)
		BREAK
				
	ENDSWITCH
	
	
	IF iType != -1
	
		sFMMCmenu.iCurrentEntitySelection[iType]+= iChange
		INT iMaximum
		IF iType = CREATION_TYPE_WEAPONS
			iMaximum = GET_WEAPON_TYPE_MAX(sFMMCmenu.iWepLibrary)
		ELIF iType = CREATION_TYPE_WEAPON_AMMO
			EDIT_CREATOR_INT_MENU_ITEM(sFMMCendStage, sWepStruct.iNumberOfClips, iChange, FMMC_MAX_CLIPS)
			sFMMCmenu.iWepClips = sWepStruct.iNumberOfClips
		ELIF iType = CREATION_TYPE_WEAPON_REST
			EDIT_CREATOR_BOOL_MENU_ITEM(sFMMCendStage, sWepStruct.bWepRest[GET_CREATOR_MENU_SELECTION(sFMMCmenu)-3])
			sFMMCmenu.bWepRest[GET_CREATOR_MENU_SELECTION(sFMMCmenu)-3] = sWepStruct.bWepRest[GET_CREATOR_MENU_SELECTION(sFMMCmenu)-3]
		ELIF iType = CREATION_TYPE_WEAPON_LIBRARY
			iMaximum = FMMC_WEAPON_LIBRARY_MAX
		ENDIF
		
		INT iWrapMax = iMaximum - 1
		
		IF sFMMCmenu.iCurrentEntitySelection[iType] < 0 
			sFMMCmenu.iCurrentEntitySelection[iType] = iWrapMax
		ELIF sFMMCmenu.iCurrentEntitySelection[iType] > iWrapMax
			sFMMCmenu.iCurrentEntitySelection[iType] = 0
		ENDIF
		
		IF iType = CREATION_TYPE_WEAPONS
			IF NOT (sFMMCmenu.iWepLibrary = FMMC_WEAPON_LIBRARY_SPECIAL AND sFMMCmenu.sActiveMenu = eFmmc_SURVIVAL_Loadout_Weapon_Select)
				CHANGE_WEAPON_TYPE(sFMMCmenu, sWepStruct, sFMMCmenu.iCurrentEntitySelection[iType], TRUE, iChange > 0, iMaximum)
			ENDIF
		ELIF iType = CREATION_TYPE_WEAPON_LIBRARY
			CHANGE_WEAPON_LIBRARY(sFMMCmenu, sWepStruct, sFMMCmenu.iCurrentEntitySelection[iType], TRUE)
		ENDIF
		
		PRINTLN("[TMS] sFMMCmenu.iWepLibrary: ", sFMMCmenu.iWepLibrary, " | iMaximum: ", iMaximum, " | sFMMCmenu.iCurrentEntitySelection[iType]: ", sFMMCmenu.iCurrentEntitySelection[iType])
	ENDIF
ENDPROC

FUNC BOOL IS_THIS_OPTION_A_MENU_ACTION()	
	
	IF sFMMCMenu.iBoundsEditPointIndex > -1
		RETURN FALSE
	ENDIF
	
	IF g_bFMMC_TutorialSelectedFromMpSkyMenu
	AND sFMMCmenu.sActiveMenu = eFmmc_TEAM_TEST
	AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_SURV_TEST_TEST
		RETURN FALSE
	ENDIF
	
	IF sFMMCmenu.sMenuAction[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] != eFmmc_Action_Null
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_THIS_OPTION_A_MENU_GOTO()	
	
	IF sFMMCmenu.sMenuGoto[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] != eFmmc_Null_item
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC
PROC REMOVE_TEMP_ENTITES_IF_NOT_NEEDED(INT iCurrentState)

	MODEL_NAMES oldModel
	
	IF iCurrentState != CREATION_TYPE_VEHICLES
		IF DOES_ENTITY_EXIST(sVehStruct.viCoronaVeh)
			oldModel = GET_ENTITY_MODEL(sVehStruct.viCoronaVeh)
			DELETE_VEHICLE(sVehStruct.viCoronaVeh)
			SET_MODEL_AS_NO_LONGER_NEEDED(oldModel)
		ENDIF
	ENDIF
	IF iCurrentState !=CREATION_TYPE_WEAPONS
		IF DOES_ENTITY_EXIST(sWepStruct.viCoronaWep)
			oldModel = GET_ENTITY_MODEL(sWepStruct.viCoronaWep)
			DELETE_OBJECT(sWepStruct.viCoronaWep)					
			SET_MODEL_AS_NO_LONGER_NEEDED(oldModel)
		ENDIF
	ENDIF
	IF iCurrentState != CREATION_TYPE_PROPS
		IF DOES_ENTITY_EXIST(sPropStruct.viCoronaObj)
			oldModel = GET_ENTITY_MODEL(sPropStruct.viCoronaObj)
			DELETE_OBJECT(sPropStruct.viCoronaObj)			
			SET_MODEL_AS_NO_LONGER_NEEDED(oldModel)
		ENDIF
		DELETE_CURRENT_PROP_TEMPLATE_OBJECTS(sPropStruct)
	ENDIF
	IF iCurrentState != CREATION_TYPE_DYNOPROPS
		IF DOES_ENTITY_EXIST(sDynoPropStruct.viCoronaObj)
			oldModel = GET_ENTITY_MODEL(sDynoPropStruct.viCoronaObj)
			DELETE_OBJECT(sDynoPropStruct.viCoronaObj)
			SET_MODEL_AS_NO_LONGER_NEEDED(oldModel)
			
			IF GET_MODEL_NAME_OF_CHILD_PROP_FOR_DYNO(oldModel) != DUMMY_MODEL_FOR_SCRIPT
				IF DOES_ENTITY_EXIST(sDynoPropStruct.oiChildTemp)
					DELETE_OBJECT(sDynoPropStruct.oiChildTemp)
				ENDIF
				SET_MODEL_AS_NO_LONGER_NEEDED(oldModel)
			ENDIF
		ENDIF
	ENDIF
	IF iCurrentState != CREATION_TYPE_PEDS
		IF DOES_ENTITY_EXIST(sPedStruct.piTempPed)
			oldModel = GET_ENTITY_MODEL(sPedStruct.piTempPed)
			DELETE_PED(sPedStruct.piTempPed)
			SET_MODEL_AS_NO_LONGER_NEEDED(oldModel)
			sPedStruct.iSwitchingINT = CREATION_STAGE_WAIT
		ENDIF	
	ENDIF
ENDPROC


PROC CANCEL_ENTITY_CREATION(INT iTypePassed)
	
	IF sFMMCmenu.sMenuBack != eFmmc_MAIN_MENU_BASE
		EXIT
	ENDIF
	
	IF sFMMCMenu.iSelectedEntity = -1
		INT i
		
		IF sCurrentVarsStruct.mnThingToPlaceForBudget != DUMMY_MODEL_FOR_SCRIPT
			REMOVE_MODEL_FROM_CREATOR_BUDGET(sCurrentVarsStruct.mnThingToPlaceForBudget)
			sCurrentVarsStruct.mnThingToPlaceForBudget = DUMMY_MODEL_FOR_SCRIPT
		ENDIF
		
		sWepStruct.iSwitchingINT 	= CREATION_STAGE_WAIT
		sVehStruct.iSwitchingINT	= CREATION_STAGE_WAIT
		sObjStruct.iSwitchingINT	= CREATION_STAGE_WAIT
		SET_CURRENT_GENERIC_CREATION_STAGE(CREATION_STAGE_WAIT)

		iTriggerCreationStage		= CREATION_STAGE_WAIT
		
		IF iTypePassed = CREATION_TYPE_VEHICLES
			FOR i = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles - 1)
				CREATE_FMMC_BLIP(sVehStruct.biVehicleBlip[i], g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].vPos, HUD_COLOUR_BLUEDARK, "SC_BLP_VSP", 1)
			ENDFOR
			UNLOAD_ALL_VEHICLE_MODELS()
		ELIF iTypePassed	=  CREATION_TYPE_WEAPONS
			FLOAT fSize
			FOR i = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons - 1)
				IF IS_SPECIAL_WEAPON_BLIP(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].pt)
					fSize = 1
				ELSE
					fSize = 1
				ENDIF
				CREATE_FMMC_BLIP(sWepStruct.biWeaponBlip[i], GET_ENTITY_COORDS(sWepStruct.oiWeapon[i]), HUD_COLOUR_GREEN, "", fSize)
				SET_BLIP_SPRITE(sWepStruct.biWeaponBlip[i] , GET_CORRECT_BLIP_SPRITE_FMMC(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].pt,g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iSubType))
			ENDFOR
			UNLOAD_ALL_FMMC_WEAPON_ASSETS(TRUE, sWepStruct)
			UNLOAD_ALL_FMMC_WEAPON_ASSETS(FALSE, sWepStruct)
		ELIF iTypePassed	=  CREATION_TYPE_PROPS
			FOR i = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfProps - 1)
				IF DOES_ENTITY_EXIST(sPropStruct.oiObject[i])
					CREATE_FMMC_BLIP(sPropStruct.biObject[i], GET_ENTITY_COORDS(sPropStruct.oiObject[i]), HUD_COLOUR_PURPLE, "FMMC_B_5", 1)
				ENDIF
			ENDFOR
			UNLOAD_ALL_PROP_MODELS(sFMMCmenu)
		ELIF iTypePassed	=  CREATION_TYPE_DYNOPROPS
			FOR i = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps - 1)
				CREATE_FMMC_BLIP(sDynoPropStruct.biObject[i], GET_ENTITY_COORDS(sDynoPropStruct.oiObject[i]), HUD_COLOUR_NET_PLAYER2, "FMMC_B_13", 1)
			ENDFOR
			UNLOAD_ALL_PROP_MODELS(sFMMCmenu)
		ELIF iTypePassed	=  CREATION_TYPE_TRIGGER
			IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.vStartPos)
				CREATE_FMMC_BLIP(sStartEndBlips.biStart, g_FMMC_STRUCT.vStartPos, HUD_COLOUR_WHITE, "FMMC_B_4", 1)	
				BLIP_SPRITE blip = GET_TRIGGER_RADAR_BLIP() 
				IF blip != RADAR_TRACE_INVALID
					SET_BLIP_SPRITE(sStartEndBlips.biStart, blip)
					SET_BLIP_NAME_FROM_TEXT_FILE(sStartEndBlips.biStart, "FMMC_B_4")	
				ENDIF
			ENDIF
		ELIF iTypePassed = CREATION_TYPE_SPAWN_LOCATION
			FOR i = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfSpawnPoints - 1)
				CREATE_FMMC_BLIP(biSpawnBlips[i], g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[i].vPos, HUD_COLOUR_BLUELIGHT, "FMMC_B_3", 1)
			ENDFOR
		ELIF iTypePassed = CREATION_TYPE_TEAM_SPAWN_LOCATION
			FOR i = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[0] - 1)
				CREATE_FMMC_BLIP(sTeamSpawnStruct[0].biPedBlip[i], g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[0][i].vPos, HUD_COLOUR_BLUELIGHT, "FMMC_B_3", 1)
			ENDFOR
		ENDIF	
		
		IF sFMMCmenu.sActiveMenu != eFMMC_GENERIC_OVERRIDE_POSITION
		AND sFMMCmenu.sActiveMenu != eFmmc_GENERIC_OVERRIDE_ROTATION
		AND sFMMCmenu.sActiveMenu != eFmmc_PROP_ADVANCED_OPTIONS
		AND sFMMCmenu.sActiveMenu != eFmmc_SNAPPING_OPTIONS
		AND sFMMCmenu.sActiveMenu != eFMMC_OFFSET_POSITION
		AND sFMMCmenu.sActiveMenu != eFMMC_OFFSET_ROTATION
			PRINTLN("[LH][VANISH] 4")
			CLEAR_BIT(sFMMCmenu.iPropBitset, ciFMMC_PROP_Rotation_Override)
			CLEAR_BIT(sFMMCmenu.iPropBitset, ciFMMC_PROP_Position_Override)
			CLEAR_BIT(sFMMCmenu.iDynoPropBitset, ciFMMC_DYNOPROP_Position_Override)
		ENDIF
		
		IF sFMMCmenu.sActiveMenu != eFMMC_GENERIC_OVERRIDE_POSITION
		AND sFMMCmenu.sActiveMenu != eFmmc_GENERIC_OVERRIDE_ROTATION
		AND sFMMCmenu.sActiveMenu != eFmmc_PROP_ADVANCED_OPTIONS
		AND sFMMCmenu.sActiveMenu != eFmmc_SNAPPING_OPTIONS
		AND sFMMCmenu.sActiveMenu != eFMMC_OFFSET_POSITION
		AND sFMMCmenu.sActiveMenu != eFMMC_OFFSET_ROTATION
			PRINTLN("[JJT] sFMMCmenu.vOverridePosition changed 8")
			SET_OVERRIDE_POSITION_COORDS(sFMMCmenu, <<0,0,0>>)
			SET_OVERRIDE_ROTATION_VECTOR(sFMMCmenu, <<0,0,0>>)
			PRINTLN("[PROP ROTATION](0) Setting override rotation to zero ")
		ENDIF
		
		SET_LONG_BIT(sFMMCMenu.iBitActive,PAN_CAM_PLACE)
		IF DOES_BLIP_EXIST(bCameraTriggerBlip)
			REMOVE_BLIP(bCameraTriggerBlip)
		ENDIF
		
		SET_CREATION_TYPE(sFMMCMenu, CREATION_TYPE_NONE)
		
		sCurrentVarsStruct.fCheckPointSize = ciDEFAULT_CHECK_POINT_SIZE
		sCurrentVarsStruct.bForceCoronaSizeCheck = FALSE
		sHCS.hcCurrentCoronaColour = sHCS.hcDefaultCoronaColour	
		REMOVE_TEMP_ENTITES_IF_NOT_NEEDED(sFMMCmenu.iEntityCreation)
	ELSE 
	
		PLAY_SOUND_FRONTEND(-1, "EDIT", GET_CREATOR_SPECIFIC_SOUND_SET())
			
		IF iTypePassed	=  CREATION_TYPE_VEHICLES		
			PRINTLN("IF iTypePassed	= CREATION_TYPE_VEHICLES")
			PRINTLN("sFMMCMenu.iSelectedEntity = ", sFMMCMenu.iSelectedEntity)	
			CANCEL_FROM_EDITING_VEHICLE(sVehStruct, sFMMCMenu.iSelectedEntity)
			sVehStruct.iSwitchingINT 				= CREATION_STAGE_WAIT
		ELIF iTypePassed	=  CREATION_TYPE_WEAPONS
			PRINTLN("IF iTypePassed	= CREATION_TYPE_WEAPONS")
			PRINTLN("sFMMCMenu.iSelectedEntity = ", sFMMCMenu.iSelectedEntity)	
			CANCEL_FROM_EDITING_WEAPON(sFMMCmenu, sWepStruct, sFMMCMenu.iSelectedEntity, g_FMMC_STRUCT.iVehicleDeathmatch = 0, sCurrentVarsStruct)
			sWepStruct.iSwitchingINT 				= CREATION_STAGE_WAIT
		ELIF iTypePassed	=  CREATION_TYPE_PROPS
			PRINTLN("IF iTypePassed	= CREATION_TYPE_PROPS")
			PRINTLN("sFMMCMenu.iSelectedEntity = ", sFMMCMenu.iSelectedEntity)	
			CANCEL_FROM_EDITING_PROP(sPropStruct, sFMMCMenu.iSelectedEntity)
			SET_CURRENT_GENERIC_CREATION_STAGE(CREATION_STAGE_WAIT)
		ELIF iTypePassed	=  CREATION_TYPE_DYNOPROPS
			PRINTLN("IF iTypePassed	= CREATION_TYPE_DYNOPROPS")
			PRINTLN("sFMMCMenu.iSelectedEntity = ", sFMMCMenu.iSelectedEntity)	
			CANCEL_FROM_EDITING_DYNOPROP(sPropStruct, sDynoPropStruct, sFMMCMenu.iSelectedEntity)
			SET_CURRENT_GENERIC_CREATION_STAGE(CREATION_STAGE_WAIT)
		ELIF iTypePassed	= CREATION_TYPE_SPAWN_LOCATION
			PRINTLN("TypePassed	= CREATION_TYPE_SPAWN_LOCATION")
			PRINTLN("sFMMCMenu.iSelectedEntity = ", sFMMCMenu.iSelectedEntity)	
			g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[sFMMCMenu.iSelectedEntity] = g_FMMC_STRUCT_ENTITIES.sEditedSpawnPoint
			
			PRINTLN("g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[sFMMCMenu.iSelectedEntity].iTeam  = ", g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[sFMMCMenu.iSelectedEntity].iTeam )
				
			CREATE_FMMC_BLIP(biSpawnBlips[sFMMCMenu.iSelectedEntity], g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[sFMMCMenu.iSelectedEntity].vPos, HUD_COLOUR_BLUELIGHT, "FMMC_B_3", 1)
					
			IF sCurrentVarsStruct.bCanCreateADecalThisFrame
				CREATE_SPAWN_POINT_DECAL(sPedStruct, sFMMCMenu.iSelectedEntity, FALSE, sCurrentVarsStruct.bCanCreateADecalThisFrame)
			ENDIF
			
			sPedStruct.iSwitchingINT 				= CREATION_STAGE_WAIT
		ELIF iTypePassed	= CREATION_TYPE_TEAM_SPAWN_LOCATION
			PRINTLN("TypePassed	= CREATION_TYPE_TEAM_SPAWN_LOCATION")
			PRINTLN("sFMMCMenu.iSelectedEntity = ", sFMMCMenu.iSelectedEntity)	
			g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[g_FMMC_STRUCT_ENTITIES.sEditedSpawnPoint.iTeam][sFMMCMenu.iSelectedEntity] = g_FMMC_STRUCT_ENTITIES.sEditedSpawnPoint
			
			PRINTLN("g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[sFMMCMenu.iSelectedEntity].iTeam  = ", g_FMMC_STRUCT_ENTITIES.sEditedSpawnPoint.iTeam)
			
			CREATE_FMMC_BLIP(sTeamSpawnStruct[g_FMMC_STRUCT_ENTITIES.sEditedSpawnPoint.iTeam].biPedBlip[sFMMCMenu.iSelectedEntity], g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[g_FMMC_STRUCT_ENTITIES.sEditedSpawnPoint.iTeam][sFMMCMenu.iSelectedEntity].vPos, HUD_COLOUR_WHITE, getNameForTeamSpawnBlip(g_FMMC_STRUCT_ENTITIES.sEditedSpawnPoint.iTeam), 1)
			SET_BLIP_COLOUR(sTeamSpawnStruct[g_FMMC_STRUCT_ENTITIES.sEditedSpawnPoint.iTeam].biPedBlip[sFMMCMenu.iSelectedEntity] , getColourForSpawnBlip(g_FMMC_STRUCT_ENTITIES.sEditedSpawnPoint.iTeam))
			
			IF sCurrentVarsStruct.bCanCreateADecalThisFrame
				CREATE_TEAM_SPAWN_POINT_DECAL(sTeamSpawnStruct[g_FMMC_STRUCT_ENTITIES.sEditedSpawnPoint.iTeam], sFMMCMenu.iSelectedEntity, FALSE, sCurrentVarsStruct.bCanCreateADecalThisFrame, g_FMMC_STRUCT_ENTITIES.sEditedSpawnPoint.iTeam)
			ENDIF
			
			sTeamSpawnStruct[g_FMMC_STRUCT_ENTITIES.sEditedSpawnPoint.iTeam].iSwitchingINT = CREATION_STAGE_WAIT
		ELIF iTypePassed = CREATION_TYPE_PEDS
		
			PRINTLN("TypePassed	= CREATION_TYPE_PEDS")
			PRINTLN("sFMMCMenu.iSelectedEntity = ", sFMMCMenu.iSelectedEntity)	
			IF DOES_ENTITY_EXIST(sPedStruct.piPed[sFMMCMenu.iSelectedEntity])	
				DELETE_PED(sPedStruct.piPed[sFMMCMenu.iSelectedEntity])
			ENDIF
			IF DOES_ENTITY_EXIST(sPedStruct.piTempPed)
				DELETE_PED(sPedStruct.piTempPed)
			ENDIF
			g_FMMC_STRUCT_ENTITIES.sPlacedPed[sFMMCMenu.iSelectedEntity] = g_FMMC_STRUCT_ENTITIES.sEditedPed	
			
			REQUEST_MODEL(g_FMMC_STRUCT_ENTITIES.sPlacedPed[sFMMCMenu.iSelectedEntity].mn)
			IF CREATE_PED_FMMC(sPedStruct.piPed[sFMMCMenu.iSelectedEntity], sFMMCMenu.iSelectedEntity, FALSE, FALSE)
				FMMC_SET_PED_VARIATION(sPedStruct.piPed[sFMMCMenu.iSelectedEntity], g_FMMC_STRUCT_ENTITIES.sPlacedPed[sFMMCMenu.iSelectedEntity].mn, g_FMMC_STRUCT_ENTITIES.sPlacedPed[sFMMCMenu.iSelectedEntity].iModelVariation)
				SET_ENTITY_COLLISION(sPedStruct.piPed[sFMMCMenu.iSelectedEntity] , TRUE)					
				CREATE_FMMC_BLIP(sPedStruct.biPedBlip[sFMMCMenu.iSelectedEntity], GET_ENTITY_COORDS(sPedStruct.piPed[sFMMCMenu.iSelectedEntity]), HUD_COLOUR_RED, "SC_BLP_ESP", 1)
				sPedStruct.iSwitchingINT = CREATION_STAGE_WAIT
			ENDIF
			IF g_FMMC_STRUCT_ENTITIES.iNumberOfPeds >= FMMC_MAX_PEDS
				IF DOES_ENTITY_EXIST(sPedStruct.piTempPed)
					DELETE_PED(sPedStruct.piTempPed)
				ENDIF
			ENDIF
			
			REFRESH_MENU(sFMMCmenu)
			
			SET_MODEL_AS_NO_LONGER_NEEDED(g_FMMC_STRUCT_ENTITIES.sEditedPed.mn)
		ENDIF	
		
	ENDIF

ENDPROC

FUNC BOOL DEAL_WITH_SKIPPING_EMPTY_OPTIONS_SURVIVAL(INT iDirection)
	
	IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) > sFMMCmenu.iCurrentMenuLength-1
		SET_CREATOR_MENU_SELECTION(sFMMCmenu, 0)
	ENDIF
	IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) < 0
		SET_CREATOR_MENU_SELECTION(sFMMCmenu, sFMMCmenu.iCurrentMenuLength - 1)
	ENDIF

	IF sFMMCmenu.sActiveMenu = eFmmc_SURVIVAL_WARNING_AREA
	
		
		
		IF g_FMMC_STRUCT.sSurvivalWaveData.iBoundsType = SURV_BOUNDS_NON_AXIS_AREA
		
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_SURV_BOUNDS_PLACE
			AND NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sSurvivalWaveData.vBounds[1])
				PRINTLN("DEAL_WITH_SKIPPING_EMPTY_OPTIONS_SURVIVAL - Skipping eFmmc_SURVIVAL_WARNING_AREA")
				SET_CREATOR_MENU_SELECTION(sFMMCmenu, GET_CREATOR_MENU_SELECTION(sFMMCmenu) + iDirection)
				RETURN TRUE
			ENDIF
		
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_SURV_BOUNDS_EDIT_FRONT
			AND IS_VECTOR_ZERO(g_FMMC_STRUCT.sSurvivalWaveData.vBounds[1])
				PRINTLN("DEAL_WITH_SKIPPING_EMPTY_OPTIONS_SURVIVAL - Skipping OPTION_SURV_BOUNDS_EDIT_FRONT")
				SET_CREATOR_MENU_SELECTION(sFMMCmenu, GET_CREATOR_MENU_SELECTION(sFMMCmenu) + iDirection)
				RETURN TRUE
			ENDIF
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_SURV_BOUNDS_EDIT_BACK
			AND IS_VECTOR_ZERO(g_FMMC_STRUCT.sSurvivalWaveData.vBounds[1])
				PRINTLN("DEAL_WITH_SKIPPING_EMPTY_OPTIONS_SURVIVAL - Skipping OPTION_SURV_BOUNDS_EDIT_BACK")
				SET_CREATOR_MENU_SELECTION(sFMMCmenu, GET_CREATOR_MENU_SELECTION(sFMMCmenu) + iDirection)
				RETURN TRUE
			ENDIF
		ELSE
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_SURV_BOUNDS_PLACE
			AND NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sSurvivalWaveData.vBounds[0])
				PRINTLN("DEAL_WITH_SKIPPING_EMPTY_OPTIONS_SURVIVAL - Skipping eFmmc_SURVIVAL_WARNING_AREA")
				SET_CREATOR_MENU_SELECTION(sFMMCmenu, GET_CREATOR_MENU_SELECTION(sFMMCmenu) + iDirection)
				RETURN TRUE
			ENDIF
			
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_SURV_BOUNDS_EDIT_FRONT
			AND IS_VECTOR_ZERO(g_FMMC_STRUCT.sSurvivalWaveData.vBounds[0])
				PRINTLN("DEAL_WITH_SKIPPING_EMPTY_OPTIONS_SURVIVAL - Skipping OPTION_SURV_BOUNDS_EDIT_FRONT")
				SET_CREATOR_MENU_SELECTION(sFMMCmenu, GET_CREATOR_MENU_SELECTION(sFMMCmenu) + iDirection)
				RETURN TRUE
			ENDIF
			
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_SURV_BOUNDS_EDIT_BACK
				PRINTLN("DEAL_WITH_SKIPPING_EMPTY_OPTIONS_SURVIVAL - Skipping OPTION_SURV_BOUNDS_EDIT_BACK")
				SET_CREATOR_MENU_SELECTION(sFMMCmenu, GET_CREATOR_MENU_SELECTION(sFMMCmenu) + iDirection)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF SHOULD_CURRENT_SELECTION_BE_SKIPPED(sFMMCmenu)
		SKIP_EMPTY_OPTION(sFMMCmenu, iDirection)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC enumFMMC_CIRCLE_MENU_ACTIONS RETURN_CIRCLE_MENU_SELECTIONS()
	IF LOAD_MENU_ASSETS()
		DRAW_MENU_SELECTION()
	ENDIF
	
	HANDLE_EVERY_FRAME_MENU_FUNCTIONALITY()
	
	BOOL bSubmenuActive = NOT IS_MENU_A_RESET_MENU(sFMMCmenu.sMenuBack)
	BOOL bAllOn, bAllOff
	
	IF IS_SCREEN_FADED_IN()
		IF FFMC_IS_ACCEPT_JUST_PRESSED(sFMMCmenu) OR (FMMC_IS_CIRCLE_MENU_A_CAMERA_MENU(sFMMCmenu.sActiveMenu) AND FMMC_IS_MAP_ACCEPT_JUST_PRESSED(sFMMCmenu))
			IF IS_THIS_OPTION_SELECTABLE()
				IF IS_THIS_OPTION_A_MENU_ACTION()
				OR IS_THIS_OPTION_A_MENU_GOTO() 
					IF (sFMMCmenu.sActiveMenu != eFmmc_PAN_CAM_BASE OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 1)
					AND sFMMCmenu.sActiveMenu != eFmmc_PHOTO_CAM_BASE
						PLAY_SOUND_FRONTEND(-1, "SELECT", GET_SOUND_SET_FROM_CREATOR_TYPE())
						PRINTSTRING("CONFIRM SOUND EFFECT!!!")PRINTNL()
					ENDIF
				ENDIF
				
				SWITCH sFMMCmenu.sActiveMenu
					CASE eFmmc_MAIN_OPTIONS_BASE
						IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_NAME
							sCurrentVarsStruct.iMenuState = MENU_STATE_TITLE
							SET_NOTIFY_LOCALISATION()
						ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_DESCRIPTION
							sCurrentVarsStruct.iMenuState = MENU_STATE_DESCRIPTION
							SET_NOTIFY_LOCALISATION()
						ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_MISSION_TAGS
						AND g_FMMC_STRUCT.iMissionType != FMMC_TYPE_SURVIVAL
						AND g_FMMC_STRUCT.iMissionType != FMMC_TYPE_GANGHIDEOUT
							sCurrentVarsStruct.iMenuState = MENU_STATE_TAGS
							SET_NOTIFY_LOCALISATION()
						ENDIF
					BREAK
					CASE eFmmc_SURVIVAL_Squad_Config
						IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_CONFIG_SQUAD_MODEL
							sFMMCmenu.iPedLibrary = GET_PED_LIBRARY_FROM_MODEL(g_FMMC_STRUCT.sSurvivalWaveData.mnPedModel[sFMMCmenu.iSurvivalWaveSelection][sFMMCmenu.iSurvivalDiffSelection], sFMMCMenu)
							sFMMCmenu.iPedType = GET_PED_TYPE_FROM_MODEL(g_FMMC_STRUCT.sSurvivalWaveData.mnPedModel[sFMMCmenu.iSurvivalWaveSelection][sFMMCmenu.iSurvivalDiffSelection], sFMMCMenu, sFMMCmenu.iPedLibrary)
						ENDIF
					BREAK
					CASE eFmmc_SURVIVAL_Veh_Config
						IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_CONFIG_VEH_SELECT
							IF g_FMMC_STRUCT.sSurvivalWaveData.mnVeh[sFMMCmenu.iSurvivalWaveSelection][sFMMCmenu.iSurvivalVehicleSelection] = DUMMY_MODEL_FOR_SCRIPT
								sFMMCmenu.iVehicleLibrary = GET_CREATOR_VEHICLE_LIBRARY_FROM_MODEL(SANDKING)
								sFMMCmenu.iVehicleType = GET_CREATOR_VEHICLE_TYPE_FROM_MODEL(SANDKING)
								g_FMMC_STRUCT.sSurvivalWaveData.mnVeh[sFMMCmenu.iSurvivalWaveSelection][sFMMCmenu.iSurvivalVehicleSelection] = SANDKING
							ELSE
								sFMMCmenu.iVehicleLibrary = GET_CREATOR_VEHICLE_LIBRARY_FROM_MODEL(g_FMMC_STRUCT.sSurvivalWaveData.mnVeh[sFMMCmenu.iSurvivalWaveSelection][sFMMCmenu.iSurvivalVehicleSelection])
								sFMMCmenu.iVehicleType = GET_CREATOR_VEHICLE_TYPE_FROM_MODEL(g_FMMC_STRUCT.sSurvivalWaveData.mnVeh[sFMMCmenu.iSurvivalWaveSelection][sFMMCmenu.iSurvivalVehicleSelection], sFMMCmenu.iVehicleLibrary)
							ENDIF
						ENDIF
					BREAK
					CASE eFmmc_SURVIVAL_Air_Config
						IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_CONFIG_AIR_SELECT
							IF g_FMMC_STRUCT.sSurvivalWaveData.mnAir[sFMMCmenu.iSurvivalWaveSelection][sFMMCmenu.iSurvivalAirVehSelection] = DUMMY_MODEL_FOR_SCRIPT
								sFMMCmenu.iVehicleLibrary = GET_CREATOR_VEHICLE_LIBRARY_FROM_MODEL(BUZZARD)
								sFMMCmenu.iVehicleType = GET_CREATOR_VEHICLE_TYPE_FROM_MODEL(BUZZARD)
								g_FMMC_STRUCT.sSurvivalWaveData.mnAir[sFMMCmenu.iSurvivalWaveSelection][sFMMCmenu.iSurvivalAirVehSelection] = BUZZARD
							ELSE
								sFMMCmenu.iVehicleLibrary = GET_CREATOR_VEHICLE_LIBRARY_FROM_MODEL(g_FMMC_STRUCT.sSurvivalWaveData.mnAir[sFMMCmenu.iSurvivalWaveSelection][sFMMCmenu.iSurvivalAirVehSelection])
								sFMMCmenu.iVehicleType = GET_CREATOR_VEHICLE_TYPE_FROM_MODEL(g_FMMC_STRUCT.sSurvivalWaveData.mnAir[sFMMCmenu.iSurvivalWaveSelection][sFMMCmenu.iSurvivalAirVehSelection], sFMMCmenu.iVehicleLibrary)
							ENDIF
						ENDIF
					BREAK
					CASE eFmmc_SURVIVAL_Wave_Config
						IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_WAVE_BASE_MODEL
							sFMMCmenu.iPedLibrary	= GET_PED_LIBRARY_FROM_MODEL(g_FMMC_STRUCT.sSurvivalWaveData.mnPedModel[sFMMCmenu.iSurvivalWaveSelection][sFMMCmenu.iSurvivalDiffSelection], sFMMCMenu)
							sFMMCmenu.iPedType		= GET_PED_TYPE_FROM_MODEL(g_FMMC_STRUCT.sSurvivalWaveData.mnPedModel[sFMMCmenu.iSurvivalWaveSelection][sFMMCmenu.iSurvivalDiffSelection], sFMMCMenu, sFMMCmenu.iPedLibrary)
						ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_WAVE_BASE_HEAVY_UNIT
							IF NOT IS_BIT_SET(g_FMMC_STRUCT.sSurvivalWaveData.iHeavyChangeBitset, sFMMCmenu.iSurvivalWaveSelection)
								EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.sSurvivalWaveData.iHeavyOnWave, sFMMCmenu.iSurvivalWaveSelection)
								IF IS_BIT_SET(g_FMMC_STRUCT.sSurvivalWaveData.iHeavyOnWave, sFMMCmenu.iSurvivalWaveSelection)
									SET_NUMBER_OF_HEAVIES_FOR_LAST_STAGE(sFMMCmenu.iSurvivalWaveSelection)
								ELSE
									SET_NUMBER_OF_HEAVIES_FOR_LAST_STAGE(sFMMCmenu.iSurvivalWaveSelection, TRUE)
								ENDIF
								REFRESH_MENU_ASAP(sFMMCMenu)
							ENDIF
						ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_WAVE_BASE_HEAVY_MOD
							sFMMCmenu.iPedLibrary	= GET_PED_LIBRARY_FROM_MODEL(g_FMMC_STRUCT.sSurvivalWaveData.mnHeavyModel[sFMMCmenu.iSurvivalWaveSelection][sFMMCmenu.iSurvivalDiffSelection], sFMMCMenu)
							sFMMCmenu.iPedType		= GET_PED_TYPE_FROM_MODEL(g_FMMC_STRUCT.sSurvivalWaveData.mnHeavyModel[sFMMCmenu.iSurvivalWaveSelection][sFMMCmenu.iSurvivalDiffSelection], sFMMCMenu, sFMMCmenu.iPedLibrary)
						ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_WAVE_BASE_LAND_VEH
							IF NOT IS_BIT_SET(g_FMMC_STRUCT.sSurvivalWaveData.iLandVehChangeBitset, sFMMCmenu.iSurvivalWaveSelection)
								EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.sSurvivalWaveData.iLandVehiclesOnWave, sFMMCmenu.iSurvivalWaveSelection)
								IF IS_BIT_SET(g_FMMC_STRUCT.sSurvivalWaveData.iLandVehiclesOnWave, sFMMCmenu.iSurvivalWaveSelection)
									g_FMMC_STRUCT.sSurvivalWaveData.iNumVehicles[sFMMCmenu.iSurvivalWaveSelection] = 2
								ELSE
									g_FMMC_STRUCT.sSurvivalWaveData.iNumVehicles[sFMMCmenu.iSurvivalWaveSelection] = 0
								ENDIF
								REFRESH_MENU_ASAP(sFMMCMenu)
							ENDIF
						ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_WAVE_BASE_LAND_MOD
							sFMMCmenu.iVehicleLibrary = GET_CREATOR_VEHICLE_LIBRARY_FROM_MODEL(g_FMMC_STRUCT.sSurvivalWaveData.mnVeh[sFMMCmenu.iSurvivalWaveSelection][0])
							sFMMCmenu.iVehicleType = GET_CREATOR_VEHICLE_TYPE_FROM_MODEL(g_FMMC_STRUCT.sSurvivalWaveData.mnVeh[sFMMCmenu.iSurvivalWaveSelection][0], sFMMCmenu.iVehicleLibrary)
						ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_WAVE_BASE_AIR_VEH
							IF NOT IS_BIT_SET(g_FMMC_STRUCT.sSurvivalWaveData.iAirVehChangeBitset, sFMMCmenu.iSurvivalWaveSelection)
								EDIT_CREATOR_BITSET_MENU_ITEM(sFMMCendStage, g_FMMC_STRUCT.sSurvivalWaveData.iAirVehiclesOnWave, sFMMCmenu.iSurvivalWaveSelection)
								IF IS_BIT_SET(g_FMMC_STRUCT.sSurvivalWaveData.iAirVehiclesOnWave, sFMMCmenu.iSurvivalWaveSelection)
									g_FMMC_STRUCT.sSurvivalWaveData.iNumberOfHelis[sFMMCmenu.iSurvivalWaveSelection] = 1
								ELSE
									g_FMMC_STRUCT.sSurvivalWaveData.iNumberOfHelis[sFMMCmenu.iSurvivalWaveSelection] = 0
								ENDIF
								REFRESH_MENU_ASAP(sFMMCMenu)
							ENDIF
						ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_WAVE_BASE_AIR_MOD
							sFMMCmenu.iVehicleLibrary = GET_CREATOR_VEHICLE_LIBRARY_FROM_MODEL(g_FMMC_STRUCT.sSurvivalWaveData.mnAir[sFMMCmenu.iSurvivalWaveSelection][0])
							sFMMCmenu.iVehicleType = GET_CREATOR_VEHICLE_TYPE_FROM_MODEL(g_FMMC_STRUCT.sSurvivalWaveData.mnAir[sFMMCmenu.iSurvivalWaveSelection][0], sFMMCmenu.iVehicleLibrary)
						ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_WAVE_BASE_RESET
							TRIGGER_CONFIRMATION_SCREEN(sCurrentVarsStruct, "SC_RESET_W", "")
						ENDIF
					BREAK
					CASE eFmmc_SURVIVAL_WARNING_AREA
						IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_SURV_BOUNDS_CLEAR
						AND IS_THIS_OPTION_SELECTABLE()
							TRIGGER_CONFIRMATION_SCREEN(sCurrentVarsStruct, "SC_RESET_PA", "")
						ENDIF
					BREAK
					CASE eFmmc_SURVIVAL_Squad_Enemy_Options
						bAllOn = CHECK_HEAVY_UNITS_FOR_ALL_WAVES()
						bAllOff = CHECK_HEAVY_UNITS_FOR_ALL_WAVES(TRUE)
						IF GET_CREATOR_MENU_SELECTION(sFMMCMenu) = OPTION_SURVIVAL_SENE_H_EAW
							IF !bAllOn
								TRIGGER_CONFIRMATION_SCREEN(sCurrentVarsStruct, "SC_AYS", "SC_HVY_AYS_EAW")
							ENDIF
						ELIF GET_CREATOR_MENU_SELECTION(sFMMCMenu) = OPTION_SURVIVAL_SENE_H_RST
							IF !bAllOff
								TRIGGER_CONFIRMATION_SCREEN(sCurrentVarsStruct, "SC_AYS", "SC_HVY_AYS_RST")
							ENDIF
						ENDIF
					BREAK
					CASE eFmmc_SURVIVAL_Land_Vehicle_Options
						bAllOn = CHECK_LAND_VEHICLES_FOR_ALL_WAVES()
						bAllOff = CHECK_LAND_VEHICLES_FOR_ALL_WAVES(TRUE)
						IF GET_CREATOR_MENU_SELECTION(sFMMCMenu) = OPTION_SURVIVAL_LVEH_ELV
							IF !bAllOn
								TRIGGER_CONFIRMATION_SCREEN(sCurrentVarsStruct, "SC_AYS", "SC_VO_AYS_EALV")
							ENDIF
						ELIF GET_CREATOR_MENU_SELECTION(sFMMCMenu) = OPTION_SURVIVAL_LVEH_DLV
							IF !bAllOff
								TRIGGER_CONFIRMATION_SCREEN(sCurrentVarsStruct, "SC_AYS", "SC_VO_AYS_DALV")
							ENDIF
						ELIF GET_CREATOR_MENU_SELECTION(sFMMCMenu) = OPTION_SURVIVAL_LVEH_RLV
							IF g_FMMC_STRUCT.sSurvivalWaveData.iLandVehiclesOnWave != ciSURVIVAL_DEFAULT_VEH_BITSET
								TRIGGER_CONFIRMATION_SCREEN(sCurrentVarsStruct, "SC_AYS", "SC_VO_AYS_RLV")
							ENDIF
						ENDIF
					BREAK
					CASE eFmmc_SURVIVAL_Air_Vehicle_Options
						bAllOn = CHECK_AIR_VEHICLES_FOR_ALL_WAVES()
						bAllOff = CHECK_AIR_VEHICLES_FOR_ALL_WAVES(TRUE)
						IF GET_CREATOR_MENU_SELECTION(sFMMCMenu) = OPTION_SURVIVAL_AVEH_EAV
							IF !bAllOn
								TRIGGER_CONFIRMATION_SCREEN(sCurrentVarsStruct, "SC_AYS", "SC_VO_AYS_EAAV")
							ENDIF
						ELIF GET_CREATOR_MENU_SELECTION(sFMMCMenu) = OPTION_SURVIVAL_AVEH_DAV
							IF !bAllOff
								TRIGGER_CONFIRMATION_SCREEN(sCurrentVarsStruct, "SC_AYS", "SC_VO_AYS_DAAV")
							ENDIF
						ELIF GET_CREATOR_MENU_SELECTION(sFMMCMenu) = OPTION_SURVIVAL_AVEH_RAV
							IF g_FMMC_STRUCT.sSurvivalWaveData.iAirVehiclesOnWave != ciSURVIVAL_DEFAULT_AIR_BITSET
								TRIGGER_CONFIRMATION_SCREEN(sCurrentVarsStruct, "SC_AYS", "SC_VO_AYS_RAV")
							ENDIF
						ENDIF
					BREAK
					CASE eFmmc_SURVIVAL_Defaults_Menu
						IF GET_CREATOR_MENU_SELECTION(sFMMCMenu) = OPTION_SURVIVAL_DEFAULT_RESET
							TRIGGER_CONFIRMATION_SCREEN(sCurrentVarsStruct, "SC_AYS", "SC_RESET_RSURV")
						ENDIF
					BREAK
					CASE eFmmc_SURVIVAL_Copy_Wave
						IF GET_CREATOR_MENU_SELECTION(sFMMCMenu) = OPTION_SURV_COPY_TO_WAVE
							TRIGGER_CONFIRMATION_SCREEN(sCurrentVarsStruct, "SC_AYS", "SC_COPY_WARN_W")
						ELIF GET_CREATOR_MENU_SELECTION(sFMMCMenu) = OPTION_SURV_COPY_TO_ALL
							TRIGGER_CONFIRMATION_SCREEN(sCurrentVarsStruct, "SC_AYS", "SC_COPY_WARN_A")
						ENDIF
					BREAK
				ENDSWITCH
				
				IF sFMMCmenu.sMenuGoto[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] != eFmmc_Null_item
					PRINTSTRING("Set up menu")PRINTNL()
					//If it is then reset up the menu
					INT iWaveToUse = sFMMCmenu.iSurvivalWaveSelection
					IF iWaveToUse = -1
						iWaveToUse = 0
					ENDIF
					INT iSubstage = sFMMCmenu.iSurvivalDiffSelection
					IF iSubstage < 0
						iSubstage = 0
					ENDIF
					INT iVehToUse = sFMMCmenu.iSurvivalVehicleSelection
					INT iAirToUse = sFMMCmenu.iSurvivalAirVehSelection
					
					IF sFMMCmenu.sMenuGoto[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] = eFmmc_SURVIVAL_Wave_Ped_Model
						sFMMCmenu.sMenuBack = sFMMCmenu.sActiveMenu
						IF g_FMMC_STRUCT.sSurvivalWaveData.mnPedModel[iWaveToUse][iSubstage] = DUMMY_MODEL_FOR_SCRIPT
							g_FMMC_STRUCT.sSurvivalWaveData.mnPedModel[iWaveToUse][iSubstage] = GET_SURVIVAL_MODEL_DATA_DEFAULT(ciSURVIVAL_DATA_PED_MODEL, iWaveToUse, 0)
						ENDIF
						sFMMCmenu.iPedLibrary	= GET_PED_LIBRARY_FROM_MODEL(g_FMMC_STRUCT.sSurvivalWaveData.mnPedModel[iWaveToUse][iSubstage], sFMMCMenu)
						sFMMCmenu.iPedType		= GET_PED_TYPE_FROM_MODEL(g_FMMC_STRUCT.sSurvivalWaveData.mnPedModel[iWaveToUse][iSubstage], sFMMCMenu, sFMMCmenu.iPedLibrary)
					
					ELIF sFMMCmenu.sMenuGoto[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] = eFmmc_SURVIVAL_Heavy_Model
						sFMMCmenu.sMenuBack = sFMMCmenu.sActiveMenu
						IF g_FMMC_STRUCT.sSurvivalWaveData.mnHeavyModel[iWaveToUse][iSubstage] = DUMMY_MODEL_FOR_SCRIPT
							g_FMMC_STRUCT.sSurvivalWaveData.mnHeavyModel[iWaveToUse][iSubstage] = GET_SURVIVAL_MODEL_DATA_DEFAULT(ciSURVIVAL_DATA_HEAVY_MODEL, iWaveToUse, 0)
						ENDIF
						sFMMCmenu.iPedLibrary	= GET_PED_LIBRARY_FROM_MODEL(g_FMMC_STRUCT.sSurvivalWaveData.mnHeavyModel[iWaveToUse][iSubstage], sFMMCMenu)
						sFMMCmenu.iPedType		= GET_PED_TYPE_FROM_MODEL(g_FMMC_STRUCT.sSurvivalWaveData.mnHeavyModel[iWaveToUse][iSubstage], sFMMCMenu, sFMMCmenu.iPedLibrary)
					
					ELIF sFMMCmenu.sMenuGoto[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] = eFmmc_SURVIVAL_Veh_Wave_Model
						sFMMCmenu.sMenuBack = sFMMCmenu.sActiveMenu
						IF sFMMCmenu.sActiveMenu = eFmmc_SURVIVAL_Defaults_Menu
							iVehToUse = 0
						ENDIF
						sFMMCmenu.iVehicleLibrary = GET_CREATOR_VEHICLE_LIBRARY_FROM_MODEL(g_FMMC_STRUCT.sSurvivalWaveData.mnVeh[iWaveToUse][iVehToUse])
						sFMMCmenu.iVehicleType = GET_CREATOR_VEHICLE_TYPE_FROM_MODEL(g_FMMC_STRUCT.sSurvivalWaveData.mnVeh[iWaveToUse][iVehToUse], sFMMCmenu.iVehicleLibrary)
					
					ELIF sFMMCmenu.sMenuGoto[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] = eFmmc_SURVIVAL_Air_Wave_Model
						sFMMCmenu.sMenuBack = sFMMCmenu.sActiveMenu
						IF sFMMCmenu.sActiveMenu = eFmmc_SURVIVAL_Defaults_Menu
							iAirToUse = 0
						ENDIF
						sFMMCmenu.iVehicleLibrary = GET_CREATOR_VEHICLE_LIBRARY_FROM_MODEL(g_FMMC_STRUCT.sSurvivalWaveData.mnAir[iWaveToUse][iAirToUse])
						sFMMCmenu.iVehicleType = GET_CREATOR_VEHICLE_TYPE_FROM_MODEL(g_FMMC_STRUCT.sSurvivalWaveData.mnAir[iWaveToUse][iAirToUse], sFMMCmenu.iVehicleLibrary)
					
					ELIF sFMMCmenu.sMenuGoto[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] = eFmmc_SURVIVAL_Squad_Weapons
						sFMMCmenu.iSurvivalWeaponSelection = 0
					
					ELIF (sFMMCmenu.sMenuGoto[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] = eFmmc_SURVIVAL_Loadout_Weapons
					OR sFMMCmenu.sMenuGoto[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] = eFmmc_SURVIVAL_Loadout_Weapon_Select)
						INT iWeaponToUse = GET_CREATOR_MENU_SELECTION(sFMMCmenu)-1
						IF sFMMCmenu.sMenuGoto[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] = eFmmc_SURVIVAL_Loadout_Weapons
							iWeaponToUse = 0
						ENDIF
						sFMMCmenu.iWepLibrary = g_FMMC_STRUCT.sSurvivalWaveData.iLoadoutWeaponCat[sFMMCmenu.iSurvivalWaveSelection][iWeaponToUse]
						sWepStruct.iSelectedWeaponType = g_FMMC_STRUCT.sSurvivalWaveData.iLoadoutWeaponType[sFMMCmenu.iSurvivalWaveSelection][iWeaponToUse]
						sFMMCmenu.iCurrentEntitySelection[CREATION_TYPE_WEAPONS] = sWepStruct.iSelectedWeaponType
						sFMMCmenu.iCurrentEntitySelection[CREATION_TYPE_WEAPON_LIBRARY] = sFMMCmenu.iWepLibrary
						PRINTLN("Setting iWepLibrary to ", sFMMCmenu.iWepLibrary, " / Setting iSelectedWeaponType to ", sWepStruct.iSelectedWeaponType, " || iWeaponToUse: ", iWeaponToUse, " | Wave: ", sFMMCmenu.iSurvivalWaveSelection)
						sFMMCmenu.sSubTypeName[CREATION_TYPE_WEAPONS] = GET_WEAPON_NAME_FROM_WEAPON_TYPE(sWepStruct.wtGunType[sWepStruct.iSelectedWeaponType])
						
						IF sFMMCmenu.sMenuGoto[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] = eFmmc_SURVIVAL_Loadout_Weapons
							sFMMCmenu.eSurvivalLoadoutMenuBack = sFMMCmenu.sActiveMenu
							PRINTLN("Setting eSurvivalLoadoutMenuBack to ", sFMMCmenu.eSurvivalLoadoutMenuBack)
						ENDIF
						
					ELIF sFMMCmenu.sMenuGoto[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] = eFmmc_SURVIVAL_Pickup_Placement_Choose_Weapon
					
						PICKUP_TYPE ptPickup = g_FMMC_STRUCT_ENTITIES.sEditedWeapon.ptSurvival_WeaponOverridePerWave[sFMMCmenu.iSurvivalWaveSelection]
						sWepStruct.iSelectedWeaponType = GET_WEAPON_SELECTION_FROM_PICK_UP_TYPE(sWepStruct, ptPickup, TRUE, g_FMMC_STRUCT_ENTITIES.sEditedWeapon.iSubType) 
						sFMMCmenu.iWepLibrary = GET_SELECTED_WEAPON_LIBRARY(sWepStruct.iSelectedWeaponType, TRUE)
						
						sFMMCmenu.iCurrentEntitySelection[CREATION_TYPE_WEAPONS] = sWepStruct.iSelectedWeaponType
						sFMMCmenu.iCurrentEntitySelection[CREATION_TYPE_WEAPON_LIBRARY] = sFMMCmenu.iWepLibrary
						PRINTLN("Setting iWepLibrary to ", sFMMCmenu.iWepLibrary, " / Setting iSelectedWeaponType to ", sWepStruct.iSelectedWeaponType, " || Selection: ", GET_CREATOR_MENU_SELECTION(sFMMCmenu), " | Wave: ", sFMMCmenu.iSurvivalWaveSelection)
						sFMMCmenu.sSubTypeName[CREATION_TYPE_WEAPONS] = GET_WEAPON_NAME_FROM_WEAPON_TYPE(sWepStruct.wtGunType[sWepStruct.iSelectedWeaponType])
					ELIF sFMMCMenu.sMenuGoto[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] = eFmmc_SURVIVAL_Veh_Config_base
						sFMMCmenu.iSurvivalVehicleSelection = 0
					ENDIF
					
					GO_TO_MENU(sFMMCmenu, sFMMCdata)
					
					IF sFMMCmenu.sActiveMenu = eFmmc_PAN_CAM_BASE
						IF IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
							sCurrentVarsStruct.iMenuState = MENU_STATE_SWITCH_CAM
						ENDIF
					ENDIF
					IF sFMMCmenu.sActiveMenu = eFmmc_PHOTO_CAM_BASE
						IF IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
							sCurrentVarsStruct.iMenuState = MENU_STATE_SWITCH_CAM
						ENDIF
					ENDIF
					
					IF sFMMCmenu.sMenuGoto[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] = eFmmc_SURVIVAL_Squad_Weapon_Select
					OR sFMMCmenu.sMenuGoto[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] = eFmmc_SURVIVAL_Squad_Weapons
						sFMMCmenu.iWepLibrary = g_FMMC_STRUCT.sSurvivalWaveData.iSquadWeaponCat[sFMMCmenu.iSurvivalWaveSelection][sFMMCmenu.iSurvivalWeaponSelection]
						sWepStruct.iSelectedWeaponType = g_FMMC_STRUCT.sSurvivalWaveData.iSquadWeaponType[sFMMCmenu.iSurvivalWaveSelection][sFMMCmenu.iSurvivalWeaponSelection]
						sWepStruct.wtGunType[sWepStruct.iSelectedWeaponType] = g_FMMC_STRUCT.sSurvivalWaveData.wtEnemyWeapons[sFMMCmenu.iSurvivalWaveSelection][sFMMCmenu.iSurvivalWeaponSelection]
//						sFMMCmenu.sSubTypeName[CREATION_TYPE_WEAPONS] = GET_WEAPON_NAME_FROM_WEAPON_TYPE(sWepStruct.wtGunType[sWepStruct.iSelectedWeaponType])
					ELIF sFMMCmenu.sMenuGoto[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] = eFmmc_SURVIVAL_Veh_Config
						IF GET_VEHICLE_MODEL_NUMBER_OF_SEATS(g_FMMC_STRUCT.sSurvivalWaveData.mnVeh[sFMMCmenu.iSurvivalWaveSelection][sFMMCmenu.iSurvivalVehicleSelection]) < g_FMMC_STRUCT.sSurvivalWaveData.iNumVehPeds[sFMMCmenu.iSurvivalWaveSelection][sFMMCmenu.iSurvivalVehicleSelection]
							g_FMMC_STRUCT.sSurvivalWaveData.iNumVehPeds[sFMMCmenu.iSurvivalWaveSelection][sFMMCmenu.iSurvivalVehicleSelection] = CLAMP_INT(GET_VEHICLE_MODEL_NUMBER_OF_SEATS(g_FMMC_STRUCT.sSurvivalWaveData.mnVeh[sFMMCmenu.iSurvivalWaveSelection][sFMMCmenu.iSurvivalVehicleSelection]), 1, 4)
							REFRESH_MENU_ASAP(sFMMCMenu)
						ENDIF
					ENDIF
					CLEAR_BIT(sFMMCdata.iBitSet, bPerformingPlacementShapeTest)	
					
					RETURN eFmmc_Action_Null
				ENDIF
				
				IF sFMMCmenu.sMenuAction[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] != eFmmc_Action_Null
						
					IF sFMMCmenu.sActiveMenu = eFmmc_PAN_CAM_BASE
					AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 0
						IF bInitialIntroCamSetup
							RETURN eFmmc_Action_Null
						ELIF GET_DISTANCE_BETWEEN_COORDS(g_FMMC_STRUCT.vStartPos, GET_ENTITY_COORDS(PLAYER_PED_ID())) > 100
							PLAY_SOUND_FRONTEND(-1, "ERROR", GET_SOUND_SET_FROM_CREATOR_TYPE())
							PRINTSTRING("ERROR SOUND EFFECT")PRINTNL()
							sCurrentVarsStruct.bDisplayFailReason = TRUE
							sCurrentVarsStruct.iFailDescriptionTimer = GET_GAME_TIMER()
							sCurrentVarsStruct.sFailReason = "FMMC_ER_023"
							RETURN eFmmc_Action_Null
						ENDIF
					ENDIF
					
					SWITCH sFMMCmenu.sActiveMenu
						CASE eFmmc_TEAM_TEST
							IF g_bFMMC_TutorialSelectedFromMpSkyMenu
							AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) != OPTION_SURV_TEST_SPWN_T
								RETURN eFmmc_Action_Null
							ENDIF
							IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_SURV_TEST_SPWN_T
								g_iTestType = 1
								g_iStartingWave = 1
							ELSE
								g_iTestType = 0
							ENDIF
							
							IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) >= OPTION_SURV_TEST_SPEC
								g_iStartingWave = GET_CREATOR_MENU_SELECTION(sFMMCmenu)-(OPTION_SURV_TEST_SPEC-1)
							ENDIF
						BREAK
					ENDSWITCH
					
					
					
					RETURN  sFMMCmenu.sMenuAction[GET_CREATOR_MENU_SELECTION(sFMMCmenu)]
				ENDIF
				
			ELSE
				PLAY_SOUND_FRONTEND(-1, "ERROR", GET_SOUND_SET_FROM_CREATOR_TYPE())
				PRINTSTRING("ERROR SOUND EFFECT")PRINTNL()
			ENDIF
			
		ENDIF
		
		IF FMMC_IS_CANCEL_JUST_PRESSED(sFMMCmenu)
			sCurrentVarsStruct.bDisplayFailReason = FALSE					
			IF IS_BIT_SET(sFMMCdata.iBitSet, biTestMissionActive)
				IF IS_BIT_SET(sFMMCdata.iBitSet, bResetMenu)
					CLEAR_BIT(sFMMCdata.iBitSet, bResetMenu)
				ENDIF
			ELSE
				sCurrentVarsStruct.bResetUpHelp = TRUE
				sCurrentVarsStruct.bDisplayFailReason = FALSE
				
				INT iSaveEntityType = sFMMCmenu.iEntityCreation
				CANCEL_ENTITY_CREATION(sFMMCmenu.iEntityCreation)
				IF bSubmenuActive
					SET_CREATION_TYPE(sFMMCMenu, iSaveEntityType)
				ENDIF
				
				IF (sFMMCMenu.iSelectedEntity != -1 AND NOT bSubmenuActive)
				OR (sFMMCmenu.sActiveMenu = eFmmc_DOORS_BASE AND sFMMCMenu.sCurrentDoor.mnDoorModel != DUMMY_MODEL_FOR_SCRIPT)
				OR (sFMMCmenu.sActiveMenu = efMMC_WORLD_PROPS AND sFMMCMenu.sCurrentWorldProp.mn != DUMMY_MODEL_FOR_SCRIPT)
					sFMMCMenu.iSelectedEntity = -1
				ELIF sFMMCMenu.iBoundsEditPointIndex > -1
					SET_EDIT_BOUNDS_POINT_INDEX(sFMMCMenu, -1)
				ELSE
					IF sFMMCmenu.sMenuBack != eFmmc_Null_item
						IF sFMMCmenu.sActiveMenu = eFmmc_SURVIVAL_Veh_Select
						OR sFMMCmenu.sActiveMenu = eFmmc_SURVIVAL_Veh_Wave_Model
							IF IS_MENU_A_SURVIVAL_OVERRIDE_MENU(sFMMCmenu)
								INT iWave, iVeh
								FOR iWave = 1 TO MAX_SURVIVAL_WAVES-1
									FOR iVeh = 0 TO MAX_SURV_LAND_VEH-1
										IF g_FMMC_STRUCT.sSurvivalWaveData.iNumVehPeds[iWave][iVeh] > CLAMP_INT(GET_VEHICLE_MODEL_NUMBER_OF_SEATS(g_FMMC_STRUCT.sSurvivalWaveData.mnVeh[1][0]), 1, 4)
											g_FMMC_STRUCT.sSurvivalWaveData.iNumVehPeds[iWave][iVeh] = CLAMP_INT(GET_VEHICLE_MODEL_NUMBER_OF_SEATS(g_FMMC_STRUCT.sSurvivalWaveData.mnVeh[1][0]), 1, 4)
										ENDIF
									ENDFOR
								ENDFOR
							ELIF sFMMCmenu.sMenuBack = eFmmc_SURVIVAL_Wave_Config
								INT iVeh
								FOR iVeh = 0 TO MAX_SURV_LAND_VEH-1
									g_FMMC_STRUCT.sSurvivalWaveData.iNumVehPeds[sFMMCmenu.iSurvivalWaveSelection][iVeh] = CLAMP_INT(GET_VEHICLE_MODEL_NUMBER_OF_SEATS(g_FMMC_STRUCT.sSurvivalWaveData.mnVeh[sFMMCmenu.iSurvivalWaveSelection][0]), 1, 4)
								ENDFOR
							ELSE
								IF CLAMP_INT(GET_VEHICLE_MODEL_NUMBER_OF_SEATS(g_FMMC_STRUCT.sSurvivalWaveData.mnVeh[sFMMCmenu.iSurvivalWaveSelection][sFMMCmenu.iSurvivalVehicleSelection]), 1, 4) < g_FMMC_STRUCT.sSurvivalWaveData.iNumVehPeds[sFMMCmenu.iSurvivalWaveSelection][sFMMCmenu.iSurvivalVehicleSelection]
									g_FMMC_STRUCT.sSurvivalWaveData.iNumVehPeds[sFMMCmenu.iSurvivalWaveSelection][sFMMCmenu.iSurvivalVehicleSelection] = CLAMP_INT(GET_VEHICLE_MODEL_NUMBER_OF_SEATS(g_FMMC_STRUCT.sSurvivalWaveData.mnVeh[sFMMCmenu.iSurvivalWaveSelection][sFMMCmenu.iSurvivalVehicleSelection]), 1, 4)
								ENDIF
							ENDIF
						ENDIF
						
						IF sFMMCmenu.sActiveMenu = eFmmc_SURVIVAL_Wave_Ped_Model
						OR sFMMCmenu.sActiveMenu = eFmmc_SURVIVAL_Veh_Wave_Model
						OR sFMMCmenu.sActiveMenu = eFmmc_SURVIVAL_Air_Wave_Model
						OR sFMMCmenu.sActiveMenu = eFmmc_SURVIVAL_Heavy_Model
							CLEAR_BIT(iLocalBitSet, biHaveShownConfirmScreenInMenu)
						ENDIF
						
						IF sFMMCmenu.sActiveMenu = eFmmc_SURVIVAL_WARNING_AREA
						AND NOT HAS_SURVIVAL_BOUNDS_BEEN_PLACED()
						AND sFMMCMenu.iBoundsEditPointIndex = -1
							REMOVE_SURVIVAL_AREAS()
						ENDIF
						
						PLAY_SOUND_FRONTEND(-1, "BACK", GET_SOUND_SET_FROM_CREATOR_TYPE())
						RESET_SELECTED_WAVE_ON_BACK(sFMMCmenu)
						GO_BACK_TO_MENU(sFMMCmenu)
						sFMMCmenu.sSubTypeName[CREATION_TYPE_CYCLE] = ""
					ENDIF
				ENDIF
				IF sFMMCmenu.sActiveMenu = eFmmc_SNAPPING_OPTIONS
				OR sFMMCmenu.sActiveMenu = eFmmc_PROP_ADVANCED_OPTIONS
					CHECK_IF_SNAPPING_SHOULD_BE_TURNED_OFF(sFMMCmenu)
				ENDIF
			ENDIF
			REFRESH_MENU(SFMMCMenu)
		ENDIF
		
		IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DELETE)
			SWITCH sFMMCmenu.sActiveMenu
				CASE efmmc_INTERIOR_LIST
				CASE efmmc_INTERIOR_BUNKER
				CASE efmmc_INTERIOR_SUB
				CASE efmmc_INTERIOR_IAA
				CASE efmmc_INTERIOR_HEISTS2
				CASE efmmc_ARENAMENUS_TOP
				CASE efmmc_INTERIOR_FIB
				 	IF GET_INTERIOR_NAME_ENUM_FROM_MENU_OPTION(sFMMCmenu.sActiveMenu, GET_CREATOR_MENU_SELECTION(sFMMCmenu)) != INTERIOR_MAX_NUM
						sCurrentVarsStruct.iMenuState = MENU_STATE_WARP_TO_INTERIOR
					ENDIF
				BREAK
				CASE efmmc_IPL_LIST
					VECTOR vIPLSelected
					IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = IPL_CARRIER
						vIPLSelected = vCarrierPosition
					ELIF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = IPL_REMOVE_IAA_FACILITY_SATELITE_ENTRY_DOOR
						vIPLSelected = vIAAFaciltyDoor
					ENDIF
					IF NOT IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
						SWAP_CAMERAS(sCamData, sFMMCdata, sFMMCMenu, sCurrentVarsStruct, TRUE, FALSE, TRUE)
					ENDIF
					IF VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), vIPLSelected) > 100
						SET_ENTITY_COORDS(PLAYER_PED_ID(), vIPLSelected)
					ENDIF
				BREAK
				CASE eFmmc_SURVIVAL_Wave_Base
					sFMMCmenu.iSurvivalWaveCopy = sFMMCmenu.iSurvivalWaveSelection+1
					IF sFMMCmenu.iSurvivalWaveCopy > g_FMMC_STRUCT.sSurvivalWaveData.iNumberOfWaves
						sFMMCmenu.iSurvivalWaveCopy = 1
					ENDIF
					sFMMCMenu.sMenuGoto[GET_CREATOR_MENU_SELECTION(sFMMCmenu)] = eFmmc_SURVIVAL_Copy_Wave
					GO_TO_MENU(sFMMCmenu, sFMMCdata)
				BREAK
			ENDSWITCH
		ENDIF
		
		IF MENU_CONTROL_DOWN_CREATOR(menuScrollController)
			SET_CREATOR_MENU_SELECTION(sFMMCmenu, GET_CREATOR_MENU_SELECTION(sFMMCmenu)+1)
			
			WHILE DEAL_WITH_SKIPPING_EMPTY_OPTIONS_SURVIVAL(1)
				PRINTLN("[SURVIVAL] Skipping options...")
			ENDWHILE
			
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) > sFMMCmenu.iCurrentMenuLength-1
				SET_CREATOR_MENU_SELECTION(sFMMCmenu, 0)
			ENDIF
			IF sFMMCmenu.iCurrentMenuLength > 1
				PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", GET_SOUND_SET_FROM_CREATOR_TYPE())
				PRINTSTRING("SCROLL SOUND EFFECT!!!")PRINTNL()
			ENDIF
			IF sFMMCmenu.sActiveMenu = eFmmc_TEAM_TEST
				IF g_FMMC_STRUCT.sSurvivalWaveData.iTestedWavesBitSet <= 1
					IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) > OPTION_SURV_TEST_SPWN_T
						SET_CREATOR_MENU_SELECTION(sFMMCmenu, 0)
					ENDIF
				ELSE
					SKIP_TESTED_WAVES(sFMMCmenu, 1)
				ENDIF
			ENDIF
			IF NOT sFMMCendStage.bHasValidROS
				sFMMCendStage.bHasValidROS = NETWORK_HAS_VALID_ROS_CREDENTIALS()
			ENDIF
			
			IF g_FMMC_STRUCT.bMissionIsPublished
				IF sFMMCmenu.sActiveMenu = eFmmc_TOP_MENU
				AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = SURV_TOP_MENU_SAVE
					SET_CREATOR_MENU_SELECTION(sFMMCmenu, GET_CREATOR_MENU_SELECTION(sFMMCmenu)+1)
				ENDIF
			ENDIF
			sCurrentVarsStruct.bResetUpHelp  = TRUE
			
			REFRESH_MENU(sFMMCmenu)
		ENDIF
		
		IF MENU_CONTROL_UP_CREATOR(menuScrollController)
			SET_CREATOR_MENU_SELECTION(sFMMCmenu, GET_CREATOR_MENU_SELECTION(sFMMCmenu)-1)
			
			WHILE DEAL_WITH_SKIPPING_EMPTY_OPTIONS_SURVIVAL(-1)
				PRINTLN("[SURVIVAL] Skipping options...")
			ENDWHILE
			
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) < 0
				SET_CREATOR_MENU_SELECTION(sFMMCmenu, sFMMCmenu.iCurrentMenuLength - 1)
			ENDIF
			IF sFMMCmenu.iCurrentMenuLength > 1
				PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", GET_SOUND_SET_FROM_CREATOR_TYPE())
				PRINTSTRING("SCROLL SOUND EFFECT!!!")PRINTNL()
			ENDIF
			IF sFMMCmenu.sActiveMenu = eFmmc_TEAM_TEST
				IF g_FMMC_STRUCT.sSurvivalWaveData.iTestedWavesBitSet <= 1
					IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) < 0
					OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) > OPTION_SURV_TEST_SPWN_T
						SET_CREATOR_MENU_SELECTION(sFMMCmenu, OPTION_SURV_TEST_SPWN_T)
					ENDIF
				ELSE
					SKIP_TESTED_WAVES(sFMMCmenu, -1)
				ENDIF
			ENDIF
			IF g_FMMC_STRUCT.bMissionIsPublished
				IF sFMMCmenu.sActiveMenu = eFmmc_TOP_MENU
				AND GET_CREATOR_MENU_SELECTION(sFMMCmenu) = SURV_TOP_MENU_SAVE
					SET_CREATOR_MENU_SELECTION(sFMMCmenu, GET_CREATOR_MENU_SELECTION(sFMMCmenu)-1)
				ENDIF
			ENDIF
			sCurrentVarsStruct.bResetUpHelp  = TRUE
			
			REFRESH_MENU(sFMMCmenu)
		ENDIF
		
		INT iChange = 0
		IF MENU_CONTROL_RIGHT_CREATOR(menuScrollController)
			iChange = 1
		ENDIF
		
		IF MENU_CONTROL_LEFT_CREATOR(menuScrollController)
			iChange = -1
		ENDIF
		
		IF iChange != 0
			EDIT_MENU_OPTIONS(iChange)
			REFRESH_MENU(sFMMCmenu)
		ENDIF
		
		HANDLE_SWITCH_TO_GROUND_CAMERA()
		
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LB)
		OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RB)
			IF IS_SURVIVAL_MENU_BUMP_SWITCH_SQUAD(sFMMCmenu)
				IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LB)
					sFMMCmenu.iSurvivalDiffSelection--
				ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RB)
					sFMMCmenu.iSurvivalDiffSelection++
				ENDIF
				CAP_FMMC_MENU_ITEM(3, sFMMCmenu.iSurvivalDiffSelection)
				REFRESH_MENU(sFMMCmenu)
			ELIF IS_SURVIVAL_MENU_BUMP_SWITCH_WAVE(sFMMCmenu)
				IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LB)
					sFMMCmenu.iSurvivalWaveSelection--
				ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RB)
					sFMMCmenu.iSurvivalWaveSelection++
				ENDIF
				CAP_FMMC_MENU_ITEM(g_FMMC_STRUCT.sSurvivalWaveData.iNumberOfWaves+1, sFMMCmenu.iSurvivalWaveSelection, 1)
				
				IF sFMMCmenu.sActiveMenu = eFmmc_SURVIVAL_Pickup_Placement_Choose_Weapon
					GET_WEAPON_INFO_FOR_OVERRIDE_WAVE_SWAP()
					sFMMCMenu.iReturnSelections[3] = sFMMCmenu.iSurvivalWaveSelection-1
				ENDIF
				IF sFMMCmenu.sActiveMenu != eFmmc_SURVIVAL_Pickup_Placement_Choose_Weapon
					sFMMCMenu.iReturnSelections[2] = sFMMCmenu.iSurvivalWaveSelection-1
				ENDIF
				REFRESH_MENU(sFMMCmenu)
			ELIF sFMMCmenu.sActiveMenu = eFmmc_SURVIVAL_Wave_Base
			AND IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RB)
			AND IS_THIS_OPTION_SELECTABLE()
			AND NOT g_bFMMC_TutorialSelectedFromMpSkyMenu
				TRIGGER_CONFIRMATION_SCREEN(sCurrentVarsStruct, "SC_AYS", "SC_CONF_RTW")
			ENDIF
		ENDIF
		
	ENDIF
	
	RETURN eFmmc_Action_Null
ENDFUNC

PROC HANDLE_MENU_STATE()
	SWITCH sCurrentVarsStruct.iMenuState
		CASE MENU_STATE_DEFAULT
			SET_FAR_RIGHT_TITLE_POSITION_HUD_THIS_FRAME()
			IF sFMMCmenu.sActiveMenu = eFmmc_MAIN_MENU_BASE
				SET_AREA_GOOD_FOR_PLACEMENT(sCurrentVarsStruct, FALSE)
				sCurrentVarsStruct.bFirstShapeTestCheckDone = FALSE
				sCurrentVarsStruct.sCoronaShapeTest.bDoStartShapeTest = FALSE
				sCurrentVarsStruct.sCoronaShapeTest.stiShapeTest  = NULL	
			ENDIF
			
			IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
				IF SHOULD_MENU_BE_DRAWN(sFMMCData, sFMMCMenu, sCurrentVarsStruct, menuScrollController, 1 + g_FMMC_STRUCT.iVehicleDeathmatch, sFMMCmenu.sActiveMenu = eFmmc_TOP_MENU, bShowMenu)
					bShowMenuHighlight = TRUE
					IF MAINTAIN_FMMC_RADIO(sFMMCmenu, menuScrollController)
						IF !bInitialRadioRefreshDone
							REFRESH_MENU(sFMMCMenu)
							bInitialRadioRefreshDone = TRUE
						ENDIF
					ENDIF
					FMMC_DO_MENU_ACTIONS(RETURN_CIRCLE_MENU_SELECTIONS(), sCurrentVarsStruct, sFMMCmenu, sFMMCData, sFMMCendStage, sHCS)
					HANDLE_ENTITY_CREATION()
					SET_INSTRUCTIONAL_BUTTONS_UNDER_HUD_THIS_FRAME(2)
				ELSE
					sCurrentVarsStruct.bDiscVisible = FALSE
					
					sPedStruct.iSwitchingINT = CREATION_STAGE_WAIT
					sWepStruct.iSwitchingINT = CREATION_STAGE_WAIT
					sVehStruct.iSwitchingINT = CREATION_STAGE_WAIT
					SET_CURRENT_GENERIC_CREATION_STAGE(CREATION_STAGE_WAIT)
					
					IF DOES_ENTITY_EXIST(sVehStruct.viCoronaVeh)
						DELETE_VEHICLE(sVehStruct.viCoronaVeh)
					ENDIF
					IF DOES_ENTITY_EXIST(sWepStruct.viCoronaWep)
						DELETE_OBJECT(sWepStruct.viCoronaWep)
					ENDIF
					IF DOES_ENTITY_EXIST(sPropStruct.viCoronaObj)
						DELETE_OBJECT(sPropStruct.viCoronaObj)
					ENDIF
					DELETE_CURRENT_PROP_TEMPLATE_OBJECTS(sPropStruct)
				ENDIF
			ENDIF
			
			IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
			AND sCurrentVarsStruct.iMenuState != MENU_STATE_SWITCH_CAM
				BOOL bReloadMenu
				IF NOT IS_BIT_sET(sFMMCdata.iBitSet, bOnTheGround)
					DEAL_WITH_SKY_CAM_SWITCH(sFMMCdata,bSwitchingCam,vSwitchVec,sCamData, bReloadMenu)
				ELSE
					DEAL_WITH_PLAYER_WARP_TO_ITEM(sFMMCdata,bSwitchingCam,vSwitchVec,fSwitchHeading, bReloadMenu)
				ENDIF
				IF bReloadMenu
					REFRESH_MENU(sFMMCMenu)
				ENDIF
			ENDIF
			
			CHECK_FOR_MENU_SET_UP(sCurrentVarsStruct, sPedStruct, sVehStruct, sWepStruct, sObjStruct, sFMMCmenu)
			IF sCurrentVarsStruct.bResetUpHelp
			AND (sCurrentVarsStruct.bFirstShapeTestCheckDone OR sFMMCmenu.iEntityCreation = -1 OR sFMMCmenu.iEntityCreation = CREATION_TYPE_PAN_CAM)
				sCurrentVarsStruct.bResetUpHelp  = FALSE
			ENDIF
		BREAK
		
		CASE MENU_STATE_PLACE_CAM
			IF PPS.iStage != WAIT_PREVIEW_PHOTO
			AND PPS.iStage != CLEANUP_PREVIEW_PHOTO
			AND sFMMCendStage.sTakePhotoVars.iTakePhotoStage != ciFMMC_TAKE_PHOTO_STAGE_OPEN_SHUT
				PPS.iStage = CLEANUP_PREVIEW_PHOTO
				PPS.iPreviewPhotoDelayCleanup = 0
			ENDIF
			
			IF sFMMCmenu.sActiveMenu = eFmmc_PHOTO_CAM_BASE							
				REMOVE_DECALS_FOR_PHOTOS(sFMMCendStage.sTakePhotoVars.iTakePhotoStage, sStartEndBlips, sPedStruct, sTeamSpawnStruct, sWepStruct, sObjStruct, sHCS.hcStartCoronaColour)
			ENDIF
			IF TAKE_PHOTO_FOR_FMMC(sFMMCendStage.sTakePhotoVars, SF_Movie_Gallery_Shutter_Index, sFMMCmenu.sActiveMenu = eFmmc_PHOTO_CAM_BASE)
				IF sFMMCmenu.sActiveMenu = eFmmc_PAN_CAM_BASE
					g_FMMC_STRUCT.vCameraPanPos = GET_CAM_COORD(GET_RENDERING_CAM())
					g_FMMC_STRUCT.vCameraPanRot = GET_CAM_ROT(GET_RENDERING_CAM())
					VECTOR vRot
					vRot = GET_CAM_ROT(GET_RENDERING_CAM())
					g_FMMC_STRUCT.fCameraPanHead = vRot.z
					PRINTLN("PAN CAM POSITIONED Rot - ", g_FMMC_STRUCT.vCameraPanRot, " Vector - ", g_FMMC_STRUCT.vCameraPanPos)
				ENDIF
				sFMMCendStage.bMajorEditOnLoadedMission = TRUE
				REFRESH_MENU(sFMMCMenu)
				sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
			ENDIF
		BREAK
		
		CASE MENU_STATE_PAN_CAM
			IF DEAL_WITH_CREATOR_CAMERA_PAN_PREVIEW(iCamPanState, jobIntroData, vInitialPreviewPos)	
				IF !SCRIPT_IS_CLOUD_AVAILABLE()
					IF NOT IS_SCREEN_FADED_IN()
						DO_SCREEN_FADE_IN(500)
					ENDIF	
				ENDIF
				sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
			ENDIF
		BREAK
		
		CASE MENU_STATE_TITLE
			IF bDelayAFrame = FALSE
				IF DEAL_WITH_SETTING_MISSION_TEXT(sFMMCendStage, TRUE)	
					sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
					REFRESH_MENU(sFMMCMenu)
					bDelayAFrame = TRUE
				ELSE							
					bShowMenuHighlight = FALSE
				ENDIF
			ELSE
				bDelayAFrame = FALSE
			ENDIF
		BREAK
		
		CASE MENU_STATE_DESCRIPTION
			IF bDelayAFrame = FALSE
				IF DEAL_WITH_SETTING_MISSION_TEXT(sFMMCendStage, FALSE)	
					sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
					REFRESH_MENU(sFMMCMenu)
					bDelayAFrame = TRUE
				ELSE							
					bShowMenuHighlight = FALSE
				ENDIF
			ELSE
				bDelayAFrame = FALSE
			ENDIF
		BREAK
		
		CASE MENU_STATE_TAGS
			IF bDelayAFrame = FALSE
				IF DEAL_WITH_SETTING_MISSION_TEXT(sFMMCendStage, FALSE, TRUE)	
					sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
					REFRESH_MENU(sFMMCMenu)
					bDelayAFrame = TRUE
				ELSE							
					bShowMenuHighlight = FALSE
				ENDIF
			ELSE
				bDelayAFrame = FALSE
			ENDIF
		BREAK
		
		CASE MENU_STATE_SWITCH_CAM
			IF NOT IS_SCREEN_FADED_OUT()
				PRINTSTRING("screen not faded out yet, calling fade out")PRINTNL()
				DO_SCREEN_FADE_OUT(500)								
				IF DOES_ENTITY_EXIST(sWepStruct.viCoronaWep)
					SET_ENTITY_ALPHA(sWepStruct.viCoronaWep, 0, FALSE)
				ENDIF
				IF DOES_ENTITY_EXIST(sPropStruct.viCoronaObj)
					SET_ENTITY_ALPHA(sPropStruct.viCoronaObj, 0, FALSE)
				ENDIF
				IF DOES_ENTITY_EXIST(sDynoPropStruct.viCoronaObj)
					SET_ENTITY_ALPHA(sDynoPropStruct.viCoronaObj, 0, FALSE)
				ENDIF
				IF DOES_ENTITY_EXIST(sVehStruct.viCoronaVeh)
					SET_ENTITY_ALPHA(sVehStruct.viCoronaVeh, 0, FALSE)
				ENDIF
				IF DOES_ENTITY_EXIST(vehSelection)
					SET_ENTITY_ALPHA(vehSelection, 0, FALSE)
				ENDIF
				IF DOES_ENTITY_EXIST(pedSelection)
					SET_ENTITY_ALPHA(pedSelection, 0, FALSE)
				ENDIF
				IF DOES_ENTITY_EXIST(sPedStruct.piTempPed)
					SET_ENTITY_ALPHA(sPedStruct.piTempPed, 0, FALSE)
				ENDIF
			ELSE
				PRINTSTRING("Screen is faded out do swap cams")PRINTNL()
				IF DOES_ENTITY_EXIST(vehSelection)
					DELETE_VEHICLE(vehSelection)
				ENDIF
				IF DOES_ENTITY_EXIST(pedSelection)
					DELETE_PED(pedSelection)
				ENDIF
				IF NOT IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
					SWAP_CAMERAS(sCamData, sFMMCdata, sFMMCMenu, sCurrentVarsStruct,TRUE,FALSE,TRUE,FALSE)
				ELSE
					SWAP_CAMERAS(sCamData, sFMMCdata, sFMMCMenu, sCurrentVarsStruct,FALSE,FALSE,TRUE,FALSE)
				ENDIF
				NEW_LOAD_SCENE_START_SPHERE(GET_ENTITY_COORDS(PLAYER_PED_ID()), 20)
				sCurrentVarsStruct.iMenuState = MENU_STATE_LOADING_AREA
				iLoadingOverrideTimer = GET_GAME_TIMER()
			ENDIF
		BREAK
		
		CASE MENU_STATE_LOADING_AREA
			IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
			OR IS_NEW_LOAD_SCENE_LOADED()	
			OR iLoadingOverrideTimer + 8000 < GET_GAME_TIMER()
			
				PRINTSTRING("LOADED, FADE IN")PRINTNL()
				IF NOT bSwitchingCam
					DO_SCREEN_FADE_IN(500)
				ENDIF
				NEW_LOAD_SCENE_STOP()
				IF IS_BIT_sET(sFMMCdata.iBitSet, bOnTheGround)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
					FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
				ELSE
					FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
				ENDIF
				
				IF DOES_ENTITY_EXIST(sWepStruct.viCoronaWep)
					RESET_ENTITY_ALPHA(sWepStruct.viCoronaWep)
				ENDIF
				IF DOES_ENTITY_EXIST(sPropStruct.viCoronaObj)
					RESET_ENTITY_ALPHA(sPropStruct.viCoronaObj)
				ENDIF
				IF DOES_ENTITY_EXIST(sDynoPropStruct.viCoronaObj)
					RESET_ENTITY_ALPHA(sDynoPropStruct.viCoronaObj)
				ENDIF
				IF DOES_ENTITY_EXIST(sVehStruct.viCoronaVeh)
					RESET_ENTITY_ALPHA(sVehStruct.viCoronaVeh)
				ENDIF								
				
				sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT

			ELSE							
				PRINTSTRING("WAITING FOR SCEEN TO LOAD")PRINTNL()
			ENDIF
		BREAK
		
		CASE MENU_STATE_TEST_MISSION
			//Empty
		BREAK
		
		CASE MENU_STATE_CUSTOM_TEMPLATE_NAME
			IF bDelayAFrame = FALSE
				IF DEAL_WITH_SETTING_CUSTOM_TEMPLATE_NAME(sFMMCendStage, g_FMMC_STRUCT.iNumberOfPropTemplates-1)
					sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
					REFRESH_MENU(sFMMCmenu)
					bDelayAFrame = TRUE
				ENDIF
			ELSE
				bDelayAFrame = FALSE
			ENDIF
		BREAK
		
		CASE MENU_STATE_WARP_TO_INTERIOR
			HANDLE_PLAYER_INTERIOR_WARP(iWarpState, iWarpStateTimeoutStart, sFMMCmenu, sFMMCdata, sCamData, sCurrentVarsStruct)
		BREAK
		
		CASE MENU_STATE_BLIMP_MESSAGE
			IF bDelayAFrame = FALSE
				IF DEAL_WITH_SETTING_MISSION_TEXT(sFMMCendStage, FALSE, FALSE, TRUE)
					REFRESH_MENU(sFMMCMenu)
					sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
					bDelayAFrame = TRUE
					CLEANUP_BLIMP(sBlimpSign)
				ELSE
					bShowMenuHighlight = FALSE
				ENDIF
			ELSE
				bDelayAFrame = FALSE
			ENDIF
		BREAK
		CASE MENU_STATE_CONFIRMATION_SCREEN
			IF DOES_ENTITY_EXIST(vehSelection)
				DELETE_VEHICLE(vehSelection)
			ENDIF
			IF DOES_ENTITY_EXIST(pedSelection)
				DELETE_PED(pedSelection)
			ENDIF
			HANDLE_CONFIRMATION_SCREEN(sFMMCmenu, sCurrentVarsStruct, sFMMCendStage, sPedStruct, sTeamSpawnStruct[0], iLocalBitSet)
		BREAK
	ENDSWITCH
ENDPROC

PROC CHECK_FOR_END_CONDITIONS_MET()
 // Handled in CAN_SURVIVAL_BE_SAVED etcd	
ENDPROC

PROC MENU_INITIALISATION()
	sFMMCmenu.sSubTypeName[CREATION_TYPE_WEAPONS] 					= GET_CREATOR_NAME_FOR_PICKUP_TYPE(GET_PICKUP_TYPE_FROM_WEAPON_TYPE(sWepStruct.wtGunType[0]))
	sFMMCmenu.sSubTypeName[CREATION_TYPE_PROPS]             		= GET_CREATOR_NAME_FOR_PROP_MODEL(GET_CREATOR_PROP_MODEL(sFMMCmenu.iPropLibrary, sFMMCmenu.iPropType))
	sFMMCmenu.sSubTypeName[CREATION_TYPE_DYNOPROPS]         		= GET_CREATOR_NAME_FOR_PROP_MODEL(GET_CREATOR_PROP_MODEL(sFMMCmenu.iPropLibrary, sFMMCmenu.iPropType))
	
	SET_ALL_MENU_ITEMS_ACTIVE()
	
	REFRESH_MENU(sFMMCMenu)
		
ENDPROC

PROC SET_STARTING_MENUS_ACTIVE()	
	SET_BIT(sFMMCmenu.iTopMenuActive, SURV_TOP_MENU_CREATOR)
	SET_BIT(sFMMCmenu.iTopMenuActive, SURV_TOP_MENU_RADIO)
	SET_BIT(sFMMCmenu.iTopMenuActive, SURV_TOP_MENU_OPTIONS)
	SET_BIT(sFMMCmenu.iTopMenuActive, SURV_TOP_MENU_EXIT)
ENDPROC

PROC HANDLE_MOUSE_ENTITY_SELECTION_CANCEL_AND_SWITCH(eFMMC_MENU_ENUM itemMenu, INT iNewItemIndex, INT iMaxItems)
	// cancel out of stuff
	CANCEL_ENTITY_CREATION(sFMMCmenu.iEntityCreation) 		
	IF sFMMCMenu.iSelectedEntity != -1	
		sFMMCMenu.iSelectedEntity = -1
	ENDIF

	// force the menu to be the one that we want
	GO_TO_MENU(sFMMCMenu, sFMMCdata, itemMenu)
	REFRESH_MENU(sFMMCMenu)

	// instead of looping through until the switch cam increases we force it
	sFMMCmenu.iSwitchCam = iNewItemIndex - 1
	DEAL_WITH_CAMERA_SWITCH_SELECTION(TRUE, iMaxItems)
	SET_CURSOR_POSITION(0.5, 0.5)
	PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", GET_SOUND_SET_FROM_CREATOR_TYPE())			
ENDPROC

PROC HANDLE_MOUSE_ENTITY_SELECTION(BOOL bTesting = FALSE)

	BOOL bMouseOverCorona = FALSE
	
	IF NOT FMMC_DO_HANDLE_MOUSE_SELECTION_CHECK(sFMMCmenu, sCurrentVarsStruct, bMouseOverCorona, bTesting)
		EXIT
	ENDIF
	
	FP_FMMC_MOUSE_ENTITY_SELECT_HANDLER funcPtr = &HANDLE_MOUSE_ENTITY_SELECTION_CANCEL_AND_SWITCH
	
	// check objects
	IF FMMC_HANDLE_MOUSE_DYNOPROP_SELECTION(sFMMCmenu, sDynoPropStruct, sCurrentVarsStruct, iLocalBitSet, funcPtr)
		EXIT
	ENDIF
	
	IF FMMC_HANDLE_MOUSE_PROP_SELECTION(sFMMCmenu, sPropStruct, sCurrentVarsStruct, iLocalBitSet, funcPtr)
		EXIT
	ENDIF
	 
	IF FMMC_HANDLE_MOUSE_SPAWN_POINT_SELECTION(sFMMCmenu, sPedStruct, sCurrentVarsStruct, iLocalBitSet, funcPtr)
		EXIT
	ENDIF
	
	IF FMMC_HANDLE_MOUSE_TEAM_0_SPAWN_POINT_SELECTION(sFMMCmenu, sTeamSpawnStruct[0], sCurrentVarsStruct, iLocalBitSet, funcPtr)
		EXIT
	ENDIF
	
	IF FMMC_HANDLE_MOUSE_TEAM_1_SPAWN_POINT_SELECTION(sFMMCmenu, sTeamSpawnStruct[1], sCurrentVarsStruct, iLocalBitSet, funcPtr)
		EXIT
	ENDIF
	
	IF FMMC_HANDLE_MOUSE_TEAM_2_SPAWN_POINT_SELECTION(sFMMCmenu, sTeamSpawnStruct[2], sCurrentVarsStruct, iLocalBitSet, funcPtr)
		EXIT
	ENDIF
	
	IF FMMC_HANDLE_MOUSE_TEAM_3_SPAWN_POINT_SELECTION(sFMMCmenu, sTeamSpawnStruct[3], sCurrentVarsStruct, iLocalBitSet, funcPtr)
		EXIT
	ENDIF
	
	IF FMMC_HANDLE_MOUSE_VEHICLE_SELECTION(sFMMCmenu, sVehStruct, sCurrentVarsStruct, iLocalBitSet, funcPtr)
		EXIT
	ENDIF
	
	IF FMMC_HANDLE_MOUSE_WEAPON_SELECTION(sFMMCmenu, sWepStruct, sCurrentVarsStruct, sInvisibleObjects, iLocalBitSet, funcPtr)
		EXIT
	ENDIF
	
	IF (bMouseOverCorona = FALSE) AND NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, GET_CREATOR_CAMERA_ORBIT_TOGGLE_BUTTON())
		FMMC_SET_CURSOR_MAP_ACCEPT_BIT(sFMMCmenu, FALSE)
	ENDIF
ENDPROC

PROC DO_END_MENU()
	
	IF NOT IS_SCREEN_FADING_OUT()
		IF NOT IS_SCREEN_FADED_OUT()
			IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
			AND NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
				DO_SCREEN_FADE_OUT(500)	
				SET_CREATOR_AUDIO(FALSE, TRUE)
			ENDIF
		ELSE

			DRAW_END_MENU()
			
			IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
				SET_ENTITY_CREATION_STATUS(STAGE_ENTITY_PLACEMENT_SETUP)
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				sCurrentVarsStruct.bSwitching = FALSE
				sCurrentVarsStruct.stiScreenCentreLOS = NULL
				DO_SCREEN_FADE_IN(500)
				SET_CREATOR_AUDIO(TRUE, TRUE)
			ENDIF
			IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
				SET_ENTITY_CREATION_STATUS(STAGE_DEAL_WITH_FINISHING)
			ENDIF
				
		ENDIF
	ENDIF
	
ENDPROC

PROC SET_RESET_STATE(RESET_STATE eNewState)
	PRINTLN("SET_RESET_STATE - Changing iResetState from ", iResetState, " to ", eNewState)
	iResetState = eNewState
ENDPROC

PROC MAINTAIN_BLIPS_FOR_PLACED_AREAS()
	
	IF IS_BIT_SET(sFMMCdata.iBitSet, biTestMissionActive)
		EXIT
	ENDIF
	
	// Spawn Area
	IF HAS_SURVIVAL_BOUNDS_BEEN_PLACED()
		IF NOT DOES_BLIP_EXIST(biPlayArea)
			SWITCH g_FMMC_STRUCT.sSurvivalWaveData.iBoundsType
				CASE SURV_BOUNDS_NON_AXIS_AREA
					biPlayArea = ADD_BLIP_FOR_AREA_FROM_EDGES(g_FMMC_STRUCT.sSurvivalWaveData.vBounds[0], GET_SURVIVAL_BOUNDS_POINT_WITH_HEIGHT(), g_FMMC_STRUCT.sSurvivalWaveData.fBoundsRadius)
				BREAK
				CASE SURV_BOUNDS_CYLINDER
					biPlayArea = ADD_BLIP_FOR_RADIUS(g_FMMC_STRUCT.sSurvivalWaveData.vBounds[0], g_FMMC_STRUCT.sSurvivalWaveData.fBoundsRadius/2)
				BREAK
			ENDSWITCH
			SET_BLIP_COLOUR(biPlayArea, BLIP_COLOUR_YELLOW)
			SET_BLIP_ALPHA(biPlayArea, 120)
			fCreatedPlayAreaWidth = g_FMMC_STRUCT.sSurvivalWaveData.fBoundsRadius
		ELSE
			IF fCreatedPlayAreaWidth != g_FMMC_STRUCT.sSurvivalWaveData.fBoundsRadius
			OR sFMMCMenu.bRefreshPlayArea
				REMOVE_BLIP(biPlayArea)
				sFMMCMenu.bRefreshPlayArea = FALSE
			ENDIF
		ENDIF
	ELSE
		IF DOES_BLIP_EXIST(biPlayArea)
			REMOVE_BLIP(biPlayArea)
		ENDIF
	ENDIF
		
ENDPROC

FUNC BOOL RESET_THE_MISSION_UP_SAFELY()

	INT i

	SWITCH iResetState
		CASE RESET_STATE_FADE
			PRINTLN("RESET_STATE_FADE")
			IF IS_FAKE_MULTIPLAYER_MODE_SET() = TRUE 
				
				SET_OVERRIDE_WEATHER("CLEAR")
				
			    IF SCRIPT_IS_CLOUD_AVAILABLE()
					PRINTLN("SCRIPT_IS_CLOUD_AVAILABLE()")
				    SET_FAKE_MULTIPLAYER_MODE(FALSE)
				ELSE
					PRINTLN("NOT SCRIPT_IS_CLOUD_AVAILABLE()")
					//NETWORK_SESSION_LEAVE_SINGLE_PLAYER() 
					g_Private_IsMultiplayerCreatorRunning = FALSE
					g_Private_MultiplayerCreatorNeedsToEnd = TRUE
				ENDIF
			ENDIF
			IF IS_FAKE_MULTIPLAYER_MODE_SET() = FALSE 
				SET_RESET_STATE(RESET_STATE_CLEAR)
				RESET_INVISIBLE_OBJECT_POSITIONS(sInvisibleObjects)
				
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())				
					SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(PLAYER_PED_ID(), KNOCKOFFVEHICLE_DEFAULT)
					SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_WillFlyThroughWindscreen, FALSE)	
				ENDIF
				
				IF bOnGroundBeforeTest = FALSE
					CLEAR_BIT(sFMMCdata.iBitSet, bCameraActive)
					PRINTLN("CLEAR_BIT(sFMMCdata.iBitSet, bCameraActive) GO BACK TO CAMERA")
				ELSE
					SET_BIT(sFMMCdata.iBitSet, bCameraActive)	
					PRINTLN("SET_BIT(sFMMCdata.iBitSet, bCameraActive) GO BACK TO GROUND")
				ENDIF
				SWAP_CAMERAS(sCamData, sFMMCdata, sFMMCMenu, sCurrentVarsStruct, FALSE, FALSE, FALSE)
			ENDIF
			
		BREAK
		
		CASE RESET_STATE_CLEAR
			PRINTLN("RESET_STATE_CLEAR")
			IF (NOT IS_FAKE_MULTIPLAYER_MODE_SET()
			AND NOT NETWORK_IS_GAME_IN_PROGRESS())
			OR NOT SCRIPT_IS_CLOUD_AVAILABLE()
				FOR i = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles - 1)
					IF DOES_BLIP_EXIST(sVehStruct.biVehicleBlip[i])
						REMOVE_BLIP(sVehStruct.biVehicleBlip[i])
					ENDIF
					IF DOES_ENTITY_EXIST(sVehStruct.veVehcile[i])
						SET_ENTITY_AS_MISSION_ENTITY(sVehStruct.veVehcile[i], TRUE, TRUE)
						DELETE_VEHICLE(sVehStruct.veVehcile[i])
					ENDIF
				ENDFOR				
				FOR i = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfProps - 1)
					IF DOES_BLIP_EXIST(sPropStruct.biObject[i])
						REMOVE_BLIP(sPropStruct.biObject[i])
					ENDIF
					if DOES_ENTITY_EXIST(sPropStruct.oiObject[i])
						DELETE_OBJECT(sPropStruct.oiObject[i])
					ENDIF
				ENDFOR						
				FOR i = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps - 1)
					IF DOES_BLIP_EXIST(sDynoPropStruct.biObject[i])
						REMOVE_BLIP(sDynoPropStruct.biObject[i])
					ENDIF
					if DOES_ENTITY_EXIST(sDynoPropStruct.oiObject[i])
						DELETE_OBJECT(sDynoPropStruct.oiObject[i])
					ENDIF
				ENDFOR	
				FOR i = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons - 1)
					IF DOES_PICKUP_EXIST(sWepStruct.Pickups[i])
						REMOVE_PICKUP(sWepStruct.Pickups[i])
					ENDIF
					IF DOES_BLIP_EXIST(sWepStruct.biWeaponBlip[i])
						REMOVE_BLIP(sWepStruct.biWeaponBlip[i])
					ENDIF
					if DOES_ENTITY_EXIST(sWepStruct.oiWeapon[i])
						DELETE_OBJECT(sWepStruct.oiWeapon[i])
					ENDIF
					IF sWepStruct.iDecalNum[i] != -1
						IF IS_DECAL_ALIVE(sWepStruct.diDecal[sWepStruct.iDecalNum[i]])
							REMOVE_DECAL(sWepStruct.diDecal[sWepStruct.iDecalNum[i]])
						ENDIF
					ENDIF
				ENDFOR				
				IF DOES_BLIP_EXIST(biPlayArea)
					REMOVE_BLIP(biPlayArea)
				ENDIF
				IF DOES_BLIP_EXIST(sStartEndBlips.biStart)
					REMOVE_BLIP(sStartEndBlips.biStart)
				ENDIF							
				IF sStartEndBlips.ciStartType != NULL
					DELETE_CHECKPOINT(sStartEndBlips.ciStartType)
					sStartEndBlips.ciStartType = NULL
				ENDIF
				SET_RESET_STATE(RESET_STATE_INIT_BUDGET)
			ENDIF
		BREAK
		
		CASE RESET_STATE_INIT_BUDGET
			PRINTLN("RESET_STATE_INIT_BUDGET")
			INIT_CREATOR_BUDGET()
			SET_RESET_STATE(RESET_STATE_INTERIORS)
		BREAK
		
		CASE RESET_STATE_INTERIORS
			PRINTLN("RESET_STATE_INTERIORS")
			LOAD_ALL_ACTIVE_INTERIORS()
			SET_RESET_STATE(RESET_STATE_WEAPONS)
		BREAK
		
		CASE RESET_STATE_WEAPONS
			PRINTLN("RESET_STATE_WEAPONS")
			IF CREATE_ALL_CURRENT_PICKUPS(sWepStruct, TRUE, sCurrentVarsStruct)
				SET_RESET_STATE(RESET_STATE_PROPS)
			ENDIF
		BREAK
		
		CASE RESET_STATE_PROPS
			PRINTLN("RESET_STATE_PROPS")
			IF CREATE_ALL_CREATOR_PROPS(sPropStruct, iPropModelTimers)
				SET_RESET_STATE(RESET_STATE_DYNOPROPS)
			ENDIF
		BREAK
		
		CASE RESET_STATE_DYNOPROPS
			PRINTLN("RESET_STATE_DYNOPROPS")
			IF CREATE_ALL_CREATOR_DYNOPROPS(sDynoPropStruct)
				SET_RESET_STATE(RESET_STATE_VEHICLES)
			ENDIF
		BREAK
		
		CASE RESET_STATE_VEHICLES
			PRINTLN("RESET_STATE_VEHICLES")
			IF CREATE_ALL_CREATOR_VEHICLES(sVehStruct, iVehicleModelLoadTimers)
				SET_RESET_STATE(RESET_STATE_PEDS)
			ENDIF			
		BREAK
		
		CASE RESET_STATE_PEDS
			PRINTLN("RESET_STATE_PEDS")
			IF CREATE_ALL_CREATOR_PEDS(sPedStruct, sVehStruct, sFMMCmenu)
				SET_RESET_STATE(RESET_STATE_ATTACH)
			ENDIF			
		BREAK
		
		CASE RESET_STATE_ATTACH
			PRINTLN("RESET_STATE_ATTACH")
			FOR i = 0 TO FMMC_MAX_VEHICLES-1
				IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAttachParent >= 0
					IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAttachParentType = CREATION_TYPE_VEHICLES
						FMMC_ATTACH_VEHICLE_TO_VEHICLE(sVehStruct.veVehcile[i], sVehStruct.veVehcile[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAttachParent])
					ELIF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAttachParentType = CREATION_TYPE_OBJECTS
						FMMC_ATTACH_VEHICLE_TO_OBJECT(sVehStruct.veVehcile[i], sObjStruct.oiObject[g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iAttachParent])
					ENDIF
				ENDIF
			ENDFOR
			SET_RESET_STATE(RESET_STATE_TEAM_SPAWNS)
		BREAK
		
		CASE RESET_STATE_SPAWNS
			PRINTLN("RESET_STATE_SPAWNS")			
			FOR i = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfSpawnPoints - 1)
				IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[i].vPos)		
					IF NOT DOES_BLIP_EXIST(biSpawnBlips[i])
						CREATE_FMMC_BLIP(biSpawnBlips[i], g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[i].vPos, HUD_COLOUR_BLUELIGHT, "FMMC_B_3", 1)
						PRINTLN("CREATED A SPAWN BLIP AND DECAL ", i)						
						RETURN FALSE
					ENDIF
				ENDIF
			ENDFOR	
			
			SET_RESET_STATE(RESET_STATE_OTHER)
			
		BREAK
		
		CASE RESET_STATE_TEAM_SPAWNS
			PRINTLN("RESET_STATE_TEAM_SPAWNS")	
			FOR i = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[0] - 1)
				IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[0][i].vPos)		
					IF NOT DOES_BLIP_EXIST(sTeamSpawnStruct[0].biPedBlip[i])
						CREATE_FMMC_BLIP(sTeamSpawnStruct[0].biPedBlip[i], g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[0][i].vPos, HUD_COLOUR_WHITE, getNameForTeamSpawnBlip(0), 1)
						SET_BLIP_COLOUR(sTeamSpawnStruct[0].biPedBlip[i], getColourForSpawnBlip(0))
						PRINTLN("CREATED A TEAM SPAWN BLIP AND DECAL ", i)	
						RETURN FALSE
					ENDIF
				ENDIF
			ENDFOR
			
			SET_RESET_STATE(RESET_STATE_OTHER)
			
		BREAK
		
		CASE RESET_STATE_OTHER
			PRINTLN("RESET_STATE_OTHER")
			
			g_FMMC_STRUCT.iMissionType = FMMC_TYPE_SURVIVAL
			
			REQUEST_STREAMED_TEXTURE_DICT("MPMissMarkers256")
			IF HAS_STREAMED_TEXTURE_DICT_LOADED("MPMissMarkers256")
				
				HAS_PATCHED_START_LOGO_STATE_SWAPPED(TRUE, 3)
			
				IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.vStartPos)
					SET_ALL_MENU_ITEMS_ACTIVE()
					
					CREATE_TRIGGER_BLIP_AND_CORONA_WITH_DECAL(sStartEndBlips.biStart, sHCS.hcStartCoronaColour, sStartEndBlips.ciStartType)
					
				ELSE
					SET_STARTING_MENUS_ACTIVE()
				ENDIF
				
				SET_RESET_STATE(RESET_STATE_FINISH)
			ENDIF
			
		BREAK
		CASE RESET_STATE_FINISH
			PRINTLN("RESET_STATE_FINISH")
			RETURN TRUE
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

PROC WARP_CAMERA_TO_START_LOCATION(FMMC_CAM_DATA &sCamDataPassed, FLOAT fHeight = 40.0)
	IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.vStartPos)
		IF DOES_CAM_EXIST(sCamDataPassed.cam)
			SET_CAM_COORD(sCamDataPassed.cam, g_FMMC_STRUCT.vStartPos+<<0.0,0.0, fHeight>>)
		ENDIF
	ELIF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[0][0].vPos)
		IF DOES_CAM_EXIST(sCamDataPassed.cam)
			SET_CAM_COORD(sCamDataPassed.cam, g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[0][0].vPos+<<0.0,0.0, fHeight>>)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL PREPARE_CONTENT_TO_BE_SAVED_OR_PUBLISHED()
	SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciSavedWithNewWeaponData)
	
	sCurrentVarsStruct.creationStats.bSuccessfulSave = FALSE
	
	PRINTLN("PRE PUBLISH/SAVE DONE. Time to go for it")
	
	IF g_FMMC_STRUCT.iInteriorBS != 0
	OR g_FMMC_STRUCT.iInteriorBS2 != 0
		SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciUSE_TIGHT_INTRO_CAM)
	ENDIF
	
	SET_BIT(sSurvTutorial.iTutorialBS, ciCREATOR_TUTORIAL__CONTENT_SAVED)
	
	RETURN TRUE
ENDFUNC

PROC SET_CORONA_ENTITIES_TO_DUMMY_MODELS()
	INT i
	FOR i = g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles TO FMMC_MAX_VEHICLES - 1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn != DUMMY_MODEL_FOR_SCRIPT
			g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn = DUMMY_MODEL_FOR_SCRIPT
			PRINTLN("SET TO DUMMY VEHICLE MODEL ", i)
		ENDIF
	ENDFOR
	FOR i = g_FMMC_STRUCT_ENTITIES.iNumberOfProps TO GET_FMMC_MAX_NUM_PROPS() - 1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn != DUMMY_MODEL_FOR_SCRIPT
			g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn = DUMMY_MODEL_FOR_SCRIPT
			PRINTLN("SET TO DUMMY PROP MODEL ", i)
		ENDIF
	ENDFOR
	FOR i = g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps TO FMMC_MAX_NUM_DYNOPROPS - 1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].mn != DUMMY_MODEL_FOR_SCRIPT
			g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[i].mn = DUMMY_MODEL_FOR_SCRIPT
			PRINTLN("SET TO DUMMY DYNOPROP MODEL ", i)
		ENDIF
	ENDFOR
ENDPROC

PROC RESET_TESTING_CONTENT_MENU_DATA(STRUCT_TEST_MENU_DATA &structMenuData)

	STRUCT_TEST_MENU_DATA sTemp
	structMenuData = sTemp
	
	NET_PRINT("Survival Creator - RESET_TESTING_CONTENT_MENU_DATA() has been called.")NET_NL()
	
ENDPROC

FUNC BOOL DISPLAY_END_OF_TEST_MENU()
	
	STRING sDescLabel = "SC_ENDTEST"
	
	STRING sHeader = "FMMC_ENDDM"
	
	
	IF g_iSurvivalEndReason = 1
		sHeader = "FMMC_END_F"
		IF IS_SPAWN_POINT_TEST()
			sDescLabel = "SC_SP_TEST_F"
		ELSE
			sDescLabel = "SC_TEST_F"
		ENDIF
	ELSE
		IF IS_SPAWN_POINT_TEST()
		AND g_iSurvivalEndReason = -1
			sDescLabel = "SC_SPWN_TEST"
			SET_BIT(g_FMMC_STRUCT.sSurvivalWaveData.iTestedWavesBitSet, 0)
			SET_BIT(g_FMMC_STRUCT.biTeamTestComplete, 0)
		ENDIF
	ENDIF
	
	SET_WARNING_MESSAGE_WITH_HEADER(sHeader, sDescLabel, FE_WARNING_OK,"",FALSE,-1,"","",TRUE )
	
	IF iTestAlertStartFrame < 0
		iTestAlertStartFrame = GET_FRAME_COUNT()
	ENDIF
	
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
	AND iTestAlertStartFrame < GET_FRAME_COUNT()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC DEAL_WITH_TEST_BEING_ACTIVE(STRUCT_TEST_MENU_DATA &structMenuData)

	IF HAS_RESUMED_FROM_SUSPEND()
	OR NETWORK_WAS_GAME_SUSPENDED()
	OR NOT NETWORK_IS_GAME_IN_PROGRESS()
		RESET_TESTING_CONTENT_MENU_DATA(structMenuData)
		REFRESH_MENU(sFMMCmenu)
		g_FMMC_STRUCT.g_b_QuitTest = TRUE
		DO_SCREEN_FADE_OUT(50)
		EXIT
	ENDIF
	TEXT_LABEL_15 tl15
	// Load menu.
	IF LOAD_MENU_ASSETS()
		
		// Process menu state.
		SWITCH structMenuData.eTestMcMissionMenuState
			
			// Wait for player to press up to activate menu. While waiting display help informing player they can do this.
			CASE eTESTINGMCMISSIONMENUSTATE_DISPLAY_HELP				
				
				IF NOT IS_PHONE_ONSCREEN()
					IF NOT structMenuData.bDisplayedHelp
						IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
							NET_PRINT("Survival Creator -called PRINT_HELP_FOREVER(MC_TEST3)")NET_NL()
							PRINT_HELP_FOREVER("MC_TEST")
							structMenuData.bDisplayedHelp = TRUE
							IF HAS_NET_TIMER_STARTED(tdNoHelpTimer)
								RESET_NET_TIMER(tdNoHelpTimer)
							ENDIF
						ENDIF
					ENDIF
					
					IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
						IF NOT HAS_NET_TIMER_STARTED(tdNoHelpTimer)
							REINIT_NET_TIMER(tdNoHelpTimer)
						ELIF HAS_NET_TIMER_EXPIRED(tdNoHelpTimer, 5000)
							structMenuData.bDisplayedHelp = FALSE
						ENDIF
					ENDIF
					
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
					AND IS_GAMEPLAY_CAM_RENDERING()
					AND NOT IS_AIMING_THROUGH_SNIPER_SCOPE(PLAYER_PED_ID())
					AND IS_SCREEN_FADED_IN()
					AND NOT IS_SCREEN_FADING_OUT()
					AND NOT IS_PAUSE_MENU_ACTIVE()
						DISABLE_CELLPHONE(TRUE)
						structMenuData.eTestMcMissionMenuState = eTESTINGMCMISSIONMENUSTATE_SETUP_MENU
						NET_PRINT("Survival Creator - pressed INPUT_FRONTEND_UP, going to eTESTINGMCMISSIONMENUSTATE_SETUP_MENU")NET_NL()
					ENDIF
				ELSE
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MC_TEST")
						CLEAR_HELP()
						structMenuData.bDisplayedHelp = FALSE
					ENDIF
				ENDIF
				
			BREAK
			
			// Setup the menu.
			CASE eTESTINGMCMISSIONMENUSTATE_SETUP_MENU
				
				CLEAR_MENU_DATA()
				tl15 = "FMMCC_TTITLE"
				IF !IS_SPAWN_POINT_TEST()
					tl15 = "SC_PREV_T"
				ENDIF
				SET_MENU_TITLE(tl15)
				SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT, MENU_ITEM_TEXT)
				SET_MENU_ITEM_JUSTIFICATION(FONT_LEFT, FONT_RIGHT)
				tl15 = "FMMCC_TEST2"
				IF !IS_SPAWN_POINT_TEST()
					tl15 = "SC_E_PREV_T"
				ENDIF
				ADD_MENU_ITEM_TEXT(0, tl15)

				structMenuData.eTestMcMissionMenuState = eTESTINGMCMISSIONMENUSTATE_SHOW_MENU
				
				NET_PRINT("Survival Creator - menu setup, going to eTESTINGMCMISSIONMENUSTATE_SHOW_MENU")NET_NL()
				
			BREAK
			
			// While menu is showing, wait for inputs from player to either exit the menu or exit testing the mission.
			CASE eTESTINGMCMISSIONMENUSTATE_SHOW_MENU
				tl15= "FMMCC_EXTEST"
				IF !IS_SPAWN_POINT_TEST()
					tl15 = "SC_E_PREV_H"
				ENDIF
				SET_CURRENT_MENU_ITEM_DESCRIPTION(tl15)
				// Draw the menu.
				DRAW_MENU()
				
				IF NOT IS_PAUSE_MENU_ACTIVE() //BC: 1789172 where the exit test menu buttons are listening with the pause menu active. 
					// Check for input.
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
					OR FMMC_IS_CANCEL_JUST_PRESSED(sFMMCmenu)
						DISABLE_CELLPHONE(FALSE)
						RESET_TESTING_CONTENT_MENU_DATA(structMenuData)
						NET_PRINT("Survival Creator - pressed INPUT_FRONTEND_UP, going to eTESTINGMCMISSIONMENUSTATE_DISPLAY_HELP")NET_NL()
					ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
						RESET_TESTING_CONTENT_MENU_DATA(structMenuData)
						REFRESH_MENU(sFMMCmenu)
						g_FMMC_STRUCT.g_b_QuitTest = TRUE
						NET_PRINT("Survival Creator - pressed INPUT_FRONTEND_ACCEPT, setting g_FMMC_STRUCT.g_b_QuitTest = TRUE.")NET_NL()
					ENDIF
				ENDIF
				
			BREAK
			
		ENDSWITCH
		
	ELSE
		NET_PRINT("Survival Creator - waiting for LOAD_MENU_ASSETS() to return TRUE.")NET_NL()
	ENDIF
	
	DISABLE_CELLPHONE_THIS_FRAME_ONLY()
	DISABLE_INTERACTION_MENU()
	CREATOR_DOORS_SETUP(iDoorSlowLoop,iDoorSetupStage, FMMC_TYPE_SURVIVAL)
	
ENDPROC

PROC DELETE_CREATOR_LOCAL_ENTITIES()
	IF DOES_BLIP_EXIST(biPlayArea)
		REMOVE_BLIP(biPlayArea)
	ENDIF
	INT i
	FOR i = 0 TO MAX_SURVIVAL_SPAWN_POINTS-1
		IF DOES_BLIP_EXIST(sTeamSpawnStruct[0].biPedBlip[i])
			REMOVE_BLIP(sTeamSpawnStruct[0].biPedBlip[i])
		ENDIF
	ENDFOR
	
	IF DOES_BLIP_EXIST(bCameraTriggerBlip)
		REMOVE_BLIP(bCameraTriggerBlip)
	ENDIF
	IF DOES_BLIP_EXIST(bCameraPanBlip)
		REMOVE_BLIP(bCameraPanBlip)
	ENDIF
	IF DOES_BLIP_EXIST(bPhotoBlip)
		REMOVE_BLIP(bPhotoBlip)
	ENDIF
ENDPROC

PROC MAINTAIN_TEST_MODE_STATE()
	IF IS_BIT_SET(sFMMCdata.iBitSet, biSetUpTestMission)
		bTextSetUp = FALSE
		IF NOT IS_SCREEN_FADED_OUT()
		AND NOT IS_SCREEN_FADING_OUT()
			DO_SCREEN_FADE_OUT(500)
			DELETE_CREATOR_LOCAL_ENTITIES()
			SET_TEAM_SPAWN_VEHICLE_DATA_FROM_CREATOR_SELECTION(sVehStruct)
			IF DOES_ENTITY_EXIST(sCurrentVarsStruct.viMapEscapeVeh)
				PRINTLN("MAINTAIN_TEST_MODE_STATE - Cleaning up viMapEscapeVeh")
				DELETE_VEHICLE(sCurrentVarsStruct.viMapEscapeVeh)
			ENDIF
			COMMON_FMMC_SCRIPT_CLEAN_UP(sPedStruct,	sVehStruct,	sWepStruct,	sObjStruct,sPropStruct, sDynoPropStruct, sInvisibleObjects, sCurrentVarsStruct, sFMMCmenu, FALSE)			
			REMOVE_TEMP_ENTITES_IF_NOT_NEEDED(sFMMCmenu.iEntityCreation)
			SET_CORONA_ENTITIES_TO_DUMMY_MODELS()
		ENDIF
		IF IS_SCREEN_FADED_OUT()
			IF IS_FAKE_MULTIPLAYER_MODE_SET() = FALSE
				SET_FAKE_MULTIPLAYER_MODE(TRUE)
			ENDIF
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("FM_Survival_Controller")) = 0
				REQUEST_SCRIPT("FM_Survival_Controller")				
				IF HAS_SCRIPT_LOADED("FM_Survival_Controller")
				AND IS_FAKE_MULTIPLAYER_MODE_SET()	
				AND NETWORK_IS_GAME_IN_PROGRESS()
					GlobalplayerBD[0].iGameState = MAIN_GAME_STATE_RUNNING
					START_NEW_SCRIPT("FM_Survival_Controller", MULTIPLAYER_FREEMODE_STACK_SIZE)
					PRINTLN("----------------------------------------------------------------------------")
					PRINTLN("--                                                                        --")
					PRINTLN("--                                                                        --")
					PRINTLN("--                                                                        --")
					PRINTLN("--                   LAUNCHING SURVIVAL CONTROLLER                        --")
					PRINTLN("--                                                                        --")
					PRINTLN("--                                                                        --")
					PRINTLN("--                                                                        --")
					PRINTLN("----------------------------------------------------------------------------")
					SET_SCRIPT_AS_NO_LONGER_NEEDED("FM_Survival_Controller")
					
					SET_BIT(sFMMCdata.iBitSet, biSetUpTestMission)
					iDoorSetupStage = 0
					REMOVE_SCENARIO_BLOCKING_AREAS()
					SET_MOBILE_RADIO_ENABLED_DURING_GAMEPLAY(FALSE)
					SET_MOBILE_PHONE_RADIO_STATE(FALSE)
					IF NOT IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
						SWAP_CAMERAS(sCamData, sFMMCdata, sFMMCMenu, sCurrentVarsStruct, TRUE, TRUE)
					ENDIF
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
						SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)
						SET_PED_DIES_IN_WATER(PLAYER_PED_ID(), FALSE)
						SET_ENTITY_PROOFS(PLAYER_PED_ID(), TRUE, TRUE, TRUE, TRUE, TRUE)
					ENDIF
					CLEAR_BIT(sFMMCdata.iBitSet, biSetUpTestMission)
					SET_CREATOR_AUDIO(FALSE, FALSE)
					g_FMMC_STRUCT.g_b_QuitTest = FALSE
					bMpNeedsCleanedUp = TRUE
					bTextSetUp = FALSE
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF NETWORK_IS_GAME_IN_PROGRESS()
			IF bTextSetUp = FALSE
				Initialise_MP_Objective_Text()
				Initialise_MP_Communications()
				NET_PRINT("init objective text and comms called") NET_NL()
				bTextSetUp = TRUE
			ENDIF
			SET_PED_UNABLE_TO_DROWN(PLAYER_PED_ID())
		ENDIF
		IF bTestModeControllerScriptStarted = FALSE
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("FM_Survival_Controller")) >= 1
				PRINTLN("----------------------------------------------------------------------------")
				PRINTLN("--                                                                        --")
				PRINTLN("--       GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH          --")
				PRINTLN("--                                                                        --")
				PRINTLN("----------------------------------------------------------------------------")
				bTestModeControllerScriptStarted = TRUE
				IF DOES_BLIP_EXIST(sStartEndBlips.biStart)
					REMOVE_BLIP(sStartEndBlips.biStart)
				ENDIF
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
				ENDIF
				DISABLE_CELLPHONE(FALSE)
			ENDIF
		ELSE
			IF bMpNeedsCleanedUp = TRUE
				IF bMpModeCleanedUp = FALSE
					// Do menu here.
					DEAL_WITH_TEST_BEING_ACTIVE(structTestMcMissionMenuData)
					IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("FM_Survival_Controller")) = 0
						RESET_TESTING_CONTENT_MENU_DATA(structTestMcMissionMenuData)
						SET_FAKE_MULTIPLAYER_MODE(FALSE)
						IF IS_FAKE_MULTIPLAYER_MODE_SET() = FALSE
							IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
							AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
								SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
								SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
							ENDIF
							CLEANUP_LEADERBOARD_CAM()
							REFRESH_MENU(sFMMCmenu)
							bMpModeCleanedUp = TRUE
							// Turn off the leaderboard camera.
							CLEANUP_LEADERBOARD_CAM()
							ENABLE_INTERACTION_MENU()
							IF DOES_CAM_EXIST(g_TransitionSessionNonResetVars.sPostMissionCleanupData.ciLeaderBoardCam)
								IF IS_CAM_ACTIVE(g_TransitionSessionNonResetVars.sPostMissionCleanupData.ciLeaderBoardCam)
									IF IS_CAM_RENDERING(g_TransitionSessionNonResetVars.sPostMissionCleanupData.ciLeaderBoardCam)
										RENDER_SCRIPT_CAMS(FALSE, FALSE, 1000, FALSE, TRUE)
										SET_CAM_ACTIVE(g_TransitionSessionNonResetVars.sPostMissionCleanupData.ciLeaderBoardCam, FALSE)
									ENDIF
								ENDIF
								DESTROY_CAM(g_TransitionSessionNonResetVars.sPostMissionCleanupData.ciLeaderBoardCam, TRUE)
							ENDIF
							
							PRINTLN("----------------------------------------------------------------------------")
							PRINTLN("--                                                                        --")
							PRINTLN("--                       MODE CLEANED UP = TRUE                           --")
							PRINTLN("--                                                                        --")
							PRINTLN("----------------------------------------------------------------------------")
						ENDIF
					ENDIF
				ELSE
					IF NETWORK_IS_GAME_IN_PROGRESS() = FALSE
					
						IF IS_PAUSE_MENU_ACTIVE()
							SET_FRONTEND_ACTIVE(FALSE)
						ENDIF
						
						IF g_FMMC_STRUCT.g_b_QuitTest
						OR DISPLAY_END_OF_TEST_MENU()
							g_FMMC_STRUCT.iMissionType = FMMC_TYPE_SURVIVAL
							SETUP_FMMC_SCRIPT(sFMMCMenu, sHCS, sCurrentVarsStruct, TRUE)
							CLEAR_BIT(sFMMCdata.iBitSet, biTestMissionActive)
							CLEAR_BIT(sFMMCdata.iBitSet, bTestHelpTextDone)
							bMpNeedsCleanedUp = FALSE
							bTestModeControllerScriptStarted = FALSE
							bTextSetUp = FALSE
							bMpModeCleanedUp = FALSE
							SET_CREATOR_AUDIO(TRUE, FALSE)
							SET_ENTITY_CREATION_STATUS(STAGE_DEAL_WITH_RECREATING_MISSION)
							sCurrentVarsStruct.creationStats.iTimesTestedLoc++
							sCurrentVarsStruct.creationStats.bMadeAChange = FALSE	
							ANIMPOSTFX_STOP_ALL()
							SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
							SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
							SET_CREATOR_MENU_SELECTION(sFMMCmenu, 0)
							g_iSurvivalEndReason = -1
							g_bOnHorde = FALSE
						ENDIF
					ELSE
						PRINTLN("Survival creator - MAINTAIN_TEST_MISSION_STATE - NETWORK_IS_GAME_IN_PROGRESS() = TRUE")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_OPTION_HAVE_SELECT_BUTTON()
	SWITCH sFMMCmenu.sActiveMenu
		CASE eFmmc_MAIN_OPTIONS_BASE
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_SURVIVAL_TITLE
			OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_SURVIVAL_DESCRIPTION
			OR GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_SURVIVAL_PHOTO
				RETURN TRUE
			ENDIF
		BREAK
		CASE eFmmc_SURVIVAL_Wave_Config
			IF (GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_WAVE_BASE_LAND_VEH AND NOT IS_BIT_SET(g_FMMC_STRUCT.sSurvivalWaveData.iLandVehChangeBitset, sFMMCmenu.iSurvivalWaveSelection))
			OR (GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_WAVE_BASE_AIR_VEH AND NOT IS_BIT_SET(g_FMMC_STRUCT.sSurvivalWaveData.iAirVehChangeBitset, sFMMCmenu.iSurvivalWaveSelection))
			OR (GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_WAVE_BASE_MODEL AND NOT IS_BIT_SET(g_FMMC_STRUCT.sSurvivalWaveData.iPedModelChangeBitset, sFMMCmenu.iSurvivalWaveSelection))
			OR (GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_WAVE_BASE_RESET)
				RETURN TRUE
			ENDIF
		BREAK
		CASE eFmmc_SURVIVAL_WARNING_AREA
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_SURV_BOUNDS_CLEAR
			AND NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sSurvivalWaveData.vBounds[0])
				RETURN TRUE
			ENDIF
		BREAK
		CASE eFmmc_SURVIVAL_Land_Vehicle_Options
			BOOL bAllLOn, bAllLOff, bNotDefaultLand
			bAllLOn = CHECK_LAND_VEHICLES_FOR_ALL_WAVES()
			bAllLOff = CHECK_LAND_VEHICLES_FOR_ALL_WAVES(TRUE)
			bNotDefaultLand = g_FMMC_STRUCT.sSurvivalWaveData.iLandVehiclesOnWave != ciSURVIVAL_DEFAULT_VEH_BITSET
			IF (GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_SURVIVAL_LVEH_ELV AND !bAllLOn)
			OR (GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_SURVIVAL_LVEH_DLV AND !bAllLOff)
			OR (GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_SURVIVAL_LVEH_RLV AND bNotDefaultLand)
				RETURN TRUE
			ENDIF
		BREAK
		CASE eFmmc_SURVIVAL_Air_Vehicle_Options
			BOOL bAllAOn ,bAllAOff, bNotDefaultAir 
			bAllAOn = CHECK_AIR_VEHICLES_FOR_ALL_WAVES()
			bAllAOff = CHECK_AIR_VEHICLES_FOR_ALL_WAVES(TRUE)
			bNotDefaultAir = g_FMMC_STRUCT.sSurvivalWaveData.iAirVehiclesOnWave != ciSURVIVAL_DEFAULT_AIR_BITSET
			IF (GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_SURVIVAL_AVEH_EAV AND !bAllAOn)
			OR (GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_SURVIVAL_AVEH_DAV AND !bAllAOff)
			OR (GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_SURVIVAL_AVEH_RAV AND bNotDefaultAir)
				RETURN TRUE
			ENDIF
		BREAK
		CASE eFmmc_SURVIVAL_Defaults_Menu
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) != OPTION_SURVIVAL_DEFAULT_WAVES
				RETURN TRUE
			ENDIF
		BREAK
		CASE eFmmc_SURVIVAL_Copy_Wave
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_OPTION_HAVE_ADJUST_BUTTON()
	SWITCH sFMMCmenu.sActiveMenu
		CASE eFmmc_SURVIVAL_Spawn_Points
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = OPTION_SURV_SPAWN_PLACE
				RETURN FALSE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_A_PLACEMENT_OPTION()
	IF sFMMCmenu.sActiveMenu = eFmmc_SURVIVAL_WARNING_AREA
		RETURN CAN_PLACE_BOUNDS_ON_THIS_OPTION(sFMMCmenu)
	ELIF sFMMCmenu.sActiveMenu = eFmmc_SURVIVAL_ENEMY_AREA
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC BUTTON_HELP()

	REMOVE_MENU_HELP_KEYS()
	
	INT iBitSet
	
	IF NOT IS_BIT_SET(sFMMCdata.iBitSet, biTestMissionActive)
		
		//Left & Right
		IF IS_THIS_OPTION_SELECTABLE()
		AND IS_THIS_OPTION_TOGGLEABLE()
		AND NOT IS_THIS_OPTION_A_MENU_GOTO()
			IF SHOULD_OPTION_HAVE_ADJUST_BUTTON()
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_CYCLE_OPTIONS)
			ENDIF
		ENDIF
		
		//A
		IF IS_BIT_SET(iHelpBitSet, biPickupEntityButton)
		AND sFMMCMenu.iSelectedEntity = -1
			SET_BIT(iBitSet, FMMC_HELP_BUTTON_EDIT)
		ELSE
			IF IS_THIS_OPTION_SELECTABLE()
				IF IS_THIS_OPTION_A_MENU_GOTO()
				OR IS_THIS_OPTION_A_MENU_ACTION()
				OR SHOULD_OPTION_HAVE_SELECT_BUTTON()
					SET_BIT(iBitSet, FMMC_HELP_BUTTON_SELECT)
				ELIF sFMMCmenu.sActiveMenu = eFmmc_SURVIVAL_ENEMY_AREA
				OR sFMMCmenu.sActiveMenu = eFmmc_SURVIVAL_WARNING_AREA
					IF (sFMMCmenu.sActiveMenu = eFmmc_SURVIVAL_ENEMY_AREA AND NOT HAVE_ALL_SURVIVAL_ESPAWN_BEEN_PLACED())
					OR (sFMMCmenu.sActiveMenu = eFmmc_SURVIVAL_WARNING_AREA AND NOT HAS_SURVIVAL_BOUNDS_BEEN_PLACED())
					OR (sFMMCmenu.sActiveMenu = eFmmc_SURVIVAL_Vehicle_Spawn AND NOT HAVE_ALL_SURVIVAL_VEHICLE_SPAWNS_BEEN_PLACED())
					OR sFMMCMenu.iBoundsEditPointIndex > -1
						IF (sCurrentVarsStruct.bAreaIsGoodForPlacement
						AND IS_A_PLACEMENT_OPTION())
						OR sFMMCMenu.iBoundsEditPointIndex > -1
							SET_BIT(iBitSet, FMMC_HELP_BUTTON_PLACE)
						ENDIF
					ENDIF
				ELIF sFMMCmenu.sActiveMenu = eFmmc_MAIN_MENU_BASE
				OR sFMMCmenu.sActiveMenu = eFmmc_SURVIVAL_Spawn_Points
					IF sCurrentVarsStruct.bitsetOkToPlace = BS_CAN_PLACE
					AND sCurrentVarsStruct.bAreaIsGoodForPlacement
						SET_BIT(iBitSet, FMMC_HELP_BUTTON_PLACE)
					ENDIF
				ELIF NOT IS_LONG_BIT_SET(sCurrentVarsStruct.iTooManyEntities, ciDRAW_CORONA)
				OR sFMMCMenu.iSelectedEntity != -1
					IF sFMMCmenu.sActiveMenu != eFmmc_RADIO_MENU
					AND sFMMCmenu.sActiveMenu != eFmmc_SURVIVAL_Pickup_Placement_Choose_Weapon
					AND sFMMCmenu.sActiveMenu != eFmmc_SURVIVAL_Loadout_Weapon_Select
					AND sCurrentVarsStruct.bitsetOkToPlace = BS_CAN_PLACE
					AND sCurrentVarsStruct.bAreaIsGoodForPlacement
						SET_BIT(iBitSet, FMMC_HELP_BUTTON_PLACE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		//B
		IF sFMMCmenu.sMenuBack != eFmmc_Null_item
		OR sFMMCMenu.iBoundsEditPointIndex > -1
			IF sFMMCMenu.iSelectedEntity = -1
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_BACK)
			ELSE
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_CANCEL)
			ENDIF
		ENDIF
		//X
		IF IS_BIT_SET(iHelpBitSet, biDeleteEntityButton)
		AND CAN_DELETE_FROM_CURRENT_MENU(sFMMCMenu, sCurrentVarsStruct)
		AND sFMMCMenu.iSelectedEntity = -1
			SET_BIT(iBitSet, FMMC_HELP_BUTTON_DELETE)
		ENDIF
		
		IF GET_HIGHLIGHTED_WORLD_PROP_INDEX(sCurrentVarsStruct.vCoronaHitEntity, sCurrentVarsStruct.vCoronaPos) != -1
			SET_BIT(iBitSet, FMMC_HELP_BUTTON_DELETE)
		ENDIF
		
		IF sFMMCmenu.sActiveMenu = eFmmc_SURVIVAL_Pickup_Placement_Override
		AND g_FMMC_STRUCT_ENTITIES.sEditedWeapon.ptSurvival_WeaponOverridePerWave[GET_CREATOR_MENU_SELECTION(sFMMCmenu) + 1] != NUM_PICKUPS
			SET_BIT(iBitSet, FMMC_HELP_BUTTON_DELETE)
		ENDIF
		
		IF sCurrentVarsStruct.bBlockDelete
			CLEAR_BIT(iBitSet, FMMC_HELP_BUTTON_DELETE)
		ENDIF
		
		IF sFMMCmenu.sActiveMenu = eFmmc_SURVIVAL_Wave_Base
		AND IS_THIS_OPTION_SELECTABLE()
			SET_BIT(iBitSet, FMMC_HELP_BUTTON_DELETE)
		ENDIF
		
		//Y
		
		//Bumpers
		IF sFMMCmenu.sActiveMenu = eFmmc_SURVIVAL_Spawn_Points
		OR IS_SURVIVAL_MENU_BUMP_SWITCH_SQUAD(sFMMCmenu)
		OR IS_SURVIVAL_MENU_BUMP_SWITCH_WAVE(sFMMCmenu)
		OR sFMMCmenu.sActiveMenu = eFmmc_PROP_BASE
		OR sFMMCmenu.sActiveMenu = eFmmc_SURVIVAL_ENEMY_AREA
		OR sFMMCmenu.sActiveMenu = eFmmc_SURVIVAL_Vehicle_Spawn
		OR DOES_ENTITY_EXIST(pedSelection)
		OR DOES_ENTITY_EXIST(vehSelection)
		OR (sFMMCmenu.sActiveMenu = eFmmc_SURVIVAL_Wave_Base AND IS_THIS_OPTION_SELECTABLE() AND NOT g_bFMMC_TutorialSelectedFromMpSkyMenu)
			SET_BIT(iBitSet, FMMC_HELP_BUTTON_ROTATE)
		ENDIF
		//Triggers
		IF NOT IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
			SET_BIT(iBitSet, FMMC_HELP_BUTTON_ZOOM)
		ENDIF
		
		//Misc
		IF sFMMCmenu.iCurrentMenuLength > 1
			SET_BIT(iBitSet, FMMC_HELP_BUTTON_SCROLL)
		ENDIF
		IF (sCurrentVarsStruct.bitsetOkToPlace = BS_CAN_PLACE) AND (sFMMCmenu.sActiveMenu != eFmmc_RADIO_MENU)
		AND sFMMCmenu.sActiveMenu != eFmmc_PAN_CAM_BASE
		AND sFMMCmenu.sActiveMenu != eFmmc_PHOTO_CAM_BASE
			IF IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
				IF IS_BIT_SET(iHelpBitSet, biWarpToCameraButton) 
					SET_BIT(iBitSet, FMMC_HELP_BUTTON_SWITCH_CAM)
				ENDIF
			ELSE
				IF sFMMCMenu.iSelectedEntity = -1
					IF IS_BIT_SET(iHelpBitSet, biWarpToCoronaButton) 
						SET_BIT(iBitSet, FMMC_HELP_BUTTON_SWITCH_CAM)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF sFMMCmenu.sActiveMenu = eFmmc_PAN_CAM_BASE
		OR sFMMCmenu.sActiveMenu = eFmmc_PHOTO_CAM_BASE
			CLEAR_BIT(iBitSet, FMMC_HELP_BUTTON_ZOOM)
			SET_BIT(iBitSet, FMMC_HELP_BUTTON_CAMERA_HEIGHT)
		ENDIF
		
		IF sFMMCmenu.sActiveMenu = eFmmc_PHOTO_CAM_BASE
			SET_BIT(iBitSet, FMMC_HELP_BUTTON_CAMERA_FOV)
			IF DOES_CAM_EXIST(sCamData.cam)
			AND GET_CAM_FOV(sCamData.cam) != 40
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_RESET_FOV)		
			ENDIF
		ENDIF
		
		IF NOT FMMC_IS_CIRCLE_MENU_A_CAMERA_MENU(sFMMCmenu.sActiveMenu) 
			IF NOT IS_GAMEPLAY_CAM_RENDERING()
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_ROTATE_CAM_HOLD)		
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_MOVE_CAMERA)
			ENDIF
		ENDIF
		
		SET_UP_PROP_HELP_BUTTONS(sFMMCmenu, iBitSet, sCurrentVarsStruct)
			
		IF sFMMCmenu.sActiveMenu = eFmmc_TOP_MENU
			CLEAR_BIT(iBitSet, FMMC_HELP_BUTTON_BACK)
		ENDIF
		
		CREATE_FMMC_INSTRUCTIONAL_BUTTONS(sFMMCmenu, iBitSet)
	ELSE
		IF sFMMCmenu.sMenuBack != eFmmc_Null_item
			IF GET_CREATOR_MENU_SELECTION(sFMMCmenu) = 1 
				SET_BIT(iBitSet, FMMC_HELP_BUTTON_SELECT)
			ENDIF
			SET_BIT(iBitSet, FMMC_HELP_BUTTON_BACK)
			SET_BIT(iBitSet, FMMC_HELP_BUTTON_SCROLL)
			CREATE_FMMC_INSTRUCTIONAL_BUTTONS(sFMMCmenu, iBitSet)
		ENDIF
	ENDIF
ENDPROC

PROC UPDATE_PLACED_MODELS()
	IF IS_BIT_SET(sFMMCdata.iBitSet, biTestMissionActive)
		EXIT
	ENDIF
	IF sFMMCMenu.iSelectedEntity != -1
		EXIT
	ENDIF
	IF sCurrentVarsStruct.iEntityCreationStatus != STAGE_ENTITY_PLACEMENT_SETUP
		EXIT
	ENDIF

	INT i
	INT iWaveToUse = sFMMCMenu.iSurvivalWaveSelection
	IF iWaveToUse = -1
		iWaveToUse = 0
	ENDIF
	
	IF sFMMCmenu.sActiveMenu = eFmmc_SURVIVAL_Wave_Ped_Model
		IF IS_MENU_A_SURVIVAL_OVERRIDE_MENU(sFMMCmenu)
			sCurrentVarsStruct.mnCurrentSurvivalPed = g_FMMC_STRUCT.sSurvivalWaveData.mnPedModel[0][0]
		ELSE
			sCurrentVarsStruct.mnCurrentSurvivalPed = g_FMMC_STRUCT.sSurvivalWaveData.mnPedModel[iWaveToUse][sFMMCMenu.iSurvivalDiffSelection]
		ENDIF
	ENDIF
	
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfPeds-1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].mn != DUMMY_MODEL_FOR_SCRIPT
		AND sCurrentVarsStruct.mnCurrentSurvivalPed != DUMMY_MODEL_FOR_SCRIPT
			IF DOES_ENTITY_EXIST(sPedStruct.piPed[i])
				IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].mn != sCurrentVarsStruct.mnCurrentSurvivalPed
					g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].mn = sCurrentVarsStruct.mnCurrentSurvivalPed
					DELETE_PED(sPedStruct.piPed[i])
					PRINTLN("UPDATE_PLACED_MODELS - Updating ped ", i, " model")
				ENDIF
				IF IS_ENTITY_ALIVE(sPedStruct.piPed[i])
					IF HAS_NET_TIMER_EXPIRED(tdPedUpdate, ciPedUpdateTime)
					AND NOT IS_ENTITY_VISIBLE(sPedStruct.piPed[i])
						SET_ENTITY_VISIBLE(sPedStruct.piPed[i], TRUE)
						FREEZE_ENTITY_POSITION(sPedStruct.piPed[i], TRUE)
					ENDIF
				ENDIF
			ELSE
				REQUEST_MODEL(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].mn)
				IF HAS_MODEL_LOADED(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].mn)
					sPedStruct.piPed[i] = CREATE_PED(PEDTYPE_MISSION, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].mn, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].vPos, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].fHead,FALSE,FALSE)
					SET_ENTITY_VISIBLE(sPedStruct.piPed[i], FALSE)
					REINIT_NET_TIMER(tdPedUpdate)
					FREEZE_ENTITY_POSITION(sPedStruct.piPed[i], TRUE)
					INT iVariation = 1
					IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].mn = INT_TO_ENUM(MODEL_NAMES, HASH("U_M_Y_Juggernaut_01"))
						FMMC_SET_PED_VARIATION(sPedStruct.piPed[i], g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].mn, iVariation)
					ELIF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].mn = HC_GUNMAN
						iVariation = 4
						FMMC_SET_PED_VARIATION(sPedStruct.piPed[i], g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].mn, iVariation)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	IF SHOULD_MENU_SHOW_PREVIEW_VEHICLE(sFMMCMenu)
		IF IS_MENU_A_SURVIVAL_OVERRIDE_MENU(sFMMCmenu)
			sCurrentVarsStruct.mnCurrentSurvivalVeh = g_FMMC_STRUCT.sSurvivalWaveData.mnVeh[0][0]
		ELSE
			sCurrentVarsStruct.mnCurrentSurvivalVeh = g_FMMC_STRUCT.sSurvivalWaveData.mnVeh[iWaveToUse][sFMMCMenu.iSurvivalVehicleSelection]
		ENDIF
	ENDIF
	
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles-1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn != DUMMY_MODEL_FOR_SCRIPT
		AND sCurrentVarsStruct.mnCurrentSurvivalVeh != DUMMY_MODEL_FOR_SCRIPT
			IF DOES_ENTITY_EXIST(sVehStruct.veVehcile[i])
				INT iVehToUse = 0
				IF sFMMCMenu.iSurvivalVehicleSelection != -1
					iVehToUse = sFMMCMenu.iSurvivalVehicleSelection
				ENDIF
				SURVIVAL_SET_VEHICLE_COLOUR(sVehStruct.veVehcile[i], iWaveToUse, iVehToUse, FALSE)
				IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn != sCurrentVarsStruct.mnCurrentSurvivalVeh 
					g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn = sCurrentVarsStruct.mnCurrentSurvivalVeh
					DELETE_VEHICLE(sVehStruct.veVehcile[i])
					PRINTLN("UPDATE_PLACED_MODELS - Updating Vehicle ", i, " model")
				ENDIF
				
			ELSE
				REQUEST_MODEL(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn)
				IF HAS_MODEL_LOADED(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn)
					CREATE_VEHICLE_FMMC(sVehStruct.veVehcile[i], i, FALSE)
					SET_VEHICLE_ON_GROUND_PROPERLY(sVehStruct.veVehcile[i])
					IF GET_ENTITY_MODEL(sVehStruct.veVehcile[i]) = POLMAV
						SET_VEHICLE_LIVERY(sVehStruct.veVehcile[i], 0) //Keeps it as a police vehicle rather than an Air Ambulance
					ENDIF
					SET_BLIP_NAME_FROM_TEXT_FILE(sVehStruct.biVehicleBlip[i], "SC_BLP_VSP")
					IF IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
						IF DOES_ENTITY_EXIST(sVehStruct.veVehcile[i])
							VECTOR vVehLoc, vPlayerLoc
							vVehLoc = GET_ENTITY_COORDS(sVehStruct.veVehcile[i])
							vPlayerLoc = GET_ENTITY_COORDS(PLAYER_PED_ID())
							IF VDIST_2D(vVehLoc, vPlayerLoc) < 1
								sCurrentVarsStruct.iMenuState = MENU_STATE_SWITCH_CAM
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
ENDPROC

PROC HANDLE_RETEST_FLAGGED()
	
	IF iNumberOfPedsLastFrame > 0
		IF g_FMMC_STRUCT_ENTITIES.iNumberOfPeds > iNumberOfPedsLastFrame
			sFMMCendStage.bRetestNeededWithChange = TRUE
			PRINTLN("Setting sFMMCendStage.bRetestNeededWithChange to TRUE, Number of peds has changed")
		ENDIF
	ENDIF
	
	iNumberOfPedsLastFrame = g_FMMC_STRUCT_ENTITIES.iNumberOfPeds
ENDPROC

PROC HANDLE_WAVE_RANDOMISATION()
	IF IS_BIT_SET(sFMMCMenu.iFMMCMenuBitset, ciFMMCMenuBS_TriggerSurvivalRandomize)
		RANDOMISE_WAVE(sFMMCMenu)
		REFRESH_MENU_ASAP(sFMMCMenu)
		CLEAR_BIT(sFMMCMenu.iFMMCMenuBitset, ciFMMCMenuBS_TriggerSurvivalRandomize)
	ENDIF
ENDPROC

SCRIPT

	IF IS_PAUSE_MENU_REQUESTING_TO_EDIT_A_MISSION()
		PRINTLN("[JA@PAUSEMENU] Set up which file to load into creator", GET_PAUSE_MENU_MISSION_FILE_TO_LOAD())

		sFMMCendStage.iLoadDeleteStage  = ciLOAD_DELETE_STAGE_LOAD
		sFMMCendStage.iMenuReturn		= GET_PAUSE_MENU_MISSION_FILE_TO_LOAD()
	ENDIF
	CLEAN_UP_PAUSE_MENU_MISSION_CREATOR_DATA()
	
	PROCESS_PRE_GAME()	
	
	IF (HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_SP_TO_MP))
		SCRIPT_CLEANUP(TRUE)
	ENDIF	
	
	// Used for the model memory budget. Must be initiated at the start.
	INIT_CREATOR_BUDGET()
	
	sCurrentVarsStruct.creationStats.iStartTimeMS = GET_GAME_TIMER()
	sCurrentVarsStruct.creationStats.iCurrentTimeMS = GET_GAME_TIMER()
	
	sFMMCmenu.bAllowMouseInput = TRUE
	sFMMCmenu.bAllowMouseSelection = TRUE

	#IF IS_DEBUG_BUILD 
	INITIALIZE_ADDITIONAL_DEBUG_AND_WIDGETS()
	#ENDIF	
	
	// Main loop.
	WHILE TRUE	
	
		#IF IS_DEBUG_BUILD
		TEXT_LABEL_63 tl63
		
		IF sFMMCmenu.iSurvivalWaveSelection != -1
		
		tl63 = "Wave: "
		tl63 += sFMMCmenu.iSurvivalWaveSelection
		DRAW_DEBUG_TEXT_2D(tl63, <<0.7,0.80,0.0>>)
		
		tl63 = "Weapon: "
		tl63 += sFMMCmenu.iSurvivalWeaponSelection 
		DRAW_DEBUG_TEXT_2D(tl63, <<0.7,0.82,0.0>>)
		
		tl63 = "sFMMCmenu.iWepLibrary: "
		tl63 += sFMMCmenu.iWepLibrary
		DRAW_DEBUG_TEXT_2D(tl63, <<0.7,0.84,0.0>>)
		
		tl63 = "sWepStruct.iSelectedWeaponType: "
		tl63 += sWepStruct.iSelectedWeaponType
		DRAW_DEBUG_TEXT_2D(tl63, <<0.7,0.86,0.0>>)
		
		tl63 = "sSubtypeName: "
		tl63 += sFMMCmenu.sSubTypeName[CREATION_TYPE_WEAPONS]
		DRAW_DEBUG_TEXT_2D(tl63, <<0.7,0.88,0.0>>)
		
		tl63 = "Gun Type: "
		tl63 += GET_WEAPON_NAME_FROM_WEAPON_TYPE(sWepStruct.wtGunType[sWepStruct.iSelectedWeaponType])
		DRAW_DEBUG_TEXT_2D(tl63, <<0.7,0.90,0.0>>)
		ENDIF
		
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_NUMPAD5, KEYBOARD_MODIFIER_NONE, "")
			SET_ALL_WAVES_TESTED()
			SET_BIT(g_FMMC_STRUCT.biTeamTestComplete, 0)
			REFRESH_MENU_ASAP(sFMMCMenu)
		ENDIF
			
		#ENDIF
		
		PROCESS_SINGLEPLAYER_MODEL_ON_CREATOR(bIsCreatorModelSetSPTC, bIsMaleSPTC)
		FMMC_FAKE_LEFT_AND_RIGHT_INPUTS_FOR_MENUS(sFMMCmenu)
		
		sCurrentVarsStruct.bDiscVisible = FALSE
		sCurrentVarsStruct.bCanCreateADecalThisFrame = TRUE
		
		WAIT(0)
		
		PROCESS_ON_DEMAND_MENU_REFRESHING(sFMMCmenu)
		
		BUTTON_HELP()
		
		IF NOT bTestModeControllerScriptStarted
			REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
			PROCESS_CREATOR_EVENTS(sCurrentVarsStruct, sFMMCmenu)
			DISPLAY_ALL_CURRENT_ROCKSTAR_CREATED(sFMMCendStage.sRocStarCreatedVars, sGetUGC_content)
			DEAL_WITH_AMBIENT_AND_HUD()
			
			
			HANDLE_TEST_MODE_AVAILABLE()
			HANDLE_SAVE_AVAILABLE()
			HANDLE_FAIL_REASONS()
			HANDLE_PUBLISH_AVAILABLE()
			CHECK_SURVIVAL_NEEDS_RETEST(sFMMCendStage, sFMMCMenu)
			HANDLE_SURVIVAL_TEST_AS_COMPLETED()
//			HANDLE_BLOCKING_MODEL_OPTIONS(sFMMCmenu)
			UPDATE_PLACED_MODELS()
			HANDLE_RETEST_FLAGGED()
			CLEANUP_SELECTED_MODEL_PREVIEW_IF_NEEDED()
			HANDLE_WAVE_RANDOMISATION()
			
			IF IS_ROCKSTAR_DEV()
				IF IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions, IPL_CARRIER)
				AND NOT bMultiplayerMapSet
					PRINTLN("SURVIVAL_CREATOR - bMultiplayerMapSet - swapping map changesets")
					SET_MP_MAP_LOADED(TRUE)
					//REVERT_CONTENT_CHANGESET_GROUP_FOR_ALL(HASH("GROUP_MAP_SP"))
					//EXECUTE_CONTENT_CHANGESET_GROUP_FOR_ALL(HASH("GROUP_MAP"))
					bMultiplayerMapSet = TRUE
				ENDIF
				HANDLE_REQUESTED_IPL(IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions, 	IPL_CARRIER), "hei_carrier" )
			ENDIF
			
			DO_BLIMP_SIGNS(sBlimpSign, sCurrentVarsStruct.iMenuState = STAGE_DEAL_WITH_FINISHING)
			
			//Keyboard and mouse
			FMMC_HANDLE_KEYBOARD_AND_MOUSE(sFMMCmenu, NOT IS_BIT_SET(sFMMCdata.iBitSet, biTestMissionActive) OR (bShowMenu), bShowMenuHighlight AND bShowMenu)
			// Switch help from mouse and keyboard to joypad if we change controls
			IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL) != bLastUseMouseKeyboard
				bLastUseMouseKeyboard = IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
			ENDIF
			
			
			
			IF IS_PAUSE_MENU_REQUESTING_TRANSITION()
				IF sCurrentVarsStruct.iEntityCreationStatus != STAGE_DEAL_WITH_FINISHING
					SET_ENTITY_CREATION_STATUS(STAGE_DEAL_WITH_FINISHING)
				ENDIF
			ENDIF
			
			IF IS_TRANSITION_TELLING_CREATOR_TO_CLEANUP()
				IF sCurrentVarsStruct.iEntityCreationStatus != STAGE_DEAL_WITH_FINISHING
					SET_ENTITY_CREATION_STATUS(STAGE_DEAL_WITH_FINISHING)
				ENDIF
			ENDIF
			
			SWITCH sCurrentVarsStruct.iEntityCreationStatus
				CASE STAGE_CHECK_TO_LOAD_CREATION
					IF g_bFMMC_LoadFromMpSkyMenu
						IF LOAD_A_MISSION_INTO_THE_CREATOR(sGetUGC_content, sFMMCendStage, g_sFMMC_LoadedMission)
							sFMMCmenu.iForcedWeapon = GET_CREATOR_WEAPON_INDEX_FROM_WEAPON_TYPE(WEAPONTYPE_PISTOL)
							IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.vStartPos)
								sCamData.camPos = g_FMMC_STRUCT.vStartPos + <<0,-0.5, 20>>
							ENDIF
							
							SET_ENTITY_CREATION_STATUS(STAGE_DEAL_WITH_LOADING)
						ENDIF
					ELSE
						SET_ENTITY_CREATION_STATUS(STAGE_DEAL_WITH_LOADING)
					ENDIF
				BREAK
				
				CASE STAGE_DEAL_WITH_LOADING
					IF SET_SKYSWOOP_UP(TRUE)
						IF RESET_THE_MISSION_UP_SAFELY()
							SET_RESET_STATE(RESET_STATE_FADE)
							IF sFMMCmenu.iRadioState = FMMC_RADIO_MAX_OPTIONS_STATE
								SET_MOBILE_PHONE_RADIO_STATE(TRUE)
							ENDIF			
							CLEAR_HELP()
							CLEAR_BIT(sFMMCdata.iBitSet, biTestMissionActive)
							CLEAR_BIT(sFMMCdata.iBitSet, biSetUpTestMission)	
							sCurrentVarsStruct.creationStats.bEditingACreation = true
							IF g_FMMC_STRUCT.bMissionIsPublished
								sCurrentVarsStruct.creationStats.bMadeAChange = FALSE
								SET_BIT(sFMMCmenu.iTopMenuActive, SURV_TOP_MENU_TEST)
							ENDIF
							//Clean up
							REFRESH_MENU(sFMMCMenu)
							SET_ENTITY_CREATION_STATUS(STAGE_ENTITY_PLACEMENT_LOAD)
							sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
							ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, TRUE)
							ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, TRUE)
							iHelpBitSetOld = -1

							WARP_CAMERA_TO_START_LOCATION(sCamData, 110.0)	
							
							IF IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciMISSION_OPTION_BS_VOTING_DISABLED) 
								CLEAR_BIT(g_FMMC_STRUCT.iOptionsMenuBitSet, ciVOTING_ENABLED)
							ELSE
								SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSet, ciVOTING_ENABLED)
							ENDIF
							
							DO_SCREEN_FADE_IN(500)
							
							IF IS_LOADING_ICON_ACTIVE()
								SET_LOADING_ICON_INACTIVE()
							ENDIF
						ELSE
							PRINTLN("RESET_THE_MISSION_UP_SAFELY = FALSE")
						ENDIF
					ENDIF
				BREAK
				
				CASE STAGE_ENTITY_PLACEMENT_LOAD
					IF HAS_ADDITIONAL_TEXT_LOADED(ODDJOB_TEXT_SLOT)
						IF LOAD_MENU_ASSETS()
							MENU_INITIALISATION()
							SET_ENTITY_CREATION_STATUS(STAGE_CG_TO_NG_WARNING)
						ENDIF
					ENDIF
				BREAK
				
				CASE STAGE_CG_TO_NG_WARNING
					IF g_FMMC_STRUCT.bIsUGCjobNG
					OR IS_STRING_EMPTY(g_FMMC_STRUCT.tl63MissionName)			
					OR DRAW_CG_TO_NG_WARNING()
						SET_ENTITY_CREATION_STATUS(STAGE_ENTITY_PLACEMENT_SETUP)
					ENDIF
				BREAK
				
				CASE STAGE_ENTITY_PLACEMENT_SETUP
					IF g_TurnOnCreatorHud
						CONTROL_CAMERA_AND_CORONA()	
						HANDLE_MOUSE_ENTITY_SELECTION(IS_BIT_SET(sFMMCdata.iBitSet, biTestMissionActive))
					ENDIF
					
					IF bRunTutorial
						IF PROCESS_TUTORIAL(sSurvTutorial, sFMMCmenu, sCurrentVarsStruct, bShowMenu, sFMMCdata)
							bRunTutorial = FALSE
							g_bFMMC_TutorialSelectedFromMpSkyMenu = FALSE
						ENDIF
					ENDIF
					
					IF IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
					OR IS_BIT_SET(sFMMCdata.iBitSet, biTestMissionActive)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPECIAL_ABILITY)
					ENDIF
					
					IF NOT IS_BIT_SET(sFMMCdata.iBitSet, biTestMissionActive) 
					AND NOT IS_BIT_SET(sFMMCdata.iBitSet, biSetUpTestMission)
						#IF IS_DEBUG_BUILD												
						// [LM][CopyAndPaste] - Needs to be above the MAINTAIN placed entity kind of functions.
						PROCESS_COPY_PASTE_FRONTEND_LOGIC(sCurrentVarsStruct)
						sFMMCMenu.iCopyFromEntity = -1
						sFMMCMenu.iCopyFromEntityTeam = -1
						#ENDIF
						
						UPDATE_SWAP_CAM_STATUS(sCurrentVarsStruct, IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround))
						RESET_MAINTAIN_PLACED_ENTITY_VARIABLES(iLocalBitSet, sCurrentVarsStruct)
						MAINTAIN_PLACED_MARKERS()
						MAINTAIN_PLACED_ENTITIES(sFMMCmenu, sHCS, sCurrentVarsStruct, sPedStruct, sVehStruct, sWepStruct, sObjStruct, sLocStruct, sTrainStruct, sPropStruct, sDynoPropStruct, sInvisibleObjects, sFMMCdata, g_CreatorsSelDetails, sFMMCendStage, sCamData, iLocalBitSet)
						MAINTAIN_BLIPS_FOR_PLACED_AREAS()
						MAINTAIN_PLACED_TEAM_SPAWNPOINTS(sTeamSpawnStruct[0], 0, sInvisibleObjects, sHCS, sCurrentVarsStruct, sFMMCmenu, sFMMCdata, iLocalBitSet, sFMMCendStage, sVehStruct)
						MAINTAIN_WORLD_PROPS(sFMMCmenu,sCurrentVarsStruct, iLocalBitSet)
						#IF IS_DEBUG_BUILD
						MAINTAIN_PLACED_DOORS(biDoors, sCurrentVarsStruct, iLocalBitSet, sFMMCmenu)
						MAINTAIN_PLACED_ZONES(sCurrentVarsStruct, iLocalBitSet, sFMMCmenu)
						#ENDIF
					ENDIF
					
					DEAL_WITH_SETTING_HEIGHT_ON_ENTITY(sFMMCmenu, sCamData, sCurrentVarsStruct, IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround), CAN_HEIGHT_BE_SET_FOR_ENTITY(sFMMCmenu, sFMMCmenu.iEntityCreation, FALSE))
					REMOVE_TEMP_ENTITES_IF_NOT_NEEDED(sFMMCmenu.iEntityCreation)
					
					HANDLE_MENU_STATE()
					
					BOOL bHideMarker
					IF sFMMCmenu.sActiveMenu = eFmmc_PAN_CAM_BASE
					OR sFMMCmenu.sActiveMenu = eFmmc_COVER_POINT_BASE
					OR sFMMCmenu.sActiveMenu = eFmmc_PHOTO_CAM_BASE
						bHideMarker = TRUE
					ELSE
						bHideMarker = FALSE
					ENDIF
					
					IF sFMMCmenu.sActiveMenu = eFmmc_RADIO_MENU
						sCurrentVarsStruct.bDiscVisible = FALSE
					ENDIF
					MAINTAIN_PLACEMENT_DISC(sCurrentVarsStruct, sFMMCmenu, iLocalBitSet, sFMMCdata.iBitSet, bHideMarker, sFMMCmenu.iEntityCreation = -1)
					
					BLIP_INDEX biFurthestBlip
					IF sFMMCmenu.bZoomedOutRadar
						biFurthestBlip = GET_FURTHEST_BLIP_TO_PLAYER(sPedStruct.biPedBlip, sVehStruct.biVehicleBlip, sWepStruct.biWeaponBlip, sPropStruct.biObject, sDynoPropStruct.biObject, sStartEndBlips.biStart, bCameraPanBlip, sCurrentVarsStruct.biLocateBlip)
					ENDIF
					IF IS_BIT_SET(sFMMCdata.iBitSet, biTestMissionActive)
						CONTROL_PLAYER_BLIP(TRUE, sCurrentVarsStruct, sFMMCmenu.iEntityCreation, TRUE, biFurthestBlip)
					ELSE
						IF sFMMCmenu.sActiveMenu != eFmmc_PAN_CAM_BASE
						AND sFMMCmenu.sActiveMenu != eFmmc_PHOTO_CAM_BASE
						AND sFMMCmenu.sActiveMenu != eFmmc_MAIN_OPTIONS_BASE
						AND sFMMCmenu.sActiveMenu != eFmmc_TOP_MENU
						AND sFMMCmenu.sActiveMenu != eFmmc_RADIO_MENU
							CONTROL_PLAYER_BLIP(FALSE, sCurrentVarsStruct, sFMMCmenu.iEntityCreation, sFMMCmenu.bZoomedOutRadar, biFurthestBlip)
						ELSE
							CONTROL_PLAYER_BLIP(TRUE, sCurrentVarsStruct, sFMMCmenu.iEntityCreation, sFMMCmenu.bZoomedOutRadar, biFurthestBlip)
						ENDIF												
					ENDIF
				BREAK
				
				CASE STAGE_DELETE_ALL
				CASE STAGE_DELETE_PEDS
				CASE STAGE_DELETE_VEHICLES
				CASE STAGE_DELETE_WEAPONS
				CASE STAGE_DELETE_PROPS
				CASE STAGE_DELETE_DYNOPROPS
				CASE STAGE_DELETE_TEAM_START
					DO_CONFIRMATION_MENU(sPedStruct, sTeamSpawnStruct, sVehStruct, sWepStruct, sObjStruct, sPropStruct, sDynoPropStruct, sLocStruct, sCurrentVarsStruct, sFMMCmenu, sInvisibleObjects, sFMMCEndStage, ButtonPressed, iLocalBitSet)
				BREAK
				
				CASE STAGE_ENTITY_PLACEMENT_END_MENU
					DO_END_MENU()
				BREAK	
				
				CASE STAGE_CLOUD_FAILURE				
					FMMC_DRAW_CLOUD_FAIL_WARNING(bCreatorLimitedCloudDown, bSignedOut, FALSE)
					IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
						IF !SCRIPT_IS_CLOUD_AVAILABLE()
							sCurrentVarsStruct.iMenuState = MENU_STATE_PAN_CAM
						ELSE
							IF NOT IS_SCREEN_FADED_IN()
								DO_SCREEN_FADE_IN(500)
							ENDIF
						ENDIF
						SET_ENTITY_CREATION_STATUS(STAGE_ENTITY_PLACEMENT_SETUP)
						REFRESH_MENU(sFMMCMenu)
					ENDIF
				BREAK
				
				CASE STAGE_DEAL_WITH_RECREATING_MISSION
					IF NOT IS_SCREEN_FADING_OUT()
						IF RESET_THE_MISSION_UP_SAFELY()
							SET_RESET_STATE(RESET_STATE_FADE)	
							SET_STORE_ENABLED(TRUE)
							SET_CREATOR_AUDIO(TRUE, FALSE)						
							IF sFMMCmenu.iRadioState = FMMC_RADIO_MAX_OPTIONS_STATE
								SET_MOBILE_PHONE_RADIO_STATE(TRUE)
							ENDIF	
							
							CLEAR_HELP()
							CLEAR_BIT(sFMMCdata.iBitSet, biTestMissionActive)
							CLEAR_BIT(sFMMCdata.iBitSet, biSetUpTestMission)	
							
							ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, FALSE)
							ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, FALSE)
							
							sFMMCmenu.sActiveMenu = eFmmc_TOP_MENU
							
							KILL_WORLD_FOR_THE_CREATORS()
							
							iHelpBitSetOld = -1
							SET_ENTITY_CREATION_STATUS(STAGE_ENTITY_PLACEMENT_SETUP)
							sCurrentVarsStruct.iMenuState = MENU_STATE_DEFAULT
							
							sFMMCdata.vPlayerPos = <<0,0,10>>
									
							//CHECK_FOR_END_CONDITIONS_MET()
							//Clean up
							REFRESH_MENU(sFMMCMenu)
													
							if IS_BIT_SET(sFMMCdata.iBitSet, bOnTheGround)
								FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
								DO_SCREEN_FADE_IN(500)
							ELSE
								FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
							ENDIF	
						ENDIF
					ENDIF
				BREAK
				
				CASE STAGE_DEAL_WITH_SAVING
					IF bContentReadyForUGC = FALSE
						IF PREPARE_CONTENT_TO_BE_SAVED_OR_PUBLISHED()
							bContentReadyForUGC = TRUE
						ENDIF
					ELSE
						IF SAVE_THIS_CREATION(sGetUGC_content, sFMMCendStage, sCurrentVarsStruct.creationStats)
							sFMMCendStage.iEndStage = 0
							sFMMCendStage.dataStruct.iLoadStage = 0
							sFMMCendStage.iPublishConformationStage = 0
							sFMMCendStage.iPublishStage = 0
							sFMMCendStage.iButtonBitSet = 0
							#IF IS_DEBUG_BUILD
							PRINT_FMMC_DEBUGDATA(FALSE)	
							#ENDIF
							SET_ENTITY_CREATION_STATUS(STAGE_ENTITY_PLACEMENT_SETUP)
							sFMMCendStage.iEndStage = ciFMMC_END_STAGE_SET_UP
							SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
							DO_SCREEN_FADE_IN(200)
							sCurrentVarsStruct.bSwitching = FALSE
							sCurrentVarsStruct.stiScreenCentreLOS = NULL
							bContentReadyForUGC = FALSE
							IF NOT IS_STRING_NULL_OR_EMPTY(sFMMCendStage.tl23ReturnedContexID)
								sFMMCendStage.tl23LoadedContexID = sFMMCendStage.tl23ReturnedContexID						
							ENDIF
						ENDIF
						IF sFMMCendStage.oskStatus = OSK_CANCELLED
							sFMMCendStage.iEndStage = 0
							sFMMCendStage.dataStruct.iLoadStage = 0
							sFMMCendStage.iPublishConformationStage = 0
							sFMMCendStage.iPublishStage = 0
							sFMMCendStage.iButtonBitSet = 0
							sFMMCendStage.oskStatus = OSK_PENDING
							sFMMCendStage.iKeyBoardStatus = 0
							SET_ENTITY_CREATION_STATUS(STAGE_ENTITY_PLACEMENT_SETUP)
							sFMMCendStage.iEndStage = ciFMMC_END_STAGE_SET_UP
							SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
							sCurrentVarsStruct.bSwitching = FALSE
							sCurrentVarsStruct.stiScreenCentreLOS = NULL
							bContentReadyForUGC = FALSE
							DO_SCREEN_FADE_IN(200)
							#IF IS_DEBUG_BUILD
							PRINTLN("oskStatus = OSK_CANCELLED")
							#ENDIF					
						ENDIF
					ENDIF
				BREAK
				
				CASE STAGE_ENTITY_PLACEMENT_PUBLISH
					IF bContentReadyForUGC = FALSE
						IF PREPARE_CONTENT_TO_BE_SAVED_OR_PUBLISHED()
							bContentReadyForUGC = TRUE
						ENDIF
					ELSE
						IF PUBLISH_THIS_CREATION(sGetUGC_content, sFMMCendStage, sCurrentVarsStruct.creationStats)
							sFMMCendStage.iEndStage = 0
							sFMMCendStage.dataStruct.iLoadStage = 0
							sFMMCendStage.iPublishConformationStage = 0
							sFMMCendStage.iPublishStage = 0
							sFMMCendStage.iButtonBitSet = 0
							#IF IS_DEBUG_BUILD
							PRINT_FMMC_DEBUGDATA(FALSE)	
							#ENDIF
							SET_ENTITY_CREATION_STATUS(STAGE_ENTITY_PLACEMENT_SETUP)	
							sFMMCendStage.iEndStage = ciFMMC_END_STAGE_SET_UP
									
							IF sCurrentVarsStruct.creationStats.bSuccessfulSave = TRUE
								PRINTLN("sFMMCendStage.bSaveCancelled = FALSE, setting sFMMCendStage.bMajorEditOnLoadedMission = FALSE")
								sFMMCendStage.bMajorEditOnLoadedMission = FALSE
								IF NOT IS_STRING_NULL_OR_EMPTY(sFMMCendStage.tl23ReturnedContexID)
									sFMMCendStage.tl23LoadedContexID = sFMMCendStage.tl23ReturnedContexID						
								ENDIF
							ENDIF
							
							SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
							sCurrentVarsStruct.bSwitching = FALSE
							sCurrentVarsStruct.stiScreenCentreLOS = NULL
							bContentReadyForUGC = FALSE
							//CHECK_FOR_END_CONDITIONS_MET()
							CLEAR_BIT(sFMMCmenu.iTopMenuActive, SURV_TOP_MENU_SAVE)
							
							REFRESH_MENU(sFMMCmenu)
							DO_SCREEN_FADE_IN(200)
						ENDIF
						IF sFMMCendStage.oskStatus = OSK_CANCELLED
							sFMMCendStage.oskStatus = OSK_PENDING
							sFMMCendStage.iKeyBoardStatus = 0
							SET_ENTITY_CREATION_STATUS(STAGE_ENTITY_PLACEMENT_SETUP)
							sFMMCendStage.iEndStage = ciFMMC_END_STAGE_SET_UP
							SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
							sCurrentVarsStruct.bSwitching = FALSE
							sCurrentVarsStruct.stiScreenCentreLOS = NULL
							bContentReadyForUGC = FALSE
							DO_SCREEN_FADE_IN(200)
							IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSet, ciDEFAULT_XP)
								g_FMMC_STRUCT.iXPReward = 0
								PRINTLN("ciDEFAULT_XP RESET TO = ", g_FMMC_STRUCT.iXPReward)
							ENDIF
							#IF IS_DEBUG_BUILD
							PRINTLN("oskStatus = OSK_CANCELLED")
							#ENDIF					
						ENDIF
					ENDIF
				BREAK
				
				CASE STAGE_DEAL_WITH_FINISHING
					IF IS_TRANSITION_TELLING_CREATOR_TO_CLEANUP()
						TRIGGER_TRANSITION_MENU_ACTIVE(TRUE) 					
						PRINTLN("[JA@PAUSEMENU] Clean up creator due to IS_TRANSITION_TELLING_CREATOR_TO_CLEANUP")
						DO_SCREEN_FADE_IN(200)
						SCRIPT_CLEANUP(TRUE)
					ELIF IS_PAUSE_MENU_REQUESTING_TRANSITION()
						TRIGGER_TRANSITION_MENU_ACTIVE(TRUE) 
						PRINTLN("[JA@PAUSEMENU] Clean up creator due to pause menu requesting transition")
						DO_SCREEN_FADE_IN(200)
						SCRIPT_CLEANUP(TRUE)
					ELIF DEAL_WITH_FINISHING_CREATOR(sFMMCendStage)
						DO_SCREEN_FADE_IN(200)
						PRINTLN("[END CREATOR] DONE")
					 	SCRIPT_CLEANUP(FALSE)
					ENDIF
				BREAK
				
			ENDSWITCH
		ELSE
			IF NETWORK_IS_GAME_IN_PROGRESS()
				MAINTAIN_MP_AMBIENT_MANAGER()
				IF bTextSetUp = TRUE
					Maintain_MP_Objective_Text()
					Maintain_MP_Communications()
				ENDIF
				HANDLE_FORCE_PLAYERS_FROM_CAR(PLAYER_ID())
				MAINTAIN_FORCE_OUT_OF_VEHICLE()
				RENDER_INVENTORY_HUD(on_mission_gang_box)
				MAINTAIN_BIG_MESSAGE() 
				PROCESS_LEADERBOARD_CAM()
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(sFMMCdata.iBitSet, bMenuJustChangedPlacementBlocker)
			PRINTLN("[LM] - Creator Bit - Clearing bMenuJustChangedPlacementBlocker")
			CLEAR_BIT(sFMMCdata.iBitSet, bMenuJustChangedPlacementBlocker)
		ENDIF
		
		IF g_Private_Gamemode_Current = GAMEMODE_FM
		AND NETWORK_IS_GAME_IN_PROGRESS()
			PRINTLN("[LH] A Network Game is in Progress! Cleaning up Creator.")
			SCRIPT_CLEANUP(TRUE)
		ENDIF
		
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(hash("main")) != 0		
			PRINTLN("[LH] The player has escaped to singleplayer! Cleaning up Creator.")
			SCRIPT_CLEANUP(TRUE)
		ENDIF
		
		IF IS_TRANSITION_TELLING_CREATOR_TO_CLEANUP()	
			PRINTLN("[LM] Pause Menu doing a transition. We're leaving!")
			sCurrentVarsStruct.iEntityCreationStatus = STAGE_DEAL_WITH_FINISHING
			TRIGGER_TRANSITION_MENU_ACTIVE(TRUE) 
			DO_SCREEN_FADE_IN(100)
			SCRIPT_CLEANUP(TRUE)
		ENDIF
		
		IF g_bKillFMMC_SignedOut
			SCRIPT_CLEANUP(TRUE)
			PRINTLN("g_bKillFMMC_SignedOut = TRUE")
		ENDIF
		
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(hash("freemode")) != 0
			PRINTLN("The player has escaped to freemode! Cleaning up Creator.")
			SCRIPT_CLEANUP(TRUE)
			NETWORK_BAIL_FROM_CREATOR()
		ENDIF
		
		IF sCurrentVarsStruct.iEntityCreationStatus != STAGE_DEAL_WITH_RECREATING_MISSION
			MAINTAIN_TEST_MODE_STATE()
		ENDIF
		
		#IF IS_DEBUG_BUILD
			
		//Update the widgets
		UPDATE_WIDGETS()
		
		//Force clean up
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
			IF IS_FAKE_MULTIPLAYER_MODE_SET() = FALSE
				sCurrentVarsStruct.iEntityCreationStatus = STAGE_ENTITY_PLACEMENT_END_MENU
			ENDIF
		ENDIF
					
		#ENDIF
		
	ENDWHILE
ENDSCRIPT

