// ___________________________________________
//
//	Game: GTAV - SURVIVAL CONTROLLER.
//  Script: FM_Survival_Controller.sc
//  Author: Jack Thallon: 03/01/2019
//	Description: Runs the updated and UGC Survival mode. 
//	Previously x:\gta5\script\dev_ng\multiplayer\scripts\modes\freemode\FM_Mission_Creator\FM_Horde_Controler.sc
// ___________________________________________

USING "globals.sch"
USING "shared_hud_displays.sch"
USING "net_spawn.sch"
USING "net_objective_text.sch"
USING "freemode_header.sch"
USING "FMMC_HEADER.sch"
USING "screen_gang_on_mission.sch"
USING "leader_board_common.sch"
USING "net_spectator_cam.sch"
USING "weapons_public.sch"
USING "leader_board_common.sch"
USING "socialclub_leaderboard.sch"
USING "FMMC_Restart_Header.sch"
USING "FM_Playlist_Header.sch"
USING "net_leaderboards.sch"
USING "social_feed_controller.sch"
USING "net_script_tunables.sch"
USING "FMMC_next_job_screen.sch"
USING "net_celebration_screen.sch"
USING "net_wait_zero.sch"
USING "net_cash_transactions.sch"


USING "FM_Survival_Controller_DEBUG.sch"
USING "FM_Survival_Controller_Ending.sch"
USING "FM_Survival_Controller_Events.sch"
USING "FM_Survival_Controller_HUD.sch"
USING "FM_Survival_Controller_Init.sch"
USING "FM_Survival_Controller_Main.sch"
USING "FM_Survival_Controller_Objects.sch"
USING "FM_Survival_Controller_Peds.sch"
USING "FM_Survival_Controller_Players.sch"
USING "FM_Survival_Controller_Using.sch"
USING "FM_Survival_Controller_Utility.sch"
USING "FM_Survival_Controller_Vehicles.sch"
USING "FM_Survival_Controller_Waves.sch"

PROC PROCESS_SERVER()
   
    SWITCH GET_SERVER_HORDE_STAGE()

        CASE eHORDESTAGE_JIP_IN
            PROCESS_eHORDESTAGE_JIP_IN_SERVER()
        BREAK
        
        CASE eHORDESTAGE_SETUP_MISSION
            PROCESS_eHORDESTAGE_SETUP_MISSION_SERVER()
        BREAK
        
        CASE eHORDESTAGE_START_FADE_IN
            PROCESS_eHORDESTAGE_START_FADE_IN_SERVER()
        BREAK
        
        CASE eHORDESTAGE_FIGHTING
            PROCESS_eHORDESTAGE_FIGHTING_SERVER()
        BREAK
        
        CASE eHORDESTAGE_PASSED_WAVE
            PROCESS_eHORDESTAGE_PASSED_WAVE_SERVER()
        BREAK
        
        CASE eHORDESTAGE_DELAY_BEFORE_NEXT_WAVE
            PROCESS_eHORDESTAGE_DELAY_BEFORE_NEXT_WAVE_SERVER()
        BREAK
        
        CASE eHORDESTAGE_SPECTATING
            PROCESS_eHORDESTAGE_SPECTATING_SERVER()
        BREAK
        
        CASE eHORDESTAGE_REWARDS_AND_FEEDACK
            PROCESS_eHORDESTAGE_REWARDS_AND_FEEDACK_SERVER()
        BREAK
        
    ENDSWITCH
    
ENDPROC

PROC PROCESS_CLIENT()
	
	IF NOT bSetSctvTickerOn
		SET_SCTV_TICKER_SCORE_ON()
		bSetSctvTickerOn = TRUE
	ENDIf
	
    // Handle events queue.
    PROCESS_EVENTS()
    
	// Player deaths stat.
	TRACK_LOCAL_PLAYER_DEATHS()
	
    // Processes if should respawn or go to spectator mode.
    PROCESS_RESPAWNING()
    
    // Mini leader board that is on dpad down.
    MAINTAIN_MINI_LEADER_BOARD()
	
	// Tell players they cannot use vehicles during a survival.
	MAINTAIN_CANNOT_USE_VEHICLES_HELP()
	
	// Challenge timer.
	MAINTAIN_CHALLENGE_TIMER()
	
	// Area Blips
	MAINTAIN_BLIPS_FOR_PLACED_AREAS()
	
	//Minimap
	PROCESS_MINIMAP()
	
	//Disabled Controls
	MAINTAIN_DISABLED_CONTROLS()
	
	//Blimp
	IF NOT g_b_OnHordeWaveBoard
		DO_BLIMP_SIGNS(sBlimpSign)
	ENDIF
	
	// Ticker to remind to kick leechers.
	// Don't want SCTV player to see ticker, they can't vote to kick.
	IF NOT IS_PLAYER_SCTV(localPlayer)
	AND NOT IS_FAKE_MULTIPLAYER_MODE_SET()
		MAINTAIN_MY_MIGHT_NOT_BE_PLAYING_STATE()
		PROCESS_KICK_LEECHERS_REMINDER_TICKER()
	ENDIF
	
	// Need paths for ai navigation.
	REQUEST_PATH_NODES_FOR_PLAY_AREA()
	
	// As soon as we have an elegant end of mode reason, save out the the player ugc data.
	SAVE_PLAYER_DATA_ON_FINISH()
	
	REFRESH_DPAD_WHEN_DATA_CHANGES(iRefreshDpad, iStoredRefreshDpadValue)
	
	PROCESS_SPECTATOR_FLAGS()
	
	IF g_FMMC_STRUCT_ENTITIES.iNumberOfWorldProps > 0
	AND g_FMMC_STRUCT_ENTITIES.iNumberOfWorldProps <= GET_FMMC_MAX_WORLD_PROPS()
		LEGACY_PROCESS_WORLD_PROPS(iWorldPropIterator, sRuntimeWorldPropData, FALSE)
		iWorldPropIterator++
		IF iWorldPropIterator >= g_FMMC_STRUCT_ENTITIES.iNumberOfWorldProps
			iWorldPropIterator = 0
		ENDIF
	ENDIF
	
	IF IS_FAKE_MULTIPLAYER_MODE_SET()
	AND GET_CLIENT_HORDE_STAGE(PARTICIPANT_ID()) != eHORDESTAGE_PASSED_WAVE
  	 	MAINTAIN_SPAWN_TEST_HUD()
	ENDIF
	
	//Disable Realtime Multiplayer
	PROCESS_DISABLE_REALTIME_MULTIPLAYER()
	
	// Main switch.
    SWITCH GET_CLIENT_HORDE_STAGE(PARTICIPANT_ID())

        CASE eHORDESTAGE_JIP_IN
            PROCESS_eHORDESTAGE_JIP_IN_CLIENT()
        BREAK
        
        CASE eHORDESTAGE_SETUP_MISSION
            PROCESS_eHORDESTAGE_SETUP_MISSION_CLIENT()
        BREAK
        
        CASE eHORDESTAGE_START_FADE_IN
            PROCESS_eHORDESTAGE_START_FADE_IN_CLIENT()
        BREAK
        
        CASE eHORDESTAGE_FIGHTING
           	PROCESS_eHORDESTAGE_FIGHTING_CLIENT()
        BREAK
        
        CASE eHORDESTAGE_PASSED_WAVE
            PROCESS_eHORDESTAGE_PASSED_WAVE_CLIENT()
        BREAK
        
        CASE eHORDESTAGE_DELAY_BEFORE_NEXT_WAVE
            PROCESS_eHORDESTAGE_DELAY_BEFORE_NEXT_WAVE_CLIENT()
        BREAK
        
        CASE eHORDESTAGE_SPECTATING
            PROCESS_eHORDESTAGE_SPECTATING_CLIENT()
        BREAK
        
        CASE eHORDESTAGE_SCTV
            PROCESS_eHORDESTAGE_SCTV_CLIENT()
        BREAK
        
        CASE eHORDESTAGE_REWARDS_AND_FEEDACK
            PROCESS_eHORDESTAGE_REWARDS_AND_FEEDACK_CLIENT()
        BREAK
        
    ENDSWITCH
    
ENDPROC

PROC CHECK_MISSION_BAIL_CONDITIONS()
	// If we have a match end event, bail.
    IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
		PRINTLN("[Survival] - SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE returning TRUE, calling SCRIPT_CLEANUP.")
		DO_MATCH_END()
        SCRIPT_CLEANUP()
    ENDIF
    IF SHOULD_PLAYER_LEAVE_MP_MISSION(TRUE) 
		PRINTLN("[Survival] - SHOULD_PLAYER_LEAVE_MP_MISSION returning TRUE, calling SCRIPT_CLEANUP.")
		IF SHOULD_I_PROCESS_END_HORDE_STATS()
			IF GET_MP_INT_CHARACTER_STAT(MP_STAT_HORDELVL) > GET_WAVE_SURVIVED_UNTIL()
				SET_MP_INT_CHARACTER_STAT(MP_STAT_HORDELVL, GET_WAVE_SURVIVED_UNTIL())
			ENDIF
		ENDIF
		DO_MATCH_END()
        SCRIPT_CLEANUP()
    ENDIF
ENDPROC

SCRIPT(MP_MISSION_DATA HordeMissionData)

    NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()
	PRINTLN("[survival] - Setting NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME")
	
    // Carry out all the initial game starting duties. 
    IF NETWORK_IS_GAME_IN_PROGRESS()
        IF NOT PROCESS_PRE_GAME(HordeMissionData)
			PRINTLN("[Survival] - Failed to receive an initial network broadcast. Calling SCRIPT_CLEANUP.")
			SCRIPT_CLEANUP()
		ENDIF
    ENDIF
	
    // Main while loop.
    WHILE TRUE

        IF IS_FAKE_MULTIPLAYER_MODE_SET()
			IF NOT IS_PAUSE_MENU_ACTIVE()
				IF g_FMMC_STRUCT.g_b_QuitTest
					DO_SCREEN_FADE_OUT(0)
					TRIGGER_MUSIC_EVENT("MP_MC_STOP")
					PRINTLN("[Survival] - MUSIC - TRIGGER_MUSIC_EVENT(MP_MC_STOP) - K")
					IF IS_FAKE_MULTIPLAYER_MODE_SET()
						g_iSurvivalEndReason = 0
					ENDIF
					PRINTLN("[Survival] - g_FMMC_STRUCT.g_b_QuitTest is true so quit")
					SCRIPT_CLEANUP()
					EXIT
				ENDIF
			ENDIF
		ENDIF
		
        // One wait to rule them all. This can be the ONLY wait from here on in...
        MP_LOOP_WAIT_ZERO()
		
        CHECK_MISSION_BAIL_CONDITIONS()
		
		INIT_LOCAL_PLAYER_DATA()
		INIT_PLAYER_BD_DATA()
		INIT_LOCAL_SPECTATOR_DATA()
		
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciFORCE_SPECIFIC_TOD)
			IF serverBD.iTimeOfDay > -1
				IF IS_FAKE_MULTIPLAYER_MODE_SET()
				AND serverBD.iTimeOfDay = 0
					NETWORK_OVERRIDE_CLOCK_TIME(14, 15, 0)
				ELSE
					SET_TIME_OF_DAY(serverBD.iTimeOfDay)
				ENDIF
			ENDIF
		ELSE
			NETWORK_OVERRIDE_CLOCK_TIME(g_FMMC_STRUCT.iTODOverrideHours, g_FMMC_STRUCT.iTODOverrideMinutes, 0)
		ENDIF
		
		
		FM_MAINTAIN_MISSION_DENSITYS_THIS_FRAME(DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
		
		DO_OUTROS()
		
		IF SHOULD_RUN_NEW_VOTING_SYSTEM(sEndOfMission.iQuitProgress, serverBD.sServerFMMC_EOM.iVoteStatus)
			IF NOT READY_TO_MOVE_TO_NEXT_JOBS(nextJobStruct)
				DO_END_JOB_VOTING_SCREEN(g_sMC_serverBDEndJob, nextJobStruct, g_sMC_serverBDEndJob.timerVotingEnds.Timer)			
			ENDIF
		ENDIF
		
        IF NETWORK_IS_GAME_IN_PROGRESS()
		
			// Deal with the debug
	        #IF IS_DEBUG_BUILD              
	            MAINTAIN_WIDGETS()
				MAINTAIN_ON_SCREEN_DEBUG()
	        #ENDIF
    
            // -----------------------------------
            // Process client game logic    
            REINIT_NET_TIMER(tdNetTimer)
            
			MAINTAIN_SPEC_NOT_HOST(iManageSpechost, iDesiredHost)
			
			// Participant loops.
            MAINTAIN_PARTICPANT_LOOP()
			
//			// Sort radar zoom level.
			CONTROL_RADAR_ZOOM()
			
			// For pre-loading the celebration where possible.
			IF GET_END_HORDE_REASON() != eENDHORDEREASON_NOT_FINISHED_YET
				TRIGGER_CELEBRATION_PRE_LOAD(sCelebrationData, TRUE)
			ENDIF
			MAINTAIN_CELEBRATION_PRE_LOAD(sCelebrationData)
		
			PROCESS_SURVIVAL_INVINCIBILITY_STAGES()
			
			MAINTAIN_LAST_WEAPON()
			
			IF NOT IS_PLAYER_SCTV(LocalPlayer)
				MAINTAIN_SPECTATOR(g_BossSpecData)
			ENDIF
			
			IF bIsLocalPlayerHost
				TRACK_NUM_ALIVE_LAND_VEHICLE_PEDS()
			ENDIF
			
			//Props
			PROCESS_PROPS()
			
			IF IS_HALLOWEEN_MODE()
				IF GET_CLIENT_GAME_STATE(iLocalPart) < SURVIVAL_GAME_STATE_RUNNING
					PROCESS_EVENTS()
				ENDIF
				PROCESS_HALLOWEEN_EVERY_FRAME()
			ENDIF
			
			g_bVendingMachinesActive = FALSE
			
			// Main switch.
            SWITCH GET_CLIENT_GAME_STATE(iLocalPart)
                                        
                // Wait until the server gives the all go before moving on.
                CASE SURVIVAL_GAME_STATE_INIT
					
					// Request anims.
                    REQUEST_DM_ANIMS()
					
					// Don;t want hud.
					HIDE_HUD_AND_RADAR_THIS_FRAME()
					
					// Add busy spinner while doing setup.
					IF NOT DID_I_JOIN_MISSION_AS_SPECTATOR()
						IF NOT BUSYSPINNER_IS_ON() //*1600246
							BEGIN_TEXT_COMMAND_BUSYSPINNER_ON("FMMC_PLYLOAD")
							END_TEXT_COMMAND_BUSYSPINNER_ON(ENUM_TO_INT(LOADING_ICON_LOADING))
							PRINTLN("[Survival] - BUSY SPINNER ON")
						ENDIF
					ENDIF
					
					// Play anim post fx.
					IF IS_CORONA_READY_TO_START_WITH_JOB()
					AND NOT DID_I_JOIN_MISSION_AS_SPECTATOR()
						IF NOT IS_CORONA_FREEZE_AND_START_FX_SET()
							SET_HIDOF_OVERRIDE(TRUE,FALSE,0,0,0,0)
							ANIMPOSTFX_PLAY("MP_job_load", 0, TRUE)
						ENDIF
					ENDIF
					
					ALLOW_PLAYER_HEIGHTS(TRUE)
					
					// If the server hasmoved beyond init.
                    IF (GET_SERVER_GAME_STATE() > SURVIVAL_GAME_STATE_INIT)
						// If I am alive.
                        IF bLocalPlayerOK
							// Load Horde text.
						    IF LOAD_HORDE_TEXT_BLOCK()
								IF LOAD_ALL_ASSETS()
							        // Create props.
							        IF CREATE_FMMC_PROPS_MP(oiHordeProp, oihordePropChildren)
							            // Create guns, armour and health.
										SET_PICKUP_TYPES_FOR_WAVE()
							            IF CREATE_PICKUPS()
											LEGACY_INITIALISE_WORLD_PROPS()
											IF DO_OUTFITS()
												// Say I am ready for start fade in.
												IF NOT bSetupInitialSpawnPos
													SETUP_SURVIVAL_PLAYER_SPAWN_POSITION(TRUE)
													bSetupInitialSpawnPos = TRUE
												ELSE
													IF DID_I_JOIN_MISSION_AS_SPECTATOR()
													OR IS_PLAYER_SCTV(localPlayer)
														IF GET_CURRENT_TRANSITION_STATE() = TRANSITION_STATE_WAITING_FOR_EXTERNAL_TERMINATION_CALL
														OR GET_CURRENT_TRANSITION_STATE() = TRANSITION_STATE_TERMINATE_MAINTRANSITION
														OR GET_CURRENT_TRANSITION_STATE() = TRANSITION_STATE_FM_SWOOP_DOWN
															bContinueToCut = TRUE
														ELSE
															#IF IS_DEBUG_BUILD
															CPRINTLN(DEBUG_SPECTATOR, 	"=== SURVIVAL === Not moving to spectate as GET_CURRENT_TRANSITION_STATE = ", 
																						GET_TRANSITION_STATE_STRING(GET_CURRENT_TRANSITION_STATE()))
															#ENDIF
														ENDIF
													ELSE
														IF WARP_TO_START_POSITION(iWarpToStartPosStage, TRUE)
														OR IS_FAKE_MULTIPLAYER_MODE_SET()
															bContinueToCut = TRUE
														ENDIF
													ENDIF
											        IF bContinueToCut
													
														// Get spectator mode runnin as early as possible.
														IF bDidJipSpecFade
															SET_TRANSITION_SESSION_PULL_CAM_DOWN_FOR_SESSION_CHANGE()
															CLEAR_IM_NOT_GOING_TO_TAKE_PART_IN_THIS_CORONA()
															SET_MG_READY_FOR_TRANSITION_STATE_FM_SWOOP_DOWN()
															IF DID_I_JOIN_MISSION_AS_SPECTATOR()
															OR IS_PLAYER_SCTV(localPlayer)
																PRINTLN("[Survival] - DID_I_JOIN_MISSION_AS_SPECTATOR = TRUE")
																DO_SCREEN_FADE_OUT(500)
																IF NOT IS_THIS_SPECTATOR_CAM_ACTIVE(g_BossSpecData.specCamData)
																	PRINTLN("[Survival] - IS_THIS_SPECTATOR_CAM_ACTIVE = FALSE")
																	IF IS_SAFE_TO_TURN_ON_SPECTATOR_CAM()
																		PRINTLN("[Survival] - IS_SAFE_TO_TURN_ON_SPECTATOR_CAM = TRUE")
																		CLEANUP_ALL_CORONA_FX(TRUE) 
										           						SET_SKYFREEZE_CLEAR()
																		IF ANIMPOSTFX_IS_RUNNING("MP_job_load")
																			ANIMPOSTFX_STOP("MP_job_load")
																		ENDIF
																		SET_GAME_STATE_ON_DEATH(AFTERLIFE_SET_SPECTATORCAM)
																		ACTIVATE_SPECTATOR_CAM(g_BossSpecData, SPEC_MODE_PLAYERS_STAY_DEAD_EVENT, NULL, ASCF_HIDE_LOCAL_PLAYER|ASCF_QUIT_SCREEN|ASCF_ONLY_FIND_TARGET_IF_PLAYER_LIST_POPULATED )
																		
																		bDidJipSpecFade = TRUE
															        ENDIF
																ENDIF
															ENDIF
														ENDIF
							
														IF IS_CORONA_READY_TO_START_WITH_JOB()
														OR DID_I_JOIN_MISSION_AS_SPECTATOR()
														OR IS_PLAYER_SCTV(localPlayer)
														OR IS_FAKE_MULTIPLAYER_MODE_SET()
															
															IF GIVE_PLAYER_WEAPONS_AT_START()
																CLEAR_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_ON_LAST_SURVIVAL_ROUND)
																
																LOCAL_PLAYER_INIT()
																
																SET_PED_AND_TRAFFIC_DENSITIES()
																
																SET_CLIENT_GAME_STATE(SURVIVAL_GAME_FADE_IN)
															ENDIF
														ELSE
															PRINTLN("[Survival] - SURVIVAL_GAME_STATE_INIT - Waiting on join method")
														ENDIF
													ELSE
														PRINTLN("[Survival] - SURVIVAL_GAME_STATE_INIT - Waiting on bContinueToCut")
													ENDIF
												ENDIF
											ELSE
												PRINTLN("[Survival] - SURVIVAL_GAME_STATE_INIT - Waiting on Outfits")
											ENDIF
										ELSE
											PRINTLN("[Survival] - SURVIVAL_GAME_STATE_INIT - Waiting on Pickups")
										ENDIF
									ELSE
										PRINTLN("[Survival] - SURVIVAL_GAME_STATE_INIT - Waiting on Props")
									ENDIF
								ELSE
									PRINTLN("[Survival] - SURVIVAL_GAME_STATE_INIT - Waiting on Assets")
								ENDIF
							ELSE
								PRINTLN("[Survival] - SURVIVAL_GAME_STATE_INIT - Waiting on Text")
							ENDIF
							
                        ENDIF
                    ENDIF
                    
                BREAK
				
				CASE SURVIVAL_GAME_FADE_IN
					HIDE_HUD_AND_RADAR_THIS_FRAME()
					IF HAS_EVERYONE_REACHED_TEAMCUTSCENE()
						//SET_CLIENT_GAME_STATE(SURVIVAL_GAME_TEAMCUT_CUTSCENE)
						g_b_TransitionActive = FALSE
						SET_BIT(g_CoronaJobLaunchData.iBS_CoronaJobFlags, ciCORONA_FAKE_PED_CLEANUP)
						IF NOT IS_PLAYER_SPECTATING(localPlayer)
						AND NOT DID_I_JOIN_MISSION_AS_SPECTATOR()
							SET_ENTITY_VISIBLE(LocalPlayerPed,TRUE)
						ENDIF
						PLAY_SOUND_FRONTEND(-1,"Hit","RESPAWN_SOUNDSET")
						SETUP_SURVIVAL_PLAYER_SPAWN_POSITION(FALSE)
						IF NOT IS_BIT_SET(serverBD.iBitset, BITSET_SERVER_NEW_PLAYERS_GO_STRAIGHT_TO_SPEC_CAM)
						AND NOT DID_I_JOIN_MISSION_AS_SPECTATOR()
							PRINTLN("[Survival] - going to state SURVIVAL_GAME_INTRO_CUTSCENE.")
							SET_CLIENT_GAME_STATE(SURVIVAL_GAME_INTRO_CUTSCENE)
						ELSE
							#IF IS_DEBUG_BUILD
							IF IS_BIT_SET(serverBD.iBitset, BITSET_SERVER_NEW_PLAYERS_GO_STRAIGHT_TO_SPEC_CAM)
								PRINTLN("[Survival] - SHOULD_PARTICIPANT_GO_STRAIGHT_TO_SPECTATOR_CAMERA = TRUE.")
							ENDIF
							
							IF DID_I_JOIN_MISSION_AS_SPECTATOR()
								PRINTLN("[Survival] - DID_I_JOIN_MISSION_AS_SPECTATOR = TRUE.")
							ENDIF
							#ENDIF
							IF NOT IS_THIS_SPECTATOR_CAM_ACTIVE(g_BossSpecData.specCamData)
								IF IS_SAFE_TO_TURN_ON_SPECTATOR_CAM()
									SET_GAME_STATE_ON_DEATH(AFTERLIFE_SET_SPECTATORCAM)
									ACTIVATE_SPECTATOR_CAM(g_BossSpecData, SPEC_MODE_PLAYERS_STAY_DEAD_EVENT, NULL, ASCF_HIDE_LOCAL_PLAYER|ASCF_QUIT_SCREEN|ASCF_ONLY_FIND_TARGET_IF_PLAYER_LIST_POPULATED )
						        ENDIF
							ENDIF
							PRINTLN("[Survival] - going to state SURVIVAL_GAME_STATE_RUNNING.")
							SET_CLIENT_GAME_STATE(SURVIVAL_GAME_STATE_RUNNING)
						ENDIF
					ENDIF
				BREAK
                
				CASE SURVIVAL_GAME_INTRO_CUTSCENE

					HIDE_HUD_AND_RADAR_THIS_FRAME()
					HIDE_PLAYER(FALSE)
					DISABLE_PLAYER_CONTROLS_THIS_FRAME()
					DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
					NET_SET_PLAYER_CONTROL(localPlayer, FALSE)
					
					IF NOT IS_LOCAL_BIT_SET(ciLocalBool_bAppearanceUpdated)
						BROADCAST_GENERAL_EVENT(GENERAL_EVENT_TYPE_PLAYER_CHANGED_APPEARANCE, ALL_PLAYERS())
						SET_LOCAL_BIT(ciLocalBool_bAppearanceUpdated)
					ENDIF
					
					IF IS_BIT_SET(serverBD.iBitset, BITSET_SERVER_NEW_PLAYERS_GO_STRAIGHT_TO_SPEC_CAM)
					OR DID_I_JOIN_MISSION_AS_SPECTATOR()
						
						#IF IS_DEBUG_BUILD
						IF IS_BIT_SET(serverBD.iBitset, BITSET_SERVER_NEW_PLAYERS_GO_STRAIGHT_TO_SPEC_CAM)
							PRINTLN("[Survival] - SHOULD_PARTICIPANT_GO_STRAIGHT_TO_SPECTATOR_CAMERA = TRUE.")
						ENDIF
						
						IF DID_I_JOIN_MISSION_AS_SPECTATOR()
							PRINTLN("[Survival] - DID_I_JOIN_MISSION_AS_SPECTATOR = TRUE.")
						ENDIF
						#ENDIF
						
						// Get spectator mode runnin as early as possible.
						IF NOT IS_THIS_SPECTATOR_CAM_ACTIVE(g_BossSpecData.specCamData)
							IF IS_SAFE_TO_TURN_ON_SPECTATOR_CAM()
								SET_GAME_STATE_ON_DEATH(AFTERLIFE_SET_SPECTATORCAM)
								ACTIVATE_SPECTATOR_CAM(g_BossSpecData, SPEC_MODE_PLAYERS_STAY_DEAD_EVENT, NULL, ASCF_HIDE_LOCAL_PLAYER|ASCF_QUIT_SCREEN|ASCF_ONLY_FIND_TARGET_IF_PLAYER_LIST_POPULATED )
					        ENDIF
						ENDIF
						
						PRINTLN("[Survival] - going to state SURVIVAL_GAME_STATE_RUNNING.")
						SET_CLIENT_GAME_STATE(SURVIVAL_GAME_STATE_RUNNING)
						
					ELSE
						
						IF g_b_OnRaceIntro
							SET_SKYFREEZE_CLEAR(TRUE)
						ENDIF
						
						IF IS_HALLOWEEN_MODE()
						AND IS_BIT_SET(g_FMMC_STRUCT.sSurvivalWaveData.iBitset, ciSurvivalBS_BeamIn)
						AND NOT IS_LOCAL_BIT_SET(ciLocalBool_bBeamedIn)
							UFO_BEAM_ANIMATION_INTROCAM()
							jobIntroData.sNewAnimData.wtWeaponHolster = WEAPONTYPE_INVALID
						ENDIF
						
	                   	IF PROCESS_DEATHMATCH_STARTING_CUT(jobIntroData, DEATHMATCH_TYPE_TDM)//PROCESS_JOB_OVERVIEW_CUT(jobIntroData)
							PRINTLN("[Survival] - PROCESS_DEATHMATCH_STARTING_CUT = TRUE.")
							PRINTLN("[Survival] - going to state SURVIVAL_GAME_INTRO_CAMERA_BLEND.")
							SET_ON_JOB_INTRO(FALSE)
							SET_CLIENT_GAME_STATE(SURVIVAL_GAME_INTRO_CAMERA_BLEND)
						ELSE
							SET_ON_JOB_INTRO(TRUE)
						ENDIF
						
					ENDIF
					
                BREAK
				
				CASE SURVIVAL_GAME_INTRO_CAMERA_BLEND
				
					HIDE_HUD_AND_RADAR_THIS_FRAME()
					HIDE_PLAYER(FALSE)
					DISABLE_PLAYER_CONTROLS_THIS_FRAME()
					DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
					
					IF PROCESS_DEATHMATCH_BLEND_OUT(jobIntroData, DEATHMATCH_TYPE_TDM)
						CLEANUP_ALL_CORONA_FX(FALSE)
						SET_SKYFREEZE_CLEAR()
						PLAY_SOUND_FRONTEND(-1,"Hit","RESPAWN_SOUNDSET")
						ANIMPOSTFX_PLAY("MinigameEndNeutral", 0, FALSE)
						CLEANUP_JOB_INTRO_CUTSCENE(jobIntroData) 
						CLEANUP_NEW_ANIMS(jobIntroData)
						CLEAN_UP_INTRO_STAGE_CAM(g_sTransitionSessionData.ciCam,FMMC_TYPE_SURVIVAL,TRUE) // Also called in DO_TUTORIAL_MISSION_CUTSCENE()
						CLEANUP_CELEBRATION_SCALEFORMS(jobIntroData)
						IF IS_PLAYLIST_SETTING_CHALLENGE_TIME()
							IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FMMC_PL_STCL")
								PRINT_HELP("FMMC_PL_STCL",6000)
							ENDIF
						ENDIF
						SET_HIDOF_OVERRIDE(FALSE,FALSE,0,0,0,0)
						NET_SET_PLAYER_CONTROL(localPlayer, TRUE)
						SET_CLIENT_GAME_STATE(SURVIVAL_GAME_STATE_RUNNING)
					ELSE
						NET_SET_PLAYER_CONTROL(localPlayer, FALSE)
					ENDIF
				
				BREAK
				
                // Main gameplay state.
                CASE SURVIVAL_GAME_STATE_RUNNING
                    // Process client logic.
                    PROCESS_CLIENT()
                BREAK   
                
                CASE SURVIVAL_GAME_STATE_WAITING_TO_LEAVE 
                        
                    SET_CLIENT_GAME_STATE(SURVIVAL_GAME_STATE_END)
                        
                BREAK
                
                CASE SURVIVAL_GAME_STATE_LEAVE  
                        
                    SET_CLIENT_GAME_STATE(SURVIVAL_GAME_STATE_END)
                        
                FALLTHRU

                CASE SURVIVAL_GAME_STATE_END         
					PRINTLN("[Survival] - in SURVIVAL_GAME_STATE_END, calling SCRIPT_CLEANUP.")
                    SCRIPT_CLEANUP()
                BREAK
                
                DEFAULT 
                    #IF IS_DEBUG_BUILD
                        SCRIPT_ASSERT("DM: Problem in SWITCH GET_CLIENT_GAME_STATE(iLocalPart)") 
                    #ENDIF
                BREAK
				
            ENDSWITCH
		ELSE
			IF IS_FAKE_MULTIPLAYER_MODE_SET()
				PRINTLN("[survival] - NOT NETWORK_IS_GAME_IN_PROGRESS - Game is not networked. Exiting")
				EXIT
			ENDIF
        ENDIF
		
        IF bIsLocalPlayerHost
    			
			// Do new participants go straight to the spectator camera?
			MAINTAIN_NEW_PLAYERS_GO_STRAIGHT_TO_SPEC_CAM_FLAG()
			
			// Maintain list of players taking part in script and their current roles on it.
			MAINTAIN_FIGHTING_PARTICIPANTS_LIST()
			
			// This sets up the value that is used in many loops. 
			MAINTAIN_NUM_FIGHTING_PARTICIPANTS()
			
			PROCESS_EOM_RESTART_VOTE_LOGIC()
            
            SWITCH GET_SERVER_GAME_STATE()
                
                CASE SURVIVAL_GAME_STATE_INIT
					
					IF serverBD.iPopulationHandle = -1
						SURVIVAL_SERVER_SET_PED_AND_TRAFFIC_DENSITIES(serverBD.iPopulationHandle, 0.0, 0.0)
					ENDIF
				
                 	SET_SERVER_GAME_STATE(SURVIVAL_GAME_STATE_RUNNING)
                BREAK
                
                CASE SURVIVAL_GAME_STATE_RUNNING                
                        
                    // Process server logic.
                    PROCESS_SERVER()
					
                    // Look for server end conditions
                    IF HAVE_END_GAME_CONDITIONS_BEEN_MET()
                        SET_SERVER_GAME_STATE(SURVIVAL_GAME_STATE_END)
                    ENDIF  
					                    
                BREAK
                
                CASE SURVIVAL_GAME_STATE_WAITING_TO_LEAVE
            		
                    IF ARE_ALL_PARTICIPANTS_WAITING_TO_LEAVE()
                        serverBD.iServerGameState = SURVIVAL_GAME_STATE_END
                    ENDIF
                    
                BREAK
                
                CASE SURVIVAL_GAME_STATE_END 

                BREAK   
                
                DEFAULT 
                    #IF IS_DEBUG_BUILD
                        SCRIPT_ASSERT("MC: Problem in SWITCH GET_SERVER_GAME_STATE()") 
                    #ENDIF
                BREAK
                    
            ENDSWITCH
        ENDIF
        
    ENDWHILE
	
ENDSCRIPT
