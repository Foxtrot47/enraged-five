USING "FM_Survival_Controller_Using.sch"
USING "FM_Survival_Controller_Utility.sch"

#IF IS_DEBUG_BUILD
PROC PRINT_DAMAGED_EVENT_DATA(STRUCT_ENTITY_DAMAGE_EVENT &sEntityID)

    PRINTLN("[Survival_Damage_Events] - sEntityID.VictimIndex = ", NATIVE_TO_INT(sEntityID.VictimIndex))
    PRINTLN("[Survival_Damage_Events] - sEntityID.DamagerIndex = ", NATIVE_TO_INT(sEntityID.DamagerIndex))
    PRINTLN("[Survival_Damage_Events] - sEntityID.Damage = ", sEntityID.Damage)
    
    IF sEntityID.VictimDestroyed
    	PRINTLN("[Survival_Damage_Events] - sEntityID.VictimDestroyed = TRUE")
    ELSE
    	PRINTLN("[Survival_Damage_Events] - sEntityID.VictimDestroyed = TRUE")
    ENDIF
	
    PRINTLN("[Survival_Damage_Events] - sEntityID.WeaponUsed = ", sEntityID.WeaponUsed)
    PRINTLN("[Survival_Damage_Events] - sEntityID.VictimSpeed = ", sEntityID.VictimSpeed)
    PRINTLN("[Survival_Damage_Events] - sEntityID.DamagerSpeed = ", sEntityID.DamagerSpeed)
    
ENDPROC
#ENDIF

FUNC BOOL IS_PED_A_HORDE_PED(PED_INDEX ped, BOOL &bHeliPed, BOOL &bHeavyPed)
    
    INT i, j, iStage
    PED_INDEX hordePed
    
	FOR iStage = 0 TO NUM_SURVIVAL_SUB_STAGES-1
	    REPEAT serverBD.sWaveData.iTotalSquads j
			REPEAT serverBD.sWaveData.iNumPedsInSquad[iStage] i
		        IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD_Enemies_S.structAiSquad[j][iStage].sPed[i].netId)
		            IF NET_TO_PED(serverBD_Enemies_S.structAiSquad[j][iStage].sPed[i].netId) != NULL
		                hordePed = NET_TO_PED(serverBD_Enemies_S.structAiSquad[j][iStage].sPed[i].netId)
		                IF ped = hordePed
							IF serverBD_Enemies_S.structAiSquad[j][iStage].sPed[i].bHeavy
								bHeavyPed = TRUE
							ENDIF
		                    RETURN TRUE     
		                ENDIF
		            ENDIF
		        ENDIF
			ENDREPEAT
	    ENDREPEAT
	ENDFOR
    
	REPEAT MAX_NUM_LAND_VEHICLES j
		REPEAT NUM_LAND_VEH_PEDS i
			IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD_Enemies_V.sSurvivalLandVehicle[j].sPed[i].netId)
				IF NET_TO_PED(serverBD_Enemies_V.sSurvivalLandVehicle[j].sPed[i].netId) != NULL
	                hordePed = NET_TO_PED(serverBD_Enemies_V.sSurvivalLandVehicle[j].sPed[i].netId)
	                IF ped = hordePed
	                    RETURN TRUE     
	                ENDIF
	            ENDIF
			ENDIF
		ENDREPEAT
	ENDREPEAT
	
	REPEAT MAX_NUM_SURVIVAL_HELIS j
		REPEAT NUM_HELI_PEDS i
			IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD_Enemies_V.sSurvivalHeli[j].sPed[i].netId)
				IF NET_TO_PED(serverBD_Enemies_V.sSurvivalHeli[j].sPed[i].netId) != NULL
	                hordePed = NET_TO_PED(serverBD_Enemies_V.sSurvivalHeli[j].sPed[i].netId)
	                IF ped = hordePed
						bHeliPed = TRUE
	                    RETURN TRUE     
	                ENDIF
	            ENDIF
			ENDIF
		ENDREPEAT
	ENDREPEAT
	
    RETURN FALSE
    
ENDFUNC

FUNC BOOL IS_VEHICLE_A_HORDE_VEHICLE(VEHICLE_INDEX vehicle, BOOL &IsHeli, INT &iVehicle)
	
	INT j
	
	REPEAT MAX_NUM_LAND_VEHICLES j
		PRINTLN("[Survival_Damage_Events] - IS_VEHICLE_A_HORDE_VEHICLE - checking land vehicle ", j)
		IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD_Enemies_V.sSurvivalLandVehicle[j].netId)
			PRINTLN("[Survival_Damage_Events] - IS_VEHICLE_A_HORDE_VEHICLE - NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID = TRUE.")
			IF NET_TO_VEH(serverBD_Enemies_V.sSurvivalLandVehicle[j].netId) != NULL
				PRINTLN("[Survival_Damage_Events] - IS_VEHICLE_A_HORDE_VEHICLE - NET_TO_VEH != NULL.")
				PRINTLN("[Survival_Damage_Events] - IS_VEHICLE_A_HORDE_VEHICLE - vehicle id = ", NATIVE_TO_INT(vehicle), ", NET_TO_VEH(serverBD_Enemies_V.sSurvivalLandVehicle[j].netId) id = ", NATIVE_TO_INT(NET_TO_VEH(serverBD_Enemies_V.sSurvivalLandVehicle[j].netId)))
				IF vehicle = NET_TO_VEH(serverBD_Enemies_V.sSurvivalLandVehicle[j].netId)
					PRINTLN("[Survival_Damage_Events] - IS_VEHICLE_A_HORDE_VEHICLE - vehicle passed = NET_TO_VEH.")
					iVehicle = j
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	REPEAT MAX_NUM_SURVIVAL_HELIS j
		PRINTLN("[Survival_Damage_Events] - IS_VEHICLE_A_HORDE_VEHICLE - checking heli ", j)
		IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD_Enemies_V.sSurvivalHeli[j].netId)	
			PRINTLN("[Survival_Damage_Events] - IS_VEHICLE_A_HORDE_VEHICLE - NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID = TRUE.")
			IF NET_TO_VEH(serverBD_Enemies_V.sSurvivalHeli[j].netId) != NULL
				PRINTLN("[Survival_Damage_Events] - IS_VEHICLE_A_HORDE_VEHICLE - NET_TO_VEH != NULL.")
				IF vehicle = NET_TO_VEH(serverBD_Enemies_V.sSurvivalHeli[j].netId)
					PRINTLN("[Survival_Damage_Events] - IS_VEHICLE_A_HORDE_VEHICLE - vehicle passed = NET_TO_VEH.")
					IsHeli = TRUE
					iVehicle = j
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	PRINTLN("[Survival_Damage_Events] - IS_VEHICLE_A_HORDE_VEHICLE - vehicle passed is not a horde vehicle.")
    RETURN FALSE
    
ENDFUNC

FUNC BOOL GET_IS_ENTITY_DAMAGER_LOCAL_PLAYER(ENTITY_INDEX &entity, BOOL bDamagerIsPed)
    
    VEHICLE_INDEX veh
    
    IF NOT DOES_ENTITY_EXIST(LocalPlayerPed)
		RETURN FALSE
    ENDIF
    
    IF IS_ENTITY_DEAD(LocalPlayerPed)
		RETURN FALSE
    ENDIF
    
    IF bDamagerIsPed
        IF GET_PED_INDEX_FROM_ENTITY_INDEX(entity) != LocalPlayerPed
			RETURN FALSE
        ENDIF
    ELSE
        IF NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed, TRUE)
			RETURN FALSE
        ENDIF
        
        IF NOT DOES_ENTITY_EXIST(GET_VEHICLE_PED_IS_USING(LocalPlayerPed))
			RETURN FALSE
        ENDIF
        
        veh = GET_VEHICLE_PED_IS_USING(LocalPlayerPed)
        
        IF NOT IS_VEHICLE_DRIVEABLE(veh)
			RETURN FALSE
        ENDIF
        
        IF GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(entity) != veh
			RETURN FALSE
        ENDIF
    ENDIF
    
    RETURN TRUE
    
ENDFUNC

FUNC PLAYER_INDEX GET_ENTITY_DAMAGER_PLAYER_INDEX(ENTITY_INDEX &entity, BOOL bDamagerIsPed)
    
    VEHICLE_INDEX veh
    PED_INDEX pedId
    
    IF bDamagerIsPed
        pedId = GET_PED_INDEX_FROM_ENTITY_INDEX(entity)
        
        IF NOT IS_PED_A_PLAYER(pedId)
			RETURN INVALID_PLAYER_INDEX()
        ENDIF
    ELSE
        IF NOT IS_ENTITY_A_VEHICLE(entity)
			RETURN INVALID_PLAYER_INDEX()
        ENDIF
        
        veh = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(entity)
        
        IF IS_VEHICLE_SEAT_FREE(veh)
			RETURN INVALID_PLAYER_INDEX()
        ENDIF
        
        pedId = GET_PED_IN_VEHICLE_SEAT(veh)
        
        IF NOT IS_PED_A_PLAYER(pedId)
			RETURN INVALID_PLAYER_INDEX()
        ENDIF
            
    ENDIF
    
    RETURN NETWORK_GET_PLAYER_INDEX_FROM_PED(pedId)
    
ENDFUNC

/// PURPOSE:
///    Tells if a ped has been killed by a head shot
/// PARAMS:
///    pedID - ped ID
/// RETURNS:
///    True or False
FUNC BOOL CHECK_HEADSHOT(PED_INDEX pedID)

    IF NOT DOES_ENTITY_EXIST(pedID)
        RETURN FALSE
    ENDIF
    
    PED_BONETAG ePedBonetag = BONETAG_NULL
    
    IF IS_ENTITY_DEAD(pedID) OR IS_PED_INJURED(pedID)
        IF NOT GET_PED_LAST_DAMAGE_BONE(pedID, ePedBonetag)     
            RETURN FALSE
        ENDIF

        RETURN ePedBonetag = BONETAG_HEAD OR ePedBonetag = BONETAG_NECK
    ENDIF
    
    RETURN FALSE
    
ENDFUNC

PROC CAP_HORDE_KILL_XP(INT &iXp)
	IF iXp > 1000
		iXp = 1000
		PRINTLN("[Survival] - CAP_HORDE_KILL_XP - iXp is > 1000, capping. iXp now equals ", iXp)
	ENDIF
ENDPROC

PROC GIVE_HORDE_KILL_XP(BOOL bVehicleKilled, VEHICLE_INDEX victimVehicle = NULL, PED_INDEX victimPed = NULL)
	
	IF playerBD[iLocalPart].iKillsWithXP >= MAX_NUMBER_OF_KILLS_WITH_XP
		PRINTLN("GIVE_HORDE_KILL_XP - This player has hit the cap of kills that grant XP.")
		EXIT
	ENDIF
	  
	 
	INT iFinalXpReward
	INT iXpReward = 10
	FLOAT fXp
	
	IF bVehicleKilled
		iXpReward = 25
	ENDIF
	
    // Get xp to give.
    iXpReward = (iXpReward * GET_KILL_STREAK_XP_MULTIPLIER())
	
	IF iXpReward < 1
		iXpReward = 1
	ENDIF
	
    // Give XP for my kill.
	IF bVehicleKilled
	
		PRINTLN("[Survival] - GIVE_HORDE_KILL_XP - XP before conversion to float = ", iXpReward)
		
		fXp = TO_FLOAT(iXpReward)
		PRINTLN("[Survival] - GIVE_HORDE_KILL_XP - XP after conversion to float = ", fXp)
		
		fXp = (fXp*g_sMPTunables.fxp_tunable_Survival_Vehicle_destroyed)
		PRINTLN("[Survival] - GIVE_HORDE_KILL_XP - fxp_tunable_Survival_Vehicle_destroyed = ", g_sMPTunables.fxp_tunable_Survival_Vehicle_destroyed, ", XP after tuneable adjustment = ", fXp)
		
		iFinalXpReward = ROUND(fXp)
		PRINTLN("[Survival] - GIVE_HORDE_KILL_XP - XP after conversion back to int = ", iFinalXpReward)
		
		CAP_HORDE_KILL_XP(iFinalXpReward)
		PRINTLN("[Survival] - GIVE_HORDE_KILL_XP - XP after cap = ", iFinalXpReward)
		
		iFinalXpReward = GIVE_LOCAL_PLAYER_FM_XP_WITH_ANIMATION(eXPTYPE_STANDARD, victimVehicle, "XPT_KAIE",XPTYPE_ACTION, XPCATEGORY_ACTION_SURVIVAL_KILLS, iFinalXpReward, 1, -1, "", TRUE)
		PRINTLN("[Survival] - GIVE_HORDE_KILL_XP - XP final value = ", iFinalXpReward)
		
		#IF IS_DEBUG_BUILD
		IF (serverBD.sWaveData.iWaveCount-1) >= 0
		AND (serverBD.sWaveData.iWaveCount-1) < serverBD.iNumberOfWaves
			iVehsDestroyedXp[(serverBD.sWaveData.iWaveCount-1)] += iFinalXpReward
		ENDIF
		#ENDIF
		
	ELSE
		
		PRINTLN("[Survival] - GIVE_HORDE_KILL_XP - XP before conversion to float = ", iXpReward)
		
		fXp = TO_FLOAT(iXpReward)
		PRINTLN("[Survival] - GIVE_HORDE_KILL_XP - XP after conversion to float = ", fXp)
		
		fXp = (fXp*g_sMPTunables.fxp_tunable_Survival_enemy_Kill)
		PRINTLN("[Survival] - GIVE_HORDE_KILL_XP - fxp_tunable_Survival_enemy_Kill = ", g_sMPTunables.fxp_tunable_Survival_enemy_Kill, ", XP after tuneable adjustment = ", fXp)
		
		iFinalXpReward = ROUND(fXp)
		PRINTLN("[Survival] - GIVE_HORDE_KILL_XP - XP after conversion back to int = ", iFinalXpReward)
		
		CAP_HORDE_KILL_XP(iFinalXpReward)
		PRINTLN("[Survival] - GIVE_HORDE_KILL_XP - XP after cap = ", iFinalXpReward)
		
    	iFinalXpReward = GIVE_LOCAL_PLAYER_FM_XP_WITH_ANIMATION(eXPTYPE_STANDARD, victimPed, "XPT_KAIE",XPTYPE_ACTION, XPCATEGORY_ACTION_SURVIVAL_KILLS, iFinalXpReward, 1, -1, "", TRUE)
		PRINTLN("[Survival] - GIVE_HORDE_KILL_XP - XP final value = ", iFinalXpReward)
		
		#IF IS_DEBUG_BUILD
		IF (serverBD.sWaveData.iWaveCount-1) >= 0
		AND (serverBD.sWaveData.iWaveCount-1) < serverBD.iNumberOfWaves
			iKillsXp[(serverBD.sWaveData.iWaveCount-1)] += iFinalXpReward
		ENDIF
		#ENDIF
		
	ENDIF
	
    playerBD[iLocalPart].iXpGained += iFinalXpReward
	playerBD[iLocalPart].iKillsWithXP++
	PRINTLN("[Survival] - GIVE_HORDE_KILL_XP - playerBD[iLocalPart].iKillsWithXP: ", playerBD[iLocalPart].iKillsWithXP)
ENDPROC

PROC INCREMENT_KILL_STATS(PED_INDEX victimPed, BOOL bVehicleKilled)
	// Increment kill stats.
	IF NOT bVehicleKilled
   		playerBD[iLocalPart].iMyKills++
    	playerBD[iLocalPart].iMyKillsThisWave++
    
		// Headshots.
		IF CHECK_HEADSHOT(victimPed)
			playerBD[iLocalPart].iMyHeadshots++
		ENDIF
	
	    // For kill streak.
	    structKillStreakData.iNumKillsInARow++
		PRINTLN("INCREMENT_KILL_STATS - playerBD[iLocalPart].iMyKills: ", playerBD[iLocalPart].iMyKills)
		PRINTLN("INCREMENT_KILL_STATS - playerBD[iLocalPart].iMyKillsThisWave: ", playerBD[iLocalPart].iMyKillsThisWave)
		PRINTLN("INCREMENT_KILL_STATS - playerBD[iLocalPart].iMyHeadshots: ", playerBD[iLocalPart].iMyHeadshots)
		PRINTLN("INCREMENT_KILL_STATS - structKillStreakData.iNumKillsInARow: ", structKillStreakData.iNumKillsInARow)
	ELSE
		playerBD[iLocalPart].iMyVehicleKills++
		PRINTLN("INCREMENT_KILL_STATS - playerBD[iLocalPart].iMyVehicleKills: ", playerBD[iLocalPart].iMyVehicleKills)
	ENDIF
ENDPROC

FUNC BOOL ARE_ANY_POLICE_PEDS_IN_VEHICLE(VEHICLE_INDEX viVeh)
	INT i
	FOR i = -1 TO ENUM_TO_INT(VS_EXTRA_RIGHT_3)
		PED_INDEX piPed = GET_PED_IN_VEHICLE_SEAT(viVeh, INT_TO_ENUM(VEHICLE_SEAT, i))
		IF DOES_ENTITY_EXIST(piPed)
			IF IS_MODEL_AMBIENT_MISSION_COP(GET_ENTITY_MODEL(piPed))
			AND IS_MODEL_AMBIENT_COP_FOR_RP(GET_ENTITY_MODEL(piPed))
				RETURN TRUE
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC

PROC SET_LAST_DAMAGER_AIR_VEHICLE(VEHICLE_INDEX victimVeh, PED_INDEX damagerPlayer)
	INT iAirVeh
	FOR iAirVeh = 0 TO MAX_SURV_HELI-1
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD_Enemies_V.sSurvivalHeli[iAirVeh].netId)
			VEHICLE_INDEX viVeh
			viVeh = NET_TO_VEH(serverBD_Enemies_V.sSurvivalHeli[iAirVeh].netId)
			IF victimVeh = viVeh
				serverBD_Enemies_V.sSurvivalHeli[iAirVeh].piLastDamager = damagerPlayer
				PRINTLN("SET_LAST_DAMAGER_AIR_VEHICLE - Updating last damager for Air Vehicle ", iAirVeh, " to ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX_FROM_PED(damagerPlayer)))
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

FUNC BOOL IS_HORDE_VEHICLE_EMPTY(BOOL bAir, INT iVeh)
	BOOL bReturn
	
	IF !bAir
		bReturn = serverBD_Enemies_V.sSurvivalLandVehicle[iVeh].bEmpty
	ENDIF
	
	RETURN bReturn
ENDFUNC

PROC PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT(INT iCount)
    
	BOOL bHeliPed, bIsHeli, bHeavyPed
	
    PLAYER_INDEX playerId
    PED_INDEX victimPed, damagerPed
	VEHICLE_INDEX victimVeh
    BOOL bDamagerIsPed
    STRUCT_ENTITY_DAMAGE_EVENT sEntityID
   	INT iVehicle
	bIHaveDamagedAnEnemy = FALSE
	
    // Grab the event data.
    GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, sEntityID, SIZE_OF(sEntityID))
    
    // Print event data.
    #IF IS_DEBUG_BUILD
    PRINTLN("[Survival_Damage_Events] - got event data.")
    PRINT_DAMAGED_EVENT_DATA(sEntityID)
    #ENDIF
    
    // Process event.
    IF DOES_ENTITY_EXIST(sEntityID.VictimIndex)

        PRINTLN("[Survival_Damage_Events] - event ", iCount, " - entity exists.")
        
        IF sEntityID.VictimDestroyed
    
            PRINTLN("[Survival_Damage_Events] - event ", iCount, " - entity was killed.")
            
            IF IS_ENTITY_A_PED(sEntityID.VictimIndex)
        
                PRINTLN("[Survival_Damage_Events] - event ", iCount, " - entity was a ped.")
                
                victimPed = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.VictimIndex)
                
                IF NOT IS_PED_A_PLAYER(victimPed)
            
                    PRINTLN("[Survival_Damage_Events] - event ", iCount, " - entity was an ai ped.")
                    
					IF DOES_ENTITY_EXIST(sEntityID.DamagerIndex)
						
						bDamagerIsPed = IS_ENTITY_A_PED(sEntityID.DamagerIndex)
						
                        IF IS_ENTITY_A_PED(sEntityID.DamagerIndex)
                    
                            PRINTLN("[Survival_Damage_Events] - event ", iCount, " - damager was a ped.")
                            
                            damagerPed = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.DamagerIndex)
                            
							PRINTLN("[Survival_Damage_Events] - event ", iCount, " - ped was a horde ped.")
							
							IF IS_PED_A_HORDE_PED(victimPed, bHeliPed, bHeavyPed)
							
								PRINTLN("[Survival_Damage_Events] - event ", iCount, " - ped was a horde ped.")
								
								IF NOT bHeliPed
									PRINTLN("[Survival_Damage_Events] - event ", iCount, " - ped was not a heli horde ped.")
									IF bIsLocalPlayerHost
										SERVER_INCREMENT_EVENT_KILLS()
										serverBd.iNumKillsThisSubStageFromEvents[serverBd.sWaveData.eStage]++
										PRINTLN("[Survival_Damage_Events] - killed a horde enemy, iNumKillsThisWaveFromEvents now equals ", serverBD.iNumKillsThisWaveFromEvents)
										PRINTLN("[Survival_Damage_Events] - killed a horde enemy, iNumKillsThisSubStageFromEvents now equals ", serverBd.iNumKillsThisSubStageFromEvents[serverBd.sWaveData.eStage])
									ENDIF
								ENDIF
								
	                            IF IS_PED_A_PLAYER(damagerPed)
									
									PRINTLN("[Survival_Damage_Events] - event ", iCount, " - damager was a player.")
									  	
	                                playerId = GET_ENTITY_DAMAGER_PLAYER_INDEX(sEntityID.DamagerIndex, bDamagerIsPed)
	                                
									IF bHeavyPed
										BROADCAST_SURVIVAL_HEAVY_UNIT_KILLED(playerID)
									ENDIF
									
									IF (playerId = localPlayer)
										
										PRINTLN("[Survival_Damage_Events] - event ", iCount, " - entity was a horde ped.")
										
								        PRINTLN("[Survival_Damage_Events] - I killed a horde ped! Give my rewards!")
								       	
										bIHaveDamagedAnEnemy = TRUE
										INCREMENT_KILL_STATS(victimPed, FALSE)
										IF NOT IS_LOCAL_BIT_SET(ciLocalBool_bInvalidWaveForKillRP)
											GIVE_HORDE_KILL_XP(FALSE, NULL, victimPed)
										ENDIF
										
								        // Increment horde kills stat.
								        INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_HORDKILLS, 1)
								        
										iRefreshDpad++
	    								
	                              	ENDIF
	                            
								ENDIF
							ELSE
								PRINTLN("[Survival_Damage_Events] - Was not a horde ped")
							ENDIF
                        
						ENDIF
					
                    ENDIF
                		
                ELSE
                        
                    playerId = NETWORK_GET_PLAYER_INDEX_FROM_PED(victimPed)
                    
                    PRINTLN("[Survival_Damage_Events] - event ", iCount, " - victim was a player. Id = ", NATIVE_TO_INT(playerId))
                    
                    IF playerId != localPlayer
                        PRINTLN("[Survival_Damage_Events] - event ", iCount, " - victim was a player who is not me.")
                        IF NETWORK_IS_PLAYER_A_PARTICIPANT(playerId)
                            PRINTLN("[Survival_Damage_Events] - event ", iCount, " - victim was a participant. Id = ", NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(playerId)))
                            PRINTLN("[Survival_Damage_Events] - event ", iCount, " - displaying big message")
                            SETUP_NEW_BIG_MESSAGE_WITH_PLAYER(BIG_MESSAGE_SURVIVAL_TM_DIED, playerId)
                        ENDIF
                    ELSE
                        PRINTLN("[Survival_Damage_Events] - event ", iCount, " - victim was a player who is me, no big message because I know I'm dead.")
                    ENDIF
                    
                ENDIF
                
			ELSE
			
				IF IS_ENTITY_A_VEHICLE(sEntityID.VictimIndex)
					
					PRINTLN("[Survival_Damage_Events] - event ", iCount, " - entity was a vehicle.")
					
					victimVeh = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(sEntityID.VictimIndex)
					
					IF DOES_ENTITY_EXIST(sEntityID.DamagerIndex)
						
						bDamagerIsPed = IS_ENTITY_A_PED(sEntityID.DamagerIndex)
						
                        IF IS_ENTITY_A_PED(sEntityID.DamagerIndex)
                    
                            PRINTLN("[Survival_Damage_Events] - event ", iCount, " - damager was a ped.")
                            
                            damagerPed = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.DamagerIndex)
                            
							IF IS_VEHICLE_A_HORDE_VEHICLE(victimVeh, bIsHeli, iVehicle)
								
								IF bIsHeli
									PRINTLN("[Survival_Damage_Events] - event ", iCount, " - vehicle was a heli.")
									IF bIsLocalPlayerHost
										SERVER_INCREMENT_EVENT_KILLS()
										serverBd.iNumKillsThisSubStageFromEvents[serverBd.sWaveData.eStage]++
										PRINTLN("[Survival_Damage_Events] - killed a horde enemy, iNumKillsThisWaveFromEvents now equals ", serverBD.iNumKillsThisWaveFromEvents)
										PRINTLN("[Survival_Damage_Events] - killed a horde enemy, iNumKillsThisSubStageFromEvents now equals ", serverBd.iNumKillsThisSubStageFromEvents[serverBd.sWaveData.eStage])
									ENDIF
								ENDIF
								
	                            IF IS_PED_A_PLAYER(damagerPed)
	                                    
	                                PRINTLN("[Survival_Damage_Events] - event ", iCount, " - damager was a player.")
	                                	
		                           	playerId = GET_ENTITY_DAMAGER_PLAYER_INDEX(sEntityID.DamagerIndex, bDamagerIsPed)
	                            	
									IF IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(victimVeh))
									OR IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(victimVeh))
										BROADCAST_SURVIVAL_AIR_VEHICLE_DESTROYED(playerId)
										PRINTLN("[Survival_Damage_Events] - event ", iCount, " - entity was horde air support, printing ticker for who killed it.")
									ENDIF
									
									IF playerId = localPlayer
										
										PRINTLN("[Survival_Damage_Events] - event ", iCount, " - entity was a horde vehicle.")
									
										PRINTLN("[Survival_Damage_Events] - I killed a horde vehicle! Give my rewards!")
										
										bIHaveDamagedAnEnemy = TRUE
										INCREMENT_KILL_STATS(NULL, TRUE)
										IF NOT IS_LOCAL_BIT_SET(ciLocalBool_bInvalidWaveForKillRP)
											BOOL bVehEmpty = IS_VEHICLE_EMPTY(victimVeh, DEFAULT, DEFAULT, DEFAULT, TRUE)
											IF NOT bIsHeli
												bVehEmpty = IS_HORDE_VEHICLE_EMPTY(bIsHeli, iVehicle)
											ENDIF
											PRINTLN("[Survival_Damage_Events] - NOT bVehEmpty: ", PICK_STRING(!bVehEmpty, "TRUE", "FALSE"))
											IF !bVehEmpty
												GIVE_HORDE_KILL_XP(TRUE, victimVeh)
											ENDIF
										ENDIF
									
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
            ENDIF
		ELSE			
			IF IS_ENTITY_A_PED(sEntityID.VictimIndex)
	        	
				victimPed = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.VictimIndex)
				
				IF NOT IS_PED_A_PLAYER(victimPed)
					IF DOES_ENTITY_EXIST(sEntityID.DamagerIndex)
							
						bDamagerIsPed = IS_ENTITY_A_PED(sEntityID.DamagerIndex)
						
	                    IF IS_ENTITY_A_PED(sEntityID.DamagerIndex)
	                	                        
	                        damagerPed = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.DamagerIndex)
	                        
	                        IF IS_PED_A_PLAYER(damagerPed)
																  	
	                            playerId = GET_ENTITY_DAMAGER_PLAYER_INDEX(sEntityID.DamagerIndex, bDamagerIsPed)
	                            
								IF (playerId = localPlayer)
								
									bIHaveDamagedAnEnemy = TRUE
									
								ENDIF
							ENDIF
						ENDIF
	            	ENDIF
	        	ENDIF
			ELSE
				IF IS_ENTITY_A_VEHICLE(sEntityID.VictimIndex)
					
					victimVeh = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(sEntityID.VictimIndex)
					
					IF DOES_ENTITY_EXIST(sEntityID.DamagerIndex)
						
						bDamagerIsPed = IS_ENTITY_A_PED(sEntityID.DamagerIndex)
						
                        IF IS_ENTITY_A_PED(sEntityID.DamagerIndex)
                            
                            damagerPed = GET_PED_INDEX_FROM_ENTITY_INDEX(sEntityID.DamagerIndex)
                            
							IF IS_VEHICLE_A_HORDE_VEHICLE(victimVeh, bIsHeli, iVehicle)
								
								IF bIsHeli
									IF IS_PED_A_PLAYER(damagerPed)
										  	
		                                playerId = GET_ENTITY_DAMAGER_PLAYER_INDEX(sEntityID.DamagerIndex, bDamagerIsPed)
										
										SET_LAST_DAMAGER_AIR_VEHICLE(victimVeh, damagerPed)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
    ENDIF
                                            
ENDPROC

PROC PROCESS_PLAYER_ALPHA_CHANGE_EVENT(INT iCount)
	SCRIPT_EVENT_DATA_FMMC_PLAYER_ALPHA_WITH_WEAPON eEventData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, eEventData, SIZE_OF(eEventData))
		PRINTLN("PROCESS_PLAYER_ALPHA_CHANGE_EVENT - Received ")
		IF eEventData.iPart != -1
		AND eEventData.iPart != iLocalPart
			PLAYER_INDEX piPlayer = INT_TO_PLAYERINDEX(eEventData.iPart)
			PED_INDEX playerPed = GET_PLAYER_PED(piPlayer)
			PRINTLN("PROCESS_PLAYER_ALPHA_CHANGE_EVENT - Part Valid ", eEventData.iPart)
			IF DOES_ENTITY_EXIST(playerPed)
				PRINTLN("PROCESS_PLAYER_ALPHA_CHANGE_EVENT - Player ped exists")
				IF eEventData.iAlphaValue != 255
					PRINTLN("PROCESS_PLAYER_ALPHA_CHANGE_EVENT - Setting Alpha ", eEventData.iAlphaValue)
					SET_ENTITY_ALPHA(playerPed, eEventData.iAlphaValue, FALSE)
					IF DOES_ENTITY_EXIST(GET_CURRENT_PED_WEAPON_ENTITY_INDEX(playerPed))
						SET_ENTITY_ALPHA(GET_CURRENT_PED_WEAPON_ENTITY_INDEX(playerPed), eEventData.iAlphaValue, FALSE)
					ENDIF
				ELSE
					RESET_ENTITY_ALPHA(playerPed)
					RESET_ENTITY_ALPHA(GET_CURRENT_PED_WEAPON_ENTITY_INDEX(playerPed))
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_COLLECTED_PICKUP_EVENT(INT iCount)
    
    STRUCT_PICKUP_EVENT PickupData
    TEXT_LABEL_15 labelTemp
    
    IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, PickupData, SIZE_OF(PickupData))
        IF IS_NET_PLAYER_OK(PickupData.PlayerIndex)
            IF PickupData.PlayerIndex = localPlayer
                IF NETWORK_IS_PLAYER_A_PARTICIPANT(PickupData.PlayerIndex)
	                labelTemp = GET_CORRECT_TICKER_STRING(PickupData.PickupType)
					IF NOT IS_STRING_NULL_OR_EMPTY(labelTemp)
	               		PRINT_WEAPON_TICKER(labelTemp)
					ENDIF
                ENDIF
            ENDIF
        ENDIF
    ENDIF
                            
ENDPROC

PROC PROCESS_SURVIVAL_AIR_VEHICLE_DESTROYED(INT iCount)
	SCRIPT_EVENT_DATA_SURVIVAL_AIR_VEHICLE_DESTROYED EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_CASINO_SURVIVAL_AIR_VEHICLE_DESTROYED
			IF NETWORK_IS_PLAYER_ACTIVE(EventData.piDestroyerPlayerID)
				PRINT_TICKER_WITH_PLAYER_NAME("SC_DAS", EventData.piDestroyerPlayerID)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SURVIVAL_HEAVY_UNIT_KILLED(INT iCount)
	SCRIPT_EVENT_DATA_SURVIVAL_HEAVY_UNIT_KILLED EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_SURVIVAL_HEAVY_UNIT_KILLED
			IF NETWORK_IS_PLAYER_ACTIVE(EventData.piDestroyerPlayerID)
				PRINT_TICKER_WITH_PLAYER_NAME("SC_KHU", EventData.piDestroyerPlayerID)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_FMMC_SCRIPT_EVENT(INT iCount)
	STRUCT_EVENT_COMMON_DETAILS Details
                            
    GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, Details, SIZE_OF(Details))
    
    SWITCH Details.Type
		CASE SCRIPT_EVENT_CASINO_SURVIVAL_AIR_VEHICLE_DESTROYED
			IF g_sBlockedEvents.bSCRIPT_EVENT_CASINO_SURVIVAL_AIR_VEHICLE_DESTROYED_BLOCKED
				BREAK
			ENDIF
			PRINTLN("PROCESS_FMMC_SCRIPT_EVENT - SCRIPT_EVENT_CASINO_SURVIVAL_AIR_VEHICLE_DESTROYED received")
			PROCESS_SURVIVAL_AIR_VEHICLE_DESTROYED(iCount)
		BREAK
		CASE SCRIPT_EVENT_SURVIVAL_HEAVY_UNIT_KILLED
			IF g_sBlockedEvents.bSCRIPT_EVENT_SURVIVAL_HEAVY_UNIT_KILLED_BLOCKED
				BREAK
			ENDIF
			PRINTLN("PROCESS_FMMC_SCRIPT_EVENT - SCRIPT_EVENT_SURVIVAL_HEAVY_UNIT_KILLED received")
			PROCESS_SURVIVAL_HEAVY_UNIT_KILLED(iCount)
		BREAK
		CASE SCRIPT_EVENT_FMMC_PLAYER_ALPHA_WITH_WEAPON
			IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_PLAYER_ALPHA_WITH_WEAPON_BLOCKED
				BREAK
			ENDIF
			PRINTLN("PROCESS_FMMC_SCRIPT_EVENT - SCRIPT_EVENT_FMMC_PLAYER_ALPHA_WITH_WEAPON received")
			PROCESS_PLAYER_ALPHA_CHANGE_EVENT(iCount)
		BREAK
	ENDSWITCH
ENDPROC

// PURPOSE : Process all the events received in the events queue
PROC PROCESS_EVENTS()

    INT iCount
    EVENT_NAMES ThisScriptEvent
	
    #IF IS_DEBUG_BUILD
        STRUCT_EVENT_COMMON_DETAILS Details
    #ENDIF
    
    //process the events
    REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_NETWORK) iCount

        ThisScriptEvent = GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_NETWORK, iCount)
        
        #IF IS_DEBUG_BUILD
            IF ThisScriptEvent = EVENT_NETWORK_SCRIPT_EVENT
                GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, Details, SIZE_OF(Details))
                IF Details.Type != SCRIPT_EVENT_OHD_TALKING
                AND Details.Type != SCRIPT_EVENT_OHD_TALKING_RESET
                    PRINTLN("[Survival] - [Events] - ThisScriptEvent = ", ENUM_TO_INT(ThisScriptEvent))
                ENDIF
            ENDIF
        #ENDIF
        
        SWITCH ThisScriptEvent
            
            CASE EVENT_NETWORK_PLAYER_COLLECTED_PICKUP
                PRINTLN("[Survival] - [Events] - recieved event EVENT_NETWORK_PLAYER_COLLECTED_PICKUP")
                PROCESS_COLLECTED_PICKUP_EVENT(iCount)
            BREAK
            
            CASE EVENT_NETWORK_DAMAGE_ENTITY
            	PRIVATE_PROCESS_ENTITY_DAMAGED_EVENT(iCount)
            BREAK
			
			CASE EVENT_NETWORK_SCRIPT_EVENT 
				PROCESS_FMMC_SCRIPT_EVENT(iCount)
			BREAK
           	
        ENDSWITCH
            
    ENDREPEAT
    
ENDPROC
