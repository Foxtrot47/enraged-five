USING "FM_Survival_Controller_Using.sch"
USING "FM_Survival_Controller_Utility.sch"
USING "FM_Survival_Controller_HUD.sch"
USING "FM_Survival_Controller_Players.sch"
USING "FM_Survival_Controller_Waves.sch"

FUNC INT GET_FMMC_END_OF_MISSION_STATUS_FOR_END_HORDE_REASON()
    
	IF NETWORK_IS_GAME_IN_PROGRESS()
	    IF NOT IS_BIT_SET(playerBD[iLocalPart].iBitset, BITSET_PLAYER_ABANDONED_HORDE)
	        SWITCH GET_END_HORDE_REASON()
	            CASE eENDHORDEREASON_BEAT_ALL_WAVES RETURN ciFMMC_END_OF_MISSION_STATUS_PASSED
	            CASE eENDHORDEREASON_ALL_PLAYERS_DEAD RETURN ciFMMC_END_OF_MISSION_STATUS_FAILED
	        ENDSWITCH
	    ENDIF
	ENDIF
	
    RETURN ciFMMC_END_OF_MISSION_STATUS_CANCELLED
    
ENDFUNC

/// PURPOSE:
///    Gets if end game conditions have been fulfilled.
/// RETURNS:
///    TRUE if conditions met, FALSE if not.
FUNC BOOL HAVE_END_GAME_CONDITIONS_BEEN_MET()
    RETURN FALSE
ENDFUNC

PROC DO_END_OF_MODE_LEADERBOARD_DATA_SORT()
	IF NOT sEndBoardData.bPopulatedHudPlayerArray
		GET_PLAYER_KILLS(sEndBoardData, TRUE)
		POPULATE_HUD_PLAYER_ARRAY(sEndBoardData, bDoWaveCompleteHudPrints, TRUE)
		sEndBoardData.bPopulatedHudPlayerArray = TRUE
	ENDIF
ENDPROC

FUNC INT GET_MY_LEADERBOARD_POSITION_FOR_PLAYSTATS()
	
	INT i
	
	REPEAT MAX_NUMBER_HORDE_FIGHTING_PARTICIPANTS i
		IF sEndBoardData.hudPlayers[i] = localPlayer
			RETURN (i+1)
		ENDIF
	ENDREPEAT
	
	RETURN 0
	
ENDFUNC

PROC CLEAR_AREA_OF_ENTITIES()
	CLEAR_AREA_OF_VEHICLES(serverBD.vBoundsMiddle, 500)
	CLEAR_AREA_OF_PEDS(serverBD.vBoundsMiddle, 500)
ENDPROC

FUNC BOOL CLEANUP_ALL_ENEMIES()
	DEBUG_PRINTCALLSTACK()
	INT i, j, iStage
	BOOL bClean = TRUE
	PRINTLN("CLEANUP_ALL_ENEMIES called")
	
	FOR iStage = 0 TO NUM_SURVIVAL_SUB_STAGES-1
		REPEAT serverBD.sWaveData.iTotalSquads j
			REPEAT serverBD.sWaveData.iNumPedsInSquad[iStage] i
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD_Enemies_S.structAiSquad[j][iStage].sPed[i].netId)
					CLEANUP_NET_ID(serverBD_Enemies_S.structAiSquad[j][iStage].sPed[i].netId)
					bClean = FALSE
				ENDIF
			ENDREPEAT
		ENDREPEAT
	ENDFOR
	
	REPEAT MAX_NUM_SURVIVAL_HELIS i
		REPEAT NUM_HELI_PEDS j
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD_Enemies_V.sSurvivalHeli[i].sPed[j].netId)
				CLEANUP_NET_ID(serverBD_Enemies_V.sSurvivalHeli[i].sPed[j].netId)
				bClean = FALSE
			ENDIF
		ENDREPEAT
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD_Enemies_V.sSurvivalHeli[i].netId)
			CLEANUP_NET_ID(serverBD_Enemies_V.sSurvivalHeli[i].netId)
			bClean = FALSE
		ENDIF
	ENDREPEAT
	
	REPEAT MAX_NUM_LAND_VEHICLES j
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD_Enemies_V.sSurvivalLandVehicle[j].netId)
			CLEANUP_NET_ID(serverBD_Enemies_V.sSurvivalLandVehicle[j].netId)
			bClean = FALSE
		ENDIF
		REPEAT NUM_LAND_VEH_PEDS i
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD_Enemies_V.sSurvivalLandVehicle[j].sPed[i].netId)
				CLEANUP_NET_ID(serverBD_Enemies_V.sSurvivalLandVehicle[j].sPed[i].netId)
				bClean = FALSE
			ENDIF
		ENDREPEAT
	ENDREPEAT
	
	CLEAR_AREA_OF_ENTITIES()
	
	RETURN bClean
	
ENDFUNC

PROC SERVER_CLEANUP()
	IF NOT bIsLocalPlayerHost
		EXIT
	ENDIF
	IF NETWORK_IS_SIGNED_ONLINE()
	AND NETWORK_IS_GAME_IN_PROGRESS()
		IF serverBD.iPopulationHandle > -1
			SURVIVAL_SERVER_CLEAR_PED_AND_TRAFFIC_DENSITIES(serverBD.iPopulationHandle)
			PRINTLN("SERVER_CLEANUP - Removed server Population multiplier area")
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE:
///    Performs script cleanup. Should be called upon script termination.
PROC SCRIPT_CLEANUP()

	DEBUG_PRINTCALLSTACK()
    
    INT i, j
    TEXT_LABEL_23 tl23Event
	
	#IF FEATURE_GEN9_EXCLUSIVE
	END_SERIES_UDS_ACTIVITY(g_FMMC_STRUCT.iRootContentIDHash)
	#ENDIF
	
	//Recache for clean up
	CACHE_PLAYER_DATA()
	
	SET_HIDOF_OVERRIDE(FALSE,FALSE,0,0,0,0)
	
	IF IS_FAKE_MULTIPLAYER_MODE_SET()
		TRIGGER_MUSIC_EVENT("MP_SM_STOP_TRACK")
	ENDIF
	
	//gdisablerankupmessage = FALSE
	SET_DISABLE_RANK_UP_MESSAGE(FALSE)
	
	UNLOAD_ALL_COMMON_LEADERBOARD_ELEMENTS(LeaderboardPlacement)
	CLEANUP_END_JOB_VOTING(nextJobStruct)
	
	CLEAR_RANK_REDICTION_DETAILS() 
	
	g_bMissionEnding = FALSE
	
	NETWORK_OVERRIDE_TEAM_RESTRICTIONS(0, FALSE)
	
	SERVER_CLEANUP()
	
	// Bugstar 2218090.
	SET_DAILY_OBJECTIVE_TICKERS_ENABLED_ON_MISSION(FALSE)
	
	CLEANUP_CELEBRATION_SCREEN(sCelebrationData)
	SET_CELEBRATION_SCREEN_AS_ACTIVE(FALSE)
	SET_CHAT_ENABLED_FOR_CELEBRATION_SCREEN(FALSE)
	ANIMPOSTFX_STOP("MP_Celeb_Win")
	ANIMPOSTFX_STOP("MP_Celeb_Lose")
	
	PRINTLN("[Survival] - stopping MP_Celeb_Win, call 0")
	
	IF DOES_CAM_EXIST(camCelebrationScreen)
		DESTROY_CAM(camCelebrationScreen, TRUE)
	ENDIF
	
	LEGACY_CLEANUP_WORLD_PROPS(sRuntimeWorldPropData)
	
	sEndOfMission.bBeingKickedFromSurvival = FALSE
	
    CLEAR_SPECIFIC_SPAWN_LOCATION()
    
	g_iOverheadNamesState = TAGS_OFF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		DO_END_OF_MODE_LEADERBOARD_DATA_SORT()
   		SET_LOCAL_PLAYER_CAN_COLLECT_HORDE_PICKUPS()
    ENDIF
	
	SET_AUDIO_FLAG("DisableFlightMusic", FALSE)
	SET_AUDIO_FLAG("WantedMusicDisabled", FALSE)
	
	STOP_AUDIO_SCENE("MP_POSITIONED_RADIO_MUTE_SCENE")
	STOP_AUDIO_SCENE("MP_CELEB_SCREEN_SCENE")
	STOP_AUDIO_SCENE("MP_LEADERBOARD_SCENE")
	
	PRINTLN("[Survival] - alling STOP_AUDIO_SCENE(MP_CELEB_SCREEN_SCENE). Call A.")
	
	CLEAR_BIT(g_iBS1_Mission, ciBS1_Mission_HoldUpRespawn)
	
	// Unregister enemy peds with spectator camera.
	CLEAR_ALL_SPECTATOR_TARGET_LISTS(g_BossSpecData.specHUDData)
	
    // Spectator cam
    IF NOT IS_THIS_SPECTATOR_CAM_OFF(g_BossSpecData.specCamData)
		FORCE_CLEANUP_SPECTATOR_CAM(g_BossSpecData, TRUE)
    ENDIF
	CLEAR_DOES_THIS_SPECTATOR_CAM_WANT_TO_LEAVE(g_BossSpecData.specCamData)
	
	IF HAS_SCALEFORM_MOVIE_LOADED(eMiniLeaderBoardData.siMovie)
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(eMiniLeaderBoardData.siMovie)
	ENDIF
	
	IF bSetBlockingAreas
		REMOVE_SCENARIO_BLOCKING_AREA(sbiScenarioBlockingIndex, FALSE)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(	(serverBD.vStartPos[0]-<<1000.0, 1000.0, 250.0>>), 
												(serverBD.vStartPos[0]+<<1000.0, 1000.0, 250.0>>),
												TRUE, FALSE)
		bSetBlockingAreas = FALSE
	ENDIF
	
    #IF IS_DEBUG_BUILD
    	PRINTLN("[Survival] - SCRIPT_CLEANUP - iCurrentMissionType = ", GlobalplayerBD_FM[NATIVE_TO_INT(localPlayer)].iCurrentMissionType)
    #ENDIF
   	
    STOP_STREAM()
    
    SET_LOCAL_PLAYER_CAN_COLLECT_PORTABLE_PICKUPS(TRUE)
    
	
   	
	CLEANUP_SOCIAL_CLUB_LEADERBOARD(scLB_control)
	IF HAS_SCALEFORM_MOVIE_LOADED(scLB_scaleformID)
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(scLB_scaleformID)
		PRINTLN("[Survival] - CLEANUP_SOCIAL_CLUB_LEADERBOARD marking SCLB scaleform movie as no longer needed")
	ENDIF
	
	IF NETWORK_IS_SIGNED_ONLINE()  
		INCREMENT_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_MC_OVER)
		IF NETWORK_IS_GAME_IN_PROGRESS()
			IF iLocalPart >= 0
				IF NOT IS_BIT_SET(playerBD[iLocalPart].iBitset, BITSET_PLAYER_ABANDONED_HORDE)
					INT IncrementCheatby = INCREMENT_ENDING_ACTIVITY_CHEAT_STAT_BY()
					INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_SUR_CHEAT_END, IncrementCheatby)
					IF IncrementCheatby > 0
						INCREMENT_MP_INT_CHARACTER_STAT(MP_STAT_SURVIVAL_END)
					ENDIF
					PRINTLN("[Survival] - SCRIPT_CLEANUP - called INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_SUR_CHEAT_END, INCREMENT_ENDING_ACTIVITY_CHEAT_STAT_BY()).")
				ENDIF
			ENDIF
		ENDIF
		INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_ACTIVITY_ENDED, 1)
	ENDIF
	
    // Player
    IF DOES_ENTITY_EXIST(LocalPlayerPed)	
        IF NOT IS_ENTITY_DEAD((LocalPlayerPed))
            SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_WillFlyThroughWindscreen, TRUE) 
        ENDIF
    ENDIF
    
    SET_PLAYER_DEFAULT_FRIENDLY_FIRE_OPTION()	//NETWORK_SET_FRIENDLY_FIRE_OPTION(TRUE)
	
	IF localPlayer != NULL
		IF NETWORK_IS_GAME_IN_PROGRESS()
    		SET_PLAYER_LOCKON(localPlayer, TRUE)
			IF NOT IS_PLAYER_SCTV(localPlayer)
			    PRINTLN("[Survival] - SET_PLAYER_TEAM(localPlayer, -1)")
			    SET_PLAYER_TEAM(localPlayer, -1)
			ENDIF
		ENDIF
	ENDIF
    
    // Emergency services
    SET_MAX_WANTED_LEVEL(6)
    SET_WANTED_LEVEL_MULTIPLIER(1.0)
    ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, TRUE)                  
    ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, TRUE)       
	SET_DISPATCH_COPS_FOR_PLAYER(localPlayer, TRUE)
    
    // HUD
    SET_WIDESCREEN_BORDERS(FALSE, -1)
    DISPLAY_RADAR(TRUE)
    DISPLAY_HUD(TRUE)
    
    IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM
        Enable_MP_Comms()
    ENDIF
    ENABLE_SELECTOR()
    CLEAR_ADDITIONAL_TEXT(MISSION_TEXT_SLOT, TRUE)
    DISABLE_SCRIPT_HUD(HUDPART_RANKBAR, FALSE)
    ENABLE_ALL_MP_HUD()
    
	TOGGLE_STRIPPER_AUDIO(TRUE)
	
    // Delete mission entities if mission has finished.
    IF GET_END_HORDE_REASON() != eENDHORDEREASON_NOT_FINISHED_YET
        CLEANUP_ALL_FMMC_ENTITIES(serverBD.sFMMC_SBD)
    ENDIF
    
    REPEAT GET_FMMC_MAX_NUM_PROPS() i
        IF DOES_ENTITY_EXIST(oiHordeProp[i])
            DELETE_OBJECT(oiHordeProp[i])
        ENDIF
    ENDREPEAT	
	FOR i = 0 TO (FMMC_MAX_NUM_PROP_CHILDREN-1)
		IF DOES_ENTITY_EXIST(oihordePropChildren[i])
		   DELETE_OBJECT(oihordePropChildren[i])
		ENDIF
	ENDFOR 
       
   	CLEAR_SPAWN_AREA()
	
    // Stop the players being invisible
	IF IS_PLAYER_ON_A_PLAYLIST(localPlayer)
	    IF NOT NETWORK_IS_TUTORIAL_SESSION_CHANGE_PENDING()
	        IF NETWORK_IS_IN_TUTORIAL_SESSION()
	            NETWORK_END_TUTORIAL_SESSION()
	            #IF IS_DEBUG_BUILD
	                PRINTLN("[Survival] - NETWORK_END_TUTORIAL_SESSION")
	            #ENDIF
	        ENDIF
	    ENDIF
	 	SET_PLAYER_NEXT_RESPAWN_LOCATION(SPAWN_LOCATION_AT_CURRENT_POSITION)
	ELSE
	    // Setup spawning.
	    SET_PLAYER_NEXT_RESPAWN_LOCATION(SPAWN_LOCATION_AUTOMATIC, FALSE)
    ENDIF
	
    // Allowed to die again.
    IF DOES_ENTITY_EXIST(LocalPlayerPed)
        IF NOT IS_ENTITY_DEAD(LocalPlayerPed)
            SET_PLAYER_INVINCIBLE(localPlayer, FALSE)
        ENDIF
    ENDIF
    
	IF (GET_END_HORDE_REASON() = eENDHORDEREASON_BEAT_ALL_WAVES)
	AND NOT IS_FAKE_MULTIPLAYER_MODE_SET()
	AND IS_ROCKSTAR_CREATED_MISSION()
    	g_TransitionSessionNonResetVars.sPostMissionCleanupData.bComingOutOfSuccessfulSurvival = TRUE
		PRINTLN("[Survival] - SCRIPT_CLEANUP - beat survival, setting flag for reward vehicles.")
	ENDIF
	
	// Make sure music stops.
	IF IS_SCORE_CORONA_OPTION_ON()
		REPEAT NUM_MISSION_MUSIC_EVENTS i
			FOR j = 1 TO serverBD.iNumberOfWaves
				tl23Event = GET_SURVIVAL_MUSIC_EVENT_STRING(INT_TO_ENUM(ENUM_MUSIC_EVENT, i), j, TRUE)
				IF NOT ARE_STRINGS_EQUAL(tl23Event, "MP_SM_STOP_TRACK")
		  			CANCEL_MUSIC_EVENT(tl23Event)
					PRINTLN("[Survival] - [Music] - cancelled music event ", tl23Event)
				ENDIF
			ENDFOR
		ENDREPEAT
		PRINTLN("[Survival] - [Music] - cancelled all survival music events.")
		SURVIVAL_START_MUSIC_EVENT(eMUSICEVENT_STOP)
	ELSE
		PRINTLN("[Survival] - [Music] - corona option says it's turned off.")
	ENDIF
	
    SET_ROADS(TRUE)
    
    RESET_FMMC_MISSION_VARIABLES(TRUE, FALSE, GET_FMMC_END_OF_MISSION_STATUS_FOR_END_HORDE_REASON())      
	
    //If the player is dead and on a playlist then force them to respawn
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF IS_PLAYER_ON_A_PLAYLIST_INT(NATIVE_TO_INT(localPlayer))
			IF NOT bLocalPlayerOK			
		  		SET_PLAYER_NEXT_RESPAWN_LOCATION(SPAWN_LOCATION_AT_CURRENT_POSITION)
				FORCE_RESPAWN(TRUE)
			ENDIF
		ENDIF	
		//Force the player to respawn
		IF g_sFMMCEOM.iVoteStatus = ciFMMC_EOM_VOTE_STATUS_QUICK_RESTART
		OR g_sFMMCEOM.iVoteStatus =  ciFMMC_EOM_VOTE_STATUS_QUICK_RESTART_SWAP
			IF NOT bLocalPlayerOK			
		  		SET_PLAYER_NEXT_RESPAWN_LOCATION(SPAWN_LOCATION_AT_CURRENT_POSITION)
				PRINTLN("[Survival] - [TS] RESET_FMMC_MISSION_VARIABLES - SPAWN_LOCATION_AT_CURRENT_POSITION - SET_PLAYER_NEXT_RESPAWN_LOCATION - FORCE_RESPAWN(TRUE)")
				FORCE_RESPAWN(TRUE)
			ENDIF
		ENDIF
	ENDIF	
	
    IF bLocalPlayerOK
		NET_SET_PLAYER_CONTROL(localPlayer, FALSE, NSPC_SET_INVISIBLE | NSPC_NO_COLLISION | NSPC_FREEZE_POSITION)
    ENDIF
    
	CLEAR_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_ON_LAST_SURVIVAL_ROUND)
	
    // Added at request of Kev B*924109.
    g_b_OnHordeWaveBoard = FALSE
	
	SET_BIG_MESSAGE_ENABLED_WHEN_SPECTATING(FALSE)
	
	SET_PED_USING_ACTION_MODE(LocalPlayerPed, FALSE)
		
	IF (GET_END_HORDE_REASON() != eENDHORDEREASON_NOT_FINISHED_YET)
		CLEANUP_ALL_ENEMIES()
	ENDIF
	
	g_bBlockAIKillXP = FALSE
	
	IF IS_PLAYER_ON_A_PLAYLIST(localPlayer)
		RESET_GAME_STATE_ON_DEATH()
	ENDIF
	
	SET_TIME_OF_DAY(TIME_OFF, TRUE)
	
	ENABLE_SPECTATOR_FADES()
	// For weapons in respawning.
    g_bOnHorde = FALSE
	LBD_SCENE_CLEANUP()
	
	SET_CELEB_BLOCK_RESURRECT_RENDERPHASE_UNPAUSE(FALSE)
	
	CLEAR_IDLE_KICK_TIME_OVERRIDDEN()
	
	DO_BLIMP_SIGNS(sBlimpSign, TRUE)
	
	RESET_WORLD_STATE()
	
	HANDLE_SURVIVAL_IPL_AND_INTERIORS(TRUE)
	
	IF iNavmeshBlocker != -1
		REMOVE_NAVMESH_BLOCKING_OBJECT(iNavmeshBlocker)
	ENDIF
	
	CLEANUP_ZONES()
	
	ALLOW_PLAYER_HEIGHTS(FALSE)
	
	REMOVE_ANIM_SET("ANIM_GROUP_MOVE_BALLISTIC")    
    REMOVE_ANIM_SET("MOVE_STRAFE_BALLISTIC")
    REMOVE_CLIP_SET("move_ballistic_2h")
	PRINTLN("[Survival] - SCRIPT_CLEANUP - Removed animsets")
		
	IF IS_THIS_SPECTATOR_CAM_ACTIVE(g_BossSpecData.specCamData)
		PRINTLN("[Survival] - SCRIPT_CLEANUP - Spectator Cam Active - Deactivating for cleanup.")
		SET_MANUAL_RESPAWN_STATE(MRS_EXIT_SPECTATOR_AFTERLIFE)
		DEACTIVATE_SPECTATOR_CAM(g_BossSpecData, DSCF_BAIL_FOR_TRANSITION)
		RESET_GLOBAL_SPECTATOR_STRUCT()
	ELSE
		PRINTLN("[Survival] - SCRIPT_CLEANUP - Spectator Cam not active.")
	ENDIF
	
	IF IS_SCREEN_FADED_OUT()
	AND SHOULD_PLAYER_LEAVE_MP_MISSION()
		DO_SCREEN_FADE_IN(5000)
	ENDIF
	
	IF IS_HALLOWEEN_MODE()
	AND NOT IS_FAKE_MULTIPLAYER_MODE_SET()
	AND NOT IS_LOCAL_BIT_SET(ciLocalBool_bInTest)
		GIVE_MP_REWARD_WEAPON_IN_FREEMODE( TRUE, FALSE, WEAPONINHAND_FIRSTSPAWN_HOLSTERED, FALSE, FALSE, FALSE, TRUE )
		REMOVE_WEAPON_FROM_PED(PLAYER_PED_ID(), WEAPONTYPE_DLC_RAYCARBINE)
		REMOVE_WEAPON_FROM_PED(PLAYER_PED_ID(), WEAPONTYPE_DLC_RAYPISTOL)
		REMOVE_WEAPON_FROM_PED(PLAYER_PED_ID(), WEAPONTYPE_DLC_RAYMINIGUN)
		PRINTLN("[Survival] - SCRIPT_CLEANUP - Weapon Cleanup")
		REMOVE_ALL_WEAPONS()
		RESET_PLAYERS_WEAPONS_OBTAINED()
		g_GIVE_PLAYER_ALL_UNLOCKED_AND_EQUIPED_WEAPONS_FM_CHECK = TRUE
		g_bMissionInventory = FALSE
	ENDIF
	
	REMOVE_ALL_COVER_BLOCKING_AREAS()
	
	REMOVE_NAMED_PTFX_ASSET("scr_bike_adversary")
	REMOVE_NAMED_PTFX_ASSET("scr_gr_def")
	SET_MODEL_AS_NO_LONGER_NEEDED(prop_alien_egg_01)
	SET_MODEL_AS_NO_LONGER_NEEDED(PROP_POWER_CELL)
	ANIMPOSTFX_STOP("SurvivalAlien")
	
	REMOVE_INVALID_WEAPONS_IN_FREEMODE()
	
	g_bCleanupHangarModelHides = FALSE
	PRINTLN("[Survival] g_bCleanupHangarModelHides set to FALSE")
	
	STOP_SOUND(iBoundsTimerSound)
	CLEAR_SOUND_ID(iBoundsTimerSound)
	
	SET_ENTITY_PROOFS(LocalPlayerPed, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE)
	
    TERMINATE_THIS_MULTIPLAYER_THREAD(serverBD.TerminationTimer)
ENDPROC

FUNC PLAYER_INDEX GET_PLAYER_WITH_MOST_KILLS(STRUCT_HORDE_BOARD_DATA &sBoardData)
	INT iMostKills = -1
	PLAYER_INDEX bestPlayer
	
	INT i = 0
	REPEAT MAX_NUMBER_HORDE_FIGHTING_PARTICIPANTS i
		IF sBoardData.hudPlayers[i] != INVALID_PLAYER_INDEX()
			IF sBoardData.iScores[i] > iMostKills
				iMostKills = sBoardData.iScores[i]
				bestPlayer = sBoardData.hudPlayers[i]
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN bestPlayer
ENDFUNC

FUNC BOOL DO_PRE_LEAVE_SURVIVAL()
	
	PRINTLN("[Survival] - DO_PRE_LEAVE_SURVIVAL being called.")
    IF IS_LOCAL_BIT_SET(ciLocalBool_bIamQuittingCoronaSpectator)
		PRINTLN("[Survival] - ciLocalBool_bIamQuittingCoronaSpectator = TRUE, DO_PRE_LEAVE_SURVIVAL returning TRUE.")
        RETURN TRUE
    ENDIF
    
    IF CONTROL_UPDATING_CHALLENGES()
		PRINTLN("[Survival] - CONTROL_UPDATING_CHALLENGES = TRUE, DO_PRE_LEAVE_SURVIVAL returning TRUE.")
        RETURN TRUE
    ENDIF
    
    RETURN FALSE
    
ENDFUNC

FUNC BOOL SHOULD_I_DISPLAY_END_LEADERBOARD()
    
	IF IS_LOCAL_BIT_SET(ciLocalBool_bModeEndedSpecCamTranisition)
		IF NOT IS_SCREEN_FADED_IN()
			RETURN FALSE
		ENDIF
	ENDIF
	
    IF NOT IS_LOCAL_BIT_SET(ciLocalBool_bIamQuittingCoronaSpectator)
        RETURN TRUE
    ELSE
        IF GET_END_HORDE_REASON() != eENDHORDEREASON_NOT_FINISHED_YET
            RETURN TRUE
        ENDIF
		IF IS_BIT_SET(playerBD[iLocalPart].iBitset, BITSET_PLAYER_ABANDONED_HORDE)
			RETURN TRUE
		ENDIF
    ENDIF
    
    RETURN FALSE
    
ENDFUNC

FUNC BOOL SHOULD_I_PROCESS_END_HORDE_STATS()

	IF IS_FAKE_MULTIPLAYER_MODE_SET()
		RETURN FALSE
	ENDIF
    
    IF IS_BIT_SET(playerBD[iLocalPart].iBitset, BITSET_PLAYER_I_WENT_STRAIGHT_TO_SPEC_CAM) 
        RETURN FALSE
    ENDIF
    
    IF IS_PLAYER_SCTV(localPlayer) // Don't want corona or SCTV spectators to track stats.
        RETURN FALSE
    ENDIF
    
    RETURN TRUE
    
ENDFUNC

FUNC INT GET_ACCURACY()

	INT iEndHits =  GET_MP_INT_CHARACTER_STAT(MP_STAT_HITS_PEDS_VEHICLES)
	INT iEndShots = GET_MP_INT_CHARACTER_STAT(MP_STAT_SHOTS)
	
	IF (iEndShots - iStartShots) > 1
		FLOAT frecentaccuracy = TO_FLOAT((iEndHits - iStartHits) / (iEndShots - iStartShots))
		RETURN ROUND(frecentaccuracy)
	ENDIF
	
	RETURN 0
	
ENDFUNC

FUNC INT CALCULATE_SURVIVAL_SCORE()
	
	INT iScore
	INt iTimeBonus
	
	// See B*1530484 for formula.
	
	// Kills bonus.
	iScore += (playerBD[iLocalPart].iMyKills*100)
	PRINTLN("[Survival] - CALCULATE_SURVIVAL_SCORE - adding kills bonus, score now equals: ", iScore)
	
	// Time bonus.
	iTimeBonus = ((GET_WAVE_SURVIVED_UNTIL()*2500) - ((GET_ABS_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stSurvivalTime)/1000)*GET_WAVE_SURVIVED_UNTIL()))
	IF iTimeBonus > 0
		iScore += iTimeBonus
		PRINTLN("[Survival] - CALCULATE_SURVIVAL_SCORE - adding time bonus, score now equals: ", iScore)
	ELSE
		PRINTLN("[Survival] - CALCULATE_SURVIVAL_SCORE - not adding time bonus, negative value: ", iTimeBonus)
	ENDIF
	
	// Headshots bonus.
	iScore += playerBD[iLocalPart].iMyHeadshots*200
	PRINTLN("[Survival] - CALCULATE_SURVIVAL_SCORE - adding headshots bonus, score now equals: ", iScore)
	
	// Accuracy bonus.
	iScore += GET_ACCURACY()*5000
	PRINTLN("[Survival] - CALCULATE_SURVIVAL_SCORE - adding accuracy bonus, score now equals: ", iScore)
	
	// Vehicle kills bonus.
	iScore += playerBD[iLocalPart].iMyVehicleKills*400
	PRINTLN("[Survival] - CALCULATE_SURVIVAL_SCORE - adding vehicle kills bonus, score now equals: ", iScore)
	
	// No deaths bonus.
	IF playerBD[iLocalPart].iDeaths = 0
		iScore += 5000
		PRINTLN("[Survival] - CALCULATE_SURVIVAL_SCORE - adding no deaths bonus, score now equals: ", iScore)
	ENDIF
	
	RETURN iScore
	
ENDFUNC

PROC WRITE_TO_SURVIVAL_SC_LB()

    // Don't write if spectating
    IF IS_PLAYER_SCTV(localPlayer)
        PRINTLN("[Survival] - WRITE_TO_SURVIVAL_SC_LB bypassed because IS_PLAYER_SCTV")
        EXIT
    ENDIF
    
	PRINTLN("[Survival] - WRITE_TO_SURVIVAL_SC_LB ") 
    IF NOT ARE_STRINGS_EQUAL(g_FMMC_STRUCT.tl31LoadedContentID,"")
		TEXT_LABEL_31 categoryNames[1] 
   		TEXT_LABEL_23 uniqueIdentifiers[1]
   		categoryNames[0] = "Mission" 
  		uniqueIdentifiers[0] = g_FMMC_STRUCT.tl31LoadedContentID
		
		// *SCORE_COLUMN( AGG_SUM ) : COLUMN_ID_LB_SCORE - LBCOLUMNTYPE_INT64
    	//WAVES_SURVIVED( AGG_SUM ) : COLUMN_ID_LB_NUM_WINS - LBCOLUMNTYPE_INT64
    	//KILLS( AGG_SUM ) : COLUMN_ID_LB_KILLS - LBCOLUMNTYPE_INT32
    	//DEATHS( AGG_SUM ) : COLUMN_ID_LB_DEATHS - LBCOLUMNTYPE_INT32
    	//KILL_DEATH_RATIO( AGG_LAST ) : COLUMN_ID_LB_KILL_DEATH_RATIO - LBCOLUMNTYPE_DOUBLE
		IF INIT_LEADERBOARD_WRITE(LEADERBOARD_FREEMODE_SURVIVAL,uniqueIdentifiers,categoryNames,1,-1,TRUE)
           	
	        // Write score
	        WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(	ENUM_TO_INT(LEADERBOARD_FREEMODE_SURVIVAL),LB_INPUT_COL_SCORE,playerBD[iLocalPart].iSurvivalScore,0)
	        
			// Write waves
	       	WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(	ENUM_TO_INT(LEADERBOARD_FREEMODE_SURVIVAL),LB_INPUT_COL_NUM_WINS,serverBD.sWaveData.iWaveCount,0)
	                
	        // Write kills
	       	WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(	ENUM_TO_INT(LEADERBOARD_FREEMODE_SURVIVAL),LB_INPUT_COL_KILLS,playerBD[iLocalPart].iMyKills,0)
	                        
	        // Write deaths                 
	        WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(	ENUM_TO_INT(LEADERBOARD_FREEMODE_SURVIVAL),LB_INPUT_COL_DEATHS,playerBD[iLocalPart].iDeaths,0)
			
			LEADERBOARDS_WRITE_ADD_COLUMN_LONG(						LB_INPUT_COL_MATCH_ID,serverbd.iMatchHistoryId[1],serverbd.iMatchHistoryId[0])
			PRINTLN("[Survival] - LEADERBOARDS_WRITE_ADD_COLUMN_LONG writing to column 8 hashedMac: ",serverbd.iMatchHistoryId[1]," matchHistoryID: " , serverbd.iMatchHistoryId[0])
	       
	        PRINTLN("[Survival] - -<>- WRITE_TO_SURVIVAL_SC_LB: Score: ", playerBD[iLocalPart].iSurvivalScore) 
	        PRINTLN("[Survival] - -<>- WRITE_TO_SURVIVAL_SC_LB: Waves: ", serverBD.sWaveData.iWaveCount) 
	        PRINTLN("[Survival] - -<>- WRITE_TO_SURVIVAL_SC_LB: Kills: ", TO_FLOAT(playerBD[iLocalPart].iMyKills)) 
	        PRINTLN("[Survival] - -<>- WRITE_TO_SURVIVAL_SC_LB: Deaths: ", playerBD[iLocalPart].iDeaths) 
		ENDIF
    ENDIF
    
ENDPROC 


FUNC BOOL DID_I_GET_MOST_SURVIVAL_KILLS()
	
	INT i
	
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
		IF i != iLocalPart
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
				IF playerBD[i].iMyKills >= playerBD[iLocalPart].iMyKills
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
	
ENDFUNC

PROC PROCESS_EOM_RESTART_VOTE_LOGIC()
	
	INT iparticipant
	
	IF (GET_END_HORDE_REASON() != eENDHORDEREASON_NOT_FINISHED_YET)
	OR IS_BIT_SET(playerBD[iLocalPart].iBitset, BITSET_PLAYER_ABANDONED_HORDE)
		FMMC_EOM_SERVER_RESET_PLAYER_COUNTS(sPlayerCounts)
	
		REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iparticipant
			FMMC_EOM_SERVER_PLAYER_PROCESSING(sPlayerCounts, iparticipant)
		ENDREPEAT
	
		FMMC_EOM_SERVER_END_OF_FRAME_PROCESSING(sPlayerCounts, serverBD.sServerFMMC_EOM, FMMC_TYPE_SURVIVAL, 0)
		IF SHOULD_RUN_NEW_VOTING_SYSTEM(sEndOfMission.iQuitProgress, serverBD.sServerFMMC_EOM.iVoteStatus)
			SERVER_JOB_VOTE_PROCESSING(g_sMC_serverBDEndJob)
		ENDIF
	ENDIF
ENDPROC

PROC DO_MATCH_END()
	
	// Save end playstats if I did start playstats.
	IF NETWORK_IS_GAME_IN_PROGRESS()
		
		IF NOT bDidMatchEnd
			IF bSavedMatchstartPlayStatsData
				DEAL_WITH_FM_MATCH_END(	FMMC_TYPE_SURVIVAL, 
										serverBD.iMatchHistoryId[0], 
										0, 
										GET_FMMC_END_OF_MISSION_STATUS_FOR_END_HORDE_REASON(), 
										GET_MY_LEADERBOARD_POSITION_FOR_PLAYSTATS(), 
										playerBD[iLocalPart].iMyKills, 
										playerBD[iLocalPart].iDeaths, 
										0, 
										0, 
										GET_PLAYER_TEAM(localPlayer),
										playerBD[iLocalPart].iMyHeadshots,
										DUMMY_MODEL_FOR_SCRIPT,
										GET_WAVE_SURVIVED_UNTIL() )
				bDidMatchEnd = TRUE
				PRINTLN("[Survival] - DO_MATCH_END() - called DEAL_WITH_FM_MATCH_END.")
			ENDIF
		ENDIF
		
	ELSE
		
		bDidMatchEnd = TRUE
		PRINTLN("[Survival] - DO_MATCH_END() - called DEAL_WITH_FM_MATCH_END.")
		
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Gets if the end mission reason is one that requires the local player to go to SURVIVAL_GAME_STATE_WAITING_TO_LEAVE.
/// RETURNS:
///    TRUE if should go to SURVIVAL_GAME_STATE_WAITING_TO_LEAVE, FALSE if not (most means should go straight to SURVIVAL_GAME_STATE_END).
FUNC BOOL SHOULD_GO_TO_WAITING_TO_LEAVE_STATE()            
    RETURN TRUE
ENDFUNC

PROC CLEANUP_BEFORE_END()

	CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_SURVIVAL_PASSED)
	CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_SURVIVAL_FAILED)
	
	PRINTLN("[Survival] - CLEANUP_BEFORE_END - being called.")
	
    // Fade screen out and leave script.
    IF DOES_ENTITY_EXIST(LocalPlayerPed)
		PRINTLN("[Survival] - CLEANUP_BEFORE_END - PLAYER_PED_ID exists.")
   		IF DO_PRE_LEAVE_SURVIVAL()
			PRINTLN("[Survival] - CLEANUP_BEFORE_END - DO_PRE_LEAVE_SURVIVAL = TRUE.")
            IF SHOULD_GO_TO_WAITING_TO_LEAVE_STATE()
				PRINTLN("[Survival] - CLEANUP_BEFORE_END - SHOULD_GO_TO_WAITING_TO_LEAVE_STATE = TRUE.")
                SET_CLIENT_GAME_STATE(SURVIVAL_GAME_STATE_WAITING_TO_LEAVE)
            ELSE
				PRINTLN("[Survival] - CLEANUP_BEFORE_END - SHOULD_GO_TO_WAITING_TO_LEAVE_STATE = FALSE.")
                SET_CLIENT_GAME_STATE(SURVIVAL_GAME_STATE_END)
            ENDIF
        ENDIF
    ENDIF
	
ENDPROC

PROC SAVE_SURVIVAL_TEAM_GAMER_HANDLES_TO_GLOBALS()
	
	INT i, iGamerHandleCount
	PLAYER_INDEX playerId
	
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_NATIVE(PARTICIPANT_INDEX, i))
			IF IS_PARTICIPANT_IN_FIGHTING_PARTICIPANTS_LIST(i)
				playerId = NETWORK_GET_PLAYER_INDEX(INT_TO_NATIVE(PARTICIPANT_INDEX, i))
				IF IS_NET_PLAYER_OK(playerId, FALSE)
					IF NOT IS_PLAYER_SCTV(playerId)
						g_TransitionSessionNonResetVars.sPostMissionCleanupData.eCreateSurvivalRewardVehsGamerHandles[iGamerHandleCount] = GET_GAMER_HANDLE_PLAYER(playerId)
						IF (playerId = localPlayer)
							g_TransitionSessionNonResetVars.sPostMissionCleanupData.iCreateSurvivalRewardVehsMyGamerHandleArrayPos = iGamerHandleCount
						ENDIF
						iGamerHandleCount++
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
ENDPROC

PROC DO_OUTROS()
	IF NOT bDoneOutro
		IF bOkToDoOutro
			ANIM_OUTRO_PREP(jobIntroData)
			REQUEST_ANIMATIONS_FOR_INTRO(jobIntroData)
			IF HAS_STARTED_NEW_OUTRO(jobIntroData)
				PRINTLN("DO_OUTROS, YES")
				bDoneOutro = TRUE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SET_REWARDS_AND_FEEDBACK_STATE(INT iNewState)
	PRINTLN("[Survival] - SET_REWARDS_AND_FEEDBACK_STATE - setting iFeedbackStage(",iFeedbackStage,") to ", iNewState)
	iFeedbackStage = iNewState
ENDPROC

PROC PROCESS_eHORDESTAGE_REWARDS_AND_FEEDACK_CLIENT()
	
	BOOL bFailedMode
	
	// ************************************************************
	// Functions that need processed regardless of feedback stage.
	// ************************************************************
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM)
	
	IF bSetSctvTickerOn
		SET_SCTV_TICKER_SCORE_OFF()
		bSetSctvTickerOn = FALSE
	ENDIF
	
	BOOL bTestMode = IS_FAKE_MULTIPLAYER_MODE_SET()
	
	IF bTestMode
		DO_SCREEN_FADE_OUT(150)
		CLEANUP_BEFORE_END()
		SET_BIT(playerBD[iLocalPart].iBitset, BITSET_PLAYER_I_HAVE_DEALT_WITH_FEEDBACK)
		EXIT
	ENDIF
	
	IF (GET_END_HORDE_REASON() != eENDHORDEREASON_NOT_FINISHED_YET)
	OR 	IS_BIT_SET(playerBD[iLocalPart].iBitset, BITSET_PLAYER_ABANDONED_HORDE)
		SET_DAILY_OBJECTIVE_TICKERS_ENABLED_ON_MISSION(FALSE)
	
		#IF IS_DEBUG_BUILD
		BOOL bStraightToSpecCam = IS_BIT_SET(playerBD[iLocalPart].iBitset, BITSET_PLAYER_I_WENT_STRAIGHT_TO_SPEC_CAM)
		BOOL bSctv = IS_PLAYER_SCTV(localPlayer)
		BOOL bAbandonded = IS_BIT_SET(playerBD[iLocalPart].iBitset, BITSET_PLAYER_ABANDONED_HORDE)
		PRINTLN("[Survival] - PROCESS_eHORDESTAGE_REWARDS_AND_FEEDACK_CLIENT - SEND_EVENT_IF_PLAYER_COMPLETES_JOB_IN_UNDER_TEN_SECONDS - been called.")
		PRINTLN("[Survival] - PROCESS_eHORDESTAGE_REWARDS_AND_FEEDACK_CLIENT - SEND_EVENT_IF_PLAYER_COMPLETES_JOB_IN_UNDER_TEN_SECONDS - IS_BIT_SET(playerBD[iLocalPart].iBitset, BITSET_PLAYER_I_WENT_STRAIGHT_TO_SPEC_CAM) = ", bStraightToSpecCam)
		PRINTLN("[Survival] - PROCESS_eHORDESTAGE_REWARDS_AND_FEEDACK_CLIENT - SEND_EVENT_IF_PLAYER_COMPLETES_JOB_IN_UNDER_TEN_SECONDS - IS_PLAYER_SCTV(localPlayer) = ", bSctv)
		PRINTLN("[Survival] - PROCESS_eHORDESTAGE_REWARDS_AND_FEEDACK_CLIENT - SEND_EVENT_IF_PLAYER_COMPLETES_JOB_IN_UNDER_TEN_SECONDS - IS_BIT_SET(playerBD[iLocalPart].iBitset, BITSET_PLAYER_ABANDONED_HORDE) = ", bAbandonded)
		#ENDIF
		
		SEND_EVENT_IF_PLAYER_COMPLETES_JOB_IN_UNDER_TEN_SECONDS(jobUnderTenSecondsTimer, 
																(IS_BIT_SET(playerBD[iLocalPart].iBitset, BITSET_PLAYER_I_WENT_STRAIGHT_TO_SPEC_CAM) OR IS_PLAYER_SCTV(localPlayer)),
																IS_BIT_SET(playerBD[iLocalPart].iBitset, BITSET_PLAYER_ABANDONED_HORDE))
		
		IF NOT IS_BIT_SET(playerBD[iLocalPart].iBitset, BITSET_PLAYER_I_WENT_STRAIGHT_TO_SPEC_CAM) 
		AND NOT IS_PLAYER_SCTV(localPlayer)
		AND NOT IS_BIT_SET(playerBD[iLocalPart].iBitset, BITSET_PLAYER_ABANDONED_HORDE)
			SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_PARTICIPATE_IN_SURVIVAL) 
		ENDIF
		
	ENDIF
			
	// ***************************
	// Feedback stage sub-stages.
	// ***************************
	
	SWITCH iFeedbackStage
		
		
		// -----------------------------------
		// Wait for wasted message to clear.
		// -----------------------------------
		
		CASE 0
			
			IF NOT IS_LOCAL_BIT_SET(ciLocalBool_bDealtWithWasted)
				
				playerBD[iLocalPart].iSurvivalScore = CALCULATE_SURVIVAL_SCORE()
				PRINTLN("[Survival] - PROCESS_eHORDESTAGE_REWARDS_AND_FEEDACK_CLIENT - calculated my survival score to be ", playerBD[iLocalPart].iSurvivalScore)
				
				IF IS_ENTITY_DEAD(LocalPlayerPed)
					IF ALLOW_FOR_WASTED_MESSAGE()
						SET_LOCAL_BIT(ciLocalBool_bDealtWithWasted)
						PRINTLN("[Survival] - PROCESS_eHORDESTAGE_REWARDS_AND_FEEDACK_CLIENT - player dead, allowed for wasted message.")
					ENDIF
					
					IF IS_THIS_SPECTATOR_CAM_ACTIVE(g_BossSpecData.specCamData) 
						IF IS_THIS_SPECTATOR_CAM_RUNNING(g_BossSpecData.specCamData)
							SET_LOCAL_BIT(ciLocalBool_bDealtWithWasted)
							PRINTLN("[Survival] - PROCESS_eHORDESTAGE_REWARDS_AND_FEEDACK_CLIENT - player on spec cam, already allowed for wasted message.")
						ENDIF
					ELSE
						SET_GAME_STATE_ON_DEATH(AFTERLIFE_JUST_AUTO_REJOIN)
					ENDIF
					SET_LOCAL_BIT(ciLocalBool_bDeadAtEnd)
				ELSE
					
					IF NOT IS_PLAYER_RESPAWNING(localPlayer)
						SAVE_PLAYER_END_JOB_POSITION_FOR_POST_MISSION_CLEANUP()
						IF DOES_ENTITY_EXIST(LocalPlayerPed)
							SET_PERSONAL_VEHICLE_SPAWN_OVERRIDE_VECTOR(GET_ENTITY_COORDS(LocalPlayerPed, FALSE))
						ENDIF
						
						SET_LOCAL_BIT(ciLocalBool_bDealtWithWasted)
						PRINTLN("[Survival] - PROCESS_eHORDESTAGE_REWARDS_AND_FEEDACK_CLIENT - player not dead, don't need to allow for wasted message.")
					ELSE
						PRINTLN("[Survival] - PROCESS_eHORDESTAGE_REWARDS_AND_FEEDACK_CLIENT - Waiting on respawn")
					ENDIF
				ENDIF
				
				NEW_LOAD_SCENE_STOP()
				PRINTLN("[Survival] - PROCESS_eHORDESTAGE_REWARDS_AND_FEEDACK_CLIENT - NEW_LOAD_SCENE_STOP().")
				
				CLEAR_FOCUS()
				PRINTLN("[Survival] - PROCESS_eHORDESTAGE_REWARDS_AND_FEEDACK_CLIENT - CLEAR_FOCUS().")
				
				CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_LEAVING_SURVIVAL)
				
				SAVE_SURVIVAL_TEAM_GAMER_HANDLES_TO_GLOBALS()
				
			ELSE
				
				CLEANUP_DPAD_LBD(eMiniLeaderBoardData.siMovie)
				
				IF SHOULD_I_PROCESS_END_HORDE_STATS()
					
					IF GET_WAVE_SURVIVED_UNTIL() > GET_MP_INT_CHARACTER_STAT(MP_STAT_HORDELVL)
						SET_MP_INT_CHARACTER_STAT(MP_STAT_HORDELVL, GET_WAVE_SURVIVED_UNTIL())
					ENDIF
					
					IF (GET_END_HORDE_REASON() = eENDHORDEREASON_BEAT_ALL_WAVES)
						INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_HORDEWINS, 1)
						PRINTLN("[Survival] - [Awards] - PROCESS_eHORDESTAGE_REWARDS_AND_FEEDACK_CLIENT - beat all waves, incrementing stat MP_STAT_HORDEWINS by 1.")
					ENDIF
					
					IF IS_LOCAL_BIT_SET(ciLocalBool_bDeadAtEnd)
						SET_CURRENT_PED_WEAPON(localPlayerPed, WEAPONTYPE_UNARMED, TRUE)
					ENDIF
					
					// Sort unlocking of other survivals. Only want to do this if we've actually played, not if we were only a spectator or an sctv person.
					IF (GET_DISTANCE_BETWEEN_COORDS(<<269.3056,2777.5464,42.8282>>, g_FMMC_STRUCT.vStartPos) <= 2.5
					OR GET_DISTANCE_BETWEEN_COORDS(<<2296.8977,3110.9358,46.4730>>, g_FMMC_STRUCT.vStartPos) <= 2.5)
						SET_PLAYER_HAS_COMPLETED_PRIMARY_SURVIVAL()
						PRINTLN("[Survival] - PROCESS_eHORDESTAGE_REWARDS_AND_FEEDACK_CLIENT - played a primary survival, called SET_PLAYER_HAS_COMPLETED_PRIMARY_SURVIVAL().")
					ENDIF
					
				ENDIF
				
				SET_REWARDS_AND_FEEDBACK_STATE(1)
				
			ENDIF
					
		BREAK
		
		
		// ------------------------------------------------------------
		// Cleanup and sorting of data before displaying end feedback.
		// ------------------------------------------------------------
		CASE 1
			
			// No more blips.
		    REMOVE_HORDE_BLIPS()
		    
		    // No more teammate died big messages.
		    CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_SURVIVAL_TM_DIED)
			
		    // Comment out on 22/02/2013 by daveyg experiment for PT 1139613
			//DISABLE_SPECTATOR_FADES_DURING_THIS_EVENT()		
			
			// Sort leader board data for end of mode.
			DO_END_OF_MODE_LEADERBOARD_DATA_SORT()
			
			// No objective text during this bit.
	        IF NOT IS_LOCAL_BIT_SET(ciLocalBool_bClearedObjectiveTextForRewards)
	            Clear_Any_Objective_Text()
	            SET_LOCAL_PLAYER_CAN_COLLECT_HORDE_PICKUPS(FALSE)
	            SET_LOCAL_BIT(ciLocalBool_bClearedObjectiveTextForRewards)
	        ENDIF
			
			// Deal with award for most survival kills.
			IF NOT bProcessedMostSurvivalKillsAward
			AND IS_ROCKSTAR_CREATED_MISSION()
				UPDATE_HORDE_WAVES_SURVIVED_AWARD()
				IF serverBD.sFightingParticipantsList.iNumFightingParticipants >= 4
					IF DID_I_GET_MOST_SURVIVAL_KILLS()
						PRINTLN("[Survival] - [Awards] - PROCESS_eHORDESTAGE_REWARDS_AND_FEEDACK_CLIENT - I got the most kills, giving myself award MP_AWARD_FMMOSTKILLSSURVIVE")
						SET_MP_BOOL_CHARACTER_AWARD (MP_AWARD_FMMOSTKILLSSURVIVE, TRUE)
					#IF IS_DEBUG_BUILD
					ELSE
						PRINTLN("[Survival] - [Awards] - PROCESS_eHORDESTAGE_REWARDS_AND_FEEDACK_CLIENT - I did not get the most kills.")
					#ENDIF
					ENDIF
				#IF IS_DEBUG_BUILD
				ELSE
					PRINTLN("[Survival] - [Awards] - PROCESS_eHORDESTAGE_REWARDS_AND_FEEDACK_CLIENT - serverBD.sFightingParticipantsList.iNumFightingParticipants < 3, cannot give award MP_AWARD_FMMOSTKILLSSURVIVE.")
				#ENDIF
				ENDIF
				bProcessedMostSurvivalKillsAward = TRUE
			ENDIF
			
			// Handle stats and awards.
	        IF SHOULD_I_PROCESS_END_HORDE_STATS()
	            IF NOT IS_LOCAL_BIT_SET(ciLocalBool_bGivenEndModeRewards)
					REWARD_XP_FOR_PLAYING_OWN_CONTENT_AGAINST_OTHERS(NETWORK_GET_NUM_PARTICIPANTS())
	                SET_LOCAL_BIT(ciLocalBool_bGivenEndModeRewards)
	            ENDIF
	        ENDIF
			
			// checks player did not abandon game BUG 1727516
			IF NOT IS_BIT_SET(playerBD[iLocalPart].iBitset, BITSET_PLAYER_ABANDONED_HORDE)
				DO_CREW_CUT_ACHIEVEMENT() // 1415006
			ENDIF
			// Give XP for taking part.
			IF (GET_END_HORDE_REASON() != eENDHORDEREASON_NOT_FINISHED_YET)
	            IF NOT IS_LOCAL_BIT_SET(ciLocalBool_bGivenTakingPartXp)
					INT iXpGiven
	               	iXpGiven = GIVE_LOCAL_PLAYER_XP(eXPTYPE_STANDARD, "XPT_TKPRTHRD", XPTYPE_COMPLETE, XPCATEGORY_COMPLETE_SURVIVAL_TAKEN_PART, TAKING_PART_XP, 1)
					playerBD[iLocalPart].iXpGained += iXpGiven
					ADD_TO_END_CELEBRATION_SCORE(iXpGiven)
					#IF IS_DEBUG_BUILD
					iTakePartXp = iXpGiven
					#ENDIF
	                SET_LOCAL_BIT(ciLocalBool_bGivenTakingPartXp)
	            ENDIF
			ENDIF
			
			CLEAR_CELEBRATION_SCREEN_MOVIE_IDS(sCelebrationData)
			
			SET_REWARDS_AND_FEEDBACK_STATE(2)
			
		BREAK
		
		
		// --------------------------------
		// Request the leaderboard camera.
		// --------------------------------
		CASE 2
			BOOL bReadyToContinue
			START_LBD_AUDIO_SCENE()
			
			IF IS_THIS_SPECTATOR_CAM_ACTIVE(g_BossSpecData.specCamData)
				IF IS_SCREEN_FADED_IN()
					FADE_SCREEN_IN()
				ENDIF
				SET_MANUAL_RESPAWN_STATE(MRS_EXIT_SPECTATOR_AFTERLIFE)
				IF IS_PLAYER_ON_A_PLAYLIST(localPlayer)
					DEACTIVATE_SPECTATOR_CAM(g_BossSpecData, DSCF_PAUSE_RENDERPHASE)
				ELSE
					//1623884 - If the player is on spectator cam due to death then freeze on exit for the celebration screen transition.
					IF IS_BIT_SET(playerBD[iLocalPart].iBitset, BITSET_PLAYER_I_WENT_STRAIGHT_TO_SPEC_CAM)  
						DEACTIVATE_SPECTATOR_CAM(g_BossSpecData, DSCF_REMAIN_VIEWING_TARGET | DSCF_BAIL_FOR_TRANSITION)
					ELSE
						DEACTIVATE_SPECTATOR_CAM(g_BossSpecData, DSCF_REMAIN_VIEWING_TARGET | DSCF_PAUSE_RENDERPHASE)
						
						IF (GET_END_HORDE_REASON() = eENDHORDEREASON_BEAT_ALL_WAVES)
							PLAY_CELEB_WIN_POST_FX()
							PRINTLN("[Survival] - PROCESS_eHORDESTAGE_REWARDS_AND_FEEDACK_CLIENT - playing MP_Celeb_Win, call 5")
						ELSE
							PLAY_CELEB_LOSE_POST_FX()
						ENDIF
						PLAY_SOUND_FRONTEND(-1, "Hit_1", "LONG_PLAYER_SWITCH_SOUNDS")
						bAlreadyTriggeredCelebrationPostFX = TRUE
					ENDIF
				ENDIF
				
				bReadyToContinue = TRUE
				PRINTLN("[Survival] - PROCESS_eHORDESTAGE_REWARDS_AND_FEEDACK_CLIENT - spec cam active while in rewards stage, calling deactivate with DSCF_REMAIN_VIEWING_TARGET flag.")

			ELSE
				BOOL bPlacedCameraSuccessfully
			
				IF (GET_END_HORDE_REASON() = eENDHORDEREASON_BEAT_ALL_WAVES)
					IF PLACE_CAMERA_FOR_CELEBRATION_SCREEN(sCelebrationData, camCelebrationScreen, bPlacedCameraSuccessfully)
						bOkToDoOutro = TRUE
						bReadyToContinue = TRUE
					ELSE
						PRINTLN("PROCESS_eHORDESTAGE_REWARDS_AND_FEEDACK_CLIENT - [eENDHORDEREASON_BEAT_ALL_WAVES], PLACE_CAMERA_FOR_CELEBRATION_SCREEN ")
					ENDIF
				ELSE
					IF IS_SCREEN_FADED_IN()
						bReadyToContinue = TRUE
					ENDIF
				ENDIF
				
				IF bReadyToContinue				
					IF bPlacedCameraSuccessfully
						RENDER_SCRIPT_CAMS(TRUE, FALSE)
					ELSE
						IF bLocalPlayerOK
						AND NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
							TASK_TURN_PED_TO_FACE_COORD(LocalPlayerPed, GET_FINAL_RENDERED_CAM_COORD(), 2000)
						ENDIF
					ENDIF
					
					IF NOT bAlreadyTriggeredCelebrationPostFX
						IF bLocalPlayerOK
							ANIMPOSTFX_STOP_ALL()
							PRINTLN("[Survival] - Stopping all Anim Post FX 6")
						ENDIF
						
						IF (GET_END_HORDE_REASON() = eENDHORDEREASON_BEAT_ALL_WAVES)
							PLAY_CELEB_WIN_POST_FX()	
							PRINTLN("[Survival] - playing MP_Celeb_Win, call 6")
						ELSE
							PLAY_CELEB_LOSE_POST_FX()
						ENDIF
					ENDIF
					IF (GET_END_HORDE_REASON() = eENDHORDEREASON_BEAT_ALL_WAVES)
						PLAY_SOUND_FRONTEND(-1, "Survival_Passed", GET_SOUND_SET_FOR_SURVIVAL())
						PRINTLN("[Survival] - SOUNDS - Survival_Passed playing from Frontend")
					ELSE
						IF IS_HALLOWEEN_MODE()
							PLAY_SOUND_FRONTEND(-1, "Survival_Failed", "DLC_VW_AS_Sounds")
							PRINTLN("[Survival] - SOUNDS - Survival_Failed playing from Frontend (1)")
						ENDIF
					ENDIF
					PLAY_SOUND_FRONTEND(-1, "MP_WAVE_COMPLETE", "HUD_FRONTEND_DEFAULT_SOUNDSET")

				ENDIF
			ENDIF
			
			IF bReadyToContinue
				REQUEST_CELEBRATION_SCREEN(sCelebrationData)
				SET_CELEBRATION_SCREEN_AS_ACTIVE(TRUE)
				KILL_UI_FOR_CELEBRATION_SCREEN()
				HIDE_UI_FOR_CELEBRATION_SCREEN_THIS_FRAME()
				IF NOT IS_PLAYER_SPECTATING(localPlayer)
				AND NOT DID_I_JOIN_MISSION_AS_SPECTATOR()
					NET_SET_PLAYER_CONTROL(localPlayer, FALSE)
				ENDIF
				
				IF IS_PAUSE_MENU_ACTIVE()					
					SET_FRONTEND_ACTIVE(FALSE)
				ENDIF

				SET_REWARDS_AND_FEEDBACK_STATE(3)
			ENDIF
		BREAK
		
		
		// --------------------
		// Do rank predicition.
		// --------------------
		
		CASE 3
			HIDE_UI_FOR_CELEBRATION_SCREEN_THIS_FRAME()
			
			// setup the social club leaderboard data.
			SETUP_SOCIAL_CLUB_LEADERBOARD_READ_DATA(scLB_control,
													GlobalplayerBD_FM[NATIVE_TO_INT(localPlayer)].iCurrentMissionType,
													g_FMMC_STRUCT.tl31LoadedContentID,
													g_FMMC_STRUCT.tl63MissionName)//GlobalplayerBD_FM[NATIVE_TO_INT(localPlayer)].tl31MissionName)
			
			IF scLB_rank_predict.bFinishedRead
			AND NOT scLB_rank_predict.bFinishedWrite
				WRITE_TO_SURVIVAL_SC_LB()
				scLB_rank_predict.bFinishedWrite = TRUE
			ENDIF
	        
			IF GET_RANK_PREDICTION_DETAILS(scLB_control)
				sclb_useRankPrediction = TRUE
				SET_REWARDS_AND_FEEDBACK_STATE(4)
			ENDIF
			
		BREAK
		
		
		// -----------------
		// Display end hud.
		// -----------------
		CASE 4
			
			// setup the social club leaderboard data.
			SETUP_SOCIAL_CLUB_LEADERBOARD_READ_DATA(scLB_control,
													GlobalplayerBD_FM[NATIVE_TO_INT(localPlayer)].iCurrentMissionType,
													g_FMMC_STRUCT.tl31LoadedContentID,
													g_FMMC_STRUCT.tl63MissionName)//GlobalplayerBD_FM[NATIVE_TO_INT(localPlayer)].tl31MissionName)
			
			// If not done feedback yet.
   			IF NOT IS_BIT_SET(playerBD[iLocalPart].iBitset, BITSET_PLAYER_I_HAVE_DEALT_WITH_FEEDBACK)
				
				PRINTLN("[Survival] - PROCESS_eHORDESTAGE_REWARDS_AND_FEEDACK_CLIENT - BITSET_PLAYER_I_HAVE_DEALT_WITH_FEEDBACK = FALSE.")
				
		        IF DOES_ENTITY_EXIST(LocalPlayerPed)
			       	HIDE_HUD_AND_RADAR_THIS_FRAME()
						
					IF NOT IS_ENTITY_DEAD(LocalPlayerPed)
						IF NOT IS_PLAYER_SPECTATING(localPlayer)
						AND NOT DID_I_JOIN_MISSION_AS_SPECTATOR()
							IF IS_SKYSWOOP_AT_GROUND() 
								NET_SET_PLAYER_CONTROL(localPlayer, FALSE)
							ENDIF
						ENDIF
					ENDIF
					
					// Sort music.
					IF IS_SCORE_CORONA_OPTION_ON()
						TEXT_LABEL_23 tl23MusicEvent
						tl23MusicEvent = GET_SURVIVAL_MUSIC_EVENT_STRING(eMUSICEVENT_STOP, serverBD.sWaveData.iWaveCount)
						IF NOT structMusicEvent[ENUM_TO_INT(eMUSICEVENT_STOP)].bTriggered
							IF PREPARE_MUSIC_EVENT(tl23MusicEvent)
								TRIGGER_MUSIC_EVENT(tl23MusicEvent)
								structMusicEvent[ENUM_TO_INT(eMUSICEVENT_STOP)].bTriggered = TRUE
							ENDIF
						ENDIF
					ENDIF
					
					IF NOT IS_ENTITY_DEAD(LocalPlayerPed)
		                SET_PLAYER_INVINCIBLE(localPlayer, TRUE)
		            ENDIF
		            
					IF NOT IS_END_OF_ACTIVITY_HUD_ACTIVE()
						bAllAwardsBeenShown = TRUE
					ENDIF
					
					IF (GET_END_HORDE_REASON() = eENDHORDEREASON_BEAT_ALL_WAVES)
						bFailedMode = TRUE
					ENDIF
					
					DISPLAY_END_OF_WAVE_TEXT(TRUE, bFailedMode)
					
					IF g_bCelebrationScreenIsActive
					OR bNewJobScreens //There's a few frames between the celebration screen and the voting screen where the rank bar appears.
						HIDE_UI_FOR_CELEBRATION_SCREEN_THIS_FRAME()
					ENDIF
					
		            // Deal with end of mission feedback
					IF NOT g_bCelebrationScreenIsActive
						
						PRINTLN("[Survival] - PROCESS_eHORDESTAGE_REWARDS_AND_FEEDACK_CLIENT - g_bCelebrationScreenIsActive = FALSE.")
						
						IF NOT g_bMissionEnding
							PRINTLN("[Survival] - PROCESS_eHORDESTAGE_REWARDS_AND_FEEDACK_CLIENT - setting g_bMissionEnding = TRUE")
							g_bMissionEnding = TRUE
						ENDIF
						
						IF NOT bDoneMatchEnd
							DO_MATCH_END()
							bDoneMatchEnd = TRUE
						ENDIF
						
						IF NOT bSetMissionOverAndOnLbForSctv
							SET_MISSION_OVER_AND_ON_LB_FOR_SCTV()
							bSetMissionOverAndOnLbForSctv = TRUE
						ENDIF
						
						IF bAllAwardsBeenShown
						OR bNewJobScreens
							
							#IF IS_DEBUG_BUILD
							IF bAllAwardsBeenShown
								PRINTLN("[Survival] - bAllAwardsBeenShown = TRUE.")
							ENDIF
							IF bNewJobScreens
								PRINTLN("[Survival] - bNewJobScreens = TRUE.")
							ENDIF
							#ENDIF
						
							IF bNewJobScreens = TRUE
								CLEAR_ALL_BIG_MESSAGES()	
							ENDIF
							
							//If the player is on a playlist
							IF IS_PLAYER_ON_A_PLAYLIST(localPlayer)
								
								PRINTLN("[Survival] - PROCESS_eHORDESTAGE_REWARDS_AND_FEEDACK_CLIENT - IS_PLAYER_ON_A_PLAYLIST = TRUE.")
								
								IF iEndOfWaveHudStage >= 3
									PRINTLN("[Survival] - PROCESS_eHORDESTAGE_REWARDS_AND_FEEDACK_CLIENT - iEndOfWaveHudStage >= 3")
									IF CONTROL_PLAYLIST_ON_MISSION_LEADER_BOARD_BUTTONS(sEndOfMission.iProgress, sEndOfMission, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
										PRINTLN("[Survival] - PROCESS_eHORDESTAGE_REWARDS_AND_FEEDACK_CLIENT - CONTROL_PLAYLIST_ON_MISSION_LEADER_BOARD_BUTTONS = TRUE")
										CLEANUP_BEFORE_END()
										SET_BIT(playerBD[iLocalPart].iBitset, BITSET_PLAYER_I_HAVE_DEALT_WITH_FEEDBACK)
									ELSE
										PRINTLN("[Survival] - PROCESS_eHORDESTAGE_REWARDS_AND_FEEDACK_CLIENT - CONTROL_PLAYLIST_ON_MISSION_LEADER_BOARD_BUTTONS = FALSE")
									ENDIF
								ENDIF
								
							//Not on a playlist do this:
							ELSE
								
								PRINTLN("[Survival] - PROCESS_eHORDESTAGE_REWARDS_AND_FEEDACK_CLIENT - IS_PLAYER_ON_A_PLAYLIST = FALSE.")
								
								// Finish up
								IF bMoveToEnd
									IF IS_LOCAL_BIT_SET(ciLocalBool_bIamQuittingCoronaSpectator)
										DEACTIVATE_SPECTATOR_CAM(g_BossSpecData/*, DSCF_REMAIN_VIEWING_TARGET*/)
									ENDIF
									CLEANUP_BEFORE_END()
									SET_BIT(playerBD[iLocalPart].iBitset, BITSET_PLAYER_I_HAVE_DEALT_WITH_FEEDBACK)
									
									#IF IS_DEBUG_BUILD
									PRINTLN("[Survival] - [CS_HORDE] PROCESS_eHORDESTAGE_REWARDS_AND_FEEDACK_CLIENT - bMoveToEnd")
									#ENDIF
								ELSE
									IF IS_BIT_SET(playerBD[iLocalPart].iBitset, BITSET_PLAYER_ABANDONED_HORDE)
										sEndOfMission.bBeingKickedFromSurvival = TRUE
										PRINTLN("[Survival] - PROCESS_eHORDESTAGE_REWARDS_AND_FEEDACK_CLIENT - BITSET_PLAYER_ABANDONED_HORDE = TRUE.")
									ENDIF
									IF bNewJobScreens = FALSE
										PRINTLN("[Survival] - PROCESS_eHORDESTAGE_REWARDS_AND_FEEDACK_CLIENT - bNewJobScreens = FALSE.")
										IF MAINTAIN_END_OF_MISSION_SCREEN(lbdVars,sEndOfMission, serverBD.sServerFMMC_EOM,TRUE,FALSE,FALSE,FALSE,FALSE,TRUE, NULL, 0, "", FALSE, TRUE, SHOULD_RUN_NEW_VOTING_SYSTEM(sEndOfMission.iQuitProgress, serverBD.sServerFMMC_EOM.iVoteStatus))
											PRINTLN("[Survival] - PROCESS_eHORDESTAGE_REWARDS_AND_FEEDACK_CLIENT - MAINTAIN_END_OF_MISSION_SCREEN = TRUE.")
											IF SHOULD_RUN_NEW_VOTING_SYSTEM(sEndOfMission.iQuitProgress, serverBD.sServerFMMC_EOM.iVoteStatus)
												PRINTLN("[Survival] - PROCESS_eHORDESTAGE_REWARDS_AND_FEEDACK_CLIENT - SHOULD_RUN_NEW_VOTING_SYSTEM = TRUE.")
												IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(localPlayer)].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bQuitJob)
													SET_READY_FOR_NEXT_JOBS(nextJobStruct)
													// Set up end Job screens
													bNewJobScreens = TRUE
													PRINTLN("[Survival] - PROCESS_eHORDESTAGE_REWARDS_AND_FEEDACK_CLIENT - [CS_HORDE] bNewJobScreens = TRUE")
													PRINTLN("[Survival] - PROCESS_eHORDESTAGE_REWARDS_AND_FEEDACK_CLIENT - setting bNewJobScreens = TRUE.")
												ELSE
													// Just finish up
													bMoveToEnd = TRUE
													PRINTLN("[Survival] - [CS_HORDE] PROCESS_eHORDESTAGE_REWARDS_AND_FEEDACK_CLIENT - bMoveToEnd = TRUE")
													#IF IS_DEBUG_BUILD
													IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(localPlayer)].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bQuitJob)
														PRINTLN("[Survival] - PROCESS_eHORDESTAGE_REWARDS_AND_FEEDACK_CLIENT - bQuitJob = TRUE.")
													ENDIF
													#ENDIF
												ENDIF
											ELSE
												// Just finish up
												bMoveToEnd = TRUE
												PRINTLN("[Survival] - [CS_HORDE] PROCESS_eHORDESTAGE_REWARDS_AND_FEEDACK_CLIENT - bMoveToEnd = TRUE")
												PRINTLN("[Survival] - PROCESS_eHORDESTAGE_REWARDS_AND_FEEDACK_CLIENT - SHOULD_RUN_NEW_VOTING_SYSTEM = FALSE.")
											ENDIF
										ELSE
											PRINTLN("[Survival] - [CS_HORDE] PROCESS_eHORDESTAGE_REWARDS_AND_FEEDACK_CLIENT - MAINTAIN_END_OF_MISSION_SCREEN = FALSE")
											PRINTLN("[Survival] - PROCESS_eHORDESTAGE_REWARDS_AND_FEEDACK_CLIENT - MAINTAIN_END_OF_MISSION_SCREEN = FALSE.")
										ENDIF
									ELSE
										PRINTLN("[Survival] - PROCESS_eHORDESTAGE_REWARDS_AND_FEEDACK_CLIENT - bNewJobScreens = TRUE.")
										IF NOT sEndOfMission.bBeingKickedFromSurvival
											IF DO_END_JOB_VOTING_SCREEN(g_sMC_serverBDEndJob, nextJobStruct, g_sMC_serverBDEndJob.timerVotingEnds.Timer)
												PRINTLN("[Survival] - PROCESS_eHORDESTAGE_REWARDS_AND_FEEDACK_CLIENT - DO_END_JOB_VOTING_SCREEN = TRUE.")
												bMoveToEnd = TRUE
											ELSE
												PRINTLN("[Survival] - PROCESS_eHORDESTAGE_REWARDS_AND_FEEDACK_CLIENT - DO_END_JOB_VOTING_SCREEN = FALSE.")
											ENDIF
										ELSE
											#IF IS_DEBUG_BUILD
											IF sEndOfMission.bBeingKickedFromSurvival
												PRINTLN("[Survival] - PROCESS_eHORDESTAGE_REWARDS_AND_FEEDACK_CLIENT - bBeingKickedFromSurvival = TRUE.")
											ENDIF
											#ENDIF
											bMoveToEnd = TRUE
										ENDIF
										PRINTLN("[Survival] - PROCESS_eHORDESTAGE_REWARDS_AND_FEEDACK_CLIENT - [CS_HORDE] DO_END_JOB_VOTING_SCREEN")
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						PRINTLN("[Survival] - PROCESS_eHORDESTAGE_REWARDS_AND_FEEDACK_CLIENT - g_bCelebrationScreenIsActive = TRUE.")
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH	
ENDPROC
