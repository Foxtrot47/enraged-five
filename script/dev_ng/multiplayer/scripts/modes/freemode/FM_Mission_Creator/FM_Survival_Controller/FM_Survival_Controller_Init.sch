USING "FM_Survival_Controller_Using.sch"
USING "FM_Survival_Controller_Utility.sch"
USING "FM_Survival_Controller_Peds.sch"
USING "FM_Survival_Controller_Debug.sch"

PROC INIT_LOCAL_PLAYER_DATA()

	CACHE_PLAYER_DATA()
	
	iWaveToUse = serverBD.sWaveData.iWaveCount
	IF iWaveToUse > g_FMMC_STRUCT.sSurvivalWaveData.iNumberOfWaves
		iWaveToUse = g_FMMC_STRUCT.sSurvivalWaveData.iNumberOfWaves
	ENDIF
ENDPROC

PROC INIT_LOCAL_SPECTATOR_DATA()
	IF NOT IS_A_SPECTATOR_CAM_ACTIVE()
		EXIT
	ENDIF
	
	PED_INDEX tempPed
	PLAYER_INDEX tempPlayer
	PARTICIPANT_INDEX tempPart
	BOOL bspectatorvalid
	
	iSpectatorTarget = -1
	
	tempPed = GET_SPECTATOR_CURRENT_FOCUS_PED()
	IF DOES_ENTITY_EXIST(tempPed)
		IF NOT IS_ENTITY_DEAD(tempPed)
			IF IS_PED_A_PLAYER(tempPed)
				tempPlayer = NETWORK_GET_PLAYER_INDEX_FROM_PED(tempPed)
				IF NETWORK_IS_PLAYER_A_PARTICIPANT(tempPlayer)
					tempPart = NETWORK_GET_PARTICIPANT_INDEX(tempPlayer)
					iSpectatorTarget = NATIVE_TO_INT(tempPart)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF iSpectatorTarget !=-1
		bspectatorvalid = TRUE
	ENDIF
	
	IF bSpectatorvalid
		IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
			IF iPartToUse != iSpectatorTarget
				iPartToUse = iSpectatorTarget
				PlayerToUse = tempPlayer
				PlayerPedToUse = tempPed
				PRINTLN("INIT_LOCAL_SPECTATOR_DATA - iPartToUse now: ", iPartToUse)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC INIT_PLAYER_BD_DATA()
	
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		EXIT
	ENDIF
	
	playerBD[iLocalPart].sPlayerData.iPart = iLocalPart
	
	playerBD[iLocalPart].sPlayerData.bPlayerOK = bLocalPlayerOK
	playerBD[iLocalPart].sPlayerData.piPlayerPedID = LocalPlayerPed
	playerBD[iLocalPart].sPlayerData.piPlayerID = LocalPlayer
	
	IF NETWORK_IS_PARTICIPANT_ACTIVE(PARTICIPANT_ID())
		IF NOT IS_PLAYER_SPECTATING(localPlayer)
		OR playerBD[iLocalPart].eSpectatorState > ci_SpectatorState_MAINTAIN
			INT iPartInTeam = GET_PARTICIPANT_NUMBER_IN_GAME(localPlayer)
			IF playerBD[iLocalPart].sPlayerData.iPartInTeam != iPartInTeam
				playerBD[iLocalPart].sPlayerData.iPartInTeam = iPartInTeam
				iLocalPartInTeam = iPartInTeam
				PRINTLN("INIT_PLAYER_BD_DATA - playerBD[iLocalPart].sPlayerData.iPartInTeam: ", playerBD[iLocalPart].sPlayerData.iPartInTeam)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC LOCAL_PLAYER_INIT()
	IF IS_FAKE_MULTIPLAYER_MODE_SET()
		SET_IDLE_KICK_TIME_OVERRIDDEN(180000)
		PRINTLN("[Survival] LOCAL_PLAYER_INIT - SET_IDLE_KICK_TIME_OVERRIDDEN: ",180000)
	ENDIF
	IF GET_ENTITY_HEALTH(localPlayerPed) != GET_PED_MAX_HEALTH(localPlayerPed)
		PRINTLN("[Survival] LOCAL_PLAYER_INIT - Healing local player to full")
		SET_ENTITY_HEALTH(localPlayerPed, GET_PED_MAX_HEALTH(localPlayerPed))
	ENDIF
	
	SET_ONLY_ALLOW_AMMO_COLLECTION_WHEN_LOW(FALSE)
ENDPROC

PROC SETUP_STRAIGHT_TO_SPECTATOR_PLAYER()
    NET_SET_PLAYER_CONTROL(localPlayer, FALSE, NSPC_SET_INVISIBLE | NSPC_NO_COLLISION | NSPC_FREEZE_POSITION)
	SET_MP_DECORATOR_BIT(localPlayer, MP_DECORATOR_BS_INVALID_TO_SPECTATE)
ENDPROC

PROC CLEAR_STRAIGHT_TO_SPECTATOR_PLAYER()
    NET_SET_PLAYER_CONTROL(localPlayer, TRUE)
	CLEAR_MP_DECORATOR_BIT(localPlayer, MP_DECORATOR_BS_INVALID_TO_SPECTATE)
ENDPROC

PROC SETUP_SURVIVAL_PLAYER_SPAWN_POSITION(BOOL bForStartMode)
	IF bForStartMode
		bForStartMode = bForStartMode
	ENDIF
	
	INT iPartInTeam = playerBD[iLocalPart].sPlayerData.iPartInTeam
	PRINTLN("SETUP_SURVIVAL_PLAYER_SPAWN_POSITION - iPartInTeam: ", iPartInTeam)
	VECTOR vStartPos = serverBD.vStartPos[iPartInTeam]
	PRINTLN("SETUP_SURVIVAL_PLAYER_SPAWN_POSITION - serverBD.vStartPos[",iPartInTeam,"]: ", serverBD.vStartPos[iPartInTeam])
	IF IS_VECTOR_ZERO(vStartPos)
		PRINTLN("SETUP_SURVIVAL_PLAYER_SPAWN_POSITION - Using backup position")
		vStartPos = serverBD.vStartPos[0]
	ENDIF
	
	SETUP_SPECIFIC_SPAWN_LOCATION(vStartPos, serverBD.fStartHeading[iPartInTeam], 0.0, TRUE, 0.0, FALSE, FALSE, 2.0)        
	SET_PLAYER_NEXT_RESPAWN_LOCATION(SPAWN_LOCATION_AT_SPECIFIC_COORDS, TRUE)
	IF NOT IS_FAKE_MULTIPLAYER_MODE_SET() // can't do this when gametype is creator. (IS_PLAYER_ON_ANY_MP_MISSION returns false)
		SET_PLAYERS_RESPAWN_TO_SPAWN_CLOSER_TO_ROADS_ON_THIS_MISSION(FALSE)
	ENDIF	
	SET_MISSION_SPAWN_OCCLUSION_ANGLED_AREA(<<-86.293831,-759.790771,43.217434>>, <<-78.534744,-786.002869,36.354721>>, 12.000000, DEFAULT, TRUE)
ENDPROC

/// PURPOSE:
///    Loads text required for the Horde script.
/// RETURNS:
///    TRUE if text has been loaded, FALSE if not.
FUNC BOOL LOAD_HORDE_TEXT_BLOCK()
	REQUEST_ADDITIONAL_TEXT("FMHRD", MISSION_TEXT_SLOT)
    IF HAS_THIS_ADDITIONAL_TEXT_LOADED("FMHRD", MISSION_TEXT_SLOT)
        PRINTLN("[Survival] - loaded text FMHRD into MISSION_TEXT_SLOT")
        RETURN TRUE
    ELSE
        PRINTLN("[Survival] - loading text FMHRD into MISSION_TEXT_SLOT")
    ENDIF
    
    RETURN FALSE
    
ENDFUNC


FUNC BOOL LOAD_ALL_ASSETS()
	BOOL bAllLoaded = TRUE
	
	REQUEST_ANIM_SET("ANIM_GROUP_MOVE_BALLISTIC")   
    REQUEST_ANIM_SET("MOVE_STRAFE_BALLISTIC")
    REQUEST_CLIP_SET("move_ballistic_2h")
    IF NOT HAS_ANIM_SET_LOADED("ANIM_GROUP_MOVE_BALLISTIC")
    OR NOT HAS_ANIM_SET_LOADED("MOVE_STRAFE_BALLISTIC")
    OR NOT HAS_CLIP_SET_LOADED("move_ballistic_2h")
		PRINTLN("LOAD_ALL_ASSETS - Haven't loaded juggernaut movement anims yet.")
		bAllLoaded = FALSE
    ENDIF
	
	IF IS_HALLOWEEN_MODE()
		REQUEST_NAMED_PTFX_ASSET("scr_bike_adversary")
		IF NOT HAS_NAMED_PTFX_ASSET_LOADED("scr_bike_adversary")
			PRINTLN("LOAD_ALL_ASSETS - Not loaded biker adversary effect asset yet")
			bAllLoaded = FALSE	
		ENDIF
		REQUEST_NAMED_PTFX_ASSET("scr_gr_def")
		IF NOT HAS_NAMED_PTFX_ASSET_LOADED("scr_gr_def")
			PRINTLN("LOAD_ALL_ASSETS - Not loaded scr_gr_def asset yet")
			bAllLoaded = FALSE	
		ENDIF
		REQUEST_ANIM_DICT("ANIM@SPECIAL_PEDS@ALIEN_LANDING")
		IF NOT HAS_ANIM_DICT_LOADED("ANIM@SPECIAL_PEDS@ALIEN_LANDING")
			PRINTLN("LOAD_ALL_ASSETS - Not loaded ANIM@SPECIAL_PEDS@ALIEN_LANDING anim dict yet")
			bAllLoaded = FALSE	
		ENDIF
		REQUEST_MODEL(prop_alien_egg_01)
		IF NOT HAS_MODEL_LOADED(prop_alien_egg_01)
			PRINTLN("LOAD_ALL_ASSETS - Not loaded Alien egg yet")
			bAllLoaded = FALSE	
		ENDIF
		REQUEST_MODEL(PROP_POWER_CELL)
		IF NOT HAS_MODEL_LOADED(PROP_POWER_CELL)
			PRINTLN("LOAD_ALL_ASSETS - Not loaded Alien Tech yet")
			bAllLoaded = FALSE	
		ENDIF
	ENDIF
	
	RETURN bAllLoaded
ENDFUNC

PROC SETUP_FIGHTING_PARTICIPANT_LIST_DEFAULT_VALUES()
	INT i
    REPEAT MAX_NUMBER_HORDE_FIGHTING_PARTICIPANTS i
        serverBD.sFightingParticipantsList.iParticipant[i] = (-1)
    ENDREPEAT
ENDPROC

PROC SET_NUMBER_OF_STARTING_PLAYERS()
	INT iPlayer, iCount
	PLAYER_INDEX piPlayer
	FOR iPlayer = 0 TO NUM_NETWORK_PLAYERS-1
		piPlayer = INT_TO_PLAYERINDEX(iPlayer)
		IF IS_NET_PLAYER_OK(piPlayer, FALSE)
			IF (NOT IS_PLAYER_SPECTATING(piPlayer)
			AND NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(piPlayer)].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_JoinedAsSpectator))
			OR playerBD[iPlayer].eSpectatorState > ci_SpectatorState_MAINTAIN
				iCount++
			ENDIF
		ENDIF
	ENDFOR
	
	serverBD.iNumberOfStartingPlayers = iCount
	PRINTLN("SET_NUMBER_OF_STARTING_PLAYERS - serverBD.iNumberOfStartingPlayers: ", serverBD.iNumberOfStartingPlayers)
ENDPROC

PROC INIT_SERVER_DATA()
	INT i
	
	FOR i = 0 TO MAX_SURVIVAL_START_POS-1
		serverBD.vStartPos[i] = g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[0][i].vPos
		serverBD.fStartHeading[i] = g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[0][i].fHead
		PRINTLN("INIT_SERVER_DATA - Setting up start point ", i, " Pos: ", serverBD.vStartPos[i], " Heading: ", serverBD.fStartHeading[i])
	ENDFOR
	
	serverBD.vBoundsMiddle = SURVIVAL_GET_PLACED_BOUNDS_MIDDLE()
	
	serverBD.iWeather = g_FMMC_STRUCT.iWeather	
	PRINTLN("[Survival] serverBD.iWeather=",serverBD.iWeather) 
	serverBD.iTimeOfDay = g_FMMC_STRUCT.iTimeOfDay	
	PRINTLN("[Survival] serverBD.iTimeOfDay=",serverBD.iTimeOfDay)
	
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfPeds-1
		serverBD.sWaveData.vSquadSpawnPos[i] = g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].vPos
		PRINTLN("INIT_SERVER_DATA - Squad Spawn Position ", i, ": ", serverBD.sWaveData.vSquadSpawnPos[i])
	ENDFOR
	
	CLEAR_SQUAD_SPAWN_POINTS()
	
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles-1
		serverBD.sWaveData.vLVehSpawnPos[i] = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].vPos
	ENDFOR
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sSurvivalWaveData.iBitset, ciSurvivalBS_EndlessWaves)
		SET_BIT(serverBD.iBitset, BITSET_SERVER_ENDLESS_WAVES_MODE)
		PRINTLN("INIT_SERVER_DATA - BITSET_SERVER_ENDLESS_WAVES_MODE is set for this mode.")
	ENDIF
	
	CLEAR_HEAVY_UNIT_SERVER_DATA()
	
	SET_NUMBER_OF_STARTING_PLAYERS()
ENDPROC

PROC SET_PED_AND_TRAFFIC_DENSITIES()
	VECTOR vMinWorldLimitCoords = <<-15500, -15500, -1700>>
	VECTOR vMaxWorldLimitCoords = <<15500, 15500, 2700>>
	
	IF NOT DOES_SCENARIO_BLOCKING_AREA_EXISTS(vMinWorldLimitCoords, vMaxWorldLimitCoords)
		sbiScenarioBlockingIndex = ADD_SCENARIO_BLOCKING_AREA(vMinWorldLimitCoords, vMaxWorldLimitCoords, FALSE, TRUE, TRUE, TRUE)
	ENDIF
	
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(vMinWorldLimitCoords, vMaxWorldLimitCoords, FALSE, FALSE)
	
	IF NOT DID_I_JOIN_MISSION_AS_SPECTATOR()
		CLEAR_AREA_OF_PROJECTILES((<<0.0, 0.0, 0.0>>), 10000, FALSE)
		CLEAR_AREA_OF_VEHICLES((<<0.0, 0.0, 0.0>>), 10000)
		CLEAR_AREA_OF_PEDS((<<0.0, 0.0, 0.0>>), 10000, TRUE)
	ENDIF
	
	PRINTLN("SET_PED_AND_TRAFFIC_DENSITIES - Set up done for ambient entities")
	bSetBlockingAreas = TRUE
ENDPROC

/// PURPOSE:
///    Process pre game logic. Called once at beginning of script.
/// PARAMS:
///    structHordeMissionData - mission data.
/// RETURNS:
///    FALSE if the script fails to receive an initial network broadcast.
FUNC BOOL PROCESS_PRE_GAME(MP_MISSION_DATA &structHordeMissionData)

	INT i, j
	
	SET_DISABLE_RANK_UP_MESSAGE(TRUE)
	
	serverBD.sServerFMMC_EOM.bAllowedToVote = FALSE
	
	IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
		GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_SURVIVAL
	ELSE
		g_TransitionSessionNonResetVars.sGlobalCelebrationData.bInTestMode = TRUE
		SET_LOCAL_BIT(ciLocalBool_bInTest)
	ENDIF
	FMMC_PROCESS_PRE_GAME_COMMON(structHordeMissionData, serverBD.sFMMC_SBD.iPlayerMissionToLoad, serverBD.sFMMC_SBD.iMissionVariation,NUM_NETWORK_PLAYERS, FALSE, FALSE)
	
	// For weapons in respawning.
	g_bOnHorde = TRUE
	
	#IF IS_DEBUG_BUILD
	DUMP_WAVE_DATA_INFO()
	#ENDIF
	
	WHILE NOT bSkipTaxiScriptCheck
		WAIT(0)
		IF NOT HAS_NET_TIMER_STARTED(stTaxtTerminateFailsafe)
			START_NET_TIMER(stTaxtTerminateFailsafe)
			PRINTLN("[Survival] - PROCESS_PRE_GAME - stTaxtTerminateFailsafe timer started.")
		ELSE
			IF HAS_NET_TIMER_EXPIRED(stTaxtTerminateFailsafe, 10000)
				bSkipTaxiScriptCheck = TRUE
				PRINTLN("[Survival] - PROCESS_PRE_GAME - stTaxtTerminateFailsafe timer expired.")
			ENDIF
		ENDIF
		IF NOT NETWORK_IS_SCRIPT_ACTIVE("AM_TAXI", DEFAULT_MISSION_INSTANCE, TRUE)
		AND NOT NETWORK_IS_SCRIPT_ACTIVE("AM_CRATE_DROP", DEFAULT_MISSION_INSTANCE, TRUE)
			PRINTLN("[Survival] - PROCESS_PRE_GAME - AM_TAXI.sc and AM_CRATE_DROP.sc are no longer active! ")
			bSkipTaxiScriptCheck = TRUE
		ENDIF
	ENDWHILE
	
	SETUP_FIGHTING_PARTICIPANT_LIST_DEFAULT_VALUES()
	
	// Reserve!
	RESERVE_NETWORK_MISSION_PEDS(MAX_NUM_ACTIVE_PEDS)
	RESERVE_NETWORK_MISSION_VEHICLES(MAX_SURV_HELI+MAX_SURV_LAND_VEH)
	
	// For heli spawning, give each one a different location on the survival area circumference so they dopn;t spawn too near each other.
	serverBD_Enemies_V.sSurvivalHeli[0].iCircCount = SURVIVAL_HELI_0_START_CIRC_COUNT
	serverBD_Enemies_V.sSurvivalHeli[1].iCircCount = SURVIVAL_HELI_1_START_CIRC_COUNT
	serverBD_Enemies_V.sSurvivalHeli[2].iCircCount = SURVIVAL_HELI_2_START_CIRC_COUNT
	
	DISABLE_CUSTOM_WEAPON_LOADOUT_FOR_MISSIONS()
	
	RESET_SERVER_NEXT_JOB_VARS()
	
	// Register broadcast data.
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD), "serverBD")
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD_Enemies_S, SIZE_OF(serverBD_Enemies_S), "serverBD_Enemies_S")
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD_Enemies_V, SIZE_OF(serverBD_Enemies_V), "serverBD_Enemies_V")
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(g_sMC_serverBDEndJob, SIZE_OF(g_sMC_serverBDEndJob), "g_sMC_serverBDEndJob")
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD), "playerBD")

	// KGM: Wait for the first network broadcast before moving on
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		RETURN FALSE
	ENDIF
	
	PRINTLN("[Survival] - PROCESS_PRE_GAME - iCurrentMissionType = ", GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType)

	CLEAR_HELP(TRUE)
	
	NETWORK_OVERRIDE_TEAM_RESTRICTIONS(0, TRUE)
	
	LOAD_HORDE_TEXT_BLOCK()
	
	TOGGLE_STRIPPER_AUDIO(FALSE)
	
	IF INCREMENT_STARTING_ACTIVITY_CHEAT_STAT_BY() > 0
		INCREMENT_MP_INT_CHARACTER_STAT(MP_STAT_SURVIVAL_START)
		SET_JOB_ACTIVITY_PROFILE_SETTINGS(PSJA_SURVIVAL_STARTED)
		PRINTLN("[Survival] - PROCESS_PRE_GAME - called SET_JOB_ACTIVITY_PROFILE_SETTINGS(PSJA_SURVIVAL_STARTED).")
	ENDIF

	SET_AUDIO_FLAG("DisableFlightMusic", TRUE)
	SET_AUDIO_FLAG("WantedMusicDisabled", TRUE)
	
	PRINTLN("[Survival] - PROCESS_PRE_GAME - player team = ", g_FMMC_STRUCT.iMyTeam)
	IF g_FMMC_STRUCT.iMyTeam != 0
	    g_FMMC_STRUCT.iMyTeam = 0
	    PRINTLN("[Survival] - PROCESS_PRE_GAME - player team now = ", g_FMMC_STRUCT.iMyTeam)
	ENDIF
	
	IF NOT IS_PLAYER_SCTV(PLAYER_ID())
		SET_PLAYER_TEAM(PLAYER_ID(), 0)
	ENDIF
	
	iStartHits = GET_MP_INT_CHARACTER_STAT(MP_STAT_HITS_PEDS_VEHICLES)
	iStartShots = GET_MP_INT_CHARACTER_STAT(MP_STAT_SHOTS)

	// Setup land vehicle ped models.
	REPEAT MAX_NUM_LAND_VEHICLES i
		REPEAT NUM_LAND_VEH_PEDS j
			serverBD_Enemies_V.sSurvivalLandVehicle[i].sPed[j].eModel = g_FMMC_STRUCT.sSurvivalWaveData.mnPedModel[iWaveToUse][eWAVESTAGE_EASY]
		ENDREPEAT
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
	REPEAT 10 i
		PRINTLN("[Survival] - PROCESS_PRE_GAME - placed vehicle ", i, " = ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].vPos)
	ENDREPEAT
	#ENDIF
	
	SET_BIG_MESSAGE_ENABLED_WHEN_SPECTATING(TRUE)
	
	SET_LOCAL_PLAYER_CAN_COLLECT_PORTABLE_PICKUPS(TRUE)
	
	DISABLE_MP_PASSIVE_MODE(PMER_START_OF_A_MATCH)
	NETWORK_SET_FRIENDLY_FIRE_OPTION(FALSE)
	
	IF DOES_ENTITY_EXIST(LocalPlayerPed)
    	IF NOT IS_ENTITY_DEAD((LocalPlayerPed))
			SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_WillFlyThroughWindscreen, FALSE)
			SET_ENTITY_HEALTH(LocalPlayerPed, GET_ENTITY_MAX_HEALTH(LocalPlayerPed))
			SET_ENTITY_VISIBLE(LocalPlayerPed, FALSE)
			SET_PED_USING_ACTION_MODE(LocalPlayerPed, TRUE)
		ENDIF
	ENDIF
	
	SET_MAX_WANTED_LEVEL(0)
	IF bLocalPlayerOK
		SET_PLAYER_WANTED_LEVEL(localPlayer, 0)
		SET_WANTED_LEVEL_MULTIPLIER(0.0)
		SET_PLAYER_WANTED_LEVEL_NOW(localPlayer)
		SET_DISPATCH_COPS_FOR_PLAYER(localPlayer, FALSE)
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		bIsLocalPlayerHost = NETWORK_IS_HOST_OF_THIS_SCRIPT()
		// Host only functions.
		IF bIsLocalPlayerHost
			// For UGC.
			PLAYSTATS_CREATE_MATCH_HISTORY_ID_2(serverBD.iMatchHistoryId[1], serverBD.iMatchHistoryId[0])
			// Squad assignment.
			INITIALSE_ENEMIES()
			
			IF g_FMMC_STRUCT.sSurvivalWaveData.iNumberOfWaves != serverBD.iNumberOfWaves
				serverBD.iNumberOfWaves = g_FMMC_STRUCT.sSurvivalWaveData.iNumberOfWaves
				PRINTLN("[Survival] - PROCESS_PRE_GAME - serverBD.iNumberOfWaves set to ", serverBD.iNumberOfWaves)
			ENDIF
			
			INIT_SERVER_DATA()
			
			IF IS_BIT_SET(g_FMMC_STRUCT.sSurvivalWaveData.iBitset, ciSUrvivalBS_Halloween)
				SET_BIT(serverBD.iBitset, BITSET_SERVER_HALLOWEEN_MODE)
			ENDIF
		ENDIF
	ENDIF
	
	g_FMMC_STRUCT_ENTITIES.iPedRespawnTime = FMMC_PED_RESPAWN_TIME_RANDOM

	#IF IS_DEBUG_BUILD
	
	IF IS_VECTOR_ZERO(serverBD.vStartPos[0])
	    NET_SCRIPT_ASSERT("[Survival] - PROCESS_PRE_GAME - serverBD.vStartPos[0] = << 0.0, 0.0, 0.0 >>")
		serverBD.vStartPos[0] = g_FMMC_STRUCT.sSurvivalWaveData.vBounds[0]
	ENDIF
	
	PRINTLN("[Survival] - PROCESS_PRE_GAME - serverBD.vStartPos[0] = ", serverBD.vStartPos[0])
	
	#ENDIF
	
	IF IS_FAKE_MULTIPLAYER_MODE_SET()
		ADD_RELATIONSHIP_GROUP("rgFM_AiHate", rgFM_AiHate)
		serverBD.sWaveData.iWaveCount = g_iStartingWave
	ENDIF
	
	IF NOT bSetBlockingAreas
		SET_PED_AND_TRAFFIC_DENSITIES()
	ENDIF
	
	IF GET_SERVER_HORDE_STAGE() = eHORDESTAGE_JIP_IN
		CLEAR_AREA(serverBD.vStartPos[0], 2000.0, FALSE, TRUE, FALSE, FALSE)
	ENDIF

	sEndBoardData.iScores[0] = (-1)
	sEndBoardData.iScores[1] = (-1)
	sEndBoardData.iScores[2] = (-1)
	sEndBoardData.iScores[3] = (-1)
	
	SET_GAME_STATE_ON_DEATH(AFTERLIFE_SET_SPECTATORCAM)
	
	ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, FALSE)
	ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, FALSE)
	
	CLIENT_SET_UP_PROXIMITY_CHAT(CHAT_PROXIMITY_MAX)
	
	// Setup spawning.
	SETUP_SURVIVAL_PLAYER_SPAWN_POSITION(TRUE)
	
	// The survival area bounds.
	SETUP_SURVIVAL_BOUNDS_DATA()
	
	// So fleeing ambient peds don't drive through the middle of the survival shootout.
	PRINTLN("[Survival] - PROCESS_PRE_GAME - SET_ROADS_IN_AREA - about to be called. bTurnedOffRoadNodes = ", bTurnedOffRoadNodes)
	IF NOT bTurnedOffRoadNodes
		SET_ROADS(FALSE)
		bTurnedOffRoadNodes = TRUE
	ENDIF
	
	// Do initial setup of pickups for wave 1.
	SET_PICKUP_TYPES_FOR_WAVE()
	
	// Set the weather.
	SET_WEATHER_FOR_FMMC_MISSION(serverBD.iWeather)
	
	// Keep TOD constant for duration of script.
	IF GET_SERVER_HORDE_STAGE() <= eHORDESTAGE_START_FADE_IN
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciFORCE_SPECIFIC_TOD)
			SET_TIME_OF_DAY(serverBD.iTimeOfDay)
		ELSE
			NETWORK_OVERRIDE_CLOCK_TIME(g_FMMC_STRUCT.iTODOverrideHours, g_FMMC_STRUCT.iTODOverrideMinutes, 0)
		ENDIF
	ENDIF
	
	DISABLE_PERSONAL_VEHICLE_CREATION_FOR_MISSION_STARTUP(TRUE)
	CLEANUP_MP_SAVED_VEHICLE(TRUE)
	
	INIT_WORLD_STATE()
	
	// Setup healthbar tags.
	g_iOverheadNamesState = g_FMMC_STRUCT.iHealthBar
	
	g_bBlockAIKillXP = TRUE
	
	HIDE_PLAYER(TRUE)
	
	IF DID_I_JOIN_MISSION_AS_SPECTATOR()
		IF NOT IS_SCREEN_FADED_OUT()
			DO_SCREEN_FADE_OUT(500)
		ENDIF
	ENDIF
	
	SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_SPECTATOR_TEAM_VOICE_CHAT)
	
	// If we're entering the mission at the beginning, set tod to corona option.
	IF GET_SERVER_HORDE_STAGE() <= eHORDESTAGE_START_FADE_IN
		SET_CLIENT_GAME_STATE(SURVIVAL_GAME_STATE_INIT)
	ENDIF
	
	HANDLE_SURVIVAL_IPL_AND_INTERIORS()
	
	g_iSurvivalEndReason = -1
	
	FOR i = 0 TO ciMAX_UFO_LIGHTS-1
		iUFOIndex[i] = -1
	ENDFOR
	FOR i = 0 TO GET_FMMC_MAX_NUM_PROPS()-1
		iUFOSoundIndex[i] = -1
	ENDFOR
	SET_UFO_LIGHT_INDEXES()
	SET_UFO_SOUND_INDEXES()
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons-1
		iPickupSounds[i] = -1
	ENDFOR
	
	#IF IS_DEBUG_BUILD
	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(bShowOnScreenDebug)
	#ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL LOAD_AND_CREATE_ALL_FMMC_ENTITIES_WK(FMMC_SERVER_DATA_STRUCT &sSDSpassed)

    IF REQUEST_LOAD_FMMC_MODELS()
		
		PRINTLN("[Survival] - [Create Entities] - REQUEST_LOAD_FMMC_MODELS = TRUE.")
		
        //Set the number of entities to load
        INT iNumberOfPeds       = g_FMMC_STRUCT_ENTITIES.iNumberOfPeds
        INT iNumberOfVehicles   = g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles
    	INT iNumberOfObjects	= g_FMMC_STRUCT_ENTITIES.iNumberOfObjects+g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps

        IF CAN_REGISTER_MISSION_ENTITIES(iNumberOfPeds, iNumberOfVehicles, iNumberOfObjects,  0)
			PRINTLN("[Survival] - [Create Entities] - CAN_REGISTER_MISSION_ENTITIES(", iNumberOfPeds, iNumberOfVehicles, iNumberOfObjects, ") = TRUE.")
            IF CREATE_FMMC_OBJECTS_MP(sSDSpassed.niObject)
				PRINTLN("[Survival] - [Create Entities] - CREATE_FMMC_OBJECTS_MP = TRUE.")
				IF CREATE_FMMC_DYNOPROPS_MP(sSDSpassed.niDynoProps,TRUE)
					PRINTLN("[Survival] - [Create Entities] - CREATE_FMMC_DYNOPROPS_MP = TRUE.")
				ELSE
					PRINTLN("[Survival] - [Create Entities] - CREATE_FMMC_DYNOPROPS_MP = FALSE.")
					RETURN FALSE
				ENDIF
            ELSE
                PRINTLN("[Survival] - [Create Entities] - CREATE_FMMC_OBJECTS_MP = FALSE.")
				RETURN FALSE
            ENDIF
        ELSE
			PRINTLN("[Survival] - [Create Entities] - CAN_REGISTER_MISSION_ENTITIES(", iNumberOfPeds, iNumberOfVehicles, iNumberOfObjects,  0, ") = FALSE.")
            RETURN FALSE
        ENDIF
    ELSE
		PRINTLN("[Survival] - [Create Entities] - REQUEST_LOAD_FMMC_MODELS = FALSE.")
    ENDIF
	
    PRINTLN("[Survival] - [Create Entities] - LOAD_AND_CREATE_ALL_FMMC_ENTITIES_WK = TRUE.")
    RETURN TRUE
    
ENDFUNC

PROC SET_PROP_MODELS_AS_NO_LONGER_NEEDED()
    INT i
    REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfProps i
        IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn != DUMMY_MODEL_FOR_SCRIPT
            SET_MODEL_AS_NO_LONGER_NEEDED(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn)
        ENDIF
    ENDREPEAT
ENDPROC

PROC SET_VEHICLE_MODELS_AS_NO_LONGER_NEEDED()
    INT i
    REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles i
        IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn != DUMMY_MODEL_FOR_SCRIPT
            SET_MODEL_AS_NO_LONGER_NEEDED(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn)
        ENDIF
    ENDREPEAT
ENDPROC

PROC SET_OBJECT_MODELS_AS_NO_LONGER_NEEDED()
    INT i
    REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfObjects i
        IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].mn != DUMMY_MODEL_FOR_SCRIPT
            SET_MODEL_AS_NO_LONGER_NEEDED(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].mn)
        ENDIF
    ENDREPEAT
ENDPROC

PROC SET_PED_MODELS_AS_NO_LONGER_NEEDED()
    INT i
    REPEAT MAX_SURV_WAVE_DIFF i
		SET_MODEL_AS_NO_LONGER_NEEDED(g_FMMC_STRUCT.sSurvivalWaveData.mnPedModel[iWaveToUse][i])
    ENDREPEAT
ENDPROC

FUNC BOOL WARP_TO_START_POSITION(INT &iStageVar, BOOL bDoQuickWarp = FALSE)

	IF  IS_FAKE_MULTIPLAYER_MODE_SET()
		SET_ENTITY_COORDS(PLAYER_PED_ID(), serverBD.vStartPos[0])
		SET_ENTITY_HEADING(PLAYER_PED_ID(), serverBD.fStartHeading[0])
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_RESPAWNING(localPlayer)
		PRINTLN("[survival] - WARP_TO_START_POSITION - Returning FALSE as player is respawning")
		RETURN FALSE
	ENDIF
	
    SWITCH iStageVar
        CASE 0
			IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
	            IF WARP_TO_SPAWN_LOCATION(SPAWN_LOCATION_AT_SPECIFIC_COORDS, FALSE, FALSE, FALSE, bDoQuickWarp)
	                iStageVar = 1
	            ENDIF
			ENDIF
        BREAK
        
        CASE 1
            RETURN TRUE
        BREAK
    ENDSWITCH
    
    RETURN FALSE
    
ENDFUNC

PROC MAINTAIN_NEW_PLAYERS_GO_STRAIGHT_TO_SPEC_CAM_FLAG()
	
	BOOL bClear
	
	IF serverBD.sFightingParticipantsList.iNumFightingParticipants >= MAX_NUMBER_HORDE_FIGHTING_PARTICIPANTS
		IF NOT IS_BIT_SET(serverBD.iBitset, BITSET_SERVER_NEW_PLAYERS_GO_STRAIGHT_TO_SPEC_CAM)
			SET_BIT(serverBD.iBitset, BITSET_SERVER_NEW_PLAYERS_GO_STRAIGHT_TO_SPEC_CAM)
			PRINTLN("[Survival] - MAINTAIN_NEW_PLAYERS_GO_STRAIGHT_TO_SPEC_CAM_FLAG - iNumFightingParticipants >= MAX_NUMBER_HORDE_FIGHTING_PARTICIPANTS - setting BITSET_SERVER_NEW_PLAYERS_GO_STRAIGHT_TO_SPEC_CAM.")
		ENDIF
	ENDIF
	
	IF GET_SERVER_HORDE_STAGE() = eHORDESTAGE_FIGHTING
	OR GET_SERVER_HORDE_STAGE() = eHORDESTAGE_PASSED_WAVE
	OR GET_SERVER_HORDE_STAGE() = eHORDESTAGE_SPECTATING
	OR GET_SERVER_HORDE_STAGE() = eHORDESTAGE_SCTV
		IF NOT IS_BIT_SET(serverBD.iBitset, BITSET_SERVER_NEW_PLAYERS_GO_STRAIGHT_TO_SPEC_CAM)
			SET_BIT(serverBD.iBitset, BITSET_SERVER_NEW_PLAYERS_GO_STRAIGHT_TO_SPEC_CAM)
			#IF IS_DEBUG_BUILD
				IF GET_SERVER_HORDE_STAGE() = eHORDESTAGE_FIGHTING
					PRINTLN("[Survival] - MAINTAIN_NEW_PLAYERS_GO_STRAIGHT_TO_SPEC_CAM_FLAG - server in stage eHORDESTAGE_FIGHTING")
				ENDIF
				IF GET_SERVER_HORDE_STAGE() = eHORDESTAGE_PASSED_WAVE
					PRINTLN("[Survival] - MAINTAIN_NEW_PLAYERS_GO_STRAIGHT_TO_SPEC_CAM_FLAG - server in stage eHORDESTAGE_PASSED_WAVE")
				ENDIF
				IF GET_SERVER_HORDE_STAGE() = eHORDESTAGE_SPECTATING
					PRINTLN("[Survival] - MAINTAIN_NEW_PLAYERS_GO_STRAIGHT_TO_SPEC_CAM_FLAG - server in stage eHORDESTAGE_SPECTATING")
				ENDIF
				IF GET_SERVER_HORDE_STAGE() = eHORDESTAGE_SCTV
					PRINTLN("[Survival] - MAINTAIN_NEW_PLAYERS_GO_STRAIGHT_TO_SPEC_CAM_FLAG - server in stage eHORDESTAGE_SCTV")
				ENDIF
				PRINTLN("[Survival] - MAINTAIN_NEW_PLAYERS_GO_STRAIGHT_TO_SPEC_CAM_FLAG - setting BITSET_SERVER_NEW_PLAYERS_GO_STRAIGHT_TO_SPEC_CAM.")
			#ENDIF
		ENDIF
	ENDIF
	
	IF bClear
		IF IS_BIT_SET(serverBD.iBitset, BITSET_SERVER_NEW_PLAYERS_GO_STRAIGHT_TO_SPEC_CAM)
			CLEAR_BIT(serverBD.iBitset, BITSET_SERVER_NEW_PLAYERS_GO_STRAIGHT_TO_SPEC_CAM)
			PRINTLN("[Survival] - MAINTAIN_NEW_PLAYERS_GO_STRAIGHT_TO_SPEC_CAM_FLAG - bClear = TRUE - clearing BITSET_SERVER_NEW_PLAYERS_GO_STRAIGHT_TO_SPEC_CAM.")
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL IsSwoopDownAlmostFinished()
	IF IS_PLAYER_SWITCH_IN_PROGRESS() 
		IF GET_PLAYER_SWITCH_TYPE() != SWITCH_TYPE_SHORT
			IF GET_PLAYER_SWITCH_STATE() > SWITCH_STATE_OUTRO_HOLD
				RETURN(TRUE)
			ENDIF
		ENDIF
	ELSE
		RETURN(TRUE)
	ENDIF
	
	RETURN(FALSE)
	
ENDFUNC

FUNC BOOL DO_OUTFITS()
	IF IS_HALLOWEEN_MODE()
	AND iLocalPart != -1
		scrShopPedComponent componentItem
		INIT_SHOP_PED_COMPONENT(componentItem)
		INT iOutfit = playerBD[iLocalPart].sPlayerData.iPartInTeam
		MP_OUTFIT_ENUM eOutfit
		SWITCH iOutfit
		
			CASE 0
				//Green & Yellow Space Creature
				eOutfit = OUTFIT_VERSUS_HIDDEN_SPACELING_0
			BREAK
			
			CASE 1
				//Orange Space Cyclops
				eOutfit = OUTFIT_VERSUS_HIDDEN_SPACELING_1
			BREAK
			
			CASE 2
				//Light Blue Space Horror
				eOutfit = OUTFIT_VERSUS_HIDDEN_SPACELING_2
			BREAK
			
			CASE 3
				//Green Cyclops
				eOutfit = OUTFIT_VERSUS_HIDDEN_SPACELING_3
			BREAK
		
		ENDSWITCH
		
		MP_OUTFITS_APPLY_DATA sApplyData
		sApplyData.pedID = localPlayerPed
		
		sApplyData.eOutfit = eOutfit    
		sApplyData.eApplyStage = AOS_SET
		sApplyData.iPlayerIndex = -1
		SET_PED_MP_OUTFIT(sApplyData) 
		
	ENDIF
	
	RETURN TRUE	
ENDFUNC
