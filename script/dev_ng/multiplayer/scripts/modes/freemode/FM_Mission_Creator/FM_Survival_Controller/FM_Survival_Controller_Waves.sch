USING "FM_Survival_Controller_Using.sch"
USING "FM_Survival_Controller_Utility.sch"
USING "FM_Survival_Controller_HUD.sch"

PROC SET_SQUAD_SPAWN_LOCATIONS_FOR_WAVE(INT iStage)
	INT  iSquad
	FOR iSquad = 0 TO serverBD.sWaveData.iNumSquads[iStage]-1
		INT iSpawn = GET_RANDOM_INT_IN_RANGE(0, g_FMMC_STRUCT_ENTITIES.iNumberOfPeds)
		WHILE NOT IS_SQUAD_SPAWN_FREE_FOR_THIS_SUBSTAGE(iSpawn, iSquad, iStage)
			iSpawn = GET_RANDOM_INT_IN_RANGE(0, g_FMMC_STRUCT_ENTITIES.iNumberOfPeds)
		ENDWHILE
		serverBD_Enemies_S.structAiSquad[iSquad][iStage].iSquadSpawnLocation = iSpawn
		PRINTLN("SET_SQUAD_SPAWN_LOCATIONS_FOR_WAVE - Substage ", iStage, " Squad ", iSquad, " using spawn point: ", serverBD_Enemies_S.structAiSquad[iSquad][iStage].iSquadSpawnLocation)
	ENDFOR
ENDPROC

PROC SET_LAND_VEHICLE_SPAWN_LOCATION_FOR_WAVE()
	INT iVeh
	FOR iVeh = 0 TO serverBD.sWaveData.iNumLandVehicles-1
		INT iSpawn = GET_RANDOM_INT_IN_RANGE(0, g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles)
		WHILE NOT IS_VEHICLE_SPAWN_FREE(iSpawn, iVeh)
			iSpawn = GET_RANDOM_INT_IN_RANGE(0, g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles)
		ENDWHILE
		serverBD.sWaveData.iLVehSpawnLocation[iVeh] = iSpawn
		PRINTLN("SET_LAND_VEHICLE_SPAWN_LOCATION_FOR_WAVE - serverBD.sWaveData.iLVehSpawnLocation[",iVeh,"]: ", serverBD.sWaveData.iLVehSpawnLocation[iVeh])
	ENDFOR
ENDPROC

FUNC BOOL IS_SQUAD_VALID_FOR_HEAVY(INT iHeavy, INT iStage, INT iSquad)
	INT  iIndexToCheck

	iIndexToCheck = iHeavy/serverBD.sWaveData.iNumSquads[iStage]
	
	PRINTLN("IS_SQUAD_VALID_FOR_HEAVY - iIndexToCheck: ", iIndexToCheck, " serverBD_Enemies_S.sHeavyData.iHeavyPeds[",iStage,"][",iSquad,"][",iIndexToCheck,"]: ", serverBD_Enemies_S.sHeavyData.iHeavyPeds[iStage][iSquad][iIndexToCheck])
	RETURN serverBD_Enemies_S.sHeavyData.iHeavyPeds[iStage][iSquad][iIndexToCheck] = -1
ENDFUNC

PROC SET_HEAVY_PEDS_FOR_WAVE()
	INT iStage, iSquad, iPed, iHeavies
	BOOL bBreak
	INT iStageHeavies
	FOR iStage = 0 TO MAX_SURV_WAVE_DIFF-1
		iStageHeavies = g_FMMC_STRUCT.sSurvivalWaveData.iNumberOfHeavies[iWaveToUse][iStage] + serverBD.sEndlessBonuses.iHeavies[iStage]
		IF iStageHeavies != 0
			PRINTLN("SET_HEAVY_PEDS_FOR_WAVE - Setting Stage ", iStage, " heavies. Num Heavies: ", iStageHeavies)
			FOR iHeavies = 0 TO iStageHeavies-1
				PRINTLN("SET_HEAVY_PEDS_FOR_WAVE - Setting heavy ", iHeavies)
				bBreak = FALSE
				FOR iSquad = 0 TO serverBD.sWaveData.iNumSquads[iStage]-1
					IF bBreak
						BREAKLOOP
					ENDIF
					IF IS_SQUAD_VALID_FOR_HEAVY(iHeavies, iStage, iSquad)
						PRINTLN("SET_HEAVY_PEDS_FOR_WAVE - Heavy going into squad ", iSquad)
						FOR iPed = 0 TO serverBD.sWaveData.iNumPedsInSquad[iStage]-1
							IF serverBD_Enemies_S.sHeavyData.iHeavyPeds[iStage][iSquad][iPed] = -1
								PRINTLN("SET_HEAVY_PEDS_FOR_WAVE - Heavy ped index is ", iped)
								PRINTLN("SET_HEAVY_PEDS_FOR_WAVE - Stage: ",iStage," Heavy: ",iHeavies," Squad: ",iSquad," Ped: ", iPed)
								serverBD_Enemies_S.sHeavyData.iHeavyPeds[iStage][iSquad][iPed] = iHeavies
								bBreak = TRUE
								BREAKLOOP
							ENDIF
						ENDFOR
					ELSE
						PRINTLN("SET_HEAVY_PEDS_FOR_WAVE - Squad ", iSquad, " already has a heavy for this round")
						RELOOP
					ENDIF
				ENDFOR
			ENDFOR
		ENDIF
	ENDFOR
ENDPROC

PROC SPAWN_TEST_SET_SQUAD_SPAWN_LOCATIONS_FOR_WAVE(INT iStage)
	INT iSquad
	FOR iSquad = 0 TO serverBD.sWaveData.iNumSquads[iStage]-1
		serverBD_Enemies_S.structAiSquad[iSquad][iStage].iSquadSpawnLocation = iPedSpawnNumber
		iPedSpawnNumber++
		IF iPedSpawnNumber = g_FMMC_STRUCT_ENTITIES.iNumberOfPeds
			SET_LOCAL_BIT(ciLocalBool_bAllPedSpawnersDone)
			iPedSpawnNumber = 0
			PRINTLN("SPAWN_TEST_SET_SQUAD_SPAWN_LOCATIONS_FOR_WAVE - Setting ciLocalBool_bAllPedSpawnersDone and resetting iPedSpawnNumber")
		ENDIF
		PRINTLN("SET_SQUAD_SPAWN_LOCATIONS_FOR_WAVE - Substage ", iStage, " Squad ", iSquad, " using spawn point: ", serverBD_Enemies_S.structAiSquad[iSquad][iStage].iSquadSpawnLocation)
	ENDFOR
	
ENDPROC

PROC SPAWN_TEST_SET_LAND_VEHICLE_SPAWN_LOCATION_FOR_WAVE()
	INT iVeh
	FOR iVeh = 0 TO serverBD.sWaveData.iNumLandVehicles-1
		serverBD.sWaveData.iLVehSpawnLocation[iVeh] = iVehicleSpawnNumber
		iVehicleSpawnNumber++
		IF iVehicleSpawnNumber = g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles
			SET_LOCAL_BIT(ciLocalBool_bAllVehicleSpawnersDone)
			iVehicleSpawnNumber = 0
			PRINTLN("SPAWN_TEST_SET_LAND_VEHICLE_SPAWN_LOCATION_FOR_WAVE - Setting ciLocalBool_bAllVehicleSpawnersDone and resetting iVehicleSpawnNumber")
		ENDIF
		PRINTLN("SPAWN_TEST_SET_LAND_VEHICLE_SPAWN_LOCATION_FOR_WAVE - serverBD.sWaveData.iLVehSpawnLocation[",iVeh,"]: ", serverBD.sWaveData.iLVehSpawnLocation[iVeh])
	ENDFOR
	
ENDPROC

PROC POPULATE_SPAWN_TEST_WAVE_DATA()
	INT iNumVehicles
	PRINTLN("POPULATE_SPAWN_TEST_WAVE_DATA - Wave Dump ", iWaveToUse)
	serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_EASY]		= 2
	serverBD.sWaveData.iNumSquads[eWAVESTAGE_EASY]			= 5
	serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_MEDIUM]	= 0
	serverBD.sWaveData.iNumSquads[eWAVESTAGE_MEDIUM]		= 0
	serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_HARD]		= 0
	serverBD.sWaveData.iNumSquads[eWAVESTAGE_HARD]			= 0
	serverBD.sWaveData.iTotalSquads = MAX_NUM_ENEMY_SQUADS
	PRINTLN("POPULATE_SPAWN_TEST_WAVE_DATA - serverBD.sWaveData.iNumPedsInSquad[0] - ", serverBD.sWaveData.iNumPedsInSquad[0])
	PRINTLN("POPULATE_SPAWN_TEST_WAVE_DATA - serverBD.sWaveData.iNumSquads[0] - ", serverBD.sWaveData.iNumSquads[0])
	PRINTLN("POPULATE_SPAWN_TEST_WAVE_DATA - serverBD.sWaveData.iNumLandVehicles - ", serverBD.sWaveData.iNumLandVehicles)
	SPAWN_TEST_SET_SQUAD_SPAWN_LOCATIONS_FOR_WAVE(0)
	
	IF g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles != 0
	AND NOT IS_LOCAL_BIT_SET(ciLocalBool_bAllVehicleSpawnersDone)
		serverBD.sWaveData.iNumLandVehicles = 4
		FOR iNumVehicles = 0 TO serverBD.sWaveData.iNumLandVehicles-1
			serverBD_Enemies_V.sSurvivalLandVehicle[iNumVehicles].iNumPeds 		= 1
			serverBD_Enemies_V.sSurvivalLandVehicle[iNumVehicles].eModel 		= SANDKING
			IF GET_VEHICLE_MODEL_NUMBER_OF_SEATS(serverBD_Enemies_V.sSurvivalLandVehicle[iNumVehicles].eModel) < serverBD_Enemies_V.sSurvivalLandVehicle[iNumVehicles].iNumPeds
				serverBD_Enemies_V.sSurvivalLandVehicle[iNumVehicles].iNumPeds = GET_VEHICLE_MODEL_NUMBER_OF_SEATS(serverBD_Enemies_V.sSurvivalLandVehicle[iNumVehicles].eModel)
				PRINTLN("[Survival] - [Populate Enemy Data] - serverBD_Enemies_V.sSurvivalLandVehicle[",iNumVehicles,"].iNumPeds reduced to ", serverBD_Enemies_V.sSurvivalLandVehicle[iNumVehicles].iNumPeds)
			ENDIF
			PRINTLN("[Survival] - [Populate Enemy Data] - serverBD_Enemies_V.sSurvivalLandVehicle[", iNumVehicles, "].iNumPeds = ", serverBD_Enemies_V.sSurvivalLandVehicle[iNumVehicles].iNumPeds)
		ENDFOR
		SPAWN_TEST_SET_LAND_VEHICLE_SPAWN_LOCATION_FOR_WAVE()
	ELSE
		serverBD.sWaveData.iNumLandVehicles = 0
		SET_LOCAL_BIT(ciLocalBool_bAllVehicleSpawnersDone)
	ENDIF
	IF serverBD.sWaveData.iNumRequiredKills    = 0
		serverBD.sWaveData.iNumRequiredSubstageKills[eWAVESTAGE_EASY] 		= serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_EASY] * serverBD.sWaveData.iNumSquads[eWAVESTAGE_EASY]
		PRINTLN("POPULATE_SPAWN_TEST_WAVE_DATA - [Populate Enemy Data] - iNumRequiredSubstageKills[eWAVESTAGE_EASY] = ", serverBD.sWaveData.iNumRequiredSubstageKills[eWAVESTAGE_EASY])
		
    	serverBD.sWaveData.iNumRequiredKills    							= ( serverBD.sWaveData.iNumRequiredSubstageKills[eWAVESTAGE_EASY]   +
                                                                    			serverBD.sWaveData.iNumRequiredSubstageKills[eWAVESTAGE_MEDIUM] +
                                                                    			serverBD.sWaveData.iNumRequiredSubstageKills[eWAVESTAGE_HARD] 		)
		PRINTLN("POPULATE_SPAWN_TEST_WAVE_DATA - [Populate Enemy Data] - serverBD.sWaveData.iNumRequiredKills = ", serverBD.sWaveData.iNumRequiredKills)
		
		INT i
		
		REPEAT MAX_NUM_LAND_VEHICLES i
			IF serverBD_Enemies_V.sSurvivalLandVehicle[i].iNumPeds > 0
				serverBD.sWaveData.iNumRequiredKills 							+= serverBD_Enemies_V.sSurvivalLandVehicle[i].iNumPeds
				PRINTLN("POPULATE_SPAWN_TEST_WAVE_DATA - [Populate Enemy Data] - Adding Vehicle ",i," enemies to required kills. serverBD.sWaveData.iNumRequiredKills = ", serverBD.sWaveData.iNumRequiredKills)
			ENDIF
		ENDREPEAT
		
		FOR i = 0 TO MAX_SURV_WAVE_DIFF-1
			IF serverBD.sWaveData.iNumRequiredSubstageKills[i] > 0
				serverBD.sWaveData.eHighestSubstage = INT_TO_ENUM(eWAVE_STAGE, i)
			ENDIF
		ENDFOR
		IF serverBD.sWaveData.iNumLandVehicles > 0
			IF serverBD.sWaveData.eHighestSubstage = eWAVESTAGE_EASY
				serverBD.sWaveData.eHighestSubstage = eWAVESTAGE_MEDIUM
			ENDIF
		ENDIF
		PRINTLN("POPULATE_SPAWN_TEST_WAVE_DATA - [Populate Enemy Data] - serverBD.sWaveData.eHighestSubstage: ", GET_SUB_STAGE_NAME(serverBD.sWaveData.eHighestSubstage))
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Sets up the wave data
PROC POPULATE_WAVE_SQUAD_DATA()
    
	INT iNumHelis
	INT iNumberOfParticipantsForWave = GET_NUM_COMPETITIVE_PARTICIPANTS_THIS_WAVE()
	INT iNumVehicles
	INT iStage
	
	serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_EASY]		= g_FMMC_STRUCT.sSurvivalWaveData.iPedsPerSquad[iWaveToUse][eWAVESTAGE_EASY]
    serverBD.sWaveData.iNumSquads[eWAVESTAGE_EASY]			= g_FMMC_STRUCT.sSurvivalWaveData.iNumberOfSquads[iWaveToUse][eWAVESTAGE_EASY]
	serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_MEDIUM]	= g_FMMC_STRUCT.sSurvivalWaveData.iPedsPerSquad[iWaveToUse][eWAVESTAGE_MEDIUM]
    serverBD.sWaveData.iNumSquads[eWAVESTAGE_MEDIUM]		= g_FMMC_STRUCT.sSurvivalWaveData.iNumberOfSquads[iWaveToUse][eWAVESTAGE_MEDIUM]
	serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_HARD]		= g_FMMC_STRUCT.sSurvivalWaveData.iPedsPerSquad[iWaveToUse][eWAVESTAGE_HARD]
    serverBD.sWaveData.iNumSquads[eWAVESTAGE_HARD]			= g_FMMC_STRUCT.sSurvivalWaveData.iNumberOfSquads[iWaveToUse][eWAVESTAGE_HARD]
	serverBD.sWaveData.iNumLandVehicles						= g_FMMC_STRUCT.sSurvivalWaveData.iNumVehicles[iWaveToUse]
	serverBD_Enemies_S.sHeavyData.iNumHeavies[eWAVESTAGE_EASY]		= g_FMMC_STRUCT.sSurvivalWaveData.iNumberOfHeavies[iWaveToUse][eWAVESTAGE_EASY]
	serverBD_Enemies_S.sHeavyData.iNumHeavies[eWAVESTAGE_MEDIUM]		= g_FMMC_STRUCT.sSurvivalWaveData.iNumberOfHeavies[iWaveToUse][eWAVESTAGE_MEDIUM]
	serverBD_Enemies_S.sHeavyData.iNumHeavies[eWAVESTAGE_HARD]		= g_FMMC_STRUCT.sSurvivalWaveData.iNumberOfHeavies[iWaveToUse][eWAVESTAGE_HARD]
	
	serverBD.sWaveData.iTotalSquads = MAX_NUM_ENEMY_SQUADS
	
	IF serverBD.sWaveData.iNumLandVehicles > 0
		FOR iNumVehicles = 0 TO serverBD.sWaveData.iNumLandVehicles-1
			serverBD_Enemies_V.sSurvivalLandVehicle[iNumVehicles].iNumPeds = g_FMMC_STRUCT.sSurvivalWaveData.iNumVehPeds[iWaveToUse][iNumVehicles]
			serverBD_Enemies_V.sSurvivalLandVehicle[iNumVehicles].iNumPeds = GET_SAFE_NUMBER_OF_VEHICLE_PEDS(g_FMMC_STRUCT.sSurvivalWaveData.mnVeh[iWaveToUse][iNumVehicles], iNumVehicles)
			PRINTLN("[Survival] - [Populate Enemy Data] - serverBD_Enemies_V.sSurvivalLandVehicle[", iNumVehicles, "].iNumPeds = ", serverBD_Enemies_V.sSurvivalLandVehicle[iNumVehicles].iNumPeds)
			serverBD_Enemies_V.sSurvivalLandVehicle[iNumVehicles].eModel 		= g_FMMC_STRUCT.sSurvivalWaveData.mnVeh[iWaveToUse][iNumVehicles]
		ENDFOR
	ENDIF
	
	iNumHelis = g_FMMC_STRUCT.sSurvivalWaveData.iNumberOfHelis[iWaveToUse]
	
	IF iNumberOfParticipantsForWave > 2
	#IF IS_DEBUG_BUILD OR bSimulateScaled #ENDIF
		serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_EASY]		= CLAMP_INT(ROUND(serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_EASY]	* 1.5), 1, MAX_SURV_PEDS_IN_SQUAD)
		serverBD.sWaveData.iNumSquads[eWAVESTAGE_EASY]			= CLAMP_INT(ROUND(serverBD.sWaveData.iNumSquads[eWAVESTAGE_EASY]		* 1.5), 1, MAX_NUM_ENEMY_SQUADS)		
		serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_MEDIUM]	= CLAMP_INT(ROUND(serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_MEDIUM]	* 1.5), 1 ,MAX_SURV_PEDS_IN_SQUAD)		
		serverBD.sWaveData.iNumSquads[eWAVESTAGE_MEDIUM]		= CLAMP_INT(ROUND(serverBD.sWaveData.iNumSquads[eWAVESTAGE_MEDIUM]		* 1.5), 0, MAX_NUM_ENEMY_SQUADS)		
		serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_HARD]		= CLAMP_INT(ROUND(serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_HARD]	* 1.5), 1, MAX_SURV_PEDS_IN_SQUAD)		
		serverBD.sWaveData.iNumSquads[eWAVESTAGE_HARD]			= CLAMP_INT(ROUND(serverBD.sWaveData.iNumSquads[eWAVESTAGE_HARD]		* 1.5), 0, MAX_NUM_ENEMY_SQUADS)
		IF serverBD.sWaveData.iNumLandVehicles > 0
			serverBD.sWaveData.iNumLandVehicles					= CLAMP_INT(serverBD.sWaveData.iNumLandVehicles + 1, 0, MAX_SURV_LAND_VEH)
			PRINTLN("[Survival] - [Populate Enemy Data] - serverBD.sWaveData.iNumLandVehicles Increased to ", serverBD.sWaveData.iNumLandVehicles)
			INT iVeh
			FOR iVeh = 0 TO serverBD.sWaveData.iNumLandVehicles-1
				IF serverBD_Enemies_V.sSurvivalLandVehicle[iVeh].iNumPeds <= 0
					serverBD_Enemies_V.sSurvivalLandVehicle[iVeh].iNumPeds = serverBD_Enemies_V.sSurvivalLandVehicle[0].iNumPeds
					PRINTLN("[Survival] - [Populate Enemy Data] - serverBD_Enemies_V.sSurvivalLandVehicle[",iVeh,"].iNumPeds set to ", serverBD_Enemies_V.sSurvivalLandVehicle[iVeh].iNumPeds)
				ENDIF
				PRINTLN("[Survival] - [Populate Enemy Data] - serverBD_Enemies_V.sSurvivalLandVehicle[", iVeh, "].iNumPeds = ", serverBD_Enemies_V.sSurvivalLandVehicle[iVeh].iNumPeds)
			ENDFOR
		ENDIF
		PRINTLN("[Survival] - [Populate Enemy Data] - More than 2 players, multiplying values by 1.5")
		PRINTLN("[survival] - [Populate Enemy Data] - serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_EASY] 	= ", serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_EASY], " 	Creator Set: ", g_FMMC_STRUCT.sSurvivalWaveData.iPedsPerSquad[iWaveToUse][eWAVESTAGE_EASY])
		PRINTLN("[survival] - [Populate Enemy Data] - serverBD.sWaveData.iNumSquads[eWAVESTAGE_EASY]		= ", serverBD.sWaveData.iNumSquads[eWAVESTAGE_EASY], " 			Creator Set: ", g_FMMC_STRUCT.sSurvivalWaveData.iNumberOfSquads[iWaveToUse][eWAVESTAGE_EASY])
		PRINTLN("[survival] - [Populate Enemy Data] - serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_MEDIUM] = ", serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_MEDIUM], " 	Creator Set: ", g_FMMC_STRUCT.sSurvivalWaveData.iPedsPerSquad[iWaveToUse][eWAVESTAGE_MEDIUM])
		PRINTLN("[survival] - [Populate Enemy Data] - serverBD.sWaveData.iNumSquads[eWAVESTAGE_MEDIUM] 		= ", serverBD.sWaveData.iNumSquads[eWAVESTAGE_MEDIUM], " 		Creator Set: ", g_FMMC_STRUCT.sSurvivalWaveData.iNumberOfSquads[iWaveToUse][eWAVESTAGE_MEDIUM])
		PRINTLN("[survival] - [Populate Enemy Data] - serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_HARD] 	= ", serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_HARD], " 	Creator Set: ", g_FMMC_STRUCT.sSurvivalWaveData.iPedsPerSquad[iWaveToUse][eWAVESTAGE_HARD])
		PRINTLN("[survival] - [Populate Enemy Data] - serverBD.sWaveData.iNumSquads[eWAVESTAGE_HARD] 		= ", serverBD.sWaveData.iNumSquads[eWAVESTAGE_HARD], " 			Creator Set: ", g_FMMC_STRUCT.sSurvivalWaveData.iNumberOfSquads[iWaveToUse][eWAVESTAGE_HARD])
	ENDIF
	
	IF IS_THIS_ENDLESS_WAVES()
	AND HAVE_ENDLESS_WAVES_STARTED()
		PRINTLN("[Survival] - [Populate Enemy Data] - Endless Waves Bonus Enemies")
		IF (serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_EASY] + serverBD.sEndlessBonuses.iEnemiesInSquad[eWAVESTAGE_EASY]) < MAX_SURV_PEDS_IN_SQUAD
			serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_EASY] += serverBD.sEndlessBonuses.iEnemiesInSquad[eWAVESTAGE_EASY]
			PRINTLN("[Survival] - [Populate Enemy Data] - Adding bonus peds to squad - eWAVESTAGE_EASY - ", serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_EASY])
		ENDIF
		IF (serverBD.sWaveData.iNumSquads[eWAVESTAGE_EASY] + serverBD.sEndlessBonuses.iSquads[eWAVESTAGE_EASY]) < MAX_NUM_ENEMY_SQUADS
			serverBD.sWaveData.iNumSquads[eWAVESTAGE_EASY] += serverBD.sEndlessBonuses.iSquads[eWAVESTAGE_EASY]
			PRINTLN("[Survival] - [Populate Enemy Data] - Adding bonus squad to wave - eWAVESTAGE_EASY - ", serverBD.sWaveData.iNumSquads[eWAVESTAGE_EASY])
		ENDIF
		
		IF (serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_MEDIUM] + serverBD.sEndlessBonuses.iEnemiesInSquad[eWAVESTAGE_MEDIUM]) < MAX_SURV_PEDS_IN_SQUAD
			serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_MEDIUM] += serverBD.sEndlessBonuses.iEnemiesInSquad[eWAVESTAGE_MEDIUM]
			PRINTLN("[Survival] - [Populate Enemy Data] - Adding bonus peds to squad - eWAVESTAGE_MEDIUM - ", serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_MEDIUM])
		ENDIF
		IF (serverBD.sWaveData.iNumSquads[eWAVESTAGE_MEDIUM] + serverBD.sEndlessBonuses.iSquads[eWAVESTAGE_MEDIUM]) < MAX_NUM_ENEMY_SQUADS
			serverBD.sWaveData.iNumSquads[eWAVESTAGE_MEDIUM] += serverBD.sEndlessBonuses.iSquads[eWAVESTAGE_MEDIUM]
			PRINTLN("[Survival] - [Populate Enemy Data] - Adding bonus squad to wave - eWAVESTAGE_MEDIUM - ", serverBD.sWaveData.iNumSquads[eWAVESTAGE_MEDIUM])
		ENDIF
		
		IF (serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_HARD] + serverBD.sEndlessBonuses.iEnemiesInSquad[eWAVESTAGE_HARD]) < MAX_SURV_PEDS_IN_SQUAD
			serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_HARD] += serverBD.sEndlessBonuses.iEnemiesInSquad[eWAVESTAGE_HARD]
			PRINTLN("[Survival] - [Populate Enemy Data] - Adding bonus peds to squad - eWAVESTAGE_HARD - ", serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_HARD])
		ENDIF
		IF (serverBD.sWaveData.iNumSquads[eWAVESTAGE_HARD] + serverBD.sEndlessBonuses.iSquads[eWAVESTAGE_HARD]) < MAX_NUM_ENEMY_SQUADS
			serverBD.sWaveData.iNumSquads[eWAVESTAGE_HARD] += serverBD.sEndlessBonuses.iSquads[eWAVESTAGE_HARD]
			PRINTLN("[Survival] - [Populate Enemy Data] - Adding bonus squad to wave - eWAVESTAGE_HARD - ", serverBD.sWaveData.iNumSquads[eWAVESTAGE_HARD])
		ENDIF
	ENDIF
    
	#IF IS_DEBUG_BUILD
		INT iDebug
		TEXT_LABEL_63 tl63Debug
		REPEAT COUNT_OF(eWAVE_STAGE) iDebug
			IF serverBD.sWaveData.iNumPedsInSquad[iDebug] > MAX_SURV_PEDS_IN_SQUAD
				tl63Debug = "iNumPedsInSquad > MAX_SURV_PEDS_IN_SQUAD. Wave = "
				tl63Debug += serverBD.sWaveData.iWaveCount
				tl63Debug += ", parts = "
				tl63Debug += iNumberOfParticipantsForWave
				NET_SCRIPT_ASSERT(tl63Debug)
			ENDIF
			IF serverBD.sWaveData.iNumSquads[iDebug] > MAX_NUM_ENEMY_SQUADS
				tl63Debug = "iNumSquads > MAX_NUM_ENEMY_SQUADS. Wave = "
				tl63Debug += serverBD.sWaveData.iWaveCount
				tl63Debug += ", parts = "
				tl63Debug += iNumberOfParticipantsForWave
				NET_SCRIPT_ASSERT(tl63Debug)
			ENDIF
		ENDREPEAT
	#ENDIF
	
	IF serverBD.sWaveData.iNumSquads[eWAVESTAGE_EASY] > 0
		SET_SQUAD_SPAWN_LOCATIONS_FOR_WAVE(ENUM_TO_INT(eWAVESTAGE_EASY))
	ENDIF
	IF serverBD.sWaveData.iNumSquads[eWAVESTAGE_MEDIUM] > 0
		SET_SQUAD_SPAWN_LOCATIONS_FOR_WAVE(ENUM_TO_INT(eWAVESTAGE_MEDIUM))
	ENDIF
	IF serverBD.sWaveData.iNumSquads[eWAVESTAGE_HARD] > 0
		SET_SQUAD_SPAWN_LOCATIONS_FOR_WAVE(ENUM_TO_INT(eWAVESTAGE_HARD))
	ENDIF
	
	IF serverBD.sWaveData.iNumLandVehicles > 0
		SET_LAND_VEHICLE_SPAWN_LOCATION_FOR_WAVE()
	ENDIF
	
	SET_HEAVY_PEDS_FOR_WAVE()
	
	IF serverBD.sWaveData.iNumRequiredKills    = 0
		PRINTLN("[Survival] - [Populate Enemy Data] - WAVE DUMP Wave ", serverBD.sWaveData.iWaveCount," Data")
		serverBD.sWaveData.iNumRequiredSubstageKills[eWAVESTAGE_EASY] 		= serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_EASY] * serverBD.sWaveData.iNumSquads[eWAVESTAGE_EASY]
		PRINTLN("[Survival] - [Populate Enemy Data] - iNumRequiredSubstageKills[eWAVESTAGE_EASY] = ", serverBD.sWaveData.iNumRequiredSubstageKills[eWAVESTAGE_EASY])
		
		serverBD.sWaveData.iNumRequiredSubstageKills[eWAVESTAGE_MEDIUM] 	= serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_MEDIUM] * serverBD.sWaveData.iNumSquads[eWAVESTAGE_MEDIUM]
		PRINTLN("[Survival] - [Populate Enemy Data] - iNumRequiredSubstageKills[eWAVESTAGE_MEDIUM] = ", serverBD.sWaveData.iNumRequiredSubstageKills[eWAVESTAGE_MEDIUM])

		serverBD.sWaveData.iNumRequiredSubstageKills[eWAVESTAGE_HARD] 		= serverBD.sWaveData.iNumPedsInSquad[eWAVESTAGE_HARD] * serverBD.sWaveData.iNumSquads[eWAVESTAGE_HARD]
		PRINTLN("[Survival] - [Populate Enemy Data] - iNumRequiredSubstageKills[eWAVESTAGE_HARD] = ", serverBD.sWaveData.iNumRequiredSubstageKills[eWAVESTAGE_HARD])
		
    	serverBD.sWaveData.iNumRequiredKills    							= ( serverBD.sWaveData.iNumRequiredSubstageKills[eWAVESTAGE_EASY]   +
                                                                    			serverBD.sWaveData.iNumRequiredSubstageKills[eWAVESTAGE_MEDIUM] +
                                                                    			serverBD.sWaveData.iNumRequiredSubstageKills[eWAVESTAGE_HARD] 		)
		PRINTLN("[Survival] - [Populate Enemy Data] - serverBD.sWaveData.iNumRequiredKills = ", serverBD.sWaveData.iNumRequiredKills)
		
		serverBD.sWaveData.iNumRequiredKills 								+= iNumHelis
		PRINTLN("[Survival] - [Populate Enemy Data] - iNumHelis = ", iNumHelis)
		PRINTLN("[Survival] - [Populate Enemy Data] - serverBD.sWaveData.iNumRequiredKills = ", serverBD.sWaveData.iNumRequiredKills)
		PRINTLN("[Survival] - [Populate Enemy Data] - serverBD.sWaveData.iTotalSquads = ", serverBD.sWaveData.iTotalSquads)
		
		INT i
		
		REPEAT MAX_NUM_LAND_VEHICLES i
			IF serverBD_Enemies_V.sSurvivalLandVehicle[i].iNumPeds > 0
				serverBD.sWaveData.iNumRequiredKills 							+= serverBD_Enemies_V.sSurvivalLandVehicle[i].iNumPeds
				PRINTLN("[Survival] - [Populate Enemy Data] - Adding Vehicle ",i," enemies to required kills. serverBD.sWaveData.iNumRequiredKills = ", serverBD.sWaveData.iNumRequiredKills)
			ENDIF
		ENDREPEAT
		
		FOR iStage = 0 TO MAX_SURV_WAVE_DIFF-1
			IF serverBD.sWaveData.iNumRequiredSubstageKills[iStage] > 0
				serverBD.sWaveData.eHighestSubstage = INT_TO_ENUM(eWAVE_STAGE, iStage)
			ENDIF
		ENDFOR
		IF serverBD.sWaveData.iNumLandVehicles > 0
		OR iNumHelis > 0
			IF serverBD.sWaveData.eHighestSubstage = eWAVESTAGE_EASY
				serverBD.sWaveData.eHighestSubstage = eWAVESTAGE_MEDIUM
			ENDIF
		ENDIF
		PRINTLN("[Survival] - [Populate Enemy Data] - serverBD.sWaveData.eHighestSubstage: ", GET_SUB_STAGE_NAME(serverBD.sWaveData.eHighestSubstage))
	ENDIF
    
ENDPROC

FUNC INT GET_ACTIVATE_NEXT_SUB_STAGE_TIME()
	
	IF serverBD.sWaveData.iWaveCount <= 4
		RETURN 5000
	ELIF serverBD.sWaveData.iWaveCount <= 6
		RETURN 7500
	ENDIF
		
	RETURN 10000
	
ENDFUNC

FUNC INT GET_START_NEXT_SUB_STAGE_TIMER_NUM_ENEMIES()
	
	RETURN g_FMMC_STRUCT.sSurvivalWaveData.iKillsToProgressStage[iWaveToUse]
	
ENDFUNC

FUNC INT GET_NUM_ENEMIES_IN_SQUAD_FOR_WAVE()
    RETURN serverBD.sWaveData.iNumPedsInSquad
ENDFUNC

FUNC INT GET_NUM_SQUADS_FOR_WAVE()
    RETURN serverBD.sWaveData.iNumSquads
ENDFUNC

FUNC INT GET_COMPLETE_WAVE_STAGE_KILL_LIMIT(eWAVE_STAGE eWaveStage)
    RETURN serverBD.sWaveData.iNumRequiredSubstageKills[ENUM_TO_INT(eWaveStage)]
ENDFUNC

PROC RESET_TRIGGER_NEXT_WAVE_SUB_STAGE_TIMER()
	SCRIPT_TIMER stTemp
	
	CLEAR_BIT(serverBD.iBitset, BITSET_SERVER_RUN_LAST_STAGE_TIMER)
    RESET_NET_TIMER(serverBD.stNextSubStageTimer)
    serverBD.stNextSubStageTimer = stTemp
ENDPROC

FUNC BOOL HAVE_ENOUGH_ENEMIES_BEEN_CREATED_TO_START_SUB_STAGE_LAST_ENEMIES_CHECK()
	
	INT iTrigger
	
	SWITCH serverBD.sWaveData.eStage
		
		CASE eWAVESTAGE_EASY
			iTrigger = serverBD.sWaveData.iNumRequiredSubstageKills[eWAVESTAGE_EASY]
		BREAK
		
		CASE eWAVESTAGE_MEDIUM
			iTrigger = serverBD.sWaveData.iNumRequiredSubstageKills[eWAVESTAGE_EASY] + serverBD.sWaveData.iNumRequiredSubstageKills[eWAVESTAGE_MEDIUM]
		BREAK
		
		CASE eWAVESTAGE_HARD
			iTrigger = serverBD.sWaveData.iNumRequiredSubstageKills[eWAVESTAGE_EASY] + serverBD.sWaveData.iNumRequiredSubstageKills[eWAVESTAGE_MEDIUM] + serverBD.sWaveData.iNumRequiredSubstageKills[eWAVESTAGE_HARD]
		BREAK
		
	ENDSWITCH
	
	RETURN (serverBD.iNumCreatedEnemies >= iTrigger)
	
ENDFUNC

FUNC BOOL MAINTAIN_TRIGGER_NEXT_WAVE_SUB_STAGE_TIMER()
	
	IF IS_BIT_SET(serverBD.iBitset, BITSET_SERVER_RUN_LAST_STAGE_TIMER)
		IF serverBD.iNumAliveSquadEnemies <= GET_START_NEXT_SUB_STAGE_TIMER_NUM_ENEMIES()
			// Run spawn overlap peds timer.
            IF NOT HAS_NET_TIMER_STARTED(serverBD.stNextSubStageTimer)
                START_NET_TIMER(serverBD.stNextSubStageTimer)
            ELSE
           	 	// If the timer has expired return true.
                IF HAS_NET_TIMER_EXPIRED(serverBD.stNextSubStageTimer, GET_ACTIVATE_NEXT_SUB_STAGE_TIME())
					PRINTLN("MAINTAIN_TRIGGER_NEXT_WAVE_SUB_STAGE_TIMER - RETURNING TRUE")
					PRINTLN("MAINTAIN_TRIGGER_NEXT_WAVE_SUB_STAGE_TIMER - serverBD.iNumAliveSquadEnemies: ", serverBD.iNumAliveSquadEnemies, " GET_START_NEXT_SUB_STAGE_TIMER_NUM_ENEMIES: ", GET_START_NEXT_SUB_STAGE_TIMER_NUM_ENEMIES())
                    RETURN TRUE
                ENDIF
            ENDIF
		ENDIF
		
	ELSE
		// If we have met the consitions to run the overlap peds timer, set flag.
        IF HAVE_ENOUGH_ENEMIES_BEEN_CREATED_TO_START_SUB_STAGE_LAST_ENEMIES_CHECK()
			PRINTLN("MAINTAIN_TRIGGER_NEXT_WAVE_SUB_STAGE_TIMER - Setting serverBD.iBitset, BITSET_SERVER_RUN_LAST_STAGE_TIMER")
			SET_BIT(serverBD.iBitset, BITSET_SERVER_RUN_LAST_STAGE_TIMER)
        ENDIF
	
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC ACTIVATE_WAVE_SUBSTAGE_SQUADS()
	
	INT i, j
	INT iStage = ENUM_TO_INT(serverBD.sWaveData.eStage)
	DEBUG_PRINTCALLSTACK()
	FOR i = 0 TO serverBD.sWaveData.iNumSquads[iStage]-1
		serverBD_Enemies_S.structAiSquad[i][iStage].eState = eSQUADSTATE_ACTIVE
		PRINTLN("ACTIVATE_WAVE_SUBSTAGE_SQUADS - Activating squad ", i)
		FOR j = 0 TO serverBD.sWaveData.iNumPedsInSquad[iStage]-1
			PRINTLN("ACTIVATE_WAVE_SUBSTAGE_SQUADS - Setting ped ", j, " to spawning in squad ", i)
			serverBD_Enemies_S.structAiSquad[i][iStage].sPed[j].eState = eENEMYSTATE_SPAWNING
		ENDFOR
	ENDFOR
	
ENDPROC

FUNC BOOL ARE_ANY_ENEMY_PEDS_ALIVE()
	
	IF serverBD.iNumAliveSquadEnemies > 0
		RETURN TRUE
	ENDIF
	
	IF serverBD.iNumAliveHelis > 0
		RETURN TRUE
	ENDIF
	
	IF serverBd.iNumAliveLandVehiclePeds > 0
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL ARE_WE_ONE_OFF_OF_SUB_STAGE_KILL_LIMIT_WITH_NO_PEDS_TO_KILL(eWAVE_STAGE eStage)
	
	IF NOT ARE_ANY_ENEMY_PEDS_ALIVE()
		IF serverBD.iKillsThisWaveSubStage[eStage] >= (serverBD.sWaveData.iNumRequiredSubstageKills[eStage] - 1)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC MAINTAIN_WAVE_PROGRESSION()
    BOOL bMoveOnSubStage, bNoPeds
    SWITCH serverBD.sWaveData.eStage
        
        CASE eWAVESTAGE_EASY
            
			SWITCH serverBD.sWaveData.iMaintainSquadsStage
			
				CASE 0
					serverBD.iKillsThisWaveSubStage[serverBd.sWaveData.eStage] = 0
					serverBD.iNumKillsThisSubStageFromEvents[serverBd.sWaveData.eStage] = 0
					RESET_TRIGGER_NEXT_WAVE_SUB_STAGE_TIMER()
					IF IS_FAKE_MULTIPLAYER_MODE_SET()
					AND g_iTestType = 1
						POPULATE_SPAWN_TEST_WAVE_DATA()
					ELSE
						POPULATE_WAVE_SQUAD_DATA()
					ENDIF
					ACTIVATE_WAVE_SUBSTAGE_SQUADS()
					REINIT_NET_TIMER(serverBD.tdSubStageLength[serverBd.sWaveData.eStage])
					serverBD.sWaveData.iMaintainSquadsStage++
				BREAK
				
				CASE 1
					
					#IF IS_DEBUG_BUILD
						IF bWaveStageDataWidgets
							PRINTLN("[Survival] - [Wave Progression] - PROCESS_WAVE_STAGE_DATA")
							PRINTLN("[Survival] - [Wave Progression] - iKillsThisWaveSubStage 								= ", serverBD.iKillsThisWaveSubStage[serverBd.sWaveData.eStage])
							PRINTLN("[Survival] - [Wave Progression] - GET_COMPLETE_WAVE_STAGE_KILL_LIMIT(eWAVESTAGE_EASY) 	= ", GET_COMPLETE_WAVE_STAGE_KILL_LIMIT(eWAVESTAGE_EASY))
							IF serverBD.iKillsThisWaveSubStage[serverBd.sWaveData.eStage] >= GET_COMPLETE_WAVE_STAGE_KILL_LIMIT(eWAVESTAGE_EASY)
							PRINTLN("[Survival] - [Wave Progression] - serverBD.iKillsThisWaveSubStage >= GET_COMPLETE_WAVE_STAGE_KILL_LIMIT")
							ELSE
							PRINTLN("[Survival] - [Wave Progression] - serverBD.iKillsThisWaveSubStage < GET_COMPLETE_WAVE_STAGE_KILL_LIMIT")
							ENDIF
						ENDIF
					#ENDIF
					
					IF HAS_ENOUGH_TIME_PASSED_ON_WAVE()
					AND HAVE_ALL_PEDS_HAVE_BEEN_CREATED_THIS_STAGE()
						IF serverBD.iKillsThisWaveSubStage[serverBd.sWaveData.eStage] >= serverBD.sWaveData.iNumRequiredSubstageKills[eWAVESTAGE_EASY]
							bMoveOnSubStage = TRUE
							PRINTLN("[Survival] - [Wave Progression] - eWAVESTAGE_EASY - Moving substage on because we have reached kill limit ", serverBD.iKillsThisWaveSubStage[serverBd.sWaveData.eStage], " >= ", serverBD.sWaveData.iNumRequiredSubstageKills[eWAVESTAGE_EASY])
						ELIF serverBD.iNumKillsThisSubStageFromEvents[serverBd.sWaveData.eStage] >= serverBD.sWaveData.iNumRequiredSubstageKills[eWAVESTAGE_EASY]
							bMoveOnSubStage = TRUE
							PRINTLN("[Survival] - [Wave Progression] - eWAVESTAGE_EASY - Moving substage on because event kills were reached ", serverBD.iNumKillsThisSubStageFromEvents[serverBd.sWaveData.eStage], " >= ", serverBD.sWaveData.iNumRequiredSubstageKills[eWAVESTAGE_EASY])
						ELIF ARE_WE_ONE_OFF_OF_SUB_STAGE_KILL_LIMIT_WITH_NO_PEDS_TO_KILL(eWAVESTAGE_EASY)
							bMoveOnSubStage = TRUE
							bNoPeds = TRUE
							PRINTLN("[Survival] - [Wave Progression] - eWAVESTAGE_EASY - Moving substage on because One off no peds left")
						ELIF MAINTAIN_TRIGGER_NEXT_WAVE_SUB_STAGE_TIMER()
							bMoveOnSubStage = TRUE
							PRINTLN("[Survival] - [Wave Progression] - eWAVESTAGE_EASY - Moving substage on because of timer expiring")
						ENDIF
					ENDIF
					
					IF bMoveOnSubStage
						PRINTLN("[Survival] - [Wave Progression] - setting serverBD.sWaveData.eStage = eWAVESTAGE_MEDIUM")
						IF serverBD.sWaveData.iNumSquads[eWAVESTAGE_MEDIUM] > 0
						OR serverBD.sWaveData.iNumSquads[eWAVESTAGE_HARD] > 0
						OR serverBD.sWaveData.iNumLandVehicles > 0
						OR g_FMMC_STRUCT.sSurvivalWaveData.iNumberOfHelis[iWaveToUse] > 0
							IF serverBD.sWaveData.iNumSquads[eWAVESTAGE_MEDIUM] > 0
							OR serverBD.sWaveData.iNumLandVehicles > 0
							OR g_FMMC_STRUCT.sSurvivalWaveData.iNumberOfHelis[iWaveToUse] > 0
				       			SET_WAVE_SUB_STAGE(eWAVESTAGE_MEDIUM)
							ELIF serverBD.sWaveData.iNumSquads[eWAVESTAGE_HARD] > 0
								SET_WAVE_SUB_STAGE(eWAVESTAGE_HARD)
							ENDIF
							serverBD.sWaveData.iMaintainSquadsStage = 0
							IF bNoPeds
	                			SERVER_INCREMENT_KILLS_THIS_WAVE()
								SERVER_INCREMENT_TOTAL_KILLS()
	                			SERVER_INCREMENT_KILLS_THIS_SUB_STAGE()
							ENDIF
						ENDIF
			      	ENDIF
					
				BREAK
			
			ENDSWITCH
			
        BREAK
        
        CASE eWAVESTAGE_MEDIUM
            
            SWITCH serverBD.sWaveData.iMaintainSquadsStage
                
                CASE 0
					serverBD.iKillsThisWaveSubStage[serverBd.sWaveData.eStage] = 0
					serverBD.iNumKillsThisSubStageFromEvents[serverBd.sWaveData.eStage] = 0
					RESET_TRIGGER_NEXT_WAVE_SUB_STAGE_TIMER()
					ACTIVATE_WAVE_SUBSTAGE_SQUADS()
					REINIT_NET_TIMER(serverBD.tdSubStageLength[serverBd.sWaveData.eStage])
                    serverBD.sWaveData.iMaintainSquadsStage++
					PRINTLN("[Survival] - [Wave Progression] eWAVESTAGE_MEDIUM - Moving into next case")
                BREAK
                
                CASE 1
					
					#IF IS_DEBUG_BUILD
						IF bWaveStageDataWidgets
							PRINTLN("[Survival] - [Wave Progression] - PROCESS_WAVE_STAGE_DATA")
							PRINTLN("[Survival] - [Wave Progression] - iKillsThisWaveSubStage 								= ", serverBD.iKillsThisWaveSubStage[serverBd.sWaveData.eStage])
							PRINTLN("[Survival] - [Wave Progression] - GET_COMPLETE_WAVE_STAGE_KILL_LIMIT(eWAVESTAGE_MEDIUM) 	= ", GET_COMPLETE_WAVE_STAGE_KILL_LIMIT(eWAVESTAGE_MEDIUM))
							IF serverBD.iKillsThisWaveSubStage[serverBd.sWaveData.eStage] >= GET_COMPLETE_WAVE_STAGE_KILL_LIMIT(eWAVESTAGE_EASY)
							PRINTLN("[Survival] - [Wave Progression] - serverBD.iKillsThisWaveSubStage >= GET_COMPLETE_WAVE_STAGE_KILL_LIMIT")
							ELSE
							PRINTLN("[Survival] - [Wave Progression] - serverBD.iKillsThisWaveSubStage < GET_COMPLETE_WAVE_STAGE_KILL_LIMIT")
							ENDIF
						ENDIF
					#ENDIF
					IF HAS_ENOUGH_TIME_PASSED_ON_WAVE()
					AND HAS_ENOUGH_TIME_PASSED_ON_SUBSTAGE()
					AND HAVE_ALL_PEDS_HAVE_BEEN_CREATED_THIS_STAGE()
						IF serverBD.iKillsThisWaveSubStage[serverBd.sWaveData.eStage] >= serverBD.sWaveData.iNumRequiredSubstageKills[eWAVESTAGE_MEDIUM]
							bMoveOnSubStage = TRUE
							PRINTLN("[Survival] - [Wave Progression] - eWAVESTAGE_MEDIUM - Moving substage on because we have reached kill limit ", serverBD.iKillsThisWaveSubStage[serverBd.sWaveData.eStage], " >= ", serverBD.sWaveData.iNumRequiredSubstageKills[eWAVESTAGE_MEDIUM])
						ELIF serverBD.iNumKillsThisSubStageFromEvents[serverBd.sWaveData.eStage] >= serverBD.sWaveData.iNumRequiredSubstageKills[eWAVESTAGE_MEDIUM]
							bMoveOnSubStage = TRUE
							PRINTLN("[Survival] - [Wave Progression] - eWAVESTAGE_MEDIUM - Moving substage on because event kills were reached ", serverBD.iNumKillsThisSubStageFromEvents[serverBd.sWaveData.eStage], " >= ", serverBD.sWaveData.iNumRequiredSubstageKills[eWAVESTAGE_MEDIUM])
						ELIF ARE_WE_ONE_OFF_OF_SUB_STAGE_KILL_LIMIT_WITH_NO_PEDS_TO_KILL(eWAVESTAGE_MEDIUM)
							bMoveOnSubStage = TRUE
							bNoPeds = TRUE
							PRINTLN("[Survival] - [Wave Progression] - eWAVESTAGE_MEDIUM - Moving substage on because One off no peds left")
						ELIF MAINTAIN_TRIGGER_NEXT_WAVE_SUB_STAGE_TIMER()
							bMoveOnSubStage = TRUE
							PRINTLN("[Survival] - [Wave Progression] - eWAVESTAGE_MEDIUM - Moving substage on because of timer expiring")
						ENDIF
					ENDIF
					IF serverBD.sWaveData.iNumRequiredSubstageKills[ENUM_TO_INT(eWAVESTAGE_HARD)] > 0
						IF bMoveOnSubStage
							PRINTLN("[Survival] - [Wave Progression] - setting serverBD.sWaveData.eStage = eWAVESTAGE_HARD")
	                        SET_WAVE_SUB_STAGE(eWAVESTAGE_HARD)
	                        serverBD.sWaveData.iMaintainSquadsStage = 0
							IF bNoPeds
	                			SERVER_INCREMENT_KILLS_THIS_WAVE()
								SERVER_INCREMENT_TOTAL_KILLS()
	                			SERVER_INCREMENT_KILLS_THIS_SUB_STAGE()
							ENDIF
	                    ENDIF
					ENDIF
					
                BREAK
                
            ENDSWITCH
            
        BREAK
        
        CASE eWAVESTAGE_HARD
    
            SWITCH serverBD.sWaveData.iMaintainSquadsStage
                
                CASE 0
                    serverBD.iKillsThisWaveSubStage[serverBd.sWaveData.eStage] = 0
					serverBD.iNumKillsThisSubStageFromEvents[serverBd.sWaveData.eStage] = 0
					RESET_TRIGGER_NEXT_WAVE_SUB_STAGE_TIMER()
					ACTIVATE_WAVE_SUBSTAGE_SQUADS()
					REINIT_NET_TIMER(serverBD.tdSubStageLength[serverBd.sWaveData.eStage])
                    serverBD.sWaveData.iMaintainSquadsStage++
                BREAK
                
                CASE 1
                    // Do nothing, wait for end of wave.
                BREAK
                
            ENDSWITCH
            
        BREAK
        
    ENDSWITCH
	
	IF serverBD.iNumAliveTotalEnemies != (serverBD.iNumAliveSquadEnemies + serverBD.iNumAliveLandVehiclePeds + serverBD.iNumAliveHelis)
		serverBD.iNumAliveTotalEnemies = (serverBD.iNumAliveSquadEnemies + serverBD.iNumAliveLandVehiclePeds + serverBD.iNumAliveHelis)
		PRINTLN("[Survival] - [Wave Progression] - serverBD.iNumAliveTotalEnemies = ", serverBD.iNumAliveTotalEnemies)
	ENDIF
    
ENDPROC

FUNC PICKUP_TYPE GET_PICKUP_TYPE_FOR_WAVE(INT iPickup)
    
	#IF IS_DEBUG_BUILD
		TEXT_LABEL_63 tl63_debugPrint
	#ENDIF
	
    // Always want health and armour to be the same.
    IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickup].pt = PICKUP_HEALTH_STANDARD
		PRINTLN("GET_PICKUP_TYPE_FOR_WAVE(", iPickup, ") || Returning PICKUP_HEALTH_STANDARD")
        RETURN PICKUP_HEALTH_STANDARD
    ENDIF
    
    IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickup].pt = PICKUP_ARMOUR_STANDARD
		PRINTLN("GET_PICKUP_TYPE_FOR_WAVE(", iPickup, ") || Returning PICKUP_ARMOUR_STANDARD")
        RETURN PICKUP_ARMOUR_STANDARD
    ENDIF
	
	WEAPON_TYPE wtPickupType = g_FMMC_STRUCT.sSurvivalWaveData.wtLoadoutWeapons[iWaveToUse][iWeaponPickupCount]
	PRINTLN("GET_PICKUP_TYPE_FOR_WAVE(", iPickup, ") iWaveToUse: ", iWaveToUse, " / iWeaponPickupCount: ", iWeaponPickupCount, "  || Returning pickup ", iWeaponPickupCount, " as ", GET_WEAPON_NAME(wtPickupType))
	
	PICKUP_TYPE ptOverride = g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickup].ptSurvival_WeaponOverridePerWave[iWaveToUse]
	IF ptOverride != NUM_PICKUPS
		wtPickupType = GET_WEAPON_TYPE_FROM_PICKUP_TYPE(ptOverride)
		PRINTLN("GET_PICKUP_TYPE_FOR_WAVE(", iPickup, ") || Overriding weapon spawnpoint ", iPickup, " with ", GET_WEAPON_NAME(wtPickupType))
	ENDIF
	
	iWeaponPickupCount++
	IF iWeaponPickupCount >= MAX_LOADOUT_WEAPONS
		iWeaponPickupCount -= MAX_LOADOUT_WEAPONS
	ENDIF
	
	PRINTLN("GET_PICKUP_TYPE_FOR_WAVE(", iPickup, ") || Returning pickup ", iWeaponPickupCount, " as ", GET_WEAPON_NAME(wtPickupType))
	RETURN GET_PICKUP_TYPE_FROM_WEAPON_HASH(wtPickupType)
	
    
    
	#IF IS_DEBUG_BUILD
		tl63_debugPrint = "GET_PICKUP_TYPE_FOR_WAVE - iWaveCount = "
		tl63_debugPrint += serverBD.sWaveData.iWaveCount
		tl63_debugPrint += ", iPickup = "
		tl63_debugPrint += iPickup
		PRINTLN(tl63_debugPrint)
	#ENDIF
    
    iWeaponPickupCount++ 
	RETURN PICKUP_WEAPON_SMG
    
ENDFUNC

//Squads
PROC INIT_SQUAD_PED(INT iSquad, INT iPed, INT iStage)
	
	serverBD_Enemies_S.structAiSquad[iSquad][iStage].sPed[iPed].sSquadIdData.iSquad = iSquad
	serverBD_Enemies_S.structAiSquad[iSquad][iStage].sPed[iPed].sSquadIdData.iSquadSlot = iPed
	serverBD_Enemies_S.structAiSquad[iSquad][iStage].sPed[iPed].bPlayedAngryShout = FALSE
	
ENDPROC

PROC INIT_SQUAD(INT iSquad, INT iStage)
	
	STRUCT_AI_SQUAD sTemp
	serverBD_Enemies_S.structAiSquad[iSquad][iStage] = sTemp
	
ENDPROC

PROC INITIALSE_ENEMIES()
	
	INT i, j, iStage
	
	FOR iStage = 0 TO NUM_SURVIVAL_SUB_STAGES-1
		REPEAT serverBD.sWaveData.iTotalSquads j
			
			// Squad data.
			INIT_SQUAD(j, iStage)
			
			// Data for each ped in the squad.
			REPEAT serverBD.sWaveData.iNumPedsInSquad[iStage] i
			
				INIT_SQUAD_PED(j, i, iStage)
				
			ENDREPEAT
			
		ENDREPEAT
	ENDFOR
	
ENDPROC

PROC RESET_ENEMY_PED_DATA()
	INT iSquad, iStage, iPed
	FOR iSquad = 0 TO MAX_SURV_ENEMY_SQUADS-1
		FOR iStage = 0 TO MAX_SURV_WAVE_DIFF-1
			serverBD_Enemies_S.structAiSquad[iSquad][iStage].eState = eSQUADSTATE_DORMANT
			FOR iPed = 0 TO MAX_SURV_PEDS_IN_SQUAD-1
				serverBD_Enemies_S.structAiSquad[iSquad][iStage].sPed[iPed].eState = eENEMYSTATE_DORMANT
				serverBD_Enemies_S.structAiSquad[iSquad][iStage].sPed[iPed].bHeavy = FALSE
			ENDFOR
		ENDFOR
	ENDFOR
ENDPROC

PROC RESET_LAND_VEHICLE_DATA()
	
	INT i
	STRUCT_SURVIVAL_LAND_VEHICLE_DATA sBlank
	REPEAT MAX_NUM_LAND_VEHICLES i
		serverBD_Enemies_V.sSurvivalLandVehicle[i] = sBlank
	ENDREPEAT
	
ENDPROC

PROC RESET_AIR_VEHICLE_DATA()
	INT i
	
	REPEAT MAX_NUM_SURVIVAL_HELIS i
		serverBD_Enemies_V.sSurvivalHeli[i].bActive = FALSE
		serverBD_Enemies_V.sSurvivalHeli[i].eState = eSURVIVALHELISTATE_NOT_EXIST
	ENDREPEAT
ENDPROC

PROC RESET_NO_VEHICLE_ENEMIES_IN_SURVIVAL_AREA_TIMER()
	
	RESET_NET_TIMER(serverBD_Enemies_V.stVehicleNotInAreaTimer)
	CLEAR_BIT(serverBD.iBitset, BITSET_SERVER_START_VEHICLE_FAIL_SAFE_TIMER)
	CLEAR_BIT(serverBD.iBitset, BITSET_SERVER_ALL_VEHICLE_PEDS_STUCK_OOB)
	
ENDPROC

PROC RESET_DATA_FOR_NEW_WAVE()
    
    CLEAR_BIT(serverBD.iBitset, BITSET_SERVER_CURRENT_WAVE_DEFEATED)
    RESET_NO_VEHICLE_ENEMIES_IN_SURVIVAL_AREA_TIMER()
	RESET_NET_TIMER(serverBD_Enemies_V.stNoSpawningEnemiesTimer)
				
	// Reset this timer.
	RESET_TRIGGER_NEXT_WAVE_SUB_STAGE_TIMER()
	
	// Reset all our enemies in prep for the new wave.
    INITIALSE_ENEMIES()
    
	// Reset all data for managing waves, new wave needs new data.
    serverBD.sWaveData.iNumRequiredKills            = 0
    serverBD.sWaveData.eStage                   	= eWAVESTAGE_EASY
    serverBD.sWaveData.iMaintainSquadsStage         = 0
    serverBD.iNumAliveSquadEnemies                	= 0
    serverBD.iNumAliveHelis                      	= 0
	PRINTLN("[Survival] - RESET_DATA_FOR_NEW_WAVE - serverBD.iNumAliveHelis = 0")
	serverBD.iNumAliveLandVehiclePeds				= 0
    serverBD.iKillsThisWave                       	= 0
		
	serverBD.iNumCreatedEnemies						= 0
    SET_SEARCHING_FOR_SQUAD_PED(-1)
	SET_SEARCHING_FOR_AIR_VEHICLE(-1)
	SET_SEARCHING_FOR_LAND_VEHICLE(-1)
	serverBd.iNumKillsThisWaveFromEvents 			= 0
	
	INT iStage
	FOR iStage = 0 TO NUM_SURVIVAL_SUB_STAGES-1
		serverBD.iKillsThisWaveSubStage[iStage]   		 	= 0
		serverBD.iNumKillsThisSubStageFromEvents[iStage]	= 0
		serverBD.iNumCreatedEnemiesThisStage[iStage]    	= 0
		serverBD.sWaveData.iSpawnChangeAttempted[iStage]	= 0
	ENDFOR	
	
	RESET_ENEMY_PED_DATA()
	RESET_LAND_VEHICLE_DATA()
	RESET_AIR_VEHICLE_DATA()
	
	CLEAR_HEAVY_UNIT_SERVER_DATA()
	
	CLEAR_SQUAD_SPAWN_POINTS()
	
	CLEAR_BIT(serverBD.iBitset, BITSET_SERVER_ENDLESS_WAVE_EXTRAS_DONE)
	PRINTLN("RESET_DATA_FOR_NEW_WAVE - Clearing BITSET_SERVER_ENDLESS_WAVE_EXTRAS_DONE for wave ", serverBD.sWaveData.iWaveCount)
	
ENDPROC

PROC INCREMENT_WAVE()
    
    IF IS_BIT_SET(serverBD.iBitset, BITSET_SERVER_DID_FIRST_WAVE_SETUP)
        serverBD.sWaveData.iWaveCount++
		PRINTLN("INCREMENT_WAVE - Incrementing wave to ", serverBD.sWaveData.iWaveCount)
		IF serverBD.sWaveData.iWaveCount > serverBD.iNumberOfWaves
			serverBD.sWaveData.iEndlessWave++
			PROCESS_ENDLESS_WAVE_EXTRAS()
			PRINTLN("INCREMENT_WAVE - Incrementing endless wave to ", serverBD.sWaveData.iEndlessWave)
		ENDIF
    ENDIF
    
    SET_BIT(serverBD.iBitset, BITSET_SERVER_DID_FIRST_WAVE_SETUP)
    
ENDPROC

FUNC INT GET_NUM_REQUIRED_KILLS_FOR_WAVE()
    
    PRINTLN("[Survival] - [Wave Progression] - GET_NUM_REQUIRED_KILLS_FOR_WAVE - being called.")
    
    RETURN serverBD.sWaveData.iNumRequiredKills
    
ENDFUNC

PROC SETUP_NEXT_WAVE()
	
	// Reset data to initial values in preperation for setting them again for the new wave.
    RESET_DATA_FOR_NEW_WAVE()
	
	// If we ghave called this function once, the first will have been setup.
    SET_BIT(serverBD.iBitset, BITSET_SERVER_DID_FIRST_WAVE_SETUP)
	
ENDPROC

PROC SET_PICKUP_TYPES_FOR_WAVE()
    
    INT i
    
    REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons i
        //g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].pt =     GET_PICKUP_TYPE_FOR_WAVE(i)
		ptWaveLoadout[i] = GET_PICKUP_TYPE_FOR_WAVE(i)
		PRINTLN("SET_PICKUP_TYPES_FOR_WAVE - Setting pickup ", i, " to be ", GET_WEAPON_NAME(GET_WEAPON_TYPE_FROM_PICKUP_TYPE(ptWaveLoadout[i])))
		
    ENDREPEAT
    
	iWeaponPickupCount = 0
	
ENDPROC

FUNC eWAVE_STAGE GET_WAVE_SUB_STAGE_LIMIT(INT iWave)
	
	IF iWave <= 3
		RETURN 	eWAVESTAGE_MEDIUM
	ENDIF
	
	RETURN eWAVESTAGE_HARD
	
ENDFUNC

FUNC BOOL ON_CURRENT_WAVE_LAST_SUB_STAGE()
	
	RETURN ( serverBD.sWaveData.eStage = GET_WAVE_SUB_STAGE_LIMIT(serverBD.sWaveData.iWaveCount) )
	
ENDFUNC

FUNC FLOAT GET_SURVIVAL_MAX_WAVE_MODIFIER()
	IF HAVE_ENDLESS_WAVES_STARTED()
		INT iRewardEndlessWave = serverBD.sWaveData.iWaveCount
		iRewardEndlessWave = CLAMP_INT(iRewardEndlessWave,0,20)
		PRINTLN("GET_SURVIVAL_MAX_WAVE_MODIFIER - Getting endless modifier. iRewardEndlessWave: ", iRewardEndlessWave)
		RETURN 1.0 + (0.05*(iRewardEndlessWave-1))
	ENDIF
	RETURN 1.0 + (0.05*(serverBD.iNumberOfWaves-1))
ENDFUNC

FUNC FLOAT GET_SURVIVAL_WAVE_REACHED_MODIFIER()
	IF HAVE_ENDLESS_WAVES_STARTED()
		INT iRewardEndlessWave = serverBD.sWaveData.iWaveCount
		iRewardEndlessWave = CLAMP_INT(iRewardEndlessWave,0,20)
		RETURN TO_FLOAT(iRewardEndlessWave)/iRewardEndlessWave
	ENDIF
	RETURN TO_FLOAT(iWaveToUse)/serverBD.iNumberOfWaves
ENDFUNC

FUNC FLOAT GET_SURVIVAL_COMPLETION_MODIFIER()
	FLOAT fCompleteMod = g_sMPTunables.fCompletedBonus
	IF GET_END_HORDE_REASON() = eENDHORDEREASON_BEAT_ALL_WAVES
		RETURN 1.0 + fCompleteMod
	ENDIF
	RETURN 1.0
ENDFUNC

FUNC FLOAT GET_SURVIVAL_TIME_MODIFIER()
	INT iTime = GET_ABS_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.tdSurvivalLength)
	PRINTLN("GET_SURVIVAL_TIME_MODIFIER - iTime: ", iTime)
	FLOAT fMod
	IF iTime >= 3600000		//1 Hour
		fMod = 3.0
	ELIF iTime >= 3300000	//55 mins
		fMod = 2.9
	ELIF iTime >= 3000000	//50
		fMod = 2.8
	ELIF iTime >= 2700000	//45
		fMod = 2.7
	ELIF iTime >= 2400000	//40
		fMod = 2.6
	ELIF iTime >= 2100000	//35
		fMod = 2.5
	ELIF iTime >= 1800000	//30
		fMod = 2.4
	ELIF iTime >= 1500000	//25
		fMod = 2.2
	ELIF iTime >= 1200000	//20
		fMod = 2
	ELIF iTime >= 900000	//15
		fMod = 1.5
	ELIF iTime >= 600000	//10
		fMod = 1
	ELIF iTime >= 540000	//9
		fMod = 0.85
	ELIF iTime >= 480000	//8
		fMod = 0.75
	ELIF iTime >= 420000	//7
		fMod = 0.6
	ELIF iTime >= 360000	//6
		fMod = 0.5
	ELIF iTime >= 300000	//5
		fMod = 0.4
	ELIF iTime >= 240000	//4
		fMod = 0.25
	ELIF iTime >= 180000	//3
		fMod = 0.15
	ELIF iTime >= 120000	//2
		fMod = 0.1
	ELSE
		fMod = 0.0
	ENDIF
	PRINTLN("GET_SURVIVAL_TIME_MODIFIER - Returning ", fMod)
	RETURN fMod
ENDFUNC

FUNC FLOAT GET_SURVIVAL_KILL_MODIFIER()
	INT iAverageKillsPerWave = g_sMPTunables.iAverageKillsPerWave
	INT iTeamKills = serverBD.iKills
	PRINTLN("GET_SURVIVAL_KILL_MODIFIER - serverBD.iKills: ", serverBD.iKills)
	FLOAT fKillPercentage = TO_FLOAT(iTeamKills)/(iAverageKillsPerWave * iWaveToUse)
	FLOAT fReturn
	
	IF fKillPercentage >= 1.0
		fReturn = 1.0
	ELIF fKillPercentage >= 0.75
		fReturn = 0.8
	ELIF fKillPercentage >= 0.5
		fReturn = 0.6
	ELIF fKillPercentage >= 0.25
		fReturn = 0.4
	ELSE
		fReturn = 0.2
	ENDIF
	
	RETURN fReturn
ENDFUNC

FUNC FLOAT GET_SURVIVAL_PLAYER_CASH_MODIFIER()
	FLOAT iPlayerMod = 1
	IF GET_NUM_COMPETITIVE_PARTICIPANTS_THIS_WAVE() >= 2
		iPlayerMod += g_sMPTunables.fNumberOfPlayersCashMod
	ENDIF
	RETURN iPlayerMod
ENDFUNC

FUNC FLOAT GET_SURVIVAL_PLAYER_RP_MODIFIER()
	FLOAT iPlayerMod = 1
	IF GET_NUM_COMPETITIVE_PARTICIPANTS_THIS_WAVE() >= 2
		iPlayerMod += g_sMPTunables.fNumberOfPlayersRPMod
	ENDIF
	RETURN iPlayerMod
ENDFUNC

FUNC INT GET_END_SURVIVAL_CASH_REWARD()
	DEBUG_PRINTCALLSTACK()
	FLOAT fTotalCash, fMaxWaveMod, fWaveReachedMod, fCompletionMod, fTimeMod, fKillMod, fPlayerMod, fUGCMod
	
	fTotalCash += g_sMPTunables.iNewSurvivalBaseCash
	PRINTLN("GET_END_SURVIVAL_CASH_REWARD - Current Total Cash: ", fTotalCash, " base")
	
	fMaxWaveMod = GET_SURVIVAL_MAX_WAVE_MODIFIER()
	fTotalCash = fTotalCash * fMaxWaveMod
	PRINTLN("GET_END_SURVIVAL_CASH_REWARD - Current Total Cash: ", fTotalCash, " After * fMaxWaveMod(", fMaxWaveMod,")")
	
	fWaveReachedMod = GET_SURVIVAL_WAVE_REACHED_MODIFIER()
	fTotalCash = fTotalCash * fWaveReachedMod
	PRINTLN("GET_END_SURVIVAL_CASH_REWARD - Current Total Cash: ", fTotalCash, " After * fWaveReachedMod(", fWaveReachedMod,")")
	
	fCompletionMod = GET_SURVIVAL_COMPLETION_MODIFIER()
	fTotalCash = fTotalCash * fCompletionMod
	PRINTLN("GET_END_SURVIVAL_CASH_REWARD - Current Total Cash: ", fTotalCash, " After * fCompletionMod(", fCompletionMod,")")
	
	fTimeMod = GET_SURVIVAL_TIME_MODIFIER()
	fTotalCash = fTotalCash * fTimeMod
	PRINTLN("GET_END_SURVIVAL_CASH_REWARD - Current Total Cash: ", fTotalCash, " After * fTimeMod(", fTimeMod,")")
	
	fKillMod = GET_SURVIVAL_KILL_MODIFIER()
	fTotalCash = fTotalCash * fKillMod
	PRINTLN("GET_END_SURVIVAL_CASH_REWARD - Current Total Cash: ", fTotalCash, " After * fKillMod(", fKillMod,")")
	
	fPlayerMod = GET_SURVIVAL_PLAYER_CASH_MODIFIER()
	fTotalCash = fTotalCash * fPlayerMod
	PRINTLN("GET_END_SURVIVAL_CASH_REWARD - Current Total Cash: ", fTotalCash, " After * fPlayerMod(", fPlayerMod,")")
	
	IF NOT IS_ROCKSTAR_CREATED_MISSION()
		fUGCMod = g_sMPTunables.fUGCModifier
		fTotalCash = fTotalCash * fUGCMod
		PRINTLN("GET_END_SURVIVAL_CASH_REWARD - Current Total Cash: ", fTotalCash, " After * fUGCMod(", fUGCMod,")")
	ENDIF
	
	PRINTLN("GET_END_SURVIVAL_CASH_REWARD - Final Cash (Rounded): ", ROUND(fTotalCash))
	RETURN ROUND(fTotalCash)
ENDFUNC

FUNC INT GET_END_SURVIVAL_RP_REWARD()
	DEBUG_PRINTCALLSTACK()
	FLOAT fTotalRP, fMaxWaveMod, fWaveReachedMod, fCompletionMod, fTimeMod, fKillMod, fPlayerMod, fUGCMod
	
	fTotalRP += g_sMPTunables.iNewSurvivalBaseRP
	PRINTLN("GET_END_SURVIVAL_RP_REWARD - Current Total RP: ", fTotalRP, " base")
	
	fMaxWaveMod = GET_SURVIVAL_MAX_WAVE_MODIFIER()
	fTotalRP = fTotalRP * fMaxWaveMod
	PRINTLN("GET_END_SURVIVAL_RP_REWARD - Current Total RP: ", fTotalRP, " After * fMaxWaveMod(", fMaxWaveMod,")")
	
	fWaveReachedMod = GET_SURVIVAL_WAVE_REACHED_MODIFIER()
	fTotalRP = fTotalRP * fWaveReachedMod
	PRINTLN("GET_END_SURVIVAL_RP_REWARD - Current Total RP: ", fTotalRP, " After * fWaveReachedMod(", fWaveReachedMod,")")
	
	fCompletionMod = GET_SURVIVAL_COMPLETION_MODIFIER()
	fTotalRP = fTotalRP * fCompletionMod
	PRINTLN("GET_END_SURVIVAL_RP_REWARD - Current Total RP: ", fTotalRP, " After * fCompletionMod(", fCompletionMod,")")
	
	fTimeMod = GET_SURVIVAL_TIME_MODIFIER()
	fTotalRP = fTotalRP * fTimeMod
	PRINTLN("GET_END_SURVIVAL_RP_REWARD - Current Total RP: ", fTotalRP, " After * fTimeMod(", fTimeMod,")")
	
	fKillMod = GET_SURVIVAL_KILL_MODIFIER()
	fTotalRP = fTotalRP * fKillMod
	PRINTLN("GET_END_SURVIVAL_RP_REWARD - Current Total RP: ", fTotalRP, " After * fKillMod(", fKillMod,")")
	
	fPlayerMod = GET_SURVIVAL_PLAYER_RP_MODIFIER()
	fTotalRP = fTotalRP * fPlayerMod
	PRINTLN("GET_END_SURVIVAL_RP_REWARD - Current Total RP: ", fTotalRP, " After * fPlayerMod(", fPlayerMod,")")
	
	IF NOT IS_ROCKSTAR_CREATED_MISSION()
		fUGCMod = g_sMPTunables.fUGCModifier
		fTotalRP = fTotalRP * fUGCMod
		PRINTLN("GET_END_SURVIVAL_RP_REWARD - Current Total RP: ", fTotalRP, " After * fUGCMod(", fUGCMod,")")
	ENDIF
	
	PRINTLN("GET_END_SURVIVAL_RP_REWARD - Final RP (Rounded): ", ROUND(fTotalRP))
	RETURN ROUND(fTotalRP)
ENDFUNC

PROC HANDLE_END_OF_SURVIVAL_RP()
	INT iXP = GET_END_SURVIVAL_RP_REWARD()
	iXp = GIVE_LOCAL_PLAYER_XP(eXPTYPE_STANDARD, "XPT_ENDHRD", XPTYPE_COMPLETE, XPCATEGORY_COMPLETE_SURVIVAL_WAVE, iXp, 1, -1, TRUE)
	playerBD[iLocalPart].iXpGained += iXp
	iTotalWaveAwardXp += iXp
	ADD_TO_END_CELEBRATION_SCORE(iXp)
ENDPROC

PROC SET_CELEBRATION_SUMMARY_STAGE(CELEBRATION_SCREEN_STAGE eStage, INT iCall)
	PRINTLN("SET_CELEBRATION_SUMMARY_STAGE - Moving to stage ", ENUM_TO_INT(eStage), " Call number: ", iCall)
	sCelebrationData.eCurrentStage = eStage
ENDPROC

FUNC BOOL IS_VALID_SURVIVAL_ENDING()
	IF IS_BIT_SET(playerBD[iLocalPart].iBitset, BITSET_PLAYER_ABANDONED_HORDE)
	OR HAS_NET_TIMER_EXPIRED(stLeaveHordeTimer, GET_OUT_OF_BOUNDS_TIMER_LIMIT())
		PRINTLN("IS_VALID_SURVIVAL_ENDING - Out of Bounds")
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_NET_TIMER_STARTED(stSurvivalTime)
		PRINTLN("IS_VALID_SURVIVAL_ENDING - Survival Time not started")
		RETURN FALSE
	ELSE
		IF NOT HAS_NET_TIMER_EXPIRED(stSurvivalTime, 120000)
			PRINTLN("IS_VALID_SURVIVAL_ENDING - Survival Time less than 2 minutes")
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Runs the celebration screen showing the end stats.
///    IF bEndOfWaveSummary = TRUE then this will set up the end of wave stats, otherwise it'll set up the end of job stats.
/// RETURNS:
///    TRUE once the sequence has finished.
FUNC BOOL HAS_CELEBRATION_SUMMARY_FINISHED(STRUCT_HORDE_BOARD_DATA &sBoardData, BOOL bJobPassed, BOOL bEndOfMode = FALSE)
	//Check if the screen should be displayed (e.g. if the player quit early then it should be skipped.
	
	#IF IS_DEBUG_BUILD
		SWITCH sCelebrationData.eCurrentStage
			CASE CELEBRATION_STAGE_SETUP						PRINTLN("[Survival] - celebration in case CELEBRATION_STAGE_SETUP")  						BREAK
			CASE CELEBRATION_STAGE_TRANSITIONING				PRINTLN("[Survival] - celebration in case CELEBRATION_STAGE_TRANSITIONING")  				BREAK
			CASE CELEBRATION_STAGE_DOING_FLASH					PRINTLN("[Survival] - celebration in case CELEBRATION_STAGE_DOING_FLASH")  					BREAK
			CASE CELEBRATION_STAGE_PLAYING						PRINTLN("[Survival] - celebration in case CELEBRATION_STAGE_PLAYING")  						BREAK
			CASE CELEBRATION_STAGE_END_TRANSITION				PRINTLN("[Survival] - celebration in case CELEBRATION_STAGE_END_TRANSITION")  				BREAK
			CASE CELEBRATION_STAGE_FINISHED						PRINTLN("[Survival] - celebration in case CELEBRATION_STAGE_FINISHED")  					BREAK
			CASE CELEBRATION_STAGE_WAITING_FOR_SPECTATOR_CAM	PRINTLN("[Survival] - celebration in case CELEBRATION_STAGE_WAITING_FOR_SPECTATOR_CAM") 	BREAK
		ENDSWITCH
	#ENDIF
	
	IF (sCelebrationData.eCurrentStage < CELEBRATION_STAGE_END_TRANSITION)
		BOOL bEndTransition
		IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(localPlayer)].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bQuitJob)
			bEndTransition = TRUE
			PRINTLN("HAS_CELEBRATION_SUMMARY_FINISHED - Setting bEndTransition to TRUE - local player is quitting")
		ENDIF
		IF IS_BIT_SET(playerBD[iLocalPart].iBitset, BITSET_PLAYER_I_WENT_STRAIGHT_TO_SPEC_CAM)
			bEndTransition = TRUE
			PRINTLN("HAS_CELEBRATION_SUMMARY_FINISHED - Setting bEndTransition to TRUE - I went straight to spectator cam")
		ENDIF
		IF IS_PLAYER_SCTV(localPlayer)
			bEndTransition = TRUE
			PRINTLN("HAS_CELEBRATION_SUMMARY_FINISHED - Setting bEndTransition to TRUE - SCTV")
		ENDIF
		
		IF bEndTransition
			IF NOT sCelebrationData.sCelebrationTimer.bInitialisedTimer
				START_NET_TIMER(sCelebrationData.sCelebrationTimer)
			ENDIF
		
			SET_CHAT_ENABLED_FOR_CELEBRATION_SCREEN(TRUE)
		
			SET_CELEBRATION_SUMMARY_STAGE(CELEBRATION_STAGE_END_TRANSITION, 0)
		ENDIF
	ENDIF
	
	IF sCelebrationData.eCurrentStage > CELEBRATION_STAGE_SETUP
		HIDE_UI_FOR_CELEBRATION_SCREEN_THIS_FRAME()
	ENDIF
	
	//Handle the screen flow.
	SWITCH sCelebrationData.eCurrentStage
		CASE CELEBRATION_STAGE_SETUP
			BOOL bReadyToProgress
		
			SET_CELEBRATION_SCREEN_AS_ACTIVE(TRUE)
			SET_CELEB_BLOCK_RESURRECT_RENDERPHASE_UNPAUSE(TRUE)
			//Sort out any cameras if necessary.
			IF NOT bEndOfMode
				IF NOT bAlreadyTriggeredCelebrationPostFX //Don't do any of this if we're transitioning out of spectator cam.
					//Place a camera in front of the player if safe to do so.
					BOOL bPlacedCameraSuccessfully
				
					IF PLACE_CAMERA_FOR_CELEBRATION_SCREEN(sCelebrationData, camCelebrationScreen, bPlacedCameraSuccessfully)				
						IF bPlacedCameraSuccessfully
							RENDER_SCRIPT_CAMS(TRUE, FALSE)
						ELSE
							//1626331 - If the celebration cam didn't successfully create then turn the ped to face the game cam.
							IF bLocalPlayerOK
							AND NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
								TASK_TURN_PED_TO_FACE_COORD(LocalPlayerPed, GET_FINAL_RENDERED_CAM_COORD(), 2000)
							ENDIF
						ENDIF
						
						//Reset the action mode to the default one.
						IF bLocalPlayerOK
							SET_PED_USING_ACTION_MODE(LocalPlayerPed, FALSE)
							SET_PED_USING_ACTION_MODE(LocalPlayerPed, TRUE, -1, "DEFAULT_ACTION")
							
							ANIMPOSTFX_STOP_ALL()
							PRINTLN("[Survival] - Stopping all Anim Post FX 2")
						ENDIF
						
						IF bJobPassed
						OR NOT bEndOfMode
							PLAY_CELEB_WIN_POST_FX()
							PRINTLN("[Survival] - playing MP_Celeb_Win, call 1")
						ELSE
							PLAY_CELEB_LOSE_POST_FX()
							PRINTLN("[Survival] - playing MP_Celeb_Lose, call 2")
						ENDIF
						
						IF NOT bEndOfMode
							IF bJobPassed
								PLAY_SOUND_FRONTEND(-1, "Round_Passed", GET_SOUND_SET_FOR_SURVIVAL())
								PRINTLN("[Survival] - SOUNDS - Round_Passed playing from Frontend")
							ELSE
								IF IS_HALLOWEEN_MODE()
									PLAY_SOUND_FRONTEND(-1, "Survival_Failed", "DLC_VW_AS_Sounds")
									PRINTLN("[Survival] - SOUNDS - Survival_Failed playing from Frontend (1)")
								ENDIF
							ENDIF
						ENDIF
						PLAY_SOUND_FRONTEND(-1, "MP_WAVE_COMPLETE", "HUD_FRONTEND_DEFAULT_SOUNDSET")
						
						bReadyToProgress = TRUE
					ENDIF
				ENDIF
			ELSE
				bReadyToProgress = TRUE
			ENDIF
			
			IF bReadyToProgress
				CLEAR_CELEBRATION_SCREEN_MOVIE_IDS(sCelebrationData)
				REQUEST_CELEBRATION_SCREEN(sCelebrationData)
				SET_CELEBRATION_SCREEN_AS_ACTIVE(TRUE)
				KILL_UI_FOR_CELEBRATION_SCREEN()
				HIDE_UI_FOR_CELEBRATION_SCREEN_THIS_FRAME()
				NET_SET_PLAYER_CONTROL(localPlayer, FALSE)
				
				//1636397 - Wait a bit before triggering the scaleform
				RESET_NET_TIMER(sCelebrationData.sCelebrationTransitionTimer)
				START_NET_TIMER(sCelebrationData.sCelebrationTransitionTimer)
				
				SET_CELEBRATION_SUMMARY_STAGE(CELEBRATION_STAGE_TRANSITIONING, 1)
			ENDIF
		BREAK
	
		CASE CELEBRATION_STAGE_TRANSITIONING
			IF IS_SCREEN_FADED_OUT()
			AND NOT IS_SCREEN_FADING_IN()
			AND (playerBD[iLocalPart].eSpectatorState = ci_SpectatorState_IDLE OR playerBD[iLocalPart].eSpectatorState = ci_SpectatorState_END)
				DO_SCREEN_FADE_IN(500)
			ENDIF
			
			REQUEST_CELEBRATION_SCREEN(sCelebrationData)
	
			//PLAYER_INDEX winningPlayer
			BOOL bFetchedStatsFromServer, bPlayerInSameCrewAsWinner
			GAMER_HANDLE sWinnerHandle
			
			IF NOT bEndOfMode
				bFetchedStatsFromServer = TRUE
				bPlayerInSameCrewAsWinner = FALSE
			ELSE
				//winningPlayer = GET_PLAYER_WITH_MOST_KILLS(sBoardData)
				//sWinnerHandle = GET_GAMER_HANDLE_PLAYER(winningPlayer)
				IF HordeLeaderboard[0].iParticipant > -1
					sWinnerHandle = lbdVars.aGamer[HordeLeaderboard[0].iParticipant] //This should be able to grab handles for players that have left already left the game.
				ENDIF
				bPlayerInSameCrewAsWinner = IS_PLAYER_IN_SAME_CREW_AS_ME(sWinnerHandle, bFetchedStatsFromServer)
			ENDIF
				
			IF HAS_CELEBRATION_SCREEN_LOADED(sCelebrationData)		
			AND bFetchedStatsFromServer
			AND IS_SCREEN_FADED_IN()
			AND HAS_NET_TIMER_EXPIRED(sCelebrationData.sCelebrationTransitionTimer, 500)
			AND NOT ANIMPOSTFX_IS_RUNNING("DeathFailMPIn")
				
				STRING strScreenName, strBackgroundColour
				INT iCurrentRP, iCash, iCurrentLvl, iNextLvl, iRPToReachCurrentLvl, iRPToReachNextLvl, iTotalKills, iChallengePart, iTotalChallengeParts, i, iNumValidPlayers
				BOOL bWonChallengePart
				TEXT_LABEL strPlayerKills, strTotalKills
			
				//Retrieve all the required stats for the race end screen.
				strScreenName = "SUMMARY"
				strBackgroundColour = "HUD_COLOUR_BLACK"
				sCelebrationData.iEstimatedScreenDuration = CELEBRATION_SCREEN_STAT_WIPE_TIME * 2
				
				REPEAT MAX_NUMBER_HORDE_FIGHTING_PARTICIPANTS i
					IF sBoardData.hudPlayers[i] != INVALID_PLAYER_INDEX()
			       		IF IS_NET_PLAYER_OK(sBoardData.hudPlayers[i], FALSE)
							iTotalKills += sBoardData.iScores[i]
						ENDIF
					ENDIF
				ENDREPEAT
				
				//Build the screen
				CREATE_CELEBRATION_SCREEN(sCelebrationData, strScreenName, strBackgroundColour)
								
				IF NOT bEndOfMode
					ADD_WAVE_REACHED_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName, serverBD.sWaveData.iWaveCount, TRUE)
					sCelebrationData.iEstimatedScreenDuration += CELEBRATION_SCREEN_STAT_DISPLAY_TIME
					
					//ADD_TOTAL_KILLS_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName, iTotalKills)
					
					CREATE_CELEBRATION_STAT_TABLE(sCelebrationData, strScreenName, "SURVIVAL_TABLE")
					
					BOOL bAlignLeft
					
					bAlignLeft = FALSE
					REPEAT MAX_NUMBER_HORDE_FIGHTING_PARTICIPANTS i
						IF sBoardData.hudPlayers[i] != INVALID_PLAYER_INDEX()
							//ADD_GAMERTAG_KILLS_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName, GET_PLAYER_NAME(sBoardData.hudPlayers[i]), sBoardData.iScores[i], TRUE, bAlignLeft)
							strPlayerKills = sBoardData.iScores[i]
							ADD_ITEM_TO_CELEBRATION_STAT_TABLE(sCelebrationData, strScreenName, "SURVIVAL_TABLE", GET_PLAYER_NAME(sBoardData.hudPlayers[i]), strPlayerKills, TRUE, TRUE, FALSE)
							iNumValidPlayers++
							bAlignLeft = NOT bAlignLeft
						ENDIF
					ENDREPEAT
					
					strTotalKills = iTotalKills
					ADD_ITEM_TO_CELEBRATION_STAT_TABLE(sCelebrationData, strScreenName, "SURVIVAL_TABLE", "CELEB_TOTAL_KILLS", strTotalKills, FALSE, TRUE, TRUE)
					ADD_CELEBRATION_STAT_TABLE_TO_CURRENT_SCREEN(sCelebrationData, strScreenName, "SURVIVAL_TABLE")
					sCelebrationData.iEstimatedScreenDuration += CELEBRATION_SCREEN_STAT_DISPLAY_TIME
				ELSE
					ADD_MISSION_RESULT_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName, CELEBRATION_JOB_TYPE_SURVIVAL, bJobPassed, "", "", "")
					ADD_WAVE_REACHED_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName, serverBD.sWaveData.iWaveCount, FALSE)
					sCelebrationData.iEstimatedScreenDuration += CELEBRATION_SCREEN_STAT_DISPLAY_TIME * 2
					IF IS_VALID_SURVIVAL_ENDING()
						iCash = MULTIPLY_CASH_BY_TUNABLE(GET_END_SURVIVAL_CASH_REWARD())
						IF iCash > 0
							IF USE_SERVER_TRANSACTIONS()
								INT iScriptTransactionIndex
								TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_JOBS, iCash, iScriptTransactionIndex, DEFAULT, DEFAULT)
								g_cashTransactionData[iScriptTransactionIndex].eExtraData.tl31ContenID = g_FMMC_STRUCT.tl31LoadedContentID
							ELSE
								NETWORK_EARN_FROM_JOB(iCash, g_FMMC_STRUCT.tl31LoadedContentID)
							ENDIF
							PRINTLN("[Survival] - [Cash_Xp] - given player $", iCash)
							
							ADD_TO_JOB_CASH_EARNINGS(iCash)
							PRINTLN("[Survival] - [Cash_Xp] - called ADD_TO_JOB_CASH_EARNINGS with $", iCash)
						ENDIF
						
						//Add RP
						HANDLE_END_OF_SURVIVAL_RP()
						
						iCurrentRP = GET_PLAYER_FM_XP(NETWORK_GET_PLAYER_INDEX(PARTICIPANT_ID())) - iTotalWaveAwardXp
						iCurrentLvl = GET_FM_RANK_FROM_XP_VALUE(iCurrentRP) //We don't want the current level, but their level before the XP was given.
						iNextLvl = iCurrentLvl + 1
						iRPToReachCurrentLvl = GET_XP_NEEDED_FOR_FM_RANK(iCurrentLvl)
						iRPToReachNextLvl = GET_XP_NEEDED_FOR_FM_RANK(iNextLvl)
						iChallengePart = g_sCurrentPlayListDetails.iPlaylistProgress
						iTotalChallengeParts = g_sCurrentPlayListDetails.iLength

						IF IS_PLAYLIST_DOING_HEAD_TO_HEAD()
							ADD_CHALLENGE_PART_RESULT_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName, CHALLENGE_PART_HEAD_TO_HEAD, iChallengePart, iTotalChallengeParts, bPlayerInSameCrewAsWinner)
							sCelebrationData.iEstimatedScreenDuration += CELEBRATION_SCREEN_STAT_DISPLAY_TIME
						ELIF IS_PLAYLIST_DOING_CHALLENGE()
							IF playerBD[iLocalPart].iSurvivalScore >= g_sCurrentPlayListDetails.sLoadedMissionDetails[g_sCurrentPlayListDetails.iCurrentPlayListPosition].iBestScore
								bWonChallengePart = TRUE
							ENDIF
						
							ADD_CHALLENGE_PART_RESULT_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName, CHALLENGE_PART_CREW, iChallengePart, iTotalChallengeParts, bWonChallengePart)
							sCelebrationData.iEstimatedScreenDuration += CELEBRATION_SCREEN_STAT_DISPLAY_TIME
						ENDIF
						
						IF CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_SURVIVAL_SERIES(g_FMMC_STRUCT.iRootContentIDHash)
							SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_PARTICIPATE_IN_SURVIVAL_SERIES)
							PRINTLN("[Survival] - Setting Daily Objective - Survival Series")
						ENDIF
						
						IF iCash != 0
							ADD_CASH_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName, iCash)
							sCelebrationData.iEstimatedScreenDuration += CELEBRATION_SCREEN_STAT_DISPLAY_TIME
						ENDIF
						
						IF (GET_END_HORDE_REASON() != eENDHORDEREASON_NOT_FINISHED_YET)
							ADD_REP_POINTS_AND_RANK_BAR_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName, iEndCelebrationScreenScore, iCurrentRP, 
													   	  	   				  iRPToReachCurrentLvl, iRPToReachNextLvl, iCurrentLvl, iNextLvl)
							sCelebrationData.iEstimatedScreenDuration += CELEBRATION_SCREEN_STAT_DISPLAY_TIME
							
							IF iCurrentRP + iEndCelebrationScreenScore > iRPToReachNextLvl
								sCelebrationData.iEstimatedScreenDuration += ((CELEBRATION_SCREEN_STAT_DISPLAY_TIME / 2) + (CELEBRATION_SCREEN_STAT_DISPLAY_TIME / 4))
							ENDIF
						ENDIF
					ENDIF
					
					sCelebrationData.iEstimatedScreenDuration += CELEBRATION_SCREEN_STAT_WIPE_TIME
				ENDIF				
				
				ADD_BACKGROUND_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName)
				SHOW_CELEBRATION_SCREEN(sCelebrationData, strScreenName)

				//Audio scenes: see B*1642903 for implementation notes.
				IF NOT IS_AUDIO_SCENE_ACTIVE("MP_CELEB_SCREEN_SCENE")
					START_AUDIO_SCENE("MP_CELEB_SCREEN_SCENE")
				ENDIF

				IF bEndOfMode
					
					SET_SKYFREEZE_FROZEN()
				
					STOP_AUDIO_SCENE("MP_POSITIONED_RADIO_MUTE_SCENE")
					START_AUDIO_SCENE("MP_JOB_CHANGE_RADIO_MUTE")
					PRINTLN("[Survival] - called START_AUDIO_SCENE(MP_JOB_CHANGE_RADIO_MUTE).")
					
					SET_CHAT_ENABLED_FOR_CELEBRATION_SCREEN(TRUE)
				ELSE
					//Leave the kills table up a bit longer.
					ADD_PAUSE_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName, CELEBRATION_SCREEN_STAT_DISPLAY_TIME + (CELEBRATION_SCREEN_STAT_WIPE_TIME * iNumValidPlayers))
					sCelebrationData.iEstimatedScreenDuration += (CELEBRATION_SCREEN_STAT_WIPE_TIME * (iNumValidPlayers + 1))
				ENDIF
				
				RESET_NET_TIMER(sCelebrationData.sCelebrationTimer)
				START_NET_TIMER(sCelebrationData.sCelebrationTimer)
				RESET_CELEBRATION_PRE_LOAD(sCelebrationData)
				SET_CELEBRATION_SUMMARY_STAGE(CELEBRATION_STAGE_PLAYING, 2)
				
			ELSE
				
				IF ANIMPOSTFX_IS_RUNNING("DeathFailMPIn")
					IF NOT HAS_NET_TIMER_STARTED(sCelebrationData.timerDeathPostFxDelay)
						START_NET_TIMER(sCelebrationData.timerDeathPostFxDelay)
						PRINTLN("[NETCELEBRATION] HAS_CELEBRATION_WINNER_FINISHED - DeathFailMPIn is running, started timer timerDeathPostFxDelay.")
					ELSE
						IF HAS_NET_TIMER_EXPIRED(sCelebrationData.timerDeathPostFxDelay, 1000)
							PRINTLN("[NETCELEBRATION] HAS_CELEBRATION_WINNER_FINISHED - DeathFailMPIn is running, timer timerDeathPostFxDelay >= 1000. Calling CLEAR_KILL_STRIP_DEATH_EFFECTS.")
							CLEAR_KILL_STRIP_DEATH_EFFECTS()
						ENDIF
					ENDIF	
				ENDIF
				
			ENDIF
		
		BREAK
		
		CASE CELEBRATION_STAGE_PLAYING
			DRAW_CELEBRATION_SCREEN_THIS_FRAME(sCelebrationData, FALSE)
			
			//Detach the cam on the player if they're stationary to stop the weird swings if the player turns slightly.
			IF bLocalPlayerOK
			AND DOES_CAM_EXIST(camCelebrationScreen)
				IF GET_ENTITY_SPEED(LocalPlayerPed) < 0.1
					DETACH_CAM(camCelebrationScreen)
					STOP_CAM_POINTING(camCelebrationScreen)
				ENDIF
			ENDIF
			
			//Once the timer finishes then progress: this depends on how many elements were added to the screen.
//			IF HAS_NET_TIMER_EXPIRED(sCelebrationData.sCelebrationTimer, sCelebrationData.iEstimatedScreenDuration - (CELEBRATION_SCREEN_STAT_WIPE_TIME / 2))
			IF HAS_CELEBRATION_SHARD_FINISHED_FALL_AWAY()	
				ANIMPOSTFX_STOP_ALL()
				PRINTLN("[Survival] - Stopping all Anim Post FX 1")
			
				IF NOT bEndOfMode
					ANIMPOSTFX_PLAY("MP_Celeb_Win_Out", 0, FALSE)
					PRINTLN("[Survival] - playing MP_Celeb_Win, call 3")
					PLAY_SOUND_FRONTEND(-1, "Hit_1", "LONG_PLAYER_SWITCH_SOUNDS")
					START_CELEBRATION_CAMERA_ZOOM_TRANSITION(camCelebrationScreen)
				ELIF bJobPassed
					ANIMPOSTFX_PLAY("MP_Celeb_Win_Out", 0, FALSE)
					PRINTLN("[Survival] - playing MP_Celeb_Win, call 4")
				ELSE
					ANIMPOSTFX_PLAY("MP_Celeb_Lose_Out", 0, FALSE)
				ENDIF
				
				IF bEndOfMode
					LEGACY_CLEANUP_WORLD_PROPS(sRuntimeWorldPropData)
				ENDIF
			
				RESET_NET_TIMER(sCelebrationData.sCelebrationTimer)
				START_NET_TIMER(sCelebrationData.sCelebrationTimer)
			
				SET_CELEBRATION_SUMMARY_STAGE(CELEBRATION_STAGE_END_TRANSITION, 3)
			ENDIF
		BREAK
		
		CASE CELEBRATION_STAGE_END_TRANSITION
			DRAW_CELEBRATION_SCREEN_THIS_FRAME(sCelebrationData, FALSE)

			IF HAS_NET_TIMER_EXPIRED(sCelebrationData.sCelebrationTimer, 1000)
			OR bEndOfMode
				BOOL bReadyToContinue
			
				IF bEndOfMode
					CDEBUG1LN(DEBUG_MISSION, "[Survival] - [NETCELEBRATION] Survival has ended, starting skyswoop")
					
					IF IS_AUDIO_SCENE_ACTIVE("MP_CELEB_SCREEN_SCENE")
						STOP_AUDIO_SCENE("MP_CELEB_SCREEN_SCENE")
						PRINTLN("[Survival] - MP_CELEB_SCREEN_SCENE is active, calling STOP_AUDIO_SCENE(MP_CELEB_SCREEN_SCENE). Call B.")
					ENDIF
					
					IF SET_SKYSWOOP_UP(TRUE, TRUE, FALSE, DEFAULT, TRUE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
						//1632888 - Keep the screen blurry and frozen at the top of the skyswoop (this should be cleaned up when the skyswoop starts descending).
						//Currently commented out as part of 1638857.
						//SET_SKYBLUR_BLURRY() 
						//SET_SKYFREEZE_FROZEN()
					
						bReadyToContinue = TRUE
					ELSE
						IF GET_SKYFREEZE_STAGE() = SKYFREEZE_FROZEN
							IF IS_SWITCH_TO_MULTI_FIRSTPART_FINISHED()
								SET_SKYFREEZE_CLEAR(TRUE)
								SET_SKYBLUR_CLEAR()
								TOGGLE_RENDERPHASES(TRUE)
							ENDIF
						ENDIF
					ENDIF
				ELSE
					CDEBUG1LN(DEBUG_MISSION, "[Survival] - [NETCELEBRATION] Survival hasn't ended, removing scripted celebration cam.")
				
					IF DOES_CAM_EXIST(camCelebrationScreen)
						DESTROY_CAM(camCelebrationScreen, TRUE)
						RENDER_SCRIPT_CAMS(FALSE, FALSE)
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
						SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
					ENDIF
					
					bReadyToContinue = TRUE
				ENDIF				
			
				IF bReadyToContinue
					PRINTLN("[Survival] - [NETCELEBRATION] bReadyToContinue = TRUE.")
					CLEAR_ALL_BIG_MESSAGES()
					STOP_CELEBRATION_SCREEN_ANIM(sCelebrationData, "SUMMARY")
					CLEANUP_CELEBRATION_SCREEN(sCelebrationData)
					SET_CELEBRATION_SCREEN_AS_ACTIVE(FALSE)
					
					IF NOT bEndOfMode
						NET_SET_PLAYER_CONTROL(localPlayer, TRUE)
					ENDIF
					
					SET_CELEBRATION_SUMMARY_STAGE(CELEBRATION_STAGE_FINISHED, 4)
				
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
		
		CASE CELEBRATION_STAGE_FINISHED
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC INT INCREMENT_POSITION_FOR_PLAYLIST_LEADERBOARD(INT iValueToAdjust)
	
	RETURN (iValueToAdjust+1)
	
ENDFUNC

FUNC BOOL IS_INT_WITHIN_PARTICIPANT_RANGE(INT iInt)
	
	IF iInt >= 0
	AND iInt <= (NETWORK_GET_MAX_NUM_PARTICIPANTS()-1)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL DISPLAY_END_OF_WAVE_TEXT(BOOL bEndOfMode, BOOL bPassedFailed)
   	
    INT iCounter, iParticipant, iScore
	BOOL bMoveOn = TRUE
	
	// No weapon wheel during end of wave HUD.
	IF DOES_ENTITY_EXIST(LocalPlayerPed)
		IF NOT IS_ENTITY_DEAD(LocalPlayerPed)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
		ENDIF
	ENDIF
	
	DISABLE_INTERACTION_MENU()
	DISABLE_INTERACTIONS_THIS_FRAME()
	
	SWITCH iEndOfWaveHudStage
		
		CASE 0
			
			IF bEndOfMode
				
				IF NOT sEndBoardData.bPopulatedHudPlayerArray
					
					GET_PLAYER_KILLS(sEndBoardData, bEndOfMode)
					POPULATE_HUD_PLAYER_ARRAY(sEndBoardData, bDoWaveCompleteHudPrints, bEndOfMode)
					sEndBoardData.bPopulatedHudPlayerArray = TRUE
				
				ENDIF
				
				IF sEndBoardData.bPopulatedHudPlayerArray
					
					REPEAT MAX_NUMBER_HORDE_FIGHTING_PARTICIPANTS iCounter
						IF sEndBoardData.hudPlayers[iCounter] != INVALID_PLAYER_INDEX()
						AND NETWORK_IS_PLAYER_A_PARTICIPANT(sEndBoardData.hudPlayers[iCounter])
							IF IS_NET_PLAYER_OK(sEndBoardData.hudPlayers[iCounter], FALSE)
								iParticipant = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(sEndBoardData.hudPlayers[iCounter]))
								IF IS_INT_WITHIN_PARTICIPANT_RANGE(iParticipant)
									IF playerBD[iParticipant].iSurvivalScore <= (-1)
										PRINTLN("[Survival] - DISPLAY_END_OF_WAVE_TEXT - case 0 - particpant ", iParticipant, " has not calculated their survival score yet, bMoveOn = FALSE.")
										bMoveOn = FALSE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDREPEAT
					
					IF IS_BIT_SET(playerBD[iLocalPart].iBitset, BITSET_PLAYER_ABANDONED_HORDE)
						PRINTLN("[Survival] - DISPLAY_END_OF_WAVE_TEXT - case 0 - BITSET_PLAYER_ABANDONED_HORDE is set, bMoveOn = TRUE.")
						bMoveOn = TRUE
					ENDIF
					
					IF bMoveOn
						PRINTLN("[Survival] - DISPLAY_END_OF_WAVE_TEXT - case 0 - DOES_THIS_SPECTATOR_CAM_WANT_TO_LEAVE is TRUE, bMoveOn = TRUE.")
						iEndOfWaveHudStage++
					ENDIF
					
				ENDIF
				
			ELSE
			
				PRINTLN("[Survival] - DISPLAY_END_OF_WAVE_TEXT - case 0 - bEndOfMode = FALSE.")
				iEndOfWaveHudStage++
			
			ENDIF
			
		BREAK
		
		CASE 1
			
			IF NOT sEndBoardData.bPopulatedHudPlayerArray
				
				GET_PLAYER_KILLS(sEndBoardData, bEndOfMode)
				POPULATE_HUD_PLAYER_ARRAY(sEndBoardData, bDoWaveCompleteHudPrints, bEndOfMode)
				sEndBoardData.bPopulatedHudPlayerArray = TRUE
			
			ELSE
				
				IF bEndOfMode
					
					IF NOT IS_BIT_SET(playerBD[iLocalPart].iBitset, BITSET_PLAYER_ABANDONED_HORDE)
					
						REPEAT MAX_NUMBER_HORDE_FIGHTING_PARTICIPANTS iCounter
							IF sEndBoardData.hudPlayers[iCounter] != INVALID_PLAYER_INDEX()
							AND NETWORK_IS_PLAYER_A_PARTICIPANT(sEndBoardData.hudPlayers[iCounter])
								IF IS_NET_PLAYER_OK(sEndBoardData.hudPlayers[iCounter], FALSE)
									
									iScore = 0
									iParticipant = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(sEndBoardData.hudPlayers[iCounter]))
									
									PRINTLN("[Survival] - DISPLAY_END_OF_WAVE_TEXT - checking participant ", iParticipant)
									
									IF IS_INT_WITHIN_PARTICIPANT_RANGE(iParticipant)
										iScore = playerBD[iParticipant].iSurvivalScore
										PRINTLN("[Survival] - DISPLAY_END_OF_WAVE_TEXT - survival score = ", iScore)
									ENDIF
									
									IF iScore > (-1)
										WRITE_MISSION_LB_DATA_TO_GLOBALS(	sEndBoardData.hudPlayers[iCounter],
																			sEndBoardData.iTeam[iCounter],
																			INCREMENT_POSITION_FOR_PLAYLIST_LEADERBOARD(iCounter),
																			-1,
																			-1,
																		    sEndBoardData.iScores[iCounter],
																			sEndBoardData.iDeaths[iCounter],
																			sEndBoardData.iHeadshots[iCounter], 
																			DUMMY_MODEL_FOR_SCRIPT, 
																			MCT_METALLIC, 
																			0, 
																			iScore	)
									ENDIF
									
								ENDIF
							ENDIF
						ENDREPEAT
						
					ENDIF
					
				ENDIF
				
				IF NOT bFinalCelebrationScreenHasDisplayed
					iEndOfWaveHudStage++
					PRINTLN("[Survival] - iEndOfWaveHudStage = ", iEndOfWaveHudStage)
				ENDIF
				
			ENDIF
			
		BREAK
		
		CASE 2			
			PRINTLN("[Survival] - [Music] - called PLAY_SOUND_FRONTEND(MP_WAVE_COMPLETE, HUD_FRONTEND_DEFAULT_SOUNDSET).")
			
			g_b_OnHordeWaveBoard = TRUE
			PRINTLN("[Survival] - DISPLAY_END_OF_WAVE_TEXT - g_b_OnHordeWaveBoard = TRUE")
			iEndOfWaveHudStage++
			PRINTLN("[Survival] - iEndOfWaveHudStage = ", iEndOfWaveHudStage)
			
		BREAK
		
		CASE 3
			IF DID_I_JOIN_MISSION_AS_SPECTATOR()
				IF HAS_CELEBRATION_SUMMARY_FINISHED(sEndBoardData, bPassedFailed, bEndOfMode)
					//Cleanup audio scenes: see B*1642903 for implementation notes
					IF IS_AUDIO_SCENE_ACTIVE("MP_CELEB_SCREEN_SCENE")
						STOP_AUDIO_SCENE("MP_CELEB_SCREEN_SCENE")
						PRINTLN("[Survival] - MP_CELEB_SCREEN_SCENE is active, calling STOP_AUDIO_SCENE(MP_CELEB_SCREEN_SCENE). Call C.")
					ENDIF
				
					RETURN TRUE
				ENDIF
			ENDIF
			IF g_b_OnHordeWaveBoard
			AND NOT bFinalCelebrationScreenHasDisplayed
				IF HAS_CELEBRATION_SUMMARY_FINISHED(sEndBoardData, bPassedFailed, bEndOfMode)
					//Cleanup audio scenes: see B*1642903 for implementation notes
					IF IS_AUDIO_SCENE_ACTIVE("MP_CELEB_SCREEN_SCENE")
						STOP_AUDIO_SCENE("MP_CELEB_SCREEN_SCENE")
						PRINTLN("[Survival] - MP_CELEB_SCREEN_SCENE is active, calling STOP_AUDIO_SCENE(MP_CELEB_SCREEN_SCENE). Call D.")
					ENDIF
				
					g_b_OnHordeWaveBoard = FALSE
					PRINTLN("[Survival] - DISPLAY_END_OF_WAVE_TEXT - g_b_OnHordeWaveBoard = FALSE")
					
					TOGGLE_RENDERPHASES(TRUE)
					IF bEndOfMode
						IF NOT IS_AUDIO_SCENE_ACTIVE("MP_LEADERBOARD_SCENE")
							START_AUDIO_SCENE("MP_LEADERBOARD_SCENE")
						ENDIF
					
						bFinalCelebrationScreenHasDisplayed = TRUE
					ENDIF
					
					RETURN TRUE
				ENDIF
			ELSE
				RETURN TRUE
			ENDIF
		BREAK		
	ENDSWITCH
	
    RETURN FALSE
    
ENDFUNC

FUNC BOOL HAS_EVERYONE_COMPLETED_END_OF_WAVE_HUD()
    
	#IF IS_DEBUG_BUILD
	
		IF bDoAllCompletedWavePrints
		
			BOOL bPartActive
			BOOL bDidInitialFighterStatusSetup
			BOOL bWentStraightToSpecCam
			BOOL bWentSpecCamForOutOfBounds
			BOOL bInStageSCTV
			BOOL bCompletedEndOfWaveHud
			
			PRINTLN("[Survival] - HAS_EVERYONE_COMPLETED_END_OF_WAVE_HUD - ************** participant ", iStaggeredAlldidEndOfWaveHudCount)
			
			bPartActive = NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iStaggeredAlldidEndOfWaveHudCount))
			PRINTLN("[Survival] - HAS_EVERYONE_COMPLETED_END_OF_WAVE_HUD - part ", iStaggeredAlldidEndOfWaveHudCount, ". bPartActive = ", bPartActive)
			
			bDidInitialFighterStatusSetup = IS_BIT_SET(playerBD[iStaggeredAlldidEndOfWaveHudCount].iBitset, BITSET_PLAYER_DID_INITIAL_FIGHTER_STATUS_SETUP)
			PRINTLN("[Survival] - HAS_EVERYONE_COMPLETED_END_OF_WAVE_HUD - part ", iStaggeredAlldidEndOfWaveHudCount, ". bDidInitialFighterStatusSetup = ", bDidInitialFighterStatusSetup)
			
		    bWentStraightToSpecCam = IS_BIT_SET(playerBD[iStaggeredAlldidEndOfWaveHudCount].iBitset, BITSET_PLAYER_I_WENT_STRAIGHT_TO_SPEC_CAM)
			PRINTLN("[Survival] - HAS_EVERYONE_COMPLETED_END_OF_WAVE_HUD - part ", iStaggeredAlldidEndOfWaveHudCount, ". bWentStraightToSpecCam = ", bWentStraightToSpecCam)
			
			bWentSpecCamForOutOfBounds = IS_BIT_SET(playerBD[iStaggeredAlldidEndOfWaveHudCount].iBitset, BITSET_PLAYER_WENT_TO_SPEC_CAM_FOR_OUT_OF_BOUNDS)
			PRINTLN("[Survival] - HAS_EVERYONE_COMPLETED_END_OF_WAVE_HUD - part ", iStaggeredAlldidEndOfWaveHudCount, ". bWentSpecCamForOutOfBounds = ", bWentSpecCamForOutOfBounds)
			
	        bInStageSCTV = GET_CLIENT_HORDE_STAGE(INT_TO_PARTICIPANTINDEX(iStaggeredAlldidEndOfWaveHudCount)) != eHORDESTAGE_SCTV
			PRINTLN("[Survival] - HAS_EVERYONE_COMPLETED_END_OF_WAVE_HUD - part ", iStaggeredAlldidEndOfWaveHudCount, ". bInStageSCTV = ", bInStageSCTV)
			
	        bCompletedEndOfWaveHud = IS_BIT_SET(playerBD[iStaggeredAlldidEndOfWaveHudCount].iBitset, BITSET_PLAYER_I_HAVE_COMPLETED_END_OF_WAVE_HUD)
			PRINTLN("[Survival] - HAS_EVERYONE_COMPLETED_END_OF_WAVE_HUD - part ", iStaggeredAlldidEndOfWaveHudCount, ". bCompletedEndOfWaveHud = ", bCompletedEndOfWaveHud)
			
		ENDIF
	
	#ENDIF
	
    IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iStaggeredAlldidEndOfWaveHudCount))
		IF IS_BIT_SET(playerBD[iStaggeredAlldidEndOfWaveHudCount].iBitset, BITSET_PLAYER_DID_INITIAL_FIGHTER_STATUS_SETUP)
	        IF NOT IS_BIT_SET(playerBD[iStaggeredAlldidEndOfWaveHudCount].iBitset, BITSET_PLAYER_I_WENT_STRAIGHT_TO_SPEC_CAM)
				IF NOT IS_BIT_SET(playerBD[iStaggeredAlldidEndOfWaveHudCount].iBitset, BITSET_PLAYER_WENT_TO_SPEC_CAM_FOR_OUT_OF_BOUNDS)
		            IF GET_CLIENT_HORDE_STAGE(INT_TO_PARTICIPANTINDEX(iStaggeredAlldidEndOfWaveHudCount)) != eHORDESTAGE_SCTV
		                IF NOT IS_BIT_SET(playerBD[iStaggeredAlldidEndOfWaveHudCount].iBitset, BITSET_PLAYER_I_HAVE_COMPLETED_END_OF_WAVE_HUD)
		                    iStaggeredAlldidEndOfWaveHudCount = 0
		                    RETURN FALSE
		                ENDIF
		            ENDIF
				ENDIF
	        ENDIF
		ENDIF
    ENDIF
    
    iStaggeredAlldidEndOfWaveHudCount++
    
    IF iStaggeredAlldidEndOfWaveHudCount >= NETWORK_GET_MAX_NUM_PARTICIPANTS()
        iStaggeredAlldidEndOfWaveHudCount = 0
        RETURN TRUE
    ENDIF
    
    RETURN FALSE
    
ENDFUNC

PROC INIT_LOCAL_WAVE_DATA()
	
	IF IS_LOCAL_BIT_SET(ciLocalBool_bLocalWaveDataSetUp)
		EXIT
	ENDIF
	PRINTLN("INIT_LOCAL_WAVE_DATA - Called - Wave ", serverBD.sWaveData.iWaveCount)
	INT i
	MODEL_NAMES mnModel
	FOR i = 0 TO MAX_SURV_WAVE_DIFF-1
		mnModel = g_FMMC_STRUCT.sSurvivalWaveData.mnPedModel[iWaveToUse][i]
		IF IS_MODEL_AMBIENT_COP_FOR_RP(mnModel)
		OR mnModel = S_M_M_MARINE_01
			PRINTLN("INIT_LOCAL_WAVE_DATA - Model: ", GET_MODEL_NAME_FOR_DEBUG(mnModel))
			SET_LOCAL_BIT(ciLocalBool_bInvalidWaveForKillRP)
			BREAKLOOP
		ENDIF
	ENDFOR
	
	
	SET_LOCAL_BIT(ciLocalBool_bLocalWaveDataSetUp)
ENDPROC



