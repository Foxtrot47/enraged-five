USING "FM_Survival_Controller_Using.sch"
USING "FM_Survival_Controller_Utility.sch"
USING "FM_Survival_Controller_Ending.sch"
USING "FM_Survival_Controller_Waves.sch"
USING "FM_Survival_Controller_Vehicles.sch"
USING "FM_Survival_Controller_Objects.sch"
USING "net_spectator_cam.sch"

// *********************************** 	\\
// *********************************** 	\\
// 										\\
// PARTICIPANT LOOP                   	\\
// 										\\
// *********************************** 	\\
// *********************************** 	\\

PROC PRE_PROCESS_PARTICIPANT_LOOP()
    fFurthestTargetDist = 50.0
	fPartDis = 0
	SET_LOCAL_BIT(ciLocalBool_bAllPlayersLostAllLivesFlag, FALSE)
	iPlayerIsNetOkBitset = 0
	iParticpantIsDeadBitset = 0
ENDPROC

TEXT_LABEL_23 tl23_Names[NUM_NETWORK_PLAYERS]

PROC PROCESS_PARTICPANT_LOOP()

    INT i
	PLAYER_INDEX playerId
	PARTICIPANT_INDEX partId
	PED_INDEX pedID
	INT iPlayerId
	BOOL bPartActive
	
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
		
		// Sort IDs and net player ok checks.
		partId = INT_TO_NATIVE(PARTICIPANT_INDEX, i)
		bPartActive = NETWORK_IS_PARTICIPANT_ACTIVE(partId)
		IF bPartActive
			playerId = NETWORK_GET_PLAYER_INDEX(partId)
			iPlayerId = NATIVE_TO_INT(playerId)
			IF IS_NET_PLAYER_OK(playerId, FALSE)
				tl23_Names[i] = GET_PLAYER_NAME(playerId)
				pedID = GET_PLAYER_PED(playerId)
				SET_BIT(iPlayerIsNetOkBitset, iPlayerId)
				IF IS_ENTITY_DEAD(pedID)
					SET_BIT(iParticpantIsDeadBitset, i)
				ENDIF
			ENDIF
			MAINTAIN_HAVE_ALL_PARTICIPANTS_LOST_ALL_LIVES_FLAG(i, iPlayerId, partId)
		ENDIF
		
		// Radar zoom. 
    	CALCULATE_FURTHEST_PLAYER_DISTANCE(i)
		
		// Ticker.
		PROCESS_PARTICPANT_ABANDONDED_SURVIVAL_TICKER(i, partId, bPartActive)
		
	ENDREPEAT
	
ENDPROC

PROC POST_PROCESS_PARTICIPANT_LOOP()
	UPDATE_SCTV_TICKER_SCORE_BASED_ON_WHO_IM_SPECTATING_HORDE_LEADERBOARD_STRUCT(HordeLeaderboard, tl23_Names)
ENDPROC

PROC MAINTAIN_PARTICPANT_LOOP()
    PRE_PROCESS_PARTICIPANT_LOOP()
    PROCESS_PARTICPANT_LOOP()
    POST_PROCESS_PARTICIPANT_LOOP()
ENDPROC

FUNC INT GET_FREE_SLOT_IN_FIGHTING_PARTICIPANT_LIST()
    INT i
    REPEAT MAX_NUMBER_HORDE_FIGHTING_PARTICIPANTS i
        IF serverBD.sFightingParticipantsList.iParticipant[i] = (-1)
            RETURN i
        ENDIF
    ENDREPEAT
    
    RETURN (-1)
    
ENDFUNC

PROC ADD_PARTICIPANT_TO_FIGHTING_PARTICIPANT_LIST(INT iPart, INT iSlot, BOOL bFullfighter)
	
	// Add them to the list and update the list data.
	
    PRINTLN("[Survival] - [Participant List] - adding participant ", iPart, " to fighting participant list slot ", iSlot)
    
	IF NOT IS_BIT_SET(serverBD.sFightingParticipantsList.iPotentialFighterBitset, iPart)
		
		PRINTLN("[Survival] - [Participant List] - bit ", iPart, " of sFightingParticipantsList.iPotentialFighterBitset not set.")
		
	    serverBD.sFightingParticipantsList.iParticipant[iSlot] = iPart
	    SET_BIT(serverBD.sFightingParticipantsList.iAddedBitset, iPart)
		
		PRINTLN("[Survival] - [Participant List] - sFightingParticipantsList.iParticipant[iSlot] = ", iPart)
		PRINTLN("[Survival] - [Participant List] - setting bit ", iPart, " of sFightingParticipantsList.iAddedBitset.")
		
	ENDIF
	
	// If the participant is a straight to spectator player.
   	IF NOT bFullfighter
		PRINTLN("[Survival] - [Participant List] - bFullfighter = FALSE, setting bit ", iPart, " of sFightingParticipantsList.iPotentialFighterBitset.")
    	SET_BIT(serverBD.sFightingParticipantsList.iPotentialFighterBitset, iPart)
	ELSE
		PRINTLN("[Survival] - [Participant List] - bFullfighter = TRUE, clearing bit ", iPart, " of sFightingParticipantsList.iPotentialFighterBitset.")
		CLEAR_BIT(serverBD.sFightingParticipantsList.iPotentialFighterBitset, iPart)
	ENDIF
	
ENDPROC

PROC INITIALISE_HORDE_FIGHTING_PARTICIPANT_LIST_DATA()
    
    INT i
    INT iSlot
    
    REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
        IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
           	
			 IF NOT IS_PLAYER_SCTV(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i)))
				IF NOT IS_PLAYER_SPECTATING(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i)))
				
		            IF NOT IS_BIT_SET(serverBD.sFightingParticipantsList.iAddedBitset, i)
		                
		                iSlot = GET_FREE_SLOT_IN_FIGHTING_PARTICIPANT_LIST()
		                
		                IF iSlot != (-1)
		            
		                    PRINTLN("[Survival] - [Participant List] - adding participant ", i, " to fighting participant list slot ", iSlot)
		                    
		                    ADD_PARTICIPANT_TO_FIGHTING_PARTICIPANT_LIST(	i, iSlot, TRUE)
		                    
		                    PRINTLN("[Survival] - [Participant List] - iNumFightingParticipants now = ", serverBD.sFightingParticipantsList.iNumFightingParticipants)
		                    
		                ENDIF
		            ENDIF
				ENDIF
			ENDIF
        ENDIF
    ENDREPEAT
    
    SET_BIT(serverBD.iBitset, BITSET_SERVER_FIGHTING_PART_LIST_INITIALISED)
    
ENDPROC

FUNC BOOL IS_PARTICIPANT_IN_VALID_STAGE_FOR_ADD_TO_PART_LIST_CHECK(PARTICIPANT_INDEX partId, BOOL bCheckingPotentialFighter = FALSE)
	
	SWITCH GET_CLIENT_HORDE_STAGE(partId) 
		CASE eHORDESTAGE_JIP_IN 					RETURN FALSE
	    CASE eHORDESTAGE_SETUP_MISSION 				RETURN FALSE
	    CASE eHORDESTAGE_START_FADE_IN 				RETURN FALSE
	    CASE eHORDESTAGE_FIGHTING 					RETURN TRUE
	    CASE eHORDESTAGE_PASSED_WAVE 				RETURN TRUE
	    CASE eHORDESTAGE_DELAY_BEFORE_NEXT_WAVE		RETURN TRUE
	    CASE eHORDESTAGE_SPECTATING 				
			IF NOT bCheckingPotentialFighter
				RETURN FALSE
			ELSE
				RETURN TRUE
			ENDIF
		BREAK
	    CASE eHORDESTAGE_SCTV 						RETURN FALSE
	    CASE eHORDESTAGE_REWARDS_AND_FEEDACK 		RETURN FALSE
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

PROC MAINTAIN_NUM_FIGHTING_PARTICIPANTS()
	
	INT i
	
	#IF IS_DEBUG_BUILD
	INT iBeforeLoopValue = serverBD.sFightingParticipantsList.iNumFightingParticipants
	INT iBeforeLoopValue2 = serverBD.sFightingParticipantsList.iNumPotentialfightingParticipants
	#ENDIF
	
	serverBD.sFightingParticipantsList.iNumFightingParticipants = 0
	serverBD.sFightingParticipantsList.iNumPotentialfightingParticipants = 0
	
	REPEAT MAX_NUMBER_HORDE_FIGHTING_PARTICIPANTS  i
		IF serverBD.sFightingParticipantsList.iParticipant[i] >= 0
			IF IS_BIT_SET(serverBD.sFightingParticipantsList.iPotentialFighterBitset, serverBD.sFightingParticipantsList.iParticipant[i])
				serverBD.sFightingParticipantsList.iNumPotentialfightingParticipants++
			ELSE
				serverBD.sFightingParticipantsList.iNumFightingParticipants++
			ENDIF
		ENDIF
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
	IF iBeforeLoopValue != serverBD.sFightingParticipantsList.iNumFightingParticipants
		PRINTLN("[Survival] - iNumFightingParticipants changed to ", serverBD.sFightingParticipantsList.iNumFightingParticipants)
	ENDIF
	IF iBeforeLoopValue2 != serverBD.sFightingParticipantsList.iNumPotentialfightingParticipants
		PRINTLN("[Survival] - iNumPotentialfightingParticipants changed to ", serverBD.sFightingParticipantsList.iNumPotentialfightingParticipants)
	ENDIF
	#ENDIF
	
ENDPROC

PROC MAINTAIN_FIGHTING_PARTICIPANTS_LIST()
	
	INT iSlot, iListIndex, i
	
	// Initialise the list.
    IF NOT IS_BIT_SET(serverBD.iBitset, BITSET_SERVER_FIGHTING_PART_LIST_INITIALISED)
      	
		IF GET_SERVER_HORDE_STAGE() = eHORDESTAGE_DELAY_BEFORE_NEXT_WAVE 
        	INITIALISE_HORDE_FIGHTING_PARTICIPANT_LIST_DATA()
      	ENDIF
		
    ELSE
		
		REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
			
			// If particpant is active and not an sctv participant.
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
				IF IS_BIT_SET(playerBD[i].iBitset, BITSET_PLAYER_DID_INITIAL_FIGHTER_STATUS_SETUP)
					IF bPrintPartListInfo 
						PRINTLN("[Survival] - [Participant List] - participant = ", i, ", participant active.")
					ENDIF
					
					IF NOT IS_PLAYER_SCTV(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i)))
						
						IF bPrintPartListInfo PRINTLN("[Survival] - [Participant List] - participant = ", i, " not an sctv participant.") ENDIF
						
						IF NOT IS_PLAYER_SPECTATING(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i)))
						AND IS_PARTICIPANT_IN_VALID_STAGE_FOR_ADD_TO_PART_LIST_CHECK(INT_TO_PARTICIPANTINDEX(i))
							
							// If we haven't added them to the list yet.
							IF NOT IS_BIT_SET(serverBD.sFightingParticipantsList.iAddedBitset, i)
								
								IF bPrintPartListInfo PRINTLN("[Survival] - [Participant List] - participant = ", i, " not been added to list.") ENDIF
								
								// If we have room on the list, add participant to it.
								IF (serverBD.sFightingParticipantsList.iNumFightingParticipants + serverBD.sFightingParticipantsList.iNumPotentialfightingParticipants) < MAX_NUMBER_HORDE_FIGHTING_PARTICIPANTS
									
									IF bPrintPartListInfo PRINTLN("[Survival] - [Participant List] - participant = ", i, ", room on list.") ENDIF
									
									// Get a free slot for them.
								    iSlot = GET_FREE_SLOT_IN_FIGHTING_PARTICIPANT_LIST()
								    
									// If we successfully got a free slot.
								    IF iSlot != (-1)
										ADD_PARTICIPANT_TO_FIGHTING_PARTICIPANT_LIST(i, iSlot, TRUE)
										IF bPrintPartListInfo PRINTLN("[Survival] - [Participant List] - participant = ", i, ", added to list.") ENDIF
									ENDIF
									
								ENDIF
								
							// If participant is already on the list.
							ELSE
								
								IF bPrintPartListInfo PRINTLN("[Survival] - [Participant List] - participant = ", i, ", already on list.") ENDIF
								
								// If participant is only a potential fighter.
								IF IS_BIT_SET(serverBD.sFightingParticipantsList.iPotentialFighterBitset, i)
									
									IF bPrintPartListInfo
										PRINTLN("[Survival] - [Participant List] - participant = ", i, ", is a potential fighter.")
									ENDIF
									
									// If the participant is no longer a straight to spectator participant.
									IF NOT IS_BIT_SET(playerBD[i].iBitset, BITSET_PLAYER_I_WENT_STRAIGHT_TO_SPEC_CAM)
										
										IF bPrintPartListInfo 
											PRINTLN("[Survival] - [Participant List] - participant = ", i, ", not a straight to spec cam participant.")
										ENDIF
										
										// Set them as not just a potential fighter (give them full fighter status next time they are found in the loop!).
										ADD_PARTICIPANT_TO_FIGHTING_PARTICIPANT_LIST(i, iSlot, TRUE)
									ENDIF
								ENDIF
							ENDIF
						ELSE
							
							IF IS_BIT_SET(playerBD[i].iBitset, BITSET_PLAYER_I_WENT_STRAIGHT_TO_SPEC_CAM)
							
								// If we haven't added them to the list yet.
								IF NOT IS_BIT_SET(serverBD.sFightingParticipantsList.iAddedBitset, i)
									
									// If we have room on the list, add participant to it.
									IF (serverBD.sFightingParticipantsList.iNumFightingParticipants + serverBD.sFightingParticipantsList.iNumPotentialfightingParticipants) < MAX_NUMBER_HORDE_FIGHTING_PARTICIPANTS
										
										// Get a free slot for them.
									    iSlot = GET_FREE_SLOT_IN_FIGHTING_PARTICIPANT_LIST()
										
										// If we successfully got a free slot.
									    IF iSlot != (-1)
											ADD_PARTICIPANT_TO_FIGHTING_PARTICIPANT_LIST(	i, iSlot, FALSE)
											IF bPrintPartListInfo
												PRINTLN("[Survival] - [Participant List] - participant = ", i, ", added to list.")
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			// If the participant is no longer active.
			ELSE
				
				// If the participant was added to the list.
				IF IS_BIT_SET(serverBD.sFightingParticipantsList.iAddedBitset, i)
					
					// Loop through the list.
					REPEAT MAX_NUMBER_HORDE_FIGHTING_PARTICIPANTS iListIndex
						
						// If we have found the participant in the list.
						IF serverBD.sFightingParticipantsList.iParticipant[iListIndex] = i
				        	
							// Remove them from the list.
							PRINTLN("[Survival] - [Participant List] - removing participant ", i, " from fighting participant list slot ", iListIndex)
				            
				            serverBD.sFightingParticipantsList.iParticipant[iListIndex] = (-1)
				            CLEAR_BIT(serverBD.sFightingParticipantsList.iAddedBitset, i)
				            CLEAR_BIT(serverBD.sFightingParticipantsList.iPotentialFighterBitset, i)
							
				            IF bPrintPartListInfo
								PRINTLN("[Survival] - [Participant List] - iNumFightingParticipants now = ", serverBD.sFightingParticipantsList.iNumFightingParticipants)
							ENDIF
				        	
						ENDIF
					ENDREPEAT
				ENDIF
			ENDIF
		ENDREPEAT		
	ENDIF
	
ENDPROC

////////
///    Spectator Start
////////

FUNC BOOL GET_AM_I_IN_FIGHTING_PARTICIPANTS_LIST_AS_POTENTIAL_FIGHTER()
    INT i
    REPEAT MAX_NUMBER_HORDE_FIGHTING_PARTICIPANTS i
        IF iLocalPart = serverBD.sFightingParticipantsList.iParticipant[i]
			IF IS_BIT_SET(serverBD.sFightingParticipantsList.iPotentialFighterBitset, serverBD.sFightingParticipantsList.iParticipant[i])
            	RETURN TRUE
			ENDIF
        ENDIF
    ENDREPEAT
    
    RETURN FALSE
    
ENDFUNC

FUNC BOOL AM_I_STRAIGHT_TO_SPECTATOR_PLAYER_THAT_SHOULD_JOIN_FIGHT()
	IF GET_SERVER_HORDE_STAGE() = eHORDESTAGE_DELAY_BEFORE_NEXT_WAVE
		IF GET_AM_I_IN_FIGHTING_PARTICIPANTS_LIST_AS_POTENTIAL_FIGHTER()
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL AM_I_ABANDONED_HORDE_ON_PLAYLIST_PLAYER_THAT_SHOULD_JOIN_FIGHT()
	IF GET_SERVER_HORDE_STAGE() = eHORDESTAGE_DELAY_BEFORE_NEXT_WAVE
		IF IS_BIT_SET(playerBD[iLocalPart].iBitset, BITSET_PLAYER_WENT_TO_SPEC_CAM_FOR_OUT_OF_BOUNDS)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC LEAVE_SPECTATOR_STAGE_AND_JOIN_FIGHT(INT iDebugCall)

    PRINTLN("[Survival] - LEAVE_SPECTATOR_STAGE_AND_JOIN_FIGHT - being called - iDebugCall =", iDebugCall)
	
	INT iFlags = DSCF_NONE
	
    IF iDebugCall = iDebugCall
        iDebugCall = iDebugCall
    ENDIF
	
    IF IS_THIS_SPECTATOR_CAM_ACTIVE(g_BossSpecData.specCamData)
        DEACTIVATE_SPECTATOR_CAM(g_BossSpecData, iFlags)
    ENDIF
	
	IF NOT DID_I_JOIN_MISSION_AS_SPECTATOR()
		SET_LOCAL_BIT(ciLocalBool_bRespawningBetweenWaves)
	ENDIF
	
	CLEAR_I_JOIN_MISSION_AS_SPECTATOR()
	
	CLEAR_STRAIGHT_TO_SPECTATOR_PLAYER()
	
	RESET_RESPAWNING_DATA_CLIENT(0, AFTERLIFE_SET_SPECTATORCAM)
	SETUP_SURVIVAL_PLAYER_SPAWN_POSITION(FALSE)
    CLEAR_LOCAL_BIT(ciLocalBool_bGotWaveProgressLabel)
	
	DISPLAY_RADAR(TRUE)
	
	HIDE_PLAYER(FALSE)
	bDoUnhide = TRUE
	PRINTLN("[Survival] - bDoUnhide = TRUE")
	
	IF NOT IS_PLAYER_SCTV(localPlayer)
		SET_PLAYER_TEAM(localPlayer, 0)
	ENDIF
	
	ANIMPOSTFX_STOP_ALL()
	PRINTLN("[Survival] - Stopping all Anim Post FX 5")
	PRINTLN("[Survival] - cleared post fx - ANIMPOSTFX_STOP_ALL()")
	
	NETWORK_OVERRIDE_TEAM_RESTRICTIONS(0, TRUE)
	
	CLEAR_SPECTATOR_DOING_LEGITIMATE_SKYSWOOP_UP()
	PRINTLN("[Survival] - calling CLEAR_SPECTATOR_DOING_LEGITIMATE_SKYSWOOP_UP() to fix bug url:bugstar:2371909")
		
    SET_CLIENT_HORDE_STAGE(GET_SERVER_HORDE_STAGE())
    
ENDPROC

FUNC BOOL FIND_PREFERRED_SPECTATOR_TARGET(INT &iTarget)
	INT iPart			
	FOR iPart = 0 TO (MAX_NUM_MC_PLAYERS - 1)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPart))
			PLAYER_INDEX tempPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPart))
			IF IS_NET_PLAYER_OK(tempPlayer)
			AND NOT IS_MP_DECORATOR_BIT_SET(tempPlayer, MP_DECORATOR_BS_INVALID_TO_SPECTATE)
				SET_BIT(MPSpecGlobals.SCTVData.iBitSet, SCTV_BIT_ALLOW_SPECTATING_RIVAL_TEAMS)
				iTarget = NATIVE_TO_INT(tempPlayer)
				PRINTLN("[LM][Survival_Spec][SET_EARLY_END_SURVIVAL_SPECTATE] - FIND_PREFERRED_SPECTATOR_TARGET - SCTV_BIT_ALLOW_SPECTATING_RIVAL_TEAMS.")				
				RETURN TRUE
			ENDIF
		ENDIF
	ENDFOR
	RETURN FALSE
ENDFUNC

PROC PROCESS_eHORDESTAGE_SPECTATING_CLIENT()

	IF playerBD[iLocalPart].eSpectatorState < ci_SpectatorState_CLEANUP
	AND playerBD[iLocalPart].eSpectatorState != ci_SpectatorState_MAINTAIN
	AND playerBD[iLocalPart].eSpectatorState != ci_SpectatorState_REJOINING
	AND GET_SERVER_HORDE_STAGE() = eHORDESTAGE_REWARDS_AND_FEEDACK
		PRINTLN("[LM][Survival_Spec][PROCESS_EARLY_END_SURVIVAL_SPECTATORS] - Forcing to cleanup (eHORDESTAGE_REWARDS_AND_FEEDACK)")
		SET_BIT(iSpectatorBitset, ci_SpectatorBS_Ending)
		SETUP_SURVIVAL_PLAYER_SPAWN_POSITION(FALSE)
		SET_EARLY_END_SURVIVAL_SPECTATE(ci_SpectatorState_CLEANUP)
		
	ENDIF
	
	IF NOT IS_THIS_SPECTATOR_CAM_ACTIVE(g_BossSpecData.specCamData)
	AND (GET_END_HORDE_REASON() = eENDHORDEREASON_ALL_PLAYERS_DEAD OR g_BossSpecData.specCamData.bDoTransition OR (GET_END_HORDE_REASON() = eENDHORDEREASON_BEAT_ALL_WAVES AND IS_THIS_ENDLESS_WAVES()))
		IF IS_VALID_INTERIOR(sInterior.iInteriorIndex)
			RETAIN_ENTITY_IN_INTERIOR(localPlayerPed, sInterior.iInteriorIndex)
			PRINTLN("[LM][Survival_Spec][PROCESS_EARLY_END_SURVIVAL_SPECTATORS] - PROCESS_eHORDESTAGE_SPECTATING_CLIENT - Setting to be in interior")
		ENDIF
		VECTOR vTempPos
		FLOAT fZ, fPlayerZ
		vTempPos = GET_ENTITY_COORDS(localPlayerPed)
		fPlayerZ = vTempPos.z
		IF GET_GROUND_Z_FOR_3D_COORD(vTempPos,fZ)
			vTempPos.z = fZ
			PRINTLN("PROCESS_EARLY_END_SURVIVAL_SPECTATORS - vTempPos.z: ", vTempPos.z, " fZ: ",fZ," fPlayerZ(",fPlayerZ,") - fZ = ", fPlayerZ - fZ )
			IF (fPlayerZ - fZ) > 10
			AND playerBD[iLocalPart].sPlayerData.iPartInTeam != -1
				SET_ENTITY_COORDS(localPlayerPed, serverBD.vStartPos[playerBD[iLocalPart].sPlayerData.iPartInTeam])
				PRINTLN("PROCESS_EARLY_END_SURVIVAL_SPECTATORS - ci_SpectatorState_CLEANUP - Setting player to their start position")
			ELIF (fPlayerZ - fZ) > 0.75
				SET_ENTITY_COORDS(localPlayerPed, vTempPos)
				PRINTLN("PROCESS_EARLY_END_SURVIVAL_SPECTATORS - ci_SpectatorState_CLEANUP - Setting player on ground")
			ENDIF
		ENDIF
	ENDIF
	
	
	INT i, j
	
	SWITCH playerBD[iLocalPart].eSpectatorState
		CASE ci_SpectatorState_IDLE
			IF GET_FRAME_COUNT() % 60 = 0
				PRINTLN("[LM][Survival_Spec][PROCESS_EARLY_END_SURVIVAL_SPECTATORS] - Processing ci_SpectatorState_IDLE")
			ENDIF
		BREAK
		CASE ci_SpectatorState_WAIT
			PRINTLN("[LM][Survival_Spec][PROCESS_EARLY_END_SURVIVAL_SPECTATORS] - Processing ci_SpectatorState_WAIT")
			
			IF NOT HAS_NET_TIMER_STARTED(tdSpectatorStartTimer)
				PRINTLN("[LM][Survival_Spec][PROCESS_EARLY_END_SURVIVAL_SPECTATORS] - Starting Timer and Setting to AFTERLIFE_SET_SPECTATORCAM and AFTERLIFE_SET_SPECTATORCAM_AFTER_KILLSTRIP.")
				START_NET_TIMER(tdSpectatorStartTimer)
				g_bMissionHideRespawnBar = TRUE
				SET_PLAYER_RESPAWN_IN_VEHICLE(FALSE)
				SET_GAME_STATE_ON_DEATH(AFTERLIFE_SET_SPECTATORCAM_AFTER_KILLSTRIP)
				SET_BIT(g_iBS1_Mission, ciBS1_Mission_HoldUpRespawn)
				DISABLE_SPECTATOR_FADES_DURING_THIS_EVENT(TRUE)
				BLOCK_SPECTATOR_COPYING_TARGET_FADES(TRUE)
			ENDIF			
			
			IF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdSpectatorStartTimer, 3000)
				IF NOT IS_SCREEN_FADING_OUT()
				AND NOT IS_SCREEN_FADED_OUT()
					PRINTLN("[LM][Survival_Spec][PROCESS_EARLY_END_SURVIVAL_SPECTATORS] - Fading out.")
					DO_SCREEN_FADE_OUT(500)
				ELSE
					IF IS_SCREEN_FADED_OUT()
						PRINTLN("[LM][Survival_Spec][PROCESS_EARLY_END_SURVIVAL_SPECTATORS] - Fade complete.")
						SET_BIT(MPSpecGlobals.SCTVData.iBitSet, SCTV_BIT_ALLOW_SPECTATING_RIVAL_TEAMS)
						MPSpecGlobals.iPreferredSpectatorPlayerID = -1
						IF NOT IS_MP_DECORATOR_BIT_SET(LocalPlayer, MP_DECORATOR_BS_INVALID_TO_SPECTATE)
							PRINTLN("[LM][Survival_Spec][PROCESS_EARLY_END_SURVIVAL_SPECTATORS] - Setting as invalid to spectate!")
							SET_MP_DECORATOR_BIT(LocalPlayer, MP_DECORATOR_BS_INVALID_TO_SPECTATE)
						ENDIF
						SET_BIT(iSpectatorBitset, ci_SpectatorBS_RequiresInit)
						RESET_NET_TIMER(tdSpectatorStartTimer)
						SET_EARLY_END_SURVIVAL_SPECTATE(ci_SpectatorState_INIT)
					ELSE
						PRINTLN("[LM][Survival_Spec][PROCESS_EARLY_END_SURVIVAL_SPECTATORS] - Waiting for fade out.")
					ENDIF
				ENDIF
			ELSE
				PRINTLN("[LM][Survival_Spec][PROCESS_EARLY_END_SURVIVAL_SPECTATORS] - Waiting for timer to leave.")
			ENDIF		
		BREAK
		
		CASE ci_SpectatorState_INIT
			PRINTLN("[LM][Survival_Spec][PROCESS_EARLY_END_SURVIVAL_SPECTATORS] - Processing ci_SpectatorState_INIT")
			
			REPLAY_PREVENT_RECORDING_THIS_FRAME()
			
			IF IS_BIT_SET(iSpectatorBitset, ci_SpectatorBS_RequiresInit)
				CLEANUP_KILL_STRIP()
				CLEAR_KILL_STRIP_DEATH_EFFECTS()
				CLEAR_ALL_BIG_MESSAGES()
				HIDE_MY_PLAYER_BLIP(TRUE, TRUE)
				SET_PLAYER_WANTED_LEVEL(LocalPlayer, 0)
				SET_PLAYER_WANTED_LEVEL_NOW(LocalPlayer)
				g_bMissionRemovedWanted = TRUE	
				IF IS_PHONE_ONSCREEN()
			        HANG_UP_AND_PUT_AWAY_PHONE(TRUE)
			    ENDIF
				SET_ENTITY_VISIBLE(localPlayerPed, FALSE)
				SET_PLAYER_INVINCIBLE(LocalPlayer, TRUE)
				FREEZE_ENTITY_POSITION(localPlayerPed, TRUE)
				IF NOT GET_ENTITY_COLLISION_DISABLED(localPlayerPed)
					SET_ENTITY_COLLISION(localPlayerPed, FALSE)
					PRINTLN("[LM][Survival_Spec][PROCESS_EARLY_END_SURVIVAL_SPECTATORS] - Disabled collision for local player as spectator")
				ENDIF				
				g_bEndOfMissionCleanUpHUD = FALSE				
				NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE, NSPC_SET_INVISIBLE | NSPC_NO_COLLISION | NSPC_FREEZE_POSITION)								
				IF IS_PED_INJURED(PLAYER_PED_ID())
					NETWORK_RESURRECT_LOCAL_PLAYER(GET_ENTITY_COORDS(localPlayerPed), 0.0, 0, TRUE, FALSE)
				ENDIF
				
				LocalPlayerPed = PLAYER_PED_ID()
				
				SET_ENTITY_VISIBLE(localPlayerPed, FALSE)
				SET_PLAYER_INVINCIBLE(LocalPlayer, TRUE)
				FREEZE_ENTITY_POSITION(localPlayerPed, TRUE)
				
				CLEAR_BIT(iSpectatorBitset, ci_SpectatorBS_RequiresInit)	
			ENDIF
			
			IF FIND_PREFERRED_SPECTATOR_TARGET(MPSpecGlobals.iPreferredSpectatorPlayerID)
				PLAYER_INDEX piPlayerTarget				
				PED_INDEX pedPlayerTarget
				
				piPlayerTarget = INT_TO_NATIVE(PLAYER_INDEX, MPSpecGlobals.iPreferredSpectatorPlayerID)
				pedPlayerTarget = GET_PLAYER_PED(piPlayerTarget)
				
				IF NETWORK_IS_PLAYER_ACTIVE(piPlayerTarget)
					PRINTLN("[LM][Survival_Spec][PROCESS_EARLY_END_SURVIVAL_SPECTATORS] - iPreferredSpectatorPlayerID: ", MPSpecGlobals.iPreferredSpectatorPlayerID, " Player Target: ", GET_PLAYER_NAME(piPlayerTarget))
				ELSE
					PRINTLN("[LM][Survival_Spec][PROCESS_EARLY_END_SURVIVAL_SPECTATORS] - iPreferredSpectatorPlayerID: ", MPSpecGlobals.iPreferredSpectatorPlayerID, " Player Target: NONE")
				ENDIF
				
				DISABLE_SPECTATOR_FADES_DURING_THIS_EVENT(TRUE)
				
				CLEAR_BIT(g_BossSpecData.specCamData.iBitSet, SPEC_CAM_BS_DEACTIVATED_DUE_TO_SKYSWOOP_UP)
				
				ACTIVATE_SPECTATOR_CAM(g_BossSpecData, SPEC_MODE_PLAYERS_STAY_DEAD_EVENT, pedPlayerTarget, ASCF_HIDE_LOCAL_PLAYER | ASCF_QUIT_SCREEN)
				
				SET_EARLY_END_SURVIVAL_SPECTATE(ci_SpectatorState_SET_TO_SPECTATOR)
			ELSE
				PRINTLN("[LM][Survival_Spec][PROCESS_EARLY_END_SURVIVAL_SPECTATORS] - Cannot find a valid spectator target yet.")
			ENDIF
		BREAK
		
		CASE ci_SpectatorState_SET_TO_SPECTATOR
			PRINTLN("[LM][Survival_Spec][PROCESS_EARLY_END_SURVIVAL_SPECTATORS] - Processing ci_SpectatorState_SET_TO_SPECTATOR")
			
			REPLAY_PREVENT_RECORDING_THIS_FRAME()			
			
			IF IS_THIS_SPECTATOR_CAM_RUNNING(g_BossSpecData.specCamData)
			AND IS_BIT_SET(g_BossSpecData.specHUDData.iBitset, SPEC_HUD_BS_ACTIVE)
				NETWORK_SET_OVERRIDE_SPECTATOR_MODE(TRUE)
				NETWORK_OVERRIDE_SEND_RESTRICTIONS_ALL(TRUE)
				NETWORK_OVERRIDE_RECEIVE_RESTRICTIONS_ALL(TRUE)
				NETWORK_SET_SPECTATOR_TO_NON_SPECTATOR_TEXT_CHAT(TRUE)
				
				HIDE_SPECTATOR_BUTTONS(FALSE)
				HIDE_SPECTATOR_HUD(FALSE)
				SET_BIG_MESSAGE_ENABLED_WHEN_SPECTATING(TRUE)
				ENABLE_SPECTATOR_FADES() //  to make sure target switch works.
				BLOCK_SPECTATOR_COPYING_TARGET_FADES(FALSE)
				DO_SCREEN_FADE_IN(300)
				SET_EARLY_END_SURVIVAL_SPECTATE(ci_SpectatorState_MAINTAIN)
			ELSE
				PRINTLN("[LM][Survival_Spec][PROCESS_EARLY_END_SURVIVAL_SPECTATORS] - Waiting for Spectator Cam to be running.")
			ENDIF
		BREAK
		
		CASE ci_SpectatorState_MAINTAIN
			PRINTLN("[LM][Survival_Spec][PROCESS_EARLY_END_SURVIVAL_SPECTATORS] - Processing ci_SpectatorState_MAINTAIN")
			
			IF GET_SPECTATOR_HUD_STAGE(g_BossSpecData.specHUDData) = SPEC_HUD_STAGE_QUIT_YES_NO
				IF NOT IS_BIT_SET(iSpectatorBitset, ci_SpectatorBS_QuitFadedOut)
					SET_BIT(iSpectatorBitset, ci_SpectatorBS_QuitFadedOut)
					//DO_SCREEN_FADE_OUT(0)
					TOGGLE_PAUSED_RENDERPHASES(FALSE)
					PRINTLN("[LM][Survival_Spec][PROCESS_EARLY_END_SURVIVAL_SPECTATORS] - Fade Out (Quitting)")
				ENDIF
			ELSE				
				IF IS_BIT_SET(iSpectatorBitset, ci_SpectatorBS_QuitFadedOut)
					CLEAR_BIT(iSpectatorBitset, ci_SpectatorBS_QuitFadedOut)
					//DO_SCREEN_FADE_IN(0)
					TOGGLE_PAUSED_RENDERPHASES(TRUE)
					PRINTLN("[LM][Survival_Spec][PROCESS_EARLY_END_SURVIVAL_SPECTATORS] - Fade In (Not Quitting)")
				ENDIF
			ENDIF
			
		    BOOL bGoToRewardsAndFeedback
			
		    // Process ped actions.
		    PROCESS_SQUAD_ENEMY_PED_BODIES()
		    
		    // Helicopters.
		    REPEAT MAX_NUM_SURVIVAL_HELIS i
		      	PROCESS_HELI_BODY(serverBD_Enemies_V.sSurvivalHeli[i], i)
		    ENDREPEAT
		    
			// Bikes.
			REPEAT MAX_NUM_LAND_VEHICLES i
				REPEAT NUM_LAND_VEH_PEDS j
					UPDATE_ENEMY_NET_PED_BLIP(	serverBD_Enemies_V.sSurvivalLandVehicle[i].sPed[j].netId, 
												structLandVehicleEnemyBlip[i][j], 
												DEFAULT,
												FALSE, 
												FORCE_ENEMY_BLIPS_ON() )
					PROCESS_LAND_VEHICLE_BODY(serverBD_Enemies_V.sSurvivalLandVehicle[i], j, i)
				ENDREPEAT
			ENDREPEAT
			
		    // Process pickups
		    PROCESS_PICKUP_LOGIC()
			
			IF IS_BIT_SET(playerBD[iLocalPart].iBitset, BITSET_PLAYER_I_WENT_STRAIGHT_TO_SPEC_CAM)
				START_MUSIC_FOR_JIPPER()
			ENDIF
			
			PROCESS_SURVIVAL_MUSIC(TRUE)
			
			bIWentFromSpectatingToPassedWaveStage = TRUE
			
		    // If the mission has not ended yet.
		    IF (GET_SERVER_HORDE_STAGE() != eHORDESTAGE_REWARDS_AND_FEEDACK)
		        IF DOES_THIS_SPECTATOR_CAM_WANT_TO_LEAVE(g_BossSpecData.specCamData)
					SET_CLIENT_GAME_STATE(SURVIVAL_GAME_STATE_LEAVE)
					TOGGLE_RENDERPHASES(FALSE)
					DEACTIVATE_SPECTATOR_CAM(g_BossSpecData, DSCF_BAIL_FOR_TRANSITION)
					
					DO_MATCH_END()
		            bGoToRewardsAndFeedback = TRUE
					PRINTLN("[LM][Survival_Spec][PROCESS_EARLY_END_SURVIVAL_SPECTATORS] - DOES_THIS_SPECTATOR_CAM_WANT_TO_LEAVE = TRUE, setting bGoToRewardsAndFeedback = TRUE")
		        ENDIF
		    ELSE
		       	bGoToRewardsAndFeedback = TRUE
				PRINTLN("[LM][Survival_Spec][PROCESS_EARLY_END_SURVIVAL_SPECTATORS] - GET_SERVER_HORDE_STAGE() = eHORDESTAGE_REWARDS_AND_FEEDACK, setting bGoToRewardsAndFeedback = TRUE")
		    ENDIF
		    
		    // If we should be ending the mission.
		    IF bGoToRewardsAndFeedback
				IF IS_THIS_SPECTATOR_CAM_ACTIVE(g_BossSpecData.specCamData)
				AND NOT IS_THIS_SPECTATOR_CAM_RUNNING(g_BossSpecData.specCamData)
					SET_LOCAL_BIT(ciLocalBool_bModeEndedSpecCamTranisition)
				ENDIF
		        SET_BIT(iSpectatorBitset, ci_SpectatorBS_Ending)
				IF (GET_SERVER_HORDE_STAGE() = eHORDESTAGE_REWARDS_AND_FEEDACK)
					SET_EARLY_END_SURVIVAL_SPECTATE(ci_SpectatorState_REJOINING)
				ELSE
					SET_EARLY_END_SURVIVAL_SPECTATE(ci_SpectatorState_CLEANUP)
				ENDIF
		    ELSE
				IF GET_CLIENT_HORDE_STAGE(PARTICIPANT_ID()) != eHORDESTAGE_SCTV
			        IF IS_BIT_SET(playerBD[iLocalPart].iBitset, BITSET_PLAYER_I_WENT_STRAIGHT_TO_SPEC_CAM)
			        OR IS_BIT_SET(playerBD[iLocalPart].iBitset, BITSET_PLAYER_WENT_TO_SPEC_CAM_FOR_OUT_OF_BOUNDS)
						IF AM_I_STRAIGHT_TO_SPECTATOR_PLAYER_THAT_SHOULD_JOIN_FIGHT()
						OR AM_I_ABANDONED_HORDE_ON_PLAYLIST_PLAYER_THAT_SHOULD_JOIN_FIGHT()
							
							SET_EARLY_END_SURVIVAL_SPECTATE(ci_SpectatorState_REJOINING)
							
							#IF IS_DEBUG_BUILD
								IF IS_BIT_SET(playerBD[iLocalPart].iBitset, BITSET_PLAYER_WENT_TO_SPEC_CAM_FOR_OUT_OF_BOUNDS)
									PRINTLN("[LM][Survival_Spec][PROCESS_EARLY_END_SURVIVAL_SPECTATORS] - BITSET_PLAYER_WENT_TO_SPEC_CAM_FOR_OUT_OF_BOUNDS")
								ENDIF
								IF AM_I_STRAIGHT_TO_SPECTATOR_PLAYER_THAT_SHOULD_JOIN_FIGHT()
									PRINTLN("[LM][Survival_Spec][PROCESS_EARLY_END_SURVIVAL_SPECTATORS] - AM_I_STRAIGHT_TO_SPECTATOR_PLAYER_THAT_SHOULD_JOIN_FIGHT() = TRUE")
								ENDIF
							#ENDIF
						ENDIF
			        ENDIF
					
					IF GET_SERVER_HORDE_STAGE() = eHORDESTAGE_PASSED_WAVE
						SET_EARLY_END_SURVIVAL_SPECTATE(ci_SpectatorState_REJOINING)
					ENDIF
				ENDIF
		    ENDIF
			
			MAINTAIN_SPECTATOR_HORDE_CAM()
		BREAK
		
		CASE ci_SpectatorState_REJOINING
			PRINTLN("[LM][Survival_Spec][PROCESS_EARLY_END_SURVIVAL_SPECTATORS] - Processing ci_SpectatorState_REJOINING")
			
			CLEAR_BIT(g_iBS1_Mission, ciBS1_Mission_HoldUpRespawn)
			DISABLE_SPECTATOR_FADES_DURING_THIS_EVENT(TRUE)
			BLOCK_SPECTATOR_COPYING_TARGET_FADES(TRUE)
			
			IF NOT IS_BIT_SET(iSpectatorBitset, ci_SpectatorBS_Warped)
				IF NOT bSetupSpecificSpawnCoords
					IF IS_SCREEN_FADED_OUT()
						PRINTLN("[LM][Survival_Spec][PROCESS_EARLY_END_SURVIVAL_SPECTATORS] - Fadeout Completed - Deactivating spectator and setting up respawn.")						
						DEACTIVATE_SPECTATOR_CAM(g_BossSpecData)
						INT iSpawnPos, iFightingPart
						FOR iFightingPart = 0 TO MAX_NUMBER_HORDE_FIGHTING_PARTICIPANTS-1
							IF serverBD.sFightingParticipantsList.iParticipant[iFightingPart] != -1
								IF serverBD.sFightingParticipantsList.iParticipant[iFightingPart] = iLocalPart
									iSpawnPos = iFightingPart
									PRINTLN("[Survival_Spec][PROCESS_EARLY_END_SURVIVAL_SPECTATORS] - Found a valid spawn point for rejoining spectator. Spawn pos: ", iSpawnPos)
									BREAKLOOP
								ENDIF
							ENDIF
						ENDFOR
						IF iSpawnPos = -1
							IF iLocalPartInTeam != -1
								iSpawnPos = iLocalPartInTeam
								PRINTLN("[Survival_Spec][PROCESS_EARLY_END_SURVIVAL_SPECTATORS] - Using Part in team as spawn point location")
							ELSE
								PRINTLN("[Survival_Spec][PROCESS_EARLY_END_SURVIVAL_SPECTATORS] - Using backup position.")
								iSpawnPos = 0
							ENDIF
						ENDIF
						SETUP_SPECIFIC_SPAWN_LOCATION(serverBD.vStartPos[iSpawnPos], serverBD.fStartHeading[iSpawnPos], 5.0, TRUE, 0.0, TRUE, FALSE, 4.0)
						g_BossSpecData.specCamData.vHideReturn = serverBD.vStartPos[iSpawnPos]
						PRINTLN("[Survival_Spec][PROCESS_EARLY_END_SURVIVAL_SPECTATORS] - Updated g_BossSpecData.specCamData.vHideReturn to ", g_BossSpecData.specCamData.vHideReturn)
						bSetupSpecificSpawnCoords = TRUE
					ELSE
						IF NOT IS_SCREEN_FADING_OUT()
							PRINTLN("[LM][Survival_Spec][PROCESS_EARLY_END_SURVIVAL_SPECTATORS] - Doing screen fadeout before rejoining.")
							DO_SCREEN_FADE_OUT(250)
						ELSE
							PRINTLN("[LM][Survival_Spec][PROCESS_EARLY_END_SURVIVAL_SPECTATORS] - Waiting for fadeout to finish.")
						ENDIF
					ENDIF
				ELSE
					IF IS_THIS_SPECTATOR_CAM_OFF(g_BossSpecData.specCamData)
						IF WARP_TO_SPAWN_LOCATION(SPAWN_LOCATION_AT_SPECIFIC_COORDS, FALSE, FALSE, FALSE, FALSE, DEFAULT, TRUE, FALSE, FALSE, VEHICLE_RESPOT_TIME, TRUE, DEFAULT, FALSE, FALSE, TRUE, FALSE, TRUE)
							PRINTLN("[LM][Survival_Spec][PROCESS_EARLY_END_SURVIVAL_SPECTATORS] - WARP_TO_SPAWN_LOCATION successful.")
							bSetupSpecificSpawnCoords = FALSE
							CLEAR_SPECIFIC_SPAWN_LOCATION()
			                CLEAR_BIT(playerBD[iLocalPart].iBitset, BITSET_PLAYER_I_WENT_STRAIGHT_TO_SPEC_CAM)
							CLEAR_BIT(playerBD[iLocalPart].iBitset, BITSET_PLAYER_WENT_TO_SPEC_CAM_FOR_OUT_OF_BOUNDS)
			                NET_SET_PLAYER_CONTROL(localPlayer, TRUE)
							CLEAR_MP_DECORATOR_BIT(localPlayer, MP_DECORATOR_BS_INVALID_TO_SPECTATE)
							Clear_Any_Objective_Text_From_This_Script()						
							SET_PLAYER_INVINCIBLE(LocalPlayer, FALSE)
							SET_ENTITY_VISIBLE(localPlayerPed, TRUE)
							SET_ENTITY_COLLISION(localPlayerPed, TRUE)
							FREEZE_ENTITY_POSITION(localPlayerPed, FALSE)
							SET_ENTITY_VISIBLE(localPlayerPed, FALSE)
							CLEAR_TIMECYCLE_MODIFIER()
							ANIMPOSTFX_STOP_ALL()
							PRINTLN("[Survival] - Stopping all Anim Post FX 4")
							RESET_ALL_HUD_COLOURS()
							SET_BIT(iSpectatorBitset, ci_SpectatorBS_Warped)
						ELSE
							PRINTLN("[LM][Survival_Spec][PROCESS_EARLY_END_SURVIVAL_SPECTATORS] - Waiting for WARP_TO_SPAWN_LOCATION")
						ENDIF
					ELSE
						PRINTLN("[LM][Survival_Spec][PROCESS_EARLY_END_SURVIVAL_SPECTATORS] - Waiting for Spectator Camera to deactivate.")
					ENDIF
				ENDIF
			ELSE
				SET_EARLY_END_SURVIVAL_SPECTATE(ci_SpectatorState_CLEANUP)
			ENDIF
		BREAK
		
		CASE ci_SpectatorState_CLEANUP
			PRINTLN("[LM][Survival_Spec][PROCESS_EARLY_END_SURVIVAL_SPECTATORS] - Processing ci_SpectatorState_CLEANUP")			
			
			IF (GET_SERVER_HORDE_STAGE() != eHORDESTAGE_REWARDS_AND_FEEDACK
			AND GET_SERVER_HORDE_STAGE() != eHORDESTAGE_PASSED_WAVE)
			OR GET_END_HORDE_REASON() = eENDHORDEREASON_ALL_PLAYERS_DEAD
				DO_SCREEN_FADE_IN(1500)
				SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
				IF IS_VALID_INTERIOR(GET_INTERIOR_FROM_ENTITY(LocalPlayerPed))
					PRINTLN("PROCESS_EARLY_END_SURVIVAL_SPECTATORS - ci_SpectatorState_CLEANUP - Player is in an interior")
				ENDIF
				IF IS_VALID_INTERIOR(GET_INTERIOR_AT_COORDS(GET_ENTITY_COORDS(localPlayerPed)))
					PRINTLN("PROCESS_EARLY_END_SURVIVAL_SPECTATORS - ci_SpectatorState_CLEANUP - Player coords is in an interior")
				ENDIF
			ENDIF
						
			DEACTIVATE_SPECTATOR_CAM(g_BossSpecData)
			ENABLE_SPECTATOR_FADES()
			BLOCK_SPECTATOR_COPYING_TARGET_FADES(FALSE)
			SET_MINIMAP_IN_SPECTATOR_MODE(FALSE)			
			g_bMissionHideRespawnBar = FALSE	
			HIDE_MY_PLAYER_BLIP(FALSE, TRUE)			
			CLEAR_BIT(g_iBS1_Mission, ciBS1_Mission_HoldUpRespawn)
			RESET_NET_TIMER(tdSpectatorStartTimer)
			CLEAR_SPECTATOR_DOING_LEGITIMATE_SKYSWOOP_UP()
			
			IF IS_BIT_SET(iSpectatorBitset, ci_SpectatorBS_Ending)
				SET_EARLY_END_SURVIVAL_SPECTATE(ci_SpectatorState_END)
			ELSE
				LEAVE_SPECTATOR_STAGE_AND_JOIN_FIGHT(0)
				SET_EARLY_END_SURVIVAL_SPECTATE(ci_SpectatorState_IDLE)
			ENDIF
			
			iSpectatorBitset = 0			
		BREAK
		
		CASE ci_SpectatorState_END
			PRINTLN("[LM][Survival_Spec][PROCESS_EARLY_END_SURVIVAL_SPECTATORS] - Processing ci_SpectatorState_END")
			SET_CLIENT_HORDE_STAGE(eHORDESTAGE_REWARDS_AND_FEEDACK)
		BREAK
		
	ENDSWITCH
ENDPROC

PROC PROCESS_eHORDESTAGE_SCTV_CLIENT()
   	
    // Process ped actions.
    PROCESS_SQUAD_ENEMY_PED_BODIES()
    
    // Process pickups
    PROCESS_PICKUP_LOGIC()
    
    // HUD.
    MAINTAIN_SPECTATOR_HORDE_HUD()
    
	IF IS_BIT_SET(MPSpecGlobals.SCTVData.iBitSet, SCTV_BIT_FADE_IN_REQUESTED)
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_SPECTATOR, "=== SURVIVAL === IS_BIT_SET(MPSpecGlobals.SCTVData.iBitSet, SCTV_BIT_FADE_IN_REQUESTED)")					
		#ENDIF
		IF NOT IS_FADE_SWITCH_CLASHING_WITH_OTHER_FADES()
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SPECTATOR, "=== SURVIVAL === IS_FADE_SWITCH_CLASHING_WITH_OTHER_FADES()")					
			#ENDIF
			IF NOT IS_SCREEN_FADED_IN()
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_SPECTATOR, "=== SURVIVAL === NOT IS_SCREEN_FADED_IN()")					
				#ENDIF
				IF NOT IS_SCREEN_FADING_IN()
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_SPECTATOR, "=== SURVIVAL === NOT IS_SCREEN_FADING_IN()")					
					#ENDIF
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME_SHORT)
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_SPECTATOR, "=== SURVIVAL === CLEAR_BIT(MPSpecGlobals.SCTVData.iBitSet, SCTV_BIT_FADE_IN_REQUESTED)")					
				#ENDIF
				CLEAR_BIT(MPSpecGlobals.SCTVData.iBitSet, SCTV_BIT_FADE_IN_REQUESTED)
			ENDIF
		ENDIF
	ENDIF
	
	// So SCTV players don;t hold up the survival for competitiors.
	IF IS_SCREEN_FADED_IN()
		SET_BIT(playerBD[iLocalPart].iBitset, BITSET_PLAYER_COMPLETED_START_FADE_IN)
	ENDIF
	
    IF IS_BIT_SET(MPSpecGlobals.iBitset, GLOBAL_SPEC_BS_READY_FOR_LEADERBOARD)
        PROCESS_LEADERBOARD()
	ELSE
		SET_ON_LBD_GLOBAL(FALSE)
    ENDIF
    
    IF IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_QUIT_REQUESTED)
        DO_SCREEN_FADE_OUT(50)
		PRINTLN("[Survival] - GLOBAL_SPEC_BS_QUIT_REQUESTED bit is set, calling SCRIPT_CLEANUP.")
		// Sort leader board data for end of mode.
        SCRIPT_CLEANUP()
    ENDIF
    
    // Everyone go to rewards and feedback if end of mode.
    IF GET_SERVER_HORDE_STAGE() = eHORDESTAGE_REWARDS_AND_FEEDACK
        IF IS_THIS_SPECTATOR_CAM_ACTIVE(g_BossSpecData.specCamData)
            DEACTIVATE_SPECTATOR_CAM(g_BossSpecData)
        ENDIF
		SETUP_SURVIVAL_PLAYER_SPAWN_POSITION(FALSE)
        SET_CLIENT_HORDE_STAGE(GET_SERVER_HORDE_STAGE())
    ENDIF
            
ENDPROC

////////
///    Spectator End
////////

// *********************************** \\
// *********************************** \\
//                                     \\
//			CLIENT LOGIC    	       \\
//                                     \\
// *********************************** \\
// *********************************** \\


PROC PROCESS_eHORDESTAGE_JIP_IN_CLIENT()
 
	BOOL bTimeout
	
	IF NOT HAS_NET_TIMER_STARTED(stJipTimer)
		START_NET_TIMER(stJipTimer)
	ELSE
		IF HAS_NET_TIMER_EXPIRED(stJipTimer, 20000)
			bTimeout = TRUE
			NET_SCRIPT_ASSERT("Survival - JIP in stage timed out - are there any pickups or props missing?")
		ENDIF
	ENDIF
	
	SET_PLAYER_INVINCIBLE(PLAYER_ID(), TRUE)
	
    // Create props.
    IF CREATE_FMMC_PROPS_MP(oiHordeProp, oihordePropChildren)
    OR bTimeout
        // Create guns, armour and health.
        IF CREATE_PICKUPS()
    	OR bTimeout
            // If the server has moved on.
            IF GET_SERVER_HORDE_STAGE() != eHORDESTAGE_JIP_IN
                SET_PROP_MODELS_AS_NO_LONGER_NEEDED()
                SET_CLIENT_HORDE_STAGE(eHORDESTAGE_SETUP_MISSION)
				IF NOT DID_I_JOIN_MISSION_AS_SPECTATOR()
					HIDE_PLAYER(FALSE)
                	NET_SET_PLAYER_CONTROL(localPlayer, TRUE)
				ELSE
					DO_SCREEN_FADE_OUT(0)
				ENDIF
            ENDIF
    
        ENDIF
        
    ENDIF
   	
ENDPROC

PROC PROCESS_eHORDESTAGE_SETUP_MISSION_CLIENT()
    IF GET_SERVER_HORDE_STAGE() != eHORDESTAGE_SETUP_MISSION
       	SET_CLIENT_HORDE_STAGE(eHORDESTAGE_START_FADE_IN)
    ENDIF
ENDPROC

PROC PROCESS_eHORDESTAGE_START_FADE_IN_CLIENT()
    
    IF IS_PLAYER_SCTV(localPlayer)

		PRINTLN("[Survival] - I am an sctv player.")
		
        // Give control back to player.
        IF DOES_ENTITY_EXIST(LocalPlayerPed)
            IF NOT IS_ENTITY_DEAD(LocalPlayerPed)
                SET_PLAYER_CONTROL(localPlayer,TRUE)
            ENDIF
        ENDIF
		CLEANUP_ALL_CORONA_FX(TRUE) 
		SET_SKYFREEZE_CLEAR()
		IF ANIMPOSTFX_IS_RUNNING("MP_job_load")
			ANIMPOSTFX_STOP("MP_job_load")
		ENDIF
		SET_BIT(playerBD[iLocalPart].iBitset, BITSET_PLAYER_DID_INITIAL_FIGHTER_STATUS_SETUP)
		CLIENT_SET_UP_EVERYONE_CHAT()
        SET_CLIENT_HORDE_STAGE(eHORDESTAGE_SCTV)
       	
    ELSE
	
	    // Do start fade in.
	    IF NOT IS_BIT_SET(playerBD[iLocalPart].iBitset, BITSET_PLAYER_COMPLETED_START_FADE_IN)
		
	        // Give control back to player.
	        IF DOES_ENTITY_EXIST(LocalPlayerPed)
	            IF NOT IS_ENTITY_DEAD(LocalPlayerPed)
	                SET_PLAYER_CONTROL(localPlayer,TRUE)
	            ENDIF
	        ENDIF
			
	        // If I am not ready to fade in yet.
			IF NOT IS_BIT_SET(playerBD[iLocalPart].iBitset, BITSET_PLAYER_READY_FOR_START_FADE_IN)
				
				// Say I am ready for start fade in.
				IF DID_I_JOIN_MISSION_AS_SPECTATOR()
					bSetupInitialSpawnPos = TRUE
					SET_BIT(playerBD[iLocalPart].iBitset, BITSET_PLAYER_READY_FOR_START_FADE_IN)
				ELSE
					IF NOT bSetupInitialSpawnPos
						SETUP_SURVIVAL_PLAYER_SPAWN_POSITION(FALSE)
						bSetupInitialSpawnPos = TRUE
					ELSE
				        IF IS_FAKE_MULTIPLAYER_MODE_SET()
						OR WARP_TO_START_POSITION(iWarpToStartPosStage)						
							PRINTLN("[Survival] - warped to start position, going to, ready to begin fade in.")
				            SET_BIT(playerBD[iLocalPart].iBitset, BITSET_PLAYER_READY_FOR_START_FADE_IN)
				        ENDIF
					ENDIF
				ENDIF
				
			// If I am ready to fade in.
			ELSE
			
				// Do fade in once everyone is ready for it.
		        IF NOT IS_LOCAL_BIT_SET(ciLocalBool_bStartFadeInTriggered)
		            IF ARE_ALL_PARTICIPANTS_READY_TO_DO_START_FADE_IN()
						PRINTLN("[Survival] - all participants are ready to fade in.")
		                IF IS_BIT_SET(serverBd.iBitset, BITSET_SERVER_NEW_PLAYERS_GO_STRAIGHT_TO_SPEC_CAM)
							PRINTLN("[Survival] - server says new players should go straight to the spectator camera.")
		                    SETUP_STRAIGHT_TO_SPECTATOR_PLAYER()
		                    MAINTAIN_SPECTATOR_HORDE_CAM()
		                    SET_BIT(playerBD[iLocalPart].iBitset, BITSET_PLAYER_COMPLETED_START_FADE_IN)
		                ENDIF
						SET_LOCAL_BIT(ciLocalBool_bStartFadeInTriggered)
		            ENDIF
		        ELSE
			       	IF DID_I_JOIN_MISSION_AS_SPECTATOR()
					OR FADE_SCREEN_IN()
						PRINTLN("[Survival] - completed screen fade in.")
			          	SET_BIT(playerBD[iLocalPart].iBitset, BITSET_PLAYER_COMPLETED_START_FADE_IN)
			       	ENDIF
		        ENDIF
			
	        ENDIF
			
	    ELSE

			PRINTLN("[Survival] - I am not an sctv player.")
			
            // If server has moved on to a different stage, we should do that too.
            IF (IS_BIT_SET(serverBd.iBitset, BITSET_SERVER_NEW_PLAYERS_GO_STRAIGHT_TO_SPEC_CAM) AND serverBD.sWaveData.iWaveCount > 1) 
			OR DID_I_JOIN_MISSION_AS_SPECTATOR()
			
				PRINTLN("[Survival] - server says new players go straght to spectator camera*.")
				
                SET_BIT(playerBD[iLocalPart].iBitset, BITSET_PLAYER_I_WENT_STRAIGHT_TO_SPEC_CAM)
                SET_BIT(playerBD[iLocalPart].iBitset, BITSET_PLAYER_DID_INITIAL_FIGHTER_STATUS_SETUP)
				
                IF GET_SERVER_HORDE_STAGE() != eHORDESTAGE_START_FADE_IN
            		
					PRINTLN("[Survival] - server is no longer at horde stage eHORDESTAGE_START_FADE_IN.")
					
                    SETUP_STRAIGHT_TO_SPECTATOR_PLAYER()
                    
                    // Give control back to player.
                    IF DOES_ENTITY_EXIST(LocalPlayerPed)
                        IF NOT IS_ENTITY_DEAD(LocalPlayerPed)
                            SET_PLAYER_CONTROL(localPlayer,TRUE)
                        ENDIF
                    ENDIF
                    
					CLIENT_SET_UP_EVERYONE_CHAT()
					SET_EARLY_END_SURVIVAL_SPECTATE(ci_SpectatorState_WAIT)
                    SET_CLIENT_HORDE_STAGE(eHORDESTAGE_SPECTATING)
                    
                ENDIF
                    
            ELSE
        		
				PRINTLN("[Survival] - server says new players go to fighting stage.")
				
                IF GET_SERVER_HORDE_STAGE() != eHORDESTAGE_START_FADE_IN
					PRINTLN("[Survival] - server is no longer at horde stage eHORDESTAGE_START_FADE_IN.")
					SET_BIT(playerBD[iLocalPart].iBitset, BITSET_PLAYER_DID_INITIAL_FIGHTER_STATUS_SETUP)
                    SET_ENTITY_VISIBLE(LocalPlayerPed, TRUE)
					CLIENT_SET_UP_EVERYONE_CHAT()
                    SET_CLIENT_HORDE_STAGE(eHORDESTAGE_FIGHTING)
                ENDIF
                    
            ENDIF
                
        ENDIF
        
    ENDIF
    
ENDPROC

PROC PROCESS_eHORDESTAGE_FIGHTING_CLIENT()
    
    INT i, j
    
	TEXT_LABEL_23 tl23MusicEvent
	
    BOOL bPlayerAtShootout = NOT IS_PED_OUT_OF_BOUNDS(localPlayerPed)
    
	INIT_LOCAL_WAVE_DATA()
	
    // J-skips.
    #IF IS_DEBUG_BUILD
    MAINTAIN_CLIENT_J_SKIPS()
    #ENDIF
    
    // Process ped actions.
    PROCESS_SQUAD_ENEMY_PED_BODIES()
	
	//Endless Shard
	PROCESS_ENDLESS_WAVE_SHARD()
    
	// Bugstar 2218090.
	SET_DAILY_OBJECTIVE_TICKERS_ENABLED_ON_MISSION(TRUE)
    
    // Helicopters.
    REPEAT MAX_NUM_SURVIVAL_HELIS i
      	PROCESS_HELI_BODY(serverBD_Enemies_V.sSurvivalHeli[i], i)
    ENDREPEAT
		
	// Don't want ambient peds trying to take out the players.
	SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(localPlayer)
    
	// Bikes.
	REPEAT MAX_NUM_LAND_VEHICLES i
		REPEAT NUM_LAND_VEH_PEDS j
	     	UPDATE_ENEMY_NET_PED_BLIP(	serverBD_Enemies_V.sSurvivalLandVehicle[i].sPed[j].netId, 
										structLandVehicleEnemyBlip[i][j],
										DEFAULT,
										FALSE, 
										FORCE_ENEMY_BLIPS_ON() )
			PROCESS_LAND_VEHICLE_BODY(serverBD_Enemies_V.sSurvivalLandVehicle[i], j, i)
		ENDREPEAT
	ENDREPEAT
	
	// Make sure this is false if we are fighting so spec cam works on next death.
	bTurnedOffSpecCam = FALSE
	
	// Start playstats since I am a competitive player.
	IF NOT bSavedMatchstartPlayStatsData
	AND NOT IS_FAKE_MULTIPLAYER_MODE_SET()
		DEAL_WITH_FM_MATCH_START(FMMC_TYPE_SURVIVAL, serverBD.iMatchHistoryId[0], serverBD.iMatchHistoryId[1])
		bSavedMatchstartPlayStatsData = TRUE
		IF NOT HAS_NET_TIMER_STARTED(stSurvivalTime)
			START_NET_TIMER(stSurvivalTime)
			PRINTLN("[Survival] - started stSurvivalTime.")
		ENDIF
		PRINTLN("[Survival] - called DEAL_WITH_FM_MATCH_START(FMMC_TYPE_SURVIVAL)")
	ENDIF
	
	// Display help explaining lives if never played survival before.
	SET_BIT(MPGlobalsAmbience.iFmNmhBitSet4, BI_FM_NMH4_DO_SURV_LIVES_HELP) 
	
	// Tell player to get back to the shoot out if not near it.
	MAINTAIN_GET_BACK_TO_SHOOT_OUT_OBJECTIVE(bPlayerAtShootout)
	
	IF NOT bResetReminderTimer
		RESET_REMINDER_TIMER_FOR_ACTIVITY(FMMC_TYPE_SURVIVAL) 
		bResetReminderTimer = TRUE
	ENDIF
	
    IF bPlayerAtShootout
    #IF IS_DEBUG_BUILD
	OR bBlockDistanceCheck
	#ENDIF
	   	
        // Process pickups
        PROCESS_PICKUP_LOGIC()
        
		// Background audio track.
		IF IS_BIT_SET(serverBD.iBitset, BITSET_SERVER_DID_FIRST_WAVE_SETUP)
			PROCESS_SURVIVAL_MUSIC()
		ENDIF
		
        MAINTAIN_SURVIVAL_OBJECTIVE_TEXT()
		
        // Display wave progress.
        MAINTAIN_WAVE_PROGRESS_BAR(HUDORDER_BOTTOM)
        
    ENDIF
    
    // If server has moved on to a different stage, we should do that too.
    IF NOT IS_BIT_SET(playerBD[iLocalPart].iBitset, BITSET_PLAYER_ABANDONED_HORDE)
        IF GET_SERVER_HORDE_STAGE() != eHORDESTAGE_FIGHTING
            Clear_Any_Objective_Text_From_This_Script()
            CLEAR_LOCAL_BIT(ciLocalBool_bGotWaveProgressLabel)
			ENUM_MUSIC_EVENT eMusicEvent
			IF IS_BIT_SET(serverBD.iBitset, BITSET_SERVER_DID_FIRST_WAVE_SETUP)
				IF serverBD.sWaveData.iWaveCount < g_FMMC_STRUCT.sSurvivalWaveData.iNumberOfWaves
				OR IS_THIS_ENDLESS_WAVES()
					eMusicEvent = eMUSICEVENT_WAVE_COMPLETE
				ELSE
					eMusicEvent = eMUSICEVENT_STOP
				ENDIF
				IF IS_SCORE_CORONA_OPTION_ON()
					IF NOT structMusicEvent[ENUM_TO_INT(eMusicEvent)].bTriggered
						tl23MusicEvent = GET_SURVIVAL_MUSIC_EVENT_STRING(eMUSICEVENT_START, serverBD.sWaveData.iWaveCount, TRUE)
						CANCEL_MUSIC_EVENT(tl23MusicEvent)
						tl23MusicEvent = GET_SURVIVAL_MUSIC_EVENT_STRING(eMUSICEVENT_WAVE_40P, serverBD.sWaveData.iWaveCount, TRUE)
						CANCEL_MUSIC_EVENT(tl23MusicEvent)
						tl23MusicEvent = GET_SURVIVAL_MUSIC_EVENT_STRING(eMUSICEVENT_WAVE_70P, serverBD.sWaveData.iWaveCount, TRUE)
						CANCEL_MUSIC_EVENT(tl23MusicEvent)
						SURVIVAL_START_MUSIC_EVENT(eMusicEvent)
						RESET_FIGHTING_STAGE_MUSIC_EVENTS()
						IF bMutedNearbyRadios
							STOP_AUDIO_SCENE("MP_POSITIONED_RADIO_MUTE_SCENE")
							bMutedNearbyRadios = FALSE
						ENDIF
						IF GET_SERVER_HORDE_STAGE() = eHORDESTAGE_PASSED_WAVE
							IF NOT IS_CURRENT_WAVE_LESS_THAN_MAX()
							AND IS_FAKE_MULTIPLAYER_MODE_SET()
								DO_SCREEN_FADE_OUT(500)
							ENDIF
						ENDIF
            			SET_CLIENT_HORDE_STAGE(GET_SERVER_HORDE_STAGE())
					ENDIF
				ELSE
					SET_CLIENT_HORDE_STAGE(GET_SERVER_HORDE_STAGE())
				ENDIF
			ELSE
				IF IS_SCORE_CORONA_OPTION_ON()
					RESET_FIGHTING_STAGE_MUSIC_EVENTS()
					IF bMutedNearbyRadios
						STOP_AUDIO_SCENE("MP_POSITIONED_RADIO_MUTE_SCENE")
						bMutedNearbyRadios = FALSE
					ENDIF
				ENDIF
            	SET_CLIENT_HORDE_STAGE(GET_SERVER_HORDE_STAGE())
			ENDIF
        ENDIF
    ENDIF
    
ENDPROC

PROC PROCESS_eHORDESTAGE_PASSED_WAVE_CLIENT()
    
	INT i, j
	
	IF IS_FAKE_MULTIPLAYER_MODE_SET()
	AND g_iTestType = 1
		IF NOT IS_LOCAL_BIT_SET(ciLocalBool_bAllPedSpawnersDone)
			iPedSpawnTested = iPedSpawnNumber
		ELSE
			iPedSpawnTested = g_FMMC_STRUCT_ENTITIES.iNumberOfPeds
		ENDIF
		IF NOT IS_LOCAL_BIT_SET(ciLocalBool_bAllVehicleSpawnersDone)
			iVehicleSpawnTested = iVehicleSpawnNumber
		ELSE
			iVehicleSpawnTested = g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles
		ENDIF
	ENDIF
	// B*1089716
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM)
	
    // J-skips.
    #IF IS_DEBUG_BUILD
    MAINTAIN_CLIENT_J_SKIPS()
    #ENDIF
    
    // Process ped actions.
    PROCESS_SQUAD_ENEMY_PED_BODIES()
    
	SET_DAILY_OBJECTIVE_TICKERS_ENABLED_ON_MISSION(FALSE)
	
    // Helicopters.
    REPEAT MAX_NUM_SURVIVAL_HELIS i
      	PROCESS_HELI_BODY(serverBD_Enemies_V.sSurvivalHeli[i], i)
    ENDREPEAT
    
	// No enemy blips during passed wave section.
	REMOVE_ON_FOOT_PED_BLIPS()
	REMOVE_VEHICLE_PED_BLIPS()
	
	// Bikes.
	REPEAT MAX_NUM_LAND_VEHICLES i
		REPEAT NUM_LAND_VEH_PEDS j
			PROCESS_LAND_VEHICLE_BODY(serverBD_Enemies_V.sSurvivalLandVehicle[i], j, i)
		ENDREPEAT
	ENDREPEAT
	
    // Process pickups
    PROCESS_PICKUP_LOGIC()
    
	// No heli blips.
    REPEAT MAX_NUM_SURVIVAL_HELIS i
		REMOVE_HELI_BLIP(i)
    ENDREPEAT
	
	IF DOES_BLIP_EXIST(biLastEnemy)
		REMOVE_BLIP(biLastEnemy)
		PRINTLN("[survival] - PROCESS_eHORDESTAGE_PASSED_WAVE_CLIENT - Removing biLastEnemy")
	ENDIF
    
    // Remove current pickups, will recreate them next wave with new pickup types.
    REMOVE_PICKUPS()
	
	MAINTAIN_VALID_TEST_TRACKING()
	
	IF HAS_NET_TIMER_STARTED(stLeaveHordeTimer)
		RESET_NET_TIMER(stLeaveHordeTimer)
	ENDIF
	
	IF IS_BIT_SET(playerBD[iLocalPart].iBitset, BITSET_PLAYER_I_HAVE_COMPLETED_END_OF_WAVE_HUD)
		IF IS_CURRENT_WAVE_LESS_THAN_MAX()
		AND (GET_END_HORDE_REASON() = eENDHORDEREASON_NOT_FINISHED_YET)
			IF NOT IS_SCREEN_FADING_OUT()
				IF CLEANUP_ALL_ENEMIES()
					SET_LOCAL_BIT(ciLocalBool_bCleanedUpEnemies, TRUE)
				ENDIF
			ENDIF
		ELSE
			SET_LOCAL_BIT(ciLocalBool_bCleanedUpEnemies, TRUE)
		ENDIF
	ENDIF
	
	IF IS_LOCAL_BIT_SET(ciLocalBool_bLocalWaveDataSetUp)
		CLEAR_LOCAL_BIT(ciLocalBool_bLocalWaveDataSetUp)
		CLEAR_LOCAL_BIT(ciLocalBool_bInvalidWaveForKillRP)
	ENDIF
            
    // Display end of wave text and move on once server does.
    IF NOT IS_BIT_SET(playerBD[iLocalPart].iBitset, BITSET_PLAYER_I_HAVE_COMPLETED_END_OF_WAVE_HUD)
            
        IF IS_CURRENT_WAVE_LESS_THAN_MAX()
			
			IF NOT bTurnedOffSpecCam
			AND IS_LOCAL_PLAYER_SPECTATOR()
				DISABLE_SPECTATOR_FADES_DURING_THIS_EVENT()
				LEAVE_SPECTATOR_STAGE_AND_JOIN_FIGHT(2)
				RESET_RESPAWNING_DATA_CLIENT(1, AFTERLIFE_JUST_REJOIN)
				bTurnedOffSpecCam = TRUE
			ENDIF
		
			MAINTAIN_TIME_SPENT_IN_PAUSE_MENU()
			
			// Make sure respawners have no control. 
			IF g_bCelebrationScreenIsActive
				IF (sCelebrationData.eCurrentStage < CELEBRATION_STAGE_END_TRANSITION)
					IF IS_PLAYER_CONTROL_ON(localPlayer)
						NET_SET_PLAYER_CONTROL(localPlayer, FALSE) 
					ENDIF
				ENDIF
			ENDIF
			
            IF DISPLAY_END_OF_WAVE_TEXT(FALSE, TRUE)
			OR bBeenInPauseMenuTooLongOnWavePassed
				#IF IS_DEBUG_BUILD
					IF bBeenInPauseMenuTooLongOnWavePassed
						PRINTLN("[Survival] - bBeenInPauseMenuTooLongOnWavePassed = TRUE.")
					ENDIF
				#ENDIF
				TIME_DATATYPE temp
				iTimeForHold = temp
				iTimeInPauseMenu = 0
				bGotFirstTimeForHoldValue = FALSE
				bBeenInPauseMenuTooLongOnWavePassed = FALSE
				CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_WAVE_COMPLETE)
				
				IF DOES_CAM_EXIST(camCelebrationScreen)
					DESTROY_CAM(camCelebrationScreen, TRUE)
					RENDER_SCRIPT_CAMS(FALSE, FALSE)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
					ANIMPOSTFX_STOP_ALL()
					PRINTLN("[Survival] - Stopping all Anim Post FX 3")
					ANIMPOSTFX_PLAY("MP_Celeb_Win_Out", 0, FALSE)
				ENDIF
				
				CLEAR_AREA_OF_ENTITIES()
				
				CLEAR_ALL_BIG_MESSAGES()
				STOP_CELEBRATION_SCREEN_ANIM(sCelebrationData, "SUMMARY")
				CLEANUP_CELEBRATION_SCREEN(sCelebrationData)
				SET_CELEBRATION_SCREEN_AS_ACTIVE(FALSE)
				
				IF NOT IS_PLAYER_RESPAWNING(localPlayer)
					NET_SET_PLAYER_CONTROL(localPlayer, TRUE)
				#IF IS_DEBUG_BUILD
				ELSE
					PRINTLN("[Survival] - check 0, IS_PLAYER_RESPAWNING = TRUE, skipping setting control TRUE, respawning should handle it.")
				#ENDIF
				ENDIF
				
                SET_COMPLETED_END_OF_WAVE_HUD()
				
            ENDIF
			
        // On last wave skip straight to eom stage and use hud there.
        ELSE
            SET_COMPLETED_END_OF_WAVE_HUD()
        ENDIF
    ELSE
    	HIDE_SCRIPTED_HUD_COMPONENT_THIS_FRAME(HUD_MP_RANK_BAR)
	
        IF GET_SERVER_HORDE_STAGE() != eHORDESTAGE_PASSED_WAVE
            playerBD[iLocalPart].iMyKillsThisWave = 0
			playerBD[iLocalPart].iDeathsThisWave = 0
            CLEAR_BIT(playerBD[iLocalPart].iBitset, BITSET_PLAYER_I_HAVE_COMPLETED_END_OF_WAVE_HUD)
            
			structMusicEvent[ENUM_TO_INT(eMUSICEVENT_WAVE_COMPLETE)].bTriggered = FALSE
			structMusicEvent[ENUM_TO_INT(eMUSICEVENT_STOP)].bTriggered = FALSE
			
			IF DOES_ENTITY_EXIST(LocalPlayerPed)
				IF NOT IS_ENTITY_DEAD(LocalPlayerPed)
					RESET_RESPAWNING_DATA_CLIENT(8)
				ENDIF
			ENDIF
			
            CLEAR_HUD_PLAYER_ARRAY(sEndBoardData)
            CLEAR_LOCAL_BIT(ciLocalBool_bGivenEndOfWaveXp)
            iEndOfWaveHudStage = 0
			bTurnedOffSpecCam = FALSE
			
			IF NOT IS_PLAYER_RESPAWNING(localPlayer)
				NET_SET_PLAYER_CONTROL(localPlayer, TRUE)
			#IF IS_DEBUG_BUILD
			ELSE
				PRINTLN("[Survival] - check 1, IS_PLAYER_RESPAWNING = TRUE, skipping setting control TRUE, respawning should handle it.")
			#ENDIF
			ENDIF
			
			RESET_FIGHTING_STAGE_MUSIC_EVENTS()
			
			IF GET_END_HORDE_REASON() != eENDHORDEREASON_ALL_PLAYERS_DEAD
				bIWentFromSpectatingToPassedWaveStage = FALSE
				PRINTLN("[Survival] - GET_END_HORDE_REASON() != eENDHORDEREASON_ALL_PLAYERS_DEAD, setting bIWentFromSpectatingToPassedWaveStage = FALSE.")
			ENDIF
			
			UPDATE_HORDE_WAVES_SURVIVED_AWARD()
			
			SET_CLIENT_HORDE_STAGE(GET_SERVER_HORDE_STAGE())
            
        ENDIF
            
    ENDIF
    
ENDPROC

PROC PROCESS_eHORDESTAGE_DELAY_BEFORE_NEXT_WAVE_CLIENT()
	
	HIDE_SCRIPTED_HUD_COMPONENT_THIS_FRAME(HUD_MP_RANK_BAR) //1623915 - Prevent the rank bar from appearing.
	
	IF IS_AUDIO_SCENE_ACTIVE("MP_CELEB_SCREEN_SCENE")
		STOP_AUDIO_SCENE("MP_CELEB_SCREEN_SCENE")	
		PRINTLN("[Survival] - MP_CELEB_SCREEN_SCENE is active, calling STOP_AUDIO_SCENE(MP_CELEB_SCREEN_SCENE). Call E.")
	ENDIF
					
    // J-skips.
    #IF IS_DEBUG_BUILD
    MAINTAIN_CLIENT_J_SKIPS()
    #ENDIF
    
	//gdisablerankupmessage = FALSE
	SET_DISABLE_RANK_UP_MESSAGE(FALSE)
	
    // Process pickup
   	PROCESS_PICKUP_LOGIC()
    
	SET_DAILY_OBJECTIVE_TICKERS_ENABLED_ON_MISSION(TRUE)
	    
	// Show countdown until next wave.
    MAINTAIN_NEXT_WAVE_HUD(HUDORDER_BOTTOM)
	
    // New pickups!
    CREATE_PICKUPS_FOR_NEW_WAVE(iCreatePickupsStage)
    
	ANIMPOSTFX_STOP("MP_Celeb_Win")
	ANIMPOSTFX_STOP("MP_Celeb_Lose")
	
	CLEAR_LOCAL_BIT(ciLocalBool_bIncrementedTestedWaveCounter)
	
    // Broadcast if I'm ready for the next wave countdown.
    IF NOT IS_BIT_SET(playerBD[iLocalPart].iBitset, BITSET_PLAYER_I_AM_ALIVE_AND_FADED_IN)
        IF bLocalPlayerOK
            IF IS_SCREEN_FADED_IN()
				IF NOT IS_PLAYER_RESPAWNING(localPlayer)
				OR IS_FAKE_MULTIPLAYER_MODE_SET()
                    SET_BIT(playerBD[iLocalPart].iBitset, BITSET_PLAYER_I_AM_ALIVE_AND_FADED_IN)
					PRINTLN("[Survival] - PROCESS_eHORDESTAGE_DELAY_BEFORE_NEXT_WAVE_CLIENT - Setting BITSET_PLAYER_I_AM_ALIVE_AND_FADED_IN")
                ENDIF
            ENDIF
        ENDIF
    ENDIF
    
	IF bDoUnhide
		HIDE_PLAYER(FALSE)
	ENDIF
	
	IF IS_LOCAL_BIT_SET(ciLocalBool_bRespawningBetweenWaves)
		SET_UP_PLAYER_AFTER_RESPAWN()
	ENDIF
	
	IF IS_LOCAL_BIT_SET(ciLocalBool_bEndlessShardDone)
		CLEAR_LOCAL_BIT(ciLocalBool_bEndlessShardDone)
	ENDIF
	
	IF NOT IS_LOCAL_BIT_SET(ciLocalBool_bResetHUDColours)
		RESET_ALL_HUD_COLOURS()
		SET_LOCAL_BIT(ciLocalBool_bResetHUDColours)
	ENDIF
	
    // Move on once server does.
    IF GET_SERVER_HORDE_STAGE() != eHORDESTAGE_DELAY_BEFORE_NEXT_WAVE
	
        #IF IS_DEBUG_BUILD
        playerBD[iLocalPart].bSkipWave = FALSE
		bDoingWarpToHorde = FALSE
		bJSkipped = FALSE
        #ENDIF
		
		IF bDoUnhide
			bDoUnhide = FALSE
			PRINTLN("[Survival] - bDoUnhide = FALSE")
		ENDIF

        iCountDownAudioStage = 0
        iCreatePickupsStage = 0
        CLEAR_BIT(playerBD[iLocalPart].iBitset, BITSET_PLAYER_I_AM_ALIVE_AND_FADED_IN)
		
		structMusicEvent[ENUM_TO_INT(eMUSICEVENT_WAVE_COMPLETE)].bTriggered = FALSE
		CLEAR_LOCAL_BIT(ciLocalBool_bResetHUDColours)
        SET_CLIENT_HORDE_STAGE(GET_SERVER_HORDE_STAGE())
		
    ENDIF
    
ENDPROC

// *********************************** \\
// *********************************** \\
//                                     \\
// 				SERVER LOGIC           \\
//                                     \\
// *********************************** \\
// *********************************** \\

PROC PROCESS_eHORDESTAGE_JIP_IN_SERVER()
    SET_SERVER_HORDE_STAGE(eHORDESTAGE_SETUP_MISSION)
ENDPROC

PROC PROCESS_eHORDESTAGE_SETUP_MISSION_SERVER()
    IF LOAD_AND_CREATE_ALL_FMMC_ENTITIES_WK(serverBD.sFMMC_SBD)
        SET_VEHICLE_MODELS_AS_NO_LONGER_NEEDED()
        SET_OBJECT_MODELS_AS_NO_LONGER_NEEDED()
        SET_PED_MODELS_AS_NO_LONGER_NEEDED()
        SET_SERVER_HORDE_STAGE(eHORDESTAGE_START_FADE_IN)
    ENDIF
ENDPROC

PROC PROCESS_eHORDESTAGE_START_FADE_IN_SERVER()
    // Once everyone has faded in, move onto the next wave stage.
    IF HAVE_ALL_PARTICIPANTS_COMPLETED_START_FADE_IN()
		serverBD.sFightingParticipantsList.iNumFightingParticipantsBeginningOfWave = serverBD.sFightingParticipantsList.iNumFightingParticipants
		PRINTLN("[Survival] - call 1 - set serverBD.sFightingParticipantsList.iNumFightingParticipantsBeginningOfWave = serverBD.sFightingParticipantsList.iNumFightingParticipants, ", serverBD.sFightingParticipantsList.iNumFightingParticipantsBeginningOfWave)
    	SET_SERVER_HORDE_STAGE(eHORDESTAGE_DELAY_BEFORE_NEXT_WAVE)
    ENDIF
ENDPROC

PROC SET_CURRENT_WAVE_DEFEATED()
	DEBUG_PRINTCALLSTACK()
	SET_BIT(serverBd.iBitset, BITSET_SERVER_CURRENT_WAVE_DEFEATED)
	PRINTLN("SET_CURRENT_WAVE_DEFEATED - WAVE END - Wave ", serverBD.sWaveData.iWaveCount, " defeated! Setting BITSET_SERVER_CURRENT_WAVE_DEFEATED")
ENDPROC

PROC SERVER_CURRENT_WAVE_DEFEATED()
	
	// On foot peds.
	IF serverBD.iNumAliveTotalEnemies <= 0
	OR (GET_NUM_CREATED_MISSION_PEDS() = 0 AND HAS_ENOUGH_TIME_PASSED_ON_SUBSTAGE())
		//Actual end
		IF (serverBD.iKillsThisWave >= serverBD.sWaveData.iNumRequiredKills)
			PRINTLN("[Survival] - WAVE END - SERVER_CURRENT_WAVE_DEFEATED - Kills are equal/greater than required amount")
            SET_CURRENT_WAVE_DEFEATED()
        ENDIF
		
		//All enemies dead, but 1 kill off and on last substage. Just end
		IF (serverBD.sWaveData.eStage = serverBD.sWaveData.eHighestSubstage AND (serverBD.sWaveData.iNumRequiredKills - serverBD.iKillsThisWave) = 1)
			PRINTLN("[Survival] - WAVE END - SERVER_CURRENT_WAVE_DEFEATED - All enemies dead, last substage but one kill off")
			SET_CURRENT_WAVE_DEFEATED()
		ENDIF
		
		//Starting failsafe timer
		IF (serverBd.iKillsThisWaveSubStage[serverBd.sWaveData.eStage] < serverBd.sWaveData.iNumRequiredSubstageKills[serverBD.sWaveData.eStage])
		OR (serverBD.iKillsThisWave < serverBD.sWaveData.iNumRequiredKills)
			IF NOT HAS_NET_TIMER_STARTED(serverBD_Enemies_V.stNoSpawningEnemiesTimer)
				PRINTLN("[Survival] - WAVE END - SERVER_CURRENT_WAVE_DEFEATED - starting no enemies spawn failsafe.")
				IF (serverBD.iNumKillsThisWaveFromEvents > 0) 
				AND (serverBd.iNumKillsThisSubStageFromEvents[serverBd.sWaveData.eStage] > 0)
					PRINTLN("[Survival] - WAVE END - SERVER_CURRENT_WAVE_DEFEATED - (serverBD.iNumKillsThisWaveFromEvents > 0) ")
					IF (serverBD.iNumKillsThisWaveFromEvents > serverBD.iKillsThisWave)
						PRINTLN("[Survival] - WAVE END - SERVER_CURRENT_WAVE_DEFEATED - serverBD.iKillsThisWave = ", serverBD.iKillsThisWave, ", serverBD.iNumKillsThisWaveFromEvents = ", serverBD.iNumKillsThisWaveFromEvents)
						serverBD.iKillsThisWave = serverBD.iNumKillsThisWaveFromEvents
						PRINTLN("[Survival] - WAVE END - SERVER_CURRENT_WAVE_DEFEATED - (serverBD.iNumKillsThisWaveFromEvents > serverBD.iKillsThisWave), setting (serverBD.iKillsThisWave = serverBD.iNumKillsThisWaveFromEvent)")
						PRINTLN("[Survival] - WAVE END - SERVER_CURRENT_WAVE_DEFEATED - serverBD.iKillsThisWave = ", serverBD.iKillsThisWave, ", serverBD.iNumKillsThisWaveFromEvents = ", serverBD.iNumKillsThisWaveFromEvents)
					ENDIF
					IF serverBd.iNumKillsThisSubStageFromEvents[serverBd.sWaveData.eStage] > serverBD.iKillsThisWaveSubStage[serverBd.sWaveData.eStage]
						serverBD.iKillsThisWaveSubStage[serverBd.sWaveData.eStage] = serverBD.iNumKillsThisSubStageFromEvents[serverBd.sWaveData.eStage]
						PRINTLN("[Survival] - WAVE END - SERVER_CURRENT_WAVE_DEFEATED - (serverBD.iNumKillsThisSubStageFromEvents > serverBD.iKillsThisWaveSubStage), setting (serverBD.iKillsThisWaveSubStage = serverBD.iNumKillsThisSubStageFromEvents)")
					ENDIF
				ENDIF
				START_NET_TIMER(serverBD_Enemies_V.stNoSpawningEnemiesTimer)
			ENDIF
		ELSE
			RESET_NET_TIMER(serverBD_Enemies_V.stNoSpawningEnemiesTimer)
		ENDIF
		
		//Failsafe timer action.
		IF HAS_NET_TIMER_STARTED(serverBD_Enemies_V.stNoSpawningEnemiesTimer)
			IF HAS_NET_TIMER_EXPIRED(serverBD_Enemies_V.stNoSpawningEnemiesTimer, 7500)
				PRINTLN("[Survival] - WAVE END - no enemies have spawned for 7.5 seconds, setting sub stage kill count to the same as required kills.")
				serverBD.iKillsThisWaveSubStage[serverBd.sWaveData.eStage] = serverBd.sWaveData.iNumRequiredSubstageKills[serverBD.sWaveData.eStage]
				serverBD.iNumKillsThisSubStageFromEvents[serverBd.sWaveData.eStage] = serverBd.sWaveData.iNumRequiredSubstageKills[serverBD.sWaveData.eStage]

				serverBD.iNumKillsThisWaveFromEvents = serverBD.sWaveData.iNumRequiredKills
				serverBD.iKillsThisWave = serverBD.sWaveData.iNumRequiredKills
			ENDIF
			IF HAS_NET_TIMER_EXPIRED(serverBD_Enemies_V.stNoSpawningEnemiesTimer, 10000)
				#IF IS_DEBUG_BUILD
					PRINTLN("[Survival] - WAVE END - SERVER_CURRENT_WAVE_DEFEATED - no enemies have spawned for 10 seconds when we are supposed to have more, forcing end of wave.")
				#ENDIF
	            SET_CURRENT_WAVE_DEFEATED()
			ENDIF
		ENDIF
	ELSE
		RESET_NET_TIMER(serverBD_Enemies_V.stNoSpawningEnemiesTimer)
	ENDIF
	
	// Vehicle peds.
	IF IS_BIT_SET(serverBD.iBitset, BITSET_SERVER_ALL_VEHICLE_PEDS_STUCK_OOB)
		#IF IS_DEBUG_BUILD
			PRINTLN("[Survival] - WAVE END - SERVER_CURRENT_WAVE_DEFEATED - serverBD.iBitset, BITSET_SERVER_ALL_VEHICLE_PEDS_STUCK_OOB, forcing end of wave.")
		#ENDIF
        SET_CURRENT_WAVE_DEFEATED()
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF NOT IS_BIT_SET(serverBd.iBitset, BITSET_SERVER_CURRENT_WAVE_DEFEATED)
			IF (serverBD.iKillsThisWave >= serverBD.sWaveData.iNumRequiredKills)
				PRINTLN("SERVER_CURRENT_WAVE_DEFEATED - serverBD.iNumAliveSquadEnemies: ", serverBD.iNumAliveSquadEnemies, " serverBD.iNumAliveHelis: ", serverBD.iNumAliveHelis, " serverBD.iNumAliveLandVehiclePeds: ", serverBD.iNumAliveLandVehiclePeds)
			ENDIF
		ENDIF
	#ENDIF
ENDPROC

PROC PROCESS_eHORDESTAGE_FIGHTING_SERVER()
    
    // S and F skips for quick pass/fail.
    #IF IS_DEBUG_BUILD
    MAINTAIN_S_AND_F_SKIPS()
    #ENDIF
	
    // Process ped decisions.
    PROCESS_ENEMY_PED_BRAINS()
    
    // Helicopters.
    MAINTAIN_SURVIVAL_HELIS_SERVER()
    
	// Enemies arriving on bikes.
	MAINTAIN_SURVIVAL_LAND_VEHICLES_SERVER()
	
	// Update the wave stage. This used in the squad spawning to build up the enemy numbers as the wave progresses.
    MAINTAIN_WAVE_PROGRESSION()
	
	// In case vehicles get stuck outside of the survival.
	MAINTAIN_NO_VEHICLE_ENEMIES_IN_SURVIVAL_AREA_FAILSAFE()
	
	MAINTAIN_TURRET_PED_STUCK_OUT_OF_SURVIVAL_AREA()
	
	// Check for end of wave. If we have killed everything then we win.
    SERVER_CURRENT_WAVE_DEFEATED()
	
    // Once the current wave is defeated, move onto passed wave stage.
    IF IS_BIT_SET(serverBD.iBitset, BITSET_SERVER_DID_FIRST_WAVE_SETUP)
        IF IS_BIT_SET(serverBD.iBitset, BITSET_SERVER_CURRENT_WAVE_DEFEATED)
        #IF IS_DEBUG_BUILD
        OR serverBD.bSkipWave
        #ENDIF
			IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(stEndOfWaveDelay, 1000) //1623906 - Request to reduce delay from 2 seconds to 1 second.
	            PRINTLN("[Survival] - WAVE END - BITSET_SERVER_CURRENT_WAVE_DEFEATED bit is set, going to eHORDESTAGE_PASSED_WAVE.")
				RESET_NET_TIMER(stEndOfWaveDelay)
				RESET_NO_VEHICLE_ENEMIES_IN_SURVIVAL_AREA_TIMER()
				RESET_NET_TIMER(serverBD_Enemies_V.stNoSpawningEnemiesTimer)
				CLEAR_BIT(serverBD.iBitset, BITSET_SERVER_CURRENT_WAVE_DEFEATED)
	            SET_SERVER_HORDE_STAGE(eHORDESTAGE_PASSED_WAVE)
			ENDIF
        ENDIF
    ENDIF
    
    // If all players lost all lives, set end reason and go to end stage.
    IF IS_LOCAL_BIT_SET(ciLocalBool_bAllPlayersLostAllLivesFlag)
		PRINTLN("[Survival] - ciLocalBool_bAllPlayersLostAllLivesFlag is set.")
		IF HAVE_ENDLESS_WAVES_STARTED()
       		SET_END_HORDE_REASON(eENDHORDEREASON_BEAT_ALL_WAVES)
		ELSE
			SET_END_HORDE_REASON(eENDHORDEREASON_ALL_PLAYERS_DEAD)
		ENDIF
		serverBD.sServerFMMC_EOM.bAllowedToVote = TRUE
		RESET_NO_VEHICLE_ENEMIES_IN_SURVIVAL_AREA_TIMER()
		RESET_NET_TIMER(serverBD_Enemies_V.stNoSpawningEnemiesTimer)
        SET_SERVER_HORDE_STAGE(eHORDESTAGE_REWARDS_AND_FEEDACK)
   	ENDIF
	
ENDPROC

PROC RESET_SERVER_VARIABLES_ON_WAVE_PASS()
	PRINTLN("RESET_SERVER_VARIABLES_BETWEEN_WAVES called")
	CLEAR_BIT(serverBd.iBitset, BITSET_SERVER_CURRENT_WAVE_DEFEATED)
	serverBd.iNumKillsThisWaveFromEvents = 0
	serverBd.iNumKillsThisSubStageFromEvents[eWAVESTAGE_EASY] = 0
	serverBd.iNumKillsThisSubStageFromEvents[eWAVESTAGE_MEDIUM] = 0
	serverBd.iNumKillsThisSubStageFromEvents[eWAVESTAGE_HARD] = 0
	SET_SEARCHING_FOR_AIR_VEHICLE(-1)
	SET_SEARCHING_FOR_LAND_VEHICLE(-1)
	SET_SEARCHING_FOR_SQUAD_PED(-1)
	DEACTIVATE_LAND_VEHICLES()
	SET_WAVE_SUB_STAGE(eWAVESTAGE_EASY)
ENDPROC

PROC PROCESS_eHORDESTAGE_PASSED_WAVE_SERVER()
    
    // S and F skips for quick pass/fail.
    #IF IS_DEBUG_BUILD
    MAINTAIN_S_AND_F_SKIPS()
    REMOVE_PREVIOUS_WAVE_ENTITIES_DEBUG()
    #ENDIF
    
	
	
    IF HAS_EVERYONE_COMPLETED_END_OF_WAVE_HUD()
		IF IS_LOCAL_BIT_SET(ciLocalBool_bCleanedUpEnemies)
		OR NOT IS_CURRENT_WAVE_LESS_THAN_MAX()
	        #IF IS_DEBUG_BUILD
	        IF NOT serverBD.bSkipWave
	        #ENDIF
	        serverBD.iKillsThisWave = 0
			
	        IF IS_CURRENT_WAVE_LESS_THAN_MAX()
                INCREMENT_WAVE()
				RESET_SERVER_VARIABLES_ON_WAVE_PASS()
				SET_SERVER_HORDE_STAGE(eHORDESTAGE_DELAY_BEFORE_NEXT_WAVE)
				CLEAR_LOCAL_BIT(ciLocalBool_bCleanedUpEnemies, TRUE)
            ELSE
                SET_END_HORDE_REASON(eENDHORDEREASON_BEAT_ALL_WAVES)
				serverBD.sServerFMMC_EOM.bAllowedToVote = TRUE
                SET_SERVER_HORDE_STAGE(eHORDESTAGE_REWARDS_AND_FEEDACK)
            ENDIF
	        #IF IS_DEBUG_BUILD
	        ENDIF
	        #ENDIF
		ENDIF
    ENDIF
    
ENDPROC

PROC PROCESS_eHORDESTAGE_DELAY_BEFORE_NEXT_WAVE_SERVER()
    
    // S and F skips for quick pass/fail.
    #IF IS_DEBUG_BUILD
    MAINTAIN_S_AND_F_SKIPS()
    REMOVE_PREVIOUS_WAVE_ENTITIES_DEBUG()
    #ENDIF
    
    SWITCH serverBD.sDelayBeforeNextWaveStageData
        
        CASE eDELAYBEFORENEXTWAVESTAGE_ADD_NEW_FIGHTING_PARTICIPANTS
            // If we have data, move onto wait for fade in stage.
            IF IS_BIT_SET(serverBD.iBitset, BITSET_SERVER_FIGHTING_PART_LIST_INITIALISED)
                PRINTLN("[Survival] - going to delay before next wave stage eDELAYBEFORENEXTWAVESTAGE_WAIT_FOR_ALL_FIGHTERS_TO_FADE_IN.")
                serverBD.sDelayBeforeNextWaveStageData = eDELAYBEFORENEXTWAVESTAGE_WAIT_FOR_ALL_FIGHTERS_TO_FADE_IN
            ENDIF
        BREAK
        
        CASE eDELAYBEFORENEXTWAVESTAGE_WAIT_FOR_ALL_FIGHTERS_TO_FADE_IN
            // If doing first countdown at beginning of mode, skip through.
            IF serverBD.sWaveData.iWaveCount = 1
                PRINTLN("[Survival] - wave = 1, setting BITSET_SERVER_READY_FOR_DELAY_COUNTDOWN.")
                SET_BIT(serverBD.iBitset, BITSET_SERVER_READY_FOR_DELAY_COUNTDOWN)
            ELSE
                // Wait for all fighting participants to have spawned and faded in.
                IF NOT IS_BIT_SET(serverBD.iBitset, BITSET_SERVER_READY_FOR_DELAY_COUNTDOWN)
                    IF HAVE_ALL_FIGHTING_PARTICIPANTS_SPAWNED_FADED_IN()
                        PRINTLN("[Survival] - all fighting participants have respawned and faded in, setting BITSET_SERVER_READY_FOR_DELAY_COUNTDOWN.")
                        SET_BIT(serverBD.iBitset, BITSET_SERVER_READY_FOR_DELAY_COUNTDOWN)
					ELSE
						PRINTLN("[Survival] - PROCESS_eHORDESTAGE_DELAY_BEFORE_NEXT_WAVE_SERVER - Waiting on all fighting participants to be faded in")
                    ENDIF
                ENDIF
            ENDIF
            
			// Go to the countdown if we're ready for it.
            IF IS_BIT_SET(serverBD.iBitset, BITSET_SERVER_READY_FOR_DELAY_COUNTDOWN)
                PRINTLN("[Survival] - BITSET_SERVER_READY_FOR_DELAY_COUNTDOWN is set, going to delay before next wave stage eDELAYBEFORENEXTWAVESTAGE_DO_COUNT_DOWN.")
                serverBD.sDelayBeforeNextWaveStageData = eDELAYBEFORENEXTWAVESTAGE_DO_COUNT_DOWN
            ENDIF
        BREAK
        
        CASE eDELAYBEFORENEXTWAVESTAGE_DO_COUNT_DOWN
			IF NOT HAS_NET_TIMER_STARTED(jobUnderTenSecondsTimer)
				START_NET_TIMER(jobUnderTenSecondsTimer)
				PRINTLN("[Survival] - jobUnderTenSecondsTimer has been started.")
			ENDIF
			
			// Wave countdown.
            IF NOT HAS_NET_TIMER_STARTED(serverBD.stNextWaveDelay)
                PRINTLN("[Survival] - starting countdown to next wave timer.")
                START_NET_TIMER(serverBD.stNextWaveDelay)
				IF NOT HAS_NET_TIMER_STARTED(serverBD.tdSurvivalLength)
					START_NET_TIMER(serverBD.tdSurvivalLength)
					PRINTLN("[Survival] - serverBD.tdSurvivalLength has been started")
				ENDIF
            ELSE
				// If we have finished the countdown.
                IF HAS_NET_TIMER_EXPIRED(serverBD.stNextWaveDelay, GET_DELAY_BETWEEN_WAVES_LENGTH())
                    PRINTLN("[Survival] - countdown to next wave timer has expired.")
                    #IF IS_DEBUG_BUILD
                    IF NOT serverBD.bSkipWave
                    #ENDIF
					
					// Setup the next wave.
					serverBD.sFightingParticipantsList.iNumFightingParticipantsBeginningOfWave = serverBD.sFightingParticipantsList.iNumFightingParticipants
					PRINTLN("[Survival] - call 2 - set serverBD.sFightingParticipantsList.iNumFightingParticipantsBeginningOfWave = serverBD.sFightingParticipantsList.iNumFightingParticipants, ", serverBD.sFightingParticipantsList.iNumFightingParticipantsBeginningOfWave)
                    RESET_NET_TIMER(serverBD.stNextWaveDelay)
                    SETUP_NEXT_WAVE()
                    CLEAR_BIT(serverBD.iBitset, BITSET_SERVER_READY_FOR_DELAY_COUNTDOWN)
                    serverBD.sDelayBeforeNextWaveStageData = eDELAYBEFORENEXTWAVESTAGE_ADD_NEW_FIGHTING_PARTICIPANTS
                    SET_SERVER_HORDE_STAGE(eHORDESTAGE_FIGHTING)
					REINIT_NET_TIMER(serverBD.tdWaveLength)
					PRINTLN("[Survival] - STARTING Wave ", serverBD.sWaveData.iWaveCount)
                    #IF IS_DEBUG_BUILD
                    ENDIF
                    #ENDIF
                ENDIF
            ENDIF
        BREAK
        
    ENDSWITCH
    
ENDPROC

PROC PROCESS_eHORDESTAGE_SPECTATING_SERVER()
    // Nothing.
ENDPROC

PROC PROCESS_eHORDESTAGE_REWARDS_AND_FEEDACK_SERVER()
    IF SHOULD_GO_TO_WAITING_TO_LEAVE_STATE()
        SET_SERVER_GAME_STATE(SURVIVAL_GAME_STATE_WAITING_TO_LEAVE)
    ELSE
        SET_SERVER_GAME_STATE(SURVIVAL_GAME_STATE_END)
    ENDIF
ENDPROC

PROC SAVE_PLAYER_DATA_ON_FINISH()
	IF NOT bSavedOutPlayerUgcData
		IF (GET_END_HORDE_REASON() != eENDHORDEREASON_NOT_FINISHED_YET)
		OR IS_BIT_SET(playerBD[iLocalPart].iBitset, BITSET_PLAYER_ABANDONED_HORDE)
			IF SAVE_OUT_UGC_PLAYER_DATA(GlobalplayerBD_FM[NATIVE_TO_INT(localPlayer)].tl31CurrentMissionFileName, sSaveOutVars)
				bSavedOutPlayerUgcData = TRUE
				PRINTLN("[Survival] - saved out UGC player data.")
			ELSE
				PRINTLN("[Survival] - saving out UGC player data.")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_PROPS_EVERY_FRAME()
	INT iProp
	INT iR, iG, iB, iA
	FLOAT fZ
	FOR iProp = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfProps-1
		IF DOES_ENTITY_EXIST(oihordeProp[iProp])
			
			IF (g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].mn = INT_TO_ENUM(MODEL_NAMES, HASH("imp_prop_ship_01a")) 
			OR g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].mn = INT_TO_ENUM(MODEL_NAMES, HASH("gr_Prop_DamShip_01a"))
			OR g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].mn = INT_TO_ENUM(MODEL_NAMES, HASH("p_spinning_anus_s")))
				
				INT iUFOSound = iUFOSoundIndex[iProp]
				IF iUFOSound != -1
					IF IS_SOUND_ID_VALID(iUFOSoundID[iUFOSound])
						IF NOT IS_BIT_SET(iUFOSoundBS, iUFOSound)
							PLAY_SOUND_FROM_ENTITY(iUFOSoundID[iUFOSound], "Ship_Loop", oihordeProp[iProp], "DLC_VW_AS_Sounds")
							PRINTLN("[Survival] - SOUNDS - Ship_Loop playing from Entity")
							SET_BIT(iUFOSoundBS, iUFOSound)
						ENDIF
					ELSE
						iUFOSoundID[iUFOSound] = GET_SOUND_ID()
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropBitset2, ciFMMC_PROP2_UFO_Spin)
					FLOAT fHeading = GET_ENTITY_HEADING(oihordeProp[iProp])
					fHeading += GET_FRAME_TIME()*5
					SET_ENTITY_HEADING(oihordeProp[iProp], fHeading)
				ENDIF
			
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropBitset2, ciFMMC_PROP2_UFO_Light)
				AND DOES_ENTITY_EXIST(oihordeProp[iProp])
					
					INT iUFO = GET_UFO_LIGHT_INDEX(iProp)
					IF iUFO != -1
						
						IF NOT HAS_NET_TIMER_STARTED(tdUFOLights[iUFO])
							IF GET_RANDOM_INT_IN_RANGE(0, 501) = 10
								REINIT_NET_TIMER(tdUFOLights[iUFO])
							ENDIF
						ELSE
							IF HAS_NET_TIMER_EXPIRED(tdUFOLights[iUFO], 10000)
								RESET_NET_TIMER(tdUFOLights[iUFO])
							ELSE
								VECTOR vCoord = GET_ENTITY_COORDS(oihordeProp[iProp])
								vCoord.z -= 2.0
								VECTOR vDirection = <<0.0, 0.0, 0.0>>
								VECTOR vRotation = <<0.0, 180, 0>>
								VECTOR vTemp = vCoord
								vTemp.z -= 5.0
								GET_GROUND_Z_FOR_3D_COORD(vTemp, fZ)
								VECTOR vScale = <<7.5, 7.5, vTemp.z-fZ>>
								GET_HUD_COLOUR(hcUFOLight, iR, iG, iB, iA)
								iA = 40
								DRAW_MARKER_EX(MARKER_CYLINDER, vCoord, vDirection, vRotation, vScale, iR, iG, iB, iA, FALSE, FALSE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
								
								//Rings
								INT iRings, iRingTime, iAlpha
								FLOAT fRingT, fRingSize
								VECTOR vRingPos = vTemp
								iRingTime = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdUFOLights[iUFO])
								fRingT = TO_FLOAT(iRingTime) / 10000
								vRingPos.z -= 45.0*fRingT
								iAlpha = 90
								fRingSize = 0.5
								fRingSize += 2*fRingT
								IF fRingSize < 0
									fRingSize = 0
								ENDIF
								FOR iRings = 0 TO 5
									VECTOR vTempRingPos = vRingPos
									vTempRingPos.z -= 5*iRings
									iAlpha -= ROUND(100.0*(fRingT-(iRings*0.06)))
									DRAW_CYLINDER_MARKER_FROM_VECTOR(vTempRingPos, 7.5, fRingSize, hcUFOLight, iAlpha, TRUE, -0.1)
								ENDFOR
								
								//Lights
								vCoord.z = fZ
								DRAW_LIGHT_WITH_RANGE(vCoord, iR, iG, iB, 10, 50)
								FLOAT fSize
								fSize = SIN(GET_GAME_TIMER() * 0.05) * 50
								IF fSize < 10.0
									fSize = 10.0
								ENDIF
								DRAW_LIGHT_WITH_RANGE(vTemp, iR, iG, iB, 10, fSize)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			MAINTAIN_PLACED_PROPS_PARTICLE_EFFECTS(iProp, oihordeProp[iProp])
			
		ENDIF
	ENDFOR
ENDPROC

PROC PROCESS_PROPS()
	
	PROCESS_PROPS_EVERY_FRAME()
	
	iStaggeredProp++
	IF iStaggeredProp >= g_FMMC_STRUCT_ENTITIES.iNumberOfProps
		iStaggeredProp = 0
	ENDIF
ENDPROC
