USING "FM_Survival_Controller_Using.sch"
USING "FM_Survival_Controller_Utility.sch"
USING "FM_Survival_Controller_Peds.sch"

FUNC BOOL SHOULD_LAND_VEHICLE_SPAWN_AT_PLACED_POINT(INT iSpawnPoint)
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iSpawnPoint].iVehBitsetEight, ciFMMC_VEHICLE8_PlacedOnAnEntity)
		PRINTLN("SHOULD_LAND_VEHICLE_SPAWN_AT_PLACED_POINT - Spawn point ",iSpawnPoint," - ciFMMC_VEHICLE8_PlacedOnAnEntity is set")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC INT FIND_VALID_VEHICLE_SPAWN_POINT(INT iSpawner, INT iVeh)
	INT iNewSpawner
	BOOL bSearch, bSuccess
	VECTOR vNewPos
	PRINTLN("FIND_VALID_VEHICLE_SPAWN_POINT - Given spawner: ", iSpawner)
	
	IF ARE_ANY_PLAYERS_NEAR_COORD(serverBD.sWaveData.vLVehSpawnPos[iSpawner], 40)
		bSearch = TRUE
		PRINTLN("FIND_VALID_VEHICLE_SPAWN_POINT - A player is too close to this spawn")
	ENDIF
	IF CAN_ANY_PLAYER_SEE_POINT(serverBD.sWaveData.vLVehSpawnPos[iSpawner], 20, TRUE, TRUE)
		bSearch = TRUE
		PRINTLN("FIND_VALID_VEHICLE_SPAWN_POINT - A player can see this spawn point")
	ENDIF
	
	IF bSearch
		FOR iNewSpawner = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles-1
			vNewPos = serverBD.sWaveData.vLVehSpawnPos[iNewSpawner]
			IF NOT IS_VECTOR_ZERO(vNewPos)
			AND NOT ARE_ANY_PLAYERS_NEAR_COORD(vNewPos)
			AND NOT CAN_ANY_PLAYER_SEE_POINT(vNewPos, 20, TRUE, TRUE)
			AND IS_VEHICLE_SPAWN_FREE(iNewSpawner, iVeh)
				iSpawner = iNewSpawner
				PRINTLN("FIND_VALID_VEHICLE_SPAWN_POINT - Updating spawner to ", iSpawner)
				bSuccess = TRUE
				BREAKLOOP	
			ENDIF
		ENDFOR
		
		IF bSuccess
			PRINTLN("FIND_VALID_VEHICLE_SPAWN_POINT - Found a new spawner for vehicle ", iVeh)
		ELSE
			PRINTLN("FIND_VALID_VEHICLE_SPAWN_POINT - Failed to find a new spawner for vehicle ", iVeh)
		ENDIF
	ENDIF
	
	RETURN iSpawner
ENDFUNC

FUNC BOOL FIND_LAND_VEHICLE_SPAWN_COORDS(STRUCT_SURVIVAL_LAND_VEHICLE_DATA &sSurvivalLandVehicle, INT iLandVehicle)
	VECTOR vtemp
	FLOAT fTemp
    // Get spawn point.
    IF IS_VECTOR_ZERO(sSurvivalLandVehicle.vSpawncoords)
		PRINTLN("[Survival] - [AI Spawning] - FIND_LAND_VEHICLE_SPAWN_COORDS - iLandVehicle: ", iLandVehicle, " sSurvivalLandVehicle.vSpawncoords = ZERO.")
				
		VEHICLE_SPAWN_LOCATION_PARAMS eFallbackParams
		eFallbackParams.fMinDistFromCoords = 0.0
		eFallbackParams.bConsiderHighways= TRUE
		eFallbackParams.fMaxDistance= 200.0
		eFallbackParams.bConsiderOnlyActiveNodes= FALSE
		eFallbackParams.bAvoidSpawningInExclusionZones= FALSE
		eFallbackParams.bCheckEntityArea = TRUE
		eFallbackParams.bUseExactCoordsIfPossible = TRUE
		
		INT iSpawner = serverBD.sWaveData.iLVehSpawnLocation[iLandVehicle]
		iSpawner = FIND_VALID_VEHICLE_SPAWN_POINT(iSpawner, iLandVehicle)
		IF serverBD.sWaveData.iLVehSpawnLocation[iLandVehicle] != iSpawner
			serverBD.sWaveData.iLVehSpawnLocation[iLandVehicle] = iSpawner
			PRINTLN("FIND_LAND_VEHICLE_SPAWN_COORDS - serverBD.sWaveData.iLVehSpawnLocation[iLandVehicle] updated to ", serverBD.sWaveData.iLVehSpawnLocation[iLandVehicle])
		ENDIF
				
		IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iSpawner].vPos)
			sSurvivalLandVehicle.vSpawncoords = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iSpawner].vPos
			PRINTLN("[Survival] - [AI Spawning] - FIND_LAND_VEHICLE_SPAWN_COORDS - iLandVehicle: ", iLandVehicle, " Spawning directly at coords ", sSurvivalLandVehicle.vSpawncoords)
			RETURN TRUE
		ELIF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
		AND HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iSpawner].vPos, serverBD.vStartPos[0], sSurvivalLandVehicle.eModel, FALSE, vTemp, fTemp, eFallbackParams)
			sSurvivalLandVehicle.vSpawncoords = vTemp
			PRINTLN("[Survival] - [AI Spawning] - FALLBACK - FIND_LAND_VEHICLE_SPAWN_COORDS - iLandVehicle: ", iLandVehicle, " GET_SAFE_COORDS_IN_AREA_FOR_CREATING_ENTITY got coords at ", sSurvivalLandVehicle.vSpawncoords)
			RETURN TRUE
        ENDIF
	ELSE
		RETURN TRUE
    ENDIF
   	
	RETURN FALSE
	
ENDFUNC

FUNC VEHICLE_SEAT GET_TURRET_SEAT_FOR_VEHICLE(MODEL_NAMES mnVeh)
	SWITCH mnVeh
		CASE CARACARA 
		CASE MENACER
			RETURN VS_EXTRA_LEFT_1
		CASE TECHNICAL
		CASE TECHNICAL2
		CASE TECHNICAL3 
			RETURN VS_BACK_LEFT
	ENDSWITCH
	
	RETURN VS_FRONT_RIGHT
ENDFUNC

FUNC VEHICLE_SEAT GET_FIRST_NON_TURRET_SEAT(MODEL_NAMES mnVeh)
	SWITCH mnVeh
		CASE CARACARA
		CASE TECHNICAL
		CASE TECHNICAL2
		CASE TECHNICAL3
			RETURN VS_FRONT_RIGHT
	ENDSWITCH
	
	RETURN VS_BACK_LEFT
ENDFUNC

FUNC VEHICLE_SEAT GET_SEAT_FOR_VEHICLE_PED(INT iPed, MODEL_NAMES mnVeh)
	SWITCH iPed
		CASE 0 	RETURN VS_DRIVER
		CASE 1	RETURN GET_TURRET_SEAT_FOR_VEHICLE(mnVeh)
		CASE 2	RETURN GET_FIRST_NON_TURRET_SEAT(mnVeh)
		CASE 3	RETURN VS_BACK_RIGHT
	ENDSWITCH
	
	RETURN VS_ANY_PASSENGER
ENDFUNC

FUNC BOOL IS_VEHICLE_A_TANK(VEHICLE_INDEX tempVeh)
	SWITCH GET_ENTITY_MODEL(tempVeh)
		CASE RHINO
		CASE KHANJALI
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC SETUP_COMBAT_VEHICLE_DRIVER(PED_INDEX piPassed)
	SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_DISABLE_SEEK_DUE_TO_LINE_OF_SIGHT, FALSE)
	SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_REQUIRES_LOS_TO_AIM, FALSE)
	SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_CAN_SHOOT_WITHOUT_LOS, TRUE)
	SET_COMBAT_FLOAT(piPassed, CCF_MIN_DISTANCE_TO_TARGET, 20.0)
	SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_MAINTAIN_MIN_DISTANCE_TO_TARGET, FALSE)
	SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_REQUIRES_LOS_TO_SHOOT, FALSE)
	
ENDPROC

PROC INIT_LAND_VEHICLE(VEHICLE_INDEX viVeh, INT iVeh)
	PRINTLN("INIT_LAND_VEHICLE - Called for Vehicle: ", iVeh)
	SET_VEHICLE_DOORS_LOCKED(viVeh, VEHICLELOCK_LOCKED)
	SET_ENTITY_MAX_HEALTH(viVeh, g_FMMC_STRUCT.sSurvivalWaveData.iVehHealth[iWaveToUse][iVeh])
	SET_ENTITY_HEALTH(viVeh, g_FMMC_STRUCT.sSurvivalWaveData.iVehHealth[iWaveToUse][iVeh])
	SET_VEHICLE_BODY_HEALTH(viVeh, TO_FLOAT(g_FMMC_STRUCT.sSurvivalWaveData.iVehHealth[iWaveToUse][iVeh]))
	PRINTLN("[survival] SPAWN_SURVIVAL_LAND_VEHICLE - Vehicle ", iVeh, " has been created")
	IF g_FMMC_STRUCT.sSurvivalWaveData.iVehColour[iWaveToUse][iVeh] != -1
	AND g_FMMC_STRUCT.sSurvivalWaveData.iVehColour[iWaveToUse][iVeh] != ciNUMBER_OF_VEHICLE_COLOURS
		FMMC_SET_THIS_VEHICLE_COLOURS(viVeh, GET_VEHICLE_COLOUR_FROM_SELECTION(g_FMMC_STRUCT.sSurvivalWaveData.iVehColour[iWaveToUse][iVeh]), -1)
	ENDIF
	
	IF IS_VEHICLE_A_TANK(viVeh)
		SET_ARRIVE_DISTANCE_OVERRIDE_FOR_VEHICLE_PERSUIT_ATTACK(viVeh, 20)
	ELSE
		SET_ARRIVE_DISTANCE_OVERRIDE_FOR_VEHICLE_PERSUIT_ATTACK(viVeh, 10)
	ENDIF
ENDPROC

FUNC BOOL SPAWN_SURVIVAL_LAND_VEHICLE(STRUCT_SURVIVAL_LAND_VEHICLE_DATA &sSurvivalLandVehicle, INT iVeh)
    INT iVehRef = iVeh
	IF iVehRef >= g_FMMC_STRUCT.sSurvivalWaveData.iNumVehicles[iWaveToUse]
		PRINTLN("SPAWN_SURVIVAL_LAND_VEHICLE - Vehicle ", iVeh, " is an extra vehicle for more than 2 players. Using vehicle 0 as reference.")
		iVehRef = 0
	ENDIF
	
	MODEL_NAMES mnPedModel = g_FMMC_STRUCT.sSurvivalWaveData.mnPedModel[iWaveToUse][eWAVESTAGE_EASY]
    MODEL_NAMES mnVehModel = g_FMMC_STRUCT.sSurvivalWaveData.mnVeh[iWaveToUse][iVehRef]
	
	IF REQUEST_LOAD_MODEL(mnVehModel)
        IF REQUEST_LOAD_MODEL(mnPedModel)
            IF NOT IS_NET_VEHICLE_DRIVEABLE(sSurvivalLandVehicle.netId)
                IF CREATE_NET_VEHICLE(sSurvivalLandVehicle.netId, mnVehModel, sSurvivalLandVehicle.vSpawnCoords, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[serverBD.sWaveData.iLVehSpawnLocation[iVeh]].fHead, TRUE)
					
					VEHICLE_INDEX viVeh = NET_TO_VEH(sSurvivalLandVehicle.netId)
                   	INIT_LAND_VEHICLE(viVeh, iVeh)
					
					INT iPed, iNumCreated
					FOR iPed = 0 TO GET_SAFE_NUMBER_OF_VEHICLE_PEDS(mnVehModel, iVehRef)-1
						IF CREATE_NET_PED_IN_VEHICLE(sSurvivalLandVehicle.sPed[iPed].netId, sSurvivalLandVehicle.netId, PEDTYPE_CIVMALE, mnPedModel, GET_SEAT_FOR_VEHICLE_PED(iPed, mnVehModel), TRUE)
							PED_INDEX piPed = NET_TO_PED(sSurvivalLandVehicle.sPed[iPed].netId)
							IF DOES_ENTITY_EXIST(piPed)
								IF iPed = 0
			                        // Set driver properties.
			                        SET_PED_RELATIONSHIP_GROUP_HASH(piPed, rgFM_AiHate)
			                        SETUP_PED_FOR_WAVE(0, 0, piPed, DEFAULT, DEFAULT, DEFAULT, iVeh)
									IF IS_WEAPON_A_MELEE_WEAPON(serverBD_Enemies_V.sSurvivalLandVehicle[iVeh].sPed[iPed].wtWeapon)
										PRINTLN("[Survival] - SPAWN_SURVIVAL_LAND_VEHICLE - Driver - ", GET_WEAPON_NAME(serverBD_Enemies_V.sSurvivalLandVehicle[iVeh].sPed[iPed].wtWeapon))
										serverBD_Enemies_V.sSurvivalLandVehicle[iVeh].sPed[iPed].bMelee = TRUE
										serverBD.iNumberOfMeleeEnemies++
										PRINTLN("[Survival] - SPAWN_SURVIVAL_LAND_VEHICLE - Driver - serverBD.iNumberOfMeleeEnemies: ", serverBD.iNumberOfMeleeEnemies, " Wave: ", serverBD.sWaveData.iWaveCount, " Stage: ", serverBD.sWaveData.eStage)
									ENDIF
									SET_PED_COMBAT_ATTRIBUTES(piPed, CA_DISABLE_TARGET_CHANGES_DURING_VEHICLE_PURSUIT, TRUE)
			                        SET_PED_COMBAT_ATTRIBUTES(piPed, CA_DO_DRIVEBYS, TRUE)
									SET_PED_AS_ENEMY(piPed, TRUE)
									SET_PED_DROPS_WEAPONS_WHEN_DEAD(piPed, FALSE)
									IF IS_COMBAT_VEHICLE(viVeh)
										SET_PED_COMBAT_ATTRIBUTES(piPed, CA_USE_VEHICLE_ATTACK, TRUE)
										SET_PED_COMBAT_ATTRIBUTES(piPed, CA_USE_VEHICLE_ATTACK_IF_VEHICLE_HAS_MOUNTED_GUNS, TRUE)
										SET_PED_ACCURACY(piPed, 3)
										SET_PED_SHOOT_RATE(piPed, 100)
										SETUP_COMBAT_VEHICLE_DRIVER(piPed)
										IF IS_VEHICLE_A_TANK(viVeh)
											SET_PED_FIRING_PATTERN(piPed,FIRING_PATTERN_SLOW_FIRE_TANK)
										ELSE
											SET_PED_FIRING_PATTERN(piPed,FIRING_PATTERN_BURST_FIRE_HELI)
										ENDIF
									ENDIF
									IF IS_FAKE_MULTIPLAYER_MODE_SET()
										TASK_COMBAT_PED(piPed, localPlayerPed, COMBAT_PED_PREVENT_CHANGING_TARGET)
									ENDIF
									PRINTLN("[survival] SPAWN_SURVIVAL_LAND_VEHICLE - Driver has been created")
									iNumCreated++
								ELSE
									SET_PED_RELATIONSHIP_GROUP_HASH(piPed, rgFM_AiHate)
		                            
		                            SET_PED_COMBAT_ATTRIBUTES(piPed, CA_DO_DRIVEBYS, TRUE)
									SET_PED_AS_ENEMY(piPed, TRUE)
									
									VEHICLE_SEAT vSeat = GET_SEAT_PED_IS_IN(piPed)
									BOOL bTurretPed
									IF IS_TURRET_SEAT(viVeh, vSeat)
										SET_PED_COMBAT_ATTRIBUTES(piPed, CA_USE_VEHICLE_ATTACK, TRUE)
										SET_PED_COMBAT_ATTRIBUTES(piPed, CA_USE_VEHICLE_ATTACK_IF_VEHICLE_HAS_MOUNTED_GUNS, TRUE)
										SET_PED_ACCURACY(piPed, 5)
										SET_PED_SHOOT_RATE(piPed, 100)
										SET_PED_FIRING_PATTERN(piPed,FIRING_PATTERN_BURST_FIRE_HELI)
										SET_PED_CONFIG_FLAG(piPed, PCF_DontLeaveVehicleIfLeaderNotInVehicle, TRUE)
										SET_PED_COMBAT_ATTRIBUTES(piPed, CA_LEAVE_VEHICLES, FALSE)
										SET_PED_COMBAT_ATTRIBUTES(piPed, CA_RESTRICT_IN_VEHICLE_AIMING_TO_CURRENT_SIDE, TRUE)
										SET_PED_COMBAT_ATTRIBUTES(piPed, CA_FORCE_CHECK_ATTACK_ANGLE_FOR_MOUNTED_GUNS, TRUE)
										PRINTLN("[survival] SPAWN_SURVIVAL_LAND_VEHICLE - Ped ",iPed," Vehicle ", iVeh," is in turret seat")
										bTurretPed = TRUE
									ENDIF
									SETUP_PED_FOR_WAVE(0, 0, piPed, bTurretPed, bTurretPed, DEFAULT, iVeh)
									IF IS_WEAPON_A_MELEE_WEAPON(serverBD_Enemies_V.sSurvivalLandVehicle[iVeh].sPed[iPed].wtWeapon)
										PRINTLN("[Survival] - SPAWN_SURVIVAL_LAND_VEHICLE - Passenger - ", GET_WEAPON_NAME(serverBD_Enemies_V.sSurvivalLandVehicle[iVeh].sPed[iPed].wtWeapon))
										serverBD_Enemies_V.sSurvivalLandVehicle[iVeh].sPed[iPed].bMelee = TRUE
										serverBD.iNumberOfMeleeEnemies++
										PRINTLN("[Survival] - SPAWN_SURVIVAL_LAND_VEHICLE - Passenger - serverBD.iNumberOfMeleeEnemies: ", serverBD.iNumberOfMeleeEnemies, " Wave: ", serverBD.sWaveData.iWaveCount, " Stage: ", serverBD.sWaveData.eStage)
									ENDIF
									SET_PED_DROPS_WEAPONS_WHEN_DEAD(piPed, FALSE)
									IF IS_FAKE_MULTIPLAYER_MODE_SET()
										TASK_COMBAT_PED(piPed, localPlayerPed, COMBAT_PED_PREVENT_CHANGING_TARGET)
									ENDIF
									PRINTLN("[survival] SPAWN_SURVIVAL_LAND_VEHICLE - Ped ",iPed," has been created")
									iNumCreated++
								ENDIF
							ENDIF
						ELSE
							BREAKLOOP
						ENDIF
					ENDFOR
					IF iNumCreated = serverBD_Enemies_V.sSurvivalLandVehicle[iVehRef].iNumPeds
						SET_MODEL_AS_NO_LONGER_NEEDED(mnVehModel)
						SET_MODEL_AS_NO_LONGER_NEEDED(mnPedModel)
						serverBD.iNumVehiclesCreatedThisStage[serverBD.sWaveData.eStage]++
						PRINTLN("[survival] SPAWN_SURVIVAL_LAND_VEHICLE - Incrementing serverBD.iNumVehiclesCreatedThisStage[",serverBD.sWaveData.eStage,"] to ", serverBD.iNumVehiclesCreatedThisStage[serverBD.sWaveData.eStage])
						RETURN TRUE
					ENDIF
				ELSE
					PRINTLN("[survival] SPAWN_SURVIVAL_LAND_VEHICLE - Vehicle creation failed")
                ENDIF
			ELSE
				PRINTLN("[survival] SPAWN_SURVIVAL_LAND_VEHICLE - Vehicle is drivable")
            ENDIF
		ELSE
			PRINTLN("[survival] SPAWN_SURVIVAL_LAND_VEHICLE - Requesting ped model")
        ENDIF
	ELSE
		PRINTLN("[survival] SPAWN_SURVIVAL_LAND_VEHICLE - Requesting vehicle model")
    ENDIF
    
    RETURN FALSE
    
ENDFUNC

// *********************************** \\
// *********************************** \\
//                                     \\
//			AI HELICOPTERS             \\
//                                     \\
// *********************************** \\
// *********************************** \\

FUNC BOOL FIND_SURVIVAL_HELI_SPAWN_COORDS(STRUCT_SURVIVAL_HELI_DATA &sSurvivalHeli)
    
    VECTOR vtemp
    
    IF IS_VECTOR_ZERO(sSurvivalHeli.vSpawnCoords)
        
        vtemp = GET_OFFSET_FROM_COORD_IN_WORLD_COORDS(  serverBD.vBoundsMiddle,
                                                        0.0,
                                                        <<(-1*250 * SIN(TO_FLOAT(sSurvivalHeli.iCircCount)*30)),(250 * COS(TO_FLOAT(sSurvivalHeli.iCircCount)*30)),0>>      )
        
        vtemp.z = GET_APPROX_HEIGHT_FOR_POINT(Vtemp.x, Vtemp.y)
        
        vtemp.z += 30.0
        
        IF IS_POINT_OK_FOR_NET_ENTITY_CREATION(vtemp, 20.0)
		AND NOT CAN_ANY_PLAYER_SEE_POINT(vtemp, 20.0, TRUE, TRUE, 200.0, DEFAULT, DEFAULT, 200.0)
            sSurvivalHeli.vSpawnCoords = vtemp
			PRINTLN("FIND_SURVIVAL_HELI_SPAWN_COORDS found spawn point at ", sSurvivalHeli.vSpawnCoords)
        ELSE
            sSurvivalHeli.iCircCount++
            
            IF sSurvivalHeli.iCircCount >= 12
                sSurvivalHeli.iCircCount = 0
            ENDIF
        ENDIF
    ELSE
        RETURN TRUE
    ENDIF
    
    RETURN FALSE
    
ENDFUNC

FUNC VEHICLE_SEAT GET_CORRECT_FIRST_SEAT_AIR(MODEL_NAMES mnVeh, INT iSeats)
	SWITCH mnVeh
		CASE AVENGER
		CASE SEASPARROW RETURN VS_FRONT_RIGHT
		CASE MOGUL RETURN VS_BACK_LEFT
	ENDSWITCH
	
	IF iSeats = 2
		RETURN VS_FRONT_RIGHT
	ENDIF
	
	RETURN VS_BACK_LEFT
ENDFUNC

FUNC VEHICLE_SEAT GET_AIR_VEHICLE_SEAT(INT iPed, MODEL_NAMES mnVeh, INT iSeats)
	SWITCH iPed
		CASE 0	RETURN VS_DRIVER
		CASE 1	RETURN GET_CORRECT_FIRST_SEAT_AIR(mnVeh, iSeats)
		CASE 2	RETURN VS_BACK_RIGHT
	ENDSWITCH
	RETURN VS_ANY_PASSENGER
ENDFUNC

PROC INIT_AIR_VEH(STRUCT_SURVIVAL_HELI_DATA &sSurvivalHeli, INT iHeli)
	PRINTLN("[survival] INIT_AIR_VEH - Setting up Air Vehicle ", iHeli)
	VEHICLE_INDEX viAirVeh = NET_TO_VEH(sSurvivalHeli.netId)
	IF DOES_ENTITY_EXIST(viAirVeh)
		SET_VEHICLE_DOORS_LOCKED(viAirVeh, VEHICLELOCK_LOCKED)
	    SET_HELI_BLADES_FULL_SPEED(viAirVeh)
		SET_VEHICLE_ENGINE_ON(viAirVeh, TRUE, TRUE)
		ACTIVATE_PHYSICS(viAirVeh)
		SET_ENTITY_MAX_HEALTH(viAirVeh, g_FMMC_STRUCT.sSurvivalWaveData.iAirHealth[iWaveToUse][iHeli])
		SET_VEHICLE_BODY_HEALTH(viAirVeh, TO_FLOAT(g_FMMC_STRUCT.sSurvivalWaveData.iAirHealth[iWaveToUse][iHeli]))
		
		SET_ENTITY_HEALTH(viAirVeh, g_FMMC_STRUCT.sSurvivalWaveData.iAirHealth[iWaveToUse][iHeli])
		
		IF g_FMMC_STRUCT.sSurvivalWaveData.iAirColour[iWaveToUse][iHeli] != -1
		AND g_FMMC_STRUCT.sSurvivalWaveData.iAirColour[iWaveToUse][iHeli] != ciNUMBER_OF_VEHICLE_COLOURS
			FMMC_SET_THIS_VEHICLE_COLOURS(viAirVeh, GET_VEHICLE_COLOUR_FROM_SELECTION(g_FMMC_STRUCT.sSurvivalWaveData.iAirColour[iWaveToUse][iHeli]), -1)
		ENDIF
		
		MODEL_NAMES mnAirVeh = GET_ENTITY_MODEL(viAirVeh)
		IF IS_THIS_MODEL_A_PLANE(mnAirVeh)
			SET_VEHICLE_FORWARD_SPEED(viAirVeh, 60.0)
			IF GET_VEHICLE_HAS_LANDING_GEAR(viAirVeh)
				CONTROL_LANDING_GEAR(viAirVeh, LGC_RETRACT_INSTANT)
			ENDIF
			
			IF mnAirVeh = HYDRA
				SET_VEHICLE_FLIGHT_NOZZLE_POSITION_IMMEDIATE(viAirVeh, 0.0)
			ENDIF
		ENDIF
		
		IF IS_THIS_MODEL_A_HELI(mnAirVeh)
		AND IS_COMBAT_VEHICLE(viAirVeh)
			SET_ARRIVE_DISTANCE_OVERRIDE_FOR_VEHICLE_PERSUIT_ATTACK(viAirVeh, 10)
			PRINTLN("INIT_AIR_VEH - Setting Arrive distance override for vehicle persuit attack for air vehicle ", iHeli)
		ENDIF
		
		SWITCH mnAirVeh
			CASE POLMAV
				SET_VEHICLE_LIVERY(viAirVeh, 0) //Keeps it as a police vehicle rather than an Air Ambulance
			BREAK
			CASE AKULA
			CASE ANNIHILATOR2
				SET_DEPLOY_FOLDING_WINGS(viAirVeh, TRUE, TRUE)
			BREAK
			CASE CARGOBOB
			CASE CARGOBOB2
				SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(viAirVeh,SC_DOOR_REAR_LEFT, FALSE)
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

FUNC BOOL SPAWN_SURVIVAL_AIR_VEH(STRUCT_SURVIVAL_HELI_DATA &sSurvivalHeli, INT iHeli)
	
	sSurvivalHeli.eModel = g_FMMC_STRUCT.sSurvivalWaveData.mnAir[iWaveToUse][iHeli]
	MODEL_NAMES mnPedModel = g_FMMC_STRUCT.sSurvivalWaveData.mnPedModel[iWaveToUse][eWAVESTAGE_EASY]
	PED_INDEX piPed
    IF REQUEST_LOAD_MODEL(sSurvivalHeli.eModel)
        IF REQUEST_LOAD_MODEL(sSurvivalHeli.sPed[0].eModel)
            
            IF NOT IS_NET_VEHICLE_DRIVEABLE(sSurvivalHeli.netId)
        
                IF NETWORK_DOES_NETWORK_ID_EXIST(sSurvivalHeli.netId)
				OR CREATE_NET_VEHICLE(sSurvivalHeli.netId, sSurvivalHeli.eModel, sSurvivalHeli.vSpawnCoords, 0.0, TRUE)
                    
                    INIT_AIR_VEH(sSurvivalHeli, iHeli)
					
					INT iPed, iNumCreated
					INT iSeats = 3
					IF GET_VEHICLE_MODEL_NUMBER_OF_SEATS(sSurvivalHeli.eModel) < 3
						iSeats = GET_VEHICLE_MODEL_NUMBER_OF_SEATS(sSurvivalHeli.eModel)
					ENDIF
					iSeats = GET_FORCED_NUMBER_OF_VEHICLE_SEATS(sSurvivalHeli.eModel, iSeats)
					PRINTLN("SPAWN_SURVIVAL_AIR_VEH - Spawning Air Vehicle ", iHeli, " seats: ", iSeats)
					FOR iPed = 0 TO iSeats-1
						REQUEST_MODEL(mnPedModel)
						IF CREATE_NET_PED_IN_VEHICLE(sSurvivalHeli.sPed[iPed].netId, sSurvivalHeli.netId, PEDTYPE_CIVMALE, mnPedModel, GET_AIR_VEHICLE_SEAT(iPed, sSurvivalHeli.eModel, iSeats), TRUE)
							piPed = NET_TO_PED(sSurvivalHeli.sPed[iPed].netId)
							VEHICLE_INDEX viVeh = NET_TO_VEH(sSurvivalHeli.netId)
							IF iped = 0
								//Set Driver properties
								SET_PED_RELATIONSHIP_GROUP_HASH(piPed, rgFM_AiHate)
		                        SET_PED_COMBAT_ATTRIBUTES(piPed, CA_ALWAYS_FIGHT, TRUE)
		                        SET_PED_COMBAT_MOVEMENT(piPed, CM_WILLADVANCE)
								SET_PED_CONFIG_FLAG(piPed, PCF_GetOutBurningVehicle, FALSE)
								SET_PED_CONFIG_FLAG(piPed, PCF_GetOutUndriveableVehicle, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(piPed, CA_LEAVE_VEHICLES, FALSE)
								SET_PED_AS_ENEMY(piPed, TRUE)
								GIVE_DELAYED_WEAPON_TO_PED(piPed, WEAPONTYPE_COMBATMG, 15000, TRUE)
								IF (IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(viVeh))
								AND DOES_VEHICLE_HAVE_WEAPONS(viVeh))
								OR IS_COMBAT_VEHICLE(viVeh)
									SET_PED_COMBAT_ATTRIBUTES(piPed, CA_USE_VEHICLE_ATTACK, TRUE)
									SET_PED_COMBAT_ATTRIBUTES(piPed, CA_USE_VEHICLE_ATTACK_IF_VEHICLE_HAS_MOUNTED_GUNS, TRUE)
									SET_PED_ACCURACY(piPed, 3)
									SET_PED_SHOOT_RATE(piPed, 100)
									SET_PED_FIRING_PATTERN(piPed,FIRING_PATTERN_BURST_FIRE_HELI)
									SET_PED_COMBAT_ATTRIBUTES(piPed, CA_CAN_SHOOT_WITHOUT_LOS, FALSE)
									IF IS_COMBAT_HELI_WITH_MISSILES(viVeh)
										PRINTLN("SPAWN_SURVIVAL_AIR_VEH - Combat Heli Pilot combat settings changed")
										SET_PED_SHOOT_RATE(piPed, 50)
										SET_PED_COMBAT_ABILITY(piPed, CAL_POOR)
									ENDIF
								ENDIF
								IF IS_FAKE_MULTIPLAYER_MODE_SET()
									TASK_COMBAT_PED(piPed, localPlayerPed, COMBAT_PED_PREVENT_CHANGING_TARGET)
								ENDIF
								iNumCreated++
								PRINTLN("SPAWN_SURVIVAL_AIR_VEH - Veh ", iHeli, " Passengers created: ", iNumCreated, "/", iSeats)
							ELSE
								SET_PED_RELATIONSHIP_GROUP_HASH(piPed, rgFM_AiHate)
	                            GIVE_DELAYED_WEAPON_TO_PED(piPed, WEAPONTYPE_COMBATMG, 15000, TRUE)
	                            SET_PED_COMBAT_ATTRIBUTES(piPed, CA_DO_DRIVEBYS, TRUE)
	                            SET_PED_COMBAT_ATTRIBUTES(piPed, CA_ALWAYS_FIGHT, TRUE)
	                            SET_PED_COMBAT_MOVEMENT(piPed, CM_WILLADVANCE)
	                            SET_PED_FIRING_PATTERN(piPed,FIRING_PATTERN_BURST_FIRE_HELI)
								SET_PED_CONFIG_FLAG(piPed, PCF_GetOutBurningVehicle, FALSE)
								SET_PED_CONFIG_FLAG(piPed, PCF_GetOutUndriveableVehicle, FALSE)
								SET_PED_COMBAT_ATTRIBUTES(piPed, CA_LEAVE_VEHICLES, FALSE)
								SET_PED_AS_ENEMY(piPed, TRUE)
								SET_PED_ACCURACY(piPed, 9)
								
								VEHICLE_SEAT vSeat = GET_SEAT_PED_IS_IN(piPed)
								IF IS_TURRET_SEAT(NET_TO_VEH(sSurvivalHeli.netId), vSeat)
									SET_PED_COMBAT_ATTRIBUTES(piPed, CA_USE_VEHICLE_ATTACK, TRUE)
									SET_PED_COMBAT_ATTRIBUTES(piPed, CA_USE_VEHICLE_ATTACK_IF_VEHICLE_HAS_MOUNTED_GUNS, TRUE)
									SET_PED_ACCURACY(piPed, 5)
									SET_PED_SHOOT_RATE(piPed, 100)
									SET_PED_CONFIG_FLAG(piPed, PCF_DontLeaveVehicleIfLeaderNotInVehicle, TRUE)
									SET_PED_COMBAT_ATTRIBUTES(piPed, CA_LEAVE_VEHICLES, FALSE)
									PRINTLN("[survival] SPAWN_SURVIVAL_AIR_VEH - Ped ",iPed," Air Vehicle ", iHeli," is in turret seat")
								ENDIF
								IF IS_COMBAT_HELI_WITH_MISSILES(viVeh)
									PRINTLN("SPAWN_SURVIVAL_AIR_VEH - Combat Heli Passenger combat settings changed")
									SET_PED_ACCURACY(piPed, 3)
									SET_PED_SHOOT_RATE(piPed, 50)
									SET_PED_COMBAT_ABILITY(piPed, CAL_POOR)
								ENDIF
								IF IS_FAKE_MULTIPLAYER_MODE_SET()
									TASK_COMBAT_PED(piPed, localPlayerPed, COMBAT_PED_PREVENT_CHANGING_TARGET)
								ENDIF
								iNumCreated++
								PRINTLN("SPAWN_SURVIVAL_AIR_VEH - Veh ", iHeli, " Passengers created: ", iNumCreated, "/", iSeats)
							ENDIF
						ELSE
							BREAKLOOP
						ENDIF
					ENDFOR
					IF iNumCreated = iSeats
						serverBD.iNumAliveHelis++
						SET_MODEL_AS_NO_LONGER_NEEDED(sSurvivalHeli.eModel)
						SET_MODEL_AS_NO_LONGER_NEEDED(mnPedModel)
						PRINTLN("SPAWN_SURVIVAL_AIR_VEH - Air vehicle successfully created")
						serverBD.iNumVehiclesCreatedThisStage[serverBD.sWaveData.eStage]++
						PRINTLN("[survival] SPAWN_SURVIVAL_AIR_VEH - Incrementing serverBD.iNumVehiclesCreatedThisStage[",serverBD.sWaveData.eStage,"] to ", serverBD.iNumVehiclesCreatedThisStage[serverBD.sWaveData.eStage])
						RETURN TRUE
					ENDIF
                ENDIF
            ENDIF
        ENDIF
    ENDIF
    
    RETURN FALSE
    
ENDFUNC

PROC MAINTAIN_ACTIVATING_LAND_VEHICLES()
	INT iVeh
	IF serverBD.sWaveData.iNumLandVehicles = 0
		EXIT
	ENDIF
	FOR iVeh = 0 TO serverBD.sWaveData.iNumLandVehicles-1
		IF serverBD_Enemies_V.sSurvivalLandVehicle[iVeh].eState = eSURVIVALLANDVEHICLESTATE_DOES_NOT_EXIST
			IF (serverBD.sWaveData.eStage = eWAVESTAGE_MEDIUM 
			AND GET_TOTAL_ALIVE_GROUND_BASED_ENEMY_PEDS() <= 5)
			OR serverBD.sWaveData.eStage = eWAVESTAGE_HARD
				PRINTLN("[Survival] - [AI Spawning] - MAINTAIN_ACTIVATING_LAND_VEHICLES - activating Vehicle ", iVeh)
				serverBD_Enemies_V.sSurvivalLandVehicle[iVeh].eState = eSURVIVALLANDVEHICLESTATE_SPAWNING
			ENDIF
		ENDIF
	ENDFOR
	
ENDPROC

PROC DEACTIVATE_LAND_VEHICLES()
	INT iVeh
	FOR iVeh = 0 TO serverBD.sWaveData.iNumLandVehicles-1
		IF serverBD_Enemies_V.sSurvivalLandVehicle[iVeh].eState = eSURVIVALLANDVEHICLESTATE_DOES_NOT_EXIST
			PRINTLN("[Survival] - [AI Spawning] - DEACTIVATE_LAND_VEHICLES - Deactivating Vehicle ", iVeh)
			serverBD_Enemies_V.sSurvivalLandVehicle[iVeh].eState = eSURVIVALLANDVEHICLESTATE_DOES_NOT_EXIST
		ENDIF
	ENDFOR
ENDPROC

PROC MAINTAIN_ACTIVATING_HELIS()
    INT iHeli
	IF g_FMMC_STRUCT.sSurvivalWaveData.iNumberOfHelis[iWaveToUse] = 0
	OR (IS_FAKE_MULTIPLAYER_MODE_SET() AND g_iTestType = 1)
		EXIT
	ENDIF
	FOR iHeli = 0 TO g_FMMC_STRUCT.sSurvivalWaveData.iNumberOfHelis[iWaveToUse]-1
		IF NOT serverBD_Enemies_V.sSurvivalHeli[iHeli].bActive
			IF serverBD.sWaveData.eStage >= eWAVESTAGE_MEDIUM
				PRINTLN("[Survival] - [AI Spawning] - MAINTAIN_ACTIVATING_HELIS - activating heli ", iHeli)
				serverBD_Enemies_V.sSurvivalHeli[iHeli].bActive = TRUE
			ENDIF
		ENDIF
	ENDFOR
    
ENDPROC

FUNC BOOL IS_HELI_WITHIN_ATTACK_RANGE_OF_SURVIVAL(NETWORK_INDEX niHeli)
	
	PARTICIPANT_INDEX partId
	PLAYER_INDEX playerId
	PED_INDEX pedId
	VEHICLE_INDEX viHeli
	VECTOR vHeliCoords
	
	IF NOT IS_NET_VEHICLE_DRIVEABLE(niHeli)
		RETURN FALSE
	ENDIF
	
	viHeli = NET_TO_VEH(niHeli)
	vHeliCoords = GET_ENTITY_COORDS(viHeli)
	
	IF (GET_DISTANCE_BETWEEN_COORDS(vHeliCoords, serverBD.vStartPos[0]) <= 150.0)
		PRINTLN("[Survival] - [AI Spawning] - heli is <= 150.0m from survival.")
		RETURN TRUE
	ENDIF
	
	IF (serverBD.sFightingParticipantsList.iParticipant[iHeliAttackRangePartCount] != -1)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_NATIVE(PARTICIPANT_INDEX, serverBD.sFightingParticipantsList.iParticipant[iHeliAttackRangePartCount]))
			IF NOT IS_BIT_SET(serverBD.sFightingParticipantsList.iPotentialFighterBitset, serverBD.sFightingParticipantsList.iParticipant[iHeliAttackRangePartCount])
				
				partId = INT_TO_NATIVE(PARTICIPANT_INDEX, serverBD.sFightingParticipantsList.iParticipant[iHeliAttackRangePartCount])
				
				IF NOT IS_PLAYER_SCTV(playerId)
					IF NOT IS_PLAYER_SPECTATING(playerId)
						
						playerId = NETWORK_GET_PLAYER_INDEX(partId)
						pedId = GET_PLAYER_PED(playerId)
						
						IF DOES_ENTITY_EXIST(pedId)
							IF NOT IS_PED_INJURED(pedId)
								IF (GET_DISTANCE_BETWEEN_ENTITIES(viHeli, pedId) <= 200.0)
									PRINTLN("[Survival] - [AI Spawning] - heli is <= 200.0m from participant ", serverBD.sFightingParticipantsList.iParticipant[iHeliAttackRangePartCount])
									RETURN TRUE
								ENDIF
							ENDIF
						ENDIF
						
					ENDIF
				ENDIF
			
			ENDIF
		ENDIF
	ENDIF
	
	iHeliAttackRangePartCount++
	
	IF (iHeliAttackRangePartCount >= MAX_NUMBER_HORDE_FIGHTING_PARTICIPANTS)
		iHeliAttackRangePartCount = 0
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC REMOVE_HELI_BLIP(INT iHeli)
    IF DOES_BLIP_EXIST(biHeliBlip[iHeli])
        REMOVE_BLIP(biHeliBlip[iHeli])
    ENDIF
ENDPROC

PROC SET_SURVIVAL_HELI_STATE(STRUCT_SURVIVAL_HELI_DATA &sSurvivalHeli, eSURVIVAL_HELI_STATE eNewState)
	DEBUG_PRINTCALLSTACK()
	PRINTLN("[Survival] - [SET_SURVIVAL_STATE] - [SET_SURVIVAL_HELI_STATE] - eOldState: ", GET_SURVIVAL_HELI_STATE_NAME(sSurvivalHeli.eState), " >>>> eNewState: ", GET_SURVIVAL_HELI_STATE_NAME(eNewState))
	sSurvivalHeli.eState = eNewState
ENDPROC

PROC PROCESS_HELI_BRAIN(STRUCT_SURVIVAL_HELI_DATA &sSurvivalHeli, INT iHeli)
    
    BOOL bResetState = TRUE
    STRUCT_SURVIVAL_HELI_DATA sTemp
	eSURVIVAL_HELI_STATE eTempState
	
	PRINTLN("PROCESS_HELI_BRAIN - Air vehicle: ", iHeli)
	
    SWITCH sSurvivalHeli.eState
        
        CASE eSURVIVALHELISTATE_NOT_EXIST
            
            IF sSurvivalHeli.bActive                
				SET_SURVIVAL_HELI_STATE(sSurvivalHeli, eSURVIVALHELISTATE_SPAWNING)
                PRINTLN("[Survival] - [AI Spawning] - sSurvivalHeli.bActive - setting heli state to eSURVIVALHELISTATE_SPAWNING")
            ENDIF
            
        BREAK
        
        CASE eSURVIVALHELISTATE_SPAWNING
          	
            IF (serverBd.iSearchingForChopper = (-1) AND (serverBd.iSearchingForPed = -1) AND (serverBd.iSearchingForLandVehicle = -1))
            OR (serverBd.iSearchingForChopper = iHeli AND (serverBd.iSearchingForPed = -1) AND (serverBd.iSearchingForLandVehicle = -1))
                IF FIND_SURVIVAL_HELI_SPAWN_COORDS(sSurvivalHeli)
                    IF SPAWN_SURVIVAL_AIR_VEH(sSurvivalHeli, iHeli)
                        SET_SEARCHING_FOR_AIR_VEHICLE(-1)
						serverBD.iNumCreatedEnemies++
                        PRINTLN("[survival] serverBD.iNumCreatedEnemies: ", serverBD.iNumCreatedEnemies)
						SET_SURVIVAL_HELI_STATE(sSurvivalHeli, eSURVIVALHELISTATE_FLYING_TO_SURVIVAL)
                        PRINTLN("[Survival] - [AI Spawning] - heli spawned - setting heli state to eSURVIVALHELISTATE_FLYING_TO_SURVIVAL")
                    ELSE
                        PRINTLN("[Survival] - [AI Spawning] - found suitable spawn coords, spawning heli.")
                    ENDIF
                ELSE
                    PRINTLN("[Survival] - [AI Spawning] - finding heli spawn coords.")
                ENDIF
            ENDIF
                
        BREAK
        
		CASE eSURVIVALHELISTATE_FLYING_TO_SURVIVAL
			
			IF NOT IS_NET_VEHICLE_DRIVEABLE(sSurvivalHeli.netId)
				IF IS_NET_PED_INJURED(sSurvivalHeli.sPed[0].netId)
				AND IS_NET_PED_INJURED(sSurvivalHeli.sPed[1].netId)
				AND IS_NET_PED_INJURED(sSurvivalHeli.sPed[2].netId)
	                SERVER_DECREMENT_REMAINING_AIR_VEHICLES()
					SERVER_INCREMENT_KILLS_THIS_WAVE()
					SERVER_INCREMENT_TOTAL_KILLS()
                	SERVER_INCREMENT_KILLS_THIS_SUB_STAGE()
					RESET_NET_TIMER(sSurvivalHeli.stBlownUpTimer)
					START_NET_TIMER(sSurvivalHeli.stBlownUpTimer)
					SET_SURVIVAL_HELI_STATE(sSurvivalHeli, eSURVIVALHELISTATE_MAKE_SURE_BLOWN_UP)
	                PRINTLN("[Survival] - [AI Spawning] - heli not driveable - setting heli state to eSURVIVALHELISTATE_MAKE_SURE_BLOWN_UP")
				ENDIF
            ELIF IS_NET_PED_INJURED(sSurvivalHeli.sPed[0].netId)
				IF IS_NET_PED_INJURED(sSurvivalHeli.sPed[1].netId)
				AND IS_NET_PED_INJURED(sSurvivalHeli.sPed[2].netId)
	                SERVER_DECREMENT_REMAINING_AIR_VEHICLES()
					SERVER_INCREMENT_KILLS_THIS_WAVE()
					SERVER_INCREMENT_TOTAL_KILLS()
                	SERVER_INCREMENT_KILLS_THIS_SUB_STAGE()
					RESET_NET_TIMER(sSurvivalHeli.stBlownUpTimer)
					START_NET_TIMER(sSurvivalHeli.stBlownUpTimer)
					SET_SURVIVAL_HELI_STATE(sSurvivalHeli, eSURVIVALHELISTATE_MAKE_SURE_BLOWN_UP)
	                PRINTLN("[Survival] - [AI Spawning] - pilot - setting heli state to eSURVIVALHELISTATE_MAKE_SURE_BLOWN_UP")
				ENDIF
            ENDIF
			
			IF IS_HELI_WITHIN_ATTACK_RANGE_OF_SURVIVAL(sSurvivalHeli.netId)
			OR NOT IS_THIS_MODEL_A_HELI(sSurvivalHeli.eModel)
				SET_SURVIVAL_HELI_STATE(sSurvivalHeli, eSURVIVALHELISTATE_FLYING_FIGHTING)
            	PRINTLN("[Survival] - [AI Spawning] - heli spawned - setting heli state to eSURVIVALHELISTATE_FLYING_FIGHTING")
			ENDIF
			
		BREAK
		
        CASE eSURVIVALHELISTATE_FLYING_FIGHTING
              
            IF NOT IS_NET_VEHICLE_DRIVEABLE(sSurvivalHeli.netId)
                SERVER_DECREMENT_REMAINING_AIR_VEHICLES()
				SERVER_INCREMENT_KILLS_THIS_WAVE()
				SERVER_INCREMENT_TOTAL_KILLS()
            	SERVER_INCREMENT_KILLS_THIS_SUB_STAGE()
				RESET_NET_TIMER(sSurvivalHeli.stBlownUpTimer)
				START_NET_TIMER(sSurvivalHeli.stBlownUpTimer)
				SET_SURVIVAL_HELI_STATE(sSurvivalHeli, eSURVIVALHELISTATE_MAKE_SURE_BLOWN_UP)
                PRINTLN("[Survival] - [AI Spawning] - heli not driveable - setting heli state to eSURVIVALHELISTATE_MAKE_SURE_BLOWN_UP")
            ELIF IS_VEHICLE_EMPTY(NET_TO_VEH(sSurvivalHeli.netId), TRUE)
                SERVER_DECREMENT_REMAINING_AIR_VEHICLES()
				SERVER_INCREMENT_KILLS_THIS_WAVE()
				SERVER_INCREMENT_TOTAL_KILLS()
            	SERVER_INCREMENT_KILLS_THIS_SUB_STAGE()
				RESET_NET_TIMER(sSurvivalHeli.stBlownUpTimer)
				START_NET_TIMER(sSurvivalHeli.stBlownUpTimer)
				SET_SURVIVAL_HELI_STATE(sSurvivalHeli, eSURVIVALHELISTATE_MAKE_SURE_BLOWN_UP)
                PRINTLN("[Survival] - [AI Spawning] - pilot - setting heli state to eSURVIVALHELISTATE_MAKE_SURE_BLOWN_UP")
            ENDIF
                
        BREAK
        
		CASE eSURVIVALHELISTATE_MAKE_SURE_BLOWN_UP
			REMOVE_HELI_BLIP(iHeli)
			IF NETWORK_DOES_NETWORK_ID_EXIST(sSurvivalHeli.netId)
				IF IS_ENTITY_DEAD(NET_TO_VEH(sSurvivalHeli.netId))
					CLEANUP_NET_ID(sSurvivalHeli.netId)
					SET_SURVIVAL_HELI_STATE(sSurvivalHeli, eSURVIVALHELISTATE_DEAD)
					PRINTLN("[Survival] - [AI Spawning] - pilot - setting heli state to eSURVIVALHELISTATE_DEAD")
				ENDIF
			ENDIF
			
		BREAK
		
        CASE eSURVIVALHELISTATE_DEAD
            INT i
			FOR i = 0 TO NUM_HELI_PEDS-1
				IF NETWORK_DOES_NETWORK_ID_EXIST(sSurvivalHeli.sPed[i].netId)
					CLEANUP_NET_ID(sSurvivalHeli.sPed[i].netId)
					bResetState = FALSE
					PRINTLN("[Survival] - [AI Spawning] - [eSURVIVALLANDVEHICLESTATE_DRIVING] - iHeli: ", iHeli, " iPed: ", i, " Still exists Not resetting state yet...")
					BREAKLOOP
				ENDIF
			ENDFOR
			IF NETWORK_DOES_NETWORK_ID_EXIST(sSurvivalHeli.netId)
				PRINTLN("[Survival] - [AI Spawning] - [eSURVIVALLANDVEHICLESTATE_DRIVING] - iHeli: ", iHeli, " Still exists Not resetting state yet...")
                bResetState = FALSE
            ENDIF
            
            IF bResetState
                
				eTempState						= sSurvivalHeli.eState
                sSurvivalHeli                   = sTemp
                SET_SEARCHING_FOR_AIR_VEHICLE(-1)
                sSurvivalHeli.eState			= eTempState
				
                SWITCH iHeli
                    CASE 0  serverBD_Enemies_V.sSurvivalHeli[0].iCircCount = SURVIVAL_HELI_0_START_CIRC_COUNT        BREAK
                    CASE 1  serverBD_Enemies_V.sSurvivalHeli[1].iCircCount = SURVIVAL_HELI_1_START_CIRC_COUNT        BREAK
                    CASE 2  serverBD_Enemies_V.sSurvivalHeli[2].iCircCount = SURVIVAL_HELI_2_START_CIRC_COUNT        BREAK
                ENDSWITCH
                
                IF serverBD.sWaveData.eStage = eWAVESTAGE_EASY
					SET_SURVIVAL_HELI_STATE(sSurvivalHeli, eSURVIVALHELISTATE_NOT_EXIST)                    
                ENDIF
                
            ENDIF
            
        BREAK
        
    ENDSWITCH
    
ENDPROC

PROC TRACK_NUM_ALIVE_LAND_VEHICLE_PEDS()
	
	INT i, j
	
	serverBD.iNumAliveLandVehiclePeds = 0
	
	REPEAT MAX_NUM_LAND_VEHICLES i
		REPEAT NUM_LAND_VEH_PEDS j
			IF NOT IS_NET_PED_INJURED(serverBD_Enemies_V.sSurvivalLandVehicle[i].sPed[j].netId)
				serverBD.iNumAliveLandVehiclePeds++
				PRINTLN("TRACK_NUM_ALIVE_LAND_VEHICLE_PEDS - Adding SCRIPT_PED_", NETWORK_ENTITY_GET_OBJECT_ID(NET_TO_ENT(serverBD_Enemies_V.sSurvivalLandVehicle[i].sPed[j].netId)))
			ENDIF
		ENDREPEAT
	ENDREPEAT 
	
ENDPROC


PROC ADD_HELI_BLIP(NETWORK_INDEX netId, INT iHeli)
	
	IF NOT DOES_BLIP_EXIST(biHeliBlip[iHeli])
	AND GET_BLIP_FROM_ENTITY(NET_TO_VEH(netId)) = NULL
	AND IS_NET_VEHICLE_DRIVEABLE(netId)
		PRINTLN("[survival] - ADD_HELI_BLIP - Adding blip for air vehicle ", iHeli)
        biHeliBlip[iHeli] = ADD_BLIP_FOR_ENTITY(NET_TO_VEH(netId))
		BLIP_SPRITE BlipSprite = GET_CORRECT_PED_BLIP_SPRITE_FOR_VEHICLE_MODEL(g_FMMC_STRUCT.sSurvivalWaveData.mnAir[iWaveToUse][iHeli])
		IF (BlipSprite != GET_STANDARD_BLIP_ENUM_ID())
		AND BlipSprite != RADAR_TRACE_INVALID
			SET_BLIP_SPRITE(biHeliBlip[iHeli], BlipSprite)
			SET_BLIP_COLOUR(biHeliBlip[iHeli], BLIP_COLOUR_HUDCOLOUR_RED)
		ELSE
			SET_BLIP_SPRITE(biHeliBlip[iHeli], RADAR_TRACE_ENEMY_HELI_SPIN)
		ENDIF
    ENDIF
					
ENDPROC

PROC HANDLE_HELI_SEARCHLIGHTS(NETWORK_INDEX netId)
	VEHICLE_INDEX viVeh = NET_TO_VEH(netId)
	
	// Sort searchlights.
	IF DOES_ENTITY_EXIST(viVeh)
		IF GET_ENTITY_MODEL(viVeh) != BUZZARD
		OR GET_ENTITY_MODEL(viVeh) != BUZZARD2
			EXIT
		ENDIF
		IF NOT DOES_VEHICLE_HAVE_WEAPONS(viVeh)
			EXIT
		ENDIF
		IF DOES_TOD_NEED_HELI_SEARCHLIGHTS()
			IF NOT IS_VEHICLE_SEARCHLIGHT_ON(viVeh)
				SET_VEHICLE_SEARCHLIGHT(viVeh, TRUE)
			ENDIF
		ELSE
			IF IS_VEHICLE_SEARCHLIGHT_ON(viVeh)
				SET_VEHICLE_SEARCHLIGHT(viVeh, FALSE)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_HELI_BODY(STRUCT_SURVIVAL_HELI_DATA &sSurvivalHeli, INT iHeli)
    
    PED_INDEX pedId
	VEHICLE_INDEX viHeli
	VEHICLE_MISSION eVehicleMission
	VECTOR vHeliGoto
	INT iPed
	
    SWITCH sSurvivalHeli.eState
            
            CASE eSURVIVALHELISTATE_NOT_EXIST
                REMOVE_HELI_BLIP(iHeli)
            BREAK
            
            CASE eSURVIVALHELISTATE_SPAWNING
                REMOVE_HELI_BLIP(iHeli)
            BREAK
            
			CASE eSURVIVALHELISTATE_FLYING_TO_SURVIVAL
				
				// If heli is still flying.
                IF IS_NET_VEHICLE_DRIVEABLE(sSurvivalHeli.netId)
					
					// Get Ids.
					viHeli = NET_TO_VEH(sSurvivalHeli.netId)
					eVehicleMission = GET_ACTIVE_VEHICLE_MISSION_TYPE(viHeli)
					
					// Handle searchlights and collision.
					IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(sSurvivalHeli.netId)
						
						IF IS_THIS_MODEL_A_HELI(sSurvivalHeli.eModel)
							HANDLE_HELI_SEARCHLIGHTS(sSurvivalHeli.netId)
						ENDIF
						
						// If the ped gets stuck waiting on collision, load it around them so they can get moving again.
						IF IS_ENTITY_WAITING_FOR_WORLD_COLLISION(NET_TO_VEH(sSurvivalHeli.netId))
							SET_ENTITY_LOAD_COLLISION_FLAG(NET_TO_VEH(sSurvivalHeli.netId), TRUE)
						ENDIF
						
						// Blip the heli.
						ADD_HELI_BLIP(sSurvivalHeli.netId, iHeli)
						
					ENDIF
					
					// Tell the heli to fly towards the survival.
					IF NOT IS_NET_PED_INJURED(sSurvivalHeli.sPed[0].netId)
						
						// Get data.
						pedId = NET_TO_PED(sSurvivalHeli.sPed[0].netId)
						vHeliGoto = serverBD.vStartPos[0] + << 0.0, 0.0, 20.0 >>
						
						// Get flight height.
						IF fHeightMapHeight = -1.0
							fHeightMapHeight = GET_APPROX_HEIGHT_FOR_POINT(vHeliGoto.x, vHeliGoto.y) + 10.0
						ENDIF
						
						// If not flying towards the survival, do it.
						IF (eVehicleMission != MISSION_GOTO)
						AND IS_THIS_MODEL_A_HELI(sSurvivalHeli.eModel)
							TASK_HELI_MISSION(pedId, viHeli, NULL, NULL,  vHeliGoto, MISSION_GOTO, 20.0, 40.0, -1.0, CEIL(fHeightMapHeight), 10)
							PRINTLN("[Survival] - [AI Spawning] - given heli ", iHeli, " task MISSION_GOTO with coords ", vHeliGoto, " and flight height ", fHeightMapHeight)
						ENDIF
						
					ENDIF
				
				ENDIF
				
			BREAK
			
            CASE eSURVIVALHELISTATE_FLYING_FIGHTING
                
				// If heli is stil flying.
                IF IS_NET_VEHICLE_DRIVEABLE(sSurvivalHeli.netId)
                   	
					IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(sSurvivalHeli.netId)
						
						HANDLE_HELI_SEARCHLIGHTS(sSurvivalHeli.netId)
						
						// If the ped gets stuck waiting opn collision, load it around them so they can get moving again.
						IF IS_ENTITY_WAITING_FOR_WORLD_COLLISION(NET_TO_VEH(sSurvivalHeli.netId))
							SET_ENTITY_LOAD_COLLISION_FLAG(NET_TO_VEH(sSurvivalHeli.netId), TRUE)
						ENDIF
						
					ENDIF
					
					// Blip the heli.
                    ADD_HELI_BLIP(sSurvivalHeli.netId, iHeli)
                    
					FOR iPed = 0 TO NUM_HELI_PEDS-1
						IF NOT IS_NET_PED_INJURED(sSurvivalHeli.sPed[iPed].netId)
	                        IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(sSurvivalHeli.sPed[iPed].netId)
	                            pedId = NET_TO_PED(sSurvivalHeli.sPed[iPed].netId)
	                            IF NOT IS_PED_IN_COMBAT(pedId)	
									IF GET_SCRIPT_TASK_STATUS(pedId, SCRIPT_TASK_COMBAT_HATED_TARGETS_IN_AREA) != PERFORMING_TASK
									AND GET_SCRIPT_TASK_STATUS(pedId, SCRIPT_TASK_COMBAT_HATED_TARGETS_IN_AREA) != WAITING_TO_START_TASK
	                              		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedId, FALSE)
										IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
	                                		TASK_COMBAT_HATED_TARGETS_IN_AREA(pedId, GET_ENTITY_COORDS(pedId), 299.0)
										ELSE
											TASK_COMBAT_PED(pedId, localPlayerPed, COMBAT_PED_PREVENT_CHANGING_TARGET)
										ENDIF
										PRINTLN("PROCESS_HELI_BODY - SCRIPT_PED_", NETWORK_ENTITY_GET_OBJECT_ID(pedId)," retasked with combat as was out of combat.")
									ENDIF
	                            ENDIF
								// If the ped gets stuck waiting opn collision, load it around them so they can get moving again.
								IF IS_ENTITY_WAITING_FOR_WORLD_COLLISION(pedId)
									SET_ENTITY_LOAD_COLLISION_FLAG(pedId, TRUE)
								ENDIF
	                        ENDIF
	                    ENDIF
					ENDFOR
                        
                ENDIF
                
				// If heli or pilot are dead, kill crew.
				IF NOT IS_NET_VEHICLE_DRIVEABLE(sSurvivalHeli.netId)
				OR IS_NET_PED_INJURED(sSurvivalHeli.sPed[0].netId)
					
					FOR iPed = 0 TO NUM_HELI_PEDS-1
						IF NOT IS_NET_PED_INJURED(sSurvivalHeli.sPed[iPed].netId)
	                        IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(sSurvivalHeli.sPed[iPed].netId)
	                            pedId = NET_TO_PED(sSurvivalHeli.sPed[iPed].netId)
								IF DOES_ENTITY_EXIST(pedId)
	                           		SET_ENTITY_HEALTH(pedId, 0, sSurvivalHeli.piLastDamager)
								ENDIF
	                        ENDIF
	                    ENDIF
					ENDFOR					
				ENDIF
				
            BREAK
            
			CASE eSURVIVALHELISTATE_MAKE_SURE_BLOWN_UP
				
				IF NETWORK_DOES_NETWORK_ID_EXIST(sSurvivalHeli.netId)
					IF IS_NET_VEHICLE_DRIVEABLE(sSurvivalHeli.netId)
						IF HAS_NET_TIMER_STARTED(sSurvivalHeli.stBlownUpTimer)
							IF HAS_NET_TIMER_EXPIRED(sSurvivalHeli.stBlownUpTimer, 10000)
								IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(sSurvivalHeli.netId)
									NETWORK_EXPLODE_HELI(NET_TO_VEH(sSurvivalHeli.netId), TRUE, FALSE, sSurvivalHeli.netId)
									PRINTLN("[Survival] - alive timer expired, calling EXPLODE_VEHICLE to make sure it's dead.")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
			BREAK
			
            CASE eSURVIVALHELISTATE_DEAD
                
                REMOVE_HELI_BLIP(iHeli)
                    
            BREAK
            
    ENDSWITCH
	
	IF NOT IS_NET_VEHICLE_DRIVEABLE(sSurvivalHeli.netId)
		REMOVE_HELI_BLIP(iHeli)
	ELSE
		IF DOES_BLIP_EXIST(biHeliBlip[iHeli])
			IF SHOULD_BLIP_BE_MANUALLY_ROTATED(biHeliBlip[iHeli])
				IF IS_NET_VEHICLE_DRIVEABLE(sSurvivalHeli.netId)
					SET_BLIP_ROTATION(biHeliBlip[iHeli], ROUND(GET_ENTITY_HEADING(NET_TO_VEH(sSurvivalHeli.netId))))
				ENDIF
			ENDIF
		ENDIF
		viHeli = NET_TO_VEH(sSurvivalHeli.netId)
		IF DOES_ENTITY_EXIST(viHeli)
			IF GET_VEHICLE_BODY_HEALTH(viHeli) <= 0.0
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(sSurvivalHeli.netId)
					INT iCulprit = -1
					IF DOES_ENTITY_EXIST(sSurvivalHeli.piLastDamager)
						iCulprit = NATIVE_TO_INT(NETWORK_GET_PLAYER_INDEX_FROM_PED(sSurvivalHeli.piLastDamager))
					ENDIF
					PRINTLN("PROCESS_HELI_BODY - Air vehicle ", iHeli, " body health = zero. Explosion caused by player: ", iCulprit)
					NETWORK_EXPLODE_VEHICLE(viHeli, TRUE, TRUE, iCulprit)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
    
ENDPROC

PROC MAINTAIN_SURVIVAL_HELIS_SERVER()
    
    INT i
    
    MAINTAIN_ACTIVATING_HELIS()
    	
    REPEAT g_FMMC_STRUCT.sSurvivalWaveData.iNumberOfHelis[iWaveToUse] i
        PROCESS_HELI_BRAIN(serverBD_Enemies_V.sSurvivalHeli[i], i)
    ENDREPEAT
    
ENDPROC

//////////////
///    Land Vehicles
/////////////

	
FUNC BOOL IS_PED_LEAVING_VEHICLE(VEHICLE_INDEX vehId, PED_INDEX ped)
	
	IF GET_VEHICLE_PED_IS_USING(ped) = vehId
		IF NOT IS_PED_SITTING_IN_VEHICLE(ped, vehId)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC SET_SURVIVAL_LAND_VEHICLE_STATE(STRUCT_SURVIVAL_LAND_VEHICLE_DATA &sSurvivalLandVehicle, eSURVIVAL_LAND_VEHICLE_STATE eNewState)
	DEBUG_PRINTCALLSTACK()
	PRINTLN("[Survival] - [SET_SURVIVAL_STATE] - [SET_SURVIVAL_LAND_VEHICLE_STATE] - eOldState: ", GET_LAND_VEHICLE_STATE_NAME(sSurvivalLandVehicle.eState), " >>>> eNewState: ", GET_LAND_VEHICLE_STATE_NAME(eNewState))
	sSurvivalLandVehicle.eState = eNewState
ENDPROC

FUNC BOOL HAS_VEH_SPAWNED_THIS_FRAME()
	RETURN IS_BIT_SET(serverBd.ibitset, BITSET_SERVER_VEHICLE_SPAWNED_THIS_FRAME)
ENDFUNC

PROC PROCESS_LAND_VEHICILE_BRAIN(STRUCT_SURVIVAL_LAND_VEHICLE_DATA &sSurvivalLandVehicle, INT iVehicle)
	
	INT i
    BOOL bResetState = TRUE
    STRUCT_SURVIVAL_LAND_VEHICLE_DATA sTemp
	eSURVIVAL_LAND_VEHICLE_STATE eState
	
	FOR i = 0 TO serverBD_Enemies_V.sSurvivalLandVehicle[iVehicle].iNumPeds-1
    	IF sSurvivalLandVehicle.sPed[i].bDied
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(sSurvivalLandVehicle.sPed[i].netId)
				CLEANUP_NET_ID(sSurvivalLandVehicle.sPed[i].netId)
			ENDIF
		ENDIF
	ENDFOR
	
    SWITCH sSurvivalLandVehicle.eState
    	
        CASE eSURVIVALLANDVEHICLESTATE_SPAWNING
			IF NOT HAS_VEH_SPAWNED_THIS_FRAME()
			AND NOT sSurvivalLandVehicle.bSpawned
	            IF (serverBd.iSearchingForLandVehicle = (-1) AND (serverBd.iSearchingForPed = -1) AND (serverBd.iSearchingForChopper = -1))
	            OR (serverBd.iSearchingForLandVehicle = iVehicle AND (serverBd.iSearchingForPed = -1) AND (serverBd.iSearchingForChopper = -1))
					SET_SEARCHING_FOR_LAND_VEHICLE(iVehicle)
					PRINTLN("[Survival] - [AI Spawning] - PROCESS_LAND_VEHICILE_BRAIN - serverBd.iSearchingForLandVehicle = ", serverBd.iSearchingForLandVehicle)
	                IF FIND_LAND_VEHICLE_SPAWN_COORDS(sSurvivalLandVehicle, iVehicle)
	                    IF SPAWN_SURVIVAL_LAND_VEHICLE(sSurvivalLandVehicle, iVehicle)
							serverBD.iNumCreatedEnemies += sSurvivalLandVehicle.iNumPeds
	                        SET_SEARCHING_FOR_LAND_VEHICLE(-1)
							SET_SURVIVAL_LAND_VEHICLE_STATE(sSurvivalLandVehicle, eSURVIVALLANDVEHICLESTATE_DRIVING)
							SET_BIT(serverBd.ibitset, BITSET_SERVER_VEHICLE_SPAWNED_THIS_FRAME)
							sSurvivalLandVehicle.bSpawned = TRUE
							PRINTLN("[survival] - [AI Spawning] - PROCESS_LAND_VEHICILE_BRAIN - Setting BITSET_SERVER_VEHICLE_SPAWNED_THIS_FRAME")
	                        PRINTLN("[Survival] - [AI Spawning] - PROCESS_LAND_VEHICILE_BRAIN - land vehicle ", iVehicle," spawned - setting state to eSURVIVALLANDVEHICLESTATE_DRIVING")
	                    ELSE
	                        PRINTLN("[Survival] - [AI Spawning] - PROCESS_LAND_VEHICILE_BRAIN - found suitable spawn coords, spawning land vehicle ", iVehicle)
	                    ENDIF
	                ELSE
	                    PRINTLN("[Survival] - [AI Spawning] - PROCESS_LAND_VEHICILE_BRAIN - Failed to find spawn coords for vehicle ", iVehicle)
	                ENDIF
	            ENDIF
            ENDIF
        BREAK
        
        CASE eSURVIVALLANDVEHICLESTATE_DRIVING
           	
			// If all the peds in the vehicle have been killed, the vehicle is now considered dead.
			BOOL bAllDead
			bAllDead = TRUE
			FOR i = 0 TO serverBD_Enemies_V.sSurvivalLandVehicle[iVehicle].iNumPeds-1
				IF NOT IS_NET_PED_INJURED(sSurvivalLandVehicle.sPed[i].netId)
					PRINTLN("[Survival] - [AI Spawning] - [eSURVIVALLANDVEHICLESTATE_DRIVING] - iVehicle: ", iVehicle, " iPed: ", i, " is not dead.")
					bAllDead = FALSE
				ELSE
					IF NOT sSurvivalLandVehicle.sPed[i].bDied
						PRINTLN("[Survival] - SCRIPT_PED_", NETWORK_ENTITY_GET_OBJECT_ID(NET_TO_PED(sSurvivalLandVehicle.sPed[i].netId)), " has just died. Vehicle Ped.")
						SERVER_INCREMENT_KILLS_THIS_WAVE()
						SERVER_INCREMENT_TOTAL_KILLS()
						IF sSurvivalLandVehicle.sPed[i].bMelee
							serverBD.iNumberOfMeleeEnemies--
						ENDIF
						sSurvivalLandVehicle.sPed[i].bDied = TRUE
					ENDIF
				ENDIF
				
			ENDFOR
			
			IF bAllDead
				PRINTLN("[Survival] - [AI Spawning] - [eSURVIVALLANDVEHICLESTATE_DRIVING] - iVehicle: ", iVehicle, " All peds are dead")
				
				serverBD.iKills += sSurvivalLandVehicle.iNumPeds
                serverBD.iKillsThisWaveSubStage[serverBd.sWaveData.eStage] += sSurvivalLandVehicle.iNumPeds
				CLEANUP_NET_ID(serverBD_Enemies_V.sSurvivalLandVehicle[iVehicle].netId)
				SET_SURVIVAL_LAND_VEHICLE_STATE(sSurvivalLandVehicle, eSURVIVALLANDVEHICLESTATE_DEAD)
			ENDIF
			
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(sSurvivalLandVehicle.netId)
				IF DOES_ENTITY_EXIST(NET_TO_VEH(sSurvivalLandVehicle.netId))
					IF IS_VEHICLE_EMPTY(NET_TO_VEH(sSurvivalLandVehicle.netId), DEFAULT, DEFAULT, DEFAULT, TRUE)
					AND NOT sSurvivalLandVehicle.bEmpty
						sSurvivalLandVehicle.bEmpty = TRUE
						PRINTLN("[Survival] - PROCESS_LAND_VEHICILE_BRAIN - Vehicle ", iVehicle, " is now empty")
					ENDIF
				ENDIF
			ENDIF
        BREAK
        
		CASE eSURVIVALLANDVEHICLESTATE_ARRIVED
			// Keep this here for if we need to do anything special for the arrived state.
		BREAK
		
        CASE eSURVIVALLANDVEHICLESTATE_DEAD
            FOR i = 0 TO serverBD_Enemies_V.sSurvivalLandVehicle[iVehicle].iNumPeds-1
				IF NETWORK_DOES_NETWORK_ID_EXIST(sSurvivalLandVehicle.sPed[i].netId)
					bResetState = FALSE
					PRINTLN("[Survival] - [AI Spawning] - [eSURVIVALLANDVEHICLESTATE_DRIVING] - iVehicle: ", iVehicle, " iPed: ", i, " Still exists Not resetting state yet...")
					BREAKLOOP
				ENDIF
			ENDFOR
            
            IF bResetState
				
				eState									= sSurvivalLandVehicle.eState
                sSurvivalLandVehicle                  	= sTemp
				SET_SEARCHING_FOR_LAND_VEHICLE(-1)
				sSurvivalLandVehicle.eState				= eState
				
                IF serverBD.sWaveData.eStage = eWAVESTAGE_EASY
					SET_SURVIVAL_LAND_VEHICLE_STATE(sSurvivalLandVehicle, eSURVIVALLANDVEHICLESTATE_DOES_NOT_EXIST)
                ENDIF
                
            ENDIF
            
        BREAK
        
    ENDSWITCH
	
ENDPROC

PROC PROCESS_LAND_VEHICLE_BODY(STRUCT_SURVIVAL_LAND_VEHICLE_DATA &sSurvivalLandVehicle, INT iPed, INT iVeh)
    
    PED_INDEX pedId
    VEHICLE_INDEX vehId
	BOOL bVehDriveable, bLeavingVehicle, bNeedToLeaveVehicle
	
    SWITCH sSurvivalLandVehicle.eState
            
            CASE eSURVIVALLANDVEHICLESTATE_DOES_NOT_EXIST
                    
            BREAK
            
            CASE eSURVIVALLANDVEHICLESTATE_SPAWNING
                  
            BREAK
            
            CASE eSURVIVALLANDVEHICLESTATE_DRIVING
                
                IF NOT IS_NET_PED_INJURED(sSurvivalLandVehicle.sPed[iPed].netId)
					
					// Setup various flags about ped and vehicle before doing rest of body processing.
					pedId = NET_TO_PED(sSurvivalLandVehicle.sPed[iPed].netId)
					
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(sSurvivalLandVehicle.netId)
						
						vehId = NET_TO_VEH(sSurvivalLandVehicle.netId)
						
						bVehDriveable = IS_VEHICLE_DRIVEABLE(vehId)
						
						IF IS_PED_LEAVING_VEHICLE(vehId, pedId)
							bLeavingVehicle = TRUE
						ENDIF
						
						IF bVehDriveable
							IF IS_PED_IN_VEHICLE(pedId, vehId)
								IF IS_ENTITY_ON_FIRE(vehId)
									bNeedToLeaveVehicle = TRUE
								ENDIF
							ENDIF
						ELSE
							IF IS_PED_IN_ANY_VEHICLE(pedID)
								bNeedToLeaveVehicle = TRUE
							ENDIF
						ENDIF
						
						IF HAS_NET_TIMER_STARTED(serverBD_Enemies_V.stBailFromTurretTimer[iVeh])
							IF HAS_NET_TIMER_EXPIRED(serverBD_Enemies_V.stBailFromTurretTimer[iVeh], 30000)
								bNeedToLeaveVehicle = TRUE
								SET_PED_COMBAT_ATTRIBUTES(pedId, CA_LEAVE_VEHICLES, TRUE)
								PRINTLN("PROCESS_LAND_VEHICLE_BODY - Stuck in turret without driver")
							ENDIF
						ENDIF
						
					ENDIF
					
					// Process ped if I have control of him.
                    IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(sSurvivalLandVehicle.sPed[iPed].netId)
						
						// If on foot and with no weapon in hand, put a weapon in hand so they don't flee.
						IF GET_PEDS_CURRENT_WEAPON(pedId) = WEAPONTYPE_UNARMED
						AND NOT IS_PED_IN_ANY_VEHICLE(pedId)
							SETUP_PED_FOR_WAVE(0,0,pedId,TRUE, DEFAULT, DEFAULT, iVeh)
							IF IS_WEAPON_A_MELEE_WEAPON(serverBD_Enemies_V.sSurvivalLandVehicle[iVeh].sPed[iPed].wtWeapon)
								PRINTLN("[Survival] - SPAWN_SURVIVAL_LAND_VEHICLE - Driver - ", GET_WEAPON_NAME(serverBD_Enemies_V.sSurvivalLandVehicle[iVeh].sPed[iPed].wtWeapon))
								serverBD_Enemies_V.sSurvivalLandVehicle[iVeh].sPed[iPed].bMelee = TRUE
								serverBD.iNumberOfMeleeEnemies++
								PRINTLN("[Survival] - PROCESS_LAND_VEHICLE_BODY - serverBD.iNumberOfMeleeEnemies: ", serverBD.iNumberOfMeleeEnemies, " Wave: ", serverBD.sWaveData.iWaveCount, " Stage: ", serverBD.sWaveData.eStage)
							ENDIF
							PRINTLN("[Survival] - land ped has become unarmed, giving weapon to ped. Land Ped = ", iPed)
						ENDIF
					
						// If not in the process of leaving the vehicle, task him to combat.
                        IF NOT IS_PED_IN_COMBAT(pedId)
							IF NOT bLeavingVehicle
								IF bNeedToLeaveVehicle
									IF GET_SCRIPT_TASK_STATUS(pedId, SCRIPT_TASK_LEAVE_ANY_VEHICLE) != PERFORMING_TASK
									AND GET_SCRIPT_TASK_STATUS(pedId, SCRIPT_TASK_LEAVE_ANY_VEHICLE) != WAITING_TO_START_TASK
										PRINTLN("PROCESS_LAND_VEHICLE_BODY - SCRIPT_PED_", NETWORK_ENTITY_GET_OBJECT_ID(pedId)," tasking to exit vehicle.")
										TASK_LEAVE_ANY_VEHICLE(pedId, 0, ECF_DONT_CLOSE_DOOR | ECF_DONT_WAIT_FOR_VEHICLE_TO_STOP | ECF_RESUME_IF_INTERRUPTED)
									ENDIF
								ELSE
									IF GET_SCRIPT_TASK_STATUS(pedId, SCRIPT_TASK_COMBAT_HATED_TARGETS_IN_AREA) != PERFORMING_TASK
									AND GET_SCRIPT_TASK_STATUS(pedId, SCRIPT_TASK_COMBAT_HATED_TARGETS_IN_AREA) != WAITING_TO_START_TASK
		                            	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedId, FALSE)
		                            	TASK_COMBAT_HATED_TARGETS_IN_AREA(pedId, GET_ENTITY_COORDS(pedId), 299.0)
										PRINTLN("PROCESS_LAND_VEHICLE_BODY - SCRIPT_PED_", NETWORK_ENTITY_GET_OBJECT_ID(pedId)," retasked with combat as was out of combat.")
									ENDIF
								ENDIF
							ENDIF
                        ENDIF
						IF NOT IS_PED_IN_ANY_VEHICLE(pedId)
							IF GET_PED_ALERTNESS(pedId) = AS_NOT_ALERT
								SET_PED_ALERTNESS(pedId, AS_MUST_GO_TO_COMBAT)
								PRINTLN("PROCESS_LAND_VEHICLE_BODY - SCRIPT_PED_", NETWORK_ENTITY_GET_OBJECT_ID(pedId)," setting alert state as not alert.")
							ENDIF
						ENDIF
						
						// If the ped gets stuck waiting on collision, load it around them so they can get moving again.
						// Don't have to worry about if in a vehicle because they have special ai for navigating at long range.
						IF NOT IS_PED_IN_ANY_VEHICLE(pedId)
							IF IS_ENTITY_WAITING_FOR_WORLD_COLLISION(pedId)
								SET_ENTITY_LOAD_COLLISION_FLAG(pedId, TRUE)
							ENDIF
						ENDIF
						
					ENDIF
					
                ENDIF
				
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(sSurvivalLandVehicle.netId)
					IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(sSurvivalLandVehicle.netId)
					AND IS_ENTITY_ALIVE(vehId)
					AND NOT IS_VEHICLE_EMPTY(vehId)
						IF IS_VEHICLE_STUCK_TIMER_UP(vehId, VEH_STUCK_ON_ROOF, ROOF_TIME)
						OR IS_VEHICLE_STUCK_TIMER_UP(vehId, VEH_STUCK_ON_SIDE, 15000)
						OR IS_VEHICLE_STUCK_TIMER_UP(vehId, VEH_STUCK_HUNG_UP, 15000)
						OR IS_VEHICLE_STUCK_TIMER_UP(vehId, VEH_STUCK_JAMMED, 15000)
					    	SET_VEHICLE_ENGINE_HEALTH(vehId, -1000)
							SET_VEHICLE_PETROL_TANK_HEALTH(vehId, -1000)
							SET_VEHICLE_BODY_HEALTH(vehId, -1000)
							PRINTLN("PROCESS_LAND_VEHICLE_BODY - A stuck timer failed.")
						ENDIF
	          	  	ENDIF
				ENDIF
                
            BREAK
            
			CASE eSURVIVALLANDVEHICLESTATE_ARRIVED
				
			BREAK
			
            CASE eSURVIVALLANDVEHICLESTATE_DEAD
                     
            BREAK
            
    ENDSWITCH
    
ENDPROC

PROC MAINTAIN_SURVIVAL_LAND_VEHICLES_SERVER()
	
	INT i
	
	MAINTAIN_ACTIVATING_LAND_VEHICLES()
	
	IF IS_BIT_SET(serverBd.ibitset, BITSET_SERVER_VEHICLE_SPAWNED_THIS_FRAME)
		CLEAR_BIT(serverBd.ibitset, BITSET_SERVER_VEHICLE_SPAWNED_THIS_FRAME)
		PRINTLN("[survival] - MAINTAIN_SURVIVAL_LAND_VEHICLES_SERVER - Clearing BITSET_SERVER_VEHICLE_SPAWNED_THIS_FRAME")
	ENDIF
	
	REPEAT serverBD.sWaveData.iNumLandVehicles i
        PROCESS_LAND_VEHICILE_BRAIN(serverBD_Enemies_V.sSurvivalLandVehicle[i], i)
    ENDREPEAT
	
ENDPROC
