USING "FM_Survival_Controller_Using.sch"
USING "FM_Survival_Controller_Utility.sch"

PROC MAINTAIN_NO_VEHICLE_ENEMIES_IN_SURVIVAL_AREA_FAILSAFE()
	
	INT iPed, iVehicle
	
	CLEAR_BIT(serverBD.iBitset, BITSET_SERVER_START_VEHICLE_FAIL_SAFE_TIMER)
	VEHICLE_INDEX viTemp
	IF (serverBd.iNumAliveSquadEnemies <= 0)
	AND (serverBD.iNumAliveHelis <= 0)
		REPEAT MAX_NUM_LAND_VEHICLES iVehicle
			IF IS_NET_VEHICLE_DRIVEABLE(serverBD_Enemies_V.sSurvivalLandVehicle[iVehicle].netId)
				viTemp = NET_TO_VEH(serverBD_Enemies_V.sSurvivalLandVehicle[iVehicle].netId)
				IF (DOES_ENTITY_EXIST(viTemp) AND IS_COMBAT_VEHICLE(viTemp))
				OR HAS_NET_TIMER_STARTED(serverBD_Enemies_V.stBailFromTurretTimer[iVehicle])
					CLEAR_BIT(serverBD.iBitset, BITSET_SERVER_START_VEHICLE_FAIL_SAFE_TIMER)
					BREAKLOOP
				ENDIF
				REPEAT serverBD_Enemies_V.sSurvivalLandVehicle[iVehicle].iNumPeds iPed
					IF NOT IS_NET_PED_INJURED(serverBD_Enemies_V.sSurvivalLandVehicle[iVehicle].sPed[iPed].netId)
						IF IS_PED_IN_ANY_VEHICLE(NET_TO_PED(serverBD_Enemies_V.sSurvivalLandVehicle[iVehicle].sPed[iPed].netId))
							IF IS_PED_OUT_OF_BOUNDS(NET_TO_PED(serverBD_Enemies_V.sSurvivalLandVehicle[iVehicle].sPed[iPed].netId))
								IF NOT IS_BIT_SET(serverBD.iBitset, BITSET_SERVER_START_VEHICLE_FAIL_SAFE_TIMER)
									PRINTLN("[Survival] - set serverBD.iBitset, BITSET_SERVER_START_VEHICLE_FAIL_SAFE_TIMER for vehicle ", iVehicle, ", ped ", iPed)
									SET_BIT(serverBD.iBitset, BITSET_SERVER_START_VEHICLE_FAIL_SAFE_TIMER)
								ENDIF
							ENDIF		
						ENDIF
					ENDIF
				ENDREPEAT
			ENDIF
		ENDREPEAT
	ENDIF
	
	IF IS_BIT_SET(serverBD.iBitset, BITSET_SERVER_START_VEHICLE_FAIL_SAFE_TIMER)
		IF NOT HAS_NET_TIMER_STARTED(serverBD_Enemies_V.stVehicleNotInAreaTimer)
			START_NET_TIMER(serverBD_Enemies_V.stVehicleNotInAreaTimer)
			PRINTLN("[Survival] - serverBD.iBitset, BITSET_SERVER_START_VEHICLE_FAIL_SAFE_TIMER = TRUE, starting timer stVehicleNotInAreaTimer.")
		ELSE
			IF HAS_NET_TIMER_EXPIRED(serverBD_Enemies_V.stVehicleNotInAreaTimer, 60000)
				PRINTLN("[Survival] - set stVehicleNotInAreaTimer been running for 30 seconds, setting serverBD.iBitset, BITSET_SERVER_ALL_VEHICLE_PEDS_STUCK_OOB.")
				SET_BIT(serverBD.iBitset, BITSET_SERVER_ALL_VEHICLE_PEDS_STUCK_OOB)
			ENDIF
		ENDIF
	ELSE
		RESET_NO_VEHICLE_ENEMIES_IN_SURVIVAL_AREA_TIMER()
	ENDIF
	
ENDPROC

PROC MAINTAIN_TURRET_PED_STUCK_OUT_OF_SURVIVAL_AREA()
	BOOL bStartTimer
	INT iVehicle, iPed
	REPEAT MAX_NUM_LAND_VEHICLES iVehicle
		bStartTimer = FALSE
		IF IS_NET_VEHICLE_DRIVEABLE(serverBD_Enemies_V.sSurvivalLandVehicle[iVehicle].netId)
			REPEAT serverBD_Enemies_V.sSurvivalLandVehicle[iVehicle].iNumPeds iPed
				IF NOT IS_NET_PED_INJURED(serverBD_Enemies_V.sSurvivalLandVehicle[iVehicle].sPed[iPed].netId)
					PED_INDEX tempPed = NET_TO_PED(serverBD_Enemies_V.sSurvivalLandVehicle[iVehicle].sPed[iPed].netId)
					IF IS_PED_IN_ANY_VEHICLE(tempPed)
						IF GET_PED_IN_VEHICLE_SEAT(GET_VEHICLE_PED_IS_IN(tempPed), VS_DRIVER) = NULL
							IF IS_PED_OUT_OF_BOUNDS(tempPed)
								bStartTimer = TRUE
								IF NOT HAS_NET_TIMER_STARTED(serverBD_Enemies_V.stBailFromTurretTimer[iVehicle])
									PRINTLN("MAINTAIN_TURRET_PED_STUCK_OUT_OF_SURVIVAL_AREA - Starting turret stuck timer for vehicle ", iVehicle)
									REINIT_NET_TIMER(serverBD_Enemies_V.stBailFromTurretTimer[iVehicle])
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
		
		IF !bStartTimer
			PRINTLN("MAINTAIN_TURRET_PED_STUCK_OUT_OF_SURVIVAL_AREA - Clearing turret stuck timer for vehicle ", iVehicle)
			RESET_NET_TIMER(serverBD_Enemies_V.stBailFromTurretTimer[iVehicle])
		ENDIF
	ENDREPEAT
ENDPROC

FUNC WEAPON_TYPE PICK_WEAPON_TYPE_FOR_ENEMY(INT iWave)
	INT iWep
	WEAPON_TYPE wtWep
	
	iWep = GET_RANDOM_INT_IN_RANGE(0, SURV_HEAVY_WEAPON_INDEX)
	wtWep = g_FMMC_STRUCT.sSurvivalWaveData.wtEnemyWeapons[iWave][iWep]
	
	RETURN wtWep
ENDFUNC

FUNC COMBAT_ABILITY_LEVEL GET_COMBAT_LEVEL(INT iWave, INT iStage)
	IF IS_THIS_ENDLESS_WAVES()
	AND HAVE_ENDLESS_WAVES_STARTED()
		RETURN CAL_PROFESSIONAL
	ENDIF
	
	RETURN INT_TO_ENUM(COMBAT_ABILITY_LEVEL, g_FMMC_STRUCT.sSurvivalWaveData.iCombatAbility[iWave][iStage])
ENDFUNC

PROC SETUP_PED_FOR_WAVE(INT iSquad, INT iPed, PED_INDEX piPassed, BOOL bRearmingPed = FALSE, BOOL bTurretPed = FALSE, BOOL bIsHeavyUnit = FALSE, INT iVeh = -1)
    
    INT iAccuracy
    INT iArmour
    INT iHealth
	INT iStage = ENUM_TO_INT(serverBD.sWaveData.eStage)
	INT iWave = iWaveToUse
    WEAPON_TYPE eWeapon
    COMBAT_ABILITY_LEVEL eCombatAbility
    
    FLOAT fPedHealth
	
    PRINTLN("[Survival] - [Populate Enemy Data] - Wave: ",iWave," iPed = ", iPed, ", iStage = ", iStage)
    PRINTLN("SETUP_PED_FOR_WAVE - Setting up SCRIPT_PED_", NETWORK_ENTITY_GET_OBJECT_ID(piPassed))
	iAccuracy 		= g_FMMC_STRUCT.sSurvivalWaveData.iAccuracy[iWave][iStage] + serverBD.sEndlessBonuses.iAccuracy[iStage]
	iArmour			= g_FMMC_STRUCT.sSurvivalWaveData.iArmour[iWave][iStage] + serverBD.sEndlessBonuses.iArmour[iStage]
	iHealth			= g_FMMC_STRUCT.sSurvivalWaveData.iHealth[iWave][iStage] + serverBD.sEndlessBonuses.iHealth[iStage]
	eCombatAbility	= GET_COMBAT_LEVEL(iWave, iStage)
	PRINTLN("[Survival] - [Populate Enemy Data] - iAccuracy: ", iAccuracy, " iArmour:", iArmour, " iHealth: ", iHealth, " eCombatAbility: ", eCombatAbility)
	eWeapon 		= PICK_WEAPON_TYPE_FOR_ENEMY(iWave)
    
	serverBD_Enemies_S.structAiSquad[iSquad][iStage].sPed[iPed].iAccuracy = iAccuracy
	
    SET_PED_ACCURACY(piPassed, serverBD_Enemies_S.structAiSquad[iSquad][iStage].sPed[iPed].iAccuracy)
    SET_PED_COMBAT_ABILITY(piPassed, eCombatAbility)
    
    SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_ALWAYS_FIGHT, TRUE)      
    SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_CAN_FLANK, TRUE) 
    SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_CAN_CHARGE, TRUE)
	SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_DISABLE_SEEK_DUE_TO_LINE_OF_SIGHT, TRUE)
	SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_AGGRESSIVE, TRUE)
	SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_ALWAYS_FLEE, FALSE)
	SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_DISABLE_FLEE_FROM_COMBAT, TRUE)
	SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_DISABLE_ALL_RANDOMS_FLEE, TRUE)
	SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_REQUIRES_LOS_TO_AIM, TRUE)
	SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_CAN_FIGHT_ARMED_PEDS_WHEN_NOT_ARMED, TRUE)
	SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_DISABLE_PINNED_DOWN, TRUE)
	SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_MOVE_TO_LOCATION_BEFORE_COVER_SEARCH, TRUE)
	SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_CAN_SHOOT_WITHOUT_LOS, FALSE)
	
	SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_MAINTAIN_MIN_DISTANCE_TO_TARGET, TRUE)
	SET_COMBAT_FLOAT(piPassed, CCF_MIN_DISTANCE_TO_TARGET, GET_RANDOM_FLOAT_IN_RANGE(4.0, 15.0))
	
	SET_PED_FLEE_ATTRIBUTES(piPassed, FA_DISABLE_FLEE_FROM_INDIRECT_THREATS, TRUE)
	SET_PED_FLEE_ATTRIBUTES(piPassed, FA_NEVER_FLEE, TRUE)
	
	SET_PED_PATH_CAN_USE_LADDERS(piPassed, TRUE)
	
	SET_PED_CONFIG_FLAG(piPassed, PCF_GetOutBurningVehicle, FALSE)
	SET_PED_CONFIG_FLAG(piPassed,PCF_OnlyWritheFromWeaponDamage,TRUE)
	SET_PED_CONFIG_FLAG(piPassed, PCF_DisableGoToWritheWhenInjured, TRUE)
	SET_PED_CONFIG_FLAG(piPassed, PCF_GiveWeaponOnGetup, TRUE)
	SET_PED_CONFIG_FLAG(piPassed, PCF_DisableHurt, TRUE)
	
    SET_PED_COMBAT_MOVEMENT(piPassed, CM_WILLADVANCE)
	
	IF IS_ROCKSTAR_CREATED_MISSION()
		SET_PED_DIES_IN_WATER(piPassed, TRUE)
		SET_PED_MAX_TIME_IN_WATER(piPassed, 5.0)
		SET_PED_MAX_TIME_UNDERWATER(piPassed, 2.0)
		SET_PED_DIES_IN_SINKING_VEHICLE(piPassed, TRUE)
	ENDIF
	
	IF NOT bTurretPed
		SET_PED_COMBAT_RANGE(piPassed, CR_MEDIUM)
		SET_PED_SEEING_RANGE(piPassed, 45)
	ENDIF
	
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(piPassed, FALSE)
	
    SET_PED_TO_INFORM_RESPECTED_FRIENDS(piPassed, 299, 20)
    SET_PED_TARGET_LOSS_RESPONSE(piPassed, TLR_NEVER_LOSE_TARGET)
	
	IF bIsHeavyUnit
		SET_RAGDOLL_BLOCKING_FLAGS(piPassed, RBF_BULLET_IMPACT | RBF_PLAYER_IMPACT | RBF_MELEE | RBF_EXPLOSION)
		iHealth *= g_FMMC_STRUCT.sSurvivalWaveData.iHealthMultiplier[iWave][iStage]
		iArmour *= g_FMMC_STRUCT.sSurvivalWaveData.iArmourMultiplier[iWave][iStage]
		SET_PED_SUFFERS_CRITICAL_HITS(piPassed, FALSE)
		SET_PED_UPPER_BODY_DAMAGE_ONLY(piPassed, TRUE)
		eWeapon = g_FMMC_STRUCT.sSurvivalWaveData.wtEnemyWeapons[iWave][3]
	ENDIF
    
	IF NOT bRearmingPed
	    IF iArmour > 0
	        SET_PED_ARMOUR(piPassed, iArmour)
	    ENDIF
		fPedHealth = iHealth*g_sMPTunables.fAiHealthModifier
		iHealth = ROUND(fPedHealth)
		SET_ENTITY_MAX_HEALTH(piPassed, iHealth)
	    SET_ENTITY_HEALTH(piPassed, iHealth)
	ENDIF
	
	BOOL bForceFirstWeapon = TRUE
	
	IF eWeapon = WEAPONTYPE_INVALID
		eWeapon = WEAPONTYPE_PISTOL
	ENDIF
	
	GIVE_WEAPON_TO_PED(piPassed, eWeapon, 25000, bForceFirstWeapon, bForceFirstWeapon)
	
	IF iVeh != -1
		serverBD_Enemies_V.sSurvivalLandVehicle[iVeh].sPed[iPed].wtWeapon = eWeapon
	ENDIF
	
	IF IS_WEAPON_A_MELEE_WEAPON(eWeapon)
		SET_PED_CONFIG_FLAG(piPassed, PCF_ForceIgnoreMaxMeleeActiveSupportCombatants, TRUE)
		IF NOT bRearmingPed
		AND iVeh = -1
			serverBD_Enemies_S.structAiSquad[iSquad][iStage].sPed[iPed].bMelee = TRUE
			serverBD.iNumberOfMeleeEnemies++
			PRINTLN("[Survival] - [Populate Enemy Data] serverBD.iNumberOfMeleeEnemies: ", serverBD.iNumberOfMeleeEnemies, " Wave: ", serverBD.sWaveData.iWaveCount, " Stage: ", serverBD.sWaveData.eStage)
		ENDIF
	ENDIF
	
	IF eWeapon = WEAPONTYPE_MINIGUN
		SET_RAGDOLL_BLOCKING_FLAGS(piPassed, RBF_BULLET_IMPACT | RBF_PLAYER_IMPACT | RBF_MELEE)
	ENDIF
	
	IF IS_THIS_ENDLESS_WAVES()
	AND HAVE_ENDLESS_WAVES_STARTED()
	AND serverBD.sWaveData.iEndlessWave >= ciEndlessStage_HealthBonus
		SET_RAGDOLL_BLOCKING_FLAGS(piPassed, RBF_BULLET_IMPACT | RBF_PLAYER_IMPACT | RBF_MELEE)
	ENDIF
	
ENDPROC

PROC SET_UP_FMMC_PED_WK(PED_INDEX piPassed, INT iPedNumber,BOOL bGivePedWeapon = TRUE, BOOL bsetForCombat = TRUE)
    
    INT irepeat
    INT iAccuracy
    COMBAT_ABILITY_LEVEL eCombatAbility
    
    #IF IS_DEBUG_BUILD
   	TEXT_LABEL_23 tl23
    #ENDIF
    IF (g_FMMC_STRUCT.iMissionType = FMMC_TYPE_SURVIVAL) // If in horde mode, we want the weapon to always start as the pistol so we can increase difficulty later by giving better weapons.
        g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].gun = WEAPONTYPE_PISTOL
    ENDIF
    
    IF bGivePedWeapon
    	GIVE_DELAYED_WEAPON_TO_PED(piPassed, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].gun, 25000, TRUE)
    ENDIF
    FOR irepeat = 0 TO (FMMC_MAX_TEAMS-1)
        IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iTeam[irepeat] = ciPED_RELATION_SHIP_LIKE
            //iteamrel[irepeat] = FM_RelationshipLike 
            SET_PED_CAN_BE_TARGETTED_BY_TEAM(piPassed, irepeat, FALSE)
            SET_ENTITY_IS_TARGET_PRIORITY(piPassed, FALSE)
        ELIF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iTeam[irepeat] = ciPED_RELATION_SHIP_DISLIKE
            //iteamrel[irepeat] = FM_RelationshipDislike
            SET_PED_CAN_BE_TARGETTED_BY_TEAM(piPassed, irepeat, TRUE)
            SET_ENTITY_IS_TARGET_PRIORITY(piPassed, FALSE)
        ELIF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iTeam[irepeat] = ciPED_RELATION_SHIP_HATE
            //iteamrel[irepeat] = FM_RelationshipHate
            SET_PED_CAN_BE_TARGETTED_BY_TEAM(piPassed, irepeat, TRUE)
            SET_ENTITY_IS_TARGET_PRIORITY(piPassed, TRUE)
        ENDIF
    ENDFOR
    
    SET_PED_RELATIONSHIP_GROUP_HASH(piPassed, rgFM_AiHate)  //rgFM_AiDislike)
    
    #IF IS_DEBUG_BUILD
        tl23 = "niPed "
        tl23 += iPedNumber 
        SET_PED_NAME_DEBUG(piPassed, tl23)
    #ENDIF
    
    //Set Behaviours
    IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_SURVIVAL
        IF bsetForCombat
            iAccuracy = 10
            eCombatAbility = CAL_POOR
        ENDIF
    ELSE
        IF bsetForCombat
            IF g_FMMC_STRUCT.iDifficulity = FMMC_DIFFICULTY_EASY
                iAccuracy = 10
                eCombatAbility = CAL_POOR
            ELIF g_FMMC_STRUCT.iDifficulity = FMMC_DIFFICULTY_MEDIUM
                iAccuracy = 30
                eCombatAbility = CAL_AVERAGE
                ADD_ARMOUR_TO_PED(piPassed,50)
            ELIF g_FMMC_STRUCT.iDifficulity = FMMC_DIFFICULTY_HARD
                iAccuracy = 45
                eCombatAbility = CAL_PROFESSIONAL
                ADD_ARMOUR_TO_PED(piPassed,100)
            ENDIF
        ENDIF   
        SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(piPassed,FALSE)
    ENDIF
    
    IF bsetForCombat
        SET_PED_ACCURACY(piPassed, iAccuracy)
        SET_PED_COMBAT_ABILITY(piPassed, eCombatAbility)
        SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_ALWAYS_FIGHT, TRUE)
        SET_PED_COMBAT_MOVEMENT(piPassed, CM_WILLADVANCE)
    ENDIF
    
    SET_PED_DIES_WHEN_INJURED(piPassed,TRUE)
    SET_PED_TARGET_LOSS_RESPONSE(piPassed, TLR_NEVER_LOSE_TARGET)
    SET_PED_TO_INFORM_RESPECTED_FRIENDS(piPassed, 299, 20)
    
    IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].gun = WEAPONTYPE_MINIGUN
        SET_PED_FIRING_PATTERN(piPassed,FIRING_PATTERN_FULL_AUTO)
    ENDIF
	
	SET_PED_MONEY(piPassed, 0)
	SET_PED_CONFIG_FLAG(piPassed, PCF_BlockDroppingHealthSnacksOnDeath, TRUE)
	SET_HEALTH_SNACKS_CARRIED_BY_ALL_NEW_PEDS(0,0)
    IF GET_RANDOM_INT_IN_RANGE(1, 6) = 1
	AND NOT IS_PED_IN_ANY_VEHICLE(piPassed, TRUE)
		SET_PED_DROPS_WEAPONS_WHEN_DEAD(piPassed, TRUE)
	ELSE
		SET_PED_DROPS_WEAPONS_WHEN_DEAD(piPassed, FALSE)
	ENDIF
ENDPROC

FUNC BOOL CAN_THIS_SQUAD_SPAWNER_BE_SHARED(INT iSpawner, INT iSquad)
	INT iAttempts
	IF NOT IS_SQUAD_SPAWN_FREE_FOR_THIS_SUBSTAGE(iSpawner, iSquad, ENUM_TO_INT(serverBD.sWaveData.eStage))
		iAttempts++
		PRINTLN("FIND_VALID_SPAWN_POINT - CAN_THIS_SQUAD_SPAWNER_BE_SHARED - Another squad is also using this spawnpoint(",iSpawner,")")
		IF iAttempts >= 2
			PRINTLN("FIND_VALID_SPAWN_POINT - CAN_THIS_SQUAD_SPAWNER_BE_SHARED - RETURN FALSE - Spawner ", iSpawner, " has too many squads")
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF DOES_SQUAD_SPAWN_HAVE_TOO_MANY_ENEMIES_FOR_THIS_SUBSTAGE(iSpawner, iSquad, ENUM_TO_INT(serverBD.sWaveData.eStage))
		PRINTLN("FIND_VALID_SPAWN_POINT - CAN_THIS_SQUAD_SPAWNER_BE_SHARED - RETURN FALSE - Spawner ", iSpawner, " would have too many enemies")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC INT FIND_VALID_SPAWN_POINT(INT iSpawner, INT iSquad)
	IF IS_BIT_SET(serverBD.sWaveData.iSpawnChangeAttempted[ENUM_TO_INT(serverBD.sWaveData.eStage)], iSquad)
		RETURN iSpawner
	ENDIF
	INT iNewSpawner, iAttempts
	INT iFarthestSpawn = -1
	BOOL bSearch
	VECTOR vNewPos
	FLOAT fFarthest, fTempDist
	PRINTLN("FIND_VALID_SPAWN_POINT - ################")
	PRINTLN("FIND_VALID_SPAWN_POINT - Given spawner: ", iSpawner, " Wave ", serverBD.sWaveData.iWaveCount)
	IF ARE_ANY_PLAYERS_NEAR_COORD(serverBD.sWaveData.vSquadSpawnPos[iSpawner], 20)
		bSearch = TRUE
		PRINTLN("FIND_VALID_SPAWN_POINT - A player is too close to this spawn")
	ENDIF
	IF CAN_ANY_PLAYER_SEE_POINT(serverBD.sWaveData.vSquadSpawnPos[iSpawner], 20, TRUE, TRUE, 50.0)
		bSearch = TRUE
		PRINTLN("FIND_VALID_SPAWN_POINT - A player can see this spawn point")
	ENDIF
	
	IF bSearch
		PRINTLN("FIND_VALID_SPAWN_POINT - Looking for an alternative spawner for ", iSpawner)
		FOR iNewSpawner = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfPeds-1
			fTempDist = 0
			PRINTLN("FIND_VALID_SPAWN_POINT ------------------------ ")
			PRINTLN("FIND_VALID_SPAWN_POINT - Checking spawn ", iNewSpawner)
			vNewPos = serverBD.sWaveData.vSquadSpawnPos[iNewSpawner]
			IF NOT IS_VECTOR_ZERO(vNewPos)
				BOOL bPointOK = TRUE
				IF ARE_ANY_PLAYERS_NEAR_COORD(vNewPos)
					PRINTLN("FIND_VALID_SPAWN_POINT - Failing due to ARE_ANY_PLAYERS_NEAR_COORD Spawner: ", iNewSpawner)
					bPointOK = FALSE
				ENDIF
				IF CAN_ANY_PLAYER_SEE_POINT(vNewPos, 20, TRUE, TRUE, 50.0)
					PRINTLN("FIND_VALID_SPAWN_POINT - Failing due to CAN_ANY_PLAYER_SEE_POINT Spawner: ", iNewSpawner)
					bPointOK = FALSE
				ENDIF
				IF NOT CAN_THIS_SQUAD_SPAWNER_BE_SHARED(iNewSpawner, iSquad)
					PRINTLN("FIND_VALID_SPAWN_POINT - Failing due to CAN_THIS_SQUAD_SPAWNER_BE_SHARED Spawner: ", iNewSpawner)
					bPointOK = FALSE
				ENDIF
				IF bPointOK
					PRINTLN("FIND_VALID_SPAWN_POINT - Updating spawner ",iSpawner," to ", iNewSpawner)
					iSpawner = iNewSpawner
					BREAKLOOP
				ENDIF
			ENDIF
			fTempDist = VDIST_2D(serverBD.sWaveData.vSquadSpawnPos[iNewSpawner], serverBD.vBoundsMiddle)
			IF fTempDist > fFarthest
				fFarthest = fTempDist
				iFarthestSpawn = iNewSpawner
				PRINTLN("FIND_VALID_SPAWN_POINT - New Farthest spawner (",iFarthestSpawn,") at ", fFarthest)
			ENDIF
			iAttempts++
			PRINTLN("FIND_VALID_SPAWN_POINT - iAttempts: ", iAttempts,"/",g_FMMC_STRUCT_ENTITIES.iNumberOfPeds)
		ENDFOR
	ENDIF
	IF iAttempts >= g_FMMC_STRUCT_ENTITIES.iNumberOfPeds
	AND NOT IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SILO_03) //All spawning at one place in the silo causes peds to fall through the map.
		IF iFarthestSpawn != -1
			iSpawner = iFarthestSpawn
			PRINTLN("FIND_VALID_SPAWN_POINT - Updating spawner to Farthest spawner: ", iSpawner)
		ENDIF
	ENDIF
	SET_BIT(serverBD.sWaveData.iSpawnChangeAttempted[ENUM_TO_INT(serverBD.sWaveData.eStage)], iSquad)
	PRINTLN("FIND_VALID_SPAWN_POINT - Set bit for squad ", iSquad)
	RETURN iSpawner
ENDFUNC

FUNC BOOL SHOULD_ENEMY_SPAWN_AT_PLACED_POINT(INT iSpawnPoint)
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iSpawnPoint].iPedBitsetTwelve, ciPed_BSTwelve_PlacedOnAnEntity)
		PRINTLN("SHOULD_ENEMY_SPAWN_AT_PLACED_POINT - Spawn point ",iSpawnPoint," - ciPed_BSTwelve_PlacedOnAnEntity is set")
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SILO_03)
		PRINTLN("SHOULD_ENEMY_SPAWN_AT_PLACED_POINT - This Survival is in the Missile Silo.")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL GET_SPAWN_COORDS_FOR_ENEMY_PED(VECTOR &vspawnloc, FLOAT &fspawnhead, INT iSquad, INT iStage, INT iPed)
	UNUSED_PARAMETER(iPed)
	VECTOR vMin, vMax, vStartPos
	FLOAT fRadius

	vMin = g_FMMC_STRUCT.sSurvivalWaveData.vBounds[0]
	vMax = g_FMMC_STRUCT.sSurvivalWaveData.vBounds[1]
	fRadius = g_FMMC_STRUCT.sSurvivalWaveData.fBoundsRadius	
	
	vStartPos = serverBD.vStartPos[0]
	
	SPAWN_SEARCH_PARAMS SpawnSearchParams
	SpawnSearchParams.vFacingCoords = serverBD.vStartPos[0]
	SpawnSearchParams.fMinDistFromPlayer = 30.0
	SpawnSearchParams.bConsiderInteriors = TRUE
	SpawnSearchParams.bAvoidOtherPeds = TRUE
	
	PRINTLN("RESPAWN_MISSION_PED - ******* Squad ", iSquad, " Stage: ", iStage, " iPed: ", iPed)
	
	INT iSpawner = FIND_VALID_SPAWN_POINT(serverBD_Enemies_S.structAiSquad[iSquad][iStage].iSquadSpawnLocation, iSquad)
	IF serverBD_Enemies_S.structAiSquad[iSquad][iStage].iSquadSpawnLocation != iSpawner
		serverBD_Enemies_S.structAiSquad[iSquad][iStage].iSquadSpawnLocation = iSpawner
		PRINTLN("GET_SPAWN_COORDS_FOR_ENEMY_PED - serverBD_Enemies_S.structAiSquad[iSquad][iStage].iSquadSpawnLocation updated to ", serverBD_Enemies_S.structAiSquad[iSquad][iStage].iSquadSpawnLocation)
	ENDIF
	IF NOT IS_VECTOR_ZERO(serverBD.sWaveData.vSquadSpawnPos[iSpawner])
		IF SHOULD_ENEMY_SPAWN_AT_PLACED_POINT(iSpawner)
			PRINTLN("[Survival] - [AI Spawning] - RESPAWN_MISSION_PED - GET_SPAWN_COORDS_FOR_ENEMY_PED - Spawning directly at spawn loc ", iSpawner)
			vspawnloc = serverBD.sWaveData.vSquadSpawnPos[iSpawner]
			fspawnhead = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iSpawner].fHead
			RETURN TRUE
		ELSE
			PRINTLN("[Survival] - [AI Spawning] - RESPAWN_MISSION_PED - GET_SPAWN_COORDS_FOR_ENEMY_PED - Getting coords from spawn loc ", iSpawner)
			IF GET_SAFE_COORDS_IN_AREA_FOR_CREATING_ENTITY(serverBD.sWaveData.vSquadSpawnPos[iSpawner], cf_PedSpawnRadius, vspawnloc, fspawnhead, SpawnSearchParams)
				IF VDIST2(serverBD.sWaveData.vSquadSpawnPos[iSpawner], vspawnloc) > POW(cf_PedSpawnRadius, 2)
					PRINTLN("[Survival] - [AI Spawning] - RESPAWN_MISSION_PED - GET_SPAWN_COORDS_FOR_ENEMY_PED - Spawn found is far from point. Forcing to: ", iSpawner)
					vspawnloc = serverBD.sWaveData.vSquadSpawnPos[iSpawner]
					fspawnhead = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iSpawner].fHead
					RETURN TRUE
				ELSE
					PRINTLN("[Survival] - [AI Spawning] - RESPAWN_MISSION_PED - GET_SPAWN_COORDS_FOR_ENEMY_PED - Spawn point found: ", vspawnloc)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_VECTOR_ZERO(g_FMMC_STRUCT.sSurvivalWaveData.vBounds[0])
		PRINTLN("RESPAWN_MISSION_PED - Getting position from player's start pos")
		RETURN GET_SAFE_COORDS_IN_AREA_FOR_CREATING_ENTITY(vStartPos, 150.0, vspawnloc, fspawnhead, SpawnSearchParams)
	ENDIF
	
	IF NOT IS_VECTOR_ZERO(serverBD_Enemies_S.structAiSquad[iSquad][iStage].vSquadSpawnPos)
		PRINTLN("RESPAWN_MISSION_PED - Getting position from cached spawn location")
		RETURN GET_SAFE_COORDS_IN_AREA_FOR_CREATING_ENTITY(serverBD_Enemies_S.structAiSquad[iSquad][iStage].vSquadSpawnPos, 3.0, vspawnloc, fSpawnHead, SpawnSearchParams)
	ENDIF
	
	BOOL bReturn
	SWITCH g_FMMC_STRUCT.sSurvivalWaveData.iBoundsType
		CASE SURV_BOUNDS_NON_AXIS_AREA
			bReturn = GET_SAFE_COORDS_IN_ANGLED_AREA_FOR_CREATING_ENTITY(vMin, vMax, fRadius, vspawnloc, fspawnhead, SpawnSearchParams)
		BREAK
		CASE SURV_BOUNDS_CYLINDER
			bReturn = GET_SAFE_COORDS_IN_AREA_FOR_CREATING_ENTITY(vMin, fRadius, vspawnloc, fspawnhead, SpawnSearchParams)
		BREAK
	ENDSWITCH
	PRINTLN("RESPAWN_MISSION_PED - Getting position from spawn area")
	RETURN bReturn
	
ENDFUNC

FUNC INT GET_SPAWN_PED_UNIQUE_ID(INT iPed, INT iSquad)
	RETURN (iPed*(iSquad+1))
ENDFUNC

FUNC BOOL IS_THIS_PED_A_HEAVY(INT iped, INT iSquadId)
	INT iHeavy
	FOR iHeavy = 0 TO serverBD.sWaveData.iNumPedsInSquad[ENUM_TO_INT(serverBD.sWaveData.eStage)]-1
		IF serverBD_Enemies_S.sHeavyData.iHeavyPeds[ENUM_TO_INT(serverBD.sWaveData.eStage)][iSquadId][iHeavy] = iPed
			RETURN TRUE
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC

FUNC BOOL RESPAWN_MISSION_PED(INT iped, INT iSquadId)
    
    FLOAT fspawnhead
    VECTOR vTemp
	INT iStage = ENUM_TO_INT(serverBD.sWaveData.eStage)
	MODEL_NAMES eModel = g_FMMC_STRUCT.sSurvivalWaveData.mnPedModel[iWaveToUse][iStage]
	BOOL bIsHeavyUnit = IS_THIS_PED_A_HEAVY(iped, iSquadId)
	
	IF bIsHeavyUnit
		eModel = g_FMMC_STRUCT.sSurvivalWaveData.mnHeavyModel[iWaveToUse][iStage]
		serverBD_Enemies_S.structAiSquad[iSquadId][iStage].sPed[iPed].bHeavy = TRUE
	ENDIF
	
	#IF IS_DEBUG_BUILD
    IF bDoRespawnMissionPedPrints
    PRINTLN("[Survival] - [AI Spawning] - Spawn Process for ped: ", iped, " Squad ",iSquadId," Stage: ",iStage,".")
    ENDIF
    #ENDIF
	
    IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD_Enemies_S.structAiSquad[iSquadId][iStage].sPed[iPed].netId)
        
        #IF IS_DEBUG_BUILD
        IF bDoRespawnMissionPedPrints
        PRINTLN("[Survival] - [AI Spawning] - RESPAWN_MISSION_PED ped: ", iped, " net id does not exist.")
        ENDIF
        #ENDIF
        
        IF eModel != DUMMY_MODEL_FOR_SCRIPT
    
            #IF IS_DEBUG_BUILD
            IF bDoRespawnMissionPedPrints
            PRINTLN("[Survival] - [AI Spawning] - RESPAWN_MISSION_PED ped: ",iped, " model != DUMMY_MODEL_FOR_SCRIPT.")
            ENDIF
            #ENDIF
            
            REQUEST_MODEL(eModel)
            
            IF HAS_MODEL_LOADED(eModel)
                
                #IF IS_DEBUG_BUILD
                IF bDoRespawnMissionPedPrints
                PRINTLN("[Survival] - [AI Spawning] - RESPAWN_MISSION_PED ped: ", iped, " model loaded.")
                ENDIF
                #ENDIF
                
                IF CAN_REGISTER_MISSION_PEDS(1)
				AND serverBD.iNumAliveSquadEnemies < MAX_NUM_ACTIVE_SQUAD_PEDS
				AND serverBD.iNumberOfMeleeEnemies < (MAX_NUM_MELEE_ENEMIES_PER_PLAYER * serverBD.sFightingParticipantsList.iNumFightingParticipants)
                    
                    #IF IS_DEBUG_BUILD
                    IF bDoRespawnMissionPedPrints
                    PRINTLN("[Survival] - [AI Spawning] - RESPAWN_MISSION_PED ped: ", iped, " CAN_REGISTER_MISSION_PEDS(1) = TRUE.")
                    ENDIF
                    #ENDIF
                    
                    SET_SEARCHING_FOR_SQUAD_PED(GET_SPAWN_PED_UNIQUE_ID(iped, iSquadId))
                    
					IF GET_SPAWN_COORDS_FOR_ENEMY_PED(vTemp, fspawnhead, iSquadID, iStage, iped)
					
                    	IF IS_VECTOR_ZERO(serverBD_Enemies_S.structAiSquad[iSquadId][iStage].vSquadSpawnPos)
							serverBD_Enemies_S.structAiSquad[iSquadId][iStage].vSquadSpawnPos = vTemp
							PRINTLN("[Survival] - [AI Spawning] - Setting squad ",iSquadId," spawn pos to ", serverBD_Enemies_S.structAiSquad[iSquadId][iStage].vSquadSpawnPos)
						ENDIF
						serverBD_Enemies_S.structAiSquad[iSquadId][iStage].sPed[iPed].vCurrentSpawnCoords = vTemp
						
						#IF IS_DEBUG_BUILD
                        IF bDoRespawnMissionPedPrints
                        PRINTLN("[Survival] - [AI Spawning] - found spawn coords for ped: ", iped, " at coords: ", serverBD_Enemies_S.structAiSquad[iSquadId][iStage].sPed[iPed].vCurrentSpawnCoords)
						ENDIF
						#ENDIF
                    
                        IF CREATE_NET_PED(serverBD_Enemies_S.structAiSquad[iSquadId][iStage].sPed[iPed].netId, PEDTYPE_MISSION, eModel, serverBD_Enemies_S.structAiSquad[iSquadId][iStage].sPed[iPed].vCurrentSpawnCoords, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].fHead)
                           	PED_INDEX pedID = NET_TO_PED(serverBD_Enemies_S.structAiSquad[iSquadId][iStage].sPed[iPed].netId)
							PRINTLN("RESPAWN_MISSION_PED - Ped ", iPed, " squad ", iSquadID, " has been created *************")
							
							SET_UP_FMMC_PED_WK(pedID, iped, FALSE)
                            SETUP_PED_FOR_WAVE(iSquadId, iped, pedID, FALSE, FALSE, bIsHeavyUnit)
							
                            SET_PED_DIES_WHEN_INJURED(pedID, TRUE)
							MODEL_NAMES mnPedModel = GET_ENTITY_MODEL(pedID)
							IF mnPedModel != INT_TO_ENUM(MODEL_NAMES, HASH("U_M_Y_Juggernaut_01"))
							AND mnPedModel != HC_GUNMAN
								SET_PED_RANDOM_COMPONENT_VARIATION(pedID)
							ELSE
								INT iVariation = 1
								IF mnPedModel != HC_GUNMAN
									FMMC_SET_PED_VARIATION(pedID, mnPedModel, iVariation)
								ELSE
									iVariation = 4
									FMMC_SET_PED_VARIATION(pedID, mnPedModel, iVariation)
								ENDIF
								SET_PED_MOVEMENT_CLIPSET(pedID, "ANIM_GROUP_MOVE_BALLISTIC")
								SET_PED_STRAFE_CLIPSET(pedID, "MOVE_STRAFE_BALLISTIC")
								SET_WEAPON_ANIMATION_OVERRIDE(pedID, HASH("BALLISTIC"))
								IF mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("U_M_Y_Juggernaut_01"))
									DISABLE_PED_PAIN_AUDIO(pedID, TRUE)
									STOP_PED_SPEAKING(pedID, TRUE)
								ENDIF
								
								PRINTLN("[Survival] - [AI Spawning] - Juggernaut anim and component variations applied")
							ENDIF
							
							IF IS_HALLOWEEN_MODE()
								IF mnPedModel = S_M_M_HighSec_01
									SET_PED_PROP_INDEX(pedID, ANCHOR_EYES, 1, 1)
								ENDIF
							ENDIF
							
							SET_PED_AS_ENEMY(pedID, TRUE)
							
                            SET_MODEL_AS_NO_LONGER_NEEDED(eModel)
							FLOAT fZ
							VECTOR vTempPos = GET_ENTITY_COORDS(pedID)
							IF GET_GROUND_Z_FOR_3D_COORD(vTempPos,fZ)
								vTempPos.z = fZ
								SET_ENTITY_COORDS(pedID, vTempPos)
							ENDIF
							SET_ENTITY_PROOFS(pedID, TRUE, TRUE, TRUE, TRUE, TRUE, FALSE, FALSE, FALSE)
							PRINTLN("[Survival] - [AI Spawning] - Setting proofs for ped: ", iped, " Squad: ", iSquadId)
                            SET_SEARCHING_FOR_SQUAD_PED(-1)
                            PRINTLN("[Survival] - [AI Spawning] - spawning bug - successfully respawned ped: ", iped, " at coords: ", serverBD_Enemies_S.structAiSquad[iSquadId][iStage].sPed[iPed].vCurrentSpawnCoords)
                            serverBD_Enemies_S.structAiSquad[iSquadId][iStage].sPed[iPed].vCurrentSpawnCoords = << 0.0, 0.0, 0.0 >>
							
                            RETURN TRUE
						ELSE
							#IF IS_DEBUG_BUILD
                            IF bDoRespawnMissionPedPrints
                            PRINTLN("[Survival] - [AI Spawning] - Failed to create ped: ", iped, " at coords: ", serverBD_Enemies_S.structAiSquad[iSquadId][iStage].sPed[iPed].vCurrentSpawnCoords)
							PRINTLN("[Survival] - [AI Spawning] - serverBD.iNumAliveSquadEnemies: ", serverBD.iNumAliveSquadEnemies, " serverBD.iNumberOfMeleeEnemies: ", serverBD.iNumberOfMeleeEnemies, " serverBD.sFightingParticipantsList.iNumFightingParticipants: ", serverBD.sFightingParticipantsList.iNumFightingParticipants)
							ENDIF
							#ENDIF
                        ENDIF
                    ENDIF
                ELSE
                    #IF IS_DEBUG_BUILD
                    IF bDoRespawnMissionPedPrints
                    PRINTLN("[Survival] - [AI Spawning] - RESPAWN_MISSION_PED ped: ", iped, " CAN_REGISTER_MISSION_PEDS(1) = FALSE.")
                    ENDIF
                    #ENDIF
                ENDIF
            ELSE
                #IF IS_DEBUG_BUILD
                IF bDoRespawnMissionPedPrints
                PRINTLN("[Survival] - [AI Spawning] - RESPAWN_MISSION_PED ped: ", iped, " - loading model.")
                ENDIF
                #ENDIF
            ENDIF
        ENDIF
    ELSE
		IF NOT IS_PED_INJURED(NET_TO_PED(serverBD_Enemies_S.structAiSquad[iSquadId][iStage].sPed[iPed].netId))
            serverBD_Enemies_S.structAiSquad[iSquadId][iStage].sPed[iPed].vCurrentSpawnCoords = << 0.0, 0.0, 0.0 >>
            PRINTLN("[Survival] - [AI Spawning] - spawning bug - ped: ", iped, " already exists.")
            RETURN TRUE
		ENDIF
    ENDIF
    
    RETURN FALSE

ENDFUNC

FUNC INT GET_ENEMY_RESPAWN_TIME(BOOL bOverlapPed = FALSE)
    
    INT iReturnValue = g_FMMC_STRUCT_ENTITIES.iPedRespawnTime
    
    IF g_FMMC_STRUCT_ENTITIES.iPedRespawnTime = FMMC_PED_RESPAWN_TIME_RANDOM
		IF bOverlapPed
			iReturnValue = GET_RANDOM_INT_IN_RANGE(FMMC_PED_RESPAWN_TIME_8_SECONDS, FMMC_PED_RESPAWN_TIME_12_SECONDS)
		ELSE
            iReturnValue = GET_RANDOM_INT_IN_RANGE(FMMC_PED_RESPAWN_TIME_2_SECONDS, FMMC_PED_RESPAWN_TIME_6_SECONDS)
		ENDIF
    ENDIF
    
    RETURN (iReturnValue*1000)
    
ENDFUNC

PROC TRACK_SQUAD_ENEMY_KILL_STATS(INT iSquad, INT iPed, INT iStage, BOOL bInjured)
	
	SWITCH serverBD_Enemies_S.structAiSquad[iSquad][iStage].sPed[iPed].iEnemyTrackKillsStage
		
		CASE 0
			IF NOT bInjured
				serverBD.iNumCreatedEnemies++
				serverBD.iNumCreatedEnemiesThisStage[iStage]++
				serverBD_Enemies_S.structAiSquad[iSquad][iStage].sPed[iPed].iEnemyTrackKillsStage++
				PRINTLN("[Survival] - [AI Spawning] - TRACK_SQUAD_ENEMY_KILL_STATS - ", iPed, " bInjured = FALSE")
				PRINTLN("[Survival] - [AI Spawning] - TRACK_SQUAD_ENEMY_KILL_STATS - ", iPed, " iNumCreatedEnemies = ", serverBD.iNumCreatedEnemies)
				PRINTLN("[Survival] - [AI Spawning] - TRACK_SQUAD_ENEMY_KILL_STATS - ", iPed, " serverBD.iNumCreatedEnemiesThisStage[",iStage,"] = ", serverBD.iNumCreatedEnemiesThisStage[iStage])
				PRINTLN("[Survival] - [AI Spawning] - TRACK_SQUAD_ENEMY_KILL_STATS - ", iPed, " iEnemyTrackKillsStage = ", serverBD_Enemies_S.structAiSquad[iSquad][iStage].sPed[iPed].iEnemyTrackKillsStage)
			ENDIF
		BREAK
		
		CASE 1
			IF bInjured
				#IF IS_DEBUG_BUILD
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD_Enemies_S.structAiSquad[iSquad][iStage].sPed[iPed].netId)
					PRINTLN("[Survival] - SCRIPT_PED_", NETWORK_ENTITY_GET_OBJECT_ID(NET_TO_PED(serverBD_Enemies_S.structAiSquad[iSquad][iStage].sPed[iPed].netId)), " has just died. Squad Ped.")
				ENDIF
				#ENDIF
                SERVER_INCREMENT_KILLS_THIS_WAVE()
                SERVER_INCREMENT_KILLS_THIS_SUB_STAGE()
				SERVER_INCREMENT_TOTAL_KILLS()
				serverBD_Enemies_S.structAiSquad[iSquad][iStage].sPed[iPed].iEnemyTrackKillsStage = 0
				serverBD.iNumAliveSquadEnemies--
				IF serverBD_Enemies_S.structAiSquad[iSquad][iStage].sPed[iPed].bMelee
					serverBD.iNumberOfMeleeEnemies--
				ENDIF
				PRINTLN("[Survival] - [AI Spawning] - ", iPed, " bInjured = TRUE")
				PRINTLN("[Survival] - [AI Spawning] - ", iPed, " iKills = ", serverBD.iKills)
				PRINTLN("[Survival] - [AI Spawning] - ", iPed, " iKillsThisWave = ", serverBD.iKillsThisWave)
				PRINTLN("[Survival] - [AI Spawning] - ", iPed, " iKillsThisWaveSubStage = ", serverBD.iKillsThisWaveSubStage[serverBd.sWaveData.eStage])
				PRINTLN("[Survival] - [AI Spawning] - ", iPed, " iNumRequiredKills = ", serverBD.sWaveData.iNumRequiredKills)
				PRINTLN("[Survival] - [AI Spawning] - ", iPed, " iNumRequiredSubstageKills = ", serverBD.sWaveData.iNumRequiredSubstageKills[ENUM_TO_INT(serverBD.sWaveData.eStage)])
				PRINTLN("[Survival] - [AI Spawning] - ", iPed, " iEnemyTrackKillsStage = ", serverBD_Enemies_S.structAiSquad[iSquad][iStage].sPed[iPed].iEnemyTrackKillsStage)
				PRINTLN("[Survival] - [AI Spawning] - ", iPed, " Number of enemies alive = ", serverBD.iNumAliveSquadEnemies)
				PRINTLN("[Survival] - [AI Spawning] - ", iPed, " Number of melee enemies alive = ", serverBD.iNumberOfMeleeEnemies)
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD_Enemies_S.structAiSquad[iSquad][iStage].sPed[iPed].netId)
					CLEANUP_NET_ID(serverBD_Enemies_S.structAiSquad[iSquad][iStage].sPed[iPed].netId)
				ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH
	
ENDPROC

PROC INCREMENT_BRAIN_STAGGERED_ITERATOR(INT iStage, INT iSquad)
	iStaggeredPedBrain[iStage][iSquad]++
ENDPROC

PROC PROCESS_ENEMY_PED_BRAINS()
    
    INT iPed, iSquad, iStage
	BOOL bPedInjured
	
	// If we are not in the fighting stage, setup searching for the first ped for when we do go into the fighting stage again.
    IF GET_SERVER_HORDE_STAGE() = eHORDESTAGE_PASSED_WAVE
    	SET_SEARCHING_FOR_SQUAD_PED(-1)
    ENDIF
	
	FOR iStage = 0 TO NUM_SURVIVAL_SUB_STAGES-1
	
		REPEAT serverBD.sWaveData.iTotalSquads iSquad
			
			// Process this squad if it active.
	        IF serverBD_Enemies_S.structAiSquad[iSquad][iStage].eState = eSQUADSTATE_ACTIVE
				
				SWITCH serverBD.iSquadSpawningStage
					
					CASE 0
						
						#IF IS_DEBUG_BUILD
						//	IF bPedSpawningBug
								PRINTLN("[Survival] - [AI Spawning] - squad state = eSQUADSTATE_ACTIVE. Squad = ", iSquad, " Stage: ", iStage)
						//	ENDIF
						#ENDIF
									
						// Enemy loop.
					    IF iStaggeredPedBrain[iStage][iSquad] <= serverBD.sWaveData.iNumPedsInSquad[iStage]
							
							iPed = iStaggeredPedBrain[iStage][iSquad]
						
							// Store if the enemy is injured so we only need to call this once in the loop for the current ped.
							bPedInjured = IS_NET_PED_INJURED(serverBD_Enemies_S.structAiSquad[iSquad][iStage].sPed[iPed].netId)
							
							// Track the number of enemies created and killed. this is needed for end of wave calculations elsewhere.
							TRACK_SQUAD_ENEMY_KILL_STATS(iSquad, iPed, iStage, bPedInjured)
							
							#IF IS_DEBUG_BUILD
								IF bPedSpawningBug
									PRINTLN("[Survival] - [AI Spawning] - vSquadSpawnPoint != zero. Ped = ", iPed)
								ENDIF
							#ENDIF
							
							// Enemy state machine switch.
					        SWITCH serverBD_Enemies_S.structAiSquad[iSquad][iStage].sPed[iPed].eState
					                
					            // Spawning state. This is where we create our enemies.
					            CASE eENEMYSTATE_SPAWNING
					                
									#IF IS_DEBUG_BUILD
									//	IF bPedSpawningBug
											PRINTLN("[Survival] - [AI Spawning] - in enemy state spawning. Ped = ", iPed, " Stage: ", iStage)
											PRINTLN("[Survival] - [AI Spawning] - iSearchingForPed = ", serverBd.iSearchingForPed)
											PRINTLN("[Survival] - [AI Spawning] - iSearchingForChopper = ", serverBd.iSearchingForChopper)
											PRINTLN("[Survival] - [AI Spawning] - iSearchingForLandVehicle = ", serverBd.iSearchingForLandVehicle)
									//	ENDIF
									#ENDIF
									
									// If we are searching for this ped.
									IF ((serverBd.iSearchingForPed = -1) AND (serverBd.iSearchingForChopper = -1) AND (serverBd.iSearchingForLandVehicle = -1))
			                        OR ((serverBd.iSearchingForPed = GET_SPAWN_PED_UNIQUE_ID(iPed, iSquad)) AND (serverBd.iSearchingForChopper = -1) AND (serverBd.iSearchingForLandVehicle = -1))
										
										#IF IS_DEBUG_BUILD
											IF bPedSpawningBug
												PRINTLN("[Survival] - [AI Spawning] - iSearchingForPed = GET_SPAWN_PED_UNIQUE_ID(iPed, iSquad) or = (-1). Ped = ", iPed)
											ENDIF
										#ENDIF
											
										// Spawn the ped.
		                                IF RESPAWN_MISSION_PED(iPed, iSquad)
		                                    SET_SEARCHING_FOR_SQUAD_PED(-1)
											serverBD.iNumCreatedEnemies++
											serverBD.iNumCreatedEnemiesThisStage[iStage]++
											serverBD_Enemies_S.structAiSquad[iSquad][iStage].sPed[iPed].iEnemyTrackKillsStage++
											PRINTLN("[Survival] - [AI Spawning] - RESPAWN_MISSION_PED - ", iPed, " iNumCreatedEnemies = ", serverBD.iNumCreatedEnemies)
											PRINTLN("[Survival] - [AI Spawning] - RESPAWN_MISSION_PED - ", iPed, " serverBD.iNumCreatedEnemiesThisStage[",iStage,"] = ", serverBD.iNumCreatedEnemiesThisStage[iStage])
											PRINTLN("[Survival] - [AI Spawning] - RESPAWN_MISSION_PED - ", iPed, " iEnemyTrackKillsStage = ", serverBD_Enemies_S.structAiSquad[iSquad][iStage].sPed[iPed].iEnemyTrackKillsStage)
											serverBD.iSquadSpawningStage = 0
											// If the enemy is not injured, increment num alive enemies count.
											serverBD.iNumAliveSquadEnemies++
											PRINTLN("[Survival] - [AI Spawning] - serverBD.iNumAliveSquadEnemies = ", serverBD.iNumAliveSquadEnemies, " stage: ", iStage)
											serverBD_Enemies_S.structAiSquad[iSquad][iStage].sPed[iPed].eState = eENEMYSTATE_FIGHTING
		                                    PRINTLN("[Survival] - [AI Spawning] - SCRIPT_PED_", NETWORK_ENTITY_GET_OBJECT_ID(NET_TO_PED(serverBD_Enemies_S.structAiSquad[iSquad][iStage].sPed[iPed].netId)), " Moving to state eENEMYSTATE_FIGHTING")
		                                ENDIF
									ENDIF
									
				                BREAK
				                
				                CASE eENEMYSTATE_FIGHTING
				                    
									// If the ped is injured.
				                    IF bPedInjured
				                        
										// Set to the dead state.
			                           	serverBD_Enemies_S.structAiSquad[iSquad][iStage].sPed[iPed].eState = eENEMYSTATE_DEAD
			                            PRINTLN("[Survival] - [AI Spawning] - ped ", iPed, " brain, ped net injured, going to state eENEMYSTATE_DEAD.") 
			                           
			                            #IF IS_DEBUG_BUILD
			                            IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD_Enemies_S.structAiSquad[iSquad][iStage].sPed[iPed].netId)
											PRINTLN("[Survival] - [AI Spawning] - SCRIPT_PED_", NETWORK_ENTITY_GET_OBJECT_ID(NET_TO_PED(serverBD_Enemies_S.structAiSquad[iSquad][iStage].sPed[iPed].netId)), " Moving to state eENEMYSTATE_DEAD")
			                                PRINT_INJURED_PED_DATA(serverBD_Enemies_S.structAiSquad[iSquad][iStage].sPed[iPed].netId)
			                            ENDIF
			                            #ENDIF
										
				                    	CLEANUP_NET_ID(serverBD_Enemies_S.structAiSquad[iSquad][iStage].sPed[iPed].netId)
									ELSE
										#IF IS_DEBUG_BUILD
											IF bPedSpawningBug
												VECTOR vTempVector
												vTempVector = GET_ENTITY_COORDS(NET_TO_PED(serverBD_Enemies_S.structAiSquad[iSquad][iStage].sPed[iPed].netId))
												PRINTLN("[Survival] - [AI Spawning] - alive ped = ", iPed)
												PRINTLN("[Survival] - [AI Spawning] - alive ped coords = ", vTempVector)
											ENDIF
										#ENDIF
										
				                    ENDIF
				                    
				                BREAK
				                
				                // Process spawning stage every frame because HAS_NET_TIMER_EXPIRED() needs to be called every frame.
				                CASE eENEMYSTATE_DEAD
								
									// Do nothing for now, ped is dead.
									#IF IS_DEBUG_BUILD
										IF bPedSpawningBug
											PRINTLN("[Survival] - [AI Spawning] - ped ", iPed, " in state eENEMYSTATE_DEAD.")
										ENDIF
									#ENDIF
									
									
				                BREAK
								
			                ENDSWITCH
							
							IF (GET_FRAME_COUNT() % 5) = 0 
								IF iStaggeredPedBrain[iStage][iSquad]+ 1 >= serverBD.sWaveData.iNumPedsInSquad[iStage]
									iStaggeredPedBrain[iStage][iSquad] = 0
								ELSE
									INCREMENT_BRAIN_STAGGERED_ITERATOR(iStage, iSquad)
								ENDIF
							ENDIF
						ELSE
							iStaggeredPedBrain[iStage][iSquad] = 0
						ENDIF	
						
					BREAK
					
					CASE 1
						
						serverBD.iSquadSpawningStage = 0
						
					BREAK
					
				ENDSWITCH
				
			ENDIF
		    
		ENDREPEAT
	ENDFOR
	
ENDPROC

PROC MAINTAIN_ANGRY_SHOUTING(PED_INDEX pedId, BOOL &bPlayedAngryShout)
	
	IF NOT bPlayedAngryShout
		
		PLAYER_INDEX playerId
		VECTOR vPedCoords, vPlayerCoords
		INT iPlayer
		
		iPlayer = GET_NEAREST_PLAYER_TO_ENTITY(pedId)
		playerId = INT_TO_NATIVE(PLAYER_INDEX, iPlayer)
		
		IF IS_NET_PLAYER_OK(playerId)
			
			vPedCoords = GET_ENTITY_COORDS(pedId)
			vPlayerCoords = GET_ENTITY_COORDS(GET_PLAYER_PED(playerId))
			
			IF VDIST2(vPedCoords, vPlayerCoords) <= 400
				IF GET_RANDOM_INT_IN_RANGE(0,2) = 0
					PLAY_PED_AMBIENT_SPEECH(pedId, "GENERIC_INSULT_HIGH", SPEECH_PARAMS_FORCE_SHOUTED)
					bPlayedAngryShout = TRUE
				ELSE
					bPlayedAngryShout = TRUE
				ENDIF
			ENDIF
			
		ENDIF
		
	ENDIF
	
ENDPROC

PROC MAINTAIN_CHALLENGE_TIMER()
	IF IS_PLAYLIST_DOING_CHALLENGE()
		INT iCurrentScoreToBeat = GET_CURRENT_MISSION_TIME_SCORE_TO_BEAT()
		DRAW_GENERIC_SCORE(iCurrentScoreToBeat, "SCORE_CHALLSC")
	ENDIF
ENDPROC

FUNC BOOL IS_PED_PERFORMING_TASK(PED_INDEX tempPed, SCRIPT_TASK_NAME eScriptTask)
	
	SCRIPTTASKSTATUS eTaskStatus = GET_SCRIPT_TASK_STATUS(tempPed, eScriptTask)
	
	IF eTaskStatus = PERFORMING_TASK
	OR eTaskStatus = WAITING_TO_START_TASK
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC PED_INDEX GET_CLOSEST_VISIBLE_PLAYER(PED_INDEX tempPed, BOOL bIgnorePerceptionCheck = FALSE, FLOAT maxDistance = 30.0, BOOL bIgnoreLOSCheck = FALSE)

	INT iClosestPlayer = GET_NEAREST_PLAYER_TO_ENTITY(tempPed)
	PLAYER_INDEX ClosestPlayer = INT_TO_PLAYERINDEX(iClosestPlayer)
	
	IF IS_NET_PLAYER_OK(ClosestPlayer)
		
		PED_INDEX ClosestPlayerPed = GET_PLAYER_PED(ClosestPlayer)
		
		IF GET_DISTANCE_BETWEEN_ENTITIES(tempPed,ClosestPlayerPed) < maxDistance
		AND (bIgnorePerceptionCheck OR
			IS_TARGET_PED_IN_PERCEPTION_AREA(tempPed,ClosestPlayerPed)) //added a perception check so peds don't head-track the player while sneaking up for a stealth kill
		AND (bIgnoreLOSCheck OR
			HAS_ENTITY_CLEAR_LOS_TO_ENTITY(tempPed,ClosestPlayerPed,SCRIPT_INCLUDE_MOVER|SCRIPT_INCLUDE_VEHICLE|SCRIPT_INCLUDE_PED|SCRIPT_INCLUDE_RAGDOLL|SCRIPT_INCLUDE_OBJECT|SCRIPT_INCLUDE_FOLIAGE))
			RETURN ClosestPlayerPed
		ENDIF
	ENDIF
	RETURN NULL
ENDFUNC

PROC INCREMENT_PED_BODY_ITERATOR(INT iStage, INT iSquad)
	iStaggeredPedBody[iStage][iSquad]++
ENDPROC

FUNC PED_INDEX FIND_VALID_TARGET()
	INT iParticipant
	PED_INDEX piValidTarget
	FOR iParticipant = 0 TO MAX_NUMBER_HORDE_FIGHTING_PARTICIPANTS-1
		IF serverBD.sFightingParticipantsList.iParticipant[iParticipant] != -1
			PLAYER_INDEX tempPlayer = INT_TO_PLAYERINDEX(serverBD.sFightingParticipantsList.iParticipant[iParticipant])
			IF NETWORK_IS_PLAYER_ACTIVE(tempPlayer)
			AND NOT IS_PLAYER_SPECTATING(tempPlayer)
				piValidTarget = GET_PLAYER_PED(tempPlayer)
				PRINTLN("FIND_VALID_TARGET - Using participant ", iParticipant, " as valid target.")
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN piValidTarget
ENDFUNC

PROC PROCESS_ENEMY_PED_SAFETY_MEASURES(PED_INDEX pedId)
	BOOL bIsOutOfBounds = IS_PED_OUT_OF_BOUNDS(pedId)
	BOOL bShouldDie
	IF bIsOutOfBounds
	
		SET_PED_COMBAT_ATTRIBUTES(pedId, CA_USE_COVER, FALSE)
		
		VECTOR vPedPos
		vPedPos = GET_ENTITY_COORDS(pedId)
		IF (serverBD.vBoundsMiddle.z - vPedPos.z) > ((MAX_SURVIVAL_BOUNDS_SIZE/2.0) + 35.0)
			bShouldDie = TRUE
			PRINTLN("[Survival] - PROCESS_SQUAD_ENEMY_PED_BODIES - PROCESS_ENEMY_PED_SAFETY_MEASURES - Killing SCRIPT_PED_", NETWORK_ENTITY_GET_OBJECT_ID(pedId)," as it is too far away from the area")
		ENDIF
		IF IS_PED_FLEEING(pedId)
			IF VDIST_2D(serverBD.vBoundsMiddle, vPedPos) > MAX_SURVIVAL_BOUNDS_SIZE
				bShouldDie = TRUE
				PRINTLN("[Survival] - PROCESS_SQUAD_ENEMY_PED_BODIES - PROCESS_ENEMY_PED_SAFETY_MEASURES - Killing SCRIPT_PED_", NETWORK_ENTITY_GET_OBJECT_ID(pedId)," as it is fleeing out of bounds")
			ENDIF
		ENDIF
		
		IF IS_PED_ON_VEHICLE(pedId)
			VEHICLE_INDEX viVeh[1]
			GET_PED_NEARBY_VEHICLES(pedId, viVeh)
			IF DOES_ENTITY_EXIST(viVeh[0]) 
				IF GET_ENTITY_MODEL(viVeh[0]) = CARGOBOB
				OR GET_ENTITY_MODEL(viVeh[0]) = CARGOBOB2
					bShouldDie = TRUE
					PRINTLN("[Survival] - PROCESS_SQUAD_ENEMY_PED_BODIES - PROCESS_ENEMY_PED_SAFETY_MEASURES - Killing SCRIPT_PED_", NETWORK_ENTITY_GET_OBJECT_ID(pedId)," as it is stood on(in) a Cargobob")
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SILO_03)
			IF GET_INTERIOR_FROM_ENTITY(pedId) != sInterior.iInteriorIndex
				bShouldDie = TRUE
				PRINTLN("[Survival] - PROCESS_SQUAD_ENEMY_PED_BODIES - PROCESS_ENEMY_PED_SAFETY_MEASURES - Killing SCRIPT_PED_", NETWORK_ENTITY_GET_OBJECT_ID(pedId)," as it is outside of the Silo")
			ENDIF
		ENDIF
		
		IF bShouldDie
			IF IS_ENTITY_ALIVE(pedId)
				SET_ENTITY_HEALTH(pedId, 0)
			ENDIF
		ENDIF
	ELSE
		SET_PED_COMBAT_ATTRIBUTES(pedId, CA_USE_COVER, TRUE)
	ENDIF
ENDPROC

PROC PROCESS_SQUAD_ENEMY_PED_BODIES()

    INT iPed, iSquad, iStage
    PED_INDEX pedId
    BOOL bFrustratedAdvance
	ENTITY_PROOFS_STRUCT sProof
	BLIP_SPRITE bsSprite
	STRING sBlipName
	FOR iStage = 0 TO NUM_SURVIVAL_SUB_STAGES-1
		REPEAT serverBD.sWaveData.iTotalSquads iSquad
			// Process this squad if it active.
	        IF serverBD_Enemies_S.structAiSquad[iSquad][iStage].eState = eSQUADSTATE_ACTIVE
				// Enemy loop.
				IF iStaggeredPedBody[iStage][iSquad] <= serverBD.sWaveData.iNumPedsInSquad[iStage]
							
					iPed = iStaggeredPedBody[iStage][iSquad]
				
					IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD_Enemies_S.structAiSquad[iSquad][iStage].sPed[iPed].netId)
						bsSprite = RADAR_TRACE_AI
						sBlipName = NULL
						IF serverBD_Enemies_S.structAiSquad[iSquad][iStage].sPed[iPed].bHeavy
							bsSprite = RADAR_TRACE_BAT_ASSASSINATE
							sBlipName = GET_FILENAME_FOR_AUDIO_CONVERSATION("SC_HVY_BLP")
						ENDIF
						
				      	UPDATE_ENEMY_NET_PED_BLIP(	serverBD_Enemies_S.structAiSquad[iSquad][iStage].sPed[iPed].netId, 
													structSquadEnemyBlip[iSquad][iPed][iStage],
													DEFAULT,
													FALSE, 
													FORCE_ENEMY_BLIPS_ON(),
													DEFAULT,
													sBlipName,
													1,
													bsSprite)
				      	
				        IF NOT IS_NET_PED_INJURED(serverBD_Enemies_S.structAiSquad[iSquad][iStage].sPed[iPed].netId)
				            
							pedId = NET_TO_PED(serverBD_Enemies_S.structAiSquad[iSquad][iStage].sPed[iPed].netId)
							
							// Always make sure the ped has a weapon if they don't have one.
							IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD_Enemies_S.structAiSquad[iSquad][iStage].sPed[iPed].netId)
							AND NOT IS_PED_HURT(pedId)
								IF GET_PEDS_CURRENT_WEAPON(pedId) = WEAPONTYPE_UNARMED
									SETUP_PED_FOR_WAVE(iSquad,iPed,pedId,TRUE)
									PRINTLN("[Survival] - ped has become unarmed, giving weapon to ped. Squad = ", iSquad, ", Ped = ", iPed)
								ENDIF
							ENDIF
							
							IF DOES_BLIP_EXIST(structSquadEnemyBlip[iSquad][iPed][iStage].BlipID)
								IF IS_VALID_INTERIOR(sInterior.iInteriorIndex)
									SET_BLIP_SHORT_HEIGHT_THRESHOLD(structSquadEnemyBlip[iSquad][iPed][iStage].BlipID, TRUE)
								ENDIF
							ENDIF
							
				            SWITCH serverBD_Enemies_S.structAiSquad[iSquad][iStage].sPed[iPed].eState
				                    
				                CASE eENEMYSTATE_FIGHTING
				            		         
				                    IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD_Enemies_S.structAiSquad[iSquad][iStage].sPed[iPed].netId)
									
										MAINTAIN_ANGRY_SHOUTING(pedId, serverBD_Enemies_S.structAiSquad[iSquad][iStage].sPed[iPed].bPlayedAngryShout)
										
										// Make fight.
				                        IF NOT IS_PED_IN_COMBAT(pedId)
											IF NOT IS_PED_PERFORMING_TASK(pedId, SCRIPT_TASK_COMBAT_HATED_TARGETS_IN_AREA)
				                            	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedId, FALSE)
												IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
				                            		TASK_COMBAT_HATED_TARGETS_IN_AREA(pedId, GET_ENTITY_COORDS(pedId), 299.0)
													PRINTLN("PROCESS_SQUAD_ENEMY_PED_BODIES - SCRIPT_PED_", NETWORK_ENTITY_GET_OBJECT_ID(pedId)," retasked with combat as was out of combat.")
												ELSE
													TASK_COMBAT_PED(pedId, localPlayerPed, COMBAT_PED_PREVENT_CHANGING_TARGET | COMBAT_PED_DISABLE_AIM_INTRO, TASK_THREAT_RESPONSE_CAN_FIGHT_ARMED_PEDS_WHEN_NOT_ARMED)
												ENDIF
											ENDIF
				                        ENDIF
										
										IF IS_PED_FLEEING(pedId)
											PRINTLN("PROCESS_SQUAD_ENEMY_PED_BODIES - SCRIPT_PED_", NETWORK_ENTITY_GET_OBJECT_ID(pedId)," is fleeing.")
											PED_INDEX piValidTarget
											piValidTarget = FIND_VALID_TARGET()
											IF piValidTarget != NULL
												TASK_COMBAT_PED(pedID, piValidTarget, COMBAT_PED_NONE, TASK_THREAT_RESPONSE_CAN_FIGHT_ARMED_PEDS_WHEN_NOT_ARMED)
												PRINTLN("PROCESS_SQUAD_ENEMY_PED_BODIES - Tasked to combat specific target after fleeing")
											ENDIF
										ENDIF
										
										IF GET_PED_ALERTNESS(pedId) = AS_NOT_ALERT
											SET_PED_ALERTNESS(pedId, AS_MUST_GO_TO_COMBAT)
											PRINTLN("PROCESS_SQUAD_ENEMY_PED_BODIES - SCRIPT_PED_", NETWORK_ENTITY_GET_OBJECT_ID(pedId)," setting alert state as not alert.")
										ENDIF
										
										// If the ped gets stuck waiting opn collision, load it around them so they can get moving again.
										IF IS_ENTITY_WAITING_FOR_WORLD_COLLISION(pedId)
											SET_ENTITY_LOAD_COLLISION_FLAG(pedId, TRUE)
										ENDIF
																				
										// Be frustrated if only a few commrades left.
										IF serverBD.iNumAliveSquadEnemies <= 3
											bFrustratedAdvance = TRUE
										ENDIF
										
										SET_PED_COMBAT_ATTRIBUTES(pedId, CA_CAN_USE_FRUSTRATED_ADVANCE, bFrustratedAdvance)
										
										IF GET_NAVMESH_ROUTE_RESULT(pedId) = NAVMESHROUTE_ROUTE_NOT_FOUND
										OR (IS_FAKE_MULTIPLAYER_MODE_SET() AND IS_PED_DEFENSIVE_AREA_ACTIVE(pedId, FALSE))
											PRINTLN("[Survival] - PROCESS_SQUAD_ENEMY_PED_BODIES - Squad = ", iSquad, ", Ped = ", iPed," Stuck, removing defensive area")
											SET_PED_COMBAT_ATTRIBUTES(pedId, CA_PERMIT_CHARGE_BEYOND_DEFENSIVE_AREA, TRUE)
											REMOVE_PED_DEFENSIVE_AREA(pedId)
										ENDIF
										
										PROCESS_ENEMY_PED_SAFETY_MEASURES(pedId)
										
										GET_ENTITY_PROOFS(pedID, sProof.bBulletProof, sProof.bFireProof, sProof.bExplosionProof, sProof.bCollisionProof, sProof.bMeleeProof, sProof.bSteamProof, sProof.bDontResetDamageFlags, sProof.bSmokeProof)
										IF sProof.bBulletProof
										OR sProof.bExplosionProof
										OR (sProof.bMeleeProof AND NOT serverBD_Enemies_S.structAiSquad[iSquad][iStage].sPed[iPed].bHeavy)
										OR sProof.bCollisionProof
											IF serverBD_Enemies_S.structAiSquad[iSquad][iStage].sPed[iPed].bHeavy
												SET_ENTITY_PROOFS(pedID, FALSE, TRUE, FALSE, FALSE, TRUE, FALSE, FALSE, FALSE)
											ELSE
												SET_ENTITY_PROOFS(pedID, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE)
											ENDIF
											PRINTLN("[Survival] - [AI Spawning] - Clearing proofs for ped: ", iPed, " Squad: ", iSquad)
										ENDIF
				                    ENDIF
				                BREAK
				            ENDSWITCH
				        ENDIF
					ENDIF
					
			        IF (GET_FRAME_COUNT() % 5) = 0 
						IF iStaggeredPedBody[iStage][iSquad]+ 1 >= serverBD.sWaveData.iNumPedsInSquad[iStage]
							iStaggeredPedBody[iStage][iSquad] = 0
						ELSE
							INCREMENT_PED_BODY_ITERATOR(iStage, iSquad)
						ENDIF
					ENDIF
			    ELSE
					iStaggeredPedBody[iStage][iSquad] = 0
				ENDIF
			ENDIF
			
		ENDREPEAT
	ENDFOR
 	
ENDPROC

PROC REQUEST_PATH_NODES_FOR_PLAY_AREA()
	VECTOR vMin, vMax
	vMin = serverBD.vBoundsMiddle-<<MAX_SURVIVAL_BOUNDS_SIZE, MAX_SURVIVAL_BOUNDS_SIZE, 250.0>>
	vMax = serverBD.vBoundsMiddle-<<MAX_SURVIVAL_BOUNDS_SIZE, MAX_SURVIVAL_BOUNDS_SIZE, 250.0>>
	REQUEST_PATH_NODES_IN_AREA_THIS_FRAME(vMin.x, vMin.y, vMax.x, vMax.y)
ENDPROC
