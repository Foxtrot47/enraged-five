USING "FM_Survival_Controller_Using.sch"

PROC CACHE_PLAYER_DATA()
	IF NETWORK_IS_GAME_IN_PROGRESS()
		bIsLocalPlayerHost = NETWORK_IS_HOST_OF_THIS_SCRIPT()
		iLocalPart = PARTICIPANT_ID_TO_INT()
	ENDIF
	localPlayer = PLAYER_ID()
	
	IF IS_NET_PLAYER_OK(LocalPlayer)
		bLocalPlayerOK = TRUE
		LocalPlayerPed = PLAYER_PED_ID()
		IF NOT IS_PED_INJURED(LocalPlayerPed)
			bLocalPlayerPedOK = TRUE
		ELSE
			bLocalPlayerPedOK = FALSE
		ENDIF
	ELSE
		LocalPlayerPed = PLAYER_PED_ID()
		bLocalPlayerOK = FALSE
		bLocalPlayerPedOK = FALSE
	ENDIF
	
	iPartToUse = iLocalPart
	PlayerToUse = localPlayer
	PlayerPedToUse = LocalPlayerPed
ENDPROC

FUNC BOOL IS_LOCAL_BIT_SET(INT iBit)
	RETURN IS_BIT_SET(iLocalBoolArray[GET_LONG_BITSET_INDEX_ALT(iBit)],  GET_LONG_BITSET_BIT_ALT(iBit))
ENDFUNC

PROC SET_LOCAL_BIT(INT iBit, BOOL bPrint = TRUE)
	IF bPrint
		PRINTLN("SET_LOCAL_BIT - Setting iLocalBoolArray[",GET_LONG_BITSET_INDEX_ALT(iBit),"], ", GET_LONG_BITSET_BIT_ALT(iBit), " iBit: ", iBit)
	ENDIF
	SET_BIT(iLocalBoolArray[GET_LONG_BITSET_INDEX_ALT(iBit)],  GET_LONG_BITSET_BIT_ALT(iBit))
ENDPROC

PROC CLEAR_LOCAL_BIT(INT iBit, BOOL bPrint = TRUE)
	IF bPrint
		PRINTLN("CLEAR_LOCAL_BIT - Clearing iLocalBoolArray[",GET_LONG_BITSET_INDEX_ALT(iBit),"], ", GET_LONG_BITSET_BIT_ALT(iBit), " iBit: ", iBit)
	ENDIF
	CLEAR_BIT(iLocalBoolArray[GET_LONG_BITSET_INDEX_ALT(iBit)],  GET_LONG_BITSET_BIT_ALT(iBit))
ENDPROC

FUNC STRING GET_GAME_STATE_NAME(INT iState)
    
    SWITCH iState
    	CASE SURVIVAL_GAME_STATE_INIT                 	RETURN "INI"
        CASE SURVIVAL_GAME_STATE_INIT_SPAWN          	RETURN "INI_SPAWN"
        CASE SURVIVAL_GAME_STATE_RUNNING             	RETURN "RUNNING"
        CASE SURVIVAL_GAME_STATE_WAITING_TO_LEAVE       RETURN "WAITING_TO_LEAVE"
        CASE SURVIVAL_GAME_STATE_LEAVE                	RETURN "LEAVE"
        CASE SURVIVAL_GAME_STATE_END                  	RETURN "END"
    ENDSWITCH
    
    RETURN "NOT_IN_SWITCH"
    
ENDFUNC

/// PURPOSE:
///    Helper function to get the servers game state
FUNC INT GET_SERVER_GAME_STATE()
    RETURN serverBD.iServerGameState
ENDFUNC 

/// PURPOSE:
///    Sets the server game state.
/// PARAMS:
///    iState - state to set to.
PROC SET_SERVER_GAME_STATE(INT iState)
    IF serverBD.iServerGameState != iState
		#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		PRINTLN("SET_SERVER_GAME_STATE - Setting SERVER state from ", serverBD.iServerGameState, " to ", iState)
		#ENDIF
        serverBD.iServerGameState = iState
    ENDIF
ENDPROC

FUNC STRING GET_HORDE_STAGE_NAME(eHORDE_STAGE eStage)
        
    SWITCH eStage
        CASE eHORDESTAGE_JIP_IN                   	RETURN "JIP_IN"
        CASE eHORDESTAGE_SETUP_MISSION            	RETURN "SETUP_MISSION"
        CASE eHORDESTAGE_START_FADE_IN            	RETURN "START_FADE_IN"
        CASE eHORDESTAGE_FIGHTING                  	RETURN "FIGHTING"
        CASE eHORDESTAGE_PASSED_WAVE               	RETURN "PASSED_WAVE"
        CASE eHORDESTAGE_DELAY_BEFORE_NEXT_WAVE		RETURN "DELAY_BEFORE_NEXT_WAVE"
        CASE eHORDESTAGE_SPECTATING               	RETURN "SPECTATING"
        CASE eHORDESTAGE_SCTV                     	RETURN "SCTV"
        CASE eHORDESTAGE_REWARDS_AND_FEEDACK   		RETURN "REWARDS_AND_FEEDACK"
    ENDSWITCH
    
    RETURN "NOT_IN_SWITCH"
        
ENDFUNC

/// PURPOSE:
///    Gets the current value of the server stored horde stage.
/// RETURNS:
///    Mission stage.
FUNC eHORDE_STAGE GET_SERVER_HORDE_STAGE()
    RETURN serverBD.eStage
ENDFUNC

/// PURPOSE:
///    Sets the value of the aserver stored horde stage.
///    Server only command.
/// PARAMS:
///    eMissionStage - value to set stage to.
PROC SET_SERVER_HORDE_STAGE(eHORDE_STAGE eStage)
    #IF IS_DEBUG_BUILD
        IF eStage != GET_SERVER_HORDE_STAGE()
        ENDIF
    #ENDIF
	DEBUG_PRINTCALLSTACK()
	PRINTLN("[survival] - SET_SERVER_HORDE_STAGE - Setting SERVER horde stage from ", GET_HORDE_STAGE_NAME(serverBD.eStage), " to ", GET_HORDE_STAGE_NAME(eStage))
    serverBD.eStage = eStage
ENDPROC

/// PURPOSE:
///    Helper function to get a clients game state
FUNC INT GET_CLIENT_GAME_STATE(INT iParticipant)
    RETURN playerBD[iParticipant].iGameState
ENDFUNC

/// PURPOSE:
///    Sets the local participant game state.
/// PARAMS:
///    iState - state to set to.
PROC SET_CLIENT_GAME_STATE(INT iState)
    IF playerBD[iLocalPart].iGameState != iState
		#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		PRINTLN("SET_CLIENT_GAME_STATE - Setting CLIENT state from ", playerBD[iLocalPart].iGameState, " to ", iState)
		#ENDIF
        playerBD[iLocalPart].iGameState = iState
    ENDIF
ENDPROC

/// PURPOSE:
///    Gets the local horde stage of participant (stored in player BD).
/// PARAMS:
///    participant - participant to get horde stage for.
/// RETURNS:
///    Mission stage.
FUNC eHORDE_STAGE GET_CLIENT_HORDE_STAGE(PARTICIPANT_INDEX participant)
    RETURN playerBD[NATIVE_TO_INT(participant)].eStage
ENDFUNC

/// PURPOSE:
///     Sets the local horde stage of participant (in player BD).
/// PARAMS:
///    eMissionStage - horde stage to set to.
///    participant - participant to set stage for.
PROC SET_CLIENT_HORDE_STAGE(eHORDE_STAGE eStage)
    #IF IS_DEBUG_BUILD
        IF eStage != GET_CLIENT_HORDE_STAGE(PARTICIPANT_ID())
            PRINTLN("[Survival] - SET_CLIENT_HORDE_STAGE - Setting CLIENT horde stage to ", eStage)
			DEBUG_PRINTCALLSTACK()
        ENDIF
    #ENDIF
    playerBD[iLocalPart].eStage = eStage
ENDPROC

FUNC STRING GET_END_HORDE_REASON_NAME(eEND_HORDE_REASON eReason)
    
    SWITCH eReason
        CASE eENDHORDEREASON_NOT_FINISHED_YET 	RETURN "NOT_FINISHED_YET"
        CASE eENDHORDEREASON_BEAT_ALL_WAVES    	RETURN "BEAT_ALL_WAVES"
        CASE eENDHORDEREASON_ALL_PLAYERS_DEAD   RETURN "ALL_PLAYERS_DEAD"
    ENDSWITCH
    
    RETURN "NOT_IN_SWITCH"
    
ENDFUNC

/// PURPOSE:
///    Gets the current end mode reason (win/lose condition).
///    Return eENDHORDEREASON_NOT_FINISHED_YET if the horde is still ongoing.
/// RETURNS:
///    Current end reason (stored on server).
FUNC eEND_HORDE_REASON GET_END_HORDE_REASON()
    RETURN serverBD.eEndReason
ENDFUNC

/// PURPOSE:
///    Sets the end horde reason.
/// PARAMS:
///    eReason - reason to set to.
PROC SET_END_HORDE_REASON(eEND_HORDE_REASON eReason)
    #IF IS_DEBUG_BUILD
        IF eReason != GET_END_HORDE_REASON()
            PRINTLN("[Survival] - setting server horde end reason to ", GET_END_HORDE_REASON_NAME(eReason))
        ENDIF
    #ENDIF
    serverBD.eEndReason = eReason
ENDPROC

PROC SET_WAVE_SUB_STAGE(eWAVE_STAGE eNewStage)
	PRINTLN("[Survival] SET_WAVE_SUB_STAGE - Substage changing from ", serverBD.sWaveData.eStage, " to ", eNewStage)
	serverBD.sWaveData.eStage = eNewStage
ENDPROC

FUNC STRING GET_SUB_STAGE_NAME(eWAVE_STAGE eStage)
	SWITCH eStage
		CASE eWAVESTAGE_EASY 	RETURN "eWAVESTAGE_EASY"
		CASE eWAVESTAGE_MEDIUM 	RETURN "eWAVESTAGE_MEDIUM"
		CASE eWAVESTAGE_HARD 	RETURN "eWAVESTAGE_HARD"
	ENDSWITCH
	RETURN "INVALID SUB STAGE"
ENDFUNC

PROC SERVER_INCREMENT_KILLS_THIS_WAVE()
	DEBUG_PRINTCALLSTACK()
	serverBD.iKillsThisWave++
	PRINTLN("[Survival] - SERVER_INCREMENT_KILLS_THIS_WAVE - Incremented kills this wave(",serverBD.sWaveData.iWaveCount,") to ", serverBD.iKillsThisWave)
ENDPROC

PROC SERVER_INCREMENT_KILLS_THIS_SUB_STAGE()
	DEBUG_PRINTCALLSTACK()
	serverBD.iKillsThisWaveSubStage[serverBd.sWaveData.eStage]++
	PRINTLN("[Survival] - SERVER_INCREMENT_KILLS_THIS_SUB_STAGE - Incremented kills this substage(",GET_SUB_STAGE_NAME(serverBd.sWaveData.eStage),") to ", serverBD.iKillsThisWaveSubStage[serverBd.sWaveData.eStage], " Wave: ", serverBD.sWaveData.iWaveCount)
ENDPROC

PROC SERVER_INCREMENT_EVENT_KILLS()
	DEBUG_PRINTCALLSTACK()
	serverBD.iNumKillsThisWaveFromEvents++
	PRINTLN("[Survival] - SERVER_INCREMENT_EVENT_KILLS - Incremented event kills to ", serverBD.iNumKillsThisWaveFromEvents)
ENDPROC

PROC SERVER_INCREMENT_TOTAL_KILLS()
	DEBUG_PRINTCALLSTACK()
	serverBD.iKills++
	PRINTLN("[Survival] - SERVER_INCREMENT_EVENT_KILLS - Incremented total kills to ", serverBD.iKills)
ENDPROC

PROC SERVER_DECREMENT_REMAINING_AIR_VEHICLES()
	DEBUG_PRINTCALLSTACK()
	serverBD.iNumAliveHelis--
	PRINTLN("[Survival] - SERVER_DECREMENT_REMAINING_AIR_VEHICLES - Decremented number of alive air vehicles (serverBD.iNumAliveHelis) to ", serverBD.iNumAliveHelis)
ENDPROC

FUNC BOOL IS_THIS_ENDLESS_WAVES()
	RETURN IS_BIT_SET(serverBD.iBitset, BITSET_SERVER_ENDLESS_WAVES_MODE)
ENDFUNC

FUNC BOOL IS_SPAWN_TEST_FINISHED()
	IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
		RETURN FALSE
	ENDIF
	
	IF IS_FAKE_MULTIPLAYER_MODE_SET()
	AND g_iTestType != 1
		RETURN FALSE
	ENDIF
	
	IF IS_LOCAL_BIT_SET(ciLocalBool_bAllPedSpawnersDone)
	AND IS_LOCAL_BIT_SET(ciLocalBool_bAllVehicleSpawnersDone)
		DEBUG_PRINTCALLSTACK()
		PRINTLN("IS_SPAWN_TEST_FINISHED - Has been finished")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_CURRENT_WAVE_LESS_THAN_MAX()
	IF IS_THIS_ENDLESS_WAVES()
		RETURN TRUE
	ENDIF
	
	IF IS_SPAWN_TEST_FINISHED()
		RETURN FALSE
	ENDIF
	
	RETURN serverBD.sWaveData.iWaveCount < serverBD.iNumberOfWaves
ENDFUNC

FUNC BOOL HAVE_ENDLESS_WAVES_STARTED()
	RETURN serverBD.sWaveData.iWaveCount > serverBD.iNumberOfWaves
ENDFUNC

FUNC BOOL IS_HALLOWEEN_MODE()
	RETURN IS_BIT_SET(serverBD.iBitset, BITSET_SERVER_HALLOWEEN_MODE)
ENDFUNC

/// PURPOSE:
///    Copies creator data into struct of boundaries data so we have meaningful names to work with.
PROC SETUP_SURVIVAL_BOUNDS_DATA()
	
	sSurvivalBoundsData.vMin	= << 0.0, 0.0, 0.0 >>
	sSurvivalBoundsData.vMax	= << 0.0, 0.0, 0.0 >>
	sSurvivalBoundsData.fWidth	= 0.0
	
	SWITCH g_FMMC_STRUCT.sSurvivalWaveData.iBoundsType
		CASE SURV_BOUNDS_NON_AXIS_AREA
			sSurvivalBoundsData.vMin	= GET_SURVIVAL_BOUNDS_POINT_WITH_LOW_OFFSET()
			sSurvivalBoundsData.vMax	= GET_SURVIVAL_BOUNDS_POINT_WITH_HEIGHT()
			sSurvivalBoundsData.fWidth	= g_FMMC_STRUCT.sSurvivalWaveData.fBoundsRadius
			PRINTLN("[Survival] - SETUP_SURVIVAL_BOUNDS_DATA - BOX - Set min survival bound as ", sSurvivalBoundsData.vMin)
			PRINTLN("[Survival] - SETUP_SURVIVAL_BOUNDS_DATA - BOX - Set max survival bound as ", sSurvivalBoundsData.vMax)
			PRINTLN("[Survival] - SETUP_SURVIVAL_BOUNDS_DATA - BOX - Set survival bound width as ", sSurvivalBoundsData.fWidth)
		BREAK
		CASE SURV_BOUNDS_CYLINDER
			sSurvivalBoundsData.vBase	= g_FMMC_STRUCT.sSurvivalWaveData.vBounds[0]
			sSurvivalBoundsData.fWidth	= g_FMMC_STRUCT.sSurvivalWaveData.fBoundsRadius
			PRINTLN("[Survival] - SETUP_SURVIVAL_BOUNDS_DATA - Cylinder - Set Survival bounds base as ", sSurvivalBoundsData.vBase)
			PRINTLN("[Survival] - SETUP_SURVIVAL_BOUNDS_DATA - Cylinder - Set Survival bounds width as ", sSurvivalBoundsData.fWidth)
		BREAK
	ENDSWITCH
	
ENDPROC

PROC SET_SEARCHING_FOR_LAND_VEHICLE(INT iNewValue)
	IF serverBd.iSearchingForLandVehicle != iNewValue
		DEBUG_PRINTCALLSTACK()
		PRINTLN("SET_SEARCHING_FOR_LAND_VEHICLE - Setting serverBd.iSearchingForLandVehicle from ", serverBd.iSearchingForLandVehicle, " to ", iNewValue)
		serverBd.iSearchingForLandVehicle = iNewValue
	ENDIF
ENDPROC

PROC SET_SEARCHING_FOR_AIR_VEHICLE(INT iNewValue)
	IF serverBd.iSearchingForChopper != iNewValue
		DEBUG_PRINTCALLSTACK()
		PRINTLN("SET_SEARCHING_FOR_AIR_VEHICLE - Setting serverBd.iSearchingForChopper from ", serverBd.iSearchingForChopper, " to ", iNewValue)
		serverBd.iSearchingForChopper = iNewValue
	ENDIF
ENDPROC

PROC SET_SEARCHING_FOR_SQUAD_PED(INT iNewValue)
	IF serverBd.iSearchingForPed != iNewValue
		DEBUG_PRINTCALLSTACK()
		PRINTLN("SET_SEARCHING_FOR_SQUAD_PED - Setting serverBd.iSearchingForPed from ", serverBd.iSearchingForPed, " to ", iNewValue)
		serverBd.iSearchingForPed = iNewValue
	ENDIF
ENDPROC

PROC ADD_TO_END_CELEBRATION_SCORE(INT iAmountToAdd)

	iEndCelebrationScreenScore += iAmountToAdd
	PRINTLN("[Survival] - ADD_TO_END_CELEBRATION_SCORE - iAmountToAdd = ", iAmountToAdd, ", iEndCelebrationScreenScore now equals ", iEndCelebrationScreenScore)

ENDPROC

FUNC BOOL ARE_ANY_SURVIVAL_CONTROLS_BEING_PRESSED()
	 
	IF NOT IS_ANY_BUTTON_PRESSED(PLAYER_CONTROL)
	AND NOT IS_ANY_BUTTON_PRESSED(FRONTEND_CONTROL)
	AND NOT IS_ANY_BUTTON_PRESSED(CAMERA_CONTROL)
		RETURN FALSE
	ENDIF

	RETURN TRUE
	
ENDFUNC

/// PURPOSE:
///    Gets if the local player should be considered for possible leecher checks.
///    Do not want spectators do be included in checks.
/// RETURNS:
///    TRUE/FALSE.
FUNC BOOL IS_LOCAL_PLAYER_MONITOR_FOR_LEECHER_CANDIDATE()
	
	IF IS_PLAYER_SCTV(localPlayer)
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_SPECTATING(localPlayer)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

/// PURPOSE:
///    Gets if the possible leecher triggers should be monitored on the local player.
///    Checks if the player is leecher check candidate and checks the survival is in the correct stage (fighting).
/// RETURNS:
///    
FUNC BOOL SHOULD_LEECHER_TRIGGERS_BE_MONITORED()
	
	IF GET_SERVER_HORDE_STAGE() != eHORDESTAGE_FIGHTING
	AND GET_SERVER_HORDE_STAGE() != eHORDESTAGE_PASSED_WAVE
    AND GET_SERVER_HORDE_STAGE() != eHORDESTAGE_DELAY_BEFORE_NEXT_WAVE
		RETURN FALSE
	ENDIF
	
	IF NOT IS_LOCAL_PLAYER_MONITOR_FOR_LEECHER_CANDIDATE()
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

#IF IS_DEBUG_BUILD
/// PURPOSE:
///    Gets the name of a might not be playing flag as a string.
/// PARAMS:
///    eFlag - flag to get as a string.
/// RETURNS:
///    flag as a STRING.
FUNC STRING GET_MIGHT_NOT_BE_PLAYING_FLAG_NAME(eMIGHT_NOT_BE_PLAYING_FLAGS eFlag)
	SWITCH eFlag
		CASE eMIGHTNOTBEPLAYINGFLAGS_NOT_DAMAGED_ENEMY		RETURN "NOT_DAMAGED_ENEMY"
		CASE eMIGHTNOTBEPLAYINGFLAGS_IDLE					RETURN "IDLE"
	ENDSWITCH
	RETURN "NOT_IN_SWITCH"
ENDFUNC
#ENDIF

/// PURPOSE:
///    Sets the local players playerBD might not be playing bitset according to the flag passed.
/// PARAMS:
///    eFlag - bit to set (enum).
///    bOn - value to set bit to.
PROC SET_MIGHT_NOT_BE_PLAYING_FLAG(eMIGHT_NOT_BE_PLAYING_FLAGS eFlag, BOOL bOn)
//	
//	#IF IS_DEBUG_BUILD
//		PRINTLN("[Survival] - set might not be playing flag ", GET_MIGHT_NOT_BE_PLAYING_FLAG_NAME(eFlag), " to ", bOn)
//	#ENDIF
//	
	IF bOn
		SET_BIT(playerBD[iLocalPart].iMightNotBePlayingBitset, ENUM_TO_INT(eFlag))
	ELSE
		CLEAR_BIT(playerBD[iLocalPart].iMightNotBePlayingBitset, ENUM_TO_INT(eFlag))
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Gets the value of a participant's playerBD might not be playing bitset accoridng to the flag passed.
/// PARAMS:
///    iParticipant - participant to check.
///    eFlag - flag to check.
/// RETURNS:
///    TRUE/FALSE to indicate of bit is set.
FUNC BOOL GET_MIGHT_NOT_BE_PLAYING_FLAG(INT iParticipant, eMIGHT_NOT_BE_PLAYING_FLAGS eFlag)
	
	RETURN IS_BIT_SET(playerBD[iParticipant].iMightNotBePlayingBitset, ENUM_TO_INT(eFlag))
	
ENDFUNC

/// PURPOSE:
///    Gets if any might not be playing flag for a participant is set.
///    Loops through the size of the might not be playing flags enum and checks the if the bit is set in the participant's playerBD.
/// PARAMS:
///    iParticipant - participant to check.
/// RETURNS:
///    TRUE/FALSE to indicate if any flag is set.
FUNC BOOL GET_PARTICIPANT_IS_ANY_MIGHT_NOT_BE_PLAYING_FLAG_SET(INT iParticipant)
	
	INT i
	
	REPEAT COUNT_OF(eMIGHT_NOT_BE_PLAYING_FLAGS) i
		IF GET_MIGHT_NOT_BE_PLAYING_FLAG(iParticipant, INT_TO_ENUM(eMIGHT_NOT_BE_PLAYING_FLAGS, i))
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    Resets all of the local player's might not be playing flags.
PROC RESET_ALL_MIGHT_NOT_BE_PLAYING_FLAGS()
	
//	PRINTLN("[Survival] - RESET_ALL_MIGHT_NOT_BE_PLAYING_FLAGS() has been called.")
	
	INT i
	
	REPEAT COUNT_OF(eMIGHT_NOT_BE_PLAYING_FLAGS) i
		IF GET_MIGHT_NOT_BE_PLAYING_FLAG(iLocalPart, INT_TO_ENUM(eMIGHT_NOT_BE_PLAYING_FLAGS, i))
			SET_MIGHT_NOT_BE_PLAYING_FLAG(INT_TO_ENUM(eMIGHT_NOT_BE_PLAYING_FLAGS, i), FALSE)
		ENDIF
	ENDREPEAT
	
ENDPROC

/// PURPOSE:
///    Updates the local players might not be playing state. Looks for each criteria and sets/unsets the bitset iMightNotBePlayingFlags in player BD.
PROC MAINTAIN_MY_MIGHT_NOT_BE_PLAYING_STATE()
	
	IF SHOULD_LEECHER_TRIGGERS_BE_MONITORED()
		
		// Monitor for the player going idle.
		IF NOT ARE_ANY_SURVIVAL_CONTROLS_BEING_PRESSED()
			IF NOT HAS_NET_TIMER_STARTED(stIdleTimer)
				START_NET_TIMER(stIdleTimer)
			ENDIF
		ELSE
			RESET_NET_TIMER(stIdleTimer)
			SET_MIGHT_NOT_BE_PLAYING_FLAG(eMIGHTNOTBEPLAYINGFLAGS_IDLE, FALSE)
		ENDIF
		IF HAS_NET_TIMER_STARTED(stIdleTimer)
			IF HAS_NET_TIMER_EXPIRED(stIdleTimer, MIGHT_NOT_BE_PLAYING_TIMER_LIMIT)
				PRINTLN("[Survival] - MAINTAIN_MY_MIGHT_NOT_BE_PLAYING_STATE -  idle timer has expired.")
				SET_MIGHT_NOT_BE_PLAYING_FLAG(eMIGHTNOTBEPLAYINGFLAGS_IDLE, TRUE)
			ENDIF
		ENDIF
		
		// Monitor for the player not damaging any enemies.
		IF NOT bIHaveDamagedAnEnemy
			IF NOT HAS_NET_TIMER_STARTED(stNotDamagedEnemyTimer)
				START_NET_TIMER(stNotDamagedEnemyTimer)
			ENDIF
		ELSE
			RESET_NET_TIMER(stNotDamagedEnemyTimer)
			SET_MIGHT_NOT_BE_PLAYING_FLAG(eMIGHTNOTBEPLAYINGFLAGS_NOT_DAMAGED_ENEMY, FALSE)
		ENDIF
		IF HAS_NET_TIMER_STARTED(stNotDamagedEnemyTimer)
			IF HAS_NET_TIMER_EXPIRED(stNotDamagedEnemyTimer, MIGHT_NOT_BE_PLAYING_TIMER_LIMIT)
				PRINTLN("[Survival] - MAINTAIN_MY_MIGHT_NOT_BE_PLAYING_STATE -  not damaged enemy timer has expired.")
				SET_MIGHT_NOT_BE_PLAYING_FLAG(eMIGHTNOTBEPLAYINGFLAGS_NOT_DAMAGED_ENEMY, TRUE)
			ENDIF
		ENDIF
	
	// reset all if not in the fighting stage.
	ELSE
		
		RESET_NET_TIMER(stIdleTimer)
		RESET_NET_TIMER(stNotDamagedEnemyTimer)
		RESET_ALL_MIGHT_NOT_BE_PLAYING_FLAGS()
		
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Gets how far away the furtherst remote player is from the local player.
/// PARAMS:
///    iParticipant - participant to check.
PROC CALCULATE_FURTHEST_PLAYER_DISTANCE(INT iParticipant)
	
	PARTICIPANT_INDEX partId = INT_TO_NATIVE(PARTICIPANT_INDEX, iParticipant)
	PED_INDEX pedId
	PED_INDEX piMidPed = LocalPlayerPed
	PED_INDEX tempPed
	
	IF NETWORK_IS_PARTICIPANT_ACTIVE(partId)
		
		pedId = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(partId))
		
		IF DOES_ENTITY_EXIST(pedId)
			IF NOT IS_ENTITY_DEAD(pedId)
				IF GET_CLIENT_HORDE_STAGE(partId) != eHORDESTAGE_SPECTATING
					
					IF IS_PLAYER_SPECTATING(localPlayer)
						tempPed = GET_SPECTATOR_CURRENT_FOCUS_PED()
						IF DOES_ENTITY_EXIST(tempPed)
							IF NOT IS_ENTITY_DEAD(tempPed)
								piMidPed = tempPed
							ENDIF
						ENDIF
					ENDIF
					
					fPartDis = GET_DISTANCE_BETWEEN_ENTITIES(pedId, piMidPed)
					
					IF fPartDis > fFurthestTargetDist
						fFurthestTargetDist = fPartDis
					ENDIF
				
				ENDIF
				
			ENDIF
		ENDIF
		
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Updates the radar zoom so the boundary of the radar is slightly further than the furthest away remote player.
/// PARAMS:
///    bInstant - ask Rowan, he wrote this.
PROC CONTROL_RADAR_ZOOM(BOOL bInstant = FALSE)

	FLOAT fZoomLevel
	IF DO_RADAR_ZOOM()
		IF NOT bInstant
			
			IF fOldFurthestTargetDist > fFurthestTargetDist
				IF (fOldFurthestTargetDist - fFurthestTargetDist) > 5
					fOldFurthestTargetDist= fOldFurthestTargetDist-cfRADAR_ZOOM_SPEED
				ENDIF
			ELSE
				IF (fFurthestTargetDist - fOldFurthestTargetDist) > 5
					fOldFurthestTargetDist= fOldFurthestTargetDist+cfRADAR_ZOOM_SPEED
				ENDIF
			ENDIF
			
			fZoomLevel = fOldFurthestTargetDist/cfRADAR_ZOOM_ADJUST
			
			// Cap the zoom level
			IF (fZoomLevel >= cfRADAR_ZOOM_CAP)
				fZoomLevel = cfRADAR_ZOOM_CAP
			ELIF (fZoomLevel < cfRADAR_ZOOM_CAP_MIN)
				fZoomLevel = cfRADAR_ZOOM_CAP_MIN
			ENDIF
			
			IF DO_RADAR_ZOOM()
				SET_RADAR_ZOOM_TO_DISTANCE(fZoomLevel)
			ENDIF
			
		ELSE
		
			fOldFurthestTargetDist = fFurthestTargetDist
			fZoomLevel = fOldFurthestTargetDist/cfRADAR_ZOOM_ADJUST
			IF DO_RADAR_ZOOM()
				SET_RADAR_ZOOM_TO_DISTANCE(fZoomLevel)
			ENDIF
			
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Gets the number of participants that are considered to be competing participants.
/// RETURNS:
///   	INT - number of fighting participants.
FUNC INT GET_NUM_COMPETITIVE_PARTICIPANTS_THIS_WAVE()
    RETURN serverBD.sFightingParticipantsList.iNumFightingParticipantsBeginningOfWave
ENDFUNC

FUNC INT GET_FIGHTING_PARTICIPANTS_AVERAGE_RANK()
    
    INT i
    INT iTotalRank
    
    REPEAT MAX_NUMBER_HORDE_FIGHTING_PARTICIPANTS i
        
        IF serverBD.sFightingParticipantsList.iParticipant[i] != (-1)
			IF NOT IS_BIT_SET(serverBD.sFightingParticipantsList.iPotentialFighterBitset, serverBD.sFightingParticipantsList.iParticipant[i])
	            IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(serverBD.sFightingParticipantsList.iParticipant[i]))
	                iTotalRank += GET_PLAYER_RANK(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(serverBD.sFightingParticipantsList.iParticipant[i])))
	            ENDIF
			ENDIF
        ENDIF
            
    ENDREPEAT
    
    RETURN (iTotalRank/serverBD.sFightingParticipantsList.iNumFightingParticipants)
    
ENDFUNC

PROC SET_ROADS(BOOL bActive)
    
	PRINTLN("[Survival] - SET_ROADS_IN_AREA - being called.")
	
	VECTOR vMin = (serverBD.vStartPos[0]-<<1000.0, 1000.0, 250.0>>)
	VECTOR vMax = (serverBD.vStartPos[0]+<<1000.0, 1000.0, 250.0>>)
	
	IF NOT bActive
		
		SET_ROADS_IN_AREA(vMin, vMax, FALSE, FALSE)
		
		PRINTLN("[Survival] - SET_ROADS - SET_ROADS_IN_AREA - set nodes off. Min = ", vMin, ", Max = ", vMax)
	
	ELSE
	
		SET_ROADS_BACK_TO_ORIGINAL(vMin, vMax)
									
		PRINTLN("[Survival] - SET_ROADS - SET_ROADS_BACK_TO_ORIGINAL - Min = ", vMin, ", Max = ", vMax)
	
	ENDIF
	
ENDPROC

PROC SET_ROADS_ON_FOR_SURVIVAL_BOUNDS()
	SWITCH g_FMMC_STRUCT.sSurvivalWaveData.iBoundsType
		CASE SURV_BOUNDS_NON_AXIS_AREA
			SET_ROADS_IN_ANGLED_AREA(GET_SURVIVAL_BOUNDS_POINT_WITH_LOW_OFFSET(), GET_SURVIVAL_BOUNDS_POINT_WITH_HEIGHT(), g_FMMC_STRUCT.sSurvivalWaveData.fBoundsRadius, FALSE, TRUE, FALSE)
		BREAK
		CASE SURV_BOUNDS_CYLINDER
			VECTOR vMin, vMax, vAreaSize
			vAreaSize = <<g_FMMC_STRUCT.sSurvivalWaveData.fBoundsRadius, g_FMMC_STRUCT.sSurvivalWaveData.fBoundsRadius, 250.0>>
			vMin = (g_FMMC_STRUCT.sSurvivalWaveData.vBounds[0]-vAreaSize)
			vMax = (g_FMMC_STRUCT.sSurvivalWaveData.vBounds[0]+vAreaSize)
			SET_ROADS_IN_ANGLED_AREA(vMin, vMax, g_FMMC_STRUCT.sSurvivalWaveData.fBoundsRadius, FALSE, TRUE, FALSE)
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL HAS_SURVIVAL_OUT_OF_BOUNDS_DATA_BEEN_SETUP()
	
	
	SWITCH g_FMMC_STRUCT.sSurvivalWaveData.iBoundsType
		CASE SURV_BOUNDS_NON_AXIS_AREA
			IF IS_VECTOR_ZERO(sSurvivalBoundsData.vMin)
				RETURN FALSE
			ENDIF
			
			IF IS_VECTOR_ZERO(sSurvivalBoundsData.vMax)
				RETURN FALSE
			ENDIF
		BREAK
		CASE SURV_BOUNDS_CYLINDER
			IF IS_VECTOR_ZERO(sSurvivalBoundsData.vBase)
				RETURN FALSE
			ENDIF
		BREAK
	ENDSWITCH
	
	IF (sSurvivalBoundsData.fWidth = 0.0)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC INT GET_PARTICIPANT_SCORE(INT iParticipant, BOOL bGetTotalKills = FALSE)
    
    IF NOT bGetTotalKills
        RETURN playerBD[iParticipant].iMyKillsThisWave
    ENDIF
    
    RETURN playerBD[iParticipant].iMyKills
    
ENDFUNC

FUNC BOOL IS_SCORE_CORONA_OPTION_ON()
	RETURN (g_FMMC_STRUCT.iRadio = SURVIVAL_SCORE_ON)
ENDFUNC

/// PURPOSE:
///    Gets if the time of day means the enemy helis should have their searchlights on.
/// RETURNS:
///    BOOL - TRUE if should have searchlights on, FALSE if not.
FUNC BOOL DOES_TOD_NEED_HELI_SEARCHLIGHTS()
	
	IF GET_CLOCK_HOURS() >= 22
		RETURN TRUE
	ENDIF
	
	IF GET_CLOCK_HOURS() <= 6
	AND GET_CLOCK_HOURS() >= 0
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    Gets if all script participants are in SURVIVAL_GAME_STATE_WAITING_TO_LEAVE.
/// RETURNS:
///    TRUE if all participants are in SURVIVAL_GAME_STATE_WAITING_TO_LEAVE, FALSE if not.
FUNC BOOL ARE_ALL_PARTICIPANTS_WAITING_TO_LEAVE()
    
    INT i
    
    REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
        IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
            IF GET_CLIENT_GAME_STATE(i) != SURVIVAL_GAME_STATE_WAITING_TO_LEAVE
                RETURN FALSE
            ENDIF
        ENDIF
    ENDREPEAT
    
    RETURN TRUE
    
ENDFUNC

FUNC BOOL ARE_ALL_PARTICIPANTS_READY_TO_DO_START_FADE_IN()
    
	INT i, iPart
    PLAYER_INDEX playerID
	
    REPEAT NUM_NETWORK_PLAYERS i
		IF IS_NET_PLAYER_OK(INT_TO_NATIVE(PLAYER_INDEX, i))
			playerID = INT_TO_NATIVE(PLAYER_INDEX, i)
			IF NOT IS_PLAYER_SCTV(playerID)
			AND NOT DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(playerID)
			AND NOT IS_PLAYER_IN_PROCESS_OF_JOINING_MISSION_AS_SPECTATOR(playerID)
				PRINTLN("[Survival] - HAVE_ALL_PARTICIPANTS_COMPLETED_START_FADE_IN - player ", i, " is not sctv")
				IF NETWORK_IS_PLAYER_A_PARTICIPANT(playerID)
		        AND NETWORK_IS_PARTICIPANT_ACTIVE(NETWORK_GET_PARTICIPANT_INDEX(playerID))
					PRINTLN("[Survival] - HAVE_ALL_PARTICIPANTS_COMPLETED_START_FADE_IN - player ", i, " part is active")
					iPart = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(playerID))
		            IF NOT IS_BIT_SET(playerBD[iPart].iBitset, BITSET_PLAYER_READY_FOR_START_FADE_IN)
						PRINTLN("[Survival] - HAVE_ALL_PARTICIPANTS_COMPLETED_START_FADE_IN - player ", i, " BITSET_PLAYER_READY_FOR_START_FADE_IN is not set.")
		                RETURN FALSE
		            ENDIF
				ENDIF
			ELSE
				PRINTLN("[Survival] - HAVE_ALL_PARTICIPANTS_COMPLETED_START_FADE_IN - player ", i, " is a spectator or joining as one")
			ENDIF
		ENDIF
    ENDREPEAT
    
    RETURN TRUE
	
ENDFUNC

FUNC BOOL HAVE_ALL_PARTICIPANTS_COMPLETED_START_FADE_IN()
    
    INT i, iPart
    PLAYER_INDEX playerID
	
    REPEAT NUM_NETWORK_PLAYERS i
		playerID = INT_TO_NATIVE(PLAYER_INDEX, i)
		IF IS_NET_PLAYER_OK(playerID)
			IF NOT IS_PLAYER_SCTV(playerID)
			AND NOT DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(playerID)
			AND NOT IS_PLAYER_IN_PROCESS_OF_JOINING_MISSION_AS_SPECTATOR(playerID)
				IF NETWORK_IS_PLAYER_A_PARTICIPANT(playerID)
		        AND NETWORK_GET_PARTICIPANT_INDEX(playerID) != INVALID_PARTICIPANT_INDEX()
				AND NETWORK_IS_PARTICIPANT_ACTIVE(NETWORK_GET_PARTICIPANT_INDEX(playerID))
					iPart = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(playerID))
		            IF NOT IS_BIT_SET(playerBD[iPart].iBitset, BITSET_PLAYER_COMPLETED_START_FADE_IN)
		                RETURN FALSE
		            ENDIF
		        ENDIF
			ENDIF
		ENDIF
    ENDREPEAT
    
    RETURN TRUE
    
ENDFUNC

PROC SETUP_MISSION_DATA_BOX(STRUCT_HORDE_BOARD_DATA &sBoardData, BIG_MESSAGE_TYPE eMessageType, INT iWaveXp, INT iCash, INT iTotalXp = 0)
	
	INT i
	INT iScoretoDisplay
	INT iNumPlayersToBeDisplayed
	
	REPEAT MAX_NUMBER_HORDE_FIGHTING_PARTICIPANTS i
		
		IF sBoardData.hudPlayers[i] != INVALID_PLAYER_INDEX()
       		IF IS_NET_PLAYER_OK(sBoardData.hudPlayers[i], FALSE)
				iScoretoDisplay += sBoardData.iScores[i]
				iNumPlayersToBeDisplayed++
			ENDIF
		ENDIF
	ENDREPEAT
	
	ADD_MISSION_SUMMARY_TITLE("FMMC_MPM_TY4")
	
	// If we have finished the mode, display the total XP earned.
	IF (GET_END_HORDE_REASON() != eENDHORDEREASON_NOT_FINISHED_YET)
		ADD_MISSION_SUMMARY_STAT_WITH_INT("MBOX_SURTOT_XP", iTotalXp)
	ENDIF
	
	ADD_MISSION_SUMMARY_STAT_WITH_INT("MBOX_WAVE_XP", iWaveXp)
	
//	ADD_MISSION_SUMMARY_END_COMPLETION_XP("MBOX_XP", iXp)
	
	IF eMessageType != BIG_MESSAGE_WAVE_COMPLETE
		ADD_MISSION_SUMMARY_STAT_STRING_AND_CASH("HRD_CASHGIVEN", iCash)
		ADD_MISSION_SUMMARY_STAT_WITH_INT("HRD_WVRCHED", serverBD.sWaveData.iWaveCount)
	ELSE
		ADD_MISSION_SUMMARY_STAT_STRING_WITH_TWO_INTS("HRD_WAVEOUT", serverBD.sWaveData.iWaveCount, serverBD.iNumberOfWaves)
	ENDIF
	
	IF iNumPlayersToBeDisplayed >= 2
		ADD_MISSION_SUMMARY_STAT_WITH_INT("HRD_KILLSTEAM", iScoreToDisplay)
	ENDIF
	
	IF sBoardData.hudPlayers[0] != INVALID_PLAYER_INDEX()
        IF IS_NET_PLAYER_OK(sBoardData.hudPlayers[0], FALSE)
			ADD_MISSION_SUMMARY_STAT_PLAYER_WITH_INT(sBoardData.hudPlayers[0], sBoardData.iScores[0])
		ENDIF
	ENDIF
	
	IF sBoardData.hudPlayers[1] != INVALID_PLAYER_INDEX()
	    IF IS_NET_PLAYER_OK(sBoardData.hudPlayers[1], FALSE)
			ADD_MISSION_SUMMARY_STAT_PLAYER_WITH_INT(sBoardData.hudPlayers[1], sBoardData.iScores[1])
		ENDIF
	ENDIF
	
	IF sBoardData.hudPlayers[2] != INVALID_PLAYER_INDEX()
	    IF IS_NET_PLAYER_OK(sBoardData.hudPlayers[2], FALSE)
			ADD_MISSION_SUMMARY_STAT_PLAYER_WITH_INT(sBoardData.hudPlayers[2], sBoardData.iScores[2])
		ENDIF
	ENDIF
	
	IF sBoardData.hudPlayers[3] != INVALID_PLAYER_INDEX()
	    IF IS_NET_PLAYER_OK(sBoardData.hudPlayers[3], FALSE)
			ADD_MISSION_SUMMARY_STAT_PLAYER_WITH_INT(sBoardData.hudPlayers[3], sBoardData.iScores[3])
		ENDIF
	ENDIF
	
	SETUP_NEW_BIG_MESSAGE(eMessageType) //SETUP_NEW_BIG_MESSAGE_WITH_2_INTS(eMessageType, iMessageValue, iXp, sStrapline) 
	
ENDPROC

FUNC BOOL HAVE_ALL_FIGHTING_PARTICIPANTS_SPAWNED_FADED_IN()
    
    INT i
    INT iPartId
    
    REPEAT MAX_NUMBER_HORDE_FIGHTING_PARTICIPANTS i
        
        IF serverBD.sFightingParticipantsList.iParticipant[i] != (-1)
			IF NOT IS_BIT_SET(serverBD.sFightingParticipantsList.iPotentialFighterBitset, serverBD.sFightingParticipantsList.iParticipant[i])
	            iPartId = serverBD.sFightingParticipantsList.iParticipant[i]
	            IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPartId))
	                IF NOT IS_BIT_SET(playerBD[iPartId].iBitset, BITSET_PLAYER_I_AM_ALIVE_AND_FADED_IN)
	                    RETURN FALSE
	                ENDIF
	            ENDIF
			ENDIF
        ENDIF
        
    ENDREPEAT
    
    RETURN TRUE
    
ENDFUNC

FUNC INT GET_TOTAL_ALIVE_GROUND_BASED_ENEMY_PEDS()
	
	RETURN (serverBD.iNumAliveSquadEnemies + serverBD.iNumAliveLandVehiclePeds)
	
ENDFUNC

FUNC BOOL HAS_EVERYONE_REACHED_TEAMCUTSCENE()

	INT iparticipant
	PARTICIPANT_INDEX tempPart

	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iparticipant
		tempPart = INT_TO_PARTICIPANTINDEX(iparticipant)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
			IF GET_CLIENT_GAME_STATE(iparticipant) < SURVIVAL_GAME_FADE_IN
				RETURN FALSE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN TRUE

ENDFUNC

FUNC FLOAT GET_WAVE_PERCENTAGE_COMPLETE()
	RETURN (TO_FLOAT(serverBD.iKillsThisWave)/TO_FLOAT(serverBD.sWaveData.iNumRequiredKills))*100.0
ENDFUNC

PROC RESET_FIGHTING_STAGE_MUSIC_EVENTS()
	
	INT i
	
	REPEAT NUM_MISSION_MUSIC_EVENTS i
		IF (INT_TO_ENUM(ENUM_MUSIC_EVENT, i) != eMUSICEVENT_WAVE_COMPLETE)
			structMusicEvent[i].bTriggered = FALSE
		ENDIF
	ENDREPEAT
	
	PRINTLN("[Survival] - [Music] - reset data for fighting stage music events..")
	
ENDPROC

FUNC TEXT_LABEL_23 GET_SURVIVAL_MUSIC_EVENT_STRING(ENUM_MUSIC_EVENT eEvent, INT iWave, BOOL bClearing = FALSE)
	DEBUG_PRINTCALLSTACK()
	TEXT_LABEL_23 tl23 = "SM_W"
	INT iWaveMusicToUse = iWave
	
	IF iWaveMusicToUse > 10
		iWaveMusicToUse = 10
	ENDIF
	
	IF IS_HALLOWEEN_MODE()
		tl23 = "SM_CS_W"
	ENDIF
	
	SWITCH eEvent
	
		CASE eMUSICEVENT_START
			IF iWave <= 1
				IF IS_HALLOWEEN_MODE()
					tl23 = "SM_CS_W1_START_ALL"
				ELSE
					tl23 = "SM_W1_START_ALL"
				ENDIF
				IF NOT bClearing
					PRINTLN("GET_SURVIVAL_MUSIC_EVENT_STRING - Using ", tl23)
				ENDIF
				RETURN tl23
			ELSE
				tl23 += iWaveMusicToUse
				tl23 += "_START"
				IF NOT bClearing
					PRINTLN("GET_SURVIVAL_MUSIC_EVENT_STRING - Using ", tl23)
				ENDIF
				RETURN tl23
			ENDIF
		BREAK	
		
		CASE eMUSICEVENT_WAVE_40P
			tl23 += iWaveMusicToUse
			tl23 += "_MED"
			IF NOT bClearing
				PRINTLN("GET_SURVIVAL_MUSIC_EVENT_STRING - Using ", tl23)
			ENDIF
			RETURN tl23			
		BREAK
		
		CASE eMUSICEVENT_WAVE_70P				
			tl23 += iWaveMusicToUse
			tl23 += "_HIGH"
			IF NOT bClearing
				PRINTLN("GET_SURVIVAL_MUSIC_EVENT_STRING - Using ", tl23)
			ENDIF
			RETURN tl23
		BREAK
		
		CASE eMUSICEVENT_WAVE_COMPLETE				
			tl23 = "SM_W"
			tl23 += iWaveMusicToUse
			tl23 += "_END"
			IF NOT bClearing
				PRINTLN("GET_SURVIVAL_MUSIC_EVENT_STRING - Using ", tl23)
			ENDIF
			RETURN tl23
		BREAK
		
		CASE eMUSICEVENT_STOP
			IF IS_HALLOWEEN_MODE()
				tl23 = "MP_SM_CS_STOP_TRACK"
			ELSE
				tl23 = "MP_SM_STOP_TRACK"
			ENDIF
			IF NOT bClearing
				PRINTLN("GET_SURVIVAL_MUSIC_EVENT_STRING - Using ", tl23)
			ENDIF
			RETURN tl23
		BREAK
		
	ENDSWITCH
	
	PRINTLN("GET_SURVIVAL_MUSIC_EVENT_STRING - eEvent: ", ENUM_TO_INT(eEvent), " iWaveToUse: ", iWaveToUse)
	NET_SCRIPT_ASSERT("GET_SURVIVAL_MUSIC_EVENT_STRING returning invalid value.")
	IF IS_HALLOWEEN_MODE()
		tl23 = "MP_SM_CS_STOP_TRACK"
	ELSE
		tl23 = "MP_SM_STOP_TRACK"
	ENDIF
	
	RETURN tl23
	
ENDFUNC

PROC SURVIVAL_START_MUSIC_EVENT(ENUM_MUSIC_EVENT eMusicEvent)
	TEXT_LABEL_23 tl23MusicEvent
	IF NOT structMusicEvent[ENUM_TO_INT(eMusicEvent)].bTriggered
		tl23MusicEvent = GET_SURVIVAL_MUSIC_EVENT_STRING(eMusicEvent, serverBD.sWaveData.iWaveCount)
		IF PREPARE_MUSIC_EVENT(tl23MusicEvent)
			TRIGGER_MUSIC_EVENT(tl23MusicEvent)
			structMusicEvent[ENUM_TO_INT(eMusicEvent)].bTriggered = TRUE
			PRINTLN("[Survival] - [Music] - triggered ", tl23MusicEvent)
			#IF IS_DEBUG_BUILD
			tlCurrentMusic = tl23MusicEvent
			#ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC START_MUSIC_FOR_JIPPER()
	PRINTLN("[Survival] - START_MUSIC_FOR_JIPPER called")
	TEXT_LABEL_23 tl23MusicEvent
	ENUM_MUSIC_EVENT eMusicEvent = eMUSICEVENT_START
	IF NOT structMusicEvent[ENUM_TO_INT(eMusicEvent)].bTriggered
		tl23MusicEvent = GET_SURVIVAL_MUSIC_EVENT_STRING(eMusicEvent, 0)
		IF PREPARE_MUSIC_EVENT(tl23MusicEvent)
			TRIGGER_MUSIC_EVENT(tl23MusicEvent)
			structMusicEvent[ENUM_TO_INT(eMusicEvent)].bTriggered = TRUE
			PRINTLN("[Survival] - [Music] - triggered ", tl23MusicEvent)
			#IF IS_DEBUG_BUILD
			tlCurrentMusic = tl23MusicEvent
			#ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SURVIVAL_MUSIC(BOOL bSpectator = FALSE)
	FLOAT fWavePercentageComplete
	IF (GET_SERVER_HORDE_STAGE() = eHORDESTAGE_FIGHTING)
	OR bSpectator
				
		fWavePercentageComplete = GET_WAVE_PERCENTAGE_COMPLETE()
		
		IF IS_SCORE_CORONA_OPTION_ON()
			IF fWavePercentageComplete < 40.0
				SURVIVAL_START_MUSIC_EVENT(eMUSICEVENT_START)
			ELIF fWavePercentageComplete < 70.0
				SURVIVAL_START_MUSIC_EVENT(eMUSICEVENT_WAVE_40P)
			ELSE
				SURVIVAL_START_MUSIC_EVENT(eMUSICEVENT_WAVE_70P)
			ENDIF
			IF NOT bMutedNearbyRadios
				START_AUDIO_SCENE("MP_POSITIONED_RADIO_MUTE_SCENE")
				bMutedNearbyRadios = TRUE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC UPDATE_HORDE_WAVES_SURVIVED_AWARD()
	IF NOT IS_ROCKSTAR_CREATED_MISSION()
	OR IS_FAKE_MULTIPLAYER_MODE_SET()
		EXIT
	ENDIF
	IF NOT bIWentFromSpectatingToPassedWaveStage
	    IF DOES_ENTITY_EXIST(LocalPlayerPed)
	        IF NOT IS_ENTITY_DEAD(LocalPlayerPed)
				IF serverBD.sWaveData.iWaveCount > GET_MP_INT_CHARACTER_AWARD(MP_AWARD_FMHORDWAVESSURVIVE)
					SWITCH serverBD.sWaveData.iWaveCount // Wave count increments as soon as the wave is done, so need to check for one higher than the wave that was just completed.
						CASE 4 
							IF GET_MP_INT_CHARACTER_AWARD(MP_AWARD_FMHORDWAVESSURVIVE) < 3
								SET_MP_INT_CHARACTER_AWARD(MP_AWARD_FMHORDWAVESSURVIVE, 3) 
								PRINTLN("[Survival] - [Awards] - given award MP_AWARD_FMHORDWAVESSURVIVE, 3.")
							ENDIF
						BREAK
						CASE 6 
							IF GET_MP_INT_CHARACTER_AWARD(MP_AWARD_FMHORDWAVESSURVIVE) < 5
								SET_MP_INT_CHARACTER_AWARD(MP_AWARD_FMHORDWAVESSURVIVE, 5) 
								PRINTLN("[Survival] - [Awards] - given award MP_AWARD_FMHORDWAVESSURVIVE, 5.")
							ENDIF
						BREAK
						CASE 8 
							IF GET_MP_INT_CHARACTER_AWARD(MP_AWARD_FMHORDWAVESSURVIVE) < 7
								SET_MP_INT_CHARACTER_AWARD(MP_AWARD_FMHORDWAVESSURVIVE, 7) 
								PRINTLN("[Survival] - [Awards] - given award MP_AWARD_FMHORDWAVESSURVIVE, 7.")
							ENDIF
						BREAK
						CASE 10
						DEFAULT
							IF GET_END_HORDE_REASON() = eENDHORDEREASON_BEAT_ALL_WAVES
								IF GET_MP_INT_CHARACTER_AWARD(MP_AWARD_FMHORDWAVESSURVIVE) < 10
									SET_MP_INT_CHARACTER_AWARD(MP_AWARD_FMHORDWAVESSURVIVE, 10) 
									PRINTLN("[Survival] - [Awards] - given award MP_AWARD_FMHORDWAVESSURVIVE, 10.")
								ENDIF
							ENDIF
						BREAK
					ENDSWITCH
				ENDIF
			ENDIF
        ENDIF
    ENDIF
	
ENDPROC

PROC MAINTAIN_TIME_SPENT_IN_PAUSE_MENU()
	
	IF NOT bBeenInPauseMenuTooLongOnWavePassed
		
		IF IS_PAUSE_MENU_ACTIVE()
			
			TIME_DATATYPE CurrentTime = GET_NETWORK_TIME()
			
			IF bGotFirstTimeForHoldValue
				iTimeInPauseMenu += GET_TIME_DIFFERENCE(CurrentTime, iTimeForHold)
			ENDIF
			
			iTimeForHold = GET_NETWORK_TIME()
			
			IF bGotFirstTimeForHoldValue
				IF iTimeInPauseMenu >= GET_TIME_FOR_BIG_MESSAGE(BIG_MESSAGE_WAVE_COMPLETE)
					bBeenInPauseMenuTooLongOnWavePassed = TRUE
				ENDIF
			ENDIF
			
			bGotFirstTimeForHoldValue = TRUE
			
		ENDIF
		
	ENDIF
	
ENDPROC

FUNC BOOL IS_PARTICIPANT_IN_FIGHTING_PARTICIPANTS_LIST(INT iParticipant)
	
	INT i
	
	REPEAT MAX_NUMBER_HORDE_FIGHTING_PARTICIPANTS i
        IF serverBD.sFightingParticipantsList.iParticipant[i] = iParticipant
			IF NOT IS_BIT_SET(serverBD.sFightingParticipantsList.iPotentialFighterBitset, serverBD.sFightingParticipantsList.iParticipant[i])
				RETURN TRUE
			ENDIF
        ENDIF
    ENDREPEAT
	
	RETURN FALSE
	
ENDFUNC

PROC PROCESS_SPECTATOR_FLAGS()
	IF NOT IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_ON_LAST_SURVIVAL_ROUND)
		IF serverBD.sWaveData.iWaveCount = serverBD.iNumberOfWaves
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SPECTATOR, "=== SURVIVAL === SET_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_ON_LAST_SURVIVAL_ROUND)")					
			#ENDIF
			SET_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_ON_LAST_SURVIVAL_ROUND)
		ENDIF
	ENDIF
ENDPROC

PROC GET_PLAYER_KILLS(STRUCT_HORDE_BOARD_DATA &sBoardData, BOOL bGetTotalKills = FALSE)
    
    INT i
    INT iIndex
    INT iScore
    
	IF NETWORK_IS_GAME_IN_PROGRESS()
	    REPEAT MAX_NUMBER_HORDE_FIGHTING_PARTICIPANTS i
			PRINTLN("[Survival] - [Wave Progression] - checking participant list index ", i)
	        IF ( serverBD.sFightingParticipantsList.iParticipant[i] > (-1) )
				PRINTLN("[Survival] - [Wave Progression] - GET_PLAYER_KILLS - serverBD.sFightingParticipantsList.iParticipant[", i, "] > (-1)")
				IF NOT IS_BIT_SET(serverBD.sFightingParticipantsList.iPotentialFighterBitset, serverBD.sFightingParticipantsList.iParticipant[i])
					PRINTLN("[Survival] - [Wave Progression] - GET_PLAYER_KILLS - serverBD.sFightingParticipantsList.iParticipant[", i, "] is not just a potential fighter.")
		            IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(serverBD.sFightingParticipantsList.iParticipant[i]))
						PRINTLN("[Survival] - [Wave Progression] - GET_PLAYER_KILLS - serverBD.sFightingParticipantsList.iParticipant[", i, "] is an active participant.")
						IF IS_BIT_SET(playerBD[serverBD.sFightingParticipantsList.iParticipant[i]].iBitset, BITSET_PLAYER_DID_INITIAL_FIGHTER_STATUS_SETUP)
							PRINTLN("[Survival] - [Wave Progression] - GET_PLAYER_KILLS - serverBD.sFightingParticipantsList.iParticipant[", i, "] has completed the initial fighter status setup.")
			                IF NOT IS_BIT_SET(playerBD[serverBD.sFightingParticipantsList.iParticipant[i]].iBitset, BITSET_PLAYER_I_WENT_STRAIGHT_TO_SPEC_CAM)
								PRINTLN("[Survival] - [Wave Progression] - GET_PLAYER_KILLS - serverBD.sFightingParticipantsList.iParticipant[", i, "] has completed the initial fighter status setup.")
			                    IF GET_CLIENT_HORDE_STAGE(INT_TO_PARTICIPANTINDEX(serverBD.sFightingParticipantsList.iParticipant[i])) != eHORDESTAGE_SCTV
									PRINTLN("[Survival] - [Wave Progression] - GET_PLAYER_KILLS - serverBD.sFightingParticipantsList.iParticipant[", i, "] is not sctv.")
			                        IF iIndex <= 3
										
										PRINTLN("[Survival] - [Wave Progression] - GET_PLAYER_KILLS - serverBD.sFightingParticipantsList.iParticipant[", i, "], index = ", iIndex)
										
			                            sBoardData.iScores[iIndex] = (-1) // Do this to invalidate each score first. This makes sure 0 is considered a valid score in leaderboard sorting and is put above unitialised scores.
										
			                            iScore = GET_PARTICIPANT_SCORE(serverBD.sFightingParticipantsList.iParticipant[i], bGetTotalKills)
										
			                            sBoardData.iScores[iIndex]				= iScore
										sBoardData.iHeadshots[iIndex]			= playerBD[i].iMyHeadshots
										sBoardData.iTeam[iIndex]				= 0
										sBoardData.iDeaths[iIndex]				= playerBD[i].iDeaths
										sBoardData.iHeadshots[iIndex]			= playerBD[i].iMyHeadshots
										
			                            PRINTLN("[Survival] - [Wave Progression] - GET_PLAYER_KILLS - participant ", i, " iScore = ", iScore, ", storing in sBoardData.iScores index ", iIndex)
			                            iIndex++
			                            PRINTLN("[Survival] - [Wave Progression] - GET_PLAYER_KILLS - sBoardData.iScores index incremented to ", iIndex)
			                        ENDIF
			                    ENDIF
			                ENDIF
						ENDIF
		            ENDIF
				ENDIF
	        ENDIF
	    ENDREPEAT
    ENDIF
	
ENDPROC

FUNC BOOL IS_PED_OUT_OF_BOUNDS(PED_INDEX pedId)
    
	IF HAS_SURVIVAL_OUT_OF_BOUNDS_DATA_BEEN_SETUP()
		IF DOES_ENTITY_EXIST(pedId)
			IF NOT IS_ENTITY_DEAD(pedId)
				VECTOR vPedCoords = GET_ENTITY_COORDS(pedId)
				IF NOT SURVIVAL_IS_POINT_IN_PLAY_AREA(vPedCoords, HAS_SURVIVAL_BOUNDS_GOT_HEIGHT())
					RETURN TRUE
				ENDIF				
			ENDIF
		ENDIF
	ELSE
		IF IS_ENTITY_ALIVE(pedId)
	        IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(pedId), serverBD.vStartPos[0]) >= AT_HORDE_DISTANCE
	            RETURN TRUE
	        ENDIF
	    ENDIF
	ENDIF
	
	IF IS_ENTITY_IN_ANGLED_AREA(pedId, <<-86.293831,-759.790771,43.217434>>, <<-78.534744,-786.002869,36.354721>>, 12.000000) // Hack to sort exploit in bug 1768177 - will get more blocking areas added to the creator to sort this properly. 
		RETURN TRUE
	ENDIF
	
    RETURN FALSE
    
ENDFUNC

PROC GIVE_WEAPON_TO_PED_AND_EQUIP(PED_INDEX pedId, WEAPON_TYPE weapontype, INT iNumberOfClips, BOOL bDontEquip = FALSE)

	INT iAmmoToGive = (GET_MAX_AMMO_IN_CLIP(pedId, weapontype) * iNumberOfClips)
	
	GIVE_WEAPON_TO_PED(pedId, weapontype, iAmmoToGive, DEFAULT, !bDontEquip)
	IF NOT bDontEquip
		SET_CURRENT_PED_WEAPON(pedId, weapontype, TRUE)
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Gets the wave the player survived until. sibtracts one from the current wave, unless the survival has been completed.
/// RETURNS:
///    INT - wave survived to.
FUNC INT GET_WAVE_SURVIVED_UNTIL()
	
	// If we have completed the survival, we have survived to max wave count.
	IF (GET_END_HORDE_REASON() = eENDHORDEREASON_BEAT_ALL_WAVES)
	AND NOT IS_THIS_ENDLESS_WAVES()
		RETURN serverBD.iNumberOfWaves
	ENDIF
	
	// One less than the current wave (if we died on the current wave then we didn't survive it).
	RETURN (serverBD.sWaveData.iWaveCount - 1)
	
ENDFUNC

FUNC BOOL HAS_ENOUGH_TIME_PASSED_ON_WAVE()
	IF NOT HAS_NET_TIMER_STARTED(serverBD.tdWaveLength)
		RETURN FALSE
	ENDIF
	PRINTLN("HAS_ENOUGH_TIME_PASSED_ON_WAVE: ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.tdWaveLength))
	RETURN HAS_NET_TIMER_EXPIRED(serverBD.tdWaveLength, 5000)
ENDFUNC

FUNC BOOL HAVE_ALL_PEDS_HAVE_BEEN_CREATED_THIS_STAGE()
	RETURN serverBD.iNumCreatedEnemiesThisStage[serverBd.sWaveData.eStage] >= serverBD.sWaveData.iNumRequiredSubstageKills[serverBd.sWaveData.eStage]
ENDFUNC

FUNC BOOL HAS_ENOUGH_TIME_PASSED_ON_SUBSTAGE()
	INT iStage = ENUM_TO_INT(serverBD.sWaveData.eStage)
	IF iStage = -1
		RETURN FALSE
	ENDIF
	IF NOT HAS_NET_TIMER_STARTED(serverBD.tdSubStageLength[iStage])
		RETURN FALSE
	ENDIF
	PRINTLN("HAS_ENOUGH_TIME_PASSED_ON_SUBSTAGE: ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.tdSubStageLength[iStage]))
	RETURN HAS_NET_TIMER_EXPIRED(serverBD.tdSubStageLength[iStage], 5000)
ENDFUNC

PROC MAINTAIN_VALID_TEST_TRACKING()
	IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
		EXIT
	ENDIF
	IF IS_LOCAL_BIT_SET(ciLocalBool_bIncrementedTestedWaveCounter)
		EXIT
	ENDIF
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sSurvivalWaveData.iTestedWavesBitSet, serverBD.sWaveData.iWaveCount)
	AND g_iTestType != 1
		SET_BIT(g_FMMC_STRUCT.sSurvivalWaveData.iTestedWavesBitSet, serverBD.sWaveData.iWaveCount)
		PRINTLN("MAINTAIN_VALID_TEST_TRACKING - Setting bit: ", serverBD.sWaveData.iWaveCount)
	ENDIF
	SET_LOCAL_BIT(ciLocalBool_bIncrementedTestedWaveCounter)

ENDPROC

FUNC BOOL IS_PARTICIPANT_VALID_FOR_NUMBER_IN_GAME(PLAYER_INDEX piPlayer, INT iPlayer)
	
	PRINTLN("IS_PARTICIPANT_VALID_FOR_NUMBER_IN_GAME - Checking player ",iPlayer," ", GET_PLAYER_NAME(piPlayer))
	
	IF IS_PLAYER_SPECTATING(piPlayer)
	AND IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(piPlayer)].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_JoinedAsSpectator)
		PRINTLN("IS_PARTICIPANT_VALID_FOR_NUMBER_IN_GAME - Returning FALSE - Player ",iPlayer, " joined as a spectator")
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_SCTV(piPlayer)
		PRINTLN("IS_PARTICIPANT_VALID_FOR_NUMBER_IN_GAME - Returning FALSE - Player ",iPlayer, " is SCTV")
		RETURN FALSE
	ENDIF
	
	IF playerBD[iPlayer].eSpectatorState > ci_SpectatorState_IDLE
	AND playerBD[iPlayer].eSpectatorState <= ci_SpectatorState_MAINTAIN
		PRINTLN("IS_PARTICIPANT_VALID_FOR_NUMBER_IN_GAME - Returning FALSE - Spectator state is invalid for player ", iPlayer)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC INT GET_PARTICIPANT_NUMBER_IN_GAME(PLAYER_INDEX piToCheck)
	INT iPlayer, iReturn
	PLAYER_INDEX piPlayer
	
	FOR iPlayer = 0 TO NUM_NETWORK_PLAYERS-1
		piPlayer = INT_TO_PLAYERINDEX(iPlayer)
		IF IS_NET_PLAYER_OK(piPlayer, FALSE)
			IF piPlayer != piToCheck
				IF IS_PARTICIPANT_VALID_FOR_NUMBER_IN_GAME(piPlayer, iPlayer)
					iReturn++
				ENDIF
			ELSE
				BREAKLOOP
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN iReturn
ENDFUNC

FUNC BOOL CAN_ACCESS_CODE_DOOR(INT iDoorHash)
	RETURN IS_DOOR_REGISTERED_WITH_SYSTEM(iDoorHash)
	AND (NOT NETWORK_IS_DOOR_NETWORKED(iDoorHash) OR NETWORK_HAS_CONTROL_OF_DOOR(iDoorHash))
ENDFUNC

FUNC BOOL SHOULD_CREATE_ZONE(INT iZone)
	IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[0])
	AND NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[1])
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC CREATE_ZONES()
	INT iZone
	
	FOR iZone = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfZones-1)
		IF NOT IS_BIT_SET(iLocalZoneBS, iZone)
			IF SHOULD_CREATE_ZONE(iZone)
				VECTOR vZonePos0, vZonePos1, vZoneMiddle
				FLOAT fSize, fHeading
				vZonePos0 = g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[0]
				vZonePos1 = g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[1]
				fSize = g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].fRadius
				vZoneMiddle = (g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[0] + g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[1])
				vZoneMiddle.x = vZoneMiddle.x/2
				vZoneMiddle.y = vZoneMiddle.y/2
				vZoneMiddle.z = vZoneMiddle.z/2
				vZoneMiddle.z = PICK_FLOAT(vZonePos0.z < vZonePos1.z, vZonePos0.z, vZonePos1.z)
				
				fHeading = GET_HEADING_BETWEEN_VECTORS(vZonePos1, vZonePos0)
				SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iType
					CASE ciSC_ZONE_DEACTIVATE_ROADS
						IF vZonePos0.z < vZonePos1.z
							vZonePos0.z -= 10
						ELSE
							vZonePos1.z -= 10
						ENDIF
						SET_ROADS_IN_ANGLED_AREA(vZonePos0, vZonePos1,fSize, FALSE, FALSE, FALSE)
						PRINTLN("CREATE_ZONES - Zone ",iZone, " Creating ciSC_ZONE_DEACTIVATE_ROADS")
					BREAK
					CASE ciSC_ZONE_NAV_MESH_BLOCKER
						VECTOR vZoneSize
						vZoneSize = vZonePos1 - vZonePos0
						vZoneSize.x = ABSF(vZoneSize.x)
						vZoneSize.y = fSize
						vZoneSize.z = ABSF(vZoneSize.z)
						
						fHeading += 90.0
						
						PRINTLN("CREATE_ZONES - Size: ", vZoneSize)
						iNavBlockerZone[iZone] = ADD_NAVMESH_BLOCKING_OBJECT(vZoneMiddle, vZoneSize, DEG_TO_RAD(fHeading))
						PRINTLN("CREATE_ZONES - Zone ",iZone, " Creating ciSC_ZONE_NAV_MESH_BLOCKER")
					BREAK
					CASE ciSC_ZONE_BLOCK_COVER
						ADD_COVER_BLOCKING_AREA(vZonePos0, vZonePos1,TRUE,FALSE,TRUE,TRUE)
						PRINTLN("[RCC MISSION][POP] CREATE_ZONES - turning off cover in area for Zone: ",iZone," pos 0: ",vZonePos0," pos 1: ",vZonePos1)
					BREAK
				ENDSWITCH
				SET_BIT(iLocalZoneBS, iZone)
				PRINTLN("CREATE_ZONES - Zone ", iZone, " has been set up.")
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

PROC CLEANUP_ZONES()
	INT iZone
	
	FOR iZone = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfZones-1)
		IF IS_BIT_SET(iLocalZoneBS, iZone)
			VECTOR vZonePos0, vZonePos1, vZoneMiddle
			FLOAT fSize
			vZonePos0 = g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[0]
			vZonePos1 = g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[1]
			fSize = g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].fRadius
			vZoneMiddle = (g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[0] + g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[1])
			vZoneMiddle.x = vZoneMiddle.x/2
			vZoneMiddle.y = vZoneMiddle.y/2
			vZoneMiddle.z = vZoneMiddle.z/2
			
			SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iType
				CASE ciSC_ZONE_DEACTIVATE_ROADS
					SET_ROADS_BACK_TO_ORIGINAL_IN_ANGLED_AREA(vZonePos0, vZonePos1, fSize, FALSE)
				BREAK
				CASE ciSC_ZONE_NAV_MESH_BLOCKER
					IF iNavBlockerZone[iZone] != -1
						REMOVE_NAVMESH_BLOCKING_OBJECT(iNavBlockerZone[iZone])
					ENDIF
				BREAK
				CASE ciSC_ZONE_BLOCK_COVER
					REMOVE_SPECIFIC_COVER_BLOCKING_AREAS(vZonePos0, vZonePos1, FALSE, FALSE, TRUE, TRUE)
				BREAK
			ENDSWITCH
			PRINTLN("CLEANUP_ZONES - Zone ", iZone, " has been cleaned up.")
		ENDIF
	ENDFOR
ENDPROC

PROC INIT_WORLD_STATE()
	INT i
	PRINTLN("[survival] INIT_WORLD_STATE")
	SET_ALL_SHOP_LOCATES_ARE_BLOCKED(TRUE, TRUE)
	SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE(TRUE)
	HIDE_ALL_SHOP_BLIPS(TRUE)
	SET_ROADS_ON_FOR_SURVIVAL_BOUNDS()
	
	VECTOR vMin = (serverBD.vBoundsMiddle-<<MAX_SURVIVAL_BOUNDS_SIZE, MAX_SURVIVAL_BOUNDS_SIZE, 250.0>>)
	VECTOR vMax = (serverBD.vBoundsMiddle+<<MAX_SURVIVAL_BOUNDS_SIZE, MAX_SURVIVAL_BOUNDS_SIZE, 250.0>>)
	SET_ROADS_IN_AREA(vMin, vMax, TRUE, FALSE)
	
	DISABLE_TELESCOPES()
	
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfDoors-1
		iDoorID[i] = -1
	ENDFOR
	INIT_LITE_DOOR_FUNCTIONALITY(iDoorID)
	IF iNavmeshBlocker = -1
	OR NOT DOES_NAVMESH_BLOCKING_OBJECT_EXIST(iNavmeshBlocker)
		VECTOR vMiddle, vSize
		vSize = <<12, 24, 5>>
		vMiddle = <<1658.3, 27.3, 159.5>>		
		iNavmeshBlocker = ADD_NAVMESH_BLOCKING_OBJECT(vMiddle, vSize, 0.0)
	ENDIF
	
	CREATE_ZONES()
	
	SET_ALL_SHOP_LOCATES_ARE_BLOCKED(TRUE, TRUE)
	SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE(TRUE)
	HIDE_ALL_SHOP_BLIPS(TRUE)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOPTIONSBS18_DISABLE_ATM_USE_IN_MISSIONS)
		SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOPTIONSBS18_DISABLE_ATM_USE_IN_MISSIONS)
	ENDIF
	
	SET_RANDOM_TRAINS(FALSE)
	
	g_bCleanupHangarModelHides = TRUE
	PRINTLN("[Survival] g_bCleanupHangarModelHides set to TRUE")
	
ENDPROC

PROC RESET_WORLD_STATE()
	INT i, iDoorHash
	PRINTLN("[survival] RESET_WORLD_STATE")
	SET_ALL_SHOP_LOCATES_ARE_BLOCKED(FALSE)
	SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE(FALSE)
	HIDE_ALL_SHOP_BLIPS(FALSE)
	
	DISABLE_TELESCOPES(FALSE)
	
	FOR i = 0 TO (FMMC_MAX_NUM_DOORS-1)
		IF iDoorID[i] != -1
			GET_NEW_DOOR_HASH(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[i].mnDoorModel,g_FMMC_STRUCT_ENTITIES.sSelectedDoor[i].vPos,iDoorHash)
			IF CAN_ACCESS_CODE_DOOR(iDoorHash)
				DOOR_SYSTEM_SET_OPEN_RATIO(iDoorHash,0,FALSE,TRUE)
				DOOR_SYSTEM_SET_DOOR_STATE(iDoorHash, DOORSTATE_FORCE_LOCKED_THIS_FRAME,FALSE, FALSE)
     			REMOVE_DOOR_FROM_SYSTEM(iDoorHash)
			ENDIF
			iDoorID[i] = -1
		ENDIF
	ENDFOR
	
	SET_ALL_SHOP_LOCATES_ARE_BLOCKED(FALSE)
	SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE(FALSE)
	HIDE_ALL_SHOP_BLIPS(FALSE)
	
	SET_RANDOM_TRAINS(TRUE)
ENDPROC

PROC SET_INTERIOR_DATA(INTERIOR_INSTANCE_INDEX iInteriorIndex, INTERIOR_DATA_STRUCT structInteriorData, INTERIOR_NAME_ENUM interiorName)
	IF sInterior.interiorName = INTERIOR_MAX_NUM
		sInterior.iInteriorIndex = iInteriorIndex
		sInterior.structInteriorData = structInteriorData
		sInterior.interiorName = interiorName
		PRINTLN("SET_INTERIOR_DATA - Interior data set.")
	ENDIF
ENDPROC

FUNC BOOL IS_INTERIOR_IPL_ONLY(INT iInterior, BOOL bBitset2)
	IF NOT bBitset2
		SWITCH iInterior
			CASE INTERIOR_FARMHOUSE RETURN TRUE
		ENDSWITCH
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL UNCAP_AND_PIN_SURVIVAL_INTERIOR(INT iInterior, BOOL bBitset2)
	
	IF IS_INTERIOR_IPL_ONLY(iInterior, bBitset2)
		PRINTLN("UNCAP_AND_PIN_SURVIVAL_INTERIOR - Interior ", iInterior, " is an IPL only")
		RETURN FALSE
	ENDIF
	
	INTERIOR_INSTANCE_INDEX iInteriorIndex		
	INTERIOR_DATA_STRUCT structInteriorData
	
	INTERIOR_NAME_ENUM interiorName = GET_INTERIOR_NAME_ENUM(iInterior, bBitset2)
	
	structInteriorData = GET_INTERIOR_DATA(interiorName)
	iInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE( structInteriorData.vPos, structInteriorData.sInteriorName )
	
	IF iInteriorIndex = NULL
	OR interiorName = INTERIOR_MAX_NUM
		PRINTLN("UNCAP_AND_PIN_SURVIVAL_INTERIOR - Interior: ", iInterior, " not valid")
		RETURN FALSE
	ENDIF
	
	SET_INTERIOR_DATA(iInteriorIndex, structInteriorData, interiorName)
	
	SET_INTERIOR_CAPPED(interiorName, FALSE)
	SET_INTERIOR_DISABLED(interiorName, FALSE)		
	SET_INTERIOR_DISABLED_ON_EXIT(interiorName, FALSE)
	
	IF IS_VALID_INTERIOR(iInteriorIndex)
		REFRESH_INTERIOR(iInteriorIndex)
		PIN_INTERIOR_IN_MEMORY(iInteriorIndex)
	ENDIF
	
	BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
	
	IF NOT IS_INTERIOR_READY(iInteriorIndex)
		PRINTLN("UNCAP_AND_PIN_SURVIVAL_INTERIOR ", structInteriorData.sInteriorName, " is not ready")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC TURN_ON_SURVIVAL_IPL(INT iInterior, BOOL bBitset2, BOOL bIPLBitset = FALSE)
	IF NOT bIPLBitset
		IF NOT bBitset2
			SWITCH iInterior
				CASE INTERIOR_FARMHOUSE
					REMOVE_IPL("farmint_cap")
					REQUEST_IPL("farmint")
					SET_INTERIOR_CAPPED(INTERIOR_V_FARMHOUSE, FALSE)
					PRINTLN("TURN_ON_SURVIVAL_IPL - Turning on Farmhouse IPL")
					BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
				BREAK
			ENDSWITCH
		ELSE
		ENDIF
	ELSE
		SWITCH iInterior
			CASE IPL_CARRIER
				REQUEST_IPL("hei_carrier")
				REQUEST_IPL("hei_Carrier_int1")
				REQUEST_IPL("hei_Carrier_int2")
				REQUEST_IPL("hei_Carrier_int3")
				REQUEST_IPL("hei_Carrier_int4")
				REQUEST_IPL("hei_Carrier_int5")
				REQUEST_IPL("hei_Carrier_int6")
				//REMOVE_IPL("hei_carrier_DistantLights") This IPL seems to have been removed from game.
				REQUEST_IPL("hei_carrier_LODLights")
				SET_AMBIENT_ZONE_LIST_STATE_PERSISTENT("AZL_HEI_AIRCRAFT_CARRIER",TRUE,TRUE)
				PRINTLN("TURN_ON_SURVIVAL_IPL - adding carrier ipl")
				SET_BIT(g_TransitionSessionNonResetVars.iFMMC_IPL_Bitset, ciFMMC_IPL_Carrier)
				BLOCK_BUILDING_CONTROLLER_ON_MISSION(TRUE)
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

PROC CLEANUP_SURVIVAL_IPL(INT iInterior, BOOL bBitset2, BOOL bIPLBitset = FALSE) 
	
	IF NOT bIPLBitset
		IF NOT bBitset2
			SWITCH iInterior
				CASE INTERIOR_FARMHOUSE
					SET_INTERIOR_CAPPED(INTERIOR_V_FARMHOUSE, TRUE)
					REMOVE_IPL("farmint")
					REQUEST_IPL("farmint_cap")
					PRINTLN("CLEANUP_SURVIVAL_IPL - Cleaning up Farmhouse IPL")
				BREAK
			ENDSWITCH
		ELSE
		ENDIF
	ELSE
		SWITCH iInterior
			CASE IPL_CARRIER
				REMOVE_IPL("hei_carrier")
				REMOVE_IPL("hei_Carrier_int1")
				REMOVE_IPL("hei_Carrier_int2")
				REMOVE_IPL("hei_Carrier_int3")
				REMOVE_IPL("hei_Carrier_int4")
				REMOVE_IPL("hei_Carrier_int5")
				REMOVE_IPL("hei_Carrier_int6")
				//REMOVE_IPL("hei_carrier_DistantLights") This IPL seems to have been removed from game.
				REMOVE_IPL("hei_carrier_LODLights")		
				SET_AMBIENT_ZONE_LIST_STATE_PERSISTENT("AZL_HEI_AIRCRAFT_CARRIER",FALSE,TRUE)
				PRINTLN("CLEANUP_SURVIVAL_IPL - ciFMMC_IPL_Carrier bit set.")
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC 

PROC CLEANUP_SURVIVAL_INTERIORS(INT iInterior, BOOL bBitset2)
	INTERIOR_NAME_ENUM interiorName = GET_INTERIOR_NAME_ENUM(iInterior, bBitset2)
	CLEAN_UP_INTERIOR(interiorName)
ENDPROC

PROC HANDLE_SURVIVAL_IPL_AND_INTERIORS(BOOL bCleanup = FALSE)
	IF g_FMMC_STRUCT.iInteriorBS = 0
	AND g_FMMC_STRUCT.iInteriorBS2 = 0
	AND g_FMMC_STRUCT.iIPLOptions = 0
		PRINTLN("HANDLE_SURVIVAL_IPL_AND_INTERIORS Nothing set")
		EXIT
	ENDIF
	INT iInterior
	
	IF bCleanup
		IF IS_NEW_LOAD_SCENE_ACTIVE()
			NEW_LOAD_SCENE_STOP()
		ENDIF
		IF IPL_GROUP_SWAP_IS_ACTIVE()
			IPL_GROUP_SWAP_CANCEL()
		ENDIF
	ENDIF

	IF g_FMMC_STRUCT.iInteriorBS > 0
		FOR iInterior = 0 TO 31
			IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, iInterior)
				IF bCleanup
					IF IS_INTERIOR_IPL_ONLY(iInterior, FALSE)
						CLEANUP_SURVIVAL_IPL(iInterior, FALSE)
					ELSE
						CLEANUP_SURVIVAL_INTERIORS(iInterior, FALSE)
					ENDIF
				ELSE
					IF IS_INTERIOR_IPL_ONLY(iInterior, FALSE)
						PRINTLN("HANDLE_SURVIVAL_IPL_AND_INTERIORS - Interior ", iInterior, " is an IPL only")
						TURN_ON_SURVIVAL_IPL(iInterior, FALSE)
					ELSE
						UNCAP_AND_PIN_SURVIVAL_INTERIOR(iInterior, FALSE)
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	
	IF g_FMMC_STRUCT.iInteriorBS2 > 0
		FOR iInterior = 0 TO 31
			IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, iInterior)
				IF bCleanup
					IF IS_INTERIOR_IPL_ONLY(iInterior, TRUE)
						CLEANUP_SURVIVAL_IPL(iInterior, TRUE)
					ELSE
						CLEANUP_SURVIVAL_INTERIORS(iInterior, TRUE)
					ENDIF
				ELSE
					IF IS_INTERIOR_IPL_ONLY(iInterior, TRUE)
						PRINTLN("HANDLE_SURVIVAL_IPL_AND_INTERIORS - Interior2 ", iInterior, " is an IPL only")
						TURN_ON_SURVIVAL_IPL(iInterior, TRUE)
					ELSE
						UNCAP_AND_PIN_SURVIVAL_INTERIOR(iInterior, TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	
	IF g_FMMC_STRUCT.iIPLOptions > 0
		FOR iInterior = 0 TO 31
			IF IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions, iInterior)
				IF bCleanup
					CLEANUP_SURVIVAL_IPL(iInterior, TRUE, TRUE)
				ELSE
					TURN_ON_SURVIVAL_IPL(iInterior, TRUE, TRUE)
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciUSE_TIGHT_INTRO_CAM)
		PRINTLN("HANDLE_SURVIVAL_IPL_AND_INTERIORS - Tight intro cameras being used.")
	ENDIF
	
	PRINTLN("HANDLE_SURVIVAL_IPL_AND_INTERIORS all interiors done")
ENDPROC

PROC SET_EARLY_END_SURVIVAL_SPECTATE(eSPECTATOR_STATE eNewState)
	DEBUG_PRINTCALLSTACK()
	PRINTLN("[LM][Survival_Spec][SET_EARLY_END_SURVIVAL_SPECTATE] - OldState: ", ENUM_TO_INT(playerBD[iLocalPart].eSpectatorState), " NewState: ", ENUM_TO_INT(eNewState)	)
	playerBD[iLocalPart].eSpectatorState = eNewState
ENDPROC

PROC SET_COMPLETED_END_OF_WAVE_HUD()
	DEBUG_PRINTCALLSTACK()
	PRINTLN("SET_COMPLETED_END_OF_WAVE_HUD - Setting BITSET_PLAYER_I_HAVE_COMPLETED_END_OF_WAVE_HUD")
	SET_BIT(playerBD[iLocalPart].iBitset, BITSET_PLAYER_I_HAVE_COMPLETED_END_OF_WAVE_HUD)
ENDPROC

PROC SET_NEW_ENDLESS_ROTATE_STATE(INT iNewState)
	PRINTLN("SET_NEW_ENDLESS_ROTATE_STATE - Setting iEndlessRotateState(",iEndlessRotateState,") to ", iNewState)
	iEndlessRotateState = iNewState
ENDPROC

PROC ENDLESS_INCREASE_COMBAT_ABILITY()
	INT iStage
	FOR iStage = 0 TO MAX_SURV_WAVE_DIFF-1
		IF (g_FMMC_STRUCT.sSurvivalWaveData.iAccuracy[iWaveToUse][iStage] + serverBD.sEndlessBonuses.iAccuracy[iStage] + 5) < 80
			serverBD.sEndlessBonuses.iAccuracy[iStage] += 5
			serverBD.sEndlessBonuses.eModType = eENDLESS_COMBAT_INCREASE
			PRINTLN("ENDLESS_INCREASE_COMBAT_ABILITY - serverBD.sEndlessBonuses. iAccuracy upped to highest accuracy of wave: ", serverBD.sEndlessBonuses.iAccuracy[iStage])
		ENDIF
	ENDFOR
ENDPROC

PROC ENDLESS_INCREASE_HEALTH_AND_ARMOUR()
	INT iStage
	FOR iStage = 0 TO MAX_SURV_WAVE_DIFF-1
		serverBD.sEndlessBonuses.iHealth[iStage] += (g_FMMC_STRUCT.sSurvivalWaveData.iHealth[iWaveToUse][iStage] / 10)
		serverBD.sEndlessBonuses.iArmour[iStage] += (g_FMMC_STRUCT.sSurvivalWaveData.iArmour[iWaveToUse][iStage] / 10)
		PRINTLN("ENDLESS_INCREASE_HEALTH_AND_ARMOUR - Bonus health substage ", iStage, " is now ", serverBD.sEndlessBonuses.iHealth[iStage])
		PRINTLN("ENDLESS_INCREASE_HEALTH_AND_ARMOUR - Bonus armour substage ", iStage, " is now ", serverBD.sEndlessBonuses.iArmour[iStage])
		serverBD.sEndlessBonuses.eModType = eENDLESS_HEALTH_INCREASE
	ENDFOR
ENDPROC

FUNC BOOL ENDLESS_INCREASE_ENEMIES()
	INT iStage
	FOR iStage = 0 TO MAX_SURV_WAVE_DIFF-1
		
		IF (g_FMMC_STRUCT.sSurvivalWaveData.iPedsPerSquad[iWaveToUse][iStage] + serverBD.sEndlessBonuses.iEnemiesInSquad[iStage] + 1) < MAX_SURV_PEDS_IN_SQUAD
			serverBD.sEndlessBonuses.iEnemiesInSquad[iStage]++
			PRINTLN("ENDLESS_INCREASE_ENEMIES - serverBD.sEndlessBonuses.iEnemiesInSquad[",iStage,"] increased to ", serverBD.sEndlessBonuses.iEnemiesInSquad[iStage])
			serverBD.sEndlessBonuses.eModType = eENDLESS_ENEMY_INCREASE
			RETURN TRUE
		ENDIF
		
		IF (g_FMMC_STRUCT.sSurvivalWaveData.iNumberOfSquads[iWaveToUse][iStage] + serverBD.sEndlessBonuses.iSquads[iStage] + 1) < MAX_NUM_ENEMY_SQUADS
			serverBD.sEndlessBonuses.iSquads[iStage]++
			PRINTLN("ENDLESS_INCREASE_ENEMIES - serverBD.sEndlessBonuses.iSquads[",iStage,"] increased to ", serverBD.sEndlessBonuses.iSquads[iStage])
			serverBD.sEndlessBonuses.eModType = eENDLESS_ENEMY_INCREASE
			RETURN TRUE
		ENDIF
		
		IF (g_FMMC_STRUCT.sSurvivalWaveData.iNumberOfHeavies[iWaveToUse][iStage] + serverBD.sEndlessBonuses.iHeavies[iStage] + 1) <= MAX_NUM_HEAVIES_ON_STAGE
			serverBD.sEndlessBonuses.iHeavies[iStage]++
			PRINTLN("ENDLESS_INCREASE_ENEMIES - serverBD.sEndlessBonuses.iHeavies[",iStage,"] increased to ", serverBD.sEndlessBonuses.iHeavies[iStage])
			serverBD.sEndlessBonuses.eModType = eENDLESS_HEAVY_INCREASE
			RETURN TRUE
		ENDIF
		
	ENDFOR
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_ENDLESS_WAVE_EXTRAS()

	IF IS_BIT_SET(serverBD.iBitset, BITSET_SERVER_ENDLESS_WAVE_EXTRAS_DONE)
	OR NOT IS_THIS_ENDLESS_WAVES()
	OR NOT HAVE_ENDLESS_WAVES_STARTED()
		EXIT
	ENDIF
	
	SWITCH serverBD.sWaveData.iEndlessWave
		CASE ciEndlessStage_ArmourRemoved
			PRINTLN("PROCESS_ENDLESS_WAVE_EXTRAS - Blocking armour spawns endless wave ", serverBD.sWaveData.iEndlessWave)
			//Set server bitset to no longer spawn armour
			SET_BIT(serverBD.iBitset, BITSET_SERVER_ENDLESS_STOP_ARMOUR)
			serverBD.sEndlessBonuses.eModType = eENDLESS_ARMOUR_REMOVAL
		BREAK
		CASE ciEndlessStage_Combat
			//Increase bonus to combat stats
			PRINTLN("PROCESS_ENDLESS_WAVE_EXTRAS - Increased combat ability endless wave ", serverBD.sWaveData.iEndlessWave)
			ENDLESS_INCREASE_COMBAT_ABILITY()
		BREAK
		CASE ciEndlessStage_Enemies
			//Increase relevant value for enemy numbers increase
			//Number in squad -> number of squads -> substages
			PRINTLN("PROCESS_ENDLESS_WAVE_EXTRAS - Increased number of enemies endless wave ", serverBD.sWaveData.iEndlessWave)
			ENDLESS_INCREASE_ENEMIES()
		BREAK
		CASE ciEndlessStage_HealthBonus
			//Increase bonus to health
			PRINTLN("PROCESS_ENDLESS_WAVE_EXTRAS - Increased health and armour endless wave ", serverBD.sWaveData.iEndlessWave)
			ENDLESS_INCREASE_HEALTH_AND_ARMOUR()
		BREAK
		CASE ciEndlessStage_HealthRemoved
			//Set server bitset to no longer spawn health
			PRINTLN("PROCESS_ENDLESS_WAVE_EXTRAS - Blocking health spawns endless wave ", serverBD.sWaveData.iEndlessWave)
			SET_BIT(serverBD.iBitset, BITSET_SERVER_ENDLESS_STOP_HEALTH)
			serverBD.sEndlessBonuses.eModType = eENDLESS_HEALTH_REMOVAL
		BREAK
		DEFAULT
			SWITCH iEndlessRotateState
				CASE ciEndlessRotateState_Combat
					//If there is the maximum number of enemies, go straight to health
					ENDLESS_INCREASE_COMBAT_ABILITY()
					PRINTLN("PROCESS_ENDLESS_WAVE_EXTRAS - Increased combat ability endless wave ", serverBD.sWaveData.iEndlessWave)
					IF IS_BIT_SET(serverBD.iBitset, BITSET_SERVER_ENDLESS_STOP_ENEMY_INCREASE)
						SET_NEW_ENDLESS_ROTATE_STATE(ciEndlessRotateState_Health)
					ELSE
						SET_NEW_ENDLESS_ROTATE_STATE(ciEndlessRotateState_Enemies)
					ENDIF
				BREAK
				CASE ciEndlessRotateState_Enemies
					
					IF NOT ENDLESS_INCREASE_ENEMIES()
						SET_BIT(serverBD.iBitset, BITSET_SERVER_ENDLESS_STOP_ENEMY_INCREASE)
						ENDLESS_INCREASE_HEALTH_AND_ARMOUR()
						PRINTLN("PROCESS_ENDLESS_WAVE_EXTRAS - Setting BITSET_SERVER_ENDLESS_STOP_ENEMY_INCREASE")
					ENDIF
					PRINTLN("PROCESS_ENDLESS_WAVE_EXTRAS - Increased number of enemies endless wave ", serverBD.sWaveData.iEndlessWave)
					SET_NEW_ENDLESS_ROTATE_STATE(ciEndlessRotateState_Health)
				BREAK
				CASE ciEndlessRotateState_Health
					ENDLESS_INCREASE_HEALTH_AND_ARMOUR()
					PRINTLN("PROCESS_ENDLESS_WAVE_EXTRAS - Increased health and armour endless wave ", serverBD.sWaveData.iEndlessWave)
					SET_NEW_ENDLESS_ROTATE_STATE(ciEndlessRotateState_Combat)
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	//Set server bit to say this has been done this wave
	PRINTLN("PROCESS_ENDLESS_WAVE_EXTRAS - Called")
	SET_BIT(serverBD.iBitset, BITSET_SERVER_ENDLESS_WAVE_EXTRAS_DONE)
ENDPROC

FUNC TEXT_LABEL_23 GET_ENDLESS_SHARD_TEXT()
	TEXT_LABEL_23 tlReturn
	SWITCH serverBD.sEndlessBonuses.eModType
		CASE eENDLESS_ARMOUR_REMOVAL 	tlReturn = "SC_EDLS_AR" BREAK
		CASE eENDLESS_HEALTH_REMOVAL	tlReturn = "SC_EDLS_HR" BREAK
		CASE eENDLESS_COMBAT_INCREASE	tlReturn = "SC_EDLS_CI" BREAK
		CASE eENDLESS_ENEMY_INCREASE	tlReturn = "SC_EDLS_EI" BREAK
		CASE eENDLESS_HEALTH_INCREASE	tlReturn = "SC_EDLS_HI" BREAK
		CASE eENDLESS_HEAVY_INCREASE	tlReturn = "SC_EDLS_HVYI" BREAK
	ENDSWITCH
	
	RETURN tlReturn
ENDFUNC

PROC PROCESS_ENDLESS_WAVE_SHARD()
	IF IS_LOCAL_BIT_SET(ciLocalBool_bEndlessShardDone)
	OR NOT IS_THIS_ENDLESS_WAVES()
	OR NOT HAVE_ENDLESS_WAVES_STARTED()
		EXIT
	ENDIF
	TEXT_LABEL_23 tl23 = GET_ENDLESS_SHARD_TEXT()
	SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GENERIC_TEXT, "SC_EDLS_WV", tl23, DEFAULT, 4000)
	
	SET_LOCAL_BIT(ciLocalBool_bEndlessShardDone)
ENDPROC

PROC CLEAR_SQUAD_SPAWN_POINTS()
	INT i, j
	FOR i = 0 TO MAX_SURV_WAVE_DIFF-1
		FOR j = 0 TO MAX_NUM_ENEMY_SQUADS-1
			serverBD_Enemies_S.structAiSquad[j][i].iSquadSpawnLocation = -1
		ENDFOR
	ENDFOR
ENDPROC

FUNC BOOL IS_SQUAD_SPAWN_FREE_FOR_THIS_SUBSTAGE(INT iSpawn, INT iSquad, INT iStage)
	INT iSquadToCheck
	PRINTLN("IS_SQUAD_SPAWN_FREE_FOR_THIS_SUBSTAGE - Checking spawner ", iSpawn, " Checking Squad ", iSquad, " Checking Stage: ", iStage)
	FOR iSquadToCheck = 0 TO serverBD.sWaveData.iNumSquads[iStage]-1
		IF iSquad != iSquadToCheck
			PRINTLN("IS_SQUAD_SPAWN_FREE_FOR_THIS_SUBSTAGE - Checking Squad ", iSquadToCheck)
			PRINTLN("IS_SQUAD_SPAWN_FREE_FOR_THIS_SUBSTAGE - Loc of passed in: ", serverBD_Enemies_S.structAiSquad[iSquad][iStage].iSquadSpawnLocation, " Loc of checking against: ", serverBD_Enemies_S.structAiSquad[iSquadToCheck][iStage].iSquadSpawnLocation)
			IF serverBD_Enemies_S.structAiSquad[iSquadToCheck][iStage].iSquadSpawnLocation = iSpawn
				RETURN FALSE
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN TRUE
ENDFUNC

FUNC BOOL DOES_SQUAD_SPAWN_HAVE_TOO_MANY_ENEMIES_FOR_THIS_SUBSTAGE(INT iSpawn, INT iSquad, INT iStage)
	INT iSquadToCheck, iTotalEnemies
	iTotalEnemies = serverBD.sWaveData.iNumPedsInSquad[iStage]
	
	FOR iSquadToCheck = 0 TO serverBD.sWaveData.iNumSquads[iStage]-1
		IF iSquad != iSquadToCheck
			PRINTLN("DOES_SQUAD_SPAWN_HAVE_TOO_MANY_ENEMIES_FOR_THIS_SUBSTAGE - Checking Squad ", iSquadToCheck)
			IF serverBD_Enemies_S.structAiSquad[iSquadToCheck][iStage].iSquadSpawnLocation = iSpawn
				PRINTLN("DOES_SQUAD_SPAWN_HAVE_TOO_MANY_ENEMIES_FOR_THIS_SUBSTAGE - Squad ", iSquadToCheck, " is on the same spawn location as checking")
				iTotalEnemies += serverBD.sWaveData.iNumPedsInSquad[iStage]
				PRINTLN("DOES_SQUAD_SPAWN_HAVE_TOO_MANY_ENEMIES_FOR_THIS_SUBSTAGE - iTotalEnemies: ", iTotalEnemies, " after adding Squad ", iSquadToCheck)
				IF iTotalEnemies >= 10
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC

FUNC BOOL ARE_ANY_PLAYERS_NEAR_COORD(VECTOR vPosition, FLOAT fDist = 10.0)
	INT i
	FLOAT fMaxDist2 = POW(fDist, 2.0)
	REPEAT NUM_NETWORK_PLAYERS i 
		PLAYER_INDEX piPlayer = INT_TO_NATIVE(PLAYER_INDEX,i)
		
		PED_INDEX ped = GET_PLAYER_PED(piPlayer)
		IF IS_ENTITY_DEAD(ped)
			RELOOP
		ENDIF
		
		IF NETWORK_IS_PLAYER_ACTIVE(piPlayer)	
			IF VDIST2(GET_ENTITY_COORDS(ped), vPosition) <= fMaxDist2
				PRINTLN("ARE_ANY_PLAYERS_NEAR_COORD - Participant ", i, " is too close")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_VEHICLE_SPAWN_FREE(INT iSpawn, INT iVehToUse)
	INT iVeh
	FOR iVeh = 0 TO serverBD.sWaveData.iNumLandVehicles-1
		IF iVeh != iVehToUse
			IF iSpawn = serverBD.sWaveData.iLVehSpawnLocation[iVeh]
				RETURN FALSE
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN TRUE
ENDFUNC

PROC CLEAR_HEAVY_UNIT_SERVER_DATA()
	INT iStage, iSquad, iPed
	FOR iStage = 0 TO MAX_SURV_WAVE_DIFF-1
		FOR iSquad = 0 TO MAX_NUM_ENEMY_SQUADS-1
			FOR iPed = 0 TO MAX_SURV_PEDS_IN_SQUAD-1
				serverBD_Enemies_S.sHeavyData.iHeavyPeds[iStage][iSquad][iPed] = -1
			ENDFOR
		ENDFOR
	ENDFOR
ENDPROC

PROC GIVE_PLAYER_WEAPONS_FOR_CREATOR_TEST()
	GIVE_WEAPON_TO_PED_AND_EQUIP(LocalPlayerPed, WEAPONTYPE_PISTOL, 50, TRUE)
	GIVE_WEAPON_TO_PED_AND_EQUIP(LocalPlayerPed, WEAPONTYPE_DLC_HOMINGLAUNCHER, 50, TRUE)
	GIVE_WEAPON_TO_PED_AND_EQUIP(LocalPlayerPed, WEAPONTYPE_SMG, 50, TRUE)
	GIVE_WEAPON_TO_PED_AND_EQUIP(LocalPlayerPed, WEAPONTYPE_HEAVYSNIPER, 50, TRUE)
	GIVE_WEAPON_TO_PED_AND_EQUIP(LocalPlayerPed, WEAPONTYPE_ASSAULTSHOTGUN, 50, TRUE)
	GIVE_WEAPON_TO_PED_AND_EQUIP(LocalPlayerPed, WEAPONTYPE_GRENADE, 50, TRUE)
	GIVE_WEAPON_TO_PED_AND_EQUIP(LocalPlayerPed, WEAPONTYPE_GRENADELAUNCHER, 50, TRUE)
	GIVE_WEAPON_TO_PED_AND_EQUIP(LocalPlayerPed, WEAPONTYPE_KNIFE, 50, TRUE)
	GIVE_WEAPON_TO_PED_AND_EQUIP(LocalPlayerPed, WEAPONTYPE_CARBINERIFLE, 50)
ENDPROC

PROC SET_GIVE_WEAPONS_STAGE(INT iNewStage)
	PRINTLN("GIVE_PLAYER_WEAPONS_AT_START - INCREMENT_GIVE_WEAPONS_STAGE - Setting iGiveWeaponsStage to ", iNewStage, " Old stage: ", iGiveWeaponsStage)
	iGiveWeaponsStage = iNewStage
ENDPROC

FUNC BOOL VALID_STARTING_WEAPON()
	WEAPON_TYPE wtWep = GET_PEDS_CURRENT_WEAPON(localPlayerPed)
	
	IF wtWep != WEAPONTYPE_INVALID
	OR wtWep != WEAPONTYPE_UNARMED
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL GIVE_PLAYER_WEAPONS_AT_START()

	SWITCH iGiveWeaponsStage
		CASE 0
			IF IS_HALLOWEEN_MODE()
				g_bMissionInventory = TRUE
				REMOVE_ALL_PED_WEAPONS(LocalPlayerPed)
				SET_BLOCK_AMMO_GLOBAL()
				GIVE_WEAPON_TO_PED_AND_EQUIP(LocalPlayerPed, WEAPONTYPE_DLC_RAYPISTOL, -1, TRUE)
				GIVE_WEAPON_TO_PED(LocalPlayerPed, WEAPONTYPE_DLC_RAYMINIGUN, 1000, DEFAULT)
				GIVE_WEAPON_TO_PED(LocalPlayerPed, WEAPONTYPE_DLC_RAYCARBINE, 1000, DEFAULT, TRUE)
				SET_CURRENT_PED_WEAPON(LocalPlayerPed, WEAPONTYPE_DLC_RAYCARBINE, TRUE)
			ELIF IS_FAKE_MULTIPLAYER_MODE_SET()
				GIVE_PLAYER_WEAPONS_FOR_CREATOR_TEST()
			ELIF (g_myStartingWeapon = WEAPONTYPE_INVALID)
				GIVE_PLAYER_STARTING_MISSION_WEAPONS(WEAPONINHAND_LASTWEAPON_BOTH)
				PRINTLN("[Survival] GIVE_PLAYER_WEAPONS_AT_START - giving player WEAPONINHAND_LASTWEAPON_BOTH.")
			ELSE
				SET_CURRENT_PED_WEAPON(LocalPlayerPed, g_myStartingWeapon, TRUE)
				PRINTLN("[Survival] GIVE_PLAYER_WEAPONS_AT_START - giving player g_myStartingWeapon - ", GET_WEAPON_NAME(g_myStartingWeapon))
			ENDIF
			SET_GIVE_WEAPONS_STAGE(1)
		BREAK
		CASE 1
			IF NOT HAS_NET_TIMER_STARTED(tdWeaponTimer)
				REINIT_NET_TIMER(tdWeaponTimer)
			ENDIF
			BOOL bProgress
			IF g_myStartingWeapon = WEAPONTYPE_UNARMED
				PRINTLN("GIVE_PLAYER_WEAPONS_AT_START - Selected unarmed")
				bProgress = TRUE
			ENDIF
			IF VALID_STARTING_WEAPON()
				PRINTLN("GIVE_PLAYER_WEAPONS_AT_START - has weapon in hand")
				bProgress = TRUE
			ENDIF
			IF HAS_NET_TIMER_EXPIRED(tdWeaponTimer, 2500)
				PRINTLN("GIVE_PLAYER_WEAPONS_AT_START - Timer expired")
				bProgress = TRUE
			ENDIF
			
			IF bProgress
				SET_GIVE_WEAPONS_STAGE(2)
			ENDIF
		BREAK
		CASE 2
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC
FUNC INT GET_UFO_LIGHT_INDEX(INT iProp)
	INT iUFO
	FOR iUFO = 0 TO ciMAX_UFO_LIGHTS-1
		IF iUFOIndex[iUFO] = iProp
			RETURN iUFO
		ENDIF
	ENDFOR
	
	RETURN -1
ENDFUNC
PROC SET_UFO_LIGHT_INDEXES()
	INT iUFO, iProp
	
	FOR iUFO = 0 TO ciMAX_UFO_LIGHTS-1
		FOR iProp = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfProps-1
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].iPropBitset2, ciFMMC_PROP2_UFO_Light)
				IF iUFOIndex[iUFO] = -1
				AND iUFOIndex[iUFO] != iProp
				AND GET_UFO_LIGHT_INDEX(iProp) = -1
					iUFOIndex[iUFO] = iProp
					PRINTLN("SET_UFO_LIGHT_INDEXES - iUFOIndex[",iUFO,"]: ", iProp)
					BREAKLOOP
				ENDIF
			ENDIF
		ENDFOR
	ENDFOR
ENDPROC

PROC SET_UFO_SOUND_INDEXES()
	INT iUFO, iProp
	FOR iUFO = 0 TO ciMAX_UFO_SOUNDS-1
		iUFOSoundID[iUFO] = -1
		FOR iProp = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfProps-1
			IF (g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].mn = INT_TO_ENUM(MODEL_NAMES, HASH("imp_prop_ship_01a")) 
			OR g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].mn = INT_TO_ENUM(MODEL_NAMES, HASH("gr_Prop_DamShip_01a"))
			OR g_FMMC_STRUCT_ENTITIES.sPlacedProp[iProp].mn = INT_TO_ENUM(MODEL_NAMES, HASH("p_spinning_anus_s")))
				IF iUFOSoundIndex[iProp] = -1
					iUFOSoundIndex[iProp] = iUFO
					PRINTLN("SET_UFO_SOUND_INDEXES - iUFOSoundIndex[",iProp,"]: ", iUFO)
					BREAKLOOP
				ENDIF
			ENDIF
		ENDFOR
	ENDFOR
ENDPROC

PROC PLAY_ALIEN_INTRO_ANIM(INT iAnim)
	SWITCH iAnim
		CASE 0
			TASK_PLAY_ANIM(localPlayerPed, "ANIM@SPECIAL_PEDS@ALIEN_LANDING", "INTRO_A", INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT)
		BREAK
		CASE 1
			TASK_PLAY_ANIM(localPlayerPed, "ANIM@SPECIAL_PEDS@ALIEN_LANDING", "INTRO_B", INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT)
		BREAK
		CASE 2
			TASK_PLAY_ANIM(localPlayerPed, "ANIM@SPECIAL_PEDS@ALIEN_LANDING", "INTRO_A", INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT)
		BREAK
		CASE 3
			TASK_PLAY_ANIM(localPlayerPed, "ANIM@SPECIAL_PEDS@ALIEN_LANDING", "INTRO_B", INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT)
		BREAK
	ENDSWITCH
ENDPROC

FUNC INT GET_ALIEN_INTRO_DELAY(INT iPartInTeam)
	INT iDelay
	SWITCH iPartInTeam
		CASE 0
			iDelay = 500
		BREAK
		CASE 1
			iDelay = 0
		BREAK
		CASE 2
			iDelay = 750
		BREAK
		CASE 3
			iDelay = 250
		BREAK
	ENDSWITCH
	RETURN iDelay
ENDFUNC

PROC PLAY_WARP_IN_PFX_ON_ENTITY(ENTITY_INDEX eiToUse)
	USE_PARTICLE_FX_ASSET("scr_gr_def")
	START_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_gr_warp_in", eiToUse, <<0,0,0>>, <<0,0,0>>)
ENDPROC

PROC DRAW_INTRO_BEAM()
	VECTOR vBeamPos, vDirection, vRotation, vScale, vTemp, vRingPos, vRingScale, vAvr
	FLOAT fZ, t, t2, fRingT, fDistance
	INT iShrinkTime, iRingTime, i, iNumberOfPlayers, iRings
	IF HAS_NET_TIMER_EXPIRED(eBeamDownUFO.tdUFOIntro, 2000)
		IF NOT HAS_NET_TIMER_STARTED(eBeamDownUFO.tdRadiusShrinkTimer)
			REINIT_NET_TIMER(eBeamDownUFO.tdRadiusShrinkTimer)
		ELSE
			iShrinkTime = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(eBeamDownUFO.tdRadiusShrinkTimer)
		ENDIF
	ELSE
		iShrinkTime = 1
	ENDIF
	
	t = TO_FLOAT(iShrinkTime) / cfUFO_BEAM
	t2 = 1.0 - (t/2)
	t = 1.0 - t
	
	iRingTime = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(eBeamDownUFO.tdRingTimer[0])
	fRingT = TO_FLOAT(iRingTime) / (cfUFO_BEAM*2)
	iNumberOfPlayers = serverBD.iNumberOfStartingPlayers
	
	FOR i = 0 TO iNumberOfPlayers-1
		vAvr += serverBD.vStartPos[i]
		IF i > 0 
			fDistance += VDIST(serverBD.vStartPos[i-1], serverBD.vStartPos[i])
		ENDIF
	ENDFOR
	fDistance = (fDistance / iNumberOfPlayers) + (7.5 * iNumberOfPlayers)
	fDistance*=t
	IF fDistance < 0
		fDistance = 0
	ENDIF
	vScale = <<fDistance, fDistance, 40.0>>
	vAvr.x = vAvr.x / iNumberOfPlayers
	vAvr.y = vAvr.y / iNumberOfPlayers
	vAvr.z = vAvr.z / iNumberOfPlayers
	fZ = 0.0
	IF GET_GROUND_Z_FOR_3D_COORD(vAvr, fZ)
		vAvr.z = fZ
	ENDIF
	
	vRingPos = vAvr
	vRingPos.z += 10.0
	vRingScale = vScale
	vRingPos.z -= 30.0*fRingT
	FOR iRings = 0 TO 5
		vRingPos.z += 1*iRings
		DRAW_CYLINDER_MARKER_FROM_VECTOR(vRingPos, vRingScale.x, 2.0, hcUFOLight, 100, TRUE, -0.1)
	ENDFOR
	
	//Main beam and Light
	GET_HUD_COLOUR(hcUFOLight, eBeamDownUFO.iR, eBeamDownUFO.iG, eBeamDownUFO.iB, eBeamDownUFO.iA)
	eBeamDownUFO.iA = 80
	DRAW_MARKER_EX(MARKER_CYLINDER, vBeamPos, vDirection, vRotation, vScale, eBeamDownUFO.iR, eBeamDownUFO.iG, eBeamDownUFO.iB, eBeamDownUFO.iA, FALSE, FALSE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
	DRAW_LIGHT_WITH_RANGE(vTemp, eBeamDownUFO.iR, eBeamDownUFO.iG, eBeamDownUFO.iB, 10*t, 50*t2)

ENDPROC

PROC DO_PLAYER_BEAM_FADE_IN()
	FLOAT fFadeTime
	INT iFadeTime, iShrinkTime
	iShrinkTime = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(eBeamDownUFO.tdUFOIntro)
	IF iShrinkTime > (4000 + GET_ALIEN_INTRO_DELAY(playerBD[iLocalPart].sPlayerData.iPartInTeam))
		IF NOT IS_LOCAL_BIT_SET(ciLocalBool_bUFOIntroAnimStarted)
			PLAY_ALIEN_INTRO_ANIM(playerBD[iLocalPart].sPlayerData.iPartInTeam)
			SET_LOCAL_BIT(ciLocalBool_bUFOIntroAnimStarted)
		ENDIF
		IF NOT HAS_NET_TIMER_STARTED(eBeamDownUFO.tdFadeIn)
			REINIT_NET_TIMER(eBeamDownUFO.tdFadeIn)
		ELSE
			iFadeTime = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(eBeamDownUFO.tdFadeIn)
			fFadeTime = TO_FLOAT(iFadeTime) / cfUFO_FADE
			fFadeTime *= 255
			IF fFadeTime > 255
				RESET_ENTITY_ALPHA(localPlayerPed)
				SET_ENTITY_VISIBLE(LocalPlayerPed, TRUE)
				BROADCAST_FMMC_PLAYER_ALPHA_WITH_WEAPON(iLocalPart, 255, GET_FRAME_COUNT())
				RESET_ENTITY_ALPHA(GET_CURRENT_PED_WEAPON_ENTITY_INDEX(LocalPlayerPed))
				SET_ENTITY_VISIBLE(GET_CURRENT_PED_WEAPON_ENTITY_INDEX(LocalPlayerPed), TRUE)
			ELSE
				SET_ENTITY_ALPHA(localPlayerPed, ROUND(fFadeTime), FALSE)
				IF (GET_FRAME_COUNT() % 5) = 0
					BROADCAST_FMMC_PLAYER_ALPHA_WITH_WEAPON(iLocalPart, ROUND(fFadeTime), GET_FRAME_COUNT())
				ENDIF
				SET_ENTITY_ALPHA(GET_CURRENT_PED_WEAPON_ENTITY_INDEX(LocalPlayerPed), ROUND(fFadeTime), FALSE)
			ENDIF
		ENDIF
	ELIF iShrinkTime > (2250 + GET_ALIEN_INTRO_DELAY(playerBD[iLocalPart].sPlayerData.iPartInTeam))
		IF NOT IS_LOCAL_BIT_SET(ciLocalBool_bBeamParticle)
			PLAY_WARP_IN_PFX_ON_ENTITY(localPlayerPed)
			SET_LOCAL_BIT(ciLocalBool_bBeamParticle)
		ENDIF
	ELIF iShrinkTime > (1500 + GET_ALIEN_INTRO_DELAY(playerBD[iLocalPart].sPlayerData.iPartInTeam))
		IF NOT IS_LOCAL_BIT_SET(ciLocalBool_UFOSpawnSound)
			PLAY_SOUND_FROM_ENTITY(-1, "Initial_Spawn", localPlayerPed, "DLC_VW_AS_Sounds")
			SET_LOCAL_BIT(ciLocalBool_UFOSpawnSound)
		ENDIF
	ELSE
		IF DOES_ENTITY_EXIST(GET_CURRENT_PED_WEAPON_ENTITY_INDEX(LocalPlayerPed))
			SET_ENTITY_ALPHA(GET_CURRENT_PED_WEAPON_ENTITY_INDEX(LocalPlayerPed), 0, FALSE)
		ENDIF
	ENDIF
ENDPROC

PROC UFO_BEAM_ANIMATION_INTROCAM()
	SWITCH eBeamDownUFO.eUFOState
		CASE eUFO_INIT
			IF NOT HAS_NET_TIMER_STARTED(eBeamDownUFO.tdRingTimer[0])
			AND NOT HAS_NET_TIMER_STARTED(eBeamDownUFO.tdUFOIntro)
				REINIT_NET_TIMER(eBeamDownUFO.tdRingTimer[0])
				REINIT_NET_TIMER(eBeamDownUFO.tdUFOIntro)
			ELSE
				SET_ENTITY_ALPHA(localPlayerPed, 0, FALSE)
				IF DOES_ENTITY_EXIST(GET_CURRENT_PED_WEAPON_ENTITY_INDEX(LocalPlayerPed))
					SET_ENTITY_ALPHA(GET_CURRENT_PED_WEAPON_ENTITY_INDEX(LocalPlayerPed), 0, FALSE)
				ENDIF
				BROADCAST_FMMC_PLAYER_ALPHA_WITH_WEAPON(iLocalPart, 0, GET_FRAME_COUNT())
				START_AUDIO_SCENE("DLC_GR_CS2_General_Scene")
				AUDIO_SCENE_TOGGLE("dlc_vw_survival_intro_scene", TRUE)
				
				eBeamDownUFO.eUFOState = eUFO_PROCESSING
				PRINTLN("UFO_BEAM_ANIMATION_INTROCAM - Setting eBeamDownUFO.eUFOState to eUFO_PROCESSING")
			ENDIF
		BREAK
		CASE eUFO_PROCESSING
			//Beam
			DRAW_INTRO_BEAM()
			
			//Fade In		
			DO_PLAYER_BEAM_FADE_IN()
			
			IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(eBeamDownUFO.tdUFOIntro) > cfUFO_INTRO
				eBeamDownUFO.eUFOState = eUFO_CLEANUP
				PRINTLN("UFO_BEAM_ANIMATION_INTROCAM - Setting eBeamDownUFO.eUFOState to eUFO_CLEANUP")
			ENDIF
			
		BREAK
		CASE eUFO_CLEANUP
			SET_LOCAL_BIT(ciLocalBool_bBeamedIn)
			RESET_NET_TIMER(eBeamDownUFO.tdRadiusShrinkTimer)
			RESET_NET_TIMER(eBeamDownUFO.tdRingTimer[0])
			RESET_NET_TIMER(eBeamDownUFO.tdUFOIntro)
			AUDIO_SCENE_TOGGLE("dlc_vw_survival_intro_scene", FALSE)
			STOP_AUDIO_SCENE("DLC_GR_CS2_General_Scene")
			eBeamDownUFO.eUFOState = eUFO_FINISHED
			PRINTLN("UFO_BEAM_ANIMATION_INTROCAM - Setting eBeamDownUFO.eUFOState to eUFO_FINISHED")
		BREAK
		CASE eUFO_FINISHED
		BREAK
	ENDSWITCH		
		
ENDPROC

PROC SET_ALIEN_WEAPON_ALPHA()
	INT iPart
	FOR iPart = 0 TO MAX_NUM_MC_PLAYERS-1
		PLAYER_INDEX tempPlayer = INT_TO_PLAYERINDEX(iPart)
		IF NETWORK_IS_PLAYER_ACTIVE(tempPlayer)
		AND NOT IS_PLAYER_SPECTATING(tempPlayer)
		
			PED_INDEX tempPed = GET_PLAYER_PED(tempPlayer)
			IF IS_ENTITY_ALIVE(tempPed)
				IF DOES_ENTITY_EXIST(GET_CURRENT_PED_WEAPON_ENTITY_INDEX(tempPed))
					SET_ENTITY_ALPHA(GET_CURRENT_PED_WEAPON_ENTITY_INDEX(tempPed), GET_ENTITY_ALPHA(tempPed), FALSE)
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

PROC PROCESS_HALLOWEEN_EVERY_FRAME()
	IF NOT IS_HALLOWEEN_MODE()
		EXIT
	ENDIF
	
	IF GET_CLIENT_GAME_STATE(iLocalPart) < SURVIVAL_GAME_STATE_RUNNING
		SET_ALIEN_WEAPON_ALPHA()
	ELSE
		IF NOT IS_PED_INJURED(localPlayerPed)
			IF NOT IS_LOCAL_BIT_SET(ciLocalBool_bhasAshPFX)
				USE_PARTICLE_FX_ASSET("scr_bike_adversary")
				START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_adversary_judgement_ash", localPlayerPed, <<0,0,0>>, <<0,0,0>>)
				SET_LOCAL_BIT(ciLocalBool_bhasAshPFX)
			ENDIF
		ELSE
			CLEAR_LOCAL_BIT(ciLocalBool_bhasAshPFX)
		ENDIF
	ENDIF
	
	IF GET_CLIENT_GAME_STATE(iLocalPart) <= SURVIVAL_GAME_STATE_RUNNING
		g_GIVE_PLAYER_ALL_UNLOCKED_AND_EQUIPED_WEAPONS_FM_CHECK = FALSE
		IF NOT ANIMPOSTFX_IS_RUNNING("SurvivalAlien")
			ANIMPOSTFX_PLAY("SurvivalAlien", 0, TRUE)
			PRINTLN("PROCESS_HALLOWEEN_EVERY_FRAME - Starting Anim Post FX")
		ENDIF
	ENDIF

ENDPROC

FUNC BOOL IS_COMBAT_VEHICLE(VEHICLE_INDEX tempVeh)
	
	SWITCH GET_ENTITY_MODEL(tempVeh)
		CASE RHINO
		CASE LAZER
		CASE BUZZARD
		CASE ANNIHILATOR
		CASE SAVAGE
		CASE TAMPA3
		CASE HUNTER
		CASE VALKYRIE
		CASE KHANJALI
		CASE AKULA
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC
FUNC BOOL IS_COMBAT_HELI_WITH_MISSILES(VEHICLE_INDEX tempVeh)
	
	SWITCH GET_ENTITY_MODEL(tempVeh)
		CASE HUNTER
		CASE SAVAGE
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC
FUNC INT GET_FORCED_NUMBER_OF_VEHICLE_SEATS(MODEL_NAMES eModel, INT iSeats)
	SWITCH eModel
		CASE AVENGER
		CASE SEASPARROW 
		CASE MOGUL 
			RETURN 2
		CASE RHINO
		CASE KHANJALI
			RETURN 1
	ENDSWITCH
	RETURN iSeats
ENDFUNC

FUNC INT GET_SAFE_NUMBER_OF_VEHICLE_PEDS(MODEL_NAMES mnVeh, INT iVeh)
	INT iPeds = serverBD_Enemies_V.sSurvivalLandVehicle[iVeh].iNumPeds
	
	IF GET_VEHICLE_MODEL_NUMBER_OF_SEATS(mnVeh) < serverBD_Enemies_V.sSurvivalLandVehicle[iVeh].iNumPeds
		iPeds = GET_VEHICLE_MODEL_NUMBER_OF_SEATS(mnVeh)
		PRINTLN("GET_SAFE_NUMBER_OF_VEHICLE_PEDS - Force changing number of peds in vehicle to ", iPeds)
	ENDIF
	
	iPeds = GET_FORCED_NUMBER_OF_VEHICLE_SEATS(mnVeh, iPeds)
	
	RETURN iPeds
ENDFUNC

FUNC INT GET_DELAY_BETWEEN_WAVES_LENGTH()
	IF IS_FAKE_MULTIPLAYER_MODE_SET()
	AND g_iTestType = 1
		RETURN TEST_MODE_DELAY_BETWEEN_WAVES
	ENDIF
	RETURN DELAY_BETWEEN_WAVES
ENDFUNC

PROC SURVIVAL_SERVER_SET_PED_AND_TRAFFIC_DENSITIES(INT& iHandle, FLOAT fPedDensity, FLOAT fTrafficDensity)
	BOOL bLocalOnly = FALSE
	VECTOR vMinWorldLimitCoords = <<-14000, -14000, -1700>>
	VECTOR vMaxWorldLimitCoords = <<14000, 14000, 2700>>
	IF iHandle = -1
		iHandle = ADD_POP_MULTIPLIER_AREA(vMinWorldLimitCoords, vMaxWorldLimitCoords, fPedDensity, fTrafficDensity, bLocalOnly)
		PRINTLN("[POP] [SURVIVAL_SERVER_SET_PED_AND_TRAFFIC_DENSITIES] fPedDensity = ", fPedDensity, "  fTrafficDensity = ", fTrafficDensity, " iHandle = ", iHandle)
	ENDIF
ENDPROC

PROC SURVIVAL_SERVER_CLEAR_PED_AND_TRAFFIC_DENSITIES(INT iHandle)
	BOOL bLocalOnly = FALSE
	IF iHandle > -1
		//Added to stop assert - 2116964 - No area registered for index 0 - either not registered or already freed up?
		IF DOES_POP_MULTIPLIER_AREA_EXIST(iHandle)
			REMOVE_POP_MULTIPLIER_AREA(iHandle, bLocalOnly)
		ENDIF
	ENDIF
	PRINTLN("[POP] [SURVIVAL_SERVER_CLEAR_PED_AND_TRAFFIC_DENSITIES] iHandle = ", iHandle)
ENDPROC

FUNC BOOL IS_SOUND_ID_VALID(INT iSoundID)
	RETURN iSoundID >= 0
ENDFUNC

PROC CLEAR_SOUND_ID(INT& iSoundToClear)
	IF IS_SOUND_ID_VALID(iSoundToClear)
		PRINTLN("CLEAR_SOUND_ID - Clearing ID ", iSoundToClear)
		RELEASE_SOUND_ID(iSoundToClear)
		iSoundToClear = -1
	ELSE
		PRINTLN("CLEAR_SOUND_ID - Invalid ID ", iSoundToClear)
	ENDIF
ENDPROC

PROC STOP_PICKUP_SOUND(INT iPickup)
	IF IS_SOUND_ID_VALID(iPickupSounds[iPickup])
		STOP_SOUND(iPickupSounds[iPickup])
		CLEAR_BIT(iPickupSoundBitSet, iPickup)
		CLEAR_SOUND_ID(iPickupSounds[iPickup])
		PRINTLN("STOP_PICKUP_SOUND - Pickup: ", iPickup)
	ENDIF
ENDPROC

FUNC STRING GET_SOUND_SET_FOR_SURVIVAL()
	STRING sSoundSet = "DLC_VW_Survival_Sounds"
	IF IS_HALLOWEEN_MODE()
		sSoundSet = "DLC_VW_AS_Sounds"
	ENDIF
	RETURN sSoundSet
ENDFUNC

PROC PROCESS_DISABLE_REALTIME_MULTIPLAYER()
	
	IF g_bBlock_PROCESS_DISABLE_REALTIME_MULTIPLAYER
		PRINTLN("PROCESS_DISABLE_REALTIME_MULTIPLAYER - GLOBAL BLOCK SET - EXITING")
		EXIT
	ENDIF
	
	IF g_sMPTunables.bDisableRealtimeMPCheck_JIP_SURVIVAL // If set use the functionality tested before url:bugstar:7534395
		IF g_sMPTunables.bEnableRealtimeMPCheck_Survival
		AND GET_CLIENT_HORDE_STAGE(PARTICIPANT_ID()) >= eHORDESTAGE_SPECTATING
		AND GET_CLIENT_HORDE_STAGE(PARTICIPANT_ID()) != eHORDESTAGE_SCTV
			NETWORK_DISABLE_REALTIME_MULTIPLAYER()
			PRINTLN("PROCESS_DISABLE_REALTIME_MULTIPLAYER - Calling NETWORK_DISABLE_REALTIME_MULTIPLAYER for Survivals - bDisableRealtimeMPCheck_JIP_SURVIVAL")
		ENDIF
	ELSE
		IF g_sMPTunables.bEnableRealtimeMPCheck_Survival
		AND GET_CLIENT_HORDE_STAGE(PARTICIPANT_ID()) = eHORDESTAGE_REWARDS_AND_FEEDACK
			NETWORK_DISABLE_REALTIME_MULTIPLAYER()
			PRINTLN("PROCESS_DISABLE_REALTIME_MULTIPLAYER - Calling NETWORK_DISABLE_REALTIME_MULTIPLAYER for Survivals")
		ENDIF
	ENDIF
	
ENDPROC

