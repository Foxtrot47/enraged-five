USING "FM_Survival_Controller_Using.sch"
USING "FM_Survival_Controller_Utility.sch"

PROC ADD_PICKUP_BLIP(INT iBlip)
	IF NOT DOES_BLIP_EXIST(biPickupBlip[iBlip])
		PRINTLN("ADD_PICKUP_BLIP - Adding blip for pickup: ", iBlip, " Type: ", GET_CREATOR_NAME_FOR_PICKUP_TYPE(ptWaveLoadout[iBlip]), " (", ptWaveLoadout[iBlip],")")
        biPickupBlip[iBlip]=ADD_BLIP_FOR_COORD(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iBlip].vPos)
        SET_BLIP_SPRITE(biPickupBlip[iBlip], GET_CORRECT_BLIP_SPRITE_FMMC(ptWaveLoadout[iBlip]))
        SET_BLIP_SCALE(biPickupBlip[iBlip], BLIP_SIZE_NETWORK_PICKUP_LARGE)
        SHOW_HEIGHT_ON_BLIP(biPickupBlip[iBlip], TRUE)
		SET_BLIP_PRIORITY(biPickupBlip[iBlip], BLIPPRIORITY_LOWEST)
		IF IS_PICKUP_HEALTH(ptWaveLoadout[iBlip])
			SET_BLIP_COLOUR(biPickupBlip[iBlip], BLIP_COLOUR_GREEN)
		ELIF IS_PICKUP_ARMOUR(ptWaveLoadout[iBlip])
			SET_BLIP_COLOUR(biPickupBlip[iBlip], BLIP_COLOUR_BLUE)
		ENDIF
    ENDIF
ENDPROC

PROC REMOVE_PICKUP_BLIP(INT iBlip)
	IF DOES_BLIP_EXIST(biPickupBlip[iBlip])
        REMOVE_BLIP(biPickupBlip[iBlip])
    ENDIF
ENDPROC

PROC PROCESS_PICKUP_BLIPS()
    
	INT i
	
	SWITCH iProcessPickupBlipsStage
		// Initial add of blips so they don;t all appear in a messy stagger onto the radar.
		CASE 0
			REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons i
				IF DOES_PICKUP_EXIST(piPickup[i])
					ADD_PICKUP_BLIP(i)
				ENDIF
			ENDREPEAT
			iProcessPickupBlipsStage++
		BREAK
		
		// Staggered processing of pickup blips after initial add.
		CASE 1
			IF DOES_PICKUP_EXIST(piPickup[iStaggeredPickUpBlipCount])
		        IF NOT DOES_PICKUP_OBJECT_EXIST(piPickup[iStaggeredPickUpBlipCount])
		            REMOVE_PICKUP_BLIP(iStaggeredPickUpBlipCount)
		        ELSE
		            ADD_PICKUP_BLIP(iStaggeredPickUpBlipCount)
		        ENDIF
		    ELSE
		        REMOVE_PICKUP_BLIP(iStaggeredPickUpBlipCount)
		    ENDIF
		    
		    iStaggeredPickUpBlipCount++
		    
		    IF iStaggeredPickUpBlipCount >= g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons
		        iStaggeredPickUpBlipCount = 0
		    ENDIF
		BREAK
	ENDSWITCH    
ENDPROC

PROC PROCESS_HALLOWEEN_PICKUPS()
	IF !IS_HALLOWEEN_MODE()
		EXIT
	ENDIF
	
	INT i
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons-1
		
		IF DOES_PICKUP_EXIST(piPickup[i])
			IF HAS_PICKUP_BEEN_COLLECTED(piPickup[i])
				PRINTLN("PROCESS_HALLOWEEN_PICKUPS - Pickup ", i, " collected Sound ID: ", iPickupSounds[i])
				STOP_SOUND(iPickupSounds[i])
				CLEAR_SOUND_ID(iPickupSounds[i])
			ENDIF
			IF DOES_PICKUP_OBJECT_EXIST(piPickup[i])
				OBJECT_INDEX oiPickup = GET_PICKUP_OBJECT(piPickup[i])
				IF DOES_ENTITY_EXIST(oiPickup)
					IF NOT IS_BIT_SET(iPickupParticleBitSet, i)
						USE_PARTICLE_FX_ASSET("scr_gr_def")
						START_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_gr_warp_in", oiPickup, <<0,0,0>>, <<0,0,0>>)
						SET_BIT(iPickupParticleBitSet, i)
						PRINTLN("[Survival] - SOUNDS - Pickup_Spawn playing from Entity")
						SET_BIT(iPickupParticleBitSet, 31)
					ENDIF
					IF ptWaveLoadout[i] = PICKUP_HEALTH_STANDARD
						IF NOT IS_BIT_SET(iPickupSoundBitSet, i)
							IF IS_SOUND_ID_VALID(iPickupSounds[i])
								IF HAS_SOUND_FINISHED(iPickupSounds[i])
									PLAY_SOUND_FROM_ENTITY(iPickupSounds[i], "Health_Pickup_Loop", oiPickup, "DLC_VW_AS_Sounds")
									PRINTLN("[Survival] - SOUNDS - Health_Pickup_Loop playing from Entity")
									SET_BIT(iPickupSoundBitSet, i)
								ENDIF
							ELSE
								iPickupSounds[i] = GET_SOUND_ID()
								PRINTLN("[Survival] - SOUNDS - Setting sound ID for pickup ", i," SoundID: ",iPickupSounds[i])
							ENDIF
						ENDIF
					ELIF ptWaveLoadout[i] = PICKUP_ARMOUR_STANDARD
						IF NOT IS_BIT_SET(iPickupSoundBitSet, i)
							IF IS_SOUND_ID_VALID(iPickupSounds[i])
								IF HAS_SOUND_FINISHED(iPickupSounds[i])
									PLAY_SOUND_FROM_ENTITY(iPickupSounds[i], "Armour_Pickup_Loop", oiPickup, "DLC_VW_AS_Sounds")
									PRINTLN("[Survival] - SOUNDS - Armour_Pickup_Loop playing from Entity")
									SET_BIT(iPickupSoundBitSet, i)
								ENDIF
							ELSE
								iPickupSounds[i] = GET_SOUND_ID()
								PRINTLN("[Survival] - SOUNDS - Setting sound ID for pickup ", i ," SoundID: ",iPickupSounds[i])
							ENDIF
						ENDIF
					ELSE
						STOP_PICKUP_SOUND(i)
					ENDIF
				ELSE
					STOP_PICKUP_SOUND(i)
				ENDIF
			ELSE
				CLEAR_BIT(iPickupParticleBitSet, i)
				STOP_PICKUP_SOUND(i)
			ENDIF
		ELSE
			STOP_PICKUP_SOUND(i)
		ENDIF
	ENDFOR
	
ENDPROC

PROC PROCESS_PICKUP_SOUNDS()

	IF NOT IS_HALLOWEEN_MODE()
		EXIT
	ENDIF
	
	INT i
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons-1
		IF NOT IS_BIT_SET(iPickupOneShotSoundBitSet, i)
			IF DOES_PICKUP_EXIST(piPickup[i])
				IF DOES_PICKUP_OBJECT_EXIST(piPickup[i])
					OBJECT_INDEX oiPickup = GET_PICKUP_OBJECT(piPickup[i])
					IF DOES_ENTITY_EXIST(oiPickup)
						PLAY_SOUND_FROM_ENTITY(-1, "Pickup_Spawn", oiPickup, GET_SOUND_SET_FOR_SURVIVAL())
						PRINTLN("[Survival] - SOUNDS - Pickup_Spawn playing from Entity")
						SET_BIT(iPickupOneShotSoundBitSet, i)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

PROC PROCESS_PICKUP_LOGIC()
	PROCESS_PICKUP_BLIPS()
	
	PROCESS_PICKUP_SOUNDS()
	
	PROCESS_HALLOWEEN_PICKUPS()
ENDPROC

FUNC BOOL SHOULD_PICKUP_BE_CREATED(INT iPickup)
	
	IF IS_BIT_SET(serverBD.iBitset, BITSET_SERVER_ENDLESS_STOP_ARMOUR)
	AND ptWaveLoadout[iPickup] = PICKUP_ARMOUR_STANDARD
		PRINTLN("SHOULD_PICKUP_BE_CREATED - Pickup ", iPickup, " not being created because Endless Waves stopping armour")
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(serverBD.iBitset, BITSET_SERVER_ENDLESS_STOP_HEALTH)
	AND ptWaveLoadout[iPickup] = PICKUP_HEALTH_STANDARD
		PRINTLN("SHOULD_PICKUP_BE_CREATED - Pickup ", iPickup, " not being created because Endless Waves stopping health")
		RETURN FALSE
	ENDIF
	
	INT iPickupArray = iPickup
	IF iPickupArray >= MAX_LOADOUT_WEAPONS
		iPickupArray -= MAX_LOADOUT_WEAPONS
	ENDIF
	IF g_FMMC_STRUCT.sSurvivalWaveData.iLoadoutWeaponCat[iWaveToUse][iPickupArray] = FMMC_WEAPON_LIBRARY_SPECIAL
		PRINTLN("SHOULD_PICKUP_BE_CREATED - Pickup ", iPickup, " not being created because Loadout override is set to None")
		RETURN FALSE
	ENDIF
	
	IF ptWaveLoadout[iPickup] != PICKUP_ARMOUR_STANDARD
	AND ptWaveLoadout[iPickup] != PICKUP_HEALTH_STANDARD
		PICKUP_TYPE ptOverride = g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickup].ptSurvival_WeaponOverridePerWave[iWaveToUse]
		IF ptOverride != NUM_PICKUPS
			IF GET_WEAPON_TYPE_FROM_PICKUP_TYPE(ptOverride) = WEAPONTYPE_INVALID
				PRINTLN("SHOULD_PICKUP_BE_CREATED - Pickup ", iPickup, " not being created because Weapon Override is set to None")
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL CREATE_PICKUPS()

    INT i
    INT iPlacementFlags = 0
    VECTOR vPos
   	INT iAmmo
	
    SET_FM_PICKUP_ORIENTATION_FLAGS(iPlacementFlags)
    SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_MAP))
    
    IF g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons = 0
        RETURN TRUE
    ENDIF
    
    REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons i
        IF NOT DOES_PICKUP_EXIST(piPickup[i])
            vPos = g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].vPos
            IF NOT IS_VECTOR_ZERO(vPos)
            AND ptWaveLoadout[i] != NUM_PICKUPS
			AND SHOULD_PICKUP_BE_CREATED(i)
                PRINTLN("[Survival] - making pickup: ", i, " at the coordinates: ", vPos)
                PRINTLN("[Survival] - making pickup: ", i, " - pickup type = ", ENUM_TO_INT(ptWaveLoadout[i]))
                IF ptWaveLoadout[i] = PICKUP_WEAPON_GRENADELAUNCHER
				OR ptWaveLoadout[i] = PICKUP_WEAPON_GRENADE
					iAmmo = 2
				ELSE
					iAmmo = GET_AMMO_AMOUNT_FOR_MP_PICKUP(GET_WEAPON_TYPE_FROM_PICKUP_TYPE(ptWaveLoadout[i]))
				ENDIF
				// We give rpgs on the waves with helicopters. Give one more rocket than the number of helis there are.
				IF (ptWaveLoadout[i] = PICKUP_WEAPON_RPG)
					SWITCH serverBd.sWaveData.iWaveCount
						CASE 4
							iAmmo = 2
						BREAK
						CASE 7
							IF serverBD.sFightingParticipantsList.iNumFightingParticipants > 1
								iAmmo = 3
							ENDIF
						BREAK
						CASE 10
							IF serverBD.sFightingParticipantsList.iNumFightingParticipants > 2
								iAmmo = 4
							ELIF serverBD.sFightingParticipantsList.iNumFightingParticipants > 1
								iAmmo = 3
							ENDIF
						BREAK
					ENDSWITCH
				ELIF ptWaveLoadout[i] = PICKUP_WEAPON_DLC_RAYMINIGUN
					iAmmo = 200
				ENDIF
				IF serverBD.sWaveData.iWaveCount <= 5
				OR ptWaveLoadout[i] = PICKUP_HEALTH_STANDARD
				OR ptWaveLoadout[i] = PICKUP_ARMOUR_STANDARD
			    	CLEAR_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_REGENERATES))
				ELSE
					SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_REGENERATES))
				ENDIF
				MODEL_NAMES mnModelOverride = DUMMY_MODEL_FOR_SCRIPT
				IF IS_HALLOWEEN_MODE()
					IF ptWaveLoadout[i] = PICKUP_HEALTH_STANDARD
						mnModelOverride = prop_alien_egg_01
					ELIF ptWaveLoadout[i] = PICKUP_ARMOUR_STANDARD
						mnModelOverride = PROP_POWER_CELL
					ENDIF
				ENDIF
				piPickup[i] = CREATE_PICKUP_ROTATE(ptWaveLoadout[i], GET_SAFE_PICKUP_COORDS(vPos, 0.0, 4.0) + <<0,0,0.40>>, <<0,360,0>>, iPlacementFlags, iAmmo, DEFAULT, DEFAULT, mnModelOverride)
				IF serverBD.sWaveData.iWaveCount = 1
					SET_BIT(iPickupParticleBitSet, i)
					SET_BIT(iPickupOneShotSoundBitSet, i)
				ENDIF
            ENDIF
        ENDIF
    ENDREPEAT
    
    REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons i
        IF NOT DOES_PICKUP_EXIST(piPickup[i])
		AND SHOULD_PICKUP_BE_CREATED(i)
            RETURN FALSE
        ENDIF
    ENDREPEAT
    
    RETURN TRUE
    
ENDFUNC

PROC REMOVE_PICKUPS()
    
    INT i
    
    REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons i
        IF DOES_PICKUP_EXIST(piPickup[i])
            PRINTLN("[Survival] - removing pickup: ", i)
			STOP_PICKUP_SOUND(i)
			CLEAR_BIT(iPickupSoundBitSet, i)
            REMOVE_PICKUP(piPickup[i])
			REMOVE_PICKUP_BLIP(i)
			CLEAR_BIT(iPickupParticleBitSet, i)
			CLEAR_BIT(iPickupOneShotSoundBitSet, i)
        ENDIF
    ENDREPEAT
    
	iProcessPickupBlipsStage = 0 // For blip processing.
	
ENDPROC

PROC CREATE_PICKUPS_FOR_NEW_WAVE(INT &iCreateStage)
    SWITCH iCreateStage
        CASE 0
            SET_PICKUP_TYPES_FOR_WAVE()
            iCreateStage++
            PRINTLN("[Survival] - setup new pickups types. Going to iCreateStage ", iCreateStage)
        BREAK
        CASE 1
            IF CREATE_PICKUPS()
                iCreateStage++
                PRINTLN("[Survival] - created new pickups. Going to iCreateStage ", iCreateStage)
            ENDIF
        BREAK
        CASE 2
            // New pickups created!
        BREAK
    ENDSWITCH
ENDPROC
