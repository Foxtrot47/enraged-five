//╔═════════════════════════════════════════════════════════════════════════════╗
//║		FM_Race_Controler														║
//║		Christopher Speirs														║
//╚═════════════════════════════════════════════════════════════════════════════╝	

USING "globals.sch"
USING "net_races.sch" 
USING "FMMC_MP_Setup_Mission.sch"
USING "net_script_tunables.sch"		// KGM 25/7/12 - added so that Refresh_MP_Script_Tunables() can be called to gather Race specific values
USING "FMMC_HEADER.sch"
USING "net_wait_zero.sch"

#IF IS_DEBUG_BUILD
USING "profiler.sch"
#ENDIF

//╔═════════════════════════════════════════════════════════════════════════════╗
//║				VARIABLES														║
//╚═════════════════════════════════════════════════════════════════════════════╝
BIG_RACE_STRUCT sharedBigRaceVars

ServerBroadcastData serverBD 
PlayerBroadcastData playerBD[FMMC_MAX_NUM_RACERS]
SC_LB_DATA_BIG sharedSCLbd
SC_LBD_PLAYER_LIST sharedLBDPlayers[FMMC_MAX_NUM_RACERS]
LEADERBOARD_PLACEMENT_TOOLS Placement 
INT iLoopParticipant


//╔═════════════════════════════════════════════════════════════════════════════╗
//║				PROCS															║
//╚═════════════════════════════════════════════════════════════════════════════╝
PROC SET_TEAM_RELATIONSHIPS(RELATIONSHIP_TYPE relType, REL_GROUP_HASH relGroup)
	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		IF relGroup <> sharedBigRaceVars.rgFM_RACES[i]
			SET_RELATIONSHIP_BETWEEN_GROUPS(relType, relGroup, sharedBigRaceVars.rgFM_RACES[i]) 
		ENDIF
	ENDREPEAT
ENDPROC

PROC MAKE_RIVAL_TEAMS_HATE_EACH_OTHER()
	INT i
	
	// Setup rel groups
	REPEAT NUM_NETWORK_PLAYERS i
		TEXT_LABEL_63 tlRelGroup
		tlRelGroup = "sharedBigRaceVars.rgFM_RACES"
		tlRelGroup += i
		ADD_RELATIONSHIP_GROUP(tlRelGroup, sharedBigRaceVars.rgFM_RACES[i])
	ENDREPEAT
	REPEAT NUM_NETWORK_PLAYERS i
		IF IS_THIS_TARGET_RACE(serverBD) AND IS_DISABLE_DAMAGE_TO_VEHICLE_SET()
			SET_TEAM_RELATIONSHIPS(ACQUAINTANCE_TYPE_PED_LIKE, sharedBigRaceVars.rgFM_RACES[i])
		ELSE
	 // #IF FEATURE_TARGET_RACES
			SET_TEAM_RELATIONSHIPS(ACQUAINTANCE_TYPE_PED_HATE, sharedBigRaceVars.rgFM_RACES[i])
		ENDIF
	 // #IF FEATURE_TARGET_RACES
	ENDREPEAT
ENDPROC

#IF IS_DEBUG_BUILD
CONST_INT GLOBAL_STAGE_INITIAL 			0
CONST_INT GLOBAL_STAGE_RESET   			1
CONST_INT GLOBAL_STAGE_CLIENT_BEFORE 	2
CONST_INT GLOBAL_STAGE_CLIENT_AFTER 	3
CONST_INT GLOBAL_STAGE_HOST_BEFORE 		4
CONST_INT GLOBAL_STAGE_HOST_AFTER		5
PROC PRINT_CORONA_VALUES(INT iStage)
	PRINTNL()
	SWITCH iStage
		CASE GLOBAL_STAGE_INITIAL 			PRINTLN("[1927180] START____GLOBAL_STAGE_INITIAL__________") BREAK
		CASE GLOBAL_STAGE_RESET   			PRINTLN("[1927180] START____GLOBAL_STAGE_RESET__________") BREAK
		CASE GLOBAL_STAGE_CLIENT_BEFORE 	PRINTLN("[1927180] START____GLOBAL_STAGE_CLIENT_BEFORE__________") BREAK
		CASE GLOBAL_STAGE_CLIENT_AFTER 		PRINTLN("[1927180] START____GLOBAL_STAGE_CLIENT_AFTER__________") BREAK
		CASE GLOBAL_STAGE_HOST_BEFORE 		PRINTLN("[1927180] START____GLOBAL_STAGE_HOST_BEFORE__________") BREAK
		CASE GLOBAL_STAGE_HOST_AFTER		PRINTLN("[1927180] START____GLOBAL_STAGE_HOST_AFTER__________") BREAK
	ENDSWITCH
	PRINTNL()
	PRINTLN("PRINT_CORONA_VALUES iLaps			 	= ", g_sRC_SB_CoronaOptions.iLaps)
	PRINTLN("PRINT_CORONA_VALUES iRaceType		 	= ", g_sRC_SB_CoronaOptions.iRaceType)
	PRINTLN("PRINT_CORONA_VALUES iRaceMode		 	= ", g_sRC_SB_CoronaOptions.iRaceMode)
	PRINTLN("PRINT_CORONA_VALUES iVehicleDamage	 	= ", g_sRC_SB_CoronaOptions.iVehicleDamage)
	PRINTLN("PRINT_CORONA_VALUES iTags			 	= ", g_sRC_SB_CoronaOptions.iTags)
	PRINTLN("PRINT_CORONA_VALUES iWeapons 		 	= ", g_sRC_SB_CoronaOptions.iWeapons)
	PRINTLN("PRINT_CORONA_VALUES iTraffic 		 	= ", g_sRC_SB_CoronaOptions.iTraffic)
	PRINTLN("PRINT_CORONA_VALUES iRaceClass 		= ", g_sRC_SB_CoronaOptions.iRaceClass)
	PRINTLN("PRINT_CORONA_VALUES iPedestrians  	 	= ", g_sRC_SB_CoronaOptions.iPedestrians)
	PRINTLN("PRINT_CORONA_VALUES iTimeOfDay 		= ", g_sRC_SB_CoronaOptions.iTimeOfDay)
	PRINTLN("PRINT_CORONA_VALUES iStartingGrid 	 	= ", g_sRC_SB_CoronaOptions.iStartingGrid)
	PRINTLN("PRINT_CORONA_VALUES iPolice	 		= ", g_sRC_SB_CoronaOptions.iPolice)
	PRINTLN("PRINT_CORONA_VALUES iRadio		 	 	= ", g_sRC_SB_CoronaOptions.iRadio)
	PRINTLN("PRINT_CORONA_VALUES iWeather			= ", g_sRC_SB_CoronaOptions.iWeather)
	PRINTLN("PRINT_CORONA_VALUES iSwapRoles		 	= ", g_sRC_SB_CoronaOptions.iSwapRoles)
	PRINTLN("PRINT_CORONA_VALUES iSplitComparison  	= ", g_sRC_SB_CoronaOptions.iSplitComparison)
	PRINTLN("PRINT_CORONA_VALUES iSlipStream		= ", g_sRC_SB_CoronaOptions.iSlipStream)
	PRINTNL()
	PRINTLN("___________________________________________________________________________________")
	PRINTNL()
ENDPROC
#ENDIF

//Resset the players PlayerBroadcastData
PROC RESET_PlayerBroadcastData()
	PlayerBroadcastData playerBBReset
	playerBD[PARTICIPANT_ID_TO_INT()] = playerBBReset
	PRINTLN("[RESET] RESET_PlayerBroadcastData ")
ENDPROC

//Reset all the ServerBroadcastData
PROC RESET_ServerBroadcastData()
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		ServerBroadcastData serverBDReset 
		serverBD = serverBDReset
		PRINTLN("[RESET] RESET_ServerBroadcastData ")
	ENDIF
ENDPROC

PROC RESET_GlobalServerBD_Race()
	GlobalServerBroadcastDataRaces GlobalServerBD_DM_RESET
	GlobalServerBD_Races = GlobalServerBD_DM_RESET
	PRINTLN("[RESET] RESET_GlobalServerBD_Race()")
ENDPROC

PROC INIT_OPEN_WHEEL_TYRE_HEALTH()
	INT iTire
	FOR iTire = 0 TO FMMC_OPEN_WHEEL_TIRES-1
		sharedBigRaceVars.fRetainTireHealth[iTire] = -1
		PRINTLN("INIT_OPEN_WHEEL_TYRE_HEALTH - init for sharedBigRaceVars.fRetainTireHealth[",iTire,"]")
	ENDFOR
ENDPROC

/// PURPOSE:
///    Runs on all clients at startup.  Used to initialize values and set/register variables and such
/// RETURNS:
///    FALSE if the scritp fails to receive an initial network broadcast.
FUNC BOOL PROCESS_PRE_GAME(MP_MISSION_DATA &fmmcMissionData)

//	#IF IS_DEBUG_BUILD
//	g_FMMC_STRUCT.sArenaInfo.iArena_Lighting = ARENA_LIGHTING_NIGHT
//	#ENDIF

	serverBD.sServerFMMC_EOM.bAllowedToVote = FALSE

	GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_RACE
	
	FMMC_PROCESS_PRE_GAME_COMMON(fmmcMissionData, serverBD.iPlayerMissionToLoad, serverBD.iMissionVariation)
	
//	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
//		PRINTLN("PROCESS_PRE_GAME, NETWORK_IS_HOST_OF_THIS_SCRIPT, bServerStartsRaces ") // added 15/08/14
//		g_sRC_SB_CoronaOptions.bServerStartsRaces = TRUE
//	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			PRINTLN("PROCESS_PRE_GAME, NETWORK_IS_HOST_OF_THIS_SCRIPT, bServerStartsRaces ") // added 15/08/14
		ENDIF
		PRINT_CORONA_VALUES(GLOBAL_STAGE_INITIAL)
	#ENDIF
	
	// #1 Everyone resets menu globals 
	RESET_GlobalServerBD_Race()
	RESET_PlayerBroadcastData()
	RESET_ServerBroadcastData()
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF SHOULD_JIP_SHOW_STAY_AS_SPECTATOR_SCREEN_BETWEEN_ROUNDS()
			SET_JIP_SHOW_STAY_AS_SPECTATOR_SCREEN_BETWEEN_ROUNDS(FALSE)
			PRINTLN("[LM][SPEC_SPEC][JIP] - PROCESS_PRE_GAME Hiding Menu. It's too late now.")
		ENDIF
		
		IF SHOULD_JIP_STAY_AS_SPECTATOR_BETWEEN_ROUNDS()
			PRINTLN("[LM][SPEC_SPEC][JIP] - PROCESS_PRE_GAME SHOULD_JIP_STAY_AS_SPECTATOR_BETWEEN_ROUNDS = TRUE")
		ELSE
			PRINTLN("[LM][SPEC_SPEC][JIP] - PROCESS_PRE_GAME SHOULD_JIP_STAY_AS_SPECTATOR_BETWEEN_ROUNDS = FALSE")
		ENDIF		
		IF GlobalplayerBD_FM[NATIVE_TO_INT(GET_PLAYER_INDEX())].sClientCoronaData.iTeamChosen = -1
		AND SHOULD_LOCAL_PLAYER_STAY_IN_ARENA_BOX_BETWEEN_ROUNDS()
		AND SHOULD_JIP_STAY_AS_SPECTATOR_BETWEEN_ROUNDS()
			PRINTLN("[LM][SPEC_SPEC][JIP] - PROCESS_PRE_GAME - GlobalplayerBD_FM[NATIVE_TO_INT(GET_PLAYER_INDEX())].sClientCoronaData.iTeamChosen = -1")
			SET_I_JOIN_MISSION_AS_SPECTATOR()
			SET_BIT(g_sTransitionSessionData.sMissionRoundData.iBitSet, ciROUNDS_BITSET_NEED_TO_BALANCE_MY_TEAM)
			PUSH_ME_TO_MOST_APPROPREATE_TEAM_ONROUNDS_JIP()
		ENDIF
		SET_JIP_STAYS_AS_SPECTATOR_BETWEEN_ROUNDS(FALSE) // reset
	ENDIF
	
	#IF IS_DEBUG_BUILD
		PRINT_CORONA_VALUES(GLOBAL_STAGE_RESET)
	#ENDIF
	
	// #2 Clients must register up front so that the initial data is consistent with the server
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		#IF IS_DEBUG_BUILD
			PRINT_CORONA_VALUES(GLOBAL_STAGE_CLIENT_BEFORE)
		#ENDIF
		GlobalServerBD_Races = g_sRC_SB_CoronaOptions
		#IF IS_DEBUG_BUILD
			PRINT_CORONA_VALUES(GLOBAL_STAGE_CLIENT_AFTER)
		#ENDIF
	ENDIF
	RESET_SERVER_NEXT_JOB_VARS()
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD), "serverBD")
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(g_sMC_serverBDEndJob, SIZE_OF(g_sMC_serverBDEndJob), "g_sMC_serverBDEndJob")
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD), "playerBD")
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(GlobalServerBD_Races, SIZE_OF(GlobalServerBD_Races), "GlobalServerBD_Races")
	
	// #3 Host needs to store the data after registration to ensure it gets broadcast to late joiners
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		#IF IS_DEBUG_BUILD
			PRINT_CORONA_VALUES(GLOBAL_STAGE_HOST_BEFORE)
		#ENDIF
		GlobalServerBD_Races = g_sRC_SB_CoronaOptions
		#IF IS_DEBUG_BUILD
			PRINT_CORONA_VALUES(GLOBAL_STAGE_HOST_AFTER)
		#ENDIF
	ENDIF
	
		IF SHOULD_SPECTATOR_BE_ROAMING_AT_START()
		PRINTLN("[RC[SPEC_SPEC][PROCESS_PRE_GAME] SHOULD_SPECTATOR_BE_ROAMING_AT_START - Pausing Renderphase to prevent Arena Interior Pop in.")
		TOGGLE_PAUSED_RENDERPHASES(FALSE)
	ENDIF	
	
	// KGM: Wait for the first network broadcast before moving on
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		RETURN FALSE
	ENDIF
	
	// This script will not be paused if another script calls PAUSE_GAME
	SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
//		serverBD.iRaceID = FMMC
		PLAYSTATS_CREATE_MATCH_HISTORY_ID_2(serverBD.iHashedMac, serverBD.iMatchHistoryId)
	ENDIF

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(PLAYER_PED_ID(),KNOCKOFFVEHICLE_DEFAULT)
	ENDIF
	SET_FM_MISSION_LAUNCHED_SUCESS(fmmcMissionData.mdID.idCreator, fmmcMissionData.mdID.idVariation, fmmcMissionData.iInstanceId)
	
	// 825058
	IF NOT DOES_SCENARIO_BLOCKING_AREA_EXISTS(<<-7000.0, -7000.0, -1000.0>>, <<7000.0, 7000.0, 1000.0>>)
		PRINTLN("ADD_SCENARIO_BLOCKING_AREA, races ")
		ADD_SCENARIO_BLOCKING_AREA(<<-7000.0, -7000.0, -1000.0>>, <<7000.0, 7000.0, 1000.0>>)
	ELSE
		PRINTLN("ADD_SCENARIO_BLOCKING_AREA, races - FAILED - already exists")
	ENDIF
	
	// 3357240
	WATER_REFLECTION_SET_SCRIPT_OBJECT_VISIBILITY(TRUE)
	
	MAKE_RIVAL_TEAMS_HATE_EACH_OTHER()
	SET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID(), sharedBigRaceVars.rgFM_RACES[PARTICIPANT_ID_TO_INT()])	//Put all players on default team to start with
	UNLOCK_AIRPORT_GATES()
	INT iRank = GET_HIGHEST_TRANSITION_PLAYER_RANK() 
	SET_CHECK_RANK_CREATE_PICKUP_GLOBALS(iRank)
	
	PRINTLN("PROCESS_PRE_GAME FM_race_controller ")
	
	#IF IS_DEBUG_BUILD
	PARTICIPANT_INDEX piHost = NETWORK_GET_HOST_OF_THIS_SCRIPT()
	PLAYER_INDEX playerHost = NETWORK_GET_PLAYER_INDEX(piHost)
	STRING sHost
	IF IS_NET_PLAYER_OK(playerHost)
		sHost = GET_PLAYER_NAME(playerHost) 
		PRINTLN("[NET_RACES][2342992][321] FM_race_controller, PROCESS_PRE_GAME, HOST_IS_CALLED ", sHost, " RACE = ", g_FMMC_STRUCT.tl63MissionName)
	ENDIF
	DEBUG_PRINT_PLAYERS()
	#ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciDISABLE_TRAINS_AND_TRAMS)
		SET_RANDOM_TRAINS(FALSE)
		DELETE_ALL_TRAINS()
	ENDIF
	
	CLEAR_WAS_A_SPECIAL_SPECTATOR_IN_THIS_ROUND()
	
	IF CONTENT_IS_USING_ARENA()		
		CLEANUP_ARENA_ANNOUNCER_VARIABLES(TRUE)
		
		INIT_UGC_ARENA_PREGAME(sharedBigRaceVars.sTrapInfo_Local)
		
		IF serverBD.iArena_Lighting > -1
		AND DID_I_JOIN_MISSION_AS_SPECTATOR()
			g_FMMC_STRUCT.sArenaInfo.iArena_Lighting = serverBD.iArena_Lighting
			PRINTLN("[ARENA] Updating lighting to match serverBD.iArena_Lighting which is ", serverBD.iArena_Lighting)
		ENDIF
		
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_Disable_Arena_Mode)
			PREVENT_LOCAL_PLAYER_FALLING_OUT_WHEN_DEAD()
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_CLEAN_UP_LEAVERS_FOR_MISSION)
	OR IS_OPEN_WHEEL_RACE()
		SET_PLAYER_LEAVE_PED_BEHIND(PLAYER_ID(), FALSE)
		NETWORK_DISABLE_LEAVE_REMOTE_PED_BEHIND(TRUE)
	ENDIF
	
	IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.vPostMissionPos[0])
		g_TransitionSessionNonResetVars.sPostMissionCleanupData.vPostMissionPos = g_FMMC_STRUCT.vPostMissionPos[0]
		PRINTLN("PROCESS_PRE_GAME - Setting g_TransitionSessionNonResetVars.sPostMissionCleanupData.vPostMissionPos to ", g_TransitionSessionNonResetVars.sPostMissionCleanupData.vPostMissionPos)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirty, ciOptionsBS30_EnablePublicRaceAntiGrief)
		sharedBigRaceVars.iAntiGriefTime = g_sMPTunables.iANTI_GRIEF_TIMER
		PRINTLN("PROCESS_PRE_GAME - Overriding anti grief time to ", sharedBigRaceVars.iAntiGriefTime," seconds")
	ELSE
		sharedBigRaceVars.iAntiGriefTime = g_FMMC_STRUCT.iAntiGriefDrainTimer
		PRINTLN("PROCESS_PRE_GAME - Setting anti grief time to ", sharedBigRaceVars.iAntiGriefTime," seconds")
	ENDIF
	
	#IF IS_DEBUG_BUILD
	sharedBigRaceVars.debugRaceVars.bOutPutCelebSpam = TRUE
	PRINTLN("PROCESS_PRE_GAME - Setting debugRaceVars.bOutPutCelebSpam to TRUE")
	#ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
/// Adds speed zones around props to stop traffic driving up / into them.
PROC ADD_PROP_SPEED_ZONES(OBJECT_INDEX &oiProp[], INT &iPropSpeedZones[] )

	IF serverBD.iTraffic != TRAFFIC_OFF
	
		INT i
		VECTOR vObjectCoords
		
		#IF IS_DEBUG_BUILD
		INT iDebugCount
		#ENDIF
		
		IF NOT IS_PLAYER_SPECTATOR(playerBD, PLAYER_ID(), PARTICIPANT_ID_TO_INT())
			IF g_FMMC_STRUCT_ENTITIES.iNumberOfProps > 0
				FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfProps-1
					IF DOES_ENTITY_EXIST(oiProp[i])
						IF IS_MODEL_STUNT_ROAD_PROP(GET_ENTITY_MODEL(oiProp[i]))
							IF NOT IS_PROP_ANY_PAD(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn)
								vObjectCoords = GET_ENTITY_COORDS(oiProp[i])
								iPropSpeedZones[i] = ADD_ROAD_NODE_SPEED_ZONE(vObjectCoords,6.0, 0.0, TRUE)
								PRINTLN("ADD_PROP_SPEED_ZONES, IS_MODEL_STUNT_PROP, TRUE i = ", i)
								
								#IF IS_DEBUG_BUILD
								iDebugCount++
								#ENDIF
							ENDIF
						ELSE
							PRINTLN("ADD_PROP_SPEED_ZONES, IS_MODEL_STUNT_PROP, FALSE i = ", i)
						ENDIF
					ENDIF
				ENDFOR
			ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
		PRINTLN("ADD_PROP_SPEED_ZONES, g_FMMC_STRUCT_ENTITIES.iNumberOfProps = ", g_FMMC_STRUCT_ENTITIES.iNumberOfProps, " iDebugCount = ", iDebugCount)
		#ENDIF
	
	ENDIF
ENDPROC

PROC RESET_CHECKPOINT_STRUCT()
	CHECKPOINTS_STRUCT tempCheckpointData
	sharedBigRaceVars.currentCheckPoint = tempCheckpointData
	sharedBigRaceVars.nextCheckPoint = tempCheckpointData
ENDPROC

PROC RESET_BIG_RACE_STRUCT()
	BIG_RACE_STRUCT tempBigRaceData
	sharedBigRaceVars = tempBigRaceData
ENDPROC

PROC RESET_THE_LEADERBOARD_STRUCT()
	THE_LEADERBOARD_STRUCT leaderboardBlank
	INT i
	REPEAT FMMC_MAX_NUM_RACERS i
		serverBD.leaderboard[i] = leaderboardBlank
	ENDREPEAT
ENDPROC

//Put players ELO into globals
PROC MAINTAIN_PLAYER_ELO_GLOBALS()
	INT iLoop 
	FOR iLoop = 0 TO (NUM_NETWORK_PLAYERS - 1)
		g_iPlayerELO[iLoop] = playerBD[iLoop].iCurrentELO
	ENDFOR
ENDPROC 

FUNC BOOL HAS_NEXT_RACE_LOADED()

	IF serverBD.iNextMission = -1
		//If the mission to load has not changed the move to the initilisation stage
		
		NET_PRINT("CONTROL_LOADING_NEXT_RACE, serverBD.iNextMission = -1") NET_NL()		
//		SET_CLIENT_GAME_STATE(playerBD, GAME_STATE_INI)
		playerBD[PARTICIPANT_ID_TO_INT()].iFinishTime = 0
//		HAS_CLIENT_CREATED_PICKUPS(sharedBigRaceVars)
		
		RETURN TRUE
	ELSE
	
		HIDE_HUD_AND_RADAR_THIS_FRAME()
		//Load all the data from the requested mission
		//IF FMMC_LOAD_MISSION_DATA(sFMMCdataFile, playerBD[PARTICIPANT_ID_TO_INT()].iDataFileBitSet, FMMC_ROCKSTAR_CREATOR_ID, #IF IS_DEBUG_BUILD serverBD.iNextMission, #ENDIF g_FMMC_ROCKSTAR_VERIFIED.tlOwner[serverBD.iNextMission], FMMC_TYPE_RACE, g_FMMC_ROCKSTAR_VERIFIED.tlName[serverBD.iNextMission])
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].tl23CurrentMissionOwner		= g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[serverBD.iNextMission].tlOwner
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].tl31CurrentMissionFileName	= g_FMMC_ROCKSTAR_VERIFIED.sMissionHeaderVars[serverBD.iNextMission].tlName
			PRINTLN("GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].tl23CurrentMissionOwner		=", GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].tl23CurrentMissionOwner)
			PRINTLN("GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].tl31CurrentMissionFileName	=", GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].tl31CurrentMissionFileName)
			SET_NEXT_FM_MISSION_LAUNCH_SUCESS(FMMC_ROCKSTAR_CREATOR_ID, serverBD.iNextMission, g_FMMC_STRUCT.vStartPos)
			SET_FM_MISSION_AS_JOINABLE()
			IF GlobalServerBD_Races.iRaceType > 1
				IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
					GlobalServerBD_Races.iRaceType	 = 1
				ENDIF
				g_mnMyRaceModel = GET_VEHICLE_MODEL_FROM_RACE_TYPE_AND_SELECTION(0, 0)
			ENDIF
			IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
				//Set up the server broadcast data to the loaded values
				GlobalServerBD_Races.iLaps 		 = g_FMMC_STRUCT.iNumLaps
				GlobalServerBD_Races.iRaceType   = g_FMMC_STRUCT.iRaceType
				GlobalServerBD_Races.iWeapons    = g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons 
				GlobalServerBD_Races.iTraffic    = g_FMMC_STRUCT.iTraffic 
			ENDIF
			sharedBigRaceVars.bLoadedMissionRatingDetails = FALSE
			//Move to the initilisation stage
			NET_PRINT("CONTROL_LOADING_NEXT_RACE, serverBD.iNextMission <> -1") NET_NL()
			NET_PRINT("serverBD.iNextMission = ")NET_PRINT_INT(serverBD.iNextMission)NET_NL()
//			SET_CLIENT_GAME_STATE(playerBD, GAME_STATE_INI)

//			HAS_CLIENT_CREATED_PICKUPS(sharedBigRaceVars)

			RETURN TRUE
	//	ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SET_MENU_OPTIONS_TO_DEFAULT()
	PRINTLN("SET_MENU_OPTIONS_TO_DEFAULT")
	PRINTLN("sharedBigRaceVars.sRaceMenuOptions.sLocalPlayerOptions.iSelection[", ciOPTION_CAR, "] = ", g_iMyRaceModelChoice)
	sharedBigRaceVars.sRaceMenuOptions.sLocalPlayerOptions.iSelection[ciOPTION_CAR] = g_iMyRaceModelChoice
ENDPROC

#IF IS_DEBUG_BUILD
PROC DEBUG_DISABLE_OPTIONS()
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_DisableAntiGrief")
		PRINTLN("DEBUG_DISABLE_OPTIONS - Disabling ANTI GRIEF")
		CLEAR_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyTwo, ciOptionsBS22_EnableRaceAntiGrief_Ghost)
		CLEAR_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyTwo, ciOptionsBS22_EnableRaceAntiGrief_HealthDrain)
		CLEAR_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySeven, ciOptionsBS27_EnableRaceAntiGrief_CheckpointGhost)
	ENDIF
ENDPROC
#ENDIF

//╔═════════════════════════════════════════════════════════════════════════════╗
//║			MAIN LOOP															║
//╚═════════════════════════════════════════════════════════════════════════════╝	

SCRIPT(MP_MISSION_DATA fmmcMissionData)

	PRINTLN("------------------------------------------------------------------------------")
	PRINTLN("--                                                                          --")
	PRINTLN("--                    * START FM_Race_Controller LAUNCHED!!!11              --")
	PRINTLN("--                                                                          --")
	PRINTLN("------------------------------------------------------------------------------")
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciFORCE_RACE_LOAD)
		REQUEST_LOAD_FMMC_MODELS()
	ENDIF
	// Carry out all the initial game starting duties. 
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF NOT PROCESS_PRE_GAME(fmmcMissionData)
			PRINTLN("[NET_RACES] NOT PROCESS_PRE_GAME, SCRIPT_CLEANUP_RACES ")
			SCRIPT_CLEANUP_RACES(serverBD, playerBD, sharedBigRaceVars, Placement, ciFMMC_END_OF_MISSION_STATUS_CANCELLED)
		ENDIF
	ENDIF
//	HAS_CLIENT_CREATED_PICKUPS(sharedBigRaceVars)
	
	//Reset the ELO globals
	CLEAN_PLAYER_ELO_GLOBALS()
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		SERVER_SETS_POINT_TO_POINT_RACES_AS_TO_ONE_LAP(serverBD)
	ENDIF
	
	// KGM 25/7/12: Load Tunable Script Variable values from the cloud for Freemode Races
//	Refresh_MP_Script_Tunables(TUNE_CONTEXT_FM_RACES)
	
	PRINTLN(" GET_PED_WEAPONS_MP, FM_Race_Controller")
	GET_PED_WEAPONS_MP(PLAYER_PED_ID(), sharedBigRaceVars.structWeaponInfo)
	
	#IF IS_DEBUG_BUILD
		CREATE_RACE_WIDGETS(serverBD, playerBD, sharedBigRaceVars)
//		CREATE_TEXT_STYLE_WIDGETS(Placement.aStyle)
	#ENDIF
	
	//Check to see if we need to load the prop animations
	CHECK_TO_LOAD_PROP_ANIMS()
	
	SETUP_TRANSFORM_RACE_CUSTOM_VEHICLE_DATA(sharedBigRaceVars)
	
	WHILE TRUE 

		#IF IS_DEBUG_BUILD
		IF g_b4357710
			g_IgnoreTeamGTARaceCoronaRules = TRUE
			g_b_IsRacePassenger = TRUE
		ENDIF
		
//		INT i
//		IF NOT DOES_ENTITY_EXIST(sharedBigRaceVars.sCelebrationData.ArenaProps[i])
//			CREATE_ARENA_PROP(serverBD.sCelebServer, sharedBigRaceVars.sCelebrationData.ArenaProps[i], ARENA_PROP_CREATION_COORDS(i), ARENA_PROP_MODEL(i), ARENA_PROP_HEADING(i))
//		ENDIF
		
		#ENDIF
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		SCRIPT_PROFILER_END_OF_FRAME()
		#ENDIF
		#ENDIF		
		
		// One wait to rule them all. This can be the ONLY wait from here on in.....
		MP_LOOP_WAIT_ZERO()
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		SCRIPT_PROFILER_START_OF_FRAME()
		#ENDIF
		#ENDIF	
		
		#IF IS_DEBUG_BUILD
		IF g_debugDisplayState = DEBUG_DISP_CLEANUP
			PRINTLN("[NET_RACES] DEBUG_DISP_CLEANUP, SCRIPT_CLEANUP_RACES ")
			SCRIPT_CLEANUP_RACES(serverBD, playerBD, sharedBigRaceVars, Placement, ciFMMC_END_OF_MISSION_STATUS_CANCELLED)
		ENDIF
		
		#ENDIF	
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("FM_Race_Controler.sc, DEBUG_DISP_CLEANUP ")
		#ENDIF
		#ENDIF
		
		// If we have a match end event, bail.
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			PRINTLN("[NET_RACES] SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE, SCRIPT_CLEANUP_RACES ")
			
			// url:bugstar:5452493
			IF CONTENT_IS_USING_ARENA()
			AND NOT DID_I_JOIN_MISSION_AS_SPECTATOR()
				HANDLE_END_OF_ARENA_EVENT_PLAYER_CAREER_REWARDS(0, IS_THIS_TEAM_RACE(serverBD), TRUE, DEFAULT, DEFAULT, DEFAULT, serverBD.iNumberOfTeams)
			ENDIF
			
			SCRIPT_CLEANUP_RACES(serverBD, playerBD, sharedBigRaceVars, Placement, ciFMMC_END_OF_MISSION_STATUS_CANCELLED)
		ENDIF
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("FM_Race_Controler.sc, SCRIPT_CLEANUP_RACES ")
		#ENDIF
		#ENDIF
		
		#IF IS_DEBUG_BUILD
		PROCESS_PAUSED_TIMERS(playerBD, sharedBigRaceVars)
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("PROCESS_PAUSED_TIMERS")
		#ENDIF
		#ENDIF
		
		IF IS_BIT_SET(serverBD.iServerBitSetTwo, SERV_BITSET_TWO_BLIMP_ACTIVE)
			DO_BLIMP_SIGNS(sharedBigRaceVars.sBlimpSign, HAS_RACE_ENDED(serverBD))
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
				ADD_SCRIPT_PROFILE_MARKER("DO_BLIMP_SIGNS") 
			#ENDIF
			#ENDIF
		ENDIF
		
		INT iSlot = NATIVE_TO_INT(PLAYER_ID())
		IF iSlot <> -1
			IF IS_BIT_SET(GlobalplayerBD_FM[iSlot].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bQuitJob) = FALSE
			OR IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID())
			OR TRANSITION_SESSIONS_QUIT_CURRENT_PLAYLIST()
				IF SHOULD_PLAYER_LEAVE_MP_MISSION(TRUE) 
					SET_FINISH_TIME_INVALID_IF_FAILED_TO_FINISH(serverBD, playerBD, sharedBigRaceVars)
					PRINTLN("[NET_RACES] SHOULD_PLAYER_LEAVE_MP_MISSION, SCRIPT_CLEANUP_RACES ")
					SCRIPT_CLEANUP_RACES(serverBD, playerBD, sharedBigRaceVars, Placement, ciFMMC_END_OF_MISSION_STATUS_CANCELLED)
				ENDIF
			ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("FM_Race_Controler.sc, SCRIPT_CLEANUP_RACES 2 ")
		#ENDIF
		#ENDIF
		
		// Deal with the debug.
		#IF IS_DEBUG_BUILD		
			UPDATE_RACE_WIDGETS(serverBD, playerBD, sharedBigRaceVars)
		#ENDIF
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("UPDATE_RACE_WIDGETS")
		#ENDIF
		#ENDIF
		
		// render server leader board
		#IF IS_DEBUG_BUILD
			IF (sharedBigRaceVars.debugRaceVars.bRenderServerLeaderBoard)
				DEBUG_RENDER_SERVER_LEADERBOARD(serverBD, sharedBigRaceVars, playerBD)
			ENDIF
		#ENDIF
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("DEBUG_RENDER_SERVER_LEADERBOARD")
		#ENDIF
		#ENDIF
		
		SPECTATOR_SET_TIME_WEATHER_PRE_RACE(serverBD, sharedBigRaceVars)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("SPECTATOR_SET_TIME_WEATHER_PRE_RACE")
		#ENDIF
		#ENDIF
		
		PROCESS_RACE_EVENTS(serverBD, playerBD, sharedBigRaceVars)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("PROCESS_RACE_EVENTS")
		#ENDIF
		#ENDIF
		
		CONTROL_RADAR_ZOOM(sharedBigRaceVars)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("CONTROL_RADAR_ZOOM")
		#ENDIF
		#ENDIF 
		
		SAVE_THUMB_VOTE(sharedBigRaceVars)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("SAVE_THUMB_VOTE")
		#ENDIF
		#ENDIF 
		
		IF NOT HAS_RACE_STARTED(serverBD)
			DISABLE_FIRST_PERSON_CAMS()
			PRINTLN("DISABLE_FIRST_PERSON_CAMS, NOT HAS_RACE_STARTED")
			IF serverBD.iNumRaceStarters = 1
				DISABLE_SCRIPT_HUD_THIS_FRAME(HUDPART_ALL_OVERHEADS)
			ENDIF
		ENDIF
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("DISABLE_FIRST_PERSON_CAMS") 
		#ENDIF
		#ENDIF

		MAKE_RACE_PROPS_AND_PICKUPS(serverBD, playerBD, sharedBigRaceVars)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("MAKE_RACE_PROPS_AND_PICKUPS") 
		#ENDIF
		#ENDIF
		
		IF CONTENT_IS_USING_ARENA()
			PROCESS_UGC_ARENA(sharedBigRaceVars.sArenaSoundsInfo, GET_CLIENT_GAME_STATE(playerBD, PARTICIPANT_ID_TO_INT()) <= GAME_STATE_RUNNING, serverBD.iArena_Lighting)
		ENDIF
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("PROCESS_ARENA_SOUNDS") 
		#ENDIF
		#ENDIF
	
		// 1687329 block trains
		BLOCK_TRAINS(serverBD)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("BLOCK_TRAINS")
		#ENDIF
		#ENDIF 
		
		
		MAINTAIN_RACE_EVENT_NETWORK_CONNECTION_TIMEOUTS(sharedBigRaceVars)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_RACE_EVENT_NETWORK_CONNECTION_TIMEOUTS")
		#ENDIF
		#ENDIF 
		
		BLOCK_SEAT_SHUFFLE_FROM_INPUT(serverBD)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("BLOCK_SEAT_SHUFFLE_FROM_INPUT")
		#ENDIF
		#ENDIF
		
		MAINTAIN_RACE_VEHICLE_WEAPONS(serverBD)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_VEHICLE_RACE_WEAPONS")
		#ENDIF
		#ENDIF
		
		// 1802067
		IF HAS_RACE_ENDED(serverBD)
			IF NOT IS_MISSION_OVER_AND_ON_LB_FOR_SCTV(PLAYER_ID())
				SET_MISSION_OVER_AND_ON_LB_FOR_SCTV()
			ENDIF
		ENDIF
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("SET_MISSION_OVER_AND_ON_LB_FOR_SCTV") 
		#ENDIF
		#ENDIF
		
		 // #IF FEATURE_SPECIAL_RACES
		g_bMissionClientGameStateRunning = FALSE
//╔═════════════════════════════════════════════════════════════════════════════╗
//║			CLIENT																║
//╚═════════════════════════════════════════════════════════════════════════════╝	
		SWITCH GET_CLIENT_GAME_STATE(playerBD, PARTICIPANT_ID_TO_INT())
						
			CASE GAME_STATE_INI
			
				playerBD[PARTICIPANT_ID_TO_INT()].iFinishTime = 0				
				HIDE_HUD_AND_RADAR_THIS_FRAME()
//				// Has to be here
//				SET_PREFERRED_OPTIONS_FOR_TEAM_MODES(serverBD, playerBD)

				IF GET_SERVER_GAME_STATE(serverBD) >= GAME_STATE_GRID	
//					IF CREATE_FMMC_PROPS_MP(sharedBigRaceVars.oiProps)
					IF LOAD_RACE_IPL(g_ArenaInterior, sharedBigRaceVars.interiorArena_VIPLounge)
						IF PLACE_STUNTJUMPS(sharedBigRaceVars)
							IF IS_NET_PLAYER_OK(PLAYER_ID())
								BOOL bAllLoaded
								bAllLoaded= TRUE								
								IF CONTENT_IS_USING_ARENA()									
									IF NOT LOAD_AND_INITIALIZE_ARENA_ANNOUNCER()
										PRINTLN("[RACE][LM-JT][ArenaAnnouncer][GAME_STATE_INI] - bAllLoaded = FALSE")
										bAllLoaded = FALSE
									ENDIF
								ENDIF
								IF bAllLoaded
									IF serverBD.iRaceVehClass = FMMC_VEHICLE_CLASS_CYCLES
										REMOVE_BOOST_PICKUPS(sharedBigRaceVars)
									ENDIF
									HOST_SET_ARENA_AUDIO_SCORE(serverBD)
									ADD_PROP_SPEED_ZONES(sharedBigRaceVars.oiProps,sharedBigRaceVars.iPropSpeedZone)
									REMOVE_ALL_PRIVATE_YACHTS_IPLS_NOW()
									BOAT_RACE_PREP(serverBD)
									CLIENT_CLEAR_WHOLE_MAP_AREA()
									ENABLE_EMERGENCY_SERVICES(FALSE)
									sharedBigRaceVars.iVehicleMode_Deluxo = -1
									sharedBigRaceVars.iVehicleMode_Stromberg = -1
									
									#IF IS_DEBUG_BUILD
									DEBUG_DISABLE_OPTIONS()
									#ENDIF
									
									INIT_OPEN_WHEEL_TYRE_HEALTH()
									
									g_sGb_Telemetry_data.sdata.m_match1 = serverBD.iMatchID1
									g_sGb_Telemetry_data.sdata.m_match2 = serverBD.iMatchID2
									
									PRINTLN("     ---------->	GET_SERVER_GAME_STATE(serverBD) > GAME_STATE_INI")
									
									SET_CLIENT_GAME_STATE(playerBD, GAME_STATE_RUNNING)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
//					ENDIF
				ENDIF
			BREAK
			
			// Main gameplay state.
			CASE GAME_STATE_RUNNING		
				g_bMissionClientGameStateRunning = TRUE							
				PROCESS_RACE_CLIENT(Placement, serverBD, playerBD, sharedBigRaceVars)
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE 
					ADD_SCRIPT_PROFILE_MARKER("PROCESS_RACE_CLIENT")
				#ENDIF
				#ENDIF
				
				// Look for the server say the mission has ended.
				IF GET_SERVER_GAME_STATE(serverBD) = GAME_STATE_END		
					PRINTLN("----------> server = GAME_STATE_END moving to  client GAME_STATE_LEAVE ", "Race") 

					SET_CLIENT_GAME_STATE(playerBD, GAME_STATE_LEAVE)	
				ENDIF
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE 
					ADD_SCRIPT_PROFILE_MARKER("Look for the server say the mission has ended.")
				#ENDIF
				#ENDIF
								
				// -----------------------------------	DEBUGGERY ----------------------------------------------------------------------
				#IF IS_DEBUG_BUILD
					UPDATE_DEBUG_SKIPS(serverBD, playerBD, sharedBigRaceVars)
				#ENDIF
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE 
					ADD_SCRIPT_PROFILE_MARKER("DEBUGGERY")
				#ENDIF
				#ENDIF
				// ---------------------------------------------------------------------------------------------------------------------
			BREAK
						
			CASE GAME_STATE_LEAVE	
				SET_CLIENT_GAME_STATE(playerBD, GAME_STATE_END)
			FALLTHRU

			CASE GAME_STATE_END		
				PRINTLN("[NET_RACES] GAME_STATE_END, SCRIPT_CLEANUP_RACES ")
				SCRIPT_CLEANUP_RACES(serverBD, playerBD, sharedBigRaceVars, Placement, ciFMMC_END_OF_MISSION_STATUS_PASSED)
			BREAK
			
			DEFAULT 
				#IF IS_DEBUG_BUILD
					SCRIPT_ASSERT("fm_RACES: Problem in SWITCH GET_CLIENT_MISSION_STATE(PARTICIPANT_ID_TO_INT())") 
				#ENDIF
			BREAK
		ENDSWITCH
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("game state processing")
		#ENDIF
		#ENDIF
		
//╔═════════════════════════════════════════════════════════════════════════════╗
//║		SERVER																	║
//╚═════════════════════════════════════════════════════════════════════════════╝		

		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		
			BOOL bCoronaReady = TRUE
			IF IS_PLAYER_IN_CORONA()
				bCoronaReady = IS_CORONA_READY_TO_START_WITH_JOB()
			ENDIF
		
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
			OPEN_SCRIPT_PROFILE_MARKER_GROUP("rc server stuff") 
			#ENDIF
			#ENDIF	
			
			SERVER_GIVES_LATE_JOINERS_GRID_POSITION(serverBD, playerBD)
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
				ADD_SCRIPT_PROFILE_MARKER("SERVER_GIVES_LATE_JOINERS_GRID_POSITION")
			#ENDIF
			#ENDIF
			
			START_RACE_TIMER(serverBD)
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
				ADD_SCRIPT_PROFILE_MARKER("START_RACE_TIMER")
			#ENDIF
			#ENDIF
			
			IF IS_BIT_SET(sharedBigRaceVars.iNonBDBitSet3, BS3_MADE_PROPS)
				SERVER_SET_PROP_PRESENT_FLAG(serverBD, sharedBigRaceVars)
			ENDIF
			
			IF bCoronaReady
				iLoopParticipant += 1			
				IF (iLoopParticipant = NETWORK_GET_MAX_NUM_PARTICIPANTS())
					iLoopParticipant = 0 	
				ENDIF
			
				SERVER_MAINTAIN_RACE_LEADERBOARD_FOR_PARTICIPANT(serverBD, serverBD.leaderboard, playerBD, sharedBigRaceVars, iLoopParticipant)		
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE 
					ADD_SCRIPT_PROFILE_MARKER("SERVER_MAINTAIN_RACE_LEADERBOARD")
				#ENDIF
				#ENDIF
											
				SERVER_SETS_RACE_END_BIT(serverBD, sharedBigRaceVars)
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE 
					ADD_SCRIPT_PROFILE_MARKER("SERVER_SETS_RACE_END_BIT")
				#ENDIF
				#ENDIF
				
				SERVER_SETS_BIT_WHEN_FIFTEEN_SECONDS_REMAIN(serverBD)
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE 
					ADD_SCRIPT_PROFILE_MARKER("SERVER_SETS_BIT_WHEN_FIFTEEN_SECONDS_REMAIN")
				#ENDIF
				#ENDIF
				
				SERVER_CHOOSE_ARENA_ANIMATIONS(serverBD.sCelebServer, GET_CELEB_TYPE(serverBD), sharedBigRaceVars.sCelebrationData.iNumPlayerFoundForWinnerScene)
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE 
					ADD_SCRIPT_PROFILE_MARKER("SERVER_CHOOSE_ARENA_ANIMATIONS")
				#ENDIF
				#ENDIF
			ENDIF
		
			SWITCH GET_SERVER_GAME_STATE(serverBD)
			
				CASE GAME_STATE_INI							
			
					IF HAVE_ALL_CORONA_PLAYERS_JOINED(sharedBigRaceVars.timeCoronaFailSafe)
						IF SHOULD_LONE_SPECTATOR_CLEAN_UP(sharedBigRaceVars.timeCoronaFailSafe)
						
							PRINTLN("[NET_RACES] SHOULD_LONE_SPECTATOR_CLEAN_UP, SCRIPT_CLEANUP_RACES ")
							SCRIPT_CLEANUP_RACES(serverBD, playerBD, sharedBigRaceVars, Placement, ciFMMC_END_OF_MISSION_STATUS_CANCELLED)
						ELSE
							INIT_SERVER_DATA(serverBD, playerBD)
							IF serverBD.iPopulationHandle = -1
								SERVER_SET_PED_AND_TRAFFIC_DENSITIES(serverBD.iPopulationHandle, GET_RACES_PED_DENSITY(), GET_TRAFFIC_DENSITY_FOR_RACES(serverBD))
							ELSE
								PRINTLN("[NET_RACES] PED_AND_TRAFFIC_DENSITIES Population densities already blocked handle: ", serverBD.iPopulationHandle)
							ENDIF
							SERVER_SET_NUMBER_OF_LAPS(serverBD)
							SERVER_SET_NUMBER_CHECKPOINTS(serverBD)
							SERVER_SET_NUMBER_PICKUPS(serverBD)
							SET_INVINCIBLE_START_OF_JOB(TRUE)
							RESERVE_NETWORK_MISSION_PEDS(NUM_RACE_PEDS_TO_RESERVE + g_FMMC_STRUCT_ENTITIES.iNumberOfUnitPeds) // Lamar in tutorial, SCTV heli pilot for races
							IF IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL() 
								REQUEST_WAYPOINT_RECORDING("multirace0_route3")
							ENDIF
							SERVER_SET_EXPLODE_BIT(serverBD)
							
								IF g_sRC_SB_CoronaOptions.bAggregate
									PRINTLN("[AGG_RACE] SERV_BITSET_TWO_AGGREGATE, SET")
									SET_BIT(serverBD.iServerBitSetTwo, SERV_BITSET_TWO_AGGREGATE)
								ENDIF
							
							SET_SERVER_GAME_STATE(serverBD, GAME_STATE_GRID)
						ENDIF
					ELSE
						PRINTLN("Failing on HAVE_ALL_CORONA_PLAYERS_JOINED")
					ENDIF
				BREAK
				
				CASE GAME_STATE_GRID
					IF HAVE_ALL_PLAYERS_BEEN_ROLES(serverBD,playerBD)
						IF HAS_STARTING_GRID_BEEN_GENERATED(serverBD, sharedSCLbd, sharedLBDPlayers, serverBD.iNumRaceStarters, playerBD)
							IF HAS_RACING_AI_BEEN_MADE(serverBD, sharedBigRaceVars)
								IF HAS_SERVER_ASSIGNED_LAMAR_FAKE_PARTICIPANT_ID(serverBD)
									RESET_THE_LEADERBOARD_STRUCT()
									CLIENT_CLEAR_WHOLE_MAP_AREA()
									REINIT_NET_TIMER(serverBD.raceJoinTimer)
									
									SET_SERVER_GAME_STATE(serverBD, GAME_STATE_COUNTDOWN)
								ELSE
									PRINTLN("stuck HAS_RACING_AI_BEEN_MADE, HAS_SERVER_ASSIGNED_LAMAR_FAKE_PARTICIPANT_ID")
								ENDIF
							ELSE
								PRINTLN("stuck HAS_RACING_AI_BEEN_MADE, GAME_STATE_INI")
							ENDIF
						ELSE
							PRINTLN("stuck HAS_STARTING_GRID_BEEN_GENERATED, GAME_STATE_INI")
						ENDIF
					ELSE
						PRINTLN("stuck HAVE_ALL_TEAMS_BEEN_ASSIGNED, GAME_STATE_INI")
					ENDIF
				BREAK
				
				CASE GAME_STATE_COUNTDOWN
					IF bCoronaReady
						IF LOAD_AND_CREATE_RACE_DYNOPROPS(serverBD)
							IF CREATE_ALL_UNITS_FOR_RACE(serverBD)
								IF HAS_SERVER_CHECKED_ALL_CLIENTS_READY_TO_DRAW_COUNTDOWN(serverBD)
									SERVER_STARTS_RACE_COUNTDOWN(serverBD)
									RESET_THE_LEADERBOARD_STRUCT()
									SET_MP_INT_PLAYER_STAT(GET_VEHICLE_CLASS_STAT(g_FMMC_STRUCT.iRaceType, GlobalServerBD_Races.iRaceClass), g_iMyRaceModelChoice)																

									SET_SERVER_GAME_STATE(serverBD, GAME_STATE_RUNNING)						
								ENDIF
							ELSE
								PRINTLN("Stuck HAS_RACING_AI_BEEN_MADE, CREATE_ALL_UNITS_FOR_RACE")
							ENDIF
						ELSE
							PRINTLN("Stuck HAS_RACING_AI_BEEN_MADE, LOAD_AND_CREATE_RACE_DYNOPROPS")
						ENDIF
						
						CHECK_THRUSTER_RACE_SETTING(serverBD)
						#IF IS_DEBUG_BUILD
						#IF SCRIPT_PROFILER_ACTIVE 
							ADD_SCRIPT_PROFILE_MARKER("CHECK_THRUSTER_RACE_SETTING")
						#ENDIF
						#ENDIF
						
						CHECK_DELUXO_RACE_SETTING(serverBD)
						#IF IS_DEBUG_BUILD
						#IF SCRIPT_PROFILER_ACTIVE 
							ADD_SCRIPT_PROFILE_MARKER("CHECK_DELUXO_RACE_SETTING")
						#ENDIF
						#ENDIF
						 // #IF FEATURE_TARGET_RACES
					
						SERVER_PROCESSING(serverBD, playerBD, sharedBigRaceVars)
						#IF IS_DEBUG_BUILD
						#IF SCRIPT_PROFILER_ACTIVE 
							ADD_SCRIPT_PROFILE_MARKER("SERVER_PROCESSING")
						#ENDIF
						#ENDIF
					ENDIF
				BREAK
				
				// Look for game end conditions.
				CASE GAME_STATE_RUNNING		
					
					SERVER_GRABS_AVERAGE_RANK_OF_RACE_PLAYERS(serverBD)
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE 
						ADD_SCRIPT_PROFILE_MARKER("SERVER_GRABS_AVERAGE_RANK_OF_RACE_PLAYERS")
					#ENDIF
					#ENDIF

					SERVER_PROCESSING(serverBD, playerBD, sharedBigRaceVars)
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE 
						ADD_SCRIPT_PROFILE_MARKER("SERVER_PROCESSING")
					#ENDIF
					#ENDIF
					
					SERVER_STORE_CELEB_WINNER_VEHICLE(serverBD, playerBD, sharedBigRaceVars)
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE 
						ADD_SCRIPT_PROFILE_MARKER("SERVER_STORE_CELEB_WINNER_VEHICLE")
					#ENDIF
					#ENDIF
					
					SERVER_SETS_LEADER(serverBD)
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE 
						ADD_SCRIPT_PROFILE_MARKER("SERVER_SETS_LEADER")
					#ENDIF
					#ENDIF
					
					SERVER_MAINTAIN_RACE_END_COUNTER(serverBD  #IF IS_DEBUG_BUILD , sharedBigRaceVars #ENDIF)
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE 
						ADD_SCRIPT_PROFILE_MARKER("SERVER_MAINTAIN_RACE_END_COUNTER")
					#ENDIF
					#ENDIF
					
					SERVER_CONTROLS_DRAG_AMOUNTS(serverBD)
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE 
						ADD_SCRIPT_PROFILE_MARKER("SERVER_CONTROLS_DRAG_AMOUNTS")
					#ENDIF
					#ENDIF
					
					//  Track winning team
					// -----------------------------------------------------------------------------------------------//
					SERVER_SORT_WINNING_TEAMS(serverBD  , playerBD  )
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE 
						ADD_SCRIPT_PROFILE_MARKER("SERVER_SORT_WINNING_TEAMS")
					#ENDIF
					#ENDIF
					
					SERVER_ASSIGN_WINNING_TEAMS(serverBD, playerBD)
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE 
						ADD_SCRIPT_PROFILE_MARKER("SERVER_ASSIGN_WINNING_TEAMS")
					#ENDIF
					#ENDIF
					
					SERVER_DOES_RESTART_STUFF(g_sMC_serverBDEndJob, serverBD, sharedBigRaceVars)
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE 
						ADD_SCRIPT_PROFILE_MARKER("SERVER_DOES_RESTART_STUFF")
					#ENDIF
					#ENDIF
					
					//MAINTAIN_AI_RACING(serverBD, sharedBigRaceVars)
					MAINTAIN_AI_CHECKPOINT_NUMBER(serverBD, playerBD)
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE 
						ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_AI_CHECKPOINT_NUMBER") 
					#ENDIF
					#ENDIF
					
					SERVER_MAINTAIN_AI_BOOST(serverBD)
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE 
						ADD_SCRIPT_PROFILE_MARKER("SERVER_MAINTAIN_AI_BOOST") 
					#ENDIF
					#ENDIF
					
					SERVER_MAINTAIN_PED_AND_TRAFFIC_DENSITIES(serverBD)
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE 
						ADD_SCRIPT_PROFILE_MARKER("SERVER_MAINTAIN_PED_AND_TRAFFIC_DENSITIES") 
					#ENDIF
					#ENDIF
					
				BREAK
				
				CASE GAME_STATE_END			
				
					SERVER_MAINTAIN_RACE_END_COUNTER(serverBD  #IF IS_DEBUG_BUILD , sharedBigRaceVars #ENDIF)
					CLEARUP_PLACED_PROP_PTFX()
					SERVER_DOES_RESTART_STUFF(g_sMC_serverBDEndJob, serverBD, sharedBigRaceVars)
				BREAK	
				
				DEFAULT 
					#IF IS_DEBUG_BUILD
						SCRIPT_ASSERT("DM: Problem in SWITCH GET_SERVER_MISSION_STATE()") 
					#ENDIF
				BREAK
			ENDSWITCH
			
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
			CLOSE_SCRIPT_PROFILE_MARKER_GROUP() 
			#ENDIF
			#ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
			iPrintNum = 0
		#ENDIF
		
		PROCESS_INSTANCED_CONTENT_FADE_SCREEN_OUT_FOR_PERIOD_OF_TIME()
		
		IF CONTENT_IS_USING_ARENA()
			PROCESS_TRAP_CAM_TRAP_BLIPS(sharedBigRaceVars.biTrapCamBlip)
			PROCESS_SPECIAL_SPECTATOR_MODES_FOR_INSTANCED_CONTENT()
			PROCESS_TIME_REMAINING_CACHE(ServerBD)
		ENDIF
		
		IF CONTENT_IS_USING_ARENA()
		AND IS_SKYSWOOP_AT_GROUND()
			PROCESS_ARENA_ANNOUNCER_EVERY_FRAME()
		ENDIF
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("PROCESS_SPECIAL_SPECTATOR_MODES_FOR_INSTANCED_CONTENT")
		#ENDIF
		#ENDIF
		
		MAINTAIN_PLAYER_ELO_GLOBALS()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_PLAYER_ELO_GLOBALS")
		#ENDIF
		#ENDIF
		
	ENDWHILE
ENDSCRIPT

