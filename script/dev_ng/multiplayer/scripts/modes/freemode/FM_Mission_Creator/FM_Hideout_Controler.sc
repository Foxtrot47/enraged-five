//////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        FM_Hideout_controler.sc																//
// Description: Controls the custom Hideout missions in Freemode.									//
// Written by:  Ryan Baker																			//
// Date: 12/09/2012																					//
//////////////////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"
//USING "net_lobby.sch"
USING "shared_hud_displays.sch"
USING "net_spawn.sch"
USING "net_objective_text.sch"
USING "freemode_header.sch"
USING "FMMC_HEADER.sch"
USING "screen_gang_on_mission.sch"
USING "leader_board_common.sch"
USING "net_spectator_cam.sch"
//USING "net_safe_crack.sch"
USING "net_ammo_drop.sch"
USING "net_gang_angry.sch"
USING "net_bounty.sch"
USING "commands_money.sch"
USING "net_xp_func.sch"
USING "net_hideout.sch"
USING "net_celebration_screen.sch"
USING "net_wait_zero.sch"

USING "freemode_events_header.sch"

USING "net_synced_ambient_pickups.sch"

#IF IS_DEBUG_BUILD
USING "profiler.sch"
#ENDIF

CONST_INT MAX_NUM_PEDS				6
CONST_INT CASH_PER_KILL				50
CONST_INT END_CASH_REWARD			250
CONST_INT END_XP_REWARD				500
//CONST_INT MAX_NUM_PICKUPS			2
//CONST_INT MAX_NUM_HEALTH_PICKUPS	2
//CONST_INT CASH_PACKAGE_AMOUNT		1000
//CONST_INT SAFE_CONTENTS_AMOUNT		10000
CONST_INT LEAVE_AREA_RANGE			100
CONST_INT LAST_PEDS_NUM				4
//CONST_INT TERRITORY_CONTROL_TIME	300000			//(5*60*1000)
CONST_FLOAT WARNING_PED_RANGE		5.0
CONST_INT WARNING_2_DELAY			15000
CONST_INT WARNING_ATTACK_DELAY		15000

// Game States
CONST_INT GAME_STATE_INI 							0
CONST_INT GAME_STATE_INI_SPAWN						1
CONST_INT GAME_STATE_RUNNING						2
CONST_INT GAME_STATE_WAITING_TO_LEAVE				3
CONST_INT GAME_STATE_LEAVE							4
CONST_INT GAME_STATE_END							5

ENUM HIDEOUT_STAGE
	HIDEOUT_STAGE_JIP_IN = 0,
	HIDEOUT_STAGE_SETUP,
	HIDEOUT_STAGE_WARNING,
	HIDEOUT_STAGE_PRIMARY,
	HIDEOUT_STAGE_SECONDARY,
	HIDEOUT_STAGE_REWARDS_AND_FEEDACK
ENDENUM

ENUM HIDEOUT_VARIATION
	HIDEOUT_KILL_PEDS	// = 0,
	//HIDEOUT_TERRITORY_CONTROL,
	//HIDEOUT_PACKAGE_STEAL,
	//HIDEOUT_RANDOM
	//HIDEOUT_DESTROY_VEHICLE,
	//HIDEOUT_VEHICLE_STEAL,
	//HIDEOUT_ESCORT_PED
ENDENUM

ENUM PED_STATE
	ePED_IDLE,
	ePED_FIGHTING,
	ePED_SPAWNING,
	ePED_DEAD
ENDENUM

STRUCT PED_DATA
	PED_STATE ePedState = ePED_IDLE	//ePED_FIGHTING
	INT PedRespawnTimeLimit
	BOOL bRespawnInInterior
	BOOL bInitialAttackDone
ENDSTRUCT

//ENUM PICKUP_CONTENTS_TYPE
//	PU_CONTENTS_CASH,
//	PU_CONTENTS_WEAPON,
//	PU_CONTENTS_RANDOM,
//	PU_CONTENTS_XP
//ENDENUM
//
//STRUCT PICKUP_DATA
//	NETWORK_INDEX NetID
//	MODEL_NAMES Model = PROP_DRUG_PACKAGE
//	PICKUP_CONTENTS_TYPE iContentsType = PU_CONTENTS_RANDOM
//	INT iContentsAmount = CASH_PACKAGE_AMOUNT
//	WEAPON_TYPE iContentsWeapon = WEAPONTYPE_COMBATPISTOL
//	INT iParticpantGotPickUp = -1
//	INT iParticpantDeliveredPackage = -1
//ENDSTRUCT

//STRUCT SAFE_DATA
//	NETWORK_INDEX NetID
//	MODEL_NAMES Model = V_ILEV_GANGSAFE	//PROP_LD_INT_SAFE_01
//	INT iContentsAmount = SAFE_CONTENTS_AMOUNT
//	NETWORK_INDEX niDoor
//	MODEL_NAMES DoorModel = V_ILEV_GANGSAFEDOOR
//	VECTOR vSafeDoor
//	FLOAT fSafeDoorHeading
//ENDSTRUCT
//
//STRUCT VEHICLE_DATA
//	NETWORK_INDEX NetID
//	MODEL_NAMES Model = ZTYPE
//	VECTOR vCoord
//	FLOAT fHeading
//ENDSTRUCT

//BOOL bUseCrates


ENUM HIDEOUT_GANGS_ENUM
	HG_VAGOS,
	HG_LOST,
	HG_FAMILY,
	HG_BALLA,
	HG_KOREAN,
	HG_HILLBILLY,
	HG_SALVA,
	HG_ALBANIAN,
	HG_PROFESSIONAL
ENDENUM

STRUCT ANGLED_AREA_DATA
	VECTOR vMin
	VECTOR vMax
	FLOAT fWidth
ENDSTRUCT

CONST_INT biS_SetupFirstPedForDialogue		0
CONST_INT biS_WarningLine1Done				1
CONST_INT biS_WarningLine2Done				2
CONST_INT biS_AttackLineDone				3
CONST_INT biS_MoveToFightStage				4
CONST_INT biS_AllGangPedsDead				5
//CONST_INT biS_NoPackagesExist				6
CONST_INT biS_MissionNotJoinable			7
//CONST_INT biS_SafeUnlocked					8
CONST_INT biS_FirstPedTaskGiven				9
CONST_INT biS_SetAMGAPickupData				10
CONST_INT biS_BlockingAreaDone				11

// *********************************** \\
//				SERVER BD			   \\
// *********************************** \\

STRUCT ServerBroadcastData
	
	INT iServerGameState = GAME_STATE_INI
	SCRIPT_TIMER TerminationTimer
	
	INT iServerBitSet
	
	HIDEOUT_STAGE eStage = HIDEOUT_STAGE_WARNING	//HIDEOUT_STAGE_JIP_IN
	
	FMMC_SERVER_DATA_STRUCT sFMMC_SBD
	
	PED_DATA PedData[MAX_NUM_PEDS]
	//HIDEOUT_GANGS_ENUM ePedGang = HG_PROFESSIONAL
	MODEL_NAMES PedGangModel = MP_G_M_Pros_01
	
	VECTOR vHideoutBlipLocation
	
	VECTOR vHideoutCentre
	FLOAT fHideoutRadius
		
	INT iTotalKills
	
	INT iKillGoal = 15
	INT iPedsSpawned = MAX_NUM_PEDS	//Already Create the Max Num Peds at the start
	INT iNumAlivePeds
	
	//PICKUP_DATA PickUpData[MAX_NUM_PICKUPS]
	//SAFE_DATA SafeData
	//VEHICLE_DATA VehicleData
	
	NETWORK_INDEX niVehicle[MAX_NUM_VEH]
	MODEL_NAMES VehicleModel[MAX_NUM_VEH]
	
	SCRIPT_TIMER iStartTime
	INT iTimeTaken
	
	INT iNumParticpants = 1
	INT iHighestNumParticpants = 1
	INT iDifficultyFromPlayerStats
	INT iAverageRank
	INT iAccuracy = 20
	
	INT iWarningPed = -1
	
	HIDEOUT_VARIATION eHideoutVariation = HIDEOUT_KILL_PEDS	//HIDEOUT_RANDOM
	//Territory Control
	//SCRIPT_TIMER TerritoryControlTimer
	//SCRIPT_TIMER TimeSinceLastUpdateTimer
	//INT iTakeControlTime = TERRITORY_CONTROL_TIME
	INT iNumParticpantsInArea = 1
	
	INT iPlayerKillPoints[NUM_NETWORK_PLAYERS]
	INT iTotalPlayerKillPoints
	
	INT iPlayerDeaths[NUM_NETWORK_PLAYERS]
	
	//VECTOR vPackageDropOff
	//INT iStealPackageGang
	
	INT iPlayStatsID[2]
	
	INT iGangID = -1
	
	INT iReservedVehicles = MAX_NUM_VEH
	INT iReservedPeds = 6
	INT iReservedObjects = MAX_NUM_DESTRUCTIBLE_CRATES	//(3 + MAX_NUM_DESTRUCTIBLE_CRATES)
	
	INT iVehiclesCreatedBitSet
	
	INT iPedTrackedPoints[MAX_NUM_PEDS]
	INT iVehicleTrackedPoints[MAX_NUM_VEH]
	
	PLAYER_INDEX PlayerWithMostKills
	
	DESTRUCTIBLE_CRATE_DATA DCrateData[MAX_NUM_DESTRUCTIBLE_CRATES]
	
	GANG_ATTACK_PICKUPS_DATA GAPickupData
	
	ANGLED_AREA_DATA MainArea
	ANGLED_AREA_DATA AiSpawnArea
	ANGLED_AREA_DATA PlayerSpawnArea
	ANGLED_AREA_DATA NearlyLeaveArea
	ANGLED_AREA_DATA LeaveArea
ENDSTRUCT
ServerBroadcastData serverBD


// *********************************** \\
//				PLAYER BD			   \\
// *********************************** \\
//CONST_INT biP_CollectedPickUp1	0
//CONST_INT biP_CollectedPickUp2	1
CONST_INT biP_WarningLine1Done	2
CONST_INT biP_WarningLine2Done	3
CONST_INT biP_AttackLineDone	4
CONST_INT biP_HasPackage		5
CONST_INT biP_FirstPedTaskGiven	6
//CONST_INT biP_DeliveredPackage1	6
//CONST_INT biP_DeliveredPackage2	7
//CONST_INT biP_HasUnlockedSafe	8
//CONST_INT biP_InSafeUnlockMinigame	9

STRUCT PlayerBroadcastData
	
	INT iGameState = GAME_STATE_INI
	
	INT iPlayerBitSet
	
	HIDEOUT_STAGE eStage = HIDEOUT_STAGE_JIP_IN
	
	INT iDeaths = 0
	INT iKills = 0
	INT iHeadShots = 0
	INT iPointBlanks = 0
	INT iTotalKillPoints = 0
	
	INT iWarningPed = -1
	
	#IF IS_DEBUG_BUILD
		INT iDebugPassFail = 0
		INT iDebugSkip = 0
	#ENDIF
ENDSTRUCT
PlayerBroadcastData playerBD[NUM_NETWORK_PLAYERS]


// *********************************** \\
//			LOCAL VARIABLES			   \\
// *********************************** \\

INT iBoolsBitSet
CONST_INT biInitialGodText				0
CONST_INT biRewardDone					1
CONST_INT biSecondaryGodText			2
CONST_INT biEndScreenDone				3
//CONST_INT biPickUpDone1					4
//CONST_INT biPickUpDone2					5
//CONST_INT biBlockingAreaDone			6
//CONST_INT biAnyPackageDropped			7
//CONST_INT biPackageDeliveryRewardDone1	8
//CONST_INT biPackageDeliveryRewardDone2	9
CONST_INT biAlternativeGodText			10
//CONST_INT biSomeoneElseIsCrackingSafe	11
//CONST_INT biToldToProtectCracker		12
CONST_INT biCleanUpUi					13
CONST_INT biStartedPlayStats			14
CONST_INT biDeathTrackingDone			15
CONST_INT biInitialGangAngryCheckDone	16
CONST_INT biBigMessageDone				17
CONST_INT biTimesPlayedIncremented		18
CONST_INT biTriggerStartBigMessage		19
CONST_INT biLaunchedAMGAPickups			20

INT iPedTrackingBitSet

INT iStaggeredPedCountBrain = 0
INT iStaggeredPedCount = 0
INT isearchingforPed = -1
SCRIPT_TIMER PedRespawnTimer[MAX_NUM_PEDS]
//BLIP_INDEX PedBlip[MAX_NUM_PEDS]
AI_BLIP_STRUCT PedBlipData[MAX_NUM_PEDS]

OBJECT_INDEX oiProp[GA_MAX_NUM_PROPS]

//BLIP_INDEX biPickup[MAX_NUM_PICKUPS]
//BLIP_INDEX biDropOff
//BLIP_INDEX biHideoutArea
//BLIP_INDEX SafeBlip

//PICKUP_INDEX piHealth[MAX_NUM_HEALTH_PICKUPS]
//BLIP_INDEX biPickupBlip[MAX_NUM_HEALTH_PICKUPS]
//INT iStaggeredHealthPickUpBlipCount

INT iServerStaggeredPedCount

SCRIPT_TIMER iWarningHelpTimer
SCRIPT_TIMER iBlockedHelpTimer
INT iWarningHelpTimerDelay = -1

//SCRIPT_TIMER tdNetTimer  //  The network timer

SCENARIO_BLOCKING_INDEX ScenarioBlockingArea

//structPedsForConversation sSpeech
SCRIPT_TIMER iSpeechTimer
SCRIPT_TIMER iWarning2Timer
SCRIPT_TIMER iWarningAttackTimer
SCRIPT_TIMER iTurnTimer[MAX_NUM_PEDS]

SCRIPT_TIMER iMissionPassedTimer
CONST_INT MISSION_PASSED_DELAY	1000

INT iNumParticipantsStored

//INT iKillStreak
//INT iKillStreakMultiplier = 1

INT iInitialWarpStage = 0

//SAFE_CRACK_STRUCT SafeCrackData
//BOOL bSafeCrackDone
//INT iLocalPlayerCrackingSafe = -1


//** END SCREEN DATA **//
CONST_INT MAX_NUM_END_SCREEN_RESULTS	4

//BitSet
CONST_INT biTrackedPedKill		0

//Struct
STRUCT END_SCREEN_DATA_STRUCT
//	TEXT_STYLE EndScreenTextStyle
//	TEXT_PLACEMENT EndScreenTitle[MAX_NUM_END_SCREEN_RESULTS]
//	TEXT_PLACEMENT EndScreenResult[MAX_NUM_END_SCREEN_RESULTS]
//	RECT EndScreenBackground
	INT iTrackKillsBitSet
	INT iTrackedKills
	INT iHeadshots
	INT iPointBlankKills
ENDSTRUCT
END_SCREEN_DATA_STRUCT EndScreenData

//Celebration screen.
CELEBRATION_SCREEN_DATA sCelebrationData
CAMERA_INDEX camEndScreen

INT iNavmeshBlock

#IF IS_DEBUG_BUILD
BOOL bHostEndMissionNow
BOOL bMakePedsDefensive
#ENDIF

#IF IS_DEBUG_BUILD
FUNC STRING GET_HIDEOUT_STAGE_NAME(HIDEOUT_STAGE eStage)

	SWITCH eStage
		CASE HIDEOUT_STAGE_JIP_IN 				RETURN "JIP_IN"
		CASE HIDEOUT_STAGE_SETUP 				RETURN "SETUP_MISSION"
		CASE HIDEOUT_STAGE_WARNING 				RETURN "WARNING"
		CASE HIDEOUT_STAGE_PRIMARY 				RETURN "MAIN"
		CASE HIDEOUT_STAGE_SECONDARY 			RETURN "LEAVE AREA"
		CASE HIDEOUT_STAGE_REWARDS_AND_FEEDACK 	RETURN "REWARDS_AND_FEEDACK"
	ENDSWITCH
	
	RETURN "NOT_IN_SWITCH"
ENDFUNC
#ENDIF

/// PURPOSE:
///    Helper function to get the servers game/mission state
FUNC INT GET_SERVER_GAME_STATE()
	RETURN serverBD.iServerGameState
ENDFUNC	

/// PURPOSE:
///    Sets the server game state.
/// PARAMS:
///    iState - state to set to.
PROC SET_SERVER_GAME_STATE(INT iState)
	IF serverBD.iServerGameState != iState
		serverBD.iServerGameState = iState
		NET_PRINT("    ----->     HIDEOUT - iServerGameState = ")NET_PRINT_INT(iState)NET_NL()
	ENDIF
ENDPROC

/// PURPOSE:
///    Gets the current value of the server stored Hideout stage.
/// RETURNS:
///    Mission stage.
FUNC HIDEOUT_STAGE GET_SERVER_HIDEOUT_STAGE()
	RETURN serverBD.eStage
ENDFUNC

/// PURPOSE:
///    Sets the value of the server stored Hideout stage.
///    Server only command.
/// PARAMS:
///    eMissionStage - value to set stage to.
PROC SET_SERVER_HIDEOUT_STAGE(HIDEOUT_STAGE eStage)
	#IF IS_DEBUG_BUILD
		IF eStage != GET_SERVER_HIDEOUT_STAGE()
			NET_PRINT("    ----->     HIDEOUT - SERVER: setting server Hideout stage to ")NET_PRINT(GET_HIDEOUT_STAGE_NAME(eStage))NET_NL()
		ENDIF
	#ENDIF
	serverBD.eStage = eStage
ENDPROC

/// PURPOSE:
///    Helper function to get a clients game state
FUNC INT GET_CLIENT_GAME_STATE(INT iParticipant)
	RETURN playerBD[iParticipant].iGameState
ENDFUNC

/// PURPOSE:
///    Sets the local participant game state.
/// PARAMS:
///    iState - state to set to.
PROC SET_CLIENT_GAME_STATE(INT iState)
	IF playerBD[PARTICIPANT_ID_TO_INT()].iGameState != iState
		playerBD[PARTICIPANT_ID_TO_INT()].iGameState = iState
		NET_PRINT("    ----->     HIDEOUT - CLIENT - iGameState = ")NET_PRINT_INT(iState)NET_NL()
	ENDIF
ENDPROC

/// PURPOSE:
///    Getas the local Hideout stage of participant (stored in player BD).
/// PARAMS:
///    participant - participant to get Hideout stage for.
/// RETURNS:
///    Mission stage.
FUNC HIDEOUT_STAGE GET_CLIENT_HIDEOUT_STAGE(PARTICIPANT_INDEX participant)
	RETURN playerBD[NATIVE_TO_INT(participant)].eStage
ENDFUNC

/// PURPOSE:
///   	Sets the local Hideout stage of participant (in player BD).
/// PARAMS:
///    eMissionStage - Hideout stage to set to.
///    participant - participant to set stage for.
PROC SET_CLIENT_HIDEOUT_STAGE(HIDEOUT_STAGE eStage)
	//#IF IS_DEBUG_BUILD
	//	IF eStage != GET_CLIENT_HIDEOUT_STAGE()
			NET_PRINT("    ----->     HIDEOUT - CLIENT: setting client Hideout stage to ")NET_PRINT(GET_HIDEOUT_STAGE_NAME(eStage))NET_NL()
	//	ENDIF
	//#ENDIF
	playerBD[PARTICIPANT_ID_TO_INT()].eStage = eStage
ENDPROC

#IF IS_DEBUG_BUILD
PROC DEBUG_SKIPS()
	
	//Skip to end of stage
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
	AND IS_NET_PLAYER_OK(PLAYER_ID())
		NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - J-SKIP  <----  ") NET_NL()
		
		SWITCH GET_CLIENT_HIDEOUT_STAGE(PARTICIPANT_ID())
			CASE HIDEOUT_STAGE_WARNING
				playerBD[PARTICIPANT_ID_TO_INT()].iDebugSkip = 1
				NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - J-SKIP - SKIP WARNING STAGE ") NET_NL()
			BREAK
			
			CASE HIDEOUT_STAGE_PRIMARY
				SWITCH serverBD.eHideoutVariation
					CASE HIDEOUT_KILL_PEDS
						playerBD[PARTICIPANT_ID_TO_INT()].iDebugPassFail = 1
						NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - J-SKIP - OPEN SAFE - FORCE PASS ") NET_NL()
							
						//IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InSafeUnlockMinigame)
						//	playerBD[PARTICIPANT_ID_TO_INT()].iDebugSkip = 2
						//	NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - J-SKIP - SKIP KILL PEDS STAGE ") NET_NL()
						/*ELSE
							CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InSafeUnlockMinigame)
							SET_BIT(SafeCrackData.iBitSet, SC_BS_HAS_PLAYER_PASSED_SAFE_CRACK)
							PASSED_SAFE_CRACK_ANIM()
							SET_PLAYER_HAS_PASSED_SAFE_CRACK(SafeCrackData)
							CLEAR_HELP()
							CLEAR_BIT(SafeCrackData.iBitSet, SC_BS_IS_PLAYER_SAFE_CRACKING)
							TRIGGER_MUSIC_EVENT("GA_KILL_LEAVE")
						ENDIF*/
					BREAK
					
					/*CASE HIDEOUT_TERRITORY_CONTROL
						playerBD[PARTICIPANT_ID_TO_INT()].iTotalKillPoints += 50
						NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - J-SKIP - ADD 5 HEADSHOTS TO CONTROL ") NET_NL()
					BREAK*/
			
					/*CASE HIDEOUT_PACKAGE_STEAL
						IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InSafeUnlockMinigame)
							IF serverBD.PickUpData[0].iParticpantGotPickUp = -1
								IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.PickUpData[0].NetID)
									J_SKIP_MP(GET_ENTITY_COORDS(NET_TO_OBJ(serverBD.PickUpData[0].NetID)), <<2, 2, 3>>, FALSE, FALSE)
									NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - J-SKIP - WARP TO PACKAGE ") NET_PRINT_INT(0) NET_NL()
								ENDIF
							ENDIF
						ELSE
							CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InSafeUnlockMinigame)
							SET_BIT(SafeCrackData.iBitSet, SC_BS_HAS_PLAYER_PASSED_SAFE_CRACK)
							PASSED_SAFE_CRACK_ANIM()
							SET_PLAYER_HAS_PASSED_SAFE_CRACK(SafeCrackData)
							CLEAR_HELP()
							CLEAR_BIT(SafeCrackData.iBitSet, SC_BS_IS_PLAYER_SAFE_CRACKING)
							TRIGGER_MUSIC_EVENT("GA_KILL_LEAVE")
						ENDIF
					BREAK*/
				ENDSWITCH
			BREAK
			
			CASE HIDEOUT_STAGE_SECONDARY
				SWITCH serverBD.eHideoutVariation
					CASE HIDEOUT_KILL_PEDS
						//IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InSafeUnlockMinigame)
							playerBD[PARTICIPANT_ID_TO_INT()].iDebugPassFail = 1
							NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - J-SKIP - OPEN SAFE - FORCE PASS ") NET_NL()
						/*ELSE
							CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InSafeUnlockMinigame)
							SET_BIT(SafeCrackData.iBitSet, SC_BS_HAS_PLAYER_PASSED_SAFE_CRACK)
							PASSED_SAFE_CRACK_ANIM()
							SET_PLAYER_HAS_PASSED_SAFE_CRACK(SafeCrackData)
							CLEAR_HELP()
							CLEAR_BIT(SafeCrackData.iBitSet, SC_BS_IS_PLAYER_SAFE_CRACKING)
							TRIGGER_MUSIC_EVENT("GA_KILL_LEAVE")
						ENDIF*/
					BREAK
					
					/*CASE HIDEOUT_TERRITORY_CONTROL
						playerBD[PARTICIPANT_ID_TO_INT()].iDebugPassFail = 1
						NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - J-SKIP - KILL REMAINING - FORCE PASS ") NET_NL()
					BREAK*/
			
					/*CASE HIDEOUT_PACKAGE_STEAL
						J_SKIP_MP(serverBD.vPackageDropOff, <<1.5, 1.5, 4.0>>, FALSE, FALSE)
						NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - J-SKIP - WARP TO DROP OFF ") NET_NL()
					BREAK*/
				ENDSWITCH
			BREAK
		ENDSWITCH
		
	ENDIF
		
	//Force Pass
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
		playerBD[PARTICIPANT_ID_TO_INT()].iDebugPassFail = 1
		NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - J-SKIP - FORCE PASS ") NET_NL()
	ENDIF
	//Force Fail
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
		playerBD[PARTICIPANT_ID_TO_INT()].iDebugPassFail = 2
		NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - J-SKIP - FORCE FAIL ") NET_NL()
	ENDIF
ENDPROC
#ENDIF

PROC INITIALISE_TRACKED_POINTS()

	INT k

	REPEAT MAX_NUM_PEDS k
		serverBD.iPedTrackedPoints[k] = -1
	ENDREPEAT
	
	REPEAT MAX_NUM_VEH k
		serverBD.iVehicleTrackedPoints[k] = -1
	ENDREPEAT

ENDPROC

PROC CREATE_TRACKED_POINT_FOR_COORD(INT &iTrackedPoint, VECTOR vPosition, FLOAT fRadius)

	IF ( iTrackedPoint = -1  )
		iTrackedPoint = CREATE_TRACKED_POINT()
		SET_TRACKED_POINT_INFO(iTrackedPoint, vPosition, fRadius)
		NET_PRINT("  ---->  HIDEOUT - Created tracked point with id: ") NET_PRINT_INT(iTrackedPoint) NET_PRINT(" for coord ") NET_PRINT_VECTOR(vPosition) NET_PRINT(" and radius ") NET_PRINT_FLOAT(fRadius) NET_NL()
	ENDIF

ENDPROC

PROC CLEANUP_TRACKED_POINT(INT &iTrackedPoint)

	IF ( iTrackedPoint != -1  )
		DESTROY_TRACKED_POINT(iTrackedPoint)
		NET_PRINT("  ---->  HIDEOUT - Destroyed tracked point with id: ") NET_PRINT_INT(iTrackedPoint) NET_NL()
		iTrackedPoint = -1
	ENDIF

ENDPROC

FUNC BOOL DOES_TRACKED_POINT_EXIST(INT iTrackedPoint)

	RETURN iTrackedPoint != -1
	
ENDFUNC

PROC CREATE_TRACKED_POINTS()

	INT k

	REPEAT MAX_NUM_PEDS k
		IF NOT DOES_TRACKED_POINT_EXIST(serverBD.iPedTrackedPoints[k])
			CREATE_TRACKED_POINT_FOR_COORD(serverBD.iPedTrackedPoints[k], g_FMMC_STRUCT_ENTITIES.sPlacedPed[k].vPos, 1.5)
		ENDIF
	ENDREPEAT
	
	REPEAT MAX_NUM_VEH k
		IF k < serverBD.iReservedVehicles
			IF NOT DOES_TRACKED_POINT_EXIST(serverBD.iVehicleTrackedPoints[k])
				CREATE_TRACKED_POINT_FOR_COORD(serverBD.iVehicleTrackedPoints[k], g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[k].vPos, 2.0)
			ENDIF
		ENDIF
	ENDREPEAT

ENDPROC

PROC CLEANUP_TRACKED_POINTS()

	INT k

	REPEAT MAX_NUM_PEDS k
		CLEANUP_TRACKED_POINT(serverBD.iPedTrackedPoints[k])
	ENDREPEAT
	
	REPEAT MAX_NUM_VEH k
		CLEANUP_TRACKED_POINT(serverBD.iVehicleTrackedPoints[k])
	ENDREPEAT

ENDPROC



//PURPOSE: (PLACEHOLDER - UNTIL WE CAN DOWNLOAD THE ANGLED AREA) Checks if the a Gang Attack should launch based on an angled area 
PROC GET_DESTRUCTIBLE_CRATE_LOCATION()
	
	INT i
	REPEAT MAX_NUM_DESTRUCTIBLE_CRATES i
		serverBD.DCrateData[i].vCoord 				=	g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].vPos
		serverBD.DCrateData[i].fHeading				=	g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].fHead
		serverBD.DCrateData[i].Model				=	g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].mn
		//Check we aren't using an old model
		IF serverBD.DCrateData[i].Model = Prop_Box_Wood02A
		//OR serverBD.DCrateData[i].Model = Prop_Container_LD_PU
		OR serverBD.DCrateData[i].Model = DUMMY_MODEL_FOR_SCRIPT
			serverBD.DCrateData[i].Model = Prop_Box_Wood02A_PU
		ENDIF
		serverBD.DCrateData[i].iContents			=	g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iCrateContents
		//Contents
		IF serverBD.DCrateData[i].iContents = ciCRATE_CONTENTS_CASH
			serverBD.DCrateData[i].iContentsAmount	=	g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iCrateValue
			
		ELIF serverBD.DCrateData[i].iContents = ciCRATE_CONTENTS_WEAPON
			serverBD.DCrateData[i].ContentsWeapon	=	INT_TO_ENUM(PICKUP_TYPE, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iCrateValue)
			NET_PRINT(" -[]- GET_DESTRUCTIVLE_CRATE_LOCATION - WEAPON ENUM = ") NET_PRINT_INT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iCrateValue) NET_NL()
			
		ELIF serverBD.DCrateData[i].iContents = ciCRATE_CONTENTS_AMMO
			IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iCrateValue = 0
				serverBD.DCrateData[i].iContentsAmount = AMMO_SMALL
			ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iCrateValue = 1
				serverBD.DCrateData[i].iContentsAmount = AMMO_MEDIUM
			ELSE
				serverBD.DCrateData[i].iContentsAmount = AMMO_LARGE
			ENDIF
		ENDIF
	ENDREPEAT
	
	//Trailer Park - LOST
	/*IF ARE_VECTORS_ALMOST_EQUAL(<<61.369, 3694.225, 38.755>>, serverBD.vHideoutCentre, 25.0)
		NET_PRINT(" -[]- GET_DESTRUCTIVLE_CRATE_LOCATION - Trailer Park - Vector = ") NET_PRINT_VECTOR(<<66.6058, 3666.5156, 38.7341>>) NET_NL()
		//serverBD.DCrateData[0].vCoord = <<66.6058, 3666.5156, 38.7341>>
		
		//bUseCrates = TRUE
		
		serverBD.DCrateData[0].vCoord = <<47.03, 3715.32, 38.74>>
		serverBD.DCrateData[0].fHeading = 50.86
		serverBD.DCrateData[0].Model = Prop_Box_Wood02A_PU
		serverBD.DCrateData[0].iContents = ciCRATE_CONTENTS_CASH
		serverBD.DCrateData[0].iContentsAmount = (600*GET_RANDOM_INT_IN_RANGE(2, 10))
		
		serverBD.DCrateData[1].vCoord = <<48.02, 3713.64, 38.76>>
		serverBD.DCrateData[1].fHeading = -38.22
		serverBD.DCrateData[1].Model = Prop_Box_Wood02A_PU
		serverBD.DCrateData[1].iContents = ciCRATE_CONTENTS_DRUGS
		serverBD.DCrateData[1].iContentsAmount = GET_RANDOM_INT_IN_RANGE(20, 81)
		
		serverBD.DCrateData[2].vCoord = <<48.71, 3714.70, 38.75>>		//47.51, 3714.21, 39.39+0.25>>
		serverBD.DCrateData[2].fHeading = -39.53						//-38.90
		serverBD.DCrateData[2].Model = Prop_Box_Wood02A_PU
		serverBD.DCrateData[2].iContents = ciCRATE_CONTENTS_DRUGS
		serverBD.DCrateData[2].iContentsAmount = GET_RANDOM_INT_IN_RANGE(20, 81)
		
		serverBD.DCrateData[3].vCoord = <<50.61, 3718.11, 38.75>>
		serverBD.DCrateData[3].fHeading = -24.13
		serverBD.DCrateData[3].Model = Prop_Box_Wood02A_MWS
		serverBD.DCrateData[3].iContents = ciCRATE_CONTENTS_WEAPON
		serverBD.DCrateData[3].ContentsWeapon = PICKUP_WEAPON_PUMPSHOTGUN
		
		serverBD.DCrateData[4].vCoord = <<51.74, 3725.99, 38.75>>
		serverBD.DCrateData[4].fHeading = -21.14
		serverBD.DCrateData[4].Model = Prop_Container_LD_PU
		serverBD.DCrateData[4].iContents = ciCRATE_CONTENTS_WEAPON
		serverBD.DCrateData[4].ContentsWeapon = PICKUP_WEAPON_ADVANCEDRIFLE
	ENDIF*/
	
	//NET_PRINT(" -[]- GET_DESTRUCTIVLE_CRATE_LOCATION - NONE FOUND - Vector = ") NET_PRINT_VECTOR(<<0, 0, 0>>) NET_NL()
	//RETURN <<0, 0, 0>>
ENDPROC



////PURPOSE: Sets a Drop Off location
//PROC SET_DROP_OFF_LOCATION()
//	INT iRand
//	
//	//CITY
//	IF serverBD.vHideoutCentre.y < 800
//		iRand = GET_RANDOM_INT_IN_RANGE(0, 4)
//		IF iRand = 0
//			serverBD.vPackageDropOff			= <<304.9397, -3121.5227, 4.7969>>
//			NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - SET_DROP_OFF_LOCATION - CITY - DOCKS HUT") NET_NL()
//		ELIF iRand = 1
//			serverBD.vPackageDropOff			= <<1152.6261, -431.8657, 66.0120>>
//			NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - SET_DROP_OFF_LOCATION - CITY - WEED STORE") NET_NL()
//		ELIF iRand = 2
//			serverBD.vPackageDropOff			= <<-1487.8517, -207.5742, 49.8292>>
//			NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - SET_DROP_OFF_LOCATION - CITY - SQUARE BACK DOOR") NET_NL()
//		ELSE
//			serverBD.vPackageDropOff			= << -21.4635, -1002.0228, 28.5017>>
//			NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - SET_DROP_OFF_LOCATION - CITY - CENTRE BLUE DOOR") NET_NL()
//		ENDIF
//		
//	//COUNTRY
//	ELSE
//		iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
//		IF iRand = 0
//			serverBD.vPackageDropOff			= <<2160.1150, 4789.6514, 40.7138>>
//			NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - SET_DROP_OFF_LOCATION - COUNTRY - NORTH OF LAKE AIRPORT") NET_NL()
//		ELIF iRand = 1
//			serverBD.vPackageDropOff			= <<1716.2910, 3295.1038, 40.2865>>
//			NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - SET_DROP_OFF_LOCATION - COUNTRY - SOUTH OF LAKE AIRPORT") NET_NL()
//		ELSE
//			serverBD.vPackageDropOff			= <<-287.5460, 2535.5979, 74.4786>>
//			NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - SET_DROP_OFF_LOCATION - COUNTRY - OLD HOUSE") NET_NL()
//		ENDIF
//	ENDIF
//
//	//TEMP TEST
//	//serverBD.vPackageDropOff			= <<304.9397, -3121.5227, 4.7969>>
//ENDPROC

////PURPOSE: Sets up the Angled Areas for each location (until the mission creator is set up)
PROC SET_HIDEOUT_ANGLED_AREAS()
	serverBD.MainArea.vMin			= g_FMMC_STRUCT_ENTITIES.sGotoLocationData[1].vloC[0]
	serverBD.MainArea.vMax			= g_FMMC_STRUCT_ENTITIES.sGotoLocationData[2].vloC[0]
	serverBD.MainArea.fWidth		= g_FMMC_STRUCT_ENTITIES.sGotoLocationData[2].fRadius[0]
	
	serverBD.AiSpawnArea.vMin		= g_FMMC_STRUCT_ENTITIES.sGotoLocationData[3].vloC[0]
	serverBD.AiSpawnArea.vMax		= g_FMMC_STRUCT_ENTITIES.sGotoLocationData[4].vloC[0]
	serverBD.AiSpawnArea.fWidth		= g_FMMC_STRUCT_ENTITIES.sGotoLocationData[4].fRadius[0]
	
	serverBD.PlayerSpawnArea.vMin	= g_FMMC_STRUCT_ENTITIES.sGotoLocationData[5].vloC[0]
	serverBD.PlayerSpawnArea.vMax	= g_FMMC_STRUCT_ENTITIES.sGotoLocationData[6].vloC[0]
	serverBD.PlayerSpawnArea.fWidth	= g_FMMC_STRUCT_ENTITIES.sGotoLocationData[6].fRadius[0]
	
	serverBD.LeaveArea.vMin			= g_FMMC_STRUCT_ENTITIES.sGotoLocationData[7].vloC[0] - <<0, 0, 25>>
	serverBD.LeaveArea.vMax			= g_FMMC_STRUCT_ENTITIES.sGotoLocationData[8].vloC[0] + <<0, 0, 25>>
	serverBD.LeaveArea.fWidth		= g_FMMC_STRUCT_ENTITIES.sGotoLocationData[8].fRadius[0]

	serverBD.NearlyLeaveArea.vMin	= g_FMMC_STRUCT_ENTITIES.sGotoLocationData[9].vloC[0]  - <<0, 0, 25>>
	serverBD.NearlyLeaveArea.vMax	= g_FMMC_STRUCT_ENTITIES.sGotoLocationData[10].vloC[0] + <<0, 0, 25>>
	serverBD.NearlyLeaveArea.fWidth	= g_FMMC_STRUCT_ENTITIES.sGotoLocationData[10].fRadius[0]
ENDPROC

//PURPOSE: Sets up data for the Hideout Variation
PROC SET_HIDEOUT_VARIATION()
	
	//Check if it should be a Steal Package Variation
	/*IF ARE_VECTORS_ALMOST_EQUAL(MPGlobalsAmbience.HideoutData.vStealPackageLocation, serverBD.vHideoutBlipLocation, 10.0)
	#IF IS_DEBUG_BUILD OR bGangAttackDebugPackage #ENDIF
		
		serverBD.iStealPackageGang = MPGlobalsAmbience.HideoutData.iStealPackageForGang
		NET_PRINT("    ----->     HIDEOUT - iStealPackageGang = ") NET_PRINT_INT(serverBD.iStealPackageGang) NET_NL()
		
		IF serverBD.iStealPackageGang = GHO_SPG_LOST
			serverBD.vPackageDropOff = <<981.9021, -103.0495, 73.8538+0.25>>
			NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - SET_HIDEOUT_VARIATION - LOST - serverBD.vPackageDropOff = ") NET_PRINT_VECTOR(serverBD.vPackageDropOff) NET_NL()
			//SEND EVENT TO UPDATE GANG bStealPackageForGang AND CLEAR vStealPackageLocation
		ELSE	// = GHO_SPG_VAGOS
			serverBD.vPackageDropOff = <<967.7084, -1829.3368, 30.2374+0.4>>
			NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - SET_HIDEOUT_VARIATION - VAGOS - serverBD.vPackageDropOff = ") NET_PRINT_VECTOR(serverBD.vPackageDropOff) NET_NL()
			//SEND EVENT TO UPDATE GANG bStealPackageForGang AND CLEAR vStealPackageLocation
		ENDIF
		
		serverBD.iReservedObjects = 3
		
		serverBD.eHideoutVariation = HIDEOUT_PACKAGE_STEAL
		NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - SET_HIDEOUT_VARIATION = HIDEOUT_PACKAGE_STEAL") NET_NL()
		
		NET_PRINT("    ----->     HIDEOUT - vPackageDropOff = ") NET_PRINT_VECTOR(serverBD.vPackageDropOff) NET_NL()
		SET_MAX_NUM_PORTABLE_PICKUPS_CARRIED_BY_PLAYER(serverBD.PickUpData[0].Model, 1)
		BROADCAST_GASP_CLEANUP()
	ENDIF*/
	
	//Random Variation
	//IF serverBD.eHideoutVariation = HIDEOUT_RANDOM
	//	NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - SET_HIDEOUT_VARIATION - RANDOMISED") NET_NL()
		
		serverBD.eHideoutVariation = HIDEOUT_KILL_PEDS
		NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - SET_HIDEOUT_VARIATION = HIDEOUT_KILL_PEDS") NET_NL()
		
		//TERRITORY CONTROL - HELD BACK UNTIL DLC
		/*INT iRand = GET_RANDOM_INT_IN_RANGE(0, 100)
		IF iRand < 60
			serverBD.eHideoutVariation = HIDEOUT_KILL_PEDS
			NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - SET_HIDEOUT_VARIATION = HIDEOUT_KILL_PEDS") NET_NL()
		//ELIF iRand >= 90
		//	serverBD.eHideoutVariation = HIDEOUT_PACKAGE_STEAL
		ELSE
			serverBD.eHideoutVariation = HIDEOUT_TERRITORY_CONTROL
			NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - SET_HIDEOUT_VARIATION = HIDEOUT_TERRITORY_CONTROL") NET_NL()
		ENDIF*/
	//ENDIF
	
	#IF IS_DEBUG_BUILD
		IF bGangAttackDebugKill
			serverBD.eHideoutVariation = HIDEOUT_KILL_PEDS
			NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - SET_HIDEOUT_VARIATION - DEBUG - HIDEOUT_KILL_PEDS") NET_NL()
		//ELIF bGangAttackDebugControl
		//	serverBD.eHideoutVariation = HIDEOUT_TERRITORY_CONTROL
		//	NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - SET_HIDEOUT_VARIATION - DEBUG - HIDEOUT_TERRITORY_CONTROL") NET_NL()
		/*ELIF bGangAttackDebugPackage
			serverBD.eHideoutVariation = HIDEOUT_PACKAGE_STEAL
			serverBD.iReservedObjects = 3
			NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - SET_HIDEOUT_VARIATION - DEBUG - HIDEOUT_PACKAGE_STEAL") NET_NL()*/
		ENDIF
	#ENDIF
	
ENDPROC

//PURPOSE: Sets the Gang ID so we can make certain Gangs Angry at the player
PROC SET_PED_GANG_ID()
	SWITCH serverBD.PedGangModel
		//VAGOS
		/*CASE G_M_Y_MEXGOON_01
		CASE G_M_Y_MEXGOON_02
			//serverBD.iGangID = GANG_ATTACK_GANG_VAGOS
			//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iGangBeingFoughtInGangAttack = GANG_ATTACK_GANG_VAGOS
			NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - SET_PED_GANG_ID - G_M_Y_MEXGOON_01") NET_NL()
		BREAK*/
		
		//LOST
		/*CASE G_M_Y_Lost_01
			//serverBD.iGangID = GANG_ATTACK_GANG_LOST
			//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iGangBeingFoughtInGangAttack = GANG_ATTACK_GANG_LOST
			NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - SET_PED_GANG_ID - G_M_Y_Lost_01") NET_NL()
		BREAK*/
		
		//FAMILIES
		CASE G_M_Y_FAMCA_01
			serverBD.iGangID = GANG_ATTACK_GANG_FAMILIES
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iGangBeingFoughtInGangAttack = GANG_ATTACK_GANG_FAMILIES
			NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - SET_PED_GANG_ID - G_M_Y_FAMCA_01") NET_NL()
		BREAK
		
		//MERRYWEATHER
		CASE S_M_Y_BLACKOPS_01
			serverBD.iGangID = GANG_ATTACK_GANG_MERRYWEATHER
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iGangBeingFoughtInGangAttack = GANG_ATTACK_GANG_MERRYWEATHER
			NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - SET_PED_GANG_ID - S_M_Y_BLACKOPS_01") NET_NL()
		BREAK		
	ENDSWITCH
ENDPROC

/// PURPOSE: Loads then launches a mission script, passing missionData as args to the launched script.  This will setup a mission and instance.
/// NOTES:	 Added a new temporary variable defaulting to FALSE so that I can tell if this function is called by my new routines or the older stuff.
FUNC BOOL NET_LOAD_AND_LAUNCH_SCRIPT_WITH_GA_PICKUP_ARGS(GANG_ATTACK_PICKUPS_DATA missionData, BOOL paramCalledFromMissionTriggerer = FALSE)

	//Bail as we are killing all scripts. 
	IF SHOULD_TRANSITION_SESSION_KILL_ALL_MP_SCRIPTS()
		#IF IS_DEBUG_BUILD
		NET_PRINT(" (NOT called using Mission Triggerer)")
		#ENDIF
		RETURN FALSE
	ENDIF
	
	// KGM 31/8/12: My new parameter doesn't get used in release, so I'm going to fool the compiler - this variable will be removed at some point anyway.
	paramCalledFromMissionTriggerer = paramCalledFromMissionTriggerer

	TEXT_LABEL_31 tl32ScriptName = GET_MP_MISSION_NAME(missionData.idMission)
	
	IF DOES_SCRIPT_EXIST(tl32ScriptName)
	
		REQUEST_SCRIPT(tl32ScriptName)
	
		#IF IS_DEBUG_BUILD
		NET_PRINT_STRINGS("...KGM MP: NET_LOAD_AND_LAUNCH_SCRIPT_WITH_GA_PICKUP_ARGS: Launching - ", tl32ScriptName)
		IF (paramCalledFromMissionTriggerer)
			NET_PRINT(" (called by Mission Triggerer)")
		ELSE
			NET_PRINT(" (NOT called using Mission Triggerer)")
		ENDIF
		NET_NL()
		#ENDIF
		
		IF HAS_SCRIPT_LOADED(tl32ScriptName)
			// KGM 3/12/12: Don't launch the script if another version with the same instance ID is still running - when script thinks the script is
			//					removed, code can still be cleaning up under-the-hood waiting for other particpants to acknowledge they've quit the script.
			BOOL localCheckOnly = TRUE
			IF (NETWORK_IS_SCRIPT_ACTIVE(tl32ScriptName, missionData.iInstanceId, localCheckOnly))
				#IF IS_DEBUG_BUILD
					NET_PRINT("...KGM MP: DELAYING LAUNCH: Code must still be cleaning up a previous instance with duplicate details: ")
					NET_PRINT(tl32ScriptName)
					NET_PRINT(" (instID=")
					NET_PRINT_INT(missionData.iInstanceId)
					NET_PRINT(")")
					NET_NL()
				#ENDIF
				
				RETURN FALSE
			ENDIF
			
			THREADID thread = START_NEW_SCRIPT_WITH_ARGS(tl32ScriptName, missionData, SIZE_OF(missionData), GET_MP_MISSION_STACK_SIZE(missionData.idMission) )
			SET_SCRIPT_AS_NO_LONGER_NEEDED(tl32ScriptName)
			
			IF NATIVE_TO_INT(thread) > 0 
						
				#IF IS_DEBUG_BUILD
				NET_PRINT_TIME()
				NET_PRINT_STRINGS("NET_LOAD_AND_LAUNCH_SCRIPT_WITH_GA_PICKUP_ARGS: Launched = ", tl32ScriptName)
				NET_PRINT_STRING_INT(" with thread ID = ", NATIVE_TO_INT(thread)) 
				NET_PRINT_STRING_INT(" and instance ID = ", missionData.iInstanceId) 
				NET_NL()
				#ENDIF
				
				RETURN TRUE
			
			ELSE			
				NET_PRINT_STRINGS("...KGM MP: NET_LOAD_AND_LAUNCH_SCRIPT_WITH_GA_PICKUP_ARGS: Bad thread ID: ", tl32ScriptName) NET_NL()
			ENDIF
			
		ENDIF
		
	ELSE	
		NET_PRINT_STRINGS("...KGM MP: NET_LOAD_AND_LAUNCH_SCRIPT_WITH_GA_PICKUP_ARGS:  Script does not exist: ", tl32ScriptName) NET_NL()
	ENDIF
	
	RETURN FALSE
	
ENDFUNC


//PURPOSE: Launches the Gang Attack Pickup Maintenance script (Used by CLIENT)
FUNC BOOL HAS_GA_PICKUP_MAINTENANCED_LAUNCHED()
	
	IF NOT NETWORK_IS_SCRIPT_ACTIVE("AM_GA_PICKUPS", serverBD.GAPickupData.iInstanceId, TRUE)
		IF NET_LOAD_AND_LAUNCH_SCRIPT_WITH_GA_PICKUP_ARGS(serverBD.GAPickupData)
			#IF IS_DEBUG_BUILD NET_NL() NET_PRINT("    ----->     HIDEOUT - LAUNCHED SCRIPT AM_BRU_BOX.sc - INSTANCE = ") NET_PRINT_INT(serverBD.GAPickupData.iInstanceId) NET_NL() #ENDIF
			RETURN TRUE
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD NET_NL() NET_PRINT("    ----->     HIDEOUT - SCRIPT ALREADY RUNNING AM_BRU_BOX.sc - INSTANCE = ") NET_PRINT_INT(serverBD.GAPickupData.iInstanceId) NET_NL() #ENDIF
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


//PURPOSE: Get spare Instance for AM_GA_PICKUPS.sc (Used by SERVER)
FUNC INT GET_GA_PICKUP_INSTANCE()
	INT i
	REPEAT 63 i
		IF NOT NETWORK_IS_SCRIPT_ACTIVE("AM_GA_PICKUPS", i, FALSE)
			RETURN i
		ENDIF
	ENDREPEAT
	
	RETURN -1
ENDFUNC

//PURPOSE: Set Data for AM_GA_PICKUPS.sc (Used by SERVER)
PROC SET_GA_PICKUP_DATA() 
	IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_SetAMGAPickupData)
		serverBD.GAPickupData.iInstanceId = GET_GA_PICKUP_INSTANCE()
		PRINTLN("  ---->  HIDEOUT - SET_GA_PICKUP_DATA - iInstance = ", serverBD.GAPickupData.iInstanceId)
		
		IF serverBD.GAPickupData.iInstanceId != -1
			
			serverBD.GAPickupData.vGangAttackLocation = serverBD.vHideoutCentre
			PRINTLN("  ---->  HIDEOUT - SET_GA_PICKUP_DATA - vGangAttackLocation = ", serverBD.GAPickupData.vGangAttackLocation)
			
			INT i
			REPEAT MAX_NUM_DESTRUCTIBLE_CRATES i
				IF NOT IS_BIT_SET(serverBD.DCrateData[i].iBitSet, iBS_DC_CrateDestroyed)
					SET_BIT(serverBD.GAPickupData.iBitSet, iBS_GAP_Valid)
					SET_BIT(serverBD.GAPickupData.iBitSet, iBS_GAP_Launch)
					
					serverBD.GAPickupData.vCoord[i]				=	serverBD.DCrateData[i].vCoord
					serverBD.GAPickupData.fHeading[i]			=	serverBD.DCrateData[i].fHeading
					serverBD.GAPickupData.iContents[i] 			=	serverBD.DCrateData[i].iContents			//= ciCRATE_CONTENTS_CASH
					serverBD.GAPickupData.ContentsWeapon[i] 	=	serverBD.DCrateData[i].ContentsWeapon		//= PICKUP_WEAPON_ADVANCEDRIFLE
					serverBD.GAPickupData.iContentsAmount[i] 	=	serverBD.DCrateData[i].iContentsAmount		// = 500
					
					PRINTLN("  ---->  HIDEOUT - SET_GA_PICKUP_DATA - DONE FOR CRATE ", i)
				
				#IF IS_DEBUG_BUILD
				ELSE
					PRINTLN("  ---->  HIDEOUT - SET_GA_PICKUP_DATA - ALREADY SMASHED CRATE ", i)
				#ENDIF
				ENDIF
			ENDREPEAT
			
			//ABORT IF NO VALID CRATES FOUND
			#IF IS_DEBUG_BUILD
			IF NOT IS_BIT_SET(serverBD.GAPickupData.iBitSet, iBS_GAP_Launch)
				PRINTLN("  ---->  HIDEOUT - SET_GA_PICKUP_DATA - NO VALID CRATES FOUND")
			ENDIF
			#ENDIF
			
		//ABORT NO SPARE INSTANCE FOUND - SHOULDN'T HAPPEN
		#IF IS_DEBUG_BUILD
		ELSE
			PRINTLN("  ---->  HIDEOUT - SET_GA_PICKUP_DATA - NO SPARE INSTANCE FOUND - SHOULDN'T HAPPEN")
		#ENDIF
		ENDIF
		
		SET_BIT(serverBD.iServerBitSet, biS_SetAMGAPickupData)
		PRINTLN("  ---->  HIDEOUT - SET_GA_PICKUP_DATA - biS_SetAMGAPickupData SET")
	ENDIF
ENDPROC


//PURPOSE: Sets the passed in peds voice if they don't have one with the correct ambient speech
PROC SET_GANG_PED_VOICE(PED_INDEX &pedId)
	SWITCH GET_ENTITY_MODEL(pedId)
		CASE S_M_Y_Cop_01
		CASE S_M_Y_Swat_01
		CASE S_M_Y_MARINE_03
		CASE S_M_Y_BLACKOPS_01
		CASE s_m_m_highsec_01
		CASE S_m_m_armoured_01
		CASE S_M_M_Prisguard_01
		CASE S_M_M_Security_01
		CASE mp_g_m_pros_01
			SET_AMBIENT_VOICE_NAME(pedId, "G_M_M_ARMLIEUT_01_WHITE_ARMENIAN_MINI_01")
		BREAK
		CASE S_M_Y_Prisoner_01
			SET_AMBIENT_VOICE_NAME(pedId, "G_M_Y_LOST_03_WHITE_FULL_01")
		BREAK
		CASE G_M_Y_SalvaGoon_01
			SET_AMBIENT_VOICE_NAME(pedId, "G_M_Y_SALVAGOON_01_SALVADORIAN_MINI_03")
		BREAK
		CASE G_M_Y_Azteca_01
			SET_AMBIENT_VOICE_NAME(pedId, "G_M_Y_LATINO01_LATINO_MINI_02")
		BREAK
		CASE A_M_O_ACULT_02
			SET_AMBIENT_VOICE_NAME(pedId, "A_M_M_HILLBILLY_02_WHITE_MINI_03")
		BREAK
	ENDSWITCH
ENDPROC


//Showed we hand gang attack UI
FUNC BOOL SHOULD_HIDE_GANG_ATTACK_UI()
	IF FM_EVENT_IS_PLAYER_ON_ANY_FM_EVENT(PLAYER_ID())
	OR IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_TIME_TRIAL)
	OR GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) = GB_UI_LEVEL_FULL
		IF FM_EVENT_GET_PLAYER_CURRENT_FM_EVENT(PLAYER_ID()) = FMMC_TYPE_DEAD_DROP
		OR FM_EVENT_GET_PLAYER_CURRENT_FM_EVENT(PLAYER_ID()) = FMMC_TYPE_HOT_PROPERTY
		OR FM_EVENT_GET_PLAYER_CURRENT_FM_EVENT(PLAYER_ID()) = FMMC_TYPE_MOVING_TARGET
		OR FM_EVENT_GET_PLAYER_CURRENT_FM_EVENT(PLAYER_ID()) = FMMC_TYPE_KING_CASTLE
		OR FM_EVENT_GET_PLAYER_CURRENT_FM_EVENT(PLAYER_ID()) = FMMC_TYPE_HUNT_THE_BEAST
		OR FM_EVENT_GET_PLAYER_CURRENT_FM_EVENT(PLAYER_ID()) = FMMC_TYPE_HOLD_THE_WHEEL
		
			IF NOT IS_BIT_SET(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iFmAmbientEventBitSet, ciSET_PLAYER_IN_FM_EVENT_PARTICIPATION_RANGE)
			AND NOT IS_BIT_SET(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iFmAmbientEventBitSet, ciSET_PLAYER_IS_PERMANENT_PARTICIPANT_IN_FM_EVENT)
				RETURN FALSE
			ENDIF
		ENDIF
		IF NOT IS_BIT_SET(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iFmAmbientEventBitSet, ciSET_PLAYER_IS_RESTRICTED_WITH_HIDE_OPTION)
		OR GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) = GB_UI_LEVEL_FULL
			RETURN TRUE
		ENDIF
	ENDIF
	IF IS_PLAYER_USING_RCBANDITO(PLAYER_ID()) 
 		RETURN TRUE
	ENDIF
	
	#IF FEATURE_CASINO_HEIST
	IF IS_PLAYER_USING_RC_TANK(PLAYER_ID())
 		RETURN TRUE
	ENDIF
	#ENDIF
	
	IF IS_PLAYER_AN_ANIMAL(PLAYER_ID())
		RETURN TRUE
	ENDIF
	
	IF IS_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITITCAL_ENTITY()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC






INT iMusicEventBitSet
CONST_INT biME_Warning		0
CONST_INT biME_Fight		1
CONST_INT biME_Halfway		2
CONST_INT biME_Secondary	3
CONST_INT biME_End			4

//PURPOSE: Triggers the music event for the mission variation
FUNC BOOL PLAY_MUSIC_EVENT(INT iEventNum)
	IF SHOULD_HIDE_GANG_ATTACK_UI()
		RETURN FALSE
	ENDIF
	SWITCH iEventNum
		CASE 1
			/*IF serverBD.eHideoutVariation = HIDEOUT_TERRITORY_CONTROL
				TRIGGER_MUSIC_EVENT("GA_CAPTURE_START")
				NET_PRINT("    ----->     HIDEOUT - CONTROL_MUSIC_EVENTS - GA_CAPTURE_START") NET_NL()
			ELIF serverBD.eHideoutVariation = HIDEOUT_PACKAGE_STEAL
				TRIGGER_MUSIC_EVENT("GA_GRAB_START")
				NET_PRINT("    ----->     HIDEOUT - CONTROL_MUSIC_EVENTS - GA_GRAB_START") NET_NL()
			ELSE*/
				TRIGGER_MUSIC_EVENT("GA_KILL_START")
				NET_PRINT("    ----->     HIDEOUT - CONTROL_MUSIC_EVENTS - GA_KILL_START") NET_NL()
			//ENDIF
		RETURN TRUE
		
		CASE 2
			IF IS_BIT_SET(iMusicEventBitSet, biME_Warning)
				/*IF serverBD.eHideoutVariation = HIDEOUT_TERRITORY_CONTROL
					TRIGGER_MUSIC_EVENT("GA_CAPTURE_ALERTED")
					NET_PRINT("    ----->     HIDEOUT - CONTROL_MUSIC_EVENTS - GA_CAPTURE_ALERTED") NET_NL()
				ELIF serverBD.eHideoutVariation = HIDEOUT_PACKAGE_STEAL
					TRIGGER_MUSIC_EVENT("GA_GRAB_ALERTED")
					NET_PRINT("    ----->     HIDEOUT - CONTROL_MUSIC_EVENTS - GA_GRAB_ALERTED") NET_NL()
				ELSE*/
					TRIGGER_MUSIC_EVENT("GA_KILL_ALERTED")
					NET_PRINT("    ----->     HIDEOUT - CONTROL_MUSIC_EVENTS - GA_KILL_ALERTED") NET_NL()
				//ENDIF
			ELSE
				/*IF serverBD.eHideoutVariation = HIDEOUT_TERRITORY_CONTROL
					TRIGGER_MUSIC_EVENT("GA_CAPTURE_ALERTED_RS")
					NET_PRINT("    ----->     HIDEOUT - CONTROL_MUSIC_EVENTS - GA_CAPTURE_ALERTED_RS") NET_NL()
				ELIF serverBD.eHideoutVariation = HIDEOUT_PACKAGE_STEAL
					TRIGGER_MUSIC_EVENT("GA_GRAB_ALERTED_RS")
					NET_PRINT("    ----->     HIDEOUT - CONTROL_MUSIC_EVENTS - GA_GRAB_ALERTED_RS") NET_NL()
				ELSE*/
					TRIGGER_MUSIC_EVENT("GA_KILL_ALERTED_RS")
					NET_PRINT("    ----->     HIDEOUT - CONTROL_MUSIC_EVENTS - GA_KILL_ALERTED_RS") NET_NL()
				//ENDIF
			ENDIF
		RETURN TRUE
		
		CASE 3
			IF IS_BIT_SET(iMusicEventBitSet, biME_Fight)
				/*IF serverBD.eHideoutVariation = HIDEOUT_TERRITORY_CONTROL
					TRIGGER_MUSIC_EVENT("GA_CAPTURE_50")
					NET_PRINT("    ----->     HIDEOUT - CONTROL_MUSIC_EVENTS - GA_CAPTURE_50") NET_NL()
				ELIF serverBD.eHideoutVariation = HIDEOUT_PACKAGE_STEAL
					TRIGGER_MUSIC_EVENT("GA_GRAB_GOT_PACKAGE")
					NET_PRINT("    ----->     HIDEOUT - CONTROL_MUSIC_EVENTS - GA_GRAB_GOT_PACKAGE") NET_NL()
				ELSE*/
					TRIGGER_MUSIC_EVENT("GA_KILL_HALF")
					NET_PRINT("    ----->     HIDEOUT - CONTROL_MUSIC_EVENTS - GA_KILL_HALF") NET_NL()
				//ENDIF
			ELSE
				/*IF serverBD.eHideoutVariation = HIDEOUT_TERRITORY_CONTROL
					TRIGGER_MUSIC_EVENT("GA_CAPTURE_50_RS")
					NET_PRINT("    ----->     HIDEOUT - CONTROL_MUSIC_EVENTS - GA_CAPTURE_50_RS") NET_NL()
				ELIF serverBD.eHideoutVariation = HIDEOUT_PACKAGE_STEAL
					TRIGGER_MUSIC_EVENT("GA_GRAB_GOT_PACKAGE_RS")
					NET_PRINT("    ----->     HIDEOUT - CONTROL_MUSIC_EVENTS - GA_GRAB_GOT_PACKAGE_RS") NET_NL()
				ELSE*/
					TRIGGER_MUSIC_EVENT("GA_KILL_HALF_RS")
					NET_PRINT("    ----->     HIDEOUT - CONTROL_MUSIC_EVENTS - GA_KILL_HALF_RS") NET_NL()
				//ENDIF
			ENDIF
		RETURN TRUE
		
		CASE 4
			IF IS_BIT_SET(iMusicEventBitSet, biME_Halfway)
				/*IF serverBD.eHideoutVariation = HIDEOUT_TERRITORY_CONTROL
					TRIGGER_MUSIC_EVENT("GA_CAPTURE_KILL_REMAINING")
					NET_PRINT("    ----->     HIDEOUT - CONTROL_MUSIC_EVENTS - GA_CAPTURE_KILL_REMAINING") NET_NL()
				ELIF serverBD.eHideoutVariation = HIDEOUT_PACKAGE_STEAL
					TRIGGER_MUSIC_EVENT("GA_GRAB_PACKAGE_OUT")
					NET_PRINT("    ----->     HIDEOUT - CONTROL_MUSIC_EVENTS - GA_GRAB_PACKAGE_OUT") NET_NL()
				ELSE*/
					TRIGGER_MUSIC_EVENT("GA_KILL_LEAVE")
					NET_PRINT("    ----->     HIDEOUT - CONTROL_MUSIC_EVENTS - GA_KILL_LEAVE") NET_NL()
				//ENDIF
			ELSE
				/*IF serverBD.eHideoutVariation = HIDEOUT_TERRITORY_CONTROL
					TRIGGER_MUSIC_EVENT("GA_CAPTURE_KILL_REMAINING_RS")
					NET_PRINT("    ----->     HIDEOUT - CONTROL_MUSIC_EVENTS - GA_CAPTURE_KILL_REMAINING_RS") NET_NL()
				ELIF serverBD.eHideoutVariation = HIDEOUT_PACKAGE_STEAL
					TRIGGER_MUSIC_EVENT("GA_GRAB_PACKAGE_OUT_RS")
					NET_PRINT("    ----->     HIDEOUT - CONTROL_MUSIC_EVENTS - GA_GRAB_PACKAGE_OUT_RS") NET_NL()
				ELSE*/
					TRIGGER_MUSIC_EVENT("GA_KILL_LEAVE_RS")
					NET_PRINT("    ----->     HIDEOUT - CONTROL_MUSIC_EVENTS - GA_KILL_LEAVE_RS") NET_NL()
				//ENDIF
				NET_PRINT("    ----->     HIDEOUT - CONTROL_MUSIC_EVENTS - biME_Secondary - RS") NET_NL()
			ENDIF
		RETURN TRUE
		
		CASE 5
			/*IF serverBD.eHideoutVariation = HIDEOUT_TERRITORY_CONTROL
				IF PREPARE_MUSIC_EVENT("GA_CAPTURE_COMPLETE")
					TRIGGER_MUSIC_EVENT("GA_CAPTURE_COMPLETE")
					NET_PRINT("    ----->     HIDEOUT - CONTROL_MUSIC_EVENTS - GA_CAPTURE_COMPLETE") NET_NL()
					RETURN TRUE
				ENDIF
			ELIF serverBD.eHideoutVariation = HIDEOUT_PACKAGE_STEAL
				IF PREPARE_MUSIC_EVENT("GA_GRAB_COMPLETE")
					TRIGGER_MUSIC_EVENT("GA_GRAB_COMPLETE")
					NET_PRINT("    ----->     HIDEOUT - CONTROL_MUSIC_EVENTS - GA_GRAB_COMPLETE") NET_NL()
					RETURN TRUE
				ENDIF
			ELSE*/
				IF PREPARE_MUSIC_EVENT("GA_KILL_COMPLETE")
					TRIGGER_MUSIC_EVENT("GA_KILL_COMPLETE")
					NET_PRINT("    ----->     HIDEOUT - CONTROL_MUSIC_EVENTS - GA_KILL_COMPLETE") NET_NL()
					RETURN TRUE
				ENDIF
			//ENDIF
			
		RETURN FALSE
	ENDSWITCH
	
	NET_SCRIPT_ASSERT("HIDEOUT - PLAY_MUSIC_EVENT - INVALID MUSIC EVENT PASSED IN")
	RETURN FALSE
ENDFUNC

//PURPOSE: Returns TRUE when the variation has passed a halfway marker
FUNC BOOL PASSED_VARIATION_HALF_WAY()
	/*IF serverBD.eHideoutVariation = HIDEOUT_TERRITORY_CONTROL
		IF HAS_NET_TIMER_STARTED(serverBD.TerritoryControlTimer)
			IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.TerritoryControlTimer) >= (serverBD.iTakeControlTime)
				RETURN TRUE
			ENDIF
		ENDIF
	ELIF serverBD.eHideoutVariation = HIDEOUT_PACKAGE_STEAL
		IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.PickUpData[0].NetID)
			IF NOT IS_ENTITY_IN_ANGLED_AREA(NET_TO_OBJ(serverBD.PickUpData[0].NetID), serverBD.MainArea.vMin-<<0, 0, 250>>, serverBD.MainArea.vMax+<<0, 0, 150>>, serverBD.MainArea.fWidth)
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE*/
		IF serverBD.iTotalKills >= (serverBD.iKillGoal/2)
			RETURN TRUE
		ENDIF
	//ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Plays the music event for the current stage
PROC CONTROL_MUSIC_EVENTS()
	SWITCH GET_CLIENT_HIDEOUT_STAGE(PARTICIPANT_ID())
		CASE HIDEOUT_STAGE_WARNING
			IF NOT IS_BIT_SET(iMusicEventBitSet, biME_Warning)
				IF PLAY_MUSIC_EVENT(1)
					SET_BIT(iMusicEventBitSet, biME_Warning)
					NET_PRINT("    ----->     HIDEOUT - CONTROL_MUSIC_EVENTS - biME_Warning SET") NET_NL()
				ENDIF
			ENDIF
		BREAK
		
		CASE HIDEOUT_STAGE_PRIMARY
			IF NOT PASSED_VARIATION_HALF_WAY()
				IF NOT IS_BIT_SET(iMusicEventBitSet, biME_Fight)
					IF PLAY_MUSIC_EVENT(2)
						SET_BIT(iMusicEventBitSet, biME_Fight)
						NET_PRINT("    ----->     HIDEOUT - CONTROL_MUSIC_EVENTS - biME_Fight SET") NET_NL()
					ENDIF
				ENDIF
			ELSE	
				IF NOT IS_BIT_SET(iMusicEventBitSet, biME_Halfway)
					IF PLAY_MUSIC_EVENT(3)
						SET_BIT(iMusicEventBitSet, biME_Halfway)
						NET_PRINT("    ----->     HIDEOUT - CONTROL_MUSIC_EVENTS - biME_Halfway SET") NET_NL()
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE HIDEOUT_STAGE_SECONDARY
			//IF serverBD.eHideoutVariation != HIDEOUT_PACKAGE_STEAL
				IF NOT IS_BIT_SET(iMusicEventBitSet, biME_Secondary)
					IF PLAY_MUSIC_EVENT(4)
						SET_BIT(iMusicEventBitSet, biME_Secondary)
						NET_PRINT("    ----->     HIDEOUT - CONTROL_MUSIC_EVENTS - biME_Secondary SET") NET_NL()
					ENDIF
				ENDIF
			/*ELSE	
				IF NOT PASSED_VARIATION_HALF_WAY()
					IF NOT IS_BIT_SET(iMusicEventBitSet, biME_Halfway)
						IF PLAY_MUSIC_EVENT(3)
							SET_BIT(iMusicEventBitSet, biME_Halfway)
							NET_PRINT("    ----->     HIDEOUT - CONTROL_MUSIC_EVENTS - biME_Halfway SET") NET_NL()
						ENDIF
					ENDIF
				ELSE	
					IF NOT IS_BIT_SET(iMusicEventBitSet, biME_Secondary)
						IF PLAY_MUSIC_EVENT(4)
							CLEAR_SPAWN_AREA()
							SET_BIT(iMusicEventBitSet, biME_Secondary)
							NET_PRINT("    ----->     HIDEOUT - CONTROL_MUSIC_EVENTS - biME_Secondary SET") NET_NL()
						ENDIF
					ENDIF
				ENDIF
			ENDIF*/
		BREAK
		
		CASE HIDEOUT_STAGE_REWARDS_AND_FEEDACK
			IF NOT IS_BIT_SET(iMusicEventBitSet, biME_End)
				IF PLAY_MUSIC_EVENT(5)
					SET_BIT(iMusicEventBitSet, biME_End)
					NET_PRINT("    ----->     HIDEOUT - CONTROL_MUSIC_EVENTS - biME_End SET") NET_NL()
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

//PURPOSE: Controls starting the playstats
PROC CONTROL_PLAY_STATS()
	IF NOT IS_BIT_SET(iBoolsBitSet, biStartedPlayStats)
		IF GET_CLIENT_HIDEOUT_STAGE(PARTICIPANT_ID()) > HIDEOUT_STAGE_WARNING
		AND GET_CLIENT_HIDEOUT_STAGE(PARTICIPANT_ID()) < HIDEOUT_STAGE_REWARDS_AND_FEEDACK
			DEAL_WITH_FM_MATCH_START(FMMC_TYPE_GANGHIDEOUT, serverBD.iPlayStatsID[1], serverBD.iPlayStatsID[0])
			SET_BIT(iBoolsBitSet, biStartedPlayStats)
			NET_PRINT("    ----->     HIDEOUT - CONTROL_PLAY_STATS - DEAL_WITH_FM_MATCH_START DONE") NET_NL()
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Controls the Gang being angry with the Player. Angry Gangs can't be called upon for abilities.
PROC MAINTAIN_GANG_ANGRY_STAT()
	IF serverBD.iGangID != -1
		IF GET_CLIENT_HIDEOUT_STAGE(PARTICIPANT_ID()) > HIDEOUT_STAGE_WARNING
			IF NOT IS_BIT_SET(iBoolsBitSet, biInitialGangAngryCheckDone)
				SET_GANG_ANGRY_AT_PLAYER(serverBD.iGangID, GANG_ANGRY_TIME_MEDIUM)
				NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - MAINTAIN_GANG_ANGRY_STAT - GANG_ANGRY_TIME_MEDIUM") NET_NL()
				SET_BIT(iBoolsBitSet, biInitialGangAngryCheckDone)
			ELSE
				IF NOT IS_GANG_ANGRY_AT_PLAYER(serverBD.iGangID)
					SET_GANG_ANGRY_AT_PLAYER(serverBD.iGangID, GANG_ANGRY_TIME_SHORT)
					NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - MAINTAIN_GANG_ANGRY_STAT - GANG_ANGRY_TIME_SHORT") NET_NL()
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Adds onto the difficulty based on the passed in player
PROC CALCULATE_DIFFICULTY_ADDITION_FOR_PLAYER(PLAYER_INDEX thisPlayer)
	serverBD.iDifficultyFromPlayerStats += 60
	//serverBD.iDifficultyFromPlayerStats += ROUND(GET_PLAYER_SKILL_FLOAT(thisPlayer, PLAYER_SKILL_SHOOTING))
	INT iRank = GET_PLAYER_FM_RANK(thisPlayer)
	serverBD.iDifficultyFromPlayerStats += iRank
	serverBD.iAverageRank += iRank
ENDPROC

//PURPOSE: Returns TRUE if the player ahs left the Hideout area
FUNC BOOL HAS_PED_LEFT_AREA(PED_INDEX thisPed, BOOL bLongRange = FALSE)
	IF bLongRange = FALSE
		IF NOT IS_ENTITY_IN_ANGLED_AREA(thisPed, serverBD.MainArea.vMin, serverBD.MainArea.vMax, serverBD.MainArea.fWidth)
			RETURN TRUE
		ENDIF
	ELSE
		IF NOT IS_ENTITY_IN_ANGLED_AREA(thisPed, serverBD.LeaveArea.vMin, serverBD.LeaveArea.vMax, serverBD.LeaveArea.fWidth)
			NET_PRINT("    ----->     HIDEOUT - HAS_PED_LEFT_AREA - LONG RANGE - TRUE") NET_NL()
			PRINTLN("    ----->     HIDEOUT - HAS_PED_LEFT_AREA - LONG RANGE - vMin ", serverBD.LeaveArea.vMin)
			PRINTLN("    ----->     HIDEOUT - HAS_PED_LEFT_AREA - LONG RANGE - vMax ", serverBD.LeaveArea.vMax)
			PRINTLN("    ----->     HIDEOUT - HAS_PED_LEFT_AREA - LONG RANGE - fWidth ", serverBD.LeaveArea.fWidth)
			#IF IS_DEBUG_BUILD
			VECTOR vTemp = GET_ENTITY_COORDS(PLAYER_PED_ID())
			#ENDIF
			PRINTLN("    ----->     HIDEOUT - HAS_PED_LEFT_AREA - LONG RANGE - Player ", vTemp)
			RETURN TRUE
		ELIF NOT IS_ENTITY_IN_ANGLED_AREA(thisPed, serverBD.PlayerSpawnArea.vMin, serverBD.PlayerSpawnArea.vMax, serverBD.PlayerSpawnArea.fWidth)
		AND  NOT IS_ENTITY_IN_ANGLED_AREA(thisPed, serverBD.NearlyLeaveArea.vMin, serverBD.NearlyLeaveArea.vMax, serverBD.NearlyLeaveArea.fWidth)
			IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				IF GET_CLIENT_HIDEOUT_STAGE(PARTICIPANT_ID()) > HIDEOUT_STAGE_WARNING
					IF HAS_NET_TIMER_EXPIRED(iWarningHelpTimer, iWarningHelpTimerDelay)
						IF NOT SHOULD_HIDE_GANG_ATTACK_UI()
							PRINT_HELP("GHO_WARN")
							iWarningHelpTimerDelay = 30000
							RESET_NET_TIMER(iWarningHelpTimer)
							NET_PRINT("    ----->     HIDEOUT - HAS_PED_LEFT_AREA - WARNING HELP - PRINTED") NET_NL()
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("GHO_WARN")
				iWarningHelpTimerDelay = 3000
				CLEAR_HELP()
				NET_PRINT("    ----->     HIDEOUT - HAS_PED_LEFT_AREA - WARNING HELP - CLEARED") NET_NL()
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


//PUROSE: SERVER ONLY - Stores the updated Ped Accuracy to be used
PROC STORE_PED_DIFFICULTY()
	//Difficulty
	serverBD.iDifficultyFromPlayerStats = (serverBD.iDifficultyFromPlayerStats/4)		//((serverBD.iDifficultyFromPlayerStats + (serverBD.iPedsSpawned*25)) / 10)
	IF serverBD.iDifficultyFromPlayerStats > 100
		serverBD.iDifficultyFromPlayerStats = 100
	ELIF serverBD.iDifficultyFromPlayerStats < 0
		serverBD.iDifficultyFromPlayerStats = 0
	ENDIF
	NET_PRINT_TIME() NET_PRINT("    ----->     HIDEOUT - STORE_PED_DIFFICULTY - serverBD.iDifficultyFromPlayerStats = ") NET_PRINT_INT(serverBD.iDifficultyFromPlayerStats) NET_NL()
	
	//Average Rank
	serverBD.iAverageRank = (serverBD.iAverageRank/serverBD.iNumParticpants)
	NET_PRINT("    ----->     HIDEOUT - STORE_PED_DIFFICULTY - iAverageRank = ") NET_PRINT_INT(serverBD.iAverageRank) NET_NL()
	
	//Accuracy
	serverBD.iAccuracy = (serverBD.iDifficultyFromPlayerStats/2)
	IF serverBD.iAccuracy > 40
		serverBD.iAccuracy = 40
	ENDIF
	NET_PRINT_TIME() NET_PRINT("    ----->     HIDEOUT - STORE_PED_DIFFICULTY - serverBD.iAccuracy = ") NET_PRINT_INT(serverBD.iAccuracy) NET_NL()
ENDPROC

//PURPOSE: Runs through the max num participants and does relevant checks
PROC MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER()
	
	//Check if we should recalculate difficulty because the number fo particpants has changed
	BOOL bCalculateDifficulty
	IF serverBD.eStage < HIDEOUT_STAGE_REWARDS_AND_FEEDACK
		IF serverBD.iNumParticpants != iNumParticipantsStored
			serverBD.iDifficultyFromPlayerStats = 0
			NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - NUM PARTICIPANTS HAS CHANGED - STORED = ") NET_PRINT_INT(iNumParticipantsStored) NET_PRINT(" NEW = ") NET_PRINT_INT(serverBD.iNumParticpants)NET_NL()
			iNumParticipantsStored = serverBD.iNumParticpants
			IF serverBD.iHighestNumParticpants < serverBD.iNumParticpants
				serverBD.iHighestNumParticpants = serverBD.iNumParticpants
			ENDIF
			serverBD.iAverageRank = 0
			bCalculateDifficulty = TRUE
		ENDIF
	ENDIF
	
	//SET_BIT(serverBD.iServerBitSet, biS_NoPackagesExist)
	serverBD.iNumParticpants = 0
	serverBD.iNumParticpantsInArea = 0
	INT iParticipant
	
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
			PLAYER_INDEX PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))
			//PED_INDEX PlayerPedId = GET_PLAYER_PED(PlayerId)
			
			serverBD.iNumParticpants++
			
			//Calculate Difficulty
			IF bCalculateDifficulty = TRUE
				CALCULATE_DIFFICULTY_ADDITION_FOR_PLAYER(PlayerId)
			ENDIF
			
			//Check for Warning Ped
			IF serverBD.iWarningPed = -1
				IF playerBD[iParticipant].iWarningPed != -1
					serverBD.iWarningPed = playerBD[iParticipant].iWarningPed
					PRINTLN("    ----->     HIDEOUT - SERVER - iWarningPed = ", serverBD.iWarningPed)
				ENDIF
			ENDIF
			
//			//Check if Warning Line 1 has been done
//			IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_WarningLine1Done)
//				IF IS_BIT_SET(playerBD[iParticipant].iPlayerBitSet, biP_WarningLine1Done)
//					SET_BIT(serverBD.iServerBitSet, biS_WarningLine1Done)
//					NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - biS_WarningLine1Done - TRUE    <----------     ") NET_NL()
//				ENDIF
//			ENDIF
//			//Check if Warning Line 2 has been done
//			IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_WarningLine2Done)
//				IF IS_BIT_SET(playerBD[iParticipant].iPlayerBitSet, biP_WarningLine2Done)
//					SET_BIT(serverBD.iServerBitSet, biS_WarningLine2Done)
//					NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - biS_WarningLine2Done - TRUE    <----------     ") NET_NL()
//				ENDIF
//			ENDIF
//			//Check if Attack Line has been done
//			IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_AttackLineDone)
//				IF IS_BIT_SET(playerBD[iParticipant].iPlayerBitSet, biP_AttackLineDone)
//					SET_BIT(serverBD.iServerBitSet, biS_AttackLineDone)
//					NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - biS_AttackLineDone - TRUE    <----------     ") NET_NL()
//				ENDIF
//			ENDIF
			
//			//Update Player Kill Points Total
//			IF serverBD.eHideoutVariation = HIDEOUT_TERRITORY_CONTROL
//				IF playerBD[iParticipant].iTotalKillPoints > serverBD.iPlayerKillPoints[iParticipant]
//					serverBD.iTotalPlayerKillPoints += (playerBD[iParticipant].iTotalKillPoints-serverBD.iPlayerKillPoints[iParticipant])
//					
//					IF serverBD.eHideoutVariation = HIDEOUT_TERRITORY_CONTROL
//						serverBD.TerritoryControlTimer.Timer = GET_TIME_OFFSET(serverBD.TerritoryControlTimer.Timer ,(-1*(playerBD[iParticipant].iTotalKillPoints-serverBD.iPlayerKillPoints[iParticipant])*2*1000))
//						NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - HIDEOUT_TERRITORY_CONTROL - KILL TIMER UPDATE    <----------     ") NET_NL()
//					ENDIF
//					
//					serverBD.iPlayerKillPoints[iParticipant] = playerBD[iParticipant].iTotalKillPoints
//				ENDIF
//			ENDIF
			
			/*IF serverBD.eHideoutVariation = HIDEOUT_PACKAGE_STEAL
				INT i
				REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons i
					IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.PickUpData[i].NetID)
						CLEAR_BIT(serverBD.iServerBitSet, biS_NoPackagesExist)
					ENDIF
					
//					//Check on delivering a package
//					//IF serverBD.eHideoutVariation = HIDEOUT_PACKAGE_STEAL
//						IF serverBD.PickUpData[i].iParticpantDeliveredPackage = -1
//							IF IS_BIT_SET(playerBD[iParticipant].iPlayerBitSet, biP_DeliveredPackage1+i)
//								serverBD.PickUpData[i].iParticpantDeliveredPackage = iParticipant
//								NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - DELIVERED PACKAGE ") NET_PRINT_INT(i) NET_PRINT(" Player ") NET_PRINT(GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(serverBD.PickUpData[i].iParticpantDeliveredPackage)))) NET_NL()
//							ENDIF
//						ENDIF
//					//ENDIF
//					
//					IF IS_NET_PLAYER_OK(PlayerId)
//						//Check on collecting a pick up
//						IF serverBD.PickUpData[i].iParticpantGotPickUp = -1
//							IF IS_BIT_SET(playerBD[iParticipant].iPlayerBitSet, biP_CollectedPickUp1+i)
//								serverBD.PickUpData[i].iParticpantGotPickUp = iParticipant
//								SET_BIT(serverBD.iServerBitSet, biS_MoveToFightStage)
//								NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - COLLECTED PACKAGE ") NET_PRINT_INT(i) NET_PRINT(" Player ") NET_PRINT(GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(serverBD.PickUpData[i].iParticpantGotPickUp)))) NET_NL()
//							ENDIF
//						ELSE
//							IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.PickUpData[i].NetID)
//								IF NOT IS_ENTITY_ATTACHED_TO_ANY_PED(NET_TO_OBJ(serverBD.PickUpData[i].NetID))
//									serverBD.PickUpData[i].iParticpantGotPickUp = -1
//									NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - DROPPED PACKAGE ") NET_PRINT_INT(i) NET_NL()
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
				ENDREPEAT
			ENDIF*/
			
//			//Check if a player has unlocked the Safe
//			IF serverBD.eHideoutVariation = HIDEOUT_KILL_PEDS
//				IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_SafeUnlocked)
//					IF IS_BIT_SET(playerBD[iParticipant].iPlayerBitSet, biP_HasUnlockedSafe)
//						//OPEN_SAFE_CRACK_DOOR(SafeCrackData)
//						SET_BIT(serverBD.iServerBitSet, biS_SafeUnlocked)
//						NET_PRINT_TIME() NET_PRINT( "  ---->  HIDEOUT - Safe has been unlocked - biS_SafeUnlocked SET")
//					ENDIF
//				ENDIF
//			ENDIF
			
//			IF IS_NET_PLAYER_OK(PlayerId)
				
				//Update number of players in the area
				/*IF serverBD.eHideoutVariation = HIDEOUT_TERRITORY_CONTROL
					IF NOT HAS_PED_LEFT_AREA(PlayerPedId)
						serverBD.iNumParticpantsInArea++
					ENDIF
				ENDIF*/
				
//				//Move on to Fight stage if player is in aggressive vehicle (helicopter, plane, police, etc)
//				IF playerBD[iParticipant].eStage = HIDEOUT_STAGE_WARNING
//					IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_MoveToFightStage)
//						IF IS_PED_IN_ANY_HELI(PlayerPedId)
//						OR IS_PED_IN_ANY_PLANE(PlayerPedId)
//						OR IS_PED_IN_ANY_POLICE_VEHICLE(PlayerPedId)
//							SET_BIT(serverBD.iServerBitSet, biS_MoveToFightStage)
//							NET_PRINT("    ----->     HIDEOUT - AGGRESSIVE VEHICLE - biS_MoveToFightStage") NET_NL()
//						ENDIF
//					ENDIF
//				ENDIF
//				
//				#IF IS_DEBUG_BUILD
//				//Debug Skip Checks
//				IF playerBD[iParticipant].eStage = HIDEOUT_STAGE_WARNING
//					IF playerBD[iParticipant].iDebugSkip = 1
//						SET_BIT(serverBD.iServerBitSet, biS_MoveToFightStage)
//						NET_PRINT("    ----->     HIDEOUT - SKIP CHECK - biS_MoveToFightStage") NET_NL()
//					ENDIF
//				ENDIF
//				IF playerBD[iParticipant].eStage = HIDEOUT_STAGE_PRIMARY
//					IF playerBD[iParticipant].iDebugSkip = 2
//						serverBD.iTotalKills = serverBD.iKillGoal
//						NET_PRINT("    ----->     HIDEOUT - SKIP CHECK - iTotalKills = iKillGoal") NET_NL()
//					ENDIF
//				ENDIF
//				
//				//Debug Fail Checks
//				IF playerBD[iParticipant].eStage = HIDEOUT_STAGE_WARNING
//				OR playerBD[iParticipant].eStage = HIDEOUT_STAGE_PRIMARY
//				OR playerBD[iParticipant].eStage = HIDEOUT_STAGE_SECONDARY
//					IF playerBD[iParticipant].iDebugPassFail = 1
//						serverBD.iTotalKills = serverBD.iKillGoal
//						serverBD.eStage = HIDEOUT_STAGE_REWARDS_AND_FEEDACK
//						NET_PRINT("    ----->     HIDEOUT - DEBUG CHECKS - S-PASS") NET_NL()
//					ELIF playerBD[iParticipant].iDebugPassFail = 2
//						serverBD.iServerGameState = GAME_STATE_END
//						NET_PRINT("    ----->     HIDEOUT - DEBUG CHECKS - F-FAIL") NET_NL()
//					ENDIF
//				ENDIF
//				#ENDIF
				
//			ENDIF
				
		ENDIF
	ENDREPEAT
	
	//Average Rank
	IF bCalculateDifficulty = TRUE
		STORE_PED_DIFFICULTY()
	ENDIF
	
	
	//STAGGERED LOOP
	//Reset Values
//	IF iServerStaggeredPedCount = 0
//		SET_BIT(serverBD.iServerBitSet, biS_NoPackagesExist)
//	ENDIF
	
	//Do Loop
	IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iServerStaggeredPedCount))
		PLAYER_INDEX PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iServerStaggeredPedCount))
		PED_INDEX PlayerPedId = GET_PLAYER_PED(PlayerId)
		
		
		//Check if Warning Ped First Task has been done
		IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_FirstPedTaskGiven)
			IF IS_BIT_SET(playerBD[iServerStaggeredPedCount].iPlayerBitSet, biP_FirstPedTaskGiven)
				SET_BIT(serverBD.iServerBitSet, biS_FirstPedTaskGiven)
				NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - biS_FirstPedTaskGiven - TRUE    <----------     ") NET_NL()
			ENDIF
		ENDIF
		//Check if Warning Line 1 has been done
		IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_WarningLine1Done)
			IF IS_BIT_SET(playerBD[iServerStaggeredPedCount].iPlayerBitSet, biP_WarningLine1Done)
				SET_BIT(serverBD.iServerBitSet, biS_WarningLine1Done)
				NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - biS_WarningLine1Done - TRUE    <----------     ") NET_NL()
			ENDIF
		ENDIF
		//Check if Warning Line 2 has been done
		IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_WarningLine2Done)
			IF IS_BIT_SET(playerBD[iServerStaggeredPedCount].iPlayerBitSet, biP_WarningLine2Done)
				SET_BIT(serverBD.iServerBitSet, biS_WarningLine2Done)
				NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - biS_WarningLine2Done - TRUE    <----------     ") NET_NL()
			ENDIF
		ENDIF
		//Check if Attack Line has been done
		IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_AttackLineDone)
			IF IS_BIT_SET(playerBD[iServerStaggeredPedCount].iPlayerBitSet, biP_AttackLineDone)
				SET_BIT(serverBD.iServerBitSet, biS_AttackLineDone)
				NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - biS_AttackLineDone - TRUE    <----------     ") NET_NL()
			ENDIF
		ENDIF
		
		//Update Player Kill Points Total
		/*IF serverBD.eHideoutVariation = HIDEOUT_TERRITORY_CONTROL
			IF playerBD[iServerStaggeredPedCount].iTotalKillPoints > serverBD.iPlayerKillPoints[iServerStaggeredPedCount]
				serverBD.iTotalPlayerKillPoints += (playerBD[iServerStaggeredPedCount].iTotalKillPoints-serverBD.iPlayerKillPoints[iServerStaggeredPedCount])
				
				IF serverBD.eHideoutVariation = HIDEOUT_TERRITORY_CONTROL
					serverBD.TerritoryControlTimer.Timer = GET_TIME_OFFSET(serverBD.TerritoryControlTimer.Timer ,(-1*(playerBD[iServerStaggeredPedCount].iTotalKillPoints-serverBD.iPlayerKillPoints[iServerStaggeredPedCount])*2*1000))
					NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - HIDEOUT_TERRITORY_CONTROL - KILL TIMER UPDATE    <----------     ") NET_NL()
				ENDIF
				
				serverBD.iPlayerKillPoints[iServerStaggeredPedCount] = playerBD[iServerStaggeredPedCount].iTotalKillPoints
			ENDIF
		ENDIF*/
		
		//Update Player Deaths
		/*IF serverBD.eHideoutVariation = HIDEOUT_TERRITORY_CONTROL
			IF playerBD[iServerStaggeredPedCount].iDeaths > serverBD.iPlayerDeaths[iServerStaggeredPedCount]
				serverBD.TerritoryControlTimer.Timer = GET_TIME_OFFSET(serverBD.TerritoryControlTimer.Timer ,((playerBD[iServerStaggeredPedCount].iDeaths-serverBD.iPlayerDeaths[iServerStaggeredPedCount])*2*10000))
				NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - HIDEOUT_TERRITORY_CONTROL - PLAYER DEATH TIMER UPDATE    <----------     ") NET_NL()
				
				serverBD.iPlayerDeaths[iServerStaggeredPedCount] = playerBD[iServerStaggeredPedCount].iDeaths
			ENDIF
		ENDIF*/
		
		/*IF serverBD.eHideoutVariation = HIDEOUT_PACKAGE_STEAL
			INT i
			REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons i
//				IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.PickUpData[i].NetID)
//					CLEAR_BIT(serverBD.iServerBitSet, biS_NoPackagesExist)
//				ENDIF
				
				//Check on delivering a package
				//IF serverBD.eHideoutVariation = HIDEOUT_PACKAGE_STEAL
					IF serverBD.PickUpData[i].iParticpantDeliveredPackage = -1
						IF IS_BIT_SET(playerBD[iServerStaggeredPedCount].iPlayerBitSet, biP_DeliveredPackage1+i)
							serverBD.PickUpData[i].iParticpantDeliveredPackage = iServerStaggeredPedCount
							NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - DELIVERED PACKAGE ") NET_PRINT_INT(i) NET_PRINT(" Player ") NET_PRINT(GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(serverBD.PickUpData[i].iParticpantDeliveredPackage)))) NET_NL()
						ENDIF
					ENDIF
				//ENDIF
				
				IF IS_NET_PLAYER_OK(PlayerId)
					//Check on collecting a pick up
					IF serverBD.PickUpData[i].iParticpantGotPickUp = -1
						IF IS_BIT_SET(playerBD[iServerStaggeredPedCount].iPlayerBitSet, biP_CollectedPickUp1+i)
							serverBD.PickUpData[i].iParticpantGotPickUp = iServerStaggeredPedCount
							SET_BIT(serverBD.iServerBitSet, biS_MoveToFightStage)
							NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - COLLECTED PACKAGE ") NET_PRINT_INT(i) NET_PRINT(" Player ") NET_PRINT(GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(serverBD.PickUpData[i].iParticpantGotPickUp)))) NET_NL()
						ENDIF
					ELSE
						IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.PickUpData[i].NetID)
							IF NOT IS_ENTITY_ATTACHED_TO_ANY_PED(NET_TO_OBJ(serverBD.PickUpData[i].NetID))
								serverBD.PickUpData[i].iParticpantGotPickUp = -1
								NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - DROPPED PACKAGE ") NET_PRINT_INT(i) NET_NL()
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF*/
		
		//Check if a player has unlocked the Safe
		/*IF serverBD.eHideoutVariation = HIDEOUT_KILL_PEDS
		//OR serverBD.eHideoutVariation = HIDEOUT_PACKAGE_STEAL
			IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_SafeUnlocked)
				IF IS_BIT_SET(playerBD[iServerStaggeredPedCount].iPlayerBitSet, biP_HasUnlockedSafe)
					//OPEN_SAFE_CRACK_DOOR(SafeCrackData)
					SET_BIT(serverBD.iServerBitSet, biS_SafeUnlocked)
					NET_PRINT_TIME() NET_PRINT( "  ---->  HIDEOUT - Safe has been unlocked - biS_SafeUnlocked SET")
				ENDIF
			ENDIF
		ENDIF*/
		
		IF IS_NET_PLAYER_OK(PlayerId)
						
			//Move on to Fight stage if player is in aggressive vehicle (helicopter, plane, police, etc)
			IF playerBD[iServerStaggeredPedCount].eStage = HIDEOUT_STAGE_WARNING
				IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_MoveToFightStage)
					IF IS_PED_IN_ANY_HELI(PlayerPedId)
					OR IS_PED_IN_ANY_PLANE(PlayerPedId)
					OR IS_PED_IN_ANY_POLICE_VEHICLE(PlayerPedId)
						SET_BIT(serverBD.iServerBitSet, biS_MoveToFightStage)
						NET_PRINT("    ----->     HIDEOUT - AGGRESSIVE VEHICLE - biS_MoveToFightStage") NET_NL()
					ENDIF
				ENDIF
			ENDIF
			
			#IF IS_DEBUG_BUILD
			//Debug Skip Checks
			IF playerBD[iServerStaggeredPedCount].eStage = HIDEOUT_STAGE_WARNING
				IF playerBD[iServerStaggeredPedCount].iDebugSkip = 1
					SET_BIT(serverBD.iServerBitSet, biS_MoveToFightStage)
					NET_PRINT("    ----->     HIDEOUT - SKIP CHECK - biS_MoveToFightStage") NET_NL()
				ENDIF
			ENDIF
			IF playerBD[iServerStaggeredPedCount].eStage = HIDEOUT_STAGE_PRIMARY
				IF playerBD[iServerStaggeredPedCount].iDebugSkip = 2
					serverBD.iTotalKills = serverBD.iKillGoal
					NET_PRINT("    ----->     HIDEOUT - SKIP CHECK - iTotalKills = iKillGoal") NET_NL()
				ENDIF
			ENDIF
			
			//Debug Fail Checks
			IF playerBD[iServerStaggeredPedCount].eStage = HIDEOUT_STAGE_WARNING
			OR playerBD[iServerStaggeredPedCount].eStage = HIDEOUT_STAGE_PRIMARY
			OR playerBD[iServerStaggeredPedCount].eStage = HIDEOUT_STAGE_SECONDARY
				IF playerBD[iServerStaggeredPedCount].iDebugPassFail = 1
					serverBD.iTotalKills = serverBD.iKillGoal
					SET_GA_PICKUP_DATA()
					serverBD.eStage = HIDEOUT_STAGE_REWARDS_AND_FEEDACK
					NET_PRINT("    ----->     HIDEOUT - DEBUG CHECKS - S-PASS") NET_NL()
				ELIF playerBD[iServerStaggeredPedCount].iDebugPassFail = 2
					serverBD.iServerGameState = GAME_STATE_END
					NET_PRINT("    ----->     HIDEOUT - DEBUG CHECKS - F-FAIL") NET_NL()
				ENDIF
			ENDIF
			#ENDIF
			
		ENDIF
			
	ENDIF
	
	//Restart Loop
	iServerStaggeredPedCount++
    IF iServerStaggeredPedCount >= NETWORK_GET_MAX_NUM_PARTICIPANTS()
        iServerStaggeredPedCount = 0
    ENDIF
ENDPROC

//PURPOES: Warps the player to the Gang Hideout if they aren't in the area
FUNC BOOL WARP_TO_GANG_HIDEOUT()
	RETURN TRUE		// url:bugstar:5028799 - Hacker Truck - Player was stuck on a black loading screen after trying to enter the Nerve Center.
					// Just returning true after discussing with Bobby, don't think we ever wanted this system warping players
	
	IF IS_NET_PLAYER_OK(PLAYER_ID())	
		SWITCH iInitialWarpStage
			CASE 0
				IF HAS_PED_LEFT_AREA(PLAYER_PED_ID(), TRUE)
				AND (IS_SCREEN_FADED_OUT() OR IS_SCREEN_FADING_OUT())
					//IF IS_SCREEN_FADED_OUT()
						SET_MISSION_SPAWN_ANGLED_AREA(serverBD.PlayerSpawnArea.vMin, serverBD.PlayerSpawnArea.vMax, serverBD.PlayerSpawnArea.fWidth)
						SET_MISSION_SPAWN_OCCLUSION_ANGLED_AREA(serverBD.MainArea.vMin, serverBD.MainArea.vMax, serverBD.MainArea.fWidth)
						SET_PLAYER_WILL_SPAWN_FACING_COORDS(serverBD.vHideoutCentre, TRUE, FALSE)
						iInitialWarpStage = 1
						NET_PRINT("    ----->     HIDEOUT - WARP_TO_GANG_HIDEOUT - iInitialWarpStage = 1 ") NET_NL()
					//ELIF NOT IS_SCREEN_FADING_OUT()
					//	DO_SCREEN_FADE_OUT(500)
					//	NET_PRINT("    ----->     HIDEOUT - WARP_TO_GANG_HIDEOUT - DO SCREEN FADE OUT ") NET_NL()
					//ENDIF
				ELSE
					NET_PRINT("    ----->     HIDEOUT - WARP_TO_GANG_HIDEOUT - IN AREA ALREADY ") NET_NL()
					RETURN TRUE
				ENDIF
			BREAK
			
			CASE 1
				//IF NET_WARP_TO_COORD(g_FMMC_STRUCT.vStartPos, GET_HEADING_FROM_VECTOR_2D(serverBD.vHideoutCentre.x-g_FMMC_STRUCT.vStartPos.x,serverBD.vHideoutCentre.y-g_FMMC_STRUCT.vStartPos.y), FALSE, TRUE)
				IF WARP_TO_SPAWN_LOCATION()
					//SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					iInitialWarpStage = 2
					NET_PRINT("    ----->     HIDEOUT - WARP_TO_GANG_HIDEOUT - WARP DONE ") NET_NL()
					RETURN TRUE
				ELSE
					NET_PRINT("    ----->     HIDEOUT - WARP_TO_GANG_HIDEOUT - DOING WARP ") NET_NL()
				ENDIF
			BREAK
			
			CASE 2		NET_PRINT("    ----->     HIDEOUT - WARP_TO_GANG_HIDEOUT - DONE 2 ") NET_NL()		RETURN TRUE
			DEFAULT		NET_PRINT("    ----->     HIDEOUT - WARP_TO_GANG_HIDEOUT - DONE DEFAULT ") NET_NL()	RETURN TRUE
		ENDSWITCH
	ENDIF
	
	RETURN FALSE
ENDFUNC

// *********************************** \\
//				FUNCTIONS			   \\
// *********************************** \\

////PURPOSE: Sets the Mission Data as a test before the MIssion Creator is setup.
//PROC SET_TEMP_MISSION_DATA()
//	g_FMMC_STRUCT.tl63MissionName = "HIDE1"
//	//g_FMMC_STRUCT.iAreaTriggered = 1
//	g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION_CTF
//	//g_FMMC_STRUCT.iRespawnTime = 2
//	g_FMMC_STRUCT.iDifficulity = FMMC_DIFFICULTY_MEDIUM
//	g_FMMC_STRUCT.iNumberOfTeams = 1
//	g_FMMC_STRUCT.iMaxNumberOfTeams = 1
//	g_FMMC_STRUCT.iNumberOfLives = NUMBER_OF_LIVES_UNLIMITED
//	//g_FMMC_STRUCT.iDMPedRule
//	serverBD.vHideoutCentre = <<-1092.8593, -1633.6359, 6.7641>>
//	g_FMMC_STRUCT.fStartRadius = 30
//	//g_FMMC_STRUCT.sFMMCEndConditions[0].iNumberOfPedKillsRule
//	g_FMMC_STRUCT_ENTITIES.iPedRespawnTime = FMMC_PED_RESPAWN_TIME_4_SECONDS
//	g_FMMC_STRUCT_ENTITIES.iNumberOfPeds = MAX_NUM_PEDS
//	//g_FMMC_STRUCT_ENTITIES.iPedPallet
//	
//	//PEDS
//	g_FMMC_STRUCT_ENTITIES.sPlacedPed[0].vPos	= <<-1080.3209, -1677.8591, 3.5754>>
//	g_FMMC_STRUCT_ENTITIES.sPlacedPed[0].fHead	= 305.0289
//	g_FMMC_STRUCT_ENTITIES.sPlacedPed[1].vPos	= <<-1069.5006, -1662.0159, 3.4507>>
//	g_FMMC_STRUCT_ENTITIES.sPlacedPed[1].fHead	= 130.1648
//	g_FMMC_STRUCT_ENTITIES.sPlacedPed[2].vPos	= <<-1092.8593, -1633.6359, 6.7641>>
//	g_FMMC_STRUCT_ENTITIES.sPlacedPed[2].fHead	= 138.6005
//	g_FMMC_STRUCT_ENTITIES.sPlacedPed[3].vPos	= <<-1099.3181, -1651.4531, 3.3987>>
//	g_FMMC_STRUCT_ENTITIES.sPlacedPed[3].fHead	= 340.3410
//	
//	INT i
//	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfPeds i
//		g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].mn			=	MP_G_M_PROS_01
//		g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].gun			=	WEAPONTYPE_PISTOL
//		g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].skill		=	CAL_AVERAGE
//		g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iRespawn	=	serverBD.iKillGoal
//		//g_FMMC_STRUCT_ENTITIES.sPlacedPed[0].fRange
//		//g_FMMC_STRUCT_ENTITIES.sPlacedPed[0].iTeam
//		//g_FMMC_STRUCT_ENTITIES.sPlacedPed[0].iRule
//		//g_FMMC_STRUCT_ENTITIES.sPlacedPed[0].iPriority
//		//g_FMMC_STRUCT_ENTITIES.sPlacedPed[0].iNumberOfTasks
//		//g_FMMC_STRUCT_ENTITIES.sPlacedPed[0].sPedTask
//	ENDREPEAT
//	
//	NET_PRINT("    ----->     HIDEOUT - SET_TEMP_MISSION_DATA") NET_NL()
//ENDPROC

//PURPOSE: Controls how many entities are reserved based on serverBD
PROC MAINTAIN_RESERVED_ENTITIES()
	INT iCurrentlyReserved = GET_NUM_RESERVED_MISSION_VEHICLES(FALSE)
	IF serverBD.iReservedVehicles != iCurrentlyReserved
		IF serverBD.iReservedVehicles < iCurrentlyReserved
		OR CAN_RESERVE_NETWORK_VEHICLES_FOR_THIS_SCRIPT(serverBD.iReservedVehicles, FALSE, TRUE)
			RESERVE_NETWORK_MISSION_VEHICLES(serverBD.iReservedVehicles)
			NET_PRINT_TIME() NET_PRINT("    ----->     HIDEOUT - MAINTAIN_RESERVED_ENTITIES - RESERVED VECHILES = ") NET_PRINT_INT(serverBD.iReservedVehicles) NET_NL()
		ENDIF
	ENDIF
	
	iCurrentlyReserved = GET_NUM_RESERVED_MISSION_PEDS(FALSE)
	IF serverBD.iReservedPeds != iCurrentlyReserved
		IF serverBD.iReservedPeds < iCurrentlyReserved
		OR CAN_RESERVE_NETWORK_PEDS_FOR_THIS_SCRIPT(serverBD.iReservedPeds, FALSE, TRUE)
			RESERVE_NETWORK_MISSION_PEDS(serverBD.iReservedPeds)
			NET_PRINT_TIME() NET_PRINT("    ----->     HIDEOUT - MAINTAIN_RESERVED_ENTITIES - RESERVED PEDS = ") NET_PRINT_INT(serverBD.iReservedPeds) NET_NL()
		ENDIF
	ENDIF
	
	iCurrentlyReserved = GET_NUM_RESERVED_MISSION_OBJECTS(FALSE)
	IF serverBD.iReservedObjects != iCurrentlyReserved
		IF serverBD.iReservedObjects < iCurrentlyReserved
		OR CAN_RESERVE_NETWORK_OBJECTS_FOR_THIS_SCRIPT(serverBD.iReservedObjects, FALSE, TRUE)
			RESERVE_NETWORK_MISSION_OBJECTS(serverBD.iReservedObjects)
			NET_PRINT_TIME() NET_PRINT("    ----->     HIDEOUT - MAINTAIN_RESERVED_ENTITIES - RESERVED OBJECTS = ") NET_PRINT_INT(serverBD.iReservedObjects) NET_NL()
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Returns how many players are within 200m
FUNC INT GET_NUMBER_OF_PLAYERS_IN_RANGE()
	INT iPlayer
	INT iCount
	
	#IF IS_DEBUG_BUILD
	PRINTLN("    ----->     HIDEOUT - GET_NUMBER_OF_PLAYERS_IN_RANGE - serverBD.vHideoutCentre = ", serverBD.vHideoutCentre, " RADIUS CHECK = ", 200)
	VECTOR vPlayer = GET_PLAYER_COORDS(PLAYER_ID())
	PRINTLN("    ----->     HIDEOUT - GET_NUMBER_OF_PLAYERS_IN_RANGE - LOCAL PLAYER COORDS = ", vPlayer, " DISTANCE = ", GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID()), serverBD.vHideoutCentre))
	#ENDIF
	
	REPEAT NUM_NETWORK_PLAYERS iPlayer
		PLAYER_INDEX PlayerId = INT_TO_NATIVE(PLAYER_INDEX, iPlayer)

		IF IS_NET_PLAYER_OK(PlayerId)
			IF IS_ENTITY_AT_COORD(GET_PLAYER_PED(PlayerId), serverBD.vHideoutCentre, <<200, 200, 200>>)
				iCount++
			ENDIF
		ENDIF
	ENDREPEAT
	
	NET_PRINT("    ----->     HIDEOUT - GET_NUMBER_OF_PLAYERS_IN_RANGE - iCount = ") NET_PRINT_INT(iCount) NET_NL()
	RETURN iCount
ENDFUNC

/// PURPOSE:
///    Process pre game logic. Called once at beginning of script.
/// PARAMS:
///    structHideoutMissionData - mission data.
/// RETURNS:
///    FALSE if the script fails to receive an initial network broadcast.
FUNC BOOL PROCESS_PRE_GAME(MP_MISSION_DATA &structHideoutMissionData)
	
	//SET_TEMP_MISSION_DATA()
	
	FMMC_PROCESS_PRE_GAME_COMMON(structHideoutMissionData, serverBD.sFMMC_SBD.iPlayerMissionToLoad, serverBD.sFMMC_SBD.iMissionVariation)
	GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_GANGHIDEOUT
	//RESERVE_NETWORK_MISSION_OBJECTS(g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons+1)//g_FMMC_STRUCT_ENTITIES.iNumberOfProps)
	//NET_PRINT("    ----->     HIDEOUT - NUM OBJECTS RESEVERD = ") NET_PRINT_INT(g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons) NET_NL()
	//NET_PRINT("    ----->     HIDEOUT - HEALTH PICKUPS = ") NET_PRINT_INT(g_FMMC_STRUCT_ENTITIES.iNumberOfProps) NET_NL()
	
	//Cap Numbers
	IF g_FMMC_STRUCT_ENTITIES.iNumberOfPeds > MAX_NUM_PEDS
		g_FMMC_STRUCT_ENTITIES.iNumberOfPeds = MAX_NUM_PEDS
		NET_PRINT("    ----->     HIDEOUT - g_FMMC_STRUCT_ENTITIES.iNumberOfPeds = MAX_NUM_PEDS") NET_NL()
	ENDIF
	/*IF g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons > MAX_NUM_PICKUPS
		g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons = MAX_NUM_PICKUPS
		NET_PRINT("    ----->     HIDEOUT - g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons = MAX_NUM_PICKUPS") NET_NL()
	ENDIF*/
	/*IF g_FMMC_STRUCT_ENTITIES.iNumberOfProps > MAX_NUM_HEALTH_PICKUPS
		g_FMMC_STRUCT_ENTITIES.iNumberOfProps = MAX_NUM_HEALTH_PICKUPS
		NET_PRINT("    ----->     HIDEOUT - g_FMMC_STRUCT_ENTITIES.iNumberOfProps = MAX_NUM_HEALTH_PICKUPS") NET_NL()
	ENDIF*/
		
	//SET_FM_MISSION_LAUNCHED_SUCESS(structHideoutMissionData.iFMCreatorID, structHideoutMissionData.mdID.idVariation, structHideoutMissionData.iInstanceId)
	
	SET_GAME_STATE_ON_DEATH(AFTERLIFE_JUST_REJOIN)
	
	SET_CLIENT_GAME_STATE(GAME_STATE_INI)
	
	//CREATE_FM_MISSION_RELATIONSHIP_GROUPS()
	
	SET_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_GANGHIDEOUT, TRUE)
	
	//SET_PLAYER_TEAM(PLAYER_ID(), g_FMMC_STRUCT.iMyTeam)
	//SET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID(), rgfm_PlayerTeam[g_FMMC_STRUCT.iMyTeam])
	//SET_PED_CAN_BE_TARGETTED_BY_TEAM(PLAYER_PED_ID(), GET_PLAYER_TEAM(PLAYER_ID()), FALSE)
	//DISABLE_MP_PASSIVE_MODE()
	//NETWORK_SET_FRIENDLY_FIRE_OPTION(FALSE)
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		CALCULATE_DIFFICULTY_ADDITION_FOR_PLAYER(PLAYER_ID())
	ENDIF
	
	//SET_BIT(iBoolsBitSet, biAnyPackageDropped)
	
	//ALL PLAYERS SET STANDARD SERVER BD
	serverBD.vHideoutBlipLocation = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[0].vloc[0]	//g_FMMC_STRUCT.vStartPos
	NET_PRINT("    ----->     HIDEOUT - vHideoutBlipLocation = Start = ") NET_PRINT_VECTOR(serverBD.vHideoutBlipLocation) NET_NL()
	
	IF NOT ARE_VECTORS_EQUAL(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[0].vloc[0], <<0, 0, 0>>)
		serverBD.vHideoutCentre = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[0].vloc[0]
		NET_PRINT("    ----->     HIDEOUT - vHideoutCentre = GoTo = ") NET_PRINT_VECTOR(serverBD.vHideoutCentre) NET_NL()
	//ELSE
	//	serverBD.vHideoutCentre = g_FMMC_STRUCT.vStartPos
	//	NET_PRINT("    ----->     HIDEOUT - vHideoutCentre = Start = ") NET_PRINT_VECTOR(serverBD.vHideoutCentre) NET_NL()
	ENDIF
	IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[0].fRadius[0] > 15
		serverBD.fHideoutRadius = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[0].fRadius[0]
		NET_PRINT("    ----->     HIDEOUT - fHideoutRadius = GoTo = ") NET_PRINT_FLOAT(serverBD.fHideoutRadius) NET_NL()
	ELSE
		serverBD.fHideoutRadius = LEAVE_AREA_RANGE
		NET_PRINT("    ----->     HIDEOUT - fHideoutRadius = LEAVE_AREA_RANGE = ") NET_PRINT_FLOAT(serverBD.fHideoutRadius) NET_NL()
	ENDIF
	SET_HIDEOUT_ANGLED_AREAS()
	
	INT i
	REPEAT MAX_NUM_PEDS i
		IF GET_INTERIOR_AT_COORDS(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].vPos) != NULL
			serverBD.PedData[i].bRespawnInInterior = TRUE
			NET_PRINT("    ----->     HIDEOUT - bRespawnInInterior = TRUE,  g_FMMC_STRUCT_ENTITIES.sPlacedPed[") NET_PRINT_INT(i) NET_PRINT("].vPos = ") NET_PRINT_VECTOR(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].vPos) NET_NL()
		ENDIF
	ENDREPEAT
	
	IF g_FMMC_STRUCT.mnGangHideoutPed = DUMMY_MODEL_FOR_SCRIPT
		serverBD.PedGangModel = GET_GANG_MODEL_FOR_AREA(serverBD.vHideoutCentre)
		NET_PRINT("    ----->     HIDEOUT - USE GANG MODEL FOR AREA - MODEL = ") NET_PRINT(GET_MODEL_NAME_FOR_DEBUG(serverBD.PedGangModel)) NET_NL()
	ELSE
		serverBD.PedGangModel = g_FMMC_STRUCT.mnGangHideoutPed
		NET_PRINT("    ----->     HIDEOUT - USE CREATOR SET MODEL - MODEL = ") NET_PRINT(GET_MODEL_NAME_FOR_DEBUG(serverBD.PedGangModel)) NET_NL()
	ENDIF
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))
	
	// KGM: Wait for the first network broadcast before moving on
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		RETURN FALSE
	ENDIF
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		PLAYSTATS_CREATE_MATCH_HISTORY_ID_2(serverBD.iPlayStatsID[0], serverBD.iPlayStatsID[1])

		IF g_FMMC_STRUCT.iPedDensity <= 2
			serverBD.iKillGoal = (5*GET_RANDOM_INT_IN_RANGE(3, 7)) 	// 15 to 30
			NET_PRINT("    ----->     HIDEOUT - serverBD.iKillGoal = RANDOM") NET_NL()
		ELSE
			serverBD.iKillGoal = g_FMMC_STRUCT.iPedDensity
		ENDIF
		NET_PRINT("    ----->     HIDEOUT - serverBD.iKillGoal = ") NET_PRINT_INT(serverBD.iKillGoal) NET_NL()
		
		//initialise tracked points to value of -1
		INITIALISE_TRACKED_POINTS()
		
		//Multiply Kill Goal
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSet, ciPLAYER_NUM_MULTIPLIER) 
			NET_PRINT("    ----->     HIDEOUT - ciPLAYER_NUM_MULTIPLIER SET - CALCULATE NEW KILL GOAL") NET_NL()
			INT iPlayerInRange = GET_NUMBER_OF_PLAYERS_IN_RANGE()
			IF iPlayerInRange > 1
				serverBD.iKillGoal = (serverBD.iKillGoal * iPlayerInRange)
				NET_PRINT("    ----->     HIDEOUT - PLAYERS IN RANGE MULTIPLIER - serverBD.iKillGoal = ") NET_PRINT_INT(serverBD.iKillGoal) NET_NL()
			ENDIF
		ENDIF
		IF g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles > MAX_NUM_VEH
			g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles = MAX_NUM_VEH
		ENDIF
		serverBD.iReservedVehicles = g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles
		serverBD.iReservedPeds = MAX_NUM_PEDS
		serverBD.iReservedObjects = MAX_NUM_DESTRUCTIBLE_CRATES
		
		SET_HIDEOUT_VARIATION()
		GET_DESTRUCTIBLE_CRATE_LOCATION()
		SET_PED_GANG_ID()
		
		/*//SAFE CRACKING DATA
		IF serverBD.eHideoutVariation = HIDEOUT_KILL_PEDS
		//OR serverBD.eHideoutVariation = HIDEOUT_PACKAGE_STEAL
			SafeCrackData.vSafeCoord 				= g_FMMC_STRUCT_ENTITIES.sPlacedObject[0].vPos        	//g_FMMC_STRUCT_ENTITIES.sPlacedProp[0].vPos
			SafeCrackData.fSafeHeading				= g_FMMC_STRUCT_ENTITIES.sPlacedObject[0].fHead      	//g_FMMC_STRUCT_ENTITIES.sPlacedProp[0].fHead 
			SafeCrackData.vSafeCoord 				= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(SafeCrackData.vSafeCoord, SafeCrackData.fSafeHeading, <<0.40, -0.46, -0.47>>)
			
			serverBD.SafeData.vSafeDoor				= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(SafeCrackData.vSafeCoord, SafeCrackData.fSafeHeading, <<0, 0, 0.1>>)
			serverBD.SafeData.fSafeDoorHeading		= SafeCrackData.fSafeHeading
			
			SafeCrackData.vPlayerSafeCrackCoord 	= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(SafeCrackData.vSafeCoord, SafeCrackData.fSafeHeading, <<-0.55, -0.55, 0>>)//<<0.24, -0.85, 0>>)	//<<974.460, -1838.990, 35.123>>
			SafeCrackData.fPlayerSafeCrackHeading 	= SafeCrackData.fSafeHeading	//185.6107
			NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - SafeCrackData.vPlayerSafeCrackCoord ") NET_PRINT_VECTOR(SafeCrackData.vPlayerSafeCrackCoord) NET_NL()
			
			SafeCrackData.vFloatingHelpMin			= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(SafeCrackData.vSafeCoord, SafeCrackData.fSafeHeading, <<-5, -5, -1.0>>)	//<<<964.221985,-1835.220825,35.106377>>
			SafeCrackData.vFloatingHelpMax			= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(SafeCrackData.vSafeCoord, SafeCrackData.fSafeHeading, <<5, 5, 2.0>>)	//<<<979.696716,-1836.617310,37.533894>>
			SafeCrackData.fFloatingHelpWidth		= 8.5
			NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - SafeCrackData.vFloatingHelpMin ") NET_PRINT_VECTOR(SafeCrackData.vFloatingHelpMin) NET_NL()
			NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - SafeCrackData.vFloatingHelpMax ") NET_PRINT_VECTOR(SafeCrackData.vFloatingHelpMax) NET_NL()
			
			SafeCrackData.vCameraPos				= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(SafeCrackData.vSafeCoord, SafeCrackData.fSafeHeading, <<1.4, -1.2, 0.75+0.5>>)//<<0.87, -1.15, 1>>)	//<<<973.353027,-1838.694946,36.330345>>
			SafeCrackData.fCameraFOV				= 60.0
			
			SafeCrackData.vSafeDoorProp				= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(SafeCrackData.vSafeCoord, SafeCrackData.fSafeHeading, <<0.32, -0.34, -0.68>>)	//<<973.9, -1839.5, 35.9>>
			//SafeCrackData.iSafeDoorEnumHash			= DOOR_VAGOS_SAFE
			
			CLEAR_AREA(SafeCrackData.vSafeCoord, 2, FALSE, FALSE, FALSE, TRUE)
			*/
		//VEHICLE DATA
		/*ELIF serverBD.eHideoutVariation = HIDEOUT_TERRITORY_CONTROL
			IF MPGlobalsAmbience.gCarModelOnList != DUMMY_MODEL_FOR_SCRIPT
				serverBD.VehicleData.Model = MPGlobalsAmbience.gCarModelOnList
				MPGlobalsAmbience.gCarModelOnList = DUMMY_MODEL_FOR_SCRIPT
			ENDIF
			serverBD.VehicleData.vCoord = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[0].vPos
			serverBD.VehicleData.fHeading = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[0].fHead*/
		//ENDIF
		
		
	//CLIENT DATA
	/*ELSE
		
		//SAFE CRACKING DATA
		IF serverBD.eHideoutVariation = HIDEOUT_KILL_PEDS
		//OR serverBD.eHideoutVariation = HIDEOUT_PACKAGE_STEAL
			SafeCrackData.vSafeCoord 				= g_FMMC_STRUCT_ENTITIES.sPlacedObject[0].vPos       	//g_FMMC_STRUCT_ENTITIES.sPlacedProp[0].vPos 
			SafeCrackData.fSafeHeading				= g_FMMC_STRUCT_ENTITIES.sPlacedObject[0].fHead       	//g_FMMC_STRUCT_ENTITIES.sPlacedProp[0].fHead
			SafeCrackData.vSafeCoord 				= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(SafeCrackData.vSafeCoord, SafeCrackData.fSafeHeading, <<0.40, -0.46, -0.47>>)
			
			SafeCrackData.vPlayerSafeCrackCoord 	= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(SafeCrackData.vSafeCoord, SafeCrackData.fSafeHeading, <<-0.25, -0.75, 0>>)//<<0.24, -0.85, 0>>)	//<<974.460, -1838.990, 35.123>>
			SafeCrackData.fPlayerSafeCrackHeading 	= SafeCrackData.fSafeHeading	//185.6107
			NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - SafeCrackData.vPlayerSafeCrackCoord ") NET_PRINT_VECTOR(SafeCrackData.vPlayerSafeCrackCoord) NET_NL()
			
			SafeCrackData.vFloatingHelpMin			= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(SafeCrackData.vSafeCoord, SafeCrackData.fSafeHeading, <<-5, -5, -1.0>>)	//<<<964.221985,-1835.220825,35.106377>>
			SafeCrackData.vFloatingHelpMax			= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(SafeCrackData.vSafeCoord, SafeCrackData.fSafeHeading, <<5, 5, 2.0>>)	//<<<979.696716,-1836.617310,37.533894>>
			SafeCrackData.fFloatingHelpWidth		= 8.5
			NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - SafeCrackData.vFloatingHelpMin ") NET_PRINT_VECTOR(SafeCrackData.vFloatingHelpMin) NET_NL()
			NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - SafeCrackData.vFloatingHelpMax ") NET_PRINT_VECTOR(SafeCrackData.vFloatingHelpMax) NET_NL()
			
			SafeCrackData.vCameraPos				= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(SafeCrackData.vSafeCoord, SafeCrackData.fSafeHeading, <<1.4, -1.2, 0.75+0.5>>)//<<0.87, -1.15, 1>>)	//<<<973.353027,-1838.694946,36.330345>>
			SafeCrackData.fCameraFOV				= 60.0
			
			SafeCrackData.vSafeDoorProp				= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(SafeCrackData.vSafeCoord, SafeCrackData.fSafeHeading, <<0.32, -0.34, -0.68>>)	//<<973.9, -1839.5, 35.9>>
			//SafeCrackData.iSafeDoorEnumHash			= DOOR_VAGOS_SAFE
		ENDIF*/
	ENDIF
	
	//CREATE_SAFE_CRACK_SEQUENCES(SafeCrackData)
	
	ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, FALSE)
	ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, FALSE)
	SET_AUDIO_FLAG("DisableFlightMusic", TRUE)
	SET_AUDIO_FLAG("WantedMusicDisabled", TRUE)
	
	
	MPGlobals.bRunningGangHideout = TRUE	//For Blips to check if they should still use player colour
	PRINTLN("GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_GANGHIDEOUT")
	PRINTLN("GlobalplayerBD_FM[iMyGBD].currentMissionData.mdID.idVariation = ", GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.mdID.idVariation)	
	/*IF NOT IS_SCREEN_FADED_IN()
	AND NOT IS_SCREEN_FADING_IN()
		DO_SCREEN_FADE_IN(500)
		NET_PRINT("    ----->     HIDEOUT - PROCESS_PRE_GAME - FADE IN ") NET_NL()
	ENDIF*/
		
	//Set player spawn area
	//IF serverBD.eHideoutVariation != HIDEOUT_PACKAGE_STEAL
		SET_MISSION_SPAWN_ANGLED_AREA(serverBD.PlayerSpawnArea.vMin, serverBD.PlayerSpawnArea.vMax, serverBD.PlayerSpawnArea.fWidth)
		SET_MISSION_SPAWN_OCCLUSION_ANGLED_AREA(serverBD.MainArea.vMin, serverBD.MainArea.vMax, serverBD.MainArea.fWidth)
		//SET_MISSION_SPAWN_SPHERE(serverBD.vHideoutCentre, serverBD.fHideoutRadius*1.15)
		//SET_MISSION_SPAWN_OCCLUSION_SPHERE(serverBD.vHideoutCentre, serverBD.fHideoutRadius*0.5) // added by NeilF 15/10/2012 - to fix PT 864184
		SET_PLAYER_WILL_SPAWN_FACING_COORDS(serverBD.vHideoutCentre, TRUE, FALSE)
	//ELSE
	/*IF serverBD.eHideoutVariation = HIDEOUT_PACKAGE_STEAL
		SET_MAX_NUM_PORTABLE_PICKUPS_CARRIED_BY_PLAYER(serverBD.PickUpData[0].Model, 1)
	ENDIF*/
	
	MAINTAIN_RESERVED_ENTITIES()
	
	IF IS_POINT_IN_ANGLED_AREA(<<-475.1000, -1686.1000, 28.1030>>, serverBD.MainArea.vMin, serverBD.MainArea.vMax, serverBD.MainArea.fWidth)
		iNavmeshBlock = ADD_NAVMESH_BLOCKING_OBJECT(<<-475.1000, -1686.1000, 28.1030>>, <<2.5, 2.5, 2.5>>, 0)
		NET_PRINT("    ----->     HIDEOUT - PROCESS_PRE_GAME - ADD_NAVMESH_BLOCKING_OBJECT - WATER TOWER AT <<-475.1000, -1686.1000, 28.1030>>") NET_NL()
	ENDIF
	
	NET_PRINT("    ----->     HIDEOUT - PROCESS_PRE_GAME - DONE - KK") NET_NL()
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Performs script cleanup. Should be called upon script termination.
PROC SCRIPT_CLEANUP()
	IF DOES_CAM_EXIST(camEndScreen)
		DESTROY_CAM(camEndScreen, TRUE)
		RENDER_SCRIPT_CAMS(FALSE, TRUE)
	ENDIF
	
	SET_CELEB_BLOCK_RESURRECT_RENDERPHASE_UNPAUSE(FALSE)
	
	// Ensure screen is faded in
	/*IF NOT IS_SCREEN_FADED_IN()
	AND NOT IS_SCREEN_FADING_IN()
		DO_SCREEN_FADE_IN(500)
	ENDIF*/
	
	STOP_STREAM()
	
	//SET_LOCAL_PLAYER_CAN_COLLECT_PORTABLE_PICKUPS(TRUE)
	
	// DO NOT RESET ANY GLOBAL DEATHMATCH VARIABLES IN HERE 
		
	SET_PLAYER_LOCKON(PLAYER_ID(), TRUE)
	SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableLockonToRandomPeds, FALSE)
	
	//Store GLobal if player Quit via Store (Bug 2002718)
	IF SHOULD_TRANSITION_SESSIONS_RESTART_FM_AFTER_END_RESERVING_SLOT()
		g_gaContentIdHashActiveOnGotoStore = GET_HASH_KEY(g_FMMC_STRUCT.tl31LoadedContentID)
		NET_PRINT("    ----->     HIDEOUT .KGM [GangAttack] - SCRIPT_CLEANUP - CLEANUP DUE TO ACCESSING STORE - g_gaContentIdHashActiveOnGotoStore = ")
		NET_PRINT_INT(g_gaContentIdHashActiveOnGotoStore)
		NET_PRINT(" - ")
		NET_PRINT(g_FMMC_STRUCT.tl31LoadedContentID)
		NET_NL()
	ENDIF
	
	//RESET_FMMC_MISSION_VARIABLES()	
	
	//IF IS_NET_PLAYER_OK(PLAYER_ID())
	//	SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_WillFlyThroughWindscreen, TRUE)
	//ENDIF
	
	// Player
	//IF IS_NET_PLAYER_OK(PLAYER_ID())
		// Reset afterlife state (411389)
		SET_GAME_STATE_ON_DEATH(AFTERLIFE_JUST_REJOIN)
	//ENDIF
	INT i
	REPEAT MAX_NUM_PEDS i
		IF DOES_BLIP_EXIST(PedBlipData[i].BlipID)
			REMOVE_BLIP(PedBlipData[i].BlipID)
		ENDIF
	ENDREPEAT
	
	CLEANUP_CELEBRATION_SCREEN(sCelebrationData)
	SET_CELEBRATION_SCREEN_AS_ACTIVE(FALSE)
	
	IF GET_FM_JOB_ENTERY_TYPE() = ciMISSION_ENTERY_TYPE_WALK_IN
		CLEAR_FM_JOB_ENTERY_TYPE()
	ENDIF
	// Returns FALSE by the cleanup stage
	// Cleanup team targetting
	SET_PLAYER_DEFAULT_FRIENDLY_FIRE_OPTION()	//NETWORK_SET_FRIENDLY_FIRE_OPTION(TRUE)
	//SET_PED_CAN_BE_TARGETTED_BY_TEAM(PLAYER_PED_ID(), GET_PLAYER_TEAM(PLAYER_ID()), TRUE)
	SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE)
	
	/*IF NOT IS_PLAYER_TELEPORT_ACTIVE()
	AND NOT IS_TRANSITION_ACTIVE()
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	ENDIF*/
	//PRINTLN("SET_PLAYER_TEAM(PLAYER_ID(), -1)")
	//SET_PLAYER_TEAM(PLAYER_ID(), -1)
	
	//SET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID(), rgFM_DefaultPlayer)
	
	// Emergency services
	IF NOT FM_EVENT_IS_PLAYER_ON_ANY_FM_EVENT(PLAYER_ID())
	AND NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_g_bNoCopsActive)
	AND NOT IS_BIT_SET(g_GB_WORK_VARS.iEventBitSet, ciGB_WORK_WANTED_SUPRESSED_BY_EVENT)
		NET_PRINT("    ----->     HIDEOUT - CALLING SET_MAX_WANTED_LEVEL(6)")NET_NL()
		SET_MAX_WANTED_LEVEL(6)
		SET_WANTED_LEVEL_MULTIPLIER(1.0)
	ENDIF
	
	ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, TRUE)			
	ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, TRUE)
	SET_AUDIO_FLAG("DisableFlightMusic", FALSE)
	SET_AUDIO_FLAG("WantedMusicDisabled", FALSE)
	STOP_AUDIO_SCENE("MP_CELEB_SCREEN_SCENE")
	
	// HUD
	//SET_WIDESCREEN_BORDERS(FALSE, -1)
	//DISPLAY_RADAR(TRUE)
	//DISPLAY_HUD(TRUE)
	
	//CLEANUP_BIG_MESSAGE()
	
	// (414073)
	IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM
		Enable_MP_Comms()
	ENDIF
	/*ENABLE_SELECTOR()
	CLEAR_ADDITIONAL_TEXT(MISSION_TEXT_SLOT, TRUE)
	DISABLE_SCRIPT_HUD(HUDPART_RANKBAR, FALSE)
	ENABLE_ALL_MP_HUD()*/
	
	MPGlobals.bRunningGangHideout = FALSE
	
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("GHO_WARN")
		CLEAR_HELP()
	ENDIF
	Clear_Any_Objective_Text_From_This_Script()
	
	// Stop the players being invisible
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF NOT NETWORK_IS_TUTORIAL_SESSION_CHANGE_PENDING()
			IF NETWORK_IS_IN_TUTORIAL_SESSION()
				NETWORK_END_TUTORIAL_SESSION()
				NET_PRINT("    ----->     HIDEOUT - NETWORK_END_TUTORIAL_SESSION")NET_NL()
			ENDIF
		ENDIF
		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			IF IS_BIT_SET(serverBD.iServerBitSet, biS_BlockingAreaDone)
				REMOVE_SCENARIO_BLOCKING_AREA(ScenarioBlockingArea, TRUE)
				CLEAR_BIT(serverBD.iServerBitSet, biS_BlockingAreaDone)
				NET_PRINT("    ----->     HIDEOUT - REMOVE_SCENARIO_BLOCKING_AREA - NETWORKED")NET_NL()
			ENDIF
		ENDIF
	ENDIF
	
//	CLEANUP_RAMPAGE_SCALEFORM()
		
	//SET_LOCAL_PLAYER_CAN_COLLECT_PORTABLE_PICKUPS(TRUE)
	//CLEAR_A_INVENTORY_BOX_CONTENT(1)
	//CLEAR_INVENTORY_BOX_CONTENT()	//From bug 1091669
	//NET_PRINT("    ----->     HIDEOUT - CLEAR_A_INVENTORY_BOX_CONTENT - A") NET_NL()
	
	SET_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_GANGHIDEOUT, FALSE)
	
	CLEAR_SPAWN_AREA()
	
	IF DOES_NAVMESH_BLOCKING_OBJECT_EXIST(iNavmeshBlock)
		REMOVE_NAVMESH_BLOCKING_OBJECT(iNavmeshBlock)
		NET_PRINT("    ----->     HIDEOUT - SCRIPT_CLEANUP - REMOVE_NAVMESH_BLOCKING_OBJECT") NET_NL()
	ENDIF
	
	/*IF serverBD.eHideoutVariation = HIDEOUT_KILL_PEDS
	//OR serverBD.eHideoutVariation = HIDEOUT_PACKAGE_STEAL
		CLEAR_SAFE_CRACK_PERSISTING_DATA(SafeCrackData)
		CANCEL_MUSIC_EVENT("GA_KILL_SAFE")
		NET_PRINT("    ----->     HIDEOUT - SCRIPT_CLEANUP - CANCEL MUSIC - GA_KILL_SAFE - B") NET_NL()
	ENDIF*/
	IF NOT SHOULD_HIDE_GANG_ATTACK_UI()
		IF NOT IS_BIT_SET(iMusicEventBitSet, biME_End)
			TRIGGER_MUSIC_EVENT("GA_STOP")
			SET_BIT(iMusicEventBitSet, biME_End)
			NET_PRINT("    ----->     HIDEOUT - SCRIPT_CLEANUP - MUSIC EVENT - GA_STOP") NET_NL()
		ENDIF
	ENDIF
	
	/*IF serverBD.eHideoutVariation = HIDEOUT_TERRITORY_CONTROL
		SET_VEHICLE_MODEL_IS_SUPPRESSED(serverBD.VehicleData.Model, FALSE)
	ENDIF*/
	
	GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iGangBeingFoughtInGangAttack = -1
	
	IF IS_BIT_SET(iBoolsBitSet, biInitialGangAngryCheckDone)
	AND serverBD.iGangID != -1
		SET_GANG_ANGRY_AT_PLAYER(serverBD.iGangID, GANG_ANGRY_TIME_SHORT)
	ENDIF
	
	INT iPassFail
	IF NOT IS_BIT_SET(iBoolsBitSet, biRewardDone)
		iPassFail = ciFMMC_END_OF_MISSION_STATUS_CANCELLED
	ELSE
		iPassFail = ciFMMC_END_OF_MISSION_STATUS_PASSED
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF IS_BIT_SET(iBoolsBitSet, biStartedPlayStats)
			DEAL_WITH_FM_MATCH_END(FMMC_TYPE_GANGHIDEOUT, serverBD.iPlayStatsID[1], ENUM_TO_INT(serverBD.eHideoutVariation), iPassFail, 0, playerBD[PARTICIPANT_ID_TO_INT()].iKills, playerBD[PARTICIPANT_ID_TO_INT()].iDeaths, 0, 0, 0, playerBD[PARTICIPANT_ID_TO_INT()].iHeadShots)
			NET_PRINT("    ----->     HIDEOUT - CONTROL_PLAY_STATS - DEAL_WITH_FM_MATCH_END DONE") NET_NL()
		ENDIF
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		RESET_FMMC_MISSION_VARIABLES(TRUE, FALSE, iPassFail, FALSE)
	ENDIF
	
	CLEANUP_TRACKED_POINTS()
	IF IS_THIS_MISSION_CONTENT_ID_HASH_A_GANG_ATTACK(g_FMMC_STRUCT.iRootContentIDHash)
		g_FMMC_STRUCT.iRootContentIDHash = 0
		PRINTLN("    ----->     HIDEOUT - SCRIPT_CLEANUP - IS_THIS_MISSION_CONTENT_ID_HASH_A_GANG_ATTACK - g_FMMC_STRUCT.iRootContentIDHash = 0") 
	ENDIF
	NET_PRINT("    ----->     HIDEOUT - SCRIPT_CLEANUP") NET_NL()
	TERMINATE_THIS_MULTIPLAYER_THREAD(serverBD.TerminationTimer)
ENDPROC

/// PURPOSE:
///    Loads text required for the Hideout script.
/// RETURNS:
///    TRUE if text has been loaded, FALSE if not.
FUNC BOOL LOAD_HIDEOUT_TEXT_BLOCK()
	//REQUEST_ADDITIONAL_TEXT("HIDEOUT", MISSION_TEXT_SLOT)
	
	//IF HAS_THIS_ADDITIONAL_TEXT_LOADED("HIDEOUT", MISSION_TEXT_SLOT)
		RETURN TRUE
	//ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Gets if end game conditions have been fulfilled.
/// RETURNS:
///    TRUE if conditions met, FALSE if not.
//FUNC BOOL HAVE_END_GAME_CONDITIONS_BEEN_MET()
//	RETURN FALSE
//ENDFUNC

FUNC BOOL FADE_SCREEN_IN()
	
	IF IS_SCREEN_FADED_OUT()
	OR IS_SCREEN_FADING_OUT()
		DO_SCREEN_FADE_IN(500)
		
	ELIF IS_SCREEN_FADED_IN()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

#IF IS_DEBUG_BUILD
PROC CREATE_WIDGETS()
	INT iPlayer
	TEXT_LABEL_15 tl63

	START_WIDGET_GROUP("HIDEOUT")  
		START_WIDGET_GROUP("TEMP DEBUG")
			ADD_WIDGET_BOOL("Create Peds as Defensive", bMakePedsDefensive)
			ADD_WIDGET_INT_READ_ONLY("iNumberOfWeapons", g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons)
			ADD_WIDGET_INT_READ_ONLY("iNumberOfProps", g_FMMC_STRUCT_ENTITIES.iNumberOfProps)
		STOP_WIDGET_GROUP()	
		ADD_WIDGET_INT_READ_ONLY("Kills Required", serverBD.iKillGoal)
		ADD_WIDGET_INT_READ_ONLY("Peds Spawned", serverBD.iPedsSpawned)
		ADD_WIDGET_INT_READ_ONLY("Server Total Kills", serverBD.iTotalKills)
		ADD_WIDGET_INT_READ_ONLY("Peds Alive", serverBD.iNumAlivePeds)
		START_WIDGET_GROUP("Entity Reservations")
			ADD_WIDGET_INT_READ_ONLY("Vehicles Reserved", serverBD.iReservedVehicles)
			ADD_WIDGET_INT_READ_ONLY("Peds Reserved", serverBD.iReservedPeds)
			ADD_WIDGET_INT_READ_ONLY("Objects Reserved", serverBD.iReservedObjects)
		STOP_WIDGET_GROUP()	
		START_WIDGET_GROUP("Player Kills")
			ADD_WIDGET_INT_READ_ONLY("Total", EndScreenData.iTrackedKills)
			ADD_WIDGET_INT_READ_ONLY("Headshots", EndScreenData.iHeadshots)
			ADD_WIDGET_INT_READ_ONLY("Point Blank", EndScreenData.iPointBlankKills)
			ADD_WIDGET_INT_READ_ONLY("Own Deaths", playerBD[PARTICIPANT_ID_TO_INT()].iDeaths)
		STOP_WIDGET_GROUP()	
		ADD_WIDGET_INT_READ_ONLY("Num Players", serverBD.iNumParticpants)
		ADD_WIDGET_INT_READ_ONLY("Num Players in Area", serverBD.iNumParticpantsInArea)
		ADD_WIDGET_INT_READ_ONLY("Highest Num Players", serverBD.iHighestNumParticpants)
		ADD_WIDGET_FLOAT_READ_ONLY("Hideout Radius", serverBD.fHideoutRadius)
		ADD_WIDGET_VECTOR_SLIDER("Hideout Centre", serverBD.vHideoutCentre, -5000, 5000, 0)
		
		START_WIDGET_GROUP("Difficulty")
			ADD_WIDGET_INT_READ_ONLY("Average Rank", serverBD.iAverageRank)
			ADD_WIDGET_INT_READ_ONLY("iDifficultyFromPlayerStats", serverBD.iDifficultyFromPlayerStats)
		STOP_WIDGET_GROUP()	
		
		/*START_WIDGET_GROUP("End Screen")
			CREATE_A_RECT_PLACEMENT_WIDGET(EndScreenData.EndScreenBackground, "Background")
			CREATE_A_TEXT_STYLE_WIGET(EndScreenData.EndScreenTextStyle, "Text Style")
			CREATE_A_TEXT_PLACEMENT_WIDGET(EndScreenData.EndScreenTitle[0], "Total Kills")
			CREATE_A_TEXT_PLACEMENT_WIDGET(EndScreenData.EndScreenResult[0], "Total Kills Result")
			CREATE_A_TEXT_PLACEMENT_WIDGET(EndScreenData.EndScreenTitle[1], "Personal Kills")
			CREATE_A_TEXT_PLACEMENT_WIDGET(EndScreenData.EndScreenResult[1], "Personal Kills Result")
			CREATE_A_TEXT_PLACEMENT_WIDGET(EndScreenData.EndScreenTitle[2], "Headshots")
			CREATE_A_TEXT_PLACEMENT_WIDGET(EndScreenData.EndScreenResult[2], "Headshots Result")
			CREATE_A_TEXT_PLACEMENT_WIDGET(EndScreenData.EndScreenTitle[3], "Point Blank")
			CREATE_A_TEXT_PLACEMENT_WIDGET(EndScreenData.EndScreenResult[3], "Point Blank Result")
		STOP_WIDGET_GROUP()	*/
		
		// Gameplay debug, sever only.
		START_WIDGET_GROUP("Server Only Gameplay") 
			ADD_WIDGET_BOOL("End mission", bHostEndMissionNow)	
			ADD_WIDGET_INT_READ_ONLY("iTotalPlayerKillPoints", serverBD.iTotalPlayerKillPoints)		
		STOP_WIDGET_GROUP()		
		
		// Data about the server
		START_WIDGET_GROUP("Server BD") 
				ADD_WIDGET_INT_SLIDER("S. Game state", serverBD.iServerGameState,-1, HIGHEST_INT,1)
		STOP_WIDGET_GROUP()	

		// Data about the clients. * = You.
		START_WIDGET_GROUP("Client BD")  				
			REPEAT NUM_NETWORK_PLAYERS iPlayer
				tl63 = "Player "
				tl63 += iPlayer
				IF iPlayer = PARTICIPANT_ID_TO_INT()
					tl63 += "*"
				ENDIF
				START_WIDGET_GROUP(tl63)
					ADD_WIDGET_INT_SLIDER("Game state", playerBD[iPlayer].iGameState,-1, HIGHEST_INT,1)
				STOP_WIDGET_GROUP()
			ENDREPEAT				
		STOP_WIDGET_GROUP()	
		
	STOP_WIDGET_GROUP()
ENDPROC
#ENDIF

//PURPOSE: Creates the Safe
//FUNC BOOL CREATE_SAFE()
//	IF serverBD.eHideoutVariation = HIDEOUT_KILL_PEDS
//	//OR serverBD.eHideoutVariation = HIDEOUT_PACKAGE_STEAL
//		REQUEST_MODEL(serverBD.SafeData.Model)
//		REQUEST_MODEL(serverBD.SafeData.DoorModel)
//		
//		IF HAS_MODEL_LOADED(serverBD.SafeData.Model)
//		AND HAS_MODEL_LOADED(serverBD.SafeData.DoorModel)
//			
//			//Safe Body
//			IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.SafeData.NetID)
//				IF CAN_REGISTER_MISSION_OBJECTS(1)
//					
//					IF CREATE_NET_OBJ(serverBD.SafeData.NetID, serverBD.SafeData.Model, SafeCrackData.vSafeCoord, TRUE, TRUE, TRUE)
//						SET_NETWORK_ID_EXISTS_ON_ALL_MACHINES(serverBD.SafeData.NetID, TRUE)
//						SET_ENTITY_HEADING(NET_TO_OBJ(serverBD.SafeData.NetID), SafeCrackData.fSafeHeading)
//						PLACE_OBJECT_ON_GROUND_PROPERLY(NET_TO_OBJ(serverBD.SafeData.NetID))
//						FREEZE_ENTITY_POSITION(NET_TO_OBJ(serverBD.SafeData.NetID), TRUE)
//						//RETAIN_ENTITY_IN_INTERIOR(NET_TO_OBJ(serverBD.SafeData.NetID), GET_INTERIOR_AT_COORDS(SafeCrackData.vSafeCoord))
//						NET_PRINT_TIME() NET_PRINT("    ----->     HIDEOUT - CREATE_SAFE BODY - DONE - VECTOR = ") NET_PRINT_VECTOR(SafeCrackData.vSafeCoord) NET_PRINT(" HEADING = ") NET_PRINT_FLOAT(SafeCrackData.fSafeHeading) NET_NL()
//					ENDIF
//					
//				ENDIF
//			//ENDIF
//			
//			//Safe Door
//			ELIF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.SafeData.niDoor)
//				IF CAN_REGISTER_MISSION_OBJECTS(1)
//				
//					serverBD.SafeData.vSafeDoor = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(NET_TO_OBJ(serverBD.SafeData.NetID)), SafeCrackData.fSafeHeading, <<0, 0, 0.1-0.76>>)
//						
//					IF CREATE_NET_OBJ(serverBD.SafeData.niDoor, serverBD.SafeData.DoorModel, serverBD.SafeData.vSafeDoor, TRUE, TRUE, TRUE)
//						FREEZE_ENTITY_POSITION(NET_TO_OBJ(serverBD.SafeData.niDoor), TRUE)
//						SET_ENTITY_HEADING(NET_TO_OBJ(serverBD.SafeData.niDoor), serverBD.SafeData.fSafeDoorHeading)
//						SET_NETWORK_ID_EXISTS_ON_ALL_MACHINES(serverBD.SafeData.niDoor, TRUE)
//						//RETAIN_ENTITY_IN_INTERIOR(NET_TO_OBJ(serverBD.SafeData.niDoor), GET_INTERIOR_AT_COORDS(SafeCrackData.vSafeCoord))
//						NET_PRINT_TIME() NET_PRINT("    ----->     HIDEOUT - CREATE_SAFE DOOR - DONE ") NET_NL()
//					ENDIF
//					
//				ENDIF
//			ENDIF
//			
//		ENDIF
//	ELSE
//		RETURN TRUE
//	ENDIF
//	
//	IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.SafeData.NetID)
//	AND NETWORK_DOES_NETWORK_ID_EXIST(serverBD.SafeData.niDoor)
//		SET_MODEL_AS_NO_LONGER_NEEDED(serverBD.SafeData.Model)
//		SET_MODEL_AS_NO_LONGER_NEEDED(serverBD.SafeData.DoorModel)
//		RETURN TRUE
//	ENDIF
//	
//	RETURN FALSE
//ENDFUNC


//PURPOSE: Creates the Destructible Crates
FUNC BOOL CREATE_DESTRUCTIBLE_CRATES()
	INT i
	
	/*IF bUseCrates = FALSE
		RETURN TRUE
	ENDIF*/
	
	REPEAT MAX_NUM_DESTRUCTIBLE_CRATES i
		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.DCrateData[i].NetID)
		AND NOT IS_VECTOR_ZERO(serverBD.DCrateData[i].vCoord)
			REQUEST_MODEL(serverBD.DCrateData[i].Model)
			
			IF HAS_MODEL_LOADED(serverBD.DCrateData[i].Model)
				IF CAN_REGISTER_MISSION_OBJECTS(1)
					CLEAR_AREA(serverBD.DCrateData[i].vCoord, 1.0, TRUE, FALSE, FALSE, TRUE)
					IF CREATE_NET_OBJ(serverBD.DCrateData[i].NetID, serverBD.DCrateData[i].Model, serverBD.DCrateData[i].vCoord , TRUE, TRUE, TRUE)
						SET_NETWORK_ID_EXISTS_ON_ALL_MACHINES(serverBD.DCrateData[i].NetID, TRUE)
						SET_ENTITY_HEADING(NET_TO_OBJ(serverBD.DCrateData[i].NetID), serverBD.DCrateData[i].fHeading)
						PLACE_OBJECT_ON_GROUND_PROPERLY(NET_TO_OBJ(serverBD.DCrateData[i].NetID))
						//FREEZE_ENTITY_POSITION(NET_TO_OBJ(serverBD.SafeData.NetID), TRUE)
						//RETAIN_ENTITY_IN_INTERIOR(NET_TO_OBJ(serverBD.SafeData.NetID), GET_INTERIOR_AT_COORDS(SafeCrackData.vSafeCoord))
						NET_PRINT_TIME() NET_PRINT("    ----->     HIDEOUT - CREATE_DESTRUCTIBLE_CRATES BODY - DONE - CRATE = ") NET_PRINT_INT(i) NET_PRINT(" VECTOR = ") NET_PRINT_VECTOR(serverBD.DCrateData[i].vCoord) NET_PRINT(" HEADING = ") NET_PRINT_FLOAT(serverBD.DCrateData[i].fHeading) NET_NL()
					ENDIF
					
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	REPEAT MAX_NUM_DESTRUCTIBLE_CRATES i
		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.DCrateData[i].NetID)
		AND NOT IS_VECTOR_ZERO(serverBD.DCrateData[i].vCoord)
			RETURN FALSE
		ENDIF
	ENDREPEAT
	
	//SET_MODEL_AS_NO_LONGER_NEEDED(serverBD.DCrateData[i].Model)
	RETURN TRUE
ENDFUNC

//PURPOSE: Creates a Cash pickup that should be in every crate
FUNC BOOL CREATE_BONUS_CASH_PICKUP(INT iCrate, INT iPickup)
	VECTOR vPickupPos
	INT iPlacementFlags = 0
	//SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_UPRIGHT))
	SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_SNAP_TO_GROUND))
	SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ORIENT_TO_GROUND))
	//SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_PLAYER_GIFT))
	//SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ROTATE)
	
	IF CAN_REGISTER_MISSION_ENTITIES(0, 0, 0, 1)
		IF NOT DOES_ENTITY_EXIST(serverBD.DCrateData[iCrate].Pickup[iPickup].oiPickup)
			vPickupPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(NET_TO_OBJ(serverBD.DCrateData[iCrate].NetID), <<0, 0.25-(iPickup*0.25), 0.1>>)
			serverBD.DCrateData[iCrate].Pickup[iPickup] = CREATE_AMBIENT_PICKUP_WITH_SAP_PICKUP_ID(PICKUP_GANG_ATTACK_MONEY, vPickupPos, iPlacementFlags, ROUND(g_sMPTunables.icash_modifier_crate_drop_gang_attack * g_sMPTunables.cashMultiplier), DEFAULT, TRUE)
			PRINTLN("  ---->  HIDEOUT - CREATE_BONUS_CASH_PICKUP ", iCrate, " : ", iPickup , " : CASH AMOUNT = ", g_sMPTunables.icash_modifier_crate_drop_gang_attack * g_sMPTunables.cashMultiplier)
		ENDIF
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Creates the contents of the passed in Crate.
FUNC BOOL CREATE_DESTRUCTIBLE_CRATE_CONTENTS(INT i)
	
	IF IS_VECTOR_ZERO(serverBD.DCrateData[i].vCoord)
		PRINTLN("  ---->  HIDEOUT - CREATE_DESTRUCTIBLE_CRATE_CONTENTS - INVALID DATA ", i)
		RETURN TRUE
	ENDIF
	
	//IF NOT DOES_PICKUP_EXIST(serverBD.DCrateData[i].Pickup[k])
		INT k
		VECTOR vPickupPos
		INT iPlacementFlags = 0
		//SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_UPRIGHT))
		SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_SNAP_TO_GROUND))
		SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ORIENT_TO_GROUND))
		//SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ROTATE))
		
		SWITCH serverBD.DCrateData[i].iContents
			CASE ciCRATE_CONTENTS_CASH
				//REQUEST_MODEL(PROP_LD_HEALTH_PACK)
				//IF HAS_MODEL_LOADED(PROP_LD_HEALTH_PACK)
				IF CAN_REGISTER_MISSION_ENTITIES(0, 0, 0, MAX_NUM_CASH_PICKUPS)
					REPEAT MAX_NUM_CASH_PICKUPS k
						IF NOT DOES_ENTITY_EXIST(serverBD.DCrateData[i].Pickup[k].oiPickup)
							vPickupPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(NET_TO_OBJ(serverBD.DCrateData[i].NetID), <<0.6-(k*0.4), 0, 0.1>>)
							serverBD.DCrateData[i].Pickup[k] = CREATE_AMBIENT_PICKUP_WITH_SAP_PICKUP_ID(PICKUP_GANG_ATTACK_MONEY, vPickupPos, iPlacementFlags, serverBD.DCrateData[i].iContentsAmount, DEFAULT, TRUE)
							PRINTLN("  ---->  HIDEOUT - CREATE_DESTRUCTIBLE_CRATE_CONTENTS - CASH ", i, " : ", k)
						ENDIF
					ENDREPEAT
					RETURN TRUE
				ENDIF
			BREAK
			
			/*CASE ciCRATE_CONTENTS_DRUGS
				REQUEST_MODEL(PROP_DRUG_PACKAGE_02)
				IF HAS_MODEL_LOADED(PROP_DRUG_PACKAGE_02)
				AND CAN_REGISTER_MISSION_ENTITIES(0, 0, 0, MAX_NUM_DRUG_PICKUPS)
					REPEAT MAX_NUM_DRUG_PICKUPS k
						IF NOT DOES_PICKUP_EXIST(serverBD.DCrateData[i].Pickup[k])
							vPickupPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(NET_TO_OBJ(serverBD.DCrateData[i].NetID), <<0.6-(k*0.4), 0, 0.1>>)
							serverBD.DCrateData[i].Pickup[k] = CREATE_PICKUP(PICKUP_MONEY_MED_BAG, vPickupPos, iPlacementFlags, serverBD.DCrateData[i].iContentsAmount, TRUE, PROP_DRUG_PACKAGE_02)
							PRINTLN("  ---->  HIDEOUT - CREATE_DESTRUCTIBLE_CRATE_CONTENTS - DRUGS ", i)
						ENDIF
					ENDREPEAT
					RETURN TRUE
				ENDIF
			BREAK*/
			
			CASE ciCRATE_CONTENTS_WEAPON
				IF CAN_REGISTER_MISSION_ENTITIES(0, 0, 0, HIDEOUT_MAX_NUM_WEAPON_PICKUPS)
					//SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_PLAYER_GIFT))
					REPEAT HIDEOUT_MAX_NUM_WEAPON_PICKUPS k
						IF NOT DOES_ENTITY_EXIST(serverBD.DCrateData[i].Pickup[k].oiPickup)
							vPickupPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(NET_TO_OBJ(serverBD.DCrateData[i].NetID), <<0, 0.25-(k*0.25), 0.1>>)
							serverBD.DCrateData[i].Pickup[k] = CREATE_AMBIENT_PICKUP_WITH_SAP_PICKUP_ID(serverBD.DCrateData[i].ContentsWeapon, vPickupPos, iPlacementFlags, DEFAULT, DEFAULT, TRUE)
							PRINTLN("  ---->  HIDEOUT - CREATE_DESTRUCTIBLE_CRATE_CONTENTS - WEAPON ", i, " : ", k)
						ENDIF
					ENDREPEAT
					IF CREATE_BONUS_CASH_PICKUP(i, HIDEOUT_MAX_NUM_WEAPON_PICKUPS)
						RETURN TRUE
					ENDIF
				ENDIF
			BREAK
			
			CASE ciCRATE_CONTENTS_AMMO
				IF CAN_REGISTER_MISSION_ENTITIES(0, 0, 0, MAX_NUM_AMMO_PICKUPS)
					//SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_PLAYER_GIFT))
					REPEAT MAX_NUM_AMMO_PICKUPS k
						IF NOT DOES_ENTITY_EXIST(serverBD.DCrateData[i].Pickup[k].oiPickup)
							vPickupPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(NET_TO_OBJ(serverBD.DCrateData[i].NetID), <<0, 0.25-(k*0.25), 0.2>>)
							serverBD.DCrateData[i].Pickup[k] = CREATE_AMBIENT_PICKUP_WITH_SAP_PICKUP_ID(PICKUP_AMMO_BULLET_MP, vPickupPos, iPlacementFlags, serverBD.DCrateData[i].iContentsAmount, DEFAULT, TRUE)
							PRINTLN("  ---->  HIDEOUT - CREATE_DESTRUCTIBLE_CRATE_CONTENTS - AMMO ", i, " : ", k)
						ENDIF
					ENDREPEAT
					IF CREATE_BONUS_CASH_PICKUP(i, MAX_NUM_AMMO_PICKUPS)
						RETURN TRUE
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	//ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Moves the pickups up slightly, due to the snap to ground
PROC MOVE_DESTRUCTIBLE_CRATE_CONTENTS_UP(INT i)
	INT k
	REPEAT (MAX_NUM_CASH_PICKUPS+1) k	//ADD ONE ON FOR BONUS CASH
		IF DOES_ENTITY_EXIST(serverBD.DCrateData[i].Pickup[k].oiPickup)
			IF NETWORK_HAS_CONTROL_OF_ENTITY(serverBD.DCrateData[i].Pickup[k].oiPickup)
				SET_AMBIENT_PICKUP_COORDS_WITH_SAP_PICKUP_ID(serverBD.DCrateData[i].Pickup[k], GET_ENTITY_COORDS(serverBD.DCrateData[i].Pickup[k].oiPickup) + <<0, 0, 0.046*1.5>>)
				PRINTLN("  ---->  HIDEOUT - MOVE_DESTRUCTIBLE_CRATE_CONTENTS_UP - ", i, " : ", k)
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

//PURPOSE: Checks if the Destructible Crates have been destroyed and creates the contents if they have.
PROC MAINTAIN_DESTRUCTIBLE_CRATES()
	INT i
	
	REPEAT MAX_NUM_DESTRUCTIBLE_CRATES i
		IF NOT IS_BIT_SET(serverBD.DCrateData[i].iBitSet, iBS_DC_CrateDestroyed)
			IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.DCrateData[i].NetID)
			//AND NOT DOES_PICKUP_EXIST(serverBD.DCrateData[i].Pickup)
				//IF TAKE_CONTROL_OF_NET_ID(serverBD.DCrateData[i].NetID)
					IF HAS_OBJECT_BEEN_BROKEN(NET_TO_OBJ(serverBD.DCrateData[i].NetID))
						IF CREATE_DESTRUCTIBLE_CRATE_CONTENTS(i)
							MOVE_DESTRUCTIBLE_CRATE_CONTENTS_UP(i)
							SET_BIT(serverBD.DCrateData[i].iBitSet, iBS_DC_CrateDestroyed)
						ENDIF
					ENDIF
				//ENDIF
			ENDIF
		ELIF NOT IS_BIT_SET(serverBD.DCrateData[i].iBitSet, iBS_DC_CratePickupsMoveUp)
			MOVE_DESTRUCTIBLE_CRATE_CONTENTS_UP(i)
			IF serverBD.DCrateData[i].iMoveCounter > TIMES_TO_MOVE_UP
				SET_BIT(serverBD.DCrateData[i].iBitSet, iBS_DC_CratePickupsMoveUp)
			ENDIF
			serverBD.DCrateData[i].iMoveCounter++
		ENDIF
	ENDREPEAT
ENDPROC

//PURPOSE: Returns the area to check for the model type
FUNC FLOAT GET_VEHICLE_RANGE_CHECK(INT i)
	//PRINTLN(" GET_VEHICLE_RANGE_CHECK - CALLED")
	IF IS_THIS_MODEL_A_CAR(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn)
	OR IS_THIS_MODEL_A_BIKE(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn)
		RETURN 3.0
	ELIF IS_THIS_MODEL_A_BOAT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn)
		RETURN 6.0
	ELIF IS_THIS_MODEL_A_PLANE(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn)
		//PRINTLN(" GET_VEHICLE_RANGE_CHECK - 10.0")
		RETURN 10.0
	ELIF IS_THIS_MODEL_A_HELI(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn)
		RETURN 10.0
	ENDIF
	
	RETURN 3.0
ENDFUNC

//PURPOSE: Creates the vehicles if there is space.
FUNC BOOL CREATE_VEHICLES()
	INT i
	
	IF g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles > MAX_NUM_VEH
		g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles = MAX_NUM_VEH
	ENDIF
	
	IF ARE_NETWORK_VEHICLES_AVAILABLE_FOR_SCRIPT_LAUNCH(g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles, TRUE, FALSE)
		REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles i	
			IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn != DUMMY_MODEL_FOR_SCRIPT
				IF 	NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niVehicle[i])
				AND NOT IS_BIT_SET(serverBD.iVehiclesCreatedBitSet, i)
					serverBD.VehicleModel[i] = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn
					REQUEST_MODEL(serverBD.VehicleModel[i])
					IF HAS_MODEL_LOADED(serverBD.VehicleModel[i])
						IF DOES_TRACKED_POINT_EXIST(serverBD.iVehicleTrackedPoints[i]) AND NOT IS_TRACKED_POINT_VISIBLE(serverBD.iVehicleTrackedPoints[i])
						AND IS_POINT_OK_FOR_NET_ENTITY_CREATION(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].vPos, GET_VEHICLE_RANGE_CHECK(i), 1.0, 1.0, 3.0, FALSE, TRUE, FALSE, 30, TRUE, -1, FALSE, 0, TRUE, 0, TRUE)
							//CLEAR_AREA(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].vPos, 1.0, FALSE, FALSE, FALSE, TRUE)
							IF CREATE_NET_VEHICLE(serverBD.niVehicle[i], g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].vPos, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].fHead)
								FMMC_SET_THIS_VEHICLE_EXTRAS(NET_TO_VEH(serverBD.niVehicle[i]), g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iVehBitSet)
								FMMC_SET_THIS_VEHICLE_COLOURS(NET_TO_VEH(serverBD.niVehicle[i]), g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iColour, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iLivery)
								SET_UP_FMMC_VEH_RC(NET_TO_VEH(serverBD.niVehicle[i]),i,serverBD.niVehicle,g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].vPos, FALSE) //I don't think this should be using this function...
								
								SET_VEHICLE_INFLUENCES_WANTED_LEVEL(NET_TO_VEH(serverBD.niVehicle[i]), FALSE)
								SET_VEHICLE_DOORS_LOCKED(NET_TO_VEH(serverBD.niVehicle[i]), VEHICLELOCK_LOCKED)
								SET_VEHICLE_MODEL_IS_SUPPRESSED(serverBD.VehicleModel[i], TRUE)
								SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(NET_TO_VEH(serverBD.niVehicle[i]), TRUE)
								
								SET_BIT(serverBD.iVehiclesCreatedBitSet, i)
								
								NET_PRINT("  ---->  HIDEOUT - Created vehicle at this position: ") NET_PRINT_VECTOR(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].vPos)NET_PRINT(" vehicle number: ") NET_PRINT_INT(i)  NET_NL()
							ENDIF
						ELSE
						
							SET_BIT(serverBD.iVehiclesCreatedBitSet, i)
							
							NET_PRINT("  ---->  HIDEOUT - Skipped creating vehicle at this position: ") NET_PRINT_VECTOR(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].vPos)NET_PRINT(" vehicle number: ") NET_PRINT_INT(i)  NET_NL()
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ELSE
		NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - VEHICLE NOT SET TO SPAWN AT START     <----------     ") NET_NL()
		RETURN TRUE
	ENDIF
	
	//Check we have created the Vehicles
	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles i
		IF 	NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.niVehicle[i])
		AND	NOT IS_BIT_SET(serverBD.iVehiclesCreatedBitSet, i)
		AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn != DUMMY_MODEL_FOR_SCRIPT
			RETURN FALSE
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

//PURPOSE: Unlocks all the remaining vehicle doors
PROC UNLOCK_VEHICLE_DOORS()
	INT i
	
	IF g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles > MAX_NUM_VEH
		g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles = MAX_NUM_VEH
	ENDIF
	
	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles i	
		IF IS_NET_VEHICLE_DRIVEABLE(serverBD.niVehicle[i])
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niVehicle[i])
			OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBD.niVehicle[i]) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
				SET_VEHICLE_DOORS_LOCKED(NET_TO_VEH(serverBD.niVehicle[i]), VEHICLELOCK_UNLOCKED)
				SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(NET_TO_VEH(serverBD.niVehicle[i]), FALSE)
				NET_PRINT("  ---->  HIDEOUT - UNLOCK_VEHICLE_DOORS - DOORS UNLOCKED - Vehicle Number: ") NET_PRINT_INT(i) NET_NL()
			ENDIF
		ENDIF
		
		SET_VEHICLE_MODEL_IS_SUPPRESSED(serverBD.VehicleModel[i], FALSE)
	ENDREPEAT
ENDPROC

//PURPOSE: Creates the Props - Done locally by each player
FUNC BOOL CREATE_LOCAL_PROPS()
	INT i	
	
	IF g_FMMC_STRUCT_ENTITIES.iNumberOfProps > GA_MAX_NUM_PROPS
		g_FMMC_STRUCT_ENTITIES.iNumberOfProps = GA_MAX_NUM_PROPS
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn != DUMMY_MODEL_FOR_SCRIPT
		REQUEST_MODEL(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn)
		IF HAS_MODEL_LOADED(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn)
			REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfProps i
				IF NOT DOES_ENTITY_EXIST(oiProp[i])
					oiProp[i] = CREATE_OBJECT(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn , g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].vPos, FALSE, FALSE)
					SET_UP_FMMC_PROP(oiProp[i], i)
					NET_PRINT_TIME() NET_PRINT_STRINGS("  ---->  HIDEOUT - PROP CREATED     <----------     ", " Num ") NET_PRINT_INT(i) NET_NL()
				ENDIF
			ENDREPEAT
			//Check we have created all the things
			REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfProps i
				IF NOT DOES_ENTITY_EXIST(oiProp[i])
				AND g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn != DUMMY_MODEL_FOR_SCRIPT
					RETURN FALSE
				ENDIF
			ENDREPEAT
		ELSE
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

//PURPOSE: Creates the Vehicle
/*FUNC BOOL CREATE_PARKED_VEHICLE()
	IF serverBD.eHideoutVariation = HIDEOUT_TERRITORY_CONTROL
		REQUEST_MODEL(serverBD.VehicleData.Model)
			
		IF HAS_MODEL_LOADED(serverBD.VehicleData.Model)
					
			IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.VehicleData.NetID)
				IF CAN_REGISTER_MISSION_VEHICLES(1)				
					
					IF CREATE_NET_VEHICLE(serverBD.VehicleData.NetID, serverBD.VehicleData.Model, serverBD.VehicleData.vCoord, serverBD.VehicleData.fHeading)
						SET_VEHICLE_INFLUENCES_WANTED_LEVEL(serverBD.VehicleData.NetID, FALSE)
						SET_VEHICLE_DOORS_LOCKED(NET_TO_VEH(serverBD.VehicleData.NetID), VEHICLELOCK_LOCKED)
						SET_VEHICLE_MODEL_IS_SUPPRESSED(serverBD.VehicleData.Model, TRUE)
						NET_PRINT_TIME() NET_PRINT("    ----->     HIDEOUT - CREATE_VEHICLE - DONE ") NET_PRINT_VECTOR(serverBD.VehicleData.vCoord) NET_NL()
					ENDIF
					
				ENDIF
			ENDIF
			
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.VehicleData.NetID)
		SET_MODEL_AS_NO_LONGER_NEEDED(serverBD.VehicleData.Model)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC*/

//PURPOSE: Gives the player the contents of the pickup
//PROC GIVE_PICKUP_CONTENTS(INT i)
//	
//	INT iAmmo
//	INT iCash
//	VECTOR vPickupCoords
//	IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.PickUpData[i].NetID)
//		vPickupCoords = GET_ENTITY_COORDS(NET_TO_OBJ(serverBD.PickUpData[i].NetID))
//	ELSE
//		vPickupCoords = GET_PLAYER_COORDS(PLAYER_ID())
//	ENDIF
//	
//	SWITCH serverBD.PickUpData[i].iContentsType
//	
//		CASE PU_CONTENTS_CASH
//			iCash = MULTIPLY_CASH_BY_TUNABLE(serverBD.PickUpData[i].iContentsAmount)
//			GIVE_LOCAL_PLAYER_CASH(iCash/serverBD.iNumParticpants)
//			NETWORK_EARN_FROM_TAKEOVER_GANG_TURF(iCash/serverBD.iNumParticpants)
//			SET_CASH_REWARD_FOR_LOCATION(vPickupCoords, iCash/serverBD.iNumParticpants)
//			PRINT_TICKER_WITH_INT("GHO_TCASH", iCash/serverBD.iNumParticpants)	//Hideout package: $~1~
//			NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - GIVE_PICKUP_CONTENTS - PU_CONTENTS_CASH ") NET_PRINT_INT(i) NET_NL()
//		BREAK
//		
//		CASE PU_CONTENTS_WEAPON
//			iAmmo = (serverBD.PickUpData[i].iContentsAmount * GET_MAX_AMMO_IN_CLIP(PLAYER_PED_ID(), serverBD.PickUpData[i].iContentsWeapon))
//			GIVE_DELAYED_WEAPON_TO_PED(PLAYER_PED_ID(), serverBD.PickUpData[i].iContentsWeapon, iAmmo, FALSE)
//			PRINT_TICKER_WITH_STRING("GHO_TWEAP", GET_WEAPON_NAME(serverBD.PickUpData[i].iContentsWeapon))	//Hideout Package: ~a~
//			NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - GIVE_PICKUP_CONTENTS - PU_CONTENTS_WEAPON ") NET_PRINT_INT(i) NET_NL()
//		BREAK
//		
//	ENDSWITCH
//	
//	NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - GIVE_PICKUP_CONTENTS - DONE ") NET_PRINT_INT(i) NET_NL()
//ENDPROC
//
////PURPOSE: Populates the pickup data
//PROC GET_PICKUP_DATA(INT i)
//
//	//Random
//	IF serverBD.PickUpData[i].iContentsType = PU_CONTENTS_RANDOM
//		/*IF serverBD.eHideoutVariation = HIDEOUT_PACKAGE_STEAL
//			serverBD.PickUpData[i].iContentsType = PU_CONTENTS_CASH
//			NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - GET_PICKUP_DATA - HIDEOUT_PACKAGE_STEAL - iContentsType = PU_CONTENTS_CASH") NET_NL()
//		EL*/IF serverBD.eHideoutVariation = HIDEOUT_KILL_PEDS
//			serverBD.PickUpData[i].iContentsType = PU_CONTENTS_WEAPON
//			NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - GET_PICKUP_DATA - HIDEOUT_KILL_PEDS - iContentsType = PU_CONTENTS_WEAPON") NET_NL()
//		/*ELSE
//			serverBD.PickUpData[i].iContentsType = INT_TO_ENUM(PICKUP_CONTENTS_TYPE , GET_RANDOM_INT_IN_RANGE(0, ENUM_TO_INT(PU_CONTENTS_RANDOM)))
//			NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - GET_PICKUP_DATA - RANDOMISED") NET_NL()*/
//		ENDIF
//	ENDIF
//	
//	//Contents
//	SWITCH serverBD.PickUpData[i].iContentsType
//		CASE PU_CONTENTS_CASH
//			serverBD.PickUpData[i].Model = PROP_DRUG_PACKAGE	//PROP_CASH_CASE_01
//			serverBD.PickUpData[i].iContentsAmount = CASH_PACKAGE_AMOUNT //GET_RANDOM_INT_IN_RANGE(400, 1001)
//			NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - GET_PICKUP_DATA - CASH ") NET_PRINT_INT(serverBD.PickUpData[i].iContentsAmount) NET_NL()
//		BREAK
//		
//		CASE PU_CONTENTS_WEAPON
//			serverBD.PickUpData[i].Model = PROP_DRUG_PACKAGE
//			
//			INT iRand
//			iRand = GET_RANDOM_INT_IN_RANGE(0, 10)
//			IF iRand = 1
//				serverBD.PickUpData[i].iContentsAmount = 4
//				serverBD.PickUpData[i].iContentsWeapon = WEAPONTYPE_MG
//			ELIF iRand = 2
//				serverBD.PickUpData[i].iContentsAmount = 4
//				serverBD.PickUpData[i].iContentsWeapon = WEAPONTYPE_COMBATPISTOL
//			ELIF iRand = 3
//				serverBD.PickUpData[i].iContentsAmount = 4
//				serverBD.PickUpData[i].iContentsWeapon = WEAPONTYPE_APPISTOL
//			ELIF iRand = 4
//				serverBD.PickUpData[i].iContentsAmount = 4
//				serverBD.PickUpData[i].iContentsWeapon = WEAPONTYPE_PUMPSHOTGUN
//			ELIF iRand = 5
//				serverBD.PickUpData[i].iContentsAmount = 4
//				serverBD.PickUpData[i].iContentsWeapon = WEAPONTYPE_SAWNOFFSHOTGUN
//			ELIF iRand = 6
//				serverBD.PickUpData[i].iContentsAmount = 4
//				serverBD.PickUpData[i].iContentsWeapon = WEAPONTYPE_MICROSMG
//			ELIF iRand = 7
//				serverBD.PickUpData[i].iContentsAmount = 4
//				serverBD.PickUpData[i].iContentsWeapon = WEAPONTYPE_SMG
//			ELIF iRand = 8
//				serverBD.PickUpData[i].iContentsAmount = 4
//				serverBD.PickUpData[i].iContentsWeapon = WEAPONTYPE_ASSAULTRIFLE
//			ELIF iRand = 9
//				serverBD.PickUpData[i].iContentsAmount = 4
//				serverBD.PickUpData[i].iContentsWeapon = WEAPONTYPE_GRENADE
//			ELSE
//				serverBD.PickUpData[i].iContentsAmount = 4
//				serverBD.PickUpData[i].iContentsWeapon = WEAPONTYPE_STICKYBOMB
//			ENDIF
//			NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - GET_PICKUP_DATA - WEAPON ") NET_PRINT(GET_WEAPON_NAME(serverBD.PickUpData[i].iContentsWeapon)) NET_NL()
//		BREAK
//	ENDSWITCH
//ENDPROC

//PURPOSE: Create Crate Pickups
//FUNC BOOL HAS_SERVER_CREATED_MISSION_PICKUPS()
//
//	#IF IS_DEBUG_BUILD
//		NET_SCRIPT_HOST_ONLY_COMMAND_ASSERT(" HIDEOUT - HAS_SERVER_CREATED_PICKUPS called on client when it's a host only function")
//	#ENDIF
//
//	//INT iPlacementFlags = 0
//	//SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_SNAP_TO_GROUND))
//	//SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ORIENT_TO_GROUND))
//	
//	INT i
//	VECTOR vPos
//
//	IF g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons = 0
//		RETURN TRUE
//	ENDIF
//	
//	//Only Crate one Pickup
//	/*IF serverBD.eHideoutVariation = HIDEOUT_PACKAGE_STEAL
//		IF GET_RANDOM_BOOL()
//			g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[0].vPos = g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[1].vPos
//			NET_PRINT(" HIDEOUT - HAS_SERVER_CREATED_PICKUPS - HIDEOUT_PACKAGE_STEAL - use second package data")  NET_NL()
//		ENDIF
//		g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons = 1
//		NET_PRINT(" HIDEOUT - HAS_SERVER_CREATED_PICKUPS - HIDEOUT_PACKAGE_STEAL - only use 1 package") NET_NL()
//	ELSE*/
//		g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons = 0
//		NET_PRINT(" HIDEOUT - HAS_SERVER_CREATED_PICKUPS - KILL PEDS AND TERRITORY CONTROL HAVE NO PACKAGES ") NET_NL()
//	//ENDIF
//	
//	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons i		//MAX_NUM_PICKUPS i
//		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.PickUpData[i].NetID)
//			/*IF serverBD.eHideoutVariation = HIDEOUT_PACKAGE_STEAL
//				vPos = serverBD.SafeData.vSafeDoor
//				vPos.z = -100
//			ELSE*/
//				vPos = g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].vPos
//			//ENDIF
//			
//			IF NOT IS_VECTOR_ZERO(vPos)
//			AND g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].pt <> NUM_PICKUPS
//				REQUEST_MODEL(serverBD.PickUpData[i].Model)
//				IF HAS_MODEL_LOADED(serverBD.PickUpData[i].Model)
//				AND CAN_REGISTER_MISSION_ENTITIES(0, 0, 2, 0)	
//					NET_PRINT(" HIDEOUT - HAS_SERVER_CREATED_PICKUPS Making pickup: ") NET_PRINT_INT(i) NET_PRINT(" at the coordinates: ") NET_PRINT_VECTOR(vPos) NET_NL()
//					
//					GET_PICKUP_DATA(i)
//										
//					//serverBD.PickUpData[i] = CREATE_PICKUP_ROTATE(PICKUP_MONEY_CASE, vPos + <<0,0,0.10>>, <<0,360,0>>, iPlacementFlags, CASH_PICKUP_AMOUNT)
//					serverBD.PickUpData[i].NetID = OBJ_TO_NET(CREATE_PORTABLE_PICKUP(PICKUP_PORTABLE_PACKAGE, vPos, TRUE, serverBD.PickUpData[i].Model))
//					
//					SET_ENTITY_INVINCIBLE(NET_TO_OBJ(serverBD.PickUpData[i].NetID), TRUE)
//					
//					/*IF serverBD.eHideoutVariation = HIDEOUT_PACKAGE_STEAL
//						PREVENT_COLLECTION_OF_PORTABLE_PICKUP(NET_TO_OBJ(serverBD.PickUpData[i].NetID), TRUE)
//						NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - PREVENT_COLLECTION_OF_PORTABLE_PICKUP - TRUE ") NET_NL()
//					ENDIF*/
//					
//					/*IF NOT DOES_BLIP_EXIST(biPickup[i])
//						biPickup[i] = ADD_BLIP_FOR_ENTITY(NET_TO_OBJ(serverBD.PickUpData[i].NetID))
//						SET_BLIP_SPRITE(biPickup[i], RADAR_TRACE_CASH_PICKUP)	//GET_CORRECT_BLIP_SPRITE_FMMC(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].pt))
//						SET_BLIP_SCALE(biPickup[i], BLIP_SIZE_NETWORK_PICKUP)	//1.5)
//						SET_BLIP_AS_SHORT_RANGE(biPickup[i], TRUE)
//						SET_BLIP_PRIORITY(biPickup[i], BLIPPRIORITY_LOW_MED)
//						SHOW_HEIGHT_ON_BLIP(biPickup[i], TRUE)	//NET_SHOW_HEIGHT_ON_BLIP(biPickup[i])
//						NET_PRINT(" HIDEOUT - CONTROL_PICKUP_BLIPS - A - BLIP ADDED ") NET_PRINT_INT(i) NET_NL()
//					ENDIF*/
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDREPEAT
//	
//	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons i		//MAX_NUM_PICKUPS i
//		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.PickUpData[i].NetID)
//			IF NOT IS_VECTOR_ZERO(vPos)
//			AND g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].pt <> NUM_PICKUPS
//				RETURN FALSE
//			ENDIF
//		ENDIF
//	ENDREPEAT
//	
//	RETURN TRUE
//ENDFUNC

//PURPOSE: Create Health Pickups
//FUNC BOOL HAS_CLIENT_CREATED_HEALTH_PICKUPS()
//
//	INT iPlacementFlags = 0
//	SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_SNAP_TO_GROUND))
//	SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ORIENT_TO_GROUND))
//	SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_MAP))
//	SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_FIXED))
//    CLEAR_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_REGENERATES))
//
//	
//	INT i
//	VECTOR vPos
//
//	IF g_FMMC_STRUCT_ENTITIES.iNumberOfProps= 0
//		RETURN TRUE
//	ENDIF
//		
//	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfProps i
//		IF NOT DOES_PICKUP_EXIST(piHealth[i])
//			vPos = g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].vPos
//			IF NOT IS_VECTOR_ZERO(vPos)
//			//AND g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].pt <> NUM_PICKUPS
//				REQUEST_MODEL(PROP_LD_HEALTH_PACK)
//				IF HAS_MODEL_LOADED(PROP_LD_HEALTH_PACK)
//				AND CAN_REGISTER_MISSION_ENTITIES(0, 0, 0, 2)	
//					NET_PRINT(" HIDEOUT - HAS_SERVER_CREATED_HEALTH_PICKUPS Making pickup: ") NET_PRINT_INT(i) NET_PRINT(" at the coordinates: ") NET_PRINT_VECTOR(vPos) NET_NL()
//					
//					//serverBD.PickUpData[i] = CREATE_PICKUP_ROTATE(PICKUP_MONEY_CASE, vPos + <<0,0,0.10>>, <<0,360,0>>, iPlacementFlags, CASH_PICKUP_AMOUNT)
//					piHealth[i] = (CREATE_PICKUP(PICKUP_HEALTH_STANDARD, vPos, iPlacementFlags))
//					SET_PICKUP_GLOW_OFFSET(piHealth[i], 0.0) 
//					
//					//SET_ENTITY_INVINCIBLE(NET_TO_OBJ(serverBD.niHealth), TRUE)
//					
//					/*IF NOT DOES_BLIP_EXIST(biPickup[i])
//						biPickup[i] = ADD_BLIP_FOR_ENTITY(NET_TO_OBJ(serverBD.PickUpData[i].NetID))
//						SET_BLIP_SPRITE(biPickup[i], RADAR_TRACE_CASH_PICKUP)	//GET_CORRECT_BLIP_SPRITE_FMMC(g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].pt))
//						SET_BLIP_SCALE(biPickup[i], BLIP_SIZE_NETWORK_PICKUP)	//1.5)
//						SET_BLIP_AS_SHORT_RANGE(biPickup[i], TRUE)
//						SET_BLIP_PRIORITY(biPickup[i], BLIPPRIORITY_LOW_MED)
//						SHOW_HEIGHT_ON_BLIP(biPickup[i], TRUE)	//NET_SHOW_HEIGHT_ON_BLIP(biPickup[i])
//						NET_PRINT(" HIDEOUT - CONTROL_PICKUP_BLIPS - A - BLIP ADDED ") NET_PRINT_INT(i) NET_NL()
//					ENDIF*/
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDREPEAT
//	
//	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfProps i		//MAX_NUM_PICKUPS i
//		IF NOT DOES_PICKUP_EXIST(piHealth[i])
//			IF NOT IS_VECTOR_ZERO(vPos)
//			//AND g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].pt <> NUM_PICKUPS
//				RETURN FALSE
//			ENDIF
//		ENDIF
//	ENDREPEAT
//	
//	RETURN TRUE
//ENDFUNC
//
//PROC CONTROL_HEALTH_PICKUP_BLIPS()
//    
//    IF DOES_PICKUP_EXIST(piHealth[iStaggeredHealthPickUpBlipCount])
//        IF NOT DOES_PICKUP_OBJECT_EXIST(piHealth[iStaggeredHealthPickUpBlipCount])
//            IF DOES_BLIP_EXIST(biPickupBlip[iStaggeredHealthPickUpBlipCount])
//                REMOVE_BLIP(biPickupBlip[iStaggeredHealthPickUpBlipCount])
//				NET_PRINT(" HIDEOUT - CONTROL_HEALTH_PICKUP_BLIPS - A - BLIP REMOVED ") NET_PRINT_INT(iStaggeredHealthPickUpBlipCount) NET_NL()
//            ENDIF
//        ELSE
//            IF NOT DOES_BLIP_EXIST(biPickupBlip[iStaggeredHealthPickUpBlipCount])
//                biPickupBlip[iStaggeredHealthPickUpBlipCount]=ADD_BLIP_FOR_PICKUP(piHealth[iStaggeredHealthPickUpBlipCount])
//                SET_BLIP_SPRITE(biPickupBlip[iStaggeredHealthPickUpBlipCount], GET_CORRECT_BLIP_SPRITE_FMMC(PICKUP_HEALTH_STANDARD))
//                SET_BLIP_SCALE(biPickupBlip[iStaggeredHealthPickUpBlipCount],1.5)
//                SET_BLIP_AS_SHORT_RANGE(biPickupBlip[iStaggeredHealthPickUpBlipCount],TRUE)
//                SHOW_HEIGHT_ON_BLIP(biPickupBlip[iStaggeredHealthPickUpBlipCount], TRUE)
//				NET_PRINT(" HIDEOUT - CONTROL_HEALTH_PICKUP_BLIPS - BLIP ADDED ") NET_PRINT_INT(iStaggeredHealthPickUpBlipCount) NET_NL()
//            ENDIF
//        ENDIF
//    ELSE
//        IF DOES_BLIP_EXIST(biPickupBlip[iStaggeredHealthPickUpBlipCount])
//            REMOVE_BLIP(biPickupBlip[iStaggeredHealthPickUpBlipCount])
//			NET_PRINT(" HIDEOUT - CONTROL_HEALTH_PICKUP_BLIPS - B - BLIP REMOVED ") NET_PRINT_INT(iStaggeredHealthPickUpBlipCount) NET_NL()
//        ENDIF
//    ENDIF
//    
//    iStaggeredHealthPickUpBlipCount++
//    
//    IF iStaggeredHealthPickUpBlipCount >= g_FMMC_STRUCT_ENTITIES.iNumberOfProps
//        iStaggeredHealthPickUpBlipCount = 0
//    ENDIF
//    
//ENDPROC

//PURPOSE: Retursn TRUE if the Safe has been cracked
//FUNC BOOL HAS_SAFE_BEEN_CRACKED()
//	IF IS_BIT_SET(serverBD.iServerBitSet, biS_SafeUnlocked)
//	OR IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_HasUnlockedSafe)
//		RETURN TRUE
//	ENDIF
//
//	RETURN FALSE
//ENDFUNC

//PURPOSE: Cleans up the Pickup blips if the pickup no longer exists
//PROC CONTROL_PICKUP_BLIPS(INT i)
//	//INT i
//	//REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons i		//MAX_NUM_PICKUPS i
//		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.PickUpData[i].NetID)
//		OR playerBD[PARTICIPANT_ID_TO_INT()].eStage >= HIDEOUT_STAGE_REWARDS_AND_FEEDACK
//		OR ( serverBD.eHideoutVariation = HIDEOUT_PACKAGE_STEAL AND playerBD[PARTICIPANT_ID_TO_INT()].eStage = HIDEOUT_STAGE_SECONDARY )
//			IF DOES_BLIP_EXIST(biPickup[i])
//				REMOVE_BLIP(biPickup[i])
//				NET_PRINT(" HIDEOUT - CONTROL_PICKUP_BLIPS - B - BLIP REMOVED ") NET_PRINT_INT(i) NET_NL()
//			ENDIF
//		ELSE
//			IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.PickUpData[i].NetID)
//				
//				IF serverBD.PickUpData[i].iParticpantGotPickUp != PARTICIPANT_ID_TO_INT()
//				AND ( serverBD.eHideoutVariation = HIDEOUT_PACKAGE_STEAL AND HAS_SAFE_BEEN_CRACKED() )
//					IF NOT DOES_BLIP_EXIST(biPickup[i])
//						biPickup[i] = ADD_BLIP_FOR_ENTITY(NET_TO_OBJ(serverBD.PickUpData[i].NetID))
//						SET_BLIP_SPRITE(biPickup[i], RADAR_TRACE_GANG_ATTACK_PACKAGE)	//GET_CORRECT_BLIP_SPRITE_FMMC(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].pt))
//						SET_BLIP_SCALE(biPickup[i], BLIP_SIZE_NETWORK_PICKUP)	//1.5)
//						IF serverBD.eHideoutVariation != HIDEOUT_PACKAGE_STEAL
//							SET_BLIP_AS_SHORT_RANGE(biPickup[i], TRUE)
//							SET_BLIP_PRIORITY(biPickup[i], BLIPPRIORITY_LOWEST)
//							NET_PRINT(" HIDEOUT - CONTROL_PICKUP_BLIPS - B - BLIPPRIORITY_LOWEST - BLIP  ") NET_PRINT_INT(i) NET_NL()
//						ELSE
//							SET_BLIP_COLOUR(biPickup[i], BLIP_COLOUR_GREEN)
//						ENDIF
//						SHOW_HEIGHT_ON_BLIP(biPickup[i], TRUE)
//						SET_BLIP_NAME_FROM_TEXT_FILE(biPickup[i], "GHO_PACK")	//Package
//						NET_PRINT(" HIDEOUT - CONTROL_PICKUP_BLIPS - B - BLIP ADDED ") NET_PRINT_INT(i) NET_NL()
//					ENDIF
//					//IF serverBD.eHideoutVariation = HIDEOUT_PACKAGE_STEAL
//					//AND serverBD.PickUpData[i].iParticpantGotPickUp = -1
//					//	DRAW_SPRITE_ON_OBJECTIVE_OBJECT_THIS_FRAME(NET_TO_OBJ(serverBD.PickUpData[i].NetID), MP_TAG_PACKAGES)
//					//ENDIF
//				ELSE
//					IF DOES_BLIP_EXIST(biPickup[i])
//						REMOVE_BLIP(biPickup[i])
//						NET_PRINT(" HIDEOUT - CONTROL_PICKUP_BLIPS - C - BLIP REMOVED ") NET_PRINT_INT(i) NET_NL()
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
//	//ENDREPEAT
//ENDPROC

//PURPOSE: Controls the player collecting a pick up
//PROC CONTROL_PICKUP_COLLECTION(INT i)
//	IF NOT IS_BIT_SET(iBoolsBitSet, biPickUpDone1+i)
//		IF serverBD.PickUpData[i].iParticpantGotPickUp = -1
//			
//			//Check if I have picked it up.
//			IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_CollectedPickUp1+i)
//				IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.PickUpData[i].NetID)
//					IF IS_ENTITY_ATTACHED_TO_ENTITY(NET_TO_OBJ(serverBD.PickUpData[i].NetID), PLAYER_PED_ID())
//						
//						SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_CollectedPickUp1+i)
//						SET_BIT(iBoolsBitSet, biPickUpDone1+i)
//						SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_HasPackage)
//						NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - CONTROL_PICKUP_COLLECTION - PLAYER HAS COLLECTED PACKAGE ") NET_PRINT_INT(i) NET_NL()
//						
//						SCRIPT_EVENT_DATA_TICKER_MESSAGE TickerEventData
//						TickerEventData.TickerEvent = TICKER_EVENT_GANG_HIDEOUT_PLAYER_PICKED_UP
//						TickerEventData.playerID = PLAYER_ID()
//						BROADCAST_TICKER_EVENT(TickerEventData, ALL_PLAYERS_ON_SCRIPT(FALSE))
//						
//						IF serverBD.eHideoutVariation != HIDEOUT_PACKAGE_STEAL
//							GIVE_PICKUP_CONTENTS(i)
//							IF TAKE_CONTROL_OF_NET_ID(serverBD.PickUpData[i].NetID)
//								DELETE_NET_ID(serverBD.PickUpData[i].NetID)
//								NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - CONTROL_PICKUP_COLLECTION - DELETED PACKAGE ") NET_PRINT_INT(i) NET_NL()
//							ENDIF
//						ELSE
//							PRINT_TICKER("GHO_TPUPA")	//You picked up a Gang Attack package.
//							UPDATE_INVENTORY_BOX_CONTENT(1, MP_TAG_PACKAGES, TRUE)
//							NET_PRINT("    ----->     HIDEOUT - UPDATE_INVENTORY_BOX_CONTENT - A") NET_NL()
//						ENDIF
//					ELSE
//						SET_BIT(iBoolsBitSet, biAnyPackageDropped)
//					ENDIF
//					
//				ENDIF
//			ENDIF
//			
//		ELSE
//			//Give Shared Pick Up Cash
//			IF serverBD.eHideoutVariation != HIDEOUT_PACKAGE_STEAL
//				INT iCash = MULTIPLY_CASH_BY_TUNABLE(serverBD.PickUpData[i].iContentsAmount)
//				GIVE_LOCAL_PLAYER_CASH(iCash/serverBD.iNumParticpants)
//				NETWORK_EARN_FROM_TAKEOVER_GANG_TURF(iCash/serverBD.iNumParticpants)
//				NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - CONTROL_PICKUP_COLLECTION - GIVE PLAYER SHARED CASH ") NET_PRINT_INT(i) NET_NL()
//				NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - CONTROL_PICKUP_COLLECTION - CASH = ") NET_PRINT_INT(iCash/serverBD.iNumParticpants) NET_NL()
//			ENDIF
//			
//			/*IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(serverBD.PickUpData[i].iParticpantGotPickUp))
//				PRINT_TICKER_WITH_PLAYER_NAME("GHO_TPLAY", NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(serverBD.PickUpData[i].iParticpantGotPickUp)))	//~a~ ~s~picked up a package.
//			ELSE
//				PRINT_TICKER_WITH_PLAYER_NAME("GHO_TPLAA", NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(serverBD.PickUpData[i].iParticpantGotPickUp)))	//~s~A Gang Attack package was picked up.
//			ENDIF*/
//			SET_BIT(iBoolsBitSet, biPickUpDone1+i)
//			NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - CONTROL_PICKUP_COLLECTION - OTHER PLAYER COLLECTED PACKAGE ") NET_PRINT_INT(i) NET_NL()
//		ENDIF
//		
//	//Control Dropping this package
//	ELSE
//		IF serverBD.eHideoutVariation = HIDEOUT_PACKAGE_STEAL
//			IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.PickUpData[i].NetID)
//				IF serverBD.PickUpData[i].iParticpantGotPickUp = -1
//				AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_CollectedPickUp1+i)
//					CLEAR_BIT(iBoolsBitSet, biPickUpDone1+i)
//					NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - CONTROL_PICKUP_COLLECTION - CLEARED biPickUpDone1 ") NET_PRINT_INT(i) NET_NL()
//				ELSE
//					//Check if player has dropped the package
//					IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_CollectedPickUp1+i)
//						IF NOT IS_ENTITY_ATTACHED_TO_ENTITY(NET_TO_OBJ(serverBD.PickUpData[i].NetID), PLAYER_PED_ID())
//							CLEAR_A_INVENTORY_BOX_CONTENT(1)
//							NET_PRINT("    ----->     HIDEOUT - CLEAR_A_INVENTORY_BOX_CONTENT - B") NET_NL()
//							CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_CollectedPickUp1+i)
//							CLEAR_BIT(iBoolsBitSet, biPickUpDone1+i)
//							NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - CONTROL_PICKUP_COLLECTION - I DROPPED THE PACKAGE ") NET_PRINT_INT(i) NET_NL()
//						ELSE
//							SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_HasPackage)
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
//ENDPROC

//PURPOSE: Controls the player delivery a package to a Drop Off
//PROC CONTROL_PACKAGE_DELIVERY(INT i)
//	IF serverBD.eHideoutVariation = HIDEOUT_PACKAGE_STEAL
//		IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DeliveredPackage1+i)
//		AND NOT IS_BIT_SET(iBoolsBitSet, biPackageDeliveryRewardDone1+i)	
//			//Do Delivery
//			IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_CollectedPickUp1+i)
//				IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.PickUpData[i].NetID)
//					IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), serverBD.vPackageDropOff, <<1.5, 1.5, LOCATE_SIZE_HEIGHT>>, TRUE, TRUE, TM_ON_FOOT)
//						//GIVE_LOCAL_PLAYER_CASH((serverBD.PickUpData[i].iContentsAmount/serverBD.iNumParticpants)*2)
//						//NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - CONTROL_PACKAGE_DELIVERY - CASH REWARD = ") NET_PRINT_INT((serverBD.PickUpData[i].iContentsAmount/serverBD.iNumParticpants)*2) NET_NL()
//						PRINT_TICKER("GHO_TDEPA")	//You delivered a Hideout package.
//						
//						SCRIPT_EVENT_DATA_TICKER_MESSAGE TickerEventData
//						TickerEventData.TickerEvent = TICKER_EVENT_GANG_HIDEOUT_PLAYER_DELIVERY
//						TickerEventData.playerID = PLAYER_ID()
//						TickerEventData.dataInt = ((serverBD.PickUpData[i].iContentsAmount/serverBD.iNumParticpants)*2)
//						BROADCAST_TICKER_EVENT(TickerEventData, ALL_PLAYERS_ON_SCRIPT(FALSE))
//						
//						SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DeliveredPackage1+i)
//						SET_BIT(iBoolsBitSet, biPackageDeliveryRewardDone1+i)
//						CLEAR_A_INVENTORY_BOX_CONTENT(1)
//						NET_PRINT("    ----->     HIDEOUT - CLEAR_A_INVENTORY_BOX_CONTENT - C") NET_NL()
//						NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - CONTROL_PACKAGE_DELIVERY - I HAVE DELIVERED PACKAGE ") NET_PRINT_INT(i) NET_NL()
//						IF TAKE_CONTROL_OF_NET_ID(serverBD.PickUpData[i].NetID)
//							DELETE_NET_ID(serverBD.PickUpData[i].NetID)
//							NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - CONTROL_PACKAGE_DELIVERY - DELETED PACKAGE ") NET_PRINT_INT(i) NET_NL()
//						ENDIF
//						
//						//Give Cash - ONLY TO THIS PLAYER NOW
//						INT iCashReward = ((CASH_PACKAGE_AMOUNT*5)*serverBD.iNumParticpants)
//						iCashReward = MULTIPLY_CASH_BY_TUNABLE(iCashReward)
//						GIVE_LOCAL_PLAYER_CASH(iCashReward)
//						NETWORK_EARN_FROM_TAKEOVER_GANG_TURF(iCashReward)
//						SET_CASH_REWARD_FOR_ENTITY(PLAYER_PED_ID(), iCashReward)
//						SET_LAST_JOB_DATA(eFM_GANG_ATTACK_CLOUD, iCashReward)
//						NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - CONTROL_PACKAGE_DELIVERY - PACKAGE DELIVERED REWARD = ") NET_PRINT_INT(iCashReward) NET_NL()
//						
//					ENDIF
//				ENDIF
//			ENDIF
//			
//			//Do Reward
//			IF serverBD.PickUpData[i].iParticpantDeliveredPackage != -1
//				//GIVE_LOCAL_PLAYER_CASH((serverBD.PickUpData[i].iContentsAmount/serverBD.iNumParticpants)*2)
//				//PRINT_TICKER_WITH_PLAYER_NAME("GHO_TPLDP", NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(serverBD.PickUpData[i].iParticpantDeliveredPackage)))	//~a~ ~s~delivered a Hideout package.
//				SET_BIT(iBoolsBitSet, biPackageDeliveryRewardDone1+i)
//				NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - CONTROL_PACKAGE_DELIVERY - REMOTE PLAYER HAS DELIVERED PACKAGE ") NET_PRINT_INT(i) NET_NL()
//			ENDIF
//		ENDIF
//	ENDIF
//ENDPROC

//PURPOSE: Controls the help text for the Hideout
PROC CONTROL_HELP_TEXT()
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF !SHOULD_HIDE_GANG_ATTACK_UI()
			//Do initial help text
			IF NOT IS_BIT_SET(MPGlobalsAmbience.HideoutData.iBitSet, biGHO_InitialHelpTextDone)
				INT iStatInt = GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_NON_MS_HELP_TEXT5)
				IF NOT IS_BIT_SET(iStatInt, biNMH5_GangHideoutHelp1)
				OR NOT IS_BIT_SET(iStatInt, biNMH5_GangHideoutHelp2)
				OR NOT IS_BIT_SET(iStatInt, biNMH5_GangHideoutHelp3)
				OR NOT IS_BIT_SET(iStatInt, biNMH5_GangHideoutHelp4)
					IF IS_NET_PLAYER_OK(PLAYER_ID())
						IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
						AND NOT IS_CUSTOM_MENU_ON_SCREEN()
						AND NOT IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()
						AND NOT IS_PAUSE_MENU_ACTIVE()
						AND NOT SHOULD_HIDE_GANG_ATTACK_UI()
							TEXT_LABEL_15 tlHelp = "GHO_HELP"
							tlHelp += ENUM_TO_INT(serverBD.eHideoutVariation)
							PRINT_HELP(tlHelp)	//~s~There are various gang hideouts across the city.~n~......
							
							IF NOT IS_BIT_SET(iStatInt, biNMH5_GangHideoutHelp1)
								SET_BIT(iStatInt, biNMH5_GangHideoutHelp1)
							ELIF NOT IS_BIT_SET(iStatInt, biNMH5_GangHideoutHelp2)
								SET_BIT(iStatInt, biNMH5_GangHideoutHelp2)
							ELIF NOT IS_BIT_SET(iStatInt, biNMH5_GangHideoutHelp3)
								SET_BIT(iStatInt, biNMH5_GangHideoutHelp3)
							ELIF NOT IS_BIT_SET(iStatInt, biNMH5_GangHideoutHelp4)
								SET_BIT(iStatInt, biNMH5_GangHideoutHelp4)
							ENDIF			
							
							SET_BIT(MPGlobalsAmbience.HideoutData.iBitSet, biGHO_InitialHelpTextDone)
							NET_PRINT_TIME() NET_PRINT("     ---------->     HIDEOUT - CONTROL_HELP_TEXT - biGHO_InitialHelpTextDone ") NET_NL()
						ENDIF
					ENDIF
				ELSE
					SET_BIT(MPGlobalsAmbience.HideoutData.iBitSet, biGHO_InitialHelpTextDone)
					NET_PRINT_TIME() NET_PRINT("     ---------->     HIDEOUT - CONTROL_HELP_TEXT - ALREADY DONE - biGHO_InitialHelpTextDone ") NET_NL()
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Adds a blip to the drop off lcoation
//PROC ADD_DROP_OFF_BLIP()
//	IF NOT DOES_BLIP_EXIST(biDropOff)
//		biDropOff = ADD_BLIP_FOR_COORD(serverBD.vPackageDropOff)
//		SET_BLIP_SCALE(biDropOff, BLIP_SIZE_NETWORK_COORD)
//	    SET_BLIP_COLOUR(biDropOff, BLIP_COLOUR_YELLOW)
//		SET_BLIP_ROUTE(biDropOff, TRUE)
//		NET_PRINT_TIME() NET_PRINT("     ---------->     HIDEOUT - ADD_DROP_OFF_BLIP - DONE") NET_NL()
//	ENDIF
//ENDPROC

//PURPOSE: Removes the drop off blip
//PROC REMOVE_DROP_OFF_BLIP()
//	IF DOES_BLIP_EXIST(biDropOff)
//		REMOVE_BLIP(biDropOff)
//		NET_PRINT_TIME() NET_PRINT("     ---------->     HIDEOUT - REMOVE_DROP_OFF_BLIP - DONE") NET_NL()
//	ENDIF
//ENDPROC


//PURPOSE: Adds a blip to the Safe
//PROC ADD_SAFE_BLIP()
//	IF NOT DOES_BLIP_EXIST(SafeBlip)
//		SafeBlip = ADD_BLIP_FOR_ENTITY(NET_TO_OBJ(serverBD.SafeData.NetID))
//		SET_BLIP_SCALE(SafeBlip, BLIP_SIZE_NETWORK_OBJECT)
//	    SET_BLIP_SPRITE(SafeBlip, RADAR_TRACE_CASH_PICKUP)
//	    SET_BLIP_COLOUR(SafeBlip, BLIP_COLOUR_GREEN)
//		SET_BLIP_NAME_FROM_TEXT_FILE(SafeBlip, "GHO_SAFEB")
//		SHOW_HEIGHT_ON_BLIP(SafeBlip, TRUE)
//		//SET_BLIP_ROUTE(SafeBlip, TRUE)
//		NET_PRINT_TIME() NET_PRINT("     ---------->     HIDEOUT - ADD_SAFE_BLIP - DONE") NET_NL()
//	ENDIF
//ENDPROC

//PURPOSE: Removes the Safe blip
//PROC REMOVE_SAFE_BLIP()
//	IF DOES_BLIP_EXIST(SafeBlip)
//		REMOVE_BLIP(SafeBlip)
//		NET_PRINT_TIME() NET_PRINT("     ---------->     HIDEOUT - REMOVE_SAFE_BLIP - DONE") NET_NL()
//	ENDIF
//ENDPROC

//PURPOSE: Adds a radius blip for the hideout area.
//PROC ADD_AREA_RADIUS_BLIP()
//	IF NOT DOES_BLIP_EXIST(biHideoutArea)
//		biHideoutArea = ADD_BLIP_FOR_RADIUS(serverBD.vHideoutCentre, serverBD.fHideoutRadius)
//		//biHideoutArea = ADD_BLIP_FOR_AREA(serverBD.vHideoutCentre, serverBD.fHideoutRadius)
//		//SET_BLIP_ROTATION(biHideoutArea, ROUND(GET_HIDEOUT_ROTATION()))
//		
//	    SET_BLIP_COLOUR(biHideoutArea, BLIP_COLOUR_YELLOW)
//		SET_BLIP_ALPHA(biHideoutArea, 150)	//PATROL_BLIP_ALPHA)	//220
//        SET_BLIP_AS_SHORT_RANGE(biHideoutArea, TRUE)
//       	SET_BLIP_PRIORITY(biHideoutArea, BLIPPRIORITY_LOW)
//		NET_PRINT_TIME() NET_PRINT("     ---------->     HIDEOUT - ADD_BLIP_FOR_AREA - DONE") NET_NL()
//	ENDIF
//ENDPROC

//PURPOSE: Removes the radius blip for the hideout area.
//PROC REMOVE_AREA_RADIUS_BLIP()
//	IF DOES_BLIP_EXIST(biHideoutArea)
//		REMOVE_BLIP(biHideoutArea)
//		NET_PRINT_TIME() NET_PRINT("     ---------->     HIDEOUT - REMOVE_AREA_RADIUS_BLIP - DONE") NET_NL()
//	ENDIF
//ENDPROC

//PURPOSE: Control the God Text display
PROC CONTROL_GOD_TEXT()
	IF GET_CLIENT_HIDEOUT_STAGE(PARTICIPANT_ID()) > HIDEOUT_STAGE_WARNING	//>= HIDEOUT_STAGE_WARNING
	AND GET_CLIENT_HIDEOUT_STAGE(PARTICIPANT_ID()) <= HIDEOUT_STAGE_PRIMARY
		IF !SHOULD_HIDE_GANG_ATTACK_UI()
		//IF serverBD.eHideoutVariation != HIDEOUT_PACKAGE_STEAL
		//OR IS_BIT_SET(iBoolsBitSet, biAnyPackageDropped)
			IF NOT IS_BIT_SET(iBoolsBitSet, biInitialGodText)
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					//REMOVE_DROP_OFF_BLIP()
					TEXT_LABEL_15 tlGod = "GHO_MAIN"
					//IF serverBD.eHideoutVariation != HIDEOUT_PACKAGE_STEAL
						tlGod += ENUM_TO_INT(serverBD.eHideoutVariation)
					/*ELSE
						IF NOT HAS_SAFE_BEEN_CRACKED()
							ADD_SAFE_BLIP()
							tlGod = "GHO_SECO0"
						ELSE
							REMOVE_SAFE_BLIP()
							tlGod += ENUM_TO_INT(serverBD.eHideoutVariation)
						ENDIF
					ENDIF*/
					
					
					
					Print_Objective_Text(tlGod)	//~s~Kill the ~r~hostiles.
					SET_BIT(iBoolsBitSet, biInitialGodText)
					CLEAR_BIT(iBoolsBitSet, biAlternativeGodText)
					CLEAR_BIT(iBoolsBitSet, biSecondaryGodText)
					NET_PRINT_TIME() NET_PRINT("     ---------->     HIDEOUT - CONTROL_GOD_TEXT - biInitialGodText ") NET_NL()
				ENDIF
				
				/*IF serverBD.eHideoutVariation = HIDEOUT_TERRITORY_CONTROL
					ADD_AREA_RADIUS_BLIP()
				ENDIF*/
			ELSE
				IF serverBD.eHideoutVariation = HIDEOUT_KILL_PEDS
				AND (serverBD.iKillGoal-serverBD.iTotalKills) <= 1
					Print_Objective_Text("GHO_REMAIN")
				ENDIF
			ENDIF
		/*ELSE
			IF NOT IS_BIT_SET(iBoolsBitSet, biAlternativeGodText)
				ADD_DROP_OFF_BLIP()
				Print_Objective_Text("GHO_MAIN2B")	//~s~Escort a ~BLIP_CASH_PICKUP~ package to the ~y~drop off.
				SET_BIT(iBoolsBitSet, biAlternativeGodText)
				CLEAR_BIT(iBoolsBitSet, biInitialGodText)
				CLEAR_BIT(iBoolsBitSet, biSecondaryGodText)
				NET_PRINT_TIME() NET_PRINT("     ---------->     HIDEOUT - CONTROL_GOD_TEXT - biAlternativeGodText ") NET_NL()
			ENDIF
		ENDIF*/
		ENDIF
	ELIF GET_CLIENT_HIDEOUT_STAGE(PARTICIPANT_ID()) = HIDEOUT_STAGE_SECONDARY
		/*IF serverBD.eHideoutVariation = HIDEOUT_TERRITORY_CONTROL 
		AND serverBD.iNumAlivePeds <= 0
			EXIT
		ENDIF*/
		
		IF NOT IS_BIT_SET(iBoolsBitSet, biSecondaryGodText)
//			/*IF serverBD.eHideoutVariation = HIDEOUT_PACKAGE_STEAL
//				REMOVE_SAFE_BLIP()
//				ADD_DROP_OFF_BLIP()
//			ENDIF*/
//			IF serverBD.eHideoutVariation = HIDEOUT_KILL_PEDS
//				ADD_SAFE_BLIP()
//				CLEAR_SPAWN_AREA()
//				SET_MISSION_SPAWN_ANGLED_AREA(serverBD.MainArea.vMin, serverBD.MainArea.vMax, serverBD.MainArea.fWidth)
//				//SET_MISSION_SPAWN_SPHERE(serverBD.vHideoutCentre, serverBD.fHideoutRadius)
//				//SET_MISSION_SPAWN_OCCLUSION_SPHERE(serverBD.vHideoutCentre, serverBD.fHideoutRadius*0.5)
//				NET_PRINT_TIME() NET_PRINT("     ---------->     HIDEOUT - CONTROL_GOD_TEXT - SET NEW SPAWN AREA ") NET_NL()
//			ENDIF
//			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("GHO_WARN")
//				CLEAR_HELP()
//			ENDIF
//			TEXT_LABEL_15 tlGod = "GHO_SECO"
//			tlGod += ENUM_TO_INT(serverBD.eHideoutVariation)
//			IF ENUM_TO_INT(serverBD.eHideoutVariation) = 2
//				tlGod += serverBD.iStealPackageGang
//			ENDIF
//			
//			/*IF serverBD.eHideoutVariation = HIDEOUT_TERRITORY_CONTROL
//			AND serverBD.iNumAlivePeds <= 1
//				tlGod = "GHO_REMAIN"
//				Print_Objective_Text(tlGod)	//~s~Kill reaminaing peds.
//			EL*/IF serverBD.eHideoutVariation = HIDEOUT_KILL_PEDS 
//			AND NOT HAS_SAFE_BEEN_CRACKED()
//				Print_Objective_Text(tlGod)	//~s~Crack the ~g~safe.
//			/*ELSE
//				Print_Objective_Text(tlGod)	//~s~Deliver package.*/
//			ENDIF
			
			Clear_Any_Objective_Text_From_This_Script()
			
			SET_BIT(iBoolsBitSet, biSecondaryGodText)
			CLEAR_BIT(iBoolsBitSet, biInitialGodText)
			CLEAR_BIT(iBoolsBitSet, biAlternativeGodText)
			NET_PRINT_TIME() NET_PRINT("     ---------->     HIDEOUT - CONTROL_GOD_TEXT - biSecondaryGodText ") NET_NL()
		/*ELSE
			IF serverBD.eHideoutVariation = HIDEOUT_TERRITORY_CONTROL
			AND serverBD.iNumAlivePeds <= 1
				Print_Objective_Text("GHO_REMAIN")
			ENDIF*/
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Draws a bar in the bottom right to show the progress
PROC DRAW_KILLS_BAR()
	//DRAW_GENERIC_METER(serverBD.iTotalKills, serverBD.iKillGoal, "GHO_KILLB", HUD_COLOUR_RED)
	IF !SHOULD_HIDE_GANG_ATTACK_UI()
		DRAW_GENERIC_BIG_DOUBLE_NUMBER((serverBD.iKillGoal-serverBD.iTotalKills), serverBD.iKillGoal, "GHO_KILLB")
	ENDIF
ENDPROC


//PURPOSE: If requried attachs a portable pickup package to the player once they have cracked the safe.
//FUNC BOOL GIVE_SAFE_PACKAGE_IF_NEEDED()
//	IF serverBD.eHideoutVariation != HIDEOUT_PACKAGE_STEAL
//		RETURN TRUE
//	ENDIF
//	
//	IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.PickUpData[0].NetID)
//		IF TAKE_CONTROL_OF_NET_ID(serverBD.PickUpData[0].NetID)
//			PREVENT_COLLECTION_OF_PORTABLE_PICKUP(NET_TO_OBJ(serverBD.PickUpData[0].NetID), FALSE)
//			ATTACH_PORTABLE_PICKUP_TO_PED(NET_TO_OBJ(serverBD.PickUpData[0].NetID), PLAYER_PED_ID())
//			NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - PREVENT_COLLECTION_OF_PORTABLE_PICKUP - FALSE ") NET_NL()
//			
//			UPDATE_INVENTORY_BOX_CONTENT(1, MP_TAG_PACKAGES, TRUE)
//			NET_PRINT("    ----->     HIDEOUT - UPDATE_INVENTORY_BOX_CONTENT - B") NET_NL()
//			
//			SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_CollectedPickUp1)
//			SET_BIT(iBoolsBitSet, biPickUpDone1)
//			SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_HasPackage)
//			NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - GIVE_SAFE_PACKAGE_IF_NEEDED - PLAYER HAS COLLECTED PACKAGE - B ") NET_NL()
//			
//			NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - GIVE_SAFE_PACKAGE_IF_NEEDED - DONE ") NET_NL()
//			RETURN TRUE
//		ENDIF
//	ENDIF
//		
//	RETURN FALSE
//ENDFUNC

//PURPOSE: Controls the player doing the Safe Crack minigame
//PROC CONTROL_SAFE_CRACKING()
//	IF NOT HAS_SAFE_BEEN_CRACKED()
//		IF NOT IS_BIT_SET(iBoolsBitSet, biSomeoneElseIsCrackingSafe)
//			
//			IF IS_BIT_SET(iBoolsBitSet, biToldToProtectCracker)
//				IF NOT IS_BIT_SET(iBoolsBitSet, biAnyoneCrackingSafe)
//					Clear_Any_Objective_Text_From_This_Script()
//					IF GET_CLIENT_HIDEOUT_STAGE(PARTICIPANT_ID()) <= HIDEOUT_STAGE_PRIMARY
//						TEXT_LABEL_15 tlGod = "GHO_MAIN"
//						tlGod += ENUM_TO_INT(serverBD.eHideoutVariation)
//						Print_Objective_Text(tlGod)
//					ELSE
//						Print_Objective_Text("GHO_SECO0")	//~s~Crack the ~g~safe.
//					ENDIF
//					CLEAR_BIT(iBoolsBitSet, biToldToProtectCracker)
//					NET_PRINT("    ----->     HIDEOUT - CONTROL_SAFE_CRACKING - CRACK TEXT - B") NET_NL()
//				ENDIF
//			ENDIF
//			
//			PROCESS_PLAYER_SAFE_CRACKING(SafeCrackData)
//
//			IF HAS_PLAYER_CRACKED_SAFE(SafeCrackData)
//				//IF GIVE_SAFE_PACKAGE_IF_NEEDED()
//					IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_HasUnlockedSafe)
//						
//						//IF serverBD.eHideoutVariation != HIDEOUT_PACKAGE_STEAL
//							INT iCashReward = (serverBD.SafeData.iContentsAmount)
//							iCashReward = MULTIPLY_CASH_BY_TUNABLE(iCashReward)
//							GIVE_LOCAL_PLAYER_CASH(iCashReward)
//							NETWORK_EARN_FROM_TAKEOVER_GANG_TURF(iCashReward)
//							SET_CASH_REWARD_FOR_ENTITY(PLAYER_PED_ID(), iCashReward)
//							SET_LAST_JOB_DATA(eFM_GANG_ATTACK_CLOUD, iCashReward)
//							NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - CONTROL_PACKAGE_DELIVERY - SAFE CRACKED REWARD = ") NET_PRINT_INT(iCashReward) NET_NL()
//						//ENDIF
//						
//						SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_HasUnlockedSafe)
//						NET_PRINT("    ----->     HIDEOUT - CONTROL_SAFE_CRACKING - biP_HasUnlockedSafe SET") NET_NL()
//					ENDIF
//				//ENDIF
//			ENDIF
//			
//			//-- Track if the local player is cracking the safe
//			IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InSafeUnlockMinigame)
//				IF IS_BIT_SET(SafeCrackData.iBitSet, SC_BS_IS_PLAYER_SAFE_CRACKING)
//					SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InSafeUnlockMinigame)
//					//SET_EVERYONE_IGNORE_PLAYER(PLAYER_ID(), TRUE)
//					TRIGGER_MUSIC_EVENT("GA_KILL_SAFE")
//					NET_PRINT("    ----->     HIDEOUT - CONTROL_SAFE_CRACKING - TRIGGER MUSIC - GA_KILL_SAFE") NET_NL()
//				ENDIF
//			ELSE
//				IF NOT IS_BIT_SET(SafeCrackData.iBitSet, SC_BS_IS_PLAYER_SAFE_CRACKING)
//					CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InSafeUnlockMinigame)
//					//SET_EVERYONE_IGNORE_PLAYER(PLAYER_ID(), FALSE)
//					CANCEL_MUSIC_EVENT("GA_KILL_SAFE")
//					NET_PRINT("    ----->     HIDEOUT - CONTROL_SAFE_CRACKING - CANCEL MUSIC - GA_KILL_SAFE") NET_NL()
//					TRIGGER_MUSIC_EVENT("GA_KILL_LEAVE")
//					NET_PRINT("    ----->     HIDEOUT - CONTROL_SAFE_CRACKING - TRIGGER MUSIC - GA_KILL_LEAVE") NET_NL()
//				ENDIF
//			ENDIF
//		ELSE
//			//--Someone else is cracking safe
//			IF NOT IS_BIT_SET(iBoolsBitSet, biToldToProtectCracker)
//				IF iLocalPlayerCrackingSafe > -1
//					CLEAR_SAFE_CRACK_FLOATING_HELP()
//					Clear_Any_Objective_Text_From_This_Script()
//					Print_Objective_Text_With_Player_Name("GHO_SECO0B", INT_TO_PLAYERINDEX(iLocalPlayerCrackingSafe))	//~s~Protect ~a~ ~s~while they crack the ~g~safe.
//					SET_BIT(iBoolsBitSet, biToldToProtectCracker)
//					NET_PRINT("    ----->     HIDEOUT - CONTROL_SAFE_CRACKING - PROTECT TEXT") NET_NL()
//				ENDIF
//			ELSE
//				IF iLocalPlayerCrackingSafe = -1
//					Clear_Any_Objective_Text_From_This_Script()
//					Print_Objective_Text("GHO_SECO0")	//~s~Crack the ~g~safe.
//					CLEAR_BIT(iBoolsBitSet, biToldToProtectCracker)
//					NET_PRINT("    ----->     HIDEOUT - CONTROL_SAFE_CRACKING - CRACK TEXT") NET_NL()
//				ENDIF
//			ENDIF
//		ENDIF
//	
//	ELSE
//		
//		//HIDE_WEAPON_DURING_SAFE_CRACK_OPEN_DOOR_ANIM(SafeCrackData)
//		IF DOES_BLIP_EXIST(SafeBlip)
//			REMOVE_BLIP(SafeBlip)
//		ENDIF
//		
//		//SET_CLIENT_MISSION_STAGE(eMS_GET_CASH_TO_SAFE_HOUSE)
//	ENDIF
//	
//ENDPROC

//PURPOSE: Displays a ticker when the player kills a ped
/*PROC DO_TERRITORY_CONTROL_KILL_TICKER(INT iKillType)	
	IF serverBD.eHideoutVariation = HIDEOUT_TERRITORY_CONTROL
	AND GET_CLIENT_HIDEOUT_STAGE(PARTICIPANT_ID()) = HIDEOUT_STAGE_PRIMARY
		IF iKillType = 0
			PRINT_TICKER("GHO_KPCB")	//Kill +2 ~s~control bonus
			playerBD[PARTICIPANT_ID_TO_INT()].iTotalKillPoints += 2
		ELIF iKillType = 1
			PRINT_TICKER("GHO_PBCB")	//Point Blank +5 ~s~control bonus
			playerBD[PARTICIPANT_ID_TO_INT()].iTotalKillPoints += 5
		ELSE
			PRINT_TICKER("GHO_HSCB")	//Headshot +10 ~s~control bonus
			playerBD[PARTICIPANT_ID_TO_INT()].iTotalKillPoints += 10
		ENDIF
	ENDIF
ENDPROC*/

//PURPOSE: Controls the territory control progress bars
//PROC PROCESS_TERRITORY_CONTROL_CLIENT()
//	IF HAS_NET_TIMER_STARTED(serverBD.TerritoryControlTimer)
//		DRAW_GENERIC_METER(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.TerritoryControlTimer), (2*serverBD.iTakeControlTime), "GHO_CONTR", HUD_COLOUR_YELLOW, -1, HUDORDER_BOTTOM)
//	ENDIF
//ENDPROC

//TIME_DATATYPE iFramerateForHold

//PURPOSE: Controls the territory control progress bars
//FUNC BOOL PROCESS_TERRITORY_CONTROL_SERVER()
//	//INT iteamrepeat
//	//INT iparticipant
//	INT imodifier
//	FLOAT ftimemultiplier
//	//PLAYER_INDEX tempPlayer
//	
//	IF serverBD.iNumParticpantsInArea > 0
//        IF NOT HAS_NET_TIMER_STARTED(serverBD.TerritoryControlTimer)
//            START_NET_TIMER(serverBD.TerritoryControlTimer)
//        ELSE
//			IF HAS_NET_TIMER_EXPIRED(serverBD.TimeSinceLastUpdateTimer, 2000)
//            imodifier = serverBD.iNumParticpantsInArea //serverBD.iNumParticpants
//
//            ftimemultiplier = (TO_FLOAT(imodifier) - TO_FLOAT(1)) / TO_FLOAT(10)
//            //NET_PRINT("number of players in Area: ") NET_PRINT_INT(iloc) NET_PRINT(" for team: ") NET_PRINT_INT(iteam) NET_PRINT(" = ")  NET_PRINT_INT(iNumParticpants) NET_NL()
//            ftimemultiplier =ftimemultiplier*-1
//            IF ftimemultiplier > 0.8
//                ftimemultiplier = 0.8
//            ELIF ftimemultiplier < -0.8
//                ftimemultiplier = -0.8
//            ENDIF
//            ftimemultiplier = 1 - ftimemultiplier 
//
//            //NET_PRINT("setting team number time adjust for Area: ") NET_PRINT_INT(iloc) NET_PRINT(" team: ") NET_PRINT_INT(iteam) NET_PRINT(" multiplier: ")  NET_PRINT_FLOAT(ftimemultiplier) NET_NL()
//            //NET_PRINT("setting offset to timer of: ")  NET_PRINT_INT(ROUND(-1*ftimemultiplier*GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.TimeSinceLastUpdateTimer))) NET_NL()
//            //IF HAS_NET_TIMER_STARTED(serverBD.TimeSinceLastUpdateTimer)
//                serverBD.TerritoryControlTimer.Timer = GET_TIME_OFFSET(serverBD.TerritoryControlTimer.Timer, ROUND(-1*ftimemultiplier*GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.TimeSinceLastUpdateTimer)))
//           		NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - PROCESS_TERRITORY_CONTROL_SERVER - KILL TIMER UPDATE    <----------     ") NET_NL()
//		   // ENDIF
//            //NET_PRINT("time difference from current: ")     NET_PRINT_INT(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.TerritoryControlTimer)) NET_NL()
//            RESET_NET_TIMER(serverBD.TimeSinceLastUpdateTimer)	//REINIT_NET_TIMER
//                        
//           /* IF MC_serverBD.iNumTeamControlKills[iloc][iteam] > MC_serverBD.iOldTeamControlkills[iloc][iteam]
//                //NET_PRINT("old number of kills in prop: ") NET_PRINT_INT(iproperty) NET_PRINT(" for team: ") NET_PRINT_INT(iteamrepeat) NET_PRINT(" kills: ")  NET_PRINT_INT(GP_serverBD.ServerGangProp[iproperty].ioldkillsinprop[iteamrepeat]) NET_NL()
//                //NET_PRINT("new number of kills in prop: ") NET_PRINT_INT(iproperty) NET_PRINT(" for team: ") NET_PRINT_INT(iteamrepeat) NET_PRINT(" kills: ")  NET_PRINT_INT(GP_serverBD.ServerGangProp[iproperty].ikillsinprop[iteamrepeat]) NET_NL()
//                serverBD.TerritoryControlTimer.Timer = GET_TIME_OFFSET(serverBD.TerritoryControlTimer.Timer ,(-1*(MC_serverBD.iNumTeamControlKills[iloc][iteam] - MC_serverBD.iOldTeamControlkills[iloc][iteam])*2*1000))
//                //NET_PRINT("setting kill time adjust for Area: ") NET_PRINT_INT(iloc) NET_PRINT(" team: ") NET_PRINT_INT(iteam) NET_PRINT(" seconds gained: ")  NET_PRINT_INT((MC_serverBD.iNumTeamControlKills[iloc][iteam] - MC_serverBD.iOldTeamControlkills[iloc][iteam])) NET_NL()
//                MC_serverBD.iOldTeamControlkills[iloc][iteam] = MC_serverBD.iNumTeamControlKills[iloc][iteam]
//            ENDIF*/
//			
//            //IF MC_serverBD.iGotoLocationDataPriority[iloc][iteam] < FMMC_MAX_RULES
//                IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.TerritoryControlTimer) >= (2*serverBD.iTakeControlTime) 
//                                  
//                    /*IF iteam = 0
//                        BROADCAST_FMMC_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_TEAM0_CON_AREA)
//                    ELIF iteam = 1
//                        BROADCAST_FMMC_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_TEAM1_CON_AREA)
//                    ELIF iteam = 2
//                        BROADCAST_FMMC_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_TEAM2_CON_AREA)
//                    ELIF iteam = 3
//                        BROADCAST_FMMC_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_TEAM3_CON_AREA)
//                    ENDIF
//                                  
//                    INCREMENT_TEAM_SCORE(iteam)
//
//                    FOR iteamrepeat = 0 TO (MC_serverBD.iNumberOfTeams-1)
//                        MC_serverBD.inumberOfTeamInArea[iloc][iteamrepeat] = 0
//                        RESET_NET_TIMER(tdtimesincelastupdate[iloc][iteamrepeat])
//                        RESET_NET_TIMER(MC_serverBD.tdAreaTimer[iloc][iteamrepeat])
//                        MC_serverBD.iNumTeamControlKills[iloc][iteamrepeat] = 0
//                        MC_serverBD.iOldTeamControlkills[iloc][iteamrepeat] = 0
//                   		IF MC_serverBD.iGotoLocationDataPriority[iloc][iteamrepeat] < FMMC_MAX_RULES
//	                        IF MC_serverBD.iRuleReCapture[iteamrepeat][MC_serverBD.iGotoLocationDataPriority[iloc][iteamrepeat]]<= 0
//	                            MC_serverBD.iGotoLocationDataPriority[iloc][iteamrepeat] = FMMC_PRIORITY_IGNORE
//	                            MC_serverBD_4.iGotoLocationDataRule[iloc][iteamrepeat]  = FMMC_OBJECTIVE_LOGIC_NONE
//	                        ENDIF
//                            IF MC_serverBD.iGotoLocationDataPriority[iloc][iteamrepeat] < FMMC_MAX_RULES
//                                IF MC_serverBD.iRuleReCapture[iteamrepeat][MC_serverBD.iGotoLocationDataPriority[iloc][iteamrepeat]]!= UNLIMITED_CAPTURES
//                                    MC_serverBD.iRuleReCapture[iteamrepeat][MC_serverBD.iGotoLocationDataPriority[iloc][iteamrepeat]]--
//                                ENDIF
//                            ENDIF
//                        ENDIF
//                    ENDFOR
//                                  
//				    REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iparticipant
//				        IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iparticipant))
//				            tempplayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iparticipant))
//				            IF IS_NET_PLAYER_OK(tempplayer)
//				                IF MC_playerBD[iparticipant].iCurrentLoc = iloc
//				                    IF MC_playerBD[iparticipant].iteam = iteam
//				                        BROADCAST_GIVE_PLAYER_XP(tempplayer,200,-1,TRUE)
//				                    ENDIF
//				                ENDIF
//				            ENDIF
//				        ENDIF
//				    ENDREPEAT
//					*/
//				    NET_PRINT("    ----->     HIDEOUT - TERRITORY CONTROL TIMER DONE")  NET_NL()
//				    RETURN TRUE
//                ENDIF
//            ENDIF
//        ENDIF
//    ELSE
//	
//        //IF HAS_NET_TIMER_STARTED(serverBD.TimeSinceLastUpdateTimer)
//        IF HAS_NET_TIMER_EXPIRED(serverBD.TimeSinceLastUpdateTimer, 350)
//		//    //NET_PRINT("reducing take over time for property: ") NET_PRINT_INT(iproperty) NET_PRINT("by: ") NET_PRINT_INT((inetworktimer - itimesincelastupdate[iproperty][iteamrepeat])) NET_NL()
//            serverBD.TerritoryControlTimer.Timer  = GET_TIME_OFFSET(serverBD.TerritoryControlTimer.Timer , GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.TimeSinceLastUpdateTimer))
//        	RESET_NET_TIMER(serverBD.TimeSinceLastUpdateTimer)
//			NET_PRINT("    ----->     HIDEOUT - TERRITORY CONTROL TIMER - RESET - NOBODY IN AREA")  NET_NL()
//		ENDIF
//		
//        //REINIT_NET_TIMER(serverBD.TimeSinceLastUpdateTimer)
//		
//        //NET_PRINT("setting time since last update for property: ") NET_PRINT_INT(iproperty) NET_NL()
//		/*IF HAS_NET_TIMER_STARTED(serverBD.TimeSinceLastUpdateTimer)
//			RESET_NET_TIMER(serverBD.TimeSinceLastUpdateTimer)	//REINIT_NET_TIMER
//		ENDIF*/
//		
//		/*TIME_DATATYPE CurrentTime = GET_NETWORK_TIME() 
//	  	NET_NL()NET_PRINT("HOLD - serverBD.TerritoryControlTimer - increase time by ")NET_PRINT_INT(GET_TIME_DIFFERENCE(CurrentTime, iFramerateForHold))
//	 	serverBD.TerritoryControlTimer = GET_NET_TIMER_OFFSET(serverBD.TerritoryControlTimer, GET_TIME_DIFFERENCE(CurrentTime, iFramerateForHold))
//	 
//	 	iFramerateForHold = GET_NETWORK_TIME()
//		
// 		IF NOT HAS_NET_TIMER_STARTED(serverBD.TerritoryControlTimer)
//            START_NET_TIMER(serverBD.TerritoryControlTimer)
//		ENDIF*/
//    ENDIF
//	
//	RETURN FALSE
//ENDFUNC

/// PURPOSE:
///    Tells if a ped has been killed by a head shot
/// PARAMS:
///    pedID - ped ID
/// RETURNS:
///    True or False
FUNC BOOL CHECK_HEADSHOT(PED_INDEX pedID)

	IF NOT DOES_ENTITY_EXIST(pedID)
		RETURN FALSE
	ENDIF
	
	PED_BONETAG ePedBonetag = BONETAG_NULL
	
	IF IS_ENTITY_DEAD(pedID) OR IS_PED_INJURED(pedID)
	
		IF WAS_PED_KILLED_BY_STEALTH(pedID)
		OR WAS_PED_KILLED_BY_TAKEDOWN(pedID)
			RETURN FALSE
		ENDIF
	
		IF NOT GET_PED_LAST_DAMAGE_BONE(pedID, ePedBonetag)
			RETURN FALSE
		ENDIF
	
		RETURN ePedBonetag = BONETAG_HEAD OR ePedBonetag = BONETAG_NECK
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Tracks the Hideout enemies that the player has killed
PROC TRACK_KILLS(INT iPed)
	IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.sFMMC_SBD.niPed[iPed])
		IF NOT IS_BIT_SET(EndScreenData.iTrackKillsBitSet, biTrackedPedKill+iPed)
			IF IS_ENTITY_DEAD(NET_TO_PED(serverBD.sFMMC_SBD.niPed[iPed]))
				//Check if player was Killer
				WEAPON_TYPE TempWeapon
				IF PLAYER_ID() = NETWORK_GET_DESTROYER_OF_NETWORK_ID(serverBD.sFMMC_SBD.niPed[iPed], TempWeapon)
					//INT iKillType = 0
					EndScreenData.iTrackedKills++
					playerBD[PARTICIPANT_ID_TO_INT()].iKills++
					//iKillStreak++
					
					INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_GHKILLS , 1)
					
					// Get square magnitude of distance - point blank range is 2 meters (2*2 = 4)
					IF IS_NET_PLAYER_OK(PLAYER_ID())
						IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(NET_TO_PED(serverBD.sFMMC_SBD.niPed[iPed]))) < 4.0
							EndScreenData.iPointBlankKills++
							playerBD[PARTICIPANT_ID_TO_INT()].iPointBlanks++
							//iKillType = 1
						ENDIF
					ENDIF
					
					// Check for head shot.
					IF CHECK_HEADSHOT(NET_TO_PED(serverBD.sFMMC_SBD.niPed[iPed]))
						EndScreenData.iHeadshots++
						playerBD[PARTICIPANT_ID_TO_INT()].iHeadShots++
						//iKillType = 2
					ENDIF
					
					//Make sure the XP isn't below the minimum
					/*INT iXP = (serverBD.iAverageRank*3)
					IF iXP < 15
						iXP = 15
					ENDIF*/
					
					//KILL STREAK  - BONUSES
					//iXP = (iXP * iKillStreakMultiplier)
					
					//IF iKillStreakMultiplier > 1				//REMOVED FOR 1074265
					//	iXP += ((iKillStreakMultiplier-1)*50)
					//ENDIF
					
					GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD, NET_TO_PED(serverBD.sFMMC_SBD.niPed[iPed]), "XPT_KAIE", XPTYPE_ACTION, XPCATEGORY_ACTION_HIDEOUT_KILLS, ROUND(10*g_sMPTunables.fxp_tunable_Gang_Attack_enemy_kills), 1)	//REDUCED TO 10XP for BUG 1622514, REDUCED TO 25XP for BUG 1041318
					
					//DO_TERRITORY_CONTROL_KILL_TICKER(iKillType)					
					
					SET_BIT(EndScreenData.iTrackKillsBitSet, biTrackedPedKill+iPed)
					NET_PRINT_TIME() NET_PRINT("     ---------->     TRACK_KILLS - ENEMY KILLED ") NET_PRINT_INT(EndScreenData.iTrackedKills) NET_NL()
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_PED_INJURED(NET_TO_PED(serverBD.sFMMC_SBD.niPed[iPed]))
				CLEAR_BIT(EndScreenData.iTrackKillsBitSet, biTrackedPedKill+iPed)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Loops through the players and finds which one killed the most Gang Peds
PROC GET_PLAYER_WITH_MOST_KILLS()
	
	INT iHighestKills = -1
	INT iParticipant
	
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
			PLAYER_INDEX PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))
			
			IF playerBD[iParticipant].iKills > iHighestKills
				serverBD.PlayerWithMostKills = PlayerId
				iHighestKills = playerBD[iParticipant].iKills
			ENDIF
			
		ENDIF
	ENDREPEAT
	
	NET_PRINT("    ----->     HIDEOUT - GET_PLAYER_WITH_MOST_KILLS - PlayerWithMostKills = ") NET_PRINT(GET_PLAYER_NAME(serverBD.PlayerWithMostKills)) NET_PRINT(" Kills = ") NET_PRINT_INT(iHighestKills) NET_NL()
ENDPROC

// ************************************ \\
//			SETUP AI PEDS	   			\\
// ************************************ \\

//PURPOSE: Sets the ped for a random weapon
FUNC WEAPON_TYPE GET_RANDOM_GANG_PED_WEAPON()
	
	INT iMinRan = 0
	IF serverBD.iDifficultyFromPlayerStats > 25
		iMinRan = ((serverBD.iDifficultyFromPlayerStats/2)-5)
		IF iMinRan > 45
			iMinRan = 45
		ENDIF
	ENDIF
	
	INT iRand = GET_RANDOM_INT_IN_RANGE(iMinRan, 100)
	
	IF iRand < 25
		RETURN WEAPONTYPE_PISTOL
		
	ELIF iRand >= 25
	AND iRand < 50
		RETURN WEAPONTYPE_MICROSMG
		
	ELIF iRand >= 50
	AND iRand < 70
		RETURN WEAPONTYPE_PUMPSHOTGUN
		
	ELIF iRand >= 70
	AND iRand < 90
		RETURN WEAPONTYPE_ASSAULTRIFLE
		
	ELIF iRand >= 90
	AND iRand < 92
		RETURN WEAPONTYPE_SNIPERRIFLE
				
	ELIF iRand >= 92
	AND iRand < 94
		RETURN WEAPONTYPE_MINIGUN
		
	ELSE
		RETURN WEAPONTYPE_MG
	ENDIF
	
ENDFUNC

//PURPOSE: Gives the gang ped a weapon suitable for their gang if possible
FUNC WEAPON_TYPE GET_GANG_PED_WEAPON(INT iPedNumber)
		
	SWITCH serverBD.PedGangModel
	
		//VAGOS
		CASE G_M_Y_MEXGOON_01
		CASE G_M_Y_MEXGOON_02
			SWITCH GET_WEAPONTYPE_GROUP(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].gun)
				CASE WEAPONGROUP_PISTOL		RETURN WEAPONTYPE_PISTOL
				CASE WEAPONGROUP_SHOTGUN	RETURN WEAPONTYPE_PUMPSHOTGUN
				CASE WEAPONGROUP_SMG		RETURN WEAPONTYPE_MICROSMG
				CASE WEAPONGROUP_MG			RETURN WEAPONTYPE_MG
				CASE WEAPONGROUP_RIFLE		RETURN WEAPONTYPE_ASSAULTRIFLE
			ENDSWITCH
		BREAK
	
		//LOST
		CASE G_M_Y_Lost_01
			SWITCH GET_WEAPONTYPE_GROUP(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].gun)
				CASE WEAPONGROUP_PISTOL		RETURN WEAPONTYPE_COMBATPISTOL
				CASE WEAPONGROUP_SHOTGUN	RETURN WEAPONTYPE_SAWNOFFSHOTGUN
				CASE WEAPONGROUP_SMG		RETURN WEAPONTYPE_SMG
				CASE WEAPONGROUP_MG			RETURN WEAPONTYPE_MG
				CASE WEAPONGROUP_RIFLE		RETURN WEAPONTYPE_ASSAULTRIFLE
			ENDSWITCH
		BREAK
	
		//BALLA
		CASE G_M_Y_BallaOrig_01
			SWITCH GET_WEAPONTYPE_GROUP(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].gun)
				CASE WEAPONGROUP_PISTOL		RETURN WEAPONTYPE_COMBATPISTOL
				CASE WEAPONGROUP_SHOTGUN	RETURN WEAPONTYPE_PUMPSHOTGUN
				CASE WEAPONGROUP_SMG		RETURN WEAPONTYPE_MICROSMG
				CASE WEAPONGROUP_MG			RETURN WEAPONTYPE_MG
				CASE WEAPONGROUP_RIFLE		RETURN WEAPONTYPE_ASSAULTRIFLE
			ENDSWITCH
		BREAK
	
		//FAMILY
		CASE G_M_Y_FAMCA_01
			SWITCH GET_WEAPONTYPE_GROUP(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].gun)
				CASE WEAPONGROUP_PISTOL		RETURN WEAPONTYPE_PISTOL
				CASE WEAPONGROUP_SHOTGUN	RETURN WEAPONTYPE_PUMPSHOTGUN
				CASE WEAPONGROUP_SMG		RETURN WEAPONTYPE_MICROSMG
				CASE WEAPONGROUP_MG			RETURN WEAPONTYPE_MG
				CASE WEAPONGROUP_RIFLE		RETURN WEAPONTYPE_ASSAULTRIFLE
			ENDSWITCH
		BREAK
	
		//KOREAN
		CASE G_M_Y_Korean_02
			SWITCH GET_WEAPONTYPE_GROUP(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].gun)
				CASE WEAPONGROUP_PISTOL		RETURN WEAPONTYPE_COMBATPISTOL
				CASE WEAPONGROUP_SHOTGUN	RETURN WEAPONTYPE_PUMPSHOTGUN
				CASE WEAPONGROUP_SMG		RETURN WEAPONTYPE_SMG
				CASE WEAPONGROUP_MG			RETURN WEAPONTYPE_COMBATMG
				CASE WEAPONGROUP_RIFLE		RETURN WEAPONTYPE_CARBINERIFLE
			ENDSWITCH
		BREAK
	
		//HILLBILLY
		CASE A_M_M_Hillbilly_02
			SWITCH GET_WEAPONTYPE_GROUP(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].gun)
				CASE WEAPONGROUP_PISTOL		RETURN WEAPONTYPE_COMBATPISTOL
				CASE WEAPONGROUP_SHOTGUN	RETURN WEAPONTYPE_SAWNOFFSHOTGUN
				CASE WEAPONGROUP_SMG		RETURN WEAPONTYPE_MICROSMG
				CASE WEAPONGROUP_MG			RETURN WEAPONTYPE_MG
				CASE WEAPONGROUP_RIFLE		RETURN WEAPONTYPE_COMBATMG
			ENDSWITCH
		BREAK
	
		//SALVA
		CASE G_M_Y_SalvaGoon_01
			SWITCH GET_WEAPONTYPE_GROUP(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].gun)
				CASE WEAPONGROUP_PISTOL		RETURN WEAPONTYPE_PISTOL
				CASE WEAPONGROUP_SHOTGUN	RETURN WEAPONTYPE_PUMPSHOTGUN
				CASE WEAPONGROUP_SMG		RETURN WEAPONTYPE_MICROSMG
				CASE WEAPONGROUP_MG			RETURN WEAPONTYPE_MG
				CASE WEAPONGROUP_RIFLE		RETURN WEAPONTYPE_ASSAULTRIFLE
			ENDSWITCH
		BREAK
	
		//PROFESSIONAL
		CASE MP_G_M_Pros_01
			SWITCH GET_WEAPONTYPE_GROUP(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].gun)
				CASE WEAPONGROUP_PISTOL		RETURN WEAPONTYPE_PISTOL
				CASE WEAPONGROUP_SHOTGUN	RETURN WEAPONTYPE_PUMPSHOTGUN
				CASE WEAPONGROUP_SMG		RETURN WEAPONTYPE_SMG
				CASE WEAPONGROUP_MG			RETURN WEAPONTYPE_ADVANCEDRIFLE
				CASE WEAPONGROUP_RIFLE		RETURN WEAPONTYPE_COMBATMG
			ENDSWITCH
		BREAK
		
		//ARMENIAN
		CASE G_M_M_ArmGoon_01
			SWITCH GET_WEAPONTYPE_GROUP(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].gun)
				CASE WEAPONGROUP_PISTOL		RETURN WEAPONTYPE_PISTOL
				CASE WEAPONGROUP_SHOTGUN	RETURN WEAPONTYPE_SAWNOFFSHOTGUN
				CASE WEAPONGROUP_SMG		RETURN WEAPONTYPE_SMG
				CASE WEAPONGROUP_MG			RETURN WEAPONTYPE_MG
				CASE WEAPONGROUP_RIFLE		RETURN WEAPONTYPE_CARBINERIFLE
			ENDSWITCH
		BREAK
		
		//TRIAD
		CASE G_M_M_ChiGoon_01
			SWITCH GET_WEAPONTYPE_GROUP(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].gun)
				CASE WEAPONGROUP_PISTOL		RETURN WEAPONTYPE_PISTOL
				CASE WEAPONGROUP_SHOTGUN	RETURN WEAPONTYPE_PUMPSHOTGUN
				CASE WEAPONGROUP_SMG		RETURN WEAPONTYPE_ADVANCEDRIFLE
				CASE WEAPONGROUP_MG			RETURN WEAPONTYPE_MG
				CASE WEAPONGROUP_RIFLE		RETURN WEAPONTYPE_CARBINERIFLE
			ENDSWITCH
		BREAK
		
		//MERRYWEATHER
		CASE S_M_Y_BLACKOPS_01
			SWITCH GET_WEAPONTYPE_GROUP(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].gun)
				CASE WEAPONGROUP_PISTOL		RETURN WEAPONTYPE_COMBATPISTOL
				CASE WEAPONGROUP_SHOTGUN	RETURN WEAPONTYPE_PUMPSHOTGUN
				CASE WEAPONGROUP_SMG		RETURN WEAPONTYPE_SMG
				CASE WEAPONGROUP_MG			RETURN WEAPONTYPE_COMBATMG
				CASE WEAPONGROUP_RIFLE		RETURN WEAPONTYPE_CARBINERIFLE
			ENDSWITCH
		BREAK
		
		//CULT
		CASE A_M_O_ACULT_02
			SWITCH GET_WEAPONTYPE_GROUP(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].gun)
				CASE WEAPONGROUP_PISTOL		RETURN WEAPONTYPE_PISTOL
				CASE WEAPONGROUP_SHOTGUN	RETURN WEAPONTYPE_SAWNOFFSHOTGUN
				CASE WEAPONGROUP_SMG		RETURN WEAPONTYPE_MICROSMG
				CASE WEAPONGROUP_MG			RETURN WEAPONTYPE_MG
				CASE WEAPONGROUP_RIFLE		RETURN WEAPONTYPE_ASSAULTRIFLE
			ENDSWITCH
		BREAK
		
		DEFAULT
			SWITCH GET_WEAPONTYPE_GROUP(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].gun)
				CASE WEAPONGROUP_PISTOL		RETURN WEAPONTYPE_PISTOL
				CASE WEAPONGROUP_SHOTGUN	RETURN WEAPONTYPE_PUMPSHOTGUN
				CASE WEAPONGROUP_SMG		RETURN WEAPONTYPE_SMG
				CASE WEAPONGROUP_MG			RETURN WEAPONTYPE_COMBATMG
				CASE WEAPONGROUP_RIFLE		RETURN WEAPONTYPE_CARBINERIFLE
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].gun
ENDFUNC

////PURPOSE: Retursn the voice name for the gang
//FUNC STRING GET_VOICE_NAME()
//	
//	SWITCH serverBD.PedGangModel
//		CASE G_M_Y_MEXGOON_01	NET_PRINT("    ----->     HIDEOUT - GET_VOICE_NAME = FM_VAGOS") NET_NL() 			RETURN "FM_VAGOS"
//		CASE G_M_Y_Lost_01		NET_PRINT("    ----->     HIDEOUT - GET_VOICE_NAME = FM_LOST") NET_NL() 			RETURN "FM_LOST"
//		CASE G_M_Y_BallaOrig_01	NET_PRINT("    ----->     HIDEOUT - GET_VOICE_NAME = FM_BALLA") NET_NL() 			RETURN "FM_BALLA"
//		CASE G_M_Y_FAMCA_01		NET_PRINT("    ----->     HIDEOUT - GET_VOICE_NAME = FM_FAMILY") NET_NL() 			RETURN "FM_FAMILY"
//		CASE G_M_Y_Korean_02	NET_PRINT("    ----->     HIDEOUT - GET_VOICE_NAME = FM_KOREAN") NET_NL() 			RETURN "FM_KOREAN"
//		CASE A_M_M_Hillbilly_02	NET_PRINT("    ----->     HIDEOUT - GET_VOICE_NAME = FM_HILLBILLY") NET_NL() 		RETURN "FM_HILLBILLY"
//		CASE G_M_Y_SalvaGoon_01	NET_PRINT("    ----->     HIDEOUT - GET_VOICE_NAME = FM_SALVA") NET_NL() 			RETURN "FM_SALVA"
//		CASE MP_G_M_Pros_01		NET_PRINT("    ----->     HIDEOUT - GET_VOICE_NAME = FM_PROFESSIONAL") NET_NL() 	RETURN "FM_PROFESSIONAL"
//		CASE G_M_M_ArmGoon_01	NET_PRINT("    ----->     HIDEOUT - GET_VOICE_NAME = FM_ARMENIAN") NET_NL() 		RETURN "FM_ARMENIAN"
//		CASE G_M_M_ChiGoon_01	NET_PRINT("    ----->     HIDEOUT - GET_VOICE_NAME = FM_TRIAD") NET_NL() 			RETURN "FM_TRIAD"
//	ENDSWITCH
//	
//	NET_PRINT("    ----->     HIDEOUT - NO VOICE FOUND - GET_VOICE_NAME = FM_PROFESSIONAL") NET_NL()
//	RETURN "FM_PROFESSIONAL"
//ENDFUNC

//PURPOSE: Checks whether the ped should be aggressive or defensive
FUNC BOOL SHOULD_AI_PED_BE_AGGRESSIVE(INT iPedNumber)
	
	IF GET_WEAPONTYPE_GROUP(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].gun) = WEAPONGROUP_SHOTGUN
		RETURN TRUE
	ENDIF
		
	SWITCH serverBD.eHideoutVariation
		CASE HIDEOUT_KILL_PEDS
			IF iPedNumber = 0
			OR iPedNumber = 1
			OR iPedNumber = 5
				RETURN TRUE
			ELIF GET_RANDOM_BOOL()
				RETURN FALSE
			ENDIF
		BREAK
		
		/*CASE HIDEOUT_TERRITORY_CONTROL
			IF iPedNumber = 0
			OR iPedNumber = 3
			OR iPedNumber = 5
				RETURN TRUE
			ELIF GET_RANDOM_BOOL()
				RETURN FALSE
			ENDIF
		BREAK

		CASE HIDEOUT_PACKAGE_STEAL
			IF iPedNumber = 0
			OR iPedNumber = 2
			OR iPedNumber = 4
				RETURN TRUE
			ELIF GET_RANDOM_BOOL()
				RETURN FALSE
			ENDIF
		BREAK*/
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

//PURPOSE: Returns TRUE if the Weapon passed shouldn't be used for this variation (i.e. Sniper Rifles in Farmhouse)
FUNC BOOL IS_WEAPON_RESTRICTED_FOR_THIS_VARIATION(WEAPON_TYPE thisWeapon)
	//Farmhouse - No Sniper Rifles	- Bug 1068135
	IF thisWeapon = WEAPONTYPE_SNIPERRIFLE
		IF ARE_VECTORS_ALMOST_EQUAL(<<2446.64575, 4976.47754, 56.53719>>, serverBD.vHideoutCentre, 100.0)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SET_UP_GANG_PEDS(PED_INDEX piPassed, INT iPedNumber, BOOL bInitialSetup = FALSE)
	
	//INT iteamrel[FMMC_MAX_TEAMS]
	//INT irepeat
		
	//INT iAccuracy
	INT iHealth
	INT iArmour
	COMBAT_ABILITY_LEVEL eCombatAbility
	
	INT iDifficultyLevel = serverBD.iDifficultyFromPlayerStats
	
	#IF IS_DEBUG_BUILD
		TEXT_LABEL_23 tl23
		tl23 = "niPed "
		tl23 += iPedNumber 
		SET_PED_NAME_DEBUG(piPassed, tl23)
	#ENDIF
	
	SET_PED_RANDOM_COMPONENT_VARIATION(piPassed)
	//Set Professionals not to use masks
	IF serverBD.PedGangModel = MP_G_M_Pros_01
		SET_PED_COMPONENT_VARIATION(piPassed, PED_COMP_SPECIAL2, 0, 0)
	ENDIF
	
	//Control Giving Ped a Weapon if none is set (Get Random Weapon then make sure it is swapped to the gang variation, then make sure it is unlocked at the average rank)
	//IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].gun = WEAPONTYPE_UNARMED
		g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].gun = GET_RANDOM_GANG_PED_WEAPON()
		g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].gun = GET_GANG_PED_WEAPON(iPedNumber)
		IF GET_FM_WEAPON_UNLOCK_RANK(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].gun) > serverBD.iAverageRank
		//OR (bInitialSetup = TRUE AND iPedNumber = 0)	//Make sure Warning ped has pistol
		OR IS_WEAPON_RESTRICTED_FOR_THIS_VARIATION(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].gun)
			IF GET_RANDOM_BOOL()
				g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].gun = WEAPONTYPE_PISTOL
			ELSE
				g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].gun = WEAPONTYPE_MICROSMG
			ENDIF
			g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].gun = GET_GANG_PED_WEAPON(iPedNumber)
			NET_PRINT_TIME() NET_PRINT("    ----->     HIDEOUT - SET_UP_GANG_PEDS - USED BACKUP LOWER RANK WEAPON ") NET_NL()
		ENDIF
		NET_PRINT_TIME() NET_PRINT("    ----->     HIDEOUT - SET_UP_GANG_PEDS - WEAPON GIVEN  = ") NET_PRINT(GET_WEAPON_NAME(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].gun, TRUE)) NET_NL()
	//ENDIF
	GIVE_DELAYED_WEAPON_TO_PED(piPassed, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].gun, 25000, TRUE)
	
	/*FOR irepeat = 0 TO (FMMC_MAX_TEAMS-1)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iTeam[irepeat] = ciPED_RELATION_SHIP_LIKE
			iteamrel[irepeat] = FM_RelationshipLike 
			 SET_PED_CAN_BE_TARGETTED_BY_TEAM(piPassed, irepeat, FALSE)
			 SET_ENTITY_IS_TARGET_PRIORITY(piPassed, FALSE)
		ELIF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iTeam[irepeat] = ciPED_RELATION_SHIP_DISLIKE
			iteamrel[irepeat] = FM_RelationshipDislike
			SET_PED_CAN_BE_TARGETTED_BY_TEAM(piPassed, irepeat, TRUE)
			SET_ENTITY_IS_TARGET_PRIORITY(piPassed, FALSE)
		ELIF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].iTeam[irepeat] = ciPED_RELATION_SHIP_HATE
			iteamrel[irepeat] = FM_RelationshipHate
			SET_PED_CAN_BE_TARGETTED_BY_TEAM(piPassed, irepeat, TRUE)
			SET_ENTITY_IS_TARGET_PRIORITY(piPassed, TRUE)
		ENDIF
	ENDFOR*/
	
	IF bInitialSetup = FALSE
		SET_PED_RELATIONSHIP_GROUP_HASH(piPassed, rgFM_AiHate)	
		PRINTLN("    ----->     HIDEOUT - SET PED RELGROUP rgFM_AiHate - A - PED ", iPedNumber)
		SET_PED_AS_ENEMY(piPassed, TRUE)	
	ELSE
		SET_PED_RELATIONSHIP_GROUP_HASH(piPassed, rgFM_AiDislike)
		PRINTLN("    ----->     HIDEOUT - SET PED RELGROUP rgFM_AiDislike - A - PED ", iPedNumber)
	ENDIF
	
	SET_PED_CONFIG_FLAG(piPassed, PCF_DontInfluenceWantedLevel, TRUE)
	SET_PED_CONFIG_FLAG(piPassed, PCF_ListensToSoundEvents, TRUE)
	
	//Set Behaviours
	IF iDifficultyLevel < 25
		eCombatAbility = CAL_POOR
	ELIF iDifficultyLevel < 75
		eCombatAbility = CAL_AVERAGE
	ELSE
		eCombatAbility = CAL_PROFESSIONAL
	ENDIF
	SET_PED_ACCURACY(piPassed, serverBD.iAccuracy)
	NET_PRINT_TIME() NET_PRINT("    ----->     HIDEOUT - SET_UP_GANG_PEDS - ACCURACY = ") NET_PRINT_INT(serverBD.iAccuracy) NET_NL()
	
	//HEALTH
	IF iDifficultyLevel < 20
		iHealth = 100
		iArmour = 0
	ELIF iDifficultyLevel < 30
		iHealth = 125
		iArmour = 0
	ELIF iDifficultyLevel < 40
		iHealth = 125
		iArmour = 25
	ELIF iDifficultyLevel < 50
		iHealth = 150
		iArmour = 25
	ELIF iDifficultyLevel < 60
		iHealth = 150
		iArmour = 50
	ELIF iDifficultyLevel < 70
		iHealth = 175
		iArmour = 50
	ELIF iDifficultyLevel < 80
		iHealth = 175
		iArmour = 75
	ELIF iDifficultyLevel < 90
		iHealth = 200
		iArmour = 75
	ELSE
		iHealth = 200
		iArmour = 100
	ENDIF
	SET_ENTITY_MAX_HEALTH(piPassed, ROUND(iHealth*g_sMPTunables.fAiHealthModifier))
	SET_ENTITY_HEALTH(piPassed, ROUND(iHealth*g_sMPTunables.fAiHealthModifier))
	SET_PED_ARMOUR(piPassed, iArmour)
	PRINTLN("    ----->     HIDEOUT - SET_UP_GANG_PEDS - iHealth = ", ROUND(iHealth*g_sMPTunables.fAiHealthModifier), " Armour = ", iArmour)
	
	SET_PED_COMBAT_ABILITY(piPassed, eCombatAbility)
	SET_PED_HIGHLY_PERCEPTIVE(piPassed, TRUE)
	
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(piPassed, FALSE)
	SET_PED_DIES_IN_WATER(piPassed, TRUE)
	
	//SET_PED_SPHERE_DEFENSIVE_AREA(piPassed, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].vPos, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].fRange)
	//TASK_GUARD_SPHERE_DEFENSIVE_AREA(piPassed,g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].vPos,g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].fHead,g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].fRange,-1,g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].vPos,g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].fRange)
	
	SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_ALWAYS_FIGHT, TRUE)
	SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_CAN_USE_FRUSTRATED_ADVANCE, TRUE)

	SET_PED_ANGLED_DEFENSIVE_AREA(piPassed, serverBD.AiSpawnArea.vMin, serverBD.AiSpawnArea.vMax, serverBD.AiSpawnArea.fWidth)
	
	//Defensive or Agressive
	IF NOT SHOULD_AI_PED_BE_AGGRESSIVE(iPedNumber)
	#IF IS_DEBUG_BUILD OR bMakePedsDefensive #ENDIF
		SET_PED_COMBAT_MOVEMENT(piPassed, CM_DEFENSIVE)
		SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_SWITCH_TO_ADVANCE_IF_CANT_FIND_COVER, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_SWITCH_TO_DEFENSIVE_IF_IN_COVER, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_CLEAR_AREA_SET_DEFENSIVE_IF_DEFENSIVE_CANNOT_BE_REACHED, TRUE)
		NET_PRINT_TIME() NET_PRINT("    ----->     HIDEOUT - SET_UP_GANG_PEDS - DEFENSIVE ") NET_PRINT_INT(iPedNumber) NET_NL()
	ELSE
		SET_PED_COMBAT_MOVEMENT(piPassed, CM_WILLADVANCE)
		SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_CAN_CHARGE, TRUE)
		NET_PRINT_TIME() NET_PRINT("    ----->     HIDEOUT - SET_UP_GANG_PEDS - WILL DAVANCE ") NET_PRINT_INT(iPedNumber) NET_NL()
	ENDIF
	
	SET_PED_TARGET_LOSS_RESPONSE(piPassed, TLR_NEVER_LOSE_TARGET)
	SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_DISABLE_SEEK_DUE_TO_LINE_OF_SIGHT, FALSE)

	SET_PED_TO_INFORM_RESPECTED_FRIENDS(piPassed, 150, 10)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].gun = WEAPONTYPE_MINIGUN
		SET_PED_FIRING_PATTERN(piPassed, FIRING_PATTERN_FULL_AUTO)
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].gun = WEAPONTYPE_SNIPERRIFLE
	OR  g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPedNumber].gun = WEAPONTYPE_RPG
		SET_PED_COMBAT_RANGE(piPassed, CR_FAR)
	ENDIF
	
	SET_PED_COMBAT_ATTRIBUTES(piPassed, CA_DISABLE_AIM_AT_AI_TARGETS_IN_HELIS, TRUE)
	
	//SET_PED_AS_ENEMY(piPassed, TRUE)
	
	SET_PED_AMMO_TO_DROP(piPassed, GET_RANDOM_INT_IN_RANGE(15, 31))
							
	SET_PED_KEEP_TASK(piPassed, TRUE)
	
	//Setup first ped for dialogue
	//IF iPedNumber = 0
	//	ADD_PED_FOR_DIALOGUE(sSpeech, 3, piPassed, GET_VOICE_NAME(), FALSE)
	//ENDIF
	
	serverBD.iNumAlivePeds++
ENDPROC


// ************************************ \\
//		CREATE INITIAL AI PEDS	   		\\
// ************************************ \\

//FUNC BOOL CREATE_GANG_PEDS(NETWORK_INDEX &niPed[]) //, INT iStage = 0 )
//	INT i
//	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfPeds i
//		IF serverBD.PedGangModel != DUMMY_MODEL_FOR_SCRIPT
//			IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niPed[i])
//				REQUEST_MODEL(serverBD.PedGangModel)
//	            
//	            IF HAS_MODEL_LOADED(serverBD.PedGangModel)
//	                IF CAN_REGISTER_MISSION_PEDS(1)
//						IF CREATE_NET_PED(niPed[i], PEDTYPE_MISSION, serverBD.PedGangModel, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].vPos, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].fHead)
//							SET_UP_GANG_PEDS(NET_TO_PED(niPed[i]), i, TRUE)
//							NET_PRINT_TIME() NET_PRINT("     ---------->     GANG PED CREATED     <----------     ") NET_NL()
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF	
//	ENDREPEAT
//	
//	//Check we have created all Gang Peds
//	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfPeds i
//		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niPed[i])
//		AND serverBD.PedGangModel != DUMMY_MODEL_FOR_SCRIPT
//			RETURN FALSE
//		ENDIF
//	ENDREPEAT
//	
//	RETURN TRUE
//ENDFUNC


BOOL bGetSafeCoord[MAX_NUM_PEDS]
BOOL bCoordFound[MAX_NUM_PEDS]
FUNC BOOL CREATE_GANG_PEDS(NETWORK_INDEX &niPed[]) //, INT iStage = 0 )
	INT i
	
	VECTOR vPos
	FLOAT fHeading
	SPAWN_SEARCH_PARAMS SpawnSearchParams
	
	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfPeds i
		//IF serverBD.PedGangModel != DUMMY_MODEL_FOR_SCRIPT
			IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niPed[i])
				REQUEST_MODEL(serverBD.PedGangModel)
	            
	            IF HAS_MODEL_LOADED(serverBD.PedGangModel)
	                IF CAN_REGISTER_MISSION_PEDS(1)
						//Check Original Coord
						IF bGetSafeCoord[i] = FALSE
							vPos = g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].vPos
							fHeading = g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].fHead
							//IF WOULD_ENTITY_BE_OCCLUDED(serverBD.PedGangModel, vPos)
							IF NOT IS_ANY_PLAYER_NEAR_POINT(vPos, 10.0)	//IS_POINT_OK_FOR_NET_ENTITY_CREATION(vPos, 3, 1, 1, 3)
							AND DOES_TRACKED_POINT_EXIST(serverBD.iPedTrackedPoints[i]) 
							AND NOT IS_TRACKED_POINT_VISIBLE(serverBD.iPedTrackedPoints[i]) 
							AND IS_POINT_OK_FOR_NET_ENTITY_CREATION(vPos, 3.0, 1.0, 1.0, 2.0, FALSE, TRUE, FALSE, 30, TRUE, -1, FALSE, 0, TRUE, 0, TRUE)
								bCoordFound[i] = TRUE
								NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - CREATE_GANG_PEDS - ORIGINAL POS OKAY - PED ") NET_PRINT_INT(i) NET_NL()
							ELSE
								bGetSafeCoord[i] = TRUE
							ENDIF
						ENDIF
						
						//Get Safe Coord
						IF bGetSafeCoord[i] = TRUE
						
							SpawnSearchParams.vFacingCoords = serverBD.vHideoutCentre
							SpawnSearchParams.bConsiderInteriors = serverBD.PedData[i].bRespawnInInterior
							SpawnSearchParams.fMinDistFromPlayer = 5
						
							IF GET_SAFE_COORDS_IN_ANGLED_AREA_FOR_CREATING_ENTITY(serverBD.AiSpawnArea.vMin, serverBD.AiSpawnArea.vMax, serverBD.AiSpawnArea.fWidth, vPos, fHeading, SpawnSearchParams)
								bCoordFound[i] = TRUE
								NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - CREATE_GANG_PEDS - SAFE COORD FOUND - PED ") NET_PRINT_INT(i) NET_NL()
							ENDIF
						ENDIF
						
						//Create ped if coord is okay
						IF bCoordFound[i] = TRUE
							IF CREATE_NET_PED(niPed[i], PEDTYPE_MISSION, serverBD.PedGangModel, vPos, fHeading)
								//RETAIN_ENTITY_IN_INTERIOR(NET_TO_PED(niPed[i]), GET_INTERIOR_AT_COORDS(vPos))
								SET_UP_GANG_PEDS(NET_TO_PED(niPed[i]), i, TRUE)
								SET_ENTITY_LOAD_COLLISION_FLAG(NET_TO_PED(niPed[i]), TRUE) //** TWH - RLB - (fix for 1423684, load navmesh around peds so they don't stand still and will always make their way into the gang attack area).
								NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - CREATE_GANG_PEDS - GANG PED CREATED - PED ") NET_PRINT_INT(i) NET_PRINT(" AT ") NET_PRINT_VECTOR(vPos) NET_NL()
							ENDIF
						ENDIF
					
					#IF IS_DEBUG_BUILD
					ELSE
						NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - CREATE_GANG_PEDS - CAN_REGISTER_MISSION_PEDS = FALSE ") NET_PRINT_INT(i) NET_NL()
					#ENDIF
					
					ENDIF
				ENDIF
			ENDIF
		//ENDIF
		
		bGetSafeCoord[i] = FALSE
		bCoordFound[i] = FALSE
	ENDREPEAT
	
	//Check we have created all Gang Peds
	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfPeds i
		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niPed[i])
		//AND serverBD.PedGangModel != DUMMY_MODEL_FOR_SCRIPT
			RETURN FALSE
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

FUNC BOOL LOAD_AND_CREATE_ALL_ENTITIES(FMMC_SERVER_DATA_STRUCT &sSDSpassed)

	IF CREATE_GANG_PEDS(sSDSpassed.niPed)
	AND CREATE_VEHICLES()
	AND CREATE_DESTRUCTIBLE_CRATES()
	//AND CREATE_SAFE()
	//AND CREATE_PARKED_VEHICLE()
		NET_PRINT_TIME() NET_PRINT_STRINGS("  ---->  HIDEOUT - LOAD_AND_CREATE_ALL_ENTITIES RETURNING TRUE    <----------     ", "LOAD_AND_CREATE_ALL_ENTITIES") NET_NL()
		RETURN TRUE
	ELSE
		NET_PRINT_TIME() NET_PRINT_STRINGS("  ---->  HIDEOUT - Waiting to create PEDS <-----", "LOAD_AND_CREATE_ALL_ENTITIES") NET_NL()
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

// *********************************** \\
//			AI PEDS	RESPAWN	 		   \\
// *********************************** \\
//PURPOSE: Checks if the gang peds should be respawned
FUNC BOOL SHOULD_RESPAWN_PEDS()
	SWITCH serverBD.eHideoutVariation
		CASE HIDEOUT_KILL_PEDS
			
			/*INT iSpawnsLeft - Removed as we are going to keep using these peds for backup
			iSpawnsLeft = (serverBD.iKillGoal - serverBD.iPedsSpawned)
			IF iSpawnsLeft < MAX_NUM_PEDS
				IF serverBD.iReservedPeds != iSpawnsLeft
					serverBD.iReservedPeds = iSpawnsLeft
					NET_PRINT_TIME() NET_PRINT("    ----->     HIDEOUT - SERVER - CHANGE RESERVED PEDS - serverBD.iReservedPeds = ") NET_PRINT_INT(serverBD.iReservedPeds) NET_NL()
				ENDIF
			ENDIF*/
			
			IF serverBD.iPedsSpawned >= serverBD.iKillGoal
				RETURN FALSE
			ENDIF
		BREAK
		/*CASE HIDEOUT_TERRITORY_CONTROL
			IF GET_SERVER_HIDEOUT_STAGE() != HIDEOUT_STAGE_PRIMARY
				RETURN FALSE
			ENDIF
		BREAK
		CASE HIDEOUT_PACKAGE_STEAL
			IF GET_SERVER_HIDEOUT_STAGE() = HIDEOUT_STAGE_REWARDS_AND_FEEDACK
				RETURN FALSE
			ENDIF
		BREAK*/
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

//PURPOSE: Controls the respawning of peds using GET_SAFE_COORDS_IN_ANGLED_AREA_FOR_CREATING_ENTITY
FUNC BOOL RESPAWN_GANG_PED(INT iped)
    VECTOR vspawnloc
    FLOAT fspawnhead
    
	IF SHOULD_RESPAWN_PEDS()
	    IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.sFMMC_SBD.niPed[iped])
	        //IF serverBD.PedGangModel != DUMMY_MODEL_FOR_SCRIPT
	            
	            REQUEST_MODEL(serverBD.PedGangModel)
	            
	            IF HAS_MODEL_LOADED(serverBD.PedGangModel)
	                IF CAN_REGISTER_MISSION_PEDS(1)
	                    
	                    IF (isearchingforPed = -1) // AND isearchingforObj = -1 AND isearchingforVeh = -1)
	                    OR (isearchingforPed = iped) // AND isearchingforObj = -1 AND isearchingforVeh = -1)
	                        
	                        isearchingforped = iped
	                        
							SPAWN_SEARCH_PARAMS SpawnSearchParams
							SpawnSearchParams.vFacingCoords = serverBD.vHideoutCentre
							SpawnSearchParams.bConsiderInteriors = serverBD.PedData[iped].bRespawnInInterior
							SpawnSearchParams.fMinDistFromPlayer = 20
							
	                        //IF GET_SAFE_COORDS_IN_AREA_FOR_CREATING_ENTITY(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].vPos, 75, vspawnloc, fspawnhead, serverBD.vHideoutCentre, serverBD.PedData[iped].bRespawnInInterior, -1, FALSE, 20)
	                        IF GET_SAFE_COORDS_IN_ANGLED_AREA_FOR_CREATING_ENTITY(serverBD.AiSpawnArea.vMin, serverBD.AiSpawnArea.vMax, serverBD.AiSpawnArea.fWidth, vspawnloc, fspawnhead, SpawnSearchParams)

	                        	NET_PRINT("    ----->     HIDEOUT - calling create respawn ped: ") NET_PRINT_INT(iped)  NET_PRINT("at coords: ") NET_PRINT_VECTOR(vspawnloc)  NET_NL()
	                            
	                            IF CREATE_NET_PED(serverBD.sFMMC_SBD.niPed[iped], PEDTYPE_MISSION, serverBD.PedGangModel, vspawnloc, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].fHead)
	                               	//RETAIN_ENTITY_IN_INTERIOR(NET_TO_PED(serverBD.sFMMC_SBD.niPed[iped]), GET_INTERIOR_AT_COORDS(vspawnloc))
									SET_UP_GANG_PEDS(NET_TO_PED(serverBD.sFMMC_SBD.niPed[iped]), iped)
	                               	TASK_COMBAT_HATED_TARGETS_IN_AREA(NET_TO_PED(serverBD.sFMMC_SBD.niPed[iped]), serverBD.vHideoutCentre, serverBD.fHideoutRadius*1.5)
									SET_ENTITY_LOAD_COLLISION_FLAG(NET_TO_PED(serverBD.sFMMC_SBD.niPed[iped]), TRUE) //** TWH - RLB - (fix for 1423684, load navmesh around peds so they don't stand still and will always make their way into the gang attack area).
									//SET_MODEL_AS_NO_LONGER_NEEDED(serverBD.PedGangModel)
	                                isearchingforped = -1
									serverBD.iPedsSpawned++
	                                RETURN TRUE
	                            ENDIF
	                        ELSE
	                            NET_PRINT("    ----->     HIDEOUT - unable to find spawn coords for ped: ") NET_PRINT_INT(iped)  NET_PRINT("at coords: ") NET_PRINT_VECTOR(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].vPos)  NET_NL()
	                        ENDIF
	                    ENDIF
	                
					#IF IS_DEBUG_BUILD 				
	                ELSE
	                    NET_PRINT("    ----->     HIDEOUT - Cannot register script ped: ") NET_PRINT_INT(iped) NET_NL()
					#ENDIF
					
	                ENDIF
	            ENDIF
	                    
	        //ENDIF
	    ELSE
	        isearchingforped = -1
	        RETURN TRUE
	    ENDIF
    ENDIF
	  
    RETURN FALSE
ENDFUNC

PROC WIPE_ENEMY_ID(INT iPed, BOOL bCleanNetId)
    
    IF bCleanNetId
        CLEANUP_NET_ID(serverBd.sFMMC_SBD.niPed[iPed])
    ENDIF
        
    serverBD.sFMMC_SBD.niPed[iPed] = NULL
    NET_PRINT("    ----->     HIDEOUT - WIPE_ENEMY_ID ") NET_PRINT_INT(iped) NET_NL()
ENDPROC

FUNC INT GET_ENEMY_RESPAWN_TIME()
    //INT iReturnValue = g_FMMC_STRUCT_ENTITIES.iPedRespawnTime
        
    //IF g_FMMC_STRUCT_ENTITIES.iPedRespawnTime <= FMMC_PED_RESPAWN_TIME_2_SECONDS	//FMMC_PED_RESPAWN_TIME_RANDOM
        INT iReturnValue = GET_RANDOM_INT_IN_RANGE(FMMC_PED_RESPAWN_TIME_2_SECONDS, FMMC_PED_RESPAWN_TIME_8_SECONDS)	//FMMC_PED_RESPAWN_TIME_4_SECONDS	FMMC_PED_RESPAWN_TIME_12_SECONDS
    //ENDIF
    
    RETURN (iReturnValue*1000)    
ENDFUNC

//PURPOSE: Makes the passed in ped turn to face the player
PROC MAKE_PED_TURN(PED_INDEX pedId, INT iPed)
	IF GET_SCRIPT_TASK_STATUS(pedId, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY) != PERFORMING_TASK
	AND GET_SCRIPT_TASK_STATUS(pedId, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY) != WAITING_TO_START_TASK
		IF HAS_NET_TIMER_EXPIRED(iTurnTimer[iPed], 5000)
			RESET_NET_TIMER(iTurnTimer[iPed])
			TASK_LOOK_AT_ENTITY(pedId, PLAYER_PED_ID(), 10000)
			TASK_TURN_PED_TO_FACE_ENTITY(pedId, PLAYER_PED_ID(), 10000)
			NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - MAKE PED TURN TO FACE PLAYER - PED ") NET_PRINT_INT(iPed)  NET_NL()
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Updates the peds accuracy to the new level
PROC UPDATE_PED_ACCURACY(INT iPed)
	IF GET_PED_ACCURACY(NET_TO_PED(serverBd.sFMMC_SBD.niPed[iPed])) < (serverBD.iAccuracy-2)
	OR GET_PED_ACCURACY(NET_TO_PED(serverBd.sFMMC_SBD.niPed[iPed])) > (serverBD.iAccuracy+2)
	//NET_PRINT_TIME() NET_PRINT("    ----->     HIDEOUT - UPDATE_PED_ACCURACY - A - GET_PED_ACCURACY = ") NET_PRINT_INT(GET_PED_ACCURACY(NET_TO_PED(serverBd.sFMMC_SBD.niPed[iPed]))) NET_NL()
	//NET_PRINT_TIME() NET_PRINT("    ----->     HIDEOUT - UPDATE_PED_ACCURACY - A - ACCURACY = ") NET_PRINT_INT(serverBD.iAccuracy) NET_NL()
	//IF GET_PED_ACCURACY(NET_TO_PED(serverBd.sFMMC_SBD.niPed[iPed])) != serverBD.iAccuracy
		IF TAKE_CONTROL_OF_NET_ID(serverBd.sFMMC_SBD.niPed[iPed])
			SET_PED_ACCURACY(NET_TO_PED(serverBd.sFMMC_SBD.niPed[iPed]), serverBD.iAccuracy)
			NET_PRINT_TIME() NET_PRINT("    ----->     HIDEOUT - UPDATE_PED_ACCURACY - GET_PED_ACCURACY = ") NET_PRINT_INT(GET_PED_ACCURACY(NET_TO_PED(serverBd.sFMMC_SBD.niPed[iPed]))) NET_NL()
			NET_PRINT_TIME() NET_PRINT("    ----->     HIDEOUT - UPDATE_PED_ACCURACY - ACCURACY = ") NET_PRINT_INT(serverBD.iAccuracy) NET_NL()
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_GANG_PEDS_BRAINS()
    INT i
    NETWORK_INDEX pedNetId
	
    SET_BIT(serverBD.iServerBitSet, biS_AllGangPedsDead)
	
    REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfPeds i
        
        SWITCH serverBD.PedData[i].ePedState
            // Process spawning stage every frame because RESPAWN_GANG_PED() needs to be called every frame.
            CASE ePED_SPAWNING
                IF RESPAWN_GANG_PED(i)
                    serverBD.PedData[i].ePedState = ePED_FIGHTING
    				NET_PRINT("    ----->     HIDEOUT - PROCESS_GANG_PEDS_BRAINS - B - PED STATE = ePED_FIGHTING ") NET_PRINT_INT(i) NET_NL()
                ENDIF            
            BREAK
            
            // Process spawning stage every frame because HAS_NET_TIMER_EXPIRED() needs to be called every frame.
            CASE ePED_DEAD
                pedNetId = serverBd.sFMMC_SBD.niPed[i]
                            
                // Cleanup id.
                IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(pedNetId)
                    IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(pedNetId)
                        WIPE_ENEMY_ID(i, TRUE)
                    ELSE
                        NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(pedNetId)
                    ENDIF
                ELSE
                    WIPE_ENEMY_ID(i, FALSE)
                ENDIF
                            
                // Get respawn time. Once respawn time has been reached and the id has been cleaned up go spawning state.
                IF NOT HAS_NET_TIMER_STARTED(PedRespawnTimer[i])
                    //IF serverBD.PedData[i].PedRespawnTimeLimit = (-1)
                        serverBD.PedData[i].PedRespawnTimeLimit = GET_ENEMY_RESPAWN_TIME()
    					NET_PRINT("    ----->     HIDEOUT - PROCESS_GANG_PEDS_BRAINS - PedRespawnTimeLimit = ") NET_PRINT_INT(serverBD.PedData[i].PedRespawnTimeLimit) NET_NL()
                    //ENDIF
                    serverBD.iNumAlivePeds--
					START_NET_TIMER(PedRespawnTimer[i])
    				NET_PRINT("    ----->     HIDEOUT - PROCESS_GANG_PEDS_BRAINS - PedRespawnTimer Started ") NET_PRINT_INT(i) NET_NL()
                ELSE
                    IF HAS_NET_TIMER_EXPIRED(PedRespawnTimer[i], serverBD.PedData[i].PedRespawnTimeLimit)
                        IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(pedNetId)
                            //serverBD.PedData[i].PedRespawnTimeLimit = (-1)
                            RESET_NET_TIMER(PedRespawnTimer[i])
                            serverBD.PedData[i].ePedState = ePED_SPAWNING
    						NET_PRINT("    ----->     HIDEOUT - PROCESS_GANG_PEDS_BRAINS - PED STATE = ePED_SPAWNING ") NET_PRINT_INT(i) NET_NL()
                        ENDIF
                    ENDIF
                ENDIF     
            BREAK
            
			DEFAULT
				CLEAR_BIT(serverBD.iServerBitSet, biS_AllGangPedsDead)
			BREAK
			  
        ENDSWITCH
                
    ENDREPEAT
        
    // Process other states in a stagger, more optimised this way.
    pedNetId = serverBd.sFMMC_SBD.niPed[iStaggeredPedCountBrain]
        
    SWITCH serverBD.PedData[iStaggeredPedCountBrain].ePedState
        CASE ePED_IDLE
			IF GET_SERVER_HIDEOUT_STAGE() > HIDEOUT_STAGE_WARNING //IS_BIT_SET(serverBD.iServerBitSet, biS_MoveToFightStage)
                serverBD.PedData[iStaggeredPedCountBrain].ePedState = ePED_FIGHTING
				NET_PRINT("    ----->     HIDEOUT - PROCESS_GANG_PEDS_BRAINS - A - PED STATE = ePED_FIGHTING ") NET_PRINT_INT(iStaggeredPedCountBrain) NET_NL()
			ELIF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_MoveToFightStage)
				IF NOT IS_NET_PED_INJURED(serverBd.sFMMC_SBD.niPed[iStaggeredPedCountBrain])
					IF IS_PED_IN_COMBAT(NET_TO_PED(serverBd.sFMMC_SBD.niPed[iStaggeredPedCountBrain]))
						SET_BIT(serverBD.iServerBitSet, biS_MoveToFightStage)
						NET_PRINT("    ----->     HIDEOUT - PROCESS_GANG_PEDS_BRAINS - IN COMBAT - biS_MoveToFightStage SET ") NET_PRINT_INT(iStaggeredPedCountBrain) NET_NL()
					ENDIF
				ELSE
					SET_BIT(serverBD.iServerBitSet, biS_MoveToFightStage)
					NET_PRINT("    ----->     HIDEOUT - PROCESS_GANG_PEDS_BRAINS - PED KILLED - biS_MoveToFightStage SET ") NET_PRINT_INT(iStaggeredPedCountBrain) NET_NL()
				ENDIF
            ENDIF
			IF serverBD.iTotalKills > 0
				SET_BIT(serverBD.iServerBitSet, biS_MoveToFightStage)
				NET_PRINT("    ----->     HIDEOUT - PROCESS_GANG_PEDS_BRAINS - PED KILLED - biS_MoveToFightStage SET ") NET_PRINT_INT(iStaggeredPedCountBrain) NET_NL()
			ENDIF
        BREAK
		
		CASE ePED_FIGHTING
            //IF IS_NET_PED_INJURED(pedNetId)
            IF IS_ENTITY_DEAD(NET_TO_PED(pedNetId))
				serverBD.PedData[iStaggeredPedCountBrain].ePedState = ePED_DEAD
                serverBD.iTotalKills++
				/*IF serverBD.eHideoutVariation = HIDEOUT_TERRITORY_CONTROL
					serverBD.TerritoryControlTimer.Timer = GET_TIME_OFFSET(serverBD.TerritoryControlTimer.Timer ,(-1*2*1000))
    				NET_PRINT("    ----->     HIDEOUT - PROCESS_GANG_PEDS_BRAINS - HIDEOUT_TERRITORY_CONTROL - KILL LOWERS TIMER ") NET_NL()
				ENDIF*/
    			NET_PRINT("    ----->     HIDEOUT - PROCESS_GANG_PEDS_BRAINS - PED STATE = ePED_DEAD ") NET_PRINT_INT(iStaggeredPedCountBrain) NET_NL()
            ENDIF
       BREAK
    ENDSWITCH
        
    iStaggeredPedCountBrain++
    IF iStaggeredPedCountBrain >= g_FMMC_STRUCT_ENTITIES.iNumberOfPeds
        iStaggeredPedCountBrain = 0
    ENDIF
ENDPROC

PROC PROCESS_GANG_PEDS_BODY()
    PED_INDEX pedId
    //TEXT_LABEL_15 tlRootLabel
	
    SWITCH serverBD.PedData[iStaggeredPedCount].ePedState
        
		CASE ePED_IDLE
            IF NOT IS_NET_PED_INJURED(serverBd.sFMMC_SBD.niPed[iStaggeredPedCount])
               
			   	//Get Warning Ped
				IF serverBD.iWarningPed = -1
				AND playerBD[PARTICIPANT_ID_TO_INT()].iWarningPed = -1
					IF NOT IS_BIT_SET(iPedTrackingBitSet, iStaggeredPedCount)
						REQUEST_PED_VISIBILITY_TRACKING(NET_TO_PED(serverBd.sFMMC_SBD.niPed[iStaggeredPedCount]))
						SET_BIT(iPedTrackingBitSet, iStaggeredPedCount)
					ELSE
						IF IS_ENTITY_VISIBLE_TO_SCRIPT(NET_TO_PED(serverBd.sFMMC_SBD.niPed[iStaggeredPedCount]))
						AND IS_TRACKED_PED_VISIBLE(NET_TO_PED(serverBd.sFMMC_SBD.niPed[iStaggeredPedCount]))
							playerBD[PARTICIPANT_ID_TO_INT()].iWarningPed = iStaggeredPedCount
							PRINTLN("    ----->     HIDEOUT - CLIENT - SEEN - iWarningPed = ", iStaggeredPedCount)
						ELIF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), NET_TO_PED(serverBd.sFMMC_SBD.niPed[iStaggeredPedCount]), <<5, 5, 5>>)
							playerBD[PARTICIPANT_ID_TO_INT()].iWarningPed = iStaggeredPedCount
							PRINTLN("    ----->     HIDEOUT - CLIENT - NEAR - iWarningPed = ", iStaggeredPedCount)
						ENDIF
					ENDIF
				ENDIF
			   
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBd.sFMMC_SBD.niPed[iStaggeredPedCount])
				OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBd.sFMMC_SBD.niPed[iStaggeredPedCount]) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
                    pedId = NET_TO_PED(serverBd.sFMMC_SBD.niPed[iStaggeredPedCount])
					
					UPDATE_PED_ACCURACY(iStaggeredPedCount)
						
                    IF iStaggeredPedCount = serverBD.iWarningPed	//0
					OR (iStaggeredPedCount = playerBD[PARTICIPANT_ID_TO_INT()].iWarningPed AND serverBD.iWarningPed = -1)
						
						
						IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_WarningLine1Done)
						OR NOT IS_BIT_SET(serverBD.iServerBitSet, biS_WarningLine2Done)
						OR NOT IS_BIT_SET(serverBD.iServerBitSet, biS_AttackLineDone)
							
							IF NOT IS_PED_IN_COMBAT(pedId) 
							//AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								
								IF NOT HAS_PED_LEFT_AREA(PLAYER_PED_ID())	//IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedId, <<40, 40, 40>>)
								
									//tlRootLabel = "FM_GH_W"
									
									IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_FirstPedTaskGiven)
									AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_FirstPedTaskGiven)
										IF IS_NET_PLAYER_OK(PLAYER_ID())
											TASK_GO_TO_ENTITY(pedId, PLAYER_PED_ID(), -1, WARNING_PED_RANGE, PEDMOVEBLENDRATIO_RUN)
											NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - TASK_GO_TO_ENTITY 1 ") NET_NL()
											//TASK_FOLLOW_NAV_MESH_TO_COORD(pedId, GET_ENTITY_COORDS(PLAYER_PED_ID()), PEDMOVEBLENDRATIO_RUN, -1, 6.0, ENAV_GO_FAR_AS_POSSIBLE_IF_TARGET_NAVMESH_NOT_LOADED)
											
											SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_FirstPedTaskGiven)
											NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - WARNING PED TASK GIVEN")  NET_NL()
										ENDIF
									ENDIF
									
									//Warning 1
									IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_WarningLine1Done)
									AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_WarningLine1Done)
										IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedId, <<20, 20, 20>>)	
											RESET_NET_TIMER(iSpeechTimer)
											
											SET_GANG_PED_VOICE(pedId)
											PLAY_PED_AMBIENT_SPEECH_NATIVE(pedId, "SHOUT_THREATEN_PED", "SPEECH_PARAMS_FORCE_SHOUTED_CRITICAL")	//PROVOKE_TRESPASS
											
											SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_WarningLine1Done)
											NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - AMBIENT_SPEECH - WARNING LINE 1")  NET_NL()
										ENDIF
									//ENDIF
									
									//Warning 2
									ELIF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_WarningLine2Done)
									AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_WarningLine2Done)
										IF HAS_NET_TIMER_EXPIRED(iWarning2Timer, WARNING_2_DELAY)
											RESET_NET_TIMER(iSpeechTimer)
											IF IS_NET_PLAYER_OK(PLAYER_ID())
												//TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY(pedId, PLAYER_PED_ID(), PLAYER_PED_ID(), PEDMOVEBLENDRATIO_WALK, FALSE, 2.0, 5)
												TASK_GOTO_ENTITY_AIMING(pedId, PLAYER_PED_ID(), WARNING_PED_RANGE, 25.0)
												//TASK_AIM_GUN_AT_ENTITY(pedId, PLAYER_PED_ID(), 16000)
												NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - TASK_GOTO_ENTITY_AIMING 1") NET_NL()
											ENDIF
											
											SET_GANG_PED_VOICE(pedId)
											PLAY_PED_AMBIENT_SPEECH_NATIVE(pedId,"PROVOKE_TRESPASS","SPEECH_PARAMS_FORCE_SHOUTED_CRITICAL")
											
											SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_WarningLine2Done)
											NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - AMBIENT_SPEECH - WARNING LINE 2") NET_NL()
										
										ELSE
											IF IS_NET_PLAYER_OK(PLAYER_ID())
												IF NOT HAS_PED_LEFT_AREA(PLAYER_PED_ID())
													//OUT OF RANGE - GO TO PLAYER
													IF NOT IS_ENTITY_AT_ENTITY(pedId, PLAYER_PED_ID(), <<WARNING_PED_RANGE*2, WARNING_PED_RANGE*2, WARNING_PED_RANGE*2>>)
														IF IS_BIT_SET(serverBD.iServerBitSet, biS_WarningLine2Done)
														OR IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_WarningLine2Done)
															IF GET_SCRIPT_TASK_STATUS(pedId, SCRIPT_TASK_GOTO_ENTITY_AIMING) != PERFORMING_TASK
															AND GET_SCRIPT_TASK_STATUS(pedId, SCRIPT_TASK_GOTO_ENTITY_AIMING) != WAITING_TO_START_TASK	
																TASK_GOTO_ENTITY_AIMING(pedId, PLAYER_PED_ID(), WARNING_PED_RANGE, 25.0)
																NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - TASK_GOTO_ENTITY_AIMING 2")  NET_NL()
															ENDIF
														ELSE
															IF GET_SCRIPT_TASK_STATUS(pedId, SCRIPT_TASK_GO_TO_ENTITY) != PERFORMING_TASK
															AND GET_SCRIPT_TASK_STATUS(pedId, SCRIPT_TASK_GO_TO_ENTITY) != WAITING_TO_START_TASK
																TASK_GO_TO_ENTITY(pedId, PLAYER_PED_ID(), -1, WARNING_PED_RANGE, PEDMOVEBLENDRATIO_RUN)
																NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - TASK_GO_TO_ENTITY 2") NET_NL()
															ENDIF
														ENDIF
													
													//IN RANGE - JUST FACE PLAYER
													ELSE
														IF IS_BIT_SET(serverBD.iServerBitSet, biS_WarningLine2Done)
														OR IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_WarningLine2Done)
															IF GET_SCRIPT_TASK_STATUS(pedId, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY) != PERFORMING_TASK
															AND GET_SCRIPT_TASK_STATUS(pedId, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY) != WAITING_TO_START_TASK
																TASK_TURN_PED_TO_FACE_ENTITY(pedId, PLAYER_PED_ID(), -1)
																NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - TASK_TURN_PED_TO_FACE_ENTITY") NET_NL()
															ENDIF
														ELSE
															IF GET_SCRIPT_TASK_STATUS(pedId, SCRIPT_TASK_AIM_GUN_AT_ENTITY) != PERFORMING_TASK
															AND GET_SCRIPT_TASK_STATUS(pedId, SCRIPT_TASK_AIM_GUN_AT_ENTITY) != WAITING_TO_START_TASK
																TASK_AIM_GUN_AT_ENTITY(pedId, PLAYER_PED_ID(), -1)
																NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - TASK_AIM_GUN_AT_ENTITY") NET_NL()
															ENDIF
														ENDIF
													ENDIF
												ELSE
													MAKE_PED_TURN(pedId, iStaggeredPedCount)
												ENDIF
											ENDIF
										ENDIF
									//ENDIF
									
									//Attack Task and Dialogue
									ELIF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_AttackLineDone)
									AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_AttackLineDone)
										IF HAS_NET_TIMER_EXPIRED(iWarningAttackTimer, WARNING_ATTACK_DELAY)
											SET_PED_RELATIONSHIP_GROUP_HASH(pedId, rgFM_AiHate)	
											PRINTLN("    ----->     HIDEOUT - SET PED RELGROUP rgFM_AiHate - B - PED ", iStaggeredPedCount)
											SET_PED_AS_ENEMY(pedId, TRUE)	
											TASK_COMBAT_HATED_TARGETS_IN_AREA(pedId, serverBD.vHideoutCentre, serverBD.fHideoutRadius*1.5)
											
											SET_GANG_PED_VOICE(pedId)
											IF serverBD.iNumParticpants > 1
												PLAY_PED_AMBIENT_SPEECH_NATIVE(pedId,"SHOUT_THREATEN_GANG","SPEECH_PARAMS_FORCE_SHOUTED_CRITICAL")
											ELSE
												PLAY_PED_AMBIENT_SPEECH_NATIVE(pedId,"SHOUT_THREATEN_PED","SPEECH_PARAMS_FORCE_SHOUTED_CRITICAL")
											ENDIF
								
											SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_AttackLineDone)
											NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - AMBIENT_SPEECH - ATTACK LINE - A") NET_NL()
										ENDIF
									ENDIF
									
								ELSE
									MAKE_PED_TURN(pedId, iStaggeredPedCount)
								ENDIF
							ENDIF
						ENDIF
					ELSE
						MAKE_PED_TURN(pedId, iStaggeredPedCount)
                    ENDIF
					
                ENDIF
            ENDIF
        BREAK
		
        CASE ePED_FIGHTING
            IF NOT IS_NET_PED_INJURED(serverBd.sFMMC_SBD.niPed[iStaggeredPedCount])
                IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBd.sFMMC_SBD.niPed[iStaggeredPedCount])
				OR (NOT IS_NETWORK_ID_OWNED_BY_PARTICIPANT(serverBd.sFMMC_SBD.niPed[iStaggeredPedCount]) AND NETWORK_IS_HOST_OF_THIS_SCRIPT())
                    pedId = NET_TO_PED(serverBd.sFMMC_SBD.niPed[iStaggeredPedCount])
                    IF NOT IS_PED_IN_COMBAT(pedId) 
					//IF GET_SCRIPT_TASK_STATUS(pedId, SCRIPT_TASK_COMBAT_HATED_TARGETS_AROUND_PED) != PERFORMING_TASK
                    //AND GET_SCRIPT_TASK_STATUS(pedId, SCRIPT_TASK_COMBAT_HATED_TARGETS_AROUND_PED) != WAITING_TO_START_TASK
                        IF serverBD.PedData[iStaggeredPedCount].bInitialAttackDone = FALSE
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedId, FALSE)
							SET_PED_RELATIONSHIP_GROUP_HASH(pedId, rgFM_AiHate)
							PRINTLN("    ----->     HIDEOUT - SET PED RELGROUP rgFM_AiHate - C - PED ", iStaggeredPedCount)	
							SET_PED_AS_ENEMY(pedId, TRUE)
	                        //TASK_COMBAT_HATED_TARGETS_IN_AREA(pedId, GET_ENTITY_COORDS(pedId), 75.0)
							TASK_COMBAT_HATED_TARGETS_IN_AREA(pedId, serverBD.vHideoutCentre, serverBD.fHideoutRadius*1.5)
							serverBD.PedData[iStaggeredPedCount].bInitialAttackDone = TRUE
							NET_PRINT_TIME() NET_PRINT("     ---------->     ePED_FIGHTING - bInitialAttackDone     <----------     ") NET_NL()
						ELSE
							IF GET_SCRIPT_TASK_STATUS(pedId, SCRIPT_TASK_GUARD_SPHERE_DEFENSIVE_AREA) != PERFORMING_TASK
                    		AND GET_SCRIPT_TASK_STATUS(pedId, SCRIPT_TASK_GUARD_SPHERE_DEFENSIVE_AREA) != WAITING_TO_START_TASK
								//TASK_STAND_GUARD(pedId, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iStaggeredPedCount].vPos, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iStaggeredPedCount].fHead, "Scenario_Stand_Guard")
								TASK_GUARD_SPHERE_DEFENSIVE_AREA(pedId, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iStaggeredPedCount].vPos, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iStaggeredPedCount].fHead, 10.0, -1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iStaggeredPedCount].vPos, 10.0)
								NET_PRINT_TIME() NET_PRINT("     ---------->     ePED_FIGHTING - TASK_STAND_GUARD     <----------     ") NET_NL()
							ENDIF
						ENDIF
                    ENDIF
					
					UPDATE_PED_ACCURACY(iStaggeredPedCount)
					
					//Attack Dialogue
					IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_AttackLineDone)
					AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_AttackLineDone)
						IF iStaggeredPedCount = 0
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								
								SET_GANG_PED_VOICE(pedId)
								IF serverBD.iNumParticpants > 1
									PLAY_PED_AMBIENT_SPEECH_NATIVE(NET_TO_PED(serverBd.sFMMC_SBD.niPed[iStaggeredPedCount]),"SHOUT_THREATEN_GANG","SPEECH_PARAMS_FORCE_SHOUTED_CRITICAL")
								ELSE
									PLAY_PED_AMBIENT_SPEECH_NATIVE(NET_TO_PED(serverBd.sFMMC_SBD.niPed[iStaggeredPedCount]),"SHOUT_THREATEN_PED","SPEECH_PARAMS_FORCE_SHOUTED_CRITICAL")
								ENDIF
								
								SET_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_AttackLineDone)
								NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - AMBIENT_SPEECH - ATTACK LINE - B") NET_NL()
							ELSE
								KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
							ENDIF
						ENDIF
					ENDIF
                ENDIF
            ENDIF
        BREAK
                
    ENDSWITCH

ENDPROC

//PURPOSE: Checks if the gang peds should be blipped permanently
FUNC BOOL SHOULD_ALWAYS_BLIP_PEDS()
	SWITCH serverBD.eHideoutVariation
		CASE HIDEOUT_KILL_PEDS
			IF (serverBD.iKillGoal - serverBD.iTotalKills) <= LAST_PEDS_NUM
				RETURN TRUE
			ENDIF
		BREAK
		/*CASE HIDEOUT_TERRITORY_CONTROL
			IF GET_SERVER_HIDEOUT_STAGE() >= HIDEOUT_STAGE_SECONDARY
				RETURN TRUE
			ENDIF
		BREAK
		CASE HIDEOUT_PACKAGE_STEAL
			//IF GET_SERVER_HIDEOUT_STAGE() = HIDEOUT_STAGE_REWARDS_AND_FEEDACK
			//	RETURN TRUE
			//ENDIF
		BREAK*/
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Adds blips for alive enemies and removes them for dead ones.
PROC PROCESS_ENEMY_PED_BLIPS(INT iPed, BOOL bAlwaysBlip)
    
	//IF NOT SHOULD_ALWAYS_BLIP_PEDS()	//(serverBD.iKillGoal - serverBD.iTotalKills) > LAST_PEDS_NUM
		UPDATE_ENEMY_NET_PED_BLIP( serverBD.sFMMC_SBD.niPed[iPed], PedBlipData[iPed], DEFAULT, FALSE, bAlwaysBlip )
	/*ELSE
		IF NOT IS_NET_PED_INJURED(serverBD.sFMMC_SBD.niPed[iPed])
			IF NOT DOES_BLIP_EXIST(PedBlipData[iPed].BlipID)
				PedBlipData[iPed].BlipID = ADD_BLIP_FOR_ENTITY(NET_TO_PED(serverBD.sFMMC_SBD.niPed[iPed]))
	            SET_BLIP_SCALE(PedBlipData[iPed].BlipID, BLIP_SIZE_NETWORK_PED)
	           	SET_BLIP_SPRITE(PedBlipData[iped].BlipID, RADAR_TRACE_AI)
				SET_BLIP_COLOUR(PedBlipData[iPed].BlipID, BLIP_COLOUR_RED)
				SHOW_HEIGHT_ON_BLIP(PedBlipData[iPed].BlipID, TRUE)
				NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - PROCESS_ENEMY_PED_BLIPS - BLIP ADDED TO PED ") NET_PRINT_INT(iPed) NET_NL()
			ELSE
				IF GET_BLIP_ALPHA(PedBlipData[iPed].BlipID) != 255
					SET_BLIP_ALPHA(PedBlipData[iPed].BlipID, 255)
				ENDIF
			ENDIF
		ELSE
			IF DOES_BLIP_EXIST(PedBlipData[iPed].BlipID)
				REMOVE_BLIP(PedBlipData[iPed].BlipID)
				NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - PROCESS_ENEMY_PED_BLIPS - BLIP REMOVED FROM PED ") NET_PRINT_INT(iPed) NET_NL()
			ENDIF
		ENDIF
	ENDIF	*/
ENDPROC

//PURPOSE: Loops through the peds
PROC PROCESS_PED_LOOP()
	
	#IF IS_DEBUG_BUILD #IF SCRIPT_PROFILER_ACTIVE  OPEN_SCRIPT_PROFILE_MARKER_GROUP("PROCESS_PED_LOOP") #ENDIF #ENDIF
	
	INT i
	BOOL bAlwaysBlip = SHOULD_ALWAYS_BLIP_PEDS()
	#IF IS_DEBUG_BUILD #IF SCRIPT_PROFILER_ACTIVE  ADD_SCRIPT_PROFILE_PRE_LOOP_MARKER("loop") #ENDIF #ENDIF
	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfPeds i
		#IF IS_DEBUG_BUILD #IF SCRIPT_PROFILER_ACTIVE   #ENDIF #ENDIF
		
		TRACK_KILLS(i)
		#IF IS_DEBUG_BUILD #IF SCRIPT_PROFILER_ACTIVE  ADD_SCRIPT_PROFILE_MARKER("TRACK_KILLS") #ENDIF #ENDIF
		
		PROCESS_ENEMY_PED_BLIPS(i, bAlwaysBlip)
		#IF IS_DEBUG_BUILD #IF SCRIPT_PROFILER_ACTIVE  ADD_SCRIPT_PROFILE_MARKER("PROCESS_ENEMY_PED_BLIPS") #ENDIF #ENDIF
		
	ENDREPEAT
	#IF IS_DEBUG_BUILD #IF SCRIPT_PROFILER_ACTIVE  ADD_SCRIPT_PROFILE_POST_LOOP_MARKER() #ENDIF #ENDIF
	
	#IF IS_DEBUG_BUILD #IF SCRIPT_PROFILER_ACTIVE  CLOSE_SCRIPT_PROFILE_MARKER_GROUP() #ENDIF #ENDIF
	
ENDPROC

//PURPOSE: Loops through the pickups
//PROC PROCESS_PICKUP_LOOP()
//	/*IF serverBD.eHideoutVariation = HIDEOUT_PACKAGE_STEAL
//		INT i
//		CLEAR_BIT(iBoolsBitSet, biAnyPackageDropped)
//		CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_HasPackage)
//		REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons i	
//			CONTROL_PICKUP_BLIPS(i)
//			CONTROL_PICKUP_COLLECTION(i)
//			CONTROL_PACKAGE_DELIVERY(i)
//		ENDREPEAT
//	ENDIF*/
//	//CONTROL_HEALTH_PICKUP_BLIPS()
//ENDPROC

//PURPOSE: Controls the players Kill Streaks
//PROC PROCESS_KILL_STREAKS()
//	IF IS_NET_PLAYER_OK(PLAYER_ID())
//		IF iKillStreak >= BIG_STREAK
//			IF iKillStreakMultiplier != LARGE_STREAK_MULTIPLIER
//				SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_KILLS_LARGE)
//				iKillStreakMultiplier = LARGE_STREAK_MULTIPLIER
//				NET_PRINT_TIME() NET_PRINT("     ---------->     PROCESS_KILL_STREAKS - LARGE KILL STREAK STARTED - MULTIPLIER = ") NET_PRINT_INT(iKillStreakMultiplier) NET_NL()
//			ENDIF
//		ELIF iKillStreak >= MEDIUM_STREAK
//			IF iKillStreakMultiplier != MED_STREAK_MULTIPLIER
//				SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_KILLS_MED)
//				iKillStreakMultiplier = MED_STREAK_MULTIPLIER
//				NET_PRINT_TIME() NET_PRINT("     ---------->     PROCESS_KILL_STREAKS - MEDIUM KILL STREAK STARTED - MULTIPLIER = ") NET_PRINT_INT(iKillStreakMultiplier) NET_NL()
//			ENDIF
//		ELIF iKillStreak >= SMALL_STREAK
//			IF iKillStreakMultiplier != SMALL_STREAK_MULTIPLIER
//				SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_KILLS_SMALL)
//				iKillStreakMultiplier = SMALL_STREAK_MULTIPLIER
//				NET_PRINT_TIME() NET_PRINT("     ---------->     PROCESS_KILL_STREAKS - SMALL KILL STREAK STARTED - MULTIPLIER = ") NET_PRINT_INT(iKillStreakMultiplier) NET_NL()
//			ENDIF
//		ENDIF
//	ELSE
//		IF iKillStreak > 0
//			iKillStreak = 0
//			NET_PRINT_TIME() NET_PRINT("     ---------->     PROCESS_KILL_STREAKS - KILL STREAK TRACKING RESET - KILLS = ") NET_PRINT_INT(0) NET_NL()
//		ENDIF
//		IF iKillStreakMultiplier > 1
//			iKillStreakMultiplier = 1
//			NET_PRINT_TIME() NET_PRINT("     ---------->     PROCESS_KILL_STREAKS - KILL STREAK ENDED - MULTIPLIER = ") NET_PRINT_INT(1) NET_NL()
//		ENDIF
//	ENDIF
//ENDPROC

FUNC FLOAT GET_MISSION_END_XP_MULTIPLIER()
	INT iParticipant
	FLOAT fMultiplier = 1.0
	
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
			PLAYER_INDEX PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))
			//PED_INDEX PlayerPedId = GET_PLAYER_PED(PlayerId)
			
			IF PlayerId != PLAYER_ID()
				
				IF ARE_PLAYERS_ON_SAME_ACTIVE_CLAN(PLAYER_ID(), PlayerId)
					NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - GET_MISSION_END_XP_MULTIPLIER - WITH A CREW MEMBER - fMultiplier = ") NET_PRINT_FLOAT(2.0) NET_NL()
					RETURN 2.0
				ENDIF
				
				fMultiplier = 1.5
				NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - GET_MISSION_END_XP_MULTIPLIER - WITH OTHER PLAYERS - fMultiplier = ") NET_PRINT_FLOAT(fMultiplier) NET_NL()
			ENDIF
			
		ENDIF
	ENDREPEAT
	
	RETURN fMultiplier
ENDFUNC

// *********************************** \\
//			PARTICIPANT LOOP		   \\
// *********************************** \\

//PROC PRE_PROCESS_PARTICIPANT_LOOP()
//	
//ENDPROC
//
//PROC PROCESS_PARTICPANT_LOOP()
//	IF serverBD.eHideoutVariation = HIDEOUT_KILL_PEDS
//	//OR serverBD.eHideoutVariation = HIDEOUT_PACKAGE_STEAL
//		INT iParticipant
//		CLEAR_BIT(iBoolsBitSet, biSomeoneElseIsCrackingSafe)
//		CLEAR_BIT(iBoolsBitSet, biAnyoneCrackingSafe)
//		iLocalPlayerCrackingSafe = -1
//		
//		REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
//			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
//				PLAYER_INDEX PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))
//				//PED_INDEX PlayerPedId = GET_PLAYER_PED(PlayerId)
//				
//				//Check if any player is safe cracking
//				IF IS_BIT_SET(playerBD[iParticipant].iPlayerBitSet, biP_InSafeUnlockMinigame)
//					SET_BIT(iBoolsBitSet, biAnyoneCrackingSafe)
//					IF iParticipant <> PARTICIPANT_ID_TO_INT()
//						SET_BIT(iBoolsBitSet, biSomeoneElseIsCrackingSafe)
//						iLocalPlayerCrackingSafe = NATIVE_TO_INT(PlayerId)
//						FORCE_EXIT_SAFE_CRACKING(SafeCrackData)
//						CLEAR_BIT(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_InSafeUnlockMinigame)
//					ENDIF
//				ENDIF
//				
//			ENDIF
//		ENDREPEAT
//	ENDIF
//ENDPROC
//
//PROC POST_PROCESS_PARTICIPANT_LOOP()
//	
//ENDPROC

//PROC MAINTAIN_PARTICPANT_LOOP()
//	
//	PRE_PROCESS_PARTICIPANT_LOOP()
//	PROCESS_PARTICPANT_LOOP()
//	POST_PROCESS_PARTICIPANT_LOOP()
//	
//ENDPROC



// *********************************** \\
//				STAGGERED			   \\
//			PARTICIPANT LOOP		   \\
// *********************************** \\

PROC PRE_PROCESS_STAGGERED_PARTICIPANT_LOOP()
	
ENDPROC

PROC PROCESS_STAGGERED_PARTICPANT_LOOP()
	PROCESS_GANG_PEDS_BODY()
ENDPROC

PROC POST_PROCESS_STAGGERED_PARTICIPANT_LOOP()
	iStaggeredPedCount++    
    IF iStaggeredPedCount >= g_FMMC_STRUCT_ENTITIES.iNumberOfPeds
        iStaggeredPedCount = 0
    ENDIF
ENDPROC

PROC MAINTAIN_STAGGERED_PARTICPANT_LOOP()

	PRE_PROCESS_STAGGERED_PARTICIPANT_LOOP()
	PROCESS_STAGGERED_PARTICPANT_LOOP()
	POST_PROCESS_STAGGERED_PARTICIPANT_LOOP()

ENDPROC


//PURPOSE: Checks to see if the mission should be set as no longer joinable
PROC CONTROL_MISSION_JOINABLE_CHECKS()
	//Check if Mission should not be joinable
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_MissionNotJoinable)	
			IF GET_SERVER_HIDEOUT_STAGE() >= HIDEOUT_STAGE_REWARDS_AND_FEEDACK
			OR (/*serverBD.eHideoutVariation != HIDEOUT_PACKAGE_STEAL AND */GET_SERVER_HIDEOUT_STAGE() >= HIDEOUT_STAGE_SECONDARY)
			//OR (serverBD.eHideoutVariation = HIDEOUT_PACKAGE_STEAL AND PASSED_VARIATION_HALF_WAY())
			//OR IS_BIT_SET(serverBD.iServerBitSet, biS_SafeUnlocked)
				SET_BIT(serverBD.iServerBitSet, biS_MissionNotJoinable)	
				NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - SERVER - CONTROL_MISSION_JOINABLE_CHECKS - biS_MissionNotJoinable SET ") NET_NL()
			ENDIF
		ENDIF
	ENDIF
	
	//Check if we should say we are on a not joinable mission
	IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciMissionIsNotJoinable)
		IF IS_BIT_SET(serverBD.iServerBitSet, biS_MissionNotJoinable)	
			SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciMissionIsNotJoinable)
			NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - CLIENT - CONTROL_MISSION_JOINABLE_CHECKS - ciMissionIsNotJoinable SET ") NET_NL()
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Controls stats for how many times the player has played a Gang Attack
PROC MAINTAIN_TIMES_PLAYED_STAT()
	//Increment Hideouts Played
	IF NOT IS_BIT_SET(iBoolsBitSet, biTimesPlayedIncremented)
		IF GET_CLIENT_HIDEOUT_STAGE(PARTICIPANT_ID()) = HIDEOUT_STAGE_PRIMARY
		OR GET_CLIENT_HIDEOUT_STAGE(PARTICIPANT_ID()) = HIDEOUT_STAGE_SECONDARY
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_CRGANGHIDE, 1)
			RESET_REMINDER_TIMER_FOR_ACTIVITY(FMMC_TYPE_GANGHIDEOUT)
			SET_BIT(iBoolsBitSet, biTimesPlayedIncremented)
			NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - CLIENT - MAINTAIN_TIMES_PLAYED_STAT - MP_STAT_CRGANGHIDE +1 ") NET_NL()
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Increments specific Gang counters and if necessary triggers a Bounty
PROC GANG_BOUNTY_CHECK()
	SWITCH serverBD.PedGangModel
		//VAGOS
		CASE G_M_Y_MEXGOON_01
		CASE G_M_Y_MEXGOON_02
			MPGlobalsAmbience.HideoutData.iSpecificGangsAttackedCounter[0]++
			NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - GANG_BOUNTY_CHECK - G_M_Y_MEXGOON_01 - Count = ") NET_PRINT_INT(MPGlobalsAmbience.HideoutData.iSpecificGangsAttackedCounter[0]) NET_NL()
			IF MPGlobalsAmbience.HideoutData.iSpecificGangsAttackedCounter[0] >= GHO_BOUNTY_TRIGGER
				TRIGGER_NPC_BOUNTY(PLAYER_ID(), "GHO_GANGB0", CHAR_MP_MEX_BOSS, TRUE)
				MPGlobalsAmbience.HideoutData.iSpecificGangsAttackedCounter[0] = 0
				NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - GANG_BOUNTY_CHECK - G_M_Y_MEXGOON_01 - BOUNTY TRIGGERED") NET_NL()
			ENDIF
		BREAK
	
		//LOST
		CASE G_M_Y_Lost_01
			MPGlobalsAmbience.HideoutData.iSpecificGangsAttackedCounter[1]++
			NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - GANG_BOUNTY_CHECK - G_M_Y_Lost_01 - Count = ") NET_PRINT_INT(MPGlobalsAmbience.HideoutData.iSpecificGangsAttackedCounter[1]) NET_NL()
			IF MPGlobalsAmbience.HideoutData.iSpecificGangsAttackedCounter[1] >= GHO_BOUNTY_TRIGGER
				TRIGGER_NPC_BOUNTY(PLAYER_ID(), "GHO_GANGB1", CHAR_MP_BIKER_BOSS, TRUE)
				MPGlobalsAmbience.HideoutData.iSpecificGangsAttackedCounter[1] = 0
				NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - GANG_BOUNTY_CHECK - G_M_Y_Lost_01 - BOUNTY TRIGGERED") NET_NL()
			ENDIF
		BREAK
	
		//BALLA
		CASE G_M_Y_BallaOrig_01
			MPGlobalsAmbience.HideoutData.iSpecificGangsAttackedCounter[2]++
			NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - GANG_BOUNTY_CHECK - G_M_Y_BallaOrig_01 - Count = ") NET_PRINT_INT(MPGlobalsAmbience.HideoutData.iSpecificGangsAttackedCounter[2]) NET_NL()
			IF MPGlobalsAmbience.HideoutData.iSpecificGangsAttackedCounter[2] >= GHO_BOUNTY_TRIGGER
				TRIGGER_NPC_BOUNTY(PLAYER_ID(), "GHO_GANGB2", CHAR_BLOCKED, TRUE)
				MPGlobalsAmbience.HideoutData.iSpecificGangsAttackedCounter[2] = 0
				NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - GANG_BOUNTY_CHECK - G_M_Y_BallaOrig_01 - BOUNTY TRIGGERED") NET_NL()
			ENDIF
		BREAK
	
		//FAMILY
		CASE G_M_Y_FAMCA_01
			MPGlobalsAmbience.HideoutData.iSpecificGangsAttackedCounter[3]++
			NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - GANG_BOUNTY_CHECK - G_M_Y_FAMCA_01 - Count = ") NET_PRINT_INT(MPGlobalsAmbience.HideoutData.iSpecificGangsAttackedCounter[3]) NET_NL()
			IF MPGlobalsAmbience.HideoutData.iSpecificGangsAttackedCounter[3] >= GHO_BOUNTY_TRIGGER
				TRIGGER_NPC_BOUNTY(PLAYER_ID(), "GHO_GANGB3", CHAR_MP_FAM_BOSS, TRUE)
				MPGlobalsAmbience.HideoutData.iSpecificGangsAttackedCounter[3] = 0
				NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - GANG_BOUNTY_CHECK - G_M_Y_FAMCA_01 - BOUNTY TRIGGERED") NET_NL()
			ENDIF
		BREAK
	
		//KOREAN
		CASE G_M_Y_Korean_02
			MPGlobalsAmbience.HideoutData.iSpecificGangsAttackedCounter[4]++
			NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - GANG_BOUNTY_CHECK - G_M_Y_Korean_02 - Count = ") NET_PRINT_INT(MPGlobalsAmbience.HideoutData.iSpecificGangsAttackedCounter[4]) NET_NL()
			IF MPGlobalsAmbience.HideoutData.iSpecificGangsAttackedCounter[4] >= GHO_BOUNTY_TRIGGER
				TRIGGER_NPC_BOUNTY(PLAYER_ID(), "GHO_GANGB4",CHAR_BLOCKED, TRUE)
				MPGlobalsAmbience.HideoutData.iSpecificGangsAttackedCounter[4] = 0
				NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - GANG_BOUNTY_CHECK - G_M_Y_Korean_02 - BOUNTY TRIGGERED") NET_NL()
			ENDIF
		BREAK
	
		//HILLBILLY
		CASE A_M_M_Hillbilly_02
			MPGlobalsAmbience.HideoutData.iSpecificGangsAttackedCounter[5]++
			NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - GANG_BOUNTY_CHECK - A_M_M_Hillbilly_02 - Count = ") NET_PRINT_INT(MPGlobalsAmbience.HideoutData.iSpecificGangsAttackedCounter[5]) NET_NL()
			IF MPGlobalsAmbience.HideoutData.iSpecificGangsAttackedCounter[5] >= GHO_BOUNTY_TRIGGER
				TRIGGER_NPC_BOUNTY(PLAYER_ID(), "GHO_GANGB5", CHAR_BLOCKED, TRUE)
				MPGlobalsAmbience.HideoutData.iSpecificGangsAttackedCounter[5] = 0
				NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - GANG_BOUNTY_CHECK - A_M_M_Hillbilly_02 - BOUNTY TRIGGERED") NET_NL()
			ENDIF
		BREAK
	
		//SALVA
		CASE G_M_Y_SalvaGoon_01
			MPGlobalsAmbience.HideoutData.iSpecificGangsAttackedCounter[6]++
			NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - GANG_BOUNTY_CHECK - G_M_Y_SalvaGoon_01 - Count = ") NET_PRINT_INT(MPGlobalsAmbience.HideoutData.iSpecificGangsAttackedCounter[6]) NET_NL()
			IF MPGlobalsAmbience.HideoutData.iSpecificGangsAttackedCounter[6] >= GHO_BOUNTY_TRIGGER
				TRIGGER_NPC_BOUNTY(PLAYER_ID(), "GHO_GANGB6", CHAR_BLOCKED, TRUE)
				MPGlobalsAmbience.HideoutData.iSpecificGangsAttackedCounter[6] = 0
				NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - GANG_BOUNTY_CHECK - G_M_Y_SalvaGoon_01 - BOUNTY TRIGGERED") NET_NL()
			ENDIF
		BREAK
	
		//PROFESSIONAL
		CASE MP_G_M_Pros_01
			MPGlobalsAmbience.HideoutData.iSpecificGangsAttackedCounter[7]++
			NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - GANG_BOUNTY_CHECK - MP_G_M_Pros_01 - Count = ") NET_PRINT_INT(MPGlobalsAmbience.HideoutData.iSpecificGangsAttackedCounter[7]) NET_NL()
			IF MPGlobalsAmbience.HideoutData.iSpecificGangsAttackedCounter[7] >= GHO_BOUNTY_TRIGGER
				TRIGGER_NPC_BOUNTY(PLAYER_ID(), "GHO_GANGB7", CHAR_MP_PROF_BOSS, TRUE)
				MPGlobalsAmbience.HideoutData.iSpecificGangsAttackedCounter[7] = 0
				NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - GANG_BOUNTY_CHECK - MP_G_M_Pros_01 - BOUNTY TRIGGERED") NET_NL()
			ENDIF
		BREAK
		
		//ARMENIAN
		CASE G_M_M_ArmGoon_01
			MPGlobalsAmbience.HideoutData.iSpecificGangsAttackedCounter[8]++
			NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - GANG_BOUNTY_CHECK - G_M_M_ArmGoon_01 - Count = ") NET_PRINT_INT(MPGlobalsAmbience.HideoutData.iSpecificGangsAttackedCounter[8]) NET_NL()
			IF MPGlobalsAmbience.HideoutData.iSpecificGangsAttackedCounter[8] >= GHO_BOUNTY_TRIGGER
				TRIGGER_NPC_BOUNTY(PLAYER_ID(), "GHO_GANGB8", CHAR_BLOCKED, TRUE)
				MPGlobalsAmbience.HideoutData.iSpecificGangsAttackedCounter[8] = 0
				NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - GANG_BOUNTY_CHECK - G_M_M_ArmGoon_01 - BOUNTY TRIGGERED") NET_NL()
			ENDIF
		BREAK
		
		//TRIAD
		CASE G_M_M_ChiGoon_01
			MPGlobalsAmbience.HideoutData.iSpecificGangsAttackedCounter[9]++
			NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - GANG_BOUNTY_CHECK - G_M_M_ChiGoon_01 - Count = ") NET_PRINT_INT(MPGlobalsAmbience.HideoutData.iSpecificGangsAttackedCounter[9]) NET_NL()
			IF MPGlobalsAmbience.HideoutData.iSpecificGangsAttackedCounter[9] >= GHO_BOUNTY_TRIGGER
				TRIGGER_NPC_BOUNTY(PLAYER_ID(), "GHO_GANGB9",CHAR_BLOCKED, TRUE)
				MPGlobalsAmbience.HideoutData.iSpecificGangsAttackedCounter[9] = 0
				NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - GANG_BOUNTY_CHECK - G_M_M_ChiGoon_01 - BOUNTY TRIGGERED") NET_NL()
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC


// *********************************** \\
//				CLIENT LOGIC		   \\
// *********************************** \\

PROC PROCESS_HIDEOUT_STAGE_JIP_IN_CLIENT()
	
	PROCESS_PED_LOOP()
	
	IF GET_SERVER_HIDEOUT_STAGE() != HIDEOUT_STAGE_JIP_IN
		IF WARP_TO_GANG_HIDEOUT()
		
			/*IF NOT IS_SCREEN_FADED_IN()
			AND NOT IS_SCREEN_FADING_IN()
				DO_SCREEN_FADE_IN(500)
				NET_PRINT("    ----->     HIDEOUT - PROCESS_HIDEOUT_STAGE_JIP_IN_CLIENT - FADE IN ") NET_NL()
			ENDIF
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)*/
			
			SET_CLIENT_HIDEOUT_STAGE(HIDEOUT_STAGE_SETUP)
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_HIDEOUT_STAGE_SETUP_CLIENT()
	
	PROCESS_PED_LOOP()
	
	// Move on to next stage if server does.
	IF GET_SERVER_HIDEOUT_STAGE() != HIDEOUT_STAGE_SETUP
		////IF serverBD.eHideoutVariation != HIDEOUT_PACKAGE_STEAL
			SET_MISSION_SPAWN_ANGLED_AREA(serverBD.PlayerSpawnArea.vMin, serverBD.PlayerSpawnArea.vMax, serverBD.PlayerSpawnArea.fWidth)
			SET_MISSION_SPAWN_OCCLUSION_ANGLED_AREA(serverBD.MainArea.vMin, serverBD.MainArea.vMax, serverBD.MainArea.fWidth)
			//SET_MISSION_SPAWN_SPHERE(serverBD.vHideoutCentre, serverBD.fHideoutRadius*1.15)
			//SET_MISSION_SPAWN_OCCLUSION_SPHERE(serverBD.vHideoutCentre, serverBD.fHideoutRadius*0.5) // added by NeilF 15/10/2012 - to fix PT 864184
			SET_PLAYER_WILL_SPAWN_FACING_COORDS(serverBD.vHideoutCentre, TRUE, FALSE)
		////ELSE
		/*IF serverBD.eHideoutVariation = HIDEOUT_PACKAGE_STEAL
			SET_MAX_NUM_PORTABLE_PICKUPS_CARRIED_BY_PLAYER(serverBD.PickUpData[0].Model, 1)
		ENDIF*/
		
		IF GET_SERVER_HIDEOUT_STAGE() != HIDEOUT_STAGE_WARNING
			SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), TRUE)
		ENDIF
				
		SET_CLIENT_HIDEOUT_STAGE(GET_SERVER_HIDEOUT_STAGE())
	ENDIF
	
ENDPROC

PROC PROCESS_HIDEOUT_STAGE_WARNING_CLIENT()
	
	PROCESS_PED_LOOP()
	//PROCESS_PICKUP_LOOP()
	
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		CONTROL_HELP_TEXT()
		CONTROL_GOD_TEXT()
	ENDIF
	
	// Move on to next stage if server does.
	IF GET_SERVER_HIDEOUT_STAGE() != HIDEOUT_STAGE_WARNING
		SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), TRUE)
		SET_CLIENT_HIDEOUT_STAGE(GET_SERVER_HIDEOUT_STAGE())
	ENDIF
	
ENDPROC

PROC MAINTAIN_DISABLED_HELP()
	//Clean up the UI if we go on an event
	IF NOT IS_BIT_SET(iBoolsBitSet, biCleanUpUi)
		IF SHOULD_HIDE_GANG_ATTACK_UI()
			IF NOT HAS_NET_TIMER_STARTED(iBlockedHelpTimer)
				START_NET_TIMER(iBlockedHelpTimer)
				Clear_Any_Objective_Text_From_This_Script()
			ELIF HAS_NET_TIMER_EXPIRED(iBlockedHelpTimer, 7500)
			AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				SET_BIT(iBoolsBitSet, biCleanUpUi)
				IF NOT IS_PLAYER_AN_ANIMAL(PLAYER_ID())
					IF GB_GET_PLAYER_UI_LEVEL(PLAYER_ID()) = GB_UI_LEVEL_FULL
						PRINT_HELP_NO_SOUND("GB_EV_GA_UI")
					ELSE
						PRINT_HELP_NO_SOUND("FM_EV_GA_UI")
						SET_FREEMODE_EVENT_HELP_BACKGROUND()
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC


PROC PROCESS_HIDEOUT_STAGE_PRIMARY_CLIENT()
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		OPEN_SCRIPT_PROFILE_MARKER_GROUP("PROCESS_HIDEOUT_STAGE_PRIMARY_CLIENT")
	#ENDIF
	#ENDIF
	
	// If server has moved on then we should too
	IF GET_SERVER_HIDEOUT_STAGE() > HIDEOUT_STAGE_PRIMARY
		SET_CLIENT_HIDEOUT_STAGE(GET_SERVER_HIDEOUT_STAGE())	//HIDEOUT_STAGE_SECONDARY)
	ENDIF
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("server moved on")
	#ENDIF
	#ENDIF
		
	PROCESS_PED_LOOP()	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("PROCESS_PED_LOOP")
	#ENDIF
	#ENDIF
	
	//PROCESS_PICKUP_LOOP()
	//#IF IS_DEBUG_BUILD
	//#IF SCRIPT_PROFILER_ACTIVE 
	//	ADD_SCRIPT_PROFILE_MARKER("PROCESS_PICKUP_LOOP")
	//#ENDIF
	//#ENDIF
	
	//PROCESS_KILL_STREAKS()
	MAINTAIN_DISABLED_HELP()	
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF NOT IS_BIT_SET(iBoolsBitSet, biTriggerStartBigMessage)
			IF !SHOULD_HIDE_GANG_ATTACK_UI()
				SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_START_OF_JOB, "GHO_NAME")	//Gang Attack
			ENDIF
			SET_BIT(iBoolsBitSet, biTriggerStartBigMessage)
			NET_PRINT("    ----->     HIDEOUT - BIG_MESSAGE_START_OF_JOB - TRIGGERED") NET_NL()
			Remove_Retained_Gang_Attack_Blip()
			NET_PRINT("    ----->     HIDEOUT - KGM MP: RETAINED GANG ATTACK RADIUS BLIP - REQUESTED REMOVAL") NET_NL()
			
			DISABLE_MP_PASSIVE_MODE(PMER_START_OF_A_MATCH)
			IF NOT SHOULD_HIDE_GANG_ATTACK_UI()
//				NETWORK_SET_FRIENDLY_FIRE_OPTION(FALSE)
			ENDIF
			IF IS_NET_PLAYER_OK(PLAYER_ID())
				SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableLockonToRandomPeds, TRUE)
			ENDIF
			
			//Send Ticker to other Players
			SCRIPT_EVENT_DATA_TICKER_MESSAGE TickerData
			TickerData.TickerEvent = TICKER_EVENT_GANG_HIDEOUT_STARTED
			BROADCAST_TICKER_EVENT(TickerData, ALL_PLAYERS())
		ENDIF
		
		CONTROL_HELP_TEXT()
		CONTROL_GOD_TEXT()
		IF IS_BIT_SET(iBoolsBitSet, biDeathTrackingDone)
			SET_PLAYER_RESPAWN_TO_CONSIDER_INTERIORS_ON_THIS_MISSION(FALSE) 
			CLEAR_BIT(iBoolsBitSet, biDeathTrackingDone)
			NET_PRINT("    ----->     HIDEOUT - biDeathTrackingDone CLEARED") NET_NL()
		ENDIF
	ELSE
		IF NOT IS_BIT_SET(iBoolsBitSet, biDeathTrackingDone)
			IF GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) != NULL	//NOT IS_COLLISION_MARKED_OUTSIDE(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)) 
                SET_PLAYER_RESPAWN_TO_CONSIDER_INTERIORS_ON_THIS_MISSION(TRUE) 
                NET_PRINT("    ----->     HIDEOUT - PLAYER DIED INSIDE ") NET_PRINT_VECTOR(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)) NET_PRINT(" - Setting dead spawn to consider interiors ") NET_NL()
			#IF IS_DEBUG_BUILD
			ELSE
				NET_PRINT("    ----->     HIDEOUT - PLAYER DIED OUTSIDE ") NET_PRINT_VECTOR(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)) NET_NL()
			#ENDIF
            ENDIF

			playerBD[PARTICIPANT_ID_TO_INT()].iDeaths++
			NET_PRINT("    ----->     HIDEOUT - biDeathTrackingDone SET - iDeaths++ = ") NET_PRINT_INT(playerBD[PARTICIPANT_ID_TO_INT()].iDeaths) NET_NL()
			SET_BIT(iBoolsBitSet, biDeathTrackingDone)
		ENDIF
	ENDIF
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("control help and god text")
	#ENDIF
	#ENDIF
	
	//PROCESS VARIATIONS
	SWITCH serverBD.eHideoutVariation
 		CASE HIDEOUT_KILL_PEDS
			DRAW_KILLS_BAR()
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
				ADD_SCRIPT_PROFILE_MARKER("DRAW_KILLS_BAR")
			#ENDIF
			#ENDIF
			
			/*CONTROL_SAFE_CRACKING()
			IF GET_SERVER_HIDEOUT_STAGE() > HIDEOUT_STAGE_PRIMARY
				SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE)
				SET_CLIENT_HIDEOUT_STAGE(GET_SERVER_HIDEOUT_STAGE())
			ENDIF
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
				ADD_SCRIPT_PROFILE_MARKER("CONTROL_SAFE_CRACKING")
			#ENDIF
			#ENDIF*/
			
		BREAK
		
		/*CASE HIDEOUT_TERRITORY_CONTROL
			PROCESS_TERRITORY_CONTROL_CLIENT()
			IF GET_SERVER_HIDEOUT_STAGE() > HIDEOUT_STAGE_PRIMARY
				SET_CLIENT_HIDEOUT_STAGE(GET_SERVER_HIDEOUT_STAGE())
			ENDIF
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
				ADD_SCRIPT_PROFILE_MARKER("PROCESS_TERRITORY_CONTROL_CLIENT")
			#ENDIF
			#ENDIF
		BREAK*/
		
		/*CASE HIDEOUT_PACKAGE_STEAL
			
			IF NOT HAS_SAFE_BEEN_CRACKED()
				CONTROL_SAFE_CRACKING()
				
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE 
					ADD_SCRIPT_PROFILE_MARKER("CONTROL_SAFE_CRACKING")
				#ENDIF
				#ENDIF
				
			ELSE
				//Move on if we have a package
				IF IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_HasPackage)
					SET_CLIENT_HIDEOUT_STAGE(HIDEOUT_STAGE_SECONDARY)
				ENDIF
			ENDIF
			
			//If the server is done then so are we
			IF GET_SERVER_HIDEOUT_STAGE() >= HIDEOUT_STAGE_REWARDS_AND_FEEDACK
				SET_CLIENT_HIDEOUT_STAGE(GET_SERVER_HIDEOUT_STAGE())
			ENDIF
		BREAK*/
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		CLOSE_SCRIPT_PROFILE_MARKER_GROUP()
	#ENDIF
	#ENDIF	
	
ENDPROC

PROC PROCESS_HIDEOUT_STAGE_SECONDARY_CLIENT()
	
	PROCESS_PED_LOOP()	
	//PROCESS_PICKUP_LOOP()
	//PROCESS_KILL_STREAKS()
	
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		CONTROL_GOD_TEXT()
	ENDIF
	
	MAINTAIN_DISABLED_HELP()
	
	//PROCESS VARIATIONS
	SWITCH serverBD.eHideoutVariation
 		CASE HIDEOUT_KILL_PEDS
			//Move on when player has left the area
			//IF HAS_PED_LEFT_AREA(PLAYER_PED_ID())
			//	SET_CLIENT_HIDEOUT_STAGE(HIDEOUT_STAGE_REWARDS_AND_FEEDACK)
			//ENDIF
			
			//CONTROL_SAFE_CRACKING()
		BREAK
		
		/*CASE HIDEOUT_TERRITORY_CONTROL
			//PROCESS_TERRITORY_CONTROL_CLIENT()
			//IF HAS_PED_LEFT_AREA(PLAYER_PED_ID())
			//	SET_CLIENT_HIDEOUT_STAGE(HIDEOUT_STAGE_REWARDS_AND_FEEDACK)
			//ENDIF
		BREAK*/
		
		/*CASE HIDEOUT_PACKAGE_STEAL
			CONTROL_SAFE_CRACKING()
			//If we no longer have a package move back
			IF NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_HasPackage)
			AND NOT IS_BIT_SET(playerBD[PARTICIPANT_ID_TO_INT()].iPlayerBitSet, biP_DeliveredPackage1)
				SET_CLIENT_HIDEOUT_STAGE(HIDEOUT_STAGE_PRIMARY)
				NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - PROCESS_HIDEOUT_STAGE_SECONDARY_CLIENT - GO BACK TO PRIMARY - PLAYER DOESN'T HAVE PACKAGE ") NET_NL()
			ENDIF
		BREAK*/
	ENDSWITCH
	
	//If the server is done then so are we
	IF GET_SERVER_HIDEOUT_STAGE() >= HIDEOUT_STAGE_REWARDS_AND_FEEDACK
		SET_CLIENT_HIDEOUT_STAGE(GET_SERVER_HIDEOUT_STAGE())
	ENDIF
ENDPROC

PROC PROCESS_HIDEOUT_STAGE_REWARDS_AND_FEEDACK_CLIENT()
	
	//INT iXp
	
	//PROCESS_PICKUP_LOOP()
	PROCESS_PED_LOOP()	
	//IF serverBD.eHideoutVariation = HIDEOUT_KILL_PEDS
	//	CONTROL_SAFE_CRACKING()
	//ENDIF
	
	IF HAS_NET_TIMER_EXPIRED(iMissionPassedTimer, MISSION_PASSED_DELAY)
		IF NOT IS_BIT_SET(iBoolsBitSet, biRewardDone)
			Clear_Any_Objective_Text_From_This_Script()
			
			GANG_BOUNTY_CHECK()
			
			//Increment Hideouts Done
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_GANGHIDWINS, 1)
			NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - PROCESS_HIDEOUT_STAGE_REWARDS_AND_FEEDACK_CLIENT - MP_STAT_GANGHIDWINS +1    = ") NET_PRINT_INT(GET_MP_INT_CHARACTER_STAT(MP_STAT_GANGHIDWINS)) NET_NL()
			
			//Increments Hideotus Done in City/Country
			IF GET_HASH_OF_MAP_AREA_AT_COORDS(serverBD.vHideoutCentre) = MAP_AREA_CITY
				IF serverBD.PedGangModel != G_M_Y_MEXGOON_01
				AND serverBD.PedGangModel != G_M_Y_MEXGOON_02
					INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_CR_GANGATTACK_VAGOS, 1)
					NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - PROCESS_HIDEOUT_STAGE_REWARDS_AND_FEEDACK_CLIENT - MP_STAT_CR_GANGATTACK_VAGOS +1    = ") NET_PRINT_INT(GET_MP_INT_CHARACTER_STAT(MP_STAT_CR_GANGATTACK_VAGOS)) NET_NL()
				ENDIF
				INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_CR_GANGATTACK_CITY, 1)
				NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - PROCESS_HIDEOUT_STAGE_REWARDS_AND_FEEDACK_CLIENT - MP_STAT_CR_GANGATTACK_CITY +1    = ") NET_PRINT_INT(GET_MP_INT_CHARACTER_STAT(MP_STAT_CR_GANGATTACK_CITY)) NET_NL()
			ELSE
				IF serverBD.PedGangModel != G_M_Y_Lost_01
					INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_CR_GANGATTACK_LOST, 1)
					NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - PROCESS_HIDEOUT_STAGE_REWARDS_AND_FEEDACK_CLIENT - MP_STAT_CR_GANGATTACK_LOST +1    = ") NET_PRINT_INT(GET_MP_INT_CHARACTER_STAT(MP_STAT_CR_GANGATTACK_LOST)) NET_NL()
				ENDIF
				INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_CR_GANGATTACK_COUNTRY, 1)
				NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - PROCESS_HIDEOUT_STAGE_REWARDS_AND_FEEDACK_CLIENT - MP_STAT_CR_GANGATTACK_COUNTRY +1    = ") NET_PRINT_INT(GET_MP_INT_CHARACTER_STAT(MP_STAT_CR_GANGATTACK_COUNTRY)) NET_NL()
			ENDIF
			
			//INT iCash = END_CASH_REWARD
			//iCash += (CASH_PER_KILL*EndScreenData.iTrackedKills)
			//iCash += ((CASH_PER_KILL/2)*EndScreenData.iPointBlankKills)
			//iCash += (CASH_PER_KILL*EndScreenData.iHeadshots)
			//GIVE_LOCAL_PLAYER_CASH(iCash)//, 1, TRUE, 0)
			
			/*IF serverBD.eHideoutVariation = HIDEOUT_KILL_PEDS
				//GIVE_LOCAL_PLAYER_CASH((serverBD.SafeData.iContentsAmount/serverBD.iNumParticpants))
				//NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - HIDEOUT_KILL_PEDS - SAFE CASH REWARD = ") NET_PRINT_INT((serverBD.SafeData.iContentsAmount/serverBD.iNumParticpants)) NET_NL()
			ELIF serverBD.eHideoutVariation = HIDEOUT_TERRITORY_CONTROL
				INT iCash = MULTIPLY_CASH_BY_TUNABLE(CASH_PACKAGE_AMOUNT*2)
				GIVE_LOCAL_PLAYER_CASH(iCash)
				NETWORK_EARN_FROM_TAKEOVER_GANG_TURF(iCash)
				NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - HIDEOUT_TERRITORY_CONTROL - CASH REWARD = ") NET_PRINT_INT(iCash) NET_NL()
				
			ELIF serverBD.eHideoutVariation = HIDEOUT_PACKAGE_STEAL
				//GIVE_LOCAL_PLAYER_CASH(CASH_PACKAGE_AMOUNT*5)
				//NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - HIDEOUT_PACKAGE_STEAL - CASH REWARD = ") NET_PRINT_INT(CASH_PACKAGE_AMOUNT*5) NET_NL()
				
				////INT iTotalAmmoCollected
				////GIVE_AMMO_FOR_EACH_TYPE(iTotalAmmoCollected)
				////SET_AMMO_REWARD_FOR_ENTITY(PLAYER_PED_ID(), iTotalAmmoCollected)
				////NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - HIDEOUT_PACKAGE_STEAL - AMMO REWARD = ") NET_PRINT_INT(iTotalAmmoCollected) NET_NL()
			ENDIF*/
			
			//FLOAT fMultiplier = GET_MISSION_END_XP_MULTIPLIER()
			
			//INT iXP = ROUND(END_XP_REWARD * fMultiplier)
			
			//iXp = GIVE_LOCAL_PLAYER_XP(eXPTYPE_CO_OP_AMBIENTFM_ON_SCRIPT, "XPT_MEDIUMT", XPTYPE_COMPLETE, XPCATEGORY_COMPLETE_HIDEOUT_CLEARED, END_XP_REWARD, 1)	//ROUND(END_XP_REWARD*(0.5*serverBD.iHighestNumParticpants))
			//NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - PROCESS_HIDEOUT_STAGE_REWARDS_AND_FEEDACK_CLIENT - XP = ") NET_PRINT_INT(iXp) NET_NL()
			
			//Do Big Message and End Screen
			//TEXT_LABEL_15 tlMessage
			//tlMessage = "GHO_TIT"
			//tlMessage += ENUM_TO_INT(serverBD.eHideoutVariation)
			//ADD_MISSION_SUMMARY_TITLE(tlMessage)
			//ADD_MISSION_SUMMARY_STAT_WITH_INT("MBOX_REP", iXp)	// ** TWH - RLB - Speirs 1437898
			//ADD_MISSION_SUMMARY_STAT_WITH_INT("GHO_ESTOK", serverBD.iTotalKills)
			//ADD_MISSION_SUMMARY_STAT_WITH_INT("GHO_ESPEK", EndScreenData.iTrackedKills)
			//ADD_MISSION_SUMMARY_STAT_WITH_INT("GHO_ESHSK", EndScreenData.iHeadshots)
			//ADD_MISSION_SUMMARY_STAT_WITH_INT("GHO_ESPBK", EndScreenData.iPointBlankKills)
	//		ADD_MISSION_SUMMARY_END_COMPLETION_XP("MBOX_XP", END_XP_REWARD) //
			//tlMessage = "GHO_END"
			//tlMessage += ENUM_TO_INT(serverBD.eHideoutVariation)
			//SETUP_NEW_BIG_MESSAGE_WITH_INT_AND_STRING(BIG_MESSAGE_MISSION_PASSED, END_XP_REWARD, tlMessage)
			//SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_MISSION_PASSED)
			
			//Remove Blips
			INT i
			REPEAT MAX_NUM_PEDS i
				IF DOES_BLIP_EXIST(PedBlipData[i].BlipID)
					REMOVE_BLIP(PedBlipData[i].BlipID)
				ENDIF
			ENDREPEAT
			/*INT k
			REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons k		//MAX_NUM_PICKUPS k
				IF DOES_BLIP_EXIST(biPickup[k])
					REMOVE_BLIP(biPickup[k])
					NET_PRINT(" HIDEOUT - SCRIPT CLEANUP - A - BLIP REMOVED ") NET_PRINT_INT(i) NET_NL()
				ENDIF
				IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.PickUpData[k].NetID)
					IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.PickUpData[k].NetID)
						IF NOT CAN_ANY_PLAYER_SEE_POINT(GET_ENTITY_COORDS(NET_TO_OBJ(serverBD.PickUpData[k].NetID)), 1.0, TRUE, TRUE, 60)
							DELETE_NET_ID(serverBD.PickUpData[k].NetID)
							NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - SCRIPT CLEANUP - DELETED PACKAGE ") NET_PRINT_INT(i) NET_NL()
						ENDIF
					ENDIF
				ENDIF
				//CONTROL_PACKAGE_DELIVERY(k)	//To make sure we have given ourselves the reward
			ENDREPEAT*/
			//REMOVE_DROP_OFF_BLIP()
			//REMOVE_AREA_RADIUS_BLIP()
			
			//Track and give the done all unique Gang Hideouts award
			IF GET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_FMATTGANGHQ) = FALSE
				NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - PROCESS_HIDEOUT_STAGE_REWARDS_AND_FEEDACK_CLIENT - currentMissionData.mdID.idVariation = ") NET_PRINT_INT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.mdID.idVariation) NET_NL()
				
				/*IF NOT IS_BIT_SET(g_sDoneNumMissionTypeAwardData[HIDEOUT_DONE_NUM_MISSION_TYPE_AWARD].TrackBitSet, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.mdID.idVariation)
					g_sDoneNumMissionTypeAwardData[HIDEOUT_DONE_NUM_MISSION_TYPE_AWARD].iCurrentNumber++
					SET_BIT(g_sDoneNumMissionTypeAwardData[HIDEOUT_DONE_NUM_MISSION_TYPE_AWARD].TrackBitSet, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.mdID.idVariation)
					NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - PROCESS_HIDEOUT_STAGE_REWARDS_AND_FEEDACK_CLIENT - g_sDoneNumMissionTypeAwardData - INCREMENTED") NET_NL()
					IF g_sDoneNumMissionTypeAwardData[HIDEOUT_DONE_NUM_MISSION_TYPE_AWARD].iCurrentNumber >= g_sDoneNumMissionTypeAwardData[HIDEOUT_DONE_NUM_MISSION_TYPE_AWARD].iTargetNumber
						SET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_FMATTGANGHQ, TRUE)
						NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - PROCESS_HIDEOUT_STAGE_REWARDS_AND_FEEDACK_CLIENT - MP_AWARD_FMATTGANGHQ - DONE") NET_NL()
					ENDIF
				ENDIF*/
				
				INT iBitSet = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.mdID.idVariation/32
				INT iBitToSet = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.mdID.idVariation%32
				NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - PROCESS_HIDEOUT_STAGE_REWARDS_AND_FEEDACK_CLIENT - iBitSet = ") NET_PRINT_INT(iBitSet) NET_PRINT(" iBitToSet = ") NET_PRINT_INT(iBitToSet)   NET_NL()
				IF NOT IS_BIT_SET(g_sDoneNumMissionTypeAwardData[HIDEOUT_DONE_NUM_MISSION_TYPE_AWARD].TrackBitSet[iBitSet], iBitToSet)
					g_sDoneNumMissionTypeAwardData[HIDEOUT_DONE_NUM_MISSION_TYPE_AWARD].iCurrentNumber++
					SET_BIT(g_sDoneNumMissionTypeAwardData[HIDEOUT_DONE_NUM_MISSION_TYPE_AWARD].TrackBitSet[iBitSet], iBitToSet)
					NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - PROCESS_HIDEOUT_STAGE_REWARDS_AND_FEEDACK_CLIENT - g_sDoneNumMissionTypeAwardData - INCREMENTED - iCurrentNumber = ") NET_PRINT_INT(g_sDoneNumMissionTypeAwardData[HIDEOUT_DONE_NUM_MISSION_TYPE_AWARD].iCurrentNumber) NET_NL()
					IF g_sDoneNumMissionTypeAwardData[HIDEOUT_DONE_NUM_MISSION_TYPE_AWARD].iCurrentNumber >= 5	//g_sDoneNumMissionTypeAwardData[HIDEOUT_DONE_NUM_MISSION_TYPE_AWARD].iTargetNumber
						
						IF NOT IS_MP_TATTOO_UNLOCKED(TATTOO_MP_FM_GANGHIDEOUT_CLEAR)
							SET_MP_TATTOO_UNLOCKED(TATTOO_MP_FM_GANGHIDEOUT_CLEAR, TRUE)
							NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - PROCESS_HIDEOUT_STAGE_REWARDS_AND_FEEDACK_CLIENT - TATTOO_MP_FM_GANGHIDEOUT_CLEAR - DONE") NET_NL()
						ENDIF
						
						SET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_FMATTGANGHQ, TRUE)
						NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - PROCESS_HIDEOUT_STAGE_REWARDS_AND_FEEDACK_CLIENT - MP_AWARD_FMATTGANGHQ - DONE") NET_NL()
					ENDIF
				ENDIF
				
			ELSE
				NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - PROCESS_HIDEOUT_STAGE_REWARDS_AND_FEEDACK_CLIENT - MP_AWARD_FMATTGANGHQ - ALREADY DONE") NET_NL()
			ENDIF
			
			//Check Smoke 'Em Out Award (Killed the most gang peds)
			IF serverBD.PlayerWithMostKills = PLAYER_ID()
			AND serverBD.iNumParticpants > 1
				IF GET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_FMMOSTKILLSGANGHIDE) = FALSE
					SET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_FMMOSTKILLSGANGHIDE, TRUE)
					NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - PROCESS_HIDEOUT_STAGE_REWARDS_AND_FEEDACK_CLIENT - MP_AWARD_FMMOSTKILLSGANGHIDE - DONE") NET_NL()
				ELSE
					NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - PROCESS_HIDEOUT_STAGE_REWARDS_AND_FEEDACK_CLIENT - MP_AWARD_FMMOSTKILLSGANGHIDE - ALREADY DONE") NET_NL()
				ENDIF
			ENDIF
			
			
			//Check Achievment - No Deaths +10 Kills 
			CPRINTLN(DEBUG_ACHIEVEMENT, "[ACHIEVEMENT]: ACH40 - Never Saw It Coming - Kills:", playerBD[PARTICIPANT_ID_TO_INT()].iKills, " of ", ACH40_KILLSNEEDED)
			
			IF playerBD[PARTICIPANT_ID_TO_INT()].iDeaths = 0
			AND playerBD[PARTICIPANT_ID_TO_INT()].iKills >= ACH40_KILLSNEEDED
				AWARD_ACHIEVEMENT(ACH40) // Never Saw It Coming 
				NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - PROCESS_HIDEOUT_STAGE_REWARDS_AND_FEEDACK_CLIENT - AWARD_ACHIEVEMENT(ACH40) - DONE") NET_NL()
			ENDIF
			
			/*Check Achievment - 20 Gang Attacks Cleared - THIS ACHIEVEMENT HAS BEEN REMOVED
			CPRINTLN(DEBUG_ACHIEVEMENT, "[ACHIEVEMENT]: ACH37 - Gang Banged - Cleared:", GET_MP_INT_CHARACTER_STAT(MP_STAT_GANGHIDWINS), " of ", ACH37_CLEARSNEEDED)
			IF GET_MP_INT_CHARACTER_STAT(MP_STAT_GANGHIDWINS) >= ACH37_CLEARSNEEDED
				AWARD_ACHIEVEMENT(ACH37) // Gang Banged
				NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - PROCESS_HIDEOUT_STAGE_REWARDS_AND_FEEDACK_CLIENT - AWARD_ACHIEVEMENT(ACH37) - DONE") NET_NL()
			ENDIF
			*/
			
			REQUEST_SYSTEM_ACTIVITY_TYPE_DONE_GANG_ATTACK()
			
			UNLOCK_VEHICLE_DOORS()
			
			REQUEST_SAVE(SSR_REASON_INSTANCED_CONTENT, STAT_SAVETYPE_END_MISSION)
			SET_BIT(iBoolsBitSet, biRewardDone)
			NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - PROCESS_HIDEOUT_STAGE_REWARDS_AND_FEEDACK_CLIENT - biRewardDone") NET_NL()
		ENDIF
		
		//Check when player can move on
		IF CONTROL_UPDATING_CHALLENGES()
		AND (IS_BIT_SET(iMusicEventBitSet, biME_End) OR SHOULD_HIDE_GANG_ATTACK_UI())
		AND IS_BIT_SET(iBoolsBitSet, biLaunchedAMGAPickups)
			IF NOT SHOULD_HIDE_GANG_ATTACK_UI()
				TRIGGER_CELEBRATION_PRE_LOAD(sCelebrationData, TRUE)
			ELSE
				NET_PRINT("  ---->  HIDEOUT - FM_EVENT_IS_PLAYER_ON_ANY_FM_EVENT = TRUE, skipping call to TRIGGER_CELEBRATION_PRE_LOAD") NET_NL()
			ENDIF
			SET_CLIENT_GAME_STATE(GAME_STATE_WAITING_TO_LEAVE)
		ENDIF
	ENDIF
	
	//Launch AM_GA_PICKUPS
	IF NOT IS_BIT_SET(iBoolsBitSet, biLaunchedAMGAPickups)
		IF IS_BIT_SET(serverBD.GAPickupData.iBitSet, iBS_GAP_Launch)
			IF HAS_GA_PICKUP_MAINTENANCED_LAUNCHED()
				SET_BIT(iBoolsBitSet, biLaunchedAMGAPickups)
				NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - PROCESS_HIDEOUT_STAGE_REWARDS_AND_FEEDACK_CLIENT - LAUNCHED AM_GA_PICKUPS") NET_NL()
			ENDIF
		ELSE
			SET_BIT(iBoolsBitSet, biLaunchedAMGAPickups)
			NET_PRINT_TIME() NET_PRINT("  ---->  HIDEOUT - PROCESS_HIDEOUT_STAGE_REWARDS_AND_FEEDACK_CLIENT - DON'T LAUNCH AM_GA_PICKUPS") NET_NL()
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Runs the race celebration screen showing the race end stats.
/// RETURNS:
///    TRUE once the sequence has finished.
FUNC BOOL HAS_CELEBRATION_SUMMARY_FINISHED()
	
	//Handle the screen flow.
	SWITCH sCelebrationData.eCurrentStage
		CASE CELEBRATION_STAGE_SETUP
			//Place a camera in front of the player if safe to do so.
			BOOL bPlacedCameraSuccessfully
			SET_CELEB_BLOCK_RESURRECT_RENDERPHASE_UNPAUSE(TRUE)
			IF PLACE_CAMERA_FOR_CELEBRATION_SCREEN(sCelebrationData, camEndScreen, bPlacedCameraSuccessfully)
				IF IS_NET_PLAYER_OK(PLAYER_ID())
					ANIMPOSTFX_STOP_ALL()
				ENDIF
			
				PLAY_CELEB_WIN_POST_FX()
				PLAY_SOUND_FRONTEND(-1, "MP_WAVE_COMPLETE", "HUD_FRONTEND_DEFAULT_SOUNDSET")
				REQUEST_CELEBRATION_SCREEN(sCelebrationData)
				SET_CELEBRATION_SCREEN_AS_ACTIVE(TRUE)
				KILL_UI_FOR_CELEBRATION_SCREEN()
				HIDE_UI_FOR_CELEBRATION_SCREEN_THIS_FRAME()
				IF NOT SHOULD_HIDE_GANG_ATTACK_UI()
					NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				ENDIF
				IF bPlacedCameraSuccessfully
					RENDER_SCRIPT_CAMS(TRUE, FALSE)
				ELSE
					//1626331 - If the celebration cam didn't successfully create then turn the ped to face the game cam.
					IF IS_NET_PLAYER_OK(PLAYER_ID())
					AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						TASK_TURN_PED_TO_FACE_COORD(PLAYER_PED_ID(), GET_FINAL_RENDERED_CAM_COORD())
					ENDIF
				ENDIF
				
				sCelebrationData.eCurrentStage = CELEBRATION_STAGE_TRANSITIONING
			ENDIF
		BREAK
		
		CASE CELEBRATION_STAGE_TRANSITIONING
			
			HIDE_UI_FOR_CELEBRATION_SCREEN_THIS_FRAME()
			
			//Set up all the assets and begin when ready.
			REQUEST_CELEBRATION_SCREEN(sCelebrationData)
	
			IF HAS_CELEBRATION_SCREEN_LOADED(sCelebrationData)
			AND IS_SCREEN_FADED_IN()
			AND NOT ANIMPOSTFX_IS_RUNNING("DeathFailMPIn")
				
				STRING strScreenName, strBackgroundColour
				INT iCurrentRP, iCurrentLvl, iNextLvl, iRPToReachCurrentLvl, iRPToReachNextLvl, iXPToShow
			
				//Retrieve all the required stats for the race end screen.
				strScreenName = "SUMMARY"
				strBackgroundColour = "HUD_COLOUR_BLACK"
				iCurrentRP = GET_PLAYER_FM_XP(NETWORK_GET_PLAYER_INDEX(PARTICIPANT_ID())) 
				iXPToShow = GIVE_LOCAL_PLAYER_XP(eXPTYPE_CO_OP_AMBIENTFM_ON_SCRIPT, "XPT_MEDIUMT", XPTYPE_COMPLETE, XPCATEGORY_COMPLETE_HIDEOUT_CLEARED, ROUND(END_XP_REWARD*g_sMPTunables.fxp_tunable_Gang_Attack* g_sMPTunables.xpMultiplier), 1)
				iCurrentLvl = GET_FM_RANK_FROM_XP_VALUE(iCurrentRP) //GET_PLAYER_GLOBAL_RANK(NETWORK_GET_PLAYER_INDEX(PARTICIPANT_ID())) //We don't want the current level, but their level before the XP was given.
				iNextLvl = iCurrentLvl + 1
				iRPToReachCurrentLvl = GET_XP_NEEDED_FOR_FM_RANK(iCurrentLvl)
				iRPToReachNextLvl = GET_XP_NEEDED_FOR_FM_RANK(iNextLvl)
				sCelebrationData.iEstimatedScreenDuration = CELEBRATION_SCREEN_STAT_WIPE_TIME * 2
				
				//Build the screen
				CREATE_CELEBRATION_SCREEN(sCelebrationData, strScreenName, strBackgroundColour)
				
				ADD_MISSION_RESULT_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName, CELEBRATION_JOB_TYPE_GANG_ATTACK, TRUE, "", "", "")
				sCelebrationData.iEstimatedScreenDuration += CELEBRATION_SCREEN_STAT_DISPLAY_TIME
				
				ADD_TOTAL_KILLS_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName, EndScreenData.iTrackedKills)
				sCelebrationData.iEstimatedScreenDuration += CELEBRATION_SCREEN_STAT_DISPLAY_TIME

				IF EndScreenData.iHeadshots > 0
					ADD_HEADSHOTS_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName, EndScreenData.iHeadshots)
					sCelebrationData.iEstimatedScreenDuration += CELEBRATION_SCREEN_STAT_DISPLAY_TIME
				ENDIF
				
				IF EndScreenData.iPointBlankKills > 0
					ADD_POINT_BLANKS_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName, EndScreenData.iPointBlankKills)
					sCelebrationData.iEstimatedScreenDuration += CELEBRATION_SCREEN_STAT_DISPLAY_TIME
				ENDIF

				ADD_REP_POINTS_AND_RANK_BAR_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName, iXPTOShow, iCurrentRP, 
											   	   iRPToReachCurrentLvl, iRPToReachNextLvl, iCurrentLvl, iNextLvl)
				sCelebrationData.iEstimatedScreenDuration += CELEBRATION_SCREEN_STAT_DISPLAY_TIME
				
				//If the player levelled up we need to add a bit more to the duration.
				IF iCurrentRP + iXPToShow >= iRPToReachNextLvl
					sCelebrationData.iEstimatedScreenDuration += ((CELEBRATION_SCREEN_STAT_DISPLAY_TIME / 2) + (CELEBRATION_SCREEN_STAT_DISPLAY_TIME / 4))
				ENDIF
				
				ADD_BACKGROUND_TO_CELEBRATION_SCREEN(sCelebrationData, strScreenName)
				SHOW_CELEBRATION_SCREEN(sCelebrationData, strScreenName)
				
				RESET_NET_TIMER(sCelebrationData.sCelebrationTimer)
				START_NET_TIMER(sCelebrationData.sCelebrationTimer)
				
				//Audio scenes: see B*1642903 for implementation notes.
				IF NOT IS_AUDIO_SCENE_ACTIVE("MP_CELEB_SCREEN_SCENE")
					START_AUDIO_SCENE("MP_CELEB_SCREEN_SCENE")
				ENDIF
				
				CDEBUG1LN(DEBUG_MISSION, "[NETCELEBRATION] Adding stats ---------------------------")
				CDEBUG1LN(DEBUG_MISSION, "iCurrentRP ", iCurrentRP)
				CDEBUG1LN(DEBUG_MISSION, "iXPToShow ", iXPToShow)
				CDEBUG1LN(DEBUG_MISSION, "iCurrentLvl ", iCurrentLvl)
				CDEBUG1LN(DEBUG_MISSION, "iNextLvl ", iNextLvl)
				CDEBUG1LN(DEBUG_MISSION, "iRPToReachCurrentLvl ", iRPToReachCurrentLvl)
				CDEBUG1LN(DEBUG_MISSION, "iRPToReachNextLvl ", iRPToReachNextLvl)
				CDEBUG1LN(DEBUG_MISSION, "EndScreenData.iTrackedKills ", EndScreenData.iTrackedKills)
				CDEBUG1LN(DEBUG_MISSION, "EndScreenData.iHeadshots ", EndScreenData.iHeadshots)
				CDEBUG1LN(DEBUG_MISSION, "EndScreenData.iPointBlankKills ", EndScreenData.iPointBlankKills)
				CDEBUG1LN(DEBUG_MISSION, "---------------------------------------------------------")
				RESET_CELEBRATION_PRE_LOAD(sCelebrationData)
				sCelebrationData.eCurrentStage = CELEBRATION_STAGE_PLAYING
			ELSE
			
				IF ANIMPOSTFX_IS_RUNNING("DeathFailMPIn")
					IF NOT HAS_NET_TIMER_STARTED(sCelebrationData.timerDeathPostFxDelay)
						START_NET_TIMER(sCelebrationData.timerDeathPostFxDelay)
						PRINTLN("[NETCELEBRATION] HAS_CELEBRATION_WINNER_FINISHED - DeathFailMPIn is running, started timer timerDeathPostFxDelay.")
					ELSE
						IF HAS_NET_TIMER_EXPIRED(sCelebrationData.timerDeathPostFxDelay, 1000)
							PRINTLN("[NETCELEBRATION] HAS_CELEBRATION_WINNER_FINISHED - DeathFailMPIn is running, timer timerDeathPostFxDelay >= 1000. Calling CLEAR_KILL_STRIP_DEATH_EFFECTS.")
							CLEAR_KILL_STRIP_DEATH_EFFECTS()
						ENDIF
					ENDIF	
				ENDIF
				
			ENDIF
		BREAK
		
		CASE CELEBRATION_STAGE_PLAYING
			
			HIDE_UI_FOR_CELEBRATION_SCREEN_THIS_FRAME()
			
			DRAW_CELEBRATION_SCREEN_THIS_FRAME(sCelebrationData, FALSE)
			
			//Detach the cam on the player if they're stationary to stop the weird swings if the player turns slightly.
			IF IS_NET_PLAYER_OK(PLAYER_ID())
			AND DOES_CAM_EXIST(camEndScreen)
				IF GET_ENTITY_SPEED(PLAYER_PED_ID()) < 0.1
					DETACH_CAM(camEndScreen)
					STOP_CAM_POINTING(camEndScreen)
				ENDIF
			ENDIF
			
			//Once the timer finishes then progress: this depends on how many elements were added to the screen.
//			IF HAS_NET_TIMER_EXPIRED(sCelebrationData.sCelebrationTimer, sCelebrationData.iEstimatedScreenDuration - (CELEBRATION_SCREEN_STAT_WIPE_TIME / 2))
			IF HAS_CELEBRATION_SHARD_FINISHED_FALL_AWAY()	
				ANIMPOSTFX_STOP_ALL()
				ANIMPOSTFX_PLAY("MP_Celeb_Win_Out", 0, FALSE)
				PLAY_SOUND_FRONTEND(-1, "Hit_1", "LONG_PLAYER_SWITCH_SOUNDS")
				START_CELEBRATION_CAMERA_ZOOM_TRANSITION(camEndScreen)
			
				RESET_NET_TIMER(sCelebrationData.sCelebrationTimer)
				START_NET_TIMER(sCelebrationData.sCelebrationTimer)
			
				sCelebrationData.eCurrentStage = CELEBRATION_STAGE_END_TRANSITION
			ENDIF
		BREAK
		
		CASE CELEBRATION_STAGE_END_TRANSITION
			DRAW_CELEBRATION_SCREEN_THIS_FRAME(sCelebrationData, FALSE)

			IF HAS_NET_TIMER_EXPIRED(sCelebrationData.sCelebrationTimer, 1000)
				//ANIMPOSTFX_PLAY("MinigameEndNeutral", 0, FALSE)
				//PLAY_SOUND_FRONTEND(-1, "Hit_1", "LONG_PLAYER_SWITCH_SOUNDS")
				CLEAR_ALL_BIG_MESSAGES()
				STOP_CELEBRATION_SCREEN_ANIM(sCelebrationData, "SUMMARY")
				CLEANUP_CELEBRATION_SCREEN(sCelebrationData)
				SET_CELEBRATION_SCREEN_AS_ACTIVE(FALSE)
				
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				
				IF DOES_CAM_EXIST(camEndScreen)
					DESTROY_CAM(camEndScreen, TRUE)
					RENDER_SCRIPT_CAMS(FALSE, FALSE)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
				ENDIF
			
				sCelebrationData.eCurrentStage = CELEBRATION_STAGE_FINISHED
				
				RETURN TRUE
			ELSE
				HIDE_UI_FOR_CELEBRATION_SCREEN_THIS_FRAME()
			ENDIF
		BREAK
		
		CASE CELEBRATION_STAGE_FINISHED
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	//DISPLAY_TEXT_WITH_NUMBER(0.3, 0.5, "FM_NXT_VCNT", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(sCelebrationData.sCelebrationTimer))
	//DISPLAY_TEXT_WITH_NUMBER(0.3, 0.6, "FM_NXT_VCNT", sCelebrationData.iEstimatedScreenDuration)
	
	RETURN FALSE
ENDFUNC


PROC PROCESS_CLIENT()
	
	SWITCH GET_CLIENT_HIDEOUT_STAGE(PARTICIPANT_ID())
		CASE HIDEOUT_STAGE_JIP_IN
			PROCESS_HIDEOUT_STAGE_JIP_IN_CLIENT()
		BREAK
		
		CASE HIDEOUT_STAGE_SETUP
			PROCESS_HIDEOUT_STAGE_SETUP_CLIENT()
		BREAK
		
		CASE HIDEOUT_STAGE_WARNING
			PROCESS_HIDEOUT_STAGE_WARNING_CLIENT()
		BREAK
		
		CASE HIDEOUT_STAGE_PRIMARY
			PROCESS_HIDEOUT_STAGE_PRIMARY_CLIENT()
		BREAK
		
		CASE HIDEOUT_STAGE_SECONDARY
			PROCESS_HIDEOUT_STAGE_SECONDARY_CLIENT()
		BREAK
		
		CASE HIDEOUT_STAGE_REWARDS_AND_FEEDACK
			PROCESS_HIDEOUT_STAGE_REWARDS_AND_FEEDACK_CLIENT()
		BREAK
	ENDSWITCH
	
	MAINTAIN_TIMES_PLAYED_STAT()
	CONTROL_MISSION_JOINABLE_CHECKS()
	
	SUPPRESS_CRIME_THIS_FRAME(PLAYER_ID(), CRIME_FIREARM_DISCHARGE)
	SUPPRESS_CRIME_THIS_FRAME(PLAYER_ID(), CRIME_SHOOT_VEHICLE)
	SUPPRESS_CRIME_THIS_FRAME(PLAYER_ID(), CRIME_POSSESSION_GUN)
	SUPPRESS_CRIME_THIS_FRAME(PLAYER_ID(), CRIME_CAR_SET_ON_FIRE)
	SUPPRESS_CRIME_THIS_FRAME(PLAYER_ID(), CRIME_RUNOVER_PED)
	SUPPRESS_CRIME_THIS_FRAME(PLAYER_ID(), CRIME_PED_SET_ON_FIRE)
	SUPPRESS_CRIME_THIS_FRAME(PLAYER_ID(), CRIME_CAUSE_EXPLOSION)
	SUPPRESS_CRIME_THIS_FRAME(PLAYER_ID(), CRIME_STAB_PED)
	SUPPRESS_CRIME_THIS_FRAME(PLAYER_ID(), CRIME_DESTROY_VEHICLE)
	SUPPRESS_CRIME_THIS_FRAME(PLAYER_ID(), CRIME_DAMAGE_TO_PROPERTY)
	SUPPRESS_CRIME_THIS_FRAME(PLAYER_ID(), CRIME_MOLOTOV)
	SUPPRESS_CRIME_THIS_FRAME(PLAYER_ID(), CRIME_SHOOT_NONLETHAL_PED)
	SUPPRESS_CRIME_THIS_FRAME(PLAYER_ID(), CRIME_THROW_GRENADE)
	SUPPRESS_CRIME_THIS_FRAME(PLAYER_ID(), CRIME_TERRORIST_ACTIVITY)
	SUPPRESS_CRIME_THIS_FRAME(PLAYER_ID(), CRIME_VEHICLE_EXPLOSION)
	SUPPRESS_CRIME_THIS_FRAME(PLAYER_ID(), CRIME_KILL_PED)
	SUPPRESS_CRIME_THIS_FRAME(PLAYER_ID(), CRIME_SUICIDE)
	SUPPRESS_CRIME_THIS_FRAME(PLAYER_ID(), CRIME_DISTURBANCE)
	SUPPRESS_CRIME_THIS_FRAME(PLAYER_ID(), CRIME_STEALTH_KILL_PED)
	SUPPRESS_CRIME_THIS_FRAME(PLAYER_ID(), CRIME_JACK_DEAD_PED)
	SUPPRESS_CRIME_THIS_FRAME(PLAYER_ID(), CRIME_SHOOT_PED_SUPPRESSED)
ENDPROC



// *********************************** \\
//				SERVER LOGIC		   \\
// *********************************** \\

PROC PROCESS_HIDEOUT_STAGE_JIP_IN_SERVER()
	SET_SERVER_HIDEOUT_STAGE(HIDEOUT_STAGE_SETUP)
ENDPROC

PROC PROCESS_HIDEOUT_STAGE_SETUP_SERVER()
	// if setup data
		// if setup entities
			SET_SERVER_HIDEOUT_STAGE(HIDEOUT_STAGE_WARNING)
		// endif
	// endif
ENDPROC

PROC PROCESS_HIDEOUT_STAGE_WARNING_SERVER()
	
 	IF IS_BIT_SET(serverBD.iServerBitSet, biS_MoveToFightStage)
		START_NET_TIMER(serverBD.iStartTime)
		SET_SERVER_HIDEOUT_STAGE(HIDEOUT_STAGE_PRIMARY)
	ENDIF
	
ENDPROC

PROC PROCESS_HIDEOUT_STAGE_PRIMARY_SERVER()
	
	SWITCH serverBD.eHideoutVariation
 		CASE HIDEOUT_KILL_PEDS
			IF serverBD.iTotalKills >= serverBD.iKillGoal
				serverBD.iTimeTaken = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.iStartTime)
				SET_SERVER_HIDEOUT_STAGE(HIDEOUT_STAGE_SECONDARY)
			ENDIF
		BREAK
		
		/*CASE HIDEOUT_TERRITORY_CONTROL
			IF PROCESS_TERRITORY_CONTROL_SERVER()
				SET_SERVER_HIDEOUT_STAGE(HIDEOUT_STAGE_SECONDARY)
			ENDIF
		BREAK*/
		
		/*CASE HIDEOUT_PACKAGE_STEAL
			IF IS_BIT_SET(serverBD.iServerBitSet, biS_NoPackagesExist)
				serverBD.iTimeTaken = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.iStartTime)
				GET_PLAYER_WITH_MOST_KILLS()
				SET_SERVER_HIDEOUT_STAGE(HIDEOUT_STAGE_REWARDS_AND_FEEDACK)
			ENDIF
		BREAK*/
	ENDSWITCH
	
ENDPROC

PROC PROCESS_HIDEOUT_STAGE_SECONDARY_SERVER()
	
	SWITCH serverBD.eHideoutVariation
		CASE HIDEOUT_KILL_PEDS
			//IF IS_BIT_SET(serverBD.iServerBitSet, biS_SafeUnlocked)
				serverBD.iTimeTaken = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.iStartTime)
				GET_PLAYER_WITH_MOST_KILLS()
				SET_SERVER_HIDEOUT_STAGE(HIDEOUT_STAGE_REWARDS_AND_FEEDACK)
				SET_GA_PICKUP_DATA()
			//ENDIF
		BREAK
		
		/*CASE HIDEOUT_TERRITORY_CONTROL
			IF IS_BIT_SET(serverBD.iServerBitSet, biS_AllGangPedsDead)
				serverBD.iTimeTaken = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.iStartTime)
				GET_PLAYER_WITH_MOST_KILLS()
				SET_SERVER_HIDEOUT_STAGE(HIDEOUT_STAGE_REWARDS_AND_FEEDACK)
			ENDIF
		BREAK*/
		
		/*CASE HIDEOUT_PACKAGE_STEAL
			IF IS_BIT_SET(serverBD.iServerBitSet, biS_NoPackagesExist)
				serverBD.iTimeTaken = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.iStartTime)
				GET_PLAYER_WITH_MOST_KILLS()
				SET_SERVER_HIDEOUT_STAGE(HIDEOUT_STAGE_REWARDS_AND_FEEDACK)
			ENDIF
		BREAK*/
	ENDSWITCH
	
ENDPROC

//PROC PROCESS_HIDEOUT_STAGE_REWARDS_AND_FEEDACK_SERVER()
//	//Do Nothing
//ENDPROC

//PURPOSE: Makes sure the server has blocked the scenario blocking area
PROC CONTROL_SCENARIO_BLOCKING_AREA()
	IF NOT IS_BIT_SET(serverBD.iServerBitSet, biS_BlockingAreaDone)
		VECTOR vArea = <<serverBD.fHideoutRadius*1.25, serverBD.fHideoutRadius*1.25, serverBD.fHideoutRadius*1.25>>
		IF NOT DOES_SCENARIO_BLOCKING_AREA_EXISTS(g_FMMC_STRUCT_ENTITIES.sPlacedPed[0].vPos-vArea, g_FMMC_STRUCT_ENTITIES.sPlacedPed[0].vPos+vArea)
			ScenarioBlockingArea = ADD_SCENARIO_BLOCKING_AREA(g_FMMC_STRUCT_ENTITIES.sPlacedPed[0].vPos-vArea, g_FMMC_STRUCT_ENTITIES.sPlacedPed[0].vPos+vArea, TRUE, TRUE)
		ELSE
			NET_PRINT("    ----->     HIDEOUT - ADD_SCENARIO_BLOCKING_AREA - NETWORKED - AT - FAILED - already exists ") NET_PRINT_VECTOR(g_FMMC_STRUCT_ENTITIES.sPlacedPed[0].vPos) NET_NL()
		ENDIF
		SET_BIT(serverBD.iServerBitSet, biS_BlockingAreaDone)
		NET_PRINT("    ----->     HIDEOUT - ADD_SCENARIO_BLOCKING_AREA - NETWORKED - AT ") NET_PRINT_VECTOR(g_FMMC_STRUCT_ENTITIES.sPlacedPed[0].vPos) NET_NL()
	ENDIF
ENDPROC

PROC PROCESS_SERVER()
	
	SWITCH GET_SERVER_HIDEOUT_STAGE()
		CASE HIDEOUT_STAGE_JIP_IN
			PROCESS_HIDEOUT_STAGE_JIP_IN_SERVER()
		BREAK
		
		CASE HIDEOUT_STAGE_SETUP
			PROCESS_HIDEOUT_STAGE_SETUP_SERVER()
		BREAK
		
		CASE HIDEOUT_STAGE_WARNING
			PROCESS_HIDEOUT_STAGE_WARNING_SERVER()
		BREAK
		
		CASE HIDEOUT_STAGE_PRIMARY
			PROCESS_HIDEOUT_STAGE_PRIMARY_SERVER()
		BREAK
		
		CASE HIDEOUT_STAGE_SECONDARY
			PROCESS_HIDEOUT_STAGE_SECONDARY_SERVER()
		BREAK
		
		CASE HIDEOUT_STAGE_REWARDS_AND_FEEDACK
			SET_GA_PICKUP_DATA()
			//DO NOTHING		//PROCESS_HIDEOUT_STAGE_REWARDS_AND_FEEDACK_SERVER()
		BREAK
	ENDSWITCH
	
	PROCESS_GANG_PEDS_BRAINS()
	CONTROL_SCENARIO_BLOCKING_AREA()
	
	CONTROL_MISSION_JOINABLE_CHECKS()
ENDPROC


// *********************************** \\
//				MAIN LOOP			   \\
// *********************************** \\

/// PURPOSE:
///    Main loop.
/// PARAMS:
///    HideoutMissionData - mission data.
SCRIPT(MP_MISSION_DATA HideoutMissionData)
	
	// Carry out all the initial game starting duties. 
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF NOT PROCESS_PRE_GAME(HideoutMissionData)
			NET_PRINT("    ----->     HIDEOUT - FAILED TO RECEIVE AN INITIAL NETWORK BROADCAST ") NET_NL()
			SCRIPT_CLEANUP()
		ENDIF
	ENDIF
	
	// KGM 11/9/14 [BUG 1984713]: If the server is already set as 'not joinable' then instantly quit
	// Not entirely sure of the cause of this bug, but this check should prevent new players joining after it is not joinable
	IF (IS_BIT_SET(serverBD.iServerBitSet, biS_MissionNotJoinable))
		NET_PRINT("    ----->     HIDEOUT - SCRIPT LAUNCHED BUT SERVER IS ALREADY SET TO NOT JOINABLE: IMMEDIATE CLEANUP") NET_NL()
		SCRIPT_CLEANUP()
	ENDIF
	
	// Create debug widgets.
	#IF IS_DEBUG_BUILD
		CREATE_WIDGETS()
		#IF SCRIPT_PROFILER_ACTIVE
			CREATE_SCRIPT_PROFILER_WIDGET()
		#ENDIF
	#ENDIF
	
	// Main while loop.
	WHILE TRUE
		
		MP_LOOP_WAIT_ZERO()
		
		// Keep TOD constant for duration of script.
		//CLIENT_OVERRIDE_CLOCK_TIME(g_FMMC_STRUCT.iTimeOfDay)
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		SCRIPT_PROFILER_START_OF_FRAME()
		#ENDIF
		#ENDIF	
		
		// If we have a match end event, bail.
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			NET_PRINT("    ----->     HIDEOUT - SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE ") NET_NL()
			SCRIPT_CLEANUP()
		ENDIF
		IF SHOULD_PLAYER_LEAVE_MP_MISSION()
			NET_PRINT("    ----->     HIDEOUT - SHOULD_PLAYER_LEAVE_MP_MISSION ") NET_NL()
			SCRIPT_CLEANUP()
		ENDIF
		
		IF SHOULD_GB_MISSION_CLEANUP_ACTIVE_GANG_ATTACK()
			NET_PRINT("    ----->     HIDEOUT - cleanup due to large gang boss mission ") NET_NL()
			SCRIPT_CLEANUP()
		ENDIF
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("SCRIPT_CLEANUP")
		#ENDIF
		#ENDIF
		
		SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
		MAINTAIN_CELEBRATION_PRE_LOAD(sCelebrationData)
		
		IF NETWORK_IS_GAME_IN_PROGRESS()
		
			// -----------------------------------
			// Process client game logic	
			//REINIT_NET_TIMER(tdNetTimer)
			
			SWITCH GET_CLIENT_GAME_STATE(PARTICIPANT_ID_TO_INT())
							
				// Wait until the server gives the all go before moving on.
				CASE GAME_STATE_INI
					
					//IF REQUEST_SCALE_FORM("RAMPAGE", sScaleFormData.sFormRampage)
					//IF HAS_CLIENT_CREATED_HEALTH_PICKUPS()
					IF CREATE_LOCAL_PROPS()
						IF GET_SERVER_GAME_STATE() > GAME_STATE_INI
							IF IS_NET_PLAYER_OK(PLAYER_ID())
							AND NOT  IS_PLAYER_AN_ANIMAL(PLAYER_ID())
								//Make sure Safe Cracking anims and sequences are done
								IF serverBD.eHideoutVariation = HIDEOUT_KILL_PEDS
								//OR serverBD.eHideoutVariation = HIDEOUT_PACKAGE_STEAL
									//IF CREATE_SAFE_CRACK_SEQUENCES(SafeCrackData)
										SET_CLIENT_GAME_STATE(GAME_STATE_RUNNING)
										NET_PRINT("    ----->     HIDEOUT - Moving CLIENT to GAME_STATE_RUNNING - AA") NET_NL()
									//ENDIF
								/*ELSE
									SET_CLIENT_GAME_STATE(GAME_STATE_RUNNING)
									NET_PRINT("    ----->     HIDEOUT - Moving CLIENT to GAME_STATE_RUNNING - A") NET_NL()*/
								ENDIF
							ENDIF
						ENDIF
					ELSE
						NET_PRINT("    ----->     HIDEOUT - WAITING FOR PROP CREATION") NET_NL()
					ENDIF
					
					//SETUP_END_SCREEN()	//TEMP TEST
				BREAK
				
				// Main gameplay state.
				CASE GAME_STATE_RUNNING		
					
					//DRAW_END_SCREEN()		//TEMP TEST
					
					MAINTAIN_RESERVED_ENTITIES()
					
					// Participant loops.
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE 
						OPEN_SCRIPT_PROFILE_MARKER_GROUP("GAME_STATE_RUNNING - hideout")
					#ENDIF
					#ENDIF
					//MAINTAIN_PARTICPANT_LOOP()
					//#IF IS_DEBUG_BUILD
					//#IF SCRIPT_PROFILER_ACTIVE 
					//	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_PARTICPANT_LOOP")
					//#ENDIF
					//#ENDIF
		
					MAINTAIN_STAGGERED_PARTICPANT_LOOP()
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE 
						ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_STAGGERED_PARTICPANT_LOOP")
					#ENDIF
					#ENDIF
					
					// Process client logic.
					PROCESS_CLIENT()
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE 
						ADD_SCRIPT_PROFILE_MARKER("PROCESS_CLIENT")
					#ENDIF
					#ENDIF
					
					CONTROL_MUSIC_EVENTS()
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE 
						ADD_SCRIPT_PROFILE_MARKER("CONTROL_MUSIC_EVENTS")
					#ENDIF
					#ENDIF
					
					CONTROL_PLAY_STATS()
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE 
						ADD_SCRIPT_PROFILE_MARKER("CONTROL_PLAY_STATS")
					#ENDIF
					#ENDIF
					
					MAINTAIN_GANG_ANGRY_STAT()
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE 
						ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_GANG_ANGRY_STAT")
					#ENDIF
					#ENDIF
					//DRAW_KILLSTREAK_DISPLAY(iKillStreakMultiplier)
					
					// If the server says the game's over, then I should think that too
					IF GET_SERVER_GAME_STATE()= GAME_STATE_END
						SET_CLIENT_GAME_STATE(GAME_STATE_END)
						NET_PRINT("    ----->     HIDEOUT - Moving CLIENT to GAME_STATE_END - A") NET_NL()
					ELIF GET_CLIENT_HIDEOUT_STAGE(PARTICIPANT_ID()) = HIDEOUT_STAGE_WARNING
					OR (GET_CLIENT_HIDEOUT_STAGE(PARTICIPANT_ID()) = HIDEOUT_STAGE_PRIMARY)	// AND NOT IS_BIT_SET(serverBD.iServerBitSet, biS_SafeUnlocked))
					OR (GET_CLIENT_HIDEOUT_STAGE(PARTICIPANT_ID()) = HIDEOUT_STAGE_SECONDARY)// AND serverBD.eHideoutVariation != HIDEOUT_PACKAGE_STEAL)
					
						//NET_PRINT("    ----->     HIDEOUT - HAS_PED_LEFT_AREA - CHECK A - ") NET_PRINT_BOOL(GET_CLIENT_HIDEOUT_STAGE(PARTICIPANT_ID()) = HIDEOUT_STAGE_WARNING) NET_NL()
						//NET_PRINT("    ----->     HIDEOUT - HAS_PED_LEFT_AREA - CHECK B - ") NET_PRINT_BOOL(GET_CLIENT_HIDEOUT_STAGE(PARTICIPANT_ID()) = HIDEOUT_STAGE_PRIMARY) NET_NL()
						//NET_PRINT("    ----->     HIDEOUT - HAS_PED_LEFT_AREA - CHECK C - ") NET_PRINT_BOOL((GET_CLIENT_HIDEOUT_STAGE(PARTICIPANT_ID()) = HIDEOUT_STAGE_SECONDARY AND serverBD.eHideoutVariation != HIDEOUT_PACKAGE_STEAL)) NET_NL()
						//NET_PRINT("    ----->     HIDEOUT - HAS_PED_LEFT_AREA - GET_CLIENT_HIDEOUT_STAGE = ") NET_PRINT(GET_HIDEOUT_STAGE_NAME(GET_CLIENT_HIDEOUT_STAGE(PARTICIPANT_ID()))) NET_NL()
						
						IF HAS_PED_LEFT_AREA(PLAYER_PED_ID(), TRUE)
						OR IS_PLAYER_AN_ANIMAL(PLAYER_ID())
							IF NOT SHOULD_HIDE_GANG_ATTACK_UI()
							AND NOT IS_BIT_SET(iMusicEventBitSet, biME_End)
								TRIGGER_MUSIC_EVENT("GA_LEAVE_AREA")
								SET_BIT(iMusicEventBitSet, biME_End)
								NET_PRINT("    ----->     HIDEOUT - HAS_PED_LEFT_AREA - MUSIC EVENT - GA_LEAVE_AREA") NET_NL()
							ENDIF
							SET_CLIENT_GAME_STATE(GAME_STATE_END)
							NET_PRINT("    ----->     HIDEOUT - Moving CLIENT to GAME_STATE_END - B - HAS_PED_LEFT_AREA - EARLY") NET_NL()
						ENDIF
					ENDIF
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE 
						ADD_SCRIPT_PROFILE_MARKER("end checks")
					#ENDIF
					#ENDIF
					
					#IF IS_DEBUG_BUILD
						DEBUG_SKIPS()
					#ENDIF
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE 
						ADD_SCRIPT_PROFILE_MARKER("DEBUG_SKIPS")
					#ENDIF
					#ENDIF
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE 
						CLOSE_SCRIPT_PROFILE_MARKER_GROUP()
					#ENDIF
					#ENDIF
					
				BREAK	
				
				CASE GAME_STATE_WAITING_TO_LEAVE 
					
					//MAINTAIN_PARTICPANT_LOOP()
					
					IF NOT SHOULD_HIDE_GANG_ATTACK_UI()
						TRIGGER_CELEBRATION_PRE_LOAD(sCelebrationData, TRUE)
					ELSE
						NET_PRINT("  ---->  HIDEOUT - FM_EVENT_IS_PLAYER_ON_ANY_FM_EVENT = TRUE, skipping call to TRIGGER_CELEBRATION_PRE_LOAD") NET_NL()
					ENDIF
					
					IF NOT IS_BIT_SET(iBoolsBitSet, biBigMessageDone)
						IF SHOULD_HIDE_GANG_ATTACK_UI()
							SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_AM_COMPLETE_GANG_ATTACK)
							SET_BIT(iBoolsBitSet, biBigMessageDone)
							IF sCelebrationData.eCurrentStage > CELEBRATION_STAGE_SETUP
								NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
							ENDIF
							NET_PRINT("    ----->     HIDEOUT - FM_EVENT_IS_PLAYER_ON_ANY_FM_EVENT = TRUE") NET_NL()
							NET_PRINT("    ----->     HIDEOUT - biBigMessageDone SET") NET_NL()
						ELSE
							IF HAS_CELEBRATION_SUMMARY_FINISHED()
								//Cleanup audio scenes: see B*1642903 for implementation notes
								IF IS_AUDIO_SCENE_ACTIVE("MP_CELEB_SCREEN_SCENE")
									STOP_AUDIO_SCENE("MP_CELEB_SCREEN_SCENE")
								ENDIF
								
								SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_AM_COMPLETE_GANG_ATTACK)
								
								SET_BIT(iBoolsBitSet, biBigMessageDone)
								NET_PRINT("    ----->     HIDEOUT - biBigMessageDone SET") NET_NL()
							ENDIF
						ENDIF
					ELSE
						SET_MISSION_FINISHED(serverBD.TerminationTimer)
						
						IF IS_MISSION_READY_TO_CLEANUP(serverBD.TerminationTimer)
							//IF GET_SERVER_GAME_STATE() = GAME_STATE_END
								SET_CLIENT_GAME_STATE(GAME_STATE_END)
								NET_PRINT("    ----->     HIDEOUT - Moving CLIENT to GAME_STATE_END - C") NET_NL()
							//ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE GAME_STATE_LEAVE
					SET_CLIENT_GAME_STATE(GAME_STATE_END)
					NET_PRINT("    ----->     HIDEOUT - Moving CLIENT to GAME_STATE_END - D") NET_NL()
				FALLTHRU

				CASE GAME_STATE_END
					SCRIPT_CLEANUP()
				BREAK
				
				DEFAULT 
					#IF IS_DEBUG_BUILD
						SCRIPT_ASSERT("HIDEOUT: Problem in SWITCH GET_CLIENT_GAME_STATE(PARTICIPANT_ID_TO_INT())") 
					#ENDIF
				BREAK
			ENDSWITCH
		ENDIF
		
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("process main game state")
		#ENDIF
		#ENDIF
		
		// -----------------------------------
		// Process server game logic	
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		
			#IF IS_DEBUG_BUILD
				NET_SCRIPT_HOST_ONLY_COMMAND_ASSERT("HIDEOUT: SERVER stuff being called by machine that is not host! Host only command!")
			#ENDIF

			SWITCH GET_SERVER_GAME_STATE()
				
				CASE GAME_STATE_INI
				
					CREATE_TRACKED_POINTS()
				
					IF LOAD_AND_CREATE_ALL_ENTITIES(serverBD.sFMMC_SBD)
					//AND HAS_SERVER_CREATED_MISSION_PICKUPS()
						
						/*serverBD.iKillGoal = g_FMMC_STRUCT_ENTITIES.sPlacedPed[0].iRespawn
						NET_PRINT("    ----->     HIDEOUT - 2 - serverBD.iKillGoal = ") NET_PRINT_INT(serverBD.iKillGoal) NET_NL()*/
						
						CLEANUP_TRACKED_POINTS()
						
						SET_SERVER_GAME_STATE(GAME_STATE_RUNNING)
						NET_PRINT("    ----->     HIDEOUT - Moving SERVER to GAME_STATE_RUNNING - A") NET_NL()
					ENDIF
				BREAK
				
				CASE GAME_STATE_RUNNING		
					
					PROCESS_SERVER()
					
					MAINTAIN_MAX_PARTICIPANT_CHECKS_SERVER()
					MAINTAIN_DESTRUCTIBLE_CRATES()
					
					// Look for server end conditions
					/*IF HAVE_END_GAME_CONDITIONS_BEEN_MET()
						SET_SERVER_GAME_STATE(GAME_STATE_END)
						NET_PRINT("    ----->     HIDEOUT - Moving SERVER to GAME_STATE_END - A") NET_NL()
					ENDIF*/	
					
					#IF IS_DEBUG_BUILD
						IF bHostEndMissionNow
							serverBD.iServerGameState = GAME_STATE_END
							NET_PRINT("    ----->     HIDEOUT - Moving SERVER to GAME_STATE_END - bHostEndMissionNow - X") NET_NL()
						ENDIF
					#ENDIF
				BREAK
				 
				CASE GAME_STATE_WAITING_TO_LEAVE
				
					// IF all participants in waiting to leave state.
						serverBD.iServerGameState = GAME_STATE_END
						NET_PRINT("    ----->     HIDEOUT - Moving SERVER to GAME_STATE_END - B") NET_NL()
					// ENDIF
					
				BREAK
				
				CASE GAME_STATE_END		
					// Do nothing, clients handle this.
				BREAK	
				
				DEFAULT 
					#IF IS_DEBUG_BUILD
						SCRIPT_ASSERT("HIDEOUT: Problem in SWITCH GET_SERVER_GAME_STATE()") 
					#ENDIF
				BREAK
				
			ENDSWITCH
		ENDIF
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("server processing")
		#ENDIF
		#ENDIF
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		SCRIPT_PROFILER_END_OF_FRAME()
		#ENDIF
		#ENDIF	
		
	ENDWHILE
ENDSCRIPT
