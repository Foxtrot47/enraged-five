// ________________________________________
//
//	Game: GTAV - Multiplayer Races.
//  Script: MP_Races
//  Author: Kevin Wong
//	Description: Parent script for multiplayer races.
// ___________________________________________   

USING "globals.sch"
USING "net_races.sch" 
USING "FMMC_MP_Setup_Mission.sch"
USING "net_script_tunables.sch"		// KGM 25/7/12 - added so that Refresh_MP_Script_Tunables() can be called to gather Race specific values
#IF IS_DEBUG_BUILD
USING "profiler.sch"
#ENDIF
USING "net_wait_zero.sch"

// ----------------------------------- RACE VARIABLES ------------------------------------------------------------
BIG_RACE_STRUCT sharedBigRaceVars
SC_LB_DATA_BIG sharedSCLbd
ServerBroadcastData serverBD 
PlayerBroadcastData playerBD[FMMC_MAX_NUM_RACERS]
SC_LBD_PLAYER_LIST sharedLBDPlayers[FMMC_MAX_NUM_RACERS]
LEADERBOARD_PLACEMENT_TOOLS Placement 
INT iLoopParticipant
// -----------------------------------	MAIN PROCS ----------------------------------------------------------------------

PROC SETUP_basejump_RACE()
	g_FMMC_STRUCT.iNumLaps = 1
	SET_RACE_TYPE(FMMC_RACE_TYPE_BASEJUMP_P2P)
	// global data
	GlobalServerBD_Races.iLaps				=	g_FMMC_STRUCT.iNumLaps 
	GlobalServerBD_Races.iRaceType			=	g_FMMC_STRUCT.iRaceType					
	GlobalServerBD_Races.iRaceClass 	=g_FMMC_STRUCT.iRaceClass			
	g_bturn_on_basejumps_cloud 		= TRUE
	PRINTLN("g_bturn_on_basejumps_cloud 		= TRUE ")	
ENDPROC

//Resset the players PlayerBroadcastData
PROC RESET_PlayerBroadcastData()
	PlayerBroadcastData playerBBReset
	playerBD[PARTICIPANT_ID_TO_INT()] = playerBBReset
	PRINTLN("[RESET] RESET_PlayerBroadcastData ")
ENDPROC

//Reset all the ServerBroadcastData
PROC RESET_ServerBroadcastData()
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		ServerBroadcastData serverBDReset 
		serverBD = serverBDReset
		PRINTLN("[RESET] RESET_ServerBroadcastData ")
	ENDIF
ENDPROC

PROC RESET_GlobalServerBD_Race()
	GlobalServerBroadcastDataRaces GlobalServerBD_DM_RESET
	GlobalServerBD_Races = GlobalServerBD_DM_RESET
	PRINTLN("[RESET] RESET_GlobalServerBD_Race()")
ENDPROC

/// PURPOSE:
///    Runs on all clients at startup.  Used to initialize values and set/register variables and such.
/// RETURNS:
///    FALSE if the script fails to receive an initial network broadcast.
FUNC BOOL PROCESS_PRE_GAME(MP_MISSION_DATA &fmmcMissionData)

	serverBD.sServerFMMC_EOM.bAllowedToVote = FALSE

	GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_BASE_JUMP
	
	FMMC_PROCESS_PRE_GAME_COMMON(fmmcMissionData, serverBD.iPlayerMissionToLoad, serverBD.iMissionVariation)
	
//	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
//		g_sRC_SB_CoronaOptions.bServerStartsRaces = TRUE
//	ENDIF
	
	// #1 Everyone resets menu globals 
	RESET_GlobalServerBD_Race()
	RESET_PlayerBroadcastData()
	RESET_ServerBroadcastData()
	
	// #2 Clients must register up front so that the initial data is consistent with the server
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		GlobalServerBD_Races = g_sRC_SB_CoronaOptions
	ENDIF
	RESET_SERVER_NEXT_JOB_VARS()
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(g_sMC_serverBDEndJob, SIZE_OF(g_sMC_serverBDEndJob))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(GlobalServerBD_Races, SIZE_OF(GlobalServerBD_Races))
	
	// #3 Host needs to store the data after registration to ensure it gets broadcast to late joiners
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		GlobalServerBD_Races = g_sRC_SB_CoronaOptions
	ENDIF
	
	// This script will not be paused if another script calls PAUSE_GAME
	SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	
	// KGM: Wait for the first network broadcast before moving on - call this as the last instruction in pre-game
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		RETURN FALSE
	ENDIF
		
	GlobalServerBD_Races.iRaceType = g_FMMC_STRUCT.iRaceType
	GlobalServerBD_Races.iRaceClass = g_FMMC_STRUCT.iRaceClass
	PRINTLN("Setting GlobalServerBD_Races.iRaceType  = ", GlobalServerBD_Races.iRaceType)
	PRINTLN("Setting GlobalServerBD_Races.iRaceClass  = ", GlobalServerBD_Races.iRaceClass)
	GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_BASE_JUMP
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
//		serverBD.iRaceID = FMMC
	
		PLAYSTATS_CREATE_MATCH_HISTORY_ID_2(serverBD.iHashedMac, serverBD.iMatchHistoryId)
	//	PLAYSTATS_CREATE_MATCH_HISTORY_ID_2(serverBD.iPlayStatsID[0], serverBD.iPlayStatsID[1])
	ENDIF
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(PLAYER_PED_ID(),KNOCKOFFVEHICLE_DEFAULT)
	ENDIF
	SET_FM_MISSION_LAUNCHED_SUCESS(fmmcMissionData.mdID.idCreator, fmmcMissionData.mdID.idVariation, fmmcMissionData.iInstanceId)

	g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles 		= FMMC_MAX_VEHICLES
	g_sRC_SB_CoronaOptions.iRaceTrack 		= FMMC_RACE
	g_sRC_SB_CoronaOptions.iLaps				= (g_FMMC_STRUCT.iNumLaps + 1)
				
	RESET_REMINDER_TIMER_FOR_ACTIVITY(FMMC_TYPE_BASE_JUMP) 	
	
	IF NOT DOES_SCENARIO_BLOCKING_AREA_EXISTS(<<-744.8, -1544.2, 0.0>>,<<-722.0,-1445.1,6.0>>)
		sharedBigRaceVars.racing_scenario_blocking_index_1 = ADD_SCENARIO_BLOCKING_AREA(<<-744.8, -1544.2, 0.0>>,<<-722.0,-1445.1,6.0>>, FALSE)
	ELSE
		PRINTLN("RACE CONTROLER - ADD_SCENARIO_BLOCKING_AREA x 2 -1st - failed blocking area already exists", GlobalServerBD_Races.iRaceClass)
	ENDIF
	IF NOT DOES_SCENARIO_BLOCKING_AREA_EXISTS(<<-117.8, -849.2, 300.0>>,<<-0.0,-773.1,400.0>>)
		sharedBigRaceVars.racing_scenario_blocking_index_2 = ADD_SCENARIO_BLOCKING_AREA(<<-117.8, -849.2, 300.0>>,<<-0.0,-773.1,400.0>>, FALSE)
	ELSE
		PRINTLN("RACE CONTROLER - ADD_SCENARIO_BLOCKING_AREA x 2 -2nd - failed blocking area already exists", GlobalServerBD_Races.iRaceClass)
	ENDIF
	
	PRINTLN("RACE CONTROLER - ADD_SCENARIO_BLOCKING_AREA x 2", GlobalServerBD_Races.iRaceClass)
	
	RETURN TRUE
ENDFUNC

PROC PRINT_CLOUD_VALUES()
	#IF IS_DEBUG_BUILD
		PRINTLN("-------------------------------------------------------")
		PRINTLN("-------------------------------------------------------")
		PRINTLN("--                                                   --")
		PRINTLN("--                 BASEJUMP DATA                     --")
		PRINTLN("--                                                   --")
		PRINTLN("-------------------------------------------------------")
		PRINTLN("-------------------------------------------------------")
//				PRINTLN(" g_sRC_SB_CoronaOptions.iNumRacers     = ", g_sRC_SB_CoronaOptions.iNumRacers)
		PRINTLN(" g_FMMC_STRUCT.iRaceType             = ", GlobalServerBD_Races.iRaceType)
		PRINTLN(" g_FMMC_STRUCT.iRaceClassBitSet      = ", GlobalServerBD_Races.iRaceClass)
		PRINTLN(" g_FMMC_STRUCT.iNumParticipants     = ", g_FMMC_STRUCT.iNumParticipants)
		PRINTLN(" g_FMMC_STRUCT_ENTITIES.iNumberOfPeds     = ", g_FMMC_STRUCT_ENTITIES.iNumberOfPeds)
		PRINTLN(" g_sRC_SB_CoronaOptions.iRaceTrack  = ", g_sRC_SB_CoronaOptions.iRaceTrack)
		PRINTLN(" g_sRC_SB_CoronaOptions.iLaps 	     = ", g_sRC_SB_CoronaOptions.iLaps)
		PRINTLN(" g_FMMC_STRUCT.iNumberOfCheckPoints = ", g_FMMC_STRUCT.iNumberOfCheckPoints)
		PRINTLN(" g_FMMC_STRUCT.vStartPos 	         = ", g_FMMC_STRUCT.vStartPos)
		PRINTLN(" g_FMMC_STRUCT.vStartingGrid 	     = ", g_FMMC_STRUCT.vStartingGrid)
		PRINTLN(" g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles    = ", g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles )
		PRINTLN(" g_FMMC_STRUCT.inumberofcheckpoints    = ", g_FMMC_STRUCT.iNumberOfCheckPoints )
		PRINTLN(" g_FMMC_STRUCT_ENTITIES.sPlacedPed[0].vPos      = ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[0].vPos )
		PRINTLN(" g_FMMC_STRUCT_ENTITIES.sPlacedPed[0].fHead     = ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[0].fHead )
		
		PRINTLN(" g_FMMC_STRUCT_ENTITIES.sPlacedPed[1].vPos      = ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[1].vPos )
		PRINTLN(" g_FMMC_STRUCT_ENTITIES.sPlacedPed[1].fHead     = ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[1].fHead )
		
		PRINTLN(" g_FMMC_STRUCT_ENTITIES.sPlacedPed[2].vPos      = ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[2].vPos )
		PRINTLN(" g_FMMC_STRUCT_ENTITIES.sPlacedPed[2].fHead     = ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[2].fHead )
		
		PRINTLN(" g_FMMC_STRUCT_ENTITIES.sPlacedPed[3].vPos      = ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[3].vPos )
		PRINTLN(" g_FMMC_STRUCT_ENTITIES.sPlacedPed[3].fHead     = ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[3].fHead )
		
		PRINTLN(" g_FMMC_STRUCT_ENTITIES.sPlacedPed[4].vPos      = ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[4].vPos )
		PRINTLN(" g_FMMC_STRUCT_ENTITIES.sPlacedPed[4].fHead     = ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[4].fHead )
		
		PRINTLN(" g_FMMC_STRUCT.sPlacedCheckpoint[0].fCheckPointRespawn     = ", g_FMMC_STRUCT.sPlacedCheckpoint[0].fCheckPointRespawn )
		PRINTLN(" g_FMMC_STRUCT.sPlacedCheckpoint[1].fCheckPointRespawn     = ", g_FMMC_STRUCT.sPlacedCheckpoint[1].fCheckPointRespawn )
		PRINTLN(" g_FMMC_STRUCT.sPlacedCheckpoint[2].fCheckPointRespawn     = ", g_FMMC_STRUCT.sPlacedCheckpoint[3].fCheckPointRespawn )
		PRINTLN(" g_FMMC_STRUCT.sPlacedCheckpoint[3].fCheckPointRespawn     = ", g_FMMC_STRUCT.sPlacedCheckpoint[4].fCheckPointRespawn )
		PRINTLN(" g_FMMC_STRUCT.sPlacedCheckpoint[4].fCheckPointRespawn     = ", g_FMMC_STRUCT.sPlacedCheckpoint[5].fCheckPointRespawn )
		PRINTLN(" g_FMMC_STRUCT.sPlacedCheckpoint[5].fCheckPointRespawn     = ", g_FMMC_STRUCT.sPlacedCheckpoint[6].fCheckPointRespawn )
		
		
		PRINTLN(" g_FMMC_STRUCT.sPlacedCheckpoint[0].vCheckPoint             = ", g_FMMC_STRUCT.sPlacedCheckpoint[0].vCheckPoint  )		
		PRINTLN(" g_FMMC_STRUCT.sPlacedCheckpoint[1].vCheckPoint             = ", g_FMMC_STRUCT.sPlacedCheckpoint[1].vCheckPoint  )
		PRINTLN(" g_FMMC_STRUCT.sPlacedCheckpoint[2].vCheckPoint             = ", g_FMMC_STRUCT.sPlacedCheckpoint[2].vCheckPoint  )
		PRINTLN(" g_FMMC_STRUCT.sPlacedCheckpoint[3].vCheckPoint             = ", g_FMMC_STRUCT.sPlacedCheckpoint[3].vCheckPoint  )
		PRINTLN(" g_FMMC_STRUCT.sPlacedCheckpoint[4].vCheckPoint             = ", g_FMMC_STRUCT.sPlacedCheckpoint[4].vCheckPoint  )

		PRINTLN(" g_FMMC_STRUCT.sPlacedCheckpoint[5].vCheckPoint             = ", g_FMMC_STRUCT.sPlacedCheckpoint[5].vCheckPoint  )		
		PRINTLN(" g_FMMC_STRUCT.sPlacedCheckpoint[6].vCheckPoint             = ", g_FMMC_STRUCT.sPlacedCheckpoint[6].vCheckPoint  )
		PRINTLN(" g_FMMC_STRUCT.sPlacedCheckpoint[7].vCheckPoint             = ", g_FMMC_STRUCT.sPlacedCheckpoint[7].vCheckPoint  )
		PRINTLN(" g_FMMC_STRUCT.sPlacedCheckpoint[8].vCheckPoint             = ", g_FMMC_STRUCT.sPlacedCheckpoint[8].vCheckPoint  )
		PRINTLN(" g_FMMC_STRUCT.sPlacedCheckpoint[9].vCheckPoint             = ", g_FMMC_STRUCT.sPlacedCheckpoint[9].vCheckPoint  )
		
		
		PRINTLN(" g_FMMC_STRUCT_ENTITIES.sPlacedPed[0].vPos  = ",   g_FMMC_STRUCT_ENTITIES.sPlacedPed[0].vPos  )
		PRINTLN(" g_FMMC_STRUCT_ENTITIES.sPlacedPed[0].fHead = ",   g_FMMC_STRUCT_ENTITIES.sPlacedPed[0].fHead )
		PRINTLN(" g_FMMC_STRUCT_ENTITIES.sPlacedPed[1].vPos  = ",   g_FMMC_STRUCT_ENTITIES.sPlacedPed[1].vPos  )
		PRINTLN(" g_FMMC_STRUCT_ENTITIES.sPlacedPed[1].fHead = ",   g_FMMC_STRUCT_ENTITIES.sPlacedPed[1].fHead )
		PRINTLN(" g_FMMC_STRUCT_ENTITIES.sPlacedPed[2].vPos  = ",   g_FMMC_STRUCT_ENTITIES.sPlacedPed[2].vPos  )
		PRINTLN(" g_FMMC_STRUCT_ENTITIES.sPlacedPed[2].fHead = ",   g_FMMC_STRUCT_ENTITIES.sPlacedPed[2].fHead )
		PRINTLN(" g_FMMC_STRUCT_ENTITIES.sPlacedPed[3].vPos  = ",   g_FMMC_STRUCT_ENTITIES.sPlacedPed[3].vPos  )
		PRINTLN(" g_FMMC_STRUCT_ENTITIES.sPlacedPed[3].fHead = ",   g_FMMC_STRUCT_ENTITIES.sPlacedPed[3].fHead )
		PRINTLN(" g_FMMC_STRUCT_ENTITIES.sPlacedPed[4].vPos  = ",   g_FMMC_STRUCT_ENTITIES.sPlacedPed[4].vPos  )
		PRINTLN(" g_FMMC_STRUCT_ENTITIES.sPlacedPed[4].fHead = ",   g_FMMC_STRUCT_ENTITIES.sPlacedPed[4].fHead )
		PRINTLN(" g_FMMC_STRUCT_ENTITIES.sPlacedPed[5].vPos  = ",   g_FMMC_STRUCT_ENTITIES.sPlacedPed[5].vPos  )
		PRINTLN(" g_FMMC_STRUCT_ENTITIES.sPlacedPed[5].fHead = ",   g_FMMC_STRUCT_ENTITIES.sPlacedPed[5].fHead )
		PRINTLN(" g_FMMC_STRUCT_ENTITIES.sPlacedPed[6].vPos  = ",   g_FMMC_STRUCT_ENTITIES.sPlacedPed[6].vPos  )
		PRINTLN(" g_FMMC_STRUCT_ENTITIES.sPlacedPed[6].fHead = ",   g_FMMC_STRUCT_ENTITIES.sPlacedPed[6].fHead )
		PRINTLN(" g_FMMC_STRUCT_ENTITIES.sPlacedPed[7].vPos  = ",   g_FMMC_STRUCT_ENTITIES.sPlacedPed[7].vPos  )
		PRINTLN(" g_FMMC_STRUCT_ENTITIES.sPlacedPed[7].fHead = ",   g_FMMC_STRUCT_ENTITIES.sPlacedPed[7].fHead )
	#ENDIF
ENDPROC

PROC RESET_CHECKPOINT_STRUCT()
	CHECKPOINTS_STRUCT tempCheckpointData
	sharedBigRaceVars.currentCheckPoint = tempCheckpointData
	sharedBigRaceVars.nextCheckPoint = tempCheckpointData
ENDPROC

PROC RESET_BIG_RACE_STRUCT()
	BIG_RACE_STRUCT tempBigRaceData
	sharedBigRaceVars = tempBigRaceData
ENDPROC

PROC RESET_THE_LEADERBOARD_STRUCT()
	THE_LEADERBOARD_STRUCT leaderboardBlank
	INT i
	REPEAT FMMC_MAX_NUM_RACERS i
		serverBD.leaderboard[i] = leaderboardBlank
	ENDREPEAT
ENDPROC

FUNC BOOL HAS_NEXT_RACE_LOADED()

	IF serverBD.iNextMission = -1
		//If the mission to load has not changed the move to the initilisation stage
		
		NET_PRINT("CONTROL_LOADING_NEXT_RACE, serverBD.iNextMission = -1") NET_NL()
		
//		SET_CLIENT_GAME_STATE(playerBD, GAME_STATE_INI)
		playerBD[PARTICIPANT_ID_TO_INT()].iFinishTime = 0
		
		RETURN TRUE
	ELSE
		HIDE_HUD_AND_RADAR_THIS_FRAME()
		//Load all the data from the requested mission
		//IF FMMC_LOAD_MISSION_DATA(sFMMCdataFile, playerBD[PARTICIPANT_ID_TO_INT()].iDataFileBitSet, FMMC_ROCKSTAR_CREATOR_ID, #IF IS_DEBUG_BUILD serverBD.iNextMission, #endif  sharedBigRaceVars.stRockStarRaceToLoad, FMMC_TYPE_RACE)
			SET_NEXT_FM_MISSION_LAUNCH_SUCESS(FMMC_ROCKSTAR_CREATOR_ID, serverBD.iNextMission, g_FMMC_STRUCT.vStartPos)
			SET_FM_MISSION_AS_JOINABLE()
			IF GlobalServerBD_Races.iRaceType > 1
				IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
					GlobalServerBD_Races.iRaceType	 = 1
				ENDIF
				g_mnMyRaceModel = GET_VEHICLE_MODEL_FROM_RACE_TYPE_AND_SELECTION(0, 0)
			ENDIF
			IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
				//Set up the server broadcast data to the loaded values
				GlobalServerBD_Races.iLaps 		 = g_FMMC_STRUCT.iNumLaps
				GlobalServerBD_Races.iRaceType   = g_FMMC_STRUCT.iRaceType
				GlobalServerBD_Races.iWeapons    = g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons 
				GlobalServerBD_Races.iTraffic    = g_FMMC_STRUCT.iTraffic 
			ENDIF
			sharedBigRaceVars.bLoadedMissionRatingDetails = FALSE
			//Move to the initilisation stage
			NET_PRINT("CONTROL_LOADING_NEXT_RACE, serverBD.iNextMission <> -1") NET_NL()
			NET_PRINT("serverBD.iNextMission = ")NET_PRINT_INT(serverBD.iNextMission)NET_NL()
//			SET_CLIENT_GAME_STATE(playerBD, GAME_STATE_INI)

			RETURN TRUE
		//ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SET_MENU_OPTIONS_TO_DEFAULT()
	PRINTLN("SET_MENU_OPTIONS_TO_DEFAULT")
	PRINTLN("sharedBigRaceVars.sRaceMenuOptions.sLocalPlayerOptions.iSelection[", ciOPTION_CAR, "] = ", g_iMyRaceModelChoice)
	sharedBigRaceVars.sRaceMenuOptions.sLocalPlayerOptions.iSelection[ciOPTION_CAR] = g_iMyRaceModelChoice
ENDPROC

// -----------------------------------	MAIN LOOP ----------------------------------------------------------------------
SCRIPT(MP_MISSION_DATA fmmcMissionData)
	
	// Carry out all the initial game starting duties. 
	IF NETWORK_IS_GAME_IN_PROGRESS()
		// Carry out all the initial game starting duties. 
		IF NOT PROCESS_PRE_GAME(fmmcMissionData)
			PRINTLN("FM_BJ_RACE_CONTROLLER: Failed to receive an initial network broadcast. Cleaning up.")
			SCRIPT_CLEANUP_RACES(serverBD, playerBD, sharedBigRaceVars, Placement, ciFMMC_END_OF_MISSION_STATUS_CANCELLED)
		ENDIF
	ENDIF
	
	HAS_CLIENT_CREATED_PICKUPS(serverBD, sharedBigRaceVars)
	
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
//		HAS_STARTING_GRID_BEEN_GENERATED(serverBD, sharedBigRaceVars, GlobalServerBD_Races.iNumRacers)
		SERVER_SETS_POINT_TO_POINT_RACES_AS_TO_ONE_LAP(serverBD)
	ENDIF

	SETUP_basejump_RACE()

	#IF IS_DEBUG_BUILD
		CREATE_RACE_WIDGETS(serverBD, playerBD, sharedBigRaceVars)
		PRINT_CLOUD_VALUES()
	#ENDIF
	
	WHILE TRUE 
		
		//Load the mission rating
		IF sharedBigRaceVars.bLoadedMissionRatingDetails = FALSE
			sharedBigRaceVars.bLoadedMissionRatingDetails = TRUE
		ENDIF	
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		SCRIPT_PROFILER_END_OF_FRAME()
		#ENDIF
		#ENDIF		
		
		// One wait to rule them all. This can be the ONLY wait from here on in.....
		MP_LOOP_WAIT_ZERO()
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		SCRIPT_PROFILER_START_OF_FRAME()
		#ENDIF
		#ENDIF	
		
		// If we have a match end event, bail.
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			//DO_SCREEN_FADE_OUT(50)
			SCRIPT_CLEANUP_RACES(serverBD, playerBD, sharedBigRaceVars, Placement, ciFMMC_END_OF_MISSION_STATUS_CANCELLED)
		ENDIF
		IF SHOULD_PLAYER_LEAVE_MP_MISSION(TRUE) 
			//DO_SCREEN_FADE_OUT(50)
			SCRIPT_CLEANUP_RACES(serverBD, playerBD, sharedBigRaceVars, Placement, ciFMMC_END_OF_MISSION_STATUS_CANCELLED)
		ENDIF
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("cleanup checks")
		#ENDIF
		#ENDIF		
		
		// Deal with the debug.
		#IF IS_DEBUG_BUILD		
			UPDATE_RACE_WIDGETS(serverBD, playerBD, sharedBigRaceVars)
		#ENDIF
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("UPDATE_RACE_WIDGETS")
		#ENDIF
		#ENDIF
		
		SPECTATOR_SET_TIME_WEATHER_PRE_RACE(serverBD, sharedBigRaceVars)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("SPECTATOR_SET_TIME_WEATHER_PRE_RACE")
		#ENDIF
		#ENDIF
		
		PROCESS_RACE_EVENTS(serverBD, playerBD, sharedBigRaceVars)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("PROCESS_RACE_EVENTS")
		#ENDIF
		#ENDIF
		
		SAVE_THUMB_VOTE(sharedBigRaceVars)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("SAVE_THUMB_VOTE")
		#ENDIF
		#ENDIF 
		
		HANDLE_PARACHUTE_DETACH(playerBD)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("HANDLE_PARACHUTE_DETACH")
		#ENDIF
		#ENDIF 
		
		MAKE_RACE_PROPS_AND_PICKUPS(serverBD, playerBD, sharedBigRaceVars)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("MAKE_RACE_PROPS_AND_PICKUPS") 
		#ENDIF
		#ENDIF

		//This now has to happen every frame otherwise it will not properly ensure the correct traffic state on the server or client
		FM_MAINTAIN_MISSION_DENSITYS_THIS_FRAME(GET_RACES_PED_DENSITY(), 0.0, 0.0, GET_TRAFFIC_DENSITY_FOR_RACES(serverBD), GET_TRAFFIC_DENSITY_FOR_RACES(serverBD), GET_PARKED_TRAFFIC_DENSITY_FOR_RACES(serverBD), GET_TRAFFIC_DENSITY_FOR_RACES(serverBD))

// -----------------------------------	CLIENT
		SWITCH GET_CLIENT_GAME_STATE(playerBD, PARTICIPANT_ID_TO_INT())
						
			// Wait until the server gives the all go before moving on.
			CASE GAME_STATE_INI
			
				playerBD[PARTICIPANT_ID_TO_INT()].iFinishTime = 0				
					
				HIDE_HUD_AND_RADAR_THIS_FRAME()

				IF GET_SERVER_GAME_STATE(serverBD) > GAME_STATE_GRID	
					IF IS_NET_PLAYER_OK(PLAYER_ID())
						SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
						RESERVE_NETWORK_MISSION_OBJECTS(1)
					
						NET_PRINT("     ---------->	GET_SERVER_GAME_STATE(serverBD) > GAME_STATE_INI") NET_NL()	
							
						SET_CLIENT_GAME_STATE(playerBD, GAME_STATE_RUNNING)
					ENDIF
				ENDIF
			BREAK
			
			// Main gameplay state.
			CASE GAME_STATE_RUNNING											
				PROCESS_RACE_CLIENT(Placement, serverBD, playerBD, sharedBigRaceVars)
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE 
					ADD_SCRIPT_PROFILE_MARKER("PROCESS_RACE_CLIENT")
				#ENDIF
				#ENDIF
				
				REQUEST_ALL_BJ_ANIMS_AND_MODELS()
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE 
					ADD_SCRIPT_PROFILE_MARKER("REQUEST_ALL_BJ_ANIMS_AND_MODELS") 
				#ENDIF
				#ENDIF
				
				// Look for the server say the mission has ended.
				IF GET_SERVER_GAME_STATE(serverBD) = GAME_STATE_END		
					#IF IS_DEBUG_BUILD
						NET_PRINT_TIME() NET_PRINT_STRINGS("----------> server = GAME_STATE_END moving to  client GAME_STATE_LEAVE ", "Race") NET_NL()	
					#ENDIF
					
					SET_CLIENT_GAME_STATE(playerBD, GAME_STATE_LEAVE)	
				ENDIF
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE 
					ADD_SCRIPT_PROFILE_MARKER("Look for the server say the mission has ended.")
				#ENDIF
				#ENDIF
				
				// -----------------------------------	DEBUGGERY ----------------------------------------------------------------------
				#IF IS_DEBUG_BUILD
				
					UPDATE_DEBUG_SKIPS(serverBD, playerBD, sharedBigRaceVars)
					
				#ENDIF
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE 
					ADD_SCRIPT_PROFILE_MARKER("DEBUGGERY")
				#ENDIF
				#ENDIF
				// ---------------------------------------------------------------------------------------------------------------------
			BREAK	
			
			CASE GAME_STATE_LEAVE	
				SET_CLIENT_GAME_STATE(playerBD, GAME_STATE_END)
			FALLTHRU

			CASE GAME_STATE_END				
				//DO_SCREEN_FADE_OUT(50)
	
				SCRIPT_CLEANUP_RACES(serverBD, playerBD, sharedBigRaceVars, Placement, ciFMMC_END_OF_MISSION_STATUS_PASSED)
			BREAK
			
			DEFAULT 
				#IF IS_DEBUG_BUILD
					SCRIPT_ASSERT("fm_RACES: Problem in SWITCH GET_CLIENT_MISSION_STATE(PARTICIPANT_ID_TO_INT())") 
				#ENDIF
			BREAK
		ENDSWITCH
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("game state processing")
		#ENDIF
		#ENDIF
		
// -----------------------------------	SERVER		

		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		
			BOOL bCoronaReady = TRUE
			IF IS_PLAYER_IN_CORONA()
				bCoronaReady = IS_CORONA_READY_TO_START_WITH_JOB()
			ENDIF
		
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
			OPEN_SCRIPT_PROFILE_MARKER_GROUP("server processing") 
			#ENDIF
			#ENDIF	
			
			SERVER_GIVES_LATE_JOINERS_GRID_POSITION(serverBD, playerBD)
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
				ADD_SCRIPT_PROFILE_MARKER("SERVER_GIVES_LATE_JOINERS_GRID_POSITION")
			#ENDIF
			#ENDIF
			
			IF bCoronaReady
				iLoopParticipant += 1			
				IF (iLoopParticipant = NETWORK_GET_MAX_NUM_PARTICIPANTS())
					iLoopParticipant = 0 	
				ENDIF
				
				SERVER_MAINTAIN_RACE_LEADERBOARD_FOR_PARTICIPANT(serverBD, serverBD.leaderboard, playerBD, sharedBigRaceVars, iLoopParticipant)			
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE 
					ADD_SCRIPT_PROFILE_MARKER("SERVER_MAINTAIN_RACE_LEADERBOARD")
				#ENDIF
				#ENDIF
			
				SERVER_SETS_RACE_END_BIT(serverBD, sharedBigRaceVars)
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE 
					ADD_SCRIPT_PROFILE_MARKER("SERVER_SETS_RACE_END_BIT")
				#ENDIF
				#ENDIF
				
				SERVER_SETS_BIT_WHEN_FIFTEEN_SECONDS_REMAIN(serverBD)
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE 
					ADD_SCRIPT_PROFILE_MARKER("SERVER_SETS_BIT_WHEN_FIFTEEN_SECONDS_REMAIN")
				#ENDIF
				#ENDIF
			ENDIF
			
			SWITCH GET_SERVER_GAME_STATE(serverBD)
			
				CASE GAME_STATE_INI							
//					IF GlobalServerBD_Races.bServerStartsRaces	
						IF HAVE_ALL_CORONA_PLAYERS_JOINED(sharedBigRaceVars.timeCoronaFailSafe)
							IF SHOULD_LONE_SPECTATOR_CLEAN_UP(sharedBigRaceVars.timeCoronaFailSafe)
							
								PRINTLN("[CS_PARA] [HAVE_ALL_CORONA_PLAYERS_JOINED], SHOULD_LONE_SPECTATOR_CLEAN_UP, SCRIPT_CLEANUP_RACES ")
								SCRIPT_CLEANUP_RACES(serverBD, playerBD, sharedBigRaceVars, Placement, ciFMMC_END_OF_MISSION_STATUS_CANCELLED)
							ELSE
								serverbd.bis_baseJump = TRUE
								GlobalServerBD_Races.iRaceMode = ciRACE_SUB_TYPE_STANDARD
								INIT_SERVER_DATA(serverBD, playerBD)
								SERVER_SET_PED_AND_TRAFFIC_DENSITIES(serverBD.iPopulationHandle, GET_RACES_PED_DENSITY(), GET_TRAFFIC_DENSITY_FOR_RACES(serverBD))
								SERVER_SET_NUMBER_OF_LAPS(serverBD)
								SERVER_SET_NUMBER_CHECKPOINTS(serverBD)
								SERVER_SET_NUMBER_PICKUPS(serverBD)
								RESERVE_NETWORK_MISSION_OBJECTS(1)
							
								SET_SERVER_GAME_STATE(serverBD, GAME_STATE_GRID)
							ENDIF
						ENDIF
//					ENDIF
				BREAK
				
				CASE GAME_STATE_GRID
					IF HAS_STARTING_GRID_BEEN_GENERATED(serverBD, sharedSCLbd, sharedLBDPlayers, serverBD.iNumRaceStarters, playerBD)
													
						CLIENT_CLEAR_WHOLE_MAP_AREA()
						RESET_THE_LEADERBOARD_STRUCT()
						SET_SERVER_GAME_STATE(serverBD, GAME_STATE_COUNTDOWN)
					ENDIF
				BREAK
				
				CASE GAME_STATE_COUNTDOWN
					IF bCoronaReady
						IF HAS_SERVER_CHECKED_ALL_CLIENTS_READY_TO_DRAW_COUNTDOWN(serverBD)
							SERVER_STARTS_RACE_COUNTDOWN(serverBD)
							RESET_THE_LEADERBOARD_STRUCT()
							SET_SERVER_GAME_STATE(serverBD, GAME_STATE_RUNNING)	
						ENDIF
						
						SERVER_PROCESSING(serverBD, playerBD, sharedBigRaceVars)
						#IF IS_DEBUG_BUILD
						#IF SCRIPT_PROFILER_ACTIVE 
							ADD_SCRIPT_PROFILE_MARKER("SERVER_PROCESSING")
						#ENDIF
						#ENDIF
					ENDIF
				BREAK
				
				// Look for game end conditions.
				CASE GAME_STATE_RUNNING	
				
					START_RACE_TIMER(serverBD)
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE 
						ADD_SCRIPT_PROFILE_MARKER("START_RACE_TIMER")
					#ENDIF
					#ENDIF
					
					SERVER_GRABS_AVERAGE_RANK_OF_RACE_PLAYERS(serverBD)
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE 
						ADD_SCRIPT_PROFILE_MARKER("SERVER_GRABS_AVERAGE_RANK_OF_RACE_PLAYERS")
					#ENDIF
					#ENDIF

					SERVER_PROCESSING(serverBD, playerBD, sharedBigRaceVars)
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE 
						ADD_SCRIPT_PROFILE_MARKER("SERVER_PROCESSING")
					#ENDIF
					#ENDIF
					
					SERVER_SETS_LEADER(serverBD)
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE 
						ADD_SCRIPT_PROFILE_MARKER("SERVER_SETS_LEADER")
					#ENDIF
					#ENDIF
					
					SERVER_MAINTAIN_RACE_END_COUNTER(serverBD  #IF IS_DEBUG_BUILD , sharedBigRaceVars #ENDIF)
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE 
						ADD_SCRIPT_PROFILE_MARKER("SERVER_MAINTAIN_RACE_END_COUNTER")
					#ENDIF
					#ENDIF
					
					SERVER_DOES_RESTART_STUFF(g_sMC_serverBDEndJob, serverBD, sharedBigRaceVars, FMMC_TYPE_BASE_JUMP)
					#IF IS_DEBUG_BUILD
					#IF SCRIPT_PROFILER_ACTIVE 
						ADD_SCRIPT_PROFILE_MARKER("SERVER_DOES_RESTART_STUFF")
					#ENDIF
					#ENDIF
				BREAK
				
				CASE GAME_STATE_END			
				
					SERVER_MAINTAIN_RACE_END_COUNTER(serverBD  #IF IS_DEBUG_BUILD , sharedBigRaceVars #ENDIF)
									
					SERVER_DOES_RESTART_STUFF(g_sMC_serverBDEndJob, serverBD, sharedBigRaceVars, FMMC_TYPE_BASE_JUMP)
				BREAK	
				
				DEFAULT 
					#IF IS_DEBUG_BUILD
						SCRIPT_ASSERT("DM: Problem in SWITCH GET_SERVER_MISSION_STATE()") 
					#ENDIF
				BREAK
			ENDSWITCH
			
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
			CLOSE_SCRIPT_PROFILE_MARKER_GROUP() 
			#ENDIF
			#ENDIF
			
		ENDIF
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("server processing")
		#ENDIF
		#ENDIF

	ENDWHILE
ENDSCRIPT
