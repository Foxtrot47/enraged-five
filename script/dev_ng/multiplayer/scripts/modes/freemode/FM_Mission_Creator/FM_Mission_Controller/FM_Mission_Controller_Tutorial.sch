//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//
//!!! NEW SECTION BELOW: TUTORIAL INTRO CUTSCENES !!!
//
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************

USING "FM_Mission_Controller_USING.sch"

INT iTutorialMissionProg
CAMERA_INDEX camTut

MODEL_NAMES mPlayers[4]
PED_INDEX pedPlayerCut[4]
PLAYER_INDEX playerForCut[4]

SCRIPT_TIMER timeTutMission

VEHICLE_INDEX vehTutStart
VEHICLE_SETUP_STRUCT_MP vehSetupOriginal
VEHICLE_SEAT vehSeatToUse[4]
INT iDriverPlayerIndex = -1


FUNC BOOL HAS_EVERYONE_FINISHED_TUTORIAL_CUT()
	INT iparticipant
	PARTICIPANT_INDEX tempPart
	PLAYER_INDEX playerTemp
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iparticipant
		tempPart = INT_TO_PARTICIPANTINDEX(iparticipant)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
			playerTemp = NETWORK_GET_PLAYER_INDEX(tempPart)
			IF IS_NET_PLAYER_OK(playerTemp, FALSE)
				IF NOT HAS_PLAYER_COMPLETED_RACE_AND_DM_TUTORIAL(playerTemp)
					IF NOT IS_BIT_SET(MC_playerBD[iparticipant].iTutorialBitset, TUTORIAL_PLAYER_FINISHED_TUTORIAL_CUT)
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

FUNC BOOL HAVE_ALL_MODELS_LOADED_FOR_TUTORIAL_MISSION_CUT()
	INT i
	FOR i = 0 TO 3
		IF mPlayers[i] <> DUMMY_MODEL_FOR_SCRIPT
			IF NOT HAS_MODEL_LOADED(mPlayers[i])
				RETURN FALSE
			ENDIF
		ENDIF
	ENDFOR
	RETURN TRUE
ENDFUNC

/// PURPOSE: Sets the colour of the vehicle passed.
FUNC BOOL SET_UP_TUTORIAL_VEHICLE_COLOURS(VEHICLE_INDEX vehIndex, INT iLivery, MOD_COLOR_TYPE modColor, INT iCrewR, INT iCrewG, INT iCrewB)
	#IF IS_DEBUG_BUILD
		PRINTLN("[SET_UP_TUTORIAL_VEHICLE_COLOURS] ***** Called with new GET_RACE_COLOUR proc! .... ")
		IF IS_VEHICLE_DRIVEABLE(vehIndex)
			MODEL_NAMES model = GET_ENTITY_MODEL(vehIndex)
			PRINTLN("[SET_UP_TUTORIAL_VEHICLE_COLOURS] vehicle model = ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(model))
		ENDIF
		
		PRINTLN("[SET_UP_TUTORIAL_VEHICLE_COLOURS] iLivery = ", iLivery, " GET_VEHICLE_LIVERY_COUNT() = ", GET_VEHICLE_LIVERY_COUNT(vehIndex))
		PRINTLN("[SET_UP_TUTORIAL_VEHICLE_COLOURS] modColor = ",ENUM_TO_INT(modColor), " GET_NUM_MOD_KITS() = ", GET_NUM_MOD_KITS(vehIndex))
		PRINTLN("[SET_UP_TUTORIAL_VEHICLE_COLOURS] iCrewR = ", iCrewR, " iCrewG = ", iCrewG," iCrewB = ", iCrewB)
		PRINTLN("[SET_UP_TUTORIAL_VEHICLE_COLOURS] ... Done output *****")
	#ENDIF
	
	IF ENUM_TO_INT(modColor) > 0 ENDIF
	IF DOES_ENTITY_EXIST(vehIndex)
	AND NOT IS_ENTITY_DEAD(vehIndex)
		IF iLivery = -1
			
			SET_VEHICLE_CUSTOM_PRIMARY_COLOUR(vehIndex, iCrewR, iCrewG, iCrewB)
			SET_VEHICLE_CUSTOM_SECONDARY_COLOUR(vehIndex, iCrewR, iCrewG, iCrewB)
			PRINTLN("[dsw] [SET_UP_TUTORIAL_VEHICLE_COLOURS] - SET_VEHICLE_CUSTOM_SECONDARY_COLOUR: iRed = ", iCrewR, ", iGreen = ", iCrewG, ", iBlue = ", iCrewB)
		ELIF GET_VEHICLE_LIVERY_COUNT(vehIndex) > 0
		AND iLivery > -1
		 
			PRINTLN("[dsw] [SET_UP_TUTORIAL_VEHICLE_COLOURS] - setting vehcile LIVERY: ", iLivery)
			SET_VEHICLE_LIVERY(vehIndex, iLivery)
			
			IF GET_NUM_MOD_KITS(vehIndex) > 0
				SET_VEHICLE_MOD_KIT(vehIndex, 0)
				PRINTLN("[dsw] [SET_UP_TUTORIAL_VEHICLE_COLOURS] - Setting mod kit as GET_NUM_MOD_KITS = ", GET_NUM_MOD_KITS(vehIndex))
			ENDIF
		ELSE
			INT iValue = GET_RACE_COLOUR(PLAYER_ID(),iLivery,TRUE)
			
			SET_VEHICLE_COLOURS(vehIndex, iValue, iValue)
			SET_VEHICLE_EXTRA_COLOURS(vehIndex, iValue, 156)
			
			PRINTLN("[dsw] [SET_UP_TUTORIAL_VEHICLE_COLOURS] Setting vehicle colours: ", iValue)
		ENDIF

		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_DRIVER_OF_MISSION_VEH()
	INT iDriver = -1

	INT iparticipant
	INT iPlayerInt
	INT iBestPos = 100
	PARTICIPANT_INDEX tempPart
	PLAYER_INDEX playerTemp
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iparticipant
		tempPart = INT_TO_PARTICIPANTINDEX(iparticipant)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
			playerTemp = NETWORK_GET_PLAYER_INDEX(tempPart)
			IF IS_NET_PLAYER_OK(playerTemp, FALSE)
				iPlayerInt = NATIVE_TO_INT(playerTemp)		
				IF GlobalplayerBD_FM[iPlayerInt].fmRaceTutorial.iMyTutRacePos < iBestPos
					iBestPos = GlobalplayerBD_FM[iPlayerInt].fmRaceTutorial.iMyTutRacePos
					iDriver = iPlayerInt
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
		IF iDriver > -1
			PRINTLN("[dsw] [GET_DRIVER_OF_MISSION_VEH] Think this player did best in the race ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(iDriver)))
			PRINTLN("[dsw] [GET_DRIVER_OF_MISSION_VEH] They finished in this postion... ", iBestPos)
		ELSE	
			PRINTLN("[dsw] [GET_DRIVER_OF_MISSION_VEH] Failed to find a driver!")
		ENDIF
	#ENDIF
	
	RETURN iDriver
ENDFUNC



FUNC BOOL IS_PLAYER_DRIVER_OF_TUTORIAL_MISSION_CAR(INT index)
	RETURN (index = iDriverPlayerIndex)
ENDFUNC

FUNC VEHICLE_SEAT GET_PLAYER_VEHICLE_SEAT_FOR_TUTORIAL_MISSION(INT iPlayer)
	PRINTLN("[dsw] [GET_PLAYER_VEHICLE_SEAT_FOR_TUTORIAL_MISSION] Called with iPlayer = ", iPlayer)
	IF IS_PLAYER_DRIVER_OF_TUTORIAL_MISSION_CAR(iPlayer)
		PRINTLN("[dsw] [GET_PLAYER_VEHICLE_SEAT_FOR_TUTORIAL_MISSION] Player is driver... ")
		RETURN VS_DRIVER
	ENDIF
	VEHICLE_SEAT seat = VS_ANY_PASSENGER
	INT i
	INT index = -1
	FOR i = 0 TO 3
		IF NATIVE_TO_INT(playerForCut[i]) = iPlayer
			PRINTLN("[dsw] [GET_PLAYER_VEHICLE_SEAT_FOR_TUTORIAL_MISSION] Found in playerForCut array, i = ", i)
			index = i		
		ENDIF
	ENDFOR
	
	IF index > -1
		seat = vehSeatToUse[index]
		PRINTLN("[dsw] [GET_PLAYER_VEHICLE_SEAT_FOR_TUTORIAL_MISSION] seat =  ", ENUM_TO_INT(seat))
	ELSE	
		PRINTLN("[dsw] [GET_PLAYER_VEHICLE_SEAT_FOR_TUTORIAL_MISSION] Failed to find seat! ")
	ENDIF
	
	RETURN seat
ENDFUNC

PROC GET_CREATE_POSITION_FOR_TUTORIAL_MISSION_PED(INT index, VECTOR &vCreate, FLOAT &fHead)
	VEHICLE_SEAT vehSeat = GET_PLAYER_VEHICLE_SEAT_FOR_TUTORIAL_MISSION(NATIVE_TO_INT(playerForCut[index]))
	SWITCH vehSeat
		CASE VS_DRIVER 		vCreate =  <<365.7222, 278.0202, 102.2282>>  fHead = 262.5417 BREAK // DRIVER 
		CASE VS_FRONT_RIGHT vCreate =  <<370.3030, 275.8884, 102.1547>> fHead = 29.8921  BREAK // FRONT RIGHT
		CASE VS_BACK_LEFT	vCreate = <<365.3474, 276.9801, 102.2114>> fHead = 246.8323 BREAK // BACK LEFT
		CASE VS_BACK_RIGHT vCreate = <<369.9776, 274.6177, 102.1430>> fHead =  51.4049 BREAK
		
//		CASE VS_DRIVER 		vCreate =<<379.9914, 268.0341, 102.0145>>   fHead = 168.9818BREAK // DRIVER 
//		CASE VS_FRONT_RIGHT vCreate = <<378.5975, 262.4559, 102.0092>> fHead = 347.1148 BREAK // FRONT RIGHT
//		CASE VS_BACK_LEFT	vCreate = <<379.0344, 268.5128, 102.0207>> fHead =167.6443 BREAK // BACK LEFT
//		CASE VS_BACK_RIGHT vCreate =<<376.5000, 262.4131, 102.0095>>   fHead = 321.3387 BREAK
		
		
	ENDSWITCH
ENDPROC

PROC UNFREEZE_ALL_PEDS_FOR_TUTORIAL_MISSION()
	INT i
	FOR i = 0 TO 3
		IF DOES_ENTITY_EXIST(pedPlayerCut[i])
			IF NOT IS_PED_INJURED(pedPlayerCut[i])
				FREEZE_ENTITY_POSITION(pedPlayerCut[i], FALSE)
				PRINTLN("[dsw] [UNFREEZE_ALL_PEDS_FOR_TUTORIAL_MISSION] Unfrozen ped ", i)
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

PROC PUT_PLAYER_INTO_MISSION_CAR(INT iCarIndex)
	INT i
	VEHICLE_SEAT mySeat = VS_ANY_PASSENGER
	FOR i = 0 TO 3
		IF mySeat = VS_ANY_PASSENGER
			IF playerForCut[i] = LocalPlayer
				mySeat = GET_PLAYER_VEHICLE_SEAT_FOR_TUTORIAL_MISSION(NATIVE_TO_INT(playerForCut[i]))
				PRINTLN("[dsw] [PUT_PLAYER_INTO_MISSION_CAR] mySeat = ", ENUM_TO_INT(mySeat))
			ENDIF
		ENDIF
	ENDFOR
	
	IF mySeat <> VS_ANY_PASSENGER
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iCarIndex])
			IF IS_VEHICLE_DRIVEABLE(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iCarIndex]))
				IF bLocalPlayerPedOk
					PRINTLN("[dsw] [PUT_PLAYER_INTO_MISSION_CAR] I'm warping into seat ", ENUM_TO_INT(mySeat))
					TASK_ENTER_VEHICLE(LocalPlayerPed, NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iCarIndex]), 1, mySeat, PEDMOVEBLENDRATIO_RUN, ECF_WARP_PED )
				ENDIF
			ENDIF
		ENDIF
	ELSE	
		PRINTLN("[dsw] [PUT_PLAYER_INTO_MISSION_CAR] Didn't find a seat")
	ENDIF
	
	IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iCarIndex])
		IF IS_VEHICLE_DRIVEABLE(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iCarIndex]))
			SET_VEHICLE_ENGINE_ON(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iCarIndex]), TRUE, TRUE)
		//	SET_INITIAL_PLAYER_STATION("RADIO_06_COUNTRY")
		//	SET_VEH_RADIO_STATION(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iCarIndex]), "RADIO_06_COUNTRY")
			PRINTLN("[dsw] [PUT_PLAYER_INTO_MISSION_CAR] I set the radio station")
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL ARE_ALL_CUTSCENE_PEDS_IN_CAR()
	INT i
	FOR i = 0 TO 3
		IF DOES_ENTITY_EXIST(pedPlayerCut[i])
			IF NOT IS_PED_INJURED(pedPlayerCut[i])
				IF IS_VEHICLE_DRIVEABLE(vehTutStart)
					IF NOT IS_PED_SITTING_IN_VEHICLE(pedPlayerCut[i], vehTutStart)
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN TRUE
ENDFUNC
FUNC BOOL AM_I_DRIVER_OF_TUTORIAL_MISSION_VEHICLE()
	//RETURN (playerForCut[0] = LocalPlayer)
	RETURN (NATIVE_TO_INT(LocalPlayer) = iDriverPlayerIndex)
ENDFUNC

PROC SET_TUTORIAL_CUTSCENE_ENTITIES_VISIBLE(BOOL bVisible)
	INT i
	FOR i = 0 TO 3
		IF NOT IS_PED_INJURED(pedPlayerCut[i])
			SET_ENTITY_VISIBLE(pedPlayerCut[i], bVisible)
		ENDIF
	ENDFOR
	
//	IF IS_VEHICLE_DRIVEABLE(vehTutStart)
//		SET_ENTITY_VISIBLE(vehTutStart, bVisible)
//	ENDIF
ENDPROC

PED_INDEX pedLamarForCut
VEHICLE_INDEX vehLamarForcut
FUNC BOOL CREATE_LAMAR_FOR_TUTORIAL_MISSION_CUT()
	
	REQUEST_MODEL(EMPEROR)
	REQUEST_NPC_PED_MODEL(CHAR_LAMAR)
	IF NOT DOES_ENTITY_EXIST(vehLamarForcut)
		IF HAS_MODEL_LOADED(EMPEROR)
			vehLamarForcut = CREATE_VEHICLE(EMPEROR, <<364.6949, 287.5316, 102.3959>>, 342.0867, FALSE, FALSE)
			SET_VEHICLE_COLOURS(vehLamarForcut, 0, 3)
			SET_VEHICLE_EXTRA_COLOURS(vehLamarForcut, 0, 156)
			SET_MODEL_AS_NO_LONGER_NEEDED(EMPEROR)
			SET_ENTITY_VISIBLE(vehLamarForcut, FALSE)
			PRINTLN("[dsw] [DO_TUTORIAL_MISSION_CUTSCENE] [CREATE_LAMAR_FOR_TUTORIAL_MISSION_CUT] Created Lamar's car") 
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(vehLamarForcut)
		IF IS_VEHICLE_DRIVEABLE(vehLamarForcut)
			IF HAS_MODEL_LOADED(GET_NPC_PED_MODEL(CHAR_LAMAR))
				
				CREATE_NPC_PED_INSIDE_VEHICLE(pedLamarForCut, CHAR_LAMAR, vehLamarForcut)
				SET_VEHICLE_DOORS_LOCKED(vehLamarForcut, VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
				SET_ENTITY_INVINCIBLE(pedLamarForCut, TRUE)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedLamarForCut, TRUE)
				SET_ENTITY_VISIBLE(pedLamarForCut, FALSE)
				PRINTLN("[dsw] [DO_TUTORIAL_MISSION_CUTSCENE] [CREATE_LAMAR_FOR_TUTORIAL_MISSION_CUT] Created Lamar") 
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(pedLamarForCut)
		RETURN FALSE
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(vehLamarForcut)
		RETURN FALSE
	ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(vehLamarForcut)
		IF NOT HAVE_VEHICLE_MODS_STREAMED_IN(vehLamarForcut)
			RETURN FALSE
		ENDIF
	ENDIF
	RETURN TRUE
ENDFUNC

SCRIPT_TIMER timeTutMocap
SCRIPT_TIMER timeLamar
PROC MAINTAIN_LAMAR_FOR_TUTORIAL_CUTSCENE()
	ENTITY_INDEX ent
	IF IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL()
		IF NOT IS_PED_INJURED(pedLamarForCut)
			IF IS_VEHICLE_DRIVEABLE(vehLamarForcut)
				IF NOT IS_BIT_SET(iTutorialMissionCutBitset, biTut_GivenDriveTask)
					IF HAS_NET_TIMER_STARTED(timeTutMocap)
						IF HAS_NET_TIMER_EXPIRED(timeTutMocap, 48000)
						OR IS_BIT_SET(iTutorialMissionCutBitset, biTut_StartScriptCam)
							SET_ENTITY_VISIBLE(pedLamarForCut, TRUE)
							SET_ENTITY_VISIBLE(vehLamarForcut, TRUE)
							START_NET_TIMER(timeLamar)
							TASK_VEHICLE_MISSION_COORS_TARGET(pedLamarForCut,vehLamarForcut, <<324.8459, 325.0962, 104.3525>>, MISSION_GOTO, 15.0, DrivingMode_StopForCars|DF_AdjustCruiseSpeedBasedOnRoadSpeed, 5, 5, FALSE)
							SET_BIT(iTutorialMissionCutBitset, biTut_GivenDriveTask)
							PRINTLN("[dsw] [MAINATIN_LAMAR_FOR_TUTORIAL_CUTSCENE] Given Lamar drive task")
						ENDIF
					ENDIF
				ELSE
					// Given drive task 
					IF NOT IS_BIT_SET(iTutorialMissionCutBitset, biTut_CleanedUpLamar)
						IF IS_ENTITY_IN_ANGLED_AREA( pedLamarForCut, <<349.868286,329.370422,102.323898>>, <<344.610474,306.641571,108.900757>>, 11.437500)
						OR HAS_NET_TIMER_EXPIRED(timeLamar, 30000)
							IF DOES_ENTITY_EXIST(pedLamarForCut)
								ent = pedLamarForCut
								DELETE_ENTITY(ent)
							ENDIF
							IF DOES_ENTITY_EXIST(vehLamarForcut)
								ent = vehLamarForcut
								DELETE_ENTITY(ent)
							ENDIF
							SET_BIT(iTutorialMissionCutBitset, biTut_CleanedUpLamar)
							PRINTLN("[dsw] [MAINATIN_LAMAR_FOR_TUTORIAL_CUTSCENE] Cleaned up Lamar")
						ENDIF
					ENDIF
				ENDIF	
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC TEXT_LABEL GET_TUTORIAL_MOCAP_HANDLE_FOR_INDEX(INT index)
//	PRINTLN("[dsw] [GET_TUTORIAL_MOCAP_HANDLE_FOR_INDEX] called with index = ", index)
	TEXT_LABEL tl
	IF index = indexLocalPlayer
		tl = "MP_1"
	ELIF index = indexMp2
		tl = "MP_2"
	ELIF index = indexMp3
		tl = "MP_3"
	ELIF index = indexMp4
		tl = "MP_4"
	ENDIF
//	PRINTLN("[dsw] [GET_TUTORIAL_MOCAP_HANDLE_FOR_INDEX] returning handle = ", tl)
	
	RETURN tl
ENDFUNC

PROC MAINTAIN_TUTORIAL_CUT_PEDS_ENTER_VEHICLE()
	INT i
	VEHICLE_SEAT vehSeat
	TEXT_LABEL tlHandle
	FOR i = 0 TO iNumPlayersOnMission - 1
		IF IS_VEHICLE_DRIVEABLE(vehTutStart)
			IF NETWORK_IS_PLAYER_ACTIVE(playerForCut[i])
				IF IS_NET_PLAYER_OK(playerForCut[i], FALSE)
					
					IF NOT IS_PED_INJURED(pedPlayerCut[i])
						IF NOT IS_BIT_SET(iTutorialMissionCutBitset, biTut_PedIntoCar0 + i)
							IF NOT IS_BIT_SET(iTutorialMissionCutBitset, biTut_SetExitState0 + i)
								tlHandle = GET_TUTORIAL_MOCAP_HANDLE_FOR_INDEX(i)
								IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY(tlHandle)
								OR CAN_SET_EXIT_STATE_FOR_CAMERA()
									SET_BIT(iTutorialMissionCutBitset, biTut_SetExitState0 + i)
									PRINTLN("[dsw] [MAINTAIN_TUTORIAL_CUT_PEDS_ENTER_VEHICLE] Can set exit state for handle ", tlHandle)
								ENDIF
							ENDIF
							
							IF IS_BIT_SET(iTutorialMissionCutBitset, biTut_SetExitState0 + i)
								IF IS_PLAYER_DRIVER_OF_TUTORIAL_MISSION_CAR(NATIVE_TO_INT(playerForCut[i]))
									PRINTLN("[dsw] [MAINTAIN_TUTORIAL_CUT_PEDS_ENTER_VEHICLE] Think player is driver, i = ", i, " Player name = ", GET_PLAYER_NAME(playerForCut[i]))
									SET_ENTITY_COORDS(pedPlayerCut[i],<<369.1088, 279.5931, 102.2169>>  )
									SET_ENTITY_HEADING(pedPlayerCut[i], 94.9746)
									
									TASK_ENTER_VEHICLE( pedPlayerCut[i], vehTutStart, 50000, VS_DRIVER, PEDMOVEBLENDRATIO_WALK)
									FORCE_PED_MOTION_STATE(pedPlayerCut[i], MS_ON_FOOT_WALK)
									FORCE_PED_AI_AND_ANIMATION_UPDATE(LocalPlayerPed)
								ELSE
									PRINTLN("[dsw] [MAINTAIN_TUTORIAL_CUT_PEDS_ENTER_VEHICLE] Think player is NOT driver, i = ", i, " Player name = ", GET_PLAYER_NAME(playerForCut[i]))
									vehSeat = GET_PLAYER_VEHICLE_SEAT_FOR_TUTORIAL_MISSION(NATIVE_TO_INT(playerForCut[i]))
									IF vehSeat = VS_FRONT_RIGHT
										SET_ENTITY_COORDS(pedPlayerCut[i],<<371.0742, 275.4869, 102.1413>>)
										SET_ENTITY_HEADING(pedPlayerCut[i], 50.5932)
										
									ELIF vehSeat = VS_BACK_RIGHT
										SET_ENTITY_COORDS(pedPlayerCut[i],<<369.4283, 275.0704, 102.1546>>) 
										SET_ENTITY_HEADING(pedPlayerCut[i], 52.0473)
									 
									ELIF vehSeat = VS_BACK_LEFT
										SET_ENTITY_COORDS(pedPlayerCut[i], <<367.0331, 274.4661, 102.1555>>) 
										SET_ENTITY_HEADING(pedPlayerCut[i], 60.9009 )
									 
									ENDIF
									TASK_ENTER_VEHICLE( pedPlayerCut[i], vehTutStart, 50000, vehSeat, PEDMOVEBLENDRATIO_WALK)
									FORCE_PED_MOTION_STATE(pedPlayerCut[i], MS_ON_FOOT_WALK)
									FORCE_PED_AI_AND_ANIMATION_UPDATE(pedPlayerCut[i])
									PRINTLN("[dsw] [MAINTAIN_TUTORIAL_CUT_PEDS_ENTER_VEHICLE] ped i = ", i, " using seat ", ENUM_TO_INT(vehSeat))
								//	TASK_ENTER_VEHICLE( pedPlayerCut[i], vehTutStart, 1, GET_PLAYER_VEHICLE_SEAT_FOR_TUTORIAL_MISSION(NATIVE_TO_INT(playerForCut[i])), PEDMOVEBLENDRATIO_RUN, ECF_WARP_PED )
								//	FREEZE_ENTITY_POSITION(pedPlayerCut[i], TRUE)
								ENDIF
								SET_BIT(iTutorialMissionCutBitset, biTut_PedIntoCar0 + i)
								SET_PED_RESET_FLAG(pedPlayerCut[i],PRF_IgnoreVehicleEntryCollisionTests , TRUE)
							ENDIF	
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
ENDPROC


//


FUNC BOOL CREATE_ALL_PLAYER_PEDS_FOR_TUTORIAL_MISSION_CUT()
	INT i
	VECTOR vCreate
	FLOAT fHead
	//VEHICLE_SEAT vehSeat
	PRINTLN("[dsw] [CREATE_ALL_PLAYER_PEDS_FOR_TUTORIAL_MISSION_CUT] Called with iNumPlayersOnMission = ", iNumPlayersOnMission)
	FOR i = 0 TO iNumPlayersOnMission - 1
		IF NETWORK_IS_PLAYER_ACTIVE(playerForCut[i])
			IF IS_NET_PLAYER_OK(playerForCut[i], FALSE)
				IF NOT DOES_ENTITY_EXIST(pedPlayerCut[i])
					IF mPlayers[i] <> DUMMY_MODEL_FOR_SCRIPT
						
						IF HAS_MODEL_LOADED(mPlayers[i])
							IF NOT IS_PED_INJURED(GET_PLAYER_PED(playerForCut[i]))
								PRINTLN("[dsw] [CREATE_ALL_PLAYER_PEDS_FOR_TUTORIAL_MISSION_CUT] Recreating player... ", GET_PLAYER_NAME(playerForCut[i]))
								GET_CREATE_POSITION_FOR_TUTORIAL_MISSION_PED(i, vCreate, fHead)
								IF GET_ENTITY_MODEL(GET_PLAYER_PED(playerForCut[i])) = MP_M_FREEMODE_01
									pedPlayerCut[i] = CREATE_PED(PEDTYPE_CIVMALE, mPlayers[i],vCreate, fHead, FALSE, FALSE)
									PRINTLN("[dsw] [CREATE_ALL_PLAYER_PEDS_FOR_TUTORIAL_MISSION_CUT] Created this player as male = ", i)
								ELSE
									pedPlayerCut[i] = CREATE_PED(PEDTYPE_CIVFEMALE, mPlayers[i],vCreate, fHead, FALSE, FALSE)
									PRINTLN("[dsw] [CREATE_ALL_PLAYER_PEDS_FOR_TUTORIAL_MISSION_CUT] Created this player as male = ", i)
								ENDIF
								CLONE_PED_TO_TARGET(GET_PLAYER_PED(playerForCut[i]), pedPlayerCut[i])
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedPlayerCut[i], TRUE)
								SET_PED_RELATIONSHIP_GROUP_HASH(pedPlayerCut[i], rgFM_AiLike)

							ENDIF
						ENDIF
					ELSE	
						PRINTLN("[dsw] [CREATE_ALL_PLAYER_PEDS_FOR_TUTORIAL_MISSION_CUT] mPlayers = DUMMY_MODEL_FOR_SCRIPT! i = ", i)
					ENDIF
				ENDIF
			ELSE
				PRINTLN("[dsw] [CREATE_ALL_PLAYER_PEDS_FOR_TUTORIAL_MISSION_CUT] Player not ok! i = ", i)
			ENDIF
		ELSE	
			PRINTLN("[dsw] [CREATE_ALL_PLAYER_PEDS_FOR_TUTORIAL_MISSION_CUT] Player not active! i = ", i)
		ENDIF
	ENDFOR
	
	FOR i = 0 TO iNumPlayersOnMission - 1
		IF NETWORK_IS_PLAYER_ACTIVE(playerForCut[i])
			IF IS_NET_PLAYER_OK(playerForCut[i], FALSE)
				IF NOT DOES_ENTITY_EXIST(pedPlayerCut[i])
					PRINTLN("[dsw] [CREATE_ALL_PLAYER_PEDS_FOR_TUTORIAL_MISSION_CUT] Still waiting to create ped i = ", i)
					RETURN FALSE
				ELSE	
					IF NOT IS_PED_INJURED(pedPlayerCut[i])
						IF NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedPlayerCut[i])	
							PRINTLN("[dsw] [CREATE_ALL_PLAYER_PEDS_FOR_TUTORIAL_MISSION_CUT] Still waiting for streaming requests for ped i = ", i)
							RETURN FALSE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN TRUE
ENDFUNC

PROC SETUP_PLAYER_SEATS_FOR_TUTORIAL_MISSION()
	PRINTLN("[dsw] [DO_TUTORIAL_MISSION_CUTSCENE] Counting players ")
	
	INT i
	INT iPedCount
	INT iSeatCount
	PARTICIPANT_INDEX part
	PLAYER_INDEX playerTemp
	iPedCount = 0
	iSeatCount = 0
	iDriverPlayerIndex = -1
	iDriverPlayerIndex = GET_DRIVER_OF_MISSION_VEH()
	FOR i = 0 TO 3
		mPlayers[i] = DUMMY_MODEL_FOR_SCRIPT
	ENDFOR
	
	FOR i = 0 TO NETWORK_GET_MAX_NUM_PARTICIPANTS() - 1 
		part = INT_TO_PARTICIPANTINDEX(i)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(part)
			playerTemp = NETWORK_GET_PLAYER_INDEX(part)
			IF IS_NET_PLAYER_OK(playerTemp)
				IF iNumPlayersOnMission < 4
					PRINTLN("[dsw] [DO_TUTORIAL_MISSION_CUTSCENE] Think this player is ready for cut ", GET_PLAYER_NAME(playerTemp))
					PRINTLN("[dsw] [DO_TUTORIAL_MISSION_CUTSCENE] Allocating seats...")
					IF IS_PLAYER_DRIVER_OF_TUTORIAL_MISSION_CAR(NATIVE_TO_INT(playerTemp))
						vehSeatToUse[iPedCount] = VS_DRIVER
						PRINTLN("[dsw] [DO_TUTORIAL_MISSION_CUTSCENE] Think this player is driver ", GET_PLAYER_NAME(playerTemp))
					ELSE
						vehSeatToUse[iPedCount] = INT_TO_ENUM(VEHICLE_SEAT, iSeatCount)
						
						PRINTLN("[dsw] [DO_TUTORIAL_MISSION_CUTSCENE] This player ", GET_PLAYER_NAME(playerTemp), " will use seat ", iSeatCount)
						iSeatCount++
					ENDIF
					mPlayers[iPedCount] = GET_ENTITY_MODEL(GET_PLAYER_PED(playerTemp))
					playerForCut[iPedCount] = playerTemp
					REQUEST_MODEL(mPlayers[iPedCount])
					iPedCount++
					iNumPlayersOnMission++
					
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
ENDPROC
//PURPOSE: Custcene which plays at the start of the tutorial mission
FUNC BOOL DO_TUTORIAL_MISSION_CUTSCENE()
	INT i
//	INT iPedCount
//	INT iSeatCount
//	PARTICIPANT_INDEX part
//	PLAYER_INDEX playerTemp
	INT iPlayVehIndex = 0 //3 See also SET_VEHICLE_TO_USE_FOR_TUTORIAL_MISSION in FM_TRIGGER_TUT.SCH
	MODEL_NAMES mVeh
	VECTOR vRgb
//	PED_INDEX pedPlayer
	//-- Think veh 3 is the mission start vehicle
	
	IF iTutorialMissionProg < 7
		HIDE_HUD_AND_RADAR_THIS_FRAME()
	ENDIF
	IF bLocalPlayerPedOk
		SET_PED_RESET_FLAG(LocalPlayerPed, PRF_CannotBeTargetedByAI, TRUE)
	ENDIF	
	
	SET_ALL_MISSION_ENTITIES_VISIBLITY(MC_serverBD_1.sFMMC_SBD, oiProps, oiPropsChildren, TRUE)
	
	SWITCH iTutorialMissionProg
		CASE 0
			IF IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL()
				IF IS_BIT_SET(iTutorialMissionCutBitset, biTut_StartScriptedSceneSetup)	
					IF IS_BIT_SET(MC_serverBD.iServerBitSet,SBBOOL_ALL_JOINED_START )
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iPlayVehIndex])
							IF IS_PLAYER_OK_TO_START_MP_CUTSCENE()

									
									IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.bPreTutorialMissionInFlow
										g_TransitionSessionNonResetVars.sPostMissionCleanupData.bPreTutorialMissionInFlow = FALSE
										#IF IS_DEBUG_BUILD
											PRINTLN("[dsw] [wjk] [DO_TUTORIAL_MISSION_CUTSCENE] g_TransitionSessionNonResetVars.sPostMissionCleanupData.bPreTutorialMissionInFlow = FALSE")
										#ENDIF
									ENDIF
									
									MAKE_PLAYER_SAFE_FOR_MP_CUTSCENE(FALSE)
									START_MP_CUTSCENE()
									
									IF NOT DOES_ENTITY_EXIST(vehTutStart)
										IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iPlayVehIndex])
											
											
											GET_VEHICLE_SETUP_MP(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iPlayVehIndex]), vehSetupOriginal)

											mVeh = GET_ENTITY_MODEL(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iPlayVehIndex]))
											PRINTLN("[dsw] [DO_TUTORIAL_MISSION_CUTSCENE] Start vehicle exists. ")
											REQUEST_MODEL(mVeh) 
											PRINTLN("[dsw] [DO_TUTORIAL_MISSION_CUTSCENE] requested vehicle model ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(mVeh))
											
			//								IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iPlayVehIndex].mn <> DUMMY_MODEL_FOR_SCRIPT
			//									REQUEST_MODEL(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iPlayVehIndex].mn)
			//									PRINTLN("[dsw] [DO_TUTORIAL_MISSION_CUTSCENE] requested vehicle model ", GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iPlayVehIndex].mn))
			//								ELSE	
			//									PRINTLN("[dsw] [DO_TUTORIAL_MISSION_CUTSCENE] Start vehicle DOES NOT exist.")
			//								ENDIF
										ELSE	
											PRINTLN("[dsw] [DO_TUTORIAL_MISSION_CUTSCENE] Start vehicle DOES NOT exist.")
										ENDIF
									ELSE
										PRINTLN("[dsw] [DO_TUTORIAL_MISSION_CUTSCENE] cutscene vehicle already exists.")
									ENDIF

									PRINTLN("[dsw] [DO_TUTORIAL_MISSION_CUTSCENE] Think there's this many players ", iNumPlayersOnMission)
									
									
									iTutorialMissionProg++
									PRINTLN("[dsw] [DO_TUTORIAL_MISSION_CUTSCENE] iTutorialMissionProg = ", iTutorialMissionProg)

							ENDIF
						ELSE
							PRINTLN("[dsw] [DO_TUTORIAL_MISSION_CUTSCENE] Waiting for vehicle to exist") 
						ENDIF
					ELSE	
						PRINTLN("[dsw] [DO_TUTORIAL_MISSION_CUTSCENE] Waiting for SBBOOL_ALL_JOINED_START") 
					ENDIF
				ENDIF
			ELSE
				iTutorialMissionProg = 100
				PRINTLN("[dsw] [DO_TUTORIAL_MISSION_CUTSCENE] Not running tutorial iTutorialMissionProg = ", iTutorialMissionProg)
			ENDIF
		BREAK
		
		CASE 1
			IF NOT DOES_ENTITY_EXIST(vehTutStart)
//				mVeh = GET_ENTITY_MODEL(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iPlayVehIndex]))
//				IF HAS_MODEL_LOADED(mVeh)
//				AND HAVE_ALL_MODELS_LOADED_FOR_TUTORIAL_MISSION_CUT()
//					vehTutStart = CREATE_VEHICLE(mVeh, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iPlayVehIndex].vPos, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iPlayVehIndex].fHead, FALSE, FALSE)
//					
//					
//					
//					
//					vRgb = << GlobalplayerBD_FM[iDriverPlayerIndex].fmRaceTutorial.iCrewRed, 
//							GlobalplayerBD_FM[iDriverPlayerIndex].fmRaceTutorial.iCrewGreen, 
//							GlobalplayerBD_FM[iDriverPlayerIndex].fmRaceTutorial.iCrewBlue >>
//							
//					SET_VEHICLE_SETUP(vehTutStart,vehSetupOriginal) 
//					SET_VEHICLE_ENGINE_ON(vehTutStart, TRUE, TRUE)
//					SET_ENTITY_VISIBLE(vehTutStart, FALSE)
//					IF iDriverPlayerIndex > -1
//						IF GlobalplayerBD_FM[iDriverPlayerIndex].fmRaceTutorial.iLivery > -1
//						OR (GlobalplayerBD_FM[iDriverPlayerIndex].fmRaceTutorial.iLivery = -1 AND NOT ARE_VECTORS_EQUAL(vRgb, <<0.0, 0.0, 0.0>>))
//							SET_UP_TUTORIAL_VEHICLE_COLOURS(vehTutStart, 
//															GlobalplayerBD_FM[iDriverPlayerIndex].fmRaceTutorial.iLivery, 
//															GlobalplayerBD_FM[iDriverPlayerIndex].fmRaceTutorial.modColourType,
//															GlobalplayerBD_FM[iDriverPlayerIndex].fmRaceTutorial.iCrewRed,
//															GlobalplayerBD_FM[iDriverPlayerIndex].fmRaceTutorial.iCrewGreen,
//															GlobalplayerBD_FM[iDriverPlayerIndex].fmRaceTutorial.iCrewBlue)
//															
//							PRINTLN("[dsw] [DO_TUTORIAL_MISSION_CUTSCENE] Set cutscene vehicle colours") 
//							
//						ELSE
//							PRINTLN("[dsw] [DO_TUTORIAL_MISSION_CUTSCENE] Tried to set vehicle colours, but GlobalplayerBD_FM[iDriverPlayerIndex].fmRaceTutorial.iLivery = ", GlobalplayerBD_FM[iDriverPlayerIndex].fmRaceTutorial.iLivery) 
//						ENDIF
//					ELSE
//						PRINTLN("[dsw] [DO_TUTORIAL_MISSION_CUTSCENE] Tried to set vehicle colours, but iDriverPlayerIndex = ", iDriverPlayerIndex) 
//					ENDIF
//					
//					SETUP_AMBIENT_RACECARS()
//				//	SET_ENTITY_COLLISION(vehTutStart, FALSE)
//					PRINTLN("[dsw] [DO_TUTORIAL_MISSION_CUTSCENE] Created vehTutStart") 
//					iTutorialMissionProg++
//					PRINTLN("[dsw] [DO_TUTORIAL_MISSION_CUTSCENE] iTutorialMissionProg = ", iTutorialMissionProg)
//				ELSE	
//					#IF IS_DEBUG_BUILD
//						IF NOT HAS_MODEL_LOADED(mVeh)
//							PRINTLN("[dsw] [DO_TUTORIAL_MISSION_CUTSCENE] mVeh not loaded! Model is ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(mVeh)) 
//						ENDIF
//						
//						IF NOT HAVE_ALL_MODELS_LOADED_FOR_TUTORIAL_MISSION_CUT()
//							PRINTLN("[dsw] [DO_TUTORIAL_MISSION_CUTSCENE] Still waiting for HAVE_ALL_MODELS_LOADED_FOR_TUTORIAL_MISSION_CUT")
//						ENDIF
//					#ENDIF
//				ENDIF
			ELSE
				
				iTutorialMissionProg++
				PRINTLN("[dsw] [DO_TUTORIAL_MISSION_CUTSCENE] vehTutStart alreadt exists! iTutorialMissionProg = ", iTutorialMissionProg)
			ENDIF
		BREAK
		
		CASE 2
			IF CREATE_ALL_PLAYER_PEDS_FOR_TUTORIAL_MISSION_CUT()
				IF CREATE_LAMAR_FOR_TUTORIAL_MISSION_CUT()
				//	SET_TUTORIAL_CUTSCENE_ENTITIES_VISIBLE(FALSE)
					PRINTLN("[dsw] [DO_TUTORIAL_MISSION_CUTSCENE] Created All player peds") 
					iTutorialMissionProg++
					PRINTLN("[dsw] [DO_TUTORIAL_MISSION_CUTSCENE] iTutorialMissionProg = ", iTutorialMissionProg)
				ELSE
					PRINTLN("[dsw] [DO_TUTORIAL_MISSION_CUTSCENE] Waiting for CREATE_LAMAR_FOR_TUTORIAL_MISSION_CUT") 
				ENDIF
			ENDIF
		BREAK
		CASE 3
			//-- Cleanup any of Ben's intro cam stuff
			
			IF DOES_CAM_EXIST(g_CamTutorial)
				
				SET_CAM_ACTIVE(g_CamTutorial, FALSE)
				PRINTLN("[dsw] [DO_TUTORIAL_MISSION_CUTSCENE] g_CamTutorial exists, setting not active ")
			ENDIF
			
//			PRINTLN("[dsw] [DO_TUTORIAL_MISSION_CUTSCENE] Calling CLEAN_UP_INTRO_STAGE_CAM ")
//			CLEAN_UP_INTRO_STAGE_CAM(g_sTransitionSessionData.ciCam,FMMC_TYPE_MISSION,TRUE)
//			CLEANUP_ALL_CORONA_FX()
//			SET_SKYFREEZE_CLEAR()
			
			
								
			camTut= CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", FALSE) 
			SET_CAM_ACTIVE(camTut, TRUE)
									
			SET_CAM_PARAMS(camTut, <<368.2373, 269.5873, 104.3782>>, <<-4.1860, -0.0000, 1.9618>>,  50.0000)
		//	SET_CAM_PARAMS(camTut, <<388.3285, 264.8162, 104.1024>>, <<-8.5109, 0.0000, 88.1782>>, 32.6336, 30000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)	
			
//			RENDER_SCRIPT_CAMS(TRUE, FALSE)
//			UNFREEZE_ALL_PEDS_FOR_TUTORIAL_MISSION()
//			DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
//			START_NET_TIMER(timeTutMission)
			iTutorialMissionProg++
			PRINTLN("[dsw] [DO_TUTORIAL_MISSION_CUTSCENE] iTutorialMissionProg = ", iTutorialMissionProg)
		BREAK
		
		CASE 4
			IF IS_BIT_SET(iTutorialMissionCutBitset, biTut_StartScriptCam)
				SET_TUTORIAL_CUTSCENE_ENTITIES_VISIBLE(TRUE)
				SET_CAM_PARAMS(camTut, <<366.2780, 270.8071, 104.0242>>, <<-1.4772, 0.0000, -13.3879>>, 50.0000, 8500, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
				UNFREEZE_ALL_PEDS_FOR_TUTORIAL_MISSION()
			//	SET_ALL_TUTORIAL_CUT_PEDS_ENTER_VEHICLE()
				START_NET_TIMER(timeTutMission)
				iTutorialMissionProg++
				PRINTLN("[dsw] [DO_TUTORIAL_MISSION_CUTSCENE] iTutorialMissionProg = ", iTutorialMissionProg)
			ENDIF
		BREAK
		
		CASE 5
			IF HAS_NET_TIMER_EXPIRED(timeTutMission, 7000)
			#IF IS_DEBUG_BUILD OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_RIGHT) #ENDIF
				IF ARE_ALL_CUTSCENE_PEDS_IN_CAR()
				OR HAS_NET_TIMER_EXPIRED(timeTutMission, 15000)
				#IF IS_DEBUG_BUILD OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_RIGHT) #ENDIF
				 	IF IS_BIT_SET(iTutorialMissionCutBitset, biTut_CleanedUpLamar)
				
						SET_BIT(MC_playerBD[iLocalPart].iTutorialBitset, TUTORIAL_PLAYER_FINISHED_TUTORIAL_CUT)
						PRINTLN("[dsw] [DO_TUTORIAL_MISSION_CUTSCENE] Setting TUTORIAL_PLAYER_FINISHED_TUTORIAL_CUT as I've finished cut")
						
						IF NOT IS_BIT_SET(iTutorialMissionCutBitset, biTut_UnmuteRadio)
							STOP_AUDIO_SCENE("MP_JOB_CHANGE_RADIO_MUTE")
							PRINTLN("[dsw] [DO_TUTORIAL_MISSION_CUTSCENE] Called STOP_AUDIO_SCENE - MP_JOB_CHANGE_RADIO_MUTE ")
						
							SET_BIT(iTutorialMissionCutBitset, biTut_UnmuteRadio)
						ENDIF
				
						iTutorialMissionProg++
						PRINTLN("[dsw] [DO_TUTORIAL_MISSION_CUTSCENE] iTutorialMissionProg = ", iTutorialMissionProg)
					ELSE
						PRINTLN("[dsw] [DO_TUTORIAL_MISSION_CUTSCENE] Waiting for biTut_CleanedUpLamar")
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 6
			IF HAS_EVERYONE_FINISHED_TUTORIAL_CUT()
			#IF IS_DEBUG_BUILD OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_RIGHT) #ENDIF
				ENTITY_INDEX ent 
				FOR i = 0 TO 3
					IF DOES_ENTITY_EXIST(pedPlayerCut[i])
						ent = pedPlayerCut[i]
						DELETE_ENTITY(ent)
					ENDIF
				ENDFOR
				
				IF DOES_ENTITY_EXIST(vehTutStart)
					ent = vehTutStart
					DELETE_ENTITY(ent)
				ENDIF
				
//				IF DOES_CAM_EXIST(camTut)
//					SET_CAM_ACTIVE(camTut, FALSE)
//				ENDIF
				CLEANUP_MP_CUTSCENE()
				
				
				
				SET_FRONTEND_RADIO_ACTIVE(TRUE)
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iPlayVehIndex])
					IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iPlayVehIndex])
						IF IS_VEHICLE_DRIVEABLE(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iPlayVehIndex]))
							SET_VEHICLE_RADIO_ENABLED(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iPlayVehIndex]), TRUE)
							SET_VEH_RADIO_STATION(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iPlayVehIndex]), "RADIO_07_DANCE_01")
							PRINTLN("[dsw] [DO_TUTORIAL_MISSION_CUTSCENE] Set radio to Soulwax ")
							VECTOR vTemp
							vTemp = GET_ENTITY_COORDS(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iPlayVehIndex]))
							
							IF vTemp.z > 103.0
								SET_VEHICLE_ON_GROUND_PROPERLY(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iPlayVehIndex]))
								
								PRINTLN("[dsw] [DO_TUTORIAL_MISSION_CUTSCENE] Car not on ground! ")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				
				
				SET_BIT(iTutorialMissionCutBitset, biTut_RadioOff)
				PRINTLN("[dsw] [DO_TUTORIAL_MISSION_CUTSCENE] Calling CLEAR_AREA ")
				CLEAR_AREA_LEAVE_VEHICLE_HEALTH(<<366.2780, 270.8071, 104.0242>>,300.0,TRUE)

				IF bLocalPlayerPedOk
					IF IS_VEHICLE_DRIVEABLE(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iPlayVehIndex]))
					
						IF NOT IS_PED_SITTING_IN_VEHICLE(LocalPlayerPed, NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iPlayVehIndex]))
							PRINTLN("[dsw] [DO_TUTORIAL_MISSION_CUTSCENE] Cutscene finishing and I'm not in the mission car! ")
							PUT_PLAYER_INTO_MISSION_CAR(iPlayVehIndex)
						ENDIF
					ENDIF
					
					SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_PreventAutoShuffleToDriversSeat, FALSE)
				ENDIF
				
				
				SET_ROADS_BACK_TO_ORIGINAL_IN_ANGLED_AREA(<<269.985535,333.320984,92.176788>>, <<419.747925,300.674927,111.001175>>, 10.625000)
				RENDER_SCRIPT_CAMS(FALSE, TRUE, 4000, FALSE, TRUE)
				RESET_NET_TIMER(timeTutMission)
				START_NET_TIMER(timeTutMission)
			
				

				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_LOOK_LR)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_LOOK_UD)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_LOOK_BEHIND) 
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_LOOK_BEHIND)
				
//				DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				iTutorialMissionProg = 99
				PRINTLN("[dsw] [DO_TUTORIAL_MISSION_CUTSCENE] iTutorialMissionProg = ", iTutorialMissionProg)
			ENDIF
			
		BREAK
		
		CASE 7
			IF NOT IS_INTERPOLATING_FROM_SCRIPT_CAMS()
			OR HAS_NET_TIMER_EXPIRED(timeTutMission, 10000)
				#IF IS_DEBUG_BUILD
					IF HAS_NET_TIMER_EXPIRED(timeTutMission, 10000)
						PRINTLN("[dsw] [DO_TUTORIAL_MISSION_CUTSCENE] Case 7 Timer expired!")
					ENDIF
					
					IF NOT IS_INTERPOLATING_FROM_SCRIPT_CAMS()
						PRINTLN("[dsw] [DO_TUTORIAL_MISSION_CUTSCENE] No longer interpolating!")
					ENDIF
				#ENDIF
				IF DOES_CAM_EXIST(camTut)
					SET_CAM_ACTIVE(camTut, FALSE)
				ENDIF
				
				DESTROY_ALL_CAMS()
				
				iTutorialMissionProg = 99
				PRINTLN("[dsw] [DO_TUTORIAL_MISSION_CUTSCENE] iTutorialMissionProg = ", iTutorialMissionProg)
			ELSE
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_LOOK_LR)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_LOOK_UD)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_LOOK_BEHIND) 
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_LOOK_BEHIND)
			ENDIF
				
		BREAK
		
		
		
		CASE 99
			IF IS_SCREEN_FADED_IN()
				iTutorialMissionProg = 100
				PRINTLN("[dsw] [DO_TUTORIAL_MISSION_CUTSCENE] Not running tutorial iTutorialMissionProg = ", iTutorialMissionProg)
			ENDIF
		BREAK
		CASE 100
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	IF IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL()
		MAINTAIN_LAMAR_FOR_TUTORIAL_CUTSCENE()
		
		//-- Set the colours of the mission vehicle
		IF iTutorialMissionProg < 100
			IF NOT IS_BIT_SET(iTutorialMissionCutBitset, biTutCut_SetMissVehColours)
				IF iDriverPlayerIndex > -1
					vRgb = << GlobalplayerBD_FM[iDriverPlayerIndex].fmRaceTutorial.iCrewRed, 
							GlobalplayerBD_FM[iDriverPlayerIndex].fmRaceTutorial.iCrewGreen, 
							GlobalplayerBD_FM[iDriverPlayerIndex].fmRaceTutorial.iCrewBlue >>
							
					IF GlobalplayerBD_FM[iDriverPlayerIndex].fmRaceTutorial.iLivery > -1
					OR (GlobalplayerBD_FM[iDriverPlayerIndex].fmRaceTutorial.iLivery = -1 
						AND NOT ARE_VECTORS_EQUAL(vRgb, <<0.0, 0.0, 0.0>>))	
						
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iPlayVehIndex])
							IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iPlayVehIndex])
								PRINTLN("[dsw] [DO_TUTORIAL_MISSION_CUTSCENE] Setting up MIssion vehicle colours with iPlayVehIndex = ", iPlayVehIndex," and iDriverPlayerIndex = ", iDriverPlayerIndex) 
								
								SET_UP_TUTORIAL_VEHICLE_COLOURS(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iPlayVehIndex]), 
																GlobalplayerBD_FM[iDriverPlayerIndex].fmRaceTutorial.iLivery, 
																GlobalplayerBD_FM[iDriverPlayerIndex].fmRaceTutorial.modColourType,
																GlobalplayerBD_FM[iDriverPlayerIndex].fmRaceTutorial.iCrewRed,
																GlobalplayerBD_FM[iDriverPlayerIndex].fmRaceTutorial.iCrewGreen,
																GlobalplayerBD_FM[iDriverPlayerIndex].fmRaceTutorial.iCrewBlue)
								
								SET_BIT(iTutorialMissionCutBitset, biTutCut_SetMissVehColours)
								
								SET_VEH_RADIO_STATION(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iPlayVehIndex]), "OFF")
								
								PRINTLN("[dsw] [DO_TUTORIAL_MISSION_CUTSCENE] Set MISSION vehicle colours") 
							ELSE
							//	PRINTLN("[dsw] [DO_TUTORIAL_MISSION_CUTSCENE] Not Setting MISSION vehicle colours as I don't have control") 
							//	SET_BIT(iTutorialMissionCutBitset, biTutCut_SetMissVehColours)
						//		PRINTLN("[dsw] [DO_TUTORIAL_MISSION_CUTSCENE] Else 4 - don't have control") 
							ENDIF
						ELSE
							PRINTLN("[dsw] [DO_TUTORIAL_MISSION_CUTSCENE] Else 3") 
						ENDIF
					ELSE
						SET_BIT(iTutorialMissionCutBitset, biTutCut_SetMissVehColours)
						PRINTLN("[dsw] [DO_TUTORIAL_MISSION_CUTSCENE] Tried to set MISSION vehicle colours, but GlobalplayerBD_FM[iDriverPlayerIndex].fmRaceTutorial.iLivery = ", GlobalplayerBD_FM[iDriverPlayerIndex].fmRaceTutorial.iLivery) 
					ENDIF
				ELSE
					PRINTLN("[dsw] [DO_TUTORIAL_MISSION_CUTSCENE] Else 2") 
				ENDIF
			ELSE
			//	PRINTLN("[dsw] [DO_TUTORIAL_MISSION_CUTSCENE] Else 1") 
			ENDIF
			
			//-- Start walking the peds into the car
			IF IS_BIT_SET(iTutorialMissionCutBitset, biTUt_StartEnterVehicle)
				MAINTAIN_TUTORIAL_CUT_PEDS_ENTER_VEHICLE()
			ENDIF
			
			IF NOT IS_BIT_SET(iTutorialMissionCutBitset, biTut_RadioOff)
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iPlayVehIndex])
					IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iPlayVehIndex])
						IF IS_VEHICLE_DRIVEABLE(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iPlayVehIndex]))
							SET_VEH_RADIO_STATION(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iPlayVehIndex]), "OFF")
							SET_VEHICLE_RADIO_ENABLED(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iPlayVehIndex]), FALSE)
						//	SET_BIT(iTutorialMissionCutBitset, biTut_RadioOff)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(vehTutStart)
				SET_FORCE_HD_VEHICLE(vehTutStart, TRUE)
			ENDIF
		ENDIF
		
		IF iTutorialMissionProg > 0
		AND iTutorialMissionProg < 100
			FOR i = 0 TO 3
				IF NOT IS_PED_INJURED(pedPlayerCut[i])
					IF NOT IS_PED_SITTING_IN_ANY_VEHICLE(pedPlayerCut[i])
						SET_PED_RESET_FLAG(pedPlayerCut[i],PRF_IgnoreVehicleEntryCollisionTests , TRUE)	
					ENDIF
				ENDIF
			ENDFOR
			
			IF NOT IS_BIT_SET(iTutorialMissionCutBitset, biTutCut_WArpedPlayerIntoCar)
				IF AM_I_DRIVER_OF_TUTORIAL_MISSION_VEHICLE()
					PRINTLN("[dsw] [DO_TUTORIAL_MISSION_CUTSCENE] I'm driver, will warp into driver's seat")
					PUT_PLAYER_INTO_MISSION_CAR(iPlayVehIndex)
					SET_BIT(iTutorialMissionCutBitset, biTutCut_WArpedPlayerIntoCar)
				ELSE	
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iPlayVehIndex])
						IF IS_VEHICLE_DRIVEABLE(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iPlayVehIndex]))
						//	IF NOT IS_VEHICLE_SEAT_FREE(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iPlayVehIndex]), VS_DRIVER)
								PRINTLN("[dsw] [DO_TUTORIAL_MISSION_CUTSCENE] I'm not driver, warping to my seat...")
								SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_PreventAutoShuffleToDriversSeat, TRUE)
								PUT_PLAYER_INTO_MISSION_CAR(iPlayVehIndex)
								SET_BIT(iTutorialMissionCutBitset, biTutCut_WArpedPlayerIntoCar)
						//	ELSE
						//		PRINTLN("[dsw] [DO_TUTORIAL_MISSION_CUTSCENE] Not warping into car as driver seat still free")
						//	ENDIF
						ELSE
							PRINTLN("[dsw] [DO_TUTORIAL_MISSION_CUTSCENE] Warp fail else 2")
						ENDIF
					ELSE
						PRINTLN("[dsw] [DO_TUTORIAL_MISSION_CUTSCENE] Warp fail else 1")
					ENDIF
				ENDIF
			ENDIF
			

		ENDIF
	ENDIF 
	
	DISABLE_FRONTEND_THIS_FRAME()
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_TUTORIAL_CUSTSCENE_CLEANUP()
	IF IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL()
		IF DOES_CAM_EXIST(camTut)
			IF NOT IS_INTERPOLATING_FROM_SCRIPT_CAMS()
			OR HAS_NET_TIMER_EXPIRED(timeTutMission, 10000)
				#IF IS_DEBUG_BUILD
					IF HAS_NET_TIMER_EXPIRED(timeTutMission, 10000)
						PRINTLN("[dsw] [DO_TUTORIAL_MISSION_CUTSCENE] [PROCESS_TUTORIAL_CUSTSCENE_CLEANUP] Timer expired!")
					ENDIF
					
					IF NOT IS_INTERPOLATING_FROM_SCRIPT_CAMS()
						PRINTLN("[dsw] [DO_TUTORIAL_MISSION_CUTSCENE] [PROCESS_TUTORIAL_CUSTSCENE_CLEANUP] No longer interpolating!")
					ENDIF
				#ENDIF
				IF DOES_CAM_EXIST(camTut)
					SET_CAM_ACTIVE(camTut, FALSE)
				ENDIF
				
				DESTROY_ALL_CAMS()
				
				SET_BIT(iLocalBoolCheck3,LBOOL3_MUSIC_PLAYING)
				
				SET_MISSION_MUSIC(g_FMMC_STRUCT.iAudioScore, FALSE)
				
				SET_MUSIC_STATE(MUSIC_SILENT)
				
				PRINTLN("[dsw] [DO_TUTORIAL_MISSION_CUTSCENE] [PROCESS_TUTORIAL_CUSTSCENE_CLEANUP] Done")
			ELSE
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_LOOK_LR)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_LOOK_UD)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_LOOK_BEHIND) 
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_LOOK_BEHIND)
			ENDIF
		ENDIF
	ENDIF
ENDPROC
//---------------------------------  Dave W - Tutorial mission intro mocap

#IF IS_DEBUG_BUILD
//---------------------------------  Dave W - Tutorial Debug functions
	

	PROC PRINT_TRIGGER_STRING(STRING sText)
		TEXT_LABEL_31 tl31
		IF NETWORK_IS_GAME_IN_PROGRESS()
			tl31 = GET_TIME_AS_STRING(GET_NETWORK_TIME())
		ENDIF
		CPRINTLN(DEBUG_MP_TUTORIAL,tl31 ," [dsw] [TriggerTut] ", sText)
	ENDPROC
	
	PROC PRINT_TRIGGER_STRING_WITH_PLAYER_NAME(STRING sText, PLAYER_INDEX player)
		TEXT_LABEL_31 tl31
		IF NETWORK_IS_GAME_IN_PROGRESS()
			tl31 = GET_TIME_AS_STRING(GET_NETWORK_TIME())
		ENDIF
		CPRINTLN(DEBUG_MP_TUTORIAL,tl31," [dsw] [TriggerTut] ", sText, GET_PLAYER_NAME(player))
	ENDPROC
	
	PROC PRINT_TRIGGER_STRING_INT(STRING sText, INT iToPrint)

		TEXT_LABEL_31 tl31
		IF NETWORK_IS_GAME_IN_PROGRESS()
			tl31 = GET_TIME_AS_STRING(GET_NETWORK_TIME())
		ENDIF
		CPRINTLN(DEBUG_MP_TUTORIAL,tl31," [dsw] [TriggerTut] ", sText, iToPrint)
	ENDPROC
#ENDIF

PED_INDEX pedForTutMocap[4]

PROC GET_CREATE_POSITION_FOR_TUTORIAL_INTRO_MOCAP_PED(INT index, VECTOR &vPos)
	SWITCH index
		CASE 0	vPos = <<360.8283, 269.0206, 102.0461>> BREAK
		CASE 1	vPos = <<359.1538, 269.6615, 102.0468>> BREAK
		CASE 2	vPos = <<357.3361, 270.0221, 102.0405>> BREAK
		CASE 3 	vPos = <<355.9884, 271.3311, 102.0583>> BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL CREATE_LOCAL_PLAYERS_FOR_TUTORIAL_INTRO_MOCAP()

	
	INT tempLoop = 0
	INT iNumPlayers = 0
	 
//	INT iNumPlayersCreated = 0
	PLAYER_INDEX PlayersForCut[4]
	PLAYER_INDEX thePlayerID
	
	PARTICIPANT_INDEX part
	
	FOR tempLoop = 0 TO 3
		PlayersForCut[tempLoop] = INVALID_PLAYER_INDEX()
	ENDFOR

	#IF IS_DEBUG_BUILD
		PRINT_TRIGGER_STRING("")
		PRINT_TRIGGER_STRING("********************* [CREATE_LOCAL_PLAYERS_FOR_TUTORIAL_INTRO_MOCAP] *********************")
		PRINT_TRIGGER_STRING("")
		PRINT_TRIGGER_STRING("[CREATE_LOCAL_PLAYERS_FOR_TUTORIAL_INTRO_MOCAP] Building list of players who will be in the mocap")
	#ENDIF
	
	tempLoop = 0
	
	PlayersForCut[0] =  LocalPlayer
	iNumPlayers++
	
	FOR tempLoop = 0 TO NETWORK_GET_MAX_NUM_PARTICIPANTS() - 1 
		part = INT_TO_PARTICIPANTINDEX(tempLoop)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(part)
			thePlayerID = NETWORK_GET_PLAYER_INDEX(part)
			IF IS_NET_PLAYER_OK(thePlayerID)
				IF thePlayerID <> LocalPlayer
					PlayersForCut[iNumPlayers] =  thePlayerID
					iNumPlayers++
					
					#IF IS_DEBUG_BUILD
						PRINT_TRIGGER_STRING_WITH_PLAYER_NAME("[CREATE_LOCAL_PLAYERS_FOR_TUTORIAL_INTRO_MOCAP] This player will be in the mocap: ", thePlayerID)
					#ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
						PRINT_TRIGGER_STRING_WITH_PLAYER_NAME("[CREATE_LOCAL_PLAYERS_FOR_TUTORIAL_INTRO_MOCAP] I will be in the mocap ", LocalPlayer)
					#ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
			

	
	tempLoop = 0
	MODEL_NAMES mPlayer
	VECTOR vCreate
	FOR tempLoop = 0 TO iNumPlayers -1
		IF NOT DOES_ENTITY_EXIST(pedForTutMocap[tempLoop])
			IF PlayersForCut[tempLoop] <> INVALID_PLAYER_INDEX()
				GET_CREATE_POSITION_FOR_TUTORIAL_INTRO_MOCAP_PED(tempLoop, vCreate)
				mPlayer = GET_ENTITY_MODEL(GET_PLAYER_PED(PlayersForCut[tempLoop]))
			 	pedForTutMocap[tempLoop]  = CREATE_PED(PEDTYPE_MISSION, mPlayer, vCreate, 0.0, FALSE, FALSE)
				CLONE_PED_TO_TARGET(GET_PLAYER_PED(PlayersForCut[tempLoop]), pedForTutMocap[tempLoop])
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedForTutMocap[tempLoop], TRUE)
				SET_ENTITY_INVINCIBLE(pedForTutMocap[tempLoop], TRUE)
				#IF IS_DEBUG_BUILD
					PRINT_TRIGGER_STRING_WITH_PLAYER_NAME("[CREATE_LOCAL_PLAYERS_FOR_TUTORIAL_INTRO_MOCAP] Created this player's clone ", thePlayerID)
				#ENDIF
				
			ELSE
				//-- Invalid player for some reason
				#IF IS_DEBUG_BUILD
					PRINT_TRIGGER_STRING_INT("[CREATE_LOCAL_PLAYERS_FOR_TUTORIAL_INTRO_MOCAP] Trying to create temp players but player is invalid! tempLoop = ", tempLoop)
				#ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	tempLoop = 0
	FOR tempLoop = 0 TO iNumPlayers -1
		IF NOT DOES_ENTITY_EXIST(pedForTutMocap[tempLoop])
			#IF IS_DEBUG_BUILD
				PRINT_TRIGGER_STRING_INT("[CREATE_LOCAL_PLAYERS_FOR_TUTORIAL_INTRO_MOCAP]S till waiting for someone, tempLoop = ", tempLoop)
			#ENDIF
			RETURN FALSE
		ENDIF
	ENDFOR
	
	RETURN TRUE
ENDFUNC

PROC MANAGE_TUTORIAL_MOCAP_PLAYER_TALKING()
	INT tempLoop
	IF bLocalPlayerPedOk
		
		SET_PED_RESET_FLAG(LocalPlayerPed, PRF_DisableVoiceDrivenMouthMovement , TRUE)
	ENDIF
	
	FOR tempLoop = 0 TO 3 
		IF NOT IS_PED_INJURED(pedPlayerCut[tempLoop])
			IF NETWORK_IS_PLAYER_ACTIVE(playerForCut[tempLoop])
				IF IS_NET_PLAYER_OK(playerForCut[tempLoop])
					IF NETWORK_PLAYER_HAS_HEADSET(playerForCut[tempLoop])
						IF NETWORK_IS_PLAYER_TALKING(playerForCut[tempLoop]) 
					//		SET_PED_RESET_FLAG(pedPlayerCut[tempLoop], PRF_EnableVoiceDrivenMouthMovement, TRUE)
					//		#IF IS_DEBUG_BUILD PRINT_TRIGGER_STRING_WITH_PLAYER_NAME("[MANAGE_TUTORIAL_MOCAP_PLAYER_TALKING] Think this player is talking... ", playerForCut[tempLoop]) #ENDIF
						ENDIF 
					ENDIF
				ENDIF
			ENDIF 
		ENDIF
	ENDFOR
	
ENDPROC

PROC CLEANUP_TUTORIAL_MOCAP_PEDS()
	INT iTemp
	ENTITY_INDEX ent
	FOR iTemp = 0 TO 2
		IF DOES_ENTITY_EXIST(pedForTutMocap[iTemp])
			ent = pedForTutMocap[iTemp]
			DELETE_ENTITY(ent)
		ENDIF
	ENDFOR
ENDPROC

PROC MAINTAIN_FIX_MOCAP_AUDIO()
	IF IS_BIT_SET(iTutorialMissionCutBitset, biTut_StartFixingAudio)
		IF NOT IS_BIT_SET(iTutorialMissionCutBitset, biTut_StartCutsceneOverrideSound)
			IF HAS_NET_TIMER_EXPIRED(timeFixAudio, 20000)
				PLAY_SOUND_FRONTEND(-1, "CUTSCENE_DIALOGUE_OVERRIDE_SOUND_01", DEFAULT, FALSE)
				SET_BIT(iTutorialMissionCutBitset, biTut_StartCutsceneOverrideSound)
				PRINTLN("[dsw] [DO_TUTORIAL_MISSION_INTRO_MOCAP] [MAINTAIN_FIX_MOCAP_AUDIO] Set biTut_StartCutsceneOverrideSound")
			ENDIF
		ENDIF
		
		IF NOT IS_BIT_SET(iTutorialMissionCutBitset, biTut_StartAudioScene)
			IF HAS_NET_TIMER_EXPIRED(timeFixAudio, 25000)
				START_AUDIO_SCENE("MP_LAMAR_DIALOGUE_MUTE_SCENE") 
				PLAY_SOUND_FRONTEND(-1, "CUTSCENE_DIALOGUE_OVERRIDE_SOUND_02", DEFAULT, FALSE) 
				SET_BIT(iTutorialMissionCutBitset, biTut_StartAudioScene)
				PRINTLN("[dsw] [DO_TUTORIAL_MISSION_INTRO_MOCAP] [MAINTAIN_FIX_MOCAP_AUDIO] Set nbiTut_StartAudioScene")
			ENDIF
		ELSE
			IF NOT IS_BIT_SET(iTutorialMissionCutBitset, biTut_SToppedAudioScene)
				IF HAS_NET_TIMER_EXPIRED(timeFixAudio, 27000)
					SET_BIT(iTutorialMissionCutBitset, biTut_SToppedAudioScene)
					STOP_AUDIO_SCENE("MP_LAMAR_DIALOGUE_MUTE_SCENE")
					PRINTLN("[dsw] [DO_TUTORIAL_MISSION_INTRO_MOCAP] [MAINTAIN_FIX_MOCAP_AUDIO] Set biTut_SToppedAudioScene")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL DO_TUTORIAL_MISSION_INTRO_MOCAP()
	INT iPlayVehIndex = 0
	
	MODEL_NAMES mVeh
	VECTOR vRgb
	
	INT iLoop
	
	DO_TUTORIAL_MISSION_CUTSCENE()
				
	SWITCH iTutMissionIntroMocapProg
		CASE 0
			IF IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL()
				IF IS_BIT_SET(MC_serverBD.iServerBitSet,SBBOOL_ALL_JOINED_START )
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iPlayVehIndex])	
//						IF NOT g_bFailedTutorialMission
//						OR (g_bFailedTutorialMission AND IS_SKYSWOOP_AT_GROUND())
							/*
							
								MISSION VEHICLE GETS CREATED IN LOAD_AND_CREATE_ALL_FMMC_ENTITIES_RC. ONLY ON SERVER
							
							
							*/
							START_AUDIO_SCENE("MP_JOB_CHANGE_RADIO_MUTE") 
							PRINTLN("[dsw] [DO_TUTORIAL_MISSION_INTRO_MOCAP] Called START_AUDIO_SCENE - MP_JOB_CHANGE_RADIO_MUTE")

							SET_OVERRIDE_WEATHER("EXTRASUNNY")
							
							SET_NEWS_FEED_SPLASH_SCREEN_ACTIVE(FALSE)
					
	//						IF HAS_SCALEFORM_MOVIE_LOADED(g_TransitionSessionNonResetVars.sPostMissionCleanupData.scaleSplash)
	//							SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(g_TransitionSessionNonResetVars.sPostMissionCleanupData.scaleSplash)
	//							PRINTLN("[dsw] [DO_TUTORIAL_MISSION_INTRO_MOCAP] Cleaned up scaleform")
	//						ENDIF
							
							g_bPassedMyTutorialMission = FALSE
							g_bFailedTutorialMission = FALSE
							PRINTLN("[dsw] [DO_TUTORIAL_MISSION_INTRO_MOCAP] g_bPassedMyTutorialMission = FALSE")
							
							PRINTLN("[dsw] [DO_TUTORIAL_MISSION_INTRO_MOCAP] Setting weather sunny")
							START_MP_CUTSCENE(TRUE)
							SET_FRONTEND_RADIO_ACTIVE(FALSE)
						//	iDriverPlayerIndex = -1
							SET_ROADS_IN_ANGLED_AREA( <<269.985535,333.320984,92.176788>>, <<419.747925,300.674927,111.001175>>, 10.625000, FALSE, FALSE, FALSE)
							
						//	iDriverPlayerIndex = GET_DRIVER_OF_MISSION_VEH()
							SETUP_PLAYER_SEATS_FOR_TUTORIAL_MISSION()
							CLEAR_AREA_OF_VEHICLES(<<372.8201, 275.9439, 102.1275>>, 20.0)
							
							GET_VEHICLE_SETUP_MP(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iPlayVehIndex]), vehSetupOriginal)

							mVeh = GET_ENTITY_MODEL(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iPlayVehIndex]))
							PRINTLN("[dsw] [DO_TUTORIAL_MISSION_INTRO_MOCAP] Start vehicle exists. ")
							REQUEST_MODEL(mVeh) 
							PRINTLN("[dsw] [DO_TUTORIAL_MISSION_INTRO_MOCAP] requested vehicle model ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(mVeh))
											
							
							iTutMissionIntroMocapProg++
							#IF IS_DEBUG_BUILD
								PRINT_TRIGGER_STRING_INT("[DO_TUTORIAL_MISSION_INTRO_MOCAP] TutMissionIntroMocapProg = ", iTutMissionIntroMocapProg)
							#ENDIF
//						ELSE
//							PRINTLN("[dsw] [DO_TUTORIAL_MISSION_INTRO_MOCAP] Waiting for IS_TRANSITION_ACTIVE")
//						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
							PRINT_TRIGGER_STRING("[dsw] [DO_TUTORIAL_MISSION_INTRO_MOCAP] Waiting for MC_serverBD_1.sFMMC_SBD.niVehicle")
						#ENDIF
					ENDIF
					
				ELSE	
					#IF IS_DEBUG_BUILD
						PRINT_TRIGGER_STRING("[dsw] [DO_TUTORIAL_MISSION_INTRO_MOCAP] Waiting for SBBOOL_ALL_JOINED_START")
					#ENDIF

				ENDIF
			ELSE
				iTutMissionIntroMocapProg = 100
				#IF IS_DEBUG_BUILD
					PRINT_TRIGGER_STRING_INT("[DO_TUTORIAL_MISSION_INTRO_MOCAP]  Not running tutorial TutMissionIntroMocapProg = ", iTutMissionIntroMocapProg)
				#ENDIF

			ENDIF
		BREAK
		
		CASE 1
			mVeh = GET_ENTITY_MODEL(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iPlayVehIndex]))
			IF HAS_MODEL_LOADED(mVeh)
				vehTutStart = CREATE_VEHICLE(mVeh, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iPlayVehIndex].vPos, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iPlayVehIndex].fHead, FALSE, FALSE)
				
				#IF IS_DEBUG_BUILD
					VECTOR vCarLoc
					vCarLoc = GET_ENTITY_COORDS(vehTutStart)
					PRINTLN("[dsw] [DO_TUTORIAL_MISSION_CUTSCENE] Created vehTutStart at coords = ", vCarLoc) 
				#ENDIF
					
				SET_VEHICLE_SETUP_MP(vehTutStart,vehSetupOriginal) 
				SET_VEHICLE_ENGINE_ON(vehTutStart, TRUE, TRUE)
				SET_VEH_RADIO_STATION(vehTutStart, "OFF")
				SET_VEHICLE_RADIO_ENABLED(vehTutStart, FALSE)
				
				IF iDriverPlayerIndex > -1
					vRgb = << GlobalplayerBD_FM[iDriverPlayerIndex].fmRaceTutorial.iCrewRed, 
							GlobalplayerBD_FM[iDriverPlayerIndex].fmRaceTutorial.iCrewGreen, 
							GlobalplayerBD_FM[iDriverPlayerIndex].fmRaceTutorial.iCrewBlue >>
							
					IF GlobalplayerBD_FM[iDriverPlayerIndex].fmRaceTutorial.iLivery > -1
					OR (GlobalplayerBD_FM[iDriverPlayerIndex].fmRaceTutorial.iLivery = -1 
						AND NOT ARE_VECTORS_EQUAL(vRgb, <<0.0, 0.0, 0.0>>))	
						PRINTLN("[dsw] [DO_TUTORIAL_MISSION_INTRO_MOCAP] Setting up Cutscene vehicle colours with iPlayVehIndex = ", iPlayVehIndex," and iDriverPlayerIndex = ", iDriverPlayerIndex) 
						SET_UP_TUTORIAL_VEHICLE_COLOURS(vehTutStart, 
														GlobalplayerBD_FM[iDriverPlayerIndex].fmRaceTutorial.iLivery, 
														GlobalplayerBD_FM[iDriverPlayerIndex].fmRaceTutorial.modColourType,
														GlobalplayerBD_FM[iDriverPlayerIndex].fmRaceTutorial.iCrewRed,
														GlobalplayerBD_FM[iDriverPlayerIndex].fmRaceTutorial.iCrewGreen,
														GlobalplayerBD_FM[iDriverPlayerIndex].fmRaceTutorial.iCrewBlue)
														
						PRINTLN("[dsw] [DO_TUTORIAL_MISSION_INTRO_MOCAP] Set cutscene vehicle colours") 
						
					ELSE
						PRINTLN("[dsw] [DO_TUTORIAL_MISSION_INTRO_MOCAP] Tried to set vehicle colours, but GlobalplayerBD_FM[iDriverPlayerIndex].fmRaceTutorial.iLivery = ", GlobalplayerBD_FM[iDriverPlayerIndex].fmRaceTutorial.iLivery) 
					ENDIF
				ELSE
					PRINTLN("[dsw] [DO_TUTORIAL_MISSION_INTRO_MOCAP] Tried to set vehicle colours, but iDriverPlayerIndex = ", iDriverPlayerIndex) 
				ENDIF
				
				iTutMissionIntroMocapProg++
				#IF IS_DEBUG_BUILD
					PRINT_TRIGGER_STRING_INT("[DO_TUTORIAL_MISSION_INTRO_MOCAP] TutMissionIntroMocapProg = ", iTutMissionIntroMocapProg)
				#ENDIF
			ENDIF
		BREAK
		
		CASE 2
			IF CREATE_ALL_PLAYER_PEDS_FOR_TUTORIAL_MISSION_CUT()
			//	IF CREATE_LOCAL_PLAYERS_FOR_TUTORIAL_INTRO_MOCAP()
					REQUEST_CUTSCENE("mp_intro_mcs_8_a1")
					
					// [NG-INTEGRATE] Next-gen only block - Start
					
					interiorMocap247 = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< 378.2, 326.7, 104.2 >>, "v_shop_247")
					IF interiorMocap247 <> NULL
						PIN_INTERIOR_IN_MEMORY(interiorMocap247)
						PRINTLN("[dsw] [DO_TUTORIAL_MISSION_INTRO_MOCAP] Pinned 247 interior") 
					ELSE
						PRINTLN("[dsw] [DO_TUTORIAL_MISSION_INTRO_MOCAP] Failed to find 247 interior!") 
					ENDIF
					
					// [NG-INTEGRATE] Next-gen only block - End
					
					iTutMissionIntroMocapProg++
					
					#IF IS_DEBUG_BUILD
						PRINT_TRIGGER_STRING_INT("[DO_TUTORIAL_MISSION_INTRO_MOCAP] TutMissionIntroMocapProg = ", iTutMissionIntroMocapProg)
					#ENDIF
			//	ENDIF
			ENDIF
		BREAK
		
		CASE 3
			IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
				
				
				FOR iLoop = 0 TO 3
					IF indexLocalPlayer = -1
						IF NOT IS_PED_INJURED(pedPlayerCut[iLoop])
							IF NETWORK_IS_PLAYER_ACTIVE(playerForCut[iLoop])
								IF IS_NET_PLAYER_OK(playerForCut[iLoop], FALSE)
									IF playerForCut[iLoop] = LocalPlayer
										indexLocalPlayer = iLoop
										#IF IS_DEBUG_BUILD
											PRINT_TRIGGER_STRING_INT("[DO_TUTORIAL_MISSION_INTRO_MOCAP] setting indexLocalPlayer = ", iLoop)
										#ENDIF
									ENDIF
								ENDIF	
							ENDIF
						ENDIF
					ENDIF
				ENDFOR
				
				#IF IS_DEBUG_BUILD
					IF indexLocalPlayer = -1
						PRINT_TRIGGER_STRING("[DO_TUTORIAL_MISSION_INTRO_MOCAP] FAILED TO FIND LOCAL PLAYER!!! ")
					ENDIF	
				#ENDIF
				
				FOR iLoop = 0 TO 3
					IF iLoop <> indexLocalPlayer
						IF NOT IS_PED_INJURED(pedPlayerCut[iLoop])
							IF indexMp3 = -1
								indexMp3 = iLoop
								#IF IS_DEBUG_BUILD
									PRINT_TRIGGER_STRING_INT("[DO_TUTORIAL_MISSION_INTRO_MOCAP] setting indexMp3 = ", iLoop)
								#ENDIF
							ELIF indexMp2 = -1
								indexMp2 = iLoop
								#IF IS_DEBUG_BUILD
									PRINT_TRIGGER_STRING_INT("[DO_TUTORIAL_MISSION_INTRO_MOCAP] setting indexMp2 = ", iLoop)
								#ENDIF
							ELIF indexMp4 = -1
								indexMp4 = iLoop
								#IF IS_DEBUG_BUILD
									PRINT_TRIGGER_STRING_INT("[DO_TUTORIAL_MISSION_INTRO_MOCAP] setting indexMp4 = ", iLoop)
								#ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDFOR
				
				SET_CUTSCENE_ENTITY_STREAMING_FLAGS("MP_1", DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS)
				SET_CUTSCENE_ENTITY_STREAMING_FLAGS("MP_2", DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS)
				SET_CUTSCENE_ENTITY_STREAMING_FLAGS("MP_3", DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS)
				SET_CUTSCENE_ENTITY_STREAMING_FLAGS("MP_4", DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS)

				iTutMissionIntroMocapProg++
				
				#IF IS_DEBUG_BUILD
					PRINT_TRIGGER_STRING_INT("[DO_TUTORIAL_MISSION_INTRO_MOCAP] TutMissionIntroMocapProg = ", iTutMissionIntroMocapProg)
				#ENDIF
			ENDIF
		BREAK
		
		CASE 4
			IF HAS_CUTSCENE_LOADED()	
				//-- Register all entities for the mocap
				IF NOT IS_PED_INJURED(pedPlayerCut[indexLocalPlayer])
				//	REGISTER_ENTITY_FOR_CUTSCENE(LocalPlayerPed, "MP_1", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
					REGISTER_ENTITY_FOR_CUTSCENE(pedPlayerCut[indexLocalPlayer], "MP_1", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
				ENDIF
				IF indexMp3 > -1
				AND	NOT IS_PED_INJURED(pedPlayerCut[indexMp3])
					REGISTER_ENTITY_FOR_CUTSCENE(pedPlayerCut[indexMp3], "MP_3", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
				ELSE
					REGISTER_ENTITY_FOR_CUTSCENE(NULL, "MP_3", CU_DONT_ANIMATE_ENTITY , DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
				ENDIF
				
				IF indexMp2 > -1
				AND NOT IS_PED_INJURED(pedPlayerCut[indexMp2])
					REGISTER_ENTITY_FOR_CUTSCENE(pedPlayerCut[indexMp2], "MP_2", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
				ELSE
					REGISTER_ENTITY_FOR_CUTSCENE(NULL, "MP_2", CU_DONT_ANIMATE_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
				ENDIF
				
				IF indexMp4 > -1
				AND NOT IS_PED_INJURED(pedPlayerCut[indexMp4])
					REGISTER_ENTITY_FOR_CUTSCENE(pedPlayerCut[indexMp4], "MP_4", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
				ELSE
					REGISTER_ENTITY_FOR_CUTSCENE(NULL, "MP_4", CU_DONT_ANIMATE_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
				ENDIF
				
				REGISTER_ENTITY_FOR_CUTSCENE(NULL, "MP_Car_generic", CU_DONT_ANIMATE_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
//				IF DOES_ENTITY_EXIST(vehTutStart)
//					IF IS_VEHICLE_DRIVEABLE(vehTutStart)
//						REGISTER_ENTITY_FOR_CUTSCENE(vehTutStart, "MP_Car_generic", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
//					ENDIF
//				ENDIF

				SET_STORE_ENABLED(FALSE)
				
				#IF IS_DEBUG_BUILD
					PRINT_TRIGGER_STRING("[DO_TUTORIAL_MISSION_INTRO_MOCAP] SET_STORE_ENABLED(FALSE)")
				#ENDIF
				//-- Start the mocap
				START_CUTSCENE(CUTSCENE_DO_NOT_REPOSITION_PLAYER_TO_SCENE_ORIGIN)
				
				
				
				
				
				iTutMissionIntroMocapProg++
				
				#IF IS_DEBUG_BUILD
					PRINT_TRIGGER_STRING_INT("[DO_TUTORIAL_MISSION_INTRO_MOCAP] TutMissionIntroMocapProg = ", iTutMissionIntroMocapProg)
				#ENDIF
			ENDIF
		BREAK
		
		CASE 5
			IF IS_CUTSCENE_PLAYING()
				SET_BIT(iTutorialMissionCutBitset, biTut_StartFixingAudio)
				START_NET_TIMER(timeFixAudio)
				
				SET_BIT(iTutorialMissionCutBitset, biTut_CleanupPostFx)
				START_NET_TIMER(timeFixPan)
				
				g_TransitionSessionNonResetVars.sPostMissionCleanupData.bPreTutorialMissionInFlow = FALSE
				
				ENTITY_INDEX eLamar
				VEHICLE_INDEX vehLamar
				eLamar = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("MP_LAMAR_CAR", EMPEROR)
				IF DOES_ENTITY_EXIST(eLamar)
					IF IS_ENTITY_A_VEHICLE(eLamar)
						vehLamar = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(eLamar)
						IF IS_VEHICLE_DRIVEABLE(vehLamar)
							SET_VEHICLE_COLOURS(vehLamar, 0, 3)
							SET_VEHICLE_EXTRA_COLOURS(vehLamar, 0, 156)
						
							#IF IS_DEBUG_BUILD
								PRINT_TRIGGER_STRING("[DO_TUTORIAL_MISSION_INTRO_MOCAP] Set Lamar's Mocap car colours! ")
							#ENDIF
						ELSE
							#IF IS_DEBUG_BUILD
								PRINT_TRIGGER_STRING("[DO_TUTORIAL_MISSION_INTRO_MOCAP] Failed to get Lamar's Mocap car! 3 ")
							#ENDIF
						ENDIF
					ELSe
						#IF IS_DEBUG_BUILD
							PRINT_TRIGGER_STRING("[DO_TUTORIAL_MISSION_INTRO_MOCAP] Failed to get Lamar's Mocap car! 2 ")
						#ENDIF
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
						PRINT_TRIGGER_STRING("[DO_TUTORIAL_MISSION_INTRO_MOCAP] Failed to get Lamar's Mocap car! 1 ")
					#ENDIF
				ENDIF

				
				#IF IS_DEBUG_BUILD
					PRINT_TRIGGER_STRING("[dsw] [wjk] [DO_TUTORIAL_MISSION_INTRO_MOCAP] g_TransitionSessionNonResetVars.sPostMissionCleanupData.bPreTutorialMissionInFlow = FALSE")
				#ENDIF
				#IF IS_DEBUG_BUILD
					PRINT_TRIGGER_STRING("[DO_TUTORIAL_MISSION_INTRO_MOCAP] Calling CLEAN_UP_INTRO_STAGE_CAM ")
				#ENDIF
				

				IF NOT HAS_LOCAL_PLAYER_CREATED_LAMAR_CUT_ASSETS()
					SET_LOCAL_PLAYER_CREATED_LAMAR_POST_RACE_CUT_ASSETS(TRUE)
				ENDIF
				
				SET_BIT(iTutorialMissionCutBitset, biTut_StartScriptedSceneSetup)	
				SET_BIT(iTutorialMissionCutBitset, biTUt_StartEnterVehicle)
				START_NET_TIMER(timeTutMocap)
				iTutMissionIntroMocapProg++
				
				#IF IS_DEBUG_BUILD
					PRINT_TRIGGER_STRING_INT("[DO_TUTORIAL_MISSION_INTRO_MOCAP] TutMissionIntroMocapProg = ", iTutMissionIntroMocapProg)
				#ENDIF
			ENDIF
		BREAK
		
		CASE 6
			IF HAS_CUTSCENE_FINISHED()
			//	CLEANUP_MP_CUTSCENE()
				CLEANUP_TUTORIAL_MOCAP_PEDS()
				REMOVE_CUTSCENE()
				
				// [NG-INTEGRATE] Next-gen only block - Start
				
				IF interiorMocap247 <> NULL
					UNPIN_INTERIOR(interiorMocap247)
					PRINTLN("[dsw] [DO_TUTORIAL_MISSION_INTRO_MOCAP] Unpinned 247 interior")
				ENDIF
				
				// [NG-INTEGRATE] Next-gen only block - End

				SET_LOCAL_PLAYER_HAS_COMPLETED_POST_RACE_CUTSCENE(TRUE)
				iTutMissionIntroMocapProg = 100
				
				#IF IS_DEBUG_BUILD
					PRINT_TRIGGER_STRING_INT("[DO_TUTORIAL_MISSION_INTRO_MOCAP] TutMissionIntroMocapProg = ", iTutMissionIntroMocapProg)
				#ENDIF
			ELSE
				MANAGE_TUTORIAL_MOCAP_PLAYER_TALKING()
			ENDIF	
		BREAK
		
		CASE 100
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	IF NOT IS_BIT_SET(iTutorialMissionCutBitset, biTut_StartScriptCam)	
		IF CAN_SET_EXIT_STATE_FOR_CAMERA()
			IF DOES_CAM_EXIST(camTut)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				SET_TUTORIAL_CUTSCENE_ENTITIES_VISIBLE(TRUE)
				SET_BIT(iTutorialMissionCutBitset, biTut_StartScriptCam)	
				#IF IS_DEBUG_BUILD
					PRINT_TRIGGER_STRING("Rendered script cams...")
				#ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	MAINTAIN_FIX_MOCAP_AUDIO()
	
	IF IS_BIT_SET(iTutorialMissionCutBitset, biTut_CleanupPostFx)
		IF HAS_NET_TIMER_EXPIRED(timeFixPan, 1500)
			//-- Cleanup any previous cameras
			IF DOES_CAM_EXIST(g_CamTutorial)
			
				SET_CAM_ACTIVE(g_CamTutorial, FALSE)
				PRINTLN("[dsw] [DO_TUTORIAL_MISSION_CUTSCENE] g_CamTutorial exists, setting not active ")
				#IF IS_DEBUG_BUILD
					PRINT_TRIGGER_STRING("[DO_TUTORIAL_MISSION_INTRO_MOCAP] g_CamTutorial exists, setting not active ")
				#ENDIF
			ENDIF
			
			CLEAN_UP_INTRO_STAGE_CAM(g_sTransitionSessionData.ciCam,FMMC_TYPE_MISSION,TRUE)
			ANIMPOSTFX_STOP("MinigameTransitionIn")
			ANIMPOSTFX_STOP_ALL()
			CLEANUP_ALL_CORONA_FX()
			SET_SKYFREEZE_CLEAR(TRUE)
			
			//-- Fade in, if we restarted mission due to failing / quitting
			IF NOT IS_SCREEN_FADED_IN()
				IF bLocalPlayerOK
					IF NOT IS_SCREEN_FADING_IN()
						ANIMPOSTFX_STOP("MinigameTransitionIn")
						TOGGLE_RENDERPHASES(TRUE)
						DO_SCREEN_FADE_IN(500)
					ENDIF
				ENDIF
				
			
			ENDIF
			
			CLEAR_BIT(iTutorialMissionCutBitset, biTut_CleanupPostFx)
			
			#IF IS_DEBUG_BUILD
				PRINT_TRIGGER_STRING("[DO_TUTORIAL_MISSION_INTRO_MOCAP] Done postfx cleanup ")
			#ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC
