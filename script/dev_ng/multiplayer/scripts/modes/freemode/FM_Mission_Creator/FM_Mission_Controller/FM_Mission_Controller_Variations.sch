
USING "FM_Mission_Controller_USING.sch"
USING "FM_Mission_Controller_Utility.sch"


// Mission Controller - MC_ServerBD_1.iMissionVariationChoicesBS ...
CONST_INT MCVarConfigBS_BlipCCTVCams								0		
CONST_INT MCVarConfigBS_BlipGuards									1		
CONST_INT MCVarConfigBS_PedHealthPercentMod_1						2		// ShipmentsDisrupted_1
CONST_INT MCVarConfigBS_PedHealthPercentMod_2						3		// ShipmentsDisrupted_2
CONST_INT MCVarConfigBS_PedHealthPercentMod_3						4		// ShipmentsDisrupted_3
CONST_INT MCVarConfigBS_InventoryOverride_1							5		// ShipmentsDisrupted_1
CONST_INT MCVarConfigBS_InventoryOverride_2							6		// ShipmentsDisrupted_2
CONST_INT MCVarConfigBS_InventoryOverride_3							7		// ShipmentsDisrupted_3
CONST_INT MCVarConfigBS_EquipNightVisionOutfit						8
CONST_INT MCVarConfigBS_HasHandheldDrill							9		
CONST_INT MCVarConfigBS_HasEMP										10		
CONST_INT MCVarConfigBS_EndOfMissionDropOffOverride1				11		// EndOfMissionBuyer1
CONST_INT MCVarConfigBS_EndOfMissionDropOffOverride2				12		// EndOfMissionBuyer2		
CONST_INT MCVarConfigBS_EndOfMissionDropOffOverride3				13		// EndOfMissionBuyer3			
CONST_INT MCVarConfigBS_EndOfMissionDropOffOverrideShort			14		// EndOfMissionBuyerShort	
CONST_INT MCVarConfigBS_EndOfMissionDropOffOverrideMedium			15		// EndOfMissionBuyerMedium	
CONST_INT MCVarConfigBS_EndOfMissionDropOffOverrideLong				16		// EndOfMissionBuyerLong	
CONST_INT MCVarConfigBS_DecoyCrewMemberActive						17
CONST_INT MCVarConfigBS_SwitchGetawayVehicleActive					18
CONST_INT MCVarConfigBS_GetawayVehicleModPreset1					19
CONST_INT MCVarConfigBS_GetawayVehicleModPreset2					20
CONST_INT MCVarConfigBS_GetawayVehicleModPreset3					21
CONST_INT MCVarConfigBS_OutfitBugstar								22
CONST_INT MCVarConfigBS_OutfitMechanic								23		
CONST_INT MCVarConfigBS_OutfitGruppeSechs							24		
CONST_INT MCVarConfigBS_OutfitBrucie								25		
CONST_INT MCVarConfigBS_WeaponExpertLoadout1  						26		//CASINO_HEIST_CREW_WEAPON_EXPERTS
CONST_INT MCVarConfigBS_WeaponExpertLoadout2						27		
CONST_INT MCVarConfigBS_WeaponExpertLoadout3						28		
CONST_INT MCVarConfigBS_WeaponExpertLoadout4						29		
CONST_INT MCVarConfigBS_WeaponExpertLoadout5						30
CONST_INT MCVarConfigBS_AlternativeWeaponLoadout					31
CONST_INT MCVarConfigBS_DriverChosen1								32		//CASINO_HEIST_CREW_DRIVERS
CONST_INT MCVarConfigBS_DriverChosen2								33		
CONST_INT MCVarConfigBS_DriverChosen3								34		
CONST_INT MCVarConfigBS_DriverChosen4								35		
CONST_INT MCVarConfigBS_DriverChosen5								36		
CONST_INT MCVarConfigBS_DriverVehAlt1								37		
CONST_INT MCVarConfigBS_DriverVehAlt2								38		
CONST_INT MCVarConfigBS_DriverVehAlt3								39
CONST_INT MCVarConfigBS_DriverVehAlt4_FromJobComplete				40
CONST_INT MCVarConfigBS_HackerChosen1								41		//CASINO_HEIST_CREW_HACKERS
CONST_INT MCVarConfigBS_HackerChosen2								42		
CONST_INT MCVarConfigBS_HackerChosen3								43		
CONST_INT MCVarConfigBS_HackerChosen4								44		
CONST_INT MCVarConfigBS_HackerChosen5								45		
CONST_INT MCVarConfigBS_KeycardAccessLevel1							46		
CONST_INT MCVarConfigBS_KeycardAccessLevel2							47
CONST_INT MCVarConfigBS_EscapeOutfitNoose							48		
CONST_INT MCVarConfigBS_EscapeOutfitFireman							49		
CONST_INT MCVarConfigBS_EscapeOutfitHighRoller						50		
CONST_INT MCVarConfigBS_PhotographedEntranceExit1					51		
CONST_INT MCVarConfigBS_PhotographedEntranceExit2					52		
CONST_INT MCVarConfigBS_PhotographedEntranceExit3					53		
CONST_INT MCVarConfigBS_PhotographedEntranceExit4					54		
CONST_INT MCVarConfigBS_PhotographedEntranceExit5					55		
CONST_INT MCVarConfigBS_PhotographedEntranceExit6					56		
CONST_INT MCVarConfigBS_PhotographedEntranceExit7					57		
CONST_INT MCVarConfigBS_PhotographedEntranceExit8					58		
CONST_INT MCVarConfigBS_PhotographedEntranceExit9					59
CONST_INT MCVarConfigBS_PhotographedEntranceExit10					60
CONST_INT MCVarConfigBS_PhotographedEntranceExit11					61
CONST_INT MCVarConfigBS_SelectedEntrance1							62		
CONST_INT MCVarConfigBS_SelectedEntrance2							63		
CONST_INT MCVarConfigBS_SelectedEntrance3							64		
CONST_INT MCVarConfigBS_SelectedEntrance4							65
CONST_INT MCVarConfigBS_SelectedEntrance5							66
CONST_INT MCVarConfigBS_SelectedEntrance6							67
CONST_INT MCVarConfigBS_SelectedEntrance7							68
CONST_INT MCVarConfigBS_SelectedEntrance8							69
CONST_INT MCVarConfigBS_SelectedEntrance9							70
CONST_INT MCVarConfigBS_SelectedEntrance10							71
CONST_INT MCVarConfigBS_SelectedEntrance11							72
CONST_INT MCVarConfigBS_SelectedExit1								73		
CONST_INT MCVarConfigBS_SelectedExit2								74		
CONST_INT MCVarConfigBS_SelectedExit3								75		
CONST_INT MCVarConfigBS_SelectedExit4								76		
CONST_INT MCVarConfigBS_SelectedExit5								77		
CONST_INT MCVarConfigBS_SelectedExit6								78	
CONST_INT MCVarConfigBS_SelectedExit7								79
CONST_INT MCVarConfigBS_SelectedExit8								80
CONST_INT MCVarConfigBS_SelectedExit9								81
CONST_INT MCVarConfigBS_ApproachStealth								82
CONST_INT MCVarConfigBS_ApproachSubterfuge							83
CONST_INT MCVarConfigBS_ApproachDirect								84
CONST_INT MCVarConfigBS_Target_1									85
CONST_INT MCVarConfigBS_Target_2									86
CONST_INT MCVarConfigBS_Target_3									87
CONST_INT MCVarConfigBS_Target_4									88
CONST_INT MCVarConfigBS_OfficeInfested								89
CONST_INT MCVarConfigBS_HasRappelGear								90
CONST_INT MCVarConfigBS_ArcadeOwnedLaMesa							91
CONST_INT MCVarConfigBS_WestVineWood								92
CONST_INT MCVarConfigBS_Davis										93
CONST_INT MCVarConfigBS_RockfordHills								94
CONST_INT MCVarConfigBS_Grapeseed									95
CONST_INT MCVarConfigBS_PaletoBay									96
CONST_INT MCVarConfigBS_PedDisableImmuneToCriticalHits				97
CONST_INT MCVarConfigBS_SYNC_DUMMY_CONST							98 // SHOULD ALWAYS BE AT THE BOTTOM

// Make sure to modify FMMC_MAX_MC_VAR_CHOICE_BS if necessary!  (FMMC_MAX_MC_VAR_CHOICE_BS % 32)
// SwitchGetawayVehicleMods and potentially normal getaway vehicle mods?

CONST_INT ciMCVar_ASSOCIATED_SELECTION_CASINO_DRIVER		0
CONST_INT ciMCVar_ASSOCIATED_SELECTION_CASINO_GUNNER		1

// [MissionVariation] Server Data Define functions. ================================================================================= START

#IF IS_DEBUG_BUILD
INT iLocalMissionVariationChoicesBS[FMMC_MAX_MC_VAR_CHOICE_BS]
BOOL bMustBailOutOfSyncChecked
BOOL bMustBailOutOfSync
BOOL bMustBailOutOfSyncFlash
SCRIPT_TIMER bMustBailOutOfSyncTimer
#ENDIF

PROC DEFINE_MISSION_VARIATION_SETTINGS_SERVER_DATA_CASINO_HEIST()
#IF FEATURE_CASINO_HEIST

	SWITCH g_sCasinoHeistMissionConfigData.eShipmentDisruptionLevel
		CASE CASINO_HEIST_SHIPMENT_DISRUPTION_LEVEL__NONE
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Shipments Disrupted) (None)")			
		BREAK
		CASE CASINO_HEIST_SHIPMENT_DISRUPTION_LEVEL__MINOR
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Shipments Disrupted) Setting MCVarConfigBS_PedHealthPercentMod_1")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_PedHealthPercentMod_1)
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Shipments Disrupted) Setting MCVarConfigBS_InventoryOverride_1")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_InventoryOverride_1)
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Shipments Disrupted) Setting MCVarConfigBS_PedDisableImmuneToCriticalHits (1)")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_PedDisableImmuneToCriticalHits)
		BREAK
		CASE CASINO_HEIST_SHIPMENT_DISRUPTION_LEVEL__MAJOR
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Shipments Disrupted) Setting MCVarConfigBS_PedHealthPercentMod_2")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_PedHealthPercentMod_2)
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Shipments Disrupted) Setting MCVarConfigBS_InventoryOverride_2")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_InventoryOverride_2)
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Shipments Disrupted) Setting MCVarConfigBS_PedDisableImmuneToCriticalHits (2)")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_PedDisableImmuneToCriticalHits)
		BREAK
		CASE CASINO_HEIST_SHIPMENT_DISRUPTION_LEVEL__SEVERE
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Shipments Disrupted) Setting MCVarConfigBS_PedHealthPercentMod_3")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_PedHealthPercentMod_3)
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Shipments Disrupted) Setting MCVarConfigBS_InventoryOverride_3")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_InventoryOverride_3)
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Shipments Disrupted) Setting MCVarConfigBS_PedDisableImmuneToCriticalHits (3)")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_PedDisableImmuneToCriticalHits)
		BREAK
	ENDSWITCH
	
	IF g_sCasinoHeistMissionConfigData.bSecurityCameraLocationsScoped 
		PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Blip CCTV Cams) Setting MCVarConfigBS_BlipCCTVCams")
		SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_BlipCCTVCams)
	ENDIF
	
	IF g_sCasinoHeistMissionConfigData.bGuardPatrolRoutesScoped	
		PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Blip Guards) Setting MCVarConfigBS_BlipGuards")
		SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_BlipGuards)
	ENDIF
	
	IF g_sCasinoHeistMissionConfigData.bStealthNightVisionAcquired
		PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Stealth night Vision) Setting MCVarConfigBS_EquipNightVisionOutfit")
		SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EquipNightVisionOutfit)
	ENDIF
	
	IF g_sCasinoHeistMissionConfigData.bHandheldDrillAcquired
		PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Hand Drill) Setting MCVarConfigBS_HasHandheldDrill")
		SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_HasHandheldDrill)
	ENDIF
	
	IF g_sCasinoHeistMissionConfigData.bEMPAcquired	
		PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Has EMP) Setting MCVarConfigBS_HasEMP")
		SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_HasEMP)
	ENDIF
	
	IF g_sCasinoHeistMissionConfigData.bStealthNightVisionAcquired
		PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Has Rappelling Gear) Setting MCVarConfigBS_HasRappelGear")
		SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_HasRappelGear)
	ENDIF
	
	SWITCH g_sCasinoHeistMissionConfigData.eDropoffLocation
		CASE CASINO_HEIST_DROPOFF_LOCATION__SHORT_1
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (DropOff Override) Setting MCVarConfigBS_EndOfMissionDropOffOverride1")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EndOfMissionDropOffOverride1)			
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (DropOff Override) Setting MCVarConfigBS_EndOfMissionDropOffOverrideShort")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EndOfMissionDropOffOverrideShort)
		BREAK
		CASE CASINO_HEIST_DROPOFF_LOCATION__SHORT_2
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (DropOff Override) Setting MCVarConfigBS_EndOfMissionDropOffOverride2")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EndOfMissionDropOffOverride2)			
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (DropOff Override) Setting MCVarConfigBS_EndOfMissionDropOffOverrideShort")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EndOfMissionDropOffOverrideShort)
		BREAK
		CASE CASINO_HEIST_DROPOFF_LOCATION__SHORT_3
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (DropOff Override) Setting MCVarConfigBS_EndOfMissionDropOffOverride3")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EndOfMissionDropOffOverride3)
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (DropOff Override) Setting MCVarConfigBS_EndOfMissionDropOffOverrideShort")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EndOfMissionDropOffOverrideShort)
		BREAK
		CASE CASINO_HEIST_DROPOFF_LOCATION__MEDIUM_1
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (DropOff Override) Setting MCVarConfigBS_EndOfMissionDropOffOverride1")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EndOfMissionDropOffOverride1)
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (DropOff Override) Setting MCVarConfigBS_EndOfMissionDropOffOverrideMedium")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EndOfMissionDropOffOverrideMedium)
		BREAK
		CASE CASINO_HEIST_DROPOFF_LOCATION__MEDIUM_2
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (DropOff Override) Setting MCVarConfigBS_EndOfMissionDropOffOverride2")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EndOfMissionDropOffOverride2)
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (DropOff Override) Setting MCVarConfigBS_EndOfMissionDropOffOverrideMedium")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EndOfMissionDropOffOverrideMedium)			
		BREAK
		CASE CASINO_HEIST_DROPOFF_LOCATION__MEDIUM_3
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (DropOff Override) Setting MCVarConfigBS_EndOfMissionDropOffOverride3")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EndOfMissionDropOffOverride3)
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (DropOff Override) Setting MCVarConfigBS_EndOfMissionDropOffOverrideMedium")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EndOfMissionDropOffOverrideMedium)			
		BREAK
		CASE CASINO_HEIST_DROPOFF_LOCATION__LONG_1
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (DropOff Override) Setting MCVarConfigBS_EndOfMissionDropOffOverride1")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EndOfMissionDropOffOverride1)
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (DropOff Override) Setting MCVarConfigBS_EndOfMissionDropOffOverrideLong")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EndOfMissionDropOffOverrideLong)			
		BREAK
		CASE CASINO_HEIST_DROPOFF_LOCATION__LONG_2
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (DropOff Override) Setting MCVarConfigBS_EndOfMissionDropOffOverride2")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EndOfMissionDropOffOverride2)
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (DropOff Override) Setting MCVarConfigBS_EndOfMissionDropOffOverrideLong")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EndOfMissionDropOffOverrideLong)	
			
		BREAK
		CASE CASINO_HEIST_DROPOFF_LOCATION__LONG_3
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (DropOff Override) Setting MCVarConfigBS_EndOfMissionDropOffOverride3")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EndOfMissionDropOffOverride3)
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (DropOff Override) Setting MCVarConfigBS_EndOfMissionDropOffOverrideLong")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EndOfMissionDropOffOverrideLong)				
		BREAK
	ENDSWITCH
	
	SWITCH g_sCasinoHeistMissionConfigData.eDropoffSubLocation
		CASE CASINO_HEIST_DROPOFF_SUB_LOCATION_1
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EndOfMissionDropOffOverride1)
			CLEAR_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EndOfMissionDropOffOverride2)
			CLEAR_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EndOfMissionDropOffOverride3)
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (DropOff Override) Force Setting MCVarConfigBS_EndOfMissionDropOffOverride1 to be active.")
		BREAK
		CASE CASINO_HEIST_DROPOFF_SUB_LOCATION_2
			CLEAR_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EndOfMissionDropOffOverride1)
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EndOfMissionDropOffOverride2)
			CLEAR_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EndOfMissionDropOffOverride3)
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (DropOff Override) Force Setting MCVarConfigBS_EndOfMissionDropOffOverride2 to be active.")
		BREAK
		CASE CASINO_HEIST_DROPOFF_SUB_LOCATION_3
			CLEAR_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EndOfMissionDropOffOverride1)
			CLEAR_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EndOfMissionDropOffOverride2)
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EndOfMissionDropOffOverride3)
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (DropOff Override) Force Setting MCVarConfigBS_EndOfMissionDropOffOverride3 to be active.")
		BREAK
	ENDSWITCH
	
	IF g_sCasinoHeistMissionConfigData.bDecoyCrewMemberPurchased	
		PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Decoy Crew Member) Setting MCVarConfigBS_DecoyCrewMemberActive")
		SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_DecoyCrewMemberActive)				
	ENDIF
	
	IF g_sCasinoHeistMissionConfigData.bSwitchGetawayVehiclePurchased
		PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Switch Getaway Veh) Setting MCVarConfigBS_SwitchGetawayVehicleActive")
		SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SwitchGetawayVehicleActive)				
	ENDIF
	
	SWITCH g_sCasinoHeistMissionConfigData.eVehicleModPresetChosen
		CASE CASINO_HEIST_VEHICLE_MOD_PRESET__1
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Getaway Veh Mod) Setting MCVarConfigBS_GetawayVehicleModPreset1")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_GetawayVehicleModPreset1)
		BREAK
		CASE CASINO_HEIST_VEHICLE_MOD_PRESET__2
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Getaway Veh Mod) Setting MCVarConfigBS_GetawayVehicleModPreset2")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_GetawayVehicleModPreset2)
		BREAK
		CASE CASINO_HEIST_VEHICLE_MOD_PRESET__3
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Getaway Veh Mod) Setting MCVarConfigBS_GetawayVehicleModPreset3")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_GetawayVehicleModPreset3)
		BREAK
	ENDSWITCH
	
	SWITCH g_sCasinoHeistMissionConfigData.eCrewWeaponsExpertChosen 
		CASE CASINO_HEIST_WEAPON_EXPERT__KARL_ABOLAJI
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Weapons Expert) Setting MCVarConfigBS_WeaponExpertLoadout1")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_WeaponExpertLoadout1)
		BREAK
		CASE CASINO_HEIST_WEAPON_EXPERT__CHARLIE
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Weapons Expert) Setting MCVarConfigBS_WeaponExpertLoadout2")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_WeaponExpertLoadout2)
		BREAK
		CASE CASINO_HEIST_WEAPON_EXPERT__PACKIE_MCREARY
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Weapons Expert) Setting MCVarConfigBS_WeaponExpertLoadout3")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_WeaponExpertLoadout3)
		BREAK
		CASE CASINO_HEIST_WEAPON_EXPERT__GUSTAVO_MOTA
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Weapons Expert) Setting MCVarConfigBS_WeaponExpertLoadout4")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_WeaponExpertLoadout4)
		BREAK
		CASE CASINO_HEIST_WEAPON_EXPERT__WEAPONS_EXPERT
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Weapons Expert) Setting MCVarConfigBS_WeaponExpertLoadout5")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_WeaponExpertLoadout5)
		BREAK
		DEFAULT
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Weapons Expert) No Weapon Expert Selected: ", ENUM_TO_INT(g_sCasinoHeistMissionConfigData.eCrewWeaponsExpertChosen))
		BREAK
	ENDSWITCH
	
	IF g_sCasinoHeistMissionConfigData.eCrewWeaponsLoadoutChosen = CASINO_HEIST_WEAPON_LOADOUT__ALTERNATE
		PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Weapons Expert Loadout) Setting MCVarConfigBS_AlternativeWeaponLoadout")
		SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_AlternativeWeaponLoadout)
	ENDIF

	SWITCH g_sCasinoHeistMissionConfigData.eCrewDriverChosen 
		CASE CASINO_HEIST_DRIVER__KARIM_DENZ
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Driver) Setting MCVarConfigBS_DriverChosen1")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_DriverChosen1)
		BREAK
		CASE CASINO_HEIST_DRIVER__TALIANA_MARTINEZ
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Driver) Setting MCVarConfigBS_DriverChosen2")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_DriverChosen2)
		BREAK
		CASE CASINO_HEIST_DRIVER__EDDIE_TOH
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Driver) Setting MCVarConfigBS_DriverChosen3")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_DriverChosen3)
		BREAK
		CASE CASINO_HEIST_DRIVER__ZACH
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Driver) Setting MCVarConfigBS_DriverChosen4")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_DriverChosen4)
		BREAK
		CASE CASINO_HEIST_DRIVER__WEAPONS_EXPERT
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Driver) Setting MCVarConfigBS_DriverChosen5")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_DriverChosen5)
		BREAK
	ENDSWITCH
	
	// or something like this one GET_NUMBER_OF_TIMES_HEIST_STRAND_COMPLETED
	INT iBossPart = -1 
	PLAYER_INDEX piPlayerBoss = GB_GET_LOCAL_PLAYER_GANG_BOSS()	
	IF piPlayerBoss != INVALID_PLAYER_INDEX()
		IF NETWORK_IS_PLAYER_A_PARTICIPANT(piPlayerBoss)
			iBossPart = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(piPlayerBoss))
		ELSE
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Drive Alt Vehicle) Boss is not yet a player participant")
		ENDIF
	ELSE
		PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Drive Alt Vehicle) Boss is invalid from GB_GET_LOCAL_PLAYER_GANG_BOSS")
	ENDIF
	
	PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Drive Alt Vehicle) iBossPart: ", iBossPart)
	
	IF GET_HEIST_COMPLETION_STAT_FOR_PARTICIPANT(iBossPart, HBCA_BS_TUTORIAL_CAR)
		PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Drive Alt Vehicle) Setting MCVarConfigBS_DriverVehAlt4_FromJobComplete")
		SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_DriverVehAlt4_FromJobComplete)		
	ELSE
		PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Drive Alt Vehicle) Incorrect data for boss!")
	ENDIF
	
	SWITCH g_sCasinoHeistMissionConfigData.eCrewVehiclesLoadoutChosen
		CASE CASINO_HEIST_VEHICLE_SELECTION__ALTERNATE
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Drive Alt Vehicle) Setting MCVarConfigBS_DriverVehAlt1")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_DriverVehAlt1)
		BREAK
		CASE CASINO_HEIST_VEHICLE_SELECTION__ALTERNATE_2
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Drive Alt Vehicle) Setting MCVarConfigBS_DriverVehAlt2")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_DriverVehAlt2)
		BREAK
		CASE CASINO_HEIST_VEHICLE_SELECTION__ALTERNATE_3
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Drive Alt Vehicle) Setting MCVarConfigBS_DriverVehAlt3")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_DriverVehAlt3)
		BREAK
	ENDSWITCH

	SWITCH g_sCasinoHeistMissionConfigData.eCrewHackerChosen 
		CASE CASINO_HEIST_HACKER__RICKIE_LUKENS
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Hacker) Setting MCVarConfigBS_HackerChosen1")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_HackerChosen1)
		BREAK
		CASE CASINO_HEIST_HACKER__CHRISTIAN_FELTZ
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Hacker) Setting MCVarConfigBS_HackerChosen2")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_HackerChosen2)
		BREAK
		CASE CASINO_HEIST_HACKER__YOHAN
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Hacker) Setting MCVarConfigBS_HackerChosen3")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_HackerChosen3)
		BREAK
		CASE CASINO_HEIST_HACKER__AVI_SCHWARTZMAN
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Hacker) Setting MCVarConfigBS_HackerChosen4")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_HackerChosen4)
		BREAK
		CASE CASINO_HEIST_HACKER__PAIGE_HARRIS
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Hacker) Setting MCVarConfigBS_HackerChosen5")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_HackerChosen5)
		BREAK
	ENDSWITCH	
	
	SWITCH g_sCasinoHeistMissionConfigData.eCrewKeyAccessLevel
		CASE CASINO_HEIST_KEY_ACCESS_LEVEL__ONE
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Inside Man) Setting MCVarConfigBS_KeycardAccessLevel1")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_KeycardAccessLevel1)
		BREAK		
		CASE CASINO_HEIST_KEY_ACCESS_LEVEL__TWO
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Inside Man) Setting MCVarConfigBS_KeycardAccessLevel2")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_KeycardAccessLevel2)
		BREAK
	ENDSWITCH

	SWITCH g_sCasinoHeistMissionConfigData.eEntranceChosen
		CASE CASINO_HEIST_ACCESS_POINT__MAIN_ENTRANCE
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Entrance Chosen) Setting MCVarConfigBS_SelectedEntrance1")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SelectedEntrance1)
		BREAK
		CASE CASINO_HEIST_ACCESS_POINT__STAFF_ENTRANCE
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Entrance Chosen) Setting MCVarConfigBS_SelectedEntrance2")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SelectedEntrance2)
		BREAK
		CASE CASINO_HEIST_ACCESS_POINT__GARBAGE_ENTRANCE
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Entrance Chosen) Setting MCVarConfigBS_SelectedEntrance3")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SelectedEntrance3)
		BREAK
		CASE CASINO_HEIST_ACCESS_POINT__SOUTH_WEST_ROOF_TERRACE_ENTRANCE
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Entrance Chosen) Setting MCVarConfigBS_SelectedEntrance4")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SelectedEntrance4)
		BREAK
		CASE CASINO_HEIST_ACCESS_POINT__NORTH_WEST_ROOF_TERRACE_ENTRANCE
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Entrance Chosen) Setting MCVarConfigBS_SelectedEntrance5")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SelectedEntrance5)
		BREAK
		CASE CASINO_HEIST_ACCESS_POINT__SOUTH_EAST_ROOF_TERRACE_ENTRANCE
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Entrance Chosen) Setting MCVarConfigBS_SelectedEntrance6")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SelectedEntrance6)
		BREAK
		CASE CASINO_HEIST_ACCESS_POINT__NORTH_EAST_ROOF_TERRACE_ENTRANCE
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Entrance Chosen) Setting MCVarConfigBS_SelectedEntrance7")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SelectedEntrance7)
		BREAK
		CASE CASINO_HEIST_ACCESS_POINT__SOUTH_HELIPAD_ENTRANCE
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Entrance Chosen) Setting MCVarConfigBS_SelectedEntrance8")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SelectedEntrance8)
		BREAK
		CASE CASINO_HEIST_ACCESS_POINT__NORTH_HELIPAD_ENTRANCE
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Entrance Chosen) Setting MCVarConfigBS_SelectedEntrance9")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SelectedEntrance9)
		BREAK
		CASE CASINO_HEIST_ACCESS_POINT__SECURITY_VEHICLE_ENTRANCE
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Entrance Chosen) Setting MCVarConfigBS_SelectedEntrance10")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SelectedEntrance10)
		BREAK
		CASE CASINO_HEIST_ACCESS_POINT__SEWER_ENTRANCE
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Entrance Chosen) Setting MCVarConfigBS_SelectedEntrance11")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SelectedEntrance11)
		BREAK		
	ENDSWITCH
	
	INT iBSLoop, iBSLoopMax
	iBSLoopMax = MCVarConfigBS_PhotographedEntranceExit11 - MCVarConfigBS_PhotographedEntranceExit1
	FOR iBSLoop = 0 TO iBSLoopMax
		IF IS_BIT_SET(g_sCasinoHeistMissionConfigData.iAccessPointBitset, iBSLoop)
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Photographed Entrances) Access Point ", iBSLoop, " Is photographed. Setting MCVarConfigBS_PhotographedEntranceExit", iBSLoop)
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, iBSLoop + MCVarConfigBS_PhotographedEntranceExit1)
		ENDIF
	ENDFOR
	
	SWITCH g_sCasinoHeistMissionConfigData.eExitChosen
		CASE CASINO_HEIST_ACCESS_POINT__MAIN_ENTRANCE
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Exit Chosen) Setting MCVarConfigBS_SelectedExit1")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SelectedExit1)
		BREAK
		CASE CASINO_HEIST_ACCESS_POINT__STAFF_ENTRANCE
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Exit Chosen) Setting MCVarConfigBS_SelectedExit2")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SelectedExit2)
		BREAK
		CASE CASINO_HEIST_ACCESS_POINT__GARBAGE_ENTRANCE
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Exit Chosen) Setting MCVarConfigBS_SelectedExit3")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SelectedExit3)
		BREAK
		CASE CASINO_HEIST_ACCESS_POINT__SOUTH_WEST_ROOF_TERRACE_ENTRANCE
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Exit Chosen) Setting MCVarConfigBS_SelectedExit4")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SelectedExit4)
		BREAK
		CASE CASINO_HEIST_ACCESS_POINT__NORTH_WEST_ROOF_TERRACE_ENTRANCE
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Exit Chosen) Setting MCVarConfigBS_SelectedExit5")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SelectedExit5)
		BREAK
		CASE CASINO_HEIST_ACCESS_POINT__SOUTH_EAST_ROOF_TERRACE_ENTRANCE
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Exit Chosen) Setting MCVarConfigBS_SelectedExit6")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SelectedExit6)
		BREAK
		CASE CASINO_HEIST_ACCESS_POINT__NORTH_EAST_ROOF_TERRACE_ENTRANCE
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Exit Chosen) Setting MCVarConfigBS_SelectedExit7")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SelectedExit7)
		BREAK
		CASE CASINO_HEIST_ACCESS_POINT__SOUTH_HELIPAD_ENTRANCE
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Exit Chosen) Setting MCVarConfigBS_SelectedExit8")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SelectedExit8)
		BREAK
		CASE CASINO_HEIST_ACCESS_POINT__NORTH_HELIPAD_ENTRANCE
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Exit Chosen) Setting MCVarConfigBS_SelectedExit9")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SelectedExit9)
		BREAK
	ENDSWITCH
	
	SWITCH g_sCasinoHeistMissionConfigData.eSubterfugeOutfitsOut
		CASE CASINO_HEIST_OUTFIT_OUT__SWAT
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Escape Outfit) Setting MCVarConfigBS_EscapeOutfitNoose")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EscapeOutfitNoose)
		BREAK
		CASE CASINO_HEIST_OUTFIT_OUT__FIREMAN
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Escape Outfit) Setting MCVarConfigBS_EscapeOutfitFireman")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EscapeOutfitFireman)
		BREAK
		CASE CASINO_HEIST_OUTFIT_OUT__HIGH_ROLLER
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Escape Outfit) Setting MCVarConfigBS_EscapeOutfitHighRoller")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EscapeOutfitHighRoller)
		BREAK
	ENDSWITCH
	
	SWITCH g_sCasinoHeistMissionConfigData.eChosenApproachType
		CASE CASINO_HEIST_APPROACH_TYPE__STEALTH
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Approach) Setting MCVarConfigBS_ApproachStealth")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_ApproachStealth)
		BREAK
		CASE CASINO_HEIST_APPROACH_TYPE__SUBTERFUGE
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Approach) Setting MCVarConfigBS_ApproachSubterfuge")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_ApproachSubterfuge)
		BREAK
		CASE CASINO_HEIST_APPROACH_TYPE__DIRECT
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Approach) Setting MCVarConfigBS_ApproachDirect")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_ApproachDirect)
		BREAK
	ENDSWITCH
	
	SWITCH g_sCasinoHeistMissionConfigData.eTarget
		CASE CASINO_HEIST_TARGET_TYPE__CASH
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Target Type) Setting MCVarConfigBS_Target_1")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_Target_1)
		BREAK
		CASE CASINO_HEIST_TARGET_TYPE__GOLD
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Target Type) Setting MCVarConfigBS_Target_2")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_Target_2)
		BREAK
		CASE CASINO_HEIST_TARGET_TYPE__ART
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Target Type) Setting MCVarConfigBS_Target_3")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_Target_3)
		BREAK
		CASE CASINO_HEIST_TARGET_TYPE__DIAMONDS
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Target Type) Setting MCVarConfigBS_Target_4")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_Target_4)
		BREAK
	ENDSWITCH
	
	IF g_sCasinoHeistMissionConfigData.bOfficeInfested	
		PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Office Infested) Setting MCVarConfigBS_OfficeInfested")
		SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_OfficeInfested)				
	ENDIF
	
	IF g_sCasinoHeistMissionConfigData.bRappelGearAcquired
		PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Rappel Gear) Setting MCVarConfigBS_HasRappelGear")
		SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_HasRappelGear)				
	ENDIF
	
	SWITCH g_sCasinoHeistMissionConfigData.eSubterfugeOutfitsIn
		CASE CASINO_HEIST_OUTFIT_IN__BUGSTAR
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Starting Outfit) Setting MCVarConfigBS_OutfitBugstar")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_OutfitBugstar)
		BREAK
		
		CASE CASINO_HEIST_OUTFIT_IN__MECHANIC
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Starting Outfit) Setting MCVarConfigBS_OutfitMechanic")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_OutfitMechanic)
		BREAK
		
		CASE CASINO_HEIST_OUTFIT_IN__GRUPPE_SECHS
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Starting Outfit) Setting MCVarConfigBS_OutfitGruppeSechs")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_OutfitGruppeSechs)
		BREAK
		
		CASE CASINO_HEIST_OUTFIT_IN__CELEBRITY
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Starting Outfit) Setting MCVarConfigBS_OutfitBrucie")
			SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_OutfitBrucie)
		BREAK
	ENDSWITCH
	
	// Might be worth having this assigned and passed in to corona vars.	
	IF piPlayerBoss != INVALID_PLAYER_INDEX()
		ARCADE_PROPERTY_ID eArcadePropertyID = GET_PLAYERS_OWNED_ARCADE_PROPERTY(piPlayerBoss)
		SIMPLE_INTERIORS siInstance = GET_SIMPLE_INTERIOR_ID_FROM_ARCADE_PROPERTY_ID(eArcadePropertyID) 
		SWITCH siInstance
			CASE SIMPLE_INTERIOR_ARCADE_PALETO_BAY
				PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Starting Arcade) Setting MCVarConfigBS_PaletoBay")
				SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_PaletoBay)
			BREAK
			CASE SIMPLE_INTERIOR_ARCADE_GRAPESEED
				PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Starting Arcade) Setting MCVarConfigBS_Grapeseed")
				SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_Grapeseed)
			BREAK
			CASE SIMPLE_INTERIOR_ARCADE_DAVIS
				PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Starting Arcade) Setting MCVarConfigBS_Davis")
				SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_Davis)
			BREAK
			CASE SIMPLE_INTERIOR_ARCADE_WEST_VINEWOOD
				PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Starting Arcade) Setting MCVarConfigBS_WestVineWood")
				SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_WestVineWood)
			BREAK
			CASE SIMPLE_INTERIOR_ARCADE_ROCKFORD_HILLS
				PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Starting Arcade) Setting MCVarConfigBS_RockfordHills")
				SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_RockfordHills)			
			BREAK
			CASE SIMPLE_INTERIOR_ARCADE_LA_MESA
				PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Starting Arcade) Setting MCVarConfigBS_ArcadeOwnedLaMesa")
				SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_ArcadeOwnedLaMesa)			
			BREAK
			DEFAULT
				PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Starting Arcade) DOES NOT OWN AN ARCADE INTERIOR, WEIRD.")
			BREAK
		ENDSWITCH
	ELSE
		PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Starting Arcade) COULD NOT FIND A GANG BOSS!")
	ENDIF
#ENDIF
ENDPROC
// [MissionVariation] Server Data Define functions. ================================================================================= END

// [MissionVariation] Non-Generic System functions. ================================================================================= START
FUNC BOOL IS_CASINO_HEIST_WEAPON_LOADOUT_DEFAULT()
	RETURN NOT IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_AlternativeWeaponLoadout)
ENDFUNC

PROC SET_SUBTERFUGE_WEAPON_LOADOUT(INT& iInventoryBit, INT& iInventoryBitTwo, INT& iInventoryBitThree, INT& iInventoryBitFour)
	
	iInventoryBit = 0
	iInventoryBitTwo = 0
	iInventoryBitThree = 0
	iInventoryBitFour = 0
	IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_WeaponExpertLoadout1)
		IF IS_CASINO_HEIST_WEAPON_LOADOUT_DEFAULT()
			SET_BIT(iInventoryBit, ciSTARTING_WEAPON_MICRO_SMG)
		ELSE
			SET_BIT(iInventoryBitTwo, ciSTARTING_WEAPON_DBBL_BARREL_SHOTGUN%32)
		ENDIF
		
	ELIF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_WeaponExpertLoadout2)
		IF IS_CASINO_HEIST_WEAPON_LOADOUT_DEFAULT()
			SET_BIT(iInventoryBitTwo, ciSTARTING_WEAPON_MACHINEPISTOL%32)
		ELSE
			SET_BIT(iInventoryBitFour, ciSTARTING_WEAPON_SWEEPER_SHOTGUN%32)
		ENDIF
	
	ELIF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_WeaponExpertLoadout3)
		IF IS_CASINO_HEIST_WEAPON_LOADOUT_DEFAULT()
			SET_BIT(iInventoryBit, ciSTARTING_WEAPON_SAWN_OFF_SHOTGUN)
		ELSE
			SET_BIT(iInventoryBitFour, ciSTARTING_WEAPON_COMPACT_RIFLE_DRUM%32)
		ENDIF
		
	ELIF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_WeaponExpertLoadout4)
		IF IS_CASINO_HEIST_WEAPON_LOADOUT_DEFAULT()
			SET_BIT(iInventoryBit, ciSTARTING_WEAPON_CARBINE_RIFLE)
		ELSE
			SET_BIT(iInventoryBit, ciSTARTING_WEAPON_ASSUALT_SHOT_GUN)
		ENDIF
	
	ELIF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_WeaponExpertLoadout5)
		IF IS_CASINO_HEIST_WEAPON_LOADOUT_DEFAULT()
			SET_BIT(iInventoryBitFour, ciSTARTING_WEAPON_SMG_MK2_HOLLOW%32)
		ELSE
			SET_BIT(iInventoryBitFour, ciSTARTING_WEAPON_CARBINE_RIFLE_MK2_ARMOR_PIERCE%32)
		ENDIF
	ENDIF
	
	IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_OutfitBugstar)
		SET_BIT(iInventoryBitFour, ciSTARTING_WEAPON_TEAR_GAS%32)
	ENDIF
ENDPROC

PROC SET_SUBTERFUGE_WEAPONS_BASED_ON_OUTFIT(INT& iInventoryBit, INT& iInventoryBitTwo, INT& iInventoryBitThree, INT& iInventoryBitFour)
	UNUSED_PARAMETER(iInventoryBit)
	IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_OutfitBugstar)
		SET_BIT(iInventoryBitThree, ciSTARTING_WEAPON_HAMMER%32)
	ELIF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_OutfitMechanic)
		SET_BIT(iInventoryBitTwo, ciSTARTING_WEAPON_WRENCH%32)
	ELIF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_OutfitGruppeSechs)
		SET_SUBTERFUGE_WEAPON_LOADOUT(iInventoryBit, iInventoryBitTwo, iInventoryBitThree, iInventoryBitFour)
		SET_BIT(iInventoryBitTwo, ciSTARTING_WEAPON_FLASHLIGHT%32)
	ELIF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_OutfitBrucie)
		SET_SUBTERFUGE_WEAPON_LOADOUT(iInventoryBit, iInventoryBitTwo, iInventoryBitThree, iInventoryBitFour)
		SET_BIT(iInventoryBitThree, ciSTARTING_WEAPON_SWITCHBLADE%32)
	ENDIF
	SET_BIT(iInventoryBitFour, ciSTARTING_WEAPON_CERAMIC_PISTOL%32)
ENDPROC

PROC SET_CASINO_HEIST_WEAPON_LOADOUT_KARL(INT& iInventoryBit, INT& iInventoryBitTwo, INT& iInventoryBitThree, INT& iInventoryBitFour)
	PRINTLN("[SCRIPT INIT][MissionVariation] - (Player Inventory) SET_UP_PLAYER_WEAPON_LOADOUT_VARIATION: Using Weapon Expert 1. Alternate? ", !IS_CASINO_HEIST_WEAPON_LOADOUT_DEFAULT())
	INT iTeam = g_i_Mission_team
	iInventoryBit = 0
	iInventoryBitTwo = 0
	iInventoryBitThree = 0
	iInventoryBitFour = 0
	IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_ApproachStealth)
		IF IS_CASINO_HEIST_WEAPON_LOADOUT_DEFAULT()
			SET_BIT(iInventoryBitFour, ciSTARTING_WEAPON_MICRO_SMG_STOCK_SILENCER%32)
			SET_BIT(iInventoryBitFour, ciSTARTING_WEAPON_VINTAGE_PISTOL_SILENCER%32)
			SET_BIT(iInventoryBitThree, ciSTARTING_WEAPON_STUNGUN%32)
			SET_BIT(iInventoryBit, ciSTARTING_WEAPON_KNIFE)
		ELSE
			SET_BIT(iInventoryBitFour, ciSTARTING_WEAPON_MACHINE_PISTOL_SILENCER%32)
			SET_BIT(iInventoryBitThree, ciSTARTING_WEAPON_COMBAT_PISTOL_SILENCER%32)
			SET_BIT(iInventoryBitThree, ciSTARTING_WEAPON_STUNGUN%32)
			SET_BIT(iInventoryBit, ciSTARTING_WEAPON_KNIFE)
		ENDIF
		SET_BIT(iInventoryBit, ciSTARTING_WEAPON_PARACHUTE)
		g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iStartingInventoryAmmo[ciSTARTING_WEAPON_PARACHUTE] = -3
		g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMidMissionInventoryAmmo[ciSTARTING_WEAPON_PARACHUTE] = - 3
	ELIF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_ApproachSubterfuge)
		SET_SUBTERFUGE_WEAPONS_BASED_ON_OUTFIT(iInventoryBit, iInventoryBitTwo, iInventoryBitThree, iInventoryBitFour)
	ELIF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_ApproachDirect)
		IF IS_CASINO_HEIST_WEAPON_LOADOUT_DEFAULT()
			SET_BIT(iInventoryBit, ciSTARTING_WEAPON_SAWN_OFF_SHOTGUN)
			SET_BIT(iInventoryBit, ciSTARTING_WEAPON_SMG)
			SET_BIT(iInventoryBitFour, ciSTARTING_WEAPON_MOLOTOV%32)
			SET_BIT(iInventoryBitThree, ciSTARTING_WEAPON_KNUCKLEDUSTERS%32)
		ELSE
			SET_BIT(iInventoryBitTwo, ciSTARTING_WEAPON_REVOLVER%32)
			SET_BIT(iInventoryBit, ciSTARTING_WEAPON_SMG)
			SET_BIT(iInventoryBitFour, ciSTARTING_WEAPON_MOLOTOV%32)
			SET_BIT(iInventoryBitThree, ciSTARTING_WEAPON_KNUCKLEDUSTERS%32)
		ENDIF
		SET_BIT(iInventoryBit, ciSTARTING_WEAPON_PARACHUTE)
		g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iStartingInventoryAmmo[ciSTARTING_WEAPON_PARACHUTE] = -3
		g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMidMissionInventoryAmmo[ciSTARTING_WEAPON_PARACHUTE] = - 3
	ENDIF
ENDPROC

PROC SET_CASINO_HEIST_WEAPON_LOADOUT_GUSTAVO(INT& iInventoryBit, INT& iInventoryBitTwo, INT& iInventoryBitThree, INT& iInventoryBitFour)
	PRINTLN("[SCRIPT INIT][MissionVariation] - (Player Inventory) SET_UP_PLAYER_WEAPON_LOADOUT_VARIATION: Using Weapon Expert 2. Alternate? ", !IS_CASINO_HEIST_WEAPON_LOADOUT_DEFAULT())
	INT iTeam = g_i_Mission_team
	iInventoryBit = 0
	iInventoryBitTwo = 0
	iInventoryBitThree = 0
	iInventoryBitFour = 0
	IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_ApproachStealth)
		IF IS_CASINO_HEIST_WEAPON_LOADOUT_DEFAULT()
			SET_BIT(iInventoryBitThree, ciSTARTING_WEAPON_ASSAULT_SMG_SILENCER%32)
			SET_BIT(iInventoryBitThree, ciSTARTING_WEAPON_COMBAT_PISTOL_SILENCER%32)
			SET_BIT(iInventoryBitThree, ciSTARTING_WEAPON_STUNGUN%32)
			SET_BIT(iInventoryBit, ciSTARTING_WEAPON_KNIFE)
		ELSE
			SET_BIT(iInventoryBit, ciSTARTING_WEAPON_ASSUALT_SHOT_GUN)
			SET_BIT(iInventoryBitFour, ciSTARTING_WEAPON_PISTOL50_SILENCER%32)
			SET_BIT(iInventoryBitThree, ciSTARTING_WEAPON_STUNGUN%32)
			SET_BIT(iInventoryBit, ciSTARTING_WEAPON_KNIFE)
		ENDIF
		SET_BIT(iInventoryBit, ciSTARTING_WEAPON_PARACHUTE)
		g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iStartingInventoryAmmo[ciSTARTING_WEAPON_PARACHUTE] = -3
		g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMidMissionInventoryAmmo[ciSTARTING_WEAPON_PARACHUTE] = - 3
	ELIF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_ApproachSubterfuge)
		SET_SUBTERFUGE_WEAPONS_BASED_ON_OUTFIT(iInventoryBit, iInventoryBitTwo, iInventoryBitThree, iInventoryBitFour)
	ELIF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_ApproachDirect)
		IF IS_CASINO_HEIST_WEAPON_LOADOUT_DEFAULT()
			SET_BIT(iInventoryBitTwo, ciSTARTING_WEAPON_ASSAULTSMG%32)
			SET_BIT(iInventoryBit, ciSTARTING_WEAPON_SMG)
			SET_BIT(iInventoryBit, ciSTARTING_WEAPON_GRENADE)
			SET_BIT(iInventoryBitTwo, ciSTARTING_WEAPON_HAMMER%32)
		ELSE
			SET_BIT(iInventoryBit, ciSTARTING_WEAPON_PUMP_SHOT_GUN)
			SET_BIT(iInventoryBit, ciSTARTING_WEAPON_SMG)
			SET_BIT(iInventoryBitTwo, ciSTARTING_WEAPON_GRENADE)
			SET_BIT(iInventoryBitThree, ciSTARTING_WEAPON_HAMMER%32)
		ENDIF
		SET_BIT(iInventoryBit, ciSTARTING_WEAPON_PARACHUTE)
		g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iStartingInventoryAmmo[ciSTARTING_WEAPON_PARACHUTE] = -3
		g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMidMissionInventoryAmmo[ciSTARTING_WEAPON_PARACHUTE] = - 3
	ENDIF
ENDPROC

PROC SET_CASINO_HEIST_WEAPON_LOADOUT_CHARLIE(INT& iInventoryBit, INT& iInventoryBitTwo, INT& iInventoryBitThree, INT& iInventoryBitFour)
	PRINTLN("[SCRIPT INIT][MissionVariation] - (Player Inventory) SET_UP_PLAYER_WEAPON_LOADOUT_VARIATION: Using Weapon Expert 3. Alternate? ", !IS_CASINO_HEIST_WEAPON_LOADOUT_DEFAULT())
	INT iTeam = g_i_Mission_team
	iInventoryBit = 0
	iInventoryBitTwo = 0
	iInventoryBitThree = 0
	iInventoryBitFour = 0
	IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_ApproachStealth)
		IF IS_CASINO_HEIST_WEAPON_LOADOUT_DEFAULT()
			SET_BIT(iInventoryBitFour, ciSTARTING_WEAPON_COMBAT_PDW%32)
			SET_BIT(iInventoryBitFour, ciSTARTING_WEAPON_PISTOL50_SILENCER%32)
			SET_BIT(iInventoryBitThree, ciSTARTING_WEAPON_STUNGUN%32)
			SET_BIT(iInventoryBit, ciSTARTING_WEAPON_KNIFE)
		ELSE
			SET_BIT(iInventoryBitThree, ciSTARTING_WEAPON_ASSAULT_RIFLE_SILENCER%32)
			SET_BIT(iInventoryBitFour, ciSTARTING_WEAPON_PISTOL50_SILENCER%32)
			SET_BIT(iInventoryBitThree, ciSTARTING_WEAPON_STUNGUN%32)
			SET_BIT(iInventoryBit, ciSTARTING_WEAPON_KNIFE)
		ENDIF
		SET_BIT(iInventoryBit, ciSTARTING_WEAPON_PARACHUTE)
		g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iStartingInventoryAmmo[ciSTARTING_WEAPON_PARACHUTE] = -3
		g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMidMissionInventoryAmmo[ciSTARTING_WEAPON_PARACHUTE] = - 3
	ELIF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_ApproachSubterfuge)
		SET_SUBTERFUGE_WEAPONS_BASED_ON_OUTFIT(iInventoryBit, iInventoryBitTwo, iInventoryBitThree, iInventoryBitFour)
	ELIF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_ApproachDirect)
		IF IS_CASINO_HEIST_WEAPON_LOADOUT_DEFAULT()
			SET_BIT(iInventoryBitTwo, ciSTARTING_WEAPON_HEAVYSHOTGUN%32)
			SET_BIT(iInventoryBitFour, ciSTARTING_WEAPON_SMG_EXTENDED%32)
			SET_BIT(iInventoryBit, ciSTARTING_WEAPON_STICKY_BOMB)
			SET_BIT(iInventoryBitTwo, ciSTARTING_WEAPON_CROWBAR%32)
		ELSE
			SET_BIT(iInventoryBit, ciSTARTING_WEAPON_COMBAT_MG)
			SET_BIT(iInventoryBitFour, ciSTARTING_WEAPON_SMG_EXTENDED%32)
			SET_BIT(iInventoryBit, ciSTARTING_WEAPON_STICKY_BOMB)
			SET_BIT(iInventoryBitTwo, ciSTARTING_WEAPON_CROWBAR%32)
		ENDIF
		SET_BIT(iInventoryBit, ciSTARTING_WEAPON_PARACHUTE)
		g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iStartingInventoryAmmo[ciSTARTING_WEAPON_PARACHUTE] = -3
		g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMidMissionInventoryAmmo[ciSTARTING_WEAPON_PARACHUTE] = - 3
	ENDIF
ENDPROC

PROC SET_CASINO_HEIST_WEAPON_LOADOUT_WEAPONS_EXPERT(INT& iInventoryBit, INT& iInventoryBitTwo, INT& iInventoryBitThree, INT& iInventoryBitFour)
	PRINTLN("[SCRIPT INIT][MissionVariation] - (Player Inventory) SET_UP_PLAYER_WEAPON_LOADOUT_VARIATION: Using Weapon Expert 4. Alternate? ", !IS_CASINO_HEIST_WEAPON_LOADOUT_DEFAULT())
	INT iTeam = g_i_Mission_team
	iInventoryBit = 0
	iInventoryBitTwo = 0
	iInventoryBitThree = 0
	iInventoryBitFour = 0
	IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_ApproachStealth)
		IF IS_CASINO_HEIST_WEAPON_LOADOUT_DEFAULT()
			SET_BIT(iInventoryBitFour, ciSTARTING_WEAPON_CARBINE_RIFLE_SILENCER%32)
			SET_BIT(iInventoryBitFour, ciSTARTING_WEAPON_HEAVY_PISTOL_EXTENDED_SILENCER%32)
			SET_BIT(iInventoryBitThree, ciSTARTING_WEAPON_STUNGUN%32)
			SET_BIT(iInventoryBit, ciSTARTING_WEAPON_KNIFE)
		ELSE
			SET_BIT(iInventoryBitThree, ciSTARTING_WEAPON_ASSAULT_SHOTGUN_SILENCER%32)
			SET_BIT(iInventoryBitFour, ciSTARTING_WEAPON_PISTOL50_EXTENDED_SILENCER%32)
			SET_BIT(iInventoryBitThree, ciSTARTING_WEAPON_STUNGUN%32)
			SET_BIT(iInventoryBit, ciSTARTING_WEAPON_KNIFE)
		ENDIF
		SET_BIT(iInventoryBit, ciSTARTING_WEAPON_PARACHUTE)
		g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iStartingInventoryAmmo[ciSTARTING_WEAPON_PARACHUTE] = -3
		g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMidMissionInventoryAmmo[ciSTARTING_WEAPON_PARACHUTE] = - 3
	ELIF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_ApproachSubterfuge)
		SET_SUBTERFUGE_WEAPONS_BASED_ON_OUTFIT(iInventoryBit, iInventoryBitTwo, iInventoryBitThree, iInventoryBitFour)
	ELIF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_ApproachDirect)
		IF IS_CASINO_HEIST_WEAPON_LOADOUT_DEFAULT()
			SET_BIT(iInventoryBit, ciSTARTING_WEAPON_CARBINE_RIFLE)
			SET_BIT(iInventoryBitFour, ciSTARTING_WEAPON_SMG_DRUM%32)
			SET_BIT(iInventoryBit, ciSTARTING_WEAPON_PROXIMITY_MINE)
			SET_BIT(iInventoryBitTwo, ciSTARTING_WEAPON_MACHETE%32)
		ELSE
			SET_BIT(iInventoryBit, ciSTARTING_WEAPON_ASSUALT_SHOT_GUN)
			SET_BIT(iInventoryBitFour, ciSTARTING_WEAPON_SMG_DRUM%32)
			SET_BIT(iInventoryBit, ciSTARTING_WEAPON_PROXIMITY_MINE)
			SET_BIT(iInventoryBitTwo, ciSTARTING_WEAPON_MACHETE%32)
		ENDIF
		SET_BIT(iInventoryBit, ciSTARTING_WEAPON_PARACHUTE)
		g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iStartingInventoryAmmo[ciSTARTING_WEAPON_PARACHUTE] = -3
		g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMidMissionInventoryAmmo[ciSTARTING_WEAPON_PARACHUTE] = - 3
	ENDIF
ENDPROC

PROC SET_CASINO_HEIST_WEAPON_LOADOUT_PACKIE(INT& iInventoryBit, INT& iInventoryBitTwo, INT& iInventoryBitThree, INT& iInventoryBitFour)
	PRINTLN("[SCRIPT INIT][MissionVariation] - (Player Inventory) SET_UP_PLAYER_WEAPON_LOADOUT_VARIATION: Using Weapon Expert 5. Alternate? ", !IS_CASINO_HEIST_WEAPON_LOADOUT_DEFAULT())
	INT iTeam = g_i_Mission_team
	iInventoryBit = 0
	iInventoryBitTwo = 0
	iInventoryBitThree = 0
	iInventoryBitFour = 0
	IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_ApproachStealth)
		IF IS_CASINO_HEIST_WEAPON_LOADOUT_DEFAULT()
			SET_BIT(iInventoryBitFour, ciSTARTING_WEAPON_PUMPSHOTGUN_MK2_STEEL_BUCK_SILENCER%32)
			SET_BIT(iInventoryBitFour, ciSTARTING_WEAPON_PISTOL50_EXTENDED_SILENCER%32)
			SET_BIT(iInventoryBitThree, ciSTARTING_WEAPON_STUNGUN%32)
			SET_BIT(iInventoryBit, ciSTARTING_WEAPON_KNIFE)
		ELSE
			SET_BIT(iInventoryBitFour, ciSTARTING_WEAPON_CARBINE_RIFLE_MK2_ARMOR_PIERCE_SILENCER%32)
			SET_BIT(iInventoryBitFour, ciSTARTING_WEAPON_PISTOL50_EXTENDED_SILENCER%32)
			SET_BIT(iInventoryBitThree, ciSTARTING_WEAPON_STUNGUN%32)
			SET_BIT(iInventoryBit, ciSTARTING_WEAPON_KNIFE)
		ENDIF
		SET_BIT(iInventoryBit, ciSTARTING_WEAPON_PARACHUTE)
		g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iStartingInventoryAmmo[ciSTARTING_WEAPON_PARACHUTE] = -3
		g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMidMissionInventoryAmmo[ciSTARTING_WEAPON_PARACHUTE] = - 3
	ELIF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_ApproachSubterfuge)
		SET_SUBTERFUGE_WEAPONS_BASED_ON_OUTFIT(iInventoryBit, iInventoryBitTwo, iInventoryBitThree, iInventoryBitFour)
	ELIF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_ApproachDirect)
		IF IS_CASINO_HEIST_WEAPON_LOADOUT_DEFAULT()
			SET_BIT(iInventoryBitFour, ciSTARTING_WEAPON_PUMPSHOTGUN_MK2_STEEL_BUCK%32)
			SET_BIT(iInventoryBitFour, ciSTARTING_WEAPON_SMG_MK2_HOLLOW_SCOPE%32)
			SET_BIT(iInventoryBitTwo, ciSTARTING_WEAPON_PIPEBOMB%32)
			SET_BIT(iInventoryBitTwo, ciSTARTING_WEAPON_WRENCH%32)
		ELSE
			SET_BIT(iInventoryBitThree, ciSTARTING_WEAPON_CARBINE_RIFLE_MK2_ARMOR_PIERCE%32)
			SET_BIT(iInventoryBitFour, ciSTARTING_WEAPON_SMG_MK2_HOLLOW_SCOPE%32)
			SET_BIT(iInventoryBitTwo, ciSTARTING_WEAPON_PIPEBOMB%32)
			SET_BIT(iInventoryBitTwo, ciSTARTING_WEAPON_WRENCH%32)
		ENDIF
		SET_BIT(iInventoryBit, ciSTARTING_WEAPON_PARACHUTE)
		g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iStartingInventoryAmmo[ciSTARTING_WEAPON_PARACHUTE] = -3
		g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMidMissionInventoryAmmo[ciSTARTING_WEAPON_PARACHUTE] = - 3
	ENDIF
ENDPROC

PROC SET_WEAPON_LOADOUTS_FOR_CASINO_HEIST(INT& iInventoryBit, INT& iInventoryBitTwo, INT& iInventoryBitThree, INT& iInventoryBitFour)
	IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_WeaponExpertLoadout1)
		SET_CASINO_HEIST_WEAPON_LOADOUT_KARL(iInventoryBit, iInventoryBitTwo, iInventoryBitThree, iInventoryBitFour)
	
	ELIF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_WeaponExpertLoadout2)
		SET_CASINO_HEIST_WEAPON_LOADOUT_GUSTAVO(iInventoryBit, iInventoryBitTwo, iInventoryBitThree, iInventoryBitFour)
	
	ELIF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_WeaponExpertLoadout3)
		SET_CASINO_HEIST_WEAPON_LOADOUT_CHARLIE(iInventoryBit, iInventoryBitTwo, iInventoryBitThree, iInventoryBitFour)
	
	ELIF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_WeaponExpertLoadout4)
		SET_CASINO_HEIST_WEAPON_LOADOUT_WEAPONS_EXPERT(iInventoryBit, iInventoryBitTwo, iInventoryBitThree, iInventoryBitFour)
	
	ELIF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_WeaponExpertLoadout5)
		SET_CASINO_HEIST_WEAPON_LOADOUT_PACKIE(iInventoryBit, iInventoryBitTwo, iInventoryBitThree, iInventoryBitFour)
	ENDIF
ENDPROC

PROC SET_PRESET_LOADOUT_FOR_CASINO_HEIST()
	
	IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_ApproachDirect)
		IF IS_CASINO_HEIST_WEAPON_LOADOUT_DEFAULT()
			g_FMMC_STRUCT.sFMMCEndConditions[g_i_Mission_team].iPresetInventorySelection = ciPRESET_INVENTORY__CASINO_HEIST_DIR_1
		ELSE
			g_FMMC_STRUCT.sFMMCEndConditions[g_i_Mission_team].iPresetInventorySelection = ciPRESET_INVENTORY__CASINO_HEIST_DIR_2
		ENDIF
	ELIF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_ApproachStealth)
		IF IS_CASINO_HEIST_WEAPON_LOADOUT_DEFAULT()
			g_FMMC_STRUCT.sFMMCEndConditions[g_i_Mission_team].iPresetInventorySelection = ciPRESET_INVENTORY__CASINO_HEIST_STE_1
		ELSE
			g_FMMC_STRUCT.sFMMCEndConditions[g_i_Mission_team].iPresetInventorySelection = ciPRESET_INVENTORY__CASINO_HEIST_STE_2
		ENDIF
	ELIF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_ApproachSubterfuge)
		IF IS_CASINO_HEIST_WEAPON_LOADOUT_DEFAULT()
			g_FMMC_STRUCT.sFMMCEndConditions[g_i_Mission_team].iPresetInventorySelection =  ciPRESET_INVENTORY__CASINO_HEIST_SUB_1
		ELSE
			g_FMMC_STRUCT.sFMMCEndConditions[g_i_Mission_team].iPresetInventorySelection =  ciPRESET_INVENTORY__CASINO_HEIST_SUB_2
		ENDIF
	ENDIF
	
	
	IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_WeaponExpertLoadout1)
		g_FMMC_STRUCT.sFMMCEndConditions[g_i_Mission_team].iPresetInventoryVariation = ciPRESET_INVENTORY_VAR_ONE
	
	ELIF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_WeaponExpertLoadout2)
		g_FMMC_STRUCT.sFMMCEndConditions[g_i_Mission_team].iPresetInventoryVariation = ciPRESET_INVENTORY_VAR_TWO
	
	ELIF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_WeaponExpertLoadout3)
		g_FMMC_STRUCT.sFMMCEndConditions[g_i_Mission_team].iPresetInventoryVariation = ciPRESET_INVENTORY_VAR_THREE
	
	ELIF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_WeaponExpertLoadout4)
		g_FMMC_STRUCT.sFMMCEndConditions[g_i_Mission_team].iPresetInventoryVariation = ciPRESET_INVENTORY_VAR_FOUR
	
	ELIF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_WeaponExpertLoadout5)
		g_FMMC_STRUCT.sFMMCEndConditions[g_i_Mission_team].iPresetInventoryVariation = ciPRESET_INVENTORY_VAR_FIVE
	ENDIF
	
	PRINTLN("[SCRIPT INIT][MissionVariation] - (Player Inventory) SET_PRESET_LOADOUT_FOR_CASINO_HEIST - g_FMMC_STRUCT.sFMMCEndConditions[g_i_Mission_team].iPresetInventorySelection: ", g_FMMC_STRUCT.sFMMCEndConditions[g_i_Mission_team].iPresetInventorySelection)
	PRINTLN("[SCRIPT INIT][MissionVariation] - (Player Inventory) SET_PRESET_LOADOUT_FOR_CASINO_HEIST - g_FMMC_STRUCT.sFMMCEndConditions[g_i_Mission_team].iPresetInventoryVariation: ", g_FMMC_STRUCT.sFMMCEndConditions[g_i_Mission_team].iPresetInventoryVariation)
ENDPROC

PROC SET_UP_PLAYER_WEAPON_LOADOUT_VARIATION()
	IF g_FMMC_STRUCT.sMissionVariation.eMCVarConfigEnum = FMMC_MC_VAR_CONFIG_ENUM_CASINO_HEIST
	AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_UseCasinoHeistInventoryVariations)
		g_i_Mission_team = 0
		PRINTLN("[SCRIPT INIT][MissionVariation] - (Player Inventory) SET_UP_PLAYER_WEAPON_LOADOUT_VARIATION: Setting g_i_Mission_team to 0")
		SET_WEAPON_LOADOUTS_FOR_CASINO_HEIST(g_FMMC_STRUCT.sFMMCEndConditions[g_i_Mission_team].iStartingInventory, g_FMMC_STRUCT.sFMMCEndConditions[g_i_Mission_team].iStartingInventoryTwo,
											g_FMMC_STRUCT.sFMMCEndConditions[g_i_Mission_team].iStartingInventoryThree, g_FMMC_STRUCT.sFMMCEndConditions[g_i_Mission_team].iStartingInventoryFour)
		PRINTLN("[SCRIPT INIT][MissionVariation] - (Player Inventory) SET_UP_PLAYER_WEAPON_LOADOUT_VARIATION: Inventory bitsets - BS0: ",g_FMMC_STRUCT.sFMMCEndConditions[g_i_Mission_team].iStartingInventory, " BS1: ",g_FMMC_STRUCT.sFMMCEndConditions[g_i_Mission_team].iStartingInventoryTwo)
		PRINTLN("[SCRIPT INIT][MissionVariation] - (Player Inventory) SET_UP_PLAYER_WEAPON_LOADOUT_VARIATION: Inventory bitsets - BS2: ",g_FMMC_STRUCT.sFMMCEndConditions[g_i_Mission_team].iStartingInventoryThree, " BS3: ",g_FMMC_STRUCT.sFMMCEndConditions[g_i_Mission_team].iStartingInventoryFour)
		
		//Setting mid mission loadout for subterfuge missions
		SET_SUBTERFUGE_WEAPON_LOADOUT(g_FMMC_STRUCT.sFMMCEndConditions[g_i_Mission_team].iMidMissionInventory, g_FMMC_STRUCT.sFMMCEndConditions[g_i_Mission_team].iMidMissionInventoryTwo,
									g_FMMC_STRUCT.sFMMCEndConditions[g_i_Mission_team].iMidMissionInventoryThree, g_FMMC_STRUCT.sFMMCEndConditions[g_i_Mission_team].iMidMissionInventoryFour)
		PRINTLN("[SCRIPT INIT][MissionVariation] - (Player Inventory) SET_UP_PLAYER_WEAPON_LOADOUT_VARIATION: Mid Mission Inventory bitsets - BS0: ",g_FMMC_STRUCT.sFMMCEndConditions[g_i_Mission_team].iMidMissionInventory, " BS1: ",g_FMMC_STRUCT.sFMMCEndConditions[g_i_Mission_team].iMidMissionInventoryTwo)
		PRINTLN("[SCRIPT INIT][MissionVariation] - (Player Inventory) SET_UP_PLAYER_WEAPON_LOADOUT_VARIATION: Mid Mission Inventory bitsets - BS2: ",g_FMMC_STRUCT.sFMMCEndConditions[g_i_Mission_team].iMidMissionInventoryThree, " BS3: ",g_FMMC_STRUCT.sFMMCEndConditions[g_i_Mission_team].iMidMissionInventoryFour)
		
		SET_PRESET_LOADOUT_FOR_CASINO_HEIST()
	
	ENDIF
ENDPROC

FUNC BOOL IS_MISSION_VARIATION_USING_NIGHT_VISION_TOGGLING()
	IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_ApproachStealth)
	AND IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EquipNightVisionOutfit)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC INT GET_DIALOGUE_TRIGGER_REQUIREMENT_INT_FROM_BOOL(BOOL bBool)
	IF bBool
		RETURN 1
	ENDIF
	RETURN 2
ENDFUNC

FUNC BOOL DOES_DIALOGUE_TRIGGER_REQUIREMENT_MATCH_BIT_SET(INT iDTReq, INT iBSBit, INT iMod = 1)
	iDTReq -= iMod
	RETURN IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, iBSBit+iDTReq)
ENDFUNC

FUNC BOOL DOES_DIALOGUE_TRIGGER_REQUIREMENT_MATCH_DROPOFF(INT iDTReq)
	SWITCH INT_TO_ENUM(CASINO_HEIST_DROPOFF_LOCATIONS, iDTReq-1)
		CASE CASINO_HEIST_DROPOFF_LOCATION__SHORT_1
			IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EndOfMissionDropOffOverride1)			
			AND IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EndOfMissionDropOffOverrideShort)
				RETURN FALSE
			ENDIF
		BREAK
		CASE CASINO_HEIST_DROPOFF_LOCATION__SHORT_2
			IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EndOfMissionDropOffOverride2)			
			AND IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EndOfMissionDropOffOverrideShort)
				RETURN FALSE
			ENDIF
		BREAK
		CASE CASINO_HEIST_DROPOFF_LOCATION__SHORT_3
			IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EndOfMissionDropOffOverride3)
			AND IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EndOfMissionDropOffOverrideShort)
				RETURN FALSE
			ENDIF
		BREAK
		CASE CASINO_HEIST_DROPOFF_LOCATION__MEDIUM_1
			IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EndOfMissionDropOffOverride1)
			AND IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EndOfMissionDropOffOverrideMedium)
				RETURN FALSE
			ENDIF
		BREAK
		CASE CASINO_HEIST_DROPOFF_LOCATION__MEDIUM_2
			IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EndOfMissionDropOffOverride2)
			AND IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EndOfMissionDropOffOverrideMedium)
				RETURN FALSE
			ENDIF		
		BREAK
		CASE CASINO_HEIST_DROPOFF_LOCATION__MEDIUM_3
			IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EndOfMissionDropOffOverride3)
			AND IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EndOfMissionDropOffOverrideMedium)
				RETURN FALSE
			ENDIF			
		BREAK
		CASE CASINO_HEIST_DROPOFF_LOCATION__LONG_1
			IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EndOfMissionDropOffOverride1)
			AND IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EndOfMissionDropOffOverrideLong)
				RETURN FALSE
			ENDIF			
		BREAK
		CASE CASINO_HEIST_DROPOFF_LOCATION__LONG_2
			IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EndOfMissionDropOffOverride2)
			AND IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EndOfMissionDropOffOverrideLong)
				RETURN FALSE
			ENDIF	
		BREAK
		CASE CASINO_HEIST_DROPOFF_LOCATION__LONG_3
			IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EndOfMissionDropOffOverride3)
			AND IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EndOfMissionDropOffOverrideLong)
				RETURN FALSE
			ENDIF				
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_THIS_DIALOGUE_TRIGGER_USING_REQUIREMENTS(INT iDialogueTrigger)
	RETURN (IS_USING_MISSION_VARIATION_DIALOGUE_TRIGGER_REQUIREMENTS()
	AND IS_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iDialogueBitset4, iBS_Dialogue4_VarsRequirementEnabled))
ENDFUNC

FUNC BOOL DOES_DIALOGUE_TRIGGER_REQUIREMENT_MATCH_DATA(INT iDialogueTrigger)

	IF IS_THIS_DIALOGUE_TRIGGER_USING_REQUIREMENTS(iDialogueTrigger)
		//Approach
		IF g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iVarRequirement[DT_CASINO_REQ_APPROACH] != 0
			IF NOT DOES_DIALOGUE_TRIGGER_REQUIREMENT_MATCH_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iVarRequirement[DT_CASINO_REQ_APPROACH], MCVarConfigBS_ApproachStealth)
				PRINTLN("[SCRIPT INIT][MissionVariation] - DOES_DIALOGUE_TRIGGER_REQUIREMENT_MATCH_DATA - Dialogue Trigger: ", iDialogueTrigger, " doesn't meet requirement DT_CASINO_REQ_APPROACH")
				RETURN FALSE
			ENDIF
		ENDIF
		//Target
		IF g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iVarRequirement[DT_CASINO_REQ_TARGET] != 0
			IF NOT DOES_DIALOGUE_TRIGGER_REQUIREMENT_MATCH_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iVarRequirement[DT_CASINO_REQ_TARGET], MCVarConfigBS_Target_1)
				PRINTLN("[SCRIPT INIT][MissionVariation] - DOES_DIALOGUE_TRIGGER_REQUIREMENT_MATCH_DATA - Dialogue Trigger: ", iDialogueTrigger, " doesn't meet requirement DT_CASINO_REQ_TARGET")
				RETURN FALSE
			ENDIF
		ENDIF
		//Cameras
		IF g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iVarRequirement[DT_CASINO_REQ_CAMERAS] != 0
			IF g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iVarRequirement[DT_CASINO_REQ_CAMERAS] != GET_DIALOGUE_TRIGGER_REQUIREMENT_INT_FROM_BOOL(IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_BlipCCTVCams))
				PRINTLN("[SCRIPT INIT][MissionVariation] - DOES_DIALOGUE_TRIGGER_REQUIREMENT_MATCH_DATA - Dialogue Trigger: ", iDialogueTrigger, " doesn't meet requirement DT_CASINO_REQ_CAMERAS")
				RETURN FALSE
			ENDIF
		ENDIF
		//Guard Patrol
		IF g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iVarRequirement[DT_CASINO_REQ_PATROLS] != 0
			IF g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iVarRequirement[DT_CASINO_REQ_PATROLS] != GET_DIALOGUE_TRIGGER_REQUIREMENT_INT_FROM_BOOL(IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_BlipGuards))
				PRINTLN("[SCRIPT INIT][MissionVariation] - DOES_DIALOGUE_TRIGGER_REQUIREMENT_MATCH_DATA - Dialogue Trigger: ", iDialogueTrigger, " doesn't meet requirement DT_CASINO_REQ_PATROLS")
				RETURN FALSE
			ENDIF
		ENDIF
		//Shipment Disruption
		IF g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iVarRequirement[DT_CASINO_REQ_DISRUPTION] != 0
			IF NOT DOES_DIALOGUE_TRIGGER_REQUIREMENT_MATCH_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iVarRequirement[DT_CASINO_REQ_DISRUPTION], MCVarConfigBS_PedHealthPercentMod_1)
			AND NOT DOES_DIALOGUE_TRIGGER_REQUIREMENT_MATCH_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iVarRequirement[DT_CASINO_REQ_DISRUPTION], MCVarConfigBS_InventoryOverride_1)
				PRINTLN("[SCRIPT INIT][MissionVariation] - DOES_DIALOGUE_TRIGGER_REQUIREMENT_MATCH_DATA - Dialogue Trigger: ", iDialogueTrigger, " doesn't meet requirement DT_CASINO_REQ_DISRUPTION")
				RETURN FALSE
			ENDIF
		ENDIF
		//Stealth Outfit
		IF g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iVarRequirement[DT_CASINO_REQ_STEALTH_OUTFIT] != 0
			IF g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iVarRequirement[DT_CASINO_REQ_STEALTH_OUTFIT] != GET_DIALOGUE_TRIGGER_REQUIREMENT_INT_FROM_BOOL(IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EquipNightVisionOutfit))
				PRINTLN("[SCRIPT INIT][MissionVariation] - DOES_DIALOGUE_TRIGGER_REQUIREMENT_MATCH_DATA - Dialogue Trigger: ", iDialogueTrigger, " doesn't meet requirement DT_CASINO_REQ_STEALTH_OUTFIT")
				RETURN FALSE
			ENDIF
		ENDIF
		//Handheld Drill
		IF g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iVarRequirement[DT_CASINO_REQ_HANDHELD_DRILL] != 0
			IF g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iVarRequirement[DT_CASINO_REQ_HANDHELD_DRILL] != GET_DIALOGUE_TRIGGER_REQUIREMENT_INT_FROM_BOOL(IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_HasHandheldDrill))
				PRINTLN("[SCRIPT INIT][MissionVariation] - DOES_DIALOGUE_TRIGGER_REQUIREMENT_MATCH_DATA - Dialogue Trigger: ", iDialogueTrigger, " doesn't meet requirement DT_CASINO_REQ_HANDHELD_DRILL")
				RETURN FALSE
			ENDIF
		ENDIF
		//EMP
		IF g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iVarRequirement[DT_CASINO_REQ_EMP] != 0
			IF g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iVarRequirement[DT_CASINO_REQ_EMP] != GET_DIALOGUE_TRIGGER_REQUIREMENT_INT_FROM_BOOL(IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_HasEMP))
				PRINTLN("[SCRIPT INIT][MissionVariation] - DOES_DIALOGUE_TRIGGER_REQUIREMENT_MATCH_DATA - Dialogue Trigger: ", iDialogueTrigger, " doesn't meet requirement DT_CASINO_REQ_EMP")
				RETURN FALSE
			ENDIF
		ENDIF
		//Dropoff
		IF g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iVarRequirement[DT_CASINO_REQ_DROPOFF] != 0
			IF NOT DOES_DIALOGUE_TRIGGER_REQUIREMENT_MATCH_DROPOFF(g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iVarRequirement[DT_CASINO_REQ_DROPOFF])
				PRINTLN("[SCRIPT INIT][MissionVariation] - DOES_DIALOGUE_TRIGGER_REQUIREMENT_MATCH_DATA - Dialogue Trigger: ", iDialogueTrigger, " doesn't meet requirement DT_CASINO_REQ_DROPOFF")
				RETURN FALSE
			ENDIF
		ENDIF
		//Decoy
		IF g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iVarRequirement[DT_CASINO_REQ_DECOY] != 0
			IF g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iVarRequirement[DT_CASINO_REQ_DECOY] != GET_DIALOGUE_TRIGGER_REQUIREMENT_INT_FROM_BOOL(IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_DecoyCrewMemberActive))
				PRINTLN("[SCRIPT INIT][MissionVariation] - DOES_DIALOGUE_TRIGGER_REQUIREMENT_MATCH_DATA - Dialogue Trigger: ", iDialogueTrigger, " doesn't meet requirement DT_CASINO_REQ_DECOY")
				RETURN FALSE
			ENDIF
		ENDIF
		//Switch Veh
		IF g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iVarRequirement[DT_CASINO_REQ_SWITCH_GETAWAY] != 0
			IF g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iVarRequirement[DT_CASINO_REQ_SWITCH_GETAWAY] != GET_DIALOGUE_TRIGGER_REQUIREMENT_INT_FROM_BOOL(IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SwitchGetawayVehicleActive))
				PRINTLN("[SCRIPT INIT][MissionVariation] - DOES_DIALOGUE_TRIGGER_REQUIREMENT_MATCH_DATA - Dialogue Trigger: ", iDialogueTrigger, " doesn't meet requirement DT_CASINO_REQ_SWITCH_GETAWAY")
				RETURN FALSE
			ENDIF
		ENDIF
		//Weapon Expert
		IF g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iVarRequirement[DT_CASINO_REQ_WEAPON_EXPERT] != 0
			IF NOT DOES_DIALOGUE_TRIGGER_REQUIREMENT_MATCH_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iVarRequirement[DT_CASINO_REQ_WEAPON_EXPERT], MCVarConfigBS_WeaponExpertLoadout1)
				PRINTLN("[SCRIPT INIT][MissionVariation] - DOES_DIALOGUE_TRIGGER_REQUIREMENT_MATCH_DATA - Dialogue Trigger: ", iDialogueTrigger, " doesn't meet requirement DT_CASINO_REQ_WEAPON_EXPERT")
				RETURN FALSE
			ENDIF
		ENDIF
		//Weapon Loadout
		IF g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iVarRequirement[DT_CASINO_REQ_WEAPON_LOADOUT] != 0
			IF g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iVarRequirement[DT_CASINO_REQ_WEAPON_LOADOUT] != GET_DIALOGUE_TRIGGER_REQUIREMENT_INT_FROM_BOOL(IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_AlternativeWeaponLoadout))
				PRINTLN("[SCRIPT INIT][MissionVariation] - DOES_DIALOGUE_TRIGGER_REQUIREMENT_MATCH_DATA - Dialogue Trigger: ", iDialogueTrigger, " doesn't meet requirement DT_CASINO_REQ_WEAPON_LOADOUT")
				RETURN FALSE
			ENDIF
		ENDIF
		//Driver
		IF g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iVarRequirement[DT_CASINO_REQ_DRIVER] != 0
			IF NOT DOES_DIALOGUE_TRIGGER_REQUIREMENT_MATCH_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iVarRequirement[DT_CASINO_REQ_DRIVER], MCVarConfigBS_DriverChosen1)
				PRINTLN("[SCRIPT INIT][MissionVariation] - DOES_DIALOGUE_TRIGGER_REQUIREMENT_MATCH_DATA - Dialogue Trigger: ", iDialogueTrigger, " doesn't meet requirement DT_CASINO_REQ_DRIVER")
				RETURN FALSE
			ENDIF
		ENDIF
		//Vehicle Selection
		IF g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iVarRequirement[DT_CASINO_REQ_VEHICLE_SELECTION] != 0
			IF NOT DOES_DIALOGUE_TRIGGER_REQUIREMENT_MATCH_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iVarRequirement[DT_CASINO_REQ_VEHICLE_SELECTION], MCVarConfigBS_DriverVehAlt1)
				PRINTLN("[SCRIPT INIT][MissionVariation] - DOES_DIALOGUE_TRIGGER_REQUIREMENT_MATCH_DATA - Dialogue Trigger: ", iDialogueTrigger, " doesn't meet requirement DT_CASINO_REQ_VEHICLE_SELECTION")
				RETURN FALSE
			ENDIF
		ENDIF
		//Hacker
		IF g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iVarRequirement[DT_CASINO_REQ_HACKER] != 0
			IF NOT DOES_DIALOGUE_TRIGGER_REQUIREMENT_MATCH_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iVarRequirement[DT_CASINO_REQ_HACKER], MCVarConfigBS_HackerChosen1)
				PRINTLN("[SCRIPT INIT][MissionVariation] - DOES_DIALOGUE_TRIGGER_REQUIREMENT_MATCH_DATA - Dialogue Trigger: ", iDialogueTrigger, " doesn't meet requirement DT_CASINO_REQ_HACKER")
				RETURN FALSE
			ENDIF
		ENDIF
		//Keycard Access
		IF g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iVarRequirement[DT_CASINO_REQ_KEYCARD] != 0
			PRINTLN("[SCRIPT INIT][MissionVariation] - DOES_DIALOGUE_TRIGGER_REQUIREMENT_MATCH_DATA - DT_CASINO_REQ_KEYCARD - Dialogue Trigger: ", iDialogueTrigger, " Set as: ", g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iVarRequirement[DT_CASINO_REQ_KEYCARD])
			
			IF g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iVarRequirement[DT_CASINO_REQ_KEYCARD] = 1
				IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_KeycardAccessLevel1)
				OR IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_KeycardAccessLevel2)
					PRINTLN("[SCRIPT INIT][MissionVariation] - DOES_DIALOGUE_TRIGGER_REQUIREMENT_MATCH_DATA - Dialogue Trigger: ", iDialogueTrigger, " doesn't meet requirement DT_CASINO_REQ_KEYCARD - None Set")
					RETURN FALSE
				ENDIF
			ELSE
				IF NOT DOES_DIALOGUE_TRIGGER_REQUIREMENT_MATCH_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iVarRequirement[DT_CASINO_REQ_KEYCARD], MCVarConfigBS_KeycardAccessLevel1, 2)
					PRINTLN("[SCRIPT INIT][MissionVariation] - DOES_DIALOGUE_TRIGGER_REQUIREMENT_MATCH_DATA - Dialogue Trigger: ", iDialogueTrigger, " doesn't meet requirement DT_CASINO_REQ_KEYCARD")
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
		//Entrance
		IF g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iVarRequirement[DT_CASINO_REQ_ENTRANCE] != 0
			IF NOT DOES_DIALOGUE_TRIGGER_REQUIREMENT_MATCH_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iVarRequirement[DT_CASINO_REQ_ENTRANCE], MCVarConfigBS_SelectedEntrance1)			
				PRINTLN("[SCRIPT INIT][MissionVariation] - DOES_DIALOGUE_TRIGGER_REQUIREMENT_MATCH_DATA - Dialogue Trigger: ", iDialogueTrigger, " doesn't meet requirement DT_CASINO_REQ_ENTRANCE")
				RETURN FALSE
			ENDIF
		ENDIF
		//Exit
		IF g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iVarRequirement[DT_CASINO_REQ_EXIT] != 0
			IF NOT DOES_DIALOGUE_TRIGGER_REQUIREMENT_MATCH_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iVarRequirement[DT_CASINO_REQ_EXIT], MCVarConfigBS_SelectedExit1)
				PRINTLN("[SCRIPT INIT][MissionVariation] - DOES_DIALOGUE_TRIGGER_REQUIREMENT_MATCH_DATA - Dialogue Trigger: ", iDialogueTrigger, " doesn't meet requirement DT_CASINO_REQ_EXIT")
				RETURN FALSE
			ENDIF
		ENDIF
		//Outfit In
		IF g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iVarRequirement[DT_CASINO_REQ_OUTFIT_IN] != 0
			IF NOT DOES_DIALOGUE_TRIGGER_REQUIREMENT_MATCH_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iVarRequirement[DT_CASINO_REQ_OUTFIT_IN], MCVarConfigBS_OutfitBugstar)
				PRINTLN("[SCRIPT INIT][MissionVariation] - DOES_DIALOGUE_TRIGGER_REQUIREMENT_MATCH_DATA - Dialogue Trigger: ", iDialogueTrigger, " doesn't meet requirement DT_CASINO_REQ_OUTFIT_IN")
				RETURN FALSE
			ENDIF
		ENDIF
		//Outfit Out
		IF g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iVarRequirement[DT_CASINO_REQ_OUTFIT_OUT] != 0
			IF g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iVarRequirement[DT_CASINO_REQ_OUTFIT_OUT] = -1
				IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EscapeOutfitNoose)
				OR IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EscapeOutfitFireman)
				OR IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EscapeOutfitHighRoller)
					PRINTLN("[SCRIPT INIT][MissionVariation] - DOES_DIALOGUE_TRIGGER_REQUIREMENT_MATCH_DATA - Dialogue Trigger: ", iDialogueTrigger, " doesn't meet requirement DT_CASINO_REQ_OUTFIT_OUT (none selected)")
					RETURN FALSE
				ENDIF
			ELSE
				IF NOT DOES_DIALOGUE_TRIGGER_REQUIREMENT_MATCH_BIT_SET(g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iVarRequirement[DT_CASINO_REQ_OUTFIT_OUT], MCVarConfigBS_EscapeOutfitNoose)
					PRINTLN("[SCRIPT INIT][MissionVariation] - DOES_DIALOGUE_TRIGGER_REQUIREMENT_MATCH_DATA - Dialogue Trigger: ", iDialogueTrigger, " doesn't meet requirement DT_CASINO_REQ_OUTFIT_OUT (Outfit selected)")
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
		//Infested
		IF g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iVarRequirement[DT_CASINO_REQ_INFESTED] != 0
			IF g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iVarRequirement[DT_CASINO_REQ_INFESTED] != GET_DIALOGUE_TRIGGER_REQUIREMENT_INT_FROM_BOOL(IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_OfficeInfested))
				PRINTLN("[SCRIPT INIT][MissionVariation] - DOES_DIALOGUE_TRIGGER_REQUIREMENT_MATCH_DATA - Dialogue Trigger: ", iDialogueTrigger, " doesn't meet requirement DT_CASINO_REQ_INFESTED")
				RETURN FALSE
			ENDIF
		ENDIF
		//Rappel Gear
		IF g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iVarRequirement[DT_CASINO_REQ_RAPPEL_GEAR] != 0
			IF g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iVarRequirement[DT_CASINO_REQ_RAPPEL_GEAR] != GET_DIALOGUE_TRIGGER_REQUIREMENT_INT_FROM_BOOL(IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_HasRappelGear))
				PRINTLN("[SCRIPT INIT][MissionVariation] - DOES_DIALOGUE_TRIGGER_REQUIREMENT_MATCH_DATA - Dialogue Trigger: ", iDialogueTrigger, " doesn't meet requirement DT_CASINO_REQ_RAPPEL_GEAR")
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

// [MissionVariation] Non-Generic System functions. ================================================================================= END

// [MissionVariation] helper functions. ================================================================================= START
FUNC BOOL DOES_MISSION_VARIATION_SPAWN_FROM_ASSOCIATED_SELECTION_MATCH(INT iSelection, INT iSelectionType)
	SWITCH iSelectionType
		CASE ciMCVar_ASSOCIATED_SELECTION_CASINO_DRIVER
			SWITCH iSelection
				CASE 0
					IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_DriverChosen1)
						RETURN TRUE
					ENDIF
				BREAK
				CASE 1
					IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_DriverChosen2)
						RETURN TRUE
					ENDIF
				BREAK
				CASE 2
					IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_DriverChosen3)
						RETURN TRUE
					ENDIF
				BREAK
				CASE 3
					IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_DriverChosen4)
						RETURN TRUE
					ENDIF
				BREAK
				CASE 4
					IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_DriverChosen5)
						RETURN TRUE
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE ciMCVar_ASSOCIATED_SELECTION_CASINO_GUNNER
			SWITCH iSelection
				CASE 0
					IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_WeaponExpertLoadout1)
						RETURN TRUE
					ENDIF
				BREAK
				CASE 1
					IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_WeaponExpertLoadout2)
						RETURN TRUE
					ENDIF
				BREAK
				CASE 2
					IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_WeaponExpertLoadout3)
						RETURN TRUE
					ENDIF
				BREAK
				CASE 3
					IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_WeaponExpertLoadout4)
						RETURN TRUE
					ENDIF
				BREAK
				CASE 4
					IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_WeaponExpertLoadout5)
						RETURN TRUE
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_MISSION_VARIATION_PREREQUISITE_MISSION_ELEMENT_BEEN_COMPLETED(INT iPreReqMission)
	SWITCH iPreReqMission
		CASE 0					RETURN IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_OfficeInfested)
		CASE 1					RETURN IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_BlipCCTVCams)
		CASE 2					RETURN IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EquipNightVisionOutfit)
		CASE 3					RETURN IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_HasHandheldDrill)
		CASE 4					RETURN IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_HasEMP)
		CASE 5					RETURN IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_HasRappelGear)
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

// Enemy Health Percent Override ------------------------------------------------------------------------------------
// IS_USING_MISSION_VARIATION_ENEMY_HEALTH_OVERRIDE() (x:\gta5\script\dev_ng\multiplayer\include\public\freemode\fmmc_vars.sch)
FUNC INT GET_MISSION_VARIATION_ENEMY_HEALTH_OVERRIDE_INDEX()
	IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_PedHealthPercentMod_3)
		RETURN 2
	ENDIF
	IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_PedHealthPercentMod_2)
		RETURN 1
	ENDIF
	IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_PedHealthPercentMod_1)
		RETURN 0
	ENDIF
	
	RETURN -1
ENDFUNC
FUNC INT GET_MISSION_VARIATION_ENEMY_HEALTH_OVERRIDE()
	INT iIndex = GET_MISSION_VARIATION_ENEMY_HEALTH_OVERRIDE_INDEX()
	IF iIndex < 0
	OR iIndex >= FMMC_MAX_MC_VAR_SIZE_ENEMY_HEALTH
		RETURN -1
	ENDIF
	RETURN g_FMMC_STRUCT.sMissionVariation.iVarEnemyHealthPercent[iIndex]
ENDFUNC
FUNC BOOL IS_THIS_PED_USING_MISSION_VARIATION_ENEMY_HEALTH_OVERRIDE(INT iPed)
	RETURN (IS_USING_MISSION_VARIATION_ENEMY_HEALTH_OVERRIDE()
			AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].ipedBitsetThirteen, ciPED_BSThirteen_MissionVariation_EnableHealthOverride) 
			AND GET_MISSION_VARIATION_ENEMY_HEALTH_OVERRIDE_INDEX() > -1)
ENDFUNC

// Enemy Crit Immunity Override ------------------------------------------------------------------------------------
// IS_USING_MISSION_VARIATION_ENEMY_CRIT_IMMUNITY_OVERRIDE() (x:\gta5\script\dev_ng\multiplayer\include\public\freemode\fmmc_vars.sch)
FUNC BOOL IS_THIS_PED_USING_MISSION_VARIATION_ENEMY_DISABLE_CRIT_IMMUNITY(INT iPed)	
	IF IS_USING_MISSION_VARIATION_ENEMY_CRIT_IMMUNITY_OVERRIDE()
	AND IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_PedDisableImmuneToCriticalHits)
	AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].ipedBitsetFourteen, ciPED_BSFourteen_MissionVariationDisableCriticalImmunity)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC
	
// Enemy Equipped Weapon Index Override ------------------------------------------------------------------------------------
// IS_USING_MISSION_VARIATION_ENEMY_INVENTORY_OVERRIDE()  (x:\gta5\script\dev_ng\multiplayer\include\public\freemode\fmmc_vars.sch)
FUNC INT GET_MISSION_VARIATION_ENEMY_INVENTORY_OVERRIDE_INDEX(INT iPed)

	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].ipedBitsetThirteen, ciPED_BSThirteen_MissionVariation_EnableInventoryOverride)
		IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_InventoryOverride_3)
			RETURN 4
		ENDIF
		IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_InventoryOverride_2)
			RETURN 2
		ENDIF	
		IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_InventoryOverride_1)
			RETURN 0
		ENDIF
	ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].ipedBitsetThirteen, ciPED_BSThirteen_MissionVariation_EnableInventoryOverrideSecondary)
		IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_InventoryOverride_3)
			RETURN 5
		ENDIF
		IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_InventoryOverride_2)
			RETURN 3
		ENDIF	
		IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_InventoryOverride_1)
			RETURN 1
		ENDIF
	ENDIF
	
	RETURN -1
ENDFUNC
FUNC INT GET_MISSION_VARIATION_ENEMY_INVENTORY_OVERRIDE(INT iPed)
	INT iIndex = GET_MISSION_VARIATION_ENEMY_INVENTORY_OVERRIDE_INDEX(iPed)
	IF iIndex < 0
	OR iIndex >= FMMC_MAX_MC_VAR_SIZE_ENEMY_INVENTORY
		RETURN -1
	ENDIF
	RETURN g_FMMC_STRUCT.sMissionVariation.iVarEnemyInventoryIndex[iIndex]
ENDFUNC
FUNC BOOL IS_THIS_PED_USING_MISSION_VARIATION_ENEMY_INVENTORY_OVERRIDE(INT iPed)
	RETURN (IS_USING_MISSION_VARIATION_ENEMY_INVENTORY_OVERRIDE() AND GET_MISSION_VARIATION_ENEMY_INVENTORY_OVERRIDE_INDEX(iPed) > -1 AND GET_MISSION_VARIATION_ENEMY_INVENTORY_OVERRIDE(iPed) != -1)
ENDFUNC

// Ped Blip Override ------------------------------------------------------------------------------------
// IS_USING_MISSION_VARIATION_PED_BLIP_ALL_OVERRIDE()  (x:\gta5\script\dev_ng\multiplayer\include\public\freemode\fmmc_vars.sch)

FUNC BOOL IS_MISSION_VARIATION_ENEMY_ALL_BLIP_OVERRIDE_ACTIVE()
		
	IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_BlipGuards)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_THIS_MISSION_USING_MISSION_VARIATION_ENEMY_ALL_BLIP_OVERRIDE()
	
	IF NOT IS_USING_MISSION_VARIATION_PED_BLIP_ALL_OVERRIDE()
		RETURN FALSE
	ENDIF
	
	IF NOT IS_MISSION_VARIATION_ENEMY_ALL_BLIP_OVERRIDE_ACTIVE()
		RETURN FALSE
	ENDIF

	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sMissionVariation.iVarBSOne, ci_MCVarBsOne_BlipAllPlacedPeds) 
	AND NOT IS_BIT_SET(g_FMMC_STRUCT.sMissionVariation.iVarBSOne, ci_MCVarBsOne_BlipAllPlacedPedsConditional)
		RETURN FALSE
	ENDIF
			
	RETURN TRUE
ENDFUNC
FUNC BOOL IS_THIS_PED_USING_MISSION_VARIATION_ENEMY_ALL_BLIP_OVERRIDE(INT iPed)
	
	IF NOT IS_THIS_MISSION_USING_MISSION_VARIATION_ENEMY_ALL_BLIP_OVERRIDE()
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sMissionVariation.iVarBSOne, ci_MCVarBsOne_BlipAllPlacedPedsConditional) AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].ipedBitsetThirteen, ciPED_BSThirteen_MissionVariation_EnableBlipForcedConditional)
		RETURN FALSE
	ENDIF
			
	RETURN TRUE
ENDFUNC

// Vehicle Block Spawn Override ------------------------------------------------------------------------------------
// IS_USING_MISSION_VARIATION_VEHICLE_BLOCK_SPAWN_OVERRIDE()  (x:\gta5\script\dev_ng\multiplayer\include\public\freemode\fmmc_vars.sch)
FUNC BOOL GET_MISSION_VARIATION_SHOULD_BLOCK_SPAWN_VEHICLE()

	IF NOT IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SwitchGetawayVehicleActive)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC
FUNC BOOL IS_THIS_VEHICLE_USING_MISSION_VARIATION_VEHICLE_BLOCK_SPAWN_OVERRIDE(INT iVeh)
	RETURN (IS_USING_MISSION_VARIATION_VEHICLE_BLOCK_SPAWN_OVERRIDE()
			AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetEight, ciFMMC_VEHICLE8_BlockSpawnForMissionVariation) 
			AND GET_MISSION_VARIATION_SHOULD_BLOCK_SPAWN_VEHICLE())
ENDFUNC
/// Checks if the vehicle is set up as a switch/clean vehicle.
FUNC BOOL IS_THIS_VEHICLE_A_SWITCH_VEHICLE(INT iVeh) 
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBlockWantedConeResponse = ci_VEHICLE_BLOCK_WANTED_CONE_RESPONSE_On
	OR g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBlockWantedConeResponse = ci_VEHICLE_BLOCK_WANTED_CONE_RESPONSE_UntilIllegal
		PRINTLN("[IS_THIS_VEHICLE_A_SWITCH_VEHICLE] Returning TRUE")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

// Vehicle Model Swap ------------------------------------------------------------------------------------
// IS_USING_MISSION_VARIATION_VEHICLE_MODEL_SWAP_OVERRIDE()  (x:\gta5\script\dev_ng\multiplayer\include\public\freemode\fmmc_vars.sch)
FUNC INT GET_MISSION_VARIATION_VEHICLE_MODEL_SWAP_OVERRIDE_INDEX(INT iVeh)
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetEight, ciFMMC_VEHICLE8_ModelSwapCandidateForMissionVariation_1) 
		IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_DriverChosen1)
			IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_DriverVehAlt3)
				RETURN 2
			ENDIF
			IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_DriverVehAlt2)
				RETURN 1
			ENDIF	
			IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_DriverVehAlt1)
				RETURN 0
			ENDIF
		ENDIF
		IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_DriverChosen2)
			IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_DriverVehAlt3)
				RETURN 5
			ENDIF
			IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_DriverVehAlt2)
				RETURN 4
			ENDIF	
			IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_DriverVehAlt1)
				RETURN 3
			ENDIF
		ENDIF
		IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_DriverChosen3)
			IF IS_THIS_VEHICLE_A_SWITCH_VEHICLE(iVeh)
				//Only swap the vehicle model for the switch car if the Kuruma is being used. 
				IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_DriverVehAlt4_FromJobComplete)
					RETURN 9
				ENDIF
			ELSE
				IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_DriverVehAlt4_FromJobComplete)
					RETURN 9
				ENDIF
				IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_DriverVehAlt3)
					RETURN 8
				ENDIF
				IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_DriverVehAlt2)
					RETURN 7
				ENDIF	
				IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_DriverVehAlt1)
					RETURN 6
				ENDIF
			ENDIF
		ENDIF
		IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_DriverChosen4)
			IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_DriverVehAlt3)
				RETURN 12
			ENDIF
			IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_DriverVehAlt2)
				RETURN 11
			ENDIF	
			IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_DriverVehAlt1)
				RETURN 10
			ENDIF
		ENDIF
		IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_DriverChosen5)
			IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_DriverVehAlt3)
				RETURN 15
			ENDIF
			IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_DriverVehAlt2)
				RETURN 14
			ENDIF	
			IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_DriverVehAlt1)
				RETURN 13
			ENDIF
		ENDIF
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetEight, ciFMMC_VEHICLE8_ModelSwapCandidateForMissionVariation_2) 
		IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_OutfitBugstar)
			RETURN 0
		ENDIF
		IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_OutfitMechanic)
			RETURN 1
		ENDIF
		IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_OutfitGruppeSechs)
			RETURN 2
		ENDIF
		IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_OutfitBrucie)
			RETURN 3
		ENDIF	
	ENDIF
	
	RETURN -1
ENDFUNC
FUNC BOOL IS_THIS_VEHICLE_USING_MISSION_VARIATION_MODEL_SWAP_CREATOR_SETTINGS(INT iVeh)
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetEight, ciFMMC_VEHICLE8_ModelSwapCandidateForMissionVariation_1) 
	OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetEight, ciFMMC_VEHICLE8_ModelSwapCandidateForMissionVariation_2) 
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC
FUNC BOOL IS_THIS_VEHICLE_USING_MISSION_VARIATION_VEHICLE_MODEL_SWAP_OVERRIDE(INT iVeh)
	RETURN (IS_USING_MISSION_VARIATION_VEHICLE_MODEL_SWAP_OVERRIDE()
			AND IS_THIS_VEHICLE_USING_MISSION_VARIATION_MODEL_SWAP_CREATOR_SETTINGS(iVeh)
			AND GET_MISSION_VARIATION_VEHICLE_MODEL_SWAP_OVERRIDE_INDEX(iVeh) > -1)
ENDFUNC

// Controler actions this.
FUNC MODEL_NAMES GET_MISSION_VARIATION_VEHICLE_MODEL_SWAP_OVERRIDE(INT iVeh)
	INT iIndex = GET_MISSION_VARIATION_VEHICLE_MODEL_SWAP_OVERRIDE_INDEX(iVeh)
	IF iIndex < 0
	OR iIndex >= FMMC_MAX_MC_VAR_SIZE_VEHICLE_MODEL_SWAP
		RETURN DUMMY_MODEL_FOR_SCRIPT
	ENDIF
	RETURN INT_TO_ENUM(MODEL_NAMES, g_FMMC_STRUCT.sMissionVariation.iVarVehicleModelSwapIndex[iIndex])
ENDFUNC

FUNC INT GET_MISSION_VARIATION_VEHICLE_BLIP_SPRITE_OVERRIDE(INT iVeh)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn = ZHABA
		PRINTLN("[GET_MISSION_VARIATION_VEHICLE_BLIP_SPRITE_OVERRIDE] Returning FMMC_BLIP_SPRITE_ZHABA")
		RETURN FMMC_BLIP_SPRITE_ZHABA
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn = STRYDER
		PRINTLN("[GET_MISSION_VARIATION_VEHICLE_BLIP_SPRITE_OVERRIDE] Returning FMMC_BLIP_SPRITE_GANG_BIKE")
		RETURN FMMC_BLIP_SPRITE_GANG_BIKE
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn = FURIA
		PRINTLN("[GET_MISSION_VARIATION_VEHICLE_BLIP_SPRITE_OVERRIDE] Returning FMMC_BLIP_SPRITE_SPORTS_CAR")
		RETURN FMMC_BLIP_SPRITE_SPORTS_CAR
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn = VAGRANT
		PRINTLN("[GET_MISSION_VARIATION_VEHICLE_BLIP_SPRITE_OVERRIDE] Returning FMMC_BLIP_SPRITE_BUGGY_1")
		RETURN FMMC_BLIP_SPRITE_BUGGY_1
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn = OUTLAW
		PRINTLN("[GET_MISSION_VARIATION_VEHICLE_BLIP_SPRITE_OVERRIDE] Returning FMMC_BLIP_SPRITE_BUGGY_2")
		RETURN FMMC_BLIP_SPRITE_BUGGY_2
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn = EVERON
		PRINTLN("[GET_MISSION_VARIATION_VEHICLE_BLIP_SPRITE_OVERRIDE] Returning FMMC_BLIP_SPRITE_SNOW_TRUCK")
		RETURN FMMC_BLIP_SPRITE_SNOW_TRUCK
	ELIF IS_THIS_MODEL_A_BIKE(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn)
		PRINTLN("[GET_MISSION_VARIATION_VEHICLE_BLIP_SPRITE_OVERRIDE] Returning FMMC_BLIP_SPRITE_GANG_BIKE")
		RETURN FMMC_BLIP_SPRITE_GANG_BIKE
	ENDIF
	
	PRINTLN("[GET_MISSION_VARIATION_VEHICLE_BLIP_SPRITE_OVERRIDE] Returning default FMMC_BLIP_SPRITE_GETAWAY_CAR")
	RETURN FMMC_BLIP_SPRITE_GETAWAY_CAR

ENDFUNC

// Vehicle Block Spawn Override ------------------------------------------------------------------------------------
// IS_USING_MISSION_VARIATION_VEHICLE_SPAWN_FROM_ASSOCIATED_SELECTION_OVERRIDE()  (x:\gta5\script\dev_ng\multiplayer\include\public\freemode\fmmc_vars.sch)
FUNC BOOL IS_THIS_VEHICLE_USING_MISSION_VARIATION_SPAWN_FROM_ASSOCIATED_SELECTION_OVERRIDE(INT iVeh)
	IF NOT IS_USING_MISSION_VARIATION_VEHICLE_SPAWN_FROM_ASSOCIATED_SELECTION_OVERRIDE()
		RETURN FALSE
	ENDIF
	RETURN DOES_MISSION_VARIATION_SPAWN_FROM_ASSOCIATED_SELECTION_MATCH(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehMCVarAssociatedSpawnIndex, ciMCVar_ASSOCIATED_SELECTION_CASINO_DRIVER)
ENDFUNC

// Vehicle Blip Override ------------------------------------------------------------------------------------
// IS_USING_MISSION_VARIATION_VEHICLE_BLIP_OVERRIDE()  (x:\gta5\script\dev_ng\multiplayer\include\public\freemode\fmmc_vars.sch)
FUNC BOOL IS_THIS_VEHICLE_USING_MISSION_VARIATION_VEHICLE_BLIP_OVERRIDE(INT iVeh)
	IF IS_USING_MISSION_VARIATION_VEHICLE_BLIP_OVERRIDE()
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetEight, ciFMMC_VEHICLE8_SetBlipOverrideForMissionVariation) 
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

// Vehicle Radio Sequence Blocking -----------------------------------------------------------------------------
// IS_USING_MISSION_VARIATION_VEHICLE_RADIO_SEQUENCE_OVERRIDE()  (x:\gta5\script\dev_ng\multiplayer\include\public\freemode\fmmc_vars.sch)
FUNC BOOL IS_MISSION_VARIATION_BLOCKING_VEHICLE_RADIO_SEQUENCE()
	IF NOT IS_USING_MISSION_VARIATION_VEHICLE_RADIO_SEQUENCE_OVERRIDE()
		RETURN FALSE
	ENDIF
	
	IF NOT IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_ApproachSubterfuge)
		RETURN TRUE
	ENDIF
	
	IF NOT IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_OutfitBrucie)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

// Ped Block Spawn Override ------------------------------------------------------------------------------------
// IS_USING_MISSION_VARIATION_PED_ASSOCIATED_SPAWN_SELECTION_1_OVERRIDE()  (x:\gta5\script\dev_ng\multiplayer\include\public\freemode\fmmc_vars.sch)
FUNC BOOL IS_THIS_PED_USING_MISSION_VARIATION_SPAWN_FROM_ASSOCIATED_SELECTION_1_OVERRIDE(INT iPed)
	IF NOT IS_USING_MISSION_VARIATION_PED_ASSOCIATED_SPAWN_SELECTION_1_OVERRIDE()
		RETURN FALSE
	ENDIF
	RETURN DOES_MISSION_VARIATION_SPAWN_FROM_ASSOCIATED_SELECTION_MATCH(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedMCVarAssociatedSpawnIndex, ciMCVar_ASSOCIATED_SELECTION_CASINO_GUNNER)
ENDFUNC

// Ped Cop Decoy Override ------------------------------------------------------------------------------------
// IS_USING_MISSION_VARIATION_PED_COP_DECOY_OVERRIDE()  (x:\gta5\script\dev_ng\multiplayer\include\public\freemode\fmmc_vars.sch)
FUNC BOOL IS_THIS_PED_USING_MISSION_VARIATION_COP_DECOY_OVERRIDE(INT iPed)
	IF NOT IS_USING_MISSION_VARIATION_PED_COP_DECOY_OVERRIDE()
		RETURN FALSE
	ENDIF
	RETURN IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetThirteen, ciPED_BSThirteen_MissionVariation_UseCopDecoyOverride)
ENDFUNC

FUNC INT GET_MISSION_VARIATION_COP_DECOY_ACTIVE_TIME_OVERRIDE()
	
	IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_WeaponExpertLoadout1)   //CASINO_HEIST_WEAPON_EXPERT__KARL_ABOLAJI
		RETURN 50
	ELIF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_WeaponExpertLoadout2) //CASINO_HEIST_WEAPON_EXPERT__GUSTAVO_MOTA
		RETURN 70
	ELIF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_WeaponExpertLoadout3) //CASINO_HEIST_WEAPON_EXPERT__CHARLIE
		RETURN 70
	ELIF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_WeaponExpertLoadout4) //CASINO_HEIST_WEAPON_EXPERT__WEAPONS_EXPERT
		RETURN 90
	ELIF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_WeaponExpertLoadout5) //CASINO_HEIST_WEAPON_EXPERT__PACKIE_MCREARY
		RETURN 90
	ENDIF
	
	RETURN 30
ENDFUNC

FUNC INT GET_MISSION_VARIATION_COP_DECOY_LOSE_2_STAR_CHANCE_OVERRIDE()

	IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_WeaponExpertLoadout1)   //CASINO_HEIST_WEAPON_EXPERT__KARL_ABOLAJI
		RETURN 20
	ELIF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_WeaponExpertLoadout2) //CASINO_HEIST_WEAPON_EXPERT__GUSTAVO_MOTA
		RETURN 70
	ELIF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_WeaponExpertLoadout3) //CASINO_HEIST_WEAPON_EXPERT__CHARLIE
		RETURN 55
	ELIF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_WeaponExpertLoadout4) //CASINO_HEIST_WEAPON_EXPERT__WEAPONS_EXPERT
		RETURN 75
	ELIF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_WeaponExpertLoadout5) //CASINO_HEIST_WEAPON_EXPERT__PACKIE_MCREARY
		RETURN 80
	ENDIF
	
	RETURN 10
ENDFUNC

// Ped Armour Override ------------------------------------------------------------------------------------
// IS_THIS_PED_USING_MISSION_VARIATION_MODEL_VARIATION_OVERRIDE()  (x:\gta5\script\dev_ng\multiplayer\include\public\freemode\fmmc_vars.sch)
FUNC BOOL IS_THIS_PED_USING_MISSION_VARIATION_MODEL_VARIATION_OVERRIDE(INT iPed)
	IF NOT IS_USING_MISSION_VARIATION_PED_MODEL_VARIATION_OVERRIDE()
		PRINTLN("[ML][IS_THIS_PED_USING_MISSION_VARIATION_MODEL_VARIATION_OVERRIDE] Not using, so returning FALSE")
		RETURN FALSE
	ENDIF
	RETURN IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetThirteen, ciPED_BSThirteen_MissionVariation_UseModelVariationOverride)
ENDFUNC

FUNC INT GET_MISSION_VARIATION_MODEL_VARIATION_OVERRIDE()
	
	IF g_sCasinoHeistMissionConfigData.eShipmentDisruptionLevel = CASINO_HEIST_SHIPMENT_DISRUPTION_LEVEL__NONE
		PRINTLN("[ML][GET_MISSION_VARIATION_MODEL_VARIATION_OVERRIDE] Returning armour level 4")
		RETURN 4
	ELIF g_sCasinoHeistMissionConfigData.eShipmentDisruptionLevel = CASINO_HEIST_SHIPMENT_DISRUPTION_LEVEL__MINOR
		PRINTLN("[ML][GET_MISSION_VARIATION_MODEL_VARIATION_OVERRIDE] Returning armour level 3")
		RETURN 3
	ELIF g_sCasinoHeistMissionConfigData.eShipmentDisruptionLevel = CASINO_HEIST_SHIPMENT_DISRUPTION_LEVEL__MAJOR
		PRINTLN("[ML][GET_MISSION_VARIATION_MODEL_VARIATION_OVERRIDE] Returning armour level 2")
		RETURN 2
	ELIF g_sCasinoHeistMissionConfigData.eShipmentDisruptionLevel = CASINO_HEIST_SHIPMENT_DISRUPTION_LEVEL__SEVERE
		PRINTLN("[ML][GET_MISSION_VARIATION_MODEL_VARIATION_OVERRIDE] Returning armour level 1")
		RETURN 1
	ENDIF
	
	PRINTLN("[ML][GET_MISSION_VARIATION_MODEL_VARIATION_OVERRIDE] Returning armour level 0 (NOT SET), so random will be used")
	RETURN 0
ENDFUNC

// Reposition from Associated Selection ------------------------------------------------------------------------------------
FUNC INT GET_MISSION_VARIATION_REPOSITION_FROM_ASSOCIATED_SELECTION_INDEX()

	IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EndOfMissionDropOffOverride1)
		IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EndOfMissionDropOffOverrideShort)
			RETURN 0
		ENDIF
		IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EndOfMissionDropOffOverrideMedium)
			RETURN 3
		ENDIF
		IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EndOfMissionDropOffOverrideLong)
			RETURN 6
		ENDIF
	ENDIF	
	IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EndOfMissionDropOffOverride2)
		IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EndOfMissionDropOffOverrideShort)
			RETURN 1
		ENDIF
		IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EndOfMissionDropOffOverrideMedium)
			RETURN 4
		ENDIF
		IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EndOfMissionDropOffOverrideLong)
			RETURN 7
		ENDIF
	ENDIF	
	IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EndOfMissionDropOffOverride3)
		IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EndOfMissionDropOffOverrideShort)
			RETURN 2
		ENDIF
		IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EndOfMissionDropOffOverrideMedium)
			RETURN 5
		ENDIF
		IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EndOfMissionDropOffOverrideLong)
			RETURN 8
		ENDIF
	ENDIF
	
	RETURN -1
ENDFUNC
// Vehicle Reposition from Associated Selection ------------------------------------------------------------------------------------
// IS_USING_MISSION_VARIATION_VEHICLE_REPOSITION_FROM_ASSOCIATED_SELECTION_OVERRIDE()  (x:\gta5\script\dev_ng\multiplayer\include\public\freemode\fmmc_vars.sch)
FUNC BOOL IS_THIS_VEHICLE_USING_MISSION_VARIATION_REPOSITION_FROM_ASSOCIATED_SELECTION_OVERRIDE(INT iVeh)
	IF NOT IS_USING_MISSION_VARIATION_VEHICLE_REPOSITION_FROM_ASSOCIATED_SELECTION_OVERRIDE()
		RETURN FALSE
	ENDIF
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetEight, ciFMMC_VEHICLE8_UseAsociatedSelectionSpawnPosMissionVariation)
		RETURN FALSE
	ENDIF
	INT iIndex = GET_MISSION_VARIATION_REPOSITION_FROM_ASSOCIATED_SELECTION_INDEX()
	IF iIndex < -1
	OR iIndex > FMMC_MAX_RULE_VECTOR_WARPS
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC
// Ped Reposition from Associated Selection ------------------------------------------------------------------------------------
// IS_USING_MISSION_VARIATION_PED_REPOSITION_FROM_ASSOCIATED_SELECTION_OVERRIDE()  (x:\gta5\script\dev_ng\multiplayer\include\public\freemode\fmmc_vars.sch)
FUNC BOOL IS_THIS_PED_USING_MISSION_VARIATION_REPOSITION_FROM_ASSOCIATED_SELECTION_OVERRIDE(INT iPed)
	IF NOT IS_USING_MISSION_VARIATION_PED_REPOSITION_FROM_ASSOCIATED_SELECTION_OVERRIDE()
		RETURN FALSE
	ENDIF
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetThirteen, ciPED_BSThirteen_MissionVariation_UseAsociatedSelectionSpawnPos)
		RETURN FALSE
	ENDIF
	INT iIndex = GET_MISSION_VARIATION_REPOSITION_FROM_ASSOCIATED_SELECTION_INDEX()
	IF iIndex < -1
	OR iIndex > FMMC_MAX_RULE_VECTOR_WARPS
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

// Casino Keypad Minigame ------------------------------------------------------------------------------------ ((might move this to non-generic functions.))
// IS_USING_MISSION_VARIATION_CASINO_KEYPAD()  (x:\gta5\script\dev_ng\multiplayer\include\public\freemode\fmmc_vars.sch)
FUNC BOOL IS_THIS_OBJECT_USING_MISSION_VARIATION_CASINO_KEYPAD_MINIGAME(INT iObj)
	RETURN (IS_USING_MISSION_VARIATION_CASINO_KEYPAD()
	AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetThree, cibsOBJ3_MissionVariation_UseCasinoKeypadVariation))
ENDFUNC

//Laser drill		  ------------------------------------------------------------------------------------
FUNC BOOL GET_MISSION_VARIATION_HAS_LASER()
	RETURN  g_sCasinoHeistMissionConfigData.eChosenApproachType = CASINO_HEIST_APPROACH_TYPE__STEALTH 
ENDFUNC

// Change Clothes Minigame ------------------------------------------------------------------------------------
// IS_USING_MISSION_VARIATION_OBJECTS_CHANGE_CLOTHES()  (x:\gta5\script\dev_ng\multiplayer\include\public\freemode\fmmc_vars.sch)
FUNC BOOL IS_MISSION_VARIATION_OBJECTS_CHANGE_CLOTHES_ACTIVE()
	
	IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EscapeOutfitNoose)
	OR IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EscapeOutfitFireman)
	OR IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EscapeOutfitHighRoller)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC
FUNC BOOL IS_THIS_OBJECT_USING_MISSION_VARIATION_OBJECTS_CHANGE_CLOTHES(INT iObj)
	RETURN (IS_USING_MISSION_VARIATION_OBJECTS_CHANGE_CLOTHES()
	AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetThree, cibsOBJ3_MissionVariation_UseOutfitVariation))
	AND IS_MISSION_VARIATION_OBJECTS_CHANGE_CLOTHES_ACTIVE()
ENDFUNC

PROC SET_MISSION_VARIATION_OUT_OUTFIT_CHOICE(INT iObj)
	IF IS_THIS_OBJECT_USING_MISSION_VARIATION_OBJECTS_CHANGE_CLOTHES(iObj)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__CLOTHES
			IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EscapeOutfitNoose)
				g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iClothesChangeOutfit = ciChangeClothesNOOSE
			
			ELIF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EscapeOutfitFireman)
				g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iClothesChangeOutfit = ciChangeClothesFireman
			
			ELIF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EscapeOutfitHighRoller)
				g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iClothesChangeOutfit = ciChangeClothesHighroller
			ELSE
				g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iClothesChangeOutfit = ciChangeClothesNone
			ENDIF
		ENDIF
	ENDIF
ENDPROC

// Objects Scoped Out ------------------------------------------------------------------------------------
// IS_USING_MISSION_VARIATION_OBJECTS_SCOPED_OUT()  (x:\gta5\script\dev_ng\multiplayer\include\public\freemode\fmmc_vars.sch)

FUNC BOOL IS_MISSION_VARIATION_OBJECTS_SCOPED_OUT_ACTIVE()
	
	IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_BlipCCTVCams)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_THIS_OBJECT_USING_MISSION_VARIATION_OBJECTS_SCOPED_OUT(INT iObj)
	RETURN (IS_USING_MISSION_VARIATION_OBJECTS_SCOPED_OUT()
	AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetThree, cibsOBJ3_MissionVariation_ObjectScopedOut_1))
	AND IS_MISSION_VARIATION_OBJECTS_SCOPED_OUT_ACTIVE()
ENDFUNC

// Objects Target Swap ------------------------------------------------------------------------------------
//IS_USING_MISSION_VARIATION_OBJECTS_MODEL_SWAP() (x:\gta5\script\dev_ng\multiplayer\include\public\freemode\fmmc_vars.sch)
FUNC BOOL IS_MISSION_VARIATION_OBJECTS_TARGET_SWAP_ACTIVE()
	IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_Target_1)
	OR IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_Target_2)
	OR IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_Target_3)
	OR IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_Target_4)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_THIS_OBJECT_USING_MISSION_VARIATION_OBJECTS_TARGET_SWAP(INT iObj)
	RETURN IS_USING_MISSION_VARIATION_OBJECTS_MODEL_SWAP()
	AND IS_MISSION_VARIATION_OBJECTS_TARGET_SWAP_ACTIVE()
	AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetThree, cibsOBJ3_MissionVariation_ModelSwap)
ENDFUNC

FUNC INT GET_MISSION_VARIATION_OBJECT_TARGET_SWAP_INDEX()
	IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_Target_1)
		RETURN 0
	ELIF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_Target_2)
		RETURN 1
	ELIF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_Target_3)
		RETURN 2
	ELIF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_Target_4)
		RETURN 3
	ENDIF
	RETURN -1
ENDFUNC

FUNC BOOL SHOULD_MINIGAME_VARIATION_USE_SUBTYPE(INT iIndex)
	
	IF g_FMMC_STRUCT.sMissionVariation.sObjectSwap[iIndex].iMinigameSubType != -1
		IF g_FMMC_STRUCT.sMissionVariation.sObjectSwap[iIndex].iMinigameType = LEGACY_MINI_GAME_TYPE_INTERACT_WITH
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_HIGHEST_MINIGAME_PAINTING_ID()
	INT iObj, iHighestID
	iHighestID = -1
	FOR iObj = 0 TO FMMC_MAX_NUM_OBJECTS-1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPaintingIndex != -1
		AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPaintingIndex > iHighestID
			iHighestID = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPaintingIndex
		ENDIF
	ENDFOR
	
	IF iHighestID + 1 < ciFMMC_MAX_PAINTING_INDEX_LEGACY
		RETURN iHighestID + 1
	ENDIF
	RETURN -1
ENDFUNC

FUNC INT GET_CASINO_TROLLEY_MODEL_NUMBER(MODEL_NAMES mnCurrent)
	IF IS_CASH_TROLLEY(mnCurrent)
		IF mnCurrent = INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Ch_Cash_Trolly_01B"))
			RETURN 1
		ELIF mnCurrent = INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Ch_Cash_Trolly_01C"))
			RETURN 2
		ENDIF
	ELIF IS_DIAMOND_TROLLEY(mnCurrent)
		IF mnCurrent = INT_TO_ENUM(MODEL_NAMES, HASH("CH_PROP_DIAMOND_TROLLY_01B"))
			RETURN 1
		ELIF mnCurrent = INT_TO_ENUM(MODEL_NAMES, HASH("CH_PROP_DIAMOND_TROLLY_01C"))
			RETURN 2
		ENDIF
	ELIF IS_GOLD_TROLLEY(mnCurrent)
		IF mnCurrent = INT_TO_ENUM(MODEL_NAMES, HASH("CH_PROP_GOLD_TROLLY_01B"))
			RETURN 1
		ELIF mnCurrent = INT_TO_ENUM(MODEL_NAMES, HASH("CH_PROP_GOLD_TROLLY_01C"))
			RETURN 2
		ENDIF
	ENDIF
	
	RETURN 0
ENDFUNC

FUNC MODEL_NAMES GET_CASINO_TROLLEY_MODEL_FROM_NUMBER(INT iModelID, MODEL_NAMES mnToSwapTo)
	IF IS_CASH_TROLLEY(mnToSwapTo)
		SWITCH iModelID
			CASE 0 RETURN INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Ch_Cash_Trolly_01A"))
			CASE 1 RETURN INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Ch_Cash_Trolly_01B"))
			CASE 2 RETURN INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Ch_Cash_Trolly_01C"))
		ENDSWITCH
	ELIF IS_DIAMOND_TROLLEY(mnToSwapTo)
		SWITCH iModelID
			CASE 0 RETURN CH_PROP_DIAMOND_TROLLY_01A
			CASE 1 RETURN INT_TO_ENUM(MODEL_NAMES, HASH("CH_PROP_DIAMOND_TROLLY_01B"))
			CASE 2 RETURN INT_TO_ENUM(MODEL_NAMES, HASH("CH_PROP_DIAMOND_TROLLY_01C"))
		ENDSWITCH
	ELIF IS_GOLD_TROLLEY(mnToSwapTo)
		SWITCH iModelID
			CASE 0 RETURN CH_PROP_GOLD_TROLLY_01A
			CASE 1 RETURN INT_TO_ENUM(MODEL_NAMES, HASH("CH_PROP_GOLD_TROLLY_01B"))
			CASE 2 RETURN INT_TO_ENUM(MODEL_NAMES, HASH("CH_PROP_GOLD_TROLLY_01C"))
		ENDSWITCH
	ENDIF
	
	RETURN mnToSwapTo
ENDFUNC

FUNC INT GET_NUMBER_OF_STACKS_PER_TROLLEY()
	IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_Target_2)
		RETURN ciMAX_CASH_TROLLEY_GOLD_STACKS
	ENDIF
	RETURN ciMAX_CASH_TROLLEY_CASH_STACKS
ENDFUNC

FUNC INT GET_VALUE_PER_CASH_STACK()
	INT iTotalStacks = (GET_NUMBER_OF_STACKS_PER_TROLLEY() * iNumCashTrollies)
	FLOAT fTotalValue = GET_CASINO_HEIST_TARGET_VALUE(GET_PLAYER_CASINO_HEIST_TARGET(GB_GET_LOCAL_PLAYER_GANG_BOSS())) * GET_CASINO_HEIST_TARGET_VALUE_DIFFICULTY_MODIFIER(g_FMMC_STRUCT.iDifficulity)
	RETURN FLOOR(fTotalValue / iTotalStacks)
ENDFUNC

PROC SET_MISSION_VARIATION_OBJECT_TARGET_SWAP(INT iObj)
	INT iIndex = GET_MISSION_VARIATION_OBJECT_TARGET_SWAP_INDEX()
	g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].mn = GET_CASINO_TROLLEY_MODEL_FROM_NUMBER(GET_CASINO_TROLLEY_MODEL_NUMBER(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].mn), g_FMMC_STRUCT.sMissionVariation.sObjectSwap[iIndex].mnVarObjectSwap)
	g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iCashAmount = GET_VALUE_PER_CASH_STACK()
	
	IF g_FMMC_STRUCT.sMissionVariation.sObjectSwap[iIndex].iMinigameType != -1
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iLegacyMiniGameBitSet = 0
		SET_MINIGAME_TYPE_BITSET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iLegacyMiniGameBitSet, g_FMMC_STRUCT.sMissionVariation.sObjectSwap[iIndex].iMinigameType)
		IF SHOULD_MINIGAME_VARIATION_USE_SUBTYPE(iIndex)
			g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = g_FMMC_STRUCT.sMissionVariation.sObjectSwap[iIndex].iMinigameSubType
			IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__CUT_OUT_PAINTING
				g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPaintingIndex = GET_HIGHEST_MINIGAME_PAINTING_ID()
			ENDIF
		ENDIF
	ENDIF
	#IF IS_DEBUG_BUILD
	PRINTLN("[SCRIPT INIT][MissionVariation] - SET_MISSION_VARIATION_OBJECT_TARGET_SWAP - iObj: ", iObj, " Object model is now: ", GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].mn))
	PRINTLN("[SCRIPT INIT][MissionVariation] - SET_MISSION_VARIATION_OBJECT_TARGET_SWAP - iObj: ", iObj, " Object Minigame BS is set to ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iLegacyMiniGameBitSet)
	PRINTLN("[SCRIPT INIT][MissionVariation] - SET_MISSION_VARIATION_OBJECT_TARGET_SWAP - iObj: ", iObj, " Object Minigame Subtype is set to: ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim)
	PRINTLN("[SCRIPT INIT][MissionVariation] - SET_MISSION_VARIATION_OBJECT_TARGET_SWAP - iObj: ", iObj, " Object Minigame iPaintingIndex is set to: ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPaintingIndex)
	#ENDIF
ENDPROC

// Objects Reposition Target Type ------------------------------------------------------------------------------------
//IS_USING_MISSION_VARIATION_OBJECTS_REPOSITION() (x:\gta5\script\dev_ng\multiplayer\include\public\freemode\fmmc_vars.sch)
FUNC BOOL IS_MISSION_VARIATION_OBJECTS_REPOSITION_BASED_ON_TARGET_ACTIVE()
	IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_Target_1)
	OR IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_Target_2)
	OR IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_Target_3)
	OR IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_Target_4)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC
FUNC INT GET_MISSION_VARIATION_OBJECT_REPOSITION_BASED_ON_TARGET_INDEX()
	IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_Target_1)
		RETURN 0
	ELIF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_Target_2)
		RETURN 1
	ELIF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_Target_3)
		RETURN 2
	ELIF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_Target_4)
		RETURN 3
	ENDIF
	RETURN -1
ENDFUNC
FUNC BOOL IS_THIS_OBJECT_USING_MISSION_VARIATION_OBJECTS_REPOSITION_BASED_ON_TARGET(INT iObj)
	RETURN IS_USING_MISSION_VARIATION_OBJECTS_REPOSITION()
	AND IS_MISSION_VARIATION_OBJECTS_REPOSITION_BASED_ON_TARGET_ACTIVE()	
	AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetFour, cibsOBJ4_MissionVariation_RepositionBasedOnTargetType)
ENDFUNC

// DynoProps Scoped Out ------------------------------------------------------------------------------------
// IS_USING_MISSION_VARIATION_DYNOPROPS_SCOPED_OUT()  (x:\gta5\script\dev_ng\multiplayer\include\public\freemode\fmmc_vars.sch)

FUNC BOOL IS_MISSION_VARIATION_DYNOPROPS_SCOPED_OUT_ACTIVE()
	
	IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_BlipCCTVCams)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_THIS_DYNOPROP_USING_MISSION_VARIATION_DYNOPROPS_SCOPED_OUT(INT iObj)
	RETURN (IS_USING_MISSION_VARIATION_DYNOPROPS_SCOPED_OUT()
	AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iObj].iDynopropBitset, ciFMMC_DYNOPROP_AffectedByScopingOut))
	AND IS_MISSION_VARIATION_DYNOPROPS_SCOPED_OUT_ACTIVE()
ENDFUNC

// Spawn Group Forcing ------------------------------------------------------------------------------------
// IS_USING_MISSION_VARIATION_SPAWN_GROUP_FORCING_OVERRIDE()  (x:\gta5\script\dev_ng\multiplayer\include\public\freemode\fmmc_vars.sch)
FUNC INT GET_MISSION_VARIATION_SPAWN_GROUP_FORCING_1_GROUP_TO_FORCE_INDEX()

	IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EndOfMissionDropOffOverrideShort)
		RETURN 0
	ENDIF
	IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EndOfMissionDropOffOverrideMedium)
		RETURN 1
	ENDIF
	IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EndOfMissionDropOffOverrideLong)
		RETURN 2
	ENDIF
	
	RETURN -1
ENDFUNC
FUNC INT GET_MISSION_VARIATION_SPAWN_GROUP_FORCING_2_GROUP_TO_FORCE_INDEX()
	
	IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SelectedEntrance4)
	OR IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SelectedEntrance5)
	OR IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SelectedEntrance6)
	OR IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SelectedEntrance7)
	OR IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SelectedEntrance8)
	OR IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SelectedEntrance9)		
		IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_Davis)
		OR IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_WestVineWood)
		OR IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_RockfordHills)		
		OR IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_ArcadeOwnedLaMesa)
			RETURN 0
		ELIF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_PaletoBay)
		OR IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_Grapeseed)
			RETURN 1
		ENDIF
	ELIF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SelectedEntrance1)
	OR IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SelectedEntrance2)
	OR IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SelectedEntrance3)
		RETURN 2
	ENDIF
	
	RETURN -1
ENDFUNC
FUNC INT GET_MISSION_VARIATION_SPAWN_GROUP_FORCING_3_GROUP_TO_FORCE_INDEX()
	
	IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_DecoyCrewMemberActive)
		RETURN 0
	ENDIF	
	
	RETURN -1
ENDFUNC
FUNC INT GET_MISSION_VARIATION_SPAWN_GROUP_FORCING_4_GROUP_TO_FORCE_INDEX()
	IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_OutfitBugstar)
		RETURN 0
	ENDIF
	IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_OutfitMechanic)
		RETURN 1
	ENDIF
	IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_OutfitGruppeSechs)
		RETURN 2
	ENDIF
	IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_OutfitBrucie)
		RETURN 3
	ENDIF
	RETURN -1
ENDFUNC
FUNC INT GET_MISSION_VARIATION_SPAWN_GROUP_FORCING_5_GROUP_TO_FORCE_INDEX()
	IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EscapeOutfitNoose)
		RETURN 0
	ENDIF
	IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EscapeOutfitFireman)
		RETURN 1
	ENDIF
	IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EscapeOutfitHighRoller)
		RETURN 2
	ENDIF
	
	RETURN -1
ENDFUNC
FUNC INT GET_MISSION_VARIATION_SPAWN_GROUP_FORCING_6_GROUP_TO_FORCE_INDEX()	
	IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SelectedEntrance11)
		RETURN 0
	ENDIF
	IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SelectedEntrance1)
	OR IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SelectedEntrance2)
	OR IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SelectedEntrance3)
	OR IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SelectedEntrance4)
	OR IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SelectedEntrance5)
	OR IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SelectedEntrance6)
	OR IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SelectedEntrance7)
	OR IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SelectedEntrance8)
	OR IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SelectedEntrance9)
	OR IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SelectedEntrance10)
		RETURN 1
	ENDIF
	RETURN -1
ENDFUNC
FUNC INT GET_MISSION_VARIATION_SPAWN_GROUP_FORCING_7_GROUP_TO_FORCE_INDEX()
	RETURN -1
ENDFUNC

FUNC INT GET_MISSION_VARIATION_SPAWN_GROUP_FORCING_SPAWN_GROUP_TO_FORCE_INDEX(INT iGroupSetIndex)
	INT iIndex
	SWITCH iGroupSetIndex
		CASE 0 iIndex = GET_MISSION_VARIATION_SPAWN_GROUP_FORCING_1_GROUP_TO_FORCE_INDEX()	BREAK
		CASE 1 iIndex = GET_MISSION_VARIATION_SPAWN_GROUP_FORCING_2_GROUP_TO_FORCE_INDEX()	BREAK
		CASE 2 iIndex = GET_MISSION_VARIATION_SPAWN_GROUP_FORCING_3_GROUP_TO_FORCE_INDEX()	BREAK
		CASE 3 iIndex = GET_MISSION_VARIATION_SPAWN_GROUP_FORCING_4_GROUP_TO_FORCE_INDEX()	BREAK
		CASE 4 iIndex = GET_MISSION_VARIATION_SPAWN_GROUP_FORCING_5_GROUP_TO_FORCE_INDEX()	BREAK
		CASE 5 iIndex = GET_MISSION_VARIATION_SPAWN_GROUP_FORCING_6_GROUP_TO_FORCE_INDEX()	BREAK
		CASE 6 iIndex = GET_MISSION_VARIATION_SPAWN_GROUP_FORCING_7_GROUP_TO_FORCE_INDEX()	BREAK
	ENDSWITCH
	RETURN iIndex
ENDFUNC
FUNC INT GET_MISSION_VARIATION_SPAWN_GROUP_FORCING_SPAWN_GROUP_TO_FORCE(INT iGroupSetIndex)
	INT iIndex = GET_MISSION_VARIATION_SPAWN_GROUP_FORCING_SPAWN_GROUP_TO_FORCE_INDEX(iGroupSetIndex)
	IF iIndex <= -1
		RETURN -1
	ENDIF
	
	RETURN g_FMMC_STRUCT.sMissionVariation.iVarMissionSpawnGroupForceIndex[iGroupSetIndex][iIndex]+1
ENDFUNC

FUNC BOOL IS_THIS_MISSION_USING_MISSION_VARIATION_SPAWN_GROUP_FORCING_OVERRIDE(INT iGroupSetIndex)
	RETURN IS_USING_MISSION_VARIATION_SPAWN_GROUP_FORCING_OVERRIDE()
	AND GET_MISSION_VARIATION_SPAWN_GROUP_FORCING_SPAWN_GROUP_TO_FORCE(iGroupSetIndex) > 0
ENDFUNC

//EMP Override  ------------------------------------------------------------------------------------
FUNC BOOL GET_MISSION_VARIATION_HAS_EMP()
	RETURN IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_HasEMP)
ENDFUNC

//Handheld Drill Override  ------------------------------------------------------------------------------------
FUNC BOOL GET_MISSION_VARIATION_HAS_HANDHELD_DRILL()
	RETURN IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_HasHandheldDrill)
ENDFUNC

// Location Link (1) ------------------------------------------------------------------------------------
// IS_USING_MISSION_VARIATION_LOCATION_LINK_ONE_SCOPED_ENTRANCE_OVERRIDE()  (x:\gta5\script\dev_ng\multiplayer\include\public\freemode\fmmc_vars.sch)
// and more ---
FUNC INT GET_MISSION_VARIATION_LOCATION_LINK_INDEX(INT iSet)

	SWITCH iSet
		CASE ciMCVar_SelectionIndex_SelectedEntrance
			IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SelectedEntrance1)
				RETURN 0
			ENDIF
			IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SelectedEntrance2)
				RETURN 1
			ENDIF
			IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SelectedEntrance3)
				RETURN 2
			ENDIF
			IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SelectedEntrance4)
				RETURN 3
			ENDIF
			IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SelectedEntrance5)
				RETURN 4
			ENDIF
			IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SelectedEntrance6)
				RETURN 5
			ENDIF
			IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SelectedEntrance7)
				RETURN 6
			ENDIF
			IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SelectedEntrance8)
				RETURN 7
			ENDIF
			IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SelectedEntrance9)
				RETURN 8
			ENDIF
			IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SelectedEntrance10)
				RETURN 9
			ENDIF
			IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SelectedEntrance11)
				RETURN 10
			ENDIF
		BREAK
		
		CASE ciMCVar_SelectionIndex_SelectedExit
			IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SelectedExit1)
				RETURN 0
			ENDIF
			IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SelectedExit2)
				RETURN 1
			ENDIF
			IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SelectedExit3)
				RETURN 2
			ENDIF
			IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SelectedExit4)
				RETURN 3
			ENDIF
			IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SelectedExit5)
				RETURN 4
			ENDIF
			IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SelectedExit6)
				RETURN 5
			ENDIF
			IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SelectedExit7)
				RETURN 6
			ENDIF
			IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SelectedExit8)
				RETURN 7
			ENDIF
			IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SelectedExit9)
				RETURN 8
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN -1
ENDFUNC

FUNC BOOL IS_THIS_LOCATION_USING_MISSION_VARIATION_LOCATION_LINK_ONE(INT iLoc)

	INT iIndex = GET_MISSION_VARIATION_LOCATION_LINK_INDEX(ciMCVar_SelectionIndex_SelectedEntrance)
	IF iIndex < -1
		RETURN FALSE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocMCVarSelectionIndex_One != iIndex
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_THIS_MISSION_USING_MISSION_VARIATION_LOCATION_LINK_ONE_OVERRIDE()
	RETURN (IS_USING_MISSION_VARIATION_LOCATION_LINK_ONE_SCOPED_ENTRANCE_OVERRIDE()) // OR OTHER
	AND GET_MISSION_VARIATION_LOCATION_LINK_INDEX(ciMCVar_SelectionIndex_SelectedEntrance) > -1
ENDFUNC

// Controller actions on this.
FUNC BOOL IS_THIS_MISSION_VARIATION_USING_LOCATION_LINK_ONE_LARGE_BLIP_CHOSEN_ENTRANCE()
	RETURN  IS_USING_MISSION_VARIATION_LOCATION_LINK_ONE_SCOPED_ENTRANCE_OVERRIDE()
		AND IS_BIT_SET(g_FMMC_STRUCT.sMissionVariation.iVarBSOne, ci_MCVarBsOne_MakeChosenEntranceBlipLarge)
ENDFUNC

FUNC BOOL IS_THIS_LOCATION_USING_MISSION_VARIATION_LOCATION_LINK_ONE_SPAWN_BLOCK(INT iLoc)
	
	IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocMCVarSelectionIndex_One = -1
		RETURN FALSE
	ENDIF
	
	INT iBSToCheck = (g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocMCVarSelectionIndex_One + MCVarConfigBS_PhotographedEntranceExit1)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS3, ciLoc_BS3_BlockSpawnIfEntranceOrExitNotSelected)
	AND NOT IS_THIS_LOCATION_USING_MISSION_VARIATION_LOCATION_LINK_ONE(iLoc)
		RETURN TRUE
	ENDIF
	
	IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, iBSToCheck)
	OR IS_THIS_LOCATION_USING_MISSION_VARIATION_LOCATION_LINK_ONE(iLoc) // Selected Entrance
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

// Location Link (2) ------------------------------------------------------------------------------------
// IS_USING_MISSION_VARIATION_LOCATION_LINK_TWO_SCOPED_EXIT_OVERRIDE()  (x:\gta5\script\dev_ng\multiplayer\include\public\freemode\fmmc_vars.sch)
// and more...
FUNC BOOL IS_THIS_LOCATION_USING_MISSION_VARIATION_LOCATION_LINK_TWO(INT iLoc)

	INT iIndex = GET_MISSION_VARIATION_LOCATION_LINK_INDEX(ciMCVar_SelectionIndex_SelectedExit)
	IF iIndex < -1
		RETURN FALSE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocMCVarSelectionIndex_Two != iIndex	
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_THIS_MISSION_USING_MISSION_VARIATION_LOCATION_LINK_TWO_OVERRIDE()
	RETURN (IS_USING_MISSION_VARIATION_LOCATION_LINK_TWO_SCOPED_EXIT_OVERRIDE()) // OR OTHER
	AND GET_MISSION_VARIATION_LOCATION_LINK_INDEX(ciMCVar_SelectionIndex_SelectedExit) > -1
ENDFUNC

// Controller actions on this.
FUNC BOOL IS_THIS_MISSION_VARIATION_USING_LOCATION_LINK_TWO_LARGE_BLIP_CHOSEN_EXIT()
	RETURN  IS_USING_MISSION_VARIATION_LOCATION_LINK_TWO_SCOPED_EXIT_OVERRIDE()
		AND IS_BIT_SET(g_FMMC_STRUCT.sMissionVariation.iVarBSOne, ci_MCVarBsOne_MakeChosenExitBlipLarge)
ENDFUNC

FUNC BOOL IS_THIS_LOCATION_USING_MISSION_VARIATION_LOCATION_LINK_TWO_SPAWN_BLOCK(INT iLoc)
	
	IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocMCVarSelectionIndex_Two = -1
		RETURN FALSE
	ENDIF
	
	INT iBSToCheck = (g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocMCVarSelectionIndex_Two + MCVarConfigBS_PhotographedEntranceExit1)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS3, ciLoc_BS3_BlockSpawnIfEntranceOrExitNotSelected)
	AND NOT IS_THIS_LOCATION_USING_MISSION_VARIATION_LOCATION_LINK_TWO(iLoc) 
		RETURN TRUE
	ENDIF
	
	IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, iBSToCheck)
	OR IS_THIS_LOCATION_USING_MISSION_VARIATION_LOCATION_LINK_TWO(iLoc) // Selected Exit
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

//Location prerequisites ------------------------------------------------------------------------------------
FUNC BOOL IS_THIS_LOCATION_USING_MISSION_VARIATION_LOCATION_PREREQUISITE(INT iLoc)
	IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocMCVarPrerequisite = -1
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC	

PROC PROCESS_MISSION_VARIATION_LOCATION_PREQUISITE(INT iLoc)
	IF NOT HAS_MISSION_VARIATION_PREREQUISITE_MISSION_ELEMENT_BEEN_COMPLETED(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocMCVarPrerequisite)
		INT iTeam
		FOR iTeam = 0 TO FMMC_MAX_TEAMS-1
			g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iPriority[iTeam] = 99999
			#IF IS_DEBUG_BUILD
			PRINTLN("[SCRIPT INIT][MissionVariation] - PROCESS_MISSION_VARIATION_LOCATION_PREQUISITE - Locate ", iLoc," has had it's priority cleared due to prerequisite ", g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocMCVarPrerequisite)
			#ENDIF
		ENDFOR
	ENDIF
ENDPROC	

// Dummy Blip Overrides ------------------------------------------------------------------------------------
FUNC BOOL IS_THIS_DUMMY_BLIP_USING_MISSION_VARIATION_EXIT_RESTRICTION(INT iDummyBlip)

	IF NOT IS_USING_MISSION_VARIATION_DUMMY_BLIP_EXIT_RESTRICTION()
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iBitSet, ciDUMMY_BLIP_SHOW_FOR_EXIT_1)
	OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iBitSet, ciDUMMY_BLIP_SHOW_FOR_EXIT_2)
	OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iBitSet, ciDUMMY_BLIP_SHOW_FOR_EXIT_3)
	OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iBitSet, ciDUMMY_BLIP_SHOW_FOR_EXIT_4)
	OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iBitSet, ciDUMMY_BLIP_SHOW_FOR_EXIT_5)
	OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iBitSet, ciDUMMY_BLIP_SHOW_FOR_EXIT_6)
	OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iBitSet, ciDUMMY_BLIP_SHOW_FOR_EXIT_7)
	OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iBitSet, ciDUMMY_BLIP_SHOW_FOR_EXIT_8)
	OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].iBitSet, ciDUMMY_BLIP_SHOW_FOR_EXIT_9)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_WRONG_EXIT_BEEN_CHOSEN_FOR_DUMMY_BLIP_MISSION_VARIATION(DUMMY_BLIP_STRUCT& sDummyBlip)
	
	IF IS_BIT_SET(sDummyBlip.iBitSet, ciDUMMY_BLIP_SHOW_FOR_EXIT_1)
		IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SelectedExit1)
			PRINTLN("[SCRIPT INIT][MissionVariation] - HAS_WRONG_EXIT_BEEN_CHOSEN_FOR_DUMMY_BLIP_MISSION_VARIATION | Exit 1 is available")
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(sDummyBlip.iBitSet, ciDUMMY_BLIP_SHOW_FOR_EXIT_2)
		IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SelectedExit2)
			PRINTLN("[SCRIPT INIT][MissionVariation] - HAS_WRONG_EXIT_BEEN_CHOSEN_FOR_DUMMY_BLIP_MISSION_VARIATION | Exit 2 is available")
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(sDummyBlip.iBitSet, ciDUMMY_BLIP_SHOW_FOR_EXIT_3)
		IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SelectedExit3)
			PRINTLN("[SCRIPT INIT][MissionVariation] - HAS_WRONG_EXIT_BEEN_CHOSEN_FOR_DUMMY_BLIP_MISSION_VARIATION | Exit 3 is available")
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(sDummyBlip.iBitSet, ciDUMMY_BLIP_SHOW_FOR_EXIT_4)
		IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SelectedExit4)
			PRINTLN("[SCRIPT INIT][MissionVariation] - HAS_WRONG_EXIT_BEEN_CHOSEN_FOR_DUMMY_BLIP_MISSION_VARIATION | Exit 4 is available")
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(sDummyBlip.iBitSet, ciDUMMY_BLIP_SHOW_FOR_EXIT_5)
		IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SelectedExit5)
			PRINTLN("[SCRIPT INIT][MissionVariation] - HAS_WRONG_EXIT_BEEN_CHOSEN_FOR_DUMMY_BLIP_MISSION_VARIATION | Exit 5 is available")
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(sDummyBlip.iBitSet, ciDUMMY_BLIP_SHOW_FOR_EXIT_6)
		IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SelectedExit6)
			PRINTLN("[SCRIPT INIT][MissionVariation] - HAS_WRONG_EXIT_BEEN_CHOSEN_FOR_DUMMY_BLIP_MISSION_VARIATION | Exit 6 is available")
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(sDummyBlip.iBitSet, ciDUMMY_BLIP_SHOW_FOR_EXIT_7)
		IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SelectedExit7)
			PRINTLN("[SCRIPT INIT][MissionVariation] - HAS_WRONG_EXIT_BEEN_CHOSEN_FOR_DUMMY_BLIP_MISSION_VARIATION | Exit 7 is available")
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(sDummyBlip.iBitSet, ciDUMMY_BLIP_SHOW_FOR_EXIT_8)
		IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SelectedExit8)
			PRINTLN("[SCRIPT INIT][MissionVariation] - HAS_WRONG_EXIT_BEEN_CHOSEN_FOR_DUMMY_BLIP_MISSION_VARIATION | Exit 8 is available")
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(sDummyBlip.iBitSet, ciDUMMY_BLIP_SHOW_FOR_EXIT_9)
		IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SelectedExit9)
			PRINTLN("[SCRIPT INIT][MissionVariation] - HAS_WRONG_EXIT_BEEN_CHOSEN_FOR_DUMMY_BLIP_MISSION_VARIATION | Exit 9 is available")
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC
PROC PROCESS_MISSION_VARIATION_DUMMY_BLIP_EXITS(INT iDummyBlip)
	IF HAS_WRONG_EXIT_BEEN_CHOSEN_FOR_DUMMY_BLIP_MISSION_VARIATION(g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip])
		g_FMMC_STRUCT_ENTITIES.sDummyBlips[iDummyBlip].vPos = <<0,0,0>>
		PRINTLN("[SCRIPT INIT][MissionVariation] - PROCESS_MISSION_VARIATION_DUMMY_BLIP_EXITS - Dummy blip ", iDummyBlip, " is being hidden due to the exit that has been chosen")
	ELSE
		PRINTLN("[SCRIPT INIT][MissionVariation] - PROCESS_MISSION_VARIATION_DUMMY_BLIP_EXITS - Dummy blip ", iDummyBlip, " made it through the exit checks")
	ENDIF
ENDPROC	

// Jump To Objective Overrides ------------------------------------------------------------------------------------
FUNC BOOL IS_THIS_MISSION_VARIATION_USING_LOCATION_JUMP_TO_OBJECTIVE_OVERRIDE()

	RETURN g_FMMC_STRUCT.sMissionVariation.iVarJumpToObjectiveEntityType = ciMCVAR_JUMP_TO_OBJECTIVE_ENTITY_TYPE_LOCATION

ENDFUNC

FUNC BOOL IS_THIS_LOCATION_USING_MISSION_VARIATION_JUMP_TO_OBJECTIVE_OVERRIDE(INT iLoc)
	
	IF iLoc >= FMMC_MAX_GO_TO_LOCATIONS
		RETURN FALSE
	ENDIF
	
	IF iLoc < 0
		RETURN FALSE
	ENDIF
	
	RETURN IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocBS3, ciLoc_BS3_MissionVarOverrideJumpToObjective)
	
ENDFUNC

FUNC INT GET_MISSION_VARIATION_JUMP_TO_OBJECTIVE_OVERRIDE()
	
	IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_ApproachStealth)
		RETURN 0
	ENDIF
	
	IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_ApproachSubterfuge)
		RETURN 1
	ENDIF
	
	IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_ApproachDirect)
		RETURN 2
	ENDIF	
	
	RETURN -1
	
ENDFUNC

PROC PROCESS_APPLY_MISSION_VARIATION_JUMP_TO_OBJECTIVE_OVERRIDE(INT iLoc)
	
	IF iLoc >= FMMC_MAX_GO_TO_LOCATIONS
		EXIT
	ENDIF
	
	IF iLoc < 0
		EXIT
	ENDIF
	
	INT iOverrideIndex = GET_MISSION_VARIATION_JUMP_TO_OBJECTIVE_OVERRIDE()
	
	IF iOverrideIndex = -1
		EXIT
	ENDIF
	
	INT iTeam = 0
	FOR iTeam = 0 TO FMMC_MAX_TEAMS - 1
		g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iJumpToObjectivePass[iTeam] = g_FMMC_STRUCT.sMissionVariation.iVarJumpToObjectivePass[iOverrideIndex][iTeam]
		g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iJumpToObjectiveFail[iTeam] = g_FMMC_STRUCT.sMissionVariation.iVarJumpToObjectiveFail[iOverrideIndex][iTeam]
		PRINTLN("[SCRIPT INIT][MissionVariation] - PROCESS_APPLY_MISSION_VARIATION_JUMP_TO_OBJECTIVE_OVERRIDE - iTeam ", iTeam, " iLoc ",iLoc, " iJumpToObjectivePass ", g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iJumpToObjectivePass[iTeam], " iJumpToObjectiveFail", g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iJumpToObjectiveFail[iTeam])
	ENDFOR
	
ENDPROC

// Timer Overrides ------------------------------------------------------------------------------------
// IS_USING_MISSION_VARIATION_TIMER_OVERRIDING_OVERRIDE()  (x:\gta5\script\dev_ng\multiplayer\include\public\freemode\fmmc_vars.sch)
FUNC INT GET_MISSION_VARIATION_OVERRIDE_TIMER_1_INDEX()

	IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_HackerChosen1)
		RETURN 0
	ENDIF
	
	IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_HackerChosen2)
		RETURN 1
	ENDIF
	
	IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_HackerChosen3)
		RETURN 2
	ENDIF	
	
	IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_HackerChosen4)
		RETURN 3
	ENDIF
	
	IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_HackerChosen5)
		RETURN 4
	ENDIF
	
	RETURN -1
ENDFUNC
FUNC INT GET_MISSION_VARIATION_OVERRIDE_TIMER_2_INDEX()

	IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_HackerChosen1)
		RETURN 0
	ENDIF
	
	IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_HackerChosen2)
		RETURN 1
	ENDIF
	
	IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_HackerChosen3)
		RETURN 2
	ENDIF	
	
	IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_HackerChosen4)
		RETURN 3
	ENDIF
	
	IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_HackerChosen5)
		RETURN 4
	ENDIF
	
	RETURN -1
ENDFUNC
FUNC INT GET_MISSION_VARIATION_OVERRIDE_TIMER_3_INDEX()	
	RETURN -1
ENDFUNC
FUNC INT GET_MISSION_VARIATION_OVERRIDE_TIMER_4_INDEX()
	RETURN -1
ENDFUNC

FUNC BOOL IS_MISSION_VARIATION_TIMER_OVERRIDE_FOR_ANY_TIMER(INT iTimerIndex)
	RETURN (g_FMMC_STRUCT.sMissionVariation.eVarMissionTimerTypeEnum[iTimerIndex] != FMMC_MC_VAR_TIMER_TYPE_OFF)
ENDFUNC

FUNC BOOL IS_MISSION_VARIATION_TIMER_OVERRIDE_FOR_RULE_TIMER(INT iTimerIndex)
	RETURN (g_FMMC_STRUCT.sMissionVariation.eVarMissionTimerTypeEnum[iTimerIndex] = FMMC_MC_VAR_TIMER_TYPE_RULE)
ENDFUNC

FUNC BOOL IS_MISSION_VARIATION_TIMER_OVERRIDE_FOR_MULTIRULE_TIMER(INT iTimerIndex)
	RETURN (g_FMMC_STRUCT.sMissionVariation.eVarMissionTimerTypeEnum[iTimerIndex] = FMMC_MC_VAR_TIMER_TYPE_MULTIRULE)
ENDFUNC

FUNC BOOL IS_MISSION_VARIATION_TIMER_OVERRIDE_FOR_ZONE_TIMER(INT iTimerIndex)
	RETURN (g_FMMC_STRUCT.sMissionVariation.eVarMissionTimerTypeEnum[iTimerIndex] = FMMC_MC_VAR_TIMER_TYPE_ZONE_TIMER)
ENDFUNC

FUNC INT GET_MISSION_VARIATION_OVERRIDE_TIMER_INDEX(INT iTimerIndex)
	INT iIndex
	SWITCH iTimerIndex
		CASE 0 iIndex = GET_MISSION_VARIATION_OVERRIDE_TIMER_1_INDEX()	BREAK
		CASE 1 iIndex = GET_MISSION_VARIATION_OVERRIDE_TIMER_2_INDEX()	BREAK
		CASE 2 iIndex = GET_MISSION_VARIATION_OVERRIDE_TIMER_3_INDEX()	BREAK
		CASE 3 iIndex = GET_MISSION_VARIATION_OVERRIDE_TIMER_4_INDEX()	BREAK
	ENDSWITCH
	RETURN iIndex
ENDFUNC
FUNC INT GET_MISSION_VARIATION_TIMER_OVERRIDE_VALUE(INT iTimerIndex)
	
	INT iIndex = GET_MISSION_VARIATION_OVERRIDE_TIMER_INDEX(iTimerIndex)
	IF iIndex <= -1
		RETURN -1
	ENDIF
	
	RETURN g_FMMC_STRUCT.sMissionVariation.iVarMissionTimerModifier[iTimerIndex][iIndex]
ENDFUNC
FUNC BOOL IS_MISSION_VARIATION_TIMER_OVERRIDE_VALID_FOR_RULE(INT iTimerIndex, INT iRule)

	IF g_FMMC_STRUCT.sMissionVariation.eVarMissionTimerTypeEnum[iTimerIndex] = FMMC_MC_VAR_TIMER_TYPE_ZONE_TIMER
		RETURN TRUE
	ENDIF
	
	INT iRuleFrom = g_FMMC_STRUCT.sMissionVariation.iVarMissionTimerRuleFrom[iTimerIndex]
	INT iRuleTo = g_FMMC_STRUCT.sMissionVariation.iVarMissionTimerRuleTo[iTimerIndex]
	IF iRuleTo = -1
		iRuleTo = FMMC_MAX_RULES
	ENDIF
	
	IF iRule >= iRuleFrom
	AND iRule <= iRuleTo
		RETURN TRUE
	ENDIF
	
	RETURN FALSE	
ENDFUNC

// Object Link (1) ------------------------------------------------------------------------------------
// IS_USING_MISSION_VARIATION_OBJECTS_LINK_TO_SELECTION()  (x:\gta5\script\dev_ng\multiplayer\include\public\freemode\fmmc_vars.sch)
// IS_USING_MISSION_VARIATION_OBJECTS_LINK_TO_SELECTION_BIG_BLIP() (x:\gta5\script\dev_ng\multiplayer\include\public\freemode\fmmc_vars.sch)
FUNC BOOL IS_THIS_OBJECT_USING_MISSION_VARIATION_LOCATION_LINK_ONE(INT iObj)

	INT iIndex = GET_MISSION_VARIATION_LOCATION_LINK_INDEX(ciMCVar_SelectionIndex_SelectedEntrance)
	IF iIndex < -1
		RETURN FALSE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjMCVarSelectionIndex_One != iIndex
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_THIS_MISSION_USING_MISSION_VARIATION_OBJECT_LINK_ONE_OVERRIDE()
	RETURN (IS_USING_MISSION_VARIATION_OBJECTS_LINK_TO_SELECTION()) // OR OTHER
	AND GET_MISSION_VARIATION_LOCATION_LINK_INDEX(ciMCVar_SelectionIndex_SelectedEntrance) > -1
ENDFUNC

FUNC BOOL IS_THIS_OBJECT_USING_MISSION_VARIATION_LOCATION_LINK_ONE_SPAWN_BLOCK(INT iObj)
		
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjMCVarSelectionIndex_One = -1
		RETURN FALSE
	ENDIF
	
	INT iBSToCheck = (g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjMCVarSelectionIndex_One + MCVarConfigBS_PhotographedEntranceExit1)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetFour, cibsOBJ4_LocationLinkSpawnBlock)
		RETURN FALSE
	ENDIF
	
	IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, iBSToCheck)
	OR IS_THIS_OBJECT_USING_MISSION_VARIATION_LOCATION_LINK_ONE(iObj)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_THIS_OBJECT_USING_MISSION_VARIATION_LOCATION_LINK_ONE_DEACTIVATE_MINIGAME(INT iObj)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjMCVarSelectionIndex_One = -1
		RETURN FALSE
	ENDIF
	
	INT iBSToCheck = (g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjMCVarSelectionIndex_One + MCVarConfigBS_PhotographedEntranceExit1)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetFour, cibsOBJ4_LocationLinkDeactivateMG)
		RETURN FALSE
	ENDIF
	
	IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, iBSToCheck)
	OR IS_THIS_OBJECT_USING_MISSION_VARIATION_LOCATION_LINK_ONE(iObj)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

// Controller actions on this.
FUNC BOOL IS_THIS_OBJ_USING_MISSION_VARIATION_OBJECT_LINK_ONE_LARGE_BLIP_OVERRIDE(INT iObj)
	RETURN  IS_THIS_MISSION_USING_MISSION_VARIATION_OBJECT_LINK_ONE_OVERRIDE()
		AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitsetThree, cibsOBJ3_LinkToSelectionBigBlip)
ENDFUNC

// Object Link (2) ------------------------------------------------------------------------------------
// IS_USING_MISSION_VARIATION_OBJECTS_LINK_TO_SELECTION()  (x:\gta5\script\dev_ng\multiplayer\include\public\freemode\fmmc_vars.sch)
// IS_USING_MISSION_VARIATION_OBJECTS_LINK_TO_SELECTION_BIG_BLIP() (x:\gta5\script\dev_ng\multiplayer\include\public\freemode\fmmc_vars.sch)
FUNC BOOL IS_THIS_OBJECT_USING_MISSION_VARIATION_LOCATION_LINK_TWO(INT iObj)

	INT iIndex = GET_MISSION_VARIATION_LOCATION_LINK_INDEX(ciMCVar_SelectionIndex_SelectedExit)
	IF iIndex < -1
		RETURN FALSE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjMCVarSelectionIndex_Two != iIndex
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_THIS_MISSION_USING_MISSION_VARIATION_OBJECT_LINK_TWO_OVERRIDE()
	RETURN (IS_USING_MISSION_VARIATION_OBJECTS_LINK_TO_SELECTION()) // OR OTHER
	AND GET_MISSION_VARIATION_LOCATION_LINK_INDEX(ciMCVar_SelectionIndex_SelectedExit) > -1
ENDFUNC

FUNC BOOL IS_THIS_OBJECT_USING_MISSION_VARIATION_LOCATION_LINK_TWO_SPAWN_BLOCK(INT iObj)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjMCVarSelectionIndex_Two = -1
		RETURN FALSE
	ENDIF
	
	INT iBSToCheck = (g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjMCVarSelectionIndex_Two + MCVarConfigBS_PhotographedEntranceExit1)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetFour, cibsOBJ4_LocationLinkSpawnBlock)
		RETURN FALSE
	ENDIF
	
	IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, iBSToCheck)
	OR IS_THIS_OBJECT_USING_MISSION_VARIATION_LOCATION_LINK_TWO(iObj)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_THIS_OBJECT_USING_MISSION_VARIATION_LOCATION_LINK_TWO_DEACTIVATE_MINIGAME(INT iObj)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjMCVarSelectionIndex_Two != -1
		RETURN FALSE
	ENDIF
	
	INT iBSToCheck = (g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjMCVarSelectionIndex_Two + MCVarConfigBS_PhotographedEntranceExit1)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetFour, cibsOBJ4_LocationLinkDeactivateMG)
		RETURN FALSE
	ENDIF
	
	IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, iBSToCheck)
	OR IS_THIS_OBJECT_USING_MISSION_VARIATION_LOCATION_LINK_TWO(iObj)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

// PTFX Prerequisite Mission (1) ------------------------------------------------------------------------------------
// IS_USING_MISSION_VARIATION_PTFX_USING_PREREQUISITE_MISSION_ELEMENT_OVERRIDE()  (x:\gta5\script\dev_ng\multiplayer\include\public\freemode\fmmc_vars.sch)
// Note: HAS_MISSION_VARIATION_PREREQUISITE_MISSION_ELEMENT_BEEN_COMPLETED()
FUNC INT GET_MISSION_VARIATION_PTFX_PREREQUISITE_MISSION_ELEMENT(INT iPTFX)
	RETURN g_FMMC_STRUCT.sPlacedPTFX[iPTFX].iMCVarConfig_PreReqMissionPointer
ENDFUNC
FUNC BOOL IS_THIS_PTFX_USING_A_PREREQUISITE_MISSION_ELEMENT(INT iPTFX)
	IF NOT IS_USING_MISSION_VARIATION_PTFX_USING_PREREQUISITE_MISSION_ELEMENT_OVERRIDE()
		RETURN FALSE
	ENDIF
	IF GET_MISSION_VARIATION_PTFX_PREREQUISITE_MISSION_ELEMENT(iPTFX) = -1
		RETURN FALSE
	ENDIF
	RETURN TRUE	
ENDFUNC

// PTFX Block Spawn (1) ------------------------------------------------------------------------------------
// IS_USING_MISSION_VARIATION_PTFX_BLOCK_SPAWN_FROM_PREREQUISITE_1_OVERRIDE()  (x:\gta5\script\dev_ng\multiplayer\include\public\freemode\fmmc_vars.sch)
FUNC BOOL IS_MISSION_VARIATION_PTFX_SPAWN_BLOCKED_FROM_PREREQUISITE(INT iPTFX)
	RETURN IS_BIT_SET(g_FMMC_STRUCT.sPlacedPTFX[iPTFX].iPTFXBS, ciPLACED_PTFX_MCVarConfig_BlockSpawnUnlessCompletedPrereqMission_1)
ENDFUNC
FUNC BOOL IS_THIS_PTFX_USING_BLOCK_SPAWN_FROM_PREREQUISITE(INT iPTFX)
	IF NOT IS_USING_MISSION_VARIATION_PTFX_BLOCK_SPAWN_FROM_PREREQUISITE_1_OVERRIDE()
		RETURN FALSE
	ENDIF	
	IF NOT IS_MISSION_VARIATION_PTFX_SPAWN_BLOCKED_FROM_PREREQUISITE(iPTFX)
		RETURN FALSE
	ENDIF
	RETURN TRUE	
ENDFUNC



// End Cutscene Override (1) ------------------------------------------------------------------------------------
//  IS_USING_MISSION_VARIATION_END_CUTSCENE_OVERRIDE()  (x:\gta5\script\dev_ng\multiplayer\include\public\freemode\fmmc_vars.sch)
FUNC INT GET_MISSION_VARIATION_END_CTUSCENE_OVERRIDE_INDEX()
	
	IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EndOfMissionDropOffOverride1)
		RETURN 0
	ELIF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EndOfMissionDropOffOverride2)
		RETURN 1
	ELIF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EndOfMissionDropOffOverride3)
		RETURN 2
	ENDIF
	
	RETURN -1
ENDFUNC

FUNC INT GET_MISSION_VARIATION_END_CTUSCENE_OVERRIDE_VALUE(INT iIndex)
	RETURN g_FMMC_STRUCT.sMissionVariation.iVarMissionEndCutsceneOverride[iIndex]
ENDFUNC

FUNC BOOL IS_MISSION_VARIATION_END_CUTSCENE_OVERRIDE_VALID()
	IF NOT IS_USING_MISSION_VARIATION_END_CUTSCENE_OVERRIDE()
		RETURN FALSE
	ENDIF
	INT iIndex = GET_MISSION_VARIATION_END_CTUSCENE_OVERRIDE_INDEX()
	IF iIndex >= FMMC_MAX_MC_VAR_SIZE_END_CUTSCENE_OVERRIDE
	OR iIndex < 0
		RETURN FALSE
	ENDIF	
	
	RETURN TRUE
ENDFUNC

PROC PROCESS_OFFSET_AXIS_ALIGNED_ZONE_FOR_ENTITY_LINK_LOCATION_0(INT iZone, VECTOR vNewCentre, VECTOR &vAssignPosition)
	VECTOR vCentre = (g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[0] + g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[1])
	vCentre.x /= 2
	vCentre.y /= 2
	vCentre.z /= 2
	
	VECTOR vOffset0 = vCentre - g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[1]
	vOffset0.x = ABSF(vOffset0.x)
	vOffset0.y = ABSF(vOffset0.y)
	vOffset0.z = ABSF(vOffset0.z)
	
	vNewCentre.x -= (vOffset0.x/2)
	vNewCentre.y -= (vOffset0.y/2)
	vNewCentre.z -= (vOffset0.z/2)
	vAssignPosition = vNewCentre
ENDPROC
PROC PROCESS_OFFSET_AXIS_ALIGNED_ZONE_FOR_ENTITY_LINK_LOCATION_1(INT iZone, VECTOR vNewCentre, VECTOR &vAssignPosition)
	VECTOR vCentre = (g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[0] + g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[1])
	vCentre.x /= 2
	vCentre.y /= 2
	vCentre.z /= 2
	
	VECTOR vOffset1 = vCentre - g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[1]
	
	vOffset1.x = ABSF(vOffset1.x)
	vOffset1.y = ABSF(vOffset1.y)
	vOffset1.z = ABSF(vOffset1.z)
	
	vNewCentre.x += (vOffset1.x/2)
	vNewCentre.y += (vOffset1.y/2)
	vNewCentre.z += (vOffset1.z/2)
	vAssignPosition = vNewCentre
ENDPROC
PROC PROCESS_MISSION_VARIATION_MOVE_ZONES_TO_LINKED_ENTITIES(INT iZone, VECTOR vNewCentre)
	VECTOR vNewPos0, vNewPos1
	IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iAreaType = ciFMMC_ZONE_SHAPE__SPHERE
		g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[0] = vNewCentre
		PRINTLN("[SCRIPT INIT][MissionVariation] - (Zone Reposition) DONE iZone: ", iZone, " vNewCentre: ", vNewCentre)
	ELSE
	
		#IF IS_DEBUG_BUILD
			VECTOR vOldCentre = (g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[0] + g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[1])
			vOldCentre.x /= 2
			vOldCentre.y /= 2
			vOldCentre.z /= 2
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Zone Reposition) iZone: ", iZone, " vNewCentre: ", vNewCentre, " vOldCentre: ", vOldCentre)
		#ENDIF
		
		PROCESS_OFFSET_AXIS_ALIGNED_ZONE_FOR_ENTITY_LINK_LOCATION_0(iZone, vNewCentre, vNewPos0)
		PROCESS_OFFSET_AXIS_ALIGNED_ZONE_FOR_ENTITY_LINK_LOCATION_1(iZone, vNewCentre, vNewPos1)
		
		PRINTLN("[SCRIPT INIT][MissionVariation] - (Zone Reposition) DONE iZone: ", iZone, " vOldPos0: ", g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[0], " vNewPos0: ", vNewPos0, " vOldPos1: ", g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[1], " vNewPos1: ", vNewPos1)
		g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[0] = vNewPos0
		g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[1] = vNewPos1
	ENDIF
ENDPROC

FUNC BOOL IS_THIS_ZONE_USING_MISSION_VARIATION_REPOSITION_TO_LINKED_ENTITY(INT iZone)
	RETURN (IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneBS, ciFMMC_ZONEBS_MissionVariationPositionZoneAtVarEntity) OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneBS, ciFMMC_ZONEBS_MissionVariationPositionZoneAtVarEntity2))
	AND IS_USING_MISSION_VARIATION_ZONE_REPOSITION_AT_LINKED_ENTITY()
ENDFUNC

// [MissionVariation] helper functions. ================================================================================= END

// [MissionVariation] init functions. ================================================================================= START

PROC PROCESS_APPLY_MISSION_VARIATION_SETTINGS_FOR_PLACED_PEDS()
	// Modify Globals
	INT iTeam = 0
	INT iPed = 0
	
	FOR iPed = 0 TO FMMC_MAX_PEDS-1
		IF IS_THIS_PED_USING_MISSION_VARIATION_ENEMY_HEALTH_OVERRIDE(iPed)
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Enemy Health) Setting for Ped: ", iPed)
			g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].fHealthVarMultiplier = TO_FLOAT(GET_MISSION_VARIATION_ENEMY_HEALTH_OVERRIDE())
		ENDIF
		IF IS_THIS_PED_USING_MISSION_VARIATION_ENEMY_INVENTORY_OVERRIDE(iPed)
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Enemy Inventory) Setting for Ped: ", iPed, " iGun: ", GET_MISSION_VARIATION_ENEMY_INVENTORY_OVERRIDE(iPed), " ENUM: ", INT_TO_ENUM(WEAPON_TYPE, GET_MISSION_VARIATION_ENEMY_INVENTORY_OVERRIDE(iPed)))
			g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].gun = INT_TO_ENUM(WEAPON_TYPE, GET_MISSION_VARIATION_ENEMY_INVENTORY_OVERRIDE(iPed))
		ENDIF
		IF IS_THIS_PED_USING_MISSION_VARIATION_ENEMY_ALL_BLIP_OVERRIDE(iPed)
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Enemy Blips Always On) Setting for Ped: ", iPed)
			CLEAR_BIT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFive, ciPED_BSFive_Blip_After_Combat)
			CLEAR_BIT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitset, ciPED_BS_BlipOff)
			g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iBlipNoticeRange = 9999
			FOR iTeam = 0 TO FMMC_MAX_TEAMS-1
				g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].sPedBlipStruct.sTeamBlip[iTeam].iBlipOverrideStartRule = 0
				g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sPedBlipStruct.sTeamBlip[iTeam].iBlipOverrideEndRule = FMMC_MAX_RULES
			ENDFOR
		ENDIF
		IF IS_THIS_PED_USING_MISSION_VARIATION_ENEMY_DISABLE_CRIT_IMMUNITY(iPed)
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Ped Crit Immunity) Ped: ", iPed, " Clearing Critical Hit Immunity.")
			CLEAR_BIT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitset, ciPED_BS_SufferCritsOverride)
		ENDIF
		IF IS_THIS_PED_USING_MISSION_VARIATION_REPOSITION_FROM_ASSOCIATED_SELECTION_OVERRIDE(iPed)			
			INT iIndex = GET_MISSION_VARIATION_REPOSITION_FROM_ASSOCIATED_SELECTION_INDEX()
			IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].sWarpLocationSettings.vPosition[iIndex])
				PRINTLN("[SCRIPT INIT][MissionVariation] - (Ped Reposition) Setting new coordinates up vPos: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sWarpLocationSettings.vPosition[iIndex], " fHead: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].sWarpLocationSettings.fHeading[iIndex], " for Ped: ", iPed)
				g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].vpos = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].sWarpLocationSettings.vPosition[iIndex]
				g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].fHead = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].sWarpLocationSettings.fHeading[iIndex]			
			ELSE
				PRINTLN("[SCRIPT INIT][MissionVariation] - (Ped Reposition) Ped: ", iPed, " iIndex: ", iIndex, " sWarpLocationSettings.vPosition Vector is zero not repositioning.")
			ENDIF
		ENDIF 
		IF IS_THIS_PED_USING_MISSION_VARIATION_SPAWN_FROM_ASSOCIATED_SELECTION_1_OVERRIDE(iPed)
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Ped Spawn Associated) Clearing Block Spawn override for Ped: ", iPed)
			CLEAR_BIT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetThirteen, ciPED_BSThirteen_MissionVariation_UseAsociatedSelectionSpawn)
		ENDIF
		IF IS_THIS_PED_USING_MISSION_VARIATION_COP_DECOY_OVERRIDE(iPed)
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Cop Decoy) Applying Cop Decoy overrides for Ped: ", iPed)
			SET_BIT(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetThirteen, ciPED_BSThirteen_SetAsCopDecoy)
			g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedCopDecoyActiveTime = GET_MISSION_VARIATION_COP_DECOY_ACTIVE_TIME_OVERRIDE()
			g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedCopDecoyLose2StarChance = GET_MISSION_VARIATION_COP_DECOY_LOSE_2_STAR_CHANCE_OVERRIDE()
		ENDIF
		IF IS_THIS_PED_USING_MISSION_VARIATION_MODEL_VARIATION_OVERRIDE(iPed)
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Model Variation Override) Applying Enemy Model Variation overrides for Ped: ", iPed)
			g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iModelVariation = GET_MISSION_VARIATION_MODEL_VARIATION_OVERRIDE()
			PRINTLN("[ML] Model Variation override = ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iModelVariation)
		ENDIF
	ENDFOR
ENDPROC
PROC PROCESS_APPLY_MISSION_VARIATION_SETTINGS_FOR_PLACED_VEHICLES()
	BOOL bUnblockedSwitch
	INT iVeh = 0
	FOR iVeh = 0 TO FMMC_MAX_VEHICLES-1	
		bUnblockedSwitch = FALSE
		IF IS_USING_MISSION_VARIATION_VEHICLE_BLOCK_SPAWN_OVERRIDE()			
			IF IS_THIS_VEHICLE_USING_MISSION_VARIATION_VEHICLE_BLOCK_SPAWN_OVERRIDE(iVeh)
				PRINTLN("[SCRIPT INIT][MissionVariation] - (Vehicle Spawn) Block Spawn override set for Veh: ", iVeh)
			ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetEight, ciFMMC_VEHICLE8_BlockSpawnForMissionVariation)
				CLEAR_BIT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetEight, ciFMMC_VEHICLE8_BlockSpawnForMissionVariation)
				bUnblockedSwitch = TRUE				
				PRINTLN("[SCRIPT INIT][MissionVariation] - (Vehicle Spawn) Clearing Block Spawn override for Veh: ", iVeh)
			ENDIF
		ENDIF
		IF IS_THIS_VEHICLE_USING_MISSION_VARIATION_VEHICLE_MODEL_SWAP_OVERRIDE(iVeh)			
			IF GET_MISSION_VARIATION_VEHICLE_MODEL_SWAP_OVERRIDE(iVeh) != DUMMY_MODEL_FOR_SCRIPT
				MODEL_NAMES mnNew = GET_MISSION_VARIATION_VEHICLE_MODEL_SWAP_OVERRIDE(iVeh)
				PRINTLN("[SCRIPT INIT][MissionVariation] - (Veh Model Swap) Swapping Model for: ", iVeh, " New Model is: " , mnNew)
				g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn = mnNew
			
				IF g_FMMC_STRUCT.sMissionVariation.eMCVarConfigEnum = FMMC_MC_VAR_CONFIG_ENUM_CASINO_HEIST 
					SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn
						CASE BURRITO2
							PRINTLN("[ML][MissionVariation][PROCESS_APPLY_MISSION_VARIATION_SETTINGS_FOR_PLACED_VEHICLES] Setting BURRITO2 to mod preset 0")
							g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iModPreset = 0
						BREAK
						CASE BOXVILLE
							PRINTLN("[ML][MissionVariation][PROCESS_APPLY_MISSION_VARIATION_SETTINGS_FOR_PLACED_VEHICLES] Setting BOXVILLE to mod preset 0")
							g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iModPreset = 0
						BREAK
						CASE STOCKADE
							IF GET_PACKED_STAT_INT(PACKED_SUBTERFUGE_LICENCE_PLATE) = ciCASINO_HEIST_GRUPPE_SECHS_LICENCE_PLATE_NONE
								PRINTLN("[ML][MissionVariation][PROCESS_APPLY_MISSION_VARIATION_SETTINGS_FOR_PLACED_VEHICLES] No stat stored for the STOCKADE, so defaulting to preset 0. This should never happen though, except for dev testing")
								g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iModPreset = 0
							ELIF GET_PACKED_STAT_INT(PACKED_SUBTERFUGE_LICENCE_PLATE) = ciCASINO_HEIST_GRUPPE_SECHS_LICENCE_PLATE_47RPB540
								PRINTLN("[ML][MissionVariation][PROCESS_APPLY_MISSION_VARIATION_SETTINGS_FOR_PLACED_VEHICLES] Setting STOCKADE to mod preset 0")
								g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iModPreset = 0
							ELIF GET_PACKED_STAT_INT(PACKED_SUBTERFUGE_LICENCE_PLATE) = ciCASINO_HEIST_GRUPPE_SECHS_LICENCE_PLATE_28AVY903
								PRINTLN("[ML][MissionVariation][PROCESS_APPLY_MISSION_VARIATION_SETTINGS_FOR_PLACED_VEHICLES] Setting STOCKADE to mod preset 1")
								g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iModPreset = 1
							ELIF GET_PACKED_STAT_INT(PACKED_SUBTERFUGE_LICENCE_PLATE) = ciCASINO_HEIST_GRUPPE_SECHS_LICENCE_PLATE_29FNS081
								PRINTLN("[ML][MissionVariation][PROCESS_APPLY_MISSION_VARIATION_SETTINGS_FOR_PLACED_VEHICLES] Setting STOCKADE to mod preset 2")
								g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iModPreset = 2
							ENDIF
						BREAK
					ENDSWITCH
				ENDIF
			ENDIF
		ENDIF
		IF IS_USING_MISSION_VARIATION_VEHICLE_MODEL_SWAP_OVERRIDE()
		AND IS_THIS_VEHICLE_USING_MISSION_VARIATION_MODEL_SWAP_CREATOR_SETTINGS(iVeh)
			//Set the correct mod preset for the getaway car models.
			IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehGetawayVehicleSlot > -1
				g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iModPreset = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehGetawayVehicleSlot
				PRINTLN("[SCRIPT INIT][MissionVariation] - (Veh Mod Preset) Setting Mod Preset for: ", iVeh, " to: " , g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehGetawayVehicleSlot)
			ENDIF
		ENDIF
		IF IS_THIS_VEHICLE_USING_MISSION_VARIATION_SPAWN_FROM_ASSOCIATED_SELECTION_OVERRIDE(iVeh)
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Vehicle Spawn Associated) Clearing Block Spawn override for Veh: ", iVeh)
			CLEAR_BIT(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetEight, ciFMMC_VEHICLE8_BlockSpawnForAssociatedSelectionMissionVariation)
			IF bUnblockedSwitch
				PRINTLN("[SCRIPT INIT][MissionVariation] - (Zone Reposition) Veh: ", iVeh, " Has been flagged as an unblocked switch vehicle")
				SET_BIT(iVehicleSwapSwitchUnBlocked, iVeh)
			ENDIF
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetEight, ciFMMC_VEHICLE8_BuyerSpecialDropoffVehicle)
				PRINTLN("[SCRIPT INIT][MissionVariation] - (Zone Reposition) Veh: ", iVeh, " Has been flagged as a Buyer Special Drop off Vehicle.")
				SET_BIT(iVehicleBuyerLocationSpawned, iVeh)
			ENDIF
		ENDIF
		IF IS_THIS_VEHICLE_USING_MISSION_VARIATION_REPOSITION_FROM_ASSOCIATED_SELECTION_OVERRIDE(iVeh)			
			INT iIndex = GET_MISSION_VARIATION_REPOSITION_FROM_ASSOCIATED_SELECTION_INDEX()
			IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sWarpLocationSettings.vPosition[iIndex])
				PRINTLN("[SCRIPT INIT][MissionVariation] - (Vehicle Reposition) Setting new coordinates up vPos: ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sWarpLocationSettings.vPosition[iIndex], " fHead: ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sWarpLocationSettings.fHeading[iIndex], " for Veh: ", iVeh)
				g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].vpos = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sWarpLocationSettings.vPosition[iIndex]
				g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].fHead = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sWarpLocationSettings.fHeading[iIndex]			
			ELSE
				PRINTLN("[SCRIPT INIT][MissionVariation] - (Vehicle Reposition) iVeh: ", iVeh, " iIndex: ", iIndex, " sWarpLocationSettings.vPosition Vector is zero not repositioning.")
			ENDIF
		ENDIF
		IF IS_THIS_VEHICLE_USING_MISSION_VARIATION_VEHICLE_BLIP_OVERRIDE(iVeh) //Make sure this happens after any model swapping.
			g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].sVehBlipStruct.iBlipSpriteOverride = GET_MISSION_VARIATION_VEHICLE_BLIP_SPRITE_OVERRIDE(iVeh)
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Vehicle Blip Sprite) Blip Sprite Override set for Veh: ", iVeh)
		ENDIF
		IF IS_USING_MISSION_VARIATION_VEHICLE_RADIO_SEQUENCE_OVERRIDE() 
			IF IS_MISSION_VARIATION_BLOCKING_VEHICLE_RADIO_SEQUENCE()
			AND g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].eVehRadioSequenceType != VEHICLE_RADIO_SEQUENCE_TYPE_NONE
				PRINTLN("[SCRIPT INIT][MissionVariation] - (Radio Sequence) Sequence set from ", g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].eVehRadioSequenceType," to NONE for Veh: ", iVeh)
				g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].eVehRadioSequenceType = VEHICLE_RADIO_SEQUENCE_TYPE_NONE
			ENDIF
		ENDIF
	ENDFOR
	
ENDPROC

PROC PROCESS_APPLY_MISSION_VARIATION_SETTINGS_FOR_PLACED_OBJECTS()
	INT iTeam = 0
	INT iObj = 0	
	FOR iObj = 0 TO FMMC_MAX_NUM_OBJECTS - 1
		IF IS_THIS_OBJECT_USING_MISSION_VARIATION_CASINO_KEYPAD_MINIGAME(iObj)
			
			INT iClearanceRequired = INTERACT_WITH_KEYPAD_CLEARANCE_REQUIRED__NONE
			
			IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_KeycardAccessLevel1)
				iClearanceRequired = INTERACT_WITH_KEYPAD_CLEARANCE_REQUIRED__INSIDE_MAN_LVL1
			ELIF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_KeycardAccessLevel2)
				iClearanceRequired = INTERACT_WITH_KEYPAD_CLEARANCE_REQUIRED__INSIDE_MAN_LVL2
			ENDIF
			
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Casino Keypad) Setting for Obj: ", iObj, " | iClearanceRequired: ", iClearanceRequired)
			
			IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectKeypadAccessRequired <= iClearanceRequired
				// We can just swipe our card
				CLEAR_BIT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iLegacyMiniGameBitSet, cibsLEGACY_FMMC_MINI_GAME_TYPE_ORDER_UNLOCK)
				CLEAR_BIT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iLegacyMiniGameBitSet, cibsLEGACY_FMMC_MINI_GAME_TYPE_FINGERPRINT_CLONE)
				SET_BIT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iLegacyMiniGameBitSet, cibsLEGACY_FMMC_MINI_GAME_TYPE_INTERACT_WITH)
				g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInteractionAnim = ciINTERACT_WITH_PRESET__USE_KEYCARD
				PRINTLN("[SCRIPT INIT][MissionVariation] - (Casino Keypad) We have the swipe card! Setting iObjectInteractionAnim to INTERACT_WITH_SWIPE_CASINO_KEYPAD || Obj: ", iObj)
				
			ELSE
				// Fall back to having to hack it
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iLegacyMiniGameBitSet, cibsLEGACY_FMMC_MINI_GAME_TYPE_ORDER_UNLOCK)
					PRINTLN("[SCRIPT INIT][MissionVariation] - (Casino Keypad) Setting up hacking minigame: Order Unlock! || Obj: ", iObj)
				ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iLegacyMiniGameBitSet, cibsLEGACY_FMMC_MINI_GAME_TYPE_FINGERPRINT_CLONE)
					PRINTLN("[SCRIPT INIT][MissionVariation] - (Casino Keypad) Setting up hacking minigame: Fingerprint Clone! || Obj: ", iObj)
				ELSE
					PRINTLN("[SCRIPT INIT][MissionVariation] - (Casino Keypad) Setting up hacking minigame: Invalid type for Casino Keypad! || Obj: ", iObj)
				ENDIF
				
				// Hacking difficulty
				IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_HackerChosen1)
					CLEAR_BIT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iLegacyMiniGameBitSet, cibsLEGACY_FMMC_MINI_GAME_DIFF_EASY)
					CLEAR_BIT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iLegacyMiniGameBitSet, cibsLEGACY_FMMC_MINI_GAME_DIFF_MEDIUM)
					SET_BIT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iLegacyMiniGameBitSet, cibsLEGACY_FMMC_MINI_GAME_DIFF_HARD)
					PRINTLN("[SCRIPT INIT][MissionVariation] - (Casino Keypad) Setting up hacking minigame! || Obj: ", iObj, " Difficulty set to HARD.")
				ELIF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_HackerChosen2)
				OR   IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_HackerChosen3)
					CLEAR_BIT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iLegacyMiniGameBitSet, cibsLEGACY_FMMC_MINI_GAME_DIFF_EASY)
					CLEAR_BIT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iLegacyMiniGameBitSet, cibsLEGACY_FMMC_MINI_GAME_DIFF_HARD)
					SET_BIT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iLegacyMiniGameBitSet, cibsLEGACY_FMMC_MINI_GAME_DIFF_MEDIUM)
					PRINTLN("[SCRIPT INIT][MissionVariation] - (Casino Keypad) Setting up hacking minigame! || Obj: ", iObj, " Difficulty set to MEDIUM.")
				ELIF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_HackerChosen4)
				OR 	 IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_HackerChosen5)
					CLEAR_BIT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iLegacyMiniGameBitSet, cibsLEGACY_FMMC_MINI_GAME_DIFF_MEDIUM)
					CLEAR_BIT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iLegacyMiniGameBitSet, cibsLEGACY_FMMC_MINI_GAME_DIFF_HARD)
					SET_BIT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iLegacyMiniGameBitSet, cibsLEGACY_FMMC_MINI_GAME_DIFF_EASY)
					PRINTLN("[SCRIPT INIT][MissionVariation] - (Casino Keypad) Setting up hacking minigame! || Obj: ", iObj, " Difficulty set to EASY.")
				ENDIF
				
			ENDIF
		ENDIF
		IF IS_THIS_OBJECT_USING_MISSION_VARIATION_OBJECTS_SCOPED_OUT(iObj)			
			FOR iTeam = 0 TO FMMC_MAX_TEAMS-1
				PRINTLN("[SCRIPT INIT][MissionVariation] - (Objects Scoped Out) - Obj: ", iObj, " Setting that object should be blipped as it has been scoped out for iTeam: ", iTeam)
				g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].sObjBlipStruct.sTeamBlip[iTeam].iBlipOverrideStartRule = 0
				g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].sObjBlipStruct.sTeamBlip[iTeam].iBlipOverrideEndRule = FMMC_MAX_RULES
				g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].sObjBlipStruct.fBlipRange = 9999
			ENDFOR
		ENDIF
		
		IF IS_THIS_OBJECT_USING_MISSION_VARIATION_LOCATION_LINK_ONE_SPAWN_BLOCK(iObj)
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Objects Scoped Out - 1) - Obj: ", iObj, " Object was not scoped out. Setting that it's spawn should be blocked.")
			g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].eSpawnConditionFlag[0] = SPAWN_CONDITION_FLAG_NEVER_SPAWN
		ENDIF
		
		IF IS_THIS_OBJECT_USING_MISSION_VARIATION_LOCATION_LINK_TWO_SPAWN_BLOCK(iObj)
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Objects Scoped Out - 2) - Obj: ", iObj, " Object was not scoped out. Setting that it's spawn should be blocked.")
			g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].eSpawnConditionFlag[0] = SPAWN_CONDITION_FLAG_NEVER_SPAWN
		ENDIF
		
		IF IS_THIS_OBJECT_USING_MISSION_VARIATION_LOCATION_LINK_ONE_DEACTIVATE_MINIGAME(iObj)
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Objects Scoped Out - 1) - Obj: ", iObj, " Object was not scoped out. Setting that it's Minigame should be Deactivated.")			
			FOR iTeam = 0 TO MC_serverBD.iNumberOfTeams-1
				g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iPriority[iTeam] = FMMC_PRIORITY_IGNORE
				g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iRule[iTeam] = FMMC_OBJECTIVE_LOGIC_NONE
			ENDFOR
		ENDIF
		
		IF IS_THIS_OBJECT_USING_MISSION_VARIATION_LOCATION_LINK_TWO_DEACTIVATE_MINIGAME(iObj)
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Objects Scoped Out - 2) - Obj: ", iObj, " Object was not scoped out. Setting that it's Minigame should be Deactivated.")
			FOR iTeam = 0 TO MC_serverBD.iNumberOfTeams-1
				g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iPriority[iTeam] = FMMC_PRIORITY_IGNORE
				g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iRule[iTeam] = FMMC_OBJECTIVE_LOGIC_NONE
			ENDFOR
		ENDIF
				
		//Laser drill for vault
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iLegacyMiniGameBitSet, cibsLEGACY_FMMC_MINI_GAME_TYPE_VAULT_DRILLING)
			IF 	GET_MISSION_VARIATION_HAS_LASER()
				IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectBitSetFour, cibsOBJ4_VaultDoor_AllowLaser)
					SET_BIT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectBitSetFour, cibsOBJ4_VaultDoor_AllowLaser)
				ENDIF
			ELSE
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectBitSetFour, cibsOBJ4_VaultDoor_AllowLaser)
					CLEAR_BIT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectBitSetFour, cibsOBJ4_VaultDoor_AllowLaser)
				ENDIF		
			ENDIF
		ENDIF
		
		IF IS_THIS_OBJECT_USING_MISSION_VARIATION_OBJECTS_CHANGE_CLOTHES(iObj)
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Change Clothes) - Obj: ", iObj," Setting new outfit for changing clothes minigame object.")
			SET_MISSION_VARIATION_OUT_OUTFIT_CHOICE(iObj)
		ENDIF
		IF IS_THIS_MISSION_USING_MISSION_VARIATION_OBJECT_LINK_ONE_OVERRIDE()
			IF IS_THIS_OBJECT_USING_MISSION_VARIATION_LOCATION_LINK_ONE(iObj)
				PRINTLN("[SCRIPT INIT][MissionVariation] - (Object Link 1) - Obj: ", iObj," This is a linked object with a menu selection.")
				IF IS_THIS_OBJ_USING_MISSION_VARIATION_OBJECT_LINK_ONE_LARGE_BLIP_OVERRIDE(iObj)
					PRINTLN("[SCRIPT INIT][MissionVariation] - (Object Link 1 - Big Blip) - Obj: ", iObj," is having it's blip scale increased!")
					g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].sObjBlipStruct.fBlipScale += 0.2
					g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].sObjBlipStruct.fBlipRange = 0
				ENDIF
				//or other stuff...
			ENDIF
		ENDIF
		IF IS_THIS_OBJECT_USING_MISSION_VARIATION_OBJECTS_TARGET_SWAP(iObj)
			SET_MISSION_VARIATION_OBJECT_TARGET_SWAP(iObj)
		ENDIF
		IF IS_THIS_OBJECT_USING_MISSION_VARIATION_OBJECTS_REPOSITION_BASED_ON_TARGET(iObj)
			INT iIndex = GET_MISSION_VARIATION_OBJECT_REPOSITION_BASED_ON_TARGET_INDEX()
			IF iIndex > -1
				IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].sWarpLocationSettings.vPosition[iIndex])
					PRINTLN("[SCRIPT INIT][MissionVariation] - (Object Reposition) iObj: ", iObj, " iIndex: ", iIndex, " Setting new coordinates up vPos: ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].sWarpLocationSettings.vPosition[iIndex], " fHead: ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].sWarpLocationSettings.fHeading[iIndex], " for Obj: ", iObj)
					g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].vpos = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].sWarpLocationSettings.vPosition[iIndex]
					g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].fHead = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].sWarpLocationSettings.fHeading[iIndex]			
				ELSE
					PRINTLN("[SCRIPT INIT][MissionVariation] - (Object Reposition) iObj: ", iObj, " iIndex: ", iIndex, " sWarpLocationSettings.vPosition Vector is zero not repositioning.")
				ENDIF	
			ELSE
				PRINTLN("[SCRIPT INIT][MissionVariation] - (Object Reposition) iObj: ", iObj, " iIndex: ", iIndex, " invalid...")
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

PROC PROCESS_APPLY_MISSION_VARIATION_SETTINGS_FOR_PLACED_DYNOPROPS()
	INT iDynoProp = 0	
	FOR iDynoProp = 0 TO FMMC_MAX_NUM_DYNOPROPS - 1
		IF IS_THIS_DYNOPROP_USING_MISSION_VARIATION_DYNOPROPS_SCOPED_OUT(iDynoProp)
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Dynoprops Scoped Out) - iDynoProp: ", iDynoProp, " Setting that dynoprop should be blipped as it has been scoped out")
			SET_BIT(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].iDynopropBitset, ciFMMC_DYNOPROP_ShowBlip)
			g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoProp].sDynoPropBlipStruct.fBlipRange = 9999.0
		ENDIF
	ENDFOR
ENDPROC

PROC PROCESS_APPLY_MISSION_VARIATION_SETTINGS_FOR_LOCATIONS()		
	INT iTeam
	INT iLoc = 0
	FOR iLoc = 0 TO FMMC_MAX_GO_TO_LOCATIONS - 1	
		IF IS_THIS_MISSION_VARIATION_USING_LOCATION_LINK_ONE_LARGE_BLIP_CHOSEN_ENTRANCE()
			IF IS_THIS_LOCATION_USING_MISSION_VARIATION_LOCATION_LINK_ONE(iLoc)
				PRINTLN("[SCRIPT INIT][MissionVariation] - (Location Link 1 - Large Blip Entrance) iLoc: ", iLoc, " is having it's blip ranges set to -1!")
				g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].sLocBlipStruct.fBlipRange = -1.0
				g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].sLocBlipStruct.fBlipHeightDifference = -1.0
			ELSE
				IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocMCVarSelectionIndex_One != -1
					PRINTLN("[SCRIPT INIT][MissionVariation] - (Location Link 1 - Large Blip Entrance) iLoc: ", iLoc, " is having it's blip scale decreased as isn't scoped as the entrance!")
					g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].sLocBlipStruct.fBlipScale -= 0.3
				ENDIF
			ENDIF
		ENDIF
		IF IS_THIS_MISSION_VARIATION_USING_LOCATION_LINK_TWO_LARGE_BLIP_CHOSEN_EXIT()
			IF IS_THIS_LOCATION_USING_MISSION_VARIATION_LOCATION_LINK_TWO(iLoc)
				PRINTLN("[SCRIPT INIT][MissionVariation] - (Location Link 2 - Large Blip Exit) iLoc: ", iLoc, " is having it's blip ranges set to -1!")
				g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].sLocBlipStruct.fBlipRange = -1.0
			ELSE
				IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocMCVarSelectionIndex_Two != -1
					PRINTLN("[SCRIPT INIT][MissionVariation] - (Location Link 2 - Large Blip Exit) iLoc: ", iLoc, " is having it's blip scale decreased as isn't scoped as the exit!")
					g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].sLocBlipStruct.fBlipScale -= 0.3
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_THIS_LOCATION_USING_MISSION_VARIATION_LOCATION_LINK_ONE_SPAWN_BLOCK(iLoc)
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Location Link 1 - Block Spawn) iLoc: ", iLoc, " is having it's Spawn blocked as isn't scoped as the entrance!")
			FOR iTeam = 0 TO FMMC_MAX_TEAMS-1
				g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iPriority[iTeam] = FMMC_PRIORITY_IGNORE
				g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iRule[iTeam] = FMMC_OBJECTIVE_LOGIC_NONE
			ENDFOR
		ELIF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocMCVarSelectionIndex_One > -1
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Location Link 1 - Block Spawn) iLoc: ", iLoc, " is not blocked by variation system...")
		ENDIF
		
		IF IS_THIS_LOCATION_USING_MISSION_VARIATION_LOCATION_LINK_TWO_SPAWN_BLOCK(iLoc)
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Location Link 2 - Block Spawn) iLoc: ", iLoc, " is having it's Spawn blocked as isn't scoped as the exit!")
			FOR iTeam = 0 TO FMMC_MAX_TEAMS-1
				g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iPriority[iTeam] = FMMC_PRIORITY_IGNORE
				g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iRule[iTeam] = FMMC_OBJECTIVE_LOGIC_NONE
			ENDFOR
		ELIF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iLocMCVarSelectionIndex_Two > -1
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Location Link 2 - Block Spawn) iLoc: ", iLoc, " is not blocked by variation system...")	
		ENDIF
		
		IF IS_USING_MISSION_VARIATION_LOCATION_PREREQUISITE()
		AND IS_THIS_LOCATION_USING_MISSION_VARIATION_LOCATION_PREREQUISITE(iLoc)
			PROCESS_MISSION_VARIATION_LOCATION_PREQUISITE(iLoc)
		ENDIF
		IF IS_THIS_MISSION_VARIATION_USING_LOCATION_JUMP_TO_OBJECTIVE_OVERRIDE()
		AND IS_THIS_LOCATION_USING_MISSION_VARIATION_JUMP_TO_OBJECTIVE_OVERRIDE(iLoc)
			PROCESS_APPLY_MISSION_VARIATION_JUMP_TO_OBJECTIVE_OVERRIDE(iLoc)
		ENDIF
	ENDFOR
ENDPROC

PROC PROCESS_APPLY_MISSION_VARIATION_SETTINGS_FOR_DIALOGUE_TRIGGERS()
	INT iDialogueTrigger
	FOR iDialogueTrigger = 0 TO FMMC_MAX_DIALOGUES_LEGACY-1
		IF NOT DOES_DIALOGUE_TRIGGER_REQUIREMENT_MATCH_DATA(iDialogueTrigger)
			g_FMMC_STRUCT.sDialogueTriggers[iDialogueTrigger].iRule = -1
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Dialogue Trigger Requirement) DT ", iDialogueTrigger, " didn't meet it's requirements. Disabling it.")
		ENDIF
	ENDFOR
ENDPROC

PROC SEED_SPAWN_SUBGROUP_FOR_MISSION_VARIATION_DROPOFF_SPECIAL_CASE(INT iIndex)
	INT iSubSpawnIndex = -1
	IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EndOfMissionDropOffOverride1)
		iSubSpawnIndex = 0
	ELIF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EndOfMissionDropOffOverride2)
		iSubSpawnIndex = 1
	ELIF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EndOfMissionDropOffOverride3)
		iSubSpawnIndex = 2
	ENDIF
	
	PRINTLN("[SCRIPT INIT][MissionVariation] - drop off menu index: ", iSubSpawnIndex)
	
	IF iSubSpawnIndex > -1			
		SET_BIT(MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeed_SubGroupBS[iIndex-1], iSubSpawnIndex)
	ENDIF
ENDPROC

PROC SEED_SPAWN_SUBGROUP_FOR_MISSION_VARIATION(INT iIndex, INT iGroup)
	IF g_FMMC_STRUCT.sMissionVariation.eMCVarConfigEnum = FMMC_MC_VAR_CONFIG_ENUM_CASINO_HEIST		
		IF iGroup = 0
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Spawn Group Forcing ", iGroup," Hard setting spawn sub groups for drop off location.")
			SEED_SPAWN_SUBGROUP_FOR_MISSION_VARIATION_DROPOFF_SPECIAL_CASE(iIndex)
		ELSE
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Spawn Group Forcing ", iGroup," Calling to seed sub spawn groups now.")
			SEED_SPAWN_SUBGROUP(iIndex)
		ENDIF
	ELSE
		// Regular way.
		PRINTLN("[SCRIPT INIT][MissionVariation] - (Spawn Group Forcing ", iGroup," Calling to seed sub spawn groups now.")
		SEED_SPAWN_SUBGROUP(iIndex)
	ENDIF
ENDPROC

FUNC BOOL IS_MISSION_VARIATION_USING_DUFFEL_BAG()
	RETURN IS_USING_MISSION_VARIATION_HEIST_GEAR_BAG_OVERRIDE()
ENDFUNC

PROC CLEAR_DEFAULT_SELECTED_HEIST_GEAR(INT iTeam)
	INT iLoop
	FOR iLoop = 0 TO ENUM_TO_INT(GEAR_MAX_AMOUNT)-1
		IF IS_LONG_BIT_SET(g_FMMC_STRUCT.biGearAvailableBitset[iTeam], iLoop)
		AND IS_HEIST_GEAR_A_BAG(INT_TO_ENUM(MP_HEIST_GEAR_ENUM, iLoop))
			CLEAR_LONG_BIT(g_FMMC_STRUCT.biGearAvailableBitset[iTeam], iLoop)
			PRINTLN("CLEAR_DEFAULT_SELECTED_HEIST_GEAR - Clearing Long bit ", iLoop)
		ENDIF
	ENDFOR
ENDPROC

PROC SET_UP_DUFFEL_BAG_VARIATION(INT iTeam)
	
	IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_ApproachStealth)
		CLEAR_DEFAULT_SELECTED_HEIST_GEAR(iTeam)
		SET_LONG_BIT(g_FMMC_STRUCT.biGearAvailableBitset[iTeam], ENUM_TO_INT(HEIST_GEAR_DUFFEL_CAMO_4_FULL))
		g_FMMC_STRUCT.iGearDefault[iTeam] = ENUM_TO_INT(HEIST_GEAR_DUFFEL_CAMO_4_FULL)
		PRINTLN("SET_UP_DUFFEL_BAG_VARIATION - Setting Stealth Bag")
	ENDIF
	
	IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_ApproachSubterfuge)
		IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_OutfitBrucie)
			CLEAR_DEFAULT_SELECTED_HEIST_GEAR(iTeam)
			SET_LONG_BIT(g_FMMC_STRUCT.biGearAvailableBitset[iTeam], ENUM_TO_INT(HEIST_GEAR_DUFFEL_CELEB_FULL))
			g_FMMC_STRUCT.iGearDefault[iTeam] = ENUM_TO_INT(HEIST_GEAR_DUFFEL_CELEB_FULL)
			PRINTLN("SET_UP_DUFFEL_BAG_VARIATION - Setting Celebrity Bag")
		ELIF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_OutfitBugstar)
			CLEAR_DEFAULT_SELECTED_HEIST_GEAR(iTeam)
			SET_LONG_BIT(g_FMMC_STRUCT.biGearAvailableBitset[iTeam], ENUM_TO_INT(HEIST_GEAR_DUFFEL_BUGSTAR_FULL))
			g_FMMC_STRUCT.iGearDefault[iTeam] = ENUM_TO_INT(HEIST_GEAR_DUFFEL_BUGSTAR_FULL)
			PRINTLN("SET_UP_DUFFEL_BAG_VARIATION - Setting Bugstar Bag")
		ELIF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_OutfitGruppeSechs)
			CLEAR_DEFAULT_SELECTED_HEIST_GEAR(iTeam)
			SET_LONG_BIT(g_FMMC_STRUCT.biGearAvailableBitset[iTeam], ENUM_TO_INT(HEIST_GEAR_DUFFEL_BLACK_FULL))
			g_FMMC_STRUCT.iGearDefault[iTeam] = ENUM_TO_INT(HEIST_GEAR_DUFFEL_BLACK_FULL)
			PRINTLN("SET_UP_DUFFEL_BAG_VARIATION - Setting Gruppe Sechs Bag")
		ELIF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_OutfitMechanic)
			CLEAR_DEFAULT_SELECTED_HEIST_GEAR(iTeam)
			SET_LONG_BIT(g_FMMC_STRUCT.biGearAvailableBitset[iTeam], ENUM_TO_INT(HEIST_GEAR_DUFFEL_RED_FULL))
			g_FMMC_STRUCT.iGearDefault[iTeam] = ENUM_TO_INT(HEIST_GEAR_DUFFEL_RED_FULL)
			PRINTLN("SET_UP_DUFFEL_BAG_VARIATION - Setting Maintenance Bag")
		ENDIF
	ENDIF
	
	IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_ApproachDirect)
		CLEAR_DEFAULT_SELECTED_HEIST_GEAR(iTeam)
		SET_LONG_BIT(g_FMMC_STRUCT.biGearAvailableBitset[iTeam], ENUM_TO_INT(HEIST_GEAR_DUFFEL_CAMO_1_FULL))
		g_FMMC_STRUCT.iGearDefault[iTeam] = ENUM_TO_INT(HEIST_GEAR_DUFFEL_CAMO_1_FULL)
		PRINTLN("SET_UP_DUFFEL_BAG_VARIATION - Setting Direct Bag")
	ENDIF
		
	IF IS_HEIST_GEAR_A_BAG(INT_TO_ENUM(MP_HEIST_GEAR_ENUM, g_FMMC_STRUCT.iGearDefault[iTeam]))
	AND sApplyOutfitData.eGear != INT_TO_ENUM(MP_HEIST_GEAR_ENUM, g_FMMC_STRUCT.iGearDefault[iTeam])
		SET_BAG_FOR_CASINO_HEIST(TRUE)
		SET_CACHED_HEIST_GEAR_BAG()
		APPLY_DUFFEL_BAG_HEIST_GEAR(LocalPlayerPed, iLocalPart)
	ENDIF
	
	PRINTLN("SET_UP_DUFFEL_BAG_VARIATION - iTeam: ", iTeam, " g_FMMC_STRUCT.iGearDefault[iTeam]: ", g_FMMC_STRUCT.iGearDefault[iTeam], " Is the bit set? ", IS_LONG_BIT_SET(g_FMMC_STRUCT.biGearAvailableBitset[iTeam], g_FMMC_STRUCT.iGearDefault[iTeam]))
ENDPROC

FUNC TEXT_LABEL_63 GET_MISSION_VARIATION_PRIMARY_OBJECTIVE_OVERRRIDE()
	TEXT_LABEL_63 tl63 = ""
	
	IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SelectedExit1)
	AND NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sMissionVariation.tl63PrimaryObjectiveOverride[0])
		tl63 = g_FMMC_STRUCT.sMissionVariation.tl63PrimaryObjectiveOverride[0]
	ELIF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SelectedExit2)
	AND NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sMissionVariation.tl63PrimaryObjectiveOverride[1])
		tl63 = g_FMMC_STRUCT.sMissionVariation.tl63PrimaryObjectiveOverride[1]
	ELIF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SelectedExit3)
	AND NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sMissionVariation.tl63PrimaryObjectiveOverride[2])
		tl63 = g_FMMC_STRUCT.sMissionVariation.tl63PrimaryObjectiveOverride[2]
	ELIF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SelectedExit4)
	AND NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sMissionVariation.tl63PrimaryObjectiveOverride[3])
		tl63 = g_FMMC_STRUCT.sMissionVariation.tl63PrimaryObjectiveOverride[3]
	ELIF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SelectedExit5)
	AND NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sMissionVariation.tl63PrimaryObjectiveOverride[4])
		tl63 = g_FMMC_STRUCT.sMissionVariation.tl63PrimaryObjectiveOverride[4]
	ELIF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SelectedExit6)
	AND NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sMissionVariation.tl63PrimaryObjectiveOverride[5])
		tl63 = g_FMMC_STRUCT.sMissionVariation.tl63PrimaryObjectiveOverride[5]
	ELIF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SelectedExit7)
	AND NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sMissionVariation.tl63PrimaryObjectiveOverride[6])
		tl63 = g_FMMC_STRUCT.sMissionVariation.tl63PrimaryObjectiveOverride[6]
	ELIF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SelectedExit8)
	AND NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sMissionVariation.tl63PrimaryObjectiveOverride[7])
		tl63 = g_FMMC_STRUCT.sMissionVariation.tl63PrimaryObjectiveOverride[7]
	ELIF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SelectedExit9)
	AND NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sMissionVariation.tl63PrimaryObjectiveOverride[8])
		tl63 = g_FMMC_STRUCT.sMissionVariation.tl63PrimaryObjectiveOverride[8]
	ELIF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_ApproachStealth)
	AND NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sMissionVariation.tl63PrimaryObjectiveOverride[9])
		tl63 = g_FMMC_STRUCT.sMissionVariation.tl63PrimaryObjectiveOverride[9]
	ELIF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_ApproachSubterfuge)
	AND NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sMissionVariation.tl63PrimaryObjectiveOverride[10])
		tl63 = g_FMMC_STRUCT.sMissionVariation.tl63PrimaryObjectiveOverride[10]
	ELIF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_ApproachDirect)
	AND NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sMissionVariation.tl63PrimaryObjectiveOverride[11])
		tl63 = g_FMMC_STRUCT.sMissionVariation.tl63PrimaryObjectiveOverride[11]
	ENDIF
	
	RETURN tl63
ENDFUNC

PROC PROCESS_APPLY_MISSION_VARIATION_SETTINGS_FOR_GLOBALS()
	INT iRule
	INT iTeam
	INT iTimerIndex
	INT i
	
	IF bIsLocalPlayerHost // This is for Entity Creation so non-host clients won't care about this data anyway and they'll receive the update when the server broadcast data is synced across the network.
		IF IS_USING_MISSION_VARIATION_SPAWN_GROUP_FORCING_OVERRIDE()
			MC_ServerBD_4.rsgSpawnSeed.bForceValidate = TRUE
			
			FOR i = 0 TO FMMC_MAX_MC_VAR_SIZE_SPAWN_GROUP_FORCING_OPTIONS-1
				IF IS_THIS_MISSION_USING_MISSION_VARIATION_SPAWN_GROUP_FORCING_OVERRIDE(i)
					INT iIndex = GET_MISSION_VARIATION_SPAWN_GROUP_FORCING_SPAWN_GROUP_TO_FORCE(i)
					SET_BIT(MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeedBS, iIndex - 1)
					PRINTLN("[SCRIPT INIT][MissionVariation] - (Spawn Group Forcing ", i,") Setting bit: ", iIndex, " MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeedBS is now = ", MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeedBS)					
					SEED_SPAWN_SUBGROUP_FOR_MISSION_VARIATION(iIndex, i)
				ENDIF
			ENDFOR
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_PhoneEMPAvailable_CasinoPrep)
		IF NOT GET_MISSION_VARIATION_HAS_EMP()
			CLEAR_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_PhoneEMPAvailable_CasinoPrep)
			PRINTLN("[SCRIPT INIT][MissionVariation] - (EMP) - Set as not available")
		ENDIF
	ENDIF
	
	IF GET_MISSION_VARIATION_HAS_HANDHELD_DRILL()
		PRINTLN("[SCRIPT INIT][MissionVariation] - [LM][HandHeldDrill] - Setting that all players have the drill.")
		SET_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_GiveAllPlayersHandheldDrill)
	ENDIF
	
	IF NOT IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_EquipNightVisionOutfit)
		PRINTLN("[SCRIPT INIT][MissionVariation] - [LM] - Clearing ciOptionsBS25_AutomaticallyToggleNightvisionWithEMP as nightvision prep hasn't been done.")
		CLEAR_BIT(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFive, ciOptionsBS25_AutomaticallyToggleNightvisionWithEMP)
	ENDIF
	
	FOR iRule = 0 TO FMMC_MAX_RULES-1		
		IF IS_USING_MISSION_VARIATION_TIMER_OVERRIDING_OVERRIDE()
			FOR iTimerIndex = 0 TO FMMC_MAX_MC_VAR_SIZE_TIMER_OVERRIDE-1
				IF NOT IS_MISSION_VARIATION_TIMER_OVERRIDE_FOR_ANY_TIMER(iTimerIndex)
					RELOOP
				ENDIF
				IF NOT IS_MISSION_VARIATION_TIMER_OVERRIDE_VALID_FOR_RULE(iTimerIndex, iRule)
					RELOOP
				ENDIF
				IF GET_MISSION_VARIATION_TIMER_OVERRIDE_VALUE(iTimerIndex) <= -1
					RELOOP
				ENDIF
				
				IF IS_MISSION_VARIATION_TIMER_OVERRIDE_FOR_RULE_TIMER(iTimerIndex)
					FOR iTeam = 0 TO FMMC_MAX_TEAMS-1
						PRINTLN("[SCRIPT INIT][MissionVariation] - (Override Timer) (Objective) Using Override Timer: ", iTimerIndex, " Applying override value: ", GET_MISSION_VARIATION_TIMER_OVERRIDE_VALUE(iTimerIndex), " to rule: ", iRule)
						g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjectiveTimeLimitRule[iRule] = GET_MISSION_VARIATION_TIMER_OVERRIDE_VALUE(iTimerIndex)
					ENDFOR
				ELIF IS_MISSION_VARIATION_TIMER_OVERRIDE_FOR_MULTIRULE_TIMER(iTimerIndex)
					FOR iTeam = 0 TO FMMC_MAX_TEAMS-1						
						PRINTLN("[SCRIPT INIT][MissionVariation] - (Override Timer) (Multirule) Using Override Timer: ", iTimerIndex, " Applying override value: ", GET_MISSION_VARIATION_TIMER_OVERRIDE_VALUE(iTimerIndex), " to rule: ", iRule)
						g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTimeLimit[iRule] = GET_MISSION_VARIATION_TIMER_OVERRIDE_VALUE(iTimerIndex)
					ENDFOR
				ELIF IS_MISSION_VARIATION_TIMER_OVERRIDE_FOR_ZONE_TIMER(iTimerIndex)
					PRINTLN("[SCRIPT INIT][MissionVariation] - (Override Timer) (Zone Timer) Using Override Timer: ", iTimerIndex, " Applying override value: ", GET_MISSION_VARIATION_TIMER_OVERRIDE_VALUE(iTimerIndex), " to rule: ", iRule)
					g_FMMC_STRUCT_ENTITIES.sPlacedZones[g_FMMC_STRUCT.sMissionVariation.iVarMissionTimerZoneIndex[iTimerIndex]].iZoneTimer_EnableTime = GET_MISSION_VARIATION_TIMER_OVERRIDE_VALUE(iTimerIndex)
				ENDIF
			ENDFOR		
		ENDIF
		
		IF IS_MISSION_VARIATION_USING_NIGHT_VISION_TOGGLING()
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFive, ciOptionsBS25_AutomaticallyToggleNightvisionWithEMP)
				FOR iTeam = 0 TO FMMC_MAX_TEAMS-1
					#IF IS_DEBUG_BUILD
					IF iTeam = 0 and iRule = 0
						PRINTLN("[SCRIPT INIT][MissionVariation] - (Night Vision) Setting ciBS_RULE5_ENABLE_NIGHTVISION_TOGGLING on all rules")
					ENDIF
					#ENDIF
					
					SET_BIT(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFive[iRule], ciBS_RULE5_ENABLE_NIGHTVISION_TOGGLING)
				ENDFOR
			ENDIF
		ENDIF
		
		IF IS_USING_MISSION_VARIATION_PRIMARY_OBJECTIVE_TEXT_OVERRIDE()
			FOR iTeam = 0 TO FMMC_MAX_TEAMS - 1
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFourteen[iRule], ciBS_RULE14_MISSION_VARIATION_OVERRIDE_PRIMARY_TEXT)
					TEXT_LABEL_63 tl63 = GET_MISSION_VARIATION_PRIMARY_OBJECTIVE_OVERRRIDE()
					IF NOT IS_STRING_NULL_OR_EMPTY(tl63)
						g_FMMC_STRUCT.sFMMCEndConditions[iTeam].tl63Objective[iRule] = tl63
						PRINTLN("[SCRIPT INIT][MissionVariation] - (Primary Objective Text) - Setting Team: ", iTeam, " Rule: ", iRule," to ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].tl63Objective[iRule])
					ENDIF
				ENDIF
			ENDFOR
		ENDIF
	ENDFOR
	
	IF IS_MISSION_VARIATION_USING_DUFFEL_BAG()
		FOR iTeam = 0 TO FMMC_MAX_TEAMS-1
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitset4, ciBS4_TEAM_USE_BAG_VARIATION)
				SET_UP_DUFFEL_BAG_VARIATION(iTeam)
				PRINTLN("[SCRIPT INIT][MissionVariation] - (Heist Bag) - Setting default heist gear to correct bag team, ", iTeam)
			ENDIF
		ENDFOR
	ENDIF	
	
	IF IS_MISSION_VARIATION_END_CUTSCENE_OVERRIDE_VALID()
		INT iIndex = GET_MISSION_VARIATION_END_CTUSCENE_OVERRIDE_INDEX()		
		PRINTLN("[SCRIPT INIT][MissionVariation] - (End Cutscene Override) Using index: ", iIndex)
		IF iIndex > -1			
			INT iCutscene = GET_MISSION_VARIATION_END_CTUSCENE_OVERRIDE_VALUE(iIndex)
			IF iCutscene > 0
				TEXT_LABEL_15 tl15 = "FMMC_CUT"
				tl15 += iCutscene
				PRINTLN("[SCRIPT INIT][MissionVariation] - (End Cutscene Override) iCutscene set to: ", iCutscene, " Name: ", tl15)
				g_FMMC_STRUCT.iEndCutscene = iCutscene
			ENDIF
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT.sMissionVariation.eMCVarConfigEnum = FMMC_MC_VAR_CONFIG_ENUM_CASINO_HEIST 
	AND IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_OutfitMechanic)
		PRINTLN("[SCRIPT INIT][MissionVariation] Setting INTERIOR2_CASINO_USE_LAUNDRY_DAMAGE")
		SET_BIT(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO_USE_LAUNDRY_DAMAGE)
	ENDIF
	
ENDPROC

PROC PROCESS_APPLY_MISSION_VARIATION_SETTINGS_FOR_PTFX()
	
	INT i = 0
	FOR i = 0 TO g_FMMC_STRUCT.iNumPlacedPTFX-1
		IF i < FMMC_MAX_PLACED_PTFX
			IF IS_THIS_PTFX_USING_A_PREREQUISITE_MISSION_ELEMENT(i)
				INT iIndex = GET_MISSION_VARIATION_PTFX_PREREQUISITE_MISSION_ELEMENT(i)
				PRINTLN("[SCRIPT INIT][MissionVariation] - (PTFX PreReq) PTFX: ", i, " is using a Prerequisite Mission Element Index of: ", iIndex)
				
				IF HAS_MISSION_VARIATION_PREREQUISITE_MISSION_ELEMENT_BEEN_COMPLETED(iIndex)
					PRINTLN("[SCRIPT INIT][MissionVariation] - (PTFX PreReq) PTFX: ", i, " has completed a Prerequisite Mission Element Index of: ", iIndex)
					
					IF IS_THIS_PTFX_USING_BLOCK_SPAWN_FROM_PREREQUISITE(i)
						PRINTLN("[SCRIPT INIT][MissionVariation] - (PTFX PreReq) PTFX: ", i, " Clearing ciPLACED_PTFX_MCVarConfig_BlockSpawnUnlessCompletedPrereqMission_1")
						CLEAR_BIT(g_FMMC_STRUCT.sPlacedPTFX[i].iPTFXBS, ciPLACED_PTFX_MCVarConfig_BlockSpawnUnlessCompletedPrereqMission_1)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
ENDPROC

PROC PROCESS_APPLY_MISSION_VARIATION_SETTINGS_FOR_PLACED_ZONES()
	INT iZone
	FOR iZone = 0 TO FMMC_MAX_NUM_ZONES - 1			
		IF IS_THIS_ZONE_USING_MISSION_VARIATION_REPOSITION_TO_LINKED_ENTITY(iZone)
			INT iVeh = 0
			FOR iVeh = 0 TO FMMC_MAX_VEHICLES-1	
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneBS, ciFMMC_ZONEBS_MissionVariationPositionZoneAtVarEntity)
					IF IS_BIT_SET(iVehicleSwapSwitchUnBlocked, iVeh)
						PRINTLN("[SCRIPT INIT][MissionVariation] - (Zone Reposition) iZone: ", iZone, " and it's been unblocked, Veh: ", iVeh)
						PROCESS_MISSION_VARIATION_MOVE_ZONES_TO_LINKED_ENTITIES(iZone, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].vpos)
					ELSE
						PRINTLN("[SCRIPT INIT][MissionVariation] - (Zone Reposition) iZone: ", iZone, " But it has not been unblocked, Veh: ", iVeh)
					ENDIF
				ENDIF
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneBS, ciFMMC_ZONEBS_MissionVariationPositionZoneAtVarEntity2)
					IF IS_BIT_SET(iVehicleBuyerLocationSpawned, iVeh)
						PRINTLN("[SCRIPT INIT][MissionVariation] - (Zone Reposition) iZone: ", iZone, " and it's a Buyer Vehicle, Veh: ", iVeh)
						PROCESS_MISSION_VARIATION_MOVE_ZONES_TO_LINKED_ENTITIES(iZone, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].vpos)
					ELSE
						PRINTLN("[SCRIPT INIT][MissionVariation] - (Zone Reposition) iZone: ", iZone, " But it's not a Buyer Vehicle, Veh: ", iVeh)
					ENDIF
				ENDIF
			ENDFOR
		ENDIF
	ENDFOR			
ENDPROC

PROC PROCESS_APPLY_MISSION_VARIATION_SETTINGS_FOR_PLACED_DUMMY_BLIPS()
	INT iDummyBlip
	FOR iDummyBlip = 0 TO FMMC_MAX_DUMMY_BLIPS - 1
		IF IS_THIS_DUMMY_BLIP_USING_MISSION_VARIATION_EXIT_RESTRICTION(iDummyBlip)
			PROCESS_MISSION_VARIATION_DUMMY_BLIP_EXITS(iDummyBlip)
		ENDIF
	ENDFOR
ENDPROC

#IF FEATURE_CASINO_HEIST
#IF IS_DEBUG_BUILD
PROC PROCESS_BAIL_MISSION_VARIATION_SETTINGS_ARE_OUT_OF_SYNC()
	
	IF bByPassMissionVarSyncBail
		EXIT
	ENDIF
	
	IF bMustBailOutOfSyncChecked AND NOT bMustBailOutOfSync
		EXIT
	ENDIF
	
	IF MC_playerBD[iLocalPart].iGameState != GAME_STATE_RUNNING
	OR g_FMMC_STRUCT.sMissionVariation.eMCVarConfigEnum = FMMC_MC_VAR_CONFIG_ENUM_OFF
	OR g_bMissionEnding
	OR g_bCelebrationScreenIsActive
	OR NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_FIRST_UPDATE_DONE)
	OR iPlayerPressedF != -1
	OR IS_LOCAL_PLAYER_ANY_SPECTATOR()
	OR DID_I_JOIN_MISSION_AS_SPECTATOR()
	OR IS_PLAYER_SCTV(localPlayer)
		EXIT
	ENDIF
	
	IF bIsLocalPlayerHost
		SET_LONG_BIT(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SYNC_DUMMY_CONST)
	ELIF NOT IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SYNC_DUMMY_CONST)
		EXIT
	ENDIF
	
	IF NOT bMustBailOutOfSync
		INT i = 0
		FOR i = 0 TO FMMC_MAX_MC_VAR_CHOICE_BS-1
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Sync Check) iMissionVariationChoicesBS[",i,"] = ", MC_ServerBD_1.iMissionVariationChoicesBS[i], "     iLocalMissionVariationChoicesBS[",i,"] = ", iLocalMissionVariationChoicesBS[i])
		ENDFOR
		
		FOR i = 0 TO MCVarConfigBS_SYNC_DUMMY_CONST-1
			IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, i)
			AND NOT IS_LONG_BIT_SET(iLocalMissionVariationChoicesBS, i)
				bMustBailOutOfSync = TRUE	
			ENDIF
			
			IF NOT IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, i)
			AND IS_LONG_BIT_SET(iLocalMissionVariationChoicesBS, i)
				bMustBailOutOfSync = TRUE
			ENDIF
		ENDFOR
	ELIF bMustBailOutOfSync
		
		IF NOT HAS_NET_TIMER_STARTED(bMustBailOutOfSyncTimer)
			START_NET_TIMER(bMustBailOutOfSyncTimer)
		ENDIF
		FLOAT fOffsetX = GET_RANDOM_FLOAT_IN_RANGE(0.00, 0.01)
		FLOAT fOffsetY = GET_RANDOM_FLOAT_IN_RANGE(0.00, 0.01)
		IF GET_FRAME_COUNT() % 6 = 0
			
			IF bMustBailOutOfSyncFlash 
				bMustBailOutOfSyncFlash = FALSE
			ELSE
				bMustBailOutOfSyncFlash  = TRUE
			ENDIF		
		ENDIF
		
		SET_TEXT_SCALE(1.0, 1.0)
		IF bMustBailOutOfSyncFlash
			SET_TEXT_COLOUR(150, 70, 70, 150)
		ELSE
			SET_TEXT_COLOUR(225, 150, 150, 220)
		ENDIF
		SET_TEXT_DROPSHADOW(4, 0, 0, 0, 255)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.45+fOffsetX, 0.425+fOffsetY, "STRING", "CONFIG DATA SETUP IS INCORRECT")
		
		SET_TEXT_SCALE(1.0, 1.0)
		IF bMustBailOutOfSyncFlash
			SET_TEXT_COLOUR(225, 150, 150, 220)
		ELSE
			SET_TEXT_COLOUR(150, 70, 70, 150)
		ENDIF
		SET_TEXT_DROPSHADOW(4, 0, 0, 0, 255)
		
		TEXT_LABEL_63 tl63 = "BAILING FROM MISSION IN ... "
		INT iTime = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(bMustBailOutOfSyncTimer)
		iTime /= 1000
		tl63 += (5-iTime)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.45+fOffsetX, 0.5+fOffsetY, "STRING", tl63)
		
		IF HAS_NET_TIMER_EXPIRED_READ_ONLY(bMustBailOutOfSyncTimer, 5000)
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Sync Check) Setting to BAIL!")
			ASSERTLN("The Local Client's Mission variation settings are not in-sync with what the host has. This means that the config data has been incorrectly set up.")
			SET_BIT(MC_playerBD[iLocalPart].iClientBitSet, ENUM_TO_INT(PBBOOL_PRESSED_F))
		ENDIF
	ENDIF
	
	IF IS_LONG_BIT_SET(MC_ServerBD_1.iMissionVariationChoicesBS, MCVarConfigBS_SYNC_DUMMY_CONST)
		bMustBailOutOfSyncChecked = TRUE
	ENDIF
ENDPROC
PROC SET_UP_LOCAL_MISSION_VARIATION_VARS_FOR_SAFETY_BAIL()	
	INT i = 0
	FOR i = 0 TO FMMC_MAX_MC_VAR_CHOICE_BS-1
		PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (Sync Check) iMissionVariationChoicesBS[",i,"] = ", MC_ServerBD_1.iMissionVariationChoicesBS[i])
		iLocalMissionVariationChoicesBS[i] = MC_ServerBD_1.iMissionVariationChoicesBS[i]
	ENDFOR
ENDPROC
#ENDIF
#ENDIF

PROC PROCESS_APPLY_MISSION_VARIATION_SETTINGS_DEBUG()
#IF IS_DEBUG_BUILD
	PRINTLN("[SCRIPT INIT][MissionVariation] - (Setup) Starting Spew ------------------------------------------------------------------------------------------------------------------------------------")
	
	INT i
	TEXT_LABEL_15 tl15 = GET_ENUM_NAME_MISSION_VARIATION_CONFIG(g_FMMC_STRUCT.sMissionVariation.eMCVarConfigEnum)	
	PRINTLN("[SCRIPT INIT][MissionVariation] - Using Enum Config: ",  TEXT_LABEL_TO_STRING(tl15))
		
	// ADD entity loops in here so we can proper loop through and print out the values that can be applied.
	
	// For searchibility.
	IF IS_USING_MISSION_VARIATION_ENEMY_HEALTH_OVERRIDE()
		PRINTLN("[SCRIPT INIT][MissionVariation] - (Enemy Health) Used for this Config Type.")
		PRINTLN("[SCRIPT INIT][MissionVariation] - (Enemy Health) GET_MISSION_VARIATION_ENEMY_HEALTH_OVERRIDE_INDEX:					", GET_MISSION_VARIATION_ENEMY_HEALTH_OVERRIDE_INDEX())
		PRINTLN("[SCRIPT INIT][MissionVariation] - (Enemy Health) GET_MISSION_VARIATION_ENEMY_HEALTH_OVERRIDE: 							", GET_MISSION_VARIATION_ENEMY_HEALTH_OVERRIDE())
	ENDIF
	IF IS_USING_MISSION_VARIATION_ENEMY_INVENTORY_OVERRIDE()
		PRINTLN("[SCRIPT INIT][MissionVariation] - (Enemy Inventory) Used for this Config Type.")
	ENDIF
	IF IS_THIS_MISSION_USING_MISSION_VARIATION_ENEMY_ALL_BLIP_OVERRIDE()
		PRINTLN("[SCRIPT INIT][MissionVariation] - (Enemy Blips Always On) Used for this Config Type.")
		PRINTLN("[SCRIPT INIT][MissionVariation] - (Enemy Blips Always On) ci_MCVarBsOne_BlipAllPlacedPeds: 							", IS_BIT_SET(g_FMMC_STRUCT.sMissionVariation.iVarBSOne, ci_MCVarBsOne_BlipAllPlacedPeds))
		PRINTLN("[SCRIPT INIT][MissionVariation] - (Enemy Blips Always On) ci_MCVarBsOne_BlipAllPlacedPedsConditional: 					", IS_BIT_SET(g_FMMC_STRUCT.sMissionVariation.iVarBSOne, ci_MCVarBsOne_BlipAllPlacedPedsConditional)) 
	ENDIF
	IF IS_USING_MISSION_VARIATION_OBJECTS_SCOPED_OUT()
		PRINTLN("[SCRIPT INIT][MissionVariation] - (Objects Scoped Out) Used for this Config Type.")
	ENDIF
	IF IS_USING_MISSION_VARIATION_CASINO_KEYPAD()
		PRINTLN("[SCRIPT INIT][MissionVariation] - (Casino Keypad) Used for this Config Type.")
	ENDIF	
	IF IS_USING_MISSION_VARIATION_VEHICLE_BLOCK_SPAWN_OVERRIDE()
		PRINTLN("[SCRIPT INIT][MissionVariation] - (Vehicle Spawn) Used for this Config Type.")
	ENDIF
	IF IS_USING_MISSION_VARIATION_VEHICLE_MODEL_SWAP_OVERRIDE()
		PRINTLN("[SCRIPT INIT][MissionVariation] - (Veh Model Swap) Used for this Config Type.")
	ENDIF
	IF IS_USING_MISSION_VARIATION_VEHICLE_SPAWN_FROM_ASSOCIATED_SELECTION_OVERRIDE()
		PRINTLN("[SCRIPT INIT][MissionVariation] - (Vehicle Spawn Associated) Used for this Config Type.")
	ENDIF
	IF IS_USING_MISSION_VARIATION_VEHICLE_REPOSITION_FROM_ASSOCIATED_SELECTION_OVERRIDE()
		PRINTLN("[SCRIPT INIT][MissionVariation] - (Vehicle Reposition) Used for this Config Type.")
		PRINTLN("[SCRIPT INIT][MissionVariation] - (Vehicle Reposition) GET_MISSION_VARIATION_REPOSITION_FROM_ASSOCIATED_SELECTION_INDEX:					 ", GET_MISSION_VARIATION_REPOSITION_FROM_ASSOCIATED_SELECTION_INDEX())
	ENDIF	
	IF IS_USING_MISSION_VARIATION_SPAWN_GROUP_FORCING_OVERRIDE()
		PRINTLN("[SCRIPT INIT][MissionVariation] - (Spawn Group Forcing) Used for this Config Type.")
		FOR i = 0 TO FMMC_MAX_MC_VAR_SIZE_SPAWN_GROUP_FORCING_OPTIONS-1
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Spawn Group Forcing ", i,") IS_THIS_MISSION_USING_MISSION_VARIATION_SPAWN_GROUP_FORCING_OVERRIDE: ", IS_THIS_MISSION_USING_MISSION_VARIATION_SPAWN_GROUP_FORCING_OVERRIDE(i))
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Spawn Group Forcing ", i,") GET_MISSION_VARIATION_SPAWN_GROUP_FORCING_SPAWN_GROUP_TO_FORCE_INDEX: ", GET_MISSION_VARIATION_SPAWN_GROUP_FORCING_SPAWN_GROUP_TO_FORCE_INDEX(i))
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Spawn Group Forcing ", i,") GET_MISSION_VARIATION_SPAWN_GROUP_FORCING_SPAWN_GROUP_TO_FORCE: ", GET_MISSION_VARIATION_SPAWN_GROUP_FORCING_SPAWN_GROUP_TO_FORCE(i))			
		ENDFOR
	ENDIF	
	IF IS_THIS_MISSION_VARIATION_USING_LOCATION_LINK_ONE_LARGE_BLIP_CHOSEN_ENTRANCE()
		PRINTLN("[SCRIPT INIT][MissionVariation] - (Location Link 1 - Large Blip Entrance) Used for this Config Type.")
		PRINTLN("[SCRIPT INIT][MissionVariation] - (Location Link 1 - Large Blip Entrance) IS_THIS_MISSION_USING_MISSION_VARIATION_LOCATION_LINK_ONE_OVERRIDE:  ", IS_THIS_MISSION_USING_MISSION_VARIATION_LOCATION_LINK_ONE_OVERRIDE())
		PRINTLN("[SCRIPT INIT][MissionVariation] - (Location Link 1 - Large Blip Entrance) GET_MISSION_VARIATION_LOCATION_LINK_INDEX: 							", GET_MISSION_VARIATION_LOCATION_LINK_INDEX(ciMCVar_SelectionIndex_SelectedEntrance))		
	ENDIF	
	IF IS_THIS_MISSION_VARIATION_USING_LOCATION_LINK_TWO_LARGE_BLIP_CHOSEN_EXIT()
		PRINTLN("[SCRIPT INIT][MissionVariation] - (Location Link 2 - Large Blip Exit) Used for this Config Type.")
		PRINTLN("[SCRIPT INIT][MissionVariation] - (Location Link 2 - Large Blip Exit) GET_MISSION_VARIATION_LOCATION_LINK_INDEX: 								", GET_MISSION_VARIATION_LOCATION_LINK_INDEX(ciMCVar_SelectionIndex_SelectedEXIT))
		PRINTLN("[SCRIPT INIT][MissionVariation] - (Location Link 2 - Large Blip Exit) IS_THIS_MISSION_USING_MISSION_VARIATION_LOCATION_LINK_TWO_OVERRIDE:  	", IS_THIS_MISSION_USING_MISSION_VARIATION_LOCATION_LINK_TWO_OVERRIDE())		
	ENDIF
	IF IS_USING_MISSION_VARIATION_LOCATION_PREREQUISITE()
		PRINTLN("[SCRIPT INIT][MissionVariation] - (Location Prerequisite) Used for this Config Type.")
		FOR i = 0 TO FMMC_MAX_GO_TO_LOCATIONS - 1
			IF IS_THIS_LOCATION_USING_MISSION_VARIATION_LOCATION_PREREQUISITE(i)
				PRINTLN("[SCRIPT INIT][MissionVariation] - Location ", i, " has a prerequisite set")
			ENDIF
		ENDFOR
	ENDIF
	IF IS_USING_MISSION_VARIATION_TIMER_OVERRIDING_OVERRIDE()
		PRINTLN("[SCRIPT INIT][MissionVariation] - (Override Timer) Used for this Config Type.")
		FOR i = 0 TO FMMC_MAX_MC_VAR_SIZE_TIMER_OVERRIDE-1
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Override Timer ", i, ") GET_MISSION_VARIATION_TIMER_OVERRIDE_VALUE:  ", GET_MISSION_VARIATION_TIMER_OVERRIDE_VALUE(i))
			PRINTLN("[SCRIPT INIT][MissionVariation] - (Override Timer ", i, ") GET_MISSION_VARIATION_OVERRIDE_TIMER_INDEX:  ", GET_MISSION_VARIATION_OVERRIDE_TIMER_INDEX(i))			
		ENDFOR
	ENDIF
	IF IS_THIS_MISSION_USING_MISSION_VARIATION_OBJECT_LINK_ONE_OVERRIDE()
		PRINTLN("[SCRIPT INIT][MissionVariation] - (Object Link 1) Used for this Config Type.")
		PRINTLN("[SCRIPT INIT][MissionVariation] - (Object Link 1) GET_MISSION_VARIATION_LOCATION_LINK_INDEX: ", GET_MISSION_VARIATION_LOCATION_LINK_INDEX(ciMCVar_SelectionIndex_SelectedEntrance))
	ENDIF
	IF IS_USING_MISSION_VARIATION_PTFX_USING_PREREQUISITE_MISSION_ELEMENT_OVERRIDE()
		PRINTLN("[SCRIPT INIT][MissionVariation] - (PTFX PreReq) Used for this Config Type.")
	ENDIF
	IF IS_USING_MISSION_VARIATION_PTFX_BLOCK_SPAWN_FROM_PREREQUISITE_1_OVERRIDE()
		PRINTLN("[SCRIPT INIT][MissionVariation] - (PTFX PreReq Spawn 1) Used for this Config Type.")
	ENDIF
	IF IS_USING_MISSION_VARIATION_PED_ASSOCIATED_SPAWN_SELECTION_1_OVERRIDE()
		PRINTLN("[SCRIPT INIT][MissionVariation] - (Ped Spawn Associated) Used for this Config Type.")
	ENDIF
	IF IS_USING_MISSION_VARIATION_PED_REPOSITION_FROM_ASSOCIATED_SELECTION_OVERRIDE()
		PRINTLN("[SCRIPT INIT][MissionVariation] - (Ped Reposition) Used for this Config Type.")
	ENDIF
	IF IS_USING_MISSION_VARIATION_END_CUTSCENE_OVERRIDE()
		INT iIndex = GET_MISSION_VARIATION_END_CTUSCENE_OVERRIDE_INDEX()
		PRINTLN("[SCRIPT INIT][MissionVariation] - (End Cutscene Override) Used for this Config Type.")
		PRINTLN("[SCRIPT INIT][MissionVariation] - (End Cutscene Override) IS_MISSION_VARIATION_END_CUTSCENE_OVERRIDE_VALID: ", IS_MISSION_VARIATION_END_CUTSCENE_OVERRIDE_VALID())
		PRINTLN("[SCRIPT INIT][MissionVariation] - (End Cutscene Override) GET_MISSION_VARIATION_END_CTUSCENE_OVERRIDE_INDEX: ", iIndex)
		IF iIndex > -1
			PRINTLN("[SCRIPT INIT][MissionVariation] - (End Cutscene Override) GET_MISSION_VARIATION_END_CTUSCENE_OVERRIDE_VALUE: ", GET_MISSION_VARIATION_END_CTUSCENE_OVERRIDE_VALUE(iIndex))
		ELSE
			PRINTLN("[SCRIPT INIT][MissionVariation] - (End Cutscene Override) GET_MISSION_VARIATION_END_CTUSCENE_OVERRIDE_VALUE: NOT SET!!!!")
		ENDIF
	ENDIF
	
	PRINTLN("[SCRIPT INIT][MissionVariation] - (Setup) Finished Spew ----------------------------------------------------------------------------------------------------------------------------------------")
#ENDIF
ENDPROC

PROC PROCESS_DEFINE_MISSION_VARIATION_SETTINGS_SERVER_DATA()
	
	PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) Starting Spew ----------------------------------------------------------------------------------------------------------------------------------")
	
	IF bIsLocalPlayerHost
		PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) I am the one who Hosts.")
	ENDIF
	
	#IF IS_DEBUG_BUILD
	TEXT_LABEL_15 tl15 = GET_ENUM_NAME_MISSION_VARIATION_CONFIG(g_FMMC_STRUCT.sMissionVariation.eMCVarConfigEnum)
	PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) Using Enum Config: ", tl15)
	#ENDIF
		
	SWITCH g_FMMC_STRUCT.sMissionVariation.eMCVarConfigEnum
		CASE FMMC_MC_VAR_CONFIG_ENUM_OFF
			
		BREAK
		CASE FMMC_MC_VAR_CONFIG_ENUM_CASINO_HEIST
			DEFINE_MISSION_VARIATION_SETTINGS_SERVER_DATA_CASINO_HEIST()
		BREAK
	ENDSWITCH
	
	INT i = 0
	FOR i = 0 TO FMMC_MAX_MC_VAR_CHOICE_BS-1
		PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) - (End Spew) iMissionVariationChoicesBS[",i,"]: ", MC_ServerBD_1.iMissionVariationChoicesBS[i])
	ENDFOR
	
	PRINTLN("[SCRIPT INIT][MissionVariation] - (Server-Define) Finished Spew ----------------------------------------------------------------------------------------------------------------------------------")
ENDPROC

PROC PROCESS_APPLY_MISSION_VARIATION_SETTINGS()
	
	PROCESS_APPLY_MISSION_VARIATION_SETTINGS_DEBUG()
	
	PRINTLN("[SCRIPT INIT][MissionVariation] - (Application) Starting Spew ----------------------------------------------------------------------------------------------------------------------------------")
	
	// Modify Globals - Execution order of these are deliberate. Do not change without talking with Luke Moffoot.
	PROCESS_APPLY_MISSION_VARIATION_SETTINGS_FOR_GLOBALS()
	
	PROCESS_APPLY_MISSION_VARIATION_SETTINGS_FOR_PLACED_PEDS()
	
	PROCESS_APPLY_MISSION_VARIATION_SETTINGS_FOR_PLACED_OBJECTS()
		
	PROCESS_APPLY_MISSION_VARIATION_SETTINGS_FOR_PLACED_VEHICLES()
	
	PROCESS_APPLY_MISSION_VARIATION_SETTINGS_FOR_PLACED_DYNOPROPS()
	
	PROCESS_APPLY_MISSION_VARIATION_SETTINGS_FOR_LOCATIONS()
	
	PROCESS_APPLY_MISSION_VARIATION_SETTINGS_FOR_PTFX()
	
	PROCESS_APPLY_MISSION_VARIATION_SETTINGS_FOR_DIALOGUE_TRIGGERS()
	
	PROCESS_APPLY_MISSION_VARIATION_SETTINGS_FOR_PLACED_ZONES()
	
	PROCESS_APPLY_MISSION_VARIATION_SETTINGS_FOR_PLACED_DUMMY_BLIPS()
	
	SET_UP_PLAYER_WEAPON_LOADOUT_VARIATION()
	
	//SET_UP_LOCAL_MISSION_VARIATION_VARS_FOR_SAFETY_BAIL()
	
	PRINTLN("[SCRIPT INIT][MissionVariation] - (Application) Finished Spew ----------------------------------------------------------------------------------------------------------------------------------")
ENDPROC
// [MissionVariation] init functions. ================================================================================= END















