
USING "FM_Mission_Controller_USING.sch"
USING "FM_Mission_Controller_Utility.sch"
USING "FM_Mission_Controller_Entities.sch"
USING "FM_Mission_Controller_ObjectiveCompletion.sch"
USING "FM_Mission_Controller_SpecialCase.sch"
USING "FM_Mission_Controller_Rules.sch"



//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//
//!!! NEW SECTION BELOW: DOORS !!!
//
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************

FUNC BOOL IS_CASINO_HEIST_MANTRAP_DOOR(INT iDoor)
	
	IF g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].mnDoorModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_tunnel_door_01_r"))
	OR g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].mnDoorModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_tunnel_door_01_l"))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: HACK DOORS !
//
//************************************************************************************************************************************************************



FLOAT fDoorEasing

FUNC BOOL GET_NEW_HACK_DOOR_HASH(INT iRule,MODEL_NAMES eModel,VECTOR vpos,INT& iDoorHash)

	IF DOOR_SYSTEM_FIND_EXISTING_DOOR(vpos,eModel,iDoorHash)
		PRINTLN("door already exists at coords: ",vpos," iRule: ", iRule)
		RETURN FALSE
	ELSE
		PRINTLN("door DOES NOT already exists at coords: ",vpos," iRule: ", iRule)
		SWITCH iRule
			CASE 0
				iDoorHash = HACK_DOOR_ID1
				RETURN TRUE
			BREAK
			CASE 1
				iDoorHash = HACK_DOOR_ID2
				RETURN TRUE
			BREAK
			CASE 2
				iDoorHash = HACK_DOOR_ID3
				RETURN TRUE
			BREAK
			CASE 3
				iDoorHash = HACK_DOOR_ID4
				RETURN TRUE
			BREAK
			CASE 4
				iDoorHash = HACK_DOOR_ID5
				RETURN TRUE
			BREAK
			CASE 5
				iDoorHash = HACK_DOOR_ID6
				RETURN TRUE
			BREAK
			CASE 6
				iDoorHash = HACK_DOOR_ID7
				RETURN TRUE
			BREAK
			CASE 7
				iDoorHash = HACK_DOOR_ID8
				RETURN TRUE
			BREAK
			CASE 8
				iDoorHash = HACK_DOOR_ID9
				RETURN TRUE
			BREAK
			CASE 9
				iDoorHash = HACK_DOOR_ID10
				RETURN TRUE
			BREAK
			CASE 10
				iDoorHash = HACK_DOOR_ID11
				RETURN TRUE
			BREAK
			CASE 11
				iDoorHash = HACK_DOOR_ID12
				RETURN TRUE
			BREAK
			CASE 12
				iDoorHash = HACK_DOOR_ID13
				RETURN TRUE
			BREAK
			CASE 13
				iDoorHash = HACK_DOOR_ID14
				RETURN TRUE
			BREAK
			CASE 14
				iDoorHash = HACK_DOOR_ID15
				RETURN TRUE
			BREAK
			CASE 15
				iDoorHash = HACK_DOOR_ID16
				RETURN TRUE
			BREAK
			CASE 16
				iDoorHash = HACK_DOOR_ID17
				RETURN TRUE
			BREAK
			CASE 17
				iDoorHash = HACK_DOOR_ID18
				RETURN TRUE
			BREAK
			
		ENDSWITCH
	
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL GET_NEW_SECOND_HACK_DOOR_HASH(INT iRule,MODEL_NAMES eModel,VECTOR vpos,INT& iDoorHash)

	IF DOOR_SYSTEM_FIND_EXISTING_DOOR(vpos,eModel,iDoorHash)
		PRINTLN("door 2 already exists at coords: ",vpos," iRule: ", iRule)
		RETURN FALSE
	ELSE
		PRINTLN("door 2 DOES NOT already exists at coords: ",vpos," iRule: ", iRule)
		SWITCH iRule
			CASE 0
				iDoorHash = HACK_DOOR2_ID1
				RETURN TRUE
			BREAK
			CASE 1
				iDoorHash = HACK_DOOR2_ID2
				RETURN TRUE
			BREAK
			CASE 2
				iDoorHash = HACK_DOOR2_ID3
				RETURN TRUE
			BREAK
			CASE 3
				iDoorHash = HACK_DOOR2_ID4
				RETURN TRUE
			BREAK
			CASE 4
				iDoorHash = HACK_DOOR2_ID5
				RETURN TRUE
			BREAK
			CASE 5
				iDoorHash = HACK_DOOR2_ID6
				RETURN TRUE
			BREAK
			CASE 6
				iDoorHash = HACK_DOOR2_ID7
				RETURN TRUE
			BREAK
			CASE 7
				iDoorHash = HACK_DOOR2_ID8
				RETURN TRUE
			BREAK
			CASE 8
				iDoorHash = HACK_DOOR2_ID9
				RETURN TRUE
			BREAK
			CASE 9
				iDoorHash = HACK_DOOR2_ID10
				RETURN TRUE
			BREAK
			CASE 10
				iDoorHash = HACK_DOOR2_ID11
				RETURN TRUE
			BREAK
			CASE 11
				iDoorHash = HACK_DOOR2_ID12
				RETURN TRUE
			BREAK
			CASE 12
				iDoorHash = HACK_DOOR2_ID13
				RETURN TRUE
			BREAK
			CASE 13
				iDoorHash = HACK_DOOR2_ID14
				RETURN TRUE
			BREAK
			CASE 14
				iDoorHash = HACK_DOOR2_ID15
				RETURN TRUE
			BREAK
			CASE 15
				iDoorHash = HACK_DOOR2_ID16
				RETURN TRUE
			BREAK
			CASE 16
				iDoorHash = HACK_DOOR2_ID17
				RETURN TRUE
			BREAK
			CASE 17
				iDoorHash = HACK_DOOR2_ID18
				RETURN TRUE
			BREAK
			
		ENDSWITCH
	
	ENDIF
	
	RETURN TRUE
	
ENDFUNC
#IF IS_DEBUG_BUILD
PROC DRAW_DOOR_COVER_BLOCKING_ZONE(INT iDoor)

	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iBitSet, ciDoorBS_CreateCoverBlockingAreaAroundDoor)
		EXIT
	ENDIF

	MODEL_NAMES mnDoor = g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].mnDoorModel
	VECTOR vPos = g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].vPos
	VECTOR vMax, vMin	
	
	IF IS_VECTOR_ZERO(vPos)
		PRINTLN("[LM][CoverBlockDoor] - DRAW_DOOR_COVER_BLOCKING_ZONE - iDoor: ", iDoor, " Cannot Create - Vector is Zero...")
		EXIT
	ENDIF
	
	IF NOT IS_MODEL_VALID(mnDoor)
		PRINTLN("[LM][CoverBlockDoor] - DRAW_DOOR_COVER_BLOCKING_ZONE - iDoor: ", iDoor, " Cannot Create - Invalid Model Name...")
		EXIT
	ENDIF
	
	GET_MODEL_DIMENSIONS(mnDoor, vMin, vMax)
	
	vMin *= 1.5
	vMax *= 1.5

	IF vMax.x > vMax.y
		vMax.y = vMax.x
	ELSE
		vMax.x = vMax.y
	ENDIF
	
	IF vMin.x > vMin.y
		vMin.x = vMin.y
	ELSE
		vMin.y = vMin.x
	ENDIF
	
	DRAW_MARKER(MARKER_BOXES, vPos, (<<0.0, 0.0, 0.0>>), (<<0.0, 0.0, 0.0>>), vMax, 200, 200, 200, 200)	
	
	PRINTLN("[LM][CoverBlockDoor] - DRAW_DOOR_COVER_BLOCKING_ZONE - iDoor: ", iDoor, " Added Cover Blocking Area Between Coords - vMin: ", vMin, " vMax: ", vMax)
	
ENDPROC
#ENDIF
PROC CREATE_DOOR_COVER_BLOCKING_ZONE(INT iDoor)

	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iBitSet, ciDoorBS_CreateCoverBlockingAreaAroundDoor)
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF bDoorCoverBlockZones
		DRAW_DOOR_COVER_BLOCKING_ZONE(iDoor)
	ENDIF
	#ENDIF
	
	IF IS_BIT_SET(iDoorCoverAreaBlocked, iDoor)
		EXIT
	ENDIF
	
	MODEL_NAMES mnDoor = g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].mnDoorModel
	VECTOR vPos = g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].vPos
	VECTOR vPos1, vPos2
	VECTOR vMax, vMin	
	
	IF IS_VECTOR_ZERO(vPos)
		PRINTLN("[LM][CoverBlockDoor] - CREATE_DOOR_COVER_BLOCKING_ZONE - iDoor: ", iDoor, " Cannot Create - Vector is Zero...")
		EXIT
	ENDIF
	
	IF NOT IS_MODEL_VALID(mnDoor)
		PRINTLN("[LM][CoverBlockDoor] - CREATE_DOOR_COVER_BLOCKING_ZONE - iDoor: ", iDoor, " Cannot Create - Invalid Model Name...")
		EXIT
	ENDIF
	
	GET_MODEL_DIMENSIONS(mnDoor, vMin, vMax)	
	
	vMin *= 2.0
	vMax *= 2.0

	IF vMax.x > vMax.y
		vMax.y = vMax.x
	ELSE
		vMax.x = vMax.y
	ENDIF
	
	IF vMin.x > vMin.y
		vMin.x = vMin.y
	ELSE
		vMin.y = vMin.x
	ENDIF

	vPos1 = vPos+vMin
	vPos2 = vPos+vMax
	
	vPos1.z -= 0.5
	vPos2.z += 0.5
	
	ADD_COVER_BLOCKING_AREA(vPos1, vPos2, FALSE, FALSE, TRUE, FALSE)
	SET_BIT(iDoorCoverAreaBlocked, iDoor)
	
	PRINTLN("[LM][CoverBlockDoor] - CREATE_DOOR_COVER_BLOCKING_ZONE - iDoor: ", iDoor, " Added Cover Blocking Area Between Coords - vPos1: ", vPos1, " vPos2: ", vPos2)
	
ENDPROC

/// PURPOSE: At the moment, only this one door plays 2 sounds at once
FUNC BOOL SHOULD_HACK_DOOR_PLAY_SECOND_SOUND( INT iDoorIndex )
	RETURN ciFMMC_DOOR_BIOLAB_SHUTTERS_RIGHT = iDoorIndex
ENDFUNC

/// PURPOSE: Get the Sound ID for sounds that loop and will need manual stopping
FUNC INT GET_FMMC_DOOR_SOUND_ID( INT iDoorIndex )

	INT iReturnInt
	
	SWITCH iDoorIndex
		//Looping sounds:
		CASE ciFMMC_DOOR_BIOLAB_SHUTTERS_RIGHT
		CASE ciFMMC_DOOR_CHEMICAL_GARAGE
			IF iGarageDoorSoundID = -1
				iGarageDoorSoundID = GET_SOUND_ID()
			ENDIF
			iReturnInt = iGarageDoorSoundID
		BREAK
		
		//One shot sounds:
		DEFAULT
			iReturnInt = -1
		BREAK
	
	ENDSWITCH
	
	RETURN iReturnInt

ENDFUNC

/// PURPOSE: Get the string for the sound we want to play on completion of 
///    certain objectives. This will correspond with some input from the creator
///    that corresponds
FUNC STRING GET_COMPLETION_SOUND_STRING( INT iDoorNum )
	
	STRING sReturnString = "null"
	
	SWITCH iDoorNum
		
		CASE ciFMMC_DOOR_BANK_VAULT
			RETURN "Vault"
		BREAK
		
		CASE ciFMMC_DOOR_BANK_LOBBY
		CASE ciFMMC_DOOR_BANK_1
		CASE ciFMMC_DOOR_ORNATE_BANK_VAULT
			sReturnString = "Blow_Gates" 
		BREAK
		
		CASE ciFMMC_DOOR_BIOLAB_SHUTTERS_RIGHT
			sReturnString = "Destroy_Electrical_Panel"
		BREAK
		
		CASE ciFMMC_DOOR_CHEMICAL_GARAGE
			sReturnString = "Garage_Door"
		BREAK
		
		DEFAULT
			PRINTLN("[RCC MISSION] GET_COMPLETION_SOUND_STRING - Trying to play a sound, but no sound set!")
		BREAK
		
	ENDSWITCH

	RETURN sReturnString

ENDFUNC

FUNC STRING GET_COMPLETION_SOUND_SET(INT iDoorNum)

	SWITCH iDoorNum
		
		CASE ciFMMC_DOOR_BANK_VAULT
			RETURN "HACKING_DOOR_UNLOCK_SOUNDS"
		BREAK
		
	ENDSWITCH
	
	RETURN "DLC_HEISTS_GENERIC_SOUNDS"

ENDFUNC

///PURPOSE: This plays specific sounds for hack doors 
PROC PROCESS_HACK_DOOR_SOUND( INT iDoorindex, VECTOR vSoundCoords )
	PRINTLN("[RCC MISSION] PROCESS_HACK_DOOR_SOUND ")
	
	STRING sObjSoundString 	= GET_COMPLETION_SOUND_STRING( iDoorindex )
	
	PRINTLN("[RCC MISSION] PROCESS_HACK_DOOR_SOUND: ",sObjSoundString)
	
	// If we get a valid soundname back, play a sound
	IF NOT ARE_STRINGS_EQUAL( sObjSoundString, "null" )
	AND NOT IS_BIT_SET( iSoundCompletedBitset, iDoorindex )
		
		IF REQUEST_SCRIPT_AUDIO_BANK("Vault_Door")
		
			PRINTLN("[RCC MISSION] PROCESS_HACK_DOOR_SOUND - Vault Door Script bank has loaded for player")
		
			INT iObjSoundID = GET_FMMC_DOOR_SOUND_ID( iDoorindex )
			
			PRINTLN("[RCC MISSION] VALID SOUNDNAME = ID: ",iObjSoundID)
			
			// In this one instance we want to play a second sound for the garage doors
			IF SHOULD_HACK_DOOR_PLAY_SECOND_SOUND( iDoorIndex )
				PLAY_SOUND_FROM_COORD( iObjSoundID, "Garage_Door", vSoundCoords + <<0,0,0.5>>,"DLC_HEISTS_GENERIC_SOUNDS", FALSE, 0, DEFAULT )
				iObjSoundID = -1 // Setting this back, because the other sound in this instance is non-looping
			ENDIF
			
			PLAY_SOUND_FROM_COORD( iObjSoundID, sObjSoundString, vSoundCoords,  GET_COMPLETION_SOUND_SET(iDoorIndex), FALSE, 0, DEFAULT )
			PRINTLN("[RCC MISSION] PLAYED SOUND AT ",vSoundCoords)
			
			SET_BIT( iSoundCompletedBitset, iDoorindex )
		ELSE
			PRINTLN("[RCC MISSION] PROCESS_HACK_DOOR_SOUND - Vault Door Script bank has *** NOT *** loaded for player")	
		ENDIF
	ENDIF
ENDPROC

PROC CLOSE_HACK_DOOR(INT iDoorHash)

	DOOR_SYSTEM_SET_OPEN_RATIO(iDoorHash,0,FALSE,FALSE)
	DOOR_SYSTEM_SET_DOOR_STATE(idoorHash,DOORSTATE_LOCKED,FALSE,FALSE)
	
ENDPROC

FUNC BOOL OPEN_HACK_DOOR( INT iRule, INT iDoor, BOOL bBlown, BOOL bSwingFree, FLOAT& fOpenRatio, BOOL bApplyThermiteDamage )

	//door hashes are registered inside INIT_HACK_DOORS via GET_NEW_HACK_DOOR_HASH() GET_NEW_SECOND_HACK_DOOR_HASH()
	//DOOR_SYSTEM_FIND_EXISTING_DOOR will obtain the hash for an added door  
	
	INT iHackDoorHash
	GET_NEW_HACK_DOOR_HASH( iRule, GET_FMMC_DOOR_MODEL( iDoor, IS_BIT_SET(iLocalHackDoorThermiteBitset, iRule) ), GET_FMMC_DOOR_COORDS( iDoor ), iHackDoorHash )
	
	IF NOT bSwingFree
		PROCESS_HACK_DOOR_SOUND( iDoor, GET_FMMC_DOOR_COORDS( iDoor ) )
	ENDIF

	//If the door is being blown up using thermite then this will swap the door model out for a damaged version.
	//This occurs before the door unlocking, so don't allow the unlocking portion to run if we're waiting for the door swap to complete.
	IF bApplyThermiteDamage
	AND NOT IS_BIT_SET(iLocalHackDoorThermiteBitset, iRule)
	AND NOT IS_BIT_SET(iLocalHackDoorThermiteFailedBitset, iRule)
		MODEL_NAMES modelToSwapTo = GET_FMMC_DOOR_MODEL(iDoor, TRUE)
	
		IF modelToSwapTo != DUMMY_MODEL_FOR_SCRIPT
		AND IS_MODEL_VALID(modelToSwapTo)
			REQUEST_MODEL(modelToSwapTo)
			
			IF HAS_MODEL_LOADED(modelToSwapTo)
				PRINTLN("[RCC MISSION] [OPEN_HACK_DOOR] swapping door for damaged version. Rule: ", iRule, " on door: ", iDoor, " HASH VALUE: ",iHackDoorHash )
			
				CREATE_MODEL_SWAP(GET_FMMC_DOOR_COORDS(iDoor), 3.0, GET_FMMC_DOOR_MODEL(iDoor), modelToSwapTo, TRUE)
				SET_BIT(iLocalHackDoorThermiteBitset, iRule)
				SET_MODEL_AS_NO_LONGER_NEEDED(modelToSwapTo)
			ENDIF
		ELSE
			PRINTLN("[RCC MISSION] [OPEN_HACK_DOOR] Valid damaged door model not found for swap. Rule: ", iRule, " on door: ", iDoor, " HASH VALUE: ",iHackDoorHash )
		
			SET_BIT(iLocalHackDoorThermiteFailedBitset, iRule)
		ENDIF
	ELSE
	
		SWITCH iDoor
			
			CASE ciFMMC_DOOR_CHEMICAL_GARAGE
			
				IF bSwingFree
					SET_AMBIENT_ZONE_STATE("AZ_DLC_HEIST_BIOLAB_GARAGE",TRUE,TRUE)
					SET_SCRIPT_UPDATE_DOOR_AUDIO(iHackDoorHash,TRUE)
					DOOR_SYSTEM_SET_OPEN_RATIO(iHackDoorHash,0.97,FALSE,FALSE)
					DOOR_SYSTEM_SET_DOOR_STATE(iHackDoorHash,DOORSTATE_UNLOCKED,FALSE,TRUE)
					RETURN TRUE
				ENDIF
				
				IF fOpenRatio >= 0.98
					
					// This stops the garage door sound when it's playing.
					STOP_SOUND( GET_FMMC_DOOR_SOUND_ID( iDoor ) )
				
					RETURN TRUE
				ELSE
					SET_SCRIPT_UPDATE_DOOR_AUDIO( iHackDoorHash, TRUE )
					DOOR_SYSTEM_SET_OPEN_RATIO( iHackDoorHash, fOpenRatio, FALSE, FALSE )
					DOOR_SYSTEM_SET_DOOR_STATE( iHackDoorHash, DOORSTATE_FORCE_LOCKED_THIS_FRAME, FALSE, TRUE )
					IF bBlown
						fOpenRatio= fOpenRatio + 0.1
					ELSE
						fOpenRatio= fOpenRatio + 0.01
					ENDIF
				ENDIF
				
			BREAK
			
			CASE ciFMMC_DOOR_VINEWOOD_GARAGE
			CASE ciFMMC_DOOR_GOV_FACILITY_GATE
			CASE ciFMMC_DOOR_AIRPORT_GATE_1
			CASE ciFMMC_DOOR_PRISON_MANSION_GATE_LEFT
			CASE ciFMMC_DOOR_PRISON_MANSION_GATE_RIGHT
				
				IF bSwingFree
					SET_SCRIPT_UPDATE_DOOR_AUDIO(iHackDoorHash,TRUE)
					DOOR_SYSTEM_SET_OPEN_RATIO(iHackDoorHash,fOpenRatio,FALSE,FALSE)
					DOOR_SYSTEM_SET_DOOR_STATE(iHackDoorHash,DOORSTATE_UNLOCKED,FALSE,TRUE)
					RETURN TRUE
				ENDIF
				
				IF fOpenRatio >= 0.98
					
					// This stops the garage door sound when it's playing.
					STOP_SOUND( GET_FMMC_DOOR_SOUND_ID( iDoor ) )
				
					RETURN TRUE
				ELSE
					SET_SCRIPT_UPDATE_DOOR_AUDIO( iHackDoorHash, TRUE )
					DOOR_SYSTEM_SET_OPEN_RATIO( iHackDoorHash, fOpenRatio, FALSE, FALSE )
					DOOR_SYSTEM_SET_DOOR_STATE( iHackDoorHash, DOORSTATE_FORCE_LOCKED_THIS_FRAME, FALSE, TRUE )
					IF bBlown
						fOpenRatio= fOpenRatio + 0.1
					ELSE
						fOpenRatio= fOpenRatio + 0.01
					ENDIF
				ENDIF
				
			BREAK
			
			CASE ciFMMC_DOOR_BIOLAB_SHUTTERS_LEFT
			CASE ciFMMC_DOOR_BIOLAB_SHUTTERS_RIGHT
			CASE ciFMMC_DOOR_PRISON_GATE
			
				PRINTLN("[RCC MISSION] calling open door: ",iDoor)
				
				IF bSwingFree
					DOOR_SYSTEM_SET_OPEN_RATIO(iHackDoorHash,fOpenRatio,FALSE,FALSE)
					DOOR_SYSTEM_SET_DOOR_STATE(iHackDoorHash,DOORSTATE_UNLOCKED,FALSE,TRUE)
					RETURN TRUE
				ENDIF
				
				IF fOpenRatio >= 0.98
					
					// This stops the garage door sound when it's playing.
					STOP_SOUND( GET_FMMC_DOOR_SOUND_ID( iDoor ) )
					
					RETURN TRUE
				ELSE
					DOOR_SYSTEM_SET_OPEN_RATIO(iHackDoorHash,fOpenRatio,FALSE,FALSE)
					DOOR_SYSTEM_SET_DOOR_STATE(iHackDoorHash,DOORSTATE_FORCE_LOCKED_THIS_FRAME ,FALSE,TRUE)
					fOpenRatio= fOpenRatio + 0.01
				ENDIF
				
			BREAK
			
			CASE ciFMMC_DOOR_BANK_1
			CASE ciFMMC_DOOR_DOCK_CONTROL_2
			CASE ciFMMC_DOOR_BANK_LOBBY
			CASE ciFMMC_DOOR_FLECCA_RFHILLS2
			CASE ciFMMC_DOOR_FLECCA_PERSHING2
			CASE ciFMMC_DOOR_FLECCA_CHUMASH2
			CASE ciFMMC_DOOR_BIOLAB_LAB1_DOOR_RIGHT
			
				PRINTLN("[RCC MISSION] BANK VAULT DOOR - TEST 0")
		
				IF bSwingFree
					
					IF NOT IS_BIT_SET(iLocalHackDoorThermiteBitset, iRule)
					
						DOOR_SYSTEM_SET_OPEN_RATIO(iHackDoorHash,fOpenRatio,FALSE,FALSE)
						DOOR_SYSTEM_SET_DOOR_STATE(iHackDoorHash,DOORSTATE_UNLOCKED,FALSE,TRUE)
						RETURN TRUE
						
					ELSE

						IF NOT IS_BIT_SET(iLocal_unlock_thermite_door_bitset, iRule)
						
							DOOR_SYSTEM_SET_OPEN_RATIO(iHackDoorHash,fOpenRatio,FALSE,FALSE)
							DOOR_SYSTEM_SET_DOOR_STATE(iHackDoorHash,DOORSTATE_UNLOCKED,FALSE,TRUE)
							
							SET_BIT(iLocal_unlock_thermite_door_bitset, iRule)
						
						ENDIF

					ENDIF 
				ENDIF
				
				IF NOT IS_BIT_SET(iLocalHackDoorThermiteBitset, iRule)
				
					PRINTLN("[RCC MISSION] BANK VAULT DOOR - TEST 1")

					IF fOpenRatio >= 0.98		
						RETURN TRUE
					ELSE
						
						DOOR_SYSTEM_SET_OPEN_RATIO(iHackDoorHash,fOpenRatio,FALSE,FALSE)
						DOOR_SYSTEM_SET_DOOR_STATE(iHackDoorHash,DOORSTATE_FORCE_LOCKED_THIS_FRAME ,FALSE,TRUE)
						IF bBlown
							fOpenRatio= fOpenRatio + 0.1
						ELSE
							fOpenRatio= fOpenRatio + 0.02
						ENDIF
					ENDIF
					
				ELSE
				
					PRINTLN("[RCC MISSION] BANK VAULT DOOR - TEST 2")
				
					IF NOT IS_BIT_SET(iLocal_force_open_hack_door_thermite_bitset, iRule)
						IF DOOR_SYSTEM_GET_OPEN_RATIO(iHackDoorHash) > 0.9
							SET_BIT(iLocal_force_open_hack_door_thermite_bitset, iRule)
						ENDIF
					ENDIF 

					IF IS_BIT_SET(iLocal_force_open_hack_door_thermite_bitset, iRule)
					
						PRINTLN("[RCC MISSION] BANK VAULT DOOR - TEST 3")
					
						IF fOpenRatio >= 0.98		
							RETURN TRUE
						ELSE

							IF bBlown
								fOpenRatio= DOOR_SYSTEM_GET_OPEN_RATIO(iHackDoorHash) + 0.05
								PRINTLN("[RCC MISSION] BANK VAULT DOOR BLOWN")
							ELSE
								fOpenRatio= DOOR_SYSTEM_GET_OPEN_RATIO(iHackDoorHash) + 0.02
								PRINTLN("[RCC MISSION] BANK VAULT DOOR NOT BLOWN")
							ENDIF
							
							DOOR_SYSTEM_SET_OPEN_RATIO(iHackDoorHash,fOpenRatio,FALSE,FALSE)
							DOOR_SYSTEM_SET_DOOR_STATE(iHackDoorHash,DOORSTATE_FORCE_LOCKED_THIS_FRAME ,FALSE,TRUE)
							
						ENDIF
						
					ENDIF 
				
				ENDIF 
				
			BREAK

			CASE ciFMMC_DOOR_BANK_VAULT
				
				PRINTLN("[RCC MISSION] BANK VAULT DOOR ")
				
				IF bSwingFree
	
					PRINTLN("[RCC MISSION] BANK VAULT DOOR - SWING FREE")
					DOOR_SYSTEM_SET_OPEN_RATIO(iHackDoorHash,fOpenRatio,FALSE,FALSE)
					DOOR_SYSTEM_SET_DOOR_STATE(iHackDoorHash,DOORSTATE_UNLOCKED,FALSE,TRUE)
					
					RETURN TRUE
				
				ENDIF

				IF fopenratio <= -0.8
					
					door_system_set_door_state(ihackdoorhash, doorstate_force_locked_this_frame, false, true)
					
					PRINTLN("[rcc mission] bank vault door - finished opening")
				
					return true
				
				ELSE
			
					IF bblown
						PRINTLN("[rcc mission] bank vault door - opening (blown)")
						fopenratio= fopenratio - 0.1
					ELSE
						PRINTLN("[rcc mission] bank vault door - opening (regular)")
						fopenratio= fopenratio - 0.005
					ENDIF
					
					door_system_set_open_ratio(ihackdoorhash, fopenratio, false, false)
					door_system_set_door_state(ihackdoorhash, doorstate_force_locked_this_frame, false, true)
					
					PRINTLN("[rcc mission] bank vault door - opening")
					
				ENDIF

				
			BREAK
			
			CASE ciFMMC_DOOR_ORNATE_BANK_VAULT
			
				PRINTLN("[RCC MISSION] ORNATE BANK VAULT DOOR - OPENING")

				IF bSwingFree
					DOOR_SYSTEM_SET_OPEN_RATIO(iHackDoorHash,fOpenRatio,FALSE,FALSE)
					DOOR_SYSTEM_SET_DOOR_STATE(iHackDoorHash,DOORSTATE_UNLOCKED,FALSE,TRUE)
					PRINTLN("[RCC MISSION] ORNATE BANK VAULT DOOR - SWING FREE")
					RETURN TRUE
				ENDIF

				IF fOpenRatio <= -0.98
					
					PRINTLN("[RCC MISSION] ORNATE BANK VAULT DOOR - IT'S OPEN")
					
					DOOR_SYSTEM_SET_DOOR_STATE(iHackDoorHash, DOORSTATE_FORCE_LOCKED_THIS_FRAME, FALSE, TRUE)
				
					RETURN TRUE
				
				ELSE
					
					IF fOpenRatio = 0
						fDoorEasing = 0
					ENDIF
					PRINTLN("[RCC MISSION] ORNATE BANK VAULT DOOR - OPENING DOOR NORMALLY")
					
					DOOR_SYSTEM_SET_OPEN_RATIO(iHackDoorHash, fOpenRatio, FALSE, FALSE)
					DOOR_SYSTEM_SET_DOOR_STATE(iHackDoorHash, DOORSTATE_FORCE_LOCKED_THIS_FRAME, FALSE, TRUE) //commented out for bug 2227094
					
					IF bBlown
						fOpenRatio= fOpenRatio - 0.1
					ELSE
						IF fOpenRatio < -0.25
							fDoorEasing = LERP_FLOAT(0,1,0.01)
							fOpenRatio= fOpenRatio - 0.02 * fDoorEasing
						ELSE
							IF fOpenRatio < -0.75
								fOpenRatio= fOpenRatio - 0.02
							ELSE
								fDoorEasing = LERP_FLOAT(1,0.01,0.01)
								fOpenRatio= fOpenRatio - 0.02 * fDoorEasing
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK

			CASE ciFMMC_DOOR_DOCK_CONTROL
			CASE ciFMMC_DOOR_FLECCA_RFHILLSEXT
			CASE ciFMMC_DOOR_FLECCA_PERSHINGEXT
			CASE ciFMMC_DOOR_FLECCA_CHUMASHEXT
			CASE ciFMMC_DOOR_POLICE_STATION_SECURITY
			CASE ciFMMC_DOOR_POLICE_LOCKER_ROOM
			CASE ciFMMC_DOOR_POLICE_LOCKER_ROOM_2
			CASE ciFMMC_DOOR_BIOLAB_LAB1_DOOR_LEFT
			
				IF NOT IS_BIT_SET(iLocalHackDoorThermiteBitset, iRule)
				
					IF bSwingFree
						DOOR_SYSTEM_SET_OPEN_RATIO(iHackDoorHash,fOpenRatio,FALSE,FALSE)
						DOOR_SYSTEM_SET_DOOR_STATE(iHackDoorHash,DOORSTATE_UNLOCKED,FALSE,TRUE)
						RETURN TRUE
					ENDIF
					
				ELSE 
				
					IF NOT IS_BIT_SET(iLocal_unlock_thermite_door_bitset, iRule)
						
						DOOR_SYSTEM_SET_OPEN_RATIO(iHackDoorHash,fOpenRatio,FALSE,FALSE)
						DOOR_SYSTEM_SET_DOOR_STATE(iHackDoorHash,DOORSTATE_UNLOCKED,FALSE,TRUE)
						
						SET_BIT(iLocal_unlock_thermite_door_bitset, iRule)
					
					ENDIF
				
				ENDIF 
				
				IF NOT IS_BIT_SET(iLocalHackDoorThermiteBitset, iRule) 
				
					IF fOpenRatio <= -0.98
						RETURN TRUE
					ELSE
						DOOR_SYSTEM_SET_OPEN_RATIO(iHackDoorHash,fOpenRatio,FALSE,FALSE)
						DOOR_SYSTEM_SET_DOOR_STATE(iHackDoorHash,DOORSTATE_FORCE_LOCKED_THIS_FRAME ,FALSE,TRUE)
						IF bBlown
							fOpenRatio= fOpenRatio - 0.1
						ELSE
							fOpenRatio= fOpenRatio - 0.02
						ENDIF
					ENDIF
					
				ELSE 
				
					IF NOT IS_BIT_SET(iLocal_force_open_hack_door_thermite_bitset, iRule)
						IF DOOR_SYSTEM_GET_OPEN_RATIO(iHackDoorHash) < -0.90
							SET_BIT(iLocal_force_open_hack_door_thermite_bitset, iRule)
						ENDIF
					ENDIF 
					
					IF IS_BIT_SET(iLocal_force_open_hack_door_thermite_bitset, iRule)
					
						IF fOpenRatio <= -0.98
							RETURN TRUE
						ELSE
					
							IF bBlown
								fOpenRatio= DOOR_SYSTEM_GET_OPEN_RATIO(iHackDoorHash) - 0.05
							ELSE
								fOpenRatio= DOOR_SYSTEM_GET_OPEN_RATIO(iHackDoorHash) - 0.02
							ENDIF
							
							DOOR_SYSTEM_SET_OPEN_RATIO(iHackDoorHash,fOpenRatio,FALSE,FALSE)
							DOOR_SYSTEM_SET_DOOR_STATE(iHackDoorHash,DOORSTATE_FORCE_LOCKED_THIS_FRAME ,FALSE,TRUE)
						ENDIF
						
					ENDIF 
				
				ENDIF 
				
			BREAK
			
			CASE ciFMMC_DOOR_PLAYBOY
				
				IF bSwingFree
					DOOR_SYSTEM_SET_OPEN_RATIO(iHackDoorHash,fOpenRatio,FALSE,FALSE)
					DOOR_SYSTEM_SET_DOOR_STATE(iHackDoorHash,DOORSTATE_UNLOCKED,FALSE,TRUE)
					RETURN TRUE
				ENDIF
				
				IF fOpenRatio <= -0.98
					RETURN TRUE
				ELSE
					DOOR_SYSTEM_SET_OPEN_RATIO(iHackDoorHash,fOpenRatio,FALSE,FALSE)
					DOOR_SYSTEM_SET_DOOR_STATE(iHackDoorHash,DOORSTATE_FORCE_LOCKED_THIS_FRAME ,FALSE,TRUE)
					IF bBlown
						fOpenRatio = fOpenRatio - 0.1
					ELSE
						fOpenRatio = fOpenRatio - 0.01
					ENDIF
				ENDIF
				
			BREAK
			
		ENDSWITCH
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC INIT_HACK_DOORS(INT iteam, INT iRule)

INT iDoorHash
	
	IF g_FMMC_STRUCT.sFMMCEndConditions[iteam].iLinkedDoor[iRule] !=-1
		IF iHackDoorID[iRule] = -1
			
			iHackDoorID[iRule] = g_FMMC_STRUCT.sFMMCEndConditions[iteam].iLinkedDoor[iRule]
			
			// Get IF we only want to unlock the door, NOT let it swing free.
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iLinkedDoorSwingFree1, iRule)
				SET_BIT(iDoor1Swingfree, iRule)
			ENDIF
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iLinkedDoorSwingFree2, iRule)
				SET_BIT(iDoor2Swingfree, iRule)
			ENDIF
			
			IF GET_NEW_HACK_DOOR_HASH(iRule,GET_FMMC_DOOR_MODEL(iHackDoorID[iRule]),GET_FMMC_DOOR_COORDS(iHackDoorID[iRule]),iDoorHash)
				ADD_DOOR_TO_SYSTEM(iDoorHash,GET_FMMC_DOOR_MODEL(iHackDoorID[iRule]),GET_FMMC_DOOR_COORDS(iHackDoorID[iRule]),FALSE,FALSE)
				PRINTLN("[RCC MISSION] ADDING HACK DOOR: ",iHackDoorID[iRule]," for RULE: ",iRule," HASH VALUE ", iDoorHash)
			ELSE
				PRINTLN("[RCC MISSION] HACK DOOR EXISTS ALREADY: ",iHackDoorID[iRule]," for RULE: ",iRule," HASH VALUE ", iDoorHash)
			ENDIF
			CLOSE_HACK_DOOR(iDoorHash)
			
			IF g_FMMC_STRUCT.sFMMCEndConditions[iteam].iLinkedDoor2[iRule] !=-1
				iHackDoorID2[iRule] = g_FMMC_STRUCT.sFMMCEndConditions[iteam].iLinkedDoor2[iRule]
			ENDIF
			IF iHackDoorID2[iRule] != -1
				IF GET_NEW_SECOND_HACK_DOOR_HASH(iRule,GET_FMMC_DOOR_MODEL(iHackDoorID2[iRule]),GET_FMMC_DOOR_COORDS(iHackDoorID2[iRule]),iDoorHash)
					IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(iDoorHash)
						ADD_DOOR_TO_SYSTEM(iDoorHash,GET_FMMC_DOOR_MODEL(iHackDoorID2[iRule]),GET_FMMC_DOOR_COORDS(iHackDoorID2[iRule]),FALSE,FALSE)
						PRINTLN("[RCC MISSION] ADDING HACK DOOR 2: ",iHackDoorID2[iRule]," for RULE: ",iRule," HASH VALUE ", iDoorHash)
					ENDIF
				ELSE
					PRINTLN("[RCC MISSION] HACK DOOR 2 EXISTS ALREADY: ",iHackDoorID2[iRule]," for RULE: ",iRule," HASH VALUE ", iDoorHash)
				ENDIF
				CLOSE_HACK_DOOR(iDoorHash)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: MAP/OBJECT DOORS !
//
//************************************************************************************************************************************************************

FUNC BOOL IS_VAULT_DOOR_OPEN(INT iRule,INT iDoor)

	PRINTLN("[RCC MISSION] IS_VAULT_DOOR_OPEN vault door object for rule: ", iRule, " on door: ",iDoor," fopen ratio: ",fHackDoorOpenRatio[iRule]," HASH VALUE: " )
	IF iDoor =1
	ENDIF
	IF fHackDoorOpenRatio[iRule] > 65.0
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
					
ENDFUNC

//clamps heading in the range of 0 - 360
proc clamp_heading(float &current_heading)
	
	IF current_heading > 360.00
		current_heading -= 360.00
	ENDIF
	
	IF current_heading < 0.0
		current_heading += 360.00
	ENDIF 
	
endproc 

///Purpose. Rotates the door towards the heading addition.  
///    		e.g. door heading = 100 door_target_heading_addition = 90.  
///    		target heading = current door heading + door_target_heading_addition. 
///    		target heading = 190.00
///    		function interpolates global towards the door_target_heading_addition. 
func bool SET_OBJECT_DOOR_RATIO(object_index tempObj, INT iDoor, float door_target_heading_addition)
	
	door_target_heading_addition *= 65.00 //door_target_heading_addition will be either 1.0 or -1.0 when passed in
	
	float door_rotation_speed = 30.00 
	FLOAT door_heading
	
	#IF IS_DEBUG_BUILD
	INT iTemp
	#ENDIF
	
	IF does_entity_exist(tempObj)
		
		PRINTLN("[RCC MISSION] LK - door_target_heading_addition = ", door_target_heading_addition)
		
		IF fHackDoorStartHeading[iDoor] = 0
			fHackDoorStartHeading[iDoor] = GET_ENTITY_HEADING(tempObj)
			PRINTLN("[RCC MISSION] - SET_OBJECT_DOOR_RATIO is NOT init so grabbing fHackDoorStartHeading[[", iDoor, "] = ", fHackDoorStartHeading[iDoor])
			BROADCAST_FMMC_OPEN_DOOR_EVENT(iMapObjectDoorCount,fHackDoorStartHeading[iDoor])
		ENDIF
		
		#IF IS_DEBUG_BUILD
			iTemp = NATIVE_TO_INT(tempObj)
			PRINTLN("[RCC MISSION] - SET_OBJECT_DOOR_RATIO Vault_door[", iDoor, "] = ", iTemp)
			PRINTLN("[RCC MISSION] - SET_OBJECT_DOOR_RATIO fHackDoorStartHeading[[", iDoor, "] = ", fHackDoorStartHeading[iDoor])
		#ENDIF
		
		//figure out what direction to increment in
		IF door_target_heading_addition >= 0.0
			current_door_heading_addition[iDoor] += (fLastFrameTime * door_rotation_speed)
			
			IF current_door_heading_addition[iDoor] > door_target_heading_addition
				current_door_heading_addition[iDoor] = door_target_heading_addition
			ENDIF
		ELSE
			current_door_heading_addition[iDoor] -= (fLastFrameTime * door_rotation_speed)
			
			IF current_door_heading_addition[iDoor] < door_target_heading_addition
				current_door_heading_addition[iDoor] = door_target_heading_addition
			ENDIF
		ENDIF
		
		door_heading = fHackDoorStartHeading[iDoor] + current_door_heading_addition[iDoor]
		clamp_heading(door_heading)
		set_entity_heading(tempObj, door_heading)
		
		PRINTLN("[RCC MISSION] - SET_OBJECT_DOOR_RATIO (fHackDoorStartHeading[[", iDoor, "] + current_door_heading_addition[", iDoor, "]) = ", (fHackDoorStartHeading[iDoor] + current_door_heading_addition[iDoor]))
		
		IF current_door_heading_addition[iDoor] = door_target_heading_addition
			PRINTLN("[RCC MISSION] - SET_OBJECT_DOOR_RATIO (fHackDoorStartHeading[[", iDoor, "] + door_target_heading_addition) = ", (fHackDoorStartHeading[iDoor] + door_target_heading_addition))
			return true
		ENDIF
		
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE: This function takes in the inputted door ID and sets the inputted fOpenRatio
///    on the door it finds on the aircraft carrier.
PROC SET_FAKE_DOOR_RATIO(object_index tempObj, FLOAT fOpenRatio , INT iDoor)
	
	IF DOES_ENTITY_EXIST( tempObj )
		PRINTLN( "[RCC MISSION] - SET_FAKE_DOOR_RATIO - Setting up door num ", iDoor)
		PRINTLN( "[RCC MISSION] - SET_FAKE_DOOR_RATIO - (passed in) fOpenRatio: " , fOpenRatio)
		PRINTLN( "[RCC MISSION] - SET_FAKE_DOOR_RATIO - (creator setting) fOpenRatio: " , g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].sDoorConfigs[ciDOOR_CONFIG__STANDARD].fOpenRatio)
		PRINTLN( "[RCC MISSION] - SET_FAKE_DOOR_RATIO - (creator setting) fUpdate: " , g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].sDoorConfigs[ciDOOR_CONFIG__ALT].fOpenRatio)
		
		#IF IS_DEBUG_BUILD
			VECTOR vDoorRotation = GET_ENTITY_ROTATION(tempObj)
			PRINTLN( "[RCC MISSION] - SET_FAKE_DOOR_RATIO - old z Rotation:" , vDoorRotation.z)			
		#ENDIF
		
		SET_ENTITY_ROTATION( tempObj, GET_CREATOR_FAKE_DOOR_ROTATION_FROM_OPEN_RATIO(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor], fOpenRatio))
		
		#IF IS_DEBUG_BUILD
			vDoorRotation = GET_ENTITY_ROTATION(tempObj)
			PRINTLN( "[RCC MISSION] - SET_FAKE_DOOR_RATIO - new z Rotation:" , vDoorRotation.z)		
		#ENDIF
		
	#IF IS_DEBUG_BUILD
	ELSE
		PRINTLN( "[RCC MISSION] - SET_FAKE_DOOR_RATIO DOES NOT EXIST - door num " , iDoor)
	#ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE: returns true if the door is a vault door. Currently the 3 doors checked within the function are 
///    		 flecca bank doors.
FUNC BOOL IS_DOOR_VAULT_OBJECT_DOOR(INT iDoor)
	
	IF g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].mnDoorModel = GET_FMMC_DOOR_MODEL(ciFMMC_DOOR_FLECCA_RFHILLS1)
	OR g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].mnDoorModel = GET_FMMC_DOOR_MODEL(ciFMMC_DOOR_FLECCA_PERSHING1)
	OR g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].mnDoorModel = GET_FMMC_DOOR_MODEL(ciFMMC_DOOR_FLECCA_CHUMASH1)//flecca mission doors
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

func bool IS_DOOR_A_FLECCA_BANK_DOOR(int idoor)

	IF g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].mnDoorModel = get_fmmc_door_model(cifmmc_door_flecca_chumash1)
		return true
	ENDIF
	
	return false

endfunc

proc PLAY_SOUND_FROM_VAULT_DOOR(int idoor)

	//all these doors are models v_ilev_gb_vauldr. Play the sound off them when they open.  
	IF IS_DOOR_VAULT_OBJECT_DOOR(idoor)
		IF REQUEST_SCRIPT_AUDIO_BANK("vault_door")
			PLAY_SOUND_FROM_COORD(-1, "vault_unlock", g_FMMC_STRUCT_ENTITIES.sSelectedDoor[idoor].vpos, "dlc_heist_fleeca_bank_door_sounds",FALSE)
			SET_BIT(ilocalboolcheck8, lbool8_vault_door_sound)
			PRINTLN("[lk mission] play sound vault_unlock for door ", idoor)
		ENDIF
	ENDIF

endproc

///Purpose: returns TRUE IF the door time has exceeded Or the door time is 0. The doors time is 0 by default so 
FUNC BOOL HAS_DOOR_TIME_EXCEEDED(INT iDoor)
	IF g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iTime != 0
		IF NOT HAS_NET_TIMER_STARTED(door_time[iDoor])
			START_NET_TIMER(door_time[iDoor])
		ELSE
			IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(door_time[iDoor]) >= g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iTime
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

///purpose: checks specIFic door functionality conditions e.g. 
///    
///    			- ciDoorBS_LEGACY_only_update_door_on_checkpoint_1
///    			- ciDoorBS_LEGACY_only_update_door_on_checkpoint_2
///    			
///    	IF they have been met on the specIFic door the function will return true otherwise false. 
///    		
///     In all other cases it will return true.
func bool DOOR_FUNCTIONALITY_CHECKS(int iDoor)

	IF IS_THIS_A_QUICK_RESTART_JOB()
	OR IS_CORONA_INITIALISING_A_QUICK_RESTART()
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iBitSet, ciDoorBS_LEGACY_OnlyUpdateOnCheckpoint_4)
			IF IS_MISSION_START_CHECKPOINT_SET(4)
				PRINTLN("[RCC MISSION] ALLOW_DOOR_TO_UPDATE - Checkpoint 4 has passed, return TRUE")
				RETURN TRUE
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iBitSet, ciDoorBS_LEGACY_OnlyUpdateOnCheckpoint_3)
			IF IS_MISSION_START_CHECKPOINT_SET(3)
				PRINTLN("[RCC MISSION] ALLOW_DOOR_TO_UPDATE - Checkpoint 3 has passed, return TRUE")
				RETURN TRUE
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iBitSet, ciDoorBS_LEGACY_only_update_door_on_checkpoint_2)
			IF IS_MISSION_START_CHECKPOINT_SET(2)
				PRINTLN("[RCC MISSION] ALLOW_DOOR_TO_UPDATE - Checkpoint 2 has passed, return TRUE")
				return true
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iBitSet, ciDoorBS_LEGACY_only_update_door_on_checkpoint_1)
			IF IS_MISSION_START_CHECKPOINT_SET(1)
				PRINTLN("[RCC MISSION] ALLOW_DOOR_TO_UPDATE - Checkpoint 1 has passed, return TRUE")
				return true
			ENDIF
		ENDIF
		
		//IF any of the bits are set then we need to wait wait until the checkpoints are passed before allowing the door to be updated
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iBitSet, ciDoorBS_LEGACY_only_update_door_on_checkpoint_2)
		or IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iBitSet, ciDoorBS_LEGACY_only_update_door_on_checkpoint_1)
		OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iBitSet, ciDoorBS_LEGACY_OnlyUpdateOnCheckpoint_3)
		OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iBitSet, ciDoorBS_LEGACY_OnlyUpdateOnCheckpoint_4)
			PRINTLN("[RCC MISSION] ALLOW_DOOR_TO_UPDATE - Returning FALSE, this is a quick restart but we're not at the right checkpoint")
			return false
		ENDIF
	ELSE
		//in the event we are not quick restarting i.e. first attempt on mission
		//we don't want to update the door on a normal play through IF any of the bit sets are set.
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iBitSet, ciDoorBS_LEGACY_only_update_door_on_checkpoint_2)
		or IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iBitSet, ciDoorBS_LEGACY_only_update_door_on_checkpoint_1)
		OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iBitSet, ciDoorBS_LEGACY_OnlyUpdateOnCheckpoint_3)
		OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iBitSet, ciDoorBS_LEGACY_OnlyUpdateOnCheckpoint_4)
			PRINTLN("[RCC MISSION] ALLOW_DOOR_TO_UPDATE - Returning FALSE, this is not a quick restart")
			return false
		ENDIF
	ENDIF
	
	return true
	
endfunc 

FUNC BOOL SHOULD_MAP_DOOR_OPEN( INT iDoor, BOOL &bIsUpdate )
	
	INT iDoorTeam = g_FMMC_STRUCT_ENTITIES.sSelectedDoor[ iDoor ].iUpdateTeam
	INT iDoorRule = g_FMMC_STRUCT_ENTITIES.sSelectedDoor[ iDoor ].iUpdateRule

	// the data for the door wasn't set
	IF iDoorTeam <= 0
	AND iDoorRule <= 0
		RETURN TRUE
	ENDIF
	
	IF iDoorTeam >= 0
		INT iRule = MC_ServerBD_4.iCurrentHighestPriority[iDoorTeam]
		
		IF iRule < FMMC_MAX_RULES
		AND iRule >= iDoorRule // we've hit the specified rule
		
			IF g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].bUpdateOnMidpoint // if we want to wait for the midpoint of the rule
				
				IF iRule = iDoorRule
					
					PRINTLN("bank_vault_door_opens test 0")
					
					IF IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iDoorTeam], iRule) // if we've hit the midpoint of the rule
						IF door_functionality_checks(iDoor)
							bIsUpdate = TRUE
							RETURN TRUE
						ENDIF
					ENDIF
				ELSE
					IF DOOR_FUNCTIONALITY_CHECKS(iDoor)
						PRINTLN("bank_vault_door_opens test 1")
						bIsUpdate = TRUE
						return true
					ENDIF
				ENDIF
			ELSE
				IF DOOR_FUNCTIONALITY_CHECKS(iDoor)
					bIsUpdate = TRUE
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE
	
ENDFUNC

PROC REQUEST_FLECCA_BANK_DOOR_ASSETS()
	
	REQUEST_ANIM_DICT("anim@heists@fleeca_bank@bank_vault_door")
	REQUEST_MODEL(GET_FMMC_DOOR_MODEL(CIFMMC_DOOR_FLECCA_CHUMASH1))
	
ENDPROC

FUNC BOOL HAS_FLEECA_DOOR_ASSETS_LOADED()

	IF HAS_ANIM_DICT_LOADED("anim@heists@fleeca_bank@bank_vault_door")
	AND HAS_MODEL_LOADED(GET_FMMC_DOOR_MODEL(CIFMMC_DOOR_FLECCA_CHUMASH1))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL BANK_DOOR_ANIMATION_SYSTEM(object_index door, int door_index)
					
	IF DOES_ENTITY_EXIST(door)
		IF IS_DOOR_A_FLECCA_BANK_DOOR(door_index)

			IF NOT IS_BIT_SET(iLocalBoolCheck9, LBOOL9_fleeca_bank_door_anim_triggered)

				REQUEST_FLECCA_BANK_DOOR_ASSETS()
								
				IF HAS_FLEECA_DOOR_ASSETS_LOADED()
					PLAY_ENTITY_ANIM(door, "bank_vault_door_opens", "anim@heists@fleeca_bank@bank_vault_door", slow_blend_in, false, true, false, 0, enum_to_int(AF_NOT_INTERRUPTABLE))
					FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(door)
					SET_BIT(iLocalBoolCheck9, LBOOL9_fleeca_bank_door_anim_triggered)
				ENDIF
			ELSE 
				#IF IS_DEBUG_BUILD
				vector door_rot = get_entity_rotation(door)
				PRINTLN("get_entity_rotation(door) = ", door_rot)
				#ENDIF
				
				IF IS_ENTITY_PLAYING_ANIM(door, "anim@heists@fleeca_bank@bank_vault_door", "bank_vault_door_opens")
					
					PRINTLN("bank_vault_door_opens ", door_index, " anim phase = ", get_entity_anim_current_time(door, "anim@heists@fleeca_bank@bank_vault_door", "bank_vault_door_opens"))
					
					IF GET_ENTITY_ANIM_CURRENT_TIME(door, "anim@heists@fleeca_bank@bank_vault_door", "bank_vault_door_opens") >= 1.0
						FREEZE_ENTITY_POSITION(door, true)
						STOP_ENTITY_ANIM(door, "bank_vault_door_opens", "anim@heists@fleeca_bank@bank_vault_door", instant_blend_out)
						SET_ENTITY_ROTATION(door, (GET_ENTITY_ROTATION(door) + <<0.0, 0.0, -80.00>>))
						FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(door)
						SET_MODEL_AS_NO_LONGER_NEEDED(GET_FMMC_DOOR_MODEL(CIFMMC_DOOR_FLECCA_CHUMASH1))
						
						PRINTLN("bank_vault_door_opens OPENED")
						
						return true
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF
	ENDIF
	
	return false
	
endfunc

/// PURPOSE: returns true IF the currently playing mission is humane labs emp
FUNC BOOL IS_CURRENT_MISSION_HUMANE_LABS_EMP()
	
	BOOL bReturn = FALSE
	
	INT iRootContentID = GET_STRAND_ROOT_CONTENT_ID_HASH()
	
	IF iRootContentID = 0
		iRootContentID = g_FMMC_STRUCT.iRootContentIdHash
	ENDIF
	
	IF iRootContentID = g_sMPTunables.iroot_id_HASH_Humane_Labs_EMP
	OR iRootContentID = g_sMPTunables.iroot_id_HASH_Humane_Labs_EMP_v2
		bReturn = TRUE
	ENDIF
	
	#IF IS_DEBUG_BUILD
		PRINTLN("IS_CURRENT_MISSION_HUMANE_LABS_EMP - bReturn = ", bReturn)
	#ENDIF
	
	RETURN bReturn
	
ENDFUNC

/// PURPOSE: The system will put the door back to it's original heading.
///    		 In the event the checkpoint door has been allowed to update resulting in the door being locked. 
///    		 If the player falls off the aircraft carrier into the sea and then makes their way through the carrier
///     	 as a result of the door being locked they will NOT be able to progress. 
///    		 The sytem will put the door back to the original heading.
PROC PROCESS_HUMANE_LABS_EMP_DOORS(INT iDoor)

	IF NOT IS_BIT_SET(iLocalBoolCheck12, LBOOL12_EMP_AIRCRAFT_CHECKPOINT_1_DOOR_OPEN)
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iBitSet, ciDoorBS_LEGACY_only_update_door_on_checkpoint_1)
			IF is_mission_start_checkpoint_set(1)
				IF IS_FAKE_DOOR(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].mnDoorModel)
					IF does_entity_exist(map_door[iDoor])
						IF is_entity_in_angled_area(player_ped_id(), <<3075.592, -4702.483, 9.742>>, <<3077.649, -4710.214, 12.742>>, 2.9)//area near checkpoint door 1
							SET_ENTITY_ROTATION(map_door[iDoor], <<0, 0, -141.00>>)
							SET_BIT(iLocalBoolCheck12, LBOOL12_EMP_AIRCRAFT_CHECKPOINT_1_DOOR_OPEN)
							
							PRINTLN("[RCC MISSION] LBOOL12_EMP_AIRCRAFT_CHECKPOINT_1_DOOR_OPEN, Door = ", iDoor)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(iLocalBoolCheck12, LBOOL12_EMP_AIRCRAFT_CHECKPOINT_2_DOOR_OPEN)
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iBitSet, ciDoorBS_LEGACY_only_update_door_on_checkpoint_2)
			IF is_mission_start_checkpoint_set(2)
				IF IS_FAKE_DOOR(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].mnDoorModel)
					IF does_entity_exist(map_door[iDoor])
						IF is_entity_in_angled_area(player_ped_id(), <<3075.592, -4702.483, 9.742>>, <<3077.649, -4710.214, 12.742>>, 2.9)//area near checkpoint door 1
							SET_ENTITY_ROTATION(map_door[iDoor], <<0, 0, -141.00>>)
							SET_BIT(iLocalBoolCheck12, LBOOL12_EMP_AIRCRAFT_CHECKPOINT_2_DOOR_OPEN)
							
							PRINTLN("[RCC MISSION] LBOOL12_EMP_AIRCRAFT_CHECKPOINT_2_DOOR_OPEN, Door index = ", iDoor)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE:
///    The following two functions have been set up to deal with url:bugstar:4144706
///    After some updates to the door system the doors in the Humane Raid EMP mission 
///    from heists one stopped working and we were unable to fix them. These two 
///    functions are called in MAINTAIN_SETTING_MAP_OBJECT_DOOR_RATIOS and manage the 
///    broken door specifically. 
///    Any questions speak to Paul D
///    
/// PARAMS:
///    iDoor - 
/// RETURNS:
///    
FUNC BOOL IS_SPECIAL_CASE_EMP_DOOR(INT iDoor)

	IF IS_CURRENT_MISSION_HUMANE_LABS_EMP()
	#IF IS_DEBUG_BUILD
	OR (IS_ROCKSTAR_DEV() AND ARE_STRINGS_EQUAL(g_FMMC_STRUCT.tl63MissionName, "Humane Raid - EMP"))
	#ENDIF
		
		IF g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].mnDoorModel = HEI_PROP_HEIST_CARRIERDOORL
		AND ARE_VECTORS_ALMOST_EQUAL(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].vPos, <<3086.7395, -4704.5254, 10.8015>>, 0.1)
			PRINTLN("IS_SPECIAL_CASE_EMP_DOOR - returned true for ", iDoor)
			RETURN TRUE
		ENDIF
		
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC MAINTAIN_SPECIAL_CASE_EMP_DOOR()

	VECTOR vRot
	
	VECTOR vDoorPos = <<3086.7395, -4704.5254, 10.8015>>
	
	IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(vDoorPos, 0.1, HEI_PROP_HEIST_CARRIERDOORL)
		OBJECT_INDEX objTemp = GET_CLOSEST_OBJECT_OF_TYPE(vDoorPos, 0.1, HEI_PROP_HEIST_CARRIERDOORL, FALSE)
		IF DOES_ENTITY_EXIST(objTemp)
			vRot = GET_ENTITY_ROTATION(objTemp)
			PRINTLN("MAINTAIN_SPECIAL_CASE_EMP_DOOR 5 - Rotation is ", vRot.Z)
			IF ABSF(vRot.Z - 105) < 5 // If it's closed
				PRINTLN("MAINTAIN_SPECIAL_CASE_EMP_DOOR 5 - Door is CLOSED")
				IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<3077.915283,-4705.912109,13.153502>>, <<3047.710205,-4713.943848,5.081580>>, 62.750000)
					PRINTLN("MAINTAIN_SPECIAL_CASE_EMP_DOOR 5 - Opening door")
					SET_ENTITY_ROTATION(objTemp, <<0, 0, -141.00>>)
				ENDIF
			ELIF ABSF(vRot.Z - -140) < 5 // If it's open
				PRINTLN("MAINTAIN_SPECIAL_CASE_EMP_DOOR 5 - Door is OPEN")
				IF NOT IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<3077.915283,-4705.912109,13.153502>>, <<3047.710205,-4713.943848,5.081580>>, 62.750000)
				AND VDIST2(vDoorPos, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)) > 15*15
					PRINTLN("MAINTAIN_SPECIAL_CASE_EMP_DOOR 5 - Closing door")
					SET_ENTITY_ROTATION(objTemp, <<0, 0, 105.00>>)
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC INCREMENT_DOOR_NUMBER()
	iMapObjectDoorCount++
	IF iMapObjectDoorCount >= g_FMMC_STRUCT_ENTITIES.iNumberOfDoors
		iMapObjectDoorCount = 0
	ENDIF
ENDPROC

PROC MAINTAIN_SETTING_MAP_OBJECT_DOOR_RATIOS()
	
	//	url:bugstar:4144706 Special case door handling, see function definition for IS_SPECIAL_CASE_EMP_DOOR for more info - Paul D
	IF IS_SPECIAL_CASE_EMP_DOOR(iMapObjectDoorCount)
		MAINTAIN_SPECIAL_CASE_EMP_DOOR()
		INCREMENT_DOOR_NUMBER()
		EXIT
	ENDIF
	// 	url:bugstar:4144706 Special case door handling, see function definition for IS_SPECIAL_CASE_EMP_DOOR for more info  - Paul D
	
	
	FLOAT fRatio
	BOOL bIsUpdate
	
	IF NOT IS_BIT_SET(iMapObjectDoorRatioSetBitset, iMapObjectDoorCount)
	OR NOT IS_BIT_SET(iMapObjectDoorRatioFirstSetBitset, iMapObjectDoorCount)
		
		IF SHOULD_MAP_DOOR_OPEN(iMapObjectDoorCount,bIsUpdate)
		OR NOT IS_BIT_SET(iMapObjectDoorRatioFirstSetBitset, iMapObjectDoorCount)		
			
			IF HAS_DOOR_TIME_EXCEEDED(iMapObjectDoorCount) OR IS_BIT_SET(iDoorUpdatedBS, iMapObjectDoorCount)
				
				IF IS_DOOR_VAULT_OBJECT_DOOR(iMapObjectDoorCount)
				OR IS_FAKE_DOOR(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iMapObjectDoorCount].mnDoorModel)
					
					IF NOT DOES_ENTITY_EXIST(map_door[iMapObjectDoorCount])
						
						IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iMapObjectDoorCount].vpos, 1.0, g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iMapObjectDoorCount].mnDoorModel)
							
							IF CAN_REGISTER_MISSION_OBJECTS(1)
								
								IF IS_DOOR_A_FLECCA_BANK_DOOR(iMapObjectDoorCount)
									
									REQUEST_FLECCA_BANK_DOOR_ASSETS()
									
									IF HAS_FLEECA_DOOR_ASSETS_LOADED()
										MC_RESERVE_NETWORK_MISSION_OBJECTS(1)
										map_door[iMapObjectDoorCount] = GET_CLOSEST_OBJECT_OF_TYPE(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iMapObjectDoorCount].vpos, 1.0, g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iMapObjectDoorCount].mnDoorModel, true, false, false)
										FREEZE_ENTITY_POSITION(map_door[iMapObjectDoorCount], true)
										
										PRINTLN("[RCC MISSION] MAINTAIN_SETTING_MAP_OBJECT_DOOR_RATIOS LK - get_closest_object_of_type = ",iMapObjectDoorCount)
										
									#IF IS_DEBUG_BUILD
									ELSE
										PRINTLN("[RCC MISSION] MAINTAIN_SETTING_MAP_OBJECT_DOOR_RATIOS LK - fleeca door assets NOT loaded = ")
									#ENDIF
									ENDIF
								ELSE
									MC_RESERVE_NETWORK_MISSION_OBJECTS(1)
									map_door[imapobjectdoorcount] = GET_CLOSEST_OBJECT_OF_TYPE(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iMapObjectDoorCount].vpos, 1.0, g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iMapObjectDoorCount].mnDoorModel, TRUE, false, false)
									
									PRINTLN("[RCC MISSION] MAINTAIN_SETTING_MAP_OBJECT_DOOR_RATIOS LK - GET_CLOSEST_OBJECT_OF_TYPE = ",iMapObjectDoorCount)
								ENDIF
								
								PRINTLN("[RCC MISSION] MAINTAIN_SETTING_MAP_OBJECT_DOOR_RATIOS LK - closest door found door count = ",iMapObjectDoorCount)
							ELSE
								PRINTLN("[RCC MISSION] MAINTAIN_SETTING_MAP_OBJECT_DOOR_RATIOS LK - Unable to register 1 mission object for map door ",iMapObjectDoorCount)
							ENDIF
						ELSE
							PRINTLN("[RCC MISSION] MAINTAIN_SETTING_MAP_OBJECT_DOOR_RATIOS LK - Unable to find object for map door ",iMapObjectDoorCount)
						ENDIF
					ENDIF
					
					IF DOES_ENTITY_EXIST(map_door[iMapObjectDoorCount])
						
						PRINTLN("[RCC MISSION] MAINTAIN_SETTING_MAP_OBJECT_DOOR_RATIOS, Door ",iMapObjectDoorCount," - Attempting to update door at vPos ",g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iMapObjectDoorCount].vPos)
						
						IF bIsUpdate
						AND IS_BIT_SET(iMapObjectDoorRatioFirstSetBitset, iMapObjectDoorCount)
							fRatio = g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iMapObjectDoorCount].sDoorConfigs[ciDOOR_CONFIG__ALT].fOpenRatio
						ELSE							
							fRatio = g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iMapObjectDoorCount].sDoorConfigs[ciDOOR_CONFIG__STANDARD].fOpenRatio
						ENDIF
						
						IF IS_DOOR_VAULT_OBJECT_DOOR(iMapObjectDoorCount)
						
							PRINTLN("[RCC MISSION] MAINTAIN_SETTING_MAP_OBJECT_DOOR_RATIOS, Door ",iMapObjectDoorCount," - Calling SET_OBJECT_DOOR_RATIO with fRatio ",fRatio)
							
							IF (door_index_to_update_rotation_on = -1)
								door_index_to_update_rotation_on = iMapObjectDoorCount
							ENDIF
						ELIF IS_FAKE_DOOR(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iMapObjectDoorCount].mnDoorModel)
							
							PRINTLN("[RCC MISSION] MAINTAIN_SETTING_MAP_OBJECT_DOOR_RATIOS, Door ",iMapObjectDoorCount," - Calling SET_AIRCRAFT_CARRIER_DOOR_RATIO with fRatio ",fRatio)
							SET_FAKE_DOOR_RATIO(map_door[iMapObjectDoorCount], fRatio , iMapObjectDoorCount  )
							
							IF NOT IS_BIT_SET(iMapObjectDoorRatioFirstSetBitset, iMapObjectDoorCount)
								SET_BIT(iMapObjectDoorRatioFirstSetBitset, iMapObjectDoorCount)
							ELSE							
								SET_BIT(iMapObjectDoorRatioSetBitset,iMapObjectDoorCount)
							ENDIF
						ELSE
							SET_BIT(iMapObjectDoorRatioSetBitset,iMapObjectDoorCount)
							PRINTLN("[RCC MISSION] MAINTAIN_SETTING_MAP_OBJECT_DOOR_RATIOS - Door ",iMapObjectDoorCount," isn't a vault door or aircraft carrier! Model_name = ",g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iMapObjectDoorCount].mnDoorModel)
						ENDIF
					ENDIF	
				ENDIF
			ENDIF
		ENDIF
		
	ELSE
		IF IS_CURRENT_MISSION_HUMANE_LABS_EMP()
			PROCESS_HUMANE_LABS_EMP_DOORS(iMapObjectDoorCount)
		ENDIF
	ENDIF
	
	//forces the door to update every frame rather than once every total number of doors(frames) e.g. 5 doors will mean door 0 will have to wait 5 frames before it is updated again
	IF door_index_to_update_rotation_on != -1
		IF SHOULD_MAP_DOOR_OPEN(door_index_to_update_rotation_on, bIsUpdate)
			IF HAS_DOOR_TIME_EXCEEDED(door_index_to_update_rotation_on) 
			OR IS_BIT_SET(iDoorUpdatedBS, door_index_to_update_rotation_on)
				
				IF bIsUpdate
					fRatio = g_FMMC_STRUCT_ENTITIES.sSelectedDoor[door_index_to_update_rotation_on].sDoorConfigs[ciDOOR_CONFIG__ALT].fOpenRatio
				ELSE
					fRatio = g_FMMC_STRUCT_ENTITIES.sSelectedDoor[door_index_to_update_rotation_on].sDoorConfigs[ciDOOR_CONFIG__STANDARD].fOpenRatio
				ENDIF
				
				IF IS_DOOR_VAULT_OBJECT_DOOR(door_index_to_update_rotation_on)
					
					PRINTLN("[RCC MISSION] MAINTAIN_SETTING_MAP_OBJECT_DOOR_RATIOS, Door ",door_index_to_update_rotation_on," - Calling SET_OBJECT_DOOR_RATIO with fRatio ",fRatio)
					
					IF NOT IS_BIT_SET(iLocalBoolCheck8, LBOOL8_vault_door_sound)
						IF NOT g_TransitionSessionNonResetVars.bHasQuickRestarted 
						OR FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(0)
							play_sound_from_vault_door(door_index_to_update_rotation_on)
						ENDIF
					ENDIF
					
					IF IS_DOOR_A_FLECCA_BANK_DOOR(door_index_to_update_rotation_on)
						
						IF DOES_ENTITY_HAVE_DRAWABLE(map_door[door_index_to_update_rotation_on])
							IF bank_door_animation_system(map_door[door_index_to_update_rotation_on], door_index_to_update_rotation_on)
								CLEAR_BIT(ilocalboolcheck8, LBOOL8_vault_door_sound)
								CLEAR_BIT(iLocalBoolCheck9, LBOOL9_fleeca_bank_door_anim_triggered)
								SET_BIT(iMapObjectDoorRatioSetBitset,door_index_to_update_rotation_on)
								SET_BIT(iMapObjectDoorRatioFirstSetBitset,door_index_to_update_rotation_on)
								door_index_to_update_rotation_on = -1
								PRINTLN("[RCC MISSION] MAINTAIN_SETTING_MAP_OBJECT_DOOR_RATIOS, anim finished ")
							ENDIF
						ENDIF
						
					ELSE
						
						IF SET_OBJECT_DOOR_RATIO(map_door[door_index_to_update_rotation_on], door_index_to_update_rotation_on, fRatio)
							CLEAR_BIT(ilocalboolcheck8, LBOOL8_vault_door_sound)
							CLEAR_BIT(iLocalBoolCheck9, LBOOL9_fleeca_bank_door_anim_triggered)
							SET_BIT(iMapObjectDoorRatioSetBitset, door_index_to_update_rotation_on)
							SET_BIT(iMapObjectDoorRatioFirstSetBitset, door_index_to_update_rotation_on)
							door_index_to_update_rotation_on = -1
						ENDIF
						
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
		
	
	INCREMENT_DOOR_NUMBER()
	
ENDPROC

PROC PROCESS_CHANGES_TO_CASINO_DOOR(INT iDoorHash, FLOAT fCurrentDoorOpenRatio, BOOL bNetworked = TRUE)
	SET_SCRIPT_UPDATE_DOOR_AUDIO(iDoorHash, TRUE)
	IF NETWORK_HAS_CONTROL_OF_DOOR(iDoorHash)
		DOOR_SYSTEM_SET_OPEN_RATIO(iDoorHash, fCurrentDoorOpenRatio, bNetworked)
		DOOR_SYSTEM_SET_DOOR_STATE(iDoorHash, DOORSTATE_FORCE_LOCKED_THIS_FRAME, bNetworked, TRUE)
	ENDIF
ENDPROC

PROC SET_DAILY_CASH_ROOM_STATE(CASINO_DAILY_CASH_DOOR_STATE eNewState)
	IF eCasinoDailyCashState != eNewState
		PRINTLN("[CasinoDailyDoor] SET_DAILY_CASH_ROOM_STATE - Setting eCasinoDailyCashState from ", eCasinoDailyCashState, " to ", eNewState)
		eCasinoDailyCashState = eNewState
	ENDIF
ENDPROC

FUNC BOOL IS_ANY_PLAYER_HOLDING_DOOR_BUTTON(INT iDoor)
	
	IF NOT IS_BIT_SET(iDoorHasLinkedEntityBS, iDoor)
		// This door has no button
		RETURN FALSE
	ENDIF
	
	INT iPart, iNumPlayersChecked
	
	WHILE DO_PLAYER_LOOP(iPart, iNumPlayersChecked)
		IF MC_playerBD_1[iPart].iCurrentHoldInteract != -1
		AND (g_FMMC_STRUCT_ENTITIES.sPlacedObject[MC_playerBD_1[iPart].iCurrentHoldInteract].iLinkedDoor1 = iDoor OR g_FMMC_STRUCT_ENTITIES.sPlacedObject[MC_playerBD_1[iPart].iCurrentHoldInteract].iLinkedDoor2 = iDoor)
			PRINTLN("IS_ANY_PLAYER_HOLDING_DOOR_BUTTON - Returning true for object ", MC_playerBD_1[iPart].iCurrentHoldInteract, " Door ", iDoor)
			RETURN TRUE
		ENDIF
		
		iPart++
	ENDWHILE
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_TIMED_DOOR_UPDATE_TIMER_RUNNING(INT iDoor)
	IF HAS_NET_TIMER_STARTED(tdUpdateTimedDoorTimer[iDoor])
		RETURN NOT HAS_NET_TIMER_EXPIRED(tdUpdateTimedDoorTimer[iDoor], ciTIMED_DOOR_UPDATE)
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_DAILY_DOOR_OPEN(INT iDoor)
	IF IS_ANY_PLAYER_HOLDING_DOOR_BUTTON(iDoor)
		RETURN TRUE
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(tdUpdateTimedDoorTimer[iDoor])
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CAN_DOOR_UPDATE_TIMER_START(INT iDoor)
	IF IS_ANY_PLAYER_HOLDING_DOOR_BUTTON(iDoor)
		PRINTLN("CAN_DOOR_UPDATE_TIMER_START - Cannot start timer as someone is holding the door open with an interact with")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC PROCESS_UPDATE_DOOR_TIMED_OPENING(INT iDoor)
	IF NOT HAS_NET_TIMER_STARTED(tdUpdateTimedDoorTimer[iDoor])
		IF CAN_DOOR_UPDATE_TIMER_START(iDoor)
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iBitSet, ciDoorBS_LEGACY_TimedUpdate)
			AND IS_BIT_SET(MC_serverBD_1.iDoorUpdateOffRuleBS, iDoor)
				REINIT_NET_TIMER(tdUpdateTimedDoorTimer[iDoor])
				PRINTLN("PROCESS_UPDATE_DOOR_TIMED_OPENING - Starting tdUpdateTimedDoorTimer[",iDoor,"]")
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_TIMED_DOOR_UPDATE_TIMER_RUNNING(iDoor)
			RESET_NET_TIMER(tdUpdateTimedDoorTimer[iDoor])
			BROADCAST_FMMC_UPDATE_DOOR_OFF_RULE_EVENT(FALSE, iDoor, ciUPDATE_DOOR_OFF_RULE_METHOD__TIMED_OPENING)
			PRINTLN("PROCESS_UPDATE_DOOR_TIMED_OPENING - Reset tdUpdateTimedDoorTimer[",iDoor,"] and broadcasted")
		ENDIF
	ENDIF
ENDPROC

#IF IS_DEBUG_BUILD
PROC CASINO_DAILY_DOOR_DEBUG(VECTOR vDailyDoorPos)

	IF NOT IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD3)
		EXIT
	ENDIF
	
	IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(iCasinoDailyDoorHash)
		DRAW_DEBUG_TEXT("DOOR NOT REGISTERED", vDailyDoorPos)
		EXIT
	ENDIF
	
	TEXT_LABEL_63 tlEntityDebug = ""
	
	tlEntityDebug = "eCasinoDailyCashState: "
	tlEntityDebug += ENUM_TO_INT(eCasinoDailyCashState)
	DRAW_DEBUG_TEXT(tlEntityDebug, vDailyDoorPos + (<<0,0, 0.2>>))
	
	tlEntityDebug = "fCasinoDailyDoor_CurrentOpenRatio: "
	tlEntityDebug += FLOAT_TO_STRING(fCasinoDailyDoor_CurrentOpenRatio)
	DRAW_DEBUG_TEXT(tlEntityDebug, vDailyDoorPos + (<<0,0, -0.2>>))
	
	tlEntityDebug = "fCasinoDailyDoor_CurrentSpeed: "
	tlEntityDebug += FLOAT_TO_STRING(fCasinoDailyDoor_CurrentSpeed)
	DRAW_DEBUG_TEXT(tlEntityDebug, vDailyDoorPos + (<<0,0, -0.3>>))	
	
ENDPROC
#ENDIF

PROC PROCESS_DAILY_DOOR_DECELERATION_FOR_TRANSITION(BOOL bOpening, BOOL bNetworked)
	fCasinoDailyDoor_CurrentSpeed -= GET_FRAME_TIME() * (cfCasinoDailyDoor_DoorOpenDecelerationTransition + (fCasinoDailyDoor_CurrentSpeed * 0.9))
	fCasinoDailyDoor_CurrentSpeed = CLAMP(fCasinoDailyDoor_CurrentSpeed, 0.01, cfCasinoDailyDoor_DoorOpenMaxSpeed)
	
	// Increase the actual ratio using the speed
	IF bOpening
		fCasinoDailyDoor_CurrentOpenRatio -= GET_FRAME_TIME() * fCasinoDailyDoor_CurrentSpeed
	ELSE
		fCasinoDailyDoor_CurrentOpenRatio += GET_FRAME_TIME() * fCasinoDailyDoor_CurrentSpeed
	ENDIF
	fCasinoDailyDoor_CurrentOpenRatio = CLAMP(fCasinoDailyDoor_CurrentOpenRatio, 0.0, cfCasinoDailyDoor_DoorOpenMaxRatio)
	
	// Apply our vars to the door
	PROCESS_CHANGES_TO_CASINO_DOOR(iCasinoDailyDoorHash, -fCasinoDailyDoor_CurrentOpenRatio, bNetworked)
ENDPROC

PROC ADD_DAILY_VAULT_DOOR_TO_DOOR_SYSTEM(INT iDoor, BOOL bNetworked)
	IF bNetworked
	AND NOT bIsLocalPlayerHost
		EXIT
	ENDIF
	
	ADD_DOOR_TO_SYSTEM(iCasinoDailyDoorHash, g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].mnDoorModel,g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].vPos, FALSE, bNetworked)
	iCachedDoorHashes[iDoor] = iCasinoDailyDoorHash
	PRINTLN("[CasinoDailyDoor] Attempting to add Casino Daily door with hash ", iCasinoDailyDoorHash, " at pos ", g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].vPos)
ENDPROC

PROC PROCESS_CASINO_DAILY_CASH_ROOM(INT iDoor)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO_BACK_AREA)
		EXIT
	ENDIF
	
	MODEL_NAMES mnDoorModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_vault_d_door_01a"))
	BOOL bNetworked = TRUE
	
	IF eCasinoDailyCashState != CASINO_DAILY_CASH_STATE__OPENING
	AND fCasinoDailyDoor_CurrentOpenRatio != -1.0
	AND IS_BIT_SET(iLocalBoolCheck33, LBOOL33_DAILY_CASH_ROOM_DOOR_OPEN_SOUND_PLAYED)
		CLEAR_BIT(iLocalBoolCheck33, LBOOL33_DAILY_CASH_ROOM_DOOR_OPEN_SOUND_PLAYED)
		PRINTLN("PROCESS_CASINO_DAILY_CASH_ROOM_SOUNDS - Clearing LBOOL33_DAILY_CASH_ROOM_DOOR_OPEN_SOUND_PLAYED")
	ENDIF
	
	SWITCH eCasinoDailyCashState
		CASE CASINO_DAILY_CASH_STATE__INIT
			REQUEST_MODEL(mnDoorModel)
			IF NOT HAS_MODEL_LOADED(mnDoorModel)
				PRINTLN("[CasinoDailyDoor] Waiting to load daily door model!")
				EXIT
			ENDIF
			
			fCasinoDailyDoor_CurrentOpenRatio = 0.0
			fCasinoDailyDoor_CurrentSpeed = 0.0
			
			IF GET_NEW_DOOR_HASH(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].mnDoorModel, g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].vPos, iCasinoDailyDoorHash, iCachedDoorHashes[iDoor])
				IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(iCasinoDailyDoorHash)
					ADD_DAILY_VAULT_DOOR_TO_DOOR_SYSTEM(iDoor, bNetworked)
				ENDIF
			ENDIF	
			
			PRINTLN("[CasinoDailyDoor] Casino Daily door has been added with hash ", iCasinoDailyDoorHash)
			
			IF IS_DOOR_REGISTERED_WITH_SYSTEM(iCasinoDailyDoorHash)
				IF NETWORK_HAS_CONTROL_OF_DOOR(iCasinoDailyDoorHash)
					DOOR_SYSTEM_SET_DOOR_STATE(iCasinoDailyDoorHash, DOORSTATE_LOCKED)
				ENDIF
				SET_DAILY_CASH_ROOM_STATE(CASINO_DAILY_CASH_STATE__WAITING)
			ENDIF
		BREAK
		
		CASE CASINO_DAILY_CASH_STATE__WAITING
			fCasinoDailyDoor_CurrentOpenRatio = 0.0
			fCasinoDailyDoor_CurrentSpeed = 0.0
			IF NETWORK_HAS_CONTROL_OF_DOOR(iCasinoDailyDoorHash)
				DOOR_SYSTEM_SET_DOOR_STATE(iCasinoDailyDoorHash, DOORSTATE_FORCE_LOCKED_THIS_FRAME, bNetworked, TRUE)
			ENDIF
			IF SHOULD_DAILY_DOOR_OPEN(iDoor)
				IF NETWORK_HAS_CONTROL_OF_DOOR(iCasinoDailyDoorHash)
					PLAY_SOUND_FROM_COORD(-1,"cash_room_door_open", g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].vPos, "dlc_ch_heist_finale_sounds", TRUE)
				ENDIF
				SET_DAILY_CASH_ROOM_STATE(CASINO_DAILY_CASH_STATE__TRANSITION_OPEN)				
			ENDIF
		BREAK
		
		CASE CASINO_DAILY_CASH_STATE__TRANSITION_OPEN
			
			PROCESS_DAILY_DOOR_DECELERATION_FOR_TRANSITION(TRUE, bNetworked)
			
			IF fCasinoDailyDoor_CurrentSpeed <= cfCasinoDailyDoor_DoorOpenMinSpeed
				SET_DAILY_CASH_ROOM_STATE(CASINO_DAILY_CASH_STATE__OPENING)
			ENDIF
		BREAK
				
		CASE CASINO_DAILY_CASH_STATE__OPENING
			IF NOT SHOULD_DAILY_DOOR_OPEN(iDoor)
				SET_DAILY_CASH_ROOM_STATE(CASINO_DAILY_CASH_STATE__TRANSITION_CLOSE)
			ENDIF
			
			// Increase the speed
			IF fCasinoDailyDoor_CurrentOpenRatio < cfCasinoDailyDoor_DoorOpenMaxRatio * cfCasinoDailyDoor_DecelerationRatio
				fCasinoDailyDoor_CurrentSpeed += GET_FRAME_TIME() * (cfCasinoDailyDoor_DoorOpenAcceleration + (fCasinoDailyDoor_CurrentSpeed * cfCasinoDailyDoor_DoorOpenSpeedBuildupMultiplier))
			ELSE
				fCasinoDailyDoor_CurrentSpeed -= GET_FRAME_TIME() * (cfCasinoDailyDoor_DoorOpenDeceleration + (fCasinoDailyDoor_CurrentSpeed * cfCasinoDailyDoor_DoorOpenSpeedBuildupMultiplier))
			ENDIF
			fCasinoDailyDoor_CurrentSpeed = CLAMP(fCasinoDailyDoor_CurrentSpeed, cfCasinoDailyDoor_DoorOpenMinSpeed, cfCasinoDailyDoor_DoorOpenMaxSpeed)
			
			// Increase the actual ratio using the speed
			fCasinoDailyDoor_CurrentOpenRatio += GET_FRAME_TIME() * fCasinoDailyDoor_CurrentSpeed
			fCasinoDailyDoor_CurrentOpenRatio = CLAMP(fCasinoDailyDoor_CurrentOpenRatio, 0.0, cfCasinoDailyDoor_DoorOpenMaxRatio)
			
			// Apply our vars to the door
			PROCESS_CHANGES_TO_CASINO_DOOR(iCasinoDailyDoorHash, -fCasinoDailyDoor_CurrentOpenRatio, bNetworked)
			
			IF fCasinoDailyDoor_CurrentOpenRatio = 1.0
			AND NOT IS_BIT_SET(iLocalBoolCheck33, LBOOL33_DAILY_CASH_ROOM_DOOR_OPEN_SOUND_PLAYED)
				IF NETWORK_HAS_CONTROL_OF_DOOR(iCasinoDailyDoorHash)
					PLAY_SOUND_FROM_COORD(-1,"cash_room_door_open_end", g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].vPos, "dlc_ch_heist_finale_sounds", TRUE)
				ENDIF
				SET_BIT(iLocalBoolCheck33, LBOOL33_DAILY_CASH_ROOM_DOOR_OPEN_SOUND_PLAYED)
				PRINTLN("PROCESS_CASINO_DAILY_CASH_ROOM_SOUNDS - Setting LBOOL33_DAILY_CASH_ROOM_DOOR_OPEN_SOUND_PLAYED")
			ENDIF
		BREAK
		
		CASE CASINO_DAILY_CASH_STATE__TRANSITION_CLOSE
		
			PROCESS_DAILY_DOOR_DECELERATION_FOR_TRANSITION(FALSE, bNetworked)
			
			IF fCasinoDailyDoor_CurrentSpeed <= cfCasinoDailyDoor_DoorOpenMinSpeed
				SET_DAILY_CASH_ROOM_STATE(CASINO_DAILY_CASH_STATE__CLOSING)
			ENDIF
		BREAK
		
		CASE CASINO_DAILY_CASH_STATE__CLOSING
		
			IF SHOULD_DAILY_DOOR_OPEN(iDoor)
				SET_DAILY_CASH_ROOM_STATE(CASINO_DAILY_CASH_STATE__TRANSITION_OPEN)
			ENDIF
			
			// Increase the speed
			IF fCasinoDailyDoor_CurrentOpenRatio > cfCasinoDailyDoor_DoorOpenMaxRatio * (cfCasinoDailyDoor_DoorOpenMaxRatio - cfCasinoDailyDoor_DecelerationRatio)
				fCasinoDailyDoor_CurrentSpeed += GET_FRAME_TIME() * (cfCasinoDailyDoor_DoorOpenAcceleration + (fCasinoDailyDoor_CurrentSpeed * cfCasinoDailyDoor_DoorOpenSpeedBuildupMultiplier))
			ELSE
				fCasinoDailyDoor_CurrentSpeed -= GET_FRAME_TIME() * (cfCasinoDailyDoor_DoorOpenDeceleration + (fCasinoDailyDoor_CurrentSpeed * cfCasinoDailyDoor_DoorOpenSpeedBuildupMultiplier))
			ENDIF
			fCasinoDailyDoor_CurrentSpeed = CLAMP(fCasinoDailyDoor_CurrentSpeed, cfCasinoDailyDoor_DoorOpenMinSpeed, cfCasinoDailyDoor_DoorOpenMaxSpeed)
			
			// Increase the actual ratio using the speed
			fCasinoDailyDoor_CurrentOpenRatio -= GET_FRAME_TIME() * fCasinoDailyDoor_CurrentSpeed
			fCasinoDailyDoor_CurrentOpenRatio = CLAMP(fCasinoDailyDoor_CurrentOpenRatio, 0.0, cfCasinoDailyDoor_DoorOpenMaxRatio)
			
			// Apply our vars to the door
			PROCESS_CHANGES_TO_CASINO_DOOR(iCasinoDailyDoorHash, -fCasinoDailyDoor_CurrentOpenRatio, bNetworked)
			
			IF fCasinoDailyDoor_CurrentOpenRatio = 0.0
				IF NETWORK_HAS_CONTROL_OF_DOOR(iCasinoDailyDoorHash)
					PLAY_SOUND_FROM_COORD(-1,"cash_room_door_close_end", g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].vPos, "dlc_ch_heist_finale_sounds", TRUE)
				ENDIF
				SET_DAILY_CASH_ROOM_STATE(CASINO_DAILY_CASH_STATE__WAITING)
			ENDIF
		BREAK
		
		
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
	CASINO_DAILY_DOOR_DEBUG(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].vPos)
	#ENDIF
ENDPROC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: NORMAL/REAL DOORS !
//
//************************************************************************************************************************************************************

FUNC INT GET_DOOR_NUM_AT_LOCATION(VECTOR vLoc)
	
	INT iDoorNum = -1
	INT i, iDoorHash
	
	FOR i = 0 TO (FMMC_MAX_NUM_DOORS-1)
		IF iDoorID[i] != -1
			GET_NEW_DOOR_HASH(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[i].mnDoorModel, g_FMMC_STRUCT_ENTITIES.sSelectedDoor[i].vPos,iDoorHash, iCachedDoorHashes[i])
			IF IS_DOOR_REGISTERED_WITH_SYSTEM(iDoorHash)
     			IF (ARE_VECTORS_ALMOST_EQUAL(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[i].vPos,vLoc))
					iDoorNum = i
					i = FMMC_MAX_NUM_DOORS
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN iDoorNum
	
ENDFUNC

PROC SET_REAL_DOOR_RATIO(INT iDoorHash,FLOAT fratio, bool network)
	PRINTLN("SET_REAL_DOOR_RATIO - Setting door with hash ", iDoorHash, " To ratio ", fratio, " networked ", network)
	DOOR_SYSTEM_SET_OPEN_RATIO(idoorHash, fratio, network, TRUE)
	DOOR_SYSTEM_SET_DOOR_STATE(idoorHash, DOORSTATE_FORCE_LOCKED_THIS_FRAME, network, TRUE)
ENDPROC

FUNC BOOL SHOULD_DOOR_BE_LOCKED_DUE_TO_LINKED_OBJECT_NOT_EXISTING(INT iDoor)
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iBitSet, ciDoorBS_LEGACY_LockIfNoLinkedObj)
		IF NOT IS_BIT_SET(iDoorHasLinkedEntityBS, iDoor)
			PRINTLN("[RCC MISSION][MCDoors] SHOULD_DOOR_BE_LOCKED_DUE_TO_LINKED_OBJECT_NOT_EXISTING - Returning TRUE ", iDoor, " due to ciDoorBS_LEGACY_LockIfNoLinkedObj")
			RETURN TRUE
		ELSE
			PRINTLN("[RCC MISSION][MCDoors] SHOULD_DOOR_BE_LOCKED_DUE_TO_LINKED_OBJECT_NOT_EXISTING - Door ", iDoor, " has at least one linked object")
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_DOOR_SWING_FREE_DUE_TO_LINKED_OBJECT_NOT_EXISTING(INT iDoor)
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iBitSet, ciDoorBS_LEGACY_UnlockIfNoLinkedObj)
		IF NOT IS_BIT_SET(iDoorHasLinkedEntityBS, iDoor)
			PRINTLN("[RCC MISSION][MCDoors] SHOULD_DOOR_SWING_FREE_DUE_TO_LINKED_OBJECT_NOT_EXISTING - Returning TRUE ", iDoor, " due to ciDoorBS_LEGACY_LockIfNoLinkedObj")
			RETURN TRUE
		ELSE
			PRINTLN("[RCC MISSION][MCDoors] SHOULD_DOOR_SWING_FREE_DUE_TO_LINKED_OBJECT_NOT_EXISTING - Door ", iDoor, " has at least one linked object")
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_DOOR_HAVE_OPEN_RATIO_DUE_TO_LINKED_OBJECT_NOT_EXISTING(INT iDoor)
	IF g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].fOpenRatioIfNoLinkedObj > 0.0
		IF NOT IS_BIT_SET(iDoorHasLinkedEntityBS, iDoor)
			PRINTLN("[RCC MISSION][MCDoors] SHOULD_DOOR_HAVE_OPEN_RATIO_DUE_TO_LINKED_OBJECT_NOT_EXISTING - Returning TRUE ", iDoor, " due to ciDoorBS_LEGACY_LockIfNoLinkedObj")
			RETURN TRUE
		ELSE
			PRINTLN("[RCC MISSION][MCDoors] SHOULD_DOOR_HAVE_OPEN_RATIO_DUE_TO_LINKED_OBJECT_NOT_EXISTING - Door ", iDoor, " has at least one linked object")
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL GET_DOOR_OPEN_RATIO( INT iDoor, MODEL_NAMES mnDoor, FLOAT &fOpenRatio)
	INT iDoorModel = ENUM_TO_INT(mnDoor)
	
	IF SHOULD_DOOR_HAVE_OPEN_RATIO_DUE_TO_LINKED_OBJECT_NOT_EXISTING(iDoor)
		fOpenRatio = g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].fOpenRatioIfNoLinkedObj
		RETURN TRUE
	ENDIF
	
	SWITCH iDoorModel
		
		CASE ciFMMC_DOOR_PRISON_SLIDING_GATE2 //hash of model name enum_to_int(prop_gate_prison_01)
			fOpenRatio -= 0.02
			
			PRINTLN("STRUCT IS ", g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].sDoorConfigs[ciDOOR_CONFIG__ALT].fOpenRatio, " - FLOAT IS", fOpenRatio)
			IF g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].sDoorConfigs[ciDOOR_CONFIG__ALT].fOpenRatio > fOpenRatio
				fOpenRatio = g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].sDoorConfigs[ciDOOR_CONFIG__ALT].fOpenRatio
			ENDIF
			
			RETURN TRUE
	
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE: Returns TRUE IF this door needs to be updated from an object (for doors that unlock when a minigame object is passed/interacted/destroyed etc.)
FUNC BOOL SHOULD_DOOR_UPDATE_FROM_OBJECT(INT iDoor)

	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iBitSet, ciDoorBS_LEGACY_UpdateFromObject)
		RETURN TRUE
	ENDIF

	RETURN FALSE
	
ENDFUNC

//// DIRECTIONAL DOOR LEGACY UTILITY FUNCTIONS ////
FUNC FLOAT LEGACY_GET_DOOR_DIRECTIONAL_LOCK_UNLOCK_POSITION_SIZE(FMMCselectedDoorStruct& sDoor)
	FLOAT fSize = 1.0
	IF sDoor.mnDoorModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_lobay_gate01"))
		fSize = 5.5
	ELIF sDoor.mnDoorModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_tunnel_door_01_r"))
	OR sDoor.mnDoorModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_tunnel_door_01_l"))
		fSize = 1.75
	ENDIF
	
	IF sDoor.sDoorConfigs[ciDOOR_CONFIG__STANDARD].fAutomaticDistance > 0
		fSize *= sDoor.sDoorConfigs[ciDOOR_CONFIG__STANDARD].fAutomaticDistance * 0.5
	ENDIF
	
	RETURN fSize
ENDFUNC

FUNC FMMC_DIRECTIONAL_DOOR_LOCK_TYPE LEGACY_GET_DOOR_CURRENT_DIRECTIONAL_LOCK_TYPE(FMMCselectedDoorStruct& sDoor, BOOL bUseUpdateSettings)
	
	IF bUseUpdateSettings
	AND sDoor.sDoorConfigs[ciDOOR_CONFIG__ALT].eDirectionalLockType != FMMC_DIRECTIONAL_DOOR_LOCK_TYPE__NO_OVERRIDE
		RETURN sDoor.sDoorConfigs[ciDOOR_CONFIG__ALT].eDirectionalLockType
	ELSE
		RETURN sDoor.sDoorConfigs[ciDOOR_CONFIG__STANDARD].eDirectionalLockType
	ENDIF
	
	RETURN FMMC_DIRECTIONAL_DOOR_LOCK_TYPE__NONE
ENDFUNC

FUNC VECTOR LEGACY_GET_DOOR_CURRENT_DIRECTIONAL_LOCK_DIRECTION(FMMCselectedDoorStruct& sDoor, BOOL bUseUpdateSettings)
	
	IF bUseUpdateSettings
	AND sDoor.sDoorConfigs[ciDOOR_CONFIG__ALT].eDirectionalLockType != FMMC_DIRECTIONAL_DOOR_LOCK_TYPE__NO_OVERRIDE
		RETURN sDoor.sDoorConfigs[ciDOOR_CONFIG__ALT].vDoorUnlockDirection
	ELSE
		RETURN sDoor.sDoorConfigs[ciDOOR_CONFIG__STANDARD].vDoorUnlockDirection
	ENDIF
	
	RETURN <<0,0,0>>
ENDFUNC

FUNC VECTOR LEGACY_GET_DOOR_DIRECTIONAL_LOCK_RIGHT_DIRECTION(FMMCselectedDoorStruct& sDoor, FMMC_DIRECTIONAL_DOOR_LOCK_TYPE eLockType, BOOL bUseUpdateSettings)
	
	VECTOR vDir = <<1,0,0>>
	IF eLockType = FMMC_DIRECTIONAL_DOOR_LOCK_TYPE__FRONT
		vDir = CROSS_PRODUCT(LEGACY_GET_DOOR_CURRENT_DIRECTIONAL_LOCK_DIRECTION(sDoor, bUseUpdateSettings), <<0,0,1>>)
	ELIF eLockType = FMMC_DIRECTIONAL_DOOR_LOCK_TYPE__BACK
		vDir = CROSS_PRODUCT(-LEGACY_GET_DOOR_CURRENT_DIRECTIONAL_LOCK_DIRECTION(sDoor, bUseUpdateSettings), <<0,0,1>>)
	ENDIF
	
	IF IS_BIT_SET(sDoor.iBitSet, ciDoorBS_DoorShouldBeFlipped)
		vDir = -vDir
	ENDIF
	
	RETURN vDir
ENDFUNC

FUNC VECTOR LEGACY_GET_DOOR_MIDDLE(INT iDoorIndex, FMMCselectedDoorStruct& sDoor, BOOL bUseUpdateSettings)
	
	IF NOT IS_VECTOR_ZERO(vCachedDoorMiddlePositions[iDoorIndex])
		RETURN vCachedDoorMiddlePositions[iDoorIndex]
	ENDIF
	
	FLOAT fRightDistance = GET_DOOR_DIRECTIONAL_LOCK_UNLOCK_POSITION_RIGHT_OFFSET_SIZE_BASED_ON_MODEL(sDoor)
	VECTOR vRightVector = LEGACY_GET_DOOR_DIRECTIONAL_LOCK_RIGHT_DIRECTION(sDoor, LEGACY_GET_DOOR_CURRENT_DIRECTIONAL_LOCK_TYPE(sDoor, bUseUpdateSettings), bUseUpdateSettings)
	
	vCachedDoorMiddlePositions[iDoorIndex] = (sDoor.vPos + (vRightVector  * fRightDistance))
	RETURN vCachedDoorMiddlePositions[iDoorIndex]
ENDFUNC

FUNC VECTOR LEGACY_GET_DOOR_DIRECTIONAL_LOCK_UNLOCK_POSITION(INT iDoorIndex, FMMCselectedDoorStruct& sDoor, BOOL bUseUpdateSettings)

	IF NOT IS_VECTOR_ZERO(vCachedDoorUnlockPositions[iDoorIndex])
	AND NOT bUseUpdateSettings
		RETURN vCachedDoorUnlockPositions[iDoorIndex]
	ENDIF
	
	FLOAT fDistance = LEGACY_GET_DOOR_DIRECTIONAL_LOCK_UNLOCK_POSITION_SIZE(sDoor)

	VECTOR vUnlockDirection = LEGACY_GET_DOOR_CURRENT_DIRECTIONAL_LOCK_DIRECTION(sDoor, bUseUpdateSettings) * fDistance
	VECTOR vUnlockPosition = (LEGACY_GET_DOOR_MIDDLE(iDoorIndex, sDoor, bUseUpdateSettings) - vUnlockDirection)
	
	vUnlockPosition += GET_DOOR_DIRECTIONAL_LOCK_UNLOCK_POSITION_OFFSET_BASED_ON_MODEL(sDoor)
	vCachedDoorUnlockPositions[iDoorIndex] = vUnlockPosition
	
	RETURN vUnlockPosition
ENDFUNC

#IF IS_DEBUG_BUILD
PROC LEGACY_DRAW_FMMC_DIRECTIONAL_DOOR_LOCK_HUD(INT iDoorIndex, FMMCselectedDoorStruct& sDoor, BOOL bUseUpdateSettings)
	
	FMMC_DIRECTIONAL_DOOR_LOCK_TYPE eLockType = LEGACY_GET_DOOR_CURRENT_DIRECTIONAL_LOCK_TYPE(sDoor, bUseUpdateSettings)
	
	IF eLockType = FMMC_DIRECTIONAL_DOOR_LOCK_TYPE__NONE
	OR eLockType = FMMC_DIRECTIONAL_DOOR_LOCK_TYPE__NO_OVERRIDE
		EXIT
	ENDIF
	
	VECTOR vArrowPos = LEGACY_GET_DOOR_DIRECTIONAL_LOCK_UNLOCK_POSITION(iDoorIndex, sDoor, bUseUpdateSettings)
	VECTOR vArrowDir = LEGACY_GET_DOOR_CURRENT_DIRECTIONAL_LOCK_DIRECTION(sDoor, bUseUpdateSettings)
	FLOAT fArrowHeading = GET_HEADING_FROM_VECTOR_2D(vArrowDir.x, vArrowDir.y)
	
	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
	
	IF bUseUpdateSettings
		DRAW_DEBUG_ARROW(vArrowPos, fArrowHeading, 0.0, 1.0, 0, 0, 255)
	ELSE
		DRAW_DEBUG_ARROW(vArrowPos, fArrowHeading, 0.0, 1.0)
	ENDIF
	DRAW_DEBUG_SPHERE(vArrowPos, LEGACY_GET_DOOR_DIRECTIONAL_LOCK_UNLOCK_POSITION_SIZE(sDoor), 255, 0, 0, 50)
	
	DRAW_DEBUG_LINE(vArrowPos, vArrowPos + LEGACY_GET_DOOR_DIRECTIONAL_LOCK_RIGHT_DIRECTION(sDoor, eLockType, bUseUpdateSettings), 255, 0, 0)
	DRAW_DEBUG_LINE(vArrowPos, vArrowPos + LEGACY_GET_DOOR_CURRENT_DIRECTIONAL_LOCK_DIRECTION(sDoor, bUseUpdateSettings), 0, 0, 255)
	
ENDPROC
#ENDIF

/// PURPOSE: returns true IF the relevant conditions have been met to allow the door to update.   
FUNC BOOL ALLOW_DOOR_TO_UPDATE(int iDoor, int irule, BOOL bStrict = FALSE)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iBitSet, ciDoorBS_LEGACY_OnlyUpdateFromPreviousMission)
	AND IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_DoorsUsedAltConfigTracking)
		IF g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iContinuityId > -1
		AND IS_LONG_BIT_SET(MC_serverBD_1.sLEGACYMissionContinuityVars.iDoorsUseAltConfigBitset, g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iContinuityId)
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF SHOULD_DOOR_UPDATE_FROM_OBJECT(iDoor)
		IF IS_BIT_SET(MC_serverBD_1.iDoorUpdateOffRuleBS, iDoor)
			RETURN TRUE
		ENDIF
		
		IF IS_ANY_PLAYER_HOLDING_DOOR_BUTTON(iDoor)
			RETURN TRUE
		ENDIF
		
		IF IS_TIMED_DOOR_UPDATE_TIMER_RUNNING(iDoor)
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF NOT bStrict
		IF IS_BIT_SET(iDirectionalDoorShouldUpdateBS, iDoor)
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF ((g_FMMC_STRUCT_ENTITIES.sSelectedDoor[idoor].iupdaterule > 0) or (g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].mnDoorModel = v_ilev_gb_teldr))
	AND irule >= g_FMMC_STRUCT_ENTITIES.sSelectedDoor[idoor].iupdaterule
	
		IF DOOR_FUNCTIONALITY_CHECKS(iDoor)
		
			RETURN TRUE 
		
		ENDIF 
		
	ENDIF 
	
	RETURN FALSE 
	
ENDFUNC

FUNC FMMC_DIRECTIONAL_DOOR_LOCK_TYPE GET_DOOR_DIRECTIONAL_LOCK_TYPE(INT iDoor)
	
	IF g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iUpdateTeam > -1
		IF ALLOW_DOOR_TO_UPDATE(iDoor, MC_ServerBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iUpdateTeam])
			IF g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].sDoorConfigs[ciDOOR_CONFIG__ALT].eDirectionalLockType != FMMC_DIRECTIONAL_DOOR_LOCK_TYPE__NO_OVERRIDE
				#IF IS_DEBUG_BUILD
				IF bDirectionalDoorDebug
					DRAW_DEBUG_TEXT("Using Update Dir", g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].vPos + <<1.0, 0.0, 0.0>>)
				ENDIF
				#ENDIF
				
				RETURN g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].sDoorConfigs[ciDOOR_CONFIG__ALT].eDirectionalLockType
			ELSE
				#IF IS_DEBUG_BUILD
				IF bDirectionalDoorDebug
					DRAW_DEBUG_TEXT("Using Update Dir / No override", g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].vPos + <<1.0, 0.0, 0.0>>)
				ENDIF
				#ENDIF
				
				RETURN g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].sDoorConfigs[ciDOOR_CONFIG__STANDARD].eDirectionalLockType
			ENDIF
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].sDoorConfigs[ciDOOR_CONFIG__STANDARD].eDirectionalLockType != FMMC_DIRECTIONAL_DOOR_LOCK_TYPE__NONE
		#IF IS_DEBUG_BUILD
		IF bDirectionalDoorDebug
			DRAW_DEBUG_TEXT("Using Standard Dir", g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].vPos + <<1.0, 0.0, 0.0>>)
		ENDIF
		#ENDIF
		
		RETURN g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].sDoorConfigs[ciDOOR_CONFIG__STANDARD].eDirectionalLockType
	ENDIF
	
	RETURN FMMC_DIRECTIONAL_DOOR_LOCK_TYPE__NONE
ENDFUNC

/// PURPOSE: Returns TRUE IF this door needs to be updated every frame (for doors like the HL barriers that reset its values IF the player is far enough)
FUNC BOOL SHOULD_DOOR_UPDATE_EVERY_FRAME(INT iDoor)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iBitSet, ciDoorBS_HideDoorDuringCutscene)
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iBitSet, ciDoorBS_LEGACY_UPDATE_EVERY_FRAME)
		RETURN TRUE
	ENDIF
	
	IF GET_DOOR_DIRECTIONAL_LOCK_TYPE(iDoor) != FMMC_DIRECTIONAL_DOOR_LOCK_TYPE__NONE
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iBitSet, ciDoorBS_LEGACY_TimedUpdate)
	AND IS_TIMED_DOOR_UPDATE_TIMER_RUNNING(iDoor)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE: Returns TRUE IF there is at least one door that needs to be updated every frame (for doors like the HL barriers that reset its values IF the player is far enough)
FUNC BOOL SHOULD_CHECK_ANY_DOORS_EVERY_FRAME()
	
	INT iDoor
	
	FOR iDoor = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfDoors - 1
		IF SHOULD_DOOR_UPDATE_EVERY_FRAME(iDoor)
		OR SHOULD_DOOR_UPDATE_FROM_OBJECT(iDoor)
			RETURN TRUE
		ENDIF
	ENDFOR
	
	RETURN FALSE

ENDFUNC

FUNC BOOL CAN_ACCESS_CODE_DOOR(INT iDoorHash)
	RETURN IS_DOOR_REGISTERED_WITH_SYSTEM(iDoorHash)
	AND (NOT NETWORK_IS_DOOR_NETWORKED(iDoorHash) OR NETWORK_HAS_CONTROL_OF_DOOR(iDoorHash))
ENDFUNC

FUNC BOOL SHOULD_DOOR_OPENING_BE_DELAYED(INT iDoor)
	IF (g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].mnDoorModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_tunnel_door_01_l"))
	OR g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].mnDoorModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_tunnel_door_01_r")))
		IF g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].vPos.z > -73
		AND g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].vPos.z < -69
			IF NOT HAS_NET_TIMER_EXPIRED(stMantrapDoorDelayTimer, ciMantrapDoorDelayTime)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: Updates the values of this door (to be called every frame, for doors like the HL barriers that reset its values IF the player is far enough)
PROC UPDATE_DOOR_STATE_EVERY_FRAME(INT iTeam, INT iDoor, INT iDoorHash, BOOL &bNotReady, bool network)
		
	CREATE_DOOR_COVER_BLOCKING_ZONE(iDoor)
	
	FLOAT fAutomaticRate, fAutomaticDistance, fOpenRatio
	BOOL bSwingFree
	
	IF g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iUpdateTeam = -1
	OR (g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iUpdateTeam = iTeam AND g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iUpdateRule > MC_ServerBD_4.iCurrentHighestPriority[iTeam])
		fAutomaticRate = g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].sDoorConfigs[ciDOOR_CONFIG__STANDARD].fAutomaticRate
		fAutomaticDistance = g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].sDoorConfigs[ciDOOR_CONFIG__STANDARD].fAutomaticDistance
		fOpenRatio = g_FMMC_STRUCT_ENTITIES.sSelectedDoor[ iDoor ].sDoorConfigs[ciDOOR_CONFIG__STANDARD].fOpenRatio
		bSwingFree = g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].bSwingFree
	ELIF (g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iUpdateTeam = iTeam AND g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iUpdateRule <= MC_ServerBD_4.iCurrentHighestPriority[iTeam])
		fAutomaticRate = g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].sDoorConfigs[ciDOOR_CONFIG__ALT].fAutomaticRate
		fAutomaticDistance = g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].sDoorConfigs[ciDOOR_CONFIG__ALT].fAutomaticDistance
		fOpenRatio = g_FMMC_STRUCT_ENTITIES.sSelectedDoor[ iDoor ].sDoorConfigs[ciDOOR_CONFIG__ALT].fOpenRatio
		bSwingFree = g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].bSwingFreeUpdate
	ELSE
		EXIT
	ENDIF
	
	IF GET_DOOR_DIRECTIONAL_LOCK_TYPE(iDoor) = FMMC_DIRECTIONAL_DOOR_LOCK_TYPE__NONE
		IF fAutomaticRate != 0
			DOOR_SYSTEM_SET_AUTOMATIC_RATE(iDoorHash, fAutomaticRate, network)
			PRINTLN("[RCC MISSION] DOOR_SYSTEM_SET_AUTOMATIC_RATE door: ",iDoor," UPDATE_AUTOMATIC_RATE: ", fAutomaticRate," at pos: ",g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].vPos)
		ENDIF
		
		IF fAutomaticDistance != 0
			DOOR_SYSTEM_SET_AUTOMATIC_DISTANCE(iDoorHash, fAutomaticDistance, network)
			PRINTLN("[RCC MISSION] DOOR_SYSTEM_SET_AUTOMATIC_DISTANCE door: ",iDoor," UPDATE_AUTOMATIC_DISTANCE: ", fAutomaticDistance," at pos: ",g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].vPos)
		ENDIF
	ENDIF
	
	IF MC_ServerBD_4.iCurrentHighestPriority[iTeam] > -1
	AND MC_ServerBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
		IF g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].bSwingFreeUpdate
		AND ALLOW_DOOR_TO_UPDATE(iDoor, MC_ServerBD_4.iCurrentHighestPriority[iTeam])
			bSwingFree = TRUE
			PRINTLN("[RCC MISSION][MCDoors] UPDATE_DOOR_STATE - Setting door to swing free due to bSwingFreeUpdate & ALLOW_DOOR_TO_UPDATE")
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iLinkedElevator > -1
		IF MC_serverBD_1.eElevatorState[g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iLinkedElevator] != FMMC_ELEVATOR_SERVER_STATE_READY
			bSwingFree = FALSE
			PRINTLN("[RCC MISSION][MCDoors][ELEVATORS] UPDATE_DOOR_STATE - Setting door to not swing free because it's an elevator that isn't ready | g_FMMC_STRUCT_ENTITIES.sSelectedDoor[", iDoor, "].iLinkedElevator: ", g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iLinkedElevator)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(MC_serverBD_4.iHostDirectionalDoorUnlockedBS, iDoor)
		bSwingFree = TRUE
		PRINTLN("[RCC MISSION][MCDoors][ELEVATORS] UPDATE_DOOR_STATE - Setting door to swing free due to iHostDirectionalDoorUnlockedBS")
	ENDIF
	
	IF SHOULD_DOOR_BE_LOCKED_DUE_TO_LINKED_OBJECT_NOT_EXISTING(iDoor)
		bSwingFree = FALSE
		PRINTLN("[RCC MISSION][MCDoors] UPDATE_DOOR_STATE - Locking door ", iDoor, " due to SHOULD_DOOR_BE_LOCKED_DUE_TO_LINKED_OBJECT_NOT_EXISTING")
	ENDIF
	IF SHOULD_DOOR_SWING_FREE_DUE_TO_LINKED_OBJECT_NOT_EXISTING(iDoor)
		bSwingFree = TRUE
		PRINTLN("[RCC MISSION][MCDoors] UPDATE_DOOR_STATE - Setting door ", iDoor, " to swing free due to SHOULD_DOOR_SWING_FREE_DUE_TO_LINKED_OBJECT_NOT_EXISTING")
	ENDIF
	IF SHOULD_DOOR_HAVE_OPEN_RATIO_DUE_TO_LINKED_OBJECT_NOT_EXISTING(iDoor)
		bSwingFree = FALSE
		PRINTLN("[RCC MISSION][MCDoors] UPDATE_DOOR_STATE - Setting door ", iDoor, " NOT to swing free due to SHOULD_DOOR_HAVE_OPEN_RATIO_DUE_TO_LINKED_OBJECT_NOT_EXISTING")
	ENDIF
	
	IF SHOULD_DOOR_OPENING_BE_DELAYED(iDoor)
		bSwingFree = FALSE
		PRINTLN("[RCC MISSION][MCDoors] UPDATE_DOOR_STATE - Setting door ", iDoor, " NOT to swing free due to SHOULD_DOOR_OPENING_BE_DELAYED")
	ENDIF
	
	IF bSwingFree
		DOOR_SYSTEM_SET_DOOR_STATE(iDoorHash, DOORSTATE_UNLOCKED, network)
		PRINTLN("[RCC MISSION] UPDATE_DOOR_STATE - DOOR set to swing free: ",iDoor," at pos: ",g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].vPos)
	ELSE
		IF GET_DOOR_OPEN_RATIO(iDoor, g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].mnDoorModel, fGateOpenRatio) //check if this door has an open ratio
			SET_REAL_DOOR_RATIO(iDoorHash, fGateOpenRatio, network)
			PRINTLN("[RCC MISSION] UPDATE_DOOR_STATE - setting normal door: ",iDoor," to ratio (fGateOpenRatio) : ", fGateOpenRatio)
			IF fGateOpenRatio != fOpenRatio
				bNotReady = TRUE //only set to ready once we are open
			ENDIF
		ELSE
			SET_REAL_DOOR_RATIO(iDoorHash, fOpenRatio, network)
			PRINTLN("[RCC MISSION] UPDATE_DOOR_STATE FALSE TARGET RATIO MET - setting normal door: ",iDoor," to ratio: ", fOpenRatio)
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_DOOR_FOR_UPDATE_DOOR_STATE(INT iDoor, INT iDoorHash, BOOL network, INT iteam, BOOL &bNotReady)

	IF SHOULD_DOOR_UPDATE_EVERY_FRAME(iDoor)
		UPDATE_DOOR_STATE_EVERY_FRAME(iTeam, iDoor, iDoorHash, bNotReady, network)
	ELSE
	
		BOOL bSwingFree = FALSE
		
		IF g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].bSwingFreeUpdate
		OR IS_BIT_SET(MC_serverBD_4.iHostDirectionalDoorUnlockedBS, iDoor)
			bSwingFree = TRUE
		ENDIF
		
		IF g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].sDoorConfigs[ciDOOR_CONFIG__ALT].fAutomaticRate != 0
			DOOR_SYSTEM_SET_AUTOMATIC_RATE(iDoorHash, g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].sDoorConfigs[ciDOOR_CONFIG__ALT].fAutomaticRate, network)
			PRINTLN("[RCC MISSION] UPDATE_DOOR_STATE DOOR_SYSTEM_SET_AUTOMATIC_RATE door: ",iDoor," UPDATE_AUTOMATIC_RATE: ",g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].sDoorConfigs[ciDOOR_CONFIG__ALT].fAutomaticRate," at pos: ",g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].vPos)
		ENDIF
		
		IF g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].sDoorConfigs[ciDOOR_CONFIG__ALT].fAutomaticDistance != 0
			DOOR_SYSTEM_SET_AUTOMATIC_DISTANCE(iDoorHash, g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].sDoorConfigs[ciDOOR_CONFIG__ALT].fAutomaticDistance, network)
			PRINTLN("[RCC MISSION] UPDATE_DOOR_STATE DOOR_SYSTEM_SET_AUTOMATIC_DISTANCE door: ",iDoor," UPDATE_AUTOMATIC_DISTANCE: ",g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].sDoorConfigs[ciDOOR_CONFIG__ALT].fAutomaticDistance," at pos: ",g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].vPos)
		ENDIF
		
		IF SHOULD_DOOR_BE_LOCKED_DUE_TO_LINKED_OBJECT_NOT_EXISTING(iDoor)
			bSwingFree = FALSE
			PRINTLN("[RCC MISSION][MCDoors] PROCESS_DOOR_FOR_UPDATE_DOOR_STATE - Locking door ", iDoor, " due to SHOULD_DOOR_BE_LOCKED_DUE_TO_LINKED_OBJECT_NOT_EXISTING")
		ENDIF
		IF SHOULD_DOOR_SWING_FREE_DUE_TO_LINKED_OBJECT_NOT_EXISTING(iDoor)
			bSwingFree = TRUE
			PRINTLN("[RCC MISSION][MCDoors] PROCESS_DOOR_FOR_UPDATE_DOOR_STATE - Setting door ", iDoor, " to swing free due to SHOULD_DOOR_SWING_FREE_DUE_TO_LINKED_OBJECT_NOT_EXISTING")
		ENDIF
		IF SHOULD_DOOR_HAVE_OPEN_RATIO_DUE_TO_LINKED_OBJECT_NOT_EXISTING(iDoor)
			bSwingFree = FALSE
			PRINTLN("[RCC MISSION][MCDoors] PROCESS_DOOR_FOR_UPDATE_DOOR_STATE - Setting door ", iDoor, " NOT to swing free due to SHOULD_DOOR_HAVE_OPEN_RATIO_DUE_TO_LINKED_OBJECT_NOT_EXISTING")
		ENDIF
		
		IF SHOULD_DOOR_OPENING_BE_DELAYED(iDoor)
			bSwingFree = FALSE
			PRINTLN("[RCC MISSION][MCDoors] PROCESS_DOOR_FOR_UPDATE_DOOR_STATE - Setting door ", iDoor, " NOT to swing free due to SHOULD_DOOR_OPENING_BE_DELAYED")
		ENDIF
		
		IF bSwingFree
			DOOR_SYSTEM_SET_DOOR_STATE(iDoorHash, DOORSTATE_UNLOCKED, network)
			PRINTLN("[RCC MISSION] PROCESS_DOOR_FOR_UPDATE_DOOR_STATE - DOOR set to swing free: ",iDoor," at pos: ",g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].vPos)
		ELSE
			IF GET_DOOR_OPEN_RATIO( iDoor, g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].mnDoorModel, fGateOpenRatio ) //check IF this door has a set open ratio
				SET_REAL_DOOR_RATIO( iDoorHash, fGateOpenRatio, network)
				PRINTLN("[RCC MISSION] UPDATE_DOOR_STATE - setting normal door: ",iDoor," to ratio (fGateOpenRatio) : ",fGateOpenRatio)
				IF fGateOpenRatio != g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].sDoorConfigs[ciDOOR_CONFIG__ALT].fOpenRatio
					bNotReady = TRUE
				ENDIF
			ELSE
				SET_REAL_DOOR_RATIO(iDoorHash,g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].sDoorConfigs[ciDOOR_CONFIG__ALT].fOpenRatio, network)
				PRINTLN("[RCC MISSION] UPDATE_DOOR_STATE TARGET RATIO MET - setting normal door: ",iDoor," to ratio: ",g_FMMC_STRUCT_ENTITIES.sSelectedDoor[ iDoor ].sDoorConfigs[ciDOOR_CONFIG__ALT].fOpenRatio)
			ENDIF
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE: sets the door using the relevant code commands DOOR_SYSTEM_SET_DOOR_STATE() etc
PROC PROCESS_DOOR_FOR_SET_DOOR_START_STATE(int iDoor, int iDoorHash, bool network)
	
	FLOAT fOpenRatio = g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].sDoorConfigs[ciDOOR_CONFIG__STANDARD].fOpenRatio
	
	IF g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].sDoorConfigs[ciDOOR_CONFIG__STANDARD].fAutomaticRate != 0
		DOOR_SYSTEM_SET_AUTOMATIC_RATE(iDoorHash, g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].sDoorConfigs[ciDOOR_CONFIG__STANDARD].fAutomaticRate, network)
		PRINTLN("[RCC MISSION] SET_DOOR_START_STATE DOOR_SYSTEM_SET_AUTOMATIC_RATE door: ",iDoor," AUTOMATIC_RATE: ",g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].sDoorConfigs[ciDOOR_CONFIG__STANDARD].fAutomaticRate," at pos: ",g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].vPos)
	ENDIF 
	
	IF g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].sDoorConfigs[ciDOOR_CONFIG__STANDARD].fAutomaticDistance != 0
		DOOR_SYSTEM_SET_AUTOMATIC_DISTANCE(iDoorHash, g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].sDoorConfigs[ciDOOR_CONFIG__STANDARD].fAutomaticDistance, network)
		PRINTLN("[RCC MISSION] SET_DOOR_START_STATE DOOR_SYSTEM_SET_AUTOMATIC_DISTANCE door: ",iDoor," AUTOMATIC_DISTANCE: ",g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].sDoorConfigs[ciDOOR_CONFIG__STANDARD].fAutomaticDistance," at pos: ",g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].vPos)
	ENDIF
	
	BOOL bSwingFree = g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].bSwingFree
	IF SHOULD_DOOR_BE_LOCKED_DUE_TO_LINKED_OBJECT_NOT_EXISTING(iDoor)
		bSwingFree = FALSE
		PRINTLN("[RCC MISSION][MCDoors] PROCESS_DOOR_FOR_SET_DOOR_START_STATE - Locking door ", iDoor, " due to SHOULD_DOOR_BE_LOCKED_DUE_TO_LINKED_OBJECT_NOT_EXISTING")
	ENDIF
	IF SHOULD_DOOR_SWING_FREE_DUE_TO_LINKED_OBJECT_NOT_EXISTING(iDoor)
		bSwingFree = TRUE
		PRINTLN("[RCC MISSION][MCDoors] PROCESS_DOOR_FOR_SET_DOOR_START_STATE - Setting door ", iDoor, " to swing free due to SHOULD_DOOR_SWING_FREE_DUE_TO_LINKED_OBJECT_NOT_EXISTING")
	ENDIF
	IF SHOULD_DOOR_HAVE_OPEN_RATIO_DUE_TO_LINKED_OBJECT_NOT_EXISTING(iDoor)
		bSwingFree = FALSE
		fOpenRatio = g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].fOpenRatioIfNoLinkedObj
		PRINTLN("[RCC MISSION][MCDoors] PROCESS_DOOR_FOR_SET_DOOR_START_STATE - Setting door ", iDoor, " NOT to swing free due to SHOULD_DOOR_HAVE_OPEN_RATIO_DUE_TO_LINKED_OBJECT_NOT_EXISTING & setting fOpenRatio to ", fOpenRatio)
	ENDIF
		
	IF bSwingFree
		DOOR_SYSTEM_SET_DOOR_STATE(iDoorHash, DOORSTATE_UNLOCKED, network)
		PRINTLN("[RCC MISSION] SET_DOOR_START_STATE DOOR set to swing free: ",iDoor," at pos: ",g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].vPos)
	ELSE
		SET_REAL_DOOR_RATIO(iDoorHash, fOpenRatio, network)
		PRINTLN("[RCC MISSION] SET_DOOR_START_STATE SET_REAL_DOOR_RATIO: ",iDoor," at pos: ",g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].vPos)
	ENDIF
	
ENDPROC

// DIRECTIONAL LOCKS
FUNC BOOL IS_DIRECTIONAL_DOOR_READY_TO_RELOCK(INT iDoor, INT iDoorHash)

	UNUSED_PARAMETER(iDoor)
	IF NOT IS_DOOR_CLOSED(iDoorHash)
		RETURN FALSE
	ENDIF

	RETURN TRUE
ENDFUNC

FUNC FLOAT GET_AUTOMATIC_DISTANCE_FOR_LOCKED_DIRECTIONAL_DOOR(INT iDoor) 
	
	IF g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].mnDoorModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_lobay_gate01"))
		RETURN 2.0
	ENDIF
	
	RETURN 1.0
ENDFUNC

FUNC BOOL SHOULD_ALLOW_DIRECTIONAL_DOOR_FOR_ELEVATOR(INT iDoor)

	IF g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iLinkedElevator = -1
		RETURN TRUE
	ENDIF
	
	IF MC_serverBD_1.eElevatorState[g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iLinkedElevator] = FMMC_ELEVATOR_SERVER_STATE_READY
	OR MC_serverBD_1.eElevatorState[g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iLinkedElevator] = FMMC_ELEVATOR_SERVER_STATE_DISABLED
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_PED_VALID_TO_UNLOCK_DIRECTIONAL_DOOR(PED_INDEX tempPed, VECTOR vDoorMiddle)
	
	IF NOT IS_ENTITY_ALIVE(tempPed)
		RETURN FALSE
	ENDIF
	
	VECTOR vPedVelocity = GET_ENTITY_VELOCITY(tempPed)
	
	IF VMAG2(vPedVelocity) >= 1.0
		VECTOR vPedDirection = NORMALISE_VECTOR(vPedVelocity)
		VECTOR vDirectionToDoorMiddle = NORMALISE_VECTOR(vDoorMiddle - GET_ENTITY_COORDS(tempPed, FALSE))
		FLOAT fVelocityDOT = DOT_PRODUCT(vPedDirection, vDirectionToDoorMiddle)

		IF fVelocityDOT < 0.65
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC PROCESS_PEDS_IN_DIRECTIONAL_DOOR(INT iDoor, VECTOR vUnlockPosition, FLOAT fUnlockDistance, BOOL bUseAltSettings)
	IF iStaggeredIterator < g_FMMC_STRUCT_ENTITIES.iNumberOfPeds
		BOOL bPedInArea = FALSE
		
		IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[iStaggeredIterator])
			PED_INDEX tempPed = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iStaggeredIterator])
			
			IF IS_PED_VALID_TO_UNLOCK_DIRECTIONAL_DOOR(tempPed, LEGACY_GET_DOOR_MIDDLE(iDoor, g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor], bUseAltSettings))
				IF VDIST2(GET_ENTITY_COORDS(tempPed, FALSE), vUnlockPosition) < (fUnlockDistance * fUnlockDistance)
					bPedInArea = TRUE
				ENDIF
			ENDIF
		ENDIF
		
		IF bPedInArea
			IF NOT IS_BIT_SET(iPedsInDirectionalDoorsBS[GET_LONG_BITSET_BIT(iDoor)][GET_LONG_BITSET_INDEX(iStaggeredIterator)], GET_LONG_BITSET_BIT(iStaggeredIterator))
				SET_BIT(iPedsInDirectionalDoorsBS[GET_LONG_BITSET_BIT(iDoor)][GET_LONG_BITSET_INDEX(iStaggeredIterator)], GET_LONG_BITSET_BIT(iStaggeredIterator))
				PRINTLN("[DirDoorLock] SETTING ", iStaggeredIterator, " in iPedsInDirectionalDoorsBS for door ", iDoor)
			ENDIF
		ELSE
			IF IS_BIT_SET(iPedsInDirectionalDoorsBS[GET_LONG_BITSET_BIT(iDoor)][GET_LONG_BITSET_INDEX(iStaggeredIterator)], GET_LONG_BITSET_BIT(iStaggeredIterator))
				CLEAR_BIT(iPedsInDirectionalDoorsBS[GET_LONG_BITSET_BIT(iDoor)][GET_LONG_BITSET_INDEX(iStaggeredIterator)], GET_LONG_BITSET_BIT(iStaggeredIterator))
				PRINTLN("[DirDoorLock] Clearing ", iStaggeredIterator, " in iPedsInDirectionalDoorsBS for door ", iDoor)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_DIRECTIONAL_DOOR_UNLOCK(INT iDoor, VECTOR vUnlockPosition, FLOAT fUnlockDistance)
	
	IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		IF VDIST2(GET_WORLD_POSITION_OF_ENTITY_BONE(LocalPlayerPed, 0), vUnlockPosition) < (fUnlockDistance * fUnlockDistance)
			RETURN TRUE
		ENDIF
	ELSE
		IF VDIST2(GET_ENTITY_COORDS(LocalPlayerPed, FALSE), vUnlockPosition) < (fUnlockDistance * fUnlockDistance)
			RETURN TRUE
		ENDIF
	ENDIF
	
	ENTITY_INDEX eiDrone = GET_LOCAL_DRONE_PROP_OBJ_INDEX()
	IF IS_ENTITY_ALIVE(eiDrone)
		IF VDIST2(GET_ENTITY_COORDS(eiDrone, FALSE), vUnlockPosition) < (fUnlockDistance * fUnlockDistance)
			RETURN TRUE
		ENDIF
	ENDIF
	
	INT iPedBS = 0
	FOR iPedBS = 0 TO ciFMMC_PED_BITSET_SIZE - 1
		IF iPedsInDirectionalDoorsBS[iDoor][iPedBS] != 0
			RETURN TRUE
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_FMMC_DIRECTIONAL_DOOR_LOCK(INT iDoor, INT iDoorHash, INT iTeam, INT iRule)
	UNUSED_PARAMETER(iDoorHash)
	UNUSED_PARAMETER(iTeam)
	
	BOOL bUseAltSettings = ALLOW_DOOR_TO_UPDATE(iDoor, iRule, TRUE)
	FMMC_DIRECTIONAL_DOOR_LOCK_TYPE eLockType = LEGACY_GET_DOOR_CURRENT_DIRECTIONAL_LOCK_TYPE(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor], bUseAltSettings)
	
	IF eLockType = FMMC_DIRECTIONAL_DOOR_LOCK_TYPE__NONE
		EXIT
	ENDIF
	
	IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(iDoorHash)
		PRINTLN("[DirDoorLock] PROCESS_FMMC_DIRECTIONAL_DOOR_LOCK exiting because door ", iDoorHash, " hasn't been registered!")
		EXIT
	ENDIF
	
	BOOL bNetwork = NETWORK_IS_DOOR_NETWORKED(iDoorHash)
	BOOL bCanSendEvent = NOT HAS_NET_TIMER_STARTED(stDirectionalDoorEventTimer)
	FLOAT fUnlockDistance = LEGACY_GET_DOOR_DIRECTIONAL_LOCK_UNLOCK_POSITION_SIZE(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor])
	
	#IF IS_DEBUG_BUILD
	IF bDirectionalDoorDebug
		TEXT_LABEL_63 tlDebugText3D = "D"
		tlDebugText3D += iDoor
		tlDebugText3D += " | H: "
		tlDebugText3D += iDoorHash
		tlDebugText3D += " | LUL: "
		tlDebugText3D += GET_STRING_FROM_BOOL(IS_BIT_SET(iDirectionalDoorLocallyUnlockedBS, iDoor))
		tlDebugText3D += " | HUL: "
		tlDebugText3D += GET_STRING_FROM_BOOL(IS_BIT_SET(MC_serverBD_4.iHostDirectionalDoorUnlockedBS, iDoor))
		tlDebugText3D += " | SU: "
		tlDebugText3D += GET_STRING_FROM_BOOL(IS_BIT_SET(iDirectionalDoorShouldUpdateBS, iDoor))
		DRAW_DEBUG_TEXT(tlDebugText3D, g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].vPos)
		
		tlDebugText3D = " | fDirectionalDoorAutoDistance: "
		tlDebugText3D += FLOAT_TO_STRING(fDirectionalDoorAutoDistance[iDoor])
		DRAW_DEBUG_TEXT(tlDebugText3D, g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].vPos + <<0.0, 0.0, -0.5>>)
		
		LEGACY_DRAW_FMMC_DIRECTIONAL_DOOR_LOCK_HUD(iDoor, g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor], bUseAltSettings)
	ENDIF
	#ENDIF
	
	IF IS_BIT_SET(iDirectionalDoorShouldUpdateBS, iDoor)
		CLEAR_BIT(iDirectionalDoorShouldUpdateBS, iDoor)
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(stDirectionalDoorEventTimer)
		IF HAS_NET_TIMER_EXPIRED(stDirectionalDoorEventTimer, ciDirectionalDoorEventTimerDuration)
			RESET_NET_TIMER(stDirectionalDoorEventTimer)
			PRINTLN("[DirDoorLock] PROCESS_FMMC_DIRECTIONAL_DOOR_LOCK | Resetting stDirectionalDoorEventTimer")
		ENDIF
	ENDIF
				
	/////
	VECTOR vUnlockPosition = LEGACY_GET_DOOR_DIRECTIONAL_LOCK_UNLOCK_POSITION(iDoor, g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor], bUseAltSettings)
	PROCESS_PEDS_IN_DIRECTIONAL_DOOR(iDoor, vUnlockPosition, fUnlockDistance, bUseAltSettings)
	
	IF SHOULD_ALLOW_DIRECTIONAL_DOOR_FOR_ELEVATOR(iDoor)
	AND SHOULD_DIRECTIONAL_DOOR_UNLOCK(iDoor, vUnlockPosition, fUnlockDistance)
		
		IF NOT IS_BIT_SET(iDirectionalDoorLocallyUnlockedBS, iDoor)
			IF bCanSendEvent
				BROADCAST_FMMC_DIRECTIONAL_DOOR_EVENT(iDoor, TRUE, FALSE, g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].sDoorConfigs[ciDOOR_CONFIG__STANDARD].fAutomaticDistance)
				REINIT_NET_TIMER(stDirectionalDoorEventTimer)
				SET_BIT(iDirectionalDoorLocallyUnlockedBS, iDoor)
			ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
		IF bDirectionalDoorDebug
			DRAW_DEBUG_SPHERE(vUnlockPosition, fUnlockDistance, 0, 255, 0, 100)
		ENDIF
		#ENDIF
	ELSE
		IF IS_BIT_SET(iDirectionalDoorLocallyUnlockedBS, iDoor)
			IF IS_DIRECTIONAL_DOOR_READY_TO_RELOCK(iDoor, iDoorHash)
				IF bCanSendEvent
					BROADCAST_FMMC_DIRECTIONAL_DOOR_EVENT(iDoor, FALSE, TRUE)
					REINIT_NET_TIMER(stDirectionalDoorEventTimer)
					CLEAR_BIT(iDirectionalDoorLocallyUnlockedBS, iDoor)
				ENDIF
			ENDIF
			
			IF fDirectionalDoorAutoDistance[iDoor] != GET_AUTOMATIC_DISTANCE_FOR_LOCKED_DIRECTIONAL_DOOR(iDoor)
				BROADCAST_FMMC_DIRECTIONAL_DOOR_EVENT(iDoor, FALSE, FALSE, GET_AUTOMATIC_DISTANCE_FOR_LOCKED_DIRECTIONAL_DOOR(iDoor))
				fDirectionalDoorAutoDistance[iDoor] = GET_AUTOMATIC_DISTANCE_FOR_LOCKED_DIRECTIONAL_DOOR(iDoor)
			ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
		IF bDirectionalDoorDebug
			DRAW_DEBUG_SPHERE(vUnlockPosition, fUnlockDistance, 255, 0, 0, 100)
		ENDIF
		#ENDIF
	ENDIF
	
	IF fDirectionalDoorAutoDistance[iDoor] != 0.0
		IF DOOR_SYSTEM_GET_AUTOMATIC_DISTANCE(iDoorHash) != fDirectionalDoorAutoDistance[iDoor]
			PRINTLN("[DirDoorLock] PROCESS_FMMC_DIRECTIONAL_DOOR_LOCK | Setting door automatic distance to ", fDirectionalDoorAutoDistance[iDoor])
			DOOR_SYSTEM_SET_AUTOMATIC_DISTANCE(iDoorHash, fDirectionalDoorAutoDistance[iDoor], bNetwork)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_HIDE_DOORS_FOR_CUTSCENES(INT iDoor)
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iBitSet, ciDoorBS_HideDoorDuringCutscene)
		
		#IF IS_DEBUG_BUILD
		IF GET_FRAME_COUNT() % 60 = 0
			PRINTLN("[RCC MISSION][CutsceneDoorHide] PROCESS_HIDE_DOORS_FOR_CUTSCENES - iDoor ", iDoor, " Should hide in cutscenes.")
		ENDIF
		#ENDIF
		
		IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].vPos)			
			IF IS_MODEL_VALID(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].mnDoorModel)
				IF (IS_CUTSCENE_PLAYING() OR IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet4, PBBOOL4_REACHED_CUT_PROCESS_SHOTS))
				AND g_bInMissionControllerCutscene
					IF NOT IS_BIT_SET(iDoorBS_HiddenForCutscene, iDoor)
						OBJECT_INDEX oiDoor = GET_WORLD_DOOR_OBJECT(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].mnDoorModel, g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].vPos)
						IF DOES_ENTITY_EXIST(oiDoor)
							PRINTLN("[RCC MISSION][CutsceneDoorHide] PROCESS_HIDE_DOORS_FOR_CUTSCENES - In a cutscene, Hiding iDoor ", iDoor)
							SET_ENTITY_ALPHA(oiDoor, 0, FALSE)							
						ELSE
							PRINTLN("[RCC MISSION][CutsceneDoorHide] PROCESS_HIDE_DOORS_FOR_CUTSCENES - In a cutscene, Would hide iDoor ", iDoor, " but no door exists")
						ENDIF
						SET_BIT(iDoorBS_HiddenForCutscene, iDoor)
					ENDIF				
				ELIF IS_BIT_SET(iDoorBS_HiddenForCutscene, iDoor)
					OBJECT_INDEX oiDoor = GET_WORLD_DOOR_OBJECT(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].mnDoorModel, g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].vPos)				
					CLEAR_BIT(iDoorBS_HiddenForCutscene, iDoor)
					IF DOES_ENTITY_EXIST(oiDoor)
						PRINTLN("[RCC MISSION][CutsceneDoorHide] PROCESS_HIDE_DOORS_FOR_CUTSCENES - No longer in a cutscene, Showing iDoor ", iDoor)
						SET_ENTITY_ALPHA(oiDoor, 255, FALSE)
					ELSE
						PRINTLN("[RCC MISSION][CutsceneDoorHide] PROCESS_HIDE_DOORS_FOR_CUTSCENES -  No longer in a cutscene, Would show iDoor ", iDoor, " but no door exists")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL UPDATE_DOOR_STATE(INT iteam, INT irule)

	INT iDoor,iDoorHash
	
	BOOL door_update_finished = true

	PRINTLN("[RCC MISSION] UPDATE_DOOR_STATE - running door update for team ",iteam,", rule ",irule)
	
	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfDoors iDoor
	
		IF NOT IS_BIT_SET(iDoorUpdatedBS, iDoor)
		OR SHOULD_DOOR_UPDATE_EVERY_FRAME(iDoor)
			PRINTLN("[RCC MISSION] UPDATE_DOOR_STATE - Door ",iDoor," has update team ",g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iUpdateTeam,", update rule ",g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iUpdateRule)
			
			IF iteam = g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iUpdateTeam
			OR (g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iUpdateTeam = -1 AND SHOULD_DOOR_UPDATE_EVERY_FRAME(iDoor))
			OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iBitSet, ciDoorBS_LEGACY_OnlyUpdateFromPreviousMission)
			OR (IS_BIT_SET(MC_serverBD_1.iDoorUpdateOffRuleBS, iDoor) AND SHOULD_DOOR_UPDATE_FROM_OBJECT(iDoor))
			OR (IS_BIT_SET(iDirectionalDoorShouldUpdateBS, iDoor))
			
				IF ALLOW_DOOR_TO_UPDATE(iDoor, irule)
				OR SHOULD_DOOR_UPDATE_EVERY_FRAME(iDoor)
					IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].vPos)
						IF NOT g_FMMC_STRUCT_ENTITIES.sSelectedDoor[ iDoor ].bUpdateOnMidpoint
						OR IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iteam], irule)
							
							IF has_door_time_exceeded(iDoor)
				
								IF iDoorID[iDoor] = -1
									iDoorID[iDoor] = iDoor
								ENDIF
								
								BOOL bNotReady = false
								
								IF g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].mnDoorModel = INT_TO_ENUM(MODEL_NAMES, HASH("cs1_02_cfdoor_front"))
								OR g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].mnDoorModel = INT_TO_ENUM(MODEL_NAMES, HASH("cs1_02_cfdr_back")) 
									
									IF g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].sDoorConfigs[ciDOOR_CONFIG__ALT].fOpenRatio = 1.0
										REMOVE_IPL("cs1_02_cf_offmission")
										REQUEST_IPL("cs1_02_cf_onmission1")
										REQUEST_IPL("cs1_02_cf_onmission2")
										REQUEST_IPL("cs1_02_cf_onmission3")
										REQUEST_IPL("cs1_02_cf_onmission4")
										PRINTLN("[RCC MISSION] UPDATE_DOOR_STATE - opening CF door: ",iDoor)
									ELSE
										REQUEST_IPL("cs1_02_cf_offmission")
										REMOVE_IPL("cs1_02_cf_onmission1")
										REMOVE_IPL("cs1_02_cf_onmission2")
										REMOVE_IPL("cs1_02_cf_onmission3")
										REMOVE_IPL("cs1_02_cf_onmission4")
										PRINTLN("[RCC MISSION] UPDATE_DOOR_STATE - closing CF door: ",iDoor)
									ENDIF
									
								ELIF g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].mnDoorModel = INT_TO_ENUM(MODEL_NAMES, HASH("cs2_30_chem_grill_ipl_group"))
									
									IF g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].sDoorConfigs[ciDOOR_CONFIG__ALT].fOpenRatio = 1.0
										IF IS_IPL_ACTIVE("chemgrill_grp1")
											REMOVE_IPL("chemgrill_grp1")
											PRINTLN("[RCC MISSION] UPDATE_DOOR_STATE - opening humane labs tunnel grill, door: ",iDoor)
										ENDIF
									ELSE
										IF NOT IS_IPL_ACTIVE("chemgrill_grp1")
											REQUEST_IPL("chemgrill_grp1")
											PRINTLN("[RCC MISSION] UPDATE_DOOR_STATE - closing humane labs tunnel grill, door: ",iDoor)
										ENDIF
									ENDIF
									
								ELIF IS_DOOR_VAULT_OBJECT_DOOR(iDoor)
									
									//This is needed to stop the vault doors from falling into the state below for any door model
								ELIF g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].mnDoorModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_vault_d_door_01a"))
									PROCESS_CASINO_DAILY_CASH_ROOM(iDoor)
								ELIF g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].mnDoorModel = V_ILEV_GB_TELDR
									
									FLOAT fStartHeading, fHeadingChange
									
									fHeadingChange = ((g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].sDoorConfigs[ciDOOR_CONFIG__ALT].fOpenRatio) * 85)
									
									IF fHeadingChange != 0
										IF NOT DOES_ENTITY_EXIST(FleecaTellerDoor)
											IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].vPos, 5.0, V_ILEV_GB_TELDR)
												IF CAN_REGISTER_MISSION_OBJECTS(1)
													MC_RESERVE_NETWORK_MISSION_OBJECTS(1)
													FleecaTellerDoor = GET_CLOSEST_OBJECT_OF_TYPE(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].vPos,5.0,V_ILEV_GB_TELDR, true, false, false)
													bNotReady = TRUE
												ENDIF
											ENDIF
										ELSE
											fStartHeading  = GET_ENTITY_HEADING(FleecaTellerDoor)
											
											IF (fStartHeading + fHeadingChange) < 0
												SET_ENTITY_HEADING(FleecaTellerDoor,((fStartHeading + fHeadingChange) + 360.0))
											ELIF (fStartHeading + fHeadingChange) >= 360
												SET_ENTITY_HEADING(FleecaTellerDoor,((fStartHeading + fHeadingChange) - 360.0))
											ELSE
												SET_ENTITY_HEADING(FleecaTellerDoor,(fStartHeading + fHeadingChange))
											ENDIF
										ENDIF
									ENDIF
									
								ELIF IS_FAKE_DOOR(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].mnDoorModel)
								
									PRINTLN("[RCC MISSION] UPDATE_DOOR_STATE - IS_FAKE_DOOR: ", iDoor," to ratio: ", g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].sDoorConfigs[ciDOOR_CONFIG__STANDARD].fOpenRatio, " at pos: ", g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].vPos)
									
								ELIF IS_MODEL_VALID(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].mnDoorModel)
									IF GET_NEW_DOOR_HASH(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].mnDoorModel, g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].vPos, iDoorHash)
										IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(iDoorHash)
											ADD_DOOR_TO_SYSTEM(iDoorHash, g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].mnDoorModel, g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].vPos, FALSE, FALSE)
											IF NOT IS_CASINO_HEIST_MANTRAP_DOOR(iDoor)
												iCachedDoorHashes[iDoor] = iDoorHash
											ENDIF
											PRINTLN("[Doors] Adding door to system: ", iDoor, " - iDoorHash: ", iDoorHash)
										ENDIF
										
										PRINTLN("[RCC MISSION] UPDATE_DOOR_STATE - ADDING DOOR: ",iDoor," to ratio: ", g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].sDoorConfigs[ciDOOR_CONFIG__STANDARD].fOpenRatio, " at pos: ", g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].vPos)
									ELSE
										PRINTLN("[RCC MISSION] UPDATE_DOOR_STATE - DOOR EXISTS ALREADY: ", iDoor, " to ratio: ", g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].sDoorConfigs[ciDOOR_CONFIG__STANDARD].fOpenRatio, " at pos: ", g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].vPos)
									ENDIF
									
									PRINTLN("[RCC MISSION] UPDATE_DOOR_STATE: Door ", iDoor, " has door hash ", iDoorHash)
									
									//initialise the door to it's original ratio value
									IF NOT bOpenGate
										fGateOpenRatio = g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].sDoorConfigs[ciDOOR_CONFIG__STANDARD].fOpenRatio
										bOpenGate = TRUE
									ENDIF
									
									IF IS_DOOR_REGISTERED_WITH_SYSTEM(iDoorHash)
										IF NETWORK_IS_DOOR_NETWORKED(iDoorHash)
											PRINTLN("[RCC MISSION] UPDATE_DOOR_STATE network_is_door_networked TRUE: ", iDoor)
											IF NETWORK_HAS_CONTROL_OF_DOOR(iDoorHash)
												PRINTLN("[RCC MISSION] UPDATE_DOOR_STATE network_has_control_of_door TRUE: ", iDoor)
												PROCESS_DOOR_FOR_UPDATE_DOOR_STATE(iDoor, iDoorHash, TRUE, iteam, bNotReady)
											ELSE
												PRINTLN("[RCC MISSION] UPDATE_DOOR_STATE network_has_control_of_door FALSE: ", iDoor)
											ENDIF
										ELSE
											PRINTLN("[RCC MISSION] UPDATE_DOOR_STATE network_is_door_networked FALSE: ", iDoor)
											PROCESS_DOOR_FOR_UPDATE_DOOR_STATE(iDoor, iDoorHash, FALSE, iteam, bNotReady)
										ENDIF
									ENDIF
								ENDIF
								
								IF NOT bNotReady
									RESET_NET_TIMER(door_time[idoor])
									SET_BIT(iDoorUpdatedBS, iDoor)
									bOpenGate = FALSE
									
									IF bIsLocalPlayerHost
										IF IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_DoorsUsedAltConfigTracking)
										AND g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iContinuityId > -1
										AND NOT IS_BIT_SET(MC_serverBD_4.iHostDirectionalDoorUnlockedBS, iDoor)
										AND NOT IS_BIT_SET(iDirectionalDoorShouldUpdateBS, iDoor)
										AND ALLOW_DOOR_TO_UPDATE(iDoor, iRule)
											IF NOT IS_LONG_BIT_SET(MC_serverBD_1.sLEGACYMissionContinuityVars.iDoorsUseAltConfigBitset, g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iContinuityId)
												PRINTLN("[JS][CONTINUITY][Doors] - UPDATE_DOOR_STATE - Door ", iDoor, " with continuity ID ", g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iContinuityId, " has been updated")
												SET_LONG_BIT(MC_serverBD_1.sLEGACYMissionContinuityVars.iDoorsUseAltConfigBitset, g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iContinuityId)
											ENDIF
										ENDIF
									ENDIF
								ELSE
									door_update_finished = FALSE
								ENDIF
								
							ELSE
								
								door_update_finished = FALSE
								
							ENDIF 
						ELSE
							door_update_finished = FALSE
						ENDIF
						
					ENDIF
					
				ENDIF
				
			ENDIF
			
			PROCESS_UPDATE_DOOR_TIMED_OPENING(iDoor)
			
		ENDIF
		
		PROCESS_FMMC_DIRECTIONAL_DOOR_LOCK(iDoor, iDoorHash, iTeam, iRule)
		
		PROCESS_HIDE_DOORS_FOR_CUTSCENES(iDoor)
		
	ENDREPEAT
	
	RETURN door_update_finished
	
ENDFUNC

PROC SET_DOOR_START_STATE()

	BOOL retVal = TRUE
	INT iDoor,iDoorHash
	
	IF NOT IS_BIT_SET(g_TransitionSessionNonResetVars.iFMMCMissionBS, iFMMCMissionBS__MISSION_CONTROLLER_IS_CONTROLLING_DOORS)
		SET_BIT(g_TransitionSessionNonResetVars.iFMMCMissionBS, iFMMCMissionBS__MISSION_CONTROLLER_IS_CONTROLLING_DOORS)
		PRINTLN("[RCC MISSION][Doors] SET_DOOR_START_STATE: ",iDoor," setting iFMMCMissionBS__MISSION_CONTROLLER_IS_CONTROLLING_DOORS")
	ENDIF
	
	IF IS_BIT_SET(g_TransitionSessionNonResetVars.iFMMCMissionBS, iFMMCMissionBS__LAUNCHER_HANDLING_MANTRAP_DOORS)
		CLEANUP_CASINO_HEIST_MANTRAP_DOORS_FROM_TRANSITION()
		CLEAR_BIT(g_TransitionSessionNonResetVars.iFMMCMissionBS, iFMMCMissionBS__LAUNCHER_HANDLING_MANTRAP_DOORS)
		PRINTLN("[RCC MISSION][Doors] SET_DOOR_START_STATE: ",iDoor," clearing iFMMCMissionBS__LAUNCHER_HANDLING_MANTRAP_DOORS")
	ENDIF
	
	FOR iDoor = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfDoors-1)
	
		PRINTLN("[RCC MISSION] SET_DOOR_START_STATE: ",iDoor," iUpdateTeam: ",g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iUpdateTeam," iUpdateRule: ",g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].iUpdateRule)
		
		IF iDoorID[iDoor] = -1
		
			IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].vPos)
			
				iDoorID[iDoor] = iDoor
				
				IF g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].mnDoorModel = INT_TO_ENUM(MODEL_NAMES, HASH("cs1_02_cfdoor_front"))
				OR g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].mnDoorModel = INT_TO_ENUM(MODEL_NAMES, HASH("cs1_02_cfdr_back"))
				
					IF g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].sDoorConfigs[ciDOOR_CONFIG__STANDARD].fOpenRatio = 1.0
						REMOVE_IPL("cs1_02_cf_offmission")
						REQUEST_IPL("cs1_02_cf_onmission1")
						REQUEST_IPL("cs1_02_cf_onmission2")
						REQUEST_IPL("cs1_02_cf_onmission3")
						REQUEST_IPL("cs1_02_cf_onmission4")
					ENDIF
					
					PRINTLN("[RCC MISSION] SET_DOOR_START_STATE Setting CF door: ", iDoor, " to ratio: ",g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].sDoorConfigs[ciDOOR_CONFIG__STANDARD].fOpenRatio," at pos: ",g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].vPos)
					
				ELIF g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].mnDoorModel = INT_TO_ENUM(MODEL_NAMES, HASH("cs2_30_chem_grill_ipl_group"))
				
					IF g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].sDoorConfigs[ciDOOR_CONFIG__STANDARD].fOpenRatio = 1.0
						IF IS_IPL_ACTIVE("chemgrill_grp1")
							REMOVE_IPL("chemgrill_grp1")
						ENDIF
					ENDIF
					
					PRINTLN("[RCC MISSION] SET_DOOR_START_STATE Setting chem grill door: ",iDoor," to ratio: ",g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].sDoorConfigs[ciDOOR_CONFIG__STANDARD].fOpenRatio," at pos: ",g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].vPos)
					
				ELIF IS_FAKE_DOOR(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].mnDoorModel) //Not processed here. Added to stop the aircraft carrier doors being added as a proper door via the hash. All doors processed within MAINTAIN_SETTING_MAP_OBJECT_DOOR_RATIOS()
				
					PRINTLN("[RCC MISSION] SET_DOOR_START_STATE aircraft doors are processed within MAINTAIN_SETTING_MAP_OBJECT_DOOR_RATIOS()")
					
				ELIF g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].mnDoorModel = V_ILEV_GB_TELDR
				
					PRINTLN("[RCC MISSION] SET_DOOR_START_STATE V_ILEV_GB_TELDR doesn't work, need to run in update")
					
				ELIF IS_MODEL_VALID(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].mnDoorModel)
				
					IF GET_NEW_DOOR_HASH(g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].mnDoorModel, g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].vPos, iDoorHash, iCachedDoorHashes[iDoor])
						IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(iDoorHash)
							PRINTLN("[RCC MISSION] SET_DOOR_START_STATE ADDING DOOR: ",iDoor," with hash ", iDoorHash," to ratio: ",g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].sDoorConfigs[ciDOOR_CONFIG__STANDARD].fOpenRatio," at pos: ",g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].vPos)
							ADD_DOOR_TO_SYSTEM(iDoorHash,g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].mnDoorModel, g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].vPos,FALSE,FALSE)
							IF NOT IS_CASINO_HEIST_MANTRAP_DOOR(iDoor)
								iCachedDoorHashes[iDoor] = iDoorHash
							ENDIF
						ENDIF
					ELSE
						PRINTLN("[RCC MISSION] SET_DOOR_START_STATE DOOR EXISTS ALREADY: ",iDoor," to ratio: ", g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].sDoorConfigs[ciDOOR_CONFIG__STANDARD].fOpenRatio," at pos: ",g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].vPos)
					ENDIF
					
					PRINTLN("[RCC MISSION] SET_DOOR_START_STATE: Door ", iDoor, " has door hash ", iDoorHash)
					
					//checks the door hash to see if it is one of connor's networks doors. 
					//there are currently 9 network doors 
					IF (GET_ONE_WAY_DOOR_ID(iDoorHash) != -1)
						IF g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].bSwingFree
							MISSION_CONTROL_NET_DOOR_OVERRIDE(iDoorHash, MC_DOOR_OVERRIDE_UNLOCKED) 
							PRINTLN("[RCC MISSION] NETWORK DOOR UNLOCKED: ",iDoor," to ratio: ", g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].sDoorConfigs[ciDOOR_CONFIG__STANDARD].fOpenRatio," at pos: ", g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].vPos)
						ELSE
							MISSION_CONTROL_NET_DOOR_OVERRIDE(iDoorHash, MC_DOOR_OVERRIDE_LOCKED)
							PRINTLN("[RCC MISSION] ADDING DOOR MC_DOOR_OVERRIDE_LOCKED. THE DOOR WILL BE LOCKED IN IT'S CURRENT STATE / RATIO: ",iDoor,"  at pos: ", g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].vPos)
						ENDIF
					ENDIF
					
					IF NETWORK_IS_DOOR_NETWORKED(iDoorHash)	
						PRINTLN("[RCC MISSION] SET_DOOR_START_STATE network_is_door_networked TRUE: ", iDoor)
						IF NETWORK_HAS_CONTROL_OF_DOOR(iDoorHash)
							PRINTLN("[RCC MISSION] SET_DOOR_START_STATE network_has_control_of_door TRUE: ", iDoor)
							PRINTLN("[RCC MISSION] SET_DOOR_START_STATE DOOR_SYSTEM_SET_AUTOMATIC_RATE door: ",iDoor," AUTOMATIC_RATE: ",g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].sDoorConfigs[ciDOOR_CONFIG__STANDARD].fAutomaticRate," at pos: ", g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].vPos)
							PROCESS_DOOR_FOR_SET_DOOR_START_STATE(iDoor, iDoorHash, TRUE)
						ELSE 
							PRINTLN("[RCC MISSION] SET_DOOR_START_STATE network_has_control_of_door FALSE: ", iDoor)
						ENDIF
					ELSE
						PRINTLN("[RCC MISSION] SET_DOOR_START_STATE network_is_door_networked FALSE: ", iDoor)
						PRINTLN("[RCC MISSION] SET_DOOR_START_STATE DOOR_SYSTEM_SET_AUTOMATIC_RATE door: ",iDoor," AUTOMATIC_RATE: ",g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].sDoorConfigs[ciDOOR_CONFIG__STANDARD].fAutomaticRate," at pos: ", g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].vPos)
						PROCESS_DOOR_FOR_SET_DOOR_START_STATE(iDoor, iDoorHash, FALSE)
					ENDIF
					
				ELSE
				
					iDoorID[iDoor] = -1
					retVal = FALSE
					PRINTLN("[RCC MISSION] SET_DOOR_START_STATE Invalid model: ", g_FMMC_STRUCT_ENTITIES.sSelectedDoor[iDoor].mnDoorModel)
					PRINTLN("[RCC MISSION] SET_DOOR_START_STATE | Door ", iDoor, " model is currently considered invalid! Waiting for it to be loaded in properly before moving on")
				ENDIF
				
			ELSE
				PRINTLN("[RCC MISSION] SET_DOOR_START_STATE Pos is zero")
				
			ENDIF
			
		ELSE
			PRINTLN("[RCC MISSION] SET_DOOR_START_STATE Door id is: ", iDoorID[iDoor])
			
		ENDIF
		
	ENDFOR
	
	bDoorsInited = retVal
ENDPROC

// CASINO VAULT DOOR // // // // // // // // // // // // // // // // // // // //
PROC SET_CASINO_VAULT_DOOR_STATE(CASINO_VAULT_DOOR_STATE eNewState)
	IF eCasinoVaultDoorState != eNewState
		eCasinoVaultDoorState = eNewState
		PRINTLN("[CasinoVaultDoor] Setting eCasinoVaultDoorState to ", eCasinoVaultDoorState)
	ENDIF
ENDPROC

#IF IS_DEBUG_BUILD
PROC CASINO_VAULT_DOOR_DEBUG(VECTOR vVaultDoorPos)

	IF NOT bCasinoVaultDoorDebug
		EXIT
	ENDIF
	
	IF NOT IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD3)
		EXIT
	ENDIF
	
	TEXT_LABEL_63 tlEntityDebug = ""
	
	tlEntityDebug = "eCasinoVaultDoorState: "
	tlEntityDebug += ENUM_TO_INT(eCasinoVaultDoorState)
	DRAW_DEBUG_TEXT(tlEntityDebug, vVaultDoorPos + (<<0,0, 0.2>>))
	
	IF eCasinoVaultDoorState = CASINO_VAULT_DOOR_STATE__OPENING
		tlEntityDebug = "fCasinoVaultDoor_CurrentOpenRatio: "
		tlEntityDebug += FLOAT_TO_STRING(fCasinoVaultDoor_CurrentOpenRatio)
		DRAW_DEBUG_TEXT(tlEntityDebug, vVaultDoorPos + (<<0,0, -0.2>>))
		
		tlEntityDebug = "fCasinoVaultDoor_CurrentSpeed: "
		tlEntityDebug += FLOAT_TO_STRING(fCasinoVaultDoor_CurrentSpeed)
		DRAW_DEBUG_TEXT(tlEntityDebug, vVaultDoorPos + (<<0,0, -0.3>>))
	ENDIF
	
	IF DOES_ENTITY_EXIST(oiSpawnedVaultDoor_Animated)
		tlEntityDebug = "Anim Door Here"
		DRAW_DEBUG_TEXT_ABOVE_ENTITY(oiSpawnedVaultDoor_Animated, tlEntityDebug, 0.1)
	ENDIF
	
	IF DOES_ENTITY_EXIST(oiSpawnedVaultDoor_StaticCollision)
		tlEntityDebug = "Static Collision Door Here"
		DRAW_DEBUG_TEXT_ABOVE_ENTITY(oiSpawnedVaultDoor_StaticCollision, tlEntityDebug, 0.1)
	ENDIF
	
	IF IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD2)
		SET_CASINO_VAULT_DOOR_STATE(CASINO_VAULT_DOOR_STATE__INIT)
	ENDIF
	
ENDPROC
#ENDIF

FUNC BOOL SHOULD_CASINO_VAULT_DOOR_OPEN()
	
	#IF IS_DEBUG_BUILD
	IF bCasinoVaultDoorDebug
		IF IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD3)
		AND IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD6)
			RETURN TRUE
		ENDIF
	ENDIF
	#ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_CASINO_VAULT_DOOR_BREAK()
	
	#IF IS_DEBUG_BUILD
	IF bCasinoVaultDoorDebug
		IF IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD3)
		AND IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD9)
			RETURN TRUE
		ENDIF
	ENDIF
	#ENDIF
	
	IF IS_BIT_SET(iCasinoVaultDoorExplosionBS, ciCasinoVaultDoorExplosionBS__StartedSequence)
		PRINTLN("[CasinoVaultDoor] SHOULD_CASINO_VAULT_DOOR_BREAK || ciCasinoVaultDoorExplosionBS__StartedSequence")
		RETURN TRUE
	ENDIF
	
	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	IF iRule < FMMC_MAX_RULES
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThirteen[iRule], ciBS_RULE13_EXPLODE_VAULT_DOOR)
			PRINTLN("[CasinoVaultDoor] SHOULD_CASINO_VAULT_DOOR_BREAK || ciBS_RULE13_EXPLODE_VAULT_DOOR")
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck31, LBOOL31_READY_TO_USE_DAMAGED_VAULT_DOOR)
		PRINTLN("[CasinoVaultDoor] SHOULD_CASINO_VAULT_DOOR_BREAK || LBOOL31_READY_TO_USE_DAMAGED_VAULT_DOOR = TRUE")
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_CASINO_VAULT_DOOR_HIDE()
	
	#IF IS_DEBUG_BUILD
	IF bCasinoVaultDoorDebug
		IF IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD3)
		AND IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD9)
			RETURN TRUE
		ENDIF
	ENDIF
	#ENDIF
	
	IF DOES_ENTITY_EXIST(oiSpawnedVaultDoor_Animated)
	AND IS_ENTITY_VISIBLE(oiSpawnedVaultDoor_Animated)
		PRINTLN("[CasinoVaultDoor] SHOULD_CASINO_VAULT_DOOR_HIDE || oiSpawnedVaultDoor_Animated is visible")
		RETURN TRUE
	ENDIF
	
	IF eCasinoVaultDoorState = CASINO_VAULT_DOOR_STATE__BROKEN
		PRINTLN("[CasinoVaultDoor] SHOULD_CASINO_VAULT_DOOR_HIDE || CASINO_VAULT_DOOR_STATE__BROKEN")
		RETURN TRUE
	ENDIF
	
	IF IS_CUTSCENE_PLAYING()
		PRINTLN("[CasinoVaultDoor] SHOULD_CASINO_VAULT_DOOR_HIDE || IS_CUTSCENE_PLAYING")
		RETURN TRUE
	ENDIF
	
	IF eScriptedCutsceneProgress != SCRIPTEDCUTPROG_INIT
		PRINTLN("[CasinoVaultDoor] SHOULD_CASINO_VAULT_DOOR_HIDE || eScriptedCutsceneProgress")
		RETURN TRUE
	ENDIF
	
	IF SHOULD_CASINO_VAULT_DOOR_BREAK()
		PRINTLN("[CasinoVaultDoor] SHOULD_CASINO_VAULT_DOOR_HIDE || SHOULD_CASINO_VAULT_DOOR_BREAK")
		RETURN TRUE
	ENDIF
	
	PRINTLN("[CasinoVaultDoor_SPAM] SHOULD_CASINO_VAULT_DOOR_HIDE || It should not hide!!")
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_SPAWNED_CASINO_VAULT_DOOR_HIDE()
	IF IS_CUTSCENE_PLAYING()
		PRINTLN("[CasinoVaultDoor] SHOULD_SPAWNED_CASINO_VAULT_DOOR_HIDE || IS_CUTSCENE_PLAYING")
		RETURN TRUE
	ENDIF
	
	PRINTLN("[CasinoVaultDoor_SPAM] SHOULD_SPAWNED_CASINO_VAULT_DOOR_HIDE || It should not hide!!")
	RETURN FALSE
ENDFUNC

PROC HIDE_FAKE_CASINO_VAULT_DOOR()
	
	IF IS_ENTITY_VISIBLE(oiSpawnedVaultDoor_Animated)
		SET_ENTITY_VISIBLE(oiSpawnedVaultDoor_Animated, FALSE)
		SET_ENTITY_COMPLETELY_DISABLE_COLLISION(oiSpawnedVaultDoor_Animated, FALSE)
		PRINTLN("[CasinoVaultDoor] HIDE_FAKE_CASINO_VAULT_DOOR | Making oiSpawnedVaultDoor_Animated invisible and disabling collision!")
	ENDIF
	
	IF IS_ENTITY_VISIBLE(oiVaultDoorDebrisObj)
		SET_ENTITY_VISIBLE(oiVaultDoorDebrisObj, FALSE)
		SET_ENTITY_COMPLETELY_DISABLE_COLLISION(oiVaultDoorDebrisObj, FALSE)
		PRINTLN("[CasinoVaultDoor] HIDE_FAKE_CASINO_VAULT_DOOR | Making oiVaultDoorDebrisObj invisible and disabling collision!")
	ENDIF
ENDPROC

PROC HIDE_CASINO_VAULT_DOOR()
	
	IF NETWORK_GET_ENTITY_IS_NETWORKED(oiRealVaultDoor)
		IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(oiRealVaultDoor)
			PRINTLN("[CasinoVaultDoor] HIDE_CASINO_VAULT_DOOR | I don't have control of oiRealVaultDoor")
			EXIT
		ENDIF
	ENDIF
	
	IF IS_ENTITY_VISIBLE(oiRealVaultDoor)
		SET_ENTITY_VISIBLE(oiRealVaultDoor, FALSE)
		SET_ENTITY_COMPLETELY_DISABLE_COLLISION(oiRealVaultDoor, FALSE)
		PRINTLN("[CasinoVaultDoor] HIDE_CASINO_VAULT_DOOR | Making oiRealVaultDoor invisible and disabling collision!")
	ENDIF
ENDPROC

PROC SHOW_CASINO_VAULT_DOOR()
	
	IF NETWORK_GET_ENTITY_IS_NETWORKED(oiRealVaultDoor)
		IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(oiRealVaultDoor)
			PRINTLN("[CasinoVaultDoor] SHOW_CASINO_VAULT_DOOR | I don't have control of oiRealVaultDoor")
			EXIT
		ENDIF
	ENDIF
	
	IF NOT IS_ENTITY_VISIBLE(oiRealVaultDoor)
		SET_ENTITY_VISIBLE(oiRealVaultDoor, TRUE)
		SET_ENTITY_COMPLETELY_DISABLE_COLLISION(oiRealVaultDoor, TRUE)
		PRINTLN("[CasinoVaultDoor] SHOW_CASINO_VAULT_DOOR | Making oiRealVaultDoor visible and turning collision back on!")
	ENDIF
ENDPROC

FUNC BOOL BREAK_CASINO_VAULT_DOOR(VECTOR vVaultDoorPos, MODEL_NAMES mnDoorModel, MODEL_NAMES mnDecalModel)
	
	UNUSED_PARAMETER(vVaultDoorPos)
	UNUSED_PARAMETER(mnDoorModel)	
	
	IF NOT IS_BIT_SET(iCasinoVaultDoorExplosionBS, ciCasinoVaultDoorExplosionBS__StartedSequence)
		PRINTLN("[CasinoVaultDoor] BREAK_CASINO_VAULT_DOOR || Starting PTFX!")
		USE_PARTICLE_FX_ASSET("cut_hs3f")
		START_PARTICLE_FX_NON_LOOPED_AT_COORD("cut_hs3f_exp_vault", <<2505, -238.5, -70.5>>, <<0,0,0>>)
		PLAY_SOUND_FROM_COORD(-1, "vault_door_explosion", vVaultDoorPos, "dlc_ch_heist_finale_sounds")
		
		REMOVE_DECALS_IN_RANGE(<<2505, -238.5, -70.5>>, 8.0)
		REINIT_NET_TIMER(stCasinoVaultDoorExplosionTimer)
		
		SET_BIT(iCasinoVaultDoorExplosionBS, ciCasinoVaultDoorExplosionBS__StartedSequence)
	ENDIF
	
	IF IS_ENTITY_VISIBLE(oiSpawnedVaultDoor_Animated)
	AND IS_ENTITY_VISIBLE(oiVaultDoorDebrisObj)
		
		PRINTLN("[CasinoVaultDoor] BREAK_CASINO_VAULT_DOOR || Starting animations!")
		
		IF NOT IS_BIT_SET(iCasinoVaultDoorExplosionBS, ciCasinoVaultDoorExplosionBS__PlayedPedAnim)
		AND HAS_NET_TIMER_EXPIRED(stCasinoVaultDoorExplosionTimer, ciCasinoVaultDoorExplosionTiming_PlayPedAnim)
			SWITCH GET_RANDOM_INT_IN_RANGE(0, 2)
				CASE 0
					TASK_PLAY_ANIM(LocalPlayerPed, "anim_heist@hs3f@ig8_vault_explosive_react@left@male@", "player_react_explosive", DEFAULT, DEFAULT, DEFAULT, (AF_HIDE_WEAPON))
				BREAK
				CASE 1
					TASK_PLAY_ANIM(LocalPlayerPed, "anim_heist@hs3f@ig8_vault_explosive_react@right@male@", "player_react_explosive", DEFAULT, DEFAULT, DEFAULT, (AF_HIDE_WEAPON))
				BREAK
			ENDSWITCH
			
			SET_BIT(iCasinoVaultDoorExplosionBS, ciCasinoVaultDoorExplosionBS__PlayedPedAnim)
		ENDIF
		
		IF NOT IS_BIT_SET(iCasinoVaultDoorExplosionBS, ciCasinoVaultDoorExplosionBS__PlayedDoorAnimation)
		AND HAS_NET_TIMER_EXPIRED(stCasinoVaultDoorExplosionTimer, ciCasinoVaultDoorExplosionTiming_PlayDoorAnim)
			SET_ENTITY_COMPLETELY_DISABLE_COLLISION(oiSpawnedVaultDoor_Animated, FALSE)
			SET_ENTITY_COMPLETELY_DISABLE_COLLISION(oiSpawnedVaultDoor_StaticCollision, TRUE)
			
			iVaultExplosionSyncScene = CREATE_SYNCHRONIZED_SCENE(<<2488.348000, -267.364000, -71.646000>>, <<0,0,0>>)
			PLAY_SYNCHRONIZED_ENTITY_ANIM(oiSpawnedVaultDoor_Animated, iVaultExplosionSyncScene, "explosion_vault_01", "anim_heist@hs3f@ig8_vault_door_explosion@", INSTANT_BLEND_IN, DEFAULT, DEFAULT, INSTANT_BLEND_IN)
			PLAY_SYNCHRONIZED_ENTITY_ANIM(oiVaultDoorDebrisObj, iVaultExplosionSyncScene, "explosion_vault_02", "anim_heist@hs3f@ig8_vault_door_explosion@", INSTANT_BLEND_IN, DEFAULT, DEFAULT, INSTANT_BLEND_IN)
			SET_SYNCHRONIZED_SCENE_PHASE(iVaultExplosionSyncScene, 0.056)
			
			SET_BIT(iCasinoVaultDoorExplosionBS, ciCasinoVaultDoorExplosionBS__PlayedDoorAnimation)
		ENDIF
		
		IF NOT IS_BIT_SET(iCasinoVaultDoorExplosionBS, ciCasinoVaultDoorExplosionBS__ShownDecal)
		AND HAS_NET_TIMER_EXPIRED(stCasinoVaultDoorExplosionTimer, ciCasinoVaultDoorExplosionTiming_ShowDecal)
			REMOVE_MODEL_HIDE(vVaultDoorPos, 25.0, mnDecalModel, FALSE)
			SET_BIT(iCasinoVaultDoorExplosionBS, ciCasinoVaultDoorExplosionBS__ShownDecal)
		ENDIF
		
		IF NOT IS_BIT_SET(iCasinoVaultDoorExplosionBS, ciCasinoVaultDoorExplosionBS__HiddenExplosives)
		AND HAS_NET_TIMER_EXPIRED(stCasinoVaultDoorExplosionTimer, ciCasinoVaultDoorExplosionTiming_HiddenExplosives)
		
			CREATE_MODEL_HIDE(vVaultDoorPos, 25.0, INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Ch_Explosive_01a")), FALSE)
			CLEAR_AREA_OF_PROJECTILES(vVaultDoorPos, 25.0, FALSE)
			SET_BIT(iCasinoVaultDoorExplosionBS, ciCasinoVaultDoorExplosionBS__HiddenExplosives)
			
			IF bIsLocalPlayerHost
				BROADCAST_FMMC_INTERACT_WITH_EVENT(-1, TRUE)
			ENDIF
		ENDIF
		
		IF NOT IS_BIT_SET(iCasinoVaultDoorExplosionBS, ciCasinoVaultDoorExplosionBS__PlayedCamShake)
		AND HAS_NET_TIMER_EXPIRED(stCasinoVaultDoorExplosionTimer, ciCasinoVaultDoorExplosionTiming_PlayCamShake)
			SET_CONTROL_SHAKE(PLAYER_CONTROL, 130, 256)
			SHAKE_GAMEPLAY_CAM("LARGE_EXPLOSION_SHAKE", 0.5)
			SET_BIT(iCasinoVaultDoorExplosionBS, ciCasinoVaultDoorExplosionBS__PlayedCamShake)
		ENDIF
		
		IF IS_BIT_SET(iCasinoVaultDoorExplosionBS, ciCasinoVaultDoorExplosionBS__StartedSequence)
		AND IS_BIT_SET(iCasinoVaultDoorExplosionBS, ciCasinoVaultDoorExplosionBS__PlayedDoorAnimation)
		AND IS_BIT_SET(iCasinoVaultDoorExplosionBS, ciCasinoVaultDoorExplosionBS__PlayedPedAnim)
		AND IS_BIT_SET(iCasinoVaultDoorExplosionBS, ciCasinoVaultDoorExplosionBS__PlayedCamShake)
		AND IS_BIT_SET(iCasinoVaultDoorExplosionBS, ciCasinoVaultDoorExplosionBS__ShownDecal)
			SET_CASINO_VAULT_DOOR_STATE(CASINO_VAULT_DOOR_STATE__BROKEN)
			RETURN TRUE
		ENDIF
	ELSE
		SET_ENTITY_VISIBLE(oiSpawnedVaultDoor_Animated, TRUE)
		SET_ENTITY_VISIBLE(oiVaultDoorDebrisObj, TRUE)
		
		PRINTLN("[CasinoVaultDoor] BREAK_CASINO_VAULT_DOOR | Making entities visible!")
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC INIT_VAULT_EXPLOSION_PROP_OBJ(OBJECT_INDEX oiObj, BOOL bInteriorReady, BOOL bMatchVaultDoor = TRUE, BOOL bStartVisible = TRUE)
	
	FREEZE_ENTITY_POSITION(oiObj, TRUE)
	
	IF bMatchVaultDoor
		VECTOR vTargetCoords = GET_ENTITY_COORDS(oiRealVaultDoor)
		SET_ENTITY_COORDS(oiObj, vTargetCoords)
		PRINTLN("[CasinoVaultDoor] Calling INIT_VAULT_EXPLOSION_PROP_OBJ on ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(oiObj)), " which has been moved to ", vTargetCoords)
	ENDIF
	
	IF NOT bStartVisible
		SET_ENTITY_VISIBLE(oiObj, FALSE)
	ENDIF
	
	IF bInteriorReady
		RETAIN_ENTITY_IN_INTERIOR(oiObj, interiorCasinoVaultIndex)
		FORCE_ROOM_FOR_ENTITY(oiObj, interiorCasinoVaultIndex, ciVaultDoorRoomKey)
	ENDIF
ENDPROC

FUNC BOOL PROCESS_REAL_CASINO_VAULT_DOOR_OBJ(MODEL_NAMES mnDoorModel, VECTOR vVaultDoorPos)
	IF DOES_ENTITY_EXIST(oiRealVaultDoor)
		IF SHOULD_CASINO_VAULT_DOOR_HIDE()
			HIDE_CASINO_VAULT_DOOR()
		ELSE
			SHOW_CASINO_VAULT_DOOR()
		ENDIF
		
		RETURN TRUE		
	ELSE
		PRINTLN("[CasinoVaultDoor] Grabbing oiRealVaultDoor")
		oiRealVaultDoor = GET_WORLD_DOOR_OBJECT(mnDoorModel, vVaultDoorPos)
		
		IF NOT DOES_ENTITY_EXIST(oiRealVaultDoor)
			PRINTLN("[CasinoVaultDoor] oiRealVaultDoor is still null")
		ENDIF
		
		RETURN FALSE
	ENDIF
ENDFUNC

PROC PROCESS_SPAWNED_CASINO_VAULT_DOOR_OBJ(MODEL_NAMES mnDoorModel, VECTOR vVaultDoorPos, BOOL bInteriorReady)
	
	UNUSED_PARAMETER(mnDoorModel)
	UNUSED_PARAMETER(vVaultDoorPos)
	
	IF NOT DOES_ENTITY_EXIST(oiSpawnedVaultDoor_Animated)
		PRINTLN("[CasinoVaultDoor] oiSpawnedVaultDoor_Animated doesn't exist yet!")
		EXIT
	ENDIF
	
	IF IS_ENTITY_VISIBLE(oiSpawnedVaultDoor_Animated)
		IF SHOULD_SPAWNED_CASINO_VAULT_DOOR_HIDE()
			HIDE_FAKE_CASINO_VAULT_DOOR()
		ENDIF
	ENDIF
	
	IF bInteriorReady
		IF GET_INTERIOR_FROM_ENTITY(oiSpawnedVaultDoor_Animated) != interiorCasinoVaultIndex
		OR GET_ROOM_KEY_FROM_ENTITY(oiSpawnedVaultDoor_Animated) != ciVaultDoorRoomKey
			RETAIN_ENTITY_IN_INTERIOR(oiSpawnedVaultDoor_Animated, interiorCasinoVaultIndex)
			FORCE_ROOM_FOR_ENTITY(oiSpawnedVaultDoor_Animated, interiorCasinoVaultIndex, ciVaultDoorRoomKey)
			PRINTLN("[CasinoVaultDoor] Sorting interior/room for oiSpawnedVaultDoor_Animated || GET_ROOM_KEY_FROM_ENTITY(oiSpawnedVaultDoor_Animated): ", GET_ROOM_KEY_FROM_ENTITY(oiSpawnedVaultDoor_Animated))
		ENDIF
		
		IF GET_INTERIOR_FROM_ENTITY(oiVaultDoorDebrisObj) != interiorCasinoVaultIndex
		OR GET_ROOM_KEY_FROM_ENTITY(oiVaultDoorDebrisObj) != ciVaultDoorRoomKey
			RETAIN_ENTITY_IN_INTERIOR(oiVaultDoorDebrisObj, interiorCasinoVaultIndex)
			FORCE_ROOM_FOR_ENTITY(oiVaultDoorDebrisObj, interiorCasinoVaultIndex, ciVaultDoorRoomKey)
			PRINTLN("[CasinoVaultDoor] Sorting interior/room for  oiVaultDoorDebrisObj || GET_ROOM_KEY_FROM_ENTITY(oiVaultDoorDebrisObj): ", GET_ROOM_KEY_FROM_ENTITY(oiVaultDoorDebrisObj))
		ENDIF
	ELSE
		PRINTLN("[CasinoVaultDoor] Interior is not ready")
	ENDIF
	
ENDPROC

PROC PROCESS_CASINO_VAULT_DOOR()
	
	IF NOT IS_BIT_SET(iLocalBoolCheck8, LBOOL8_INITIAL_IPL_SET_UP_COMPLETE)
		PRINTLN("[CasinoVaultDoor_SPAM] Waiting for LBOOL8_INITIAL_IPL_SET_UP_COMPLETE")
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_CASINO_VAULT)
		EXIT
	ENDIF
	
	IF NOT IS_THIS_MISSION_A_CASINO_HEIST_MISSION(g_FMMC_STRUCT.iRootContentIDHash)
		EXIT
	ENDIF
	
	MODEL_NAMES mnDoorModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_vaultdoor01x"))
	MODEL_NAMES mnDoorCollisionModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_des_heist3_vault_end"))
	MODEL_NAMES mnDecalModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_vault_wall_damage"))
	MODEL_NAMES mnAnimDoorModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_DES_heist3_vault_01"))
	MODEL_NAMES mnDebrisModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_DES_heist3_vault_02"))
	VECTOR vVaultDoorPos = (<<2504.9609, -240.3102, -70.0700>>)
	VECTOR vSafeInteriorPos = <<2499.7, -238.5, -70.0>>
	
	VECTOR vBrokenVaultDoorPos = vVaultDoorPos //(<<2499.5, -240.1, -70.7>>) + (<<5.4, -0.22, -0.52>>)
	VECTOR vBrokenVaultDoorRot = (<<0,0,0>>)
	
	IF NOT IS_VALID_INTERIOR(interiorCasinoVaultIndex)
		interiorCasinoVaultIndex = GET_INTERIOR_AT_COORDS(vVaultDoorPos)
	ENDIF
	BOOL bInteriorReady = IS_INTERIOR_READY(interiorCasinoVaultIndex)
	
	PRINTLN("[CasinoVaultDoor_SPAM] PROCESS_CASINO_VAULT_DOOR | eCasinoVaultDoorState: ", eCasinoVaultDoorState)
	
	PROCESS_SPAWNED_CASINO_VAULT_DOOR_OBJ(mnDoorModel, vVaultDoorPos, bInteriorReady)
	
	IF NOT PROCESS_REAL_CASINO_VAULT_DOOR_OBJ(mnDoorModel, vVaultDoorPos)
		EXIT
	ENDIF
	
	SWITCH eCasinoVaultDoorState
		CASE CASINO_VAULT_DOOR_STATE__INIT
			
			REQUEST_MODEL(mnDoorModel)
			REQUEST_MODEL(mnDoorCollisionModel)
			
			REQUEST_ANIM_DICT("anim_heist@hs3f@ig8_vault_explosive_react@left@male@")
			REQUEST_ANIM_DICT("anim_heist@hs3f@ig8_vault_explosive_react@right@male@")
			REQUEST_ANIM_DICT("anim_heist@hs3f@ig8_vault_door_explosion@")
			REQUEST_NAMED_PTFX_ASSET("cut_hs3f")
			
			IF NOT HAS_MODEL_LOADED(mnDoorModel)
				PRINTLN("[CasinoVaultDoor_SPAM] Waiting to load vault door model!")
				EXIT
			ENDIF
			IF NOT HAS_MODEL_LOADED(mnDoorCollisionModel)
				PRINTLN("[CasinoVaultDoor_SPAM] Waiting to load mnDoorCollisionModel!")
				EXIT
			ENDIF
			
			IF NOT HAS_ANIM_DICT_LOADED("anim_heist@hs3f@ig8_vault_explosive_react@left@male@")
			OR NOT HAS_ANIM_DICT_LOADED("anim_heist@hs3f@ig8_vault_explosive_react@right@male@")
				PRINTLN("[CasinoVaultDoor_SPAM] Waiting to load reaction anims!")
				EXIT
			ENDIF
			
			IF NOT HAS_ANIM_DICT_LOADED("anim_heist@hs3f@ig8_vault_door_explosion@")
				PRINTLN("[CasinoVaultDoor_SPAM] Waiting to load explosion anims!")
				EXIT
			ENDIF
			
			IF NOT HAS_NAMED_PTFX_ASSET_LOADED("cut_hs3f")
				PRINTLN("[CasinoVaultDoor_SPAM] Waiting to load explosion PTFX!")
				EXIT
			ENDIF
		
			CREATE_MODEL_HIDE(vVaultDoorPos, 25.0, mnDecalModel, TRUE)
			PRINTLN("[CasinoVaultDoor] PROCESS_CASINO_VAULT_DOOR | Creating model hide for decal model at ", vVaultDoorPos)
			
			fCasinoVaultDoor_CurrentOpenRatio = 0.0
			fCasinoVaultDoor_CurrentSpeed = 0.0
			
			SET_CASINO_VAULT_DOOR_STATE(CASINO_VAULT_DOOR_STATE__CREATING_DOOR_FOR_ANIM)
		BREAK
		
		CASE CASINO_VAULT_DOOR_STATE__CREATING_DOOR_FOR_ANIM
			IF DOES_ENTITY_EXIST(oiSpawnedVaultDoor_Animated)
				SET_CASINO_VAULT_DOOR_STATE(CASINO_VAULT_DOOR_STATE__WAITING)
			ELSE
				oiSpawnedVaultDoor_Animated = CREATE_OBJECT(mnAnimDoorModel, vSafeInteriorPos, FALSE, FALSE, TRUE)
				INIT_VAULT_EXPLOSION_PROP_OBJ(oiSpawnedVaultDoor_Animated, bInteriorReady)
				
				oiSpawnedVaultDoor_StaticCollision = CREATE_OBJECT(mnDoorCollisionModel, vBrokenVaultDoorPos, FALSE, FALSE, TRUE)
				INIT_VAULT_EXPLOSION_PROP_OBJ(oiSpawnedVaultDoor_StaticCollision, bInteriorReady, FALSE, FALSE)
				SET_ENTITY_COMPLETELY_DISABLE_COLLISION(oiSpawnedVaultDoor_StaticCollision, FALSE)
				SET_CAN_AUTO_VAULT_ON_ENTITY(oiSpawnedVaultDoor_StaticCollision, FALSE)
				SET_CAN_CLIMB_ON_ENTITY(oiSpawnedVaultDoor_StaticCollision, FALSE)
				SET_ENTITY_COORDS(oiSpawnedVaultDoor_StaticCollision, vBrokenVaultDoorPos)
				SET_ENTITY_ROTATION(oiSpawnedVaultDoor_StaticCollision, vBrokenVaultDoorRot)
				
				oiVaultDoorDebrisObj = CREATE_OBJECT(mnDebrisModel, vSafeInteriorPos, FALSE, FALSE, TRUE)
				INIT_VAULT_EXPLOSION_PROP_OBJ(oiVaultDoorDebrisObj, bInteriorReady)
			ENDIF
		BREAK
		
		CASE CASINO_VAULT_DOOR_STATE__WAITING
			IF SHOULD_CASINO_VAULT_DOOR_OPEN()
				SET_CASINO_VAULT_DOOR_STATE(CASINO_VAULT_DOOR_STATE__OPENED) // Animation is handled by a mo-cap so we skip straight to Opened
			ENDIF
			
			IF SHOULD_CASINO_VAULT_DOOR_BREAK()
				BREAK_CASINO_VAULT_DOOR(vVaultDoorPos, mnDoorModel, mnDecalModel)
			ENDIF
		BREAK
		
		CASE CASINO_VAULT_DOOR_STATE__HIDDEN
			
		BREAK
		
		CASE CASINO_VAULT_DOOR_STATE__OPENING
			
			// Increase the speed
			IF fCasinoVaultDoor_CurrentOpenRatio < cfCasinoVaultDoor_DoorOpenMaxRatio * 0.5
				fCasinoVaultDoor_CurrentSpeed += GET_FRAME_TIME() * (cfCasinoVaultDoor_DoorOpenAcceleration + (fCasinoVaultDoor_CurrentSpeed * cfCasinoVaultDoor_DoorOpenSpeedBuildupMultiplier))
			ELSE
				fCasinoVaultDoor_CurrentSpeed -= GET_FRAME_TIME() * (cfCasinoVaultDoor_DoorOpenDeceleration + (fCasinoVaultDoor_CurrentSpeed * cfCasinoVaultDoor_DoorOpenSpeedBuildupMultiplier))
			ENDIF
			fCasinoVaultDoor_CurrentSpeed = CLAMP(fCasinoVaultDoor_CurrentSpeed, 0.002, cfCasinoVaultDoor_DoorOpenMaxSpeed)
			
			// Increase the actual ratio using the speed
			fCasinoVaultDoor_CurrentOpenRatio += GET_FRAME_TIME() * fCasinoVaultDoor_CurrentSpeed
			fCasinoVaultDoor_CurrentOpenRatio = CLAMP(fCasinoVaultDoor_CurrentOpenRatio, 0.0, cfCasinoVaultDoor_DoorOpenMaxRatio)
			
			// Apply our vars to the door
			//PROCESS_CHANGES_TO_CASINO_DOOR(ciCasinoVaultDoorHash, fCasinoVaultDoor_CurrentOpenRatio)
			SET_CONTROL_SHAKE(PLAYER_CONTROL, 50, FLOOR(fCasinoVaultDoor_CurrentSpeed * 20))
			
			IF fCasinoVaultDoor_CurrentOpenRatio >= cfCasinoVaultDoor_DoorOpenMaxRatio
				SET_CASINO_VAULT_DOOR_STATE(CASINO_VAULT_DOOR_STATE__OPENED)
			ENDIF
		BREAK
		
		CASE CASINO_VAULT_DOOR_STATE__OPENED
		
		BREAK
		
		CASE CASINO_VAULT_DOOR_STATE__BROKEN
			
		BREAK
		
		CASE CASINO_VAULT_DOOR_STATE__INACTIVE_SYSTEM
			EXIT
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
		CASINO_VAULT_DOOR_DEBUG(vVaultDoorPos)
	#ENDIF
ENDPROC

