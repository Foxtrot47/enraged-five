
USING "FM_Mission_Controller_USING.sch"



//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//
//!!! NEW SECTION BELOW: GENERAL UTILITY FUNCTIONS !!!
//
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
FUNC INT GET_MAXIMUM_PLAYERS_ON_MISSION()
	
	IF g_bForceLegacyMissionControllerMaxPlayerCount
	OR IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL()
		RETURN MAX_NUM_MC_PLAYERS
	ENDIF
	
	RETURN g_FMMC_STRUCT.iNumParticipants + ciMAX_TRANSITION_SESSION_SPECTATORS
	
ENDFUNC
/// PURPOSE:
///    Checks if the object is visible and broken
///    Fixes issues caused by HAS_OBJECT_BEEN_BROKEN returning true if the obejct is invisible (e.g. during cutscenes)
/// PARAMS:
///    oiObj - The object to check
///    bNetworked - passed directly into HAS_OBJECT_BEEN_BROKEN's networked param
FUNC BOOL IS_OBJECT_BROKEN_AND_VISIBLE(OBJECT_INDEX oiObj, BOOL bNetworked = FALSE)
	IF NOT IS_ENTITY_VISIBLE(oiObj)
		RETURN FALSE
	ENDIF
	
	RETURN HAS_OBJECT_BEEN_BROKEN(oiObj, bNetworked)
ENDFUNC

FUNC INT GET_FIRST_SET_BIT_IN_BITSET(INT iBitset)
	
	IF iBitset = 0
		RETURN -1
	ENDIF
	
	INT iBit
	
	FOR iBit = 0 TO 31
		IF IS_BIT_SET(iBitset, iBit)
			RETURN iBit
		ENDIF
	ENDFOR
	
	RETURN -1
	
ENDFUNC

FUNC BOOL IS_DIALOGUE_PLAYING_RIGHT_NOW()
	TEXT_LABEL_23 tlSpeaker = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
	
	RETURN (NOT ARE_STRINGS_EQUAL(tlSpeaker,"NULL"))
ENDFUNC

PROC PLAY_DIALOGUE_TRIGGER_ASAP(INT iDialogueTriggerIndex)
	MC_playerBD[iLocalPart].iPlayThisAudioTriggerASAP_CLIENT = iDialogueTriggerIndex
	PRINTLN("[DialogueTrigger] PLAY_DIALOGUE_TRIGGER_ASAP - Playing trigger ", iDialogueTriggerIndex, " as soon as possible")
ENDPROC

FUNC BOOL IS_SOUND_ID_VALID(INT tempSoundID)
	IF tempSoundID >= 0
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC MC_RESERVE_NETWORK_MISSION_OBJECTS(INT iNumToReserve, BOOL bIgnoreNumReserved = FALSE, BOOL bAllScripts = FALSE)
	INT iInitialObjects = GET_NUM_RESERVED_MISSION_OBJECTS(bAllScripts)
	IF bIgnoreNumReserved
		iInitialObjects = 0
	ENDIF
	RESERVE_NETWORK_MISSION_OBJECTS(iInitialObjects + iNumToReserve)
	DEBUG_PRINTCALLSTACK()
	PRINTLN("[Reserve Obj] MC_RESERVE_NETWORK_MISSION_OBJECTS - Changing number of reserved Objects by ", iNumToReserve, " Reserved Objects is now: ", GET_NUM_RESERVED_MISSION_OBJECTS(bAllScripts))
ENDPROC

PROC HANDLE_HEIST_STATS()
	
	//Completion of specific missions:
	
	IF NOT IS_THIS_IS_A_STRAND_MISSION()
	OR IS_THIS_THE_LAST_IN_A_SERIES_OF_STRAND_MISSIONS()
		IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
			PRINTLN("[RCC MISSION] HANDLE_HEIST_STATS - iCurrentHeistMissionIndex = ",iCurrentHeistMissionIndex)
			PRINTLN("[RCC MISSION] HANDLE_HEIST_STATS - GET_HEIST_MISSION_COMPLETION_STAT = ",GET_HEIST_MISSION_COMPLETION_STAT(iCurrentHeistMissionIndex))
			INCREMENT_MP_INT_CHARACTER_STAT(GET_HEIST_MISSION_COMPLETION_STAT(iCurrentHeistMissionIndex))
			PRINTLN("[RCC MISSION] HANDLE_HEIST_STATS - GET_MP_INT_CHARACTER_STAT = ",GET_MP_INT_CHARACTER_STAT(GET_HEIST_MISSION_COMPLETION_STAT(iCurrentHeistMissionIndex)))
		ENDIF
	ENDIF
	
ENDPROC

PROC CLEANUP_BIRDSEYE_CAM()
	bUsingBirdsEyeCam = FALSE
		
	IF DOES_CAM_EXIST(g_birdsEyeCam)
		IF IS_CAM_ACTIVE(g_birdsEyeCam)
			SET_CAM_ACTIVE(g_birdsEyeCam,FALSE)
		ENDIF
		DESTROY_CAM(g_birdsEyeCam)
		
		PRINTLN("[DODGYCELEBRATIONSCREEN] CLEANUP_BIRDSEYE_CAM() - Killing g_birdsEyeCam")
		DEBUG_PRINTCALLSTACK()
	ENDIF
ENDPROC

PROC SCRIPT_RELEASE_SOUND_ID(INT& tempSoundID)
	IF IS_SOUND_ID_VALID(tempSoundID)
	
		#IF IS_DEBUG_BUILD
			DEBUG_PRINTCALLSTACK()
			PRINTLN("[RCC MISSION] SCRIPT_RELEASE_SOUND_ID - Releasing sound ID: ", tempSoundID)
		#ENDIF
		
		RELEASE_SOUND_ID(tempSoundID)
		tempSoundID = -1
	ENDIF
ENDPROC

FUNC BOOL SHOULD_USE_LOCAL_MINIMUM_VEH_SPEED_CHECK()
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_KEEP_THE_PACE(g_FMMC_STRUCT.iAdversaryModeType) // put old content using the speed bar in here
	OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_STUNTING_PACK(g_FMMC_STRUCT.iAdversaryModeType)
		RETURN TRUE
	ENDIF
	
	RETURN IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_MIN_SPEED_ONLY_CHECK_LOCAL_VEH)
ENDFUNC

PROC DESTROY_ALL_BOMBABLE_BLOCKS()
	INT i
	
	FOR i = 0 TO FMMC_MAX_PROP_BITSET - 1
		MC_serverBD_3.iPropDestroyedBS[i] = SCRIPT_MAX_INT32
	ENDFOR
	
	PRINTLN("DESTROY_ALL_BOMBABLE_BLOCKS - Destroying all bombable blocks now!")
ENDPROC

FUNC MODEL_NAMES GET_LARGE_VERSION_OF_POWERUP(MODEL_NAMES mnPowerup)
	SWITCH ENUM_TO_INT(mnPowerup)
		
		CASE HASH("prop_battle_blastdec")
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_blastdecL"))
		BREAK
		CASE HASH("prop_battle_blastinc")
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_blastincL"))
		BREAK
		CASE HASH("prop_battle_bombdec")
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_bombdecL"))
		BREAK
		CASE HASH("prop_battle_bombinc")
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_bombincL"))
		BREAK
		CASE HASH("prop_battle_lifeinc")
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_lifeincL"))
		BREAK
		CASE HASH("prop_battle_ic_blastdec")
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_blastdecL"))
		BREAK
		CASE HASH("prop_battle_ic_blastdec_green")
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_blastdec_greenL"))
		BREAK
		CASE HASH("prop_battle_ic_blastdec_orange")
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_blastdec_orngL"))
		BREAK
		CASE HASH("prop_battle_ic_blastdec_pink")
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_blastdec_pinkL"))
		BREAK
		CASE HASH("prop_battle_ic_blastdec_purple")
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_blastdec_purpL"))
		BREAK
		
		CASE HASH("prop_battle_ic_blastinc")
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_blastincL"))
		BREAK
		CASE HASH("prop_battle_ic_blastinc_green")
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_blastinc_greenL"))
		BREAK
		CASE HASH("prop_battle_ic_blastinc_orange")
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_blastinc_orngL"))
		BREAK
		CASE HASH("prop_battle_ic_blastinc_pink")
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_blastinc_pinkL"))
		BREAK
		CASE HASH("prop_battle_ic_blastinc_purple")
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_blastinc_purpL"))
		BREAK
		CASE HASH("prop_battle_ic_bombdec")
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_bombdecL"))
		BREAK
		CASE HASH("prop_battle_ic_bombdec_green")
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_bombdec_greenL"))
		BREAK
		CASE HASH("prop_battle_ic_bombdec_orange")
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_bombdec_orngL"))
		BREAK
		CASE HASH("prop_battle_ic_bombdec_pink")
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_bombdec_pinkL"))
		BREAK
		CASE HASH("prop_battle_ic_bombdec_purple")
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_bombdec_purpL"))
		BREAK
		
		CASE HASH("prop_battle_ic_bombinc")
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_bombincL"))
		BREAK
		CASE HASH("prop_battle_ic_bombinc_green")
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_bombinc_greenL"))
		BREAK
		CASE HASH("prop_battle_ic_bombinc_orange")
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_bombinc_orngL"))
		BREAK
		CASE HASH("prop_battle_ic_bombinc_pink")
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_bombinc_pinkL"))
		BREAK
		CASE HASH("prop_battle_ic_bombinc_purple")
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_bombinc_purpL"))
		BREAK
		
		CASE HASH("prop_battle_ic_lifeinc")
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_lifeincL"))
		BREAK
		CASE HASH("prop_battle_ic_lifeinc_green")
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_lifeinc_greenL"))
		BREAK
		CASE HASH("prop_battle_ic_lifeinc_orange")
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_lifeinc_orngL"))
		BREAK
		CASE HASH("prop_battle_ic_lifeinc_pink")
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_lifeinc_pinkL"))
		BREAK
		CASE HASH("prop_battle_ic_lifeinc_purple")
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_lifeinc_purpL"))
		BREAK
		
	ENDSWITCH
	ASSERTLN("GET_LARGE_VERSION_OF_POWERUP - Powerup ", GET_MODEL_NAME_FOR_DEBUG(mnPowerup), " has no large counterpart set!")
	PRINTLN("GET_LARGE_VERSION_OF_POWERUP - Powerup ", GET_MODEL_NAME_FOR_DEBUG(mnPowerup), " has no large counterpart set!")
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

PROC HIDE_RADAR_THIS_FRAME()
	SET_BIT(iLocalBoolCheck28, LBBOOL28_HIDE_RADAR_THIS_FRAME)
	iRadarHiddenFrame = GET_FRAME_COUNT() + 1
ENDPROC

FUNC BOOL IS_CARGOBOB_MANUAL_RESPAWN_OVERRIDE()
	INT iTeam = MC_playerBD[iPartToUse].iteam
	IF iTeam < FMMC_MAX_TEAMS
		INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
		IF iRule < FMMC_MAX_RULES
			RETURN (g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iCBManRespawnExclusionRadius[iRule] != 0
				AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[0][0].iSpawnBitSet, ci_SpawnBS_AirSpawn))
		ENDIF
		RETURN FALSE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_THIS_MODE_HAVE_NO_TIME_LIMIT()
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_STUNTING_PACK(g_FMMC_STRUCT.iAdversaryModeType)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_REASON_HUD_IS_HIDDEN()
	RETURN iHUDHiddenReason
ENDFUNC

FUNC BOOL IS_HACK_START_SOUND_BLOCKED()
	IF iSpectatorTarget > -1
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_HACK_STOP_SOUND_BLOCKED()
	IF iHackPercentage = 100 OR iHackPercentage = 0
		RETURN TRUE
	ENDIF
	
	IF iSpectatorTarget > -1
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_INTRO_SHARD_BEGUN()
	RETURN HAS_NET_TIMER_STARTED(jobIntroData.jobIntroTextTimer)
ENDFUNC

FUNC BOOL IS_INTRO_SHARD_AT_BOTTOM_OF_SCREEN(BOOL bUseSlightDelayAfter = FALSE)
	IF HAS_NET_TIMER_STARTED(jobIntroData.jobIntroTextTimer)
		PRINTLN("IS_INTRO_SHARD_AT_BOTTOM_OF_SCREEN - ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(jobIntroData.jobIntroTextTimer))
		IF (bUseSlightDelayAfter AND GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(jobIntroData.jobIntroTextTimer) >= 1150)
		OR (NOT bUseSlightDelayAfter AND GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(jobIntroData.jobIntroTextTimer) >= 400)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC HANDLE_AIRQUOTA_RADIO()
	IF NOT IS_PLAYER_RESPAWNING(LocalPlayer)
	AND IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		IF IS_MOBILE_PHONE_RADIO_ACTIVE()
//			//PRINTLN("[AirQuota][TMS] Mobile phone radio is ACTIVE")
//			//PRINTLN("[AirQuota][TMS] Radio station is: ", GET_PLAYER_RADIO_STATION_INDEX())
//			g_SpawnData.iRadioStation = GET_PLAYER_RADIO_STATION_INDEX()
//			
//			//DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(LocalPlayerPed), 10, 255, 5, 5, 200)
//		ELSE
//			IF GET_PLAYER_RADIO_STATION_INDEX() <> 255
//				IF NOT IS_RADIO_RETUNING()
//					PRINTLN("[AirQuota][TMS] Mobile phone radio is !!NOT!! ACTIVE - Enabling it now - Station is ", GET_PLAYER_RADIO_STATION_INDEX())
//					SET_MOBILE_RADIO_ENABLED_DURING_GAMEPLAY(TRUE)
//					SET_MOBILE_PHONE_RADIO_STATE(TRUE)
//				ENDIF
//			ELSE
//				PRINTLN("[AirQuota][TMS] Mobile radio is off but that's okay because the radio is off in general - ", GET_PLAYER_RADIO_STATION_INDEX())
//				SET_MOBILE_PHONE_RADIO_STATE(FALSE)
//				SET_VEHICLE_RADIO_ENABLED(GET_VEHICLE_PED_IS_IN(LocalPlayerPed), FALSE)
//				SET_RADIO_TO_STATION_INDEX(255)
//			ENDIF
		ENDIF
		
//		IF GET_PLAYER_RADIO_STATION_INDEX() = iTransformRadioStation
//		OR iTransformRadioStation = 255
//			iTransformRadioStation = -1
//				
//			ASSERTLN("[AirQuota][TMS] Setting iTransformRadioStation to -1 because GET_PLAYER_RADIO_STATION_INDEX() is returning: ", GET_PLAYER_RADIO_STATION_INDEX())
//		ENDIF
//		
//		IF iTransformRadioStation > -1
//		AND GET_PLAYER_RADIO_STATION_INDEX() != iTransformRadioStation
//		AND sVehicleSwap.eVehicleSwapState = eVehicleSwapState_IDLE
//			SET_RADIO_TO_STATION_INDEX(iTransformRadioStation)
//			SET_VEH_RADIO_STATION(GET_VEHICLE_PED_IS_IN(LocalPlayerPed), GET_RADIO_STATION_NAME(iTransformRadioStation))
//			ASSERTLN("[AirQuota][TMS] Setting radio station to iTransformRadioStation, which is: ", iTransformRadioStation, ", GET_PLAYER_RADIO_STATION_INDEX() is returning: ", GET_PLAYER_RADIO_STATION_INDEX())
//		ENDIF
		
	ENDIF
	
	//PRINTLN("[AirQuota][TMS] iTransformRadioStation: ", iTransformRadioStation)
ENDPROC

//Last intro camera pan before gameplay starts
FUNC BOOL IS_INTRO_IN_LAST_CAMERA_PAN()
	RETURN jobIntroData.sNewAnimData.iBlendStage = ciNEW_BLEND_PROCESS
ENDFUNC

FUNC INT GET_RANDOM_INDEX(INT iElements)
	INT iRand = GET_RANDOM_INT_IN_RANGE(0, ciRANDOM_INDEX_ACCURACY)
	PRINTLN("GET_RANDOM_INDEX - iRand = ", iRand)
	
	INT i, iIdxMin, iIdxMax, iIncrement
	
	iIncrement = ciRANDOM_INDEX_ACCURACY / iElements
	
	iIdxMin = 0
	iIdxMax = iIncrement
	
	FOR i = 0 TO iElements-1
		IF iRand >= iIdxMin AND iRand < iIdxMax
			RETURN i
		ENDIF
		
		iIdxMin = iIdxMax
		iIdxMax += iIncrement
	ENDFOR
	
	RETURN -1
ENDFUNC

//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: GAME STATE GETTERS !
//
//************************************************************************************************************************************************************



/// PURPOSE:
///    Helper function to get the servers game/mission state
FUNC INT GET_MC_SERVER_GAME_STATE()

	RETURN MC_serverBD.iServerGameState
	
ENDFUNC	

/// PURPOSE:
///    Gets the local players client mission stage.
FUNC INT GET_MC_CLIENT_MISSION_STAGE(INT ipart)

	RETURN MC_playerBD[ipart].iClientLogicStage
	
ENDFUNC

/// PURPOSE:
///    Helper function to get a clients game state
FUNC INT GET_MC_CLIENT_GAME_STATE(INT iPart)

	RETURN MC_playerBD[iPart].iGameState
	
ENDFUNC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: PLAYER / PARTICIPANT VARIABLES !
//
//************************************************************************************************************************************************************

//IF arg1 = TRUE, RETURN arg2, ELSE, return arg3
FUNC BOOL TERNARY_BOOL(BOOL bEval, BOOL bTrueReturn, BOOL bFalseReturn)
	IF bEval
		RETURN bTrueReturn
	ELSE
		RETURN bFalseReturn
	ENDIF
ENDFUNC

/// PURPOSE: Caches all the local player variables used
///    DOES NOT TAKE SPECTATORS INTO ACCOUNT
PROC CACHE_LOCAL_PLAYER_VARIABLES()
	iPartToUse = PARTICIPANT_ID_TO_INT()
	PlayerToUse = PLAYER_ID()
	LocalPlayerPed = PLAYER_PED_ID()
	LocalPlayer = PLAYER_ID()
	
	IF IS_NET_PLAYER_OK( LocalPlayer )
		bPlayerToUseOK = TRUE
	ENDIF
ENDPROC

PROC RE_CACHE_LOCAL_PLAYER_PED_ID()
	IF LocalPlayerPed != PLAYER_PED_ID()
		PRINTLN("[RCC MISSION] - RE_CACHE_LOCAL_PLAYER_PED_ID - LocalPlayer = PLAYER_PED_ID()")
		LocalPlayerPed = PLAYER_PED_ID()
	ENDIF
ENDPROC


FUNC INT GET_REMAINING_TIME_ON_RULE_SUDDENDEATH(INT iTeam, INT iRule, BOOL bUseMultiRuleTimer = FALSE)
	IF iTeam < FMMC_MAX_TEAMS
	AND iRule < FMMC_MAX_RULES
	
		IF (IS_BIT_SET(MC_serverBD.iServerBitSet1,SBBOOL1_SUDDEN_DEATH)	AND IS_BIT_SET(MC_ServerBD.iServerBitSet6, SBBOOL6_SUDDEN_DEATH_RUNNING_BACK_REMIX))
			RETURN ((GET_FMMC_OBJECTIVE_TIME_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjectiveTimeLimitRule[iRule]) + GET_FMMC_OBJECTIVE_TIME_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjectiveSuddenDeathTimeLimit[iRule])) - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdObjectiveLimitTimer[MC_playerBD[iPartToUse].iteam]) - MC_serverBD_3.iTimerPenalty[MC_playerBD[iPartToUse].iteam])
		ENDIF
		
		IF NOT bUseMultiRuleTimer
			IF HAS_NET_TIMER_STARTED(MC_serverBD_3.tdObjectiveLimitTimer[iTeam])
				RETURN ((GET_FMMC_OBJECTIVE_TIME_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjectiveTimeLimitRule[iRule]) + 
						 GET_FMMC_OBJECTIVE_TIME_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjectiveSuddenDeathTimeLimit[iRule])) - 
						 GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdObjectiveLimitTimer[iTeam]) - MC_serverBD_3.iTimerPenalty[iTeam])
			ENDIF
		ELSE
			IF HAS_NET_TIMER_STARTED(MC_serverBD_3.tdMultiObjectiveLimitTimer[iTeam])
				IF (IS_BIT_SET(MC_serverBD.iServerBitSet1,SBBOOL1_SUDDEN_DEATH)	AND IS_BIT_SET(MC_ServerBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_SUMO_PENNED_IN) AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_USE_SD_SCORE_FOR_SUMO_SD))
					PRINTLN("[GET_REMAINING_TIME_ON_RULE_SUDDENDEATH][RH] Using time")
					RETURN ((MC_serverBD_3.iMultiObjectiveTimeLimit[iTeam]  + (g_FMMC_STRUCT.iSphereShrinkTime + g_FMMC_STRUCT.iTimeToGetInsideSphere)) - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdMultiObjectiveLimitTimer[iTeam]) - MC_serverBD_3.iTimerPenalty[iTeam])
				ELSE
					RETURN ((MC_serverBD_3.iMultiObjectiveTimeLimit[iTeam] + 
						 GET_FMMC_OBJECTIVE_TIME_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjectiveSuddenDeathTimeLimit[iRule])) - 
						 GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdMultiObjectiveLimitTimer[iTeam]))
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF
	RETURN -1
ENDFUNC

FUNC INT GET_REMAINING_TIME_ON_RULE(INT iTeam, INT iRule)
	
	IF iTeam < FMMC_MAX_TEAMS
	AND iRule < FMMC_MAX_RULES
	AND HAS_NET_TIMER_STARTED(MC_serverBD_3.tdObjectiveLimitTimer[iTeam])
		RETURN (GET_FMMC_OBJECTIVE_TIME_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjectiveTimeLimitRule[iRule]) - 
			    GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdObjectiveLimitTimer[iTeam]) - MC_serverBD_3.iTimerPenalty[iTeam])
	ENDIF
	RETURN -1
ENDFUNC

FUNC INT GET_TIME_REMAINING_ON_MULTIRULE_TIMER(INT iTeamToUse = -1)
	IF iTeamToUse = -1
		iTeamToUse = MC_playerBD[iPartToUse].iteam
	ENDIF
	RETURN MC_serverBD_3.iMultiObjectiveTimeLimit[iTeamToUse] - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdMultiObjectiveLimitTimer[iTeamToUse])
ENDFUNC

FUNC INT GET_REMAINING_TIME_ON_SHRINKING_BOUNDS(INT iTeam)
	
	IF iTeam < FMMC_MAX_TEAMS
	AND HAS_NET_TIMER_STARTED(MC_serverBD.tdSuddenDeath_ShrinkingBoundsHUDTimer[iTeam])
		RETURN (g_FMMC_STRUCT.iSphereShrinkTime - 
			    GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdSuddenDeath_ShrinkingBoundsHUDTimer[iTeam]))
	ENDIF
	RETURN -1
ENDFUNC

//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: PLAYER PROOFS !
//
//************************************************************************************************************************************************************



PROC RUN_LOCAL_PLAYER_PROOF_SETTING(BOOL bForce = FALSE, BOOL bOverrideExplosionProof = FALSE)
	
	BOOL bBulletProof = IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].iTeamBitset, ciBS_Player_BulletProofFlag)
	BOOL bFlameProof = IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].iTeamBitset, ciBS_Player_FlameProofFlag)
	BOOL bExplosionProof = IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].iTeamBitset, ciBS_Player_ExplosionProofFlag) OR bOverrideExplosionProof
	BOOL bCollisionProof = IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].iTeamBitset, ciBS_Player_CollisionProofFlag)
	BOOL bMeleeProof = IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].iTeamBitset, ciBS_Player_MeleeProofFlag)
	BOOL bSteamProof = IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].iTeamBitset, ciBS_Player_SteamProofFlag)
	BOOL bSmokeProof = IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].iTeamBitset, ciBS_Player_SmokeProofFlag)
	BOOL bForceEntry
	
	IF NOT bFlameProof
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF IS_ENTITY_ALIVE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
				IF GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) = SCARAB
				AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].iTeamBitset4, ciBS4_TEAM_VEHICLE_DISABLE_SCARAB_FIREPROOF)
					bForceEntry = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_FAKE_MULTIPLAYER_MODE_SET(TRUE) OR bForce
		IF bBulletProof OR bFlameProof OR bExplosionProof OR bCollisionProof OR bMeleeProof OR bSteamProof OR bSmokeProof OR bForce OR bForceEntry
			PRINTLN("[RCC MISSION] RUN_LOCAL_PLAYER_PROOF_SETTING - bullet ",bBulletProof,", flame ",bFlameProof,", explosion ",bExplosionProof,", collision ",bCollisionProof,", melee ",bMeleeProof,", steam ",bSteamProof,", smoke ",bSmokeProof)
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				SET_ENTITY_PROOFS(PLAYER_PED_ID(),bBulletProof,bFlameProof,bExplosionProof,bCollisionProof,bMeleeProof,bSteamProof,DEFAULT,bSmokeProof)
			#IF IS_DEBUG_BUILD
			ELSE
				PRINTLN("[RCC MISSION] RUN_LOCAL_PLAYER_PROOF_SETTING - player ped is injured!")
			#ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_FAKE_MULTIPLAYER_MODE_SET(TRUE) 
	AND NOT (g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_CTF OR g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_LTS)
		SET_ENTITY_PROOFS(PLAYER_PED_ID(),TRUE,TRUE,TRUE,TRUE,TRUE,TRUE,TRUE,TRUE) 
	ENDIF
		
ENDPROC

PROC SET_PLAYER_PROOFS_FOR_TEAM(INT iTeam)
	
	BOOL bBulletProof = IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitset, ciBS_Player_BulletProofFlag)
	BOOL bFlameProof = IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitset, ciBS_Player_FlameProofFlag)
	BOOL bExplosionProof = IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitset, ciBS_Player_ExplosionProofFlag)
	BOOL bCollisionProof = IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitset, ciBS_Player_CollisionProofFlag)
	BOOL bMeleeProof = IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitset, ciBS_Player_MeleeProofFlag)
	BOOL bSteamProof = IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitset, ciBS_Player_SteamProofFlag)
	BOOL bSmokeProof = IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitset, ciBS_Player_SmokeProofFlag)
	
	IF NOT IS_FAKE_MULTIPLAYER_MODE_SET(TRUE)
		PRINTLN("[RCC MISSION] SET_PLAYER_PROOFS_FOR_TEAM - bullet ",bBulletProof,", flame ",bFlameProof,", explosion ",bExplosionProof,", collision ",bCollisionProof,", melee ",bMeleeProof,", steam ",bSteamProof,", smoke ",bSmokeProof)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			SET_ENTITY_PROOFS(PLAYER_PED_ID(),bBulletProof,bFlameProof,bExplosionProof,bCollisionProof,bMeleeProof,bSteamProof,DEFAULT,bSmokeProof)
		#IF IS_DEBUG_BUILD
		ELSE
			PRINTLN("[RCC MISSION] SET_PLAYER_PROOFS_FOR_TEAM - player ped is injured!")
		#ENDIF
		ENDIF
	ENDIF
	
ENDPROC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: TEAM-RELATED FUNCTIONS !
//
//************************************************************************************************************************************************************


FUNC STRING GET_TEAM_NAME_FROM_OUTFIT(INT iTeam)

	TEXT_LABEL_15 tlAltTeam
	//STRING strTemp
	
	tlAltTeam = GET_ALTERNATIVE_TEAM_NAME(MC_serverBD_3.iVersusOutfitStyleSetup, INT_TO_ENUM(VERSUS_OUTFIT_STYLE, MC_serverBD_3.iVersusOutfitStyleChoice), iTeam)
	//strTemp = TEXT_LABEL_INTO_STRING(tlAltTeam)

	RETURN GET_FILENAME_FOR_AUDIO_CONVERSATION(tlAltTeam)
	
ENDFUNC

FUNC BOOL DOES_ANY_TEAM_HAVE_NEW_PRIORITY_THIS_FRAME(BOOL bAlsoCheckMidpoint = FALSE)
	
	BOOL bNewRule
	
	INT iTeam
	
	FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
		IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam)
		OR (bAlsoCheckMidpoint AND IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_MIDPOINT_THIS_FRAME + iTeam))
			bNewRule = TRUE
			iTeam = MC_serverBD.iNumberOfTeams // Break out!
		ENDIF
	ENDFOR
	
	RETURN bNewRule
	
ENDFUNC

FUNC BOOL HAS_TEAM_FAILED(INT iteam)
	
	SWITCH iteam
	
		CASE 0
			IF IS_BIT_SET(MC_serverBD.iServerBitSet,SSBOOL_TEAM0_FAILED)
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE 1
			IF IS_BIT_SET(MC_serverBD.iServerBitSet,SSBOOL_TEAM1_FAILED)
				RETURN TRUE
			ENDIF
		BREAK
		CASE 2
			IF IS_BIT_SET(MC_serverBD.iServerBitSet,SSBOOL_TEAM2_FAILED)
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE 3
			IF IS_BIT_SET(MC_serverBD.iServerBitSet,SSBOOL_TEAM3_FAILED)
				RETURN TRUE
			ENDIF
		BREAK
		
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL HAS_TEAM_FINISHED(INT iteam)
	
	SWITCH iteam
		
		CASE 0
			IF IS_BIT_SET(MC_serverBD.iServerBitSet,SSBOOL_TEAM0_FINISHED)
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE 1
			IF IS_BIT_SET(MC_serverBD.iServerBitSet,SSBOOL_TEAM1_FINISHED)
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE 2
			IF IS_BIT_SET(MC_serverBD.iServerBitSet,SSBOOL_TEAM2_FINISHED)
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE 3
			IF IS_BIT_SET(MC_serverBD.iServerBitSet,SSBOOL_TEAM3_FINISHED)
				RETURN TRUE
			ENDIF
		BREAK
		
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL HAVE_ALL_TEAMS_FINISHED()
	
	IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet,SSBOOL_TEAM0_FINISHED)
		RETURN FALSE
	ENDIF
	
	IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet,SSBOOL_TEAM1_FINISHED)
		RETURN FALSE
	ENDIF
	
	IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet,SSBOOL_TEAM2_FINISHED)
		RETURN FALSE
	ENDIF
	
	IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet,SSBOOL_TEAM3_FINISHED)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL IS_TEAM_SET_AS_CRITICAL(INT iteam)
	
	#IF IS_DEBUG_BUILD
	IF NOT IS_TRANSITION_DEBUG_LAUNCH_VAR_SET()
	#ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iteam].iTeamBitset, ciBS_TEAM_MISSION_CRITIAL)
		RETURN TRUE
	ENDIF
	#IF IS_DEBUG_BUILD
	ENDIF
	#ENDIF
	
	IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
	#IF IS_DEBUG_BUILD
	AND NOT IS_TRANSITION_DEBUG_LAUNCH_VAR_SET()
	#ENDIF
		IF Is_Player_Currently_On_MP_Heist(LocalPlayer)
		OR Is_Player_Currently_On_MP_Heist_Planning(LocalPlayer)		// KGM 5/3/14: Added Heist Planning check
			RETURN TRUE
		ENDIF
	ENDIF
	
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_TEAM_ACTIVE(INT iteam)

	#IF IS_DEBUG_BUILD
	IF GlobalServerBD_DM.bOnePlayerDeathmatch
	
		RETURN TRUE
	ENDIF
	#ENDIF
	
	IF iteam = 0
		IF IS_BIT_SET(MC_serverbd.iServerBitSet, SBBOOL_TEAM_0_ACTIVE)
			IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet,SSBOOL_TEAM0_FINISHED)
				RETURN TRUE
			ENDIF
		ENDIF
	ELIF iteam = 1
		IF IS_BIT_SET(MC_serverbd.iServerBitSet, SBBOOL_TEAM_1_ACTIVE)
			IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet,SSBOOL_TEAM1_FINISHED)
				RETURN TRUE
			ENDIF
		ENDIF
	ELIF iteam = 2
		IF IS_BIT_SET(MC_serverbd.iServerBitSet, SBBOOL_TEAM_2_ACTIVE)
			IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet,SSBOOL_TEAM2_FINISHED)
				RETURN TRUE
			ENDIF
		ENDIF
	ELIF iteam = 3
		IF IS_BIT_SET(MC_serverbd.iServerBitSet, SBBOOL_TEAM_3_ACTIVE)
			IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet,SSBOOL_TEAM3_FINISHED)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC INT GET_NUM_ACTIVE_TEAMS()
	INT i, iActiveTeams
	
	FOR i = 0 TO FMMC_MAX_TEAMS

		IF IS_BIT_SET(MC_serverbd.iServerBitSet, SBBOOL_TEAM_0_ACTIVE + i)
			IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet,SSBOOL_TEAM0_FINISHED + i)
				iActiveTeams++
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN iActiveTeams
	
ENDFUNC

FUNC INT GET_PRE_ASSIGNMENT_PLAYER_TEAM()
	
	INT iTeam
	
	IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()		
		DEBUG_PRINTCALLSTACK()
		
		IF GET_PLAYER_TEAM(localPlayer) > -1
			PRINTLN("[RCC MISSION] GET_PRE_ASSIGNMENT_PLAYER_TEAM - IS_FAKE_MULTIPLAYER_MODE_SET() = FALSE, Using GET_PLAYER_TEAM(): ", GET_PLAYER_TEAM(localPlayer), " as a team has been assigned now... (Implemented because of team rebalancing) url:bugstar:3583710")
			iTeam = GET_PLAYER_TEAM(localPlayer)
		ELSE
			iTeam = GlobalplayerBD_FM[NATIVE_TO_INT(GET_PLAYER_INDEX())].sClientCoronaData.iTeamChosen
			PRINTLN("[RCC MISSION] GET_PRE_ASSIGNMENT_PLAYER_TEAM - IS_FAKE_MULTIPLAYER_MODE_SET() = FALSE, setting iTeam = GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].sClientCoronaData.iTeamChosen : ",GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].sClientCoronaData.iTeamChosen) 
		ENDIF
	ELSE
		iTeam = g_FMMC_STRUCT.iTestMyTeam
		PRINTLN("[RCC MISSION] GET_PRE_ASSIGNMENT_PLAYER_TEAM - IS_FAKE_MULTIPLAYER_MODE_SET() = TRUE, setting MC_playerBD[iLocalPart].iteam = g_FMMC_STRUCT.iTestMyTeam")
	ENDIF
	
	PRINTLN("[RCC MISSION] GET_PRE_ASSIGNMENT_PLAYER_TEAM - PLAYER TEAM: ",iTeam)
	
	IF iTeam < 0
		PRINTLN("GET_PRE_ASSIGNMENT_PLAYER_TEAM - TEAM IS COMING THROUGH AS LESS THAN 0, ADD A BUG")
		SCRIPT_ASSERT("GET_PRE_ASSIGNMENT_PLAYER_TEAM - TEAM IS COMING THROUGH AS LESS THAN 0, ADD A BUG")
		iTeam = 0
	ENDIF
	
	IF iTeam >=FMMC_MAX_TEAMS
		IF NOT IS_PLAYER_SCTV(GET_PLAYER_INDEX())
			PRINTLN("GET_PRE_ASSIGNMENT_PLAYER_TEAM - TEAM IS COMING THROUGH MORE THAN FMMC_MAX_TEAMS, ADD A BUG")
			SCRIPT_ASSERT("GET_PRE_ASSIGNMENT_PLAYER_TEAM - TEAM IS COMING THROUGH MORE THAN FMMC_MAX_TEAMS, ADD A BUG")
		ENDIF
		iTeam = 3
	ENDIF
	
	RETURN iTeam
	
ENDFUNC

//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: SPECTATOR FUNCTIONS !
//
//************************************************************************************************************************************************************



FUNC BOOL IS_PARTICIPANT_A_NON_HEIST_SPECTATOR( INT iParticipant )
	IF IS_BIT_SET(MC_playerBD[ iParticipant ].iClientBitSet, PBBOOL_ANY_SPECTATOR )
	AND NOT IS_BIT_SET(MC_playerBD[ iParticipant ].iClientBitSet, PBBOOL_HEIST_SPECTATOR )
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_ANY_SPECTATOR()

	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF IS_BIT_SET(iLocalBoolCheck3,LBOOL3_LATE_SPECTATE)
		OR IS_BIT_SET(iLocalBoolCheck3,LBOOL3_WAIT_END_SPECTATE)
		OR IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet,PBBOOL_ANY_SPECTATOR)
		OR IS_PLAYER_SCTV(LocalPlayer)
		OR USING_HEIST_SPECTATE()
		OR IS_THIS_SPECTATOR_CAM_ACTIVE(g_BossSpecData.specCamData)
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_PLAYER_SPECTATOR_ONLY(PLAYER_INDEX playerId)

	IF DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(playerId)
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_SCTV(playerId)
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

PROC FIND_PREFERRED_SPECTATOR_TARGET()
	
	IF g_FMMC_STRUCT.iDefaultSpectatorTeam != -1
		
		CLEAR_BIT(MPSpecGlobals.SCTVData.iBitSet, SCTV_BIT_ALLOW_SPECTATING_RIVAL_TEAMS)
		MPSpecGlobals.iPreferredSpectatorPlayerID = -1
		INT iPart
		
		FOR iPart = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
			IF (MC_playerBD[iPart].iteam = g_FMMC_STRUCT.iDefaultSpectatorTeam)
			AND NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPart))
				PLAYER_INDEX tempPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPart))
				IF IS_NET_PLAYER_OK(tempPlayer)
					SET_BIT(MPSpecGlobals.SCTVData.iBitSet, SCTV_BIT_ALLOW_SPECTATING_RIVAL_TEAMS)
					MPSpecGlobals.iPreferredSpectatorPlayerID = NATIVE_TO_INT(tempPlayer)
					CPRINTLN(DEBUG_SPECTATOR, " - MISSION CONTROLLER - FIND_PREFERRED_SPECTATOR_TARGET - SCTV_BIT_ALLOW_SPECTATING_RIVAL_TEAMS")
				ENDIF
			ENDIF
		ENDFOR
		
	ENDIF
	
ENDPROC

FUNC BOOL IS_PARTICIPANT_A_SPECTATOR(INT iParticipant)

	PARTICIPANT_INDEX thisParticipant = INT_TO_PARTICIPANTINDEX( iParticipant )
	IF NETWORK_IS_PARTICIPANT_ACTIVE( thisParticipant )
		PLAYER_INDEX thisPlayer = NETWORK_GET_PLAYER_INDEX( thisParticipant )
		// And if they exist
		IF ( thisPlayer != INVALID_PLAYER_INDEX() )
			// The important check - we don't want to change the scores if the player that just joined is spectating
			IF IS_PLAYER_SPECTATING( thisPlayer )
			OR IS_PLAYER_IN_PROCESS_OF_JOINING_MISSION_AS_SPECTATOR( thisPlayer )
			OR DID_PLAYER_JOIN_MISSION_AS_SPECTATOR( thisPlayer )
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE

ENDFUNC

FUNC INT GET_SPECTATOR_TARGET_PARTICIPANT_OF_PARTICIPANT( INT iSpectatorPart )
	
	INT iReturnTarget = -1
	
	PARTICIPANT_INDEX partSpectator = INT_TO_PARTICIPANTINDEX( iSpectatorPart ) 
	
	IF NETWORK_IS_PARTICIPANT_ACTIVE( partSpectator )
	AND IS_NET_PLAYER_OK( NETWORK_GET_PLAYER_INDEX( partSpectator ) )
		iReturnTarget = MC_playerBD[iSpectatorPart].iPartIAmSpectating
	ENDIF
	
	RETURN iReturnTarget
ENDFUNC

//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//
//!!! NEW SECTION BELOW: ENTITY SPAWNING/CLEANUP !!!
//
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************

FUNC BOOL HAS_ENTITY_SPAWN_ZONE_REQUIREMENT_BEEN_MET(INT iZone, INT iPlayerReq)
	IF iZone < 0
		RETURN FALSE
	ENDIF
	IF IS_BIT_SET(iZoneBS_PlayersInside[iZone], ciZoneBS_PlayersInside_1+(iPlayerReq-1))
		RETURN TRUE
	ENDIF	
	IF iPlayerReq = 0
		IF IS_BIT_SET(iZoneBS_PlayersInside[iZone], ciZoneBS_PlayersInside_All)
			RETURN TRUE
		ENDIF	
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(INT iAssociatedSpawn, INT iAssociatedTeam, INT iAssociatedObjective, BOOL bPropAction = FALSE, INT iScoreRequired = 0, INT iAlwaysSpawnOnRule = -1)
	
	BOOL bSpawn
	
	PRINTLN("[TMSTEMP] HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS - iAssociatedSpawn: ", iAssociatedSpawn, " || iAssociatedObjective: ", iAssociatedObjective, " || iAssociatedTeam: ", iAssociatedTeam, " || iScoreRequired: ", iScoreRequired)
	
	SWITCH iAssociatedSpawn
		CASE ciOBJECTIVE_SPAWN_LIMIT_OFF
			PRINTLN("HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS - ciOBJECTIVE_SPAWN_LIMIT_OFF - Objective ", iAssociatedObjective, " - Setting bSpawn to TRUE due to ciOBJECTIVE_SPAWN_LIMIT_OFF")
			bSpawn = TRUE
		BREAK
		CASE ciOBJECTIVE_SPAWN_LIMIT_ON
			IF iAssociatedObjective <= MC_serverBD_4.iCurrentHighestPriority[iAssociatedTeam]
				PRINTLN("[TMSTEMP] HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS - ciOBJECTIVE_SPAWN_LIMIT_ON - Objective ", iAssociatedObjective, " - Setting bSpawn to TRUE due to iAssociatedObjective being <= current rule")
				bSpawn = TRUE
			ELSE
				PRINTLN("[TMSTEMP] HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS - ciOBJECTIVE_SPAWN_LIMIT_ON - Objective ", iAssociatedObjective, " is too high for us to spawn right now || MC_serverBD_4.iCurrentHighestPriority[iAssociatedTeam]: ", MC_serverBD_4.iCurrentHighestPriority[iAssociatedTeam])
			ENDIF
		BREAK
		CASE ciOBJECTIVE_SPAWN_LIMIT_ON_OBJECTIVE_ONLY
			IF NOT bPropAction // This option doesn't exist for prop action triggering?
			AND iAssociatedObjective = MC_serverBD_4.iCurrentHighestPriority[iAssociatedTeam]
				PRINTLN("HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS - ciOBJECTIVE_SPAWN_LIMIT_ON_OBJECTIVE_ONLY - Objective ", iAssociatedObjective, " - Setting bSpawn to TRUE due to iAssociatedObjective being = current rule")
				bSpawn = TRUE
			ENDIF
		BREAK
		CASE ciOBJECTIVE_SPAWN_LIMIT_ON_COLLECTION
			IF iAssociatedObjective = MC_serverBD_4.iCurrentHighestPriority[iAssociatedTeam]
				IF IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iAssociatedTeam], iAssociatedObjective)
					PRINTLN("HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS - ciOBJECTIVE_SPAWN_LIMIT_ON_COLLECTION - Objective ", iAssociatedObjective, " - Setting bSpawn to TRUE due to iObjectiveMidPointBitset")
					bSpawn = TRUE
				ENDIF
			ELIF iAssociatedObjective < MC_serverBD_4.iCurrentHighestPriority[iAssociatedTeam]
				PRINTLN("HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS - ciOBJECTIVE_SPAWN_LIMIT_ON_COLLECTION - Objective ", iAssociatedObjective, " - Setting bSpawn to TRUE due to iAssociatedObjective being < current rule")
				bSpawn = TRUE
			ENDIF
		BREAK
		CASE ciOBJECTIVE_SPAWN_LIMIT_OnImminentAggroFail
			IF SVM_FLOW_IS_CURRENT_MISSION_SVM_FLOW()	
			OR WVM_FLOW_IS_CURRENT_MISSION_WVM_FLOW()
			OR AM_I_ON_A_HEIST()
				// Keeping old functionality as to not break old missions with this ELSE fix.
				IF IS_BIT_SET(MC_serverBD.iServerBitSet1,SBBOOL1_AGGRO_TRIGGERED_FOR_TEAM_0 + iAssociatedTeam)
					PRINTLN("HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS - ciOBJECTIVE_SPAWN_LIMIT_OnImminentAggroFail - Objective ", iAssociatedObjective, " - 1")
					bSpawn = TRUE
				ENDIF
			ELSE
				IF iAssociatedObjective <= MC_serverBD_4.iCurrentHighestPriority[iAssociatedTeam]
					IF IS_BIT_SET(MC_serverBD.iServerBitSet1,SBBOOL1_AGGRO_TRIGGERED_FOR_TEAM_0 + iAssociatedTeam)
						PRINTLN("HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS - ciOBJECTIVE_SPAWN_LIMIT_OnImminentAggroFail - Objective ", iAssociatedObjective, " - 2")
						bSpawn = TRUE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE ciOBJECTIVE_SPAWN_LIMIT_ON_SCORE_REACHED
			PRINTLN("[TMSTEMP] HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS - iScore on Rule: ", MC_ServerBD.iScoreOnThisRule[iAssociatedTeam], " iRule: ", MC_serverBD_4.iCurrentHighestPriority[iAssociatedTeam])
			
			IF MC_ServerBD.iScoreOnThisRule[iAssociatedTeam] >= iScoreRequired
			AND iAssociatedObjective <= MC_serverBD_4.iCurrentHighestPriority[iAssociatedTeam]
				PRINTLN("HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS - ciOBJECTIVE_SPAWN_LIMIT_ON_SCORE_REACHED - Objective ", iAssociatedObjective, " - Score is high enough and iAssociatedObjective <= current rule")
				bSpawn = TRUE
			ENDIF
			IF iAssociatedTeam > -1
				IF iAlwaysSpawnOnRule > -1
				AND MC_serverBD_4.iCurrentHighestPriority[iAssociatedTeam] > -1
				AND iAlwaysSpawnOnRule <= MC_serverBD_4.iCurrentHighestPriority[iAssociatedTeam]				
					PRINTLN("[RCC MISSION][HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS] ciOBJECTIVE_SPAWN_LIMIT_ON_SCORE_REACHED - Overriding Spawn Conditions for Associated Rule. Spawning now. iAlwaysSpawnOnRule = ", iAlwaysSpawnOnRule)
					bSpawn = TRUE
				ENDIF
			ENDIF
		BREAK		
	ENDSWITCH
	
	PRINTLN("[TMSTEMP][RCC MISSION][HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS] Returning ", bSpawn)
	RETURN bSpawn
	
ENDFUNC

FUNC BOOL HAS_ANY_TEAM_TRIGGERED_AGGRO()
	RETURN IS_BIT_SET(MC_serverBD.iServerBitSet1,SBBOOL1_AGGRO_TRIGGERED_FOR_TEAM_0)
			OR IS_BIT_SET(MC_serverBD.iServerBitSet1,SBBOOL1_AGGRO_TRIGGERED_FOR_TEAM_1)
			OR IS_BIT_SET(MC_serverBD.iServerBitSet1,SBBOOL1_AGGRO_TRIGGERED_FOR_TEAM_2)
			OR IS_BIT_SET(MC_serverBD.iServerBitSet1,SBBOOL1_AGGRO_TRIGGERED_FOR_TEAM_3)
ENDFUNC

FUNC BOOL IS_THIS_PICKUP_VISIBLE_TO_THIS_TEAM( INT iPickupIndex, INT iTeamIndex )
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciCONDEMNED_MODE_ENABLED)
		IF iTeamIndex != MC_serverBD_3.iCurrentCondemnedTeam
			RETURN TRUE
		ENDIF
		RETURN FALSE
	ENDIF
	PRINTLN("[JS] WEAPON TYPE ", GET_WEAPON_NAME_FROM_PT(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[ iPickupIndex ].pt), " = ", g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[ iPickupIndex ].bRest[ iTeamIndex ], " for team ", iTeamIndex)
	IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[ iPickupIndex ].bRest[ iTeamIndex ]
		RETURN FALSE
	ELSE
		RETURN TRUE
	ENDIF
ENDFUNC

FUNC BOOL USING_SHOWDOWN_POINTS()
	RETURN IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_ENABLE_SHOWDOWN_POINTS)
ENDFUNC

FUNC BOOL SHOULD_WEP_SPAWN_NOW( INT iwep )
	
	BOOL bReturn = FALSE

	IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iwep].iAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iwep].iAssociatedTeam > -1
		IF HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iwep].iAssociatedSpawn, g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iwep].iAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iwep].iAssociatedObjective)
			bReturn = TRUE
		ENDIF
	ELSE
		bReturn = TRUE
	ENDIF
	
	IF NOT bReturn // If we already know it's going to spawn, don't bother with checking again
	AND g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iwep].iSecondAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iwep].iSecondAssociatedTeam > -1
		IF HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iwep].iSecondAssociatedSpawn, g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iwep].iSecondAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iwep].iSecondAssociatedObjective)
			bReturn = TRUE
		ENDIF
	ENDIF
	
	IF NOT bReturn
	AND g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iwep].iThirdAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iwep].iThirdAssociatedTeam > -1
		IF HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iwep].iThirdAssociatedSpawn, g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iwep].iThirdAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iwep].iThirdAssociatedObjective)
			bReturn = TRUE
		ENDIF
	ENDIF
	
	IF NOT bReturn
	AND g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iwep].iFourthAssociatedObjective > -1
	AND g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iwep].iFourthAssociatedTeam > -1
		IF HAS_TEAM_REACHED_ENTITY_SPAWN_CONDITIONS(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iwep].iFourthAssociatedSpawn, g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iwep].iFourthAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iwep].iFourthAssociatedObjective)
			bReturn = TRUE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iwep].iPlacedBitset, ciFMMC_WEP_SPAWN_IF_OPPONENT_SCORES)
		IF MC_serverBD_3.iTeamToScoreLast > -1
			IF MC_serverBD_3.iTeamToScoreLast != MC_playerBD[iPartToUse].iteam
			AND IS_THIS_PICKUP_VISIBLE_TO_THIS_TEAM( iwep, MC_playerBD[iPartToUse].iteam )
				bReturn = TRUE
			ELSE
				bReturn = FALSE
			ENDIF
		ELSE
			bReturn = FALSE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iwep].iPlacedBitset, ciFMMC_WEP_ONLY_SPAWN_WHEN_LOSING)
		IF MC_serverBD.iWinningTeam != MC_playerBD[iPartToUse].iteam
		AND IS_THIS_PICKUP_VISIBLE_TO_THIS_TEAM( iwep, MC_playerBD[iPartToUse].iteam )
			bReturn = TRUE
		ELSE
			bReturn = FALSE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciCONDEMNED_MODE_ENABLED)
		bReturn = IS_THIS_PICKUP_VISIBLE_TO_THIS_TEAM( iwep, MC_playerBD[iPartToUse].iteam )
	ENDIF
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_ENTOURAGE(g_FMMC_STRUCT.iAdversaryModeType)
	AND IS_JOB_IN_CASINO()
		IF NOT IS_THIS_PICKUP_VISIBLE_TO_THIS_TEAM(iwep, MC_playerBD[iPartToUse].iteam)
			bReturn = FALSE
			PRINTLN("SHOULD_WEP_SPAWN_NOW - Setting bReturn to FALSE for pickup ", iwep, " because this pickup is restricted for team ", MC_playerBD[iPartToUse].iteam)
		ENDIF
	ENDIF
	
	//Spawngroup
	IF NOT DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP(iwep, MC_serverBD_4.rsgSpawnSeed, eSGET_Weapon)
		bReturn = FALSE
	ENDIF
	
	RETURN bReturn
	
ENDFUNC

FUNC INT GET_TEAM_TO_USE_FOR_PICKUP_CHECK(BOOL bUseCoronaTeam, INT PlayerID)
	IF bUseCoronaTeam
		RETURN GlobalplayerBD_FM[PlayerID].sClientCoronaData.iTeamChosen
	ELSE
		RETURN MC_playerBD[PlayerID].iTeam
	ENDIF
ENDFUNC

FUNC BOOL SHOULD_WEAPON_SPAWN_AT_START(INT i)
	
	IF SHOULD_ENTITY_SPAWN_AT_START(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iAssociatedTeam, g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iAssociatedObjective, g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iAssociatedSpawn, TRUE, MC_serverBD_4.rsgSpawnSeed)
		IF WILL_ENTITY_CLEANUP_IMMEDIATELY(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iCleanupTeam, g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iCleanupObjective, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iPlacedBitset, ciFMMC_WEP_CleanupAtMidpoint), MC_serverBD_4.rsgSpawnSeed)
			RETURN FALSE
		ENDIF
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

//This doesn't and won't work for weapon pickups or any pickups with the PLACEMENT_FLAG_MAP flag!
PROC ENABLE_PICKUP_FOR_TEAMS(INT iPickupIndex, PICKUP_TYPE ptPickup, BOOL bUseCoronaTeam)

	INT iThisParticipant
	PLAYER_INDEX PlayerID
	INT iPlayer
	
	INT iPlayerFlags
	
	REPEAT NUM_NETWORK_PLAYERS iThisParticipant
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iThisParticipant))
			PlayerID = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iThisParticipant))	
			IF IS_NET_PLAYER_OK(PlayerId, FALSE)
				iPlayer = NATIVE_TO_INT(PlayerID)
				INT iTeam = GET_TEAM_TO_USE_FOR_PICKUP_CHECK(bUseCoronaTeam, iPlayer)
				IF IS_THIS_PICKUP_VISIBLE_TO_THIS_TEAM(iPickupIndex, iTeam)
					SET_BIT(iPlayerFlags, iPlayer)
					PRINTLN("[JS] RESTRICTED WEAPONS - Enabling pickup ", GET_WEAPON_NAME_FROM_PT(ptPickup) ," for player: ", GET_PLAYER_NAME(PlayerId), "(",iPlayer,") on team ", iTeam )
				ELSE
					PRINTLN("[JS] RESTRICTED WEAPONS - NOT Enabling pickup ", GET_WEAPON_NAME_FROM_PT(ptPickup) ," for player: ", GET_PLAYER_NAME(PlayerId), "(",iPlayer,") on team ", iTeam )
				ENDIF
			ELSE
				PRINTLN("[JS] RESTRICTED WEAPONS - player: ", GET_PLAYER_NAME(PlayerId), "(player: ",iPlayer,") is not ok")
			ENDIF
		ENDIF
	ENDREPEAT
	
	PRINTLN("[JS] RESTRICTED WEAPONS - BITSET value: ", iPlayerFlags)
	
	BLOCK_PLAYERS_FOR_PICKUP(pipickup[iPickupIndex], iPlayerFlags)
	
ENDPROC

FUNC MODEL_NAMES GET_MODEL_FOR_RAGE_PICKUP(INT iType, INT iTeam, INT iCustomModelIndex)
	PRINTLN("[JS] GET_MODEL_FOR_RAGE_PICKUP - iType = ", iType)
	PRINTLN("[JS] GET_MODEL_FOR_RAGE_PICKUP - iTeam = ", iTeam)
	
	//using white models for spectators
	IF IS_LOCAL_PLAYER_ANY_SPECTATOR()
		iTeam = -1
	ENDIF
	
	IF iType = ciCUSTOM_PICKUP_TYPE__PLAYER_LIVES
		RETURN INT_TO_ENUM(MODEL_NAMES, HASH("stt_prop_lives_bottle"))
	ELIF iType = ciCUSTOM_PICKUP_TYPE__WEAPON_BAG
		RETURN GET_MODEL_NAME_FOR_WEAPON_BAG(iCustomModelIndex)
	ENDIF
	
	SWITCH(iTeam)
	CASE -1
		SWITCH(iType)
			CASE ciCUSTOM_PICKUP_TYPE__BEAST_MODE 
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_BMD_WH"))
			CASE ciCUSTOM_PICKUP_TYPE__BULLSHARK_TEST	
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_B_SHARK_WH"))
			CASE ciCUSTOM_PICKUP_TYPE__THERMAL_VISION
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_MK_THERMAL_WH"))
			CASE ciCUSTOM_PICKUP_TYPE__SWAP	
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_SWAP_WH"))
			CASE ciCUSTOM_PICKUP_TYPE__FILTER	
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_WEED_WH"))
			CASE ciCUSTOM_PICKUP_TYPE__SLOW_DOWN 
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_MK_S_TIME_WH"))
			CASE ciCUSTOM_PICKUP_TYPE__BULLET_TIME
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_B_TIME_WH"))
			CASE ciCUSTOM_PICKUP_TYPE__HIDE_BLIPS
				//[JS] Add blip model
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_HIDDEN_WH"))
			CASE ciCUSTOM_PICKUP_TYPE__RANDOM
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_RANDOM_WH"))
			DEFAULT
				RETURN DUMMY_MODEL_FOR_SCRIPT
		ENDSWITCH
		BREAK
	CASE 0
		SWITCH(iType)
			CASE ciCUSTOM_PICKUP_TYPE__BEAST_MODE 
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_BMD"))
			CASE ciCUSTOM_PICKUP_TYPE__BULLSHARK_TEST	
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_B_SHARK"))
			CASE ciCUSTOM_PICKUP_TYPE__THERMAL_VISION
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_MK_THERMAL"))
			CASE ciCUSTOM_PICKUP_TYPE__SWAP	
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_SWAP_P"))
			CASE ciCUSTOM_PICKUP_TYPE__FILTER	
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_WEED_P"))
			CASE ciCUSTOM_PICKUP_TYPE__SLOW_DOWN 
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_MK_S_TIME"))
			CASE ciCUSTOM_PICKUP_TYPE__BULLET_TIME
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_B_TIME"))
			CASE ciCUSTOM_PICKUP_TYPE__HIDE_BLIPS
				//[JS] Add blip model
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_HIDDEN"))
			CASE ciCUSTOM_PICKUP_TYPE__RANDOM
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_RANDOM"))
			DEFAULT
				RETURN DUMMY_MODEL_FOR_SCRIPT
		ENDSWITCH
	BREAK
	CASE 1
		SWITCH(iType)
			CASE ciCUSTOM_PICKUP_TYPE__BEAST_MODE 
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_BMD_P"))
			CASE ciCUSTOM_PICKUP_TYPE__BULLSHARK_TEST	
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_B_SHARK_P"))
			CASE ciCUSTOM_PICKUP_TYPE__THERMAL_VISION
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_MK_THERMAL"))
			CASE ciCUSTOM_PICKUP_TYPE__SWAP	
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_SWAP"))
			CASE ciCUSTOM_PICKUP_TYPE__FILTER	
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_WEED"))
			CASE ciCUSTOM_PICKUP_TYPE__SLOW_DOWN 
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_MK_S_TIME"))
			CASE ciCUSTOM_PICKUP_TYPE__BULLET_TIME
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_B_TIME_P"))
			CASE ciCUSTOM_PICKUP_TYPE__HIDE_BLIPS
				//[JS] Add blip model
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_HIDDEN_P"))
			CASE ciCUSTOM_PICKUP_TYPE__RANDOM
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_RANDOM_P"))
			DEFAULT
				RETURN DUMMY_MODEL_FOR_SCRIPT
		ENDSWITCH
	BREAK
	DEFAULT
		RETURN DUMMY_MODEL_FOR_SCRIPT
	ENDSWITCH
	
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

FUNC MODEL_NAMES GET_MODEL_FOR_VEHICLE_WEAPON(INT iType, INT iTeam, INT iWeaponIndex)
	UNUSED_PARAMETER(iWeaponIndex)
	PRINTLN("[KH] GET_MODEL_FOR_VEHICLE_WEAPON - iType: ", iType, " - iTeam: ", iTeam)
	
	//using white models for spectators
	IF IS_LOCAL_PLAYER_ANY_SPECTATOR()
	OR IS_PLAYER_IN_PROCESS_OF_JOINING_MISSION_AS_SPECTATOR( LocalPlayer )
	OR DID_PLAYER_JOIN_MISSION_AS_SPECTATOR( LocalPlayer )
		iTeam = -1
	ENDIF
		
	SWITCH(iTeam)
	CASE -1
		SWITCH(iType)
			CASE ciVEH_WEP_ROCKETS
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Rock_WH_TR"))
				ELSE
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Rock_WH"))
				ENDIF
			BREAK
			CASE ciVEH_WEP_SPEED_BOOST	
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Boost_WH"))
			CASE ciVEH_WEP_GHOST
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Ghost_WH"))
			CASE ciVEH_WEP_BEAST
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Arm_WH"))
			CASE ciVEH_WEP_PRON
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_DeadL_WH_TR"))
				ELSE
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_DeadL_WH"))
				ENDIF
			BREAK
			CASE ciVEH_WEP_FORCE_ACCELERATE	
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Acce_WH"))
			CASE ciVEH_WEP_FLIPPED_CONTROLS
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_SWAP_WH_TR"))
				ELSE
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_SWAP_WH"))
				ENDIF
			BREAK
			CASE ciVEH_WEP_ZONED
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_B_TIME_WH"))
			CASE ciVEH_WEP_DETONATE
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Deton_WH_TR"))
				ELSE
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Deton_WH"))
				ENDIF
			BREAK
			CASE ciVEH_WEP_BOMB
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Bomb_WH_TR"))
				ELSE
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Bomb_WH"))
				ENDIF
			BREAK
			CASE ciVEH_WEP_BOUNCE
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Hop_WH"))
			CASE ciVEH_WEP_REPAIR
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Repair_WH"))
			CASE ciVEH_WEP_RANDOM
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_RANDOM_WH_TR"))
				ELSE
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_RANDOM_WH"))
				ENDIF
			BREAK
			CASE ciVEH_WEP_MACHINE_GUN
				RETURN VEHICLE_GUN_PICKUP_MODEL(iTeam)
				
			CASE ciVEH_WEP_RANDOM_SPECIAL_VEH
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Special_Vehicle_WH_TR"))
				ELSE
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Special_Vehicle_WH"))
				ENDIF
			BREAK
			CASE ciVEH_WEP_RUINER_SPECIAL_VEH
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Special_Ruiner_WH_TR"))
				ELSE
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Special_Ruiner_WH"))
				ENDIF
			BREAK
			CASE ciVEH_WEP_RAMP_SPECIAL_VEH
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Special_Buggy_WH_TR"))
				ELSE
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Special_Buggy_WH"))
				ENDIF
			BREAK
			CASE ciVEH_WEP_BOMB_LENGTH
				IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWeaponIndex].iVehBombLengthIncrease < 0
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_blastdec"))
				ENDIF
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_blastinc"))
			BREAK	
			CASE ciVEH_WEP_BOMB_MAX
				IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWeaponIndex].iVehBombMaxIncrease < 0
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_bombdec"))
				ENDIF
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_bombinc"))
			BREAK
			CASE ciVEH_WEP_EXTRA_LIFE
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_lifeinc"))
			DEFAULT
				RETURN DUMMY_MODEL_FOR_SCRIPT
		ENDSWITCH
	BREAK
	CASE 0
		SWITCH(iType)
			CASE ciVEH_WEP_ROCKETS
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Rock_TR"))
				ELSE
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Rock"))
				ENDIF
				BREAK
			BREAK
			CASE ciVEH_WEP_SPEED_BOOST	
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Boost"))
			CASE ciVEH_WEP_GHOST
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Ghost"))
			CASE ciVEH_WEP_BEAST
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Arm"))
			CASE ciVEH_WEP_PRON
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_DeadL_TR"))
				ELSE
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_DeadL"))
				ENDIF
			BREAK
			CASE ciVEH_WEP_FORCE_ACCELERATE	
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Acce_P"))
			CASE ciVEH_WEP_FLIPPED_CONTROLS
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_SWAP_TR"))
				ELSE
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_SWAP_P"))			
				ENDIF
			BREAK
			CASE ciVEH_WEP_ZONED
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_B_TIME"))
			CASE ciVEH_WEP_DETONATE
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Deton"))
			CASE ciVEH_WEP_BOMB
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Bomb_TR"))
				ELSE
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Bomb"))
				ENDIF
				BREAK
			CASE ciVEH_WEP_BOUNCE
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Hop"))
			CASE ciVEH_WEP_REPAIR
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Repair"))
			CASE ciVEH_WEP_RANDOM
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_RANDOM_TR"))
				ELSE
					IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
						RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_RANDOM_TR"))
					ELSE
						RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_RANDOM"))
					ENDIF
				ENDIF
				BREAK
			CASE ciVEH_WEP_MACHINE_GUN
				RETURN VEHICLE_GUN_PICKUP_MODEL(iTeam)
				
			CASE ciVEH_WEP_RANDOM_SPECIAL_VEH
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Special_Vehicle_TR"))
				ELSE
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Special_Vehicle"))
				ENDIF
				BREAK
			BREAK
			CASE ciVEH_WEP_RUINER_SPECIAL_VEH
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Special_Ruiner_TR"))
				ELSE
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Special_Ruiner"))
				ENDIF
			BREAK
			CASE ciVEH_WEP_RAMP_SPECIAL_VEH
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Special_Buggy_TR"))
				ELSE
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Special_Buggy"))
				ENDIF
			BREAK
			CASE ciVEH_WEP_BOMB_LENGTH
				IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWeaponIndex].iVehBombLengthIncrease < 0
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_blastdec_orange"))
				ENDIF
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_blastinc_orange"))
			BREAK	
			CASE ciVEH_WEP_BOMB_MAX
				IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWeaponIndex].iVehBombMaxIncrease < 0
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_bombdec_orange"))
				ENDIF
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_bombinc_orange"))
			BREAK
			CASE ciVEH_WEP_EXTRA_LIFE
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_lifeinc_orange"))
			DEFAULT
				RETURN DUMMY_MODEL_FOR_SCRIPT
		ENDSWITCH
	BREAK
	CASE 1
		SWITCH(iType)
			CASE ciVEH_WEP_ROCKETS
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TURF_WARS(g_FMMC_STRUCT.iAdversaryModeType)
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Rock_B"))
				ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Rock_P_TR"))
				ELSE
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Rock_P"))
				ENDIF
				BREAK
			CASE ciVEH_WEP_SPEED_BOOST	
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Boost_P"))
			CASE ciVEH_WEP_GHOST
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Ghost_P"))
			CASE ciVEH_WEP_BEAST
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TURF_WARS(g_FMMC_STRUCT.iAdversaryModeType)
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Arm_B"))
				ELSE
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Arm_P"))
				ENDIF
				BREAK
			CASE ciVEH_WEP_PRON
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TURF_WARS(g_FMMC_STRUCT.iAdversaryModeType)
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_DeadL_B"))
				ELSE
					IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
						RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_DeadL_P_TR"))
					ELSE
						RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_DeadL_P"))
					ENDIF
				ENDIF
				BREAK
			CASE ciVEH_WEP_FORCE_ACCELERATE	
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TURF_WARS(g_FMMC_STRUCT.iAdversaryModeType)
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Accel_B"))
				ELSE
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Accel"))
				ENDIF
				BREAK
			CASE ciVEH_WEP_FLIPPED_CONTROLS
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_SWAP_P_TR"))
				ELSE
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_SWAP"))
				ENDIF
				BREAK
			CASE ciVEH_WEP_ZONED
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_B_TIME_P"))
			CASE ciVEH_WEP_DETONATE
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TURF_WARS(g_FMMC_STRUCT.iAdversaryModeType)
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Deton_B"))
				ELSE
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Deton_P"))
				ENDIF
				BREAK
			CASE ciVEH_WEP_BOMB
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TURF_WARS(g_FMMC_STRUCT.iAdversaryModeType)
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Bomb_B"))
				ELSE
					IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
						RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Bomb_P_TR"))
					ELSE
						RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Bomb_P"))
					ENDIF
				ENDIF
				BREAK
			CASE ciVEH_WEP_BOUNCE
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Hop_P"))
			CASE ciVEH_WEP_REPAIR
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Repair_P"))
			CASE ciVEH_WEP_RANDOM
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_RANDOM_P_TR"))
				ELSE
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_RANDOM_P"))
				ENDIF
				BREAK
			CASE ciVEH_WEP_MACHINE_GUN
				RETURN VEHICLE_GUN_PICKUP_MODEL(iTeam)
				
			CASE ciVEH_WEP_RANDOM_SPECIAL_VEH
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_special_vehicle__P_TR"))
				ELSE
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Special_Vehicle_P"))
				ENDIF
			BREAK
			CASE ciVEH_WEP_RUINER_SPECIAL_VEH
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Special_Ruiner_P_TR"))
				ELSE
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Special_Ruiner_P"))
				ENDIF
			BREAK
			CASE ciVEH_WEP_RAMP_SPECIAL_VEH
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Special_Buggy_P_TR"))
				ELSE
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Special_Buggy_P"))
				ENDIF
			BREAK
			CASE ciVEH_WEP_BOMB_LENGTH
				IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWeaponIndex].iVehBombLengthIncrease < 0
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_blastdec_purple"))
				ENDIF
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_blastinc_purple"))
			BREAK	
			CASE ciVEH_WEP_BOMB_MAX
				IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWeaponIndex].iVehBombMaxIncrease < 0
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_bombdec_purple"))
				ENDIF
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_bombinc_purple"))
			BREAK
			CASE ciVEH_WEP_EXTRA_LIFE
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_lifeinc_purple"))
			DEFAULT
				RETURN DUMMY_MODEL_FOR_SCRIPT
		ENDSWITCH
	BREAK
	CASE 2
		SWITCH(iType)
			CASE ciVEH_WEP_ROCKETS
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_ICRocket_PK_TR"))
				ELSE
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Rock_PK"))
				ENDIF
				BREAK
			CASE ciVEH_WEP_SPEED_BOOST	
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Boost_PK"))
			CASE ciVEH_WEP_GHOST
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Ghost_PK"))
			CASE ciVEH_WEP_BEAST
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Arm_PK"))
			CASE ciVEH_WEP_PRON
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_DeadL_PK_TR"))
				ELSE
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_DeadL_PK"))
				ENDIF
				BREAK
			CASE ciVEH_WEP_FORCE_ACCELERATE	
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Accel_PK"))
			CASE ciVEH_WEP_FLIPPED_CONTROLS
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_SWAP_PK_TR"))
				ELSE
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_SWAP_PK"))
				ENDIF
				BREAK
			CASE ciVEH_WEP_ZONED
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_B_TIME_PK"))
			CASE ciVEH_WEP_DETONATE
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Deton_PK"))
			CASE ciVEH_WEP_BOMB
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Bomb_PK_TR"))
				ELSE
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Bomb_PK"))
				ENDIF
				BREAK
			CASE ciVEH_WEP_BOUNCE
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Hop_PK"))
			CASE ciVEH_WEP_REPAIR
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Repair_PK"))
			CASE ciVEH_WEP_RANDOM
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_RANDOM_PK_TR"))
				ELSE
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_RANDOM_PK"))
				ENDIF
				BREAK
			CASE ciVEH_WEP_MACHINE_GUN
				RETURN VEHICLE_GUN_PICKUP_MODEL(iTeam)
			
			CASE ciVEH_WEP_RANDOM_SPECIAL_VEH
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Special_Vehicle_PK_TR"))
				ELSE
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Special_Vehicle_PK"))
				ENDIF
			BREAK
			CASE ciVEH_WEP_RUINER_SPECIAL_VEH
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Special_Ruiner_PK_TR"))
				ELSE
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Special_Ruiner_PK"))
				ENDIF
			BREAK
			CASE ciVEH_WEP_RAMP_SPECIAL_VEH
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Special_Buggy_PK_TR"))
				ELSE
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Special_Buggy_PK"))
				ENDIF
			BREAK
			CASE ciVEH_WEP_BOMB_LENGTH
				IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWeaponIndex].iVehBombLengthIncrease < 0
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_blastdec_pink"))
				ENDIF
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_blastinc_pink"))
			BREAK	
			CASE ciVEH_WEP_BOMB_MAX
				IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWeaponIndex].iVehBombMaxIncrease < 0
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_bombdec_pink"))
				ENDIF
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_bombinc_pink"))
			BREAK
			CASE ciVEH_WEP_EXTRA_LIFE
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_lifeinc_pink"))
			DEFAULT
				RETURN DUMMY_MODEL_FOR_SCRIPT
		ENDSWITCH
	BREAK
	CASE 3
		SWITCH(iType)
			CASE ciVEH_WEP_ROCKETS
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Rock_G_TR"))
				ELSE
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Rock_G"))
				ENDIF
				BREAK
			CASE ciVEH_WEP_SPEED_BOOST	
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Boost_G"))
			CASE ciVEH_WEP_GHOST
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Ghost_G"))
			CASE ciVEH_WEP_BEAST
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Arm_G"))
			CASE ciVEH_WEP_PRON
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_DeadL_G_TR"))
				ELSE
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_DeadL_G"))
				ENDIF
			BREAK
			CASE ciVEH_WEP_FORCE_ACCELERATE	
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Accel_G"))
			CASE ciVEH_WEP_FLIPPED_CONTROLS
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_SWAP_G_TR"))
				ELSE
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_SWAP_G"))
				ENDIF
			BREAK
			CASE ciVEH_WEP_ZONED
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_B_TIME_G"))
			CASE ciVEH_WEP_DETONATE
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Deton_G"))
			CASE ciVEH_WEP_BOMB
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Bomb_G_TR"))
				ELSE
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Bomb_G"))
				ENDIF
			BREAK
			CASE ciVEH_WEP_BOUNCE
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Hop_G"))
			CASE ciVEH_WEP_REPAIR
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Repair_G"))
			CASE ciVEH_WEP_RANDOM
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_RANDOM_G_TR"))
				ELSE
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("PROP_EX_RANDOM_G"))
				ENDIF
			BREAK
			CASE ciVEH_WEP_MACHINE_GUN
				RETURN VEHICLE_GUN_PICKUP_MODEL(iTeam)
		
			CASE ciVEH_WEP_RANDOM_SPECIAL_VEH
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Special_Vehicle_G_TR"))
				ELSE
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Special_Vehicle_G"))
				ENDIF
			BREAK
			CASE ciVEH_WEP_RUINER_SPECIAL_VEH
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Special_Ruiner_G_TR"))
				ELSE
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Special_Ruiner_G"))
				ENDIF
			BREAK
			CASE ciVEH_WEP_RAMP_SPECIAL_VEH
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)				
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Special_Buggy_G_TR"))
				ELSE
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("Prop_HX_Special_Buggy_G"))
				ENDIF
			BREAK
			CASE ciVEH_WEP_BOMB_LENGTH
				IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWeaponIndex].iVehBombLengthIncrease < 0
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_blastdec_green"))
				ENDIF
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_blastinc_green"))
			BREAK	
			CASE ciVEH_WEP_BOMB_MAX
				IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iWeaponIndex].iVehBombMaxIncrease < 0
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_bombdec_green"))
				ENDIF
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_bombinc_green"))
			BREAK
			CASE ciVEH_WEP_EXTRA_LIFE
				RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_battle_ic_lifeinc_green"))
			DEFAULT
				RETURN DUMMY_MODEL_FOR_SCRIPT
		ENDSWITCH
	BREAK
	DEFAULT
		RETURN DUMMY_MODEL_FOR_SCRIPT
	ENDSWITCH


	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

FUNC INT GET_NUMBER_OF_PLAYING_PLAYERS_ON_TEAMS(INT iTeamBS)
	INT i, iNumberOfPlayers
	FOR i = 0 TO FMMC_MAX_TEAMS - 1
		IF IS_BIT_SET(iTeamBS, i)
			iNumberOfPlayers += MC_serverBD.iNumberOfPlayingPlayers[i]
		ENDIF
	ENDFOR
	RETURN iNumberOfPlayers
ENDFUNC

FUNC INT GET_NUMBER_OF_PLAYING_PLAYERS()
	INT i, iNumberOfPlayers
	FOR i = 0 TO FMMC_MAX_TEAMS - 1
		iNumberOfPlayers = iNumberOfPlayers + MC_serverBD.iNumberOfPlayingPlayers[i]
	ENDFOR
	
	RETURN iNumberOfPlayers
ENDFUNC

FUNC VECTOR GET_ROTATION_FOR_MISSION_PICKUP(INT iPickup)
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TINY_RACERS(g_FMMC_STRUCT.iAdversaryModeType)
		RETURN <<270.0,360.0,0>>
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickup].iPlacedBitset, ciFMMC_WEP_Rotation_Override)
		RETURN g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickup].vRot
	ENDIF
	
	RETURN <<0,360,0>>
ENDFUNC

PROC SET_PLACEMENT_FLAGS_FOR_MISSION_PICKUPS(INT iPickup, INT &iPlacementFlags)
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickup].iPlacedBitset, ciFMMC_WEP_Rotation_Override)
		CLEAR_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ROTATE))
		PRINTLN("[WEAPON PICKUPS] SET_PLACEMENT_FLAGS_FOR_MISSION_PICKUPS - Rotation Overridden - Rotation disabled - ", iPickup)
	ELSE
		SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ROTATE))
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciENABLE_STATIC_PICKUPS)
		CLEAR_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ROTATE))
		PRINTLN("[WEAPON PICKUPS] SET_PLACEMENT_FLAGS_FOR_MISSION_PICKUPS - ciENABLE_STATIC_PICKUPS - Rotation disabled - ", iPickup)
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickup].iPlacedBitset, ciFMMC_WEP_Can_Pickup_in_vehicle)
		SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_CREATION_FLAG_COLLECTABLE_IN_VEHICLE))
		PRINTLN("[WEAPON PICKUPS] SET_PLACEMENT_FLAGS_FOR_MISSION_PICKUPS setting PLACEMENT_CREATION_FLAG_COLLECTABLE_IN_VEHICLE for weapon ", iPickup)
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickup].iPlacedBitset, ciFMMC_WEP_Grounded)
		CLEAR_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ROTATE))
		SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ORIENT_TO_GROUND))
		SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_SNAP_TO_GROUND))
		SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_UPRIGHT))
		PRINTLN("[WEAPON PICKUPS] SET_PLACEMENT_FLAGS_FOR_MISSION_PICKUPS - Stopping rotation. Snapping and Orienting pickup to ground. - ", iPickup)
	ENDIF
	PRINTLN("[WEAPON PICKUPS] SET_PLACEMENT_FLAGS_FOR_MISSION_PICKUPS - iPlacementFlags: ", iPlacementFlags)
ENDPROC

FUNC BOOL IS_CONTINUITY_BLOCKING_PICKUP_SPAWN(INT iPickup)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_WeaponPickupTracking)
		RETURN FALSE
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickup].iContinuityId = -1
		RETURN FALSE
	ENDIF
	
	IF NOT IS_LONG_BIT_SET(sLEGACYMissionLocalContinuityVars.iPickupCollectedTrackingBitset, g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickup].iContinuityId)
		RETURN FALSE
	ENDIF
	
	PRINTLN("[JS][CONTINUITY] - IS_CONTINUITY_BLOCKING_PICKUP_SPAWN - Pickup ", iPickup, " blocked")
	RETURN TRUE
	
ENDFUNC

///PURPOSE: This function creates all pickups in a mission and returns true once that's one
///    Attachments are done in PROCESS_PICKUP_BLIPS_AND_ATTACHMENTS
FUNC BOOL HAS_PLAYER_CREATED_MISSION_PICKUPS(INT iTeam = -1) 

	INT iPlacementFlags = 0
	
	MODEL_NAMES mn
	
	WEAPON_TYPE wt
	PICKUP_TYPE tempPickup
	SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_REGENERATES))
	SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_MAP)) 
	
	SET_FM_PICKUP_ORIENTATION_FLAGS(iPlacementFlags)
	
	INT i
	VECTOR vPos

	IF g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons = 0
		RETURN TRUE
	ENDIF
		
	IF IS_LOCAL_PLAYER_ANY_SPECTATOR()
		IF MC_playerBD[iPartToUse].iTeam < FMMC_MAX_TEAMS
			iTeam = MC_playerBD[iPartToUse].iTeam
		ENDIF
	ENDIF
	
	IF iTeam = -1
		iTeam = GET_PRE_ASSIGNMENT_PLAYER_TEAM()
	ENDIF
	
	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons i
		IF i < FMMC_MAX_WEAPONS
			
			//Catch to detect blocked pickups in UGC content
			IF g_sMPTunables.bENABLE_CREATOR_BLOCK
				IF NOT IS_CURRENT_MISSION_ROCKSTAR_CREATED()
					wt = GET_WEAPON_TYPE_FROM_PICKUP_TYPE(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].pt)
					IF IS_WEAPON_SELECTION_DEV_ONLY(wt)
					AND NOT IS_ROCKSTAR_DEV()
						ASSERTLN("[CREATOR_BLOCK] INVALID WEAPON DETECTED SWITCHING TO PISTOL")
						PRINTLN("[CREATOR_BLOCK] INVALID WEAPON DETECTED SWITCHING TO PISTOL")
						g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].pt = PICKUP_VEHICLE_WEAPON_PISTOL
					ENDIF
				ENDIF
			ENDIF
			
			PRINTLN("[RCC MISSION] HAS_PLAYER_CREATED_MISSION_PICKUPS - iWeap: ", i, " iPickupSpawnTotalNumberOfPlayers: ", g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iPickupSpawnTotalNumberOfPlayers, " GET_NUMBER_OF_PLAYING_PLAYERS: ", GET_NUMBER_OF_PLAYING_PLAYERS())
			
			IF IS_CONTINUITY_BLOCKING_PICKUP_SPAWN(i)
				RELOOP
			ENDIF
			
			IF (GET_NUMBER_OF_PLAYING_PLAYERS() >= g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iPickupSpawnTotalNumberOfPlayers
			OR g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iPickupSpawnTotalNumberOfPlayers = 0
			OR IS_FAKE_MULTIPLAYER_MODE_SET())
				IF NOT DOES_PICKUP_EXIST(pipickup[i])
					vPos = g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].vPos
					IF NOT IS_VECTOR_ZERO(vPos)
					AND g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].pt <> NUM_PICKUPS
						
						IF (Is_Player_Currently_On_MP_LTS_Mission(LocalPlayer)
						OR Is_Player_Currently_On_MP_CTF_Mission(LocalPlayer))
						AND NOT IS_FAKE_MULTIPLAYER_MODE_SET()
							g_iHighestRankPlayer = MC_serverBD_1.iCoronaHighestRankPlayer
							PRINTLN("[RCC MISSION] HAS_PLAYER_CREATED_MISSION_PICKUPS - g_iHighestRankPlayer = MC_serverBD_1.iCoronaHighestRankPlayer ... ", g_iHighestRankPlayer, " = ", MC_serverBD_1.iCoronaHighestRankPlayer)
							
							tempPickup = GET_WEAPON_PICKUP_TYPE(i)
						ELSE
							tempPickup = g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].pt
						ENDIF
						IF IS_PICKUP_WEAPON_MK2(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].pt)
							SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_CREATION_FLAG_FORCE_DEFERRED_MODEL))
						ELSE
							CLEAR_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_CREATION_FLAG_FORCE_DEFERRED_MODEL))
						ENDIF
					
						IF Is_Player_Currently_On_MP_LTS_Mission(LocalPlayer) 
							IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSet, ciDMWeaponsLocked) 
							AND IS_PICKUP_HEALTH(tempPickup) = FALSE
							AND IS_PICKUP_ARMOUR(tempPickup) = FALSE
							AND IS_PICKUP_PARACHUTE(tempPickup) = FALSE
								tempPickup = GET_PICKUP_TYPE_FROM_WEAPON_TYPE(GET_DEATHMATCH_CREATOR_RESPAWN_WEAPON(g_FMMC_STRUCT_ENTITIES.iWeaponPallet))
							ENDIF
						ENDIF
						PRINTLN("[RCC MISSION] HAS_PLAYER_CREATED_MISSION_PICKUPS Making pickup: ",i," at the coordinates: ",vPos)
					
						INT iammo
						IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iClips != 0
							IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciMINIGUN_AMMO_OVERRIDE)
							AND tempPickup = PICKUP_WEAPON_MINIGUN
								iammo = g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iClips * 100
								PRINTLN("[RCC MISSION] HAS_PLAYER_CREATED_MISSION_PICKUPS ammo override - Minigun special override. Ammo = ",iammo)
							ELSE
								iammo = GET_AMMO_IN_WEAPON_NUM_CLIPS(GET_WEAPON_TYPE_FROM_PICKUP_TYPE(tempPickup), g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iClips)
								PRINTLN("[RCC MISSION] HAS_PLAYER_CREATED_MISSION_PICKUPS ammo override: ",iammo)
							ENDIF
						ELSE
							iammo = GET_AMMO_AMOUNT_FOR_MP_PICKUP(GET_WEAPON_TYPE_FROM_PICKUP_TYPE(tempPickup))
							PRINTLN("[RCC MISSION] HAS_PLAYER_CREATED_MISSION_PICKUPS standard ammo: ",iammo)
						ENDIF
						
						INT iTempRagePickupType = g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iCustomPickupType
						IF iTempRagePickupType >= ciCUSTOM_PICKUP_TYPE__MISSION_EQUIPMENT
							iTempRagePickupType = -1
						ENDIF
						INT iTempVehicleWeaponPickupType = g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iVehicleWeaponPickupType
					
						//Make Health/Armour Full
						IF tempPickup = PICKUP_ARMOUR_STANDARD
						OR tempPickup = PICKUP_HEALTH_STANDARD
						OR tempPickup = PICKUP_CUSTOM_SCRIPT
							iammo = 500
							PRINTLN("[RCC MISSION] HAS_PLAYER_CREATED_MISSION_PICKUPS Health/Armour ammo: ",iammo)
						ENDIF
						
						PRINTLN("[KH][WEAPON PICKUPS] HAS_PLAYER_CREATED_MISSION_PICKUPS - [ ",i," ] Pickup ammo: ", iammo)
					
						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iPlacedBitset, ciPLACED_WEAPON_LOCAL_ONLY)
							PRINTLN("[RCC MISSION] HAS_PLAYER_CREATED_MISSION_PICKUPS ciPLACED_WEAPON_LOCAL_ONLY set for pickup ", i)
							SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_LOCAL_ONLY))
						ELSE
							CLEAR_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_LOCAL_ONLY))
						ENDIF
					
						IF AM_I_ON_A_HEIST()
						AND g_FMMC_STRUCT.iRootContentIDHash = g_sMPTUNABLES.iroot_id_HASH_Prison_Break_Station
							IF tempPickup = PICKUP_WEAPON_PETROLCAN
								PRINTLN("[RCC MISSION] HAS_PLAYER_CREATED_MISSION_PICKUPS - Pickup type is Petrol can - CLEAR_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_LOCAL_ONLY)) for pickup ", i)
								CLEAR_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_LOCAL_ONLY))
							ENDIF
						ENDIF
						
						PRINTLN("[KH][WEAPON PICKUPS] HAS_PLAYER_CREATED_MISSION_PICKUPS - Weapon Name: ", GET_WEAPON_NAME(FMMC_GET_WEAPON_TYPE_FROM_PICKUP_TYPE(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].pt, TRUE)))
						
						//set the type to the random type if the option has been switched on
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciRANDOM_PICKUP_TYPES_OPTION)
						AND g_FMMC_STRUCT_ENTITIES.bRandomPickupTypes = TRUE
							PRINTLN("[KH] g_FMMC_STRUCT_ENTITIES.bRandomPickupTypes is TRUE - Setting to random pickup types")
							IF iTempRagePickupType > -1
								iTempRagePickupType = 7
							ENDIF
						ENDIF
						
						BOOL bHasLoadedModel = TRUE
						IF iTempRagePickupType != -1
							mn = GET_MODEL_FOR_RAGE_PICKUP(iTempRagePickupType, iTeam, g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iCustomPickupModel)
							IF IS_MODEL_VALID(mn)
								REQUEST_MODEL(mn)
							ENDIF
							PRINTLN("[WEAPON PICKUPS] HAS_PLAYER_CREATED_MISSION_PICKUPS - Checking model ", GET_MODEL_NAME_FOR_DEBUG(mn), " has loaded. Rage pickup type: ", iTempRagePickupType)
							IF NOT HAS_MODEL_LOADED(mn)
								bHasLoadedModel = FALSE
							ENDIF
						ENDIF

						VECTOR vRotation = GET_ROTATION_FOR_MISSION_PICKUP(i)
						
						SET_PLACEMENT_FLAGS_FOR_MISSION_PICKUPS(i, iPlacementFlags)
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwelve, ciENABLE_STATIC_PICKUPS)
							tempPickup = PICKUP_VEHICLE_CUSTOM_SCRIPT_NO_ROTATE
						ENDIF
						
						IF bHasLoadedModel
							IF iTempRagePickupType = -1
							AND iTempVehicleWeaponPickupType = -1
							AND g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].pt != PICKUP_CUSTOM_SCRIPT
								VECTOR vOffset = <<0,0,0.10>>
								
								IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iPlacedBitset, ciFMMC_WEP_Position_Override)
								OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iPlacedBitset, ciFMMC_WEP_Rotation_Override)
									vOffset = <<0,0,-0.025>>
								ENDIF
								
								pipickup[i] = CREATE_PICKUP_ROTATE(tempPickup, vPos + vOffset, vRotation, iPlacementFlags, iammo, EULER_YXZ, FALSE)
								PRINTLN("[KH][WEAPON PICKUPS] HAS_PLAYER_CREATED_MISSION_PICKUPS - [ ",i," ] Pickup created - Weapon/Armour/Health pickup type: ", ENUM_TO_INT(tempPickup))
							ELSE
								IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iCustomPickupType != ciCUSTOM_PICKUP_TYPE__NONE
									PRINTLN("[KH][WEAPON PICKUPS] HAS_PLAYER_CREATED_MISSION_PICKUPS - [ ",i," ] Pickup created - Rage pickup type: ", g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iCustomPickupType, " iPlacementFlags: ", iPlacementFlags)
									IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iCustomPickupType = ciCUSTOM_PICKUP_TYPE__PLAYER_LIVES
										vPos.z -= 0.4
									ENDIF
									
									pipickup[i] = CREATE_PICKUP_ROTATE(tempPickup, vPos + <<0,0,0.40>>, vRotation, iPlacementFlags, iammo,EULER_YXZ,FALSE, GET_MODEL_FOR_RAGE_PICKUP(iTempRagePickupType, iTeam, g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iCustomPickupModel))
								ELIF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iVehicleWeaponPickupType != -1
									IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_PICKUPS_CORONA_OPTION) AND g_FMMC_STRUCT.bAllowPickUpsCorona
									OR NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_PICKUPS_CORONA_OPTION)
										PRINTLN("[KH][WEAPON PICKUPS] HAS_PLAYER_CREATED_MISSION_PICKUPS - [ ",i," ] Pickup created - Vehicle Weapon type: ", g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iVehicleWeaponPickupType)
										
										MODEL_NAMES mnPickupModel = GET_MODEL_FOR_VEHICLE_WEAPON(iTempVehicleWeaponPickupType, iTeam, i)
										FLOAT fZOffset = 0.40
										
										IF IS_DROP_THE_BOMB_PICKUP(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iVehicleWeaponPickupType)
											fZOffset = 3.0
										ENDIF
										
										pipickup[i] = CREATE_PICKUP_ROTATE(tempPickup, vPos + <<0.0, 0.0, fZOffset>>, vRotation, iPlacementFlags, iAmmo, EULER_YXZ, FALSE, mnPickupModel)
										
										IF GET_MODEL_FOR_VEHICLE_WEAPON(iTempVehicleWeaponPickupType, iTeam, i) = INT_TO_ENUM(MODEL_NAMES, HASH("Prop_IC_Deton"))
											PRINTLN("[WEAPON PICKUPS] Detonator pickup index: ", NATIVE_TO_INT(pipickup[i]))
										ENDIF
									ENDIF
								ELSE
									PRINTLN("[KH][WEAPON PICKUPS] HAS_PLAYER_CREATED_MISSION_PICKUPS - [ ",i," ] Pickup created - Weapon/Armour/Health pickup type: ", ENUM_TO_INT(tempPickup))
									pipickup[i] = CREATE_PICKUP_ROTATE(tempPickup, vPos + <<0,0,0.10>>, vRotation, iPlacementFlags, iammo,EULER_YXZ,FALSE, GET_MODEL_FOR_RAGE_PICKUP(iTempRagePickupType, iTeam, g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iCustomPickupModel))
								ENDIF
							ENDIF
							
							//Pickup Setup
							IF DOES_PICKUP_EXIST(pipickup[i])
								SET_PICKUP_GLOW_OFFSET(pipickup[i], 0.51)
								SET_PICKUP_REGENERATION_TIME(pipickup[i], GET_MC_SPAWN_TIME_FROM_INT(g_FMMC_STRUCT_ENTITIES.iWeaponRespawnTime))
							ENDIF
							
							IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TURF_WARZONE(g_FMMC_STRUCT.iAdversaryModeType)
								IF bIsLocalPlayerHost
									IF i < ciMAX_PTFX_FOR_WEAPONS
										USE_PARTICLE_FX_ASSET("scr_sr_adversary")						
										ptfxWeaponGlow[i] = START_PARTICLE_FX_LOOPED_AT_COORD("scr_sr_lg_weapon_highlight", g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].vPos, <<0.0, 0.0, 0.0>>, 1.0)
										PRINTLN("[KH][WEAPON PICKUPS] - Creating Glow")
									ENDIF
								ENDIF
							ENDIF
							
							IF (g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].fDamageMultiplier != 1.0)
								SET_WEAPON_DAMAGE_MODIFIER(GET_WEAPON_TYPE_FROM_PICKUP_TYPE(tempPickup), g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].fDamageMultiplier)
								PRINTLN("[JS] increasing damage modifier of ", GET_WEAPON_NAME_FROM_PT(tempPickup), " to ", g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].fDamageMultiplier)
							ENDIF
							
							g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].id = pipickup[i]
							IF NOT IS_BIT_SET(iSpawnedWeapons[GET_LONG_BITSET_INDEX(i)],GET_LONG_BITSET_BIT(i))
								SET_BIT(iSpawnedWeapons[GET_LONG_BITSET_INDEX(i)],GET_LONG_BITSET_BIT(i))
							ENDIF
							
							//Stops the pickup from being collectable
							IF NOT SHOULD_WEP_SPAWN_NOW(i)
							OR NOT IS_THIS_PICKUP_VISIBLE_TO_THIS_TEAM( i, iTeam )
								PRINTLN("[KH][WEAPON PICKUPS] HAS_PLAYER_CREATED_MISSION_PICKUPS - [ ",i," ] Making invisible and uncollectable")
								SET_PICKUP_UNCOLLECTABLE(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].id, TRUE)
								SET_PICKUP_HIDDEN_WHEN_UNCOLLECTABLE(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].id, TRUE)
							ELSE
								PRINTLN("[KH][WEAPON PICKUPS] HAS_PLAYER_CREATED_MISSION_PICKUPS - [ ",i," ] Making visible and collectable")
								IF NOT IS_BIT_SET(iActiveWeapons[GET_LONG_BITSET_INDEX(i)],GET_LONG_BITSET_BIT(i))
									SET_BIT(iActiveWeapons[GET_LONG_BITSET_INDEX(i)],GET_LONG_BITSET_BIT(i))
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_PICKUPS_CORONA_OPTION) AND g_FMMC_STRUCT.bAllowPickUpsCorona
	OR NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_PICKUPS_CORONA_OPTION)
		REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons i
			IF i < FMMC_MAX_WEAPONS
				IF IS_CONTINUITY_BLOCKING_PICKUP_SPAWN(i)
					RELOOP
				ENDIF
				
				IF NOT DOES_PICKUP_EXIST(pipickup[i])
					IF NOT IS_VECTOR_ZERO(vPos)
					AND g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].pt <> NUM_PICKUPS
					AND IS_THIS_PICKUP_VISIBLE_TO_THIS_TEAM( i, iTeam )
					AND (g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iPickupSpawnTotalNumberOfPlayers >= GET_NUMBER_OF_PLAYING_PLAYERS() OR g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iPickupSpawnTotalNumberOfPlayers = 0 OR IS_FAKE_MULTIPLAYER_MODE_SET())
						PRINTLN("[KH][WEAPON PICKUPS] HAS_PLAYER_CREATED_MISSION_PICKUPS - [ ",i," ] - WEAPON NOT CREATED!!")
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
		
	RETURN TRUE
ENDFUNC

FUNC INT GET_ACTIVE_PARTICIPANT_INDEX_FROM_NUMBER(INT iParticipantNumber)
	INT i
	INT iReturn = 0
	PRINTLN("[PLAYER_LOOP] - GET_ACTIVE_PARTICIPANT_INDEX_FROM_NUMBER")
	FOR i = 0 TO MAX_NUM_MC_PLAYERS -1
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
		AND NOT IS_PLAYER_SPECTATOR_ONLY(INT_TO_PLAYERINDEX(i))
		AND IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(i))
			IF i = iParticipantNumber
				PRINTLN("[RCC MISSION] GET_ACTIVE_PARTICIPANT_INDEX_FROM_NUMBER - Ordered Participant Number (1st 2nd etc): ", iParticipantNumber, " Participant Index: ", i)
				RETURN iReturn
			ENDIF
			iReturn++
		ENDIF
	ENDFOR
	
	RETURN -1
			
ENDFUNC

FUNC INT GET_ORDERED_PARTICIPANT_NUMBER_IN_TEAM(INT iParticipantNumber)
	INT i
	INT iReturn = -1
	FOR i = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		IF MC_playerBD[i].iTeam = MC_playerBD[iParticipantNumber].iTeam
			IF ((NOT IS_BIT_SET(MC_playerBD[i].iClientBitSet, PBBOOL_ANY_SPECTATOR)) OR IS_BIT_SET(MC_playerBD[i].iClientBitSet, PBBOOL_HEIST_SPECTATOR))
			AND NOT IS_BIT_SET(MC_playerBD[i].iClientBitSet,PBBOOL_PLAYER_FAIL)
			AND IS_BIT_SET(MC_playerBD[i].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
			AND NOT IS_BIT_SET(MC_playerBD[i].iClientBitSet, PBBOOL_FINISHED)
				PARTICIPANT_INDEX tempPart = INT_TO_PARTICIPANTINDEX(i)
				IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
					iReturn++
					IF i = iParticipantNumber
						PRINTLN("[RCC MISSION] GET_ORDERED_PARTICIPANT_NUMBER_IN_TEAM - Participant ", iParticipantNumber, " Ordered Number: ", iReturn)
						RETURN iReturn
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN -1
			
ENDFUNC

FUNC INT GET_ORDERED_PARTICIPANT_NUMBER(INT iParticipantNumber)
	INT i
	INT iReturn = -1
	FOR i = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
		AND NOT IS_PLAYER_SPECTATOR_ONLY(INT_TO_PLAYERINDEX(i))
			iReturn++
			IF i = iParticipantNumber
				PRINTLN("[RCC MISSION] GET_ORDERED_PARTICIPANT_NUMBER - Participant ", iParticipantNumber, " Ordered Number: ", iReturn)
				RETURN iReturn
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN -1
			
ENDFUNC

FUNC INT GET_PARTICIPANT_NUMBER_IN_TEAM() 

INT iplayer,iplayerGBD
INT iMyteam
PLAYER_INDEX tempPlayer
INT ireturn

	IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
		iMyteam = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen  
		PRINTLN("[RCC MISSION] GET_PARTICIPANT_NUMBER_IN_TEAM - local player team = ",iMyteam)
	ELSE
		RETURN 0
	ENDIF

	iteamstartPlayerJoinBitset = Get_Bitfield_Of_Players_On_Or_Joining_Mission(Get_UniqueID_For_This_Players_Mission(PLAYER_ID()))
	PRINTLN("[RCC MISSION] GET_PARTICIPANT_NUMBER_IN_TEAM - iteamstartPlayerJoinBitset = ", iteamstartPlayerJoinBitset)

	FOR iplayer = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		PRINTLN("[RCC MISSION] GET_PARTICIPANT_NUMBER_IN_TEAM - iplayer = ",iplayer)
		tempPlayer = INT_TO_PLAYERINDEX(iplayer)
		
		IF IS_NET_PLAYER_OK(tempPlayer,FALSE)
			PRINTLN("[RCC MISSION] GET_PARTICIPANT_NUMBER_IN_TEAM - player ok = ",iplayer)
	       	IF tempPlayer!= PLAYER_ID()
				PRINTLN("[RCC MISSION] GET_PARTICIPANT_NUMBER_IN_TEAM - not local player ",iplayer)
				IF NOT IS_PLAYER_SCTV(tempPlayer)
					iplayerGBD = NATIVE_TO_INT(tempPlayer)
					PRINTLN("[RCC MISSION] GET_PARTICIPANT_NUMBER_IN_TEAM - native to int on player id: ",iplayerGBD)
					IF (IS_BIT_SET(iteamstartPlayerJoinBitset, iplayerGBD) OR DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(tempPlayer))
					AND (NOT DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(tempPlayer) OR NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_RUNNING_BACK_REMIX(g_FMMC_STRUCT.iAdversaryModeType))
					AND NOT IS_THIS_PLAYER_NOT_GOING_TO_TAKE_PART_IN_THIS_CORONA(tempPlayer)
						PRINTLN("[RCC MISSION] GET_PARTICIPANT_NUMBER_IN_TEAM - past iteamstartPlayerJoinBitset: ",iplayerGBD)						
						PRINTLN("[RCC MISSION] GET_PARTICIPANT_NUMBER_IN_TEAM - other player team = ",GlobalplayerBD_FM[iplayerGBD].sClientCoronaData.iTeamChosen)
						IF GlobalplayerBD_FM[iplayerGBD].sClientCoronaData.iTeamChosen = iMyteam
						AND (iMyTeam > -1 AND iMyTeam != 8)
							ireturn++
							PRINTLN("[RCC MISSION] GET_PARTICIPANT_NUMBER_IN_TEAM - past team check for player: ",iplayerGBD," return value is now: ",ireturn)
						ENDIF					
					ELSE
						PRINTLN("[RCC MISSION] GET_PARTICIPANT_NUMBER_IN_TEAM - ", iplayerGBD, " was not set in bitset iteamstartPlayerJoinBitset, which is currently: ", iteamstartPlayerJoinBitset," JIP: ", DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(tempPlayer))
					ENDIF
				ENDIF
			ELSE
				PRINTLN("[RCC MISSION] GET_PARTICIPANT_NUMBER_IN_TEAM - Player is local player ",iplayer)
				iplayer = NUM_NETWORK_PLAYERS
			ENDIF
		ENDIF
	ENDFOR

	PRINTLN("[RCC MISSION] GET_PARTICIPANT_NUMBER_IN_TEAM return value ", ireturn)
	RETURN ireturn
	
ENDFUNC

FUNC VECTOR GET_CUSTOM_RESPAWN_POSITION(FLOAT &fNewHead, INT iTeamSpawnPointCounter, INT iTeam, INT iRespawnPoint, BOOL bPrintOut = FALSE)

	VECTOR vCoord				= g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iTeamSpawnPointCounter].vPos
	FLOAT fHead					= g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iTeamSpawnPointCounter].fHead
		
	IF iRespawnPoint > -1
		INT iAreaType				= g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iTeamSpawnPointCounter].iAreaType
		INT iAreaScale				= g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iTeamSpawnPointCounter].iAreaScale
		INT iSpawnPointQuantity 	= g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iTeamSpawnPointCounter].iSpawnPointQuantity
		INT iGetGroundFromHeight 	= g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iTeamSpawnPointCounter].iGetGroundFromHeight
		// Stagger the print so we don't spam
		IF GET_FRAME_COUNT() % 60 = 0
		OR bPrintOut
			PRINTLN("[LM][MAINTAIN_CUSTOM_RESPAWN_DECALS] - Maintaining the Decals for Custom Spawn Points, Point: ", iTeamSpawnPointCounter)
			PRINTLN("[LM][MAINTAIN_CUSTOM_RESPAWN_DECALS] - vCoord:            		", vCoord)
			PRINTLN("[LM][MAINTAIN_CUSTOM_RESPAWN_DECALS] - fHead:             		", fHead)
			PRINTLN("[LM][MAINTAIN_CUSTOM_RESPAWN_DECALS] - iAreaType:              ", iAreaType)
			PRINTLN("[LM][MAINTAIN_CUSTOM_RESPAWN_DECALS] - iAreaScale: 			", iAreaScale)
			PRINTLN("[LM][MAINTAIN_CUSTOM_RESPAWN_DECALS] - iSpawnPointQuantity:    ", iSpawnPointQuantity)
		ENDIF
		
		VECTOR vNewPos
		SWITCH INT_TO_ENUM(eCustomRespawnPointType, iAreaType)
			CASE eCustomRespawnPointType_Box
				vNewPos = GET_VECTOR_CUSTOM_RESPAWN_AREA_BOX(iRespawnPoint, iSpawnPointQuantity, iAreaScale, vCoord, fHead)		BREAK
			CASE eCustomRespawnPointType_LineX
				vNewPos = GET_VECTOR_CUSTOM_RESPAWN_AREA_LINEX(iRespawnPoint, iSpawnPointQuantity, iAreaScale, vCoord, fHead)		BREAK
			CASE eCustomRespawnPointType_LineY
				vNewPos = GET_VECTOR_CUSTOM_RESPAWN_AREA_LINEY(iRespawnPoint, iSpawnPointQuantity, iAreaScale, vCoord, fHead)		BREAK
			CASE eCustomRespawnPointType_Scattered
				vNewPos = GET_VECTOR_CUSTOM_RESPAWN_AREA_SCATTERED(iRespawnPoint, iSpawnPointQuantity, iAreaScale, vCoord, fHead)	BREAK
		ENDSWITCH
		
		IF iGetGroundFromHeight > 0
			VECTOR vStart = vNewPos
			vStart += <<0.0, 0.0, GET_GROUND_Z_HEIGHT_FROM_INDEX(iGetGroundFromHeight)>>
			
			GET_GROUND_Z_FOR_3D_COORD(vStart, vNewPos.z)
		ENDIF
			
		fNewHead = fHead
		RETURN vNewPos
	ENDIF

	fNewHead = fHead
	RETURN vCoord
ENDFUNC

FUNC BOOL IS_RULE_VALID_FOR_CUSTOM_SPAWN_POINT(INT iSpawnPoint, INT iTeam, INT iRule)
	
	IF g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iSpawnPoint].iTreatAsRuleRespawn != -1
		IF iRule != g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iSpawnPoint].iTreatAsRuleRespawn
			PRINTLN("[RCC MISSION] IS_RULE_VALID_FOR_CUSTOM_SPAWN_POINT - Not on correct rule ",g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iSpawnPoint].iTreatAsRuleRespawn," for point ",iSpawnPoint)
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iSpawnPoint].iValidForRules_Start != -1
		IF iRule < g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iSpawnPoint].iValidForRules_Start
			PRINTLN("[RCC MISSION] IS_RULE_VALID_FOR_CUSTOM_SPAWN_POINT - Not yet past iValidForRules_Start ",g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iSpawnPoint].iValidForRules_Start," for point ",iSpawnPoint)
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iSpawnPoint].iValidForRules_End != -1
		IF iRule >= g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iSpawnPoint].iValidForRules_End
			PRINTLN("[RCC MISSION] IS_RULE_VALID_FOR_CUSTOM_SPAWN_POINT - Reached iValidForRules_End ",g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iSpawnPoint].iValidForRules_End," for point ",iSpawnPoint)
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL SHOULD_USE_CUSTOM_SPAWN_POINTS(INT iRuleMod = 0, INT iCustomRule = -1)
	
	INT iRuleToUse = MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam] + iRuleMod
	
	IF iCustomRule > -1
		iRuleToUse = iCustomRule
		PRINTLN("[spawning] SHOULD_USE_CUSTOM_SPAWN_POINTS - Using iCustomRule: ", iCustomRule)
	ENDIF
	
	INT iTeamSpawnPointCounter
	FOR iTeamSpawnPointCounter = 0 TO FMMC_MAX_TEAMSPAWNPOINTS - 1
		IF iRuleToUse < FMMC_MAX_RULES
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[MC_playerBD[iLocalPart].iteam][iTeamSpawnPointCounter].iSpawnBitset, ci_SpawnBS_Treat_As_Respawn_Point)
				IF IS_RULE_VALID_FOR_CUSTOM_SPAWN_POINT(iTeamSpawnPointCounter, MC_playerBD[iLocalPart].iteam, (iRuleToUse))
					PRINTLN("[spawning][RCC MISSION] SHOULD_USE_CUSTOM_SPAWN_POINTS - Returning TRUE as point ",iTeamSpawnPointCounter," is valid for use")
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	PRINTLN("SHOULD_USE_CUSTOM_SPAWN_POINTS - Returning FALSE")
	RETURN FALSE
	
ENDFUNC

FUNC BOOL SHOULD_USE_CUSTOM_START_SPAWN_POINTS()

	INT iTeamSpawnPointCounter
	FOR iTeamSpawnPointCounter = 0 TO FMMC_MAX_TEAMSPAWNPOINTS-1
		IF MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam] < FMMC_MAX_RULES
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[MC_playerBD[iLocalPart].iteam][iTeamSpawnPointCounter].iSpawnBitset, ci_SpawnBS_Initial_Start_Spawn_Point)
				PRINTLN("[spawning][RCC MISSION][SHOULD_USE_CUSTOM_START_SPAWN_POINTS] Using Custom START Points")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDFOR
	RETURN FALSE
ENDFUNC

PROC ADD_ONLY_START_CUSTOM_SPAWN_POINT()
	USE_CUSTOM_SPAWN_POINTS(TRUE, default, default, default, default, default, default, default, default, default, default, TRUE)
	INT iTeamSpawnPointCounter		
	FOR iTeamSpawnPointCounter = 0 TO FMMC_MAX_TEAMSPAWNPOINTS-1
		IF MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam] < FMMC_MAX_RULES
			VECTOR vCoord = <<0.0, 0.0, 0.0>>
			FLOAT fHead = 0.0
			PRINTLN("[FM_MISSION_CONTROLLER_PLAYERS][ADD_ONLY_START_CUSTOM_SPAWN_POINT] Adding some custom Start point(s)")
			
			// We only want to add the points which have Initial Start Spawn Point enabled.
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[MC_playerBD[iLocalPart].iteam][iTeamSpawnPointCounter].iSpawnBitset, ci_SpawnBS_Initial_Start_Spawn_Point)
				// If we have an area, generate multiple points.
				IF g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[MC_playerBD[iLocalPart].iteam][iTeamSpawnPointCounter].iSpawnPointQuantity > 0
					PRINTLN("[FM_MISSION_CONTROLLER_PLAYERS][ADD_ONLY_START_CUSTOM_SPAWN_POINT] iSpawnPointQuantity > 0: generate multiple respawn points.")
					INT i
					FOR i = 1 TO g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[MC_playerBD[iLocalPart].iteam][iTeamSpawnPointCounter].iSpawnPointQuantity	//Skip 0 as that is the marker
						vCoord = GET_CUSTOM_RESPAWN_POSITION(fHead, iTeamSpawnPointCounter, MC_playerBD[iLocalPart].iteam, i, TRUE)
						ADD_CUSTOM_SPAWN_POINT(vCoord, fHead)
					ENDFOR
				ELSE
					PRINTLN("[FM_MISSION_CONTROLLER_PLAYERS][ADD_ONLY_START_CUSTOM_SPAWN_POINT] adding point")
					vCoord = GET_CUSTOM_RESPAWN_POSITION(fHead, iTeamSpawnPointCounter, MC_playerBD[iLocalPart].iteam, -1, TRUE)
					ADD_CUSTOM_SPAWN_POINT(vCoord, fHead)
				ENDIF
			ENDIF
		ENDIF
	ENDFOR	
ENDPROC

PROC ADD_ALL_CUSTOM_SPAWN_POINTS(INT iRuleOverride = -1)
	PRINTLN("[spawning][RCC MISSION][LM][ADD_ALL_CUSTOM_SPAWN_POINTS] - Resetting the Custom Spawn Points and adding new ones. Calling USE_CUSTOM_SPAWN_POINTS ...")
	USE_CUSTOM_SPAWN_POINTS(TRUE, default, default, default, default, default, default, default, default, default, default, TRUE)
	SET_BIT(iLocalBoolCheck21, LBOOL21_USING_CUSTOM_RESPAWN_POINTS)
	
	INT iTeam = MC_playerBD[iLocalPart].iteam
	INT iRuleToUse = MC_ServerBD_4.iCurrentHighestPriority[iTeam]
	PRINTLN("[LM][ADD_ALL_CUSTOM_SPAWN_POINTS] iRuleToUse = ", iRuleToUse, " for team ", iTeam)
	
	IF iRuleOverride > -1
		PRINTLN("[LM][ADD_ALL_CUSTOM_SPAWN_POINTS] - Overriding rule from ", iRuleToUse, " to ", iRuleOverride)
		DEBUG_PRINTCALLSTACK()
		iRuleToUse = iRuleOverride
	ENDIF
	
	INT iTeamSpawnPointCounter
	FOR iTeamSpawnPointCounter = 0 TO FMMC_MAX_TEAMSPAWNPOINTS-1
		IF iRuleToUse < FMMC_MAX_RULES
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iTeamSpawnPointCounter].iSpawnBitset, ci_SpawnBS_Treat_As_Respawn_Point)
												
				PRINTLN("[LM][ADD_ALL_CUSTOM_SPAWN_POINTS] - This Point: ", iTeamSpawnPointCounter)
				PRINTLN("[LM][ADD_ALL_CUSTOM_SPAWN_POINTS] - iRule: ", iRuleToUse, ", iRuleOverride: ", iRuleOverride)
				PRINTLN("[LM][ADD_ALL_CUSTOM_SPAWN_POINTS] - Active on: ", g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iTeamSpawnPointCounter].iTreatAsRuleRespawn)
				PRINTLN("[LM][ADD_ALL_CUSTOM_SPAWN_POINTS] - Valid from: ", g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iTeamSpawnPointCounter].iValidForRules_Start)
				PRINTLN("[LM][ADD_ALL_CUSTOM_SPAWN_POINTS] - Valid until: ", g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iTeamSpawnPointCounter].iValidForRules_End)
				PRINTLN("[LM][ADD_ALL_CUSTOM_SPAWN_POINTS] - iActivationRadius: ", g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iTeamSpawnPointCounter].iActivationRadius)
				
				IF IS_RULE_VALID_FOR_CUSTOM_SPAWN_POINT(iTeamSpawnPointCounter, iTeam, iRuleToUse)
										
					IF IS_BIT_SET(iBSRespawnActiveFromRadius[iTeam][GET_LONG_BITSET_INDEX(iTeamSpawnPointCounter)], GET_LONG_BITSET_BIT(iTeamSpawnPointCounter))
					OR g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iTeamSpawnPointCounter].iActivationRadius = 0
						
						IF NOT IS_BIT_SET(iBSRespawnDeactivated[iTeam][GET_LONG_BITSET_INDEX(iTeamSpawnPointCounter)], GET_LONG_BITSET_BIT(iTeamSpawnPointCounter))
						OR g_bMissionEnding //If the mission is ending and we're still trying to respawn, just spawn somewhere
							VECTOR vCoord = <<0.0, 0.0, 0.0>>
							FLOAT fHead = 0.0
							PRINTLN("[FM_MISSION_CONTROLLER_PLAYERS][ADD_ALL_CUSTOM_SPAWN_POINTS] iTeamSpawnPointCounter:, ", iTeamSpawnPointCounter, " adding some custom respawn points..")
							// If we have an area, generate multiple points.
							IF g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iTeamSpawnPointCounter].iSpawnPointQuantity > 0
								PRINTLN("[FM_MISSION_CONTROLLER_PLAYERS][ADD_ALL_CUSTOM_SPAWN_POINTS] iSpawnPointQuantity > 0: generate multiple respawn points.")
								INT i
								FOR i = 1 TO g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iTeamSpawnPointCounter].iSpawnPointQuantity	//Skip 0 as that is the marker
									IF GET_NUMBER_OF_CUSTOM_SPAWN_POINTS() < MAX_NUMBER_OF_CUSTOM_SPAWN_POINTS
										vCoord = GET_CUSTOM_RESPAWN_POSITION(fHead, iTeamSpawnPointCounter, iTeam, i, TRUE)
										ADD_CUSTOM_SPAWN_POINT(vCoord, fHead)
										PRINTLN("[FM_MISSION_CONTROLLER_PLAYERS][ADD_ALL_CUSTOM_SPAWN_POINTS] adding point")
									ELSE
										PRINTLN("[FM_MISSION_CONTROLLER_PLAYERS][ADD_ALL_CUSTOM_SPAWN_POINTS] RESPAWN POINTS FULL")
									ENDIF
								ENDFOR
							ELSE
								PRINTLN("[FM_MISSION_CONTROLLER_PLAYERS][ADD_ALL_CUSTOM_SPAWN_POINTS] iTeamSpawnPointCounter:, ", iTeamSpawnPointCounter, " adding a custom respawn point..")
								IF GET_NUMBER_OF_CUSTOM_SPAWN_POINTS() < MAX_NUMBER_OF_CUSTOM_SPAWN_POINTS
									vCoord = GET_CUSTOM_RESPAWN_POSITION(fHead, iTeamSpawnPointCounter, iTeam, -1, TRUE)
									ADD_CUSTOM_SPAWN_POINT(vCoord, fHead)
									PRINTLN("[FM_MISSION_CONTROLLER_PLAYERS][ADD_ALL_CUSTOM_SPAWN_POINTS] adding point")
								ELSE
									PRINTLN("[FM_MISSION_CONTROLLER_PLAYERS][ADD_ALL_CUSTOM_SPAWN_POINTS] RESPAWN POINTS FULL")									
								ENDIF
							ENDIF
						ELSE
							PRINTLN("[FM_MISSION_CONTROLLER_PLAYERS][ADD_ALL_CUSTOM_SPAWN_POINTS] iTeamSpawnPointCounter:, ", iTeamSpawnPointCounter, " Is deactivated. Will not add..")
						ENDIF
					ELSE
						PRINTLN("[LM][ADD_ALL_CUSTOM_SPAWN_POINTS] - Currently Deactivated. Can be activated Via Radius.")
					ENDIF				
				ENDIF
			ENDIF
		ENDIF
		PRINTLN("[LM][ADD_ALL_CUSTOM_SPAWN_POINTS] - ------------------------------------------------------------------------------------------------------------------------------")
	ENDFOR	
ENDPROC

PROC MAINTAIN_CUSTOM_RESPAWN_STATUS_DEBUG(INT iTeam, INT iPoint)
	VECTOR vCoord
	FLOAT fActivationRadius 
	
	vCoord 				= g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iPoint].vPos
	fActivationRadius 	= GET_ACTIVATION_RADIUS_RESPAWN_POINT_FROM_INDEX(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iPoint].iActivationRadius)
		
	INT r, g, b
	SWITCH iTeam
		CASE 0		r=50 g=150 b=200		BREAK
		CASE 1		r=200 g=150 b=50		BREAK
		CASE 2		r=50 g=200 b=50 		BREAK
		CASE 3		r=200 g=50 b=200 		BREAK
	ENDSWITCH
	
	INT iStatus
	iStatus = 0
	
	IF IS_BIT_SET(iBSRespawnActiveFromRadius[iTeam][GET_LONG_BITSET_INDEX(iPoint)], GET_LONG_BITSET_BIT(iPoint))
	OR g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iPoint].iActivationRadius = 0
		iStatus = 1
	ENDIF
	
	IF IS_BIT_SET(iBSRespawnDeactivated[iTeam][GET_LONG_BITSET_INDEX(iPoint)], GET_LONG_BITSET_BIT(iPoint))
		iStatus = 2
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iPoint].iSpawnBitset, ci_SpawnBS_Cannot_Be_Deactived_Ever)
	AND IS_BIT_SET(iBSRespawnActiveFromRadius[iTeam][GET_LONG_BITSET_INDEX(iPoint)], GET_LONG_BITSET_BIT(iPoint))
	OR g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iPoint].iActivationRadius = 0
	AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iPoint].iSpawnBitset, ci_SpawnBS_Cannot_Be_Deactived_Ever)
		iStatus = 3
	ENDIF
	
	INT rStatus, gStatus, bStatus, aStatus
	SWITCH iStatus
		CASE 0		rStatus=100 	gStatus=100 	bStatus=100 	aStatus=150		BREAK	// default
		CASE 1		rStatus=50 		gStatus=200 	bStatus=50 		aStatus=150		BREAK	// active
		CASE 2		rStatus=200 	gStatus=50	 	bStatus=50 		aStatus=150		BREAK	// Deactivated
		CASE 3		rStatus=50 		gStatus=50	 	bStatus=200 	aStatus=150		BREAK	// Permanent
	ENDSWITCH	
	
	// Status Representation.
	DRAW_DEBUG_SPHERE(<<vCoord.x, vCoord.y, vCoord.z+1>>, 0.4, rStatus, gStatus, bStatus, aStatus)
	
	// Actual Radius for activation
	DRAW_DEBUG_SPHERE(vCoord, fActivationRadius, r, g, b, 50)	
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iPoint].iSpawnBitset, ci_SpawnBS_Deactivate_All_Others_When_Activated)
		DRAW_DEBUG_SPHERE(<<vCoord.x, vCoord.y, vCoord.z+2>>, 0.4, 200, 200, 100, aStatus)
	ENDIF
ENDPROC

PROC CHECK_IF_NEW_RESPAWN_POINTS_SHOULD_BE_ADDED()
	IF NOT IS_PED_INJURED(localPlayerPed)
		BOOL bMustReloopAndDeactivatePoints
		BOOL bMustActivateSpawnPoint
		INT iIndexActive
		INT iTeamSpawnPointCounter	
		INT iTeam = MC_playerBD[iLocalPart].iteam
		INT iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]
		
		// Check to see if the player is close enough to have activated a set of respawn points.
		IF NOT IS_BIT_SET(iLocalBoolCheck28, LBOOL28_CAN_TEAMS_ACTIVATE_OTHER_TEAMS_RESPAWN_POINTS)
			FOR iTeamSpawnPointCounter = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[iTeam]-1
				IF iRule < FMMC_MAX_RULES
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iTeamSpawnPointCounter].iSpawnBitset, ci_SpawnBS_Treat_As_Respawn_Point)					
						IF IS_RULE_VALID_FOR_CUSTOM_SPAWN_POINT(iTeamSpawnPointCounter, iTeam, iRule)							
							FLOAT fRadius = GET_ACTIVATION_RADIUS_RESPAWN_POINT_FROM_INDEX(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iTeamSpawnPointCounter].iActivationRadius)
							#IF IS_DEBUG_BUILD
							IF bSlipstreamRespawnDebug
								MAINTAIN_CUSTOM_RESPAWN_STATUS_DEBUG(iTeam, iTeamSpawnPointCounter)
							ENDIF
							#ENDIF
							
							IF fRadius > 0
								// If the respawn point is already active, then do not run the below script. We don't want to set off the flow of removing respawn points and such, if we're not activating one.
								IF NOT IS_BIT_SET(iBSRespawnActiveFromRadius[iTeam][GET_LONG_BITSET_INDEX(iTeamSpawnPointCounter)], GET_LONG_BITSET_BIT(iTeamSpawnPointCounter))
									//PRINTLN("[LM][CHECK_IF_NEW_RESPAWN_POINTS_SHOULD_BE_ADDED] - iTeamSpawnPointCounter: ", iTeamSpawnPointCounter)
									//PRINTLN("[LM][CHECK_IF_NEW_RESPAWN_POINTS_SHOULD_BE_ADDED] - We have a respawn point that activates on a radial distance to it,  fRadius: ", fRadius)
									
									VECTOR vPos = g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iTeamSpawnPointCounter].vPos
									VECTOR vPlayerCoords = GET_ENTITY_COORDS(localPlayerPed)
									//PRINTLN("[LM][CHECK_IF_NEW_RESPAWN_POINTS_SHOULD_BE_ADDED] - Ped is not dead, so we want to perform a VDIST2 check with: ")
									//PRINTLN("[LM][CHECK_IF_NEW_RESPAWN_POINTS_SHOULD_BE_ADDED] - vPos: ",  vPos, "  and vPlayerCoords: ", vPlayerCoords)
									
									// If we are within the distance check.
									IF VDIST2(vPlayerCoords, vPos) < POW(fRadius, 2)
										PRINTLN("[LM][CHECK_IF_NEW_RESPAWN_POINTS_SHOULD_BE_ADDED] - We are setting iTeamSpawnPointCounter: ", iTeamSpawnPointCounter, " To now be active..")
										SET_BIT(iBSRespawnActiveFromRadius[iTeam][GET_LONG_BITSET_INDEX(iTeamSpawnPointCounter)], GET_LONG_BITSET_BIT(iTeamSpawnPointCounter))
										CLEAR_BIT(iBSRespawnDeactivated[iTeam][GET_LONG_BITSET_INDEX(iTeamSpawnPointCounter)], GET_LONG_BITSET_BIT(iTeamSpawnPointCounter))
										bMustActivateSpawnPoint = TRUE
										
										// If this point is set in the creator to deactivate other points.
										IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iTeamSpawnPointCounter].iSpawnBitset, ci_SpawnBS_Deactivate_All_Others_When_Activated)
											//PRINTLN("[LM][CHECK_IF_NEW_RESPAWN_POINTS_SHOULD_BE_ADDED] - This spawn point wants to deactivate others: ci_SpawnBS_Deactivate_All_Others_When_Activated")
											iIndexActive = iTeamSpawnPointCounter
											bMustReloopAndDeactivatePoints = TRUE
										ENDIF
									ENDIF				
								ENDIF
							ENDIF
						ELSE
							// Make sure previous points are cleared up							
							CLEAR_BIT(iBSRespawnActiveFromRadius[iTeam][GET_LONG_BITSET_INDEX(iTeamSpawnPointCounter)], GET_LONG_BITSET_BIT(iTeamSpawnPointCounter))
							CLEAR_BIT(iBSRespawnDeactivated[iTeam][GET_LONG_BITSET_INDEX(iTeamSpawnPointCounter)], GET_LONG_BITSET_BIT(iTeamSpawnPointCounter))
						ENDIF						
					ENDIF
				ENDIF
			ENDFOR
		ENDIF
		
		// Remotely add points.
		IF IS_BIT_SET(iLocalBoolCheck28, LBOOL28_CAN_TEAMS_ACTIVATE_OTHER_TEAMS_RESPAWN_POINTS)
			INT iTeamLoop = 0
			FOR iTeamLoop = 0 TO FMMC_MAX_TEAMS-1
				// Check to see if the player is close enough to have activated a set of respawn points.
				FOR iTeamSpawnPointCounter = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[iTeamLoop]-1
					IF MC_ServerBD_4.iCurrentHighestPriority[iTeamLoop] < FMMC_MAX_RULES
						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeamLoop][iTeamSpawnPointCounter].iSpawnBitset, ci_SpawnBS_Treat_As_Respawn_Point)
						
							IF IS_RULE_VALID_FOR_CUSTOM_SPAWN_POINT(iTeamSpawnPointCounter, iTeamLoop, MC_ServerBD_4.iCurrentHighestPriority[iTeamLoop])
								FLOAT fRadius = GET_ACTIVATION_RADIUS_RESPAWN_POINT_FROM_INDEX(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeamLoop][iTeamSpawnPointCounter].iActivationRadius)
								#IF IS_DEBUG_BUILD
								IF bSlipstreamRespawnDebug
									MAINTAIN_CUSTOM_RESPAWN_STATUS_DEBUG(iTeamLoop, iTeamSpawnPointCounter)
								ENDIF
								#ENDIF
								
								IF fRadius > 0
									// If the respawn point is already active, then do not run the below script. We don't want to set off the flow of removing respawn points and such, if we're not activating one.									
									VECTOR vPos = g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeamLoop][iTeamSpawnPointCounter].vPos
									VECTOR vPlayerCoords = GET_ENTITY_COORDS(localPlayerPed)
									
									// If we are within the distance check.
									IF VDIST2(vPlayerCoords, vPos) < POW(fRadius, 2)										
										// Send out event iActivationTeam
										IF g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeamLoop][iTeamSpawnPointCounter].iActivationTeam > -2
											IF g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeamLoop][iTeamSpawnPointCounter].iActivationTeam = iTeam
											OR g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeamLoop][iTeamSpawnPointCounter].iActivationTeam = -1
												IF NOT IS_BIT_SET(iBSRespawnActiveFromRadiusRemoteToggle[iTeamLoop][GET_LONG_BITSET_INDEX(iTeamSpawnPointCounter)], GET_LONG_BITSET_BIT(iTeamSpawnPointCounter))
													BROADCAST_FMMC_REMOTELY_ADD_RESPAWN_POINTS(iTeamLoop, iTeamSpawnPointCounter, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeamLoop][iTeamSpawnPointCounter].iSpawnBitset, ci_SpawnBS_Deactivate_All_Others_When_Activated))
													SET_BIT(iBSRespawnActiveFromRadiusRemoteToggle[iTeamLoop][GET_LONG_BITSET_INDEX(iTeamSpawnPointCounter)], GET_LONG_BITSET_BIT(iTeamSpawnPointCounter))
												ENDIF
											ENDIF
										ENDIF
									ELSE
										CLEAR_BIT(iBSRespawnActiveFromRadiusRemoteToggle[iTeamLoop][GET_LONG_BITSET_INDEX(iTeamSpawnPointCounter)], GET_LONG_BITSET_BIT(iTeamSpawnPointCounter))
									ENDIF
								ENDIF
							ELSE
								// Make sure previous points are cleared up
								CLEAR_BIT(iBSRespawnActiveFromRadius[iTeamLoop][GET_LONG_BITSET_INDEX(iTeamSpawnPointCounter)], GET_LONG_BITSET_BIT(iTeamSpawnPointCounter))
								CLEAR_BIT(iBSRespawnDeactivated[iTeamLoop][GET_LONG_BITSET_INDEX(iTeamSpawnPointCounter)], GET_LONG_BITSET_BIT(iTeamSpawnPointCounter))
								CLEAR_BIT(iBSRespawnActiveFromRadiusRemoteToggle[iTeamLoop][GET_LONG_BITSET_INDEX(iTeamSpawnPointCounter)], GET_LONG_BITSET_BIT(iTeamSpawnPointCounter))
							ENDIF				
						ENDIF
					ENDIF
				ENDFOR
			ENDFOR
		ENDIF
		
		IF NOT IS_BIT_SET(iLocalBoolCheck28, LBOOL28_CAN_TEAMS_ACTIVATE_OTHER_TEAMS_RESPAWN_POINTS)
		OR iRespawnPointIndexToKeep > -1
		OR IS_BIT_SET(iLocalBoolCheck28, LBOOL28_OTHER_TEAM_ACTIVATED_MY_RESPAWN_POINTS)
			
			// If activated from a different team..
			IF iRespawnPointIndexToKeep > -1
				PRINTLN("[LM][CHECK_IF_NEW_RESPAWN_POINTS_SHOULD_BE_ADDED] - iRespawnPointIndexToKeep: ", iRespawnPointIndexToKeep)
				iIndexActive = iRespawnPointIndexToKeep
				bMustReloopAndDeactivatePoints = TRUE
			ENDIF
			
			// A Respawn Point we have reached wants to deactivate all other respawn points. Loop through and deactivate them.
			IF bMustReloopAndDeactivatePoints
				PRINTLN("[LM][CHECK_IF_NEW_RESPAWN_POINTS_SHOULD_BE_ADDED] - We are going to disable all the -OTHER- Respawn points for this team: ", MC_playerBD[iLocalPart].iteam, " and Rule: ", MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam])
				FOR iTeamSpawnPointCounter = 0 TO FMMC_MAX_TEAMSPAWNPOINTS-1
					IF MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam] < FMMC_MAX_RULES
						IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[MC_playerBD[iLocalPart].iteam][iTeamSpawnPointCounter].iSpawnBitset, ci_SpawnBS_Cannot_Be_Deactived_Ever)
							IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[MC_playerBD[iLocalPart].iteam][iTeamSpawnPointCounter].iSpawnBitset, ci_SpawnBS_Treat_As_Respawn_Point)
								IF IS_RULE_VALID_FOR_CUSTOM_SPAWN_POINT(iTeamSpawnPointCounter, MC_playerBD[iLocalPart].iteam, MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam])
									IF iIndexActive != iTeamSpawnPointCounter
										// Putting this in a check to avoid log print spam
										IF NOT IS_BIT_SET(iBSRespawnDeactivated[iTeam][GET_LONG_BITSET_INDEX(iTeamSpawnPointCounter)], GET_LONG_BITSET_BIT(iTeamSpawnPointCounter))
										OR IS_BIT_SET(iBSRespawnActiveFromRadius[iTeam][GET_LONG_BITSET_INDEX(iTeamSpawnPointCounter)], GET_LONG_BITSET_BIT(iTeamSpawnPointCounter))
											//PRINTLN("[LM][CHECK_IF_NEW_RESPAWN_POINTS_SHOULD_BE_ADDED] - Deactivating: ", iTeamSpawnPointCounter, " For team: ", MC_playerBD[iLocalPart].iteam)
										ENDIF
										SET_BIT(iBSRespawnDeactivated[iTeam][GET_LONG_BITSET_INDEX(iTeamSpawnPointCounter)], GET_LONG_BITSET_BIT(iTeamSpawnPointCounter))
										CLEAR_BIT(iBSRespawnActiveFromRadius[iTeam][GET_LONG_BITSET_INDEX(iTeamSpawnPointCounter)], GET_LONG_BITSET_BIT(iTeamSpawnPointCounter))
									ELSE
										//PRINTLN("[LM][CHECK_IF_NEW_RESPAWN_POINTS_SHOULD_BE_ADDED] - Don't Deactivate the newly activated point: ", iTeamSpawnPointCounter, " For team: ", MC_playerBD[iLocalPart].iteam)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDFOR
				PRINTLN("[LM][CHECK_IF_NEW_RESPAWN_POINTS_SHOULD_BE_ADDED] - Calling ADD_ALL_CUSTOM_SPAWN_POINTS to reinitialize the respawn points in Neils system. bMustReloopAndDeactivatePoints")
				// Add the custom spawn points now we have deactivated ones we don't want anymore.		
				ADD_ALL_CUSTOM_SPAWN_POINTS()
				
				iRespawnPointIndexToKeep = -1
				CLEAR_BIT(iLocalBoolCheck28, LBOOL28_OTHER_TEAM_ACTIVATED_MY_RESPAWN_POINTS)
			ELIF bMustActivateSpawnPoint
			OR IS_BIT_SET(iLocalBoolCheck28, LBOOL28_OTHER_TEAM_ACTIVATED_MY_RESPAWN_POINTS)
				PRINTLN("[LM][CHECK_IF_NEW_RESPAWN_POINTS_SHOULD_BE_ADDED] - Calling ADD_ALL_CUSTOM_SPAWN_POINTS to reinitialize the respawn points in Neils system. bMustActivateSpawnPoint")
				ADD_ALL_CUSTOM_SPAWN_POINTS()
				
				iRespawnPointIndexToKeep = -1
				CLEAR_BIT(iLocalBoolCheck28, LBOOL28_OTHER_TEAM_ACTIVATED_MY_RESPAWN_POINTS)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC INCREASE_PLAYER_HEALTH(INT iNewMaxHealth)
	
	IF NOT bFMMCHealthBoosted
		IF IS_NET_PLAYER_OK(LocalPlayer)
			iMaxHealthBeforebigfoot = GET_PED_MAX_HEALTH(LocalPlayerPed)
			PRINTLN("[JS] [BEASTMODE] Increasing health from ", iMaxHealthBeforebigfoot," to ", iNewMaxHealth)
			SET_PED_MAX_HEALTH(LocalPlayerPed, iNewMaxHealth + 100)
			SET_ENTITY_HEALTH(LocalPlayerPed, iNewMaxHealth + 100)
			SET_PED_SUFFERS_CRITICAL_HITS(LocalPlayerPed, FALSE)
			SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_IgnoreMeleeFistWeaponDamageMult, TRUE)
			FLOAT fMaxDamage = TO_FLOAT(iNewMaxHealth / 5)
			IF fMaxDamage >= 1000
				fMaxDamage = 999
			ENDIF
			SET_PLAYER_MAX_EXPLOSIVE_DAMAGE(LocalPlayer, fMaxDamage)
			bFMMCHealthBoosted = TRUE
		ENDIF
	ENDIF

ENDPROC

PROC BLOCK_PLAYER_HEALTH_REGEN(BOOL bSet)
	IF bSet
		PRINTLN("[JS] BLOCK_PLAYER_HEALTH_REGEN - SET")
		SET_PLAYER_HEALTH_RECHARGE_MULTIPLIER(localPlayer, 0.0)
	ELSE
		PRINTLN("[JS] BLOCK_PLAYER_HEALTH_REGEN - NOT SET")
		SET_PLAYER_HEALTH_RECHARGE_MULTIPLIER(localPlayer, 1.0)
	ENDIF
ENDPROC

PROC SET_MINIGUN_PLAYER_DAMAGE(FLOAT fDamage = 1.0)
	PRINTLN("[JS] SET_MINIGUN_PLAYER_DAMAGE - Set to ", fDamage)
	SET_PLAYER_WEAPON_MINIGUN_DEFENSE_MODIFIER(LocalPlayer, fDamage)
ENDPROC

PROC IGNORE_NO_GPS_FLAG(BOOL bSet)
	IF bSet
		PRINTLN("[JT] SET_IGNORE_NO_GPS_FLAG(TRUE)")
		SET_IGNORE_NO_GPS_FLAG(TRUE)
	ELSE
		PRINTLN("[JT] SET_IGNORE_NO_GPS_FLAG(FALSE)")
		SET_IGNORE_NO_GPS_FLAG(FALSE)
	ENDIF
ENDPROC

PROC GIVE_WEAPON_WITH_ACTION_CHECK(WEAPON_TYPE wtWeaponToUse)
	DEBUG_PRINTCALLSTACK()
	IF NOT IS_PED_CLIMBING(LocalPlayerPed)
	AND NOT IS_PED_SWIMMING(LocalPlayerPed)
		SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), wtWeaponToUse, TRUE)
	ELSE
		SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), wtWeaponToUse)
	ENDIF
ENDPROC

PROC SUDDEN_DEATH_LOST_AND_DAMNED_STRONG_LOADOUT(INT iTeam)
	//Checks to see if they have the weapons or not
	IF NOT IS_BIT_SET(iLocalBoolCheck17, LBOOL18_TLAD_SUDDEN_DEATH_WEAPONS + iTeam)
	AND NOT IS_PED_INJURED(localPlayerPed)
		IF DOES_TEAM_HAVE_AN_FMMC_GIVEN_INVENTORY(iTeam, FALSE, FALSE, TRUE )
			IF iTeam != -1
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciMID_MISSION_INVENTORY_2_REMOVE_WEAPONS)
					PRINTLN("[JT] Removing weapons")
					//If the player is on strong weapons when timer runs out, make sure they keep their current weapon.
					IF IS_BIT_SET(iLocalBoolCheck13, LBOOL13_BEEN_GIVEN_MIDMISSION_INVENTORY_2)
						wtLastWeapon[PARTICIPANT_ID_TO_INT()] = GET_SELECTED_PED_WEAPON(PLAYER_PED_ID())
						PRINTLN("[JT LOADOUT] SD WEAPONS, saving weapon if they were on the strong weapons.")
					ENDIF
					
					REMOVE_ALL_PLAYERS_WEAPONS()
					
					SET_PED_ARMOUR(PLAYER_PED_ID(), 0)
					SET_PLAYER_MAX_ARMOUR(PLAYER_ID(), 50)
					
					//Clearing bit for inventory 1
					PRINTLN("[JT] inventory cleared")
					IF IS_BIT_SET(iLocalBoolCheck4, LBOOL4_BEEN_GIVEN_MIDMISSION_INVENTORY)
						CLEAR_BIT(iLocalBoolCheck4, LBOOL4_BEEN_GIVEN_MIDMISSION_INVENTORY)
					ENDIF
				ENDIF
				PRINTLN("[JT][WEAPONS] SD Loadout is active")
				//Giving the loadout
				GIVE_PLAYER_STARTING_MISSION_WEAPONS( WEAPONINHAND_LASTWEAPON_BOTH, FALSE, FALSE, FALSE, TRUE, IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciWEAPONS_FORCE_NO_ADDONS_MID_MISSION_INVENTORY_2))
				
				INT iParticipantNumber = PARTICIPANT_ID_TO_INT()
				
				SET_PLAYER_MAX_ARMOUR(PLAYER_ID(), 250)
				SET_PED_ARMOUR(PLAYER_PED_ID(), 250)
				PRINTLN("[JT][ARMOUR] Armour amount SD: 250")
				
				//Sets player as having the weapons
				IF wtLastWeapon[iParticipantNumber] = WEAPONTYPE_UNARMED OR wtLastWeapon[iParticipantNumber] = WEAPONTYPE_INVALID
					IF DOES_PLAYER_HAVE_WEAPON(GET_MISSION_STARTING_WEAPON(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMidMissionInventory2StartWep))
						GIVE_WEAPON_WITH_ACTION_CHECK(GET_MISSION_STARTING_WEAPON(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMidMissionInventory2StartWep))
						PRINTLN("[JT][LOADOUT] SD Weapon now in hand: ", GET_WEAPON_NAME_FROM_WEAPON_TYPE(wtLastWeapon[iParticipantNumber]))
						wtLastWeapon[iParticipantNumber] = GET_MISSION_STARTING_WEAPON(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMidMissionInventory2StartWep)
						gweapon_type_CurrentlyHeldWeapon = wtLastWeapon[iParticipantNumber]
						SET_BIT(iLocalBoolCheck17,LBOOL18_TLAD_SUDDEN_DEATH_WEAPONS + iTeam)
					ENDIF
				ELSE
					IF DOES_PLAYER_HAVE_WEAPON(wtLastWeapon[iParticipantNumber])
						GIVE_WEAPON_WITH_ACTION_CHECK(wtLastWeapon[iParticipantNumber])
						gweapon_type_CurrentlyHeldWeapon = wtLastWeapon[iParticipantNumber]
						PRINTLN("[JT][LOADOUT] SD Giving player the last weapon they used (wtLastWeapon)")
						SET_BIT(iLocalBoolCheck17,LBOOL18_TLAD_SUDDEN_DEATH_WEAPONS + iTeam)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		PRINTLN("[JT][LOADOUT] SD Team " ,iTeam, " already have weapons")
	ENDIF
ENDPROC

FUNC BOOL DOES_PLAYER_HAVE_A_WEAPON(PED_INDEX piPlayerToCheck)
	IF GET_BEST_PED_WEAPON(piPlayerToCheck, TRUE) = WEAPONTYPE_UNARMED
	OR GET_BEST_PED_WEAPON(piPlayerToCheck, TRUE) = WEAPONTYPE_INVALID
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC
///PURPOSE:
///    Adding particles to the hand of the player
PROC TLAD_WEAPON_PFX(INT iTeam)
	IF NOT IS_LOCAL_PLAYER_SPECTATOR()
		IF iTeam = 0
			IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(TLAD_DevilWeapon)
				USE_PARTICLE_FX_ASSET("scr_bike_adversary")
				TLAD_DevilWeapon = START_NETWORKED_PARTICLE_FX_LOOPED_ON_ENTITY_BONE("scr_adversary_weap_smoke",LocalPlayerPed,<<0,0,0>>,<<0,0,0>>,GET_PED_BONE_INDEX(LocalPlayerPed,BONETAG_PH_R_HAND))
				REINIT_NET_TIMER(stWeaponPFXtimer)
				PRINTLN("[JT PFX] Smoke timer started")
			ENDIF
		ELIF iTeam = 1
			IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(TLAD_AngelWeapon)
				USE_PARTICLE_FX_ASSET("scr_bike_adversary")
				TLAD_AngelWeapon = START_NETWORKED_PARTICLE_FX_LOOPED_ON_ENTITY_BONE("scr_adversary_weap_glow",LocalPlayerPed,<<0,0,0>>,<<0,0,0>>,GET_PED_BONE_INDEX(LocalPlayerPed,BONETAG_PH_R_HAND))
				REINIT_NET_TIMER(stWeaponPFXtimer)
				PRINTLN("[JT PFX] Glow timer started")
			ENDIF	
		ENDIF
	ENDIF
ENDPROC

PROC TLAD_WEAPON_PFX_END()
	IF IS_ANY_INTERACTION_ANIM_PLAYING()
	//OR PIMenuData.iCurrentSelection = 8 //Quickplay actions
		IF DOES_PARTICLE_FX_LOOPED_EXIST(TLAD_DevilWeapon)
			REMOVE_PARTICLE_FX(TLAD_DevilWeapon)
			SET_BIT(iLocalBoolCheck19, LBOOL19_TLAD_BLOCK_WEAPON_PFX)
			PRINTLN("[JT PFX] Force removing particle TLAD_DevilWeapon because of quickplay anim")
		ELIF DOES_PARTICLE_FX_LOOPED_EXIST(TLAD_AngelWeapon)
			REMOVE_PARTICLE_FX(TLAD_AngelWeapon)
			SET_BIT(iLocalBoolCheck19, LBOOL19_TLAD_BLOCK_WEAPON_PFX)
			PRINTLN("[JT PFX] Force removing particle AngelWeapon because of quickplay anim")
		ENDIF
	ELIF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stWeaponPFXtimer) >= 4000
		IF DOES_PARTICLE_FX_LOOPED_EXIST(TLAD_DevilWeapon)
			STOP_PARTICLE_FX_LOOPED(TLAD_DevilWeapon)
			RESET_NET_TIMER(stWeaponPFXtimer)
			PRINTLN("[JT PFX] ending weapon switch particles Devils")
		ELIF DOES_PARTICLE_FX_LOOPED_EXIST(TLAD_AngelWeapon)
			STOP_PARTICLE_FX_LOOPED(TLAD_AngelWeapon)
			RESET_NET_TIMER(stWeaponPFXtimer)
			PRINTLN("[JT PFX] ending weapon switch particles Angels")
		ENDIF
	ENDIF
ENDPROC

PROC BLOCK_WEAPON_ACTIONS_WHILE_SWAPPING()
	IF (IS_PED_RELOADING(LocalPlayerPed) OR IS_PED_RELOADING(localPlayerPed) OR IS_PED_SHOOTING(localPlayerPed) 
	OR IS_PED_PERFORMING_MELEE_ACTION(LocalPlayerPed) OR IS_PED_USING_ACTION_MODE(LocalPlayerPed) OR IS_PED_IN_MELEE_COMBAT(LocalPlayerPed))
	AND NOT GET_PED_STEALTH_MOVEMENT(LocalPlayerPed)
	AND NOT IS_PED_JUMPING(LocalPlayerPed)
	AND NOT IS_PED_CLIMBING(LocalPlayerPed)
		PRINTLN("[JT][LOADOUT] Loadout block Clear Sec Tasks")
		CLEAR_PED_SECONDARY_TASK(LocalPlayerPed)
		SET_PED_USING_ACTION_MODE(LocalPlayerPed, FALSE)
		CLEAR_PED_TASKS(LocalPlayerPed)
	ENDIF
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_UD)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_LR)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_NEXT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_PREV)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_NEXT_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_PREV_WEAPON)
ENDPROC

PROC SET_WEAPON_LOADOUT_FOR_RULE(INT iTeam, INT iRule)

	// Sets a Local Player as ready to receive a new weapon.
	IF IS_BIT_SET(iLocalBoolCheck6, (LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam))
		//Store weapon at the start of weak rules
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSeven[iRule], ciBS_RULE7_WEAPON_LOADOUT1)
			IF DOES_PLAYER_HAVE_A_WEAPON(PLAYER_PED_ID())
				wtLastWeapon[PARTICIPANT_ID_TO_INT()] = GET_SELECTED_PED_WEAPON(PLAYER_PED_ID())
			ENDIF
		ENDIF
		SET_BIT(iLocalBoolCheck18, LBOOL18_WEAPON_LOADOUT_READY)
		
		IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_LOST_DAMNED(g_FMMC_STRUCT.iAdversaryModeType)
			RESET_NET_TIMER(stWeaponPFXActivation)
			START_NET_TIMER(stWeaponPFXActivation)
			IF iRule = 0
				REINIT_NET_TIMER(stWeaponPFXtimer)
			ENDIF
			PRINTLN("[JT PFX] Timer length ", GET_FMMC_OBJECTIVE_TIME_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjectiveTimeLimitRule[iRule]) - 1000, " Rule: ", iRule)
		ENDIF
		PRINTLN("[JT][LOADOUT] SETTING BIT: LBOOL18_WEAPON_LOADOUT_READY")
	ENDIF
	
	BOOL bHasRemainingTime = FALSE
	
	IF GET_TIME_REMAINING_ON_MULTIRULE_TIMER() > 1000
	OR GET_REMAINING_TIME_ON_RULE_SUDDENDEATH(iTeam, iRule, TRUE) > 1000
	OR GET_REMAINING_TIME_ON_RULE_SUDDENDEATH(iTeam, iRule, FALSE) > 1000
	OR GET_REMAINING_TIME_ON_RULE(iTeam, iRule) > 1000
		bHasRemainingTime = TRUE
	ENDIF
	
	IF g_bMissionEnding
	OR IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
	OR HAS_TEAM_FINISHED(MC_playerBD[iLocalPart].iteam)
	OR HAS_TEAM_FAILED(MC_playerBD[iLocalPart].iteam)
	OR IS_PED_INJURED(LocalPlayerPed)
		EXIT
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSeven[iRule], ciBS_RULE7_WEAPON_LOADOUT1)
	AND (NOT IS_BIT_SET(MC_serverBD.iServerBitSet1,SBBOOL1_SUDDEN_DEATH) OR (IS_BIT_SET(MC_serverBD.iServerBitSet1,SBBOOL1_SUDDEN_DEATH) AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_LOST_DAMNED(g_FMMC_STRUCT.iAdversaryModeType)))
		//Checking if ped is injured before giving weapons
		IF NOT IS_PED_INJURED(localPlayerPed)
		AND IS_BIT_SET(iLocalBoolCheck18, LBOOL18_WEAPON_LOADOUT_READY)
		AND bHasRemainingTime
			IF DOES_TEAM_HAVE_AN_FMMC_GIVEN_INVENTORY(iTeam, FALSE, TRUE )
			AND NOT IS_BIT_SET( iLocalBoolCheck4, LBOOL4_BEEN_GIVEN_MIDMISSION_INVENTORY )
				IF iTeam != -1
				AND iRule != -1
					IF MC_serverBD_4.iCurrentHighestPriority[iTeam] >= iRule
					AND MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
						//If option is set in creator, clear all current weapons and armour
						INT iParticipantNumber = PARTICIPANT_ID_TO_INT()
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciMID_MISSION_INVENTORY_REMOVE_WEAPONS)
							
							//Setting wtLastWeapon before clearing weapons
							IF IS_BIT_SET(iLocalBoolCheck13, LBOOL13_BEEN_GIVEN_MIDMISSION_INVENTORY_2)
								wtLastWeapon[iParticipantNumber] = GET_SELECTED_PED_WEAPON(PLAYER_PED_ID())
								CLEAR_BIT(iLocalBoolCheck13, LBOOL13_BEEN_GIVEN_MIDMISSION_INVENTORY_2)
								PRINTLN("[JT][LOADOUT] Weapon just stored: ", GET_WEAPON_NAME_FROM_WEAPON_TYPE(wtLastWeapon[iParticipantNumber]))
							ENDIF
							IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMidMissionInventoryStartWep != -1
								gweapon_type_CurrentlyHeldWeapon = GET_MISSION_STARTING_WEAPON(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMidMissionInventoryStartWep)
							ENDIF
							PRINTLN("[JT][LOADOUT] wtLastWeapon confirm", GET_WEAPON_NAME_FROM_WEAPON_TYPE(wtLastWeapon[iParticipantNumber]))
							
							IF GET_PED_STEALTH_MOVEMENT(LocalPlayerPed)
								PRINTLN("[JT][LOADOUT] was in stealth. setting LBOOL26_PUT_BACK_INTO_STEALTH_MODE")
								SET_BIT(iLocalBoolCheck26, LBOOL26_PUT_BACK_INTO_STEALTH_MODE_WEP_CHANGE)
							ENDIF
							
							IF (IS_PED_RELOADING(LocalPlayerPed) OR IS_PED_RELOADING(localPlayerPed) OR IS_PED_SHOOTING(localPlayerPed) 
							OR IS_PED_PERFORMING_MELEE_ACTION(LocalPlayerPed) OR IS_PED_USING_ACTION_MODE(LocalPlayerPed) OR IS_PED_IN_MELEE_COMBAT(LocalPlayerPed))
							AND NOT IS_PED_JUMPING(LocalPlayerPed)
							AND NOT IS_PED_CLIMBING(LocalPlayerPed)
							AND NOT GET_PED_STEALTH_MOVEMENT(LocalPlayerPed)
								PRINTLN("[JT][LOADOUT] Loadout 1 Clear Sec Tasks")								
								SET_PED_USING_ACTION_MODE(LocalPlayerPed, FALSE)
							
								CLEAR_PED_TASKS(LocalPlayerPed)
								CLEAR_PED_SECONDARY_TASK(LocalPlayerPed)
							ENDIF
							
							REMOVE_ALL_PLAYERS_WEAPONS()
							SET_PED_ARMOUR(PLAYER_PED_ID(), 0)
						ENDIF
						
						BLOCK_WEAPON_ACTIONS_WHILE_SWAPPING()
						
						//Giving player the selected loadout
						PRINTLN("[JT][LOADOUT] Loadout 1 is active")
						PRINTLN("[LM][LOADOUT] Should weapon addons and tints be forced off:  ", IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciWEAPONS_FORCE_NO_ADDONS_MID_MISSION_INVENTORY))
						IF GIVE_PLAYER_STARTING_MISSION_WEAPONS( WEAPONINHAND_LASTWEAPON_BOTH, FALSE, TRUE, !IS_JOB_OWNED_WEAPONS_PLUS_PICKUPS(), DEFAULT, IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciWEAPONS_FORCE_NO_ADDONS_MID_MISSION_INVENTORY))
						
							SET_PED_ARMOUR(PLAYER_PED_ID(), g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iArmourAmount[iRule])
							
							PRINTLN("[JT][LOADOUT] Creator set starting weapon ", GET_WEAPON_NAME_FROM_WEAPON_TYPE(GET_MISSION_STARTING_WEAPON(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMidMissionInventoryStartWep)))
							
							IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMidMissionInventoryStartWep = -1
								PRINTLN("[JT][LOADOUT] STARTING WEAPON Loadout 1. Team: ", iTeam, " Setting as strongest weapon")
								GIVE_WEAPON_WITH_ACTION_CHECK(GET_BEST_PED_WEAPON(PLAYER_PED_ID()))
								SET_BIT(iLocalBoolCheck4, LBOOL4_BEEN_GIVEN_MIDMISSION_INVENTORY)
							ELSE
								IF DOES_PLAYER_HAVE_WEAPON(GET_MISSION_STARTING_WEAPON(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMidMissionInventoryStartWep))
									GIVE_WEAPON_WITH_ACTION_CHECK(GET_MISSION_STARTING_WEAPON(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMidMissionInventoryStartWep))
									SET_BIT(iLocalBoolCheck4, LBOOL4_BEEN_GIVEN_MIDMISSION_INVENTORY)
									PRINTLN("[JT][LOADOUT] STARTING WEAPON Loadout 1. Team: ", iTeam, "   ", GET_WEAPON_NAME_FROM_WEAPON_TYPE(GET_MISSION_STARTING_WEAPON(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMidMissionInventoryStartWep)))
								ENDIF
							ENDIF
							IF DOES_PLAYER_HAVE_A_WEAPON(PLAYER_PED_ID())
								IF IS_BIT_SET(iLocalBoolCheck4, LBOOL4_BEEN_GIVEN_MIDMISSION_INVENTORY)
									PRINTLN("[JT][LOADOUT] Player has a valid weapon. Clearing ready bit")
									CLEAR_BIT(iLocalBoolCheck18, LBOOL18_WEAPON_LOADOUT_READY)
									
									BLOCK_WEAPON_ACTIONS_WHILE_SWAPPING()
								ENDIF
							ELSE
								CLEAR_BIT(iLocalBoolCheck4, LBOOL4_BEEN_GIVEN_MIDMISSION_INVENTORY)
							ENDIF
							
							IF IS_BIT_SET(iLocalBoolCheck26, LBOOL26_PUT_BACK_INTO_STEALTH_MODE_WEP_CHANGE)
								SET_PED_STEALTH_MOVEMENT(LocalPlayerPed, TRUE)
								CLEAR_BIT(iLocalBoolCheck26, LBOOL26_PUT_BACK_INTO_STEALTH_MODE_WEP_CHANGE)
								PRINTLN("[JT][LOADOUT] Putting back into stealth.")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
			//Reload Ammo
			ELIF DOES_TEAM_HAVE_AN_FMMC_GIVEN_INVENTORY(iTeam, FALSE, TRUE )
			AND IS_BIT_SET( iLocalBoolCheck4, LBOOL4_BEEN_GIVEN_MIDMISSION_INVENTORY )
				PRINTLN("[JT][LOADOUT] Perform a reload. (1)")
				CLEAR_BIT( iLocalBoolCheck4, LBOOL4_BEEN_GIVEN_MIDMISSION_INVENTORY )
			ENDIF
		ENDIF
	ELIF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSeven[iRule], ciBS_RULE7_WEAPON_LOADOUT2)
	AND (NOT IS_BIT_SET(MC_serverBD.iServerBitSet1,SBBOOL1_SUDDEN_DEATH) OR (IS_BIT_SET(MC_serverBD.iServerBitSet1,SBBOOL1_SUDDEN_DEATH) AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_LOST_DAMNED(g_FMMC_STRUCT.iAdversaryModeType)))
		//Checking if ped is injured before giving weapons
		IF NOT IS_PED_INJURED(localPlayerPed)
			IF IS_BIT_SET(iLocalBoolCheck18, LBOOL18_WEAPON_LOADOUT_READY)
			AND bHasRemainingTime
				IF DOES_TEAM_HAVE_AN_FMMC_GIVEN_INVENTORY(iTeam, FALSE, FALSE, TRUE )
				AND NOT IS_BIT_SET( iLocalBoolCheck13, LBOOL13_BEEN_GIVEN_MIDMISSION_INVENTORY_2 )
					IF iTeam != -1
					AND iRule != -1
						IF MC_serverBD_4.iCurrentHighestPriority[iTeam] >= iRule
						AND MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
							//If option is set in creator, clear all current weapons and armour
							IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciMID_MISSION_INVENTORY_2_REMOVE_WEAPONS)
								REMOVE_ALL_PLAYERS_WEAPONS()
								SET_PED_ARMOUR(PLAYER_PED_ID(), 0)
								//Clearing bit for inventory 1
								IF IS_BIT_SET(iLocalBoolCheck4, LBOOL4_BEEN_GIVEN_MIDMISSION_INVENTORY)
									CLEAR_BIT(iLocalBoolCheck4, LBOOL4_BEEN_GIVEN_MIDMISSION_INVENTORY)
								ENDIF
								
								IF wtLastWeapon[PARTICIPANT_ID_TO_INT()] != WEAPONTYPE_INVALID
									gweapon_type_CurrentlyHeldWeapon = wtLastWeapon[PARTICIPANT_ID_TO_INT()]
									PRINTLN("[JT][LOADOUT] Setting CurrentlyHeldWeapon to: ", GET_WEAPON_NAME_FROM_WEAPON_TYPE(gweapon_type_CurrentlyHeldWeapon))
								ENDIF
							ENDIF
								
							IF GET_PED_STEALTH_MOVEMENT(LocalPlayerPed)
								PRINTLN("[JT][LOADOUT] was in stealth. setting LBOOL26_PUT_BACK_INTO_STEALTH_MODE")
								SET_BIT(iLocalBoolCheck26, LBOOL26_PUT_BACK_INTO_STEALTH_MODE_WEP_CHANGE)
							ENDIF
							
							BLOCK_WEAPON_ACTIONS_WHILE_SWAPPING()
							
							//Giving player the selected loadout
							PRINTLN("[JT][LOADOUT] Loadout 2 is active")
							PRINTLN("[LM][LOADOUT] Should weapon addons and tints be forced off:  ", IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciWEAPONS_FORCE_NO_ADDONS_MID_MISSION_INVENTORY_2))
							IF GIVE_PLAYER_STARTING_MISSION_WEAPONS( WEAPONINHAND_LASTWEAPON_BOTH, FALSE, FALSE, !IS_JOB_OWNED_WEAPONS_PLUS_PICKUPS(), TRUE, IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciWEAPONS_FORCE_NO_ADDONS_MID_MISSION_INVENTORY_2))
								INT iParticipantNumber = PARTICIPANT_ID_TO_INT()
								
								IF (IS_PED_RELOADING(LocalPlayerPed) OR IS_PED_RELOADING(localPlayerPed) OR IS_PED_SHOOTING(localPlayerPed) 
								OR IS_PED_PERFORMING_MELEE_ACTION(LocalPlayerPed) OR IS_PED_USING_ACTION_MODE(LocalPlayerPed) OR IS_PED_IN_MELEE_COMBAT(LocalPlayerPed))
								AND NOT IS_PED_JUMPING(LocalPlayerPed)
								AND NOT IS_PED_CLIMBING(LocalPlayerPed)
								AND NOT GET_PED_STEALTH_MOVEMENT(LocalPlayerPed)
									PRINTLN("[JT][LOADOUT] Loadout 2 Clear Sec Tasks")									
									SET_PED_USING_ACTION_MODE(LocalPlayerPed, FALSE)
									
									CLEAR_PED_TASKS(LocalPlayerPed)
									CLEAR_PED_SECONDARY_TASK(LocalPlayerPed)
								ENDIF
								
								PRINTLN("[JT][LOADOUT] No weapon yet, wtLastWeapon: ", GET_WEAPON_NAME_FROM_WEAPON_TYPE(wtLastWeapon[iParticipantNumber]))
								SET_BIT(iLocalBoolCheck13, LBOOL13_BEEN_GIVEN_MIDMISSION_INVENTORY_2)
								SET_PED_ARMOUR(PLAYER_PED_ID(), g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iArmourAmount[iRule])
								
								PRINTLN("[JT][LOADOUT] Creator set starting weapon 2: ", GET_WEAPON_NAME_FROM_WEAPON_TYPE(GET_MISSION_STARTING_WEAPON(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMidMissionInventory2StartWep)))
								IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMidMissionInventory2StartWep = -1 AND wtLastWeapon[iParticipantNumber] = WEAPONTYPE_INVALID
									PUT_WEAPON_IN_HAND(WEAPONINHAND_LASTWEAPON_BOTH)
									PRINTLN("[JT][LOADOUT] no iMidMissionInventory2StartWep or wtLastWeapon. Giving a weapon from inventory.")
									SET_BIT(iLocalBoolCheck13, LBOOL13_BEEN_GIVEN_MIDMISSION_INVENTORY_2)
								ELSE
									IF wtLastWeapon[iParticipantNumber] = WEAPONTYPE_INVALID OR wtLastWeapon[iParticipantNumber] = WEAPONTYPE_UNARMED
										//Give player the starting weapon if wtLastWeapon is invalid
										IF DOES_PLAYER_HAVE_WEAPON(GET_MISSION_STARTING_WEAPON(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMidMissionInventory2StartWep))
											GIVE_WEAPON_WITH_ACTION_CHECK(GET_MISSION_STARTING_WEAPON(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMidMissionInventory2StartWep))
											PRINTLN("[JT][LOADOUT] STARTING WEAPON Loadout 2. Team: ", iTeam, "   ", GET_WEAPON_NAME_FROM_WEAPON_TYPE(GET_MISSION_STARTING_WEAPON(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMidMissionInventory2StartWep)))
											wtLastWeapon[iParticipantNumber] = GET_MISSION_STARTING_WEAPON(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMidMissionInventory2StartWep)
											PRINTLN("[JT][LOADOUT] Weapon just stored: ", GET_WEAPON_NAME_FROM_WEAPON_TYPE(wtLastWeapon[iParticipantNumber]))
											gweapon_type_CurrentlyHeldWeapon = wtLastWeapon[iParticipantNumber]
											SET_BIT(iLocalBoolCheck13, LBOOL13_BEEN_GIVEN_MIDMISSION_INVENTORY_2)
											PRINTLN("[JT][LOADOUT] Giving player the set starting weapon")
										ENDIF
									ELSE 
										//Give player wtLastWeapon if they have it
										IF DOES_PLAYER_HAVE_WEAPON(wtLastWeapon[iParticipantNumber])
											GIVE_WEAPON_WITH_ACTION_CHECK(wtLastWeapon[iParticipantNumber])
											gweapon_type_CurrentlyHeldWeapon = wtLastWeapon[iParticipantNumber]
											SET_BIT(iLocalBoolCheck13, LBOOL13_BEEN_GIVEN_MIDMISSION_INVENTORY_2)
											PRINTLN("[JT][LOADOUT] Giving player the last weapon they used (wtLastWeapon)")
										ELSE
											//Fallback for if it gets through to here, will give starting weapon.
											IF DOES_PLAYER_HAVE_WEAPON(GET_MISSION_STARTING_WEAPON(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMidMissionInventory2StartWep))
												GIVE_WEAPON_WITH_ACTION_CHECK(GET_MISSION_STARTING_WEAPON(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMidMissionInventory2StartWep))
												wtLastWeapon[iParticipantNumber] = GET_MISSION_STARTING_WEAPON(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMidMissionInventory2StartWep)
												PRINTLN("[JT][LOADOUT] Weapon just stored: ", GET_WEAPON_NAME_FROM_WEAPON_TYPE(wtLastWeapon[iParticipantNumber]))
												gweapon_type_CurrentlyHeldWeapon = wtLastWeapon[iParticipantNumber]
												SET_BIT(iLocalBoolCheck13, LBOOL13_BEEN_GIVEN_MIDMISSION_INVENTORY_2)
												PRINTLN("[JT][LOADOUT] Giving player the starting weapon as a fallback")
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								
								IF DOES_PLAYER_HAVE_A_WEAPON(PLAYER_PED_ID())
									IF IS_BIT_SET(iLocalBoolCheck13, LBOOL13_BEEN_GIVEN_MIDMISSION_INVENTORY_2)
										CLEAR_BIT(iLocalBoolCheck18, LBOOL18_WEAPON_LOADOUT_READY)
										PRINTLN("[JT][LOADOUT] Player has weapon. Clearing Loadout ready Bit")
										
										BLOCK_WEAPON_ACTIONS_WHILE_SWAPPING()
									ENDIF
								ELSE
									CLEAR_BIT(iLocalBoolCheck13, LBOOL13_BEEN_GIVEN_MIDMISSION_INVENTORY_2)
								ENDIF
								
								IF IS_BIT_SET(iLocalBoolCheck26, LBOOL26_PUT_BACK_INTO_STEALTH_MODE_WEP_CHANGE)
									SET_PED_STEALTH_MOVEMENT(LocalPlayerPed, TRUE)
									CLEAR_BIT(iLocalBoolCheck26, LBOOL26_PUT_BACK_INTO_STEALTH_MODE_WEP_CHANGE)
									PRINTLN("[JT][LOADOUT] Putting back into stealth.")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				
				// Reload Ammo
				ELIF DOES_TEAM_HAVE_AN_FMMC_GIVEN_INVENTORY(iTeam, FALSE, FALSE, TRUE )
				AND IS_BIT_SET( iLocalBoolCheck13, LBOOL13_BEEN_GIVEN_MIDMISSION_INVENTORY_2 )
					PRINTLN("[JT][LOADOUT] perform a reload. (2)")	
					CLEAR_BIT( iLocalBoolCheck13, LBOOL13_BEEN_GIVEN_MIDMISSION_INVENTORY_2 )
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//Sudden Death and particle management for Lost and Damned
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_LOST_DAMNED(g_FMMC_STRUCT.iAdversaryModeType)
		
		IF IS_BIT_SET(MC_serverBD.iServerBitSet1,SBBOOL1_SUDDEN_DEATH)
			PRINTLN("[JT] SUDDEN DEATH WEAPONS ACTIVATING, Current team: ", iTeam)
			SUDDEN_DEATH_LOST_AND_DAMNED_STRONG_LOADOUT(iTeam)
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTen, ciTLAD_USE_PARTICLE_EFFECTS)
			IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stWeaponPFXActivation) >= (GET_FMMC_OBJECTIVE_TIME_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjectiveTimeLimitRule[iRule]) - iWepFXStartTime)
			AND GET_TIME_REMAINING_ON_MULTIRULE_TIMER() > iWepFXStartTime
			AND NOT IS_QUICKPLAY_ANIM_BUTTON_PRESSED()
				IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet1,SBBOOL1_SUDDEN_DEATH)
				AND NOT IS_BIT_SET(iLocalBoolCheck19, LBOOL19_TLAD_BLOCK_WEAPON_PFX)
					TLAD_WEAPON_PFX(iTeam)
					RESET_NET_TIMER(stWeaponPFXActivation)
				ENDIF
			ENDIF
			//Runs checks for ending the particles
			TLAD_WEAPON_PFX_END()
			
		ENDIF
	ENDIF
	
ENDPROC

PROC GET_SPECIAL_AMMO_AMOUNT(WEAPON_TYPE WeaponType, INT& iAmmoCount, AMMO_TYPE& ammoType)
	IF GET_WEAPON_AMMO_TYPE_FROM_WEAPON_TYPE(WeaponType, ammoType)
		IF GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), WeaponType) != 0
			iAmmoCount = GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), WeaponType)
		ENDIF
	ENDIF
ENDPROC

PROC SWITCH_TO_JUGGERNAUT_FOR_SUDDEN_DEATH()
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_JUGGERNAUT(g_FMMC_STRUCT.iAdversaryModeType)
		IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_SUDDEN_DEATH_JUGGERNAUT)
			PRINTLN("[JS] SWITCH_TO_JUGGERNAUT_FOR_SUDDEN_DEATH - We are already a juggernaut")
			EXIT
		ENDIF
		INT iTeam
		FOR iTeam = 0 TO FMMC_MAX_TEAMS - 1
			IF iTeam != MC_PlayerBD[iLocalPart].iteam
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitset2, ciBS2_JUGGERNAUTS)
				AND DOES_TEAM_LIKE_TEAM(iTeam, MC_PlayerBD[iLocalPart].iteam)
					PRINTLN("[JS] SWITCH_TO_JUGGERNAUT_FOR_SUDDEN_DEATH - Friendly Juggernaut team found: ", iTeam)
					BREAKLOOP
				ENDIF
			ENDIF
			IF iTeam >= FMMC_MAX_TEAMS - 1
				PRINTLN("[JS] SWITCH_TO_JUGGERNAUT_FOR_SUDDEN_DEATH - No friendly Juggernaut team found, bailing")
				EXIT
			ENDIF
		ENDFOR
		//Do Juggernaut init
		SET_BIT(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_SUDDEN_DEATH_JUGGERNAUT)
		//SOUNDS
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciENABLE_POWER_MAD_SOUNDS)
			STOP_AUDIO_SCENE("DLC_GR_PM_JN_Player_Is_Attacker_Scene")
			START_AUDIO_SCENE("DLC_GR_PM_JN_Player_Is_JN_Scene")
			PLAY_SOUND_FROM_ENTITY(-1, "Transform_JN_VFX", LocalPlayerPed, "DLC_GR_PM_Juggernaut_Player_Sounds", TRUE)
		ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_ENABLE_TRADING_PLACES_SOUNDS)
			STOP_AUDIO_SCENE("DLC_GR_PM_JN_Player_Is_Attacker_Scene")
			START_AUDIO_SCENE("DLC_GR_PM_JN_Player_Is_JN_Scene")
			PLAY_SOUND_FROM_ENTITY(-1, "Transform_JN_VFX", LocalPlayerPed, "DLC_BTL_TP_Remix_Juggernaut_Player_Sounds", TRUE)
		ELSE
			STOP_AUDIO_SCENE("DLC_IE_JN_Player_Is_Attacker_Scene")
			START_AUDIO_SCENE("DLC_IE_JN_Player_Is_JN_Scene")
			PLAY_SOUND_FROM_ENTITY(-1, "Transform_JN_VFX", LocalPlayerPed, "DLC_IE_JN_Player_Sounds", TRUE)
		ENDIF
		//OUTFIT
		MP_OUTFITS_APPLY_DATA   sApplyData
		sApplyData.pedID		= LocalPlayerPed
		sApplyData.eApplyStage 	= AOS_SET
		IF GET_HUD_COLOUR_FOR_FMMC_TEAM(iTeam, LocalPlayer) = HUD_COLOUR_ORANGE
			sApplyData.eOutfit		= OUTFIT_HIDDEN_IE_JN4_ORANGE_TARGET_0
		ELIF GET_HUD_COLOUR_FOR_FMMC_TEAM(iTeam, LocalPlayer) = HUD_COLOUR_PURPLE
			sApplyData.eOutfit		= OUTFIT_HIDDEN_IE_JN4_PURPLE_TARGET_0
		ELSE
			sApplyData.eOutfit		= OUTFIT_HIDDEN_IE_JN_TARGET_0
		ENDIF
		MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(sApplyData.eOutfit)
		SET_PED_MP_OUTFIT(sApplyData, FALSE, FALSE, FALSE, FALSE)
		//WEAPONS
		REMOVE_ALL_PLAYERS_WEAPONS()
		g_i_Mission_team = iTeam	//Temp change to set weapons
		IF GIVE_PLAYER_STARTING_MISSION_WEAPONS( WEAPONINHAND_LASTWEAPON_BOTH, TRUE, FALSE, TRUE, DEFAULT, DEFAULT)
			CLEAR_PED_TASKS_IMMEDIATELY(LocalPlayerPed)
			PRINTLN("[JS] SWITCH_TO_JUGGERNAUT_FOR_SUDDEN_DEATH - Swapped weapons")
			WEAPON_TYPE wtCurrentWeap
			GET_CURRENT_PED_WEAPON(LocalPlayerPed, wtCurrentWeap)
			IF DOES_PLAYER_HAVE_WEAPON(WEAPONTYPE_MINIGUN)
				PRINTLN("[JS] SWITCH_TO_JUGGERNAUT_FOR_SUDDEN_DEATH - Minigun")
				IF wtCurrentWeap != WEAPONTYPE_MINIGUN
					SET_CURRENT_PED_WEAPON(LocalPlayerPed, WEAPONTYPE_MINIGUN, TRUE)
				ENDIF
			ELSE
				PRINTLN("[JS] SWITCH_TO_JUGGERNAUT_FOR_SUDDEN_DEATH - Best weapon")
				IF wtCurrentWeap != GET_BEST_PED_WEAPON(LocalPlayerPed)
					SET_CURRENT_PED_WEAPON(LocalPlayerPed, GET_BEST_PED_WEAPON(LocalPlayerPed), TRUE)
				ENDIF
			ENDIF
			FORCE_PED_AI_AND_ANIMATION_UPDATE(LocalPlayerPed)
		ENDIF
		g_i_Mission_team = MC_PlayerBD[iLocalPart].iTeam	//Change our team back
		//VFX
		IF HAS_NAMED_PTFX_ASSET_LOADED("scr_impexp_jug")
			PRINTLN("[JS] SWITCH_TO_JUGGERNAUT_FOR_SUDDEN_DEATH - JUGGERNAUT Transform_Local_Player")
			USE_PARTICLE_FX_ASSET("scr_impexp_jug")
			START_NETWORKED_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_impexp_jug_outfit_swap", LocalPlayerPed, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
		ENDIF
		//Ped Changes
		SET_PLAYER_CAN_USE_COVER(LocalPlayer, FALSE)
		IF MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEight[MC_serverBD_4.iCurrentHighestPriority[iTeam]], ciBS_RULE8_EXPLOSIONS_DONT_RAGDOLL)
				SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_DontActivateRagdollFromExplosions, TRUE)
			ELSE
				SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_DontActivateRagdollFromExplosions, FALSE)
			ENDIF
			
			SET_MINIGUN_PLAYER_DAMAGE(TO_FLOAT(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMinigunDamage[MC_serverBD_4.iCurrentHighestPriority[iTeam]])/100)	
			
			BLOCK_PLAYER_HEALTH_REGEN(IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEight[MC_serverBD_4.iCurrentHighestPriority[iTeam]], ciBS_RULE8_BLOCK_HEALTH_REGEN))
	
			IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMaxHealth[MC_serverBD_4.iCurrentHighestPriority[iTeam]] != 0
				bFMMCHealthBoosted = FALSE
				INT iNewMaxHealth = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMaxHealth[MC_serverBD_4.iCurrentHighestPriority[iTeam]]
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEight[MC_serverBD_4.iCurrentHighestPriority[iTeam]], ciBS_RULE8_SCALE_MAX_PLAYER_HEALTH)
					INT i
					INT iEnemyPlayers
					FOR i = 0 TO FMMC_MAX_TEAMS - 1
						PRINTLN("[JS] SWITCH_TO_JUGGERNAUT_FOR_SUDDEN_DEATH - Health is being scaled by players: ", MC_serverBD.iNumberOfPlayingPlayers[i])
						iEnemyPlayers = iEnemyPlayers + MC_serverBD.iNumberOfPlayingPlayers[i]
					ENDFOR
					iNewMaxHealth = (iNewMaxHealth * iEnemyPlayers)
				ENDIF
				INCREASE_PLAYER_HEALTH(iNewMaxHealth)
				PRINTLN("[JS] SWITCH_TO_JUGGERNAUT_FOR_SUDDEN_DEATH - Player Health Modded to ", iNewMaxHealth)
			ELSE
				IF iMaxHealthBeforebigfoot != 0
					SET_ENTITY_MAX_HEALTH(LocalPlayerPed, iMaxHealthBeforebigfoot)
					iMaxHealthBeforebigfoot = 0
				ENDIF
				SET_ENTITY_HEALTH(LocalPlayerPed, GET_ENTITY_MAX_HEALTH(LocalPlayerPed))
				SET_PED_SUFFERS_CRITICAL_HITS(LocalPlayerPed, TRUE)
				bFMMCHealthBoosted = FALSE
				PRINTLN("[JS] SWITCH_TO_JUGGERNAUT_FOR_SUDDEN_DEATH - SET_ENTITY_HEALTH = GET_ENTITY_MAX_HEALTH")
			ENDIF
		ENDIF
		SET_PLAYER_PROOFS_FOR_TEAM(iTeam)
	ELSE
		ASSERTLN("[JS] SWITCH_TO_JUGGERNAUT_FOR_SUDDEN_DEATH - Called in a non Juggernaut mission, don't do this")
	ENDIF
ENDPROC

FUNC BOOL IS_PARTICIPANT_A_JUGGERNAUT(INT iParticipant)
	RETURN 	IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iParticipant].iTeam].iTeamBitset2, ciBS2_JUGGERNAUTS)
			OR IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet3, PBBOOL3_SUDDEN_DEATH_JUGGERNAUT)
			OR IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet3, PBBOOL3_TRANSFORMED_INTO_JUGGERNAUT)
ENDFUNC

FUNC BOOL IS_PARTICIPANT_A_BEAST(INT iParticipant)
	RETURN 	IS_BIT_SET(MC_playerBD[iParticipant].iRagePickupBS, ciCUSTOM_PICKUP_TYPE__BEAST_MODE)
			OR IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet3, PBBOOL3_TRANSFORMED_INTO_BEAST)
ENDFUNC

PROC PROCESS_MY_PLAYER_BLIP_VISIBILITY(INT iTeam, INT iRule)
	
	IF iSpectatorTarget = -1
		
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeam1BlipVisibility, iRule)
		AND NOT IS_BIT_SET(iLocalBoolCheck14, LBOOL14_BLIP_ON_LOCATE)
		AND NOT IS_BIT_SET(iLocalBoolCheck16, LBOOL16_BLIP_ON_COLLECT)
		AND NOT IS_BIT_SET(iBlippingPlayerBS, iLocalPart)
		AND NOT (IS_BIT_SET( MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH )
			AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciBLIP_PLAYERS_AT_SUDDEN_DEATH))
		AND NOT IS_BIT_SET(iLocalBoolCheck23, LBOOL23_BLIP_FORCED_ON_BY_ZONE)
			PRINTLN("[RCC MISSION] PROCESS_MY_PLAYER_BLIP_VISIBILITY - Hiding my blip (team ",iTeam,") from team 0")
			HIDE_MY_PLAYER_BLIP_FROM_TEAM(TRUE,0)
		ELSE
			IF NOT IS_BIT_SET(iPowerPlayEffectBS, ciCUSTOM_PICKUP_TYPE__HIDE_BLIPS)
				PRINTLN("[RCC MISSION] PROCESS_MY_PLAYER_BLIP_VISIBILITY - Showing my blip (team ",iTeam,") to team 0")
				HIDE_MY_PLAYER_BLIP_FROM_TEAM(FALSE,0)
			ELSE
				PRINTLN("[RCC MISSION] PROCESS_MY_PLAYER_BLIP_VISIBILITY - Preventing showing my blip (team ",iTeam,") to team 0 - power play")
			ENDIF
		ENDIF
		
		IF MC_serverBD.iNumberOfTeams >= 2
					
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeam2BlipVisibility, iRule)
			AND NOT IS_BIT_SET(iLocalBoolCheck14, LBOOL14_BLIP_ON_LOCATE)
			AND NOT IS_BIT_SET(iLocalBoolCheck16, LBOOL16_BLIP_ON_COLLECT)
			AND NOT IS_BIT_SET(iBlippingPlayerBS, iLocalPart) 
			AND NOT (IS_BIT_SET( MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH )
				AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciBLIP_PLAYERS_AT_SUDDEN_DEATH))
			AND NOT IS_BIT_SET(iLocalBoolCheck23, LBOOL23_BLIP_FORCED_ON_BY_ZONE)
				PRINTLN("[RCC MISSION] PROCESS_MY_PLAYER_BLIP_VISIBILITY - Hiding my blip (team ",iTeam,") from team 1")
				HIDE_MY_PLAYER_BLIP_FROM_TEAM(TRUE,1)
			ELSE
				IF NOT IS_BIT_SET(iPowerPlayEffectBS, ciCUSTOM_PICKUP_TYPE__HIDE_BLIPS)
					PRINTLN("[RCC MISSION] PROCESS_MY_PLAYER_BLIP_VISIBILITY - Showing my blip (team ",iTeam,") to team 1")
					HIDE_MY_PLAYER_BLIP_FROM_TEAM(FALSE,1)
				ELSE
					PRINTLN("[RCC MISSION] PROCESS_MY_PLAYER_BLIP_VISIBILITY - Preventing showing my blip (team ",iTeam,") to team 1 - power play")
				ENDIF
			ENDIF
			
			IF MC_serverBD.iNumberOfTeams >= 3
				
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeam3BlipVisibility, iRule)
				AND NOT IS_BIT_SET(iLocalBoolCheck14, LBOOL14_BLIP_ON_LOCATE)
				AND NOT IS_BIT_SET(iLocalBoolCheck16, LBOOL16_BLIP_ON_COLLECT)
				AND NOT IS_BIT_SET(iBlippingPlayerBS, iLocalPart) 
				AND NOT (IS_BIT_SET( MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH )
					AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciBLIP_PLAYERS_AT_SUDDEN_DEATH))
				AND NOT IS_BIT_SET(iLocalBoolCheck23, LBOOL23_BLIP_FORCED_ON_BY_ZONE)
					PRINTLN("[RCC MISSION] PROCESS_MY_PLAYER_BLIP_VISIBILITY - Hiding my blip (team ",iTeam,") from team 2")
					HIDE_MY_PLAYER_BLIP_FROM_TEAM(TRUE,2)
				ELSE
					IF NOT IS_BIT_SET(iPowerPlayEffectBS, ciCUSTOM_PICKUP_TYPE__HIDE_BLIPS)
						PRINTLN("[RCC MISSION] PROCESS_MY_PLAYER_BLIP_VISIBILITY - Showing my blip (team ",iTeam,") to team 2")
						HIDE_MY_PLAYER_BLIP_FROM_TEAM(FALSE,2)
					ELSE
						PRINTLN("[RCC MISSION] PROCESS_MY_PLAYER_BLIP_VISIBILITY - Preventing showing my blip (team ",iTeam,") to team 2 - power play")
					ENDIF
				ENDIF
				
				IF MC_serverBD.iNumberOfTeams >= 4
					
					IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeam4BlipVisibility, iRule)
					AND NOT IS_BIT_SET(iLocalBoolCheck14, LBOOL14_BLIP_ON_LOCATE)
					AND NOT IS_BIT_SET(iLocalBoolCheck16, LBOOL16_BLIP_ON_COLLECT)
					AND NOT IS_BIT_SET(iBlippingPlayerBS, iLocalPart) 
					AND NOT (IS_BIT_SET( MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH )
						AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciBLIP_PLAYERS_AT_SUDDEN_DEATH))
					AND NOT IS_BIT_SET(iLocalBoolCheck23, LBOOL23_BLIP_FORCED_ON_BY_ZONE)
						PRINTLN("[RCC MISSION] PROCESS_MY_PLAYER_BLIP_VISIBILITY - Hiding my blip (team ",iTeam,") from team 3")
						HIDE_MY_PLAYER_BLIP_FROM_TEAM(TRUE,3)
					ELSE
						IF NOT IS_BIT_SET(iPowerPlayEffectBS, ciCUSTOM_PICKUP_TYPE__HIDE_BLIPS)
							PRINTLN("[RCC MISSION] PROCESS_MY_PLAYER_BLIP_VISIBILITY - Showing my blip (team ",iTeam,") to team 3")
							HIDE_MY_PLAYER_BLIP_FROM_TEAM(FALSE,3)
						ELSE
							PRINTLN("[RCC MISSION] PROCESS_MY_PLAYER_BLIP_VISIBILITY - Preventing showing my blip (team ",iTeam,") to team 3 - power play")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC SET_ALL_PLAYER_BLIPS_LONG_RANGE(BOOL bIsLongRange)
	IF NETWORK_IS_ACTIVITY_SESSION()
		INT i
		FOR i = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1			
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
				SET_PLAYER_BLIP_AS_LONG_RANGE(INT_TO_PLAYERINDEX(i),bIsLongRange)
			ENDIF
		ENDFOR
	ENDIF	
ENDPROC

FUNC MP_OUTFIT_ENUM GET_RANDOM_BEAST_OUTFIT(BOOL bUseTradingPlacesVariants = FALSE)
	IF !bUseTradingPlacesVariants
		SWITCH GET_RANDOM_INT_IN_RANGE(0,11)
			CASE 0 RETURN OUTFIT_ADVERSARY_B_OVERRIDE_0
			CASE 1 RETURN OUTFIT_ADVERSARY_B_OVERRIDE_1
			CASE 2 RETURN OUTFIT_ADVERSARY_B_OVERRIDE_2
			CASE 3 RETURN OUTFIT_ADVERSARY_B_OVERRIDE_3
			CASE 4 RETURN OUTFIT_ADVERSARY_B_OVERRIDE_4
			CASE 5 RETURN OUTFIT_ADVERSARY_B_OVERRIDE_5
			CASE 6 RETURN OUTFIT_ADVERSARY_B_OVERRIDE_6
			CASE 7 RETURN OUTFIT_ADVERSARY_B_OVERRIDE_7
			CASE 8 RETURN OUTFIT_ADVERSARY_B_OVERRIDE_8
			CASE 9 RETURN OUTFIT_ADVERSARY_B_OVERRIDE_9
			CASE 10 RETURN OUTFIT_ADVERSARY_B_OVERRIDE_10
			CASE 11 RETURN OUTFIT_ADVERSARY_B_OVERRIDE_11
		ENDSWITCH
	ELSE
		SWITCH GET_RANDOM_INT_IN_RANGE(0,7)
			CASE 0 RETURN OUTFIT_VERSUS_HIDDEN_TRADINGPLACES_BEAST_0
			CASE 1 RETURN OUTFIT_VERSUS_HIDDEN_TRADINGPLACES_BEAST_1
			CASE 2 RETURN OUTFIT_VERSUS_HIDDEN_TRADINGPLACES_BEAST_2
			CASE 3 RETURN OUTFIT_VERSUS_HIDDEN_TRADINGPLACES_BEAST_3
			CASE 4 RETURN OUTFIT_VERSUS_HIDDEN_TRADINGPLACES_BEAST_4
			CASE 5 RETURN OUTFIT_VERSUS_HIDDEN_TRADINGPLACES_BEAST_5
			CASE 6 RETURN OUTFIT_VERSUS_HIDDEN_TRADINGPLACES_BEAST_6
			CASE 7 RETURN OUTFIT_VERSUS_HIDDEN_TRADINGPLACES_BEAST_7
		ENDSWITCH
	ENDIF
	RETURN OUTFIT_ADVERSARY_B_OVERRIDE_0
ENDFUNC

FUNC MP_OUTFIT_ENUM GET_BEAST_OUTFIT_BASED_ON_PART_NUM(INT iPartNum)
	IF iPartNum > 7
		iPartNum = iPartNum - 7
	ENDIF
	PRINTLN("GET_BEAST_OUTFIT_BASED_ON_PART_NUM - iPartNum", iPartNum)
	SWITCH iPartNum
		CASE 0 RETURN OUTFIT_VERSUS_HIDDEN_TRADINGPLACES_BEAST_0
		CASE 1 RETURN OUTFIT_VERSUS_HIDDEN_TRADINGPLACES_BEAST_1
		CASE 2 RETURN OUTFIT_VERSUS_HIDDEN_TRADINGPLACES_BEAST_2
		CASE 3 RETURN OUTFIT_VERSUS_HIDDEN_TRADINGPLACES_BEAST_3
		CASE 4 RETURN OUTFIT_VERSUS_HIDDEN_TRADINGPLACES_BEAST_4
		CASE 5 RETURN OUTFIT_VERSUS_HIDDEN_TRADINGPLACES_BEAST_5
		CASE 6 RETURN OUTFIT_VERSUS_HIDDEN_TRADINGPLACES_BEAST_6
		CASE 7 RETURN OUTFIT_VERSUS_HIDDEN_TRADINGPLACES_BEAST_7
	ENDSWITCH
	RETURN OUTFIT_VERSUS_HIDDEN_TRADINGPLACES_BEAST_0
ENDFUNC

FUNC MP_OUTFIT_ENUM GET_RANDOM_JUGGERNAUT_OUTFIT()
	SWITCH GET_RANDOM_INT_IN_RANGE(0,7)
		CASE 0 RETURN OUTFIT_VERSUS_HIDDEN_TRADINGPLACES_JUG_0
		CASE 1 RETURN OUTFIT_VERSUS_HIDDEN_TRADINGPLACES_JUG_1
		CASE 2 RETURN OUTFIT_VERSUS_HIDDEN_TRADINGPLACES_JUG_2
		CASE 3 RETURN OUTFIT_VERSUS_HIDDEN_TRADINGPLACES_JUG_3
		CASE 4 RETURN OUTFIT_VERSUS_HIDDEN_TRADINGPLACES_JUG_4
		CASE 5 RETURN OUTFIT_VERSUS_HIDDEN_TRADINGPLACES_JUG_5
		CASE 6 RETURN OUTFIT_VERSUS_HIDDEN_TRADINGPLACES_JUG_6
		CASE 7 RETURN OUTFIT_VERSUS_HIDDEN_TRADINGPLACES_JUG_7
	ENDSWITCH
	RETURN OUTFIT_VERSUS_HIDDEN_TRADINGPLACES_JUG_0
ENDFUNC

FUNC MP_OUTFIT_ENUM GET_JUGGERNAUT_OUTFIT_BASED_ON_PART_NUM(INT iPartNum)
	IF iPartNum > 7
		iPartNum = iPartNum - 7
	ENDIF
	PRINTLN("GET_JUGGERNAUT_OUTFIT_BASED_ON_PART_NUM - iPartNum", iPartNum)
	SWITCH iPartNum
		CASE 0 RETURN OUTFIT_VERSUS_HIDDEN_TRADINGPLACES_JUG_0
		CASE 1 RETURN OUTFIT_VERSUS_HIDDEN_TRADINGPLACES_JUG_1
		CASE 2 RETURN OUTFIT_VERSUS_HIDDEN_TRADINGPLACES_JUG_2
		CASE 3 RETURN OUTFIT_VERSUS_HIDDEN_TRADINGPLACES_JUG_3
		CASE 4 RETURN OUTFIT_VERSUS_HIDDEN_TRADINGPLACES_JUG_4
		CASE 5 RETURN OUTFIT_VERSUS_HIDDEN_TRADINGPLACES_JUG_5
		CASE 6 RETURN OUTFIT_VERSUS_HIDDEN_TRADINGPLACES_JUG_6
		CASE 7 RETURN OUTFIT_VERSUS_HIDDEN_TRADINGPLACES_JUG_7
	ENDSWITCH
	RETURN OUTFIT_VERSUS_HIDDEN_TRADINGPLACES_JUG_0
ENDFUNC

PROC PROCESS_IMMEDIATE_TEAM_SWAP_LOGIC()
	IF SHOULD_USE_CUSTOM_SPAWN_POINTS()	
		PRINTLN("[JT][TeamSwaps] - PROCESS_IMMEDIATE_TEAM_SWAP_LOGIC - Switched teams need to update spawn points -  ADD_ALL_CUSTOM_SPAWN_POINTS")
		ADD_ALL_CUSTOM_SPAWN_POINTS()
	ELSE
		IF IS_BIT_SET(iLocalBoolCheck21, LBOOL21_USING_CUSTOM_RESPAWN_POINTS)
			PRINTLN("[JT][TeamSwaps] - PROCESS_IMMEDIATE_TEAM_SWAP_LOGIC - We are using Custom Respawn Points, but they are not valid. Setting should clear")
			SET_BIT(iLocalBoolCheck21, LBOOL21_SHOULD_CLEAR_CUSTOM_RESPAWN_POINTS)
		ENDIF
	ENDIF
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TRADING_PLACES_REMIX(g_FMMC_STRUCT.iAdversaryModeType)
		SET_BIT(iLocalBoolCheck29, LBOOL29_TEAM_SWAP_UPDATE_SPAWN_LOCATION)
		PRINTLN("[JT][TeamSwaps] - PROCESS_IMMEDIATE_TEAM_SWAP_LOGIC - Setting LBOOL29_TEAM_SWAP_UPDATE_SPAWN_LOCATION")
	ENDIF
ENDPROC

PROC APPLY_TRADING_PLACES_SWAP_SETTINGS(INT iNewTeam)
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iNewTeam].iTeamBitset2, ciBS2_KILL_PLAYER_IN_WATER)
		PRINTLN("[RCC MISSION] APPLY_TRADING_PLACES_SWAP_SETTINGS - Setting player dies instantly in water.")
		SET_PED_DIES_INSTANTLY_IN_WATER(LocalPlayerPed, TRUE)
	ELSE
		SET_PED_DIES_INSTANTLY_IN_WATER(LocalPlayerPed, FALSE)
	ENDIF
	
	IF iNewTeam = 1
		iPlayerThermalRefreshBitset = 0
		PRINTLN("[RCC MISSION] APPLY_TRADING_PLACES_SWAP_SETTINGS - CLOBBER_OLD_TEAM_DATA_FOR_PLAYER - Clearing iPlayerThermalRefreshBitset: ", iPlayerThermalRefreshBitset)
	ELSE
		GET_PED_WEAPONS_MP(LocalPlayerPed, pwsCachedPedWeapons)
		IF DOES_PLAYER_HAVE_WEAPON(WEAPONTYPE_DLC_PUMPSHOTGUN_MK2)
			GET_SPECIAL_AMMO_AMOUNT(WEAPONTYPE_DLC_PUMPSHOTGUN_MK2, iMK2ShotgunAmmo, aAmmoType)
		ENDIF
		RESET_PED_MOVEMENT_CLIPSET(LocalPlayerPed)
		RESET_PED_STRAFE_CLIPSET(LocalPlayerPed)
		SET_WEAPON_ANIMATION_OVERRIDE(LocalPlayerPed, HASH("Default"))
	ENDIF	
	REMOVE_ALL_PLAYERS_WEAPONS()
	
ENDPROC

PROC CLOBBER_OLD_TEAM_DATA_FOR_PLAYER(INT iNewTeam)
	Clear_Any_Objective_Text()
	
	IF iNewTeam = 1 //Winners
		SET_TEMP_PASSIVE_MODE(TRUE, TRUE)
		TURN_OFF_PASSIVE_MODE_OPTION(TRUE)
		REINIT_NET_TIMER(tdTeamSwapPassiveModeTimer)
		SET_ENTITY_ALPHA(LocalPlayerPed, 200, FALSE)
		PRINTLN("[MMacK][TeamSwaps] Set Passive")
	ENDIF
	
	MP_OUTFITS_APPLY_DATA   sApplyData
	sApplyData.pedID		= LocalPlayerPed
	sApplyData.eApplyStage 	= AOS_SET
	INT iOutfitToUse
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_JUGGERNAUT(g_FMMC_STRUCT.iAdversaryModeType)
	OR (IS_THIS_ROCKSTAR_MISSION_NEW_VS_POWER_MAD(g_FMMC_STRUCT.iAdversaryModeType) AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciENABLE_POWER_MAD_SOUNDS))
		IF iNewTeam = 1
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciENABLE_POWER_MAD_SOUNDS)
				STOP_AUDIO_SCENE("DLC_IE_JN_Player_Is_Attacker_Scene")
				START_AUDIO_SCENE("DLC_IE_JN_Player_Is_JN_Scene")
			ELSE
				STOP_AUDIO_SCENE("DLC_GR_PM_JN_Player_Is_Attacker_Scene")
				START_AUDIO_SCENE("DLC_GR_PM_JN_Player_Is_JN_Scene")
			ENDIF
			iOutfitToUse = ENUM_TO_INT(OUTFIT_HIDDEN_IE_JN_TARGET_0) 
		ELSE
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciENABLE_POWER_MAD_SOUNDS)
				STOP_AUDIO_SCENE("DLC_IE_JN_Player_Is_JN_Scene")
				START_AUDIO_SCENE("DLC_IE_JN_Player_Is_Attacker_Scene")
			ELSE
				STOP_AUDIO_SCENE("DLC_GR_PM_JN_Player_Is_JN_Scene")
				START_AUDIO_SCENE("DLC_GR_PM_JN_Player_Is_Attacker_Scene")
			ENDIF
			iOutfitToUse = ENUM_TO_INT(OUTFIT_HIDDEN_IE_JN_ATTACKER_0)
			INT iPartInTeam = GET_PARTICIPANT_NUMBER_IN_TEAM()
			IF iPartInTeam > 5 
				iPartInTeam = iPartInTeam/2
			ENDIF
			iOutfitToUse += iPartInTeam
		ENDIF
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TRADING_PLACES_REMIX(g_FMMC_STRUCT.iAdversaryModeType)
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iNewTeam].iTeamBitset2, ciBS2_JUGGERNAUTS)
			STOP_AUDIO_SCENE("DLC_BTL_TP_Remix_JN_Player_Is_Attacker_Scene")
			START_AUDIO_SCENE("DLC_BTL_TP_Remix_JN_Player_Is_JN_Scene")
			iOutfitToUse = ENUM_TO_INT(GET_JUGGERNAUT_OUTFIT_BASED_ON_PART_NUM(iLocalPart))
			PRINTLN("[MMacK][TeamSwaps] Setting outfit to use to juggernaut outfit ", iOutfitToUse)
		ELSE 
			STOP_AUDIO_SCENE("DLC_BTL_TP_Remix_JN_Player_Is_JN_Scene")
			START_AUDIO_SCENE("DLC_BTL_TP_Remix_JN_Player_Is_Attacker_Scene")
			iOutfitToUse = ENUM_TO_INT(GET_BEAST_OUTFIT_BASED_ON_PART_NUM(iLocalPart))
			PRINTLN("[MMacK][TeamSwaps] Setting outfit to use to beast outfit ", iOutfitToUse)
		ENDIF
		CLEAR_PED_BLOOD_DAMAGE(LocalPlayerPed)
	ELSE
		iOutfitToUse = ENUM_TO_INT(OUTFIT_VERSUS_EXEC1_TRADING_WINNER_0) 
		iOutfitToUse += GET_PARTICIPANT_NUMBER_IN_TEAM()
		IF iNewTeam = 0
			iOutfitToUse += 6 //number of outfits in winner block
		ENDIF
	ENDIF
	MP_OUTFIT_ENUM eOutfitToSwapTo = INT_TO_ENUM(MP_OUTFIT_ENUM, iOutfitToUse)
	
	sApplyData.eOutfit		= eOutfitToSwapTo
	SET_PED_MP_OUTFIT(sApplyData, FALSE, FALSE, FALSE, FALSE)
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_JUGGERNAUT(g_FMMC_STRUCT.iAdversaryModeType)
		REMOVE_ALL_PLAYERS_WEAPONS()
		IF GIVE_PLAYER_STARTING_MISSION_WEAPONS( WEAPONINHAND_LASTWEAPON_BOTH, TRUE, FALSE, TRUE)
			PRINTLN("[JS][JUGG] - CLOBBER_OLD_TEAM_DATA_FOR_PLAYER - Swapped weapons")
			WEAPON_TYPE wtCurrentWeap
			GET_CURRENT_PED_WEAPON(LocalPlayerPed, wtCurrentWeap)
			IF iNewTeam = 1
			AND DOES_PLAYER_HAVE_WEAPON(WEAPONTYPE_MINIGUN)
				PRINTLN("[JS][JUGG] - CLOBBER_OLD_TEAM_DATA_FOR_PLAYER - Minigun")
				IF wtCurrentWeap != WEAPONTYPE_MINIGUN
					SET_CURRENT_PED_WEAPON(LocalPlayerPed, WEAPONTYPE_MINIGUN, TRUE)
				ENDIF
			ELSE
				PRINTLN("[JS][JUGG] - CLOBBER_OLD_TEAM_DATA_FOR_PLAYER - Best weapon")
				IF wtCurrentWeap != GET_BEST_PED_WEAPON(LocalPlayerPed)
					SET_CURRENT_PED_WEAPON(LocalPlayerPed, GET_BEST_PED_WEAPON(LocalPlayerPed), TRUE)
				ENDIF
			ENDIF
			FORCE_PED_AI_AND_ANIMATION_UPDATE(LocalPlayerPed)
		ENDIF
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TRADING_PLACES_REMIX(g_FMMC_STRUCT.iAdversaryModeType)
		
		APPLY_TRADING_PLACES_SWAP_SETTINGS(iNewTeam)
		
		IF iNewTeam = 1
		AND MC_serverBD_4.iCurrentHighestPriority[iNewTeam] < FMMC_MAX_RULES
			
			SET_PED_WEAPONS_MP(LocalPlayerPed,pwsCachedPedWeapons,FALSE)
			IF GIVE_PLAYER_STARTING_MISSION_WEAPONS( WEAPONINHAND_LASTWEAPON_BOTH, TRUE, FALSE, TRUE, TRUE)
				PRINTLN("CLOBBER_OLD_TEAM_DATA_FOR_PLAYER - Swapped weapons for Juggernaut team")
				WEAPON_TYPE wtCurrentWeap
				GET_CURRENT_PED_WEAPON(LocalPlayerPed, wtCurrentWeap)
				IF iNewTeam = 1
				AND DOES_PLAYER_HAVE_WEAPON(WEAPONTYPE_MINIGUN)
					PRINTLN("CLOBBER_OLD_TEAM_DATA_FOR_PLAYER - Minigun")
					IF wtCurrentWeap != WEAPONTYPE_MINIGUN
						SET_CURRENT_PED_WEAPON(LocalPlayerPed, WEAPONTYPE_MINIGUN, TRUE)
					ENDIF
					SET_PED_WEAPON_TINT_INDEX(LocalPlayerPed, WEAPONTYPE_MINIGUN, 0)
				ELSE
					IF wtCurrentWeap != GET_BEST_PED_WEAPON(LocalPlayerPed)
						SET_CURRENT_PED_WEAPON(LocalPlayerPed, GET_BEST_PED_WEAPON(LocalPlayerPed), TRUE)
					ENDIF
				ENDIF
				IF DOES_PLAYER_HAVE_WEAPON(WEAPONTYPE_DLC_PUMPSHOTGUN_MK2)
					IF iMK2ShotgunAmmo > 0
						ADD_PED_AMMO_BY_TYPE(PLAYER_PED_ID(), aAmmoType, iMK2ShotgunAmmo)
					ENDIF
				ENDIF
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK1)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK2)
				
				CLEAR_PED_TASKS(LocalPlayerPed)
				IF IS_PED_CLIMBING(LocalPlayerPed)
					SET_PED_TO_RAGDOLL(LocalPlayerPed, 0, 1000, TASK_RELAX, FALSE, FALSE, FALSE)
				ENDIF
				
				FORCE_PED_AI_AND_ANIMATION_UPDATE(LocalPlayerPed)
			ENDIF
		ENDIF
	ELSE
		IF DOES_PLAYER_HAVE_WEAPON(wtCachedLastWeaponHeld)
			SET_CURRENT_PED_WEAPON(LocalPlayerPed, wtCachedLastWeaponHeld, TRUE)
		ELSE
			SET_CURRENT_PED_WEAPON(LocalPlayerPed, GET_BEST_PED_WEAPON(LocalPlayerPed), TRUE)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciCASH_EXPLOSION_ON_TEAM_SWAP)
	AND IS_ENTITY_VISIBLE(LocalPlayerPed)
		IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_JUGGERNAUT(g_FMMC_STRUCT.iAdversaryModeType)
		OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_TRADING_PLACES_REMIX(g_FMMC_STRUCT.iAdversaryModeType)
			IF iNewTeam = 1
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_ENABLE_TRADING_PLACES_SOUNDS)
					PLAY_SOUND_FROM_ENTITY(-1, "Transform_JN_VFX", LocalPlayerPed, "DLC_BTL_TP_Remix_Juggernaut_Player_Sounds", TRUE)
				ELSE
					PLAY_SOUND_FROM_ENTITY(-1, "Transform_JN_VFX", LocalPlayerPed, "DLC_IE_JN_Player_Sounds", TRUE)
				ENDIF
			ELIF iNewTeam = 0
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_ENABLE_TRADING_PLACES_SOUNDS)
					PLAY_SOUND_FROM_ENTITY(-1, "Transform_Attacker_VFX", LocalPlayerPed, "DLC_BTL_TP_Remix_Juggernaut_Player_Sounds", TRUE)
				ELSE
					PLAY_SOUND_FROM_ENTITY(-1, "Transform_Attacker_VFX", LocalPlayerPed, "DLC_IE_JN_Player_Sounds", TRUE)
				ENDIF
			ENDIF
			IF HAS_NAMED_PTFX_ASSET_LOADED("scr_impexp_jug")
				PRINTLN("[RCC MISSION] Playing JUGGERNAUT Transform_Local_Player")
				USE_PARTICLE_FX_ASSET("scr_impexp_jug")
				START_NETWORKED_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_impexp_jug_outfit_swap", LocalPlayerPed, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
			ENDIF
		ELSE
		
			IF iNewTeam = 1 //Winning Team
				IF HAS_NAMED_PTFX_ASSET_LOADED("scr_tplaces")
					BROADCAST_TRADING_PLACES_PLAY_TRANSFORM_SFX()
					PRINTLN("[RCC MISSION] Playing Transform_Local_Player")
					PLAY_SOUND_FRONTEND(-1, "Transform_Local_Player", "DLC_Exec_TP_SoundSet", FALSE)
					
					USE_PARTICLE_FX_ASSET("scr_tplaces")
					SET_PARTICLE_FX_NON_LOOPED_COLOUR(1.0, 0.94, 0.57)
					START_NETWORKED_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_tplaces_team_swap", LocalPlayerPed, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
				ENDIF
			ENDIF
			
			IF iNewTeam = 0	//Losing Team
				IF MC_serverBD_1.piTimeBarWinners[0] != INVALID_PLAYER_INDEX()	//Final Team Swap
					BROADCAST_TRADING_PLACES_PLAY_TRANSFORM_SFX()
					PRINTLN("[RCC MISSION] Playing Transform_Local_Player")
					PLAY_SOUND_FRONTEND(-1, "Transform_Loser_Local_Player", "DLC_Exec_TP_SoundSet", FALSE)
					
					USE_PARTICLE_FX_ASSET("scr_tplaces")
					START_NETWORKED_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_tplaceS_team_swap_nocash", LocalPlayerPed, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	SET_BIT(iLocalBoolCheck17, LBOOL17_TEAM_SWAP_PICKUP_PROCESS)
	
	IF NOT IS_BIT_SET( g_FMMC_STRUCT.iOptionsMenuBitSet, ciHIDE_VERSUS_BLIPS )
		SHOW_ALL_PLAYER_BLIPS(TRUE)
		SET_ALL_PLAYER_BLIPS_LONG_RANGE(TRUE)
		PRINTLN("[MMacK][TeamSwaps] SHOW_ALL_PLAYER_BLIPS = TRUE")
	ELSE
		SHOW_ALL_PLAYER_BLIPS(FALSE)
		PRINTLN("[MMacK][TeamSwaps] SHOW_ALL_PLAYER_BLIPS = FALSE")
	ENDIF
	
	iCustomPlayerBlipSetBS = 0
	
	IF g_FMMC_STRUCT.iTeamBlipType[iNewTeam] = TEAM_BLIP_RED_SKULL
		SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(LocalPlayer, RADAR_TRACE_BOUNTY_HIT, TRUE)
		PRINTLN("[MMacK][TeamSwaps] Set Blip To Skull for LocalPlayer")
	ELSE
		SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(LocalPlayer, RADAR_TRACE_BOUNTY_HIT, FALSE)
		PRINTLN("[MMacK][TeamSwaps] Set Blip To Default for LocalPlayer")
	ENDIF
			
	IF MC_serverBD_4.iCurrentHighestPriority[iNewTeam] < FMMC_MAX_RULES
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iNewTeam].iRuleBitsetEight[MC_serverBD_4.iCurrentHighestPriority[iNewTeam]], ciBS_RULE8_EXPLOSIONS_DONT_RAGDOLL)
		OR IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet3, PBBOOL3_SUDDEN_DEATH_JUGGERNAUT)
		OR IS_PARTICIPANT_A_JUGGERNAUT(iLocalPart)
			SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_DontActivateRagdollFromExplosions, TRUE)
		ELSE
			SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_DontActivateRagdollFromExplosions, FALSE)
		ENDIF
		
		PROCESS_MY_PLAYER_BLIP_VISIBILITY(iNewTeam, MC_serverBD_4.iCurrentHighestPriority[iNewTeam])
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iNewTeam].iRuleBitsetFour[MC_serverBD_4.iCurrentHighestPriority[iNewTeam]], ciBS_RULE4_BLIP_PLAYERS)
			IF NOT IS_BIT_SET(iLocalBoolCheck,LBOOL_SHOW_ALL_PLAYER_BLIPS)
				SHOW_ALL_PLAYER_BLIPS(TRUE)
				SET_ALL_PLAYER_BLIPS_LONG_RANGE(TRUE)
				PRINTLN("[RCC MISSION][TeamSwaps] CLOBBER_OLD_TEAM_DATA_FOR_PLAYER - Show all player blips")
				SET_BIT(iLocalBoolCheck,LBOOL_SHOW_ALL_PLAYER_BLIPS)
			ENDIF
		ELSE
			IF IS_BIT_SET(iLocalBoolCheck,LBOOL_SHOW_ALL_PLAYER_BLIPS)
				IF NOT IS_BIT_SET( MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH )
				AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciBLIP_PLAYERS_AT_SUDDEN_DEATH)
					SHOW_ALL_PLAYER_BLIPS(FALSE)
					PRINTLN("[RCC MISSION] PROCESS_PLAYER_BLIPS - Stop showing all player blips")
					
					CLEAR_BIT(iLocalBoolCheck,LBOOL_SHOW_ALL_PLAYER_BLIPS)
				ENDIF
			ENDIF
		ENDIF
		
		BLOCK_PLAYER_HEALTH_REGEN(IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iNewTeam].iRuleBitsetEight[MC_serverBD_4.iCurrentHighestPriority[iNewTeam]], ciBS_RULE8_BLOCK_HEALTH_REGEN))
		
		IF g_FMMC_STRUCT.sFMMCEndConditions[iNewTeam].iMaxHealth[MC_serverBD_4.iCurrentHighestPriority[iNewTeam]] != 0
			bFMMCHealthBoosted = FALSE
			INT iNewMaxHealth = g_FMMC_STRUCT.sFMMCEndConditions[iNewTeam].iMaxHealth[MC_serverBD_4.iCurrentHighestPriority[iNewTeam]]
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iNewTeam].iRuleBitsetEight[MC_serverBD_4.iCurrentHighestPriority[iNewTeam]], ciBS_RULE8_SCALE_MAX_PLAYER_HEALTH)
				INT i
				INT iEnemyPlayers
				FOR i = 0 TO FMMC_MAX_TEAMS - 1
					PRINTLN("[MMacK][TeamSwaps] Health is being scaled by players: ", MC_serverBD.iNumberOfPlayingPlayers[i])
					iEnemyPlayers = iEnemyPlayers + MC_serverBD.iNumberOfPlayingPlayers[i]
				ENDFOR
				iNewMaxHealth = (iNewMaxHealth * iEnemyPlayers)
			ENDIF
			INCREASE_PLAYER_HEALTH(iNewMaxHealth)
			PRINTLN("[MMacK][TeamSwaps] Player Health Modded to ", iNewMaxHealth)
		ELSE
			IF iMaxHealthBeforebigfoot != 0
				SET_ENTITY_MAX_HEALTH(LocalPlayerPed, iMaxHealthBeforebigfoot)
				iMaxHealthBeforebigfoot = 0
			ENDIF
			IF NOT IS_PED_INJURED(LocalPlayerPed)
				SET_ENTITY_HEALTH(LocalPlayerPed, GET_ENTITY_MAX_HEALTH(LocalPlayerPed))
			ENDIF
			SET_PED_SUFFERS_CRITICAL_HITS(LocalPlayerPed, TRUE)
			bFMMCHealthBoosted = FALSE
			PRINTLN("[MMacK][TeamSwaps] SET_ENTITY_HEALTH = GET_ENTITY_MAX_HEALTH")
		ENDIF
	ENDIF
	
	SET_PLAYER_PROOFS_FOR_TEAM(iNewTeam)
	
	PRINTLN("[MMacK][TeamSwaps] CLOBBER_OLD_TEAM_DATA_FOR_PLAYER - SUCCESS")
	CLEAR_BIT(iLocalBoolCheck16,LBOOL16_DELETED_PICKUPS_ON_TEAM_SWAP)
ENDPROC

FUNC BOOL APPLY_OUTFIT_TO_LOCAL_PLAYER(MP_OUTFIT_ENUM eOutfit)
	MP_OUTFITS_APPLY_DATA   sApplyData
	sApplyData.pedID		= LocalPlayerPed
	sApplyData.eApplyStage 	= AOS_SET
	sApplyData.eOutfit		= eOutfit
	RETURN SET_PED_MP_OUTFIT(sApplyData, FALSE, FALSE, FALSE, FALSE)
ENDFUNC

PROC RESET_PLAYER_HEALTH()
	IF iMaxHealthBeforebigfoot != 0
		IF IS_NET_PLAYER_OK(LocalPlayer)	
			PRINTLN("[JS] [BEASTMODE] Decreasing health from ", GET_PED_MAX_HEALTH(LocalPlayerPed)," to ", iMaxHealthBeforebigfoot)
			SET_PED_MAX_HEALTH(LocalPlayerPed, iMaxHealthBeforebigfoot)
			SET_ENTITY_HEALTH(LocalPlayerPed, iMaxHealthBeforebigfoot)
			SET_PED_SUFFERS_CRITICAL_HITS(LocalPlayerPed, TRUE)
			SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_IgnoreMeleeFistWeaponDamageMult, FALSE)
			SET_PLAYER_MAX_EXPLOSIVE_DAMAGE(LocalPlayer, -1)
			iMaxHealthBeforebigfoot = 0
			bFMMCHealthBoosted = FALSE
		ENDIF
	ENDIF
ENDPROC

FUNC FLOAT GET_BEAST_PARTICLE_COLOUR(INT iRGBA, BOOL bForceColour = FALSE)
	INT iR, iG, iB, iA
	HUD_COLOURS tempColour
	SWITCH(g_FMMC_STRUCT.iRATCBeastColour)
		CASE 0
			GET_RGB_FROM_INT(GET_INT_FROM_HUD_COLOUR(HUD_COLOUR_WHITE), iR, iG, iB, iA)
		BREAK
		
		CASE 1
			tempColour = GET_HUD_COLOUR_FOR_FMMC_TEAM(MC_playerBD[iLocalPart].iteam, LocalPlayer)
			GET_RGB_FROM_INT(GET_INT_FROM_HUD_COLOUR(tempColour), iR, iG, iB, iA)
		BREAK
			
		CASE 2
			GET_RGB_FROM_INT(GET_INT_FROM_HUD_COLOUR(HUD_COLOUR_BLACK), iR, iG, iB, iA)
		BREAK
		
		CASE 3
			GET_RGB_FROM_INT(GET_INT_FROM_HUD_COLOUR(HUD_COLOUR_RED), iR, iG, iB, iA)
		BREAK
	ENDSWITCH
	
	IF bForceColour
		tempColour = GET_HUD_COLOUR_FOR_FMMC_TEAM(MC_playerBD[iLocalPart].iteam, LocalPlayer)
		GET_RGB_FROM_INT(GET_INT_FROM_HUD_COLOUR(tempColour), iR, iG, iB, iA)
	ENDIF
	
	SWITCH(iRGBA)
		CASE 0
			RETURN TO_FLOAT(iR) / 255
		CASE 1
			RETURN TO_FLOAT(iG) / 255
		CASE 2
			RETURN TO_FLOAT(iB) / 255
		CASE 3
			RETURN TO_FLOAT(iA) / 255
		DEFAULT
			RETURN 0.0
	ENDSWITCH
ENDFUNC

PROC PLAY_TEAM_SWAP_TRANSFORM_EFFECTS()
	SET_PARTICLE_FX_NON_LOOPED_COLOUR(GET_BEAST_PARTICLE_COLOUR(0), GET_BEAST_PARTICLE_COLOUR(1), GET_BEAST_PARTICLE_COLOUR(2))
	USE_PARTICLE_FX_ASSET("scr_powerplay")
	START_NETWORKED_PARTICLE_FX_NON_LOOPED_ON_PED_BONE("scr_powerplay_beast_appear", localPlayerPed, <<0, 0, -0.3>>, <<0, 0, 0>>, BONETAG_PELVIS, 0.75)
ENDPROC

PROC INITIALISE_NEW_TEAM_FOR_PLAYER(INT iNewTeam)
	
	Clear_Any_Objective_Text()
	
	PLAY_TEAM_SWAP_TRANSFORM_EFFECTS()
	APPLY_OUTFIT_TO_LOCAL_PLAYER(INT_TO_ENUM(MP_OUTFIT_ENUM, GET_LOCAL_DEFAULT_OUTFIT(iNewTeam)))
	
	REMOVE_ALL_PLAYERS_WEAPONS()
	IF GIVE_PLAYER_STARTING_MISSION_WEAPONS( WEAPONINHAND_LASTWEAPON_BOTH, TRUE, FALSE, TRUE, DEFAULT, IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciWEAPONS_FORCE_NO_ADDONS_START_MISSION_INVENTORY))
		PRINTLN("[JS][LOCSWAP] - INITIALISE_NEW_TEAM_FOR_PLAYER - Swapped weapons")
		WEAPON_TYPE wtCurrentWeap
		GET_CURRENT_PED_WEAPON(LocalPlayerPed, wtCurrentWeap)
		IF wtCurrentWeap != GET_BEST_PED_WEAPON(LocalPlayerPed)
			SET_CURRENT_PED_WEAPON(LocalPlayerPed, GET_BEST_PED_WEAPON(LocalPlayerPed), TRUE)
		ENDIF
		FORCE_PED_AI_AND_ANIMATION_UPDATE(LocalPlayerPed)
	ENDIF
	
	//VFX/Sounds go here
	
	SET_BIT(iLocalBoolCheck17, LBOOL17_TEAM_SWAP_PICKUP_PROCESS)
	
	iCustomPlayerBlipSetBS = 0
	
	IF SHOULD_USE_CUSTOM_SPAWN_POINTS()	
		PRINTLN("[JS][LOCSWAP] - INITIALISE_NEW_TEAM_FOR_PLAYER -  ADD_ALL_CUSTOM_SPAWN_POINTS")
		ADD_ALL_CUSTOM_SPAWN_POINTS()
	ELSE
		IF IS_BIT_SET(iLocalBoolCheck21, LBOOL21_USING_CUSTOM_RESPAWN_POINTS)
			PRINTLN("[JS][LOCSWAP] - INITIALISE_NEW_TEAM_FOR_PLAYER -  We are using Custom Respawn Points, but they are not valid. Setting should clear")
			SET_BIT(iLocalBoolCheck21, LBOOL21_SHOULD_CLEAR_CUSTOM_RESPAWN_POINTS)
		ENDIF
	ENDIF
		
	RESET_PLAYER_HEALTH()	
	IF MC_serverBD_4.iCurrentHighestPriority[iNewTeam] < FMMC_MAX_RULES
		IF g_FMMC_STRUCT.sFMMCEndConditions[iNewTeam].iMaxHealth[MC_serverBD_4.iCurrentHighestPriority[iNewTeam]] != 0
			bFMMCHealthBoosted = FALSE
			INT iNewMaxHealth = g_FMMC_STRUCT.sFMMCEndConditions[iNewTeam].iMaxHealth[MC_serverBD_4.iCurrentHighestPriority[iNewTeam]]
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iNewTeam].iRuleBitsetEight[MC_serverBD_4.iCurrentHighestPriority[iNewTeam]], ciBS_RULE8_SCALE_MAX_PLAYER_HEALTH)
				INT i
				INT iEnemyPlayers
				FOR i = 0 TO FMMC_MAX_TEAMS - 1
					PRINTLN("[JS][LOCSWAP] - INITIALISE_NEW_TEAM_FOR_PLAYER -  Health is being scaled by players: ", MC_serverBD.iNumberOfPlayingPlayers[i])
					iEnemyPlayers = iEnemyPlayers + MC_serverBD.iNumberOfPlayingPlayers[i]
				ENDFOR
				iNewMaxHealth = (iNewMaxHealth * iEnemyPlayers)
			ENDIF
			INCREASE_PLAYER_HEALTH(iNewMaxHealth)
			PRINTLN("[JS][LOCSWAP] - INITIALISE_NEW_TEAM_FOR_PLAYER - Player health modded to ", iNewMaxHealth)
		ELSE
			PRINTLN("[JS][LOCSWAP] - INITIALISE_NEW_TEAM_FOR_PLAYER - Player health reset")
			SET_ENTITY_HEALTH(LocalPlayerPed, GET_PED_MAX_HEALTH(LocalPlayerPed))
		ENDIF
	ENDIF
	
	SET_PLAYER_PROOFS_FOR_TEAM(iNewTeam)
	
	PRINTLN("[JS][LOCSWAP] - INITIALISE_NEW_TEAM_FOR_PLAYER - SUCCESS")
	CLEAR_BIT(iLocalBoolCheck16,LBOOL16_DELETED_PICKUPS_ON_TEAM_SWAP)
	CLEAR_BIT(iLocalBoolCheck23, LBOOL23_LOCATE_TEAM_SWAP_INITIALISED)
ENDPROC

PROC SET_TEAM_TARGETS_ON_SWAP(INT iNewTeam)
	SET_PED_RELATIONSHIP_GROUP_HASH(LocalPlayerPed, rgfm_PlayerTeam[iNewTeam])
	
	NETWORK_SET_FRIENDLY_FIRE_OPTION(FALSE)
		
	IF NETWORK_IS_GAME_IN_PROGRESS()
		INT iTeamLoop
		
		FOR iTeamLoop = 0 TO (MC_serverBD.iNumberOfTeams - 1)
			IF DOES_TEAM_LIKE_TEAM(iNewTeam, iTeamLoop)
				SET_PED_CAN_BE_TARGETTED_BY_TEAM(LocalPlayerPed, iTeamLoop, FALSE)
				PRINTLN("[RCC MISSION] CHANGE_PLAYER_TEAM - SET_PED_CAN_BE_TARGETTED_BY_TEAM called with FALSE for team ",iTeamLoop)
		   		NETWORK_OVERRIDE_TEAM_RESTRICTIONS(iTeamLoop, TRUE)
			ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSet, ciUseAllTeamChat)
				SET_PED_CAN_BE_TARGETTED_BY_TEAM(LocalPlayerPed, iTeamLoop, TRUE)
				NETWORK_OVERRIDE_TEAM_RESTRICTIONS(iTeamLoop, TRUE)
				PRINTLN("[RCC MISSION] CHANGE_PLAYER_TEAM - ciUseAllTeamChat called with TRUE for team ",iTeamLoop)
		    ELSE
				SET_PED_CAN_BE_TARGETTED_BY_TEAM(LocalPlayerPed, iTeamLoop, TRUE)
				NETWORK_OVERRIDE_TEAM_RESTRICTIONS(iTeamLoop, FALSE)
				PRINTLN("[RCC MISSION] CHANGE_PLAYER_TEAM - SET_PED_CAN_BE_TARGETTED_BY_TEAM called with TRUE for team ",iTeamLoop)
		    ENDIF
		ENDFOR
	ENDIF
ENDPROC

PROC SET_PLAYER_TEAM_SWAP_DATA(INT iNewTeam)
	IF NOT IS_PED_DEAD_OR_DYING(LocalPlayerPed)
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciCLOBBER_PLAYER_ON_TEAM_SWAP)
			CLOBBER_OLD_TEAM_DATA_FOR_PLAYER(MC_playerBD[iLocalPart].iteam)
			SET_TEAM_TARGETS_ON_SWAP(iNewTeam)
			CLEAR_BIT(iLocalBoolCheck16,LBOOL16_CHANGE_PLAYER_TEAM_ALIVE_VARS_SETTER)
			PRINTLN("[MMacK][TeamSwaps] CHANGE_PLAYER_TEAM - Completed with CLOBBERING TIME!")
		ELIF IS_BIT_SET(iLocalBoolCheck23, LBOOL23_LOCATE_TEAM_SWAP_INITIALISED)
			INITIALISE_NEW_TEAM_FOR_PLAYER(iNewTeam)
			SET_TEAM_TARGETS_ON_SWAP(iNewTeam)
			CLEAR_BIT(iLocalBoolCheck16,LBOOL16_CHANGE_PLAYER_TEAM_ALIVE_VARS_SETTER)
			PRINTLN("[JS][LOCSWAP] CHANGE_PLAYER_TEAM - Completed!")
		ELSE
			SET_TEAM_TARGETS_ON_SWAP(iNewTeam)
			CLEAR_BIT(iLocalBoolCheck16,LBOOL16_CHANGE_PLAYER_TEAM_ALIVE_VARS_SETTER)
			PRINTLN("[MMacK][TeamSwaps] CHANGE_PLAYER_TEAM - Completed!")
		ENDIF	
	#IF IS_DEBUG_BUILD
	ELSE
		PRINTLN("[RCC MISSION] CHANGE_PLAYER_TEAM - Unable to call SET_PED_RELATIONSHIP_GROUP_HASH as player ped is injured!")
	#ENDIF
	ENDIF
ENDPROC

PROC CHANGE_PLAYER_TEAM(INT iNewTeam, BOOL bMidMission)
	
	PRINTLN("[RCC MISSION] CHANGE_PLAYER_TEAM - Changing to team ",iNewTeam," for my participant (",iLocalPart,") - bMidMission = ",bMidMission,", old team = ",MC_playerBD[iLocalPart].iteam)
		
	MC_playerBD[iLocalPart].iteam = iNewTeam
	g_i_Mission_team = iNewTeam
	
	IF bMidMission
		SET_PLAYER_TEAM(LocalPlayer, iNewTeam)
		GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].sClientCoronaData.iTeamChosen = iNewTeam //so respawn points work
		IF DECOR_EXIST_ON(GET_PLAYER_PED_SCRIPT_INDEX(LocalPlayer), "TeamId")
			DECOR_REMOVE(GET_PLAYER_PED_SCRIPT_INDEX(LocalPlayer), "TeamId")
			PRINTLN("[RCC MISSION] CHANGE_PLAYER_TEAM - DECOR_REMOVE(GET_PLAYER_PED_SCRIPT_INDEX(LocalPlayer), TeamId)")
		#IF IS_DEBUG_BUILD
		ELSE
			PRINTLN("[RCC MISSION] CHANGE_PLAYER_TEAM - We're changing team mid-mission, but the TeamId decorator doesn't exist on my player")
		#ENDIF
		ENDIF
	ENDIF
	
	IF bIsLocalPlayerHost
		MC_serverBD.iHostRefreshDpadValue++
	ENDIF
	
	MC_PlayerBD[iLocalPart].bSwapTeamStarted = TRUE
	MC_PlayerBD[iLocalPart].bSwapTeamFinished = TRUE
		
	SET_LOCAL_PLAYERS_FM_TEAM_DECORATOR(iNewTeam)
			
	IF bMidMission
		SET_BIT(iLocalBoolCheck16,LBOOL16_CHANGE_PLAYER_TEAM_ALIVE_VARS_SETTER)
		
		PRINTLN("[MMacK][TeamSwaps] CHANGE_PLAYER_TEAM - it's CLOBBERING TIME!")
	ENDIF
ENDPROC

FUNC BOOL GET_ANY_TEAM_GAPS()
	BOOL bLastTeamGap = FALSE
	INT iTeamLoop
	FOR iTeamLoop = 0 TO FMMC_MAX_TEAMS - 2 //Don't care about the last team
		INT iPlayerLoop
		FOR iPlayerLoop = 0 TO MAX_NUM_MC_PLAYERS-1
			PLAYER_INDEX piTempPlayer = INT_TO_NATIVE(PLAYER_INDEX, iPlayerLoop)
			IF NETWORK_IS_PLAYER_ACTIVE(piTempPlayer)
				IF NETWORK_IS_PLAYER_A_PARTICIPANT(piTempPlayer)
					IF NETWORK_IS_PARTICIPANT_ACTIVE(NETWORK_GET_PARTICIPANT_INDEX(piTempPlayer))
						IF GlobalplayerBD_FM[iPlayerLoop].sClientCoronaData.iTeamChosen = iTeamLoop
							PRINTLN("[JS] GET_ANY_TEAM_GAPS - Player ", iPlayerLoop," is on team ", iTeamLoop)  
							IF bLastTeamGap
								RETURN TRUE
							ENDIF
							bLastTeamGap = FALSE
							BREAKLOOP
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			IF iPlayerLoop = GET_MAXIMUM_PLAYERS_ON_MISSION()-1
				PRINTLN("[JS] GET_ANY_TEAM_GAPS - Team ", iTeamLoop," is a gap")  
				bLastTeamGap = TRUE
			ENDIF
		ENDFOR
	ENDFOR

	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_LOCAL_PLAYER_BE_KICKED_FROM_MODE()
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_BE_MY_VALENTINE(g_FMMC_STRUCT.iRootContentIDHash)
		INT iTeam = GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].sClientCoronaData.iTeamChosen   
		INT iTeamCount
		INT iPlayerLoop
		FOR iPlayerLoop = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
			PLAYER_INDEX piTempPlayer = INT_TO_NATIVE(PLAYER_INDEX, iPlayerLoop)
			IF NETWORK_IS_PLAYER_ACTIVE(piTempPlayer)
				IF NETWORK_IS_PLAYER_A_PARTICIPANT(piTempPlayer)
					IF NOT IS_PLAYER_SCTV(piTempPlayer)
						IF NETWORK_IS_PARTICIPANT_ACTIVE(NETWORK_GET_PARTICIPANT_INDEX(piTempPlayer))
							IF GlobalplayerBD_FM[iPlayerLoop].sClientCoronaData.iTeamChosen = iTeam
								iTeamCount ++
								IF piTempPlayer = LocalPlayer
									IF iTeamCount > g_FMMC_STRUCT.iMaxNumPlayersPerTeam[iTeam]
										PRINTLN("[JS] SHOULD_LOCAL_PLAYER_BE_KICKED_FROM_MODE - Kicking player to spectator, part ", iTeamCount, " max - ", g_FMMC_STRUCT.iMaxNumPlayersPerTeam[iTeam])  
										RETURN TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	RETURN FALSE
ENDFUNC

PROC SET_LOCAL_PLAYER_FAILED(INT iCall)

	PRINTLN("[LM] - SET_LOCAL_PLAYER_FAILED - Setting TRUE - From iCall: ", iCall)
	
	SET_BIT(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_PLAYER_FAIL)
	
ENDPROC

PROC ASSIGN_PLAYER_TEAM()

	g_bFM_ON_TEAM_MISSION = TRUE
	g_iOverheadNamesState = TAGS_WHEN_INTEAM
	
	INT iTeam
	
	IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
		iTeam = GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].sClientCoronaData.iTeamChosen     
		PRINTLN("[RCC MISSION] IS_FAKE_MULTIPLAYER_MODE_SET() = FALSE, setting MC_playerBD[iLocalPart].iteam = GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].sClientCoronaData.iTeamChosen : ",GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].sClientCoronaData.iTeamChosen) 
		IF bIsLocalPlayerHost
			IF GET_ANY_TEAM_GAPS()
				INT iTeamLoop
				FOR iTeamLoop = 0 TO FMMC_MAX_TEAMS - 1
					IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet,SSBOOL_TEAM0_FAILED + iTeamLoop)
						SET_BIT(MC_serverBD.iServerBitSet,SSBOOL_TEAM0_FINISHED + iTeamLoop)	
						SET_BIT(MC_serverBD.iServerBitSet,SSBOOL_TEAM0_FAILED + iTeamLoop)
						MC_serverBD.eCurrentTeamFail[iTeamLoop] = mFail_GAPS_IN_TEAMS
						PRINTLN("[RCC MISSION] FAILING all teams and setting them to FINISHED - THis is because of gaps in the teams.") 
					ENDIF
				ENDFOR
			ENDIF
		ENDIF
		IF SHOULD_LOCAL_PLAYER_BE_KICKED_FROM_MODE()
			MC_playerBD[iLocalPart].iReasonForPlayerFail = PLAYER_FAIL_OUT_OF_LIVES
			SET_LOCAL_PLAYER_FAILED(9)
		ENDIF
	ELSE
		iTeam = g_FMMC_STRUCT.iTestMyTeam
		PRINTLN("[RCC MISSION] IS_FAKE_MULTIPLAYER_MODE_SET() = TRUE, setting MC_playerBD[iLocalPart].iteam = g_FMMC_STRUCT.iTestMyTeam")
	ENDIF
	
	PRINTLN("[RCC MISSION] PLAYER TEAM: ",MC_playerBD[iLocalPart].iteam)
	
	IF iTeam < 0
		SCRIPT_ASSERT("TEAM IS COMING THROUGH AS LESS THAN 0, ADD A BUG FOR ROBERT WRIGHT")
		iTeam = 0
	ENDIF
	
	IF iTeam >=FMMC_MAX_TEAMS
		SCRIPT_ASSERT("TEAM IS COMING THROUGH MORE THAN FMMC_MAX_TEAMS, ADD A BUG FOR ROBERT WRIGHT")
		iTeam = 3
	ENDIF
	
	CHANGE_PLAYER_TEAM(iTeam, FALSE)
	
	SET_BIT(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_ASSIGNED_PLAYER_TEAM)
	
ENDPROC

PROC PROCESS_PLAYER_TEAM_SWITCH(INT iVictimPart, PLAYER_INDEX piKiller, BOOL bSuicide = FALSE, BOOL bNoVictim = FALSE)
	PRINTLN("[RCC MISSION][MMacK][ServerSwitch] PROCESS_PLAYER_TEAM_SWITCH INVOKED")
	IF IS_BIT_SET(iLocalBoolCheck20, LBOOL20_JUGGERNAUT_LEAVING_SWAP_DONE)
		CLEAR_BIT(iLocalBoolCheck20, LBOOL20_JUGGERNAUT_LEAVING_SWAP_DONE)
	ENDIF
	
	IF iVictimPart != -1 //save the bother if we have no victim
	OR bNoVictim
		INT iKillerPart = -1
		BOOL bValidLocks
		IF bIsLocalPlayerHost
			IF NETWORK_IS_PLAYER_A_PARTICIPANT(piKiller)
				PARTICIPANT_INDEX piKillerPart = NETWORK_GET_PARTICIPANT_INDEX(piKiller)
				IF NETWORK_IS_PARTICIPANT_ACTIVE(piKillerPart)
					iKillerPart = NATIVE_TO_INT(piKillerPart)
					
					PRINTLN("[RCC MISSION][MMacK][ServerSwitch] iKillerPart = ", iKillerPart)
					PRINTLN("[RCC MISSION][MMacK][ServerSwitch] iVictimPart = ", iVictimPart)
					
					IF iKillerPart != -1 
					AND iKillerPart != iVictimPart 
						IF IS_BIT_SET(MC_serverBD_4.iTeamSwapLockFlag, iKillerPart)
							PRINTLN("[RCC MISSION][MMacK][ServerSwitch] Killer Swap Locked")
						ELIF NOT bNoVictim
						AND IS_BIT_SET(MC_serverBD_4.iTeamSwapLockFlag, iVictimPart)
							PRINTLN("[RCC MISSION][MMacK][ServerSwitch] Victim Swap Locked")
						ELSE
							PRINTLN("[RCC MISSION][MMacK][ServerSwitch] Locks Validated")
							SET_BIT(MC_serverBD_4.iTeamSwapLockFlag, iKillerPart)
							IF NOT bNoVictim
								SET_BIT(MC_serverBD_4.iTeamSwapLockFlag, iVictimPart)
							ENDIF
							bValidLocks = TRUE
						ENDIF
					ELSE
						PRINTLN("[RCC MISSION][MMacK][ServerSwitch] iKillerPart is not determined to be valid")
					ENDIF
				ELSE
					PRINTLN("[RCC MISSION][MMacK][ServerSwitch] Killer is no longer active, purge this request!")
				ENDIF
			ELSE
				PRINTLN("[RCC MISSION][MMacK][ServerSwitch] Killer is no longer participating, purge this request!")
			ENDIF
			
			IF bValidLocks
				PRINTLN("[RCC MISSION][MMacK][ServerSwitch] OK to switch this player, they haven't moved recently")
				IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iKillerPart].iTeam] < FMMC_MAX_RULES
					IF NOT bNoVictim
						IF NOT (MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iVictimPart].iTeam] < FMMC_MAX_RULES)
							PRINTLN("[RCC MISSION][MMacK][ServerSwitch] Invalid Rule")
							EXIT
						ENDIF
					ENDIF
					
					INT iKillerTeam = g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iKillerPart].iTeam].iSwapToTeamOnKill[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iKillerPart].iTeam]]
						
					INT iVictimTeam
					IF NOT bNoVictim
						iVictimTeam = g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iVictimPart].iTeam].iSwapToTeamOnDeath[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iVictimPart].iTeam]]
					ENDIF
					
					IF iKillerTeam != -1 AND iKillerTeam < FMMC_MAX_TEAMS
						IF NOT bNoVictim
							IF NOT (iVictimTeam != -1 AND iVictimTeam < FMMC_MAX_TEAMS)
								PRINTLN("[RCC MISSION][MMacK][ServerSwitch] Invalid Team For Switching")
								EXIT
							ENDIF
						ENDIF
						BROADCAST_SERVER_AUTHORISED_TEAM_SWAP(iKillerPart, iKillerTeam, iVictimPart, iVictimTeam, bSuicide, MC_ServerBD.iSessionScriptEventKey)
						IF bNoVictim
							SET_BIT(iLocalBoolCheck20, LBOOL20_JUGGERNAUT_LEAVING_SWAP_DONE)
						ENDIF
					ELSE
						PRINTLN("[RCC MISSION][MMacK][ServerSwitch] Invalid Team For Switching")
					ENDIF
				ELSE
					PRINTLN("[RCC MISSION][MMacK][ServerSwitch] Invalid Rule")
				ENDIF
			ELSE
				PRINTLN("[RCC MISSION][MMacK][ServerSwitch] Server determined the request to be invalid, reason above")
			ENDIF
		ELSE
			PRINTLN("[RCC MISSION][MMacK][ServerSwitch] I'm not the server, so really should never be seeing this")	
		ENDIF
	ENDIF
ENDPROC

FUNC PLAYER_INDEX GET_PLAYER_FROM_LOSING_TEAM_FOR_SWAP()
	INT iPart, iPartForSwap, iTopKills
	
	iPartForSwap = iLocalPart //defaults to the local player
	
	IF iPartLastDamager != -1
	AND MC_playerBD[iPartLastDamager].iTeam = 0
	AND NOT MC_PlayerBD[iPartLastDamager].bSwapTeamStarted
	AND NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPartLastDamager))
		PRINTLN("[RCC MISSION][MMacK][ServerSwitch] CACHED DAMAGER - GET_PLAYER_FROM_LOSING_TEAM_FOR_SWAP = ", iPartLastDamager)
		RETURN NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPartLastDamager))
	ENDIF
	
	FOR iPart = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		IF (MC_playerBD[iPart].iTeam = 0) //losing team
		AND NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPart))
		AND NOT MC_PlayerBD[iPart].bSwapTeamStarted
		AND iPart != iLocalPart
			PLAYER_INDEX tempPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPart))
			IF IS_NET_PLAYER_OK(tempPlayer)
				IF MC_playerBD[iPart].iNumPlayerKills > iTopKills
					PRINTLN("[RCC MISSION][MMacK][ServerSwitch] TOP KILLER = GET_PLAYER_FROM_LOSING_TEAM_FOR_SWAP = ", iPartForSwap)
					iPartForSwap = iPart
					iTopKills = MC_playerBD[iPart].iNumPlayerKills 
				ENDIF
			ENDIF
		ENDIF
	ENDFOR	
	
	IF iTopKills = 0
		PRINTLN("[RCC MISSION][MMacK][ServerSwitch] NO CANDIDATE")
		//lets see if our part counterpart is good to swap?
		INT iMyTeamPart = 0
		INT iOtherTeamParts = 0
		FOR iPart = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
			IF (MC_playerBD[iPart].iTeam = 1)
				IF iPart != iLocalPart
					iMyTeamPart++
				ELSE
					FOR iOtherTeamParts = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
						IF (MC_playerBD[iOtherTeamParts].iTeam = 0)
						AND NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iOtherTeamParts))
							IF iMyTeamPart = 0
								IF NOT MC_PlayerBD[iOtherTeamParts].bSwapTeamStarted
								AND NOT MC_PlayerBD[iOtherTeamParts].bSwapTeamFinished
								AND NOT IS_BIT_SET(MC_serverBD_4.iTeamSwapLockFlag, iOtherTeamParts)
								AND NOT IS_BIT_SET(MC_playerBD[iOtherTeamParts].iClientBitSet, PBBOOL_ANY_SPECTATOR)
									PRINTLN("[RCC MISSION][MMacK][ServerSwitch] COUNTERPART = GET_PLAYER_FROM_LOSING_TEAM_FOR_SWAP = ", iOtherTeamParts)
									RETURN NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iOtherTeamParts))
								ELSE
									BREAKLOOP
								ENDIF	
							ELSE
								iMyTeamPart--
							ENDIF
						ENDIF
					ENDFOR
				ENDIF
			ENDIF
		ENDFOR
		
		FOR iPart = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
			IF (MC_playerBD[iPart].iTeam = 0) //losing team
			AND NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPart))
			AND NOT MC_PlayerBD[iPart].bSwapTeamStarted
			AND iPart != iLocalPart
			AND NOT IS_BIT_SET(MC_serverBD_4.iTeamSwapLockFlag, iPart)
			AND NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_ANY_SPECTATOR)		
				PRINTLN("[RCC MISSION][MMacK][ServerSwitch] FIRST AVAILABLE = GET_PLAYER_FROM_LOSING_TEAM_FOR_SWAP = ", iPart)
				RETURN NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPart))	
			ENDIF
		ENDFOR
	ENDIF
	
	PRINTLN("[RCC MISSION][MMacK][ServerSwitch] BAILED")

	RETURN NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPartForSwap))
ENDFUNC

PROC INCREMENT_SERVER_TEAM_SCORE(INT iTeam, INT iPriority, INT iScore)
	
	DEBUG_PRINTCALLSTACK()
	
	IF iTeam < FMMC_MAX_TEAMS
		
		MC_serverBD.iTeamScore[iTeam] += iScore
		PRINTLN("[RCC MISSION] INCREMENT_SERVER_TEAM_SCORE team: ",iteam," priority ",iPriority,", increased by ",iScore,",  total score ", MC_serverBD.iTeamScore[iTeam])

		
		IF iPriority < FMMC_MAX_RULES
		AND iPriority = MC_serverBD_4.iCurrentHighestPriority[iTeam]
			MC_serverBD.iScoreOnThisRule[iTeam] += iScore
			PRINTLN("[RCC MISSION] INCREMENT_SERVER_TEAM_SCORE team: ",iteam," priority ",iPriority,", increased by ",iScore,",  total score on this rule ", MC_serverBD.iScoreOnThisRule[iTeam])
		ENDIF
	ENDIF

ENDPROC

PROC SET_RENDERPHASE_PAUSE_WITH_UNPAUSE_DELAY(INT iDelayPause, INT iDelayUnpause #IF IS_DEBUG_BUILD , INT iDebugPauseIndex #ENDIF )
	DEBUG_PRINTCALLSTACK()
	IF NOT HAS_NET_TIMER_STARTED(tdRenderPhaseUnPauseDelay)
		PRINTLN("[LM][PROCESS_RENDERPHASE_PAUSE_UNPAUSE_WITH_DELAY] - Start Timer with iDelayPause: ", iDelayPause, " iDelayUnpause: ", iDelayUnpause, " iDebugPauseIndex: ", iDebugPauseIndex)
		iRenderPhasePauseDelay = iDelayPause
		iRenderPhaseUnPauseDelay = iDelayUnpause
		START_NET_TIMER(tdRenderPhaseUnPauseDelay)
		
		// Immediate
		IF iDelayPause = -1
			PRINTLN("[LM][PROCESS_RENDERPHASE_PAUSE_UNPAUSE_WITH_DELAY] - Timer Expired. PAUSING THE RENDERPHASE (immediately)")
			TOGGLE_PAUSED_RENDERPHASES(FALSE)
		ENDIF
	ELSE
		PRINTLN("[LM][PROCESS_RENDERPHASE_PAUSE_UNPAUSE_WITH_DELAY] - Timer Already Started.. Failed: ", iDebugPauseIndex)
	ENDIF
ENDPROC

PROC PROCESS_RENDERPHASE_UNPAUSE_WITH_DELAY()
	IF HAS_NET_TIMER_STARTED(tdRenderPhaseUnPauseDelay)
		PRINTLN("[LM][PROCESS_RENDERPHASE_PAUSE_UNPAUSE_WITH_DELAY] - Time Progressed for Pause: ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdRenderPhaseUnPauseDelay))
		
		IF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdRenderPhaseUnPauseDelay, iRenderPhasePauseDelay)
		AND iRenderPhasePauseDelay > -1
			PRINTLN("[LM][PROCESS_RENDERPHASE_PAUSE_UNPAUSE_WITH_DELAY] - Timer Expired. PAUSING THE RENDERPHASE")
			TOGGLE_PAUSED_RENDERPHASES(FALSE)
		ENDIF
		
		IF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdRenderPhaseUnPauseDelay, iRenderPhaseUnPauseDelay)
			PRINTLN("[LM][PROCESS_RENDERPHASE_PAUSE_UNPAUSE_WITH_DELAY] - Timer Expired. UNPAUSING / RESETTING THE RENDERPHASE")
			RESET_NET_TIMER(tdRenderPhaseUnPauseDelay)
			RESET_PAUSED_RENDERPHASES()
		ENDIF
	ENDIF
ENDPROC

PROC START_MISSION_TIMERS()
	
	PRINTLN("[RCC MISSION] START_MISSION_TIMERS called")
	
	REINIT_NET_TIMER(MC_serverBD.tdMissionLengthTimer)
	
	INT iTeam
	
	FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
		REINIT_NET_TIMER(MC_serverBD_3.tdTeamMissionTime[iTeam])
	ENDFOR
	
ENDPROC

PROC END_TEAM_MISSION_TIMER(INT iTeam)
	
	PRINTLN("[RCC MISSION] END_TEAM_MISSION_TIMER called for team ",iTeam)
	DEBUG_PRINTCALLSTACK()
	
	IF MC_serverBD_3.iTeamMissionTime[iTeam] = 0
		MC_serverBD_3.iTeamMissionTime[iTeam] = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdTeamMissionTime[iTeam])
		PRINTLN("[RCC MISSION] END_TEAM_MISSION_TIMER - Setting MC_serverBD_3.iTeamMissionTime[",iTeam,"] = ",MC_serverBD_3.iTeamMissionTime[iTeam])
	ENDIF
	
ENDPROC

PROC END_MISSION_MRT_CHECKPOINT_TIMERS(INT iCheckpoint)
	
	PRINTLN("[MMacK][Checkpoint][RetryMRT] Calling END_MISSION_MRT_CHECKPOINT_TIMERS")
	
	INT iTeam
	INT iMRTThreshold, iBit
	
	FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
		
		PRINTLN("[MMacK][Checkpoint][RetryMRT] END_MISSION_MRT_CHECKPOINT_TIMERS - iTeam: ", iTeam, " iCheckpoint = ", iCheckpoint)
		
		SWITCH iCheckpoint
			CASE 1
				iMRTThreshold = g_FMMC_STRUCT.iCheckpointMRTThreshold[0][iTeam]
				iBit = SB_CP1_MRT_1 + iTeam
			BREAK
			CASE 2
				iMRTThreshold = g_FMMC_STRUCT.iCheckpointMRTThreshold[1][iTeam]
				iBit = SB_CP2_MRT_1 + iTeam
			BREAK
			CASE 3
				iMRTThreshold = g_FMMC_STRUCT.iCheckpointMRTThreshold[2][iTeam]
				iBit = SB_CP3_MRT_1 + iTeam
			BREAK
			CASE 4
				iMRTThreshold = g_FMMC_STRUCT.iCheckpointMRTThreshold[3][iTeam]
				iBit = SB_CP4_MRT_1 + iTeam
			BREAK
		ENDSWITCH
		
		PRINTLN("[MMacK][Checkpoint][RetryMRT] END_MISSION_MRT_CHECKPOINT_TIMERS - iTeam: ", iTeam, " iBit = ", iBit)
		
		IF IS_BIT_SET(MC_ServerBD.iCheckpointBitset, iBit)
			IF MC_ServerBD_4.iTransitionSessionMultirule_Checkpoint[iTeam] != iCheckpoint
			OR MC_ServerBD_4.iTransitionSessionMultirule_TimeStamp[iTeam] = 0				
				INT iMultiRuleTimer_Remaining = GET_TIME_REMAINING_ON_MULTIRULE_TIMER()
				
				IF iMultiRuleTimer_Remaining >= iMRTThreshold
					MC_ServerBD_4.iTransitionSessionMultirule_TimeStamp[iTeam] = iMultiRuleTimer_Remaining
					PRINTLN("[MMacK][Checkpoint][RetryMRT] END_MISSION_MRT_CHECKPOINT_TIMERS - CP ",iCheckpoint," Team ",iTeam," time left ",iMultiRuleTimer_Remaining," is above/equal to iMRTThreshold ",iMRTThreshold,", set iRetryMultiRuleTimeStamp as that")
				ELSE
					MC_ServerBD_4.iTransitionSessionMultirule_TimeStamp[iTeam] = iMRTThreshold
					PRINTLN("[MMacK][Checkpoint][RetryMRT] END_MISSION_MRT_CHECKPOINT_TIMERS - CP ",iCheckpoint," Team ",iTeam," time left ",iMultiRuleTimer_Remaining," is less than iMRTThreshold ",iMRTThreshold,", set iRetryMultiRuleTimeStamp as iMRTThreshold")
				ENDIF
				
				MC_ServerBD_4.iTransitionSessionMultirule_Checkpoint[iTeam] = iCheckpoint
				PRINTLN("[MMacK][Checkpoint][RetryMRT] END_MISSION_MRT_CHECKPOINT_TIMERS - CP ",iCheckpoint," Team ",iTeam,", set iRetryMultiRuleTimeStamp_Checkpoint as ",MC_ServerBD_4.iTransitionSessionMultirule_Checkpoint[iTeam])
			#IF IS_DEBUG_BUILD
			ELSE
				PRINTLN("[MMacK][Checkpoint][RetryMRT] END_MISSION_MRT_CHECKPOINT_TIMERS - CP ",iCheckpoint," Team ",iTeam," already has iRetryMultiRuleTimeStamp set as ",MC_ServerBD_4.iTransitionSessionMultirule_TimeStamp[iTeam]," for this checkpoint")
			#ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
ENDPROC

PROC END_MISSION_TIMERS()
	
	IF bIsLocalPlayerHost
		
		PRINTLN("[RCC MISSION] END_MISSION_TIMERS called")
		DEBUG_PRINTCALLSTACK()
		
		IF MC_serverBD.iTotalMissionEndTime = 0
			MC_serverBD.iTotalMissionEndTime = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdMissionLengthTimer)
			PRINTLN("[RCC MISSION] END_MISSION_TIMERS - Setting MC_serverBD.iTotalMissionEndTime = ",MC_serverBD.iTotalMissionEndTime)
		ENDIF
		
		INT iTeam
		
		FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
			END_TEAM_MISSION_TIMER(iTeam)
		ENDFOR
						
		IF IS_BIT_SET(MC_ServerBD.iCheckpointBitset, SB_CHECKPOINT_4)
			END_MISSION_MRT_CHECKPOINT_TIMERS(4)
		ELIF IS_BIT_SET(MC_ServerBD.iCheckpointBitset, SB_CHECKPOINT_3)
			END_MISSION_MRT_CHECKPOINT_TIMERS(3)
		ELIF IS_BIT_SET(MC_ServerBD.iCheckpointBitset, SB_CHECKPOINT_2)
			END_MISSION_MRT_CHECKPOINT_TIMERS(2)
		ELIF IS_BIT_SET(MC_ServerBD.iCheckpointBitset, SB_CHECKPOINT_1)
			END_MISSION_MRT_CHECKPOINT_TIMERS(1)
		ELSE
			PRINTLN("[MMacK][Checkpoint][RetryMRT] END_MISSION_TIMERS - No checkpoints, clear iRetryMultiRuleTimeStamp & iRetryMultiRuleTimeStamp_Checkpoint for all teams")
			FOR iTeam = 0 TO FMMC_MAX_TEAMS-1
				g_TransitionSessionNonResetVars.iRetryMultiRuleTimeStamp_Checkpoint[iTeam] = 0
				g_TransitionSessionNonResetVars.iRetryMultiRuleTimeStamp[iTeam] = 0
			ENDFOR
		ENDIF
		
	ENDIF
	
ENDPROC

PROC CLEAR_SPAWN_GROUP_STRUCT(sFMMC_RandomSpawnGroupStruct &rsgSpawnStruct)
	
	PRINTLN("[RCC MISSION] CLEAR_SPAWN_GROUP_STRUCT called")
	DEBUG_PRINTCALLSTACK()
	
	sFMMC_RandomSpawnGroupStruct sEmpty
	rsgSpawnStruct = sEmpty
	
ENDPROC

FUNC INT GET_NUMBER_OF_RANDOM_ENTITY_SPAWN_GROUPS_TO_SPAWN(INT iSubGroupArray = -1)
	
	INT iNum = 0
	INT iBS
	
	IF iSubGroupArray = -1
		iNum = g_FMMC_STRUCT.iRandomEntitySpawn_RandomNumbersToUse
		iBS = g_FMMC_STRUCT.iRandomEntitySpawn_Bitset
	ELSE
		iNum = g_FMMC_STRUCT.iRandomEntitySpawnSub_RandomNumbersToUse[iSubGroupArray]
		iBS = g_FMMC_STRUCT.iRandomEntitySpawnSub_Bitset[iSubGroupArray]
	ENDIF
	
	PRINTLN("[RCC MISSION] GET_NUMBER_OF_RANDOM_ENTITY_SPAWN_GROUPS_TO_SPAWN - iSubGroupArray ",iSubGroupArray," iNum = ",iNum)
	
	IF iNum = ciNUMBER_OF_RANDOM_SPAWNS_PLAYER_NUM
		PRINTLN("[RCC MISSION] GET_NUMBER_OF_RANDOM_ENTITY_SPAWN_GROUPS_TO_SPAWN - player num! iBS = ",iBS)
		
		IF iBS > 0
			INT iTeam
			iNum = 0
			
			FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
				IF IS_BIT_SET(iBS, ciRandomSpawn_SpawnNumberOfPlayers_T0 + iTeam)
					iNum += MC_serverBD.iNumStartingPlayers[iTeam]
					PRINTLN("[RCC MISSION] GET_NUMBER_OF_RANDOM_ENTITY_SPAWN_GROUPS_TO_SPAWN - BS is set for team ",iTeam,", add on ",MC_serverBD.iNumStartingPlayers[iTeam]," players for a current total of ",iNum)
				ENDIF
			ENDFOR
		ELSE
			iNum = MC_serverBD.iTotalNumStartingPlayers
			PRINTLN("[RCC MISSION] GET_NUMBER_OF_RANDOM_ENTITY_SPAWN_GROUPS_TO_SPAWN - BS is empty, just return total starting players of ",MC_serverBD.iTotalNumStartingPlayers)
		ENDIF
	ENDIF
	
	RETURN iNum
	
ENDFUNC

PROC ADD_NEW_SPAWN_GROUP_TO_BUILD_IN_PLAY(INT iSpawnGroupToAdd)
	
	IF iSpawnGroupToAdd <= 0
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_BuildSpawnGroupsDuringPlay)
		EXIT
	ENDIF
	
	IF MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeedBS != 0
		PRINTLN("[RCC MISSION][ran] ADD_NEW_SPAWN_GROUP_TO_BUILD_IN_PLAY - Bail, rsgSpawnSeed is already set up")
		EXIT
	ENDIF
	
	PRINTLN("[RCC MISSION][ran] ADD_NEW_SPAWN_GROUP_TO_BUILD_IN_PLAY - Called w iSpawnGroupToAdd ",iSpawnGroupToAdd,", callstack:")
	DEBUG_PRINTCALLSTACK()
	
	IF IS_BIT_SET(MC_serverBD_4.iSpawnGroupsBuiltInPlay_TempSpawnSeedBS, iSpawnGroupToAdd)
		PRINTLN("[RCC MISSION][ran] ADD_NEW_SPAWN_GROUP_TO_BUILD_IN_PLAY - iSpawnGroupsBuiltInPlay_TempSpawnSeedBS bit ",iSpawnGroupToAdd," is already set")
		EXIT
	ENDIF
	
	PRINTLN("[RCC MISSION][ran] ADD_NEW_SPAWN_GROUP_TO_BUILD_IN_PLAY - Set iSpawnGroupsBuiltInPlay_TempSpawnSeedBS bit ",(iSpawnGroupToAdd - 1))
	SET_BIT(MC_serverBD_4.iSpawnGroupsBuiltInPlay_TempSpawnSeedBS, iSpawnGroupToAdd - 1)
	
	INT iNumberOfSetBits = COUNT_SET_BITS(MC_serverBD_4.iSpawnGroupsBuiltInPlay_TempSpawnSeedBS)
	
	IF iNumberOfSetBits < GET_NUMBER_OF_RANDOM_ENTITY_SPAWN_GROUPS_TO_SPAWN()
		PRINTLN("[RCC MISSION][ran] ADD_NEW_SPAWN_GROUP_TO_BUILD_IN_PLAY - Not enough bits set yet, iNumberOfSetBits = ",iNumberOfSetBits," < ",GET_NUMBER_OF_RANDOM_ENTITY_SPAWN_GROUPS_TO_SPAWN())
		EXIT
	ENDIF
		
	PRINTLN("[RCC MISSION][ran] ADD_NEW_SPAWN_GROUP_TO_BUILD_IN_PLAY - iNumberOfSetBits = ",iNumberOfSetBits," >= ",GET_NUMBER_OF_RANDOM_ENTITY_SPAWN_GROUPS_TO_SPAWN(),", set the spawn seed!")
	
	MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeedBS = MC_serverBD_4.iSpawnGroupsBuiltInPlay_TempSpawnSeedBS
	PRINTLN("[RCC MISSION][ran] ADD_NEW_SPAWN_GROUP_TO_BUILD_IN_PLAY - MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeedBS = ",MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeedBS,", remove any invalid entities")
	
	INT i, iTeam
	
	FOR i = 0 TO (MC_serverBD.iNumLocCreated - 1)
		IF NOT DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP(i, MC_serverBD_4.rsgSpawnSeed, eSGET_GotoLoc)
			PRINTLN("[RCC MISSION][ran] ADD_NEW_SPAWN_GROUP_TO_BUILD_IN_PLAY - Remove loc ",i," rules & FailBitset")
			FOR iTeam = 0 TO (FMMC_MAX_TEAMS - 1)
				MC_serverBD_4.iGotoLocationDataPriority[i][iTeam] = FMMC_PRIORITY_IGNORE
				MC_serverBD_4.iGotoLocationDataRule[i][iTeam] = FMMC_OBJECTIVE_LOGIC_NONE
				CLEAR_BIT(MC_serverBD.iLocteamFailBitset[i], iteam)
			ENDFOR
		ENDIF
	ENDFOR
	
	FOR i = 0 TO (MC_serverBD.iNumPedCreated - 1)
		IF NOT DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP(i, MC_serverBD_4.rsgSpawnSeed, eSGET_Ped)
			PRINTLN("[RCC MISSION][ran] ADD_NEW_SPAWN_GROUP_TO_BUILD_IN_PLAY - Remove ped ",i," rules & Crit & FailBitset")
			FOR iTeam = 0 TO (FMMC_MAX_TEAMS - 1)
				MC_serverBD_4.iPedPriority[i][iTeam] = FMMC_PRIORITY_IGNORE
				MC_serverBD_4.iPedRule[i][iTeam] = FMMC_OBJECTIVE_LOGIC_NONE
				CLEAR_BIT(MC_serverBD.iMissionCriticalPed[iTeam][GET_LONG_BITSET_INDEX(i)], GET_LONG_BITSET_BIT(i))
				CLEAR_BIT(MC_serverBD.iPedteamFailBitset[i], iTeam)
			ENDFOR
		ENDIF
	ENDFOR
	
	FOR i = 0 TO (MC_serverBD.iNumVehCreated - 1)
		IF NOT DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP(i, MC_serverBD_4.rsgSpawnSeed, eSGET_Vehicle)
			PRINTLN("[RCC MISSION][ran] ADD_NEW_SPAWN_GROUP_TO_BUILD_IN_PLAY - Remove vehicle ",i," rules & Crit & FailBitset")
			FOR iTeam = 0 TO (FMMC_MAX_TEAMS - 1)
				MC_serverBD_4.iVehPriority[i][iTeam] = FMMC_PRIORITY_IGNORE
				MC_serverBD_4.ivehRule[i][iTeam] = FMMC_OBJECTIVE_LOGIC_NONE
				CLEAR_BIT(MC_serverBD.iMissionCriticalVeh[iteam], i)
				CLEAR_BIT(MC_serverBD.iVehteamFailBitset[i], iteam)
			ENDFOR
		ENDIF
	ENDFOR
	
	FOR i = 0 TO (MC_serverBD.iNumObjCreated - 1)
		IF NOT DOES_ENTITY_HAVE_SUITABLE_SPAWN_GROUP(i, MC_serverBD_4.rsgSpawnSeed, eSGET_Object)
			PRINTLN("[RCC MISSION][ran] ADD_NEW_SPAWN_GROUP_TO_BUILD_IN_PLAY - Remove object ",i," rules & Crit & FailBitset")
			FOR iTeam = 0 TO (FMMC_MAX_TEAMS - 1)
				MC_serverBD_4.iObjPriority[i][iteam] = FMMC_PRIORITY_IGNORE
				MC_serverBD_4.iObjRule[i][iteam] = FMMC_OBJECTIVE_LOGIC_NONE
				CLEAR_BIT(MC_serverBD.iObjteamFailBitset[i], iteam)
				CLEAR_BIT(MC_serverBD.iMissionCriticalObj[iTeam], i)
			ENDFOR
		ENDIF
	ENDFOR
	
ENDPROC




//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: CREATOR DATA EXTRACTION FUNCTIONS (getting data from the creator globals to use it in missions) !
//
//************************************************************************************************************************************************************

FUNC INT GET_EXTRA_POINTS_FOR_TEAM(INT iTeam)

	UNUSED_PARAMETER(iTeam)
	
	RETURN 0
ENDFUNC


FUNC INT GET_FMMC_POINTS_FOR_TEAM(INT iTeam, INT iRule = -1)
	
	INT iPoints
	
	IF iTeam != -1
		
		IF iRule = -1
			iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
		ENDIF
		
		IF iRule < FMMC_MAX_RULES
			INT iSelection = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjectiveScore[iRule]
			
			iPoints = GET_FMMC_OBJECTIVE_SCORE_LIMIT_FROM_SELECTION(iSelection, MC_ServerBD.iNumberOfPlayingPlayers[iTeam])
			
			PRINTLN("[RCC MISSION] GET_FMMC_POINTS_FOR_TEAM - Team ",iTeam," should receive ",iPoints," points (creator selection ",iSelection,") for completing objective ",iRule)
		ENDIF
	ENDIF
	
	RETURN iPoints + GET_EXTRA_POINTS_FOR_TEAM(iTeam)
	
ENDFUNC

FUNC INT GET_FMMC_CAPTURE_TIME_FROM_SELECTION(INT iSelection)

    SWITCH iSelection
        CASE FMMC_CAPTURE_TIME_10_SECONDS             RETURN 10000
        CASE FMMC_CAPTURE_TIME_20_SECONDS             RETURN 20000
        CASE FMMC_CAPTURE_TIME_30_SECONDS             RETURN 30000
        CASE FMMC_CAPTURE_TIME_40_SECONDS             RETURN 40000
        CASE FMMC_CAPTURE_TIME_50_SECONDS             RETURN 50000
        CASE FMMC_CAPTURE_TIME_1_MINUTE               RETURN 60000
        CASE FMMC_CAPTURE_TIME_2_MINUTES              RETURN 120000
        CASE FMMC_CAPTURE_TIME_3_MINUTES              RETURN 180000
        CASE FMMC_CAPTURE_TIME_4_MINUTES              RETURN 240000
        CASE FMMC_CAPTURE_TIME_5_MINUTES              RETURN 300000
        CASE FMMC_CAPTURE_TIME_6_MINUTES              RETURN 360000
        CASE FMMC_CAPTURE_TIME_7_MINUTES              RETURN 420000
        CASE FMMC_CAPTURE_TIME_8_MINUTES              RETURN 480000
        CASE FMMC_CAPTURE_TIME_9_MINUTES              RETURN 540000
        CASE FMMC_CAPTURE_TIME_10_MINUTES             RETURN 600000
        CASE FMMC_CAPTURE_TIME_1_SECOND				  RETURN 1000
        CASE FMMC_CAPTURE_TIME_2_SECONDS			  RETURN 2000
        CASE FMMC_CAPTURE_TIME_3_SECONDS			  RETURN 3000
        CASE FMMC_CAPTURE_TIME_4_SECONDS			  RETURN 4000
        CASE FMMC_CAPTURE_TIME_5_SECONDS			  RETURN 5000
        CASE FMMC_CAPTURE_TIME_6_SECONDS			  RETURN 6000
        CASE FMMC_CAPTURE_TIME_7_SECONDS			  RETURN 7000
        CASE FMMC_CAPTURE_TIME_8_SECONDS			  RETURN 8000
        CASE FMMC_CAPTURE_TIME_9_SECONDS			  RETURN 9000
		
    ENDSWITCH
		
    RETURN 120000
ENDFUNC

FUNC INT GET_FMMC_GRANULAR_CAPTURE_TARGET(INT iPriority, INT iTeam)
	RETURN MC_serverBD.iObjTakeoverTime[iTeam][iPriority] / 1000
ENDFUNC

FUNC INT GET_FMMC_MISSION_LIVES_FOR_TEAM(INT iteam, BOOL bLocalPlayerLives = FALSE)

	INT iCreatorSetting = g_FMMC_STRUCT.sFMMCEndConditions[iteam].iPlayerLives
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSet, ciALL_LIVES_POOL_OPTION)
		PRINTLN("[RCC MISSION] GET_FMMC_MISSION_LIVES_FOR_TEAM - ciALL_LIVES_POOL_OPTION: ", iCreatorSetting)
		iCreatorSetting = g_FMMC_STRUCT.sFMMCEndConditions[0].iPlayerLives
	ENDIF
	
	IF MC_serverBD_4.iCurrentHighestPriority[iteam] < FMMC_MAX_RULES
	AND (g_FMMC_STRUCT.sFMMCEndConditions[iteam].iPlayerLivesPerRule[MC_serverBD_4.iCurrentHighestPriority[iteam]] != -1)
		iCreatorSetting = g_FMMC_STRUCT.sFMMCEndConditions[iteam].iPlayerLivesPerRule[MC_serverBD_4.iCurrentHighestPriority[iteam]]
		PRINTLN("[RCC MISSION] GET_FMMC_MISSION_LIVES_FOR_TEAM - iPlayerLivesPerRule: ", iCreatorSetting)
	ENDIF
	
	IF NOT (bLocalPlayerLives AND IS_BIT_SET(iLocalBoolCheck23, LBOOL23_DIED_BEFORE_SUDDEN_DEATH))	//Player died before sudden death, so allow them to respawn
		IF IS_BIT_SET(MC_serverBD.iServerBitSet1,SBBOOL1_SUDDEN_DEATH)
		AND (IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_SUDDEN_DEATH_KILLALLENEMIES)
		OR IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_BE_MY_VALENTINES)
		OR IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_KILLALLENEMIES_PENNED_IN)
		OR IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_LOST_AND_DAMNED)
		OR IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_SUMO_PENNED_IN)
		OR (IS_BIT_SET(MC_serverBD.iServerBitSet4, SBBOOL4_SUDDEN_DEATH_TURFWAR) AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_TURF_WARZONE(g_FMMC_STRUCT.iAdversaryModeType))
		OR IS_BIT_SET(MC_ServerBD.iServerBitSet4, SBBOOL4_SUDDEN_DEATH_JUGGERNAUT)
		OR IS_BIT_SET(MC_serverBD.iServerBitSet6, SBBOOL6_SUDDEN_DEATH_GUARDIAN)
		OR (IS_BIT_SET(MC_serverBD.iServerBitSet6, SBBOOL6_SUDDEN_DEATH_STOCKPILE) AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciNEXT_DELIVERY_WINS_SD_ONLY))
		OR IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_POWER_PLAY) AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciSUDDEN_DEATH_POWER_PLAY_CAPTURE)
		OR (IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_POWER_PLAY) AND IS_THIS_ROCKSTAR_MISSION_NEW_VS_HARD_TARGET(g_FMMC_STRUCT.iAdversaryModeType))
		OR IS_BIT_SET(MC_ServerBD.iServerBitSet7, SBBOOL7_SUDDEN_DEATH_TAG_TEAM))
			PRINTLN("[RCC MISSION] GET_FMMC_MISSION_LIVES_FOR_TEAM - In sudden death, returning 0 lives")
			RETURN 0
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciCORONA_TEAM_LIVES_SETTING)
	//AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet1,SBBOOL1_SUDDEN_DEATH)
		iCreatorSetting = MC_serverBD_1.iCoronaTeamLives[iTeam]
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciEND_ROUND_ON_ZERO_LIVES)
			iCreatorSetting = iCreatorSetting - 1
		ENDIF
		
		PRINTLN("[RCC MISSION] GET_FMMC_MISSION_LIVES_FOR_TEAM - !OVERRIDING LIVES! - iCoronaTeamLives: ", iCreatorSetting)
		RETURN iCreatorSetting
	ENDIF
	
	INT iAdditionalLives
	IF bLocalPlayerLives
		iAdditionalLives = MC_playerBD[iPartToUse].iAdditionalPlayerLives
	ELSE
		iAdditionalLives = MC_serverBD_3.iAdditionalTeamLives[iteam]
	ENDIF
	PRINTLN("[RCC MISSION] GET_FMMC_MISSION_LIVES_FOR_TEAM - iCreatorSetting: ", iCreatorSetting, " iAdditionalLives", iAdditionalLives, " iTeam: ", iteam)
	SWITCH iCreatorSetting
		CASE ciLEGACY__NUMBER_OF_LIVES_0_LIVES							RETURN 0 + iAdditionalLives
		CASE ciLEGACY__NUMBER_OF_LIVES_UNLIMITED		            	RETURN ciFMMC_UNLIMITED_LIVES
		CASE ciLEGACY__NUMBER_OF_LIVES_1_LIFE            				RETURN 1 + iAdditionalLives
		CASE ciLEGACY__NUMBER_OF_LIVES_2_LIVES             				RETURN 2 + iAdditionalLives
		CASE ciLEGACY__NUMBER_OF_LIVES_3_LIVES            				RETURN 3 + iAdditionalLives
		CASE ciLEGACY__NUMBER_OF_LIVES_4_LIVES             				RETURN 4 + iAdditionalLives
		CASE ciLEGACY__NUMBER_OF_LIVES_5_LIVES             				RETURN 5 + iAdditionalLives
		CASE ciLEGACY__NUMBER_OF_LIVES_6_LIVES             				RETURN 6 + iAdditionalLives
		CASE ciLEGACY__NUMBER_OF_LIVES_7_LIVES            				RETURN 7 + iAdditionalLives
		CASE ciLEGACY__NUMBER_OF_LIVES_8_LIVES             				RETURN 8 + iAdditionalLives
		CASE ciLEGACY__NUMBER_OF_LIVES_9_LIVES             				RETURN 9 + iAdditionalLives
		CASE ciLEGACY__NUMBER_OF_LIVES_10_LIVES               			RETURN 10 + iAdditionalLives
		CASE ciLEGACY__NUMBER_OF_LIVES_15_LIVES              			RETURN 15 + iAdditionalLives
		CASE ciLEGACY__NUMBER_OF_LIVES_20_LIVES              			RETURN 20 + iAdditionalLives
		CASE ciLEGACY__NUMBER_OF_LIVES_30_LIVES              			RETURN 30 + iAdditionalLives
		CASE ciLEGACY__NUMBER_OF_LIVES_50_LIVES              			RETURN 50 + iAdditionalLives
		CASE ciLEGACY__NUMBER_OF_LIVES_100_LIVES             			RETURN 100 + iAdditionalLives
		CASE ciLEGACY__NUMBER_OF_LIVES_PLAYER_NUMS             			RETURN MC_serverBD.iVariablePlayerLives[iteam] + iAdditionalLives
		CASE ciLEGACY__NUMBER_OF_LIVES_PLAYER_NUMS_2             		RETURN (2 * MC_serverBD.iVariablePlayerLives[iteam]) + iAdditionalLives
	ENDSWITCH
	
	RETURN 0
	
ENDFUNC

FUNC INT GET_FMMC_MISSION_LIVES_FOR_COOP_TEAMS()
	RETURN GET_FMMC_MISSION_LIVES_FOR_TEAM(0)
ENDFUNC

FUNC INT GET_FMMC_MISSION_LIVES_FOR_LOCAL_PLAYER()
	
	IF NOT IS_BIT_SET(iLocalBoolCheck23, LBOOL23_DIED_BEFORE_SUDDEN_DEATH)	//Player died before sudden death, so allow them to respawn
		IF IS_BIT_SET(MC_serverBD.iServerBitSet1,SBBOOL1_SUDDEN_DEATH)
		AND (IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_SUDDEN_DEATH_KILLALLENEMIES)
		OR IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_BE_MY_VALENTINES)
		OR IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_KILLALLENEMIES_PENNED_IN)
		OR IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_LOST_AND_DAMNED)
		OR IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_SUMO_PENNED_IN)
		OR (IS_BIT_SET(MC_serverBD.iServerBitSet4, SBBOOL4_SUDDEN_DEATH_TURFWAR) AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_TURF_WARZONE(g_FMMC_STRUCT.iAdversaryModeType))
		OR IS_BIT_SET(MC_serverBD.iServerBitSet4, SBBOOL4_SUDDEN_DEATH_JUGGERNAUT)
		OR IS_BIT_SET(MC_serverBD.iServerBitSet6, SBBOOL6_SUDDEN_DEATH_GUARDIAN)
		OR IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_POWER_PLAY) AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciSUDDEN_DEATH_POWER_PLAY_CAPTURE)
		OR (IS_BIT_SET(MC_serverBD.iServerBitSet6, SBBOOL6_SUDDEN_DEATH_STOCKPILE) AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciNEXT_DELIVERY_WINS_SD_ONLY))
		OR (IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_POWER_PLAY) AND IS_THIS_ROCKSTAR_MISSION_NEW_VS_HARD_TARGET(g_FMMC_STRUCT.iAdversaryModeType))
		OR IS_BIT_SET(MC_ServerBD.iServerBitSet7, SBBOOL7_SUDDEN_DEATH_TAG_TEAM))
			IF iSuddenDeath_PlayerLives = -1
				iSuddenDeath_PlayerLives = MC_playerBD[iPartToUse].iNumPlayerDeaths + 1
				PRINTLN("[RCC MISSION] GET_FMMC_MISSION_LIVES_FOR_LOCAL_PLAYER - iSuddenDeath_PlayerLives initialised to ",iSuddenDeath_PlayerLives," (should have one more life)")
			ENDIF
			RETURN iSuddenDeath_PlayerLives
		ENDIF
	ENDIF
	
	RETURN GET_FMMC_MISSION_LIVES_FOR_TEAM(MC_playerBD[iPartToUse].iteam, TRUE)
	
ENDFUNC

FUNC INT GET_FMMC_OBJECTIVE_RESPAWNS_FROM_SELECTION(INT iSelection)

    SWITCH iSelection
        CASE LEGACY__NUMBER_OF_RESPAWNS_0            			RETURN 0
        CASE LEGACY__NUMBER_OF_RESPAWNS_1            			RETURN 1
        CASE LEGACY__NUMBER_OF_RESPAWNS_2             			RETURN 2
        CASE LEGACY__NUMBER_OF_RESPAWNS_3            			RETURN 3
        CASE LEGACY__NUMBER_OF_RESPAWNS_4             			RETURN 4
        CASE LEGACY__NUMBER_OF_RESPAWNS_5             			RETURN 5
        CASE LEGACY__NUMBER_OF_RESPAWNS_6               		RETURN 6
        CASE LEGACY__NUMBER_OF_RESPAWNS_7              			RETURN 7
        CASE LEGACY__NUMBER_OF_RESPAWNS_8              			RETURN 8
        CASE LEGACY__NUMBER_OF_RESPAWNS_9              			RETURN 9
        CASE LEGACY__NUMBER_OF_RESPAWNS_10              		RETURN 10
        CASE LEGACY__NUMBER_OF_RESPAWNS_15             			RETURN 15
        CASE LEGACY__NUMBER_OF_RESPAWNS_20              		RETURN 20
		CASE LEGACY__NUMBER_OF_RESPAWNS_UNLIMITED		        RETURN UNLIMITED_RESPAWNS
		CASE LEGACY__NUMBER_OF_RESPAWNS_PLAYER_NUM				RETURN MC_serverBD.iVariableEnemyRespawns
    ENDSWITCH
	
    RETURN 0
	
ENDFUNC

FUNC INT GET_MISSION_END_TIME_FROM_MENU_SELECTION(INT iSelection)

    SWITCH iSelection
		CASE ciFMMC_MISSION_END_TIME_IGNORE             RETURN -1
        CASE ciFMMC_MISSION_END_TIME_1_MINUTE           RETURN (60*1000)
        CASE ciFMMC_MISSION_END_TIME_2_MINUTES          RETURN (2*60*1000)
        CASE ciFMMC_MISSION_END_TIME_5_MINUTES          RETURN (5*60*1000)
        CASE ciFMMC_MISSION_END_TIME_10_MINUTES         RETURN (10*60*1000)
        CASE ciFMMC_MISSION_END_TIME_15_MINUTES         RETURN (15*60*1000)
        CASE ciFMMC_MISSION_END_TIME_20_MINUTES         RETURN (20*60*1000)
        CASE ciFMMC_MISSION_END_TIME_30_MINUTES         RETURN (30*60*1000)
        CASE ciFMMC_MISSION_END_TIME_1_HOUR             RETURN (60*60*1000)
        CASE ciFMMC_MISSION_END_TIME_2_HOURS            RETURN (120*60*1000)
    ENDSWITCH
	
    RETURN -1
ENDFUNC





//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: MISSION TYPE CHECKS !
//
//************************************************************************************************************************************************************



FUNC BOOL IS_TEAM_COOP_MISSION()
INT iteam
INT iteamrepeat

	IF MC_serverBD.iNumberOfTeams < 2
		RETURN TRUE
	ENDIF
	
	FOR iteam = 0 TO (FMMC_MAX_TEAMS-1)
		FOR iteamrepeat = 0 TO (FMMC_MAX_TEAMS-1)
			IF NOT DOES_TEAM_LIKE_TEAM(iteam,iteamrepeat)
				RETURN FALSE
			ENDIF
		ENDFOR
	ENDFOR
	
	RETURN TRUE

ENDFUNC

FUNC BOOL IS_TEAM_VS_MISSION()

INT iteamrepeat
INT iteam

	IF MC_serverBD.iNumberOfTeams < 2
		RETURN FALSE
	ENDIF

	FOR iteam = 0 TO (FMMC_MAX_TEAMS-1)
		FOR iteamrepeat = 0 TO (FMMC_MAX_TEAMS-1)
			IF iteam != iteamrepeat
				IF NOT DOES_TEAM_LIKE_TEAM(iteam,iteamrepeat)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDFOR
	ENDFOR
	
	RETURN FALSE

ENDFUNC

FUNC BOOL IS_THIS_A_JOB_THAT_HAS_QUICK_RESTARTS()
	IF AM_I_ON_A_HEIST() OR Is_Player_Currently_On_MP_Contact_Mission( PLAYER_ID() ) OR Is_Player_Currently_On_MP_Coop_Mission( PLAYER_ID() )
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_THIS_THE_FIRST_ROUND_OR_NOT_A_ROUNDS_MISSION()
	IF ( IS_THIS_A_ROUNDS_MISSION() AND g_sTransitionSessionData.sMissionRoundData.iNumberOfRoundsPlayed = 0 )
	OR NOT IS_THIS_A_ROUNDS_MISSION()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: TICKER BROADCAST SYSTEM !
//
//************************************************************************************************************************************************************



FUNC INT GET_ENTITY_PRIORITY(INT iTeam, INT iEntityType,INT iEntityNum)

	INT ipriority = FMMC_PRIORITY_IGNORE
	BOOL bCheckExtraObjectives
	INT iRuleType = ciRULE_TYPE_NONE
	
	INT iCurrentRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	iCurrentRule = CLAMP_INT(iCurrentRule, 0, FMMC_MAX_RULES) // We don't want to be getting rules with FMMC_PRIORITY_IGNORE
	
	SWITCH iEntityType
		CASE ci_TARGET_LOCATION
			IF iEntityNum >= 0
			AND iEntityNum < MC_serverBD.iNumLocCreated
				
				ipriority = MC_serverBD_4.iGotoLocationDataPriority[iEntityNum][iTeam]
				
				IF ipriority >= FMMC_MAX_RULES
					
					ipriority = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iEntityNum].iPriority[iTeam]
					
					IF ipriority < iCurrentRule
						bCheckExtraObjectives = TRUE
						iRuleType = ciRULE_TYPE_GOTO
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE ci_TARGET_PED
			IF iEntityNum >= 0
			AND iEntityNum < MC_serverBD.iNumPedCreated
				
				ipriority = MC_serverBD_4.iPedPriority[iEntityNum][iTeam]
				
				IF ipriority >= FMMC_MAX_RULES
					
					ipriority = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iEntityNum].iPriority[iTeam]
					
					IF ipriority < iCurrentRule
						bCheckExtraObjectives = TRUE
						iRuleType = ciRULE_TYPE_PED
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE ci_TARGET_VEHICLE
			IF iEntityNum >= 0
			AND iEntityNum < MC_serverBD.iNumVehCreated
				
				ipriority = MC_serverBD_4.iVehPriority[iEntityNum][iTeam]
				
				IF ipriority >= FMMC_MAX_RULES
					
					ipriority = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iEntityNum].iPriority[iTeam]
					
					IF ipriority < iCurrentRule
						bCheckExtraObjectives = TRUE
						iRuleType = ciRULE_TYPE_VEHICLE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE ci_TARGET_OBJECT
			IF iEntityNum >= 0
			AND iEntityNum < MC_serverBD.iNumObjCreated
				
				ipriority = MC_serverBD_4.iObjPriority[iEntityNum][iTeam]
				
				IF ipriority >= FMMC_MAX_RULES
					
					ipriority = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iEntityNum].iPriority[iTeam]
					
					IF ipriority < iCurrentRule
						bCheckExtraObjectives = TRUE
						iRuleType = ciRULE_TYPE_OBJECT
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH
	
	//Get the last objective we had:
	IF bCheckExtraObjectives
		INT iextraObjectiveNum
		// Loop through all potential entites that have multiple rules
		FOR iextraObjectiveNum = 0 TO ( MAX_NUM_EXTRA_OBJECTIVE_ENTITIES-1 )
			// If the type matches the one passed
			IF g_FMMC_STRUCT.iExtraObjectiveEntityType[iextraObjectiveNum] = iRuleType
			AND g_FMMC_STRUCT.iExtraObjectiveEntityId[iextraObjectiveNum] = iEntityNum
				INT iextraObjectiveLoop
				
				FOR iextraObjectiveLoop = 0 TO (MAX_NUM_EXTRA_OBJECTIVE_PER_ENTITY - 1)
					IF g_FMMC_STRUCT.iExtraObjectivePriority[iextraObjectiveNum][iextraObjectiveLoop][iTeam] < iCurrentRule
					AND (GET_FMMC_RULE_FROM_CREATOR_RULE(g_FMMC_STRUCT.iExtraObjectiveRule[iextraObjectiveNum][iextraObjectiveLoop][iTeam]) != FMMC_OBJECTIVE_LOGIC_NONE)
						ipriority = g_FMMC_STRUCT.iExtraObjectivePriority[iextraObjectiveNum][iextraObjectiveLoop][iTeam]
					ENDIF
				ENDFOR
				//We found the multi-rule number for this entity, there won't be any more so break out of the loop:
				iextraObjectiveNum = MAX_NUM_EXTRA_OBJECTIVE_ENTITIES
			ENDIF
		ENDFOR
	ENDIF
	
	RETURN ipriority

ENDFUNC

FUNC INT GET_ENTITY_PRIORITY_FOR_ANY_TEAM(INT iEntityType,INT iEntityNum,INT& iReturnTeam)

	INT ipriority = FMMC_PRIORITY_IGNORE
	INT iTempPriority
	INT iTeam
	
	iReturnTeam = -1
	
	FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
		iTempPriority = GET_ENTITY_PRIORITY(iTeam, iEntityType, iEntityNum)
		
		IF iTempPriority < FMMC_MAX_RULES
			ipriority = iTempPriority
			iReturnTeam = iTeam
			iTeam = MC_serverBD.iNumberOfTeams // Break out!
		ENDIF
	ENDFOR
	
	RETURN ipriority

ENDFUNC

PROC BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENTS aTicker, INT iscore = 0, INT iteam = -1,INT isublogic = -1,PLAYER_INDEX player = NULL,INT iEntityType = -1, INT iEntityNum = -1,BOOL bincludeself = TRUE,INT iShardOption = 0, BOOL bBlockAudio = FALSE)
	DEBUG_PRINTCALLSTACK()
	SCRIPT_EVENT_DATA_TICKER_CUSTOM_MESSAGE TickerMessage
	
	TickerMessage.TickerEvent = aTicker
	TickerMessage.iEntityNum = iEntityNum
	INT ipriority = FMMC_PRIORITY_IGNORE
	BOOL bBroadcast
	INT iReturnTeam = -1
	INT iteamtoUse
	
	IF iteam > -1
		
		IF iEntityType > -1
		AND iEntityNum > -1
			ipriority = GET_ENTITY_PRIORITY(iTeam,iEntityType,iEntityNum)
		ENDIF
		
		IF ipriority >= FMMC_MAX_RULES
			ipriority = GET_ENTITY_PRIORITY_FOR_ANY_TEAM(iEntityType,iEntityNum,iReturnTeam)
		ENDIF
		
		IF ipriority >= FMMC_MAX_RULES
			ipriority = MC_serverBD_4.iCurrentHighestPriority[iteam]
		ENDIF
		
		IF iReturnTeam > -1
			iteamtoUse = iReturnTeam
		ELSE
			iteamtoUse = iteam
		ENDIF
		
		IF ipriority < FMMC_MAX_RULES
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iteamtoUse].iRuleBitset[ipriority], ciBS_RULE_HIDE_TICKERS)
				bBroadcast = TRUE
			ENDIF
		ENDIF
		
		PRINTLN("BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS: iEntityType: ",iEntityType,", iEntityNum: ",iEntityNum,", ipriority: ",ipriority,", iteamtouse: ",iteamtouse,", bBroadcast: ",bBroadcast)
	ENDIF
	
	IF bBroadcast
		IF iscore > 0
			TickerMessage.dataInt = iscore
		ENDIF
		
		TickerMessage.TeamInt = iteam
		
		TickerMessage.playerID = player
		
		TickerMessage.iSubType = isublogic
		
		TickerMessage.iTeamToUse = iteamtoUse
		TickerMessage.iRule = ipriority
		TickerMessage.blockAudio = bBlockAudio
		TickerMessage.dataDisplayOption = iShardOption
		BROADCAST_CUSTOM_TICKER_EVENT(TickerMessage, ALL_PLAYERS_ON_SCRIPT(bincludeself))
	ENDIF
	
ENDPROC


//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: NETWORKED SYNC SCENE CHECKING FUNCTIONS !
//
//************************************************************************************************************************************************************



INT iThisLocalSceneID

//PURPOSE: Returns TRUE if the Sync Scene is active not finished like the poorly named command IS_SYNC_SCENE_ACTIVE!!
FUNC BOOL IS_SYNC_SCENE_RUNNING(INT ThisSynchScene)
	iThisLocalSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(ThisSynchScene)
	IF iThisLocalSceneID != -1
		IF IS_SYNCHRONIZED_SCENE_RUNNING(iThisLocalSceneID)		
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Returns TRUE if the SYnc Scene has finsihed
FUNC BOOL HAS_SYNC_SCENE_FINISHED(INT ThisSynchScene)
	iThisLocalSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(ThisSynchScene)
	IF iThisLocalSceneID != -1
		IF IS_SYNCHRONIZED_SCENE_RUNNING(iThisLocalSceneID)	
			IF GET_SYNCHRONIZED_SCENE_PHASE(iThisLocalSceneID) >= 1.0
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Returns TRUE if the SYnc Scene has finsihed
FUNC BOOL IS_SYNC_SCENE_ACTIVE(INT ThisSynchScene)
	iThisLocalSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(ThisSynchScene)
	IF iThisLocalSceneID != -1
		IF IS_SYNCHRONIZED_SCENE_RUNNING(iThisLocalSceneID)	
			IF GET_SYNCHRONIZED_SCENE_PHASE(iThisLocalSceneID) >= 1.0
				RETURN TRUE
			ENDIF
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Stops the sync scene
PROC STOP_SYNC_SCENE(INT ThisSynchScene)
	iThisLocalSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(ThisSynchScene)
	IF iThisLocalSceneID != -1
		IF IS_SYNCHRONIZED_SCENE_RUNNING(iThisLocalSceneID)	
			NETWORK_STOP_SYNCHRONISED_SCENE(iThisLocalSceneID)
			NET_PRINT_TIME() NET_PRINT("     ---------->     HOLD UP - STOP_SYNC_SCENE      <----------     ") NET_NL()
		ENDIF
	ENDIF
ENDPROC

FUNC FLOAT GET_SYNC_SCENE_PHASE(INT ThisSynchScene)
	iThisLocalSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(ThisSynchScene)
	 PRINTLN("    ---------->     GET_SYNC_SCENE_PHASE     iThisLocalSceneID ",iThisLocalSceneID)
	IF iThisLocalSceneID != -1
		IF IS_SYNCHRONIZED_SCENE_RUNNING(iThisLocalSceneID)
			RETURN GET_SYNCHRONIZED_SCENE_PHASE(iThisLocalSceneID)		
		ENDIF
	ENDIF
	return 0.0
ENDFUNC

//PURPOSE: Returns TRUE if the sync scene is past the specified phase
FUNC BOOL IS_PAST_SYNC_SCENE_PHASE(INT ThisSynchScene,FLOAT fPhase)
	iThisLocalSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(ThisSynchScene)
	 PRINTLN("    ---------->     IS_PAST_SYNC_SCENE_PHASE     iThisLocalSceneID ",iThisLocalSceneID)
	IF iThisLocalSceneID != -1
		IF IS_SYNCHRONIZED_SCENE_RUNNING(iThisLocalSceneID)
			PRINTLN("    ---------->     GET_SYNCHRONIZED_SCENE_PHASE      ",GET_SYNCHRONIZED_SCENE_PHASE(iThisLocalSceneID))
			IF GET_SYNCHRONIZED_SCENE_PHASE(iThisLocalSceneID) >= fPhase
				PRINTLN("    ---------->     GET_SYNCHRONIZED_SCENE_PHASE    true  ")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE 
ENDFUNC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: HEADING MATHS FUNCTIONS !
//
//************************************************************************************************************************************************************



FUNC FLOAT MOD_FLOAT( FLOAT fVal, FLOAT fMod )
	#IF IS_DEBUG_BUILD
	IF( fMod < 0.0 )
		SCRIPT_ASSERT( "MOD_FLOAT - fMod must be >= 0.0" )
		RETURN fVal
	ENDIF
	#ENDIF

	IF( fVal >= 0.0 )
		IF( fVal <= fMod )
			RETURN fVal
		ELSE // > fMod
			RETURN MOD_FLOAT( fVal - fMod, fMod )
		ENDIF
	ELSE // < 0.0
		RETURN MOD_FLOAT( fVal + fMod, fMod )
	ENDIF
ENDFUNC

PROC GET_NORMALISED_DELTA_HEADINGS( FLOAT &fHeadingOne, FLOAT &fHeadingTwo )
	fHeadingOne = MOD_FLOAT( fHeadingOne, 360.0 )
	fHeadingTwo = MOD_FLOAT( fHeadingTwo, 360.0 )
	
	#IF IS_DEBUG_BUILD
	IF( fHeadingOne < 0.0 )
		ASSERTLN( "GET_NORMALISED_DELTA_HEADINGS fHeadingOne < 0.0! fHeadingOne:", fHeadingOne )
	ENDIF
	IF( fHeadingTwo < 0.0 )
		ASSERTLN( "GET_NORMALISED_DELTA_HEADINGS fHeadingTwo < 0.0! fHeadingTwo:", fHeadingTwo )
	ENDIF
	#ENDIF
	
	IF( ABSF( fHeadingTwo - fHeadingOne ) > 180.0 )
		IF( fHeadingTwo - fHeadingOne <= 0.0 )
			fHeadingTwo += 360.0
		ELSE
			fHeadingOne += 360.0
		ENDIF
	ENDIF
ENDPROC

FUNC FLOAT GET_DELTA_HEADING( FLOAT fHeadingOne, FLOAT fHeadingTwo )
	GET_NORMALISED_DELTA_HEADINGS( fHeadingOne, fHeadingTwo )

	RETURN fHeadingTwo - fHeadingOne
ENDFUNC

FUNC FLOAT GET_ABSOLUTE_DELTA_HEADING( FLOAT fHeadingOne, FLOAT fHeadingTwo )
	GET_NORMALISED_DELTA_HEADINGS( fHeadingOne, fHeadingTwo )

	IF( fHeadingOne > fHeadingTwo )
		RETURN ABSF( fHeadingOne - fHeadingTwo )
	ELSE	
		RETURN ABSF( fHeadingTwo - fHeadingOne )
	ENDIF
ENDFUNC

FUNC VECTOR GET_COORD_ALONG_HEADING_FROM_ENTITY( ENTITY_INDEX entity, FLOAT fHeading )
	VECTOR vEntityPos = GET_ENTITY_COORDS( entity, FALSE )
	VECTOR vResult = <<0.0, 1.0, 0.0>>
	RotateVec( vResult, <<0.0, 0.0, fHeading>> )
	RETURN vEntityPos + vResult
ENDFUNC

FUNC FLOAT GET_NORMALISED_HEADING( FLOAT fHeading )
	fHeading = fHeading % 360.0
	IF( fHeading < 0.0 )
		fHeading += 360.0
	ENDIF
	RETURN fHeading
ENDFUNC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: PERSONAL VEHICLE DISABLING !
// 
//************************************************************************************************************************************************************



FUNC BOOL IS_PV_DISABLED()
	
	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	IF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(3) AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_DisablePersonalVehOnCheckpoint_3)
		RETURN TRUE
	ENDIF
	
	IF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(2) AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciOptionsBS17_DisablePersonalVehOnCheckpoint_2)
		RETURN TRUE
	ENDIF
	
	IF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(1) AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciDISABLE_PERSONAL_VEH_CHECKPOINT_1)
		RETURN TRUE
	ENDIF
	
	IF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(0) AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciDISABLE_PERSONAL_VEH_CHECKPOINT_0)
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciDISABLE_PERSONAL_VEH)
	OR (iRule < FMMC_MAX_RULES AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThree[iRule], ciBS_RULE3_DISABLE_PV_SPAWNING))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC PED_INDEX GET_SCARY_PED_ON_TEAM_ZERO()
	INT tempScaryPedInt = GET_NEAREST_PLAYER_TO_ENTITY_ON_TEAM(LocalPlayerPed, 0)
	PLAYER_INDEX tempScaryPedIndex = INT_TO_PLAYERINDEX( tempScaryPedInt )
	PED_INDEX tempScaryPed = GET_PLAYER_PED( tempScaryPedIndex )
	
	RETURN tempScaryPed
ENDFUNC

PROC MC_SET_PED_AND_TRAFFIC_DENSITIES(INT& iHandle, BOOL& bVehGen, FLOAT fPedDensity, FLOAT fTrafficDensity)
	BOOL bLocalOnly = TRUE
	VECTOR vMinWorldLimitCoords = <<-16000, -16000, -1700>>
	VECTOR vMaxWorldLimitCoords = <<16000, 16000, 2700>>
	
	IF iHandle = -1 
		//Can't use DOES_POP_MULTIPLIER_AREA_EXIST(iHandle) here as -1 is an invalid index for it.
		iHandle = ADD_POP_MULTIPLIER_AREA(vMinWorldLimitCoords, vMaxWorldLimitCoords, fPedDensity, fTrafficDensity, bLocalOnly)
	ENDIF
	
	IF fPedDensity = 0 AND fTrafficDensity = 0
		IF NOT bVehGen
			SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(vMinWorldLimitCoords, vMaxWorldLimitCoords, FALSE, FALSE)
			PRINTLN("[POP] [MC_SET_PED_AND_TRAFFIC_DENSITIES] SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA = FALSE")
			bVehGen = TRUE
			
			CLEAR_AREA_OF_PROJECTILES((<<0.0, 0.0, 0.0>>), 10000, FALSE)
			CLEAR_AREA_OF_VEHICLES((<<0.0, 0.0, 0.0>>), 10000, TRUE)
			CLEAR_AREA_OF_PEDS((<<0.0, 0.0, 0.0>>), 10000, TRUE)
			
			// Below is still causing issues, so trying to call the above all the time instead of just in interiors. (Engine Code Suggestion)... If we get no bugs about this, remove this commented section...
			// url:bugstar:4029952 - Slashers V - Multiple Objects fade away then fade back in during the intro camera.
			// url:bugstar:4163805 - Hard Target VII - Barrier prop flickers in and out during the intro cam transition into a round. "prop_barier_conc_02a at -40.957,-2444.443,4.996"
			/*IF NOT IS_PED_INJURED(localPlayerPed)
			AND GET_INTERIOR_FROM_ENTITY(localPlayerPed) != NULL
				CLEAR_AREA_OF_PROJECTILES((<<0.0, 0.0, 0.0>>), 10000, FALSE)
				CLEAR_AREA_OF_VEHICLES((<<0.0, 0.0, 0.0>>), 10000, TRUE)
				CLEAR_AREA_OF_PEDS((<<0.0, 0.0, 0.0>>), 10000, TRUE)
			ELSE
				CLEAR_AREA((<<0.0, 0.0, 0.0>>), 10000, FALSE, TRUE, FALSE, FALSE)
			ENDIF*/
		ENDIF
	ENDIF
		
	PRINTLN("[POP] [MC_SET_PED_AND_TRAFFIC_DENSITIES] fPedDensity = ", fPedDensity, "  fTrafficDensity = ", fTrafficDensity, " iHandle = ", iHandle)
	
ENDPROC

PROC MC_CLEAR_PED_AND_TRAFFIC_DENSITIES(INT& iHandle, BOOL& bVehGen)
	BOOL bLocalOnly = TRUE
	VECTOR vMinWorldLimitCoords = <<-16000, -16000, -1700>>
	VECTOR vMaxWorldLimitCoords = <<16000, 16000, 2700>>
	
	IF iHandle > -1
		//Added to stop assert - 2116964 - No area registered for index 0 - either not registered or already freed up?
		IF DOES_POP_MULTIPLIER_AREA_EXIST(iHandle)
			REMOVE_POP_MULTIPLIER_AREA(iHandle, bLocalOnly)
		ENDIF
		iHandle = -1
	ENDIF
	
	IF bVehGen
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(vMinWorldLimitCoords,vMaxWorldLimitCoords,TRUE,FALSE)
		SET_ROADS_BACK_TO_ORIGINAL(vMinWorldLimitCoords,vMaxWorldLimitCoords, (NOT bLocalOnly))
		bVehGen = FALSE
	ENDIF
	
	PRINTLN("[POP] [MC_CLEAR_PED_AND_TRAFFIC_DENSITIES] iHandle = ", iHandle)
	
ENDPROC

//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: RESPAWN STATE CHECKS

//************************************************************************************************************************************************************

FUNC PlayerRespawnBackAtStartState GET_PLAYER_RESPAWN_BACK_AT_START_STATE(INT iPart)
	RETURN MC_playerBD[iPart].ePlayerRespawnBackAtStartState
ENDFUNC

PROC SET_PLAYER_RESPAWN_BACK_AT_START_STATE(PlayerRespawnBackAtStartState eState)
	MC_playerBD[iLocalPart].ePlayerRespawnBackAtStartState = eState
	PRINTLN("[MMacK][Restart] SET_PLAYER_RESPAWN_BACK_AT_START_STATE - Set player state to ", eState)
	DEBUG_PRINTCALLSTACK()
ENDPROC

FUNC BOOL IS_PLAYER_IN_RESPAWN_BACK_AT_START_LOGIC(INT iPart)
	RETURN ((GET_PLAYER_RESPAWN_BACK_AT_START_STATE(iPart) != PRBSS_PlayingNormally) AND (GET_PLAYER_RESPAWN_BACK_AT_START_STATE(iPart) != PRBSS_Invalid))
ENDFUNC

//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: GLOBAL HUD STATE SETTERS

//************************************************************************************************************************************************************

//Sets WASTED shard text to be changed
PROC CHANGE_WASTED_SHARD_TEXT(BOOL bSet = FALSE)
	IF bSet
		PRINTLN("[KH] - WASTED TEXT CHANGED")
	ELSE
		PRINTLN("[KH] - WASTED TEXT CHANGED BACK TO ORIGINAL")
	ENDIF
	g_ChangeWastedShardText = bSet
ENDPROC

//Force player off of a warning screen - ONLY NEED TO CALL THIS ONCE
PROC FORCE_PLAYER_OFF_WARNING_SCREEN(BOOL bSet = FALSE)
	IF bSet
		PRINTLN("[KH] FORCE_PLAYER_OFF_WARNING_SCREEN - Forcing player off warning screen")
	ELSE
		PRINTLN("[KH] FORCE_PLAYER_OFF_WARNING_SCREEN - Calling this as false does nothing as g_bForceCloseWarningScreen is set to FALSE as soon as it has been used")
	ENDIF
	g_bForceCloseWarningScreen = bSet
ENDPROC

//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: MODE SPECIFIC FUNCTIONS

//************************************************************************************************************************************************************

FUNC BOOL CAN_I_SWAN_DIVE(INT iTeam, INT iRule, BOOL bOnlyCheckAnimLoc = FALSE)
	IF NOT bOnlyCheckAnimLoc
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciENABLE_DIVE_SCORE_ANIM)
		OR IS_BIT_SET(iLocalBoolCheck16, LBOOL16_IS_DIVE_SCORE_ANIM_IN_PROGRESS)
		OR MC_playerBD[iLocalPart].iObjCarryCount = 0
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF NOT IS_ENTITY_PLAYING_ANIM(LocalPlayerPed, "ANIM@SPORTS@BALLGAME@HANDBALL@", "BALL_GET_UP", ANIM_SCRIPT)
	AND NOT IS_ENTITY_PLAYING_ANIM(LocalPlayerPed, "ANIM@SPORTS@BALLGAME@HANDBALL@", "BALL_RSTOP_R_SLIDE", ANIM_SCRIPT)
	
		IF IS_ENTITY_IN_ANGLED_AREA(LocalPlayerPed, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vDropOff[iRule],
									g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vDropOff2[iRule],		//Only half of this const on either side of the drop off, that's why we double it
									g_FMMC_STRUCT.sFMMCEndConditions[iTeam].fDropOffRadius[iRule] + (ciDIVE_SCORE_DIVEABLE_AREA*2),
									FALSE )
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL ARE_ANY_PLAYERS_IN_RESTART_LOGIC()

	INT iParticipant
	PARTICIPANT_INDEX piTemp
	PLAYER_INDEX piPlayer

	REPEAT GET_MAXIMUM_PLAYERS_ON_MISSION() iParticipant
		piTemp = INT_TO_PARTICIPANTINDEX(iParticipant)
		
		IF NETWORK_IS_PARTICIPANT_ACTIVE(piTemp)
			piPlayer = NETWORK_GET_PLAYER_INDEX(piTemp)

			IF IS_NET_PLAYER_OK(piPlayer, FALSE)
			AND (NOT(IS_PLAYER_SPECTATING(piPlayer)) OR (IS_BIT_SET(MC_playerBD[iParticipant].iClientBitset, PBBOOL_HEIST_SPECTATOR) OR IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN)))
			AND (GET_PLAYER_TEAM(piPlayer) != -1)
			AND IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_TEAM_0_SHOULD_RESPAWN_BACK_AT_START_NOW + MC_playerBD[iParticipant].iTeam)
				IF GET_PLAYER_RESPAWN_BACK_AT_START_STATE(iParticipant) != PRBSS_PlayingNormally
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT

	RETURN FALSE
	
ENDFUNC

PROC PROCESS_TEAM_VEHICLE_CLEAN_UP()
	IF IS_BIT_SET(iLocalBoolCheck16, LBOOL16_VEHICLE_MARKED_FOR_CLEAN_UP)
		PRINTLN("[KH] PROCESS_TEAM_VEHICLE_CLEAN_UP - Vehicle Marked for cleanup!")	
		IF HAS_NET_TIMER_STARTED(vehicleCleanUpTimer)	
			INT iTime = ciCLEAN_UP_VEHICLE_TIME			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwenty, ciOptionsBS20_Enable_Turn_Rotor_Tag_Team)
			OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyTwo, ciOptionsBS22_EnableGameMastersMode)
				iTime = 2000
			ENDIF
			
			IF HAS_NET_TIMER_EXPIRED(vehicleCleanUpTimer, iTime)
			AND (NOT IS_PLAYER_RESPAWNING(PLAYER_ID()) OR IS_LOCAL_PLAYER_ANY_SPECTATOR())
			OR ARE_ANY_PLAYERS_IN_RESTART_LOGIC()
				IF NETWORK_DOES_NETWORK_ID_EXIST(netVehicleToCleanUp)
					IF NOT NETWORK_HAS_CONTROL_OF_NETWORK_ID(netVehicleToCleanUp)
						IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iTeamBitSet3, ciBS3_ENABLE_MULTIPLE_PLAYER_SAME_VEHICLE_RESPAWN)
						OR (IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iTeamBitSet3, ciBS3_ENABLE_MULTIPLE_PLAYER_SAME_VEHICLE_RESPAWN) 
						AND GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(GET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER()) = iLocalPart)
							NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(netVehicleToCleanUp)
							IF IS_VEHICLE_EMPTY(NET_TO_VEH(netVehicleToCleanUp))
								PRINTLN("[KH] PROCESS_TEAM_VEHICLE_CLEAN_UP - Requesting control of Net ID netVehicleToCleanUp")
								NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(netVehicleToCleanUp)
							ELSE
								NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(netVehicleToCleanUp)
								PRINTLN("[KH] PROCESS_TEAM_VEHICLE_CLEAN_UP - Could not request control of the vehicle as it is occupied (because another player has taken it)")
								PRINTLN("[KH] PROCESS_TEAM_VEHICLE_CLEAN_UP - Marking netVehicleToCleanUp as no longer needed")		
								IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciCLEAR_UP_VEHICLE_ON_DEATH)
									IF IS_VEHICLE_EMPTY(NET_TO_VEH(netVehicleToCleanUp))
										SET_ENTITY_COLLISION( NET_TO_VEH( netVehicleToCleanUp ), FALSE ) 
										SET_ENTITY_VISIBLE( NET_TO_VEH( netVehicleToCleanUp ), FALSE )
										DELETE_NET_ID(netVehicleToCleanUp)
									ELSE
										PRINTLN("[KH] PROCESS_TEAM_VEHICLE_CLEAN_UP - NETWORK_FADE_OUT_ENTITY Vehicle is not empty (1)")
									ENDIF
								ELSE
									CLEANUP_NET_ID(netVehicleToCleanUp)
								ENDIF
								CLEAR_BIT(iLocalBoolCheck16, LBOOL16_VEHICLE_MARKED_FOR_CLEAN_UP)
								RESET_NET_TIMER(vehicleCleanUpTimer)
							ENDIF
						ENDIF
					ELSE
						IF IS_VEHICLE_EMPTY(NET_TO_VEH(netVehicleToCleanUp), DEFAULT, DEFAULT, DEFAULT, TRUE)
							IF NOT IS_ENTITY_VISIBLE_TO_SCRIPT(NET_TO_VEH(netVehicleToCleanUp))		
								PRINTLN("[KH] PROCESS_TEAM_VEHICLE_CLEAN_UP - Deleting netVehicleToCleanUp")	
								SET_ENTITY_COLLISION( NET_TO_VEH( netVehicleToCleanUp ), FALSE ) 
								SET_ENTITY_VISIBLE( NET_TO_VEH( netVehicleToCleanUp ), FALSE )
								DELETE_NET_ID(netVehicleToCleanUp)
								CLEAR_BIT(iLocalBoolCheck16, LBOOL16_VEHICLE_MARKED_FOR_CLEAN_UP)
								RESET_NET_TIMER(vehicleCleanUpTimer)
							ELSE
								PRINTLN("[KH] PROCESS_TEAM_VEHICLE_CLEAN_UP - NETWORK_FADE_OUT_ENTITY netVehicleToCleanUp")	
								NETWORK_FADE_OUT_ENTITY(NET_TO_VEH(netVehicleToCleanUp), TRUE, TRUE)
							ENDIF
						ELSE
							PRINTLN("[KH] PROCESS_TEAM_VEHICLE_CLEAN_UP - NETWORK_FADE_OUT_ENTITY Vehicle is not empty (2)")
						ENDIF
					ENDIF
				ELSE
					PRINTLN("[KH] PROCESS_TEAM_VEHICLE_CLEAN_UP - netVehicleToCleanUp does not exist")
					CLEAR_BIT(iLocalBoolCheck16, LBOOL16_VEHICLE_MARKED_FOR_CLEAN_UP)
					RESET_NET_TIMER(vehicleCleanUpTimer)
				ENDIF
			ELSE
				PRINTLN("[KH] PROCESS_TEAM_VEHICLE_CLEAN_UP - Waiting for Timer, or we are respawning.")
			ENDIF
		ELSE
			PRINTLN("[KH] PROCESS_TEAM_VEHICLE_CLEAN_UP - Timer not started.")
		ENDIF
	ENDIF
ENDPROC

FUNC STRING GET_SWAN_CAM_AS_STRING()
	SWITCH currentSwanDiveCamState
	CASE SDSCS_WaitingToPlay				RETURN "SDSCS_WaitingToPlay"			
	CASE SDSCS_FindCameraSlot				RETURN "SDSCS_FindCameraSlot"			
	CASE SDSCS_InitCamera					RETURN "SDSCS_InitCamera"				
	CASE SDSCS_WaitForDiveFinish			RETURN "SDSCS_WaitForDiveFinish"		
	CASE SDSCS_Reset						RETURN "SDSCS_Reset"
												   
	CASE SDSCS_FindRemoteCamSlot			RETURN "SDSCS_FindRemoteCamSlot"		
	CASE SDSCS_InitNonScorerCam				RETURN "SDSCS_InitNonScorerCam"			
	CASE SDSCS_WaitForNonScoreCamFinish		RETURN "SDSCS_WaitForNonScoreCamFinish"

	ENDSWITCH
	
	RETURN "INVALID STRING"
ENDFUNC

FUNC STRING GET_SWAN_DIVE_AS_STRING()
	SWITCH currentSwanDiveState
	CASE PSDAS_WaitingToPlay			RETURN "PSDAS_WaitingToPlay"
	CASE PSDAS_PlayDive					RETURN "PSDAS_PlayDive"
	CASE PSDAS_ValidatingDiveAnim		RETURN "PSDAS_ValidatingDiveAnim"
	CASE PSDAS_WaitingForDiveFinish		RETURN "PSDAS_WaitingForDiveFinish"
	CASE PSDAS_PlayGetUp				RETURN "PSDAS_PlayGetUp"
	CASE PSDAS_WaitingForGetUpFinish	RETURN "PSDAS_WaitingForGetUpFinish"
	CASE PSDAS_Reset					RETURN "PSDAS_Reset"
	
	
	ENDSWITCH
	
	RETURN "INVALID STRING"
ENDFUNC

FUNC STRING GET_TR_ROUND_STATE_AS_STRING()
	SWITCH MC_playerBD[iLocalPart].ePlayerTinyRacersRoundState
	CASE TR_Racing				RETURN "TR_Racing"
	CASE TR_Init				RETURN "TR_Init"
	CASE TR_LastManTimer		RETURN "TR_LastManTimer"
	CASE TR_MoveLastPlayer		RETURN "TR_MoveLastPlayer"
	CASE TR_Respawn				RETURN "TR_Respawn"
	CASE TR_Warp				RETURN "TR_Warp"
	CASE TR_Sync				RETURN "TR_Sync"
	CASE TR_321					RETURN "TR_321"
	CASE TR_Reset				RETURN "TR_Reset"
	CASE TR_Invalid				RETURN "TR_Invalid"

	ENDSWITCH
	
	RETURN "INVALID STRING"
ENDFUNC

FUNC INT GET_INT_FOR_PP_ANNOUNCER_ROOT(STRING sRoot)
	
//#### PRE INTRO LINES	
	IF 	 ARE_STRINGS_EQUAL(sRoot, "EXPOW_NEUT1")	RETURN ciPP_PRE_INTRO_ROOT_NEUT1
	ELIF ARE_STRINGS_EQUAL(sRoot, "EXPOW_NEUT2")	RETURN ciPP_PRE_INTRO_ROOT_NEUT2
	ELIF ARE_STRINGS_EQUAL(sRoot, "EXPOW_NEUT3")	RETURN ciPP_PRE_INTRO_ROOT_NEUT3
	ELIF ARE_STRINGS_EQUAL(sRoot, "EXPOW_NEUT4")	RETURN ciPP_PRE_INTRO_ROOT_NEUT4
	ELIF ARE_STRINGS_EQUAL(sRoot, "EXPOW_NEUT5")	RETURN ciPP_PRE_INTRO_ROOT_NEUT5
	ELIF ARE_STRINGS_EQUAL(sRoot, "EXPOW_NEUT6")	RETURN ciPP_PRE_INTRO_ROOT_NEUT6
	ELIF ARE_STRINGS_EQUAL(sRoot, "EXPOW_NEUT7")	RETURN ciPP_PRE_INTRO_ROOT_NEUT7
	ELIF ARE_STRINGS_EQUAL(sRoot, "EXPOW_NEUT8")	RETURN ciPP_PRE_INTRO_ROOT_NEUT8
	ELIF ARE_STRINGS_EQUAL(sRoot, "EXPOW_NEUT9")	RETURN ciPP_PRE_INTRO_ROOT_NEUT9
	ELIF ARE_STRINGS_EQUAL(sRoot, "EXPOW_NEUT10")	RETURN ciPP_PRE_INTRO_ROOT_NEUT10

//#### TEAM INTRO LINES			  
	ELIF ARE_STRINGS_EQUAL(sRoot, "EXPOW_NCVB")		RETURN ciPP_TEAM_INTRO_ROOT_NCVB 	//Cocks vs Boars
	ELIF ARE_STRINGS_EQUAL(sRoot, "EXPOW_NBVC")		RETURN ciPP_TEAM_INTRO_ROOT_NBVC	//Boars vs Cocks
	ELIF ARE_STRINGS_EQUAL(sRoot, "EXPOW_TVK")		RETURN ciPP_TEAM_INTRO_ROOT_TVK		//Toxic Pests vs Killer bugs
	ELIF ARE_STRINGS_EQUAL(sRoot, "EXPOW_KVT")		RETURN ciPP_TEAM_INTRO_ROOT_KVT		//Killer bugs vs Toxic Pests
	ELIF ARE_STRINGS_EQUAL(sRoot, "EXPOW_RVS")		RETURN ciPP_TEAM_INTRO_ROOT_RVS		//Rainbows vs Shades
	ELIF ARE_STRINGS_EQUAL(sRoot, "EXPOW_SVR")		RETURN ciPP_TEAM_INTRO_ROOT_SVR		//Shades vs Rainbows
	ELIF ARE_STRINGS_EQUAL(sRoot, "EXPOW_MVA")		RETURN ciPP_TEAM_INTRO_ROOT_MVA		//Munchies vs Animals
	ELIF ARE_STRINGS_EQUAL(sRoot, "EXPOW_AVM")		RETURN ciPP_TEAM_INTRO_ROOT_AVM		//Animals vs Munchies
	
	ELIF ARE_STRINGS_EQUAL(sRoot, "EXPOW_STARG1")	RETURN ciPP_TEAM_INTRO_ROOT_STARG1	//General announcements
	ELIF ARE_STRINGS_EQUAL(sRoot, "EXPOW_STARG2")	RETURN ciPP_TEAM_INTRO_ROOT_STARG2
	ELIF ARE_STRINGS_EQUAL(sRoot, "EXPOW_STARG3")	RETURN ciPP_TEAM_INTRO_ROOT_STARG3
	ELIF ARE_STRINGS_EQUAL(sRoot, "EXPOW_STARG4")	RETURN ciPP_TEAM_INTRO_ROOT_STARG4
	ELIF ARE_STRINGS_EQUAL(sRoot, "EXPOW_STARG5")	RETURN ciPP_TEAM_INTRO_ROOT_STARG5

//#### SUDDEN DEATH LINES
	ELIF ARE_STRINGS_EQUAL(sRoot, "EXPOW_NSUD1")	RETURN ciPP_SUDDEN_DEATH_ROOT_1
	ELIF ARE_STRINGS_EQUAL(sRoot, "EXPOW_NSUD2")	RETURN ciPP_SUDDEN_DEATH_ROOT_2
	ELIF ARE_STRINGS_EQUAL(sRoot, "EXPOW_NSUD3")	RETURN ciPP_SUDDEN_DEATH_ROOT_3
	ELIF ARE_STRINGS_EQUAL(sRoot, "EXPOW_NSUD4")	RETURN ciPP_SUDDEN_DEATH_ROOT_4

//#### WINNER ANNOUNCER LINES
	ELIF ARE_STRINGS_EQUAL(sRoot, "EXPOW_NCOCK1")	RETURN ciPP_WINNER_ROOT_COCKS_1	
	ELIF ARE_STRINGS_EQUAL(sRoot, "EXPOW_NCOCK2")	RETURN ciPP_WINNER_ROOT_COCKS_2	
	ELIF ARE_STRINGS_EQUAL(sRoot, "EXPOW_NCOCK3")	RETURN ciPP_WINNER_ROOT_COCKS_3
											   
	ELIF ARE_STRINGS_EQUAL(sRoot, "EXPOW_NBOAR1")	RETURN ciPP_WINNER_ROOT_BOARS_1	
	ELIF ARE_STRINGS_EQUAL(sRoot, "EXPOW_NBOAR2")	RETURN ciPP_WINNER_ROOT_BOARS_2	
	ELIF ARE_STRINGS_EQUAL(sRoot, "EXPOW_NBOAR3")	RETURN ciPP_WINNER_ROOT_BOARS_3
											   
	ELIF ARE_STRINGS_EQUAL(sRoot, "EXPOW_TOXIC1")	RETURN ciPP_WINNER_ROOT_TOXIC_1
	ELIF ARE_STRINGS_EQUAL(sRoot, "EXPOW_TOXIC2")	RETURN ciPP_WINNER_ROOT_TOXIC_2
	ELIF ARE_STRINGS_EQUAL(sRoot, "EXPOW_TOXIC3")	RETURN ciPP_WINNER_ROOT_TOXIC_3
											   
	ELIF ARE_STRINGS_EQUAL(sRoot, "EXPOW_KILL1")	RETURN ciPP_WINNER_ROOT_KILLER_1
	ELIF ARE_STRINGS_EQUAL(sRoot, "EXPOW_KILL2")	RETURN ciPP_WINNER_ROOT_KILLER_2
	ELIF ARE_STRINGS_EQUAL(sRoot, "EXPOW_KILL3")	RETURN ciPP_WINNER_ROOT_KILLER_3
											   
	ELIF ARE_STRINGS_EQUAL(sRoot, "EXPOW_MUNCH1")	RETURN ciPP_WINNER_ROOT_MUNCHIES_1
	ELIF ARE_STRINGS_EQUAL(sRoot, "EXPOW_MUNCH2")	RETURN ciPP_WINNER_ROOT_MUNCHIES_2
	ELIF ARE_STRINGS_EQUAL(sRoot, "EXPOW_MUNCH3")	RETURN ciPP_WINNER_ROOT_MUNCHIES_3
											   
	ELIF ARE_STRINGS_EQUAL(sRoot, "EXPOW_ANIM1")	RETURN ciPP_WINNER_ROOT_ANIMALS_1
	ELIF ARE_STRINGS_EQUAL(sRoot, "EXPOW_ANIM2")	RETURN ciPP_WINNER_ROOT_ANIMALS_2
	ELIF ARE_STRINGS_EQUAL(sRoot, "EXPOW_ANIM3")	RETURN ciPP_WINNER_ROOT_ANIMALS_3
											   
	ELIF ARE_STRINGS_EQUAL(sRoot, "EXPOW_RAIN1")	RETURN ciPP_WINNER_ROOT_RAINBOW_1
	ELIF ARE_STRINGS_EQUAL(sRoot, "EXPOW_RAIN2")	RETURN ciPP_WINNER_ROOT_RAINBOW_2
	ELIF ARE_STRINGS_EQUAL(sRoot, "EXPOW_RAIN3")	RETURN ciPP_WINNER_ROOT_RAINBOW_3
											   
	ELIF ARE_STRINGS_EQUAL(sRoot, "EXPOW_SHADE1")	RETURN ciPP_WINNER_ROOT_SHADE_1
	ELIF ARE_STRINGS_EQUAL(sRoot, "EXPOW_SHADE2")	RETURN ciPP_WINNER_ROOT_SHADE_2
	ELIF ARE_STRINGS_EQUAL(sRoot, "EXPOW_SHADE3")	RETURN ciPP_WINNER_ROOT_SHADE_3
											   
	ELIF ARE_STRINGS_EQUAL(sRoot, "EXPOW_GENW1")	RETURN ciPP_WINNER_ROOT_GENERAL_1
	ELIF ARE_STRINGS_EQUAL(sRoot, "EXPOW_GENW2")	RETURN ciPP_WINNER_ROOT_GENERAL_2
	ELIF ARE_STRINGS_EQUAL(sRoot, "EXPOW_GENW3")	RETURN ciPP_WINNER_ROOT_GENERAL_3
	ELIF ARE_STRINGS_EQUAL(sRoot, "EXPOW_GENW4")	RETURN ciPP_WINNER_ROOT_GENERAL_4
	ELIF ARE_STRINGS_EQUAL(sRoot, "EXPOW_GENW5")	RETURN ciPP_WINNER_ROOT_GENERAL_5
	
//#### TIE ANNOUNCER LINES
	ELIF ARE_STRINGS_EQUAL(sRoot, "EXPOW_NTIE1")	RETURN ciPP_TIE_ROOT_1	
	ELIF ARE_STRINGS_EQUAL(sRoot, "EXPOW_NTIE2")	RETURN ciPP_TIE_ROOT_2	
	ELIF ARE_STRINGS_EQUAL(sRoot, "EXPOW_NTIE3")	RETURN ciPP_TIE_ROOT_3
	ELIF ARE_STRINGS_EQUAL(sRoot, "EXPOW_NTIE4")	RETURN ciPP_TIE_ROOT_4
	ELIF ARE_STRINGS_EQUAL(sRoot, "EXPOW_NTIE5")	RETURN ciPP_TIE_ROOT_5

//#### EXIT ANNOUNCER LINES
	ELIF ARE_STRINGS_EQUAL(sRoot, "EXPOW_NEXIT1")	RETURN ciPP_EXIT_ROOT_1	
	ELIF ARE_STRINGS_EQUAL(sRoot, "EXPOW_NEXIT2")	RETURN ciPP_EXIT_ROOT_2	
	ELIF ARE_STRINGS_EQUAL(sRoot, "EXPOW_NEXIT3")	RETURN ciPP_EXIT_ROOT_3	
	ELIF ARE_STRINGS_EQUAL(sRoot, "EXPOW_NEXIT4")	RETURN ciPP_EXIT_ROOT_4	
	ELIF ARE_STRINGS_EQUAL(sRoot, "EXPOW_NEXIT5")	RETURN ciPP_EXIT_ROOT_5	
	ELIF ARE_STRINGS_EQUAL(sRoot, "EXPOW_NEXIT6")	RETURN ciPP_EXIT_ROOT_6	
	ELIF ARE_STRINGS_EQUAL(sRoot, "EXPOW_NEXIT7")	RETURN ciPP_EXIT_ROOT_7	
	ELIF ARE_STRINGS_EQUAL(sRoot, "EXPOW_NEXIT8")	RETURN ciPP_EXIT_ROOT_8	
	ELIF ARE_STRINGS_EQUAL(sRoot, "EXPOW_NEXIT9")	RETURN ciPP_EXIT_ROOT_9	
	ELIF ARE_STRINGS_EQUAL(sRoot, "EXPOW_NEXI10")	RETURN ciPP_EXIT_ROOT_10	
	ELIF ARE_STRINGS_EQUAL(sRoot, "EXPOW_NEXI11")	RETURN ciPP_EXIT_ROOT_11	
	ELIF ARE_STRINGS_EQUAL(sRoot, "EXPOW_NEXI12")	RETURN ciPP_EXIT_ROOT_12	
	ELIF ARE_STRINGS_EQUAL(sRoot, "EXPOW_NEXI13")	RETURN ciPP_EXIT_ROOT_13

	ENDIF
	
	RETURN -1
ENDFUNC

FUNC STRING GET_STRING_FOR_PP_ANNOUNCER_ROOT(INT iRootNum)
	SWITCH iRootNum
//#### PRE INTRO LINES	
	CASE ciPP_PRE_INTRO_ROOT_NEUT1					RETURN "EXPOW_NEUT1"
	CASE ciPP_PRE_INTRO_ROOT_NEUT2					RETURN "EXPOW_NEUT2"
	CASE ciPP_PRE_INTRO_ROOT_NEUT3					RETURN "EXPOW_NEUT3"
	CASE ciPP_PRE_INTRO_ROOT_NEUT4					RETURN "EXPOW_NEUT4"
	CASE ciPP_PRE_INTRO_ROOT_NEUT5					RETURN "EXPOW_NEUT5"
	CASE ciPP_PRE_INTRO_ROOT_NEUT6					RETURN "EXPOW_NEUT6"
	CASE ciPP_PRE_INTRO_ROOT_NEUT7					RETURN "EXPOW_NEUT7"
	CASE ciPP_PRE_INTRO_ROOT_NEUT8					RETURN "EXPOW_NEUT8"
	CASE ciPP_PRE_INTRO_ROOT_NEUT9					RETURN "EXPOW_NEUT9"
	CASE ciPP_PRE_INTRO_ROOT_NEUT10					RETURN "EXPOW_NEUT10"
	
//#### TEAM INTRO LINES	
	CASE ciPP_TEAM_INTRO_ROOT_NCVB 					RETURN "EXPOW_NCVB"	
	CASE ciPP_TEAM_INTRO_ROOT_NBVC					RETURN "EXPOW_NBVC"	
	CASE ciPP_TEAM_INTRO_ROOT_TVK					RETURN "EXPOW_TVK"	
	CASE ciPP_TEAM_INTRO_ROOT_KVT					RETURN "EXPOW_KVT"	
	CASE ciPP_TEAM_INTRO_ROOT_RVS					RETURN "EXPOW_RVS"	
	CASE ciPP_TEAM_INTRO_ROOT_SVR					RETURN "EXPOW_SVR"	
	CASE ciPP_TEAM_INTRO_ROOT_MVA					RETURN "EXPOW_MVA"	
	CASE ciPP_TEAM_INTRO_ROOT_AVM					RETURN "EXPOW_AVM"
				   
	CASE ciPP_TEAM_INTRO_ROOT_STARG1				RETURN "EXPOW_STARG1"
	CASE ciPP_TEAM_INTRO_ROOT_STARG2				RETURN "EXPOW_STARG2"
	CASE ciPP_TEAM_INTRO_ROOT_STARG3				RETURN "EXPOW_STARG3"
	CASE ciPP_TEAM_INTRO_ROOT_STARG4				RETURN "EXPOW_STARG4"
	CASE ciPP_TEAM_INTRO_ROOT_STARG5				RETURN "EXPOW_STARG5"
	
//#### SUDDEN DEATH LINES	
	CASE ciPP_SUDDEN_DEATH_ROOT_1					RETURN "EXPOW_NSUD1"
	CASE ciPP_SUDDEN_DEATH_ROOT_2					RETURN "EXPOW_NSUD2"
	CASE ciPP_SUDDEN_DEATH_ROOT_3					RETURN "EXPOW_NSUD3"
	CASE ciPP_SUDDEN_DEATH_ROOT_4					RETURN "EXPOW_NSUD4"
	
//#### WINNER ANNOUNCER LINES	
	CASE ciPP_WINNER_ROOT_COCKS_1					RETURN "EXPOW_NCOCK1"
	CASE ciPP_WINNER_ROOT_COCKS_2					RETURN "EXPOW_NCOCK2"
	CASE ciPP_WINNER_ROOT_COCKS_3					RETURN "EXPOW_NCOCK3"
				   			   
	CASE ciPP_WINNER_ROOT_BOARS_1					RETURN "EXPOW_NBOAR1"
	CASE ciPP_WINNER_ROOT_BOARS_2					RETURN "EXPOW_NBOAR2"
	CASE ciPP_WINNER_ROOT_BOARS_3					RETURN "EXPOW_NBOAR3"
				   			   
	CASE ciPP_WINNER_ROOT_TOXIC_1					RETURN "EXPOW_TOXIC1"
	CASE ciPP_WINNER_ROOT_TOXIC_2					RETURN "EXPOW_TOXIC2"
	CASE ciPP_WINNER_ROOT_TOXIC_3					RETURN "EXPOW_TOXIC3"
				   			   
	CASE ciPP_WINNER_ROOT_KILLER_1					RETURN "EXPOW_KILL1"
	CASE ciPP_WINNER_ROOT_KILLER_2					RETURN "EXPOW_KILL2"
	CASE ciPP_WINNER_ROOT_KILLER_3					RETURN "EXPOW_KILL3"
				   			   
	CASE ciPP_WINNER_ROOT_MUNCHIES_1				RETURN "EXPOW_MUNCH1"
	CASE ciPP_WINNER_ROOT_MUNCHIES_2				RETURN "EXPOW_MUNCH2"
	CASE ciPP_WINNER_ROOT_MUNCHIES_3				RETURN "EXPOW_MUNCH3"
				   			   
	CASE ciPP_WINNER_ROOT_ANIMALS_1					RETURN "EXPOW_ANIM1"
	CASE ciPP_WINNER_ROOT_ANIMALS_2					RETURN "EXPOW_ANIM2"
	CASE ciPP_WINNER_ROOT_ANIMALS_3					RETURN "EXPOW_ANIM3"
				   			   
	CASE ciPP_WINNER_ROOT_RAINBOW_1					RETURN "EXPOW_RAIN1"
	CASE ciPP_WINNER_ROOT_RAINBOW_2					RETURN "EXPOW_RAIN2"
	CASE ciPP_WINNER_ROOT_RAINBOW_3					RETURN "EXPOW_RAIN3"
				   			   
	CASE ciPP_WINNER_ROOT_SHADE_1					RETURN "EXPOW_SHADE1"
	CASE ciPP_WINNER_ROOT_SHADE_2					RETURN "EXPOW_SHADE2"
	CASE ciPP_WINNER_ROOT_SHADE_3					RETURN "EXPOW_SHADE3"
				   			   
	CASE ciPP_WINNER_ROOT_GENERAL_1					RETURN "EXPOW_GENW1"
	CASE ciPP_WINNER_ROOT_GENERAL_2					RETURN "EXPOW_GENW2"
	CASE ciPP_WINNER_ROOT_GENERAL_3					RETURN "EXPOW_GENW3"
	CASE ciPP_WINNER_ROOT_GENERAL_4					RETURN "EXPOW_GENW4"
	CASE ciPP_WINNER_ROOT_GENERAL_5					RETURN "EXPOW_GENW5"

//#### TIE ANNOUNCER LINES
	CASE ciPP_TIE_ROOT_1							RETURN "EXPOW_NTIE1"
	CASE ciPP_TIE_ROOT_2							RETURN "EXPOW_NTIE2"
	CASE ciPP_TIE_ROOT_3							RETURN "EXPOW_NTIE3"
	CASE ciPP_TIE_ROOT_4							RETURN "EXPOW_NTIE4"
	CASE ciPP_TIE_ROOT_5							RETURN "EXPOW_NTIE5"
	
//#### EXIT ANNOUNCER LINES
	CASE ciPP_EXIT_ROOT_1							RETURN "EXPOW_NEXIT1"
	CASE ciPP_EXIT_ROOT_2							RETURN "EXPOW_NEXIT2"
	CASE ciPP_EXIT_ROOT_3							RETURN "EXPOW_NEXIT3"
	CASE ciPP_EXIT_ROOT_4							RETURN "EXPOW_NEXIT4"
	CASE ciPP_EXIT_ROOT_5							RETURN "EXPOW_NEXIT5"
	CASE ciPP_EXIT_ROOT_6							RETURN "EXPOW_NEXIT6"
	CASE ciPP_EXIT_ROOT_7							RETURN "EXPOW_NEXIT7"
	CASE ciPP_EXIT_ROOT_8							RETURN "EXPOW_NEXIT8"
	CASE ciPP_EXIT_ROOT_9							RETURN "EXPOW_NEXIT9"
	CASE ciPP_EXIT_ROOT_10							RETURN "EXPOW_NEXI10"
	CASE ciPP_EXIT_ROOT_11							RETURN "EXPOW_NEXI11"
	CASE ciPP_EXIT_ROOT_12							RETURN "EXPOW_NEXI12"
	CASE ciPP_EXIT_ROOT_13							RETURN "EXPOW_NEXI13"
	
	ENDSWITCH
	RETURN "INVALID_ROOT"
ENDFUNC

PROC SET_POWER_PLAY_SHARD_VISIBLE(BOOL bSet)
	IF bPowerPlayScaleFormActive
		PRINTLN("[KH][POWERPLAYUI] SET_POWER_PLAY_SHARD_VISIBLE - Setting power play shard visibility: ", bSet)
		BEGIN_SCALEFORM_MOVIE_METHOD (SF_PPH_MovieIndex, "SET_MESSAGE_VISIBILITY")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bSet)
		END_SCALEFORM_MOVIE_METHOD()
		
		IF bSet
			IF NOT IS_BIT_SET(iLocalBoolCheck17, LBOOL17_IS_POWER_PLAY_SHARD_VISIBLE)
				SET_BIT(iLocalBoolCheck17, LBOOL17_IS_POWER_PLAY_SHARD_VISIBLE)
			ENDIF
		ELSE
			IF IS_BIT_SET(iLocalBoolCheck17, LBOOL17_IS_POWER_PLAY_SHARD_VISIBLE)
				CLEAR_BIT(iLocalBoolCheck17, LBOOL17_IS_POWER_PLAY_SHARD_VISIBLE)
			ENDIF
		ENDIF		
	ELSE
		PRINTLN("[KH][POWERPLAYUI] SET_POWER_PLAY_SHARD_VISIBLE - bPowerPlayScaleFormActive is not true, Scaleform movie is not active!")
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks if the shard should be visible rather than if it is on screen at this point in time 
FUNC BOOL IS_POWER_PLAY_SHARD_VISIBLE()
	IF IS_BIT_SET(iLocalBoolCheck17, LBOOL17_IS_POWER_PLAY_SHARD_VISIBLE)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


PROC SET_VW_SHARD_VISIBLE(BOOL bSet)
	IF bVehicleVendettaScaleFormActive
		PRINTLN("[JS][VVHUD] SET_VW_SHARD_VISIBLE - Setting VW shard visibility: ", bSet)
		BEGIN_SCALEFORM_MOVIE_METHOD (SF_VVH_MovieIndex, "SET_MESSAGE_VISIBILITY")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bSet)
		END_SCALEFORM_MOVIE_METHOD()
		
		IF bSet
			IF NOT IS_BIT_SET(iLocalBoolCheck17, LBOOL17_IS_VW_SHARD_VISIBLE)
				SET_BIT(iLocalBoolCheck17, LBOOL17_IS_VW_SHARD_VISIBLE)
			ENDIF
		ELSE
			IF IS_BIT_SET(iLocalBoolCheck17, LBOOL17_IS_VW_SHARD_VISIBLE)
				CLEAR_BIT(iLocalBoolCheck17, LBOOL17_IS_VW_SHARD_VISIBLE)
			ENDIF
		ENDIF		
	ELSE
		PRINTLN("[JS][VVHUD] SET_VW_SHARD_VISIBLE - bVehicleVendettaScaleFormActive is not true, Scaleform movie is not active!")
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks if the shard should be visible rather than if it is on screen at this point in time 
FUNC BOOL IS_VW_SHARD_VISIBLE()
	IF IS_BIT_SET(iLocalBoolCheck17, LBOOL17_IS_VW_SHARD_VISIBLE)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC STRING GET_VEHICLE_WEAPON_STRING(INT iVehWep)
	SWITCH(iVehWep)
		CASE ciVEH_WEP_ROCKETS
			RETURN "ciVEH_WEP_ROCKETS"
		BREAK
		CASE ciVEH_WEP_SPEED_BOOST
			RETURN "ciVEH_WEP_SPEED_BOOST"
		BREAK
		CASE ciVEH_WEP_GHOST	
			RETURN "ciVEH_WEP_GHOST"
		BREAK
		CASE ciVEH_WEP_BEAST
			RETURN "ciVEH_WEP_BEAST"
		BREAK
		CASE ciVEH_WEP_PRON
			RETURN "ciVEH_WEP_PRON"
		BREAK
		CASE ciVEH_WEP_FORCE_ACCELERATE
			RETURN "ciVEH_WEP_FORCE_ACCELERATE"
		BREAK
		CASE ciVEH_WEP_FLIPPED_CONTROLS
			RETURN "ciVEH_WEP_FLIPPED_CONTROLS"
		BREAK
		CASE ciVEH_WEP_ZONED
			RETURN "ciVEH_WEP_ZONED"
		BREAK
		CASE ciVEH_WEP_BOUNCE
			RETURN "ciVEH_WEP_BOUNCE"
		BREAK
		CASE ciVEH_WEP_DETONATE
			RETURN "ciVEH_WEP_DETONATE"
		BREAK
		CASE ciVEH_WEP_BOMB
			RETURN "ciVEH_WEP_BOMB"
		BREAK
		CASE ciVEH_WEP_REPAIR
			RETURN "ciVEH_WEP_REPAIR"
		BREAK	
		CASE ciVEH_WEP_RANDOM
			RETURN "ciVEH_WEP_RANDOM"
		BREAK	
		CASE ciVEH_WEP_MACHINE_GUN
			RETURN "ciVEH_WEP_MACHINE_GUN"
		BREAK	
		CASE ciVEH_WEP_RANDOM_SPECIAL_VEH
			RETURN "ciVEH_WEP_RANDOM_SPECIAL_VEH"
		BREAK
		CASE ciVEH_WEP_RUINER_SPECIAL_VEH
			RETURN "ciVEH_WEP_RUINER_SPECIAL_VEH"
		BREAK
		CASE ciVEH_WEP_RAMP_SPECIAL_VEH
			RETURN "ciVEH_WEP_RAMP_SPECIAL_VEH"
		BREAK
		CASE ciVEH_WEP_BOMB_LENGTH
			RETURN "ciVEH_WEP_BOMB_LENGTH"
		BREAK
		CASE ciVEH_WEP_BOMB_MAX
			RETURN "ciVEH_WEP_BOMB_MAX"
		BREAK
		CASE ciVEH_WEP_EXTRA_LIFE
			RETURN "ciVEH_WEP_EXTRA_LIFE"
		BREAK
	ENDSWITCH
	
	RETURN "INVALID VEHICLE WEAPON!!"
ENDFUNC

//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: SUDDEN DEATH MODES

//************************************************************************************************************************************************************

FUNC BOOL IS_INSIDE_SPHERE(VECTOR playerCoord, VECTOR sphereCentre, FLOAT radius)
	IF GET_DISTANCE_BETWEEN_COORDS(playerCoord, sphereCentre) < radius
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

// PURPOSE:
//    Adjust the size of the area from fStartSize to fEndSize over fTime ms
//FUNC BOOL ADJUST_BLIP_RADIUS_OVER_TIME(FLOAT fStartSize, FLOAT fEndSize, INT fTime)
FUNC BOOL ADJUST_BLIP_RADIUS_OVER_TIME()
	FLOAT fCurrentProgress = 1.0 - ((fCurrentSphereRadius - fSphereEndRadius) / (fSphereStartRadius - fSphereEndRadius))
	FLOAT fDiff = (ABSF(fSphereStartRadius - fSphereEndRadius) * 1000)
	IF fCurrentProgress < 0.33
		fDiff = (ABSF(fSphereStartRadius - fSphereEndRadius) * 1000) + ((ABSF(fSphereStartRadius - fSphereEndRadius) * 1000) / 2)
	ELIF fCurrentProgress < 0.66
		fDiff = (ABSF(fSphereStartRadius - fSphereEndRadius) * 1000)
	ELSE
		fDiff = (ABSF(fSphereStartRadius - fSphereEndRadius) * 1000) - ((ABSF(fSphereStartRadius - fSphereEndRadius) * 1000) / 2)
	ENDIF
	
	FLOAT fDiffThisFrame = (fDiff / fSphereShrinkTime) * GET_FRAME_TIME()
	IF fDiffThisFrame < 0.0025
		fDiffThisFrame = 0.0025
	ENDIF
	
	IF fCurrentSphereRadius > fSphereEndRadius
		fCurrentSphereRadius -= fDiffThisFrame
		IF fCurrentSphereRadius < fSphereEndRadius
			fCurrentSphereRadius = fSphereEndRadius
		ENDIF
	ELSE
		fCurrentSphereRadius = fSphereEndRadius
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
/// Wrapper to determine if 'Explode on fail' has been turned on for the current rule.
/// PARAMS:
///    iTeam - Team to check
///    iRule - Rule to check
///    bSecondary - If checking the secondary bounds
///    bEither - If either rule is on
/// RETURNS: 
///    BOOL - If the rule is turned on
FUNC BOOL SHOULD_RULE_EXPLODE_ON_FAIL(INT iTeam, INT iRule, BOOL bSecondary = FALSE, BOOL bEither = FALSE)
	IF bEither
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFour[iRule], ciBS_RULE4_EXPLODE_ON_BOUNDS_FAIL)
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwelve[iRule], ciBS_RULE12_EXPLODE_ON_PRIMARY_BOUNDS_FAIL)
			OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwelve[iRule], ciBS_RULE12_EXPLODE_ON_SECONDARY_BOUNDS_FAIL)
				RETURN TRUE
			ELSE
				RETURN FALSE
			ENDIF
		ELSE 
			RETURN TRUE
		ENDIF
	ENDIF 
	
	IF NOT bSecondary
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFour[iRule], ciBS_RULE4_EXPLODE_ON_BOUNDS_FAIL)
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwelve[iRule], ciBS_RULE12_EXPLODE_ON_PRIMARY_BOUNDS_FAIL)
				RETURN FALSE
			ELSE
				RETURN TRUE
			ENDIF
		ELSE
			RETURN TRUE
		ENDIF
	ELSE
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFour[iRule], ciBS_RULE4_EXPLODE_ON_BOUNDS_FAIL)
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwelve[iRule], ciBS_RULE12_EXPLODE_ON_SECONDARY_BOUNDS_FAIL)
				RETURN FALSE
			ELSE
				RETURN TRUE
			ENDIF
		ELSE
			RETURN TRUE
		ENDIF		
	ENDIF
ENDFUNC

PROC SET_PLAYER_FAILED_OUTSIDE_SPHERE()
	INT iTeam = MC_playerBD[iLocalPart].iTeam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet2, PBBOOL2_PLAYER_OUT_BOUNDS_FAIL)	
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			//[KH] explode on out of bounds fail 
			IF iRule < FMMC_MAX_RULES
				IF SHOULD_RULE_EXPLODE_ON_FAIL(iTeam, iRule, DEFAULT, TRUE)
					
					//[KH] if vehicle is explosion proof
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciVEHICLE_EXPLOSION_PROOF)
						SET_ENTITY_PROOFS(GET_VEHICLE_PED_IS_IN(LocalPlayerPed), FALSE, FALSE, FALSE, FALSE, FALSE)
					ENDIF
					
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_VEHICLE_WEAPON_BULLETPROOF_PLAYER)
						SET_ENTITY_PROOFS(LocalPlayerPed, FALSE, FALSE, FALSE, FALSE, FALSE)
					ENDIF
					
					//B*2957034 - Fix for exploding player and vehicle in pron mode
					SET_ENTITY_INVINCIBLE(LocalPlayerPed, FALSE)
					SET_ENTITY_INVINCIBLE(GET_VEHICLE_PED_IS_IN(LocalPlayerPed), FALSE)
					
					NETWORK_EXPLODE_VEHICLE(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
				ENDIF
			ENDIF
		ENDIF
		
		//Stop elimination sound
		IF IS_BIT_SET(MC_playerBD[iPartToUse].iSuddenDeathBS, ciPBDBOOL_SSDS_COUNTDOWN_SOUND_PLAYING)
			STOP_SOUND(iOutAreaSound)
			SCRIPT_RELEASE_SOUND_ID(iOutAreaSound)
		ENDIF
		SET_BIT(MC_playerBD[iPartToUse].iClientBitSet2, PBBOOL2_PLAYER_OUT_BOUNDS_FAIL)
	ENDIF
	IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_PLAYER_FAIL)
		SET_LOCAL_PLAYER_FAILED(10)
	ENDIF		
	IF MC_playerBD[iPartToUse].iReasonForPlayerFail != PLAYER_FAIL_REASON_BOUNDS
		MC_playerBD[iPartToUse].iReasonForPlayerFail = PLAYER_FAIL_REASON_BOUNDS
	ENDIF
	//[KH] - Displays out of bounds shard if option selected in creator
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciMISSION_CUSTOM_SHARD_ON_OUT_OF_BOUNDS)
		IF NOT IS_BIT_SET(iLocalBoolCheck15, LBOOL15_IS_OUT_OF_BOUNDS_SHARD_DISPLAYED)
			PRINTLN("[KH] | CUSTOM SHARD ON OUT OF BOUNDS | BOUNDS 1 | SHOW OUT OF BOUNDS SHARD")
			BROADCAST_FMMC_OBJECTIVE_PLAYER_KNOCKED_OUT(MC_playerBD[iLocalPart].iteam)
			SET_BIT(iLocalBoolCheck15, LBOOL15_IS_OUT_OF_BOUNDS_SHARD_DISPLAYED)
		ENDIF
	ENDIF
ENDPROC

PROC EXPLODE_LOCAL_PLAYER_DUE_TO_SUDDEN_DEATH_FAIL()
	IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
	AND NETWORK_HAS_CONTROL_OF_ENTITY(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
		
		VEHICLE_INDEX viVehiclePlayerIsIn = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
		
		IF GET_PLAYER_INVINCIBLE(LocalPlayer)
			SET_ENTITY_INVINCIBLE(LocalPlayerPed, FALSE)
		ENDIF
			
		SET_ENTITY_INVINCIBLE(viVehiclePlayerIsIn, FALSE)
		
		NETWORK_EXPLODE_VEHICLE(viVehiclePlayerIsIn)
		PRINTLN("[RCC MISSION] EXPLODE_LOCAL_PLAYER_DUE_TO_SUDDEN_DEATH_FAIL - calling NETWORK_EXPLODE_VEHICLE on player vehicle due to sudden death fail")
	ENDIF
ENDPROC

FUNC BOOL SHOULD_SUMO_SUDDEN_DEATH_START_FOR_LOCAL_PLAYER(INT iTeam)
	
	INT iTeamToCheck
	FOR iTeamToCheck = 0 TO (MC_serverBD.iNumberOfTeams - 1)
		IF IS_TEAM_ACTIVE(iTeamToCheck)
			IF(IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciPRON_DEATHMATCH_TOGGLE_CORONA) OR IS_BIT_SET(MC_serverBD.iServerBitSet4, SBBOOL4_PRON_DEATHMATCH_TOGGLE_CORONA))
				IF MC_serverBD.iTeamScore[iTeam] < MC_serverBD.iTeamScore[iTeamToCheck]
					PRINTLN("[KH] SHOULD_SUMO_SUDDEN_DEATH_START_FOR_LOCAL_PLAYER - Has less points than an enemy team, should not go to sudden death")
					CHANGE_WASTED_SHARD_TEXT(TRUE)
					SET_BIG_MESSAGE_SUPPRESS_WASTED(TRUE)
					SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_IN_STRAPLINE(BIG_MESSAGE_CUSTOM_FAILED, "DEADLINE_HELP_SD", "", DEFAULT, DEFAULT, "RESPAWN_W_MP")
					EXPLODE_LOCAL_PLAYER_DUE_TO_SUDDEN_DEATH_FAIL()
					RETURN FALSE
				ENDIF
			ELSE
				IF MC_serverBD.iTeamDeaths[iTeam] > MC_serverBD.iTeamDeaths[iTeamToCheck]
					PRINTLN("[KH] SHOULD_SUMO_SUDDEN_DEATH_START_FOR_LOCAL_PLAYER - Has more deaths than an enemy team, should not go to sudden death")
					CHANGE_WASTED_SHARD_TEXT(TRUE)
					SET_BIG_MESSAGE_SUPPRESS_WASTED(TRUE)
					SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_IN_STRAPLINE(BIG_MESSAGE_CUSTOM_FAILED, "DEADLINE_HELP_SD", "", DEFAULT, DEFAULT, "RESPAWN_W_MP")
					EXPLODE_LOCAL_PLAYER_DUE_TO_SUDDEN_DEATH_FAIL()
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	PRINTLN("[KH] SHOULD_SUMO_SUDDEN_DEATH_START_FOR_LOCAL_PLAYER - Should go to sudden death, starting for local player")
	RETURN TRUE
	
ENDFUNC

FUNC BOOL SHOULD_SKIP_ELIMINATION_TIMER()
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_DROP_THE_BOMB(g_FMMC_STRUCT.iAdversaryModeType)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    [KH][SSDS] Process the shrinking sphere with the player's position and remove from the game if they are outside the sphere
PROC PROCESS_SUDDEN_DEATH_SUMO_PENNED_IN()
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_AIRSHOOTOUT(g_FMMC_STRUCT.iAdversaryModeType)
		IF IS_BIT_SET(MC_serverBD.iServerBitset3, SBBOOL3_TEAM_0_SHOULD_RESPAWN_BACK_AT_START_NOW)
		OR IS_BIT_SET(MC_serverBD.iServerBitset3, SBBOOL3_TEAM_1_SHOULD_RESPAWN_BACK_AT_START_NOW)
		OR IS_SCREEN_FADED_OUT()
		OR IS_SCREEN_FADING_OUT()
		OR IS_SCREEN_FADING_IN()
			CLEAR_BIT(iLocalBoolCheck15, LBOOL15_SSDS_SUDDEN_DEATH_STARTED)
			RESET_NET_TIMER(stTimeToGetInSphere)
			EXIT
		ENDIF
	ENDIF
	INT iR, iG, iB, iA
	FLOAT fA = 0.5
	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	HUD_COLOURS colourToUse = HUD_COLOUR_YELLOW
	
	IF iRule < FMMC_MAX_RULES
		IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].hudColouring != HUD_COLOUR_YELLOW
			colourToUse = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].hudColouring
		ENDIF
	ENDIF
	
	GET_HUD_COLOUR(colourToUse, iR, iG, iB, iA)
	
	IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_OBJECTIVE_BLOCKER)
		SET_BIT(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_OBJECTIVE_BLOCKER)
	ENDIF
	
	IF NOT IS_BIT_SET(iLocalBoolCheck15, LBOOL15_SSDS_SUDDEN_DEATH_STARTED)
		PRINTLN("[KH][SSD][LIVE] - SUMO SUDDEN DEATH STARTED")
			
		vSphereSpawnPoint = g_FMMC_STRUCT.vSphereSpawnPoint
		fSphereStartRadius = g_FMMC_STRUCT.fSphereStartRadius
		fSphereShrinkTime = g_FMMC_STRUCT.iSphereShrinkTime
		fSphereEndRadius = g_FMMC_STRUCT.fSphereEndRadius
		fTimeToGetInsideSphere = g_FMMC_STRUCT.iTimeToGetInsideSphere

		//set initial sphere radius
		fCurrentSphereRadius = fSphereStartRadius
		
		SET_BIT(iLocalBoolCheck15, LBOOL15_SSDS_SUDDEN_DEATH_STARTED)
		
		IF iSpectatorTarget != -1
		OR IS_LOCAL_PLAYER_ANY_SPECTATOR()
			IF MC_serverBD_3.fSumoSuddenDeathRadius > 0
				PRINTLN("[KH] PROCESS_SUDDEN_DEATH_SUMO_PENNED_IN - SPECTATOR - INITIALISING LATE START SPHERE SIZE")
				fCurrentSphereRadius = MC_serverBD_3.fSumoSuddenDeathRadius
			ENDIF
			
			IF MC_playerBD[iLocalPart].eSumoSuddenDeathStage != eSSDS_TIMER_GET_INSIDE_SPHERE
				MC_playerBD[iLocalPart].eSumoSuddenDeathStage = eSSDS_TIMER_GET_INSIDE_SPHERE
			ENDIF
			
			IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iSuddenDeathBS, ciPBDBOOL_SSDS_HAS_COUNTDOWN_FINISHED)
				IF MC_serverBD_3.fSumoSuddenDeathTimeToGetIn > 0
					fTimeToGetInsideSphere = MC_serverBD_3.fSumoSuddenDeathTimeToGetIn
				ENDIF
				//start the countdown timer for getting inside the sphere
				START_NET_TIMER(stTimeToGetInSphere)
				PRINTLN("[KH] PROCESS_SUDDEN_DEATH_SUMO_PENNED_IN - SPECTATOR - INITIALISING LATE START COUNTDOWN TIMER")
			ELSE
				IF NOT IS_BIT_SET(iLocalBoolCheck15, LBOOL15_HAS_ELIMINATION_COUNTDOWN_FINISHED)
					SET_BIT(iLocalBoolCheck15, LBOOL15_HAS_ELIMINATION_COUNTDOWN_FINISHED)
					PRINTLN("[KH] PROCESS_SUDDEN_DEATH_SUMO_PENNED_IN - SPECTATOR - SETTING LATE START COUNTDOWN FINISH")
				ENDIF
			ENDIF
		ELSE
			//start the countdown timer for getting inside the sphere
			START_NET_TIMER(stTimeToGetInSphere)
		ENDIF
		
		IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_DCTL(g_FMMC_STRUCT.iAdversaryModeType)
			IF NOT SHOULD_SUMO_SUDDEN_DEATH_START_FOR_LOCAL_PLAYER(MC_playerBD[iPartToUse].iteam)
				SET_BIT(iLocalBoolCheck19, LBOOL19_PLAYER_SHOULD_NOT_GO_TO_SUMO_SUDDEN_DEATH)
			ENDIF
		ENDIF
			
	ENDIF
	
	//aimed at spectators
	IF fTimeToGetInsideSphere = 0
		fTimeToGetInsideSphere = MC_serverBD_3.fSumoSuddenDeathTimeToGetIn
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF HAS_NET_TIMER_STARTED(MC_ServerBD.tdSuddenDeath_ShrinkingBoundsHUDTimer[0])
	AND IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD5)
		PRINTLN("[SUMOSUDDENDEATH] g_FMMC_STRUCT.iSphereShrinkTime = ", g_FMMC_STRUCT.iSphereShrinkTime)
		
		TEXT_LABEL_63 tlDebug = "Time: "
		tlDebug += g_FMMC_STRUCT.iSphereShrinkTime
		tlDebug += " / Time left: "
		tlDebug += GET_REMAINING_TIME_ON_SHRINKING_BOUNDS(0)
		DRAW_DEBUG_TEXT_2D(tlDebug, <<0.5, 0.5, 0.5>>)
		DRAW_DEBUG_TEXT_2D("Timer has started!!", <<0.85, 0.85, 0.5>>)
	ENDIF
	#ENDIF
	
	//time elapsed since starting the countdown to get into the sphere
	INT iTimeElapsed = 0
	iTimeElapsed = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stTimeToGetInSphere)
	INT iTimeElapsedCountdown = fTimeToGetInsideSphere - iTimeElapsed
	
	IF (GET_MC_SERVER_GAME_STATE() != GAME_STATE_END
	AND NOT IS_BIT_SET(iLocalBoolCheck19, LBOOL19_PLAYER_SHOULD_NOT_GO_TO_SUMO_SUDDEN_DEATH))
		//countdown timer should have finished before shrinking the sphere
		IF iTimeElapsed >= fTimeToGetInsideSphere
		OR SHOULD_SKIP_ELIMINATION_TIMER()
			//IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iSuddenDeathBS, ciPBDBOOL_SSDS_COUNTDOWN_TIMER_FINISHED)
			IF NOT IS_BIT_SET(iLocalBoolCheck15, LBOOL15_HAS_ELIMINATION_COUNTDOWN_FINISHED)
				PRINTLN("[KH][SSD] - COUNTDOWN TIMER HAS FINISHED")
				SET_BIT(iLocalBoolCheck15, LBOOL15_HAS_ELIMINATION_COUNTDOWN_FINISHED)
				IF iSpectatorTarget = -1
				OR NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
					IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iSuddenDeathBS, ciPBDBOOL_SSDS_HAS_COUNTDOWN_FINISHED)
						SET_BIT(MC_playerBD[iPartToUse].iSuddenDeathBS, ciPBDBOOL_SSDS_HAS_COUNTDOWN_FINISHED)
					ENDIF
				ENDIF
			ENDIF
			
			IF bIsLocalPlayerHost
				INT iTeamLoop
				FOR iTeamLoop = 0 TO FMMC_MAX_TEAMS-1
					IF NOT HAS_NET_TIMER_STARTED(MC_ServerBD.tdSuddenDeath_ShrinkingBoundsHUDTimer[iTeamLoop])
						REINIT_NET_TIMER(MC_ServerBD.tdSuddenDeath_ShrinkingBoundsHUDTimer[iTeamLoop])
						PRINTLN("[SUMOSUDDENDEATH] Starting timer now for team ", iTeamLoop)
					ENDIF
				ENDFOR
			ENDIF
		ELSE
			//IF MC_playerBD[iPartToUse].eSumoSuddenDeathStage != eSSDS_PUSHED_OUT_OF_SPHERE
			//	IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iSuddenDeathBS, ciPBDBOOL_SSDS_IS_INSIDE_SPHERE)
			
					IF NOT IS_BIT_SET(iLocalBoolCheck15, LBOOL15_HAS_ELIMINATION_COUNTDOWN_FINISHED)
						IF bIsLocalPlayerHost
							IF fTimeToGetInsideSphere > 0
								IF NOT HAS_NET_TIMER_STARTED(stSumoSDCountdownUpdateTimer)
									REINIT_NET_TIMER(stSumoSDCountdownUpdateTimer)
								ENDIF
								
								IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stSumoSDCountdownUpdateTimer) >= 250
									REINIT_NET_TIMER(stSumoSDCountdownUpdateTimer)
									MC_serverBD_3.fSumoSuddenDeathTimeToGetIn = iTimeElapsedCountdown	
								ENDIF
							ENDIF
						ENDIF
				
						HUD_COLOURS hcTimerColour = HUD_COLOUR_RED
						
						IF MC_playerBD[iPartToUse].eSumoSuddenDeathStage = eSSDS_INSIDE_SPHERE OR MC_playerBD[iPartToUse].eSumoSuddenDeathStage = eSSDS_PUSHED_OUT_OF_SPHERE OR IS_BIT_SET(MC_playerBD[iPartToUse].iSuddenDeathBS, ciPBDBOOL_SSDS_IS_INSIDE_SPHERE)
							IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_VEHICULAR_WARFARE(g_FMMC_STRUCT.iAdversaryModeType)
							OR USING_SHOWDOWN_POINTS()
								//Don't display timer as you are inside the safe zone in Vehicle Warfare (Bug 3800158)
								hcTimerColour = HUD_COLOUR_WHITE
								PRINTLN("[SUMOSUDDENDEATH][TMS] Elimination timer turning white because we are safe")
							ENDIF
						ENDIF
						
						IF NOT IS_PED_INJURED(PlayerPedToUse)
							DRAW_GENERIC_TIMER(iTimeElapsedCountdown, "SUMO_ELIM", 0, TIMER_STYLE_USEMILLISECONDS, -1, PODIUMPOS_NONE, HUDORDER_BOTTOM, FALSE, hcTimerColour, HUDFLASHING_NONE, 0, FALSE, hcTimerColour, FALSE, DEFAULT, DEFAULT, TRUE) //B* 2666917
						ENDIF
					ENDIF
			//	ENDIF
			//ENDIF
		ENDIF
	ENDIF
	
	//----- UPDATE AND DRAW SPHERE -----//

	IF GET_MC_SERVER_GAME_STATE() != GAME_STATE_END
		//shrink sphere when countdown timer has finished
		IF IS_BIT_SET(iLocalBoolCheck15, LBOOL15_HAS_ELIMINATION_COUNTDOWN_FINISHED)
		OR IS_BIT_SET(MC_playerBD[iPartToUse].iSuddenDeathBS, ciPBDBOOL_SSDS_HAS_COUNTDOWN_FINISHED)
			ADJUST_BLIP_RADIUS_OVER_TIME()
		ENDIF
		
		//aimed at specator
		IF fCurrentSphereRadius = 0
			fCurrentSphereRadius = MC_serverBD_3.fSumoSuddenDeathRadius
		ENDIF

		IF bIsLocalPlayerHost
			IF fCurrentSphereRadius > 0
				IF NOT HAS_NET_TIMER_STARTED(stSumoSDBubbleUpdateTimer)
					REINIT_NET_TIMER(stSumoSDBubbleUpdateTimer)
				ENDIF
				
				IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stSumoSDBubbleUpdateTimer) >= ciSUMO_SD_BUBBLE_UPDATE_INTERVAL
					REINIT_NET_TIMER(stSumoSDBubbleUpdateTimer)
					MC_serverBD_3.fSumoSuddenDeathRadius = fCurrentSphereRadius
				ENDIF
			ENDIF
		ENDIF
		
		DRAW_MARKER_SPHERE(vSphereSpawnPoint, fCurrentSphereRadius + fCurrentSphereRadius_VisualOffset, iR, iG, iB, fA)

	ENDIF
	
	//----------------------------------//
		
	//at 10 seconds on the elimination timer start the beeps
	IF GET_MC_SERVER_GAME_STATE() != GAME_STATE_END
		IF MC_playerBD[iPartToUse].eSumoSuddenDeathStage != eSSDS_PUSHED_OUT_OF_SPHERE
			IF iTimeElapsedCountdown <= 10000 & iTimeElapsedCountdown > 0
				IF NOT IS_BIT_SET(iLocalBoolCheck15, LBOOL15_IS_SUDDEN_DEATH_SOUND_PLAYING)
					IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iSuddenDeathBS, ciPBDBOOL_SSDS_IS_INSIDE_SPHERE)
						SET_BIT(iLocalBoolCheck15, LBOOL15_IS_SUDDEN_DEATH_SOUND_PLAYING)
						
						PRINTLN("[KH][SSD] - START PLAYING ELIMINATION COUNTDOWN SOUND")
						
						IF iOutAreaSound < 0
							iOutAreaSound = GET_SOUND_ID()
						ENDIF
						
						PLAY_SOUND_FRONTEND(iOutAreaSound, "OOB_Timer_Dynamic", "GTAO_FM_Events_Soundset", FALSE)
						
						INT timeToStartTimer
						
						//case to deal with if the time to get inside the sphere is set to be less than 10 seconds
						IF fTimeToGetInsideSphere <= 10000
							timeToStartTimer = fTimeToGetInsideSphere
						ELSE
							timeToStartTimer = iTimeElapsedCountdown
						ENDIF
						
						SET_VARIABLE_ON_SOUND(iOutAreaSound, "Time", (TO_FLOAT(timeToStartTimer))/1000.0)

					ENDIF
				ELSE
					IF IS_BIT_SET(MC_playerBD[iPartToUse].iSuddenDeathBS, ciPBDBOOL_SSDS_IS_INSIDE_SPHERE)
						IF NOT HAS_SOUND_FINISHED(iOutAreaSound)
							STOP_SOUND(iOutAreaSound)
							SCRIPT_RELEASE_SOUND_ID(iOutAreaSound)
						ENDIF
						CLEAR_BIT(iLocalBoolCheck15, LBOOL15_IS_SUDDEN_DEATH_SOUND_PLAYING)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF NOT HAS_SOUND_FINISHED(iOutAreaSound)
			STOP_SOUND(iOutAreaSound)
			SCRIPT_RELEASE_SOUND_ID(iOutAreaSound)
		ENDIF
	ENDIF
			
	//Checks for the player being inside the sphere
	IF iSpectatorTarget = -1
	AND NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
		IF NOT IS_PED_INJURED(LocalPlayerPed)
			IF IS_INSIDE_SPHERE(GET_ENTITY_COORDS(LocalPlayerPed), vSphereSpawnPoint, fCurrentSphereRadius)
				IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iSuddenDeathBS, ciPBDBOOL_SSDS_IS_INSIDE_SPHERE)
					SET_BIT(MC_playerBD[iPartToUse].iSuddenDeathBS, ciPBDBOOL_SSDS_IS_INSIDE_SPHERE)
				ENDIF
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_USE_BLIP_FOR_SHRINKING_AREA)
					IF DOES_BLIP_EXIST(SuddenDeathAreaBlip)
						PRINTLN("[PROCESS_SUDDEN_DEATH_SUMO_PENNED_IN][RH] Shrinking area blip is removed")
						REMOVE_BLIP(SuddenDeathAreaBlip)
					ENDIF
				ENDIF 
			ELSE
				IF IS_BIT_SET(MC_playerBD[iPartToUse].iSuddenDeathBS, ciPBDBOOL_SSDS_IS_INSIDE_SPHERE)
					CLEAR_BIT(MC_playerBD[iPartToUse].iSuddenDeathBS, ciPBDBOOL_SSDS_IS_INSIDE_SPHERE) 
				ENDIF
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_USE_BLIP_FOR_SHRINKING_AREA)
					IF NOT DOES_BLIP_EXIST(SuddenDeathAreaBlip)
						PRINTLN("[PROCESS_SUDDEN_DEATH_SUMO_PENNED_IN][RH] Shrinking area blip is created")
						SuddenDeathAreaBlip = CREATE_BLIP_FOR_COORD(vSphereSpawnPoint)
						SET_BLIP_COLOUR(SuddenDeathAreaBlip, GET_BLIP_COLOUR_FROM_HUD_COLOUR(colourToUse))	
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF IS_BIT_SET(MC_playerBD[iPartToUse].iSuddenDeathBS, ciPBDBOOL_SSDS_IS_INSIDE_SPHERE)
		AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_USE_BLIP_FOR_SHRINKING_AREA)
			IF DOES_BLIP_EXIST(SuddenDeathAreaBlip)
				PRINTLN("[PROCESS_SUDDEN_DEATH_SUMO_PENNED_IN][RH] Shrinking area blip is removed")
				REMOVE_BLIP(SuddenDeathAreaBlip)
			ENDIF
		ELSE
			IF NOT DOES_BLIP_EXIST(SuddenDeathAreaBlip)
			AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_USE_BLIP_FOR_SHRINKING_AREA)
				PRINTLN("[PROCESS_SUDDEN_DEATH_SUMO_PENNED_IN][RH] Shrinking area blip is created")
				SuddenDeathAreaBlip = CREATE_BLIP_FOR_COORD(vSphereSpawnPoint)
				SET_BLIP_COLOUR(SuddenDeathAreaBlip, GET_BLIP_COLOUR_FROM_HUD_COLOUR(colourToUse))	
			ENDIF
		ENDIF
	ENDIF
	
	IF GET_MC_SERVER_GAME_STATE() != GAME_STATE_END			
		//states for each player during the stage
		SWITCH MC_playerBD[iPartToUse].eSumoSuddenDeathStage
			CASE eSSDS_TIMER_GET_INSIDE_SPHERE			
				//IF iTimeElapsed >= fTimeToGetInsideSphere
					IF IS_BIT_SET(MC_playerBD[iPartToUse].iSuddenDeathBS, ciPBDBOOL_SSDS_IS_INSIDE_SPHERE)
						PRINTLN("[KH][SSD] - PLAYER GOT TO THE SPHERE IN TIME")
						IF NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
							MC_playerBD[iPartToUse].eSumoSuddenDeathStage = eSSDS_INSIDE_SPHERE
						ENDIF
					ELSE
						IF iTimeElapsed >= fTimeToGetInsideSphere
							IF NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
								MC_playerBD[iPartToUse].eSumoSuddenDeathStage = eSSDS_PUSHED_OUT_OF_SPHERE
							ENDIF
						ENDIF
					ENDIF
				//ENDIF
			BREAK
			//after the timer has counted down and now players should be killed if they leave the bubble
			CASE eSSDS_INSIDE_SPHERE	
				IF NOT HAS_SOUND_FINISHED(iOutAreaSound)
					STOP_SOUND(iOutAreaSound)
					SCRIPT_RELEASE_SOUND_ID(iOutAreaSound)
				ENDIF
				IF NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
					IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iSuddenDeathBS, ciPBDBOOL_SSDS_IS_INSIDE_SPHERE)
					AND iTimeElapsed >= fTimeToGetInsideSphere
						PRINTLN("[KH][SSD] - WENT OUTSIDE SPHERE BOUNDS: REMOVE PLAYER")
						MC_playerBD[iPartToUse].eSumoSuddenDeathStage = eSSDS_PUSHED_OUT_OF_SPHERE
					ELSE
						IF iTimeElapsed < fTimeToGetInsideSphere
							MC_playerBD[iPartToUse].eSumoSuddenDeathStage = eSSDS_TIMER_GET_INSIDE_SPHERE
						ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE eSSDS_PUSHED_OUT_OF_SPHERE
				IF NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
					IF iTimeElapsed >= fTimeToGetInsideSphere
						PRINTLN("[KH][SSD] - PLAYER PUSHED OUT OF SPHERE")
						CLEAR_BIT(MC_playerBD[iPartToUse].iSuddenDeathBS, ciPBDBOOL_SSDS_IS_INSIDE_SPHERE)
						SET_PLAYER_FAILED_OUTSIDE_SPHERE()
					ELSE
						MC_playerBD[iPartToUse].eSumoSuddenDeathStage = eSSDS_TIMER_GET_INSIDE_SPHERE
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	ELSE
		IF NOT HAS_SOUND_FINISHED(iOutAreaSound)
			STOP_SOUND(iOutAreaSound)
			SCRIPT_RELEASE_SOUND_ID(iOutAreaSound)
		ENDIF
	ENDIF
ENDPROC

FUNC VECTOR GET_LOWEST_GROUND_POSITION_FOR_SUDDEN_DEATH_SPHERE(VECTOR vSpherePos, FLOAT fSphereRadius)
	INT iPoint
	FLOAT fTempGroundZ
	FLOAT fLowestGroundZ
	
	VECTOR vTestPoint[9]
	
	vTestPoint[0] = <<vSpherePos.x, vSpherePos.y, vSpherePos.z>>
	vTestPoint[1] = <<vSpherePos.x + fSphereRadius, vSpherePos.y, vSpherePos.z>>
	vTestPoint[2] = <<vSpherePos.x - fSphereRadius, vSpherePos.y, vSpherePos.z>>
	vTestPoint[3] = <<vSpherePos.x, vSpherePos.y + fSphereRadius, vSpherePos.z>>
	vTestPoint[4] = <<vSpherePos.x, vSpherePos.y - fSphereRadius, vSpherePos.z>>
	vTestPoint[5] = <<vSpherePos.x + fSphereRadius, vSpherePos.y + fSphereRadius, vSpherePos.z>>
	vTestPoint[6] = <<vSpherePos.x + fSphereRadius, vSpherePos.y - fSphereRadius, vSpherePos.z>>
	vTestPoint[7] = <<vSpherePos.x - fSphereRadius, vSpherePos.y - fSphereRadius, vSpherePos.z>>
	vTestPoint[8] = <<vSpherePos.x - fSphereRadius, vSpherePos.y + fSphereRadius, vSpherePos.z>>
	
	FOR iPoint = 0 TO 8
		GET_GROUND_Z_EXCLUDING_OBJECTS_FOR_3D_COORD(vTestPoint[iPoint], fTempGroundZ, TRUE)
		PRINTLN("[KH] GET_LOWEST_GROUND_POSITION_FOR_SUDDEN_DEATH_SPHERE - Looking at point ", iPoint, " ground z = ", fTempGroundZ)
		IF fTempGroundZ < fLowestGroundZ
		OR fLowestGroundZ = 0.0
			fLowestGroundZ = fTempGroundZ
			PRINTLN("[KH] GET_LOWEST_GROUND_POSITION_FOR_SUDDEN_DEATH_SPHERE - Setting this as the lowest ground z")
		ENDIF
	ENDFOR
	
	RETURN <<vSpherePos.x, vSpherePos.y, fLowestGroundZ>>
ENDFUNC

PROC SET_SUDDEN_DEATH_SPHERE_POSITION(ENTITY_INDEX oiPassed)
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciDAWN_RAID_ENABLE_MODE)
		IF GET_ENTITY_MODEL(oiPassed) = INT_TO_ENUM(MODEL_NAMES, HASH("imp_prop_adv_hdsec"))
			IF IS_VECTOR_ZERO(vSphereSpawnPosition)
				IF DOES_ENTITY_EXIST(oiPassed)
					IF NOT IS_ENTITY_IN_DEEP_WATER(oiPassed)
					OR NOT IS_ENTITY_IN_WATER(oiPassed)
						VECTOR vPos = GET_ENTITY_COORDS(oiPassed)
						vSphereSpawnPosition = GET_LOWEST_GROUND_POSITION_FOR_SUDDEN_DEATH_SPHERE(vPos, g_FMMC_STRUCT.fSphereStartRadius)
						PRINTLN("[SET_SUDDEN_DEATH_SPHERE_POSITION] - Setting sphere spawn position vSphereSpawnPosition: ", vSphereSpawnPosition)
					ELSE
						VEHICLE_NODE_ID nidNodeChecking = GET_NTH_CLOSEST_VEHICLE_NODE_ID(GET_ENTITY_COORDS(oiPassed), 1)
						GET_VEHICLE_NODE_POSITION(nidNodeChecking, vSphereSpawnPosition)
						PRINTLN("[SET_SUDDEN_DEATH_SPHERE_POSITION] - !ENTITY IS UNDERWATER! - Setting sphere spawn position to nearest road node vSphereSpawnPosition: ", vSphereSpawnPosition)
					ENDIF
				ELSE
					IF NOT IS_VECTOR_ZERO(vTargetObj)
						vSphereSpawnPosition = vTargetObj
						PRINTLN("[SET_SUDDEN_DEATH_SPHERE_POSITION] - Setting sphere spawn position vTargetObj: ", vSphereSpawnPosition)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SUDDEN_DEATH_KILLALLENEMIES_PENNED_IN(INT iRule)

	IF GET_MC_SERVER_GAME_STATE() != GAME_STATE_END
		INT iR, iG, iB, iA
		FLOAT fA = 0.5
		VECTOR vPlayerPos
		
		IF NOT IS_PED_INJURED(localPlayerPed)
			vPlayerPos = GET_ENTITY_COORDS(localPlayerPed)
		ENDIF
		
		GET_HUD_COLOUR(HUD_COLOUR_YELLOW, iR, iG, iB, iA)
		
		//Sets the start position of the sphere when in sudden death so that it spawns where the package is
		SET_SUDDEN_DEATH_SPHERE_POSITION(objDawnPickup)
		
		IF IS_VECTOR_ZERO(vSphereSpawnPosition)	//If for some reason we don't have valid coordinates just use the centre of team 0s bounds...
			IF iRule >= 0 AND iRule < FMMC_MAX_RULES
				vSphereSpawnPosition = (g_FMMC_STRUCT.sFMMCEndConditions[0].sBoundsStruct[iRule].vPos1 + g_FMMC_STRUCT.sFMMCEndConditions[0].sBoundsStruct[iRule].vPos2) * 0.5
				
				GET_GROUND_Z_FOR_3D_COORD(vSphereSpawnPosition, vSphereSpawnPosition.Z)
				
				PRINTLN("[PROCESS_SUDDEN_DEATH_KILLALLENEMIES_PENNED_IN] vSphereSpawnPosition = <<", vSphereSpawnPosition.X, ", ", vSphereSpawnPosition.Y, ", ", vSphereSpawnPosition.Z, ">>")
			ENDIF
			
			// Ignore Z pos
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_IGNORE_Z_HEIGHT_CHECKS_SD_SHRINKING_BOUNDS)
				vSphereSpawnPosition.Z = vPlayerPos.z
			ENDIF
		ENDIF
		
		//Update timer to that spectators can get an accurate time to start their timer if they join in after sudden death has started
		IF iSpectatorTarget = -1
		AND NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
			IF NOT HAS_NET_TIMER_STARTED(timeToBeInsideSphereUpdateTimer)
				REINIT_NET_TIMER(timeToBeInsideSphereUpdateTimer)
			ENDIF
				
			IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(timeToBeInsideSphereUpdateTimer) >= 350
				REINIT_NET_TIMER(timeToBeInsideSphereUpdateTimer)
				MC_playerBD[iLocalPart].iKillAllEnemiesPennedInUpdateTimer = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(timeToBeInsideSphere)
			ENDIF
		ENDIF
		
		//Effectively sudden death initialisation
		IF NOT HAS_NET_TIMER_STARTED(timeToBeInsideSphere)
			PRINTLN("[PROCESS_SUDDEN_DEATH_KILLALLENEMIES_PENNED_IN] - Sudden death has started, starting timer to get inside sphere")
			IF iSpectatorTarget != -1
			OR IS_LOCAL_PLAYER_ANY_SPECTATOR()
				PRINTLN("[PROCESS_SUDDEN_DEATH_KILLALLENEMIES_PENNED_IN] - Sudden death has started and yet someone has still decided they want to jip in, updating jipper's timer")
				iTimeToBeInsideSphere = g_FMMC_STRUCT.iTimeToGetInsideSphere - MC_playerBD[iPartToUse].iKillAllEnemiesPennedInUpdateTimer
			ELSE
				iTimeToBeInsideSphere = g_FMMC_STRUCT.iTimeToGetInsideSphere
			ENDIF
			
			IF IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_OO_BOUNDS)
				CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_OO_BOUNDS)
			ENDIF
			
			//set the blips coords for this
			IF DOES_BLIP_EXIST(DeliveryBlip)
				REMOVE_BLIP(DeliveryBlip)
			ENDIF
			
			START_NET_TIMER(timeToBeInsideSphere)
		ELSE
			//If the timer is up
			IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(timeToBeInsideSphere) > iTimeToBeInsideSphere
				//And the local player isn't dead already
				IF NOT IS_PED_INJURED(PlayerPedToUse)
					IF iSpectatorTarget = -1
					AND NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
						IF NOT IS_INSIDE_SPHERE(GET_ENTITY_COORDS(PlayerPedToUse), vSphereSpawnPosition, g_FMMC_STRUCT.fSphereStartRadius)
						AND NOT IS_BIT_SET(MC_playerBD[iPartToUse].iKillAllEnemiesPennedInBS, ciKAE_ELIMINATION_TIMER_FINISHED)
							IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_PLAYER_FAIL)
								SET_LOCAL_PLAYER_FAILED(11)
								SET_BIT(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_PLAYER_OUT_OF_LIVES)
								SET_GAME_STATE_ON_DEATH(AFTERLIFE_SET_SPECTATORCAM_AFTER_KILLSTRIP)
								CLEAR_ENTITY_LAST_DAMAGE_ENTITY(LocalPlayerPed)
								SET_ENTITY_HEALTH(PlayerPedToUse, 0)
								PRINTLN("[PROCESS_SUDDEN_DEATH_KILLALLENEMIES_PENNED_IN] - Player died, failing them...")
							ENDIF
							
							IF MC_playerBD[iPartToUse].iReasonForPlayerFail != PLAYER_FAIL_REASON_BOUNDS
								MC_playerBD[iPartToUse].iReasonForPlayerFail = PLAYER_FAIL_REASON_BOUNDS
							ENDIF
						ENDIF
					ENDIF
					
					IF IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_GENERIC_MIDSIZED_CUSTOM)
						CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_GENERIC_MIDSIZED_CUSTOM)
					ENDIF
				ENDIF
				
				IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iKillAllEnemiesPennedInBS, ciKAE_ELIMINATION_TIMER_FINISHED)
					SET_BIT(MC_playerBD[iPartToUse].iKillAllEnemiesPennedInBS, ciKAE_ELIMINATION_TIMER_FINISHED)
				ENDIF
			ELSE
				IF NOT IS_PED_INJURED(PlayerPedToUse)
					IF NOT IS_INSIDE_SPHERE(GET_ENTITY_COORDS(PlayerPedToUse), vSphereSpawnPosition, g_FMMC_STRUCT.fSphereStartRadius)
						IF NOT IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_GENERIC_MIDSIZED_CUSTOM)
						AND NOT IS_BIG_MESSAGE_ALREADY_REQUESTED(BIG_MESSAGE_GENERIC_MIDSIZED_CUSTOM)
							PRINTLN("[PROCESS_SUDDEN_DEATH_KILLALLENEMIES_PENNED_IN] - Displaying out of bounds shard")
							
							STRING sStrapline = "MC_RTN_OBJ2"
							
							IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_DAWN_RAID(g_FMMC_STRUCT.iAdversaryModeType)
								IF IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH)
									sStrapline = "MC_RTN_OBJ3"
								ENDIF
							ENDIF			
							
							SETUP_NEW_BIG_MESSAGE_WITH_STRING(BIG_MESSAGE_GENERIC_MIDSIZED_CUSTOM, "", sStrapline, "BM_OO_BOUNDS", 999999)
						ENDIF
					ELSE				
						IF IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_GENERIC_MIDSIZED_CUSTOM)
							PRINTLN("[PROCESS_SUDDEN_DEATH_KILLALLENEMIES_PENNED_IN] - Clearing out of bounds shard")
							CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_GENERIC_MIDSIZED_CUSTOM)
						ENDIF
						
						IF DOES_BLIP_EXIST(SuddenDeathAreaBlip)
							REMOVE_BLIP(SuddenDeathAreaBlip)
						ENDIF
						
						//Set that the player is inside the sphere
						IF iSpectatorTarget = -1
						AND NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
							IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iKillAllEnemiesPennedInBS, ciKAE_PENNED_IN_INSIDE_SPHERE)
								SET_BIT(MC_playerBD[iPartToUse].iKillAllEnemiesPennedInBS, ciKAE_PENNED_IN_INSIDE_SPHERE)
							ENDIF
						ENDIF
					ENDIF
					
					IF NOT IS_INSIDE_SPHERE(GET_ENTITY_COORDS(PlayerPedToUse), vSphereSpawnPosition, g_FMMC_STRUCT.fSphereStartRadius)
					AND NOT IS_BIT_SET(MC_playerBD[iPartToUse].iKillAllEnemiesPennedInBS, ciKAE_PENNED_IN_INSIDE_SPHERE)
					AND NOT IS_PED_INJURED(PlayerPedToUse)
						INT iTimeElapsedCountdown = iTimeToBeInsideSphere - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(timeToBeInsideSphere)
						DRAW_GENERIC_TIMER(iTimeElapsedCountdown, "SUMO_ELIM", 0, TIMER_STYLE_DONTUSEMILLISECONDS, -1, PODIUMPOS_NONE, HUDORDER_BOTTOM, FALSE, HUD_COLOUR_RED, HUDFLASHING_NONE, 0, FALSE, HUD_COLOUR_RED, FALSE) //B* 2666917
					ENDIF
				ELSE
					IF iSpectatorTarget = -1
					AND NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
						IF NOT IS_BIT_SET(iLocalBoolCheck23, LBOOL23_DIED_BEFORE_SUDDEN_DEATH)
							IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_PLAYER_FAIL)
								SET_LOCAL_PLAYER_FAILED(12)
								SET_BIT(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_PLAYER_OUT_OF_LIVES)
								SET_GAME_STATE_ON_DEATH(AFTERLIFE_SET_SPECTATORCAM_AFTER_KILLSTRIP)
								PRINTLN("[PROCESS_SUDDEN_DEATH_KILLALLENEMIES_PENNED_IN] - Player died, failing them...")
							ENDIF
							
							IF MC_playerBD[iPartToUse].iReasonForPlayerFail != PLAYER_FAIL_REASON_BOUNDS
								MC_playerBD[iPartToUse].iReasonForPlayerFail = PLAYER_FAIL_REASON_BOUNDS
							ENDIF
						ENDIF
					ENDIF
					
					IF IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_GENERIC_MIDSIZED_CUSTOM)
						CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_GENERIC_MIDSIZED_CUSTOM)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//Out of area timer once the player is inside the sphere
		IF IS_BIT_SET(MC_playerBD[iPartToUse].iKillAllEnemiesPennedInBS, ciKAE_PENNED_IN_INSIDE_SPHERE)
			IF NOT IS_INSIDE_SPHERE(GET_ENTITY_COORDS(PlayerPedToUse), vSphereSpawnPosition, g_FMMC_STRUCT.fSphereStartRadius)
				INT iTimeElapsedCountdown = ciOUT_OF_AREA_TIME - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(suddenDeathOutOfAreaTimer)
				IF iTimeElapsedCountdown > 0
					DRAW_GENERIC_TIMER(iTimeElapsedCountdown, "BM_OO_BOUNDS", 0, TIMER_STYLE_DONTUSEMILLISECONDS, -1, PODIUMPOS_NONE, HUDORDER_TOP, FALSE, HUD_COLOUR_RED, HUDFLASHING_NONE, 0, FALSE, HUD_COLOUR_RED, FALSE) //B* 2666917
					
					INT iSeconds = iTimeElapsedCountdown / 1000
					
					IF IS_SOUND_ID_VALID(iBoundsTimerSound)
					AND HAS_SOUND_FINISHED(iBoundsTimerSound)
						PLAY_SOUND_FRONTEND(iBoundsTimerSound, "Out_of_Bounds", "MP_MISSION_COUNTDOWN_SOUNDSET", FALSE)
						PRINTLN("[PROCESS_SUDDEN_DEATH_KILLALLENEMIES_PENNED_IN] SHOULD_PLAY_OUT_OF_BOUNDS_SOUND Returning TRUE and the sound hasn't started. Playing sound.")
						SET_VARIABLE_ON_SOUND(iBoundsTimerSound, "Time", TO_FLOAT(iSeconds))
					ENDIF
				ENDIF
				IF NOT HAS_NET_TIMER_STARTED(suddenDeathOutOfAreaTimer)
					START_NET_TIMER(suddenDeathOutOfAreaTimer)
				ELSE
					IF iTimeElapsedCountdown <= 0
						IF iSpectatorTarget = -1
						AND NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
							IF IS_SOUND_ID_VALID(iBoundsTimerSound)
							AND NOT HAS_SOUND_FINISHED(iBoundsTimerSound)
								STOP_SOUND(iBoundsTimerSound)
								PRINTLN("[PROCESS_SUDDEN_DEATH_KILLALLENEMIES_PENNED_IN] SHOULD_PLAY_OUT_OF_BOUNDS_SOUND Returning FALSE and the sound hasn't finished. Force it to stop. FAILED")
							ENDIF
							
							IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_PLAYER_FAIL)
								SET_LOCAL_PLAYER_FAILED(13)
								SET_BIT(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_PLAYER_OUT_OF_LIVES)
								SET_GAME_STATE_ON_DEATH(AFTERLIFE_SET_SPECTATORCAM_AFTER_KILLSTRIP)
								SET_ENTITY_HEALTH(PlayerPedToUse, 0)
								
								PRINTLN("[KH] PROCESS_SUDDEN_DEATH_KILLALLENEMIES_PENNED_IN - Player has left the sphere, killing and failing them...")
							ENDIF
							
							IF MC_playerBD[iPartToUse].iReasonForPlayerFail != PLAYER_FAIL_REASON_BOUNDS
								MC_playerBD[iPartToUse].iReasonForPlayerFail = PLAYER_FAIL_REASON_BOUNDS
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF IS_SOUND_ID_VALID(iBoundsTimerSound)
				AND NOT HAS_SOUND_FINISHED(iBoundsTimerSound)
					STOP_SOUND(iBoundsTimerSound)
					PRINTLN("[PROCESS_SUDDEN_DEATH_KILLALLENEMIES_PENNED_IN] SHOULD_PLAY_OUT_OF_BOUNDS_SOUND Returning FALSE and the sound hasn't finished. Force it to stop.")
				ENDIF
				
				IF HAS_NET_TIMER_STARTED(suddenDeathOutOfAreaTimer)
					RESET_NET_TIMER(suddenDeathOutOfAreaTimer)
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT DOES_BLIP_EXIST(SuddenDeathAreaBlip)
		AND NOT IS_BIT_SET(MC_playerBD[iPartToUse].iKillAllEnemiesPennedInBS, ciKAE_PENNED_IN_INSIDE_SPHERE)
			SuddenDeathAreaBlip = ADD_BLIP_FOR_COORD(vSphereSpawnPosition)
		ENDIF
		
		//Draw the sphere for the players
		DRAW_MARKER_SPHERE(vSphereSpawnPosition, g_FMMC_STRUCT.fSphereStartRadius, iR, iG, iB, fA)
	ELSE
		IF IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_GENERIC_TEXT)
			CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_GENERIC_TEXT)
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL IS_USING_SCALED_SCORE_VALUES()
	RETURN IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitsetTen, ciMODE_RULE_BASED_SCORE_SCALING) 
ENDFUNC

FUNC FLOAT GET_AMOUNT_OF_TEAMS_SCORE_MULTIPLIER()
	FLOAT fMultToAdd = 0.0
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTen, ciENABLE_TEAM_AMOUNT_SCORE_SCALING)
		IF MC_serverBD.iNumberOfTeams = 3
			fMultToAdd = g_FMMC_STRUCT.iTeamSizeScoreScalingForThree
		ELIF MC_serverBD.iNumberOfTeams = 4
			fMultToAdd = g_FMMC_STRUCT.iTeamSizeScoreScalingForFour
		ENDIF
	ENDIF
	
	RETURN fMultToAdd
ENDFUNC

FUNC FLOAT GET_MULTIPLAYER_BASED_ON_PLAYERS_AND_CORONA_SETTING(INT iTeam, INT iRule)
	IF NOT IS_USING_SCALED_SCORE_VALUES()
		RETURN 1.0
	ENDIF
	FLOAT fMult = (0.1 + (MC_ServerBD.iNumStartingPlayers[iTeam]*0.1)) + (0.5*MC_ServerBD_4.iTargetScoreMultiplierSetting) + GET_AMOUNT_OF_TEAMS_SCORE_MULTIPLIER()
	INT iScore = GET_FMMC_OBJECTIVE_SCORE_LIMIT_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTargetScore[iRule], MC_ServerBD.iNumberOfPlayingPlayers[iTeam])
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[LM][GET_MULTIPLAYER_BASED_ON_PLAYERS_AND_CORONA_SETTING] - (Multiplier Corona Index) MC_ServerBD_4.iTargetScoreMultiplierSetting: ",	 	MC_ServerBD_4.iTargetScoreMultiplierSetting)
		PRINTLN("[LM][GET_MULTIPLAYER_BASED_ON_PLAYERS_AND_CORONA_SETTING] - (Multiplier setting) MC_ServerBD_4.iTargetScoreMultiplierSetting: ", 			(0.5*MC_ServerBD_4.iTargetScoreMultiplierSetting))
		PRINTLN("[LM][GET_MULTIPLAYER_BASED_ON_PLAYERS_AND_CORONA_SETTING] - (Amount of players) MC_ServerBD.iNumStartingPlayers[iTeam]: ", 				MC_ServerBD.iNumStartingPlayers[iTeam])
		PRINTLN("[LM][GET_MULTIPLAYER_BASED_ON_PLAYERS_AND_CORONA_SETTING] - (Players Multiplier): ", 														(0.1 + (MC_ServerBD.iNumStartingPlayers[iTeam]*0.1)))
		PRINTLN("[LM][GET_MULTIPLAYER_BASED_ON_PLAYERS_AND_CORONA_SETTING] - iTeamSizeScoreScalingForThree: ", 												g_FMMC_STRUCT.iTeamSizeScoreScalingForThree)
		PRINTLN("[LM][GET_MULTIPLAYER_BASED_ON_PLAYERS_AND_CORONA_SETTING] - iTeamSizeScoreScalingForFour: ",												g_FMMC_STRUCT.iTeamSizeScoreScalingForFour)		
		PRINTLN("[LM][GET_MULTIPLAYER_BASED_ON_PLAYERS_AND_CORONA_SETTING] - fMult: ", 																		fMult)
		PRINTLN("[LM][GET_MULTIPLAYER_BASED_ON_PLAYERS_AND_CORONA_SETTING] - Target Score is: ", 															iScore)
		PRINTLN("[LM][GET_MULTIPLAYER_BASED_ON_PLAYERS_AND_CORONA_SETTING] - Target Score should be set to: ", 												ROUND(iScore * fMult))
	#ENDIF
	
	// Contingency to make sure new target score is never set to 0
	IF ROUND(iScore * fMult) = 0
		PRINTLN("[LM][GET_MULTIPLAYER_BASED_ON_PLAYERS_AND_CORONA_SETTING] - We would have a target score of ZERO, starting Contingency plan")
		fMult = 0
		INT i = 0
		FOR i = 0 TO 11
			fMult += 0.1
			PRINTLN("[LM][GET_MULTIPLAYER_BASED_ON_PLAYERS_AND_CORONA_SETTING] - iScore: ", iScore, " * fMult: ", fMult, " EQUALS: " , 						ROUND(iScore * fMult))
			IF ROUND(iScore * fMult) = 1				
				PRINTLN("[LM][GET_MULTIPLAYER_BASED_ON_PLAYERS_AND_CORONA_SETTING] - Returning fMult (Contingency): ", 										fMult)		
				RETURN fMult
			ENDIF
		ENDFOR
	ENDIF
	
	PRINTLN("[LM][GET_MULTIPLAYER_BASED_ON_PLAYERS_AND_CORONA_SETTING] - Returning fMult: ", 																fMult)		
	RETURN fMult
ENDFUNC

FUNC BOOL HAS_MULTI_RULE_TIMER_EXPIRED(INT iTeam)
	
	IF HAS_NET_TIMER_STARTED(MC_serverBD_3.tdMultiObjectiveLimitTimer[iteam])
		IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdMultiObjectiveLimitTimer[iteam]) > MC_serverBD_3.iMultiObjectiveTimeLimit[iTeam]
			RETURN TRUE
		ENDIF
	ENDIF	
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_MULTI_SD_TIMER_EXPIRED(INT iTeam)
	
	IF HAS_NET_TIMER_STARTED(MC_serverBD_3.tdMultiObjectiveLimitTimer[iteam])
		IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdMultiObjectiveLimitTimer[iteam]) > MC_serverBD_3.iMultiObjectiveTimeLimit[iTeam] + GET_FMMC_OBJECTIVE_TIME_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iteam].iObjectiveSuddenDeathTimeLimit[MC_ServerBD_4.iCurrentHighestPriority[iTeam]])
			PRINTLN("[TMS][BFSD] HAS_MULTI_SD_TIMER_EXPIRED - Returning TRUE")
			RETURN TRUE
		ENDIF
	ENDIF	
	
	PRINTLN("[TMS][BFSD] HAS_MULTI_SD_TIMER_EXPIRED - Returning FALSE")
	RETURN FALSE
ENDFUNC

FUNC BOOL CHECK_SUICIDE_DECREMENT_DOES_NOT_EXCEED_PREV_RULE(INT iRule, INT iTeam, INT iMultiplier = 0, INT iChange = 0)
	BOOL bNewScoreLessThanPrevRule = FALSE
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciNEGATIVE_SUICIDE_SCORE_RULE_CAPPED)
		PRINTLN("[RCC MISSION] Bitset: ciNEGATIVE_SUICIDE_SCORE_RULE_CAPPED is set")
		INT iNewScoreToAdd 
		
		IF iMultiplier <> 0
			iNewScoreToAdd = (GET_FMMC_POINTS_FOR_TEAM(iTeam, iRule)*imultiplier)
		ENDIF
		IF iChange <> 0
			iNewScoreToAdd = iChange
		ENDIF
		
		INT iNewScore = MC_ServerBD.iTeamScore[MC_PlayerBD[iLocalPart].iTeam] + iNewScoreToAdd
		
		PRINTLN("[RCC MISSION] iNewScoreToAdd: ",												iNewScoreToAdd)
		PRINTLN("[RCC MISSION] Team iOldScore: ",												iNewScore-iNewScoreToAdd)
		PRINTLN("[RCC MISSION] Team iNewScore: ",												iNewScore)		
		
		PRINTLN("[RCC MISSION] iRule: ",														iRule)
		INT iPreviousRuleRequiredScore = 0
		INT iRuleCount = 0
		FOR iRuleCount = 0 TO iRule-1 			
			iPreviousRuleRequiredScore += ROUND(GET_FMMC_OBJECTIVE_SCORE_LIMIT_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTargetScore[iRuleCount], MC_ServerBD.iNumberOfPlayingPlayers[iTeam])*GET_MULTIPLAYER_BASED_ON_PLAYERS_AND_CORONA_SETTING(iTeam, iRuleCount))
		ENDFOR
		
		PRINTLN("[RCC MISSION] iPreviousRuleRequiredScore: ",								iPreviousRuleRequiredScore)
		PRINTLN("[RCC MISSION] MC_playerBD[iPartToUse].iPlayerScore: ",						MC_playerBD[iPartToUse].iPlayerScore)
		PRINTLN("[RCC MISSION] iTeamScore: ",												MC_ServerBD.iTeamScore[MC_PlayerBD[iLocalPart].iTeam])
		PRINTLN("[RCC MISSION] iNewScoreToAdd: ",											iNewScoreToAdd) 
		PRINTLN("[RCC MISSION] iNewScore: ",												iNewScore) 
		
		IF iNewScoreToAdd > 0
			PRINTLN("[RCC MISSION] ", iNewScoreToAdd, " > ", 0) 
			PRINTLN("[RCC MISSION] iNewScoreToAdd: Positive Num, Allow the Increment") 	
			RETURN FALSE
		ENDIF
		
		IF iRule = 0
		AND iNewScore > -1
			PRINTLN("[RCC MISSION] iRule = 0   &  ", iNewScore, " > ", -1)
			PRINTLN("[RCC MISSION] We are on the First Rule, so 0 is the minimum score. NewScore is above that amount. Allow the decrement") 
				
			RETURN FALSE
		ENDIF		
						
		IF iNewScore < iPreviousRuleRequiredScore
			PRINTLN("[RCC MISSION] ", iNewScore, " < ", iPreviousRuleRequiredScore) 	
			PRINTLN("[RCC MISSION] The decrement would set us below the Rule Score we are current on. Don't allow the decrement, exit the function.") 	
			bNewScoreLessThanPrevRule = TRUE
		ENDIF
		
		IF (MC_serverBD.iScoreOnThisRule[iTeam] + iNewScoreToAdd) < 0
			PRINTLN("[RCC MISSION] MC_serverBD.iScoreOnThisRule[iTeam]: ", MC_serverBD.iScoreOnThisRule[iTeam])
			PRINTLN("[RCC MISSION] MC_serverBD.iScoreOnThisRule[iTeam] would be less than zero if we did this decrement, so we're exiting the function.") 	
			bNewScoreLessThanPrevRule = TRUE
		ENDIF
	ENDIF
	
	PRINTLN("[RCC MISSION] Returning bNewScoreLessThanPrevRule ",								bNewScoreLessThanPrevRule)
	RETURN bNewScoreLessThanPrevRule
ENDFUNC

FUNC WEAPON_TYPE GET_WEAPON_FOR_GUNSMITH(INT iRule, INT iTeam)
	WEAPON_TYPE weapToReturn = WEAPONTYPE_UNARMED
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTen, ciOVERRIDE_GUNSMITH_LOADOUT_SETTINGS)
		SWITCH MC_ServerBD_4.iGunsmithLoadoutSetting
		// Standard...
		CASE 0
			SWITCH iRule
				CASE 0		weapToReturn = WEAPONTYPE_RPG					BREAK
				CASE 1		weapToReturn = WEAPONTYPE_MINIGUN				BREAK
				CASE 2		weapToReturn = WEAPONTYPE_DLC_SPECIALCARBINE	BREAK
				CASE 3		weapToReturn = WEAPONTYPE_DLC_REVOLVER			BREAK
				CASE 4		weapToReturn = WEAPONTYPE_SAWNOFFSHOTGUN		BREAK
				CASE 5		weapToReturn = WEAPONTYPE_DLC_MACHETE			BREAK
			ENDSWITCH
		BREAK
		
		// Alt 1...
		CASE 1
			SWITCH iRule
				CASE 0		weapToReturn = WEAPONTYPE_DLC_RAILGUN			BREAK
				CASE 1		weapToReturn = WEAPONTYPE_COMBATMG				BREAK
				CASE 2		weapToReturn = WEAPONTYPE_DLC_MUSKET			BREAK
				CASE 3		weapToReturn = WEAPONTYPE_HEAVYSNIPER			BREAK
				CASE 4		weapToReturn = WEAPONTYPE_PUMPSHOTGUN			BREAK
				CASE 5		weapToReturn = WEAPONTYPE_DLC_KNUCKLE			BREAK
			ENDSWITCH
		BREAK
		
		// Alt 2...
		CASE 2
			SWITCH iRule
				CASE 0		weapToReturn = WEAPONTYPE_DLC_AUTOSHOTGUN		BREAK
				CASE 1		weapToReturn = WEAPONTYPE_DLC_COMPACTRIFLE		BREAK
				CASE 2		weapToReturn = WEAPONTYPE_DLC_MINISMG			BREAK
				CASE 3		weapToReturn = WEAPONTYPE_DLC_COMPACTLAUNCHER	BREAK
				CASE 4		weapToReturn = WEAPONTYPE_DLC_DBSHOTGUN			BREAK
				CASE 5		weapToReturn = WEAPONTYPE_DLC_POOLCUE			BREAK
			ENDSWITCH
		BREAK
		
		// Alt 3...
		CASE 3
			SWITCH iRule
				CASE 0		weapToReturn = WEAPONTYPE_RPG					BREAK
				CASE 1		weapToReturn = WEAPONTYPE_DLC_RAILGUN			BREAK
				CASE 2		weapToReturn = WEAPONTYPE_DLC_HEAVYSHOTGUN		BREAK
				CASE 3		weapToReturn = WEAPONTYPE_ADVANCEDRIFLE			BREAK
				CASE 4		weapToReturn = WEAPONTYPE_APPISTOL				BREAK
				CASE 5		weapToReturn = WEAPONTYPE_KNIFE					BREAK
			ENDSWITCH
		BREAK			
		ENDSWITCH
	ELSE
		// Initialise the weapon & ammo values.
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSeven[iRule], ciBS_RULE7_ENABLE_GUN_ROULETTE_RANDOM_OVERRIDE)
			weapToReturn = GET_RANDOM_ROULETTE_WEAPON(GET_RANDOM_INT_IN_RANGE(0, GET_RANDOM_ROULETTE_WEAPON_TOTAL()))		
		ELSE
			weapToReturn = INT_TO_ENUM(WEAPON_TYPE, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sGRouletteStruct[iRule].iWeaponActualIndex)
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF weapToReturn = WEAPONTYPE_UNARMED
			PRINTLN("[LM][GET_WEAPON_FOR_GUNSMITH] - Something went wrong because GET_WEAPON_FOR_GUNSMITH returned WEAPONTYPE_UNARMED")
		ENDIF
	#ENDIF
	
	RETURN weapToReturn
ENDFUNC

FUNC BOOL SHOULD_GUNSMITH_WEAPON_HAVE_UNLIMITED_AMMO(WEAPON_TYPE weapType)
	BOOL bUnlimitedAmmo = FALSE
	
	SWITCH weapType
		CASE WEAPONTYPE_RPG							bUnlimitedAmmo = TRUE		 BREAK
		CASE WEAPONTYPE_MINIGUN						bUnlimitedAmmo = TRUE		 BREAK
		CASE WEAPONTYPE_DLC_FIREWORK				bUnlimitedAmmo = TRUE		 BREAK
		CASE WEAPONTYPE_DLC_FLAREGUN				bUnlimitedAmmo = TRUE		 BREAK
		CASE WEAPONTYPE_GRENADELAUNCHER				bUnlimitedAmmo = TRUE		 BREAK
		CASE WEAPONTYPE_DLC_COMPACTLAUNCHER			bUnlimitedAmmo = TRUE		 BREAK
		CASE WEAPONTYPE_STICKYBOMB					bUnlimitedAmmo = TRUE		 BREAK
		CASE WEAPONTYPE_DLC_PROXMINE				bUnlimitedAmmo = TRUE		 BREAK
		CASE WEAPONTYPE_GRENADE						bUnlimitedAmmo = TRUE		 BREAK
		CASE WEAPONTYPE_DLC_DBSHOTGUN				bUnlimitedAmmo = TRUE		 BREAK
		CASE WEAPONTYPE_DLC_MUSKET					bUnlimitedAmmo = TRUE		 BREAK
		CASE WEAPONTYPE_DLC_MARKSMANPISTOL			bUnlimitedAmmo = TRUE		 BREAK
		CASE WEAPONTYPE_DLC_PIPEBOMB				bUnlimitedAmmo = TRUE		 BREAK
		CASE WEAPONTYPE_BZGAS						bUnlimitedAmmo = TRUE		 BREAK
		CASE WEAPONTYPE_DLC_HOMINGLAUNCHER			bUnlimitedAmmo = TRUE		 BREAK
		CASE WEAPONTYPE_DLC_RAILGUN					bUnlimitedAmmo = TRUE		 BREAK
	ENDSWITCH
	RETURN bUnlimitedAmmo
ENDFUNC

FUNC BOOL IS_DAMAGE_FROM_SCRIPT(INT weapType)
	PRINTLN("[LM][IS_DAMAGE_FROM_SCRIPT] - Is the damage type from Script: ", weapType = HASH("WEAPONTYPE_SCRIPT_HEALTH_CHANGE"))
	RETURN weapType = HASH("WEAPONTYPE_SCRIPT_HEALTH_CHANGE")
ENDFUNC

FUNC BOOL DOES_ANY_TEAM_HAVE_MAX_POINTS()
	INT i, iPassScore
	
	iPassScore = g_FMMC_STRUCT.sFMMCEndConditions[0].iMissionScoreLimit
	
	FOR i = 0 TO FMMC_MAX_TEAMS - 1
		
		IF MC_serverBD.iTeamScore[i] >= iPassScore
			RETURN TRUE
			PRINTLN("[JT POINTLESS] team that has max: ", i)
		ENDIF
	ENDFOR
	RETURN FALSE
ENDFUNC

FUNC INT GET_NUMBER_OF_ACTIVE_PARTICIPANTS_ON_TEAM(INT iTeam)
	INT i, iActivePartCount
	FOR i = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
		AND NOT IS_PARTICIPANT_A_SPECTATOR(i)
		AND iTeam = MC_PlayerBD[i].iteam
			iActivePartCount++
		ENDIF
	ENDFOR
	RETURN iActivePartCount
ENDFUNC

FUNC INT GET_NUMBER_OF_ACTIVE_PARTICIPANTS()
	INT i, iActivePartCount
	FOR i = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
		AND NOT IS_PARTICIPANT_A_SPECTATOR(i)
			iActivePartCount++
		ENDIF
	ENDFOR
	RETURN iActivePartCount
ENDFUNC


FUNC INT GET_CRITICAL_MINIMUM_FOR_TEAM(INT iTeam)
	IF g_FMMC_STRUCT.iCriticalMinimumForTeam[iTeam] = ciPLAYER_NUM_VARIABLE_CRITICAL_MIN
		IF ((MC_ServerBD.iNumStartingPlayers[iTeam] < 2 AND MC_ServerBD.iNumberOfTeams = 1)
		OR (MC_ServerBD.iNumberOfTeams > 1 AND MC_ServerBD.iNumStartingPlayers[iTeam] < 1))
		AND NOT IS_FAKE_MULTIPLAYER_MODE_SET()
			RETURN 2
		ENDIF
		RETURN MC_ServerBD.iNumStartingPlayers[iTeam]
	ELSE
		RETURN g_FMMC_STRUCT.iCriticalMinimumForTeam[iTeam]
	ENDIF
ENDFUNC

FUNC INT GET_FREE_INDEX_FOR_DISTANCE_TRACKING_BAR()
	INT i = 0
	FOR i = 0 TO ci_TOTAL_DISTANCE_CHECK_BARS-1
		IF iGPSTrackingMeterPedIndex[i] = -1
			RETURN i
		ENDIF
	ENDFOR
	RETURN -1
ENDFUNC

FUNC BOOL DOES_PED_REQUIRE_DISTANCE_TRACKING(INT iPed, INT iRule)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetEight, ciPED_BSEight_DistanceGPSTracking) 
	AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedTrackingRule = iRule
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetEight, ciPED_BSEight_DistanceGPSTracking) 
	AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedTrackingRule = -1
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_THIS_LOCATION_HAVE_A_SPECIFIC_ENTRY_HEADING( INT iLoc )
	BOOL bReturn = FALSE
	
	IF ( IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ iLoc ].iLocBS, ciLoc_BS_DirectionFailObjective )
	OR IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ iLoc ].iLocBS, ciLoc_BS_DirectionFailMission )
	OR IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ iLoc ].iLocBS2, ciLoc_BS2_DirectionRestrictPass ) )
		bReturn = TRUE
	ENDIF
	
	RETURN bReturn
ENDFUNC

FUNC BOOL IS_PARTICIPANT_FACING_CORRECT_DIRECTION_FOR_LOCATION( INT iLoc, INT iParticipant )
	BOOL bReturn = FALSE
	
	FLOAT fTargetHeading 	= g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ iLoc ].fDir
	FLOAT fTolerance		= g_FMMC_STRUCT_ENTITIES.sGotoLocationData[ iLoc ].fDirTolerance
	FLOAT fPlayerHeading
	PARTICIPANT_INDEX partID = INT_TO_PARTICIPANTINDEX( iParticipant )
	PLAYER_INDEX playerID = NETWORK_GET_PLAYER_INDEX( partID )
	
	IF IS_ENTITY_ALIVE(GET_PLAYER_PED( playerID ))
	 fPlayerHeading	= GET_ENTITY_HEADING( GET_PLAYER_PED( playerID ) )
	ENDIF

	IF ABSF( GET_ANGULAR_DIFFERENCE( fPlayerHeading, fTargetHeading - 180.0 ) )  <= fTolerance
		bReturn = TRUE
	ENDIF
	
	RETURN bReturn
ENDFUNC

// These two functions (IS_ANY_TEAM_ON_MISSION_SCORE_LIMIT_SD) (ARE_MULTIPLE_TEAMS_ON_MISSION_SCORE_LIMIT_SD) - are helper functions designed to assess whether sudden death
// Should start. They were created because of Sudden Death being required to work when the Timer has not run out using g_FMMC_STRUCT.sFMMCEndConditions[0].iMissionScoreLimit and  MC_serverBD.iTeamScore[i]
FUNC BOOL IS_ANY_TEAM_ON_MISSION_SCORE_LIMIT_SD()
	INT iTeamA = 0
	FOR iTeamA = 0 to FMMC_MAX_TEAMS-1
		IF MC_serverBD.iTeamScore[iTeamA] >= g_FMMC_STRUCT.sFMMCEndConditions[0].iMissionScoreLimit
			RETURN TRUE
		ENDIF
	ENDFOR
	RETURN FALSE
ENDFUNC

FUNC BOOL ARE_MULTIPLE_TEAMS_ON_MISSION_SCORE_LIMIT_SD()
	INT iCacheScore = -1
	INT iTeamA = 0
	FOR iTeamA = 0 to FMMC_MAX_TEAMS-1 
		IF iCacheScore = MC_serverBD.iTeamScore[iTeamA]
			PRINTLN("[RCC MISSION] - iCacheScore is Matched with Team: ", iTeamA, " for a value of: ", iCacheScore, " Returning TRUE so that we can process sudden death...")
			RETURN TRUE
		ENDIF
		
		IF MC_serverBD.iTeamScore[iTeamA] >= g_FMMC_STRUCT.sFMMCEndConditions[0].iMissionScoreLimit
			iCacheScore = MC_serverBD.iTeamScore[iTeamA]
			PRINTLN("[RCC MISSION] - iCacheScore is now equal to Team: ", iTeamA, " for a value of: ", iCacheScore)
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC

FUNC BOOL TURFWAR_BLOCK_POINT_ALLOCATION_AND_PROP_RESET_DUE_TO_MISSION_ENDING()
	IF IS_ANY_TEAM_ON_MISSION_SCORE_LIMIT_SD()
	AND NOT ARE_MULTIPLE_TEAMS_ON_MISSION_SCORE_LIMIT_SD()
	AND NOT IS_BIT_SET( MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH )
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL TURFWAR_BLOCK_SHARD_FOR_LAST_POINTS()
	BOOL bBlockShard = FALSE
	
	// Regular
	IF NOT ARE_MULTIPLE_TEAMS_ON_MISSION_SCORE_LIMIT_SD()
	AND IS_ANY_TEAM_ON_MISSION_SCORE_LIMIT_SD()
		bBlockShard = TRUE
	ENDIF
	
	IF IS_BIT_SET( MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH )
		bBlockShard = TRUE
	ENDIF
	
	RETURN bBlockShard
ENDFUNC

FUNC BOOL TURFWAR_BLOCK_PROPS_RESET_FOR_LAST_POINTS()
	BOOL bBlockResetProps = FALSE
	
	IF IS_ANY_TEAM_ON_MISSION_SCORE_LIMIT_SD()
	AND NOT IS_BIT_SET( MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH )
	OR IS_BIT_SET(MC_serverBD.iServerBitSet4, SBBOOL4_TW_SD_END_POINTS_ALLOCATED)
		bBlockResetProps = TRUE
	ENDIF
	
	RETURN bBlockResetProps
ENDFUNC

FUNC INT GET_TEAM_WITH_MOST_PLAYER_KILLS()
	
	// Assign a winner for this "interval/mini-round" and give points.
	INT iTeamWinner = -1
	INT iTeamWinnerAmount = -1
	INT iTeam
	FOR iTeam = 0 TO FMMC_MAX_TEAMS-1
		IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SSBOOL_TEAM0_FAILED + iTeam)
		AND IS_TEAM_ACTIVE(iTeam)
			PRINTLN("[LM][GET_TEAM_WITH_MOST_PLAYER_KILLS] - Kills iTeam has: ",  iTeam, " : ", MC_ServerBD.iTeamKills[iTeam])	
			IF MC_ServerBD.iTeamKills[iTeam] > iTeamWinnerAmount
				iTeamWinner = iTeam
				iTeamWinnerAmount = MC_ServerBD.iTeamKills[iTeam]
				PRINTLN("[LM][GET_TEAM_WITH_MOST_PLAYER_KILLS] - Assinging Winner: ", iTeamWinner, " with Kill Amount: ", iTeamWinnerAmount)
			ENDIF
		ELSE
			PRINTLN("[LM][TURF WAR][GET_TEAM_WITH_MOST_PLAYER_KILLS] - Team: ", iTeam, " Excluded from check as they have failed")
		ENDIF
	ENDFOR
	
	INT iDraws = 0
	FOR iTeam = 0 TO FMMC_MAX_TEAMS-1
		IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SSBOOL_TEAM0_FAILED + iTeam)
		AND IS_TEAM_ACTIVE(iTeam)
			IF iTeamWinnerAmount = MC_ServerBD.iTeamKills[iTeam]
				PRINTLN("[LM][GET_TEAM_WITH_MOST_PLAYER_KILLS] - Incrementing iDraws")
				iDraws++
			ENDIF
			PRINTLN("[LM][GET_TEAM_WITH_MOST_PLAYER_KILLS] - iDraws =  ", iDraws)
		ELSE
			PRINTLN("[LM][TURF WAR][GET_TEAM_WITH_MOST_PLAYER_KILLS] - Team: ", iTeam, " Excluded from check as they have failed")
		ENDIF
	ENDFOR
	
	IF iDraws > 1
	OR iTeamWinnerAmount = -1
	OR iTeamWinner = -1
		PRINTLN("[LM][GET_TEAM_WITH_MOST_PLAYER_KILLS] - Returning -1")
		RETURN -1
	ENDIF
	
	PRINTLN("[LM][GET_TEAM_WITH_MOST_PLAYER_KILLS] - Returning iTeamWinner: ", iTeamWinner)
	RETURN iTeamWinner	
ENDFUNC

FUNC INT GET_TEAM_WITH_MOST_OWNED_PROPS()
	
	// Assign a winner for this "interval/mini-round" and give points.
	INT iTeamWinner = -1
	INT iTeamWinnerAmount = -1
	INT iTeam
	FOR iTeam = 0 TO FMMC_MAX_TEAMS-1
		IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SSBOOL_TEAM0_FAILED + iTeam)
		AND IS_TEAM_ACTIVE(iTeam)
			PRINTLN("[LM][TURF WAR][GET_TEAM_WITH_MOST_OWNED_PROPS] - Props Owned by iTeam: ",  iTeam, " : ", MC_ServerBD_4.iPropsAmountOwnedByTeam[iTeam])	
			IF MC_ServerBD_4.iPropsAmountOwnedByTeam[iTeam] > iTeamWinnerAmount
				iTeamWinner = iTeam
				iTeamWinnerAmount = MC_ServerBD_4.iPropsAmountOwnedByTeam[iTeam]
				PRINTLN("[LM][TURF WAR][GET_TEAM_WITH_MOST_OWNED_PROPS] - Assinging Winner: ", iTeamWinner, " with props: ", iTeamWinnerAmount)
			ENDIF
		ELSE
			PRINTLN("[LM][TURF WAR][GET_TEAM_WITH_MOST_OWNED_PROPS] - Team: ", iTeam, " Excluded from check as they have failed")
		ENDIF
	ENDFOR
	
	INT iDraws = 0
	FOR iTeam = 0 TO FMMC_MAX_TEAMS-1
		IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SSBOOL_TEAM0_FAILED + iTeam)
		AND IS_TEAM_ACTIVE(iTeam)
			IF iTeamWinnerAmount = MC_ServerBD_4.iPropsAmountOwnedByTeam[iTeam]
				PRINTLN("[LM][TURF WAR][GET_TEAM_WITH_MOST_OWNED_PROPS] - Incrementing iDraws")
				iDraws++
			ENDIF
			PRINTLN("[LM][TURF WAR][GET_TEAM_WITH_MOST_OWNED_PROPS] - iDraws =  ", iDraws)
		ELSE
			PRINTLN("[LM][TURF WAR][GET_TEAM_WITH_MOST_OWNED_PROPS] - Team: ", iTeam, " Excluded from check as they have failed")
		ENDIF
	ENDFOR
	
	IF iDraws > 1
	OR iTeamWinnerAmount = -1
	OR iTeamWinner = -1
		PRINTLN("[LM][TURF WAR][GET_TEAM_WITH_MOST_OWNED_PROPS] - Returning -1")
		RETURN -1
	ENDIF
	
	PRINTLN("[LM][TURF WAR][GET_TEAM_WITH_MOST_OWNED_PROPS] - Returning iTeamWinner: ", iTeamWinner)
	RETURN iTeamWinner
ENDFUNC

FUNC BOOL DOES_THIS_TEAM_OWN_CLAIMED_PROP(INT iTeam, INT iProp)
	SWITCH MC_ServerBD_4.ePropClaimState[iProp]
		CASE ePropClaimingState_Claimed_Team_0
			RETURN iTeam = 0
		CASE ePropClaimingState_Claimed_Team_1
			RETURN iTeam = 1
		CASE ePropClaimingState_Claimed_Team_2
			RETURN iTeam = 2
		CASE ePropClaimingState_Claimed_Team_3 
			RETURN iTeam = 3
	ENDSWITCH
	// Other states return false anyway.
	RETURN FALSE
ENDFUNC

PROC CALCULATE_TEAM_PROP_OWNERSHIP()
	PRINTLN("[LM][TURF WAR][CALCULATE_TEAM_PROP_OWNERSHIP] - CALCULATING PROP OWNERSHIP")
	
	IF IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
		PRINTLN("[LM][TURF WAR][CALCULATE_TEAM_PROP_OWNERSHIP] - Not calculating, as the mission is over!")
		EXIT
	ENDIF
	
	INT iPropOwned[FMMC_MAX_TEAMS]
	INT iProp = 0	
	FOR iProp = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfProps-1
		IF DOES_THIS_TEAM_OWN_CLAIMED_PROP(0, iProp)
			iPropOwned[0]++
		ELIF DOES_THIS_TEAM_OWN_CLAIMED_PROP(1, iProp)
			iPropOwned[1]++
		ELIF DOES_THIS_TEAM_OWN_CLAIMED_PROP(2, iProp)
			iPropOwned[2]++
		ELIF DOES_THIS_TEAM_OWN_CLAIMED_PROP(3, iProp)
			iPropOwned[3]++
		ENDIF
	ENDFOR
	
	MC_ServerBD_4.iPropsAmountOwnedByTeam[0] = iPropOwned[0]
	MC_ServerBD_4.iPropsAmountOwnedByTeam[1] = iPropOwned[1]
	MC_ServerBD_4.iPropsAmountOwnedByTeam[2] = iPropOwned[2]
	MC_ServerBD_4.iPropsAmountOwnedByTeam[3] = iPropOwned[3]
	
	PRINTLN("[LM][TURF WAR][CALCULATE_TEAM_PROP_OWNERSHIP] - Team 0: ", iPropOwned[0], " Team 1: ", iPropOwned[1], " Team 2: ", iPropOwned[2], "  Team 3: ", iPropOwned[3])
	
	/* This was reworked, but leaving it commented out just incase they want it back to this.
	IF bIsLocalPlayerHost
		INT iCountPlayers = 0
		INT iPart
		INT iPartMax = GET_NUMBER_OF_PLAYING_PLAYERS()
		
		FOR iPart = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
			
			MC_ServerBD_4.iPropAmountOwnedByPart[iPart] = iLocalPropAmountOwnedByPart[iPart]
			
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_NATIVE(PARTICIPANT_INDEX, iPart))
			AND NOT IS_PARTICIPANT_A_SPECTATOR(iPart)
				iCountPlayers++
			ENDIF
			IF iCountPlayers >= iPartMax
				BREAKLOOP
			ENDIF
		ENDFOR
	ENDIF
	
	
	PRINTLN("[LM][TURF WAR][CALCULATE_TEAM_PROP_OWNERSHIP] - Part 0: ", MC_ServerBD_4.iPropAmountOwnedByPart[0], " Part 1: ", MC_ServerBD_4.iPropAmountOwnedByPart[1], " Part 2: ", MC_ServerBD_4.iPropAmountOwnedByPart[2], "  Part 3: ", MC_ServerBD_4.iPropAmountOwnedByPart[3], " 
															 Part 4: ", MC_ServerBD_4.iPropAmountOwnedByPart[4], " Part 5: ", MC_ServerBD_4.iPropAmountOwnedByPart[5], " Part 6: ", MC_ServerBD_4.iPropAmountOwnedByPart[6], "  Part 7: ", MC_ServerBD_4.iPropAmountOwnedByPart[7])
															 */
ENDPROC

PROC TURF_WAR_RECALCULATE_LEADER_FOR_HUD()
	INT iTeamWithMostOwnedProps = GET_TEAM_WITH_MOST_OWNED_PROPS()
	BROADCAST_FMMC_PROP_TURF_WARS_TEAM_LEADING_CHANGED(iTeamWithMostOwnedProps, iPropsHighestTeamForHUD)
ENDPROC

FUNC STRING GET_SVM_AUDIO_SCENE_FROM_MODEL_NAME(MODEL_NAMES vehModel)
	SWITCH vehModel
		CASE TECHNICAL2 		RETURN "DLC_IE_SVM_technical2_Vehicle_Scene"
		CASE BOXVILLE5			RETURN "DLC_IE_SVM_boxville5_Vehicle_Scene"
		CASE WASTELANDER		RETURN "DLC_IE_SVM_wastelander_Vehicle_Scene"
		CASE PHANTOM2			RETURN "DLC_IE_SVM_phantom2_Vehicle_Scene"
		CASE VOLTIC2			RETURN "DLC_IE_SVM_voltic2_Vehicle_Scene"
		CASE DUNE4				RETURN "DLC_IE_SVM_dune4_Vehicle_Scene"
		CASE RUINER2			RETURN "DLC_IE_SVM_ruiner2_Vehicle_Scene"
		CASE BLAZER5			RETURN "DLC_IE_SVM_blazer5_Vehicle_Scene"
	ENDSWITCH
	RETURN ""
ENDFUNC

FUNC INT GET_SVM_INT_FROM_AUDIO_SCENE_FROM_MODEL_NAME(MODEL_NAMES vehModel)
	SWITCH vehModel
		CASE  TECHNICAL2 	RETURN 	ci_SVM_AUDSCENE_TECHNICAL2	
		CASE  BOXVILLE5		RETURN 	ci_SVM_AUDSCENE_BOXVILLE5	
		CASE  WASTELANDER	RETURN 	ci_SVM_AUDSCENE_WASTELANDER	
		CASE  PHANTOM2		RETURN 	ci_SVM_AUDSCENE_PHANTOM2	
		CASE  VOLTIC2		RETURN 	ci_SVM_AUDSCENE_VOLTIC2		
		CASE  DUNE4			RETURN 	ci_SVM_AUDSCENE_DUNE4		
		CASE  RUINER2		RETURN 	ci_SVM_AUDSCENE_RUINER2		
		CASE  BLAZER5		RETURN 	ci_SVM_AUDSCENE_BLAZER5		
	ENDSWITCH
	RETURN -1
ENDFUNC

FUNC STRING GET_SVM_AUDIO_SCENE_FROM_CACHE_INT(INT iAudScene)
	IF iAudScene > -1
		SWITCH iAudScene
			CASE	ci_SVM_AUDSCENE_TECHNICAL2		RETURN "DLC_IE_SVM_technical2_Vehicle_Scene"
			CASE	ci_SVM_AUDSCENE_BOXVILLE5		RETURN "DLC_IE_SVM_boxville5_Vehicle_Scene"
			CASE	ci_SVM_AUDSCENE_WASTELANDER		RETURN "DLC_IE_SVM_wastelander_Vehicle_Scene"
			CASE	ci_SVM_AUDSCENE_PHANTOM2		RETURN "DLC_IE_SVM_phantom2_Vehicle_Scene"
			CASE	ci_SVM_AUDSCENE_VOLTIC2			RETURN "DLC_IE_SVM_voltic2_Vehicle_Scene"
			CASE	ci_SVM_AUDSCENE_DUNE4			RETURN "DLC_IE_SVM_dune4_Vehicle_Scene"
			CASE	ci_SVM_AUDSCENE_RUINER2			RETURN "DLC_IE_SVM_ruiner2_Vehicle_Scene"
			CASE	ci_SVM_AUDSCENE_BLAZER5			RETURN "DLC_IE_SVM_blazer5_Vehicle_Scene"
		ENDSWITCH
	ENDIF
	
	RETURN ""
ENDFUNC

PROC SET_SVM_AUDIO_SCENE()
	
ENDPROC

FUNC BOOL IS_SVM_MISSION_AND_PASSED()
	IF SVM_FLOW_IS_CURRENT_MISSION_SVM_FLOW()
		INT iSVM = SVM_FLOW_GET_CURRENT_MISSION_FLOW_POS()
		
		IF iSVM > -1 AND iSVM < ciSVM_FLOW_MISSION_MAX
			IF SVM_HAS_THIS_MISSION_BEEN_PASSED(iSVM)
				PRINTLN("[IS_SVM_MISSION_AND_PASSED] Mission is iSVM = ", iSVM, " and has passed.")
				RETURN TRUE
			ELSE
				PRINTLN("[IS_SVM_MISSION_AND_PASSED] Mission is iSVM = ", iSVM, " and has not passed.")
			ENDIF
		ELSE
			PRINTLN("[IS_SVM_MISSION_AND_PASSED] iSVM = ", iSVM, " is out of range!")
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_TEAM_SCORE_FOR_DISPLAY(INT iTeam)
	RETURN MC_serverBD.iTeamScore[iTeam] - MC_ServerBD.iPointsGivenToPass[iTeam]
ENDFUNC

PROC GIVE_TEAM_ENOUGH_POINTS_TO_PASS(INT iTeam)
	
	INT iPassScore
	
	IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMissionTargetScore = -1
		IF MC_serverBD.iNumberOfPlayingPlayers[iTeam] > 0
			iPassScore = MC_serverBD.iNumberOfPlayingPlayers[iTeam]
		ELSE
			iPassScore = 1
		ENDIF
	ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMissionTargetScore != 0
		iPassScore = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMissionTargetScore
	ELSE
		iPassScore = 1
		
		IF Is_Player_Currently_On_MP_CTF_Mission(LocalPlayer)
			iPassScore = 0
		ENDIF
	ENDIF
	
	INT iPointsToGive = iPassScore - MC_serverBD.iTeamScore[iTeam]
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_AIRSHOOTOUT(g_FMMC_STRUCT.iAdversaryModeType)
	AND IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH)
		iPointsToGive = 1
	ENDIF
	
	IF iPointsToGive > 0
		MC_ServerBD.iPointsGivenToPass[iTeam] += iPointsToGive
		PRINTLN("[RCC MISSION] GIVE_TEAM_ENOUGH_POINTS_TO_PASS - Giving team ",iTeam," ",iPointsToGive," so they can reach their target score of ",iPassScore," if they haven't already")
		INCREMENT_SERVER_TEAM_SCORE(iTeam, MC_serverBD_4.iCurrentHighestPriority[iTeam], iPointsToGive)
	ELSE
		PRINTLN("[RCC MISSION] GIVE_TEAM_ENOUGH_POINTS_TO_PASS - Team ",iTeam," has already reached a score of at least ",iPassScore," (MC_serverBD.iTeamScore[iteam] = ",MC_serverBD.iTeamScore[iteam])
	ENDIF
	
ENDPROC

FUNC INT GET_TOTAL_JUGGERNAUT_DAMAGE_FOR_TEAM(INT iTeam)
	INT iJuggDamage
	INT iPart
	INT iPlayersChecked
	INT iPlayersToCheck = MC_serverBD.iTotalNumStartingPlayers
	
	FOR iPart = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		IF MC_playerBD[iPart].iTeam >= 0
			iPlayersChecked++
			IF DOES_TEAM_LIKE_TEAM(MC_playerBD[iPart].iTeam, iTeam)
				iJuggDamage += MC_playerBD[iPart].iDamageToJuggernaut
			ENDIF
		ENDIF
		IF iPlayersChecked >= iPlayersToCheck
			BREAKLOOP
		ENDIF
	ENDFOR
	PRINTLN("[JS] GET_TOTAL_JUGGERNAUT_DAMAGE_FOR_TEAM - total damage for team ", iTeam, " = ", iJuggDamage)
	RETURN iJuggDamage
ENDFUNC

FUNC PARTICIPANT_INDEX GET_LAST_PARTICIPANT_ALIVE()
	INT i, iTemp
	PARTICIPANT_INDEX partToReturn
	
	FOR i = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
			PLAYER_INDEX tempPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
			IF IS_NET_PLAYER_OK(tempPlayer, TRUE)
			AND IS_ENTITY_ALIVE(GET_PLAYER_PED(tempPlayer))
			AND NOT IS_PARTICIPANT_A_SPECTATOR(i)
			AND NOT(IS_BIT_SET(MC_playerBD[i].iClientBitSet, PBBOOL_ANY_SPECTATOR) OR IS_BIT_SET(MC_playerBD[i].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN))
			AND NOT IS_BIT_SET(MC_playerBD[i].iClientBitSet,PBBOOL_PLAYER_FAIL)
			AND NOT IS_BIT_SET(MC_playerBD[i].iClientBitSet, PBBOOL_FINISHED)
				iTemp++
				partToReturn = INT_TO_PARTICIPANTINDEX(i)
				IF iTemp > 1
					PRINTLN("[JT] GET_LAST_PARTICIPANT_ALIVE - More than 1 player is alive.")
					BREAKLOOP
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
				IF (GET_FRAME_COUNT() % 30) = 0
					PRINTLN("[JT TR] Participant: ", i," IS_NET_PLAYER_OK() = ", IS_NET_PLAYER_OK(tempPlayer, TRUE), " IS_ENTITY_ALIVE = ", IS_ENTITY_ALIVE(GET_PLAYER_PED(tempPlayer)), " IS_PARTICIPANT_A_SPECTATOR = ", IS_PARTICIPANT_A_SPECTATOR(i))
				ENDIF
				#ENDIF
			ENDIF
		ENDIF
	ENDFOR
	IF iTemp = 1
		PRINTLN("[JT] GET_LAST_PARTICIPANT_ALIVE - Only one player left alive.")
		RETURN partToReturn
	ENDIF
	RETURN INT_TO_PARTICIPANTINDEX(-1)
ENDFUNC

FUNC BOOL ARE_ALL_PARTICIPANTS_ALIVE()
	INT i, iTemp
	INT iPlayersToCheck = GET_NUMBER_OF_PLAYING_PLAYERS()
	INT iPlayersChecked
	
	FOR i = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		IF NOT(IS_BIT_SET(MC_playerBD[i].iClientBitSet, PBBOOL_ANY_SPECTATOR) OR IS_BIT_SET(MC_playerBD[i].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN))
		AND NOT IS_BIT_SET(MC_playerBD[i].iClientBitSet,PBBOOL_PLAYER_FAIL)
		AND IS_BIT_SET(MC_playerBD[i].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
		AND NOT IS_BIT_SET(MC_playerBD[i].iClientBitSet, PBBOOL_FINISHED)
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
				PRINTLN("[JT] Processing participant: ", i)
				iPlayersChecked++
				PLAYER_INDEX tempPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
				IF IS_NET_PLAYER_OK(tempPlayer, TRUE)
					iTemp++
					IF iTemp >= iPlayersToCheck
						PRINTLN("[JT] ARE_ALL_PARTICIPANTS_ALIVE - All Participants alive")
						RETURN TRUE
					ENDIF
					
				ENDIF
			ENDIF
		ENDIF
		IF iPlayersChecked >= iPlayersToCheck
			BREAKLOOP
		ENDIF
	ENDFOR
	PRINTLN("[JT] ARE_ALL_PARTICIPANTS_ALIVE - GET_NUMBER_OF_PLAYING_PLAYERS(): ", iPlayersToCheck, " iTemp: ", iTemp)
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_CURRENT_ACTIVE_TEAM_SPAWN_POINT()
	INT iTeamSpawnPointCounter
	INT iTeam = MC_playerBD[iLocalPart].iteam
	
	IF MC_ServerBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
		FOR iTeamSpawnPointCounter = 0 TO FMMC_MAX_TEAMSPAWNPOINTS-1
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iTeam][iTeamSpawnPointCounter].iSpawnBitset, ci_SpawnBS_Treat_As_Respawn_Point)
				PRINTLN("[JT SP] GET_CURRENT_ACTIVE_TEAM_SPAWN_POINT - 1, spawn point ", iTeamSpawnPointCounter," is a respawn point")
				 IF IS_RULE_VALID_FOR_CUSTOM_SPAWN_POINT(iTeamSpawnPointCounter, iTeam, MC_ServerBD_4.iCurrentHighestPriority[iTeam])
				 	PRINTLN("[JT SP] GET_CURRENT_ACTIVE_TEAM_SPAWN_POINT - 2, spawn point ", iTeamSpawnPointCounter," is valid for the current rule")
				 	IF NOT IS_BIT_SET(iBSRespawnDeactivated[iTeam][GET_LONG_BITSET_INDEX(iTeamSpawnPointCounter)], GET_LONG_BITSET_BIT(iTeamSpawnPointCounter))
						PRINTLN("[JT SP] GET_CURRENT_ACTIVE_TEAM_SPAWN_POINT - 3, spawn point ", iTeamSpawnPointCounter," hasn't been deactivated, return this spawn point!")
						RETURN iTeamSpawnPointCounter
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	RETURN -1
ENDFUNC

FUNC INT GET_NUMBER_OF_LIVING_PARTICIPANTS()
	INT iPart, iNumPlayers
	FOR iPart = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		IF IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(iPart))
		AND NOT IS_PARTICIPANT_A_SPECTATOR(iPart)
		AND (NOT(IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_ANY_SPECTATOR)) OR IS_BIT_SET(MC_playerBD[iPart].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN))
		AND NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet,PBBOOL_PLAYER_FAIL)
		AND IS_BIT_SET(MC_playerBD[iPart].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
		AND NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_FINISHED)
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPart))
				iNumPlayers++
			ENDIF
		ENDIF
	ENDFOR
	RETURN iNumPlayers
ENDFUNC

FUNC BOOL IS_ANY_TEAM_ATTEMPTING_TO_RESTART_RULE()
	INT iTeam
	FOR iTeam = 0 TO FMMC_MAX_TEAMS - 1
		IF IS_BIT_SET(MC_serverBD.iServerBitSet5, SBBOOL5_RETRY_CURRENT_RULE_TEAM_0 + iTeam)
			RETURN TRUE
		ENDIF
	ENDFOR
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ANY_TEAM_BEING_WARPED_TO_START_POINT()
	INT iTeam
	FOR iTeam = 0 TO FMMC_MAX_TEAMS - 1
		IF IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_TEAM_0_SHOULD_RESPAWN_BACK_AT_START_NOW + iTeam)
			RETURN TRUE
		ENDIF
	ENDFOR
	RETURN FALSE
ENDFUNC

FUNC BOOL SET_DRIVER_BACK_INTO_VEHICLE(VEHICLE_INDEX vehOverride)
	
	CPRINTLN( DEBUG_CONTROLLER, "[LM][SET_DRIVER_BACK_INTO_VEHICLE][SPEC_SPEC] - Called.")

	VEHICLE_INDEX veh = vehOverride
	
	IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(veh)
		CPRINTLN( DEBUG_CONTROLLER, "[LM][SET_DRIVER_BACK_INTO_VEHICLE][SPEC_SPEC] - Requesting Control.")
		NETWORK_REQUEST_CONTROL_OF_ENTITY(veh)
		RETURN FALSE
	ELSE
		CPRINTLN( DEBUG_CONTROLLER, "[LM][SET_DRIVER_BACK_INTO_VEHICLE][SPEC_SPEC] - We have control.")
	ENDIF

	IF DOES_ENTITY_EXIST(veh)
		IF IS_VEHICLE_DRIVEABLE(veh)
			CPRINTLN( DEBUG_CONTROLLER, "[LM][SET_DRIVER_BACK_INTO_VEHICLE][SPEC_SPEC] - Vehicle Target: ", NATIVE_TO_INT(veh))
								
			IF NOT IS_PED_IN_ANY_VEHICLE_OR_WAITING_TO_START_TASK_ENTER_VEHICLE(localPlayerPed)
				// check player is near to vehicle
				VECTOR vPlayerCoords = GET_ENTITY_COORDS(localPlayerPed, FALSE)
				VECTOR vCarCoords = GET_ENTITY_COORDS(veh, FALSE)
				
				CPRINTLN( DEBUG_CONTROLLER,"[LM][SET_DRIVER_BACK_INTO_VEHICLE][SPEC_SPEC] - warpPlayerIntoCar - vPlayerCoords = ") 
				CPRINTLN( DEBUG_CONTROLLER, vPlayerCoords, ", vCarCoords = ", vCarCoords)
				
		 		IF (VDIST(vPlayerCoords, vCarCoords) > POW(150.0, 2))
					vCarCoords.y += 2.0
					SET_ENTITY_COORDS(localPlayerPed, vCarCoords, FALSE)
					CPRINTLN( DEBUG_CONTROLLER, "[LM][SET_DRIVER_BACK_INTO_VEHICLE][SPEC_SPEC] - warpPlayerIntoCar - moving player closer to car.")
				ENDIF
										
				CPRINTLN( DEBUG_CONTROLLER, "[LM][SET_DRIVER_BACK_INTO_VEHICLE][SPEC_SPEC] - Calling to start task: Enter Vehicle")
				
				FREEZE_ENTITY_POSITION(veh, FALSE)
				SET_ENTITY_COLLISION(veh, TRUE)
				
				CLEAR_PED_TASKS_IMMEDIATELY(localPlayerPed)
				
				FORCE_PED_AI_AND_ANIMATION_UPDATE(localPlayerPed)
				SET_ENTITY_COLLISION(localPlayerPed, TRUE)
				FREEZE_ENTITY_POSITION(localPlayerPed, FALSE)
				SET_PED_CAN_RAGDOLL(localPlayerPed, FALSE)
				SET_PED_RESET_FLAG(localPlayerPed, PRF_SuppressInAirEvent, TRUE) // 3309718
				TASK_ENTER_VEHICLE(LocalPlayerPed, veh, -1, VS_DRIVER, DEFAULT, ECF_WARP_PED)
				SET_PED_RESET_FLAG(localPlayerPed, PRF_SuppressInAirEvent, TRUE) // 31146107
			ELSE
				CPRINTLN( DEBUG_CONTROLLER,"[LM][SET_DRIVER_BACK_INTO_VEHICLE][SPEC_SPEC] - Waiting to enter veh.... ") 
			ENDIF
		ELSE
			CPRINTLN( DEBUG_CONTROLLER, "[LM][SET_DRIVER_BACK_INTO_VEHICLE][SPEC_SPEC] - Not Driveable")
		ENDIF
	ELSE		
		CPRINTLN( DEBUG_CONTROLLER, "[LM][SET_DRIVER_BACK_INTO_VEHICLE][SPEC_SPEC] - No Vehicle Target.")
	ENDIF

	IF IS_PED_IN_VEHICLE(localPlayerPed, veh)
		CPRINTLN( DEBUG_CONTROLLER, "[LM][SET_DRIVE_BACK_INTO_VEHICLE][SPEC_SPEC] - in vehicle:						returning TRUE")
		RETURN TRUE
	ELSE
		CPRINTLN( DEBUG_CONTROLLER, "[LM][SET_DRIVE_BACK_INTO_VEHICLE][SPEC_SPEC] - not in vehicle:					returning FALSE")
		RETURN FALSE
	ENDIF
ENDFUNC

FUNC BOOL SET_PASSENGER_INTO_VEHICLE()
	CPRINTLN( DEBUG_CONTROLLER, "[LM][MULTI_VEH_RESPAWN] - SET_PASSENGER_INTO_VEHICLE - Called.")
	
	INT iDriverPart = GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(GET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER())
	
	IF iDriverPart > -1
		CPRINTLN( DEBUG_CONTROLLER, "[LM][MULTI_VEH_RESPAWN] - SET_PASSENGER_INTO_VEHICLE - (Final) iDriverPart: ", iDriverPart)
		
		VEHICLE_INDEX veh
		PED_INDEX ped = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iDriverPart)))
		
		//Part checks already performed in the above functions.	
		IF NOT IS_PED_INJURED(ped)
		AND NOT IS_PLAYER_RESPAWNING(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iDriverPart)))
			IF IS_PED_IN_ANY_VEHICLE(ped)
				CPRINTLN( DEBUG_CONTROLLER, "[LM][MULTI_VEH_RESPAWN] - SET_PASSENGER_INTO_VEHICLE - Target is in Vehicle.")
				
				veh = GET_VEHICLE_PED_IS_IN(ped)
				
				PARTICIPANT_INDEX piPart
				PLAYER_INDEX piPlayer
				PED_INDEX pedPlayer
				
				// Stops the players from caching the same position.
				BOOL bSafe = TRUE
				INT iPartner = 0
				FOR iPartner = 0 TO MAX_VEHICLE_PARTNERS-1
					IF GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iVehiclePartners[iPartner] > -1
						piPart = INT_TO_PARTICIPANTINDEX(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iVehiclePartners[iPartner])
						
						IF NETWORK_IS_PARTICIPANT_ACTIVE(piPart)
						AND GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iVehiclePartners[iPartner] > -1
						AND GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iVehiclePartners[iPartner] != iLocalPart
							piPlayer = NETWORK_GET_PLAYER_INDEX(piPart)
							pedPlayer = GET_PLAYER_PED(piPlayer)
							
							IF IS_PED_IN_ANY_VEHICLE_OR_WAITING_TO_START_TASK_ENTER_VEHICLE(pedPlayer)
							AND NOT IS_PED_IN_ANY_VEHICLE(pedPlayer, FALSE)
								bSafe = FALSE
							ENDIF
						ENDIF
					ENDIF
				ENDFOR
				
				IF DOES_ENTITY_EXIST(veh)
				AND IS_VEHICLE_DRIVEABLE(veh)
					CPRINTLN( DEBUG_CONTROLLER, "[LM][MULTI_VEH_RESPAWN] - SET_PASSENGER_INTO_VEHICLE - Vehicle Target: ", NATIVE_TO_INT(veh))
										
					IF NOT IS_PED_IN_ANY_VEHICLE_OR_WAITING_TO_START_TASK_ENTER_VEHICLE(localPlayerPed)
					AND bSafe
						// check player is near to vehicle
						VECTOR vPlayerCoords = GET_ENTITY_COORDS(localPlayerPed, FALSE)
						VECTOR vCarCoords = GET_ENTITY_COORDS(veh, FALSE)
						
						CPRINTLN( DEBUG_CONTROLLER,"[LM][MULTI_VEH_RESPAWN] - SET_PASSENGER_INTO_VEHICLE WarpPlayerIntoCar - vPlayerCoords = ") 
						CPRINTLN( DEBUG_CONTROLLER, vPlayerCoords, ", vCarCoords = ", vCarCoords)
						
				 		IF NOT (VDIST(vPlayerCoords, vCarCoords) < 5.0)
							vCarCoords.z += -4.0

							SET_ENTITY_COORDS(localPlayerPed, vCarCoords, FALSE)
							CPRINTLN( DEBUG_CONTROLLER, "[LM][MULTI_VEH_RESPAWN] - SET_PASSENGER_INTO_VEHICLE WarpPlayerIntoCar - moving player closer to car.")
						ENDIF
												
						CPRINTLN( DEBUG_CONTROLLER, "[LM][MULTI_VEH_RESPAWN] - SET_PASSENGER_INTO_VEHICLE - Calling to start task: Enter Vehicle")
						
						BOOL bShouldIgnoreTurretSeat = (GlobalplayerBD_FM[iLocalPart].sClientCoronaData.iPreferredRole = ciMISSION_CLIENT_SEAT_OPTION_PASSENGER_REGULAR)						
						
						FORCE_PED_AI_AND_ANIMATION_UPDATE(localPlayerPed)						
						SET_ENTITY_COLLISION(localPlayerPed, TRUE)	
						FREEZE_ENTITY_POSITION(localPlayerPed, FALSE)	
						SET_PED_CAN_RAGDOLL(localPlayerPed, FALSE)
						SET_PED_RESET_FLAG(localPlayerPed, PRF_SuppressInAirEvent, TRUE) // 3309718
						TASK_ENTER_VEHICLE(LocalPlayerPed, veh, 1, INT_TO_ENUM(VEHICLE_SEAT, GetFirstFreeSeatOfVehicle(veh, TRUE, bShouldIgnoreTurretSeat)), DEFAULT, ECF_WARP_PED)
						SET_PED_RESET_FLAG(localPlayerPed, PRF_SuppressInAirEvent, TRUE) // 31146107						
					ENDIF
				ELSE
					CPRINTLN( DEBUG_CONTROLLER, "[LM][MULTI_VEH_RESPAWN] - SET_PASSENGER_INTO_VEHICLE - No Vehicle Target.")
				ENDIF
			ELSE
				CPRINTLN( DEBUG_CONTROLLER, "[LM][MULTI_VEH_RESPAWN] - SET_PASSENGER_INTO_VEHICLE - Target Ped is not in vehicle yet.")
			ENDIF
		ELSE
			CPRINTLN( DEBUG_CONTROLLER, "[LM][MULTI_VEH_RESPAWN] - SET_PASSENGER_INTO_VEHICLE - Target Ped is dead or Respawning.")
		ENDIF
	ELSE
		CPRINTLN( DEBUG_CONTROLLER, "[LM][MULTI_VEH_RESPAWN] - SET_PASSENGER_INTO_VEHICLE - Something went wrong becase iDriverPart = ", iDriverPart)
	ENDIF

	IF IS_PED_IN_ANY_VEHICLE(localPlayerPed)
	AND iDriverPart > -1
		CPRINTLN( DEBUG_CONTROLLER, "[LM][MULTI_VEH_RESPAWN] - SET_PASSENGER_INTO_VEHICLE - in vehicle:						returning TRUE")
		RETURN TRUE
	ELSE
		CPRINTLN( DEBUG_CONTROLLER, "[LM][MULTI_VEH_RESPAWN] - SET_PASSENGER_INTO_VEHICLE - not in vehicle:					returning FALSE")
		RETURN FALSE
	ENDIF
ENDFUNC

FUNC BOOL USING_MULTIPLE_PASSENGER_TEAM_VEHICLE(INT iPlayerTeam)
	IF g_FMMC_STRUCT.iTeamVehicleRespawnPassengers[iPlayerTeam] >= 1
	AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iPlayerTeam].iTeamBitSet3, ciBS3_ENABLE_MULTIPLE_PLAYER_SAME_VEHICLE_RESPAWN)
		CPRINTLN( DEBUG_CONTROLLER, "[LM][MULTI_VEH_RESPAWN] USING_MULTIPLE_PASSENGER_TEAM_VEHICLE - Using System, iPlayerTeam: ", iPlayerTeam, " g_FMMC_STRUCT.iTeamVehicleRespawnPassengers: ", g_FMMC_STRUCT.iTeamVehicleRespawnPassengers[iPlayerTeam])
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_DRIVER_FOR_MULTIPLE_TEAM_VEHICLE(INT iPlayerTeam)	
	IF iPlayerTeam > -1
		IF USING_MULTIPLE_PASSENGER_TEAM_VEHICLE(iPlayerTeam)		
			IF GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(GET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER()) = iLocalPart
				CPRINTLN( DEBUG_CONTROLLER, "[LM][MULTI_VEH_RESPAWN] IS_DRIVER_FOR_MULTIPLE_TEAM_VEHICLE - Returning True.")
				RETURN TRUE
			ENDIF
			
			CPRINTLN( DEBUG_CONTROLLER, "[LM][MULTI_VEH_RESPAWN] IS_DRIVER_FOR_MULTIPLE_TEAM_VEHICLE - Returning False ")
			
			RETURN FALSE
		ENDIF
	ENDIF
	RETURN TRUE
ENDFUNC

PROC FLASH_OUT_OF_BOUNDS_PLAYER_BLIP()
	INT iPlayer
	FOR iPlayer = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		IF HAS_NET_TIMER_STARTED(MC_playerBD[iPlayer].tdBoundstimer)
			IF NOT IS_BLIP_FLASHING_FOR_PLAYER(INT_TO_PLAYERINDEX(iPlayer))
				FLASH_BLIP_FOR_PLAYER(INT_TO_PLAYERINDEX(iPlayer), TRUE, 1000)
			ENDIF
		ELSE
			IF IS_BLIP_FLASHING_FOR_PLAYER(INT_TO_PLAYERINDEX(iPlayer))
				FLASH_BLIP_FOR_PLAYER(INT_TO_PLAYERINDEX(iPlayer), FALSE, 0)
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

FUNC BOOL DOES_TEAM_HAVE_ANY_PLAYERS_ALIVE(INT iTeam)
	INT i, iPlayersToCheck, iPlayersChecked
	iPlayersToCheck = MC_serverBD.iNumberOfPlayingPlayers[iTeam]
	FOR i = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		IF (NOT(IS_BIT_SET(MC_playerBD[i].iClientBitSet, PBBOOL_ANY_SPECTATOR)) OR IS_BIT_SET(MC_playerBD[i].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN))
		AND NOT IS_BIT_SET(MC_playerBD[i].iClientBitSet,PBBOOL_PLAYER_FAIL)
		AND IS_BIT_SET(MC_playerBD[i].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
		AND NOT IS_BIT_SET(MC_playerBD[i].iClientBitSet, PBBOOL_FINISHED)
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
			AND MC_playerBD[i].iteam = iTeam
				iPlayersChecked++
				PLAYER_INDEX tempPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
				IF IS_ENTITY_ALIVE(GET_PLAYER_PED(tempPlayer))
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
		IF iPlayersChecked >= iPlayersToCheck
			BREAKLOOP
		ENDIF
	ENDFOR
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_TEAM_HAVE_ANY_PLAYERS_DEAD(INT iTeam)
	INT i, iPlayersToCheck, iPlayersChecked
	iPlayersToCheck = MC_serverBD.iNumberOfPlayingPlayers[iTeam]
	FOR i = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		IF (NOT(IS_BIT_SET(MC_playerBD[i].iClientBitSet, PBBOOL_ANY_SPECTATOR)) OR IS_BIT_SET(MC_playerBD[i].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN))
		AND NOT IS_BIT_SET(MC_playerBD[i].iClientBitSet,PBBOOL_PLAYER_FAIL)
		AND IS_BIT_SET(MC_playerBD[i].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
		AND NOT IS_BIT_SET(MC_playerBD[i].iClientBitSet, PBBOOL_FINISHED)
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
			AND MC_playerBD[i].iteam = iTeam
				iPlayersChecked++
				PLAYER_INDEX tempPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
				IF NOT IS_ENTITY_ALIVE(GET_PLAYER_PED(tempPlayer))
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
		IF iPlayersChecked >= iPlayersToCheck
			BREAKLOOP
		ENDIF
	ENDFOR
	RETURN FALSE
ENDFUNC

PROC SET_VEHICLE_HEALTH_FROM_RULE_AND_TEAM(INT iTeam, INT iRule, VEHICLE_INDEX veh)
	INT iVehHealth
	iVehHealth = GET_FMMC_PLAYER_VEHICLE_HEALTH_FOR_TEAM_AND_RULE(iTeam, iRule)
	SET_VEHICLE_FIXED(veh)
	SET_ENTITY_HEALTH(veh, iVehHealth)
	SET_VEHICLE_WEAPON_DAMAGE_SCALE(veh, GET_FMMC_PLAYER_VEHICLE_WEAPON_DAMAGE_SCALE_FOR_TEAM(iTeam))
	SET_VEHICLE_ENGINE_HEALTH(veh, iVehHealth * 1.0)
	SET_VEHICLE_PETROL_TANK_HEALTH(veh, iVehHealth * 1.0)
	
	IF iVehHealth >= 3000 
		SET_VEHICLE_EXPLODES_ON_HIGH_EXPLOSION_DAMAGE(veh,FALSE)
	ENDIF
ENDPROC

PROC SET_VEHICLE_WEAPON_MODS_FROM_RULE_AND_TEAM(INT iTeam, INT iRule, VEHICLE_INDEX veh)
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTen[iRule], ciBS_RULE10_VEHICLE_SWAP_MODS)
	OR IS_BIT_SET(MC_serverBD.iServerBitSet6, SBBOOL6_AIRQUOTA_SEQUENCE_TOGGLE_CORONA)
		INT iVeh = -1
		INT iVModTurret = MC_serverBD_4.sVehicleSwaps[iTeam][iRule].iVehicleTurretSwap
		INT iVModArmour = MC_serverBD_4.sVehicleSwaps[iTeam][iRule].iVehicleArmourSwap
		INT iVModFrontBumper = MC_serverBD_4.sVehicleSwaps[iTeam][iRule].iVehicleAirCounterSwap
		INT iVModExhaust = MC_serverBD_4.sVehicleSwaps[iTeam][iRule].iVehicleExhaustSwap
		INT iVModBombbay = MC_serverBD_4.sVehicleSwaps[iTeam][iRule].iVehicleBombBaySwap
		INT iVModSpoiler = -1
		INT iVehBitset = 0
		BOOL bForceUseBombBayMod = FALSE
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTen[iRule], ciBS_RULE10_VEHICLE_SWAP_BOMB_BAY)
			bForceUseBombBayMod = TRUE
		ENDIF
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTen[iRule], ciBS_RULE10_VEHICLE_SWAP_USE_REAR_WEAPON_MOD)
			SET_BIT(iVehBitset, ciFMMC_VEHICLE5_USE_REAR_WEAPON_MOD)
		ENDIF
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTen[iRule], ciBS_RULE10_VEHICLE_SWAP_USE_FRONT_WEAPON_MOD)
			SET_BIT(iVehBitset, ciFMMC_VEHICLE5_USE_FRONT_WEAPON_MOD)
		ENDIF
		PRINTLN("[VehSwap][SET_VEHICLE_WEAPON_MODS_FROM_RULE_AND_TEAM] iVModTurret: ", iVModTurret,
			" iVModArmour: ", iVModArmour, " iVehBitset: ", iVehBitset, " iVModFrontBumper: ", iVModFrontBumper, " iVModExhaust: ", iVModExhaust,
			" iVModBombbay: ", iVModBombbay, " iVModSpoiler: ", iVModSpoiler, " bForceUseBombBayMod: ", bForceUseBombBayMod)
		SET_VEHICLE_WEAPON_MODS(veh, iVModTurret, iVModArmour, iVehBitset, iVeh, iVModFrontBumper, iVModExhaust,
			iVModBombbay, iVModSpoiler, iTeam, bForceUseBombBayMod)
	ENDIF
ENDPROC

FUNC PLAYER_INDEX PICK_PLAYER_ID(BOOL bCheck, PLAYER_INDEX piTrue, PLAYER_INDEX piFalse)
	IF bCheck
		RETURN piTrue
	ELSE
		RETURN piFalse
	ENDIF
ENDFUNC

FUNC INT GET_LOCATION_FROM_RULE(INT iRule, INT iTeam)
	INT i
	FOR i = 0 TO FMMC_MAX_RULES - 1
		IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].iPriority[iTeam] = iRule
			RETURN i
		ENDIF
	ENDFOR
	RETURN -1
ENDFUNC

FUNC BOOL OVERTIME_DID_ANYONE_SUCCESSFULLY_LAND()
	INT i = 0
	INT iPlayerCount
	INT iPlayerMax = GET_NUMBER_OF_PLAYING_PLAYERS()
	FOR i = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		IF IS_NET_PLAYER_OK(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i)))
		AND NOT IS_PLAYER_SPECTATOR_ONLY(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i)))
			IF IS_BIT_SET(MC_PlayerBD[i].iClientBitset3, PBBOOL3_OVERTIME_LANDED_SUCCESSFULLY)
				RETURN TRUE
			ENDIF
			iPlayerCount++
		ENDIF		
			
		IF iPlayerCount >= iPlayerMax
			BREAKLOOP
		ENDIF
	ENDFOR
		
	RETURN FALSE
ENDFUNC

FUNC INT GET_OVERTIME_SUCCESSFUL_LANDER()
	INT i = 0
	INT iPlayerCount
	INT iPlayerMax = GET_NUMBER_OF_PLAYING_PLAYERS()
	FOR i = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		IF IS_NET_PLAYER_OK(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i)))
		AND NOT IS_PLAYER_SPECTATOR_ONLY(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i)))
			IF IS_BIT_SET(MC_PlayerBD[i].iClientBitset3, PBBOOL3_OVERTIME_LANDED_SUCCESSFULLY)
				RETURN i
			ENDIF
			iPlayerCount++
		ENDIF
			
		IF iPlayerCount >= iPlayerMax
			BREAKLOOP
		ENDIF
	ENDFOR
		
	RETURN -1
ENDFUNC

FUNC BOOL ARE_ANY_TEAMS_DRAWING_ON_SCORE()	
	INT iTeam
	INT iHighestScore = -1
	
	FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
		IF IS_TEAM_ACTIVE(iTeam)
			IF MC_serverBD.iTeamScore[iTeam] > iHighestScore
				iHighestScore = MC_serverBD.iTeamScore[iTeam]
			ELIF MC_serverBD.iTeamScore[iTeam] = iHighestScore
				RETURN TRUE
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_VEHICLE_STOPPED(VEHICLE_INDEX tempVeh)
	
	IF GET_ENTITY_SPEED(tempVeh) < 2.0
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL HAS_VEHICLE_STOPPED_VELOCITY_ANGULAR(VEHICLE_INDEX tempVeh)

	IF VMAG2(GET_ENTITY_ROTATION_VELOCITY(tempVeh)) < 0.07
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL HAS_VEHICLE_STOPPED_VELOCITY(VEHICLE_INDEX tempVeh)
	
	IF VMAG2(GET_ENTITY_VELOCITY(tempVeh)) < 0.1
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC


FUNC BOOL IS_PROP_IN_LIST_OF_LOCATE_LINKED_PROPS(INT iProp, INT iProp2, INT iProp3)
	INT iLocate
	
	FOR iLocate = 0 TO FMMC_MAX_GO_TO_LOCATIONS-1
		//Primary Selection
		IF (g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLocate].iLocatePropLinkIndex = iProp AND iProp > -1)
		OR (g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLocate].iLocatePropLinkIndex = iProp2 AND iProp2 > -1)
		OR (g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLocate].iLocatePropLinkIndex = iProp3 AND iProp3 > -1)
		
		// Secondary Selection
		OR (g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLocate].iLocatePropLinkIndexSecondary = iProp AND iProp > -1)
		OR (g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLocate].iLocatePropLinkIndexSecondary = iProp2 AND iProp2 > -1)
		OR (g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLocate].iLocatePropLinkIndexSecondary = iProp3 AND iProp3 > -1)
		
		// Tertiary Selection
		OR (g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLocate].iLocatePropLinkIndexTertiary = iProp AND iProp > -1)
		OR (g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLocate].iLocatePropLinkIndexTertiary = iProp2 AND iProp2 > -1)
		OR (g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLocate].iLocatePropLinkIndexTertiary = iProp3 AND iProp3 > -1)
		
			RETURN TRUE
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC

//Checking if players have stopped by default.
FUNC BOOL OVERTIME_HAVE_ALL_PLAYERS_FINISHED_TURN(BOOL bCheckLanded = FALSE)
	INT i, iPlayersChecked
	INT iPlayersToCheck = GET_NUMBER_OF_PLAYING_PLAYERS()
	
	FOR i = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		IF(NOT(IS_BIT_SET(MC_playerBD[i].iClientBitSet, PBBOOL_ANY_SPECTATOR)) OR (IS_BIT_SET(MC_playerBD[i].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN) OR IS_BIT_SET(MC_playerBD[i].iClientBitSet,PBBOOL_HEIST_SPECTATOR)))
		AND NOT IS_BIT_SET(MC_playerBD[i].iClientBitSet,PBBOOL_PLAYER_FAIL)
		AND IS_BIT_SET(MC_playerBD[i].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
		AND NOT IS_BIT_SET(MC_playerBD[i].iClientBitSet, PBBOOL_FINISHED)
			PARTICIPANT_INDEX tempPart = INT_TO_PARTICIPANTINDEX(i)
			IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
				PLAYER_INDEX tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
				iPlayersChecked++
				PED_INDEX tempPed = GET_PLAYER_PED(tempPlayer)
				
				// If we're on a valid prop but have not stopped and we are not in the air, then do not pass the rule yet.
				IF NOT IS_PED_INJURED(tempPed)
					VEHICLE_INDEX veh
			
					IF IS_PED_IN_ANY_VEHICLE(tempPed)
						Veh = GET_VEHICLE_PED_IS_IN(tempPed)
					ENDIF
					
					IF DOES_ENTITY_EXIST(veh)
						PRINTLN("[LM][OVERTIME] - OVERTIME_HAVE_ALL_PLAYERS_FINISHED_TURN: iCurrentPropHit: ", MC_PlayerBD[iOTPartToMove].iCurrentPropHit[0])
						
						// If someone is on the prop, and has not stopped, return false.
						IF IS_PROP_IN_LIST_OF_LOCATE_LINKED_PROPS(MC_PlayerBD[i].iCurrentPropHit[0], MC_PlayerBD[i].iCurrentPropHit[1], MC_PlayerBD[i].iCurrentPropHit[2]) AND NOT HAS_VEHICLE_STOPPED_VELOCITY(Veh)
							PRINTLN("[LM][OVERTIME] - OVERTIME_HAVE_ALL_PLAYERS_FINISHED_TURN: Returning False: IS_PROP_IN_LIST_OF_LOCATE_LINKED_PROPS")
							RETURN FALSE
						ENDIF
					ENDIF
				ENDIF
					
				IF bCheckLanded
					IF NOT IS_BIT_SET(MC_PlayerBD[i].iClientBitset3, PBBOOL3_OVERTIME_LANDED_SUCCESSFULLY)
					AND NOT IS_PED_INJURED(tempPed) //url:bugstar:3469511
						RETURN FALSE
					ENDIF
				ENDIF
			
				IF iPlayersChecked >= iPlayersToCheck
					BREAKLOOP
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN TRUE
ENDFUNC

FUNC BOOL CAN_LAP_BASED_TEAM_SCORE_SCORE_BE_MATCHED(INT iTeam, INT &iTeamWinner)
	
	INT iEnemyTeam = PICK_INT(iTeam = 0, 1, 0)
	INT iTeamLapsCompleted = PICK_INT((MC_ServerBD_4.iTeamLapsCompleted[0] > MC_ServerBD_4.iTeamLapsCompleted[1]), MC_ServerBD_4.iTeamLapsCompleted[0], MC_ServerBD_4.iTeamLapsCompleted[1]) // Laps are progressed together, but on separate frames.
	
	INT iThisTeamScore
	INT iEnemyTeamScore
	iThisTeamScore = MC_serverBD.iTeamScore[iTeam]
	iEnemyTeamScore = MC_serverBD.iTeamScore[iEnemyTeam]
	INT iRoundsRemainingMyTeam = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMissionLapLimit - iTeamLapsCompleted
		
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_OFFSET_TEAM_ATTEMPTS_BY_RULE_INCREMENT_T1)
		IF iTeam = 0
		AND MC_serverBD_4.iCurrentHighestPriority[iTeam] = 1
			iRoundsRemainingMyTeam -= 1
		ENDIF
	ENDIF
	
	PRINTLN("[LM] CAN_LAP_BASED_TEAM_SCORE_SCORE_BE_MATCHED - iThisTeamLapsCompleted = ", MC_ServerBD_4.iTeamLapsCompleted[iTeam])
	PRINTLN("[LM] CAN_LAP_BASED_TEAM_SCORE_SCORE_BE_MATCHED - iEnemyTeamLapsCompleted = ", MC_ServerBD_4.iTeamLapsCompleted[iEnemyTeam])
	
	PRINTLN("[LM] CAN_LAP_BASED_TEAM_SCORE_SCORE_BE_MATCHED - iThisTeamRule = ", MC_serverBD_4.iCurrentHighestPriority[iTeam])
	PRINTLN("[LM] CAN_LAP_BASED_TEAM_SCORE_SCORE_BE_MATCHED - iEnemyTeamRule = ", MC_serverBD_4.iCurrentHighestPriority[iEnemyTeam])
	
	PRINTLN("[LM] CAN_LAP_BASED_TEAM_SCORE_SCORE_BE_MATCHED - iRoundsRemainingMyTeam = ", iRoundsRemainingMyTeam)
	PRINTLN("[LM] CAN_LAP_BASED_TEAM_SCORE_SCORE_BE_MATCHED - iThisTeamScore = ", iThisTeamScore)
	PRINTLN("[LM] CAN_LAP_BASED_TEAM_SCORE_SCORE_BE_MATCHED - iEnemyTeamScore = ", iEnemyTeamScore)
	
	IF MC_serverBD_4.iCurrentHighestPriority[iTeam] != MC_serverBD_4.iCurrentHighestPriority[iEnemyTeam]
	OR MC_ServerBD_4.iTeamLapsCompleted[iTeam] != MC_ServerBD_4.iTeamLapsCompleted[iEnemyTeam]
	OR (HAS_NET_TIMER_STARTED(tdScoreAndLapSyncTimer)
	AND NOT HAS_NET_TIMER_EXPIRED_READ_ONLY(tdScoreAndLapSyncTimer, ci_SCORE_AND_LAP_SYNC_TIMER))	
		PRINTLN("[LM] CAN_LAP_BASED_TEAM_SCORE_SCORE_BE_MATCHED - Returning true to exit as we're not yet rule synced.")
		RETURN TRUE
	ENDIF
	
	IF (iThisTeamScore + iRoundsRemainingMyTeam) >= iEnemyTeamScore
	OR iTeamLapsCompleted >= g_FMMC_STRUCT.sFMMCEndConditions[iteam].iMissionLapLimit
		RETURN TRUE
	ENDIF
	
	iTeamWinner = iEnemyTeam
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CAN_BOMBUSHKA_TEAM_SCORE_BE_MATCHED(INT iTeam)
	INT iEnemyTeam = PICK_INT(iTeam = 0, 1, 0)
	
	INT iThisTeamScore
	INT iEnemyTeamScore
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciBOMBUSHKA_GIVE_POINT_TO_WINNER)
		iThisTeamScore = MC_serverBD.iCachedRuleScore[iTeam]
		iEnemyTeamScore = MC_serverBD.iCachedRuleScore[iEnemyTeam]
	ELSE
		iThisTeamScore = MC_serverBD.iCachedBestRuleScore[iTeam]
		iEnemyTeamScore = MC_serverBD.iCachedBestRuleScore[iEnemyTeam]
	ENDIF	
	
	INT iRoundsRemaining = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMissionLapLimit - MC_ServerBD_4.iTeamLapsCompleted[iTeam]
	INT iMaxScore = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMissionLapLimit
	
	PRINTLN("[LM] CAN_BOMBUSHKA_TEAM_SCORE_BE_MATCHED - iRoundsRemaining = ", iRoundsRemaining)
	PRINTLN("[LM] CAN_BOMBUSHKA_TEAM_SCORE_BE_MATCHED - iThisTeamScore = ", iThisTeamScore)
	PRINTLN("[LM] CAN_BOMBUSHKA_TEAM_SCORE_BE_MATCHED - iEnemyTeamScore = ", iEnemyTeamScore)
	PRINTLN("[LM] CAN_BOMBUSHKA_TEAM_SCORE_BE_MATCHED - iMaxScore = ", iMaxScore)
	
	IF iEnemyTeamScore + (iRoundsRemaining * iMaxScore) >= iThisTeamScore
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CAN_OVERTIME_TEAM_SCORE_BE_MATCHED(INT iTeam)
	INT iEnemyTeam = PICK_INT(iTeam = 0, 1, 0)
	INT iThisTeamScore = MC_serverBD.iTeamScore[iTeam]
	INT iEnemyTeamScore = MC_serverBD.iTeamScore[iEnemyTeam]
	INT iRoundsRemaining = g_FMMC_STRUCT.iOvertimeRounds - (MC_serverBD_1.iCurrentRound)
	IF IS_BIT_SET(MC_serverBD.iServerBitSet5, SBBOOL5_OVERTIME_SUDDEN_DEATH_ROUNDS)
		IF g_FMMC_STRUCT.iOvertimeSDRounds > 0
			iRoundsRemaining += g_FMMC_STRUCT.iOvertimeSDRounds 
		ENDIF
	ENDIF
	IF iEnemyTeam = 0 //This can only affect the first team
		IF MC_serverBD_1.iTeamToMove = 1
			PRINTLN("[JS] CAN_OVERTIME_TEAM_SCORE_BE_MATCHED - iRoundsRemaining -1")
			iRoundsRemaining = CLAMP_INT(iRoundsRemaining - 1,0, iRoundsRemaining)
		ENDIF
	ENDIF
	PRINTLN("[JS] CAN_OVERTIME_TEAM_SCORE_BE_MATCHED - iRoundsRemaining = ", iRoundsRemaining)
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	INT iMaxScore = ci_OVERTIME_ZONE_POINTS_MAX - 1
	IF iRule < FMMC_MAX_RULES
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEight[iRule], ciBS_RULE8_OVERTIME_TICK_HUD)
			iMaxScore = 1
		ENDIF
	ENDIF
	IF iEnemyTeamScore + (iRoundsRemaining * iMaxScore) >= iThisTeamScore
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL CAN_OVERTIME_RUMBLE_TEAM_WIN(INT iTeam)
	INT iEnemyTeam = PICK_INT(iTeam = 0, 1, 0)
	INT iRoundsRemaining = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMissionLapLimit - (MC_ServerBD_4.iTeamLapsCompleted[iTeam] + 1)
	INT iMaxPossibleScore = iRoundsRemaining * (ci_OVERTIME_ZONE_POINTS_MAX - 1) * MC_ServerBD.iNumberOfPlayingPlayers[iTeam]
	PRINTLN("[JS] CAN_OVERTIME_RUMBLE_TEAM_WIN - iTeam = ",iTeam," iMaxPossibleScore = ", iMaxPossibleScore," MC_serverBD.iTeamScore[iTeam] = ", MC_serverBD.iTeamScore[iTeam], " MC_serverBD.iTeamScore[iEnemyTeam] = ", MC_serverBD.iTeamScore[iEnemyTeam])
	IF MC_serverBD.iTeamScore[iEnemyTeam] - MC_serverBD.iTeamScore[iTeam] > iMaxPossibleScore
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_TEAM_BEATING_ALL_OTHERS_BY_X(INT iTeam, INT iX)
	INT i
	FOR i = 0 TO MC_serverBD.iNumberOfTeams - 1
		IF i != iTeam
			IF (MC_serverBD.iTeamScore[i] + iX) >= MC_serverBD.iTeamScore[iTeam]
				PRINTLN("[JS] [TURNS] - IS_TEAM_BEATING_ALL_OTHERS_BY_X - team ", iTeam," is not ", iX," higher than team ", i)
				RETURN FALSE
			ENDIF
		ENDIF
	ENDFOR
	RETURN TRUE
ENDFUNC

FUNC INT GET_LARGEST_STARTING_TEAM()
	INT i
	INT iLargest
	FOR i = 0 TO MC_serverBD.iNumberOfTeams - 1
		IF MC_serverBD.iNumStartingPlayers[i] > iLargest
			iLargest = MC_serverBD.iNumStartingPlayers[i]
		ENDIF
	ENDFOR
	RETURN iLargest
ENDFUNC

FUNC BOOL HAS_ANY_TEAM_BEEN_GIVEN_ELIMINIATION_SCORE_THIS_RULE()
	INT i
	FOR i = 0 TO MC_serverBD.iNumberOfTeams - 1
		IF IS_BIT_SET(MC_serverBD.iServerBitSet5, SBBOOL5_ELIMINATION_SCORE_GIVEN_THIS_RULE_TEAM_0 + i)
			RETURN TRUE
		ENDIF
	ENDFOR
	RETURN FALSE
ENDFUNC

PROC ASSIGN_NUMBER_OF_VALID_FORCE_STATIC_CAMERA_LOCATIONS()
	INT i = 0
	FOR i = 0 TO ciFORCE_STATIC_CAMERA_MAX_LOCATIONS - 1
		IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.vBEVCamFSLPos[i])
			iValidStaticCams++
		ENDIF
	ENDFOR
	
	PRINTLN("[LM][ASSIGN_NUMBER_OF_VALID_FORCE_STATIC_CAMERA_LOCATIONS] - There are: ", iValidStaticCams, " Cam Positions for Top Down Cam.")
ENDPROC

FUNC BOOL ARE_TEAMS_UNBALANCED_FOR_OVERTIME(INT &iSmallerTeam, INT &iRemainder)
	IF MC_serverBD.iNumberOfPlayingPlayers[0] > MC_serverBD.iNumberOfPlayingPlayers[1]
		iRemainder = MC_serverBD.iNumberOfPlayingPlayers[0] - MC_serverBD.iNumberOfPlayingPlayers[1]
		iSmallerTeam = MC_serverBD.iNumberOfPlayingPlayers[1]
		RETURN TRUE
	ELIF MC_serverBD.iNumberOfPlayingPlayers[1] > MC_serverBD.iNumberOfPlayingPlayers[0]
		iRemainder = MC_serverBD.iNumberOfPlayingPlayers[1] - MC_serverBD.iNumberOfPlayingPlayers[0]
		iSmallerTeam = MC_serverBD.iNumberOfPlayingPlayers[0]
		RETURN TRUE
	ENDIF
	iRemainder = 0
	iSmallerTeam = -1
	RETURN FALSE
ENDFUNC

// THIS DECIDES WHOSE TURN IT IS NEXT IN SUDDEN DEATH.
FUNC INT GET_PARTICIPANT_NUMBER_FROM_TEAM_PARTICIPANT_NUMBER_ORDERED_BY_CASH(INT iTeam, INT iPartOnTeam = -1)
	INT iPart
	INT iPartOnTeamLoop = -1
	BOOL bShowPrint = FALSE
	INT iChosenPart
	
	INT iPlayerCash
	INT iPlayerCashes[NUM_NETWORK_PLAYERS]
	INT iPlayerParts[NUM_NETWORK_PLAYERS]
	
	#IF IS_DEBUG_BUILD
		bShowPrint = bParticipantLoopPrints
	#ENDIF
	
	FOR iPart = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1	
		iPlayerParts[iPart] = -1
		
		IF (NOT(IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_ANY_SPECTATOR)) OR (IS_BIT_SET(MC_playerBD[iPart].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN) OR IS_BIT_SET(MC_playerBD[iPart].iClientBitSet,PBBOOL_HEIST_SPECTATOR)))
		AND NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet,PBBOOL_PLAYER_FAIL)
		AND ((IS_BIT_SET(MC_playerBD[iPart].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING) AND IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_FIRST_UPDATE_DONE))
		OR NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_FIRST_UPDATE_DONE)) //Only care about this if they join midgame
		AND NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_FINISHED)		
			PARTICIPANT_INDEX tempPart = INT_TO_PARTICIPANTINDEX(iPart)
			IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
				PLAYER_INDEX tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
				IF MC_playerBD[iPart].iTeam = iTeam
				AND NOT IS_PLAYER_SCTV(tempPlayer)
				AND NOT DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(tempPlayer)
				AND GlobalplayerBD_FM[NATIVE_TO_INT(tempPlayer)].sClientCoronaData.iTeamChosen > -1
				AND GlobalplayerBD_FM[NATIVE_TO_INT(tempPlayer)].sClientCoronaData.iTeamChosen < FMMC_MAX_TEAMS
					// Found a participant. Increment our number.
					
					iPlayerCash = GET_PLAYER_CASH(tempPlayer)
					
					IF bShowPrint
						PRINTLN("[RCC MISSION][GET_PARTICIPANT_NUMBER_FROM_TEAM_PARTICIPANT_NUMBER_MC_SD] Looking at iPlayerCash: ", iPlayerCash, " iPart: ", iPart)
					ENDIF
					
					iPartOnTeamLoop++
					
					IF bShowPrint
						PRINTLN("[RCC MISSION][GET_PARTICIPANT_NUMBER_FROM_TEAM_PARTICIPANT_NUMBER_MC_SD] Adding iPlayerCash: ", iPlayerCash, " to index: ", iPartOnTeamLoop)
					ENDIF
					iPlayerCashes[iPartOnTeamLoop] = iPlayerCash
					iPlayerParts[iPartOnTeamLoop] = iPart
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	// Even - Descending
	IF MC_ServerBD.iSessionScriptEventKey % 2 != 0	
		INT i, j, iCash, iPartC
		FOR i = 0 TO iPartOnTeamLoop
			FOR j = 0 TO iPartOnTeamLoop
				IF iPlayerCashes[i] <= iPlayerCashes[j]
					iCash = iPlayerCashes[j]
					iPlayerCashes[j] = iPlayerCashes[i]
					iPlayerCashes[i] = iCash
					
					iPartC = iPlayerParts[j]
					iPlayerParts[j] = iPlayerParts[i]
					iPlayerParts[i] = iPartC
				ENDIF
			ENDFOR
		ENDFOR

	// Odd - Ascending
	ELSE
		INT i, j, iCash, iPartC
		FOR i = 0 TO iPartOnTeamLoop
			FOR j = 0 TO iPartOnTeamLoop
				IF iPlayerCashes[i] >= iPlayerCashes[j]
					iCash = iPlayerCashes[j]
					iPlayerCashes[j] = iPlayerCashes[i]
					iPlayerCashes[i] = iCash
					
					iPartC = iPlayerParts[j]
					iPlayerParts[j] = iPlayerParts[i]
					iPlayerParts[i] = iPartC
				ENDIF
			ENDFOR
		ENDFOR
	ENDIF
	
	// For Debug...
	IF bShowPrint
		INT i
		FOR i = 0 TO iPartOnTeamLoop
			PRINTLN("[RCC MISSION][GET_PARTICIPANT_NUMBER_FROM_TEAM_PARTICIPANT_NUMBER_MC_SD] Looping at iPart: ", iPlayerParts[i], " iPlayerCash: ", iPlayerCashes[i])
		ENDFOR
	ENDIF
	
	iChosenPart = iPlayerParts[iPartOnTeam]
	
	IF bShowPrint
		PRINTLN("[RCC MISSION][GET_PARTICIPANT_NUMBER_FROM_TEAM_PARTICIPANT_NUMBER_MC_SD] Returning iChosenPart: ", iChosenPart)
	ENDIF
	
	RETURN iChosenPart
ENDFUNC

FUNC INT GET_TEAM_PARTICIPANT_NUMBER_FROM_PARTICIPANT_NUMBER_MC(INT iTeam, INT iPartIn)
	INT iPart
	INT iPartOnTeamLoop = -1
	BOOL bShowPrint = FALSE
	
	#IF IS_DEBUG_BUILD
		bShowPrint = bParticipantLoopPrints
	#ENDIF
	bShowPrint = true

	FOR iPart = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		
		IF bShowPrint
			PRINTLN("[RCC MISSION][GET_TEAM_PARTICIPANT_NUMBER_FROM_PARTICIPANT_NUMBER_MC] iTeam = ", iTeam, " iPart = ", iPart, " iPartIn = ", iPartIn, " iPartOnTeamLoop = ", iPartOnTeamLoop)
		ENDIF
		
		IF (NOT(IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_ANY_SPECTATOR)) OR (IS_BIT_SET(MC_playerBD[iPart].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN) OR IS_BIT_SET(MC_playerBD[iPart].iClientBitSet,PBBOOL_HEIST_SPECTATOR)))
		AND NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet,PBBOOL_PLAYER_FAIL)
		AND ((IS_BIT_SET(MC_playerBD[iPart].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING) AND IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_FIRST_UPDATE_DONE))
		OR NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_FIRST_UPDATE_DONE)) //Only care about this if they join midgame
		AND NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_FINISHED)
			PARTICIPANT_INDEX tempPart = INT_TO_PARTICIPANTINDEX(iPart)
			IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
				PLAYER_INDEX tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
				IF MC_playerBD[iPart].iTeam = iTeam
				AND NOT IS_PLAYER_SCTV(tempPlayer)
				AND NOT DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(tempPlayer)
				AND GlobalplayerBD_FM[NATIVE_TO_INT(tempPlayer)].sClientCoronaData.iTeamChosen > -1
				AND GlobalplayerBD_FM[NATIVE_TO_INT(tempPlayer)].sClientCoronaData.iTeamChosen < FMMC_MAX_TEAMS
				AND iPart <= iPartIn
					// Found a participant. Increment our number.
					iPartOnTeamLoop++
					
					IF bShowPrint
						PRINTLN("[RCC MISSION][GET_TEAM_PARTICIPANT_NUMBER_FROM_PARTICIPANT_NUMBER_MC] iPartOnTeamLoop: ", iPartOnTeamLoop)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF bShowPrint
				PRINTLN("[RCC MISSION][GET_TEAM_PARTICIPANT_NUMBER_FROM_PARTICIPANT_NUMBER_MC] player not ok = ", iPart)
			ENDIF
		ENDIF
	ENDFOR
	
	PRINTLN("[RCC MISSION][GET_TEAM_PARTICIPANT_NUMBER_FROM_PARTICIPANT_NUMBER_MC] RETURNING: ", iPartOnTeamLoop)
	RETURN iPartOnTeamLoop
ENDFUNC

FUNC INT GET_PARTICIPANT_NUMBER_FROM_TEAM_PARTICIPANT_NUMBER_MC(INT iTeam, INT iPartOnTeam)
	INT iPart
	INT iPartOnTeamLoop = -1
	BOOL bShowPrint = FALSE
	
	#IF IS_DEBUG_BUILD
		bShowPrint = bParticipantLoopPrints
	#ENDIF
	
	INT iPlayersToCheck = MC_serverBD.iNumberOfPlayingPlayers[iTeam]
	IF iPartOnTeam > iPlayersToCheck
		RETURN -1
	ENDIF
	
	FOR iPart = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		
		IF bShowPrint
			PRINTLN("[RCC MISSION][GET_PARTICIPANT_NUMBER_FROM_TEAM_PARTICIPANT_NUMBER] iTeam = ", iTeam, " iPart = ", iPart, " iPartOnTeam = ", iPartOnTeam, " iPartOnTeamLoop = ", iPartOnTeamLoop)
		ENDIF
		
		IF (NOT(IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_ANY_SPECTATOR)) OR (IS_BIT_SET(MC_playerBD[iPart].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN) OR IS_BIT_SET(MC_playerBD[iPart].iClientBitSet,PBBOOL_HEIST_SPECTATOR)))
		AND NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet,PBBOOL_PLAYER_FAIL)
		AND ((IS_BIT_SET(MC_playerBD[iPart].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING) AND IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_FIRST_UPDATE_DONE))
		OR NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_FIRST_UPDATE_DONE)) //Only care about this if they join midgame
		AND NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_FINISHED)		
			PARTICIPANT_INDEX tempPart = INT_TO_PARTICIPANTINDEX(iPart)
			IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
				PLAYER_INDEX tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
				IF MC_playerBD[iPart].iTeam = iTeam
				AND NOT IS_PLAYER_SCTV(tempPlayer)
				AND NOT DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(tempPlayer)
				AND GlobalplayerBD_FM[NATIVE_TO_INT(tempPlayer)].sClientCoronaData.iTeamChosen > -1
				AND GlobalplayerBD_FM[NATIVE_TO_INT(tempPlayer)].sClientCoronaData.iTeamChosen < FMMC_MAX_TEAMS
					// Found a participant. Increment our number.
					iPartOnTeamLoop++
					
					IF bShowPrint
						PRINTLN("[RCC MISSION][GET_PARTICIPANT_NUMBER_FROM_TEAM_PARTICIPANT_NUMBER] iPartOnTeamLoop: ", iPartOnTeamLoop)
					ENDIF

					IF iPartOnTeam = iPartOnTeamLoop
						IF bShowPrint
							PRINTLN("[RCC MISSION][GET_PARTICIPANT_NUMBER_FROM_TEAM_PARTICIPANT_NUMBER] Returning: ", iPart)
						ENDIF
						RETURN iPart
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF bShowPrint
				PRINTLN("[RCC MISSION][GET_PARTICIPANT_NUMBER_FROM_TEAM_PARTICIPANT_NUMBER] player not ok = ", iPart)
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN -1
ENDFUNC

FUNC INT OVERTIME_GET_PARTICIPANT_NUMBER_FROM_TEAM_PARTICIPANT_NUMBER(INT iTeam, INT iTeamPart, INT iRemainder = -1)
	IF iRemainder = -1
		iRemainder = MC_serverBD.iNumberOfPlayingPlayers[iTeam]
	ENDIF
	
	IF iTeamPart >= MC_serverBD.iNumberOfPlayingPlayers[iTeam]
	AND iRemainder > 0
		INT iTeamPartTemp = iTeamPart - iRemainder
		RETURN OVERTIME_GET_PARTICIPANT_NUMBER_FROM_TEAM_PARTICIPANT_NUMBER(iTeam, iTeamPartTemp, iRemainder)
	ELSE
		IF  IS_BIT_SET(MC_serverBD.iServerBitSet5, SBBOOL5_OVERTIME_SUDDEN_DEATH_ROUNDS)
			RETURN GET_PARTICIPANT_NUMBER_FROM_TEAM_PARTICIPANT_NUMBER_ORDERED_BY_CASH(iTeam, iTeamPart)
		ELSE
			RETURN GET_PARTICIPANT_NUMBER_FROM_TEAM_PARTICIPANT_NUMBER_MC(iTeam, iTeamPart)
		ENDIF
	ENDIF
ENDFUNC

// to catch leavers in sudden death modes (specifically for Turf Wars/Land Grab) the mode was getting stuck, never passing end conditions.
FUNC BOOL IS_ONLY_ONE_TEAM_LEFT_ACTIVE()
	
	INT iTeamBS, i, iTeamsStillPlaying
	
	FOR i = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
			PLAYER_INDEX tempPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
			IF IS_NET_PLAYER_OK(tempPlayer, FALSE)
			AND NOT IS_PARTICIPANT_A_SPECTATOR(i)
				IF NOT IS_BIT_SET(iTeamBS, MC_playerBD[i].iteam)
					PRINTLN("[LM] IS_ONLY_ONE_TEAM_LEFT_ACTIVE -  team ", MC_playerBD[i].iteam, " is still going")
					SET_BIT(iTeamBS, MC_playerBD[i].iteam)
					iTeamsStillPlaying++
					IF iTeamsStillPlaying > 1
						PRINTLN("[LM] IS_ONLY_ONE_TEAM_LEFT_ACTIVE - Nope")
						BREAKLOOP
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	IF iTeamsStillPlaying <= 1
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
	
ENDFUNC

FUNC BOOL ARE_MULTIPLE_TEAMS_ON_SAME_SCORE()
	
	INT iTeam
	BOOL bMultipleTeamsOnScore = FALSE
	INT iTeamsOnSameScore = 0
	INT iHighestScore = 0

	FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
		IF IS_TEAM_ACTIVE(iTeam)
			IF MC_serverBD.iTeamScore[iTeam] > iHighestScore
				iHighestScore = MC_serverBD.iTeamScore[iTeam]
			ENDIF
		ENDIF
	ENDFOR
	
	PRINTLN("[JS] ARE_MULTIPLE_TEAMS_ON_SAME_SCORE - HIGHEST SCORE: ", iHighestScore)

	FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
		IF IS_TEAM_ACTIVE(iTeam)
			IF MC_serverBD.iTeamScore[iTeam] = iHighestScore
				PRINTLN("[JS] ARE_MULTIPLE_TEAMS_ON_SAME_SCORE - TEAM: ", iTeam," has the highest score")
				iTeamsOnSameScore++
			ENDIF
		ENDIF
	ENDFOR
	
	IF iTeamsOnSameScore >= 2
		bMultipleTeamsOnScore = TRUE
	ENDIF
	
	RETURN bMultipleTeamsOnScore
	
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_LAST_ALIVE_ON_TEAM(INT iTeam)
	INT i, iPlayersToCheck, iPlayersChecked
	iPlayersToCheck = MC_serverBD.iNumberOfPlayingPlayers[iTeam]
	FOR i = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		IF (NOT(IS_BIT_SET(MC_playerBD[i].iClientBitSet, PBBOOL_ANY_SPECTATOR)) OR IS_BIT_SET(MC_playerBD[i].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN))
		AND NOT IS_BIT_SET(MC_playerBD[i].iClientBitSet,PBBOOL_PLAYER_FAIL)
		AND IS_BIT_SET(MC_playerBD[i].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
		AND NOT IS_BIT_SET(MC_playerBD[i].iClientBitSet, PBBOOL_FINISHED)
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
			AND MC_playerBD[i].iTeam = iTeam
				iPlayersChecked++
				IF i != iLocalPart
					PLAYER_INDEX tempPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
					IF IS_NET_PLAYER_OK(tempPlayer, TRUE)
					AND NOT IS_BIT_SET(MC_playerBD[i].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN)
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		IF iPlayersChecked >= iPlayersToCheck
			BREAKLOOP
		ENDIF
	ENDFOR
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_LAST_ALIVE()
	
	IF IS_BIT_SET(iLocalboolCheck24, LBOOL24_LAST_PLAYER_ALIVE)
		RETURN TRUE
	ENDIF
	
	INT iTeam	
	FOR iTeam = 0 TO FMMC_MAX_TEAMS-1
		IF NOT IS_LOCAL_PLAYER_LAST_ALIVE_ON_TEAM(iTeam)
			RETURN FALSE
		ENDIF
	ENDFOR
	
	IF NOT IS_BIT_SET(iLocalboolCheck24, LBOOL24_LAST_PLAYER_ALIVE)
		PRINTLN("[LM][IS_LOCAL_PLAYER_LAST_ALIVE] - Setting bit LBOOL24_LAST_PLAYER_ALIVE")
		SET_BIT(iLocalBoolCheck24, LBOOL24_LAST_PLAYER_ALIVE)
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC RENDER_SPAWN_PROTECTION_SPHERES_EARLY()

	IF MC_playerBD[iLocalPart].iGameState = GAME_STATE_MISSION_OVER
	OR MC_playerBD[iLocalPart].iGameState = GAME_STATE_LEAVE
	OR MC_playerBD[iLocalPart].iGameState = GAME_STATE_END
	OR GET_SKYSWOOP_STAGE() != SKYSWOOP_NONE
	OR IS_BIT_SET(MC_PlayerBD[iLocalPart].iClientBitset, PBBOOL_ON_LEADERBOARD)
	OR IS_BIT_SET(MC_PlayerBD[iLocalPart].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
	OR IS_PLAYER_IN_CORONA()
		EXIT
	ENDIF
	
	INT iTeam = MC_PlayerBD[iLocalPart].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	IF iRule < FMMC_MAX_RULES
		INT iTeamZ
		INT iEveryFrameZoneLoop
		FOR iEveryFrameZoneLoop = 0 TO FMMC_MAX_NUM_ZONES - 1
			IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].iType = ciFMMC_ZONE_TYPE__SPAWN_PROTECTION		
			AND g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].iZoneDelay <= 0
			
				iTeamZ = ROUND(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].fZoneValue)
				
				PRINTLN("[RCC MISSION][POP] RENDER_SPAWN_PROTECTION_SPHERES_EARLY - ciFMMC_ZONE_TYPE__SPAWN_PROTECTION: ",iEveryFrameZoneLoop)
				PRINTLN("[RCC MISSION][POP] RENDER_SPAWN_PROTECTION_SPHERES_EARLY - Checking iTeam: ", iTeamZ)
				
				IF iTeamZ > -1
				AND iTeamZ < FMMC_MAX_TEAMS				
					// If always on or on for the first rule
					IF (g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].iZoneStartPriority[iTeamZ] = -1 OR g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].iZoneStartPriority[iTeamZ] = 0)
						iTeamZ = ROUND(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].fZoneValue)
						
						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].iZoneBS, ciFMMC_ZONEBS_ONLY_SHOW_WITH_PICKUP)
							IF MC_playerBD[iPartToUse].iObjCarryCount = 0
								PRINTLN("[RCC MISSION][POP] RENDER_SPAWN_PROTECTION_SPHERES_EARLY - Exiting because of no pickups")
								EXIT
							ENDIF
						ENDIF
						
						IF iTeamZ > -1
						AND iTeamZ < FMMC_MAX_TEAMS
							IF MC_serverBD.iNumStartingPlayers[iTeamZ] > 0
								IF IS_LOCATION_IN_FMMC_ZONE(GET_ENTITY_COORDS(LocalPlayerPed), iEveryFrameZoneLoop, FALSE, FALSE, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].iZoneBS, ciFMMC_ZONEBS_IncludeZAxisInAreaCheck), fZoneGrowth[iEveryFrameZoneLoop])
								OR IS_BIT_SET(iZoneBS_ActivatedRemotely, iEveryFrameZoneLoop)
									BOOL bBlockActions = TRUE
									IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_STOCKPILE(g_FMMC_STRUCT.iAdversaryModeType)
										bBlockActions = FALSE
									ENDIF
									IF bBlockActions
										DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)	
										DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_UD)
										DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_LR)
										DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_NEXT)
										DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_PREV)
										DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_NEXT_WEAPON)
										DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_PREV_WEAPON)
										
										DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK, TRUE)
										DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM, TRUE)
										
										DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MELEE_LEFT)
										
										IF NOT IS_PED_INJURED(localPlayerPed)
											SET_PED_RESET_FLAG(LocalPlayerPed, PRF_BlockWeaponFire, TRUE)
										ENDIF
										
										SET_PED_CONFIG_FLAG(localPlayerPed, PCF_DontActivateRagdollFromExplosions, TRUE)
										SET_PED_CONFIG_FLAG(localPlayerPed, PCF_DontActivateRagdollFromFire, TRUE)
										SET_PED_CONFIG_FLAG(localPlayerPed, PCF_DontActivateRagdollFromElectrocution, TRUE)
										
										WEAPON_TYPE wt = WEAPONTYPE_INVALID
										GET_CURRENT_PED_WEAPON(LocalPlayerPed, wt)
									
										IF wt != WEAPONTYPE_UNARMED
											DISABLE_PLAYER_THROW_GRENADE_WHILE_USING_GUN()
											DISABLE_PLAYER_FIRING(localPlayer, TRUE)										
										ENDIF
										
										IF wt = WEAPONTYPE_GRENADE
											SET_CURRENT_PED_WEAPON(LocalPlayerPed, WEAPONTYPE_UNARMED, TRUE)
										ENDIF
										
										DISABLE_VEHICLE_MINES(TRUE)
									ENDIF		
								ENDIF		
								
								PRINTLN("[RCC MISSION][POP] RENDER_SPAWN_PROTECTION_SPHERES_EARLY - There is a playing on this team.")
								INT iR = 0, iG = 0, iB = 0, iA = 0
								HUD_COLOURS tempColour
								tempColour = GET_HUD_COLOUR_FOR_FMMC_TEAM(ROUND(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].fZoneValue), LocalPlayer)
								IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].iZoneBS, ciFMMC_ZONEBS_APPLY_TO_ALL_TEAMS)
									tempColour = HUD_COLOUR_RED
								ENDIF
								GET_RGB_FROM_INT(GET_INT_FROM_HUD_COLOUR(tempColour), iR, iG, iB, iA)
								iA = 100
								IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].iAreaType = ciFMMC_ZONE_SHAPE__SPHERE
									DRAW_MARKER(MARKER_SPHERE, g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].vPos[0], <<0,0,0>>, <<0,0,0>>, <<g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].fRadius,g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].fRadius,g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].fRadius>>, iR, iG, iB, iA)									
									PRINTLN("[RCC MISSION][POP] RENDER_SPAWN_PROTECTION_SPHERES_EARLY - Drawing Sphere")
								ELSE
									VECTOR vMidPoint = (g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].vPos[0] + g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].vPos[1])
									vMidPoint = <<vMidPoint.x / 2, vMidPoint.y / 2, vMidPoint.z / 2>>
									FLOAT fScale = VDIST2(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].vPos[0], g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].vPos[1])
									fScale = PICK_FLOAT(fScale > POW(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].fRadius, 2), SQRT(fScale), g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].fRadius)
									
									DRAW_ANGLED_AREA_FROM_FACES(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].vPos[0], g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].vPos[1], g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].fRadius, iR, iG, iB, iA)
									PRINTLN("[RCC MISSION][POP] RENDER_SPAWN_PROTECTION_SPHERES_EARLY - Drawing Angled Area")
								ENDIF
							ELSE
								PRINTLN("[RCC MISSION][POP] RENDER_SPAWN_PROTECTION_SPHERES_EARLY - MC_serverBD.iNumStartingPlayers[iTeamZ]: ", MC_serverBD.iNumStartingPlayers[iTeamZ])
							ENDIF
						ELSE
							PRINTLN("[RCC MISSION][POP] RENDER_SPAWN_PROTECTION_SPHERES_EARLY - There is a playing on this team.")
						ENDIF
					ENDIF
				ELSE
					PRINTLN("[RCC MISSION][POP] RENDER_SPAWN_PROTECTION_SPHERES_EARLY- iTeam: ", iTeamZ, " is not active, therefore we will not create the Zone.")
				ENDIF
			ELIF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].iType = ciFMMC_ZONE_TYPE__BLOCK_RUNNING
			AND iRule >= g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].iZoneStartPriority[iTeam]
			AND (iRule < g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].iZoneEndPriority[iTeam] OR g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].iZoneStartPriority[iTeam] = -1)
				PRINTLN("[RCC MISSION] PROCESS_RESTRICTION_ZONES Hitting the blocked running zone")
				IF (iSpectatorTarget = -1)
				AND IS_LOCATION_IN_FMMC_ZONE(GET_ENTITY_COORDS(LocalPlayerPed), iEveryFrameZoneLoop, FALSE, FALSE, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].iZoneBS, ciFMMC_ZONEBS_IncludeZAxisInAreaCheck), fZoneGrowth[iEveryFrameZoneLoop])
				OR IS_BIT_SET(iZoneBS_ActivatedRemotely, iEveryFrameZoneLoop)
					PRINTLN("[RCC MISSION] RENDER_SPAWN_PROTECTION_SPHERES_EARLY Player in Blocked Running zone - Blocking run action")
					bInBlockedRunningZoneStart = TRUE
					fBlockedRunningZoneSpeed = g_FMMC_STRUCT_ENTITIES.sPlacedZones[iEveryFrameZoneLoop].fZoneValue
					IF NOT IS_BIT_SET(iBlockRunningZoneBitset, iEveryFrameZoneLoop)
						SET_BIT(iBlockRunningZoneBitset, iEveryFrameZoneLoop)
					ENDIF
				ELSE
					IF IS_BIT_SET(iBlockRunningZoneBitset, iEveryFrameZoneLoop)
						PRINTLN("[RCC MISSION] RENDER_SPAWN_PROTECTION_SPHERES_EARLY Player has left Blocked Running zone - Re-allowing run action")
						bInBlockedRunningZoneStart = FALSE
						fBlockedRunningZoneSpeed = 0.0
						CLEAR_BIT(iBlockRunningZoneBitset, iEveryFrameZoneLoop)
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
			
		IF bInBlockedRunningZoneStart
			PRINTLN("[RCC MISSION] RENDER_SPAWN_PROTECTION_SPHERES_EARLY - DISABLING RUN fBlockedRunningZoneSpeed: ", fBlockedRunningZoneSpeed)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPRINT)
			IF fBlockedRunningZoneSpeed < 0.1
				SET_PED_MAX_MOVE_BLEND_RATIO(LocalPlayerPed, PEDMOVEBLENDRATIO_WALK)
			ELSE
				SET_PED_MAX_MOVE_BLEND_RATIO(LocalPlayerPed, fBlockedRunningZoneSpeed)			
				SET_PED_MOVE_RATE_OVERRIDE(LocalPlayerPed, fBlockedRunningZoneSpeed)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC CLEAN_UP_VEHICLE_SWAP()
	CLEAR_BIT(iLocalBoolCheck22, LBOOL22_SWAPPED_TO_POWERUP_VEHICLE)
	RESET_NET_TIMER(tdSwapTimer)
	vehicleSwapState = eVehicleSwapRuleTransformState_INITIALISE
	
	IF IS_BIT_SET(MC_playerBD[iPartToUse].iVehicleWeaponEffectActiveBS, ciVEH_WEP_RUINER_SPECIAL_VEH)
		CLEAR_BIT(MC_playerBD[iPartToUse].iVehicleWeaponEffectActiveBS, ciVEH_WEP_RUINER_SPECIAL_VEH)
		CLEAR_BIT(MC_playerBD[iPartToUse].iVehicleWeaponCollectedBS, ciVEH_WEP_RUINER_SPECIAL_VEH)			
		PRINTLN("[LM][MISSION][PROCESS_SPECIFIC_VEHICLE_SWAP_MANAGEMENT] - Clearing player ciVEH_WEP_RUINER_SPECIAL_VEH power up")	
	ENDIF
	
	IF IS_BIT_SET(MC_playerBD[iPartToUse].iVehicleWeaponEffectActiveBS, ciVEH_WEP_RAMP_SPECIAL_VEH)
		CLEAR_BIT(MC_playerBD[iPartToUse].iVehicleWeaponEffectActiveBS, ciVEH_WEP_RAMP_SPECIAL_VEH)
		CLEAR_BIT(MC_playerBD[iPartToUse].iVehicleWeaponCollectedBS, ciVEH_WEP_RAMP_SPECIAL_VEH)						
		PRINTLN("[LM][MISSION][PROCESS_SPECIFIC_VEHICLE_SWAP_MANAGEMENT] - Clearing player ciVEH_WEP_RAMP_SPECIAL_VEH power up")
	ENDIF
	
	mnOldVehicle = DUMMY_MODEL_FOR_SCRIPT
ENDPROC

FUNC BOOL INITIALIZE_DUMMY_COMPASS_OBJECT()
	//Create a small extra prop which can be used to calculate the direction of bones
	IF NOT DOES_ENTITY_EXIST(objTopDownCamCompass)
		REQUEST_MODEL(PROP_LD_TEST_01)
		IF HAS_MODEL_LOADED(PROP_LD_TEST_01)
			objTopDownCamCompass = CREATE_OBJECT(PROP_LD_TEST_01, GET_ENTITY_COORDS(localPlayerPed), FALSE)
			SET_ENTITY_VISIBLE(objTopDownCamCompass, FALSE)
			SET_ENTITY_COLLISION(objTopDownCamCompass, FALSE)
		ELSE
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC SET_SWAN_DIVE_CAM_STATE(SwanDiveScoreCameraState eState)
	PRINTLN("[SET_SWAN_DIVE_CAM_STATE] currentSwanDiveCamState was = ", ENUM_TO_INT(currentSwanDiveCamState))
	PRINTLN("[SET_SWAN_DIVE_CAM_STATE] eState changes it to = ", ENUM_TO_INT(eState))
	DEBUG_PRINTCALLSTACK()
	
	currentSwanDiveCamState = eState
ENDPROC

PROC RESET_SWAN_DIVE_SCORE_CAMERA(BOOL bTinyRacers, BOOL bPauseOnPlayer, PED_INDEX pedForCamera)
	
	PRINTLN("PROCESS_SWAN_DIVE_SCORE_CAMERA - RESET_SWAN_DIVE_SCORE_CAMERA")	
	
	IF DOES_CAM_EXIST(swanDiveCamera)
		DESTROY_CAM(swanDiveCamera)
		IF (eiSwanCamRayEntity != NULL)
			RELEASE_SCRIPT_GUID_FROM_ENTITY(eiSwanCamRayEntity)
		ENDIF
		
		IF NOT g_bCelebrationScreenIsActive
			RENDER_SCRIPT_CAMS(FALSE,FALSE)	
		ENDIF
	ENDIF
	
	VEHICLE_INDEX vehIn
	IF IS_PED_IN_ANY_VEHICLE(localPlayerPed)
		PRINTLN("[LM] PROCESS_SWAN_DIVE_SCORE_CAMERA - SDSCS_Reset In a vehicle.")
		vehIn = GET_VEHICLE_PED_IS_IN(localPlayerPed)
		
		IF NETWORK_HAS_ENTITY_BEEN_REGISTERED_WITH_THIS_THREAD(vehIn)
		AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(VEH_TO_NET(vehIn))
		AND NETWORK_HAS_CONTROL_OF_ENTITY(vehIn)		
			PRINTLN("PROCESS_SWAN_DIVE_SCORE_CAMERA - SDSCS_Reset UNFREEZING VEH")
			FREEZE_ENTITY_POSITION(vehIn, FALSE)
		ELSE
			PRINTLN("[LM] PROCESS_SWAN_DIVE_SCORE_CAMERA - SDSCS_Reset NO CONTROL")
		ENDIF
	ENDIF
	
	RESET_NET_TIMER(tdEffectDelayBeforeCamTimer)
	
	iSwanCamRayHitSomething = 0
	vSwanCamRayPos = <<-1,-1,-1>>
	vSwanCamRayNormal = <<-1,-1,-1>>
	iSwanCamAngleTestSelection = 1
	siSwanDiveCamRay = NULL
	iCamRenderingChecks = 0
	vSwanCamCached = <<0,0,0>>
	
	IF NOT IS_BIT_SET(iLocalBoolCheck16, LBOOL16_DIVE_SCORE_RENDERPAUSE_ACTIVE)
	OR (bTinyRacers AND NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_TINY_RACERS_SCREEN_PAUSED) AND NOT IS_PLAYER_TELEPORT_ACTIVE())
	OR (bPauseOnPlayer AND GET_PLAYER_RESPAWN_BACK_AT_START_STATE(iLocalPart) = PRBSS_PlayingNormally)
		IF pedForCamera = LocalPlayerPed
		AND NOT IS_PARTICIPANT_A_SPECTATOR(iLocalPart)
		
			NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
		ENDIF
		
		IF bTinyRacers
			IF IS_PED_INJURED(pedForCamera)
				SET_BIT(iLocalBoolCheck22, LBOOL22_TINY_RACERS_RENDER_PAUSE_TARGET_DEAD)
				PRINTLN("[JT TR][PROCESS_SWAN_DIVE_SCORE_CAMERA] Setting LBOOL22_TINY_RACERS_RENDER_PAUSE_TARGET_DEAD")
			ENDIF
		ENDIF
		
		SET_SWAN_DIVE_CAM_STATE(SDSCS_WaitingToPlay)
	ENDIF
ENDPROC

PROC PROCESS_SWAN_DIVE_SCORE_CAMERA(BOOL bTinyRacers = FALSE, BOOL bPauseOnPlayer = FALSE)
	PED_INDEX pedForCamera = LocalPlayerPed	
	PLAYER_INDEX playerForCamera = LocalPlayer
	
	IF NOT bTinyRacers
		IF IS_LOCAL_PLAYER_ANY_SPECTATOR()
		AND NOT ((bTinyRacers OR bPauseOnPlayer) AND IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN))
			PRINTLN("PROCESS_SWAN_DIVE_SCORE_CAMERA - spectator")
			EXIT
		ENDIF
	ENDIF
	
	VECTOR vTempCoord = GET_ENTITY_COORDS(pedForCamera)
	IF bTinyRacers
		IF MC_serverBD.iTRLastParticipantToUse != -1
			PARTICIPANT_INDEX partForCamera = INT_TO_PARTICIPANTINDEX(MC_serverBD.iTRLastParticipantToUse)
			IF NETWORK_IS_PARTICIPANT_ACTIVE(partForCamera)
				pedForCamera = GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(partForCamera))
				vTempCoord = GET_ENTITY_COORDS(pedForCamera)
			ELSE
				PRINTLN("PROCESS_SWAN_DIVE_SCORE_CAMERA - bTinyRacers EXIT 1, partForCamera is not active (MC_serverBD.iTRLastParticipantToUse = ",MC_serverBD.iTRLastParticipantToUse,")")
				EXIT
			ENDIF
		ELSE
			PRINTLN("PROCESS_SWAN_DIVE_SCORE_CAMERA - bTinyRacers EXIT 2, last part to use = -1 (MC_serverBD.iTRLastParticipantToUse = ",MC_serverBD.iTRLastParticipantToUse,")")
			EXIT
		ENDIF
		IF vTempCoord.z < 0
			PRINTLN("PROCESS_SWAN_DIVE_SCORE_CAMERA - bTinyRacers EXIT 3, partForCamera has z < 0, vTempCoord = ",vTempCoord," (MC_serverBD.iTRLastParticipantToUse = ",MC_serverBD.iTRLastParticipantToUse,")")
			EXIT
		ENDIF
	ELIF bPauseOnPlayer
		IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN)
		OR IS_PED_INJURED(pedForCamera)
			IF MPGlobalsAmbience.piHeistSpectateTarget != INVALID_PLAYER_INDEX()
			AND IS_NET_PLAYER_OK(MPGlobalsAmbience.piHeistSpectateTarget)
				pedForCamera = GET_PLAYER_PED(MPGlobalsAmbience.piHeistSpectateTarget)
			ELSE
				INT iPart
				FOR iPart = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
					PARTICIPANT_INDEX tempPart = INT_TO_PARTICIPANTINDEX(iPart)
					IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
					AND NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_ANY_SPECTATOR)
					AND NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet,PBBOOL_PLAYER_FAIL)
					AND IS_BIT_SET(MC_playerBD[iPart].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
					AND NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_FINISHED)
						PLAYER_INDEX tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
						IF IS_NET_PLAYER_OK(tempPlayer)
						AND NOT IS_PARTICIPANT_A_SPECTATOR(iPart)	
							pedForCamera = GET_PLAYER_PED(tempPlayer)
							BREAKLOOP
						ENDIF
					ENDIF
				ENDFOR
			ENDIF
		ENDIF
		
		IF IS_PED_INJURED(pedForCamera)
			PRINTLN("PROCESS_SWAN_DIVE_SCORE_CAMERA - pedForCamera is dead, reset")
			IF currentSwanDiveCamState != SDSCS_WaitingToPlay
				SET_SWAN_DIVE_CAM_STATE(SDSCS_Reset)
			ENDIF
			CLEAR_BIT(iLocalBoolCheck22, LBOOL22_START_PAUSE_CAM_FOR_RESET)
		ENDIF
	ENDIF
	
	INT iTimeToCheck = ciDIVE_SCORE_STALL_CHECK * 2
	
	
	IF (currentSwanDiveCamState != SDSCS_WaitingToPlay
		AND GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stDiveCamStallTimer) > iTimeToCheck
		OR IS_PED_INJURED(pedForCamera)
		OR (bTinyRacers AND (vTempCoord.z < 0)))
		PRINTLN("PROCESS_SWAN_DIVE_SCORE_CAMERA - Manual reset: ", currentSwanDiveCamState, ", ", 
				GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stDiveCamStallTimer), ", ", IS_PED_INJURED(pedForCamera),
				" Z for ped: ", vTempCoord.z)
		SET_SWAN_DIVE_CAM_STATE(SDSCS_Reset)
	ENDIF
	
	IF currentSwanDiveCamState != SDSCS_WaitingToPlay
		IF NOT bTinyRacers
			DISABLE_PLAYER_CONTROLS_THIS_FRAME()
		ENDIF
		IF pedForCamera = LocalPlayerPed
			IF NOT IS_BIT_SET(iLocalBoolCheck16, LBOOL16_INVINCIBLE_DURING_POINT_CAM)
				//SET_ENTITY_PROOFS(LocalPlayerPed,TRUE,TRUE,TRUE,TRUE,TRUE,TRUE,TRUE,TRUE) 
				SET_ENTITY_CAN_BE_DAMAGED(LocalPlayerPed, FALSE)
				SET_BIT(iLocalBoolCheck16, LBOOL16_INVINCIBLE_DURING_POINT_CAM)
				PRINTLN("PROCESS_SWAN_DIVE_SCORE_CAMERA - set player SET_ENTITY_CAN_BE_DAMAGED FALSE")
			ENDIF
		ENDIF
	ELSE
		IF pedForCamera = LocalPlayerPed
			IF NOT g_bMissionEnding
				IF IS_BIT_SET(iLocalBoolCheck16, LBOOL16_INVINCIBLE_DURING_POINT_CAM)
					//SET_ENTITY_PROOFS(LocalPlayerPed,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE) 
					SET_ENTITY_CAN_BE_DAMAGED(LocalPlayerPed, TRUE)
					CLEAR_BIT(iLocalBoolCheck16, LBOOL16_INVINCIBLE_DURING_POINT_CAM)
					PRINTLN("PROCESS_SWAN_DIVE_SCORE_CAMERA - set player SET_ENTITY_CAN_BE_DAMAGED TRUE")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
		
	IF (IS_PED_INJURED(pedForCamera)
	OR IS_PLAYER_RESPAWNING(playerForCamera)
	OR (manualRespawnState != eManualRespawnState_OKAY_TO_SPAWN OR HAS_NET_TIMER_STARTED(timerManualRespawn)))
	AND NOT bTinyRacers
		
		IF currentSwanDiveCamState != SDSCS_WaitingToPlay
			SET_SWAN_DIVE_CAM_STATE(SDSCS_WaitingToPlay)
			RESET_SWAN_DIVE_SCORE_CAMERA(bTinyRacers, bPauseOnPlayer, pedForCamera)
		ENDIF
		
		IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_RESURRECTION(g_FMMC_STRUCT.iAdversaryModeType)
		AND GET_PLAYER_RESPAWN_BACK_AT_START_STATE(iLocalPart) = PRBSS_Init
			IF NOT HAS_NET_TIMER_STARTED(tdResurrectionStuckTimer)
				REINIT_NET_TIMER(tdResurrectionStuckTimer)
			ELIF HAS_NET_TIMER_EXPIRED(tdResurrectionStuckTimer, ciResurrectionStuckTimerLength)
			AND NOT IS_BIT_SET(iLocalBoolCheck21, LBOOL21_SPECTATOR_RESPAWN_FORCE_RESPAWN) 
				PRINTLN("PROCESS_SWAN_DIVE_SCORE_CAMERA - RESURRECTION STUCK TIMER EXPIRED")
				SET_BIT(iLocalBoolCheck21, LBOOL21_SPECTATOR_RESPAWN_FORCE_RESPAWN) 
			ENDIF
		ENDIF
		
		PRINTLN("PROCESS_SWAN_DIVE_SCORE_CAMERA - EXITTING - Player is Dead, Manually Respawning or Respawning.")
		EXIT
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(tdResurrectionStuckTimer)
		RESET_NET_TIMER(tdResurrectionStuckTimer)
	ENDIF
	
	SWITCH currentSwanDiveCamState
	
		CASE SDSCS_WaitingToPlay

			IF MC_playerBD[iPartToUse].iObjectiveTypeCompleted = JOB_COMP_DELIVER_OBJ
			OR (bTinyRacers
				AND MC_playerBD[iPartToUse].ePlayerTinyRacersRoundState = TR_LastManTimer 
				AND IS_BIT_SET(iLocalBoolCheck21, LBOOL21_TINY_RACERS_ROUND_SHARD_SHOWN)
				AND NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_TINY_RACERS_SCREEN_PAUSED))
			OR (bPauseOnPlayer AND IS_BIT_SET(iLocalBoolCheck22, LBOOL22_START_PAUSE_CAM_FOR_RESET))				
				BOOL bReadyToProgress
				bReadyToProgress = TRUE
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_RUNNING_BACK_REMIX(g_FMMC_STRUCT.iAdversaryModeType)
					IF NOT ANIMPOSTFX_IS_RUNNING("InchOrange")
						PRINTLN("[LM][PROCESS_SWAN_DIVE_SCORE_CAMERA] - Playing Orange FX")
						ANIMPOSTFX_PLAY("InchOrange", 0, TRUE)
					ENDIF
					
					IF NOT HAS_NET_TIMER_STARTED(tdEffectDelayBeforeCamTimer)
						START_NET_TIMER(tdEffectDelayBeforeCamTimer)
						bReadyToProgress = FALSE
					ELSE
						IF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdEffectDelayBeforeCamTimer, 850)
							RESET_NET_TIMER(tdEffectDelayBeforeCamTimer)
						ELSE
							bReadyToProgress = FALSE
						ENDIF
					ENDIF
				ENDIF				
				
				IF bReadyToProgress
					RESET_NET_TIMER(tdEffectDelayBeforeCamTimer)
					IF NOT bTinyRacers
					AND NOT bPauseOnPlayer
						BROADCAST_FMMC_PLAYER_IS_SWAN_DIVE_SCORING()
					ENDIF	
					IF bPauseOnPlayer
						iSwanCamAngleTestSelection = 1
						SET_SWAN_DIVE_CAM_STATE(SDSCS_FindRemoteCamSlot)
						CLEAR_BIT(iLocalBoolCheck22, LBOOL22_START_PAUSE_CAM_FOR_RESET)
						
						IF g_FMMC_STRUCT.iOverridePauseCamDistance > -1	
							iSwanCamAngleTestSelection = GET_RANDOM_INT_IN_RANGE(1,3)
						ENDIF
					ELSE
						iSwanCamAngleTestSelection = GET_RANDOM_INT_IN_RANGE(1,4)
						SET_SWAN_DIVE_CAM_STATE(SDSCS_FindCameraSlot)
					ENDIF
					REINIT_NET_TIMER(stDiveCamStallTimer)
				ENDIF
			ENDIF
		BREAK
		
		CASE SDSCS_FindCameraSlot

			
			IF (siSwanDiveCamRay = NULL)				
				//6 camera angles - 
				//1, 2, 3 	are prefered. 
				//4, 5 		are backups.
				//6 		2nd backup
				//Default	Don't do a special camera
				FLOAT fRange
				IF bTinyRacers
					fRange = 6
				ELSE
					fRange = 2
				ENDIF
				
				IF g_FMMC_STRUCT.iOverridePauseCamDistance > -1
					fRange = TO_FLOAT(g_FMMC_STRUCT.iOverridePauseCamDistance)
				ENDIF
				
				PRINTLN("PROCESS_SWAN_DIVE_SCORE_CAMERA - fRange: ", fRange)
				PRINTLN("PROCESS_SWAN_DIVE_SCORE_CAMERA - iSwanCamAngleTestSelection: ", iSwanCamAngleTestSelection)
				
				SWITCH iSwanCamAngleTestSelection
				CASE 1 		vSwanCamOffset = << -2, 8, 0.1 		+ GET_RANDOM_FLOAT_IN_RANGE(0, fRange) 	>>		BREAK //Front Left
				CASE 2 		vSwanCamOffset = << 0, 8, 0.1 		+ GET_RANDOM_FLOAT_IN_RANGE(0, fRange) 	>> 		BREAK //Front Middle
				CASE 3  	vSwanCamOffset = << 2, 8, 0.1 		+ GET_RANDOM_FLOAT_IN_RANGE(0, fRange) 	>> 		BREAK //Front Right
				CASE 4  	vSwanCamOffset = << -8, 0.1, 0.1 	+ GET_RANDOM_FLOAT_IN_RANGE(0, fRange) 	>> 		BREAK //Side Left
				CASE 5  	vSwanCamOffset = << 8, 0.1, 0.1 	+ GET_RANDOM_FLOAT_IN_RANGE(0, fRange)	>> 		BREAK //Side Right
				CASE 6  	vSwanCamOffset = << 0, -2, 7.5 >>  													BREAK //Top Down				
				DEFAULT
					SET_SWAN_DIVE_CAM_STATE(SDSCS_Reset)
					REINIT_NET_TIMER(stDiveCamStallTimer)
				BREAK
				ENDSWITCH
				
				IF NOT IS_VECTOR_ZERO(vSwanCamOffset)
					
					#IF IS_DEBUG_BUILD
					IF IS_PED_A_PLAYER(pedForCamera)
						INT iPartForCamera
						iPartForCamera = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(NETWORK_GET_PLAYER_INDEX_FROM_PED(pedForCamera)))
						PRINTLN("PROCESS_SWAN_DIVE_SCORE_CAMERA - SDSCS_FindCameraSlot, iPartForCamera = ",iPartForCamera,", IS_PED_INJURED = ",IS_PED_INJURED(pedForCamera))
					ELSE
						PRINTLN("PROCESS_SWAN_DIVE_SCORE_CAMERA - SDSCS_FindCameraSlot, pedForCamera isn't a player? IS_PED_INJURED = ",IS_PED_INJURED(pedForCamera))
					ENDIF
					#ENDIF
					
					VECTOR vPedForCameraPos, scrVecPos
					FLOAT fPedForCameraHeading
					
					vPedForCameraPos = GET_ENTITY_COORDS(pedForCamera)
					fPedForCameraHeading = GET_ENTITY_HEADING(pedForCamera)
					scrVecPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPedForCameraPos, fPedForCameraHeading, vSwanCamOffset*1.1)
					
					PRINTLN("PROCESS_SWAN_DIVE_SCORE_CAMERA - SDSCS_FindCameraSlot, calling START_SHAPE_TEST_CAPSULE! vSwanCamOffset = ",vSwanCamOffset,", iSwanCamAngleTestSelection = ",iSwanCamAngleTestSelection)
					PRINTLN("PROCESS_SWAN_DIVE_SCORE_CAMERA - vPedForCameraPos = ",vPedForCameraPos,", fPedForCameraHeading = ",fPedForCameraHeading," => scrVecPos = ",scrVecPos)
					
					FLOAT fRadius
					IF bTinyRacers
						fRadius = 0.20
					ELSE
						fRadius = 0.15
					ENDIF
					
					siSwanDiveCamRay = START_SHAPE_TEST_CAPSULE(scrVecPos, vPedForCameraPos, fRadius, SCRIPT_INCLUDE_MOVER | SCRIPT_INCLUDE_VEHICLE | SCRIPT_INCLUDE_PED | SCRIPT_INCLUDE_OBJECT, pedForCamera)
					
				#IF IS_DEBUG_BUILD
				ELSE
					PRINTLN("PROCESS_SWAN_DIVE_SCORE_CAMERA - SDSCS_FindCameraSlot, vSwanCamOffset is zero so don't start shapetest! iSwanCamAngleTestSelection = ",iSwanCamAngleTestSelection)
				#ENDIF
				ENDIF
			ELSE
				
				SHAPETEST_STATUS stStatus
				stStatus = GET_SHAPE_TEST_RESULT(siSwanDiveCamRay, iSwanCamRayHitSomething, vSwanCamRayPos, vSwanCamRayNormal, eiSwanCamRayEntity)
				
				IF stStatus = SHAPETEST_STATUS_RESULTS_READY	
					
					PRINTLN("PROCESS_SWAN_DIVE_SCORE_CAMERA - SDSCS_FindCameraSlot, GET_SHAPE_TEST_RESULT returned SHAPETEST_STATUS_RESULTS_READY")
					
					IF DOES_ENTITY_EXIST(eiSwanCamRayEntity)
					AND iSwanCamRayHitSomething != 0						
						
						iSwanCamAngleTestSelection++
						
						IF (eiSwanCamRayEntity != NULL)
							RELEASE_SCRIPT_GUID_FROM_ENTITY(eiSwanCamRayEntity)
						ENDIF
						
						iSwanCamRayHitSomething = 0
						vSwanCamRayPos = <<-1,-1,-1>>
						vSwanCamRayNormal = <<-1,-1,-1>>
						siSwanDiveCamRay = NULL
						
						PRINTLN("PROCESS_SWAN_DIVE_SCORE_CAMERA - SDSCS_FindCameraSlot, shapetest hit something. Increment iSwanCamAngleTestSelection to ",iSwanCamAngleTestSelection," and try again")
					ELSE
						PRINTLN("PROCESS_SWAN_DIVE_SCORE_CAMERA - SDSCS_FindCameraSlot, shapetest didn't hit anything. Move on.")
						SET_SWAN_DIVE_CAM_STATE(SDSCS_InitCamera)
						REINIT_NET_TIMER(stDiveCamStallTimer)
					ENDIF
				ELSE
					IF stStatus = SHAPETEST_STATUS_NONEXISTENT
						
						iSwanCamAngleTestSelection++
						
						IF (eiSwanCamRayEntity != NULL)
							RELEASE_SCRIPT_GUID_FROM_ENTITY(eiSwanCamRayEntity)
						ENDIF
						
						iSwanCamRayHitSomething = 0
						vSwanCamRayPos = <<-1,-1,-1>>
						vSwanCamRayNormal = <<-1,-1,-1>>
						siSwanDiveCamRay = NULL
						
						ASSERTLN("PROCESS_SWAN_DIVE_SCORE_CAMERA - SDSCS_FindCameraSlot, GET_SHAPE_TEST_RESULT returned SHAPETEST_STATUS_NONEXISTENT! This should recover.")
						PRINTLN("PROCESS_SWAN_DIVE_SCORE_CAMERA - SDSCS_FindCameraSlot, GET_SHAPE_TEST_RESULT returned SHAPETEST_STATUS_NONEXISTENT! Increment iSwanCamAngleTestSelection to ",iSwanCamAngleTestSelection," and try again")
						
					#IF IS_DEBUG_BUILD
					ELSE
						PRINTLN("PROCESS_SWAN_DIVE_SCORE_CAMERA - SDSCS_FindCameraSlot, GET_SHAPE_TEST_RESULT returning SHAPETEST_STATUS_RESULTS_NOTREADY")
					#ENDIF
					ENDIF
				ENDIF

			ENDIF
		BREAK
		
		CASE SDSCS_InitCamera
			IF NOT DOES_CAM_EXIST(swanDiveCamera)			
				swanDiveCamera = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA",FALSE) 
			ENDIF
			
			IF DOES_CAM_EXIST(swanDiveCamera)
				IF (bTinyRacers
				AND HAS_NET_TIMER_STARTED(MC_serverBD_1.stTRLastPlayerTimer)
				AND GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_1.stTRLastPlayerTimer) > (g_FMMC_STRUCT.iTRResetTimer * 1000))
				OR NOT bTinyRacers

					IF bTinyRacers
						IF DOES_CAM_EXIST(g_birdsEyeCam)
							IF IS_CAM_ACTIVE(g_birdsEyeCam)
								SET_CAM_ACTIVE(g_birdsEyeCam, FALSE)
							ENDIF
						ENDIF
					ENDIF
					
					SET_CAM_ACTIVE(swanDiveCamera,TRUE)

					SET_CAM_PARAMS(swanDiveCamera, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(pedForCamera),GET_ENTITY_HEADING(pedForCamera), vSwanCamOffset), 
								   << 0, 0, 0 >>, PICK_FLOAT(IS_BIT_SET(iLocalBoolCheck16, LBOOL16_IS_DIVE_SCORE_ANIM_IN_PROGRESS), TO_FLOAT(GET_RANDOM_INT_IN_RANGE(18, 25)), 30))
					
					IF IS_BIT_SET(iLocalBoolCheck16, LBOOL16_IS_DIVE_SCORE_ANIM_IN_PROGRESS)
						POINT_CAM_AT_ENTITY(swanDiveCamera,pedForCamera,<<GET_RANDOM_FLOAT_IN_RANGE(-0.45,0.45),
																			GET_RANDOM_FLOAT_IN_RANGE(0.45,0.45),
																			GET_RANDOM_FLOAT_IN_RANGE(-0.5,-0.1) - (vSwanCamOffset.z / 4) >>)
					ELSE
						POINT_CAM_AT_ENTITY(swanDiveCamera,pedForCamera,<< 0,0,0 >>)
					ENDIF
					
					IF bTinyRacers
						IF DOES_CAM_EXIST(g_birdsEyeCam)
							SET_CAM_DOF_FNUMBER_OF_LENS(g_birdsEyeCam, fTRNumLens)
							SET_CAM_DOF_FOCAL_LENGTH_MULTIPLIER(g_birdsEyeCam, fTRFocalLength)
							SET_CAM_DOF_MAX_NEAR_IN_FOCUS_DISTANCE(g_birdsEyeCam, 2)
							SET_CAM_DOF_MAX_NEAR_IN_FOCUS_DISTANCE_BLEND_LEVEL(g_birdsEyeCam, fTRBlendLevelNearIn)
							SET_CAM_DOF_STRENGTH(g_birdsEyeCam, fTRDOF)
						ENDIF
					ENDIF
					
					SET_BIT(iLocalBoolCheck16, LBOOL16_DIVE_SCORE_SCREEN_FX_ACTIVE)
					
					PRINTLN("PROCESS_SWAN_DIVE_SCORE_CAMERA - setting LBOOL16_DIVE_SCORE_SCREEN_FX_ACTIVE. 1")
					
					RENDER_SCRIPT_CAMS(TRUE,FALSE)
					SET_SWAN_DIVE_CAM_STATE(SDSCS_WaitForDiveFinish)
					REINIT_NET_TIMER(stDiveCamStallTimer)
				ENDIF
			ENDIF
		BREAK
		CASE SDSCS_WaitForDiveFinish			
			IF IS_BIT_SET(iLocalBoolCheck16, LBOOL16_DIVE_SCORE_RENDERPAUSE_ACTIVE)
			OR (bTinyRacers AND IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_TINY_RACERS_SCREEN_PAUSED))
				SET_SWAN_DIVE_CAM_STATE(SDSCS_Reset)
				REINIT_NET_TIMER(stDiveCamStallTimer)
			ENDIF
		BREAK
		
		CASE SDSCS_FindRemoteCamSlot
			PRINTLN("SDSCS_FindRemoteCamSlot 1")
			IF (siSwanDiveCamRay = NULL)				
				vSwanCamOffset = << 0.3, 0.9, 0.40 >>
				IF IS_PED_IN_ANY_PLANE(pedForCamera)
					iSwanCamAngleTestSelection = 2
				ENDIF					
								
				IF g_FMMC_STRUCT.iOverridePauseCamDistance > -1
					IF GET_PLAYER_RESPAWN_BACK_AT_START_STATE(iLocalPart) != PRBSS_Invalid
						IF NOT IS_PED_INJURED(LocalPlayerPed)
							PRINTLN("[LM][PROCESS_SWAN_DIVE_SCORE_CAMERA] - calling RESET_ENTITY_ALPHA On Player")
							RESET_ENTITY_ALPHA(LocalPlayerPed)
							IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
							AND DOES_ENTITY_EXIST(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
								PRINTLN("[LM][PROCESS_SWAN_DIVE_SCORE_CAMERA] - calling RESET_ENTITY_ALPHA On Veh")
								RESET_ENTITY_ALPHA(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
							ENDIF
						ENDIF
					ENDIF
					
					VEHICLE_INDEX vehIn
					FLOAT fRange
					fRange = GET_RANDOM_FLOAT_IN_RANGE(3, TO_FLOAT(g_FMMC_STRUCT.iOverridePauseCamDistance))
					
					PRINTLN("[LM] PROCESS_SWAN_DIVE_SCORE_CAMERA - fRange: ", fRange)
					PRINTLN("[LM] PROCESS_SWAN_DIVE_SCORE_CAMERA - iSwanCamAngleTestSelection: ", iSwanCamAngleTestSelection)
					
					IF IS_PED_IN_ANY_VEHICLE(PedForCamera)					
						PRINTLN("[LM] PROCESS_SWAN_DIVE_SCORE_CAMERA - In a vehicle.")						
						vehIn = GET_VEHICLE_PED_IS_IN(PedForCamera)
												
						IF NETWORK_HAS_ENTITY_BEEN_REGISTERED_WITH_THIS_THREAD(vehIn)
						AND NETWORK_DOES_NETWORK_ID_EXIST(VEH_TO_NET(vehIn))
						AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(VEH_TO_NET(vehIn))
							PRINTLN("PROCESS_SWAN_DIVE_SCORE_CAMERA - SET_VEHICLE_HANDBRAKE ON VEH")
							SET_VEHICLE_HANDBRAKE(vehIn, TRUE)
						ELSE
							PRINTLN("[LM] PROCESS_SWAN_DIVE_SCORE_CAMERA - NOT DRIVER")
						ENDIF
					ELSE
						PRINTLN("[LM] PROCESS_SWAN_DIVE_SCORE_CAMERA - Not in a vehicle.")
					ENDIF
					
					IF NOT INITIALIZE_DUMMY_COMPASS_OBJECT()
						PRINTLN("[LM] PROCESS_SWAN_DIVE_SCORE_CAMERA - Waiting for dummy obj..")
						EXIT
					ENDIF
					
					VECTOR vecObjDummy
					FLOAT frotObjDummy					
					ENTITY_INDEX eiFocus
					
					eiFocus = PedForCamera
					IF DOES_ENTITY_EXIST(vehIn)
						eiFocus = vehIn
					ENDIF
					vecObjDummy = GET_ENTITY_COORDS(eiFocus)					
					frotObjDummy = GET_ENTITY_HEADING(eiFocus)
					SET_ENTITY_COORDS(objTopDownCamCompass, vecObjDummy)
					SET_ENTITY_HEADING(objTopDownCamCompass, frotObjDummy)
					
					SWITCH iSwanCamAngleTestSelection
					CASE 1 		vSwanCamOffset = << -2-fRange,	 4 + fRange, 	    0.1 	+ (fRange/4) 	>>		BREAK //Front Left
					CASE 2 		vSwanCamOffset = << 0, 		  	 6 + fRange, 		0.1 	+ (fRange/4)	>> 		BREAK //Front Middle
					CASE 3  	vSwanCamOffset = << 2+ fRange,   4 + fRange, 		0.1 	+ (fRange/4) 	>> 		BREAK //Front Right
					CASE 4  	vSwanCamOffset = << -8-fRange, 	 0.1, 				0.1 	+ (fRange/4) 	>> 		BREAK //Side Left
					CASE 5  	vSwanCamOffset = << 8+ fRange, 	 0.1, 				0.1 	+ (fRange/4)	>> 		BREAK //Side Right
					CASE 6  	vSwanCamOffset = << 0, 			 -4 - fRange, 		0.1 	+ (fRange/4)	>> 		BREAK //Behind
					CASE 7  	vSwanCamOffset = << 4+ fRange,   -4 + fRange, 		0.1 	+ (fRange/4)	>> 		BREAK //Behind Right
					CASE 8  	vSwanCamOffset = << 0, -2, 7.5 >>  													BREAK //Top Down
					CASE 9  	vSwanCamOffset = << 0, -2, -7.5 >>  												BREAK //Under
					CASE 10  	vSwanCamOffset = << 0, 4, -7.5 >>  													BREAK //Under Offset Right				
					CASE 11
						vSwanCamOffset = << 0, 0, 7.5 >>
						IF DOES_ENTITY_EXIST(vehIn)
							IF NOT IS_ENTITY_UPRIGHT(vehIn, 90)
								vSwanCamOffset = << 0, 0, -7.5 >>
							ENDIF
						ENDIF
					BREAK //Top Down Flat
					DEFAULT
						SET_SWAN_DIVE_CAM_STATE(SDSCS_Reset)
						REINIT_NET_TIMER(stDiveCamStallTimer)
						siSwanDiveCamRay = NULL
					BREAK
					ENDSWITCH
					
					IF NOT IS_VECTOR_ZERO(vSwanCamOffset)
						siSwanDiveCamRay = START_SHAPE_TEST_CAPSULE(GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(eiFocus), GET_ENTITY_HEADING(eiFocus), vSwanCamOffset*1.05), 
																	  GET_ENTITY_COORDS(eiFocus), 0.1, SCRIPT_INCLUDE_MOVER | SCRIPT_INCLUDE_VEHICLE | SCRIPT_INCLUDE_OBJECT, eiFocus)
					ENDIF
					
					vSwanCamCached = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(eiFocus),GET_ENTITY_HEADING(eiFocus), vSwanCamOffset*1.05)
					
					#IF IS_DEBUG_BUILD
					IF bRunningBackDiveCamLine 
						vDebugForDiveCamStartPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(eiFocus),GET_ENTITY_HEADING(eiFocus), vSwanCamOffset*1.05)
						vDebugForDiveCamStartEndPos = GET_ENTITY_COORDS(eiFocus)
					ENDIF
					#ENDIF
				ELSE
					SWITCH iSwanCamAngleTestSelection
					CASE 1  	vSwanCamOffset = << 0.3, 0.9, 0.40 >>  		BREAK
					CASE 2		vSwanCamOffset = << 2, 8, 0.8 + GET_RANDOM_FLOAT_IN_RANGE(0, 5) >> BREAK
					DEFAULT
						SET_SWAN_DIVE_CAM_STATE(SDSCS_Reset)
						REINIT_NET_TIMER(stDiveCamStallTimer)
						siSwanDiveCamRay = NULL
					BREAK
					ENDSWITCH
					IF IS_PED_IN_ANY_VEHICLE(localPlayerPed)
						// High Dashboard cars.
						IF GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(localPlayerPed)) = PANTO
							vSwanCamOffset += << 0.0, 0.1, 0.25 >>
						ELIF GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(localPlayerPed)) = TEZERACT
							vSwanCamOffset += << 0.0, 0.075, 0.15 >>
						ENDIF
					ENDIF
					IF NOT IS_VECTOR_ZERO(vSwanCamOffset)
						siSwanDiveCamRay = START_SHAPE_TEST_CAPSULE(GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(pedForCamera),GET_ENTITY_HEADING(pedForCamera), vSwanCamOffset*1.05), 
																	  GET_ENTITY_COORDS(pedForCamera), 0.25, SCRIPT_INCLUDE_MOVER | SCRIPT_INCLUDE_VEHICLE | SCRIPT_INCLUDE_PED | SCRIPT_INCLUDE_OBJECT,pedForCamera)
					ENDIF
				ENDIF
			ELSE
				PRINTLN("SDSCS_FindRemoteCamSlot 3")
				IF GET_SHAPE_TEST_RESULT(siSwanDiveCamRay,iSwanCamRayHitSomething,vSwanCamRayPos,
										 vSwanCamRayNormal,eiSwanCamRayEntity) = SHAPETEST_STATUS_RESULTS_READY		
					PRINTLN("SDSCS_FindRemoteCamSlot 4")					 
					
					IF DOES_ENTITY_EXIST(eiSwanCamRayEntity)
					AND iSwanCamRayHitSomething != 0						
						PRINTLN("SDSCS_FindRemoteCamSlot 5")
						iSwanCamAngleTestSelection++
						IF (eiSwanCamRayEntity != NULL)
							RELEASE_SCRIPT_GUID_FROM_ENTITY(eiSwanCamRayEntity)
						ENDIF
						
						IF IS_ENTITY_AN_OBJECT(eiSwanCamRayEntity)
							PRINTLN("SDSCS_FindRemoteCamSlot 5 We hit: ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(eiSwanCamRayEntity)))
						ELSE
							PRINTLN("SDSCS_FindRemoteCamSlot 5 We hit: Not a script object")
						ENDIF
						
						iSwanCamRayHitSomething = 0
						vSwanCamRayPos = <<-1,-1,-1>>
						vSwanCamRayNormal = <<-1,-1,-1>>
						siSwanDiveCamRay = NULL
						eiSwanCamRayEntity = NULL
					ELSE
						iSwanCamAngleTestSelection = 1
						
						IF (eiSwanCamRayEntity != NULL)
							RELEASE_SCRIPT_GUID_FROM_ENTITY(eiSwanCamRayEntity)
						ENDIF
						
						iSwanCamRayHitSomething = 0
						vSwanCamRayPos = <<-1,-1,-1>>
						vSwanCamRayNormal = <<-1,-1,-1>>
						siSwanDiveCamRay = NULL
						eiSwanCamRayEntity = NULL
											
						PRINTLN("SDSCS_FindRemoteCamSlot 6")
						SET_SWAN_DIVE_CAM_STATE(SDSCS_InitNonScorerCam)
						REINIT_NET_TIMER(stDiveCamStallTimer)
						
						PROCESS_SWAN_DIVE_SCORE_CAMERA(bTinyRacers, bPauseOnPlayer)
					ENDIF
				ENDIF
			ENDIF
		BREAK

		CASE SDSCS_InitNonScorerCam		 	
			IF NOT DOES_CAM_EXIST(swanDiveCamera)			
				swanDiveCamera = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA",FALSE) 
			ENDIF
			
			IF DOES_CAM_EXIST(swanDiveCamera)
				IF NOT IS_CAM_RENDERING(swanDiveCamera)
					IF bPauseOnPlayer
						IF DOES_CAM_EXIST(g_birdsEyeCam)
							IF IS_CAM_ACTIVE(g_birdsEyeCam)
								SET_CAM_ACTIVE(g_birdsEyeCam, FALSE)
							ENDIF
							bUsingBirdsEyeCam = FALSE
							g_iFMMCCurrentStaticCam = -1
						ENDIF
					ENDIF
					
					IF pedForCamera = LocalPlayerPed
					AND NOT IS_PARTICIPANT_A_SPECTATOR(iLocalPart)
						NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE, NSPC_CLEAR_TASKS | NSPC_NO_COLLISION | NSPC_FREEZE_POSITION)
						CLEAR_PED_TASKS(LocalPlayerPed)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(LocalPlayerPed, FALSE, FALSE)
					ENDIF
					
					SET_CAM_PARAMS(swanDiveCamera,GET_ENTITY_COORDS(pedForCamera), << 0, 0, 0 >>, 45)
					
					IF DOES_ENTITY_EXIST(objTopDownCamCompass) // only use with conjunction: g_FMMC_STRUCT.iOverridePauseCamDistance > -1
					AND NOT IS_VECTOR_ZERO(vSwanCamCached)
						SET_CAM_PARAMS(swanDiveCamera, vSwanCamCached, << 0, 0, 0 >>, 45)
						POINT_CAM_AT_ENTITY(swanDiveCamera, objTopDownCamCompass, << 0,0,-0.675 >>)
					ELSE
						POINT_CAM_AT_ENTITY(swanDiveCamera,pedForCamera, << 0,0,0.675 >>)
						ATTACH_CAM_TO_ENTITY(swanDiveCamera, pedForCamera, vSwanCamOffset)
					ENDIF
				
					IF bPauseOnPlayer
						SET_CAM_DOF_OVERRIDDEN_FOCUS_DISTANCE(swanDiveCamera, VDIST(GET_CAM_COORD(swanDiveCamera), GET_PED_BONE_COORDS(pedForCamera, BONETAG_HEAD, <<0,0,0>>)))
						SET_CAM_DOF_OVERRIDDEN_FOCUS_DISTANCE_BLEND_LEVEL(swanDiveCamera, 1.0)
						SET_LOCK_ADAPTIVE_DOF_DISTANCE(TRUE)
						PRINTLN("[JS] LOCKING DOF")
					ENDIF
					
					SET_CAM_ACTIVE(swanDiveCamera,TRUE)
					RENDER_SCRIPT_CAMS(TRUE,FALSE)
				ENDIF
				
				iCamRenderingChecks++			
				
				IF IS_CAM_RENDERING(swanDiveCamera)
				OR iCamRenderingChecks >= 3
					IF bPauseOnPlayer
						IF g_FMMC_STRUCT.iOverridePauseCamDistance > -1
						AND GET_PLAYER_RESPAWN_BACK_AT_START_STATE(iLocalPart) != PRBSS_Sync
						AND GET_PLAYER_RESPAWN_BACK_AT_START_STATE(iLocalPart) != PRBSS_Invalid	
							IF NOT IS_PED_INJURED(LocalPlayerPed)
								PRINTLN("[LM][PROCESS_SWAN_DIVE_SCORE_CAMERA] - calling RESET_ENTITY_ALPHA On Player")
								RESET_ENTITY_ALPHA(LocalPlayerPed)
								IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
								AND DOES_ENTITY_EXIST(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
									PRINTLN("[LM][PROCESS_SWAN_DIVE_SCORE_CAMERA] - calling RESET_ENTITY_ALPHA On Veh")
									RESET_ENTITY_ALPHA(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
								ENDIF
							ENDIF
							PRINTLN("[LM][PROCESS_SWAN_DIVE_SCORE_CAMERA] - PAUSING ON PLAYER AT FIRST SIGN OF A SAFE CAMERA ANGLE - THIS IS TO FIX ISSUES WHERE THE CAMERA BECOMES UNSAFE (under the floor)..")
							IF NOT ANIMPOSTFX_IS_RUNNING("InchOrange")
								PRINTLN("[LM][PROCESS_SWAN_DIVE_SCORE_CAMERA] - Playing Orange FX")
								ANIMPOSTFX_PLAY("InchOrange", 0, TRUE)
							ENDIF
							TOGGLE_PAUSED_RENDERPHASES(FALSE)
						ENDIF
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_ENABLE_RUNNING_BACK_REMIX_SOUNDS)
						AND NOT IS_BIT_SET(iLocalBoolCheck29, LBOOL29_PLAYED_RUNNING_BACK_FLASH_SFX)
						AND GET_PLAYER_RESPAWN_BACK_AT_START_STATE(iLocalPart) != PRBSS_Sync
						AND GET_PLAYER_RESPAWN_BACK_AT_START_STATE(iLocalPart) != PRBSS_PlayingNormally
						AND GET_PLAYER_RESPAWN_BACK_AT_START_STATE(iLocalPart) != PRBSS_Invalid
							PLAY_SOUND_FRONTEND(-1, "End_Zone_Flash", "DLC_BTL_RB_Remix_Sounds")
							START_AUDIO_SCENE("DLC_BTL_RB_Remix_End_Zone_Mute_Scene")
							SET_BIT(iLocalBoolCheck29, LBOOL29_PLAYED_RUNNING_BACK_FLASH_SFX)
						ENDIF
					ENDIF
					
					PRINTLN("PROCESS_SWAN_DIVE_SCORE_CAMERA - setting LBOOL16_DIVE_SCORE_SCREEN_FX_ACTIVE. 2")
					
					SET_BIT(iLocalBoolCheck16, LBOOL16_DIVE_SCORE_SCREEN_FX_ACTIVE)
					SET_SWAN_DIVE_CAM_STATE(SDSCS_WaitForNonScoreCamFinish)
					REINIT_NET_TIMER(stDiveCamStallTimer)
					iCamRenderingChecks = 0
				ENDIF
			ENDIF
		BREAK
		
		CASE SDSCS_WaitForNonScoreCamFinish
			IF pedForCamera = LocalPlayerPed
				CLEAR_PED_TASKS(LocalPlayerPed)
			ENDIF
			IF (IS_BIT_SET(iLocalBoolCheck16, LBOOL16_DIVE_SCORE_RENDERPAUSE_ACTIVE) AND NOT bPauseOnPlayer)
			OR (bPauseOnPlayer AND IS_BIT_SET(MC_serverBD.iServerBitSet5, SBBOOL5_ALL_PLAYERS_RENDERPHASES_PAUSED) AND NOT GET_TOGGLE_PAUSED_RENDERPHASES_STATUS())
			OR (bTinyRacers AND IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_TINY_RACERS_SCREEN_PAUSED))
			OR (bPauseOnPlayer AND GET_PLAYER_RESPAWN_BACK_AT_START_STATE(iLocalPart) = PRBSS_PlayingNormally AND HAS_NET_TIMER_EXPIRED_READ_ONLY(stDiveCamStallTimer, 3500))
				PRINTLN("[JS][PROCESS_SWAN_DIVE_SCORE_CAMERA] RESET SDSCS_WaitForNonScoreCamFinish")
				SET_SWAN_DIVE_CAM_STATE(SDSCS_Reset)
				REINIT_NET_TIMER(stDiveCamStallTimer)
			ENDIF
		BREAK
		
		CASE SDSCS_Reset			
			RESET_SWAN_DIVE_SCORE_CAMERA(bTinyRacers, bPauseOnPlayer, pedForCamera)
		BREAK
	ENDSWITCH
ENDPROC

PROC ADD_PLAYER_TO_RESPAWN_QUEUE(INT iPart, INT iTeam)
	INT i
	IF iPart != -1
		FOR i = 0 TO FMMC_MAX_REZQ_PARTS - 1
			IF MC_serverBD_1.iRespawnQueue[iTeam][i] = iPart
				EXIT
			ENDIF
		ENDFOR
		FOR i = 0 TO FMMC_MAX_REZQ_PARTS - 1
			
			IF MC_serverBD_1.iRespawnQueue[iTeam][i] = -1
				PRINTLN("[JS] [REZQ] - ADD_PLAYER_TO_RESPAWN_QUEUE - inserting part: ",iPart," at position: ",i," for team: ", iTeam)
				MC_serverBD_1.iRespawnQueue[iTeam][i] = iPart
				BREAKLOOP
			ENDIF
		ENDFOR
	ENDIF
ENDPROC

FUNC INT GET_POLICE_MAX_WANTED_FROM_MENU_SETTING(INT iwanted)

INT imaxwanted = 5

	SWITCH iwanted
		CASE ciMISSION_MAX_WANTED_1
			imaxwanted = 1
		BREAK
		CASE ciMISSION_MAX_WANTED_2
			imaxwanted = 2
		BREAK
		CASE ciMISSION_MAX_WANTED_3
			imaxwanted = 3
		BREAK
		CASE ciMISSION_MAX_WANTED_4
			imaxwanted = 4
		BREAK
		CASE ciMISSION_MAX_WANTED_5
			imaxwanted = 5
		BREAK
	ENDSWITCH
	
	RETURN imaxwanted
	
ENDFUNC

PROC DO_CORONA_POLICE_SETTINGS()
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].iSurpressWantedBitset, 0)
		CLIENT_SET_POLICE(POLICE_OFF)
		g_bDeactivateRestrictedAreas = TRUE
	ELSE
		IF MC_serverBD.iPolice > 1
			IF GET_POLICE_MAX_WANTED_FROM_MENU_SETTING(MC_serverBD.iPolice) > 0
				SET_MAX_WANTED_LEVEL(GET_POLICE_MAX_WANTED_FROM_MENU_SETTING(MC_serverBD.iPolice))
				PRINTLN("[RCC MISSION] setting max wanted on client to : ",GET_POLICE_MAX_WANTED_FROM_MENU_SETTING(MC_serverBD.iPolice))
				g_bDeactivateRestrictedAreas = FALSE
			ENDIF
		ELSE
			IF MC_serverBD.iPolice = 1
				g_bDeactivateRestrictedAreas = TRUE
				PRINTLN("[RCC MISSION]corona police setting cops and military base off")
			ELSE
				g_bDeactivateRestrictedAreas = FALSE
				PRINTLN("[RCC MISSION]corona police setting cops and military base on")
			ENDIF
			
			CLIENT_SET_POLICE(MC_serverBD.iPolice)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciBLOCK_ON_FOOT_DISPATCH)
		SET_CREATE_RANDOM_COPS( FALSE )
		SET_PED_MODEL_IS_SUPPRESSED(S_M_M_Security_01, TRUE)
		PRINTLN("GAME_STATE_INI - Disabling cops and security guards" )
	ENDIF
ENDPROC

FUNC BOOL IS_ANY_OVERTIME_HUD_ACTIVE()
	INT iTeam = MC_playerBD[iLocalPart].iteam
	
	IF MC_serverBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEight[MC_serverBD_4.iCurrentHighestPriority[iTeam]], ciBS_RULE8_OVERTIME_SCORE_HUD)
		OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEight[MC_serverBD_4.iCurrentHighestPriority[iTeam]], ciBS_RULE8_OVERTIME_TICK_HUD)
		OR IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetNine[MC_serverBD_4.iCurrentHighestPriority[iTeam]], ciBS_RULE9_OVERTIME_RUMBLE_HUD)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC OVERTIME_DID_NOT_SCORE(INT iPart)
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciOVERTIME_WAIT_YOUR_TURN)
		IF iOTPartToMove = iLocalPart
		AND MC_serverBD_1.iPartToMove[MC_serverBD_1.iTeamToMove] < MAX_PENALTY_PLAYERS
			PRINTLN("OVERTIME_DID_NOT_SCORE - resetting iMyTurnScore[",MC_serverBD_1.iPartToMove[MC_serverBD_1.iTeamToMove],"] to 0")
			MC_Playerbd[iLocalPart].iMyTurnScore[MC_serverBD_1.iPartToMove[MC_serverBD_1.iTeamToMove]] = 0
		ENDIF		

		// We want to delay a shard so we need to cache this information.
		iDelayedOvertimeShardScore = 0		
		iDelayedOvertimeShardTeam = MC_playerBD[iPart].iteam
		iDelayedOvertimeShardRule = MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iPart].iteam]
		iDelayedOvertimePartToMove = iPart
		iDelayedOvertimeCurrentRound = MC_serverBD_1.iCurrentRound
	ENDIF
	
	SET_BIT(iLocalBoolCheck22, LBOOL22_OVERTIME_DELAYED_SCORE_SHARD)
	SET_BIT(iLocalBoolCheck23, LBOOL23_OVERTIME_IGNORE_RESET_AT_START_DELAY)
	
	PRINTLN("OVERTIME_DID_NOT_SCORE - Called...")
	
ENDPROC

PROC BLOCK_ALL_PICKUPS_FOR_LOCAL_PLAYER(BOOL bBlock)
	INT i
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons - 1
		IF i < FMMC_MAX_WEAPONS
			IF DOES_PICKUP_EXIST(pipickup[i])
				SET_PICKUP_UNCOLLECTABLE(pipickup[i], bBlock)
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

PROC PLAY_JUGGERNAUT_TRANSFORM_EFFECTS()
	IF NOT g_bMissionOver
		SET_PARTICLE_FX_NON_LOOPED_COLOUR(GET_BEAST_PARTICLE_COLOUR(0, TRUE), GET_BEAST_PARTICLE_COLOUR(1, TRUE), GET_BEAST_PARTICLE_COLOUR(2, TRUE))
		USE_PARTICLE_FX_ASSET("scr_powerplay")
		START_NETWORKED_PARTICLE_FX_NON_LOOPED_ON_PED_BONE("scr_powerplay_beast_appear", localPlayerPed, <<0, 0, -0.3>>, <<0, 0, 0>>, BONETAG_PELVIS, 0.75)
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciENABLE_POWER_MAD_SOUNDS)
			PLAY_SOUND_FROM_ENTITY(-1, "Transform_JN_VFX", LocalPlayerPed, "DLC_GR_PM_Juggernaut_Player_Sounds", TRUE)
		ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_ENABLE_TRADING_PLACES_SOUNDS)
			PLAY_SOUND_FROM_ENTITY(-1, "Transform_JN_VFX", LocalPlayerPed, "DLC_BTL_TP_Remix_Juggernaut_Player_Sounds", TRUE)
		ELSE
			PLAY_SOUND_FROM_ENTITY(-1, "Transform_JN_VFX", LocalPlayerPed, "DLC_IE_JN_Player_Sounds", TRUE)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL TRANSFORM_LOCAL_PLAYER_INTO_JUGGERNAUT(BOOL bTransformBack = FALSE, INT iMaxHealth = 1100)
	IF bTransformBack
		IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_TRANSFORMED_INTO_JUGGERNAUT)
			PRINTLN("[JS] TRANSFORM_LOCAL_PLAYER_INTO_JUGGERNAUT - We are not a juggernaut")
			RETURN FALSE
		ENDIF
		
		REINIT_NET_TIMER(tdTransformDelayTimer)
		
		CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_TRANSFORMED_INTO_JUGGERNAUT)
		PRINTLN("[JS] - TRANSFORM_LOCAL_PLAYER_INTO_JUGGERNAUT - NO LONGER THE JUGGERNAUT")
		
		//Audio
		STOP_AUDIO_SCENE("DLC_IE_JN_Player_Is_JN_Scene")
		
		//Outfit
		APPLY_OUTFIT_TO_LOCAL_PLAYER(INT_TO_ENUM(MP_OUTFIT_ENUM, GET_LOCAL_DEFAULT_OUTFIT(MC_playerBD[iLocalPart].iTeam)))
		
		//Weapons
		IF HAS_PED_GOT_WEAPON(LocalPlayerPed, WEAPONTYPE_MINIGUN)
			SET_PED_AMMO(LocalPlayerPed, WEAPONTYPE_MINIGUN, 0, TRUE)
			REMOVE_WEAPON_FROM_PED(LocalPlayerPed, WEAPONTYPE_MINIGUN)
		ENDIF
		IF HAS_PED_GOT_WEAPON(LocalPlayerPed, WEAPONTYPE_DLC_RAILGUN)
			SET_PED_AMMO(LocalPlayerPed, WEAPONTYPE_DLC_RAILGUN, 0, TRUE)
			REMOVE_WEAPON_FROM_PED(LocalPlayerPed, WEAPONTYPE_DLC_RAILGUN)
		ENDIF
		IF HAS_PED_GOT_WEAPON(LocalPlayerPed, WEAPONTYPE_DLC_PROXMINE)
			SET_PED_AMMO(LocalPlayerPed, WEAPONTYPE_DLC_PROXMINE, 0, TRUE)
			REMOVE_WEAPON_FROM_PED(LocalPlayerPed, WEAPONTYPE_DLC_PROXMINE)
		ENDIF
		SET_CAN_PED_SELECT_ALL_WEAPONS(LocalPlayerPed, TRUE)
		WEAPON_TYPE wtCurrentWeap
		GET_CURRENT_PED_WEAPON(LocalPlayerPed, wtCurrentWeap)
		IF wtCurrentWeap != GET_BEST_PED_WEAPON(LocalPlayerPed)
			SET_CURRENT_PED_WEAPON(LocalPlayerPed, GET_BEST_PED_WEAPON(LocalPlayerPed), TRUE)
		ENDIF
		SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_BlockAutoSwapOnWeaponPickups, FALSE)
		BLOCK_ALL_PICKUPS_FOR_LOCAL_PLAYER(FALSE)
		//VFX
		IF bLocalPlayerPedOK
			PLAY_JUGGERNAUT_TRANSFORM_EFFECTS()
		ENDIF
		//Ped Settings
		RESET_PLAYER_HEALTH()
		BLOCK_PLAYER_HEALTH_REGEN(FALSE)
		SET_PLAYER_CAN_USE_COVER(LocalPlayer, TRUE)
		SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_DontActivateRagdollFromExplosions, FALSE)
		IF MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam] < FMMC_MAX_RULES
		AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].iRuleBitsetSix[MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam]], ciBS_RULE6_RESPAWN_ON_RULE_END)
			SET_PLAYER_PROOFS_FOR_TEAM(MC_playerBD[iLocalPart].iTeam)
		ELSE
			IF NOT bLocalPlayerPedOK
				SET_PLAYER_PROOFS_FOR_TEAM(MC_playerBD[iLocalPart].iTeam)
			ENDIF
		ENDIF
		
		IF NOT bLocalPlayerPedOK
			SET_BIT(iLocalBoolCheck23, LBOOL23_CLEANUP_OUTFIT_ON_RESPAWN)
		ENDIF
		
	ELSE
		IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_TRANSFORMED_INTO_JUGGERNAUT)
		OR NOT bLocalPlayerPedOk
		OR IS_PLAYER_RESPAWNING(LocalPlayer)
		OR g_bMissionOver
			PRINTLN("[JS] TRANSFORM_LOCAL_PLAYER_INTO_JUGGERNAUT - We are already a juggernaut or dead")
			RETURN FALSE
		ENDIF
		
		IF HAS_NET_TIMER_STARTED(tdTransformDelayTimer)
			IF NOT HAS_NET_TIMER_EXPIRED(tdTransformDelayTimer, ciTRANSFORM_DELAY_TIME)
				PRINTLN("[JS] TRANSFORM_LOCAL_PLAYER_INTO_JUGGERNAUT - Trying to transform too quickly after another transform")
				RETURN FALSE
			ENDIF
		ENDIF
		
		SET_BIT(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_TRANSFORMED_INTO_JUGGERNAUT)
		PRINTLN("[JS] - TRANSFORM_LOCAL_PLAYER_INTO_JUGGERNAUT - IM THE JUGGERNAUT")
		
		//Audio
		START_AUDIO_SCENE("DLC_IE_JN_Player_Is_JN_Scene")
		PLAY_SOUND_FROM_ENTITY(-1, "Become_JN", LocalPlayerPed, "DLC_GR_PM_Juggernaut_Player_Sounds", TRUE)
		
		//Ped Settings
		SET_PLAYER_CAN_USE_COVER(LocalPlayer, FALSE)
		SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_DontActivateRagdollFromExplosions, TRUE)
		BLOCK_PLAYER_HEALTH_REGEN(TRUE)
		bFMMCHealthBoosted = FALSE
		IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iTeam] < FMMC_MAX_RULES
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iTeam].iRuleBitsetEight[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iTeam]], ciBS_RULE8_SCALE_MAX_PLAYER_HEALTH)
				INT i
				INT iEnemyPlayers
				FOR i = 0 TO MC_serverBD.iNumberOfTeams - 1
					PRINTLN("[JS] - TRANSFORM_LOCAL_PLAYER_INTO_JUGGERNAUT - Health is being scaled by players: ", MC_serverBD.iNumberOfPlayingPlayers[i])
					iEnemyPlayers = iEnemyPlayers + MC_serverBD.iNumberOfPlayingPlayers[i]
				ENDFOR
				iMaxHealth = (iMaxHealth * iEnemyPlayers)
			ENDIF
		ENDIF
		INCREASE_PLAYER_HEALTH(iMaxHealth)
		SET_ENTITY_PROOFS(LocalPlayerPed, FALSE, FALSE, FALSE, FALSE, TRUE)
		
		//Outfit
		MP_OUTFIT_ENUM eOutfit = OUTFIT_HIDDEN_IE_JN_TARGET_0
		SWITCH(GET_HUD_COLOUR_FOR_FMMC_TEAM(MC_playerBD[iLocalPart].iteam, LocalPlayer))
			CASE HUD_COLOUR_ORANGE
				eOutfit = OUTFIT_HIDDEN_IE_JN4_ORANGE_TARGET_0
			BREAK
			CASE HUD_COLOUR_PURPLE
				eOutfit = OUTFIT_HIDDEN_IE_JN4_PURPLE_TARGET_0
			BREAK
			CASE HUD_COLOUR_GREEN
				eOutfit = OUTFIT_HIDDEN_IE_JN4_GREEN_TARGET_0
			BREAK
			CASE HUD_COLOUR_PINK
				eOutfit = OUTFIT_HIDDEN_IE_JN4_PINK_TARGET_0
			BREAK
		ENDSWITCH
		APPLY_OUTFIT_TO_LOCAL_PLAYER(eOutfit)
		
		//Weapons
		CLEAR_PED_TASKS_IMMEDIATELY(LocalPlayerPed)
		SET_CAN_PED_SELECT_ALL_WEAPONS(LocalPlayerPed, FALSE)
		GIVE_WEAPON_TO_PED(LocalPlayerPed, WEAPONTYPE_DLC_RAILGUN, 2)
		SET_CAN_PED_SELECT_INVENTORY_WEAPON(LocalPlayerPed, WEAPONTYPE_DLC_RAILGUN, TRUE)
		HUD_SET_WEAPON_WHEEL_TOP_SLOT(WEAPONTYPE_DLC_RAILGUN)
		GIVE_WEAPON_TO_PED(LocalPlayerPed, WEAPONTYPE_DLC_PROXMINE, 5)
		SET_CAN_PED_SELECT_INVENTORY_WEAPON(LocalPlayerPed, WEAPONTYPE_DLC_PROXMINE, TRUE)
		HUD_SET_WEAPON_WHEEL_TOP_SLOT(WEAPONTYPE_DLC_PROXMINE)
		GIVE_WEAPON_TO_PED(LocalPlayerPed, WEAPONTYPE_MINIGUN, -1, TRUE)
		SET_CAN_PED_SELECT_INVENTORY_WEAPON(LocalPlayerPed, WEAPONTYPE_MINIGUN, TRUE)
		HUD_SET_WEAPON_WHEEL_TOP_SLOT(WEAPONTYPE_MINIGUN)
		IF HAS_PED_GOT_WEAPON(LocalPlayerPed, WEAPONTYPE_MINIGUN)
			WEAPON_TYPE wtCurrentWeapon
			GET_CURRENT_PED_WEAPON(LocalPlayerPed, wtCurrentWeapon)
			IF wtCurrentWeapon != WEAPONTYPE_MINIGUN
				SET_CURRENT_PED_WEAPON(LocalPlayerPed, WEAPONTYPE_MINIGUN, TRUE)
			ELSE
				PRINTLN("[JS] - TRANSFORM_LOCAL_PLAYER_INTO_JUGGERNAUT - already holding a minigun???")
			ENDIF
		ELSE
			PRINTLN("[JS] - TRANSFORM_LOCAL_PLAYER_INTO_JUGGERNAUT - We don't have a minigun???")
		ENDIF
		SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_BlockAutoSwapOnWeaponPickups, TRUE)
		BLOCK_ALL_PICKUPS_FOR_LOCAL_PLAYER(TRUE)
		
		//VFX
		PLAY_JUGGERNAUT_TRANSFORM_EFFECTS()
	ENDIF
	RETURN TRUE
ENDFUNC

PROC CLEAN_UP_BEAST_MODE(BOOL bFinalCleanup = FALSE, BOOL bDoHealthCleanup = TRUE, BOOL bResetClothing = FALSE)
	//Reset flags
	IF IS_BIT_SET(iLocalBoolCheck14, LBOOL14_IN_BEAST_MODE)
		CLEAR_BIT(iLocalBoolCheck14, LBOOL14_IN_BEAST_MODE)
	ENDIF
	IF IS_BIT_SET(iLocalBoolCheck14, LBOOL14_BEAST_STEALTH_TOGGLE)
		CLEAR_BIT(iLocalBoolCheck14, LBOOL14_BEAST_STEALTH_TOGGLE)
	ENDIF
	IF IS_BIT_SET(iLocalBoolCheck14, LBOOL14_BEAST_IS_STEALTHED)
		CLEAR_BIT(iLocalBoolCheck14, LBOOL14_BEAST_IS_STEALTHED)
	ENDIF	
	IF IS_BIT_SET(iLocalBoolCheck14, LBOOL14_IN_BEAST_COSTUME)
		CLEAR_BIT(iLocalBoolCheck14, LBOOL14_IN_BEAST_COSTUME)
	ENDIF
	IF IS_BIT_SET(iLocalBoolCheck14, LBOOL14_OTHER_TEAM_IN_BEAST_MODE)
		CLEAR_BIT(iLocalBoolCheck14, LBOOL14_OTHER_TEAM_IN_BEAST_MODE)
	ENDIF	
	IF IS_BIT_SET(iLocalBoolCheck28, LBOOL28_TPR_APPEARANCE_UPDATED)
		CLEAR_BIT(iLocalBoolCheck28, LBOOL28_TPR_APPEARANCE_UPDATED)
	ENDIF
	bFMMCHealthBoosted = FALSE
	MC_playerBD[iLocalPart].iBeastAlpha = 255
	IF NOT IS_BIT_SET(mc_playerBD[iLocalPart].iClientBitSet3,PBBOOL3_SPAWN_PROTECTION_ON)
	AND NOT IS_PLAYER_IN_TEMP_PASSIVE_MODE(PLAYER_ID())
		SET_ENTITY_ALPHA(LocalPlayerPed, MC_playerBD[iLocalPart].iBeastAlpha , FALSE)
	ENDIF
	SET_PLAYER_HEALTH_RECHARGE_MULTIPLIER(localPlayer, 1.0)
	SET_PLAYER_HEALTH_RECHARGE_MAX_PERCENT(LocalPlayer, 0.5)
	SET_PLAYER_MELEE_WEAPON_DAMAGE_MODIFIER(LocalPlayer, 1.0)					
	PRINTLN("[JS] [BEASTMODE] Cleaning up melee modifier")
	SET_PED_SUFFERS_CRITICAL_HITS(LocalPlayerPed, TRUE)
	SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_IgnoreMeleeFistWeaponDamageMult, FALSE)
	SET_PLAYER_MAX_EXPLOSIVE_DAMAGE(LocalPlayer, -1)
	SET_DISABLE_HIGH_FALL_DEATH(LocalPlayerPed, FALSE)
	SET_PLAYER_FALL_DISTANCE_TO_TRIGGER_RAGDOLL_OVERRIDE(LocalPlayer, -1.0)
	
	IF bDoHealthCleanup
		PRINTLN("[JS] [BEASTMODE] Cleaning up health")
		IF iMaxHealthBeforebigfoot > 0
			SET_PED_MAX_HEALTH(LocalPlayerPed, iMaxHealthBeforebigfoot)
			IF NOT IS_PED_INJURED(LocalPlayerPed)
			AND IS_NET_PLAYER_OK(LocalPlayer)
				PRINTLN("[JS] [BEASTMODE] Setting health back to normal (", iMaxHealthBeforebigfoot,")")
				SET_ENTITY_HEALTH(LocalPlayerPed, iMaxHealthBeforebigfoot)
			ENDIF
			iMaxHealthBeforebigfoot = 0
		ENDIF
	ENDIF
	
	IF bFinalCleanup
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciENABLE_POWER_MAD_SOUNDS)
			PRINTLN("[JS] [BEASTMODE] Doing final cleanup")
			RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_APARTMENT/APT_BEAST")
			IF IS_AUDIO_SCENE_ACTIVE("GTAO_BvS_Gameplay_Scene")
				STOP_AUDIO_SCENE("GTAO_BvS_Gameplay_Scene")
			ENDIF
			SET_AMBIENT_ZONE_STATE("AZ_COUNTRY_SAWMILL",TRUE,TRUE)
			SET_AMBIENT_ZONE_STATE("AZ_SAWMILL_CONVEYOR_01",TRUE,TRUE)
			SET_STATIC_EMITTER_ENABLED("SE_COUNTRY_SAWMILL_MAIN_BUILDING", TRUE)
		ELSE
			//[LM][Reminder] CLEAN UP new Beast Scene for Power Mad.
			IF IS_AUDIO_SCENE_ACTIVE("DLC_GR_PM_Beast_Gameplay_Scene")
				PRINTLN("[LM] START_AUDIO_SCENE(\"DLC_GR_PM_Beast_Gameplay_Scene\") Stopping Scene.")
				STOP_AUDIO_SCENE("DLC_GR_PM_Beast_Gameplay_Scene")
			ENDIF
		ENDIF
	ENDIF
	
	IF iBeastSprintSoundID > -1
		IF NOT HAS_SOUND_FINISHED(iBeastSprintSoundID)
			STOP_SOUND(iBeastSprintSoundID)
			SCRIPT_RELEASE_SOUND_ID(iBeastSprintSoundID)
		ENDIF
	ENDIF
	
	IF bResetClothing OR IS_BIT_SET(iLocalBoolCheck17, LBOOL17_CLEANUP_BEAST_MODE_ON_RESPAWN)
		IF IS_CORONA_READY_TO_START_WITH_JOB()
		OR IS_FAKE_MULTIPLAYER_MODE_SET()
			MP_OUTFITS_APPLY_DATA   sApplyData
			sApplyData.pedID		= LocalPlayerPed
			sApplyData.eApplyStage 	= AOS_SET
			sApplyData.eOutfit		= INT_TO_ENUM(MP_OUTFIT_ENUM, GET_LOCAL_DEFAULT_OUTFIT(MC_playerBD[iPartToUse].iTeam))
			//sApplyData.eMask		= INT_TO_ENUM(MP_OUTFIT_MASK_ENUM, GET_LOCAL_DEFAULT_MASK(MC_playerBD[iPartToUse].iTeam))
			IF SET_PED_MP_OUTFIT(sApplyData, FALSE, FALSE, FALSE, FALSE)
				PRINTLN("[JS][BEASTMODE] Outfit not reset")
				SET_BIT(iLocalBoolCheck17, LBOOL17_CLEANUP_BEAST_MODE_ON_RESPAWN)
			ELSE
				PRINTLN("[JS][BEASTMODE] Reset player outfit")
				CLEAR_BIT(iLocalBoolCheck17, LBOOL17_CLEANUP_BEAST_MODE_ON_RESPAWN)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

FUNC STRING GET_BEAST_MODE_SOUNDSET()
	STRING sSoundSet = "APT_BvS_Soundset"

	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciENABLE_POWER_MAD_SOUNDS)
		sSoundSet = "DLC_GR_PM_Beast_Soundset"
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_ENABLE_TRADING_PLACES_SOUNDS)
		sSoundSet = "DLC_BTL_TP_Remix_Beast_Soundset"
	ENDIF
	
	PRINTLN("[RCC MISSION] GET_BEAST_MODE_SOUNDSET - Returning soundset: ", sSoundSet)
	RETURN sSoundSet
ENDFUNC

PROC PLAY_BEAST_TRANSFORM_EFFECTS(BOOL bTransformBack = FALSE)
	IF NOT g_bMissionOver
		IF bTransformBack
			SHAKE_GAMEPLAY_CAM("SMALL_EXPLOSION_SHAKE")
			SET_PARTICLE_FX_NON_LOOPED_COLOUR(GET_BEAST_PARTICLE_COLOUR(0, TRUE), GET_BEAST_PARTICLE_COLOUR(1, TRUE), GET_BEAST_PARTICLE_COLOUR(2, TRUE))
			USE_PARTICLE_FX_ASSET("scr_powerplay")
			START_NETWORKED_PARTICLE_FX_NON_LOOPED_ON_PED_BONE("scr_powerplay_beast_vanish", localPlayerPed, <<0, 0, -0.3>>, <<0, 0, 0>>, BONETAG_PELVIS)
			STRING sSoundSet = GET_BEAST_MODE_SOUNDSET()
			PLAY_SOUND_FRONTEND(-1, "Beast_Attack", sSoundSet)
		ELSE
			SHAKE_GAMEPLAY_CAM("SMALL_EXPLOSION_SHAKE")
			SET_PARTICLE_FX_NON_LOOPED_COLOUR(GET_BEAST_PARTICLE_COLOUR(0, TRUE), GET_BEAST_PARTICLE_COLOUR(1, TRUE), GET_BEAST_PARTICLE_COLOUR(2, TRUE))
			USE_PARTICLE_FX_ASSET("scr_powerplay")
			START_NETWORKED_PARTICLE_FX_NON_LOOPED_ON_PED_BONE("scr_powerplay_beast_appear", localPlayerPed, <<0, 0, -0.3>>, <<0, 0, 0>>, BONETAG_PELVIS, 0.75)
			STRING sSoundSet = GET_BEAST_MODE_SOUNDSET()
			PLAY_SOUND_FROM_ENTITY(-1, "Beast_Attack",LocalPlayerPed, sSoundSet, TRUE, 250)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL TRANSFORM_LOCAL_PLAYER_INTO_BEAST(BOOL bTransformBack = FALSE)
	IF bTransformBack
		IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_TRANSFORMED_INTO_BEAST)
			PRINTLN("[JS] TRANSFORM_LOCAL_PLAYER_INTO_BEAST - We are not a beast")
			RETURN FALSE
		ENDIF
		
		REINIT_NET_TIMER(tdTransformDelayTimer)
		
		CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_TRANSFORMED_INTO_BEAST)
		PRINTLN("[JS] - TRANSFORM_LOCAL_PLAYER_INTO_BEAST - NO LONGER THE BEAST")

		//Weapons
		WEAPON_TYPE wtCurrentWeap
		GET_CURRENT_PED_WEAPON(LocalPlayerPed, wtCurrentWeap)
		IF wtCurrentWeap != GET_BEST_PED_WEAPON(LocalPlayerPed)
			SET_CURRENT_PED_WEAPON(LocalPlayerPed, GET_BEST_PED_WEAPON(LocalPlayerPed), TRUE)
		ENDIF
		BLOCK_ALL_PICKUPS_FOR_LOCAL_PLAYER(FALSE)

		//VFX
		IF bLocalPlayerPedOK
			PLAY_BEAST_TRANSFORM_EFFECTS(bTransformBack)
		ENDIF
		
		//Ped Settings
		MC_playerBD[iLocalPart].iBeastAlpha = 255
		IF bLocalPlayerPedOK
			CLEAN_UP_BEAST_MODE(DEFAULT, TRUE, TRUE)
		ELSE
			SET_BIT(iLocalBoolCheck23, LBOOL23_CLEANUP_OUTFIT_ON_RESPAWN)
			CLEAN_UP_BEAST_MODE(DEFAULT, FALSE, TRUE)
		ENDIF
		SET_PLAYER_WEAPON_TAKEDOWN_DEFENSE_MODIFIER(LocalPlayer, 1.0)
	ELSE
		IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_TRANSFORMED_INTO_BEAST)
		OR NOT bLocalPlayerPedOk
		OR IS_PLAYER_RESPAWNING(LocalPlayer)
		OR g_bMissionOver
			PRINTLN("[JS] TRANSFORM_LOCAL_PLAYER_INTO_BEAST - We are already a beast or dead")
			RETURN FALSE
		ENDIF
		
		IF HAS_NET_TIMER_STARTED(tdTransformDelayTimer)
			IF NOT HAS_NET_TIMER_EXPIRED(tdTransformDelayTimer, ciTRANSFORM_DELAY_TIME)
				PRINTLN("[JS] TRANSFORM_LOCAL_PLAYER_INTO_BEAST - Trying to transform too quickly after another transform")
				RETURN FALSE
			ENDIF
		ENDIF
		
		SET_BIT(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_TRANSFORMED_INTO_BEAST)
		PRINTLN("[JS] - TRANSFORM_LOCAL_PLAYER_INTO_BEAST - IM THE BEAST")
		
		//VFX
		PLAY_BEAST_TRANSFORM_EFFECTS(bTransformBack)
		
		//Ped Settings
		bFMMCHealthBoosted = FALSE
		SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_BlockAutoSwapOnWeaponPickups, TRUE)
		BLOCK_ALL_PICKUPS_FOR_LOCAL_PLAYER(TRUE)
		
		SET_PLAYER_WEAPON_TAKEDOWN_DEFENSE_MODIFIER(LocalPlayer, 0.2)
		
		SET_BIT(AssortedBoolBitset, HasBeastHelpFinished)
		INT iTeam = MC_PlayerBD[iLocalPart].iTeam
		INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]			
		IF iRule < FMMC_MAX_RULES
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFive[iRule], ciBS_RULE5_BEASTMODE_ENABLE_SUPER_JUMP)
			AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFive[iRule], ciBS_RULE5_BEASTMODE_ENABLE_STEALTH)
				PRINT_HELP("BEAST_HELP_06")
			ELIF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFive[iRule], ciBS_RULE5_BEASTMODE_ENABLE_SUPER_JUMP)
				PRINT_HELP("BEAST_HELP_06c")
			ELIF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetFive[iRule], ciBS_RULE5_BEASTMODE_ENABLE_STEALTH)
				PRINT_HELP("BEAST_HELP_06b")
			ENDIF
		ENDIF
		
	ENDIF
	RETURN TRUE
ENDFUNC

PROC PLAY_VEHICLE_UPGRADE_EFFECTS(VEHICLE_INDEX veh, BOOL bTransformBack = FALSE)
	IF bTransformBack
		SHAKE_GAMEPLAY_CAM("SMALL_EXPLOSION_SHAKE")
		SET_PARTICLE_FX_NON_LOOPED_COLOUR(GET_BEAST_PARTICLE_COLOUR(0), GET_BEAST_PARTICLE_COLOUR(1), GET_BEAST_PARTICLE_COLOUR(2))
		USE_PARTICLE_FX_ASSET("scr_powerplay")
		START_NETWORKED_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_powerplay_beast_vanish", veh, <<0,0,0>>, <<0,0,0>>, 4.0)
		
		STRING sSoundSet = GET_BEAST_MODE_SOUNDSET()
		
		PLAY_SOUND_FROM_ENTITY(-1, "Beast_Attack",LocalPlayerPed, sSoundSet, TRUE, 250)
	ELSE
		SHAKE_GAMEPLAY_CAM("SMALL_EXPLOSION_SHAKE")
		SET_PARTICLE_FX_NON_LOOPED_COLOUR(GET_BEAST_PARTICLE_COLOUR(0), GET_BEAST_PARTICLE_COLOUR(1), GET_BEAST_PARTICLE_COLOUR(2))
		USE_PARTICLE_FX_ASSET("scr_powerplay")
		START_NETWORKED_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_powerplay_beast_appear", veh, <<0,0,0>>, <<0,0,0>>, 2.5)
		
		STRING sSoundSet = GET_BEAST_MODE_SOUNDSET()
		
		PLAY_SOUND_FROM_ENTITY(-1, "Beast_Attack",LocalPlayerPed, sSoundSet, TRUE, 250)
	ENDIF
ENDPROC

PROC APPLY_VEHICLE_UPGRADE_MODS(VEHICLE_INDEX veh, BOOL bTransformBack = FALSE)
	IF bTransformBack
		FMMC_CLEAR_ALL_VEHICLE_MODS(veh)
	ELSE
		IF DOES_ENTITY_EXIST(veh) AND NOT IS_ENTITY_DEAD(veh) AND GET_NUM_MOD_KITS(veh) > 0
			//Apply Mod Presets
			SET_VEHICLE_MOD_KIT(veh, 0)
			FMMC_CLEAR_ALL_VEHICLE_MODS(veh)
			MODEL_NAMES model = GET_ENTITY_MODEL(veh)
			IF model = TAMPA3
				FMMC_SET_VEHICLE_MOD_CHECK(veh, MOD_ROOF, 0)
				FMMC_SET_VEHICLE_MOD_CHECK(veh, MOD_BUMPER_F, 0)
				FMMC_SET_VEHICLE_MOD_CHECK(veh, MOD_BUMPER_R, 1)
				FMMC_SET_VEHICLE_MOD_CHECK(veh, MOD_CHASSIS, 2)
				FMMC_SET_VEHICLE_MOD_CHECK(veh, MOD_BONNET, 2)
				FMMC_SET_VEHICLE_MOD_CHECK(veh, MOD_WHEELS, 15)
				RELEASE_PRELOAD_MODS(veh)
			ELIF DOES_VEHICLE_HAVE_ANY_WEAPON_MODS(model)
				//this is just the same as the tampa to give them something to play with.
				//other presets can be included above.
				FMMC_SET_VEHICLE_MOD_CHECK(veh, MOD_ROOF, 0)
				FMMC_SET_VEHICLE_MOD_CHECK(veh, MOD_BUMPER_F, 0)
				FMMC_SET_VEHICLE_MOD_CHECK(veh, MOD_BUMPER_R, 1)
				FMMC_SET_VEHICLE_MOD_CHECK(veh, MOD_CHASSIS, 2)
				FMMC_SET_VEHICLE_MOD_CHECK(veh, MOD_BONNET, 2)
				FMMC_SET_VEHICLE_MOD_CHECK(veh, MOD_WHEELS, 15)
				RELEASE_PRELOAD_MODS(veh)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC UPGRADE_WEAPONISED_VEHICLE(BOOL bTransformBack = FALSE)
	
	VEHICLE_INDEX veh
	
	IF bTransformBack
		IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_UPGRADED_WEP_VEHICLE)
			PRINTLN("UPGRADE_WEAPONISED_VEHICLE - PBBOOL3_UPGRADED_WEP_VEHICLE is not set")
			EXIT
		ENDIF
		
		CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_UPGRADED_WEP_VEHICLE)
		PRINTLN("UPGRADE_WEAPONISED_VEHICLE - NO LONGER IN AN UPGRADED VEHICLE")
		
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			veh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
			
			//VFX
			PLAY_VEHICLE_UPGRADE_EFFECTS(veh, bTransformBack)
			
			//Apply Mods
			APPLY_VEHICLE_UPGRADE_MODS(veh, bTransformBack)
		ENDIF
	ELSE
		IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_UPGRADED_WEP_VEHICLE)
		OR NOT bLocalPlayerPedOk
			PRINTLN("UPGRADE_WEAPONISED_VEHICLE - PBBOOL3_UPGRADED_WEP_VEHICLE is already set so we're already a super vehicle or dead")
			EXIT
		ENDIF
		
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			veh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
			
			SET_BIT(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_UPGRADED_WEP_VEHICLE)
			PRINTLN("UPGRADE_WEAPONISED_VEHICLE - IM A SUPER VEHICLE")
			
			//VFX
			PLAY_VEHICLE_UPGRADE_EFFECTS(veh, bTransformBack)
			
			//Apply Mods
			APPLY_VEHICLE_UPGRADE_MODS(veh, bTransformBack)
		ENDIF
	ENDIF
ENDPROC

PROC CLEANUP_OBJECT_INVENTORIES()
	
	iInventoryObjectIndex = -1
	IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_HAS_OBJECT_INVENTORY)
		PRINTLN("CLEANUP_OBJECT_INVENTORIES - Clearing the player's Inventory as they are no longer holding Obj!")
		REMOVE_ALL_PLAYERS_WEAPONS()
		GIVE_PLAYER_STARTING_MISSION_WEAPONS(WEAPONINHAND_LASTWEAPON_BOTH, TRUE, DEFAULT, DEFAULT, DEFAULT, IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciWEAPONS_FORCE_NO_ADDONS_START_MISSION_INVENTORY))
		CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_HAS_OBJECT_INVENTORY)
	ENDIF
	
ENDPROC

PROC CLEANUP_OBJECT_TRANSFORMATIONS()
	IF NOT g_bMissionOver
		IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_TRANSFORMED_INTO_JUGGERNAUT)
			TRANSFORM_LOCAL_PLAYER_INTO_JUGGERNAUT(TRUE)
		ENDIF
		
		IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_TRANSFORMED_INTO_BEAST)
			TRANSFORM_LOCAL_PLAYER_INTO_BEAST(TRUE)
		ENDIF
		
		IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_UPGRADED_WEP_VEHICLE)
			UPGRADE_WEAPONISED_VEHICLE(TRUE)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_THIS_CURRENTLY_OVERTIME_TURNS()
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_OVERTIME_TURNS(g_FMMC_STRUCT.iAdversaryModeType)
	OR (IS_THIS_ROCKSTAR_MISSION_NEW_VS_OVERTIME_RUMBLE(g_FMMC_STRUCT.iAdversaryModeType)
	AND IS_BIT_SET(MC_serverBD.iServerBitSet5, SBBOOL5_SUDDEN_DEATH_OVERTIME_RUMBLE)
	AND IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH))
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_THIS_CURRENTLY_OVERTIME_RUMBLE()
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_OVERTIME_RUMBLE(g_FMMC_STRUCT.iAdversaryModeType)
	AND ((IS_BIT_SET(MC_serverBD.iServerBitSet5, SBBOOL5_SUDDEN_DEATH_OVERTIME_RUMBLE)
	AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH))
	OR NOT IS_BIT_SET(MC_serverBD.iServerBitSet5, SBBOOL5_SUDDEN_DEATH_OVERTIME_RUMBLE))
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_THIS_TEAM_HAVE_HIGHEST_GRANULAR_CAPTURE_PROGRESS(INT iTeam)
	INT i
	INT iCurrentHighestScore = -1
	INT iCurrentHighestProgress = -1
	BOOL bEqual = FALSE

	FOR i = 0 TO (FMMC_MAX_TEAMS - 1)
		IF MC_serverBD.iTeamScore[i] > iCurrentHighestScore
			iCurrentHighestScore = MC_serverBD.iTeamScore[i]
		ENDIF
		IF MC_serverBD.iGranularCurrentPoints[i] > iCurrentHighestProgress
			iCurrentHighestProgress = MC_serverBD.iGranularCurrentPoints[i]
			bEqual = FALSE
		ELIF MC_serverBD.iGranularCurrentPoints[i] = iCurrentHighestProgress
		AND iCurrentHighestProgress != -1
			bEqual = TRUE
		ENDIF
	ENDFOR
	
	IF bEqual
		PRINTLN("[RCC MISSION] DOES_THIS_TEAM_HAVE_HIGHEST_CAPTURE_PROGRESS - multiple teams drawing on highest")
		RETURN FALSE
	ENDIF
	
	PRINTLN("[RCC MISSION] DOES_THIS_TEAM_HAVE_HIGHEST_CAPTURE_PROGRESS - iCurrentHighestScore: ", iCurrentHighestScore, " iCurrentHighestProgress: ", iCurrentHighestProgress)
	
	FOR i = 0 TO (FMMC_MAX_TEAMS - 1)
		IF MC_serverBD.iTeamScore[iteam] = iCurrentHighestScore
			IF MC_serverBD.iGranularCurrentPoints[i] = iCurrentHighestProgress
				PRINTLN("[RCC MISSION] DOES_THIS_TEAM_HAVE_HIGHEST_CAPTURE_PROGRESS - Team ", iTeam, " has the highest progress")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDFOR
	PRINTLN("[RCC MISSION] DOES_THIS_TEAM_HAVE_HIGHEST_CAPTURE_PROGRESS - Returning false for team", iTeam)
	RETURN FALSE
ENDFUNC

PROC PROCESS_CLIENT_LOCATE_TEAM_SWAP()
	INT iTeam = MC_PlayerBD[iLocalPart].iTeam
	INt iPriority = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	IF iPriority < FMMC_MAX_RULES
		BROADCAST_FMMC_OBJECTIVE_PLAYER_SWITCHED_TEAMS(iTeam, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iLocateSwapTeam[iPriority], TRUE)
		CHANGE_PLAYER_TEAM(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iLocateSwapTeam[iPriority], TRUE)
		SET_BIT(iLocalBoolCheck23, LBOOL23_LOCATE_TEAM_SWAP_INITIALISED)
		PRINTLN("[JS][LOCSWAP] - PROCESS_CLIENT_LOCATE_TEAM_SWAP - Swap Complete (client)")
		CLEAR_BIT(iLocalBoolCheck23, LBOOL23_LOCATE_TEAM_SWAP_REQUESTED)
	ENDIF
ENDPROC

FUNC BOOL CAN_ROUND_NUMBER_UPDATE_IN_OVERTIME()

	IF DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(localPlayer)
	AND GET_TOGGLE_PAUSED_RENDERPHASES_STATUS() // we are not paused
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(MC_serverBD.iServerBitset3, SBBOOL3_TEAM_0_SHOULD_RESPAWN_BACK_AT_START_NOW + MC_PlayerBD[iPartToUse].iTeam)
	OR IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + MC_PlayerBD[iPartToUse].iTeam)
	OR IS_BIT_SET(iLocalBoolCheck23, LBOOL23_OVERTIME_MODE_SWITCH_OBJ_BLOCK)
	OR IS_BIT_SET(iLocalBoolCheck22, LBOOL22_NEW_OT_ROUND_THIS_FRAME)		
	OR IS_BIT_SET(iLocalBoolCheck23, LBOOL23_OVERTIME_SCORE_SHARD_ON_SCREEN)
	OR NOT IS_BIT_SET(iLocalBoolCheck23, LBOOL23_OVERTIME_READY_TO_GHOST)
	OR NOT GET_TOGGLE_PAUSED_RENDERPHASES_STATUS()
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC DISABLE_RUINER_WEAPONS_TO_SHOW_COVER_PANEL()
	VEHICLE_INDEX vehPlayer = GET_VEHICLE_PED_IS_IN(localPlayerPed)
	
	SET_VEHICLE_WEAPON_RESTRICTED_AMMO(vehPlayer, 0, 0)
	SET_VEHICLE_WEAPON_RESTRICTED_AMMO(vehPlayer, 1, 0)
	
	SET_CURRENT_PED_VEHICLE_WEAPON(LocalPlayerPed, WEAPONTYPE_UNARMED)
	SET_CURRENT_PED_WEAPON(LocalPlayerPed, WEAPONTYPE_UNARMED, TRUE)
	
	DISABLE_VEHICLE_WEAPON(TRUE,WEAPONTYPE_DLC_VEHICLE_RUINER_ROCKET, vehPlayer, LocalPlayerPed)
	DISABLE_VEHICLE_WEAPON(TRUE,WEAPONTYPE_DLC_VEHICLE_RUINER_BULLET, vehPlayer, LocalPlayerPed)			
	
	SET_PED_CAN_SWITCH_WEAPON(LocalPlayerPed, FALSE)
	
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)	
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_NEXT_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_PREV_WEAPON)
	
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
	
	PRINTLN("[LM][DISABLE_RUINER_WEAPONS_TO_SHOW_COVER_PANEL] - This is called to disable the Ruiner Weapons and Expose the Panel to cover them. Mainly required to be called before Game State Running so that this happens during the intro cam.")
ENDPROC

PROC SET_GLOBAL_AIRSTRIKE_COORDS(VECTOR vTargetCoords)
	PRINTLN("[RCC MISSION][AIRSTRIKE] - SET_GLOBAL_AIRSTRIKE_COORDS Setting g_vAirstrikeCoords to ", vTargetCoords)
	g_vAirstrikeCoords = vTargetCoords
ENDPROC

FUNC ENTITY_INDEX GET_AIRSTRIKE_TARGET_ENTITY(INT iEntityType, INT iEntityID)
	IF iEntityType = ciAIRSTRIKE_TARGET_ENTITY_TYPE_PED
		IF iEntityID > -1
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niPed[iEntityID])
				RETURN CONVERT_TO_ENTITY_INDEX(NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iEntityID]))
			ENDIF
		ENDIF
	ELIF iEntityType = ciAIRSTRIKE_TARGET_ENTITY_TYPE_VEHICLE
		IF iEntityID > -1
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iEntityID])
				RETURN CONVERT_TO_ENTITY_INDEX(NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iEntityID]))
			ENDIF
		ENDIF
	ELIF iEntityType = ciAIRSTRIKE_TARGET_ENTITY_TYPE_OBJECT
		IF iEntityID > -1
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niObject[iEntityID])
				RETURN CONVERT_TO_ENTITY_INDEX(NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[iEntityID]))
			ENDIF
		ENDIF
	ENDIF
	RETURN NULL
ENDFUNC

PROC GET_AIRSTRIKE_TARGET_COORDS(VECTOR &vTargetCoords, INT iTeam, INT iRule)

	INT iEntityType = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sAirstrikeStruct[iRule].iEntityType
	INT iEntityID = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sAirstrikeStruct[iRule].iEntityID

	SWITCH iEntityType
		CASE ciAIRSTRIKE_TARGET_ENTITY_TYPE_PED
			IF iEntityID > -1
			AND DOES_ENTITY_EXIST(GET_AIRSTRIKE_TARGET_ENTITY(iEntityType, iEntityID))
				vTargetCoords = GET_ENTITY_COORDS(GET_AIRSTRIKE_TARGET_ENTITY(iEntityType, iEntityID), FALSE)
				PRINTLN("[RCC MISSION][AIRSTRIKE] GET_AIRSTRIKE_TARGET_COORDS - PED - setting vTargetCoords to: ", vTargetCoords)
			ELSE
				PRINTLN("[RCC MISSION][AIRSTRIKE] No Entity ID for Ped")
			ENDIF
		BREAK
		CASE ciAIRSTRIKE_TARGET_ENTITY_TYPE_VEHICLE
			IF iEntityID > -1
			AND DOES_ENTITY_EXIST(GET_AIRSTRIKE_TARGET_ENTITY(iEntityType, iEntityID))
				vTargetCoords = GET_ENTITY_COORDS(GET_AIRSTRIKE_TARGET_ENTITY(iEntityType, iEntityID), FALSE)
				PRINTLN("[RCC MISSION][AIRSTRIKE] GET_AIRSTRIKE_TARGET_COORDS  - VEHICLE - setting vTargetCoords to: ", vTargetCoords)
			ELSE
				PRINTLN("[RCC MISSION][AIRSTRIKE] No Entity ID for Vehicle")
			ENDIF
		BREAK
		CASE ciAIRSTRIKE_TARGET_ENTITY_TYPE_OBJECT
			IF iEntityID > -1
			AND DOES_ENTITY_EXIST(GET_AIRSTRIKE_TARGET_ENTITY(iEntityType, iEntityID))
				vTargetCoords = GET_ENTITY_COORDS(GET_AIRSTRIKE_TARGET_ENTITY(iEntityType, iEntityID), FALSE)
				PRINTLN("[RCC MISSION][AIRSTRIKE] GET_AIRSTRIKE_TARGET_COORDS - OBJECT - setting vTargetCoords to: ", vTargetCoords)
			ELSE
				PRINTLN("[RCC MISSION][AIRSTRIKE] No Entity ID for Object")
			ENDIF
		BREAK
		CASE ciAIRSTRIKE_TARGET_ENTITY_TYPE_LOCATION
			IF iEntityID > -1
			AND DOES_ENTITY_EXIST(GET_AIRSTRIKE_TARGET_ENTITY(iEntityType, iEntityID))
				vTargetCoords = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iEntityID].vLoc[0]
				PRINTLN("[RCC MISSION][AIRSTRIKE] GET_AIRSTRIKE_TARGET_COORDS - LOCATION - setting vTargetCoords to: ", vTargetCoords)
			ELSE
				PRINTLN("[RCC MISSION][AIRSTRIKE] No Entity ID for Location")
			ENDIF
		BREAK
		DEFAULT
			vTargetCoords = <<0,0,0>>
			PRINTLN("[RCC MISSION][AIRSTRIKE] GET_AIRSTRIKE_TARGET_COORDS - DEFAULT - setting vTargetCoords to: ", vTargetCoords, " Mission Rule ", iRule, " probably doesn't have entity type set")
		BREAK
	ENDSWITCH
ENDPROC

PROC PROCESS_RESET_DUNE_REAR_MINES()
	IF NOT IS_THIS_ROCKSTAR_MISSION_WVM_DUNE3(g_FMMC_STRUCT.iRootContentIDHash)
		EXIT
	ENDIF

	IF NOT IS_PED_INJURED(localPlayerPed)
		IF NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			IF NOT g_bDisableVehicleMines
				PRINTLN("[LM][PROCESS_RESET_DUNE_REAR_MINES][MINES] - g_bDisableVehicleMines is set, but we are NOT in a vehicle. Setting to TRUE to block the mines.")
				DISABLE_VEHICLE_MINES(TRUE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC VECTOR GET_RANDOM_OFFSET_ON_RING(FLOAT xMin, FLOAT xMax, FLOAT yMin, FLOAT yMax)
	
	// Random Direction
	RETURN << GET_RANDOM_FLOAT_IN_RANGE(xMin,xMax) , GET_RANDOM_FLOAT_IN_RANGE(yMin,yMax), 0 >>
		
ENDFUNC

FUNC VECTOR GET_RANDOM_DIRECTION_FOR_AIRSTRIKE(FLOAT xMin, FLOAT xMax, FLOAT yMin, FLOAT yMax)
	FLOAT fXValue = GET_RANDOM_FLOAT_IN_RANGE(xMin,xMax)
	FLOAT fYValue = GET_RANDOM_FLOAT_IN_RANGE(yMin, yMax)
	VECTOR vRandomDir = <<fXValue, fYValue, 0>>
	PRINTLN("[RCC MISSION] GET_RANDOM_DIRECTION_FOR_AIRSTRIKE: ", vRandomDir)
	RETURN vRandomDir
ENDFUNC

FUNC VECTOR GET_AIRSTRIKE_OFFSET_COORDS(FLOAT fMinRadius, FLOAT fMaxRadius, VECTOR vDirection)
	IF fMaxRadius < fMinRadius
		fMaxRadius = fMinRadius
	ENDIF
	
	VECTOR vDir = GET_VECTOR_OF_LENGTH(vDirection, GET_RANDOM_FLOAT_IN_RANGE(fMinRadius, fMaxRadius))
	
	RETURN vDir
	
ENDFUNC

FUNC FLOAT GET_RANDOM_ROTATION_FOR_AIRSTRIKE(FLOAT fLow, FLOAT fHigh)
	IF GET_RANDOM_BOOL() //Positive
		RETURN GET_RANDOM_FLOAT_IN_RANGE(fLow, fHigh)
	ELSE //Negative
		RETURN GET_RANDOM_FLOAT_IN_RANGE(-fHigh, -fLow)
	ENDIF
ENDFUNC

PROC UPDATE_AIRSTRIKE_OFFSET_VECTOR(INT iEntityType, INT iEntityID, INT iTeam, INT iRule)
	IF DOES_ENTITY_EXIST(GET_AIRSTRIKE_TARGET_ENTITY(iEntityType, iEntityID))
		VECTOR vTempForward = GET_ENTITY_FORWARD_VECTOR(GET_AIRSTRIKE_TARGET_ENTITY(iEntityType, iEntityID))
		
		ROTATE_VECTOR_FMMC(vTempForward, <<0,0,GET_RANDOM_ROTATION_FOR_AIRSTRIKE(TO_FLOAT(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sAirstrikeStruct[iRule].iOffsetLowAngle),
																					TO_FLOAT(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sAirstrikeStruct[iRule].iOffsetHighAngle))>>)
		vAirstrikeOffset = GET_AIRSTRIKE_OFFSET_COORDS(TO_FLOAT(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sAirstrikeStruct[iRule].iOffsetDistanceMin),
														TO_FLOAT(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sAirstrikeStruct[iRule].iOffsetDistanceMax), vTempForward)
	ELSE
		PRINTLN("[RCC MISSION][UPDATE_AIRSTRIKE_OFFSET_VECTOR] iEntityID does not exist!")
	ENDIF
ENDPROC

FUNC TinyRacersRoundState TINY_RACERS_GET_ROUND_STATE(INT iPart)
	RETURN MC_playerBD[iPart].ePlayerTinyRacersRoundState
ENDFUNC

FUNC BOOL SHOULD_OVERTIME_HUD_ELEMENTS_SHOW(BOOL bShouldHideHud)
	INT iTeam = MC_playerBD[iLocalPart].iteam
			
	IF bShouldHideHud
		PRINTLN("[LM][OVERTIME] - HUD HIDDEN - bShouldHideHud Set Reason: (0)")
	ENDIF
	
	INT iTeamOT
	FOR iTeamOT = 0 TO FMMC_MAX_TEAMS-1
		IF IS_BIT_SET(MC_serverBD.iServerBitset3, SBBOOL3_TEAM_0_SHOULD_RESPAWN_BACK_AT_START_NOW + iTeam)
			bShouldHideHud = TRUE
			PRINTLN("[LM][OVERTIME] - HUD HIDDEN - bShouldHideHud Set Reason: (1)") // Respawning back at start.
		ENDIF
	ENDFOR
	IF HAS_NET_TIMER_STARTED(tdOvertimeScoredShardTimer)
		IF NOT HAS_NET_TIMER_EXPIRED_READ_ONLY(tdOvertimeScoredShardTimer, ci_OVERTIME_SCORED_SHARD_TIME)
			bShouldHideHud = TRUE
			PRINTLN("[LM][OVERTIME] - HUD HIDDEN - bShouldHideHud Set Reason: (2)") // LBOOL23_OVERTIME_SCORE_SHARD_ON_SCREEN Score shard on screen. (timer)
		ENDIF		
	ENDIF
	IF IS_BIT_SET(iLocalBoolCheck23, LBOOL23_OVERTIME_SCORE_SHARD_ON_SCREEN)
		bShouldHideHud = TRUE
		PRINTLN("[LM][OVERTIME] - HUD HIDDEN - bShouldHideHud Set Reason: (3)") // LBOOL23_OVERTIME_SCORE_SHARD_ON_SCREEN Score shard on screen.
	ENDIF
	IF IS_PLAYER_ON_PAUSE_MENU(LocalPlayer)
	OR IS_BIT_SET(iLocalBoolCheck23, LBOOL23_OVERTIME_MODE_SWITCH_OBJ_BLOCK)
		bShouldHideHud = TRUE
		PRINTLN("[LM][OVERTIME] - HUD HIDDEN - bShouldHideHud Set Reason: (4)") // we are transitioning to Spectator.")
	ENDIF
	IF iOTPartToMove > -1
	AND iOTPartToMove != iLocalPart
	AND (NOT IS_LOCAL_PLAYER_ANY_SPECTATOR() OR (IS_LOCAL_PLAYER_ANY_SPECTATOR() AND GET_SPEC_CAM_STATE(g_BossSpecData.specCamData) != eSPECCAMSTATE_WATCHING))
		bShouldHideHud = TRUE
		PRINTLN("[LM][OVERTIME] - HUD HIDDEN - bShouldHideHud Set Reason: (5)")	// we have not yet transitioned to spectator. The transition clears shards so we must prevent it from playing.
	ENDIF
	
	RETURN bShouldHideHud
ENDFUNC

FUNC BOOL SHOULD_HIDE_TIMER_FOR_OVERTIME()
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_OVERTIME_ANY(g_FMMC_STRUCT.iAdversaryModeType)
		IF IS_BIT_SET(iLocalBoolCheck22, LBOOL22_OVERTIME_DELAYED_SCORE_SHARD)
			PRINTLN("[LM][OVERTIME] - HUD HIDDEN - Timer not showing (1)")
			RETURN TRUE
		ENDIF
		IF IS_BIT_SET(iLocalBoolCheck23, LBOOL23_OVERTIME_SCORE_SHARD_ON_SCREEN)
			PRINTLN("[LM][OVERTIME] - HUD HIDDEN - Timer not showing (2)")
			RETURN TRUE
		ENDIF
		IF MC_playerBD[iLocalPart].iClientLogicStage = CLIENT_MISSION_STAGE_PROTECT_OBJ
			PRINTLN("[LM][OVERTIME] - HUD HIDDEN - Timer not showing (3)")
			RETURN TRUE
		ENDIF
		IF IS_THIS_CURRENTLY_OVERTIME_TURNS()
		AND HAS_NET_TIMER_STARTED(tdOvertimeScoredShardTimer)
			PRINTLN("[LM][OVERTIME] - HUD HIDDEN - Timer not showing (4)")
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC


PROC RESTART_LOGIC_TURN_PHONE_RADIO_ON()
	IF NOT IS_STRING_NULL_OR_EMPTY(sCurrentRadioStation)
	AND NOT ARE_STRINGS_EQUAL(sCurrentRadioStation, "OFF")
	AND NOT IS_MOBILE_PHONE_RADIO_ACTIVE()
		SET_MOBILE_PHONE_RADIO_STATE(TRUE)
		SET_AUDIO_FLAG("MobileRadioInGame", TRUE)
		SET_RADIO_TO_STATION_NAME(sCurrentRadioStation)
		g_SpawnData.iRadioStation = GET_PLAYER_RADIO_STATION_INDEX()
		PRINTLN("[TMS] Phone radio should now be on")
	ENDIF
ENDPROC

PROC RESTART_LOGIC_TURN_PHONE_RADIO_OFF()
	IF NOT IS_STRING_NULL_OR_EMPTY(sCurrentRadioStation)
	AND IS_MOBILE_PHONE_RADIO_ACTIVE()
		SET_MOBILE_PHONE_RADIO_STATE(FALSE)
		SET_AUDIO_FLAG("MobileRadioInGame", FALSE)
		SET_RADIO_TO_STATION_NAME("OFF")		
		g_SpawnData.iRadioStation = 255
		PRINTLN("[TMS] Phone radio should now be off")
	ENDIF
ENDPROC

PROC SET_ALLOW_INTERIOR_SCRIPT_FOR_MISSION(BOOL bActive)
	g_SimpleInteriorData.bAllowInteriorScriptForActivitySession = bActive
	PRINTLN("[RCC MISSION] SET_ALLOW_INTERIOR_SCRIPT_FOR_MISSION - Setting g_SimpleInteriorData.bAllowInteriorScriptForActivitySession to ", BOOL_TO_STRING(bActive))
ENDPROC

FUNC BOOL SHOULD_CLEANUP_OBJECT_TRANSFORMATION_NOW()
	BOOL bReturn = TRUE
	
	//Don't transform bacj ever during sudden death
	IF (IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH)
	AND (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciTRANSFORM_TO_JUGGERNAUT_ON_SUDDEN_DEATH)
	OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciTRANSFORM_TO_BEAST_ON_SUDDEN_DEATH)))
		bReturn = FALSE
	ENDIF
	
	//Wait for renderphase to pause before transforming back
	INT iTeam = MC_PlayerBD[iLocalPart].iTeam
	INT iRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]
	IF iRule < FMMC_MAX_RULES
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEight[iRule],ciBS_RULE8_PAUSE_ON_PLAYER_DURING_RESTART)
			IF (NOT (IS_BIT_SET(MC_serverBD.iServerBitset3, SBBOOL3_TEAM_0_SHOULD_RESPAWN_BACK_AT_START_NOW + iTeam)
			AND IS_BIT_SET(MC_PlayerBD[iLocalPart].iClientBitSet3, PBBOOL3_RESTART_RENDERPHASE_PAUSED)))
			AND bLocalPlayerPedOk
				bReturn = FALSE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN bReturn
ENDFUNC

PROC GIVE_LOCAL_PLAYER_FINAL_RP_REWARD_BONUS()
	IF NOT IS_BIT_SET(iLocalBoolCheck24, LBOOL24_FINAL_RP_REWARD_GIVEN)
		INT iMinutesPlayed = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdMissionLengthTimer) / 60000
		INT iFinalRP = g_sMPTunables.iGR_ADVERSARY_FINAL_OBJECTIVE_BONUS * iMinutesPlayed
		//Set the bonus XP here
		iBonusXP = iFinalRP
		PRINTLN("[JS] GIVE_LOCAL_PLAYER_FINAL_RP_REWARD_BONUS - Minutes Played = ", iMinutesPlayed, " Multiplier = ", g_sMPTunables.iGR_ADVERSARY_FINAL_OBJECTIVE_BONUS, " Final RP = ", iFinalRP)
		GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD, LocalPlayerPed,"XPT_MEDIUMT", XPTYPE_COMPLETE, XPCATEGORY_COMPLETE_MISSION_PASS, iFinalRP)
	ENDIF
ENDPROC

FUNC BOOL DOES_VEHICLE_HAVE_DELIVERED_DECOR_FOR_TEAM(INT iTeam, INT iVeh, INT iRule)
	IF iTeam = -1
	OR iTeam >= FMMC_MAX_TEAMS
		RETURN FALSE
	ENDIF
	STRING strDecorName
	SWITCH iTeam
		CASE 0	strDecorName = "MC_Team0_VehDeliveredRules"		BREAK
		CASE 1	strDecorName = "MC_Team1_VehDeliveredRules"		BREAK
		CASE 2	strDecorName = "MC_Team2_VehDeliveredRules"		BREAK
		CASE 3	strDecorName = "MC_Team3_VehDeliveredRules"		BREAK
	ENDSWITCH
	IF DECOR_IS_REGISTERED_AS_TYPE(strDecorName, DECOR_TYPE_INT)
		VEHICLE_INDEX vehID = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
		IF DECOR_EXIST_ON(vehID, strDecorName)
			IF IS_BIT_SET(DECOR_GET_INT(vehID, strDecorName), iRule)
				PRINTLN("[RCC MISSION]DOES_VEHICLE_HAVE_DELIVERED_DECOR_FOR_TEAM Veh ", iVeh, " has been delivered.")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL ARE_INTS_IN_RANGE(INT iOne, INT iTwo, INT iRange)
	RETURN iOne <= iTwo + iRange
	AND iOne >= iTwo - iRange
ENDFUNC

FUNC BOOL IS_ANY_PLAYER_IN_AVENGER_PILOT_SEAT(INT &iPilotPart)
	INT i, iPlayersToCheck, iPlayersChecked

	iPlayersToCheck = GET_NUMBER_OF_PLAYING_PLAYERS()
	FOR i = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		IF (NOT(IS_BIT_SET(MC_playerBD[i].iClientBitSet, PBBOOL_ANY_SPECTATOR)) OR IS_BIT_SET(MC_playerBD[i].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN))
		AND NOT IS_BIT_SET(MC_playerBD[i].iClientBitSet,PBBOOL_PLAYER_FAIL)
		AND IS_BIT_SET(MC_playerBD[i].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
		AND NOT IS_BIT_SET(MC_playerBD[i].iClientBitSet, PBBOOL_FINISHED)
			PARTICIPANT_INDEX tempPart = INT_TO_PARTICIPANTINDEX(i)
			IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
				iPlayersChecked++
				IF MC_playerBD[i].iVehNear != -1
					PLAYER_INDEX tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
					PED_INDEX tempPed = GET_PLAYER_PED(tempPlayer)
					IF IS_PED_IN_ANY_VEHICLE(tempPed)
						VEHICLE_INDEX tempVeh = GET_VEHICLE_PED_IS_IN(tempPed)
						IF DOES_ENTITY_EXIST(tempVeh)
							IF GET_ENTITY_MODEL(tempVeh) = AVENGER
								iPilotPart = i
								RETURN TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		IF iPlayersChecked >= iPlayersToCheck
			BREAKLOOP
		ENDIF
	ENDFOR
	iPilotPart = -1
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ANY_PLAYER_IN_VEHICLE_INTERIOR()
	INT i
	
	FOR i = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		IF (NOT(IS_BIT_SET(MC_playerBD[i].iClientBitSet, PBBOOL_ANY_SPECTATOR)) OR IS_BIT_SET(MC_playerBD[i].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN))
		AND NOT IS_BIT_SET(MC_playerBD[i].iClientBitSet,PBBOOL_PLAYER_FAIL)
		AND IS_BIT_SET(MC_playerBD[i].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
		AND NOT IS_BIT_SET(MC_playerBD[i].iClientBitSet, PBBOOL_FINISHED)
			PARTICIPANT_INDEX tempPart = INT_TO_PARTICIPANTINDEX(i)
			IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
				PLAYER_INDEX tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
				IF IS_PLAYER_IN_CREATOR_TRAILER(tempPlayer)
				OR IS_PLAYER_IN_CREATOR_AIRCRAFT(tempPlayer)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN FALSE

ENDFUNC

FUNC BOOL ARE_REMAINING_PLAYERS_IN_VEHICLE_INTERIOR(BOOL bCheckAvenger = FALSE, INT iPlayersToCheckOverride = -1, INT iTeamBS = 15)

	INT i, iPlayersToCheck, iPlayersChecked, iPlayersInMOC
	IF iPlayersToCheckOverride > -1
		iPlayersToCheck = iPlayersToCheckOverride
	ELSE
		iPlayersToCheck = GET_NUMBER_OF_PLAYING_PLAYERS_ON_TEAMS(iTeamBS)
	ENDIF
	IF iPlayersToCheck > 1
		FOR i = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
			IF MC_playerBD[i].iTeam > -1
			AND IS_BIT_SET(iTeamBS, MC_playerBD[i].iTeam)
				IF (NOT(IS_BIT_SET(MC_playerBD[i].iClientBitSet, PBBOOL_ANY_SPECTATOR)) OR IS_BIT_SET(MC_playerBD[i].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN))
				AND NOT IS_BIT_SET(MC_playerBD[i].iClientBitSet,PBBOOL_PLAYER_FAIL)
				AND IS_BIT_SET(MC_playerBD[i].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
				AND NOT IS_BIT_SET(MC_playerBD[i].iClientBitSet, PBBOOL_FINISHED)
					PARTICIPANT_INDEX tempPart = INT_TO_PARTICIPANTINDEX(i)
					IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
						iPlayersChecked++
						PLAYER_INDEX tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
						IF IS_PLAYER_IN_CREATOR_TRAILER(tempPlayer)
						OR (bCheckAvenger AND (IS_PLAYER_IN_CREATOR_AIRCRAFT(tempPlayer) AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(tempPlayer)))
							iPlayersInMOC++
						ELSE
							PED_INDEX tempPed = GET_PLAYER_PED(tempPlayer)
							IF IS_PED_IN_ANY_VEHICLE(tempPed)
								VEHICLE_INDEX tempVeh = GET_VEHICLE_PED_IS_IN(tempPed)
								IF DOES_ENTITY_EXIST(tempVeh)
									IF NOT bCheckAvenger
										VEHICLE_INDEX tempTrailer = NULL
										IF GET_VEHICLE_TRAILER_VEHICLE(tempVeh, tempTrailer)
											IF GET_ENTITY_MODEL(tempTrailer) = trailerlarge
												iPlayersInMOC++
											ENDIF
										ENDIF
									ELSE
										IF GET_ENTITY_MODEL(tempVeh) = AVENGER
											iPlayersInMOC++
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						IF iPlayersChecked >= iPlayersToCheck
							BREAKLOOP
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
		IF iPlayersInMOC >= (iPlayersToCheck)
			PRINTLN("[RCC MISSION] ARE_REMAINING_PLAYERS_IN_VEHICLE_INTERIOR - Returning TRUE")
			RETURN TRUE
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_VEHICLE_USING_RESPAWN_POOL(INT iVeh)
	RETURN g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehicleRespawnLives = LEGACY__NUMBER_OF_RESPAWNS_VEHICLE_POOL
ENDFUNC

FUNC INT GET_VEHICLE_RESPAWNS(INT iVeh)
	IF IS_VEHICLE_USING_RESPAWN_POOL(iVeh)
		RETURN MC_serverBD.iVehicleRespawnPool
	ELSE
		RETURN MC_serverBD_2.iCurrentVehRespawnLives[iVeh]
	ENDIF
ENDFUNC

PROC DECREMENT_VEHICLE_RESPAWNS(INT iVeh)
	IF IS_VEHICLE_USING_RESPAWN_POOL(iVeh)
		MC_serverBD.iVehicleRespawnPool--
		PRINTLN("[JS][VEHPOOL] - DECREMENT_VEHICLE_RESPAWNS - Decrementing vehicle pool respawns to ", MC_serverBD.iVehicleRespawnPool, " due to vehicle: ", iVeh)
	ELSE
		MC_serverBD_2.iCurrentVehRespawnLives[iVeh]--
		PRINTLN("[JS][VEHPOOL] - DECREMENT_VEHICLE_RESPAWNS - Decrementing vehicle ", iVeh, "'s respawns to ", MC_serverBD_2.iCurrentVehRespawnLives[iVeh])
	ENDIF
ENDPROC

PROC SET_VEHICLE_RESPAWNS(INT iVeh, INT iValue)
	IF IS_VEHICLE_USING_RESPAWN_POOL(iVeh)
		MC_serverBD.iVehicleRespawnPool = iValue
		PRINTLN("[JS][VEHPOOL] - SET_VEHICLE_RESPAWNS - Setting vehicle pool respawns to ", MC_serverBD.iVehicleRespawnPool, " due to vehicle: ", iVeh)
	ELSE
		MC_serverBD_2.iCurrentVehRespawnLives[iVeh] = iValue
		PRINTLN("[JS][VEHPOOL] - SET_VEHICLE_RESPAWNS - Setting vehicle ", iVeh, "'s respawns to ", MC_serverBD_2.iCurrentVehRespawnLives[iVeh])
	ENDIF
ENDPROC

FUNC BOOL SHOULD_TEAM_RESPAWN_BACK_AT_START(INT iTeam)
	
	IF MC_serverBD_4.iCurrentHighestPriority[iTeam] >= FMMC_MAX_RULES
		RETURN FALSE
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSix[MC_serverBD_4.iCurrentHighestPriority[iTeam]], ciBS_RULE6_RESPAWN_ON_RULE_END)
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam)
	AND IS_BIT_SET(iLocalBoolCheck16, LBOOL16_TEAM_0_IS_PAST_THEIR_FIRST_RULE + iTeam)
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(MC_serverBD.iServerBitSet5, SBBOOL5_RETRY_CURRENT_RULE_TEAM_0 + iTeam)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL ARE_ALL_RELEVANT_PLAYERS_PLAYING_NORMALLY()

	INT iParticipant
	PARTICIPANT_INDEX piTemp
	PLAYER_INDEX piPlayer

	REPEAT GET_MAXIMUM_PLAYERS_ON_MISSION() iParticipant
		piTemp = INT_TO_PARTICIPANTINDEX(iParticipant)
		
		IF NETWORK_IS_PARTICIPANT_ACTIVE(piTemp)
			piPlayer = NETWORK_GET_PLAYER_INDEX(piTemp)
			
			IF IS_NET_PLAYER_OK(piPlayer, FALSE)
			AND NOT IS_PLAYER_SCTV(piPlayer)
			AND NOT DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(piPlayer)
			AND GET_PLAYER_TEAM(piPlayer) != -1
			AND GET_PLAYER_TEAM(piPlayer) < 8
				IF GET_PLAYER_RESPAWN_BACK_AT_START_STATE(iParticipant) != PRBSS_PlayingNormally
					PRINTLN("ARE_ALL_RELEVANT_PLAYERS_PLAYING_NORMALLY - waiting for part ", iparticipant, ", they are in state ", GET_PLAYER_RESPAWN_BACK_AT_START_STATE(iParticipant))
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT

	RETURN TRUE
	
ENDFUNC

FUNC BOOL IS_ENTITY_IN_ANY_INTERIOR(ENTITY_INDEX EntityIndex)
	IF DOES_ENTITY_EXIST(EntityIndex)
		IF GET_INTERIOR_FROM_ENTITY(EntityIndex) != NULL
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ENTITY_IN_SAME_INTERIOR_AS_LOCAL_PLAYER(ENTITY_INDEX EntityIndex)
	
	IF DOES_ENTITY_EXIST(EntityIndex) 
		IF GET_INTERIOR_FROM_ENTITY(LocalPlayerPed) = GET_INTERIOR_FROM_ENTITY(EntityIndex)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_THIS_RULE_A_CUTSCENE()
	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iPlayerRuleLoop
	
	FOR iPlayerRuleLoop = 0 TO MC_serverBD.iNumPlayerRuleCreated-1 
		IF MC_serverBD_4.iPlayerRulePriority[iPlayerRuleLoop][iteam] = MC_serverBD_4.iCurrentHighestPriority[iteam]
			IF MC_serverBD_4.iPlayerRule[ iPlayerRuleLoop ][ iteam ] = FMMC_OBJECTIVE_LOGIC_SCRIPTED_CUTSCENE
				RETURN TRUE
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_NEXT_RULE_A_CUTSCENE()
	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iPlayerRuleLoop
	
	FOR iPlayerRuleLoop = 0 TO MC_serverBD.iNumPlayerRuleCreated-1 
		IF MC_serverBD_4.iPlayerRulePriority[iPlayerRuleLoop][iteam] = MC_serverBD_4.iCurrentHighestPriority[iteam]+1
			IF MC_serverBD_4.iPlayerRule[ iPlayerRuleLoop ][ iteam ] = FMMC_OBJECTIVE_LOGIC_SCRIPTED_CUTSCENE
				RETURN TRUE
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_A_SCRIPTED_CUTSCENE_PLAYING()
	IF g_iFMMCScriptedCutscenePlaying < FMMC_GET_MAX_SCRIPTED_CUTSCENES()
		IF g_iFMMCScriptedCutscenePlaying != -1
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

//Incredbile flow play hack for bug 3574647 - please remove this once code fix the issue! (ADDED IN: 13752537)
PROC FREEZE_OPPRESSOR_FOR_DATA_BREACH()	
	IF IS_THIS_ROCKSTAR_MISSION_WVM_OPPRESSOR_1(g_FMMC_STRUCT.iRootContentIDHash)

		IF MC_serverBD.iNumVehCreated > 0
		AND MC_serverBD.iNumVehCreated <= FMMC_MAX_VEHICLES
			INT iVeh
			
			REPEAT MC_serverBD.iNumVehCreated iVeh
				
				IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])
					VEHICLE_INDEX vehIndex = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVeh])					
					IF NOT IS_PED_INJURED(localPlayerPed)
						IF DOES_ENTITY_EXIST(vehIndex)
							IF GET_ENTITY_MODEL(vehIndex) = OPPRESSOR						
								IF IS_ENTITY_ALIVE(vehIndex)
									IF IS_VEHICLE_DRIVEABLE(vehIndex)
										IF IS_ENTITY_IN_ANGLED_AREA(vehIndex, <<-1238.635254,-3318.097656,14.945050>>, <<-1269.123413,-3367.976807,22.945152>>, 12.000000)
											IF GET_VEHICLE_PED_IS_ENTERING(localPlayerPed) = vehIndex
												IF NETWORK_HAS_CONTROL_OF_ENTITY(vehIndex)
													PRINTLN("[3574647] (g_bsOppressorFreezeHack) UnFreezing OPPRESSOR ... iVeh = ", iVeh)												
												
													FREEZE_ENTITY_POSITION(vehIndex, FALSE)		
													
													CLEAR_BIT(g_bsOppressorFreezeHack, iVeh)
												ENDIF
											ELSE
												IF NOT IS_BIT_SET(g_bsOppressorFreezeHack, iVeh)
													IF IS_VEHICLE_STOPPED(vehIndex)														
														IF IS_VEHICLE_EMPTY(vehIndex)
															IF NETWORK_HAS_CONTROL_OF_ENTITY(vehIndex)
																PRINTLN("[3574647] (g_bsOppressorFreezeHack) Freezing OPPRESSOR ... iVeh = ", iVeh)
																
																SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehIndex, FALSE)
																
																FREEZE_ENTITY_POSITION(vehIndex, TRUE)
																
																SET_BIT(g_bsOppressorFreezeHack, iVeh)
															ENDIF
														ENDIF
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL ARE_ANY_PORTABLE_PICKUPS_CURRENTLY_HELD()
	INT iObj
	FOR iObj = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfObjects - 1)
		IF MC_serverBD.iObjCarrier[iObj] != -1
			RETURN TRUE
		ENDIF
	ENDFOR
	RETURN FALSE
ENDFUNC

PROC PROCESS_STOCKPILE_BAGS_AT_HOLDING_SCORE()
	IF NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
	AND NOT HAS_TEAM_FINISHED(MC_PlayerBD[iLocalPart].iteam)
		INT iObj, iNumberOfCapturedFlags
		FOR iObj = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfObjects - 1)
			IF MC_serverBD_1.iPTLDroppedPackageLastOwner[iobj] = iLocalPart
			AND IS_BIT_SET(MC_serverBD.iObjAtYourHolding[MC_PlayerBD[iLocalPart].iteam],iObj) 
				iNumberOfCapturedFlags++
			ENDIF
		ENDFOR
		IF iNumberOfCapturedFlags != MC_PlayerBD[iLocalPart].iPackagesAtHolding
			MC_PlayerBD[iLocalPart].iPackagesAtHolding = iNumberOfCapturedFlags
			PRINTLN("[RCC MISSION] PROCESS_STOCKPILE_BAGS_AT_HOLDING_SCORE - Updating MC_PlayerBD[iLocalPart].iPackagesAtHolding to ", MC_PlayerBD[iLocalPart].iPackagesAtHolding)
		ENDIF
	ENDIF
ENDPROC

FUNC INT GET_NUMBER_OF_OBJECTS_AT_TEAM_HOLDING(INT iTeam)
	INT iObj, iNumberOfObjects
	FOR iObj = 0 TO FMMC_MAX_NUM_OBJECTS - 1
		IF IS_BIT_SET(MC_serverBD.iObjAtYourHolding[iTeam], iObj)
			iNumberOfObjects++
		ENDIF
	ENDFOR
	
	PRINTLN("[BMBFB] GET_NUMBER_OF_OBJECTS_AT_TEAM_HOLDING returning ", iNumberOfObjects, " for team ", iTeam)
	RETURN iNumberOfObjects
ENDFUNC

FUNC BOOL WAS_PICKUP_AT_ANOTHER_TEAM_HOLDING(INT iObjectIndex, INT iTeamToCheckAgainst)
	INT iTeam
	FOR iTeam = 0 TO (FMMC_MAX_TEAMS - 1)
		IF iTeam != iTeamToCheckAgainst
			IF IS_BIT_SET(MC_serverBD.iObjCaptured, iObjectIndex)
			AND IS_BIT_SET(MC_serverBD.iObjAtYourHolding[iTeam], iObjectIndex)
				PRINTLN("[RCC MISSION] WAS_PICKUP_AT_ANOTHER_TEAM_HOLDING - Returning True, was at team ", iTeam,"'s holding")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDFOR
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PICKUP_AT_ANY_HOLDING(INT iObjectIndex)
	INT iTeam
	FOR iTeam = 0 TO (FMMC_MAX_TEAMS - 1)
		IF IS_BIT_SET(MC_serverBD.iObjAtYourHolding[iTeam], iObjectIndex)
			RETURN TRUE
		ENDIF
	ENDFOR
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PICKUP_AT_SPECIFIC_HOLDING(INT iObjectIndex, INT iTeam)
	RETURN IS_BIT_SET(MC_serverBD.iObjAtYourHolding[iTeam], iObjectIndex)
ENDFUNC

FUNC INT GET_TEAM_WITH_MOST_OBJECTS_AT_HOLDING()
	INT iWinningTeam, iTeam, iHighestHoldingScore, iNumTeamObjects, iNumTeamsOnSameScore
	iWinningTeam = -1
	FOR iTeam = 0 TO (FMMC_MAX_TEAMS - 1)
		IF IS_TEAM_ACTIVE(iTeam)
			iNumTeamObjects = GET_NUMBER_OF_OBJECTS_AT_TEAM_HOLDING(iTeam)
			IF iNumTeamObjects > iHighestHoldingScore
				iWinningTeam = iTeam
				iHighestHoldingScore = iNumTeamObjects
				PRINTLN("[MC] GET_TEAM_WITH_MOST_OBJECTS_AT_HOLDING - Updating iWinningTeam: ", iWinningTeam, " & iHighestHoldingScore: ", iHighestHoldingScore)
			ENDIF
		ENDIF
	ENDFOR
	
	FOR iTeam = 0 TO (FMMC_MAX_TEAMS - 1)
		IF IS_TEAM_ACTIVE(iTeam)
			IF GET_NUMBER_OF_OBJECTS_AT_TEAM_HOLDING(iTeam) = iHighestHoldingScore
				iNumTeamsOnSameScore++
				PRINTLN("[MC] GET_TEAM_WITH_MOST_OBJECTS_AT_HOLDING highest score is: ", iHighestHoldingScore,"iNumTeamsOnSameScore: ", iNumTeamsOnSameScore)
			ENDIF
		ENDIF
	ENDFOR
	
	IF iNumTeamsOnSameScore >= 2
		RETURN -1 //Returning -1 as it is a draw
	ENDIF
	
	RETURN iWinningTeam
ENDFUNC

PROC PROCESS_INCREMENT_HOLDING_PACKAGE_DRAWING_SCORES(INT iRule)
	INT iTeam, iHighestScore, iNumTeamObjects
	FOR iTeam = 0 TO (FMMC_MAX_TEAMS - 1)
		IF IS_TEAM_ACTIVE(iTeam)
			iNumTeamObjects = GET_NUMBER_OF_OBJECTS_AT_TEAM_HOLDING(iTeam)
			IF iNumTeamObjects > iHighestScore
				iHighestScore = iNumTeamObjects
			ENDIF
		ENDIF
	ENDFOR
	
	FOR iTeam = 0 TO (FMMC_MAX_TEAMS - 1)
		IF IS_TEAM_ACTIVE(iTeam)
			IF GET_NUMBER_OF_OBJECTS_AT_TEAM_HOLDING(iTeam)  = iHighestScore
				INCREMENT_SERVER_TEAM_SCORE(iTeam, iRule, GET_FMMC_POINTS_FOR_TEAM(iTeam, iRule))
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

FUNC BOOL IS_PED_IN_LOCATION_SIMPLE(INT iLoc, PED_INDEX tempPed)
	IF NOT IS_PED_INJURED(tempPed)
		IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].vloc1)
		AND NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].vloc2)
			IF IS_ENTITY_IN_ANGLED_AREA(tempPed,g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].vloc1,g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].vloc2,g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].fWidth)
				PRINTLN("[RCC MISSION] IS_PED_IN_LOCATION - RETURNING TRUE 1")
				RETURN TRUE
			ENDIF
		ELSE
			VECTOR vLoc = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].vloc[0]
			
			FLOAT fDist = POW(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iloc].fRadius[0], 2)
			
			IF VDIST2(GET_ENTITY_COORDS(tempPed),vLoc) < fDist
				PRINTLN("[RCC MISSION] IS_PED_IN_LOCATION_SIMPLE - RETURNING TRUE 2")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC PLAYER_INDEX GET_PLAYER_IN_CAPTURE_AREA(INT iLoc, BOOL bGetLone = TRUE)
	INT iTeam, iPlayer
	FOR iTeam = 0 TO (FMMC_MAX_TEAMS-1)
		IF (MC_serverBD_1.inumberOfTeamInArea[iLoc][iteam] = 1 OR NOT bGetLone)
		AND MC_serverBD.iLocOwner[iLoc] != iTeam
			FOR iPlayer = 0 TO (GET_MAXIMUM_PLAYERS_ON_MISSION()-1)
				IF IS_PED_IN_LOCATION_SIMPLE(iLoc, GET_PLAYER_PED(INT_TO_PLAYERINDEX(iPlayer)))
				AND NOT IS_PED_INJURED(GET_PLAYER_PED(INT_TO_PLAYERINDEX(iPlayer)))
					PRINTLN("[RCC MISSION] GET_PLAYER_IN_CAPTURE_AREA returning player: ", iplayer)
					RETURN INT_TO_PLAYERINDEX(iPlayer)
				ENDIF
			ENDFOR
		ENDIF
	ENDFOR
	
	RETURN INVALID_PLAYER_INDEX()
ENDFUNC

PROC CLEANUP_MULTI_VEHICLE_SEAT_SWAP(BOOL bDirtyCleanup = FALSE)
	PRINTLN("[LM][MULTI_VEH_RESPAWN][CLEANUP_MULTI_VEHICLE_SEAT_SWAP][SWAP_SEAT] - CLEANUP_MULTI_VEHICLE_SEAT_SWAP called")
	iCachedNewVehSeatPref = -3
	CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_INITIATED_VEHICLE_SEAT_SWAP)
	CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_DATA_CHANGE_READY_TO_MOVE_VEHICLE)	
	CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_DATA_CHANGING_SEAT_FINAL)
	CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_DATA_CHANGING_SEAT_DONE)
	CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_DATA_CHANGING_SEAT_GAVE_CONSENT)
	
	RESET_NET_TIMER(tdVehicleSeatSwapConsentTimer)
	RESET_NET_TIMER(tdVehicleSeatSwapSafetyTimer)
	
	// Fixes assert.
	IF NOT IS_PED_IN_ANY_VEHICLE(localPlayerPed)
		SET_ENTITY_COLLISION(localPlayerPed, TRUE)
	ENDIF
	
	SET_ENTITY_INVINCIBLE(localPlayerPed, FALSE)
	SET_ENTITY_VISIBLE(localPlayerPed, TRUE)
	
	RESET_NET_TIMER(tdVehicleSeatSwapCooldownTimer)
	START_NET_TIMER(tdVehicleSeatSwapCooldownTimer)
	
	CLEAR_HELP()
	
	g_bMissionSeatSwapping = FALSE
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_STOCKPILE(g_FMMC_STRUCT.iAdversaryModeType)
		SET_LOCAL_PLAYER_CAN_COLLECT_PORTABLE_PICKUPS(TRUE)
		SET_MAX_NUM_PORTABLE_PICKUPS_CARRIED_BY_PLAYER(DUMMY_MODEL_FOR_SCRIPT, g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iLocalPart].iteam].iMaxDeliveryEntitiesCanCarry[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam]])
	ENDIF
	
	// These values HAVE to default if we perform a dirty cleanup. This will sadly cause the vehicle swapping order to return to normal. May have to put this in an event to sync people up with the same values.
	IF bDirtyCleanup
		PRINTLN("[LM][MULTI_VEH_RESPAWN][CLEANUP_MULTI_VEHICLE_SEAT_SWAP][SWAP_SEAT] - PERFORMED DIRTY CLEANUP, RESETTING A BUNCH OF VALUES!")
		iCachedPartOnTeamVehicleRespawn = -1
	ENDIF
ENDPROC

FUNC BOOL GET_FREE_TURRET_VEHICLE_SEAT(VEHICLE_INDEX VehicleID, VEHICLE_SEAT &vs_ToAssign)
	INT i
	IF DOES_ENTITY_EXIST(VehicleID)
	AND NOT IS_ENTITY_DEAD(VehicleID)
		REPEAT GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(VehicleID) i
			IF IS_VEHICLE_SEAT_FREE(VehicleID, INT_TO_ENUM(VEHICLE_SEAT, i))
				IF IS_TURRET_SEAT(VehicleID, INT_TO_ENUM(VEHICLE_SEAT, i))
					vs_ToAssign = INT_TO_ENUM(VEHICLE_SEAT, i)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL SHUFFLE_MULTIPLE_PLAYER_SAME_VEHICLE_DATA_CYCLE_SEATS()

	INT iPartOnTeam = iCachedPartOnTeamVehicleRespawn
	INT i
		
	PRINTLN("[LM][MULTI_VEH_RESPAWN][SHUFFLE_MULTIPLE_PLAYER_SAME_VEHICLE_DATA_CYCLE_SEATS][SWAP_SEAT] - iPartOnTeam Pointer = ", iPartOnTeam)
		
	iPartOnTeam+=1
		
	PRINTLN("[LM][MULTI_VEH_RESPAWN][SHUFFLE_MULTIPLE_PLAYER_SAME_VEHICLE_DATA_CYCLE_SEATS][SWAP_SEAT] - iPartOnTeam Incremented to = ", iPartOnTeam)

	// loop until we come back round or find a valid selection.
	FOR i = 0 TO MAX_VEHICLE_PARTNERS-1
		IF iPartOnTeam >= MAX_VEHICLE_PARTNERS
			iPartOnTeam = 0
			
			PRINTLN("[LM][MULTI_VEH_RESPAWN][SHUFFLE_MULTIPLE_PLAYER_SAME_VEHICLE_DATA_CYCLE_SEATS][SWAP_SEAT] - (iPartOnTeam) Cylcing round to: ", iPartOnTeam, " because the index was greater than the MAX_VEHICLE_PARTNERS: ", MAX_VEHICLE_PARTNERS)
			
			BREAKLOOP
		ENDIF
		IF GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iVehiclePartners[iPartOnTeam] = -1
			iPartOnTeam+=1
		
			PRINTLN("[LM][MULTI_VEH_RESPAWN][SHUFFLE_MULTIPLE_PLAYER_SAME_VEHICLE_DATA_CYCLE_SEATS][SWAP_SEAT] - (Invalid Player) (iPartOnTeam) Incrementing to ", iPartOnTeam)
		ELSE 
			BREAKLOOP
		ENDIF
		IF iPartOnTeam >= MAX_VEHICLE_PARTNERS
			iPartOnTeam = 0
			
			PRINTLN("[LM][MULTI_VEH_RESPAWN][SHUFFLE_MULTIPLE_PLAYER_SAME_VEHICLE_DATA_CYCLE_SEATS][SWAP_SEAT] - (iPartOnTeam) Cylcing round to: ", iPartOnTeam, " because the index was greater than the MAX_VEHICLE_PARTNERS: ", MAX_VEHICLE_PARTNERS)
			
			BREAKLOOP
		ENDIF
	ENDFOR
	
	IF iPartOnTeam > -1 AND iPartOnTeam < MAX_VEHICLE_PARTNERS
		PRINTLN("[LM][MULTI_VEH_RESPAWN][SHUFFLE_MULTIPLE_PLAYER_SAME_VEHICLE_DATA_CYCLE_SEATS][SWAP_SEAT] - Old Seat INT: ", GlobalplayerBD[iLocalPart].iRespawnSeatPreference, " New Seat INT: ", GlobalplayerBD[GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iVehiclePartners[iPartOnTeam]].iRespawnSeatPreference)
				
		iCachedNewVehSeatPref = GlobalplayerBD[GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iVehiclePartners[iPartOnTeam]].iRespawnSeatPreference
		
		RETURN TRUE
	ELSE
		PRINTLN("[LM][MULTI_VEH_RESPAWN][SHUFFLE_MULTIPLE_PLAYER_SAME_VEHICLE_DATA_CYCLE_SEATS][SWAP_SEAT] - Cannot find passenger... emergency cleanup.")
		
		DO_SCREEN_FADE_IN(250)
		CLEANUP_MULTI_VEHICLE_SEAT_SWAP()
	ENDIF	
	
	RETURN FALSE	
ENDFUNC

FUNC BOOL IS_PART_IN_SAME_VEHICLE_AS_PART(INT iPart1, INT iPart2)
	
	IF iPart1 != -1
	AND iPart2 != -1
		PLAYER_INDEX player1 = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPart1))
		PLAYER_INDEX player2 = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPart2))
				
		IF IS_NET_PLAYER_OK(player1)
		AND IS_NET_PLAYER_OK(player2)
			PED_INDEX ped1 = GET_PLAYER_PED(player1)
			PED_INDEX ped2 = GET_PLAYER_PED(player2)
			
			IF NOT IS_PED_INJURED(ped1)
			AND NOT IS_PED_INJURED(ped2)
				VEHICLE_INDEX vehPlayer1
				IF IS_PED_IN_ANY_VEHICLE(ped1)
					vehPlayer1 = GET_VEHICLE_PED_IS_IN(ped1)
				ENDIF
				
				VEHICLE_INDEX vehPlayer2
				IF IS_PED_IN_ANY_VEHICLE(ped2)
					vehPlayer2 = GET_VEHICLE_PED_IS_IN(ped2)
				ENDIF
				
				IF vehPlayer1 != vehPlayer2
					RETURN FALSE
				ENDIF
			ELSE
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	// True because even if the above stuff fails, we don't want to execute what this function will do if it returns false.
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_PED_CARRYING_ANY_OBJECTS(PED_INDEX pedToCheck)
	INT iObj
	FOR iObj = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfObjects-1
		IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niObject[iObj])
			IF IS_ENTITY_ATTACHED_TO_ENTITY(NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[iObj]), pedToCheck)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDFOR
	RETURN FALSE
ENDFUNC

FUNC INT GET_OBJECT_INDEX_ATTACHED_TO_ENTITY(PED_INDEX pedToCheck)
	INT iObj
	FOR iObj = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfObjects-1
		IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niObject[iObj])
			IF IS_ENTITY_ATTACHED_TO_ENTITY(NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[iObj]), pedToCheck)
				RETURN iObj
			ENDIF
		ENDIF
	ENDFOR
	RETURN -1
ENDFUNC

// This function gets the driver number for the player and team passed in (team part)
FUNC INT SET_DRIVER_AND_VEHICLE_PARTNERS_FROM_MY_PART_AND_TEAM_CORONA_PREFS(INT iPlayerTeam, INT iPartNumberInTeam)
	INT iPlayer
	INT iPlayerStartFrom = 0
	INT iDriverNumberIBelongTo = 0
	INT iDrivers[6]
	INT iDriverPartTeamNumber = -1
	
	// Init
	INT i, ii, iii
	FOR i = 0 TO 5
		iDrivers[i] = -1
	ENDFOR
	
	PLAYER_INDEX tempPlayer1
	PLAYER_INDEX tempPlayer2
	
	BOOL bDoneFindingPartners = FALSE
	
	INT iVehiclePartner = 0
	
	// Loop
	FOR i = 0 TO NUM_NETWORK_PLAYERS-1
		IF NOT bDoneFindingPartners
			
			// Initialize our saved drivers.
			FOR iii = 0 TO MAX_VEHICLE_PARTNERS-1
				SET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(iii, -1)
			ENDFOR
			iVehiclePartner = 0
			
			tempPlayer1 = INT_TO_PLAYERINDEX(i)
		
			IF IS_NET_PLAYER_OK(tempPlayer1, FALSE)
			AND NOT IS_PLAYER_SPECTATING(tempPlayer1)
			
				IF GlobalplayerBD_FM[i].sClientCoronaData.iTeamChosen = iPlayerTeam
					iDriverPartTeamNumber++
				
					// Found a participant. Increment our number.
					IF GlobalplayerBD_FM[i].sClientCoronaData.iPreferredRole = ciMISSION_CLIENT_SEAT_OPTION_DRIVER
					AND NOT IS_DRIVER_IN_LIST(iDrivers, i)
						iDrivers[iDriverNumberIBelongTo] = i
						
						PRINTLN("[LM][MULTI_VEH_RESPAWN][GET_DRIVER_PART_NUMBER_IN_TEAM_FROM_MY_PART_IN_TEAM_CORONA_PREF] Found a Driver iPart: ", iDrivers[iDriverNumberIBelongTo])
					ENDIF
					
					// We have a Driver Participant number on our team now, check which players are going to be in there.
					IF iDrivers[iDriverNumberIBelongTo] > -1			
						
						// Start from the player index we left off at. This is so that we don't look at players who belong to other drivers.
						FOR ii = iPlayerStartFrom TO NUM_NETWORK_PLAYERS-1
							tempPlayer2 = INT_TO_PLAYERINDEX(ii)							
							
							IF IS_NET_PLAYER_OK(tempPlayer2, FALSE)
							AND NOT IS_PLAYER_SPECTATING(tempPlayer2)
								
								IF GlobalplayerBD_FM[ii].sClientCoronaData.iTeamChosen = iPlayerTeam
									SET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(iVehiclePartner, ii)
									iVehiclePartner++
									
									// If we find our participant number in the loop, then we know we can use iDriverPartTeamNumber as our driver. We don't break here because we want to add the rest of our vehicle partners.
									IF iPlayer = iPartNumberInTeam
										bDoneFindingPartners = TRUE
									ENDIF
									
									// Increment our Participant Number in Team var.
									iPlayer++
									
									// If we exceed the number of players allowed in a particular vehicle then breakloop and move onto the next driver.
									IF iPlayer >= (g_FMMC_STRUCT.iTeamVehicleRespawnPassengers[iPlayerTeam]*(iDriverNumberIBelongTo+1))
										iDriverNumberIBelongTo++
										iPlayerStartFrom++
										BREAKLOOP
									ENDIF
								ENDIF
							ENDIF
							iPlayerStartFrom++
						ENDFOR
					ENDIF
				ENDIF
			ENDIF
		ELSE
			BREAKLOOP
		ENDIF
	ENDFOR
	
	PRINTLN("[LM][MULTI_VEH_RESPAWN][GET_DRIVER_PART_NUMBER_IN_TEAM_FROM_MY_PART_IN_TEAM_CORONA_PREF] (Team Participant number) Returning iDriverPartTeamNumber: ", iDriverPartTeamNumber, 
																									" (is this the first, or second driver etc) iDriverNumberIBelongTo: ", (iDriverNumberIBelongTo+1), // starts at 0 because of array.
																									" (How many players we looped through) iPlayer: " , iPlayer, 
																									" (Our Participant Number) iPartNumberInTeam: " , iPartNumberInTeam)
	
	RETURN iDriverPartTeamNumber
ENDFUNC

FUNC BOOL IS_OK_TO_MULTI_TEAM_VEHICLE_SEAT_SWAP()
	INT i = 0 
	PARTICIPANT_INDEX driverPart
	
	IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		IF GET_NUM_PEDS_IN_VEHICLE(GET_VEHICLE_PED_IS_IN(LocalPlayerPed)) < 2
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_PHONE_ONSCREEN()
	OR IS_CELLPHONE_CAMERA_IN_USE()
		RETURN FALSE
	ENDIF
	
	IF IS_INTERACTION_MENU_OPEN()
		RETURN FALSE
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(MC_playerBD[iLocalPart].tdBoundstimer) 
	OR IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_OO_BOUNDS)
		PRINT_HELP("CONSET_NO_2", ci_VEH_SEAT_SWAP_INVALID_TIME)
		RETURN FALSE
	ENDIF
	
	IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(GET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER())))
		driverPart = INT_TO_PARTICIPANTINDEX(GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(GET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER()))
		
		IF driverPart != INVALID_PARTICIPANT_INDEX()
			IF IS_PED_CARRYING_ANY_OBJECTS(GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(driverPart)))
				PRINT_HELP("CONSET_NO_1", ci_VEH_SEAT_SWAP_INVALID_TIME)
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	FOR i = 0 TO MAX_VEHICLE_PARTNERS-1
		IF GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(i) > -1
		AND NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(i)))
			IF IS_PART_IN_SAME_VEHICLE_AS_PART(iLocalPart, GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(i))
				RETURN TRUE
			ENDIF
		ENDIF
	ENDFOR	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_MULTI_TEAM_VEHICLE_SEAT_SWAP_IN_PROGRESS()
		
	IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_DATA_CHANGING_SEAT_DONE)
	OR IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_DATA_CHANGE_READY_TO_MOVE_VEHICLE)
	OR IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_INITIATED_VEHICLE_SEAT_SWAP)
	OR IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_DATA_CHANGING_SEAT_FINAL)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_MULTI_TEAM_VEHICLE_SEAT_SWAP_PAST_INITIATED()
		
	IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_DATA_CHANGING_SEAT_DONE)
	OR IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_DATA_CHANGE_READY_TO_MOVE_VEHICLE)
	OR IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_DATA_CHANGING_SEAT_FINAL)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC CACHE_VEHICLE_SEATING_PREFERENCES_FOR_MULTI_VEH()
	
	PRINTLN("[LM][MULTI_VEH_RESPAWN] - [CACHE_VEHICLE_SEATING_PREFERENCES_FOR_MULTI_VEH][SWAP_SEAT] - Calling to re-cache our current vehicle seat.")
	
	IF NOT IS_PED_INJURED(localPlayerPed)
		IF IS_PED_IN_ANY_VEHICLE(localPlayerPed)
			
			VEHICLE_SEAT vehSeat = GET_SEAT_PED_IS_IN(localPlayerPed, TRUE)
			VEHICLE_SEAT vehSeatToCheck
			PRINTLN("[LM][MULTI_VEH_RESPAWN] - [CACHE_VEHICLE_SEATING_PREFERENCES_FOR_MULTI_VEH][SWAP_SEAT] - Current Vehicle Seat: ", GET_PLAYER_RESPAWN_VEHICLE_SEAT_PREFERENCE(), " changing to actual seat: ", ENUM_TO_INT(vehSeat))
				
			PARTICIPANT_INDEX piPart
			PLAYER_INDEX piPlayer
			PED_INDEX pedPlayer
			
			INT iDriver = -1
			INT i = 0			
			FOR i = 0 TO MAX_VEHICLE_PARTNERS-1
				IF GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iVehiclePartners[i] > -1
					PRINTLN("[LM][MULTI_VEH_RESPAWN] - [CACHE_VEHICLE_SEATING_PREFERENCES_FOR_MULTI_VEH][SWAP_SEAT] - Valid player: ", i)
					
					piPart = INT_TO_PARTICIPANTINDEX(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iVehiclePartners[i])
					piPlayer = NETWORK_GET_PLAYER_INDEX(piPart)
					pedPlayer = GET_PLAYER_PED(piPlayer)
					
					IF NOT IS_PED_INJURED(pedPlayer)
						vehSeatToCheck = GET_SEAT_PED_IS_IN(pedPlayer, TRUE)
						
						PRINTLN("[LM][MULTI_VEH_RESPAWN] - [CACHE_VEHICLE_SEATING_PREFERENCES_FOR_MULTI_VEH][SWAP_SEAT] - Player: ", GET_PLAYER_NAME(piPlayer), " is in seat: ", ENUM_TO_INT(vehSeatToCheck))
						
						IF vehSeatToCheck = VS_DRIVER
							PRINTLN("[LM][MULTI_VEH_RESPAWN] - [CACHE_VEHICLE_SEATING_PREFERENCES_FOR_MULTI_VEH][SWAP_SEAT] - Found our driver in Partner slot: ", i)
							iDriver = i
							BREAKLOOP
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
			
			IF iDriver > -1
				PRINTLN("[LM][MULTI_VEH_RESPAWN] - [CACHE_VEHICLE_SEATING_PREFERENCES_FOR_MULTI_VEH][SWAP_SEAT] - iDriver is in Partner Slot: ", iDriver, " and is true participant number: ", GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iVehiclePartners[iDriver])
			ENDIF
			
			IF vehSeat = VS_DRIVER
				PRINTLN("[LM][MULTI_VEH_RESPAWN] - [CACHE_VEHICLE_SEATING_PREFERENCES_FOR_MULTI_VEH][SWAP_SEAT] - We are now cached as a Driver")
				SET_RACE_PASSENGER_GLOBAL(FALSE)
				SET_RACE_DRIVER_GLOBAL(TRUE)
			ELSE
				PRINTLN("[LM][MULTI_VEH_RESPAWN] - [CACHE_VEHICLE_SEATING_PREFERENCES_FOR_MULTI_VEH][SWAP_SEAT] - We are now cached as a Passenger")
				SET_RACE_PASSENGER_GLOBAL(TRUE)
				SET_RACE_DRIVER_GLOBAL(FALSE)
			ENDIF
			
			IF iDriver > -1
				SET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER(iDriver)
			ELSE
				PRINTLN("[LM][MULTI_VEH_RESPAWN] - [CACHE_VEHICLE_SEATING_PREFERENCES_FOR_MULTI_VEH][SWAP_SEAT] - iDriver IS -1, something funky happen!")
			ENDIF
			
			SET_PLAYER_RESPAWN_VEHICLE_SEAT_PREFERENCE(ENUM_TO_INT(vehSeat))
		ELSE
			PRINTLN("[LM][MULTI_VEH_RESPAWN] - [CACHE_VEHICLE_SEATING_PREFERENCES_FOR_MULTI_VEH][SWAP_SEAT] - Cannot re-cache, we are not in a vehicle.")
		ENDIF
	ELSE
		PRINTLN("[LM][MULTI_VEH_RESPAWN] - [CACHE_VEHICLE_SEATING_PREFERENCES_FOR_MULTI_VEH][SWAP_SEAT] - Cannot re-cache, we are dead.")
	ENDIF
ENDPROC

PROC CACHE_AND_SETUP_VEHICLE_PARTNERS()

	PRINTLN("[LM][MULTI_VEH_RESPAWN] - [CACHE_AND_SETUP_VEHICLE_PARTNERS] - Calling to initialise the data.")
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[ iLocalPart ].iteam].iTeamBitSet3, ciBS3_ENABLE_STOP_PLAYER_FROM_SHUFFLING_SEATS)
		SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_PreventAutoShuffleToDriversSeat, TRUE)
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[ iLocalPart ].iteam].iTeamBitSet3, ciBS3_ENABLE_CHOOSE_SEAT_PREFERENCE_IN_CORONA)
		// Default (No corona settings): 
		INT iPlayerTeam = MC_playerBD[ iLocalPart ].iteam
		INT iPartNumberInTeam = GET_PARTICIPANT_NUMBER_IN_TEAM_PRIVATE()	
		INT iDriverPartTeam = GET_DRIVER_PART_NUMBER_IN_TEAM_FROM_MY_PART_IN_TEAM(iPlayerTeam, iPartNumberInTeam)
		INT iDriverPart = GET_PARTICIPANT_NUMBER_FROM_TEAM_PARTICIPANT_NUMBER(iPlayerTeam, iDriverPartTeam)
				
		PRINTLN("[LM][MULTI_VEH_RESPAWN] - [CACHE_AND_SETUP_VEHICLE_PARTNERS] - iDriver:  ", iDriverPart)
		
		// The driver may need to be mindful of all his passengers. We will know how many he is supposed to have based on a creator setting.
		IF iDriverPart = iLocalPart
			PRINTLN("[LM][MULTI_VEH_RESPAWN] - [CACHE_AND_SETUP_VEHICLE_PARTNERS] - (Driver) I am a Driver. Adding Driver Part: ", iDriverPart, " iDriverPartTeam: " ,iDriverPartTeam)
			
			SET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER(0)
			
			INT i = 0
			FOR i = 0 TO MAX_VEHICLE_PARTNERS-1
				IF i+iDriverPartTeam < NUM_NETWORK_PLAYERS
					IF i < g_FMMC_STRUCT.iTeamVehicleRespawnPassengers[iPlayerTeam]
						SET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(i, GET_PASSENGER_PARTICIPANT_NUMBER_FROM_MY_PARTICIPANT_NUMBER(i+iDriverPartTeam))
						PRINTLN("[LM][MULTI_VEH_RESPAWN] - [CACHE_AND_SETUP_VEHICLE_PARTNERS] - (Driver) Adding :  ", GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(i), " to my Vehicle Partners List.")
					ELSE
						SET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(i, -1)
						PRINTLN("[LM][MULTI_VEH_RESPAWN] - [CACHE_AND_SETUP_VEHICLE_PARTNERS] - (Driver) Adding :  -1 (Forced (1)) - No more passengers")
					ENDIF
				ELSE
					SET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(i, -1)
					PRINTLN("[LM][MULTI_VEH_RESPAWN] - [CACHE_AND_SETUP_VEHICLE_PARTNERS] - (Driver) Adding :  -1 (Forced (2))")
				ENDIF
				
				IF GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(i) = iLocalPart
					iCachedPartOnTeamVehicleRespawn = i
					PRINTLN("[LM][MULTI_VEH_RESPAWN] - [CACHE_AND_SETUP_VEHICLE_PARTNERS] - iCachedPartOnTeamVehicleRespawn:  ", iCachedPartOnTeamVehicleRespawn)
				ENDIF
			ENDFOR
			
			SET_PLAYER_RESPAWN_VEHICLE_SEAT_PREFERENCE(ENUM_TO_INT(VS_DRIVER))
			SET_RACE_DRIVER_GLOBAL(TRUE)
		ELSE
			PRINTLN("[LM][MULTI_VEH_RESPAWN] - [CACHE_AND_SETUP_VEHICLE_PARTNERS] - (Passenger) I am a passenger. Adding Driver Part: ", iDriverPart, " iDriverPartTeam: " ,iDriverPartTeam)
			
			SET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER(0)

			INT i = 0
			FOR i = 0 TO MAX_VEHICLE_PARTNERS-1
				IF i+iDriverPartTeam < NUM_NETWORK_PLAYERS
					IF i < g_FMMC_STRUCT.iTeamVehicleRespawnPassengers[iPlayerTeam]
						SET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(i, GET_PASSENGER_PARTICIPANT_NUMBER_FROM_MY_PARTICIPANT_NUMBER(i+iDriverPartTeam))
						PRINTLN("[LM][MULTI_VEH_RESPAWN] - [CACHE_AND_SETUP_VEHICLE_PARTNERS] - (Passenger) Adding :  ", GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(i), " to my Vehicle Partners List.")
					ELSE
						SET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(i, -1)
						PRINTLN("[LM][MULTI_VEH_RESPAWN] - [CACHE_AND_SETUP_VEHICLE_PARTNERS] - (Passenger) Adding :  -1 (Forced (1)) - No more passengers")
					ENDIF
				ELSE
					SET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(i, -1)
					PRINTLN("[LM][MULTI_VEH_RESPAWN] - [CACHE_AND_SETUP_VEHICLE_PARTNERS] - (Passenger) Adding :  -1 (Forced (2))")
				ENDIF
				
				IF GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(i) = NATIVE_TO_INT(PLAYER_ID())
					iCachedPartOnTeamVehicleRespawn = i
					PRINTLN("[LM][MULTI_VEH_RESPAWN] - [CACHE_AND_SETUP_VEHICLE_PARTNERS] - iCachedPartOnTeamVehicleRespawn:  ", iCachedPartOnTeamVehicleRespawn)
				ENDIF
			ENDFOR
			
			SET_PLAYER_RESPAWN_VEHICLE_SEAT_PREFERENCE(ENUM_TO_INT(VS_ANY_PASSENGER))
			
			SET_RACE_PASSENGER_GLOBAL(TRUE)
		ENDIF
	ELSE
		// Default (No corona settings): 
		INT iPlayerTeam = MC_playerBD[ iLocalPart ].iteam
		INT iPartNumberInTeam = GET_PARTICIPANT_NUMBER_IN_TEAM_PRIVATE()
		
		// Populating the Vehicle Partners is done in this function here;
		SET_DRIVER_AND_VEHICLE_PARTNERS_FROM_MY_PART_AND_TEAM_CORONA_PREFS(iPlayerTeam, iPartNumberInTeam)
		
		INT i = 0		
		FOR i = 0 TO MAX_VEHICLE_PARTNERS-1
			IF GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(i) > -1
				IF GlobalplayerBD_FM[GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(i)].sClientCoronaData.iPreferredRole = ciMISSION_CLIENT_SEAT_OPTION_DRIVER
					SET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER(i)
				ENDIF
			ENDIF
			
			IF GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(i) = NATIVE_TO_INT(PLAYER_ID())
				iCachedPartOnTeamVehicleRespawn = i
			ENDIF
		ENDFOR
				
		IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iPreferredRole = ciMISSION_CLIENT_SEAT_OPTION_DRIVER
			SET_RACE_DRIVER_GLOBAL(TRUE)
			SET_PLAYER_RESPAWN_VEHICLE_SEAT_PREFERENCE(ENUM_TO_INT(VS_DRIVER))
			
			PRINTLN("[LM][MULTI_VEH_RESPAWN] - [CACHE_AND_SETUP_VEHICLE_PARTNERS] - (Corona) iPreferredRole: ciMISSION_CLIENT_SEAT_OPTION_DRIVER")
		ELSE
			IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iPreferredRole = ciMISSION_CLIENT_SEAT_OPTION_PASSENGER_TURRET
				SET_RACE_PASSENGER_GLOBAL(TRUE)								
				SET_PLAYER_RESPAWN_VEHICLE_SEAT_PREFERENCE(ENUM_TO_INT(VS_ANY_PASSENGER))
				
				PRINTLN("[LM][MULTI_VEH_RESPAWN] - [CACHE_AND_SETUP_VEHICLE_PARTNERS] - (Corona) iPreferredRole: ciMISSION_CLIENT_SEAT_OPTION_PASSENGER_TURRET")
			ELIF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iPreferredRole = ciMISSION_CLIENT_SEAT_OPTION_PASSENGER_REGULAR
				SET_RACE_PASSENGER_GLOBAL(TRUE)
				SET_PLAYER_RESPAWN_VEHICLE_SEAT_PREFERENCE(ENUM_TO_INT(VS_ANY_PASSENGER))
				
				PRINTLN("[LM][MULTI_VEH_RESPAWN] - [CACHE_AND_SETUP_VEHICLE_PARTNERS] - (Corona) iPreferredRole: ciMISSION_CLIENT_SEAT_OPTION_PASSENGER_REGULAR")
			ENDIF
		ENDIF	
	ENDIF
	
	// ############################################## CONTINGENCIES ##############################################
	// We have no driver, and no vehicle partners promote ourself.
	BOOL bDriverPromoteContingency = TRUE
	BOOL bDriverPromoteContingencyB = FALSE
	
	INT i = 0		
	FOR i = 0 TO MAX_VEHICLE_PARTNERS-1
		IF GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(i) > -1
		AND GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(i) != iLocalPart
			bDriverPromoteContingency = FALSE
		ENDIF
	ENDFOR
	
	IF bDriverPromoteContingency
		PRINTLN("[LM][MULTI_VEH_RESPAWN][CACHE_AND_SETUP_VEHICLE_PARTNERS] - Setting bDriverPromoteContingencyA true (1)")
	ENDIF
	
	IF GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(GET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER()) = -1
		bDriverPromoteContingencyB = TRUE
		PRINTLN("[LM][MULTI_VEH_RESPAWN][CACHE_AND_SETUP_VEHICLE_PARTNERS] - Setting bDriverPromoteContingencyB true (1)")
	ELSE
		IF GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(GET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER()) > -1
		AND iLocalPart = GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(GET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER())
		
			INT iPlayer = GET_PLAYER_INDEX_NUMBER_FROM_PARTICIPANT_INDEX_NUMBER(GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(GET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER()))
			IF GlobalplayerBD_FM[iPlayer].sClientCoronaData.iPreferredRole != ciMISSION_CLIENT_SEAT_OPTION_DRIVER
				bDriverPromoteContingencyB = TRUE	
				PRINTLN("[LM][MULTI_VEH_RESPAWN][CACHE_AND_SETUP_VEHICLE_PARTNERS] - Setting bDriverPromoteContingencyB true (2)")
			ENDIF
		ENDIF
	ENDIF
	
	IF bDriverPromoteContingency
		PRINTLN("[LM][MULTI_VEH_RESPAWN][CACHE_AND_SETUP_VEHICLE_PARTNERS] - INITIATING CONTINGENCY A - Player has no vehicle partners and no driver.")
		
		GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iPreferredRole = ciMISSION_CLIENT_SEAT_OPTION_DRIVER
		SET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER(0)
		SET_RACE_DRIVER_GLOBAL(TRUE)
		SET_RACE_PASSENGER_GLOBAL(FALSE)
		
		SET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(0, iLocalPart)
	ELIF bDriverPromoteContingencyB
		PRINTLN("[LM][MULTI_VEH_RESPAWN][CACHE_AND_SETUP_VEHICLE_PARTNERS] - INITIATING CONTINGENCY B - Player has vehicle partners but no driver.")
		
		INT iOldDriverPointer = GET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER()
		INT iNewDriverPointer = (iOldDriverPointer+1)
		
		PRINTLN("[LM][MULTI_VEH_RESPAWN][CACHE_AND_SETUP_VEHICLE_PARTNERS] - DriverPointer is old and is pointing to an invalid participant. Reassigning iOldDriverPointer: ", iOldDriverPointer)			
		PRINTLN("[LM][MULTI_VEH_RESPAWN][CACHE_AND_SETUP_VEHICLE_PARTNERS] - iNewDriverPointer: ", iNewDriverPointer, " PartNum: ", GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(iNewDriverPointer))
		
		SET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER(iNewDriverPointer)
		
		// If we are the new Driver then we need to call some stuff...
		IF GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(GET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER()) = iLocalPart
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iPreferredRole = ciMISSION_CLIENT_SEAT_OPTION_DRIVER
			SET_RACE_PASSENGER_GLOBAL(FALSE)
			SET_RACE_DRIVER_GLOBAL(TRUE)
			SET_PLAYER_RESPAWN_VEHICLE_SEAT_PREFERENCE(ENUM_TO_INT(VS_DRIVER))
			PRINTLN("[LM][MULTI_VEH_RESPAWN][CACHE_AND_SETUP_VEHICLE_PARTNERS] - I am now promoted as the new Driver")
		ENDIF
	ENDIF
	
	SET_BIT(iLocalBoolCheck25, LBOOL25_INITIALIZED_SEATING_DATA_FOR_MULTI_VEH)
	
ENDPROC

FUNC BOOL IS_PLAYER_SEAT_PREFERENCE_PASSENGER()
	RETURN (INT_TO_ENUM(VEHICLE_SEAT, GET_PLAYER_RESPAWN_VEHICLE_SEAT_PREFERENCE()) != VS_DRIVER)
ENDFUNC

FUNC BOOL IS_PARTICIPANT_DRIVING_SEAT_SWITCH_VEHICLE(PARTICIPANT_INDEX partToCheck)
	IF NETWORK_IS_PARTICIPANT_ACTIVE(partToCheck)
		IF partToCheck = INT_TO_PARTICIPANTINDEX(GET_PLAYER_VEHICLE_PARTNER_FROM_INDEX(GET_PLAYER_VEHICLE_PARTNER_DRIVER_POINTER()))
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PART_NOT_FINISHED_OR_REAL_SPECTATOR(INT iPart)
	RETURN ((NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_ANY_SPECTATOR)) OR IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_HEIST_SPECTATOR))
			AND NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet,PBBOOL_PLAYER_FAIL)
			AND NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_FINISHED)
ENDFUNC

FUNC PLAYER_INDEX GET_NEXT_TURN_PLAYER_ON_TEAM(INT iTeam)
	
	PLAYER_INDEX piFirstFound = INVALID_PLAYER_INDEX()
	PLAYER_INDEX piToReturn = INVALID_PLAYER_INDEX()
	BOOL bReturnNext = FALSE
	IF MC_serverBD.piTurnsActivePlayer[iTeam] = INVALID_PLAYER_INDEX()
		bReturnNext = TRUE
	ENDIF
	INT iPart
	FOR iPart = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		IF MC_playerBD[iPart].iTeam = iTeam
			PRINTLN("GET_NEXT_TURN_PLAYER_ON_TEAM - MC_playerBD[iPart].iTeam: ", MC_playerBD[iPart].iTeam, " iTeam: ", iTeam)
			IF IS_PART_NOT_FINISHED_OR_REAL_SPECTATOR(iPart)
				PRINTLN("GET_NEXT_TURN_PLAYER_ON_TEAM - Not finished, participant ", iPart)
				IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPart))
					PLAYER_INDEX tempPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPart))
					IF bReturnNext
						PRINTLN("GET_NEXT_TURN_PLAYER_ON_TEAM - ReturnNext ", ipart)
						piToReturn = tempPlayer
						BREAKLOOP
					ENDIF
					IF piFirstFound = INVALID_PLAYER_INDEX()
						PRINTLN("GET_NEXT_TURN_PLAYER_ON_TEAM - firstfound ", ipart)
						piFirstFound = tempPlayer
					ENDIF
					IF tempPlayer = MC_serverBD.piTurnsActivePlayer[iTeam]
						PRINTLN("GET_NEXT_TURN_PLAYER_ON_TEAM - Setting bReturnNext")
						bReturnNext = TRUE
					ENDIF
				ELSE
					PRINTLN("GET_NEXT_TURN_PLAYER_ON_TEAM - participant ", ipart," is not active")
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	IF bReturnNext
	AND piToReturn = INVALID_PLAYER_INDEX()
		piToReturn = piFirstFound
	ENDIF
	RETURN piToReturn
ENDFUNC

FUNC INT GET_THIS_TURN_PART_ON_TEAM(INT iTeam, INT iTurn)
	INT iPart, iTurnCount, iPlayerCount
	IF MC_serverBD.iNumberOfPlayingPlayers[iTeam] > 0
		FOR iPart = 0 TO (NUM_NETWORK_PLAYERS - 1)
			IF MC_playerBD[iPart].iTeam = iTeam
				IF IS_PART_NOT_FINISHED_OR_REAL_SPECTATOR(iPart)
					IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPart))
						IF iTurnCount = iTurn
							RETURN iPart
						ENDIF
						iPlayerCount++
						iTurnCount++
					ENDIF
				ENDIF
			ENDIF
			
			IF iPlayerCount >= MC_serverBD.iNumberOfPlayingPlayers[iTeam]
			AND iTurnCount <= iTurn
				iPlayerCount = 0
				iPart = -1 //RELOOPing will add 1
				RELOOP
			ENDIF
		ENDFOR
	ENDIF
	RETURN -1
ENDFUNC

FUNC BOOL ARE_ALL_PLAYER_RENDERHASES_PAUSED_PRIVATE()

	INT iParticipant
	PARTICIPANT_INDEX piTemp
	PLAYER_INDEX piPlayer

	REPEAT GET_MAXIMUM_PLAYERS_ON_MISSION() iParticipant
		piTemp = INT_TO_PARTICIPANTINDEX(iParticipant)
		
		IF NETWORK_IS_PARTICIPANT_ACTIVE(piTemp)
			piPlayer = NETWORK_GET_PLAYER_INDEX(piTemp)
			
			IF IS_NET_PLAYER_OK(piPlayer, FALSE)
			AND (NOT(IS_PLAYER_SPECTATING(piPlayer)) OR (IS_BIT_SET(MC_playerBD[iParticipant].iClientBitset, PBBOOL_HEIST_SPECTATOR) OR IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN)))
			AND (GET_PLAYER_TEAM(piPlayer) != -1)
				IF NOT IS_BIT_SET(MC_PlayerBD[iParticipant].iClientBitSet3, PBBOOL3_RESTART_RENDERPHASE_PAUSED)
					PRINTLN("[JS][Restart] ARE_ALL_PLAYER_RENDERHASES_PAUSED_PRIVATE - waiting for part ", iparticipant, " to pause their renderphase")
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT

	RETURN TRUE
	
ENDFUNC

FUNC BOOL IS_DISABLED_CONTROL_HELD(CONTROL_ACTION caAction, INT iHoldDuration)
	
	IF IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, caAction)
	AND HAS_NET_TIMER_STARTED(tdButtonHeldTimer)
	AND HAS_NET_TIMER_EXPIRED(tdButtonHeldTimer, iHoldDuration)
	AND IS_BIT_SET(iLocalBoolCheck25, LBOOL25_HOLDING_CONTROL)
		RETURN TRUE
	ELIF IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, caAction)
		IF NOT IS_BIT_SET(iLocalBoolCheck25, LBOOL25_HOLDING_CONTROL)
			SET_BIT(iLocalBoolCheck25, LBOOL25_HOLDING_CONTROL)
			START_NET_TIMER(tdButtonHeldTimer)
		ENDIF
	ELSE
		IF IS_BIT_SET(iLocalBoolCheck25, LBOOL25_HOLDING_CONTROL)		
			CLEAR_BIT(iLocalBoolCheck25, LBOOL25_HOLDING_CONTROL)
		ENDIF
		IF HAS_NET_TIMER_STARTED(tdButtonHeldTimer)
			RESET_NET_TIMER(tdButtonHeldTimer)
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC INT GET_PLAYER_TEAM_FOR_INTRO(INT iTeamOverride = -1)
	INT iTeam = MC_playerBD[iLocalPart].iTeam
	INT i
	
	IF iTeamOverride != -1
		iTeam = iTeamOverride
	ENDIF
	
	PRINTLN("[MMacK][TeamIntro] Player team is ", iTeam)
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciMOVE_TEAM_SPAWNS_EACH_ROUND)
	AND g_sTransitionSessionData.sMissionRoundData.iNumberOfRoundsPlayed > 0
		FOR i = 0 TO g_sTransitionSessionData.sMissionRoundData.iNumberOfRoundsPlayed-1
			iTeam++
			IF iTeam >= g_FMMC_STRUCT.iNumberOfTeams  
				iTeam = 0
			ENDIF
			PRINTLN("[MMacK][TeamIntro] Increased team for round ", i)
		ENDFOR
	ENDIF
	
	PRINTLN("[MMacK][TeamIntro] Set player team updated to ", iTeam)
	
	IF iTeam < 0 OR iTeam >= FMMC_MAX_TEAMS
		iTeam = 0
		PRINTLN("[MMacK][TeamIntro] something went very wrong, team set to default ", iTeam)
	ENDIF
	
	RETURN iTeam
ENDFUNC

// This function will make the player respawn back at the start of the mission using their restart pos.
PROC CLEANUP_PLAYER_FOR_WINNER_LOSER_SHARD_APPEARANCE(BOOL bKillPlayer = FALSE, BOOL inNotVehicle = FALSE)
	
	PRINTLN("[LM][CLEANUP_PLAYER_FOR_WINNER_LOSER_SHARD_APPEARANCE] - Settting the player in a state so that they don't look ridiculous on the WINNER/LOSER ")
	
	IF NOT IS_PED_INJURED(localPlayerPed)
		CLEAR_PED_TASKS_IMMEDIATELY(localPlayerPed)
		SET_PED_TO_RAGDOLL(LocalPlayerPed, 3500, 5000, TASK_RELAX, FALSE, FALSE)
	ENDIF
	
	INT iPlayerTeam = GET_PLAYER_TEAM_FOR_INTRO()
	INT iTeamSlot = GET_PARTICIPANT_NUMBER_IN_TEAM()
	VECTOR vStartPos = g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[iPlayerTeam][iTeamSlot].vPos + <<0,0,1>>
	FLOAT fHeading = g_FMMC_STRUCT.fTeamRestartHeading[iPlayerTeam][0][iTeamSlot]
	
	SETUP_SPECIFIC_SPAWN_LOCATION(vStartPos, fHeading, 5.0, FALSE, 0, DEFAULT, DEFAULT, 1)
	SET_PLAYER_NEXT_RESPAWN_LOCATION(SPAWN_LOCATION_AT_SPECIFIC_COORDS, TRUE) 
	
	IF inNotVehicle
		SET_PLAYER_RESPAWN_IN_VEHICLE(FALSE)
		SET_BIT(iLocalBoolCheck25, LBOOL25_CLEANUP_RESPAWN_SPAWN_AT_START_NO_VEH)
		PRINTLN("[LM][CLEANUP_PLAYER_FOR_WINNER_LOSER_SHARD_APPEARANCE] - Making sure they don't spawn in a vehicle.")
	ENDIF
	
	IF bKillPlayer
		IF NOT IS_PED_INJURED(localPlayerPed)
			SET_ENTITY_INVINCIBLE(localPlayerPed, FALSE)
			SET_ENTITY_CAN_BE_DAMAGED(localPlayerPed, TRUE)
			SET_ENTITY_HEALTH(localPlayerPed, 0)
		ENDIF
		PRINTLN("[LM][CLEANUP_PLAYER_FOR_WINNER_LOSER_SHARD_APPEARANCE] - Killing the player so that they respawn at the start, and that the shard wait for us.")
	ENDIF
		
ENDPROC

FUNC BOOL ARE_ALL_PLAYING_PLAYERS_NOT_DEAD()

	INT iParticipant
	PARTICIPANT_INDEX piTemp
	PLAYER_INDEX piPlayer

	REPEAT GET_MAXIMUM_PLAYERS_ON_MISSION() iParticipant
		piTemp = INT_TO_PARTICIPANTINDEX(iParticipant)
		
		IF NETWORK_IS_PARTICIPANT_ACTIVE(piTemp)
			piPlayer = NETWORK_GET_PLAYER_INDEX(piTemp)
				
			IF NOT IS_PLAYER_SPECTATING(piPlayer)
			AND NOT IS_BIT_SET(MC_playerBD[iParticipant].iClientBitset, PBBOOL_HEIST_SPECTATOR)
			AND NOT IS_BIT_SET(MC_playerBD[iParticipant].iClientBitSet3, PBBOOL3_WAITING_IN_SPECTATOR_FOR_RESPAWN)
			AND GET_PLAYER_TEAM(piPlayer) != -1
			
				PED_INDEX pedPlayer = GET_PLAYER_PED(piPlayer)
			
				IF IS_PED_INJURED(pedPlayer)
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT

	RETURN TRUE
	
ENDFUNC

FUNC BOOL ARE_ANY_RESPAWN_ON_RULE_VEHICLES_DEAD()
	IF MC_serverBD.iNumVehCreated > 0
		INT i
		FOR i = 0 TO (MC_serverBD.iNumVehCreated-1)
			IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iRespawnOnRuleChangeBS > 0
				INT iTeam
				FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].iRespawnOnRuleChangeBS, iTeam)
						IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[i])
							RETURN TRUE
						ELSE
							VEHICLE_INDEX vehIndex = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i])
							IF NOT IS_VEHICLE_DRIVEABLE(vehIndex)
								RETURN TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDFOR
			ENDIF
		ENDFOR
	ENDIF
	
	RETURN FALSE
ENDFUNC

//[KH][SSDS] check if only 1 team alive to pass mission
/// PURPOSE: Checks how many teams are still playing and 
///    
/// RETURNS: TRUE if only 1 team is remaining in the game
///    
FUNC BOOL IS_ONLY_ONE_TEAM_ALIVE(INT& iWinningTeam)
	
	INT iTeamBS, i, iTeamsStillPlaying, winningTeam
	
	FOR i = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
			PLAYER_INDEX tempPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
			IF IS_NET_PLAYER_OK(tempPlayer, TRUE)
			AND IS_ENTITY_ALIVE(GET_PLAYER_PED(tempPlayer))
			AND NOT IS_PARTICIPANT_A_SPECTATOR(i)
				IF NOT IS_BIT_SET(iTeamBS, MC_playerBD[i].iteam)
					PRINTLN("[KH] IS_ONLY_ONE_TEAM_ALIVE -  team ", MC_playerBD[i].iteam, " is still going")
					SET_BIT(iTeamBS, MC_playerBD[i].iteam)
					iTeamsStillPlaying++
					winningTeam = MC_playerBD[i].iteam
					IF iTeamsStillPlaying > 1
						PRINTLN("[KH] IS_ONLY_ONE_TEAM_ALIVE - Nope")
						BREAKLOOP
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	IF iTeamsStillPlaying <= 1
		iWinningTeam = winningTeam
		RETURN TRUE
	ELSE
		iWinningTeam = -1
		RETURN FALSE
	ENDIF
	
ENDFUNC

FUNC INT GET_NUMBER_OF_PARTICIPANTS_FROM_TEAM_IN_THIS_AREA(INT iLoc, INT iTeam)
	INT iPart, iNumberInArea
	FOR iPart = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		IF MC_PlayerBD[iPart].iteam = iTeam
			IF MC_playerBD[iPart].iCurrentLoc = iLoc
			AND NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPart))
				iNumberInArea++
			ENDIF
		ENDIF
	ENDFOR
	RETURN iNumberInArea
ENDFUNC

///PURPOSE: For those times where something needs to be added for gangops that might intefere with Heist logic.
FUNC BOOL IS_THIS_GANG_OPS_AND_BOOL_OR_NOT_GANG_OPS(BOOL bCheck)
	IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION() AND bCheck
	OR NOT GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_TEAM_CONTROL_ALL_CAPTURE_LOCATIONS(INT iTeam)
	INT i
	FOR i = 0 TO MC_serverBD.iNumLocCreated-1
		IF MC_serverBD.iLocOwner[i] != iTeam
			RETURN FALSE
		ENDIF
	ENDFOR
	RETURN TRUE
ENDFUNC

FUNC BOOL DOES_OPPOSING_TEAMS_HAVE_ANY_LEFT_ALIVE()
	INT iPart
	FOR iPart = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		IF NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPart)) != INVALID_PLAYER_INDEX()
			IF NOT IS_PLAYER_SPECTATOR_ONLY(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPart)))
				IF MC_PlayerBD[iPart].iteam != MC_playerBD[iLocalPart].iteam
				AND NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPart))
				AND IS_NET_PLAYER_OK(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPart)))
				AND NOT DOES_TEAM_LIKE_TEAM(MC_PlayerBD[iPart].iteam, MC_playerBD[iLocalPart].iteam)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	RETURN FALSE
ENDFUNC

FUNC BOOL DO_ALL_TEAMS_NEED_TO_HAVE_FINISHED_LAPS()	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_AllTeamsAllLaps)
	OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_RUNNING_BACK_REMIX(g_FMMC_STRUCT.iAdversaryModeType) //Remove this once we know that the creators have set the above option
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAVE_ALL_TEAMS_FINISHED_ALL_LAPS()
	INT i
	
	FOR i = 0 TO MC_serverBD.iNumberOfTeams - 1
		IF MC_ServerBD_4.iTeamLapsCompleted[i] < g_FMMC_STRUCT.sFMMCEndConditions[i].iMissionLapLimit
			PRINTLN("HAVE_ALL_TEAMS_FINISHED_ALL_LAPS - Team ", i, " hasn't finished all laps yet!")
			RETURN FALSE
		ENDIF
	ENDFOR
	
	PRINTLN("HAVE_ALL_TEAMS_FINISHED_ALL_LAPS - All teams have finished their laps!")
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_ZONE_BEING_CONTESTED(INT iLoc, BOOL bCheckForOwner = TRUE, BOOL bUnCapturedCheck = FALSE)
	INT i
	FOR i = 0 TO MC_serverBD.iNumActiveTeams-1
		IF NOT DOES_TEAM_LIKE_TEAM(MC_serverBD.iLocOwner[iLoc], i)
		AND MC_serverBD.iLocOwner[iLoc] != i
		AND (MC_serverBD.iLocOwner[iLoc] != -1 OR NOT bCheckForOwner)
			IF NOT bUnCapturedCheck
				IF MC_serverBD_1.inumberOfTeamInArea[iLoc][i] > 0 AND (NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_HOSTILE_TAKEOVER(g_FMMC_STRUCT.iAdversaryModeType))
				OR iHostileTakeoverInZoneCount[iloc][i] > 0
					RETURN TRUE
				ENDIF
			ELSE
				IF i != MC_playerBD[iPartToUse].iteam
					IF MC_serverBD_1.inumberOfTeamInArea[iLoc][i] > 0 AND (NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_HOSTILE_TAKEOVER(g_FMMC_STRUCT.iAdversaryModeType))
					OR iHostileTakeoverInZoneCount[iloc][i] > 0
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC

PROC ADD_FAKE_VISION_CONE_TO_BLIP(BLIP_INDEX biToUse, FLOAT fVisualFieldMinAzimuthAngle, FLOAT fVisualFieldMaxAzimuthAngle, FLOAT fCentreOfGazeMaxAngle, FLOAT fPeripheralRange, FLOAT fFocusRange, FLOAT fRotation, BOOL bContinuousUpdate)
	
	IF NOT DOES_BLIP_EXIST(biToUse)
		PRINTLN("[RCC MISSION] ADD_FAKE_VISION_CONE_TO_ENTITY - biToUse does not exist, early out")	
		EXIT
	ENDIF
	
	PRINTLN("[RCC MISSION] ADD_FAKE_VISION_CONE_TO_ENTITY - Cone being added with the following settings:")
	PRINTLN("[RCC MISSION] ADD_FAKE_VISION_CONE_TO_ENTITY - fVisualFieldMinAzimuthAngle: ", fVisualFieldMinAzimuthAngle)
	PRINTLN("[RCC MISSION] ADD_FAKE_VISION_CONE_TO_ENTITY - fVisualFieldMaxAzimuthAngle: ", fVisualFieldMaxAzimuthAngle)
	PRINTLN("[RCC MISSION] ADD_FAKE_VISION_CONE_TO_ENTITY - fCentreOfGazeMaxAngle: ", fCentreOfGazeMaxAngle)
	PRINTLN("[RCC MISSION] ADD_FAKE_VISION_CONE_TO_ENTITY - fPeripheralRange: ", fPeripheralRange)
	PRINTLN("[RCC MISSION] ADD_FAKE_VISION_CONE_TO_ENTITY - fFocusRange: ", fFocusRange)
	PRINTLN("[RCC MISSION] ADD_FAKE_VISION_CONE_TO_ENTITY - fRotation: ", fRotation)
	PRINTLN("[RCC MISSION] ADD_FAKE_VISION_CONE_TO_ENTITY - bContinuousUpdate: ", BOOL_TO_STRING(bContinuousUpdate))

	//Clear fake cone array before calling this for the first time
	IF NOT IS_BIT_SET(iLocalBoolCheck25, LBOOL25_CLEAR_FAKE_CONE_ARRAY_CALLED)
		PRINTLN("[RCC MISSION] ADD_FAKE_VISION_CONE_TO_ENTITY - calling CLEAR_FAKE_CONE_ARRAY")	
		CLEAR_FAKE_CONE_ARRAY()
		SET_BIT(iLocalBoolCheck25, LBOOL25_CLEAR_FAKE_CONE_ARRAY_CALLED)
	ENDIF
	
	SETUP_FAKE_CONE_DATA(biToUse,fVisualFieldMinAzimuthAngle,fVisualFieldMaxAzimuthAngle,fCentreOfGazeMaxAngle,fPeripheralRange, fFocusRange, fRotation, bContinuousUpdate)
	SET_BLIP_SHOW_CONE(biToUse, TRUE)
	
ENDPROC

FUNC BOOL IS_THIS_CAM_THIS_ENTITY(INT iCam, INT iEntityIndex, FMMC_ELECTRONIC_ENTITY_TYPE eType)
	IF iEntityIndex = MC_serverBD.iCameraNumber[iCam]
	AND eType = MC_serverBD.eCameraType[iCam]
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_CCTV_INDEX_FOR_ENTITY(INT iIndex, FMMC_ELECTRONIC_ENTITY_TYPE eType)
	
	INT iCam
	FOR iCam = 0 TO (MAX_NUM_CCTV_CAM - 1)
		IF IS_THIS_CAM_THIS_ENTITY(iCam, iIndex, eType)
			RETURN iCam
		ENDIF
	ENDFOR
	
	RETURN -1
ENDFUNC

PROC REMOVE_CCTV_VISION_CONE(BLIP_INDEX biToUse, INT iIndex, FMMC_ELECTRONIC_ENTITY_TYPE eType)
	
	IF NOT DOES_BLIP_EXIST(biToUse)
		PRINTLN("[RCC MISSION] REMOVE_CCTV_VISION_CONE - iIndex ", iIndex, " Blip does not exist, exitting.")
		EXIT
	ENDIF
	
	INT iCam = GET_CCTV_INDEX_FOR_ENTITY(iIndex, eType)
	IF iCam = -1
		PRINTLN("[RCC MISSION] REMOVE_CCTV_VISION_CONE - iIndex ", iIndex, " no iCam found")
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(iCCTVConeBitSet, ci_CCTV_CONE_BLIP_ADDED_0 + iCam)
		PRINTLN("[RCC MISSION] REMOVE_CCTV_VISION_CONE - iIndex ", iIndex, " is iCam ", iCam, ", but cone blip not yet added")
		EXIT
	ENDIF
	
	PRINTLN("[RCC MISSION] REMOVE_CCTV_VISION_CONE - iIndex ", iIndex, " is iCam ", iCam, ", cone being removed from blip")
	
	SET_BLIP_SHOW_CONE(biToUse, FALSE)
	REMOVE_FAKE_CONE_DATA(biToUse)
	
	CLEAR_BIT(iCCTVConeBitSet, ci_CCTV_CONE_BLIP_ADDED_0 + iCam)
	
ENDPROC

FUNC VECTOR GET_CLOSEST_POINT_AT_EDGE_OF_BOUNDS(VECTOR vPoint, VECTOR vCoord1, VECTOR vCoord2, FLOAT fWidth)
	vCoord1.Z = FMAX(vCoord1.Z, vCoord2.Z)
	vCoord2.Z = vCoord1.Z
	
	VECTOR vDir = vCoord2 - vCoord1
	
	FLOAT fTemp = vDir.X
	vDir.X = vDir.Y
	vDir.Y = 0 - fTemp
	
	vDir = NORMALISE_VECTOR(vDir)
	
	fWidth = (fWidth / 2) - 2
	
	VECTOR vCorner[4]
	
	vCorner[0] = vCoord1 + (vDir * fWidth)
	vCorner[1] = vCoord1 + (vDir * -fWidth)
	vCorner[2] = vCoord2 + (vDir * fWidth)
	vCorner[3] = vCoord2 + (vDir * -fWidth)
	
	INT iClosestPoint = 0
	
	VECTOR vClosestPoint[4]
	
	vClosestPoint[0] = GET_CLOSEST_POINT_ON_LINE(vPoint, vCorner[0], vCorner[1])
	vClosestPoint[1] = GET_CLOSEST_POINT_ON_LINE(vPoint, vCorner[0], vCorner[2])
	vClosestPoint[2] = GET_CLOSEST_POINT_ON_LINE(vPoint, vCorner[2], vCorner[3])
	vClosestPoint[3] = GET_CLOSEST_POINT_ON_LINE(vPoint, vCorner[1], vCorner[3])
	
	INT i
	
	FOR i = 1 TO 3	//Skip 0, it's our starting point
		IF GET_DISTANCE_BETWEEN_COORDS(vPoint, vClosestPoint[i]) < GET_DISTANCE_BETWEEN_COORDS(vPoint, vClosestPoint[iClosestPoint])
			iClosestPoint = i
		ENDIF
	ENDFOR
	
	RETURN vClosestPoint[iClosestPoint]
ENDFUNC

FUNC FLOAT CHECK_Z_FOR_OVERHANG(VECTOR vObjCoord)
	FLOAT fGroundZ
	VECTOR tempVector
	tempVector.z = vObjCoord.z - 2
	
	IF GET_GROUND_Z_FOR_3D_COORD(tempVector, fGroundZ)
		vObjCoord.z = fGroundZ
		PRINTLN("[JT] CHECK_Z_FOR_OVERHANG Valid ground z found: ", fGroundZ)
	ENDIF	
	
	RETURN vObjCoord.z
ENDFUNC

FUNC BOOL IS_ENTITY_OUT_OF_BOUNDS(ENTITY_INDEX entToUse, INT iTeam)
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	PRINTLN("[RCC MISSION] IS_ENTITY_OUT_OF_BOUNDS team: ", iTeam, " iRule: ", iRule)
	IF iRule < FMMC_MAX_RULES
		
		IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].iType = 1
			PRINTLN("[RCC MISSION] IS_ENTITY_OUT_OF_BOUNDS g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].iType = 1")
			IF NOT IS_ENTITY_IN_ANGLED_AREA(entToUse, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].vPos1, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].vPos2, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].fWidth)
				PRINTLN("[RCC MISSION] IS_ENTITY_OUT_OF_BOUNDS returning true")
				RETURN TRUE
			ENDIF
		ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].iType = 0
		AND NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].vPos)
			VECTOR vEntCoords
			IF VDIST2(vEntCoords, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].vPos) < POW(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].fRadius, 2)
				PRINTLN("[RCC MISSION] IS_ENTITY_OUT_OF_BOUNDS returning true")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_VEHICLE_A_TRAILER(VEHICLE_INDEX vehToCheck)
	IF DOES_ENTITY_EXIST(vehToCheck)
		IF GET_ENTITY_MODEL(vehToCheck) = INT_TO_ENUM(MODEL_NAMES, HASH("TRAILERLARGE"))
		OR GET_ENTITY_MODEL(vehToCheck) = INT_TO_ENUM(MODEL_NAMES, HASH("TRAILERSMALL2"))
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_CLOSEST_PART_TO_PED_FROM_TEAMS(PED_INDEX pedToCheck, INT iTeamsBS, FLOAT& fClosestDistance2)

	INT iClosestPart = -1
	FLOAT fClosestDist2 = 999999999 //Furthest distance apart on map is O(10,000)
	FLOAT fTempDist2
	
	INT iTeam
	FOR iTeam = 0 TO (FMMC_MAX_TEAMS - 1)
		
		IF IS_BIT_SET(iTeamsBS,iTeam)
			INT iClosestPlyrOnTeam = GET_NEAREST_PLAYER_TO_ENTITY_ON_TEAM(pedToCheck,iTeam)
			PLAYER_INDEX tempPlayer = INT_TO_PLAYERINDEX(iClosestPlyrOnTeam)
			IF (tempPlayer != INVALID_PLAYER_INDEX())
			AND IS_NET_PLAYER_OK(tempPlayer)
				fTempDist2 = VDIST2(GET_PLAYER_COORDS(tempPlayer),GET_ENTITY_COORDS(pedToCheck))
				IF (fTempDist2 < fClosestDist2)
					fClosestDist2 = fTempDist2
					iClosestPart = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(tempPlayer))
				ENDIF
			ENDIF
		ENDIF
		
	ENDFOR
	
	fClosestDistance2 = fClosestDist2
	
	RETURN iClosestPart
	
ENDFUNC

FUNC BOOL SHOULD_PED_PRIORITIZE_ANIM_OVER_ACTION_TASK(INT iPed, BOOL bCalledFromCombat = FALSE)
	
	// This function is called in places where I couldn't pass in a tempPed or ThisPed reference so I had to create a reference to the ped here.
	IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[iped])
		PED_INDEX ThisPed = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iped])
		IF DOES_ENTITY_EXIST(ThisPed)
			BOOL bReady = FALSE
			
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEleven, ciPed_BSEleven_PrioritizeSpecialAnimOverTaskForRule)					
				INT iTeam = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iSpecialAnimTeam
				
				PRINTLN("[SHOULD_PED_PRIORITIZE_ANIM_OVER_ACTION_TASK] - iPed: ", iPed, " ciPed_BSEleven_PrioritizeSpecialAnimOverTaskForRule Set. Checking Rule & Teams to see if Anim should take priority. iSpecialAnimTeam: ", iTeam)
				
				IF iTeam > -1
					IF IS_TEAM_ACTIVE(iTeam)
					AND MC_ServerBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
					AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iSpecialAnimRule > -1
						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo, ciPED_BSTwo_SpecialAnim_On_Midpoint)
							IF IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iTeam], MC_ServerBD_4.iCurrentHighestPriority[iTeam])
								bReady = TRUE
							ENDIF
						ELSE
							bReady = TRUE
						ENDIF
						
						IF bCalledFromCombat
							IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetThirteen, ciPed_BSThirteen_SpecialAnimPrioritizedOverCombat)
								PRINTLN("[SHOULD_PED_PRIORITIZE_ANIM_OVER_ACTION_TASK] - iPed: ", iPed, " Called from Combat - ciPed_BSThirteen_SpecialAnimPrioritizedOverCombat NOT Set. bReady = FALSE")
								bReady = FALSE
							ELSE
								PRINTLN("[SHOULD_PED_PRIORITIZE_ANIM_OVER_ACTION_TASK] - iPed: ", iPed, " Called from Combat - ciPed_BSThirteen_SpecialAnimPrioritizedOverCombat Set. Should be ready.")
							ENDIF
						ENDIF
						
						IF IS_BIT_SET(sMissionPedsLocalVars[iPed].iPedBS, ci_MissionPedBS_iSyncSceneAnimationFinished)
							bReady = FALSE
						ENDIF
						
						IF bReady					
							IF MC_ServerBD_4.iCurrentHighestPriority[iTeam] = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iSpecialAnimRule
								IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].fSpecialAnimTriggerRange < cfMAX_SPECIALANIM_TRIGGER_DIST
									
									// Copied from start - PROCESS_PED_ANIMATION
									FLOAT fClosestDistance2
									INT iClosestPart
									
									iClosestPart = GET_CLOSEST_PART_TO_PED_FROM_TEAMS(ThisPed, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iSpecialAnimTriggerTeamBitset, fClosestDistance2)
									
									IF iClosestPart != -1								
										IF (fClosestDistance2 <= ((g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].fSpecialAnimTriggerRange)*(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].fSpecialAnimTriggerRange)) )
											PRINTLN("[SHOULD_PED_PRIORITIZE_ANIM_OVER_ACTION_TASK] iPed: ", iPed, " Returning TRUE iSpecialAnimRule: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iSpecialAnimRule, " = iCurrentHighestPriority of iTeam: ", iTeam)
											RETURN TRUE
										ENDIF
									ENDIF
									// Copied from end.
								ELSE
									PRINTLN("[SHOULD_PED_PRIORITIZE_ANIM_OVER_ACTION_TASK] iPed: ", iPed, " Returning TRUE iSpecialAnimRule: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iSpecialAnimRule, " = iCurrentHighestPriority of iTeam: ", iTeam)
									RETURN TRUE
								ENDIF
							ENDIF			
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetEleven, ciPed_BSEleven_PrioritizeIdleAnimOverTaskForRule)				
				INT iTeam = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iIdleAnimTeam
				
				PRINTLN("[SHOULD_PED_PRIORITIZE_ANIM_OVER_ACTION_TASK] iPed: ", iPed, " ciPed_BSEleven_PrioritizeIdleAnimOverTaskForRule Set. Checking Rule & Teams to see if Anim should take priority - iIdleAnimTeam: ", iTeam)
				
				IF iTeam > -1
					IF IS_TEAM_ACTIVE(iTeam)
					AND MC_ServerBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
						
						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo, ciPED_BSTwo_IdleAnim_On_Midpoint)
							IF IS_BIT_SET(MC_serverBD.iObjectiveMidPointBitset[iTeam], MC_ServerBD_4.iCurrentHighestPriority[iTeam])
								bReady = TRUE
							ENDIF
						ELSE
							bReady = TRUE
						ENDIF
						
						IF bCalledFromCombat
							IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetThirteen, ciPed_BSThirteen_IdleAnimPrioritizedOverCombat)							
								PRINTLN("[SHOULD_PED_PRIORITIZE_ANIM_OVER_ACTION_TASK] - iPed: ", iPed, " Called from Combat - ciPed_BSThirteen_IdleAnimPrioritizedOverCombat NOT Set. bReady = FALSE")
								bReady = FALSE
							ELSE
								PRINTLN("[SHOULD_PED_PRIORITIZE_ANIM_OVER_ACTION_TASK] - iPed: ", iPed, " Called from Combat - ciPed_BSThirteen_IdleAnimPrioritizedOverCombat Set. Should be ready.")
							ENDIF
						ENDIF
						
						IF IS_BIT_SET(sMissionPedsLocalVars[iPed].iPedBS, ci_MissionPedBS_iSyncSceneAnimationFinished)
							bReady = FALSE
						ENDIF
						
						IF bReady		
							IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo, ciPED_BSTwo_IdleAnimOnJustThisRule)
								IF MC_ServerBD_4.iCurrentHighestPriority[iTeam] = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iIdleAnimRule					
									PRINTLN("[SHOULD_PED_PRIORITIZE_ANIM_OVER_ACTION_TASK] iPed: ", iPed, " Returning TRUE iIdleAnimRule: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iIdleAnimRule, " = iCurrentHighestPriority of iTeam: ", iTeam)
									RETURN TRUE
								ENDIF
							ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo, ciPED_BSTwo_IdleAnimBeforeAndInclThisRule)
								IF MC_ServerBD_4.iCurrentHighestPriority[iTeam] <= g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iIdleAnimRule
									PRINTLN("[SHOULD_PED_PRIORITIZE_ANIM_OVER_ACTION_TASK] iPed: ", iPed, " Returning TRUE iIdleAnimRule: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iIdleAnimRule, " <= iCurrentHighestPriority of iTeam: ", iTeam)
									RETURN TRUE
								ENDIF
							ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwo, ciPED_BSTwo_IdleAnimAfterAndInclThisRule)
								IF MC_ServerBD_4.iCurrentHighestPriority[iTeam] >= g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iIdleAnimRule
									PRINTLN("[SHOULD_PED_PRIORITIZE_ANIM_OVER_ACTION_TASK] iPed: ", iPed, " Returning TRUE iIdleAnimRule: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iIdleAnimRule, " >= iCurrentHighestPriority of iTeam: ", iTeam)
									RETURN TRUE
								ENDIF
							ELSE
								PRINTLN("[SHOULD_PED_PRIORITIZE_ANIM_OVER_ACTION_TASK] iPed: ", iPed, " Returning TRUE iIdleAnimRule: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iIdleAnimRule, " >= iCurrentHighestPriority of iTeam: ", iTeam)
								RETURN TRUE
							ENDIF
						ENDIF						
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL CAN_PLACED_PED_BE_TARGETTED_BY_ORBITAL_CANNON(PED_INDEX piTarget, INT iPed)
	
	IF IS_ENTITY_A_GHOST(piTarget)
	OR IS_PED_IN_ANY_VEHICLE(piTarget) AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetEleven, ciPED_BSEleven_HideMarkerFromOrbitalCannonIfInsideVehicle)
	OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetEleven, ciPED_BSEleven_HideMarkerFromOrbitalCannon)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL CAN_PLACED_VEHICLE_BE_TARGETTED_BY_ORBITAL_CANNON(VEHICLE_INDEX vehIndex, INT iVeh)
	
	IF IS_ENTITY_A_GHOST(vehIndex)	
	OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetSeven, ciFMMC_VEHICLE7_HIDE_MARKER_FROM_ORBITAL_CANNON)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Handles drawing boxes around players in session, and arrows for players offscreen
PROC DRAW_PLACED_PED_TRACKERS()
	PED_INDEX piTarget
	
	INT i
	REPEAT FMMC_MAX_PEDS i
		IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[i])
			piTarget = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[i])
			
			IF DOES_ENTITY_EXIST(piTarget)
			AND NOT IS_PED_INJURED(piTarget)
				IF CAN_PLACED_PED_BE_TARGETTED_BY_ORBITAL_CANNON(piTarget, i)
					IF HAS_STREAMED_TEXTURE_DICT_LOADED("helicopterhud")
						DRAW_BOX_AROUND_TARGET(piTarget, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sPedBlipStruct.iBlipSpriteOverride > 0)
						DRAW_OFFSCREEN_ARROW_FOR_COORD(piTarget)
						
						IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sPedBlipStruct.iBlipSpriteOverride > 0
							DRAW_TARGET_LETTER_FOR_COORD(piTarget, g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].sPedBlipStruct.iBlipSpriteOverride)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
//    Handles drawing boxes around players in session, and arrows for players offscreen
PROC DRAW_PLACED_VEHICLE_TRACKERS()
	VEHICLE_INDEX vehIndex
	
	INT i
	REPEAT FMMC_MAX_VEHICLES i
		IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[i])
			vehIndex = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[i])
			
			IF DOES_ENTITY_EXIST(vehIndex)
			AND IS_VEHICLE_DRIVEABLE(vehIndex)
				IF CAN_PLACED_VEHICLE_BE_TARGETTED_BY_ORBITAL_CANNON(vehIndex, i)
					IF HAS_STREAMED_TEXTURE_DICT_LOADED("helicopterhud")
						DRAW_BOX_AROUND_TARGET_VEHICLE(vehIndex, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].sVehBlipStruct.iBlipSpriteOverride > 0)
						DRAW_OFFSCREEN_ARROW_FOR_COORD_VEHICLE(vehIndex)
						IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].sVehBlipStruct.iBlipSpriteOverride > 0
							DRAW_TARGET_LETTER_FOR_COORD_VEHICLE(vehIndex, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].sVehBlipStruct.iBlipSpriteOverride)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

FUNC VECTOR GET_LOCATION_OF_ORBITAL_CANNON_TERMINAL(INT &iIndex)
	
	INT i
	REPEAT FMMC_MAX_NUM_OBJECTS i
		IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niObject[i])
			IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iOrbitalCannonIndex > -1
				iIndex = g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iOrbitalCannonIndex
				RETURN g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].vPos
			ENDIF
		ENDIF
	ENDREPEAT
	
	iIndex = -1
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

PROC CLEAN_UP_FLARE_SFX_FOR_THIS_PROP(INT iProp)
	INT i
	FOR i = 0 TO iNumCreatedFlarePTFX-1
		IF FlareSoundIDPropNum[i] = iProp
			IF FlareSoundID[i] > -1
				PRINTLN("[RCC MISSION] - Cleaning up flare sound index: ", FlareSoundID[i])
				IF NOT HAS_SOUND_FINISHED(FlareSoundID[i])
					STOP_SOUND(FlareSoundID[i])
				ENDIF
				RELEASE_SOUND_ID(FlareSoundID[i])				
			ENDIF
			BREAKLOOP
		ENDIF
	ENDFOR
ENDPROC

FUNC INT GET_STARTING_PLAYERS_FOR_TEAM(INT iTeam)
	INT i
	INT iCount
	FOR i = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		IF MC_playerBD[i].iTeam = iTeam
			IF (NOT IS_BIT_SET(MC_playerBD[i].iClientBitSet,PBBOOL_ANY_SPECTATOR) OR IS_BIT_SET(MC_playerBD[i].iClientBitSet,PBBOOL_HEIST_SPECTATOR))
			AND NOT IS_BIT_SET(MC_playerBD[i].iClientBitSet,PBBOOL_PLAYER_FAIL)
				PARTICIPANT_INDEX tempPart = INT_TO_PARTICIPANTINDEX(i)
				IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
					PLAYER_INDEX tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
					IF NOT IS_PLAYER_SCTV(tempPlayer)
						iCount++
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	RETURN iCount
ENDFUNC

PROC PROCESS_SILO_ALARM()
	IF IS_THIS_ROCKSTAR_MISSION_GANGOPS_MISSION_OF_TYPE(ciGANGOPS_FLOW_MISSION_MISSILE_SILO_FINALE_P2)		
		INT iTeam = MC_playerBD[iPartToUse].iteam
		INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
		
		IF iRule < FMMC_MAX_RULES
			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetEleven[iRule], ciBS_RULE11_SILO_NUKE_ALARM)
				
				IF NOT IS_BIT_SET(g_sTransitionSessionData.sStrandMissionData.iBitSet, ciSTRAND_BITSET_STRAND_MISSION_SILO_ACTIVATE_ALARM)
					SET_BIT(g_sTransitionSessionData.sStrandMissionData.iBitSet, ciSTRAND_BITSET_STRAND_MISSION_SILO_ACTIVATE_ALARM)
					CLEAR_BIT(g_sTransitionSessionData.sStrandMissionData.iBitSet, ciSTRAND_BITSET_STRAND_MISSION_SILO_DEACTIVATE_ALARM)
					
					PRINTLN("[TMS][SiloAlarm] start power-up sound")				
				ENDIF
				
			ELSE
				IF IS_BIT_SET(g_sTransitionSessionData.sStrandMissionData.iBitSet, ciSTRAND_BITSET_STRAND_MISSION_SILO_ACTIVATE_ALARM)
					SET_BIT(g_sTransitionSessionData.sStrandMissionData.iBitSet, ciSTRAND_BITSET_STRAND_MISSION_SILO_DEACTIVATE_ALARM)
					CLEAR_BIT(g_sTransitionSessionData.sStrandMissionData.iBitSet, ciSTRAND_BITSET_STRAND_MISSION_SILO_ACTIVATE_ALARM)
				
					PRINTLN("[TMS][SiloAlarm] stop power-up sound")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_MISSION_ORBITAL_CANNON_FOR_CREATOR()	
	IF NOT bHasLaunchedFakeMissionOrbitalCannon
		IF (IS_THIS_ROCKSTAR_MISSION_GANGOPS_MISSION_OF_TYPE(ciGANGOPS_FLOW_MISSION_MISSILE_SILO_FINALE_P2) OR g_bMissionPlacedOrbitalCannon)
		AND (GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<258.5167, 6123.9121, -160.4223>>) < 30.0 OR g_bMissionPlacedOrbitalCannon)		 
			PRINTLN("[AM_MP_ORBITAL_CANNON] - MAINTAIN_MISSION_ORBITAL_CANNON - Requesting")
			
			REQUEST_SCRIPT("AM_MP_ORBITAL_CANNON")
			
			IF HAS_SCRIPT_LOADED("AM_MP_ORBITAL_CANNON")
				PRINTLN("[AM_MP_ORBITAL_CANNON] - MAINTAIN_MISSION_ORBITAL_CANNON - Launching")
				
				START_NEW_SCRIPT("AM_MP_ORBITAL_CANNON", DEFAULT_STACK_SIZE)
				SET_SCRIPT_AS_NO_LONGER_NEEDED("AM_MP_ORBITAL_CANNON")
				bHasLaunchedFakeMissionOrbitalCannon = TRUE
			ELSE
				PRINTLN("[AM_MP_ORBITAL_CANNON] - MAINTAIN_MISSION_ORBITAL_CANNON - Script not loaded")
			ENDIF
		ENDIF
	ELSE
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH ("AM_MP_ORBITAL_CANNON")) = 0
		AND bHasLaunchedFakeMissionOrbitalCannon
		AND (GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<258.5167, 6123.9121, -160.4223>>) > 30 AND NOT g_bMissionPlacedOrbitalCannon)		
			bHasLaunchedFakeMissionOrbitalCannon = FALSE
			
			PRINTLN("[AM_MP_ORBITAL_CANNON] - MAINTAIN_MISSION_ORBITAL_CANNON - Resetting")
		ENDIF
	ENDIF
ENDPROC

FUNC INT GET_LOCAL_TEAM_PERSONAL_LOCATE_TOTAL()
	INT i, iReturn
	FOR i = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		IF MC_playerBD[i].iteam = MC_playerBD[iLocalPart].iteam
			iReturn += MC_playerBD[i].iPersonalLocatePoints
		ENDIF
	ENDFOR
	
	RETURN iReturn
ENDFUNC

FUNC STRING GET_SCRIPTED_CUTSCENE_STATE_PROGRESS_NAME(eFMMC_SCRIPTED_CUTSCENE_PROGRESS eState)
	SWITCH eState
		CASE	SCRIPTEDCUTPROG_INIT					RETURN	"SCRIPTEDCUTPROG_INIT (0)"
		CASE	SCRIPTEDCUTPROG_WAIT_PLAYER_SELECTION	RETURN	"SCRIPTEDCUTPROG_WAIT_PLAYER_SELECTION (1)"
		CASE	SCRIPTEDCUTPROG_FADEOUT					RETURN	"SCRIPTEDCUTPROG_FADEOUT (2)"
		CASE	SCRIPTEDCUTPROG_FADEOUT_AND_WARP		RETURN	"SCRIPTEDCUTPROG_FADEOUT_AND_WARP (2)"
		CASE	SCRIPTEDCUTPROG_WARPINTRO				RETURN	"SCRIPTEDCUTPROG_WARPINTRO (3)"
		CASE	SCRIPTEDCUTPROG_PROCESS_SHOTS			RETURN	"SCRIPTEDCUTPROG_PROCESS_SHOTS (4)"
		CASE	SCRIPTEDCUTPROG_WARPEND_SHORT			RETURN	"SCRIPTEDCUTPROG_WARPEND_SHORT (5)"
		CASE	SCRIPTEDCUTPROG_WARPEND_LONG			RETURN	"SCRIPTEDCUTPROG_WARPEND_LONG (6)"
		CASE	SCRIPTEDCUTPROG_ENDING_PROCEDURES		RETURN	"SCRIPTEDCUTPROG_ENDING_PROCEDURES (7)"
		CASE	SCRIPTEDCUTPROG_CLEANUP					RETURN	"SCRIPTEDCUTPROG_CLEANUP (8)"
	ENDSWITCH
	
	RETURN "invalid"
ENDFUNC

FUNC eFMMC_SCRIPTED_CUTSCENE_PROGRESS GET_SCRIPTED_CUTSCENE_STATE_PROGRESS()
	RETURN eScriptedCutsceneProgress
ENDFUNC

PROC SET_SCRIPTED_CUTSCENE_STATE_PROGRESS(eFMMC_SCRIPTED_CUTSCENE_PROGRESS eNewState)
	PRINTLN("[LM][SET_SCRIPTED_CUTSCENE_STATE_PROGRESS][PROCESS_SCRIPTED_CUTSCENE] - Setting Scripted Cutscene State from ", 
	GET_SCRIPTED_CUTSCENE_STATE_PROGRESS_NAME(eScriptedCutsceneProgress), " to ", GET_SCRIPTED_CUTSCENE_STATE_PROGRESS_NAME(eNewState))
	
	DEBUG_PRINTCALLSTACK()
	eScriptedCutsceneProgress = eNewState
ENDPROC


FUNC STRING GET_MOCAP_MANAGE_CUTSCENE_STATE_PROGRESS_NAME(eFMMC_MOCAP_PROGRESS eState)
	SWITCH eState
		CASE	MOCAPPROG_INIT						RETURN	"MOCAPPROG_INIT (0)"
		CASE	MOCAPPROG_WAIT_PLAYER_SELECTION		RETURN	"MOCAPPROG_WAIT_PLAYER_SELECTION (1)"
		CASE	MOCAPPROG_FADEOUT					RETURN	"MOCAPPROG_FADEOUT (2)"
		CASE	MOCAPPROG_INTRO						RETURN	"MOCAPPROG_INTRO (3)"
		CASE	MOCAPPROG_APARTMENT					RETURN	"MOCAPPROG_APARTMENT (4)"
		CASE	MOCAPPROG_PROCESS_SHOTS				RETURN	"MOCAPPROG_PROCESS_SHOTS (5)"
		CASE	MOCAPPROG_WARPEND					RETURN	"MOCAPPROG_WARPEND (6)"
		CASE 	MOCAPPROG_ENDING_PROCEDURES			RETURN	"MOCAPPROG_ENDING_PROCEDURES (7)"
		CASE	MOCAPPROG_CLEANUP					RETURN	"MOCAPPROG_CLEANUP (8)"	
	ENDSWITCH
	
	RETURN "invalid"
ENDFUNC

FUNC eFMMC_MOCAP_PROGRESS GET_MOCAP_MANAGE_CUTSCENE_STATE_PROGRESS()
	RETURN eMocapManageCutsceneProgress
ENDFUNC

PROC SET_MOCAP_MANAGE_CUTSCENE_STATE_PROGRESS(eFMMC_MOCAP_PROGRESS eNewState)
	PRINTLN("[LM][SET_MOCAP_MANAGE_CUTSCENE_STATE_PROGRESS][PROCESS_MOCAP_CUTSCENE] - Setting Mocap Manage State from ", 
	GET_MOCAP_MANAGE_CUTSCENE_STATE_PROGRESS_NAME(eMocapManageCutsceneProgress), " to ", GET_MOCAP_MANAGE_CUTSCENE_STATE_PROGRESS_NAME(eNewState))
	
	DEBUG_PRINTCALLSTACK()
	eMocapManageCutsceneProgress = eNewState
ENDPROC

FUNC STRING GET_MOCAP_RUNNING_CUTSCENE_STATE_PROGRESS_NAME(eFMMC_MOCAP_RUNNING_PROGRESS eState)
	SWITCH eState
		CASE	MOCAPRUNNING_STAGE_0			RETURN	"MOCAPRUNNING_STAGE_0"
		CASE	MOCAPRUNNING_STAGE_1			RETURN	"MOCAPRUNNING_STAGE_1"
		CASE	MOCAPRUNNING_STAGE_2			RETURN	"MOCAPRUNNING_STAGE_2"
		CASE	MOCAPRUNNING_STAGE_3			RETURN	"MOCAPRUNNING_STAGE_3"
		CASE	MOCAPRUNNING_STAGE_4			RETURN	"MOCAPRUNNING_STAGE_4"
		CASE	MOCAPRUNNING_STAGE_5			RETURN	"MOCAPRUNNING_STAGE_5"
		CASE	MOCAPRUNNING_STAGE_6			RETURN	"MOCAPRUNNING_STAGE_6"	
	ENDSWITCH
	
	RETURN "invalid"
ENDFUNC

FUNC eFMMC_MOCAP_RUNNING_PROGRESS GET_MOCAP_RUNNING_CUTSCENE_STATE_PROGRESS()
	RETURN eMocapRunningCutsceneProgress
ENDFUNC

PROC SET_MOCAP_RUNNING_CUTSCENE_STATE_PROGRESS(eFMMC_MOCAP_RUNNING_PROGRESS eNewState)
	PRINTLN("[LM][SET_MOCAP_RUNNING_CUTSCENE_STATE_PROGRESS][RUN_MOCAP_CUTSCENE] - Setting Mocap Running State from ", 
	GET_MOCAP_RUNNING_CUTSCENE_STATE_PROGRESS_NAME(eMocapRunningCutsceneProgress), " to ", GET_MOCAP_RUNNING_CUTSCENE_STATE_PROGRESS_NAME(eNewState))
	
	DEBUG_PRINTCALLSTACK()
	eMocapRunningCutsceneProgress = eNewState
ENDPROC

PROC KEEP_FORCE_UNDER_RECOMMENDED_LIMIT(VECTOR& vForceToCheck)
	IF vForceToCheck.x > 150
		PRINTLN("[RCC MISSION] KEEP_FORCE_UNDER_RECOMMENDED_LIMIT - Capping X force to 145")
		vForceToCheck.x = 145
	ENDIF
	IF vForceToCheck.y > 150
		PRINTLN("[RCC MISSION] KEEP_FORCE_UNDER_RECOMMENDED_LIMIT - Capping Y force to 145")
		vForceToCheck.y = 145
	ENDIF
	IF vForceToCheck.z > 150
		PRINTLN("[RCC MISSION] KEEP_FORCE_UNDER_RECOMMENDED_LIMIT - Capping Z force to 145")
		vForceToCheck.z = 145
	ENDIF
ENDPROC

PROC PLAY_HOSTILE_TAKEOVER_SOUND(eCaptureLocateStateEnum eLocateState, INT iLoc, INT iTeamWhoOwnThis, STRING sSoundSet)
	PRINTLN("[RCC MISSION] HT SOUNDS - Calling")
	SWITCH eLocateState
	
		CASE eCapture_captured
			IF iTeamWhoOwnThis != -1
				IF iLocPreContestOwnerTeam[iLoc] != iTeamWhoOwnThis
					IF iTeamWhoOwnThis = MC_PlayerBD[iPartToUse].iteam
						IF MC_serverBD_2.playerLocOwner[iLoc][MC_PlayerBD[iPartToUse].iteam] = PlayerToUse
							PLAY_SOUND_FRONTEND(-1,"Zone_Captured",sSoundSet,FALSE)
							PRINTLN("[RCC MISSION] HT SOUNDS - Playing Local Capture sound for zone ", iLoc)
						ELSE
							PLAY_SOUND_FRONTEND(-1,"Zone_Captured_Remote",sSoundSet,FALSE)
							PRINTLN("[RCC MISSION] HT SOUNDS - Playing Remote Capture sound for zone ", iLoc)
						ENDIF
					ELSE
						PLAY_SOUND_FRONTEND(-1,"Zone_Lost",sSoundSet,FALSE)
						PRINTLN("[RCC MISSION] HT SOUNDS - Playing Lose sound for zone ", iLoc)
					ENDIF
				ENDIF
				iColourTeamCacheGotoLocateOld[iLoc] = iTeamWhoOwnThis
				PRINTLN("[RCC MISSION] HT SOUNDS - Setting iColourTeamCacheGotoLocateOld[iLoc] to ", iColourTeamCacheGotoLocateOld[iLoc])
			ENDIF
		BREAK
		
		CASE eCapture_contested
		
			IF MC_playerBD[iPartToUse].iCurrentLoc = iLoc
			AND eCaptureLocateStateLastFrame[iLoc] != eCapture_contested
				PLAY_SOUND_FRONTEND(-1,"Zone_Contested",sSoundSet,FALSE)
				PRINTLN("[RCC MISSION] HT SOUNDS - Contested sound.")
			ENDIF
			PRINTLN("[RCC MISSION] HT SOUNDS - zone ", iLoc," is contested")
		BREAK
	ENDSWITCH
ENDPROC

FUNC SIMPLE_INTERIORS GET_DEFUNCT_BASE_SIMPLE_INTERIOR_FROM_BASE_ID(DEFUNCT_BASE_ID eDefunctBaseID)
	SWITCH eDefunctBaseID
		CASE DEFUNCT_BASE_1 RETURN SIMPLE_INTERIOR_DEFUNCT_BASE_1
		CASE DEFUNCT_BASE_2 RETURN SIMPLE_INTERIOR_DEFUNCT_BASE_2
		CASE DEFUNCT_BASE_3 RETURN SIMPLE_INTERIOR_DEFUNCT_BASE_3
		CASE DEFUNCT_BASE_4 RETURN SIMPLE_INTERIOR_DEFUNCT_BASE_4
		CASE DEFUNCT_BASE_6 RETURN SIMPLE_INTERIOR_DEFUNCT_BASE_6
		CASE DEFUNCT_BASE_7 RETURN SIMPLE_INTERIOR_DEFUNCT_BASE_7
		CASE DEFUNCT_BASE_8 RETURN SIMPLE_INTERIOR_DEFUNCT_BASE_8
		CASE DEFUNCT_BASE_9 RETURN SIMPLE_INTERIOR_DEFUNCT_BASE_9
		CASE DEFUNCT_BASE_10 RETURN SIMPLE_INTERIOR_DEFUNCT_BASE_10
	ENDSWITCH
	RETURN SIMPLE_INTERIOR_INVALID
ENDFUNC

FUNC FLOAT GET_CURRENT_PLAYER_FORWARD_SPEED()
	IF DOES_ENTITY_EXIST(LocalPlayerPed)
	AND IS_ENTITY_ALIVE(LocalPlayerPed)
		RETURN GET_ENTITY_SPEED(LocalPlayerPed)
	ELSE
		RETURN 0.0
	ENDIF
ENDFUNC

FUNC FLOAT GET_SHOWDOWN_DRAIN_AMOUNT()

	FLOAT fDamage = g_FMMC_STRUCT.fShowdown_TickDrainSpeed
	
	IF GET_NUMBER_OF_PLAYING_PLAYERS() = 2
		fDamage = g_FMMC_STRUCT.fShowdown_TickDrainSpeed_TwoPlayers
	ENDIF
	
	IF ABSF(GET_CURRENT_PLAYER_FORWARD_SPEED()) <= cfShowdown_IdleSpeed
	#IF IS_DEBUG_BUILD
	AND NOT bShowdown_DisableIdleDrain
	#ENDIF
		IF NOT HAS_NET_TIMER_STARTED(stShowdown_IdleTimer)
			REINIT_NET_TIMER(stShowdown_IdleTimer)
		ELSE
			IF HAS_NET_TIMER_EXPIRED(stShowdown_IdleTimer, ciShowdown_IdleTimeAllowed)
				fDamage += g_FMMC_STRUCT.fShowdown_IdleTickDamage
			ENDIF
		ENDIF
	ELSE
		RESET_NET_TIMER(stShowdown_IdleTimer)
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF bShowdown_DisableDrain
		PRINTLN("[SHOWDOWN] GET_SHOWDOWN_DRAIN_AMOUNT returning 0.0 due to bShowdown_DisableDrain")
		RETURN 0.0
	ENDIF
	#ENDIF
	
	RETURN fDamage
ENDFUNC

FUNC VISUAL_AID_MODES GET_PLAYER_VISUAL_AID_MODE()
	IF GET_USINGNIGHTVISION()
		RETURN VISUALAID_NIGHT
	ELIF GET_USINGSEETHROUGH()
		RETURN VISUALAID_THERMAL
	ELSE
		RETURN VISUALAID_OFF
	ENDIF
ENDFUNC

PROC VEHICULAR_VENDETTA_SET_LOCAL_PLAYER_AS_GHOST(BOOL bActivate)
	SET_LOCAL_PLAYER_AS_GHOST(bActivate, TRUE)	
	IF bActivate = TRUE
		SET_GHOST_ALPHA(50)
	ELSE
		RESET_GHOST_ALPHA()
	ENDIF
ENDPROC
PROC VEHICULAR_VENDETTA_SET_GHOSTED_PLAYER_ALPHA(BOOL bActivate)
	IF bActivate = TRUE
		SET_GHOST_ALPHA(50)
	ELSE
		RESET_GHOST_ALPHA()
	ENDIF
ENDPROC

PROC PROCESS_PLAYER_RESET_SPAWN_PROTECTION()
	IF IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_ANY_SPECTATOR)
		EXIT
	ENDIF
	IF HAS_NET_TIMER_STARTED(tdDefenseSphereInvulnerabilityTimer)		
		IF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdDefenseSphereInvulnerabilityTimer, (g_FMMC_STRUCT.iSpawnProtectionDuration*100))
			PRINTLN("[LM][PROCESS_RESTRICTION_ZONES](PROCESS_PLAYER_RESET_SPAWN_PROTECTION) - Resetting Ghost and Invincibility.")
			IF NOT IS_PED_INJURED(localPlayerPed)
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_HOSTILE_TAKEOVER(g_FMMC_STRUCT.iAdversaryModeType) //Keeping it in a check incase it will have any effect on old content.
					IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
						IF IS_ENTITY_ALIVE(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
							SET_ENTITY_ALPHA(GET_VEHICLE_PED_IS_IN(LocalPlayerPed), 255, TRUE)
						ENDIF
					ENDIF
				ENDIF
				SET_ENTITY_INVINCIBLE(localPlayerPed, FALSE)
				VEHICULAR_VENDETTA_SET_LOCAL_PLAYER_AS_GHOST(FALSE)
				
				BROADCAST_FMMC_SPAWN_PROTECTION_ALPHA(FALSE, iLocalPart, GET_FRAME_COUNT())
			ENDIF
			RESET_NET_TIMER(tdDefenseSphereInvulnerabilityTimer)
		ENDIF
	ENDIF	
ENDPROC

FUNC CONTROL_ACTION GET_BOMBDROP_BUTTON()
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_DROP_THE_BOMB(g_FMMC_STRUCT.iAdversaryModeType)
		RETURN INPUT_VEH_AIM
	ENDIF
	
	RETURN INPUT_FRONTEND_Y
ENDFUNC

FUNC BOOL HAVE_ALL_PLAYERS_BEEN_ON_SMALLEST_TEAM()
	INT iParticipant
	PARTICIPANT_INDEX piTemp
	PLAYER_INDEX piPlayer
	
	INT iPartMax = GET_NUMBER_OF_PLAYING_PLAYERS()
	INT iCount
	
	FOR iParticipant = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		piTemp = INT_TO_PARTICIPANTINDEX(iParticipant)
		
		IF NETWORK_IS_PARTICIPANT_ACTIVE(piTemp)
			piPlayer = NETWORK_GET_PLAYER_INDEX(piTemp)	

			IF NOT IS_PLAYER_SCTV(piPlayer)
			AND NOT DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(piPlayer)
				IF IS_BIT_SET(GlobalplayerBD_FM_2[NATIVE_TO_INT(piPlayer)].iMissionDataBitSetTwo, ciMISSION_DATA_TWO_HAS_JOINED_SMALLEST_TEAM)
					iCount++
					PRINTLN("HAVE_ALL_PLAYERS_BEEN_ON_SMALLEST_TEAM - iCount: ", iCount, "/",iPartMax)
				ENDIF
				
				IF iCount >= iPartMax
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN FALSE

ENDFUNC

FUNC BOOL IS_RUNNING_BACK_REMIX_SUDDEN_DEATH()
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_RUNNING_BACK_REMIX(g_FMMC_STRUCT.iAdversaryModeType)
		IF MC_ServerBD_4.iTeamLapsCompleted[0] >= g_FMMC_STRUCT.sFMMCEndConditions[0].iMissionLapLimit
		AND MC_ServerBD_4.iTeamLapsCompleted[1] >= g_FMMC_STRUCT.sFMMCEndConditions[1].iMissionLapLimit
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CAN_MODEL_BE_SUPPRESSED(MODEL_NAMES mn)
	INT iModel	
	FOR iModel = 0 TO g_iNumSupressed
		IF mnSuppressedVehicles[iModel] = mn
			RETURN FALSE
		ENDIF
	ENDFOR
	RETURN TRUE
ENDFUNC

PROC VALIDATE_NUMBER_MODELS_SUPRESSED(INT iAdd)
	g_iNumSupressed += iAdd	
	IF g_iNumSupressed < 0
		g_iNumSupressed = 0
	ENDIF
	#IF IS_DEBUG_BUILD
		IF g_iNumSupressed >= MAX_NUM_SUPRESSED_MODELS
			ASSERTLN(" No space left to add another model in SET_VEHICLE_MODEL_IS_SUPPRESSED CURRENT: ",g_iNumSupressed," MAX: ",MAX_NUM_SUPRESSED_MODELS)
		ENDIF
	#ENDIF
ENDPROC

PROC CLEANUP_DYNAMIC_FLARE(INT i)	
	IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxFlareIndex[i])
		STOP_PARTICLE_FX_LOOPED(ptfxFlareIndex[i])
	ENDIF
	flare_data[i].iFlareState = FLARE_STATE_INIT
	flare_data[i].fNextRed = 0
	flare_data[i].FNextGreen = 0
	flare_data[i].fNextBlue = 0 
	flare_data[i].fRed = 0.0
	flare_data[i].fGreen = 0.0
	flare_data[i].fBlue = 0.0
	flare_data[i].iPTFXval = -1
	
	corona_flare_data[i].iFlareState = FLARE_STATE_INIT
	corona_flare_data[i].fNextRed = 0
	corona_flare_data[i].FNextGreen = 0
	corona_flare_data[i].fNextBlue = 0 
	corona_flare_data[i].fRed = 0.0
	corona_flare_data[i].fGreen = 0.0
	corona_flare_data[i].fBlue = 0.0
	corona_flare_data[i].iPTFXval = -1
	PRINTLN("[RCC MISSION][AW SMOKE] CLEANUP_DYNAMIC_FLARE |   ", i)
	PRINTLN("[RCC MISSION][AW SMOKE] CLEANUP_DYNAMIC_FLARE (CORONA) |   ", i)
	
	iNumCreatedDynamicFlarePTFX--

ENDPROC

PROC CLEANUP_DYNAMIC_FLARES()
	INT i	
	FOR i = 0 TO ciTOTAL_PTFX -1
		CLEANUP_DYNAMIC_FLARE(i)
	ENDFOR
	
	FOR i = 0 TO ((GET_FMMC_MAX_NUM_PROPS() / 32) + 1)-1
		iCreatedFlaresBitset[i] = 0
	ENDFOR
	
	iNumCreatedDynamicFlarePTFX = 0
	iNumCreatedFlarePTFX = 0
ENDPROC

FUNC BOOL IS_THERE_A_PLACED_PED_IN_THIS_VEHICLE(VEHICLE_INDEX viVehicleToCheck)

	IF DOES_ENTITY_EXIST(viVehicleToCheck)
		IF NOT IS_VEHICLE_EMPTY(viVehicleToCheck)
			INT iPlacedPed
			FOR iPlacedPed = 0 TO MC_serverBD.iNumPedCreated - 1
				PED_INDEX piVehiclePed 
				IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[iPlacedPed])
					piVehiclePed = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iPlacedPed])
				ENDIF
				
				IF DOES_ENTITY_EXIST(piVehiclePed)
					IF IS_PED_IN_THIS_VEHICLE(piVehiclePed, viVehicleToCheck)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDFOR
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC INT GET_PLACED_PED_IN_VEHICLE(VEHICLE_INDEX viVehicleToCheck)

	IF DOES_ENTITY_EXIST(viVehicleToCheck)
		IF NOT IS_VEHICLE_EMPTY(viVehicleToCheck)
			INT iPlacedPed
			FOR iPlacedPed = 0 TO MC_serverBD.iNumPedCreated - 1
				PED_INDEX piVehiclePed 
				IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[iPlacedPed])
					piVehiclePed = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iPlacedPed])
				ENDIF
				
				IF DOES_ENTITY_EXIST(piVehiclePed)
					IF IS_PED_IN_THIS_VEHICLE(piVehiclePed, viVehicleToCheck)
						RETURN iPlacedPed
					ENDIF
				ENDIF
			ENDFOR
		ENDIF
	ENDIF
	
	RETURN -1
ENDFUNC

FUNC BOOL DOES_MODE_USE_TEAM_CLOBBER_AND_NOT_WANT_TO_END()
	IF (IS_THIS_ROCKSTAR_MISSION_NEW_VS_JUGGERNAUT(g_FMMC_STRUCT.iAdversaryModeType) 
	OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_TRADING_PLACES_REMIX(g_FMMC_STRUCT.iAdversaryModeType))
	AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciCLOBBER_PLAYER_ON_TEAM_SWAP)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_ANY_PLAYERS_ON_TEAM_BE_CONSIDERED_WANTED(INT iTeamToCheck)
	INT iPart
	INT iNumberOfPlayersChecked = 0
	INT iNumberOfPlayers = (MC_serverBD.iNumberOfPlayingPlayers[0] + MC_serverBD.iNumberOfPlayingPlayers[1] + MC_serverBD.iNumberOfPlayingPlayers[2] + MC_serverBD.iNumberOfPlayingPlayers[3]) 
	
	FOR iPart = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		IF ((NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_ANY_SPECTATOR)) OR IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_HEIST_SPECTATOR))
		AND NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet,PBBOOL_PLAYER_FAIL)
		AND IS_BIT_SET(MC_playerBD[iPart].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
		AND NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_FINISHED)
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPart))
				iNumberOfPlayersChecked++
				
				IF MC_playerBD[iPart].iteam = iTeamToCheck
					IF IS_BIT_SET(MC_playerBD[iPart].iClientBitSet4, PBBOOL4_TREAT_AS_WANTED)
						PRINTLN("SHOULD_ANY_PLAYERS_ON_TEAM_BE_CONSIDERED_WANTED - Participant ", iPart, " is considered Wanted!")
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
			
			IF iNumberOfPlayersChecked >= iNumberOfPlayers
				BREAKLOOP
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC

FUNC BOOL ARE_ANY_PLAYERS_ON_TEAM_DEAD(INT iTeamToCheck)
	INT iPart
	INT iNumberOfPlayersChecked = 0
	INT iNumberOfPlayers = (MC_serverBD.iNumberOfPlayingPlayers[0] + MC_serverBD.iNumberOfPlayingPlayers[1] + MC_serverBD.iNumberOfPlayingPlayers[2] + MC_serverBD.iNumberOfPlayingPlayers[3]) 
	
	FOR iPart = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		IF ((NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_ANY_SPECTATOR)) OR IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_HEIST_SPECTATOR))
		AND NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet,PBBOOL_PLAYER_FAIL)
		AND IS_BIT_SET(MC_playerBD[iPart].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
		AND NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_FINISHED)
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPart))
				iNumberOfPlayersChecked++
				
				IF MC_playerBD[iPart].iteam = iTeamToCheck
					PLAYER_INDEX piTemp = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPart))
					PED_INDEX pedTemp = GET_PLAYER_PED(piTemp)					
					
					IF IS_PLAYER_DEAD(piTemp)
					OR IS_PED_DEAD_OR_DYING(pedTemp)
					OR IS_PED_INJURED(pedTemp)
					OR (IS_PLAYER_RESPAWNING(piTemp) AND NOT IS_FAKE_MULTIPLAYER_MODE_SET(TRUE))
						PRINTLN("ARE_ANY_PLAYERS_ON_TEAM_DEAD - Participant ", iPart, " is dead!")
						RETURN TRUE
					ELSE
					ENDIF
				ENDIF
			ENDIF
			
			IF iNumberOfPlayersChecked >= iNumberOfPlayers
				BREAKLOOP
			ENDIF
		ELSE
			PRINTLN("ARE_ANY_PLAYERS_ON_TEAM_DEAD - Participant ", iPart, " is Spectator or Fail!")
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC

PROC SET_RUNNING_BACK_VEHICLE_DESTROYED_FOR_SHARD(INT iVeh)
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_RUNNING_BACK_REMIX(g_FMMC_STRUCT.iAdversaryModeType)
		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh])
		AND NOT IS_BIT_SET(MC_ServerBD.iServerBitSet7, SBBOOL7_SHOULD_PLAY_RUNNING_BACK_REMIX_BLOCKED)
		AND GET_PLAYER_RESPAWN_BACK_AT_START_STATE(iLocalPart) = PRBSS_PlayingNormally			
			PRINTLN("[RCC MISSION][PROCESS_VEH_BRAIN][LM][PROCESS_RUNNING_BACK_SHARDS] - SBBOOL7_SHOULD_PLAY_RUNNING_BACK_REMIX_BLOCKED - Setting bit. (Dead)")
			SET_BIT(MC_ServerBD.iServerBitSet7, SBBOOL7_SHOULD_PLAY_RUNNING_BACK_REMIX_BLOCKED)
			SET_BIT(MC_ServerBD.iServerBitSet7, SBBOOL7_SHOULD_PLAY_RUNNING_BACK_REMIX_DESTROYED)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_THIS_BOMB_FOOTBALL()
	//RETURN IS_THIS_ROCKSTAR_MISSION_NEW_VS_BOMB_FOOTBALL(g_FMMC_STRUCT.iAdversaryModeType) //TEMP until ciOptionsBS19_BOMB_FOOTBALL_FUNCTIONALITY is turned on in all bomb football modes
	RETURN IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNineteen, ciOptionsBS19_BOMB_FOOTBALL_FUNCTIONALITY)
ENDFUNC

FUNC OBJECT_INDEX GET_BOMB_FOOTBALL_BOMB(INT i)
	
	IF i > -1
	AND i < FMMC_MAX_NUM_OBJECTS
		IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niObject[i])
			RETURN NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[i])
		ENDIF
	ENDIF
	
	RETURN NULL
ENDFUNC

FUNC STRING GET_BOMB_EXPLOSION_SOUND_EFFECT()
	
	SWITCH g_FMMC_STRUCT.sArenaInfo.iArena_Theme
		CASE ARENA_THEME_DYSTOPIAN				RETURN "Apoc_Bomb_Explode"
		CASE ARENA_THEME_SCIFI					RETURN "Future_Bomb_Explode"
		CASE ARENA_THEME_CONSUMERWASTELAND		RETURN "Consumer_Bomb_Explode"
	ENDSWITCH
	
	RETURN "INVALID"
ENDFUNC

PROC EXPLODE_BOMB_FOOTBALL_BOMB(INT i, BOOL bEffectsOnly = FALSE)

	OBJECT_INDEX oiBombObj = GET_BOMB_FOOTBALL_BOMB(i)

	IF DOES_ENTITY_EXIST(oiBombObj)
		PRINTLN("[BMBFB]", "[BMB", i, "] EXPLODE_BOMB_FOOTBALL_BOMB - Exploding football ", i, "!")
		
		USE_PARTICLE_FX_ASSET("scr_xs_props")
		START_NETWORKED_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_xs_ball_explosion", GET_ENTITY_COORDS(oiBombObj), <<0.0, 0.0, 0.0>>)
		
		PLAY_SOUND_FROM_COORD(-1, GET_BOMB_EXPLOSION_SOUND_EFFECT(), GET_ENTITY_COORDS(oiBombObj), "DLC_AW_BB_Sounds", TRUE, 550)
		
		SET_ENTITY_HEALTH(oiBombObj, 0, LocalPlayerPed)
		
		IF NOT bEffectsOnly
			NETWORK_INDEX niBomb = OBJ_TO_NET(oiBombObj)
			DELETE_NET_ID(niBomb)
		ENDIF
	ENDIF
ENDPROC

PROC RESET_BOMB_OBJECT()
	OBJECT_INDEX oiBombObj = GET_BOMB_FOOTBALL_BOMB()
	
	IF oiBombObj = NULL
		EXIT
	ENDIF
	
	IF NETWORK_HAS_CONTROL_OF_ENTITY(oiBombObj)
		SET_ENTITY_COORDS(oiBombObj, g_FMMC_STRUCT_ENTITIES.sPlacedObject[0].vPos)
		SET_ENTITY_VISIBLE(oiBombObj, TRUE)
	ENDIF
ENDPROC

FUNC BOOL IS_BOMB_READY_TO_PROCEED()

	OBJECT_INDEX oiBombObj = GET_BOMB_FOOTBALL_BOMB()
	
	IF oiBombObj = NULL	
		PRINTLN("[BMBFB] BOMB_IS_READY_TO_PROCEED - Returning FALSE because of oiBombObj being null")
		RETURN FALSE
	ENDIF
	
	IF NOT IS_ENTITY_AT_COORD(oiBombObj, g_FMMC_STRUCT_ENTITIES.sPlacedObject[0].vPos, <<1.0, 1.0, 1.0>>)
		PRINTLN("[BMBFB] BOMB_IS_READY_TO_PROCEED - Returning FALSE because of coord check")
		RETURN FALSE
	ENDIF
	
	IF IS_ENTITY_VISIBLE(oiBombObj)
		PRINTLN("[BMBFB] BOMB_IS_READY_TO_PROCEED - Returning FALSE because of visibility")
		RETURN FALSE
	ENDIF
	
	PRINTLN("[BMBFB] BOMB_IS_READY_TO_PROCEED - Returning TRUE")
	RETURN TRUE
ENDFUNC

FUNC INT GET_LAST_PART_TO_HIT_FOOTBALL(INT iBall)
	
	INT iResult = -1
	INT iCurrentClosestTimeStamp = -SCRIPT_MAX_INT32
	
	INT i
	
	FOR i = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		IF MC_playerBD_1[i].iLastBombFootballHitTimestamp[iBall] > iCurrentClosestTimeStamp
			iResult = i
			iCurrentClosestTimeStamp = MC_playerBD_1[i].iLastBombFootballHitTimestamp[iBall]
		ENDIF
	ENDFOR
	
	PRINTLN("[BMBFB TOUCH SPAM] GET_LAST_PART_TO_HIT_FOOTBALL - Frame ", GET_FRAME_COUNT(), " Returning participant ", iResult)
	RETURN iResult
ENDFUNC

FUNC INT GET_LAST_PART_TO_HIT_FOOTBALL_ON_TEAM(INT iFootball, INT iTeam)
	
	INT iResult = -1
	INT iCurrentClosestTimeStamp = (-SCRIPT_MAX_INT32) + 5
	
	INT i
	
	PRINTLN("[TMS] GET_LAST_PART_TO_HIT_FOOTBALL_ON_TEAM - Starting check for Bomb ", iFootball, " for team ", iTeam, " =========================================")
	
	FOR i = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		
		IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
			PRINTLN("GET_LAST_PART_TO_HIT_FOOTBALL_ON_TEAM - Player ", i, " isn't active")
			RELOOP
		ENDIF
		
		IF MC_playerBD[i].iteam != iTeam
			PRINTLN("GET_LAST_PART_TO_HIT_FOOTBALL_ON_TEAM - Player ", i, " isn't on the right team")
			RELOOP
		ENDIF
		
		IF MC_playerBD_1[i].iLastBombFootballHitTimestamp[iFootball] > iCurrentClosestTimeStamp
		AND MC_playerBD_1[i].iLastBombFootballHitTimestamp[iFootball] != 0 
			iResult = i
			iCurrentClosestTimeStamp = MC_playerBD_1[i].iLastBombFootballHitTimestamp[iFootball]
			PRINTLN("GET_LAST_PART_TO_HIT_FOOTBALL_ON_TEAM - Setting current closest to player ", i, " | iCurrentClosestTimeStamp: ", iCurrentClosestTimeStamp)
		ENDIF
	ENDFOR
	
	PRINTLN("[BMBFB TOUCH SPAM] GET_LAST_PART_TO_HIT_FOOTBALL_ON_TEAM - Frame ", GET_FRAME_COUNT(), " Returning participant ", iResult)
	PRINTLN("[TMS] GET_LAST_PART_TO_HIT_FOOTBALL_ON_TEAM - Ending =========================================")
	RETURN iResult
ENDFUNC

FUNC INT GET_TEAM_THAT_WILL_SCORE_FROM_BALL_EXPLODING(INT iBall)

	INT iTeam = 0
	
	FOR iTeam = 0 TO FMMC_MAX_TEAMS - 1
		IF IS_BIT_SET(MC_serverBD.iObjAtYourHolding[iTeam], iBall)
			PRINTLN("[BMBFB] GET_TEAM_THAT_WILL_SCORE_FROM_BALL_EXPLODING - Team ", iTeam, " is holding ball ", iBall)
			RETURN iTeam
		ENDIF
	ENDFOR
	
	PRINTLN("[BMBFB] GET_TEAM_THAT_WILL_SCORE_FROM_BALL_EXPLODING - Returning -1 because nobody is holding ball ", iBall)
	RETURN -1
ENDFUNC

PROC CLEAR_OBJECTIVE_TIMER_PENALTY()
	INT i 
	
	FOR i = 0 TO FMMC_MAX_TEAMS - 1
		MC_serverBD_3.iTimerPenalty[i] = 0
	ENDFOR
	
	PRINTLN("CLEAR_OBJECTIVE_TIMER_PENALTY - Setting iTimerPenalty to 0 for all teams")
ENDPROC

PROC ROUND_MULTIRULE_TIMER_TO_NEAREST_MINUTE(INT iTeam, BOOL bForceRoundUp = FALSE)

	PRINTLN("[TMSRM] ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
	PRINTLN("[TMSRM] Starting net timer difference: ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdMultiObjectiveLimitTimer[iTeam]))
	
	INT iCurTime = MC_serverBD_3.iMultiObjectiveTimeLimit[MC_playerBD[iPartToUse].iteam] - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdMultiObjectiveLimitTimer[iTeam])
	FLOAT fMinutes = TO_FLOAT(iCurTime) / 60000.0
	INT iRoundedTime
	
	IF bForceRoundUp
		iRoundedTime = CEIL(fMinutes) * 60000
	ELSE
		iRoundedTime = ROUND(fMinutes) * 60000
	ENDIF
	
	INT iDifferenceToAdd = MC_serverBD_3.iMultiObjectiveTimeLimit[MC_playerBD[iPartToUse].iteam] - iRoundedTime
	PRINTLN("[TMSRM] iCurTume: ", iCurTime, " | fMinutes: ", fMinutes, " | iRoundedTime: ", iRoundedTime, " | iDifferenceToAdd: ", iDifferenceToAdd)
	
	MC_serverBD_3.tdMultiObjectiveLimitTimer[iTeam].Timer = GET_TIME_OFFSET(GET_NETWORK_TIME(), -iDifferenceToAdd)
	PRINTLN("[TMSRM] Final net timer difference: ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdMultiObjectiveLimitTimer[iTeam]))
ENDPROC

PROC START_BOMB_FOOTBALL_MINIROUND()
	PRINTLN("[BMBFB][BMBFB MAJOR] START_BOMB_FOOTBALL_MINIROUND: ", iBmbFBCurrentMiniRound, " ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
	
	IF bIsLocalPlayerHost
		MC_serverBD_3.iBombFB_GoalProcessedBS = 0
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_Reset_On_Goal) 
			CLEAR_OBJECTIVE_TIMER_PENALTY()
		ENDIF
		
		REINIT_NET_TIMER(MC_serverBD_3.tdObjectiveLimitTimer[0])
		REINIT_NET_TIMER(MC_serverBD_3.tdObjectiveLimitTimer[1])
		PRINTLN("[BMBFB] Restarting objective timers for both teams!")
		
		ROUND_MULTIRULE_TIMER_TO_NEAREST_MINUTE(0, TRUE)
		ROUND_MULTIRULE_TIMER_TO_NEAREST_MINUTE(1, TRUE)
		
		MC_serverBD_3.iBombFB_ExplodedBS = 0
		MC_serverBD_3.iBombFB_GoalProcessedBS = 0
		PRINTLN("[BMBFB][BMBFB MAJOR] Fully clearing iBombFB_ExplodedBS and iBombFB_GoalProcessedBS!")
		
		MC_serverBD_3.iBombFB_ExplosionPointsThisMiniRound[0] = 0
		MC_serverBD_3.iBombFB_ExplosionPointsThisMiniRound[1] = 0
		
		MC_serverBD_3.iBombFB_OfficialRuleIndex = MC_serverBD_4.iCurrentHighestPriority[0]
		PRINTLN("[BMBFB][BMBFB MAJOR] Setting iBombFB_OfficialRuleIndex to ", MC_serverBD_3.iBombFB_OfficialRuleIndex)
		
		ARENA_CONTESTANT_TURRET_SET_REMAINING_COOLDOWN(MC_serverBD_3.arenaContestantTurretServer, 30000)
	ENDIF
	
	IF IS_BIT_SET(MC_ServerBD.iServerBitSet7, SBBOOL7_IN_BMBFB_SUDDEN_DEATH)
		PRINTLN("[BMBFB][BMBFB MAJOR] Displaying sudden death shard!")
		SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GENERIC_TEXT, "FMMC_BMB_SD", "FMMC_BMB_SD_SH")
		
		SET_ARENA_ANNOUNCER_BS_MODE_SPECIFIC(g_iAA_PlaySound_Detonation_SuddenDeath, TRUE)
		PRINTLN("[BMBFBAnnouncer] g_iAA_PlaySound_Detonation_SuddenDeath")
		
		SET_BIT(iLocalBoolCheck30, LBOOL30_BMBFB_SD_OFFICIALLY_STARTED)
	ELSE
		IF iBmbFBCurrentMiniRound > 0
		AND iBmbFBCurrentMiniRound <= 5
		AND NETWORK_IS_HOST_OF_THIS_SCRIPT()
			
			INT iMiniRoundOffset = (iBmbFBCurrentMiniRound - 1)
			iMiniRoundOffset *= 3
			PRINTLN("[SCORES] iMiniRoundOffset: ", iMiniRoundOffset, " || ", MC_serverBD.iTeamScore[0], " | ", MC_serverBD.iTeamScore[1])
							
			IF MC_serverBD.iTeamScore[0] = MC_serverBD.iTeamScore[1]
				BROADCAST_SET_ARENA_ANNOUNCER_SOUND_BS(ci_Arena_Announcer_DataTypeForEvent_ModeSpecific, g_iAA_PlaySound_Detonation_TiedMiniRound1 + iMiniRoundOffset)
				PRINTLN("[BMBFBAnnouncer] g_iAA_PlaySound_Detonation_TiedMiniRound1 + ", iMiniRoundOffset)
			ELSE
				PRINTLN("[TMS][BMBFBAnnouncer] ", MC_serverBD.iTeamScore[0], " | ", MC_serverBD.iTeamScore[1])
				IF MC_serverBD.iTeamScore[0] > MC_serverBD.iTeamScore[1]
					BROADCAST_SET_ARENA_ANNOUNCER_SOUND_BS(ci_Arena_Announcer_DataTypeForEvent_ModeSpecific, g_iAA_PlaySound_Detonation_Team1WonMiniRound1 + iMiniRoundOffset)
					PRINTLN("[BMBFBAnnouncer] g_iAA_PlaySound_Detonation_Team1WonMiniRound1 + ", iMiniRoundOffset)
				ELSE
					BROADCAST_SET_ARENA_ANNOUNCER_SOUND_BS(ci_Arena_Announcer_DataTypeForEvent_ModeSpecific, g_iAA_PlaySound_Detonation_Team2WonMiniRound1 + iMiniRoundOffset)
					PRINTLN("[BMBFBAnnouncer] g_iAA_PlaySound_Detonation_Team2WonMiniRound1 + ", iMiniRoundOffset)
				ENDIF
			ENDIF
		ENDIF
		
		PRINTLN("[BMBFB] It isn't sudden death yet")
	ENDIF
	
	IF NOT IS_BIT_SET(g_iSpecialSpectatorBS, g_ciSpecial_Spectator_BS_Start_Roaming_Spectator)
	AND NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
		SET_ENTITY_VISIBLE(LocalPlayerPed, TRUE)
		CLEAR_AREA_OF_PROJECTILES(GET_ENTITY_COORDS(LocalPlayerPed), 250)
	ENDIF
	
	INT i
	FOR i = 0 TO FMMC_MAX_TEAMS - 1
		iTeamGoalsThisMiniRound[i] = 0
		PRINTLN("[BMBFBAnnouncer] Resetting iTeamGoalsThisMiniRound for team ", i)
	ENDFOR
	iBmbFBCurrentMiniRound++
	
	IF iBmbFBCurrentMiniRound > 1
	AND NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
	AND NOT IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()
		PLAY_SOUND_FRONTEND(-1, "Period_Start", "DLC_AW_BB_Sounds")
	ENDIF
	
	RESET_NET_TIMER(stBmbFB_ExplosionDelayTimer)
	CLEAR_BIT(iLocalBoolCheck30, LBOOL30_BMBFB_BALLS_HAVE_BEEN_UNEVEN)
	CLEAR_BIT(iLocalBoolCheck29, LBOOL29_GIVEN_BFOOTBALL_LOCAL_SCORE_THIS_MINIROUND)
	
	CLEAR_ARENA_ANNOUNCER_BS_MODE_SPECIFIC_LOCKED(g_iAA_PlaySound_Detonation_EvenScoreAfter30Sec)
	
	CLEAR_ARENA_ANNOUNCER_BS_MODE_SPECIFIC_LOCKED(g_iAA_PlaySound_Detonation_Team1FullyScored)
	CLEAR_ARENA_ANNOUNCER_BS_MODE_SPECIFIC_LOCKED(g_iAA_PlaySound_Detonation_Team2FullyScored)
	
	INT iBomb
	FOR iBomb = 0 TO (FMMC_MAX_NUM_OBJECTS - 1)
		MC_playerBD_1[iLocalPart].iLastBombFootballHitTimestamp[iBomb] = -SCRIPT_MAX_INT32
	ENDFOR
ENDPROC

PROC END_OBJECTIVE_TIMER_EARLY()
	INT i 
	
	FOR i = 0 TO FMMC_MAX_TEAMS - 1
		MC_serverBD_3.iTimerPenalty[i] = SCRIPT_MAX_INT32
	ENDFOR
	
	PRINTLN("END_OBJECTIVE_TIMER_EARLY - Setting iTimerPenalty to SCRIPT_MAX_INT32 for all teams")
ENDPROC

FUNC HUD_COLOURS GET_MISSING_HUD_COLOUR_FROM_BLIP_COLOUR(INT iBlipColour)
	SWITCH iBlipColour
		CASE BLIP_COLOUR_RED 			RETURN HUD_COLOUR_RED
		CASE BLIP_COLOUR_BLUEDARK 		RETURN HUD_COLOUR_BLUEDARK
		CASE BLIP_COLOUR_BLUE 			RETURN HUD_COLOUR_BLUE
		CASE BLIP_COLOUR_GREEN 			RETURN HUD_COLOUR_GREEN
		CASE BLIP_COLOUR_YELLOW 		RETURN HUD_COLOUR_YELLOW
		CASE BLIP_COLOUR_WHITE 			RETURN HUD_COLOUR_WHITE
		CASE BLIP_COLOUR_SPECIAL_BLACK 	RETURN HUD_COLOUR_BLACK
		CASE BLIP_COLOUR_PURPLE 		RETURN HUD_COLOUR_PURPLE
	ENDSWITCH
	
	RETURN HUD_COLOUR_WHITE
ENDFUNC

FUNC HUD_COLOURS MC_GET_HUD_COLOUR_FROM_BLIP_COLOUR(INT iBlipColour)
	HUD_COLOURS retColour = GET_HUD_COLOUR_FROM_BLIP_COLOUR(iBlipColour)
	IF ENUM_TO_INT(retColour) = -1
		retColour = GET_MISSING_HUD_COLOUR_FROM_BLIP_COLOUR(iBlipColour)
	ENDIF
	RETURN retColour
ENDFUNC 

FUNC BOOL HAS_SERVER_CHECKED_ALL_CLIENTS_READY_FOR_INTRO()
	RETURN IS_BIT_SET( MC_serverBD.iServerBitSet, SSBOOL_ALL_READY_INTRO_CUT )
ENDFUNC

FUNC BOOL IS_EVERYONE_RUNNING(BOOL bPrints)

INT iparticipant
PARTICIPANT_INDEX tempPart
PLAYER_INDEX tempPlayer

	REPEAT GET_MAXIMUM_PLAYERS_ON_MISSION() iparticipant
		tempPart = INT_TO_PARTICIPANTINDEX(iparticipant)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
			
			tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
			
			IF IS_NET_PLAYER_OK(tempPlayer, FALSE, FALSE)
			AND (NOT IS_PLAYER_SPECTATOR_ONLY(tempPlayer))
				IF GET_MC_CLIENT_GAME_STATE(iparticipant) < GAME_STATE_RUNNING
					IF bPrints
						PRINTLN("[RCC MISSION] IS_EVERYONE_RUNNING Waiting for iparticipant: ", iparticipant)
					ENDIF
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN TRUE

ENDFUNC

FUNC BOOL IS_EVERYONE_READY_FOR_NEXT_ROUND()
	
	IF IS_THIS_A_ROUNDS_MISSION()
	OR IS_HEIST_QUICK_RESTART()
	OR IS_THIS_A_QUICK_RESTART_JOB()
		
		INT iparticipant
		PARTICIPANT_INDEX tempPart
		BOOL bIsPlayerSpectator 
		
		REPEAT GET_MAXIMUM_PLAYERS_ON_MISSION() iparticipant
			
			tempPart = INT_TO_PARTICIPANTINDEX(iparticipant)
			
			IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
				
				bIsPlayerSpectator = IS_BIT_SET( MC_playerBD[iparticipant].iClientBitSet, PBBOOL_ANY_SPECTATOR )
				
				IF SHOULD_THIS_SPECTATOR_BE_ROAMING_AT_START(NETWORK_GET_PLAYER_INDEX(tempPart))
				OR IS_PLAYER_SCTV(NETWORK_GET_PLAYER_INDEX(tempPart))
					bIsPlayerSpectator = TRUE
				ENDIF
				
				IF MC_playerBD[iparticipant].iSyncRoundCutsceneStage != ANIM_STAGE_FOUR
				AND MC_playerBD[iparticipant].iSyncRoundIntroCutsceneStage != JOB_CUT_PLAY 
				AND MC_playerBD[iparticipant].iSyncRoundIntroCutsceneStage != JOB_CUT_TIME_TRAIL
				AND MC_playerBD[iparticipant].iSyncRoundTeamCutsceneStage != JOB_TEAMCUT_PLAY
				AND bIsPlayerSpectator = FALSE
					CPRINTLN(DEBUG_MISSION,"[RCC MISSION][ROUNDCAM] - IS_EVERYONE_READY_FOR_NEXT_ROUND: FALSE for iparticipant = ",iparticipant, " MC_playerBD[iparticipant].iSyncRoundCutsceneStage = ", ENUM_TO_INT(MC_playerBD[iparticipant].iSyncRoundCutsceneStage) )
					CPRINTLN(DEBUG_MISSION,"[RCC MISSION][ROUNDCAM] - IS_EVERYONE_READY_FOR_NEXT_ROUND: FALSE for iparticipant = ",iparticipant, " MC_playerBD[iparticipant].iSyncRoundTeamCutsceneStage = ", ENUM_TO_INT(MC_playerBD[iparticipant].iSyncRoundTeamCutsceneStage) )
					CPRINTLN(DEBUG_MISSION,"[RCC MISSION][ROUNDCAM] - IS_EVERYONE_READY_FOR_NEXT_ROUND: FALSE for iparticipant = ",iparticipant, " MC_playerBD[iparticipant].iSyncRoundIntroCutsceneStage = ", ENUM_TO_INT(MC_playerBD[iparticipant].iSyncRoundIntroCutsceneStage) )
					CPRINTLN(DEBUG_MISSION,"[RCC MISSION][ROUNDCAM] - IS_EVERYONE_READY_FOR_NEXT_ROUND: FALSE for iparticipant = ",iparticipant, " bIsPlayerSpectator = ", bIsPlayerSpectator )
					
					iparticipant = NETWORK_GET_MAX_NUM_PARTICIPANTS()
					
					RETURN FALSE
				ELSE
					CPRINTLN(DEBUG_MISSION,"[RCC MISSION][ROUNDCAM] - IS_EVERYONE_READY_FOR_NEXT_ROUND: TRUE for iparticipant = ",iparticipant, " MC_playerBD[iparticipant].iSyncRoundCutsceneStage = ", ENUM_TO_INT(MC_playerBD[iparticipant].iSyncRoundCutsceneStage) )
					CPRINTLN(DEBUG_MISSION,"[RCC MISSION][ROUNDCAM] - IS_EVERYONE_READY_FOR_NEXT_ROUND: TRUE for iparticipant = ",iparticipant, " MC_playerBD[iparticipant].iSyncRoundTeamCutsceneStage = ", ENUM_TO_INT(MC_playerBD[iparticipant].iSyncRoundTeamCutsceneStage) )
					CPRINTLN(DEBUG_MISSION,"[RCC MISSION][ROUNDCAM] - IS_EVERYONE_READY_FOR_NEXT_ROUND: TRUE for iparticipant = ",iparticipant, " MC_playerBD[iparticipant].iSyncRoundIntroCutsceneStage = ", ENUM_TO_INT(MC_playerBD[iparticipant].iSyncRoundIntroCutsceneStage) )
					CPRINTLN(DEBUG_MISSION,"[RCC MISSION][ROUNDCAM] - IS_EVERYONE_READY_FOR_NEXT_ROUND: TRUE for iparticipant = ",iparticipant, " bIsPlayerSpectator = ", bIsPlayerSpectator )
				ENDIF
				
			ENDIF
			
		ENDREPEAT
		
		CPRINTLN(DEBUG_MISSION,"[RCC MISSION][ROUNDCAM] - IS_EVERYONE_READY_FOR_NEXT_ROUND = TRUE, everyone is ready ")
		RETURN TRUE
	ELSE
		CPRINTLN(DEBUG_MISSION,"[RCC MISSION][ROUNDCAM] - IS_EVERYONE_READY_FOR_NEXT_ROUND = TRUE, not a round match or quick restartt ")
		RETURN TRUE
	ENDIF
	
	CPRINTLN(DEBUG_MISSION,"[RCC MISSION][ROUNDCAM] - IS_EVERYONE_READY_FOR_NEXT_ROUND = FALSE, wait ")
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    This function waits for all non-spectating players to have synchronised for all intro
///    cutscenes to happen at the same time
/// RETURNS:
///    TRUE when every participant has MC_playerBD[iparticipant].bSyncCutsceneDone set to true
FUNC BOOL IS_EVERYONE_READY_FOR_GAMEPLAY()
	
	IF IS_THIS_A_ROUNDS_MISSION()
	OR IS_HEIST_QUICK_RESTART()
	OR IS_THIS_A_QUICK_RESTART_JOB()
	OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_DAWN_RAID(g_FMMC_STRUCT.iAdversaryModeType)	//Fix for bug 3578389
		INT iParticipant
		PARTICIPANT_INDEX tempPart
		
		REPEAT GET_MAXIMUM_PLAYERS_ON_MISSION() iParticipant
			tempPart = INT_TO_PARTICIPANTINDEX(iParticipant)
			
			IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
				IF MC_playerBD[iParticipant].bSyncCutsceneDone = FALSE
				AND NOT IS_BIT_SET( MC_playerBD[iParticipant].iClientBitSet, PBBOOL_ANY_SPECTATOR)
				AND NOT IS_PARTICIPANT_A_SPECTATOR(iParticipant)
					CPRINTLN(DEBUG_MISSION,"[RCC MISSION][ROUNDCAM] - IS_EVERYONE_READY_FOR_GAMEPLAY: FALSE iParticipant = ", iParticipant, " MC_playerBD[iParticipant].bSyncCutsceneDone = ", (MC_playerBD[iParticipant].bSyncCutsceneDone))
					
					RETURN FALSE
				ELSE
					CPRINTLN(DEBUG_MISSION,"[RCC MISSION][ROUNDCAM] - IS_EVERYONE_READY_FOR_GAMEPLAY: iParticipant = ", iParticipant, " MC_playerBD[iParticipant].bSyncCutsceneDone = ", (MC_playerBD[iParticipant].bSyncCutsceneDone))
				ENDIF
			ENDIF
		ENDREPEAT
		
		CPRINTLN(DEBUG_MISSION,"[RCC MISSION][ROUNDCAM] - IS_EVERYONE_READY_FOR_GAMEPLAY = TRUE, everyone is ready")
		
		RETURN TRUE
	ELSE
		CPRINTLN(DEBUG_MISSION,"[RCC MISSION][ROUNDCAM] - IS_EVERYONE_READY_FOR_GAMEPLAY = TRUE, not a round match or a quick restart")
		
		RETURN TRUE
	ENDIF
	
	CPRINTLN(DEBUG_MISSION,"[RCC MISSION][ROUNDCAM] - IS_EVERYONE_READY_FOR_GAMEPLAY = FALSE, wait")
	
	RETURN FALSE
	
ENDFUNC

FUNC INT IS_ENTITY_A_CHASE_PED(ENTITY_INDEX eiPassed)
	
	INT iEntity = -1

	IF DECOR_IS_REGISTERED_AS_TYPE("MC_ChasePedID", DECOR_TYPE_INT)
		IF DECOR_EXIST_ON(eiPassed,"MC_ChasePedID")
			iEntity = DECOR_GET_INT(eiPassed,"MC_ChasePedID")
			PRINTLN("[RCC MISSION] IS_ENTITY_A_CHASE_PED ENTITY: ", iEntity)
		ENDIF
	ENDIF

	RETURN iEntity
	
ENDFUNC

FUNC INT IS_OBJ_A_MISSION_CREATOR_OBJ(OBJECT_INDEX obj)
	
	INT i
	OBJECT_INDEX MissionObj
	
	FOR i = 0 TO (MC_serverBD.iNumObjCreated - 1)
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niObject[i])
			MissionObj = NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[i])
			IF MissionObj != NULL
				IF obj = MissionObj
					RETURN i
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN -1
	
ENDFUNC

FUNC INT IS_OBJ_A_MISSION_CREATOR_DYNOPROP(OBJECT_INDEX obj)
	
	INT i
	OBJECT_INDEX MissionObj
	
	FOR i = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps - 1)
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niDynoProps[i])
			MissionObj = NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niDynoProps[i])
			IF MissionObj != NULL
				IF obj = MissionObj
					RETURN i
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN -1
	
ENDFUNC

FUNC INT GET_INT_FROM_OBJ_ID(NETWORK_INDEX Niobj)
	
	INT i
	
	FOR i = 0 TO (MC_serverBD.iNumObjCreated - 1)
		IF MC_serverBD_1.sFMMC_SBD.niObject[i] = Niobj
			RETURN i
		ENDIF
	ENDFOR
	
	RETURN -1
	
ENDFUNC

FUNC INT IS_VEH_A_MISSION_CREATOR_VEH(VEHICLE_INDEX veh)
	
	RETURN IS_ENTITY_A_MISSION_CREATOR_ENTITY(veh)
	
ENDFUNC

///PURPOSE: This function will unfreeze the plane/heli the player is spawned in
///    so long as it is airborne, then given them a boost forwards
PROC UNFREEZE_AND_PUNT_PLAYER_SPAWN_VEHICLE_FORWARDS( FLOAT fSpeed )
	VEHICLE_INDEX vehPlayer
	
	IF IS_PED_IN_ANY_VEHICLE( LocalPlayerPed )
	OR NETWORK_DOES_NETWORK_ID_EXIST(netRespawnVehicle)
		
		IF IS_PED_IN_ANY_VEHICLE( LocalPlayerPed )
			vehPlayer = GET_VEHICLE_PED_IS_IN( LocalPlayerPed )
		ELSE
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] UNFREEZE_AND_PUNT_PLAYER_SPAWN_VEHICLE_FORWARDS - player isn't in a vehicle so use netRespawnVehicle! LocalPlayerPed = ",NATIVE_TO_INT(LocalPlayerPed),", player ped = ",NATIVE_TO_INT(PLAYER_PED_ID()))
			vehPlayer = NET_TO_VEH(netRespawnVehicle)
		ENDIF
		
		IF NETWORK_HAS_CONTROL_OF_ENTITY(vehPlayer)
			CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] UNFREEZE_AND_PUNT_PLAYER_SPAWN_VEHICLE_FORWARDS - unfrozen player spawn vehicle.")
			FREEZE_ENTITY_POSITION(vehPlayer, FALSE)
			
			IF IS_BIT_SET( g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciAERIAL_VEHICLE_SPAWN )
			OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciENABLE_RESPAWN_HYDRAS_IN_FLIGHT_MODE)
				
				IF (IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(vehPlayer))
				OR IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(vehPlayer)) AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciENABLE_RESPAWN_HYDRAS_IN_FLIGHT_MODE))
				AND IS_ENTITY_ALIVE(vehPlayer)
					SET_VEHICLE_FORWARD_SPEED(vehPlayer, fSpeed)
					IF GET_VEHICLE_HAS_LANDING_GEAR(vehPlayer)
						CONTROL_LANDING_GEAR(vehPlayer, LGC_RETRACT_INSTANT)
					ENDIF
					CPRINTLN(DEBUG_CONTROLLER, "[RCC MISSION] UNFREEZE_AND_PUNT_PLAYER_SPAWN_VEHICLE_FORWARDS - Setting vehicle speed to ", fSpeed)
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciENABLE_RESPAWN_HYDRAS_IN_FLIGHT_MODE)
			AND GET_ENTITY_MODEL(vehPlayer) = HYDRA
				SET_VEHICLE_FLIGHT_NOZZLE_POSITION_IMMEDIATE(vehPlayer, 0.0)
				PRINTLN("[RCC MISSION] UNFREEZE_AND_PUNT_PLAYER_SPAWN_VEHICLE_FORWARDS - Calling SET_VEHICLE_FLIGHT_NOZZLE_POSITION_IMMEDIATE with 0.0 on our Hydra.")
			ENDIF
		#IF IS_DEBUG_BUILD
		ELSE
			INT iveh = IS_VEH_A_MISSION_CREATOR_VEH(vehPlayer)
			
			IF iveh != -1
				CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] UNFREEZE_AND_PUNT_PLAYER_SPAWN_VEHICLE_FORWARDS - We're starting in vehicle ",iveh,", we don't have control so we can't unfreeze it but it will be unfrozen later in SET_ALL_LEGACY_MISSION_ENTITIES_UNFROZEN_AND_ABLE_TO_MIGRATE")
			ELSE
				CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] UNFREEZE_AND_PUNT_PLAYER_SPAWN_VEHICLE_FORWARDS - Starting in a non-MC vehicle, but we don't have control over it??")
			ENDIF
		#ENDIF
		ENDIF
		
	#IF IS_DEBUG_BUILD
	ELSE
		CPRINTLN( DEBUG_CONTROLLER, "[RCC MISSION] UNFREEZE_AND_PUNT_PLAYER_SPAWN_VEHICLE_FORWARDS - player isn't in a vehicle, and netRespawnVehicle doesn't exist! LocalPlayerPed = ",NATIVE_TO_INT(LocalPlayerPed),", player ped = ",NATIVE_TO_INT(PLAYER_PED_ID()))
	#ENDIF
	ENDIF
	
ENDPROC

FUNC VECTOR GET_CENTER_OF_AREA(VECTOR vCoord1, VECTOR vCoord2)
	RETURN (vCoord1 + vCoord2) * <<0.5,0.5,0.5>>
ENDFUNC

PROC PROCESS_VEH_HEALTH_REGEN_FOR_BOUNDS(VEHICLE_INDEX vehToRegen = NULL, INT iCapOverride = 0)
	IF NOT HAS_NET_TIMER_STARTED(tdVehicleRegenTimer)
		START_NET_TIMER(tdVehicleRegenTimer)
	ENDIF
	
	VEHICLE_INDEX vehPlayer	
	IF IS_PED_IN_ANY_VEHICLE(localPlayerPed)
		vehPlayer = GET_VEHICLE_PED_IS_IN(localPlayerPed)
	ENDIF	
	IF DOES_ENTITY_EXIST(vehToRegen)
		vehPlayer = vehToRegen
	ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(vehPlayer)
		FLOAT fTotal = GET_FMMC_PLAYER_VEHICLE_BODY_HEALTH_FOR_TEAM(MC_PlayerBD[iLocalPart].iteam)
		IF iPlacedVehicleIAmIn > -1
			fTotal = TO_FLOAT(GET_FMMC_VEHICLE_MAX_HEALTH(iPlacedVehicleIAmIn))
		ENDIF
		
		INT iRegenCap = iRegenCapFromBounds		
		IF iCapOverride != 0
			iRegenCap = iCapOverride
		ENDIF
		
		IF MC_playerBD_1[iLocalPart].iTaggedOutCounter = 0
			iRegenCap = 100
		ELIF MC_playerBD_1[iLocalPart].iTaggedOutCounter = 1
			iRegenCap = 75
		ELIF MC_playerBD_1[iLocalPart].iTaggedOutCounter = 2
			iRegenCap = 50
		ELIF MC_playerBD_1[iLocalPart].iTaggedOutCounter >= 3
			iRegenCap = 25
		ENDIF
		
		FLOAT fCappedTotal = (fTotal * (TO_FLOAT(iRegenCap)/100))
		FLOAT fVehicleCurrentBodyHP = GET_VEHICLE_BODY_HEALTH(vehPlayer)
		
		PRINTLN("[LM][PROCESS_BOUNDS_VEHICLE_REGEN] (before) HP Checks - iPlacedVehicleIAmIn: ", iPlacedVehicleIAmIn, " fCappedTotal: ", fCappedTotal, " fTotal: ", fTotal, " fVehicleCurrentBodyHP: ", fVehicleCurrentBodyHP, " iRegenCap: ", iRegenCap, " iTaggedOutCounter: ", MC_playerBD_1[iLocalPart].iTaggedOutCounter)
		
		IF fVehicleCurrentBodyHP < fCappedTotal
		AND HAS_NET_TIMER_EXPIRED_READ_ONLY(tdVehicleRegenTimer, ci_REGEN_VEHICLE_AFTER_THIS_TIME)
		
			fVehicleCurrentBodyHP += (fTotal * 0.04)
			
			IF fVehicleCurrentBodyHP > fCappedTotal
				fVehicleCurrentBodyHP = fCappedTotal
			ENDIF
						
			SET_VEHICLE_BODY_HEALTH(vehPlayer, fVehicleCurrentBodyHP)
			RESET_NET_TIMER(tdVehicleRegenTimer)
			PRINTLN("[LM][PROCESS_BOUNDS_VEHICLE_REGEN] Healing Vehicle!")
			PRINTLN("[LM][PROCESS_BOUNDS_VEHICLE_REGEN] (after) HP Checks - fVehicleCurrentBodyHP: ", GET_VEHICLE_BODY_HEALTH(vehPlayer))
			
			IF fVehicleCurrentBodyHP >= fTotal
				SET_VEHICLE_FIXED(vehToRegen)
			ENDIF
		ENDIF
	ELSE
		PRINTLN("[LM][PROCESS_BOUNDS_VEHICLE_REGEN] - Veh not driveable.....")
	ENDIF
	
ENDPROC

FUNC BOOL IS_BOMB_FOOTBALL_SUDDEN_DEATH()
	RETURN IS_BIT_SET(MC_ServerBD.iServerBitSet7, SBBOOL7_IN_BMBFB_SUDDEN_DEATH)
ENDFUNC

FUNC BOOL SHOULD_SHOW_BMBFB_EXTRA_DEBUG()
	#IF IS_DEBUG_BUILD
	RETURN bextrabombfootballdebug OR IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD2)
	#ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_OBJECTIVE_TIMER_EXPIRED(INT iTeam, INT iPriority = -1)
	
	IF iPriority = -1
		iPriority = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	ENDIF
	
	IF NOT HAS_NET_TIMER_STARTED(MC_serverBD_3.tdObjectiveLimitTimer[iteam])
		RETURN FALSE
	ENDIF
	
	RETURN GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdObjectiveLimitTimer[iteam]) > GET_FMMC_OBJECTIVE_TIME_FROM_SELECTION(g_FMMC_STRUCT.sFMMCEndConditions[iteam].iObjectiveTimeLimitRule[iPriority]) - MC_serverBD_3.iTimerPenalty[iTeam]
ENDFUNC

FUNC BOOL HAS_MULTIRULE_TIMER_EXPIRED(INT iTeam)
	RETURN GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdMultiObjectiveLimitTimer[iteam]) > MC_serverBD_3.iMultiObjectiveTimeLimit[iTeam]	
ENDFUNC	

FUNC BOOL NEED_TO_WAIT_FOR_FINAL_POINTS()
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_BOMB_FOOTBALL(g_FMMC_STRUCT.iAdversaryModeType)
	
		IF IS_BIT_SET(iLocalBoolCheck30, LBOOL30_BMBFB_SD_OFFICIALLY_STARTED)
			PRINTLN("[BMBFB] NEED_TO_WAIT_FOR_FINAL_POINTS - Returning FALSE due to LBOOL30_BMBFB_SD_OFFICIALLY_STARTED")
			RETURN FALSE
		ENDIF
	
		IF IS_BIT_SET(iLocalBoolCheck29, LBOOL29_WAITING_FOR_OBJ_TIMER_AND_FINAL_FOOTBALL_POINTS)
			IF NOT IS_BIT_SET(iLocalBoolCheck29, LBOOL29_GIVEN_FINAL_FOOTBALL_POINTS)
				PRINTLN("[BMBFB] NEED_TO_WAIT_FOR_FINAL_POINTS - Returning TRUE (Waiting for LBOOL29_GIVEN_FINAL_FOOTBALL_POINTS)")
				RETURN TRUE
			ENDIF
		ELSE
			PRINTLN("[BMBFB] NEED_TO_WAIT_FOR_FINAL_POINTS - LBOOL29_WAITING_FOR_OBJ_TIMER_AND_FINAL_FOOTBALL_POINTS is FALSE")
		ENDIF
		
		IF MC_serverBD.iTeamScore[0] = MC_serverBD.iTeamScore[1] //ARE_MULTIPLE_TEAMS_ON_SAME_SCORE()
			PRINTLN("[BMBFB] NEED_TO_WAIT_FOR_FINAL_POINTS - Returning TRUE (Same points)")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL NEED_TO_WAIT_FOR_OBJECTIVE_TIMER(INT iTeam)
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_Delay_Multirule_End_Wait_Until_Rule_Timer)
		
		IF IS_BOMB_FOOTBALL_SUDDEN_DEATH()
			IF HAS_NET_TIMER_STARTED(MC_serverBD_3.tdObjectiveLimitTimer[0])
			AND GET_REMAINING_TIME_ON_RULE(0, MC_serverBD_4.iCurrentHighestPriority[0]) <= 55000
				IF MC_serverBD.iTeamScore[0] != MC_serverBD.iTeamScore[1]
					PRINTLN("[RCC MISSION][MultiRuleTimer][BMBFB] NEED_TO_WAIT_FOR_OBJECTIVE_TIMER returning FALSE because the points are uneven in Bomb Football sudden death!")
					RETURN FALSE
				ELSE
					PRINTLN("[RCC MISSION][MultiRuleTimer][BMBFB] NEED_TO_WAIT_FOR_OBJECTIVE_TIMER - Waiting for points to be uneven!")
				ENDIF
			ELSE
				PRINTLN("[RCC MISSION][MultiRuleTimer][BMBFB] NEED_TO_WAIT_FOR_OBJECTIVE_TIMER - GET_REMAINING_TIME_ON_RULE is ", GET_REMAINING_TIME_ON_RULE(0, MC_serverBD_4.iCurrentHighestPriority[0]))
			ENDIF
		ENDIF
		
		IF NOT HAS_OBJECTIVE_TIMER_EXPIRED(iTeam)
			PRINTLN("[RCC MISSION][MultiRuleTimer] NEED_TO_WAIT_FOR_OBJECTIVE_TIMER returning TRUE due to HAS_OBJECTIVE_TIMER_EXPIRED!")
			RETURN TRUE
		ENDIF
		
		RETURN FALSE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

PROC SET_ARENA_SPAWN_FIX_COORDS(VECTOR vStartPos, FLOAT fStartHeading)
	IF g_sTransitionSessionData.sMissionRoundData.iNumberOfRoundsPlayed > 0
	AND CONTENT_IS_USING_ARENA()
		vArenaSpawnVec = vStartPos
		fArenaSpawnHead = fStartHeading
		PRINTLN("[ARENA_SPAWN_FIX] SET_ARENA_SPAWN_FIX_COORDS - Storing start position: ", vArenaSpawnVec, " Heading: ", fArenaSpawnHead)
	ENDIF
ENDPROC

FUNC BOOL ARENA_SPAWN_FIX()
	IF g_sTransitionSessionData.sMissionRoundData.iNumberOfRoundsPlayed > 0
	AND CONTENT_IS_USING_ARENA()
	AND NOT IS_VECTOR_ZERO(vArenaSpawnVec)
		IF DOES_ENTITY_EXIST(LocalPlayerPed)
			VECTOR vPlayerPos = GET_ENTITY_COORDS(LocalPlayerPed)
			IF NOT ARE_VECTORS_ALMOST_EQUAL(vArenaSpawnVec, vPlayerPos, 3) AND (GET_FRAME_COUNT() % 20 = 0)
			OR NOT IS_BIT_SET(iLocalBoolCheck29, LBOOL29_ARENA_SPAWN_FIX_HAS_BEEN_DONE)
				VECTOR vNewCoord = vArenaSpawnVec
				PRINTLN("ARENA_SPAWN_FIX - Warping from: ", vPlayerPos, " to ", vArenaSpawnVec)
				IF NET_WARP_TO_COORD(vNewCoord , fArenaSpawnHead, TRUE, FALSE, FALSE, FALSE, TRUE, TRUE, TRUE, TRUE)
					VEHICLE_INDEX viPlayerVeh
					IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
						viPlayerVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
						IF DOES_ENTITY_EXIST(viPlayerVeh)
							IF SET_VEHICLE_ON_GROUND_PROPERLY(viPlayerVeh, 5)
								vPlayerPos = GET_ENTITY_COORDS(viPlayerVeh)
								PRINTLN("ARENA_SPAWN_FIX - Set play on ground 1. Player vehicle position: ", vPlayerPos)
							ENDIF
						ENDIF
					ENDIF
					vPlayerPos = GET_ENTITY_COORDS(LocalPlayerPed)
					PRINTLN("ARENA_SPAWN_FIX - Warping to position. vPlayerPos: ", vPlayerPos)
					SET_BIT(iLocalBoolCheck29, LBOOL29_ARENA_SPAWN_FIX_HAS_BEEN_DONE)
					RETURN TRUE
				ENDIF
			ELSE
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE	
		VECTOR vdebug = GET_ENTITY_COORDS(LocalPlayerPed)
		PRINTLN("ARENA_SPAWN_FIX - ", vdebug, " vs ", vArenaSpawnVec, " Rounds: ", g_sTransitionSessionData.sMissionRoundData.iNumberOfRoundsPlayed, " using Arena: ", CONTENT_IS_USING_ARENA())
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SET_ARENA_SPAWN_FIX_STAGE(eArenaSpawnFixStage eNewStage)
	PRINTLN("SET_ARENA_SPAWN_FIX_STAGE - New stage = ", ENUM_TO_INT(eNewStage))
	MC_playerBD_1[iLocalPart].eArenaSpawnStage = eNewStage
ENDPROC

FUNC BOOL PROCESS_ARENA_SPAWN_FIX()
	BOOL bReturn
	VECTOR vTemp
	VEHICLE_INDEX viPlayerVeh
	IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		viPlayerVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
	ENDIF
	
	BOOL bDontWarp = IS_LOCAL_PLAYER_SPAWNING_IN_ARENA_BOX_SEAT() OR IS_PLAYER_IN_ARENA_BOX_SEAT()
	
	IF g_sTransitionSessionData.sMissionRoundData.iNumberOfRoundsPlayed > 0
	AND CONTENT_IS_USING_ARENA()
	AND NOT IS_VECTOR_ZERO(vArenaSpawnVec)
		SWITCH MC_playerBD_1[iLocalPart].eArenaSpawnStage
			CASE eArenaSpawnFix_Idle	
				IF (((GET_PLAYER_SWITCH_TYPE() != SWITCH_TYPE_SHORT
				AND GET_PLAYER_SWITCH_STATE() = SWITCH_STATE_JUMPCUT_DESCENT
				AND GET_SKYSWOOP_STAGE() != SKYSWOOP_NONE)))
				OR IS_SKYSWOOP_AT_GROUND()
					SET_ARENA_SPAWN_FIX_STAGE(eArenaSpawnFix_FadeOut)
				ENDIF
			BREAK
			CASE eArenaSpawnFix_FadeOut
				PRINTLN("PROCESS_ARENA_SPAWN_FIX - In eArenaSpawnFix_FadeOut")
				IF NOT IS_SCREEN_FADING_OUT()
					DO_SCREEN_FADE_OUT(2500)
				ENDIF
				IF IS_SCREEN_FADED_OUT()
					SET_ARENA_SPAWN_FIX_STAGE(eArenaSpawnFix_Warp)
				ENDIF
			BREAK
			CASE eArenaSpawnFix_Warp
				PRINTLN("PROCESS_ARENA_SPAWN_FIX - In eArenaSpawnFix_Warp")
				IF bDontWarp
					PRINTLN("PROCESS_ARENA_SPAWN_FIX - Not doing the warp.")
					SET_ARENA_SPAWN_FIX_STAGE(eArenaSpawnFix_Wait)
				ELSE
					IF ARENA_SPAWN_FIX()
						IF DOES_ENTITY_EXIST(viPlayerVeh)
							FREEZE_ENTITY_POSITION(viPlayerVeh, TRUE)
						ENDIF
						SET_ARENA_SPAWN_FIX_STAGE(eArenaSpawnFix_Wait)
					ENDIF
				ENDIF
			BREAK
			CASE eArenaSpawnFix_Wait
				PRINTLN("PROCESS_ARENA_SPAWN_FIX - In eArenaSpawnFix_Wait")
				IF IS_SKYSWOOP_AT_GROUND()
				AND (IS_INTERIOR_READY(g_ArenaInterior) OR IS_BIT_SET(g_iArenaLoadingBS, ciARENALOADINGBS_ArenaHasLoaded)) // Cannot trust the native. If it's returned that it's ready when we loaded it, we'll trust that from LOAD_UGC_ARENA.
					IF NOT HAS_NET_TIMER_STARTED(tdArenaSpawnFixTimer)
						REINIT_NET_TIMER(tdArenaSpawnFixTimer)
						REINIT_NET_TIMER(tdArenaSpawnFixSafety)
						PRINTLN("PROCESS_ARENA_SPAWN_FIX - Starting wait timer(s)")
					ELSE
						IF DOES_ENTITY_EXIST(viPlayerVeh)
						AND NOT bDontWarp
							IF HAS_NET_TIMER_EXPIRED(tdArenaSpawnFixTimer, 5500)
								IF (SET_VEHICLE_ON_GROUND_PROPERLY(viPlayerVeh, 5)
								AND HAS_NET_TIMER_EXPIRED(tdArenaSpawnFixTimer, 6500))
									PRINTLN("PROCESS_ARENA_SPAWN_FIX - placed on ground properly first time.")
									SET_ARENA_SPAWN_FIX_STAGE(eArenaSpawnFix_FadeIn)
								ELIF HAS_NET_TIMER_EXPIRED(tdArenaSpawnFixSafety, 7000)
									PRINTLN("PROCESS_ARENA_SPAWN_FIX - Safety timer stage 1 expired.")
									vtemp = GET_ENTITY_COORDS(viPlayerVeh)
									IF GET_GROUND_Z_FOR_3D_COORD(vTemp, vTemp.z)
										vTemp.z += 1
										IF vTemp.z > 100.0
											PRINTLN("PROCESS_ARENA_SPAWN_FIX - Ground Z pos: ", vTemp)
											SET_ENTITY_COORDS(viPlayerVeh, vTemp)
										ELSE
											vtemp = vArenaSpawnVec - <<0, 0, 0.5>>
											SET_ENTITY_COORDS(viPlayerVeh, vtemp)
											PRINTLN("PROCESS_ARENA_SPAWN_FIX - Moving player to stored position -1z: ", vtemp)
										ENDIF
									ENDIF
									IF SET_VEHICLE_ON_GROUND_PROPERLY(viPlayerVeh, 7)
										PRINTLN("PROCESS_ARENA_SPAWN_FIX - Safety timer on ground properly.")
										SET_ARENA_SPAWN_FIX_STAGE(eArenaSpawnFix_FadeIn)
									ELSE
										IF HAS_NET_TIMER_EXPIRED(tdArenaSpawnFixSafety, 7500)
											PRINTLN("PROCESS_ARENA_SPAWN_FIX - Safety timer stage 2 expired.")
											SET_ARENA_SPAWN_FIX_STAGE(eArenaSpawnFix_FadeIn)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ELSE
							IF HAS_NET_TIMER_EXPIRED(tdArenaSpawnFixTimer, 2000)
								SET_ARENA_SPAWN_FIX_STAGE(eArenaSpawnFix_Sync)
							ENDIF
						ENDIF
					ENDIF
				ELSE
					bReturn = FALSE
					PRINTLN("PROCESS_ARENA_SPAWN_FIX - Camera not at ground or interior not ready yet.")
				ENDIF
			BREAK
			CASE eArenaSpawnFix_Sync
				INT iParticipantsReady, i
				FOR i = 0 TO MAX_NUM_MC_PLAYERS-1
					IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
					AND NOT IS_PARTICIPANT_A_SPECTATOR(i)
						IF MC_playerBD_1[i].eArenaSpawnStage >= eArenaSpawnFix_Sync
							iParticipantsReady++
						ELSE
							PRINTLN("PROCESS_ARENA_SPAWN_FIX - participant ", i," is not in sync state. They are in state: ", MC_playerBD_1[i].eArenaSpawnStage)
						ENDIF
					ENDIF
				ENDFOR
				IF GET_NUMBER_OF_ACTIVE_PARTICIPANTS() = iParticipantsReady
					SET_ARENA_SPAWN_FIX_STAGE(eArenaSpawnFix_FadeIn)
					REINIT_NET_TIMER(tdArenaSpawnFadeInTimer)
				ENDIF
			BREAK
			CASE eArenaSpawnFix_FadeIn
				bReturn = TRUE
				IF HAS_NET_TIMER_EXPIRED(tdArenaSpawnFadeInTimer, 1500) //to stop a camera pop on intro cam.
				AND (jobIntroData.jobIntroStage = JOB_CUT_TIME_TRAIL OR bDontWarp)
					PRINTLN("PROCESS_ARENA_SPAWN_FIX - In eArenaSpawnFix_FadeIn")
					IF NOT IS_SCREEN_FADING_IN()
						DO_SCREEN_FADE_IN(750)
						IF DOES_ENTITY_EXIST(viPlayerVeh)
						AND NOT IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()
							SET_VEHICLE_FORWARD_SPEED(viPlayerVeh, 0.1)
						ENDIF
						PRINTLN("PROCESS_ARENA_SPAWN_FIX - SET_VEHICLE_FORWARD_SPEED(viPlayerVeh, 0.1) called to fix wheels")
					ENDIF
					IF IS_SCREEN_FADED_IN()
						SET_ARENA_SPAWN_FIX_STAGE(eArenaSpawnFix_Progress)
					ENDIF
				ENDIF
			BREAK
			CASE eArenaSpawnFix_Progress
				bReturn = TRUE
			BREAK
		ENDSWITCH
	ELSE
		IF g_sTransitionSessionData.sMissionRoundData.iNumberOfRoundsPlayed = 0
			PRINTLN("PROCESS_ARENA_SPAWN_FIX - Not doing arena spawn fix. Is the first round.")
		ENDIF
		IF NOT CONTENT_IS_USING_ARENA()
			PRINTLN("PROCESS_ARENA_SPAWN_FIX - Not doing arena spawn fix. Arena not in use.")
		ENDIF
		IF IS_VECTOR_ZERO(vArenaSpawnVec)
			PRINTLN("PROCESS_ARENA_SPAWN_FIX - Not doing arena spawn fix. vArenaSpawnVec is zero.")
		ENDIF
		bReturn = TRUE
	ENDIF
		
	RETURN bReturn
ENDFUNC

FUNC BOOL SHOULD_HIDE_CIRCLE_SCORE_HUD_TIMER()
	IF IS_THIS_BOMB_FOOTBALL()
		IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iTeam] = 2
			
			IF SHOULD_SHOW_BMBFB_EXTRA_DEBUG()
				DRAW_DEBUG_TEXT_2D("Not drawing timer cos on holding rule", <<0.5, 0.5, 0.5>>)
			ENDIF
			
			PRINTLN("[TMS] SHOULD_HIDE_CIRCLE_SCORE_HUD_TIMER - Not drawing timer cos on holding rule")
			RETURN TRUE
		ENDIF
		
		IF HAS_OBJECTIVE_TIMER_EXPIRED(MC_playerBD[iLocalPart].iteam)
			IF SHOULD_SHOW_BMBFB_EXTRA_DEBUG()
				DRAW_DEBUG_TEXT_2D("Not drawing timer cos timer has expired", <<0.5, 0.55, 0.5>>)
			ENDIF
			
			PRINTLN("[TMS] SHOULD_HIDE_CIRCLE_SCORE_HUD_TIMER - Not drawing timer cos timer has expired")
			RETURN TRUE
		ENDIF
		
		IF MC_serverBD_3.iBombFB_OfficialRuleIndex != MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam]
		AND MC_serverBD_3.iBombFB_OfficialRuleIndex > -1
			IF SHOULD_SHOW_BMBFB_EXTRA_DEBUG()
				DRAW_DEBUG_TEXT_2D("Not drawing timer cos we're in a transition period", <<0.5, 0.6, 0.5>>)
			ENDIF
			
			PRINTLN("[TMS] SHOULD_HIDE_CIRCLE_SCORE_HUD_TIMER - Not drawing timer cos we're in a transition period")
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_RP_BAR_ACTIVE_IN_ARENA_BOX()
		PRINTLN("[LM] SHOULD_HIDE_CIRCLE_SCORE_HUD_TIMER - RETURN TRUE")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PRINT_GAMES_MASTERS_INFO()
	IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_GAMES_MASTERS(g_FMMC_STRUCT.iAdversaryModeType)
	//	EXIT
	ENDIF
	INT i, j
	PRINTLN("*****PRINT_GAMES_MASTERS_INFO*****")

	FOR j = 0 TO FMMC_MAX_TEAMS-1
		PRINTLN("Team ", j,":")
		FOR i = 0 TO FMMC_MAX_GO_TO_LOCATIONS-1
			PRINTLN("Locate ", i, " Fail Rule: ", g_FMMC_STRUCT_ENTITIES.sGotoLocationData[i].iJumpToObjectiveFail[j])
		ENDFOR
		FOR i = 0 TO FMMC_MAX_RULES-1
			PRINTLN("ciBS_RULE12_DONT_FAIL_ON_MULTI_RULE Rule: ", i, ": ", BOOL_TO_STRING(IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[j].iRuleBitsetTwelve[i], ciBS_RULE12_DONT_FAIL_ON_MULTI_RULE)))
		ENDFOR
	ENDFOR

ENDPROC

FUNC HUD_COLOURS GET_COLOUR_OF_OPPOSITION_TEAM(INT iTeam, PLAYER_INDEX piPlayerToUse)
	SWITCH iTeam
		CASE 0 RETURN HUD_COLOUR_PURPLE
		CASE 1 RETURN HUD_COLOUR_ORANGE
	ENDSWITCH
	RETURN GET_HUD_COLOUR_FOR_FMMC_TEAM(iTeam, piPlayerToUse)
ENDFUNC

PROC SET_UP_SCRIPT_VARIABLE_HUD_COLOUR_FROM_HUD_COLOUR(HUD_COLOURS hcHudColour)
	INT iR, iG, iB, iA
	GET_HUD_COLOUR(hcHudColour, iR, iG, iB, iA)
	SET_SCRIPT_VARIABLE_HUD_COLOUR(iR, iG, iB, iA)
ENDPROC

FUNC BOOL HAS_PLAYER_QUIT_MISSION(INT iPlayer)
	RETURN IS_BIT_SET(GlobalplayerBD_FM[iPlayer].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bQuitJob)//GlobalplayerBD_FM[iPlayer].bQuitJob
ENDFUNC

PROC HOST_SET_ARENA_AUDIO_SCORE()
	IF NOT CONTENT_IS_USING_ARENA()
	OR NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		EXIT
	ENDIF
	
	IF MC_ServerBD_2.iArenaAudioScore = -1
		MC_ServerBD_2.iArenaAudioScore = GET_RANDOM_INT_IN_RANGE(1, 9)
		PRINTLN("[HOST_SET_ARENA_AUDIO_SCORE] - Setting Audio Score for MC to iArenaAudioScore: ", MC_ServerBD_2.iArenaAudioScore)
	ENDIF
ENDPROC

PROC CANCEL_ARENA_AUDIO_SCORE()	
	IF MC_ServerBD_2.iArenaAudioScore > -1
		TEXT_LABEL_15 tl15_track = "MC_AW_MUSIC_"
		tl15_track += MC_ServerBD_2.iArenaAudioScore
		CANCEL_MUSIC_EVENT(tl15_track)
	ENDIF
ENDPROC

PROC PROCESS_PLAY_ARENA_AUDIO_SCORE()
	IF NOT CONTENT_IS_USING_ARENA()
		EXIT
	ENDIF
	
	IF g_bMissionEnding
	OR g_bCelebrationScreenIsActive	
		PRINTLN("[PROCESS_PLAY_ARENA_AUDIO_SCORE] - Exitting because g_bMissionEnding: ", g_bMissionEnding, " g_bCelebrationScreenIsActive: ", g_bCelebrationScreenIsActive)
		EXIT
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck30, LBOOL30_PLAYED_30s_COUNTDOWN_FOR_ARENA_WARS)
		PRINTLN("[PROCESS_PLAY_ARENA_AUDIO_SCORE] - Exitting because 30s Countdown: ", IS_BIT_SET(iLocalBoolCheck30, LBOOL30_PLAYED_30s_COUNTDOWN_FOR_ARENA_WARS))
		EXIT
	ENDIF
	
	IF IS_BIT_SET(MC_serverBD.iServerBitSet2, SBBOOL2_FINISHED_COUNTDOWN)
		IF MC_ServerBD_2.iArenaAudioScore > -1
			IF NOT IS_BIT_SET(iLocalBoolCheck30, LBOOL30_PLAYED_ARENA_WARS_AUDIO_TRACK)
				START_AUDIO_SCENE("MP_POSITIONED_RADIO_MUTE_SCENE")
				SET_AUDIO_FLAG("AllowScoreAndRadio", FALSE)
				TEXT_LABEL_15 tl15_track = "MC_AW_MUSIC_"
				tl15_track += MC_ServerBD_2.iArenaAudioScore
				TRIGGER_MUSIC_EVENT(tl15_track)
			
				SET_BIT(iLocalBoolCheck30, LBOOL30_PLAYED_ARENA_WARS_AUDIO_TRACK)
				SET_BIT(iLocalBoolCheck3, LBOOL3_MUSIC_PLAYING)
				
				PRINTLN("[PROCESS_PLAY_ARENA_AUDIO_SCORE] - iArenaAudioScore: ", MC_ServerBD_2.iArenaAudioScore, " Playing: ", tl15_track)
			ENDIF
		ELSE
			PRINTLN("[PROCESS_PLAY_ARENA_AUDIO_SCORE] - Waiting for iArenaAudioScore to be > -1")
		ENDIF
	ENDIF
ENDPROC

FUNC INT ARENA_WARS_SC_LDB_GET_MODE_STAT_1()
	PRINTLN("[ARENA][SC LDB] ARENA_WARS_SC_LDB_GET_MODE_STAT_1 - ", g_FMMC_STRUCT.tl63MissionName)
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_ARENA_CTF(g_FMMC_STRUCT.iAdversaryModeType)
		PRINTLN("[ARENA][SC LDB] ARENA_WARS_SC_LDB_GET_MODE_STAT_1 - Flag count: ", MC_playerBD[iLocalPart].iNumberOfDeliveries)
		RETURN MC_playerBD[iLocalPart].iNumberOfDeliveries
		
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_BOMB_FOOTBALL(g_FMMC_STRUCT.iAdversaryModeType)
		PRINTLN("[ARENA][SC LDB] ARENA_WARS_SC_LDB_GET_MODE_STAT_1 - Points: ", MC_playerBD[iLocalPart].iPlayerScore)
		RETURN MC_playerBD[iLocalPart].iPlayerScore
		
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_GAMES_MASTERS(g_FMMC_STRUCT.iAdversaryModeType)
		PRINTLN("[ARENA][SC LDB] ARENA_WARS_SC_LDB_GET_MODE_STAT_1 - Checkpoints: ", MC_PlayerBD[iLocalPart].iPlayerScore)
		RETURN MC_PlayerBD[iLocalPart].iPlayerScore
		
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_MONSTER_JAM(g_FMMC_STRUCT.iAdversaryModeType)
	OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_TAG_TEAM(g_FMMC_STRUCT.iAdversaryModeType)
		PRINTLN("[ARENA][SC LDB] ARENA_WARS_SC_LDB_GET_MODE_STAT_1 - Kills: ", MC_playerBD[iLocalPart].iNumPlayerKills)
		RETURN MC_playerBD[iLocalPart].iNumPlayerKills
	ENDIF
	PRINTLN("[ARENA][SC LDB] ARENA_WARS_SC_LDB_GET_MODE_STAT_1 Returning 0")
	RETURN 0
ENDFUNC
FUNC INT ARENA_WARS_SC_LDB_GET_MODE_STAT_2()
	PRINTLN("[ARENA][SC LDB] ARENA_WARS_SC_LDB_GET_MODE_STAT_2 - ", g_FMMC_STRUCT.tl63MissionName)
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_ARENA_CTF(g_FMMC_STRUCT.iAdversaryModeType)
	OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_GAMES_MASTERS(g_FMMC_STRUCT.iAdversaryModeType)
		PRINTLN("[ARENA][SC LDB] ARENA_WARS_SC_LDB_GET_MODE_STAT_2 - Kills: ", MC_playerBD[iLocalPart].iNumPlayerKills)
		RETURN MC_playerBD[iLocalPart].iNumPlayerKills
		
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_BOMB_FOOTBALL(g_FMMC_STRUCT.iAdversaryModeType)
		PRINTLN("[ARENA][SC LDB] ARENA_WARS_SC_LDB_GET_MODE_STAT_2 - Goals: ", MC_playerBD_1[iLocalPart].iBmbFb_Goals)
		RETURN MC_playerBD_1[iLocalPart].iBmbFb_Goals
		
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_MONSTER_JAM(g_FMMC_STRUCT.iAdversaryModeType)
		PRINTLN("[ARENA][SC LDB] ARENA_WARS_SC_LDB_GET_MODE_STAT_2 - Damage: ", ROUND(MC_playerBD[iLocalPart].fDamageDealt))
		RETURN ROUND(MC_playerBD[iLocalPart].fDamageDealt)
		
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TAG_TEAM(g_FMMC_STRUCT.iAdversaryModeType)
		PRINTLN("[ARENA][SC LDB] ARENA_WARS_SC_LDB_GET_MODE_STAT_2 - Tagged In: ", MC_playerBD_1[iLocalPart].iTaggedInCounter)
		RETURN MC_playerBD_1[iLocalPart].iTaggedInCounter
	ENDIF
	PRINTLN("[ARENA][SC LDB] ARENA_WARS_SC_LDB_GET_MODE_STAT_2 Returning 0")
	RETURN 0
ENDFUNC

PROC PROCESS_MINIROUND_TRANSITIONS_FOR_SPECIAL_SPECTATORS()
	IF g_eSpecialSpectatorState != SSS_MAINTAIN
		EXIT
	ENDIF

	IF NOT IS_USING_ROAMING_SPECTATOR_POWERUP()
	AND NOT IS_LOCAL_PLAYER_USING_DRONE()
	AND NOT ((TURRET_MANAGER_GET_GROUP(PLAYER_ID()) = TGT_ARENA_CONTESTANT) AND TURRET_CAM_IS_RUNNING() AND TURRET_CAM_IS_READY())
		EXIT
	ENDIF
	
	IF IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_TEAM_0_SHOULD_RESPAWN_BACK_AT_START_NOW)
	OR IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_TEAM_1_SHOULD_RESPAWN_BACK_AT_START_NOW)
	OR IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_TEAM_2_SHOULD_RESPAWN_BACK_AT_START_NOW)
	OR IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_TEAM_3_SHOULD_RESPAWN_BACK_AT_START_NOW)
	OR IS_BIT_SET(MC_serverBD.iServerBitSet5, SBBOOL5_ALL_PLAYERS_RENDERPHASES_PAUSED)
		IF NOT ANIMPOSTFX_IS_RUNNING("InchPurple")
			ANIMPOSTFX_PLAY("InchPurple", 0, FALSE)
			PRINTLN("[ARENA][TMS] PROCESS_MINIROUND_TRANSITIONS_FOR_SPECIAL_SPECTATORS - Playing 'InchPurple' to match other players during mini-round transition!")
		ENDIF
		
		TOGGLE_PAUSED_RENDERPHASES(FALSE)
		PRINTLN("[ARENA][TMS] PROCESS_MINIROUND_TRANSITIONS_FOR_SPECIAL_SPECTATORS - Pausing renderphase to match other players during mini-round transition!")
		
		SET_BIT(iLocalBoolCheck30, LBOOL30_SPEC_SPEC_FAKE_MINIROUND_TRANSITION_ACTIVATED)
	ELSE
		IF IS_BIT_SET(iLocalBoolCheck30, LBOOL30_SPEC_SPEC_FAKE_MINIROUND_TRANSITION_ACTIVATED)
			IF ANIMPOSTFX_IS_RUNNING("InchPurple")
				ANIMPOSTFX_STOP("InchPurple") 
				PRINTLN("[ARENA][TMS] PROCESS_MINIROUND_TRANSITIONS_FOR_SPECIAL_SPECTATORS - Stopping 'InchPurple'")
			ENDIF
		ENDIF
		
		TOGGLE_PAUSED_RENDERPHASES(TRUE)
		PRINTLN("[ARENA][TMS] PROCESS_MINIROUND_TRANSITIONS_FOR_SPECIAL_SPECTATORS - Resuming renderphase as we have reached the next round!")
		
		CLEAR_BIT(iLocalBoolCheck30, LBOOL30_SPEC_SPEC_FAKE_MINIROUND_TRANSITION_ACTIVATED)
	ENDIF
	
ENDPROC

// YACHT MANAGER //

PROC PROCESS_FMMC_YACHT()
	
	IF NOT USING_FMMC_YACHT()
		EXIT
	ENDIF
	
	sMissionYachtData.iLocation = GET_FMMC_YACHT_ID(0)
	sMissionYachtData.Appearance.iOption = g_FMMC_STRUCT.sMissionYachtInfo[0].iMYacht_ModelIndex
	sMissionYachtData.Appearance.iTint = g_FMMC_STRUCT.sMissionYachtInfo[0].iMYacht_ColourIndex
	sMissionYachtData.Appearance.iLighting = g_FMMC_STRUCT.sMissionYachtInfo[0].iMYacht_LightingIndex
	sMissionYachtData.Appearance.iRailing = g_FMMC_STRUCT.sMissionYachtInfo[0].iMYacht_FittingsIndex
	sMissionYachtData.Appearance.iFlag = g_FMMC_STRUCT.sMissionYachtInfo[0].iMYacht_FlagIndex
	
	IF IS_BIT_SET(MC_serverBD.iServerBitSet7, SBBOOL7_YACHT_MOVED_TO_DESTINATION)
		sMissionYachtData.iLocation = GET_FMMC_YACHT_DESTINATION_ID()
		g_SpawnData.iYachtToWarpTo = sMissionYachtData.iLocation
		PRINTLN("[MYACHT] Using post cutscene location")
	ENDIF
	
	IF sMissionYachtVars.eMissionYachtSpawnState != FMMC_YACHT_SPAWN_STATE__COMPLETE
		REQUEST_ASSETS_FOR_MISSION_CREATOR_YACHT(sMissionYachtData)
		PRINTLN("PROCESS_FMMC_YACHT - Requesting Yacht assets")
		DRAW_DEBUG_TEXT_2D("PROCESS_FMMC_YACHT - Requesting Yacht assets", <<0.25, 0.5, 0.5>>)
		
		IF HAVE_ASSETS_LOADED_FOR_MISSION_CREATOR_YACHT(sMissionYachtData)
        	CREATE_YACHT_FOR_MISSION_CREATOR(sMissionYachtData)
			PRINTLN("PROCESS_FMMC_YACHT - Setting ciMYACHT__LOADED")
			sMissionYachtVars.eMissionYachtSpawnState = FMMC_YACHT_SPAWN_STATE__COMPLETE
			DRAW_DEBUG_TEXT_2D("PROCESS_FMMC_YACHT - ciMYACHT__LOADED", <<0.25, 0.5, 0.5>>)
		ENDIF
	ELSE
		BOOL bRuleDelayed		
		IF NOT bRuleDelayed
			PRINTLN("PROCESS_FMMC_YACHT - Creating Yacht on INIT")
			UPDATE_YACHT_FOR_MISSION_CREATOR(sMissionYachtData)
			DRAW_DEBUG_TEXT_2D("PROCESS_FMMC_YACHT - UPDATE_YACHT_FOR_MISSION_CREATOR", <<0.25, 0.5, 0.5>>)
		ENDIF
	ENDIF
	
	IF sMissionYachtVars.eMissionYachtSpawnState = FMMC_YACHT_SPAWN_STATE__COMPLETE
		SET_BIT(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_FMMC_YACHTS_READY)
	ELSE
		CLEAR_BIT(MC_playerBD[iLocalPart].iClientBitSet4, PBBOOL4_FMMC_YACHTS_READY)
	ENDIF
ENDPROC

FUNC BOOL IS_PLAYER_VALID_FOR_PLAYER_LOOP(INT iPart)
	
	IF IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_ANY_SPECTATOR)
	OR IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_HEIST_SPECTATOR)
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_PLAYER_FAIL)
		RETURN FALSE
	ENDIF
	
	IF NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_FINISHED)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

// Put this as a WHILE and perform per-player stuff within the WHILE loop
FUNC BOOL DO_PLAYER_LOOP(INT& iPart, INT& iNumPlayersChecked)
	
	INT iNumberOfPlayers = (MC_serverBD.iNumberOfPlayingPlayers[0] + MC_serverBD.iNumberOfPlayingPlayers[1] + MC_serverBD.iNumberOfPlayingPlayers[2] + MC_serverBD.iNumberOfPlayingPlayers[3]) 
	
	IF iPart >= MAX_NUM_MC_PLAYERS
		PRINTLN("DO_PLAYER_LOOP | No valid players were found!")
		DEBUG_PRINTCALLSTACK()
		RETURN FALSE
	ENDIF
	
	IF iNumPlayersChecked >= iNumberOfPlayers
		//We've checked all players
		RETURN FALSE
	ENDIF
	
	IF NOT IS_PLAYER_VALID_FOR_PLAYER_LOOP(iPart)
		//Return TRUE without acting like it was a legit player
		RETURN TRUE
	ENDIF
	
	// Return TRUE to continue the loop
	iNumPlayersChecked++
	RETURN TRUE
ENDFUNC

FUNC BOOL HAVE_ALL_PLAYERS_LOADED_FMMC_YACHT()
	
	INT iNumberOfPlayers = (MC_serverBD.iNumberOfPlayingPlayers[0] + MC_serverBD.iNumberOfPlayingPlayers[1] + MC_serverBD.iNumberOfPlayingPlayers[2] + MC_serverBD.iNumberOfPlayingPlayers[3]) 
	INT iPart
	INT iNumberOfPlayersChecked = 0
		
	INT iPlayersReady = 0
		
	FOR iPart = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		
		IF IS_PLAYER_VALID_FOR_PLAYER_LOOP(iPart)
		
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPart))
				iNumberOfPlayersChecked++
				IF IS_BIT_SET(MC_playerBD[iPart].iClientBitSet4, PBBOOL4_FMMC_YACHTS_READY)
					iPlayersReady++
					PRINTLN("[TMS] HAVE_ALL_PLAYERS_LOADED_FMMC_YACHT - Participant ", iPart, " says they're ready")
					PRINTLN("[TMS] HAVE_ALL_PLAYERS_LOADED_FMMC_YACHT - ", iPlayersReady, " out of ", iNumberOfPlayers, " participants are ready to go")
				ELSE
					PRINTLN("[TMS] HAVE_ALL_PLAYERS_LOADED_FMMC_YACHT - Participant ", iPart, " says they're NOT ready")
				ENDIF
			ENDIF
			
			IF iNumberOfPlayersChecked >= iNumberOfPlayers
				BREAKLOOP
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN iPlayersReady >= iNumberOfPlayers
ENDFUNC

PROC UPDATE_FMMC_YACHT_WARPING(INT &iScene, SceneYachtFunc customYachtCutscene)

	INT iPlayer = NATIVE_TO_INT(PLAYER_ID())
	INT iYachtID
	STRING AnimDictName = "ANIM@MP_YACHT@YACHT_TRANS@"
	STRING outboundAnimName = "OUTBOUND_CAM", inboundAnimName = "INBOUND_CAM"
	STRING outboundSoundName = "Leave_R_L", inboundSoundName = "Arrive_R_L"
	VECTOR outboundSceneOffset = <<-0.030, 57.720, -0.860>>
	VECTOR inboundSceneOffset = outboundSceneOffset
	
	DRAW_DEBUG_TEXT_2D("UPDATE_FMMC_YACHT_WARPING", <<0.05, 0.05, 0.5>>)
	
	TEXT_LABEL_63 tlDebugText
	tlDebugText = "iWarpState: "
	tlDebugText += GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState
	DRAW_DEBUG_TEXT_2D(tlDebugText,  <<0.05, 0.11, 0.5>>)
	
	
	CONST_INT iWarp_0	0		// wait for a warp to start
	CONST_INT iWarp_1	1		// load ourbound audio scene
	CONST_INT iWarp_2	2		// play establishing shot
	CONST_INT iWarp_3	3		// play outbound animated camera
	CONST_INT iWarp_4	4		// 
	CONST_INT iWarp_5	5		// 
	CONST_INT iWarp_6	6		// switch off player control and do fade
	CONST_INT iWarp_7	7		// wait for screen to fade out
	CONST_INT iWarp_8	8		// wait for server to assign new location
	CONST_INT iWarp_9	9		// do warp
	CONST_INT iWarp_10	10		// fade out for warp to beach	
	CONST_INT iWarp_96	96		// play inbound animated camera
	CONST_INT iWarp_97	97		// 
	CONST_INT iWarp_98	98		// 
	CONST_INT iWarp_99	99		// restore gameplay / cleanup
	CONST_INT iWarp_100	100		// make sure owner has finished.
	
	// wait for a warp to start
	IF (GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_0)	
		// store the owner id, only if it is valid.
		iYachtID = GET_FMMC_YACHT_ID(0)

		PRINTLN("[MYACHT] - yacht owner has started yacht warp, Yacht ID is ", iYachtID)
		
		g_SpawnData.iYachtToWarpFrom = iYachtID
		g_SpawnData.bHasAccessToYachtWarp = TRUE
		
		PRINTLN("[MYACHT] - bHasAccessToYachtWarp = ", g_SpawnData.bHasAccessToYachtWarp, ", owner = ", NATIVE_TO_INT(g_SpawnData.WarpOwnerID))
		
		Private_Get_NET_YACHT_SCENE(iYachtID, sMoveScene
			#IF IS_DEBUG_BUILD
			, &GET_OFFSET_FROM_YACHT_IN_WORLD_COORDS
			, &GET_OFFSET_HEADING_FROM_YACHT_AS_WORLD_HEADING
			#ENDIF
			)
		GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_1														
	ENDIF
	
	// load ourbound audio scene
	IF (GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_1)
	
		UpdateStoreLastVehicleOnYacht()
		
		IF NOT DOES_ANIM_DICT_EXIST(AnimDictName)
			PRINTLN("[MYACHT] - bypass establishing shot, owner = ", NATIVE_TO_INT(g_SpawnData.WarpOwnerID))
			GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_5
		ELSE
			REQUEST_ANIM_DICT(AnimDictName)
			CDEBUG1LN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_WARPING - waiting for outbound stream \"", outboundSoundName, "\"")
			IF LOAD_STREAM(outboundSoundName,"DLC_Apartment_Yacht_Streams_Soundset")
			AND REQUEST_SCRIPT_AUDIO_BANK("DLC_APARTMENT/APT_Yacht_01")
				PRINTLN("[MYACHT] - finished loading stream ", outboundSoundName, ", owner = ", NATIVE_TO_INT(g_SpawnData.WarpOwnerID))
				iYachtCutStage = 0
				GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_2
			ENDIF
		ENDIF
	ENDIF
	
	// play establishing shot
	IF (GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_2)
	
		UpdateStoreLastVehicleOnYacht()
		
		REQUEST_ANIM_DICT(AnimDictName)
		
		INT iPrevCutStage = iYachtCutStage
		IF CALL customYachtCutscene(iYachtCutStage, sMoveScene, cYachtCam1, cYachtCam2, FALSE)
			IF HAS_ANIM_DICT_LOADED(AnimDictName)
				PRINTLN("[MYACHT] - finished playing establishing shot, owner = ", NATIVE_TO_INT(g_SpawnData.WarpOwnerID))
				IF (g_SpawnData.bHasAccessToYachtWarp)
					GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_3
				ELSE
					g_SpawnData.iYachtToWarpTo = g_SpawnData.iYachtToWarpFrom
					GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_10
					NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
					PRINTLN("[MYACHT] - dont have access, so will resapwn on beacht. owener = ", NATIVE_TO_INT(g_SpawnData.WarpOwnerID))
				ENDIF
			ENDIF
		ENDIF
		
		IF iPrevCutStage = 0
			iYachtID = g_SpawnData.iYachtToWarpFrom
			PLAY_STREAM_FROM_POSITION(GET_COORDS_OF_PRIVATE_YACHT(iYachtID))
			START_AUDIO_SCENE("DLC_APT_Yacht_Leave_Scene")
			PLAY_SOUND_FROM_COORD(-1, "Leave_Horn", GET_COORDS_OF_PRIVATE_YACHT(iYachtID), "DLC_Apartment_Yacht_Streams_Soundset")
		ENDIF
	ENDIF
	
	// play outbound animated camera
	IF (GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_3)
		
		UpdateStoreLastVehicleOnYacht()
		
		HIDE_ALL_DOCKED_YACHT_VEHICLES_LOCALLY(g_SpawnData.iYachtToWarpFrom)
		
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
		START_MP_CUTSCENE(TRUE)
		SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
			
		iYachtID = g_SpawnData.iYachtToWarpFrom
		VECTOR vOutboundSceneCoord = GET_OFFSET_FROM_YACHT_IN_WORLD_COORDS(iYachtID, outboundSceneOffset)
		iScene = CREATE_SYNCHRONIZED_SCENE(
				vOutboundSceneCoord,
				<<0,0,GET_HEADING_OF_PRIVATE_YACHT(iYachtID)>>)
		SET_SYNCHRONIZED_SCENE_PHASE(iScene, 0.0)
		cYachtCam1 = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", TRUE)
		RENDER_SCRIPT_CAMS(TRUE, FALSE)
		PLAY_SYNCHRONIZED_CAM_ANIM(cYachtCam1, iScene, outboundAnimName, AnimDictName)
		
		PRINTLN("[MYACHT] - start outbound synch scene ", iScene, " camera \"", outboundAnimName, "\", \"", AnimDictName, "\", ", vOutboundSceneCoord)
		GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_4
	ENDIF
	
	IF (GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_4)
		
		HIDE_ALL_DOCKED_YACHT_VEHICLES_LOCALLY(g_SpawnData.iYachtToWarpFrom)
	
		IF IS_SYNCHRONIZED_SCENE_RUNNING(iScene)
			IF GET_SYNCHRONIZED_SCENE_PHASE(iScene) <= 0.85
				//
				#IF IS_DEBUG_BUILD
				IF g_bEditSynchSceneOffsets
					iYachtID = g_SpawnData.iYachtToWarpFrom
					SET_SYNCHRONIZED_SCENE_PHASE(iScene, g_vSynchScenePhase)
					SET_SYNCHRONIZED_SCENE_ORIGIN(iScene,
							GET_OFFSET_FROM_YACHT_IN_WORLD_COORDS(iYachtID, outboundSceneOffset+g_vSynchSceneCoordOffset),
							<<0,0,GET_HEADING_OF_PRIVATE_YACHT(iYachtID)>>+g_vSynchSceneRotOffset)
					DRAW_DEBUG_SPHERE(GET_OFFSET_FROM_YACHT_IN_WORLD_COORDS(iYachtID, outboundSceneOffset+g_vSynchSceneCoordOffset), 0.125)
				ENDIF
				#ENDIF
			ELSE
				GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_5
			ENDIF
		ELSE
			GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_5
		ENDIF
	ENDIF
	
	IF (GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_5)
	
		HIDE_ALL_DOCKED_YACHT_VEHICLES_LOCALLY(g_SpawnData.iYachtToWarpFrom)
		
		DO_SCREEN_FADE_OUT(1000)
		PRINTLN("[MYACHT] - finished synch scene ", iScene, " camera, fade out")
		GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_6
	ENDIF
	
	// switch off player control and do fade
	IF (GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_6)
		
		HIDE_ALL_DOCKED_YACHT_VEHICLES_LOCALLY(g_SpawnData.iYachtToWarpFrom)
	
		g_SpawnData.YachtWarpTimer = GET_NETWORK_TIME_ACCURATE()
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
		IF NOT IS_SCREEN_FADED_OUT()
		AND NOT IS_SCREEN_FADING_OUT()
			DO_SCREEN_FADE_OUT(500)
		ENDIF
		g_SpawnData.iYachtToWarpTo = g_SpawnData.iYachtToWarpFrom // so it has a valid number set
		GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_7
	ENDIF

	// wait for screen to fade out
	IF (GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_7)
		
		HIDE_ALL_DOCKED_YACHT_VEHICLES_LOCALLY(g_SpawnData.iYachtToWarpFrom)
		
		IF IS_SCREEN_FADED_OUT()		
		
			g_SpawnData.YachtWarpTimer = GET_NETWORK_TIME_ACCURATE()
			
//			IF IS_PLAYER_WARPING_THEIR_OWN_PRIVATE_YACHT(PLAYER_ID())				
//				vDesiredCoords = GET_COORDS_FOR_YACHT_SPAWN_ZONE(INT_TO_ENUM(PRIVATE_YACHT_SPAWN_ZONE_ENUM, g_SpawnData.iYachtZoneToWarpTo))
//				YACHT_APPEARANCE Appearance
//				GET_APPEARANCE_OF_YACHT(g_SpawnData.iYachtToWarpFrom, Appearance)
//				BROADCAST_REASSIGN_MY_PRIVATE_YACHT_NEAR_COORDS(vDesiredCoords, Appearance)
//				SET_LOCAL_PLAYER_BD_DESIRED_YACHT_LOCATION(vDesiredCoords)
//			ENDIF

			PRINTLN("[MYACHT] Moving yacht now!")
			
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iScene)
				DETACH_SYNCHRONIZED_SCENE(iScene)
			ENDIF
			iScene = -1
			
			IF DOES_CAM_EXIST(cYachtCam1)
				DESTROY_CAM(cYachtCam1)
			ENDIF
			IF DOES_CAM_EXIST(cYachtCam2)
				DESTROY_CAM(cYachtCam2)
			ENDIF
			
			// make sure we are clear of any tasks
			STOP_STREAM()
			STOP_AUDIO_SCENE("DLC_APT_Yacht_Leave_Scene")
			CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
			
			IF NETWORK_IS_IN_MP_CUTSCENE()
				NETWORK_SET_IN_MP_CUTSCENE(FALSE)
				PRINTLN("[MYACHT] - not longer setting as network cutscene, so yacht vehicles can get created..")
			ENDIF
			
			LOAD_STREAM(inboundSoundName,"DLC_Apartment_Yacht_Streams_Soundset")			
			
			GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_8
		ELSE
			IF NOT IS_SCREEN_FADING_OUT()
				DO_SCREEN_FADE_OUT(500)
			ELSE
				CDEBUG1LN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_WARPING - doing fade out.")
			ENDIF		
		ENDIF
	ENDIF
	
	// wait for server to assign new location
	IF (GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_8)
	
		sMissionYachtVars.eMissionYachtSpawnState = FMMC_YACHT_SPAWN_STATE__INIT
		DELETE_AND_CLEANUP_ASSETS_FOR_MISSION_CREATOR_YACHT(sMissionYachtData)
		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			SET_BIT(MC_serverBD.iServerBitSet7, SBBOOL7_YACHT_MOVED_TO_DESTINATION)
		ENDIF
		
		GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_9
	ENDIF
	
	// do warp
	IF (GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_9)
	
		DoLastVehicleYachtWarp()
		
		IF sMissionYachtVars.eMissionYachtSpawnState != FMMC_YACHT_SPAWN_STATE__COMPLETE
			PRINTLN("[MYACHT] Waiting for yacht to load!")
			EXIT
		ENDIF
		
		IF NOT HAVE_ALL_PLAYERS_LOADED_FMMC_YACHT()
			PRINTLN("[MYACHT] Waiting for other players")
			EXIT
		ENDIF
		
		VECTOR vCoords = <<1, 1, 1>>
		FLOAT fHeading = 11.9
		
		GetSpawnForYachtExterior(g_SpawnData.iYachtToWarpTo, vCoords, fHeading)
		//vCoords = GET_OFFSET_FROM_YACHT_IN_WORLD_COORDS(g_SpawnData.iYachtToWarpTo, g_PrivateYachtSpawnLocationOffest[i].vPlayerLoc)
		
		IF NET_WARP_TO_COORD(vCoords, fHeading, FALSE, FALSE)
			PRINTLN("[MYACHT] We've warped to the coords - moving on. || warped to ", vCoords)
			GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_96
		ELSE
			PRINTLN("[MYACHT] Waitinf ofr warp - warping to ", vCoords)
		ENDIF
	ENDIF
	
	// wait for scereen to fade out
	IF (GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_10)
		IF IS_SCREEN_FADED_OUT()
			PRINTLN("[MYACHT] - screen is faded out, do warp. ")	
			GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_9	
		ELSE
			IF NOT IS_SCREEN_FADING_OUT()
				DO_SCREEN_FADE_OUT(500)
			ELSE
				PRINTLN("[MYACHT] - screen is fadeing out... ")
			ENDIF
		ENDIF
	ENDIF
	
	// play inbound animated camera
	IF (GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_96)
		
		HIDE_ALL_DOCKED_YACHT_VEHICLES_LOCALLY(g_SpawnData.iYachtToWarpTo)
	
		IF NOT DOES_ANIM_DICT_EXIST(AnimDictName)
			PRINTLN("[MYACHT] - bypass synch scene camera")
			
			IF NOT IS_SCREEN_FADED_IN()
			AND NOT IS_SCREEN_FADING_IN()
				DO_SCREEN_FADE_IN(1000)
			ENDIF
			GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_98
		ELSE
			IF NOT LOAD_STREAM(inboundSoundName,"DLC_Apartment_Yacht_Streams_Soundset")
			//OR (MPGlobalsPrivateYacht.bUseYachtObjectsOptimisation AND NOT IS_PRIVATE_YACHT_FULLY_LOADED(g_SpawnData.iYachtToWarpTo))
				CDEBUG1LN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_WARPING - waiting for inbound stream \"", inboundSoundName, "\" to load")
			ELSE
				iYachtID = g_SpawnData.iYachtToWarpTo
				VECTOR vInboundSceneCoord = GET_OFFSET_FROM_YACHT_IN_WORLD_COORDS(iYachtID, inboundSceneOffset)
				iScene = CREATE_SYNCHRONIZED_SCENE(
						vInboundSceneCoord, <<0,0,GET_HEADING_OF_PRIVATE_YACHT(iYachtID)>>)
				SET_SYNCHRONIZED_SCENE_PHASE(iScene, 0.0)
				cYachtCam1 = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", TRUE)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				PLAY_SYNCHRONIZED_CAM_ANIM(cYachtCam1, iScene, inboundAnimName, AnimDictName)
				PRINTLN("[MYACHT] - start inbound synch scene ", iScene, " camera \"", inboundAnimName, "\", \"", AnimDictName, "\", ", vInboundSceneCoord)
				
				
				PLAY_STREAM_FROM_POSITION(GET_COORDS_OF_PRIVATE_YACHT(iYachtID))
				START_AUDIO_SCENE("DLC_APT_Yacht_Arrive_Scene")
				PLAY_SOUND_FROM_COORD(-1, "Arrive_Horn", GET_COORDS_OF_PRIVATE_YACHT(iYachtID), "DLC_Apartment_Yacht_Streams_Soundset")
				
				IF NOT IS_SCREEN_FADED_IN()
				AND NOT IS_SCREEN_FADING_IN()
					DO_SCREEN_FADE_IN(1000)
				ENDIF
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				
				GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_97
			ENDIF
		ENDIF
	ENDIF
	IF (GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_97)
	
		HIDE_ALL_DOCKED_YACHT_VEHICLES_LOCALLY(g_SpawnData.iYachtToWarpTo)
		
		IF IS_SYNCHRONIZED_SCENE_RUNNING(iScene)
			IF GET_SYNCHRONIZED_SCENE_PHASE(iScene) <= 0.95
				//
				#IF IS_DEBUG_BUILD
				IF g_bEditSynchSceneOffsets
					iYachtID = g_SpawnData.iYachtToWarpTo
					SET_SYNCHRONIZED_SCENE_PHASE(iScene, g_vSynchScenePhase)
					SET_SYNCHRONIZED_SCENE_ORIGIN(iScene,
							GET_OFFSET_FROM_YACHT_IN_WORLD_COORDS(iYachtID, inboundSceneOffset+g_vSynchSceneCoordOffset),
							<<0,0,GET_HEADING_OF_PRIVATE_YACHT(iYachtID)>>+g_vSynchSceneRotOffset)
					DRAW_DEBUG_SPHERE(GET_OFFSET_FROM_YACHT_IN_WORLD_COORDS(iYachtID, inboundSceneOffset+g_vSynchSceneCoordOffset), 0.125)
				ENDIF
				#ENDIF
				
				IF DOES_CAM_EXIST(cYachtCam1)
					IF IS_CAM_RENDERING(cYachtCam1)
						CDEBUG3LN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_WARPING - hCam0 rendering")
					ELSE
						CERRORLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_WARPING - hCam0 not rendering")
						RENDER_SCRIPT_CAMS(TRUE, FALSE)
					ENDIF
				ELSE
					CDEBUG1LN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_WARPING - hCam0 doesn't exist")
				ENDIF
				
			ELSE
				PRINTLN("[MYACHT] - finished inbound synch scene ", iScene, " - phase > 0.95")
				GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_98
			ENDIF
		ELSE
			PRINTLN("[MYACHT] - finished inbound synch scene ", iScene, " - not running")
			GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_98
		ENDIF
	ENDIF
	IF (GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_98)
		
		HIDE_ALL_DOCKED_YACHT_VEHICLES_LOCALLY(g_SpawnData.iYachtToWarpTo)
		
		PRINTLN("[MYACHT] - finished synch scene ", iScene, " camera, clean up")
		IF DOES_CAM_EXIST(cYachtCam1)
			DESTROY_CAM(cYachtCam1)
		ENDIF
		IF DOES_CAM_EXIST(cYachtCam2)
			DESTROY_CAM(cYachtCam2)
		ENDIF
		IF DOES_ANIM_DICT_EXIST(AnimDictName)
			REMOVE_ANIM_DICT(AnimDictName)
		ENDIF
		
		STOP_STREAM()
		STOP_AUDIO_SCENE("DLC_APT_Yacht_Arrive_Scene")
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_APARTMENT/APT_Yacht_01")
		
		CLEANUP_MP_CUTSCENE()
	
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
		SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
		SET_PED_CURRENT_WEAPON_VISIBLE(PLAYER_PED_ID(),TRUE)
		
		SET_GAMEPLAY_CAM_RELATIVE_HEADING()
		SET_GAMEPLAY_CAM_RELATIVE_PITCH()
		
		CLEANUP_MP_CUTSCENE()
		
		
		
		GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_99
	ENDIF
	
	// restore gameplay / cleanup
	IF (GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_99)
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		
		GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_100
		PRINTLN("[MYACHT] - going to wait for owner cleanup confirmation. ")
		
		GlobalplayerBD[iPlayer].PrivateYachtDetails.bWarpActive = FALSE
	ENDIF
	
	// make sure owner has finished.
	IF (GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_100)
	
		IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet7, SBBOOL7_YACHT_CUTSCENE_RUNNING)
			GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_0
			PRINTLN("[MYACHT] Going back to iWarp_0")
		ENDIF

		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			CLEAR_BIT(MC_serverBD.iServerBitSet7, SBBOOL7_YACHT_CUTSCENE_RUNNING)
		ENDIF
	ENDIF
	
	IF GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState > iWarp_0
		DISABLE_DPADDOWN_THIS_FRAME()
		
		DISPLAY_AMMO_THIS_FRAME(FALSE)			
		HUD_FORCE_WEAPON_WHEEL(FALSE)
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_WHEEL)
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_ICON)
		SET_CLEAR_ON_CALL_HUD_THIS_FRAME()

		DISABLE_SELECTOR_THIS_FRAME()
		
		HIDE_HUD_AND_RADAR_THIS_FRAME()
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WANTED_STARS)
		
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH)
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_MP_CASH)
		
		DISABLE_CELLPHONE_THIS_FRAME_ONLY()
		SET_CLEAR_ON_CALL_HUD_THIS_FRAME(TRUE)
		
		SET_IDLE_KICK_DISABLED_THIS_FRAME()
	ENDIF
ENDPROC

FUNC PERCENTAGE_METER_LINE GET_PERCENTAGE_METER_LINE_FROM_PERCENTAGE_INT(INT iPercent)
	
	PERCENTAGE_METER_LINE percentLine = PERCENTAGE_METER_LINE_NONE
	
	SWITCH iPercent
		CASE 0
			percentLine = PERCENTAGE_METER_LINE_NONE
		BREAK
		CASE 10
			percentLine = PERCENTAGE_METER_LINE_10
		BREAK
		CASE 20
			percentLine = PERCENTAGE_METER_LINE_20
		BREAK
		CASE 30
			percentLine = PERCENTAGE_METER_LINE_30
		BREAK
		CASE 40
			percentLine = PERCENTAGE_METER_LINE_40
		BREAK
		CASE 50
			percentLine = PERCENTAGE_METER_LINE_50
		BREAK
		CASE 60
			percentLine = PERCENTAGE_METER_LINE_60
		BREAK
		CASE 70
			percentLine = PERCENTAGE_METER_LINE_70
		BREAK
		CASE 80
			percentLine = PERCENTAGE_METER_LINE_80
		BREAK
		CASE 90
			percentLine = PERCENTAGE_METER_LINE_90
		BREAK
	ENDSWITCH
	
	RETURN percentLine
	
ENDFUNC

FUNC STRING GET_RANDOM_CLOTHING_ANIMATION_FOR_CUTSCENE(TEXT_LABEL_63 &tlDict)
	STRING sAnim = ""
	INT i = iLocalPart
	INT iRand = GET_RANDOM_INT_IN_RANGE(0, 6)
	SWITCH i
		CASE 0
			SWITCH iRand
				CASE 0 		sAnim = "try_trousers_negative_a" 	tlDict = "Clothingtrousers"	BREAK
				CASE 1 		sAnim = "try_trousers_neutral_a" 	tlDict = "Clothingtrousers" BREAK
				CASE 2 		sAnim = "try_trousers_positive_a" 	tlDict = "Clothingtrousers" BREAK
				CASE 3 		sAnim = "try_tie_negative_a" 		tlDict = "Clothingtie"		BREAK
				CASE 4 		sAnim = "try_tie_neutral_a" 		tlDict = "Clothingtie"		BREAK
				CASE 5 		sAnim = "try_tie_positive_a" 		tlDict = "Clothingtie"		BREAK
			ENDSWITCH
		BREAK
			
		CASE 1
			SWITCH iRand
				CASE 0 		sAnim = "try_trousers_negative_b" 	tlDict = "Clothingtrousers" BREAK
				CASE 1 		sAnim = "try_trousers_neutral_b" 	tlDict = "Clothingtrousers" BREAK
				CASE 2 		sAnim = "try_trousers_positive_b" 	tlDict = "Clothingtrousers" BREAK
				CASE 3 		sAnim = "try_tie_negative_b" 		tlDict = "Clothingtie"		BREAK
				CASE 4 		sAnim = "try_tie_neutral_b" 		tlDict = "Clothingtie"		BREAK
				CASE 5 		sAnim = "try_tie_positive_b" 		tlDict = "Clothingtie"		BREAK
			ENDSWITCH
		BREAK
		
		CASE 2
			SWITCH iRand
				CASE 0 		sAnim = "try_trousers_negative_c" 	tlDict = "Clothingtrousers" BREAK
				CASE 1 		sAnim = "try_trousers_neutral_c"  	tlDict = "Clothingtrousers"	BREAK
				CASE 2 		sAnim = "try_trousers_positive_c" 	tlDict = "Clothingtrousers"	BREAK
				CASE 3 		sAnim = "try_tie_negative_c" 		tlDict = "Clothingtie"		BREAK
				CASE 4 		sAnim = "try_tie_neutral_c" 		tlDict = "Clothingtie"		BREAK
				CASE 5 		sAnim = "try_tie_positive_c" 		tlDict = "Clothingtie"		BREAK
			ENDSWITCH
		BREAK
		
		CASE 3
			SWITCH iRand
				CASE 0 		sAnim = "try_trousers_negative_d"  	tlDict = "Clothingtrousers"	BREAK
				CASE 1 		sAnim = "try_trousers_neutral_d" 	tlDict = "Clothingtrousers"	BREAK
				CASE 2 		sAnim = "try_trousers_positive_d" 	tlDict = "Clothingtrousers"	BREAK
				CASE 3 		sAnim = "try_tie_negative_d" 		tlDict = "Clothingtie"		BREAK
				CASE 4 		sAnim = "try_tie_neutral_d"  		tlDict = "Clothingtie"		BREAK
				CASE 5 		sAnim = "try_tie_positive_d"  		tlDict = "Clothingtie"		BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN sAnim	
ENDFUNC

FUNC BOOL PLAY_YACHT_SCENE(INT &iCutStage, STRUCT_NET_YACHT_SCENE &scene, CAMERA_INDEX& hCam0, CAMERA_INDEX& hCam1, BOOL bCleanupAtEnd)

	SWITCH iCutStage
		CASE 0
			START_MP_CUTSCENE(FALSE) // should still see ambient vehicle in wide shot?
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
			
			IF IS_SKYSWOOP_AT_GROUND()
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
			ENDIF
			
			IF DOES_CAM_EXIST(hCam1)
				DESTROY_CAM(hCam1)
			ENDIF
			IF DOES_CAM_EXIST(hCam0)
				DESTROY_CAM(hCam0)
			ENDIF
			
			SceneTool_ExecutePan(scene.mPans[NET_YACHT_SCENE_PAN_establishing], hCam0, hCam1)
			scene.iTimer = GET_GAME_TIMER()
			
			PRINTLN("[MYACHT] PLAY_YACHT_SCENE - start camera pan.")	
			iCutStage = 1
		BREAK
		
		CASE 1
			IF GET_GAME_TIMER() > (scene.iTimer + ROUND(scene.mPans[NET_YACHT_SCENE_PAN_establishing].fDuration * 1000.0))
				PRINTLN("[MYACHT] PLAY_YACHT_SCENE - start camera hold.")	
				iCutStage = 2
			ENDIF
		BREAK
		
		CASE 2
			IF GET_GAME_TIMER() > (scene.iTimer + ROUND(scene.fExitDelay * 1000.0))
				PRINTLN("[MYACHT] PLAY_YACHT_SCENE - finished scene, cleanup.")	
				iCutStage = 3
			ENDIF
		BREAK
		
		CASE 3
			IF bCleanupAtEnd
				CLEANUP_MP_CUTSCENE()
			
				RENDER_SCRIPT_CAMS(FALSE, FALSE)
				SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
				SET_PED_CURRENT_WEAPON_VISIBLE(PLAYER_PED_ID(),TRUE)
				
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				
				IF DOES_CAM_EXIST(hCam1)
					DESTROY_CAM(hCam1)
				ENDIF
				IF DOES_CAM_EXIST(hCam0)
					DESTROY_CAM(hCam0)
				ENDIF
			ENDIF
			
			PRINTLN("[MYACHT] PLAY_YACHT_SCENE - cleanup camera data, bCleanupAtEnd:", GET_STRING_FROM_BOOL(bCleanupAtEnd))	
			iCutStage = 0
			RETURN TRUE
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ASSOCIATED_GOTO_ENTER_VEH_VALID(INT iPed, INT iIndex, VEHICLE_INDEX &vehIndex)

	IF iIndex < MAX_ASSOCIATED_GOTO_TASKS
	AND iIndex >= 0
		BOOL bReady = TRUE
		
		// This logic was added after content had been set up without caring about the rule. This check is just a safety precaution.
		IF IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_CASINO)
			INT iAssociatedRule = GET_ASSOCIATED_GOTO_TASK_DATA__ASSOCIATED_RULE(-1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].sAssociatedGotoTaskData, MC_serverBD_2.iPedGotoProgress[iPed])
			BOOL bDependsOnRule = (iAssociatedRule != -1) AND (g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAssociatedTeam != -1)
			IF bDependsOnRule
				IF (MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iAssociatedTeam] >= iAssociatedRule)
					bReady = FALSE
				ENDIF
			ENDIF
		ENDIF
		
		IF bReady
			INT iVehicle = GET_ASSOCIATED_GOTO_TASK_DATA__CHOSEN_VEHICLE(-1, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].sAssociatedGotoTaskData, iIndex)
			IF iVehicle > -1		
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iVehicle])
					vehIndex = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iVehicle])			
					IF DOES_ENTITY_EXIST(vehIndex)
					AND IS_VEHICLE_DRIVEABLE(vehIndex)
						PRINTLN("[LM] - IS_ASSOCIATED_GOTO_ENTER_VEH_VALID - Returning TRUE - iPed: ", iPed, " GoTo Progress: ", iIndex, " iVeh: ", iVehicle)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_RULE_VALID_FOR_PED_FIXATION_STATE(INT iPed)

	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule =	MC_serverBD_4.iCurrentHighestPriority[iTeam] 

	IF iRule < FMMC_MAX_RULES
		IF (iRule >= g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedFixationStartRule OR g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedFixationStartRule = -1)
		AND (iRule < g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedFixationEndRule OR g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedFixationEndRule = -1)
			PRINTLN("IS_RULE_VALID_FOR_PED_FIXATION_STATE TRUE")
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedFixationStartRule > -1
		PRINTLN("IS_RULE_VALID_FOR_PED_FIXATION_STATE - iPed: ", iPed, " iPedFixationStartRule: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedFixationStartRule)
	ENDIF
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedFixationEndRule > -1
		PRINTLN("IS_RULE_VALID_FOR_PED_FIXATION_STATE - iPed: ", iPed, " iPedFixationEndRule: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedFixationEndRule)
	ENDIF
	
	PRINTLN("IS_RULE_VALID_FOR_PED_FIXATION_STATE FALSE")
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_VEHICLE_SET_TO_WARP_ON_THIS_RULE_START(INT iRule, INT iTeam, INT iVehicle)
	IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleVehicleWarp[iRule][iVehicle] > -1
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC PROCESS_VEHICLE_WARP_ON_RULE_DATA()
	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule =	MC_serverBD_4.iCurrentHighestPriority[iTeam] 
	INT iVehicle = 0
	
	IF IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam)
		FOR iVehicle = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles-1
			
			IF IS_BIT_SET(iVehicleShouldWarpThisRuleStart, iVehicle)
				PRINTLN("[LM][Veh_Rule_warp] - iVehicle: ", iVehicle, " NEW RULE - Clearing iVehicleShouldWarpThisRuleStart.")
				CLEAR_BIT(iVehicleShouldWarpThisRuleStart, iVehicle)
			ENDIF
			
			IF IS_BIT_SET(iVehicleWarpedOnThisRule, iVehicle)
				PRINTLN("[LM][Veh_Rule_warp] - iVehicle: ", iVehicle, " NEW RULE - Clearing iVehicleWarpedOnThisRule.")
				CLEAR_BIT(iVehicleWarpedOnThisRule, iVehicle)
			ENDIF
		ENDFOR
	ENDIF
	
	IF iRule < FMMC_MAX_RULES
		FOR iVehicle = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles-1			
			IF NOT IS_BIT_SET(iVehicleWarpedOnThisRule, iVehicle)
			AND NOT IS_BIT_SET(iVehicleShouldWarpThisRuleStart, iVehicle)
				IF IS_VEHICLE_SET_TO_WARP_ON_THIS_RULE_START(iRule, iTeam, iVehicle)
					PRINTLN("[LM][Veh_Rule_warp] - iVehicle: ", iVehicle, " NEW RULE - Setting iVehicleShouldWarpThisRuleStart.")
					SET_BIT(iVehicleShouldWarpThisRuleStart, iVehicle)
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
ENDPROC

PROC CLEANUP_MODSHOP_BLIP_SETTINGS()
	IF DOES_BLIP_EXIST(biModshopBlip)
		CLEAR_ALL_BLIP_ROUTES()
		SET_BLIP_COLOUR(biModshopBlip, BLIP_COLOUR_DEFAULT)
		SET_BLIP_AS_SHORT_RANGE(biModshopBlip, TRUE)
	ENDIF
ENDPROC

FUNC FLOAT GET_PRIZE_VEHICLE_HEADING()
	RETURN 142.1 //243.5725
ENDFUNC

FUNC VECTOR GET_PRIZE_VEHICLE_POSITION()
	RETURN <<1100.0000, 220.0000, -49.45>>
ENDFUNC

FUNC MODEL_NAMES GET_PRIZE_VEHICLE_MODEL()
	RETURN THRAX
ENDFUNC

PROC MAINTAIN_PRIZE_VEHICLE_MISSION()

	IF NOT DOES_ENTITY_EXIST(objPrizedPodium)
		objPrizedPodium = GET_CLOSEST_OBJECT_OF_TYPE(<<1100.0000, 220.0000, -50.0000>>, 1.0, INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("vw_prop_vw_casino_podium_01a")), FALSE, DEFAULT, FALSE)
	ELSE
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_DisableCasinoPrizeVehicleWheel)
			fPrizedVehicleHeading = fPrizedVehicleHeading +@ 4.0
			
			IF fPrizedVehicleHeading >= 360.0
				fPrizedVehicleHeading -= 360.0
			ENDIF
			
			SET_ENTITY_HEADING(objPrizedPodium, fPrizedVehicleHeading)
		ENDIF
		
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFive, ciOptionsBS25_CasinoDisablePrizeCar)
			IF NOT DOES_ENTITY_EXIST(vehPrizedVehicle)
			
				REQUEST_MODEL(GET_PRIZE_VEHICLE_MODEL())
				
				IF HAS_MODEL_LOADED(GET_PRIZE_VEHICLE_MODEL())
					vehPrizedVehicle = CREATE_VEHICLE(GET_PRIZE_VEHICLE_MODEL(), GET_PRIZE_VEHICLE_POSITION(), GET_PRIZE_VEHICLE_HEADING(), FALSE, FALSE, TRUE)
					
					SET_ENTITY_INVINCIBLE(vehPrizedVehicle, TRUE)
					SET_VEHICLE_NOT_STEALABLE_AMBIENTLY(vehPrizedVehicle, FALSE)
					SET_VEHICLE_FULLBEAM(vehPrizedVehicle, FALSE)
					SET_VEHICLE_LIGHTS(vehPrizedVehicle, FORCE_VEHICLE_LIGHTS_OFF)
					SET_VEHICLE_DOORS_LOCKED(vehPrizedVehicle, VEHICLELOCK_LOCKED)
					SET_VEHICLE_FIXED(vehPrizedVehicle)
			        SET_ENTITY_HEALTH(vehPrizedVehicle, 1000)
			        SET_VEHICLE_ENGINE_HEALTH(vehPrizedVehicle, 1000)
			        SET_VEHICLE_PETROL_TANK_HEALTH(vehPrizedVehicle, 1000)
					SET_VEHICLE_DIRT_LEVEL(vehPrizedVehicle, 0.0)
		            SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(vehPrizedVehicle, TRUE)
					SET_VEHICLE_RESPECTS_LOCKS_WHEN_HAS_DRIVER(vehPrizedVehicle, TRUE)
					SET_ENTITY_CAN_BE_DAMAGED(vehPrizedVehicle, FALSE)
					SET_VEHICLE_RADIO_ENABLED(vehPrizedVehicle, FALSE)
					SET_ENTITY_COLLISION(vehPrizedVehicle, FALSE)
					FREEZE_ENTITY_POSITION(vehPrizedVehicle, TRUE)
					
					VEHICLE_SETUP_STRUCT_MP sData
					
					sData.VehicleSetup.iColour1 = 150
					sData.VehicleSetup.iColour2 = 150
					
					sData.VehicleSetup.iColourExtra1 = 89
					sData.VehicleSetup.iColourExtra2 = 21
					
					sData.iColour5 = 1
					sData.iColour6 = 132
					
					sData.VehicleSetup.iModIndex[MOD_LIVERY] = 0
					
					SET_VEHICLE_SETUP_MP(vehPrizedVehicle, sData, FALSE, TRUE, TRUE)
					
					ATTACH_ENTITY_TO_ENTITY(vehPrizedVehicle, objPrizedPodium, -1, <<0.0, 0.0, 0.55>>, <<0.0, 0.0, 0.0>>)
					
					SET_MODEL_AS_NO_LONGER_NEEDED(GET_PRIZE_VEHICLE_MODEL())
				ENDIF
			ELSE
				IF IS_VEHICLE_DRIVEABLE(vehPrizedVehicle)
					IF NOT IS_ENTITY_ATTACHED(vehPrizedVehicle)
						ATTACH_ENTITY_TO_ENTITY(vehPrizedVehicle, objPrizedPodium, -1, <<0.0, 0.0, 0.55>>, <<0.0, 0.0, 0.0>>)
					ENDIF
					
					// No longer freezing or rotating vehicle since it's attached to podium directly
	//				IF NOT IS_BIT_SET(iLocalBoolCheck31, LBOOL31_PRIZED_VEHICLE_FROZEN)
	//					IF HAVE_VEHICLE_MODS_STREAMED_IN(vehPrizedVehicle)
	//					AND SET_VEHICLE_ON_GROUND_PROPERLY(vehPrizedVehicle)
	//						FREEZE_ENTITY_POSITION(vehPrizedVehicle, TRUE)						
	//						SET_BIT(iLocalBoolCheck31, LBOOL31_PRIZED_VEHICLE_FROZEN)
	//					ENDIF
	//				ENDIF
					
	//				SET_ENTITY_HEADING(vehPrizedVehicle, fPrizedVehicleHeading)
					
					IF IS_BIT_SET(iLocalBoolCheck31, LBOOL31_PRIZED_VEHICLE_SHOULD_HIDE)
						IF NOT IS_BIT_SET(iLocalBoolCheck31, LBOOL31_PRIZED_VEHICLE_HIDDEN)
							SET_ENTITY_VISIBLE(vehPrizedVehicle, FALSE)
							SET_ENTITY_ALPHA(vehPrizedVehicle, 0, FALSE)
							SET_BIT(iLocalBoolCheck31, LBOOL31_PRIZED_VEHICLE_HIDDEN)
						ENDIF
					ELSE
						IF IS_BIT_SET(iLocalBoolCheck31, LBOOL31_PRIZED_VEHICLE_HIDDEN)
							SET_ENTITY_VISIBLE(vehPrizedVehicle, TRUE)
							RESET_ENTITY_ALPHA(vehPrizedVehicle)
							CLEAR_BIT(iLocalBoolCheck31, LBOOL31_PRIZED_VEHICLE_HIDDEN)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_ANY_VEHICLE_DOOR_DAMAGED(VEHICLE_INDEX viVeh)
	IF IS_VEHICLE_DOOR_DAMAGED(viVeh, SC_DOOR_BONNET)
	OR IS_VEHICLE_DOOR_DAMAGED(viVeh, SC_DOOR_BOOT)
	OR IS_VEHICLE_DOOR_DAMAGED(viVeh, SC_DOOR_FRONT_LEFT)
	OR IS_VEHICLE_DOOR_DAMAGED(viVeh, SC_DOOR_FRONT_RIGHT)
	OR IS_VEHICLE_DOOR_DAMAGED(viVeh, SC_DOOR_REAR_LEFT)
	OR IS_VEHICLE_DOOR_DAMAGED(viVeh, SC_DOOR_REAR_RIGHT)
		PRINTLN("[IS_ANY_VEHICLE_DOOR_DAMAGED][ML] A vehicle door is damaged")
		RETURN TRUE
	ELSE
		PRINTLN("[IS_ANY_VEHICLE_DOOR_DAMAGED][ML] No vehicle doors are damaged")
		RETURN FALSE
	ENDIF
ENDFUNC

FUNC BOOL CURRENT_OBJECTIVE_REPAIR_CAR(INT iPart = -1)
	INT iPartUsing = iPartToUse
	IF iPart > -1
		iPartUsing = iPart
	ENDIF
	INT iTeam = MC_playerBD[iPartUsing].iTeam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	INT iClientStage
	BOOL bRepairCar

	IF iRule < FMMC_MAX_RULES
	AND iTeam > -1
		IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMinVehHealth[iRule] > 0
			iClientStage = GET_MC_CLIENT_MISSION_STAGE(iPartUsing)
			
			IF iClientStage = CLIENT_MISSION_STAGE_DELIVER_VEH
				PRINTLN("[MMacK][RepCar] Set & Trying To Deliver")
				PLAYER_INDEX piPlayer = NETWORK_GET_PLAYER_INDEX( INT_TO_PARTICIPANTINDEX(iPartUsing) )
				IF( IS_NET_PLAYER_OK( piPlayer ) )
					PED_INDEX piTempPed = GET_PLAYER_PED( piPlayer )
					IF IS_PED_IN_ANY_VEHICLE( piTempPed )
					
						VEHICLE_INDEX viTempVeh = GET_VEHICLE_PED_IS_IN(piTempPed)
						
						INT iMissionEntityID = IS_ENTITY_A_MISSION_CREATOR_ENTITY(viTempVeh)
						
						FLOAT fPercentage
						
						IF iMissionEntityID != -1
							fPercentage = GET_VEHICLE_TOTAL_HEALTH_PERCENTAGE(viTempVeh, iMissionEntityID, MC_serverBD.iTotalNumStartingPlayers)
						ELSE
							fPercentage = GET_VEHICLE_HEALTH_PERCENTAGE(viTempVeh)
						ENDIF
						
						PRINTLN("[MMacK][RepCar] Limit is : ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMinVehHealth[iRule])
						PRINTLN("[MMacK][RepCar] Car Is : ", fPercentage)
						
						IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMinVehHealth[iRule] > fPercentage
						OR IS_ANY_VEHICLE_DOOR_DAMAGED(viTempVeh)
							PRINTLN("[MMacK][RepCar] Car Is Under Health Limit or door is missing")
							bRepairCar = TRUE
						ENDIF
					
					ENDIF
				ENDIF
			ENDIF
			
			IF iClientStage = CLIENT_MISSION_STAGE_DELIVER_VEH
			OR iClientStage = CLIENT_MISSION_STAGE_COLLECT_VEH
				IF MC_playerBD_1[iLocalPart].iVehicleNeededToRepair > -1
				AND NOT bRepairCar
					INT iMissionEntityID = MC_playerBD_1[iLocalPart].iVehicleNeededToRepair						
					NETWORK_INDEX niTempVeh = MC_serverBD_1.sFMMC_SBD.niVehicle[iMissionEntityID]
					IF NETWORK_DOES_NETWORK_ID_EXIST(niTempVeh)
						VEHICLE_INDEX viTempVeh = NET_TO_VEH(niTempVeh)
						IF DOES_ENTITY_EXIST(viTempVeh)
					
							FLOAT fPercentage						
							IF iMissionEntityID != -1
								fPercentage = GET_VEHICLE_TOTAL_HEALTH_PERCENTAGE(viTempVeh, iMissionEntityID, MC_serverBD.iTotalNumStartingPlayers)
							ELSE
								fPercentage = GET_VEHICLE_HEALTH_PERCENTAGE(viTempVeh)
							ENDIF
							
							PRINTLN("[MMacK][RepCar] (proxy) Limit is : ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMinVehHealth[iRule])
							PRINTLN("[MMacK][RepCar] (proxy) Car Is : ", fPercentage)
							
							IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMinVehHealth[iRule] > fPercentage
								PRINTLN("[MMacK][RepCar] (proxy) Car Is Under Health Limit")
								bRepairCar = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN bRepairCar
ENDFUNC

FUNC COVERPOINT_INDEX GET_CLOSEST_COVER_POINT_TO_LOCATION(VECTOR vOrigin)
	COVERPOINT_INDEX cpIndex
	FLOAT fDist = -1
	
	#IF IS_DEBUG_BUILD
	INT iReturn = -1
	#ENDIF
	
	INT i	
	FOR i = 0 TO (FMMC_MAX_NUM_COVER-1)
		IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedCoverPoints[i].vPos)
			
			PRINTLN("[LM][GET_CLOSEST_COVER_POINT_TO_LOCATION] - vOrigin: ", vOrigin, " vPos: ", g_FMMC_STRUCT_ENTITIES.sPlacedCoverPoints[i].vPos, " Distance: ", VDIST2(vOrigin, g_FMMC_STRUCT_ENTITIES.sPlacedCoverPoints[i].vPos), " Radius: ", POW(3.0, 2.0))
			
			IF VDIST2(vOrigin, g_FMMC_STRUCT_ENTITIES.sPlacedCoverPoints[i].vPos) < POW(3.0, 2.0)
				IF fDist = -1
				OR VDIST2(vOrigin, g_FMMC_STRUCT_ENTITIES.sPlacedCoverPoints[i].vPos) < fDist
					PRINTLN("[LM][GET_CLOSEST_COVER_POINT_TO_LOCATION] - Cover Point i: ", i, " is close to player.")
					cpIndex = cpPlacedCoverPoints[i]
					fDist = VDIST2(vOrigin, g_FMMC_STRUCT_ENTITIES.sPlacedCoverPoints[i].vPos)
					
					#IF IS_DEBUG_BUILD
					iReturn = i
					#ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	PRINTLN("[LM][GET_CLOSEST_COVER_POINT_TO_LOCATION] - Returning Cover Point: ", iReturn)
	
	RETURN cpIndex
ENDFUNC

PROC START_PLACED_PEDS_IN_COVER()
	
	IF NOT IS_BIT_SET(MC_ServerBD.iServerBitSet7, SBBOOL7_SET_ALL_PLACED_PEDS_INTO_COVER)
		INT i = 0
		FOR i = 0 TO FMMC_MAX_PEDS-1
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niPed[i])
				PED_INDEX tempPed = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[i])			
				IF NOT IS_PED_INJURED(tempPed)
				AND NOT IS_PED_IN_ANY_VEHICLE(tempPed)	
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[i].iPedBitsetTwelve, ciPed_BSTwelve_SpawnPedInCover)
						VECTOR vCoverCoords = GET_ENTITY_COORDS(tempPed)
						PRINTLN("[LM][RCC MISSION] START_PLACED_PEDS_IN_COVER - iPed: ", i, " Entering Cover.")
						SET_PED_TO_LOAD_COVER(tempPed, TRUE)
						COVERPOINT_INDEX cpIndex = GET_CLOSEST_COVER_POINT_TO_LOCATION(vCoverCoords)
						TASK_PUT_PED_DIRECTLY_INTO_COVER(tempPed, vCoverCoords, -1, TRUE, DEFAULT, TRUE, FALSE, cpIndex)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(tempPed, TRUE)
						SET_BIT(iPedStartedInCoverBitset[GET_LONG_BITSET_INDEX(i)], GET_LONG_BITSET_BIT(i))
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
		SET_BIT(MC_ServerBD.iServerBitSet7, SBBOOL7_SET_ALL_PLACED_PEDS_INTO_COVER)
	ENDIF
ENDPROC

FUNC PED_INDEX GET_PED_INDEX_FROM_NET_ID(NETWORK_INDEX netID)
	PED_INDEX pedTemp
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(netId)
		pedTemp = NET_TO_PED(netID)
	ENDIF	
	RETURN pedTemp	
ENDFUNC

FUNC BOOL SHOULD_PED_BE_ALLOWED_TO_START_USING_VEHICLE_WEAPON(PED_INDEX ThisPed, INT iPed)

	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetTwelve, ciPed_BSTwelve_ReenableVehicleWeaponsWhenAloneInVehicle)
		RETURN FALSE
	ENDIF
	
	IF NOT IS_PED_IN_ANY_VEHICLE(ThisPed)
		RETURN FALSE
	ENDIF
	
	VEHICLE_INDEX viVeh = GET_VEHICLE_PED_IS_USING(ThisPed)
	MODEL_NAMES mnVeh = GET_ENTITY_MODEL(viVeh)
	
	INT iSeatToCheck = 0
	INT iLivingPeds = 0
	
	FOR iSeatToCheck = ENUM_TO_INT(VS_DRIVER) TO GET_VEHICLE_MODEL_NUMBER_OF_SEATS(mnVeh) - 1
		PED_INDEX piPedInVeh
		piPedInVeh = GET_PED_IN_VEHICLE_SEAT(viVeh, INT_TO_ENUM(VEHICLE_SEAT, iSeatToCheck), TRUE)
		
		IF IS_ENTITY_ALIVE(piPedInVeh)
			iLivingPeds++
		ENDIF
	ENDFOR
	
	IF iLivingPeds = 1
		PRINTLN("SHOULD_PED_BE_ALLOWED_TO_START_USING_VEHICLE_WEAPON - Returning TRUE")
		RETURN TRUE
	ELSE
		PRINTLN("SHOULD_PED_BE_ALLOWED_TO_START_USING_VEHICLE_WEAPON - Returning FALSE")
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_PED_BE_BLOCKED_FROM_USING_VEH_WEAPON(PED_INDEX ThisPed, INT iPed)

	IF SHOULD_PED_BE_ALLOWED_TO_START_USING_VEHICLE_WEAPON(ThisPed, iPed)
		PRINTLN("SHOULD_PED_BE_BLOCKED_FROM_USING_VEH_WEAPON - SHOULD_PED_BE_ALLOWED_TO_START_USING_VEHICLE_WEAPON returning TRUE for ped ", iPed)
		RETURN FALSE
	ENDIF
	
	BOOL bReturn = IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetTwelve, ciPed_BSTwelve_PreventVehicleWeaponUsage)
	
	PRINTLN("[TMS] SHOULD_PED_BE_BLOCKED_FROM_USING_VEH_WEAPON || Ped: ", iPed, " | ", GET_STRING_FROM_BOOL(bReturn))
	RETURN bReturn
ENDFUNC

FUNC INT GET_CLOSEST_PARTICIPANT_NUMBER_ASCENDING(INT iStartFrom)
	INT iReturn = -1
	INT i
	INT iPart
	FOR i = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		iPart = i+iStartFrom
		
		IF iPart >= NUM_NETWORK_PLAYERS
			iPart -= NUM_NETWORK_PLAYERS
		ENDIF
		
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPart))
			IF NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_ANY_SPECTATOR)
			AND IS_BIT_SET(MC_playerBD[iPart].iClientBitset2, PBBOOL2_REACHED_GAME_STATE_RUNNING)
				iReturn = iPart
				BREAKLOOP
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN iReturn
ENDFUNC

FUNC INT GET_RANDOM_PARTICIPANT_ON_TEAM(INT iTeam)

	PRINTLN("[LM][GET_RANDOM_PARTICIPANT_ON_TEAM] - iTeamToAssign: ", iTeam)
	
	INT iScorePartHighest = -1
	INT iPartSaved = -1
	INT iPart = 0
	FOR iPart = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		PARTICIPANT_INDEX partIndex = INT_TO_PARTICIPANTINDEX(iPart)
		
		PRINTLN("[LM][GET_RANDOM_PARTICIPANT_ON_TEAM] - iPart: ", iPart, " iTeam: ", MC_PlayerBD[iPart].iTeam, " currentserverPart: ", MC_serverBD_3.iServerTagTeamPartTurn[iTeam])
		
		IF NETWORK_IS_PARTICIPANT_ACTIVE(partIndex)
			PLAYER_INDEX piPlayer = NETWORK_GET_PLAYER_INDEX(partIndex)	
			IF NOT DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(piPlayer)
			AND NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_HEIST_SPECTATOR)
			AND NOT IS_BIT_SET(MC_playerBD[iPart].iClientBitSet, PBBOOL_ANY_SPECTATOR)
				IF MC_PlayerBD[iPart].iTeam = iTeam
				AND MC_serverBD_3.iServerTagTeamPartTurn[iTeam] != iPart
				AND IS_BIT_SET(MC_PlayerBD[iPart].iClientBitset4, PBBOOL4_ASSIGNED_PLAYER_TEAM)
					INT iScore = GET_RANDOM_INT_IN_RANGE(0, 100)			
					IF iScore > iScorePartHighest
						iScorePartHighest = iScore
						iPartSaved = iPart
					ELSE
						PRINTLN("[LM][GET_RANDOM_PARTICIPANT_ON_TEAM]- iPart: ", iPart, " Score Error")
					ENDIF
				ELSE
					PRINTLN("[LM][GET_RANDOM_PARTICIPANT_ON_TEAM] - iPart: ", iPart, " Team Error, their team is: ", MC_PlayerBD[iPart].iTeam)
				ENDIF
			ELSE
				PRINTLN("[LM][GET_RANDOM_PARTICIPANT_ON_TEAM] - iPart: ", iPart, " Is a Spectator.")
			ENDIF
		ELSE
			PRINTLN("[LM][GET_RANDOM_PARTICIPANT_ON_TEAM] - iPart: ", iPart, " Inactive Part.")
		ENDIF
	ENDFOR
	
	PRINTLN("[LM][GET_RANDOM_PARTICIPANT_ON_TEAM] - Returning iPartSaved: ", iPartSaved)
	
	RETURN iPartSaved
ENDFUNC

PROC SERVER_INIT_TAG_TEAM_TURN_ROTOR()
	INT iTeam = 0
	FOR iTeam = 0 TO FMMC_MAX_TEAMS-1
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwenty, ciOptionsBS20_Enable_Turn_Rotor_Tag_Team)
		AND MC_serverBD_3.iServerTagTeamPartTurn[iTeam] = -1						
			MC_serverBD_3.iServerTagTeamPartTurn[iTeam] = GET_RANDOM_PARTICIPANT_ON_TEAM(iTeam)
			PRINTLN("[LM][PROCESS_TAG_TEAM_MODE][TAG_TEAM_TURNS][PROCESS_SERVER] - Choosing a Participant at random for iTeam: ", iTeam, ". Chose iServerTagTeamPartTurn: ", MC_serverBD_3.iServerTagTeamPartTurn[iTeam])
		ENDIF
	ENDFOR
ENDPROC

#IF IS_DEBUG_BUILD
FUNC STRING GET_PED_SPOOK_REASON_FOR_DEBUG(INT iReason)
	
	SWITCH iReason
		CASE ciSPOOK_NONE										RETURN "NONE"	
		CASE ciSPOOK_PLAYER_WITH_WEAPON							RETURN "PLAYER_WITH_WEAPON"
		CASE ciSPOOK_PLAYER_BLOCKED_VEHICLE						RETURN "PLAYER_BLOCKED_VEHICLE"
		CASE ciSPOOK_PLAYER_CAUSED_EXPLOSION					RETURN "PLAYER_CAUSED_EXPLOSION"
		CASE ciSPOOK_PLAYER_POTENTIAL_RUN_OVER					RETURN "PLAYER_POTENTIAL_RUN_OVER"
		CASE ciSPOOK_PLAYER_AIMED_AT							RETURN "PLAYER_AIMED_AT"
		CASE ciSPOOK_SHOTS_FIRED								RETURN "SHOTS_FIRED"
		CASE ciSPOOK_DAMAGED									RETURN "DAMAGED"
		CASE ciSPOOK_PLAYER_TOUCHING_ME							RETURN "PLAYER_TOUCHING_ME"
		CASE ciSPOOK_PLAYER_CAR_CRASH							RETURN "PLAYER_CAR_CRASH"
		CASE ciSPOOK_PLAYER_TOUCHING_MY_VEHICLE					RETURN "TOUCHING_MY_VEHICLE"
		CASE ciSPOOK_PLAYER_HEARD								RETURN "PLAYER_HEARD"
		CASE ciSPOOK_SEEN_BODY									RETURN "SEEN_BODY"
		CASE ciSPOOK_SEEN_PLAYER_NOT_IN_SECONDARY_ANIM			RETURN "SEEN_PLAYER_NOT_IN_SECONDARY_ANIM"
		CASE ciSPOOK_VEHICLE_ATTACHED							RETURN "VEHICLE_ATTACHED"
	ENDSWITCH
	
	RETURN "INVALID"
ENDFUNC
PROC SET_PED_IS_SPOOKED_DEBUG(INT iPed, BOOL bAlertedFromAll = FALSE, INT iReason = 0)
	IF iPedSpookFrameDebugWindow[iPed] = 0
		iPedSpookFrameDebugWindow[iPed] = GET_FRAME_COUNT()
	ENDIF
	IF bAlertedFromAll
		SET_BIT(iPedBSDebugWindow_SpookedFromAll[GET_LONG_BITSET_INDEX(iPed)], GET_LONG_BITSET_BIT(iPed))
	ENDIF
	IF iPedSpookOrderDebugWindow[iPed] = 0
		iPedSpookOrderDebugWindow[iPed] = (iPedsSpookedDebugWindow+1)
		iPedsSpookedDebugWindow++
		iPedSpookReasonDebugWindow[iPed] = iReason
	ENDIF
ENDPROC
PROC CLEAR_PED_IS_SPOOKED_DEBUG(INT iPed)
	iPedSpookFrameDebugWindow[iPed] = 0
	CLEAR_BIT(iPedBSDebugWindow_SpookedFromAll[GET_LONG_BITSET_INDEX(iPed)], GET_LONG_BITSET_BIT(iPed))
	iPedSpookOrderDebugWindow[iPed] = 0	
	iPedSpookReasonDebugWindow[iPed] = 0
ENDPROC
PROC SET_PED_IS_AGGROD_DEBUG(INT iPed, BOOL bAlertedFromAll = FALSE)
	IF iPedAggroFrameDebugWindow[iPed] = 0
		iPedAggroFrameDebugWindow[iPed] = GET_FRAME_COUNT()
	ENDIF
	IF bAlertedFromAll
		SET_BIT(iPedBSDebugWindow_AggroedFromAll[GET_LONG_BITSET_INDEX(iPed)], GET_LONG_BITSET_BIT(iPed))
	ENDIF
	IF iPedAggroOrderDebugWindow[iPed] = 0
		iPedAggroOrderDebugWindow[iPed] = (iPedsAggroedDebugWindow+1)
		iPedsAggroedDebugWindow++		
	ENDIF
ENDPROC
PROC CLEAR_PED_IS_AGGROD_DEBUG(INT iPed)	
	iPedAggroFrameDebugWindow[iPed] = 0
	CLEAR_BIT(iPedBSDebugWindow_AggroedFromAll[GET_LONG_BITSET_INDEX(iPed)], GET_LONG_BITSET_BIT(iPed))
	iPedAggroOrderDebugWindow[iPed] = 0	
ENDPROC
#ENDIF

FUNC BOOL SHOULD_LOAD_ELECTRONIC_ASSETS()
	INT iObj
	FOR iObj = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfObjects - 1
		IF IS_THIS_MODEL_A_FUSEBOX(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].mn)
		OR IS_THIS_MODEL_A_CCTV_CAMERA(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].mn)
			RETURN TRUE
		ENDIF
	ENDFOR
	
	INT iDynoprop
	FOR iDynoprop = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps - 1
		IF IS_THIS_MODEL_A_FUSEBOX(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoprop].mn)
		OR IS_THIS_MODEL_A_CCTV_CAMERA(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoprop].mn)
			RETURN TRUE
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC

PROC CLEANUP_CCTV_CAMERA_SOUND_EFFECT_FOR_DEATH(INT iCam)
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_CCTVAlertSpottedSounds)
		IF iSoundAlarmCCTV[iCam] > -1
		AND NOT HAS_SOUND_FINISHED(iSoundAlarmCCTV[iCam])
			PRINTLN("[LM][PROCESS_CCTV_CAMERA_ALARM_SOUNDS] - Camera is destroyed for iCam ", iCam, " Stopping all sounds.")
			STOP_SOUND(iSoundAlarmCCTV[iCam])
			SCRIPT_RELEASE_SOUND_ID(iSoundAlarmCCTV[iCam])
			iSoundAlarmType[iCam] = -1
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_PLAYER_HOLDING_METAL_DETECTOR_EXTREME_WEAPON()
	IF HAS_PED_GOT_WEAPON(LocalPlayerPed, WEAPONTYPE_MICROSMG)
	OR HAS_PED_GOT_WEAPON(LocalPlayerPed, WEAPONTYPE_DLC_DBSHOTGUN)
	OR HAS_PED_GOT_WEAPON(LocalPlayerPed, WEAPONTYPE_DLC_MACHINEPISTOL)
	OR HAS_PED_GOT_WEAPON(LocalPlayerPed, WEAPONTYPE_DLC_AUTOSHOTGUN)
	OR HAS_PED_GOT_WEAPON(LocalPlayerPed, WEAPONTYPE_DLC_COMPACTRIFLE)
	OR HAS_PED_GOT_WEAPON(LocalPlayerPed, WEAPONTYPE_SAWNOFFSHOTGUN)
	OR HAS_PED_GOT_WEAPON(LocalPlayerPed, WEAPONTYPE_CARBINERIFLE)
	OR HAS_PED_GOT_WEAPON(LocalPlayerPed, WEAPONTYPE_ASSAULTSHOTGUN)
	OR HAS_PED_GOT_WEAPON(LocalPlayerPed, WEAPONTYPE_DLC_SMG_MK2)
	OR HAS_PED_GOT_WEAPON(LocalPlayerPed, WEAPONTYPE_DLC_BULLPUPRIFLE_MK2)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PLAY_METAL_DETECTOR_SOUND(VECTOR vDetectionPos, BOOL bExtremeWeapon)
	IF bExtremeWeapon
		PLAY_SOUND_FROM_COORD(-1, "Metal_Detector_Big_Guns", vDetectionPos, "dlc_ch_heist_finale_security_alarms_sounds")
	ELSE
		PLAY_SOUND_FROM_COORD(-1, "Metal_Detector_Small_Guns", vDetectionPos, "dlc_ch_heist_finale_security_alarms_sounds")
	ENDIF
ENDPROC

PROC SET_METAL_DETECTOR_AS_ALERTED(INT iZone, VECTOR vDetectionPos, BOOL bExtremeWeapon)
	UNUSED_PARAMETER(vDetectionPos) //Will later be used to get props/lights etc
	
	PLAY_METAL_DETECTOR_SOUND(vDetectionPos, bExtremeWeapon)
	
	IF NOT IS_BIT_SET(iMetalDetectorZoneHasBeenAlertedBS, iZone)
		PRINTLN("[MetalDetector] SET_METAL_DETECTOR_AS_ALERTED || iZone: ", iZone, " | vDetectionPos: ", vDetectionPos)
		SET_BIT(iMetalDetectorZoneHasBeenAlertedBS, iZone)
		
		IF bExtremeWeapon
			SET_BIT(iLocalBoolCheck25, LBOOL25_A_PLAYER_HAS_SUPER_ALERTED_METAL_DETECTOR)
			PRINTLN("[MetalDetector] Setting LBOOL25_A_PLAYER_HAS_SUPER_ALERTED_METAL_DETECTOR")
		ENDIF
		
		INT iPed
		FOR iPed = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfPeds - 1
			NETWORK_INDEX niPed = MC_serverBD_1.sFMMC_SBD.niPed[iPed]
			
			IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niPed)
			OR NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(niPed)
				RELOOP
			ENDIF
			
			PED_INDEX piPed = NET_TO_PED(niPed)
			IF NOT IS_ENTITY_ALIVE(piPed)
				RELOOP
			ENDIF
			
			IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iAssociatedAggroZone != iZone
			AND VDIST2(GET_ENTITY_COORDS(piPed), vDetectionPos) > POW(25.0, 2.0)
				PRINTLN("[MetalDetector] Ped ", iPed, " isn't bothered about zone ", iZone)
				RELOOP
			ENDIF
			
			fPedHeadingBeforeMetalDetector[iPed] = GET_ENTITY_HEADING(piPed)
			REINIT_NET_TIMER(stPedMetalDetectorInvestigationTimer[iPed])
			
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(niPed)
				TASK_LOOK_AT_COORD(piPed, vDetectionPos, 3000, SLF_USE_TORSO | SLF_WHILE_NOT_IN_FOV | SLF_FAST_TURN_RATE, SLF_LOOKAT_VERY_HIGH)
				TASK_TURN_PED_TO_FACE_COORD(piPed, vDetectionPos, 2000)
				PRINTLN("[MetalDetector] Tasking ped ", iPed, " to look at coords ", vDetectionPos)
			ENDIF
		ENDFOR
	ENDIF
ENDPROC

FUNC BOOL IS_PHONE_EMP_AVAILABLE(BOOL bCheckHeistLeader = TRUE)
	
	#IF IS_DEBUG_BUILD
	IF IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD1)
		RETURN TRUE
	ENDIF
	#ENDIF
	
	// Only available for Heist Leader
	IF bCheckHeistLeader
	AND NOT IS_FAKE_MULTIPLAYER_MODE_SET()			//If we're in the Creator, ignore this check
	AND MC_serverBD.iNumberOfPlayingPlayers[0] > 1	//If we're testing in freemode but alone, ignore this check
		IF NOT GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
			RETURN FALSE
		ENDIF
	ENDIF
	
	// Casino Setup Check
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_PhoneEMPAvailable_CasinoPrep)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PHONE_EMP_CURRENTLY_ACTIVE()
	RETURN IS_BIT_SET(iLocalBoolCheck31, LBOOL31_PHONE_EMP_IS_ACTIVE)
ENDFUNC

FUNC BOOL HAS_PHONE_EMP_BEEN_USED()
	RETURN IS_BIT_SET(MC_serverBD_1.sLEGACYMissionContinuityVars.iGenericTrackingBitset, ciLEGACY_ContinuityGenericTrackingBS_EMPUsed)
ENDFUNC

FUNC BOOL IS_PHONE_EMP_TIMER_RUNNING()
	RETURN HAS_NET_TIMER_STARTED(stPhoneEMP_DeactivationTimer) AND NOT HAS_NET_TIMER_EXPIRED(stPhoneEMP_DeactivationTimer, g_FMMC_STRUCT.sPhoneEMPSettings.iPhoneEMP_Duration)
ENDFUNC

PROC PLAY_TASER_PTFX_ON_ENTITY(ENTITY_INDEX eiEntity, INT iIndex, FMMC_TASERED_ENTITY_VARS& sTaserVars)
	FLOAT fScale = 1.0
	VECTOR vOffset = <<0,0,0>>
	STRING sEffectName = "scr_ch_finale_fusebox_overload"
	BOOL bLooping = FALSE
	
	IF IS_THIS_MODEL_A_FUSEBOX(GET_ENTITY_MODEL(eiEntity))
		fScale = 1.0
		vOffset = <<0,0,0>>
		sEffectName = "scr_ch_finale_fusebox_overload"
		bLooping = FALSE
		
	ELIF IS_THIS_MODEL_A_CCTV_CAMERA(GET_ENTITY_MODEL(eiEntity))
		fScale = 1.0
		vOffset = <<0, 0.0, 0.0>>
		sEffectName = "scr_ch_finale_camera_stun"
		bLooping = TRUE
		
	ENDIF
	
	PRINTLN("[Taser] Playing Taser effect!")
	USE_PARTICLE_FX_ASSET("scr_ch_finale")
	
	IF bLooping
		IF DOES_PARTICLE_FX_LOOPED_EXIST(sTaserVars.ptLoopingTaserFX[iIndex])
			PRINTLN("[Taser] ptLoopingTaserFX already exists! Exiting now")
			EXIT
		ENDIF
		
		sTaserVars.ptLoopingTaserFX[iIndex] = START_PARTICLE_FX_LOOPED_ON_ENTITY(sEffectName, eiEntity, vOffset, <<0,0,0>>, fScale)
	ELSE
		START_PARTICLE_FX_NON_LOOPED_ON_ENTITY(sEffectName, eiEntity, vOffset, <<0,0,0>>, fScale)
	ENDIF
	
ENDPROC

FUNC BOOL CAN_ENTITY_BE_TASERED(ENTITY_INDEX eiEntity)
	IF IS_THIS_MODEL_A_FUSEBOX(GET_ENTITY_MODEL(eiEntity))
	OR IS_THIS_MODEL_A_CCTV_CAMERA(GET_ENTITY_MODEL(eiEntity))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_ENTITY_BEEN_HIT_BY_TASER(INT iIndex, FMMC_ELECTRONIC_ENTITY_TYPE eElectronicType)
	SWITCH eElectronicType
		CASE FMMC_ELECTRONIC_ENTITY_TYPE__OBJECT
			RETURN IS_BIT_SET(sObjTaserVars.iHitByTaserBS, iIndex)
			
		CASE FMMC_ELECTRONIC_ENTITY_TYPE__DYNOPROP
			RETURN IS_BIT_SET(sDynoPropTaserVars.iHitByTaserBS, iIndex)
			
	ENDSWITCH
	
	PRINTLN("[Taser] HAS_ENTITY_BEEN_HIT_BY_TASER | INVALID!!")
	ASSERTLN("[Taser] HAS_ENTITY_BEEN_HIT_BY_TASER | INVALID!!")
	RETURN FALSE
ENDFUNC
FUNC INT GET_ENTITY_TASER_TIMER_DIFFERENCE_WITH_CURRENT_TIME(INT iIndex, FMMC_ELECTRONIC_ENTITY_TYPE eElectronicType)
	SWITCH eElectronicType
		CASE FMMC_ELECTRONIC_ENTITY_TYPE__OBJECT
			IF HAS_NET_TIMER_STARTED(sObjTaserVars.stHitByTaserTimers[iIndex])
				RETURN GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(sObjTaserVars.stHitByTaserTimers[iIndex])
			ENDIF
		BREAK
			
		CASE FMMC_ELECTRONIC_ENTITY_TYPE__DYNOPROP
			IF HAS_NET_TIMER_STARTED(sDynoPropTaserVars.stHitByTaserTimers[iIndex])
				RETURN GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(sDynoPropTaserVars.stHitByTaserTimers[iIndex])
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN -1
ENDFUNC

PROC SET_ENTITY_HIT_BY_TASER(INT iIndex, ENTITY_INDEX eiEntity, FMMC_ELECTRONIC_ENTITY_TYPE eElectronicType, BOOL bPlayPTFX = TRUE)
	
	IF NOT CAN_ENTITY_BE_TASERED(eiEntity)
		EXIT
	ENDIF
	
	IF HAS_ENTITY_BEEN_HIT_BY_TASER(iIndex, eElectronicType)
		EXIT
	ENDIF
	
	// Set vars & start timers
	SWITCH eElectronicType
		CASE FMMC_ELECTRONIC_ENTITY_TYPE__OBJECT
		
			IF bPlayPTFX
				PLAY_TASER_PTFX_ON_ENTITY(eiEntity, iIndex, sObjTaserVars)
			ENDIF
	
			SET_BIT(sObjTaserVars.iHitByTaserBS, iIndex)
			REINIT_NET_TIMER(sObjTaserVars.stHitByTaserTimers[iIndex])
			PRINTLN("[Taser] SET_ENTITY_HIT_BY_TASER | Obj ", iIndex, " has been hit!")
			
		BREAK
			
		CASE FMMC_ELECTRONIC_ENTITY_TYPE__DYNOPROP
		
			IF bPlayPTFX
				PLAY_TASER_PTFX_ON_ENTITY(eiEntity, iIndex, sDynoPropTaserVars)
			ENDIF
			
			SET_BIT(sDynoPropTaserVars.iHitByTaserBS, iIndex)
			REINIT_NET_TIMER(sDynoPropTaserVars.stHitByTaserTimers[iIndex])
			PRINTLN("[Taser] SET_ENTITY_HIT_BY_TASER | Dynoprop ", iIndex, " has been hit!")
			
		BREAK
	ENDSWITCH
	
	IF IS_THIS_MODEL_A_FUSEBOX(GET_ENTITY_MODEL(eiEntity))
		iFuseBoxesCurrentlyTasered++
		PLAY_SOUND_FROM_COORD(-1, "Security_Box_Offline_Tazer", GET_ENTITY_COORDS(eiEntity), "dlc_ch_heist_finale_security_alarms_sounds", TRUE, 800)
		PRINTLN("[Taser] SET_ENTITY_HIT_BY_TASER | iFuseBoxesCurrentlyTasered is now ", iFuseBoxesCurrentlyTasered)
		
	ELIF IS_THIS_MODEL_A_CCTV_CAMERA(GET_ENTITY_MODEL(eiEntity))
		iCCTVCamsCurrentlyTasered++
		PLAY_SOUND_FROM_COORD(-1, "Camera_Offline", GET_ENTITY_COORDS(eiEntity), "dlc_ch_heist_finale_security_alarms_sounds", TRUE, 800)
		PRINTLN("[Taser] SET_ENTITY_HIT_BY_TASER | iCCTVCamsCurrentlyTasered is now ", iCCTVCamsCurrentlyTasered)
		
	ENDIF
ENDPROC
PROC CLEAR_ENTITY_HIT_BY_TASER(INT iIndex, FMMC_ELECTRONIC_ENTITY_TYPE eElectronicType)

	MODEL_NAMES mnEntityModel = DUMMY_MODEL_FOR_SCRIPT
	VECTOR vPosition
	
	SWITCH eElectronicType
		CASE FMMC_ELECTRONIC_ENTITY_TYPE__OBJECT
		
			IF DOES_PARTICLE_FX_LOOPED_EXIST(sObjTaserVars.ptLoopingTaserFX[iIndex])
				STOP_PARTICLE_FX_LOOPED(sObjTaserVars.ptLoopingTaserFX[iIndex], TRUE)
			ENDIF
			
			mnEntityModel = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iIndex].mn
			vPosition = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iIndex].vPos
			CLEAR_BIT(sObjTaserVars.iHitByTaserBS, iIndex)
			PRINTLN("[Taser] CLEAR_ENTITY_HIT_BY_TASER | Obj ", iIndex, " taser bit has been cleared!")
		BREAK
			
		CASE FMMC_ELECTRONIC_ENTITY_TYPE__DYNOPROP
		
			IF DOES_PARTICLE_FX_LOOPED_EXIST(sDynoPropTaserVars.ptLoopingTaserFX[iIndex])
				STOP_PARTICLE_FX_LOOPED(sDynoPropTaserVars.ptLoopingTaserFX[iIndex], TRUE)
			ENDIF
			
			mnEntityModel = g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iIndex].mn
			vPosition = g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iIndex].vPos
			CLEAR_BIT(sDynoPropTaserVars.iHitByTaserBS, iIndex)
			PRINTLN("[Taser] CLEAR_ENTITY_HIT_BY_TASER | Dynoprop ", iIndex, " taser bit has been cleared!")
		BREAK
			
	ENDSWITCH
	
	
	
	IF IS_THIS_MODEL_A_FUSEBOX(mnEntityModel)
		iFuseBoxesCurrentlyTasered--
		PLAY_SOUND_FROM_COORD(-1, "Security_Box_Online", vPosition, "dlc_ch_heist_finale_security_alarms_sounds", TRUE, 800)
		PRINTLN("[Taser] CLEAR_ENTITY_HIT_BY_TASER | iFuseBoxesCurrentlyTasered is now ", iFuseBoxesCurrentlyTasered)
		
	ELIF IS_THIS_MODEL_A_CCTV_CAMERA(mnEntityModel)
		iCCTVCamsCurrentlyTasered--
		PLAY_SOUND_FROM_COORD(-1, "Camera_Online", vPosition, "dlc_ch_heist_finale_security_alarms_sounds", TRUE, 800)
		PRINTLN("[Taser] CLEAR_ENTITY_HIT_BY_TASER | iCCTVCamsCurrentlyTasered is now ", iCCTVCamsCurrentlyTasered)
		
	ENDIF
ENDPROC

PROC SET_ENTITY_HAS_REACTED_TO_TASER(INT iIndex, FMMC_ELECTRONIC_ENTITY_TYPE eElectronicType)
	SWITCH eElectronicType
		CASE FMMC_ELECTRONIC_ENTITY_TYPE__OBJECT
			SET_BIT(sObjTaserVars.iReactedToTaserBS, iIndex)
		BREAK
		
		CASE FMMC_ELECTRONIC_ENTITY_TYPE__DYNOPROP
			SET_BIT(sDynoPropTaserVars.iReactedToTaserBS, iIndex)
		BREAK
		
	ENDSWITCH
ENDPROC
PROC CLEAR_ENTITY_HAS_REACTED_TO_TASER(INT iIndex, FMMC_ELECTRONIC_ENTITY_TYPE eElectronicType)
	SWITCH eElectronicType
		CASE FMMC_ELECTRONIC_ENTITY_TYPE__OBJECT
			CLEAR_BIT(sObjTaserVars.iReactedToTaserBS, iIndex)
		BREAK
		
		CASE FMMC_ELECTRONIC_ENTITY_TYPE__DYNOPROP
			CLEAR_BIT(sDynoPropTaserVars.iReactedToTaserBS, iIndex)
		BREAK
		
	ENDSWITCH
ENDPROC
FUNC BOOL HAS_ENTITY_REACTED_TO_TASER(INT iIndex, FMMC_ELECTRONIC_ENTITY_TYPE eElectronicType)
	SWITCH eElectronicType
		CASE FMMC_ELECTRONIC_ENTITY_TYPE__OBJECT
			RETURN IS_BIT_SET(sObjTaserVars.iReactedToTaserBS, iIndex)
			
		CASE FMMC_ELECTRONIC_ENTITY_TYPE__DYNOPROP
			RETURN IS_BIT_SET(sDynoPropTaserVars.iReactedToTaserBS, iIndex)
	ENDSWITCH
	
	PRINTLN("[Taser] HAS_ENTITY_REACTED_TO_TASER | INVALID!!")
	ASSERTLN("[Taser] HAS_ENTITY_REACTED_TO_TASER | INVALID!!")
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_ENTITY_TASER_TIMER_EXPIRED(INT iIndex, FMMC_ELECTRONIC_ENTITY_TYPE eElectronicType)
	SWITCH eElectronicType
		CASE FMMC_ELECTRONIC_ENTITY_TYPE__OBJECT
			RETURN HAS_NET_TIMER_EXPIRED(sObjTaserVars.stHitByTaserTimers[iIndex], g_FMMC_STRUCT_ENTITIES.sPlacedObject[iIndex].sObjElectronicData.iTaserDisableTime)
			
		CASE FMMC_ELECTRONIC_ENTITY_TYPE__DYNOPROP
			RETURN HAS_NET_TIMER_EXPIRED(sDynoPropTaserVars.stHitByTaserTimers[iIndex], g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iIndex].sDynoPropElectronicData.iTaserDisableTime)
	ENDSWITCH
	
	PRINTLN("[Taser] HAS_ENTITY_TASER_TIMER_EXPIRED | INVALID!!")
	ASSERTLN("[Taser] HAS_ENTITY_TASER_TIMER_EXPIRED | INVALID!!")
	RETURN FALSE
ENDFUNC

PROC PROCESS_TASERED_ENTITY(FMMC_TASERED_ENTITY_VARS& sTaserVars, FMMC_ELECTRONIC_DATA& sElectronicData, ENTITY_INDEX eiEntity, INT iIndex, FMMC_ELECTRONIC_ENTITY_TYPE eType)

	UNUSED_PARAMETER(sElectronicData)
	UNUSED_PARAMETER(eiEntity)

	IF HAS_NET_TIMER_STARTED(sTaserVars.stHitByTaserTimers[iIndex])
		IF HAS_ENTITY_TASER_TIMER_EXPIRED(iIndex, eType)
			CLEAR_ENTITY_HIT_BY_TASER(iIndex, eType)
			RESET_NET_TIMER(sTaserVars.stHitByTaserTimers[iIndex])
			PRINTLN("[Taser] Taser timer has expired for entity ", iIndex)
		ELSE
			#IF IS_DEBUG_BUILD
			IF bObjectTaserDebug
				TEXT_LABEL_63 sTaserDebug = "TASED | "
				INT iTimeLeft = (sElectronicData.iTaserDisableTime - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(sTaserVars.stHitByTaserTimers[iIndex]))
				sTaserDebug += (iTimeLeft / 1000)
				DRAW_DEBUG_TEXT_ABOVE_ENTITY(eiEntity, sTaserDebug, 0.1, 255, 255, 255)
			ENDIF
			#ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_ELECTRONIC_BE_DISABLED_THIS_FRAME(INT iIndex, FMMC_ELECTRONIC_ENTITY_TYPE eType)
	
	SWITCH eType
		CASE FMMC_ELECTRONIC_ENTITY_TYPE__OBJECT
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iIndex].sObjElectronicData.iElectronicBS, ciELECTRONIC_BS_AffectedByEMP)
				IF IS_PHONE_EMP_CURRENTLY_ACTIVE()
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
		
		CASE FMMC_ELECTRONIC_ENTITY_TYPE__DYNOPROP
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iIndex].sDynopropElectronicData.iElectronicBS, ciELECTRONIC_BS_AffectedByEMP)
				IF IS_PHONE_EMP_CURRENTLY_ACTIVE()
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	IF HAS_ENTITY_BEEN_HIT_BY_TASER(iIndex, eType)
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

PROC SET_MISSION_INIT_COMPLETE_GLOBAL()
	IF MC_playerBD[iLocalPart].iGameState >= GAME_STATE_INI
	AND MC_playerBD[iLocalPart].iGameState <= GAME_STATE_RUNNING
		IF NOT (g_bMissionInitComplete)
			g_bMissionInitComplete = TRUE 
			PRINTLN("g_bMissionInitComplete = TRUE")
		ENDIF
	ELSE
		IF (g_bMissionInitComplete)
			g_bMissionInitComplete = FALSE 
			PRINTLN("g_bMissionInitComplete = FALSE")
		ENDIF
	ENDIF
ENDPROC

FUNC INT COUNT_SET_BITS_IN_RANGE(INT iBitSet, INT iFirstBit = 0, INT iLastBit = 31)
	INT iCount = 0
	
	INT i
	
	FOR i = iFirstBit TO iLastBit
		IF IS_BIT_SET(iBitSet, i)
			iCount++
		ENDIF
	ENDFOR
	
	RETURN iCount
ENDFUNC

// [FIX_2020_CONTROLLER] - Simplify and improve readability of this function. Address the way the creator sets 1-10 instead of 0-9
PROC SEED_SPAWN_SUBGROUP(INT iParentGroup)
	
	INT iParentArray = iParentGroup - 1
	
	INT iMaxNum = g_FMMC_STRUCT.iRandomEntitySpawnSub_MaxNumber[iParentArray]
	INT iNumToChoose = GET_NUMBER_OF_RANDOM_ENTITY_SPAWN_GROUPS_TO_SPAWN(iParentArray)
	
	IF (iMaxNum > 0	AND iNumToChoose > 0)
		
		PRINTLN("[RCC MISSION][ran] INIT_SERVER_DATA / SEED_SPAWN_SUBGROUP - Getting randomised entity subgroup spawn seeds for iParentGroup ",iParentGroup,": number of available variations = ",iMaxNum,", number of randoms to seed = ",iNumToChoose)
		
		PRINTLN("[RCC MISSION][ran] INIT_SERVER_DATA / SEED_SPAWN_SUBGROUP, (BEFORE) iParentGroup ",iParentGroup," - MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeed_SubGroupBS[",iParentArray,"] = ",MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeed_SubGroupBS[iParentArray])
		
		INT iSeed
		
		FOR iSeed = 0 TO (iNumToChoose - 1)
			IF iSeed < iMaxNum // No need to make more seeds than numbers that exist!
				// Ensure there are free bits to be randomly set
				IF NOT ((COUNT_SET_BITS_IN_RANGE(MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeed_SubGroupBS[iParentArray], 0, iMaxNum) = iMaxNum)
				OR (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciDONT_REPEAT_SPAWN_SUB_GROUP)
				AND COUNT_SET_BITS_IN_RANGE(g_TransitionSessionNonResetVars.iSpawnSubGroupLastBitset[iParentArray], 0, iMaxNum) = iMaxNum))
					INT iRandom = GET_RANDOM_INT_IN_RANGE(1000, ((iMaxNum + 1) * 1000) )
					iRandom = FLOOR(TO_FLOAT(iRandom) / 1000.0)
					
					BOOL bUseSeed = TRUE
					
					IF iSeed > 0
						// Check through previous seeded numbers, make sure this is different
						IF IS_BIT_SET(MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeed_SubGroupBS[iParentArray], iRandom - 1)
							bUseSeed = FALSE
						ENDIF
					ENDIF
					
					// If this option is set we don't want to choose the same sub group as in the previous round
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciDONT_REPEAT_SPAWN_SUB_GROUP)
						IF IS_BIT_SET(g_TransitionSessionNonResetVars.iSpawnSubGroupLastBitset[iParentArray], iRandom - 1)
							bUseSeed = FALSE
						ENDIF
					ENDIF
					
					IF bUseSeed
						SET_BIT(MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeed_SubGroupBS[iParentArray], iRandom - 1)
						PRINTLN("[RCC MISSION][ran] INIT_SERVER_DATA / SEED_SPAWN_SUBGROUP - Seed number ",iSeed," = ",iRandom)
					ELSE
						iSeed-- // Try this number again!
					ENDIF
				ELSE
					SCRIPT_ASSERT("SEED_SPAWN_SUBGROUP - There are no free bits!")
					PRINTLN("SEED_SPAWN_SUBGROUP - COUNT_SET_BITS_IN_RANGE(MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeed_SubGroupBS[", iParentArray, "], 0, ", iMaxNum, ") = ", COUNT_SET_BITS_IN_RANGE(MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeed_SubGroupBS[iParentArray], 0, iMaxNum))
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciDONT_REPEAT_SPAWN_SUB_GROUP)
						PRINTLN("SEED_SPAWN_SUBGROUP - COUNT_SET_BITS_IN_RANGE(g_TransitionSessionNonResetVars.iSpawnSubGroupLastBitset[", iParentArray, "], 0, ", iMaxNum, ") = ", COUNT_SET_BITS_IN_RANGE(g_TransitionSessionNonResetVars.iSpawnSubGroupLastBitset[iParentArray], 0, iMaxNum))
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
		
		PRINTLN("[RCC MISSION][ran] INIT_SERVER_DATA / SEED_SPAWN_SUBGROUP, (AFTER) iParentGroup ",iParentGroup," - MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeed_SubGroupBS[",iParentArray,"] = ",MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeed_SubGroupBS[iParentArray])
	ENDIF
	
ENDPROC

FUNC BOOL IS_SPAWN_GROUP_TURNED_OFF(INT i)
	RETURN g_FMMC_STRUCT.eRandomEntitySpawn_DefineActivation[i] = SPAWN_GROUP_DEFINE_ACTIVATION_OFF
ENDFUNC
FUNC BOOL IS_SPAWN_GROUP_CONSIDERED_FOR_RANDOM_SELECTION(INT i)
	RETURN g_FMMC_STRUCT.eRandomEntitySpawn_DefineActivation[i] = SPAWN_GROUP_DEFINE_ACTIVATION_CONSIDER
ENDFUNC
FUNC BOOL IS_SPAWN_GROUP_FORCED_ACTIVE(INT i)
	RETURN g_FMMC_STRUCT.eRandomEntitySpawn_DefineActivation[i] = SPAWN_GROUP_DEFINE_ACTIVATION_FORCED_ON
ENDFUNC

FUNC BOOL IS_SPAWN_GROUP_BEING_OVERRIDDEN_BY_CONTINUITY(INT iSpawnGroup)
	RETURN IS_BIT_SET(g_FMMC_STRUCT.iContinuitySpawnGroupBitset, iSpawnGroup)
ENDFUNC

FUNC BOOL HAS_CONTINUITY_CONDITION_BEEN_MET_FOR_SPAWN_GROUP(INT iSpawnGroup)
	
	IF NOT IS_SPAWN_GROUP_BEING_OVERRIDDEN_BY_CONTINUITY(iSpawnGroup)
		RETURN TRUE
	ENDIF
	
	IF g_FMMC_STRUCT.iContinuitySpawnGroupContinuityId[iSpawnGroup] = -1
		PRINTLN("CONTINUITY] HAS_CONTINUITY_CONDITION_BEEN_MET_FOR_SPAWN_GROUP - Spawn group ", iSpawnGroup, " set to be activated by continuity but no ID set")
		RETURN FALSE
	ENDIF
	
	IF g_FMMC_STRUCT.iContinuitySpawnGroupCondition[iSpawnGroup] = ciMISSION_CONTINUITY_SPAWN_GROUP_CONDITION_LOCATE_COMPLETE
		
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_LocationTracking)
			PRINTLN("CONTINUITY] HAS_CONTINUITY_CONDITION_BEEN_MET_FOR_SPAWN_GROUP - Spawn group ", iSpawnGroup, " set to be activated by location continuity but tracking is off")
			RETURN FALSE
		ENDIF
		
		IF NOT IS_BIT_SET(g_TransitionSessionNonResetVars.sLEGACYMissionContinuityVars.iLocationTrackingBitset, g_FMMC_STRUCT.iContinuitySpawnGroupContinuityId[iSpawnGroup])
			PRINTLN("CONTINUITY] HAS_CONTINUITY_CONDITION_BEEN_MET_FOR_SPAWN_GROUP - Spawn group ", iSpawnGroup, " set to be activated by location continuity and location continuity ID ", g_FMMC_STRUCT.iContinuitySpawnGroupContinuityId[iSpawnGroup]," was not set")
			RETURN FALSE
		ENDIF
		
	ELIF g_FMMC_STRUCT.iContinuitySpawnGroupCondition[iSpawnGroup] = ciMISSION_CONTINUITY_SPAWN_GROUP_CONDITION_OBJECT_COMPLETE
		
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_ObjectTracking)
			PRINTLN("CONTINUITY] HAS_CONTINUITY_CONDITION_BEEN_MET_FOR_SPAWN_GROUP - Spawn group ", iSpawnGroup, " set to be activated by object continuity but tracking is off")
			RETURN FALSE
		ENDIF
		
		IF NOT IS_LONG_BIT_SET(g_TransitionSessionNonResetVars.sLEGACYMissionContinuityVars.iObjectTrackingBitset, g_FMMC_STRUCT.iContinuitySpawnGroupContinuityId[iSpawnGroup])
			PRINTLN("CONTINUITY] HAS_CONTINUITY_CONDITION_BEEN_MET_FOR_SPAWN_GROUP - Spawn group ", iSpawnGroup, " set to be activated by location continuity and object continuity ID ", g_FMMC_STRUCT.iContinuitySpawnGroupContinuityId[iSpawnGroup]," was not set")
			RETURN FALSE
		ENDIF
		
	ENDIF
	
	RETURN TRUE
ENDFUNC

// [FIX_2020_CONTROLLER] - Simplify and improve readability of this function. Address the way the creator sets 1-10 instead of 0-9.
// [FIX_2020_CONTROLLER] - CLEAN UP this function and then split it off into a LEGANCY and NEW version... There is so much functionality in here that was added for casino that goes against how original spawn groups were written so the logic has become messy.
PROC SEED_SPAWN_GROUPS()
	
	#IF IS_DEBUG_BUILD
	IF g_iMissionForcedSpawnGroup <> 0
		MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeedBS = g_iMissionForcedSpawnGroup
		PRINTLN("[LM] - [SpwnGrpDbg] Forcing Spawn Group Bitset: ", g_iMissionForcedSpawnGroup)
		INT i = 0 
		FOR i = 0 TO ciMAX_NUM_FMMC_ENTITY_SUB_SPAWN_GROUPS-1
			IF g_iMissionForcedSpawnSubGroup[i] <> 0
				PRINTLN("[LM] - [SpwnGrpDbg] Forcing Spawn Sub Group Bitset", g_iMissionForcedSpawnSubGroup[i], " For Spawn Group ", i)
				MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeed_SubGroupBS[i] = g_iMissionForcedSpawnSubGroup[i]
			ENDIF
		ENDFOR
		MC_ServerBD_4.rsgSpawnSeed.bForceValidate = TRUE
		EXIT
	ENDIF
	#ENDIF
	
	INT iLoops
	INT iMaxNum = g_FMMC_STRUCT.iRandomEntitySpawn_MaxNumber
	INT iNumToChoose = GET_NUMBER_OF_RANDOM_ENTITY_SPAWN_GROUPS_TO_SPAWN()	
	
	IF (iMaxNum > 0
	AND iNumToChoose > 0)
	OR SHOULD_BUILD_SPAWN_GROUPS_REGARDLESS_OF_CREATOR_SETTINGS()
		BOOL bSeed = TRUE
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEighteen, ciOptionsBS18_BuildSpawnGroupsDuringPlay)
			IF (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciOPTIONS4_MAINTAIN_SPAWN_GROUPS_ON_RESTART) AND (IS_THIS_A_QUICK_RESTART_JOB() OR IS_CORONA_INITIALISING_A_QUICK_RESTART()))
			AND SHOULD_WE_START_FROM_CHECKPOINT()
				MC_serverBD_4.rsgSpawnSeed = g_TransitionSessionNonResetVars.rsgFMMCSpawnSeed
				PRINTLN("[RCC MISSION][ran] INIT_SERVER_DATA / SEED_SPAWN_GROUPS - This is a quick restart to a checkpoint, loading spawn group data, g_TransitionSessionNonResetVars.rsgFMMCSpawnSeed.iEntitySpawnSeedBS: ", g_TransitionSessionNonResetVars.rsgFMMCSpawnSeed.iEntitySpawnSeedBS)
			ELSE
				IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFive, ciOptionsBS25_MaintainSpawnGroupOverStrand_DontClearSaved)
					CLEAR_SPAWN_GROUP_STRUCT(g_TransitionSessionNonResetVars.rsgFMMCSpawnSeed)
					PRINTLN("[RCC MISSION][ran] INIT_SERVER_DATA / SEED_SPAWN_GROUPS - ciOptionsBS18_BuildSpawnGroupsDuringPlay set, and this is not a quick restart to a checkpoint, don't load or seed spawn group data")
				ELSE
					PRINTLN("[RCC MISSION][ran] INIT_SERVER_DATA / SEED_SPAWN_GROUPS - NOT CLEARING g_TransitionSessionNonResetVars.rsgFMMCSpawnSeed: ", g_TransitionSessionNonResetVars.rsgFMMCSpawnSeed.iEntitySpawnSeedBS, " as ciOptionsBS25_MaintainSpawnGroupOverStrand_DontClearSaved is set.")
				ENDIF
			ENDIF
			
			bSeed = FALSE
		ELSE
			IF (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciOPTIONS4_MAINTAIN_SPAWN_GROUPS_ON_RESTART) AND (IS_THIS_A_QUICK_RESTART_JOB() OR IS_CORONA_INITIALISING_A_QUICK_RESTART()))
			OR IS_A_STRAND_MISSION_BEING_INITIALISED()
			AND (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciOPTIONS4_MAINTAIN_SPAWN_GROUPS_OVER_STRAND)	OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFive, ciOptionsBS25_MaintainSpawnGroupOverStrand_LoadOnly))
				MC_serverBD_4.rsgSpawnSeed = g_TransitionSessionNonResetVars.rsgFMMCSpawnSeed
				PRINTLN("[RCC MISSION][ran] INIT_SERVER_DATA / SEED_SPAWN_GROUPS - This is a quick restart / strand mission, loading spawn group data, g_TransitionSessionNonResetVars.rsgFMMCSpawnSeed.iEntitySpawnSeedBS: ", g_TransitionSessionNonResetVars.rsgFMMCSpawnSeed.iEntitySpawnSeedBS)
				bSeed = FALSE
			ELSE
				IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFive, ciOptionsBS25_MaintainSpawnGroupOverStrand_DontClearSaved)
					CLEAR_SPAWN_GROUP_STRUCT(g_TransitionSessionNonResetVars.rsgFMMCSpawnSeed)
				ELSE
					PRINTLN("[RCC MISSION][ran] INIT_SERVER_DATA / SEED_SPAWN_GROUPS - NOT CLEARING g_TransitionSessionNonResetVars.rsgFMMCSpawnSeed: ", g_TransitionSessionNonResetVars.rsgFMMCSpawnSeed.iEntitySpawnSeedBS, " as ciOptionsBS25_MaintainSpawnGroupOverStrand_DontClearSaved is set.")
				ENDIF
			ENDIF
		ENDIF
		
		IF bSeed
			PRINTLN("[RCC MISSION][ran] INIT_SERVER_DATA / SEED_SPAWN_GROUPS - Getting randomised entity spawn seeds: number of available variations = ",iMaxNum,", number of randoms to seed = ",iNumToChoose)
			
			INT iSeed
			FOR iSeed = 0 TO (iMaxNum - 1)
				IF (ARE_SPAWN_GROUPS_USING_CREATOR_DEFINE_SETTINGS()
				AND IS_SPAWN_GROUP_FORCED_ACTIVE(iSeed))
				OR (IS_SPAWN_GROUP_BEING_OVERRIDDEN_BY_CONTINUITY(iSeed) 
				AND HAS_CONTINUITY_CONDITION_BEEN_MET_FOR_SPAWN_GROUP(iSeed))
					SET_BIT(MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeedBS, iSeed)
					PRINTLN("[RCC MISSION][ran] INIT_SERVER_DATA / SEED_SPAWN_GROUPS - Pre-defined Creator Settings - Setting Spawngroup: ", iSeed, " as active")
					SEED_SPAWN_SUBGROUP(iSeed + 1)
				ENDIF
			ENDFOR
			FOR iSeed = 0 TO (iNumToChoose - 1)				
				IF iSeed < iMaxNum // No need to make more seeds than numbers that exist!
									
					// Ensure there are free bits to be randomly set
					IF NOT ((COUNT_SET_BITS_IN_RANGE(MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeedBS, 0, iMaxNum) = iMaxNum)
					OR (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciDONT_REPEAT_SPAWN_GROUP)
					AND COUNT_SET_BITS_IN_RANGE(g_TransitionSessionNonResetVars.iSpawnGroupLastBitset, 0, iMaxNum) = iMaxNum))
						INT iRandom = GET_RANDOM_INT_IN_RANGE(1000, ((iMaxNum + 1) * 1000) )
						iRandom = FLOOR(TO_FLOAT(iRandom) / 1000.0)
						
						BOOL bUseSeed = TRUE
						
						IF iSeed > 0						
							// Check through previous seeded numbers, make sure this is different
							IF IS_BIT_SET(MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeedBS, iRandom - 1)
								bUseSeed = FALSE
							ENDIF
						ENDIF
						
						IF IS_SPAWN_GROUP_BEING_OVERRIDDEN_BY_CONTINUITY(iRandom - 1)
						AND NOT HAS_CONTINUITY_CONDITION_BEEN_MET_FOR_SPAWN_GROUP(iRandom - 1)
							PRINTLN("[RCC MISSION][ran] INIT_SERVER_DATA / SEED_SPAWN_GROUPS - Seed number ", iSeed," Spawn Group ", iRandom - 1, " continuity condition not met.")
							bUseSeed = FALSE
						ENDIF
						
						// If this option is set we don't want to choose the same group as in the previous round
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciDONT_REPEAT_SPAWN_GROUP)
							IF IS_BIT_SET(g_TransitionSessionNonResetVars.iSpawnGroupLastBitset, iRandom - 1)
								PRINTLN("[RCC MISSION][ran] INIT_SERVER_DATA / SEED_SPAWN_GROUPS - Seed number ",iSeed," Spawn Group ", iRandom - 1, " Don't Repeat Groups turned on, and we have selected a repeat.")
								bUseSeed = FALSE
							ENDIF
						ENDIF
						
						IF ARE_SPAWN_GROUPS_USING_CREATOR_DEFINE_SETTINGS()							
							IF IS_SPAWN_GROUP_FORCED_ACTIVE(iRandom - 1)
								PRINTLN("[RCC MISSION][ran] INIT_SERVER_DATA / SEED_SPAWN_GROUPS - Seed number ",iSeed," Spawn Group ", iRandom - 1, " is already forced on - Relooping")
								bUseSeed = FALSE
							ENDIF							
							IF IS_SPAWN_GROUP_TURNED_OFF(iRandom - 1)
								PRINTLN("[RCC MISSION][ran] INIT_SERVER_DATA / SEED_SPAWN_GROUPS - Seed number ",iSeed," Spawn Group ", iRandom - 1, " is turned off - Relooping")
								bUseSeed = FALSE
							ENDIF
						ENDIF
						
						IF bUseSeed
							SET_BIT(MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeedBS, iRandom - 1)
							PRINTLN("[RCC MISSION][ran] INIT_SERVER_DATA / SEED_SPAWN_GROUPS - Seed number ",iSeed," = ",iRandom - 1, " Setting spawn group on.")
							
							SEED_SPAWN_SUBGROUP(iRandom)
						ELSE
							iSeed-- // Try this number again!
						ENDIF
					ELSE
						SCRIPT_ASSERT("SEED_SPAWN_GROUPS - There are no free bits!")
						PRINTLN("SEED_SPAWN_GROUPS - COUNT_SET_BITS_IN_RANGE(MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeedBS, 0, ", iMaxNum, ") = ", COUNT_SET_BITS_IN_RANGE(MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeedBS, 0, iMaxNum))
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciDONT_REPEAT_SPAWN_GROUP)
							PRINTLN("SEED_SPAWN_GROUPS - COUNT_SET_BITS_IN_RANGE(g_TransitionSessionNonResetVars.iSpawnGroupLastBitset, 0, ", iMaxNum, ") = ", COUNT_SET_BITS_IN_RANGE(g_TransitionSessionNonResetVars.iSpawnGroupLastBitset, 0, iMaxNum))
						ENDIF
					ENDIF
				ENDIF
				
				iLoops++
				IF iLoops > 300
					PRINTLN("[RCC MISSION][ran] INIT_SERVER_DATA / SEED_SPAWN_GROUPS - Got stuck in a loop Bailing.....")
					SCRIPT_ASSERT("SEED_SPAWN_GROUPS - COULD NOT COMPLETE SPAWN GROUP SEEDING. INCORRECT SETTINGS.")
					BREAKLOOP
				ENDIF
			ENDFOR
			
			PRINTLN("[RCC MISSION][ran] INIT_SERVER_DATA / SEED_SPAWN_GROUPS - MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeedBS = ",MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeedBS)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFive, ciOptionsBS25_HandheldDrillMGForceSpawnGroupActive)
		PRINTLN("[RCC MISSION][ran] INIT_SERVER_DATA / SEED_SPAWN_GROUPS - iHandheldDrillingMinigameSpawnGroup: ",  g_FMMC_STRUCT.iHandheldDrillingMinigameSpawnGroup, " FORCING THIS SPAWN GROUP TO BE ACTIVE.")
		SET_BIT(MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeedBS, g_FMMC_STRUCT.iHandheldDrillingMinigameSpawnGroup)
		MC_ServerBD_4.rsgSpawnSeed.bForceValidate = TRUE
		
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFive, ciOptionsBS25_HandheldDrillMGMaintainSelectedSpawngroupOverStrand)
		OR g_TransitionSessionNonResetVars.iSubSpawnGroupHandheldHackBS = 0
			SEED_SPAWN_SUBGROUP(g_FMMC_STRUCT.iHandheldDrillingMinigameSpawnGroup+1)
			PRINTLN("[RCC MISSION][ran][iSubSpawnGroupHandheldHackBS] INIT_SERVER_DATA / SEED_SPAWN_GROUPS - Sub Spawn group for handheld drill was seeded to: ", MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeed_SubGroupBS[g_FMMC_STRUCT.iHandheldDrillingMinigameSpawnGroup])
			g_TransitionSessionNonResetVars.iSubSpawnGroupHandheldHackBS = MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeed_SubGroupBS[g_FMMC_STRUCT.iHandheldDrillingMinigameSpawnGroup]
		ELSE
			MC_serverBD_4.rsgSpawnSeed.iEntitySpawnSeed_SubGroupBS[g_FMMC_STRUCT.iHandheldDrillingMinigameSpawnGroup] = g_TransitionSessionNonResetVars.iSubSpawnGroupHandheldHackBS
			PRINTLN("[RCC MISSION][ran][iSubSpawnGroupHandheldHackBS] INIT_SERVER_DATA / SEED_SPAWN_GROUPS - (maintained) setting to g_TransitionSessionNonResetVars.iSubSpawnGroupHandheldHackBS: ", g_TransitionSessionNonResetVars.iSubSpawnGroupHandheldHackBS)
		ENDIF
	ENDIF	
	
ENDPROC

FUNC BOOL IS_FORCED_WEAPON_STRAND_BEING_INITIALISED()
	RETURN (IS_JOB_FORCED_WEAPON_ONLY() OR IS_JOB_FORCED_WEAPON_PLUS_PICKUPS()) AND IS_A_STRAND_MISSION_BEING_INITIALISED()
ENDFUNC
FUNC BOOL IS_FORCED_WEAPON_STRAND()
	RETURN (IS_JOB_FORCED_WEAPON_ONLY() OR IS_JOB_FORCED_WEAPON_PLUS_PICKUPS()) AND IS_THIS_IS_A_STRAND_MISSION()
ENDFUNC

FUNC BOOL SHOULD_CREATE_PLACED_PTFX(INT iPTFX)
	BOOL bReturn = FALSE	
	INT iTeam = -1
	INT iTeamLoop
	INT iTeamMaxLoop = -1
	INT iTeamCreator = g_FMMC_STRUCT.sPlacedPTFX[iPTFX].iTeam
	INT iRuleFrom = g_FMMC_STRUCT.sPlacedPTFX[iPTFX].iRuleFrom
	INT iRuleTo = g_FMMC_STRUCT.sPlacedPTFX[iPTFX].iRuleTo	
	INT iCurrentRule
	
	IF iTeamCreator >= 0
		iTeam = iTeamCreator
		iTeamMaxLoop = iTeamCreator
	ELIF iTeamCreator = -1
		iTeam = 0
		iTeamMaxLoop = (MC_serverBD.iNumberOfTeams - 1)
	ENDIF
	IF iRuleTo = -1
		iRuleTo = FMMC_MAX_RULES
	ENDIF
	
	FOR iTeamLoop = 0 TO iTeamMaxLoop
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.sPlacedPTFX[iPTFX].iPTFXBs, ciPLACED_PTFX_SpawnOnlyForLocalTeam)
		OR MC_PlayerBD[iPartToUse].iTeam = iTeam
			iCurrentRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]	
			IF iCurrentRule >= iRuleFrom
			AND iCurrentRule < iRuleTo
				bReturn = TRUE
			ENDIF
			iTeam++
		ENDIF
	ENDFOR
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sPlacedPTFX[iPTFX].iPTFXBS, ciPLACED_PTFX_MCVarConfig_BlockSpawnUnlessCompletedPrereqMission_1)
		bReturn = FALSE
	ENDIF
	
	RETURN bReturn
ENDFUNC

FUNC BOOL SHOULD_CLEANUP_PLACED_PTFX(INT iPTFX)
	BOOL bReturn = FALSE	
	INT iTeam = -1
	INT iTeamLoop
	INT iTeamMaxLoop = -1
	INT iTeamCreator = g_FMMC_STRUCT.sPlacedPTFX[iPTFX].iTeam
	INT iRuleFrom = g_FMMC_STRUCT.sPlacedPTFX[iPTFX].iRuleFrom
	INT iRuleTo = g_FMMC_STRUCT.sPlacedPTFX[iPTFX].iRuleTo	
	INT iCurrentRule
	
	IF iTeamCreator >= 0
		iTeam = iTeamCreator
		iTeamMaxLoop = iTeamCreator
	ELIF iTeamCreator = -1
		iTeam = 0
		iTeamMaxLoop = (MC_serverBD.iNumberOfTeams - 1)
	ENDIF
	IF iRuleTo = -1
		iRuleTo = FMMC_MAX_RULES
	ENDIF
	
	FOR iTeamLoop = 0 TO iTeamMaxLoop
		iCurrentRule = MC_ServerBD_4.iCurrentHighestPriority[iTeam]	
		IF iCurrentRule < iRuleFrom
		OR iCurrentRule >= iRuleTo
			bReturn = TRUE
		ENDIF
		iTeam++
	ENDFOR
	
	IF g_FMMC_STRUCT.sPlacedPTFX[iPTFX].iTimeToPlayFor > 0
	AND HAS_NET_TIMER_EXPIRED(sPlacedPtfxLocal[iPTFX].tdPtfxStarted, g_FMMC_STRUCT.sPlacedPTFX[iPTFX].iTimeToPlayFor * 1000)
		bReturn = TRUE
	ENDIF
	
	RETURN bReturn
ENDFUNC

PROC BUILD_PLACED_PTFX_ASSOCIATED_SOUND_FX(TEXT_LABEL_63 tl63_EffectName, TEXT_LABEL_63 &tl63_SoundBank, TEXT_LABEL_63 &tl63_SoundName, TEXT_LABEL_63 &tl63_SoundSet)
	IF NOT IS_STRING_NULL_OR_EMPTY(tl63_EffectName)
		IF ARE_STRINGS_EQUAL(tl63_EffectName, "scr_ch_finale_bug_infestation")
			tl63_SoundBank = "DLC_HEIST3/CASINO_HEIST_FINALE_GENERAL_01"
			tl63_SoundName = "bugs_infestation"
			tl63_SoundSet = "dlc_ch_heist_finale_sounds"			
		ENDIF
	ENDIF
ENDPROC

PROC CLEANUP_PLACED_PTFX_ASSOCIATED_SOUND_FX(INT iPTFX)
	TEXT_LABEL_63 tl63_EffectName = g_FMMC_STRUCT.sPlacedPTFX[iPTFX].tl63EffectName
	TEXT_LABEL_63 tl63_SoundBank
	TEXT_LABEL_63 tl63_SoundName
	TEXT_LABEL_63 tl63_SoundSet
	
	BUILD_PLACED_PTFX_ASSOCIATED_SOUND_FX(tl63_EffectName, tl63_SoundBank, tl63_SoundName, tl63_SoundSet)
	
	IF sPlacedPtfxLocal[iPTFX].iPTFXSoundID > -1
		STOP_SOUND(sPlacedPtfxLocal[iPTFX].iPTFXSoundID)
		RELEASE_SOUND_ID(sPlacedPtfxLocal[iPTFX].iPTFXSoundID)
		sPlacedPtfxLocal[iPTFX].iPTFXSoundID = -1
	ENDIF
	
	IF NOT IS_STRING_NULL_OR_EMPTY(tl63_SoundBank)
	AND NOT ARE_STRINGS_EQUAL(tl63_SoundBank, "DLC_HEIST3/CASINO_HEIST_FINALE_GENERAL_01")	
		RELEASE_NAMED_SCRIPT_AUDIO_BANK(tl63_SoundBank)
	ENDIF
ENDPROC

FUNC BOOL PLAY_PLACED_PTFX_ASSOCIATED_SOUND_FX(INT iPTFX)
	BOOL bNetworked = IS_BIT_SET(g_FMMC_STRUCT.sPlacedPTFX[iPTFX].iPTFXBS, ciPLACED_PTFX_isNetworked)
	VECTOR vPos		 	= g_FMMC_STRUCT.sPlacedPTFX[iPTFX].vStartCoord
	TEXT_LABEL_63 tl63_EffectName = g_FMMC_STRUCT.sPlacedPTFX[iPTFX].tl63EffectName
	TEXT_LABEL_63 tl63_SoundBank
	TEXT_LABEL_63 tl63_SoundName
	TEXT_LABEL_63 tl63_SoundSet	
	
	BUILD_PLACED_PTFX_ASSOCIATED_SOUND_FX(tl63_EffectName, tl63_SoundBank, tl63_SoundName, tl63_SoundSet)
	
	PRINTLN("[LM][Placed_PTFX] - PLAY_PLACED_PTFX_ASSOCIATED_SOUND_FX - tl63_SoundBank: ", tl63_SoundBank, " tl63_SoundName: ", tl63_SoundName, " tl63_SoundSet: ", tl63_SoundSet)
	
	IF IS_STRING_NULL_OR_EMPTY(tl63_SoundName)
		PRINTLN("[LM][Placed_PTFX] - PLAY_PLACED_PTFX_ASSOCIATED_SOUND_FX - Does not have an associated SFX.")
		RETURN TRUE
	ENDIF
	
	IF IS_STRING_NULL_OR_EMPTY(tl63_SoundBank)
	OR REQUEST_SCRIPT_AUDIO_BANK(tl63_SoundBank)
		IF sPlacedPtfxLocal[iPTFX].iPTFXSoundID = -1
			sPlacedPtfxLocal[iPTFX].iPTFXSoundID = GET_SOUND_ID()
		ENDIF
		PRINTLN("[LM][Placed_PTFX] - PLAY_PLACED_PTFX_ASSOCIATED_SOUND_FX - Playing with SoundID: ", sPlacedPtfxLocal[iPTFX].iPTFXSoundID)
		PLAY_SOUND_FROM_COORD(sPlacedPtfxLocal[iPTFX].iPTFXSoundID, tl63_SoundName, vPos, tl63_SoundSet, bNetworked)
		RETURN TRUE
	ENDIF
	
	PRINTLN("[LM][Placed_PTFX] - PLAY_PLACED_PTFX_ASSOCIATED_SOUND_FX - tl63_SoundBank: ", tl63_SoundBank, " Waiting for it to be loaded...")
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_PLACED_PTFX_EVERY_FRAME()
	INT iPTFX
	FOR iPTFX = 0 TO g_FMMC_STRUCT.iNumPlacedPTFX-1
		IF iPTFX < FMMC_MAX_PLACED_PTFX
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.sPlacedPTFX[iPTFX].iPTFXBS, ciPLACED_PTFX_MCVarConfig_BlockSpawnUnlessCompletedPrereqMission_1)
				IF IS_BIT_SET(sPlacedPtfxLocal[iPTFX].iPtfxBS, ci_PLACED_PTFX_Loaded)
				AND NOT IS_BIT_SET(sPlacedPtfxLocal[iPTFX].iPtfxBS, ci_PLACED_PTFX_CleanedUp)
					BOOL bLocalOnly = (NOT IS_BIT_SET(g_FMMC_STRUCT.sPlacedPTFX[iPTFX].iPTFXBS, ciPLACED_PTFX_isNetworked))
					BOOL bLooped = IS_BIT_SET(g_FMMC_STRUCT.sPlacedPTFX[iPTFX].iPTFXBS, ciPLACED_PTFX_isLooped)
					FLOAT fScale = g_FMMC_STRUCT.sPlacedPTFX[iPTFX].fScale
				
					IF NOT IS_BIT_SET(sPlacedPtfxLocal[iPTFX].iPtfxBS, ci_PLACED_PTFX_Created)
						
						// Create...				
						IF SHOULD_CREATE_PLACED_PTFX(iPTFX)					
							PRINTLN("[LM][Placed_PTFX] iPTFX: ", iPTFX, " created using Effect Name: ", g_FMMC_STRUCT.sPlacedPtfx[iPTFX].tl63EffectName, " bLooped: ", bLooped, " bLocalOnly: ", bLocalOnly, " at COORD: ", g_FMMC_STRUCT.sPlacedPTFX[iPTFX].vStartCoord, " for time: ", g_FMMC_STRUCT.sPlacedPTFX[iPTFX].iTimeToPlayFor)
							
							IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sPlacedPTFX[iPTFX].tl63AssetName)
								PRINTLN("[LM][Placed_PTFX] iPTFX: ", iPTFX, " Calling to use Asset Name: ", g_FMMC_STRUCT.sPlacedPTFX[iPTFX].tl63AssetName)
								USE_PARTICLE_FX_ASSET(g_FMMC_STRUCT.sPlacedPTFX[iPTFX].tl63AssetName)
							ENDIF
							
							IF fScale < 0.1
							OR fScale > 20.0
								PRINTLN("[LM][Placed_PTFX] iPTFX: ", iPTFX, " fScale is invalid, setting to 1 (fScale: ", fScale, ")")
								fScale = 1.0
							ENDIF
							
							IF NOT bLooped
								IF bLocalOnly
									START_PARTICLE_FX_NON_LOOPED_AT_COORD(g_FMMC_STRUCT.sPlacedPTFX[iPTFX].tl63EffectName, g_FMMC_STRUCT.sPlacedPTFX[iPTFX].vStartCoord, g_FMMC_STRUCT.sPlacedPTFX[iPTFX].vStartRot, fScale)
								ELIF bIsLocalPlayerHost
									START_NETWORKED_PARTICLE_FX_NON_LOOPED_AT_COORD(g_FMMC_STRUCT.sPlacedPTFX[iPTFX].tl63EffectName, g_FMMC_STRUCT.sPlacedPTFX[iPTFX].vStartCoord, g_FMMC_STRUCT.sPlacedPTFX[iPTFX].vStartRot, fScale)
								ENDIF
							ELSE
								sPlacedPtfxLocal[iPTFX].ptfxID = START_PARTICLE_FX_LOOPED_AT_COORD(g_FMMC_STRUCT.sPlacedPTFX[iPTFX].tl63EffectName, g_FMMC_STRUCT.sPlacedPTFX[iPTFX].vStartCoord, g_FMMC_STRUCT.sPlacedPTFX[iPTFX].vStartRot, fScale, DEFAULT, DEFAULT, DEFAULT, bLocalOnly)
							ENDIF
							
							IF PLAY_PLACED_PTFX_ASSOCIATED_SOUND_FX(iPTFX)
								SET_BIT(sPlacedPtfxLocal[iPTFX].iPtfxBS,  ci_PLACED_PTFX_SFX)
							ENDIF
							
							SET_BIT(sPlacedPtfxLocal[iPTFX].iPtfxBS, ci_PLACED_PTFX_Created)
							RESET_NET_TIMER(sPlacedPtfxLocal[iPTFX].tdPtfxStarted)
							START_NET_TIMER(sPlacedPtfxLocal[iPTFX].tdPtfxStarted)		
						ENDIF
					ELSE
					
						// Process...
						IF NOT SHOULD_CLEANUP_PLACED_PTFX(iPTFX)
							IF NOT IS_BIT_SET(sPlacedPtfxLocal[iPTFX].iPtfxBS,  ci_PLACED_PTFX_SFX)
								IF PLAY_PLACED_PTFX_ASSOCIATED_SOUND_FX(iPTFX)
									SET_BIT(sPlacedPtfxLocal[iPTFX].iPtfxBS,  ci_PLACED_PTFX_SFX)
								ENDIF
							ENDIF			
						ELSE
							// Cleanup...
							IF bLooped
								IF DOES_PARTICLE_FX_LOOPED_EXIST(sPlacedPtfxLocal[iPTFX].ptfxID)
									PRINTLN("[LM][Placed_PTFX] - iPTFX: ", iPTFX, " Stopping PTFX: ", g_FMMC_STRUCT.sPlacedPtfx[iPTFX].tl63EffectName)
									STOP_PARTICLE_FX_LOOPED(sPlacedPtfxLocal[iPTFX].ptfxID)
								ENDIF
							ELSE
								
							ENDIF
							CLEANUP_PLACED_PTFX_ASSOCIATED_SOUND_FX(iPTFX)
							RESET_NET_TIMER(sPlacedPtfxLocal[iPTFX].tdPtfxStarted)
							CLEAR_BIT(sPlacedPtfxLocal[iPTFX].iPtfxBS, ci_PLACED_PTFX_Created)
							CLEAR_BIT(sPlacedPtfxLocal[iPTFX].iPtfxBS,  ci_PLACED_PTFX_SFX)
							SET_BIT(sPlacedPtfxLocal[iPTFX].iPtfxBS, ci_PLACED_PTFX_CleanedUp)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

FUNC BOOL IS_PED_CLOSE_TO_HEADING( PED_INDEX ped, FLOAT fHeading, FLOAT fTolerance = 30.0 )
	RETURN GET_ABSOLUTE_DELTA_HEADING( GET_ENTITY_HEADING( ped ), fHeading ) <= fTolerance
ENDFUNC

FUNC WEAPON_TYPE GET_CURRENT_PLAYER_WEAPON_TYPE()
	WEAPON_TYPE wtCurrent = WEAPONTYPE_INVALID
	GET_CURRENT_PED_WEAPON(LocalPlayerPed, wtCurrent)
	
	RETURN wtCurrent
ENDFUNC

PROC SET_OBJ_NEAR(INT iObjNear)
	DEBUG_PRINTCALLSTACK()
	MC_playerBD[iPartToUse].iObjNear = iObjNear
	PRINTLN("SET_OBJ_NEAR | Setting MC_playerBD[", iPartToUse, "].iObjNear to ", MC_playerBD[iPartToUse].iObjNear)
ENDPROC

//Interact-With Extras
FUNC BOOL IS_LOCAL_PLAYER_INTERACTING_WITH_THIS_OBJECT(INT iObj)
	RETURN MC_playerBD[iLocalPart].iObjHacking = iObj
	//RETURN MC_serverBD.iObjHackPart[iObj] = iLocalPart
ENDFUNC

PROC CACHE_VEHICLE_SEAT_FOR_CONTINUITY()

	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_VehicleSeatTracking)
		EXIT
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(LocalPlayerPed)
		EXIT
	ENDIF
	
	IF IS_ENTITY_DEAD(LocalPlayerPed)
		EXIT
	ENDIF
	
	IF NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		EXIT
	ENDIF
		
	VEHICLE_INDEX veh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
	
	INT iVeh = IS_VEH_A_MISSION_CREATOR_VEH(veh)
	
	IF iVeh < 0
		EXIT
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iContinuityID = -1
		EXIT
	ENDIF
	
	sLEGACYMissionLocalContinuityVars.iEndVehicleContinuityId = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iContinuityID
	sLEGACYMissionLocalContinuityVars.eEndVehicleSeat = GET_SEAT_PED_IS_IN(localPlayerPed)
	
	PRINTLN("[JS][CONTINUITY] - CACHE_VEHICLE_SEAT_FOR_CONTINUITY - iEndVehicleContinuityId = ", sLEGACYMissionLocalContinuityVars.iEndVehicleContinuityId)
	PRINTLN("[JS][CONTINUITY] - CACHE_VEHICLE_SEAT_FOR_CONTINUITY - eEndVehicleSeat = ", ENUM_TO_INT(sLEGACYMissionLocalContinuityVars.eEndVehicleSeat))
	
ENDPROC

FUNC BOOL IS_WEAPONTYPE_A_STUN_GUN(WEAPON_TYPE wtWeaponType)
	IF wtWeaponType = WEAPONTYPE_STUNGUN
	//#IF FEATURE_COPS_N_CROOKS
	//OR wtWeaponType = WEAPONTYPE_DLC_STUNGUNCNC
	//#ENDIF
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC
PROC SET_SERVER_ELEVATOR_STATE(INT iElevator, FMMC_ELEVATOR_SERVER_STATE eNewState)
	
	PRINTLN("[JS][ELEVATORS] - SET_SERVER_ELEVATOR_STATE - Changing Elevator ", iElevator, "'s state from ", 
				DEBUG_GET_FMMC_ELEVATOR_SERVER_STATE_STRING(MC_serverBD_1.eElevatorState[iElevator]), " to ", DEBUG_GET_FMMC_ELEVATOR_SERVER_STATE_STRING(eNewState))
	
	MC_serverBD_1.eElevatorState[iElevator] = eNewState
	
ENDPROC

PROC SET_CLIENT_ELEVATOR_STATE(FMMC_ELEVATOR_CLIENT_STATE eNewState)
	
	PRINTLN("[JS][ELEVATORS] - SET_CLIENT_ELEVATOR_STATE - Changing Elevator ", MC_playerBD_1[iLocalPart].iCurrentElevator, "'s state from ", 
				DEBUG_GET_FMMC_ELEVATOR_CLIENT_STATE_STRING(MC_playerBD_1[iLocalPart].eCurrentElevatorState), " to ", DEBUG_GET_FMMC_ELEVATOR_CLIENT_STATE_STRING(eNewState))
	
	MC_playerBD_1[iLocalPart].eCurrentElevatorState = eNewState
	
ENDPROC

PROC APPLY_NIGHTVISION_FROM_TEAM_GEAR_IF_AVAILABLE(INT iTeam)
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitset4, ciBS4_TEAM_GIVE_NIGHTVISION_IF_AVAILABLE)
		IF IS_LONG_BIT_SET(g_FMMC_STRUCT.biGearAvailableBitset[iTeam], ENUM_TO_INT(HEIST_GEAR_NIGHTVISION))
			IF NOT IS_MP_HEIST_GEAR_EQUIPPED(LocalPlayerPed, HEIST_GEAR_NIGHTVISION)
				SET_MP_HEIST_GEAR(LocalPlayerPed, HEIST_GEAR_NIGHTVISION)
				PRINTLN("APPLY_NIGHTVISION_FROM_TEAM_GEAR_IF_AVAILABLE - Applying: HEIST_GEAR_NIGHTVISION")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC APPLY_DUFFEL_BAG_HEIST_GEAR(PED_INDEX piPed, INT iPart)
	MP_HEIST_GEAR_ENUM eBag = HEIST_GEAR_SPORTS_BAG
	
	IF IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_CASINO_HEIST)
		IF iPart > -1
			eBag = MC_playerBD_1[iPart].eHeistGearBagType
		ENDIF
	ENDIF
	PRINTLN("[HeistBag] APPLY_DUFFEL_BAG_HEIST_GEAR called")
	SET_MP_HEIST_GEAR(piPed, eBag) 
ENDPROC
PROC REMOVE_DUFFEL_BAG_HEIST_GEAR(PED_INDEX piPed, INT iPart)
	MP_HEIST_GEAR_ENUM eBag = HEIST_GEAR_SPORTS_BAG
	
	IF IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_CASINO_HEIST)
		IF iPart > -1
			eBag = MC_playerBD_1[iPart].eHeistGearBagType
		ENDIF
	ENDIF
	PRINTLN("[HeistBag] REMOVE_DUFFEL_BAG_HEIST_GEAR called")
	REMOVE_MP_HEIST_GEAR(piPed, eBag) 
ENDPROC
PROC PRELOAD_DUFFEL_BAG_HEIST_GEAR(PED_INDEX piPed, INT iPart)
	MP_HEIST_GEAR_ENUM eBag = HEIST_GEAR_SPORTS_BAG
	
	IF IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_CASINO_HEIST)
		IF iPart > -1
			eBag = MC_playerBD_1[iPart].eHeistGearBagType
		ENDIF
	ENDIF
	PRINTLN("[HeistBag] PRELOAD_DUFFEL_BAG_HEIST_GEAR called")
	PRELOAD_MP_HEIST_GEAR(piPed, eBag) 
ENDPROC

FUNC MP_HEIST_GEAR_ENUM GET_STEALTH_BAG(INT iBagIndex)
	SWITCH iBagIndex
		CASE 0 RETURN HEIST_GEAR_DUFFEL_CAMO_4_FULL
		CASE 1 RETURN HEIST_GEAR_DUFFEL_CAMO_3_FULL
		CASE 2 RETURN HEIST_GEAR_DUFFEL_CAMO_6_FULL
		CASE 3 RETURN HEIST_GEAR_DUFFEL_CAMO_4_FULL
	ENDSWITCH
	RETURN HEIST_GEAR_DUFFEL_CAMO_4_FULL
ENDFUNC

FUNC MP_HEIST_GEAR_ENUM GET_DIRECT_BAG(INT iBagIndex)
	SWITCH iBagIndex
		CASE 0 RETURN HEIST_GEAR_DUFFEL_CAMO_1_FULL
		CASE 1 RETURN HEIST_GEAR_DUFFEL_CAMO_2_FULL
		CASE 2 RETURN HEIST_GEAR_DUFFEL_CAMO_5_FULL
		CASE 3 RETURN HEIST_GEAR_DUFFEL_CAMO_1_FULL
	ENDSWITCH
	RETURN HEIST_GEAR_DUFFEL_CAMO_1_FULL
ENDFUNC

FUNC BOOL IS_OUTFIT_CORRECT_TO_USE_ARMOURED_BAG()
	
	SWITCH GET_STYLE_FROM_OUTFIT(MC_playerBD[iLocalPart].iOutfit)
		CASE OUTFIT_STYLE_CASINO_HEIST_DIRECT_LIGHT_I
		CASE OUTFIT_STYLE_CASINO_HEIST_DIRECT_LIGHT_II
		CASE OUTFIT_STYLE_CASINO_HEIST_DIRECT_LIGHT_III
			RETURN FALSE
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

PROC SET_BAG_FOR_CASINO_HEIST(BOOL bUpdate = FALSE)
	IF CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_FINALE()
		
		IF bUpdate
			sApplyOutfitData.eGear = INT_TO_ENUM(MP_HEIST_GEAR_ENUM, g_FMMC_STRUCT.iGearDefault[MC_playerBD[iLocalPart].iTeam])
			PRINTLN("[HeistBag] SET_BAG_FOR_CASINO_HEIST - bUpdate - sApplyOutfitData.eGear is now: ", sApplyOutfitData.eGear)
		ELIF sLEGACYMissionLocalContinuityVars.iHeistBag != -1
			sApplyOutfitData.eGear = INT_TO_ENUM(MP_HEIST_GEAR_ENUM, sLEGACYMissionLocalContinuityVars.iHeistBag)
			g_FMMC_STRUCT.iGearDefault[MC_playerBD[iLocalPart].iTeam] = ENUM_TO_INT(sApplyOutfitData.eGear)
			PRINTLN("[HeistBag] SET_BAG_FOR_CASINO_HEIST - Continuity - sApplyOutfitData.eGear is now: ", sApplyOutfitData.eGear)
		ENDIF
		
		IF sApplyOutfitData.eGear = HEIST_GEAR_DUFFEL_CAMO_1_FULL
			sApplyOutfitData.eGear = GET_DIRECT_BAG(GET_PARTICIPANT_NUMBER_IN_TEAM())
			g_FMMC_STRUCT.iGearDefault[MC_playerBD[iLocalPart].iTeam] = ENUM_TO_INT(sApplyOutfitData.eGear)
			PRINTLN("[HeistBag] SET_BAG_FOR_CASINO_HEIST - Direct - sApplyOutfitData.eGear is now: ", sApplyOutfitData.eGear)
		ELIF sApplyOutfitData.eGear = HEIST_GEAR_DUFFEL_CAMO_4_FULL
			sApplyOutfitData.eGear = GET_STEALTH_BAG(GET_PARTICIPANT_NUMBER_IN_TEAM())
			g_FMMC_STRUCT.iGearDefault[MC_playerBD[iLocalPart].iTeam] = ENUM_TO_INT(sApplyOutfitData.eGear)
			PRINTLN("[HeistBag] SET_BAG_FOR_CASINO_HEIST - Stealth - sApplyOutfitData.eGear is now: ", sApplyOutfitData.eGear)
		ENDIF
		
		IF IS_PED_WEARING_ARMOUR(LocalPlayerPed)
		AND IS_OUTFIT_CORRECT_TO_USE_ARMOURED_BAG()
			IF GET_HEIST_BAG_ARMOURED_VERSION(sApplyOutfitData.eGear) != HEIST_GEAR_NONE
				sApplyOutfitData.eGear = GET_HEIST_BAG_ARMOURED_VERSION(sApplyOutfitData.eGear)
				g_FMMC_STRUCT.iGearDefault[MC_playerBD[iLocalPart].iTeam] = ENUM_TO_INT(sApplyOutfitData.eGear)
				PRINTLN("[HeistBag] SET_BAG_FOR_CASINO_HEIST - Armoured Ped - sApplyOutfitData.eGear is now: ", sApplyOutfitData.eGear)
			ELSE
				PRINTLN("[HeistBag] SET_BAG_FOR_CASINO_HEIST - No armoured bag variant, leaving bag as is.")
			ENDIF
		ELSE
			IF IS_HEIST_BAG_ARMOURED_VERSION(sApplyOutfitData.eGear)
				INT iGear = ENUM_TO_INT(sApplyOutfitData.eGear)
				iGear -= 2
				sApplyOutfitData.eGear = INT_TO_ENUM(MP_HEIST_GEAR_ENUM, iGear)
				g_FMMC_STRUCT.iGearDefault[MC_playerBD[iLocalPart].iTeam] = iGear
				PRINTLN("[HeistBag] SET_BAG_FOR_CASINO_HEIST - Non Armoured Ped was trying to use armoured bag - sApplyOutfitData.eGear is now: ", sApplyOutfitData.eGear)
			ELSE
				PRINTLN("[HeistBag] SET_BAG_FOR_CASINO_HEIST - Not wearing armour, leaving bag as is.")
			ENDIF
		ENDIF
		
		IF NOT IS_LONG_BIT_SET(g_FMMC_STRUCT.biGearAvailableBitset[MC_playerBD[iLocalPart].iTeam], g_FMMC_STRUCT.iGearDefault[MC_playerBD[iLocalPart].iTeam])
			SET_LONG_BIT(g_FMMC_STRUCT.biGearAvailableBitset[MC_playerBD[iLocalPart].iTeam], g_FMMC_STRUCT.iGearDefault[MC_playerBD[iLocalPart].iTeam])
			PRINTLN("[HeistBag] SET_BAG_FOR_CASINO_HEIST - Setting available long bitset for new bag as it wasn't set.")
		ENDIF
		
	ENDIF
ENDPROC

PROC SET_CACHED_HEIST_GEAR_BAG()
	
	IF IS_HEIST_GEAR_A_BAG(sApplyOutfitData.eGear)
		DEBUG_PRINTCALLSTACK()
		MC_playerBD_1[iLocalPart].eHeistGearBagType = sApplyOutfitData.eGear
		MC_playerBD_1[iLocalPart].mnHeistGearBag = GET_HEIST_BAG_AS_MODEL(MC_playerBD_1[iLocalPart].eHeistGearBagType)
		PRINTLN("SET_CACHED_HEIST_GEAR_BAG - MC_playerBD_1[iLocalPart].mnHeistGearBag: ", GET_MODEL_NAME_FOR_DEBUG(MC_playerBD_1[iLocalPart].mnHeistGearBag))
		IF sLEGACYMissionLocalContinuityVars.iHeistBag != ENUM_TO_INT(MC_playerBD_1[iLocalPart].eHeistGearBagType)
		AND ENUM_TO_INT(MC_playerBD_1[iLocalPart].eHeistGearBagType) != -1
			sLEGACYMissionLocalContinuityVars.iHeistBag = ENUM_TO_INT(MC_playerBD_1[iLocalPart].eHeistGearBagType)
			PRINTLN("[HeistBag] SET_CACHED_HEIST_GEAR_BAG - sLEGACYMissionLocalContinuityVars.iHeistBag: ", sLEGACYMissionLocalContinuityVars.iHeistBag)
		ENDIF
		
		PRINTLN("[HeistBag] SET_CACHED_HEIST_GEAR_BAG - eHeistGearBagType now ", MC_playerBD_1[iLocalPart].eHeistGearBagType)
	ENDIF

ENDPROC

PROC INCREMENT_OFF_RULE_MINIGAME_SCORE_FROM_OBJECT(INT iIncrement, INT iObj)
	IF iObj = -1
	OR iObj >= FMMC_MAX_NUM_OBJECTS
		EXIT
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iPointAwardsBS, ciFMMC_USE_SCORE_OFF_RULE_MINIGAMES)
	AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetThree, cibsOBJ3_AllowMinigameOffRule)
		DEBUG_PRINTCALLSTACK()
		MC_playerBD_1[iLocalPart].iOffRuleMinigameScoreForMedal += iIncrement
		PRINTLN("INCREMENT_OFF_RULE_MINIGAME_SCORE_FROM_OBJECT - Added ", iIncrement, " to iOffRuleMinigameScoreForMedal = ", MC_playerBD_1[iLocalPart].iOffRuleMinigameScoreForMedal)
	ENDIF
ENDPROC
PROC INCREMENT_OFF_RULE_MINIGAME_SCORE(INT iIncrement)
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iPointAwardsBS, ciFMMC_USE_SCORE_OFF_RULE_MINIGAMES)
		DEBUG_PRINTCALLSTACK()
		MC_playerBD_1[iLocalPart].iOffRuleMinigameScoreForMedal += iIncrement
		PRINTLN("INCREMENT_OFF_RULE_MINIGAME_SCORE - Added ", iIncrement, " to iOffRuleMinigameScoreForMedal = ", MC_playerBD_1[iLocalPart].iOffRuleMinigameScoreForMedal)
	ENDIF
ENDPROC

FUNC BOOL HAS_DRONE_BEEN_USED()
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_DroneUsageTracking)
		//Not tracking drone usage
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(sLEGACYMissionLocalContinuityVars.iGeneralLocalTrackingBitset, ciLEGACY_ContinuityGenericTrackingBS_DroneUsed)
		RETURN TRUE		
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_LOCATION_READY_FOR_PROCESSING(INT iLocation)
	IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLocation].iLocProcessingDelay > 0		
		IF NOT HAS_NET_TIMER_STARTED(tdLocationProcDelayTimer[iLocation])
		OR NOT HAS_NET_TIMER_EXPIRED_READ_ONLY(tdLocationProcDelayTimer[iLocation], g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLocation].iLocProcessingDelay*1000)		
			PRINTLN("SHOULD_PROCESS_LOCATION - IS_LOCATION_READY_FOR_PROCESSING location: ", iLocation, " has a processing delay that has not yet expired (Delay length ", g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLocation].iLocProcessingDelay, "). Returning False.")
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC



FUNC BOOL IS_LOCAL_PLAYER_WEARING_A_BUGSTAR_OUTFIT()
	IF MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_BUGSTARS_0)
	OR MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_BUGSTARS_1)
	OR MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_BUGSTARS_2)
	OR MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_BUGSTARS_3)
	OR MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_BUGSTARS_4)
	OR MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_BUGSTARS_5)
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_WEARING_A_MAINTENANCE_OUTFIT()
	IF MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_MAINTENANCE_0)
	OR MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_MAINTENANCE_1)
	OR MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_MAINTENANCE_2)
	OR MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_MAINTENANCE_3)
	OR MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_MAINTENANCE_4)
	OR MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_MAINTENANCE_5)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_WEARING_A_GRUPPE_SECHS_OUTFIT()
	IF MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_GRUPPE_SECHS_0)
	OR MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_GRUPPE_SECHS_1)
	OR MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_GRUPPE_SECHS_2)
	OR MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_GRUPPE_SECHS_3)
	OR MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_GRUPPE_SECHS_4)
	OR MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_GRUPPE_SECHS_5)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_WEARING_A_CELEB_OUTFIT()
	IF MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_CELEB_0)
	OR MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_CELEB_1)
	OR MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_CELEB_2)
	OR MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_CELEB_3)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_WEARING_A_FIREMAN_OUTFIT()
	IF MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_FIREFIGHTER_0)
	OR MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_FIREFIGHTER_1)
	OR MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_FIREFIGHTER_2)
	OR MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_FIREFIGHTER_3)
	OR MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_FIREFIGHTER_4)
	OR MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_FIREFIGHTER_5)
	OR MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_FIREFIGHTER_6)
	OR MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_FIREFIGHTER_7)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_WEARING_A_NOOSE_OUTFIT()
	IF MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_NOOSE_0)
	OR MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_NOOSE_1)
	OR MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_NOOSE_2)
	OR MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_NOOSE_3)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_WEARING_A_HIGH_ROLLER_OUTFIT()
	IF MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_HIGH_ROLLER_0)
	OR MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_HIGH_ROLLER_1)
	OR MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_HIGH_ROLLER_2)
	OR MC_playerBD[iLocalPart].iOutfit = ENUM_TO_INT(OUTFIT_CASINO_HEIST_HIGH_ROLLER_3)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


FUNC BOOL SHOULD_DISGUISE_BLOCK_AGGRO_FOR_TEAM(INT iped,INT iTeam)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iDisguiseRule[iTeam] > -1
		IF 	NOT  IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFourteen, ciPED_BSFourteen_DisguiseBeforeAndInclRuleT0 + iTeam * 2) 
		AND NOT	IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFourteen,ciPED_BSFourteen_DisguiseAfterAndInclRuleT0 + iTeam * 2) 
				
			//Only work on this rule, not on any others:
			IF MC_serverBD_4.iCurrentHighestPriority[iTeam] = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iDisguiseRule[iTeam]
//				PRINTLN("[RCC MISSION] SHOULD_DISGUISE_BLOCK_AGGRO_FOR_TEAM - Disguise active - rule is equal disguise rule (CURRENT RULE) ", 
//					MC_serverBD_4.iCurrentHighestPriority[iTeam],  "  DISGUISE RULE:  ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iDisguiseRule[iTeam])
				RETURN TRUE
			ELSE
//				PRINTLN("[RCC MISSION] SHOULD_DISGUISE_BLOCK_AGGRO_FOR_TEAM - Disguise inactive - current rule does not equal disguise rule (CURRENT RULE) ", 
//					MC_serverBD_4.iCurrentHighestPriority[iTeam],  "  DISGUISE RULE:  ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iDisguiseRule[iTeam])
				RETURN FALSE
			ENDIF
		ELSE
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFourteen,ciPED_BSFourteen_DisguiseAfterAndInclRuleT0 + iTeam * 2)
				IF MC_serverBD_4.iCurrentHighestPriority[iTeam] >= g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iDisguiseRule[iTeam]
//					PRINTLN("[RCC MISSION] SHOULD_DISGUISE_BLOCK_AGGRO_FOR_TEAM - Disguise active - rule is equal or greater disguise rule (CURRENT RULE) ", 
//						MC_serverBD_4.iCurrentHighestPriority[iTeam],  "  DISGUISE RULE:  ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iDisguiseRule[iTeam])
					RETURN TRUE
				ELSE
//					PRINTLN("[RCC MISSION] SHOULD_DISGUISE_BLOCK_AGGRO_FOR_TEAM - Disguise inactive - rule is not equal or greater disguise rule (CURRENT RULE) ", 
//						MC_serverBD_4.iCurrentHighestPriority[iTeam],  "  DISGUISE RULE:  ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iDisguiseRule[iTeam])
					RETURN FALSE
				ENDIF
			ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iPedBitsetFourteen,ciPED_BSFourteen_DisguiseBeforeAndInclRuleT0 + iTeam * 2)
				IF MC_serverBD_4.iCurrentHighestPriority[iTeam] <= g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iDisguiseRule[iTeam]
//					PRINTLN("[RCC MISSION] SHOULD_DISGUISE_BLOCK_AGGRO_FOR_TEAM - Disguise active - rule is equal or less disguise rule (CURRENT RULE) ", 
//						MC_serverBD_4.iCurrentHighestPriority[iTeam],  "  DISGUISE RULE:  ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iDisguiseRule[iTeam])
					RETURN TRUE
				ELSE
//					PRINTLN("[RCC MISSION] SHOULD_DISGUISE_BLOCK_AGGRO_FOR_TEAM - Disguise inactive - rule is not equal or less disguise rule (CURRENT RULE) ", 
//						MC_serverBD_4.iCurrentHighestPriority[iTeam],  "  DISGUISE RULE:  ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iped].iDisguiseRule[iTeam])
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
//	PRINTLN("[RCC MISSION]  - Disguise active - No disguise rule set")

	RETURN TRUE
ENDFUNC



/// PURPOSE:
///    For the casino heist; checks a ped bitset against player outfit, and then returns true if it's one they should ignore.
/// PARAMS:
///    iPed - 
/// RETURNS:
///    Returns false by default, so will pass through if no costume is set.
FUNC BOOL DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_PED(INT iPed, INT iThisTeam = -1)
	UNUSED_PARAMETER(iPed)
	#IF FEATURE_CASINO_HEIST
		//TODO: Temp outfit names - update when correct ones are available.
			
		IF iThisTeam = -1
			INT iTeam
			FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
				IF NOT SHOULD_DISGUISE_BLOCK_AGGRO_FOR_TEAM(iPed, iTeam)
					PRINTLN("[RCC MISSION]  - DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_PED - SHOULD_DISGUISE_BLOCK_AGGRO_FOR_TEAM - RULE NOT SET: ", iPed, " MC_serverBD_4.iCurrentHighestPriority[iTeam] : ", MC_serverBD_4.iCurrentHighestPriority[iTeam] , " g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iAgroFailPriority[iTeam]: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iAgroFailPriority[iTeam] )			
					RETURN FALSE
				ENDIF
			ENDFOR
		ELSE
			IF NOT SHOULD_DISGUISE_BLOCK_AGGRO_FOR_TEAM(iPed, iThisTeam)
				PRINTLN("[RCC MISSION]  - DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_PED - SHOULD_DISGUISE_BLOCK_AGGRO_FOR_TEAM - RULE NOT SET: ", iPed, " MC_serverBD_4.iCurrentHighestPriority[iThisTeam] : ", MC_serverBD_4.iCurrentHighestPriority[iThisTeam] , " g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iAgroFailPriority[iThisTeam]: ", g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iAgroFailPriority[iThisTeam] )			
				RETURN FALSE
			ENDIF
		ENDIF
		
		//Disguises don't count when the metal detector triggers - url:bugstar:6125454 - Casino Heist - New gameplay option for the metal detector zone.
		IF IS_BIT_SET(iLocalBoolCheck25, LBOOL25_A_PLAYER_HAS_SUPER_ALERTED_METAL_DETECTOR)
			PRINTLN("[RCC MISSION]  - DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_PED: Disguises don't trigger due to metal detector trigger") 
		 	RETURN FALSE
		ENDIF
		
		
		//CASINO_HEIST_OUTFIT_IN__BUGSTAR 
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetThirteen, ciPED_BSThirteen_IgnoreDisguise_Bugstar)
		AND IS_LOCAL_PLAYER_WEARING_A_BUGSTAR_OUTFIT()
			
			PRINTLN("[RCC MISSION]  - DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_PED - player is wearing bugstar disguise")			
			RETURN TRUE
		ENDIF
		
		// CASINO_HEIST_OUTFIT_IN__MECHANIC       
		IF IS_LOCAL_PLAYER_WEARING_A_MAINTENANCE_OUTFIT()
		AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetThirteen, ciPED_BSThirteen_IgnoreDisguise_Mechanic)
			PRINTLN("[RCC MISSION] - DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_PED - Ped ignores mechanic disguise")			
			RETURN TRUE
		ENDIF
		
		//  CASINO_HEIST_OUTFIT_IN__GRUPPE_SECHS
		IF IS_LOCAL_PLAYER_WEARING_A_GRUPPE_SECHS_OUTFIT()
			PRINTLN("[RCC MISSION] - DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_PED - Player wearing gruppe sechs PED : ", iPed)			
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetThirteen, ciPED_BSThirteen_IgnoreDisguise_GruppeSechs)
				PRINTLN("[RCC MISSION] - DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_PED - Ped ignores Gruppe Sechs disguise PED : ", iPed)			
				RETURN TRUE
			ENDIF
		ENDIF
		
		//CASINO_HEIST_OUTFIT_IN__CELEBRITY
		IF IS_LOCAL_PLAYER_WEARING_A_CELEB_OUTFIT()
		AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetThirteen, ciPED_BSThirteen_IgnoreDisguise_Brucie)
			PRINTLN("[RCC MISSION] - DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_PED - Ped ignores Brucie disguise")		
			RETURN TRUE
		ENDIF
		
		// CASINO_HEIST_OUTFIT_OUT__FIREMAN
		IF IS_LOCAL_PLAYER_WEARING_A_FIREMAN_OUTFIT()
		AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetThirteen, ciPED_BSThirteen_IgnoreDisguise_Fireman)
			PRINTLN("[RCC MISSION] - DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_PED - Ped ignores Fireman disguise")		
			RETURN TRUE
		ENDIF
		
		// CASINO_HEIST_OUTFIT_OUT__HIGH_ROLLER
		IF IS_LOCAL_PLAYER_WEARING_A_HIGH_ROLLER_OUTFIT()
		AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetThirteen, ciPED_BSThirteen_IgnoreDisguise_HighRoller)
			PRINTLN("[RCC MISSION] - DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_PED - Ped ignores High Roller disguise")		
			RETURN TRUE
		ENDIF
		
		//CASINO_HEIST_OUTFIT_OUT__SWAT
		IF IS_LOCAL_PLAYER_WEARING_A_NOOSE_OUTFIT()
		AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPedBitsetThirteen, ciPED_BSThirteen_IgnoreDisguise_Noose)
			PRINTLN("[RCC MISSION] - DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_PED - Ped ignores Noose disguise")		
			RETURN TRUE
		ENDIF
	#ENDIF
	RETURN FALSE
ENDFUNC


PROC OVERRIDE_PED_RELATIONSHIP_TO_PLAYER_IF_IN_VALID_DISGUISE(PED_INDEX tempPed, INT iPed)
	IF NOT IS_BIT_SET(iPedSetupDisguiseOverride[GET_LONG_BITSET_INDEX(iPed)], GET_LONG_BITSET_BIT(iPed))
	
		IF DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_PED(iped)
			PRINTLN("[SCRIPT INIT][MissionVariation] - (IS_USING_MISSION_VARIATION_PLAYER_DISGUISE) Ped: ", iPed, " set to override")
				
		
			INT iCop
				
			IF IS_PED_A_SCRIPTED_COP(iPed)
				icop = FM_RelationshipLikeCops
			ELSE
				icop = FM_RelationshipNothingCops
			ENDIF
			
			INT iteamrel[FMMC_MAX_TEAMS]
			INT irepeat
			FOR irepeat = 0 TO (FMMC_MAX_TEAMS-1)
				
				IF  irepeat = 0
				
					iteamrel[irepeat] = FM_RelationshipDislike
					
					//Only auto-target if they're going to respond with hostility
					IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iFlee != ciPED_FLEE_ON
						IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iCombatStyle = ciPED_AGRESSIVE
						OR g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iCombatStyle = ciPED_BERSERK
						OR ( g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iCombatStyle = ciPED_DEFENSIVE AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].gun != WEAPONTYPE_UNARMED AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].gun != WEAPONTYPE_INVALID )
							SET_PED_CAN_BE_TARGETTED_BY_TEAM(tempPed, irepeat, TRUE)
						ELSE
							SET_PED_CAN_BE_TARGETTED_BY_TEAM(tempPed, irepeat, FALSE)
						ENDIF
					ELSE
						IF ( g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].gun != WEAPONTYPE_UNARMED AND g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].gun != WEAPONTYPE_INVALID )
							SET_PED_CAN_BE_TARGETTED_BY_TEAM(tempPed, irepeat, TRUE)
						ELSE
							SET_PED_CAN_BE_TARGETTED_BY_TEAM(tempPed, irepeat, FALSE)
						ENDIF
					ENDIF
					
					SET_ENTITY_IS_TARGET_PRIORITY(tempPed, FALSE)
					PRINTLN("[RCC MISSION] OVERRIDE_PED_RELATIONSHIP_TO_PLAYER_IF_IN_VALID_DISGUISE - ped dislikes team: ",irepeat," ped: ",iPed) 
					SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_ALWAYS_FIGHT, FALSE)
				ELSE
					//leaves these ones as they were before.
					IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iTeam[irepeat] = ciPED_RELATION_SHIP_LIKE
						iteamrel[irepeat] = FM_RelationshipLike 
					ELIF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iTeam[irepeat] = ciPED_RELATION_SHIP_DISLIKE
						iteamrel[irepeat] = FM_RelationshipDislike
					ELIF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iTeam[irepeat] = ciPED_RELATION_SHIP_HATE
						iteamrel[irepeat] = FM_RelationshipHate
					ENDIF
				ENDIF
				
			ENDFOR
			
			SET_PED_RELATIONSHIP_GROUP_HASH(tempPed,rgFM_AiPed[iteamrel[0]][iteamrel[1]][iteamrel[2]][iteamrel[3]][icop])	//Use FM_RelationshipNothingCops or FM_RelationshipLikeCops				
		ENDIF
		
		SET_BIT(iPedSetupDisguiseOverride[GET_LONG_BITSET_INDEX(iPed)], GET_LONG_BITSET_BIT(iPed))
	ENDIF
ENDPROC



// ZONE TIMER HELPERS // // // // // // // // // // // // // // // // // // // // // // // // // // 
FUNC INT GET_ZONE_TIMER_ELAPSED_TIME(INT iZone)

	IF NOT HAS_NET_TIMER_STARTED(stZoneTimers[iZone])
		RETURN 0
	ENDIF
	
	RETURN GET_ABS_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stZoneTimers[iZone])
ENDFUNC

FUNC INT GET_ZONE_TIMER_TIME_REMAINING(INT iZone, BOOL bInSeconds = FALSE)

	INT iTimeRemaining = (g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneTimer_EnableTime - GET_ZONE_TIMER_ELAPSED_TIME(iZone))
	
	IF bInSeconds
		iTimeRemaining /= 1000
	ENDIF
	
	RETURN iTimeRemaining
ENDFUNC

FUNC BOOL HAS_ZONE_TIMER_COMPLETED(INT iZone)
	
	IF IS_BIT_SET(iZoneTimersCompletedBS, iZone)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_ZONE_TIMER_EXPIRED(INT iZone, INT iExpiryTime = -1)
	
	IF iExpiryTime = -1
		iExpiryTime = g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneTimer_EnableTime
	ENDIF
	
	RETURN GET_ZONE_TIMER_ELAPSED_TIME(iZone) >= iExpiryTime
ENDFUNC

FUNC BOOL IS_ZONE_TIMER_RUNNING(INT iZone)

	IF HAS_ZONE_TIMER_COMPLETED(iZone)
		RETURN FALSE
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(stZoneTimers[iZone])
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SET_SYNC_LOCK_PROGRESS(INT iNewProgress)
	MC_playerBD[iPartToUse].iSyncAnimProgress = iNewProgress
	
	PRINTLN("[HumaneSyncLock] SET_SYNC_LOCK_PROGRESS | Setting iSyncAnimProgress to ", MC_playerBD[iPartToUse].iSyncAnimProgress)
	DEBUG_PRINTCALLSTACK()
ENDPROC

PROC INCREMENT_SYNC_LOCK_PROGRESS()
	SET_SYNC_LOCK_PROGRESS(MC_playerBD[iPartToUse].iSyncAnimProgress + 1)
ENDPROC

FUNC BOOL SHOULD_OBJECT_MINIGAME_BE_DISABLED_BY_POISON_GAS(INT iObj = -1)
	IF iObj > -1
		IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetFour, cibsOBJ4_DisableDuringPoisonGas)
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(stPoisonGasRunningTimer)
	AND HAS_NET_TIMER_EXPIRED(stPoisonGasRunningTimer, ciPoisonGasBlockMinigameTime)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_ANIMATED_PROP_TAGS(OBJECT_INDEX oiSpawnedProp)

	// Player events
	IF HAS_ANIM_EVENT_FIRED(LocalPlayerPed, HASH("Create"))
		SET_ENTITY_VISIBLE(oiSpawnedProp, TRUE)
		PRINTLN("PROCESS_ANIMATED_PROP_TAGS - 'Create' event fired on local player | Making prop visible")
	ENDIF
	
	IF HAS_ANIM_EVENT_FIRED(LocalPlayerPed, HASH("Delete"))
		SET_ENTITY_VISIBLE(oiSpawnedProp, FALSE)
		PRINTLN("PROCESS_ANIMATED_PROP_TAGS - 'Delete' event fired on local player | Hiding prop")
	ENDIF
	
	
	// Entity events
	IF HAS_ANIM_EVENT_FIRED(oiSpawnedProp, HASH("Create"))
		SET_ENTITY_VISIBLE(oiSpawnedProp, TRUE)
		PRINTLN("PROCESS_ANIMATED_PROP_TAGS - 'Create' event fired on oiSpawnedProp | Making prop visible")
	ENDIF
	
	IF HAS_ANIM_EVENT_FIRED(oiSpawnedProp, HASH("Delete"))
		SET_ENTITY_VISIBLE(oiSpawnedProp, FALSE)
		PRINTLN("PROCESS_ANIMATED_PROP_TAGS - 'Delete' event fired on oiSpawnedProp | Hiding prop")
	ENDIF
ENDPROC

FUNC BOOL HAS_ANIM_BREAKOUT_EVENT_FIRED()

	IF HAS_ANIM_EVENT_FIRED(LocalPlayerPed, HASH("BREAKOUT"))
	OR HAS_ANIM_EVENT_FIRED(LocalPlayerPed, HASH("Breakout"))
		PRINTLN("HAS_ANIM_BREAKOUT_EVENT_FIRED | Hit a BREAKOUT")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_PARTICIPANT_LOOK_AT_CELLPHONE()

	INT i_Counter
	
	REPEAT GET_MAXIMUM_PLAYERS_ON_MISSION() i_Counter
		IF IS_BIT_SET(MC_playerBD[i_Counter].iClientBitSet2, PBBOOL2_TASK_PLAYER_CUTANIM_INTRO)
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL SHOULD_PARTICIPANT_STOP_LOOKING_AT_CELLPHONE()

	INT i_Counter
	
	REPEAT GET_MAXIMUM_PLAYERS_ON_MISSION() i_Counter
		IF IS_BIT_SET(MC_playerBD[i_Counter].iClientBitSet2, PBBOOL2_TASK_PLAYER_CUTANIM_OUTRO)
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    Returns the minimum heist take the players can steal on a heist that involves take being dropped.
///    Used in ornate bank finale only but could be extended to work in future heists as well.
///    See Lukasz for details.
/// PARAMS:
///    icash - Cash setting set in the content creator, use FMMC_CASH_ORNATE_BANK for ornate bank.
///    Any other value will return GET_CASH_VALUE_FROM_CREATOR(icash).
/// RETURNS:
///    Minimum take cash amount the players can get on a heist where money dropping is involved.
FUNC INT GET_HEIST_MINIMUM_TAKE_FOR_CASH_DROP(INT icash)

	INT 	iBaseTake, iModifiedTake, iMinimumTake, iCasinoTargetMaxLossPercentage
	FLOAT	fHeistDifficulty, fMinimumTakePercentage

	SWITCH icash

		CASE FMMC_CASH_ORNATE_BANK

			SWITCH g_FMMC_STRUCT.iDifficulity
				CASE DIFF_EASY 		fHeistDifficulty = g_sMPTunables.fheist_difficulty_easy 	BREAK
				CASE DIFF_NORMAL 	fHeistDifficulty = g_sMPTunables.fheist_difficulty_normal 	BREAK
				CASE DIFF_HARD 		fHeistDifficulty = g_sMPTunables.fheist_difficulty_hard 	BREAK
				DEFAULT				fHeistDifficulty = g_sMPTunables.fheist_difficulty_normal	BREAK
			ENDSWITCH
			
			iBaseTake 				= GET_CASH_VALUE_FROM_CREATOR(icash)
			iModifiedTake 			= FLOOR( TO_FLOAT(iBaseTake) * fHeistDifficulty )
			fMinimumTakePercentage	= TO_FLOAT(100 - CLAMP_INT(g_sMPTunables.imax_heist_cash_loss_percentage, 0, 100)) / 100.0 
			iMinimumTake			= FLOOR(TO_FLOAT(iModifiedTake) * fMinimumTakePercentage)

		BREAK
		
		CASE FMMC_CASH_CASINO_HEIST
			
			SWITCH g_sCasinoHeistMissionConfigData.eTarget
				CASE CASINO_HEIST_TARGET_TYPE__DIAMONDS iCasinoTargetMaxLossPercentage = g_sMPTunables.iCH_max_cash_loss_percentage_diamonds BREAK
				CASE CASINO_HEIST_TARGET_TYPE__GOLD		iCasinoTargetMaxLossPercentage = g_sMPTunables.iCH_max_cash_loss_percentage_gold	 BREAK
				CASE CASINO_HEIST_TARGET_TYPE__ART		iCasinoTargetMaxLossPercentage = g_sMPTunables.iCH_max_cash_loss_percentage_artwork	 BREAK
				CASE CASINO_HEIST_TARGET_TYPE__CASH		iCasinoTargetMaxLossPercentage = g_sMPTunables.iCH_max_cash_loss_percentage_cash	 BREAK
				DEFAULT 								iCasinoTargetMaxLossPercentage = g_sMPTunables.iCH_max_cash_loss_percentage_cash	 BREAK
			ENDSWITCH
			
			iBaseTake 				= MC_serverBD_1.iVaultTake + MC_serverBD_1.iDailyCashRoomTake + MC_serverBD_1.iDepositBoxTake
			fMinimumTakePercentage	= TO_FLOAT(100 - iCasinoTargetMaxLossPercentage) / 100.0 
			iMinimumTake			= FLOOR(TO_FLOAT(iBaseTake) * fMinimumTakePercentage)
			
//			fMinimumTakePercentage	= TO_FLOAT(iMinimumTake) / 100.0 
//			iMinimumTake			= FLOOR(fMinimumTakePercentage * iCasinoTargetMaxLossPercentage)
			
		BREAK
		
		DEFAULT
			iMinimumTake = GET_CASH_VALUE_FROM_CREATOR(icash)
		BREAK
		
	ENDSWITCH
	
	PRINTLN("GET_HEIST_MINIMUM_TAKE_FOR_CASH_DROP - icash: ", icash)
	PRINTLN("GET_HEIST_MINIMUM_TAKE_FOR_CASH_DROP - iBaseTake: ", iBaseTake)
	PRINTLN("GET_HEIST_MINIMUM_TAKE_FOR_CASH_DROP - fHeistDifficulty: ", fHeistDifficulty)
	PRINTLN("GET_HEIST_MINIMUM_TAKE_FOR_CASH_DROP - iModifiedTake: ", iModifiedTake)
	PRINTLN("GET_HEIST_MINIMUM_TAKE_FOR_CASH_DROP - fMinimumTakePercentage: ", fMinimumTakePercentage)
	PRINTLN("GET_HEIST_MINIMUM_TAKE_FOR_CASH_DROP - iMinimumTake: ", iMinimumTake)
	PRINTLN("GET_HEIST_MINIMUM_TAKE_FOR_CASH_DROP - fEarnings_Heists_Finale_replay_cash_reward: ", 		g_sMPTunables.fEarnings_Heists_Finale_replay_cash_reward)
	PRINTLN("GET_HEIST_MINIMUM_TAKE_FOR_CASH_DROP - fEarnings_Heists_Finale_first_play_cash_reward: ", 	g_sMPTunables.fEarnings_Heists_Finale_first_play_cash_reward)
	
	RETURN iMinimumTake

ENDFUNC

FUNC BOOL SHOULD_BLOCK_HELMET_VISOR_CONTROL()
	IF IS_THIS_ROCKSTAR_MISSION_GANGOPS_MISSION_OF_TYPE(ciGANGOPS_FLOW_MISSION_PREDATOR)
		RETURN TRUE
	ENDIF
	
	IF CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_MISSION_FINALE()
	AND NOT IS_LOCAL_PLAYER_WEARING_A_GRUPPE_SECHS_OUTFIT()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_AN_AIRLOCK_TRANSITION_ENDING_OR_STARTING()
	
	IF HAS_TEAM_FAILED(0) //Update if used outside casino heist
	OR IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_PLAYER_FAIL)
	OR IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_PLAYER_OUT_OF_LIVES)
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_TriggerEndCelebration)
	OR IS_BIT_SET(iCelebrationBitSet, ciCELEBRATION_BIT_SET_bDoDelayedCelebrationFreeze)
	OR sCelebrationData.eCurrentStage > CELEBRATION_STAGE_SETUP
	OR g_bCelebrationScreenIsActive
		RETURN FALSE
	ENDIF
	
	IF g_bStartedInAirlockTransition
		RETURN TRUE
	ENDIF
	
	IF IS_AN_AIRLOCK_STRAND_TRANSITION_RUNNING()
		RETURN TRUE
	ENDIF
	
	IF (g_bEndOfMissionCleanUpHUD
    AND (GET_STRAND_MISSION_TRANSITION_TYPE() = FMMC_BRANCHING_MISSION_TRANSITION_TYPE_AIRLOCK
	OR ((MC_serverBD.iNextMission > -1 AND MC_serverBD.iNextMission < FMMC_MAX_STRAND_MISSIONS)
	AND g_FMMC_STRUCT.eBranchingTransitionType[MC_serverBD.iNextMission] = FMMC_BRANCHING_MISSION_TRANSITION_TYPE_AIRLOCK))
    AND IS_STRAND_MISSION_READY_TO_START_DOWNLOAD())
		//Mission ending and airlock about to be started
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC enumCharacterList GET_INCOMING_CONTACT_CHARACTER_FROM_CREATOR(INT iCharacter)
	SWITCH iCharacter
		CASE DT_CHAR_AGATHA 		RETURN GET_CASINO_CONTACT()
		CASE DT_CHAR_CHENG			RETURN CHAR_CHENG
		CASE DT_CHAR_TRANSLATOR 	RETURN CHAR_CASINO_TAO_TRANSLATOR
		CASE DT_CHAR_YUNG_ANCESTOR 	RETURN CHAR_ARCADE_CELEB
	ENDSWITCH
	
	RETURN MAX_NRM_CHARACTERS_PLUS_DUMMY
ENDFUNC

FUNC INT GET_CURRENT_HEIST_COMPLETION_STAT_FOR_LEADER()
		
	INT iPart
	IF DOES_CURRENT_MISSION_USE_GANG_BOSS()
	AND GB_GET_LOCAL_PLAYER_GANG_BOSS() != INVALID_PLAYER_INDEX()
		IF NETWORK_IS_PLAYER_ACTIVE(GB_GET_LOCAL_PLAYER_GANG_BOSS())
		AND NETWORK_IS_PLAYER_A_PARTICIPANT(GB_GET_LOCAL_PLAYER_GANG_BOSS())
			iPart = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(GB_GET_LOCAL_PLAYER_GANG_BOSS()))
			IF iPart > -1
				RETURN MC_PlayerBD[iPart].iCurrentHeistCompletionStat
			ENDIF
		ENDIF
	ELIF NATIVE_TO_INT(NETWORK_GET_HOST_OF_THIS_SCRIPT()) > -1
		iPart = NATIVE_TO_INT(NETWORK_GET_HOST_OF_THIS_SCRIPT())
		RETURN MC_PlayerBD[iPart].iCurrentHeistCompletionStat
	ENDIF
	
	RETURN -1
ENDFUNC

FUNC BOOL GET_HEIST_COMPLETION_STAT_FOR_PARTICIPANT(INT iPart, INT iHeist)
	IF iPart > -1
	AND iHeist > -1 AND iHeist < HBCA_BS_MISSION_CONTROLLER_MAX
		RETURN IS_BIT_SET(MC_PlayerBD[iPart].iCurrentHeistCompletionStat, iHeist)
	ELSE
		DEBUG_PRINTCALLSTACK()
		PRINTLN("[LM][GET_HEIST_COMPLETION_STAT_FOR_PARTICIPANT] - iPart: ", iPart, " iHeist: ", iHeist)
		ASSERTLN("Invalid Params being passed into GET_HEIST_COMPLETION_STAT_FOR_PARTICIPANT")
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_CASINO_INTERIOR_FLOOR(FLOAT fZHeight)
	IF fZHeight <= -66.5
		IF g_sCasinoHeistMissionConfigData.eChosenApproachType = CASINO_HEIST_APPROACH_TYPE__DIRECT
		AND g_sCasinoHeistMissionConfigData.eEntranceChosen = CASINO_HEIST_ACCESS_POINT__SEWER_ENTRANCE
			RETURN 1
		ELSE
			RETURN 9 //Version of level 1 without the sewer tunnel.
		ENDIF
	ELIF fZHeight <= -61.5
		RETURN 2
	ELIF fZHeight <= -56.5
		RETURN 3
	ELIF fZHeight <= -51
		RETURN 4
	ELIF fZHeight <= -43
		RETURN 5
	ELIF fZHeight <= -35.5
		RETURN 6
	ELIF fZHeight <= -25.5
		RETURN 7
	ENDIF
	
	RETURN 8
ENDFUNC

PROC PRE_PROCESS_WORLD_PROPS()
	// Early Processing to avoid "popping-in" issues with world props in Interiors.	More than one loop with an EXIT because on consoles Objects take multiple frames to complete their creation process.
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_ANY_ADVERSARY_MODE(g_FMMC_STRUCT.iAdversaryModeType)
		INT i
		FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfWorldProps - 1
			LEGACY_PROCESS_WORLD_PROPS(i, sRuntimeWorldPropData)
		ENDFOR
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Warp Portals 
// ##### Description: Functions relating to the Warp Portal System. A way of setting up places that are configurable in the creator via global data, and will warp players from one place to another.
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL IS_THIS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_A_SYNC_SCENE(INT i)
	SWITCH i	
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_DOOR_TRANS_HINGE_L
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_DOOR_TRANS_HINGE_R
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_DOOR_TRANS_HINGE_L_CHARGE
			RETURN TRUE
		BREAK		
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC STRING GET_ENTRY_ANIM_DICT_FROM_WARP_PORTAL_INT(INT i)
	STRING tlReturn
	
	SWITCH i
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_APT_TRANS_HINGE_L
			tlReturn = "anim@apt_trans@hinge_l"
		BREAK
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_APT_TRANS_HINGE_R
			tlReturn = "anim@apt_trans@hinge_r"
		BREAK		
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_APT_TRANS_GARAGE
			tlReturn = "anim@apt_trans@garage"
		BREAK
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_APT_TRANS_HINGE_L_ACTION
			tlReturn = "anim@apt_trans@hinge_l_action"
		BREAK
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_APT_TRANS_VEHICLE
			tlReturn = "Vehicle"
		BREAK
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_DOOR_TRANS_HINGE_L
			tlReturn = "anim@door_trans@hinge_l@"
		BREAK
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_DOOR_TRANS_HINGE_R
			tlReturn = "anim@door_trans@hinge_r@"
		BREAK
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_DOOR_TRANS_HINGE_L_CHARGE
			tlReturn = "anim@door_trans@hinge_l@"
		BREAK		
	ENDSWITCH
	
	RETURN tlReturn
ENDFUNC

FUNC STRING GET_ENTRY_ANIM_CLIP_FROM_WARP_PORTAL_INT(INT i)
	STRING tlReturn
	
	SWITCH i
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_APT_TRANS_HINGE_L
			tlReturn = "ext_player"
		BREAK
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_APT_TRANS_HINGE_R
			tlReturn = "ext_player"
		BREAK
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_APT_TRANS_GARAGE
			tlReturn = "gar_open_1_left"
		BREAK
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_APT_TRANS_HINGE_L_ACTION
			tlReturn = "player_exit"
		BREAK
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_APT_TRANS_VEHICLE
			tlReturn = "Vehicle"
		BREAK
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_DOOR_TRANS_HINGE_L
			tlReturn = "walk_player1"
		BREAK
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_DOOR_TRANS_HINGE_R
			tlReturn = "walk_player1"
		BREAK
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_DOOR_TRANS_HINGE_L_CHARGE
			tlReturn = "charge_player1"
		BREAK		
	ENDSWITCH
	
	RETURN tlReturn
ENDFUNC

FUNC STRING GET_ENTRY_ANIM_CLIP_CAMERA_FROM_WARP_PORTAL_INT(INT i, BOOL bForceLeft)
	STRING tlReturn
	
	SWITCH i		
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_DOOR_TRANS_HINGE_L				
			IF bForceLeft
				tlReturn = "WALK_CAM_LEFT"
			ELSE
				tlReturn = "WALK_CAM_RIGHT"
			ENDIF			
		BREAK
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_DOOR_TRANS_HINGE_R
			IF bForceLeft
				tlReturn = "WALK_CAM_LEFT"
			ELSE
				tlReturn = "WALK_CAM_RIGHT"
			ENDIF			
		BREAK
		CASE FMMC_WARP_PORTAL_ENTRY_ANIM_DOOR_TRANS_HINGE_L_CHARGE
			IF bForceLeft
				tlReturn = "CHARGE_CAM_LEFT"
			ELSE
				tlReturn = "CHARGE_CAM_RIGHT"
			ENDIF	
		BREAK		
	ENDSWITCH
	
	RETURN tlReturn
ENDFUNC

FUNC VECTOR MC_GET_WARP_PORTAL_START_POSITION(INT iPortal)
	RETURN g_FMMC_STRUCT.sWarpPortals[iPortal].vStartCoord
ENDFUNC

FUNC VECTOR MC_GET_WARP_PORTAL_END_POSITION(INT iPortal)
	INT iLinkedPortal = g_FMMC_STRUCT.sWarpPortals[iPortal].iLinkedPortal
	IF iLinkedPortal = -1
		RETURN <<0.0, 0.0, 0.0>>
	ENDIF
	RETURN g_FMMC_STRUCT.sWarpPortals[iLinkedPortal].vStartCoord
ENDFUNC
FUNC FLOAT MC_GET_WARP_PORTAL_START_HEADING(INT iPortal)
	RETURN g_FMMC_STRUCT.sWarpPortals[iPortal].fStartHead
ENDFUNC

FUNC VECTOR MC_GET_WARP_PORTAL_END_FALLBACK_POSITION(INT iPortal, INT iPosition)
	INT iLinkedPortal = g_FMMC_STRUCT.sWarpPortals[iPortal].iLinkedPortal
	IF iLinkedPortal = -1
		RETURN <<0.0, 0.0, 0.0>>
	ENDIF
	RETURN g_FMMC_STRUCT.sWarpPortals[iLinkedPortal].vExtraStartCoord[iPosition]
ENDFUNC

FUNC FLOAT MC_GET_WARP_PORTAL_START_FALLBACK_HEADING(INT iPortal, INT iPosition)
	RETURN g_FMMC_STRUCT.sWarpPortals[iPortal].fExtraStartHead[iPosition]
ENDFUNC

FUNC FLOAT MC_GET_WARP_PORTAL_END_HEADING(INT iPortal)
	INT iLinkedPortal = g_FMMC_STRUCT.sWarpPortals[iPortal].iLinkedPortal
	IF iLinkedPortal = -1
		RETURN 0.0
	ENDIF
	RETURN g_FMMC_STRUCT.sWarpPortals[iLinkedPortal].fStartHead
ENDFUNC

FUNC FLOAT MC_GET_WARP_PORTAL_END_FALLBACK_HEADING(INT iPortal, INT iPosition)
	INT iLinkedPortal = g_FMMC_STRUCT.sWarpPortals[iPortal].iLinkedPortal
	IF iLinkedPortal = -1
		RETURN 0.0
	ENDIF
	RETURN g_FMMC_STRUCT.sWarpPortals[iLinkedPortal].fExtraStartHead[iPosition]
ENDFUNC

PROC PROCESS_WARP_PORTAL_DISPLAY(INT iPortal, BOOL bActive)
	
	IF IS_VECTOR_ZERO(MC_GET_WARP_PORTAL_START_POSITION(iPortal))
		EXIT
	ENDIF
		
	IF bActive
		FLOAT fRange = GET_WARP_PORTAL_START_RANGE(iPortal)
		VECTOR vRange = <<fRange*1.5, fRange*1.5, fRange*0.6>>
		//VECTOR vRangeSmall = <<fRange, fRange, fRange*0.35>>
		VECTOR vStart = <<g_FMMC_STRUCT.sWarpPortals[iPortal].vStartCoord.x, g_FMMC_STRUCT.sWarpPortals[iPortal].vStartCoord.y, g_FMMC_STRUCT.sWarpPortals[iPortal].vStartCoord.z-0.15>>
		INT iBlipCol
		INT iR, iG, iB, iA	
		HUD_COLOURS colPortal
		iBlipCol = BLIP_COLOUR_BLUE				
				
		IF IS_BIT_SET(g_FMMC_STRUCT.sWarpPortals[iPortal].iWarpPortalBS, ciWARP_PORTAL_BlipLocation)
			IF DOES_BLIP_EXIST(sWarpPortal.biStartWarpPortals[iPortal])
				
			ELSE
				sWarpPortal.biStartWarpPortals[iPortal] = ADD_BLIP_FOR_COORD(g_FMMC_STRUCT.sWarpPortals[iPortal].vStartCoord)
				SET_BLIP_COLOUR(sWarpPortal.biStartWarpPortals[iPortal], iBlipCol)
			ENDIF			
		ENDIF
		
		IF NOT IS_BIT_SET(sWarpPortal.iJustUsedPortalEndBS, iPortal)
			colPortal = HUD_COLOUR_BLUE
			GET_HUD_COLOUR(colPortal, iR, iG, iB, iA)
		ELSE
			colPortal = HUD_COLOUR_RED
			GET_HUD_COLOUR(colPortal, iR, iG, iB, iA)
		ENDIF
		iA = 140
		
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.sWarpPortals[iPortal].iWarpPortalBS, ciWARP_PORTAL_HideStartPortalMarker)
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.sWarpPortals[iPortal].iWarpPortalBS, ciWARP_PORTAL_PortalPTFX)
				DRAW_MARKER(MARKER_CYLINDER, vStart, <<0.0,0.0,0.0>>, <<0.0,0.0,0.0>>, vRange, iR, iG, iB, iA)
				//DRAW_MARKER(MARKER_CYLINDER, vStart, <<0.0,0.0,0.0>>, <<0.0,0.0,0.0>>, vRangeSmall, iR, iG, iB, 40)
			ELSE
				// Make it look like a portal (WIP)
				DRAW_MARKER(MARKER_CYLINDER, vStart, <<0.0,0.0,0.0>>, <<0.0,0.0,0.0>>, vRange, iR, iG, iB, iA)
				DRAW_MARKER(MARKER_CYLINDER, vStart+<<0.0,0.0,2.0*fRange>>, <<0.0,0.0,0.0>>, <<0.0,0.0,0.0>>, vRange, iR, iG, iB, iA)
			ENDIF
		ENDIF
	ELSE
		IF DOES_BLIP_EXIST(sWarpPortal.biStartWarpPortals[iPortal])
			REMOVE_BLIP(sWarpPortal.biStartWarpPortals[iPortal])
		ENDIF
	ENDIF
ENDPROC
 
PROC SET_INSTANCED_PORTAL_ANIM_CLIENT_STATE(WARP_PORTAL_ANIM_STATE eWarpPortalAnimStateNew)
	PRINTLN("[LM][SET_INSTANCED_PORTAL_ANIM_CLIENT_STATE][wPortals] - Setting Anim State New: ", ENUM_TO_INT(eWarpPortalAnimStateNew), " From Old: ", ENUM_TO_INT(sWarpPortal.eWarpPortalAnimState))
	sWarpPortal.eWarpPortalAnimState = eWarpPortalAnimStateNew	
ENDPROC

FUNC BOOL HAS_WARP_PORTAL_SYNC_STAGE_FINISHED(INT iPortal)
	IF IS_BIT_SET(g_FMMC_STRUCT.sWarpPortals[iPortal].iWarpPortalBS, ciWARP_PORTAL_DoNoFadeInScreenAfterUse)
		PRINTLN("[LM][PROCESS_INSTANCED_CONTENT_PORTALS][wPortals] - Set to ignore Fadein.")
		RETURN TRUE
	ELSE
		IF IS_SCREEN_FADED_OUT()
			PRINTLN("[LM][PROCESS_INSTANCED_CONTENT_PORTALS][wPortals] - Fading Screen Back In.")
			DO_SCREEN_FADE_IN(1000)
		ENDIF				
		IF IS_SCREEN_FADED_IN()
			PRINTLN("[LM][PROCESS_INSTANCED_CONTENT_PORTALS][wPortals] - Screen Faded Back In.")
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL PROCESS_MOVE_WARP_PORTAL_VEHICLE_TOWARD_DESTINATION(INT iPortal)
	
	IF NOT DOES_ENTITY_EXIST(sWarpPortal.vehEntryAnimClone)
	OR NOT IS_VEHICLE_DRIVEABLE(sWarpPortal.vehEntryAnimClone)
		RETURN FALSE
	ENDIF
	
	FLOAT fAmountToMove = 0.0035
	FLOAT fModifier = TO_FLOAT(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(sWarpPortal.tdCamAnimTimer)) / 500.0
	fModifier += 1.0
	fAmountToMove *= fModifier	
	IF fAmountToMove > 0.125
		fAmountToMove  = 0.125
	ENDIF
	
	PRINTLN("[LM][PROCESS_MOVE_WARP_PORTAL_VEHICLE_TOWARD_DESTINATION][wPortals] - fAmountToMove: ", fAmountToMove, " fModifier: ", fModifier, " Timer: ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(sWarpPortal.tdCamAnimTimer))
		
	VECTOR vCoord = GET_ENTITY_COORDS(sWarpPortal.vehEntryAnimClone)
	FLOAT fHead = GET_ENTITY_HEADING(sWarpPortal.vehEntryAnimClone)
	VECTOR vNewCoord = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCoord, fHead, <<0.0, fAmountToMove, 0.0>>)
	FLOAT fDist = GET_DISTANCE_BETWEEN_COORDS(g_FMMC_STRUCT.sWarpPortals[iPortal].vEntryAnimCoord, GET_ENTITY_COORDS(sWarpPortal.vehEntryAnimClone), FALSE)
	
	PRINTLN("[LM][PROCESS_MOVE_WARP_PORTAL_VEHICLE_TOWARD_DESTINATION][wPortals] - vCoord: ", vCoord, " fHead: ", fHead, " vNewCoord: ", vNewCoord, " fDist: ", fDist)
		
	SET_ENTITY_COORDS_NO_OFFSET(sWarpPortal.vehEntryAnimClone, vNewCoord, FALSE, FALSE, TRUE)
	FREEZE_ENTITY_POSITION(sWarpPortal.vehEntryAnimClone, TRUE)
	
	IF fDist >= 15.0
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL PROCESS_WARP_PORTAL_CUTSCENE_HELP_CLONE_VEH_AND_PASSENGERS(INT iPortal, VEHICLE_INDEX &vehToCreate, VEHICLE_INDEX vehToClone, VEHICLE_INDEX &vehTrailerToCreate)
	
	IF DOES_ENTITY_EXIST(vehToCreate)
		
		IF NOT CUTSCENE_HELP_VEH_PASSENGERS_CLONE(vehToClone, vehToCreate, sWarpPortal.pedEntryAnimClones, FALSE, TRUE, FALSE)
			PRINTLN("[LM][CUTSCENE_HELP][wPortals] - PROCESS_WARP_PORTAL_CUTSCENE_HELP_CLONE_VEH_AND_PASSENGERS - Passengers not created yet.")
			RETURN FALSE
		ENDIF
		
		IF DOES_ENTITY_EXIST(vehToClone)
			IF IS_ENTITY_ALIVE(vehToClone)
				VEHICLE_INDEX vehTrailer
				GET_VEHICLE_TRAILER_VEHICLE(vehToClone, vehTrailer)
				IF DOES_ENTITY_EXIST(vehTrailer)
					IF NOT CUTSCENE_HELP_CLONE_VEH_TRAILER(vehToClone, vehTrailerToCreate)
						PRINTLN("[LM][CUTSCENE_HELP][wPortals] - PROCESS_WARP_PORTAL_CUTSCENE_HELP_CLONE_VEH_AND_PASSENGERS - Trailer not created yet")
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
			
		RETURN TRUE
	ELSE
		IF CREATE_VEHICLE_CLONE(vehToCreate, vehToClone, g_FMMC_STRUCT.sWarpPortals[iPortal].vEntryAnimCoord, g_FMMC_STRUCT.sWarpPortals[iPortal].fEntryAnimHead, FALSE, FALSE)
			PRINTLN("[LM][CUTSCENE_HELP][wPortals] PROCESS_WARP_PORTAL_CUTSCENE_HELP_CLONE_VEH_AND_PASSENGERS - Delivery vehicle clone created succesfully")
			
			IF DOES_ENTITY_EXIST(vehToCreate)
				PRINTLN("[LM][CUTSCENE_HELP][wPortals] PROCESS_WARP_PORTAL_CUTSCENE_HELP_CLONE_VEH_AND_PASSENGERS - Vehicle exists.")
				//Clone vehicle mods.
				VEHICLE_SETUP_STRUCT_MP vehicleSetup
				GET_VEHICLE_SETUP_MP(vehToClone, vehicleSetup)
				SET_VEHICLE_SETUP_MP(vehToCreate, vehicleSetup)
				//clone extras
				VEHICLE_COPY_EXTRAS(vehToClone, vehToCreate)
				
				SET_ENTITY_COLLISION(vehToCreate, TRUE)
				SET_ENTITY_VISIBLE(vehToCreate, FALSE)
				SET_ENTITY_INVINCIBLE(vehToCreate, TRUE)
				SET_VEHICLE_CAN_BE_VISIBLY_DAMAGED(vehToCreate, FALSE)
				FREEZE_ENTITY_POSITION(vehToCreate, FALSE)
				
				MODEL_NAMES eModel = GET_ENTITY_MODEL(vehToCreate)

				IF NOT IS_THIS_MODEL_A_BIKE(eModel)
				AND NOT IS_THIS_MODEL_A_BICYCLE(eModel)
				AND NOT IS_THIS_MODEL_A_BOAT(eModel)
				AND NOT IS_THIS_MODEL_A_SEA_VEHICLE(eModel)
				AND NOT (eModel = OPPRESSOR)
					SET_VEHICLE_CAN_BREAK(vehToCreate, FALSE)
					PRINTLN("[LM][CUTSCENE_HELP][wPortals] - PROCESS_WARP_PORTAL_CUTSCENE_HELP_CLONE_VEH_AND_PASSENGERS - Preventing vehicle from being able to break apart.")
				ELSE
					PRINTLN("[LM][CUTSCENE_HELP][wPortals] - PROCESS_WARP_PORTAL_CUTSCENE_HELP_CLONE_VEH_AND_PASSENGERS - Vehicle type cannot be set as breakable.")
				ENDIF
				
				//LIGHTS
				INT iOn, iFullBeam
				GET_VEHICLE_LIGHTS_STATE(vehToClone, iOn, iFullBeam)
				PRINTLN("[LM][CUTSCENE_HELP][wPortals] - PROCESS_WARP_PORTAL_CUTSCENE_HELP_CLONE_VEH_AND_PASSENGERS - On: ", iOn, " FullBeam: ", iFullBeam)
				IF iOn <> 0
					SET_VEHICLE_LIGHTS(vehToCreate, FORCE_VEHICLE_LIGHTS_ON)
				ENDIF
				
				IF iFullBeam <> 0
					SET_VEHICLE_FULLBEAM(vehToCreate, TRUE)
				ENDIF
				
				SET_VEHICLE_ENGINE_ON(vehToCreate, TRUE, TRUE)
								
				/*INT iWheel
				FOR iWheel = SC_WHEEL_CAR_FRONT_LEFT TO SC_WHEEL_BIKE_REAR
					IF IS_VEHICLE_TYRE_BURST(vehCloneStruct.vehicle, SC_WHEEL_CAR_FRONT_LEFT)
						SET_VEHICLE_TYRE_FIXED(vehCloneStruct.vehicle, INT_TO_ENUM(SC_WHEEL_LIST, iWheel))
					ENDIF
				ENDFOR*/
				
				/*INT iWheel
				FOR iWheel = SC_WHEEL_CAR_FRONT_LEFT TO SC_WHEEL_BIKE_REAR
					IF IS_VEHICLE_TYRE_BURST(vehCloneStruct.vehicle, SC_WHEEL_CAR_FRONT_LEFT)
						SET_VEHICLE_TYRE_FIXED(vehCloneStruct.vehicle, INT_TO_ENUM(SC_WHEEL_LIST, iWheel))
					ENDIF
				ENDFOR*/
			ENDIF
		ELSE
			PRINTLN("[CUTSCENE_HELP][wPortals] - PROCESS_WARP_PORTAL_CUTSCENE_HELP_CLONE_VEH_AND_PASSENGERS - Unable to create Player vehicle clone.")
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

PROC PROCESS_WARP_PORTAL_SAFETY_TIMER()
	IF NOT HAS_NET_TIMER_STARTED(sWarpPortal.tdCamAnimTimerSafety)
		PRINTLN("[LM][HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED][wPortals] - Starting Safety Timer.")
		START_NET_TIMER(sWarpPortal.tdCamAnimTimerSafety)
	ENDIF
	IF HAS_NET_TIMER_EXPIRED_READ_ONLY(sWarpPortal.tdCamAnimTimerSafety, 8000)				
		PRINTLN("[LM][HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED][wPortals] - Safety Timer ran out.")
		SET_INSTANCED_PORTAL_ANIM_CLIENT_STATE(eWarpPortalAnimState_Cleanup)
	ENDIF
ENDPROC

FUNC BOOL HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED(INT iPortal)
	STRING sAnimDict = GET_ENTRY_ANIM_DICT_FROM_WARP_PORTAL_INT(g_FMMC_STRUCT.sWarpPortals[iPortal].iEntryAnimType)
	STRING sAnimName = GET_ENTRY_ANIM_CLIP_FROM_WARP_PORTAL_INT(g_FMMC_STRUCT.sWarpPortals[iPortal].iEntryAnimType)
	STRING sCamAnimName = GET_ENTRY_ANIM_CLIP_CAMERA_FROM_WARP_PORTAL_INT(g_FMMC_STRUCT.sWarpPortals[iPortal].iEntryAnimType, IS_BIT_SET(g_FMMC_STRUCT.sWarpPortals[iPortal].iWarpPortalBS, ciWARP_PORTAL_CamForceLeftAnim))
	INT iWarpPortalThisLocalSceneID = -1
	
	SWITCH sWarpPortal.eWarpPortalAnimState
		CASE eWarpPortalAnimState_Init
			IF IS_THIS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_A_SYNC_SCENE(g_FMMC_STRUCT.sWarpPortals[iPortal].iEntryAnimType)
				SET_INSTANCED_PORTAL_ANIM_CLIENT_STATE(eWarpPortalAnimState_Init_SyncScene)
			ELIF NOT IS_STRING_NULL_OR_EMPTY(GET_ENTRY_ANIM_DICT_FROM_WARP_PORTAL_INT(g_FMMC_STRUCT.sWarpPortals[iPortal].iEntryAnimType))
			AND ARE_STRINGS_EQUAL(GET_ENTRY_ANIM_DICT_FROM_WARP_PORTAL_INT(g_FMMC_STRUCT.sWarpPortals[iPortal].iEntryAnimType), "Vehicle")
				SET_INSTANCED_PORTAL_ANIM_CLIENT_STATE(eWarpPortalAnimState_Init_Vehicle)
			ELIF NOT IS_STRING_NULL_OR_EMPTY(GET_ENTRY_ANIM_DICT_FROM_WARP_PORTAL_INT(g_FMMC_STRUCT.sWarpPortals[iPortal].iEntryAnimType))
				SET_INSTANCED_PORTAL_ANIM_CLIENT_STATE(eWarpPortalAnimState_Init_Anim)
			ELSE
				PRINTLN("[LM][HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED][wPortals] - No Anim has been selected. Returning TRUE straight away.")
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE eWarpPortalAnimState_Init_Anim			
			PROCESS_WARP_PORTAL_SAFETY_TIMER()
						
			REQUEST_ANIM_DICT(sAnimDict)
			IF NOT HAS_ANIM_DICT_LOADED(sAnimDict)
				PRINTLN("[LM][HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED][wPortals] - Waiting for the Anim Dict to be loaded: ", sAnimDict)
				RETURN FALSE
			ENDIF
			
			IF CUTSCENE_HELP_CLONE_PLAYER(sWarpPortal.pedEntryAnimClone, PLAYER_ID(), FALSE, TRUE)
				PRINTLN("[LM][HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED][wPortals] - Clone has been created (1).")				
				SET_ENTITY_VISIBLE(sWarpPortal.pedEntryAnimClone, TRUE)
				SET_ENTITY_ALWAYS_PRERENDER(sWarpPortal.pedEntryAnimClone, TRUE)
			ENDIF
			IF DOES_ENTITY_EXIST(sWarpPortal.pedEntryAnimClone)
			AND NOT IS_PED_INJURED(sWarpPortal.pedEntryAnimClone)
				PRINTLN("[LM][HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED][wPortals] - Clone has been created (2).")
			ELSE
				PRINTLN("[LM][HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED][wPortals] - Waiting for Clone to be created")
				RETURN FALSE
			ENDIF
			
			IF NOT DOES_CAM_EXIST(sWarpPortal.camWarpPortal)
				PRINTLN("[LM][HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED][wPortals] - Creating Camera for Entry Anim at Coord: ", g_FMMC_STRUCT.sWarpPortals[iPortal].vCamAnimCoord, " With Rotation: ", g_FMMC_STRUCT.sWarpPortals[iPortal].vCamAnimRot)
								
				CAMERA_TYPE eCamType
			
				IF NOT IS_BIT_SET(g_FMMC_STRUCT.sWarpPortals[iPortal].iWarpPortalBS, ciWARP_PORTAL_ForceAnimatedCamera)
					eCamType = CAMTYPE_SCRIPTED					
				ELSE
					IF NOT IS_STRING_NULL_OR_EMPTY(sCamAnimName)
						eCamType = CAMTYPE_ANIMATED
					ELSE
						ASSERTLN("[LM][HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED][wPortals] - There is no Camera Animation associated with this kind of Entry/Exit. Falling back to Creator Placed Camera.")	
					ENDIF
				ENDIF
				
				sWarpPortal.camWarpPortal = CREATE_CAMERA_WITH_PARAMS(eCamType, g_FMMC_STRUCT.sWarpPortals[iPortal].vCamAnimCoord, g_FMMC_STRUCT.sWarpPortals[iPortal].vCamAnimRot, g_FMMC_STRUCT.sWarpPortals[iPortal].fCamAnimFOV)
			ELSE
				#IF IS_DEBUG_BUILD
					VECTOR vLoc
					vLoc = GET_ENTITY_COORDS(sWarpPortal.pedEntryAnimClone)
				#ENDIF
				IF NOT IS_ENTITY_AT_COORD(sWarpPortal.pedEntryAnimClone, g_FMMC_STRUCT.sWarpPortals[iPortal].vEntryAnimCoord, (<<0.8, 0.8, 2.0>>), FALSE, TRUE)	
				OR NOT HAS_PED_HEAD_BLEND_FINISHED(sWarpPortal.pedEntryAnimClone)
					PRINTLN("[LM][HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED][wPortals] - Waiting for CLONE ped to be at AnimCoord: ", g_FMMC_STRUCT.sWarpPortals[iPortal].vEntryAnimCoord, " Current Loc: ", vLoc)
					
					SET_ENTITY_COORDS(sWarpPortal.pedEntryAnimClone, g_FMMC_STRUCT.sWarpPortals[iPortal].vEntryAnimCoord, FALSE, FALSE, FALSE, FALSE)
					SET_ENTITY_HEADING(sWarpPortal.pedEntryAnimClone, g_FMMC_STRUCT.sWarpPortals[iPortal].fEntryAnimHead)					
					RETURN FALSE
				ELSE
					PRINTLN("[LM][HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED][wPortals] - CLONE ped is now at the AnimCoord: ", g_FMMC_STRUCT.sWarpPortals[iPortal].vEntryAnimCoord, " Current Loc: ", vLoc)
				ENDIF
				
				PRINTLN("[LM][HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED][wPortals] - Setting camera as active.")
				SET_CAM_ACTIVE(sWarpPortal.camWarpPortal, TRUE)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				
				IF IS_BIT_SET(g_FMMC_STRUCT.sWarpPortals[iPortal].iWarpPortalBS, ciWARP_PORTAL_ForceAnimatedCamera)
					PLAY_CAM_ANIM(sWarpPortal.camWarpPortal, sCamAnimName, sAnimDict, g_FMMC_STRUCT.sWarpPortals[iPortal].vEntryAnimCoord, <<0.0, 0.0, g_FMMC_STRUCT.sWarpPortals[iPortal].fEntryAnimHead>>) // Coord and Heading are Origin of the "scene".
				ENDIF
				
				IF NOT IS_BIT_SET(sWarpPortal.iWarpPortalBitset, ciWarpPortalBS_FadedPlayerOut)
					PRINTLN("[LM][HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED][wPortals] - Calling NETWORK_FADE_OUT_ENTITY on local player.")
					SET_BIT(sWarpPortal.iWarpPortalBitset, ciWarpPortalBS_FadedPlayerOut)
					SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
					SET_ENTITY_ALPHA(PLAYER_PED_ID(), 0, FALSE)
				ENDIF
			ENDIF
			
			IF DOES_CAM_EXIST(sWarpPortal.camWarpPortal)
			AND IS_CAM_ACTIVE(sWarpPortal.camWarpPortal)
			AND IS_CAM_RENDERING(sWarpPortal.camWarpPortal)
				PRINTLN("[LM][HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED][wPortals] - Starting Anim Timer.")
				
				SET_ENTITY_COLLISION(sWarpPortal.pedEntryAnimClone, TRUE)
				FREEZE_ENTITY_POSITION(sWarpPortal.pedEntryAnimClone, FALSE)
				
				ANIM_DATA sAnimDataNone
				ANIM_DATA sAnimDataBlend
				INT iAnimFlags
				sAnimDataBlend.type = APT_SINGLE_ANIM
				sAnimDataBlend.anim0 = sAnimName
				sAnimDataBlend.dictionary0 = sAnimDict
				sAnimDataBlend.phase0 = 0.0
				sAnimDataBlend.rate0 = 0.60
				iAnimFlags = 0
				iAnimFlags += ENUM_TO_INT(AF_DEFAULT)
				iAnimFlags += ENUM_TO_INT(AF_OVERRIDE_PHYSICS)
				sAnimDataBlend.flags = INT_TO_ENUM(ANIMATION_FLAGS, iAnimFlags)	
				TASK_SCRIPTED_ANIMATION(sWarpPortal.pedEntryAnimClone, sAnimDataBlend, sAnimDataNone, sAnimDataNone, 0.15, 0.15)
				
				FORCE_PED_AI_AND_ANIMATION_UPDATE(sWarpPortal.pedEntryAnimClone)
				
				RESET_NET_TIMER(sWarpPortal.tdCamAnimTimer)
				SHAKE_CAM(sWarpPortal.camWarpPortal, "HAND_SHAKE", 0.35)				
				SET_INSTANCED_PORTAL_ANIM_CLIENT_STATE(eWarpPortalAnimState_Run_Anim)			
			ENDIF	
		BREAK
		
		CASE eWarpPortalAnimState_Run_Anim
			IF NOT IS_PED_INJURED(sWarpPortal.pedEntryAnimClone)
				IF GET_ENTITY_ANIM_CURRENT_TIME(sWarpPortal.pedEntryAnimClone, GET_ENTRY_ANIM_DICT_FROM_WARP_PORTAL_INT(g_FMMC_STRUCT.sWarpPortals[iPortal].iEntryAnimType), GET_ENTRY_ANIM_CLIP_FROM_WARP_PORTAL_INT(g_FMMC_STRUCT.sWarpPortals[iPortal].iEntryAnimType)) > 0.05
					IF NOT HAS_NET_TIMER_STARTED(sWarpPortal.tdCamAnimTimer)
						START_NET_TIMER(sWarpPortal.tdCamAnimTimer)
						DO_SCREEN_FADE_IN(300)
						SET_BIT(sWarpPortal.iWarpPortalBitset, ciWarpPortalBS_EntryAnimationStarted)
						PRINTLN("[LM][HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED][wPortals] - Starting the Camera Script Timer")
					ENDIF
				ELSE
					PRINTLN("[LM][HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED][wPortals] - Waiting for Anim to start running.")
				ENDIF
			ELSE
				PRINTLN("[LM][HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED][wPortals] - Ped dead/doesn't exist, emergancy cleanup.")
				SET_INSTANCED_PORTAL_ANIM_CLIENT_STATE(eWarpPortalAnimState_Cleanup)
				RETURN FALSE
			ENDIF
			
			IF IS_BIT_SET(sWarpPortal.iWarpPortalBitset, ciWarpPortalBS_EntryAnimationStarted)
				IF HAS_NET_TIMER_EXPIRED_READ_ONLY(sWarpPortal.tdCamAnimTimer, 5000)
				OR NOT IS_ENTITY_ALIVE(sWarpPortal.pedEntryAnimClone)
				OR GET_ENTITY_ANIM_CURRENT_TIME(sWarpPortal.pedEntryAnimClone, GET_ENTRY_ANIM_DICT_FROM_WARP_PORTAL_INT(g_FMMC_STRUCT.sWarpPortals[iPortal].iEntryAnimType), GET_ENTRY_ANIM_CLIP_FROM_WARP_PORTAL_INT(g_FMMC_STRUCT.sWarpPortals[iPortal].iEntryAnimType)) > 0.4
					IF IS_SCREEN_FADED_IN()
					AND NOT IS_SCREEN_FADING_OUT()
						PRINTLN("[LM][HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED][wPortals] - Anim Timer Expired. Fading out the screen so we can clean up.")
						DO_SCREEN_FADE_OUT(500)
						SET_BIT(sWarpPortal.iWarpPortalBitset, ciWarpPortalBS_EntryAnimationFinished)
					ELSE
						PRINTLN("[LM][HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED][wPortals] - Waiting for screen fadeout.")
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(sWarpPortal.iWarpPortalBitset, ciWarpPortalBS_EntryAnimationFinished)
				AND IS_SCREEN_FADED_OUT()
					PRINTLN("[LM][HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED][wPortals] - Anim Timer Expired and Fadeout done. Sending to Cleanup.")
					SET_INSTANCED_PORTAL_ANIM_CLIENT_STATE(eWarpPortalAnimState_Cleanup)
				ENDIF			
			ELSE
				PRINTLN("[LM][HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED][wPortals] - Waiting for Camera Script Timer to start.")
			ENDIF
		BREAK
		
		CASE eWarpPortalAnimState_Init_Vehicle
			PROCESS_WARP_PORTAL_SAFETY_TIMER()
			
			IF NOT DOES_ENTITY_EXIST(sWarpPortal.vehEntryAnimOriginal)
				sWarpPortal.vehEntryAnimOriginal = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			ENDIF
			
			// Clone Vehicle
			IF NOT IS_BIT_SET(sWarpPortal.iWarpPortalBitset, ciWarpPortalBS_EntryAnimationVehCreated)
				IF PROCESS_WARP_PORTAL_CUTSCENE_HELP_CLONE_VEH_AND_PASSENGERS(iPortal, sWarpPortal.vehEntryAnimClone, sWarpPortal.vehEntryAnimOriginal, sWarpPortal.vehEntryAnimCloneTrailer)
					FREEZE_ENTITY_POSITION(sWarpPortal.vehEntryAnimClone, TRUE)
					IF DOES_ENTITY_EXIST(sWarpPortal.vehEntryAnimCloneTrailer)
					AND NOT IS_ENTITY_DEAD(sWarpPortal.vehEntryAnimCloneTrailer)
						ATTACH_VEHICLE_TO_TRAILER(sWarpPortal.vehEntryAnimClone, sWarpPortal.vehEntryAnimCloneTrailer)
					ENDIF
					
					SET_ENTITY_ALPHA(sWarpPortal.vehEntryAnimOriginal, 0, false)
					IF NETWORK_HAS_CONTROL_OF_ENTITY(sWarpPortal.vehEntryAnimOriginal)
						FREEZE_ENTITY_POSITION(sWarpPortal.vehEntryAnimOriginal, TRUE)
						SET_ENTITY_COLLISION(sWarpPortal.vehEntryAnimOriginal, FALSE)
						SET_ENTITY_VISIBLE(sWarpPortal.vehEntryAnimOriginal, FALSE)
					ENDIF
					
					SET_VEHICLE_ON_GROUND_PROPERLY(sWarpPortal.vehEntryAnimClone)					
					
					#IF IS_DEBUG_BUILD 
					MODEL_NAMES vehModel
					VECTOR vCoordToCreate
					FLOAT fHeadToCreate	
					vCoordToCreate = g_FMMC_STRUCT.sWarpPortals[iPortal].vEntryAnimCoord
					fHeadToCreate = g_FMMC_STRUCT.sWarpPortals[iPortal].fEntryAnimHead
					vehModel = GET_ENTITY_MODEL(sWarpPortal.vehEntryAnimOriginal) #ENDIF
					PRINTLN("[LM][HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED][wPortals] - Creating Clone Vehicle at: ", vCoordToCreate, " Heading: ", fHeadToCreate, " Model: ", GET_MODEL_NAME_FOR_DEBUG(vehModel))
					
					SET_BIT(sWarpPortal.iWarpPortalBitset, ciWarpPortalBS_EntryAnimationVehCreated)
				ENDIF
				RETURN FALSE
			ENDIF
						
			IF NOT DOES_CAM_EXIST(sWarpPortal.camWarpPortal)
				PRINTLN("[LM][HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED][wPortals] - Creating Camera for Entry Anim at Coord: ", g_FMMC_STRUCT.sWarpPortals[iPortal].vCamAnimCoord, " With Rotation: ", g_FMMC_STRUCT.sWarpPortals[iPortal].vCamAnimRot)
				sWarpPortal.camWarpPortal = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, g_FMMC_STRUCT.sWarpPortals[iPortal].vCamAnimCoord, g_FMMC_STRUCT.sWarpPortals[iPortal].vCamAnimRot, g_FMMC_STRUCT.sWarpPortals[iPortal].fCamAnimFOV)
			ELSE
				PRINTLN("[LM][HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED][wPortals] - Setting camera as active.")
				SET_CAM_ACTIVE(sWarpPortal.camWarpPortal, TRUE)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				
				IF NOT IS_BIT_SET(sWarpPortal.iWarpPortalBitset, ciWarpPortalBS_FadedPlayerOut)
					PRINTLN("[LM][HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED][wPortals] - Calling NETWORK_FADE_OUT_ENTITY on local player.")
					SET_BIT(sWarpPortal.iWarpPortalBitset, ciWarpPortalBS_FadedPlayerOut)
					SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
					SET_ENTITY_ALPHA(PLAYER_PED_ID(), 0, FALSE)
				ENDIF
			ENDIF
			
			IF DOES_CAM_EXIST(sWarpPortal.camWarpPortal)
			AND IS_CAM_ACTIVE(sWarpPortal.camWarpPortal)
			AND IS_CAM_RENDERING(sWarpPortal.camWarpPortal)
			AND IS_ENTITY_ALIVE(sWarpPortal.vehEntryAnimClone)
			AND IS_VEHICLE_ON_ALL_WHEELS(sWarpPortal.vehEntryAnimClone)
				PRINTLN("[LM][HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED][wPortals] - Starting Anim Timer.")
				
				SET_ENTITY_COLLISION(sWarpPortal.vehEntryAnimClone, FALSE)
				FREEZE_ENTITY_POSITION(sWarpPortal.vehEntryAnimClone, TRUE)
				RESET_ENTITY_ALPHA(sWarpPortal.vehEntryAnimClone)
				SET_ENTITY_VISIBLE(sWarpPortal.vehEntryAnimClone, TRUE)
				
				PRINTLN("[LM][HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED][wPortals] - Starting Anim Timer.")
				
				RESET_NET_TIMER(sWarpPortal.tdCamAnimTimer)
				SHAKE_CAM(sWarpPortal.camWarpPortal, "HAND_SHAKE", 0.35)
				SET_INSTANCED_PORTAL_ANIM_CLIENT_STATE(eWarpPortalAnimState_Run_Vehicle)
			ENDIF
		BREAK
		
		CASE eWarpPortalAnimState_Run_Vehicle
			IF DOES_ENTITY_EXIST(sWarpPortal.vehEntryAnimClone)
				IF NOT HAS_NET_TIMER_STARTED(sWarpPortal.tdCamAnimTimer)
					START_NET_TIMER(sWarpPortal.tdCamAnimTimer)
					DO_SCREEN_FADE_IN(300)
					SET_BIT(sWarpPortal.iWarpPortalBitset, ciWarpPortalBS_EntryAnimationStarted)
					PRINTLN("[LM][HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED][wPortals] - Starting the Camera Script Timer")
				ENDIF
			ELSE
				PRINTLN("[LM][HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED][wPortals] - Veh dead/doesn't exist, emergancy cleanup.")
				SET_INSTANCED_PORTAL_ANIM_CLIENT_STATE(eWarpPortalAnimState_Cleanup)
				RETURN FALSE
			ENDIF
			
			IF IS_BIT_SET(sWarpPortal.iWarpPortalBitset, ciWarpPortalBS_EntryAnimationStarted)
				IF HAS_NET_TIMER_EXPIRED_READ_ONLY(sWarpPortal.tdCamAnimTimer, 8000)
				OR (PROCESS_MOVE_WARP_PORTAL_VEHICLE_TOWARD_DESTINATION(iPortal) AND HAS_NET_TIMER_EXPIRED_READ_ONLY(sWarpPortal.tdCamAnimTimer, 3000))
					IF IS_SCREEN_FADED_IN()
					AND NOT IS_SCREEN_FADING_OUT()
						PRINTLN("[LM][HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED][wPortals] - Anim Timer Expired. Fading out the screen so we can clean up.")
						DO_SCREEN_FADE_OUT(500)
						SET_BIT(sWarpPortal.iWarpPortalBitset, ciWarpPortalBS_EntryAnimationFinished)
					ELSE
						PRINTLN("[LM][HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED][wPortals] - Waiting for screen fadeout.")
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(sWarpPortal.iWarpPortalBitset, ciWarpPortalBS_EntryAnimationFinished)
				AND IS_SCREEN_FADED_OUT()
					PRINTLN("[LM][HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED][wPortals] - Anim Timer Expired and Fadeout done. Sending to Cleanup.")
					SET_INSTANCED_PORTAL_ANIM_CLIENT_STATE(eWarpPortalAnimState_Cleanup)
				ENDIF			
			ELSE
				PRINTLN("[LM][HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED][wPortals] - Waiting for Camera Script Timer to start.")
			ENDIF
		BREAK
		
		CASE eWarpPortalAnimState_Init_SyncScene
			PROCESS_WARP_PORTAL_SAFETY_TIMER()
						
			REQUEST_ANIM_DICT(sAnimDict)
			IF NOT HAS_ANIM_DICT_LOADED(sAnimDict)
				PRINTLN("[LM][HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED][wPortals] - Waiting for the Anim Dict to be loaded: ", sAnimDict)
				RETURN FALSE
			ENDIF
			
			sWarpPortal.pedEntryAnimClone = PLAYER_PED_ID()
			
			IF DOES_ENTITY_EXIST(sWarpPortal.pedEntryAnimClone)
			AND NOT IS_PED_INJURED(sWarpPortal.pedEntryAnimClone)
				PRINTLN("[LM][HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED][wPortals] - Player Ped is ok.")
			ELSE
				PRINTLN("[LM][HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED][wPortals] - Waiting for Player Ped.")
				RETURN FALSE
			ENDIF
			
			IF NOT HAS_PED_HEAD_BLEND_FINISHED(sWarpPortal.pedEntryAnimClone)
				PRINTLN("[LM][HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED][wPortals] - Waiting for Ped Head Blend to finish.")
				RETURN FALSE	
			ENDIF
			
			IF NOT IS_BIT_SET(sWarpPortal.iWarpPortalBitset, ciWarpPortalBS_FadedPlayerOut)
				PRINTLN("[LM][HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED][wPortals] - Calling NETWORK_FADE_OUT_ENTITY on local player.")
				SET_BIT(sWarpPortal.iWarpPortalBitset, ciWarpPortalBS_FadedPlayerOut)
				//SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
				//SET_ENTITY_ALPHA(PLAYER_PED_ID(), 0, FALSE)
			ENDIF
			
			SET_PLAYER_VISIBLE_LOCALLY(PLAYER_ID())
			
			VECTOR vCoords
			VECTOR vRot
			vCoords = g_FMMC_STRUCT.sWarpPortals[iPortal].vEntryAnimCoord
			vRot = <<0.0, 0.0, g_FMMC_STRUCT.sWarpPortals[iPortal].fEntryAnimHead>>
				
			IK_CONTROL_FLAGS ikFlags
			ikFlags = AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK | AIK_DISABLE_HEAD_IK | AIK_DISABLE_TORSO_IK	| AIK_DISABLE_TORSO_REACT_IK
			
			SYNCED_SCENE_PLAYBACK_FLAGS sceneFlags
			sceneFlags = SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_PRESERVE_VELOCITY|SYNCED_SCENE_DONT_INTERRUPT

			PRINTLN("[LM][HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED][wPortals] - Starting Sync Scene with vCoord: ", vCoords, " vRot:, ", vRot, " sAnimDict: ", sAnimDict, " sAnimName: ", sAnimName, "sCamAnimName: ", sCamAnimName)
			
			FORCE_PED_AI_AND_ANIMATION_UPDATE(sWarpPortal.pedEntryAnimClone)
			
			sWarpPortal.iSyncScene = NETWORK_CREATE_SYNCHRONISED_SCENE(vCoords, vRot, EULER_XYZ, FALSE, FALSE)
			NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(sWarpPortal.pedEntryAnimClone, sWarpPortal.iSyncScene, sAnimDict, sAnimName, SLOW_BLEND_IN, INSTANT_BLEND_OUT, sceneFlags, DEFAULT, DEFAULT, ikFlags)
						
			NETWORK_ADD_SYNCHRONISED_SCENE_CAMERA(sWarpPortal.iSyncScene, sAnimDict, sCamAnimName)
			NETWORK_FORCE_LOCAL_USE_OF_SYNCED_SCENE_CAMERA(sWarpPortal.iSyncScene)
			
			NETWORK_START_SYNCHRONISED_SCENE(sWarpPortal.iSyncScene)	
			
			//PLAY_CAM_ANIM(sHandHeldDrillPassedIn.camDrillCam, tl63_AnimCam, sAnimDict, vCoords, vRot)
			
			DO_SCREEN_FADE_IN(250)
			TOGGLE_PAUSED_RENDERPHASES(TRUE)
			
			SET_INSTANCED_PORTAL_ANIM_CLIENT_STATE(eWarpPortalAnimState_Run_SyncScene)			
		BREAK
		
		CASE eWarpPortalAnimState_Run_SyncScene
			SET_PLAYER_VISIBLE_LOCALLY(PLAYER_ID())
			
			IF sWarpPortal.iSyncScene != -1				
				iWarpPortalThisLocalSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sWarpPortal.iSyncScene)	
			ELSE
				PRINTLN("[LM][HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED][wPortals] - Sync Scene Invalid. Going straight to cleanup.")
				SET_INSTANCED_PORTAL_ANIM_CLIENT_STATE(eWarpPortalAnimState_Cleanup)
			ENDIF
			
			IF NOT IS_BIT_SET(sWarpPortal.iWarpPortalBitset, ciWarpPortalBS_EntryAnimationStarted)
			AND IS_SYNCHRONIZED_SCENE_RUNNING(iWarpPortalThisLocalSceneID)
				PRINTLN("[LM][HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED][wPortals] - Running Sync Scene...")
				SET_BIT(sWarpPortal.iWarpPortalBitset, ciWarpPortalBS_EntryAnimationStarted)
				DO_SCREEN_FADE_IN(300)
				
			ELIF IS_BIT_SET(sWarpPortal.iWarpPortalBitset, ciWarpPortalBS_EntryAnimationStarted)
				IF GET_SYNCHRONIZED_SCENE_PHASE(iWarpPortalThisLocalSceneID) >= 0.56
				//OR HAS_ANIM_EVENT_FIRED(sWarpPortal.pedEntryAnimClone, GET_HASH_KEY("1 player"))
					IF IS_SCREEN_FADED_IN()
					AND NOT IS_SCREEN_FADING_OUT()
						PRINTLN("[LM][HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED][wPortals] - Sync Scene Timer Expired or hit Breakout Event. Fading out the screen so we can clean up.")
						DO_SCREEN_FADE_OUT(500)
						SET_BIT(sWarpPortal.iWarpPortalBitset, ciWarpPortalBS_EntryAnimationFinished)
					ELSE
						PRINTLN("[LM][HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED][wPortals] - Waiting for screen fadeout.")
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(sWarpPortal.iWarpPortalBitset, ciWarpPortalBS_EntryAnimationFinished)
				AND IS_SCREEN_FADED_OUT()
					PRINTLN("[LM][HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED][wPortals] - Finished Sync Scene!!!")
					SET_INSTANCED_PORTAL_ANIM_CLIENT_STATE(eWarpPortalAnimState_Cleanup)
				ENDIF
			ENDIF
		BREAK
		
		CASE eWarpPortalAnimState_Cleanup
			PRINTLN("[LM][HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED][wPortals] - Cleaning Up.")
			INT i 
			
			IF sWarpPortal.iSyncScene != -1
				iWarpPortalThisLocalSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sWarpPortal.iSyncScene)		
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iWarpPortalThisLocalSceneID)
					NETWORK_STOP_SYNCHRONISED_SCENE(sWarpPortal.iSyncScene)
					PRINTLN("[LM][HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED][wPortals] - Stopping Sync Scene.")
				ENDIF
			ENDIF
			sWarpPortal.iSyncScene = -1
			
			DESTROY_CAM(sWarpPortal.camWarpPortal)

			IF sWarpPortal.pedEntryAnimClone != PLAYER_PED_ID()
				DELETE_PED(sWarpPortal.pedEntryAnimClone)
			ENDIF
			
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
			RESET_ENTITY_ALPHA(PLAYER_PED_ID())
			SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
			FOR i = 0 TO ci_WARP_PORTAL_PED_CLONES_MAX-1
				IF DOES_ENTITY_EXIST(sWarpPortal.pedEntryAnimClones[i])
					DELETE_PED(sWarpPortal.pedEntryAnimClones[i])
				ENDIF
			ENDFOR		
			IF DOES_ENTITY_EXIST(sWarpPortal.vehEntryAnimClone)
				DELETE_VEHICLE(sWarpPortal.vehEntryAnimClone)
			ENDIF
			IF DOES_ENTITY_EXIST(sWarpPortal.vehEntryAnimOriginal)
				SET_ENTITY_VISIBLE(sWarpPortal.vehEntryAnimOriginal, TRUE)
				RESET_ENTITY_ALPHA(sWarpPortal.vehEntryAnimOriginal)
			ENDIF			
			IF NETWORK_HAS_CONTROL_OF_ENTITY(sWarpPortal.vehEntryAnimOriginal)
				FREEZE_ENTITY_POSITION(sWarpPortal.vehEntryAnimOriginal, FALSE)
				SET_ENTITY_COLLISION(sWarpPortal.vehEntryAnimOriginal, TRUE)
				SET_ENTITY_VISIBLE(sWarpPortal.vehEntryAnimOriginal, TRUE)
			ENDIF
			RESET_NET_TIMER(sWarpPortal.tdCamAnimTimerSafety)
			RESET_NET_TIMER(sWarpPortal.tdCamAnimTimer)
			CLEAR_BIT(sWarpPortal.iWarpPortalBitset, ciWarpPortalBS_FadedPlayerOut)
			CLEAR_BIT(sWarpPortal.iWarpPortalBitset, ciWarpPortalBS_EntryAnimationStarted)
			CLEAR_BIT(sWarpPortal.iWarpPortalBitset, ciWarpPortalBS_EntryAnimationFinished)
			CLEAR_BIT(sWarpPortal.iWarpPortalBitset, ciWarpPortalBS_EntryAnimationVehCreated)
			SET_INSTANCED_PORTAL_ANIM_CLIENT_STATE(eWarpPortalAnimState_Init)
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_SPECIAL_DRUG_INTERIOR_SPAWN_POINT_AFTER_WARP(INT iPortal)

	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_WEED_FARM)
	AND NOT IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_COCAINE_FACTORY)
	AND NOT IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_METH_FACTORY)
		EXIT
	ENDIF
	
	INTERIOR_INSTANCE_INDEX iiiNewInterior = GET_INTERIOR_AT_COORDS(sWarpPortal.vTargetLocation)
	
	IF (IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_WEED_FARM)
	AND GET_INTERIOR_AT_COORDS(<<1049.6, -3196.6, -38.5>>) = iiiNewInterior)
	OR (IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_COCAINE_FACTORY)
	AND GET_INTERIOR_AT_COORDS(<<1093.6, -3196.6, -38.5>>) = iiiNewInterior)
	OR (IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_METH_FACTORY)
	AND GET_INTERIOR_AT_COORDS(<<1009.5, -3196.6, -38.5>>) = iiiNewInterior)
		SETUP_SPECIFIC_SPAWN_LOCATION(g_FMMC_STRUCT.sWarpPortals[iPortal].vStartCoord, g_FMMC_STRUCT.sWarpPortals[iPortal].fStartHead, 5, FALSE, 0, TRUE, FALSE, 0)
		SET_BIT(iLocalBoolCheck33, LBOOL33_RESPAWN_SET_FROM_INTERIOR_WARP)
		PRINTLN("[LM][PROCESS_INSTANCED_CONTENT_PORTALS][wPortals] - PROCESS_SPECIAL_DRUG_INTERIOR_SPAWN_POINT_AFTER_WARP - In non-respawnable interior - overriding spawn location")
	ELIF IS_BIT_SET(iLocalBoolCheck33, LBOOL33_RESPAWN_SET_FROM_INTERIOR_WARP)
		PRINTLN("[LM][PROCESS_INSTANCED_CONTENT_PORTALS][wPortals] - PROCESS_SPECIAL_DRUG_INTERIOR_SPAWN_POINT_AFTER_WARP - Clearing spawn location")
		CLEAR_BIT(iLocalBoolCheck33, LBOOL33_RESPAWN_SET_FROM_INTERIOR_WARP)
		CLEAR_SPECIFIC_SPAWN_LOCATION()
	ENDIF
			
ENDPROC

FUNC BOOL WARP_PLAYER_USING_INSTANCED_CONTENT_PORTAL(INT iPortal)
	
	SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
	
	IF IS_VECTOR_ZERO(sWarpPortal.vTargetLocation)
	
		VECTOR vTargetLocation, vTargetLocationOriginal
		FLOAT fTargetHead, fHeadOriginal
		vTargetLocation = MC_GET_WARP_PORTAL_END_POSITION(iPortal)
		fTargetHead = MC_GET_WARP_PORTAL_END_HEADING(iPortal)
		sWarpPortal.bKeepVehicle = IS_BIT_SET(g_FMMC_STRUCT.sWarpPortals[iPortal].iWarpPortalBS, ciWARP_PORTAL_KeepVehicle)
		
		BOOL bSuccess
		INT i		
		VECTOR vTargetLocationMin = vTargetLocation
		VECTOR vTargetLocationMax = vTargetLocation
		vTargetLocationMin.x -= 0.15
		vTargetLocationMax.x += 0.15	
		vTargetLocationMin.y -= 0.15
		vTargetLocationMax.y += 0.15	
		vTargetLocationMax.z += 1.0
		
		vTargetLocationOriginal = vTargetLocation
		fHeadOriginal = fTargetHead
		
		IF IS_AREA_OCCUPIED(vTargetLocationMin, vTargetLocationMax, FALSE, TRUE, TRUE, FALSE, FALSE)
			FOR i = 0 TO FMMC_MAX_WARP_PORTALS_EXTRA_POSITIONS-1			
				IF NOT IS_VECTOR_ZERO(MC_GET_WARP_PORTAL_END_FALLBACK_POSITION(iPortal, i))
					vTargetLocation = MC_GET_WARP_PORTAL_END_FALLBACK_POSITION(iPortal, i)
					fTargetHead = MC_GET_WARP_PORTAL_END_FALLBACK_HEADING(iPortal, i)
				ENDIF
				
				vTargetLocationMin = vTargetLocation
				vTargetLocationMax = vTargetLocation
				vTargetLocationMin.x -= 0.15
				vTargetLocationMax.x += 0.15
				vTargetLocationMin.y -= 0.15
				vTargetLocationMax.y += 0.15
				vTargetLocationMax.z += 1.0
				
				IF NOT IS_AREA_OCCUPIED(vTargetLocationMin, vTargetLocationMax, FALSE, TRUE, TRUE, FALSE, FALSE)				
					bSuccess = TRUE
					BREAKLOOP
				ENDIF
			ENDFOR
		ELSE
			bSuccess = TRUE
		ENDIF
		
		IF NOT bSuccess
			sWarpPortal.vTargetLocation = vTargetLocationOriginal
			sWarpPortal.fTargetHead = fHeadOriginal
			PRINTLN("[LM][PROCESS_INSTANCED_CONTENT_PORTALS][wPortals] - Warping Stage - Had to use a the Main Position as all fallbacks are taken - vTargetLocation: ", vTargetLocation)
		ELSE
			sWarpPortal.vTargetLocation = vTargetLocation
			sWarpPortal.fTargetHead = fTargetHead
			PRINTLN("[LM][PROCESS_INSTANCED_CONTENT_PORTALS][wPortals] - Warping Stage - vTargetLocation: ", vTargetLocation)
		ENDIF
	ENDIF
	
	IF NOT IS_VECTOR_ZERO(sWarpPortal.vTargetLocation)
		IF NET_WARP_TO_COORD(sWarpPortal.vTargetLocation, sWarpPortal.fTargetHead, sWarpPortal.bKeepVehicle, FALSE, FALSE, FALSE, TRUE, FALSE, TRUE)
			PRINTLN("[LM][PROCESS_INSTANCED_CONTENT_PORTALS][wPortals] - Warping Stage - Warp Successful.")
			
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.sWarpPortals[iPortal].iWarpPortalBS, ciWARP_PORTAL_RequiresTriggerLegacy)
				SET_BIT(sWarpPortal.iJustUsedPortalStartBS, iPortal)
				PRINTLN("[LM][PROCESS_INSTANCED_CONTENT_PORTALS][wPortals] - Warping Stage - Setting that we just used START Portal: " , iPortal)
			ENDIF
						
			PROCESS_SPECIAL_DRUG_INTERIOR_SPAWN_POINT_AFTER_WARP(iPortal)
			
			sWarpPortal.vTargetLocation = <<0.0, 0.0, 0.0>>
			sWarpPortal.fTargetHead = 0.0
			sWarpPortal.bKeepVehicle = FALSE
			
			RETURN TRUE
		ELSE
			PRINTLN("[LM][PROCESS_INSTANCED_CONTENT_PORTALS][wPortals] - Warping Stage - Waiting for Warp to complete...")
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_PRELOAD_WARP_PORTAL_FINISHED(INT iPortal)
	BOOL bFinishedFade, bFinishedPostFx

	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sWarpPortals[iPortal].iWarpPortalBS, ciWARP_PORTAL_PostFXBeforeFade) 
		bFinishedPostFx = TRUE
		PRINTLN("[LM][HAS_PRELOAD_WARP_PORTAL_FINISHED][wPortals] - Preload Stage - No Flash...")
	ENDIF
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.sWarpPortals[iPortal].iWarpPortalBS, ciWARP_PORTAL_FadeOutBeforeWarp)
		bFinishedFade = TRUE
		PRINTLN("[LM][HAS_PRELOAD_WARP_PORTAL_FINISHED][wPortals] - Preload Stage - No Fade...")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sWarpPortals[iPortal].iWarpPortalBS, ciWARP_PORTAL_PostFXBeforeFade)
		IF NOT ANIMPOSTFX_IS_RUNNING("InchPurple")
		AND NOT HAS_NET_TIMER_STARTED(sWarpPortal.tdWarpFlashTimer)
			ANIMPOSTFX_PLAY("InchPurple", 0, TRUE)
			REINIT_NET_TIMER(sWarpPortal.tdWarpFlashTimer)
			PRINTLN("[LM][HAS_PRELOAD_WARP_PORTAL_FINISHED][wPortals] - Preload Stage - Start Flash...")
		ENDIF
		
		IF HAS_NET_TIMER_STARTED(sWarpPortal.tdWarpFlashTimer)
		AND HAS_NET_TIMER_EXPIRED_READ_ONLY(sWarpPortal.tdWarpFlashTimer, 2000)
			PRINTLN("[LM][HAS_PRELOAD_WARP_PORTAL_FINISHED][wPortals] - Preload Stage - Flash Finished...")
			bFinishedPostFx = TRUE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sWarpPortals[iPortal].iWarpPortalBS, ciWARP_PORTAL_FadeOutBeforeWarp)
	AND (NOT IS_BIT_SET(g_FMMC_STRUCT.sWarpPortals[iPortal].iWarpPortalBS, ciWARP_PORTAL_PostFXBeforeFade) OR bFinishedPostFx)
		IF IS_SCREEN_FADED_IN()
			DO_SCREEN_FADE_OUT(1000)
			PRINTLN("[LM][HAS_PRELOAD_WARP_PORTAL_FINISHED][wPortals] - Preload Stage - Start Fade...")
		ENDIF
		
		IF IS_SCREEN_FADED_OUT()
		AND NOT IS_SCREEN_FADING_OUT()
			bFinishedFade = TRUE
			PRINTLN("[LM][HAS_PRELOAD_WARP_PORTAL_FINISHED][wPortals] - Preload Stage - Fade Finished...")
		ENDIF
	ENDIF
	
	IF NOT IS_STRING_NULL_OR_EMPTY(GET_ENTRY_ANIM_DICT_FROM_WARP_PORTAL_INT(g_FMMC_STRUCT.sWarpPortals[iPortal].iEntryAnimType))
	AND ARE_STRINGS_EQUAL(GET_ENTRY_ANIM_DICT_FROM_WARP_PORTAL_INT(g_FMMC_STRUCT.sWarpPortals[iPortal].iEntryAnimType), "Vehicle")
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			PRINTLN("[LM][HAS_PRELOAD_WARP_PORTAL_FINISHED][wPortals] - Preload Stage - Using a Vehicle in Anim - Make sure it's safe.")
			VEHICLE_INDEX tempVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			
			IF IS_VEHICLE_DRIVEABLE(tempVeh)
				INT iPart = 0
				FOR iPart = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
					PARTICIPANT_INDEX piPart = INT_TO_PARTICIPANTINDEX(iPart)
					IF NETWORK_IS_PARTICIPANT_ACTIVE(piPart)
						PLAYER_INDEX piPlayer = NETWORK_GET_PLAYER_INDEX(piPart)
						PED_INDEX pedPlayer = GET_PLAYER_PED(piPlayer)
						
						IF NOT IS_PED_INJURED(pedPlayer)
							IF IS_PED_IN_ANY_VEHICLE(pedPlayer, TRUE)
								IF GET_VEHICLE_PED_IS_ENTERING(pedPlayer) = tempVeh								
									PRINTLN("[LM][HAS_PRELOAD_WARP_PORTAL_FINISHED][wPortals] - Preload Stage - we are waiting for all players to have finished entering the vehicle - Returning False.")
									RETURN FALSE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDFOR
			ENDIF
		ENDIF
	ENDIF
	
	IF (bFinishedFade AND bFinishedPostFx)
		PRINTLN("[LM][HAS_PRELOAD_WARP_PORTAL_FINISHED][wPortals] - Preload Stage - Preload Finished Returning True...")
		IF ANIMPOSTFX_IS_RUNNING("InchPurple")
			ANIMPOSTFX_STOP("InchPurple")
		ENDIF
		RESET_NET_TIMER(sWarpPortal.tdWarpFlashTimer)
		RETURN TRUE
	ENDIF
	
	PRINTLN("[LM][HAS_PRELOAD_WARP_PORTAL_FINISHED][wPortals] - Preload Stage - Preloading - Returning False...")
	
	RETURN FALSE	
ENDFUNC	

FUNC BOOL IS_PLAYER_IN_RANGE_OF_PORTAL(INT iPortal)
	PED_INDEX pedPlayer = PLAYER_PED_ID()
	IF IS_PED_INJURED(pedPlayer)
		RETURN FALSE
	ENDIF
	
	VECTOR vPlayerPos = GET_ENTITY_COORDS(pedPlayer)
	IF GET_DISTANCE_BETWEEN_COORDS(vPlayerPos, MC_GET_WARP_PORTAL_START_POSITION(iPortal)) < GET_WARP_PORTAL_START_RANGE(iPortal)
		
		IF IS_BIT_SET(g_FMMC_STRUCT.sWarpPortals[iPortal].iWarpPortalBS, ciWARP_PORTAL_WaitForDialogueToFinish)
		AND IS_CONVERSATION_STATUS_FREE()
		AND NOT IS_SCRIPTED_CONVERSATION_ONGOING()
			PRINTLN("[LM][IS_PLAYER_IN_RANGE_OF_PORTAL][wPortals] - (start) iPortal: ", iPortal, " We are still listening to dialogue. Returning False.")
			RETURN FALSE
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.sWarpPortals[iPortal].iWarpPortalBS, ciWARP_PORTAL_DisableOnFootTrigger)
		AND NOT IS_PED_IN_ANY_VEHICLE(pedPlayer, TRUE)
			PRINTLN("[LM][IS_PLAYER_IN_RANGE_OF_PORTAL][wPortals] - (start) iPortal: ", iPortal, " We are not inside a Vehicle, and On Foot triggering is disabled. Returning False.")
			RETURN FALSE
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.sWarpPortals[iPortal].iWarpPortalBS, ciWARP_PORTAL_DisableInVehicleTrigger)
		AND IS_PED_IN_ANY_VEHICLE(pedPlayer, TRUE)
			PRINTLN("[LM][IS_PLAYER_IN_RANGE_OF_PORTAL][wPortals] - (start) iPortal: ", iPortal, " We are inside a Vehicle, and In Vehicle triggering is disabled. Returning False.")
			RETURN FALSE
		ENDIF
		
		IF IS_BIT_SET(sWarpPortal.iJustUsedPortalEndBS, iPortal)
			PRINTLN("[LM][IS_PLAYER_IN_RANGE_OF_PORTAL][wPortals] - (start) iPortal: ", iPortal, " We have just used this portal. Returning False.")
			RETURN FALSE	
		ELSE
			PRINTLN("[LM][IS_PLAYER_IN_RANGE_OF_PORTAL][wPortals] - (start) iPortal: ", iPortal, " In Range. Returning True.")			
			RETURN TRUE
		ENDIF		
	ELSE
		CLEAR_BIT(sWarpPortal.iJustUsedPortalEndBS, iPortal)
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PORTAL_ACTIVE_AND_AVAILABLE(INT iPortal, INT iTeam, INT iRule, BOOL bObjectiveBlocked #IF IS_DEBUG_BUILD, BOOL bPrints #ENDIF)
	
	IF bObjectiveBlocked
	AND IS_BIT_SET(g_FMMC_STRUCT.sWarpPortals[iPortal].iWarpPortalBS, ciWARP_PORTAL_DeactivateIfObjectiveBlocked)
		#IF IS_DEBUG_BUILD IF bPrints PRINTLN("[LM][IS_PORTAL_ACTIVE_AND_AVAILABLE][wPortals] - iPortal: ", iPortal, " Objective Blocked. Returning False.") ENDIF #ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_VECTOR_ZERO(MC_GET_WARP_PORTAL_START_POSITION(iPortal))
	OR IS_VECTOR_ZERO(MC_GET_WARP_PORTAL_END_POSITION(iPortal))
		#IF IS_DEBUG_BUILD IF bPrints PRINTLN("[LM][IS_PORTAL_ACTIVE_AND_AVAILABLE][wPortals] - iPortal: ", iPortal, " Null Cordinates Returning False.") ENDIF #ENDIF
		RETURN FALSE
	ENDIF
	
	IF g_FMMC_STRUCT.sWarpPortals[iPortal].iActiveOnRule[iTeam] = -1
	AND g_FMMC_STRUCT.sWarpPortals[iPortal].iEndOnRule[iTeam] = -1
		#IF IS_DEBUG_BUILD IF bPrints PRINTLN("[LM][IS_PORTAL_ACTIVE_AND_AVAILABLE][wPortals] - iPortal: ", iPortal, " Any Rule, Returning True.") ENDIF #ENDIF
		RETURN TRUE
	ENDIF
	
	IF (iRule >= g_FMMC_STRUCT.sWarpPortals[iPortal].iActiveOnRule[iTeam] OR g_FMMC_STRUCT.sWarpPortals[iPortal].iActiveOnRule[iTeam] = -1)
	AND (iRule < g_FMMC_STRUCT.sWarpPortals[iPortal].iEndOnRule[iTeam] OR g_FMMC_STRUCT.sWarpPortals[iPortal].iEndOnRule[iTeam] = -1)		
		#IF IS_DEBUG_BUILD IF bPrints PRINTLN("[LM][IS_PORTAL_ACTIVE_AND_AVAILABLE][wPortals] - iPortal: ", iPortal, " Within rule Params.") ENDIF #ENDIF
		RETURN TRUE
	ENDIF
	
	#IF IS_DEBUG_BUILD IF bPrints PRINTLN("[LM][IS_PORTAL_ACTIVE_AND_AVAILABLE][wPortals] - iPortal: ", iPortal, " Not Within rule Params. iRule: ", iRule, " iActiveRule: ", g_FMMC_STRUCT.sWarpPortals[iPortal].iActiveOnRule[iTeam], " iEndRule: ", g_FMMC_STRUCT.sWarpPortals[iPortal].iEndOnRule[iTeam]) ENDIF #ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SET_INSTANCED_PORTAL_CLIENT_STATE(WARP_PORTAL_STATE eWarpPortalStateNew)
	PRINTLN("[LM][SET_INSTANCED_PORTAL_CLIENT_STATE][wPortals] - Setting Instanced Portal State New: ", ENUM_TO_INT(eWarpPortalStateNew), " From Old: ", ENUM_TO_INT(sWarpPortal.eWarpPortalState))
	sWarpPortal.eWarpPortalState = eWarpPortalStateNew	
ENDPROC

FUNC BOOL HAS_PLAYER_TRIED_TO_ACTIVATE_WARP_PORTAL()
	
	IF IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_CONTEXT)
		RESET_NET_TIMER(sWarpPortal.tdWarpPortalActivation)
		START_NET_TIMER(sWarpPortal.tdWarpPortalActivation)
		RETURN TRUE
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(sWarpPortal.tdWarpPortalActivation)
		IF NOT HAS_NET_TIMER_EXPIRED_READ_ONLY(sWarpPortal.tdWarpPortalActivation, 300)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CAN_INSTANCED_CONTENT_PORTALS_TRIGGER_BE_USED()
	
	IF IS_HUD_COMPONENT_ACTIVE(NEW_HUD_WEAPON_WHEEL)
	OR IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SELECT_WEAPON)
		RETURN FALSE
	ENDIF	
		
	IF IS_PAUSE_MENU_ACTIVE()
		RETURN FALSE
	ENDIF
	
	IF IS_PI_MENU_OPEN()
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC PROCESS_INSTANCED_CONTENT_PORTALS(BOOL bObjectiveBlocked = FALSE)
	
	IF sWarpPortal.iMaxNumberOfPortals = 0
		EXIT
	ENDIF
	
	IF g_bMissionEnding
	OR NOT g_bMissionClientGameStateRunning
	OR NOT IS_INSTANCED_CONTENT_COUNTDOWN_FINISHED()
	OR IS_PLAYER_SPECTATING(PLAYER_ID())
		EXIT
	ENDIF
	
	INT iTeam, iRule, iPortal
	iRule = GET_CURRENT_MC_TEAM_AND_RULE_FROM_GLOBAL_DATA(iTeam)
	IF iRule < FMMC_MAX_RULES
		
		BOOL bActivationPressed = HAS_PLAYER_TRIED_TO_ACTIVATE_WARP_PORTAL()
		IF sWarpPortal.iWarpPortalEntered > -1 
			iPortal = sWarpPortal.iWarpPortalEntered
		ELSE
			iPortal = sWarpPortal.iPortalStaggeredLoop 
		ENDIF
		
		SWITCH sWarpPortal.eWarpPortalState
			CASE eWarpPortalState_Idle				
				IF IS_PORTAL_ACTIVE_AND_AVAILABLE(iPortal, iTeam, iRule, bObjectiveBlocked #IF IS_DEBUG_BUILD , TRUE #ENDIF)
					IF IS_PLAYER_IN_RANGE_OF_PORTAL(iPortal)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_SPECIAL_TWO)
						
						IF IS_BIT_SET(g_FMMC_STRUCT.sWarpPortals[iPortal].iWarpPortalBS, ciWARP_PORTAL_RequiresTriggerLegacy)
							IF CAN_INSTANCED_CONTENT_PORTALS_TRIGGER_BE_USED()
								IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("WRPPRT_TRG_H")
									PRINT_HELP("WRPPRT_TRG_H", 5000)
									SET_BIT(sWarpPortal.iHelpTextPortalBS, iPortal)
								ENDIF
							ELSE
								IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("WRPPRT_TRG_H")
									CLEAR_HELP(TRUE)
									CLEAR_BIT(sWarpPortal.iHelpTextPortalBS, iPortal)
								ENDIF
								bActivationPressed = FALSE
							ENDIF
						ENDIF
						
						IF NOT IS_BIT_SET(g_FMMC_STRUCT.sWarpPortals[iPortal].iWarpPortalBS, ciWARP_PORTAL_RequiresTriggerLegacy)
						OR bActivationPressed
							PRINTLN("[LM][PROCESS_INSTANCED_CONTENT_PORTALS][wPortals] - Assigning sWarpPortal.iWarpPortalEntered as iPortal: ", iPortal)
							sWarpPortal.iWarpPortalEntered = iPortal
							NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
							SET_INSTANCED_PORTAL_CLIENT_STATE(eWarpPortalState_Init)
						ENDIF
					ELSE
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("WRPPRT_TRG_H")
							IF IS_BIT_SET(sWarpPortal.iHelpTextPortalBS, iPortal)
								CLEAR_HELP(TRUE)
								CLEAR_BIT(sWarpPortal.iHelpTextPortalBS, iPortal)
							ENDIF
						ENDIF
					ENDIF
				ELSE
					CLEAR_BIT(sWarpPortal.iJustUsedPortalStartBS, iPortal)
					CLEAR_BIT(sWarpPortal.iJustUsedPortalEndBS, iPortal)
				ENDIF
				
				BOOL bActive
				INT iPortalEveryFrame
				FOR iPortalEveryFrame = 0 TO sWarpPortal.iMaxNumberOfPortals-1
					bActive = FALSE
					IF IS_PORTAL_ACTIVE_AND_AVAILABLE(iPortalEveryFrame, iTeam, iRule, bObjectiveBlocked #IF IS_DEBUG_BUILD , FALSE #ENDIF)
						bActive = TRUE						
					ENDIF
					PROCESS_WARP_PORTAL_DISPLAY(iPortalEveryFrame, bActive)
				ENDFOR
			BREAK
			
			CASE eWarpPortalState_Init				
				IF IS_BIT_SET(g_FMMC_STRUCT.sWarpPortals[iPortal].iWarpPortalBS, ciWARP_PORTAL_FadeOutBeforeWarp)
				OR IS_BIT_SET(g_FMMC_STRUCT.sWarpPortals[iPortal].iWarpPortalBS, ciWARP_PORTAL_PostFXBeforeFade)
				OR NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sWarpPortals[iPortal].vEntryAnimCoord)
					SET_INSTANCED_PORTAL_CLIENT_STATE(eWarpPortalState_Preload)
				ELSE
					SET_INSTANCED_PORTAL_CLIENT_STATE(eWarpPortalState_Warping)
				ENDIF
			BREAK
			
			CASE eWarpPortalState_Preload
				IF HAS_PRELOAD_WARP_PORTAL_FINISHED(iPortal)
					IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sWarpPortals[iPortal].vEntryAnimCoord)
						SET_INSTANCED_PORTAL_CLIENT_STATE(eWarpPortalState_Cutscene)
					ELSE
						SET_INSTANCED_PORTAL_CLIENT_STATE(eWarpPortalState_Warping)
					ENDIF					
				ENDIF
			BREAK
			
			CASE eWarpPortalState_Cutscene
				IF HAS_CUTSCENE_ANIMATION_FOR_WARP_PORTAL_FINISHED(iPortal)
					SET_INSTANCED_PORTAL_CLIENT_STATE(eWarpPortalState_Warping)
				ENDIF
			BREAK
			
			CASE eWarpPortalState_Warping
				IF WARP_PLAYER_USING_INSTANCED_CONTENT_PORTAL(iPortal)
					SET_INSTANCED_PORTAL_CLIENT_STATE(eWarpPortalState_Sync)
				ENDIF
			BREAK
			
			CASE eWarpPortalState_Sync
				IF HAS_WARP_PORTAL_SYNC_STAGE_FINISHED(iPortal)
					NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					SET_INSTANCED_PORTAL_CLIENT_STATE(eWarpPortalState_Cleanup)
				ENDIF
			BREAK
			
			CASE eWarpPortalState_Cleanup
				sWarpPortal.iWarpPortalEntered = -1
				SET_INSTANCED_PORTAL_CLIENT_STATE(eWarpPortalState_Idle)
			BREAK		
		ENDSWITCH
		
		IF sWarpPortal.iWarpPortalEntered = -1 
			sWarpPortal.iPortalStaggeredLoop++
			IF sWarpPortal.iPortalStaggeredLoop >= sWarpPortal.iMaxNumberOfPortals
				sWarpPortal.iPortalStaggeredLoop = 0
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_DISABLE_REALTIME_MULTIPLAYER()
	
	IF g_bBlock_PROCESS_DISABLE_REALTIME_MULTIPLAYER
		PRINTLN("PROCESS_DISABLE_REALTIME_MULTIPLAYER - GLOBAL BLOCK SET - EXITING")
		EXIT
	ENDIF
	
	IF g_bMissionEnding
		NETWORK_DISABLE_REALTIME_MULTIPLAYER()
		PRINTLN("PROCESS_DISABLE_REALTIME_MULTIPLAYER - Calling NETWORK_DISABLE_REALTIME_MULTIPLAYER for Legacy Mission Controller")
	ENDIF
ENDPROC

PROC PROCESS_DLC_DIALOGUE_SETTINGS()
	IF NOT g_bUse_MP_DLC_Dialogue	
			
		SET_USE_DLC_DIALOGUE(TRUE)		
		
		#IF IS_DEBUG_BUILD
			PRINTLN("[Dialogue] - PROCESS_DLC_DIALOGUE_SETTINGS - g_bUse_MP_DLC_Dialogue Has Been Reset. Probably Ambient Script. Calling SET_USE_DLC_DIALOGUE")			
		#ENDIF
	ENDIF
ENDPROC


