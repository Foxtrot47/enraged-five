
USING "FM_Mission_Controller_USING.sch"
USING "FM_Mission_Controller_Utility.sch"
USING "FM_Mission_Controller_Entities.sch"
USING "FM_Mission_Controller_ObjectiveCompletion.sch"
USING "FM_Mission_Controller_SpecialCase.sch"
USING "FM_Mission_Controller_Rules.sch"
USING "FM_Mission_Controller_Doors.sch"
USING "FM_Mission_Controller_Cutscene.sch"
USING "FM_Mission_Controller_Debug.sch"
USING "FM_Mission_Controller_Locations.sch"
USING "FM_Mission_Controller_Vehicles.sch"
USING "FM_Mission_Controller_Peds_Brain.sch"
USING "FM_Mission_Controller_Peds_Body.sch"
USING "FM_Mission_Controller_Peds.sch"
USING "FM_Mission_Controller_GangChase.sch"
USING "FM_Mission_Controller_Minigame.sch"
USING "script_misc.sch"



//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//
//!!! NEW SECTION BELOW: SPECIAL PICKUPS - Binbags in Series A Pickups / Timetable in Prison Break Station !!!
//
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************



// The currently supported special pickup animations
ENUM ePickupAnimType
	ePickupAnim_PICKUP = 0,
	ePickupAnim_PUTDOWN,
	ePickupAnim_DROPOFF,
	ePickupAnim_DROPOFF_CAM,
	
	ePickupAnim_MAX
ENDENUM

PROC REMOVE_SPECIAL_PICKUP_ASSETS()
	IF( sCurrentSpecialPickupState.bAssetsLoaded )
		IF( NOT IS_STRING_NULL_OR_EMPTY( sCurrentSpecialPickupState.sClipSet ) )
			REMOVE_CLIP_SET( sCurrentSpecialPickupState.sClipSet )
			CPRINTLN( DEBUG_SIMON, "REMOVE_SPECIAL_PICKUP_ASSETS ", sCurrentSpecialPickupState.sClipSet, " removed" )
		ENDIF
		IF( NOT IS_STRING_NULL_OR_EMPTY( sCurrentSpecialPickupState.sAnimDict ) )
			REMOVE_ANIM_DICT( sCurrentSpecialPickupState.sAnimDict )
			CPRINTLN( DEBUG_SIMON, "REMOVE_SPECIAL_PICKUP_ASSETS ", sCurrentSpecialPickupState.sAnimDict, " removed" )
		ENDIF
		IF( NOT IS_STRING_NULL_OR_EMPTY( sCurrentSpecialPickupState.sWaveBank ) )
			RELEASE_NAMED_SCRIPT_AUDIO_BANK( sCurrentSpecialPickupState.sWaveBank )
			CPRINTLN( DEBUG_SIMON, "REMOVE_SPECIAL_PICKUP_ASSETS ", sCurrentSpecialPickupState.sWaveBank, " removed" )
		ENDIF
	
		sCurrentSpecialPickupState.bAssetsLoaded = FALSE
	ENDIF	
ENDPROC

///PURPOSE: The animation dictionary for the pickup/dropoff
///    anims on a special pickup
FUNC STRING GET_PICKUP_ANIM_DICT( INT iObjIndex )
	SWITCH( g_FMMC_STRUCT_ENTITIES.sPlacedObject[ iObjIndex ].mn )
		CASE HEI_PROP_HEIST_BINBAG 		RETURN "ANIM@HEISTS@NARCOTICS@TRASH" 		BREAK
		CASE HEI_PROP_HEI_TIMETABLE 	RETURN "ANIM@HEISTS@PRISON_HEISTSTATION@" 	BREAK
	ENDSWITCH
	
	RETURN ""
ENDFUNC

///PURPOSE: This function gets the correct animation clip string name
///    given the inputted object's model name and the type of animation
///    we want to play
FUNC STRING GET_PICKUP_ANIM_CLIP( INT iObjIndex, ePickupAnimType eType )
	SWITCH( g_FMMC_STRUCT_ENTITIES.sPlacedObject[ iObjIndex ].mn )
		CASE HEI_PROP_HEIST_BINBAG
			SWITCH( eType )
				CASE ePickupAnim_PICKUP 		RETURN sCurrentSpecialPickupState.sAnimName 	BREAK
				CASE ePickupAnim_PUTDOWN 		RETURN "drop_side" 	BREAK
				CASE ePickupAnim_DROPOFF 		RETURN "throw" 		BREAK
				CASE ePickupAnim_DROPOFF_CAM 	RETURN "throw_cam" 	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN ""
ENDFUNC

///PURPOSE: This function returns a specific animation clipset
///    to override the weapon movement for particular special pickups
FUNC STRING GET_PICKUP_CLIPSET_NAME( INT iObjIndex )
	SWITCH( g_FMMC_STRUCT_ENTITIES.sPlacedObject[ iObjIndex ].mn )
		CASE HEI_PROP_HEIST_BINBAG	RETURN "clipset@move@trash_fast_turn"	BREAK
	ENDSWITCH
	
	RETURN ""
ENDFUNC

///PURPOSE: Returns the wavebank name for a pickup type
FUNC STRING GET_PICKUP_WAVEBANK_NAME( INT iObjIndex )
	SWITCH( g_FMMC_STRUCT_ENTITIES.sPlacedObject[ iObjIndex ].mn )
		CASE HEI_PROP_HEIST_BINBAG	RETURN "DLC_MPHEIST/HEIST_TRASH_TRUCK"	BREAK
	ENDSWITCH
	
	RETURN ""
ENDFUNC

PROC SET_CURRENT_SPECIAL_PICKUP_STORED_WEAPON( WEAPON_TYPE wtToStore )
	IF wtToStore = WEAPONTYPE_INVALID
		SCRIPT_ASSERT( "SET_CURRENT_SPECIAL_PICKUP_STORED_WEAPON is getting passed WEAPONTYPE_INVALID - bad things will happen" )
	ENDIF
	
	IF( wtToStore <> WEAPONTYPE_DLC_GARBAGEBAG )
		sCurrentSpecialPickupState.wtStoredWeapon = wtToStore
	ENDIF
ENDPROC

FUNC WEAPON_TYPE GET_CURRENT_SPECIAL_PICKUP_STORED_WEAPON()
	IF sCurrentSpecialPickupState.wtStoredWeapon = WEAPONTYPE_INVALID
		SCRIPT_ASSERT( "Stored weapon type is WEAPONTYPE_INVALID - bad things will happen" )
	ENDIF

	RETURN sCurrentSpecialPickupState.wtStoredWeapon
ENDFUNC

PROC SET_BINBAG_NET_ID_CAN_MIGRATE( OBJECT_INDEX obj, BOOL bCanMigrate )
	SET_NETWORK_ID_CAN_MIGRATE( NETWORK_GET_NETWORK_ID_FROM_ENTITY( obj ), bCanMigrate )
	CPRINTLN( DEBUG_SIMON, "SET_BINBAG_NET_ID_CAN_MIGRATE - Setting bCanMigrate: ", bCanMigrate )
	DEBUG_PRINTCALLSTACK()
ENDPROC

PROC RESET_SPECIAL_PICKUP( BOOL bCleanup = FALSE )
	CPRINTLN( DEBUG_SIMON, "RESET_SPECIAL_PICKUP - Resetting" )
	DEBUG_PRINTCALLSTACK()
	
	IF( DOES_ENTITY_EXIST( sCurrentSpecialPickupState.obj ) )
		IF( GET_ENTITY_MODEL( sCurrentSpecialPickupState.obj ) = HEI_PROP_HEIST_BINBAG )
			IF( NOT bCleanup )
				BROADCAST_BINBAG_REQUEST( sCurrentSpecialPickupState.iCurrentlyOwnedBinbag, iPartToUse, TRUE )
			ENDIF
		ELIF( GET_ENTITY_MODEL( sCurrentSpecialPickupState.obj ) = HEI_PROP_HEI_TIMETABLE )
			RESET_OBJ_HACKING_INT()
		ENDIF
	
		IF( NETWORK_HAS_CONTROL_OF_ENTITY( sCurrentSpecialPickupState.obj ) )
			DETACH_ENTITY( sCurrentSpecialPickupState.obj )
			SET_ENTITY_COLLISION( sCurrentSpecialPickupState.obj, TRUE )
			SET_BINBAG_NET_ID_CAN_MIGRATE( sCurrentSpecialPickupState.obj, TRUE )
		ENDIF
	ENDIF

	IF( sCurrentSpecialPickupState.iCurrentlyRequestedObj > -1 
	AND sCurrentSpecialPickupState.iCurrentlyRequestedObj < FMMC_MAX_VEHICLES )
		BROADCAST_TRASH_DRIVE_ENABLE( sCurrentSpecialPickupState.iCurrentlyRequestedObj, iPartToUse, TRUE )
	ENDIF
	
	sCurrentSpecialPickupState.iCurrentlyOwnedBinbag = -1
	sCurrentSpecialPickupState.iCurrentlyRequestedObj = -1
	sCurrentSpecialPickupState.obj = NULL
	sCurrentSpecialPickupState.iStage = 0	

	IF( HAS_PED_GOT_WEAPON( LocalPlayerPed, WEAPONTYPE_DLC_GARBAGEBAG ) )
		SET_CURRENT_PED_WEAPON( LocalPlayerPed, WEAPONTYPE_UNARMED )
		REMOVE_WEAPON_FROM_PED( LocalPlayerPed, WEAPONTYPE_DLC_GARBAGEBAG )
	ENDIF
	
	// reset player ped
	IF( NOT IS_PED_INJURED( LocalPlayerPed ) AND NOT IS_LOCAL_PLAYER_ANY_SPECTATOR() )
		SET_PED_CONFIG_FLAG( LocalPlayerPed, PCF_DisableLadderClimbing, FALSE )
		CPRINTLN( DEBUG_SIMON, "RESET_SPECIAL_PICKUP - PCF_DisableLadderClimbing ENABLED" )
		IF( NOT bCleanup )
			SET_CURRENT_PED_WEAPON( LocalPlayerPed, GET_CURRENT_SPECIAL_PICKUP_STORED_WEAPON() )
		ENDIF
		
		// do not clear ped tasks if the player is currently in a strand mission cutscene
		IF( NOT bCleanup )
			CLEAR_PED_TASKS( LocalPlayerPed )
		ENDIF
		RESET_PED_MOVEMENT_CLIPSET( LocalPlayerPed, 0.5 )
	ENDIF
	
	// state vars
	IF( sCurrentSpecialPickupState.iContext <> -1 )
	 	RELEASE_CONTEXT_INTENTION( sCurrentSpecialPickupState.iContext )
	ENDIF
	
	MC_playerBD[ iPartToUse ].iVehRequest = -1
	
	sCurrentSpecialPickupState.vPlayerTargetPos = <<0.0, 0.0, 0.0>>
	sCurrentSpecialPickupState.iPosition = -1
	
	MC_playerBD[ iPartToUse ].iBinBagSyncScene = -1
	
	IF( bCleanup )
		sCurrentSpecialPickupState.ePickupState 			= eSpecialPickupState_BEFORE_PICKUP
		sCurrentSpecialPickupState.wtStoredWeapon 			= WEAPONTYPE_UNARMED
		sCurrentSpecialPickupState.fGroundHeightForPickup 	= -999999.0
		sCurrentSpecialPickupState.iTimer					= -1
		sCurrentSpecialPickupState.bAssetsLoaded			= FALSE
		INT i
		REPEAT FMMC_MAX_NUM_OBJECTS i
			sCurrentSpecialPickupState.eBinBagStage[ i ] = BBS_DORMANT
		ENDREPEAT
	ENDIF
ENDPROC

FUNC BOOL SHOULD_DROP_SPECIAL_PICKUP_CHECKS( OBJECT_INDEX obj )
	IF( DOES_ENTITY_EXIST( LocalPlayerPed ) AND DOES_ENTITY_EXIST( obj ) )
		// checks done in all cases
		IF( IS_PED_RAGDOLL( LocalPlayerPed )
		OR  IS_PED_DEAD_OR_DYING( LocalPlayerPed )
		OR  IS_PED_INJURED( LocalPlayerPed )
		OR  IS_ENTITY_ON_FIRE( LocalPlayerPed )
		OR	NOT bLocalPlayerPedOK )
			RETURN TRUE
		ENDIF
		// checks done when carrying the binbag
		IF( IS_ENTITY_ATTACHED_TO_ENTITY( obj, LocalPlayerPed ) )
			IF( IS_PED_SWIMMING( LocalPlayerPed )
			OR 	IS_PED_DIVING( LocalPlayerPed )
			OR  IS_PED_RUNNING_MELEE_TASK( LocalPlayerPed )
			OR  IS_PED_IN_PARACHUTE_FREE_FALL( LocalPlayerPed ) )
				RETURN TRUE
			ENDIF
		// checks done when not carrying the binbag
		ELSE 
			IF( IS_PED_ON_VEHICLE( LocalPlayerPed ) )
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE 
ENDFUNC

///PURPOSE: This function plays a special pickup's pick up animation 
///    once if it's not already playing
PROC PLAY_SPECIAL_PICKUP_ANIM( OBJECT_INDEX obj, ePickupAnimType eAnimToPlay, INT iObjIndex )
	
	IF( GET_ENTITY_MODEL( obj ) = HEI_PROP_HEI_TIMETABLE )
		IF( NOT IS_SYNC_SCENE_RUNNING( iSyncBusSchedule ) )
			STRING 						sAnimDict	= "ANIM@HEISTS@PRISON_HEISTSTATION@"
			STRING 						sAnimPed	= "pickup_bus_schedule"
			STRING 						sAnimObj	= "pickup_bus_schedule_schedule"
			STRING						sAnimCam	= "pickup_bus_schedule_cam"
			VECTOR 						vPos 		= <<436.7415, -996.3113, 29.6912>>
			VECTOR 						vRot		= <<0.0, 0.0, 155.00>>
			SYNCED_SCENE_PLAYBACK_FLAGS sspf 		= SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_HIDE_WEAPON | SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_EXPAND_PED_CAPSULE_FROM_SKELETON | SYNCED_SCENE_ABORT_ON_DEATH
				
			FREEZE_ENTITY_POSITION( obj, FALSE )
			FREEZE_ENTITY_POSITION( LocalPlayerPed, FALSE )
			
			iSyncBusSchedule = NETWORK_CREATE_SYNCHRONISED_SCENE( vPos, vRot )
			NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE( LocalPlayerPed, iSyncBusSchedule, sAnimDict, sAnimPed, SLOW_BLEND_IN, 
				SLOW_BLEND_OUT, sspf, RBF_PLAYER_IMPACT, SLOW_BLEND_IN )
			NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE( obj, iSyncBusSchedule, sAnimDict, sAnimObj, SLOW_BLEND_IN, SLOW_BLEND_OUT )
			NETWORK_ADD_SYNCHRONISED_SCENE_CAMERA( iSyncBusSchedule, sAnimDict, sAnimCam )
			NETWORK_START_SYNCHRONISED_SCENE( iSyncBusSchedule )
			
			//set all other remote players as hidden whilst scene runs MUST CLEAN UP!			
			/*INT iPartLoop
			PLAYER_INDEX tempPlayer
			REPEAT GET_MAXIMUM_PLAYERS_ON_MISSION() iPartLoop
				IF( iPartLoop <> iPartToUse )
					PARTICIPANT_INDEX pi = INT_TO_PARTICIPANTINDEX( iPartLoop )
					IF( NETWORK_IS_PARTICIPANT_ACTIVE( pi ) )
						tempPlayer = NETWORK_GET_PLAYER_INDEX( pi )	
						IF( tempPlayer <> INVALID_PLAYER_INDEX() )
							IF( IS_NET_PLAYER_OK( tempPlayer ) )
								NETWORK_CONCEAL_PLAYER( tempPlayer, TRUE )
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT*/
		ENDIF
	ELIF( GET_ENTITY_MODEL( obj ) = HEI_PROP_HEIST_BINBAG )
		IF( NOT IS_PED_PERFORMING_TASK( LocalPlayerPed, SCRIPT_TASK_PLAY_ANIM ) )
			FLOAT fBlendIn, fBlendOut
			IF( eAnimToPlay = ePickupAnim_PUTDOWN )
				fBlendIn = 0.75
				fBlendOut = -6.0
			ELIF( eAnimToPlay = ePickupAnim_PICKUP )
				fBlendIn = SLOW_BLEND_IN
				fBlendOut = SLOW_BLEND_OUT
			ELSE
				fBlendIn = INSTANT_BLEND_IN
				fBlendOut = INSTANT_BLEND_OUT
			ENDIF
		
			TASK_PLAY_ANIM( LocalPlayerPed, GET_PICKUP_ANIM_DICT( iObjIndex ), GET_PICKUP_ANIM_CLIP( iObjIndex, eAnimToPlay ),
				fBlendIn, fBlendOut, DEFAULT, AF_HOLD_LAST_FRAME | AF_NOT_INTERRUPTABLE )
		ENDIF
	ELSE
		ASSERTLN( "PLAY_SPECIAL_PICKUP_ANIM - special pickup type not setup!" )
	ENDIF
	
ENDPROC

FUNC BOOL IS_SPECIAL_PICKUP_ANIM_PLAYING( OBJECT_INDEX obj, ePickupAnimType eAnimToPlay, INT iObjIndex )
	IF( GET_ENTITY_MODEL( obj ) = HEI_PROP_HEI_TIMETABLE )
		RETURN IS_SYNC_SCENE_RUNNING( iSyncBusSchedule )
		
	ELIF( GET_ENTITY_MODEL( obj ) = HEI_PROP_HEIST_BINBAG )
		RETURN IS_ENTITY_PLAYING_ANIM( LocalPlayerPed, GET_PICKUP_ANIM_DICT( iObjIndex ),
			GET_PICKUP_ANIM_CLIP( iObjIndex, eAnimToPlay ) )
	ENDIF
	RETURN FALSE
ENDFUNC

///PURPOSE: Checks to see the progress of a particular animation - note the animation
///    MUST be already playing by the time you call this!
///    
///RETURNS: between 0.0 and 1.0 depending on the % through the animation passed
FUNC FLOAT GET_PICKUP_ANIM_PROGRESS( OBJECT_INDEX obj, ePickupAnimType eType, INT iObjIndex )
	IF( GET_ENTITY_MODEL( obj ) = HEI_PROP_HEI_TIMETABLE )
		IF( IS_SYNC_SCENE_RUNNING( iSyncBusSchedule ) )
			RETURN GET_SYNC_SCENE_PHASE( iSyncBusSchedule )
		ENDIF
	ELIF( GET_ENTITY_MODEL( obj ) = HEI_PROP_HEIST_BINBAG )
		STRING sAnimDict = GET_PICKUP_ANIM_DICT( iObjIndex )
		STRING sAnimName = GET_PICKUP_ANIM_CLIP( iObjIndex, eType )
		
		IF( IS_ENTITY_PLAYING_ANIM( LocalPlayerPed, sAnimDict,sAnimName ) )
			RETURN GET_ENTITY_ANIM_CURRENT_TIME( LocalPlayerPed, sAnimDict, sAnimName )
		ENDIF
	ENDIF
	
	RETURN 0.0
ENDFUNC

FUNC BOOL ARE_PICKUP_ASSETS_LOADED()
	RETURN sCurrentSpecialPickupState.bAssetsLoaded
ENDFUNC

///PURPOSE: This function will load all the associated animations
///    to do with a pickup that has associated anims/clipsets
PROC LOAD_SPECIAL_PICKUP_ASSETS( INT iObjIndex )
	IF( NOT ARE_PICKUP_ASSETS_LOADED() )
		BOOL bAnimDictLoaded 	= FALSE
		BOOL bClipSetLoaded 	= FALSE
		BOOL bWaveBankLoaded	= FALSE
		
		STRING sAnimDict 	= GET_PICKUP_ANIM_DICT( iObjIndex )
		STRING sClipSet		= GET_PICKUP_CLIPSET_NAME( iObjIndex )
		STRING sWaveBank	= GET_PICKUP_WAVEBANK_NAME( iObjIndex )

		IF( NOT IS_STRING_NULL_OR_EMPTY( sAnimDict ) )
			REQUEST_ANIM_DICT( sAnimDict )
			bAnimDictLoaded = HAS_ANIM_DICT_LOADED( sAnimDict )
			IF( bAnimDictLoaded )
				sCurrentSpecialPickupState.sAnimDict = sAnimDict
			ENDIF
		ELSE
			bAnimDictLoaded = TRUE
		ENDIF
		IF( NOT IS_STRING_NULL_OR_EMPTY( sClipSet ) )
			REQUEST_CLIP_SET( sClipSet )
			bClipSetLoaded = HAS_CLIP_SET_LOADED( sClipSet )
			IF( bClipSetLoaded )
				sCurrentSpecialPickupState.sClipSet = sClipSet
			ENDIF
		ELSE
			bClipSetLoaded = TRUE
		ENDIF
		IF( NOT IS_STRING_NULL_OR_EMPTY( sWaveBank ) )
			bWaveBankLoaded = REQUEST_SCRIPT_AUDIO_BANK( sWaveBank )
			IF( bWaveBankLoaded )
				sCurrentSpecialPickupState.sWaveBank = sWaveBank
			ENDIF
		ELSE
			bWaveBankLoaded = TRUE
		ENDIF
		
		sCurrentSpecialPickupState.bAssetsLoaded = bAnimDictLoaded AND bClipSetLoaded AND bWaveBankLoaded
		
		#IF IS_DEBUG_BUILD
		IF( sCurrentSpecialPickupState.bAssetsLoaded )
			IF( NOT IS_STRING_NULL_OR_EMPTY( sAnimDict ) )
				CPRINTLN( DEBUG_SIMON, "LOAD_SPECIAL_PICKUP_ASSETS ", sAnimDict, " loaded" )
			ENDIF
			IF( NOT IS_STRING_NULL_OR_EMPTY( sClipSet ) )
				CPRINTLN( DEBUG_SIMON, "LOAD_SPECIAL_PICKUP_ASSETS ", sClipSet, " loaded" )
			ENDIF
			IF( NOT IS_STRING_NULL_OR_EMPTY( sWaveBank ) )
				CPRINTLN( DEBUG_SIMON, "LOAD_SPECIAL_PICKUP_ASSETS ", sWaveBank, " loaded" )
			ENDIF
		ENDIF
		#ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_SPECIAL_PICKUP_INCOMPLETE( INT iObjIndex )
	IF( IS_THIS_A_SPECIAL_PICKUP( iObjIndex ) )
		INT iTeam
		FOR iTeam = 0 TO ( MC_serverBD.iNumberOfTeams - 1 )
			IF( MC_serverBD_4.iObjPriority[ iObjIndex ][ iTeam ] <> FMMC_PRIORITY_IGNORE )
				RETURN TRUE
			ENDIF
		ENDFOR
	ENDIF
	RETURN FALSE
ENDFUNC

PROC MANAGE_SPECIAL_PICKUP_ASSETS( INT iObjIndex )
	IF( IS_SPECIAL_PICKUP_INCOMPLETE( iObjIndex ) )
		IF( NOT ARE_PICKUP_ASSETS_LOADED() )
			LOAD_SPECIAL_PICKUP_ASSETS( iObjIndex )
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL GET_SPECIAL_OBJECT( INT iObjIndex, OBJECT_INDEX &obj )
	IF( iObjIndex <> -1 )
		INT iTeam = MC_playerBD[ iPartToUse ].iteam
		INT iRule = MC_serverBD_4.iCurrentHighestPriority[ iTeam ]
		NETWORK_INDEX niPickup = MC_serverBD_1.sFMMC_SBD.niObject[ iObjIndex ]
		
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID( niPickup )
		AND (iRule < FMMC_MAX_RULES)
		AND (MC_serverBD_4.iObjPriority[iObjIndex][iTeam] <= MC_serverBD_4.iCurrentHighestPriority[iTeam])
		AND (MC_serverBD_4.iObjRule[iObjIndex][iTeam] = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER)
			obj	= NET_TO_OBJ( niPickup ) // Potential pickup object
			IF( DOES_ENTITY_EXIST( obj ) )
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL GET_MISSION_OBJECT( INT iObj, OBJECT_INDEX &obj )
	IF( iObj >= 0 AND iObj < FMMC_MAX_NUM_OBJECTS )
		NETWORK_INDEX niObj = MC_serverBD_1.sFMMC_SBD.niObject[ iObj ]
		IF( NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID( niObj ) )
			obj	= NET_TO_OBJ( niObj )
			IF( DOES_ENTITY_EXIST( obj ) )
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: BINBAG SPECIAL PICKUP !
//
//************************************************************************************************************************************************************



FUNC BOOL DELAYED_RESET()
	IF( sCurrentSpecialPickupState.iTimer <> - 1 )
		IF( sCurrentSpecialPickupState.iTimer < GET_GAME_TIMER() )
			CPRINTLN( DEBUG_SIMON, "DELAYED_RESET - Finished" )
			sCurrentSpecialPickupState.iTimer = - 1
			SET_PED_CONFIG_FLAG( LocalPlayerPed, PCF_DisableLadderClimbing, FALSE )
			CPRINTLN( DEBUG_SIMON, "DELAYED_RESET - PCF_DisableLadderClimbing ENABLED" )
			IF( NOT IS_PED_INJURED( LocalPlayerPed ) )
				CLEAR_PED_TASKS( LocalPlayerPed )
			ENDIF
			WEAPON_TYPE wt
			IF( GET_CURRENT_PED_WEAPON( LocalPlayerPed, wt, FALSE ) )
				IF( wt = WEAPONTYPE_UNARMED ) 
					SET_CURRENT_PED_WEAPON( LocalPlayerPed, GET_CURRENT_SPECIAL_PICKUP_STORED_WEAPON() )
				ENDIF
			ENDIF
			sCurrentSpecialPickupState.vPlayerTargetPos = <<0.0, 0.0, 0.0>>
			sCurrentSpecialPickupState.iPosition = -1
			RETURN TRUE
		ENDIF
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

FUNC VECTOR NORM_VEC( VECTOR vec )
	RETURN vec / VMAG( vec )
ENDFUNC

FUNC VECTOR GET_TRASH_TRUE_FORWARD_VECTOR( VEHICLE_INDEX veh )
	VECTOR vPos1 = GET_WORLD_POSITION_OF_ENTITY_BONE( veh, GET_ENTITY_BONE_INDEX_BY_NAME( veh, "suspension_lm" ) ) // suspension left mid
	VECTOR vPos2 = GET_WORLD_POSITION_OF_ENTITY_BONE( veh, GET_ENTITY_BONE_INDEX_BY_NAME( veh, "suspension_lr" ) ) // suspension left rear
	RETURN NORM_VEC( vPos1 - vPos2 )
ENDFUNC

FUNC BOOL CHECK_TRASH_HAS_VALID_ORIENTATION( VEHICLE_INDEX veh )
	VECTOR vRot = GET_ENTITY_ROTATION( veh )
	FLOAT fPitch = ABSF( vRot.x )
	FLOAT fRoll = ABSF( vRot.y )
	RETURN fPitch < 5.0 AND fRoll < 5.0
ENDFUNC

FUNC BOOL ROLLING_CHECK_FOR_SPECIAL_PICKUP_OBSTRUCTION( PED_INDEX ped, ENTITY_INDEX ent, SHAPETEST_INDEX &sti, VECTOR vLocalOffset )
	VECTOR 				vPlayerPos 		= GET_ENTITY_COORDS( ped, FALSE )
	VECTOR 				vTargetPos		= GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS( ent, vLocalOffset )
	INT					iLOSInclude 	= SCRIPT_INCLUDE_MOVER | SCRIPT_INCLUDE_VEHICLE | SCRIPT_INCLUDE_PED | SCRIPT_INCLUDE_RAGDOLL | SCRIPT_INCLUDE_OBJECT | SCRIPT_INCLUDE_GLASS
	INT 				iHitSomething
	VECTOR 				vPos
	VECTOR				vNormal
	ENTITY_INDEX 		entity
	SHAPETEST_STATUS	status
	
	status = GET_SHAPE_TEST_RESULT( sti, iHitSomething, vPos, vNormal, entity )
	IF( status = SHAPETEST_STATUS_RESULTS_READY )
		sCurrentSpecialPickupState.bObstructionExists = ( iHitSomething = 1 )
		IF( sCurrentSpecialPickupState.bObstructionExists )
			IF( entity = ent )
				sCurrentSpecialPickupState.bObstructionExists = FALSE
			ENDIF
		ENDIF
	ELIF( status = SHAPETEST_STATUS_NONEXISTENT )
		sti = START_SHAPE_TEST_LOS_PROBE( vPlayerPos, vTargetPos, iLOSInclude, ped, 0 )
	ENDIF
	
	RETURN NOT sCurrentSpecialPickupState.bObstructionExists
ENDFUNC

FUNC BOOL CLEAR_ROLLING_CHECK_SHAPETEST( SHAPETEST_INDEX &sti )
	INT 				iHitSomething
	VECTOR 				vPos
	VECTOR				vNormal
	ENTITY_INDEX 		entity
	SHAPETEST_STATUS	status

	status = GET_SHAPE_TEST_RESULT( sti, iHitSomething, vPos, vNormal, entity )
	
	RETURN status = SHAPETEST_STATUS_RESULTS_READY OR status = SHAPETEST_STATUS_NONEXISTENT
ENDFUNC

FUNC FLOAT GET_TRIANGLE_ALTITUDE_FROM_VECTORS( VECTOR a, VECTOR b, VECTOR c )
	FLOAT fArea = 0.5 * VMAG( CROSS_PRODUCT( b - a, b - c ) )
	FLOAT fMagAtoC = VMAG( a - c )
	RETURN fArea / ( 0.5 * fMagAtoC )
ENDFUNC

FUNC STRING GET_DROP_OFF_PED_ANIM_NAME( PED_INDEX ped, VEHICLE_INDEX veh, BOOL bRightHanded = TRUE )
	VECTOR vPedPos 	= GET_ENTITY_COORDS( ped, FALSE )
	VECTOR vVehPos 	= GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS( veh, <<0.0, -4.4, 0.0>> )
	vVehPos.z = vPedPos.z
	FLOAT  fDist	= VDIST( vPedPos, vVehPos )

	STRING sAnimName
	IF( fDist < 1.1 )
		IF( bRightHanded )
			sAnimName = "THROW_RANGED_A"
		ELSE
			sAnimName = "THROW_RANGED_SIDE_A"
		ENDIF
	ELIF( fDist < 1.4 )
		IF( bRightHanded )
			sAnimName = "THROW_RANGED_B"
		ELSE
			sAnimName = "THROW_RANGED_SIDE_B"
		ENDIF
	ELIF( fDist < 1.9 )
		IF( bRightHanded )
			sAnimName = "THROW_RANGED_C"
		ELSE
			sAnimName = "THROW_RANGED_SIDE_C"
		ENDIF
	ELIF( fDist < 2.4 )
		IF( bRightHanded )
			sAnimName = "THROW_RANGED_D"
		ELSE
			sAnimName = "THROW_RANGED_SIDE_D"
		ENDIF
	ELIF( fDist < 2.9 )
		IF( bRightHanded )
			sAnimName = "THROW_RANGED_E"
		ELSE
			sAnimName = "THROW_RANGED_SIDE_E"
		ENDIF
	ELIF( fDist < 3.4 )
		IF( bRightHanded )
			sAnimName = "THROW_RANGED_F"		
		ELSE
			sAnimName = "THROW_RANGED_SIDE_F"
		ENDIF
	ELSE
		IF( bRightHanded )
			sAnimName = "THROW_RANGED_G"
		ELSE
			sAnimName = "THROW_RANGED_SIDE_G"
		ENDIF
	ENDIF
	
	RETURN sAnimName
ENDFUNC

#IF IS_DEBUG_BUILD

PROC DEBUG_DRAW_GET_VECTOR_PLANE_INTERSECTION_COORD( VECTOR vLineOrigin, VECTOR vLineDirection, FLOAT fExtent, 
	VECTOR vTopLeft, VECTOR vTopRight, VECTOR vBottomLeft, VECTOR vBottomRight )
	
	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE( TRUE )
	
	// draw hopper aperture
	DRAW_DEBUG_LINE( vTopLeft, vTopRight )
	DRAW_DEBUG_LINE( vTopRight, vBottomRight )
	DRAW_DEBUG_LINE( vBottomRight, vBottomLeft )
	DRAW_DEBUG_LINE( vBottomLeft, vTopLeft )
	
	// draw ped to hopper
	VECTOR vIntersect = vLineOrigin + fExtent * vLineDirection
	DRAW_DEBUG_LINE( vLineOrigin, vIntersect )
ENDPROC

PROC DEBUG_DRAW_GET_VECTOR_PLANE_INTERSECTION_COORD_SPHERE( VECTOR vIntersect, BOOL bInbounds )
	IF( bInbounds )
		DRAW_DEBUG_SPHERE( vIntersect, 0.15, 0, 255, 0, 255 )
	ELSE		
		DRAW_DEBUG_SPHERE( vIntersect, 0.15, 255, 0, 0, 255 )
	ENDIF
ENDPROC

PROC DEBUG_DRAW_THROW_ANIM_TEXT_AT_POSITION( PED_INDEX ped, VEHICLE_INDEX veh, BOOL bRightHanded = TRUE )
	DRAW_DEBUG_TEXT_2D( GET_DROP_OFF_PED_ANIM_NAME( ped, veh, bRightHanded ), <<0.05, 0.6, 0.0>> )
ENDPROC

#ENDIF

FUNC BOOL GET_VECTOR_PLANE_INTERSECTION_COORD( VECTOR vPlaneCenter, VECTOR vPlaneNormal, FLOAT fPlaneWidth, FLOAT fPlaneHeight, 
	VECTOR vLineOrigin, VECTOR vLineDirection, VECTOR &vResult )
	
	VECTOR vLeft = NORM_VEC( CROSS_PRODUCT( vPlaneNormal, <<0.0, 0.0, -1.0>> ) )
	VECTOR vUp	 = NORM_VEC( CROSS_PRODUCT( vPlaneNormal, vLeft ) )
	
	FLOAT fHalfPlaneWidth  = fPlaneWidth / 2.0
	FLOAT fHalfPlaneHeight = fPlaneHeight / 2.0
	
	VECTOR vTopLeft		= vPlaneCenter + -fHalfPlaneWidth * vLeft + fHalfPlaneHeight  * vUp
	VECTOR vTopRight	= vPlaneCenter + fHalfPlaneWidth  * vLeft + fHalfPlaneHeight  * vUp
	VECTOR vBottomLeft	= vPlaneCenter + -fHalfPlaneWidth * vLeft + -fHalfPlaneHeight * vUp
	VECTOR vBottomRight	= vPlaneCenter + fHalfPlaneWidth  * vLeft + -fHalfPlaneHeight * vUp
	
	FLOAT fExtent = DOT_PRODUCT( ( vPlaneCenter - vLineOrigin ), vPlaneNormal ) / DOT_PRODUCT( vLineDirection, vPlaneNormal )
	vResult = vLineOrigin + ( fExtent * vLineDirection )

	/*
	#IF IS_DEBUG_BUILD
	DEBUG_DRAW_GET_VECTOR_PLANE_INTERSECTION_COORD( vLineOrigin, vLineDirection, fExtent, vTopLeft, vTopRight, vBottomLeft, vBottomRight )
	#ENDIF
	*/
	
	IF( GET_TRIANGLE_ALTITUDE_FROM_VECTORS( vTopLeft, vResult, vTopRight ) <= fPlaneHeight
	AND GET_TRIANGLE_ALTITUDE_FROM_VECTORS( vBottomLeft, vResult, vBottomRight ) <= fPlaneHeight
	AND GET_TRIANGLE_ALTITUDE_FROM_VECTORS( vTopLeft, vResult, vBottomLeft ) <= fPlaneWidth
	AND GET_TRIANGLE_ALTITUDE_FROM_VECTORS( vTopRight, vResult, vBottomRight ) <= fPlaneWidth )
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

CONST_INT	BINBAGHELP_TRUCK_REAR_BLOCKED			0
CONST_INT	BINBAGHELP_TRUCK_MUST_BE_LEVEL			1
CONST_INT	BINBAGHELP_PICKUP_POSITION_UNSUITABLE	2
CONST_INT	BINBAGHELP_PICKUP_OBSTRUCTED			3
CONST_INT	BINBAGHELP_PICKUP						4
CONST_INT	BINBAGHELP_THROW						5
CONST_INT	BINBAGHELP_DROP							6
CONST_INT	BINBAGHELP_PICKUP_INIT					7
CONST_INT	BINBAGHELP_THROW_INIT					8
CONST_INT	BINBAGHELP_DROP_INIT					9

PROC SET_BIN_BAG_HELP_TEXT_BIT( INT iBit )
	SET_BIT( sCurrentSpecialPickupState.iHelpTextBits, iBit )
ENDPROC

PROC PRINT_HIGHEST_PRIORITY_BIN_BAG_HELP_TEXT()
	BOOL 	bDoSound 	= TRUE
	STRING 	sLabel 		= ""
	
	IF( IS_BIT_SET( sCurrentSpecialPickupState.iHelpTextBits, BINBAGHELP_TRUCK_REAR_BLOCKED ) )
		sLabel = "HEIST_HELP_27" // The rear of the trash truck is blocked.
		
	ELIF( IS_BIT_SET( sCurrentSpecialPickupState.iHelpTextBits, BINBAGHELP_TRUCK_MUST_BE_LEVEL ) )
		sLabel = "HEIST_HELP_31" // The trash truck must be level before dropping off a trash bag.
		
	ELIF( IS_BIT_SET( sCurrentSpecialPickupState.iHelpTextBits, BINBAGHELP_PICKUP_POSITION_UNSUITABLE ) )
		sLabel = "HEIST_HELP_40" // The trash bag is in an unsuitable position to be picked up.

	ELIF( IS_BIT_SET( sCurrentSpecialPickupState.iHelpTextBits, BINBAGHELP_PICKUP_OBSTRUCTED ) )
		sLabel = "HEIST_HELP_38" // Trash bag collection is obstructed.
		
	ELIF( IS_BIT_SET( sCurrentSpecialPickupState.iHelpTextBits, BINBAGHELP_PICKUP ) )
		sLabel = "HEIST_HELP_14" // Press ~INPUT_CONTEXT~ to pick up the trash bag.
		bDoSound = NOT IS_BIT_SET( sCurrentSpecialPickupState.iHelpTextBits, BINBAGHELP_PICKUP_INIT )
		SET_BIT( sCurrentSpecialPickupState.iHelpTextBits, BINBAGHELP_PICKUP_INIT )
		
	ELIF( IS_BIT_SET( sCurrentSpecialPickupState.iHelpTextBits, BINBAGHELP_THROW ) )
		sLabel = "HEIST_HELP_15" // Press ~INPUT_CONTEXT~ to throw the trash bag in the truck.
		bDoSound = NOT IS_BIT_SET( sCurrentSpecialPickupState.iHelpTextBits, BINBAGHELP_THROW_INIT )
		SET_BIT( sCurrentSpecialPickupState.iHelpTextBits, BINBAGHELP_THROW_INIT )
		
	ELIF( IS_BIT_SET( sCurrentSpecialPickupState.iHelpTextBits, BINBAGHELP_DROP ) )
		sLabel = "HEIST_HELP_17" // Press ~INPUT_CONTEXT~ to drop the trash bag.
		bDoSound = NOT IS_BIT_SET( sCurrentSpecialPickupState.iHelpTextBits, BINBAGHELP_DROP_INIT )
		SET_BIT( sCurrentSpecialPickupState.iHelpTextBits, BINBAGHELP_DROP_INIT )
	ENDIF
	
	IF( NOT IS_STRING_NULL_OR_EMPTY( sLabel ) )
		IF( NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED( sLabel ) )
			BEGIN_TEXT_COMMAND_DISPLAY_HELP( sLabel )
			END_TEXT_COMMAND_DISPLAY_HELP( HELP_TEXT_SLOT_STANDARD, TRUE, bDoSound )
		ENDIF
	ELSE
		IF( IS_THIS_HELP_MESSAGE_BEING_DISPLAYED( "HEIST_HELP_27" )
		OR  IS_THIS_HELP_MESSAGE_BEING_DISPLAYED( "HEIST_HELP_31" )
		OR  IS_THIS_HELP_MESSAGE_BEING_DISPLAYED( "HEIST_HELP_40" )
		OR  IS_THIS_HELP_MESSAGE_BEING_DISPLAYED( "HEIST_HELP_38" )
		OR  IS_THIS_HELP_MESSAGE_BEING_DISPLAYED( "HEIST_HELP_14" )
		OR  IS_THIS_HELP_MESSAGE_BEING_DISPLAYED( "HEIST_HELP_15" )
		OR  IS_THIS_HELP_MESSAGE_BEING_DISPLAYED( "HEIST_HELP_17" ) )
			CLEAR_ALL_HELP_MESSAGES()
		ENDIF
	ENDIF
	// clears bits 0 - 6 but preserves 7 - 9
	sCurrentSpecialPickupState.iHelpTextBits = sCurrentSpecialPickupState.iHelpTextBits & 896
ENDPROC

FUNC BOOL IS_OBJECT_IN_VEHICLE_DIMENSION_ANGLED_AREA( OBJECT_INDEX obj, VEHICLE_INDEX veh, VECTOR vPadding )
	IF( IS_ENTITY_ALIVE( obj ) AND IS_ENTITY_ALIVE( veh ) )
		VECTOR 		vVehPos 	= GET_ENTITY_COORDS( veh, FALSE )
		MODEL_NAMES modVeh 		= GET_ENTITY_MODEL( veh )
		VECTOR 		vVehDimMin, vVehDimMax
		VECTOR 		vVehForward = GET_ENTITY_FORWARD_VECTOR( veh )
		
		vVehForward = NORMALISE_VECTOR( vVehForward )
		GET_MODEL_DIMENSIONS( modVeh, vVehDimMin, vVehDimMax )
		
		IF( GET_ENTITY_MODEL( veh ) = TRASH2
		OR  GET_ENTITY_MODEL( veh ) = TRASH )
			FLOAT fDepthMod = 0.93
			FLOAT fWidthMod = 0.9
			vVehDimMin = <<vVehDimMin.x * fWidthMod, vVehDimMin.y * fDepthMod, vVehDimMin.z>>
			vVehDimMax = <<vVehDimMax.x * fWidthMod, vVehDimMax.y * fDepthMod, vVehDimMax.z>>
			vVehPos.y += 0.15
		ENDIF
		
		VECTOR vCoors1 = vVehPos + vVehForward * vVehDimMax.y + <<0.0, 0.0, vVehDimMax.z>> + <<0.0, vPadding.y, vPadding.z>>
		VECTOR vCoors2 = vVehPos + vVehForward * vVehDimMin.y + <<0.0, 0.0, vVehDimMin.z>> - <<0.0, vPadding.y, vPadding.z>>
		FLOAT  fWidth  = -vVehDimMin.x + vVehDimMax.x + 2.0 * vPadding.x
		
		RETURN IS_ENTITY_IN_ANGLED_AREA( obj, vCoors1, vCoors2, fWidth )
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_OBJECT_CONFLICTING_WITH_NEARBY_VEHICLE( OBJECT_INDEX obj, PED_INDEX ped )
	VEHICLE_INDEX vehs[ 3 ]
	GET_PED_NEARBY_VEHICLES( ped, vehs )
	INT i
	REPEAT COUNT_OF( vehs ) i
		IF( DOES_ENTITY_EXIST( vehs[ i ] ) )
			IF( IS_OBJECT_IN_VEHICLE_DIMENSION_ANGLED_AREA( obj, vehs[ i ], <<0.0, 0.0, 1.0>> ) )
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ANY_PED_BETWEEN_PED_AND_VEHICLE( PED_INDEX ped, VEHICLE_INDEX veh )
	INT 		i
	PED_INDEX 	peds[ 3 ]
	VECTOR 		vPedPos 	= GET_ENTITY_COORDS( ped, FALSE )
	VECTOR 		vVehPos		= GET_ENTITY_COORDS( veh, FALSE )
	VECTOR 		vVehBumper	= GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS( veh, <<0.0, -5.0, 0.0>> )
	VECTOR		vPedToVeh	= NORM_VEC( vVehPos - vPedPos )

	GET_PED_NEARBY_PEDS( ped, peds )
	REPEAT COUNT_OF( peds ) i
		IF( IS_ENTITY_ALIVE( peds[ i ] ) )			
			IF( NOT IS_PED_IN_ANY_VEHICLE( peds[ i ] ) )
				VECTOR vPedsPos = GET_ENTITY_COORDS( peds[ i ], FALSE )
				IF( VDIST2( vPedPos, vPedsPos ) <= VDIST2( vPedPos, vVehBumper ) )
					VECTOR vPedToPed = NORM_VEC( vPedsPos - vPedPos )
					IF( DOT_PRODUCT( vPedToVeh, vPedToPed ) > 0.75 )
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PED_ELIGIBLE_FOR_BIN_BAG_THROW( PED_INDEX ped, VEHICLE_INDEX veh, SHAPETEST_INDEX &sti, BOOL &bRightHandedThrow )
	BOOL bVehChecks = FALSE
	IF( IS_VEHICLE_DRIVEABLE( veh ) )
		IF( GET_ENTITY_SPEED( veh ) < 0.5 )
			IF( GET_VEHICLE_DOOR_ANGLE_RATIO( veh, SC_DOOR_BOOT ) > 0.8 )
				bVehChecks = TRUE
			ENDIF
		ENDIF		
	ENDIF
	BOOL bPositionChecks = FALSE
	IF( bVehChecks )
		VECTOR vPlaneNormal 	= -GET_TRASH_TRUE_FORWARD_VECTOR( veh )
		VECTOR vLineOrigin		= GET_ENTITY_COORDS( ped, FALSE )
		VECTOR vLineDirection	= NORM_VEC( GET_ENTITY_COORDS( veh, FALSE ) - vLineOrigin )
		VECTOR vIntersect
		FLOAT  fPlaneHeight		= 0.5
		
		// right handed throw area
		VECTOR vPlaneCenterR	= GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS( veh, <<-0.225, -5.0, 0.3>> )
		FLOAT  fPlaneWidthR		= 1.35
		
		// left handed throw area
		VECTOR vPlaneCenterL	= GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS( veh, <<0.675, -5.0, 0.3>> )
		FLOAT  fPlaneWidthL		= 0.45
		
		IF( NOT IS_PED_ON_VEHICLE( ped ) )
			// right handed throw
			IF( GET_VECTOR_PLANE_INTERSECTION_COORD( vPlaneCenterR, vPlaneNormal, fPlaneWidthR, fPlaneHeight, vLineOrigin, vLineDirection, vIntersect ) )
				IF( VDIST2( vLineOrigin, vIntersect ) < 9.0 )
					/*
					#IF IS_DEBUG_BUILD
					DEBUG_DRAW_THROW_ANIM_TEXT_AT_POSITION( ped, veh, TRUE )
					#ENDIF
					*/
					bRightHandedThrow = TRUE
					bPositionChecks = TRUE
				ENDIF
			// left handed throw	
			ELIF( GET_VECTOR_PLANE_INTERSECTION_COORD( vPlaneCenterL, vPlaneNormal, fPlaneWidthL, fPlaneHeight, vLineOrigin, vLineDirection, vIntersect ) )
				IF( VDIST2( vLineOrigin, vIntersect ) < 9.0 )
					/*
					#IF IS_DEBUG_BUILD
					DEBUG_DRAW_THROW_ANIM_TEXT_AT_POSITION( ped, veh, FALSE )
					#ENDIF
					*/
					bRightHandedThrow = FALSE
					bPositionChecks = TRUE
				ENDIF
			ENDIF
			
			/*
			#IF IS_DEBUG_BUILD
			DEBUG_DRAW_GET_VECTOR_PLANE_INTERSECTION_COORD_SPHERE( vIntersect, bPositionChecks )
			#ENDIF
			*/
		ENDIF
	ENDIF
	BOOL bObstructionChecks = FALSE
	BOOL bWasEligibleLastFrame = bWasPedEligibleForBinBagThrow
	IF( bPositionChecks )
		IF( CHECK_TRASH_HAS_VALID_ORIENTATION( veh ) )
			IF( ROLLING_CHECK_FOR_SPECIAL_PICKUP_OBSTRUCTION( ped, veh, sti, <<0.0, -4.4, 0.0>> )
			AND NOT IS_ANY_PED_BETWEEN_PED_AND_VEHICLE( ped, veh ) )
				bObstructionChecks = TRUE
			ELSE
				SUPPRESS_CONTEXT_BUTTON_ACTIONS_THIS_FRAME()
				SET_BIN_BAG_HELP_TEXT_BIT( BINBAGHELP_TRUCK_REAR_BLOCKED )
			ENDIF
		ELSE
			SUPPRESS_CONTEXT_BUTTON_ACTIONS_THIS_FRAME()
			SET_BIN_BAG_HELP_TEXT_BIT( BINBAGHELP_TRUCK_MUST_BE_LEVEL )
		ENDIF
	ENDIF
	
	bWasPedEligibleForBinBagThrow = bVehChecks AND bObstructionChecks AND bPositionChecks
	RETURN bVehChecks AND bObstructionChecks AND bPositionChecks AND bWasEligibleLastFrame
ENDFUNC

//PURPOSE: Calculates a time to be used in TASK_TURN_PED_TO_FACE_ENTITY which will give a consistent rate of turn,
//regardless of the magnitude of heading change. fSpeed is in degrees per second
FUNC INT GET_TURN_TASK_TIME( PED_INDEX ped, FLOAT fTargetHeading, FLOAT fSpeed = 45.0 )
	FLOAT fHeadingDeltaAbs = GET_ABSOLUTE_DELTA_HEADING( fTargetHeading, GET_ENTITY_HEADING( ped ) )
	RETURN ROUND( fHeadingDeltaAbs / fSpeed ) * 1000
ENDFUNC

PROC UPDATE_BIN_BAG_DROP_OFF_ALIGNMENT( INT &iStage, PED_INDEX ped, OBJECT_INDEX obj, VEHICLE_INDEX veh, INT iDropOffVehID, SHAPETEST_INDEX &sti, BOOL &bRightHandedThrow )
	CONST_INT	UNSET						-1
	CONST_INT	INIT_VEH_RESERVATION_0		0
	CONST_INT	INIT_HEADING_1				1
	CONST_INT	UPDATE_HEADING_2			2
	CONST_INT	UPDATE_VEH_RESERVATION_3	3
	CONST_INT	DONE_4						4
	CONST_INT	GIVE_UP_5 					5
	
	CONST_INT	FLAG_MASK					-67108864 // 6 bits
	CONST_INT	STAGE_MASK					31 // 5 bits
	
	CONST_INT	F_ALIGNMENT_FINISHED		31
	CONST_INT	F_TURNING_CLOCKWISE			30
	
	INT		iSubStage			= iStage & STAGE_MASK
	INT		iFlags				= iStage & FLAG_MASK

	SET_PED_MAX_MOVE_BLEND_RATIO( ped, PEDMOVEBLENDRATIO_WALK )
	DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_MOVE_LR )
	DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_MOVE_UD )
	DISABLE_CELLPHONE_THIS_FRAME_ONLY()
	SUPPRESS_CONTEXT_BUTTON_ACTIONS_THIS_FRAME()
	IF( GET_FOLLOW_PED_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON )
		DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_LOOK_LR )
		DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_LOOK_UD )
	ENDIF
	
	#IF IS_DEBUG_BUILD
	STRING str = ""
	SWITCH( iSubStage )
		CASE INIT_VEH_RESERVATION_0		str = "INIT_VEH_RESERVATION_0"		BREAK
		CASE INIT_HEADING_1				str = "INIT_HEADING_1"				BREAK
		CASE UPDATE_HEADING_2			str = "UPDATE_HEADING_2"			BREAK
		CASE UPDATE_VEH_RESERVATION_3	str = "UPDATE_VEH_RESERVATION_3"	BREAK
		CASE DONE_4						str = "DONE_4"						BREAK
		CASE GIVE_UP_5					str = "GIVE_UP_5"					BREAK
	ENDSWITCH
	CPRINTLN( DEBUG_SIMON, "UPDATE_BIN_BAG_DROP_OFF_ALIGNMENT iStage: ", str )
	#ENDIF
	
	VECTOR	vDepositPos			= GET_ENTITY_COORDS( veh, FALSE )
	VECTOR 	vPedPos				= GET_ENTITY_COORDS( ped, FALSE )
	FLOAT 	fDepositHeading 	= GET_HEADING_BETWEEN_VECTORS( vPedPos, vDepositPos )
	BOOL	bRecurse			= FALSE
	FLOAT	fHeadingTolerance	= 30.0
	
	IF( IS_ANY_PED_BETWEEN_PED_AND_VEHICLE( ped, veh ) 
	OR IS_PED_ON_VEHICLE( ped )
	OR SHOULD_DROP_SPECIAL_PICKUP_CHECKS( obj ) )
		iSubStage = GIVE_UP_5		
	ENDIF
	
	SWITCH( iSubStage )
		CASE INIT_VEH_RESERVATION_0 // start reservation of trash truck
			MC_playerBD[ iPartToUse ].iBinBagSyncScene = -2
			MC_playerBD[ iPartToUse ].iVehRequest = iDropOffVehID
			BROADCAST_TRASH_DRIVE_ENABLE( iDropOffVehID, iPartToUse, FALSE )
			SET_PED_MOVEMENT_CLIPSET( ped, "clipset@move@trash_fast_turn", 0.1 )
			CPRINTLN( DEBUG_SIMON, "UPDATE_BIN_BAG_DROP_OFF_ALIGNMENT - requesting veh: ", iDropOffVehID, " for part:", iPartToUse )
			iSubStage = INIT_HEADING_1
			bRecurse = TRUE
		BREAK
		CASE INIT_HEADING_1 // once the ped is slow enough if it's still in the drop off area then align ped heading
			IF( GET_ENTITY_SPEED( ped ) < 2.0 )
				IF( IS_PED_ELIGIBLE_FOR_BIN_BAG_THROW( ped, veh, sti, bRightHandedThrow ) )
					IF( NOT IS_PED_CLOSE_TO_HEADING( ped, fDepositHeading, 15.0 ) )
						TASK_TURN_PED_TO_FACE_ENTITY( ped, veh, GET_TURN_TASK_TIME( ped, fDepositHeading, 740.0 ) )
						iSubStage = UPDATE_HEADING_2
						bRecurse = TRUE
					ELSE
						TASK_TURN_PED_TO_FACE_ENTITY( ped, veh, GET_TURN_TASK_TIME( ped, fDepositHeading, 740.0 ) )
						iSubStage = UPDATE_VEH_RESERVATION_3
						bRecurse = TRUE
					ENDIF
					IF( GET_DELTA_HEADING( fDepositHeading, GET_ENTITY_HEADING( ped ) ) >= 0.0 OR iSubStage = UPDATE_VEH_RESERVATION_3 )
						SET_BIT( iFlags, F_TURNING_CLOCKWISE )
					ENDIF
				ELSE
					iSubStage = GIVE_UP_5
				ENDIF
			ENDIF
		BREAK
		CASE UPDATE_HEADING_2 // wait for heading to be achieved
			IF( IS_PED_CLOSE_TO_HEADING( ped, fDepositHeading, fHeadingTolerance ) )
				iSubStage = UPDATE_VEH_RESERVATION_3
				bRecurse = TRUE
			ELSE
				IF( NOT IS_PED_PERFORMING_TASK( ped, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY ) )
					iSubStage = INIT_HEADING_1
					bRecurse = TRUE
				ENDIF
			ENDIF
		BREAK
		CASE UPDATE_VEH_RESERVATION_3 // either give up if we haven't got veh reservation after 4s or proceed because we have it			
			iSubStage = DONE_4
			bRecurse = TRUE
		BREAK
		CASE DONE_4 // progress to next state
			SET_BIT( iFlags, F_ALIGNMENT_FINISHED )
			SET_CURRENT_SPECIAL_PICKUP_STATE( eSpecialPickupState_THROWING )
		BREAK
		CASE GIVE_UP_5 // couldn't get vehicle reservation so drop back to last state
			MC_playerBD[ iPartToUse ].iVehRequest = UNSET
			MC_playerBD[ iPartToUse ].iBinBagSyncScene = UNSET
			RESET_PED_MOVEMENT_CLIPSET( ped )
			SET_BIT( iFlags, F_ALIGNMENT_FINISHED )
			IF( NOT IS_PED_INJURED( ped ) )
				CLEAR_PED_TASKS( ped )
			ENDIF
			BROADCAST_TRASH_DRIVE_ENABLE( iDropOffVehID, iPartToUse, TRUE )
			SET_CURRENT_SPECIAL_PICKUP_STATE( eSpecialPickupState_CARRYING )
		BREAK
	ENDSWITCH
	
	IF( IS_BIT_SET( iFlags, F_ALIGNMENT_FINISHED ) )
		iStage = INIT_VEH_RESERVATION_0
	ELSE
		iStage = iFlags | iSubStage
	ENDIF
	
	IF( bRecurse )
		UPDATE_BIN_BAG_DROP_OFF_ALIGNMENT( iStage, ped, obj, veh, iDropOffVehID, sti, bRightHandedThrow )
	ENDIF
ENDPROC

//PURPOSE: Lerps along the shortest route between two float rotations in the range 0.0 to 360.0
FUNC FLOAT LERP_ROTATION_COMPONENT( FLOAT a, FLOAT b, FLOAT fAlpha )
	IF( ABSF( b - a ) > 180.0 )
		IF( b - a <= 0.0 )
			b += 360.0
		ELSE
			a += 360.0
		ENDIF
	ENDIF
	RETURN ( a + fAlpha * ( b - a ) ) % 360.0
ENDFUNC

//PURPOSE: Lerps between two rotations along the shortest route
FUNC VECTOR LERP_ROTATION( VECTOR a, VECTOR b, FLOAT fAlpha, BOOL bIgnoreZAxis = TRUE )
	VECTOR vResult
	a.x = ( a.x + 180.0 ) % 360.0
	a.y = ( a.y + 180.0 ) % 360.0
	a.z = ( a.z + 180.0 ) % 360.0
	
	b.x = ( b.x + 180.0 ) % 360.0
	b.y = ( b.y + 180.0 ) % 360.0
	b.z = ( b.z + 180.0 ) % 360.0
	
	vResult.x = LERP_ROTATION_COMPONENT( a.x, b.x, fAlpha )
	vResult.y = LERP_ROTATION_COMPONENT( a.y, b.y, fAlpha )
	IF( bIgnoreZAxis )
		vResult.z = a.z
	ELSE
		vResult.z = LERP_ROTATION_COMPONENT( a.z, b.z, fAlpha )
	ENDIF
	RETURN vResult
ENDFUNC

FUNC BOOL IS_BIN_BAG_AT_TRASH_HOPPER( OBJECT_INDEX obj, VEHICLE_INDEX veh )
	VECTOR vLeft 	= GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS( veh, <<-2.0, -3.2, -3.0>> )
	VECTOR vRight 	= GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS( veh, <<2.0, -3.2, 1.0>> )
	FLOAT  fWidth 	= 0.3
	
	RETURN IS_ENTITY_IN_ANGLED_AREA( obj, vLeft, vRight, fWidth )
ENDFUNC

FUNC STRING GET_DROP_OFF_BINBAG_ANIM_NAME( STRING sPedAnimName )
	STRING sAnim
	IF( NOT IS_STRING_NULL_OR_EMPTY( sPedAnimName ) )
		IF( ARE_STRINGS_EQUAL( sPedAnimName, "THROW_RANGED_A" ) )
			sAnim = "THROW_RANGED_A_BIN_BAG"
		ELIF( ARE_STRINGS_EQUAL( sPedAnimName, "THROW_RANGED_B" ) )
			sAnim = "THROW_RANGED_B_BIN_BAG"
		ELIF( ARE_STRINGS_EQUAL( sPedAnimName, "THROW_RANGED_C" ) )
			sAnim = "THROW_RANGED_C_BIN_BAG"
		ELIF( ARE_STRINGS_EQUAL( sPedAnimName, "THROW_RANGED_D" ) )
			sAnim = "THROW_RANGED_D_BIN_BAG"
		ELIF( ARE_STRINGS_EQUAL( sPedAnimName, "THROW_RANGED_E" ) )
			sAnim = "THROW_RANGED_E_BIN_BAG"
		ELIF( ARE_STRINGS_EQUAL( sPedAnimName, "THROW_RANGED_F" ) )
			sAnim = "THROW_RANGED_F_BIN_BAG"
		ELIF( ARE_STRINGS_EQUAL( sPedAnimName, "THROW_RANGED_G" ) )
			sAnim = "THROW_RANGED_G_BIN_BAG"
		ELIF( ARE_STRINGS_EQUAL( sPedAnimName, "THROW_RANGED_SIDE_A" ) )
			sAnim = "THROW_RANGED_SIDE_A_BIN_BAG"
		ELIF( ARE_STRINGS_EQUAL( sPedAnimName, "THROW_RANGED_SIDE_B" ) )
			sAnim = "THROW_RANGED_SIDE_B_BIN_BAG"
		ELIF( ARE_STRINGS_EQUAL( sPedAnimName, "THROW_RANGED_SIDE_C" ) )
			sAnim = "THROW_RANGED_SIDE_C_BIN_BAG"
		ELIF( ARE_STRINGS_EQUAL( sPedAnimName, "THROW_RANGED_SIDE_D" ) )
			sAnim = "THROW_RANGED_SIDE_D_BIN_BAG"
		ELIF( ARE_STRINGS_EQUAL( sPedAnimName, "THROW_RANGED_SIDE_E" ) )
			sAnim = "THROW_RANGED_SIDE_E_BIN_BAG"
		ELIF( ARE_STRINGS_EQUAL( sPedAnimName, "THROW_RANGED_SIDE_F" ) )
			sAnim = "THROW_RANGED_SIDE_F_BIN_BAG"
		ELIF( ARE_STRINGS_EQUAL( sPedAnimName, "THROW_RANGED_SIDE_G" ) )
			sAnim = "THROW_RANGED_SIDE_G_BIN_BAG"
		ENDIF
	ENDIF
	CPRINTLN( DEBUG_SIMON, "GET_DROP_OFF_BINBAG_ANIM_NAME - selected ", sAnim, " from input ", sPedAnimName )
	RETURN sAnim
ENDFUNC

FUNC BOOL SHOULD_DROP_OUT_OF_BINBAG_ANIM_EARLY()
	RETURN IS_CONTROL_PRESSED( PLAYER_CONTROL, INPUT_JUMP )
	OR  IS_CONTROL_PRESSED( PLAYER_CONTROL, INPUT_ATTACK )
	OR  IS_CONTROL_PRESSED( PLAYER_CONTROL, INPUT_AIM )
	OR	ABSF( GET_CONTROL_NORMAL( PLAYER_CONTROL, INPUT_MOVE_LR ) ) > 0.3
	OR	ABSF( GET_CONTROL_NORMAL( PLAYER_CONTROL, INPUT_MOVE_UD ) ) > 0.3
ENDFUNC

PROC UPDATE_TRASH_TRUCK_DROP_OFF( INT &iStage, PED_INDEX ped, VEHICLE_INDEX veh, OBJECT_INDEX obj, INT &iContext, 
	INT &iTimer, INT iObj, INT &iSyncScene, INT &iVehRequest, INT &iObjSyncScene, BOOL bRightHandedThrow )
	
	CONST_INT	UNSET							-1
	CONST_FLOAT QUITE_SLOW_BLEND_OUT			-6.0
	
	CONST_INT	INIT_0							0
	CONST_INT	UPDATE_1						1
	CONST_INT	DONE_2							2
	CONST_INT	GIVE_UP_3						3
	
	CONST_INT	F_BINBAG_LANDING_SOUND_PLAYED	31
	CONST_INT	F_PLAYER_DROPPED_OUT_EARLY 		30
	CONST_INT	F_IS_BINBAG_INVISIBLE			29
	CONST_INT	F_DROP_OFF_COMPLETE				28
	CONST_INT	F_WEAPON_OVERRIDEN				27
	CONST_INT	F_BINBAG_SCENE_STARTED			26
	CONST_INT	F_PLAYER_SCENE_ABORTED			25
	CONST_INT	F_PLAYER_SCENE_STARTED			24
	
	CONST_INT	FLAG_MASK						-16777216 // 8 bits
	CONST_INT	STAGE_MASK						31 // 5 bits
	
	STRING 	sAnimDict 		= "ANIM@HEISTS@NARCOTICS@TRASH"
	FLOAT 	fAnimPhase		= 1.0
	VECTOR	vDepositPos		= GET_ENTITY_COORDS( veh, FALSE )
	VECTOR 	vPedPos			= GET_ENTITY_COORDS( ped, FALSE )
	VECTOR 	vPedRot			= GET_ENTITY_ROTATION( ped )
	FLOAT 	fDepositHeading = GET_HEADING_BETWEEN_VECTORS( vPedPos, vDepositPos )
	INT		iSubStage		= iStage & STAGE_MASK
	INT		iFlags			= iStage & FLAG_MASK
	FLOAT	fBreakOutPhase	= 0.35
	FLOAT	fAnimRate		= 1.2
	INT		iLocalSyncScene
	
	IF( iSubStage >= UPDATE_1 )
		iLocalSyncScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID( iSyncScene )
		IF( IS_SYNCHRONIZED_SCENE_RUNNING( iLocalSyncScene ) )
			fAnimPhase = GET_SYNCHRONIZED_SCENE_PHASE( iLocalSyncScene )
		ELSE
			INT iLocalObjSyncScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID( iObjSyncScene )
			IF( IS_SYNCHRONIZED_SCENE_RUNNING( iLocalObjSyncScene ) )
				fAnimPhase = GET_SYNCHRONIZED_SCENE_PHASE( iLocalObjSyncScene )
			ELSE
				fAnimPhase = 0.0
			ENDIF
		ENDIF
	ENDIF

	IF( GET_FOLLOW_PED_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON )
		DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_LOOK_LR )
		DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_LOOK_UD )
	ENDIF
	
	#IF IS_DEBUG_BUILD
	STRING str = ""
	SWITCH( iSubStage )
		CASE INIT_0		str = "INIT_0"		BREAK
		CASE UPDATE_1	str = "UPDATE_1"	BREAK
		CASE DONE_2		str = "DONE_2"		BREAK
		CASE GIVE_UP_3	str = "GIVE_UP_3"	BREAK
	ENDSWITCH
	CPRINTLN( DEBUG_SIMON, "UPDATE_TRASH_TRUCK_DROP_OFF iSubStage: ", str )
	CPRINTLN( DEBUG_SIMON, "fAnimPhase: ", fAnimPhase )
	CPRINTLN( DEBUG_SIMON, "F_BINBAG_LANDING_SOUND_PLAYED:", IS_BIT_SET( iFlags, F_BINBAG_LANDING_SOUND_PLAYED ) )
	CPRINTLN( DEBUG_SIMON, "F_PLAYER_DROPPED_OUT_EARLY:", 	 IS_BIT_SET( iFlags, F_PLAYER_DROPPED_OUT_EARLY ) )
	CPRINTLN( DEBUG_SIMON, "F_IS_BINBAG_INVISIBLE:", 		 IS_BIT_SET( iFlags, F_IS_BINBAG_INVISIBLE ) )
	CPRINTLN( DEBUG_SIMON, "F_WEAPON_OVERRIDEN:", 			 IS_BIT_SET( iFlags, F_WEAPON_OVERRIDEN ) )
	CPRINTLN( DEBUG_SIMON, "F_BINBAG_SCENE_STARTED:", 		 IS_BIT_SET( iFlags, F_BINBAG_SCENE_STARTED ) )
	CPRINTLN( DEBUG_SIMON, "F_PLAYER_SCENE_ABORTED:", 		 IS_BIT_SET( iFlags, F_PLAYER_SCENE_ABORTED ) )
	CPRINTLN( DEBUG_SIMON, "F_PLAYER_SCENE_STARTED:", 		 IS_BIT_SET( iFlags, F_PLAYER_SCENE_STARTED ) )
	#ENDIF
	
	IF( NOT IS_BIT_SET( iFlags, F_PLAYER_DROPPED_OUT_EARLY ) )
		SET_PED_MAX_MOVE_BLEND_RATIO( ped, PEDMOVEBLENDRATIO_WALK )
		IF( fAnimPhase <= fBreakOutPhase )
			DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_MOVE_LR )
			DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_MOVE_UD )
		ENDIF
		DISABLE_CELLPHONE_THIS_FRAME_ONLY()
	ENDIF
	SUPPRESS_CONTEXT_BUTTON_ACTIONS_THIS_FRAME()
	
	SWITCH( iSubStage )
		CASE INIT_0
			IF( NOT SHOULD_DROP_SPECIAL_PICKUP_CHECKS( obj ) )
				IF( NOT NETWORK_HAS_CONTROL_OF_ENTITY( obj ) )
					NETWORK_REQUEST_CONTROL_OF_ENTITY( obj )
				ELSE
					SET_BINBAG_NET_ID_CAN_MIGRATE( obj, FALSE )
					SET_ENTITY_COLLISION( obj, FALSE, TRUE )
					
					sCurrentSpecialPickupState.sAnimName = GET_DROP_OFF_PED_ANIM_NAME( ped, veh, bRightHandedThrow )
					
					RAGDOLL_BLOCKING_FLAGS rbf
					rbf = RBF_BULLET_IMPACT | RBF_PLAYER_IMPACT | RBF_MELEE | RBF_RUBBER_BULLET | RBF_FALLING | RBF_DROWNING 
						| RBF_ALLOW_BLOCK_DEAD_PED | RBF_PLAYER_BUMP | RBF_PLAYER_RAGDOLL_BUMP | RBF_PED_RAGDOLL_BUMP	
					
					iSyncScene = NETWORK_CREATE_SYNCHRONISED_SCENE( vPedPos, <<vPedRot.x, vPedRot.y, fDepositHeading>>, DEFAULT, TRUE,
						DEFAULT, DEFAULT, DEFAULT, fAnimRate )
					NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE( ped, iSyncScene, sAnimDict, sCurrentSpecialPickupState.sAnimName,
						REALLY_SLOW_BLEND_IN, QUITE_SLOW_BLEND_OUT, 
						SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_ABORT_ON_DEATH, rbf, 
						NORMAL_BLEND_IN )
					NETWORK_START_SYNCHRONISED_SCENE( iSyncScene )
					MC_playerBD[ iPartToUse ].iBinBagSyncScene = iSyncScene
					iTimer = UNSET
					
					CPRINTLN( DEBUG_SIMON, "UPDATE_TRASH_TRUCK_DROP_OFF - Starting sync scene: ", sCurrentSpecialPickupState.sAnimName )
								
					iSubStage = UPDATE_1
				ENDIF
			ELSE
				// give up before the sync scene starts
				CPRINTLN( DEBUG_SIMON, "UPDATE_TRASH_TRUCK_DROP_OFF - Giving up before starting the player scene" )
				iSubStage = GIVE_UP_3
			ENDIF
		BREAK
		CASE UPDATE_1
			IF( NOT IS_BIT_SET( iFlags, F_PLAYER_SCENE_STARTED ) )
				IF( IS_SYNCHRONIZED_SCENE_RUNNING( iLocalSyncScene ) )
					SET_BIT( iFlags, F_PLAYER_SCENE_STARTED )
				ENDIF
			ENDIF
			IF( NOT IS_BIT_SET( iFlags, F_PLAYER_SCENE_ABORTED ) )
				IF( SHOULD_DROP_SPECIAL_PICKUP_CHECKS( obj )
				OR ( NOT IS_SYNCHRONIZED_SCENE_RUNNING( iLocalSyncScene ) AND IS_BIT_SET( iFlags, F_PLAYER_SCENE_STARTED ) ) )
					IF( NOT IS_BIT_SET( iFlags, F_BINBAG_SCENE_STARTED ) )
						// give up before bin bag detach
						CPRINTLN( DEBUG_SIMON, "UPDATE_TRASH_TRUCK_DROP_OFF - Giving up before starting the binbag scene" )
						iSubStage = GIVE_UP_3
					ELSE
						CPRINTLN( DEBUG_SIMON, "UPDATE_TRASH_TRUCK_DROP_OFF - Giving up after binbag scene started" )
						USE_FOOTSTEP_SCRIPT_SWEETENERS( ped, FALSE, 0 )
						IF( NOT IS_PED_INJURED( ped ) )						
							CLEAR_PED_TASKS( ped )
						ENDIF
						SET_CURRENT_PED_WEAPON( LocalPlayerPed, GET_CURRENT_SPECIAL_PICKUP_STORED_WEAPON() )
						IF( IS_SYNCHRONIZED_SCENE_RUNNING( iLocalSyncScene ) )
							NETWORK_STOP_SYNCHRONISED_SCENE( iSyncScene )
						ENDIF
						SET_BIT( iFlags, F_PLAYER_SCENE_ABORTED )
					ENDIF
				ENDIF
			ENDIF
			IF( iSubStage = UPDATE_1 )
				IF( NOT IS_BIT_SET( iFlags, F_WEAPON_OVERRIDEN ) )
					IF( NOT IS_BIT_SET( iFlags, F_PLAYER_SCENE_ABORTED ) )
						IF( fAnimPhase > 0.1 )
							SET_CURRENT_PED_WEAPON( ped, WEAPONTYPE_UNARMED, TRUE )
							SET_BIT( iFlags, F_WEAPON_OVERRIDEN )
						ENDIF
					ENDIF
				ENDIF
				
				IF( NOT IS_BIT_SET( iFlags, F_BINBAG_SCENE_STARTED ) )
					IF( fAnimPhase > fBreakOutPhase )
						DETACH_ENTITY( obj, FALSE, TRUE )
						SET_ENTITY_COLLISION( obj, FALSE, FALSE )
						iObjSyncScene = NETWORK_CREATE_SYNCHRONISED_SCENE( vPedPos, <<vPedRot.x, vPedRot.y, fDepositHeading>>, DEFAULT, TRUE, DEFAULT, DEFAULT, fAnimPhase, fAnimRate )
						NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE( obj, iObjSyncScene, sAnimDict, 
							GET_DROP_OFF_BINBAG_ANIM_NAME( sCurrentSpecialPickupState.sAnimName ), REALLY_SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT )
						NETWORK_START_SYNCHRONISED_SCENE( iObjSyncScene )
						SET_BIT( iFlags, F_BINBAG_SCENE_STARTED )
						CPRINTLN( DEBUG_SIMON, "UPDATE_TRASH_TRUCK_DROP_OFF - Started iObjSyncScene sync scene" )
					ENDIF
				ENDIF
			
				IF( NOT IS_BIT_SET( iFlags, F_PLAYER_DROPPED_OUT_EARLY ) )
					IF( NOT IS_BIT_SET( iFlags, F_PLAYER_SCENE_ABORTED ) )
						IF( fAnimPhase > fBreakOutPhase )
							IF( SHOULD_DROP_OUT_OF_BINBAG_ANIM_EARLY() )
								IF( NOT IS_PED_INJURED( ped ) )	
									CLEAR_PED_TASKS( ped )
								ENDIF
								IF( IS_SYNCHRONIZED_SCENE_RUNNING( iLocalSyncScene ) )
									NETWORK_STOP_SYNCHRONISED_SCENE( iSyncScene )
								ENDIF
								iTimer = GET_GAME_TIMER() + 500
								SET_BIT( iFlags, F_PLAYER_DROPPED_OUT_EARLY )
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF( NOT IS_BIT_SET( iFlags, F_BINBAG_LANDING_SOUND_PLAYED ) )
					IF( fAnimPhase > 0.44 )
						PLAY_SOUND_FROM_ENTITY( -1, "Trash_Bag_Land", veh, "DLC_HEIST_SERIES_A_SOUNDS", TRUE )
						SET_BIT( iFlags, F_BINBAG_LANDING_SOUND_PLAYED )
						RESET_PED_MOVEMENT_CLIPSET( ped, 1.0 )
					ENDIF
				ENDIF
				
				IF( NOT IS_BIT_SET( iFlags, F_IS_BINBAG_INVISIBLE ) )	
					IF( IS_BIN_BAG_AT_TRASH_HOPPER( obj, veh ) OR fAnimPhase > 0.9 )
						SET_ENTITY_VISIBLE( obj, FALSE )
						SET_BIT( iFlags, F_IS_BINBAG_INVISIBLE )
					ENDIF
				ENDIF
				
				IF( NOT IS_BIT_SET( iFlags, F_PLAYER_SCENE_ABORTED ) )
					IF( iTimer <> UNSET )
						IF( iTimer < GET_GAME_TIMER() )
							iTimer = UNSET
							SET_CURRENT_PED_WEAPON( LocalPlayerPed, GET_CURRENT_SPECIAL_PICKUP_STORED_WEAPON() )
						ENDIF
					ENDIF
				ENDIF
				
				IF( fAnimPhase = 1.0 )
					IF( NOT IS_BIT_SET( iFlags, F_PLAYER_DROPPED_OUT_EARLY )
					AND NOT IS_BIT_SET( iFlags, F_PLAYER_SCENE_ABORTED ) )
						IF( NOT IS_PED_INJURED( ped ) ) 
							CLEAR_PED_TASKS( ped )
						ENDIF
						IF( IS_SYNCHRONIZED_SCENE_RUNNING( iLocalSyncScene ) )
							NETWORK_STOP_SYNCHRONISED_SCENE( iSyncScene )
						ENDIF
					ENDIF
					NETWORK_STOP_SYNCHRONISED_SCENE( iObjSyncScene )
					CPRINTLN( DEBUG_SIMON, "UPDATE_TRASH_TRUCK_DROP_OFF - anim has finished, fAnimPhase:", fAnimPhase )
					iSubStage = DONE_2
				ENDIF
			ENDIF
		BREAK
		CASE DONE_2
			IF( NOT IS_BIT_SET( iFlags, F_PLAYER_SCENE_ABORTED ) )
				USE_FOOTSTEP_SCRIPT_SWEETENERS( ped, FALSE, 0 )
				SET_CURRENT_PED_WEAPON( LocalPlayerPed, GET_CURRENT_SPECIAL_PICKUP_STORED_WEAPON() )
			ENDIF
			iTimer = GET_GAME_TIMER() + 800
			IF( iContext <> -1 )
			 	RELEASE_CONTEXT_INTENTION( iContext )
			ENDIF
			SET_BIT( iFlags, F_DROP_OFF_COMPLETE )
			MC_playerBD[ iPartToUse ].iBinBagSyncScene = UNSET
			MC_playerBD[ iPartToUse ].iVehRequest = UNSET
			BROADCAST_TRASH_DRIVE_ENABLE( iVehRequest, iPartToUse, TRUE )
			sCurrentSpecialPickupState.vPlayerTargetPos = <<0.0, 0.0, 0.0>>
			sCurrentSpecialPickupState.iPosition = -1							
			SET_BIT( iObjDeliveredBitset, iObj )
			SET_CURRENT_SPECIAL_PICKUP_STATE( eSpecialPickupState_WAIT_FOR_DELIVERY )
		BREAK
		CASE GIVE_UP_3
			IF( IS_SYNCHRONIZED_SCENE_RUNNING( iLocalSyncScene ) )
				NETWORK_STOP_SYNCHRONISED_SCENE( iSyncScene )
			ENDIF
			RESET_PED_MOVEMENT_CLIPSET( ped, 0.5 )
			USE_FOOTSTEP_SCRIPT_SWEETENERS( ped, FALSE, 0 )
			SET_BIT( iFlags, F_DROP_OFF_COMPLETE )
			MC_playerBD[ iPartToUse ].iBinBagSyncScene = UNSET
			MC_playerBD[ iPartToUse ].iVehRequest = UNSET
			BROADCAST_TRASH_DRIVE_ENABLE( iVehRequest, iPartToUse, TRUE )
			sCurrentSpecialPickupState.vPlayerTargetPos = <<0.0, 0.0, 0.0>>
			sCurrentSpecialPickupState.iPosition = -1
			BROADCAST_BINBAG_REQUEST( iObj, iPartToUse, TRUE )
			SET_BINBAG_NET_ID_CAN_MIGRATE( obj, TRUE )
			RESET_SPECIAL_PICKUP()
			SET_CURRENT_SPECIAL_PICKUP_STATE( eSpecialPickupState_BEFORE_PICKUP )
		BREAK
	ENDSWITCH

	IF( IS_BIT_SET( iFlags, F_DROP_OFF_COMPLETE ) )
		iStage = INIT_0
	ELSE
		iStage = iFlags | iSubStage
	ENDIF
ENDPROC

PROC GET_PLAYER_HEADING_ALIGNED_TO_BIN_BAG( PED_INDEX ped, OBJECT_INDEX obj, FLOAT &fHeading )
	VECTOR vPedPos 		= GET_ENTITY_COORDS( ped, FALSE )
	VECTOR vObjPos 		= GET_ENTITY_COORDS( obj, FALSE )
	FLOAT fPedHeading	= GET_ENTITY_HEADING( ped )
	FLOAT fFacingHeading = GET_HEADING_BETWEEN_VECTORS( vPedPos, vObjPos )
	FLOAT fDeltaHeading = GET_DELTA_HEADING( fFacingHeading, fPedHeading )
	
	IF( fDeltaHeading >= -22.5 AND fDeltaHeading <= 22.5 ) // pickup
		fHeading = fFacingHeading
		sCurrentSpecialPickupState.sAnimName = "pickup"		
	ELIF( fDeltaHeading >= -67.5 AND fDeltaHeading <= -22.5 ) // pickup_45_r
		fHeading = fFacingHeading - 45.0
		sCurrentSpecialPickupState.sAnimName = "pickup_45_l"
	ELIF( fDeltaHeading >= -180.0 AND fDeltaHeading <= -67.5 ) // pickup_90_r
		fHeading = fFacingHeading - 90.0
		sCurrentSpecialPickupState.sAnimName = "pickup_90_l"
	ELIF( fDeltaHeading >= 22.5 AND fDeltaHeading <= 67.5 ) // pickup_45_l
		fHeading = fFacingHeading + 45.0
		sCurrentSpecialPickupState.sAnimName = "pickup_45_r"
	ELIF( fDeltaHeading >= 67.5 AND fDeltaHeading <= 180.0 ) // pickup_90_l
		fHeading = fFacingHeading + 90.0
		sCurrentSpecialPickupState.sAnimName = "pickup_90_r"
	ENDIF
	fHeading += 5.15 // offset to align hand with bin bags
	fHeading = GET_NORMALISED_HEADING( fHeading )
ENDPROC

FUNC BOOL IS_NEARBY_PED_AT_POSITION( PED_INDEX ped, VECTOR vPos, FLOAT fRange )
	PED_INDEX peds[ 4 ]
	INT iCount = GET_PED_NEARBY_PEDS( ped, peds )
	FLOAT fRange2 = fRange * fRange
	INT i
	REPEAT iCount i
		IF( IS_ENTITY_ALIVE( peds[ i ] ) )
			VECTOR vPedPos = GET_ENTITY_COORDS( peds[ i ], FALSE )
			IF( VDIST2( vPos, vPedPos ) <= fRange2 )
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

ENUM POSITION_RESULT
	PR_SEARCHING,
	PR_FOUND,
	PR_NOT_FOUND
ENDENUM

FUNC POSITION_RESULT GET_PLAYER_POSITION_ALIGNED_TO_BIN_BAG( PED_INDEX ped, OBJECT_INDEX obj, VECTOR &vPos, SHAPETEST_INDEX &sti, 
	INT &iCheckIndex, BOOL bClosestOnly = FALSE )
	VECTOR vPedPos 			= GET_ENTITY_COORDS( ped, FALSE )
	VECTOR vObjPos 			= GET_ENTITY_COORDS( obj, FALSE )
	FLOAT  fPedRadius 		= 0.4
	INT	   iInclude			= SCRIPT_INCLUDE_MOVER | SCRIPT_INCLUDE_VEHICLE | SCRIPT_INCLUDE_OBJECT | SCRIPT_INCLUDE_PICKUP
	
	INT    iBlockingFlags	= SCRIPT_INCLUDE_GLASS | SCRIPT_INCLUDE_OBJECT | SCRIPT_INCLUDE_MOVER
	VECTOR vObjToPlayer
	FLOAT  fWaterHeight
	
	INT 				iHitSomething
	VECTOR 				vTestPos
	VECTOR				vNormal
	ENTITY_INDEX 		entity
	SHAPETEST_STATUS	status
	
	status = GET_SHAPE_TEST_RESULT( sti, iHitSomething, vTestPos, vNormal, entity )
	
	IF( status = SHAPETEST_STATUS_NONEXISTENT OR IS_VECTOR_ZERO( vPos ) )
		vObjPos.z 		= vPedPos.z
		vObjToPlayer 	= NORMALISE_VECTOR( vPedPos - vObjPos )
		vObjToPlayer 	*= 0.64 // 0.7384252
		vPos 			= vObjPos + vObjToPlayer
	
		IF( iCheckIndex > -1 )
			IF( iCheckIndex < 11 )
				INT m = 0
				IF( iCheckIndex % 2 = 0 )
					m = ( iCheckIndex / 2 ) + 1
				ELSE
					m = 11 - ( iCheckIndex / 2 )
				ENDIF
				vPos = vObjToPlayer
				RotateVec( vPos, <<0.0, 0.0, m * 30.0>> )
				vPos += vObjPos
			ELSE
				iCheckIndex = -1
				RETURN PR_NOT_FOUND
			ENDIF
		ENDIF
		sti = START_SHAPE_TEST_CAPSULE( vPos - <<0.0, 0.0, 0.5>>, vPos + <<0.0, 0.0, 0.5>>, fPedRadius, iInclude, obj )
	
	ELIF( status = SHAPETEST_STATUS_RESULTS_READY )
		IF( NOT IS_VECTOR_ZERO( vPos ) )
			BOOL bIsSpaceClear = FALSE
			IF( iHitSomething <> 1 )
				bIsSpaceClear = TRUE
			ELSE
				IF( IS_ENTITY_A_PED( entity ) )
					PED_INDEX pedTest
					IF( pedTest = ped OR IS_PED_DEAD_OR_DYING( pedTest ) )
						bIsSpaceClear = TRUE
					ENDIF
				ENDIF
			ENDIF
			IF( bIsSpaceClear )
				IF( NOT IS_NEARBY_PED_AT_POSITION( ped, vPos, fPedRadius )
				AND TEST_VERTICAL_PROBE_AGAINST_ALL_WATER( vPos, iBlockingFlags, fWaterHeight ) <> SCRIPT_WATER_TEST_RESULT_WATER )					
					iCheckIndex = -1
					RETURN PR_FOUND
				ENDIF
			ENDIF
			IF( NOT bClosestOnly )
				iCheckIndex++
			ENDIF
		ENDIF
	ENDIF
	
	RETURN PR_SEARCHING
ENDFUNC

PROC UPDATE_BIN_BAG_PICKUP_ALIGNMENT( INT &iStage, PED_INDEX ped, OBJECT_INDEX obj, VECTOR &vPedTargetPos, VECTOR &vPedTestPos,
	FLOAT &fPedTargetHeading, SHAPETEST_INDEX &sti, INT &iPosition, INT iCurrentlyRequestedObj, INT &iCurrentlyOwnedBinbag, INT &iTimer )

	CONST_INT		UNSET									-1

	CONST_INT		INIT_0									0
	CONST_INT		INIT_MOVE_1								1
	CONST_INT		UPDATE_MOVE_2							2
	CONST_INT		INIT_HEADING_3							3
	CONST_INT		UPDATE_HEADING_4						4
	CONST_INT		WAITING_FOR_OBJ_REQUEST_TO_COMPLETE_5	5
	CONST_INT		DONE_6									6
	CONST_INT		GIVE_UP_7								7
	CONST_INT		TURN_TO_FACE_OBJECT_8					8
	
	CONST_FLOAT		DIST2_TOLERANCE							400.0
	
	VECTOR 			vPedPos 			= GET_ENTITY_COORDS( ped, FALSE )
	VECTOR 			vObjPos				= GET_ENTITY_COORDS( obj, FALSE )
	VECTOR			vPedForward			= GET_ENTITY_FORWARD_VECTOR( ped )
	VECTOR			vPedToObj			= NORM_VEC( vObjPos - vPedPos )
	FLOAT			fDist2PedTarget 	= VDIST2( vPedPos, vPedTargetPos )
	FLOAT			fRadius2			= 0.4 * 0.4
	BOOL			bRecurse			= FALSE
	
	POSITION_RESULT	result

	SET_PED_MAX_MOVE_BLEND_RATIO( ped, PEDMOVEBLENDRATIO_WALK )	
	DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_MOVE_LR )
	DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_MOVE_UD )
	DISABLE_CELLPHONE_THIS_FRAME_ONLY()
	SUPPRESS_CONTEXT_BUTTON_ACTIONS_THIS_FRAME()
	
	#IF IS_DEBUG_BUILD
	STRING str = ""
	SWITCH( iStage )
		CASE INIT_0									str = "INIT_0"									BREAK
		CASE INIT_MOVE_1							str = "INIT_MOVE_1"								BREAK
		CASE UPDATE_MOVE_2							str = "UPDATE_MOVE_2"							BREAK
		CASE INIT_HEADING_3							str = "INIT_HEADING_3"							BREAK
		CASE UPDATE_HEADING_4						str = "UPDATE_HEADING_4"						BREAK
		CASE WAITING_FOR_OBJ_REQUEST_TO_COMPLETE_5	str = "WAITING_FOR_OBJ_REQUEST_TO_COMPLETE_5"	BREAK
		CASE DONE_6									str = "DONE_6"									BREAK
		CASE GIVE_UP_7								str = "GIVE_UP_7"								BREAK
		CASE TURN_TO_FACE_OBJECT_8					str = "TURN_TO_FACE_OBJECT_8"								BREAK
	ENDSWITCH
	CPRINTLN( DEBUG_SIMON, "UPDATE_BIN_BAG_PICKUP_ALIGNMENT iStage: ", str )
	#ENDIF
	
	NETWORK_REQUEST_CONTROL_OF_ENTITY( obj )
	
	IF( iCurrentlyOwnedBinbag = -2 ) // failed to get ownership of the bin bag so giving up
		iStage = GIVE_UP_7
		CPRINTLN( DEBUG_SIMON, "UPDATE_BIN_BAG_PICKUP_ALIGNMENT - Failed to get ownership of object for iPart:", iPartToUse, " so giving up" )
	ENDIF
	
	SWITCH( iStage )
		CASE INIT_0 // once ped is stopped get the pickup position and heading
			result = GET_PLAYER_POSITION_ALIGNED_TO_BIN_BAG( ped, obj, vPedTargetPos, sti, iPosition )
			IF( result = PR_FOUND )
				IF( VDIST2( vPedPos, vPedTargetPos ) < DIST2_TOLERANCE )
					IF( GET_ENTITY_SPEED( ped ) < 2.0 )
						IF( DOT_PRODUCT( vPedForward, vPedToObj ) < -0.75
						AND VDIST2( vObjPos, vPedPos ) > 4.0 )
							TASK_TURN_PED_TO_FACE_ENTITY( ped, obj )
							iStage = TURN_TO_FACE_OBJECT_8
						ELSE
							iStage = INIT_MOVE_1
							iTimer = GET_GAME_TIMER() + 8000
							bRecurse = TRUE
						ENDIF
					ENDIF
				ELSE
					CPRINTLN( DEBUG_SIMON, "UPDATE_BIN_BAG_PICKUP_ALIGNMENT - Giving up because vPedTargetPos > 20.0m away" )
					iStage = GIVE_UP_7
					bRecurse = TRUE
				ENDIF
			ELIF( result = PR_NOT_FOUND )
				CPRINTLN( DEBUG_SIMON, "UPDATE_BIN_BAG_PICKUP_ALIGNMENT - Giving up because no eligible pickup position found" )
				iStage = GIVE_UP_7
				bRecurse = TRUE
			ENDIF
		BREAK
		CASE INIT_MOVE_1 // if not near target position task to move there	
			IF( fDist2PedTarget >= DIST2_TOLERANCE ) // bin bag is too far away to continue
				iStage = GIVE_UP_7
				bRecurse = TRUE
			ELIF( fDist2PedTarget > fRadius2 )
				TASK_FOLLOW_NAV_MESH_TO_COORD( ped, vPedTargetPos, 1.75, 6000, DEFAULT_NAVMESH_RADIUS, 
					ENAV_DEFAULT )
				iStage = UPDATE_MOVE_2
				bRecurse = TRUE
			ELSE
				iStage = INIT_HEADING_3
				bRecurse = TRUE
			ENDIF
		BREAK
		CASE UPDATE_MOVE_2 // check if a route has been found or move has finished
			IF( GET_NAVMESH_ROUTE_RESULT( ped ) = NAVMESHROUTE_ROUTE_NOT_FOUND
			OR  iTimer < GET_GAME_TIMER() )
				iStage = GIVE_UP_7
				bRecurse = TRUE
			ELSE
				IF( NOT IS_PED_PERFORMING_TASK( ped, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD )
				OR  fDist2PedTarget <= fRadius2 )
					iStage = INIT_HEADING_3
					bRecurse = TRUE
				ELSE
					IF( GET_PLAYER_POSITION_ALIGNED_TO_BIN_BAG( ped, obj, vPedTestPos, sti, iPosition, TRUE ) = PR_FOUND )
						IF( NOT ARE_VECTORS_ALMOST_EQUAL( vPedTestPos, vPedTargetPos, 0.2 ) ) // the closest pickup position has changed
							iStage = INIT_MOVE_1
							vPedTargetPos = vPedTestPos
							bRecurse = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE INIT_HEADING_3 // if the ped isn't facing the bin bag then task to turn
			GET_PLAYER_HEADING_ALIGNED_TO_BIN_BAG( ped, obj, fPedTargetHeading )
			IF( NOT IS_PED_CLOSE_TO_HEADING( ped, fPedTargetHeading, 30.0 ) )
				TASK_TURN_PED_TO_FACE_COORD( ped, GET_COORD_ALONG_HEADING_FROM_ENTITY( ped, fPedTargetHeading ), 
					GET_TURN_TASK_TIME( ped, fPedTargetHeading, 45.0 ) )
				iStage = UPDATE_HEADING_4
				bRecurse = TRUE
			ELSE
				iStage = WAITING_FOR_OBJ_REQUEST_TO_COMPLETE_5
				bRecurse = TRUE
			ENDIF
		BREAK
		CASE UPDATE_HEADING_4 // if the ped's turning task has finished
			result = GET_PLAYER_POSITION_ALIGNED_TO_BIN_BAG( ped, obj, vPedTestPos, sti, iPosition, TRUE )
			IF( result = PR_FOUND )
				IF( NOT ARE_VECTORS_ALMOST_EQUAL( vPedTestPos, vPedTargetPos, 0.2 ) ) // the closest pickup position has changed
					iStage = INIT_MOVE_1
					vPedTargetPos = vPedTestPos
					bRecurse = TRUE
				ELSE
					IF( NOT IS_PED_PERFORMING_TASK( ped, SCRIPT_TASK_TURN_PED_TO_FACE_COORD ) 
					OR  IS_PED_CLOSE_TO_HEADING( ped, fPedTargetHeading, 30.0 ) )
						iStage = WAITING_FOR_OBJ_REQUEST_TO_COMPLETE_5
						bRecurse = TRUE
					ENDIF
				ENDIF
			ELIF( result = PR_SEARCHING )
				IF( NOT IS_PED_PERFORMING_TASK( ped, SCRIPT_TASK_TURN_PED_TO_FACE_COORD ) 
				OR  IS_PED_CLOSE_TO_HEADING( ped, fPedTargetHeading, 30.0 ) )
					iStage = WAITING_FOR_OBJ_REQUEST_TO_COMPLETE_5
					bRecurse = TRUE
				ENDIF
			ELIF( result = PR_NOT_FOUND )
				iStage = GIVE_UP_7
				bRecurse = TRUE
			ENDIF
		BREAK
		CASE WAITING_FOR_OBJ_REQUEST_TO_COMPLETE_5
			CPRINTLN( DEBUG_SIMON, "WAITING_FOR_OBJ_REQUEST_TO_COMPLETE_5 - iCurrentlyRequestedObj:", iCurrentlyRequestedObj, 
				", iCurrentlyOwnedBinbag:", iCurrentlyOwnedBinbag, ", NETWORK_HAS_CONTROL_OF_ENTITY( obj ):", NETWORK_HAS_CONTROL_OF_ENTITY( obj ) )
			IF( iCurrentlyRequestedObj = iCurrentlyOwnedBinbag ) // if the locally selected pickup has updated on the server
				IF( NETWORK_HAS_CONTROL_OF_ENTITY( obj ) )
					CPRINTLN( DEBUG_SIMON, "UPDATE_BIN_BAG_PICKUP_ALIGNMENT - Starting pickup anim for object ", iCurrentlyOwnedBinbag )
					SET_CURRENT_SPECIAL_PICKUP_STORED_WEAPON( GET_CURRENT_PLAYER_WEAPON_TYPE() )
					SET_CURRENT_PED_WEAPON( ped, WEAPONTYPE_UNARMED, TRUE )
					SET_PED_CONFIG_FLAG( ped, PCF_DisableLadderClimbing, TRUE )	
					SET_PED_USING_ACTION_MODE( ped, FALSE )
					SET_BINBAG_NET_ID_CAN_MIGRATE( obj, FALSE )
					CPRINTLN( DEBUG_SIMON, "UPDATE_BIN_BAG_PICKUP_ALIGNMENT - PCF_DisableLadderClimbing DISABLED" )
					
					iStage = DONE_6
					bRecurse = TRUE
				ENDIF
			ELIF( iCurrentlyOwnedBinbag = -2 ) // someone else has picked up the object
				CPRINTLN( DEBUG_SIMON, "UPDATE_BIN_BAG_PICKUP_ALIGNMENT - Someone else started using object ", iCurrentlyRequestedObj )
				iCurrentlyOwnedBinbag = -1
				iStage = GIVE_UP_7
				bRecurse = TRUE
			ENDIF
		BREAK
		CASE DONE_6 // if the ped has stopped we're done
			IF( CLEAR_ROLLING_CHECK_SHAPETEST( sti ) )
				iStage = INIT_0
				SET_CURRENT_SPECIAL_PICKUP_STATE( eSpecialPickupState_ATTACH_OBJECT_TO_PED )
			ENDIF
		BREAK
		CASE GIVE_UP_7 // drop back to seeking a bin bag to pickup if failed to orientate for this one
			sCurrentSpecialPickupState.vPlayerTargetPos = <<0.0, 0.0, 0.0>>
			sCurrentSpecialPickupState.iPosition = UNSET
			MC_playerBD[ iPartToUse ].iObjHacking = UNSET
			BROADCAST_BINBAG_REQUEST( iCurrentlyRequestedObj, iPartToUse, TRUE )
			obj = NULL
			CLEAR_PED_TASKS( ped )
			iStage = INIT_0
			SET_CURRENT_SPECIAL_PICKUP_STATE( eSpecialPickupState_BEFORE_PICKUP )
		BREAK
		CASE TURN_TO_FACE_OBJECT_8
			IF( NOT IS_PED_PERFORMING_TASK( ped, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY ) )
				IF( DOT_PRODUCT( vPedForward, vPedToObj ) > 0.75 )
					iStage = INIT_0
				ELSE
					iStage = GIVE_UP_7
					bRecurse = TRUE
				ENDIF
			ELSE
				IF( DOT_PRODUCT( vPedForward, vPedToObj ) > 0.75 )
					iStage = INIT_0
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	IF( bRecurse )
		UPDATE_BIN_BAG_PICKUP_ALIGNMENT( iStage, ped, obj, vPedTargetPos, vPedTestPos, fPedTargetHeading,
			sti, iPosition, iCurrentlyRequestedObj, iCurrentlyOwnedBinbag, iTimer )
	ENDIF
ENDPROC

FUNC BOOL IS_THIS_SPECIAL_PICKUP_A_BINBAG( INT iPickupIndex )
	RETURN g_FMMC_STRUCT_ENTITIES.sPlacedObject[ iPickupIndex ].mn = HEI_PROP_HEIST_BINBAG
ENDFUNC

FUNC BOOL IS_BIN_BAG_UPRIGHT( OBJECT_INDEX obj )
	VECTOR vObjRot = GET_ENTITY_ROTATION( obj )
	vObjRot.z = 0.0
	RETURN VDIST( vObjRot, <<0.0, 0.0, 0.0>> ) < 5.0
ENDFUNC

FUNC BOOL IS_OBJECT_DIRECTLY_ABOVE_GROUND( OBJECT_INDEX obj, SHAPETEST_INDEX &sti )
	VECTOR 				vObjPos 		= GET_ENTITY_COORDS( obj, FALSE )
	VECTOR				vTarget			= vObjPos + <<0.0, 0.0, -99.0>>
	INT					iLOSInclude 	= SCRIPT_INCLUDE_MOVER | SCRIPT_INCLUDE_VEHICLE | SCRIPT_INCLUDE_PED | SCRIPT_INCLUDE_RAGDOLL | SCRIPT_INCLUDE_OBJECT | SCRIPT_INCLUDE_GLASS
	INT 				iHitSomething
	VECTOR 				vPos
	VECTOR				vNormal
	ENTITY_INDEX 		entity
	SHAPETEST_STATUS	status
	
	status = GET_SHAPE_TEST_RESULT( sti, iHitSomething, vPos, vNormal, entity )
	IF( status = SHAPETEST_STATUS_RESULTS_READY )
		IF( NOT DOES_ENTITY_EXIST( entity ) )
			RETURN TRUE
		ELSE
			IF( NOT IS_ENTITY_AN_OBJECT( entity )
			AND NOT IS_ENTITY_A_VEHICLE( entity )
			AND NOT IS_ENTITY_A_PED( entity ) )
				RETURN TRUE
			ENDIF
		ENDIF
	ELIF( status = SHAPETEST_STATUS_NONEXISTENT )
		sti = START_SHAPE_TEST_LOS_PROBE( vObjPos, vTarget, iLOSInclude, obj )
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_BINBAG_UNFREEZEABLE_VALUE( INT iObj )
	INT iStart 	= iObj * 4
	INT iEnd 	= iStart + 3
	INT iIndex 	= iStart / 32
		iStart 	= iStart % 32
		iEnd 	= iEnd % 32
	RETURN GET_BITS_IN_RANGE( iFreezeBinbagTracker[ iIndex ], iStart, iEnd )
ENDFUNC

PROC SET_BINBAG_UNFREEZABLE_VALUE( INT iObj, INT iValue )
	INT iStart 	= iObj * 4
	INT iEnd 	= iStart + 3
	INT iIndex 	= iStart / 32
		iStart 	= iStart % 32
		iEnd 	= iEnd % 32
	SET_BITS_IN_RANGE( iFreezeBinbagTracker[ iIndex ], iStart, iEnd, iValue )
ENDPROC

// sets a bit range for an object index that will tick down each frame, once the counter has hit zero objects can be frozen again
PROC SET_BINBAG_UNFREEZABLE_FOR_SHORT_TIME( INT iObj )
	IF( iObj >= 0 AND iObj < MC_serverBD.iNumObjCreated )
		#IF IS_DEBUG_BUILD
		IF( ( iObj + 1 ) * 4 <= 64 )
		#ENDIF
		SET_BINBAG_UNFREEZABLE_VALUE( iObj, MC_serverBD.iNumObjCreated )
		#IF IS_DEBUG_BUILD
		ELSE
			ASSERTLN( "SET_BINBAG_UNFREEZABLE_FOR_SHORT_TIME - iObj: ", iObj, " exceeds capacity of bit range!" )
		ENDIF
		#ENDIF
	ELSE
		SCRIPT_ASSERT( "SET_BINBAG_UNFREEZABLE_FOR_SHORT_TIME - iObj value out of range!" )
	ENDIF
ENDPROC

PROC UPDATE_BINBAG_UNFREEZABLE()
	INT i
	REPEAT MC_serverBD.iNumObjCreated i
		INT iValue = GET_BINBAG_UNFREEZEABLE_VALUE( i )
		IF( iValue > 0 )
			SET_BINBAG_UNFREEZABLE_VALUE( i, iValue - 1 )
		ENDIF
	ENDREPEAT
ENDPROC

FUNC BOOL IS_BINBAG_UNFREEZABLE( INT iObj )
	RETURN GET_BINBAG_UNFREEZEABLE_VALUE( iObj ) > 0
ENDFUNC

PROC UPDATE_BIN_BAG_CARRYING( PED_INDEX ped, VEHICLE_INDEX veh, SHAPETEST_INDEX &sti, INT &iContext, BOOL &bRightHandedThrow )
	SET_PED_CAPSULE( ped, 0.7 )
	
	BOOL bIsEligible = IS_PED_ELIGIBLE_FOR_BIN_BAG_THROW( ped, veh, sti, bRightHandedThrow )
	BOOL bWaitExpired = ( sCurrentSpecialPickupState.iTimer < GET_GAME_TIMER() )
	
	IF( iContext = -1 )
		IF( bIsEligible OR bWaitExpired )
			REGISTER_CONTEXT_INTENTION( iContext, CP_MAXIMUM_PRIORITY, "", TRUE )
		ENDIF
	ENDIF
	
	IF( bIsEligible ) // check if the ped meets the conditions to deposit the bin bag
		SET_BIN_BAG_HELP_TEXT_BIT( BINBAGHELP_THROW )
		SET_PED_MAX_MOVE_BLEND_RATIO( ped, PEDMOVEBLENDRATIO_WALK )
		
		IF( HAS_CONTEXT_BUTTON_TRIGGERED( sCurrentSpecialPickupState.iContext, FALSE ) )
			DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_MOVE_LR )
			DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_MOVE_UD )
			RELEASE_CONTEXT_INTENTION( iContext )
			SET_CURRENT_SPECIAL_PICKUP_STATE( eSpecialPickupState_ALIGNING_FOR_THROW )
		ENDIF
	
	ELIF( bWaitExpired ) // if we detect the player wanting to put the bag down away from the lorry, drop it	
		IF( HAS_CONTEXT_BUTTON_TRIGGERED( iContext, FALSE ) )
			RELEASE_CONTEXT_INTENTION( iContext )
			SET_CURRENT_SPECIAL_PICKUP_STATE( eSpecialPickupState_PUTTING_DOWN )
		ELSE
			SET_BIN_BAG_HELP_TEXT_BIT( BINBAGHELP_DROP )
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL HAS_PED_GOT_BINBAG_WEAPON_EQUIPPED( PED_INDEX ped )
	IF( IS_ENTITY_ALIVE( ped ) )
		WEAPON_TYPE wt
		GET_CURRENT_PED_WEAPON( ped, wt )
		RETURN ( wt = WEAPONTYPE_DLC_GARBAGEBAG )
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC CLEAR_FOOTSTEP_SWEETENERS_FOR_ALL_PLAYER_PEDS(BOOL bIgnoreCheck = FALSE)
	
	IF !bIgnoreCheck
		IF sCurrentSpecialPickupState.iSweetenerEnabled = 0
			EXIT
		ENDIF
	ENDIF
	
	INT i
	FOR i = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
		
		IF !bIgnoreCheck
			IF NOT IS_BIT_SET(sCurrentSpecialPickupState.iSweetenerEnabled, i)
				RELOOP
			ENDIF
		ENDIF
		
		PED_INDEX ped = GET_PLAYER_PED(INT_TO_PLAYERINDEX(i))
		IF(IS_ENTITY_ALIVE(ped))
			USE_FOOTSTEP_SCRIPT_SWEETENERS(ped, FALSE, 0)
			CLEAR_BIT(sCurrentSpecialPickupState.iSweetenerEnabled, i)
		ENDIF
	ENDFOR
ENDPROC

PROC UPDATE_BIN_BAG_FOOTSTEP_SWEETENERS( INT &iSweetenerEnabled, OBJECT_INDEX obj )
	CONST_INT 	UNSET	-1
	
	BOOL bIsLocalPlayerHoldingBinbag = DOES_ENTITY_EXIST( obj ) AND IS_ENTITY_ATTACHED_TO_ANY_PED( obj )

	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		IF( MC_serverBD.iBinbagOwners[ i ] <> UNSET ) // this ped has a bin bag
			IF( NOT IS_BIT_SET( iSweetenerEnabled, i ) ) // the sweetener isn't already enabled
				PLAYER_INDEX 	player 	= INT_TO_PLAYERINDEX( i )
				PED_INDEX 		ped 	= GET_PLAYER_PED( player )
				IF( HAS_PED_GOT_BINBAG_WEAPON_EQUIPPED( ped ) )
					IF( IS_ENTITY_ALIVE( ped ) )
						CPRINTLN( DEBUG_SIMON, "UPDATE_BIN_BAG_FOOTSTEP_SWEETENERS - Enabled" )
						USE_FOOTSTEP_SCRIPT_SWEETENERS( ped, TRUE, HASH( "BIN_BAG" ) )
						SET_BIT( iSweetenerEnabled, i )
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		IF( MC_serverBD.iBinbagOwners[ i ] = UNSET // this ped doesn't have a bin bag
		OR( i = iPartToUse AND NOT bIsLocalPlayerHoldingBinbag ) )
			IF( IS_BIT_SET( iSweetenerEnabled, i ) )
				PLAYER_INDEX 	player 	= INT_TO_PLAYERINDEX( i )
				PED_INDEX 		ped 	= GET_PLAYER_PED( player )
				IF( IS_ENTITY_ALIVE( ped ) )
					CPRINTLN( DEBUG_SIMON, "UPDATE_BIN_BAG_FOOTSTEP_SWEETENERS - Disabled" )
					USE_FOOTSTEP_SCRIPT_SWEETENERS( ped, FALSE, 0 )
					CLEAR_BIT( iSweetenerEnabled, i )
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

FUNC BOOL IS_PED_ABOVE_BIN_BAG( PED_INDEX ped, OBJECT_INDEX obj )
	VECTOR vObjCenterPos 	= GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS( obj, <<0.0, 0.0, -0.3>> )
	RETURN IS_ENTITY_IN_AREA( ped, vObjCenterPos - <<0.35, 0.35, -0.9>>, vObjCenterPos + <<0.35, 0.35, 2.0>> )
ENDFUNC

FUNC BOOL ARE_ROTATION_VECTORS_EQUAL( VECTOR v1, VECTOR v2, FLOAT fTolerance = 1.0, BOOL bIgnoreZ = FALSE )
	IF( GET_ABSOLUTE_DELTA_HEADING( v1.x, v2.x ) <= fTolerance )
		IF( GET_ABSOLUTE_DELTA_HEADING( v1.y, v2.y ) <= fTolerance )
			IF( NOT bIgnoreZ )
				IF( GET_ABSOLUTE_DELTA_HEADING( v1.z, v2.z ) > fTolerance )
					RETURN TRUE
				ENDIF
			ELSE
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

//PURPOSE: Handles the righting and freezeing of bin bag special pickup objects
PROC HANDLE_BINBAG_ORIENTATION( INT index, BOOL bCorrectRotation, BOOL bFreezeWhenStopped, BOOL bForceUpright = FALSE )
	OBJECT_INDEX obj
	IF( GET_MISSION_OBJECT( index, obj ) )
		IF( NETWORK_HAS_CONTROL_OF_ENTITY( obj ) )
			IF( GET_ENTITY_SPEED( obj ) = 0.0 )
				IF( NOT IS_ENTITY_IN_WATER( obj ) )
					BOOL	bIsAttached		= IS_ENTITY_ATTACHED_TO_ANY_PED( obj )
					VECTOR 	vRot 			= GET_ENTITY_ROTATION( obj )
					VECTOR 	vPos			= GET_ENTITY_COORDS( obj )
					FLOAT  	fBinBagHeight	= 0.64
					VECTOR  vPosBottom		= GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS( obj, <<0.0, 0.0, -fBinBagHeight>> )
					FLOAT	fSpeed			= GET_ENTITY_SPEED( obj )
					VECTOR 	vZero 			= <<0.0, 0.0, 0.0>>
					BOOL	bIsUpright		= ARE_ROTATION_VECTORS_EQUAL( vRot, <<180.0, 180.0, 0.0>>, 5.0, TRUE )
					FLOAT	fInterpSpeed	= 3.0
					FLOAT	fZDiff			= vPos.z - vPosBottom.z
					
					IF( NOT bForceUpright )
						SWITCH( sCurrentSpecialPickupState.eBinBagStage[ index ] )
							CASE BBS_DORMANT
								IF( NOT bIsAttached )
									IF( NOT bFreezeWhenStopped )
										IF( NOT bIsUpright AND bCorrectRotation )
											sCurrentSpecialPickupState.eBinBagStage[ index ] = BBS_INIT
										ENDIF
									ELSE
										IF( fSpeed < 0.1 
										AND IS_OBJECT_DIRECTLY_ABOVE_GROUND( obj, sCurrentSpecialPickupState.stiGroundCheck[ index ] ) )
											IF( NOT IS_BINBAG_UNFREEZABLE( index ) )
												FREEZE_ENTITY_POSITION( obj, TRUE )
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							BREAK
							CASE BBS_INIT
								IF( fZDiff > 0.4 )
									sCurrentSpecialPickupState.fGroundHeightForPickup = vPosBottom.z
									sCurrentSpecialPickupState.eBinBagStage[ index ] = BBS_ORIENTING
								ELSE
									sCurrentSpecialPickupState.sti = START_SHAPE_TEST_LOS_PROBE( vPos, vPos - <<0.0, 9999.0, 0.0>>, 
										SCRIPT_INCLUDE_MOVER | SCRIPT_INCLUDE_VEHICLE, obj, 0 )
									sCurrentSpecialPickupState.eBinBagStage[ index ] = BBS_SETUP
								ENDIF
							BREAK
							CASE BBS_SETUP
								INT 				iHitSomething
								VECTOR				vNormal
								ENTITY_INDEX 		entity
								SHAPETEST_STATUS	status
								
								status = GET_SHAPE_TEST_RESULT( sCurrentSpecialPickupState.sti, iHitSomething, vPos, vNormal, entity )
								IF( status = SHAPETEST_STATUS_RESULTS_READY )
									IF( iHitSomething = 1 )
										sCurrentSpecialPickupState.fGroundHeightForPickup = vPos.z
										sCurrentSpecialPickupState.eBinBagStage[ index ] = BBS_ORIENTING
									ENDIF
								ENDIF
								IF( status = SHAPETEST_STATUS_NONEXISTENT )
									sCurrentSpecialPickupState.fGroundHeightForPickup = vPos.z - GET_ENTITY_HEIGHT_ABOVE_GROUND( obj )
									sCurrentSpecialPickupState.eBinBagStage[ index ] = BBS_ORIENTING
								ENDIF
							BREAK
							CASE BBS_ORIENTING
								vPos.z = LERP_FLOAT( vPos.z, sCurrentSpecialPickupState.fGroundHeightForPickup + FMAX( fZDiff, 0.0 ), fLastFrameTime * fInterpSpeed * 1.5 )
								SLIDE_OBJECT( obj, vPos, <<0.0, 0.0, 0.20>>, FALSE )

								vRot = LERP_ROTATION( vRot, vZero, fLastFrameTime * fInterpSpeed )
								
								IF( ARE_ROTATION_VECTORS_EQUAL( vRot, <<180.0, 180.0, 0.0>>, 5.0, TRUE ) )
									vRot = <<180.0, 180.0, vRot.z>>
									sCurrentSpecialPickupState.eBinBagStage[ index ] = BBS_DORMANT
									FREEZE_ENTITY_POSITION( obj, FALSE )
								ENDIF
								
								SET_ENTITY_ROTATION( obj, vRot )
							BREAK
						ENDSWITCH
					ELSE
						FREEZE_ENTITY_POSITION( obj, FALSE )
						SET_ENTITY_ROTATION( obj, LERP_ROTATION( vRot, vZero, 1.0 ) )
						sCurrentSpecialPickupState.eBinBagStage[ index ] = BBS_DORMANT
					ENDIF
				ENDIF
				sCurrentSpecialPickupState.iBinBagFallTimer[ index ] = -1
			ELSE
				IF( NOT IS_ENTITY_ATTACHED_TO_ANY_PED( obj ) )
					IF( IS_ENTITY_IN_AIR( obj ) )
						IF( sCurrentSpecialPickupState.iBinBagFallTimer[ index ] = -1 )
							sCurrentSpecialPickupState.iBinBagFallTimer[ index ] = GET_GAME_TIMER() + 4000
						ELSE
							IF( sCurrentSpecialPickupState.iBinBagFallTimer[ index ] < GET_GAME_TIMER() )
								FREEZE_ENTITY_POSITION( obj, TRUE )
								SET_ENTITY_COORDS( obj, g_FMMC_STRUCT_ENTITIES.sPlacedObject[ index ].vPos, FALSE, FALSE, FALSE, FALSE )
								SET_ENTITY_ROTATION( obj, g_FMMC_STRUCT_ENTITIES.sPlacedObject[ index ].vRot, DEFAULT, FALSE )
								sCurrentSpecialPickupState.iBinBagFallTimer[ index ] = -1
								sCurrentSpecialPickupState.eBinBagStage[ index ] = BBS_DORMANT
							ENDIF
						ENDIF
					ELSE
						sCurrentSpecialPickupState.iBinBagFallTimer[ index ] = -1
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_PED_PLAYING_ANY_PICKUP_ANIM( PED_INDEX ped )
	STRING sAnimDict = "ANIM@HEISTS@NARCOTICS@TRASH"

	IF( IS_ENTITY_PLAYING_ANIM( ped, sAnimDict, "pickup_90_r" )
	OR  IS_ENTITY_PLAYING_ANIM( ped, sAnimDict, "pickup_45_r" )
	OR  IS_ENTITY_PLAYING_ANIM( ped, sAnimDict, "pickup" )
	OR  IS_ENTITY_PLAYING_ANIM( ped, sAnimDict, "pickup_45_l" )
	OR  IS_ENTITY_PLAYING_ANIM( ped, sAnimDict, "pickup_90_l" ) )
		RETURN TRUE
	ENDIF
	
	RETURN FALSE	
ENDFUNC

FUNC BOOL IS_PED_PLAYING_ANY_DROP_OFF_ANIM( PED_INDEX ped )
	STRING sAnimDict = "ANIM@HEISTS@NARCOTICS@TRASH"
	
	IF( IS_ENTITY_PLAYING_ANIM( ped, sAnimDict, "THROW_RANGED_A" )
	OR  IS_ENTITY_PLAYING_ANIM( ped, sAnimDict, "THROW_RANGED_B" )
	OR  IS_ENTITY_PLAYING_ANIM( ped, sAnimDict, "THROW_RANGED_C" )
	OR  IS_ENTITY_PLAYING_ANIM( ped, sAnimDict, "THROW_RANGED_D" )
	OR  IS_ENTITY_PLAYING_ANIM( ped, sAnimDict, "THROW_RANGED_E" )
	OR  IS_ENTITY_PLAYING_ANIM( ped, sAnimDict, "THROW_RANGED_F" )
	OR  IS_ENTITY_PLAYING_ANIM( ped, sAnimDict, "THROW_RANGED_G" )
	OR  IS_ENTITY_PLAYING_ANIM( ped, sAnimDict, "THROW_RANGED_SIDE_A" )
	OR  IS_ENTITY_PLAYING_ANIM( ped, sAnimDict, "THROW_RANGED_SIDE_B" )
	OR  IS_ENTITY_PLAYING_ANIM( ped, sAnimDict, "THROW_RANGED_SIDE_C" )
	OR  IS_ENTITY_PLAYING_ANIM( ped, sAnimDict, "THROW_RANGED_SIDE_D" )
	OR  IS_ENTITY_PLAYING_ANIM( ped, sAnimDict, "THROW_RANGED_SIDE_E" )
	OR  IS_ENTITY_PLAYING_ANIM( ped, sAnimDict, "THROW_RANGED_SIDE_F" )
	OR  IS_ENTITY_PLAYING_ANIM( ped, sAnimDict, "THROW_RANGED_SIDE_G" ) )
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC VECTOR CALCULATE_BINBAG_ATTACH_ROTATION_OFFSET( PED_INDEX ped, OBJECT_INDEX obj )
	FLOAT fObjHeading = GET_ENTITY_HEADING( obj )
	FLOAT fPedHeading = GET_ENTITY_HEADING( ped )
	STRING sAnimDict = "ANIM@HEISTS@NARCOTICS@TRASH"
	FLOAT fOffset

	IF( IS_ENTITY_PLAYING_ANIM( ped, sAnimDict, "pickup_90_r" ) )
		fOffset = 175.0
	ELIF( IS_ENTITY_PLAYING_ANIM( ped, sAnimDict, "pickup_45_r" ) )
		fOffset = 134.0
	ELIF( IS_ENTITY_PLAYING_ANIM( ped, sAnimDict, "pickup" ) )
		fOffset = 89.0
	ELIF( IS_ENTITY_PLAYING_ANIM( ped, sAnimDict, "pickup_45_l" ) )
		fOffset = 44.0
	ELIF( IS_ENTITY_PLAYING_ANIM( ped, sAnimDict, "pickup_90_l" ) )
		fOffset = 1.0
	ENDIF
	RETURN <<0.0, 0.0, fObjHeading + fOffset - fPedHeading>>
ENDFUNC

PROC PROCESS_REMOTE_PLAYER_BINBAG_ATTACHMENT(PED_INDEX ped, INT iPart, BOOL& bLocallyAttached)
	
	INT iBinBagSyncScene = MC_playerBD[ iPart ].iBinBagSyncScene
	BOOL bIsPlayingPickupAnim = FALSE
	FLOAT fAnimPhase = 0.0

	IF( iBinBagSyncScene = -2 )
		bLocallyAttached = TRUE
	ELSE
		INT iLocalSyncScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID( iBinBagSyncScene )
		IF( IS_SYNCHRONIZED_SCENE_RUNNING( iLocalSyncScene ) )
			fAnimPhase = GET_SYNCHRONIZED_SCENE_PHASE( iLocalSyncScene )
			
			IF( IS_PED_PLAYING_ANY_PICKUP_ANIM( ped ) )
				IF( fAnimPhase > 0.32 )
					bLocallyAttached = TRUE
					bIsPlayingPickupAnim = TRUE
				ENDIF
			ELIF( IS_PED_PLAYING_ANY_DROP_OFF_ANIM( ped ) )
				IF( fAnimPhase <= 0.35 )
					bLocallyAttached = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF( bLocallyAttached )
		INT iObj = MC_serverBD.iBinbagOwners[ iPart ]
		IF( iObj >= 0 )
			NETWORK_INDEX niObj = MC_serverBD_1.sFMMC_SBD.niObject[ iObj ]
			IF( NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID( niObj ) )
				OBJECT_INDEX obj = NET_TO_OBJ( niObj )
				IF( DOES_ENTITY_EXIST( obj ) )
					IF( NOT bIsPlayingPickupAnim 
					OR( bIsPlayingPickupAnim AND NOT IS_ENTITY_ATTACHED( obj ) ) )
						NETWORK_ALLOW_REMOTE_ATTACHMENT_MODIFICATION( obj, TRUE )
						VECTOR 	vOffset = <<0.0, 0.0, 0.0>>
						IF( NOT IS_BIT_SET( iBinBagLocallyAttachedLastFrame, iPart ) )
							IF( bIsPlayingPickupAnim )
								vLastBinBagAttachRot = CALCULATE_BINBAG_ATTACH_ROTATION_OFFSET( ped, obj )
							ELSE
								FLOAT fPedHeading = GET_ENTITY_HEADING( ped )
								FLOAT fObjHeading = GET_ENTITY_HEADING( obj )
								vLastBinBagAttachRot = <<0.0, 0.0, fPedHeading - fObjHeading>>
							ENDIF
						ENDIF
						INT iBone = GET_PED_BONE_INDEX( ped, BONETAG_PH_R_HAND )
						ATTACH_ENTITY_TO_ENTITY( obj, ped, iBone, vOffset, vLastBinBagAttachRot, TRUE, TRUE, FALSE )
						NETWORK_ALLOW_REMOTE_ATTACHMENT_MODIFICATION( obj, FALSE )
						CPRINTLN( DEBUG_SIMON, "PROCESS_SERVER_PARTICIPANT_EVERY_FRAME_CHECKS - bin bag attached remotely iPart:", 
							iPart, ", iObj:", iObj, ", fAnimPhase:", fAnimPhase )
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC DETACH_ANY_BINBAG_ATTACHED_TO_PED( PED_INDEX ped )
	IF( DOES_ENTITY_EXIST( ped ) )
		ENTITY_INDEX entity = GET_ENTITY_ATTACHED_TO( ped )
		IF( DOES_ENTITY_EXIST( entity ) )
			MODEL_NAMES model = GET_ENTITY_MODEL( entity )
			IF( model = HEI_PROP_HEIST_BINBAG )
				CPRINTLN( DEBUG_SIMON, "DETACH_ANY_BINBAG_ATTACHED_TO_PED - detaching bin bag" )
				DETACH_ENTITY( entity )
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC UPDATE_BINBAG_PICKUP( PED_INDEX ped, OBJECT_INDEX obj, INT iObjRequested, INT &iStage, VECTOR &vTargetPos, FLOAT fTargetHeading,
	INT &iTimer, INT &iPosition, INT &iSyncScene )
	
	CONST_INT	UNSET						-1
	CONST_FLOAT QUITE_SLOW_BLEND_OUT		-6.0
	
	CONST_INT	INIT_SYNC_SCENE_0			0
	CONST_INT	WAITING_FOR_ATTACH_1		1
	CONST_INT	WAITING_FOR_COMPLETE_2		2
	CONST_INT	GIVE_UP_3					3
	
	CONST_INT	F_PLAYER_DROPPED_OUT_EARLY	31
	CONST_INT	F_PICKUP_COMPLETE			30
	CONST_INT	F_WEAPON_OVERRIDE_APPLIED	29
	CONST_INT	F_PLAYER_SCENE_STARTED		28
	
	CONST_INT	STAGE_MASK					31 // 5 bits
	CONST_INT	FLAG_MASK					-67108864 // 6 bits
	
	CONST_FLOAT	ATTACH_PHASE				0.32
	
	FLOAT		fAnimPhase		= -1.0
	STRING 		sAnimDict 		= GET_PICKUP_ANIM_DICT( iObjRequested )
	STRING		sAnimName 		= GET_PICKUP_ANIM_CLIP( iObjRequested, ePickupAnim_PICKUP )
	VECTOR		VECTOR_ZERO		= <<0.0, 0.0, 0.0>>
	INT			iSubStage		= iStage & STAGE_MASK
	INT			iFlags			= iStage & FLAG_MASK
	BOOL		bRecurse		= FALSE
	INT 		iLocalSyncScene
	
	DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_PHONE )
	DISABLE_CELLPHONE_THIS_FRAME_ONLY()
	IF( iSubStage < WAITING_FOR_COMPLETE_2 )
		DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_CONTEXT )
		DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_MOVE_LR )
		DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_MOVE_UD )
	ENDIF
	
	IF( NOT IS_BIN_BAG_UPRIGHT( obj ) )
		HANDLE_BINBAG_ORIENTATION( iObjRequested, TRUE, FALSE )
	ENDIF
	IF( iSubStage > INIT_SYNC_SCENE_0 )
		iLocalSyncScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID( iSyncScene )
		IF( IS_SYNCHRONIZED_SCENE_RUNNING( iLocalSyncScene ) )
			fAnimPhase = GET_SYNCHRONIZED_SCENE_PHASE( iLocalSyncScene )
		ELSE
			fAnimPhase = 0.0
		ENDIF
		IF( iSubStage < GIVE_UP_3 
		AND NOT IS_BIT_SET( iFlags, F_PLAYER_DROPPED_OUT_EARLY ) )
			SET_PED_MAX_MOVE_BLEND_RATIO( ped, PEDMOVEBLENDRATIO_STILL )
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
		STRING str
		SWITCH( iSubStage )
			CASE INIT_SYNC_SCENE_0		str = "INIT_SYNC_SCENE_0" 		BREAK
			CASE WAITING_FOR_ATTACH_1	str = "WAITING_FOR_ATTACH_1" 	BREAK
			CASE WAITING_FOR_COMPLETE_2	str = "WAITING_FOR_COMPLETE_2" 	BREAK
			CASE GIVE_UP_3				str = "GIVE_UP_3" 				BREAK
		ENDSWITCH
		CPRINTLN( DEBUG_SIMON, "UPDATE_BINBAG_PICKUP - iSubStage:", str, ", fAnimPhase:", fAnimPhase )
		CPRINTLN( DEBUG_SIMON, "F_PLAYER_DROPPED_OUT_EARLY:", 	IS_BIT_SET( iFlags, F_PLAYER_DROPPED_OUT_EARLY ) )
		CPRINTLN( DEBUG_SIMON, "F_PICKUP_COMPLETE:", 			IS_BIT_SET( iFlags, F_PICKUP_COMPLETE ) )
		CPRINTLN( DEBUG_SIMON, "F_WEAPON_OVERRIDE_APPLIED:",	IS_BIT_SET( iFlags, F_WEAPON_OVERRIDE_APPLIED ) )
		CPRINTLN( DEBUG_SIMON, "F_PLAYER_SCENE_STARTED:", 		IS_BIT_SET( iFlags, F_PLAYER_SCENE_STARTED ) )
	#ENDIF
	
	SWITCH( iSubStage )
		CASE INIT_SYNC_SCENE_0
			IF( NOT SHOULD_DROP_SPECIAL_PICKUP_CHECKS( obj ) )
				FLOAT  fAnimRate, fAnimTotalTime, fMoveBlendTime
				VECTOR vPedRot, vSyncSceneRot
				
				fAnimRate 		= 1.2
				fAnimTotalTime	= GET_ANIM_DURATION( sAnimDict, sAnimName )
				fMoveBlendTime	= 1.0 / ( ATTACH_PHASE * ( fAnimTotalTime / fAnimRate ) )
				vPedRot 		= GET_ENTITY_ROTATION( ped )
				vSyncSceneRot 	= <<vPedRot.x, vPedRot.y, fTargetHeading>>
				
				RAGDOLL_BLOCKING_FLAGS rbf
				rbf = RBF_BULLET_IMPACT | RBF_PLAYER_IMPACT | RBF_MELEE | RBF_RUBBER_BULLET | RBF_FALLING | RBF_DROWNING 
					| RBF_ALLOW_BLOCK_DEAD_PED | RBF_PLAYER_BUMP | RBF_PLAYER_RAGDOLL_BUMP | RBF_PED_RAGDOLL_BUMP | RBF_VEHICLE_GRAB
					
				SYNCED_SCENE_PLAYBACK_FLAGS sspf
				sspf = SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_ABORT_ON_DEATH
					
				iSyncScene = NETWORK_CREATE_SYNCHRONISED_SCENE( vTargetPos, vSyncSceneRot, DEFAULT, TRUE, DEFAULT, DEFAULT, DEFAULT, fAnimRate )
				
				#IF IS_NEXTGEN_BUILD
				NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE( ped, iSyncScene, sAnimDict, sAnimName, fMoveBlendTime, QUITE_SLOW_BLEND_OUT, 
					sspf, rbf, fMoveBlendTime, AIK_DISABLE_ARM_IK )
				#ENDIF
				#IF NOT IS_NEXTGEN_BUILD
				NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE_WITH_IK( ped, iSyncScene, sAnimDict, sAnimName, fMoveBlendTime, QUITE_SLOW_BLEND_OUT, 
					sspf, rbf, fMoveBlendTime, ENUM_TO_INT( AIK_DISABLE_ARM_IK ) )
				#ENDIF
				NETWORK_START_SYNCHRONISED_SCENE( iSyncScene )	
				MC_playerBD[ iPartToUse ].iBinBagSyncScene = iSyncScene
					
				iSubStage = WAITING_FOR_ATTACH_1
			ELSE
				CPRINTLN( DEBUG_SIMON, "UPDATE_BINBAG_PICKUP - Giving up because the scene shouldn't start" )
				iSubStage = GIVE_UP_3
				bRecurse = TRUE
			ENDIF
		BREAK
		CASE WAITING_FOR_ATTACH_1
			IF( NOT IS_BIT_SET( iFlags, F_PLAYER_SCENE_STARTED ) )
				IF( IS_SYNCHRONIZED_SCENE_RUNNING( iLocalSyncScene ) )
					SET_BIT( iFlags, F_PLAYER_SCENE_STARTED ) 
				ENDIF
			ENDIF
		
			IF( NOT IS_BIT_SET( iFlags, F_WEAPON_OVERRIDE_APPLIED ) )
				IF( fAnimPhase > 0.0 )
					GIVE_WEAPON_TO_PED( ped, WEAPONTYPE_DLC_GARBAGEBAG, -1, TRUE, TRUE )
					SET_BIT( iFlags, F_WEAPON_OVERRIDE_APPLIED )
				ENDIF
			ENDIF
		
			IF( fAnimPhase > ATTACH_PHASE )
				CPRINTLN( DEBUG_SIMON, "UPDATE_BINBAG_PICKUP - Attaching object iObj:", iObjRequested, ", iPart:", iPartToUse )								
				HANDLE_BINBAG_ORIENTATION( iObjRequested, FALSE, FALSE, TRUE )
				
				#IF IS_DEBUG_BUILD
				IF( DOES_ENTITY_EXIST( obj ) )
					CPRINTLN( DEBUG_SIMON, "UPDATE_BINBAG_PICKUP - obj exists" )
				ELSE
					CPRINTLN( DEBUG_SIMON, "UPDATE_BINBAG_PICKUP - obj doesn't exist" )
				ENDIF
				#ENDIF
				
				VECTOR 	vOffset, vObjRot
				INT	   	iBone
				vObjRot = CALCULATE_BINBAG_ATTACH_ROTATION_OFFSET( ped, obj )
				vOffset = VECTOR_ZERO
				iBone 	= GET_PED_BONE_INDEX( ped, BONETAG_PH_R_HAND )
				ATTACH_ENTITY_TO_ENTITY( obj, ped, iBone, vOffset, vObjRot, TRUE, TRUE, FALSE )
				
				iSubStage = WAITING_FOR_COMPLETE_2
			ENDIF
			
			// the sync scene has been aborted before picking up the bag
			IF( SHOULD_DROP_SPECIAL_PICKUP_CHECKS( obj )
			OR ( NOT IS_SYNCHRONIZED_SCENE_RUNNING( iLocalSyncScene ) AND IS_BIT_SET( iFlags, F_PLAYER_SCENE_STARTED ) ) )
				CPRINTLN( DEBUG_SIMON, "UPDATE_BINBAG_PICKUP - Giving up before the attachment" )
				iSubStage = GIVE_UP_3
				bRecurse = TRUE
			ENDIF
		BREAK
		CASE WAITING_FOR_COMPLETE_2
			// the sync scene has been aborted after picking up the bag
			IF( SHOULD_DROP_SPECIAL_PICKUP_CHECKS( obj ) OR NOT IS_SYNCHRONIZED_SCENE_RUNNING( iLocalSyncScene ) )
				CPRINTLN( DEBUG_SIMON, "UPDATE_BINBAG_PICKUP - Giving up after attachment" )
				DETACH_ENTITY( obj )
				iSubStage = GIVE_UP_3
				bRecurse = TRUE
			ENDIF
			IF( iSubStage = WAITING_FOR_COMPLETE_2 )
				IF( NOT IS_BIT_SET( iFlags, F_PLAYER_DROPPED_OUT_EARLY ) )
					IF( SHOULD_DROP_OUT_OF_BINBAG_ANIM_EARLY() )
					
						CPRINTLN( DEBUG_SIMON, "UPDATE_BINBAG_PICKUP - Dropping out early" )
						#IF IS_DEBUG_BUILD
						IF( DOES_ENTITY_EXIST( obj ) )
							CPRINTLN( DEBUG_SIMON, "UPDATE_BINBAG_PICKUP - obj exists" )
						ELSE
							CPRINTLN( DEBUG_SIMON, "UPDATE_BINBAG_PICKUP - obj doesn't exist" )
						ENDIF
						#ENDIF
					
						fAnimPhase = 1.0
						SET_BIT( iFlags, F_PLAYER_DROPPED_OUT_EARLY )
					ENDIF
				ENDIF
				IF( fAnimPhase = 1.0 )
					#IF IS_DEBUG_BUILD
					IF( DOES_ENTITY_EXIST( obj ) )
						CPRINTLN( DEBUG_SIMON, "UPDATE_BINBAG_PICKUP - obj exists" )
					ELSE
						CPRINTLN( DEBUG_SIMON, "UPDATE_BINBAG_PICKUP - obj doesn't exist" )
					ENDIF
					IF( ped <> PLAYER_PED_ID() )
						SCRIPT_ASSERT( "UPDATE_BINBAG_PICKUP - ped <> PLAYER_PED_ID()" )
					ENDIF
					#ENDIF
					IF( IS_ENTITY_ATTACHED_TO_ENTITY( obj, ped ) )
						CPRINTLN( DEBUG_SIMON, "UPDATE_BINBAG_PICKUP - Animation finished and carrying binbag iObj:", iObjRequested, ", iPart:", iPartToUse )
						IF( NOT IS_PED_INJURED( ped ) )
							CLEAR_PED_TASKS( ped )
						ENDIF
						IF( IS_SYNCHRONIZED_SCENE_RUNNING( iLocalSyncScene ) )
							NETWORK_STOP_SYNCHRONISED_SCENE( iSyncScene )
						ENDIF
						iTimer 		= GET_GAME_TIMER() + 2000
						vTargetPos 	= VECTOR_ZERO
						iPosition 	= UNSET
						MC_playerBD[ iPartToUse ].iBinBagSyncScene = UNSET
						SET_BIT( iFlags, F_PICKUP_COMPLETE )
						SET_CURRENT_SPECIAL_PICKUP_STATE( eSpecialPickupState_CARRYING )
					ELSE
						CPRINTLN( DEBUG_SIMON, "UPDATE_BINBAG_PICKUP - Anim finished but ped not carrying object! iObj:", iObjRequested, ", iPart:", iPartToUse )
						DETACH_ENTITY( obj )
						iSubStage = GIVE_UP_3 // // the sync scene has finished but the bin bag isn't attached
						bRecurse = TRUE
					ENDIF				
				ENDIF
			ENDIF
		BREAK
		CASE GIVE_UP_3
			IF( IS_SYNCHRONIZED_SCENE_RUNNING( iLocalSyncScene ) )
				NETWORK_STOP_SYNCHRONISED_SCENE( iSyncScene )
			ENDIF
			DETACH_ANY_BINBAG_ATTACHED_TO_PED( ped ) // failsafe to make sure we drop the bag if we got here by mistake
			SET_BINBAG_NET_ID_CAN_MIGRATE( obj, TRUE )
			RESET_SPECIAL_PICKUP()
			SET_BIT( iFlags, F_PICKUP_COMPLETE )
			SET_CURRENT_SPECIAL_PICKUP_STATE( eSpecialPickupState_BEFORE_PICKUP )
		BREAK
	ENDSWITCH
	
	IF( IS_BIT_SET( iFlags, F_PICKUP_COMPLETE ) )
		iStage = INIT_SYNC_SCENE_0
	ELSE
		iStage = iSubStage | iFlags
	ENDIF
	
	IF( bRecurse )
		UPDATE_BINBAG_PICKUP( ped, obj, iObjRequested, iStage, vTargetPos, fTargetHeading, iTimer, iPosition, iSyncScene )
	ENDIF
ENDPROC

PROC UPDATE_BINBAG_PUT_DOWN( PED_INDEX ped, OBJECT_INDEX obj, INT iObjRequested, INT &iStage )

	CONST_INT	INIT_ANIM_0						0
	CONST_INT	WAITING_TO_DETACH_1				1
	CONST_INT	WAITING_FOR_PUT_DOWN_COMPLETE_2	2
	CONST_INT	GIVE_UP_3						3
	
	CONST_INT	F_PLAYER_BROKEN_OUT_EARLY		31
	CONST_INT	F_PUT_DOWN_COMPLETE				30
	
	CONST_INT	STAGE_MASK						31 // 5 bits
	CONST_INT	FLAG_MASK						-67108864 // 6 bits

	CONST_FLOAT	DETACH_PHASE					0.132
	
	FLOAT	fAnimPhase	= -1.0
	INT		iSubStage	= iStage & STAGE_MASK
	INT		iFlags		= iStage & FLAG_MASK
	STRING 	sAnimDict 	= GET_PICKUP_ANIM_DICT( iObjRequested )
	STRING 	sAnimName 	= GET_PICKUP_ANIM_CLIP( iObjRequested, ePickupAnim_PUTDOWN )
	
	IF( IS_PED_PERFORMING_TASK( ped, SCRIPT_TASK_PLAY_ANIM ) )
		IF( IS_ENTITY_PLAYING_ANIM( ped, sAnimDict, sAnimName ) )
			fAnimPhase = GET_ENTITY_ANIM_CURRENT_TIME( ped, sAnimDict, sAnimName )
		ELSE
			fAnimPhase = 0.0
		ENDIF
	ENDIF
		
	#IF IS_DEBUG_BUILD
		STRING str
		SWITCH( iSubStage )
			CASE INIT_ANIM_0						str = "INIT_ANIM_0" 					BREAK
			CASE WAITING_TO_DETACH_1				str = "WAITING_TO_DETACH_1" 			BREAK
			CASE WAITING_FOR_PUT_DOWN_COMPLETE_2	str = "WAITING_FOR_PUT_DOWN_COMPLETE_2" BREAK
			CASE GIVE_UP_3							str = "GIVE_UP_3" 						BREAK
		ENDSWITCH
		CPRINTLN( DEBUG_SIMON, "UPDATE_BINBAG_PUT_DOWN - iSubStage:", str )
		CPRINTLN( DEBUG_SIMON, "fAnimPhase:", fAnimPhase )
		CPRINTLN( DEBUG_SIMON, "F_PLAYER_BROKEN_OUT_EARLY:", IS_BIT_SET( iFlags, F_PLAYER_BROKEN_OUT_EARLY ) )
		CPRINTLN( DEBUG_SIMON, "F_PUT_DOWN_COMPLETE:", IS_BIT_SET( iFlags, F_PUT_DOWN_COMPLETE ) )
	#ENDIF

	DISABLE_CELLPHONE_THIS_FRAME_ONLY()
	
	SWITCH( iSubStage )
		CASE INIT_ANIM_0
			PLAY_SPECIAL_PICKUP_ANIM( obj, ePickupAnim_PUTDOWN, iObjRequested )
			SET_ENTITY_COLLISION( obj, TRUE )
			iSubStage = WAITING_TO_DETACH_1
		BREAK
		CASE WAITING_TO_DETACH_1
			IF( IS_PED_PERFORMING_TASK( ped, SCRIPT_TASK_PLAY_ANIM ) )
				IF( fAnimPhase > DETACH_PHASE )
					CPRINTLN( DEBUG_SIMON, "UPDATE_BINBAG_PUT_DOWN - Detaching binbag iObj:", iObjRequested, ", iPart:", iPartToUse )
					DETACH_ENTITY( obj, TRUE, FALSE )
					GIVE_WEAPON_TO_PED( ped, WEAPONTYPE_UNARMED, -1, TRUE, TRUE )
					iSubStage = WAITING_FOR_PUT_DOWN_COMPLETE_2
				ENDIF
			ELSE
				iSubStage = GIVE_UP_3
			ENDIF			
		BREAK
		CASE WAITING_FOR_PUT_DOWN_COMPLETE_2
			IF( IS_PED_PERFORMING_TASK( ped, SCRIPT_TASK_PLAY_ANIM ) )
				IF( NOT IS_BIT_SET( iFlags, F_PLAYER_BROKEN_OUT_EARLY ) )
					IF( SHOULD_DROP_OUT_OF_BINBAG_ANIM_EARLY() )
						CPRINTLN( DEBUG_SIMON, "UPDATE_BINBAG_PUT_DOWN - Dropping out of anim early iObj:", iObjRequested, ", iPart:", iPartToUse )
						IF( NOT IS_PED_INJURED( ped ) )
							CLEAR_PED_TASKS( ped )
						ENDIF
						fAnimPhase = 1.0
						SET_BIT( iFlags, F_PLAYER_BROKEN_OUT_EARLY )
					ENDIF
				ENDIF
				IF( fAnimPhase = 1.0 )
					CPRINTLN( DEBUG_SIMON, "UPDATE_BINBAG_PUT_DOWN - Putting down object iObj:", iObjRequested, ", iPart:", iPartToUse )
					IF( NOT IS_BIT_SET( iFlags, F_PLAYER_BROKEN_OUT_EARLY ) )
						IF( NOT IS_PED_INJURED( ped ) )
							CLEAR_PED_TASKS( ped )
						ENDIF
					ENDIF
					SET_BIT( iFlags, F_PUT_DOWN_COMPLETE )
					SET_BINBAG_NET_ID_CAN_MIGRATE( obj, TRUE )
					RESET_SPECIAL_PICKUP()
					SET_CURRENT_SPECIAL_PICKUP_STATE( eSpecialPickupState_BEFORE_PICKUP )
				ENDIF
			ELSE
				iSubStage = GIVE_UP_3
			ENDIF
		BREAK
		CASE GIVE_UP_3
			DETACH_ENTITY( obj, TRUE, FALSE )
			SET_BINBAG_NET_ID_CAN_MIGRATE( obj, TRUE )
			RESET_SPECIAL_PICKUP()
			SET_BIT( iFlags, F_PUT_DOWN_COMPLETE )
			SET_CURRENT_SPECIAL_PICKUP_STATE( eSpecialPickupState_BEFORE_PICKUP )
		BREAK
	ENDSWITCH
	
	IF( IS_BIT_SET( iFlags, F_PUT_DOWN_COMPLETE ) )
		iStage = INIT_ANIM_0
	ELSE
		iStage = iSubStage | iFlags
	ENDIF
ENDPROC

FUNC BOOL IS_SAFE_FOR_BINBAG_PICKUP()
	RETURN NOT IS_PLAYER_CLIMBING( LocalPlayer )
	AND NOT IS_PED_RAGDOLL( LocalPlayerPed )
	AND NOT IS_PED_DEAD_OR_DYING( LocalPlayerPed )
	AND NOT IS_PED_SWIMMING( LocalPlayerPed )
	AND NOT IS_PED_FALLING( LocalPlayerPed )
	AND NOT IS_ENTITY_IN_AIR( LocalPlayerPed )
	AND NOT IS_ENTITY_ON_FIRE( LocalPlayerPed )
	AND NOT IS_PED_IN_ANY_VEHICLE( LocalPlayerPed, TRUE )
	AND NOT IS_PED_ON_VEHICLE( LocalPlayerPed )
	AND NOT IS_PED_PERFORMING_MELEE_ACTION( LocalPlayerPed )
	AND NOT IS_PED_IN_COVER( LocalPlayerPed )
	AND NOT IS_PED_GOING_INTO_COVER( LocalPlayerPed )
	AND NOT IS_PED_JUMPING( LocalPlayerPed )
	AND NOT IS_PED_VAULTING( LocalPlayerPed )
	AND NOT IS_PED_DIVING( LocalPlayerPed )
	AND NOT IS_PED_JUMPING_OUT_OF_VEHICLE( LocalPlayerPed )
	AND NOT IS_PED_OPENING_DOOR( LocalPlayerPed )
	AND NOT IS_PED_IN_PARACHUTE_FREE_FALL( LocalPlayerPed )
	AND NOT IS_PHONE_ONSCREEN()
	AND NOT IS_PED_GETTING_UP( LocalPlayerPed )
ENDFUNC

FUNC BOOL IS_PED_ROUGHLY_LEVEL_WITH_BINBAG( PED_INDEX ped, OBJECT_INDEX obj )
	VECTOR vObjPos = GET_ENTITY_COORDS( obj, FALSE )
	VECTOR vPedPos = GET_ENTITY_COORDS( ped, FALSE )
	RETURN ABSF( vPedPos.z - vObjPos.z ) < 1.0
ENDFUNC

#IF IS_NEXTGEN_BUILD

FUNC BOOL HANDLE_BINBAG_HIDE( OBJECT_INDEX obj, BOOL bHide )
	IF( IS_ENTITY_ATTACHED_TO_ANY_PED( obj ) )
		IF( bHide )
			IF( IS_ENTITY_VISIBLE( obj ) )
				SET_ENTITY_VISIBLE( obj, FALSE )
				RETURN TRUE
			ENDIF
		ELSE
			IF( NOT IS_ENTITY_VISIBLE( obj ) )
				SET_ENTITY_VISIBLE( obj, TRUE )
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MANAGE_FIRST_PERSON_BINBAG( OBJECT_INDEX objReal, OBJECT_INDEX &objFake, PED_INDEX ped, BOOL bCleanup = FALSE )
	IF( bCleanup )
		IF( DOES_ENTITY_EXIST( objFake ) )
			DELETE_OBJECT( objFake )
		ENDIF
		CPRINTLN( DEBUG_SIMON, "MANAGE_FIRST_PERSON_BINBAG - Cleaning up fake binbag" )
		EXIT
	ENDIF
	// create fake bag
	VECTOR vZero = <<0.0, 0.0, 0.0>>

	IF( NOT DOES_ENTITY_EXIST( objFake ) )
		objFake = CREATE_OBJECT( HEI_PROP_HEIST_BINBAG, GET_ENTITY_COORDS( ped, FALSE ), FALSE, TRUE, FALSE )
		SET_ENTITY_INVINCIBLE( objFake, TRUE )
		SET_ENTITY_PROOFS( objFake, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE )
		ATTACH_ENTITY_TO_ENTITY( objFake, ped, GET_PED_BONE_INDEX( ped, BONETAG_PH_R_HAND ), vZero, vZero )
		CPRINTLN( DEBUG_SIMON, "MANAGE_FIRST_PERSON_BINBAG - Creating fake binbag" )
	ENDIF

	// handle fake bag attachment
	BOOL bIsRealAttached = FALSE
	BOOL bIsFakeAttached = FALSE
	
	IF( DOES_ENTITY_EXIST( objReal ) )
		bIsRealAttached = IS_ENTITY_ATTACHED_TO_ENTITY( objReal, ped )
		IF( DOES_ENTITY_EXIST( objFake ) )
			bIsFakeAttached = IS_ENTITY_ATTACHED_TO_ENTITY( objFake, ped )
			// only attach fake bag while not dying and real bag is attached
			IF( bIsFakeAttached )
				// move to origin if dying or real bag not attached
				IF( IS_PED_INJURED( ped ) OR IS_PED_DEAD_OR_DYING( ped ) OR NOT bIsRealAttached )
					DETACH_ENTITY( objFake, FALSE, TRUE )
					FREEZE_ENTITY_POSITION( objFake, TRUE )
					SET_ENTITY_COORDS( objFake, vZero, FALSE, FALSE, FALSE, FALSE )
					SET_ENTITY_VISIBLE( objFake, FALSE )
					SET_ENTITY_COLLISION( objFake, FALSE, FALSE )
				ENDIF
			ELSE
				// attach if not dying and real bag is attached
				IF( NOT IS_PED_INJURED( ped ) AND NOT IS_PED_DEAD_OR_DYING( ped ) AND bIsRealAttached )
					FREEZE_ENTITY_POSITION( objFake, FALSE )
					ATTACH_ENTITY_TO_ENTITY( objFake, ped, GET_PED_BONE_INDEX( ped, BONETAG_PH_R_HAND ), vZero, vZero )
					SET_ENTITY_VISIBLE( objFake, FALSE )
					SET_ENTITY_COLLISION( objFake, FALSE, FALSE )
				ENDIF
			ENDIF
		ENDIF
	ELSE
		// move to origin if real bag doesn't exist
		IF( DOES_ENTITY_EXIST( objFake ) )
			DETACH_ENTITY( objFake, FALSE, TRUE )
			FREEZE_ENTITY_POSITION( objFake, TRUE )
			SET_ENTITY_COORDS( objFake, vZero, FALSE, FALSE, FALSE, FALSE )
			SET_ENTITY_VISIBLE( objFake, FALSE )
			SET_ENTITY_COLLISION( objFake, FALSE, FALSE )
		ENDIF
	ENDIF
	bIsFakeAttached = IS_ENTITY_ATTACHED_TO_ENTITY( objFake, ped )
	
	// handle fake bag visibility
	BOOL bIsFirstPerson = ( GET_CAM_VIEW_MODE_FOR_CONTEXT( CAM_VIEW_MODE_CONTEXT_ON_FOOT ) = CAM_VIEW_MODE_FIRST_PERSON )
	
	IF( bIsFakeAttached AND bIsFirstPerson )
		SET_ENTITY_LOCALLY_INVISIBLE( objReal )
		IF( HANDLE_BINBAG_HIDE( objFake, FALSE ) )
			CPRINTLN( DEBUG_SIMON, "MANAGE_FIRST_PERSON_BINBAG - Hiding real binbag" )
			CPRINTLN( DEBUG_SIMON, "bIsRealAttached: ", bIsRealAttached, ", bIsFakeAttached: ", bIsFakeAttached, ", bIsFirstPerson: ", bIsFirstPerson )
		ENDIF
	ELSE
		IF( HANDLE_BINBAG_HIDE( objFake, TRUE ) )
			CPRINTLN( DEBUG_SIMON, "MANAGE_FIRST_PERSON_BINBAG - Hiding fake binbag" )
			CPRINTLN( DEBUG_SIMON, "bIsRealAttached: ", bIsRealAttached, ", bIsFakeAttached: ", bIsFakeAttached, ", bIsFirstPerson: ", bIsFirstPerson )
		ENDIF
	ENDIF
ENDPROC

#ENDIF

FUNC BOOL DOES_ANY_PARTICIPANT_OWN_THIS_BINBAG( INT iObj, INT iExcludePart = -1 )
	INT iPart
	REPEAT GET_MAXIMUM_PLAYERS_ON_MISSION() iPart
		IF( iPart <> iExcludePart )
			IF( MC_serverBD.iBinbagOwners[ iPart ] = iObj )
				CPRINTLN( DEBUG_SIMON, "DOES_ANY_PARTICIPANT_OWN_THIS_BINBAG - iPart:", iPart, " owns iObj:", iObj )
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	CPRINTLN( DEBUG_SIMON, "DOES_ANY_PARTICIPANT_OWN_THIS_BINBAG - No participant owns iObj:", iObj )
	RETURN FALSE
ENDFUNC

FUNC FLOAT GET_PICKUP_RANGE_FOR_SPECIAL_PICKUP( OBJECT_INDEX obj )
	SWITCH( GET_ENTITY_MODEL( obj ) )
		CASE HEI_PROP_HEI_TIMETABLE		RETURN 5.0	BREAK
		CASE HEI_PROP_HEIST_BINBAG		RETURN 3.7	BREAK
		DEFAULT	
			ASSERTLN( "GET_PICKUP_RANGE_FOR_SPECIAL_PICKUP >> pickup type not setup!" )
			RETURN 0.0
		BREAK
	ENDSWITCH
	RETURN 0.0
ENDFUNC

FUNC BOOL GET_CLOSEST_VALID_SPECIAL_OBJECT( INT &iLocalObjForPart )
	IF( MC_serverBD.iNumObjCreated > 0 AND MC_serverBD.iNumObjCreated <= FMMC_MAX_NUM_OBJECTS
	AND NOT IS_PED_IN_ANY_VEHICLE( LocalPlayerPed ) )
		INT 	iBestResultInFront 	= -1
		FLOAT 	fBestDist2InFront
		INT 	iBestResult 		= -1
		FLOAT 	fBestDist2
		VECTOR 	vPlayerPos 			= GET_ENTITY_COORDS( LocalPlayerPed, FALSE )
		VECTOR 	vPlayerForward 		= GET_ENTITY_FORWARD_VECTOR( LocalPlayerPed ) 
		INT 	i
		OBJECT_INDEX obj
		REPEAT MC_serverBD.iNumObjCreated i
			obj = NULL
			IF( GET_SPECIAL_OBJECT( i, obj ) )
				IF( NOT IS_ENTITY_ATTACHED_TO_ANY_PED( obj ) )
					VECTOR 	vObjPos 		= GET_ENTITY_COORDS( obj, FALSE )
					VECTOR 	vPlayerToObj 	= NORMALISE_VECTOR( vObjPos - vPlayerPos )
					FLOAT 	fDot 			= DOT_PRODUCT( vPlayerForward, vPlayerToObj )
					BOOL	bInFront		= fDot > 0.8
					FLOAT	fDist2			= VDIST2( vPlayerPos, vObjPos )
				
					IF( fDist2 < GET_PICKUP_RANGE_FOR_SPECIAL_PICKUP( obj ) )
						IF( bInFront )
							IF( iBestResultInFront = -1 OR fDist2 < fBestDist2InFront )
								iBestResultInFront = i
								fBestDist2InFront = fDist2
							ENDIF
						ELSE
							IF( iBestResult = -1 OR fDist2 < fBestDist2 )
								iBestResult = i
								fBestDist2 = fDist2
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		IF( iBestResultInFront <> -1 )
			iLocalObjForPart = iBestResultInFront
			GET_SPECIAL_OBJECT( iLocalObjForPart, obj )
			IF( GET_ENTITY_MODEL( obj ) = HEI_PROP_HEIST_BINBAG )
				IF( IS_OBJECT_CONFLICTING_WITH_NEARBY_VEHICLE( obj, LocalPlayerPed ) )
					IF( NOT DOES_ANY_PARTICIPANT_OWN_THIS_BINBAG( iLocalObjForPart ) AND IS_ENTITY_VISIBLE( obj ) )
						SET_BIN_BAG_HELP_TEXT_BIT( BINBAGHELP_PICKUP_POSITION_UNSUITABLE )
					ENDIF
					iLocalObjForPart = -1
					RETURN FALSE
				ENDIF
			ENDIF
			IF( GET_ENTITY_SPEED( obj ) <> 0.0 )
				iLocalObjForPart = -1
				RETURN FALSE
			ENDIF
			RETURN TRUE
		ELIF( iBestResult <> -1 )
			iLocalObjForPart = iBestResult
			GET_SPECIAL_OBJECT( iLocalObjForPart, obj )
			IF( GET_ENTITY_MODEL( obj ) = HEI_PROP_HEIST_BINBAG )
				IF( IS_OBJECT_CONFLICTING_WITH_NEARBY_VEHICLE( obj, LocalPlayerPed ) )
					IF( NOT DOES_ANY_PARTICIPANT_OWN_THIS_BINBAG( iLocalObjForPart ) AND IS_ENTITY_VISIBLE( obj ) )
						SET_BIN_BAG_HELP_TEXT_BIT( BINBAGHELP_PICKUP_POSITION_UNSUITABLE )
					ENDIF
					iLocalObjForPart = -1
					RETURN FALSE
				ENDIF
			ENDIF
			IF( GET_ENTITY_SPEED( obj ) <> 0.0 )
				iLocalObjForPart = -1
				RETURN FALSE
			ENDIF
			RETURN TRUE
		ELSE
			iLocalObjForPart = -1
			RETURN FALSE
		ENDIF
	ELSE
		iLocalObjForPart = -1
		RETURN FALSE
	ENDIF
ENDFUNC

PROC PROCESS_BINBAG_PICKUP_STATE( eSpecialPickupState eState, INT &iObjRequested, INT &iObjOwned, OBJECT_INDEX &obj, INT &iContext, 
	PED_INDEX ped, INT &iStage )
	
	CONST_INT	UNSET	-1
	INT 		iTeam 	= MC_playerBD[ iPartToUse ].iTeam
	INT 		iRule 	= MC_serverBD_4.iCurrentHighestPriority[ iTeam ]
	
	IF( iRule < FMMC_MAX_RULES )
		CLEAR_BIT( MC_playerBD[ iPartToUse ].iClientBitSet2, PBBOOL2_BLOCK_SECONDARY_AND_IDLE_ANIMS )

		INT 			iDropoffVehIndex 		= g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iDropOff_Vehicle[ iRule ]
		NETWORK_INDEX	niDropoffVeh
		VEHICLE_INDEX 	veh						= NULL
		
		IF( iDropoffVehIndex <> UNSET )
			niDropoffVeh = MC_serverBD_1.sFMMC_SBD.niVehicle[ iDropoffVehIndex ]
			IF( NETWORK_DOES_NETWORK_ID_EXIST( niDropoffVeh ) )
				veh = NET_TO_VEH( niDropoffVeh )
				
				IF( GET_ENTITY_MODEL( veh ) <> TRASH2
				AND GET_ENTITY_MODEL( veh ) <> TRASH )
					ASSERTLN( "PROCESS_BINBAG_PICKUP_STATE - vehicle isn't a TRASH or TRASH2! iDropoffVehIndex:", 
						iDropoffVehIndex, ", model hash:", ENUM_TO_INT( GET_ENTITY_MODEL( veh ) ) )
				ENDIF
			ENDIF
		ENDIF
		eSpecialPickupState eOldState = eState
		
		SWITCH( eState )
			// waiting for player to attempt pickup of an object
			CASE eSpecialPickupState_BEFORE_PICKUP
				IF( iContext = UNSET )
					REGISTER_CONTEXT_INTENTION( iContext, CP_MAXIMUM_PRIORITY, "", TRUE )
				ENDIF
				
				IF( IS_SAFE_FOR_BINBAG_PICKUP() AND IS_PED_ROUGHLY_LEVEL_WITH_BINBAG( ped, obj )
				AND NOT DOES_ANY_PARTICIPANT_OWN_THIS_BINBAG( iObjRequested ) )
					IF( NOT IS_PED_ABOVE_BIN_BAG( ped, obj ) )						
						IF( ROLLING_CHECK_FOR_SPECIAL_PICKUP_OBSTRUCTION( ped, obj, sCurrentSpecialPickupState.sti, <<0.0, 0.0, 0.0>> )
						AND GET_ENTITY_SPEED( ped ) < 4.5 )
							IF( GET_ENTITY_SPEED( obj ) = 0.0 )
								SET_BIN_BAG_HELP_TEXT_BIT( BINBAGHELP_PICKUP )
							ENDIF
							
							IF( HAS_CONTEXT_BUTTON_TRIGGERED( iContext, FALSE )
							AND NOT IS_CONTROL_PRESSED( PLAYER_CONTROL, INPUT_SELECT_WEAPON ) )
								IF( ARE_PICKUP_ASSETS_LOADED() )
									DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_JUMP )
									DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_COVER )
								
									CPRINTLN( DEBUG_SIMON, "PROCESS_BINBAG_PICKUP_STATE - Requesting pickup for object ", iObjRequested, ", iPart:", iPartToUse )
									CLEAR_PED_TASKS( ped )
									BROADCAST_BINBAG_REQUEST( iObjRequested, iPartToUse, FALSE )
									sCurrentSpecialPickupState.iCurrentlyOwnedBinbag = -1 
									SET_CURRENT_SPECIAL_PICKUP_STATE( eSpecialPickupState_ALIGNING )
									sCurrentSpecialPickupState.bObstructionExists = FALSE
								 	RELEASE_CONTEXT_INTENTION( iContext )
								ELSE
									ASSERTLN( "PROCESS_BINBAG_PICKUP_STATE - Special pickups assets not loaded!" )
								ENDIF
							ELSE
							PRINTLN("[JS] BINBAG FAILED 4: ")
							PRINTLN("HAS_CONTEXT_BUTTON_TRIGGERED( iContext, FALSE) = ", HAS_CONTEXT_BUTTON_TRIGGERED( iContext, FALSE))
							PRINTLN("IS_CONTROL_PRESSED( PLAYER_CONTROL, INPUT_SELECT_WEAPON ) ) = ", IS_CONTROL_PRESSED( PLAYER_CONTROL, INPUT_SELECT_WEAPON ) )
							ENDIF
						ELSE
							PRINTLN("[JS] BINBAG FAILED 3: ")
							PRINTLN("ROLLING_CHECK_FOR_SPECIAL_PICKUP_OBSTRUCTION( ped, obj, sCurrentSpecialPickupState.sti, <<0.0, 0.0, 0.0>> ) = ", ROLLING_CHECK_FOR_SPECIAL_PICKUP_OBSTRUCTION( ped, obj, sCurrentSpecialPickupState.sti, <<0.0, 0.0, 0.0>> ))
							PRINTLN("GET_ENTITY_SPEED( ped ) = ", GET_ENTITY_SPEED( ped ))
							SUPPRESS_CONTEXT_BUTTON_ACTIONS_THIS_FRAME()
						ENDIF
					ELSE
						PRINTLN("[JS] BINBAG FAILED 2: ")
						PRINTLN("ROLLING_CHECK_FOR_SPECIAL_PICKUP_OBSTRUCTION( ped, obj, sCurrentSpecialPickupState.sti, <<0.0, 0.0, 0.0>> ) = ", ROLLING_CHECK_FOR_SPECIAL_PICKUP_OBSTRUCTION( ped, obj, sCurrentSpecialPickupState.sti, <<0.0, 0.0, 0.0>> ))
						PRINTLN("GET_ENTITY_SPEED( ped ) = ", GET_ENTITY_SPEED( ped ))
						SUPPRESS_CONTEXT_BUTTON_ACTIONS_THIS_FRAME()
						SET_BIN_BAG_HELP_TEXT_BIT( BINBAGHELP_PICKUP_OBSTRUCTED )
					ENDIF
				ELSE	
					PRINTLN("[JS] BINBAG FAILED 1: ")
					PRINTLN("IS_SAFE_FOR_BINBAG_PICKUP = ", IS_SAFE_FOR_BINBAG_PICKUP())
					PRINTLN("IS_PED_ROUGHLY_LEVEL_WITH_BINBAG( ped, obj ) = ", IS_PED_ROUGHLY_LEVEL_WITH_BINBAG( ped, obj ))
					SUPPRESS_CONTEXT_BUTTON_ACTIONS_THIS_FRAME()
					RELEASE_CONTEXT_INTENTION( iContext )
				ENDIF
				
				IF( sCurrentSpecialPickupState.ePickupState = eSpecialPickupState_BEFORE_PICKUP )
					obj = NULL
					iObjRequested = UNSET
				ENDIF
			BREAK
			
			// align the player ped with the current bin bag
			CASE eSpecialPickupState_ALIGNING
				IF( SHOULD_DROP_SPECIAL_PICKUP_CHECKS( obj ) )
					IF( NOT IS_PED_INJURED( ped ) )
						CLEAR_PED_TASKS( ped )
					ENDIF
					BROADCAST_BINBAG_REQUEST( iObjRequested, iPartToUse, TRUE )
					sCurrentSpecialPickupState.iCurrentlyRequestedObj = UNSET
					sCurrentSpecialPickupState.iCurrentlyOwnedBinbag = UNSET
					sCurrentSpecialPickupState.vPlayerTargetPos = <<0.0, 0.0, 0.0>>
					sCurrentSpecialPickupState.iPosition = UNSET
					iStage = 0
					SET_CURRENT_SPECIAL_PICKUP_STATE( eSpecialPickupState_BEFORE_PICKUP )
				ELSE
					UPDATE_BIN_BAG_PICKUP_ALIGNMENT( 	iStage, 
														ped, 
														obj, 
														sCurrentSpecialPickupState.vPlayerTargetPos,
														sCurrentSpecialPickupState.vPlayerTestPos,
														sCurrentSpecialPickupState.fPlayerTargetHeading,
														sCurrentSpecialPickupState.stiPickup,
														sCurrentSpecialPickupState.iPosition,
														iObjRequested,
														iObjOwned,
														sCurrentSpecialPickupState.iTimer )
				ENDIF
			BREAK
			
			// track the pickup animation and attach object
			CASE eSpecialPickupState_ATTACH_OBJECT_TO_PED
				UPDATE_BINBAG_PICKUP(	ped,
										obj,
										iObjRequested,
										iStage,
										sCurrentSpecialPickupState.vPlayerTargetPos,
										sCurrentSpecialPickupState.fPlayerTargetHeading,
										sCurrentSpecialPickupState.iTimer,
										sCurrentSpecialPickupState.iPosition,
										sCurrentSpecialPickupState.iSyncScene )
			BREAK
			
			// wait for the player to drop or deposit the object
			CASE eSpecialPickupState_CARRYING
				IF( iObjOwned < 0 OR SHOULD_DROP_SPECIAL_PICKUP_CHECKS( obj ) )
					RESET_SPECIAL_PICKUP()
					SET_CURRENT_SPECIAL_PICKUP_STATE( eSpecialPickupState_BEFORE_PICKUP )
				ELSE
					SET_BIT( MC_playerBD[ iPartToUse ].iClientBitSet2, PBBOOL2_BLOCK_SECONDARY_AND_IDLE_ANIMS )
					DISABLE_CELLPHONE_THIS_FRAME_ONLY()
					
					UPDATE_BIN_BAG_CARRYING( 	ped, 
												veh,
												sCurrentSpecialPickupState.sti, 
												iContext,
												sCurrentSpecialPickupState.bRightHandedThrow )
				ENDIF
			BREAK
			
			// track the put down anim and detach object
			CASE eSpecialPickupState_PUTTING_DOWN
				IF( iObjOwned < 0 OR SHOULD_DROP_SPECIAL_PICKUP_CHECKS( obj ) )
					RESET_SPECIAL_PICKUP()
					SET_CURRENT_SPECIAL_PICKUP_STATE( eSpecialPickupState_BEFORE_PICKUP )
				ELSE
					UPDATE_BINBAG_PUT_DOWN(	ped,
											obj,
											iObjRequested,
											iStage )
				ENDIF
			BREAK
			
			// aligning player ped for throwing bin bag
			CASE eSpecialPickupState_ALIGNING_FOR_THROW
				IF( iObjOwned < 0 OR SHOULD_DROP_SPECIAL_PICKUP_CHECKS( obj ) )
					RESET_SPECIAL_PICKUP()
					SET_CURRENT_SPECIAL_PICKUP_STATE( eSpecialPickupState_BEFORE_PICKUP )
				ELSE
					UPDATE_BIN_BAG_DROP_OFF_ALIGNMENT( 	iStage, 
														ped,
														obj,
														veh,
														iDropoffVehIndex,
														sCurrentSpecialPickupState.sti,
														sCurrentSpecialPickupState.bRightHandedThrow )
				ENDIF
			BREAK
			
			// handle deposit of the object with the sync scene
			CASE eSpecialPickupState_THROWING
				UPDATE_TRASH_TRUCK_DROP_OFF( 	iStage,											// INT &iStage
												ped, 											// PED_INDEX ped
												veh, 											// VEHICLE_INDEX veh
												obj, 											// OBJECT_INDEX obj
												iContext, 										// INT &iContext
												sCurrentSpecialPickupState.iTimer, 				// INT &iTimer
												iObjRequested, 									// INT iObj
												sCurrentSpecialPickupState.iSyncScene, 			// INT &iSyncScene
												iDropoffVehIndex, 								// INT &iVehRequest
												sCurrentSpecialPickupState.iObjSyncScene, 		// INT &iObjSyncScene
												sCurrentSpecialPickupState.bRightHandedThrow )	// BOOL bRightHandedThrow
			BREAK
			
			// waiting for back end to react to delivery
			CASE eSpecialPickupState_WAIT_FOR_DELIVERY
				CPRINTLN( DEBUG_SIMON, "PROCESS_BINBAG_PICKUP_STATE - eSpecialPickupState_WAIT_FOR_DELIVERY iPart:", iPartToUse )
				DELAYED_RESET()
			BREAK
			
			// resetting state
			CASE eSpecialPickupState_RESET
				CPRINTLN( DEBUG_SIMON, "PROCESS_BINBAG_PICKUP_STATE - Resetting special pickup state iPart:", iPartToUse )
				IF( DELAYED_RESET() )
					BROADCAST_BINBAG_REQUEST( iObjRequested, iPartToUse, TRUE )			
					iObjRequested = UNSET
					iObjOwned = UNSET
					obj = NULL
					SET_CURRENT_SPECIAL_PICKUP_STATE( eSpecialPickupState_BEFORE_PICKUP )
				ENDIF
			BREAK
			
		ENDSWITCH
		
		#IF IS_NEXTGEN_BUILD
		MANAGE_FIRST_PERSON_BINBAG( obj, sCurrentSpecialPickupState.objFirstPerson, ped, FALSE )
		#ENDIF
		
		IF( eOldState <> eState ) // the state has changed so recurse the state machine
			PROCESS_BINBAG_PICKUP_STATE( eState, iObjRequested, iObjOwned, obj, iContext, ped, iStage )
		ENDIF
	ENDIF
ENDPROC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: TIMETABLE SPECIAL PICKUP !
//
//************************************************************************************************************************************************************



FUNC BOOL UPDATE_TIMETABLE_PICKUP_ALIGNMENT( INT &iStage, PED_INDEX ped, OBJECT_INDEX obj )
	VECTOR 	vPos    = <<437.8943, -995.7000, 30.6302>>
	VECTOR  vPedPos = GET_ENTITY_COORDS( ped, FALSE )
	
	NETWORK_REQUEST_CONTROL_OF_ENTITY( obj )
	DISABLE_ALL_CONTROL_ACTIONS( PLAYER_CONTROL )
	DISABLE_CELLPHONE_THIS_FRAME_ONLY()
	
	SWITCH( iStage )
		CASE 0
			TASK_TURN_PED_TO_FACE_COORD( ped, vPos )
			iStage = 1
		BREAK
		CASE 1
			IF( NOT IS_PED_PERFORMING_TASK( ped, SCRIPT_TASK_TURN_PED_TO_FACE_COORD ) 
			OR  IS_PED_CLOSE_TO_HEADING( ped, GET_HEADING_BETWEEN_VECTORS( vPedPos, vPos ), 20.0 ) )
				iStage = 2
			ENDIF
		BREAK
		CASE 2
			iStage = 0
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_TIMETABLE_PICKUP_STATE( eSpecialPickupState eState, PED_INDEX ped, OBJECT_INDEX &obj, INT &iObj, INT &iStage )
	CONST_INT	UNSET 	-1
	INT 		iTeam 	= MC_playerBD[ iPartToUse ].iteam
	INT 		iRule 	= MC_serverBD_4.iCurrentHighestPriority[ iTeam ]
	
	IF( iRule < FMMC_MAX_RULES )
		SWITCH( eState )
			// waiting for player to attempt pickup of an object
			CASE eSpecialPickupState_BEFORE_PICKUP
				IF( iObj <> UNSET AND MC_serverBD.iObjHackPart[ iObj ] = UNSET )
					IF( NOT IS_PLAYER_CLIMBING( LocalPlayer )
					AND NOT IS_PED_RAGDOLL( ped )
					AND NOT IS_PED_DEAD_OR_DYING( ped )
					AND NOT IS_PED_FALLING( ped )
					AND NOT IS_ENTITY_IN_AIR( ped )
					AND NOT IS_PED_IN_ANY_VEHICLE( ped, TRUE ) )
						IF( IS_ENTITY_IN_ANGLED_AREA( ped, <<439.019409, -996.348206, 29.689573>>, <<438.997864, -994.662354, 31.189573>>, 1.000000 )
						AND GET_ENTITY_SPEED( ped ) < 4.5 )
							DISPLAY_HELP_TEXT_THIS_FRAME( "HEIST_HELP_19", FALSE ) // Press ~INPUT_CONTEXT~ to take the schedule.
							
							IF( IS_CONTROL_PRESSED( PLAYER_CONTROL, INPUT_CONTEXT ) )
								IF( NOT IS_CONTROL_PRESSED( PLAYER_CONTROL, INPUT_SELECT_WEAPON ) )
									IF( ARE_PICKUP_ASSETS_LOADED() )
										CPRINTLN( DEBUG_SIMON, "PROCESS_TIMETABLE_PICKUP_STATE - Requesting pickup for object ", iObj )
										CLEAR_PED_TASKS( ped )
										MC_playerBD[ iPartToUse ].iObjHacking = iObj // broadcast associate request
										NETWORK_REQUEST_CONTROL_OF_ENTITY( obj )

										SET_CURRENT_SPECIAL_PICKUP_STATE( eSpecialPickupState_ALIGNING )
										
										sCurrentSpecialPickupState.bObstructionExists = FALSE
									ELSE
										CASSERTLN( DEBUG_CONTROLLER, "PROCESS_TIMETABLE_PICKUP_STATE - Special pickups assets not loaded!" )
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE eSpecialPickupState_ALIGNING
				IF( iObj = UNSET 
				OR  SHOULD_DROP_SPECIAL_PICKUP_CHECKS( obj )
				OR  ( MC_serverBD.iObjHackPart[ iObj ] <> UNSET AND MC_serverBD.iObjHackPart[ iObj ] <> iPartToUse ) )
					CLEAR_PED_TASKS( ped )
					sCurrentSpecialPickupState.vPlayerTargetPos = <<0.0, 0.0, 0.0>>
					sCurrentSpecialPickupState.iPosition = -1
					iStage = 0
					SET_CURRENT_SPECIAL_PICKUP_STATE( eSpecialPickupState_BEFORE_PICKUP )
				ELSE
					IF( UPDATE_TIMETABLE_PICKUP_ALIGNMENT( iStage, ped, obj ) )
						SET_CURRENT_SPECIAL_PICKUP_STATE( eSpecialPickupState_REQUESTING )
					ENDIF
				ENDIF
			BREAK
			
			// wait for the server to update with associated pickup and then play the pickup anim
			CASE eSpecialPickupState_REQUESTING
				DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_CONTEXT )
				DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_PHONE )
				DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_SELECT_WEAPON )
				DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_MOVE_LR )
				DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_MOVE_UD )
				DISABLE_CELLPHONE_THIS_FRAME_ONLY()
				
				IF( iObj <> UNSET AND MC_serverBD.iObjHackPart[ iObj ] = iPartToUse ) // if the locally selected pickup has updated on the server
					IF( NETWORK_HAS_CONTROL_OF_ENTITY( obj ) )
						CPRINTLN( DEBUG_SIMON, "PROCESS_TIMETABLE_PICKUP_STATE - Starting pickup anim for object ", iObj )
						NETWORK_DISABLE_PROXIMITY_MIGRATION( NETWORK_GET_NETWORK_ID_FROM_ENTITY( obj ) )
						SET_CURRENT_SPECIAL_PICKUP_STORED_WEAPON( GET_CURRENT_PLAYER_WEAPON_TYPE() )
						SET_CURRENT_PED_WEAPON( ped, WEAPONTYPE_UNARMED, TRUE )
						PLAY_SPECIAL_PICKUP_ANIM( obj, ePickupAnim_PICKUP, iObj )
						sCurrentSpecialPickupState.iCurrentlyRequestedObj = iObj
						SET_CURRENT_SPECIAL_PICKUP_STATE( eSpecialPickupState_ATTACH_OBJECT_TO_PED )
					ELSE
						CPRINTLN( DEBUG_SIMON, "PROCESS_TIMETABLE_PICKUP_STATE - Requesting network ownership of ", iObj )
						NETWORK_REQUEST_CONTROL_OF_ENTITY( obj )
					ENDIF
				ELIF( iObj <> UNSET AND MC_serverBD.iObjHackPart[ iObj ] <> UNSET ) // someone else has picked up the object
					CPRINTLN( DEBUG_SIMON, "PROCESS_TIMETABLE_PICKUP_STATE - iPart:", MC_serverBD.iObjHackPart[ iObj ],
						" started using iObj:", iObj ," returning to BEFORE_PICKUP state. This iPart is ", iPartToUse )
					SET_CURRENT_SPECIAL_PICKUP_STATE( eSpecialPickupState_BEFORE_PICKUP )
				ELIF( iObj = UNSET )
					CPRINTLN( DEBUG_SIMON, "PROCESS_TIMETABLE_PICKUP_STATE - The local hack obj is invalid ", iObj ,", returning to BEFORE_PICKUP state")
					SET_CURRENT_SPECIAL_PICKUP_STATE( eSpecialPickupState_BEFORE_PICKUP )
				ENDIF
			BREAK
			
			// track the pickup animation and attach object
			CASE eSpecialPickupState_ATTACH_OBJECT_TO_PED
				IF( SHOULD_DROP_SPECIAL_PICKUP_CHECKS( obj ) )
					RESET_SPECIAL_PICKUP()		
					SET_CURRENT_SPECIAL_PICKUP_STATE( eSpecialPickupState_BEFORE_PICKUP )
				ELSE	
					DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_CONTEXT )
					DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_PHONE )
					DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_MOVE_LR )
					DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_MOVE_UD )
					DISABLE_CELLPHONE_THIS_FRAME_ONLY()
				
					IF( NOT IS_SPECIAL_PICKUP_ANIM_PLAYING( obj, ePickupAnim_PICKUP, iObj ) )
						IF( NOT IS_ENTITY_ATTACHED( obj ) )
							CPRINTLN( DEBUG_SIMON, "PROCESS_TIMETABLE_PICKUP_STATE - Attaching object ",iobj," and moving to eSpecialPickupState_WAIT_FOR_DELIVERY")
							ATTACH_ENTITY_TO_ENTITY( obj, ped, GET_PED_BONE_INDEX( ped, BONETAG_PH_L_HAND ),
								<<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, TRUE, TRUE, FALSE )
							PLAY_SOUND_FRONTEND( -1, "Bus_Schedule_Pickup", "DLC_PRISON_BREAK_HEIST_SOUNDS", FALSE)
							SET_ENTITY_VISIBLE( obj, FALSE )
							MC_playerBD[ iPartToUse ].iObjNear = iObj
							
							obj = NULL
							SET_CURRENT_SPECIAL_PICKUP_STATE( eSpecialPickupState_WAIT_FOR_DELIVERY )
							//Clean up any hidden remote players done here as well just in case anims end wihtout reaching end phase.			
							INT iPartLoop
							PLAYER_INDEX tempPlayer
							FOR iPartLoop = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
								IF iPartLoop != iPartToUse // if not local player
									PARTICIPANT_INDEX pi
									pi = INT_TO_PARTICIPANTINDEX(iPartLoop)
									IF( NETWORK_IS_PARTICIPANT_ACTIVE( pi ) )
										tempPlayer = NETWORK_GET_PLAYER_INDEX( pi )	
										If tempPlayer != INVALID_PLAYER_INDEX()
											IF IS_NET_PLAYER_OK(tempPlayer)
												NETWORK_CONCEAL_PLAYER(tempPlayer,false)
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDFOR
						ENDIF
					ELSE					
						IF( ( GET_PICKUP_ANIM_PROGRESS( obj, ePickupAnim_PICKUP, iObj ) > 0.7
						OR  HAS_ANIM_EVENT_FIRED( LocalPlayerPed, GET_HASH_KEY( "destroy" ) ) )
						AND IS_ENTITY_VISIBLE( obj ) )
						
							SET_ENTITY_VISIBLE( obj, FALSE )
							//Clean up any hidden remote players here.			
							/*INT iPartLoop
							PLAYER_INDEX tempPlayer
							FOR iPartLoop = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
								IF iPartLoop != iPartToUse // if not local player
									PARTICIPANT_INDEX pi
									pi = INT_TO_PARTICIPANTINDEX(iPartLoop)
									IF( NETWORK_IS_PARTICIPANT_ACTIVE( pi ) )
										tempPlayer = NETWORK_GET_PLAYER_INDEX( pi )	
										tempPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPartLoop))						
										IF tempPlayer != INVALID_PLAYER_INDEX()
											IF IS_NET_PLAYER_OK(tempPlayer) 
												NETWORK_CONCEAL_PLAYER(tempPlayer,false)
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDFOR*/
						ELSE
							INT iPartLoop
							PLAYER_INDEX tempPlayer
							
							//Fix for b*2212006 - Check player is concealed
							FOR iPartLoop = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
								IF iPartLoop != iPartToUse // if not local player
									PARTICIPANT_INDEX pi
									pi = INT_TO_PARTICIPANTINDEX(iPartLoop)
									IF( NETWORK_IS_PARTICIPANT_ACTIVE( pi ) )
										tempPlayer = NETWORK_GET_PLAYER_INDEX( pi )	
										tempPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPartLoop))						
										IF tempPlayer != INVALID_PLAYER_INDEX()
											IF IS_NET_PLAYER_OK(tempPlayer)
												IF NOT NETWORK_IS_PLAYER_CONCEALED(tempPlayer)
													NETWORK_CONCEAL_PLAYER(tempPlayer,TRUE)
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDFOR
						ENDIF
						
						HIDE_HUD_AND_RADAR_THIS_FRAME()
					ENDIF
				ENDIF
			BREAK
			
			CASE eSpecialPickupState_WAIT_FOR_DELIVERY
				CPRINTLN( DEBUG_SIMON, "PROCESS_TIMETABLE_PICKUP_STATE - eSpecialPickupState_WAIT_FOR_DELIVERY" )
			BREAK
			
			CASE eSpecialPickupState_RESET
				CPRINTLN( DEBUG_SIMON, "PROCESS_TIMETABLE_PICKUP_STATE - Resetting special pickup state" )
				MC_playerBD[ iPartToUse ].iObjHacking = UNSET
				SET_BIT( iObjDeliveredBitset, iObj )
				SET_CURRENT_SPECIAL_PICKUP_STATE( eSpecialPickupState_BEFORE_PICKUP )
			BREAK
			
		ENDSWITCH
	ENDIF
ENDPROC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: PROCESS SPECIAL PICKUP STATE (main special pickup processing) !
//
//************************************************************************************************************************************************************



PROC PROCESS_SPECIAL_PICKUP_STATE()
	CONST_INT	UNSET	-1
	
	IF( sCurrentSpecialPickupState.ePickupState = eSpecialPickupState_BEFORE_PICKUP )
		INT iObj
		IF( GET_CLOSEST_VALID_SPECIAL_OBJECT( iObj ) )
			IF( GET_SPECIAL_OBJECT( iObj, sCurrentSpecialPickupState.obj ) )
				SWITCH( GET_ENTITY_MODEL( sCurrentSpecialPickupState.obj ) )
					CASE HEI_PROP_HEI_TIMETABLE
						IF( MC_serverBD.iObjHackPart[ iObj ] = UNSET )
							PROCESS_TIMETABLE_PICKUP_STATE(	sCurrentSpecialPickupState.ePickupState,
															LocalPlayerPed,
															sCurrentSpecialPickupState.obj,
															iObj,
															sCurrentSpecialPickupState.iStage )
						ENDIF
					BREAK
					CASE HEI_PROP_HEIST_BINBAG
						sCurrentSpecialPickupState.iCurrentlyRequestedObj = iObj
					BREAK
				ENDSWITCH
			ELSE
				sCurrentSpecialPickupState.bObstructionExists = TRUE
				IF( sCurrentSpecialPickupState.iContext <> UNSET )
				 	RELEASE_CONTEXT_INTENTION( sCurrentSpecialPickupState.iContext )
				ENDIF
			ENDIF
		ELSE
			sCurrentSpecialPickupState.bObstructionExists = TRUE
			IF( sCurrentSpecialPickupState.iContext <> UNSET )
			 	RELEASE_CONTEXT_INTENTION( sCurrentSpecialPickupState.iContext )
			ENDIF
		ENDIF
	ENDIF
	
	IF( DOES_ENTITY_EXIST( sCurrentSpecialPickupState.obj ) )
		SWITCH( GET_ENTITY_MODEL( sCurrentSpecialPickupState.obj ) )
			CASE HEI_PROP_HEI_TIMETABLE
				IF( sCurrentSpecialPickupState.ePickupState <> eSpecialPickupState_BEFORE_PICKUP )
					PROCESS_TIMETABLE_PICKUP_STATE(	sCurrentSpecialPickupState.ePickupState,
													LocalPlayerPed,
													sCurrentSpecialPickupState.obj,
													MC_playerBD[ iPartToUse ].iObjHacking,
													sCurrentSpecialPickupState.iStage )
				ENDIF
			BREAK
			
			CASE HEI_PROP_HEIST_BINBAG
				PROCESS_BINBAG_PICKUP_STATE(	sCurrentSpecialPickupState.ePickupState, 
												sCurrentSpecialPickupState.iCurrentlyRequestedObj,
												sCurrentSpecialPickupState.iCurrentlyOwnedBinbag,
												sCurrentSpecialPickupState.obj,
												sCurrentSpecialPickupState.iContext,
												LocalPlayerPed,
												sCurrentSpecialPickupState.iStage )
			BREAK
		ENDSWITCH
	ELSE
		IF( sCurrentSpecialPickupState.ePickupState = eSpecialPickupState_RESET
		AND sCurrentSpecialPickupState.iCurrentlyRequestedObj >= 0
		AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[ sCurrentSpecialPickupState.iCurrentlyRequestedObj ].mn = HEI_PROP_HEIST_BINBAG )
			PROCESS_BINBAG_PICKUP_STATE(	sCurrentSpecialPickupState.ePickupState, 
											sCurrentSpecialPickupState.iCurrentlyRequestedObj,
											sCurrentSpecialPickupState.iCurrentlyOwnedBinbag,
											sCurrentSpecialPickupState.obj,
											sCurrentSpecialPickupState.iContext,
											LocalPlayerPed,
											sCurrentSpecialPickupState.iStage )
		ENDIF
	ENDIF
	PRINT_HIGHEST_PRIORITY_BIN_BAG_HELP_TEXT()
ENDPROC



//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//
//!!! NEW SECTION BELOW: SERVER OBJECT PROCESSING / OBJ BRAIN !!!
//
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************

FUNC BOOL IS_PLAYER_IN_PLANE_IN_WATER(PED_INDEX tempped)
	IF NOT IS_PED_INJURED(tempped)
		IF IS_PED_A_PLAYER(tempped)
			IF IS_PED_IN_ANY_PLANE(tempped) 
				VEHICLE_INDEX vehTemp = GET_VEHICLE_PED_IS_IN(tempped)
				IF IS_VEHICLE_IN_WATER(vehTemp)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_TEAM_HOLDING_PACKAGE(INT iobj)

	INT iteam
	INT ireturnteam = -1
	
	FOR iteam = 0 to (FMMC_MAX_TEAMS-1)
		IF IS_BIT_SET(MC_serverBD.iObjAtYourHolding[iteam],iobj)
			ireturnteam = iteam
		ENDIF
	ENDFOR
	
	RETURN ireturnteam

ENDFUNC

PROC REMOVE_OBJECT_HOLDING_POINTS(INT iteam,INT iobj)

INT ipart
PARTICIPANT_INDEX tempPart
PLAYER_INDEX tempPlayer

	IF IS_BIT_SET(MC_serverBD.iObjAtYourHolding[iteam],iobj)
		CLEAR_BIT(MC_serverBD.iObjAtYourHolding[iteam],iobj)
		PRINTLN("[RCC MISSION]  SETTING PACKAGE NOT AT HOLDING: ",iobj," FOR TEAM: ", iteam)
		BOOL bRemovedPlayerScore
		FOR ipart = 0 TO GET_MAXIMUM_PLAYERS_ON_MISSION()-1
			IF IS_BIT_SET(MC_serverBD.iObjHoldPartPoints[iobj],ipart)
				tempPart = INT_TO_PARTICIPANTINDEX(ipart)
				IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciENABLE_SHARD_ON_OBJECT_STOLEN)
						IF MC_serverBD.iObjCarrier[iObj] != -1
							PRINTLN("[RCC MISSION] BROADCAST_FMMC_STOLEN_FLAG_SHARD - Object ", iObj, " Stolen. Player Score Shard")
							BROADCAST_FMMC_STOLEN_FLAG_SHARD(INT_TO_PLAYERINDEX(MC_serverBD.iObjCarrier[iObj]), MC_PlayerBD[MC_serverBD.iObjCarrier[iObj]].iteam, iTeam, iObj, GET_STOCKPILE_LAST_PICKUP_CAPTOR(iObj))
							
							IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_STOCKPILE_SOUNDS)
								BROADCAST_FMMC_STOCKPILE_SOUND(ciStockpile_Sound_Collect, MC_serverBD.iObjCarrier[iobj])
								PRINTLN("[RCC MISSION][StockpileSound] Playing ciStockpile_Sound_Collect sound (2)")
							ENDIF
						ELSE
							PRINTLN("[RCC MISSION] BROADCAST_FMMC_STOLEN_FLAG_SHARD - MC_serverBD.iObjCarrier[",iObj,"] = -1")
						ENDIF
					ENDIF
					
					tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
					INCREMENT_SERVER_REMOTE_PLAYER_SCORE(tempPlayer,(-1 * GET_FMMC_POINTS_FOR_TEAM(iteam,MC_serverBD_4.iObjPriority[iobj][iteam])),iteam,MC_serverBD_4.iObjPriority[iobj][iteam])
									
				ENDIF
				bRemovedPlayerScore = TRUE
				CLEAR_BIT(MC_serverBD.iObjHoldPartPoints[iobj],ipart)
			ENDIF
		ENDFOR
		IF NOT bRemovedPlayerScore
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciENABLE_SHARD_ON_OBJECT_STOLEN)
				IF MC_serverBD.iObjCarrier[iObj] != -1
					PRINTLN("[RCC MISSION] BROADCAST_FMMC_STOLEN_FLAG_SHARD - Object ", iObj, " Stolen. Team Score Shard")
					BROADCAST_FMMC_STOLEN_FLAG_SHARD(INT_TO_PLAYERINDEX(MC_serverBD.iObjCarrier[iObj]), MC_PlayerBD[MC_serverBD.iObjCarrier[iObj]].iteam, iTeam, iObj, GET_STOCKPILE_LAST_PICKUP_CAPTOR(iObj))
				ELSE
					PRINTLN("[RCC MISSION] BROADCAST_FMMC_STOLEN_FLAG_SHARD - MC_serverBD.iObjCarrier[",iObj,"] = -1")
				ENDIF
			ENDIF
			//Remove points, pass in a negative amount:
			INCREMENT_SERVER_TEAM_SCORE(iteam, MC_serverBD_4.iObjPriority[iobj][iteam], -GET_FMMC_POINTS_FOR_TEAM(iteam,MC_serverBD_4.iObjPriority[iobj][iteam]))
		ENDIF
	ENDIF

ENDPROC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: SERVER BREAKABLE OBJECT MANAGEMENT (weapon crates, CCTV cameras) !
//
//************************************************************************************************************************************************************



//PURPOSE: Checks if there are any crate pick ups that need to be cleaned up and removes them.
PROC CLEANUP_CRATE_PICKUPS(INT iObj)
	INT iPickup

	FOR iPickup = 0 TO (ciMAX_CRATE_PICKUPS-1)
		IF NOT IS_VECTOR_ZERO(MC_serverBD_3.vCratePickupCoords[iObj][iPickup])
		AND SHOULD_CLEANUP_OBJ(iObj, NULL)
			IF DOES_ENTITY_EXIST(piCratePickup[iObj][iPickup].oiPickup)
				DELETE_AMBIENT_PICKUP_WITH_SAP_PICKUP_ID(piCratePickup[iObj][iPickup])
				PRINTLN("[RCC MISSION] CLEANUP_CRATE_PICKUPS - Cleaning Up Pickup ", iPickup," of Object ", iObj, " early.")
			ENDIF
		ENDIF
	ENDFOR

ENDPROC

//Handles breaking of crates and CCTV cameras
PROC SERVER_MANAGE_BREAKABLE_OBJECTS(OBJECT_INDEX tempObj, INT iobj, BOOL bExists)

//BOOL bGotAllSpawnPoints = TRUE
BOOL bnotignore
INT iParticipant,iteam	//,ipickup
INT iKillerTeam = -1
INt idamage
PARTICIPANT_INDEX temppart
PLAYER_INDEX tempplayer, killerplayer
	
	IF IS_BIT_SET(MC_serverBD.iFragableCrate, iobj)
	OR IS_THIS_MODEL_A_CCTV_CAMERA(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].mn)
		
		IF( (NOT IS_BIT_SET(MC_serverBD.iFragableCrate, iobj))
			AND NOT ( IS_OBJECT_BROKEN_AND_VISIBLE(tempObj)
					  OR IS_ENTITY_DEAD(tempObj) ) )
			//Do nothing
		ELIF ( IS_BIT_SET(MC_serverBD.iFragableCrate, iobj)
			   AND NOT IS_BIT_SET(MC_serverBD.iCrateBrokenBitset, iobj) )
			//Do nothing - what used to be here is now done in PROCESS_DESTRUCTIBLE_CRATES (as that's run every frame to make the pickups appear quicker)
		ELSE
			CLEANUP_CRATE_PICKUPS(iObj)
			
			IF NOT IS_BIT_SET(MC_serverBD.iCrateDestructionProcessed, iobj)
			AND (NOT DOES_ENTITY_EXIST(tempObj) OR IS_ENTITY_VISIBLE(tempObj))
				
				#IF IS_DEBUG_BUILD
				IF bExists
					IF IS_OBJECT_BROKEN_AND_VISIBLE(tempObj)
						PRINTLN("[RCC MISSION] SERVER_MANAGE_BREAKABLE_OBJECTS - Object ",iobj," is broken!")
					ENDIF
					IF IS_ENTITY_DEAD(tempObj)
						PRINTLN("[RCC MISSION] SERVER_MANAGE_BREAKABLE_OBJECTS - Object ",iobj," is dead!")
					ENDIF
				ENDIF
				#ENDIF
				
				IF bExists
				AND IS_DAMAGE_TRACKER_ACTIVE_ON_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niObject[iobj])
					REPEAT GET_MAXIMUM_PLAYERS_ON_MISSION() iParticipant
						temppart = INT_TO_PARTICIPANTINDEX(iParticipant)
						IF NETWORK_IS_PARTICIPANT_ACTIVE(temppart)
							tempplayer = NETWORK_GET_PLAYER_INDEX(temppart)
							IF IS_NET_PLAYER_OK(tempplayer,FALSE)
								PRINTLN("[RCC MISSION] SERVER_MANAGE_BREAKABLE_OBJECTS - Checking if part ",iParticipant," (on team ",MC_playerBD[iParticipant].iteam,") has damaged object ",iobj)
								IF NETWORK_GET_ASSISTED_DAMAGE_OF_ENTITY(tempplayer,tempObj,idamage)
									IF idamage > 0
										KillerPlayer = tempplayer
										iKillerTeam = MC_playerBD[iParticipant].iteam
										PRINTLN("[RCC MISSION] SERVER_MANAGE_BREAKABLE_OBJECTS - Part ",iParticipant," (on iKillerTeam ",iKillerTeam,") did damage object ",iobj)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDREPEAT
				ENDIF
				
				IF iKillerTeam > -1
					IF MC_serverBD_4.iObjPriority[iobj][iKillerTeam] <= MC_serverBD_4.iCurrentHighestPriority[iKillerTeam]				
						BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_KILL_OBJ,0,iKillerTeam,-1,KillerPlayer,ci_TARGET_OBJECT,iobj)
						IF MC_serverBD_4.iObjPriority[iobj][iKillerTeam] < FMMC_MAX_RULES
							IF MC_serverBD_4.iObjRule[iobj][iKillerTeam] = FMMC_OBJECTIVE_LOGIC_KILL
								BROADCAST_OBJECTIVE_MID_POINT(MC_serverBD_4.iObjPriority[iobj][iKillerTeam],iKillerTeam)
								INCREMENT_SERVER_REMOTE_PLAYER_SCORE(KillerPlayer, GET_FMMC_POINTS_FOR_TEAM(iKillerTeam,MC_serverBD_4.iObjPriority[iobj][iKillerTeam]), iKillerTeam, MC_serverBD_4.iObjPriority[iobj][iKillerTeam])
								PRINTLN("[RCC MISSION] SERVER_MANAGE_BREAKABLE_OBJECTS - Fragable object ",iobj," killed & is current priority for killer team ",iKillerTeam)
							ENDIF
						ENDIF
					ELSE
						FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
							IF MC_serverBD_4.iObjPriority[iobj][iteam] < FMMC_MAX_RULES
								bnotignore = TRUE
							ENDIF
						ENDFOR
						IF bnotignore
							PRINTLN("[RCC MISSION] SERVER_MANAGE_BREAKABLE_OBJECTS - Fragable object ",iobj," killed, but isn't current highest priority for killer team ",iKillerTeam)
							BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_KILL_OBJ,0,iKillerTeam,-1,KillerPlayer,ci_TARGET_OBJECT,iobj)
						ENDIF
					ENDIF
				ELSE
					PRINTLN("[RCC MISSION] SERVER_MANAGE_BREAKABLE_OBJECTS - Fragable object ",iobj," killed, but couldn't find a killer team")
					GIVE_TEAM_POINTS_FOR_OBJ_KILL(iobj, tempObj)
				ENDIF
				
				IF IS_BIT_SET(MC_serverBD.iFragableCrate, iobj)
					IF bExists
						PRINTLN("[RCC MISSION] SERVER_MANAGE_BREAKABLE_OBJECTS - Cleaning up fragable crate, object ",iobj)
						CLEANUP_NET_ID(MC_serverBD_1.sFMMC_SBD.niObject[iobj])
					ENDIF
				ELSE
					FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
						IF MC_serverBD_4.iObjPriority[iobj][iteam] < FMMC_MAX_RULES
							//Also completes the objective:
							PRINTLN("[RCC MISSION] SERVER_MANAGE_BREAKABLE_OBJECTS - Running fail checks for team ",iteam," as fragable object ",iobj," has been destroyed")
							RUN_TEAM_OBJ_FAIL_CHECKS(iobj, iteam, MC_serverBD_4.iObjPriority[iobj][iteam])
						ENDIF
					ENDFOR
				ENDIF
				
				SET_BIT(MC_serverBD.iCrateDestructionProcessed,iobj)
			ENDIF
		ENDIF
		
	ENDIF
	
ENDPROC



//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: OBJECT CLEANUP FUNCTIONS !
//
//************************************************************************************************************************************************************



FUNC BOOL IS_CRATE_STUCK(OBJECT_INDEX tempObj,INT iobj)

    CONST_FLOAT SAFE_HANDLER_CONTAINER_ROT              35.0
	VECTOR vHandlerContainerRot = GET_ENTITY_ROTATION(tempObj)

	IF GET_TEAM_HOLDING_PACKAGE(iobj) > -1
		RETURN FALSE
	ENDIF
	
    IF NOT HAS_NET_TIMER_STARTED(tdCrateFailTimer[iobj])
        IF vHandlerContainerRot.y > SAFE_HANDLER_CONTAINER_ROT OR vHandlerContainerRot.y < -SAFE_HANDLER_CONTAINER_ROT
           REINIT_NET_TIMER(tdCrateFailTimer[iobj])
           PRINTLN("starting Fail timer for Y: ",iobj)
        ENDIF
		IF vHandlerContainerRot.x > SAFE_HANDLER_CONTAINER_ROT OR vHandlerContainerRot.x < -SAFE_HANDLER_CONTAINER_ROT
			REINIT_NET_TIMER(tdCrateFailTimer[iobj])
            PRINTLN("starting Fail timer for x: ",iobj)
        ENDIF
    ELSE 
		IF vHandlerContainerRot.y < SAFE_HANDLER_CONTAINER_ROT AND vHandlerContainerRot.y > -SAFE_HANDLER_CONTAINER_ROT
		AND vHandlerContainerRot.x < SAFE_HANDLER_CONTAINER_ROT AND vHandlerContainerRot.x > -SAFE_HANDLER_CONTAINER_ROT 
           RESET_NET_TIMER(tdCrateFailTimer[iobj])
           PRINTLN("STOP Fail timer: ",iobj)
		   RETURN FALSE
        ENDIF
		IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdCrateFailTimer[iobj]) > 20000
			RETURN TRUE
		ENDIF
    ENDIF

	RETURN FALSE

ENDFUNC

//// PURPOSE: This function gets the correct string name for the ptfx to play when an object is deleted
 ///    
 /// PARAMS:
 ///    tempModel - The model name for the object we've just dleted
 /// RETURNS: the string for the ptfx to play for that model
 ///    
FUNC STRING GET_REMOVAL_PTFX_NAME(MODEL_NAMES tempModel)

STRING tempName = "scr_mp_generic_dst"

	SWITCH tempModel
	
		CASE PROP_DRUG_PACKAGE
		CASE PROP_LD_CASE_01
			tempName = "scr_mp_drug_dst"
		BREAK
		
		CASE PROP_LAPTOP_01A
		CASE PROP_BOMB_01
			tempName = "scr_mp_elec_dst"
		BREAK
		
	ENDSWITCH
	
	RETURN tempName
	
ENDFUNC

PROC CLEAN_UP_CONTAINER_DOORS()
	
	INT iContainerObjectIndex
	
	IF DO_CRATE_DOORS_EXIST( MC_serverBD_1.niMinigameCrateDoors, FALSE )
		FOR iContainerObjectIndex = 0 TO ciMAX_MINIGAME_CRATE_DOORS - 1
			IF NETWORK_DOES_NETWORK_ID_EXIST( MC_serverBD_1.niMinigameCrateDoors[ iContainerObjectIndex ] )
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.niMinigameCrateDoors[ iContainerObjectIndex ] )
					DETACH_ENTITY( NET_TO_OBJ( MC_serverBD_1.niMinigameCrateDoors[ iContainerObjectIndex ] ) )
				ENDIF
				CLEANUP_NET_ID( MC_serverBD_1.niMinigameCrateDoors[ iContainerObjectIndex ] )
			ENDIF
		ENDFOR
	ELSE
		CPRINTLN( DEBUG_CONTROLLER, "{RCC MISSION] Trying to clean up the hack container doors when we've already done so!" )
	ENDIF

ENDPROC

PROC KEEP_OBJECT_IN_ZONE(OBJECT_INDEX Obj, INT iZone)
	VECTOR vObjCoords = GET_ENTITY_COORDS(Obj)
	PRINTLN("[JS] - KEEP_OBJECT_IN_ZONE - vObjCoords = ", vObjCoords)
	IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iAreaType = ciFMMC_ZONE_SHAPE__ANGLED_AREA
		PRINTLN("[JS] - KEEP_OBJECT_IN_ZONE - Angled area")
		IF NOT IS_ENTITY_IN_ANGLED_AREA(Obj, g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[0], g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[1], g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].fRadius)
			vObjCoords = GET_CLOSEST_POINT_AT_EDGE_OF_BOUNDS(vObjCoords, g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[0], g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[1], g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].fRadius)
			PRINTLN("[JS] - KEEP_OBJECT_IN_ZONE - vObjCoords at edge: ", vObjCoords)
			FLOAT fZ
			IF GET_GROUND_Z_FOR_3D_COORD(vObjCoords, fZ)
				vObjCoords.z = fZ
				vObjCoords.z = CHECK_Z_FOR_OVERHANG(vObjCoords)
				PRINTLN("[JS] - KEEP_OBJECT_IN_ZONE - GET_GROUND_Z_FOR_3D_COORD returned true")
			ELSE
				PRINTLN("[JS] - KEEP_OBJECT_IN_ZONE - GET_GROUND_Z_FOR_3D_COORD returned false")
				vObjCoords = (g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[0] + g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[1]) * 0.5
				IF GET_GROUND_Z_FOR_3D_COORD(vObjCoords, fZ)
					vObjCoords.z = fZ
					PRINTLN("[JS] - KEEP_OBJECT_IN_ZONE - BACKUP PLACING AT CENTER -  FOUND A VALID GROUND Z = ", fZ)
				ELSE
					PRINTLN("[JS] - KEEP_OBJECT_IN_ZONE - BACKUP PLACING AT CENTER -  NO VALID Z FOUND, LIKELY SOME STRANGE PLACEMENT")
				ENDIF
				PRINTLN("[JS] - KEEP_OBJECT_IN_ZONE - BACKUP PLACING AT CENTER - new coords: ", vObjCoords)
			ENDIF
			PRINTLN("[JS] - KEEP_OBJECT_IN_ZONE - moving obj to new coords: ", vObjCoords)
			SET_ENTITY_COORDS(Obj, vObjCoords)
		ENDIF
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iAreaType = ciFMMC_ZONE_SHAPE__SPHERE
		PRINTLN("[JS] - KEEP_OBJECT_IN_ZONE - Sphere area")
		IF VDIST2(vObjCoords, g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[0]) > POW(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].fRadius, 2)
			VECTOR vDir = g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[0] - vObjCoords
			vDir = NORMALISE_VECTOR(vDir)
			vObjCoords = g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[0] + (vDir * g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].fRadius)
			GET_GROUND_Z_FOR_3D_COORD(vObjCoords, vObjCoords.Z)
			PRINTLN("[JS] - KEEP_OBJECT_IN_ZONE - moving obj to new coords: ", vObjCoords)
			SET_ENTITY_COORDS(Obj, vObjCoords)
		ENDIF
	ENDIF
ENDPROC

PROC KEEP_OBJECT_OUT_OF_ZONE(OBJECT_INDEX Obj, INT iZone, INT iObj)
	VECTOR vObjCoords = GET_ENTITY_COORDS(Obj)
	PRINTLN("[JS] - KEEP_OBJECT_OUT_OF_ZONE - vObjCoords = ", vObjCoords)
	IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iAreaType = ciFMMC_ZONE_SHAPE__ANGLED_AREA
		PRINTLN("[JS] - KEEP_OBJECT_OUT_OF_ZONE - Angled area")
		IF IS_ENTITY_IN_ANGLED_AREA(Obj, g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[0], g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[1], g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].fRadius)
			vObjCoords = GET_CLOSEST_POINT_AT_EDGE_OF_BOUNDS(vObjCoords, g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[0], g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[1], g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].fRadius + 1.0)
			PRINTLN("[JS] - KEEP_OBJECT_OUT_OF_ZONE - vObjCoords at edge: ", vObjCoords)
			FLOAT fZ
			IF GET_GROUND_Z_FOR_3D_COORD(vObjCoords, fZ)
				vObjCoords.z = fZ
				vObjCoords.z = CHECK_Z_FOR_OVERHANG(vObjCoords)
				PRINTLN("[JS] - KEEP_OBJECT_OUT_OF_ZONE - GET_GROUND_Z_FOR_3D_COORD returned true")
			ELSE
				PRINTLN("[JS] - KEEP_OBJECT_OUT_OF_ZONE - GET_GROUND_Z_FOR_3D_COORD returned false putting back at spawn")
				vObjCoords = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].vPos
			ENDIF
			PRINTLN("[JS] - KEEP_OBJECT_OUT_OF_ZONE - moving obj to new coords: ", vObjCoords)
			SET_ENTITY_COORDS(Obj, vObjCoords)
		ENDIF
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iAreaType = ciFMMC_ZONE_SHAPE__SPHERE
		PRINTLN("[JS] - KEEP_OBJECT_OUT_OF_ZONE - Sphere area")
		IF VDIST2(vObjCoords, g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[0]) < POW(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].fRadius, 2)
			VECTOR vDir = g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[0] - vObjCoords
			vDir = NORMALISE_VECTOR(vDir)
			vObjCoords = g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[0] + (vDir * g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].fRadius + 1.0)
			GET_GROUND_Z_FOR_3D_COORD(vObjCoords + 1.0, vObjCoords.Z)
			PRINTLN("[JS] - KEEP_OBJECT_OUT_OF_ZONE - moving obj to new coords: ", vObjCoords)
			SET_ENTITY_COORDS(Obj, vObjCoords)
		ENDIF
	ENDIF
ENDPROC

PROC KEEP_OBJECT_IN_DROP_OFF(OBJECT_INDEX objIndex, INT iObj)
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam]
	
	IF iRule < FMMC_MAX_RULES
		VECTOR vObjCoords = GET_ENTITY_COORDS(objIndex)
		INT iTeam
		FOR iTeam = 0 TO FMMC_MAX_TEAMS - 1
			IF NETWORK_HAS_CONTROL_OF_ENTITY(objIndex)
			AND NOT IS_ENTITY_ATTACHED_TO_ANY_PED(objIndex)
				IF IS_BIT_SET(MC_serverBD.iObjAtYourHolding[iTeam], iobj)
					IF NOT IS_POINT_IN_ANGLED_AREA(vObjCoords, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vDropOff[iRule], 
					g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vDropOff2[iRule], 
					g_FMMC_STRUCT.sFMMCEndConditions[iTeam].fDropOffRadius[iRule])
						
						VECTOR vDropOffCenter = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vDropOff[iRule] + g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vDropOff2[iRule]
						vDropOffCenter *= << 0.5, 0.5, 0.5 >>
						PRINTLN("[RCC MISSION][KEEP_OBJECT_IN_DROP_OFF] - Moving Object ",iObj, " to vDropOffCenter = ", vDropOffCenter)
						SET_ENTITY_COORDS(objIndex, vDropOffCenter)
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
		
	ENDIF
ENDPROC

PROC KEEP_OBJECT_IN_BOUNDS(OBJECT_INDEX objIndex, BOOL bCheckOverhang = FALSE)
	PRINTLN("[KEEP_OBJECT_IN_BOUNDS]")
	INT iTeam = MC_playerBD[iPartToUse].iTeam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]

	
	IF iRule < FMMC_MAX_RULES
		VECTOR vObjCoords = GET_ENTITY_COORDS(objIndex)
		PRINTLN("[KEEP_OBJECT_IN_BOUNDS] vObjCoords = <<", vObjCoords.X, ", ", vObjCoords.Y, ", ", vObjCoords.Z, ">>")
		IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].iType = 1
			PRINTLN("[KEEP_OBJECT_IN_BOUNDS] g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].iType = 1")
			IF NOT IS_ENTITY_IN_ANGLED_AREA(objIndex, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].vPos1, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].vPos2, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].fWidth)
				vObjCoords = GET_CLOSEST_POINT_AT_EDGE_OF_BOUNDS(vObjCoords, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].vPos1, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].vPos2, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].fWidth)
				PRINTLN("[KEEP_OBJECT_IN_BOUNDS] - vObjCoords at edge: ", vObjCoords)
				FLOAT newZ
				IF GET_GROUND_Z_FOR_3D_COORD(vObjCoords, newZ)
					IF NOT bCheckOverhang
						vObjCoords.z = newZ
					ELSE
						INT i
						vObjCoords.z = newZ
						REPEAT 2 i
							vObjCoords.z = CHECK_Z_FOR_OVERHANG(vObjCoords)
						ENDREPEAT
					ENDIF
					PRINTLN("[KEEP_OBJECT_IN_BOUNDS] GET_GROUND_Z_FOR_3D_COORD returned true")
				ELSE
					PRINTLN("[KEEP_OBJECT_IN_BOUNDS] GET_GROUND_Z_FOR_3D_COORD returned false")
					VECTOR vBoundsCenter = g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sBoundsStruct[ iRule ].vPos1 + g_FMMC_STRUCT.sFMMCEndConditions[ iteam ].sBoundsStruct[ iRule ].vPos2
					vObjCoords = (vBoundsCenter * << 0.5, 0.5, 0.5 >>)
					
					IF GET_GROUND_Z_FOR_3D_COORD(vObjCoords, newZ)
						vObjCoords.z = newZ
						PRINTLN("[KEEP_OBJECT_IN_BOUNDS] - BACKUP PLACING AT CENTER -  FOUND A VALID GROUND Z = ", newZ)
					ELSE
						PRINTLN("[KEEP_OBJECT_IN_BOUNDS] - BACKUP PLACING AT CENTER -  NO VALID Z FOUND, LIKELY SOME STRANGE PLACEMENT")
					ENDIF
					PRINTLN("[KEEP_OBJECT_IN_BOUNDS] BACKUP PLACING AT CENTER - new coords: ", vObjCoords)
				ENDIF
				SET_ENTITY_COORDS(objIndex, vObjCoords)
				PRINTLN("[KEEP_OBJECT_IN_BOUNDS] NOT IS_ENTITY_IN_ANGLED_AREA")
			ENDIF
		ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].iType = 0
		AND NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].vPos)
			PRINTLN("[KEEP_OBJECT_IN_BOUNDS] g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].iType = 0")
			IF GET_DISTANCE_BETWEEN_COORDS(vObjCoords, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].vPos) > g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].fRadius
				VECTOR vDir = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].vPos - vObjCoords
				
				vDir = NORMALISE_VECTOR(vDir)
				
				vObjCoords = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].vPos + (vDir * g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].fRadius)

				GET_GROUND_Z_FOR_3D_COORD(vObjCoords, vObjCoords.Z)
				SET_ENTITY_COORDS(objIndex, vObjCoords)
				PRINTLN("[KEEP_OBJECT_IN_BOUNDS] Moving to ", vObjCoords)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC KEEP_POINTLESS_PACKAGES_IN_BOUNDS()
	INT iObj
	OBJECT_INDEX tempObj
	
	FOR iObj = 0 TO (FMMC_MAX_NUM_OBJECTS - 1)
		IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niObject[iObj])
		AND MC_serverBD.iObjCarrier[iObj] = -1
			tempObj = NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[iObj])
			
			IF NETWORK_HAS_CONTROL_OF_ENTITY(tempObj)
				IF NOT IS_ENTITY_ATTACHED_TO_ANY_PED(tempObj)
					KEEP_OBJECT_IN_BOUNDS(tempObj, TRUE)
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
ENDPROC

PROC PTL_MOVE_STARTING_PACKAGE_TO_MIDDLE(OBJECT_INDEX objIndex, INT iPlacementMod)
	FLOAT newZ
	VECTOR vObjCoords
	VECTOR vBoundsCenter = g_FMMC_STRUCT.sFMMCEndConditions[0].sBoundsStruct[0].vPos1 + g_FMMC_STRUCT.sFMMCEndConditions[0].sBoundsStruct[0].vPos2

	PRINTLN("[RCC PTL_MOVE_STARTING_PACKAGE_TO_MIDDLE] PROC CALLED")
	vObjCoords = (vBoundsCenter * << 0.5, 0.5, 0.5 >>)
	vObjCoords.x = vObjCoords.x + (iPlacementMod * 2)
	IF GET_GROUND_Z_FOR_3D_COORD(vObjCoords, newZ)
		vObjCoords.z = newZ
		PRINTLN("[PTL_MOVE_STARTING_PACKAGE_TO_MIDDLE] - PLACING UNCLAIMED AT CENTER -  FOUND A VALID GROUND Z = ", newZ)
	ELSE
		PRINTLN("[PTL_MOVE_STARTING_PACKAGE_TO_MIDDLE] - PLACING UNCLAIMED AT CENTER -  NO VALID Z FOUND, LIKELY SOME STRANGE PLACEMENT")
	ENDIF
	PRINTLN("[PTL_MOVE_STARTING_PACKAGE_TO_MIDDLE] - PLACING UNCLAIMED AT CENTER -  NEW COORDS = ", vObjCoords)		
	SET_ENTITY_COORDS(objIndex, vObjCoords)
ENDPROC

PROC DELETE_FMMC_OBJECT(INT iobj)
	
	INT iteam
	FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
		CLEAR_BIT(MC_serverBD.iObjteamFailBitset[iobj], iteam)
		CLEAR_BIT(MC_serverBD.iMissionCriticalObj[iteam], iobj)
	ENDFOR
	
	IF IS_THIS_OBJECT_A_HACK_CONTAINER( iObj )
		CPRINTLN( DEBUG_CONTROLLER, "DELETE_FMMC_OBJECT - Object ", iObj, " is a hack container - cleaning up its extra special bits too" )
		CLEAN_UP_CONTAINER_DOORS()
	ENDIF
	IF IS_THIS_BOMB_FOOTBALL() 
		EXPLODE_BOMB_FOOTBALL_BOMB(iObj, TRUE)
	ENDIF
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iRespawnOnRuleChangeBS = 0
		MC_serverBD_2.iCurrentObjRespawnLives[iobj] = 0
		PRINTLN("[RCC MISSION] DELETE_FMMC_OBJECT - Obj ",iobj," getting cleaned up early - set MC_serverBD_2.iCurrentObjRespawnLives[", iobj, "] = 0")
	ENDIF
	DELETE_NET_ID(MC_serverBD_1.sFMMC_SBD.niobject[iobj])
	PRINTLN("[RCC MISSION] DELETE_FMMC_OBJECT - Obj getting cleaned up early - delete, objective: ",g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iCleanupObjective," team: ",g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iCleanupTeam)
	
	CLEAR_BIT(MC_serverBD.iObjCleanup_NeedOwnershipBS, iobj)
	
ENDPROC

FUNC BOOL CLEANUP_OBJ_EARLY( INT iobj, OBJECT_INDEX tempObj )

	IF SHOULD_CLEANUP_OBJ( iobj, tempObj )
		IF IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectBitset, cibsOBJ_IgnoreVisCheckCleanup )
			IF NOT NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niobject[iobj])
				PRINTLN("[RCC MISSION] CLEANUP_OBJ_EARLY - Requesting control of object ",iobj," for deletion; setting MC_serverBD.iObjCleanup_NeedOwnershipBS bit")
				NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niobject[iobj])
				SET_BIT(MC_serverBD.iObjCleanup_NeedOwnershipBS, iobj)
			ELSE
				PRINTLN("[RCC MISSION] CLEANUP_OBJ_EARLY - Deleting object ",iobj)
				DELETE_FMMC_OBJECT(iobj)
				
				RETURN TRUE
			ENDIF
		ELSE
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niobject[iobj])
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niobject[iobj]), FALSE)
			ENDIF
			
			INT iteam
			FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
				CLEAR_BIT(MC_serverBD.iObjteamFailBitset[iobj], iteam)
				CLEAR_BIT(MC_serverBD.iMissionCriticalObj[iteam], iobj)
			ENDFOR
			
			IF IS_THIS_OBJECT_A_HACK_CONTAINER( iObj )
				CPRINTLN( DEBUG_CONTROLLER, "Object ", iObj, " is a hack container - cleaning up its extra special bits too" )
				CLEAN_UP_CONTAINER_DOORS()
			ENDIF
			IF IS_THIS_BOMB_FOOTBALL() 
				EXPLODE_BOMB_FOOTBALL_BOMB(iObj)
			ENDIF
			
			MC_serverBD_2.iCurrentObjRespawnLives[iobj] = 0
			PRINTLN("[RCC MISSION] Obj ",iobj," getting cleaned up early - set MC_serverBD_2.iCurrentObjRespawnLives[", iobj, "] = 0. Call 1.")
			CLEANUP_NET_ID(MC_serverBD_1.sFMMC_SBD.niobject[iobj])
			PRINTLN("[RCC MISSION] Obj getting cleaned up early - no longer needed, objective: ",g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iCleanupObjective," team: ",g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iCleanupTeam)
			
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE

ENDFUNC

FUNC BOOL SHOULD_WE_CLEANUP_OBJECT_DUE_TO_IT_BEING_DEAD(INT iObj)	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectBitSetThree, cibsOBJ3_DontCleanUpWhenDead)
		PRINTLN("PROCESS_OBJ_BRAIN - SHOULD_WE_CLEANUP_OBJECT_DUE_TO_IT_BEING_DEAD - Refusing to clean up/delete this dead object due to cibsOBJ3_DontCleanUpWhenDead. Object: ", iObj)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC


//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: PROCESS OBJECT BRAIN !
//
//************************************************************************************************************************************************************



// handles state logic for the mission objects
PROC PROCESS_OBJ_BRAIN(INT iobj)

	PED_INDEX tempped
	PLAYER_INDEX tempplayer = INVALID_PLAYER_INDEX()
	PARTICIPANT_INDEX temppart = INVALID_PARTICIPANT_INDEX()
	OBJECT_INDEX tempObj
	VEHICLE_INDEX tempveh

	INT iteam
	INT iobjTeam
	BOOL bcheckteamfail, bContainer
	INT ipriority[FMMC_MAX_TEAMS]
	BOOL bwasobjective[FMMC_MAX_TEAMS]

	
	IF NOT bIsLocalPlayerHost
		EXIT
	ENDIF
	
	IF MC_serverBD.iObjHackPart[iobj] != -1
		IF MC_playerBD[MC_serverBD.iObjHackPart[iobj]].iObjHacking = -1
			PRINTLN("[RCC MISSION] server thinks object is being hacked but participant does not for obj: ", iobj," ipart: ",MC_serverBD.iObjHackPart[iobj])
			MC_serverBD.iObjHackPart[iobj] = -1
		ENDIF
	ENDIF
	
	// manage server package attachment data
	IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niObject[iobj])
		tempObj = NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[iobj])
		IF CLEANUP_OBJ_EARLY(iobj,tempObj)
			EXIT
		ENDIF
		
		//Breakables (crates/CCTV cameras):
		SERVER_MANAGE_BREAKABLE_OBJECTS(tempObj, iobj, TRUE) // Also gets called if the object doesn't exist, but corresponding iFragableCrate bit
														// is set. This is to handle the case where the object is destroyed but it should
														// still spawn pickups
		bContainer = IS_OBJECT_A_CONTAINER( tempObj )
		IF IS_ENTITY_ATTACHED_TO_ANY_PED( tempObj )
		OR IS_SPECIAL_PICKUP_READY_FOR_DELIVERY(iObj)
		OR bContainer
			IF NOT bContainer
				ENTITY_INDEX tempPlayerEnt = GET_ENTITY_ATTACHED_TO(tempObj)
				IF DOES_ENTITY_EXIST( tempPlayerEnt )
					tempped = GET_PED_INDEX_FROM_ENTITY_INDEX( tempPlayerEnt )
					IF NOT IS_PED_INJURED(tempped)
						IF IS_PED_A_PLAYER(tempped)
							tempplayer =NETWORK_GET_PLAYER_INDEX_FROM_PED(tempped)
							temppart= NETWORK_GET_PARTICIPANT_INDEX(tempplayer)
						ENDIF
					ENDIF
				ENDIF
			ELSE
				// This does the container pickup stuff
				IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].mn != prop_container_ld_pu
					IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].mn = PROP_CONTR_03B_LD
						tempveh = FIND_HANDLER_VEHICLE_CONTAINER_IS_ATTACHED_TO(tempObj)
					ENDIF
					
					IF IS_ENTITY_ATTACHED_TO_ANY_VEHICLE(tempObj)
						tempveh = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_ATTACHED_TO(tempObj))
					ENDIF
					
					IF IS_VEHICLE_DRIVEABLE(tempveh)
						tempped = GET_PED_IN_VEHICLE_SEAT(tempveh)
						IF NOT IS_PED_INJURED(tempped)
							IF IS_PED_A_PLAYER(tempped)
								tempplayer =NETWORK_GET_PLAYER_INDEX_FROM_PED(tempped)
								temppart= NETWORK_GET_PARTICIPANT_INDEX(tempplayer)
							ENDIF
						ENDIF
					ELSE
						IF IS_CRATE_STUCK(tempObj,iobj)
						OR IS_ENTITY_IN_WATER(tempObj)
							PRINTLN("[RCC MISSION] object is returning as stuck or in water so cleaning up: ", iobj)
							FOR iteam = 0 to (MC_serverBD.iNumberOfTeams-1)
								REMOVE_OBJECT_HOLDING_POINTS(iteam,iobj)
							ENDFOR
							IF NETWORK_HAS_CONTROL_OF_ENTITY(tempObj)
								PRINTLN("[RCC MISSION] SETTING SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION FALSE on Object: ", iobj)
								SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(tempObj, FALSE)
							ENDIF
							CLEANUP_NET_ID(MC_serverBD_1.sFMMC_SBD.niObject[iobj])
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF tempplayer != INVALID_PLAYER_INDEX()
				IF NETWORK_IS_PARTICIPANT_ACTIVE(temppart) 
					IF IS_NET_PLAYER_OK(tempplayer)
						INT ipart = NATIVE_TO_INT(temppart)
						IF MC_serverBD.iObjCarrier[iobj] != ipart
							IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciPOINTLESS_MODE_ENABLED)
								// Package is still free, give it to the player
								IF NOT IS_BIT_SET(MC_serverBD.iObjAtYourHolding[MC_playerBD[ipart].iTeam],iobj)
									PRINTLN("[RCC MISSION] Object carrier - Object: ",iobj," attached to participant: ",NATIVE_TO_INT(temppart))
									IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectBitSetTwo, cibsOBJ2_DontResetTimerWhenDropped)
										RESET_NET_TIMER(MC_serverBD_1.tdControlObjTimer[iobj])
									ENDIF
																			
									MC_serverBD.iObjCarrier[iobj] = NATIVE_TO_INT(temppart)
									
									IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_STOCKPILE(g_FMMC_STRUCT.iAdversaryModeType)
										IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_STOCKPILE_SOUNDS)
											BROADCAST_FMMC_STOCKPILE_SOUND(ciStockpile_Sound_Collect, MC_serverBD.iObjCarrier[iobj])
											PRINTLN("[RCC MISSION][StockpileSound] Playing ciStockpile_Sound_Collect sound (1)")
										ENDIF
									ENDIF
									
									IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_STOCKPILE(g_FMMC_STRUCT.iAdversaryModeType)
										MC_serverBD_1.iPTLDroppedPackageLastOwner[iobj] = -1
										MC_serverBD_1.iPTLDroppedPackageOwner[iObj] = NATIVE_TO_INT(temppart)
										PRINTLN("[RCC MISSION] STOCKPILE - Setting iPTLDroppedPackageLastOwner[",iobj,"] to -1. Someone has picked up")
										PRINTLN("[RCC MISSION] STOCKPILE - Setting iPTLDroppedPackageOwner[",iobj,"] to ", NATIVE_TO_INT(temppart))
									ENDIF
									
									IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetTwo, cibsOBJ2_TransformJuggernaut)
									AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetTwo, cibsOBJ2_TransformBeast)
									AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetTwo, cibsOBJ2_UpgradeVeh)
										IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iCustomShard > 0
											BROADCAST_FMMC_DISPLAY_CUSTOM_OBJECT_PICKUP_SHARD(tempplayer, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iCustomShard, MC_playerBD[ipart].iTeam)
										ENDIF
									ENDIF
								ENDIF
							ELSE
								PRINTLN("[RCC MISSION] 2 Object carrier - Object: ",iobj," attached to participant: ",NATIVE_TO_INT(temppart))
								IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciPOINTLESS_MODE_ENABLED)
									MC_serverBD_1.iPTLDroppedPackageLastTeam[iobj] = MC_playerBD[NATIVE_TO_INT(temppart)].iTeam
									MC_serverBD_1.iPTLDroppedPackageOwner[iobj] = -1
									MC_serverBD_1.iPTLDroppedPackageLastOwner[iobj] = NATIVE_TO_INT(temppart)
									PRINTLN("[JT POINTLESS] PACKAGE: ", iobj, " has just been picked up by player: ", NATIVE_TO_INT(temppart))
									PRINTLN("[JT SCORE POINTLESS] Player ", NATIVE_TO_INT(temppart), " score: ", MC_playerBD[NATIVE_TO_INT(temppart)].iPlayerScore)
								ENDIF
																
								MC_serverBD.iObjCarrier[iobj]  = NATIVE_TO_INT(temppart)							
								
							ENDIF
						ENDIF
						
						
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitsetThirteen, ciENABLE_FORCE_CONTROL_OBJECT_BARS_TO_BE_TEAM_BASED)
							IF MC_serverBD.iObjCarrier[iobj] > -1
								MC_serverBD.iObjTeamCarrier = MC_PlayerBD[MC_serverBD.iObjCarrier[iobj]].iteam
							ENDIF
						ENDIF
						
						IF (MC_serverBD.iObjCarrier[iobj] > -1
						AND (MC_serverBD.iObjTeamCarrier > -1 OR NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitsetThirteen, ciENABLE_FORCE_CONTROL_OBJECT_BARS_TO_BE_TEAM_BASED)))
						OR (MC_serverBD_4.iObjPriority[iobj][iobjTeam] < FMMC_MAX_RULES AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iobjTeam].iRuleBitsetNine[MC_serverBD_4.iObjPriority[iobj][iobjTeam]], ciBS_RULE9_USE_GRANULAR_CAPTURE))
							PRINTLN("[LM][CONTROL OBJECT] - Valid Carrier and Team Carrier Assigned iObjCarrier: ", MC_serverBD.iObjCarrier[iobj], "  iObjTeamCarrier: ", MC_serverBD.iObjTeamCarrier)
						
							iobjTeam = MC_playerBD[MC_serverBD.iObjCarrier[iobj]].iteam
							IF MC_serverBD_4.iObjRule[iobj][iobjTeam] = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER
							OR MC_serverBD_4.iObjRule[iobj][iobjTeam] = FMMC_OBJECTIVE_LOGIC_CAPTURE
							OR MC_serverBD_4.iObjRule[iobj][iobjTeam] = FMMC_OBJECTIVE_LOGIC_GET_AND_HOLD
								IF MC_serverBD_4.iObjPriority[iobj][iobjTeam]  <= MC_serverBD_4.iCurrentHighestPriority[iobjTeam]
									iNumHighPriorityObjHeld[iobjTeam]++
									FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
										IF iteam != iobjTeam
											IF DOES_TEAM_LIKE_TEAM(iteam,iobjTeam)
												iNumHighPriorityObjHeld[iteam]++
											ENDIF
										ENDIF
									ENDFOR
								ENDIF
							ENDIF
							
							IF MC_serverBD_4.iObjRule[iobj][iobjTeam] = FMMC_OBJECTIVE_LOGIC_CAPTURE
								IF MC_serverBD_4.iObjPriority[iobj][iobjTeam] < FMMC_MAX_RULES
									IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iobjTeam].iRuleBitsetNine[MC_serverBD_4.iObjPriority[iobj][iobjTeam]], ciBS_RULE9_USE_GRANULAR_CAPTURE)
										IF NOT IS_BIT_SET(MC_playerBD[ipart].iClientBitSet,PBBOOL_I_AM_DROWNING)
										AND NOT IS_PLAYER_IN_PLANE_IN_WATER(tempped)
											IF (NOT HAS_NET_TIMER_STARTED(MC_serverBD_1.tdControlObjTimer[iobj]) AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitsetThirteen, ciENABLE_FORCE_CONTROL_OBJECT_BARS_TO_BE_TEAM_BASED))
											OR (NOT HAS_NET_TIMER_STARTED(MC_serverBD.tdControlObjTeamTimer[MC_serverBD.iObjTeamCarrier]) AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitsetThirteen, ciENABLE_FORCE_CONTROL_OBJECT_BARS_TO_BE_TEAM_BASED))
												IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitsetThirteen, ciENABLE_FORCE_CONTROL_OBJECT_BARS_TO_BE_TEAM_BASED)												
												AND NOT IS_BIT_SET(MC_serverBD.iServerBitset3, SBBOOL3_TEAM_0_SHOULD_RESPAWN_BACK_AT_START_NOW + MC_serverBD.iObjTeamCarrier)	
													START_NET_TIMER(MC_serverBD.tdControlObjTeamTimer[MC_serverBD.iObjTeamCarrier])
													PRINTLN("[RCC MISSION] starting control timer for obj: ",iobj, " iTeam: ", MC_serverBD.iObjTeamCarrier)												
												ELSE
													START_NET_TIMER(MC_serverBD_1.tdControlObjTimer[iobj])
													PRINTLN("[RCC MISSION] starting control timer for obj: ",iobj)
												ENDIF
											ELSE
											
												PRINTLN("[LM][CONTROL OBJECT] - Checking Timer Expired: ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdControlObjTeamTimer[MC_serverBD.iObjTeamCarrier]) > (MC_serverBD.iObjTakeoverTime[iobjTeam][MC_serverBD_4.iObjPriority[iobj][iobjTeam]] - MC_serverBD.iObjectCaptureOffset[iobjTeam]))
												
												IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet6, SBBOOL6_CONTROL_TIME_STOPPED_T0 + iobjTeam)											
													IF (GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_1.tdControlObjTimer[iobj]) > (MC_serverBD.iObjTakeoverTime[iobjTeam][MC_serverBD_4.iObjPriority[iobj][iobjTeam]] - MC_serverBD.iObjectCaptureOffset[iobjTeam]) AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitsetThirteen, ciENABLE_FORCE_CONTROL_OBJECT_BARS_TO_BE_TEAM_BASED))
													OR (GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdControlObjTeamTimer[MC_serverBD.iObjTeamCarrier]) > (MC_serverBD.iObjTakeoverTime[iobjTeam][MC_serverBD_4.iObjPriority[iobj][iobjTeam]] - MC_serverBD.iObjectCaptureOffset[iobjTeam]) AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitsetThirteen, ciENABLE_FORCE_CONTROL_OBJECT_BARS_TO_BE_TEAM_BASED))
																											
														INT iSublogic = -1
														
														IF IS_BIT_SET( iLocalBoolCheck5, LBOOL5_CTF_OBJECT_IS_TARGET )
															iSublogic = ciTICKER_TARGET
														ENDIF
														
														BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS( TICKER_EVENT_TEAM_CON_OBJ,
																									0,
																									iobjTeam,
																									iSublogic,
																									tempplayer,
																									ci_TARGET_OBJECT,
																									iobj )
														
														//BROADCAST_GIVE_PLAYER_XP(tempPlayer,200,-1,TRUE)
														INCREMENT_SERVER_REMOTE_PLAYER_SCORE(tempPlayer, GET_FMMC_POINTS_FOR_TEAM(iobjTeam,MC_serverBD_4.iObjPriority[iobj][iobjTeam]),iobjTeam,MC_serverBD_4.iObjPriority[iobj][iobjTeam])
														IF MC_serverBD.iRuleReCapture[iobjTeam][MC_serverBD_4.iObjPriority[iobj][iobjTeam]] <=0
															PRINTLN("[RCC MISSION] control object ",iobj," cleaning up as out of iRuleReCapture for team ",iobjTeam)
															CLEANUP_NET_ID(MC_serverBD_1.sFMMC_SBD.niObject[iobj])
															//MC_serverBD_1.sFMMC_SBD.niObject[iobj] = NULL
														ENDIF
														IF MC_serverBD_2.iCurrentObjRespawnLives[iobj] <=0
														AND MC_serverBD.iRuleReCapture[iobjTeam][MC_serverBD_4.iObjPriority[iobj][iobjTeam]] <=0
															
															FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
																iPriority[iteam] = MC_serverBD_4.iObjPriority[iobj][iteam]
																IF iteam=iobjTeam
																	IF IS_BIT_SET(MC_serverBD.iObjteamFailBitset[iobj],iteam)
																		CLEAR_BIT(MC_serverBD.iObjteamFailBitset[iobj],iteam)
																		CLEAR_BIT(MC_serverBD.iMissionCriticalObj[iTeam], iobj)
																		bwasobjective[iteam] = TRUE
																		PRINTLN("[RCC MISSION] OBJECTIVE NOT FAILED FOR TEAM: ",iobjTeam," BECAUSE THEY CAPTURED OBJ: ",iobj) 
																	ENDIF
																	SET_TEAM_OBJECTIVE_OUTCOME(iTeam,iPriority[iteam],TRUE)
																ELSE
																	IF DOES_TEAM_LIKE_TEAM(iteam,iobjTeam)
																		IF IS_BIT_SET(MC_serverBD.iObjteamFailBitset[iobj],iteam)
																			CLEAR_BIT(MC_serverBD.iObjteamFailBitset[iobj],iteam)
																			CLEAR_BIT(MC_serverBD.iMissionCriticalObj[iTeam], iobj)
																			PRINTLN("[RCC MISSION] OBJECTIVE NOT FAILED FOR TEAM: ",iteam," BECAUSE THEY friend cap obj: ",iobj) 
																			bwasobjective[iTeam] = TRUE
																		ENDIF
																	ENDIF
																ENDIF
															ENDFOR
															FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
																IF iteam!=iobjTeam
																	IF IS_BIT_SET(MC_serverBD.iObjteamFailBitset[iobj],iteam)
																		IF iPriority[iteam] < FMMC_MAX_RULES
																		OR IS_BIT_SET(MC_serverBD.iMissionCriticalObj[iTeam], iobj)
																			IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iteam].iTeamFailBitset,iPriority[iteam])
																			OR IS_BIT_SET(MC_serverBD.iMissionCriticalObj[iTeam], iobj)
																				SET_TEAM_FAILED(iteam,mFail_OBJ_CAPTURED)
																				MC_serverBD.iEntityCausingFail[iteam] = iobj
																				PRINTLN("[RCC MISSION] MISSION FAILED FOR TEAM:",iteam," BECAUSE OBJ CAPTURED: ",iobj) 
																			ENDIF
																		ENDIF
																		bwasobjective[iteam] = TRUE
																		CLEAR_BIT(MC_serverBD.iObjteamFailBitset[iobj],iteam)
																	ENDIF
																	IF DOES_TEAM_LIKE_TEAM(iteam,iobjTeam)
																		SET_TEAM_OBJECTIVE_OUTCOME(iTeam,iPriority[iteam],TRUE)
																		PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_OBJECT,iobj,iTeam,TRUE)
																	ELSE
																		SET_TEAM_OBJECTIVE_OUTCOME(iTeam,iPriority[iteam],FALSE)
																		PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_OBJECT,iobj,iTeam,FALSE)
																	ENDIF
																ELSE
																	SET_TEAM_OBJECTIVE_OUTCOME(iTeam,iPriority[iteam],TRUE)
																	PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_OBJECT,iobj,iTeam,TRUE)
																ENDIF
																MC_serverBD.iReasonForObjEnd[iteam] = OBJ_END_REASON_OBJ_CAPTURED
															ENDFOR
														ENDIF
														IF MC_serverBD_4.iObjPriority[iobj][iobjTeam] < FMMC_MAX_RULES
															IF MC_serverBD_4.iObjPriority[iobj][iobjTeam] < FMMC_MAX_RULES
																IF MC_serverBD.iRuleReCapture[iobjTeam][MC_serverBD_4.iObjPriority[iobj][iobjTeam]] !=UNLIMITED_CAPTURES_KILLS
																	MC_serverBD.iRuleReCapture[iobjTeam][MC_serverBD_4.iObjPriority[iobj][iobjTeam]]--
																ENDIF
															ENDIF
														ENDIF
														FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
															IF bwasobjective[iteam]
																IF IS_THIS_OBJECTIVE_OVER(ci_TARGET_OBJECT,iteam,iPriority[iteam])
																	SET_LOCAL_OBJECTIVE_END_MESSAGE(iteam,iPriority[iteam],MC_serverBD.iReasonForObjEnd[iTeam])
																	BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[iteam],iteam,iPriority[iteam],DID_TEAM_PASS_OBJECTIVE(iteam,iPriority[iteam]))
																ENDIF
															ENDIF
														ENDFOR
														
														PRINTLN("[RCC MISSION] Control complete for obj: ",iobj)
														RESET_NET_TIMER(MC_serverBD_1.tdControlObjTimer[iobj])
														
														IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitsetThirteen, ciENABLE_FORCE_CONTROL_OBJECT_BARS_TO_BE_TEAM_BASED)
															INT iTeamO
															FOR iTeamO = 0 TO FMMC_MAX_TEAMS-1		
																RESET_NET_TIMER(MC_serverBD.tdControlObjTeamTimer[iTeamO])
															ENDFOR
														ENDIF
													ENDIF
												ENDIF
											ENDIF
										ELSE // Else we're drowning - reset the timer
											IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectBitSetTwo, cibsOBJ2_DontResetTimerWhenDropped)
												PRINTLN("[RCC MISSION] Part ",iPart," is drowning, disable control countdown for obj: ",iobj)
												RESET_NET_TIMER(MC_serverBD_1.tdControlObjTimer[iobj])
												
												
												IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitsetThirteen, ciENABLE_FORCE_CONTROL_OBJECT_BARS_TO_BE_TEAM_BASED)
													IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectBitSetTwo, cibsOBJ2_TeamControlBarObject)
														INT iTeamO
														FOR iTeamO = 0 TO FMMC_MAX_TEAMS-1	
															PRINTLN("[LM][CONTROL OBJECT] Reseting Team: ", iTeamO, " Timer. (1)")
															RESET_NET_TIMER(MC_serverBD.tdControlObjTeamTimer[iTeamO])
														ENDFOR
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ELSE
										IF MC_serverBD_4.iCurrentHighestPriority[iobjTeam] < FMMC_MAX_RULES
											IF MC_serverBD.iGranularCurrentPoints[iobjTeam] >= GET_FMMC_GRANULAR_CAPTURE_TARGET(MC_serverBD_4.iCurrentHighestPriority[iobjTeam], iobjTeam)
											AND NOT IS_BIT_SET(MC_ServerBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH)	
												INT iSublogic = -1
															
												IF IS_BIT_SET( iLocalBoolCheck5, LBOOL5_CTF_OBJECT_IS_TARGET )
													iSublogic = ciTICKER_TARGET
												ENDIF
												
												BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS( TICKER_EVENT_TEAM_CON_OBJ,
																							0,
																							iobjTeam,
																							iSublogic,
																							tempplayer,
																							ci_TARGET_OBJECT,
																							iobj )
												
												//BROADCAST_GIVE_PLAYER_XP(tempPlayer,200,-1,TRUE)
												INCREMENT_SERVER_REMOTE_PLAYER_SCORE(tempPlayer, GET_FMMC_POINTS_FOR_TEAM(iobjTeam,MC_serverBD_4.iObjPriority[iobj][iobjTeam]),iobjTeam,MC_serverBD_4.iObjPriority[iobj][iobjTeam])
												IF MC_serverBD.iRuleReCapture[iobjTeam][MC_serverBD_4.iObjPriority[iobj][iobjTeam]] <=0
													PRINTLN("[RCC MISSION][GRANCAP] control object ",iobj," cleaning up as out of iRuleReCapture for team ",iobjTeam)
													CLEANUP_NET_ID(MC_serverBD_1.sFMMC_SBD.niObject[iobj])
													//MC_serverBD_1.sFMMC_SBD.niObject[iobj] = NULL
												ENDIF
												IF MC_serverBD_2.iCurrentObjRespawnLives[iobj] <=0
												AND MC_serverBD.iRuleReCapture[iobjTeam][MC_serverBD_4.iObjPriority[iobj][iobjTeam]] <=0
													
													FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
														iPriority[iteam] = MC_serverBD_4.iObjPriority[iobj][iteam]
														IF iteam=iobjTeam
															IF IS_BIT_SET(MC_serverBD.iObjteamFailBitset[iobj],iteam)
																CLEAR_BIT(MC_serverBD.iObjteamFailBitset[iobj],iteam)
																CLEAR_BIT(MC_serverBD.iMissionCriticalObj[iTeam], iobj)
																bwasobjective[iteam] = TRUE
																PRINTLN("[RCC MISSION][GRANCAP] OBJECTIVE NOT FAILED FOR TEAM: ",iobjTeam," BECAUSE THEY CAPTURED OBJ: ",iobj) 
															ENDIF
															SET_TEAM_OBJECTIVE_OUTCOME(iTeam,iPriority[iteam],TRUE)
														ELSE
															IF DOES_TEAM_LIKE_TEAM(iteam,iobjTeam)
																IF IS_BIT_SET(MC_serverBD.iObjteamFailBitset[iobj],iteam)
																	CLEAR_BIT(MC_serverBD.iObjteamFailBitset[iobj],iteam)
																	CLEAR_BIT(MC_serverBD.iMissionCriticalObj[iTeam], iobj)
																	PRINTLN("[RCC MISSION][GRANCAP] OBJECTIVE NOT FAILED FOR TEAM: ",iteam," BECAUSE THEY friend cap obj: ",iobj) 
																	bwasobjective[iTeam] = TRUE
																ENDIF
															ENDIF
														ENDIF
													ENDFOR
													FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
														IF iteam!=iobjTeam
															IF IS_BIT_SET(MC_serverBD.iObjteamFailBitset[iobj],iteam)
																IF iPriority[iteam] < FMMC_MAX_RULES
																OR IS_BIT_SET(MC_serverBD.iMissionCriticalObj[iTeam], iobj)
																	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iteam].iTeamFailBitset,iPriority[iteam])
																	OR IS_BIT_SET(MC_serverBD.iMissionCriticalObj[iTeam], iobj)
																		SET_TEAM_FAILED(iteam,mFail_OBJ_CAPTURED)
																		MC_serverBD.iEntityCausingFail[iteam] = iobj
																		PRINTLN("[RCC MISSION][GRANCAP] MISSION FAILED FOR TEAM:",iteam," BECAUSE OBJ CAPTURED: ",iobj) 
																	ENDIF
																ENDIF
																bwasobjective[iteam] = TRUE
																CLEAR_BIT(MC_serverBD.iObjteamFailBitset[iobj],iteam)
															ENDIF
															IF DOES_TEAM_LIKE_TEAM(iteam,iobjTeam)
																SET_TEAM_OBJECTIVE_OUTCOME(iTeam,iPriority[iteam],TRUE)
																PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_OBJECT,iobj,iTeam,TRUE)
															ELSE
																SET_TEAM_OBJECTIVE_OUTCOME(iTeam,iPriority[iteam],FALSE)
																PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_OBJECT,iobj,iTeam,FALSE)
															ENDIF
														ELSE
															SET_TEAM_OBJECTIVE_OUTCOME(iTeam,iPriority[iteam],TRUE)
															PROGRESS_SPECIFIC_OBJECTIVE(ci_TARGET_OBJECT,iobj,iTeam,TRUE)
														ENDIF
														MC_serverBD.iReasonForObjEnd[iteam] = OBJ_END_REASON_OBJ_CAPTURED
													ENDFOR
												ENDIF
												IF MC_serverBD_4.iObjPriority[iobj][iobjTeam] < FMMC_MAX_RULES
													IF MC_serverBD_4.iObjPriority[iobj][iobjTeam] < FMMC_MAX_RULES
														IF MC_serverBD.iRuleReCapture[iobjTeam][MC_serverBD_4.iObjPriority[iobj][iobjTeam]] !=UNLIMITED_CAPTURES_KILLS
															MC_serverBD.iRuleReCapture[iobjTeam][MC_serverBD_4.iObjPriority[iobj][iobjTeam]]--
														ENDIF
													ENDIF
												ENDIF
												FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
													IF bwasobjective[iteam]
														IF IS_THIS_OBJECTIVE_OVER(ci_TARGET_OBJECT,iteam,iPriority[iteam])
															SET_LOCAL_OBJECTIVE_END_MESSAGE(iteam,iPriority[iteam],MC_serverBD.iReasonForObjEnd[iTeam])
															BROADCAST_FMMC_OBJECTIVE_END_MESSAGE(MC_serverBD.iReasonForObjEnd[iteam],iteam,iPriority[iteam],DID_TEAM_PASS_OBJECTIVE(iteam,iPriority[iteam]))
														ENDIF
													ENDIF
												ENDFOR
												
												PRINTLN("[RCC MISSION][GRANCAP] Control complete for obj: ",iobj)
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ELSE
								IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectBitSetTwo, cibsOBJ2_DontResetTimerWhenDropped)
									RESET_NET_TIMER(MC_serverBD_1.tdControlObjTimer[iobj])
									
									IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitsetThirteen, ciENABLE_FORCE_CONTROL_OBJECT_BARS_TO_BE_TEAM_BASED)
										IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectBitSetTwo, cibsOBJ2_TeamControlBarObject)
											INT iTeamO
											FOR iTeamO = 0 TO FMMC_MAX_TEAMS-1
											PRINTLN("[LM][CONTROL OBJECT] Reseting Team: ", iTeamO, " Timer. (2)")
												RESET_NET_TIMER(MC_serverBD.tdControlObjTeamTimer[iTeamO])
											ENDFOR
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF MC_serverBD.iObjCarrier[iobj]  = NATIVE_TO_INT(temppart)
							PRINTLN("[RCC MISSION] Object: ",iobj," no longer carried by participant -they not ok: ",NATIVE_TO_INT(temppart))
							MC_serverBD.iObjCarrier[iobj]   = -1
							
							IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_STOCKPILE(g_FMMC_STRUCT.iAdversaryModeType)
								MC_serverBD_1.iPTLDroppedPackageLastOwner[iobj] = -1
								IF IS_BIT_SET(MC_serverBD.iObjCaptured, iobj)
									CLEAR_BIT(MC_serverBD.iObjCaptured, iobj)
									PRINTLN("[RCC MISSION] Clearing MC_serverBD.iObjCaptured, ", iobj)
								ENDIF
								PRINTLN("[RCC MISSION] STOCKPILE - Setting iPTLDroppedPackageLastOwner[",iobj,"] to -1. NOT OK")
							ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_ARENA_CTF(g_FMMC_STRUCT.iAdversaryModeType)
								BROADCAST_SET_ARENA_ANNOUNCER_SOUND_BS(ci_Arena_Announcer_DataTypeForEvent_ModeSpecific, g_iAA_PlaySound_CTF_FlagCarrierKilled)
							ENDIF
							
							IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectBitSetTwo, cibsOBJ2_DontResetTimerWhenDropped)
								RESET_NET_TIMER(MC_serverBD_1.tdControlObjTimer[iobj])
							ENDIF
						ENDIF
						
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitsetThirteen, ciENABLE_FORCE_CONTROL_OBJECT_BARS_TO_BE_TEAM_BASED)
							IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectBitSetTwo, cibsOBJ2_TeamControlBarObject)
								IF MC_serverBD.iObjTeamCarrier != -1
									PRINTLN("[LM][CONTROL OBJECT] MC_serverBD.iObjTeamCarrier: ", MC_serverBD.iObjTeamCarrier," Cleaning up to -1 because iObj: ", iobj, " no longer carried by participant -they not ok: ",NATIVE_TO_INT(temppart))
									
									IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectBitSetTwo, cibsOBJ2_DontResetTimerWhenDropped)
										PRINTLN("[LM][CONTROL OBJECT] Reseting Team: ", MC_serverBD.iObjTeamCarrier," Timer.")
										RESET_NET_TIMER(MC_serverBD.tdControlObjTeamTimer[MC_serverBD.iObjTeamCarrier])
									ENDIF
									
									MC_serverBD.iObjTeamCarrier = -1
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF MC_serverBD.iObjCarrier[iobj]  = NATIVE_TO_INT(temppart)
						
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_STOCKPILE_SOUNDS)												
							PRINTLN("[RCC MISSION][StockpileSound] - Playing: ciStockpile_Sound_Dropped (1)")
							BROADCAST_FMMC_STOCKPILE_SOUND(ciStockpile_Sound_Dropped, MC_serverBD.iObjCarrier[iobj])
						ENDIF
						
						PRINTLN("[RCC MISSION] Object: ",iobj," no longer carried by participant -they not active: ",NATIVE_TO_INT(temppart))
						MC_serverBD.iObjCarrier[iobj]  = -1
						
						IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_STOCKPILE(g_FMMC_STRUCT.iAdversaryModeType)
							MC_serverBD_1.iPTLDroppedPackageLastOwner[iobj] = -1
							IF IS_BIT_SET(MC_serverBD.iObjCaptured, iobj)
								CLEAR_BIT(MC_serverBD.iObjCaptured, iobj)
								PRINTLN("[RCC MISSION] Clearing MC_serverBD.iObjCaptured, ", iobj)
							ENDIF
							PRINTLN("[RCC MISSION] STOCKPILE - Setting iPTLDroppedPackageLastOwner[",iobj,"] to -1. NOT ACTIVE")
						ENDIF
						
						IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectBitSetTwo, cibsOBJ2_DontResetTimerWhenDropped)
							RESET_NET_TIMER(MC_serverBD_1.tdControlObjTimer[iobj])
						ENDIF
					ENDIF
					
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitsetThirteen, ciENABLE_FORCE_CONTROL_OBJECT_BARS_TO_BE_TEAM_BASED)
						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectBitSetTwo, cibsOBJ2_TeamControlBarObject)
							IF MC_serverBD.iObjTeamCarrier != -1			
								PRINTLN("[LM][CONTROL OBJECT] MC_serverBD.iObjTeamCarrier: ", MC_serverBD.iObjTeamCarrier," Cleaning up to -1 because iObj: ", iobj, " no longer carried by participant -they not active: ",NATIVE_TO_INT(temppart))
								
								IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectBitSetTwo, cibsOBJ2_DontResetTimerWhenDropped)
									PRINTLN("[LM][CONTROL OBJECT] Reseting Team: ", MC_serverBD.iObjTeamCarrier," Timer.")
									RESET_NET_TIMER(MC_serverBD.tdControlObjTeamTimer[MC_serverBD.iObjTeamCarrier])
								ENDIF
								
								MC_serverBD.iObjTeamCarrier = -1
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF MC_serverBD.iObjCarrier[iobj]   != -1
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_STOCKPILE_SOUNDS)
						PRINTLN("[RCC MISSION][StockpileSound] - Playing: ciStockpile_Sound_Dropped (2)")
						BROADCAST_FMMC_STOCKPILE_SOUND(ciStockpile_Sound_Dropped, MC_serverBD.iObjCarrier[iobj])
					ENDIF
					
					IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_ARENA_CTF(g_FMMC_STRUCT.iAdversaryModeType)
						BROADCAST_SET_ARENA_ANNOUNCER_SOUND_BS(ci_Arena_Announcer_DataTypeForEvent_ModeSpecific, g_iAA_PlaySound_CTF_FlagCarrierKilled)
					ENDIF
					
					PRINTLN("[RCC MISSION] Object: ",iobj," not carried by any ped ... 1st call")
					MC_serverBD.iObjCarrier[iobj]  = -1
					
					IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectBitSetTwo, cibsOBJ2_DontResetTimerWhenDropped)
						RESET_NET_TIMER(MC_serverBD_1.tdControlObjTimer[iobj])
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitsetThirteen, ciENABLE_FORCE_CONTROL_OBJECT_BARS_TO_BE_TEAM_BASED)
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectBitSetTwo, cibsOBJ2_TeamControlBarObject)
						IF MC_serverBD.iObjTeamCarrier != -1			
							PRINTLN("[LM][CONTROL OBJECT] MC_serverBD.iObjTeamCarrier: ", MC_serverBD.iObjTeamCarrier," Cleaning up to -1 because iObj: ", iobj, " not carried by any ped ... 1st call")
							
							IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectBitSetTwo, cibsOBJ2_DontResetTimerWhenDropped)
								PRINTLN("[LM][CONTROL OBJECT] Reseting Team: ", MC_serverBD.iObjTeamCarrier," Timer.")
								RESET_NET_TIMER(MC_serverBD.tdControlObjTeamTimer[MC_serverBD.iObjTeamCarrier])
							ENDIF
							
							MC_serverBD.iObjTeamCarrier = -1
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF MC_serverBD.iObjHackPart[iobj] != -1
				IF MC_serverBD_4.iObjRule[iobj][MC_playerBD[MC_serverBD.iObjHackPart[iobj]].iteam] = FMMC_OBJECTIVE_LOGIC_MINIGAME
					IF MC_serverBD_4.iObjPriority[iobj][MC_playerBD[MC_serverBD.iObjHackPart[iobj]].iteam]  <= MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[MC_serverBD.iObjHackPart[iobj]].iteam]
						iNumHighPriorityObjHeld[MC_playerBD[MC_serverBD.iObjHackPart[iobj]].iteam]++
						FOR iteam = 0 TO (MC_serverBD.iNumberOfTeams-1)
							IF iteam != MC_playerBD[MC_serverBD.iObjHackPart[iobj]].iteam
								IF DOES_TEAM_LIKE_TEAM(iteam,MC_playerBD[MC_serverBD.iObjHackPart[iobj]].iteam)
									iNumHighPriorityObjHeld[iteam]++
								ENDIF
							ENDIF
						ENDFOR
					ENDIF
				ENDIF
			ENDIF
			IF MC_serverBD.iObjCarrier[iobj]   != -1
				PRINTLN("[RCC MISSION] Object: ",iobj," not carried by any ped ... 2nd call")
				
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_STOCKPILE(g_FMMC_STRUCT.iAdversaryModeType)
					IF IS_NET_PARTICIPANT_INT_ID_VALID(MC_serverBD.iObjCarrier[iobj])
						IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(MC_serverBD.iObjCarrier[iobj]))
							tempplayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(MC_serverBD.iObjCarrier[iobj]))
							IF IS_NET_PLAYER_OK(tempplayer)
								MC_serverBD_1.iPTLDroppedPackageLastOwner[iobj] = MC_serverBD.iObjCarrier[iobj]
							ELSE
								IF IS_BIT_SET(MC_serverBD.iObjCaptured, iobj)
									CLEAR_BIT(MC_serverBD.iObjCaptured, iobj)
									PRINTLN("[RCC MISSION] Clearing MC_serverBD.iObjCaptured, ", iobj)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_STOCKPILE_SOUNDS)												
						PARTICIPANT_INDEX pIndex = INT_TO_PARTICIPANTINDEX(MC_serverBD.iObjCarrier[iobj])
						
						IF NETWORK_IS_PARTICIPANT_ACTIVE(pIndex)
							PLAYER_INDEX piPlayer = NETWORK_GET_PLAYER_INDEX(pIndex)
							PED_INDEX pedPlayer = GET_PLAYER_PED(piPlayer)
														
							IF IS_PED_INJURED(pedPlayer)
							OR IS_BIT_SET(MC_ServerBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH)
								PRINTLN("[RCC MISSION][StockpileSound] - Playing: ciStockpile_Sound_Dropped (3)")
								BROADCAST_FMMC_STOCKPILE_SOUND(ciStockpile_Sound_Dropped, MC_serverBD.iObjCarrier[iobj])
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				MC_serverBD.iObjCarrier[iobj]  = -1
				
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_ARENA_CTF(g_FMMC_STRUCT.iAdversaryModeType)
					BROADCAST_SET_ARENA_ANNOUNCER_SOUND_BS(ci_Arena_Announcer_DataTypeForEvent_ModeSpecific, g_iAA_PlaySound_CTF_FlagCarrierKilled)
				ENDIF
				
				IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectBitSetTwo, cibsOBJ2_DontResetTimerWhenDropped)
					RESET_NET_TIMER(MC_serverBD_1.tdControlObjTimer[iobj])
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitsetThirteen, ciENABLE_FORCE_CONTROL_OBJECT_BARS_TO_BE_TEAM_BASED)
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectBitSetTwo, cibsOBJ2_TeamControlBarObject)
					IF MC_serverBD.iObjTeamCarrier != -1			
						PRINTLN("[LM][CONTROL OBJECT] MC_serverBD.iObjTeamCarrier: ", MC_serverBD.iObjTeamCarrier," Cleaning up to -1 because iObj: ", iobj, " not carried by any ped ... 2nd call")
						
						IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectBitSetTwo, cibsOBJ2_DontResetTimerWhenDropped)
							PRINTLN("[LM][CONTROL OBJECT] Reseting Team: ", MC_serverBD.iObjTeamCarrier," Timer.")
							RESET_NET_TIMER(MC_serverBD.tdControlObjTeamTimer[MC_serverBD.iObjTeamCarrier])
						ENDIF
						
						MC_serverBD.iObjTeamCarrier = -1
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams-1)
			IF MC_serverBD_4.iObjPriority[iObj][iteam] < FMMC_MAX_RULES
			AND MC_serverBD_4.iObjPriority[iObj][iteam] > -1
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTen[MC_serverBD_4.iObjPriority[iObj][iteam]], ciBS_RULE10_PASS_RULE_THROUGH_TAGGING)
					IF IS_ENTITY_ALREADY_TAGGED(4,iObj)
						PRINTLN("[RCC MISSION] OBJECT HAS BEEN TAGGED. Calling PROGRESS_SPECIFIC_OBJECTIVE")
						INVALIDATE_SINGLE_OBJECTIVE(iteam, MC_ServerBD_4.iObjPriority[iObj][iTeam], TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
				
	ELSE // Else the network id for this object doesn't exist
	
		IF MC_serverBD.iObjCarrier[iobj]   != -1
			PRINTLN("[RCC MISSION] Object: ",iobj," does not exist") 
			MC_serverBD.iObjCarrier[iobj]  = -1
			
			IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectBitSetTwo, cibsOBJ2_DontResetTimerWhenDropped)
				RESET_NET_TIMER(MC_serverBD_1.tdControlObjTimer[iobj])
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitsetThirteen, ciENABLE_FORCE_CONTROL_OBJECT_BARS_TO_BE_TEAM_BASED)
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectBitSetTwo, cibsOBJ2_TeamControlBarObject)
				IF MC_serverBD.iObjTeamCarrier != -1			
					//PRINTLN("[LM][CONTROL OBJECT] MC_serverBD.iObjTeamCarrier: ", MC_serverBD.iObjTeamCarrier," Cleaning up to -1 because iObj: ", iobj, " does not exist")
					
					IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectBitSetTwo, cibsOBJ2_DontResetTimerWhenDropped)
						//PRINTLN("[LM][CONTROL OBJECT] Reseting Team: ", MC_serverBD.iObjTeamCarrier," Timer.")
						//RESET_NET_TIMER(MC_serverBD.tdControlObjTeamTimer[MC_serverBD.iObjTeamCarrier])
					ENDIF
				
					//MC_serverBD.iObjTeamCarrier = -1
				ENDIF
			ENDIF
		ENDIF
		
		//-- Dave W, for 2025269, need to check if any objects that no longer exist used to be fraggable crates that got destroyed 
		IF IS_BIT_SET(MC_serverBD.iFragableCrate, iobj)
			//PRINTLN("[RCC MISSION] [dsw] [crates] Object doesn't exist iobj = ", iobj, " but iFragableCrate bit is set.")
			SERVER_MANAGE_BREAKABLE_OBJECTS(tempObj, iobj, FALSE)
		ENDIF
		
	ENDIF
	
	// This plays ptfx for objects once they're deleted
	IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niObject[iobj])
		IF IS_ENTITY_DEAD(tempObj)
			RESET_NET_TIMER(MC_serverBD_1.tdControlObjTimer[iobj])
			IF NOT IS_THIS_MODEL_A_CCTV_CAMERA(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].mn)
			AND SHOULD_WE_CLEANUP_OBJECT_DUE_TO_IT_BEING_DEAD(iObj)
	            IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niObject[iobj])
					PRINTLN("[RCC MISSION] object ",iobj," is dead - play particle fx and delete it's net id")
					START_PARTICLE_FX_NON_LOOPED_AT_COORD( GET_REMOVAL_PTFX_NAME( GET_ENTITY_MODEL( tempObj ) ), GET_ENTITY_COORDS(tempObj,FALSE), <<0,0,0>> )
					DELETE_NET_ID(MC_serverBD_1.sFMMC_SBD.niObject[iobj])
				ELSE
					NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niObject[iobj])
				ENDIF
			ENDIF

//			CLEANUP_NET_ID(MC_serverBD_1.sFMMC_SBD.niObject[iobj])
			//MC_serverBD_1.sFMMC_SBD.niObject[iobj] = NULL
			
			MC_serverBD.iObjHackPart[iObj] = -1
			
			IF MC_serverBD_2.iCurrentObjRespawnLives[iobj] <= 0
				IF NOT IS_BIT_SET( MC_serverBD_1.sCreatedCount.iSkipObjBitset, iobj )
					bcheckteamfail = TRUE
				ELSE
					EXIT // Leave the function
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_ENTITY_ATTACHED(tempObj)
				VECTOR vtemp = GET_ENTITY_COORDS(tempObj)
				FLOAT fZCheck = -20.0
				
				//Particular objects
				IF GET_ENTITY_MODEL(tempObj) = INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_hdsec")) //WVM Bomb
					fZCheck = -120.0
				ENDIF
				
				//Interior checks
				IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_BUNKER)
				OR IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_SUB)
				OR IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_IAA)
				OR IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS, INTERIOR_HANGAR)
					fZCheck = -120.0
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciLOWER_OBJECT_CLEANUP)
					fZCheck = -200
				ENDIF
				
				IF (vtemp.z < fZCheck OR (GET_ENTITY_MODEL(tempObj) = HEI_PROP_HEIST_BINBAG AND IS_ENTITY_IN_WATER(tempObj)))
				AND NOT IS_LOCATION_IN_FMMC_APARTMENT(vtemp)
					PRINTLN("[RCC MISSION] object is returning as below ",fZCheck," so cleaning up: ", iobj, " object z value was: ", vtemp.z)
					CLEANUP_NET_ID(MC_serverBD_1.sFMMC_SBD.niObject[iobj])
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF IS_BIT_SET(g_iMissionObjectHasSpawnedBitset, iObj)
			FOR iTeam = 0 TO FMMC_MAX_TEAMS-1
				IF MC_ServerBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
					IF MC_serverBD_4.iObjPriority[iobj][iteam] <= FMMC_MAX_RULES
					AND MC_serverBD_4.iObjPriority[iobj][iteam] = MC_serverBD_4.iCurrentHighestPriority[iteam]
						IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjectsDestroyedRuleHudAmount[MC_ServerBD_4.iCurrentHighestPriority[iTeam]] > 0
							IF NOT IS_BIT_SET(MC_ServerBD_4.iObjectsDestroyedForSpecialHUDBitset[iTeam], iObj)
								PRINTLN("[RCC MISSION][PROCESS_OBJ_BODY][iEntityincrement] An object was cleaned up that spawned, we may have wanted to increment it ? (iObjectsDestroyedForSpecialHUD)")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
		ENDIF
			
		MC_serverBD.iObjHackPart[iObj] = -1
		IF MC_serverBD_2.iCurrentObjRespawnLives[iobj] <= 0
		AND NOT IS_BIT_SET(MC_serverBD_1.sCreatedCount.iSkipObjBitset,iobj)
			bcheckteamfail = TRUE
		ENDIF
	ENDIF
	
	FOR iteam = 0 to (MC_serverBD.iNumberOfTeams-1)
		IF MC_serverBD_4.iObjPriority[iobj][iteam] < FMMC_MAX_RULES
		OR IS_BIT_SET(MC_serverBD.iMissionCriticalObj[iTeam], iobj)
			IF bcheckteamfail
				RUN_TEAM_OBJ_FAIL_CHECKS( iobj,iteam,MC_serverBD_4.iObjPriority[iobj][iteam])
			ENDIF
		ENDIF		
		IF MC_serverBD_4.iObjPriority[iobj][iteam] < FMMC_MAX_RULES
			IF NOT bcheckteamfail
				IF MC_serverBD_4.iObjRule[iobj][iteam] != FMMC_OBJECTIVE_LOGIC_NONE
					IF MC_serverBD_4.iObjRule[iobj][iteam] = FMMC_OBJECTIVE_LOGIC_GET_AND_HOLD
						IF MC_serverBD_4.iObjPriority[iobj][iteam] <= MC_serverBD_4.iCurrentHighestPriority[iteam]
							IF IS_ENTITY_IN_DROP_OFF_AREA(tempObj, iteam, MC_serverBD_4.iObjPriority[iobj][iteam], ciRULE_TYPE_OBJECT, iobj)							
								IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_STOCKPILE(g_FMMC_STRUCT.iAdversaryModeType)
								OR NOT IS_ENTITY_ATTACHED_TO_ANY_PED(tempObj)
									IF NOT IS_BIT_SET(MC_serverBD.iObjAtYourHolding[iteam],iobj)
										IF MC_serverBD.iObjCarrier[iobj] > -1
											tempPart = INT_TO_PARTICIPANTINDEX(MC_serverBD.iObjCarrier[iobj])
											IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
												IF MC_PlayerBD[MC_serverBD.iObjCarrier[iobj]].iteam = iteam
													tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
													SET_BIT(MC_serverBD.iObjHoldPartPoints[iobj],MC_serverBD.iObjCarrier[iobj])
													SET_STOCKPILE_LAST_PICKUP_CAPTOR(iObj, MC_serverBD.iObjCarrier[iobj])
													INCREMENT_SERVER_REMOTE_PLAYER_SCORE(tempPlayer, GET_FMMC_POINTS_FOR_TEAM(iteam,MC_serverBD_4.iObjPriority[iobj][iteam]), iteam, MC_serverBD_4.iObjPriority[iobj][iteam])
													SET_BIT(MC_serverBD.iObjAtYourHolding[iteam],iobj)
													SET_BIT(MC_serverBD.iObjCaptured, iobj)
													PRINTLN("[RCC MISSION] SETTING PACKAGE AT HOLDING: ",iobj," FOR TEAM: ", iteam)
													
													IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_STOCKPILE_SOUNDS)
														BROADCAST_FMMC_STOCKPILE_SOUND(ciStockpile_Sound_Delivery, MC_serverBD.iObjCarrier[iobj])
														PRINTLN("[RCC MISSION][StockpileSound] - Playing: ciStockpile_Sound_Delivered (1)")
													ENDIF
												ENDIF
											ENDIF
										ELSE
											IF IS_TEAM_ACTIVE(iteam)
												PRINTLN("[RCC MISSION] SETTING PACKAGE AT HOLDING: ",iobj," FOR TEAM: ", iteam)
												SET_BIT(MC_serverBD.iObjAtYourHolding[iteam],iobj)
												SET_BIT(MC_serverBD.iObjCaptured, iobj)
												IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciALTERNATIVE_HOLDING_PACKAGE_SCORING)
													INCREMENT_SERVER_TEAM_SCORE(iteam,MC_serverBD_4.iObjPriority[iobj][iteam],GET_FMMC_POINTS_FOR_TEAM(iteam,MC_serverBD_4.iObjPriority[iobj][iteam]))
												ENDIF
												SET_STOCKPILE_LAST_PICKUP_CAPTOR(iObj, MC_serverBD_1.iPTLDroppedPackageLastOwner[iobj])
												IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSixteen, ciENABLE_STOCKPILE_SOUNDS)
													BROADCAST_FMMC_STOCKPILE_SOUND(ciStockpile_Sound_Delivery, MC_serverBD_1.iPTLDroppedPackageLastOwner[iobj])
													PRINTLN("[RCC MISSION][StockpileSound] - Playing: ciStockpile_Sound_Delivered (2)")
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ELSE
									PRINTLN("[RCC MISSION] object: ",iobj," WAITING FOR OBJECT TO BE DETACHED FROM PLAYER")
								ENDIF
							ELSE
								IF DOES_ENTITY_EXIST(tempObj)
								AND (IS_ENTITY_ATTACHED_TO_ANY_PED(tempObj)
								OR bContainer)
									REMOVE_OBJECT_HOLDING_POINTS(iteam,iobj)
								ENDIF
								
								IF IS_THIS_BOMB_FOOTBALL()
								AND IS_BIT_SET(MC_serverBD.iObjAtYourHolding[iteam], iobj)
									PRINTLN("[RCC MISSION][BMBFB] CLEARING PACKAGE AT HOLDING: ",iobj," FOR TEAM: ", iteam)
									CLEAR_BIT(MC_serverBD.iObjAtYourHolding[iteam], iobj)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF MC_serverBD_4.iObjPriority[iobj][iteam]  <= iCurrentHighPriority[iteam]	
						IF MC_serverBD_4.iObjPriority[iobj][iteam] <= MC_serverBD_4.iCurrentHighestPriority[iteam]
							iTempNumPriorityRespawnObj[iteam] = iTempNumPriorityRespawnObj[iteam] + MC_serverBD_2.iCurrentObjRespawnLives[iobj]
						ENDIF
						
						icurrentHighPriority[iteam] = MC_serverBD_4.iObjPriority[iobj][iteam]
						itempObjMissionLogic[iteam] = MC_serverBD_4.iObjRule[iobj][iteam]
						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_TYPE_OPEN_SAFE)
							itempObjMissionSubLogic[iteam] = ci_HACK_SUBLOGIC_SAFE
						ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_TYPE_HACK_COMPUTER)
							itempObjMissionSubLogic[iteam] = ci_HACK_SUBLOGIC_HACK
						ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_TYPE_DOWNLOAD)
							itempObjMissionSubLogic[iteam] = ci_HACK_SUBLOGIC_DOWNLOAD
						ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_TYPE_BLOW_DOOR)
							itempObjMissionSubLogic[iteam] = ci_HACK_SUBLOGIC_BLOW
						ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_TYPE_SYNC_UNLOCK)
							itempObjMissionSubLogic[iteam] = ci_HACK_SUBLOGIC_SYNC_LOCK
						//AW HLSL
						ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_TYPE_HUMANE_LABS_SYNC)
							itempObjMissionSubLogic[iteam] = ci_HACK_SUBLOGIC_SYNC_LOCK
						ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_TYPE_HACK_COMPUTER2)	
							itempObjMissionSubLogic[iteam] = ci_HACK_SUBLOGIC_HACK_2
						ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_TYPE_CASH_GRAB)	
							itempObjMissionSubLogic[iteam] = ci_HACK_SUBLOGIC_CASH_GRAB
						ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_TYPE_OPEN_CASE)
							itempObjMissionSubLogic[iteam] = ci_HACK_SUBLOGIC_OPEN_CASE
						ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_TYPE_DRILLING)
							itempObjMissionSubLogic[iteam] = ci_HACK_SUBLOGIC_DRILLING
						ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_TYPE_OPEN_CONTAINER)
							itempObjMissionSubLogic[iteam] = ci_HACK_SUBLOGIC_OPEN_CONTAINER
						ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_TYPE_LESTER_SNAKE)
							itempObjMissionSubLogic[iteam] = ci_HACK_SUBLOGIC_LESTER_SNAKE
						ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_TYPE_BEAM_HACK)
							itempObjMissionSubLogic[iteam] = ci_HACK_SUBLOGIC_BEAM_HACK
						ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_TYPE_HOTWIRE)
							itempObjMissionSubLogic[iteam] = ci_HACK_SUBLOGIC_HOTWIRE
						ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_TYPE_INTERACT_WITH)
							itempObjMissionSubLogic[iteam] = ci_HACK_SUBLOGIC_INTERACT_WITH
						ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_TYPE_ENTER_SCRIPTED_TURRET)
							itempObjMissionSubLogic[iteam] = ci_HACK_SUBLOGIC_ENTER_SCRIPTED_TURRET
						ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_TYPE_ORDER_UNLOCK)
							itempObjMissionSubLogic[iteam] = ci_HACK_SUBLOGIC_ORDER_UNLOCK
						ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_TYPE_FINGERPRINT_CLONE)
							itempObjMissionSubLogic[iteam] = ci_HACK_SUBLOGIC_FINGERPRINT_CLONE
						ENDIF
						
						IF iOldHighPriority[iteam] > icurrentHighPriority[iteam]
							iNumHighPriorityObj[iteam] = 0
							iOldHighPriority[iteam] = icurrentHighPriority[iteam]
						ENDIF
						iNumHighPriorityLoc[iteam] = 0
						itempLocMissionLogic[iteam] = FMMC_OBJECTIVE_LOGIC_NONE
						iNumHighPriorityVeh[iteam] = 0
						itempVehMissionLogic[iteam] = FMMC_OBJECTIVE_LOGIC_NONE
						iNumHighPriorityPed[iteam] = 0
						iNumHighPriorityDeadPed[iteam] = 0
						itempPedMissionLogic[iteam] = FMMC_OBJECTIVE_LOGIC_NONE
						iNumHighPriorityPlayerRule[iteam] = 0
						itempPlayerRuleMissionLogic[iteam] = FMMC_OBJECTIVE_LOGIC_NONE
						iNumHighPriorityObj[iteam]++
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
ENDPROC



//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//
//!!! NEW SECTION BELOW: CLIENT OBJECT PROCESSING / OBJ BODY !!!
//
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************



FUNC BOOL DOES_OBJECT_HAVE_ANY_PRIORITY_FOR_ANY_TEAM(INT iobj)
	
	BOOL bHasPriority = FALSE
	
	INT iTeamLoop
	
	FOR iTeamLoop = 0 TO (MC_serverBD.iNumberOfTeams - 1)
		IF (MC_serverBD_4.iObjPriority[iobj][iTeamLoop] >= 0)
		AND (MC_serverBD_4.iObjPriority[iobj][iTeamLoop] < FMMC_MAX_RULES)
			IF (MC_serverBD.iNumberOfPlayingPlayers[iTeamLoop] > 0)
				bHasPriority = TRUE
				iTeamLoop = MC_serverBD.iNumberOfTeams //Break out, we already know someone has priority left
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN bHasPriority
	
ENDFUNC

FUNC BOOL IS_OBJECT_OUTSIDE_TEAM_MISSION_BOUNDS(INT iObj, OBJECT_INDEX tempObj, INT iTeam)
	
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	BOOL bValidBounds = FALSE
	BOOL bInBounds = FALSE
	BOOL bOutOfBounds = FALSE
	
	BOOL bIgnoreLeaveArea = FALSE
	IF NOT (IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iLeaveAreaBitset, iRule)
	AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iLeaveArea2Bitset, iRule))
		bIgnoreLeaveArea = TRUE
		PRINTLN("[MC] IS_OBJECT_OUTSIDE_TEAM_MISSION_BOUNDS - Setting bIgnoreLeaveArea to TRUE")
	ENDIF
	
	IF iRule < FMMC_MAX_RULES
		IF (bIgnoreLeaveArea AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iLeaveAreaBitset, iRule))
		OR NOT bIgnoreLeaveArea
			IF g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sBoundsStruct[ iRule ].fRadius > 0
				bValidBounds = TRUE
				IF VDIST2(GET_ENTITY_COORDS(tempObj),g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].vPos) < POW(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].fRadius, 2)
					bInBounds = TRUE
				ELSE
					PRINTLN("[KH][2675414] IS_OBJECT_OUTSIDE_TEAM_MISSION_BOUNDS - OUT OF BOUNDS - RADIUS 1")
				ENDIF
			ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].fWidth > 0
				bValidBounds = TRUE
				IF IS_ENTITY_IN_ANGLED_AREA(tempObj,g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].vPos1,g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].vPos2,g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].fWidth)
					bInBounds = TRUE
				ELSE
					PRINTLN("[KH][2675414] IS_OBJECT_OUTSIDE_TEAM_MISSION_BOUNDS - OUT OF BOUNDS - ANGLED AREA 1")
				ENDIF
			ENDIF
		ENDIF
		
		IF (bIgnoreLeaveArea AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iLeaveArea2Bitset, iRule))
		OR NOT bIgnoreLeaveArea
			IF g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].sBoundsStruct2[ iRule ].fRadius > 0
				bValidBounds = TRUE
				IF VDIST2(GET_ENTITY_COORDS(tempObj),g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule].vPos) < POW(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule].fRadius, 2)
					bInBounds = TRUE
				ELSE
					VECTOR vTempVec = GET_ENTITY_COORDS(tempObj)
					PRINTLN("[KH][2675414] IS_OBJECT_OUTSIDE_TEAM_MISSION_BOUNDS - OUT OF BOUNDS - RADIUS 2 - object coords: ", vTempVec)
				ENDIF
			ELIF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule].fWidth > 0
				bValidBounds = TRUE
				IF IS_ENTITY_IN_ANGLED_AREA(tempObj,g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule].vPos1,g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule].vPos2,g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct2[iRule].fWidth)
					bInBounds = TRUE
				ELSE
					PRINTLN("[KH][2675414] IS_OBJECT_OUTSIDE_TEAM_MISSION_BOUNDS - OUT OF BOUNDS - ANGLED AREA 2")
				ENDIF
			ENDIF
		ENDIF
		
		IF bValidBounds
			IF bInBounds
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iLeaveAreaBitset, iRule)
				AND NOT bIgnoreLeaveArea
					PRINTLN("[KH][2675414] IS_OBJECT_OUTSIDE_TEAM_MISSION_BOUNDS - iLeaveAreaBitset - bOutOfBounds TRUE")
					bOutOfBounds = TRUE
				ENDIF
			ELSE
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iPlayAreaBitset, iRule)
				OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSet, cibsOBJ_Reset_If_Dropped_Out_Of_Bounds)
					PRINTLN("[KH][2675414] IS_OBJECT_OUTSIDE_TEAM_MISSION_BOUNDS - iPlayAreaBitset, cibsOBJ_Reset_If_Dropped_Out_Of_Bounds - bOutOfBounds TRUE")
					bOutOfBounds = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	RETURN bOutOfBounds
	
ENDFUNC

FUNC BOOL IS_OBJECT_OUTSIDE_ANY_TEAM_MISSION_BOUNDS(INT iObj, OBJECT_INDEX tempObj)
	
	INT iTeam
	
	FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
		IF IS_OBJECT_OUTSIDE_TEAM_MISSION_BOUNDS(iObj, tempObj, iTeam)
			RETURN TRUE
		ENDIF
	ENDFOR
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL OBJECT_WAS_DROPPED_IN_END_ZONE(object_INDEX obj)
	
	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]

	INT i = 0
	IF iRule < FMMC_MAX_RULES
		FOR i = 0 TO (FMMC_MAX_TEAMS - 1)
			IF IS_POINT_IN_ANGLED_AREA(GET_ENTITY_COORDS(obj), g_FMMC_STRUCT.sFMMCEndConditions[i].vDropOff[iRule], 
			g_FMMC_STRUCT.sFMMCEndConditions[i].vDropOff2[iRule], 
			g_FMMC_STRUCT.sFMMCEndConditions[i].fDropOffRadius[iRule])
				IF NOT IS_ENTITY_ATTACHED_TO_ANY_PED(obj)
					PRINTLN("[KH] OBJECT_WAS_DROPPED_IN_END_ZONE - OBJECT DROPPED IN END ZONE - RESETTING OBJECT TO SPAWN")
					RETURN TRUE
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: PICKUP OBJECTS !
//
//************************************************************************************************************************************************************


PROC SET_PACKAGE_NOT_COLLECTABLE_FOR_TEAM(OBJECT_INDEX tempObj,INT iobj, INT iteam)
	
	IF NOT IS_THIS_A_SPECIAL_PICKUP( iObj )
	AND IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_FIRST_UPDATE_DONE)
	AND IS_OBJECT_A_PORTABLE_PICKUP(tempObj)

		PRINTLN("[RCC MISSION] SET_PACKAGE_NOT_COLLECTABLE_FOR_TEAM - SET_TEAM_PICKUP_OBJECT - g_FMMC_STRUCT_ENTITIES.sPlacedObject[", iObj, "].mn = ", GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].mn), " iTeam = ", iTeam)
		
		SET_TEAM_PICKUP_OBJECT(tempObj,iteam,FALSE)
		
		IF MC_serverBD.iObjCarrier[iobj] = iLocalPart
			IF iteam = MC_playerBD[iPartToUse].iteam
				IF IS_ENTITY_ATTACHED_TO_ANY_PED(tempObj)
					DETACH_PORTABLE_PICKUP_FROM_PED(tempObj)
					
					//IF (g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].mn = INT_TO_ENUM(MODEL_NAMES, HASH("pil_Prop_Pilot_Icon_01")))
					IF NOT DOES_OBJECT_HAVE_ANY_PRIORITY_FOR_ANY_TEAM(iobj)
					OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectBitSet, cibsOBJ_InvisibleOnFirstDelivery)
					OR g_bMissionEnding
						//Ideally this would just set the visibility to FALSE, but that seems to take a frame to sync up on other machines
						// so to avoid that, just delete it - Bug 1927471
						PRINTLN("[RCC MISSION] SET_PACKAGE_NOT_COLLECTABLE_FOR_TEAM - Pilot school flag (object number ",iobj,") is no longer needed, setting invisible...")
						//DELETE_NET_ID(MC_serverBD_1.sFMMC_SBD.niObject[iobj])
						SET_ENTITY_VISIBLE(tempObj,FALSE)
						SET_ENTITY_COLLISION(tempObj,FALSE)
						HIDE_PORTABLE_PICKUP_WHEN_DETACHED(tempObj, TRUE ) 
					ENDIF
					
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC RUGBY_RESET_IN_ENDZONE_OBJECT_RESPAWN(OBJECT_INDEX oiPassed)
	INT iTeam = MC_playerBD[iPartToUse].iTeam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSix[iRule], ciBS_RULE6_BOUNDS_CENTRE_LINE_RESPAWN)
		VECTOR vPoint1 = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].vPos1
		VECTOR vPoint2 = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].vPos2
		
		vPoint1.Z = FMAX(vPoint1.Z, vPoint2.Z)
		vPoint2.Z = FMAX(vPoint1.Z, vPoint2.Z)
		
		VECTOR vCentrePoint = vPoint1 + ((vPoint2 - vPoint1) / 2.0)
		
		FLOAT fPlayableArea = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].fCentreLinePlayableArea
		
		VECTOR vNormalised = NORMALISE_VECTOR(vPoint2 - vPoint1)
		VECTOR vOffset = vNormalised * g_FMMC_STRUCT.sFMMCEndConditions[g_CreatorsSelDetails.iSelectedTeam].sBoundsStruct[g_CreatorsSelDetails.iRow].fCentreLineOffset
		
		VECTOR vClosestPoint = GET_CLOSEST_POINT_ON_LINE(GET_ENTITY_COORDS(localPlayerPed, FALSE), vCentrePoint - fPlayableArea, vCentrePoint + fPlayableArea)
		
		GET_GROUND_Z_FOR_3D_COORD(vClosestPoint, vClosestPoint.Z, TRUE)
		GET_GROUND_Z_FOR_3D_COORD(vCentrePoint, vCentrePoint.Z, TRUE)
		
		VECTOR vObjMin, vObjMax
		
		GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(oiPassed), vObjMin, vObjMax)
		
		vCentrePoint.Z += (vObjMax.Z - vObjMin.Z) / 2
		
		SET_ENTITY_COORDS(oiPassed, vCentrePoint + vOffset, FALSE, FALSE, FALSE, FALSE)
	ENDIF
ENDPROC

PROC RUGBY_CENTRE_LINE_OBJECT_RESPAWN(OBJECT_INDEX oiPassed)
	INT iTeam = MC_playerBD[iPartToUse].iTeam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	IF iRule < FMMC_MAX_RULES
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSix[iRule], ciBS_RULE6_BOUNDS_CENTRE_LINE_RESPAWN)
			VECTOR vPoint1 = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].vPos1
			VECTOR vPoint2 = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].vPos2
			
			vPoint1.Z = FMAX(vPoint1.Z, vPoint2.Z)
			vPoint2.Z = FMAX(vPoint1.Z, vPoint2.Z)
			
			VECTOR vCentrePoint = vPoint1 + ((vPoint2 - vPoint1) / 2.0)
			VECTOR vNormalised = NORMALISE_VECTOR(vPoint2 - vPoint1)
			VECTOR vPlayableArea = vNormalised * g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].fCentreLinePlayableArea
			
			VECTOR vClosestPoint = GET_CLOSEST_POINT_ON_LINE(GET_ENTITY_COORDS(localPlayerPed, FALSE), vCentrePoint - vPlayableArea, vCentrePoint + vPlayableArea)
			
			GET_GROUND_Z_FOR_3D_COORD(vClosestPoint, vClosestPoint.Z, TRUE)
			
			VECTOR vObjMin, vObjMax
			
			GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(oiPassed), vObjMin, vObjMax)
			
			vClosestPoint.Z -= vObjMin.Z
			
			SET_ENTITY_COORDS(oiPassed, vClosestPoint, FALSE, FALSE, FALSE, FALSE)
		ENDIF
	ENDIF
ENDPROC

FUNC INT DETACH_PORTABLE_PICKUP_FROM_SPECIFIC_PED( ENTITY_INDEX ped, BOOL bReset = FALSE, BOOL bStolen = FALSE)
	INT iobj
	OBJECT_INDEX tempObj
	INT iLastDrop = -1
	FOR iobj = 0 TO (FMMC_MAX_NUM_OBJECTS-1)
		IF MC_serverBD.iObjCarrier[iobj] = iPartToUse
			IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niObject[iobj])
				tempObj = NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[iobj])
				IF IS_ENTITY_ATTACHED_TO_ENTITY( tempObj, ped )
					IF IS_PLAYER_IN_RESPAWN_BACK_AT_START_LOGIC(iLocalPart)
						PRINTLN("[KH] DETACH_PORTABLE_PICKUP_FROM_SPECIFIC_PED - IS_PLAYER_IN_RESPAWN_BACK_AT_START_LOGIC is TRUE - Resetting the object as they somehow managed to get the ball after the round had finished")
						SET_UP_FMMC_OBJ_RC(tempObj,iobj,MC_ServerBD_1.sFMMC_SBD.niPed, MC_ServerBD_1.sLEGACYMissionContinuityVars,TRUE,TRUE)
					ENDIF
					
					IF (IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectBitSet, cibsOBJ_Reset_If_Dropped_Out_Of_Bounds)
					AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
					AND IS_OBJECT_OUTSIDE_ANY_TEAM_MISSION_BOUNDS(iobj, tempObj))
					OR bReset
						PRINTLN("[KH][2675414] DETACH_PORTABLE_PICKUP_FROM_SPECIFIC_PED - RESETTING OBJECT")
						//Reset object
						SET_UP_FMMC_OBJ_RC(tempObj,iobj,MC_ServerBD_1.sFMMC_SBD.niPed, MC_ServerBD_1.sLEGACYMissionContinuityVars,TRUE,TRUE)
						RUGBY_CENTRE_LINE_OBJECT_RESPAWN(tempObj)	//CENTRE LINE RESPAWN - Respawn the get & deliver object along the centre line (Bug 2700219)
					ENDIF
					
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciPICKUP_DROP_ON_TRIANGLE)
					OR bStolen
						//Just disabling this as soon as it is dropped to prevent instant pickup
						PREVENT_COLLECTION_OF_PORTABLE_PICKUP(tempObj, TRUE, TRUE)
						PRINTLN("[DETACH_PORTABLE_PICKUP_FROM_SPECIFIC_PED][STOLEN] - PREVENT_COLLECTION_OF_PORTABLE_PICKUP called for iObj: ", iObj)
					//	iObjectJustHadStolen = iObj
					ENDIF
					
					PRINTLN("[DETACH_PORTABLE_PICKUP_FROM_SPECIFIC_PED]- DETACH_PORTABLE_PICKUP_FROM_PED called")
					DETACH_PORTABLE_PICKUP_FROM_PED(tempObj)
					
					iLastDrop = iobj
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	PRINTLN("[DETACH_PORTABLE_PICKUP_FROM_SPECIFIC_PED] - Returning iObj ",iLastDrop)
	RETURN iLastDrop
ENDFUNC

PROC DETACH_ANY_PORTABLE_PICKUPS()

INT iobj
OBJECT_INDEX tempObj
	
	FOR iobj = 0 TO (FMMC_MAX_NUM_OBJECTS-1)
		IF MC_serverBD.iObjCarrier[iobj] = iPartToUse
			IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niObject[iobj])
				tempObj = NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[iobj])
				IF IS_ENTITY_ATTACHED_TO_ANY_PED(tempObj)
					DETACH_PORTABLE_PICKUP_FROM_PED(tempObj)
					IF (g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].mn = INT_TO_ENUM(MODEL_NAMES, HASH("pil_Prop_Pilot_Icon_01")))
					AND (NOT DOES_OBJECT_HAVE_ANY_PRIORITY_FOR_ANY_TEAM(iobj))
						PRINTLN("[RCC MISSION] DETACH_ANY_PORTABLE_PICKUPS - Pilot school flag (object number ",iobj,") is no longer needed, setting invisible...")
						SET_ENTITY_VISIBLE(tempObj,FALSE)
					ENDIF
					
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectBitSet, cibsOBJ_Reset_If_Dropped_Out_Of_Bounds)
					AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
					AND IS_OBJECT_OUTSIDE_ANY_TEAM_MISSION_BOUNDS(iobj, tempObj)
						PRINTLN("[KH][2675414] DETACH_ANY_PORTABLE_PICKUPS - RESETTING OBJECT")
						//Reset
						SET_UP_FMMC_OBJ_RC(tempObj, iobj, MC_ServerBD_1.sFMMC_SBD.niPed, MC_ServerBD_1.sLEGACYMissionContinuityVars, TRUE, TRUE)
						RUGBY_CENTRE_LINE_OBJECT_RESPAWN(tempObj)	//CENTRE LINE RESPAWN - Respawn the get & deliver object along the centre line (Bug 2700219)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
ENDPROC

PROC DETACH_AND_RESET_ANY_PICKUPS()
	
	INT iobj
	OBJECT_INDEX tempObj
	
	FOR iobj = 0 TO (FMMC_MAX_NUM_OBJECTS-1)
		IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niObject[iobj])
			tempObj = NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[iobj])
			IF IS_ENTITY_ATTACHED_TO_ENTITY(tempObj, LocalPlayerPed)
			AND NETWORK_HAS_CONTROL_OF_ENTITY(tempObj)
				PRINTLN("[KH] DETACH_AND_RESET_ANY_PICKUPS - DETACHING AND RESETTING PICKUP FROM PLAYER")
				DETACH_PORTABLE_PICKUP_FROM_PED(tempObj)
				SET_UP_FMMC_OBJ_RC(tempObj,iobj,MC_ServerBD_1.sFMMC_SBD.niPed, MC_ServerBD_1.sLEGACYMissionContinuityVars,TRUE,TRUE)
			ELSE
				PRINTLN("[KH] DETACH_AND_RESET_ANY_PICKUPS - NOT ATTACHED/OWNER OF ", iObj)
			ENDIF
		ENDIF
	ENDFOR
	
ENDPROC
	

/// PURPOSE: Allows players to manually drop pickup objects they are carrying using DPAD_LEFT or TRIANGLE
PROC PROCESS_PACKAGE_HANDLING()
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciPICKUP_DROP_ON_TRIANGLE)
		INT iTeam = MC_playerBD[iPartToUse].iteam
		INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
		
		IF iRule < FMMC_MAX_RULES
			IF NOT IS_PED_INJURED(LocalPlayerPed)
			AND NOT IS_BROWSER_OPEN()
			AND NOT IS_CELLPHONE_CAMERA_IN_USE()
			AND GET_MC_SERVER_GAME_STATE() != GAME_STATE_END
			AND NOT CAN_I_SWAN_DIVE(iTeam, iRule)
			AND NOT IS_BIT_SET(iLocalBoolCheck16, LBOOL16_IS_DIVE_SCORE_ANIM_IN_PROGRESS)
			AND NOT IS_PLAYER_IN_RESPAWN_BACK_AT_START_LOGIC(iPartToUse)
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					DISPLAY_HELP_TEXT_THIS_FRAME("RUG_HELP_1", FALSE)
				ELSE
					IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("IBI_HELP_1")
					AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("IBI_HELP_2")
					AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MC_SCORE_HT") 
					AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MC_SCORE_H")
					AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("IBI_HELP_3")
						DISPLAY_HELP_TEXT_THIS_FRAME("RUG_HELP_1", FALSE)
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_EXIT)
			AND NOT IS_BIT_SET(iLocalBoolCheck16, LBOOL16_IS_DIVE_SCORE_ANIM_IN_PROGRESS)
				IF NOT HAS_NET_TIMER_STARTED(stPickupTimeoutTimer)
					START_NET_TIMER(stPickupTimeoutTimer)
					PRINTLN("[KH] PROCESS_PACKAGE_HANDLING - STARTING PACKAGE PICKUP TIMEOUT TIMER")
				ELSE
					REINIT_NET_TIMER(stPickupTimeoutTimer)
				ENDIF
				
				DETACH_PORTABLE_PICKUP_FROM_SPECIFIC_PED(LocalPlayerPed)
				
				IF DOES_PLAYER_HAVE_WEAPON(wtCachedLastWeaponHeld)
					SET_CURRENT_PED_WEAPON(LocalPlayerPed, wtCachedLastWeaponHeld, TRUE)
				ELSE
					SET_CURRENT_PED_WEAPON(LocalPlayerPed, GET_BEST_PED_WEAPON(LocalPlayerPed), TRUE)
				ENDIF
				bHasObjectJustBeenDropped = TRUE

				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciENABLE_RUGBY_SOUNDS)
					BROADCAST_RUGBY_PLAY_SOUND(ciRUGBY_SOUND_DROPPED_BALL)
				ENDIF
			ENDIF
		ENDIF
	ELSE
		DISPLAY_HELP_TEXT_THIS_FRAME("FMMCC_DROP_PACK", FALSE)
		
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
			IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_LIONS_DEN(g_FMMC_STRUCT.iAdversaryModeType)
				IF NOT HAS_NET_TIMER_STARTED(stPickupTimeoutTimer)
					START_NET_TIMER(stPickupTimeoutTimer)
					PRINTLN("[KH] PROCESS_PACKAGE_HANDLING - STARTING PACKAGE PICKUP TIMEOUT TIMER")
				ELSE
					REINIT_NET_TIMER(stPickupTimeoutTimer)
				ENDIF
			ENDIF
			
			DETACH_PORTABLE_PICKUP_FROM_SPECIFIC_PED(LocalPlayerPed)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///		[KH]Checks whether the player should be able to pick up an object depending on creator settings that are set
PROC PROCESS_SHOULD_PREVENT_OBJECT_PICKUP(OBJECT_INDEX pickupID)
	
	BOOL bBlockingObjectPickup = FALSE
	BOOL bTriangleHeld = FALSE
	
	//If the player is holding triangle they should not be able to pick the object up
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciHOLD_TRIANGLE_NO_PICKUP)
		IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_EXIT)
		OR (IS_PLAYER_RESPAWNING(PLAYER_ID()) 
		AND NOT IS_FAKE_MULTIPLAYER_MODE_SET(TRUE))
			IF IS_PLAYER_RESPAWNING(PLAYER_ID())
				PRINTLN("[KH] PROCESS_SHOULD_PREVENT_OBJECT_PICKUP - PLAYER IS RESPAWNING - PREVENTING OBJECT PICKUP")
			ELSE
				PRINTLN("[KH] PROCESS_SHOULD_PREVENT_OBJECT_PICKUP - TRIANGLE IS PRESSED - PREVENTING OBJECT PICKUP")
			ENDIF
			bBlockingObjectPickup = TRUE
			bTriangleHeld = TRUE
		ELSE
			bBlockingObjectPickup = FALSE
		ENDIF
	ENDIF
	
	//After the player has dropped the object there should be a cooldown period before they can pick it back up
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciPICKUP_DROP_ON_TRIANGLE)
	OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_LIONS_DEN(g_FMMC_STRUCT.iAdversaryModeType)
		IF HAS_NET_TIMER_STARTED(stPickupTimeoutTimer)
			PRINTLN("[KH] PROCESS_SHOULD_PREVENT_OBJECT_PICKUP - TIMEOUT TIMER HAS STARTED - PREVENTING OBJECT PICKUP")
			INT iTimeElapsed = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stPickupTimeoutTimer)
			INT iTimeout = iPickupTimeoutTime
			
			IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_LIONS_DEN(g_FMMC_STRUCT.iAdversaryModeType)
				iTimeout = g_FMMC_STRUCT.iLionsDenPickupCollectionDelay
			ENDIF
			
			IF iTimeElapsed > iTimeout
				PRINTLN("[KH] PROCESS_SHOULD_PREVENT_OBJECT_PICKUP - RESETTING TIMER - ALLOWING OBJECT PICKUP")
				RESET_NET_TIMER(stPickupTimeoutTimer)
				
				IF !bTriangleHeld
					bBlockingObjectPickup = FALSE
				ENDIF
			ELSE
				bBlockingObjectPickup = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciPICKUP_DROP_ON_TRIANGLE)
	OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciHOLD_TRIANGLE_NO_PICKUP)
		
		//Stops the player picking the object up while respawning in the round
		IF IS_PLAYER_IN_RESPAWN_BACK_AT_START_LOGIC(iLocalPart)
			PRINTLN("[KH] PROCESS_SHOULD_PREVENT_OBJECT_PICKUP - PLAYER IS IN RESPAWN AT START LOGIC - PREVENTING OBJECT PICKUP")
			bBlockingObjectPickup = TRUE
		ENDIF
		
	ENDIF
	
	//Stops the player picking up any objects while manually respawning
	IF (manualRespawnState >= eManualRespawnState_FADING_OUT)
	OR HAS_NET_TIMER_STARTED(timerManualRespawn)
		PRINTLN("[KH] PROCESS_SHOULD_PREVENT_OBJECT_PICKUP - PLAYER IS MANUALLY RESPAWNING - PREVENTING OBJECT PICKUP")
		bBlockingObjectPickup = TRUE
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF bBlockingObjectPickup != bBlockingObjectLastFrame
			PRINTLN("[KH] PROCESS_SHOULD_PREVENT_OBJECT_PICKUP - bBlockingObjectPickup changed to: ", bBlockingObjectPickup)
		ENDIF

		bBlockingObjectLastFrame = bBlockingObjectPickup
	#ENDIF
	
	IF IS_OBJECT_A_PORTABLE_PICKUP(pickupID)
		PREVENT_COLLECTION_OF_PORTABLE_PICKUP(pickupID, bBlockingObjectPickup, TRUE)
	ENDIF
	
ENDPROC

PROC PROCESS_OBJECT_CARRIED_FOR_DISTANCE(OBJECT_INDEX obj, INT iObj)
	
	IF NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
	OR iSpectatorTarget = -1
	
		IF NOT IS_PLAYER_IN_RESPAWN_BACK_AT_START_LOGIC(iPartToUse)
		
			INT iTeam = MC_playerBD[iPartToUse].iteam
			INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
			
			VECTOR vPoint1 = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].vPos1
			VECTOR vPoint2 = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].sBoundsStruct[iRule].vPos2
					
			vPoint1.Z = FMAX(vPoint1.Z, vPoint2.Z)
			vPoint2.Z = FMAX(vPoint1.Z, vPoint2.Z)
			
			//Get position where ball was picked up
			IF piPlayerCarryingBall = INVALID_PLAYER_INDEX()
			AND MC_ServerBD.iObjCarrier[iobj] = iPartToUse
				PRINTLN("[KH] DISTANCE_GAINED - ASSIGNING PLAYER CARRYING BALL: ", iObj)
				piPlayerCarryingBall = PLAYER_ID()
				iObjBeingCarried = iObj
				vBallCollectionPoint = GET_CLOSEST_POINT_ON_LINE(GET_ENTITY_COORDS(obj), vPoint1, vPoint2)
			ENDIF

			IF iRule < FMMC_MAX_RULES
				IF piPlayerCarryingBall != INVALID_PLAYER_INDEX()
				AND piPlayerCarryingBall = PLAYER_ID()
				AND iObjBeingCarried = iObj
				AND MC_ServerBD.iObjCarrier[iObj] != iPartToUse
					PRINTLN("[KH] DISTANCE_GAINED - BALL DROPPED BY PLAYER: ", iobj)
					
					VECTOR vDropPointCentre
					VECTOR vDropOffVec1 = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vDropOff[iRule]
					VECTOR vDropOffVec2 = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].vDropOff2[iRule]
					
					vDropPointCentre.x = ((vDropOffVec1.x + vDropOffVec2.x) / 2)
					vDropPointCentre.y = ((vDropOffVec1.y + vDropOffVec2.y) / 2)
					vDropPointCentre.z = ((vDropOffVec1.z + vDropOffVec2.z) / 2)
					
					vBallDropPoint = GET_CLOSEST_POINT_ON_LINE(GET_ENTITY_COORDS(obj), vPoint1, vPoint2)
					
					INT iDistanceBallCarriedMeters
					INT iDistanceBallCarriedFeet
					INT iDistanceWhenCollected
					INT iDistanceWhenDropped
					
					iDistanceBallCarriedFeet = ROUND(CONVERT_METERS_TO_FEET(GET_DISTANCE_BETWEEN_COORDS(vBallCollectionPoint, vBallDropPoint)))
					iDistanceBallCarriedMeters = ROUND(GET_DISTANCE_BETWEEN_COORDS(vBallCollectionPoint, vBallDropPoint))
					
					IF NOT SHOULD_USE_METRIC_MEASUREMENTS()
						PRINTLN("[KH] DISTANCE_GAINED - BALL HAS BEEN CARRIED IN FEET: ", iDistanceBallCarriedFeet)
						
						//calculate the centre of the drop off area so we can tell if we're closer to it or not
						iDistanceWhenCollected = ROUND(CONVERT_METERS_TO_FEET(GET_DISTANCE_BETWEEN_COORDS(vBallCollectionPoint, vDropPointCentre)))
						iDistanceWhenDropped = ROUND(CONVERT_METERS_TO_FEET(GET_DISTANCE_BETWEEN_COORDS(vBallDropPoint, vDropPointCentre)))
					ELSE
						PRINTLN("[KH] DISTANCE_GAINED - BALL HAS BEEN CARRIED IN METERS: ", iDistanceBallCarriedMeters)
						
						//calculate the centre of the drop off area so we can tell if we're closer to it or not
						iDistanceWhenCollected = ROUND(GET_DISTANCE_BETWEEN_COORDS(vBallCollectionPoint, vDropPointCentre))
						iDistanceWhenDropped = ROUND(GET_DISTANCE_BETWEEN_COORDS(vBallDropPoint, vDropPointCentre))

					ENDIF
					
					//check if the player has gained distance (got closer to the drop off point)
					BOOL bDistanceGained = FALSE
					IF iDistanceWhenCollected > iDistanceWhenDropped
						bDistanceGained = TRUE
					ENDIF
					
					//Stops the ticker being fired if they have passed the try line
					VECTOR vClosestPointToDropCentre = GET_CLOSEST_POINT_ON_LINE(vDropPointCentre, vPoint1, vPoint2)
					VECTOR vClosestPointToPlayer = GET_CLOSEST_POINT_ON_LINE(GET_ENTITY_COORDS( LocalPlayerPed ), vPoint1, vPoint2)
					
					FLOAT fDistFromTryLine = VDIST( vClosestPointToDropCentre, vClosestPointToPlayer )
					fDistFromTryLine = fDistFromTryLine - (g_FMMC_STRUCT.sFMMCEndConditions[iTeam].fDropOffRadius[iRule] / 2)										   					   				   
					
					IF (iDistanceBallCarriedMeters != 0 OR iDistanceBallCarriedFeet != 0)
					AND fDistFromTryLine > 0
						BROADCAST_FMMC_DISTANCE_GAINED_TICKER(iTeam, iDistanceBallCarriedMeters, iDistanceBallCarriedFeet, bDistanceGained)
					ENDIF
					
					vBallDropPoint = <<0,0,0>>
					vBallCollectionPoint = <<0,0,0>>
					piPlayerCarryingBall = INVALID_PLAYER_INDEX()
					iObjBeingCarried = -1
				ENDIF
			ENDIF
		
		ENDIF
	
	ENDIF
ENDPROC

PROC DETACH_OBJECT_FROM_PLAYER_ON_SUDDEN_DEATH(OBJECT_INDEX oiPassed, INT iObj)
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciDAWN_RAID_ENABLE_MODE)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].mn = INT_TO_ENUM(MODEL_NAMES, HASH("imp_prop_adv_hdsec"))
			IF IS_ENTITY_ATTACHED_TO_ENTITY(oiPassed, LocalPlayerPed)
				PRINTLN("[KH] DETACH_OBJECT_FROM_PLAYER_ON_SUDDEN_DEATH - Detaching the object and making it uncollectable")
				DETACH_PORTABLE_PICKUP_FROM_PED(oiPassed)
			ENDIF
			
			PRINTLN("[KH] DETACH_OBJECT_FROM_PLAYER_ON_SUDDEN_DEATH - Making the object uncollectable")
			PREVENT_COLLECTION_OF_PORTABLE_PICKUP(oiPassed, TRUE, FALSE)
			SET_ENTITY_VISIBLE(oiPassed, FALSE)
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_FROZEN_OBJECTS(OBJECT_INDEX oiPassed, INT iObj)

	IF IS_ENTITY_ATTACHED(oiPassed)
		PRINTLN("MAINTAIN_FROZEN_OBJECTS - Object ", iObj, " is attached to something and therefore cannot be frozen")
		EXIT
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSet,cibsOBJ_FreezePosition)
		FREEZE_ENTITY_POSITION(oiPassed,TRUE)
	ENDIF
ENDPROC

PROC FIND_AND_STORE_NO_WEAPONS_ZONE()

INT i
	
	IF iStoredNoWeaponZone = -1
		FOR i = 0 TO FMMC_MAX_NUM_ZONES - 1
			IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iType = ciFMMC_ZONE_TYPE__BLOCK_WEAPONS
				PRINTLN("[RCC MISSION][EXPLODE]FIND_NO_WEAPONS_ZONE iStoredNoWeaponZone = ", i)
				iStoredNoWeaponZone = i
			ENDIF
		ENDFOR
	ENDIF

ENDPROC

PROC FIND_AND_STORE_SNIPERBALL_PACKAGE(OBJECT_INDEX tempObj, INT iObj)
	IF iSniperBallObj = -1
		IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iExplodeTime != -1
			oiSniperBallPackage = tempObj
			iSniperBallObj = iObj
		ENDIF	
	ENDIF
ENDPROC

PROC PLAY_EXPLODE_OUT_OF_ZONE_AUDIO(OBJECT_INDEX tempObj)

	IF iSpectatorTarget = -1
	AND IS_SOUND_ID_VALID(iExplodeCountdownSound)
	AND HAS_SOUND_FINISHED(iExplodeCountdownSound)
		PLAY_SOUND_FROM_ENTITY(iExplodeCountdownSound, "Explosion_Timer", tempObj, "DLC_Lowrider_Relay_Race_Sounds", TRUE, 20)
	
		PRINTLN("[RCC MISSION][EXPLODE] PROCESS_OBJECTS_TO_EXPLODE_WHEN_OUT_OF_ZONE")
	ENDIF
	
	IF iSniperBallObj != -1
		IF IS_SOUND_ID_VALID(iExplodeCountdownSound)
			IF NOT HAS_SOUND_FINISHED(iExplodeCountdownSound)
				SET_VARIABLE_ON_SOUND(iExplodeCountdownSound, "Time",  TO_FLOAT(iExplodeLocalTimer / (g_FMMC_STRUCT_ENTITIES.sPlacedObject[iSniperBallObj].iExplodeTime * 1000)))
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC DRAW_EXPLOSION_TIMER_HUD()
	IF iExplodeLocalTimer != -1
		DRAW_GENERIC_TIMER(iExplodeLocalTimer, "SB_OOP" ,0,TIMER_STYLE_DONTUSEMILLISECONDS,-1,PODIUMPOS_NONE,HUDORDER_TOP,FALSE,HUD_COLOUR_RED,HUDFLASHING_NONE,0,FALSE,HUD_COLOUR_RED)
	ENDIF
ENDPROC

FUNC BOOL HAS_OBJECT_BEEN_TAKEN_OUT_OF_ZONE(OBJECT_INDEX tempObj, INT iZone)
	
	//Obj attached
	//Obj Outside of zone
	IF DOES_ENTITY_EXIST(tempObj)
		//If object has been dropped
		IF NOT IS_ENTITY_ATTACHED(tempObj)
			IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[0])
				IF NOT IS_ENTITY_IN_ANGLED_AREA(tempObj,g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[0],g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[1],g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].fRadius)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

PROC PROCESS_OBJECTS_TO_EXPLODE_WHEN_OUT_OF_ZONE()
	
	VECTOR vObjCoord
	
	IF NOT IS_BIT_SET(bsHasObjExploded, iSniperBallObj)
		IF DOES_ENTITY_EXIST(oiSniperBallPackage)
			IF iStoredNoWeaponZone != -1
				IF HAS_OBJECT_BEEN_TAKEN_OUT_OF_ZONE(oiSniperBallPackage,iStoredNoWeaponZone)
					IF iExplodeLocalTimer != -1
						IF MANAGE_THIS_MINIGAME_TIMER(iExplodeLocalTimer, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iSniperBallObj].iExplodeTime)
							PRINTLN("[RCC MISSION][EXPLODE] PROCESS_OBJECTS_TO_EXPLODE_WHEN_OUT_OF_ZONE -  iobj: ", iSniperBallObj, " has been damaged. ")
							vObjCoord = GET_ENTITY_COORDS(oiSniperBallPackage,FALSE)
							ADD_EXPLOSION(vObjCoord,EXP_TAG_BARREL)
							SET_BIT(bsHasObjExploded, iSniperBallObj)
						ENDIF
						PLAY_EXPLODE_OUT_OF_ZONE_AUDIO(oiSniperBallPackage)
						DRAW_EXPLOSION_TIMER_HUD()
					ELSE
						PRINTLN("[RCC MISSION][EXPLODE] PROCESS_OBJECTS_TO_EXPLODE_WHEN_OUT_OF_ZONE - setting explode timer")
						iExplodeLocalTimer = GET_GAME_TIMER() 
					ENDIF
				ELSE
					IF iExplodeLocalTimer != - 1
						PRINTLN("[RCC MISSION][EXPLODE] PROCESS_OBJECTS_TO_EXPLODE_WHEN_OUT_OF_ZONE - resetting timer")
					ENDIF
					iExplodeLocalTimer = -1
					IF IS_SOUND_ID_VALID(iExplodeCountdownSound)
					AND HAS_SOUND_FINISHED(iExplodeCountdownSound)
						STOP_SOUND(iExplodeCountdownSound)
						RELEASE_SOUND_ID(iExplodeCountdownSound)
						
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_CCTV_CAMERA(FMMC_CCTV_DATA& sCCTVData)
	#IF FEATURE_CASINO_HEIST
		//  CASINO_HEIST_OUTFIT_IN__BUGSTAR
		IF  IS_BIT_SET(sCCTVData.iCCTVBS, ciCCTV_BS_IgnoreDisguise_Bugstar)
			PRINTLN("[RCC MISSION] DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_CCTV_CAMERA - CCTV ignores bugstar disguise")			
			IF  IS_LOCAL_PLAYER_WEARING_A_BUGSTAR_OUTFIT()
				PRINTLN("[RCC MISSION] DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_CCTV_CAMERA - Player in Bugstar disguise")			
				RETURN TRUE
			ENDIF
		ENDIF
		
		// CASINO_HEIST_OUTFIT_IN__MECHANIC  
		
		IF IS_BIT_SET(sCCTVData.iCCTVBS, ciCCTV_BS_IgnoreDisguise_Mechanic)
			PRINTLN("[RCC MISSION] DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_CCTV_CAMERA - CCTV ignores mechanic disguise")			
			IF IS_LOCAL_PLAYER_WEARING_A_MAINTENANCE_OUTFIT()
				PRINTLN("[RCC MISSION] DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_CCTV_CAMERA - Player in mechanic disguise")			
				RETURN TRUE
			ENDIF
		ENDIF
		
		//  CASINO_HEIST_OUTFIT_IN__GRUPPE_SECHS
		
		IF IS_BIT_SET(sCCTVData.iCCTVBS, ciCCTV_BS_IgnoreDisguise_GruppeSechs)
			PRINTLN("[RCC MISSION] DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_CCTV_CAMERA - CCTV ignores Gruppe Sechs disguise")			
			IF IS_LOCAL_PLAYER_WEARING_A_GRUPPE_SECHS_OUTFIT()
				PRINTLN("[RCC MISSION] DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_CCTV_CAMERA - Player in Gruppe Sechs disguise")			
				RETURN TRUE
			ENDIF
		ENDIF
		
		//CASINO_HEIST_OUTFIT_IN__BRUCIE
		IF IS_BIT_SET(sCCTVData.iCCTVBS, ciCCTV_BS_IgnoreDisguise_Brucie)
			PRINTLN("[RCC MISSION] DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_CCTV_CAMERA - CCTV ignores Brucie disguise")		
			IF IS_LOCAL_PLAYER_WEARING_A_CELEB_OUTFIT()
				PRINTLN("[RCC MISSION] DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_CCTV_CAMERA - Player wearing Brucie disguise")		
				RETURN TRUE
			ENDIF
		ENDIF
		
		// CASINO_HEIST_OUTFIT_OUT__FIREMAN	
		IF IS_BIT_SET(sCCTVData.iCCTVBS, ciCCTV_BS_IgnoreDisguise_Fireman)
			PRINTLN("[RCC MISSION] DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_CCTV_CAMERA - CCTV ignores Fireman disguise")		
			IF IS_LOCAL_PLAYER_WEARING_A_FIREMAN_OUTFIT()
				PRINTLN("[RCC MISSION] DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_CCTV_CAMERA - Player wearing Fireman disguise")		
				RETURN TRUE
			ENDIF
		ENDIF
		
		// CASINO_HEIST_OUTFIT_OUT__HIGH_ROLLER
		IF  IS_BIT_SET(sCCTVData.iCCTVBS, ciCCTV_BS_IgnoreDisguise_HighRoller)
			PRINTLN("[RCC MISSION] DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_CCTV_CAMERA - CCTV ignores High Roller disguise")		
			IF IS_LOCAL_PLAYER_WEARING_A_HIGH_ROLLER_OUTFIT()
				PRINTLN("[RCC MISSION] DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_CCTV_CAMERA - Player wearing High Roller disguise")		
				RETURN TRUE
			ENDIF
		ENDIF
		
		//CASINO_HEIST_OUTFIT_OUT__SWAT
		IF IS_BIT_SET(sCCTVData.iCCTVBS, ciCCTV_BS_IgnoreDisguise_Noose)
			PRINTLN("[RCC MISSION] DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_CCTV_CAMERA - CCTV ignores Noose disguise")		
			IF IS_LOCAL_PLAYER_WEARING_A_NOOSE_OUTFIT()
				PRINTLN("[RCC MISSION] DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_CCTV_CAMERA - Player wearing Noose disguise")		
				RETURN TRUE
			ENDIF
		ENDIF
	#ENDIF
	RETURN FALSE
ENDFUNC


//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: OBJECT BLIPPING !
//
//************************************************************************************************************************************************************



// *** ADDED BY NEIL *** see 1707129
// Every time a new blip is added and set to flash, resync all the currently flashing blips to ensure they flash in unison. 
PROC RESYNC_FLASHING_OBJECT_BLIPS()
	INT i
	REPEAT FMMC_MAX_NUM_OBJECTS i
		IF DOES_BLIP_EXIST(biObjBlip[i])
			IF IS_BLIP_FLASHING(biObjBlip[i])
				SET_BLIP_FLASHES(biObjBlip[i], TRUE)
			ENDIF
		ENDIF
	ENDREPEAT
	NET_PRINT("RESYNC_FLASHING_OBJECT_BLIPS() called. ") NET_NL()
ENDPROC
PROC SET_OBJECT_BLIP_SCALE(INT iObjIndex, FLOAT fDefaultScale, BOOL bIgnoreCustom = FALSE)
	IF NOT bIgnoreCustom
	AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjIndex].sObjBlipStruct.fBlipScale != FMMC_BLIP_SCALE_OBJECT
		fDefaultScale = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjIndex].sObjBlipStruct.fBlipScale
	ENDIF
	IF DOES_BLIP_EXIST(biObjBlip[iObjIndex])
		SET_BLIP_SCALE(biObjBlip[iObjIndex], fDefaultScale)
	ENDIF
ENDPROC
PROC SET_OBJECT_BOUNDS_BLIP_SCALE(INT iObjIndex, FLOAT fDefaultScale, BOOL bSecondary = FALSE)
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjIndex].sObjBlipStruct.fBlipScale != FMMC_BLIP_SCALE_OBJECT
		fDefaultScale = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjIndex].sObjBlipStruct.fBlipScale
	ENDIF

	IF NOT bSecondary
		SET_BLIP_SCALE(bounds_blip, fDefaultScale)
	ELSE
		SET_BLIP_SCALE(sec_bounds_blip, fDefaultScale)
	ENDIF
ENDPROC
PROC SET_OBJECT_BLIP_COLOUR(INT iObjIndex, INT iDefaultColour)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjIndex].sObjBlipStruct.iBlipColour != BLIP_COLOUR_DEFAULT
		iDefaultColour = GET_BLIP_COLOUR_FROM_CREATOR( g_FMMC_STRUCT_ENTITIES.sPlacedObject[ iObjIndex ].sObjBlipStruct.iBlipColour )
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjIndex].sObjElectronicData.sCCTVData.iCCTVBS, ciCCTV_BS_OverrideBlipColour)
		IF DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_CCTV_CAMERA(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjIndex].sObjElectronicData.sCCTVData)
			iDefaultColour = BLIP_COLOUR_WHITE
		ELSE
			iDefaultColour = BLIP_COLOUR_RED
		ENDIF
	ENDIF
	
	IF DOES_BLIP_EXIST(biObjBlip[iObjIndex])
		SET_BLIP_COLOUR(biObjBlip[iObjIndex], iDefaultColour)
	ENDIF
	
ENDPROC

PROC SET_OBJECT_BOUNDS_BLIP_COLOUR( INT iObjIndex, INT iDefaultColour, BOOL bSecondary = FALSE )
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[ iObjIndex ].sObjBlipStruct.iBlipColour != BLIP_COLOUR_DEFAULT
		iDefaultColour = GET_BLIP_COLOUR_FROM_CREATOR( g_FMMC_STRUCT_ENTITIES.sPlacedObject[ iObjIndex ].sObjBlipStruct.iBlipColour )
	ENDIF
	
	IF NOT bSecondary
		SET_BLIP_COLOUR( bounds_blip, iDefaultColour )
	ELSE
		SET_BLIP_COLOUR( sec_bounds_blip, iDefaultColour )
	ENDIF
	
ENDPROC

FUNC BOOL SHOULD_OBJECT_BLIP_HAVE_CUSTOM_SPRITE(INT iObj)
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetTwo, cibsOBJ2_TransformJuggernaut)
	OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetTwo, cibsOBJ2_TransformBeast)
	OR g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].sObjBlipStruct.iBlipSpriteOverride != 0

		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SET_CUSTOM_OBJECT_BLIP_SPRITE(BLIP_INDEX biBlip, INT iObj)
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetTwo, cibsOBJ2_TransformJuggernaut)
		SET_BLIP_SPRITE(biBlip, RADAR_TRACE_JUGG)
	ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetTwo, cibsOBJ2_TransformBeast)
		SET_BLIP_SPRITE(biBlip, RADAR_TRACE_BEAST)
	ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].sObjBlipStruct.iBlipSpriteOverride != 0
		SET_BLIP_SPRITE(biBlip, GET_BLIP_SPRITE_FROM_BLIP_SPRITE_OVERRIDE(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].sObjBlipStruct.iBlipSpriteOverride))
		SET_BLIP_OVERRIDE_SPRITE_USE_CUSTOM_NAME(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].sObjBlipStruct.iBlipSpriteOverride, biBlip)
	ENDIF
ENDPROC

PROC CREATE_OBJECT_BLIP_WITH_PARAMS( ENTITY_INDEX entToBlip, INT iObjIndex, INT iColour, FLOAT fScale )
	
	INT iThisTeam = MC_playerBD[iPartToUse].iteam
	
	IF NOT DOES_BLIP_EXIST( biObjBlip[iObjIndex])
		PRINTLN("[RCC MISSION] CREATE_OBJECT_BLIP_WITH_PARAMS - Creating blip for object ", iObjIndex)
		biObjBlip[iObjIndex] = ADD_BLIP_FOR_ENTITY(entToblip)
	ENDIF
	
	IF SHOULD_OBJECT_BLIP_HAVE_CUSTOM_SPRITE(iObjIndex)
		SET_CUSTOM_OBJECT_BLIP_SPRITE(biObjBlip[iObjIndex], iObjIndex)
	ENDIF
	
	SET_OBJECT_BLIP_COLOUR(iObjIndex, iColour)
	SET_OBJECT_BLIP_SCALE(iObjIndex, fScale)
	SET_BLIP_PRIORITY(biObjBlip[iObjIndex], BLIPPRIORITY_HIGHEST)
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjIndex].sObjBlipStruct.iBlipPriority != 0
		SET_BLIP_PRIORITY(biObjBlip[iObjIndex], INT_TO_ENUM(BLIP_PRIORITY, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjIndex].sObjBlipStruct.iBlipPriority))
		PRINTLN("CREATE_OBJECT_BLIP_WITH_PARAMS - Blip priority set to ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjIndex].sObjBlipStruct.iBlipPriority)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjIndex].iObjectBitSetTwo, cibsOBJ2_BlipHighestPriority)
		SET_BLIP_PRIORITY(biObjBlip[iObjIndex], BLIP_PRIORITY_HIGHEST_SPECIAL_HIGH)
		PRINTLN("CREATE_OBJECT_BLIP_WITH_PARAMS - Blip priority set to BLIP_PRIORITY_HIGHEST_SPECIAL_HIGH due to cibsOBJ2_BlipHighestPriority")
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjIndex].iObjectBitSetFour, cibsOBJ4_UseDefaultBlipName)
		SET_BLIP_CUSTOM_NAME(biObjBlip[iObjIndex], iThisTeam, MC_serverBD_4.iObjPriority[iObjIndex][iThisTeam], iObjBlipCachedNamePriority[iObjIndex])
	ENDIF
	
	SHOW_HEIGHT_ON_BLIP(biObjBlip[iObjIndex], IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjIndex].iObjectBitSet, cibsOBJ_ShowHeightBlipIndicator))
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjIndex].iObjectBitSet, cibsOBJ_LowBlipHeightThres)
		//2696359
		SET_BLIP_SHORT_HEIGHT_THRESHOLD(biObjBlip[iObjIndex], TRUE)
	ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjIndex].iObjectBitSetThree, cibsOBJ3_UseExtendedHeightThreshold)
		SET_BLIP_EXTENDED_HEIGHT_THRESHOLD(biObjBlip[iObjIndex], TRUE)
	ENDIF
	
	IF CONTENT_IS_USING_ARENA()
		SET_BLIP_DISPLAY(biObjBlip[iObjIndex], DISPLAY_RADAR_ONLY)
		PRINTLN("[ARENA] Settinb blip for obj ", iObjIndex, " to display on radar ONLY")
	ENDIF
	
ENDPROC

PROC CREATE_OBJECT_BOUNDS_BLIP_WITH_PARAMS( ENTITY_INDEX entToBlip, INT iObjIndex, INT iColour, BOOL bSecondary = FALSE, FLOAT fScale = FMMC_BLIP_SCALE_OBJECT )
	
	IF DOES_BLIP_EXIST(biObjBlip[iObjIndex])
		EXIT
	ENDIF
	
	INT iThisTeam = MC_playerBD[ iPartToUse ].iteam
	
	IF NOT bSecondary
		bounds_blip = ADD_BLIP_FOR_ENTITY( entToblip )
	ELSE
		sec_bounds_blip = ADD_BLIP_FOR_ENTITY( entToBlip )
	ENDIF
	
	SET_OBJECT_BOUNDS_BLIP_COLOUR( iObjIndex, iColour, bSecondary )
	SET_OBJECT_BOUNDS_BLIP_SCALE(iObjIndex, fScale, bSecondary)
	
	IF NOT bSecondary
		SET_BLIP_PRIORITY( bounds_blip, BLIPPRIORITY_HIGHEST )
		IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjIndex].iObjectBitSetFour, cibsOBJ4_UseDefaultBlipName)
			SET_BLIP_CUSTOM_NAME( bounds_blip, iThisTeam, MC_serverBD_4.iObjPriority[ iObjIndex ][ iThisTeam ], iObjBlipCachedNamePriority[ iObjIndex ] )
		ENDIF
		SHOW_HEIGHT_ON_BLIP( bounds_blip, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[ iObjIndex ].iObjectBitSet, cibsOBJ_ShowHeightBlipIndicator) )	
	ELSE
		SET_BLIP_PRIORITY( sec_bounds_blip, BLIPPRIORITY_HIGHEST )
		IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObjIndex].iObjectBitSetFour, cibsOBJ4_UseDefaultBlipName)
			SET_BLIP_CUSTOM_NAME( sec_bounds_blip, iThisTeam, MC_serverBD_4.iObjPriority[ iObjIndex ][ iThisTeam ], iObjBlipCachedNamePriority[ iObjIndex ] )
		ENDIF
		SHOW_HEIGHT_ON_BLIP( sec_bounds_blip, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[ iObjIndex ].iObjectBitSet, cibsOBJ_ShowHeightBlipIndicator) )	
	ENDIF
	
ENDPROC

PROC SET_PACKAGE_BLIP_TEAM_SECONDARY_COLOUR( BLIP_INDEX tempBlip, BOOL bSet, INT iteam )
	
	HUD_COLOURS teamcolour
	INT R,G,B,A
	 
	IF bSet
		teamcolour = GET_HUD_COLOUR_FOR_FMMC_TEAM(iteam, PlayerToUse)
		
		GET_HUD_COLOUR( teamcolour, R, G, B, A )
		SET_SECONDARY_BLIP_COLOUR( tempBlip, R, G, B )
	ENDIF
	
	SHOW_OUTLINE_INDICATOR_ON_BLIP( tempBlip, bSet )

ENDPROC

///PURPOSE: This function will make a CTF blip based on settings [to be elaborated]
PROC CREATE_CTF_BLIP_WITH_PARAMS( ENTITY_INDEX entToblip, INT iObjIndex, BOOL bSetBlipTeamSecondaryColour, INT iTeamHoldingPackage )
	
	CREATE_OBJECT_BLIP_WITH_PARAMS( entToblip, iObjIndex, BLIP_COLOUR_GREEN, FMMC_BLIP_SCALE_OBJECT)

	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[ iObjIndex ].sObjBlipStruct.iBlipStyle = FMMC_OBJECTBLIP_CTF
		IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[ iObjIndex ].mn = ind_prop_dlc_flag_02 // OWAIN
			SET_BLIP_SPRITE( biObjBlip[ iObjIndex ], RADAR_TRACE_CAPTURE_THE_USAFLAG ) // This will be the new flag radar
		ELSE
			SET_BLIP_SPRITE( biObjBlip[ iObjIndex ], RADAR_TRACE_CAPTURE_THE_FLAG )
		ENDIF
		
		IF SHOULD_OBJECT_BLIP_HAVE_CUSTOM_SPRITE(iObjIndex)
			SET_CUSTOM_OBJECT_BLIP_SPRITE(biObjBlip[iObjIndex], iObjIndex)
		ENDIF
		
		//Change the blip to a custom name if one exists for this blip
		INT iteam = MC_playerBD[ iPartToUse ].iteam
		INT ipriority = MC_serverBD_4.iCurrentHighestPriority[iteam]
		
		IF ipriority < FMMC_MAX_RULES //was array overrunning
			IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sFMMCEndConditions[iteam].tl23ObjBlip[ipriority])
				TEXT_LABEL_23 tl23LiteralStringName = g_FMMC_STRUCT.sFMMCEndConditions[iteam].tl23ObjBlip[ipriority]
				
				CDEBUG2LN(DEBUG_MC_BLIP, "[LH][CAPTUREBLIPS] This blip has a custom name. Setting cutstom blip name to ", tl23LiteralStringName, ".")
				
				BEGIN_TEXT_COMMAND_SET_BLIP_NAME("MCUSTBLIP")
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(tl23LiteralStringName)
				END_TEXT_COMMAND_SET_BLIP_NAME(biObjBlip[ iObjIndex ])
			ENDIF
			
			SET_OBJECT_BLIP_COLOUR( iObjIndex, BLIP_COLOUR_GREEN )
			SET_OBJECT_BLIP_SCALE(iObjIndex, FMMC_BLIP_SCALE_OBJECT, IS_THIS_A_CAPTURE())
		ENDIF
	ENDIF
	
	IF bSetBlipTeamSecondaryColour
		SET_PACKAGE_BLIP_TEAM_SECONDARY_COLOUR( biObjBlip[ iObjIndex ], TRUE, iTeamHoldingPackage )
	ENDIF

	SHOW_HEIGHT_ON_BLIP( biObjBlip[ iObjIndex ], TRUE )	
ENDPROC

PROC REMOVE_OBJECT_BLIP(INT iObj)
	IF DOES_BLIP_EXIST(biObjBlip[iObj])
		REMOVE_CCTV_VISION_CONE(biObjBlip[iobj], iObj, FMMC_ELECTRONIC_ENTITY_TYPE__OBJECT)
		PRINTLN("[RCC MISSION] Removing blip for object ", iObj)
		REMOVE_BLIP(biObjBlip[iobj])
	ENDIF
ENDPROC

FUNC BOOL IS_SUDDEN_DEATH_OBJECT_DURING_SUDDEN_DEATH(INT iObj)
	
	IF IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH)
	AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetTwo, cibsOBJ2_HideObjectDuringSuddenDeath)	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_OBJECT_BE_BLIPPED(INT iobj)

	INT iTeam = MC_playerBD[ iPartToUse ].iteam
	INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam] 
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectBitSetTwo, cibsOBJ2_UnblipAfterMinigame)
		IF IS_BIT_SET(MC_serverBD_4.iObjectDoNotBlipBS, iObj)
		OR IS_BIT_SET(MC_serverBD_1.iOffRuleMinigameCompleted, iObj)
			PRINTLN("SHOULD_OBJECT_BE_BLIPPED - Unblip after minigame iObj: ", iobj)
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectBitSetTwo, cibsOBJ2_HideBlipInOrbitalCannon)
		IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PlayerToUse)].iMissionModeBit, ciMISSION_USING_ORBITAL_CANNON)
			PRINTLN("SHOULD_OBJECT_BE_BLIPPED - Using orbital cannon iObj: ", iobj)
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF SHOULD_OBJECT_MINIGAME_BE_DISABLED_BY_POISON_GAS(iObj)
		PRINTLN("SHOULD_OBJECT_BE_BLIPPED - Minigame is disabled by poison gas iObj: ", iobj)
		RETURN FALSE
	ENDIF
	
	OBJECT_INDEX tempObj
	IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niObject[iobj])
		tempObj = NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[iobj])
	ENDIF
	
	IF iRule < FMMC_MAX_RULES
		IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].sObjBlipStruct.fBlipRange > 0
		AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSix[iRule], ciBS_RULE6_OVERRIDE_BLIP_RANGE)
			IF DOES_ENTITY_EXIST(tempObj)
				FLOAT fPlayerObjDist = VDIST(GET_ENTITY_COORDS(LocalPlayerPed), GET_ENTITY_COORDS(tempObj))
				IF fPlayerObjDist > g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].sObjBlipStruct.fBlipRange
					PRINTLN("SHOULD_OBJECT_BE_BLIPPED - Blip range (2) iObj: ", iobj)
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].sObjBlipStruct.fBlipHeightDifference > 0.0
		IF DOES_ENTITY_EXIST(tempObj)
			IF NOT IS_PED_INJURED(LocalPlayerPed)
				VECTOR vPlayerPos = GET_ENTITY_COORDS(LocalPlayerPed)
				VECTOR vObjPos = GET_ENTITY_COORDS(tempObj)
				
				IF NOT IS_VECTOR_ZERO(vPlayerPos)
				AND NOT IS_VECTOR_ZERO(vObjPos)
					FLOAT fZDifference = ABSF(vPlayerPos.z - vObjPos.z)
					IF fZDifference > g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].sObjBlipStruct.fBlipHeightDifference
						PRINTLN("SHOULD_OBJECT_BE_BLIPPED - Blip height difference iObj: ", iobj)
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF iTeam > -1
	AND (g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].sObjBlipStruct.sTeamBlip[iTeam].iBlipOverrideStartRule != -1
	OR g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].sObjBlipStruct.sTeamBlip[iTeam].iBlipOverrideEndRule != -1)
		INT iBlipFrom = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].sObjBlipStruct.sTeamBlip[iTeam].iBlipOverrideStartRule
		INT iBlipTo = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].sObjBlipStruct.sTeamBlip[iTeam].iBlipOverrideEndRule
		IF iRule >= iBlipFrom
		AND (iRule <= iBlipTo OR iBlipTo = -1)
			RETURN TRUE
		ELSE
			PRINTLN("SHOULD_OBJECT_BE_BLIPPED - Not in blip override range iObj: ", iobj)
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].sObjBlipStruct.fBlipRange = -1
	AND iTeam > -1 AND iTeam < FMMC_MAX_TEAMS
	AND iRule < FMMC_MAX_RULES
	AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetSix[iRule], ciBS_RULE6_OVERRIDE_BLIP_RANGE)
		PRINTLN("SHOULD_OBJECT_BE_BLIPPED - Not in range to blip iObj: ", iobj)
		RETURN FALSE
	ENDIF
	
	IF IS_SUDDEN_DEATH_OBJECT_DURING_SUDDEN_DEATH(iObj)
		PRINTLN("SHOULD_OBJECT_BE_BLIPPED - Hiding during sudden death iObj: ", iobj)
		RETURN FALSE
	ENDIF
		
	//Remove and package blips when we go into this sudden death mode
	IF IS_BIT_SET( MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH )
	AND IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_KILLALLENEMIES_PENNED_IN)
		PRINTLN("SHOULD_OBJECT_BE_BLIPPED - IN SUDDEN DEATH, SHOULD NOT BLIP PACKAGE iObj: ", iobj)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE

ENDFUNC

//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: PROCESS OBJECT BODY !
//
//************************************************************************************************************************************************************

PROC PTL_SET_PACKAGE_KILLER(INT iVictim, INT iKiller)
	INT i
	FOR i = 0 TO MC_serverBD.iNumObjCreated - 1
		IF i < FMMC_MAX_NUM_OBJECTS
		AND NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niObject[i])
			IF MC_serverBD.iObjCarrier[i] = iVictim
			AND iVictim != iKiller
				MC_serverBD_1.iPTLDroppedPackageOwner[i] = iKiller
				PRINTLN("[JT POINTLESS] *****************************")
				PRINTLN("[JT POINTLESS] Player that can pick up straight away (Killer): ", i, " is ", MC_serverBD_1.iPTLDroppedPackageOwner[i])
				PRINTLN("[JT POINTLESS] iVictim: ", iVictim, " iKiller: ", iKiller, " LocalPart: ", iLocalPart)
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

FUNC BOOL PTL_ON_SAME_TEAM_AS_KILLER(INT iPickup)
	IF MC_serverBD_1.iPTLDroppedPackageOwner[iPickup] > -1
		IF MC_playerBD[MC_serverBD_1.iPTLDroppedPackageOwner[iPickup]].iTeam = MC_playerBD[iLocalPart].iTeam
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL CAN_PLAYER_GET_PICKUP_POINTLESS(INT iPickUp)
		
	//Starting the timer
		IF MC_serverBD.iObjCarrier[iPickUp] = -1
			IF NOT HAS_NET_TIMER_STARTED(stPickupTimer[iPickUp])
				REINIT_NET_TIMER(stPickupTimer[iPickUp])
				PRINTLN("[JT POINTLESS] Timer started for pickup ", iPickUp, ". Length: ", (g_FMMC_STRUCT.iPTLPickupTimeLimit * 1000))
			ENDIF
		ENDIF
		
		//Checking timer
		IF MC_serverBD_1.iPTLDroppedPackageOwner[iPickUp] != -1 OR MC_serverBD.iObjCarrier[iPickUp] = -1
			IF MC_serverBD_1.iPTLDroppedPackageOwner[iPickUp] = PARTICIPANT_ID_TO_INT()
			OR PTL_ON_SAME_TEAM_AS_KILLER(iPickUp)
				CLEAR_BIT(iPTLLocalCollisionBit, ciARROW_BS_0 + iPickUp)		
				PRINTLN("[JT CHECK POINTLESS] Package dropped: ", iPickUp," owner is this player. MC_serverBD_1.iPTLDroppedPackageOwner: ", MC_serverBD_1.iPTLDroppedPackageOwner[iPickUp], " Part int: ", PARTICIPANT_ID_TO_INT())
				RETURN TRUE
			ELIF (MC_serverBD_1.iPTLDroppedPackageLastTeam[iPickUp] != MC_playerBD[iLocalPart].iTeam AND MC_serverBD_1.iPTLDroppedPackageOwner[iPickUp] = -1)
				PRINTLN("[JT CHECK POINTLESS] Package dropped: ", iPickUp," owner is this player. LastTeam: ", MC_serverBD_1.iPTLDroppedPackageLastTeam[iPickUp] != MC_playerBD[iLocalPart].iTeam )
				CLEAR_BIT(iPTLLocalCollisionBit, ciARROW_BS_0 + iPickUp)
				RETURN TRUE
			ELIF HAS_NET_TIMER_STARTED(stPickupTimer[iPickUp])
				IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stPickupTimer[iPickUp]) >= (g_FMMC_STRUCT.iPTLPickupTimeLimit * 1000)
					PRINTLN("[JT CHECK POINTLESS] Timer finished package ", iPickUp)
					PRINTLN("[JT CHECK POINTLESS] Timer at: ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stPickupTimer[iPickUp]))
					CLEAR_BIT(iPTLLocalCollisionBit, ciARROW_BS_0 + iPickUp)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF

	RETURN FALSE
ENDFUNC

PROC PTL_SET_PICKUP_ARROW(OBJECT_INDEX oiPickup, INT iPickupIndex, BOOL bUseArrow)
	IF NOT bUseArrow AND NOT IS_BIT_SET(iPTLLocalCollisionBit, ciARROW_OFF_16 + iPickupIndex)
	OR bUseArrow AND NOT IS_BIT_SET(iPTLLocalCollisionBit, ciARROW_BS_0 + iPickupIndex)
		IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(oiPickup)
			NETWORK_REQUEST_CONTROL_OF_ENTITY(oiPickup)
		ELSE
			IF NOT bUseArrow
				SET_PICKUP_OBJECT_ARROW_MARKER(oiPickup, FALSE)
				PRINTLN("[JT ARROW POINTLESS] - removing arrow from pickup ", iPickupIndex)
				SET_BIT(iPTLLocalCollisionBit, ciARROW_OFF_16 + iPickupIndex)
			ELSE
				SET_PICKUP_OBJECT_ARROW_MARKER(oiPickup, TRUE)
				PRINTLN("[JT ARROW POINTLESS] - Adding arrow from pickup ", iPickupIndex)
				SET_BIT(iPTLLocalCollisionBit, ciARROW_BS_0 + iPickupIndex)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_POINTLESS_PORTABLE_PICKUP_BLIPS()
	INT i
	OBJECT_INDEX oiPickup[FMMC_MAX_NUM_OBJECTS]
	
	FOR i = 0 TO MC_serverBD.iNumObjCreated-1
		IF i < FMMC_MAX_NUM_OBJECTS 
		AND NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niObject[i])
		
			IF NOT (i = 0 AND IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_POINTLESS_IGNORE_PROTECT_OBJ))
				oiPickup[i] = NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[i])
				
				IF MC_serverBD.iObjCarrier[i] != -1
					IF HAS_NET_TIMER_STARTED(stPickupTimer[i])
						RESET_NET_TIMER(stPickupTimer[i])
						IF IS_BIT_SET(ilocalPLBit, ciARROW_BS_0 + i)
							CLEAR_BIT(ilocalPLBit, ciARROW_BS_0 + i)
						ENDIF
						
						IF IS_BIT_SET(iLocalPLBit, ciDROP_SOUND_0 + i)
							CLEAR_BIT(iLocalPLBit, ciDROP_SOUND_0 + i)
						ENDIF
						
						IF IS_BIT_SET(iPTLLocalCollisionBit, ciARROW_OFF_16 + i)
							CLEAR_BIT(iPTLLocalCollisionBit, ciARROW_OFF_16 + i)
						ENDIF
						IF NOT IS_LOCAL_PLAYER_SPECTATOR()
						AND NOT IS_PED_INJURED(localPlayerPed)
							PTL_SET_PICKUP_ARROW(oiPickup[i], i, FALSE)
						ENDIF
						PRINTLN("[JT POINTLESS] - Resetting Timer: ", i, " as package has been picked up")
					ENDIF
				ENDIF
				//If the object is attached and the blip exist, it has been picked up
				IF MC_serverBD.iObjCarrier[i] != -1
				AND DOES_BLIP_EXIST(biObjBlip[i])
				AND MC_serverBD_1.iPTLDroppedPackageOwner[i] = -1
					PRINTLN("PROCESS_POINTLESS_PORTABLE_PICKUP_BLIPS - Package ", i, " has been picked up! Removing blip.")
					REMOVE_BLIP(biObjBlip[i])	
					PREVENT_COLLECTION_OF_PORTABLE_PICKUP(oiPickup[i], TRUE, TRUE)
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_POINTLESS_SOUNDS)
						//Potentially redundant based on always triggering on a score up
						PLAY_POINTLESS_SOUND(ciPTLAudio_COLLECT,MC_serverBD.iObjCarrier[i],MC_serverBD_1.iPTLDroppedPackageLastTeam[i], oiPickup[i])
					ENDIF
					PRINTLN("[JT POINTLESS] Resetting package ", i, " New carrier: ", MC_serverBD.iObjCarrier[i])
				//Else if the object is not attached and the blip does exist, it has been dropped
				ELIF MC_serverBD.iObjCarrier[i] = -1
					
					IF NOT IS_BIT_SET(iLocalPLBit, ciDROP_SOUND_0 + i)
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciENABLE_POINTLESS_SOUNDS)
						AND GET_TIME_REMAINING_ON_MULTIRULE_TIMER() > 0
							//Potentionally redundant based on always triggering on a score down.
							PLAY_POINTLESS_SOUND(ciPTLAudio_DROP,MC_serverBD_1.iPTLDroppedPackageLastOwner[i], MC_serverBD_1.iPTLDroppedPackageLastTeam[i])
							SET_BIT(iLocalPLBit, ciDROP_SOUND_0 + i)
						ENDIF
					ENDIF
					
					IF CAN_PLAYER_GET_PICKUP_POINTLESS(i)
						IF NOT IS_LOCAL_PLAYER_SPECTATOR()
						AND NOT IS_PED_INJURED(localPlayerPed)
							PTL_SET_PICKUP_ARROW(oiPickup[i], i, TRUE)
						ENDIF
						IF NOT DOES_BLIP_EXIST(biObjBlip[i])
							
							IF GET_ENTITY_ALPHA(oiPickup[i]) != 255
								RESET_PICKUP_ENTITY_GLOW(oiPickup[i])
							ENDIF		
							
							PRINTLN("PROCESS_POINTLESS_PORTABLE_PICKUP_BLIPS - Package ", i, " has been dropped! Adding blip.")
							biObjBlip[i] = ADD_BLIP_FOR_ENTITY(NET_TO_ENT(MC_serverBD_1.sFMMC_SBD.niObject[i]))
							IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEleven, ciPOINTLESS_ENABLE_CAPTURE_BLIPS)
								SET_BLIP_SPRITE(biObjBlip[i], RADAR_TRACE_CAPTURE_THE_FLAG)
								SET_BLIP_NAME_FROM_TEXT_FILE(biObjBlip[i], "POINTLESS_BLIP")
								PRINTLN("PROCESS_POINTLESS_PORTABLE_PICKUP_BLIPS - RADAR_TRACE_CAPTURE_THE_FLAG")
							ENDIF
							
							SET_BLIP_COLOUR(biObjBlip[i], BLIP_COLOUR_GREEN)
							SET_BLIP_SCALE(biObjBlip[i], FMMC_BLIP_SCALE_OBJECT)
							SET_BLIP_PRIORITY(biObjBlip[i], BLIPPRIORITY_HIGHEST)
							SHOW_HEIGHT_ON_BLIP(biObjBlip[i], TRUE)
							
														
							PRINTLN("[JT POINTLESS] - DROP INFO")
							PRINTLN("[JT POINTLESS] - Package: ", i)
							PRINTLN("[JT POINTLESS] - iObjCarrier: ", MC_serverBD.iObjCarrier[i])
							PRINTLN("[JT POINTLESS] - iPTLDroppedPackageOwner (Killer): ", MC_serverBD_1.iPTLDroppedPackageOwner[i])
							PRINTLN("[JT POINTLESS] - iPTLDroppedPackageLastOwner (Victim): ", MC_serverBD_1.iPTLDroppedPackageLastOwner[i])
							PRINTLN("[JT POINTLESS] - iPTLDroppedPackageLastTeam: ", MC_serverBD_1.iPTLDroppedPackageLastTeam[i])
							PRINTLN("[JT POINTLESS] - DROP INFO END")
							PRINTLN("[JT POINTLESS] Pickup ", i," marked")
							PRINTLN("[JT POINTLESS] *****************************")
						ENDIF
						//Moved out of check as wasn't hitting sometimes
						PREVENT_COLLECTION_OF_PORTABLE_PICKUP(oiPickup[i], FALSE, TRUE)
						
					ELSE
						IF NOT IS_LOCAL_PLAYER_SPECTATOR()
						AND NOT IS_PED_INJURED(localPlayerPed)
							PTL_SET_PICKUP_ARROW(oiPickup[i], i, FALSE)
						ENDIF
						SET_ENTITY_ALPHA(oiPickup[i],150,TRUE)
						PREVENT_COLLECTION_OF_PORTABLE_PICKUP(oiPickup[i], TRUE, TRUE)
						
						
						
//						IF NOT IS_LOCAL_PLAYER_SPECTATOR()
//						AND NOT IS_PED_INJURED(localPlayerPed)
	//						IF MC_serverBD_1.iPTLDroppedPackageOwner[i] > -1
	//							IF MC_playerBD[MC_serverBD_1.iPTLDroppedPackageOwner[i]].iTeam != MC_playerBD[iLocalPart].iTeam
	//							AND NOT IS_BIT_SET(iPTLLocalCollisionBit, ciPTLCOL_BS_0 +i)
	//								PRINTLN("[JT COLLISION POINTLESS] Killer Team: ", MC_playerBD[MC_serverBD_1.iPTLDroppedPackageOwner[i]].iTeam)
	//								PRINTLN("[JT COLLISION POINTLESS] My Team: ", MC_playerBD[iLocalPart].iTeam)
	//								IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(oiPickup[i])
	//									NETWORK_REQUEST_CONTROL_OF_ENTITY(oiPickup[i])
	//								ELSE
	//									SET_ENTITY_COLLISION(oiPickup[i],FALSE)
	//									PRINTLN("[JT COLLISION POINTLESS] BAG ",i," DROPPED - SETTING COLLISION TO FALSE")
	//									SET_BIT(iPTLLocalCollisionBit, ciPTLCOL_BS_0 +i)
	//								ENDIF
	//							ENDIF
	//						ENDIF
//						ENDIF
					ENDIF
				ENDIF
				
				PRINTLN("[JT CONSTANT POINTLESS] Package: ", i," iObjCarrier: ", MC_serverBD.iObjCarrier[i], " MC_serverBD_1.iPTLDroppedPackageOwner: ", MC_serverBD_1.iPTLDroppedPackageOwner[i], " iPTLDroppedPackageLastOwner: ", MC_serverBD_1.iPTLDroppedPackageLastOwner[i]," iPTLDroppedPackageLastTeam: ", MC_serverBD_1.iPTLDroppedPackageLastTeam[i])
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

FUNC BOOL HAVE_ALL_STARTING_PACKAGES_BEEN_GRABBED()
	IF NOT IS_BIT_SET(iLocalBoolCheck19, LBOOL19_ALL_POINTLESS_PACKAGES_GRABBED)
		INT i, iPackageCount

		FOR i = 0 TO MC_serverBD.iNumObjCreated - 1
			IF MC_serverBD.iObjCarrier[i] != -1
				iPackageCount++
				PRINTLN("[JT GRAB POINTLESS] Package ", i," has been grabbed")
			ELSE
				PRINTLN("[JT GRAB POINTLESS] Package ", i, " is unclaimed")
			ENDIF
		ENDFOR
		
		IF iPackageCount >= GET_NUMBER_OF_ACTIVE_PARTICIPANTS()
			SET_BIT(iLocalBoolCheck19, LBOOL19_ALL_POINTLESS_PACKAGES_GRABBED)
			PRINTLN("[JT GRAB POINTLESS] HAVE_ALL_STARTING_PACKAGES_BEEN_GRABBED - returned true with count of: ", iPackageCount, "/", GET_NUMBER_OF_ACTIVE_PARTICIPANTS(), " Participants with bags")
			IF bIsLocalPlayerHost
				FOR i = 0 TO MC_serverBD.iNumObjCreated - 1
					IF MC_serverBD.iObjCarrier[i] = -1
						PRINTLN("[JT GRAB POINTLESS] - MOVING PACKAGE ", i, " TO MIDDLE OF BOUNDS")
						PTL_MOVE_STARTING_PACKAGE_TO_MIDDLE(NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[i]),i)
					ENDIF
				ENDFOR
			ENDIF
		ELSE
			PRINTLN("[JT GRAB POINTLESS] HAVE_ALL_STARTING_PACKAGES_BEEN_GRABBED - returning false with count of: ", iPackageCount, "/", GET_NUMBER_OF_ACTIVE_PARTICIPANTS(), " Participants with bags")
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_HACK_OBJ_INITIAL_BLIP_COLOUR()
	INT iCol = BLIP_COLOUR_GREEN
	
	IF IS_THIS_ROCKSTAR_MISSION_GANGOPS_MISSION_OF_TYPE(ciGANGOPS_FLOW_MISSION_RIOTVAN)
		PRINTLN("[Blips] Setting blip to be red initially due to this being the Riot Van mission and the objects will start on fire")
		RETURN BLIP_COLOUR_RED
	ENDIF
	
	RETURN iCol
ENDFUNC

PROC PROCESS_OBJECT_BLIPS(INT iObj)
	OBJECT_INDEX tempObj
	INT iTeam
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niObject[iobj])
		tempObj = NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[iobj])
		IF IS_ENTITY_DEAD(tempObj)
			REMOVE_OBJECT_BLIP(iobj)
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciENABLE_TAGGING_TARGETS)
				INT iTagged
				FOR iTagged = 0 TO FMMC_MAX_TAGGED_ENTITIES-1
					IF MC_serverBD_3.iTaggedEntityType[iTagged] = ENUM_TO_INT(GET_ENTITY_TYPE(tempObj))
						IF MC_serverBD_3.iTaggedEntityIndex[iTagged] = iObj
							IF DOES_BLIP_EXIST(biTaggedEntity[iTagged])
								REMOVE_BLIP(biTaggedEntity[iTagged])
							ENDIF
						ENDIF
					ENDIF
				ENDFOR
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciENABLE_TAGGING_TARGETS)
				REMOVE_TAGGED_BLIP(4, iobj)
			ENDIF
			EXIT
		ELSE
			IF DOES_BLIP_EXIST(biObjBlip[iobj] ) 
				IF NOT DOES_BLIP_EXIST(GET_BLIP_FROM_ENTITY(tempObj))
					REMOVE_BLIP(biObjBlip[iobj] )
					NET_PRINT(" removing Object blip - was on old Object: ") NET_PRINT_INT(iobj) NET_NL()
				ENDIF
			ENDIF
		ENDIF
		
		
	ELSE
		REMOVE_OBJECT_BLIP(iobj)
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciPOINTLESS_MODE_ENABLED)
		IF SHOULD_OBJECT_BE_BLIPPED(iobj)
			IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_OBJECTIVE_BLOCKER)
				IF iLocalCarrierPart[iobj] != MC_serverBD.iObjCarrier[iobj]
				OR iLocalPackageHolding[iobj] != GET_TEAM_HOLDING_PACKAGE(iobj)
					REMOVE_OBJECT_BLIP(iobj)
					iLocalPackageHolding[iobj] =GET_TEAM_HOLDING_PACKAGE(iobj)
					iLocalCarrierPart[iobj]= MC_serverBD.iObjCarrier[iobj] 
					PRINTLN("[JS][UOBJ - OBJECT] - Requested Objective Update - PROCESS_OBJECT_BLIPS - iLocalCarrierPart[", iobj, "] = ",iLocalCarrierPart[iobj])				
					SET_BIT(iLocalBoolCheck3,LBOOL3_UPDATE_OBJECT_OBJECTIVE)
				ENDIF
				
				IF MC_serverBD_4.iObjPriority[iobj][MC_playerBD[iPartToUse].iteam] <= MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]
				AND MC_serverBD_4.iObjPriority[iobj][MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
					IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].sObjBlipStruct.iBlipStyle != FMMC_OBJECTBLIP_NONE
						
						IF NOT DOES_BLIP_EXIST(biObjBlip[iobj])
							IF  MC_serverBD_4.iObjRule[iobj][MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_KILL
								CREATE_OBJECT_BLIP_WITH_PARAMS( tempObj, iObj, BLIP_COLOUR_RED, FMMC_BLIP_SCALE_OBJECT)
							ELIF MC_serverBD_4.iObjRule[iobj][MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_MINIGAME
								INT iObjBlipColour = GET_HACK_OBJ_INITIAL_BLIP_COLOUR()
								CREATE_OBJECT_BLIP_WITH_PARAMS( tempObj, iObj, iObjBlipColour, FMMC_BLIP_SCALE_OBJECT)
							ELSE
								IF MC_serverBD.iObjCarrier[iobj] > -1
									IF MC_serverBD.iObjCarrier[ iobj ] != iPartToUse
										CREATE_CTF_BLIP_WITH_PARAMS( tempObj, iObj, TRUE, MC_PlayerBD[ MC_serverBD.iObjCarrier[ iObj ] ].iteam )
										IF GET_ENTITY_MODEL(tempObj) != PROP_CS_DUFFEL_01
											SET_BLIP_FLASHES( biObjBlip[ iobj ], TRUE )
											RESYNC_FLASHING_OBJECT_BLIPS()
										ENDIF
									ENDIF
								ELSE
									IF GET_TEAM_HOLDING_PACKAGE( iobj ) > -1
										CREATE_CTF_BLIP_WITH_PARAMS( tempObj, iObj, TRUE, GET_TEAM_HOLDING_PACKAGE( iObj ) )
									ELSE
										CREATE_CTF_BLIP_WITH_PARAMS( tempObj, iObj, FALSE, -1 )
									ENDIF
								ENDIF
							ENDIF
						ELSE
							IF GET_TEAM_HOLDING_PACKAGE( iobj ) > -1
								IF NOT IS_TEAM_ACTIVE(GET_TEAM_HOLDING_PACKAGE(iobj))
									CLEAR_SECONDARY_COLOUR_FROM_BLIP(biObjBlip[iobj])
								ENDIF
							ENDIF
							
							IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
								IF MC_serverBD.iObjHackPart[iobj] = -1
									PRINTLN("[ObjectBlips] Setting obj ", iobj, " blip alpha back to 1")
									SET_BLIP_ALPHA(biObjBlip[iobj], 255)
								ELSE
									PRINTLN("[ObjectBlips] Lowering obj ", iobj, " blip alpha because it's currently being hacked")
									SET_BLIP_ALPHA(biObjBlip[iobj], 25)
									//SET_BLIP_COLOUR(biObjBlip[iobj], BLIP_COLOUR_INACTIVE_MISSION)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].sObjBlipStruct.iBlipStyle = FMMC_OBJECTBLIP_CTF
						IF NOT DOES_BLIP_EXIST(biObjBlip[iobj])
							IF MC_serverBD.iObjCarrier[iobj] > -1
								CREATE_CTF_BLIP_WITH_PARAMS( tempObj, iObj, TRUE, MC_PlayerBD[ MC_serverBD.iObjCarrier[ iObj ] ].iteam )
								IF GET_ENTITY_MODEL(tempObj) != PROP_CS_DUFFEL_01
								AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_ARENA_CTF(g_FMMC_STRUCT.iAdversaryModeType)
									SET_BLIP_FLASHES( biObjBlip[ iobj ], TRUE )
									RESYNC_FLASHING_OBJECT_BLIPS()
								ENDIF
							ELSE
								CREATE_CTF_BLIP_WITH_PARAMS( tempObj, iObj, FALSE, -1 )
							ENDIF
						ENDIF
					ELSE
						IF (g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].sObjBlipStruct.sTeamBlip[MC_playerBD[iPartToUse].iteam].iBlipOverrideStartRule != -1)
						OR (g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].sObjBlipStruct.sTeamBlip[MC_playerBD[iPartToUse].iteam].iBlipOverrideEndRule != -1)
							IF NOT DOES_BLIP_EXIST(biObjBlip[iobj])
								INT iBlipColour = BLIP_COLOUR_RED
								IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetTwo, cibsOBJ2_UseGreenRuleOverrideBlip_T0+MC_playerBD[iPartToUse].iteam)
									iBlipColour = BLIP_COLOUR_GREEN
								ENDIF
								
								FLOAT fBlipScale = FMMC_BLIP_SCALE_OBJECT
								IF IS_THIS_MODEL_A_CCTV_CAMERA(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].mn)
									fBlipScale = 0.6
								ENDIF
								
								CREATE_OBJECT_BLIP_WITH_PARAMS(tempObj, iObj, iBlipColour, fBlipScale)
								
								iTeam = MC_playerBD[iPartToUse].iteam
								INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam] 
								
								IF iRule < FMMC_MAX_RULES
									IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sFMMCEndConditions[iteam].tl23ObjBlip[iRule])
									AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetTwo, cibsOBJ2_BlipUseCustomHud)
										TEXT_LABEL_23 tl23LiteralStringName = g_FMMC_STRUCT.sFMMCEndConditions[iteam].tl23ObjBlip[iRule]
										
										BEGIN_TEXT_COMMAND_SET_BLIP_NAME("MCUSTBLIP")
											ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(tl23LiteralStringName)
										END_TEXT_COMMAND_SET_BLIP_NAME(biObjBlip[iObj])
										
									ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetTwo, cibsOBJ2_BlipUseCustomHud)
									AND NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].tl63ObjectiveEntityHUDMessage[MC_ServerBD_4.iCurrentHighestPriority[iTeam]])
										TEXT_LABEL_23 tl23LiteralStringName = g_FMMC_STRUCT.sFMMCEndConditions[iTeam].tl63ObjectiveEntityHUDMessage[MC_ServerBD_4.iCurrentHighestPriority[iTeam]]
										
										BEGIN_TEXT_COMMAND_SET_BLIP_NAME("MCUSTBLIP")
											ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(tl23LiteralStringName)
										END_TEXT_COMMAND_SET_BLIP_NAME(biObjBlip[iObj])
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF DOES_BLIP_EXIST(biObjBlip[iobj])
				IF MC_serverBD_4.iObjPriority[iobj][MC_playerBD[iPartToUse].iteam] > MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]
					IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].sObjBlipStruct.iBlipStyle = FMMC_OBJECTBLIP_STANDARD						
						IF (g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].sObjBlipStruct.sTeamBlip[MC_playerBD[iPartToUse].iteam].iBlipOverrideStartRule = -1)
						AND (g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].sObjBlipStruct.sTeamBlip[MC_playerBD[iPartToUse].iteam].iBlipOverrideEndRule = -1)
							REMOVE_OBJECT_BLIP(iobj)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF DOES_BLIP_EXIST(biObjBlip[iobj])	//Draw GPS route for object blip - BUG 2437661
				IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitset, cibsOBJ_GPSToLocation)
					IF IS_ENTITY_ATTACHED(tempObj)
						IF DOES_BLIP_HAVE_GPS_ROUTE(biObjBlip[iobj])
							SET_BLIP_ROUTE(biObjBlip[iobj], FALSE)
						ENDIF
					ELSE
						IF NOT DOES_BLIP_HAVE_GPS_ROUTE(biObjBlip[iobj])
							SET_BLIP_ROUTE(biObjBlip[iobj], TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			REMOVE_OBJECT_BLIP(iobj)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_ARENA_CTF_FLAGS(INT iObj, OBJECT_INDEX oiCTFFlag)
	
	IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_ARENA_CTF(g_FMMC_STRUCT.iAdversaryModeType)
		EXIT
	ENDIF
		
	IF NOT IS_BIT_SET(iLocalBoolCheck30, LBOOL30_RETURNING_A_FLAG)
	AND DOES_ENTITY_EXIST(oiCTFFlag)
	AND NETWORK_HAS_CONTROL_OF_ENTITY(oiCTFFlag)
		IF NOT IS_ENTITY_ATTACHED_TO_ANY_PED(oiCTFFlag)
			VECTOR vPos = GET_ENTITY_COORDS(oiCTFFlag)
			vPos.z += 3
			FLOAT fZ = 0
			IF GET_GROUND_Z_FOR_3D_COORD(vPos, fZ)
				FLOAT fOffset = 4.25 //Keeps the flag looking like it has been placed
				IF vPos.z != fZ+fOffset
				AND fZ + fOffset > 100
					PRINTLN("PROCESS_ARENA_CTF_FLAGS - vPos.z: ", vPos.z, " new z: ", fZ+fOffset)
					vPos.z = fZ + fOffset
					PRINTLN("PROCESS_ARENA_CTF_FLAGS - Grounding flag ", iObj, ". new position: ", vPos)
					SET_ENTITY_COORDS(oiCTFFlag, vPos)
				ENDIF
			ENDIF
								
			VECTOR vObjRot = GET_ENTITY_ROTATION(oiCTFFlag)
			IF NOT (vObjRot.x = 0 AND vObjRot.y = 0)
				vObjRot.x = 0
				vObjRot.y = 0
				SET_ENTITY_ROTATION(oiCTFFLAG, vObjRot)
			ENDIF
			
			IF g_bMissionEnding
			OR g_bMissionOver
			OR g_bCelebrationScreenIsActive
				SET_ENTITY_VISIBLE(oiCTFFlag, FALSE)
				HIDE_PORTABLE_PICKUP_WHEN_DETACHED(oiCTFFlag, TRUE)
			ENDIF
			
			IF DOES_BLIP_EXIST(biObjBlip[iobj])
				SHOW_HEIGHT_ON_BLIP(biObjBlip[iobj], TRUE)
			ENDIF
			
			BOOL bMoveBack
			IF GET_INTERIOR_FROM_ENTITY(oiCTFFlag) != g_ArenaInterior
				bMoveBack = TRUE
				PRINTLN("PROCESS_ARENA_CTF_FLAGS - Flag is not in the arena interior.")
			ENDIF
			IF vPos.z < 100
				bMoveBack = TRUE
				PRINTLN("PROCESS_ARENA_CTF_FLAGS - Flag is under the arena.")
			ENDIF
			
			IF bMoveBack
				SET_ENTITY_COORDS(oiCTFFlag, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].vPos)
				SET_ENTITY_ROTATION(oiCTFFlag, <<0.0,0.0,g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].fHead>>)
				PRINTLN("PROCESS_ARENA_CTF_FLAGS - Sending flag back to the arena.")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

// handles changes to the mission objects
PROC PROCESS_OBJ_BODY(INT iobj) 
	
	INT iteam
	OBJECT_INDEX tempObj
	BOOL bNotPickup
	FLOAT fObjDist
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niObject[iobj])
		
		tempObj = NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[iobj])
		
		IF IS_ENTITY_DEAD(tempObj)			
			IF MC_playerBD[iLocalPart].iObjHacking = iobj
				IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iHackFailBitset,MC_playerBD[iLocalPart].iObjHacking)
					SET_HACK_FAIL_BIT_SET(6)
					PRINTLN("[RCC MISSION][PROCESS_OBJ_BODY] 1 hacking/cracking object destroyed setting fail for obj: ", iobj)
				ENDIF
			ENDIF	
			EXIT
		ELSE
			IF NOT SHOULD_PLACED_OBJECT_BE_PORTABLE(iobj)
				SET_ENTITY_LOD_DIST(tempObj,500)
				bNotPickup = TRUE
			ENDIF
		ENDIF
		
		IF NOT IS_BIT_SET(g_iMissionObjectHasSpawnedBitset, iObj)
			SET_BIT(g_iMissionObjectHasSpawnedBitset, iObj)
			PRINTLN("[RCC MISSION][PROCESS_OBJ_BODY] OBJECT HAS BEEN SPAWNED. Setting g_iMissionObjectHasSpawnedBitset iObj: ",  iObj)
		ENDIF
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iLinkedDoor1 > -1
			IF NOT IS_BIT_SET(iDoorHasLinkedEntityBS, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iLinkedDoor1)
				SET_BIT(iDoorHasLinkedEntityBS, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iLinkedDoor1)
				PRINTLN("[MCDoors] PROCESS_OBJ_BODY - Setting iDoorHasLinkedEntityBS for door ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iLinkedDoor1, " due to object ", iObj, " having it as their primary door")
			ENDIF
		ENDIF
		IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iLinkedDoor2 > -1
			IF NOT IS_BIT_SET(iDoorHasLinkedEntityBS, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iLinkedDoor2)
				SET_BIT(iDoorHasLinkedEntityBS, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iLinkedDoor2)
				PRINTLN("[MCDoors] PROCESS_OBJ_BODY - Setting iDoorHasLinkedEntityBS for door ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iLinkedDoor2, " due to object ", iObj, " having it as their secondary door")
			ENDIF
		ENDIF
		
		PROCESS_ARENA_CTF_FLAGS(iObj, tempObj)
		
	ELSE // Else the network id for this object doesn't exist
		
		IF IS_BIT_SET(g_iMissionObjectHasSpawnedBitset, iObj)			
			CLEAR_BIT(g_iMissionObjectHasSpawnedBitset, iObj)
			PRINTLN("[RCC MISSION][PROCESS_OBJ_BODY] OBJECT NO LONGER EXISTS. Clearing g_iMissionObjectHasSpawnedBitset iObj: ",  iObj)
		ENDIF
		
		IF MC_playerBD[iLocalPart].iObjHacking = iobj
			IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iHackFailBitset,MC_playerBD[iLocalPart].iObjHacking)
				SET_HACK_FAIL_BIT_SET(5)
				PRINTLN("[RCC MISSION][PROCESS_OBJ_BODY] 2 hacking/cracking object destroyed setting fail for obj: ", iobj)
			ENDIF
		ENDIF
		EXIT
	ENDIF
	
//	IF iObj = iObjectJustHadStolen
//	AND iObjectJustHadStolen > -1
//		IF NOT HAS_NET_TIMER_STARTED(tdStolenVehSwapCooldownTimer)
//			PRINTLN("[RCC MISSION][PROCESS_OBJ_BODY][STOLEN] Starting time to re-allow picking up of obj: ", iobj)
//			START_NET_TIMER(tdStolenVehSwapCooldownTimer)
//		ENDIF		
//		IF HAS_NET_TIMER_EXPIRED_READ_ONLY(tdStolenVehSwapCooldownTimer, ci_STOLEN_VEH_SWAP_COOLDOWN_TIME)			
//			IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niObject[iobj])
//				PRINTLN("[RCC MISSION][PROCESS_OBJ_BODY][STOLEN] Allow picking up of obj: ", iobj)
//				tempObj = NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[iobj])
//				PREVENT_COLLECTION_OF_PORTABLE_PICKUP(tempObj, TRUE, FALSE)
//			ELSE
//				PRINTLN("[RCC MISSION][PROCESS_OBJ_BODY][STOLEN] Does not exist any more... obj: ", iobj)
//			ENDIF
//			
//			iObjectJustHadStolen = -1
//			RESET_NET_TIMER(tdStolenVehSwapCooldownTimer)
//		ENDIF
//	ENDIF

	IF NOT SHOULD_PLACED_OBJECT_BE_PORTABLE(iobj)
		SET_ENTITY_LOD_DIST(tempObj,500)
		bNotPickup = TRUE
	ENDIF
	
	fObjDist = GET_DISTANCE_BETWEEN_ENTITIES(LocalPlayerPed,tempObj)
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciPOINTLESS_MODE_ENABLED)
		IF SHOULD_OBJECT_BE_BLIPPED(iobj)
			IF NOT IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_OBJECTIVE_BLOCKER)
				IF MC_serverBD_4.iObjPriority[iobj][MC_playerBD[iPartToUse].iteam] <= MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]
				AND MC_serverBD_4.iObjPriority[iobj][MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
					IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].sObjBlipStruct.iBlipStyle != FMMC_OBJECTBLIP_NONE			
						IF  MC_serverBD_4.iObjRule[iobj][MC_playerBD[iPartToUse].iteam] != FMMC_OBJECTIVE_LOGIC_NONE
							IF MC_serverBD.iObjCarrier[iobj] != iPartToUse
							AND NOT IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet,PBBOOL_WITH_CARRIER)
							AND NOT (IS_BIT_SET(MC_serverBD.iObjAtYourHolding[MC_playerBD[iPartToUse].iteam],iobj) AND NOT IS_THIS_BOMB_FOOTBALL())
								IF fObjDist < fNearestTargetDistTemp
									iNearestTargetTemp = iobj
									iNearestTargetTypeTemp = ci_TARGET_OBJECT
									fNearestTargetDistTemp = fObjDist
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].sObjBlipStruct.iBlipStyle = FMMC_OBJECTBLIP_CTF
		IF iCantCollectHelpDisp < 4
			IF fObjDist < 3.0 AND fObjDist != 0.0
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				AND DOES_OBJECT_HAVE_ANY_PRIORITY_FOR_ANY_TEAM(iobj)
				AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_ARENA_CTF(g_FMMC_STRUCT.iAdversaryModeType)
					IF (MC_serverBD_4.iObjRule[iobj][MC_playerBD[iPartToUse].iteam] != FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER
					AND MC_serverBD_4.iObjRule[iobj][MC_playerBD[iPartToUse].iteam] != FMMC_OBJECTIVE_LOGIC_GET_AND_HOLD
					AND MC_serverBD_4.iObjRule[iobj][MC_playerBD[iPartToUse].iteam] != FMMC_OBJECTIVE_LOGIC_CAPTURE)
					OR (MC_serverBD_4.iObjRule[iobj][MC_playerBD[iPartToUse].iteam]  = FMMC_OBJECTIVE_LOGIC_GET_AND_HOLD AND IS_BIT_SET(MC_serverBD.iObjAtYourHolding[MC_playerBD[iPartToUse].iteam],iobj))
						IF NOT IS_PED_INJURED(LocalPlayerPed)
							//PRINT_HELP("NOCOL_HLP")
							IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciUSE_IN_AND_OUT_HELPTEXT)	
							AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_RUGBY(g_FMMC_STRUCT.iRootContentIdHash)
								PRINT_HELP("NOCOLCTF")
							ELSE
								PRINT_HELP("MCPROTPCK")
							ENDIF
						ENDIF
						iCantCollectHelpDisp++
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(MC_serverBD.iFragableCrate,iobj)
	OR IS_THIS_MODEL_A_CCTV_CAMERA(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].mn)
		IF NOT IS_DAMAGE_TRACKER_ACTIVE_ON_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niObject[iobj])
			ACTIVATE_DAMAGE_TRACKER_ON_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niObject[iobj],TRUE)
			PRINTLN("[RCC MISSION][PROCESS_OBJ_BODY] - Damage tracking activated on crate: ",iobj)
		ENDIF
	ENDIF
	
	PROCESS_SHOULD_PREVENT_OBJECT_PICKUP(tempObj)
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciOBJECT_CARRIED_DISTANCE_TICKER)
		PROCESS_OBJECT_CARRIED_FOR_DISTANCE(tempObj, iobj)
	ENDIF
		
	IF NETWORK_HAS_CONTROL_OF_ENTITY(tempObj)
		MAINTAIN_FROZEN_OBJECTS(tempObj, iObj)
		IF IS_SUDDEN_DEATH_OBJECT_DURING_SUDDEN_DEATH(iObj)
			PREVENT_COLLECTION_OF_PORTABLE_PICKUP(tempObj, TRUE)
			
			IF IS_ENTITY_VISIBLE(tempObj)
				SET_PICKUP_OBJECT_TRANSPARENT_WHEN_UNCOLLECTABLE(tempObj, TRUE)
				SET_PICKUP_OBJECT_ALPHA_WHEN_TRANSPARENT(0)	
				SET_ENTITY_ALPHA(tempObj, 0, FALSE)
				SET_ENTITY_VISIBLE(tempObj, FALSE)
				PRINTLN("[RCC MISSION][PROCESS_OBJ_BODY] - SET_ENTITY_VISIBLE true on iObj: ", iobj)
			ENDIF
		ENDIF
	ELSE
		IF IS_SUDDEN_DEATH_OBJECT_DURING_SUDDEN_DEATH(iObj)
			IF GET_ENTITY_ALPHA(tempObj) > 0
				SET_ENTITY_ALPHA(tempObj, 0, FALSE)
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT NETWORK_HAS_CONTROL_OF_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niObject[iobj])
		EXIT
	ENDIF
	
	FIND_AND_STORE_SNIPERBALL_PACKAGE(tempObj, iobj)
	
	//Sets the start position of the sphere when in sudden death so that it spawns where the package is
	IF IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH)
	AND IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_KILLALLENEMIES_PENNED_IN) 
		DETACH_OBJECT_FROM_PLAYER_ON_SUDDEN_DEATH(tempObj, iObj)
	ENDIF
	
	//resets the packages to its original spawn if it is dropped in an end zone
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciRESET_OBJECT_IN_ENDZONE) 
		IF OBJECT_WAS_DROPPED_IN_END_ZONE(tempObj)
			SET_UP_FMMC_OBJ_RC(tempObj,iobj,MC_ServerBD_1.sFMMC_SBD.niPed, MC_ServerBD_1.sLEGACYMissionContinuityVars,TRUE,TRUE)
			RUGBY_CENTRE_LINE_OBJECT_RESPAWN(tempObj)	//CENTRE LINE RESPAWN - Respawn the get & deliver object along the centre line (Bug 2700219)
			
			vBallDropPoint = <<0,0,0>>
			vBallCollectionPoint = <<0,0,0>>
			piPlayerCarryingBall = INVALID_PLAYER_INDEX()
			iObjBeingCarried = -1
			
			//clear any tickers
			THEFEED_HIDE_THIS_FRAME()
			THEFEED_FLUSH_QUEUE()	
		ENDIF
	ENDIF
	
	// used to ensure vehicle don't get stuck on top of frozen bin bags
	IF( iCurrentHeistMissionIndex = HBCA_BS_SERIES_A_TRASH_TRUCK OR IS_FAKE_MULTIPLAYER_MODE_SET() )
		IF( GET_ENTITY_MODEL( tempObj ) = HEI_PROP_HEIST_BINBAG )
			IF( IS_ENTITY_VISIBLE( tempObj ) AND NOT IS_ENTITY_ATTACHED_TO_ANY_PED( tempObj ) )
				IF( IS_OBJECT_CONFLICTING_WITH_NEARBY_VEHICLE( tempObj, localPlayerPed ) )
					FREEZE_ENTITY_POSITION( tempObj, FALSE )
					SET_BINBAG_UNFREEZABLE_FOR_SHORT_TIME( iObj )
					CPRINTLN( DEBUG_SIMON, "[PROCESS_OBJ_BODY] - Unfreezing binbag because it's conflicting with a nearby vehicle frame: ", GET_FRAME_COUNT() )
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	// hide the timetable when dropping off to an apartment
	IF DOES_ENTITY_EXIST( tempObj )
		IF( GET_ENTITY_MODEL( tempObj ) = HEI_PROP_HEI_TIMETABLE )
			IF( IS_BIT_SET( iLocalBoolCheck10, LBOOL10_CURRENTLY_ON_APARTMENT_GO_TO ) )
				IF( IS_ENTITY_ATTACHED_TO_ANY_PED( tempObj ) )
					iTeam = MC_playerBD[ iPartToUse ].iTeam
					IF( SHOULD_OBJ_CLEAN_UP_ON_DELIVERY( iObj, iTeam ) )
						DETACH_ENTITY( tempObj )
						SET_ENTITY_VISIBLE( tempObj, FALSE )
						SET_ENTITY_COLLISION( tempObj, FALSE, FALSE )
						FREEZE_ENTITY_POSITION( tempObj, TRUE )
						SET_ENTITY_COORDS( tempObj, <<0.0, 0.0, 0.0>> )
						CPRINTLN( DEBUG_SIMON, "[PROCESS_OBJ_BODY] - LBOOL10_CURRENTLY_ON_APARTMENT_GO_TO so warping timetable" )
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_GRAB_OBJECT_CASH(tempobj)
	AND NOT IS_BIT_SET( iObjDeliveredBitset, iobj )
		IF NOT IS_ENTITY_ATTACHED_TO_ANY_PED(tempobj)
		AND MC_serverBD_3.iBombFB_ExplodedBS = 0
			IF ( (NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectBitSet, cibsOBJ_InvisibleOnFirstDelivery))
				OR (NOT IS_BIT_SET(MC_serverBD_1.iObjDeliveredOnceBS, iobj)) )
			AND NOT IS_ENTITY_VISIBLE(tempobj)
				IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_TYPE_BLOW_DOOR)
				OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_TYPE_BLOW_VISIBLE)
					IF DOES_OBJECT_HAVE_ANY_PRIORITY_FOR_ANY_TEAM(iobj)
						//This prop for pilot school needs to be invisible as the mission ends, and this would override that otherwise
						IF (g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].mn = INT_TO_ENUM(MODEL_NAMES, HASH("pil_Prop_Pilot_Icon_01")))
						OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectBitSetTwo, cibsOBJ2_SetInvisible)
						OR (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciDAWN_RAID_ENABLE_MODE)
						AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].mn = INT_TO_ENUM(MODEL_NAMES, HASH("imp_prop_adv_hdsec")))
						AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_ARENA_CTF(g_FMMC_STRUCT.iAdversaryModeType)
							PRINTLN("[KH][PROCESS_OBJ_BODY] - PREVENTING OBJECT BEING MADE VISIBLE: ", iObj)
							IF IS_BIT_SET(MC_serverBD.iServerBitSet3,SBBOOL3_MINIGAME_SHOULD_SHOW_COLLECTABLE)
								SET_ENTITY_VISIBLE(tempobj, TRUE)
							ENDIF
						ELSE
							PRINTLN("[RCC MISSION][PROCESS_OBJ_BODY] setting object visible: ",iobj) 
							SET_ENTITY_VISIBLE(tempobj,TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT bNotPickup
	AND NOT IS_BIT_SET(MC_serverBD.iFragableCrate, iobj)
	AND NOT IS_BIT_SET(MC_serverBD.iWasHackObj,iobj)
	AND NOT IS_BIT_SET( iObjDeliveredBitset, iobj )
	AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciPOINTLESS_MODE_ENABLED)
	AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciENABLE_FAST_DROP_OFF)
		FOR iteam = 0 to (MC_serverBD.iNumberOfTeams-1)
			IF MC_serverBD_4.iObjPriority[iobj][iteam] <= MC_serverBD_4.iCurrentHighestPriority[iteam]
				IF MC_serverBD_4.iObjPriority[iobj][iteam] < FMMC_MAX_RULES
					IF MC_serverBD_4.iObjRule[iobj][iteam] != FMMC_OBJECTIVE_LOGIC_NONE
						IF DOES_OBJECT_RULE_TYPE_REQUIRE_PICKUP(MC_serverBD_4.iObjRule[iobj][iteam], IS_BIT_SET(MC_serverBD.iObjAtYourHolding[iteam],iobj))	
							
							IF (g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].mn != INT_TO_ENUM(MODEL_NAMES, HASH("pil_Prop_Pilot_Icon_01")))
							AND (g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].mn != INT_TO_ENUM(MODEL_NAMES, HASH("prop_ic_cp_bag")))
							AND (g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].mn != INT_TO_ENUM(MODEL_NAMES, HASH("ar_prop_ar_cp_bag")))
							AND NOT DOES_PICKUP_NEED_BIGGER_RADIUS(iobj)
								SET_ENTITY_LOD_DIST(tempObj,150)
							ENDIF
							
							IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectBitSet, cibsOBJ_Reset_If_Dropped_Out_Of_Bounds)
							AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_MISSION_OVER)
							AND IS_BIT_SET(iObjHeldBS, iobj)
							AND NOT IS_ENTITY_ATTACHED_TO_ANY_PED(tempObj)
							AND IS_OBJECT_OUTSIDE_TEAM_MISSION_BOUNDS(iobj, tempObj, iTeam)
								PRINTLN("[KH][2675414][PROCESS_OBJ_BODY] - RESETTING OBJECT")
								//Reset object
								SET_UP_FMMC_OBJ_RC(tempObj,iobj,MC_ServerBD_1.sFMMC_SBD.niPed, MC_ServerBD_1.sLEGACYMissionContinuityVars,TRUE,TRUE)
								RUGBY_CENTRE_LINE_OBJECT_RESPAWN(tempObj)	//CENTRE LINE RESPAWN - Respawn the get & deliver object along the centre line (Bug 2700219)
							ENDIF
							
							IF NOT IS_THIS_A_SPECIAL_PICKUP( iObj )
							AND IS_OBJECT_A_PORTABLE_PICKUP(tempObj)
							AND NOT IS_BIT_SET(iLocalBoolCheck17, LBOOL17_PREVENT_PICKUP_OF_PICKUP)
								PRINTLN("[KH][2675414][PROCESS_OBJ_BODY] - SETTING AS PICKUP OBJECT")
								//SET_ENTITY_ALPHA(tempObj,255,FALSE)
								SET_TEAM_PICKUP_OBJECT(tempObj,iteam,TRUE)
							ENDIF
							
						ELSE
							SET_PACKAGE_NOT_COLLECTABLE_FOR_TEAM(tempObj,iobj,iteam)
						ENDIF
					ELSE
						SET_PACKAGE_NOT_COLLECTABLE_FOR_TEAM(tempObj,iobj,iteam)
					ENDIF
				ELSE
					SET_PACKAGE_NOT_COLLECTABLE_FOR_TEAM(tempObj,iobj,iteam)
				ENDIF
			ELSE
				SET_PACKAGE_NOT_COLLECTABLE_FOR_TEAM(tempObj,iobj,iteam)
			ENDIF
		ENDFOR
	ENDIF
	
	IF bNotPickup
		IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].mn != P_V_43_Safe_S
		AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].mn != prop_container_ld_pu
			VEHICLE_INDEX tempveh
			
			IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].mn = PROP_CONTR_03B_LD // Only this container can be attached to a handler
				tempVeh = FIND_HANDLER_VEHICLE_CONTAINER_IS_ATTACHED_TO(tempObj)
			ENDIF
			
			IF IS_ENTITY_ATTACHED_TO_ANY_VEHICLE(tempObj)
				tempveh = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_ATTACHED_TO(tempObj))
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(tempVeh)
				IF GET_PED_IN_VEHICLE_SEAT(tempVeh) = LocalPlayerPed
					IF MC_serverBD_4.iObjRule[iobj][MC_playerBD[iPartToUse].iteam]   != FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER
					AND (MC_serverBD_4.iObjRule[iobj][MC_playerBD[iPartToUse].iteam] != FMMC_OBJECTIVE_LOGIC_GET_AND_HOLD OR (MC_serverBD_4.iObjRule[iobj][MC_playerBD[iPartToUse].iteam]  = FMMC_OBJECTIVE_LOGIC_GET_AND_HOLD AND IS_BIT_SET(MC_serverBD.iObjAtYourHolding[MC_playerBD[iPartToUse].iteam],iobj)))
					AND MC_serverBD_4.iObjRule[iobj][MC_playerBD[iPartToUse].iteam]  != FMMC_OBJECTIVE_LOGIC_CAPTURE
					AND MC_serverBD_4.iObjRule[iobj][MC_playerBD[iPartToUse].iteam]  != FMMC_OBJECTIVE_LOGIC_GO_TO
						
						IF GET_ENTITY_MODEL(tempVeh) = HANDLER
							DETACH_CONTAINER_FROM_HANDLER_FRAME(tempVeh)
							PRINTLN("[RCC MISSION][PROCESS_OBJ_BODY] detach being called for handler on crate: ",iobj) 
						ELIF ( (GET_ENTITY_MODEL(tempVeh) = CARGOBOB) OR (GET_ENTITY_MODEL(tempVeh) = CARGOBOB2) OR (GET_ENTITY_MODEL(tempVeh) = CARGOBOB3) )
							DETACH_VEHICLE_FROM_CARGOBOB(tempVeh, tempVeh) //detach anything from this cargobob
							SET_CARGOBOB_PICKUP_MAGNET_ACTIVE(tempVeh, FALSE)
							PRINTLN("[RCC MISSION][PROCESS_OBJ_BODY] detach being called for cargobob on crate: ",iobj) 
						ELSE
							//DETACH_ENTITY(tempObj,TRUE,FALSE)
						ENDIF
					ELSE
						IF GET_ENTITY_MODEL(tempVeh) = HANDLER
						OR GET_ENTITY_MODEL(tempVeh) = CARGOBOB
						OR GET_ENTITY_MODEL(tempVeh) = CARGOBOB2
						OR GET_ENTITY_MODEL(tempVeh) = CARGOBOB3
							IF NOT (IS_BIT_SET(iContBitSet,iobj))
								SET_BIT(iContBitSet,iobj)
								CLEAR_BIT(iDropContBitSet,iobj)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC



//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//
//!!! NEW SECTION BELOW: EVERY FRAME OBJECT CHECKS / OBJECT HUD !!!
//
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************
//************************************************************************************************************************************************************

//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: EVERY FRAME BREAKABLE OBJECT FUNCTIONS (weapon crates) !
//
//************************************************************************************************************************************************************


PROC PROCESS_PARTICLE_FX_OVERRIDE(INT iObj)
	IF NOT IS_BIT_SET(iLocalBoolCheck20, LBOOL20_PARTICLE_FX_OVERRIDE_COCAINE)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].mn = INT_TO_ENUM(MODEL_NAMES, HASH("imp_Prop_ImpExp_BoxCoke_01"))
			REQUEST_NAMED_PTFX_ASSET("scr_ie_svm_technical2")
			IF NOT HAS_NAMED_PTFX_ASSET_LOADED("scr_ie_svm_technical2")			
				PRINTLN("[PROCESS_PARTICLE_FX_OVERRIDE] Not loaded scr_ie_svm_technical2 effect asset yet")
			ELSE
				PRINTLN("[PROCESS_PARTICLE_FX_OVERRIDE] Loaded scr_ie_svm_technical2 effect asset")
				PRINTLN("[PROCESS_PARTICLE_FX_OVERRIDE] SET_PARTICLE_FX_OVERRIDE \"ent_dst_polystyrene\" with \"scr_dst_cocaine\"")
				SET_PARTICLE_FX_OVERRIDE("ent_dst_polystyrene", "scr_dst_cocaine")
				SET_BIT(iLocalBoolCheck20, LBOOL20_PARTICLE_FX_OVERRIDE_COCAINE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_PARTICLE_FX_OVERRIDE_DYNOPROP(INT iDynoprop)
	IF NOT IS_BIT_SET(iLocalBoolCheck21, LBOOL21_PARTICLE_FX_OVERRIDE_BOX_PILE)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[iDynoprop].mn = INT_TO_ENUM(MODEL_NAMES, HASH("sr_prop_sr_boxpile_03"))
			REQUEST_NAMED_PTFX_ASSET("scr_sr_adversary")
			IF NOT HAS_NAMED_PTFX_ASSET_LOADED("scr_sr_adversary")			
				PRINTLN("[PROCESS_PARTICLE_FX_OVERRIDE_DYNOPROP] Not loaded scr_sr_adversary effect asset yet")
			ELSE
				PRINTLN("[PROCESS_PARTICLE_FX_OVERRIDE_DYNOPROP] Loaded scr_sr_adversary effect asset")
				PRINTLN("[PROCESS_PARTICLE_FX_OVERRIDE_DYNOPROP] SET_PARTICLE_FX_OVERRIDE \"ent_dst_gen_cardboard\" with \"scr_sr_dst_cardboard\"")
				SET_PARTICLE_FX_OVERRIDE("ent_dst_gen_cardboard", "scr_sr_dst_cardboard")
				SET_BIT(iLocalBoolCheck21, LBOOL21_PARTICLE_FX_OVERRIDE_BOX_PILE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC CLIENT_MANAGE_FRAG_CRATE_PICKUPS(INT iobj)
	
	IF bIsLocalPlayerHost
		INT ipickup
		INT iamount
	    INT iPlacementFlags = 0
		BOOL bGotAllSpawnPoints = TRUE
		
		IF IS_BIT_SET(MC_serverBD.iCrateBrokenBitset, iobj)
			IF NOT IS_BIT_SET(iLocalCrateBrokenBitset, iobj)
				SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_MAP))
				FOR ipickup = 0 TO (ciMAX_CRATE_PICKUPS-1)
					IF NOT IS_VECTOR_ZERO(MC_serverBD_3.vCratePickupCoords[iobj][ipickup])
						IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iCrateContents = ciCRATE_CONTENTS_CASH
							IF NOT DOES_ENTITY_EXIST(piCratePickup[iobj][ipickup].oiPickup)
								SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ORIENT_TO_GROUND))
		  						SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_SNAP_TO_GROUND))
		  						SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ON_OBJECT))
								PRINTLN("[RCC_MISSION] - DESTRUCTIBLE CRATE PICKUP - Creating CASH pickup : ", ipickup," with value : ",iamount)
								piCratePickup[iobj][ipickup] = CREATE_AMBIENT_PICKUP_WITH_SAP_PICKUP_ID(PICKUP_MONEY_VARIABLE, MC_serverBD_3.vCratePickupCoords[iobj][ipickup] + <<0,0,0.10>>, iPlacementFlags, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iCrateValue, DEFAULT, TRUE, TRUE)
								//SET_ENTITY_VISIBLE(piCratePickup[iobj][ipickup], FALSE)
								PRINTLN("[RCC_MISSION] - DESTRUCTIBLE CRATE PICKUP - SET CASH PICKUP INVISIBLE")
								bGotAllSpawnPoints = FALSE
							ENDIF
						ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iCrateContents = ciCRATE_CONTENTS_DRUGS
							IF ipickup < g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iCrateValue
								IF NOT DOES_ENTITY_EXIST(piCratePickup[iobj][ipickup].oiPickup)
									REQUEST_MODEL(PROP_DRUG_PACKAGE_02)
				               		IF HAS_MODEL_LOADED(PROP_DRUG_PACKAGE_02)
										SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ORIENT_TO_GROUND))
			  							SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_SNAP_TO_GROUND))
		  								SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ON_OBJECT))
										PRINTLN("[RCC_MISSION] - DESTRUCTIBLE CRATE PICKUP - Creating DRUGS pickup : ", ipickup," with quantity : ",iamount)
										piCratePickup[iobj][ipickup] = CREATE_AMBIENT_PICKUP_WITH_SAP_PICKUP_ID(PICKUP_MONEY_MED_BAG, MC_serverBD_3.vCratePickupCoords[iobj][ipickup] + <<0,0,0.10>>, iPlacementFlags, 500, PROP_DRUG_PACKAGE_02, TRUE, TRUE)
										//SET_ENTITY_VISIBLE(piCratePickup[iobj][ipickup], FALSE)
										PRINTLN("[RCC_MISSION] - DESTRUCTIBLE CRATE PICKUP - SET DRUGS PICKUP INVISIBLE")
									ENDIF
									bGotAllSpawnPoints = FALSE
								ENDIF
							ENDIF
						ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iCrateContents = ciCRATE_CONTENTS_WEAPON
							IF NOT DOES_ENTITY_EXIST(piCratePickup[iobj][ipickup].oiPickup)
								//SET_FM_PICKUP_ORIENTATION_FLAGS(iPlacementFlags)
								//SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_FIXED))
	  							SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ORIENT_TO_GROUND))
	  							SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_SNAP_TO_GROUND))
		  						SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ON_OBJECT))
								
								//Make Health/Armour Full
								IF INT_TO_ENUM(PICKUP_TYPE, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iCrateValue) = PICKUP_ARMOUR_STANDARD
								OR INT_TO_ENUM(PICKUP_TYPE, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iCrateValue) = PICKUP_HEALTH_STANDARD
									iAmount = 500
								ELSE	//Get Ammo
									IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iAmmoValue = 0
										iAmount = 50
									ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iAmmoValue = 1
										iAmount = 100
									ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iAmmoValue = 2
										iAmount = 200
									ELSE	//Maximum
										GET_MAX_AMMO(LocalPlayerPed, FMMC_GET_WEAPON_TYPE_FROM_PICKUP_TYPE(INT_TO_ENUM(PICKUP_TYPE, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iCrateValue), TRUE), iAmount)
										
										PRINTLN("[RCC_MISSION] - DESTRUCTIBLE CRATE PICKUP - Maximum Ammo for Weapon: ", GET_WEAPON_NAME(FMMC_GET_WEAPON_TYPE_FROM_PICKUP_TYPE(INT_TO_ENUM(PICKUP_TYPE, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iCrateValue), TRUE)), " with Ammo: ", iAmount)
									ENDIF
								ENDIF
								
								PRINTLN("[RCC_MISSION] - DESTRUCTIBLE CRATE PICKUP - Creating WEAPON pickup : ", iPickup," with ammo : ", iAmount)
								piCratePickup[iObj][iPickup] = CREATE_AMBIENT_PICKUP_WITH_SAP_PICKUP_ID(INT_TO_ENUM(PICKUP_TYPE, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iCrateValue), MC_serverBD_3.vCratePickupCoords[iObj][iPickup] + <<0,0,0.10>>, iPlacementFlags, iAmount, DEFAULT, TRUE, TRUE)
								PRINTLN("PD PICKUP DEBUG - creating pickup g_FMMC_STRUCT_ENTITIES.sPlacedObject[", iObj, "].iCrateValue is ", GET_STRING_FROM_PICKUP_TYPE(INT_TO_ENUM(PICKUP_TYPE, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iCrateValue)))
								
								//vPickupPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(NET_TO_OBJ(serverBD.DCrateData[i].NetID), <<0, 0.25-(k*0.25), 0.1>>)
								//serverBD.DCrateData[i].Pickup[k] = CREATE_AMBIENT_PICKUP(serverBD.DCrateData[i].ContentsWeapon, vPickupPos, iPlacementFlags, DEFAULT, DEFAULT, TRUE)
								
								//SET_ENTITY_VISIBLE(piCratePickup[iObj][iPickup], FALSE)
								PRINTLN("[RCC_MISSION] - DESTRUCTIBLE CRATE PICKUP - SET WEAPON PICKUP INVISIBLE")
								bGotAllSpawnPoints = FALSE
							ENDIF
						ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iCrateContents = ciCRATE_CONTENTS_AMMO
							IF NOT DOES_ENTITY_EXIST(piCratePickup[iobj][ipickup].oiPickup)
								SET_FM_PICKUP_ORIENTATION_FLAGS(iPlacementFlags)
		  						SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ON_OBJECT))
								IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iCrateValue = 0
									iamount = 50
								ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iCrateValue = 1
									iamount = 100
								ELSE
									iamount = 200
								ENDIF
								PRINTLN("[RCC_MISSION] - DESTRUCTIBLE CRATE PICKUP - Creating AMMO pickup : ", ipickup," with ammo : ",iamount)
								piCratePickup[iobj][ipickup] = CREATE_AMBIENT_PICKUP_WITH_SAP_PICKUP_ID(PICKUP_AMMO_BULLET_MP , MC_serverBD_3.vCratePickupCoords[iobj][ipickup] + <<0,0,0.10>>, iPlacementFlags,iamount, DEFAULT, TRUE, TRUE)
								//SET_ENTITY_VISIBLE(piCratePickup[iobj][ipickup], FALSE)
								PRINTLN("[RCC_MISSION] - DESTRUCTIBLE CRATE PICKUP - SET AMMO PICKUP INVISIBLE")
								bGotAllSpawnPoints = FALSE
							ENDIF
						ENDIF
					ENDIF
				ENDFOR
				IF bGotAllSpawnPoints
					SET_BIT(iLocalCrateBrokenBitset, iobj)
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

//Now in props -
//PROC PROCESS_OBJECTS_TO_BE_DESTROYED_IN_ONE_HIT(OBJECT_INDEX tempObj, INT iObj)

//	VECTOR vObjCoord
//	
//	IF NOT IS_BIT_SET(bsHasObjExploded, iobj)
//		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectBitSet, cibsOBJ_DestroyInOneHit)
//			IF DOES_ENTITY_EXIST(tempObj)
//				IF NOT IS_ENTITY_DEAD(tempObj)
//					IF HAS_OBJECT_BEEN_BROKEN(tempObj)
//					OR HAS_ENTITY_BEEN_DAMAGED_BY_ANY_PED(tempObj)
//						PRINTLN("[RCC MISSION] PROCESS_OBJECTS_TO_BE_DESTROYED_IN_ONE_HIT -  iobj: ", iobj, " has been damaged. ")
//						vObjCoord = GET_ENTITY_COORDS(tempObj,FALSE)
//						ADD_EXPLOSION(vObjCoord,EXP_TAG_BARREL)
//						SET_BIT(bsHasObjExploded, iobj)
//					ENDIF
//				ENDIF
//			ENDIF	
//		ENDIF
//	ENDIF
//ENDPROC

PROC PROCESS_DESTRUCTIBLE_CRATES(OBJECT_INDEX tempObj, INT iobj, BOOL bExists)
	INT ipickup
	
	IF IS_BIT_SET(MC_serverBD.iFragableCrate, iobj)
	AND NOT IS_BIT_SET(MC_serverBD.iCrateBrokenBitset, iobj)
		IF IS_BIT_SET(MC_serverBD.iFragableCrateCreated, iobj)
			IF bExists
				IF HAS_OBJECT_BEEN_BROKEN(tempObj)
				OR IS_ENTITY_DEAD(tempObj)
					FOR ipickup = 0 TO (ciMAX_CRATE_PICKUPS-1)
						IF IS_VECTOR_ZERO(MC_serverBD_3.vCratePickupCoords[iobj][ipickup])
							MC_serverBD_3.vCratePickupCoords[iobj][ipickup] = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(tempObj, <<0.5-(ipickup*0.5), 0, 0.0>>)//0.1>>)
							PRINTLN("[RCC MISSION] PROCESS_DESTRUCTIBLE_CRATES - OBJECT BROKEN - vCratePickupCoords was zero for iobj = ", iobj, " ipickup = ", ipickup, " vCratePickupCoords = ", MC_serverBD_3.vCratePickupCoords[iobj][ipickup])
							SET_BIT(MC_serverBD.iCrateBrokenBitset, iobj)
							PRINTLN("[RCC MISSION] PROCESS_DESTRUCTIBLE_CRATES - iCrateBrokenBitset SET -  iobj = ", iobj)
						ENDIF
					ENDFOR
				ENDIF
			ELSE
				//-- Still want to get create pickups if object is destroyed.
				IF IS_ENTITY_DEAD(tempObj)
					FOR ipickup = 0 TO (ciMAX_CRATE_PICKUPS-1)
						IF IS_VECTOR_ZERO(MC_serverBD_3.vCratePickupCoords[iobj][ipickup])
//							MC_serverBD_3.vCratePickupCoords[iobj][ipickup] = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(tempObj, <<0.5-(ipickup*0.5), 0, 0.0>>)//0.1>>)
//							PRINTLN("[RCC MISSION] PROCESS_DESTRUCTIBLE_CRATES - OBJECT DESTROYED - vCratePickupCoords was zero for iobj = ", iobj, " ipickup = ", ipickup, " vCratePickupCoords = ", MC_serverBD_3.vCratePickupCoords[iobj][ipickup])
							SET_BIT(MC_serverBD.iCrateBrokenBitset, iobj)
							PRINTLN("[RCC MISSION] PROCESS_DESTRUCTIBLE_CRATES - iCrateBrokenBitset SET -  iobj = ", iobj)
						ENDIF
					ENDFOR
				ENDIF
			ENDIF
		
		ELSE
			IF bExists
			AND DOES_ENTITY_EXIST(tempObj)
			AND NOT IS_ENTITY_DEAD(tempObj)
			AND NOT IS_VECTOR_ZERO(GET_ENTITY_COORDS(tempObj))
				SET_BIT(MC_serverBD.iFragableCrateCreated, iobj)
				PRINTLN("[RCC MISSION] PROCESS_DESTRUCTIBLE_CRATES - iFragableCrateCreated SET - AA -  iobj = ", iobj)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_DAWN_RAID_PICKUP()
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_DAWN_RAID(g_FMMC_STRUCT.iAdversaryModeType)
		IF DOES_ENTITY_EXIST(objDawnPickUp)
			IF IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH)
				IF IS_ENTITY_VISIBLE(objDawnPickUp)
					IF NETWORK_HAS_CONTROL_OF_ENTITY(objDawnPickUp)
						SET_ENTITY_VISIBLE(objDawnPickUp, FALSE)
					ELSE
						NETWORK_REQUEST_CONTROL_OF_ENTITY(objDawnPickUp)
					ENDIF
				ENDIF
				
				bDawnRaidGroundHeightCheckFinished = TRUE
			ENDIF
		ENDIF
		
		IF NOT bDawnRaidGroundHeightCheckFinished
			IF DOES_ENTITY_EXIST(objDawnPickUp)
			
				BOOL bTooCloseToCase = FALSE
				BOOL bCaseExists = FALSE
				INT iFindCaseLoop
				FOR iFindCaseLoop = 0 TO FMMC_MAX_NUM_OBJECTS - 1
					IF iThisContainerNumBeingInvestigated = iFindCaseLoop
						PRINTLN("[PROCESS_DAWN_RAID_PICKUP] Checking object ", iFindCaseLoop)
						IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niObject[iFindCaseLoop])
							OBJECT_INDEX crateObj = NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[iFindCaseLoop])
							IF GET_ENTITY_MODEL(crateObj) = INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_adv_case"))
								IF GET_DISTANCE_BETWEEN_ENTITIES(objDawnPickUp, crateObj) < 1.5
									bTooCloseToCase = TRUE
									bCaseExists = TRUE
									PRINTLN("[PROCESS_DAWN_RAID_PICKUP] Too close to crate!!")
									BREAKLOOP
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDFOR
			
				//Player died whilst opening the container
				IF DOES_ENTITY_EXIST(oiCurrentContainer)
					IF iThisContainerNumBeingInvestigated != -1
						IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_MINIGAME_OBJ_0_FOUND + iThisContainerNumBeingInvestigated)
							IF iTrackifyTargets[iThisContainerNumBeingInvestigated] > -1
								IF MC_serverBD.iObjHackPart[iTrackifyTargets[iThisContainerNumBeingInvestigated]] = iLocalPart
									IF IS_PED_INJURED(LocalPlayerPed)
										iDawnRaidReset = iThisContainerNumBeingInvestigated
										objDawnRaidContainer = oiCurrentContainer
										iDawnRaidContainerPart = MC_serverBD.iObjHackPart[iTrackifyTargets[iThisContainerNumBeingInvestigated]]
										PRINTLN("[PROCESS_DAWN_RAID_PICKUP] IS_PED_INJURED")
									ENDIF	
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//Player disconnected whilst opening the container
				IF bIsLocalPlayerHost
					INT iContainer
					FOR iContainer = 0 TO MAX_TRACKIFY_TARGETS - 1
						IF IS_BIT_SET(MC_serverBD.iServerObjectBeingInvestigatedBitSet, iContainer)
						AND iDisconnectedDawnRaidPart > -1
							IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niObject[iTrackifyTargets[iContainer]])
								IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iDisconnectedDawnRaidPart))
									IF IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_MINIGAME_SHOULD_SHOW_COLLECTABLE)
										iDawnRaidReset = iContainer
										objDawnRaidContainer = NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[iTrackifyTargets[iContainer]])
										iDawnRaidContainerPart = MC_serverBD.iObjHackPart[iTrackifyTargets[iContainer]]
										iDisconnectedDawnRaidPart = -1
										PRINTLN("[PROCESS_DAWN_RAID_PICKUP] Participant Disconnected!")
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDFOR
				ENDIF
				
				BOOL bObjFloatingAboveGround = FALSE
				IF !bTooCloseToCase
				AND bCaseExists
					IF NOT IS_ENTITY_ATTACHED_TO_ANY_PED(objDawnPickUp)
						FLOAT fGroundZ
						VECTOR vObjPos = GET_ENTITY_COORDS(objDawnPickUp)
						VECTOR vObjGroundPos = vObjPos
						GET_GROUND_Z_FOR_3D_COORD(vObjPos, fGroundZ)
						vObjGroundPos.z = fGroundZ
						IF NOT ARE_VECTORS_ALMOST_EQUAL(vObjPos, vObjGroundPos, 0.1)
							PRINTLN("[PROCESS_DAWN_RAID_PICKUP] Object is floating above ground...")
							bObjFloatingAboveGround = TRUE
						ENDIF
					ENDIF
				ENDIF
				
				IF DOES_ENTITY_EXIST(objDawnPickUp)
					IF NOT IS_ENTITY_ATTACHED_TO_ANY_PED(objDawnPickUp)
					AND !bTooCloseToCase
					AND bCaseExists
					AND bObjFloatingAboveGround
						IF NETWORK_HAS_CONTROL_OF_ENTITY(objDawnPickUp)
							INT iTeamLoop
							FOR iTeamLoop = 0 TO FMMC_MAX_TEAMS-1
								IF IS_TEAM_ACTIVE(iTeamLoop)
									SET_TEAM_PICKUP_OBJECT(objDawnPickup, iTeamLoop, TRUE)
								ENDIF
							ENDFOR
							PLACE_OBJECT_ON_GROUND_PROPERLY(objDawnPickup)
							SET_ENTITY_VISIBLE(objDawnPickup, TRUE)
							SET_ENTITY_COLLISION(objDawnPickup, TRUE)
							FREEZE_ENTITY_POSITION(objDawnPickup, FALSE)
							PRINTLN("[PROCESS_DAWN_RAID_PICKUP] Setting object on the ground!")
						ENDIF
					ENDIF
				ENDIF
				
				IF iDawnRaidReset != -1
				AND DOES_ENTITY_EXIST(objDawnRaidContainer)
					IF NOT IS_ENTITY_ATTACHED_TO_ANY_PED(objDawnPickUp)
						IF NETWORK_HAS_CONTROL_OF_ENTITY(objDawnPickUp)
							IF NOT (iDawnRaidContainerPart != -1 AND IS_SYNCHRONIZED_SCENE_RUNNING(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(MC_playerBD[iDawnRaidContainerPart].iSynchSceneID)))
								VECTOR vOffset = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objDawnRaidContainer, <<0.0, -1.0, 0.0>>)
								FLOAT fGroundZ
								GET_GROUND_Z_FOR_3D_COORD(vOffset, fGroundZ)
								vOffset.z = fGroundZ
								
								IF ARE_VECTORS_ALMOST_EQUAL(vOffset, GET_ENTITY_COORDS(objDawnPickUp), 2.0)
									PRINTLN("[PROCESS_DAWN_RAID_PICKUP] Player died /disconnected whilst opening container - objDawnPickUp is now in front of container at <<", vOffset.X, ", ", vOffset.Y, ", ", vOffset.Z, ">>")
									
									BROADCAST_CONTAINER_MINIGAME_DATA_CHANGE(iDawnRaidReset, iLocalPart, TRUE, TRUE, FALSE, TRUE) 
									
									SET_ENTITY_VISIBLE(objDawnPickUp, TRUE)
									PLACE_OBJECT_ON_GROUND_PROPERLY(objDawnPickup)
									
									INT iTeamLoop
									
									FOR iTeamLoop = 0 TO (MC_serverBD.iNumberOfTeams - 1)
										SET_TEAM_PICKUP_OBJECT(objDawnPickUp, iTeamLoop, TRUE)
										PRINTLN("[PROCESS_DAWN_RAID_PICKUP] HANDLE_INVESTIGATE_CONTAINER allowing object to be collected for team ", iTeamLoop)
									ENDFOR
									
									//REMOVE_TRACKIFY_MULTIPLE_TARGET(iDawnRaidReset)
									
									SET_BIT(iTrackifyTargetProcessed, iDawnRaidReset)
									
									iDawnRaidReset = -1
									iDisconnectedDawnRaidPart = -1
								ENDIF
							ELSE
								PRINTLN("[PROCESS_DAWN_RAID_PICKUP] Player died /disconnected whilst opening container - sync scene still running for part ", iDawnRaidContainerPart)
							ENDIF
						ELSE
							NETWORK_REQUEST_CONTROL_OF_ENTITY(objDawnPickUp)

							PRINTLN("[PROCESS_DAWN_RAID_PICKUP] Player died /disconnected whilst opening container - requesting control of objDawnPickUp")
						ENDIF
					ELSE
						PRINTLN("[PROCESS_DAWN_RAID_PICKUP] Player died whilst opening container - but objDawnPickUp has been collected")
						iDisconnectedDawnRaidPart = -1
						bDawnRaidGroundHeightCheckFinished = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_GRANULAR_CAPTURE(INT iObj)
	IF IS_BIT_SET(MC_serverBD.iServerBitset3, SBBOOL3_TEAM_0_SHOULD_RESPAWN_BACK_AT_START_NOW + MC_playerBD[iLocalPart].iTeam)
	OR IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + MC_playerBD[iLocalPart].iTeam)
	OR g_bMissionOver OR g_bMissionEnding
	OR NOT bLocalPlayerPedOk
	OR IS_BIT_SET(MC_ServerBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH)
		IF IS_BIT_SET(MC_playerBD[iLocalPart].iGranularObjCapturingBS, iObj)	
			CLEAR_BIT(MC_playerBD[iLocalPart].iGranularObjCapturingBS, iObj)
			PRINTLN("[JS][GRANCAP] - PROCESS_GRANULAR_CAPTURE - (4) we are no longer carrying obj - ", iObj)
		ENDIF
		EXIT
	ENDIF
	IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niObject[iObj])
		ENTITY_INDEX tempObj = NET_TO_ENT(MC_serverBD_1.sFMMC_SBD.niObject[iObj])
		IF IS_ENTITY_ATTACHED_TO_ANY_PED(tempObj)
			ENTITY_INDEX tempPlayerEnt = GET_ENTITY_ATTACHED_TO(tempObj)
			IF DOES_ENTITY_EXIST(tempPlayerEnt)
				IF GET_PED_INDEX_FROM_ENTITY_INDEX(tempPlayerEnt) = LocalPlayerPed
					IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iGranularObjCapturingBS, iObj)
						PRINTLN("[JS][GRANCAP] - PROCESS_GRANULAR_CAPTURE - we are now carrying obj - ", iObj)
						SET_BIT(MC_playerBD[iLocalPart].iGranularObjCapturingBS, iObj)
					ENDIF
				ELSE
					IF IS_BIT_SET(MC_playerBD[iLocalPart].iGranularObjCapturingBS, iObj)
						PRINTLN("[JS][GRANCAP] - PROCESS_GRANULAR_CAPTURE - (1) we are no longer carrying obj - ", iObj)
						CLEAR_BIT(MC_playerBD[iLocalPart].iGranularObjCapturingBS, iObj)
					ENDIF
				ENDIF
			ELSE
				IF IS_BIT_SET(MC_playerBD[iLocalPart].iGranularObjCapturingBS, iObj)
					PRINTLN("[JS][GRANCAP] - PROCESS_GRANULAR_CAPTURE - (2) we are no longer carrying obj - ", iObj)
					CLEAR_BIT(MC_playerBD[iLocalPart].iGranularObjCapturingBS, iObj)
				ENDIF
			ENDIF
		ELSE
			IF IS_BIT_SET(MC_playerBD[iLocalPart].iGranularObjCapturingBS, iObj)
				PRINTLN("[JS][GRANCAP] - PROCESS_GRANULAR_CAPTURE - (3) we are no longer carrying obj - ", iObj)
				CLEAR_BIT(MC_playerBD[iLocalPart].iGranularObjCapturingBS, iObj)
			ENDIF
		ENDIF
	ENDIF

	IF IS_BIT_SET(MC_playerBD[iLocalPart].iGranularObjCapturingBS, iObj)
		IF HAS_NET_TIMER_STARTED(tdGranularCaptureTimer)
			IF HAS_NET_TIMER_EXPIRED(tdGranularCaptureTimer, ciGRANULAR_CAPTURE_TIME)
				iGranularPointsThisFrame++
			ENDIF
		ELSE
			REINIT_NET_TIMER(tdGranularCaptureTimer)
		ENDIF
	ENDIF
ENDPROC


//************************************************************************************************************************************************************
//
//! NEW SUB-SECTION BELOW: DISPLAY OBJECT HUD !
//
//************************************************************************************************************************************************************



FUNC BOOL HAS_OBJ_RESPAWN_DELAY_EXPIRED(INT iobj)
	
	BOOL bExpired
	
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iRespawnDelay != 0
	AND IS_BIT_SET(MC_serverBD_2.iObjSpawnedOnce, iobj)
		IF NOT HAS_NET_TIMER_STARTED(MC_serverBD_3.tdObjSpawnDelay[iobj])
			REINIT_NET_TIMER(MC_serverBD_3.tdObjSpawnDelay[iobj])
		ELSE
			INT iTimeScale = 1
			IF IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_CASINO_HEIST)
				iTimeScale = 1000
			ENDIF
			IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD_3.tdObjSpawnDelay[iobj]) >= g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iRespawnDelay*iTimeScale
				bExpired = TRUE
			ENDIF
		ENDIF
	ELSE
		bExpired = TRUE
	ENDIF
	
	RETURN bExpired
	
ENDFUNC

PROC SERVER_CLEAR_OBJ_AT_HOLDING(INT iObj)
	INT iTeam
	FOR iTeam = 0 TO FMMC_MAX_TEAMS -1
		IF IS_BIT_SET(MC_serverBD.iObjAtYourHolding[iTeam],iobj)
			CLEAR_BIT(MC_serverBD.iObjAtYourHolding[iTeam],iobj)
			PRINTLN("[MC] SERVER_CLEAR_OBJ_AT_HOLDING - Clearing Obj bit: ", iobj, " from team: ", iTeam)
		ENDIF
	ENDFOR
ENDPROC



FUNC BOOL MAINTAIN_LESTER_SNAKE_MINIGAME(BOOL bForce = TRUE)
	
	IF NOT IS_BIT_SET(iLocalBoolCheck7, LBOOL7_HACKING_MINIGAME_COMPLETE) OR bForce
	
		INT iTeam = MC_playerBD[iLocalPart].iteam
		INT iRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
		
		IF IS_SAFE_TO_RUN_FAKE_PHONE_TASK(hackingMinigameData)
			IF NOT IS_PED_RUNNING_MOBILE_PHONE_TASK(LocalPlayerPed)
				TASK_USE_MOBILE_PHONE(LocalPlayerPed, TRUE, Mode_ToText)
				PRINTLN("[RCC MISSION] MAINTAIN_PHONE_HACK_MINIGAME - PHONE TASK GIVEN - TASK_USE_MOBILE_PHONE")
			ELSE
				//SET_PED_CAN_PLAY_GESTURE_ANIMS(LocalPlayerPed, FALSE)
				TASK_LOOK_AT_COORD(LocalPlayerPed, GET_PED_BONE_COORDS(LocalPlayerPed, BONETAG_PH_R_HAND, <<0,0,0>>), -1, SLF_DEFAULT)
			ENDIF
		ELSE
			PRINTLN("[RCC MISSION] NOT IS_SAFE_TO_RUN_FAKE_PHONE_TASK")
		ENDIF

		IF NOT SHOULD_DEBUG_LAUNCH_PHONE_HACK_MINIGAME() // FIX FOR DEBUG LAUNCH TESTING
			IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_USE_FAKE_PHONE)
				SET_BIT(MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_USE_FAKE_PHONE)
				PRINTLN("[RCC MISSION] MAINTAIN_PHONE_HACK_MINIGAME - SET USE FAKE PHONE")
			ENDIF
		ENDIF
		
		//DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_AIM)
		NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE)
		
		IF NOT HAS_MINIGAME_TIMER_BEEN_INITIALISED(0)
			PRINTLN("[RCC MISSION] HACKING (CIRCUIT BOARD) - INCREASE_MINIGAME_ATTEMPTS_FOR_TELEMETRY - SLOT 0")
			PRINTLN("[RCC MISSION] HACKING (CIRCUIT BOARD) - START MINIGAME TIMER - SLOT 0")							
			INCREASE_MINIGAME_ATTEMPTS_FOR_TELEMETRY(0)
			START_MINIGAME_TIMER_FOR_TELEMETRY(0)
		ENDIF
		
		IF NOT IS_BIT_SET(hackingMinigameData.i_CircuitBitSet, BIT_SET_CIRCUIT_START_TIMER)
			REINIT_NET_TIMER(stMGTimer0)
			SET_BIT(hackingMinigameData.i_CircuitBitSet, BIT_SET_CIRCUIT_START_TIMER)
		ENDIF
		
		DISABLE_DPADDOWN_THIS_FRAME()
		
		BOOL bHard
		IF SHOULD_DEBUG_LAUNCH_PHONE_HACK_MINIGAME()
			bHard = TRUE
		ELSE
			IF IS_BIT_SET(MC_serverBD_1.iServerHackMGBitset, SBBOOL_HACK_DO_HARD)
				bHard = TRUE
			ENDIF
		ENDIF
		
		PRINTLN("[RCC MISSION] MC_PlayerBD[iLocalPart].inumHacks test 0 = ", MC_PlayerBD[iLocalPart].inumHacks)

		RUN_CIRCUIT_HACKING_MINIGAME(hackingMinigameData,bHard,DEFAULT,TRUE)
		
		IF IS_BIT_SET(hackingMinigameData.i_CircuitBitSet, BIT_SET_CIRCUIT_LEVEL_FAILED)
			INCREASE_MINIGAME_ATTEMPTS_FOR_TELEMETRY(0)
			PRINTLN("[RCC MISSION] HACKING (CIRCUIT BOARD) - INCREASE_MINIGAME_ATTEMPTS_FOR_TELEMETRY - SLOT 0")
		ENDIF
		
		IF HAS_PLAYER_BEAT_CIRCUIT_HACKING(hackingMinigameData)
		OR SHOULD_FORCE_PASS_HACK_MINIGAME()
			BROADCAST_HEIST_END_HACK_MINIGAME(ALL_PLAYERS_ON_SCRIPT(TRUE), TRUE, MC_playerBD[iLocalPart].iTeam)
			CLEANUP_AND_QUIT_CIRCUIT_HACKING_MINIGAME(hackingMinigameData, IS_BIT_SET(MC_serverBD_1.iServerHackMGBitset, SBBOOL_HACK_DO_HARD))
			
			SET_BIT(iLocalBoolCheck7,LBOOL7_HACKING_MINIGAME_COMPLETE)
			
			IF IS_BIT_SET(MC_serverBD_1.iServerHackMGBitset, SBBOOL_HACK_DO_HARD)
				SET_BIT(MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_HACKING_MINIGAME_COMPLETED)
			ENDIF
			
			IF IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_OBJECTIVE_BLOCKER)
				CLEAR_BIT(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_OBJECTIVE_BLOCKER)
				PRINTLN("[RCC MISSION] PROCESS_PRE_MAIN_CHECKS - (3) Clearing - PBBOOL_OBJECTIVE_BLOCKER")
			ENDIF
			
			SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_PreventAutoShuffleToDriversSeat, FALSE)
			
			SET_BIT(MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_HACKING_MINIGAME_COMPLETED)
			
			PRINTLN("[RCC MISSION] MAINTAIN_PHONE_HACK_MINIGAME - AWARD GIVEN - HAS_PLAYER_BEAT_CIRCUIT_HACKING - TRUE")
			MC_playerBD[iLocalPart].iHackMGDialogue = -1
			
			increment_medal_objective_completed(iTeam, iRule)
			INCREMENT_LOCAL_PLAYER_SCORE_FOR_CURRENT_OBJECTIVE()
			
			STORE_MINIGAME_TIMER_FOR_TELEMETRY(0)
			STORE_MINIGAME_ATTEMPTS_FOR_TELEMETRY(0)
			PRINTLN("[RCC MISSION] HACKING (CIRCUIT BOARD) - STORE_MINIGAME_ATTEMPTS_FOR_TELEMETRY - SLOT 0")
			PRINTLN("[RCC MISSION] HACKING (CIRCUIT BOARD) - STORE_MINIGAME_TIMER_FOR_TELEMETRY( - SLOT 0")
			
			MC_PlayerBD[iLocalPart].iHackTime = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stMGTimer0)
			MC_PlayerBD[iLocalPart].inumHacks++
			PRINTLN("[RCC MISSION] inumHacks HEIST TELEMETRY CIRCUIT BOARD MINIGAME  -", MC_playerBD[iLocalPart].iNumHacks)
			
			#IF IS_DEBUG_BUILD
				bDebugCircuitHack = FALSE
			#ENDIF
			
			RETURN TRUE
			
		ELIF HAS_PLAYER_FAILED_CIRCUIT_HACKING(hackingMinigameData)
		OR SHOULD_FORCE_FAIL_HACK_MINIGAME()
			BROADCAST_HEIST_END_HACK_MINIGAME(ALL_PLAYERS_ON_SCRIPT(TRUE), FALSE, MC_playerBD[iLocalPart].iTeam)
			CLEANUP_AND_QUIT_CIRCUIT_HACKING_MINIGAME(hackingMinigameData, IS_BIT_SET(MC_serverBD_1.iServerHackMGBitset, SBBOOL_HACK_DO_HARD))
			
			SET_BIT(iLocalBoolCheck7, LBOOL7_HACKING_MINIGAME_COMPLETE)
			
			IF IS_BIT_SET(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_OBJECTIVE_BLOCKER)
				CLEAR_BIT(MC_playerBD[iPartToUse].iClientBitSet, PBBOOL_OBJECTIVE_BLOCKER)
				PRINTLN("[RCC MISSION] PROCESS_PRE_MAIN_CHECKS - (4) Clearing - PBBOOL_OBJECTIVE_BLOCKER")
			ENDIF
			
			SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_PreventAutoShuffleToDriversSeat,FALSE)
			
			SET_BIT(MC_playerBD[iLocalPart].iClientBitSet2, PBBOOL2_HACKING_MINIGAME_FAILED)
			MC_playerBD[iLocalPart].iHackMGDialogue = -1
			PRINTLN("[RCC MISSION] MAINTAIN_PHONE_HACK_MINIGAME2 - AWARD GIVEN - HAS_PLAYER_BEAT_CIRCUIT_HACKING - FALSE")
		
			INCREASE_MINIGAME_ATTEMPTS_FOR_TELEMETRY(0)
			PRINTLN("[RCC MISSION] HACKING (CIRCUIT BOARD) - INCREASE_MINIGAME_ATTEMPTS_FOR_TELEMETRY - SLOT 0")
			
			#IF IS_DEBUG_BUILD
				bDebugCircuitHack = FALSE
			#ENDIF
			
		ELSE
			//SET_BIT(MC_playerBD[iLocalPart].iClientBitSet, PBBOOL_OBJECTIVE_BLOCKER)
			IF NOT IS_THIS_ROCKSTAR_MISSION_GANGOPS_MISSION_OF_TYPE(ciGANGOPS_FLOW_MISSION_SERVERFARM) //TEMP TEXT
				CLEAR_ANY_OBJECTIVE_TEXT()
			ENDIF
			PRINTLN("[RCC MISSION] [MB] Clear_Any_Objective_Text() due to Hacking Minigame (MAINTAIN_LESTER_SNAKE_MINIGAME)")
		ENDIF
	ENDIF
		
	RETURN FALSE
ENDFUNC

PROC PROCESS_LESTER_SNAKE_GAME_MC(OBJECT_INDEX tempObj, INT iobj)
	
	IF MC_playerBD[iLocalPart].iObjHacked = -1
		IF MC_serverBD.iObjHackPart[iobj] = -1
			IF MC_playerBD[iLocalPart].iObjHacking = -1
				IF bLocalPlayerPedOk
					IF GET_DIST2_BETWEEN_ENTITIES(LocalPlayerPed, tempObj) < POW(2.0, 2.0)
					AND PLAYER_CAN_INTERACT_WITH_OBJ(iObj, tempObj)
						vGoToPointOffset = vHackKeypadGotoOffset
						
						VECTOR vAACoordOb 
						VECTOR vAACoordPro
						vAACoordOb = GET_ENTITY_COORDS(tempObj)
						vAACoordOb.z = vAACoordOb.z + fThisTweakValue1
						
						vAACoordPro = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(tempObj,vHackAAOffset)
						vAACoordPro.z = vAACoordPro.z - fThisTweakValue2
						
						//DRAW_DEBUG_ANGLED_AREA(vAACoordOb, vAACoordPro,1.000000,255,0,0,100)
						
						//vGoToPoint = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(tempObj,vGoToPointOffset)
						
						DRAW_DEBUG_CROSS(vGoToPoint,0.5,0,0,255,255)
						
						//IF IS_ENTITY_IN_ANGLED_AREA( LocalPlayerPed, vAACoordOb, vAACoordPro, 1.200000)
							IF NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
								IF NOT IS_PHONE_ONSCREEN()
								
									BOOL IsMiniGameBlocked
									IsMiniGameBlocked = IS_THE_MINIGAME_OBJECT_BLOCKED_BY_VEH(tempObj,1.0)
									
									IF NOT IsMiniGameBlocked
										DISPLAY_HELP_TEXT_THIS_FRAME("MC_HACK_1",TRUE)
									ENDIF
									DISABLE_CONTROL_ACTION(FRONTEND_CONTROL,INPUT_CONTEXT)
									IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CONTEXT)
										MC_playerBD[iLocalPart].iObjHacking = iobj
										SET_BIT(iLocalBoolCheck14, LBOOL14_ALLOW_LESTER_SNAKE_MINIGAME)
										SET_BIT(MC_serverBD_1.iServerHackMGBitset, SBBOOL_HACK_DO_HARD)
									ENDIF
									
								ENDIF
							ENDIF
						//ENDIF
					ELSE
						//If the minigame is running and we're too far away to do the hacking, clear it up
						IF IS_BIT_SET(hackingMinigameData.i_CircuitBitSet, BIT_SET_CIRCUIT_GAME_RUNNING)
							CLEANUP_AND_QUIT_CIRCUIT_HACKING_MINIGAME(hackingMinigameData, IS_BIT_SET(MC_serverBD_1.iServerHackMGBitset, SBBOOL_HACK_DO_HARD))
							PRINTLN("clearing up hacking minigame because too far away")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		
			IF IS_BIT_SET(iLocalBoolCheck7, LBOOL7_HACKING_MINIGAME_COMPLETE) //Clear this while the minigame isn't being played
				CLEAR_BIT(iLocalBoolCheck7, LBOOL7_HACKING_MINIGAME_COMPLETE)
				PRINTLN("[RCC MISSION] MAINTAIN_PHONE_HACK_MINIGAME - CLEAR_BIT(iLocalBoolCheck7, LBOOL7_HACKING_MINIGAME_COMPLETE)")
			ENDIF
		ELSE
			IF MC_serverBD.iObjHackPart[iobj] = iLocalPart AND bLocalPlayerOK
				IF MC_playerBD[iLocalPart].iObjHacked = -1
					IF MC_playerBD[iLocalPart].iObjHacking = iobj
					 
						IF MAINTAIN_LESTER_SNAKE_MINIGAME() // Runs the minigame and returns true if the player clears all of the levels
							CLEANUP_AND_QUIT_CIRCUIT_HACKING_MINIGAME(hackingMinigameData, IS_BIT_SET(MC_serverBD_1.iServerHackMGBitset, SBBOOL_HACK_DO_HARD))
							MC_playerBD[iLocalPart].iObjHacked = iobj
							RESET_OBJ_HACKING_INT()
							PRINTLN("PROCESS_LESTER_SNAKE_GAME_MC - I noticed you won the hacking minigame")
							
							CLEAR_BIT(iLocalBoolCheck14, LBOOL14_ALLOW_LESTER_SNAKE_MINIGAME)
						ENDIF
						IF IS_THIS_ROCKSTAR_MISSION_GANGOPS_MISSION_OF_TYPE(ciGANGOPS_FLOW_MISSION_SERVERFARM)
							Print_Objective_Text("TEMP_MG")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC MAKE_PLAYER_PED_HACK_ON_PHONE(OBJECT_INDEX tempObj)
	
	//Make ped look at phone
	FLOAT fHeadingTowardsObj = GET_HEADING_FROM_ENTITIES_LA(LocalPlayerPed, tempObj)
	//SET_PED_USING_ACTION_MODE(LocalPlayerPed, FALSE)
		
	IF NOT IS_PED_CLOSE_TO_HEADING(LocalPlayerPed, fHeadingTowardsObj, 20)
		IF NOT IS_PED_PERFORMING_TASK(LocalPlayerPed, SCRIPT_TASK_ACHIEVE_HEADING)
		AND NOT IS_PED_RUNNING_MOBILE_PHONE_TASK(LocalPlayerPed)
			TASK_ACHIEVE_HEADING(LocalPlayerPed, fHeadingTowardsObj, 2500)
		ENDIF
	ELSE
		SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_PhoneDisableTextingAnimations, FALSE)
		SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_PhoneDisableTalkingAnimations, FALSE)
		
		IF NOT IS_PED_RUNNING_MOBILE_PHONE_TASK(LocalPlayerPed)
		AND NOT HAS_NET_TIMER_STARTED(stHackPhoneTimer)
			
			//IF IS_PED_PERFORMING_TASK(LocalPlayerPed, SCRIPT_TASK_ACHIEVE_HEADING)
				//CLEAR_PED_TASKS_IMMEDIATELY(LocalPlayerPed)
			//ENDIF
			
			TASK_USE_MOBILE_PHONE(LocalPlayerPed, TRUE, Mode_ToText)
			
			IF g_bHideHackingVisuals
				START_NET_TIMER(stHackPhoneTimer)
			ENDIF
			
			CELL_HORIZONTAL_MODE_TOGGLE(TRUE)
			//GET_SCRIPT_TASK_STATUS(LocalPlayerPed, SCRIPT_TASK_USE_MOBILE_PHONE)
			//FORCE_PED_AI_AND_ANIMATION_UPDATE(LocalPlayerPed)
			PRINTLN("[RCC MISSION][BeamHackPhone] PROCESS_BEAM_HACK_GAME_MC - PHONE TASK GIVEN - TASK_USE_MOBILE_PHONE - bGotHackPhoneOut set to TRUE")
		ELSE
			CELL_HORIZONTAL_MODE_TOGGLE(TRUE)
			
			IF g_bHideHackingVisuals
				IF HAS_NET_TIMER_EXPIRED(stHackPhoneTimer, 3500)
					RESET_NET_TIMER(stHackPhoneTimer)
					PRINTLN("[BeamHackPhone]  resetting timer")
				ENDIF
			ENDIF
			//TASK_LOOK_AT_COORD(LocalPlayerPed, GET_PED_BONE_COORDS(LocalPlayerPed, BONETAG_PH_R_HAND, <<0,0,0>>), -1, SLF_DEFAULT)
		ENDIF
	ENDIF
ENDPROC

PROC UPDATE_GANGOPS_HACKING_TELEMETRY(INT iAttemptsToAdd, INT iTimeToAdd)
	g_sJobGangopsInfo.m_infos.m_minigameNumberOfTimes0 += iAttemptsToAdd
	g_sJobGangopsInfo.m_infos.m_minigameTimeTaken0 += iTimeToAdd
	PRINTLN("[JS][GOTEL][TEL] - UPDATE_GANGOPS_HACKING_TELEMETRY - TimeToAdd: ", iTimeToAdd, " AttemptsToAdd: ", iAttemptsToAdd, " Total Attempts: ", g_sJobHeistInfo.m_minigameNumberOfTimes0, " Total Time: ", g_sJobHeistInfo.m_minigameTimeTaken0)
ENDPROC

PROC CREATE_AND_START_SYNC_SCENE(VECTOR vPos, VECTOR vRot, STRING sDict, STRING sAnim, BOOL bLooping = FALSE)

	FLOAT fBlend = WALK_BLEND_IN
	
	IF bLooping
		fBlend = 1000
	ENDIF
	
	MC_playerBD[iLocalPart].iSynchSceneID = NETWORK_CREATE_SYNCHRONISED_SCENE(vPos, vRot, DEFAULT, DEFAULT, bLooping)
	NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(LocalPlayerPed, MC_playerBD[iLocalPart].iSynchSceneID, sDict, sAnim, REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_HIDE_WEAPON | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS, DEFAULT, fBlend)
	NETWORK_START_SYNCHRONISED_SCENE(MC_playerBD[iLocalPart].iSynchSceneID)
	
	PRINTLN("[BeamHackAnim] Sync scene has been created and started for anim ", sAnim, "!")
ENDPROC

PROC CLEAN_UP_PHYSICAL_BEAM_HACK_ANIM()
	bhaBeamHackAnimState = BHA_STATE_NONE
	STOP_SYNC_SCENE(MC_playerBD[iLocalPart].iSynchSceneID)
	CLEAR_PED_TASKS_IMMEDIATELY(LocalPlayerPed)
	TASK_PLAY_ANIM(LocalPlayerPed, "anim@GangOps@Facility@Servers@", "HOTWIRE_OUTRO", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, DEFAULT, AF_HIDE_WEAPON)
	PRINTLN("[BeamHackAnim] Stopping sync scene and playing HOTWIRE_OUTRO because our iObjHacking is -1 but we were still not in BHA_STATE_NONE")
	
	IF IS_SKYSWOOP_AT_GROUND()
		IF NETWORK_IS_GAME_IN_PROGRESS()
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			ENABLE_INTERACTION_MENU() 
			PRINTLN("[BeamHackAnim] Giving player control back!")
		ELSE
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			PRINTLN("[BeamHackAnim] Giving player control back!")
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck27, LBOOL27_WEAPON_HAS_BEEN_HIDDEN_BY_INTERACTION)
		IF sIWInfo.wtInteractWith_StoredWeapon != WEAPONTYPE_INVALID
			SET_CURRENT_PED_WEAPON(LocalPlayerPed, sIWInfo.wtInteractWith_StoredWeapon, TRUE)
		ENDIF
		
		CLEAR_BIT(iLocalBoolCheck27, LBOOL27_WEAPON_HAS_BEEN_HIDDEN_BY_INTERACTION)
		PRINTLN("[BeamHackAnim] Putting player's weapon back in their hand")
	ENDIF
ENDPROC

PROC HANDLE_PHYSICAL_BEAM_HACK_ANIM(OBJECT_INDEX tempObj, INT iobj)

	//ENTITY_INDEX eiHackObj = GET_CLOSEST_OBJECT_OF_TYPE(GET_ENTITY_COORDS(tempObj), 45, XM_BASE_CIA_SERVER_01, FALSE, FALSE, FALSE)
	ENTITY_INDEX eiHackObj = tempObj
	
	STRING sDict = "anim@GangOps@Facility@Servers@"
	STRING sAnim = "HOTWIRE"
	
	REQUEST_ANIM_DICT(sDict)
	
	IF NOT HAS_ANIM_DICT_LOADED(sDict)
		EXIT
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(eiHackObj)
		eiHackObj = GET_CLOSEST_OBJECT_OF_TYPE(GET_ENTITY_COORDS(tempObj), 45, XM_BASE_CIA_SERVER_02, FALSE, FALSE, FALSE)
		//ASSERTLN("NO eiHackObj")
		//EXIT
	ENDIF
	
	VECTOR vPos = GET_ENTITY_COORDS(eiHackObj)
	VECTOR vRot = GET_ENTITY_ROTATION(eiHackObj)
	
	//Corrections
	FLOAT fPosGroundZ = 0
	GET_GROUND_Z_FOR_3D_COORD(vPos, fPosGroundZ)
	
	VECTOR vPadForwards = GET_ENTITY_FORWARD_VECTOR(eiHackObj)
	vPos -= vPadForwards * (-0.600)
	
	vPos.z = (fPosGroundZ + 0.0)
	
	VECTOR vWalkToPos = GET_ANIM_INITIAL_OFFSET_POSITION(sDict, sAnim, vPos, vRot)
	VECTOR vWalkToRot = GET_ANIM_INITIAL_OFFSET_ROTATION(sDict, sAnim, vPos, vRot)
	FLOAT fWalkToHeading = vWalkToRot.z
	
	FLOAT fDist = VDIST2(vWalkToPos, GET_ENTITY_COORDS(LocalPlayerPed))
	FLOAT fDistAllowed = 1.25
	
	//DRAW_DEBUG_LINE(GET_ENTITY_COORDS(LocalPlayerPed), vPos, 255, 255, 255, 255)
	//DRAW_DEBUG_LINE(GET_ENTITY_COORDS(LocalPlayerPed), vWalkToPos)
	//DRAW_DEBUG_LINE(GET_ENTITY_COORDS(LocalPlayerPed), vWalkToPos, 0, 255, 0, 255)
	
	SWITCH bhaBeamHackAnimState
		CASE BHA_STATE_NONE
			
			IF MC_playerBD[iLocalPart].iObjHacking > -1
			AND NOT IS_PED_PERFORMING_TASK(LocalPlayerPed, SCRIPT_TASK_GO_STRAIGHT_TO_COORD)
				TASK_GO_STRAIGHT_TO_COORD(LocalPlayerPed, vWalkToPos, PEDMOVEBLENDRATIO_RUN, 6000, fWalkToHeading, 0.15)
				PRINTLN("[BeamHackAnim] BHA_STATE_NONE - Player has been tasked to walk early (for responsive controls)")	
			ENDIF
			
			IF MC_serverBD.iObjHackPart[iobj] = iLocalPart
			AND MC_playerBD[iLocalPart].iObjHacking > -1
				CLEAR_PED_TASKS(LocalPlayerPed)
				bhaBeamHackAnimState = BHA_STATE_WALKING
				CLEAR_BIT(iLocalBoolCheck26, LBOOL26_STARTED_ANIMATION_FOR_BEAM_HACK)
			ENDIF
		BREAK
		
		CASE BHA_STATE_WALKING
			IF NOT IS_BIT_SET(iLocalBoolCheck27, LBOOL27_WEAPON_HAS_BEEN_HIDDEN_BY_INTERACTION)
				GET_CURRENT_PED_WEAPON(LocalPlayerPed, sIWInfo.wtInteractWith_StoredWeapon)
				SET_CURRENT_PED_WEAPON(LocalPlayerPed, WEAPONTYPE_UNARMED, TRUE)
				SET_BIT(iLocalBoolCheck27, LBOOL27_WEAPON_HAS_BEEN_HIDDEN_BY_INTERACTION)
				PRINTLN("[BeamHackAnim] BHA_STATE_WALKING - Taking player's weapon away")
			ELSE
				IF NOT IS_PED_PERFORMING_TASK(LocalPlayerPed, SCRIPT_TASK_GO_STRAIGHT_TO_COORD)
					TASK_GO_STRAIGHT_TO_COORD(LocalPlayerPed, vWalkToPos, PEDMOVEBLENDRATIO_RUN, 6000, fWalkToHeading, 0.15)
					PRINTLN("[BeamHackAnim] BHA_STATE_WALKING - Player has been retasked to walk!")	
				ENDIF
				
				IF fDist <= (fDistAllowed * fDistAllowed)
					bhaBeamHackAnimState = BHA_STATE_ENTER
					PRINTLN("[BeamHackAnim] Player is now entering the BHA_STATE_ENTER state!")
				//ELSE
					//DRAW_DEBUG_SPHERE(vWalkToPos, fDistAllowed, 255, 255, 255, 125)
				ENDIF
			ENDIF
		BREAK
		
		CASE BHA_STATE_ENTER
			IF NOT IS_SYNC_SCENE_RUNNING(MC_playerBD[iLocalPart].iSynchSceneID)
				
				IF NOT IS_BIT_SET(iLocalBoolCheck26, LBOOL26_STARTED_ANIMATION_FOR_BEAM_HACK)
					CREATE_AND_START_SYNC_SCENE(vPos, vRot, sDict, "HOTWIRE_INTRO")
					SET_BIT(iLocalBoolCheck26, LBOOL26_STARTED_ANIMATION_FOR_BEAM_HACK)
				ELSE
					STOP_SYNC_SCENE(MC_playerBD[iLocalPart].iSynchSceneID)
					CLEAR_BIT(iLocalBoolCheck26, LBOOL26_STARTED_ANIMATION_FOR_BEAM_HACK)
					bhaBeamHackAnimState = BHA_STATE_LOOPING
					PRINTLN("[BeamHackAnim] BHA_STATE_ENTER - Player is now entering the BHA_STATE_LOOPING state!")
				ENDIF
				
			ENDIF
		BREAK
		
		CASE BHA_STATE_LOOPING
			IF NOT IS_SYNC_SCENE_RUNNING(MC_playerBD[iLocalPart].iSynchSceneID)
				
				IF NOT IS_BIT_SET(iLocalBoolCheck26, LBOOL26_STARTED_ANIMATION_FOR_BEAM_HACK)
					CREATE_AND_START_SYNC_SCENE(vPos, vRot, sDict, "HOTWIRE", TRUE)
					SET_BIT(iLocalBoolCheck26, LBOOL26_STARTED_ANIMATION_FOR_BEAM_HACK)
				ELSE
					//Did the rest in _main in case we aren't processing objects any more 
					
//					IF MC_serverBD.iObjHackPart[iobj] != iLocalPart // If we're not hacking any more, proceed to the exit stage
//						STOP_SYNC_SCENE(MC_playerBD[iLocalPart].iSynchSceneID)
//						CLEAR_BIT(iLocalBoolCheck26, LBOOL26_STARTED_ANIMATION_FOR_BEAM_HACK)
//						ASSERTLN("[BeamHackAnim] Player is now entering the BHA_STATE_EXIT state!")
//						bhaBeamHackAnimState = BHA_STATE_EXIT
//					ENDIF
				ENDIF
				
			ENDIF
		BREAK
		
		CASE BHA_STATE_EXIT
			IF NOT IS_SYNC_SCENE_RUNNING(MC_playerBD[iLocalPart].iSynchSceneID)
				
				IF NOT IS_BIT_SET(iLocalBoolCheck26, LBOOL26_STARTED_ANIMATION_FOR_BEAM_HACK)
					CREATE_AND_START_SYNC_SCENE(vPos, vRot, sDict, "HOTWIRE_OUTRO")
					SET_BIT(iLocalBoolCheck26, LBOOL26_STARTED_ANIMATION_FOR_BEAM_HACK)
				ELSE
					STOP_SYNC_SCENE(MC_playerBD[iLocalPart].iSynchSceneID)
					CLEAR_BIT(iLocalBoolCheck26, LBOOL26_STARTED_ANIMATION_FOR_BEAM_HACK)
					bhaBeamHackAnimState = BHA_STATE_NONE
					PRINTLN("[BeamHackAnim] BHA_STATE_EXIT - Player is now entering the BHA_STATE_NONE state!")
				ENDIF
				
			ENDIF
		BREAK
		
	ENDSWITCH
ENDPROC

PROC PROCESS_BEAM_HACK_GAME_MC(OBJECT_INDEX tempObj, INT iobj)
	
	HANDLE_PHYSICAL_BEAM_HACK_ANIM(tempObj, iObj)
	
	IF MC_playerBD[iLocalPart].iObjHacked = -1
		IF MC_serverBD.iObjHackPart[iobj] = -1
			IF MC_playerBD[iLocalPart].iObjHacking = -1
				IF IS_BIT_SET(iLocalBoolCheck26, LBOOL26_STARTED_BEAM_HACK)
					PRINTLN("[BeamHack] Cleaning up because iObjHackPart isn't me!")
					CLEAN_UP_BEAM_HACK_MINIGAME(sBeamHack[iobj])
					CLEAR_BIT(iLocalBoolCheck26, LBOOL26_STARTED_BEAM_HACK)
				ENDIF
				
				IF bLocalPlayerPedOk
					IF NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
						IF NOT IS_PHONE_ONSCREEN()
							IF GET_DIST2_BETWEEN_ENTITIES(LocalPlayerPed, tempObj) < POW(2.0, 2.0)
							AND PLAYER_CAN_INTERACT_WITH_OBJ(iobj, tempObj)
								IF NOT IS_THE_MINIGAME_OBJECT_BLOCKED_BY_VEH(tempObj,1.0)
									DISPLAY_HELP_TEXT_THIS_FRAME("MC_HACK_1",TRUE)
								ENDIF
								DISABLE_CONTROL_ACTION(FRONTEND_CONTROL,INPUT_CONTEXT)
								IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CONTEXT)
									MC_playerBD[iLocalPart].iObjHacking = iobj
									SET_BIT(iLocalBoolCheck26, LBOOL26_STARTED_BEAM_HACK)
									
									IF NETWORK_IS_GAME_IN_PROGRESS()
										NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
										PRINTLN("[BeamHackAnim] Taking player control away!")
										DISABLE_INTERACTION_MENU() 
									ELSE
										SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
										PRINTLN("[BeamHackAnim] Taking player control away!")
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			bhaBeamHackAnimState = BHA_STATE_NONE
		ELSE
			IF MC_serverBD.iObjHackPart[iobj] = iLocalPart AND bLocalPlayerOK
				IF MC_playerBD[iLocalPart].iObjHacked = -1
					IF MC_playerBD[iLocalPart].iObjHacking = iobj
					
						IF GET_DIST2_BETWEEN_ENTITIES(LocalPlayerPed, tempObj) < POW(2.0, 2.0)
							IF (bhaBeamHackAnimState = BHA_STATE_LOOPING OR IS_FAKE_MULTIPLAYER_MODE_SET())
								INT iNumPlays = GET_MP_INT_CHARACTER_STAT(MP_STAT_CR_BEAMHACK)
								PROCESS_BEAM_HACK_MINIGAME(sBeamHack[iobj], iNumPlays)
								IF IS_BIT_SET(sBeamHack[iobj].iBS, ciGOHACKBS_FIREWALL_HIT)
								OR IS_BIT_SET(sBeamHack[iobj].iBS, ciGOHACKBS_PACKET_HIT) 
									PRINTLN("[JS][Beamhack] FIREWALL/PACKET HIT DO DIALOGUE CHECK for obj ", iObj)
									SET_BIT(iLocalBoolCheck9,LBOOL9_DO_INSTANT_DIALOGUE_LOOP)
								ENDIF
								IF IS_BIT_SET(sBeamHack[iobj].iBS, ciGOHACKBS_PASSED)
									UPDATE_GANGOPS_HACKING_TELEMETRY(1, sBeamHack[iobj].iTimeElapsed)
									MC_playerBD[iLocalPart].iObjHacked = iobj
									RESET_OBJ_HACKING_INT()
									CLEAN_UP_BEAM_HACK_MINIGAME(sBeamHack[iobj])
									
									IF bhaBeamHackAnimState = BHA_STATE_LOOPING
									OR bhaBeamHackAnimState = BHA_STATE_ENTER
										bhaBeamHackAnimState = BHA_STATE_EXIT
									ENDIF
									
									CLEAR_BIT(iLocalBoolCheck26, LBOOL26_STARTED_BEAM_HACK)
									IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
										PRINTLN("[JS][Beamhack] Incrmenting MP_STAT_CR_BEAMHACK to: ", iNumPlays + 1)
										SET_MP_INT_CHARACTER_STAT(MP_STAT_CR_BEAMHACK, iNumPlays + 1)
									ENDIF
								ELIF IS_BIT_SET(sBeamHack[iobj].iBS, ciGOHACKBS_FAILED)
									MC_playerBD[iLocalPart].iHackingFails++
									UPDATE_GANGOPS_HACKING_TELEMETRY(1, sBeamHack[iobj].iTimeElapsed)
									RESET_OBJ_HACKING_INT()
									
									CLEAN_UP_BEAM_HACK_MINIGAME(sBeamHack[iobj])
									
									IF bhaBeamHackAnimState = BHA_STATE_LOOPING
									OR bhaBeamHackAnimState = BHA_STATE_ENTER
										bhaBeamHackAnimState = BHA_STATE_EXIT
									ENDIF
									
									CLEAR_BIT(iLocalBoolCheck26, LBOOL26_STARTED_BEAM_HACK)
								ELIF IS_BIT_SET(sBeamHack[iobj].iBS, ciGOHACKBS_QUIT)
									UPDATE_GANGOPS_HACKING_TELEMETRY(1, sBeamHack[iobj].iTimeElapsed)
									RESET_OBJ_HACKING_INT()
									CLEAN_UP_BEAM_HACK_MINIGAME(sBeamHack[iobj])
									
									CLEAN_UP_PHYSICAL_BEAM_HACK_ANIM()
									
									CLEAR_BIT(iLocalBoolCheck26, LBOOL26_STARTED_BEAM_HACK)
								ENDIF
							ELSE
								PRINTLN("[BeamHack][BeamHackAnim] Not processing minigame yet because we are not yet at BHA_STATE_LOOPING")
							ENDIF
						ELSE
							PRINTLN("[CannotBeamhack] out of range for OBJ ", iObj)
							RESET_OBJ_HACKING_INT()
							CLEAN_UP_BEAM_HACK_MINIGAME(sBeamHack[iobj])
							
							IF bhaBeamHackAnimState = BHA_STATE_LOOPING
							OR bhaBeamHackAnimState = BHA_STATE_ENTER
								bhaBeamHackAnimState = BHA_STATE_EXIT
							ENDIF
							
							CLEAR_BIT(iLocalBoolCheck26, LBOOL26_STARTED_BEAM_HACK)
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF MC_serverBD.iObjHackPart[iobj] != -1
				AND MC_serverBD.iObjHackPart[iobj] != iLocalPart
				AND MC_playerBD[iLocalPart].iObjHacking = iobj
					PRINTLN("[CannotBeamhack] someone else hacking OBJ ", iObj)
					RESET_OBJ_HACKING_INT()
					CLEAN_UP_BEAM_HACK_MINIGAME(sBeamHack[iobj])
					CLEAR_BIT(iLocalBoolCheck26, LBOOL26_STARTED_BEAM_HACK)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_HOTWIRE_GAME_MC(OBJECT_INDEX tempObj, INT iobj)

	PRINTLN("[CannotHotwire] Processing Hotwire Obj ", iobj, " --------------------------------------------------")
	IF MC_playerBD[iLocalPart].iObjHacked = -1
		IF MC_serverBD.iObjHackPart[iobj] = -1
			IF MC_playerBD[iLocalPart].iObjHacking = -1
				IF bLocalPlayerPedOk
					IF NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
						IF NOT IS_PHONE_ONSCREEN()
						AND NOT IS_HUD_COMPONENT_ACTIVE(NEW_HUD_WEAPON_WHEEL)
							IF GET_DIST2_BETWEEN_ENTITIES(LocalPlayerPed, tempObj) < POW(2.0, 2.0)
							AND PLAYER_CAN_INTERACT_WITH_OBJ(iobj, tempObj)
								
								//IF NOT IS_THE_MINIGAME_OBJECT_BLOCKED_BY_VEH(tempObj,1.0)
								DISPLAY_HELP_TEXT_THIS_FRAME("MC_HACK_SERVER",TRUE)
								//ENDIF
								DISABLE_CONTROL_ACTION(FRONTEND_CONTROL,INPUT_CONTEXT)
								DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_SELECT_WEAPON)
								IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CONTEXT)
									MC_playerBD[iLocalPart].iObjHacking = iobj
									SET_BIT(iLocalBoolCheck26, LBOOL26_STARTED_HOTWIRE_HACK)
								ENDIF
							ELSE
								PRINTLN("[CannotHotwire] Too far or can't interact with for OBJ ", iObj)
							ENDIF
						ELSE
							PRINTLN("[CannotHotwire] Phone or weapon wheel for OBJ ", iObj)
						ENDIF
					ELSE
						PRINTLN("[CannotHotwire] Player is in a vehicle", " for OBJ for OBJ ", iObj)
					ENDIF
				ELSE
					PRINTLN("[CannotHotwire] Player ped is not okay", " for OBJ for OBJ ", iObj)
				ENDIF
			ELSE
				PRINTLN("[CannotHotwire] iObjHacking is ", MC_playerBD[iLocalPart].iObjHacking, " for OBJ ", iObj)
			ENDIF
		ELSE
			IF MC_serverBD.iObjHackPart[iobj] = iLocalPart AND bLocalPlayerOK
				IF MC_playerBD[iLocalPart].iObjHacked = -1
					IF MC_playerBD[iLocalPart].iObjHacking = iobj
					
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MC_HACK_SERVER")
							CLEAR_HELP(TRUE)
						ENDIF
						
						IF GET_DIST2_BETWEEN_ENTITIES(LocalPlayerPed, tempObj) < POW(2.0, 2.0)
						
							MAKE_PLAYER_PED_HACK_ON_PHONE(tempObj)
							
							PROCESS_HOTWIRE_MINIGAME(sHotwire[iobj])
							
							IF IS_BIT_SET(sHotwire[iobj].iBS, ciGOHACKBS_PASSED)
								MC_playerBD[iLocalPart].iObjHacked = iobj
								RESET_OBJ_HACKING_INT()
								UPDATE_GANGOPS_HACKING_TELEMETRY(1, sHotwire[iobj].iTimeElapsed)
								CLEAN_UP_HOTWIRE_MINIGAME(sHotwire[iobj])
								CLEAR_BIT(iLocalBoolCheck26, LBOOL26_STARTED_HOTWIRE_HACK)
							ELIF IS_BIT_SET(sHotwire[iobj].iBS, ciGOHACKBS_FAILED)
								MC_playerBD[iLocalPart].iHackingFails++
								RESET_OBJ_HACKING_INT()
								CLEAN_UP_HOTWIRE_MINIGAME(sHotwire[iobj])
								CLEAR_BIT(iLocalBoolCheck26, LBOOL26_STARTED_HOTWIRE_HACK)
							ELIF IS_BIT_SET(sHotwire[iobj].iBS, ciGOHACKBS_QUIT)
								UPDATE_GANGOPS_HACKING_TELEMETRY(1, sHotwire[iobj].iTimeElapsed)
								RESET_OBJ_HACKING_INT()
								CLEAN_UP_HOTWIRE_MINIGAME(sHotwire[iobj])
								CLEAR_BIT(iLocalBoolCheck26, LBOOL26_STARTED_HOTWIRE_HACK)
							ENDIF
						ELSE
							PRINTLN("[CannotHotwire] out of range for OBJ ", iObj)
							RESET_OBJ_HACKING_INT()
							CLEAN_UP_HOTWIRE_MINIGAME(sHotwire[iobj])
							CLEAR_BIT(iLocalBoolCheck26, LBOOL26_STARTED_HOTWIRE_HACK)
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF MC_serverBD.iObjHackPart[iobj] != -1
				AND MC_serverBD.iObjHackPart[iobj] != iLocalPart
				AND MC_playerBD[iLocalPart].iObjHacking = iobj
					PRINTLN("[CannotHotwire] someone else hacking OBJ ", iObj)
					RESET_OBJ_HACKING_INT()
					CLEAN_UP_HOTWIRE_MINIGAME(sHotwire[iobj])
					CLEAR_BIT(iLocalBoolCheck26, LBOOL26_STARTED_HOTWIRE_HACK)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//_______________________________________________
//Fingerprint Keypad Hacking
//_______________________________________________

PROC CREATE_AND_START_SYNC_SCENE_FINGERPRINT_KEYPAD_HACK(VECTOR vPos, VECTOR vRot, STRING sDict, STRING sAnim, STRING sPhoneAnim, STRING sHackingDeviceAnim, BOOL bLooping = FALSE, FLOAT fBlendIn = REALLY_SLOW_BLEND_IN, FLOAT fBlendOut = REALLY_SLOW_BLEND_OUT, FLOAT fBlend = WALK_BLEND_IN)
	
	IF bLooping
	AND fBlend = WALK_BLEND_IN
		fBlend = 1000
	ENDIF
	
	vFingerprintKeypadHackAnimLastPos = vPos
	vFingerprintKeypadHackAnimLastRot = vRot
	
	STOP_SYNC_SCENE(MC_playerBD[iLocalPart].iSynchSceneID)
	
	MC_playerBD[iLocalPart].iSynchSceneID = NETWORK_CREATE_SYNCHRONISED_SCENE(vPos, vRot, DEFAULT, DEFAULT, bLooping)
	
	NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(LocalPlayerPed, MC_playerBD[iLocalPart].iSynchSceneID, sDict, sAnim, fBlendIn, fBlendOut, SYNCED_SCENE_HIDE_WEAPON | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS, DEFAULT, fBlend)
	
	NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_OBJ(MC_playerBD[iLocalPart].netMiniGameObj), MC_playerBD[iLocalPart].iSynchSceneID, sDict, sHackingDeviceAnim, INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
	NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(NET_TO_OBJ(MC_playerBD[iLocalPart].netMiniGameObj2), MC_playerBD[iLocalPart].iSynchSceneID, sDict, sPhoneAnim, INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
	
	NETWORK_START_SYNCHRONISED_SCENE(MC_playerBD[iLocalPart].iSynchSceneID)
	
	PRINTLN("[FingerprintKeypadHackAnim] Sync scene has been created and started for anim ", sAnim, "! At vPos << ", vPos.x, ", ", vPos.y, ", ", vPos.z, " >>, vRot << ", vRot.x, ", ", vRot.y, ", ", vRot.z, " >>")
ENDPROC

PROC CLEAN_UP_PHYSICAL_FINGERPRINT_KEYPAD_HACK_ANIM()
	PRINTLN("[FingerprintKeypadHackAnim] CLEAN_UP_PHYSICAL_FINGERPRINT_KEYPAD_HACK_ANIM()")
	
	fkhaFingerprintKeypadHackAnimState = FKHA_STATE_NONE
	iFingerprintKeypadForCurrentAnim = -1
	STOP_SYNC_SCENE(MC_playerBD[iLocalPart].iSynchSceneID)
	CLEAN_UP_MINIGAME_OBJECTS()
	CLEAR_BIT(iLocalBoolCheck32, LBOOL32_QUIT_KEYPAD_HACK_MANUALLY)
	
	IF IS_SKYSWOOP_AT_GROUND()
		IF NETWORK_IS_GAME_IN_PROGRESS()
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			ENABLE_INTERACTION_MENU() 
			PRINTLN("[FingerprintKeypadHackAnim] Giving player control back!")
		ELSE
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			PRINTLN("[FingerprintKeypadHackAnim] Giving player control back!")
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iLocalBoolCheck27, LBOOL27_WEAPON_HAS_BEEN_HIDDEN_BY_INTERACTION)
		IF sIWInfo.wtInteractWith_StoredWeapon != WEAPONTYPE_INVALID
			SET_CURRENT_PED_WEAPON(LocalPlayerPed, sIWInfo.wtInteractWith_StoredWeapon, TRUE)
		ENDIF
		
		CLEAR_BIT(iLocalBoolCheck27, LBOOL27_WEAPON_HAS_BEEN_HIDDEN_BY_INTERACTION)
		PRINTLN("[FingerprintKeypadHackAnim] Putting player's weapon back in their hand")
	ENDIF
	
ENDPROC

PROC HANDLE_FINGERPRINT_KEYPAD_EXIT_HIDING_PROPS()
	
	//Hacking device
	IF IS_ENTITY_VISIBLE_TO_SCRIPT(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj))
		IF HAS_ANIM_EVENT_FIRED(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj), HASH("DELETE"))
			SET_ENTITY_VISIBLE(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj), FALSE)
		ENDIF
	ENDIF

	//Fake phone
	IF IS_ENTITY_VISIBLE_TO_SCRIPT(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj2))
		IF HAS_ANIM_EVENT_FIRED(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj2), HASH("DELETE"))
			SET_ENTITY_VISIBLE(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj2), FALSE)
		ENDIF
	ENDIF

ENDPROC

PROC HANDLE_PHYSICAL_FINGERPRINT_KEYPAD_HACK_ANIM(OBJECT_INDEX tempObj, INT iobj)
	
	ENTITY_INDEX eiHackObj = tempObj
	STRING sDict
	
	IF IS_PED_WEARING_HIGH_HEELS(LocalPlayerPed)
		sDict = "anim_heist@hs3f@ig1_hack_keypad@female@"
	ELSE
		sDict = "anim_heist@hs3f@ig1_hack_keypad@male@"
	ENDIF
	
	STRING sAnim
	STRING sPhoneAnim
	STRING sHackingDeviceAnim
	
	IF IS_BIT_SET(iLocalBoolCheck32, LBOOL32_FINGERPRINT_HACK_ANIM_USE_ALT)
		sAnim = "action_var_02"
		sPhoneAnim = "action_var_02_prop_phone_ing"
		sHackingDeviceAnim = "action_var_02_ch_prop_ch_usb_drive01x"
	ELSE
		sAnim = "action_var_01"
		sPhoneAnim = "action_var_01_prop_phone_ing"
		sHackingDeviceAnim = "action_var_01_ch_prop_ch_usb_drive01x"
	ENDIF
	
	REQUEST_ANIM_DICT(sDict)
	
	IF NOT HAS_ANIM_DICT_LOADED(sDict)
		EXIT
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(eiHackObj)
		eiHackObj = GET_CLOSEST_OBJECT_OF_TYPE(GET_ENTITY_COORDS(tempObj), 45, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].mn, FALSE, FALSE, FALSE)
	ENDIF
	
	VECTOR vOffset = << 0.0, 0.0, 0.0 >>
	VECTOR vRot = GET_ENTITY_ROTATION(eiHackObj)
	VECTOR vPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(eiHackObj, vOffset)
	VECTOR vPosZOffset = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(eiHackObj, <<0.0, -0.86, 0.0>>)
	VECTOR vWalkToPos = GET_ANIM_INITIAL_OFFSET_POSITION(sDict, sAnim, vPos, vRot)
	VECTOR vWalkToRot = GET_ANIM_INITIAL_OFFSET_ROTATION(sDict, sAnim, vPos, vRot)
	FLOAT fPosGroundZ = 0
	
	IF GET_GROUND_Z_FOR_3D_COORD((vPosZOffset + << 0.0, 0.0, 0.5>>), fPosGroundZ)
		vWalkToPos.z = fPosGroundZ
		//Adjust Z of scene node to match ground pos, in case there are slight differences in where the creator objects were placed.
		vPos.z = vWalkToPos.z + 1.36
	ELSE
		//If ground check fails then use the keypad height as a fallback.
		fPosGroundZ = vPos.z
		vWalkToPos.z = fPosGroundZ
		vPos.z = vWalkToPos.z
	ENDIF
	
	FLOAT fWalkToHeading = vWalkToRot.z
	
	FLOAT fDist = VDIST2(vWalkToPos, GET_ENTITY_COORDS(LocalPlayerPed))
	FLOAT fDistAllowed = 1.25
	
	SWITCH fkhaFingerprintKeypadHackAnimState
		CASE FKHA_STATE_NONE
			
			IF MC_playerBD[iLocalPart].iObjHacking > -1
			AND MC_playerBD[iLocalPart].iObjHacking = iObj
			AND NOT IS_PED_PERFORMING_TASK(LocalPlayerPed, SCRIPT_TASK_GO_STRAIGHT_TO_COORD)
				TASK_GO_STRAIGHT_TO_COORD(LocalPlayerPed, vWalkToPos, PEDMOVEBLENDRATIO_RUN, 6000, fWalkToHeading, 0.15)
				PRINTLN("[FingerprintKeypadHackAnim] FKHA_STATE_NONE - Player has been tasked to walk early (for responsive controls)")	
			ENDIF
			
			IF MC_serverBD.iObjHackPart[iobj] = iLocalPart
			AND MC_playerBD[iLocalPart].iObjHacking > -1
				CLEAR_PED_TASKS(LocalPlayerPed)
				fkhaFingerprintKeypadHackAnimState = FKHA_STATE_WALKING
				CLEAR_BIT(iLocalBoolCheck31, LBOOL31_STARTED_ANIM_FOR_FINGERPRINT_KEYPAD_HACK)
			ENDIF
		BREAK
		
		CASE FKHA_STATE_WALKING
			IF NOT IS_BIT_SET(iLocalBoolCheck27, LBOOL27_WEAPON_HAS_BEEN_HIDDEN_BY_INTERACTION)
				GET_CURRENT_PED_WEAPON(LocalPlayerPed, sIWInfo.wtInteractWith_StoredWeapon)
				SET_CURRENT_PED_WEAPON(LocalPlayerPed, WEAPONTYPE_UNARMED, TRUE)
				SET_BIT(iLocalBoolCheck27, LBOOL27_WEAPON_HAS_BEEN_HIDDEN_BY_INTERACTION)
				PRINTLN("[FingerprintKeypadHackAnim] FKHA_STATE_WALKING - Taking player's weapon away")
			ELSE
				IF NOT IS_PED_PERFORMING_TASK(LocalPlayerPed, SCRIPT_TASK_GO_STRAIGHT_TO_COORD)
					TASK_GO_STRAIGHT_TO_COORD(LocalPlayerPed, vWalkToPos, PEDMOVEBLENDRATIO_RUN, 6000, fWalkToHeading, 0.15)
					PRINTLN("[FingerprintKeypadHackAnim] FKHA_STATE_WALKING - Player has been retasked to walk!")
				ENDIF
				
				IF fDist <= (fDistAllowed * fDistAllowed)
				AND CREATE_HACKING_KEYCARD_OBJECT(FALSE, FALSE, INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_usb_drive01x")))
				AND CREATE_FAKE_PHONE_FOR_HACKING_ANIMS(FALSE, PROP_PHONE_ING, FALSE)
					fkhaFingerprintKeypadHackAnimState = FKHA_STATE_ENTER
					PRINTLN("[FingerprintKeypadHackAnim] Player is now entering the FKHA_STATE_ENTER state!")
				//ELSE
					//DRAW_DEBUG_SPHERE(vWalkToPos, 0.25, 255, 255, 255, 125)
				ENDIF
			ENDIF
		BREAK
		
		CASE FKHA_STATE_ENTER
			
			IF IS_BIT_SET(iLocalBoolCheck32, LBOOL32_FINGERPRINT_HACK_ANIM_USE_ALT)
				sAnim = "action_var_02"
				sPhoneAnim = "action_var_02_prop_phone_ing"
				sHackingDeviceAnim = "action_var_02_ch_prop_ch_usb_drive01x"
			ELSE
				sAnim = "action_var_01"
				sPhoneAnim = "action_var_01_prop_phone_ing"
				sHackingDeviceAnim = "action_var_01_ch_prop_ch_usb_drive01x"
			ENDIF
			
			IF NOT IS_BIT_SET(iLocalBoolCheck31, LBOOL31_STARTED_ANIM_FOR_FINGERPRINT_KEYPAD_HACK)
				CREATE_AND_START_SYNC_SCENE_FINGERPRINT_KEYPAD_HACK(vPos, vRot, sDict, sAnim, sPhoneAnim, sHackingDeviceAnim, FALSE, DEFAULT, DEFAULT)
				SET_BIT(iLocalBoolCheck31, LBOOL31_STARTED_ANIM_FOR_FINGERPRINT_KEYPAD_HACK)
			ELIF NOT IS_SYNC_SCENE_RUNNING(MC_playerBD[iLocalPart].iSynchSceneID)
			OR IS_PAST_SYNC_SCENE_PHASE(MC_playerBD[iLocalPart].iSynchSceneID, 0.98)
				CLEAR_BIT(iLocalBoolCheck31, LBOOL31_STARTED_ANIM_FOR_FINGERPRINT_KEYPAD_HACK)
				
				IF GET_RANDOM_INT_IN_RANGE(0, 2) > 0
					SET_BIT(iLocalBoolCheck32, LBOOL32_FINGERPRINT_HACK_ANIM_USE_ALT)
				ELSE
					CLEAR_BIT(iLocalBoolCheck32, LBOOL32_FINGERPRINT_HACK_ANIM_USE_ALT)
				ENDIF
				
				fkhaFingerprintKeypadHackAnimState = FKHA_STATE_LOOPING
				PRINTLN("[FingerprintKeypadHackAnim] FKHA_STATE_ENTER - Player is now entering the FKHA_STATE_LOOPING state!")
			ELSE
				//Hacking device visibility
				IF NOT IS_ENTITY_VISIBLE_TO_SCRIPT(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj))
					IF HAS_ANIM_EVENT_FIRED(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj), HASH("CREATE"))
						SET_ENTITY_VISIBLE(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj), TRUE)
					ENDIF
				ENDIF
				
				//Fake phone visibility
				IF NOT IS_ENTITY_VISIBLE_TO_SCRIPT(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj2))
					IF HAS_ANIM_EVENT_FIRED(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj2), HASH("CREATE"))
						SET_ENTITY_VISIBLE(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj2), TRUE)
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE FKHA_STATE_LOOPING
			
			IF IS_BIT_SET(iLocalBoolCheck32, LBOOL32_FINGERPRINT_HACK_ANIM_USE_ALT)
				sAnim = "hack_loop_var_02"
				sPhoneAnim = "hack_loop_var_02_prop_phone_ing"
				sHackingDeviceAnim = "hack_loop_var_02_ch_prop_ch_usb_drive01x"
			ELSE
				sAnim = "hack_loop_var_01"
				sPhoneAnim = "hack_loop_var_01_prop_phone_ing"
				sHackingDeviceAnim = "hack_loop_var_01_ch_prop_ch_usb_drive01x"
			ENDIF
		
			IF NOT IS_BIT_SET(iLocalBoolCheck31, LBOOL31_STARTED_ANIM_FOR_FINGERPRINT_KEYPAD_HACK) 
				CREATE_AND_START_SYNC_SCENE_FINGERPRINT_KEYPAD_HACK(vPos, vRot, sDict, sAnim, sPhoneAnim, sHackingDeviceAnim, TRUE, FAST_BLEND_IN, DEFAULT, INSTANT_BLEND_IN)
				SET_BIT(iLocalBoolCheck31, LBOOL31_STARTED_ANIM_FOR_FINGERPRINT_KEYPAD_HACK)
			ELIF NOT IS_SYNC_SCENE_RUNNING(MC_playerBD[iLocalPart].iSynchSceneID)
				//Did the rest in _main which calls HANDLE_PHYSICAL_FINGERPRINT_KEYPAD_HACK_ANIM_FALLBACK_CLEANUP() in case we aren't processing objects any more. 
				// e.g. success and moves onto the next rule.
				
			ENDIF
		BREAK
		
		//In case of fail, return to an idle loop
		CASE FKHA_STATE_BACK_OUT 
			
			sAnim = "fail_react"
			sPhoneAnim = "fail_react_prop_phone_ing"
			sHackingDeviceAnim = "fail_react_ch_prop_ch_usb_drive01x"
			
			IF NOT IS_BIT_SET(iLocalBoolCheck31, LBOOL31_STARTED_ANIM_FOR_FINGERPRINT_KEYPAD_HACK)
				CREATE_AND_START_SYNC_SCENE_FINGERPRINT_KEYPAD_HACK(vPos, vRot, sDict, sAnim, sPhoneAnim, sHackingDeviceAnim, FALSE, FAST_BLEND_IN, DEFAULT, INSTANT_BLEND_IN)
				SET_BIT(iLocalBoolCheck31, LBOOL31_STARTED_ANIM_FOR_FINGERPRINT_KEYPAD_HACK)
			ELIF NOT IS_SYNC_SCENE_RUNNING(MC_playerBD[iLocalPart].iSynchSceneID)
			OR IS_PAST_SYNC_SCENE_PHASE(MC_playerBD[iLocalPart].iSynchSceneID, 0.98)
				CLEAR_BIT(iLocalBoolCheck31, LBOOL31_STARTED_ANIM_FOR_FINGERPRINT_KEYPAD_HACK)
				DISABLE_INTERACTION_MENU() 
				fkhaFingerprintKeypadHackAnimState = FKHA_STATE_LOOP_PRE_EXIT
				PRINTLN("[FingerprintKeypadHackAnim] FKHA_STATE_BACK_OUT - Player is now entering the FKHA_STATE_LOOP_PRE_EXIT state!")
			ELSE
				//Fake phone hide
				IF IS_ENTITY_VISIBLE_TO_SCRIPT(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj2))
					IF HAS_ANIM_EVENT_FIRED(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj2), HASH("DELETE"))
						SET_ENTITY_VISIBLE(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj2), FALSE)
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		//Player decides to fully quit or reattempt the hack
		CASE FKHA_STATE_LOOP_PRE_EXIT
		
			sAnim = "wait_idle"
			sPhoneAnim = "wait_idle_prop_phone_ing"
			sHackingDeviceAnim = "wait_idle_ch_prop_ch_usb_drive01x"
			
			IF NOT IS_BIT_SET(iLocalBoolCheck31, LBOOL31_STARTED_ANIM_FOR_FINGERPRINT_KEYPAD_HACK)
				CREATE_AND_START_SYNC_SCENE_FINGERPRINT_KEYPAD_HACK(vPos, vRot, sDict, sAnim, sPhoneAnim, sHackingDeviceAnim, TRUE, FAST_BLEND_IN, DEFAULT, INSTANT_BLEND_IN)
				SET_BIT(iLocalBoolCheck31, LBOOL31_STARTED_ANIM_FOR_FINGERPRINT_KEYPAD_HACK)
			ENDIF
		BREAK
		
		CASE FKHA_STATE_REATTEMPT
			
			sAnim = "reattempt"
			sPhoneAnim = "reattempt_prop_phone_ing"
			sHackingDeviceAnim = "reattempt_ch_prop_ch_usb_drive01x"
			
			IF NOT IS_BIT_SET(iLocalBoolCheck31, LBOOL31_STARTED_ANIM_FOR_FINGERPRINT_KEYPAD_HACK)
				CREATE_AND_START_SYNC_SCENE_FINGERPRINT_KEYPAD_HACK(vPos, vRot, sDict, sAnim, sPhoneAnim, sHackingDeviceAnim, FALSE, FAST_BLEND_IN, DEFAULT, INSTANT_BLEND_IN)
				SET_BIT(iLocalBoolCheck31, LBOOL31_STARTED_ANIM_FOR_FINGERPRINT_KEYPAD_HACK)
			ELIF NOT IS_SYNC_SCENE_RUNNING(MC_playerBD[iLocalPart].iSynchSceneID)
			OR IS_PAST_SYNC_SCENE_PHASE(MC_playerBD[iLocalPart].iSynchSceneID, 0.98)
				CLEAR_BIT(iLocalBoolCheck31, LBOOL31_STARTED_ANIM_FOR_FINGERPRINT_KEYPAD_HACK)
				fkhaFingerprintKeypadHackAnimState = FKHA_STATE_LOOPING
				PRINTLN("[FingerprintKeypadHackAnim] FKHA_STATE_LOOPING - Player is now entering the FKHA_STATE_LOOPING state!")
			ELSE
				//Fake phone reappear
				IF NOT IS_ENTITY_VISIBLE_TO_SCRIPT(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj2))
					IF HAS_ANIM_EVENT_FIRED(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj2), HASH("CREATE"))
						SET_ENTITY_VISIBLE(NET_TO_OBJ(MC_playerBD[iPartToUse].netMiniGameObj2), TRUE)
					ENDIF
				ENDIF
			ENDIF	
		BREAK
		
		//In case player manually chooses to exit.
		CASE FKHA_STATE_QUICK_EXIT
			
			sAnim = "fail_react_quick"
			sPhoneAnim = "fail_react_quick_prop_phone_ing"
			sHackingDeviceAnim = "fail_react_quick_ch_prop_ch_usb_drive01x"
			
			IF NOT IS_BIT_SET(iLocalBoolCheck31, LBOOL31_STARTED_ANIM_FOR_FINGERPRINT_KEYPAD_HACK)
				CREATE_AND_START_SYNC_SCENE_FINGERPRINT_KEYPAD_HACK(vPos, vRot, sDict, sAnim, sPhoneAnim, sHackingDeviceAnim, FALSE, FAST_BLEND_IN, FAST_BLEND_OUT, INSTANT_BLEND_IN)
				SET_BIT(iLocalBoolCheck31, LBOOL31_STARTED_ANIM_FOR_FINGERPRINT_KEYPAD_HACK)
			ELIF NOT IS_SYNC_SCENE_RUNNING(MC_playerBD[iLocalPart].iSynchSceneID)
			OR (HAS_ANIM_EVENT_FIRED(LocalPlayerPed, HASH("BREAKOUT")) AND IS_PLAYER_PRESSING_MOVEMENT_INPUT_DURING_EXIT())
				CLEAR_BIT(iLocalBoolCheck31, LBOOL31_STARTED_ANIM_FOR_FINGERPRINT_KEYPAD_HACK)
				CLEAN_UP_PHYSICAL_FINGERPRINT_KEYPAD_HACK_ANIM()
				fkhaFingerprintKeypadHackAnimState = FKHA_STATE_NONE
				PRINTLN("[FingerprintKeypadHackAnim] FKHA_STATE_QUICK_EXIT - Player is now entering the FKHA_STATE_NONE state!")
			ELSE
				HANDLE_FINGERPRINT_KEYPAD_EXIT_HIDING_PROPS()
			ENDIF
		BREAK
		
		CASE FKHA_STATE_EXIT
			DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
			
			sAnim = "exit"
			sPhoneAnim = "exit_prop_phone_ing"
			sHackingDeviceAnim = "exit_ch_prop_ch_usb_drive01x"
			
			IF NOT IS_BIT_SET(iLocalBoolCheck31, LBOOL31_STARTED_ANIM_FOR_FINGERPRINT_KEYPAD_HACK)
				CREATE_AND_START_SYNC_SCENE_FINGERPRINT_KEYPAD_HACK(vPos, vRot, sDict, sAnim, sPhoneAnim, sHackingDeviceAnim, FALSE, FAST_BLEND_IN, FAST_BLEND_OUT, INSTANT_BLEND_IN)
				SET_BIT(iLocalBoolCheck31, LBOOL31_STARTED_ANIM_FOR_FINGERPRINT_KEYPAD_HACK)
			ELIF NOT IS_SYNC_SCENE_RUNNING(MC_playerBD[iLocalPart].iSynchSceneID)
			OR (HAS_ANIM_EVENT_FIRED(LocalPlayerPed, HASH("BREAKOUT")) AND IS_PLAYER_PRESSING_MOVEMENT_INPUT_DURING_EXIT())
				CLEAR_BIT(iLocalBoolCheck31, LBOOL31_STARTED_ANIM_FOR_FINGERPRINT_KEYPAD_HACK)
				CLEAN_UP_PHYSICAL_FINGERPRINT_KEYPAD_HACK_ANIM()
				fkhaFingerprintKeypadHackAnimState = FKHA_STATE_NONE
				PRINTLN("[FingerprintKeypadHackAnim] FKHA_STATE_EXIT - Player is now entering the FKHA_STATE_NONE state!")
			ELSE
				HANDLE_FINGERPRINT_KEYPAD_EXIT_HIDING_PROPS()
			ENDIF
			
		BREAK
		
		CASE FKHA_STATE_SUCCESS_EXIT
		
			IF IS_BIT_SET(iLocalBoolCheck32, LBOOL32_FINGERPRINT_HACK_ANIM_USE_ALT)
				sAnim = "success_react_exit_var_02"
				sPhoneAnim = "success_react_exit_var_02_prop_phone_ing"
				sHackingDeviceAnim = "success_react_exit_var_02_ch_prop_ch_usb_drive01x"
			ELSE
				sAnim = "success_react_exit_var_01"
				sPhoneAnim = "success_react_exit_var_01_prop_phone_ing"
				sHackingDeviceAnim = "success_react_exit_var_01_ch_prop_ch_usb_drive01x"
			ENDIF
			
			IF NOT IS_BIT_SET(iLocalBoolCheck31, LBOOL31_STARTED_ANIM_FOR_FINGERPRINT_KEYPAD_HACK)
				CREATE_AND_START_SYNC_SCENE_FINGERPRINT_KEYPAD_HACK(vPos, vRot, sDict, sAnim, sPhoneAnim, sHackingDeviceAnim, FALSE, FAST_BLEND_IN, FAST_BLEND_OUT, INSTANT_BLEND_IN)
				SET_BIT(iLocalBoolCheck31, LBOOL31_STARTED_ANIM_FOR_FINGERPRINT_KEYPAD_HACK)
			ELIF NOT IS_SYNC_SCENE_RUNNING(MC_playerBD[iLocalPart].iSynchSceneID)
			OR (HAS_ANIM_EVENT_FIRED(LocalPlayerPed, HASH("BREAKOUT")) AND IS_PLAYER_PRESSING_MOVEMENT_INPUT_DURING_EXIT())
				CLEAR_BIT(iLocalBoolCheck31, LBOOL31_STARTED_ANIM_FOR_FINGERPRINT_KEYPAD_HACK)
				CLEAN_UP_PHYSICAL_FINGERPRINT_KEYPAD_HACK_ANIM()
				fkhaFingerprintKeypadHackAnimState = FKHA_STATE_NONE
				PRINTLN("[FingerprintKeypadHackAnim] FKHA_STATE_SUCCESS_EXIT - Player is now entering the FKHA_STATE_NONE state!")
			ELSE
				HANDLE_FINGERPRINT_KEYPAD_EXIT_HIDING_PROPS()
			ENDIF
			
		BREAK
	ENDSWITCH

ENDPROC

// Ends the hacking anim gracefully if no longer processing objects.
PROC HANDLE_PHYSICAL_FINGERPRINT_KEYPAD_HACK_ANIM_FALLBACK_CLEANUP()
	
	STRING sDict
	
	IF IS_PED_WEARING_HIGH_HEELS(LocalPlayerPed)
		sDict = "anim_heist@hs3f@ig1_hack_keypad@female@"
	ELSE
		sDict = "anim_heist@hs3f@ig1_hack_keypad@male@"
	ENDIF
	
	STRING sAnim
	STRING sPhoneAnim
	STRING sHackingDeviceAnim 
	
	IF fkhaFingerprintKeypadHackAnimState = FKHA_STATE_SUCCESS_EXIT
		IF IS_BIT_SET(iLocalBoolCheck32, LBOOL32_FINGERPRINT_HACK_ANIM_USE_ALT)
			sAnim = "success_react_exit_var_02"
			sPhoneAnim = "success_react_exit_var_02_prop_phone_ing"
			sHackingDeviceAnim = "success_react_exit_var_02_ch_prop_ch_usb_drive01x"
		ELSE
			sAnim = "success_react_exit_var_01"
			sPhoneAnim = "success_react_exit_var_01_prop_phone_ing"
			sHackingDeviceAnim = "success_react_exit_var_01_ch_prop_ch_usb_drive01x"
		ENDIF
	ELIF fkhaFingerprintKeypadHackAnimState = FKHA_STATE_EXIT
		sAnim = "exit"
		sPhoneAnim = "exit_prop_phone_ing"
		sHackingDeviceAnim = "exit_ch_prop_ch_usb_drive01x"
	ELSE fkhaFingerprintKeypadHackAnimState = FKHA_STATE_QUICK_EXIT
		sAnim = "fail_react_quick"
		sPhoneAnim = "fail_react_quick_prop_phone_ing"
		sHackingDeviceAnim = "fail_react_quick_ch_prop_ch_usb_drive01x"
	ENDIF
	
	REQUEST_ANIM_DICT(sDict)
	
	IF NOT HAS_ANIM_DICT_LOADED(sDict)
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(iLocalBoolCheck31, LBOOL31_STARTED_ANIM_FOR_FINGERPRINT_KEYPAD_HACK) 
		CREATE_AND_START_SYNC_SCENE_FINGERPRINT_KEYPAD_HACK(vFingerprintKeypadHackAnimLastPos, vFingerprintKeypadHackAnimLastRot, sDict, sAnim, sPhoneAnim, sHackingDeviceAnim, FALSE, FAST_BLEND_IN, FAST_BLEND_OUT, INSTANT_BLEND_IN)
		SET_BIT(iLocalBoolCheck31, LBOOL31_STARTED_ANIM_FOR_FINGERPRINT_KEYPAD_HACK)
	ELIF NOT IS_SYNC_SCENE_RUNNING(MC_playerBD[iLocalPart].iSynchSceneID)
	OR (HAS_ANIM_EVENT_FIRED(LocalPlayerPed, HASH("BREAKOUT")) AND IS_PLAYER_PRESSING_MOVEMENT_INPUT_DURING_EXIT())
		CLEAR_BIT(iLocalBoolCheck31, LBOOL31_STARTED_ANIM_FOR_FINGERPRINT_KEYPAD_HACK)
		CLEAN_UP_PHYSICAL_FINGERPRINT_KEYPAD_HACK_ANIM()
		fkhaFingerprintKeypadHackAnimState = FKHA_STATE_NONE
		PRINTLN("[FingerprintKeypadHackAnim] Fallback - Player is now entering the FKHA_STATE_NONE state!")
	ELSE
		HANDLE_FINGERPRINT_KEYPAD_EXIT_HIDING_PROPS()
	ENDIF

ENDPROC

FUNC BOOL CAN_RUN_THERMITE_FINGERPRINT_KEYPAD_GAME_WITH_LIMITED_CHARGES(INT iObj)
	
	IF g_sCasinoHeistMissionConfigData.eChosenApproachType = CASINO_HEIST_APPROACH_TYPE__STEALTH
	OR g_sCasinoHeistMissionConfigData.eChosenApproachType = CASINO_HEIST_APPROACH_TYPE__SUBTERFUGE
		IF IS_BIT_SET(MC_serverBD_1.sLEGACYMissionContinuityVars.iGenericTrackingBitset, ciLEGACY_ContinuityGenericTrackingBS_DroppedToDirect)
			//Don't allow the thermite game if we've dropped down to the direct branch.
			PRINTLN("[FingerprintKeypadHack] CAN_RUN_THERMITE_FINGERPRINT_KEYPAD_GAME_WITH_LIMITED_CHARGES - Return FALSE - Dropped down to direct from Stealth or Subterfuge approach.")
			RETURN FALSE
		ENDIF
	ENDIF
	
	INT iTeam = MC_playerBD[iPartToUse].iteam
	INT iChargesRemaining
	BOOL bPerPlayer = IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iTeamBitset4, ciBS4_TEAM_PER_PLAYER_THERMAL_CHARGES)
	
	IF bPerPlayer
		iChargesRemaining = MC_playerBD_1[iPartToUse].iThermitePerPlayerRemaining
	ELSE
		iChargesRemaining = MC_serverBD_1.iTeamThermalCharges[iTeam]
	ENDIF
	
	IF iChargesRemaining > -1 //If not using unlimited charges.
		IF iChargesRemaining = 0
			
			//If the thermite game is currently running when the limit hit 0, then continuing processing for the player that planted it. 
			IF IS_BIT_SET(iThermiteKeypadWasLastTeamCharge[iTeam], iObj)
			OR IS_BIT_SET(iThermiteKeypadWasLastPlayerCharge, iObj)
			OR IS_BIT_SET(iThermiteKeypadIsProcessing, iObj)
			
				IF bPerPlayer
					PRINTLN("[FingerprintKeypadHack] CAN_RUN_THERMITE_FINGERPRINT_KEYPAD_GAME_WITH_LIMITED_CHARGES - Return TRUE - Player has ", iChargesRemaining, " thermal charges remaining but processing is ongoing for final charge.")
				ELSE
					PRINTLN("[FingerprintKeypadHack] CAN_RUN_THERMITE_FINGERPRINT_KEYPAD_GAME_WITH_LIMITED_CHARGES - Return TRUE - Player's team has ", iChargesRemaining, " thermal charges remaining but processing is ongoing for final charge.")
				ENDIF
				RETURN TRUE
			ENDIF
			
			IF bPerPlayer
				PRINTLN("[FingerprintKeypadHack] CAN_RUN_THERMITE_FINGERPRINT_KEYPAD_GAME_WITH_LIMITED_CHARGES - Return FALSE - Player has ", iChargesRemaining, " thermal charges remaining.")
			ELSE
				PRINTLN("[FingerprintKeypadHack] CAN_RUN_THERMITE_FINGERPRINT_KEYPAD_GAME_WITH_LIMITED_CHARGES - Return FALSE - Player's team has ", iChargesRemaining, " thermal charges remaining.")
			ENDIF
			
			RETURN FALSE
		ELSE
			IF bPerPlayer
				PRINTLN("[FingerprintKeypadHack] CAN_RUN_THERMITE_FINGERPRINT_KEYPAD_GAME_WITH_LIMITED_CHARGES - Return TRUE - Player has ", iChargesRemaining, " thermal charges remaining.")
			ELSE
				PRINTLN("[FingerprintKeypadHack] CAN_RUN_THERMITE_FINGERPRINT_KEYPAD_GAME_WITH_LIMITED_CHARGES - Return TRUE - Player's team has ", iChargesRemaining, " thermal charges remaining.")
			ENDIF
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC INT GET_NUMBER_OF_PATTERNS_REQUIRED_FOR_KEYPAD(INT iObj)
	
	INT iRequired = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPatternsRequired
	BOOL bNormal = FALSE
	
	//Reduce the number of patterns required by 1 if playing on Normal difficulty, otherwise use the creator set value.
	IF g_FMMC_STRUCT.iDifficulity = DIFF_NORMAL
	AND iRequired > 1 //1 Minimum
		iRequired = (iRequired - 1)
		bNormal = TRUE
	ENDIF
	
	IF iRequired = 4
		IF IS_BIT_SET(iFingerprintKeypadFailedTwice, iObj)
			iRequired = 2
			PRINTLN("[FingerprintKeypadHack] GET_NUMBER_OF_PATTERNS_REQUIRED_FOR_KEYPAD Obj ", iObj, " - Player has failed this keypad twice or more - Patterns Required set to = ", iRequired)
		ELIF IS_BIT_SET(iFingerprintKeypadFailedOnce, iObj)
			iRequired = 3
			PRINTLN("[FingerprintKeypadHack] GET_NUMBER_OF_PATTERNS_REQUIRED_FOR_KEYPAD Obj ", iObj, " - Player has failed this keypad once - Patterns Required set to = ", iRequired)
		ENDIF
	ELIF iRequired = 3
		IF IS_BIT_SET(iFingerprintKeypadFailedOnce, iObj)
			iRequired = 2
			PRINTLN("[FingerprintKeypadHack] GET_NUMBER_OF_PATTERNS_REQUIRED_FOR_KEYPAD Obj ", iObj, " - Player has failed this keypad once or more - Patterns Required set to = ", iRequired)
		ENDIF
	ENDIF
	
	IF bNormal
		PRINTLN("[FingerprintKeypadHack] GET_NUMBER_OF_PATTERNS_REQUIRED_FOR_KEYPAD Obj ", iObj, " - Normal Difficulty - Patterns required = ", iRequired)
	ELSE
		PRINTLN("[FingerprintKeypadHack] GET_NUMBER_OF_PATTERNS_REQUIRED_FOR_KEYPAD Obj ", iObj, " - Hard Difficulty - Patterns required = ", iRequired)
	ENDIF
	
	RETURN iRequired

ENDFUNC

PROC PROCESS_FINGERPRINT_KEYPAD_HACK_GAME_MC(OBJECT_INDEX tempObj, INT iobj, INT iMinigameType)

	IF sIWInfo.eInteractWith_CurrentState != IW_STATE_IDLE
		EXIT
	ENDIF
	
	//Check we're using a valid minigame type... fallback to fingerprint clone if not.
	IF iMinigameType = cibsLEGACY_FMMC_MINI_GAME_TYPE_ORDER_UNLOCK
		PRINTLN("[FingerprintKeypadHack] Processing FingerprintKeypadHack Obj ", iobj, " Minigame type: Order Unlock --------------------------------------------------")
	ELIF iMinigameType = cibsLEGACY_FMMC_MINI_GAME_TYPE_FINGERPRINT_CLONE
		PRINTLN("[FingerprintKeypadHack] Processing FingerprintKeypadHack Obj ", iobj, " Minigame type: Fingerprint Clone ---------------------------------------------")
	ELSE 
		iMinigameType = cibsLEGACY_FMMC_MINI_GAME_TYPE_FINGERPRINT_CLONE
		PRINTLN("[FingerprintKeypadHack] Processing FingerprintKeypadHack Obj ", iobj, " Invalid Minigame type: Fallback to Fingerprint Clone -------------------------")
	ENDIF

	//Set the minigame as completed if processing off rule and the object is broken/fragmented early.
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetThree, cibsOBJ3_AllowMinigameOffRule)
	AND NOT IS_BIT_SET(MC_serverBD_1.iOffRuleMinigameCompleted, iObj)
		IF DOES_ENTITY_EXIST(tempObj)
		AND IS_OBJECT_BROKEN_AND_VISIBLE(tempObj)
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetFour, cibsOBJ4_UpdateLinkedDoorsOnDestroyed)
				PRINTLN("[FingerprintKeypadHack] Fingerprint Keypad Obj ", iobj, " broken! cibsOBJ4_UpdateLinkedDoorsOnDestroyed set.")
				//Update/Unlock any linked doors when destroyed.
				SET_DOOR_TO_UPDATE_OFF_RULE_FROM_OBJECT(iObj, TRUE, ciUPDATE_DOOR_OFF_RULE_METHOD__OBJ_DESTROYED)
			ELSE
				PRINTLN("[FingerprintKeypadHack] Fingerprint KeypadHack Obj ", iobj, " broken! cibsOBJ4_UpdateLinkedDoorsOnDestroyed NOT set.")
			ENDIF
			
			BROADCAST_FMMC_UPDATE_MINIGAME_OFF_RULE_EVENT(TRUE, iObj)
		ENDIF
	ENDIF
	
	//Check if the use thermite option is set and process that mini-game instead.
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iLegacyMiniGameBitSet, cibsLEGACY_FMMC_MINI_GAME_USE_THERMAL_CHARGE)
	AND CAN_RUN_THERMITE_FINGERPRINT_KEYPAD_GAME_WITH_LIMITED_CHARGES(iObj) //If we run out of charges then use hacking instead.
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetThree, cibsOBJ3_AllowMinigameOffRule)
		AND IS_BIT_SET(MC_serverBD_1.iOffRuleMinigameCompleted, iObj)
			EXIT
		ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetThree, cibsOBJ3_AllowMinigameOffRule)
		AND IS_BIT_SET(iLocalOffRuleMinigameCompletedBS, iObj)
			PRINTLN("[FingerprintKeypadHack] obj ", iObj, " iLocalOffRuleMinigameCompletedBS is set")
			EXIT
		ENDIF
		
		PROCESS_BLOW_OPEN_DOOR_WITH_THERMAL_CHARGE(tempObj, iobj, TRUE)
		EXIT
	ENDIF
	
	IF iFingerprintKeypadForCurrentAnim > -1
	AND iFingerprintKeypadForCurrentAnim = iObj
		HANDLE_PHYSICAL_FINGERPRINT_KEYPAD_HACK_ANIM(tempObj, iObj) 
	ENDIF
	
	//Exit early if already complete and we're processing off rule.
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetThree, cibsOBJ3_AllowMinigameOffRule)
	AND IS_BIT_SET(MC_serverBD_1.iOffRuleMinigameCompleted, iObj)
		
		//If a player was currently hacking an object that was just destroyed, clean up the minigame and anims.
		IF MC_playerBD[iLocalPart].iObjHacking = iObj
			RESET_OBJ_HACKING_INT()		
			IF iMinigameType = cibsLEGACY_FMMC_MINI_GAME_TYPE_FINGERPRINT_CLONE
				CLEAR_BIT(iLocalBoolCheck32, LBOOL32_STARTED_FINGERPRINT_CLONE)
				CLEAN_UP_FINGERPRINT_CLONE(sFingerprintCloneGameplay, sFingerprintClone[iobj], DEFAULT, HASH("heist"), g_HeistTelemetryData.iHeistPosixTime)
			ELIF iMinigameType = cibsLEGACY_FMMC_MINI_GAME_TYPE_ORDER_UNLOCK
				CLEAR_BIT(iLocalBoolCheck32, LBOOL32_STARTED_ORDER_UNLOCK)
				CLEAN_UP_ORDER_UNLOCK(sOrderUnlockGameplay, sOrderUnlock[iobj], DEFAULT, HASH("heist"), g_HeistTelemetryData.iHeistPosixTime)
			ENDIF
			CLEAN_UP_PHYSICAL_FINGERPRINT_KEYPAD_HACK_ANIM()
		ENDIF
		
		EXIT
	ENDIF
	
	IF MC_playerBD[iLocalPart].iObjHacked = -1
		IF MC_serverBD.iObjHackPart[iobj] = -1
			IF MC_playerBD[iLocalPart].iObjHacking = -1
				IF bLocalPlayerPedOk
					IF NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
						IF NOT IS_PHONE_ONSCREEN()
						AND NOT IS_HUD_COMPONENT_ACTIVE(NEW_HUD_WEAPON_WHEEL)
							IF PLAYER_CAN_INTERACT_WITH_OBJ(iobj, tempObj)
							AND IS_PLAYER_REQUIRED_DIST_TO_INTERACT_WITH_KEYPAD(LocalPlayerPed, tempObj)
								IF fkhaFingerprintKeypadHackAnimState  = FKHA_STATE_NONE
									IF NOT SHOULD_OBJECT_INTERACTION_HELP_TEXT_BE_BLOCKED()
										DISPLAY_HELP_TEXT_THIS_FRAME("MC_INTOBJ_HKKPH",TRUE) //"... to hack the keypad"
									ENDIF
									DISABLE_CONTROL_ACTION(FRONTEND_CONTROL,INPUT_CONTEXT)
									DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_SELECT_WEAPON)
									IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT)
										MC_playerBD[iLocalPart].iObjHacking = iobj
										iFingerprintKeypadForCurrentAnim = iobj
										
										IF GET_RANDOM_INT_IN_RANGE(0, 2) > 0
											SET_BIT(iLocalBoolCheck32, LBOOL32_FINGERPRINT_HACK_ANIM_USE_ALT)
										ELSE
											CLEAR_BIT(iLocalBoolCheck32, LBOOL32_FINGERPRINT_HACK_ANIM_USE_ALT)
										ENDIF
										
										IF iMinigameType = cibsLEGACY_FMMC_MINI_GAME_TYPE_FINGERPRINT_CLONE
											SET_BIT(iLocalBoolCheck32, LBOOL32_STARTED_FINGERPRINT_CLONE)
										ELIF iMinigameType = cibsLEGACY_FMMC_MINI_GAME_TYPE_ORDER_UNLOCK
											SET_BIT(iLocalBoolCheck32, LBOOL32_STARTED_ORDER_UNLOCK)
										ENDIF
										
										IF NETWORK_IS_GAME_IN_PROGRESS()
											NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_LEAVE_CAMERA_CONTROL_ON)
											PRINTLN("[FingerprintKeypadHack] Taking player control away!")
											DISABLE_INTERACTION_MENU() 
										ELSE
											SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
											PRINTLN("[FingerprintKeypadHack] Taking player control away!")
										ENDIF
									ENDIF
								ENDIF
							ELSE
								PRINTLN("[FingerprintKeypadHack] Too far or can't interact with for OBJ ", iObj)
							ENDIF
						ELSE
							PRINTLN("[FingerprintKeypadHack] Phone or weapon wheel for OBJ ", iObj)
						ENDIF
					ELSE
						PRINTLN("[FingerprintKeypadHack] Player is in a vehicle", " for OBJ for OBJ ", iObj)
					ENDIF
				ELSE
					PRINTLN("[FingerprintKeypadHack] Player ped is not okay", " for OBJ for OBJ ", iObj)
				ENDIF
			ELSE
				PRINTLN("[FingerprintKeypadHack] iObjHacking is ", MC_playerBD[iLocalPart].iObjHacking, " for OBJ ", iObj)
			ENDIF
		ELSE
			IF MC_serverBD.iObjHackPart[iobj] = iLocalPart AND bLocalPlayerOK
				IF MC_playerBD[iLocalPart].iObjHacked = -1
					IF MC_playerBD[iLocalPart].iObjHacking = iobj
					
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MC_INTOBJ_HKKPH")
							CLEAR_HELP(TRUE)
						ENDIF
						
						IF GET_DIST2_BETWEEN_ENTITIES(LocalPlayerPed, tempObj) < POW(2.0, 2.0)
							IF fkhaFingerprintKeypadHackAnimState = FKHA_STATE_LOOPING
							
								IF iMinigameType = cibsLEGACY_FMMC_MINI_GAME_TYPE_ORDER_UNLOCK
									INT iNumPlays = GET_MP_INT_CHARACTER_STAT(MP_STAT_CR_ORDER)
									BOOL bShowFinalQuitWarning = IS_BIT_SET(iFingerprintKeypadQuitOnce, iObj)
									
									//Don't show the warning about triggering the alarm if it's already triggered.
									IF MC_PlayerBD[iLocalPart].iTeam > -1
										IF IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_AGGRO_TRIGGERED_FOR_TEAM_0 + MC_PlayerBD[iLocalPart].iTeam)
											IF NOT IS_BIT_SET(sOrderUnlock[iobj].iBS, ciHGBS_TEAM_HAS_AGGRO)
												SET_BIT(sOrderUnlock[iobj].iBS, ciHGBS_TEAM_HAS_AGGRO)
											ENDIF
										ENDIF
									ENDIF
								
									PROCESS_ORDER_UNLOCK_MINIGAME(sOrderUnlockGameplay, sOrderUnlock[iobj], GET_NUMBER_OF_PATTERNS_REQUIRED_FOR_KEYPAD(iobj), iNumPlays, bShowFinalQuitWarning)
									IF IS_BIT_SET(sOrderUnlock[iobj].iBS, ciHGBS_PASSED_SECTION)
									OR IS_BIT_SET(sOrderUnlock[iobj].iBS, ciHGBS_LOST_LIFE) 
										PRINTLN("[FingerprintKeypadHack] PASSED/LOST LIFE do dialogue check for obj ", iObj)
										SET_BIT(iLocalBoolCheck9, LBOOL9_DO_INSTANT_DIALOGUE_LOOP)
									ENDIF
									
									IF IS_BIT_SET(sOrderUnlock[iobj].iBS, ciHGBS_PASSED)
										MC_playerBD[iLocalPart].iObjHacked = iobj
										INCREMENT_OFF_RULE_MINIGAME_SCORE_FROM_OBJECT(ciOffRuleScore_Low, iObj)
										MC_PlayerBD[iLocalPart].iHackTime =  MC_PlayerBD[iLocalPart].iHackTime + GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(sOrderUnlockGameplay.sBaseStruct.tdTimer)
										MC_PlayerBD[iLocalPart].iNumHacks++
										RESET_OBJ_HACKING_INT()
										CLEAN_UP_ORDER_UNLOCK(sOrderUnlockGameplay, sOrderUnlock[iobj], DEFAULT, HASH("heist"), g_HeistTelemetryData.iHeistPosixTime)
										
										IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
											PRINTLN("[FingerprintKeypadHack] Incrmenting MP_STAT_CR_ORDER to: ", iNumPlays + 1)
											SET_MP_INT_CHARACTER_STAT(MP_STAT_CR_ORDER, iNumPlays + 1)
										ENDIF
										
										CLEAR_BIT(iLocalBoolCheck32, LBOOL32_STARTED_ORDER_UNLOCK)
										
										IF GET_RANDOM_INT_IN_RANGE(0, 2) > 0
											SET_BIT(iLocalBoolCheck32, LBOOL32_FINGERPRINT_HACK_ANIM_USE_ALT)
										ELSE
											CLEAR_BIT(iLocalBoolCheck32, LBOOL32_FINGERPRINT_HACK_ANIM_USE_ALT)
										ENDIF
										
										IF fkhaFingerprintKeypadHackAnimState = FKHA_STATE_LOOPING
										OR fkhaFingerprintKeypadHackAnimState = FKHA_STATE_ENTER
											CLEAR_BIT(iLocalBoolCheck31, LBOOL31_STARTED_ANIM_FOR_FINGERPRINT_KEYPAD_HACK)
											fkhaFingerprintKeypadHackAnimState = FKHA_STATE_SUCCESS_EXIT
											PRINTLN("[FingerprintKeypadHackAnim] Player is now entering the FKHA_STATE_SUCCESS_EXIT state!")
										ENDIF
										
										//Update/Unlock any linked doors when passing.
										SET_DOOR_TO_UPDATE_OFF_RULE_FROM_OBJECT(iObj, TRUE, ciUPDATE_DOOR_OFF_RULE_METHOD__HACKING)
										
										//Set the minigame as completed off rule if needed.
										IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetThree, cibsOBJ3_AllowMinigameOffRule)
											BROADCAST_FMMC_UPDATE_MINIGAME_OFF_RULE_EVENT(TRUE, iObj)
										ENDIF
										
										IF NOT IS_BIT_SET(sLEGACYMissionLocalContinuityVars.iGeneralLocalTrackingBitset, ciLEGACY_ContinuityGenericTrackingBS_HackUsed)
											PRINTLN("[JS][CONTINUITY] - HACKING - Setting order unlock as used")
											SET_BIT(sLEGACYMissionLocalContinuityVars.iGeneralLocalTrackingBitset, ciLEGACY_ContinuityGenericTrackingBS_HackUsed)
										ENDIF
									
									ELIF IS_BIT_SET(sOrderUnlock[iobj].iBS, ciHGBS_FAILED)
										IF IS_BIT_SET(sOrderUnlock[iobj].iBS, ciHGBS_TIMED_OUT)
										OR IS_BIT_SET(sOrderUnlock[iobj].iBS, ciHGBS_OUT_OF_LIVES)
											MC_playerBD[iLocalPart].iHackingFails++
											MC_PlayerBD[iLocalPart].iHackTime =  MC_PlayerBD[iLocalPart].iHackTime + GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(sOrderUnlockGameplay.sBaseStruct.tdTimer)
											CLEAN_UP_ORDER_UNLOCK(sOrderUnlockGameplay, sOrderUnlock[iobj], DEFAULT, HASH("heist"), g_HeistTelemetryData.iHeistPosixTime)
											
											IF NOT IS_BIT_SET(iFingerprintKeypadFailedOnce, iObj)
												SET_BIT(iFingerprintKeypadFailedOnce, iObj)
											ELIF NOT IS_BIT_SET(iFingerprintKeypadFailedTwice, iObj)
												SET_BIT(iFingerprintKeypadFailedTwice, iObj)
											ENDIF
											
											IF fkhaFingerprintKeypadHackAnimState = FKHA_STATE_LOOPING
											OR fkhaFingerprintKeypadHackAnimState = FKHA_STATE_ENTER
												CLEAR_BIT(iLocalBoolCheck31, LBOOL31_STARTED_ANIM_FOR_FINGERPRINT_KEYPAD_HACK)
												fkhaFingerprintKeypadHackAnimState = FKHA_STATE_BACK_OUT
												PRINTLN("[FingerprintKeypadHackAnim] Player is now entering the FKHA_STATE_BACK_OUT state!")
											ENDIF
											
										ELIF IS_BIT_SET(sOrderUnlock[iobj].iBS, ciHGBS_QUIT)
											MC_PlayerBD[iLocalPart].iHackTime =  MC_PlayerBD[iLocalPart].iHackTime + GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(sOrderUnlockGameplay.sBaseStruct.tdTimer)
											RESET_OBJ_HACKING_INT()
											CLEAN_UP_ORDER_UNLOCK(sOrderUnlockGameplay, sOrderUnlock[iobj], DEFAULT, HASH("heist"), g_HeistTelemetryData.iHeistPosixTime)
											SET_BIT(iLocalBoolCheck32, LBOOL32_QUIT_KEYPAD_HACK_MANUALLY)
											
											IF NOT IS_BIT_SET(iFingerprintKeypadQuitOnce, iObj)
												SET_BIT(iFingerprintKeypadQuitOnce, iObj)
											ELIF MC_PlayerBD[iLocalPart].iTeam > -1
												IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_AGGRO_TRIGGERED_FOR_TEAM_0 + MC_PlayerBD[iLocalPart].iTeam)
													PRINTLN("[FingerprintKeypadHack] - Triggering Aggro for team: ", MC_PlayerBD[iLocalPart].iTeam, " due to player quitting keypad twice iObj: ", iObj)
													BROADCAST_FMMC_TRIGGER_AGGRO_FOR_TEAM_LEGACY(MC_PlayerBD[iLocalPart].iTeam)
												ENDIF
											ENDIF
											
											IF fkhaFingerprintKeypadHackAnimState = FKHA_STATE_LOOPING
											OR fkhaFingerprintKeypadHackAnimState = FKHA_STATE_ENTER
												CLEAR_BIT(iLocalBoolCheck31, LBOOL31_STARTED_ANIM_FOR_FINGERPRINT_KEYPAD_HACK)
												fkhaFingerprintKeypadHackAnimState = FKHA_STATE_QUICK_EXIT
												PRINTLN("[FingerprintKeypadHackAnim] Player is now entering the FKHA_STATE_QUICK_EXIT state!")
											ENDIF
										ENDIF
									ENDIF
								
								ELSE // iMinigameType = cibsLEGACY_FMMC_MINI_GAME_TYPE_FINGERPRINT_CLONE
									
									INT iNumPlays = GET_MP_INT_CHARACTER_STAT(MP_STAT_CR_FINGERPRINT)
									BOOL bShowFinalQuitWarning = IS_BIT_SET(iFingerprintKeypadQuitOnce, iObj)
									
									//Don't show the warning about triggering the alarm if it's already triggered.
									IF MC_PlayerBD[iLocalPart].iTeam > -1
										IF IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_AGGRO_TRIGGERED_FOR_TEAM_0 + MC_PlayerBD[iLocalPart].iTeam)
											IF NOT IS_BIT_SET(sFingerprintClone[iobj].iBS, ciHGBS_TEAM_HAS_AGGRO)
												SET_BIT(sFingerprintClone[iobj].iBS, ciHGBS_TEAM_HAS_AGGRO)
											ENDIF
										ENDIF
									ENDIF
									
									PROCESS_FINGERPRINT_MINIGAME(sFingerprintCloneGameplay, sFingerprintClone[iobj], GET_NUMBER_OF_PATTERNS_REQUIRED_FOR_KEYPAD(iobj), iNumPlays, bShowFinalQuitWarning) 
									IF IS_BIT_SET(sFingerprintClone[iobj].iBS, ciHGBS_PASSED_SECTION)
									OR IS_BIT_SET(sFingerprintClone[iobj].iBS, ciHGBS_LOST_LIFE) 
										PRINTLN("[FingerprintKeypadHack] PASSED/LOST LIFE do dialogue check for obj ", iObj)
										SET_BIT(iLocalBoolCheck9, LBOOL9_DO_INSTANT_DIALOGUE_LOOP)
									ENDIF
									
									IF IS_BIT_SET(sFingerprintClone[iobj].iBS, ciHGBS_PASSED)
										MC_playerBD[iLocalPart].iObjHacked = iobj
										INCREMENT_OFF_RULE_MINIGAME_SCORE_FROM_OBJECT(ciOffRuleScore_Low, iObj)
										MC_PlayerBD[iLocalPart].iHackTime =  MC_PlayerBD[iLocalPart].iHackTime + GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(sFingerprintCloneGameplay.sBaseStruct.tdTimer)
										MC_PlayerBD[iLocalPart].iNumHacks++
										RESET_OBJ_HACKING_INT()
										CLEAN_UP_FINGERPRINT_CLONE(sFingerprintCloneGameplay, sFingerprintClone[iobj], DEFAULT, HASH("heist"), g_HeistTelemetryData.iHeistPosixTime)
										
										IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
											PRINTLN("[FingerprintKeypadHack] Incrmenting MP_STAT_CR_FINGERPRINT to: ", iNumPlays + 1)
											SET_MP_INT_CHARACTER_STAT(MP_STAT_CR_FINGERPRINT, iNumPlays + 1)
										ENDIF
										
										CLEAR_BIT(iLocalBoolCheck32, LBOOL32_STARTED_FINGERPRINT_CLONE)
										
										IF GET_RANDOM_INT_IN_RANGE(0, 2) > 0
											SET_BIT(iLocalBoolCheck32, LBOOL32_FINGERPRINT_HACK_ANIM_USE_ALT)
										ELSE
											CLEAR_BIT(iLocalBoolCheck32, LBOOL32_FINGERPRINT_HACK_ANIM_USE_ALT)
										ENDIF
										
										IF fkhaFingerprintKeypadHackAnimState = FKHA_STATE_LOOPING
										OR fkhaFingerprintKeypadHackAnimState = FKHA_STATE_ENTER
											CLEAR_BIT(iLocalBoolCheck31, LBOOL31_STARTED_ANIM_FOR_FINGERPRINT_KEYPAD_HACK)
											fkhaFingerprintKeypadHackAnimState = FKHA_STATE_SUCCESS_EXIT
											PRINTLN("[FingerprintKeypadHackAnim] Player is now entering the FKHA_STATE_SUCCESS_EXIT state!")
										ENDIF
										
										//Update/Unlock any linked doors when passing.
										SET_DOOR_TO_UPDATE_OFF_RULE_FROM_OBJECT(iObj, TRUE, ciUPDATE_DOOR_OFF_RULE_METHOD__HACKING)
										
										//Set the minigame as completed off rule if needed.
										IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetThree, cibsOBJ3_AllowMinigameOffRule)
											BROADCAST_FMMC_UPDATE_MINIGAME_OFF_RULE_EVENT(TRUE, iObj)
										ENDIF
										
										IF NOT IS_BIT_SET(sLEGACYMissionLocalContinuityVars.iGeneralLocalTrackingBitset, ciLEGACY_ContinuityGenericTrackingBS_HackUsed)
											PRINTLN("[JS][CONTINUITY] - HACKING - Setting finger print unlock as used")
											SET_BIT(sLEGACYMissionLocalContinuityVars.iGeneralLocalTrackingBitset, ciLEGACY_ContinuityGenericTrackingBS_HackUsed)
										ENDIF
										
									ELIF IS_BIT_SET(sFingerprintClone[iobj].iBS, ciHGBS_FAILED)	
										IF IS_BIT_SET(sFingerprintClone[iobj].iBS, ciHGBS_TIMED_OUT)
										OR IS_BIT_SET(sFingerprintClone[iobj].iBS, ciHGBS_OUT_OF_LIVES)
											MC_playerBD[iLocalPart].iHackingFails++
											MC_PlayerBD[iLocalPart].iHackTime =  MC_PlayerBD[iLocalPart].iHackTime + GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(sFingerprintCloneGameplay.sBaseStruct.tdTimer)
											CLEAN_UP_FINGERPRINT_CLONE(sFingerprintCloneGameplay, sFingerprintClone[iobj], DEFAULT, HASH("heist"), g_HeistTelemetryData.iHeistPosixTime)
											
											IF NOT IS_BIT_SET(iFingerprintKeypadFailedOnce, iObj)
												SET_BIT(iFingerprintKeypadFailedOnce, iObj)
											ELIF NOT IS_BIT_SET(iFingerprintKeypadFailedTwice, iObj)
												SET_BIT(iFingerprintKeypadFailedTwice, iObj)
											ENDIF
											
											IF fkhaFingerprintKeypadHackAnimState = FKHA_STATE_LOOPING
											OR fkhaFingerprintKeypadHackAnimState = FKHA_STATE_ENTER
												CLEAR_BIT(iLocalBoolCheck31, LBOOL31_STARTED_ANIM_FOR_FINGERPRINT_KEYPAD_HACK)
												fkhaFingerprintKeypadHackAnimState = FKHA_STATE_BACK_OUT
												PRINTLN("[FingerprintKeypadHackAnim] Player is now entering the FKHA_STATE_BACK_OUT state!")
											ENDIF
											
										ELIF IS_BIT_SET(sFingerprintClone[iobj].iBS, ciHGBS_QUIT)
											MC_PlayerBD[iLocalPart].iHackTime =  MC_PlayerBD[iLocalPart].iHackTime + GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(sFingerprintCloneGameplay.sBaseStruct.tdTimer)
											RESET_OBJ_HACKING_INT()
											CLEAN_UP_FINGERPRINT_CLONE(sFingerprintCloneGameplay, sFingerprintClone[iobj], DEFAULT, HASH("heist"), g_HeistTelemetryData.iHeistPosixTime)
											SET_BIT(iLocalBoolCheck32, LBOOL32_QUIT_KEYPAD_HACK_MANUALLY)
											
											IF NOT IS_BIT_SET(iFingerprintKeypadQuitOnce, iObj)
												SET_BIT(iFingerprintKeypadQuitOnce, iObj)
											ELIF MC_PlayerBD[iLocalPart].iTeam > -1
												IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_AGGRO_TRIGGERED_FOR_TEAM_0 + MC_PlayerBD[iLocalPart].iTeam)
													PRINTLN("[FingerprintKeypadHack] - Triggering Aggro for team: ", MC_PlayerBD[iLocalPart].iTeam, " due to player quitting keypad twice iObj: ", iObj)
													BROADCAST_FMMC_TRIGGER_AGGRO_FOR_TEAM_LEGACY(MC_PlayerBD[iLocalPart].iTeam)
												ENDIF
											ENDIF
											
											IF fkhaFingerprintKeypadHackAnimState = FKHA_STATE_LOOPING
											OR fkhaFingerprintKeypadHackAnimState = FKHA_STATE_ENTER
												CLEAR_BIT(iLocalBoolCheck31, LBOOL31_STARTED_ANIM_FOR_FINGERPRINT_KEYPAD_HACK)
												fkhaFingerprintKeypadHackAnimState = FKHA_STATE_QUICK_EXIT
												PRINTLN("[FingerprintKeypadHackAnim] Player is now entering the FKHA_STATE_QUICK_EXIT state!")
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ELIF fkhaFingerprintKeypadHackAnimState = FKHA_STATE_LOOP_PRE_EXIT 
									
								IF iMinigameType = cibsLEGACY_FMMC_MINI_GAME_TYPE_FINGERPRINT_CLONE
									CLEAR_BIT(iLocalBoolCheck32, LBOOL32_STARTED_FINGERPRINT_CLONE) 
								ELIF iMinigameType = cibsLEGACY_FMMC_MINI_GAME_TYPE_ORDER_UNLOCK
									CLEAR_BIT(iLocalBoolCheck32, LBOOL32_STARTED_ORDER_UNLOCK)
								ENDIF
								
								IF PLAYER_CAN_INTERACT_WITH_OBJ(iobj, tempObj)
									DISPLAY_HELP_TEXT_THIS_FRAME("MC_INTOBJ_HKKPI",TRUE)
									DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_CONTEXT)
									DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_SELECT_WEAPON)
									DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
									DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
									DISABLE_CELLPHONE_THIS_FRAME_ONLY()
									
									IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT)
									
										IF iMinigameType = cibsLEGACY_FMMC_MINI_GAME_TYPE_FINGERPRINT_CLONE
											SET_BIT(iLocalBoolCheck32, LBOOL32_STARTED_FINGERPRINT_CLONE)
										ELIF iMinigameType = cibsLEGACY_FMMC_MINI_GAME_TYPE_ORDER_UNLOCK
											SET_BIT(iLocalBoolCheck32, LBOOL32_STARTED_ORDER_UNLOCK)
										ENDIF
									
										CLEAR_BIT(iLocalBoolCheck31, LBOOL31_STARTED_ANIM_FOR_FINGERPRINT_KEYPAD_HACK)
										fkhaFingerprintKeypadHackAnimState = FKHA_STATE_REATTEMPT
										PRINTLN("[FingerprintKeypadHackAnim] Player is now entering the FKHA_STATE_REATTEMPT state!")
			
									ELIF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_CANCEL)
										RESET_OBJ_HACKING_INT()
										
										CLEAR_BIT(iLocalBoolCheck31, LBOOL31_STARTED_ANIM_FOR_FINGERPRINT_KEYPAD_HACK)
										fkhaFingerprintKeypadHackAnimState = FKHA_STATE_EXIT
										PRINTLN("[FingerprintKeypadHackAnim] Player is now entering the FKHA_STATE_EXIT state!")
									ENDIF
								ENDIF
							ENDIF
						ELSE
							PRINTLN("[FingerprintKeypadHack] out of range for OBJ ", iObj)
							RESET_OBJ_HACKING_INT()
							
							IF iMinigameType = cibsLEGACY_FMMC_MINI_GAME_TYPE_FINGERPRINT_CLONE
								CLEAR_BIT(iLocalBoolCheck32, LBOOL32_STARTED_FINGERPRINT_CLONE)
								CLEAN_UP_FINGERPRINT_CLONE(sFingerprintCloneGameplay, sFingerprintClone[iobj], DEFAULT, HASH("heist"), g_HeistTelemetryData.iHeistPosixTime)
							ELIF iMinigameType = cibsLEGACY_FMMC_MINI_GAME_TYPE_ORDER_UNLOCK
								CLEAR_BIT(iLocalBoolCheck32, LBOOL32_STARTED_ORDER_UNLOCK)
								CLEAN_UP_ORDER_UNLOCK(sOrderUnlockGameplay, sOrderUnlock[iobj], DEFAULT, HASH("heist"), g_HeistTelemetryData.iHeistPosixTime)
							ENDIF
							
							CLEAN_UP_PHYSICAL_FINGERPRINT_KEYPAD_HACK_ANIM()
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF MC_serverBD.iObjHackPart[iobj] != -1
				AND MC_serverBD.iObjHackPart[iobj] != iLocalPart
				
					//Block using cover near the keypads for hacking if a hack is ongoing by another player.
					IF IS_PLAYER_REQUIRED_DIST_TO_INTERACT_WITH_KEYPAD(LocalPlayerPed, tempObj)
						DISABLE_CONTROL_ACTION(FRONTEND_CONTROL,INPUT_COVER)
						IF IS_PED_IN_COVER(LocalPlayerPed)
							CLEAR_PED_TASKS(LocalPlayerPed)
						ENDIF
					ENDIF
				
					IF MC_playerBD[iLocalPart].iObjHacking = iobj
						PRINTLN("[FingerprintKeypadHack] someone else hacking OBJ ", iObj)
						RESET_OBJ_HACKING_INT()
						
						IF iMinigameType = cibsLEGACY_FMMC_MINI_GAME_TYPE_FINGERPRINT_CLONE
							CLEAR_BIT(iLocalBoolCheck32, LBOOL32_STARTED_FINGERPRINT_CLONE)
							CLEAN_UP_FINGERPRINT_CLONE(sFingerprintCloneGameplay, sFingerprintClone[iobj], DEFAULT, HASH("heist"), g_HeistTelemetryData.iHeistPosixTime)
						ELIF iMinigameType = cibsLEGACY_FMMC_MINI_GAME_TYPE_ORDER_UNLOCK
							CLEAR_BIT(iLocalBoolCheck32, LBOOL32_STARTED_ORDER_UNLOCK)
							CLEAN_UP_ORDER_UNLOCK(sOrderUnlockGameplay, sOrderUnlock[iobj], DEFAULT, HASH("heist"), g_HeistTelemetryData.iHeistPosixTime)
						ENDIF
						CLEAN_UP_PHYSICAL_FINGERPRINT_KEYPAD_HACK_ANIM()
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_CCTV_CAMERA_DISABLED_BY_PHONE_EMP(FMMC_CCTV_DATA& sCCTVData)

	IF IS_BIT_SET(sCCTVData.iCCTVBS, ciCCTV_BS_AffectedByPhoneEMP)
	AND IS_PHONE_EMP_TIMER_RUNNING()
		PRINTLN("[Taser] IS_CCTV_CAMERA_DISABLED_BY_PHONE_EMP returning TRUE due to Phone EMP")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_CCTV_CAMERA_ALARM_SOUNDS(BOOL bSpotted, BOOL bAlerted, OBJECT_INDEX tempObj, INT iCam, INT iIndex, FMMC_ELECTRONIC_ENTITY_TYPE eElectronicType)
	IF iCam > -1
		IF (bSpotted OR bAlerted)
		AND NOT SHOULD_ELECTRONIC_BE_DISABLED_THIS_FRAME(iIndex, eElectronicType)
		
			INT iSoundType = 0
			TEXT_LABEL_63 tl63SoundSet
			TEXT_LABEL_63 tl63SoundName
			
			IF bSpotted
				PRINTLN("[LM][PROCESS_CCTV_CAMERA_ALARM_SOUNDS] - bSpotted by iCam ", iCam)
				iSoundType = 1
				
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFive, ciOptionsBS25_EnableCasinoHeistAudioBank)
					PRINTLN("[LM][PROCESS_CCTV_CAMERA_ALARM_SOUNDS] Using Camera_Alarm")
					tl63SoundSet = "dlc_ch_heist_finale_security_alarms_sounds"
					tl63SoundName = "Camera_Alarm"
					
				ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_UseCasinoCCTVAudio)
					tl63SoundSet = "dlc_vw_casino_finale_sounds"
					tl63SoundName = "CCTV_Alarm"
				ELSE
					tl63SoundSet = "ALARMS_SOUNDSET"
					tl63SoundName = "Klaxon_04"
				ENDIF
			ENDIF
						
			IF bAlerted
				PRINTLN("[LM][PROCESS_CCTV_CAMERA_ALARM_SOUNDS] - bAlerted by iCam ", iCam)
				iSoundType = 2
				
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFive, ciOptionsBS25_EnableCasinoHeistAudioBank)
					PRINTLN("[LM][PROCESS_CCTV_CAMERA_ALARM_SOUNDS] Using Camera_Alarm")
					tl63SoundSet = "dlc_ch_heist_finale_security_alarms_sounds"
					tl63SoundName = "Camera_Alarm"
					
				ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_UseCasinoCCTVAudio)
					PRINTLN("[LM][PROCESS_CCTV_CAMERA_ALARM_SOUNDS] Using Main_Alarm")
					tl63SoundSet = "dlc_vw_casino_finale_sounds"
					tl63SoundName = "Main_Alarm"
				ELSE
					PRINTLN("[LM][PROCESS_CCTV_CAMERA_ALARM_SOUNDS] Using Bell_01")
					tl63SoundSet = "ALARMS_SOUNDSET"
					tl63SoundName = "Bell_01"
				ENDIF
			ENDIF
			
			IF iSoundAlarmType[iCam] != iSoundType
				iSoundAlarmType[iCam] = iSoundType
			
				IF iSoundAlarmCCTV[iCam] = -1
					PRINTLN("[LM][PROCESS_CCTV_CAMERA_ALARM_SOUNDS] - Getting sound ID for iCam ", iCam)
					iSoundAlarmCCTV[iCam] = GET_SOUND_ID()
				ENDIF
				
				IF NOT HAS_SOUND_FINISHED(iSoundAlarmCCTV[iCam])
					PRINTLN("[LM][PROCESS_CCTV_CAMERA_ALARM_SOUNDS] - Stopping Current Sound for iCam ", iCam)
					STOP_SOUND(iSoundAlarmCCTV[iCam])
				ENDIF
				
				PRINTLN("[LM][PROCESS_CCTV_CAMERA_ALARM_SOUNDS] - Paying sound for iCam ", iCam)
				PLAY_SOUND_FROM_COORD(iSoundAlarmCCTV[iCam], tl63SoundName, GET_ENTITY_COORDS(tempObj), tl63SoundSet)
			ENDIF
		ELSE
			IF iSoundAlarmCCTV[iCam] > -1
			AND NOT HAS_SOUND_FINISHED(iSoundAlarmCCTV[iCam])
				PRINTLN("[LM][PROCESS_CCTV_CAMERA_ALARM_SOUNDS] - Not alerted or spotted for iCam ", iCam, " Stopping all sounds.")
				STOP_SOUND(iSoundAlarmCCTV[iCam])
				SCRIPT_RELEASE_SOUND_ID(iSoundAlarmCCTV[iCam])
				iSoundAlarmType[iCam] = -1
			ENDIF
		ENDIF		
	ENDIF
ENDPROC

PROC DRAW_CAMERA_VIEW_MARKERS(INT iCam, BOOL bSpotted, BOOL bAlerted, FLOAT fLightIntensity, OBJECT_INDEX tempObj)

	IF IS_BIT_SET(iCCTVBrokenBS, iCam)
		EXIT
	ENDIF
	
	VECTOR vPosition, vDirection, vRotation, vScale
	INT iR, iG, iB, iA
	HUD_COLOURS hudCol = HUD_COLOUR_WHITE
	
	IF bAlerted
		hudCol = HUD_COLOUR_RED
	ELIF bSpotted
		hudCol = HUD_COLOUR_YELLOW
	ENDIF
	
	GET_HUD_COLOUR(hudCol, iR, iG, iB, iA)
	
	IF bSpotted
	OR bAlerted
		iA = 60
	ELSE
		iA = 30
	ENDIF
	
	iA = ROUND((fLightIntensity / cf_CCTV_BaseLightIntensity) * iA)
	iA = CLAMP_INT(iA, 0, 80)
	
	vPosition = GET_ENTITY_COORDS(tempObj)
	vDirection = <<0.0, 0.0, 0.5>>	
	vRotation = <<180.0, 0.0, GET_ENTITY_HEADING(tempObj)>>
	
	vPosition = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPosition, GET_ENTITY_HEADING(tempObj), <<0.0, -1.45, -0.27>>)
	
	vScale = <<4.0, 4.0, 1.75>>
	
	DRAW_MARKER_EX(MARKER_CONE, vPosition, vDirection, vRotation, vScale, iR, iG, iB, iA, FALSe, FALSE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
	
ENDPROC

PROC TRIGGER_ALL_CCTV_CAMERAS(INT iPartTriggered = -1)
	PRINTLN("[LM][TRIGGER_ALL_CCTV_CAMERAS] - Triggering all active CCTV cameras.")
	
	INT iCam = 0
	FOR iCam = 0 TO (MAX_NUM_CCTV_CAM - 1)
		SET_BIT(iCCTVCameraSpottedPlayerBS, iCam)
		START_NET_TIMER(CCTVSpottedTimer[iCam])
		iCCTVCameraSpottedPlayer[iCam] = iPartTriggered
	ENDFOR
	
	SET_BIT(iLocalBoolCheck31, LBOOL31_HAS_TRIGGERED_ALL_CCTV_CAMERAS)
ENDPROC

PROC CCTV_CAMERA_BODY_SPOTTED_HELP_TEXT(INT iCam)
	IF NOT IS_BIT_SET(iCCTVCameraSpottedPlayerHelpTextBS, iCam)
		
		PRINTLN("[ML][CCTV_CAMERA_BODY_SPOTTED_HELP_TEXT] Camera ", iCam, " is seeing participant ", iCCTVCameraSpottedPlayer[iCam])

		IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_AGGRO_TRIGGERED_FOR_TEAM_0)
		AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_AGGRO_TRIGGERED_FOR_TEAM_1)
		AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_AGGRO_TRIGGERED_FOR_TEAM_2)
		AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_AGGRO_TRIGGERED_FOR_TEAM_3)
		AND NOT IS_BIT_SET(iLocalBoolCheck31, LBOOL31_AGGRO_HELPTEXT_PLAYED)	
			PRINTLN("[LM][CCTV_CAMERA_BODY_SPOTTED_HELP_TEXT] - iCam: ", iCam, " Setting Help Text bitset.")
			CLEAR_HELP(TRUE)
			IF IS_BIT_SET(iCCTVCameraSpottedBodyBS, iCam)
			OR IS_BIT_SET(MC_ServerBD.iServerBitSet7, SBBOOL7_CCTV_SPOTTED_DEAD_PED_BODY)
				PRINTLN("[ML][CCTV_CAMERA_BODY_SPOTTED_HELP_TEXT] Displaying helptext - CCTV spotted a body")
				PRINT_HELP("SPOTEDCCTVB", 5000)
				SET_BIT(iLocalBoolCheck31, LBOOL31_AGGRO_HELPTEXT_PLAYED)
				SET_BIT(iCCTVCameraSpottedPlayerHelpTextBS, iCam)
			ELSE
				IF iCCTVCameraSpottedPlayer[iCam] > -1
					PARTICIPANT_INDEX partTriggerer = INT_TO_PARTICIPANTINDEX(iCCTVCameraSpottedPlayer[iCam])
					PLAYER_INDEX piTriggerer = NETWORK_GET_PLAYER_INDEX(partTriggerer)	
					PRINTLN("[ML][CCTV_CAMERA_BODY_SPOTTED_HELP_TEXT] Displaying helptext - CCTV spotted a player")
					PRINT_HELP_WITH_PLAYER_NAME("SPOTEDCCTVC", GET_PLAYER_NAME(piTriggerer), HUD_COLOUR_WHITE, 5000)
					SET_BIT(iLocalBoolCheck31, LBOOL31_AGGRO_HELPTEXT_PLAYED)
					SET_BIT(iCCTVCameraSpottedPlayerHelpTextBS, iCam)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_DISPLAY_CCTV_HELPTEXT(FMMC_CCTV_DATA& sCCTVData)
	
	INT iTeam  = MC_playerBD[iPartToUse].iteam
	INT iStartShowRule, iEndShowRule
	INT iCurrentRule = MC_serverBD_4.iCurrentHighestPriority[iTeam]
	
	IF sCCTVData.iCCTVShowHelpTextFromThisRule = -1
		iStartShowRule = 0
	ELIF sCCTVData.iCCTVShowHelpTextFromThisRule > -1 
		iStartShowRule = sCCTVData.iCCTVShowHelpTextFromThisRule
	ELSE
		PRINTLN("[ML][SHOULD_DISPLAY_CCTV_HELPTEXT] Failsafe(1), iCCTVShowHelpTextFromThisRule is < -1")
	ENDIF
	
	IF sCCTVData.iCCTVBlockHelpTextFromThisRule = -1
		iEndShowRule = FMMC_MAX_RULES-1
	ELIF sCCTVData.iCCTVBlockHelpTextFromThisRule > -1 
		iEndShowRule = sCCTVData.iCCTVBlockHelpTextFromThisRule
	ELSE
		PRINTLN("[ML][SHOULD_DISPLAY_CCTV_HELPTEXT] Failsafe(2), iCCTVBlockHelpTextFromThisRule is < -1")
	ENDIF
	
	IF iStartShowRule > iEndShowRule
		PRINTLN("[ML][SHOULD_DISPLAY_CCTV_HELPTEXT] returning FALSE on rule ", iCurrentRule, " as iStartShowRule > iEndShowRule")
		RETURN FALSE
		
	ELIF iCurrentRule >= iStartShowRule
	AND iCurrentRule < iEndShowRule
		PRINTLN("[ML][SHOULD_DISPLAY_CCTV_HELPTEXT] returning TRUE on rule ", iCurrentRule)
		RETURN TRUE
		
	ELSE
		RETURN FALSE
	ENDIF
	
ENDFUNC

FUNC BOOL DOES_CAMERA_HAVE_CLEAR_LOS_TO_ENTITY(FMMC_CCTV_DATA& sCCTVData, ENTITY_INDEX entityIndexTo, ENTITY_INDEX entityIndexFrom)

	IF NOT IS_BIT_SET(sCCTVData.iCCTVBS, ciCCTV_BS_UseEntityLineOfSight)
		RETURN TRUE
	ENDIF
	
	IF HAS_ENTITY_CLEAR_LOS_TO_ENTITY(entityIndexTo, entityIndexFrom)
		PRINTLN("[CCTV] DOES_CAMERA_HAVE_CLEAR_LOS_TO_ENTITY - Has clear line of sight!")
		RETURN TRUE
	ELSE
		#IF IS_DEBUG_BUILD
		IF bCCTVDebug
			TEXT_LABEL_63 tlEntityDebug = "No CCTV LOS"
			DRAW_DEBUG_TEXT_ABOVE_ENTITY(entityIndexTo, tlEntityDebug, 0.2)
			DRAW_DEBUG_LINE(GET_ENTITY_COORDS(entityIndexFrom), GET_ENTITY_COORDS(entityIndexTo), 255, 0, 0)
		ENDIF
		#ENDIF
	ENDIF
	
	
	PRINTLN("[CCTV] DOES_CAMERA_HAVE_CLEAR_LOS_TO_ENTITY - Returning FALSE")
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_OBJECT(INT iObj)
	UNUSED_PARAMETER(iObj)
	
	
	#IF FEATURE_CASINO_HEIST
		//  CASINO_HEIST_OUTFIT_IN__BUGSTAR
		IF  IS_LOCAL_PLAYER_WEARING_A_BUGSTAR_OUTFIT()
		AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetThree, cibsOBJ3_IgnoreDisguise_Bugstar)
			PRINTLN("[RCC MISSION] DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_OBJECT() - OBJECT ignores bugstar disguise")			
			RETURN TRUE
		ENDIF
		
		// CASINO_HEIST_OUTFIT_IN__MECHANIC  
		IF IS_LOCAL_PLAYER_WEARING_A_MAINTENANCE_OUTFIT()
		AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetThree, cibsOBJ3_IgnoreDisguise_Mechanic)
			PRINTLN("[RCC MISSION] DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_OBJECT() - OBJECT ignores mechanic disguise")			
			RETURN TRUE
		ENDIF
		
		//  CASINO_HEIST_OUTFIT_IN__GRUPPE_SECHS
		IF IS_LOCAL_PLAYER_WEARING_A_GRUPPE_SECHS_OUTFIT()
		AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetThree, cibsOBJ3_IgnoreDisguise_GruppeSechs)
			PRINTLN("[RCC MISSION] DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_OBJECT() - OBJECT ignores Gruppe Sechs disguise")			
			RETURN TRUE
		ENDIF
		
		//CASINO_HEIST_OUTFIT_IN__CELEBRITY
		IF IS_LOCAL_PLAYER_WEARING_A_CELEB_OUTFIT()
		AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetThree, cibsOBJ3_IgnoreDisguise_Brucie)
			PRINTLN("[RCC MISSION] DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_OBJECT() - OBJECT ignores Brucie disguise")		
			RETURN TRUE
		ENDIF
		
		// CASINO_HEIST_OUTFIT_OUT__FIREMAN	
		IF IS_LOCAL_PLAYER_WEARING_A_FIREMAN_OUTFIT()
		AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetThree, cibsOBJ3_IgnoreDisguise_Fireman)
			PRINTLN("[RCC MISSION] DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_OBJECT() - OBJECT ignores Fireman disguise")		
			RETURN TRUE
		ENDIF
		
		// CASINO_HEIST_OUTFIT_OUT__HIGH_ROLLER
		IF IS_LOCAL_PLAYER_WEARING_A_HIGH_ROLLER_OUTFIT()
		AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetThree, cibsOBJ3_IgnoreDisguise_HighRoller)
			PRINTLN("[RCC MISSION] DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_OBJECT() - OBJECT ignores High Roller disguise")		
			RETURN TRUE
		ENDIF
		
		//CASINO_HEIST_OUTFIT_OUT__SWAT
		IF IS_LOCAL_PLAYER_WEARING_A_NOOSE_OUTFIT()
		AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetThree, cibsOBJ3_IgnoreDisguise_Noose)
			PRINTLN("[RCC MISSION] DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_OBJECT() - OBJECT ignores Noose disguise")		
			RETURN TRUE
		ENDIF
	#ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL CAN_CCTV_BREAK(OBJECT_INDEX tempObj)
	
	MODEL_NAMES mn = GET_ENTITY_MODEL(tempObj)
	
	IF mn = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_cctv_cam_01a"))
	OR mn = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_cctv_cam_02a"))
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC FLOAT CONVERT_FLOAT_TO_ROTATION_VALUE(FLOAT fInput)
	IF fInput > 360
		fInput -= 360
	ENDIF
	IF fInput < 0
		fInput = (360 + fInput)
	ENDIF
	
	RETURN fInput
ENDFUNC

PROC PROCESS_CCTV_MOVING_CAMERAS(OBJECT_INDEX tempObj, FMMC_CCTV_DATA& sCCTVData, INT iIndex, INT& iCachedCamIndex, FMMC_ELECTRONIC_ENTITY_TYPE eType = FMMC_ELECTRONIC_ENTITY_TYPE__INVALID)
		
	IF NOT IS_BIT_SET(sCCTVData.iCCTVBS, ciCCTV_BS_EnableCCTVMovement)
		EXIT
	ENDIF
	
	BLIP_INDEX biCCTVBlip
	FLOAT fInterpSpeed = sCCTVData.fCCTVTurnSpeed
	VECTOR vStartVec, vEndVec, vForwardVec
	BOOL bStopMoving
	BOOL bSpotted, bAlerted					
	FLOAT fWidthToUse = sCCTVData.fCCTVVisionWidth
	
	INT iTeam = MC_playerBD[iLocalPart].iTeam
	INT iCurrentRule = MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iTeam]
	
	IF IS_ENTITY_ALIVE(tempObj)
		biCCTVBlip = GET_BLIP_FROM_ENTITY(tempObj)
	ENDIF
	
	IF iCachedCamIndex = -1
		INT i
		FOR i = 0 TO (MAX_NUM_CCTV_CAM - 1)
			IF IS_THIS_CAM_THIS_ENTITY(i, iIndex, eType)
				iCachedCamIndex = i
				PRINTLN("[CCTV][CAM ", i, "] PROCESS_CCTV_MOVING_CAMERAS | Caching iIndex ", iIndex, " as camera ", i)
			ELSE
				RELOOP
			ENDIF
		ENDFOR
		
		IF iCachedCamIndex = -1
			ASSERTLN("[CCTV] PROCESS_CCTV_MOVING_CAMERAS | Can't find a proper cam for iIndex ", iIndex)
			PRINTLN("[CCTV][CAM ", i, "] PROCESS_CCTV_MOVING_CAMERAS | Can't find a proper cam for iIndex ", iIndex)
			EXIT
		ENDIF
	ENDIF
	
	INT iCam = iCachedCamIndex
	//FOR iCam = 0 TO (MAX_NUM_CCTV_CAM - 1)
		
		VECTOR vPlayerPosToCheck = GET_ENTITY_COORDS(LocalPlayerPed)
		VECTOR vCamPosToCheck = GET_ENTITY_COORDS(tempObj)
		vCamPosToCheck.z = vPlayerPosToCheck.z
		IF VDIST2(vPlayerPosToCheck, vCamPosToCheck) < (POW(fWidthToUse * 0.65, 2.0))
			fWidthToUse = 1.0
			PRINTLN("[CCTV][CAM ", iCam, "][RCC MISSION] - Using thinner angled area because we're so close to try to match the blip cone slightly better")
		ENDIF
		
		IF iCurrentRule > -1 AND iCurrentRule < FMMC_MAX_RULES
			INT iAlertedTime = g_FMMC_STRUCT.iCCTVDelay
			
			IF ((HAS_NET_TIMER_EXPIRED_READ_ONLY(CCTVSpottedTimer[iCam], iAlertedTime) AND HAS_NET_TIMER_STARTED(CCTVSpottedTimer[iCam]))
			OR (HAS_NET_TIMER_EXPIRED_READ_ONLY(CCTVSpottedBodyTimer[iCam], iAlertedTime) AND HAS_NET_TIMER_STARTED(CCTVSpottedBodyTimer[iCam]))
			OR IS_BIT_SET(iCCTVCameraSpottedPlayerBS, iCam))
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetThirteen[iCurrentRule], ciBS_RULE13_RULE_SHOW_PLAYER_SPOTTED_HELPTEXT)
					CCTV_CAMERA_BODY_SPOTTED_HELP_TEXT(iCam)
				ENDIF
				
				bAlerted = TRUE
			ENDIF
		ENDIF
		
		IF HAS_NET_TIMER_STARTED(CCTVSpottedTimer[iCam])
		OR HAS_NET_TIMER_STARTED(CCTVSpottedBodyTimer[iCam])
		OR IS_BIT_SET(iCCTVCameraSpottedPlayerBS, iCam)
		OR IS_BIT_SET(iCCTVCameraSpottedBodyBS, iCam)
			bSpotted = TRUE
		ENDIF
		
		#IF IS_DEBUG_BUILD
		IF bCCTVDebug
			IF MC_serverBD.iCameraNumber[iCam] > -1
				IF MC_serverBD.eCameraType[iCam] = FMMC_ELECTRONIC_ENTITY_TYPE__OBJECT
					TEXT_LABEL_63 tlDebugText3D = "OBJ | CCTV Cam "
					tlDebugText3D += iCam
					tlDebugText3D += "/"
					tlDebugText3D += iIndex
					IF IS_BIT_SET(iCCTVBrokenBS, iCam)
						tlDebugText3D += " BROKEN"
					ENDIF
					DRAW_DEBUG_TEXT(tlDebugText3D, g_FMMC_STRUCT_ENTITIES.sPlacedObject[MC_serverBD.iCameraNumber[iCam]].vPos)
					
				ELIF MC_serverBD.eCameraType[iCam] = FMMC_ELECTRONIC_ENTITY_TYPE__DYNOPROP
					TEXT_LABEL_63 tlDebugText3D = "DP | CCTV Cam "
					tlDebugText3D += iCam
					tlDebugText3D += "/"
					tlDebugText3D += iIndex
					IF IS_BIT_SET(iCCTVBrokenBS, iCam)
						tlDebugText3D += " BROKEN"
					ENDIF
					DRAW_DEBUG_TEXT(tlDebugText3D, g_FMMC_STRUCT_ENTITIES.sPlacedDynoProp[MC_serverBD.iCameraNumber[iCam]].vPos)
					
				ENDIF

				
				TEXT_LABEL_63 tlEntityDebug = "| "
				IF HAS_NET_TIMER_STARTED(CCTVSpottedTimer[iCam])
					tlEntityDebug += "Spotted Timer /"
				ENDIF
				IF HAS_NET_TIMER_STARTED(CCTVSpottedBodyTimer[iCam])
					tlEntityDebug += "Body Timer /"
				ENDIF
				tlEntityDebug += " |"
				DRAW_DEBUG_TEXT_ABOVE_ENTITY(tempObj, tlEntityDebug, -0.25)
				
				tlEntityDebug = "fCCTVCamInitialHeading: "
				tlEntityDebug += FLOAT_TO_STRING(fCCTVCamInitialHeading[iCam])
				tlEntityDebug += " / "
				IF IS_BIT_SET(MC_serverBD.CCTV_TURNED_BITSET, ci_CCTV_FINISHED_TURN_CAM_0 + iCam)
					tlEntityDebug += "Turned"
				ELSE
					tlEntityDebug += "Not turned"
				ENDIF
				DRAW_DEBUG_TEXT_ABOVE_ENTITY(tempObj, tlEntityDebug, -0.5)
				
				tlEntityDebug = "fCCTVTurnAmount: "
				tlEntityDebug += FLOAT_TO_STRING(sCCTVData.fCCTVTurnAmount)
				DRAW_DEBUG_TEXT_ABOVE_ENTITY(tempObj, tlEntityDebug, -1.0)
			ENDIF
		ENDIF
		#ENDIF
		
		IF NOT IS_BIT_SET(iCCTVBrokenBS, iCam)
			IF IS_ENTITY_DEAD(tempObj)
			OR (IS_OBJECT_BROKEN_AND_VISIBLE(tempObj, TRUE) AND NOT IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_CASINO_HEIST))
				
				IF NETWORK_HAS_CONTROL_OF_ENTITY(tempObj)
				
					PRINTLN("[CCTV][CAM ", iCam, "] PROCESS_CCTV_MOVING_CAMERAS | Breaking cam ", iCam)
					
					IF CAN_CCTV_BREAK(tempObj)
						BREAK_OBJECT_FRAGMENT_CHILD(tempObj, 1, FALSE)
						BREAK_OBJECT_FRAGMENT_CHILD(tempObj, 2, FALSE)
						
						DAMAGE_OBJECT_FRAGMENT_CHILD(tempObj, 0)
						DAMAGE_OBJECT_FRAGMENT_CHILD(tempObj, 1)
						DAMAGE_OBJECT_FRAGMENT_CHILD(tempObj, 2)
					ELSE
						APPLY_FORCE_TO_ENTITY(tempObj, APPLY_TYPE_EXTERNAL_IMPULSE, (<<0, 0, 1>>), <<0,0,0>>, 0, FALSE, FALSE, FALSE)
						APPLY_FORCE_TO_ENTITY(tempObj, APPLY_TYPE_EXTERNAL_IMPULSE, (<<0, 0, 1>>), <<0,0,0>>, 1, FALSE, FALSE, FALSE)
						APPLY_FORCE_TO_ENTITY(tempObj, APPLY_TYPE_EXTERNAL_IMPULSE, (<<0, 0, 1>>), <<0,0,0>>, 2, FALSE, FALSE, FALSE)
					ENDIF
					
					SET_ENTITY_HEALTH(tempObj, 0)
					
					CLEANUP_CCTV_CAMERA_SOUND_EFFECT_FOR_DEATH(iCam)
				ENDIF
				
				IF bIsLocalPlayerHost
					MC_serverBD_2.iCamObjDestroyed++
					PRINTLN("[CCTV][CAM ", iCam, "] PROCESS_CCTV_MOVING_CAMERAS | Host has detected that ", iCam, " is broken/dead | iCamObjDestroyed is now ", MC_serverBD_2.iCamObjDestroyed, " | g_FMMC_STRUCT.iCCTVDestroyCountToCauseAggro is ", g_FMMC_STRUCT.iCCTVDestroyCountToCauseAggro)
					
					IF g_FMMC_STRUCT.iCCTVDestroyCountToCauseAggro > -1
					AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_AGGRO_TRIGGERED_FOR_TEAM_0)
						IF MC_serverBD_2.iCamObjDestroyed >= g_FMMC_STRUCT.iCCTVDestroyCountToCauseAggro
							SET_BIT(MC_serverBD.iServerBitSet1, SBBOOL1_AGGRO_TRIGGERED_FOR_TEAM_0)
							SET_BIT(MC_serverBD.iServerBitSet1, SBBOOL1_AGGRO_TRIGGERED_FOR_TEAM_1)
							SET_BIT(MC_serverBD.iServerBitSet1, SBBOOL1_AGGRO_TRIGGERED_FOR_TEAM_2)
							SET_BIT(MC_serverBD.iServerBitSet1, SBBOOL1_AGGRO_TRIGGERED_FOR_TEAM_3)
							PRINTLN("[CCTV][CAM ", iCam, "] PROCESS_CCTV_MOVING_CAMERAS - Destroyed too many cameras! Triggering aggro now")
						ENDIF
					ENDIF
				ENDIF
				
				SET_BIT(iCCTVBrokenBS, iCam)
				PLAY_SOUND_FROM_COORD(-1, "Camera_Destroy", GET_ENTITY_COORDS(tempObj, FALSE), "DLC_HEIST_FLEECA_SOUNDSET", FALSE, 0)
				PRINTLN("[CCTV][CAM ", iCam, "] PROCESS_CCTV_MOVING_CAMERAS | Setting iCCTVBrokenBS ", iCam)
				EXIT
			ENDIF
		ELSE
			// Exiting because this camera is dead or broken
			PRINTLN("[CCTV][CAM ", iCam, "] PROCESS_CCTV_MOVING_CAMERAS | Camera ", iCam, " is dead or broken")
			EXIT
		ENDIF
		
		IF NOT SHOULD_ELECTRONIC_BE_DISABLED_THIS_FRAME(iIndex, eType)
		AND NOT IS_BIT_SET(iCCTVCameraSpottedPlayerBS, iCam)
			
			IF DOES_BLIP_EXIST(biCCTVBlip)
				
				SET_BLIP_ROTATION_WITH_FLOAT(biCCTVBlip, GET_ENTITY_HEADING(tempObj))
				
				IF IS_BIT_SET(sCCTVData.iCCTVBS, ciCCTV_BS_OverrideBlipColour)
					IF DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_CCTV_CAMERA(sCCTVData)
						SET_BLIP_COLOUR(biCCTVBlip, BLIP_COLOUR_WHITE)
					ELSE
						SET_BLIP_COLOUR(biCCTVBlip, BLIP_COLOUR_RED)
					ENDIF
				ENDIF
				
				IF NOT bAlerted
				
					FLOAT fWidthForCone = sCCTVData.fCCTVVisionWidth / 5.0
					
					IF NOT IS_BIT_SET(iCCTVConeBitSet, ci_CCTV_CONE_BLIP_ADDED_0 + iCam)
						
						IF IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_CASINO_HEIST)
							PRINTLN("[CCTV][CAM ", iCam, "][RCC MISSION] PROCESS_CCTV_MOVING_CAMERAS - iCam: ", iCam, " - Initial fake cone setup | fWidthForCone: ", fWidthForCone, " | fCCTVVisionRange: ", sCCTVData.fCCTVVisionRange)
							ADD_FAKE_VISION_CONE_TO_BLIP(biCCTVBlip, -1.0, 1.0, fWidthForCone, 1.0, (sCCTVData.fCCTVVisionRange * 1.1), DEG_TO_RAD(GET_ENTITY_HEADING(tempObj) + 180), TRUE)
						ELSE
							PRINTLN("[CCTV][CAM ", iCam, "][RCC MISSION] PROCESS_CCTV_MOVING_CAMERAS - iCam: ", iCam, " - Initial fake cone setup (OLD) | fWidthForCone: ", fWidthForCone, " | fCCTVVisionRange: ", sCCTVData.fCCTVVisionRange)
							ADD_FAKE_VISION_CONE_TO_BLIP(biCCTVBlip, -1, 1, 0.5, 1, (sCCTVData.fCCTVVisionRange * 1.1), DEG_TO_RAD(GET_ENTITY_HEADING(tempObj) + 180), TRUE)
						ENDIF
						SET_BIT(iCCTVConeBitSet, ci_CCTV_CONE_BLIP_ADDED_0 + iCam)
					ELSE
						FLOAT fRotation = DEG_TO_RAD(GET_ENTITY_HEADING(tempObj) + 180)
						
						IF IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_CASINO_HEIST)
							SETUP_FAKE_CONE_DATA(biCCTVBlip, -1.0, 1.0, fWidthForCone, 1.0, (sCCTVData.fCCTVVisionRange * 1.1), fRotation, TRUE)
						ELSE
							SETUP_FAKE_CONE_DATA(biCCTVBlip, -1.0, 1.0, 0.5, 1.0, (sCCTVData.fCCTVVisionRange * 1.1), fRotation, TRUE)
						ENDIF
					ENDIF
				ENDIF
				
			ELSE
				// 4060349
				CLEAR_BIT(iCCTVConeBitSet, ci_CCTV_CONE_BLIP_ADDED_0 + iCam)
			ENDIF
		ENDIF
		
		IF SHOULD_ELECTRONIC_BE_DISABLED_THIS_FRAME(iIndex, eType)
			IF NOT HAS_ENTITY_REACTED_TO_TASER(iIndex, eType)
				fCCTV_CurrentTargetLightIntensity[iCam] = cf_CCTV_TaserFlashIntensity
				REMOVE_CCTV_VISION_CONE(biCCTVBlip, iIndex, eType)
				
				SET_ENTITY_HAS_REACTED_TO_TASER(iIndex, eType)
				PRINTLN("[CCTV][CAM ", iCam, "][RCC MISSION][Taser] PROCESS_CCTV_MOVING_CAMERAS - iCam: ", iCam, " | Setting sObjTaserVars.iReactedToTaserBS!")
			ENDIF
		ELSE
			IF HAS_ENTITY_REACTED_TO_TASER(iIndex, eType)
				REINIT_NET_TIMER(CCTVTaserReEnablingTimer[iCam])
				
				CLEAR_ENTITY_HAS_REACTED_TO_TASER(iIndex, eType)
				PRINTLN("[CCTV][CAM ", iCam, "][RCC MISSION][Taser] PROCESS_CCTV_MOVING_CAMERAS - iCam: ", iCam, " | Starting CCTVTaserReEnablingTimer")
			ENDIF
		ENDIF
		
		IF bAlerted
			REMOVE_CCTV_VISION_CONE(biCCTVBlip, iIndex, eType)
		ENDIF
						
		SWITCH MC_serverBD.eCCTVCamState[iCam]
			CASE CamState_Wait
			
				IF sCCTVData.fCCTVTurnAmount != 0.0
					IF bIsLocalPlayerHost
						IF NOT HAS_NET_TIMER_STARTED(CCTVCameraTurnTimer[iCam])
							REINIT_NET_TIMER(CCTVCameraTurnTimer[iCam])
							
						ELSE
							INT iWaitTimer
							iWaitTimer = ROUND(sCCTVData.fCCTVWaitTime * 1000)
							IF HAS_NET_TIMER_EXPIRED(CCTVCameraTurnTimer[iCam], iWaitTimer)
								PRINTLN("[CCTV][CAM ", iCam, "][RCC MISSION] - iCam: ", iCam, " Sending Cam to Turning")
								MC_serverBD.eCCTVCamState[iCam] = CamState_Turning
								RESET_NET_TIMER(CCTVCameraTurnTimer[iCam])
							ENDIF
						ENDIF
					ENDIF
					
					IF fCCTVCamInitialHeading[iCam] = 0.0
						fCCTVCamInitialHeading[iCam] = GET_ENTITY_HEADING(tempObj)
						PRINTLN("[CCTV][CAM ", iCam, "][RCC MISSION] - iCam: ", iCam, " initialising fCCTVCamInitialHeading as ", fCCTVCamInitialHeading[iCam])
					ENDIF
				ENDIF
				
			BREAK
			CASE CamState_Turning
				bStopMoving = FALSE
				IF IS_BIT_SET(sCCTVData.iCCTVBS, ciCCTV_BS_StopMovingWhenSpotting)
					IF HAS_NET_TIMER_STARTED(CCTVSpottedBodyTimer[iCam])
					OR HAS_NET_TIMER_STARTED(CCTVSpottedTimer[iCam])
						bStopMoving = TRUE
					ENDIF
				ENDIF
				
				IF SHOULD_ELECTRONIC_BE_DISABLED_THIS_FRAME(iIndex, eType)
					bStopMoving = TRUE
				ENDIF
				
				IF NOT bStopMoving
					FLOAT fTargetHeading
					FLOAT fTempheading
					
					IF IS_BIT_SET(MC_serverBD.CCTV_TURNED_BITSET, ci_CCTV_FINISHED_TURN_CAM_0 + iCam)
						fTargetHeading = fCCTVCamInitialHeading[iCam]
					ELSE
						fTargetHeading = fCCTVCamInitialHeading[iCam] + sCCTVData.fCCTVTurnAmount
					ENDIF
					
					IF fTargetHeading < 0
						fTargetHeading = 360-(ABSF(fTargetHeading))
					ELIF fTargetHeading > 360
						fTargetHeading = (fTargetHeading)-360
					ENDIF
					
					#IF IS_DEBUG_BUILD
					IF bCCTVDebug
						TEXT_LABEL_63 tlEntityDebug
						tlEntityDebug = "Heading: "
						tlEntityDebug += FLOAT_TO_STRING(GET_ENTITY_HEADING_FROM_EULERS(tempObj))
						DRAW_DEBUG_TEXT_ABOVE_ENTITY(tempObj, tlEntityDebug, -2.8)
						
						tlEntityDebug = "Target: "
						tlEntityDebug += FLOAT_TO_STRING(fTargetHeading)
						tlEntityDebug += " | Current: "
						tlEntityDebug += FLOAT_TO_STRING(GET_ENTITY_HEADING_FROM_EULERS(tempObj))
						DRAW_DEBUG_TEXT_ABOVE_ENTITY(tempObj, tlEntityDebug, -1.2)
					ENDIF
					#ENDIF
					
					IF IS_FLOAT_IN_RANGE(GET_ENTITY_HEADING(tempObj), fTargetHeading - cfCCTVRotationTargetBuffer, fTargetHeading + cfCCTVRotationTargetBuffer)
					OR (bIsLocalPlayerHost AND IS_BIT_SET(iCCTVReachedDestinationBS, iCam))
						IF bIsLocalPlayerHost
							PRINTLN("[CCTV][CAM ", iCam, "][RCC MISSION] - iCam: ", iCam, " Sending Cam to Wait.")
							MC_serverBD.eCCTVCamState[iCam] = CamState_Wait
							
							IF IS_BIT_SET(iCCTVReachedDestinationBS, iCam)
								CLEAR_BIT(iCCTVReachedDestinationBS, iCam)
								PRINTLN("[CCTV_SPAM][CAM ", iCam, "_SPAM][RCC MISSION] - iCam: ", iCam, " Host clearing its iCCTVReachedDestinationBS (1)")
							ENDIF
							
							IF IS_BIT_SET(MC_serverBD.CCTV_TURNED_BITSET, ci_CCTV_FINISHED_TURN_CAM_0+iCam)
								CLEAR_BIT(MC_serverBD.CCTV_TURNED_BITSET, ci_CCTV_FINISHED_TURN_CAM_0+iCam)
							ELSE
								SET_BIT(MC_serverBD.CCTV_TURNED_BITSET, ci_CCTV_FINISHED_TURN_CAM_0+iCam)
							ENDIF
						ELSE
							IF NOT IS_BIT_SET(iCCTVReachedDestinationBS, iCam)
								SET_BIT(iCCTVReachedDestinationBS, iCam)
								PRINTLN("[CCTV][CAM ", iCam, "][RCC MISSION] - iCam: ", iCam, " Setting iCCTVReachedDestinationBS")
								
								IF NETWORK_HAS_CONTROL_OF_ENTITY(tempObj)
									BROADCAST_FMMC_CCTV_REACHED_DESTINATION(iCam)
									PRINTLN("[CCTV][CAM ", iCam, "][RCC MISSION] - iCam: ", iCam, " Sending event to say this cam has reached its destination.")
								ENDIF
							ELSE
								PRINTLN("[CCTV][CAM ", iCam, "][RCC MISSION] - iCam: ", iCam, " Waiting for host to notice that it has reached its destination.")
							ENDIF
						ENDIF
					ELSE
						
						IF IS_BIT_SET(iCCTVReachedDestinationBS, iCam)
							CLEAR_BIT(iCCTVReachedDestinationBS, iCam)
							PRINTLN("[CCTV_SPAM][CAM ", iCam, "_SPAM][RCC MISSION] - iCam: ", iCam, " Clearing its iCCTVReachedDestinationBS (2)")
						ENDIF
						
						fTempheading = GET_ENTITY_HEADING_FROM_EULERS(tempObj)
						
						FLOAT fMovementThisFrame
						fMovementThisFrame = (fLastFrameTime * fInterpSpeed * cfCCTVTurnSpeedScalar)
						
						IF (NOT IS_BIT_SET(MC_serverBD.CCTV_TURNED_BITSET, ci_CCTV_FINISHED_TURN_CAM_0 + iCam) AND sCCTVData.fCCTVTurnAmount > 0.0)
						OR (IS_BIT_SET(MC_serverBD.CCTV_TURNED_BITSET, ci_CCTV_FINISHED_TURN_CAM_0 + iCam) AND sCCTVData.fCCTVTurnAmount < 0.0)
							// Moving out
							IF fTargetHeading > fTempHeading
								fTempheading = CLAMP(CONVERT_FLOAT_TO_ROTATION_VALUE(fTempHeading) + fMovementThisFrame, fTempheading, fTargetHeading)
								
								#IF IS_DEBUG_BUILD
								IF bCCTVDebug
									PRINTLN("[CCTV_MOVEMENT][CAM ", iCam, "][RCC MISSION] - Adding ", fMovementThisFrame, ", clamping at ", fTargetHeading)
								ENDIF
								#ENDIF
							ELSE
								fTempheading = fTempheading + fMovementThisFrame
								
								#IF IS_DEBUG_BUILD
								IF bCCTVDebug
									PRINTLN("[CCTV_MOVEMENT][CAM ", iCam, "][RCC MISSION] - Adding ", fMovementThisFrame)
								ENDIF
								#ENDIF
							ENDIF
						ELSE
							// Coming back
							IF fTargetHeading < fTempHeading
								fTempheading = CLAMP(CONVERT_FLOAT_TO_ROTATION_VALUE(fTempHeading) - fMovementThisFrame, fTargetHeading, fTempheading)
								
								#IF IS_DEBUG_BUILD
								IF bCCTVDebug
									//PRINTLN("[CCTV_MOVEMENT][CAM ", iCam, "][RCC MISSION] - Subtracting ", fMovementThisFrame, ", clamping at ", fTargetHeading)
								ENDIF
								#ENDIF
							ELSE
								fTempheading = fTempheading - fMovementThisFrame
								
								#IF IS_DEBUG_BUILD
								IF bCCTVDebug
									//PRINTLN("[CCTV_MOVEMENT][CAM ", iCam, "][RCC MISSION] - Subtracting ", fMovementThisFrame)
								ENDIF
								#ENDIF
							ENDIF
						ENDIF
						
						IF NETWORK_HAS_CONTROL_OF_ENTITY(tempObj)
							FREEZE_ENTITY_POSITION(tempObj, FALSE)
							SET_ENTITY_HEADING(tempObj, fTempheading)
							
							#IF IS_DEBUG_BUILD
							IF bCCTVDebug
								PRINTLN("[CCTV][CAM ", iCam, "][RCC MISSION] - Setting heading to ", fTempheading)
							ENDIF
							#ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
		
		IF IS_BIT_SET(sCCTVData.iCCTVBS, ciCCTV_BS_CanAggro)
			vStartVec = GET_ENTITY_COORDS(tempObj) + (GET_ENTITY_FORWARD_VECTOR(tempObj) * -0.1)
			vForwardVec = GET_ENTITY_FORWARD_VECTOR(tempObj)
			vForwardVec = GET_VECTOR_OF_LENGTH(vForwardVec, -sCCTVData.fCCTVVisionRange)
			vEndVec = vStartVec + vForwardVec
			vEndVec.z = vEndVec.z - 5
			#IF IS_DEBUG_BUILD
			IF debugState = eControllerDebugState_OVERVIEW
			OR bCCTVDebug
				DRAW_ANGLED_AREA(vStartVec, vEndVec, fWidthToUse, 255, 0, 0, 75)
			ENDIF
			#ENDIF
			
			//Drawing of lights // 3974626
			BOOL bDrawLights = NOT IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_CASINO_HEIST)
			BOOL bFlicker = FALSE
			
			IF SHOULD_ELECTRONIC_BE_DISABLED_THIS_FRAME(iIndex, eType)
				bFlicker = TRUE
			ENDIF
			
			IF HAS_NET_TIMER_STARTED(CCTVTaserReEnablingTimer[iCam])
				IF HAS_NET_TIMER_EXPIRED(CCTVTaserReEnablingTimer[iCam], ROUND(cf_CCTV_ReenableFlickerTime * 1000))
					RESET_NET_TIMER(CCTVTaserReEnablingTimer[iCam])
					PRINTLN("[CCTV][CAM ", iCam, "][RCC MISSION][Taser] PROCESS_CCTV_MOVING_CAMERAS - iCam: ", iCam, " | Fully re-enabled!")
				ELSE
					bFlicker = TRUE
				ENDIF
			ENDIF
			
			IF bFlicker
				IF ABSF(fCCTV_CurrentLightIntensity[iCam] - fCCTV_CurrentTargetLightIntensity[iCam]) < GET_RANDOM_FLOAT_IN_RANGE(0.1, 2.5)
					fCCTV_CurrentLightIntensity[iCam] = fCCTV_CurrentTargetLightIntensity[iCam]
					fCCTV_CurrentTargetLightIntensity[iCam] = GET_RANDOM_FLOAT_IN_RANGE(0.0, cf_CCTV_FlickerLightIntensity)
				ENDIF
				
				FLOAT fTaserLightReduction
				IF HAS_ENTITY_BEEN_HIT_BY_TASER(iIndex, eType)
					fTaserLightReduction = GET_ENTITY_TASER_TIMER_DIFFERENCE_WITH_CURRENT_TIME(iIndex, eType) * 0.001
					fTaserLightReduction *= cf_CCTV_TaserLightFadeSpeed
					
				ELIF SHOULD_ELECTRONIC_BE_DISABLED_THIS_FRAME(iIndex, eType)
					fTaserLightReduction = 9999.0
				
				ELIF HAS_NET_TIMER_STARTED(CCTVTaserReEnablingTimer[iCam])
					fTaserLightReduction = (cf_CCTV_ReenableFlickerTime - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(CCTVTaserReEnablingTimer[iCam])) * 0.001
					
				ENDIF
				
				fCCTV_CurrentTargetLightIntensity[iCam] -= fTaserLightReduction
				fCCTV_CurrentTargetLightIntensity[iCam] = CLAMP(fCCTV_CurrentTargetLightIntensity[iCam], 0.0, cf_CCTV_FlickerLightIntensity)
			ELSE
				fCCTV_CurrentTargetLightIntensity[iCam] = cf_CCTV_BaseLightIntensity
			ENDIF
			fCCTV_CurrentLightIntensity[iCam] = LERP_FLOAT(fCCTV_CurrentLightIntensity[iCam], fCCTV_CurrentTargetLightIntensity[iCam], cf_CCTV_LightLerpSpeed * GET_FRAME_TIME())
			
			IF bDrawLights
				VECTOR vLightPos = LERP_VECTOR(vStartVec, vEndVec, 0.5) //Place the light in the middle of the vision area
				
				//Making sure the light is above the ground + all lights consistent
				GET_GROUND_Z_FOR_3D_COORD(vLightPos, vLightPos.z, FALSE)
				vLightPos.z += 1.1
				
				INT iR = 255
				INT iG = 255
				INT iB = 255
				
				IF bAlerted
				AND NOT SHOULD_ELECTRONIC_BE_DISABLED_THIS_FRAME(iIndex, eType)
					iR = 255
					iG = 0
					iB = 0
					fCCTV_CurrentTargetLightIntensity[iCam] = cf_CCTV_BaseLightIntensity * 2.0
				ENDIF
				
				DRAW_LIGHT_WITH_RANGE(vLightPos, iR, iG, iB, (sCCTVData.fCCTVVisionRange + fWidthToUse) * 1.25, fCCTV_CurrentLightIntensity[iCam])
			ENDIF
			
			IF NOT IS_BIT_SET(iCCTVCameraSpottedPlayerBS, iCam)
			AND SHOULD_DISPLAY_CCTV_HELPTEXT(sCCTVData)
			
				IF IS_ENTITY_IN_ANGLED_AREA(LocalPlayerPed, vStartVec, vEndVec, fWidthToUse)
				AND DOES_CAMERA_HAVE_CLEAR_LOS_TO_ENTITY(sCCTVData, tempObj, LocalPlayerPed)
				AND NOT SHOULD_ELECTRONIC_BE_DISABLED_THIS_FRAME(iIndex, eType)
				AND NOT DOES_PLAYER_HAVE_VALID_DISGUISE_FOR_THIS_CCTV_CAMERA(sCCTVData)
					IF NOT HAS_NET_TIMER_STARTED(CCTVSpottedTimer[iCam])
						PRINTLN("[CCTV][CAM ", iCam, "][RCC MISSION] - (Player has been spotted by a CCTV camera) (Starting Timer)")
						REINIT_NET_TIMER(CCTVSpottedTimer[iCam])
						
						IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_RespectCCTVSpottedDelay)
							PRINTLN("[CCTV][CAM ", iCam, "][RCC MISSION] - (Player has been spotted by a CCTV camera) (Broadcasting Event)")
							BROADCAST_FMMC_CCTV_SPOTTED(PLAYER_ID(), iCam)
							SET_BIT(iCCTVCameraSpottedPlayerBS, iCam)
						ENDIF
					ELSE
						IF HAS_NET_TIMER_EXPIRED_READ_ONLY(CCTVSpottedTimer[iCam], g_FMMC_STRUCT.iCCTVDelay)								
							IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_RespectCCTVSpottedDelay)
								PRINTLN("[CCTV][CAM ", iCam, "][RCC MISSION] - (Player has been spotted by a CCTV camera) (After Delay Broadcasting Event)")
								BROADCAST_FMMC_CCTV_SPOTTED(PLAYER_ID(), iCam)
								SET_BIT(iCCTVCameraSpottedPlayerBS, iCam)
								RESET_NET_TIMER(CCTVSpottedTimer[iCam])
							ELSE
								RESET_NET_TIMER(CCTVSpottedTimer[iCam])
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_RespectCCTVSpottedDelay)
					AND NOT HAS_NET_TIMER_EXPIRED_READ_ONLY(CCTVSpottedTimer[iCam], g_FMMC_STRUCT.iCCTVDelay)
						RESET_NET_TIMER(CCTVSpottedTimer[iCam])
					ENDIF
					
				ENDIF
			ENDIF
			
			IF iStaggeredCCTVPedToCheck[iCam] > -1
			AND iStaggeredCCTVPedToCheck[iCam] < MC_serverBD.iNumPedCreated
			AND SHOULD_DISPLAY_CCTV_HELPTEXT(sCCTVData)
			
				iStaggeredCCTVPedToCheck[iCam]++
				
				IF iStaggeredCCTVPedToCheck[iCam] >= MC_serverBD.iNumPedCreated
					iStaggeredCCTVPedToCheck[iCam] = 0
				ENDIF
				
				IF IS_BIT_SET(sCCTVData.iCCTVBS, ciCCTV_BS_CCTVTriggerWithBody)	
					IF NOT IS_BIT_SET(iCCTVCameraSpottedPlayerBS, iCam)
						IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niPed[iStaggeredCCTVPedToCheck[iCam]])
							PED_INDEX tempPed = NET_TO_PED(MC_serverBD_1.sFMMC_SBD.niPed[iStaggeredCCTVPedToCheck[iCam]])
							IF DOES_ENTITY_EXIST(tempPed)
								IF IS_ENTITY_IN_ANGLED_AREA(tempPed, vStartVec, vEndVec, fWidthToUse)
								AND DOES_CAMERA_HAVE_CLEAR_LOS_TO_ENTITY(sCCTVData, tempObj, tempPed)
								AND NOT SHOULD_ELECTRONIC_BE_DISABLED_THIS_FRAME(iIndex, eType)
									IF IS_PED_INJURED(tempPed)
										IF NOT HAS_NET_TIMER_STARTED(CCTVSpottedBodyTimer[iCam])
											PRINTLN("[CCTV_SPAM][CAM ", iCam, "_SPAM][RCC MISSION][PROCESS_CCTV_MOVING_CAMERAS] (Body Spotting) Starting body spotted timers due to ped ", iStaggeredCCTVPedToCheck[iCam])
											START_NET_TIMER(CCTVSpottedResetTimer)
											START_NET_TIMER(CCTVSpottedBodyTimer[iCam])
										ENDIF
									ENDIF
								ELSE
									PRINTLN("[CCTV_SPAM][CAM ", iCam, "_SPAM][RCC MISSION][PROCESS_CCTV_MOVING_CAMERAS] (Body Spotting) Ped ", iStaggeredCCTVPedToCheck[iCam], " isn't in zone or isn't valid")
								ENDIF
							ELSE
								PRINTLN("[CCTV_SPAM][CAM ", iCam, "_SPAM][RCC MISSION][PROCESS_CCTV_MOVING_CAMERAS] (Body Spotting) entity doesn't exist for ped ", iStaggeredCCTVPedToCheck[iCam])
							ENDIF
						ELSE
							PRINTLN("[CCTV_SPAM][CAM ", iCam, "_SPAM][RCC MISSION][PROCESS_CCTV_MOVING_CAMERAS] (Body Spotting) net id doesn't exist for ped ", iStaggeredCCTVPedToCheck[iCam])
						ENDIF
					ELSE
						PRINTLN("[CCTV_SPAM][CAM ", iCam, "_SPAM][RCC MISSION][PROCESS_CCTV_MOVING_CAMERAS] (Body Spotting) iCCTVCameraSpottedPlayerBS is set")
					ENDIF
				ELSE
					PRINTLN("[CCTV_SPAM][CAM ", iCam, "_SPAM][RCC MISSION][PROCESS_CCTV_MOVING_CAMERAS] (Body Spotting) Doesn't have ciCCTV_BS_CCTVTriggerWithBody set")
				ENDIF
			ENDIF
			
			IF HAS_NET_TIMER_STARTED(CCTVSpottedBodyTimer[iCam])
			AND HAS_NET_TIMER_EXPIRED_READ_ONLY(CCTVSpottedBodyTimer[iCam], g_FMMC_STRUCT.iCCTVDelay)
				IF bIsLocalPlayerHost
				AND NOT IS_BIT_SET(MC_ServerBD.iServerBitSet6, SBBOOL6_PLAYER_HAS_BEEN_SPOTTED_CCTV)
				AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_RespectCCTVSpottedDelay)
					PRINTLN("[CCTV][CAM ", iCam, "][RCC MISSION][PROCESS_CCTV_MOVING_CAMERAS] Setting SBBOOL6_PLAYER_HAS_BEEN_SPOTTED_CCTV (body).")
					SET_BIT(MC_ServerBD.iServerBitSet6, SBBOOL6_PLAYER_HAS_BEEN_SPOTTED_CCTV)
				ENDIF
				
				IF bIsLocalPlayerHost
				AND NOT IS_BIT_SET(MC_ServerBD.iServerBitSet7, SBBOOL7_CCTV_SPOTTED_DEAD_PED_BODY)									
				AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_RespectCCTVSpottedDelay)
					PRINTLN("[CCTV][CAM ", iCam, "][RCC MISSION][PROCESS_CCTV_MOVING_CAMERAS] Setting SBBOOL7_CCTV_SPOTTED_DEAD_PED_BODY (body).")
					SET_BIT(MC_ServerBD.iServerBitSet7, SBBOOL7_CCTV_SPOTTED_DEAD_PED_BODY)
				ENDIF
				
				IF NOT IS_BIT_SET(iCCTVCameraSpottedPlayerBS, iCam)
					PRINTLN("[CCTV][CAM ", iCam, "][RCC MISSION][PROCESS_CCTV_MOVING_CAMERAS] Setting iCCTVCameraSpottedPlayerBS for iCam: ", iCam, ". (body)")
					SET_BIT(iCCTVCameraSpottedPlayerBS, iCam)
					SET_BIT(iCCTVCameraSpottedBodyBS, iCam)
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(MC_ServerBD.iServerBitSet6, SBBOOL6_PLAYER_HAS_BEEN_SPOTTED_CCTV)
			OR IS_BIT_SET(MC_ServerBD.iServerBitSet7, SBBOOL7_CCTV_SPOTTED_DEAD_PED_BODY)	
				IF sCCTVData.iCCTVSkipToRule > -1 AND sCCTVData.iCCTVSkipToRule < FMMC_MAX_RULES
					INT iTeamToSkip
					FOR iTeamToSkip = 0 TO FMMC_MAX_TEAMS-1
						IF sCCTVData.iCCTVSkipToRule > MC_serverBD_4.iCurrentHighestPriority[iTeamToSkip] AND MC_serverBD_4.iCurrentHighestPriority[iTeamToSkip] < FMMC_MAX_RULES
							//Past content always forced passed so leaving as it was.
							//Force passing causes mission branching problems
							BOOL bForcePass = NOT IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_CASINO_HEIST)
							INVALIDATE_UP_TO_AND_INCLUDING_OBJECTIVE(iTeamToSkip, sCCTVData.iCCTVSkipToRule - 1, FALSE, bForcePass, FALSE)
							RECALCULATE_OBJECTIVE_LOGIC(iTeamToSkip)
						ENDIF
					ENDFOR
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_ShowCCTVLightCones)
				DRAW_CAMERA_VIEW_MARKERS(iCam, bSpotted, bAlerted, fCCTV_CurrentTargetLightIntensity[iCam], tempObj)
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_CCTVAlertSpottedSounds)
				PROCESS_CCTV_CAMERA_ALARM_SOUNDS(bSpotted, bAlerted, tempObj, iCam, iIndex, eType)
			ENDIF
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_TriggerAllCCTVCameras)
			AND IS_BIT_SET(MC_serverBD.iServerBitSet7, SBBOOL7_CCTV_TEAM_AGGROED_PEDS)
				IF NOT IS_BIT_SET(iLocalBoolCheck31, LBOOL31_HAS_TRIGGERED_ALL_CCTV_CAMERAS)
					PRINTLN("[CCTV][CAM ", iCam, "][RCC MISSION][PROCESS_CCTV_MOVING_CAMERAS] Peds have been aggro'd calling TRIGGER_ALL_CCTV_CAMERAS.")
					TRIGGER_ALL_CCTV_CAMERAS()
				ENDIF
			ENDIF
		ENDIF
	//ENDFOR
ENDPROC

PROC EXPLODE_SEA_MINE(OBJECT_INDEX& tempObj, VECTOR& vMineCoords, INT& iMineIndex)
	PRINTLN("mine ", iMineIndex, " is about to try to explode")
	
	IF NETWORK_HAS_CONTROL_OF_ENTITY(tempObj)		
		IF NOT IS_BIT_SET(iSeaMine_HasExploded, iMineIndex)
			SET_ENTITY_HEALTH(tempObj, 1)
			ADD_OWNED_EXPLOSION(LocalPlayerPed, vMineCoords, EXP_TAG_MINE_UNDERWATER, 8)
			
			//Makes the mine invisible right away just so it looks a bit slicker rather than disappearing briefly AFTER the explosion
			SET_ENTITY_VISIBLE(tempObj, FALSE)
			
			PRINTLN("Sea-Mine is exploding ", iMineIndex)
			SET_BIT(iSeaMine_HasExploded, iMineIndex)
		ELSE
			PRINTLN("can't explode mine ", iMineIndex, " the bit is already set")
		ENDIF	
	ELSE
		NETWORK_REQUEST_CONTROL_OF_ENTITY(tempObj)
		PRINTLN("Requesting control of sea-mine ", iMineIndex)
	ENDIF
ENDPROC

PROC PROCESS_SEA_MINES(OBJECT_INDEX& tempObj, INT iobj, BOOL bNetIDExists)
	IF NOT bNetIDExists OR IS_ENTITY_DEAD(tempObj)
		EXIT
	ENDIF
	
	MODEL_NAMES mnObj = GET_ENTITY_MODEL(tempObj)
	MODEL_NAMES mnNoChain = INT_TO_ENUM(MODEL_NAMES, HASH("xm_prop_x17_Mine_01a"))
	MODEL_NAMES mnShortChain = INT_TO_ENUM(MODEL_NAMES, HASH("xm_prop_x17_Mine_02a"))
	MODEL_NAMES mnLongChain = INT_TO_ENUM(MODEL_NAMES, HASH("xm_prop_x17_Mine_03a"))
	
	IF mnObj = mnNoChain
	OR mnObj = mnShortChain
	OR mnObj = mnLongChain
	
		VECTOR vMineCoords = GET_ENTITY_COORDS(tempObj)
		FLOAT fExplosionOffset = 0 //Not needed so much now that the pivot point of the mine model has changed
		FLOAT fExplosionDistance = (g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].fSeaMineExplodeRadius * g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].fSeaMineExplodeRadius)
		
		BOOL bDrawDebugSphere = FALSE
		
		IF mnObj = mnLongChain
			fExplosionOffset = 0
		ELIF mnObj = mnShortChain
			fExplosionOffset = 0
		ELIF mnObj = mnNoChain
			fExplosionOffset = 1
		ENDIF
		
		VECTOR vOffsetVector = GET_ENTITY_UP_VECTOR(tempObj) * fExplosionOffset
		vMineCoords += vOffsetVector
		
		IF bDrawDebugSphere
			DRAW_DEBUG_SPHERE(vMineCoords, SQRT(fExplosionDistance), 150,0,0,50)
		ENDIF
		
		FLOAT fDistanceToThisMine = VDIST2(GET_ENTITY_COORDS(LocalPlayerPed), vMineCoords)
		
		IF fDistanceToThisMine < fExplosionDistance
			EXPLODE_SEA_MINE(tempObj, vMineCoords, iobj)
		ENDIF
		
		//PRINTLN("distance to mine ", iobj, " is ", fDistanceToThisMine)
	ELSE
		EXIT
	ENDIF
ENDPROC

PROC PROCESS_BMBFB_BOMB_EXPLODING(INT i)
	
	// HOST ONLY //
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		EXIT
	ENDIF
		
	IF DOES_ENTITY_EXIST(GET_BOMB_FOOTBALL_BOMB(i))
		//Backup explosion if needed - usually the bomb will be exploded when receving the event
		IF NETWORK_HAS_CONTROL_OF_ENTITY(GET_BOMB_FOOTBALL_BOMB(i))
			PRINTLN("[BMBFB]", "[BMB", i, "] PROCESS_BMBFB_BOMB_EXPLODING - Host is exploding bomb ", i, " because it wasn't blown up the proper way")
			EXPLODE_BOMB_FOOTBALL_BOMB(i)
		ELSE
			PRINTLN("[BMBFB]", "[BMB", i, "] PROCESS_BMBFB_BOMB_EXPLODING - Host is trying to explode bomb ", i, " because it wasn't blown up the proper way - need to get control first")
			NETWORK_REQUEST_CONTROL_OF_ENTITY(GET_BOMB_FOOTBALL_BOMB(i))
		ENDIF
	ELSE
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_Reset_On_Goal)
			END_OBJECTIVE_TIMER_EARLY()
		ENDIF
	ENDIF
ENDPROC

PROC BMBFB_PROCESS_BALL(INT i)
	IF IS_BIT_SET(MC_serverBD_3.iBombFB_ExplodedBS, i)
		TEXT_LABEL_63 tlDebugText = "iBombFB_ExplodedBS is set for "
		tlDebugText += i
		DRAW_DEBUG_TEXT_2D(tlDebugText, <<0.15, 0.95, 0.5>>)
		
		PROCESS_BMBFB_BOMB_EXPLODING(i)
		
	ELSE
		//The host sends an explode event when time runs out
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		AND NOT g_bMissionEnding
			IF HAS_NET_TIMER_STARTED(MC_serverBD_3.tdObjectiveLimitTimer[0])
				IF GET_REMAINING_TIME_ON_RULE(0, MC_serverBD_4.iCurrentHighestPriority[0]) < 250
					TEXT_LABEL_63 tlDebugText2 = "GET_REMAINING_TIME_ON_RULE is low"
					DRAW_DEBUG_TEXT_2D(tlDebugText2, <<0.15, 0.85, 0.5>>)
				
					BROADCAST_FMMC_BMBFB_EXPLODE_BALL(i, GET_TEAM_THAT_WILL_SCORE_FROM_BALL_EXPLODING(i))
					PRINTLN("[BMBFB]", "[BMB", i, "] BMBFB_PROCESS_BALL - Exploding football ", i, " now due to time running out")
					
					IF i = 0
						PRINTLN("[BMBFB][BMBFB MAJOR] BMBFB_PROCESS_BALL - Exploding all footballs now because time has run out!")
					ENDIF
				ENDIF
			ENDIF
		ENDIF

		IF DOES_ENTITY_EXIST(GET_BOMB_FOOTBALL_BOMB(i))
			IF IS_ENTITY_TOUCHING_ENTITY(LocalPlayerPed, GET_BOMB_FOOTBALL_BOMB(i))
				MC_playerBD_1[iLocalPart].iLastBombFootballHitTimestamp[i] = NATIVE_TO_INT(GET_NETWORK_TIME_ACCURATE())
				PRINTLN("[BMBFB TOUCH SPAM] BMBFB_PROCESS_BALL - I've touched ball ", i, "! Timestamp: ", MC_playerBD_1[iLocalPart].iLastBombFootballHitTimestamp[i])
			ENDIF
			
			IF NOT IS_ENTITY_VISIBLE(GET_BOMB_FOOTBALL_BOMB(i))
				SET_ENTITY_VISIBLE(GET_BOMB_FOOTBALL_BOMB(i), TRUE)
				PRINTLN("[BMBFB] BMBFB_PROCESS_BALL - Making ball ", i, " visible!")
			ENDIF
			
			IF IS_INTERIOR_READY(g_ArenaInterior)
			AND GET_INTERIOR_FROM_ENTITY(GET_BOMB_FOOTBALL_BOMB(i)) != g_ArenaInterior
			AND NETWORK_HAS_CONTROL_OF_ENTITY(GET_BOMB_FOOTBALL_BOMB(i))
				RETAIN_ENTITY_IN_INTERIOR(GET_BOMB_FOOTBALL_BOMB(i), g_ArenaInterior)
				PRINTLN("[BMBFB] Moving bomb ", i, " into the arena!")
			ENDIF
			
			BLIP_INDEX biBombBlip = biObjBlip[i]
			IF DOES_BLIP_EXIST(biBombBlip)
			
				IF NOT IS_PAUSE_MENU_ACTIVE()
					SET_BLIP_DISPLAY(biBombBlip, DISPLAY_RADAR_ONLY)
				ENDIF
				
				SHOW_HEIGHT_ON_BLIP(biBombBlip, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectBitSet, cibsOBJ_ShowHeightBlipIndicator))
			ENDIF
			
		ELSE
			IF bIsLocalPlayerHost
				IF IS_BIT_SET(MC_serverBD_3.iBombFB_ExplodedBS, i)
					CLEAR_BIT(MC_serverBD_3.iBombFB_ExplodedBS, i)
					PRINTLN("[BMBFB]", "[BMB", i, "] BMBFB_PROCESS_BALL - Football ", i, " clearing its iBombFB_ExplodedBS because it doesn't exist!")
				ENDIF
				
				IF IS_BIT_SET(MC_serverBD_3.iBombFB_GoalProcessedBS, i)
					CLEAR_BIT(MC_serverBD_3.iBombFB_GoalProcessedBS, i)
					PRINTLN("[BMBFB]", "[BMB", i, "] BMBFB_PROCESS_BALL - Football ", i, " clearing its iBombFB_GoalProcessedBS because it doesn't exist!")
				ENDIF
			ENDIF
			
			IF DOES_BLIP_EXIST(biObjBlip[i])
				REMOVE_BLIP(biObjBlip[i])
				PRINTLN("[BMBFB]", "[BMB", i, "] BMBFB_PROCESS_BALL - Football ", i, " removing blip!")
			ENDIF
		ENDIF
		
		OBJECT_INDEX oiCarFootballBomb
		oiCarFootballBomb = GET_BOMB_FOOTBALL_BOMB(i)
		
		IF NOT DOES_ENTITY_EXIST(oiCarFootballBomb)
			EXIT
		ENDIF
		
		VECTOR vBallPos = GET_ENTITY_COORDS(oiCarFootballBomb)
		BOOL bProcessGoal = TRUE

		INT iGoalToCheck
		IF vBallPos.x < 2800.0
			IF vBallPos.x > 2700.0
				bProcessGoal = FALSE
			ENDIF
			iGoalToCheck = iGoalB
		ELSE
			IF vBallPos.x < 2900.0
				bProcessGoal = FALSE
			ENDIF
			iGoalToCheck = iGoalA
		ENDIF
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[iGoalToCheck].iType != ciFMMC_ZONE_TYPE__GOAL_AREA
			EXIT
		ENDIF
		
		IF iGoalToCheck > -1
			iTeamPotentialExplosionsThisMiniRound[g_FMMC_STRUCT_ENTITIES.sPlacedZones[iGoalToCheck].iGoalTeam]++
			PROCESS_BMBFB_BALL_COLOUR(GET_BOMB_FOOTBALL_BOMB(i))
		ENDIF
		
		IF bProcessGoal
		AND NETWORK_HAS_CONTROL_OF_ENTITY(oiCarFootballBomb)
		
			IF NOT IS_BIT_SET(MC_serverBD_3.iBombFB_ExplodedBS, i)
			AND NOT IS_BIT_SET(MC_serverBD_3.iBombFB_GoalProcessedBS, i)
			
				IF IS_LOCATION_IN_FMMC_ZONE(vBallPos, iGoalToCheck, TRUE, DEFAULT, TRUE, fZoneGrowth[iGoalToCheck])
				OR IS_BIT_SET(iZoneBS_ActivatedRemotely, iGoalToCheck)
					IF NETWORK_HAS_CONTROL_OF_ENTITY(oiCarFootballBomb)
						BROADCAST_FMMC_BMBFB_GOAL(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iGoalToCheck].iGoalTeam, iGoalToCheck, i)
					ENDIF
					
					#IF IS_DEBUG_BUILD
						IF IS_BIT_SET(MC_serverBD_3.iBombFB_ExplodedBS, i)
							PRINTLN("[BMBFB_SPAM] ZONES - Football ", i, " already exploded/exploding")
						ENDIF
						
						IF IS_BIT_SET(MC_serverBD_3.iBombFB_GoalProcessedBS, i)
							PRINTLN("[BMBFB_SPAM] ZONES - Football ", i, " already processed being in-goal")
						ENDIF
						DRAW_DEBUG_TEXT_ABOVE_ENTITY(oiCarFootballBomb, "In goal!", -0.15)
					#ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

FUNC BOOL IN_RANGE_OF_OBJECT_FOR_HEALTH_BAR(INT iObj, OBJECT_INDEX oiObj)
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectHealthBarInRange > 0
		IF DOES_ENTITY_EXIST(oiObj)
		AND NOT IS_ENTITY_DEAD(oiObj)
			VECTOR vObjLoc = GET_ENTITY_COORDS(oiObj)
			IF VDIST2(vObjLoc, GET_PLAYER_COORDS(PlayerToUse)) < POW(TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectHealthBarInRange), 2)
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC ENTITY_INDEX GET_EXPLOSION_OWNER_FOR_ANY_BOMB_TYPE(VECTOR tempObjCoords, INT iObj)

	ENTITY_INDEX eiExplosionOwner = NULL
	
	IF NOT DOES_ENTITY_EXIST(eiExplosionOwner)
		eiExplosionOwner = GET_OWNER_OF_EXPLOSION_IN_SPHERE(EXP_TAG_BOMB_STANDARD, tempObjCoords, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].fDestroyExplosionRadius)
		IF DOES_ENTITY_EXIST(eiExplosionOwner)
			PRINTLN("[fDestroyExplosionRadius] Hit by standard")
		ENDIF
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(eiExplosionOwner)
		eiExplosionOwner = GET_OWNER_OF_EXPLOSION_IN_SPHERE(EXP_TAG_BOMB_CLUSTER, tempObjCoords, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].fDestroyExplosionRadius)
		IF DOES_ENTITY_EXIST(eiExplosionOwner)
			PRINTLN("[fDestroyExplosionRadius] Hit by EXP_TAG_BOMB_CLUSTER")
		ENDIF
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(eiExplosionOwner)
		eiExplosionOwner = GET_OWNER_OF_EXPLOSION_IN_SPHERE(EXP_TAG_BOMB_CLUSTER_SECONDARY, tempObjCoords, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].fDestroyExplosionRadius)
		IF DOES_ENTITY_EXIST(eiExplosionOwner)
			PRINTLN("[fDestroyExplosionRadius] Hit by EXP_TAG_BOMB_CLUSTER_SECONDARY")
		ENDIF
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(eiExplosionOwner)
		PRINTLN("[fDestroyExplosionRadius] Trying incendiary")
		eiExplosionOwner = GET_OWNER_OF_EXPLOSION_IN_SPHERE(EXP_TAG_BOMB_INCENDIARY, tempObjCoords, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].fDestroyExplosionRadius)
		IF DOES_ENTITY_EXIST(eiExplosionOwner)
			PRINTLN("[fDestroyExplosionRadius] Hit by EXP_TAG_BOMB_INCENDIARY")
		ENDIF
	ENDIF
	
	RETURN eiExplosionOwner
ENDFUNC

PROC PROCESS_OBJECT_RULE_VISIBILITY(INT iObj)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetThree, cibsOBJ3_VISIBILITY_BY_RULE)
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF bHideObjects
		EXIT
	ENDIF
	#ENDIF
	
	BOOL bDisplay = FALSE
	INT iTeamLoop
	
	REPEAT MC_ServerBD.iNumberOfTeams iTeamLoop
		CLOGLN(DEBUG_MISSION, CHANNEL_SEVERITY_DEBUG1, "[PROCESS_OBJECT_RULE_VISIBILITY]  New priority for team ",iTeamLoop, " Checking iObj: ",iObj) 
		IF MC_serverBD_4.iObjPriority[iObj][iTeamLoop] = MC_ServerBD_4.iCurrentHighestPriority[iTeamLoop]
			bDisplay = TRUE
			CLOGLN(DEBUG_MISSION, CHANNEL_SEVERITY_DEBUG1, "[PROCESS_OBJECT_RULE_VISIBILITY] Current highest priority same as object priority ",MC_serverBD_4.iObjPriority[iObj][iTeamLoop]) 
			BREAKLOOP
		ENDIF
	ENDREPEAT

	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niObject[iobj])
		CLOGLN(DEBUG_MISSION, CHANNEL_SEVERITY_DEBUG1, "[PROCESS_OBJECT_RULE_VISIBILITY]  Obj: ",iObj," Doesn't exist") 
		EXIT
	ENDIF
	OBJECT_INDEX tempObj = NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[iObj])
	IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(tempObj)
		CLOGLN(DEBUG_MISSION, CHANNEL_SEVERITY_DEBUG1, "[PROCESS_OBJECT_RULE_VISIBILITY]  Obj: ",iObj," Doesn't have control of entity") 
		EXIT
	ENDIF
	IF IS_ENTITY_VISIBLE_TO_SCRIPT(tempObj) = bDisplay
		CLOGLN(DEBUG_MISSION, CHANNEL_SEVERITY_DEBUG1, "[PROCESS_OBJECT_RULE_VISIBILITY]  Obj: ",iObj," Same visibility ",bDisplay) 
		EXIT
	ENDIF
	CLOGLN(DEBUG_MISSION, CHANNEL_SEVERITY_DEBUG1, "[PROCESS_OBJECT_RULE_VISIBILITY] SET_ENTITY_VISIBLE ",bDisplay) 
	SET_ENTITY_VISIBLE(tempObj,bDisplay)
	
ENDPROC

PROC PROCESS_OBJECT_HEALTHBARS (INT iObj, OBJECT_INDEX tempObj)
	// If the vehicle has been set to show a health bar for this rule, then do it. Need to account for three different systems of vehicle health, and trigger values for undrivebale being -1000 on the engine and petrol tank. 
	IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[ MC_playerBD[iPartToUse].iteam ].iRuleBitset[MC_ServerBD_4.iCurrentHighestPriority[ MC_playerBD[iPartToUse].iteam ]], ciBS_RULE_ENABLE_HEALTH_BAR) 
		PRINTLN("[PROCESS_OBJECT_HEALTHBARS] - Object Health Bars and rule: ", IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetTwo, cibsOBJ2_EnableHealthBar ), g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectHealthBarOnRule)
		IF IS_BIT_SET( g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetTwo, cibsOBJ2_EnableHealthBar )
		AND (g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectHealthBarOnRule = -1 OR (g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectHealthBarOnRule > -1 AND MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectHealthBarOnRule))
			PRINTLN("[PROCESS_OBJECT_HEALTHBARS] - Ped Checks: ", g_bMissionEnding, IS_PED_INJURED(localPlayerPed), IS_BIT_SET(MC_serverBD.iServerBitset3, SBBOOL3_TEAM_0_SHOULD_RESPAWN_BACK_AT_START_NOW + MC_playerBD[iPartToUse].iteam))
			IF NOT g_bMissionEnding
			AND NOT IS_PED_INJURED(localPlayerPed)
			AND NOT IS_BIT_SET(MC_serverBD.iServerBitset3, SBBOOL3_TEAM_0_SHOULD_RESPAWN_BACK_AT_START_NOW + MC_playerBD[iPartToUse].iteam)
				PRINTLN("[PROCESS_OBJECT_HEALTHBARS] - In range: ", IN_RANGE_OF_OBJECT_FOR_HEALTH_BAR(iObj, tempObj))
				IF IN_RANGE_OF_OBJECT_FOR_HEALTH_BAR(iObj, tempObj)
					TEXT_LABEL_63 tl63_Temp
					BOOL isLiteralString=FALSE
					BOOL isPlayerString=FALSE
					TEXT_LABEL_23 tl23_Temp
					INT iTeamForName = MC_playerBD[iPartToUse].iteam
					IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
						IF MC_serverBD_4.iObjPriority[iObj][MC_playerBD[iPartToUse].iteam] != MC_ServerBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]
							//Not our team's priority, check if it is someone else's
							INT iTeamLoop
							FOR iTeamLoop = 0 TO MC_ServerBD.iNumberOfTeams - 1
								IF MC_serverBD_4.iObjPriority[iObj][iTeamLoop] = MC_ServerBD_4.iCurrentHighestPriority[iTeamLoop]
									iTeamForName = iTeamLoop
									BREAKLOOP
								ENDIF
							ENDFOR
						ENDIF
					ENDIF
					
					IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sFMMCEndConditions[iTeamForName].tl63ObjectiveEntityHUDMessage[MC_ServerBD_4.iCurrentHighestPriority[iTeamForName]])
						tl23_Temp = g_FMMC_STRUCT.sFMMCEndConditions[iTeamForName].tl63ObjectiveEntityHUDMessage[MC_ServerBD_4.iCurrentHighestPriority[iTeamForName]]
					ELIF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.tl23CustomTargetString[iTeamForName])
						tl23_Temp = g_FMMC_STRUCT.tl23CustomTargetString[iTeamForName]
					ELIF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sFMMCEndConditions[ iTeamForName ].tl23ObjBlip[MC_ServerBD_4.iCurrentHighestPriority[ iTeamForName ]])
						tl23_Temp = g_FMMC_STRUCT.sFMMCEndConditions[ iTeamForName ].tl23ObjBlip[MC_ServerBD_4.iCurrentHighestPriority[ iTeamForName ]]
					ENDIF
					
					IF NOT IS_STRING_NULL_OR_EMPTY(tl23_Temp)
						isLiteralString=TRUE
						
						tl23_Temp = CONVERT_STRING_TO_UPPERCASE(tl23_Temp)
						tl63_Temp += tl23_Temp
					ELSE
						 tl63_Temp = "OBJ_HEALTH"
					ENDIF
											
					INT iCurrentHealth = GET_ENTITY_HEALTH(tempObj)
					INT iMaxHealth     = GET_ENTITY_MAX_HEALTH(tempObj)
					
					IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjHealthOverride > -1
						iMaxHealth = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjHealthOverride
					ENDIF
					
					FLOAT fPercentage  = 0.0
					IF iCurrentHealth > 0 AND iMaxHealth > 0
						fPercentage = (TO_FLOAT(iCurrentHealth)/TO_FLOAT(iMaxHealth)) * 100
					ENDIF
					
					HUDORDER hudOrderObj = HUDORDER_DONTCARE
					//Standard vehicle healthbar
					DRAW_GENERIC_METER(FLOOR(fPercentage), 100, tl63_Temp, HUD_COLOUR_WHITE, -1, hudOrderObj, DEFAULT, DEFAULT, isPlayerString, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, isLiteralString)
					
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_FLAG_RETURN(INT iObj)
	
	IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_ARENA_CTF(g_FMMC_STRUCT.iAdversaryModeType)
		EXIT
	ENDIF
	
	OBJECT_INDEX tempObj = NET_TO_OBJ(MC_ServerBD_1.sFMMC_SBD.niObject[iObj])
	
	IF NOT DOES_ENTITY_EXIST(tempObj)
		EXIT
	ENDIF
	
	IF GET_TIME_REMAINING_ON_MULTIRULE_TIMER(MC_PlayerBD[iLocalPart].iteam) < 1000
	AND NOT ARE_MULTIPLE_TEAMS_ON_SAME_SCORE()
		EXIT
	ENDIF
	
	IF MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iteam] > FMMC_MAX_RULES
		EXIT
	ENDIF
	
	FLOAT fReturnSize = TO_FLOAT(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iGotoRange)
	VECTOR vPosToCheck = GET_ENTITY_COORDS(tempObj)
	
	BOOL bCloseEnough = VDIST2(vPosToCheck, GET_ENTITY_COORDS(LocalPlayerPed)) < fReturnSize * fReturnSize
	
	IF MC_serverBD_4.iObjPriority[iObj][MC_PlayerBD[iLocalPart].iteam] > FMMC_MAX_RULES
	AND NOT NETWORK_HAS_CONTROL_OF_ENTITY(tempObj)
	AND bCloseEnough
	AND NOT IS_ENTITY_ATTACHED_TO_ANY_PED(tempObj)
	AND NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
		NETWORK_REQUEST_CONTROL_OF_ENTITY(tempObj)
		PRINTLN("PROCESS_FLAG_RETURN - Requesting control of object: ", iObj, " to return it.")
	ENDIF
	
	IF MC_serverBD_4.iObjPriority[iObj][MC_PlayerBD[iLocalPart].iteam] > FMMC_MAX_RULES // If it's not the flag we're meant to be stealing - therefore must be OUR flag that somebody stole
	AND NOT ARE_VECTORS_ALMOST_EQUAL(GET_ENTITY_COORDS(tempObj), g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].vPos, 3) // If this flag isn't at its resting place right now
	AND NETWORK_HAS_CONTROL_OF_ENTITY(tempObj) // AND we have control of the entity
	AND NOT HAS_TEAM_FINISHED(MC_PlayerBD[iLocalPart].iteam)
	AND MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam] < FMMC_MAX_RULES
	AND NOT IS_PED_INJURED(LocalPlayerPed)
		IF NOT IS_ENTITY_ATTACHED_TO_ANY_PED(tempObj) // If nobody is carrying it...
			IF bCloseEnough
				SET_BIT(iLocalBoolCheck30, LBOOL30_RETURNING_A_FLAG)
				PRINTLN("PROCESS_FLAG_RETURN - Preparing to return. Setting LBOOL30_RETURNING_A_FLAG")
				SET_TEAM_PICKUP_OBJECT(tempObj,MC_PlayerBD[iLocalPart].iteam,TRUE)
				ATTACH_PORTABLE_PICKUP_TO_PED(tempObj, LocalPlayerPed)
			ENDIF
		ELSE
			IF IS_ENTITY_ATTACHED_TO_ENTITY(tempObj, PLAYER_PED_ID())
				DETACH_PORTABLE_PICKUP_FROM_PED(tempObj)
				BROADCAST_SET_ARENA_ANNOUNCER_SOUND_BS(ci_Arena_Announcer_DataTypeForEvent_ModeSpecific, g_iAA_PlaySound_CTF_Returned)
				BROADCAST_FMMC_RETURNED_FLAG_SHARD(MC_PlayerBD[iLocalPart].iteam, LocalPlayer)
				SET_PACKAGE_NOT_COLLECTABLE_FOR_TEAM(tempObj,iObj,0)
				SET_PACKAGE_NOT_COLLECTABLE_FOR_TEAM(tempObj,iObj,1)
				VECTOR vDestination = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].vPos
				vDestination.z += 1
				SET_ENTITY_COORDS(tempObj, vDestination)
				SET_ENTITY_ROTATION(tempObj, <<0.0,0.0,g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].fHead>>)
				MC_playerBD_1[iLocalPart].iObjectsReturned++
				PRINTLN("PROCESS_FLAG_RETURN - Flag ", iObj," returned. Number of flags returned: ", MC_playerBD_1[iLocalPart].iObjectsReturned)
				CLEAR_BIT(iLocalBoolCheck30, LBOOL30_RETURNING_A_FLAG)
				PRINTLN("PROCESS_FLAG_RETURN - Clearing LBOOL30_RETURNING_A_FLAG")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC DISPLAY_OBJECT_HUD(INT iobj)
	
	IF MC_serverBD_4.iCurrentHighestPriority[MC_PlayerBD[iPartToUse].iTeam] >= FMMC_MAX_RULES
		EXIT
	ENDIF
	
	VECTOR 	vObjCoords
	OBJECT_INDEX tempObj
	VEHICLE_INDEX PlayerVeh
	BOOL bObjectDead = FALSE
	
	BOOL bHost = bIsLocalPlayerHost
	BOOL bNetIDExists = NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niObject[iobj])
	
	IF bHost
		IF bNetIDExists
			IF IS_BIT_SET(MC_serverBD.iObjCleanup_NeedOwnershipBS, iobj)
				IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niObject[iobj])	
					IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iRespawnOnRuleChangeBS > 0
						IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[iobj]))
							PRINTLN("[RCC MISSION] DISPLAY_OBJECT_HUD - Requesting control of obj ",iobj," for cleanup - for delete")
							NETWORK_REQUEST_CONTROL_OF_ENTITY(NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[iobj]))
						ELSE
							IF (IS_ANY_TEAM_BEING_WARPED_TO_START_POINT()
							AND IS_BIT_SET(MC_serverBD.iServerBitSet5, SBBOOL5_ALL_PLAYERS_RENDERPHASES_PAUSED))
							OR NOT IS_ANY_TEAM_BEING_WARPED_TO_START_POINT()
								PRINTLN("[RCC MISSION] DISPLAY_OBJECT_HUD - Deleting object ",iobj)
								DELETE_FMMC_OBJECT(iobj)
								bObjectDead = TRUE
							ENDIF
						ENDIF
						SERVER_CLEAR_OBJ_AT_HOLDING(iObj)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT bNetIDExists
		IF IS_BIT_SET(iLocalBoolCheck20, LBOOL20_PARTICLE_FX_OVERRIDE_COCAINE)
			IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].mn = INT_TO_ENUM(MODEL_NAMES, HASH("imp_Prop_ImpExp_BoxCoke_01"))
				IF IS_BIT_SET(iCocaineParticleEffectBitset, iObj)
					USE_PARTICLE_FX_ASSET("scr_ie_svm_technical2")
					START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_dst_cocaine", g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].vPos, <<0.0, 0.0, 0.0>>, 1.5)
					CLEAR_BIT(iCocaineParticleEffectBitset, iObj)
				ENDIF
			ENDIF
		ENDIF
		
		IF bHost
			IF IS_BIT_SET(MC_serverBD.iObjCleanup_NeedOwnershipBS, iobj)
				//CASSERTLN(DEBUG_CONTROLLER,"[RCC MISSION] DISPLAY_OBJECT_HUD - Obj ",iobj," we were trying to request ownership of for cleanup no longer exists!")
				PRINTLN("[RCC MISSION] DISPLAY_OBJECT_HUD - Obj ",iobj," we were trying to request ownership of for cleanup no longer exists!")
				CLEAR_BIT(MC_serverBD.iObjCleanup_NeedOwnershipBS, iobj)
			ENDIF
			
			IF MC_serverBD_2.iCurrentObjRespawnLives[iobj] > 0
				IF SHOULD_OBJ_RESPAWN_NOW(iobj)
				AND HAS_OBJ_RESPAWN_DELAY_EXPIRED(iobj)
					IF RESPAWN_MISSION_OBJ(iobj)
						
						SET_BIT(MC_serverBD_2.iObjSpawnedOnce, iobj)
						
						RESET_NET_TIMER(MC_serverBD_3.tdObjSpawnDelay[iobj])
						
						IF DOES_OBJ_HAVE_PLAYER_VARIABLE_SETTING(iobj)
							IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iAssociatedObjective > -1
							AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iAssociatedTeam > -1
								IF MC_serverBD_4.iCurrentHighestPriority[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iAssociatedTeam] < FMMC_MAX_RULES
									MC_serverBD_1.sCreatedCount.iNumObjCreated[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iAssociatedObjective]++
								ENDIF
							ENDIF
						ENDIF
						
						IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iRespawnOnRuleChangeBS > 0
							INT iTeam
							FOR iTeam = 0 TO FMMC_MAX_TEAMS - 1
								IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iRule[iTeam] != FMMC_OBJECTIVE_LOGIC_NONE
									SET_BIT(MC_serverBD.iObjteamFailBitset[iobj],iteam)
									BREAKLOOP
								ENDIF
							ENDFOR
						ENDIF
						
						IF MC_serverBD_2.iCurrentObjRespawnLives[iobj] != UNLIMITED_RESPAWNS
							MC_serverBD_2.iCurrentObjRespawnLives[iobj]--
						ENDIF
						
						INT iTeam
						FOR iTeam = 0 TO FMMC_MAX_TEAMS-1
							IF MC_ServerBD_4.iCurrentHighestPriority[iTeam] < FMMC_MAX_RULES
								IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iObjectsDestroyedRuleHudAmount[MC_ServerBD_4.iCurrentHighestPriority[iTeam]] > 0
									IF IS_BIT_SET(MC_ServerBD_4.iObjectsDestroyedForSpecialHUDBitset[iTeam], iObj)
										PRINTLN("[RCC MISSION][PROCESS_OBJ_BODY][iEntityincrement] A new object has been spawned that has been destroyed previously. obj: ", iobj, " Clearing iObjectsDestroyedForSpecialHUDBitset so that we can increment destroyed later")
										CLEAR_BIT(MC_ServerBD_4.iObjectsDestroyedForSpecialHUDBitset[iTeam], iObj)
									ENDIF
								ENDIF
							ENDIF
						ENDFOR
					ENDIF
				ELSE
					IF MC_serverBD.isearchingforObj = iobj
						MC_serverBD.isearchingforObj = -1
					ENDIF
					
					IF DOES_ANY_TEAM_HAVE_NEW_PRIORITY_THIS_FRAME()
					AND (NOT CAN_OBJ_STILL_SPAWN_IN_MISSION(iobj))
						
						MC_serverBD.iObjteamFailBitset[iobj] = 0
						
						INT iTeam
						
						FOR iTeam = 0 TO (FMMC_MAX_TEAMS - 1)
							MC_serverBD_4.iObjPriority[iobj][iTeam] = FMMC_PRIORITY_IGNORE
							MC_serverBD_4.iObjRule[iobj][iTeam] = FMMC_OBJECTIVE_LOGIC_NONE
							CLEAR_BIT(MC_serverBD.iMissionCriticalObj[iTeam], iobj)
						ENDFOR
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iOrbitalCannonIndex > -1
			//PRINTLN("[OrbitalCannon] g_iOrbitalCannonObjectInUse: ", g_iOrbitalCannonObjectInUse)
			
			IF g_iOrbitalCannonObjectInUse = iobj
				g_bMissionPlacedOrbitalCannon = FALSE
				//PRINTLN("[OrbitalCannon] Clearing up Orbital Cannon because the object the player used to access it is now gone")
				//PRINTLN("[OrbitalCannon] g_bMissionPlacedOrbitalCannon: ", g_bMissionPlacedOrbitalCannon)
			ELSE
				//PRINTLN("[OrbitalCannon] Orbitalcannon in use is not obj ", iobj)
			ENDIF
		ENDIF
		
		IF IS_THIS_MODEL_A_CCTV_CAMERA(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].mn) 
			CLEANUP_CCTV_CAMERA_SOUND_EFFECT_FOR_DEATH(iObj)
		ENDIF
		
		bObjectDead = TRUE
	ELSE
		tempObj = NET_TO_OBJ(MC_serverBD_1.sFMMC_SBD.niObject[iObj])
		
		IF IS_ENTITY_DEAD(tempObj)
			PRINTLN("[RCC MISSION] DISPLAY_OBJECT_HUD - IS_ENTITY_DEAD ", iObj)
			bObjectDead = TRUE
			CLEANUP_CCTV_CAMERA_SOUND_EFFECT_FOR_DEATH(iObj)
		ENDIF
		
		#IF IS_DEBUG_BUILD
		
		IF bObjDebug
			TEXT_LABEL_63 tlEntityDebug = "Obj "
			tlEntityDebug += iObj
			DRAW_DEBUG_TEXT_ABOVE_ENTITY(tempObj, tlEntityDebug, 0.6, 255, 255, 255)
		ENDIF
		
		IF bHideObjects
			IF DOES_ENTITY_EXIST(tempObj)
				SET_ENTITY_VISIBLE(tempObj, FALSE)
			ENDIF
		ENDIF
		#ENDIF
		
		IF bObjectDead
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iLegacyMiniGameBitSet, cibsLEGACY_FMMC_MINI_GAME_TYPE_CASH_GRAB) 
			AND DOES_CAM_EXIST(sCashGrabData.cameraIndex)
				PROCESS_CASH_GRAB_FAIL_CLEANUP(iObj)
			ENDIF
		ENDIF
		
		IF bHost
			IF bObjectDead
			OR IS_OBJECT_BROKEN_AND_VISIBLE(tempObj)
				IF IS_BIT_SET(g_FMMC_STRUCT.iContinuityBitset, ciMISSION_CONTINUITY_BS_ObjectDestroyedTracking)
				AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iContinuityId > -1
				AND NOT IS_LONG_BIT_SET(MC_serverBD_1.sLEGACYMissionContinuityVars.iObjectDestroyedBitset, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iContinuityId)
					SET_LONG_BIT(MC_serverBD_1.sLEGACYMissionContinuityVars.iObjectDestroyedBitset, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iContinuityId)
					PRINTLN("[JS][CONTINUITY] - DISPLAY_OBJECT_HUD - obj ", iobj, " with continuityID ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iContinuityId, " was destroyed")
				ENDIF
			ENDIF
			
			//Interact-With quicker cleanup
			IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iLegacyMiniGameBitSet, cibsLEGACY_FMMC_MINI_GAME_TYPE_INTERACT_WITH)
				IF (MC_serverBD_4.iObjPriority[iobj][MC_playerBD[iPartToUse].iteam] = (MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam] - 1)
				OR ((NOT PLAYER_CAN_INTERACT_WITH_OBJ(iObj, tempObj)) AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetFour, cibsOBJ4_HideUninteractableBlip)))
				AND NOT IS_PAUSE_MENU_ACTIVE()
					IF DOES_BLIP_EXIST(biObjBlip[iobj])
						PRINTLN("[InteractWithBlip] Removing blip immediately for Interact With object: ", iobj)
						REMOVE_OBJECT_BLIP(iobj)
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_STOCKPILE(g_FMMC_STRUCT.iAdversaryModeType)
				IF ((MC_serverBD.iObjCarrier[iObj] != -1
				AND NOT NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(MC_serverBD.iObjCarrier[iObj])))
				OR (MC_serverBD_1.iPTLDroppedPackageOwner[iObj] != -1 
				AND NOT NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(MC_serverBD_1.iPTLDroppedPackageOwner[iObj]))))
				AND NOT g_bMissionEnding
					IF NOT IS_PICKUP_AT_ANY_HOLDING(iObj)
						IF NETWORK_HAS_CONTROL_OF_ENTITY(tempObj)
							SET_ENTITY_COORDS(tempObj,g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].vPos)
							SET_ENTITY_VISIBLE(tempObj, TRUE)
							SET_ENTITY_COLLISION(tempObj, TRUE)
							MC_serverBD.iObjCarrier[iObj] = -1
							MC_serverBD_1.iPTLDroppedPackageOwner[iObj] = -1
							PRINTLN("[RCC MISSION] Moving pickup owned by inactive player back to position!")
						ELSE
							NETWORK_REQUEST_CONTROL_OF_ENTITY(tempObj)
						ENDIF
					ENDIF
				ENDIF
			ENDIF

		ENDIF
		
		IF IS_BIT_SET(iLocalBoolCheck20, LBOOL20_PARTICLE_FX_OVERRIDE_COCAINE)
			IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].mn = INT_TO_ENUM(MODEL_NAMES, HASH("imp_Prop_ImpExp_BoxCoke_01"))
				IF NETWORK_HAS_CONTROL_OF_ENTITY(tempObj)
					IF HAS_OBJECT_BEEN_BROKEN(tempObj)
						IF GET_ENTITY_HEALTH(tempObj) > 0
							SET_ENTITY_HEALTH(tempObj, 0)
							PRINTLN("[DISPLAY_OBJECT_HUD] OBJECT = ", iObj, " ... model = imp_Prop_ImpExp_BoxCoke_01 ... HAS_OBJECT_BEEN_BROKEN = TRUE - Setting health to 0!")
						ENDIF
					ENDIF
				ELSE
					SET_BIT(iCocaineParticleEffectBitset, iObj)
				ENDIF
			ENDIF
		ENDIF
		
		IF GET_ENTITY_MODEL(tempObj) = INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_hdsec"))
			IF (GET_ENTITY_HEIGHT_ABOVE_GROUND(tempObj) < 0.2)
			AND (GET_ENTITY_HEIGHT_ABOVE_GROUND(tempObj) > 0)
				IF NOT IS_BIT_SET(iLocalObjectLightReadyForUpdate, ciOBJECT_LIGHT_READY_0 + iObj)
					SET_BIT(iLocalObjectLightReadyForUpdate, ciOBJECT_LIGHT_READY_0 + iObj)
					PRINTLN("[RCC MISSION] Setting ready bit for object ", iObj)
				ENDIF
			ENDIF
			
			IF NOT IS_BIT_SET(iLocalObjectLightUpdated, ciOBJECT_LIGHT_UPDATED_0 + iObj)
				IF IS_BIT_SET(iLocalObjectLightReadyForUpdate, ciOBJECT_LIGHT_READY_0 + iObj)
					PRINTLN("[RCC MISSION] Updating lights for Sinking bomb ", iObj)
					UPDATE_LIGHTS_ON_ENTITY(tempObj)
					SET_BIT(iLocalObjectLightUpdated, ciOBJECT_LIGHT_UPDATED_0 + iObj)
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_STOCKPILE(g_FMMC_STRUCT.iAdversaryModeType)
			KEEP_OBJECT_IN_DROP_OFF(tempObj, iObj)
		ENDIF
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeventeen, ciENABLE_TAGGING_TARGETS)
			IF MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam] < FMMC_MAX_RULES
				IF IS_IT_SAFE_TO_ADD_TAG_BLIP(MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iLocalPart].iteam])
					INT iTagged
					FOR iTagged = 0 TO FMMC_MAX_TAGGED_ENTITIES-1
						IF MC_serverBD_3.iTaggedEntityType[iTagged] = 4
							IF MC_serverBD_3.iTaggedEntityIndex[iTagged] = iObj
								IF IS_ENTITY_ALIVE(tempObj)
									VECTOR vEntityCoords = GET_ENTITY_COORDS(tempObj)
									vEntityCoords.z = (vEntityCoords.z + 1.0)
									DRAW_TAGGED_MARKER(vEntityCoords, tempObj, 4, iObj)
									
									IF NOT DOES_BLIP_EXIST(biTaggedEntity[iTagged])
									AND NOT DOES_BLIP_EXIST(GET_BLIP_FROM_ENTITY(tempObj))
										ADD_TAGGED_BLIP(tempObj, 4, iObj, iTagged)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDFOR
				ENDIF
			ENDIF
		ENDIF
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjLodDist != -1
			IF DOES_ENTITY_EXIST(tempObj)
				IF GET_ENTITY_LOD_DIST(tempObj) != g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjLodDist
					SET_ENTITY_LOD_DIST(tempObj, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjLodDist)
					PRINTLN("[RCC MISSION] DISPLAY_OBJECT_HUD - - - SET_ENTITY_LOD_DIST(tempObj, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjLodDist) = ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjLodDist)
				ENDIF
			ENDIF
		ENDIF
		
		IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].fDestroyExplosionRadius > 0.0
			IF NOT IS_BIT_SET(iExplosionDestroyedObjectBitSet, iobj)
			
				PRINTLN("[fDestroyExplosionRadius] fDestroyExplosionRadius is ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].fDestroyExplosionRadius, " for obj ", iObj)
				
				VECTOR tempObjCoords = GET_ENTITY_COORDS(tempObj)
				
				IF NOT IS_ENTITY_DEAD(tempObj)
				AND NOT HAS_OBJECT_BEEN_BROKEN(tempObj)
				
					EXPLOSION_TAG etExplType = EXP_TAG_DONTCARE
					
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetTwo,  cibsOBJ2_ExplodeOnlyVolatolWide)
						PRINTLN("[fDestroyExplosionRadius] Looking for EXP_TAG_BOMB_STANDARD_WIDE for obj ", iObj)
						etExplType = EXP_TAG_BOMB_STANDARD_WIDE
					ENDIF
				
					ENTITY_INDEX eiExplosionOwner = GET_OWNER_OF_EXPLOSION_IN_SPHERE(etExplType, tempObjCoords, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].fDestroyExplosionRadius)
					
					IF DOES_ENTITY_EXIST(eiExplosionOwner)
						IF IS_ENTITY_A_PED(eiExplosionOwner) //ensuring the turrets don't blow themselves up using this
							PRINTLN("[RCC MISSION] [fDestroyExplosionRadius] DISPLAY_OBJECT_HUD - Setting entity health to zero. Caught by a ped's explosion in set range: ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].fDestroyExplosionRadius)
							
							IF NETWORK_HAS_CONTROL_OF_ENTITY(tempObj)
								SET_ENTITY_HEALTH(tempObj, 1)
								PRINTLN("[RCC MISSION] [fDestroyExplosionRadius] DISPLAY_OBJECT_HUD - I'm currently in control of of turret ", iObj, "! Setting its health to 1 now")
							ELSE
								PRINTLN("[RCC MISSION] [fDestroyExplosionRadius] DISPLAY_OBJECT_HUD - I'm not in control of turret obj ", iObj)
							ENDIF
							
							IF GET_PED_INDEX_FROM_ENTITY_INDEX(eiExplosionOwner) = LocalPlayerPed
								ADD_OWNED_EXPLOSION(eiExplosionOwner, tempObjCoords, EXP_TAG_BARREL, 5)
								PRINTLN("[RCC MISSION] [fDestroyExplosionRadius] DISPLAY_OBJECT_HUD - I'm the owner of the explosion near turret ", iObj, "! Creating explosion on the turret now")
							ELSE
								PRINTLN("[RCC MISSION] [fDestroyExplosionRadius] DISPLAY_OBJECT_HUD - I'm not the owner of the explosion on obj ", iObj)
							ENDIF
							
							SET_BIT(iExplosionDestroyedObjectBitSet, iobj)
						ELSE
							PRINTLN("[fDestroyExplosionRadius] Owner of explosion is not a ped for obj ", iObj)
						ENDIF
					ELSE
						PRINTLN("[fDestroyExplosionRadius] No explosion detected for obj ", iObj)
					ENDIF
				ELSE
					PRINTLN("[fDestroyExplosionRadius] It's dead or broken ", iObj)
				ENDIF
			ELSE				
				IF GET_ENTITY_HEALTH(tempObj) = 1
					CLEAR_BIT(iExplosionDestroyedObjectBitSet, iobj)
					PRINTLN("[fDestroyExplosionRadius] Clearing bit so that player can have another go - turret somehow survived. Obj: ", iObj)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF bHost
		PROCESS_DESTRUCTIBLE_CRATES(tempObj, iobj, bNetIDExists)
	ENDIF
	
	PROCESS_SEA_MINES(tempObj, iobj, bNetIDExists)
	
	PROCESS_PARTICLE_FX_OVERRIDE(iObj)
	
	CLIENT_MANAGE_FRAG_CRATE_PICKUPS(iobj)
	
	IF IS_THIS_BOMB_FOOTBALL()
		BMBFB_PROCESS_BALL(iObj)
	ENDIF
	
	IF bNetIDExists
	AND bHost
		IF IS_BIT_SET(MC_serverBD.iObjCleanup_NeedOwnershipBS, iobj)
			IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(tempObj)
				IF NOT IS_LOCAL_PLAYER_ANY_SPECTATOR()
					NETWORK_REQUEST_CONTROL_OF_ENTITY(tempObj)
					PRINTLN("[RCC MISSION] DISPLAY_OBJECT_HUD - Requesting control of object ",iobj," for cleanup")
				ELSE
					PRINTLN("[RCC MISSION] DISPLAY_OBJECT_HUD - Can't request control of entity as you are a spectator")
				ENDIF
			ELSE
				IF (IS_ANY_TEAM_BEING_WARPED_TO_START_POINT()
				AND IS_BIT_SET(MC_serverBD.iServerBitSet5, SBBOOL5_ALL_PLAYERS_RENDERPHASES_PAUSED))
				OR NOT IS_ANY_TEAM_BEING_WARPED_TO_START_POINT()
					PRINTLN("[RCC MISSION] DISPLAY_OBJECT_HUD - Deleting object ",iobj)
					DELETE_FMMC_OBJECT(iobj)
					bObjectDead = TRUE
				ENDIF
			ENDIF
		ELIF DOES_ANY_TEAM_HAVE_NEW_PRIORITY_THIS_FRAME(TRUE)
			IF CLEANUP_OBJ_EARLY(iobj, tempObj)
				PRINTLN("[RCC MISSION] DISPLAY_OBJECT_HUD - Object ",iobj," has cleaned up early, new priority/midpoint check")
				bObjectDead = TRUE
			ENDIF
		ENDIF
		
	ENDIF
	
	IF bObjectDead
		IF MC_playerBD[iLocalPart].iObjHacking = iobj
		AND IS_BIT_SET(MC_playerBD[iLocalPart].iHackFailBitset,MC_playerBD[iLocalPart].iObjHacking)
		AND MC_serverBD.iObjHackPart[MC_playerBD[iLocalPart].iObjHacking] = -1
			RESET_ALL_HACKING_MINIGAMES()
			PRINTLN("[RCC MISSION]  RESET_ALL_HACKING_MINIGAMES 1")
		ENDIF
		
		EXIT
	ENDIF
	
	CLIENT_MANAGE_SAFE_CRACK_ANIM(iobj,tempObj)
	
	if IS_THIS_A_HACK_CONTAINER(iObj)
	and has_casco_lock_been_shot_off()	
		break_off_container_lock_system() 
	endif	
	
	// [FIX_2020_CONTROLLER] - Stuff like this can definitely get functionalised and tucked away somewhere out of sight...
	IF IS_BIT_SET(MC_serverBD.iOpenHackContainer, iObj)
	AND IS_THIS_A_HACK_CONTAINER(iObj)
			
		BOOL bMocap
		INT iContainerDoorMaxAngle = 145
		
		//IF bMocap
		if not HAS_CASCO_CUTSCENE_PLAYED() 
		and not is_cutscene_playing()  

			printstring("cutscene NOT playing ")
			printnl()
			
			iContainerDoorMaxAngle = 8
		
		else 
		
			printstring("cutscene PLAYING ")
			printnl()

			bMocap = true 
			
		ENDIF
		
		printstring("iContainerDoorMaxAngle = ")
		printint(iContainerDoorMaxAngle)
		printnl()
		
		//pop off lock

		//the lock is shot off the container. 
		//The mission rule is updated and MC_serverBD.iOpenHackContainer bit set set to true
		//the door will pop open via OPEN_CONTAINER_HACK_DOORS which will also detach the vehicle via
		//DETACH_ATTACHED_CAR_FROM_CONTAINER. IS_CONTAINER_DOOR_OPEN will not return true untill 
		//mocap_been_triggered is set within open_container_hack_doors() and the target ratio is met  
		//the cutscene cutscene will not start untill the vehicle is detached. HAVE_MOCAP_START_CONDITIONS_BEEN_MET
		//on the frame the vehicle is detached the door will have hit it's none mocap target ratio of 8
		//the mocap starts and the bit set PBBOOL2_PLAYED_CONTAINER_CUTSCENE is set
		//ONLY 1 MACHINE who owns the container will open the doors / control the doors. 
		//The other machine who does not control the container will just check the door target ratio and the 
		//to prove the cutscene has been triggered. Only the host (server) wi 
		
		if is_the_casco_lock_clear_of_the_container_doors()
			IF OPEN_CONTAINER_HACK_DOORS( iobj, iContainerDoorMaxAngle, bMocap )
				
				IF is_bit_set( MC_playerBD[ iLocalPart].iClientBitSet2, PBBOOL2_PLAYED_CONTAINER_CUTSCENE )
					IF bIsLocalPlayerHost
						CLEAR_BIT( MC_serverBD.iOpenHackContainer, iObj)
						PRINTLN("[RCC MISSION] CLEAR_BIT( MC_serverBD.iOpenHackContainer test 0")
					ENDIF
				endif
			
			elif IS_CONTAINER_DOOR_OPEN(iContainerDoorMaxAngle, bMocap)
			
				IF is_bit_set( MC_playerBD[ iLocalPart].iClientBitSet2, PBBOOL2_PLAYED_CONTAINER_CUTSCENE )
					IF bIsLocalPlayerHost
					AND IS_CONTAINER_VEHICLE_DETACHED(iobj)
						CLEAR_BIT( MC_serverBD.iOpenHackContainer, iObj )
						PRINTLN("[RCC MISSION] CLEAR_BIT( MC_serverBD.iOpenHackContainer test 1")
					ENDIF
				ENDIF 
			ENDIF
		endif 
		
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectBitSet, cibsOBJ_Reset_If_Dropped_Out_Of_Bounds)
	AND NOT IS_BIT_SET(iObjHeldBS, iobj)
	AND DOES_ENTITY_EXIST(tempObj)
		IF IS_ENTITY_ATTACHED_TO_ANY_PED(tempObj)
			SET_BIT(iObjHeldBS, iobj)
		ENDIF
	ENDIF
	
	IF MC_playerBD[iLocalPart].iObjHacking = iobj
		IF NOT (MC_serverBD_4.iObjPriority[iobj][MC_playerBD[iPartToUse].iteam] <= MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]
		AND MC_serverBD_4.iObjPriority[iobj][MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES
		AND MC_serverBD_4.iObjRule[iobj][MC_playerBD[iPartToUse].iteam] != FMMC_OBJECTIVE_LOGIC_NONE
		AND MC_serverBD_4.iObjRule[iobj][MC_playerBD[iPartToUse].iteam]  = FMMC_OBJECTIVE_LOGIC_MINIGAME
		AND (IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_TYPE_LESTER_SNAKE)
		OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_TYPE_BEAM_HACK)
		OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_TYPE_HOTWIRE)
		OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_TYPE_ORDER_UNLOCK)
		OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_TYPE_FINGERPRINT_CLONE)))
		AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetThree, cibsOBJ3_AllowMinigameOffRule)			
			IF IS_BIT_SET(iLocalBoolCheck14, LBOOL14_ALLOW_LESTER_SNAKE_MINIGAME)
				CLEANUP_AND_QUIT_CIRCUIT_HACKING_MINIGAME(hackingMinigameData, IS_BIT_SET(MC_serverBD_1.iServerHackMGBitset, SBBOOL_HACK_DO_HARD))
				MC_playerBD[iLocalPart].iObjHacked = -1
				RESET_OBJ_HACKING_INT()
				PRINTLN("Cleaning up Lester Hack")
				CLEAR_BIT(iLocalBoolCheck14, LBOOL14_ALLOW_LESTER_SNAKE_MINIGAME)
			ELIF IS_BIT_SET(iLocalBoolCheck26, LBOOL26_STARTED_HOTWIRE_HACK)
				RESET_OBJ_HACKING_INT()
				MC_playerBD[iLocalPart].iObjHacked = -1
				PRINTLN("[Hotwire] Cleaning up")
				CLEAN_UP_HOTWIRE_MINIGAME(sHotwire[iobj])
				CLEAR_BIT(iLocalBoolCheck26, LBOOL26_STARTED_HOTWIRE_HACK)
			ELIF IS_BIT_SET(iLocalBoolCheck32, LBOOL32_STARTED_ORDER_UNLOCK)
				RESET_OBJ_HACKING_INT()
				MC_playerBD[iLocalPart].iObjHacked = -1
				PRINTLN("[OrderUnlock] Cleaning up")
				CLEAN_UP_ORDER_UNLOCK(sOrderUnlockGameplay, sOrderUnlock[iobj], DEFAULT, HASH("heist"), g_HeistTelemetryData.iHeistPosixTime)
				CLEAR_BIT(iLocalBoolCheck32, LBOOL32_STARTED_ORDER_UNLOCK)
			ELIF IS_BIT_SET(iLocalBoolCheck32, LBOOL32_STARTED_FINGERPRINT_CLONE)
				RESET_OBJ_HACKING_INT()
				MC_playerBD[iLocalPart].iObjHacked = -1
				PRINTLN("[FingerprintClone] Cleaning up")
				CLEAN_UP_FINGERPRINT_CLONE(sFingerprintCloneGameplay, sFingerprintClone[iobj], DEFAULT, HASH("heist"), g_HeistTelemetryData.iHeistPosixTime)
				CLEAR_BIT(iLocalBoolCheck32, LBOOL32_STARTED_FINGERPRINT_CLONE)
			ELIF IS_BIT_SET(iLocalBoolCheck26, LBOOL26_STARTED_BEAM_HACK)
				RESET_OBJ_HACKING_INT()
				MC_playerBD[iLocalPart].iObjHacked = -1
				PRINTLN("[BeamHack] Cleaning up")
				CLEAN_UP_BEAM_HACK_MINIGAME(sBeamhack[iobj])
				
				IF bhaBeamHackAnimState = BHA_STATE_LOOPING
				OR bhaBeamHackAnimState = BHA_STATE_ENTER
					bhaBeamHackAnimState = BHA_STATE_EXIT
				ENDIF
								
				CLEAR_BIT(iLocalBoolCheck26, LBOOL26_STARTED_BEAM_HACK)
			ENDIF
		ENDIF
	ENDIF
	
	IF (MC_serverBD_4.iObjPriority[iobj][MC_playerBD[iPartToUse].iteam] <= MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]
	AND MC_serverBD_4.iObjPriority[iobj][MC_playerBD[iPartToUse].iteam] < FMMC_MAX_RULES)
	OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetThree, cibsOBJ3_AllowMinigameOffRule)
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitsetThirteen, ciENABLE_FORCE_CONTROL_OBJECT_BARS_TO_BE_TEAM_BASED)
		AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitsetNine[MC_serverBD_4.iObjPriority[iobj][MC_playerBD[iPartToUse].iteam]], ciBS_RULE9_USE_GRANULAR_CAPTURE)
			INT iTeamO
			FOR iTeamO = 0 TO MC_ServerBD.iNumberOfTeams - 1					
				IF IS_TEAM_ACTIVE(iTeamO)
					IF HAS_NET_TIMER_STARTED(MC_serverBD.tdControlObjTeamTimer[iTeamO])										
						PRINTLN("[LM][CONTROL OBJECT] - iTeamO: ", iTeamO, " Timer has started.")
						
						IF iTeamO = MC_serverBD.iObjTeamCarrier
						AND NOT IS_BIT_SET(MC_serverBD.iServerBitSet6, SBBOOL6_CONTROL_TIME_STOPPED_T0 + iTeamO)
							PRINTLN("[LM][CONTROL OBJECT] - iTeamO: ", iTeamO, " Is the carrier of the object. Assigning Cached Timer var.")
							iTimeObjectTeamControlBarCached[iTeamO] = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME( MC_serverBD.tdControlObjTeamTimer[iTeamO])
						ELSE
							iTimeObjectTeamControlBarCached[iTeamO] = MC_serverBD.iTimeObjectTeamControlBarCache[iTeamO]
						ENDIF
					ELSE
						PRINTLN("[LM][CONTROL OBJECT] - iTeamO: ", iTeamO, " Timer has not started.")
						iTimeObjectTeamControlBarCached[iTeamO] = 0
					ENDIF
															
					PRINTLN("[LM][CONTROL OBJECT] - iTeamO: ", iTeamO, " iTime: ", iTimeObjectTeamControlBarCached[iTeamO])
					PRINTLN("[LM][CONTROL OBJECT] - MAX: ", MC_serverBD.iObjTakeoverTime[ iTeamO ][ MC_serverBD_4.iObjPriority[iobj][MC_playerBD[iPartToUse].iteam] ], " OFFSET: ", MC_serverBD.iObjectCaptureOffset[iTeamO])
					
					fPowerMadCaptureProgress[iTeamO] = TO_FLOAT(iTimeObjectTeamControlBarCached[iTeamO] + MC_serverBD.iObjectCaptureOffset[iTeamO]) / TO_FLOAT(MC_serverBD.iObjTakeoverTime[iTeamO][MC_serverBD_4.iObjPriority[iobj][MC_playerBD[iPartToUse].iteam]])
				ENDIF
			ENDFOR
		ENDIF
		
		IF MC_serverBD_4.iObjRule[iobj][MC_playerBD[iPartToUse].iteam] != FMMC_OBJECTIVE_LOGIC_NONE
		OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetThree, cibsOBJ3_AllowMinigameOffRule)	
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciENABLE_DIVE_SCORE_ANIM)
				IF GET_MC_SERVER_GAME_STATE() != GAME_STATE_END
					IF IS_ENTITY_ATTACHED_TO_ENTITY(tempObj, LocalPlayerPed)
						IF NOT ANIMPOSTFX_IS_RUNNING("InchPickup")
							PRINTLN("[JJT] DISPLAY_OBJECT_HUD - Starting InchPickup FX")
							ANIMPOSTFX_PLAY("InchPickup", 0, TRUE)
						ENDIF
					ELSE
						IF ANIMPOSTFX_IS_RUNNING("InchPickup")
							PRINTLN("[JJT] DISPLAY_OBJECT_HUD - Killing InchPickup FX")
							ANIMPOSTFX_STOP("InchPickup")
							
							PRINTLN("[JJT] DISPLAY_OBJECT_HUD - Playing InchPickupOut FX")
							ANIMPOSTFX_PLAY("InchPickupOut", 0, FALSE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		
			IF IS_OBJECT_A_CONTAINER(tempObj)
				IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
					PlayerVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
					IF IS_VEHICLE_DRIVEABLE(PlayerVeh)
						IF IS_VEHICLE_MODEL(PlayerVeh, HANDLER)
							SET_INPUT_EXCLUSIVE(PLAYER_CONTROL, INPUT_CONTEXT) // Needed for PC to stop the horn sounding in default controls
							IF NOT IS_ANY_ENTITY_ATTACHED_TO_HANDLER_FRAME(PlayerVeh)
								IF IS_HANDLER_FRAME_LINED_UP_WITH_CONTAINER(PlayerVeh,tempObj)
									IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdDhandlerTimer) > 1000
										IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL,INPUT_CONTEXT)
											REINIT_NET_TIMER(tdDhandlerTimer)
											ATTACH_CONTAINER_TO_HANDLER_FRAME_WHEN_LINED_UP(PlayerVeh,tempObj)
										ENDIF
									ENDIF
								ENDIF
								
								IF(IS_BIT_SET(iContBitSet,iobj))
									SET_BIT(iDropContBitSet,iobj)
									CLEAR_BIT(iContBitSet,iobj)
								ENDIF
								
							ELSE
								
								//reset timer each frame when the container is attached to handler frame
								REINIT_NET_TIMER(tdDhandlerTimer)
								
								/*
								IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tdDhandlerTimer) > 5000
									PRINTLN("Ok to detach.")
									IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL,INPUT_VEH_HEADLIGHT)
										PRINTLN("Detaching.")
										REINIT_NET_TIMER(tdDhandlerTimer)
										DETACH_CONTAINER_FROM_HANDLER_FRAME(PlayerVeh)
									ENDIF
								ENDIF
								*/

							ENDIF
						ELIF ( (IS_VEHICLE_MODEL(PlayerVeh, CARGOBOB)) OR (IS_VEHICLE_MODEL(PlayerVeh, CARGOBOB2)) OR (IS_VEHICLE_MODEL(PlayerVeh, CARGOBOB3)) )
							IF NOT IS_ENTITY_ATTACHED_TO_ENTITY(tempObj,PlayerVeh)
								IF(IS_BIT_SET(iContBitSet,iobj))
									SET_BIT(iDropContBitSet,iobj)
									CLEAR_BIT(iContBitSet,iobj)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//If it was just dropped:
				IF(IS_BIT_SET(iDropContBitSet,iobj))
					IF(HAS_ENTITY_COLLIDED_WITH_ANYTHING(tempObj))
						
						INT iVeh
						
						FOR iVeh = 0 TO MC_serverBD.iNumVehCreated
							IF(NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh]))
								VEHICLE_INDEX tempVeh = NET_TO_VEH(MC_serverBD_1.sFMMC_SBD.niVehicle[iveh])
							
								IF(IS_VEHICLE_MODEL(tempVeh,FLATBED))
								OR (IS_VEHICLE_MODEL(tempVeh,INT_TO_ENUM(MODEL_NAMES, HASH("WASTELANDER"))))
									IF(GET_DISTANCE_BETWEEN_ENTITIES(tempVeh,tempObj) < 10)
										IF( IS_VECTOR_IN_AREA(GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(tempVeh,GET_ENTITY_COORDS(tempObj)),<<0.0000,-2.8000,0.4100>>,<<3,3.5,2.5>>) )
											IF( IS_VECTOR_IN_AREA(GET_ENTITY_ROTATION(tempObj),GET_ENTITY_ROTATION(tempVeh),<<80,80,80>>) )
												ATTACH_OBJECT_TO_FLATBED(tempObj,tempVeh, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].vAttachOffset, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].vAttachOffsetRotation, TRUE)
												CLEAR_BIT(iDropContBitSet,iobj)
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
						ENDFOR
						
						IF NOT (IS_ENTITY_IN_AIR(tempObj))
							CLEAR_BIT(iDropContBitSet,iobj)
						ENDIF
						
					ENDIF
				ENDIF
			ENDIF
			
			INT iTeam = MC_playerBD[iPartToUse].iteam
			INT iObjectPriority = MC_serverBD_4.iObjPriority[ iobj ][ iTeam ]
			INT iRule = MC_serverBD_4.iCurrentHighestPriority[ iTeam ]
			
			IF DOES_ENTITY_EXIST(tempObj)
				IF NOT IS_ENTITY_ATTACHED_TO_ANY_PED(tempObj)
				OR IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectBitSetTwo, cibsOBJ2_MinigameObjAttachToPed)
					
					IF MC_playerBD[iLocalPart].iObjHacking = iobj
						IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iHackFailBitset, MC_playerBD[iLocalPart].iObjHacking)
							IF g_bMissionEnding
								SET_HACK_FAIL_BIT_SET(4)
								PRINTLN("[RCC MISSION]  setting hack fail bitset - mission finished: ",iLocalPart," for object: ",MC_playerBD[iLocalPart].iObjHacking)
							ENDIF
							IF iSpectatorTarget != -1
								SET_HACK_FAIL_BIT_SET(3)
								PRINTLN("[RCC MISSION]  setting hack fail bitset - iSpectatorTarget != -1, part: ",iPartToUse," for object: ",MC_playerBD[iPartToUse].iObjHacking)
							ENDIF
							IF NOT bPlayerToUseOK
								SET_HACK_FAIL_BIT_SET(2)
								RENDER_SCRIPT_CAMS(FALSE, TRUE)
								PRINTLN("[RCC MISSION]  setting hack fail bitset - dead, part: ",iPartToUse," for object: ",MC_playerBD[iLocalPart].iObjHacking)
							ELSE
								IF GET_DISTANCE_BETWEEN_ENTITIES(LocalPlayerPed,tempObj) >= 3.0
								AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iLegacyMiniGameBitSet, cibsLEGACY_FMMC_MINI_GAME_TYPE_INTERACT_WITH)
								AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iLegacyMiniGameBitSet, cibsLEGACY_FMMC_MINI_GAME_USE_THERMAL_CHARGE)
								AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].fActivationRangeOverride <= 0.0
									SET_HACK_FAIL_BIT_SET(1)
									PRINTLN("[RCC MISSION]  setting hack fail bitset - too far, part: ",iPartToUse," for object: ",MC_playerBD[iLocalPart].iObjHacking)
								ENDIF
								
								IF SVM_FLOW_IS_CURRENT_MISSION_SVM_MISSION()
									IF IS_PED_RUNNING_RAGDOLL_TASK(LocalPlayerPed)
									OR IS_PED_RAGDOLL(LocalPlayerPed)
									AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iLegacyMiniGameBitSet, cibsLEGACY_FMMC_MINI_GAME_USE_THERMAL_CHARGE)
										SET_HACK_FAIL_BIT_SET(0)
										PRINTLN("[RCC MISSION]  setting hack fail bitset - ragdolling, part: ",iPartToUse," for object: ",MC_playerBD[iLocalPart].iObjHacking)
									ENDIF
								ENDIF
							ENDIF
						ELSE
							IF MC_serverBD.iObjHackPart[MC_playerBD[iLocalPart].iObjHacking] = -1
								RESET_ALL_HACKING_MINIGAMES()
								PRINTLN("[RCC MISSION]  RESET_ALL_HACKING_MINIGAMES 2")
							ENDIF
						ENDIF
					ELIF MC_serverBD.iObjHackPart[iobj] = iPartToUse
						SET_BIT(iLocalBoolCheck12, LBOOL12_SERVER_STILL_THINKS_IM_HACKING_TEMP)
					ENDIF
					
					IF iSpectatorTarget = -1
						
						//Why is this here?
						IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitset[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE_ENABLETRACKIFY)
							IF NOT IS_CELLPHONE_TRACKIFY_IN_USE()
								IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TRACKIFY_PHO")
								AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DR_CNIC")
									//CLEAR_HELP()
									IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciHIDE_DRIVING_ARROW_HELP)
										DISPLAY_HELP_TEXT_THIS_FRAME("TRACKIFY_PHO",TRUE)
										PRINTLN("[RCC MISSION]  DISPLAYING TRACKIFY_PHO TEXT")
									ENDIF
								ENDIF
								IF IS_PED_RUNNING_MOBILE_PHONE_TASK(LocalPlayerPed)
								AND NOT IS_CELLPHONE_CAMERA_IN_USE()
									IF NOT IS_PED_INJURED(LocalPlayerPed)
										CLEAR_PED_TASKS(LocalPlayerPed)
									ENDIF
								ENDIF
							ELSE
								IF NETWORK_HAS_CONTROL_OF_ENTITY(LocalPlayerPed)
									IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciHIDE_DRIVING_ARROW_HELP)
										IF NOT IS_PED_RUNNING_MOBILE_PHONE_TASK(LocalPlayerPed)
											CLEAR_PED_TASKS(LocalPlayerPed)
											TASK_USE_MOBILE_PHONE(LocalPlayerPed,TRUE,Mode_ToText)
											//CELL_CAM_ACTIVATE(FALSE,FALSE)
											//CELL_CAM_ACTIVATE_SELFIE_MODE(FALSE)
										ENDIF
									ENDIF
								ENDIF
								IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TRACKIFY_PHO")
									CLEAR_HELP()
									PRINTLN("[RCC MISSION]  CLEARING HELP AS TRACKIFY IS IN USE")
								ENDIF
							ENDIF
						ELSE
							IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TRACKIFY_PHO")
								CLEAR_HELP()
								PRINTLN("[RCC MISSION]  CLEARING HELP AS ENABLETRACKIFY RULE IS OFF")
							ENDIF
						ENDIF
						
						IF MC_playerBD[iPartToUse].iObjNear = iObj OR MC_playerBD[iPartToUse].iObjNear = -1
							IF MC_serverBD_4.iObjRule[iobj][MC_playerBD[iPartToUse].iteam] =  FMMC_OBJECTIVE_LOGIC_PHOTO
								IF GET_DISTANCE_BETWEEN_ENTITIES(LocalPlayerPed,tempObj) < ci_PHOTO_RANGE
									IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitsetEleven[MC_serverBD_4.iCurrentHighestPriority[MC_playerBD[iPartToUse].iteam]], ciBS_RULE11_BLOCK_PHOTO_OBJECT_HELP_TEXT)
										IF NOT IS_PHONE_ONSCREEN()
											IF GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_CELLPHONE_QUICKLAUNCH) = 1
												IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PH_FAST_PHO")
													PRINT_HELP("PH_FAST_PHO")
												ENDIF
											ELSE
												IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PH_FAST_PHO_2")
													PRINT_HELP("PH_FAST_PHO_2")
												ENDIF
											ENDIF
										ENDIF
									ENDIF
									
									SET_OBJ_NEAR(iObj)
									
									vObjCoords = GET_ENTITY_COORDS(tempObj)
									
									IF NOT DOES_PHOTO_TRACKED_POINT_EXIST(iPhotoTrackedPoint)
										//create new tracked point for current target coords
										CREATE_PHOTO_TRACKED_POINT_FOR_COORD(iPhotoTrackedPoint, vObjCoords, 1.0)
										vLocalPhotoCoords = vObjCoords
									ELSE
										IF NOT ARE_VECTORS_ALMOST_EQUAL(vObjCoords, vLocalPhotoCoords, 0.05) //If the object's moved
											vLocalPhotoCoords = vObjCoords
											SET_TRACKED_POINT_INFO(iPhotoTrackedPoint,vLocalPhotoCoords,1.0)
										ENDIF
										
										IF (GET_ENTITY_MODEL(tempObj) = PROP_BOX_WOOD02A_MWS)	//If it's one of these models handle it differently,
										OR (GET_ENTITY_MODEL(tempObj) = PROP_BOX_WOOD02A_PU)	//as they block the line of sight to their own
										OR (GET_ENTITY_MODEL(tempObj) = PROP_BOX_WOOD02A)		//centre points
										OR (GET_ENTITY_MODEL(tempObj) = PROP_CONTR_03B_LD)		
										OR (GET_ENTITY_MODEL(tempObj) = PROP_CONTAINER_LD_PU)
											
											IF IS_POINT_IN_CELLPHONE_CAMERA_VIEW(vObjCoords, 0.4)
											AND HAS_CELLPHONE_CAM_JUST_TAKEN_PIC()
											AND (stLOSCheck = NULL)
												stLOSCheck = START_SHAPE_TEST_LOS_PROBE(GET_GAMEPLAY_CAM_COORD(),vObjCoords,SCRIPT_INCLUDE_ALL,LocalPlayerPed)
											ENDIF

											IF(stLOSCheck != NULL)
												INT iHitCount
												VECTOR vHitPoint, vHitNormal
												ENTITY_INDEX eiHitEntity
												SHAPETEST_STATUS eStatus = GET_SHAPE_TEST_RESULT(stLOSCheck, iHitCount, vHitPoint, vHitNormal, eiHitEntity)
												
												IF(eStatus = SHAPETEST_STATUS_RESULTS_READY)
													stLOSCheck = NULL
													IF iHitCount = 0
														RESET_PHOTO_DATA()
														//CLEANUP_PHOTO_TRACKED_POINT(iPhotoTrackedPoint)
														MC_playerBD[iPartToUse].iObjNear 	= -1
														MC_playerBD[iPartToUse].iObjPhoto 	= iobj
														
														IF SHOULD_PUT_AWAY_PHONE_AFTER_PHOTO(MC_serverBD_4.iObjPriority[iobj][MC_playerBD[iPartToUse].iTeam], ci_TARGET_OBJECT)
															SET_BIT(iLocalBoolCheck3,LBOOL3_PHOTOTAKEN)
															REINIT_NET_TIMER(tdPhotoTakenTimer)
														ENDIF
													ELSE
														IF(DOES_ENTITY_EXIST(eiHitEntity))
															IF IS_ENTITY_AN_OBJECT(eiHitEntity)
																IF(GET_OBJECT_INDEX_FROM_ENTITY_INDEX(eiHitEntity) = tempObj)
																	RESET_PHOTO_DATA()
																	//CLEANUP_PHOTO_TRACKED_POINT()
																	MC_playerBD[iPartToUse].iObjNear 	= -1
																	MC_playerBD[iPartToUse].iObjPhoto 	= iobj
																	
																	IF SHOULD_PUT_AWAY_PHONE_AFTER_PHOTO(MC_serverBD_4.iObjPriority[iobj][MC_playerBD[iPartToUse].iTeam], ci_TARGET_OBJECT)
																		SET_BIT(iLocalBoolCheck3,LBOOL3_PHOTOTAKEN)
																		REINIT_NET_TIMER(tdPhotoTakenTimer)
																	ENDIF
																ENDIF
															ENDIF
														ENDIF
													ENDIF
												ELIF (eStatus = SHAPETEST_STATUS_NONEXISTENT)
													stLOSCheck = NULL
												ENDIF
											ENDIF
										
										ELSE //It's a normal object, use the normal checks
											IF IS_PHOTO_TRACKED_POINT_VISIBLE(iPhotoTrackedPoint)
												IF IS_POINT_IN_CELLPHONE_CAMERA_VIEW(vObjCoords, 0.2)
													IF HAS_CELLPHONE_CAM_JUST_TAKEN_PIC()
														RESET_PHOTO_DATA()
														//CLEANUP_PHOTO_TRACKED_POINT(iPhotoTrackedPoint)
														MC_playerBD[iPartToUse].iObjNear 	= -1
														MC_playerBD[iPartToUse].iObjPhoto 	= iobj
														
														IF SHOULD_PUT_AWAY_PHONE_AFTER_PHOTO(MC_serverBD_4.iObjPriority[iobj][MC_playerBD[iPartToUse].iTeam], ci_TARGET_OBJECT)
															SET_BIT(iLocalBoolCheck3,LBOOL3_PHOTOTAKEN)
															REINIT_NET_TIMER(tdPhotoTakenTimer)
														ENDIF
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ELSE
									IF MC_playerBD[iPartToUse].iObjNear = iObj
										RESET_PHOTO_DATA()
										MC_playerBD[iPartToUse].iObjNear = -1
									ENDIF
								ENDIF
							ELSE
								IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PH_FAST_PHO")
								OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PH_FAST_PHO_2")
									CLEAR_HELP()
								ENDIF
								
								IF MC_playerBD[iPartToUse].iObjNear = iObj
									RESET_PHOTO_DATA()
									MC_playerBD[iPartToUse].iObjNear = -1
								ENDIF
							ENDIF
						ENDIF
						
						IF MC_serverBD_4.iObjRule[iobj][MC_playerBD[iPartToUse].iteam]  = FMMC_OBJECTIVE_LOGIC_MINIGAME
						
							IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_TYPE_OPEN_SAFE)
								
								PROCESS_SAFE_CRACKING_MINI_GAME(tempObj,iobj)
								
							ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_TYPE_HACK_COMPUTER)

								PROCESS_HACKING_MINI_GAME(tempObj,iobj)

							ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_TYPE_BLOW_DOOR)

								PROCESS_BLOW_OPEN_DOOR(tempObj,iobj)	
								
							ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_TYPE_HACK_COMPUTER2)
							
								PROCESS_HACKING_MINI_GAME(tempObj,iobj,TRUE)
								
							ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_TYPE_SYNC_UNLOCK)

								PROCESS_SYNC_LOCK_MINI_GAME(tempObj,iobj)
								
							ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_TYPE_HUMANE_LABS_SYNC)
						
								PROCESS_HUMANE_SYNC_LOCK_MINI_GAME(tempObj,iobj)

							ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_TYPE_CASH_GRAB ) 
								
								PROCESS_CASH_GRAB(tempObj,iobj)	
								
							ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_TYPE_OPEN_CASE) 
								
								PROCESS_OPEN_CASE(tempObj,iobj)
							
							ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_TYPE_DRILLING) 
							
								PROCESS_DRILL_MINI_GAME(tempObj,iobj)
							ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_TYPE_VAULT_DRILLING) 
							
								PROCESS_VAULT_DRILL_MINI_GAME(tempObj,iobj)
							ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_TYPE_OPEN_CONTAINER) 
							
								PROCESS_INVESTIGATE_CONTAINER_MINI_GAME(tempObj,iobj)
									
							ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_TYPE_LESTER_SNAKE) 
								PROCESS_LESTER_SNAKE_GAME_MC(tempObj,iobj)
							ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_TYPE_BEAM_HACK) 
								PROCESS_BEAM_HACK_GAME_MC(tempObj,iobj)
							ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iLegacyMiniGameBitSet, cibsLEGACY_FMMC_MINI_GAME_TYPE_INTERACT_WITH) 
								PROCESS_INTERACT_WITH_OBJECT_MINIGAME(tempObj,iobj)
							
							ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_TYPE_ENTER_SCRIPTED_TURRET) 
								PROCESS_ENTER_SCRIPTED_TURRET_MINIGAME_MC(tempObj,iobj)
								
							ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_TYPE_HOTWIRE) 
								PROCESS_HOTWIRE_GAME_MC(tempObj,iobj)
							ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_TYPE_ORDER_UNLOCK) 
								PROCESS_FINGERPRINT_KEYPAD_HACK_GAME_MC(tempObj, iobj, cibsLEGACY_FMMC_MINI_GAME_TYPE_ORDER_UNLOCK)
							ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_TYPE_FINGERPRINT_CLONE) 
								PROCESS_FINGERPRINT_KEYPAD_HACK_GAME_MC(tempObj, iobj, cibsLEGACY_FMMC_MINI_GAME_TYPE_FINGERPRINT_CLONE)
							ENDIF
						ELSE
							IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetThree, cibsOBJ3_AllowMinigameOffRule)	
								IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_TYPE_CASH_GRAB ) 
									PROCESS_CASH_GRAB(tempObj,iobj)
								ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_TYPE_INTERACT_WITH) 
									PROCESS_INTERACT_WITH_OBJECT_MINIGAME(tempObj,iobj)
								ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_TYPE_ORDER_UNLOCK) 
									PROCESS_FINGERPRINT_KEYPAD_HACK_GAME_MC(tempObj, iobj, cibsLEGACY_FMMC_MINI_GAME_TYPE_ORDER_UNLOCK)
								ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iLegacyMiniGameBitSet,cibsLEGACY_FMMC_MINI_GAME_TYPE_FINGERPRINT_CLONE) 
									PROCESS_FINGERPRINT_KEYPAD_HACK_GAME_MC(tempObj, iobj, cibsLEGACY_FMMC_MINI_GAME_TYPE_FINGERPRINT_CLONE)
								ENDIF
							ENDIF
						ENDIF
						
						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectBitSetTwo, cibsOBJ2_SetInvisible)
							IF NETWORK_HAS_CONTROL_OF_ENTITY(tempObj)
							AND IS_ENTITY_VISIBLE(tempObj)
								SET_ENTITY_VISIBLE(tempObj, FALSE)
							ENDIF
						ENDIF
						
						IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].mn = HEI_PROP_HEI_TIMETABLE
						AND MC_serverBD_4.iObjPriority[iobj][MC_playerBD[iPartToUse].iTeam] > g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iPriority[MC_playerBD[iPartToUse].iTeam]
						AND DOES_ENTITY_EXIST(tempObj)
						AND NOT IS_ENTITY_ATTACHED_TO_ANY_PED(tempObj)
							IF NETWORK_HAS_CONTROL_OF_ENTITY(tempObj)
							AND VDIST2( GET_PED_BONE_COORDS(LocalPlayerPed, BONETAG_R_FOOT, <<0, 0, -0.1>>), GET_ENTITY_COORDS(tempObj) ) <= 1.0
								CPRINTLN( DEBUG_SIMON, "DISPLAY_OBJECT_HUD - Attaching timetable object ",iobj," as nobody has it and I'm next to it")
								ATTACH_ENTITY_TO_ENTITY( tempObj, LocalPlayerPed, GET_PED_BONE_INDEX( LocalPlayerPed, BONETAG_PH_L_HAND ),
									<<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, TRUE, TRUE, FALSE )
								PLAY_SOUND_FRONTEND( -1, "Bus_Schedule_Pickup", "DLC_PRISON_BREAK_HEIST_SOUNDS", FALSE)
								SET_ENTITY_VISIBLE( tempObj, FALSE )
								
								MC_playerBD[ iPartToUse ].iObjHacking = iObj // broadcast associate request							
								SET_CURRENT_SPECIAL_PICKUP_STATE( eSpecialPickupState_WAIT_FOR_DELIVERY )
							ENDIF
						ENDIF
						
						BOOL bAllowProximityPickup = TRUE
						
						IF iRule < FMMC_MAX_RULES
							IF (g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMaxDeliveryEntitiesCanCarry[iRule] > 0)
								IF (MC_playerBD[iLocalPart].iObjCarryCount >= g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMaxDeliveryEntitiesCanCarry[iRule])
									bAllowProximityPickup = FALSE
								ENDIF
							ENDIF
							IF IS_PICKUP_AT_SPECIFIC_HOLDING(iobj, iTeam)
							OR IS_BIT_SET(iLocalObjAtHolding, iObj)
								bAllowProximityPickup = FALSE
							ENDIF
							IF IS_BIT_SET(iLocalBoolCheck29, LBOOL29_MAX_NUM_OBJECTS_BEING_CARRIED)
								bAllowProximityPickup = FALSE
								IF MC_playerBD[iLocalPart].iObjCarryCount < g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iMaxDeliveryEntitiesCanCarry[iRule]
									CLEAR_BIT(iLocalBoolCheck29, LBOOL29_MAX_NUM_OBJECTS_BEING_CARRIED)
								ENDIF
							ENDIF
						ENDIF
						
						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectBitSet, cibsOBJ_UsePickupProximity)
						AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectBitSetThree, cibsOBJ3_AlternateProximityPickup)
							IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iGotoRange != 0
							AND DOES_ENTITY_EXIST(tempObj)
								IF ( NOT IS_ENTITY_ATTACHED_TO_ANY_PED( tempObj ) )
								AND ( GET_DISTANCE_BETWEEN_ENTITIES( LocalPlayerPed, tempObj ) <= g_FMMC_STRUCT_ENTITIES.sPlacedObject[ iobj ].iGotoRange )
								AND DOES_ENTITY_EXIST(tempObj)
								AND NETWORK_HAS_CONTROL_OF_ENTITY(tempObj)
									ATTACH_ENTITY_TO_ENTITY( tempObj, LocalPlayerPed, GET_PED_BONE_INDEX( LocalPlayerPed, BONETAG_PH_L_HAND ),
													<<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, TRUE, TRUE, FALSE )
									PLAY_SOUND_FRONTEND( -1, "Bus_Schedule_Pickup", "DLC_PRISON_BREAK_HEIST_SOUNDS", FALSE)
									SET_ENTITY_VISIBLE( tempObj, FALSE )
								ENDIF
							ENDIF
						ENDIF
						
						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectBitSetThree, cibsOBJ3_AlternateProximityPickup)
						AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectBitSet, cibsOBJ_UsePickupProximity)
							IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iGotoRange != 0
							AND DOES_ENTITY_EXIST(tempObj)
								IF ( NOT IS_ENTITY_ATTACHED_TO_ANY_PED( tempObj ) )
								AND bAllowProximityPickup
								AND ( GET_DISTANCE_BETWEEN_ENTITIES( LocalPlayerPed, tempObj ) <= g_FMMC_STRUCT_ENTITIES.sPlacedObject[ iobj ].iGotoRange )
								AND DOES_ENTITY_EXIST(tempObj)
								AND NETWORK_HAS_CONTROL_OF_ENTITY(tempObj)
								AND NOT IS_PED_INJURED(LocalPlayerPed)
								AND NOT HAS_NET_TIMER_STARTED(timerManualRespawn)
									PRINTLN("[RCC MISSION] cibsOBJ3_AlternateProximityPickup - Moving object ",iobj," to myself this frame.")
									SET_BIT(iLocalBoolCheck29, LBOOL29_MAX_NUM_OBJECTS_BEING_CARRIED)
									VECTOR vtempPlayerCoord = GET_ENTITY_COORDS(LocalPlayerPed)
									VECTOR vTempObjCoord = GET_ENTITY_COORDS(tempObj)
									vtempPlayerCoord.z = vTempObjCoord.z
									SET_ENTITY_COORDS(tempObj, vtempPlayerCoord)
									SET_ENTITY_VISIBLE( tempObj, FALSE )
								ELSE
									IF NOT IS_ENTITY_VISIBLE(tempObj)
									AND GET_ENTITY_ATTACHED_TO(tempObj) = NULL
									AND NETWORK_HAS_CONTROL_OF_ENTITY(tempObj)
										SET_ENTITY_VISIBLE(tempObj, TRUE)
									ENDIF
								ENDIF
							ENDIF
						ENDIF	
						
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSix, ciUSE_IN_AND_OUT_HELPTEXT)	
							IF NOT IS_BIT_SET(AssortedBoolBitset, HasSeenMaxCapHelptext)
								IF NOT IS_PLAYER_IN_DROP_OFF(5,5)
									IF MC_playerBD[iPartToUse].iObjCarryCount > 0
										IF VDIST2( GET_PED_BONE_COORDS(LocalPlayerPed, BONETAG_R_FOOT, <<0, 0, -0.1>>), GET_ENTITY_COORDS(tempObj) ) <= 1.0
											IF NOT IS_PED_INJURED(LocalPlayerPed)
												PRINT_HELP("MAXCARRY")
												SET_BIT(AssortedBoolBitset, HasSeenMaxCapHelptext)
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF IS_BIT_SET(iAICarryBitset,iobj)
				
						//-- Dave W, takes a frame or two for the object to be attached. Don't say it's been dropped if it hasn't been attached yet.
						IF IS_BIT_SET(iAIAttachedBitset,iobj)
							INT isublogic = GET_CTF_TICKER_ICON_TYPE_TO_DISPLAY()
							
							BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_AI_DROP_OBJ,0,MC_playerBD[iPartToUse].iteam,isublogic,NULL,ci_TARGET_OBJECT,iobj)
							CLEAR_BIT(iAIAttachedBitset,iobj)
							CLEAR_BIT(iAICarryBitset,iobj)
						ENDIF
					ENDIF
					
				ELSE
					//-- Attached
					IF IS_BIT_SET(iAICarryBitset,iobj)
						IF NOT IS_BIT_SET(iAIAttachedBitset,iobj)
							SET_BIT(iAIAttachedBitset,iobj)
							PRINTLN("[RCC MISSION] [dsw] Object attached obj =  ", iobj)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			// This sets up the progression rule for a go-to rule on an object (which could be a pickup)
			IF MC_serverBD_4.iObjRule[iobj][MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_GO_TO
			AND iSpectatorTarget = -1
				// This is the override value for the range at which you have "arrived" at the object
				#IF IS_DEBUG_BUILD
				IF bObjectGotoDebug
					PRINTLN("[JS][ObjGoto] - GOTO Obj - ", iobj)
				ENDIF
				#ENDIF
				BOOL bHasGoToRange
				
				//CV: if special case go to object play anim (special case is police station bus schedule)
				VECTOR vPoliceBusScheduleCoords = <<436.57104, -996.42578, 29.68951>>							
				//if using clip board and at station
				IF( ( g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].mn = HEI_PROP_HEI_TIMETABLE
				AND GET_DISTANCE_BETWEEN_ENTITY_AND_COORD( tempObj, vPoliceBusScheduleCoords ) < 5.0
				AND MC_playerBD[ iPartToUse ].iObjNear = -1 )
				OR  sCurrentSpecialPickupState.ePickupState <> eSpecialPickupState_BEFORE_PICKUP )
					#IF IS_DEBUG_BUILD
					IF bObjectGotoDebug
						PRINTLN("[JS][ObjGoto] - Special Pickup")
					ENDIF
					#ENDIF
					bHasGoToRange = TRUE
					
					IF( sCurrentSpecialPickupState.ePickupState = eSpecialPickupState_BEFORE_PICKUP )
						IF( NOT IS_ENTITY_ATTACHED_TO_ANY_PED( tempObj ) )
							PROCESS_TIMETABLE_PICKUP_STATE(	sCurrentSpecialPickupState.ePickupState,
															LocalPlayerPed,
															tempObj,
															iobj,
															sCurrentSpecialPickupState.iStage )
						ENDIF
					ELSE
						PROCESS_TIMETABLE_PICKUP_STATE(	sCurrentSpecialPickupState.ePickupState,
														LocalPlayerPed,
														tempObj,
														MC_playerBD[ iPartToUse ].iObjHacking,
														sCurrentSpecialPickupState.iStage )
					ENDIF
					
				ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iGotoRange != 0
					INT iMultiRuleNo = GET_ENTITY_MULTIRULE_NUMBER(iobj,MC_playerBD[iPartToUse].iteam,ci_TARGET_OBJECT)
							
					IF (iMultiRuleNo != -1)
					AND NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectBitSet, cibsOBJ_IgnoreProximityPrimaryRule + iMultiRuleNo)
						bHasGoToRange = TRUE
						//First Person Check added to fix url:bugstar:3130707
						IF ( (NOT IS_ENTITY_ATTACHED_TO_ANY_PED( tempObj )) OR GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON)
						AND ( GET_DISTANCE_BETWEEN_ENTITIES( LocalPlayerPed, tempObj ) <= g_FMMC_STRUCT_ENTITIES.sPlacedObject[ iobj ].iGotoRange )
							SET_OBJ_NEAR(iObj)
						ELSE
							#IF IS_DEBUG_BUILD
							IF bObjectGotoDebug
								PRINTLN("[JS][ObjGoto] - Distance not within range: ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[ iobj ].iGotoRange ," > ", GET_DISTANCE_BETWEEN_ENTITIES( LocalPlayerPed, tempObj ))
							ENDIF
							#ENDIF
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
						IF bObjectGotoDebug
							PRINTLN("[JS][ObjGoto] - cibsOBJ_IgnoreProximityPrimaryRule is set or iMultiRuleNo (",iMultiRuleNo,") is -1")
						ENDIF
						#ENDIF
					ENDIF
					
				ENDIF

				// Else the range is 0 and therefore we want you to have to pick up the object first
				IF NOT bHasGoToRange
				AND IS_ENTITY_ATTACHED_TO_ENTITY( tempObj, LocalPlayerPed )
					SET_OBJ_NEAR(iObj)
				ENDIF
			ENDIF
			
			INT iShardOption = ciTICKER_ONLY
						
			// This is the string that will get displayed on the Control Object HUD
			STRING strControlHUDText = "CON_TIME" // "CONTROL"
						
			// Which can get overriden by the word SCORE if required
			IF IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitset[ iRule ], ciBS_RULE_USE_SCORE_TEXT )
				strControlHUDText = "FMMC_CPSCR" // "SCORE"
			ENDIF
					
			IF MC_serverBD.iObjCarrier[iobj] = iPartToUse
			OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitsetThirteen, ciENABLE_FORCE_CONTROL_OBJECT_BARS_TO_BE_TEAM_BASED)
				IF MC_serverBD_4.iObjRule[iobj][iTeam]= FMMC_OBJECTIVE_LOGIC_CAPTURE
				AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetNine[iRule], ciBS_RULE9_USE_GRANULAR_CAPTURE)
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitsetThirteen, ciENABLE_FORCE_CONTROL_OBJECT_BARS_TO_BE_TEAM_BASED)
						IF MC_ServerBD.iNumberOfTeams > 3
							HIDE_BOTTOM_RIGHT_CODE_UI()
						ENDIF
						
						INT iTeamO
						FOR iTeamO = 0 TO MC_ServerBD.iNumberOfTeams - 1					
							IF IS_TEAM_ACTIVE(iTeamO)
								IF bPlayerToUseOK
									IF NOT IS_SPECTATOR_HUD_HIDDEN()
									AND NOT IS_SPECTATOR_SHOWING_NEWS_HUD()
									AND NOT IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitset[ iRule ], ciBS_RULE_HIDE_HUD )	
									AND NOT g_bMissionEnding
									AND NOT (IS_BIT_SET(MC_ServerBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH) AND IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_SUDDEN_DEATH_POWER_PLAY))
									AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciUSE_POWER_MAD_HUD)
									AND IS_BIT_SET(MC_serverBD.iServerBitSet, SBBOOL_FIRST_UPDATE_DONE)
										TEXT_LABEL_15 tl15_CONTROL
										
										IF iTeamO = MC_playerBD[iLocalPart].iteam
											tl15_CONTROL = "DZ_Y_TEAM"
										ELSE
											IF DOES_TEXT_LABEL_EXIST(g_FMMC_STRUCT.tlCustomName[iTeamO])
												tl15_CONTROL = g_FMMC_STRUCT.tlCustomName[iTeamO]
											ELSE
												tl15_CONTROL = "FM_TDM_TEAM_S"
												tl15_CONTROL += iTeamO
											ENDIF
										ENDIF								

										HUD_COLOURS hcTeamColour = GET_HUD_COLOUR_FOR_FMMC_TEAM(iTeamO, LocalPlayer)
										HUDORDER eHUDOrder = INT_TO_ENUM(HUDORDER, ENUM_TO_INT(HUDORDER_FOURTHBOTTOM) - iTeamO)
										
										
										DRAW_GENERIC_METER( iTimeObjectTeamControlBarCached[iTeamO] + MC_serverBD.iObjectCaptureOffset[iTeamO],
															MC_serverBD.iObjTakeoverTime[ iTeamO ][ iObjectPriority ],
															tl15_CONTROL,
															hcTeamColour,
															-1,
															eHUDOrder, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, hcTeamColour
															)
									ENDIF
								ENDIF
							ENDIF
						ENDFOR
					ELSE						
						IF HAS_NET_TIMER_STARTED(MC_serverBD_1.tdControlObjTimer[iobj])
							IF bPlayerToUseOK
								IF NOT IS_SPECTATOR_HUD_HIDDEN()
								AND NOT IS_SPECTATOR_SHOWING_NEWS_HUD()
								AND NOT IS_BIT_SET( g_FMMC_STRUCT.sFMMCEndConditions[ iTeam ].iRuleBitset[ iRule ], ciBS_RULE_HIDE_HUD )
								AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFifteen, ciENABLE_NEVER_SHOW_BOTTOM_RIGHT_CAPTURE_BARS)
								AND NOT g_bMissionEnding
								
									DRAW_GENERIC_METER( GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME( MC_serverBD_1.tdControlObjTimer[iobj]) + MC_serverBD.iObjectCaptureOffset[iTeam],
														MC_serverBD.iObjTakeoverTime[ iTeam ][ iObjectPriority ],
														strControlHUDText,
														HUD_COLOUR_GREEN,
														-1
														)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				IF MC_serverBD.iObjCarrier[iobj] = iPartToUse
				OR IS_ENTITY_ATTACHED_TO_ENTITY(tempObj, LocalPlayerPed)
					IF iSpectatorTarget = -1
						IF MC_serverBD_4.iObjRule[iobj][MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER
						OR MC_serverBD_4.iObjRule[iobj][MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_GET_AND_HOLD
						OR MC_serverBD_4.iObjRule[iobj][MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_CAPTURE
						OR MC_serverBD_4.iObjRule[iobj][MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_PROTECT
							
							IF MC_playerBD[iPartToUse].iObjectiveTypeCompleted = -1
								IF NOT IS_BIT_SET(iobjcolBoolCheck,iobj)
									INT isublogic = GET_CTF_TICKER_ICON_TYPE_TO_DISPLAY()
									
									IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectBitSet, cibsOBJ_Use_Booty_Collection_Messages)
										isublogic = ciTICKER_BOOTY
										iShardOption = ciTICKER_AND_SHARD
									ENDIF
									
									// Award, In order to win at Capture, someone has to get the goods. Pick up a package or vehicle in any Capture mode.
									IF MC_playerBD[iLocalPart].iClientLogicStage <> CLIENT_MISSION_STAGE_DELIVER_OBJ
										IF Is_Player_Currently_On_MP_CTF_Mission(LocalPlayer)
											IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
												PRINTLN("[RCC MISSION] AWARD, MP_AWARD_PICKUP_CAP_PACKAGES ")
												INCREMENT_MP_INT_CHARACTER_AWARD(MP_AWARD_PICKUP_CAP_PACKAGES)
											ELSE
												PRINTLN("[RCC MISSION] AWARD, MP_AWARD_PICKUP_CAP_PACKAGES NOT INCREMENTED because in fake MP mode ")
											ENDIF
										ENDIF
									ENDIF
									
									IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_STOCKPILE(g_FMMC_STRUCT.iAdversaryModeType)
										IF MC_serverBD_1.iPTLDroppedPackageLastOwner[iobj] != -1
										OR WAS_PICKUP_AT_ANOTHER_TEAM_HOLDING(iObj, MC_playerBD[iLocalPart].iteam)
											isublogic = ciTICKER_STOLEN
										ENDIF
									ENDIF									
									
									BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_COLLECT_OBJ,0,MC_playerBD[iPartToUse].iteam,isublogic,NULL,ci_TARGET_OBJECT,iobj,TRUE,iShardOption, IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciENABLE_IN_AND_OUT_SOUNDS))
									IF NOT IS_OBJECT_A_CONTAINER(tempObj)
										IF iPackageColXPCount < 20
										AND NOT IS_THIS_A_SPECIAL_PICKUP( iObj )
										AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitsetTwo[MC_serverBD_4.iObjPriority[iobj][MC_playerBD[iPartToUse].iteam]], ciBS_RULE2_DONT_GIVE_PLAYER_XP)
											IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
												GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD,LocalPlayerPed, "XPT_MINORT",XPTYPE_ACTION,XPCATEGORY_COMPLETE_MISSION_DELIVERED_OBJ, g_sMPTunables.ixp_tuneable_collect_package_mission, 1)
											ENDIF
											iPackageColXPCount++
										ENDIF
									ENDIF
									SET_BIT(iobjcolBoolCheck,iobj)
								ENDIF
							ENDIF
							iMyObjectCarryCount++
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF iSpectatorTarget = -1
					IF MC_playerBD[iPartToUse].iObjectiveTypeCompleted =-1
						IF IS_BIT_SET(iobjcolBoolCheck,iobj)
							INT isublogic = GET_CTF_TICKER_ICON_TYPE_TO_DISPLAY()
							
							IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iobj].iObjectBitSet, cibsOBJ_Use_Booty_Collection_Messages)
								isublogic = ciTICKER_BOOTY
								//iShardOption = ciTICKER_AND_SHARD
							ENDIF
							
							IF MC_serverBD_4.iObjRule[iobj][MC_playerBD[iPartToUse].iteam] = FMMC_OBJECTIVE_LOGIC_GET_AND_HOLD
								IF IS_BIT_SET(MC_serverBD.iObjAtYourHolding[MC_playerBD[iPartToUse].iteam],iobj)									
									BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_DELIVER_OBJ,0,MC_playerBD[iPartToUse].iteam,isublogic,NULL,ci_TARGET_OBJECT,iobj,TRUE,iShardOption, IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciENABLE_IN_AND_OUT_SOUNDS))
									SET_MISSION_TEST_COMPLETE()
									IF iPackageDelXPCount < 10
									AND NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_playerBD[iPartToUse].iteam].iRuleBitsetTwo[MC_serverBD_4.iObjPriority[iobj][MC_playerBD[iPartToUse].iteam]], ciBS_RULE2_DONT_GIVE_PLAYER_XP)
										IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
											FLOAT fdelRP = (100*g_sMPTunables.fxp_tunable_Deliver_a_package_bonus)
											INT idelRP = ROUND(fdelRP)
											GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD,LocalPlayerPed, "XPT_MINORT",XPTYPE_ACTION,XPCATEGORY_COMPLETE_MISSION_DELIVERED_OBJ, idelRP, 1)
										ENDIF
										iPackageDelXPCount++
									ENDIF
								ELSE
									BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_DROP_OBJ,0,MC_playerBD[iPartToUse].iteam,isublogic,NULL,ci_TARGET_OBJECT,iobj,TRUE,iShardOption, IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciENABLE_IN_AND_OUT_SOUNDS))
								ENDIF
							ELSE
								BROADCAST_FMMC_CUSTOM_TICKER_TO_ALL_PLAYERS(TICKER_EVENT_DROP_OBJ,0,MC_playerBD[iPartToUse].iteam,isublogic,NULL,ci_TARGET_OBJECT,iobj,TRUE,iShardOption, IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciENABLE_IN_AND_OUT_SOUNDS))
							ENDIF
								CLEAR_BIT(iobjcolBoolCheck,iobj)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	// ensures that the timetable isn't visible on all machines, remote or otherwise once picked up
	IF( g_FMMC_STRUCT_ENTITIES.sPlacedObject[ iobj ].mn = HEI_PROP_HEI_TIMETABLE )
		IF( DOES_ENTITY_EXIST( tempObj ) )
			IF( IS_ENTITY_ATTACHED_TO_ANY_PED( tempObj ) )
				CPRINTLN( DEBUG_SIMON, "Hiding HEI_PROP_HEI_TIMETABLE using SET_ENTITY_LOCALLY_INVISIBLE" )
				SET_ENTITY_LOCALLY_INVISIBLE( tempObj )
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_THIS_MODEL_A_CCTV_CAMERA(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].mn)
		PROCESS_CCTV_MOVING_CAMERAS(tempObj, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].sObjElectronicData.sCCTVData, iObj, iObjCachedCamIndexes[iObj], FMMC_ELECTRONIC_ENTITY_TYPE__OBJECT)
	ENDIF
	
	PROCESS_OBJECT_HEALTHBARS (iObj, tempObj)
ENDPROC

PROC PROCESS_OBJECT_TRANSFORMATIONS(INT iObj)
	IF IS_BIT_SET(iLocalBoolCheck23, LBOOL23_CLEANUP_OUTFIT_ON_RESPAWN)
	AND bLocalPlayerPedOk
		IF NOT APPLY_OUTFIT_TO_LOCAL_PLAYER(INT_TO_ENUM(MP_OUTFIT_ENUM, GET_LOCAL_DEFAULT_OUTFIT(MC_playerBD[iPartToUse].iTeam)))
			CLEAR_BIT(iLocalBoolCheck23, LBOOL23_CLEANUP_OUTFIT_ON_RESPAWN)
		ENDIF
	ENDIF
	IF (iObj = iTransformationObjectIndex
	AND NOT (IS_BIT_SET(MC_playerBD[iLocalPart].iGranularObjCapturingBS, iObj) OR MC_playerBD[iLocalPart].iObjNear = iObj))
	OR (iObj = iTransformationObjectIndex
	AND (IS_BIT_SET(MC_playerBD[iLocalPart].iGranularObjCapturingBS, iObj) OR MC_playerBD[iLocalPart].iObjNear = iObj)
	AND NOT bLocalPlayerPedOk)
		IF SHOULD_CLEANUP_OBJECT_TRANSFORMATION_NOW()
			CLEANUP_OBJECT_TRANSFORMATIONS()
			iTransformationObjectIndex = -1
		ENDIF
	ELIF (IS_BIT_SET(MC_playerBD[iLocalPart].iGranularObjCapturingBS, iObj) OR MC_playerBD[iLocalPart].iObjNear = iObj)
	AND NOT IS_BIT_SET(iObjDeliveredBitset, iObj)
	AND bLocalPlayerPedOk
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetTwo, cibsOBJ2_TransformJuggernaut)
			IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_TRANSFORMED_INTO_JUGGERNAUT)
				IF TRANSFORM_LOCAL_PLAYER_INTO_JUGGERNAUT(DEFAULT, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iJuggernautHealth)
					IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iCustomShard > 0
						BROADCAST_FMMC_DISPLAY_CUSTOM_OBJECT_PICKUP_SHARD(LocalPlayer, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iCustomShard, MC_playerBD[iLocalPart].iTeam)
					ENDIF
					iTransformationObjectIndex = iObj
				ENDIF
			ENDIF
		ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetTwo, cibsOBJ2_TransformBeast)
			IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_TRANSFORMED_INTO_BEAST)
				IF TRANSFORM_LOCAL_PLAYER_INTO_BEAST()
					IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iCustomShard > 0
						BROADCAST_FMMC_DISPLAY_CUSTOM_OBJECT_PICKUP_SHARD(LocalPlayer, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iCustomShard, MC_playerBD[iLocalPart].iTeam)
					ENDIF
					iTransformationObjectIndex = iObj
				ENDIF
			ENDIF
		ELIF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectBitSetTwo, cibsOBJ2_UpgradeVeh)
			IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_UPGRADED_WEP_VEHICLE)
				UPGRADE_WEAPONISED_VEHICLE()
				IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iCustomShard > 0
					BROADCAST_FMMC_DISPLAY_CUSTOM_OBJECT_PICKUP_SHARD(LocalPlayer, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iCustomShard, MC_playerBD[iLocalPart].iTeam)
				ENDIF
				iTransformationObjectIndex = iObj
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_MISSION_FUSEBOX_OBJECT(OBJECT_INDEX oiObj, INT iObj)

 	UNUSED_PARAMETER(oiObj)
	
	IF NOT IS_THIS_MODEL_A_FUSEBOX(g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].mn)
		EXIT
	ENDIF
	
	VECTOR vZoneCoords
	IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjLinkedZone > -1
		vZoneCoords = LERP_VECTOR(g_FMMC_STRUCT_ENTITIES.sPlacedZones[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjLinkedZone].vPos[0], g_FMMC_STRUCT_ENTITIES.sPlacedZones[g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjLinkedZone].vPos[1], 0.5)
	ENDIF
	
	IF IS_BIT_SET(sObjTaserVars.iHitByTaserBS, iObj)
	OR IS_ENTITY_DEAD(oiObj)
	
		IF IS_BIT_SET(sObjTaserVars.iReactedToTaserBS, iObj)
			// Already reacted!
			EXIT
		ENDIF
	
		IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjLinkedZone > -1
			IF NOT IS_BIT_SET(iMetalDetectorZoneDisabledBitset, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjLinkedZone)
				PRINTLN("[MetalDetector][Taser] Setting iMetalDetectorZoneDisabledBitset / ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjLinkedZone)
				SET_BIT(iMetalDetectorZoneDisabledBitset, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjLinkedZone)
				
				PLAY_SOUND_FROM_COORD(-1, "Metal_Detector_Offline", vZoneCoords, "dlc_ch_heist_finale_security_alarms_sounds", TRUE, 800)
				
				IF IS_ENTITY_DEAD(oiObj)
					PLAY_SOUND_FROM_COORD(-1, "Security_Box_Offline_Gun", GET_ENTITY_COORDS(oiObj), "dlc_ch_heist_finale_security_alarms_sounds", TRUE, 800)
				ENDIF
			ENDIF
		ENDIF
		
		SET_BIT(sObjTaserVars.iReactedToTaserBS, iObj)
	ELSE
		IF IS_BIT_SET(sObjTaserVars.iReactedToTaserBS, iObj)
			IF IS_BIT_SET(iMetalDetectorZoneDisabledBitset, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjLinkedZone)
				PRINTLN("[MetalDetector][Taser] Clearing iMetalDetectorZoneDisabledBitset / ", g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjLinkedZone)
				CLEAR_BIT(iMetalDetectorZoneDisabledBitset, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjLinkedZone)
				
				PLAY_SOUND_FROM_COORD(-1, "Metal_Detector_Online", vZoneCoords, "dlc_ch_heist_finale_security_alarms_sounds", TRUE, 800)
			ENDIF
			
			CLEAR_BIT(sObjTaserVars.iReactedToTaserBS, iObj)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_OBJECT_INVENTORIES(INT iObj)
	
	//cleanup if they aren't holding an inventory object anymore
	IF iObj = iInventoryObjectIndex
	AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInventory > 0
	AND MC_serverBD.iObjCarrier[iObj] != iLocalPart
		CLEANUP_OBJECT_INVENTORIES()
	ENDIF
	
	//give them the inventory if they are holding it
	IF MC_serverBD.iObjCarrier[iObj] = iLocalPart
	AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInventory > 0
	AND NOT IS_BIT_SET(iObjDeliveredBitset, iObj)
		IF NOT IS_BIT_SET(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_HAS_OBJECT_INVENTORY)
			PRINTLN("PROCESS_OBJECT_INVENTORIES - Player should be given an Object Inventory as they are holding Obj ", iObj)
			IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInventory = 1
				GIVE_PLAYER_STARTING_MISSION_WEAPONS(WEAPONINHAND_LASTWEAPON_BOTH, TRUE, TRUE)
				PRINTLN("PROCESS_OBJECT_INVENTORIES - Giving Player Mid-Mission Inventory 1!")
			ELIF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iObjectInventory = 2
				GIVE_PLAYER_STARTING_MISSION_WEAPONS(WEAPONINHAND_LASTWEAPON_BOTH, TRUE, FALSE, FALSE, TRUE)
				PRINTLN("PROCESS_OBJECT_INVENTORIES - Giving Player Mid-Mission Inventory 2!")
			ENDIF
			SET_BIT(MC_playerBD[iLocalPart].iClientBitSet3, PBBOOL3_HAS_OBJECT_INVENTORY)
			iInventoryObjectIndex = iObj
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_OBJECTS_EVERY_FRAME()
	BOOL bAreAnySpecialPickupsIncomplete = FALSE
	BOOL bAreBinbagsInMission = FALSE
	g_bCheckForGarbageBagInShop = FALSE
	sIWInfo.iInteractWith_HelptextGiverIndex = -1
	
	IF( MC_serverBD.iNumObjCreated > 0 AND MC_serverBD.iNumObjCreated <= FMMC_MAX_NUM_OBJECTS )
		iGranularPointsThisFrame = 0
		INT i = 0
		FOR i = 0 TO ( MC_serverBD.iNumObjCreated - 1 )
		
			OBJECT_INDEX tempObj
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_ServerBD_1.sFMMC_SBD.niObject[i])
				tempObj = NET_TO_OBJ(MC_ServerBD_1.sFMMC_SBD.niObject[i])
			ENDIF
			
			IF( IS_THIS_A_SPECIAL_PICKUP( i ) )
				IF( g_FMMC_STRUCT_ENTITIES.sPlacedObject[ i ].mn = HEI_PROP_HEIST_BINBAG )
					bAreBinbagsInMission = TRUE
				ENDIF
				bAreAnySpecialPickupsIncomplete = bAreAnySpecialPickupsIncomplete OR IS_SPECIAL_PICKUP_INCOMPLETE( i )
				// Reorientate any bin bags in the mission
				IF( IS_THIS_SPECIAL_PICKUP_A_BINBAG( i ) )
					HANDLE_BINBAG_ORIENTATION( i, FALSE, TRUE )
				ENDIF
				MANAGE_SPECIAL_PICKUP_ASSETS( i )
			ENDIF
			DISPLAY_OBJECT_HUD( i )
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciDAWN_RAID_ENABLE_MODE)
			AND g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].mn = INT_TO_ENUM(MODEL_NAMES, HASH("imp_prop_adv_hdsec"))
				IF IS_BIT_SET(MC_serverBD.iServerBitSet3, SBBOOL3_MINIGAME_TARGET_FOUND)
				AND bLocalPlayerPedOk
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_ServerBD_1.sFMMC_SBD.niObject[i])
						IF NOT IS_ENTITY_ATTACHED_TO_ANY_PED(tempObj)
						AND VDIST2(GET_ENTITY_COORDS(LocalPlayerPed), GET_ENTITY_COORDS(tempObj)) < POW(2,2)
							IF NETWORK_HAS_CONTROL_OF_ENTITY(tempObj)
								PRINTLN("[JS]PROCESS_OBJ_BODY - Manually attaching dawn raid pickup")
								ATTACH_PORTABLE_PICKUP_TO_PED(tempObj, LocalPlayerPed)
							ELSE
								NETWORK_REQUEST_CONTROL_OF_ENTITY(tempObj)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MC_ServerBD_1.sFMMC_SBD.niObject[i])
				PROCESS_FLAG_RETURN(i)
			ENDIF
			
			IF DOES_ENTITY_EXIST(tempObj)
				PROCESS_TASERED_ENTITY(sObjTaserVars, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].sObjElectronicData, tempObj, i, FMMC_ELECTRONIC_ENTITY_TYPE__OBJECT)
				PROCESS_MISSION_FUSEBOX_OBJECT(tempObj, i)
			ENDIF
			
			IF MC_serverBD_4.iObjPriority[i][MC_PlayerBD[iLocalPart].iteam] < FMMC_MAX_RULES
			AND MC_serverBD_4.iObjRule[i][MC_PlayerBD[iLocalPart].iteam] = FMMC_OBJECTIVE_LOGIC_CAPTURE
			
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iLocalPart].iteam].iRuleBitsetNine[MC_serverBD_4.iObjPriority[i][MC_PlayerBD[iLocalPart].iteam]], ciBS_RULE9_USE_GRANULAR_CAPTURE)
					PROCESS_GRANULAR_CAPTURE(i)
				ENDIF

				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_TRANSFORM_ASSETS)
				AND NOT (IS_BIT_SET(MC_serverBD.iServerBitSet1, SBBOOL1_SUDDEN_DEATH)
				AND (IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciTRANSFORM_TO_JUGGERNAUT_ON_SUDDEN_DEATH)
				OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFourteen, ciTRANSFORM_TO_BEAST_ON_SUDDEN_DEATH)))
					IF NETWORK_DOES_NETWORK_ID_EXIST(MC_serverBD_1.sFMMC_SBD.niObject[i])
						IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iLocalPart].iteam].iRuleBitsetNine[MC_serverBD_4.iObjPriority[i][MC_PlayerBD[iLocalPart].iteam]], ciBS_RULE9_USE_GRANULAR_CAPTURE)
							IF IS_ENTITY_ATTACHED_TO_ANY_PED(tempObj)
								ENTITY_INDEX tempPlayerEnt = GET_ENTITY_ATTACHED_TO(tempObj)
								IF DOES_ENTITY_EXIST(tempPlayerEnt)
									IF GET_PED_INDEX_FROM_ENTITY_INDEX(tempPlayerEnt) = LocalPlayerPed
										IF MC_playerBD[iLocalPart].iObjNear != i
											PRINTLN("[JS] OBJLOOP - we are now carrying obj - ", i)
											MC_playerBD[iLocalPart].iObjNear = i
										ENDIF
									ELSE
										IF MC_playerBD[iLocalPart].iObjNear = i
											PRINTLN("[JS] OBJLOOP - (1) we are no longer carrying obj - ", i)
											MC_playerBD[iLocalPart].iObjNear = -1
										ENDIF
									ENDIF
								ELSE
									IF MC_playerBD[iLocalPart].iObjNear = i
										PRINTLN("[JS] OBJLOOP - (2) we are no longer carrying obj - ", i)
										MC_playerBD[iLocalPart].iObjNear = -1
									ENDIF
								ENDIF
							ELSE
								IF MC_playerBD[iLocalPart].iObjNear = i
									PRINTLN("[JS] OBJLOOP - (3) we are no longer carrying obj - ", i)
									MC_playerBD[iLocalPart].iObjNear = -1
								ENDIF
							ENDIF
						ENDIF
						
						PROCESS_OBJECT_TRANSFORMATIONS(i)
						
					ELSE
						IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iLocalPart].iteam].iRuleBitsetNine[MC_serverBD_4.iObjPriority[i][MC_PlayerBD[iLocalPart].iteam]], ciBS_RULE9_USE_GRANULAR_CAPTURE)
							IF MC_playerBD[iLocalPart].iObjNear = i
								PRINTLN("[JS] OBJLOOP - (4) we are no longer carrying obj - ", i)
								MC_playerBD[iLocalPart].iObjNear = -1
							ENDIF
						ENDIF
					ENDIF
					IF NOT IS_BIT_SET(MC_serverBD.iServerBitset3, SBBOOL3_TEAM_0_SHOULD_RESPAWN_BACK_AT_START_NOW + MC_playerBD[iLocalPart].iTeam)
						IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[MC_PlayerBD[iLocalPart].iteam].iRuleBitsetNine[MC_serverBD_4.iObjPriority[i][MC_PlayerBD[iLocalPart].iteam]], ciBS_RULE9_USE_GRANULAR_CAPTURE)
							IF MC_playerBD[iLocalPart].iObjNear = i
								PRINTLN("[JS] OBJLOOP - (5) we are no longer carrying obj - ", i)
								MC_playerBD[iLocalPart].iObjNear = -1
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			PROCESS_OBJECT_INVENTORIES(i)
			PROCESS_OBJECT_RULE_VISIBILITY(i)
			IF bIsLocalPlayerHost
				IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iRespawnOnRuleChangeBS > 0
					INT iTeam
					FOR iTeam = 0 TO (MC_serverBD.iNumberOfTeams - 1)
						IF MC_serverBD_4.iCurrentHighestPriority[iTeam] > 0
							IF (MC_serverBD_4.iCurrentHighestPriority[iTeam] > 0
							AND IS_BIT_SET(iLocalBoolCheck6, LBOOL6_TEAM_0_HAS_NEW_PRIORITY_THIS_FRAME + iTeam))
							OR IS_BIT_SET(MC_serverBD.iServerBitSet5, SBBOOL5_RETRY_CURRENT_RULE_TEAM_0 + iTeam)
								IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iRespawnOnRuleChangeBS, iTeam)
									SET_BIT(MC_serverBD.iObjCleanup_NeedOwnershipBS, i)
									IF MC_serverBD_2.iCurrentObjRespawnLives[i] < 1
										MC_serverBD_2.iCurrentObjRespawnLives[i] = 1
									ENDIF
									PRINTLN( "[JS] Cleaning up obj to respawn on rule change ", i )
									BREAKLOOP
								ENDIF
							ENDIF
						ENDIF
					ENDFOR
				ENDIF
								
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitsetThirteen, ciENABLE_FORCE_CONTROL_OBJECT_BARS_TO_BE_TEAM_BASED)
					PRINTLN("[LM][CONTROL OBJECT] - MC_serverBD.iObjTeamCarrier: ",MC_serverBD.iObjTeamCarrier)
					
					IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectBitSetTwo, cibsOBJ2_TeamControlBarObject)
						INT iTeamO
						FOR iTeamO = 0 TO FMMC_MAX_TEAMS-1		
							IF iTeamO != MC_serverBD.iObjTeamCarrier
								PRINTLN("[LM][CONTROL OBJECT] - iTeamO: ", iTeamO, " Does not equal to the carrier.")
								
								IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectBitSetTwo, cibsOBJ2_DontResetTimerWhenDropped)
									IF HAS_NET_TIMER_STARTED(MC_serverBD.tdControlObjTeamTimer[iTeamO])
										
										PRINTLN("[LM][CONTROL OBJECT] - iTeamO: ", iTeamO, " Timer has started and is no longer the carrier, so we need to pause its progress.")
										
										// Causes an every frame change to server BD which causes an assert because the BD is altered too frequently.
										//PRINTLN("[LM][CONTROL OBJECT] - (temp print) Timer Before Pause: ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdControlObjTeamTimer[iTeamO]))										
										//NET_TIMER_PAUSE_THIS_FRAME(MC_serverBD.tdControlObjTeamTimer[iTeamO], tdObjectControlTimerTeamPause[iTeamO])
										//PRINTLN("[LM][CONTROL OBJECT] - (temp print) Timer After Pause: ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdControlObjTeamTimer[iTeamO]))										
										
										IF NOT IS_BIT_SET(MC_serverBD.iServerBitSet6, SBBOOL6_CONTROL_TIME_STOPPED_T0 + iTeamO)
											MC_serverBD.iTimeObjectTeamControlBarCache[iTeamO] = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdControlObjTeamTimer[iTeamO])
											SET_BIT(MC_serverBD.iServerBitSet6, SBBOOL6_CONTROL_TIME_STOPPED_T0 + iTeamO)
											PRINTLN("[LM][CONTROL OBJECT] - iTeamO: ", iTeamO, " Cached MC_serverBD.iTimeObjectTeamControlBarCache: ", MC_serverBD.iTimeObjectTeamControlBarCache[iTeamO])
										ENDIF
									ELSE										
										IF IS_BIT_SET(MC_serverBD.iServerBitSet6, SBBOOL6_CONTROL_TIME_STOPPED_T0 + iTeamO)
											CLEAR_BIT(MC_serverBD.iServerBitSet6, SBBOOL6_CONTROL_TIME_STOPPED_T0 + iTeamO)
										ENDIF
									ENDIF
								ENDIF
							ELSE								
								IF IS_BIT_SET(MC_serverBD.iServerBitSet6, SBBOOL6_CONTROL_TIME_STOPPED_T0 + iTeamO)
									CLEAR_BIT(MC_serverBD.iServerBitSet6, SBBOOL6_CONTROL_TIME_STOPPED_T0 + iTeamO)
									
									PRINTLN("[LM][CONTROL OBJECT] - Starting the Timer Back Up for iTeamO: ", iTeamO, " Cached MC_serverBD.iTimeObjectTeamControlBarCache: ", MC_serverBD.iTimeObjectTeamControlBarCache[iTeamO])
									PRINTLN("[LM][CONTROL OBJECT] - Before: GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdControlObjTeamTimer[iTeamO]): ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdControlObjTeamTimer[iTeamO]))
									
									INT iTimeOffset = (GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdControlObjTeamTimer[iTeamO]) - MC_serverBD.iTimeObjectTeamControlBarCache[iTeamO])
									INT iNewTime = (GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdControlObjTeamTimer[iTeamO]) - iTimeOffset)
									
									PRINTLN("[LM][CONTROL OBJECT] - custom iTimeOffset: ", iTimeOffset)
									PRINTLN("[LM][CONTROL OBJECT] - custom iNewTime: ", iNewTime)
									
									MC_serverBD.tdControlObjTeamTimer[iTeamO].Timer = GET_TIME_OFFSET(MC_serverBD.tdControlObjTeamTimer[iTeamO].Timer, iTimeOffset)
									MC_serverBD.tdControlObjTeamTimer[iTeamO].bInitialisedTimer = TRUE
									
									PRINTLN("[LM][CONTROL OBJECT] - After: GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdControlObjTeamTimer[iTeamO]): ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MC_serverBD.tdControlObjTeamTimer[iTeamO]))
								ENDIF
							ENDIF
						ENDFOR
					ENDIF
				ELSE
					IF MC_serverBD.iObjCarrier[i] = -1
						IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].iObjectBitSetTwo, cibsOBJ2_DontResetTimerWhenDropped)
							IF HAS_NET_TIMER_STARTED(MC_serverBD_1.tdControlObjTimer[i])
								//Originally it was just the team-based object control bar start/stop that was requested.
								//NET_TIMER_PAUSE_THIS_FRAME(MC_serverBD_1.tdControlObjTimer[i], tdObjectControlTimerPause)
								//PRINTLN("[LM][CONTROL OBJECT] - PAUSING MC_serverBD_1.tdControlObjTimer[i]: ", i)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
		
		IF MC_ServerBD_4.iCurrentHighestPriority[MC_PlayerBD[iLocalPart].iTeam] < FMMC_MAX_RULES
			IF HAS_NET_TIMER_STARTED(tdGranularCaptureTimer)
				IF HAS_NET_TIMER_EXPIRED(tdGranularCaptureTimer, ciGRANULAR_CAPTURE_TIME)
					IF iGranularPointsThisFrame > 0
						MC_PlayerBD[iLocalPart].iGranularCurrentPoints += iGranularPointsThisFrame
						MC_PlayerBD[iLocalPart].iGranularTotalPoints += iGranularPointsThisFrame
						iGranularPointsThisFrame = 0
					ENDIF
					RESET_NET_TIMER(tdGranularCaptureTimer)
				ENDIF
			ENDIF
		ENDIF
	ENDIF // Update special pickups for this player
	IF( bAreBinbagsInMission )
		UPDATE_BIN_BAG_FOOTSTEP_SWEETENERS( sCurrentSpecialPickupState.iSweetenerEnabled, sCurrentSpecialPickupState.obj )
		UPDATE_BINBAG_UNFREEZABLE()		
	ENDIF
	IF( bAreAnySpecialPickupsIncomplete )
		PROCESS_SPECIAL_PICKUP_STATE()
		g_bCheckForGarbageBagInShop = TRUE
	ELSE
		DELAYED_RESET()
		IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_JUGGERNAUT(g_FMMC_STRUCT.iAdversaryModeType)
			CLEAR_FOOTSTEP_SWEETENERS_FOR_ALL_PLAYER_PEDS()
		ENDIF
		REMOVE_SPECIAL_PICKUP_ASSETS()
	ENDIF
	
	sIWInfo.iInteractWith_LastFrameHelptextGiverIndex = sIWInfo.iInteractWith_HelptextGiverIndex
ENDPROC

